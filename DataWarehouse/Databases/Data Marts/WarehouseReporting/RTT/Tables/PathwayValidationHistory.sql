﻿CREATE TABLE [RTT].[PathwayValidationHistory] (
    [Census]                DATETIME      NOT NULL,
    [RTTPathwayID]          VARCHAR (25)  NOT NULL,
    [EventDate]             SMALLDATETIME NOT NULL,
    [EventTime]             SMALLDATETIME NOT NULL,
    [EventType]             VARCHAR (50)  NULL,
    [OutcomeCode]           VARCHAR (20)  NULL,
    [OutcomeID]             INT           NOT NULL,
    [DateValidated]         DATETIME      NULL,
    [Comments]              VARCHAR (250) NULL,
    [NotApplicable]         VARCHAR (50)  NULL,
    [UserName]              VARCHAR (50)  NULL,
    [OrderID]               INT           NULL,
    [PathwayStatus]         VARCHAR (10)  NULL,
    [RTTWksWaiting]         INT           NULL,
    [RTTPathwayStartDate]   DATETIME      NULL,
    [SourcePatientNo]       INT           NULL,
    [TypedTime]             DATETIME      NULL,
    [SignedTime]            DATETIME      NULL,
    [DocumentID]            VARCHAR (100) NULL,
    [UserAdded]             VARCHAR (100) NULL,
    [TransactionTime]       DATETIME      NULL,
    [UserLastRevised]       VARCHAR (100) NULL,
    [RevisionTime]          DATETIME      NULL,
    [WeekReview]            INT           NULL,
    [OriginalPathwayStatus] VARCHAR (20)  NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_pvhistory_pstatus_rpi_ed]
    ON [RTT].[PathwayValidationHistory]([PathwayStatus] ASC)
    INCLUDE([RTTPathwayID], [EventDate]);


﻿CREATE Procedure [RTT].[BuildPathwayValidation]
as

 
declare @Census datetime
set @Census = (Select DateValue from RTT.dbo.Parameter where Parameter = 'RTTCENSUSDATE')


-- Validation report

Insert into RTT.PathwayValidationHistory
      (
      Census
      ,RTTPathwayID
      ,EventDate
      ,EventTime
      ,EventType
      ,OutcomeCode
      ,OutcomeID
      ,OrderID
      ,PathwayStatus
      ,RTTWksWaiting
      ,RTTPathwayStartDate
      ,SourcePatientNo
      ,OriginalPathwayStatus
      )
select
      Census
      ,GynaePathwayEvent.RTTPathwayID
      ,EventDate
      ,EventTime
      ,EventType
      ,OutcomeCode
      ,OutcomeID = 
				case
					when GynaePathwayEvent.Reportable = 'N' then 
						case
							when SourceofReferralCode = 'CAS' then 13
							when ClinicCode = 'EGUFUP' then 13
							when OutcomeCode = 'IVFWL' then 13
							else 14
						end					
					when left(GynaePathwayEvent.RTTPathwayID,2) = 'No' 
						then 1
					when RTTPathwayCondition is null then
						case
							when (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) <56 
							then 19
							else 2
						end
					when left(GynaePathwayEvent.EventType,5) in ('IPAdm','OPApp') 
						and 
							(	RTTPeriodStatus is null
							or
								left(RTTPeriodStatus,2) = '99' 
							)
						then 3
					when GynaePathwayEvent.PathwayStatus = 'Non Active' 
						and
							(	
								RTTPeriodStatus not in ('90A','91A')
							or
								RTTPeriodStatus is null
							)
						and DisposalCode in ('P4TG','P4WW')
						then 4
					when GynaePathwayEvent.EventType = 'OPReferral' 
						and (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) <56 
						then 5
					when GynaePathwayEvent.PathwayStatus = 'Start'
						and GynaePathwayEvent.OutcomeCode = 'IVFWL'
						and GynaePathwayEvent.EventType = 'IPWLAdd'
						and (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) <56
						then 16
					when GynaePathwayEvent.PathwayStatus = 'Stop'
						and RTTWksWaiting <18
						then 17
					when GynaePathwayEvent.PathwayStatus = 'Stop' 
						then 6
					when GynaePathwayEvent.PathwayStatus = 'Non Active' 
						and left(RTTPeriodStatus,2) in ('90','91')
						then 8
					when GynaePathwayEvent.PathwayStatus = 'Ticking'
						and (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) < 105
						then 18	
					when GynaePathwayEvent.PathwayStatus in ('Start','Ticking')
						then 9
					when GynaePathwayEvent.PathwayStatus = 'Start'
						and OutcomeCode = 'DNA'
						and RTTPeriodStatus = '33A'
						then 15

					else 10
				end
      ,GynaePathwayEvent.OrderID
      ,PathwayStatus
      ,RTTWksWaiting
      ,RTTPathwayStartDate
      ,SourcePatientNo
      ,PathwayStatus
from
      RTT.RTT.GynaePathwayEvent
left join
	(
	Select
		Ev.RTTPathwayID
		,Ev.OrderID
		,RTTPathwayStartDate = Start.EventDate
		,RTTWksWaiting = cast((datediff(day,Start.EventDate,Ev.EventDate)/7) as int)
	from 
		RTT.RTT.GynaePathwayEvent Ev
		
	inner join RTT.RTT.GynaePathwayEvent Start
	on Ev.RTTPathwayID = Start.RTTPathwayID
	
	where 
		Ev.PathwayStatus in ('Ticking','Stop')
	and Start.PathwayStatus = 'Start'
	and Start.OrderID < Ev.OrderID
	and not exists
		(
		Select
			1
		from 
			RTT.RTT.GynaePathwayEvent Latest
		where
			Latest.RTTPathwayID = Start.RTTPathwayID
		and Latest.PathwayStatus = Start.PathwayStatus
		and Latest.RTTPathwayID = Ev.RTTPathwayID
		and Latest.OrderID < Ev.OrderID
		and Latest.OrderID > Start.OrderID
		)
	) Pathway
on GynaePathwayEvent.RTTPathwayID = Pathway.RTTPathwayID
and GynaePathwayEvent.OrderID = Pathway.OrderID

where
      EventTime between '20150201' and @Census
and not exists
            (
            select
                  1
            from 
                  RTT.PathwayValidationHistory NoChange
            where
                  GynaePathwayEvent.RTTPathwayID = NoChange.RTTPathwayID
            and GynaePathwayEvent.OrderID = NoChange.OrderID
            and GynaePathwayEvent.EventTime = NoChange.EventTime
            and GynaePathwayEvent.EventType = NoChange.EventType
            )


Update PathwayValidationHistory
set PathwayStatus = GynaePathwayEvent.PathwayStatus
from RTT.RTT.GynaePathwayEvent
inner join RTT.PathwayValidationHistory
on GynaePathwayEvent.RTTPathwayID = PathwayValidationHistory.RTTPathwayID
and GynaePathwayEvent.OrderID = PathwayValidationHistory.OrderID
and GynaePathwayEvent.EventTime = PathwayValidationHistory.EventTime
and GynaePathwayEvent.EventType = PathwayValidationHistory.EventType
and GynaePathwayEvent.PathwayStatus <> PathwayValidationHistory.PathwayStatus


Update RTT.PathwayValidationHistory
set NotApplicable = null

-- Dropped off the PTL, so we don't need to revalidate
Update 
      PathwayValidationHistory
set 
      NotApplicable = 'EventNoLongerExists'
from 
      RTT.PathwayValidationHistory

left join RTT.RTT.GynaePathwayEvent
on PathwayValidationHistory.RTTPathwayID = GynaePathwayEvent.RTTPathwayID
and PathwayValidationHistory.EventTime = GynaePathwayEvent.EventTime
and PathwayValidationHistory.EventType = GynaePathwayEvent.EventType

where 
      GynaePathwayEvent.RTTPathwayID is null


-- Multiple events for the same date, 
; With RTTMultipleSameDateCTE
      (
      RTTPathwayID
      ,EventDate
      ,OrderID
      ,PathwayStatus
      )
as
      (
      select
            PathwayValidationHistory.RTTPathwayID
            ,PathwayValidationHistory.EventDate
            ,OrderID
            ,PathwayStatus
      from
            RTT.PathwayValidationHistory
      inner join
            (
            Select
                  RTTPathwayID
                  ,EventDate
            from
                  RTT.PathwayValidationHistory
            group by
                  RTTPathwayID
                  ,EventDate
            having 
                  count(*) >1
            )
            Multiple
      on PathwayValidationHistory.RTTPathwayID = Multiple.RTTPathwayID
      and PathwayValidationHistory.EventDate = Multiple.EventDate
      )


Update PathwayValidationHistory
set NotApplicable = 'OtherKeyEventOnSameDate'
from 
      RTTMultipleSameDateCTE Main

inner join RTTMultipleSameDateCTE Multiple
on Main.RTTPathwayID = Multiple.RTTPathwayID
and Main.EventDate = Multiple.EventDate

inner join RTT.PathwayValidationHistory
on PathwayValidationHistory.RTTPathwayID = Multiple.RTTPathwayID
and PathwayValidationHistory.OrderID = Multiple.OrderID

where
      Main.PathwayStatus in ('Start','Stop')
and Multiple.PathwayStatus not in ('Start','Stop')

      
-- Multiple events in validation table, only want the latest to be on report
; With RTTMultipleCTE
      (
      RTTPathwayID
      ,OrderID
      )
as
      (
      Select
            RTTPathwayID
            ,OrderID
      from 
            RTT.PathwayValidationHistory
      where
            NotApplicable is null
      and RTTPathwayID in 
                  (
                  Select
                        RTTPathwayID
                        
                  from
                        RTT.PathwayValidationHistory
                  where
                        NotApplicable is null
                  group by
                        RTTPathwayID
                  having 
                        count(*) >1
                  )
      )

Update PathwayValidationHistory
set NotApplicable = 'NotTheLatestEvent'
from 
      RTT.PathwayValidationHistory

inner join RTTMultipleCTE
on PathwayValidationHistory.RTTPathwayID = RTTMultipleCTE.RTTPathwayID
and PathwayValidationHistory.OrderID = RTTMultipleCTE.OrderID

where 
      NotApplicable is null
and   not exists
            (
            Select
                  1
            from
                  (
                  Select
                        RTTPathwayID
                        ,OrderID
                  from
                        RTTMultipleCTE    
                  where 
                        not exists
                        (
                        Select
                              1
                        from
                              RTTMultipleCTE Latest
                        where
                              Latest.RTTPathwayID = RTTMultipleCTE.RTTPathwayID
                        and Latest.OrderID > RTTMultipleCTE.OrderID
                        )
                  ) Valid
            where 
                  RTTMultipleCTE.RTTPathwayID = Valid.RTTPathwayID
            and RTTMultipleCTE.OrderID = Valid.OrderID
            )
            

Update 
	RTT.PathwayValidationHistory
set 
	OutcomeID = 12	
where 
	NotApplicable is null
and 
	(
		(OutcomeID in (5,16)
		and (datediff(day,EventTime,getdate())/7) >= 56 
		)
	or
		(OutcomeID = 18
		and (datediff(day,EventTime,getdate())/7) >= 105
		)
	)
	
-- Letter
Update 
	RTT.PathwayValidationHistory
set 
	DocumentID = BaseDocument.DocumentID
	,TypedTime = BaseDocument.TypedTime
	,SignedTime = BaseDocument.SignedTime
from 
	RTT.PathwayValidationHistory
	
inner join WarehouseOLAPMergedV2.Dictation.BaseDocument
on  BaseDocument.SourcePatientNo = PathwayValidationHistory.SourcePatientNo
and BaseDocument.DictatedTime = PathwayValidationHistory.EventDate
and DocumentTypeCode = 'O' 
and not exists
	(
	Select
		1
	from
		WarehouseOLAPMergedV2.Dictation.BaseDocument Latest
	where
		Latest.SourcePatientNo = BaseDocument.SourcePatientNo
	and Latest.DictatedTime = BaseDocument.DictatedTime
	and Latest.TypedTime > BaseDocument.TypedTime
	)

-- System User

select 
	GynaePathwayEvent.RTTPathwayID
	,GynaePathwayEvent.OrderID
	,ActionType
	,TransactionTime
	,UserName
into
	#RTTTransaction
from
	WarehouseOLAPMergedV2.TransactionLog.Base

inner join WarehouseOLAPMergedV2.TransactionLog.Activity
on	Activity.ActivityID = Base.ActivityID
and	Activity.ActivityTypeID = Base.ActivityTypeID

inner join RTT.RTT.GynaePathwayEvent
on Base.DistrictNo = GynaePathwayEvent.DistrictNo
and Base.SourceSpellNo = GynaePathwayEvent.SourceEncounterNo
and cast(Base.ActivityTime as date) = cast(GynaePathwayEvent.EventTime as date)

inner join WarehouseOLAPMergedV2.TransactionLog.BaseReference
on	BaseReference.MergeRecno = Base.MergeRecno

inner join WarehouseOLAPMergedV2.WH.SystemUser
on BaseReference.SystemUserID = SystemUser.SystemUserID

where
	TransactionTime >= '20150101'
and ActionType in('ADD','REVISE')
and Base.ActivityID in (1,4,7,107,111,61,62,65,66,69)
-- Exclude 11 for now as not currently bringing through Discharges; Discussed with TN who advised don't need IP Pathway 12
and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.TransactionLog.Base Previous
	where
		Previous.TransactionID = Base.TransactionID
	and	Previous.ColumnID < Base.ColumnID
	)
	


Update PathwayValidationHistory
set
	UserAdded = Addition.UserName
	,TransactionTime = Addition.TransactionTime
	,UserLastRevised = Revision.UserName
	,RevisionTime = Revision.TransactionTime
from
	RTT.PathwayValidationHistory

left join	
	(
	Select 
		RTTPathwayID
		,OrderID
		,UserName
		,TransactionTime
	from 
		#RTTTransaction Base
	where
		ActionType = 'Add'
	and not exists
		(
		Select
			1
		from
			#RTTTransaction Initial
		where
			Initial.RTTPathwayID = Base.RTTPathwayID
		and Initial.OrderID = Base.OrderID
		and Initial.ActionType = Base.ActionType
		and	Initial.TransactionTime < Base.TransactionTime
		)
	) Addition
on PathwayValidationHistory.RTTPathwayID = Addition.RTTPathwayID
and PathwayValidationHistory.OrderID = Addition.OrderID

left join
	(
	Select 
		RTTPathwayID
		,OrderID
		,UserName
		,TransactionTime
	from 
		#RTTTransaction Base
	where
		ActionType = 'Revise'
	and not exists
		(
		Select
			1
		from
			#RTTTransaction Latest
		where
			Latest.RTTPathwayID = Base.RTTPathwayID
		and Latest.OrderID = Base.OrderID
		and Latest.ActionType = Base.ActionType
		and	Latest.TransactionTime > Base.TransactionTime
		)
	) Revision
on PathwayValidationHistory.RTTPathwayID = Revision.RTTPathwayID
and PathwayValidationHistory.OrderID = Revision.OrderID



-- Weekly Review
Update PathwayValidationHistory
set
	WeekReview = 
				case
					when GynaePathwayEvent.Reportable = 'N' then 
						case
							when SourceofReferralCode = 'CAS' then 13
							when ClinicCode = 'EGUFUP' then 13
							when GynaePathwayEvent.OutcomeCode = 'IVFWL' then 13
							else 14
						end					
					when left(GynaePathwayEvent.RTTPathwayID,2) = 'No' 
						then 1
					when RTTPathwayCondition is null then
						case
							when (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) <56 
							then 19
							else 2
						end
					when left(GynaePathwayEvent.EventType,5) in ('IPAdm','OPApp') 
						and 
							(	RTTPeriodStatus is null
							or
								left(RTTPeriodStatus,2) = '99' 
							)
						then 3
					when GynaePathwayEvent.PathwayStatus = 'Non Active' 
						and
							(	
								RTTPeriodStatus not in ('90A','91A')
							or
								RTTPeriodStatus is null
							)
						and DisposalCode in ('P4TG','P4WW')
						then 4
					when GynaePathwayEvent.EventType = 'OPReferral' 
						and (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) <56 
						then 5
					when GynaePathwayEvent.PathwayStatus = 'Start'
						and GynaePathwayEvent.OutcomeCode = 'IVFWL'
						and GynaePathwayEvent.EventType = 'IPWLAdd'
						and (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) <56
						then 16
					when GynaePathwayEvent.PathwayStatus = 'Stop'
						and RTTWksWaiting <18
						then 17
					when GynaePathwayEvent.PathwayStatus = 'Stop' 
						and GynaePathwayEvent.EventType = 'OPDischarge'
						then 7
					when GynaePathwayEvent.PathwayStatus = 'Stop' 
						and left(RTTPeriodStatus,1) = '3'
						then 6
					when GynaePathwayEvent.PathwayStatus = 'Non Active' 
						and left(RTTPeriodStatus,2) in ('90','91')
						then 8
					when GynaePathwayEvent.PathwayStatus = 'Ticking'
						and (datediff(day,GynaePathwayEvent.EventTime,getdate())/7) < 105
						then 18	
					when GynaePathwayEvent.PathwayStatus in ('Start','Ticking')
						and left(RTTPeriodStatus,1) in ('1','2')
						then 9
					when GynaePathwayEvent.PathwayStatus = 'Start'
						and GynaePathwayEvent.OutcomeCode = 'DNA'
						and RTTPeriodStatus = '33A'
						then 15

					else 10
				end
from 
	RTT.PathwayValidationHistory

inner join RTT.RTT.GynaePathwayEvent
on GynaePathwayEvent.RTTPathwayID = PathwayValidationHistory.RTTPathwayID
and GynaePathwayEvent.OrderID = PathwayValidationHistory.OrderID
and GynaePathwayEvent.EventTime = PathwayValidationHistory.EventTime
and GynaePathwayEvent.EventType = PathwayValidationHistory.EventType
where 
	PathwayValidationHistory.EventDate = cast(dateadd(day,-7,getdate())as date)
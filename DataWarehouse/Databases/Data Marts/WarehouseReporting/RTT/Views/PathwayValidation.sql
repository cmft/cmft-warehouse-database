﻿



--select * from RTT.[PathwayValidation] where RTTPathwayID = 'RW300000001602067        ' order by OrderID

CREATE View [RTT].[PathwayValidation]

as

Select 
	ValidationOutcome.Census
	,NationalSpecialtyCode
	,ValidationOutcome.RTTPathwayID
	,EvRTTPathwayID
	,RTTPathwayCondition
	,DistrictNo
	,ConsultantCode
	,SpecialtyCode
	,SourceofReferralCode
	,ValidationOutcome.EventDate
	,ValidationOutcome.EventTime
	,ValidationOutcome.EventType
	,ApptType
	,ClinicCode
	,ValidationOutcome.OutcomeCode
	,DisposalCode
	,DisposalReason
	,ReportingStatus = left(EventStatus,2)
	,PASRTTStatus = RTTPeriodStatus
	,ValidationOutcome.OrderID
	,ValidationOutcome.PathwayStatus
	,ValidationOutcome.RTTPathwayStartDate
	,ValidationOutcome.RTTWksWaiting
	,Reportable
	,OutcomeID
	,DateValidated
	,UserName
	,Comments
	,NotApplicable
	,TypedTime
	,SignedTime
	,DocumentID
	,UserAdded
	,TransactionTime
	,UserLastRevised
	,RevisionTime
	
from
	RTT.PathwayValidationHistory ValidationOutcome

inner join RTT.RTT.GynaePathwayEvent 
on GynaePathwayEvent.RTTPathwayID = ValidationOutcome.RTTPathwayID 
and GynaePathwayEvent.OrderID = ValidationOutcome.OrderID

inner join RTT.RTT.ValidationOutcome ValidationDescription
on ValidationOutcome.OutcomeID = ValidationDescription.ID










﻿CREATE view [PAS].[Casenote] as

SELECT
	 SourcePatientNo
	,CasenoteNumber
	,AllocatedDate
	,CasenoteLocationCode
	,CasenoteLocationDate
	,CasenoteStatus
	,WithdrawnDate
	,Comment
	,CurrentBorrowerCode
	,LoanTime
	,ExpectedReturnDate
FROM
	warehouse.PAS.Casenote

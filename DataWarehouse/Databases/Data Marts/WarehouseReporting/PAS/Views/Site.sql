﻿CREATE VIEW [PAS].[Site]

AS

SELECT [SiteCode]
      ,[Site]
      ,[Colour]
      ,[Sequence]
      ,[MappedSiteCode]
      ,[MappedSite]
  FROM [WarehouseReporting].[ETL].[BASEPASSite]

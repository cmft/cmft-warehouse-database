﻿

CREATE VIEW [PAS].[Ward] AS

select
	 WardCode = WARDID
	,Ward = WardName
	,SiteCode = HospitalCode
	,LevelOfCareCode = PtGroupintOfCare
	,DeletedFlag = MfRecStsInd
from
	Warehouse.PAS.WardBase



﻿
CREATE VIEW [PAS].[Specialty]

AS

SELECT [SpecialtyCode]
      ,[Specialty]
      ,[NationalSpecialtyCode]
      ,NationalSpecialty
      ,[TreatmentFunctionFlag]
FROM [WarehouseReporting].[ETL].[BASEPASSpecialty]

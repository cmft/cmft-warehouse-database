﻿
create view PAS.[PatientSpecialRegister]

as

select
	[SpecialRegisterRecNo]
	,[SourcePatientNo]
	,[SpecialRegisterCode]
	,[DistrictNo]
	,[NHSNumber]
	,[EnteredDate]
	,[ActivityTime]
	,[SourceEpisodeNo]
	,[Created]
	,[CreatedByWhom]
	,[Updated]
	,[UpdatedByWhom]
from
	[Warehouse].[PAS].[PatientSpecialRegister]
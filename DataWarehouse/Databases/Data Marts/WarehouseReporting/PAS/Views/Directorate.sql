﻿CREATE VIEW [PAS].[Directorate]

AS

SELECT [DirectorateCode]
      ,[Directorate]
      ,[Division]
  FROM [WarehouseReporting].[ETL].[BASEPASDirectorate]
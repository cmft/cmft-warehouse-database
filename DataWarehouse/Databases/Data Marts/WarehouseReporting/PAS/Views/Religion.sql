﻿
create view PAS.Religion

as

select
		ReligionCode = religionbase.[Religion]
		,Religion = religionbase.[Description]
		,ReligionNationalCode = XrefEntityCode

from
	[warehouse].[PAS].[ReligionBase] religionbase
	left outer join Warehouse.dbo.EntityXref xref
	on religionbase.[Religion] = xref.EntityCode
	and EntityTypeCode = 'LOCALRELIGIONCODE'
	and XrefEntityTypeCode = 'NATIONALRELIGIONCODE'
	

﻿CREATE VIEW [PAS].[SourceOfReferral]

AS 
 
SELECT
	   SourceOfReferralCode
      ,SourceOfReferral
      ,NationalSourceOfReferralCode
      ,ReferralType
      ,ReportableFlag
      ,Reportable
      ,NewFlag
      ,New
FROM
	WarehouseOLAP.dbo.OlapPASSourceOfReferral

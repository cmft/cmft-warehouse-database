﻿create view [PAS].[CasenoteLoan] as

SELECT
	 SourceUniqueID
	,SourcePatientNo
	,CasenoteNumber
	,BorrowerCode
	,SequenceNo
	,TransactionTime
	,LoanTime
	,ExpectedReturnDate
	,ReturnTime
	,Comment
	,LoanReason
	,UserId
FROM
	warehouse.PAS.CasenoteLoan

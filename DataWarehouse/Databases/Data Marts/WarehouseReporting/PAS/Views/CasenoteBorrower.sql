﻿CREATE view [PAS].[CasenoteBorrower] as

SELECT
	 BorrowerCode
	,Borrower
	,Surname
	,TitleAndInitials
	,Address1
	,Address2
	,Address3
	,Address4
	,Postcode
	,PhoneNumber
	,DeletedFlag
FROM
	warehouse.PAS.CasenoteBorrower

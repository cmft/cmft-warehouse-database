﻿CREATE VIEW [PAS].[AdmissionMethod]

AS 
 
 
SELECT
	   AdmissionMethodCode
      ,AdmissionMethod
      ,NationalAdmissionMethodCode
      ,NationalAdmissionMethod
      ,AdmissionMethodTypeCode
      ,AdmissionMethodType
FROM
	WarehouseReporting.ETL.BasePASAdmissionMethod

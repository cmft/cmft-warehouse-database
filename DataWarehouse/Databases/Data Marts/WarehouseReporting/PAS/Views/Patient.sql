﻿









CREATE view [PAS].[Patient] as

/****************************************************************************************
	View		: [PAS].[Patient]
	Description	: 

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Initial Coding
	31/07/2014	Paul Egan       Added phone number fields for OP appointment reminders
*****************************************************************************************/

select
	 --ImpCode
	 SourcePatientNo 
	,DateOfBirth  
	,DeathIndicator 
	,DateOfDeath
	,SexCode
	,MaritalStatusCode
	,Surname
	,Forenames
	,Title
	,NextOfKin 
	--,BloodGroup
	--,Rhesus
	--,RiskFactor1
	,EthnicOriginCode 
	--,RiskFactor2
	,ReligionCode
	,PreviousSurname
	,NHSNumber 
	,NHSNumberStatusId
	,DOR 
	,DistrictNo
	,MilitaryNo 
	,AddressLine1
    ,AddressLine2 
    ,AddressLine3 
    ,AddressLine4 
	,Postcode
	--,GPType
	,RegisteredGpCode --INFOCOM RegGP
	--,GDPType
	,RegisteredGdpCode
	,MRSAFlag 
	,MRSADate
	--,PatSchool
	--,PatOccn
	--,Summary1
	--,Summary2
	--,Summary3
	--,BirthName
	--,PlaceOfBirth
	--,CarerRelCd
	--,CPARevDate
	--,CPALastUpDate
	--,CPAPriority
	--,CPARiskCat1
	--,CPARiskCat2
	--,CPARiskCat3
	--,CPARiskCat4
	--,CPARiskCat5
	--,CPAGAFScore
	--,CPACategoryX
	--,CPADependencyX
	--,ExtPmiAddLine1
	--,ExtPmiAddLine2
	--,ExtPmiAddLine3
	--,ExtPmiAddLine4
	--,ExtPmiAddLine5
	--,ExtPmiAddPostcd
	
	/* Added Paul Egan 31/07/2014 */
	,[Modified]
	,[HomePhone]
	,[MobilePhone]
	,[WorkPhone]
	,[RegistrationDate]
	,AEAttendanceToday
	,Updated
	
from
	Warehouse.PAS.Patient











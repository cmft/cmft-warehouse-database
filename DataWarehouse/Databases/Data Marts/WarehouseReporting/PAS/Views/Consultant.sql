﻿
CREATE VIEW [PAS].[Consultant]

AS 
 
SELECT
		   ConsultantCode
		  ,Consultant
		  ,NationalConsultantCode
		  ,SpecialtyCode
		  ,Specialty
		  ,DomainLogin
FROM
	WarehouseReporting.[ETL].[BASEPASConsultant]


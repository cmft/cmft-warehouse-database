﻿
create view [PAS].[EthnicOrigin] as
SELECT [EthnicOriginCode]
      ,[EthnicOrigin]
      ,[NationalEthnicOriginCode]
      ,[NationalEthnicOrigin]
      ,[DeletedFlag]
  FROM [WarehouseOLAP].[dbo].[OlapPASEthnicOrigin]


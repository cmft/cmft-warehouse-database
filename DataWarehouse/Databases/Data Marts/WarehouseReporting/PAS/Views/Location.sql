﻿




CREATE VIEW [PAS].[Location]

AS

/* 
==============================================================================================
Description: 

When		Who			What
06/03/2015	Paul Egan	Initial Coding - for appointment reminders
						NB Need to ask Colin/Darren about view [Warehouse].[PAS].[Location] (DEV only 
						at the moment) as this maps to 	general location and is probably a 
						better solution than this standalone view?
===============================================================================================
*/

select 
	LocationCode
	,Location = Location1
from 
	warehouse.PAS.LocationBase




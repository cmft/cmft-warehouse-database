﻿

create view [PAS].[SpecialRegister]

as

select
	SpecialRegisterCode
	,SpecialRegister
from
	Warehouse.PAS.SpecialRegisterBase

﻿
create view PAS.Gp

as
select
	GpCode
	,Gp
	,PracticeCode
	,NationalCode
from
	Warehouse.PAS.Gp
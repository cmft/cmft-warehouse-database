﻿create view [PAS].[CasenoteLocation] as

SELECT
	 CasenoteLocationCode
	,CasenoteLocation
	,DeletedFlag
FROM
	warehouse.PAS.CasenoteLocation

﻿


CREATE VIEW [PAS].[Clinic]

AS

/* 
==============================================================================================
Description: 

When		Who			What
?			?			Initial Coding
06/03/2015	Paul Egan	Added ReportToLocationCode for appointment reminders
===============================================================================================
*/

SELECT 
 	 ClinicCode
	,Comment
	,ConsultantCode
	,Clinic
	,SiteCode
	,FunctionCode
	,DeletedFlag
	,ProviderCode
	,ServiceGroupCode
	,SpecialtyCode
	,VirtualClinicFlag
	,POAClinicFlag
	,ReportToLocationCode
FROM 
	warehouse.PAS.Clinic


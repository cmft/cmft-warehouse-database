﻿CREATE TABLE [dbo].[AKIRequest2] (
    [ResultRecno]                  INT              NULL,
    [CasenoteNumber]               VARCHAR (20)     NULL,
    [DistrictNo]                   VARCHAR (50)     NULL,
    [ResultTime]                   SMALLDATETIME    NULL,
    [SequenceNoSpell]              BIGINT           NULL,
    [SequenceNoPatient]            BIGINT           NULL,
    [RenalPatient]                 BIT              NULL,
    [Result]                       INT              NULL,
    [BaselineResult]               INT              NULL,
    [CurrentToBaselineResultRatio] FLOAT (53)       NULL,
    [LocalAKI]                     VARCHAR (13)     NOT NULL,
    [RatioDescription]             VARCHAR (15)     NOT NULL,
    [ResultForRatio]               INT              NULL,
    [RVRatio]                      DECIMAL (31, 19) NULL,
    [NationalAlert]                VARCHAR (17)     NULL
);


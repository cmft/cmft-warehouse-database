﻿CREATE TABLE [dbo].[RRAKIQueryResults] (
    [ReportingMonth]     VARCHAR (6)   NOT NULL,
    [PatientForename]    VARCHAR (80)  NULL,
    [PatientSurname]     VARCHAR (80)  NULL,
    [DistrictNo]         VARCHAR (50)  NULL,
    [CasenoteNumber]     VARCHAR (20)  NULL,
    [AgeCode]            VARCHAR (50)  NULL,
    [Sex]                VARCHAR (1)   NULL,
    [ResultTime]         SMALLDATETIME NULL,
    [ResultDate]         DATE          NULL,
    [Result]             INT           NULL,
    [PreviousResultTime] SMALLDATETIME NULL,
    [PreviousResult]     INT           NULL,
    [BaselineResultTime] SMALLDATETIME NULL,
    [BaselineResult]     INT           NULL
);


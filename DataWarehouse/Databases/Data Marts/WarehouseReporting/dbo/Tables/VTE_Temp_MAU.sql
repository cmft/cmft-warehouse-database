﻿CREATE TABLE [dbo].[VTE_Temp_MAU] (
    [AdmissionNumber] VARCHAR (20)  NOT NULL,
    [EPMINumber]      VARCHAR (20)  NULL,
    [AdmittedOn]      DATETIME      NULL,
    [DischargedOn]    DATETIME      NULL,
    [ConSurname]      VARCHAR (100) NULL,
    [admittingward]   VARCHAR (10)  NULL,
    [VTE completed]   VARCHAR (20)  NULL,
    CONSTRAINT [PK_VTE_Temp_MAU] PRIMARY KEY NONCLUSTERED ([AdmissionNumber] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_VTE_Temp_MAU]
    ON [dbo].[VTE_Temp_MAU]([AdmissionNumber] ASC);


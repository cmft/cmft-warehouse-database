﻿CREATE TABLE [dbo].[ClinicAttendanceStatisticsClinicGroups] (
    [ReportGroup]    VARCHAR (50)   NULL,
    [ClinicGroup]    NVARCHAR (255) NULL,
    [ClinicCode]     NVARCHAR (255) NULL,
    [SessionCode]    VARCHAR (8)    NOT NULL,
    [AMPM]           NCHAR (2)      NULL,
    [AvailableSlots] INT            NULL,
    [LDF]            VARCHAR (1)    NOT NULL
);


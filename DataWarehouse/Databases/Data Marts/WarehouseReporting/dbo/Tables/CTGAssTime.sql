﻿CREATE TABLE [dbo].[CTGAssTime] (
    [SessionNumber]          UNIQUEIDENTIFIER NOT NULL,
    [FetusNumber]            INT              NULL,
    [CTGStartDate]           DATE             NULL,
    [TimeBetweenAssessments] INT              NULL,
    [MonitoringType]         VARCHAR (6)      NOT NULL,
    [Sequence]               BIGINT           NULL
);


﻿

create function [dbo].[f_Last_of_Month] (
	@date datetime
)
returns datetime
as
begin
	if @date is null return null;
	
	declare @end datetime
	set @end = convert(datetime, convert(char(10), dateadd(day, -1, dateadd(month, 1, dateadd(day, -(datepart(day, @date)-1), @date))), 103), 103)
	return(@end)
end

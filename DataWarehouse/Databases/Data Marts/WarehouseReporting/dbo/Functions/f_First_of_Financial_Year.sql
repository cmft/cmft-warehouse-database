﻿

create function [dbo].[f_First_of_Financial_Year] (
	@date datetime
)
returns datetime
as
begin
	if @date is null return null;
	
	declare @first datetime
	declare @year int
	if(datepart(month, @date) < 4)
	begin
		set @year = datepart(year, @date) - 1
	end
	else
	begin
		set @year = datepart(year, @date)
	end
	set @first = convert(datetime, '01/04/' + convert(varchar(4), @year), 103)
	return(@first)
end


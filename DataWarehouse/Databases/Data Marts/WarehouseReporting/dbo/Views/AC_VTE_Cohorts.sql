﻿




CREATE VIEW [dbo].[AC_VTE_Cohorts] as

SELECT AdmissionNumber			
      ,VTEStatus			
      ,AdmittedOn			
      ,DischargedOn			
      ,EPMINumber			
      ,FamilyName			
      ,GivenName			
      ,BirthDate			
      ,ConSurname			
      ,ConFirstName			
      ,ConsultantCode			
      ,ProcedureCode			
      ,DiagnosticCode			
      ,AdmissionMethod			
      ,PatClass			
      ,admittingward			
      ,ConUnique			
      ,[Age on Admit]			
      ,InitialVTEDate			
      ,Complete			
      ,Incomplete			
      ,CompleteWithin8Hours			
      ,CompleteAfter8Hours			
      ,IncompleteWithin8Hours			
      ,IncompleteAfter8Hours		
      ,WardName
      ,NationalSpecialty bedmanSpecialty
      ,NationalSpecialtyCode bedmanSpecialtyCode
      ,operation	
      ,operationchapter
      ,diagnosis
      ,diagnosischapter
	  ,case		
			when admittingward in ('EUN','W8','PARK') 
or 			
ConsultantCode like 'MSS' 			
then 'Cohort' end 'cohort for Manual signoff'			
	  ,case		
			when left(ProcedureCode,3) in ('M42','M43','M44','M45','K75','A12','A18','E24','E25','E34','E35'
,'E36','E48','E49','E50','E51','E62','E63','G14','G15','G16','G17','G18','G19','G42','G44','G46','G45','G54'			
,'G55','G79','G80','H20','H21','H22','H23','H24','H25','H26','H27','H28','H68','H69','H70','J08','J09','J09'			
,'J38','J39','J40','J42','J43','M09','X70','X73','X28','X29','X32','X33','X34','A52','A81','M42','M43','M44'			
,'M45','Q17','Q18','E48','E49','E50','E51','Q13','C11','C12','C18','C13','C14','C16','C73') 			
OR			
ProcedureCode IN ('C27.5','H40.1','J48.2','M27.5','M29.3','M68.2','M76.7','X41.2','A84.7','X35.2','X37.3'			
,'X38.4','J09.1','J29.2','J13.1','J13.2','J14.1','M11.1','M13.1','A55.9','W36.4','W36.5','P27.3','Q55.4'			
,'E59.1','E59.2','E59.3','C12.1','C43.2','C29.3','C27.3','C15.1','C15.2','C27.1','C27.4','C11.8','S01.4'			
,'S01.5','S01.6','C18.5','C12.1','C12.5','C22.8','C65.1','C66.4','C75.1','C75.1','C71.1','C75.2','C60.1'			
,'C39.1','C79.4','C79.2','C79.7','C10.8','C40.2','C45.3','C51.2','C47.3','C46.6','C46.3','C60.1','C69.8'			
,'C22.6','C49.3','C79.1','L67.1','X85.1','Q13.1','X89.2','L91.3','G34.5','U29.3','X50.1','X92.1','U29.7'			
,'U25.4','X36.2','H52.4','G21.1','W84.6','V21.8','H44.4','H56.8','S53.2','S06.9','A65.1','M47.3')			
then 'Cohort' end as Proc_Cohort			
,case 			
		when ProcedureCode IN('A73.5'	
,'D02.1','D03.4','D06.1','D07.8','D14.1','D15.1','D15.3','D28.1','E03.6','E06.1','E07.3','E08.1','E09.1'			
,'E14.2','E37.1','F09.1','F09.4','F09.5','F20.1','F24.1','F28.1','F34.1','F36.8','F38.1','M47.4','N28.4'			
,'P03.2','P05.4','P09.1','Q01.3','Q02.3','Q12.1','Q12.2','Q16.4','Q35.2','Q41.3','S06.5','S06.6','S06.7'			
,'S06.8','S47.1','S47.2','S68.2','S70.1','T31.3','T87.2','V09.2','V54.4','W90.3','X59.8') and PatClass in('2','3')			
then 'Cohort' end 'Low Risk'			
			
,case when [DiagnosticCode] in ('M79.6',
'M79.66',
'M79.86',
'M79.87',
'I26.9') then 'Cohort' 
when AdmitanceType = 'DVT' then 'Cohort'
end 'DVT'
----------------------------------------------------------------------------------------------------------			
	  ,case		
			when admittingward in ('EUN','W8','PARK') 
or 			
ConsultantCode like 'MSS' 			
or			
			left(ProcedureCode,3) in ('M42','M43','M44','M45','K75','A12','A18','E24','E25','E34','E35'
,'E36','E48','E49','E50','E51','E62','E63','G14','G15','G16','G17','G18','G19','G42','G44','G46','G45','G54'			
,'G55','G79','G80','H20','H21','H22','H23','H24','H25','H26','H27','H28','H68','H69','H70','J08','J09','J09'			
,'J38','J39','J40','J42','J43','M09','X70','X73','X28','X29','X32','X33','X34','A52','A81','M42','M43','M44'			
,'M45','Q17','Q18','E48','E49','E50','E51','Q13','C11','C12','C18','C13','C14','C16','C73') 			
OR			
ProcedureCode IN ('C27.5','H40.1','J48.2','M27.5','M29.3','M68.2','M76.7','X41.2','A84.7','X35.2','X37.3'			
,'X38.4','J09.1','J29.2','J13.1','J13.2','J14.1','M11.1','M13.1','A55.9','W36.4','W36.5','P27.3','Q55.4'			
,'E59.1','E59.2','E59.3','C12.1','C43.2','C29.3','C27.3','C15.1','C15.2','C27.1','C27.4','C11.8','S01.4'			
,'S01.5','S01.6','C18.5','C12.1','C12.5','C22.8','C65.1','C66.4','C75.1','C75.1','C71.1','C75.2','C60.1'			
,'C39.1','C79.4','C79.2','C79.7','C10.8','C40.2','C45.3','C51.2','C47.3','C46.6','C46.3','C60.1','C69.8'			
,'C22.6','C49.3','C79.1','L67.1','X85.1','Q13.1','X89.2','L91.3','G34.5','U29.3','X50.1','X92.1','U29.7'			
,'U25.4','X36.2','H52.4','G21.1','W84.6','V21.8','H44.4','H56.8','S53.2','S06.9','A65.1','M47.3')	and PatClass in('2','3')		
or [DiagnosticCode] in ('M79.6',
'M79.66',
'M79.86',
'M79.87',
'I26.9')
or AdmitanceType = 'DVT' 
--- low risk ------
or ( ProcedureCode IN('A73.5'	
,'D02.1','D03.4','D06.1','D07.8','D14.1','D15.1','D15.3','D28.1','E03.6','E06.1','E07.3','E08.1','E09.1'			
,'E14.2','E37.1','F09.1','F09.4','F09.5','F20.1','F24.1','F28.1','F34.1','F36.8','F38.1','M47.4','N28.4'			
,'P03.2','P05.4','P09.1','Q01.3','Q02.3','Q12.1','Q12.2','Q16.4','Q35.2','Q41.3','S06.5','S06.6','S06.7'			
,'S06.8','S47.1','S47.2','S68.2','S70.1','T31.3','T87.2','V09.2','V54.4','W90.3','X59.8') and PatClass in('2','3')	
)
--- low risk ------
then 'Cohort' end 'All_Cohorts'			
			
			
			
			
FROM TGHInformation.dbo.AC_VTE_Monthly			

LEFT JOIN WarehouseReportingMerged.WH.Specialty ON SUBSTRING(bedmanSpecialtyCode,1,4) = SourceSpecialtyCode AND SourceContext = 'Ultragenda (Trafford)'






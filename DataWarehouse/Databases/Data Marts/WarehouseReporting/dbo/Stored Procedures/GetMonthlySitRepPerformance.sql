﻿
create proc GetMonthlySitRepPerformance

as

select
      [Site]
      --,[EffectiveDate]
      --,[AEType1Attendances]
      --,[Type1Breaches4hour]
      --,[Type1DTAtoAdmission4to12hours]
      --,[Type1EmergencyAdmissionsViaAE]
      --,[DirectEmergencyAdmissions]
      ,[MedicalOutliersAtMidnight]
      --,[CancellationsElectiveOps]
      ,[CancellationsUrgentElectiveOps]
      ,[CancellationsUrgentOpsfor2ndPlusTime]
      --,[ElectiveOrdinaryAdmissions]
      --,[DischargesOfEmergencyAdmissions]
      ,[AcuteBedsOpenAtMidnight]
      ,[AcuteBedsOccupiedAtMidnight]
      ,[MedicalBedsAtMidnight]
      --,[AdultCriticalCareOpenAtMidnight]
      --,[AdultCriticalCareOccupiedAtMidnight]
      --,[DelayedTransfersOfCare]
      --,[WardClosures]
      ,[NonMedicalCCTransfers]
      ,[NonMedicalCCTransfers_OO_ApprovedCCXferGroup]
      ,[Pressures]
      ,[IssuesWithStaff]
      ,[WardClosureComments]
      ,[CriticalCareIssues]
      ,[OtherComments]
      ,[ts]
      ,[isActive]
from
	[Information].[Collections].[SitRepDailyDataCollection]
	

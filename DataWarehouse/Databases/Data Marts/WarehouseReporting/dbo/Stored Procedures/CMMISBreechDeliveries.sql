﻿






create PROCEDURE [dbo].[CMMISBreechDeliveries] 

  @FromDate    Date
 ,@ToDate      Date

AS  


--DECLARE @FromDate AS DATE        = '14-FEB-2014'
--DECLARE @ToDate   AS DATE        = '16-AUG-2014'



SELECT 
 MotherReg.[M_NUMBER]                                              AS MaternalCasenote
,MotherReg.[CURRENT_SURNAME]                                       AS MaternalSurname
,MotherReg.[F_FORENAMES]                                           AS MaternalForenames
,CONVERT(varchar,MotherReg.[M_DOB],103)                            AS MaternalDoB
,InfantReg.[I_NUMBER]
,CONVERT(varchar,InfantReg.[DATE_OF_BIRTH],103)                    AS InfantDoB
,BirthReg.[OCCURRENCE]
,BirthReg.[BIRTH_ORDER]
,ISNULL(ListIndForCaesarean.[DESCRIPTION],'')                      AS ListIndForBreech
,ISNULL(BirthReg.[BREECH_DIAGNOSIS],'')                            AS BreechDiagnosis
,BirthReg.[OUTCOME]
,ListDeliveryOutcome.[DESCRIPTION]                                 AS DeliveryOutcome
,BirthReg.[METHOD_DELIVERY]                                        AS DeliveryCode
,ListIndMethodDelivery.[Description]                               AS DeliveryMethod   
,ISNULL(BirthReg.[COMMENTS],'')                                    AS DeliveryComments  
,ISNULL(BirthReg.[DELIVERY_TEAM],'')                               AS DeliveryTeam
,ISNULL(MidwifeTeam.[TEAM],'')                                     AS MidwifeTeam
,ISNULL(BirthReg.[DEL_HOSP_NAME],'')                               AS DeliveryHospitalName                                         
      
FROM  [CMISStaging].[SMMIS].[INFANT_REG]               AS InfantReg

LEFT OUTER JOIN  [CMISStaging].[SMMIS].[BIRTH_REG]     AS BirthReg
  ON InfantReg.[I_NUMBER] = BirthReg.[I_NUMBER]
LEFT OUTER JOIN  [CMISStaging].[SMMIS].[MOTHER_REG]    AS MotherReg
  ON MotherReg.[M_NUMBER] = BirthReg.[M_NUMBER]
  
LEFT OUTER JOIN  [CMISStaging].[SMMIS].[MIDWIFE_TEAM]  AS MidwifeTeam 
  ON BirthReg.[DELIVERY_TEAM] = MidwifeTeam.[TEAM_INITIAL]
  
LEFT OUTER JOIN  [CMISStaging].[SMMIS].[LIST_OPTIONS]  AS ListDeliveryOutcome
  ON BirthReg.[OUTCOME] = ListDeliveryOutcome.[KEY_CHAR]
  AND ListDeliveryOutcome.[TABLE_NAME]  = 'BIRTH_REG'
  AND ListDeliveryOutcome.[COLUMN_NAME] = 'OUTCOME' 
LEFT OUTER JOIN  [CMISStaging].[SMMIS].[LIST_OPTIONS]  AS ListIndMethodDelivery
  ON  BirthReg.[METHOD_DELIVERY] = ListIndMethodDelivery.[KEY_CHAR] 
  AND ListDeliveryOutcome.[TABLE_NAME]    = 'BIRTH_REG'
  AND ListIndMethodDelivery.[COLUMN_NAME] = 'METHOD_DELIVERY'
LEFT OUTER JOIN  [CMISStaging].[SMMIS].[LIST_OPTIONS]  AS ListIndForCaesarean
  ON ASCII(BirthReg.[INDIC_CAESAREAN]) = ASCII(ListIndForCaesarean.[KEY_CHAR])
  AND ListIndForCaesarean.[COLUMN_NAME] = 'INDIC_CAESAREAN' 
 
 
WHERE InfantReg.[DATE_OF_BIRTH] BETWEEN @FromDate AND @ToDate
  AND (
        BirthReg.[METHOD_DELIVERY] IN('A','B')
   OR BirthReg.[INDIC_CAESAREAN]    = 'B'
      )
  AND MotherReg.[CURRENT_SURNAME]   <> 'TESTTEST'

ORDER BY InfantReg.[DATE_OF_BIRTH], MotherReg.[M_NUMBER]



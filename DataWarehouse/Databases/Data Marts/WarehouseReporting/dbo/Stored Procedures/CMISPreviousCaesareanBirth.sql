﻿




CREATE PROCEDURE [dbo].[CMISPreviousCaesareanBirth] 

  @FromDate    Date
 ,@ToDate      Date

AS  


--DECLARE @FromDate AS DATE        = '14-FEB-2013'
--DECLARE @ToDate   AS DATE        = '14-FEB-2014'
SELECT 
 CONVERT(varchar,IRCaesarean.[DATE_OF_BIRTH],103) AS InfantDoB
,BRCaesarean.[OCCURRENCE]
,BRCaesarean.[M_NUMBER]                           AS MaternalCasenote
,ISNULL(NICaesarean.[NHS_NUMBER],'')              AS MaternalNHS
,MRCaesarean.[F_FORENAMES]
,MRCaesarean.[CURRENT_SURNAME]
,CONVERT(varchar,MRCaesarean.[M_DOB],103)         AS MaternalDoB
,BRCaesarean.I_Number                             AS CaesareanINumber
,OPTCaesarean.[DESCRIPTION]                       AS DeliveryType
,ListDeliveryOutcomeCaesarean.[DESCRIPTION]       AS DeliveryOutcome

,CONVERT(varchar,IRNormal.[DATE_OF_BIRTH],103)    AS InfantDoBFollowing
,BRNormal.[OCCURRENCE]                            AS OCCURRENCEFollowing
,BRNormal.[M_NUMBER]                              AS MaternalCasenoteFollowing
,BRNormal.[I_NUMBER]                              AS CurrentINumber
,OPTNormal.[DESCRIPTION]                          AS DeliveryTypeFollowing
,ListDeliveryOutcomeNormal.[DESCRIPTION]          AS DeliveryOutcomeFollowing


FROM [CMISStaging].SMMIS.BIRTH_REG                        AS BRCaesarean

INNER JOIN  [CMISStaging].[SMMIS].[INFANT_REG]             AS IRCaesarean
   ON BRCaesarean.I_Number = IRCaesarean. I_Number
   
INNER JOIN  [CMISStaging].[SMMIS].[MOTHER_REG]             AS MRCaesarean
   ON BRCaesarean.[M_NUMBER] = MRCaesarean.[M_NUMBER] 
 
LEFT OUTER JOIN   [CMISStaging].[SMMIS].[NUMBER_INDEX]     AS NICaesarean
  ON MRCaesarean.[M_NUMBER] = NICaesarean.[HOSP_NUMBER]

LEFT OUTER JOIN  [CMISStaging].[SMMIS].[LIST_OPTIONS]      AS OPTCaesarean
  ON OPTCaesarean.[COLUMN_NAME] = 'METHOD_DELIVERY' 
  AND OPTCaesarean.[TABLE_NAME] = 'BIRTH_REG'
  AND BRCaesarean.[METHOD_DELIVERY] = OPTCaesarean.[KEY_CHAR] 
  
LEFT OUTER JOIN  [CMISStaging].[SMMIS].[LIST_OPTIONS]      AS ListDeliveryOutcomeCaesarean
  ON BRCaesarean.[OUTCOME] = ListDeliveryOutcomeCaesarean.[KEY_CHAR]
  AND ListDeliveryOutcomeCaesarean.[TABLE_NAME]  = 'BIRTH_REG'
  AND ListDeliveryOutcomeCaesarean.[COLUMN_NAME] = 'OUTCOME' 
  
LEFT OUTER JOIN
 (
    [CMISStaging].SMMIS.BIRTH_REG                        AS BRNormal

   INNER JOIN  [CMISStaging].[SMMIS].[INFANT_REG]        AS IRNormal
      ON BRNormal.I_Number = IRNormal. I_Number
 
   LEFT OUTER JOIN  [CMISStaging].[SMMIS].[LIST_OPTIONS] AS OPTNormal
     ON OPTNormal.[COLUMN_NAME] = 'METHOD_DELIVERY' 
     AND OPTNormal.[TABLE_NAME] = 'BIRTH_REG'
     AND BRNormal.[METHOD_DELIVERY] = OPTNormal.[KEY_CHAR] 
 
   LEFT OUTER JOIN  [CMISStaging].[SMMIS].[LIST_OPTIONS] AS ListDeliveryOutcomeNormal
     ON BRNormal.[OUTCOME] = ListDeliveryOutcomeNormal.[KEY_CHAR]
    AND ListDeliveryOutcomeNormal.[TABLE_NAME]  = 'BIRTH_REG'
    AND ListDeliveryOutcomeNormal.[COLUMN_NAME] = 'OUTCOME' 
  ) 
  
 ON  
      BRCaesarean.[M_NUMBER]      = BRNormal.[M_NUMBER]
  AND BRCaesarean.[OCCURRENCE]    < BRNormal.[OCCURRENCE] 
  AND BRNormal.[METHOD_DELIVERY]  NOT IN('C','D','E','U')  
  
WHERE IRNormal.[DATE_OF_BIRTH]   BETWEEN @FromDate AND @ToDate
  AND BRCaesarean.[METHOD_DELIVERY] IN('C','D','E','U')
  --AND IRNormal.[DATE_OF_BIRTH]      IS NOT NULL
  
ORDER BY IRCaesarean.[DATE_OF_BIRTH],MRCaesarean.[CURRENT_SURNAME],MRCaesarean.[F_FORENAMES],BRCaesarean.[BIRTH_ORDER]




﻿
CREATE PROCEDURE [dbo].[CMMISExpectedDeliveryDate] 

  @FromDate    Date
 ,@ToDate      Date

AS  


--DECLARE @FromDate AS DATE        = '14-JUN-2014'
--DECLARE @ToDate   AS DATE        = '16-JUN-2014'

SELECT
   PREGNANCY.[M_NUMBER]                         AS Casenote
  ,PREGNANCY.[OCCURRENCE]
  ,MOTHER_REG.[CURRENT_SURNAME]
  ,MOTHER_REG.[F_FORENAMES]
  ,CONVERT(varchar,MOTHER_REG.[M_DOB],103)          AS MaternalDoB
  ,CONVERT(varchar,PREGNANCY.[REFERRAL_DATE],103)   AS ReferralDate
  ,CONVERT(varchar,PREGNANCY.[LMP_AT_REFERRAL],103) AS LMPReferral 
  ,CONVERT(varchar,PREGNANCY.[EDD_AT_REFERRAL],103) AS EstDeleiveryDateReferral
  ,PREGNANCY.[REFERRAL_GEST_WKS]
  ,PREGNANCY.[REFERRAL_GEST_DAYS] 
  ,BookingType.[Description]                        AS BookingType
  ,MaritalStatus.[Description]                      AS MarriageStatus
  ,MIDWIFE_TEAM.[TEAM]                              AS MidwifeTeam
  ,PREGNANCY.[CON_CODE]
  ,PREGNANCY.[STATUS]
  ,PREGNANCY.[TYPE]

FROM warehousesql.[CMISStaging].[SMMIS].[PREGNANCY]

LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[MOTHER_REG]
   ON PREGNANCY.[M_NUMBER] = MOTHER_REG.[M_NUMBER]
  AND PREGNANCY.[OCCURRENCE] = MOTHER_REG.[CURRENT_OCCURRENCE] 
  
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[MIDWIFE_TEAM]  
  ON PREGNANCY.[MIDWIFE_TEAM] = MIDWIFE_TEAM.[TEAM_INITIAL]
  
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[LIST_OPTIONS]  AS BookingType
  ON ASCII(PREGNANCY.[UNBOOKED]) = ASCII(BookingType.[KEY_CHAR])
  AND BookingType.[TABLE_NAME]  = 'PREGNANCY' 
  AND BookingType.[COLUMN_NAME] = 'UNBOOKED' 
  
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[LIST_OPTIONS]  AS MaritalStatus
  ON ASCII(PREGNANCY.[MARITAL]) = ASCII(MaritalStatus.[KEY_CHAR])
  AND MaritalStatus.[TABLE_NAME]  = 'PREGNANCY' 
  AND MaritalStatus.[COLUMN_NAME] = 'MARITAL'   

  
WHERE PREGNANCY.[EDD_AT_REFERRAL] BETWEEN @FromDate AND @ToDate
  
ORDER BY PREGNANCY.[EDD_AT_REFERRAL]





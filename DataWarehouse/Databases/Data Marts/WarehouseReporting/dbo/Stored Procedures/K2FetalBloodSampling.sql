﻿







create PROCEDURE [dbo].[K2FetalBloodSampling] 

  @FromDate    Date
 ,@ToDate      Date

AS  


--DECLARE @FromDate AS DATE        = '14-FEB-2014'
--DECLARE @ToDate   AS DATE        = '16-AUG-2014'

SELECT 
   Patient.[NHS_Number]
  ,Encounter.[CasenoteNumber]
  ,Patient.[Surname]
  ,Patient.[FirstName]
  ,CONVERT(varchar,Patient.[DOB],103)        AS DoB
  ,Patient.[HospitalPatientID]
  ,Encounter.[AdmissionDate]
  ,Encounter.[AdmissionTime]
  ,max(FetalBloodProcedure.[TimeEntered])    AS MaxFBPDate
  ,Encounter.[DischargeTime]
  --,FetalBloodProcedure.[NumOfSamples]
  --,FetalBloodProcedure.[sample1_ID]
  --,FetalBloodProcedure.[LowestpH]
  ,Patient.[PatientID]
  ,PatientPregnancy.[CaseNumber]
  --,SessionAssignment.[SessionNumber]

FROM [K2].[dbo].[Patient]

INNER JOIN  [K2].[dbo].[PatientPregnancy]
   ON  Patient.[PatientID] = PatientPregnancy.[PatientID] 
   
INNER JOIN [K2].[dbo].[SessionAssignment]
   ON PatientPregnancy.[CaseNumber] = SessionAssignment.[CaseNumber]
   
INNER JOIN [K2].[dbo].[FetalBloodProcedure]   
   ON SessionAssignment.[SessionNumber] = FetalBloodProcedure.[SessionNumber]
 
LEFT OUTER JOIN [WarehouseReportingMerged].[APC].[Encounter]
   ON Patient.[HospitalPatientID] = Encounter.[DistrictNo]

WHERE FetalBloodProcedure.[TimeEntered] BETWEEN @FromDate AND @ToDate
  AND Patient.[Surname] <> 'Test'

GROUP BY
   Patient.[NHS_Number]
  ,Encounter.[CasenoteNumber]
  ,Patient.[Surname]
  ,Patient.[FirstName]
  ,Patient.[DOB]
  ,Patient.[HospitalPatientID]
  ,Encounter.[AdmissionDate]
  ,Encounter.[AdmissionTime]
  ,Encounter.[DischargeTime]
  --,FetalBloodProcedure.[NumOfSamples]
  --,FetalBloodProcedure.[sample1_ID]
  --,FetalBloodProcedure.[LowestpH]
  ,Patient.[PatientID]
  ,PatientPregnancy.[CaseNumber]
  
HAVING max(FetalBloodProcedure.[TimeEntered]) BETWEEN Encounter.[AdmissionTime] AND Encounter.[DischargeTime]  

ORDER BY Patient.[HospitalPatientID], Patient.[NHS_Number]


﻿



create PROCEDURE [dbo].[COPDHomeVisits] 

  @FromDate    Date
 ,@ToDate      Date

AS  


--DECLARE @FromDate AS DATE  = '14-Apr-2014'
--DECLARE @ToDate   AS DATE  = '20-Apr-2014'

SELECT 
  Encounter.[SpecialtyCode]
 ,Encounter.[ClinicCode]
 ,Clinic.[SourceClinic]                             AS ClinicDescription
 ,Diary.[SessionCode]
 ,Diary.[SessionStartTime]
 ,Encounter.[AppointmentTime]
 ,Diary.[SessionEndTime]
 ,Diary.[SessionPeriod]
 ,Encounter.[DoctorCode]
 ,CONVERT(varchar,Encounter.[AppointmentDate],103)  AS AppointmentDate
 ,Encounter.[PatientForename]
 ,Encounter.[PatientSurname]
 ,CONVERT(varchar,Encounter.[DateOfBirth],103)      AS DoB
 ,Encounter.[CasenoteNo]
 ,ISNULL(Encounter.[NHSNumber],'')                  AS NHSNumber
 ,ISNULL(Encounter.[PatientAddress1],'')            AS Address1
 ,ISNULL(Encounter.[PatientAddress2],'')            AS Address2
 ,ISNULL(Encounter.[PatientAddress3],'')            AS Address3
 ,ISNULL(Encounter.[PatientAddress4],'')            AS Address4
 ,Encounter.[Postcode]
 ,AttendanceStatus.[NationalAttendanceStatus]
 ,AppointmentType.[LocalAppointmentType]
 ,Encounter.[SourcePatientNo]
 ,Encounter.[SourceEncounterNo]
      
FROM warehousesql.[WarehouseReportingMerged].[OP].[Encounter]

LEFT OUTER JOIN warehousesql.[WarehouseReportingMerged].[OP].[AttendanceStatus]
  ON Encounter.[AttendanceStatusID] = AttendanceStatus.[SourceAttendanceStatusID]
  
LEFT OUTER JOIN warehousesql.[WarehouseReportingMerged].[OP].[AppointmentType]
  ON Encounter.[AppointmentTypeId] = AppointmentType.[SourceAppointmentTypeID]
  
LEFT OUTER JOIN warehousesql.[WarehouseReportingMerged].[OP].[Clinic]
  ON Encounter.[ClinicID] = Clinic.[SourceClinicID]
  
LEFT OUTER JOIN warehousesql.[Warehouse].[OP].[Diary]  
  ON Encounter.[ClinicCode] = Diary.[ClinicCode]
 AND Encounter.[AppointmentTime] BETWEEN Diary.[SessionStartTime] AND Diary.[SessionEndTime]
  
  
WHERE Encounter.[SpecialtyCode] = 'COPD'
  AND Encounter.[ClinicCode]    = 'HOMEV'
  AND Encounter.[AppointmentDate] BETWEEN @FromDate AND @ToDate
  
ORDER BY Encounter.[AppointmentTime]



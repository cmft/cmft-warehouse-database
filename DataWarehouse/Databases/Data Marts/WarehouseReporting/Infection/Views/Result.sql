﻿
create view [Infection].[Result]

as

select
	[ResultRecno]
	,[AlertTypeCode]
	,[TestCategoryID]
	,[Positive]
	,[SpecimenNumber]
	,[PatientName]
	,[DistrictNo]
	,[NHSNumber]
	,[SexCode]
	,[DateOfBirth]
	,[SpecimenCollectionDate]
	,[SpecimenCollectionTime]
	,[PreviousPositiveDate]
	,[OrganismCode]
	,[SampleTypeCode]
	,[SampleSubTypeCode]
	,[SampleQualifierCode]
	,[AuthorisationDate]
	,[ExtractFileName]
	,[InterfaceCode]
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[Warehouse].[Infection].[Result]
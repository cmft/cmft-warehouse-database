﻿

create proc [RPT].[VTEBreachWatch]
(
@StartDate date
,@EndDate date
,@WardPASCode varchar(max)
)

as

/****************************************************************************************
 Stored procedure : RPT.VTEBreachWatch
 Description   : SSRS proc for the Visual Studio report
 
 Reports >> VTE >> VTEBreachWatch
 
 Measures
  Current inpatients (Bedman so Central only) without a VTE assessment

 Modification History
 ====================
 
 Date  Person   Description
 ====================================================================================
 19/02/2014 Paul Egan  Intial Coding
*****************************************************************************************/

/*============ Debug only =================*/
--Declare @StartDate date = '20 Feb 2014';
--Declare @EndDate date = '21 Feb 2014';
------------Declare @Site varchar(50) = 'DENT, MRI, NRMC, REH, SMH';  -- Decided not to use.
--Declare @WardPASCode varchar(20) = 'IH, ENDO, SMDU, SAL, 44, WCH, IHW, DC, PIU';
/*=========================================*/


/* Get all spells in period */
with RawData as
(
select
 BedManSpell.SpellID
 ,CaseNoteNo = BedManSpell.HospitalNo
 ,BedManSpell.Patient
 ,PatientInstance = -- Filter by 1 for latest spell 
  row_number() over(partition by BedManSpell.Patient order by BedManSpell.AdmitDate desc)
 ,PatientAge = WarehouseReportingMerged.dbo.f_GetAge(BedManPatient.DoB, BedManSpell.AdmitDate)
 ,BedmanSnapshotTime =
  (select max(LastRestore) from DBA.DBA.LastRestore where DestinationDatabase in ('Bedman_A', 'Bedman_B'))
 ,AdmissionTime = BedManSpell.AdmitDate
 ,DischargeTime = BedManSpell.DischTime
 ,AdmissionToDischargeMins = datediff(MINUTE, BedManSpell.AdmitDate, BedManSpell.DischTime)
 --,[Site] = BedManLocation.Hospital  -- Some NULLs here. Emailed Paul Gales, joined to Warehouse.PAS.Ward instead.
 ,[Site] = PASWard.SiteCode
 ,WardCode = BedManSpell.CurrWard
 ,Ward = BedManLocation.[Description]
 ,bedmanspell.CurrLoc
 ,CurrFacility = BedManFacility.[Description]
 ,ConsultantCode = BedManSpell.CurrCon
 ,Consultant = BedManConsultants.Surname + ', ' + BedManConsultants.Initials
 ,BedManSpell.CurrSpec
 ,VTEAssessmentCompleted = 
  case 
   when BedManEvent.Details is not null
   then 1
   else 0
  end
from

 BedmanTest.dbo.HospSpell BedManSpell
 
 left join BedmanTest.dbo.Patients BedManPatient
 on BedManSpell.Patient = BedManPatient.Patient
 
 left join BedmanTest.dbo.Facilities BedManFacility
 on BedManSpell.FacilityID = BedManFacility.FacilityID
 
 left join BedmanTest.dbo.Locations BedManLocation
 on BedManSpell.CurrLoc = BedManLocation.LocationID
 
 left join BedmanTest.dbo.Consultants BedManConsultants
 on BedManSpell.CurrCon = BedManConsultants.ConsCode
 
 left join BedmanTest.dbo.[Event] BedManEvent
 on BedManSpell.SpellID = BedManEvent.HospSpellID
 and BedManEvent.EventTypeID = 4 
 
 left join WarehouseReporting.PAS.Ward PASWard  -- Used this due to some NULL Hospital fields in Bedman.dbo.Locations. Emailed Paul Gales.
 on BedManLocation.PASCode = PASWard.WardCode
 
where
 cast(BedManSpell.AdmitDate as date) between @StartDate and @EndDate  --Parameter Admission Date
 and BedManLocation.PASCode in (select Value from WarehouseReporting.RPT.SSRS_MultiValueParamSplit(@WardPASCode, ',')) -- Parameter Ward
)

/* Get latest spell and exclude completed VTE assessments */
select *
from RawData
where PatientInstance = 1 -- Latest spell (in case of multiple spells within date range)
and VTEAssessmentCompleted = 0
and PatientAge >= 18   -- Blanket exclusion of under 18s
and (AdmissionToDischargeMins > 480 or AdmissionToDischargeMins is null)  -- Exclude if discharged < 8 hours of admission

--Still need to exclude regular days
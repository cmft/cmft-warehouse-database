﻿
create proc RPT.GetDictationProductivityStatistics

(
	@start date = null
	,@end date = null
	,@division varchar(100) = null
	,@specialty varchar(100) = null
	,@user int = null
	,@period char(1) = null
	,@subject varchar(50) = null
	,@status int = null
)

as

set dateformat dmy

set @start = coalesce(@start, '1 apr 2011')
set @end = coalesce(@end, '2 apr 2011')

set @period = coalesce(@period, 'd')
set @subject = coalesce(@subject, 'author')

/* Copy of dbo.OlapCalendar to reduce contention and blocking */

declare @OlapCalendar table 
(
	TheDate date not null primary key
	,LastDayOfWeek smalldatetime
	,TheMonth nvarchar(34) null
);

insert into @OlapCalendar
(
	TheDate
	,LastDayOfWeek
	,TheMonth
)

select 
	TheDate
	,LastDayOfWeek
	,TheMonth	
from 
	WarehouseOLAP.dbo.OlapCalendar with (nolock);


/* Author */

if @subject = 'author'

begin

	select 
		Period =  
				case
					when @Period = 'd'
					then calendar.TheDate
					when @Period = 'w'
					then calendar.LastDayOfWeek
					when @Period = 'm'
					then calendar.TheMonth
				end

		,Division = coalesce([group].Division, 'N\A')
		,Specialty = coalesce([group].Specialty, 'N\A')
		,[User] = author.[User] + 
									case
										when training.DateAttended is null
										then ''
										else ' (' + left(training.DateAttended, 11) + ')'
									end					
		,HasVRLicence = author.HasVRLicence
		,Documents = count(*)
		,DocumentLength = sum(document.[Length])
		,CorrectionTime = sum(document.CorrectionTimeTaken)
		,DictationTime = sum(document.DictationTimeTaken)
		,PassThroughTime = sum(document.PassThroughTimeTaken)
		,TurnaroundTime = sum(datediff(day, document.CreationTime, document.Correctiontime))
	from         
		Dictation.Document document 
		inner join @OlapCalendar calendar on document.CreationDate = calendar.TheDate 
		inner join Warehouse.Dictation.[User] author on document.AuthorCode = [author].UserCode 
		--inner join Warehouse.Dictation.Worktype [worktype] on document.WorktypeCode = [worktype].WorkTypeCode 
		inner join Warehouse.Dictation.[Group] [group] on document.GroupCode = [group].GroupCode
		left outer join
					(
					select
						UserCode
						,DateAttended = min(DateAttended)
					from
						Warehouse.Dictation.TrainingRecord
					where
						Outcome = 'Success'
					group by
						UserCode
					) training
					on training.UserCode = document.AuthorCode
	where     
		document.CreationDate between @start and @end
		--AND (gp.Division IN (SELECT Value FROM Dictation.Split(@Division, ',') AS Split_1))
		and (StatusCode = @status or @status is null)
		and ([group].Division = @division or @division = '(Select All)')
		and ([group].SpecialtyCode = @specialty or @specialty is null)
		and (document.AuthorCode = @user or @user is null)
	group by
		case
			when @Period = 'd'
			then calendar.TheDate
			when @Period = 'w'
			then calendar.LastDayOfWeek
			when @Period = 'm'
			then calendar.TheMonth
		end
		,[group].Division
		,[group].Specialty
		,author.[User] + 
						case
							when training.DateAttended is null
							then ''
							else ' (' + left(training.DateAttended, 11) + ')'
						end
		,author.HasVRLicence
end

else if @subject = 'transcriptionist'

begin

	select 
		Period =  
				case
					when @Period = 'd'
					then calendar.TheDate
					when @Period = 'w'
					then calendar.LastDayOfWeek
					when @Period = 'm'
					then calendar.TheMonth
				end

		,Division = coalesce([group].Division, 'N\A')
		,Specialty = coalesce([group].Specialty, 'N\A')
		,[User] = transcriptionist.[User]
		,HasVRLicence = transcriptionist.HasVRLicence
		,Documents = count(*)
		,DocumentLength = sum(document.[Length])
		,CorrectionTime = sum(document.CorrectionTimeTaken)
		,DictationTime = sum(document.DictationTimeTaken)
		,PassThroughTime = sum(document.PassThroughTimeTaken)
		,TurnaroundTime = sum(datediff(day, document.CreationTime, document.CorrectionTime))
	from         
		Dictation.Document document 
		inner join @OlapCalendar calendar on document.CorrectionDate = calendar.TheDate 
		inner join Warehouse.Dictation.[User] transcriptionist on document.TranscriptionistCode = transcriptionist.UserCode 
		--left outer join Warehouse.Dictation.Worktype [worktype] on document.WorktypeCode = [worktype].WorkTypeCode 
		inner join Warehouse.Dictation.[Group] [group] on document.GroupCode = [group].GroupCode
	where
		document.CorrectionDate between @start and @end
		--AND (gp.Division IN (SELECT Value FROM Dictation.Split(@Division, ',') AS Split_1))
		and (StatusCode = @status or @status is null)
		and ([group].Division = @division or @division = '(Select All)')
		and ([group].SpecialtyCode = @specialty or @specialty is null)
		and (document.TranscriptionistCode = @user or @user is null)
	group by
		case
			when @Period = 'd'
			then calendar.TheDate
			when @Period = 'w'
			then calendar.LastDayOfWeek
			when @Period = 'm'
			then calendar.TheMonth
		end
		,[group].Division
		,[group].Specialty
		,transcriptionist.[User]
		,transcriptionist.HasVRLicence
end


﻿
create proc RPT.GetMortalityPatients
(
@user varchar(50) = null
,@dischargemonth varchar(20) = null
,@specialty varchar(50) = null
,@division varchar(50) = null
,@providerspellnumber varchar(50) = null
)


as 

set @dischargemonth = coalesce(@dischargemonth, '(Select All)')
set @specialty = coalesce(@specialty, '(Select All)')
set @division = coalesce(@division, '(Select All)')
set @user = coalesce(@user, 'cmmc\darren.griffiths')
set @providerspellnumber = coalesce(@providerspellnumber, '(Select All)')

select 
		SiteCode
		,Division
		,EndDirectorateCode
		,SpecialtyCode
		,Specialty
		,SourcePatientNo = 
						case
							when exists 
										(
										select
											1
										from 
											RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then SourcePatientNo
							else 'Unauthorised'
						end
		,CasenoteNumber = 
						case
							when exists 
										(
										select
											1
										from 
											RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then CasenoteNumber
							else 'Unauthorised'
						end
		,DistrictNo =
						case
							when exists 
										(
										select
											1
										from 
											RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then DistrictNo
							else 'Unauthorised'
						end
		,DateOfBirth = 
						case
							when exists 
										(
										select
											1
										from 
											RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then DateOfBirth
							else '01/01/1900'
						end
		,PatientSurname = 
						case
							when exists 
										(
										select
											1
										from 
											RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then PatientSurname
							else 'Unauthorised'
						end
		,PatientForename = 
						case
							when exists 
										(
										select
											1
										from 
											RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then PatientForename
							else 'Unauthorised'
						end
		,DateOfDeath
		,ProviderSpellNo
		,SourceEncounterNo
		,AdmissionMethodCode
		,AdmissionDate
		,DischargeDate
		,DischargeMethodCode
		,ConsultantLastEpisode
		,Consultant
		,EpisodeStartTime
		,EpisodeEndTime
		,StartWardTypeCode
		,EndWardTypeCode
		,CodingCompleteDate
		,PrimaryOperationCode
		,PrimaryOperationDate
		,DiagnosisType
		,diag.Diagnosis

		from 
		(
		select
			spell.SiteCode
			,spell.EndDirectorateCode
			,dir.Division
			,episode.SpecialtyCode
			,specialtyepisode.Specialty
			,spell.SourcePatientNo  
			,spell.CasenoteNumber
			,spell.DistrictNo 
			,spell.DateOfBirth  
			,spell.PatientSurname  
			,spell.PatientForename  
			,spell.DateOfDeath
			,spell.ProviderSpellNo
			,episode.SourceEncounterNo
			,spell.AdmissionMethodCode
			,spell.AdmissionDate
			,spell.DischargeDate
			,spell.DischargeMethodCode
			,ConsultantLastEpisode = conslast.Consultant
			--,episode.ConsultantCode
			,cons.Consultant
			,episode.EpisodeStartTime
			,episode.EpisodeEndTime
			,episode.StartWardTypeCode
			,episode.EndWardTypeCode
			,episode.CodingCompleteDate
			,episode.PrimaryOperationCode
			,episode.PrimaryOperationDate
			,episode.PrimaryDiagnosisCode
			,episode.SubsidiaryDiagnosisCode
			,episode.SecondaryDiagnosisCode1
			,episode.SecondaryDiagnosisCode2
			,episode.SecondaryDiagnosisCode3
			,episode.SecondaryDiagnosisCode4
			,episode.SecondaryDiagnosisCode5
			,episode.SecondaryDiagnosisCode6
			,episode.SecondaryDiagnosisCode7
			,episode.SecondaryDiagnosisCode8
			,episode.SecondaryDiagnosisCode9
			,episode.SecondaryDiagnosisCode10
			,episode.SecondaryDiagnosisCode11
			,episode.SecondaryDiagnosisCode12
			from 
				  apc.Encounter spell
			inner join
					(
					select 
						SourceEncounterNo
						,ProviderSpellNo
						,ConsultantCode
						,SpecialtyCode
						,EpisodeStartTime
						,EpisodeEndTime
						,StartWardTypeCode
						,EndWardTypeCode
						,CodingCompleteDate
						,PrimaryOperationCode
						,PrimaryOperationDate
						,PrimaryDiagnosisCode
						,SubsidiaryDiagnosisCode
						,SecondaryDiagnosisCode1
						,SecondaryDiagnosisCode2
						,SecondaryDiagnosisCode3
						,SecondaryDiagnosisCode4
						,SecondaryDiagnosisCode5
						,SecondaryDiagnosisCode6
						,SecondaryDiagnosisCode7
						,SecondaryDiagnosisCode8
						,SecondaryDiagnosisCode9
						,SecondaryDiagnosisCode10
						,SecondaryDiagnosisCode11
						,SecondaryDiagnosisCode12
						,LastEpisodeInSpellIndicator
					from apc.Encounter episode
					) episode 
					on spell.ProviderSpellNo = episode.ProviderSpellNo
					
			left outer join PAS.Specialty specialtyepisode
			on specialtyepisode.SpecialtyCode = episode.SpecialtyCode
			
			left outer join PAS.Specialty specialtyspell
			on specialtyspell.SpecialtyCode = spell.SpecialtyCode
			
			left outer join PAS.Directorate dir
			on dir.DirectorateCode = spell.EndDirectorateCode
			
			left outer join PAS.Consultant cons
			on cons.ConsultantCode = episode.ConsultantCode
			
			left outer join PAS.Consultant conslast
			on conslast.ConsultantCode = spell.ConsultantCode
			

	
			where 
				spell.DischargeMethodCode in ('DN', 'DP','ST', 'SP')
				and spell.LastEpisodeInSpellIndicator = 'Y'
				and spell.CodingCompleteDate is not null
				and (cast(datename(month, spell.DischargeDate) as char(3)) + ' ' + cast(year(spell.DischargeDate) as char(4)) = @dischargemonth or @dischargemonth = '(Select All)')
				and (dir.Division = @division or @division = '(Select All)')
				and (specialtyspell.NationalSpecialtyCode = @specialty or @specialty = '(Select All)')
				and (spell.ProviderSpellNo = @providerspellnumber or @providerspellnumber = '(Select All)')
				) apc

		unpivot
		(
		DiagnosisCode for DiagnosisType in 
		(
		PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12

		)
		) as mortalitypivot

left outer join WH.Diagnosis diag
on diag.DiagnosisCode = mortalitypivot.DiagnosisCode

option (recompile)
 

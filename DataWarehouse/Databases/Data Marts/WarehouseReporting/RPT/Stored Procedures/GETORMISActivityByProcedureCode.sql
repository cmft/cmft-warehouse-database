﻿


CREATE PROCEDURE [RPT].[GETORMISActivityByProcedureCode]
  @ProcedureCode     varchar(5)
 ,@FromDate          Date
 ,@ToDate            Date

AS 

--DECLARE @ProcedureCode      AS varchar(5) = 'H04.2'
--DECLARE @FromDate           AS DATE       = '01-jan-2011'
--DECLARE @ToDate             AS DATE       = '03-jun-2013'

SELECT 
  CONVERT(varchar,[OP_OPERATION_DATE],103)     AS OperationDate
 ,CONVERT(varchar(10), [OP_START_DATE_TIME], 103) 
     + ' ' 
     + SUBSTRING(CONVERT(varchar(19),[OP_START_DATE_TIME], 120), 12 ,5)     AS OperationStart
 ,CONVERT(varchar(10), [OP_FINISH_DATE_TIME], 103) 
     + ' ' 
     + SUBSTRING(CONVERT(varchar(19),[OP_FINISH_DATE_TIME], 120), 12 ,5)    AS OperationFinish 
 ,[UN_DESC]
 ,[TH_CODE]
 ,[WA_CODE]
 ,[WA_DESC]   
 ,[SU_CODE]                            AS SurgeonCode
 ,[SU_LNAME] + ' - ' + [SU_FNAME]      AS Surgeon
 ,REPLACE([OP_MRN],'Y','/')            AS Casenote
 ,[OP_LNAME]+ ' - ' + [OP_FNAME]       AS PatientName
 ,[OP_AGE_YRS]
 ,@ProcedureCode                       AS SearchProcedure
 ,[CD_CODE]
 ,[CD_DESCRIPTION]
 ,'PrimarySecondary' =
   CASE WHEN [OI_PRIMARY_CD] = 1 THEN 'Primary'
        ELSE 'Secondary'
   END
 ,[OP_SEQU]
 ,[OI_SEQU]
 
FROM Warehousesql.[otprd].[dbo].[FOPERAT]

LEFT OUTER JOIN Warehousesql.[otprd].[dbo].[FOITEM]
   ON [OP_SEQU] = [OI_OP_SEQU]
LEFT OUTER JOIN  Warehousesql.[otprd].[dbo].[FCDOPER]
   ON [OI_CD_SEQU] = [CD_SEQU]
LEFT OUTER JOIN Warehousesql.[otprd].[dbo].[FTHEAT]
   ON [OP_TH_SEQU] = [TH_SEQU]
LEFT OUTER JOIN Warehousesql.[otprd].[dbo].[FUNIT]
   ON [TH_UN_SEQU] = [UN_SEQU]
LEFT OUTER JOIN Warehousesql.[otprd].[dbo].[FWARD]
   ON [OP_WA_TO_SEQU] = [WA_SEQU]
LEFT OUTER JOIN Warehousesql.[otprd].[dbo].[FSURGN]
   ON [OI_SURG1_SEQU] = [SU_SEQU]

WHERE [OP_OPERATION_DATE] BETWEEN @FromDate AND @ToDate
  AND [OP_CANCELLED] = 0
  AND CHARINDEX(@ProcedureCode,[CD_CODE]) > 0
  
ORDER BY [OP_START_DATE_TIME],[OP_SEQU],[OI_START]



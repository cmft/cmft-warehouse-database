﻿

create proc [RPT].[AESignsOfInfection]
	(
	@AEArrivalDateStart date
	,@AEArrivalDateEnd date
	)
	
as

/* 
==============================================================================================
Description:

When		Who			What
10/03/2015	Paul Egan	Initial Coding
===============================================================================================
*/

if object_id('tempdb.dbo.#SignsOfInfectionSSRSReportRawDataAETemp') is not null
	begin
		drop table #SignsOfInfectionSSRSReportRawDataAETemp;
		--print 'temp table dropped (#SignsOfInfectionSSRSReportRawDataAETemp)';
	end
--else
--	print 'temp table NOT dropped (#SignsOfInfectionSSRSReportRawDataAETemp)';

select 
	AEAttendanceNumber = AEEncounter.AttendanceNumber
	,InpatientCasenoteNumber = CurrentLocation.CasenoteNumber
	,AEObservationSetID = AEObservationSet.SourceUniqueID
	,ObservationSetStartTime = AEObservationSet.StartTime
	,ObservationSetWardCode = AEObservationWard.WardCode
	,ObservationSetWard = AEObservationWard.Ward
	,ObservationSetWardDivision = AEObservationWard.Division
	
	--,APCObservation.ObservationMeasureID
	--,APCObservation.Result
	,ObservationMeasure =  
		case
			when AEObservation.ObservationMeasureID = 11 then 'Respiratory Rate'	-- Matching up descriptions for 11 & 34 so they pivot correctly
			else AEObservationMeasureBase.ObservationMeasure
		end
	,Result = 
		case
			when AEObservation.ObservationMeasureID = 12 then AEObservationReferenceBase.[Description]	-- AVPU result description
			else cast(AEObservation.Result as varchar(50))	-- All other results
		end
	,SignsOfInfectionCount = count(*) over (partition by AEObservationSet.SourceUniqueID)	-- Total number of signs of infection per observation set
	,AEArrivalTime = AEEncounter.ArrivalTime
	
	,PatientCurrentLocationWardCode = CurrentLocation.StartWardTypeCode
	,PatientCurrentLocationWard = PASWardCurrent.Ward
	,PatientCurrentLocationSite = PASWardCurrent.SiteCode
	,InpatientDischargeTime = CurrentLocation.DischargeTime
	,LOSDays = 
		case
			when CurrentLocation.AdmissionTime is not null then
				case
					when CurrentLocation.DischargeTime is null then				
						datediff(day, CurrentLocation.AdmissionTime, getdate())
					else datediff(day, CurrentLocation.AdmissionTime, CurrentLocation.DischargeTime)
				end
			else null	
		end
	--,ReportDate = cast(cast(getdate() as date) as datetime)
	--,ReportTime = getdate()
  ,AEEncounterRecno = AEEncounter.EncounterRecno 
	--,APCCasenoteNumber = APCEncounter.CasenoteNumber
	--,APCProviderSpellNo = APCEncounter.ProviderSpellNo
	--,APCSourceEncounterNo = APCEncounter.SourceEncounterNo
	--,APCUpdateCasenoteNumber = APCUpdateEncounter.CasenoteNumber
	--,APCUpdateProviderSpellNo = APCUpdateEncounter.ProviderSpellNo
	--,APCUpdateSourceEncounterNo = APCUpdateEncounter.SourceEncounterNo
	--,AEObservationSourceUniqueID = AEObservation.SourceUniqueID		-- Be careful uncommenting this, it will affect the pivot grouping below.
	--,APCUpdateProviderSpellNo = APCUpdateEncounter.ProviderSpellNo
	--,APCUpdateSourceEncounterNo = APCUpdateEncounter.SourceEncounterNo
	,AEEncounter.DistrictNo
into #SignsOfInfectionSSRSReportRawDataAETemp
from
	Warehouse.AE.Encounter AEEncounter
	
	--left join APCUpdate.Encounter APCUpdateEncounter
	--on	APCUpdateEncounter.DistrictNo = AEEncounter.DistrictNo
	--and APCUpdateEncounter.AdmissionMethodCode = 'AE'
	--and datediff(hour, AEEncounter.DepartureTime, APCUpdateEncounter.AdmissionTime) between -4 and 24
	
	--left join APC.Encounter APCEncounter
	--on	APCEncounter.DistrictNo = AEEncounter.DistrictNo
	--and APCEncounter.AdmissionMethodCode = 'AE'
	--and datediff(hour, AEEncounter.DepartureTime, APCEncounter.AdmissionTime) between -4 and 24
	--and not exists
	--	(
	--	select 1
	--	from APC.Encounter
	--	where 
	--		Encounter.ProviderSpellNo = APCUpdateEncounter.ProviderSpellNo
	--	)

	left join Warehouse.AE.EncounterObservationSet AEEncounterObservationSet
	on	AEEncounterObservationSet.EncounterRecno = AEEncounter.EncounterRecno

	left join Warehouse.Observation.ObservationSet AEObservationSet
	on	AEObservationSet.ObservationSetRecno = AEEncounterObservationSet.ObservationSetRecno
	
	left join Warehouse.Observation.Observation AEObservation
	on	AEObservation.ObservationSetSourceUniqueID = AEObservationSet.SourceUniqueID
	
	left join Warehouse.Observation.ObservationMeasureBase AEObservationMeasureBase
	on	AEObservationMeasureBase.ObservationMeasureID = AEObservation.ObservationMeasureID
	
	left join Warehouse.Observation.Ward AEObservationWard
	on	AEObservationWard.WardID = AEObservationSet.WardID 
	
	/* Get AVPU result description */
	left join Warehouse.Observation.ReferenceBase AEObservationReferenceBase
	on	AEObservationReferenceBase.ReferenceID = AEObservation.Result
	
	/* Get patient current location if still admitted */
	left join APCUpdate.Encounter CurrentLocation
	on	CurrentLocation.DistrictNo = AEEncounter.DistrictNo
	and CurrentLocation.AdmissionMethodCode = 'AE'
	and datediff(hour, AEEncounter.DepartureTime, CurrentLocation.AdmissionTime) between -4 and 24	
	and	CurrentLocation.SourceEncounterNo = 
		(
		select max(e.SourceEncounterNo)
		from APCUpdate.Encounter e
		where e.ProviderSpellNo = CurrentLocation.ProviderSpellNo
		)
				
	/* For ward description of patient current location */
	left join PAS.Ward PASWardCurrent
	on	PASWardCurrent.WardCode = CurrentLocation.StartWardTypeCode

where
	cast(AEEncounter.ArrivalDate as date) between @AEArrivalDateStart and @AEArrivalDateEnd	--dateadd(day, -2, cast(getdate() as date)) and cast(getdate() as date)
	--and AEEncounter.AttendanceDisposalCode = '01'		-- Admitted
	
	and datediff(minute, AEEncounter.ArrivalTime ,AEObservationSet.StartTime) <= 2880	-- within 48 hours of A&E arrival

	and AEEncounter.SiteCode = 'RW3MR'
	and AEEncounter.InterfaceCode = 'SYM'
	
	and 
	(
		(
		AEObservation.ObservationMeasureID in (1, 37)			-- Temperature
		and AEObservation.Result not between 35.6 and 38.3
		)
		or
		(
		AEObservation.ObservationMeasureID in (8, 9, 28, 29)	-- Heart rate
		and AEObservation.Result > 90
		)
		or
		(
		AEObservation.ObservationMeasureID in (11, 34)			-- Respiration Rate
		and AEObservation.Result > 20
		)
		or
		(
		AEObservation.ObservationMeasureID in (12)				-- AVPU (Alert, Voice, Pain, Unconscious)
		and AEObservation.Result <> 38							-- Exclude 'Alert'
		)
	)
;
--go
	
--order by
--	AEObservationSourceUniqueID
--	-- AEArrivalTime
--	--,AttendanceNumber



select
	 AEAttendanceNumber
	,InpatientCasenoteNumber
	,AEArrivalTime
	--,APCObservationSetID
	,ObservationSetStartTime
	,pivoted.[Temperature]
	,pivoted.[Heart Rate]
	,pivoted.[Respiratory Rate]
	,pivoted.[AVPU]
	,SignsOfInfectionCount
	,ObservationSetWardCode
	,ObservationSetWard
	,ObservationSetWardDivision
	--,ObservationMeasure
	--,Result
	--,AEArrivalSequence
	,InpatientAdmissionTime = null
	,EpisodeStartWardCode = null
	,EpisodeStartWard = null
	,EpisodeStartSiteCode = null
	,PatientCurrentLocationWardCode = 
		case when InpatientDischargeTime is not null then null else PatientCurrentLocationWardCode end
	,PatientCurrentLocationWard = 
		case when InpatientDischargeTime is not null then null else PatientCurrentLocationWard end
	,PatientCurrentLocationSite =
		case when InpatientDischargeTime is not null then null else PatientCurrentLocationSite end
	,InpatientDischargeTime
	,LOSDays
	--,ReportDate = cast(cast(getdate() as date) as datetime)
	--,ReportTime = getdate()
	,AEEncounterRecno
	--,APCEncounterRecno
	--,AECasenoteNo
	--,AEDistrictNo
	--,APCDistrictNo

----------into drop table ##SignsOfInfectionSSRSReportSnapshotTemp
from
	--tempdb.dbo.#SignsOfInfectionSSRSReportRawDataAETemp a
	#SignsOfInfectionSSRSReportRawDataAETemp a
	
	pivot (max(Result) 
		for ObservationMeasure in ([AVPU], [Heart Rate], [Respiratory Rate], [Temperature])) pivoted
where
	SignsOfInfectionCount >= 2
	
	--/*	Get latest A&E arrival for same inpatient admission. */
	--/* (The fuzzy match on dates brought back a small number of multiple A&E arrivals for the same inpatient admission). */
	--and AEArrivalSequence = 1	

--order by
--	AEArrivalTime
--	,ObservationSetStartTime

/* Clean up */
if object_id('tempdb.dbo.#SignsOfInfectionSSRSReportRawDataAETemp') is not null
	begin
		drop table #SignsOfInfectionSSRSReportRawDataAETemp;
		--print 'temp table dropped (#SignsOfInfectionSSRSReportRawDataAETemp)';
	end
--else
--	print 'temp table NOT dropped (#SignsOfInfectionSSRSReportRawDataAETemp)';

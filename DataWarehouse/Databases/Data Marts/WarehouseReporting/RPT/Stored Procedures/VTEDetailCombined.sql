﻿CREATE procedure [RPT].[VTEDetailCombined] --201109 , null , null , null , null , 1
(
	 @FinancialMonthKey int
	,@DivisionCode varchar(10)
	,@SpecialtyCode varchar(10)
	,@WardCode varchar(10)
	,@ConsultantCode varchar(10)
	,@CodingMode int
	--,@Location varchar(5)
)

as
select

	 Division.Division
	,VTESpell.VTEExclusionReason
	,Encounter.ClinicalCodingStatus
	,VTESpell.VTECategory
	,Encounter.CasenoteNumber
	,Encounter.DistrictNo
	,Encounter.NHSNumber
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	--,Specialty.NationalSpecialtyCode
	,Specialty.NationalSpecialty
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.SiteCode
	,Encounter.ConsultantCode
	,Consultant.Consultant
	,Specialty.SpecialtyCode
	,Specialty.Specialty
	,Encounter.PatientCategoryCode
	,Ward.WardCode
	,Ward.Ward
	,Encounter.PrimaryDiagnosisCode
	,Diagnosis.DiagnosisChapter
	,Diagnosis.Diagnosis
	,Encounter.PrimaryOperationCode
	,Operation.OperationChapter
	,Operation.Operation
	,CasenoteLocation = CasenoteLocation.CasenoteLocation
	,CasenoteBorrower.Borrower
	,'M' as Location
from
	WarehouseReporting.APC.Encounter

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = Encounter.AdmissionDate

left join WarehouseReporting.APC.VTESpell
on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

left join WarehouseReporting.WH.Directorate
on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

left join WarehouseReporting.WH.Division
on	Division.DivisionCode = Directorate.DivisionCode

left join WarehouseReporting.PAS.Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join WarehouseReporting.PAS.Ward
on	Ward.WardCode = Encounter.StartWardTypeCode

left join WarehouseReporting.PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join WarehouseReporting.WH.Diagnosis
on	Diagnosis.DiagnosisCode = Encounter.PrimaryDiagnosisCode

left join WarehouseReporting.WH.Operation
on	Operation.OperationCode = Encounter.PrimaryOperationCode

left join WarehouseReporting.PAS.Casenote
on	Casenote.SourcePatientNo = Encounter.SourcePatientNo
and	Casenote.CasenoteNumber = Encounter.CasenoteNumber

left join WarehouseReporting.PAS.CasenoteLocation
on	CasenoteLocation.CasenoteLocationCode = Casenote.CasenoteLocationCode

left join WarehouseReporting.PAS.CasenoteBorrower
on	CasenoteBorrower.BorrowerCode = Casenote.CurrentBorrowerCode

where
	Encounter.FirstEpisodeInSpellIndicator = 1
and	Calendar.FinancialMonthKey = @FinancialMonthKey
and	Encounter.PatientCategoryCode != 'RD'
and	(
		(
			Encounter.AgeCode like '%Years'
		and	left(Encounter.AgeCode , 2) > '17'
		)
	or	Encounter.AgeCode = '99+'
	)
and (
		Directorate.DivisionCode = @DivisionCode
	or	@DivisionCode is null
	)
and	(
		Specialty.NationalSpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)
and	(
		Encounter.StartWardTypeCode = @WardCode
	or	@WardCode is null
	)
and	(
		Encounter.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and
	(
		@CodingMode = 0
	or	(
			@CodingMode = 1
		and	Encounter.ClinicalCodingStatus = 'C'
		and	VTESpell.VTEExclusionReasonCode is null
		and	VTESpell.VTECategoryCode != 'C'
		)
	)

UNION ALL

SELECT 	 
	'Trafford'
	 ,Exclusion = CASE	when Proc_Cohort = 'Cohort' then 'PROC'
						when DVT = 'Cohort' then 'DVT'
						when [cohort for Manual signoff] =  'Cohort' then 'MAU'
						else null end
	,case when (ProcedureCode IS NOT NULL OR DiagnosticCode IS NOT NULL) 
	then 'C' else Null end
	,case when All_Cohorts IS NULL and Complete = 1 then 'VTE Complete' 
	when mau.admissionnumber IS NOT NULL THEN 'VTE Complete' 
	else Null end						
	,Null			CasenoteNumber						
	,VTE.EPMINumber		DistrictNo				
	,Null			NHSNumber 					
	,VTE.GivenName			
	,VTE.FamilyName
	,BirthDate
	,bedmanSpecialty Specialty
	,VTE.AdmittedOn
	,VTE.DischargedOn		
	,Null SiteCode ------ 	
	,ConUnique
	,isnull(VTE.ConSurname + ', ' + VTE.ConFirstName, VTE.ConSurname)
	,bedmanSpecialtyCode SpecialtyCode
	,bedmanSpecialty Specialty
	,Null PatientCategoryCode	
	,VTE.[admittingward] WardCode
	,WardName Ward	
	
	,DiagnosticCode PrimaryDiagnosisCode
	,diagnosischapter DiagnosisChapter
	,diagnosis Diagnosis
	,ProcedureCode PrimaryOperationCode
	,operationchapter OperationChapter
	,operation Operation
	,Null CasenoteLocation
	,Null Borrower	
	

	,'T' as Location
	
	
	
FROM [WarehouseReporting].[dbo].[AC_VTE_Cohorts] VTE

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = cast(VTE.AdmittedOn as date)

left join dbo.VTE_Temp_MAU mau on VTE.AdmissionNumber = mau.AdmissionNumber and [vte completed] <> 'None'


	where Calendar.FinancialMonthKey = @FinancialMonthKey

and (
		'Trafford' = @DivisionCode
	or	@DivisionCode is null
	)
and	(
		Null = @SpecialtyCode
	or	@SpecialtyCode is null
	)
and	(
		VTE.[admittingward] = @WardCode
	or	@WardCode is null
	)
and	(
		ConUnique = @ConsultantCode
	or	@ConsultantCode is null
	)

and
	(
		@CodingMode = 0
	or	(
			@CodingMode = 1
		--and	Encounter.ClinicalCodingStatus = 'C'
		--and	VTESpell.VTEExclusionReasonCode is null
		--and	VTESpell.VTECategoryCode != 'C'
		)
	)

































--SELECT 
--	 Exclusion = VTESpell.VTEExclusionReasonCode
--	,DivisionCode = 'Trafford'
--	,Division = 'Trafford'
--	,SpecialtyCode = bedmanSpecialty
--	,Specialty = bedmanSpecialtyCode
--	,WardCode = VTE.[admittingward]
--	,Ward = WardName
--	,ConsultantCode = [ConsultantCode]
--	,Consultant = isnull(VTE.[ConSurname] + ', ' + VTE.[ConFirstName], VTE.[ConSurname])
--	,Cases = 1
	
--	,CodedExclusionCases = 
--		case
--		--- EXCLUDED, CODED AND NOT COMPLETE
--		when VTESpell.VTEExclusionReasonCode is not null 
--			AND (ProcedureCode IS NOT NULL OR DiagnosticCode IS NOT NULL)
--			AND (ISNULL(mau.admissionnumber,'X') = 'X' AND Complete = 0)
			
--			--VTESpell.VTEExclusionReasonCode is not null 
--			--and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
--			then 1
--		else 0
--		end

--	,UncodedExclusionCases = 
--		--- EXCLUDED, UNCODED AND NOT COMPLETE	
--		case
--		when VTESpell.VTEExclusionReasonCode is not null 
--			AND (ProcedureCode IS NULL AND DiagnosticCode IS NULL)
--			AND (ProcedureCode IS NOT NULL OR DiagnosticCode IS NOT NULL)
--			AND (ISNULL(mau.admissionnumber,'X') = 'X' AND Complete = 0)
					
--		--		Encounter.CodingCompleteDate IS NULL
--		--	AND VTESpell.VTEExclusionReasonCode is not null
--		--	and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
--			then 1
--		else 0
--		end

--	,VTECompleteCases = case when Complete = 1 then 1
--						else
--							case when ISNULL(mau.admissionnumber,'X') <> 'X' then 1
--	    					else 0 
--	    					end 
--	    				end
	    					
--		--case FormStatus
--		--when 'Complete' then 1
--		--else 0
--		--end

--	,VTEIncompleteCases =	case when Complete = 1 then 0
--							else
--								case when ISNULL(mau.admissionnumber,'X') <> 'X' then 0
--	    						else 1
--	    						end 
--	    					end

--		--case FormStatus
--		--when 'Incomplete' then 1
--		--else 0
--		--end


--	,VTEMissingCases = 0
--		--case ISNULL(FormStatus,'1')
--		--when '1' then 1
--		--else 0
--		--end
		
--	,CodingCompleteCases = 	case when (ProcedureCode IS NOT NULL OR DiagnosticCode IS NOT NULL) 
--	--AND All_Cohorts IS NULL 
--	then 1 else 0 end
						


		
--	,CodingIncompleteCases = case when (ProcedureCode IS NULL AND DiagnosticCode IS NULL) 
--	--AND All_Cohorts IS NULL 
--	then 1 else 0 end
	
--	,'T' as Location
	
	
	
	
--FROM [WarehouseReporting].[dbo].[AC_VTE_Cohorts] VTE

--inner join WarehouseReporting.WH.Calendar
--on	Calendar.TheDate = cast(AdmittedOn as date)

--left join APC.VTESpellTrafford VTESpell
--on	VTESpell.ProviderSpellNo = VTE.AdmissionNumber

--left join dbo.VTE_Temp_MAU mau on VTE.AdmissionNumber = mau.AdmissionNumber and [vte completed] <> 'None'

--	where Calendar.FinancialMonthKey = @FinancialMonthKey




























































--SELECT 	 
--	 'Trafford'
--	,Null as VTEExclusionReason
--	,Patient.CompleteForms ClinicalCodingStatus
--	,case FormStatus
--	 when 'Complete' then 'VTE Complete'
--	 else Null
--	 end VTECategory
--	,Null CasenoteNumber
--	,Patient.EPMINumber DistrictNo
--	,Patient.NHSNumber
--	,Patient.GivenName PatientForename
--	,Patient.FamilyName PatientSurname
--	,Patient.BirthDate DateOfBirth
--	,patient.Specialty NationalSpecialty
--	,AdmissionDate
--	,DischargeDate
--	,Null SiteCode ------ 
--	,ConsultantCode = isnull(PersonConsultant.FamilyName + ', ' + PersonConsultant.GivenName, PersonConsultant.FamilyName)
--	,Consultant = isnull(PersonConsultant.FamilyName + ', ' + PersonConsultant.GivenName, PersonConsultant.FamilyName)
--	,patient.SpecialtyCode 
--	,patient.Specialty
--	,Null PatientCategoryCode
--	,Ward.Code WardCode
--	,Ward.Name Ward

--	,Null PrimaryDiagnosisCode
--	,Null DiagnosisChapter
--	,Null Diagnosis
--	,Null PrimaryOperationCode
--	,Null OperationChapter
--	,Null Operation
--	,Null CasenoteLocation
--	,Null Borrower


--	,'T' as Location
	
	
	
	
--FROM
--(
--	select 
--		dept.Name Division
--		,dept.code DivisionCode
--		,sp.Name Specialty
--		,sp.code SpecialtyCode
--		,Admission.Status AdmissionStatus
--		,Admission.HISId_Number AdmissionId
--		,Admission.AdmittedOn AdmissionDate
--		,case 
--			when Admission.Status = 'DI' THEN
--				(SELECT Max(EndDate) FROM Replica_UltraGendaBedman.dbo.BedOccupancy WHERE AdmissionId = Admission.Id)
--			else
--				null
--		 end DischargeDate
--		,case 
--			WHEN vtelist.FormId is null then 'Incomplete'
--			else 'Complete'
--		 end as FormStatus
--		,case 
--			WHEN vtelist.FormId is null then null
--			else 'C'
--		 end as CompleteForms
--		,case 
--			WHEN vtelist.FormId is null then 1
--			else 0
--		 end as IncompleteForms
--		,Patient.HISId_Number EPMINumber
--		,Patient.NationalNumber1 NHSNumber
--		,PersonPatient.FamilyName
--		,PersonPatient.GivenName
--		,PersonPatient.BirthDate
--		,case 
--			when Admission.Status = 'AD' then
--				(SELECT top 1 BedId from Replica_UltraGendaBedman.dbo.BedOccupancy bo WHERE Admission.Id = bo.AdmissionId and (getdate() between StartDate and EndDate OR EndDate < getdate()) ORDER BY EndDate desc)
--			when Admission.Status = 'DI' then
--				(SELECT top 1 BedId from Replica_UltraGendaBedman.dbo.BedOccupancy bo WHERE Admission.Id = bo.AdmissionId ORDER BY EndDate desc)
--		 end as BedId
--		,(SELECT top 1 AuthorityId from Replica_UltraGendaBedman.dbo.BedOccupancy bo WHERE Admission.Id = bo.AdmissionId ORDER BY StartDate asc) as AuthorityId
--	from 
--		Replica_UltraGendaBedman.dbo.Admission 
--	inner join 
--		Replica_UltraGendaFoundation.dbo.Patient Patient
--	on
--		Admission.PatientId = Patient.Id
--	inner join
--		Replica_UltraGendaFoundation.dbo.Person PersonPatient
--	on 
--		Patient.PersonId = PersonPatient.Id
--	left join
--		Whiteboard.dbo.InfopathForm_VTEList vtelist
--	on
--		Admission.HISId_number = vtelist.AdmissionNumber
--	left join Replica_UltraGendaBedman.dbo.BedOccupancy bo 
--		on Admission.Id = bo.AdmissionId --and (getdate() between StartDate and EndDate OR EndDate < getdate())
--	left join Replica_UltraGendaBedman.dbo.AdmissionType at
--		on bo.AdmissionTypeId = at.id	
--	left join Replica_UltraGendaBedman.dbo.Speciality sp
--		on at.SpecialityId = sp.id
--	left join Replica_UltraGendaFoundation.dbo.HISDepartment dept
--		on sp.HISDepartmentId = dept.id		
--	Where 
--		Admission.IsCurrent = 1 
--	and Admission.IsDeleted = 0
--	and Admission.Status IN ('AD', 'DI')
--	and Admission.AdmittedOn >= '01 Jun 2010'
----	and (vtelist.FormStatus = 'New' or vtelist.FormStatus is null)
--	and datediff(year,PersonPatient.BirthDate , Admission.AdmittedOn) >= 18
--) Patient
--inner join 
--	Replica_UltraGendaFoundation.dbo.Authority Auth
--ON
--	Patient.AuthorityId = Auth.Id
--inner join 
--	Replica_UltraGendaFoundation.dbo.Person PersonConsultant
--on
--	Auth.PersonId = PersonConsultant.Id
--inner join Replica_UltraGendaBedman.dbo.Bed
--	on Patient.BedId = Bed.id
--inner join Replica_UltraGendaBedman.dbo.Room
--	on Room.Id = Bed.RoomId
--inner join Replica_UltraGendaBedman.dbo.Ward
--	on Ward.Id = Room.WardId
--inner join WarehouseReporting.WH.Calendar
--on	Calendar.TheDate = cast(AdmissionDate as date)
--where Calendar.FinancialMonthKey = @FinancialMonthKey

----where
----	AdmissionDate BETWEEN '01 April 2012' AND cast('30 April 2012' as datetime) + 1
	
--and (
--		'Trafford' = @DivisionCode
--	or	@DivisionCode is null
--	)	
	
--and	(
--		Patient.SpecialtyCode = @SpecialtyCode
--	or	@SpecialtyCode is null
--	)
		
--and	(
--		Ward.Code = @WardCode
--	or	@WardCode is null
--	)
		
--and	(
--		isnull(PersonConsultant.FamilyName + ', ' + PersonConsultant.GivenName, PersonConsultant.FamilyName) = @ConsultantCode
--	or	@ConsultantCode is null
--	)	
	
--and
--	(@CodingMode = 0 or 	@CodingMode = 1	)
	
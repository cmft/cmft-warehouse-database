﻿
CREATE proc RPT.AEAttendanceGpComments

as


select
	FinancialYear = Calendar.FinancialYear
	,PeriodEnding = 
			case
			when Calendar.LastDayOfWeek between '28 sep 2014' and '21 dec 2014'
			then '21 Dec 2014'
			else Calendar.LastDayOfWeek 
			end
	,ClinicianSeen = CMFT.String.Propercase(isnull(ClinicianSeen, 'Not Specified'))
	,AttendanceCases = sum(1)
	,InformationForGpPopulatedCases = 
					sum(
						case 
						when InformationForGP is null then 0
						else 1
						end
					)
	,InformationForGpNotPopulatedCases = 
					sum(
						case 
						when InformationForGP is null then 1
						else 0
						end
					)
					
from
	AE.DischargeSummary

inner join WH.Calendar
on	ArrivalDate = TheDate

group by
	Calendar.FinancialYear
	,Calendar.LastDayOfWeek
	,ClinicianSeen



﻿
CREATE PROCEDURE [RPT].[GetCodingCensusDT] 
(
	 @CensusDate Date = NULL
	,@Division varchar(MAX)= NULL
	,@NationalSpecialtyCode varchar(MAX) = NULL
	,@CodingLagDaysGroup varchar(MAX) = NULL
	,@CasesWithinTarget INT = NULL
)

AS

SELECT
	 CodingCensus.CensusDate
	,CodingCensus.ProviderSpellNo
	,Encounter.CasenoteNumber
	,Encounter.NHSNumber
	,CodingCensus.SourceEncounterNo
	,CodingCensus.EpisodeStartTime
	,CodingCensus.EpisodeEndTime
	,CodingCensus.AdmissionTime
	,CodingCensus.DischargeTime
	,CodingCensus.ClinicalCodingCompleteTime
	,CodingCensus.SpellClinicalCodingCompleteTime
	,Encounter.ClinicalCodingStatus
	,CodingCensus.CodingLagDays
	,CodingLagDaysGroup = CASE 
							WHEN CodingLagDays BETWEEN 0 AND 6 THEN convert(varchar(5),CodingLagDays)
							WHEN CodingLagDays BETWEEN 7 AND 13 THEN '07-14' 
							WHEN CodingLagDays BETWEEN 14 AND 20 THEN '14-21' 
							WHEN CodingLagDays BETWEEN 21 AND 27 THEN '21-28'
							WHEN CodingLagDays <0 THEN '0'
							ELSE '28+'
						  END
	,CasesWithinTarget = CASE
							WHEN CodingLagDays <7 THEN 1
							ELSE 0
						 END
	,Directorate.Division
	,Specialty.NationalSpecialty
FROM 
	WarehouseReporting.APC.CodingCensus CodingCensus
INNER JOIN PAS.Specialty Specialty
	ON Specialty.SpecialtyCode = CodingCensus.SpecialtyCode
INNER JOIN PAS.Directorate Directorate
	ON Directorate.DirectorateCode = CodingCensus.EndDirectorateCode
INNER JOIN APC.Encounter
	ON Encounter.SourceUniqueID = CodingCensus.SourceUniqueID
WHERE 
		(
			CensusDate = @CensusDate
		OR
			@CensusDate IS NULL
		)
	AND	
		(
		
			Directorate.Division IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
		OR
			@Division IS NULL
		)
	AND 
		(
			Specialty.NationalSpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
		OR
			@NationalSpecialtyCode IS NULL
		)
	AND 
		(
			CASE 
				WHEN CodingLagDays BETWEEN 0 AND 6 THEN convert(varchar(5),CodingLagDays)
				WHEN CodingLagDays BETWEEN 7 AND 13 THEN '07-14' 
				WHEN CodingLagDays BETWEEN 14 AND 20 THEN '14-21' 
				WHEN CodingLagDays BETWEEN 21 AND 27 THEN '21-28'
				WHEN CodingLagDays <0 THEN '0'
			ELSE '28+'
			END 
				IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@CodingLagDaysGroup,','))
		OR
			@CodingLagDaysGroup IS NULL
		)
	AND 
		(
			CASE
				WHEN CodingLagDays <=7 THEN 1
				ELSE 0
			 END
				IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@CasesWithinTarget,','))
		OR
			@CasesWithinTarget IS NULL
		)
		
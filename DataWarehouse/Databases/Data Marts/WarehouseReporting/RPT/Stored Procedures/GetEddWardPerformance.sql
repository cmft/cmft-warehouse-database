﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [RPT].[GetEddWardPerformance] 
(
	 @startdate smalldatetime =NULL
	,@enddate smalldatetime=NULL
	,@Divisioncode varchar(max)=null
)

AS 

SELECT   
	 DivisionCode = ISNULL(Directorate.DivisionCode, 'Unknown')
	,Division = ISNULL(Division.Division, 'Unknown')
	,WardCode = ISNULL(Ward.WardCode, 'Unknown')
	,Ward = ISNULL(Ward.Ward, 'Unknown')
	,Calendar.FinancialYear
	,Calendar.WeekNo
	,Calendar.WeekNoKey
	,EddStatus =	CASE
						WHEN ExpectedLOS.EddCreatedTime IS NULL THEN 'No EDD'
						WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
							AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))  <=0  
							THEN 'Achieved EDD'
						WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
							AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0  
							THEN 'Failed EDD'
						ELSE 'Unknown'
					END
	,Consultant
	,ConsultantType = Case 
		when Consultant in('IMD','SFD','MPB','SHS','SCO','PKUM','MAGS','CEWI','RLJB','AMD','SKN','EKOT') then 'Secondary'
		else 'Tertiary'
	end
	,Cases = COUNT(ExpectedLOS.SourceUniqueId)	
FROM         
	Warehouse.APC.ExpectedLOS ExpectedLOS
INNER JOIN Warehouse.WH.Directorate Directorate
	ON ExpectedLOS.DirectorateCode = Directorate.DirectorateCode 
	AND 
		(
			Directorate.DivisionCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@Divisioncode,','))
		OR
			@DivisionCode IS NULL
		)
INNER JOIN Warehouse.PAS.Ward Ward
	ON ExpectedLOS.Ward = Ward.WardCode
INNER JOIN Warehouse.PAS.Specialty Specialty
	ON ExpectedLOS.Specialty =  Specialty.SpecialtyCode 
INNER JOIN Warehouse.WH.Division Division
	ON Directorate.DivisionCode = Division.DivisionCode
INNER JOIN WH.Calendar Calendar
	ON CONVERT(date, ExpectedLOS.DischargeTime, 103) = Calendar.TheDate
WHERE     
		CONVERT(date, ExpectedLOS.DischargeTime, 103) >= @startdate --'14/Aug/2011'
	AND CONVERT(date, ExpectedLOS.DischargeTime, 103) <= @enddate --'14/Aug/2011'
	AND 
	ExpectedLOS.ManagementIntentionCode NOT IN ('R','N') 
	AND NOT (ExpectedLOS.Ward = 'ESTU' AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6)
	AND ArchiveFlag <> 'D'
	AND Specialty.NationalSpecialtyCode <> '501'
	AND ExpectedLOS.Ward <> '76A'
	AND Specialty.Specialty NOT like '%DIALYSIS%'
	AND Specialty.Specialtycode NOT like 'IH%'
	AND ExpectedLOS.Ward NOT like 'SUB%'
GROUP BY 
	 ISNULL(Directorate.DivisionCode, 'Unknown') 
	,ISNULL(Division.Division, 'Unknown')
	,ISNULL(Ward.WardCode, 'Unknown')
	,ISNULL(Ward.Ward, 'Unknown')
	,Calendar.FinancialYear
	,Calendar.WeekNo
	,Calendar.WeekNoKey
	,CASE
		WHEN ExpectedLOS.EddCreatedTime IS NULL THEN 'No EDD'
		WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
			AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))  <=0  
			THEN 'Achieved EDD'
		WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
			AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0  
			THEN 'Failed EDD'
		ELSE 'Unknown'
	 END
	 ,Consultant


 


﻿




CREATE proc [RPT].[CHAMPParticipation]
(
 @AssessmentDateStart datetime2(0)
,@AssessmentDateEnd datetime2(0)
)

as

/*===============================================================================
When		Who			What
29/01/2015	Paul Egan	Initial Coding for SSRS report
20/02/2015	Paul Egan	Added YearGroup
03/03/2015	Paul Egan	Added IsAccountRegistered
===============================================================================*/


/*============== Debug only ========================*/
--declare
-- @AssessmentDateStart datetime2(0) = '20140901'
--,@AssessmentDateEnd datetime2(0) = '20150831' --cast(getdate() - 1 as date);
/*==================================================*/

select
	CHAMPEncounter.SchoolCode
	,School.School
	--,CHAMPEncounter.SchoolYear
	,CHAMPEncounter.YearGroup
	,CHAMPEncounter.Classification
	,CHAMPEncounter.ClassificationOrder	
	,ChildrenVisited = 1
	,ChildrenMeasured = case when CHAMPEncounter.BMI is null then 0 else 1 end
	,IsMailed = cast(CHAMPEncounter.IsMailed as int)
	,IsAccountRegistered = cast(CHAMPEncounter.IsAccountRegistered as int)
	,AssessmentDate = cast(CHAMPEncounter.AssessmentTime as date)
	,CHAMPEncounter.AssessmentTime
from
	Warehouse.COM.CHAMPEncounter

	inner join Warehouse.ChildHealth.School
	on School.SchoolCode = CHAMPEncounter.SchoolCode
	
where
	cast(CHAMPEncounter.AssessmentTime as date) between @AssessmentDateStart and @AssessmentDateEnd
;




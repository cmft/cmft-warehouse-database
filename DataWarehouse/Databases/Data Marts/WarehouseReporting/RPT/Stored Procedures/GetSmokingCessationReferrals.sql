﻿
create proc RPT.GetSmokingCessationReferrals

@iscomplete bit = 0

as

select
	[id]
	,[FirstName]
	,[LastName]
	,[CaseNote]
	,[InternalPatientNumber]
	,[HomePhone]
	,[WorkPhone]
	,[MobilePhone]
	,[OtherPhoneContact]
	,[ReferralDate]
	,[InfoPathID]
	,[IsComplete]
	,CurrentWard = case
						when exists
									(
									select
										1
									from
										APC.Encounter encounter
									where
										encounter.SourcePatientNo = smokingcessation.[InternalPatientNumber]
										and DischargeDate is null
										and EpisodeStartTime < getdate()
									)
						then (
								select top 1
									Ward
								from 
									Warehouse.APC.WardStay wardstay
									inner join Warehouse.PAS.Ward ward
									on wardstay.WardCode = ward.WardCode
								where 
									wardstay.SourcePatientNo = smokingcessation.[InternalPatientNumber]
								order by
									StartTime desc
								)
						else 'Not Admitted'
					end
			
				
from
	[Information].[Information].[SmokingCessationReferrals] smokingcessation
where
	[IsComplete] = @iscomplete
	

	

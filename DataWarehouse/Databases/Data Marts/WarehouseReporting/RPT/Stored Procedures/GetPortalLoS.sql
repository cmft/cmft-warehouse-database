﻿






CREATE proc [RPT].[GetPortalLoS]


(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
,@breach int = null
)

as

set @consultant = --'CMMC\neil.parrott'
					case
						when @consultant in
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\neil.parrott'
						/* for CSS */
						when @consultant in
											(
											'CMMC\John.Butler'
											,'CMMC\Rachael.Challiner'
											,'CMMC\Bernard.Foex'
											,'CMMC\Ruari.Greer'
											,'CMMC\Steve.Jones'
											,'CMMC\Michael.Parker'
											,'CMMC\Srikanth.Amudalapalli'
											,'CMMC\roger.slater'
											,'CMMC\steve.benington'
											,'CMMC\daniel.conway'
											,'CMMC\dougal.atkinson'
											,'CMMC\maheshhan.nirmalan'
											,'CMMC\magnus.garrioch'
											,'CMMC\John.Moore'
											,'CMMC\hilary.gough' 
											)
						then 'CMMC\Jane.Eddleston'
						else @consultant
					end

select 
	directorate.[DirectorateCode]
	,[Directorate]
	,division.[DivisionCode]
	,[Division]
	,[NationalSpecialty]
	,Consultant.ConsultantCode
	,Consultant = Consultant.Consultant
	,encounter.CasenoteNumber
	,encounter.DistrictNo
	,encounter.PatientSurname
	,encounter.PatientForename
	,Patient = encounter.PatientSurname + ', ' + encounter.PatientForename --+ ' (' + encounter.CasenoteNumber + ')'
	,encounter.AdmissionTime
	,CurrentLOS = datediff(day, encounter.AdmissionDate, dateadd(dd, -1, dateadd(dd, datediff(dd, 0, getdate()), 0)))
	,encounter.ExpectedLOS
	,Cases = 1
from
	APC.Encounter encounter 
	
	left outer join [WarehouseReporting].[APC].[ExpectedLOS] expectedlos
	on expectedlos.SourceUniqueID = encounter.SourceUniqueID
	and ArchiveFlag = 'N'
	
	inner join WarehouseReporting.PAS.Consultant consultant
	on Consultant.ConsultantCode = Encounter.ConsultantCode	
	
	inner join WarehouseReporting.WH.Directorate
	on Directorate.DirectorateCode = Encounter.StartDirectorateCode

	inner join WarehouseReporting.WH.Division
	on Division.DivisionCode = Directorate.DivisionCode

	inner join WarehouseReporting.PAS.Specialty
	on Specialty.SpecialtyCode = Encounter.SpecialtyCode


	
where
	encounter.DischargeDate is null
	and encounter.EpisodeEndDate is null
	and 
		(
		case
			when datediff(day, encounter.AdmissionDate, dateadd(dd, -1, dateadd(dd, datediff(dd, 0, getdate()), 0))) > encounter.ExpectedLOS
			then 1
		end = @breach
		or @breach is null
		)
	and consultant.DomainLogin = @consultant
	
	and encounter.PrimaryDiagnosisCode not like 'O04%'
	and encounter.PrimaryOperationCode not like 'Q11%'
	and encounter.PrimaryOperationCode not like 'Q12%'
	
--option (recompile)







﻿








CREATE proc [RPT].[GetClinicalCodingPatientReport]

 @Casenote varchar(15) = null

as

--DECLARE @Casenote AS varchar(15)
--SET @Casenote = 'M12/007564'

select
	--distinct
	encounter.SourcePatientNo
	,encounter.NHSNumber
	,encounter.CasenoteNumber
	,PatientName = encounter.[PatientSurname] + ', ' + encounter.[PatientForename]
	,encounter.DateOfBirth
	,LatestAdmissionDate = encounter.AdmissionDate
	,AlwaysPresentCondition = LongTermCondition.[Diagnosis]
	,AlwaysPresentConditionLastCodedDate = LongTermCondition.AdmissionTime


from
	[APC].[Encounter] encounter

left outer join
			(
			select
				LongTermCondition.Diagnosis
				,Encounter.SourcePatientNo
				,AdmissionTime = max(Encounter.AdmissionTime)
			from
				[APC].[Diagnosis] Diagnosis

			inner join 
				APC.Encounter Encounter
			on	Diagnosis.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

			inner join 
				WarehouseReporting.WH.Diagnosis LongTermCondition
			on	LongTermCondition.[DiagnosisCode] = Diagnosis.[DiagnosisCode]
			and	LongTermCondition.IsAlwaysPresentFlag = 1

			group by
				LongTermCondition.Diagnosis
				,Encounter.SourcePatientNo
						
			) LongTermCondition

			on LongTermCondition.SourcePatientNo = Encounter.SourcePatientNo

where
    encounter.CasenoteNumber = @Casenote
and
	encounter.LastEpisodeInSpellIndicator = 'Y'
and
	AdmissionDate = (
					select
						max(AdmissionDate)
					from
						[APC].[Encounter] encounterlatest
					where
						encounterlatest.SourcePatientNo = encounter.SourcePatientNo
					)



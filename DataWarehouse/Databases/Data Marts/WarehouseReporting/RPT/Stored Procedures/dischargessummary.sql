﻿create procedure [RPT].[dischargessummary]

AS


SELECT CONVERT(VARCHAR(10), E.[EpisodeEndDate],103) EPi_End, CONVERT(VARCHAR(10),E.[DischargeTime],103) Disch_Date,
E.[SourcePatientNo] InternalNo, E.[DistrictNo], E.[CasenoteNumber] CaseNoteNo, E.[PatientForename] Forenames, E.[PatientSurname]Surname, 
E.[SpecialtyCode]AdmitSpec, E.[EndSiteCode]DischHosp,[StartDirectorateCode] Division, E.[ConsultantCode]DischConsultant,
E.[SourceSpellNo] EpisodeNo, E.[AdmissionDate] AdmitDate,[DischargeDate]DischDate,[EndWardTypeCode]DischWard, E.[LOS] LOSAdmit,
'09/08/2011' CensusDate, E.[StartSiteCode] Hospital

FROM [WarehouseReporting].[APC].[Encounter] AS E

WHERE E.[DischargeTime] > '07/22/2011'
AND E.[LastEpisodeInSpellIndicator] = 'Y'

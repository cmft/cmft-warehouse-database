﻿


	

CREATE PROCEDURE [RPT].[CreatinineAlert] 
(
	 @ReportType varchar(10) -- RATE, CHANGE or VALUE, RR added Latest
	,@RateOfChange int -- 
	,@Stage varchar (100) -- (Stage1 (>=1.5 to <2.0); Stage2 (>=2.0 to <3.0); Stage3 (>=3.0) 
	,@NationalAlert varchar(100) -- NationalAlert = AKI 1;AKI 2;AKI 3;No Alert;No Alert - Repeat;Suggest??
	,@ResultValue int
	,@Summary bit
	,@DistrictNo char(8)
	,@Interval numeric(21 , 10)
	,@FromDate date
	,@ToDate date
	,@CurrentInpatient tinyint
	,@PatientCategory varchar(2)
	
	--,@PercentageChange numeric(21 , 10)
	--,@FromPercentageChange numeric(21 , 10)
	--,@ToPercentageChange numeric(21 , 10)
	
)

as

set dateformat dmy

select
	 PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,AgeCode
	,Ethnicity 
	,Sex
	,LatestAdmissionDate
	,LatestPatientCategory
	,LatestWardCode
	,Consultant
	,Specialty
	,Result
	,ResultDate
	,Result.ResultTime
	,SequenceNoPatient
	,SequenceNoSpell
	,Result.PreviousResult
	,Result.PreviousResultTime
	--,SequenceNo
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
	,CurrentInpatient
	,LengthOfStay
	,DiedInHospital
	,Result.Stage
	,Sequence.Pattern
	,ReportSequenceNo
	,C1Result	
	,RatioDescription	
	,ResultForRatio	
	,RVRatio	
	,NationalAlert
	,InHospitalAKI
	,ResultWardCode
	,ResultAPCLinked
from
	(
	Select
		ResultRecno
		,Result.PatientForename
		,Result.PatientSurname
		,Result.DistrictNo
		,Result.CasenoteNumber
		,AgeCode = Age.Age
		,Ethnicity = EthnicOrigin.NationalEthnicOrigin
		,Sex = Result.SexCode
		,Result.LatestAdmissionDate
		,Result.LatestPatientCategory 
		,LatestWardCode = Result.WardCode
		,Consultant.Consultant
		,Specialty.Specialty
		,Result.Result
		,Result.ResultDate
		,Result.ResultTime
		,SequenceNoPatient
		,SequenceNoSpell
		,Result.PreviousResult
		,Result.PreviousResultTime
		--,Result.ResultIntervalDays
		--,Result.ResultChange
		--,Result.CurrentToPreviousResultRatio

		,Result.BaselineResult
		,Result.BaselineResultTime
		,Result.BaselineToCurrentDays
		,Result.BaselineToCurrentChange
		,Result.CurrentToBaselineResultRatio

		--,Result.ResultRateOfChangePer24Hr

		,Result.CurrentInpatient
		,Result.LengthOfStay
		,Result.DiedInHospital
		--,Result.[PreviousSpell(12mths)WithRenalEpisode]
		,ReportSequenceNo =
			case
			when @ReportType = 'RATE' then row_number () over (partition by Result.DistrictNo order by Result.ResultRateOfChangePer24Hr desc)
			when @ReportType = 'CHANGE' then  row_number () over (partition by Result.DistrictNo,Result.MergeEpisodeNo order by Result.CurrentToBaselineResultRatio desc)
			when @ReportType = 'LATEST' then  row_number () over (partition by Result.DistrictNo order by Result.ResultTime desc)
			when @ReportType = 'VALUE' then  row_number () over (partition by Result.DistrictNo,Result.MergeEpisodeNo order by Result.Result desc)
			end
		
		,Result.Stage
		
		,C1Result	
		,RatioDescription	
		,ResultForRatio	
		,RVRatio	
		,NationalAlert
		,InHospitalAKI
		,ResultWardCode
		,Result.ResultAPCLinked
	FROM
		OCM.CodedResult Result

	left join PAS.Consultant
	on	Consultant.ConsultantCode = Result.ConsultantCode

	left join PAS.Specialty
	on	Specialty.SpecialtyCode = Result.SpecialtyCode

	left join PAS.EthnicOrigin
	ON	EthnicOrigin.EthnicOriginCode = Result.EthnicOriginCode

	left join WH.Age
	on	Age.AgeCode = Result.AgeCode collate Latin1_General_CI_AS
	) 
	Result

left join
	(
	select 
		A.SourcePatientNo
		,A.ResultRecno
		,A.ResultTime
		,Pattern =
			case
				when B.Ratio is null then 50
				when A.Ratio > B.Ratio then 75
				when A.Ratio = B.Ratio then 50
				else 25
			end
	from
		(
		select
			SourcePatientNo
			,ResultRecno
			,ResultDate
			,ResultTime
			,Ratio = CurrentToBaselineResultRatio
			,Sequence = row_number () over (partition by DistrictNo order by ResultTime desc)
		from
			OCM.CodedResult
		where ResultDate < @ToDate
		) 
		A
	left join
		(
		select
			SourcePatientNo
			,ResultRecno
			,ResultDate
			,ResultTime
			,Ratio = CurrentToBaselineResultRatio
			,Sequence = row_number () over (partition by DistrictNo order by ResultTime desc)
		from
			OCM.CodedResult
		where ResultDate < @ToDate
		) 
		B
	on A.SourcePatientNo = B.SourcePatientNo
	where 
		A.Sequence = 1
	and B.Sequence = 2
	) 
	Sequence
on Sequence.ResultRecno = Result.ResultRecno

WHERE 
	Result.ResultDate between @FromDate and @ToDate
and	
	(
		Result.CurrentInpatient = @CurrentInpatient
	or	
		@CurrentInpatient is null
	)
and	
	(
		Result.LatestPatientCategory = @PatientCategory
	or	
		@PatientCategory is null
	)
and 
	(
		Result.NationalAlert in (Select Value from RPT.SSRS_MultiValueParamSplit(@NationalAlert,','))
	or 
		Result.NationalAlert is null
	)
and	
	(
		(
			@ReportType = 'CHANGE'
		and 
			(
				Result.Stage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
			or
				Result.Stage is null
			)
		and	Result.ReportSequenceNo = @Summary
		)
	or	
		(
			@ReportType = 'Latest'
		and	
			(
				Result.Stage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
			or
			Result.Stage is null
			)
		and	Result.ReportSequenceNo = @Summary
		)
	or	
		(
			@ReportType = 'VALUE'
		and	Result.Result >= @ResultValue
		and	Result.ReportSequenceNo = @Summary
		)
	)

ORDER BY
	 Result.DistrictNo
	,Result.ResultTime desc
	

--WHERE 
--	(
--		Result.ResultDate between @FromDate and @ToDate

--	--removed as per Prasanna's instructions 2013-11-29
--	--and	Result.AdmissionDate between @FromDate and @ToDate

--	and	(
--			Result.CurrentInpatient = @CurrentInpatient
--		or	@CurrentInpatient is null
--		)
--	and	(
--			--(
--			--	@ReportType = 'RATE'
--			--and	Result.ResultRateOfChangePer24Hr >= @RateOfChange
--			--and	Result.ReportSequenceNo = @Summary
--			--and Result.ResultIntervalDays >= @Interval
--			--)
--		--or	(
--		--		@ReportType = 'CHANGE'
--		--	and	Result.CurrentToPreviousResultRatio >= @PercentageChange
--		--	and	Result.ReportSequenceNo = @Summary
--		--	and Result.ResultIntervalDays >= @Interval
--		--	)
--		--or	
--			(
--				@ReportType = 'CHANGE'
--			--20140811 RR PH asked to select cohorts so added in ToPercentageChange
--			--20140905 RR after meeting with PH took this back, but he wants something different
--			--V1
--			--and Result.CurrentToBaselineResultRatio >= @PercentageChange
--			-- V2
--			--and 
--			--	(Result.CurrentToBaselineResultRatio between @FromPercentageChange and @ToPercentageChange
--			--	or
--			--		(
--			--		Result.CurrentToBaselineResultRatio >= @FromPercentageChange 
--			--		and @ToPercentageChange is null
--			--		)
--			--	)
			
--			-- 20140905 RR PH asked for it to be categorised into Stages
--			-- V3
--			and (
--				Result.Stage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
--				or
--				Result.Stage is null
--				)
			
--			and	Result.ReportSequenceNo = @Summary
--			--and Result.BaselineToCurrentDays >= @Interval
--			)
--		or	(
--				@ReportType = 'VALUE'
--			and	Result.Result >= @ResultValue
--			and	Result.ReportSequenceNo = @Summary
--			)
--		)
--	and	(
--			Result.PatientCategoryCode = @PatientCategory
--		or	@PatientCategory is null
--		)
--	)

----	-- drillthrough settings
----or
----	(
----		@Summary is null
----	and	(
----			Result.DistrictNo = @DistrictNo
----		or	@DistrictNo is null
----		)
----	)





﻿


















CREATE PROCEDURE [RPT].[GETClinicAttendancesStatasticsByClinicGroup] 
     @ReportGroupCode   varchar(50)
	,@GroupClinicCode   varchar(50)
	,@FromDate          Date
	,@ToDate            Date

AS 
--DECLARE @ReportGroupCode    AS varchar(50)   = 'Medicine'
--DECLARE @GroupClinicCode	AS	varchar(50)  = 'Gen Paeds'
--DECLARE @FromDate           AS DATE          = '05-JAN-2015'
--DECLARE @ToDate             AS DATE          = '10-JAN-2015'


SELECT DISTINCT
     ClinicAttendanceStatisticsClinicGroups.ClinicGroup
    ,Clinic.ClinicCode
    ,[Diary].[SessionCode]
    ,[Diary].[DoctorCode]
	,Clinic.Clinic
    ,DATENAME(dw, Diary.[SessionDate])                       AS DayOfClinic 
    ,CONVERT(VARCHAR(10), Diary.[SessionStartTime], 103)     AS ClinicStartDate
    ,CONVERT(VARCHAR(5), Diary.[SessionStartTime], 108)      AS ClinicStartTime   
    ,Diary.[SessionStartTime]                                AS ClinicDateFormat
	,Diary.[SessionPeriod]                                   AS AMPM
    ,Diary.[Units]
    ,Diary.[UsedUnits]
    ,Diary.[FreeUnits]
	,CONVERT(VARCHAR(10), Encounter.[AppointmentTime], 103)  AS AppointmentDate
	,CONVERT(VARCHAR(5) , Encounter.[AppointmentTime], 108)  AS AppointmentTime
	,Encounter.EncounterRecno
	,Encounter.NHSNumber
	,Encounter.CasenoteNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,CONVERT(VARCHAR(10) , Encounter.[DateOfBirth] , 103)    AS DoB
	,Encounter.Postcode
	,Encounter.DNACode
	,Encounter.AppointmentStatusCode                         AS PatientOutcomeCode
	--,Encounter.[CancelledByCode]
	,PatientOutcomeDescription = 
		CASE 
		WHEN Encounter.AppointmentStatusCode = 'ATT'  THEN 'Attended'
		WHEN Encounter.AppointmentStatusCode = 'ATTP' THEN 'Attended'
		WHEN Encounter.AppointmentStatusCode = 'H'    THEN 'Hospital Cancellation'
		WHEN Encounter.AppointmentStatusCode = 'P'    THEN 'Patient Cancellation'
		WHEN Encounter.AppointmentStatusCode = 'CND'  THEN 'CND'
		WHEN Encounter.AppointmentStatusCode = 'NATT' THEN 'Did Not Attend'
		WHEN Encounter.AppointmentStatusCode = 'DNA'  THEN 'Did Not Attend'
		WHEN Encounter.AppointmentStatusCode IS NULL  THEN 'Empty Clinic'
		WHEN Encounter.AppointmentStatusCode = 'NR'   THEN
		   CASE Encounter.[CancelledByCode]
		   WHEN 'P' THEN 'Patient Cancellation' 
		   WHEN 'H' THEN 'Hospital Cancellation'
		   ELSE 'Not Recorded'
		   END
		ELSE 'Not Recorded'       
		END
	,AttendYes = 
		CASE Encounter.DNACode
		WHEN 5 THEN 1
		ELSE 0
		END   
	,AttendNo = 
		CASE 
		WHEN  Encounter.DNACode = 5 OR Encounter.DNACode IS NULL THEN 0
		ELSE 1
		END 
	,Encounter.AppointmentTypeCode
	,Cancelled = 
	    CASE 
	    WHEN  Diary.[ReasonForCancellation] IS NULL THEN 'No' 
	    WHEN (Diary.[ReasonForCancellation] IS NOT NULL AND Diary.[UsedUnits] > 0) THEN 'No'
	    ELSE 'Yes'
	    END
	,Diary.[ReasonForCancellation]                           AS ClinicCancellationCode

FROM Warehousesql.Warehouse.[OP].[Diary]

JOIN WarehouseReporting.dbo.ClinicAttendanceStatisticsClinicGroups
  ON[Diary].[SessionCode] = ClinicAttendanceStatisticsClinicGroups.[SessionCode]
JOIN Warehousesql.WarehouseReporting.PAS.Clinic 
  ON Diary.[ClinicCode] = Clinic.ClinicCode  
LEFT OUTER JOIN Warehousesql.WarehouseReportingMerged.OP.Encounter 
  ON   (Diary.[ClinicCode] = Encounter.ClinicCode 
        AND [Encounter].[DoctorCode] = [Diary].[DoctorCode]
        AND Diary.[SessionDate] = Encounter.AppointmentDate)

WHERE 
	 Diary.[SessionDate] BETWEEN @FromDate AND @ToDate
 AND ClinicAttendanceStatisticsClinicGroups.[ReportGroup] = @ReportGroupCode	 
 --AND ClinicAttendanceStatisticsClinicGroups.[ClinicGroup] = @GroupClinicCode 
 
  AND (
        ClinicAttendanceStatisticsClinicGroups.[ClinicGroup] = @GroupClinicCode  
     OR 
         (    @GroupClinicCode  = 'All Clinic Groups'
          AND ClinicAttendanceStatisticsClinicGroups.[ReportGroup] = @ReportGroupCode
         )
       )
 
 AND ClinicAttendanceStatisticsClinicGroups.[LDF] = 'N'
 
 














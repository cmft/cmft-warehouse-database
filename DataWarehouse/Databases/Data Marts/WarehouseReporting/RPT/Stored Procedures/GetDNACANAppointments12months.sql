﻿
CREATE PROCEDURE [RPT].[GetDNACANAppointments12months]
(

	 @StartAppointmentDate Date, 
	@EndAppointmentDate Date,
	@Division varchar(MAX) = '(Select All)'
	,@Specialty varchar(MAX)= '000'
)

AS

--DECLARE @StartAppointmentDate Date = '1 apr 2011'
--DECLARE	@EndAppointmentDate Date = '1 may 2011'
--DECLARE	@Division varchar(MAX) = 'Childrens'
--DECLARE @Specialty varchar(MAX)= '171'


SELECT
tbldata.*
,SUM(AttPlusDNA) OVER(PARTITION BY TheMonth) AS 'DomTotDNAMonthTotal'
,SUM(NewAttDNA) OVER(PARTITION BY TheMonth) AS 'DomNewDNAMonthTotal'
,SUM(FolAttDNA) OVER(PARTITION BY TheMonth) AS 'DomFolDNAMonthTotal'

FROM
(
SELECT  
   	  Division =  case	
			when @Division = '(Select All)' then '(Select All)'
			else Dir.Division
		end		
   	 ,NationalReferringSpecialtyCode = 
   	 case 
   		when @Specialty = '000' then '000'
   		else RefSpec.NationalSpecialtyCode
   	  end
   	
   	  ,NationalSpecialty = case 
   		when @Specialty = '000' then '(Select All)'
   		else NationalSpecialty
   	  end
   	 
   	  --,COALESCE([NationalSpecialty],@Specialty) AS "NationalSpecialty"
   	  ,AttendanceType =
						case
							when OPAct.FirstAttendanceFlag = 1 then 'New'
							else 'Follow Up'
						end
      ,FinancialYear = FinancialYear
      ,FinancialMonthKey = FinancialMonthKey
      ,TheMonth = TheMonth
      
	, ordertype = Case 
					when OPAct.AttendanceOutcomeCode = 'DNA' then '2'
					when OPAct.AttendanceOutcomeCode = 'CND' then '4'
					when OPAct.DNACode = 4 then '3'
					when OPAct.DNACode = 2 and OPAct.CancelledByCode = 'P' then '5'
					when OPAct.DNACode in (5,6) then 1
				else '6'
					
				end				
				
      ,ActType = Case 
					when OPAct.AttendanceOutcomeCode = 'DNA' then 'DNA' 
					when OPAct.AttendanceOutcomeCode = 'CND' then 'Patient Cancelled on Day'
					when OPAct.DNACode = 4 then 'Hospital Cancellelation'
					when OPAct.DNACode = 2 and OPAct.CancelledByCode = 'P' then 'Patient Cancellelation'
					when OPAct.DNACode in (5,6) then 'Attendances'
				else 'Unknown Outcome'
					
				end
      ,Activity = COUNT(SourceUniqueID)
      ,AttPlusDNA = SUM(	Case
								when OPAct.AttendanceOutcomeCode = 'DNA' then 1
								when OPAct.DNACode in (5,6) then 1
							else 0
							end)
	
	,NewAttDNA = SUM( case
								when ( OPAct.DNACode in (3,5,6) and OPAct.FirstAttendanceFlag = 1)
							then 1
							else 0
						end)
						
	,FolAttDNA = SUM( case
								when (OPAct.DNACode in (3,5,6) and OPAct.FirstAttendanceFlag <> 1)
							then 1
							else 0
						end)
						
	,TOTCanPat = 0
	,TOTCanHos = 0
	,NewCanPat = 0
	,NewCanHos = 0
	,FolCanPat = 0
	,FolCanHos = 0
	
	
									
FROM 
	WarehouseReporting.OP.Encounter AS OPAct
INNER JOIN WarehouseReporting.PAS.Specialty AS RefSpec 
	ON OPAct.ReferringSpecialtyCode = RefSpec.SpecialtyCode
INNER JOIN WarehouseReporting.PAS.Directorate as Dir 
	on OPAct.DirectorateCode = Dir.DirectorateCode
INNER join WarehouseReporting.WH.Calendar as Cal 
	on OPAct.AppointmentDate  = Cal.TheDate
WHERE	
	AppointmentDate >=  @StartAppointmentDate
	and AppointmentDate <= @EndAppointmentDate
	and 
	(
		Division = @Division
	or	@Division = '(Select All)'
	)
and 
	(
		RefSpec.NationalSpecialtyCode = @Specialty
	Or	@Specialty = '000'
	)
	 	
	and DNACode in (2,3,4,5,6)
	and ReferringSpecialtyTypeCode = 'I'
	and AdminCategoryCode <> '02'
	and IsWardAttender = 0
Group By
	 
	  Case when @Division = '(Select All)'
		then '(Select All)'
		else Dir.Division
   	end
   	 ,case 
   		when @Specialty = '000' then '000'
   		else RefSpec.NationalSpecialtyCode
   	  end
   	  ,case 
   		when @Specialty = '000' then '(Select All)'
   		else NationalSpecialty
   	  end
	,case
		when FirstAttendanceFlag = 1 then 'New'
		else 'Follow Up'
	end
	
	,Case 
					when OPAct.AttendanceOutcomeCode = 'DNA' then '2'
					when OPAct.AttendanceOutcomeCode = 'CND' then '4'
					when OPAct.DNACode = 4 then '3'
					when OPAct.DNACode = 2 and OPAct.CancelledByCode = 'P' then '5'
					when OPAct.DNACode in (5,6) then 1
				else '6'
					
				end				
				
				
	,Case 
		when AttendanceOutcomeCode = 'DNA' then 'DNA' 
		when AttendanceOutcomeCode = 'CND' then 'Patient Cancelled on Day'
		when DNACode = 4 then 'Hospital Cancellelation'
		when DNACode = 2 and CancelledByCode = 'P' then 'Patient Cancellelation'
		when DNACode in (5,6) then 'Attendances'
	else 'Unknown Outcome'
				
	end
	,CONVERT(char(8),Cal.FirstDayOfWeek,3)
	,FinancialYear
	,FinancialMonthKey
	,TheMonth
	
	UNION all
	
	SELECT  
   	    Division =  case	
			when @Division = '(Select All)' then '(Select All)'
			else Dir.Division
		end		
   	 ,NationalReferringSpecialtyCode = 
   	 case 
   		when @Specialty = '000' then '000'
   		else RefSpec.NationalSpecialtyCode
   	  end
   	
   	  ,NationalSpecialty = case 
   		when @Specialty = '000' then '(Select All)'
   		else NationalSpecialty
   	  end
   	  ,AttendanceType =
						case
							when OPAct.FirstAttendanceFlag = 1 then 'New'
							else 'Follow Up'
						end
      ,FinancialYear = FinancialYear
      ,FinancialMonthKey = FinancialMonthKey
      ,TheMonth = TheMonth
      
		,ordertype = Case 
					
					when OPAct.DNACode = 4 then '7'
					else '8'
					
					end			
						
      ,ActType = Case 
					
					when OPAct.DNACode = 4 then 'Avg.No. Days before appointment(Hosp)'
					else 'Avg.No. Days before appointment(Pat)'
					
					end
      ,Activity = SUM( case 
							When CancelledByCode = 'P'
							then datediff(day,[AppointmentCancelDate],[AppointmentDate])
							
							else datediff(day,[AppointmentCancelDate],[AppointmentDate])
							
						end)
    
    ,AttPlusDNA = 0
	,NewAttDNA = 0
	,FolAttDNA = 0
	,TOTCanPat = SUM( case
								when (OPAct.DNACode = 2 )
							then 1
							else 0
						end)
	,TOTCanHos = SUM( case
								when (OPAct.DNACode = 4 )
							then 1
							else 0
						end)
						
	,NewCanPat = SUM( case
								when (OPAct.DNACode = 2 and FirstAttendanceFlag = 1 )
							then 1
							else 0
						end)
	,NewCanHos = SUM( case
								when (OPAct.DNACode = 4 and FirstAttendanceFlag = 1 )
							then 1
							else 0
						end)
	,FolCanPat = SUM( case
								when (OPAct.DNACode = 2 and FirstAttendanceFlag <> 1 )
							then 1
							else 0
						end)
	,FolCanHos = SUM( case
								when (OPAct.DNACode = 4 and FirstAttendanceFlag <> 1 )
							then 1
							else 0
						end)
														
											
									
FROM 
	WarehouseReporting.OP.Encounter AS OPAct
INNER JOIN WarehouseReporting.PAS.Specialty AS RefSpec 
	ON OPAct.ReferringSpecialtyCode = RefSpec.SpecialtyCode
INNER JOIN WarehouseReporting.PAS.Directorate as Dir 
	on OPAct.DirectorateCode = Dir.DirectorateCode
INNER join WarehouseReporting.WH.Calendar as Cal 
	on OPAct.AppointmentDate  = Cal.TheDate
WHERE	
	AppointmentDate >=  @StartAppointmentDate
	and AppointmentDate <= @EndAppointmentDate
	and 
	(
		Division = @Division
	or	@Division = '(Select All)'
	)
	and 
	(
		RefSpec.NationalSpecialtyCode = @Specialty
	Or	@Specialty = '000'
	)
	 	
	and DNACode in (2,4)
	and ReferringSpecialtyTypeCode = 'I'
	and AdminCategoryCode <> '02'
	and IsWardAttender = 0
Group By
	  Case when @Division = '(Select All)'
		then '(Select All)'
		else Dir.Division
   	end
   	 ,case 
   		when @Specialty = '000' then '000'
   		else RefSpec.NationalSpecialtyCode
   	  end
   	  ,case 
   		when @Specialty = '000' then '(Select All)'
   		else NationalSpecialty
   	  end
	,case
		when FirstAttendanceFlag = 1 then 'New'
		else 'Follow Up'
	end
	
	,Case 
					
				when OPAct.DNACode = 4 then '7'
				else '8'
					
		end	
				
	,Case 
					
					when OPAct.DNACode = 4 then 'Avg.No. Days before appointment(Hosp)'
					else 'Avg.No. Days before appointment(Pat)'
					
					end
	,CONVERT(char(8),Cal.FirstDayOfWeek,3)
	,FinancialYear
	,FinancialMonthKey
	,TheMonth
	
	
)tblData



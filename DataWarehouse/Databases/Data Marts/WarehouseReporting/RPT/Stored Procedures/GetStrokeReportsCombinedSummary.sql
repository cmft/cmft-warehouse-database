﻿

CREATE proc [RPT].[GetStrokeReportsCombinedSummary]

@FinancialMonthKey INT

as

--need to work out which Financial Year so that the Chart only brings back data for currently selected Financial Year
Declare @ChartStartDate date
Declare @ChartEndDate date
select 
	@ChartStartDate = '04/01/' + left(FinancialMonthKey,4)
from 
	wh.calendar
where
	FinancialMonthKey = @FinancialMonthKey

select 
	@ChartEndDate = dateadd(day,-1,DATEADD(year,1,@ChartStartDate))

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

DECLARE @StrokeTable TABLE 
(	
	InternalNo  varchar(20)
	,EpisodeNo varchar(20)
	,PrimaryDiagnosisIndicator  varchar(3)
	,OtherDiagnosisIndicator   varchar(3)
	,CasenoteNumber varchar(20)
	,PatientForename varchar(20)
	,PatientSurname varchar(30)
	,AdmissionTime smalldatetime
	,DischargeTime smalldatetime
	,Numerator int
	,DenominatorIncAE int
	,PercentageOnStrokeWardIncAE float
	,TheMonth nvarchar(34)
	,AppointmentDate datetime
	,AttendanceOutcomeCode varchar(5)
	,DNACount int
	,ArrivalTime smalldatetime
	,DepartureTime smalldatetime
	,FirstAdmissionWard varchar(10)
	,DateOfBirth datetime
	,DateOfDeath smalldatetime
	,LOS smallint
	,AttendedIn6Weeks varchar(1)
)	


insert into @StrokeTable
(
	InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator
	,OtherDiagnosisIndicator
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,AdmissionTime
	,DischargeTime
	,Numerator
	,DenominatorIncAE
	,PercentageOnStrokeWardIncAE
	,TheMonth
	,DateOfBirth
	,DateOfDeath
	,LOS

)


SELECT
	InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator =	CASE 
										WHEN DiagnosisIndicator = 'Both' then 'Yes' 
										WHEN DiagnosisIndicator = 'Primary' then 'Yes'
									ELSE 'No' END
	,OtherDiagnosisIndicator =		CASE 
										WHEN DiagnosisIndicator = 'Both' then 'Yes'
										WHEN DiagnosisIndicator = 'Secondary' then 'Yes' 
									ELSE 'No' end
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,AdmissionTime
	,DischargeTime
	,Numerator = LOSStrokeWard
	,DenominatorIncAE = Denominator + ISNULL(AETime,0)
	,PercentageOnStrokeWardIncAE = ((LOSStrokeWard*1.00)/(Denominator + ISNULL(AETime,0))*1.00)*100
	,myqry.TheMonth
	,DateOfBirth
	,DateOfDeath
	,LOS

	
FROM (
					SELECT 
						InternalNo = Encounter.SourcePatientNo
						,EpisodeNo = Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard = ISNULL(LOSStrokeWard,0)
						,Denominator = SUM(DATEDIFF(mi,Encounter.EpisodeStartTime,Encounter.EpisodeEndTime))
						,AETime = TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth
						,Encounter.DateOfBirth
						,Encounter.DateOfDeath
						,Encounter.LOS


					FROM 
						WarehouseReporting.apc.Encounter Encounter

					LEFT JOIN Information.dbo.PCT PCT 
					ON PCT.[PCT Code] = Encounter.PCTCode

					--LOS on Stroke Ward
					LEFT JOIN (		SELECT 
										ProviderSpellNo,
										LOSStrokeWard = SUM(DATEDIFF(mi,WardStay.[StartTime],WardStay.EndTime)) 
									FROM 
										[WarehouseReporting].[APC].[WardStay] 
									WHERE 
										[WardCode] in ('30M','31M','31R') 
									GROUP BY ProviderSpellNo) WardStay 
					ON WardStay.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

					--LOS in AE
					LEFT JOIN (	
									SELECT distinct
										APCEncounter.DistrictNo,
										APCEncounter.AdmissionTime,
										TimeSpentInAE = StageDurationMinutes
									FROM
										WarehouseReporting.APC.Encounter APCEncounter

									INNER JOIN WarehouseReporting.AE.Encounter AEEncounter
									ON AEEncounter.DistrictNo = APCEncounter.DistrictNo
									and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR,-2,APCEncounter.AdmissionTime) and DATEADD(HOUR,+2,APCEncounter.AdmissionTime)

									INNER JOIN WarehouseReporting.AE.Fact Fact 
									ON Fact.SourceUniqueID = AEEncounter.SourceUniqueID

									WHERE Fact.StageCode = 'INDEPARTMENTADJUSTED') LOSAE 	
					ON LOSAE.DistrictNo = Encounter.DistrictNo
					AND LOSAE.AdmissionTime = Encounter.AdmissionTime
					
					--only bring back stroke patients
					INNER JOIN (	
								SELECT 
									SourcePatientNo
									,SourceSpellNo
									,DiagnosisIndicator = CASE 
										WHEN [Primary] is not null AND [Secondary] is not null then 'Both'
										WHEN [Primary] is not null then 'Primary'
										WHEN [Secondary] is not null then 'Secondary'
									ELSE
										NULL
									END
								FROM (
										--pivot data so we can compare diagnosis columns as a primary and secondary can be the same
										SELECT
											SourcePatientNo
											,SourceSpellNo
											,[Primary]
											,[Secondary]
										FROM
												(SELECT 
													SourcePatientNo,SourceSpellNo ,DiagnosisIndicator = 'Primary'
												FROM 
													WarehouseReporting.apc.Encounter Encounter
												WHERE 
													(PrimaryDiagnosisCode like 'I61%' or PrimaryDiagnosisCode like 'I63%' or PrimaryDiagnosisCode like 'I64%' or PrimaryDiagnosisCode like 'I62%' or PrimaryDiagnosisCode like 'H34%')

												UNION	
										
												SELECT 
													Encounter.SourcePatientNo,Encounter.SourceSpellNo,DiagnosisIndicator = 'Secondary'
												FROM
													WarehouseReporting.apc.Encounter Encounter
												INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
																ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
																AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
																AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
																AND (Diagnosis.DiagnosisCode like 'I61%'  or Diagnosis.DiagnosisCode like 'I63%' or Diagnosis.DiagnosisCode like 'I64%' or Diagnosis.DiagnosisCode like 'I62%'  or Diagnosis.DiagnosisCode like 'H34%')										
												)s
										PIVOT
										(
										max(DiagnosisIndicator)
										FOR DiagnosisIndicator IN ([Primary],[Secondary])
										) as pvt
								)qry
					) StrokePatients
					ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
					AND StrokePatients.SourceSpellNo = Encounter.SourceSpellNo
					
					INNER JOIN WarehouseReporting.WH.Calendar Calendar
					ON	Calendar.TheDate = Encounter.DischargeDate

					WHERE
						Calendar.TheDate between @ChartStartDate and @ChartEndDate
					AND CASE 
							WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionDate)))))-1900)<19
							THEN 'C'
							ELSE 'A'
						END = 'A'
										

					GROUP BY 
						Encounter.SourcePatientNo
						,Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard
						,TimeSpentInAE
						,Calendar.TheMonth
						,Encounter.DateOfBirth
						,Encounter.DateOfDeath
						,Encounter.LOS
						,StrokePatients.DiagnosisIndicator


)myqry

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
------------------------------------------  Now update Table Variable  -------------------------------------

--1. update DNA Count for a patients spell

DECLARE @DNACount Table
(	
	DistrictNo varchar(20)
	,SourcePatientNo  varchar(20)
	,SourceSpellNo  varchar(20)
	,DNA int
)

insert into @DNACount(DistrictNo,SourcePatientNo,SourceSpellNo,DNA)
select
	DistrictNo
	,SourcePatientNo
	,SourceSpellNo
	,DNA = Max(RowInd)
from (
		select 
			OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.SourceSpellNo
			,OpEncounter.AppointmentTime
			,RowInd = row_number() over (partition by OpEncounter.DistrictNo,Encounter.SourceSpellNo order by OpEncounter.DistrictNo,Encounter.SourceSpellNo, OpEncounter.AppointmentTime)

		from
			[OP].[Encounter] OpEncounter
		left join
			[APC].Encounter Encounter
		on Encounter.DistrictNo = OpEncounter.DistrictNo
		--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
		AND OpEncounter.AppointmentTime >= Encounter.DischargeTime
		where 
			Encounter.SourceSpellNo is not null
		AND OpEncounter.AttendanceOutcomeCode = ('DNA')
		AND Encounter.DischargeDate between @ChartStartDate and @ChartEndDate
		group by
			OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.SourceSpellNo
			,OpEncounter.AppointmentTime
	)myqry

group by
	DistrictNo
	,SourcePatientNo
	,SourceSpellNo

--now update Table Variable Stroke
update s
set 
	DNACount = fa.DNA
from
	@StrokeTable s
inner join 	@DNACount fa
on fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
----2.
update s
set 
	DNACount = 0
from
	@StrokeTable s
where
	DNACount is null
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
----Rule3.1: Show details of first attendance (but not if the DNA count is > 1)

	DECLARE @FirstAttendance Table
	(	
		DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,SourceSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @FirstAttendance(DistrictNo,SourcePatientNo,SourceSpellNo,AppointmentTime,AttendanceOutcomeCode,RowInd)

		select
			DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd
		from
		(	select 
				OpEncounter.DistrictNo
				,Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
				,OpEncounter.AppointmentTime
				,AttendanceOutcomeCode
				,RowInd = row_number() over (partition by OpEncounter.DistrictNo,Encounter.SourceSpellNo order by OpEncounter.DistrictNo,Encounter.SourceSpellNo, OpEncounter.AppointmentTime)
			from
				[OP].[Encounter] OpEncounter
			left join
				[APC].Encounter Encounter
			on Encounter.DistrictNo = OpEncounter.DistrictNo
		--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
		AND OpEncounter.AppointmentTime >= Encounter.DischargeTime
			where 
				Encounter.SourceSpellNo is not null
				and AttendanceOutcomeCode in ('ATT','SATT','WLK')
				AND Encounter.DischargeDate between @ChartStartDate and @ChartEndDate
			group by
				OpEncounter.DistrictNo
				,Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
				,OpEncounter.AppointmentTime
				,AttendanceOutcomeCode
		)myqry
		where
			RowInd = 1

--now update Table Variable Stroke
update s
set 
	AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode

from
	@StrokeTable s
inner join @FirstAttendance fa
on fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo

where DNACount<2

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
----Rule3.2: If DNA count is > 1 then show 2nd DNA details
	DECLARE @SecondDNADetails Table
	(	
		DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,SourceSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @SecondDNADetails(DistrictNo,SourcePatientNo,SourceSpellNo,AppointmentTime,AttendanceOutcomeCode,RowInd)

		select
			DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd
		from (
				select 
					OpEncounter.DistrictNo
					,Encounter.SourcePatientNo
					,Encounter.SourceSpellNo
					,OpEncounter.AppointmentTime
					,AttendanceOutcomeCode
					,RowInd = row_number() over (partition by OpEncounter.DistrictNo,Encounter.SourceSpellNo order by OpEncounter.DistrictNo,Encounter.SourceSpellNo, OpEncounter.AppointmentTime)

				from
					[OP].[Encounter] OpEncounter
				left join
					[APC].Encounter Encounter
				on Encounter.DistrictNo = OpEncounter.DistrictNo
				--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
				AND OpEncounter.AppointmentTime >= Encounter.DischargeTime
				where 
					Encounter.SourceSpellNo is not null
				AND OpEncounter.AttendanceOutcomeCode = ('DNA')
				AND Encounter.DischargeDate between @ChartStartDate and @ChartEndDate
				group by
					OpEncounter.DistrictNo
					,Encounter.SourcePatientNo
					,Encounter.SourceSpellNo
					,OpEncounter.AppointmentTime
					,AttendanceOutcomeCode
			)myqry

		where 
			RowInd = 2
		group by 	
			DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,AppointmentTime
			,RowInd

--now update Table Variable Stroke
update s
set 
	AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode

from
	@StrokeTable s

inner join @SecondDNADetails fa
on fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo

Where
	DNACount>1


------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
----Rule3.3: If there has been only 1 DNA and no attendance then show the first DNA
	DECLARE @FirstDNADetails Table
	(	
		DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,SourceSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @FirstDNADetails(DistrictNo,SourcePatientNo,SourceSpellNo,AppointmentTime,AttendanceOutcomeCode,RowInd)

		select
			DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd
		from (
				select 
					OpEncounter.DistrictNo
					,Encounter.SourcePatientNo
					,Encounter.SourceSpellNo
					,OpEncounter.AppointmentTime
					,AttendanceOutcomeCode
					,RowInd = row_number() over (partition by OpEncounter.DistrictNo,Encounter.SourceSpellNo order by OpEncounter.DistrictNo,Encounter.SourceSpellNo, OpEncounter.AppointmentTime)

				from
					[OP].[Encounter] OpEncounter
				left join
					[APC].Encounter Encounter
				on Encounter.DistrictNo = OpEncounter.DistrictNo
				--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
				AND OpEncounter.AppointmentTime >= Encounter.DischargeTime
				where 
					Encounter.SourceSpellNo is not null
				AND OpEncounter.AttendanceOutcomeCode = ('DNA')
				AND Encounter.DischargeDate between @ChartStartDate and @ChartEndDate
				group by
					OpEncounter.DistrictNo
					,Encounter.SourcePatientNo
					,Encounter.SourceSpellNo
					,OpEncounter.AppointmentTime
					,AttendanceOutcomeCode
			)myqry

		where 
			RowInd = 1
		group by 	
			DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd

--now update Table Variable Stroke
update s
set 
	AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode

from
	@StrokeTable s

inner join @FirstDNADetails fa
on fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo

Where
	DNACount=1
AND AppointmentDate is null


------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
--Rule 4 - Get First Episode Admission Ward
--we need to get the minimum admission date as the WardStay query brings back too much data

declare @MinAdmissionDate smalldatetime
select @MinAdmissionDate = MIN(AdmissionTime) from @StrokeTable

DECLARE @FirstEpisodeWardTable TABLE 
(	SourcePatientNo  varchar(20)
	,SourceSpellNo varchar(20)
	,WardCode varchar(10)
)
	INSERT INTO @FirstEpisodeWardTable(SourcePatientNo,SourceSpellNo,WardCode)
	select
		SourcePatientNo
		,SourceSpellNo
		,WardCode

	from (
			select 
				SourcePatientNo
				,SourceSpellNo
				,WardCode
				,RowInd = row_number() over (partition by SourcePatientNo,SourceSpellNo order by StartTime asc)
			from apc.WardStay
			where StartTime >=@MinAdmissionDate
		)myqry
	where
		RowInd = 1

update s
set FirstAdmissionWard = FirstEpisodeWard.WardCode

from
	@StrokeTable s
inner join @FirstEpisodeWardTable FirstEpisodeWard
on FirstEpisodeWard.SourcePatientNo = s.InternalNo
AND FirstEpisodeWard.SourceSpellNo = s.EpisodeNo

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
--5.  Add an indicator for whether the patient attended an appointment within 6 weeks

update s
set AttendedIn6Weeks = 
	case 
		when DATEDIFF(hour,DischargeTime,AppointmentDate)/168 < 6 
	then 'Y'
	else 'N' end
from
	@StrokeTable s
where
	s.AppointmentDate is not null
AND s.AttendanceOutcomeCode in ('ATT','SATT','WLK')


	
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
--Now bring back data to the report :o)
select 
	InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator
	,OtherDiagnosisIndicator
	,CasenoteNumber 
	,PatientForename
	,PatientSurname
	,AdmissionTime
	,DischargeTime
	,Numerator
	,DenominatorIncAE 
	,PercentageOnStrokeWardIncAE 
	,TheMonth 
	,AppointmentDate 
	,AttendanceOutcomeCode
	,DNACount 
	,FirstAdmissionWard
	,DateOfBirth
	,DateOfDeath
	,LOS
	,PatientCount = count(InternalNo) over (partition by TheMonth)
	,AttendedCount = sum(CASE WHEN AttendedIn6Weeks = 'Y' then 1 else 0 end) over (partition by TheMonth)
	,PatientCountOnStrokeWard = sum(CASE WHEN Numerator > 0 then 1 else 0 end) over (partition by TheMonth)
	,AttendedCountOnStrokeWard = sum(CASE WHEN AttendedIn6Weeks = 'Y' AND Numerator<>0 then 1 else 0 end) over (partition by TheMonth)
	,PatientCountNotOnStrokeWard = sum(CASE WHEN Numerator = 0 then 1 else 0 end) over (partition by TheMonth)
	,AttendedCountNotOnStrokeWard = sum(CASE WHEN AttendedIn6Weeks = 'Y' AND Numerator=0 then 1 else 0 end) over (partition by TheMonth)
	,ActualLOS = sum(DATEDIFF(mi,AdmissionTime,DischargeTime)) over (partition by TheMonth)
	,TotalTimeOnStrokeWard = sum(Numerator)over (partition by TheMonth)
	,AttendedTotalTimeOnStrokeWard = sum(CASE WHEN AttendedIn6Weeks = 'Y' then Numerator else 0 end) over (partition by TheMonth)
	,FirstAdmissionOnStrokeWardCount = sum(CASE WHEN FirstAdmissionWard in ('30M','31M','31R') AND AttendedIn6Weeks = 'Y' then 1 else 0 end) over (partition by TheMonth)
	,PrimDiagStrokeCount = sum(CASE WHEN PrimaryDiagnosisIndicator = 'Yes' then 1 else 0 end) over (partition by TheMonth)
	,OtherDiagStrokeCount = sum(CASE WHEN OtherDiagnosisIndicator = 'Yes' then 1 else 0 end) over (partition by TheMonth)
	,BothDiagStrokeCount = sum(CASE WHEN OtherDiagnosisIndicator = 'Yes' AND PrimaryDiagnosisIndicator = 'Yes' then 1 else 0 end) over (partition by TheMonth)
	,AttendedIn6Weeks
from 
	@StrokeTable

order by 
	DischargeTime asc




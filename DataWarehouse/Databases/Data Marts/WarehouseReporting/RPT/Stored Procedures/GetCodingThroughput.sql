﻿
CREATE PROCEDURE [RPT].[GetCodingThroughput]

(
 @StartDate Date 
,@EndDate Date
,@Division varchar(MAX)
,@NationalSpecialtyCode varchar(MAX)
)


AS

SELECT 
	 Encounter.ClinicalCodingCompleteDate
	,Calendar.FinancialMonthKey
	,Calendar.TheMonth
	,Calendar.FinancialYear
	,Specialty = Specialty.NationalSpecialty
	,SpecialtyCode = Specialty.NationalSpecialtyCode
	,Division = Directorate.Division
	,CasesCodedWithinTarget = SUM( CASE 
							WHEN DATEDIFF(D,Encounter.DischargeDate,Encounter.ClinicalCodingCompleteDate) <7 THEN 1 
							ELSE 0
					  END)
	,CasesNotCodedWithinTarget = SUM( CASE 
							WHEN DATEDIFF(D,Encounter.DischargeDate,Encounter.ClinicalCodingCompleteDate) >=7 THEN 1 
							ELSE 0
					  END)
	,Cases = COUNT(Encounter.SourceUniqueID)

FROM
	apc.Encounter Encounter
INNER JOIN WH.Calendar Calendar
	ON Encounter.ClinicalCodingCompleteDate = Calendar.TheDate
INNER JOIN PAS.Specialty Specialty
	ON Specialty.SpecialtyCode = Encounter.SpecialtyCode
	AND Specialty.NationalSpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
INNER JOIN PAS.Directorate Directorate
	ON Directorate.DirectorateCode = Encounter.EndDirectorateCode
	AND Directorate.Division IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
WHERE 
	Encounter.ClinicalCodingStatus ='C'
	AND Encounter.ClinicalCodingCompleteDate >= @StartDate
	AND Encounter.ClinicalCodingCompleteDate <= @EndDate	
GROUP BY 
	 Encounter.ClinicalCodingCompleteDate
	,Calendar.FinancialMonthKey
	,Calendar.TheMonth
	,Calendar.FinancialYear
	,Specialty.NationalSpecialty
	,Specialty.NationalSpecialtyCode
	,Directorate.Division

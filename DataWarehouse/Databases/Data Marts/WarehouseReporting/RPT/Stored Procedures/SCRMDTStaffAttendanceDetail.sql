﻿










CREATE PROCEDURE [RPT].[SCRMDTStaffAttendanceDetail] 

  @FromDate          Date
 ,@MeetingType       varchar(20)
 ,@MDTMeetingSubType varchar(50)
 ,@RoleDescription   varchar(50)

as

--DECLARE @FromDate           AS DATE        = '01-JAN-2013'
--DECLARE @MeetingType        AS varchar(20) = 'Sarcoma'
--DECLARE @MDTMeetingSubType  AS varchar(50) = 'Sarcoma GMOSS MDT'
--DECLARE @RoleDescription    AS varchar(50) = 'All'

select
    [MDT_DATE]
   , MeetingType    = [MEETING_TYPE_DESC]            
    ,MeetingSubType = [SUB_DESC]
   , MDTMeeting     = [MEETING_TYPE_DESC] + ' ' + replace([SUB_DESC] , '-' , '') 
   ,[NAME]
   ,[SPECIALTY]
   ,[DISPLAY_ORDER]
   ,[COMMENTS]
 
  from warehousesql.[CancerRegister].[dbo].[vwMDT_ATTENDED]
  
  where MDT_DATE            = @FromDate 
    and [MEETING_TYPE_DESC] = @MeetingType 
    and [SUB_DESC]          = 
        (case when @MDTMeetingSubType = 'N/A' then '-'
              else @MDTMeetingSubType
         end 
        )    
    and [SPECIALTY] like
        (case when @RoleDescription = 'All' then '%' 
              else @RoleDescription 
         end
        )
    and [PRESENT] = 1
    
 order by [SPECIALTY],[NAME]



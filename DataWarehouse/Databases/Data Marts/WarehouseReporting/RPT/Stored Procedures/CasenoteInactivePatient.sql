﻿


CREATE procedure [RPT].[CasenoteInactivePatient]
(
	 @CasenoteTypeCode varchar(10)
	,@FromDate date
	,@ToDate date
	,@Deceased bit
	,@BatchId INT	= NULL
	,@AllDates BIT	= 0
	,@IsBorrowed bit = null
)
as

	/******************************************************************************
	**  Name: RPT.CasenoteInactivePatient
	**  Purpose: 
	**
	**  Casenotes from Inactive Patients
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:		Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	**				CB			Initial Coding
	** 18.03.13		MH			Added BatchId parameter to filter by a batch and where
	**							there are no duplicate Patient Master Index rows
	** 29.03.14		GC			Added TypeCode to allow for grouping
	** 17.07.14		PE			Copied LIVE version to DEV (different)
	** 17.07.14		PE			Exclude microfilmed casenotes (requested by Mellissa Blakeley) 
	******************************************************************************/

/* ========================= debug ===========================*/
--declare
--	 @CasenoteTypeCode varchar(10) = 'All'
--	,@FromDate date = '19861121' -- '1 Mar 2013'
--	,@ToDate date = '20130613' -- '31 Mar 2013'
--	,@Deceased bit
--	,@BatchId INT	= NULL
--	,@AllDates BIT	= 0
--	,@IsBorrowed bit = null
/* ===========================================================*/

select
	 Casenote.SourcePatientNo
	,Casenote.ModifiedTime
	,Casenote.Surname
	,Casenote.Forenames
	,Casenote.DateOfBirth
	,Casenote.DateOfDeath
	,Casenote.DistrictNo
	,Casenote.CasenoteNo
	,Casenote.AllocatedDate
	,Casenote.CasenoteLocationCode
	,CasenoteLocation.CasenoteLocation
	,Casenote.CasenoteLocationDate
	,Casenote.CasenoteStatus
	,Casenote.WithdrawnDate
	,Casenote.Comment
	,Casenote.CurrentBorrowerCode
	,CurrentBorrower = CasenoteBorrower.Borrower
	,Casenote.CasenoteTypeCode

from
	WarehouseOLAP.dbo.BasePASCasenoteInactive Casenote

inner join WarehouseOLAP.PMI.BasePASPatientIndex PASPI
	ON Casenote.SourcePatientNo = PASPI.InternalPatientNumber

left join Warehouse.PAS.CasenoteLocation
	ON	CasenoteLocation.CasenoteLocationCode = Casenote.CasenoteLocationCode

left join Warehouse.PAS.CasenoteBorrower
	ON	CasenoteBorrower.BorrowerCode = Casenote.CurrentBorrowerCode


where
(
	(
			Casenote.ModifiedDate between @FromDate and @ToDate
		AND @AllDates = 0
	)
	OR
	(
			Casenote.ModifiedDate between @FromDate and @ToDate
		AND @AllDates = 1
		AND @BatchId IS NULL
	)
	OR
	(
			@AllDates = 1
		AND @BatchId IS NOT NULL
	)
)

AND	(
		Casenote.Deceased = @Deceased
	OR	@Deceased is null
	)

AND COALESCE(PASPI.HasDuplicates, 0) = 0
AND COALESCE(PASPI.BatchId,'') = COALESCE(@BatchId, PASPI.BatchId,'')

and	
	(
	Casenote.CasenoteTypeCode = @CasenoteTypeCode
	or
	@CasenoteTypeCode = 'All'
		and
		(
			left(CasenoteTypeCode , 1) in ('M','E','R','S','B')
			OR
			left(CasenoteTypeCode , 2) = 'SP'
		)
		 
		and left(CasenoteTypeCode , 3) <> 'BCG'
	)

and	(
		Casenote.IsBorrowed = @IsBorrowed
	or	@IsBorrowed is null
	)
	
/* Added Paul Egan 17/07/2014 - exclude microfilmed casenotes */
and not exists
	(
	select
		1
	from
		Warehouse.dbo.EntityLookup l
	where
		l.EntityTypeCode = 'CasenoteMicrofilmed'
		and	l.EntityCode = Casenote.CasenoteNo
	)

order by
	-- Casenote.ModifiedDate
	--,Casenote.Surname
	--,Casenote.Forenames
	--,
	Casenote.CasenoteNo





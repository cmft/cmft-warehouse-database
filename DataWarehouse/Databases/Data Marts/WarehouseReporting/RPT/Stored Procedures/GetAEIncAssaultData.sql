﻿

CREATE proc [RPT].[GetAEIncAssaultData]
(
@ArrivalDateStart datetime,
@ArrivalDateEnd datetime
)
as


--declare @ArrivalDateStart datetime = (convert(varchar(6),dateadd(m,-1,getdate()),112) + '01');

--declare @ArrivalDateEnd datetime = dateadd(d,-1,(convert(varchar(6),getdate(),112) + '01'));


SELECT 
	LocalPatientID = Encounter.DistrictNo
	,Encounter.AttendanceNumber
	,NHSNumber = ISNULL(Encounter.NHSNumber,NHSNumbers.NHSNumber)
	,Encounter.DateOfBirth
	,Sex =
		CASE
		WHEN Encounter.SexCode = 'M' THEN '01'
		WHEN Encounter.SexCode = 'F' THEN '02'
		ELSE Encounter.SexCode
		END
	,Encounter.Postcode
	,ArrivalModeNationalCode = ISNULL(ArrivalMode.ArrivalModeNationalCode,'2')
	,Encounter.AttendanceCategoryCode
	,AttendanceDisposalCode = ISNULL(AttendanceDisposal.AttendanceDisposalNationalCode,'14')
	,IncidentLocationTypeCode = 
		CASE 
		WHEN Encounter.IncidentLocationTypeCode = 0 THEN '91' 
		ELSE ISNULL(Encounter.IncidentLocationTypeCode,'91') 
		END	
	,PatientGroupCode = 
		CASE
		WHEN PatientGroupCode = 0 THEN '80'
		ELSE PatientGroupCode
		END
	,SourceOfReferralCode = 
		CASE
		WHEN Encounter.SourceOfReferralCode = '' THEN '08'
		ELSE ISNULL(Encounter.SourceOfReferralCode,'08')
		END	
	,Encounter.ArrivalDate
	,Encounter.ArrivalTime
	,Encounter.AgeOnArrival
	,NationalEthnicOriginCode = ISNULL(PASEthnicOrigin.NationalEthnicOriginCode,'99')
	,Encounter.AlcoholLocation
	,Encounter.AlcoholInPast12Hours
	,AssaultDate
	,AssaultTime
	,AssaultWeapon
	,AssaultWeaponDetails
	,AssaultLocation
	,AssaultLocationDetails
	,AlcoholConsumed3Hour
	,AssaultRelationship
	,AssaultRelationshipDetails
	,ReportStartDate = @ArrivalDateStart
	,ReportEndDate = @ArrivalDateEnd
	
	
	
FROM 
	WarehouseReporting.AE.[Encounter] Encounter
		
LEFT JOIN Warehouse.AE.ArrivalMode ArrivalMode
ON	ArrivalMode.arrivalmodecode = Encounter.arrivalmodecode

LEFT JOIN WarehouseReporting.[AE].[AttendanceDisposal] AttendanceDisposal
ON AttendanceDisposal.[AttendanceDisposalCode] = Encounter.AttendanceDisposalcode

--to get Ethnic Origin we need to join onto PAS.Patient and PAS.EthnicOrigin
LEFT JOIN WarehouseReporting.PAS.Patient PASPatient
ON PASPatient.DistrictNo = Encounter.DistrictNo

LEFT JOIN WarehouseReporting.PAS.EthnicOrigin PASEthnicOrigin
ON PASEthnicOrigin.EthnicOriginCode = PASPatient.EthnicOriginCode

LEFT JOIN WarehouseReporting.AE.Assault Assault 
ON	Assault.AESourceUniqueID = Encounter.SourceUniqueID

--this query will get us missing NHS Numbers
--this one is on Live
LEFT JOIN
		(select 
			NHSNumber
			,PatientSurname
			,PatientForename
			,DateOfBirth
			,PostCode
		from [Information].[dbo].[NHS_number_internal_reference]) NHSNumbers
ON NHSNumbers.PatientForename = Encounter.PatientForename
AND NHSNumbers.PatientSurname = Encounter.PatientSurname
AND NHSNumbers.DateOfBirth = Encounter.DateOfBirth
AND NHSNumbers.PostCode = Encounter.PostCode

WHERE
	arrivaldate BETWEEN @ArrivalDateStart AND @ArrivalDateEnd
	AND reportable = 1

ORDER BY
	Encounter.DistrictNo ASC
﻿CREATE proc [RPT].[GetCOPDTrackingList2] 
(
     @NationalSpecialtyCode varchar(MAX) = NULL
	,@WardCode varchar(MAX) = NULL
	,@PracticeCode varchar(MAX) = NULL
	,@PatientCategoryCode varchar(MAX) = NULL
	,@DateRange  varchar(MAX) = NULL
)

AS


SELECT 
	 ProviderSpellNo
	,AdmissionDate
	,AdmissionTime
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,Postcode
	,Gender = SexCode
	,Ward = COALESCE(EndWard.WardCode + ' - ' + EndWard.Ward
					,StartWard.WardCode + ' - ' + StartWard.Ward
					)
	,NHSNumber
	,CaseNoteNumber
	,encounter.DistrictNo
	,PatientCategoryCode
	,ExpectedLOS
	,Specialty = Specialty.Specialty
	,Consultant = Consultant.Consultant
	,RegisteredGPPracticeCode = COALESCE(Practice.Practice
										,RegisteredGPPracticeCode
										)
FROM
	APCUpdate.encounter with(nolock)
INNER JOIN Warehouse.PAS.Consultant Consultant with(nolock) 
	ON encounter.ConsultantCode = Consultant.ConsultantCode
inner JOIN RPT.GetCOPDTrackingList_DistrictNumberFilter copdfilt
  ON APCUpdate.encounter.DistrictNo = copdfilt.DistrictNo 
LEFT OUTER JOIN Warehouse.PAS.Specialty with(nolock)
	ON APCUpdate.encounter.SpecialtyCode =  Specialty.SpecialtyCode 
LEFT OUTER JOIN Warehouse.PAS.Ward EndWard with(nolock) 
	ON APCUpdate.encounter.EndWardTypeCode = EndWard.WardCode
LEFT OUTER JOIN Warehouse.PAS.Ward StartWard with(nolock) 
	ON APCUpdate.encounter.StartWardTypeCode = StartWard.WardCode
LEFT OUTER JOIN WH.Practice Practice with(nolock)
	ON  encounter.EpisodicGpPracticeCode = Practice.PracticeCode 

WHERE 
	encounter.EpisodeEndDate is null
AND 
	(
			encounter.PatientCategoryCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@PatientCategoryCode,','))
		OR
			@PatientCategoryCode IS NULL
	)
AND(
			encounter.EpisodicGpPracticeCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@PracticeCode,','))
		OR
			@PracticeCode IS NULL
	)
AND
	(
			encounter.EndWardTypeCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@WardCode,','))
		OR
			@WardCode IS NULL
	)
AND
	(
			encounter.SpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
		OR
			@NationalSpecialtyCode IS NULL
	)
AND 
	(
			AdmissionDate >=	CASE @DateRange
								WHEN 'Today' THEN  convert(DATE,getdate())
								WHEN 'Previous 3 days' THEN DATEADD(DAY, -3, convert(DATE,getdate()))
								WHEN 'Previous 7 days' THEN DATEADD(DAY, -7, convert(DATE,getdate()))
								WHEN 'Previous 28 days' THEN DATEADD(DAY, -28, convert(DATE,getdate()))
								WHEN 'Previous 12 months' THEN DATEADD(DAY, -52, convert(DATE,getdate()))
							END
		OR
			@DateRange IS NULL
	)

﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [RPT].[VTEPerformanceCombined]
(
	 @FinancialMonthKey int
)
as



SELECT
	 Exclusion = VTESpell.VTEExclusionReasonCode
	,DivisionCode = Directorate.DivisionCode
	,Division = Division.Division
	,SpecialtyCode = Specialty.NationalSpecialtyCode
	,Specialty = Specialty.NationalSpecialty
	,WardCode = Ward.WardCode
	,Ward = Ward.Ward
	,ConsultantCode = Encounter.ConsultantCode
	,Consultant = Consultant.Consultant
	,Cases = 1

	,CodedExclusionCases =
		case
		when
				Encounter.CodingCompleteDate IS NOT NULL
			AND VTESpell.VTEExclusionReasonCode is not null 
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
		else 0
		end

	,UncodedExclusionCases =
		case
		when
				Encounter.CodingCompleteDate IS NULL
			AND VTESpell.VTEExclusionReasonCode is not null
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
		else 0
		end

	,VTECompleteCases = 
		case VTESpell.VTECategoryCode
		when 'C' then 1
		else 0
		end

	,VTEIncompleteCases = 
		case VTESpell.VTECategoryCode
		when 'I' then 1
		else 0
		end

	,VTEMissingCases = 
		case VTESpell.VTECategoryCode
		when 'M' then 1
		else 0
		end
		
	,CodingCompleteCases = 
		CASE
		WHEN Encounter.CodingCompleteDate IS NOT NULL THEN 1
		ELSE 0
		END
		
	,CodingIncompleteCases = 
		CASE
		WHEN Encounter.CodingCompleteDate IS NULL THEN 1
		ELSE 0
		END
	, 'M' as Location
FROM
	WarehouseReporting.APC.Encounter

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = Encounter.AdmissionDate

left join WarehouseReporting.APC.VTESpell
on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

left join WarehouseReporting.WH.Directorate
on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

left join WarehouseReporting.WH.Division
on	Division.DivisionCode = Directorate.DivisionCode

left join WarehouseReporting.PAS.Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join WarehouseReporting.PAS.Ward
on	Ward.WardCode = Encounter.StartWardTypeCode

left join WarehouseReporting.PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

where
	Encounter.FirstEpisodeInSpellIndicator = 1
and	Calendar.FinancialMonthKey = @FinancialMonthKey
and	Encounter.PatientCategoryCode != 'RD'
and	(
		(
			Encounter.AgeCode like '%Years'
		and	left(Encounter.AgeCode , 2) > '17'
		)
	or	Encounter.AgeCode = '99+'
	)
UNION ALL

SELECT 
	 Exclusion = CASE	when Proc_Cohort = 'Cohort' then 'PROC'
						when DVT = 'Cohort' then 'DVT'
						when [cohort for Manual signoff] =  'Cohort' then 'MAU'
						else null end
	,DivisionCode = 'Trafford'
	,Division = 'Trafford'
	,SpecialtyCode = bedmanSpecialty
	,Specialty = bedmanSpecialtyCode
	,WardCode = VTE.[admittingward]
	,Ward = WardName
	,ConsultantCode = [ConsultantCode]
	,Consultant = isnull(VTE.[ConSurname] + ', ' + VTE.[ConFirstName], VTE.[ConSurname])
	,Cases = 1
	
	,CodedExclusionCases = 
		case
		when All_cohorts = 'Cohort'
			AND (ProcedureCode IS NOT NULL OR DiagnosticCode IS NOT NULL)
			
			--VTESpell.VTEExclusionReasonCode is not null 
			--and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
			then 1
		else 0
		end

	,UncodedExclusionCases = 
		case
		when All_cohorts = 'Cohort'
			AND (ProcedureCode IS NULL AND DiagnosticCode IS NULL)

					
		--		Encounter.CodingCompleteDate IS NULL
		--	AND VTESpell.VTEExclusionReasonCode is not null
		--	and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
			then 1
		else 0
		end

	,VTECompleteCases = case when All_Cohorts IS NULL and mau.admissionnumber IS NULL then Complete 
						     when mau.admissionnumber IS NOT NULL then 1
						else 0 end
		--case FormStatus
		--when 'Complete' then 1
		--else 0
		--end

	,VTEIncompleteCases = case when All_Cohorts IS NULL and mau.admissionnumber IS NULL then incomplete 
						else 0 end 
		--case FormStatus
		--when 'Incomplete' then 1
		--else 0
		--end

	,VTEMissingCases = 0
		--case ISNULL(FormStatus,'1')
		--when '1' then 1
		--else 0
		--end
		
	,CodingCompleteCases = 	case when (ProcedureCode IS NOT NULL OR DiagnosticCode IS NOT NULL) 
	--AND All_Cohorts IS NULL 
	then 1 else 0 end
						


		
	,CodingIncompleteCases = case when (ProcedureCode IS NULL AND DiagnosticCode IS NULL) 
	--AND All_Cohorts IS NULL 
	then 1 else 0 end
	
	,'T' as Location
	
	
	
	
FROM [WarehouseReporting].[dbo].[AC_VTE_Cohorts] VTE

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = cast(AdmittedOn as date)

left join dbo.VTE_Temp_MAU mau on VTE.AdmissionNumber = mau.AdmissionNumber and [vte completed] <> 'None'

	where Calendar.FinancialMonthKey = @FinancialMonthKey


--SELECT 
--	 Exclusion = Null
--	,DivisionCode = 'Trafford'
--	,Division = 'Trafford'
--	,SpecialtyCode = patient.SpecialtyCode
--	,Specialty = patient.Specialty
--	,WardCode = Ward.Code
--	,Ward = Ward.Name
--	,ConsultantCode = isnull(PersonConsultant.FamilyName + ', ' + PersonConsultant.GivenName, PersonConsultant.FamilyName)
--	,Consultant = isnull(PersonConsultant.FamilyName + ', ' + PersonConsultant.GivenName, PersonConsultant.FamilyName)
--	,Cases = 1
	
--	,CodedExclusionCases = 
--		case
--		when CompleteForms = 1
--			AND All_Cohorts IS NOT NULL
			
--			--VTESpell.VTEExclusionReasonCode is not null 
--			--and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
--			then 1
--		else 0
--		end

--	,UncodedExclusionCases = 
--		case
--		when IncompleteForms = 1
--			AND All_Cohorts IS NOT NULL
					
--		--		Encounter.CodingCompleteDate IS NULL
--		--	AND VTESpell.VTEExclusionReasonCode is not null
--		--	and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
--			then 1
--		else 0
--		end

--	,VTECompleteCases = enc.Complete
--		--case FormStatus
--		--when 'Complete' then 1
--		--else 0
--		--end

--	,VTEIncompleteCases = enc.incomplete
--		--case FormStatus
--		--when 'Incomplete' then 1
--		--else 0
--		--end

--	,VTEMissingCases = case isnull(enc.complete,9999) when 9999 then 1 else 0 end
--		--case ISNULL(FormStatus,'1')
--		--when '1' then 1
--		--else 0
--		--end
		
--	,CodingCompleteCases = CompleteForms

		
--	,CodingIncompleteCases = IncompleteForms
--	,'T' as Location
	
	
	
	
--FROM
--(
--	select 
--		dept.Name Division
--		,dept.code DivisionCode
--		,sp.Name Specialty
--		,sp.code SpecialtyCode
--		,Admission.Status AdmissionStatus
--		,Admission.HISId_Number AdmissionId
--		,Admission.AdmittedOn AdmissionDate
--		,case 
--			when Admission.Status = 'DI' THEN
--				(SELECT Max(EndDate) FROM Replica_UltraGendaBedman.dbo.BedOccupancy WHERE AdmissionId = Admission.Id)
--			else
--				null
--		 end DischargeDate
--		,case 
--			WHEN vtelist.FormId is null then 'Incomplete'
--			else 'Complete'
--		 end as FormStatus
--		,case 
--			WHEN vtelist.FormId is null then 0
--			else 1
--		 end as CompleteForms
--		,case 
--			WHEN vtelist.FormId is null then 1
--			else 0
--		 end as IncompleteForms
--		,Patient.HISId_Number EPMINumber
--		,PersonPatient.FamilyName
--		,PersonPatient.GivenName
--		,PersonPatient.BirthDate
--		,case 
--			when Admission.Status = 'AD' then
--				(SELECT top 1 BedId from Replica_UltraGendaBedman.dbo.BedOccupancy bo WHERE Admission.Id = bo.AdmissionId and (getdate() between StartDate and EndDate OR EndDate < getdate()) ORDER BY EndDate desc)
--			when Admission.Status = 'DI' then
--				(SELECT top 1 BedId from Replica_UltraGendaBedman.dbo.BedOccupancy bo WHERE Admission.Id = bo.AdmissionId ORDER BY EndDate desc)
--		 end as BedId
--		,(SELECT top 1 AuthorityId from Replica_UltraGendaBedman.dbo.BedOccupancy bo WHERE Admission.Id = bo.AdmissionId ORDER BY StartDate asc) as AuthorityId
--	from 
--		Replica_UltraGendaBedman.dbo.Admission 
--	inner join 
--		Replica_UltraGendaFoundation.dbo.Patient Patient
--	on
--		Admission.PatientId = Patient.Id
--	inner join
--		Replica_UltraGendaFoundation.dbo.Person PersonPatient
--	on 
--		Patient.PersonId = PersonPatient.Id
--	left join
--		Whiteboard.dbo.InfopathForm_VTEList vtelist
--	on
--		Admission.HISId_number = vtelist.AdmissionNumber
--	left join Replica_UltraGendaBedman.dbo.BedOccupancy bo 
--		on Admission.Id = bo.AdmissionId --and (getdate() between StartDate and EndDate OR EndDate < getdate())
--	left join Replica_UltraGendaBedman.dbo.AdmissionType at
--		on bo.AdmissionTypeId = at.id	
--	left join Replica_UltraGendaBedman.dbo.Speciality sp
--		on at.SpecialityId = sp.id
--	left join Replica_UltraGendaFoundation.dbo.HISDepartment dept
--		on sp.HISDepartmentId = dept.id		
--	Where 
--		Admission.IsCurrent = 1 
--	and Admission.IsDeleted = 0
--	and Admission.Status IN ('AD', 'DI')
--	and Admission.AdmittedOn >= '01 Jun 2010'
----	and (vtelist.FormStatus = 'New' or vtelist.FormStatus is null)
--	and datediff(year,PersonPatient.BirthDate , Admission.AdmittedOn) >= 18
--) Patient
--inner join 
--	Replica_UltraGendaFoundation.dbo.Authority Auth
--ON
--	Patient.AuthorityId = Auth.Id
--inner join 
--	Replica_UltraGendaFoundation.dbo.Person PersonConsultant
--on
--	Auth.PersonId = PersonConsultant.Id
--inner join Replica_UltraGendaBedman.dbo.Bed
--	on Patient.BedId = Bed.id
--inner join Replica_UltraGendaBedman.dbo.Room
--	on Room.Id = Bed.RoomId
--inner join Replica_UltraGendaBedman.dbo.Ward
--	on Ward.Id = Room.WardId
--inner join WarehouseReporting.WH.Calendar
--on	Calendar.TheDate = cast(AdmissionDate as date)
--left join WarehouseReporting.dbo.AC_VTE_Cohorts enc
--	on Patient.AdmissionId = enc.AdmissionNumber

--where Calendar.FinancialMonthKey = @FinancialMonthKey

--where
--	AdmissionDate BETWEEN '01 April 2012' AND cast('30 April 2012' as datetime) + 1
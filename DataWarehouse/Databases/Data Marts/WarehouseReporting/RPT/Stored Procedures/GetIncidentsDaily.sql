﻿

CREATE proc 
		[RPT].[GetIncidentsDaily]

		(
		 @ReportType int
		)

as

		--declare @ReportType int
		--set @ReportType = 2

select

		IncidentRecno
		,SourceUniqueID
		,IncidentNumber
		,IncidentDescription
		,FinancialYear
		,FinancialMonthKey
		,TheMonth
		,TheWeek
		,WeekNoKey
		,TheDate
		,DivisionCode
		,DivisionDescription
		--,DirectorateCode
		--,DirectorateDescription
		,DepartmentCode
		,DepartmentDescription
		,SeverityCode
		,SeverityDescription
		,GradeTypeCode
		,GradeTypeDescription
		,IncidentTypeCode
		,IncidentTypeDescription
		,CauseGroupCode
		,CauseGroupDescription
		,CauseCode
		,CauseDescription
		,IncidentPersonTypeCode
		,CurrentFinYear
		,ReportingTypeCode
		,ReportingGradeCode
		,Reportable =	case 
							when (ReportingTypeCode = 3 and ReportingGradeCode in (3,4))
								then 1
							when (ReportingTypeCode in (1,2) and ReportingGradeCode in (2,3,4,5))
								then 1
							else 0
						end

from

(
			select
					IncidentRecno
					,SourceUniqueID
					,IncidentNumber
					,IncidentDescription
					,FinancialYear
					,FinancialMonthKey
					,TheMonth =					cast(TheMonth as date)
					,TheWeek =					cast(FirstDayOfWeek as date)
					,WeekNoKey
					,TheDate
					,DivisionCode =				coalesce(Division.DivisionCode,0)
					,DivisionDescription =		coalesce(DivisionDescription,'')
					--,DirectorateCode =			coalesce(Directorate.DirectorateCode,0)
					--,DirectorateDescription =	coalesce(DirectorateDescription,'')
					,DepartmentCode =			coalesce(Department.DepartmentCode,0)
					,DepartmentDescription =	coalesce(DepartmentDescription,'')
					,SeverityCode =				coalesce(SeverityCode,0)
					,SeverityDescription =		coalesce(SeverityDescription,'')
					,GradeTypeCode =			coalesce(GradeTypeCode,0)
					,GradeTypeDescription =		coalesce(GradeTypeDescription,'')
					,IncidentTypeCode =			coalesce(IncidentType.IncidentTypeCode,0)
					,IncidentTypeDescription =	coalesce(IncidentTypeDescription,'')
					,CauseGroupCode =			coalesce(CauseGroup.CauseGroupCode,0)
					,CauseGroupDescription =	coalesce(CauseGroupDescription,'')
					,CauseCode
					,CauseDescription
					,IncidentPersonTypeCode
					,CurrentFinYear =			case
													when FinancialYear = (	
																			select 
																				distinct 
																					FinancialYear 
																			from	
																					WarehouseReporting.WH.Calendar
																			where	TheDate = cast(getdate()as date)
																		  )
														then 1
													else 0
												end
						
						--datepart(yyyy,getdate()-90)+"/"+datepart(yyyy,getdate())
					,ReportingTypeCode =		case 
													when (CauseGroup.CauseGroupCode = 49 or CauseCode in (82,11266,11366,11466,11566,11766))
														then 1	--Fall
													when IncidentType.IncidentTypeCode = 81 or CauseGroup.CauseGroupCode = 70
														then 2	--MedError
													when CauseCode in ( 10176,								--PU Unclassified
																		7572,8473,8873,9776,				--PU Grade 1
																		7672,8573,8973,9876,11276,			--PU Grade 2
																		7772,8673,9073,9976,11376,			--PU Grade 3
																		7872,7972,8773,9773,70076,11476)	--PU Grade 4 
														then 3	--CMFT_PU 
													else 0 
												end
					,ReportingGradeCode =		case 
													when CauseCode in ( 7572,8473,8873,9776,				--PU Grade 1
																		7672,8573,8973,9876,11276,			--PU Grade 2
																		7772,8673,9073,9976,11376,			--PU Grade 3
																		7872,7972,8773,9773,70076,11476 )	--PU Grade 4
														then cast(substring(CauseDescription,len(CauseDescription),1) as int)
													when CauseCode in ( 10176)							--PU Unclassified
														then 1
													when GradeTypeCode is null
														then 0
													else cast(left(GradeTypeDescription ,1) as int)
												end 

			from	
					Warehouse.Incident.Incident Incident
			  
			full outer join  
					Warehouse.Incident.Division Division
			on
					Incident.DivisionCode collate latin1_general_cs_as = Division.MixedCaseCode collate latin1_general_cs_as
					
			--full outer join  
			--		Warehouse.Incident.Directorate Directorate
			--on
			--		Incident.DirectorateCode collate latin1_general_cs_as = Directorate.MixedCaseCode collate latin1_general_cs_as
					
			full outer join  
					Warehouse.Incident.Department Department
			on
					Incident.DepartmentCode collate latin1_general_cs_as = Department.MixedCaseCode collate latin1_general_cs_as

			full outer join  
					Warehouse.Incident.Severity Severity
			on
					Incident.Severity collate latin1_general_cs_as = Severity.MixedCaseCode collate latin1_general_cs_as

			full outer join  
					Warehouse.Incident.GradeType GradeType
			on
					Incident.GradeType collate latin1_general_cs_as = GradeType.MixedCaseCode collate latin1_general_cs_as
					
			full outer join  
					Warehouse.Incident.IncidentType IncidentType
			on
					Incident.IncidentTypeCode collate latin1_general_cs_as = IncidentType.MixedCaseCode collate latin1_general_cs_as
					
			full outer join  
					Warehouse.Incident.CauseGroup CauseGroup
			on
					Incident.CauseGroupCode collate latin1_general_cs_as = CauseGroup.MixedCaseCode collate latin1_general_cs_as

			inner join  
					Warehouse.Incident.Cause Cause
			on
					Incident.IncidentCause1Code collate latin1_general_cs_as = Cause.MixedCaseCode collate latin1_general_cs_as

			inner join	
					(
						select 
							IncidentCode
							,IncidentPersonTypeCode
						from 
							Warehouse.Incident.IncidentPerson 
						where 
							IncidentPersonTypeCode = 1 --Patients Only
							
					)	IncidentPerson 
			on
					Incident.IncidentCode collate latin1_general_cs_as = IncidentPerson.IncidentCode collate latin1_general_cs_as

			inner join 
					WarehouseReporting.WH.Calendar Calendar
			on
					Incident.IncidentDate = Calendar.TheDate

)		Incidents

where
		ReportingTypeCode = @ReportType
and
		DivisionCode <> 0

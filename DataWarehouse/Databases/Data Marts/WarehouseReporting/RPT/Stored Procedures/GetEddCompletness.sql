﻿
/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [RPT].[GetEddCompletness]
(
	 @startdate smalldatetime
	,@enddate smalldatetime
	,@specialtycode varchar(max)
	,@Division varchar(max)
	,@patientcategory varchar(max)
)

AS 
--DECLARE @startdate smalldatetime = '1 Jan 2012'
--,@enddate smalldatetime= '1 Feb 2012'

SELECT 
	 ISNULL(division, 'Unknown') AS Division
	,ISNULL(Specialty.NationalSpecialtyCode, 'Unknown') AS SpecialtyCode
	,ISNULL(WHSpecialty.Specialty, 'Unknown') AS Specialty
	,ISNULL(Ward.WardCode, 'Unknown') AS WardCode
	,ISNULL(Ward.Ward, 'Unknown') AS Ward
	,ISNULL(Consultant.ConsultantCode,'Unknown') AS ConsultantCode
	,ISNULL(Consultant.Consultant,'Unknown') AS Consultant
	,ISNULL(ExpectedLOS.RTTPathwayCondition,'Unknown') AS PathwayCondition
	,POD = case
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'2'
					,'3' --INTERVAL ADMISSION
					,'4'
					,'5'
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective
			--Non Elective
				else 'NE' --Non Elective
			end
	,1 as Discharges
	,CASE 
				WHEN ExpectedLOS.ExpectedLOS IS NOT NULL AND COALESCE (ModifiedFromSystem,SourceSystem) IN ('PAS','Inquire') THEN 1
				ELSE 0 
			END
		 WithExpectedLOSInPAS
	,CASE 
				WHEN ExpectedLOS.ExpectedLOS IS NOT NULL AND  COALESCE (ModifiedFromSystem,SourceSystem) IN ('Bedman') THEN 1
				ELSE 0 
			END
		 HasEddBedmanFlag
FROM 
	 warehouse.APC.ExpectedLOS ExpectedLOS
	 
LEFT OUTER JOIN Warehouse.WH.Directorate Directorate
	ON ExpectedLOS.DirectorateCode = Directorate.DirectorateCode 
	
LEFT OUTER JOIN Warehouse.WH.Division Division
	ON Directorate.DivisionCode = Division.DivisionCode

LEFT OUTER JOIN Warehouse.PAS.Specialty
	ON ExpectedLOS.Specialty =  Specialty.SpecialtyCode 

left join Warehouse.WH.TreatmentFunction WHSpecialty
	on	WHSpecialty.SpecialtyCode = Specialty.NationalSpecialtyCode

LEFT OUTER JOIN Warehouse.PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = ExpectedLOS.AdmissionMethodCode

LEFT OUTER JOIN Warehouse.PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = ExpectedLOS.ManagementIntentionCode

LEFT OUTER JOIN Warehouse.PAS.Consultant Consultant
	ON ExpectedLOS.Consultant = Consultant.ConsultantCode

LEFT OUTER JOIN Warehouse.PAS.Ward Ward
	ON ExpectedLOS.Ward = Ward.WardCode
WHERE
		CONVERT(date, ExpectedLOS.DischargeTime, 103) >= @startdate --'14/Aug/2011'
	AND CONVERT(date, ExpectedLOS.DischargeTime, 103) <= @enddate --'14/Aug/2011'
	AND ExpectedLOS.ManagementIntentionCode NOT IN ('R','N') 
	AND NOT (ExpectedLOS.Ward = 'ESTU' AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6)
	AND Specialty.NationalSpecialtyCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@specialtycode,','))
    AND Division IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))
	AND ArchiveFlag <> 'D'
	AND Specialty.NationalSpecialtyCode <> '501'
	AND ExpectedLOS.Ward <> '76A'
	AND Specialty.Specialty NOT like '%DIALYSIS%'
	AND Specialty.Specialtycode NOT like 'IH%'
	AND ExpectedLOS.Ward NOT like 'SUB%'
	AND
		case
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'2'
					,'3' --INTERVAL ADMISSION
					,'4'
					,'5'
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective
			--Non Elective
				else 'NE' --Non Elective
			end IN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@patientcategory,','))



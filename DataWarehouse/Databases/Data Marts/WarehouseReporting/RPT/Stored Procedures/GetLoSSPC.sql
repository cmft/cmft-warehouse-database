﻿
create proc RPT.GetLoSSPC
(
	@start datetime = null
	,@end datetime = null
	,@level varchar(20) = null
	,@parent varchar(50) = null
	,@losgroup varchar(20) = null
)
	
as

--declare	@start datetime = null
--		,@end datetime = null
--		,@level varchar(20) = null
--		,@parent varchar(20) = null
--		,@losgroup varchar(20) = null

set @end = coalesce(@end,(select max([SnapshotDate])
							 from ETL.LoSPatientLevelData))
set @start = coalesce(@start,dateadd(wk, -14, @end))

set @level = coalesce(@level, 'Division')

set @parent = coalesce (@parent, '(Select All)')

set @losgroup = coalesce (@losgroup, '(Select All)')

/* result set */

select 
	*		
from 
	ETL.LoSSPC
where 
	SnapshotDate between @start and @end
	and (Parent = @parent or @parent = '(Select All)')
 



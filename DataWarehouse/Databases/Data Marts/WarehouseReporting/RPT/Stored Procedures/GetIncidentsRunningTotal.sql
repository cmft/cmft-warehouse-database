﻿
CREATE proc 
		[RPT].[GetIncidentsRunningTotal]

		(
		 @ReportType int
		,@FinancialYear varchar(4)
		,@Division int
		,@Department int
		,@Period int		 
		)

as

		--declare @ReportType int
		--declare @FinancialYear varchar(9)
		--declare @Division int
		--declare @Department int
		--declare @Period int
		
		--set @ReportType = 3
		--set @FinancialYear  = '2013'
		--set @Division = 75
		--set @Department = 5566
		--set @Period = 2
		
select

		IncidentRecno
		,SourceUniqueID
		,IncidentNumber
		,FinancialYearKey
		,FinancialYear
		,FirstDate
		,LastDate
		,FinancialMonthKey
		,TheMonth
		,ReportPeriod =		case 
								when @Period = 1
									then TheMonth
								else TheWeek
							end
		,TheWeek
		,WeekNoKey
		,TheDate
		,TheDateOriginal		
		,DivisionCode
		,DepartmentCode
		,ReportingTypeCode
		,ReportingGradeCode

from

(
			select
					IncidentRecno
					,SourceUniqueID
					,IncidentNumber
					,IncidentDescription
					,FinancialYearKey = @FinancialYear
					,Calendar.FinancialYear
					,FirstDate =				case when left(FinancialMonthKey,4) = @FinancialYear - 1 then dateadd(year, +1, FirstDate) else FirstDate end
					,LastDate =					case when left(FinancialMonthKey,4) = @FinancialYear - 1 then dateadd(year, +1, LastDate) else LastDate end
					,FinancialMonthKey
					,TheMonth =					case when left(FinancialMonthKey,4) = @FinancialYear - 1 then dateadd(year, +1, cast(TheMonth as date)) else cast(TheMonth as date) end 
					,TheWeek =					case when left(FinancialMonthKey,4) = @FinancialYear - 1 then dateadd(year, +1, cast(FirstDayOfWeek as date)) else cast(FirstDayOfWeek as date) end
					,WeekNoKey
					,TheDate =					case when left(FinancialMonthKey,4) = @FinancialYear - 1 then dateadd(year, +1, TheDate) else TheDate end
					,TheDateOriginal = TheDate
					,DivisionCode =				coalesce(Division.DivisionCode,0)
					,DepartmentCode =			coalesce(Department.DepartmentCode,0)
					,IncidentPersonTypeCode
					,ReportingTypeCode =		case 
													when (CauseGroup.CauseGroupCode = 49 or CauseCode in (82,11266,11366,11466,11566,11766))
														then 1	--Fall
													when IncidentType.IncidentTypeCode = 81 or CauseGroup.CauseGroupCode = 70
														then 2	--MedError
													when CauseGroup.CauseGroupCode in (99,121) and
														 CauseCode in ( 10176,								--PU Unclassified
																		7572,8473,8873,9776,				--PU Grade 1
																		7672,8573,8973,9876,11276,			--PU Grade 2
																		7772,8673,9073,9976,11376,			--PU Grade 3
																		7872,7972,8773,9773,70076,11476)	--PU Grade 4 
														then 3	--CMFT_PU 
													else 0 
												end
					,ReportingGradeCode =		case 
													when CauseGroup.CauseGroupCode in (99,121) and
														 CauseCode in ( 7572,8473,8873,9776,				--PU Grade 1
																		7672,8573,8973,9876,11276,			--PU Grade 2
																		7772,8673,9073,9976,11376,			--PU Grade 3
																		7872,7972,8773,9773,70076,11476 )	--PU Grade 4
														then cast(substring(CauseDescription,len(CauseDescription),1) as int)
													when (GradeTypeCode is null or CauseCode in ( 10176))	--PU Unclassified
														then 0
													when GradeTypeCode = 71									--7 Actual Harm Incident                            
														then 0
													when GradeTypeCode = 70									--6 Near Miss / No Harm                             
														then 1	
													else cast(left(GradeTypeDescription ,1) as int)
												end 
			from	
					Warehouse.Incident.Incident Incident
			  
			full outer join  
					Warehouse.Incident.Division Division
			on
					Incident.DivisionCode collate latin1_general_cs_as = Division.MixedCaseCode collate latin1_general_cs_as
					
			--full outer join  
			--		Warehouse.Incident.Directorate Directorate
			--on
			--		Incident.DirectorateCode collate latin1_general_cs_as = Directorate.MixedCaseCode collate latin1_general_cs_as
					
			full outer join  
					Warehouse.Incident.Department Department
			on
					Incident.DepartmentCode collate latin1_general_cs_as = Department.MixedCaseCode collate latin1_general_cs_as

			full outer join  
					Warehouse.Incident.Severity Severity
			on
					Incident.Severity collate latin1_general_cs_as = Severity.MixedCaseCode collate latin1_general_cs_as

			full outer join  
					Warehouse.Incident.GradeType GradeType
			on
					Incident.GradeType collate latin1_general_cs_as = GradeType.MixedCaseCode collate latin1_general_cs_as
					
			full outer join  
					Warehouse.Incident.IncidentType IncidentType
			on
					Incident.IncidentTypeCode collate latin1_general_cs_as = IncidentType.MixedCaseCode collate latin1_general_cs_as
					
			full outer join  
					Warehouse.Incident.CauseGroup CauseGroup
			on
					Incident.CauseGroupCode collate latin1_general_cs_as = CauseGroup.MixedCaseCode collate latin1_general_cs_as

			inner join  
					Warehouse.Incident.Cause Cause
			on
					Incident.IncidentCause1Code collate latin1_general_cs_as = Cause.MixedCaseCode collate latin1_general_cs_as

			inner join	
					(
						select 
							IncidentCode
							,IncidentPersonTypeCode
						from 
							Warehouse.Incident.IncidentPerson 
						where 
							IncidentPersonTypeCode = 1 --Patients Only
							
					)	IncidentPerson 
			on
					Incident.IncidentCode collate latin1_general_cs_as = IncidentPerson.IncidentCode collate latin1_general_cs_as

			inner join 
					WarehouseReporting.WH.Calendar Calendar
			on
					Incident.IncidentDate = Calendar.TheDate
			
			inner join
					(
						select 
								FinancialYear
								,FirstDate = min(TheDate)
								,LastDate = max(TheDate)
						from	
								WarehouseReporting.WH.Calendar
						where	
								left(FinancialMonthKey,4) = @FinancialYear -1
						or		
								left(FinancialMonthKey,4) = @FinancialYear
						group by
								FinancialYear
					) FinancialYear
			on
					Calendar.FinancialYear = FinancialYear.FinancialYear

)		Incidents

where
		ReportingTypeCode = @ReportType
and
		(
			@Division in (DivisionCode)
		or
			@Division is null
		)
and
		(
			@Department in (DepartmentCode)
		or
			@Department is null
		)
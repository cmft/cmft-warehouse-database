﻿

CREATE proc [RPT].[GetFragileFracturesWardStayDetails]

@SourcePatientNo varchar(20)
,@SourceSpellNo varchar(20)


as

select
	SourceSpellNo
	,SourcePatientNo
	,StartTime
	,EndTime
	,WardCode
	,TimeSpentOnWard = Datediff(mi,StartTime,EndTime)
	,TimeSpentOnWardText = 

	case 
				when ((DATEDIFF(hour,StartTime,EndTime))/168)=0 then
					case 
						when (((DATEDIFF(hour,StartTime,EndTime))/24)%7)=0 then '' 
						else CAST(((DATEDIFF(hour,StartTime,EndTime))/24)%7 as varchar) + ' day' 
					end
				else
					CAST((DATEDIFF(hour,StartTime,EndTime))/168 as varchar) + ' week ' +
					case when (((DATEDIFF(hour,StartTime,EndTime))/24)%7)=0 then '' 
					else CAST(((DATEDIFF(hour,StartTime,EndTime))/24)%7 as varchar) + ' day' end
				end




from
	APC.WardStay WardStay


where
	SourcePatientNo = @SourcePatientNo
AND SourceSpellNo = @SourceSpellNo

order by 
	StartTime asc



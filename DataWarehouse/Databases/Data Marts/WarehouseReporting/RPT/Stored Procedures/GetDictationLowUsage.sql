﻿
create proc RPT.GetDictationLowUsage

	@start date = null
	,@end date = null
	,@division varchar(100) = null
	,@specialty varchar(100) = null
	,@documents int = null
	,@subject varchar(50) = null

as

set @subject = coalesce(@subject, 'author')

/* Author */

if @subject = 'author'

begin

	select
		Division = coalesce(usergroup.Division, 'N\A')
		,Specialty = coalesce(usergroup.Specialty, 'N\A')
		,[User].HasVRLicence
		,[User].[User]
		,Documents = coalesce(Documents, 0)

	from
		Dictation.[User] [User]
		inner join Dictation.UserGroup usergroup
		on [User].UserCode = usergroup.UserCode
		left outer join
						(
						select
							AuthorCode
							,Documents = count(*)
						from         
							Dictation.Document document
						where
							document.CreationDate between @start and @end
						group by
							AuthorCode
						) authordocuments
		on authordocuments.AuthorCode = [User].UserCode
		
	where coalesce(Documents, 0) <= @documents
	and (usergroup.Division = @division or @division = '(Select All)')
	and (usergroup.SpecialtyCode = @specialty or @specialty is null)

end		


else if @subject = 'transcriptionist'	

begin		
		select
		Division = coalesce(usergroup.Division, 'N\A')
		,Specialty = coalesce(usergroup.Specialty, 'N\A')
		,[User].HasVRLicence
		,[User].[User]
		,Documents = coalesce(Documents, 0)

	from
		Dictation.[User] [User]
		inner join Dictation.UserGroup usergroup
		on [User].UserCode = usergroup.UserCode
		left outer join
						(
						select
							TranscriptionistCode
							,Documents = count(*)
						from         
							Dictation.Document document
						where
							document.CorrectionDate between @start and @end
						group by
							TranscriptionistCode
						) transcriptionistdocuments
		on transcriptionistdocuments.TranscriptionistCode = [User].UserCode
	where coalesce(Documents, 0) <= @documents 
	and (usergroup.Division = @division or @division = '(Select All)')
	and (usergroup.SpecialtyCode = @specialty or @specialty is null)
end


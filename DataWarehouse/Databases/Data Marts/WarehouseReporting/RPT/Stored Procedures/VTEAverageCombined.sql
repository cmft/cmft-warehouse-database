﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [RPT].[VTEAverageCombined]
(
	 @StartDate date, @EndDate date
)
as



SELECT
	 Exclusion = VTESpell.VTEExclusionReasonCode
	,DivisionCode = Directorate.DivisionCode
	,Division = Division.Division
	,SpecialtyCode = Specialty.NationalSpecialtyCode
	,Specialty = Specialty.NationalSpecialty
	,WardCode = Ward.WardCode
	,Ward = Ward.Ward
	,ConsultantCode = Encounter.ConsultantCode
	,Consultant = Consultant.Consultant
	,Cases = 1

	,CodedExclusionCases =
		case
		when
				Encounter.CodingCompleteDate IS NOT NULL
			AND VTESpell.VTEExclusionReasonCode is not null 
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
		else 0
		end

	,UncodedExclusionCases =
		case
		when
				Encounter.CodingCompleteDate IS NULL
			AND VTESpell.VTEExclusionReasonCode is not null
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
		else 0
		end

	,VTECompleteCases = 
		case VTESpell.VTECategoryCode
		when 'C' then 1
		else 0
		end

	,VTEIncompleteCases = 
		case VTESpell.VTECategoryCode
		when 'I' then 1
		else 0
		end

	,VTEMissingCases = 
		case VTESpell.VTECategoryCode
		when 'M' then 1
		else 0
		end
		
	,CodingCompleteCases = 
		CASE
		WHEN Encounter.CodingCompleteDate IS NOT NULL THEN 1
		ELSE 0
		END
		
	,CodingIncompleteCases = 
		CASE
		WHEN Encounter.CodingCompleteDate IS NULL THEN 1
		ELSE 0
		END
	, 'M' as Location
	, TheMonth
	, FinancialMonthKey
FROM
	WarehouseReporting.APC.Encounter

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = Encounter.AdmissionDate

left join WarehouseReporting.APC.VTESpell
on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

left join WarehouseReporting.WH.Directorate
on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

left join WarehouseReporting.WH.Division
on	Division.DivisionCode = Directorate.DivisionCode

--left join WarehouseReporting.PAS.Specialty
--on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join WarehouseReportingMerged.WH.Specialty
on Specialty.SourceSpecialtyCode = Encounter.SpecialtyCode 
and SourceContext = 'Torex PAS (Central)'

left join WarehouseReporting.PAS.Ward
on	Ward.WardCode = Encounter.StartWardTypeCode

left join WarehouseReporting.PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

where
	Encounter.FirstEpisodeInSpellIndicator = 1
and	(Calendar.TheDate >= @StartDate AND Calendar.TheDate <= @EndDate)
and	Encounter.PatientCategoryCode != 'RD'
and	(
		(
			Encounter.AgeCode like '%Years'
		and	left(Encounter.AgeCode , 2) > '17'
		)
	or	Encounter.AgeCode = '99+'
	)
UNION ALL

SELECT 
	 Exclusion = VTESpell.VTEExclusionReasonCode
	,DivisionCode = 'Trafford'
	,Division = 'Trafford'
	,SpecialtyCode = bedmanSpecialtyCode
	,Specialty = bedmanSpecialty
	,WardCode = VTE.[admittingward]
	,Ward = WardName
	,ConsultantCode = [ConsultantCode]
	,Consultant = isnull(VTE.[ConSurname] + ', ' + VTE.[ConFirstName], VTE.[ConSurname])
	,Cases = 1
	
	--- EXCLUDED AND NOT COMPLETE	
	,CodedExclusionCases =  case 
				when (VTESpell.VTEExclusionReasonCode is not null -- EXCLUDED?
				OR mau.[vte completed] = 'DVT')
				AND (Complete = 0 OR SUBSTRING(mau.[vte completed],1,1) = 'A') -- COMPLETE?
					then 1
				else 0
				end

	,UncodedExclusionCases = 0

	---- COMPLETE (INCLUDING EXLUSIONS)
	--,VTECompleteCases = case when (Complete = 1 OR SUBSTRING(mau.[vte completed],1,1) = 'A') then 1
	--    				end

	---- INCOMPLETE (INCLUDING EXLUSIONS)	    					
	--,VTEIncompleteCases =	case when (Complete = 1 OR SUBSTRING(mau.[vte completed],1,1) = 'A') then 0
	    		
	--    					end

	-- COMPLETE (INCLUDING EXLUSIONS)
	,VTECompleteCases = case when Complete = 1 then 1
						else case
						when mau.[vte completed] IS NOT NULL then 1
						else 0
	    				end end

	-- INCOMPLETE (INCLUDING EXLUSIONS)	    					
	,VTEIncompleteCases =	case when Complete = 1 then 0
							else 
								case when mau.[vte completed] IS NOT NULL then 0
								else 1   		
	    					end end

	,VTEMissingCases = 0
	
	-- ALL CODING IS DONE FOR TRAFFORD	
	,CodingCompleteCases = 1
		
	,CodingIncompleteCases = 0
	
	,'T' as Location
	
	, TheMonth
	, FinancialMonthKey
	
FROM [WarehouseReporting].[dbo].[AC_VTE_Cohorts] VTE

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = cast(AdmittedOn as date)

left join APC.VTESpellTrafford VTESpell
on	VTESpell.ProviderSpellNo = VTE.AdmissionNumber

left join dbo.VTE_Temp_MAU mau on VTE.AdmissionNumber = mau.AdmissionNumber and [vte completed] <> 'None'

	where (Calendar.TheDate >= @StartDate AND Calendar.TheDate <= @EndDate)




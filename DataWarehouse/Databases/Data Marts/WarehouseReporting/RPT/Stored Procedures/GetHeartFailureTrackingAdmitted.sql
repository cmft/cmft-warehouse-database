﻿

CREATE proc [RPT].[GetHeartFailureTrackingAdmitted]

@AdultChild varchar(1) = 'B'

as


--this query shows you all patients (that have been discharged in the past 12 months and were diagnosed with Heart Failure) 
--that have been admitted in the past 7 days

--DECLARE Variables
DECLARE @EndDate smalldatetime
DECLARE @Patients TABLE 
(SourcePatientNo varchar(20))
DECLARE @Dates TABLE 
(DateField DATE)

--SET Variables
SET @EndDate = DATEADD(DAY, -7, convert(DATE,getdate()))


--1.
--need a subquery to bring back patients that have been diagnosed with Heart Failure

insert into @Patients(SourcePatientNo)

	SELECT 
		Encounter.SourcePatientNo
	FROM 
		--primarydiagnosis code matches
		[WarehouseReporting].APC.Encounter Encounter

	WHERE 
		Encounter.PrimaryDiagnosisCode in ('I11.0','I13.0','I13.2','I50.0','I50.9','I50.1')
	
	UNION
	
	SELECT 
		Encounter.SourcePatientNo
	FROM 
		--query all other Diagnosis (use apc.diagnosis)
		[WarehouseReporting].APC.Encounter Encounter
			
	INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
	ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
	AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
	AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
	AND Diagnosis.DiagnosisCode in ('I11.0','I13.0','I13.2','I50.0','I50.9','I50.1')
--2. insert 7days
insert into @Dates(DateField)
SELECT convert(DATE,getdate())
UNION
SELECT DATEADD(DAY, -1, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -2, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -3, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -4, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -5, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -6, convert(DATE,getdate()))



--2. Get data for report

SELECT
	AdmissionDate = DateField
	,InternalNo = Admissions.SourcePatientNo
	,Admissions.DistrictNo
	,Admissions.CasenoteNumber
	,Admissions.PatientForename
	,Admissions.PatientSurname
	,Admissions.DateOfBirth
	,Admissions.ConsultantCode
	,Admissions.SpecialtyCode
	,Admissions.DischargeDate
	,AdmitWard = Admissions.StartWardTypeCode
	,LOSAdmit = Admissions.LOS
	,AgeAtAdmission = dbo.f_GetAge(Admissions.DateOfBirth, DateField)
	,SiteCode
FROM
	@Dates d
LEFT JOIN (
			SELECT 
				Encounter.SourcePatientNo
				,Encounter.DistrictNo
				,Encounter.CasenoteNumber
				,Encounter.PatientForename
				,Encounter.PatientSurname
				,Encounter.DateOfBirth
				,Encounter.ConsultantCode
				,Encounter.SpecialtyCode
				,Encounter.AdmissionDate
				,Encounter.DischargeDate
				,Encounter.StartWardTypeCode
				,Encounter.LOS
				,Encounter.SiteCode

			FROM 
				[WarehouseReporting].APC.Encounter Encounter 

			WHERE 
				AdmissionDate>@EndDate
			and Encounter.SourcePatientNo in (SELECT SourcePatientNo FROM @Patients)
		)Admissions
ON Admissions.AdmissionDate = d.DateField
WHERE dbo.f_GetAge(DateOfBirth, AdmissionDate) BETWEEN (CASE @AdultChild WHEN 'A' THEN 18 
						  WHEN 'C' THEN 0
						  WHEN 'B' THEN 0 END)
AND (CASE @AdultChild WHEN 'A' THEN 999 
						  WHEN 'C' THEN 17 
						  WHEN 'B' THEN 999 END)
ORDER BY
	d.DateField DESC







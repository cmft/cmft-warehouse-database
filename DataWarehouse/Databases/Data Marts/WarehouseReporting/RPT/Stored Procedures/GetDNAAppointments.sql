﻿


CREATE PROCEDURE [RPT].[GetDNAAppointments]
(
@AppointmentDate Date
)
AS


SELECT  
   	  Dir.Division   
   	  ,NationalReferringSpecialtyCode = RefSpec.NationalSpecialtyCode
      ,PASReferringSpecialty = RefSpec.Specialty
   	  ,AttendanceType =
		case
		when FirstAttendanceFlag = 1 then 'New'
		else 'FollowUp'
		end
    
      ,WkStartDate = CONVERT(char(8),Cal.FirstDayOfWeek,3)
      
      ,FinancialYear
      ,FinancialWeekNo = LEFT(FinancialMonthKey,4)*100+(((datepart(dy,AppointmentDate) 
							+ case when right(FinancialMonthKey,2) in ('10','11','12') then datepart(dy,(convert(varchar,'31 Dec '+ LEFT(FinancialMonthKey,4),10))) else 0 end
							- datepart(dy,(convert(varchar,'01 Apr '+ LEFT(FinancialMonthKey,4),10)))+datepart(dw,(convert(varchar,'01 Apr '+ LEFT(FinancialMonthKey,4),10)))-2)/7) )

      ,WeekNoKey
      , DNAs = sum(Case when AttendanceOutcomeCode = 'DNA' then 1 
													else 0
				end)
      , Activity = COUNT(*)
      
      
      
       FROM WarehouseReporting.OP.Encounter AS OPAct
  
  
  
  LEFT OUTER JOIN WarehouseReporting.PAS.Specialty AS RefSpec ON OPAct.ReferringSpecialtyCode = RefSpec.SpecialtyCode
  left outer join WarehouseReporting.PAS.Directorate as Dir on OPAct.DirectorateCode = Dir.DirectorateCode
  left outer join WarehouseReporting.WH.Calendar as Cal on OPAct.AppointmentDate  = Cal.TheDate
  
  
  WHERE	
			
			AppointmentDate >= @AppointmentDate --'01 APR 2011'
			--AND AppointmentDate >= '01 Dec 2011'
			and DNACode in (3,5,6)
			and ReferringSpecialtyTypeCode = 'I'
						and AdminCategoryCode <> '02'
			and IsWardAttender = 0	
			and Division <> 'MH TRUST'		
  
  Group By
  
  Dir.Division   
   	  ,RefSpec.NationalSpecialtyCode
      ,RefSpec.Specialty
   	  ,
		case
		when FirstAttendanceFlag = 1 then 'New'
		else 'FollowUp'
		end
      ,CONVERT(char(8),Cal.FirstDayOfWeek,3)
      
      
      ,FinancialYear
      ,LEFT(FinancialMonthKey,4)*100+(((datepart(dy,AppointmentDate) 
						+ case when right(FinancialMonthKey,2) in ('10','11','12') then datepart(dy,(convert(varchar,'31 Dec '+ LEFT(FinancialMonthKey,4),10))) else 0 end
						- datepart(dy,(convert(varchar,'01 Apr '+ LEFT(FinancialMonthKey,4),10)))+datepart(dw,(convert(varchar,'01 Apr '+ LEFT(FinancialMonthKey,4),10)))-2)/7) )

      ,WeekNoKey
      


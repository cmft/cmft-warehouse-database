﻿CREATE procedure [RPT].[VTEDetail] --201109 , null , null , null , null , 1
(
	 @FinancialMonthKey int
	,@DivisionCode varchar(10)
	,@SpecialtyCode varchar(10)
	,@WardCode varchar(10)
	,@ConsultantCode varchar(10)
	,@CodingMode int
)

as
select

	 Division.Division
	,VTESpell.VTEExclusionReason
	,Encounter.ClinicalCodingStatus
	,VTESpell.VTECategory
	,Encounter.CasenoteNumber
	,Encounter.DistrictNo
	,Encounter.NHSNumber
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	--,Specialty.NationalSpecialtyCode
	,Specialty.NationalSpecialty
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.SiteCode
	,Encounter.ConsultantCode
	,Consultant.Consultant
	,Specialty.SpecialtyCode
	,Specialty.Specialty
	,Encounter.PatientCategoryCode
	,Ward.WardCode
	,Ward.Ward
	,Encounter.PrimaryDiagnosisCode
	,Diagnosis.DiagnosisChapter
	,Diagnosis.Diagnosis
	,Encounter.PrimaryOperationCode
	,Operation.OperationChapter
	,Operation.Operation
	,CasenoteLocation = CasenoteLocation.CasenoteLocation
	,CasenoteBorrower.Borrower
from
	WarehouseReporting.APC.Encounter

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = Encounter.AdmissionDate

left join WarehouseReporting.APC.VTESpell
on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

left join WarehouseReporting.WH.Directorate
on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

left join WarehouseReporting.WH.Division
on	Division.DivisionCode = Directorate.DivisionCode

left join WarehouseReporting.PAS.Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join WarehouseReporting.PAS.Ward
on	Ward.WardCode = Encounter.StartWardTypeCode

left join WarehouseReporting.PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join WarehouseReporting.WH.Diagnosis
on	Diagnosis.DiagnosisCode = Encounter.PrimaryDiagnosisCode

left join WarehouseReporting.WH.Operation
on	Operation.OperationCode = Encounter.PrimaryOperationCode

left join WarehouseReporting.PAS.Casenote
on	Casenote.SourcePatientNo = Encounter.SourcePatientNo
and	Casenote.CasenoteNumber = Encounter.CasenoteNumber

left join WarehouseReporting.PAS.CasenoteLocation
on	CasenoteLocation.CasenoteLocationCode = Casenote.CasenoteLocationCode

left join WarehouseReporting.PAS.CasenoteBorrower
on	CasenoteBorrower.BorrowerCode = Casenote.CurrentBorrowerCode

where
	Encounter.FirstEpisodeInSpellIndicator = 1
and	Calendar.FinancialMonthKey = @FinancialMonthKey
and	Encounter.PatientCategoryCode != 'RD'
and	(
		(
			Encounter.AgeCode like '%Years'
		and	left(Encounter.AgeCode , 2) > '17'
		)
	or	Encounter.AgeCode = '99+'
	)
and (
		Directorate.DivisionCode = @DivisionCode
	or	@DivisionCode is null
	)
and	(
		Specialty.NationalSpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)
and	(
		Encounter.StartWardTypeCode = @WardCode
	or	@WardCode is null
	)
and	(
		Encounter.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and
	(
		@CodingMode = 0
	or	(
			@CodingMode = 1
		and	Encounter.ClinicalCodingStatus = 'C'
		and	VTESpell.VTEExclusionReasonCode is null
		and	VTESpell.VTECategoryCode != 'C'
		)
	)

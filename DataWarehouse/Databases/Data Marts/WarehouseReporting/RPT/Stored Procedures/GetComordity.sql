﻿

CREATE proc [RPT].[GetComordity]
(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
)

as

set @consultant = --'CMMC\neil.parrott'
					case
						when @consultant in
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\neil.parrott'
						/* for CSS */
						when @consultant in
											(
											'CMMC\John.Butler'
											,'CMMC\Rachael.Challiner'
											,'CMMC\Bernard.Foex'
											,'CMMC\Ruari.Greer'
											,'CMMC\Steve.Jones'
											,'CMMC\Michael.Parker'
											)
						then 'CMMC\Jane.Eddleston'
						else @consultant
					end

/* Copy of dbo.OlapCalendar to reduce contention and blocking */

declare @OlapCalendar table 
(
	TheDate date not null primary key
	,TheMonth nvarchar(34) null
);

insert into @OlapCalendar
(
	TheDate
	,TheMonth
)

select 
	TheDate
	,TheMonth
from 
	WarehouseOLAP.dbo.OlapCalendar with (nolock);


select 
	--PatientName = encounter.PatientSurname + ', ' + encounter.PatientForename
	--,encounter.NHSNumber
	--,encounter.EpisodeStartDate
	DiagnosisMonth = cast(calendar.TheMonth as date)
	,Diagnosis.Diagnosis
	,Cases = 1
		
from
	APC.Encounter 
	
		unpivot
			(
			Code for [Sequence] in 
			(
			--PrimaryDiagnosisCode
			--,SubsidiaryDiagnosisCode
			SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			)
			) encounter
										
inner join WH.Diagnosis Diagnosis
on encounter.Code = Diagnosis.DiagnosisCode

inner join @OlapCalendar calendar
on calendar.TheDate = EpisodeStartDate

inner join PAS.Consultant consultant
on	encounter.ConsultantCode = consultant.ConsultantCode
												
where
	consultant.DomainLogin = @consultant
	and encounter.EpisodeStartDate between @start and @end
	and Diagnosis.IsCharlsonFlag = 1
	--and encounter.PrimaryDiagnosisCode not like 'O04%'
	--and encounter.PrimaryOperationCode not like 'Q11%'
	--and encounter.PrimaryOperationCode not like 'Q12%'
	
option (recompile)



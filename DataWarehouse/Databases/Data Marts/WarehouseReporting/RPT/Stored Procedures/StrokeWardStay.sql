﻿
CREATE proc [RPT].[StrokeWardStay]

as

--this report should be ran within first 10 days of the month and it will show data for the previous month

--Declare Variables
DECLARE @FirstDayOfMonth DATE
DECLARE @LastDayOfMonth DATE

--Set Variables
SET @FirstDayOfMonth = (convert(varchar(6),dateadd(m,-1,getdate()),112) + '01')
SET @LastDayOfMonth = dateadd(d,-1,(convert(varchar(6),getdate(),112) + '01'))

SELECT
	PCTDescription
	,InternalNo
	,EpisodeNo
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,AdmissionTime
	,DischargeTime
	,Numerator = LOSStrokeWard
	,DenominatorExcAE = Denominator
	,PercentageOnStrokeWardExcAE = ((LOSStrokeWard*1.00)/(Denominator*1.00))*100
	,DenominatorIncAE = Denominator + ISNULL(AETime,0)
	,PercentageOnStrokeWardIncAE = ((LOSStrokeWard*1.00)/(Denominator + ISNULL(AETime,0))*1.00)*100

FROM (
					SELECT 
						PCTDescription = PCT.[PCT Description]
						,InternalNo = Encounter.SourcePatientNo
						,EpisodeNo = Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard = ISNULL(LOSStrokeWard,0)
						,Denominator = SUM(DATEDIFF(mi,Encounter.EpisodeStartTime,Encounter.EpisodeEndTime))
						,AETime = TimeSpentInAE

					FROM 
						WarehouseReporting.apc.Encounter Encounter

					INNER JOIN Information.dbo.PCT PCT 
					ON PCT.[PCT Code] = Encounter.PCTCode

					--LOS on Stroke Ward
					LEFT JOIN (		SELECT 
										ProviderSpellNo,
										LOSStrokeWard = SUM(DATEDIFF(mi,WardStay.[StartTime],WardStay.EndTime)) 
									FROM 
										[WarehouseReporting].[APC].[WardStay] 
									WHERE 
										[WardCode] in ('30M','31M','31R') 
									GROUP BY ProviderSpellNo) WardStay 
					ON WardStay.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

					--LOS in AE
					LEFT JOIN (	
									SELECT distinct
										APCEncounter.DistrictNo,
										APCEncounter.AdmissionTime,
										TimeSpentInAE = StageDurationMinutes
									FROM
										WarehouseReporting.APC.Encounter APCEncounter

									INNER JOIN WarehouseReporting.AE.Encounter AEEncounter
									ON AEEncounter.DistrictNo = APCEncounter.DistrictNo
									and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR,-2,APCEncounter.AdmissionTime) and DATEADD(HOUR,+2,APCEncounter.AdmissionTime)

									INNER JOIN WarehouseReporting.AE.Fact Fact 
									ON Fact.SourceUniqueID = AEEncounter.SourceUniqueID

									WHERE Fact.StageCode = 'INDEPARTMENTADJUSTED') LOSAE 	
					ON LOSAE.DistrictNo = Encounter.DistrictNo
					AND LOSAE.AdmissionTime = Encounter.AdmissionTime
					
					--only bring back stroke patients
					INNER JOIN (	SELECT distinct
										SourcePatientNo,SourceSpellNo 
									FROM 
										WarehouseReporting.apc.Encounter Encounter
									WHERE 
										PrimaryDiagnosisCode like 'I61%' or PrimaryDiagnosisCode like 'I63%' or PrimaryDiagnosisCode like 'I64%') StrokePatients
					ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
					AND StrokePatients.SourceSpellNo = Encounter.SourceSpellNo
					
					WHERE
						DischargeDate between @FirstDayOfMonth and @LastDayOfMonth
					--and Encounter.sourcepatientno = '3257952'
					--AgeCategory must equal A
					AND CASE 
							WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionDate)))))-1900)<19
							THEN 'C'
							ELSE 'A'
						END = 'A'
										

					GROUP BY 
						PCT.[PCT Description]
						,Encounter.SourcePatientNo
						,Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard
						,TimeSpentInAE

)myqry


ORDER BY CONVERT(INT,InternalNo) ASC




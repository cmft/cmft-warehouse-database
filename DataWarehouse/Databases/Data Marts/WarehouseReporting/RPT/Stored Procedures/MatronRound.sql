﻿CREATE procedure RPT.MatronRound
	 @FromDate date
	,@ToDate date
as

--WITH XMLNAMESPACES (N'http://www..nhs.uk/InfoPath.xsd' AS ns)
WITH XMLNAMESPACES (N'http://schemas.microsoft.com/office/infopath/2003/myXSD/2009-11-10T09:47:10' as my)

select
	 FormId
	,FormDate = InfopathForm.FormXML.value('(my:myFields/my:Date)[1]', 'DATE') 
	,FormDate1 = InfopathForm.FormXML.value('(my:myFields/my:Date1)[1]', 'DATETIME') 
	,FormTime1 = InfopathForm.FormXML.value('(my:myFields/my:Time1)[1]', 'varchar(1000)') 
	,SignedBy = InfopathForm.FormXML.value('(my:myFields/my:Signed)[1]', 'varchar(1000)')
	,CreatedDate = InfopathForm.FormXML.value('(my:myFields/my:CreatedDate)[1]', 'DATE')
	,SignedDate = InfopathForm.FormXML.value('(my:myFields/my:SignedDate)[1]', 'DATE')
	,Ward = InfopathForm.FormXML.value('(my:myFields/my:Ward)[1]', 'varchar(1000)')
	,AtRiskOfFallCases = InfopathForm.FormXML.value('(my:myFields/my:FallsYno)[1]', 'varchar(1000)')
	,PressureUlcerCasesPresent = InfopathForm.FormXML.value('(my:myFields/my:group3/my:PressureUlcerGroup/my:PU_yn)[1]', 'varchar(1000)')
	,PlannedDischarges = InfopathForm.FormXML.value('(my:myFields/my:PlannedDischarges)[1]', 'varchar(1000)')
	,LCPCases = InfopathForm.FormXML.value('(my:myFields/my:LCP_number)[1]', 'varchar(1000)')
	,EWSGreaterThan3Cases = InfopathForm.FormXML.value('(my:myFields/my:EWSOver3)[1]', 'varchar(1000)')
	,ControlledDrugsCheckCompleted = InfopathForm.FormXML.value('(my:myFields/my:ControlDrugsCheck)[1]', 'varchar(1000)')
	,PatientsOnIVDrugsCases = InfopathForm.FormXML.value('(my:myFields/my:IntraDrugsY)[1]', 'varchar(1000)')
	,NasogastricFeedCases = InfopathForm.FormXML.value('(my:myFields/my:NasoFeedY)[1]', 'varchar(1000)')
	,PatientsInfected = InfopathForm.FormXML.value('(my:myFields/my:HowmanyPatientsInfection)[1]', 'varchar(1000)')

	,DiarrhoeaPresent = InfopathForm.FormXML.value('(my:myFields/my:HowManyPatientsType/@my:Diarrhoea)[1] ', 'varchar(1000)')

	,NorovirusPresent = InfopathForm.FormXML.value('(my:myFields/my:HowManyPatientsType/@my:norovirus)[1]', 'varchar(1000)')
	,CDiffPresent = InfopathForm.FormXML.value('(my:myFields/my:HowManyPatientsType/@my:cdiff)[1]', 'varchar(1000)')
	,MRSAColonisedPresent = InfopathForm.FormXML.value('(my:myFields/my:HowManyPatientsType/@my:MRSA-colonised)[1]', 'varchar(1000)')
	,MRSABactaraemiaPresent = InfopathForm.FormXML.value('(my:myFields/my:HowManyPatientsType/@my:mrsa-bactaraemia)[1]', 'varchar(1000)')
	,CPCPresent = InfopathForm.FormXML.value('(my:myFields/my:HowManyPatientsType/@my:CPC)[1]', 'varchar(1000)')
	,ESBLPresent = InfopathForm.FormXML.value('(my:myFields/my:HowManyPatientsType/@my:ESBL)[1]', 'varchar(1000)')
from
	InfoPathForms.dbo.InfopathForm
where
	FormType = 'Matron''s Daily Rounding'
and	InfopathForm.FormXML.value('(my:myFields/my:Date1)[1]', 'DATETIME') between @FromDate and @ToDate
order by
	FormDate1
	,Ward
﻿
CREATE procedure [RPT].[CasenoteFutureBooking]
(
	 @FromDate date
	,@ToDate date
	,@EncounterTypeCode varchar(3)
	,@ClinicCode varchar(20)
	,@WardCode varchar(20)
	,@SiteCode varchar(4)
	,@ServicePointCode varchar(10)
)
as

	/******************************************************************************
	**  Name: RPT.CasenoteFutureBooking
	**  Purpose: 
	**
	**  Casenotes of future bookings
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	**             CB         Initial Coding
	**				DG			Repointed columns to additonal casenote loan columns
	******************************************************************************/

----debug
--declare
--	 @FromDate date = '1 Mar 2013'
--	,@ToDate date = '31 Mar 2013'

select
	 Encounter.Surname
	,Encounter.Forenames
	,Encounter.DateOfBirth
	,Encounter.DistrictNo
	,Encounter.EncounterTypeCode
	,Encounter.BookingDate
	,Encounter.BookingTime
	,Encounter.SourcePatientNo
	,Encounter.SourceEntityRecno
	,Encounter.EncounterDate
	,Encounter.EncounterTime
	,Encounter.BookingCasenoteNo
	,Encounter.WardCode
	,Ward.Ward
	,Encounter.ClinicCode
	,Clinic.Clinic
	,Encounter.CasenoteNo
	,Encounter.AllocatedDate
	,Encounter.CasenoteLocationCode
	,CasenoteLocation.CasenoteLocation
	,Encounter.CasenoteLocationDate
	,Encounter.CasenoteStatus
	,Encounter.WithdrawnDate
	,Comment = 
				case
					when Encounter.CurrentLoanComment is null
					then Encounter.CurrentLoanReason
					else coalesce(Encounter.CurrentLoanComment, '') + '. ' + coalesce(Encounter.CurrentLoanReason,'')
				end
	,CurrentBorrowerCode = Encounter.CurrentLoanBorrowerCode
	,CurrentBorrower = CasenoteBorrower.Borrower
	,Encounter.ServicePointCode
from
	WarehouseOLAP.dbo.BasePASCasenoteBooking Encounter

left join Warehouse.PAS.CasenoteLocation
on	CasenoteLocation.CasenoteLocationCode = Encounter.CasenoteLocationCode

left join Warehouse.PAS.CasenoteBorrower
on	CasenoteBorrower.BorrowerCode = Encounter.CurrentLoanBorrowerCode

left join Warehouse.PAS.Ward
on	Ward.WardCode = Encounter.WardCode

left join Warehouse.PAS.Clinic
on	Clinic.ClinicCode = Encounter.ClinicCode

where
	EncounterDate between @FromDate and @ToDate
and	(
		Encounter.ClinicCode = @ClinicCode
	or	@ClinicCode is null
	)
and	(
		Encounter.EncounterTypeCode = @EncounterTypeCode
	or	@EncounterTypeCode is null
	)
and	(
		Encounter.WardCode = @WardCode
	or	@WardCode is null
	)
and	(
		Encounter.SiteCode = @SiteCode
	or	@SiteCode is null
	)
and	(
		Encounter.ServicePointCode = @ServicePointCode
	or	@ServicePointCode is null
	)

and	Encounter.OtherOPWaits + Encounter.OtherAPCWaits + Encounter.IsCurrentInpatient = 0

order by
	 Encounter.EncounterTime
	,Encounter.Surname
	,Encounter.Forenames

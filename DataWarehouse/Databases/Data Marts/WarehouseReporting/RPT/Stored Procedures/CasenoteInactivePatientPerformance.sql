﻿
CREATE procedure [RPT].[CasenoteInactivePatientPerformance]

AS

	/******************************************************************************
	**  Name: RPT.CasenoteInactivePatientPerformance
	**  Purpose: 
	**
	**  Casenotes from Inactive Patients - Performance Report
	**  Looks at the number of casenotes and the number withdrawn from inactive patients assigned to batch
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 20.03.13    MH         Initial Coding
	******************************************************************************/

	SELECT
		 BatchId				= PMI.BatchId
		,BatchName				= Batch.BatchName
		,BatchCreated			= Batch.DateCreated
		,Patients				= COUNT(PMI.InternalPatientNumber)
		,CaseNotes				= SUM(CN.CaseNotes)
		,CaseNotesWithdrawn		= SUM(CN.CaseNotesWithdrawn)
		,LatestWithdrawnDate	= MAX(CN.LatestWithdrawnDate)

	FROM
		WarehouseOLAP.PMI.BasePASPatientIndex PMI
	INNER JOIN WarehouseOLAP.PMI.BasePASPatientBatch Batch
		ON Batch.BatchId = PMI.BatchId
	CROSS APPLY
	(
		SELECT
			 InternalPatientNumber	= Casenote.SourcePatientNo
			,CaseNotes				= COUNT(Casenote.CasenoteNumber)
			,CaseNotesWithdrawn		= SUM(CASE WHEN COALESCE(Casenote.CasenoteStatus , '') = 'WITHDRAWN' THEN 1 ELSE 0 END)
			,LatestWithdrawnDate	= MAX(Casenote.WithdrawnDate)
		FROM
			Warehouse.PAS.Casenote
		WHERE
			Casenote.SourcePatientNo = PMI.InternalPatientNumber
			AND COALESCE(Casenote.WithdrawnDate,'2999-12-31') >= Batch.DateCreated		-- Any big date
		GROUP BY
			Casenote.SourcePatientNo
	) CN

	WHERE
			Batch.IsActive = 1
		AND PMI.HasDuplicates = 0

	GROUP BY
		 PMI.BatchId
		,Batch.BatchName
		,Batch.DateCreated

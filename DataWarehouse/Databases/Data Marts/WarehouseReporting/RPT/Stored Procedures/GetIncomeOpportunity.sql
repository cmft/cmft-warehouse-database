﻿

CREATE proc [RPT].[GetIncomeOpportunity]
(
@division varchar(50) = null
)

as

--select distinct SourceUniqueID,OpportunityFlag
--     --HRGUplift.*
--	 --SourceUniqueID
--	 --,SequenceNo = row_number() over (partition by SourceUniqueID order by PreviousDiagnosisCode)
--	 ----,DiagnosisCodeMatch = 
--		----				case
--		----					when [DiagnosisCode] is not null
--		----					then 1
--		----					else null
--		----				end
	 
--from
--	[Income].[HRGUplift] HRGUplift

--left outer join 
--	[Income].[HRGComplication]
--on
--	[ListID] = 
--				case
--					when PatientAge < 19
--					then replace([OpportunityFlag], '_p_','_')
--					else [OpportunityFlag]
--				end
--and
--	PreviousDiagnosisCode = [DiagnosisCode]

----where
----	ListID is not null

--where
--	OpportunityFlag is not null
--and
--	ListID is not null


--select
--	*
--from
--	[Income].[HRGGroupToSplit]
--where
--	HRGRoot = left('EA31Z', 4)

--select 
--	*
--from
--	[Income].[HRGComplication]
--where
--	ListID = 'LA_cc'
	



select
	Period
	,Position
	,Version
	,SourceUniqueID
	,CasenoteNumber
	,AdmissionTime
	,[PatientAge]
	,Division
	,[NationalSpecialty]
	,Consultant
	,HRGCode
	,OpportunityFlag
	,DiagnosisCode1 = [1]
	,DiagnosisCode2 = [2]
	,DiagnosisCode3 = [3]
	,DiagnosisCode4 = [4]
	,DiagnosisCode5 = [5]
	,DiagnosisCode6 = [6]
	,DiagnosisCode7 = [7]
	,DiagnosisCode8 = [8]
	,DiagnosisCode9 = [9]
	,DiagnosisCode10 = [10]
from

(
select 
	Period
	,Position
	,Version
	,SourceUniqueID
	,CasenoteNumber
	,AdmissionTime
	,PatientAge
	,Directorate.Division
	,[NationalSpecialty]
	,Consultant.Consultant
	,HRGCode
	,OpportunityFlag
	--,PreviousDiagnosisCode
	,Diagnosis = 
			case
				when Diagnosis.IsAlwaysPresentFlag = 1
				then Diagnosis.Diagnosis + ' (' + cast(cast(PreviousAdmissionTime as date) as varchar) + ')  *Always Present*'
				else Diagnosis.Diagnosis + ' (' + cast(cast(PreviousAdmissionTime as date) as varchar) + ')' 
			end
	,SequenceNo = row_number() over (partition by SourceUniqueID order by PreviousDiagnosisCode)
from
	[Income].[HRGUplift] HRGUplift

inner join
	[PAS].Directorate Directorate
on	Directorate.DirectorateCode = HRGUplift.EndDirectorateCode

inner join
	[WH].[Diagnosis] Diagnosis
on	Diagnosis.DiagnosisCode = HRGUplift.PreviousDiagnosisCode

inner join
	[PAS].[Specialty] Specialty
on	Specialty.SpecialtyCode = HRGUplift.SpecialtyCode

left outer join
	[PAS].[Consultant] Consultant
on	Consultant.ConsultantCode = HRGUplift.ConsultantCode

inner join
	[Income].[HRGComplication] HRGComplication
on
	[ListID] = 
				case
					when PatientAge < 19
					then replace([OpportunityFlag], '_p_','_')
					else [OpportunityFlag]
				end
and
	PreviousDiagnosisCode = HRGComplication.DiagnosisCode

) HRGUplift

pivot

(

	min(Diagnosis) for SequenceNo in
									(
										[1]
										,[2]
										,[3]
										,[4]
										,[5]
										,[6]
										,[7]
										,[8]
										,[9]
										,[10]
									)
) diagnosispivot

where
	(Division = @division or @division is null)



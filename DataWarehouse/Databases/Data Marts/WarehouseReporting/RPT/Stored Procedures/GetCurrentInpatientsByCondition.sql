﻿

create proc [RPT].[GetCurrentInpatientsByCondition]
(@proc varchar (10))

as


if @proc = 'FNOF'

begin

select
	 PatientName = encounter.PatientSurname + ', ' +  encounter.PatientForename
	,CasenoteNumber = encounter.CasenoteNumber 		
	,ProcedureCode = operation.OperationCode1
	,[Procedure] = operation.operation
	,InSuiteTime = operationdetail.InSuiteTime
	,OperationStartDate = operationdetail.OperationStartDate
	,InAnaestheticTime = operationdetail.InAnaestheticTime
	,OperationEndDate = operationdetail.OperationEndDate
	,InRecoveryTime = operationdetail.InRecoveryTime
	,AdmissionTime = encounter.AdmissionTime
	,EpisodeStartTime = encounter.EpisodeStartTime
	,EpisodeEndTime = encounter.EpisodeEndTime
	,EpisodeLOS = datediff(day, encounter.EpisodeStartDate, isnull(encounter.EpisodeEndDate, getdate()))
	,SpellLOS = datediff(day, encounter.AdmissionTime, isnull(encounter.DischargeTime, getdate()))
	,AdmissionToOperation = datediff(hour, encounter.AdmissionTime, operationdetail.OperationStartDate)
	,Consultant = consultant.Consultant
	,Division = division.Division
	,WardCode =
			(
			select top 1 WardCode
			from Warehouse.APC.WardStay ws 
			where ws.ProviderSpellNo = encounter.ProviderSpellNo
			order by StartTime desc
			)

from 
	Warehouse.Theatre.ProcedureDetail proceduredetail
	
	inner join Warehouse.Theatre.OperationDetail operationdetail 
	on proceduredetail.OperationDetailSourceUniqueID = operationdetail.SourceUniqueID
	
	inner join Warehouse.APC.EncounterProcedureDetail encounterproceduredetail 
	on encounterproceduredetail.ProcedureDetailSourceUniqueID = proceduredetail.SourceUniqueID
	
	inner join Warehouse.APC.Encounter encounter 
	on encounter.EncounterRecno = encounterproceduredetail.EncounterRecno
	
	inner join Warehouse.Theatre.Operation operation 
	on proceduredetail.ProcedureCode = operation.OperationCode
	
	inner join Warehouse.PAS.Consultant consultant
	on encounter.ConsultantCode = consultant.ConsultantCode
	
	inner join Warehouse.WH.Directorate directorate 
	on directorate.DirectorateCode = encounter.EndDirectorateCode
	
	inner join Warehouse.WH.Division division
	on division.DivisionCode = directorate.DivisionCode


where 
(
	operation.OperationCode1 like 'W19%'
	or operation.OperationCode1 like 'W24%'
	or operation.OperationCode1 like 'W46%'	
)

	--o.OperationCode1 in 
	--				(
	--				'W19.1'
	--				,'W19.2'
	--				,'W19.3'
	--				,'W47.1'
	--				,'W46.1'
	--				,'W38.1'
	--				,'W37.1'
	--				)
	
--Internal fixation SHS = W19.1 Primary open reduction of neck of femur and open fixation using pin and plate( includes SHS DHS DCS)
--Internal fixation Screws= W19.1 as above includes DHS( dynamic hip screw)
--Internal fixation IM nails long W19.2 ORIF long bone using rigid nail 
--W19.3 ORIF long bone using flexible nail
--Arthroplasty hemi uncemented uncoated/coated = W47.1 Bipolar/Unipolar
--Arthroplasty hemi cemented = W46.1 Bipolar/Unipolar
--Arthroplasty THR uncemented uncoated/coated = W38.1
--Arthroplasty THR cemented = W37.1

and 
	encounter.EpisodeEndDate is null
	and Division <> 'Childrens'
	
end	

else if @proc = 'HK'

begin
	
select
	 PatientName = encounter.PatientSurname + ', ' +  encounter.PatientForename
	,CasenoteNumber = encounter.CasenoteNumber 		
	,ProcedureCode = operation.OperationCode1
	,[Procedure] = operation.operation
	,InSuiteTime = operationdetail.InSuiteTime
	,OperationStartDate = operationdetail.OperationStartDate
	,InAnaestheticTime = operationdetail.InAnaestheticTime
	,OperationEndDate = operationdetail.OperationEndDate
	,InRecoveryTime = operationdetail.InRecoveryTime
	,AdmissionTime = encounter.AdmissionTime
	,EpisodeStartTime = encounter.EpisodeStartTime
	,EpisodeEndTime = encounter.EpisodeEndTime
	,EpisodeLOS = datediff(day, encounter.EpisodeStartDate, isnull(encounter.EpisodeEndDate, getdate()))
	,SpellLOS = datediff(day, encounter.AdmissionTime, isnull(encounter.DischargeTime, getdate()))
	,AdmissionToOperation = datediff(hour, encounter.AdmissionTime, operationdetail.OperationStartDate)
	,Consultant = consultant.Consultant
	,Division = division.Division
	,WardCode =
			(
			select top 1 WardCode
			from Warehouse.APC.WardStay ws 
			where ws.ProviderSpellNo = encounter.ProviderSpellNo
			order by StartTime desc
			)

from 
	Warehouse.Theatre.ProcedureDetail proceduredetail
	
	inner join Warehouse.Theatre.OperationDetail operationdetail 
	on proceduredetail.OperationDetailSourceUniqueID = operationdetail.SourceUniqueID
	
	inner join Warehouse.APC.EncounterProcedureDetail encounterproceduredetail 
	on encounterproceduredetail.ProcedureDetailSourceUniqueID = proceduredetail.SourceUniqueID
	
	inner join Warehouse.APC.Encounter encounter 
	on encounter.EncounterRecno = encounterproceduredetail.EncounterRecno
	
	inner join Warehouse.Theatre.Operation operation 
	on proceduredetail.ProcedureCode = operation.OperationCode
	
	inner join Warehouse.PAS.Consultant consultant
	on encounter.ConsultantCode = consultant.ConsultantCode
	
	inner join Warehouse.WH.Directorate directorate 
	on directorate.DirectorateCode = encounter.EndDirectorateCode
	
	inner join Warehouse.WH.Division division
	on division.DivisionCode = directorate.DivisionCode


where 
(
	/* Hip */
	operation.OperationCode1 like 'W37%'
	or operation.OperationCode1 like 'W38%'
	or operation.OperationCode1 like 'W39%'
	
	/* Knee */
	or operation.OperationCode1 like 'W40%'
	or operation.OperationCode1 like 'W41%'
	or operation.OperationCode1 like 'W42%'
)

and 
	encounter.EpisodeEndDate is null
	and floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionTime) / 365.25) >= 16
	
end	
	
else if @proc = 'CABG'

begin	
	
select
	 PatientName = encounter.PatientSurname + ', ' +  encounter.PatientForename
	,CasenoteNumber = encounter.CasenoteNumber 		
	,ProcedureCode = operation.OperationCode1
	,[Procedure] = operation.operation
	,InSuiteTime = operationdetail.InSuiteTime
	,OperationStartDate = operationdetail.OperationStartDate
	,InAnaestheticTime = operationdetail.InAnaestheticTime
	,OperationEndDate = operationdetail.OperationEndDate
	,InRecoveryTime = operationdetail.InRecoveryTime
	,AdmissionTime = encounter.AdmissionTime
	,EpisodeStartTime = encounter.EpisodeStartTime
	,EpisodeEndTime = encounter.EpisodeEndTime
	,EpisodeLOS = datediff(day, encounter.EpisodeStartDate, isnull(encounter.EpisodeEndDate, getdate()))
	,SpellLOS = datediff(day, encounter.AdmissionTime, isnull(encounter.DischargeTime, getdate()))
	,AdmissionToOperation = datediff(hour, encounter.AdmissionTime, operationdetail.OperationStartDate)
	,Consultant = consultant.Consultant
	,Division = division.Division
	,WardCode =
			(
			select top 1 WardCode
			from Warehouse.APC.WardStay ws 
			where ws.ProviderSpellNo = encounter.ProviderSpellNo
			order by StartTime desc
			)

from 
	Warehouse.Theatre.ProcedureDetail proceduredetail
	
	inner join Warehouse.Theatre.OperationDetail operationdetail 
	on proceduredetail.OperationDetailSourceUniqueID = operationdetail.SourceUniqueID
	
	inner join Warehouse.APC.EncounterProcedureDetail encounterproceduredetail 
	on encounterproceduredetail.ProcedureDetailSourceUniqueID = proceduredetail.SourceUniqueID
	
	inner join Warehouse.APC.Encounter encounter 
	on encounter.EncounterRecno = encounterproceduredetail.EncounterRecno
	
	inner join Warehouse.Theatre.Operation operation 
	on proceduredetail.ProcedureCode = operation.OperationCode
	
	inner join Warehouse.PAS.Consultant consultant
	on encounter.ConsultantCode = consultant.ConsultantCode
	
	inner join Warehouse.WH.Directorate directorate 
	on directorate.DirectorateCode = encounter.EndDirectorateCode
	
	inner join Warehouse.WH.Division division
	on division.DivisionCode = directorate.DivisionCode


where 
(

	operation.OperationCode1 like 'K40%'
	or operation.OperationCode1 like 'K41%'
	or operation.OperationCode1 like 'K42%'
	or operation.OperationCode1 like 'K43%'
	or operation.OperationCode1 like 'K44%'
	or operation.OperationCode1 like 'K44%'
)	

and 
	encounter.EpisodeEndDate is null
	and floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionTime) / 365.25) >= 16
	
end	



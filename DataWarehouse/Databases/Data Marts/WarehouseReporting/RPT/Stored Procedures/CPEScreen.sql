﻿
CREATE proc [RPT].[CPEScreen]
 
@ScreenStatus varchar(50) = null
,@WardCode varchar(10) = null


as

select
	DistrictNo
	,CasenoteNumber
	,NHSNumber
	,PatientName
	,AdmissionTime
	,EpisodeStartTime
	,WardCode
	,ScreenStatus
from
	(
	select
		DistrictNo
		,CasenoteNumber
		,NHSNumber
		,PatientName
		,AdmissionTime
		,EpisodeStartTime
		,WardCode
		,ScreenStatus = 
						case
						when StartWardCode in ('AMU', 'OMU', 'ESTU', '75', '75A')
						then
							case
							when NormalScreenStatus in ('Positive', 'Negative')
							then NormalScreenStatus
							when PCRScreenStatus = 'Screen Required'
							then 'PCR Screen Required'
							else PCRScreenStatus
							end
						else 
							case
							when PCRScreenStatus = 'Positive' 
							or  NormalScreenStatus = 'Positive'
							then 'Positive'
							when PCRScreenStatus = 'Negative' 
							or  NormalScreenStatus = 'Negative'
							then 'Negative'
							when PCRScreenStatus = 'In Progress' 
							or  NormalScreenStatus = 'In Progress'
							then 'In Progress'
							else 'Normal Screen Required'
							end				
						end
	from
		(
		select
			Encounter.DistrictNo
			,Encounter.CasenoteNumber
			,Encounter.NHSNumber
			,PatientName = Encounter.PatientSurname + ', ' + Encounter.PatientForename
			,Encounter.AdmissionTime
			,Encounter.EpisodeStartTime
			,WardCode = Encounter.EndWardTypeCode	
			,PCRScreenStatus = 
							case		
								cast(
										(
										select
											top 1 coalesce(cast(Positive as char(1)), 'I') 
										from
											Infection.Result

										inner join Warehouse.Infection.TestCategory
										on	Result.TestCategoryID = TestCategory.TestCategoryID
										and TestCategory.TestCategory in ('PCR')

										where
											Result.AlertTypeCode = 'CPE'
										and Result.DistrictNo = Encounter.DistrictNo
										and	Result.SpecimenCollectionTime between Encounter.AdmissionDate and coalesce(Encounter.DischargeTime, getdate())
										
										order by
											SpecimenCollectionTime desc
											,AuthorisationDate desc
										)
								as varchar)

							when '1' then 'Positive'
							when '0' then 'Negative'			
							when 'I' then 'In Progress'
							else 'Screen Required'
							end
			,NormalScreenStatus = 
							case		
								cast(
										(
										select
											top 1 coalesce(cast(Positive as char(1)), 'I') 
										from
											Infection.Result

										inner join Warehouse.Infection.TestCategory
										on	Result.TestCategoryID = TestCategory.TestCategoryID
										and TestCategory.TestCategory in ('Screen')

										where
											Result.AlertTypeCode = 'CPE'
										and Result.DistrictNo = Encounter.DistrictNo
										and	Result.SpecimenCollectionTime between Encounter.AdmissionDate and coalesce(Encounter.DischargeTime, getdate())
										
										order by
											SpecimenCollectionTime desc
											,AuthorisationDate desc
										)
								as varchar)

							when '1' then 'Positive'
							when '0' then 'Negative'			
							when 'I' then 'In Progress'
							else 'Screen Required'
							end
			,StartWardCode = AdmissionWard.WardCode
		from
			APCUpdate.Encounter

		inner join PAS.AdmissionMethod
		on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode
		and	AdmissionMethod.NationalAdmissionMethodCode = '21'

		inner join APC.WardStay AdmissionWard
		on	AdmissionWard.ProviderSpellNo = Encounter.ProviderSpellNo
		and not exists
						(
						select
							1
						from
							APC.WardStay AdmissionWardEarlier
						where
							AdmissionWardEarlier.ProviderSpellNo = AdmissionWard.ProviderSpellNo
						and AdmissionWardEarlier.StartTime < AdmissionWard.StartTime
						)

		where
			Encounter.DischargeDate is null
		--and	Encounter.PatientCategoryCode = 'NE'		
		and not exists
					(
					select
						1
					from
						APCUpdate.Encounter EncounterLater
					where
						EncounterLater.ProviderSpellNo = Encounter.ProviderSpellNo
					and	EncounterLater.EpisodeStartTime > Encounter.EpisodeStartTime
					)
		and not exists
					(
					select
						*
					from
						PAS.PatientSpecialRegister
					where
						PatientSpecialRegister.SourcePatientNo = Encounter.SourcePatientNo
					and PatientSpecialRegister.EnteredDate < Encounter.AdmissionDate
					and PatientSpecialRegister.SpecialRegisterCode = 'CPC'
					)
		and	exists
				(
				select
					1
				from
					APC.Encounter Previous
				where
					Previous.SourcePatientNo = Encounter.SourcePatientNo
				and Previous.AdmissionDate >= dateadd(day, -365, Encounter.AdmissionDate) 
				and Previous.AdmissionTime < Encounter.AdmissionTime
				and Previous.PatientCategoryCode not in ('DC', 'RD', 'RN')
				)
		) CPE
	) Result

	where
		(
			Result.WardCode = @WardCode
		or	@WardCode is null
		)
	--and (
	--		Result.ScreenStatus = @ScreenStatus
	--	or	@ScreenStatus is null
		--)
	and (
			Result.ScreenStatus in (select Value from RPT.SSRS_MultiValueParamSplit(@ScreenStatus, ','))
		or	Result.ScreenStatus is null
		)


--and Encounter.DistrictNo = '01400726'
--order by
--	Encounter.DistrictNo
--	,Encounter.AdmissionTime
--	,Encounter.EpisodeStartTime


	






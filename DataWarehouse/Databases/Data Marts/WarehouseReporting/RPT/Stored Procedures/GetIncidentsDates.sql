﻿

create proc 
		[RPT].[GetIncidentsDates]
as

select 
		FinancialYearhKey = left(FinancialMonthKey,4)
		,FinancialYear 
		,FirstDate = min(TheDate)
		,LastDate =max(TheDate)
from	
		WarehouseReporting.WH.Calendar
where	
		left(FinancialMonthKey,4) between year(getdate()-1000) and year(getdate())
group by
		FinancialYear
		,left(FinancialMonthKey,4)




﻿


CREATE proc [RPT].[GetIncomeAPCExclusions]

@period varchar(50) --= 'Month 2 2012/2013'
,@position varchar(50) --= 'Test'
,@version int --= 1
,@susidentifier varchar(50) = null-- = 'BMCHT 201104011530*3178286*2'

as

select
	Period
	,Position
	,Version
	,SUSIdentifier = 
				left(
					'BMCHT ' +
					replace(
						 Encounter.SourceUniqueID
						,'||'
						,'*'
					)
					,50
				)
	,ExclusionCode

from
	Income.APCSnapshot encounter
	
left outer join Warehouse.PAS.Specialty specialty
on specialty.SpecialtyCode = encounter.SpecialtyCode

/* Exclusions */
	
inner join
	
(
	select 
		*
	from	
		Income.APCExclusionRuleBase

) exclusionrulebase
on
	exclusionrulebase.ContextCode = 'CEN||PAS'
	and (encounter.SiteCode = exclusionrulebase.SiteCode or exclusionrulebase.SiteCode is null)
	and (encounter.PCTCode = exclusionrulebase.PCTCode or exclusionrulebase.PCTCode is null)
	and (exclusionrulebase.PODCode is null)
	and (exclusionrulebase.HRGCode is null)
	and (encounter.ManagementIntentionCode = exclusionrulebase.ManagementIntentionCode or exclusionrulebase.ManagementIntentionCode is null)
	and (encounter.AdmissionMethodCode = exclusionrulebase.PASAdmissionMethodCode or exclusionrulebase.PASAdmissionMethodCode is null)
	and (encounter.SpecialtyCode = exclusionrulebase.PASSpecialtyCode or exclusionrulebase.PASSpecialtyCode is null)
	and (specialty.NationalSpecialtyCode = exclusionrulebase.NationalSpecialtyCode or exclusionrulebase.NationalSpecialtyCode is null)
	and (coalesce(encounter.NeonatalLevelOfCare, '0') = exclusionrulebase.NeonatalLevelOfCareCode or exclusionrulebase.NeonatalLevelOfCareCode is null)
	and (encounter.PrimaryDiagnosisCode = exclusionrulebase.PrimaryDiagnosisCode or exclusionrulebase.PrimaryDiagnosisCode is null)
	and (encounter.PrimaryOperationCode = exclusionrulebase.PrimaryProcedureCode or exclusionrulebase.PrimaryProcedureCode is null)								
	and (encounter.ContractSerialNo = exclusionrulebase.CommissioningSerialNo or exclusionrulebase.CommissioningSerialNo is null)
	and (encounter.LocalAdminCategoryCode = exclusionrulebase.AdminCategoryCode or exclusionrulebase.AdminCategoryCode is null)			
	and (encounter.FirstEpisodeInSpellIndicator = exclusionrulebase.FirstEpisodeInSpellIndicator or exclusionrulebase.FirstEpisodeInSpellIndicator is null)
	and (datediff(day, encounter.EpisodeStartDate, encounter.EpisodeEndDate) = exclusionrulebase.LoS or exclusionrulebase.LoS is null)				
	and (encounter.DischargeDate between exclusionrulebase.EffectiveFromDate and coalesce(exclusionrulebase.EffectiveToDate, getdate()))
	
where
	(
	left(
		'BMCHT ' +
		replace(
				Encounter.SourceUniqueID
			,'||'
			,'*'
		)
		,50
	) = @susidentifier 
	or @susidentifier is null
	)
and 
	Period = @period
and
	Position = @position
and
	Version = @version




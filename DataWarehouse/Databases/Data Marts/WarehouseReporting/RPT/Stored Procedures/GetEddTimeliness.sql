﻿-- =============================================
-- Author:		Gareth Summerfield
-- Create date: 03/08/2011
-- =============================================
CREATE PROCEDURE [RPT].[GetEddTimeliness]
	 @startdate AS Date
	,@enddate AS Date
	,@specialtycode varchar(max)
	,@Division varchar(max)
	,@patientcategory varchar(max)
AS

BEGIN
	SET NOCOUNT ON;

SELECT 
	 Ward = Ward.Ward
	,Consultant = Consultant.Consultant

	,EnteredWithin48hrs =
		CASE 
		WHEN DATEDIFF(MINUTE , ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) < 1440 THEN 1
		ELSE 0
		END

	,Admissions = 1
	,Division = ISNULL(Directorate.Division , 'Not Known')

	,HourTimeBand =
		CASE 
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) < 1440 THEN '<24'
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) BETWEEN 1440 AND 2880 THEN '24-48'
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) BETWEEN 2880 AND 4320 THEN '48-72'
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) BETWEEN 4320 AND 5760 THEN '72-96'
		WHEN ExpectedLOS.ExpectedLOS IS NULL THEN 'Not Recorded'
		ELSE '96+'
		END
	
	,Specialty = Specialty.NationalSpecialty
	,PathwayCondition = ISNULL(ExpectedLOS.RTTPathwayCondition , 'Unknown')
FROM         
	Warehouse.APC.ExpectedLOS  ExpectedLOS

INNER JOIN PAS.Directorate Directorate
ON	ExpectedLOS.DirectorateCode = Directorate.DirectorateCode
AND Directorate.Division IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))

INNER JOIN PAS.Specialty Specialty
ON ExpectedLOS.Specialty =  Specialty.SpecialtyCode
AND Specialty.NationalSpecialtyCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@specialtycode,','))

INNER JOIN Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = ExpectedLOS.AdmissionMethodCode

INNER JOIN Warehouse.PAS.ManagementIntention ManagementIntention
on	ManagementIntention.ManagementIntentionCode = ExpectedLOS.ManagementIntentionCode

LEFT OUTER JOIN PAS.Consultant
ON ExpectedLOS.Consultant = Consultant.ConsultantCode

LEFT OUTER JOIN PAS.Ward Ward
ON ExpectedLOS.Ward = Ward.WardCode

WHERE     
	CONVERT(date, ExpectedLOS.AdmissionTime, 103) >= @startdate --'14/Aug/2011'
AND CONVERT(date, ExpectedLOS.AdmissionTime, 103) <= @enddate --'14/Aug/2011'
--AND ExpectedLOS.Ward NOT IN ('47', '64', '64A', '64B', '67', '67A', 'HM')
AND ExpectedLOS.ManagementIntentionCode NOT IN ('R','N') 
AND NOT
	(
		ExpectedLOS.Ward = 'ESTU'
	AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6
	)
AND ArchiveFlag <> 'D'
AND Specialty.NationalSpecialtyCode <> '501'
AND ExpectedLOS.Ward <> '76A'
AND Specialty.Specialty NOT like '%DIALYSIS%'
AND Specialty.Specialtycode NOT like 'IH%'
AND ExpectedLOS.Ward NOT like 'SUB%'
AND case
	--Elective Inpatient
	when AdmissionMethod.InternalCode in 
		(
		 '1' --Waiting List
		,'2' --Booked
		,'3' --Planned
		)
	and ManagementIntention.InternalCode in 
		(
		 '1' --INPATIENT
		,'2'
		,'3' --INTERVAL ADMISSION
		,'4'
		,'5'
		,'6' --BORN IN HOSP/ON WAY
		)
	then 'EL' --Elective
	--Non Elective
	else 'NE' --Non Elective
	end  IN (SELECT VALUE FROM dbo.SSRS_MultiValueParamSplit(@patientcategory,','))
END



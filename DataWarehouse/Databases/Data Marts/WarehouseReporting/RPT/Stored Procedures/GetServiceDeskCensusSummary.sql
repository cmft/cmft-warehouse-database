﻿
CREATE PROCEDURE [RPT].[GetServiceDeskCensusSummary]
(
 @StartDate Date
,@EndDate Date
)

AS

SELECT
	 CensusDate
	,FinancialYear
	,TheMonth
	,FinancialMonthKey
	,MonthEnd = CASE 
		WHEN 
				CensusDate = CONVERT(date,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,CensusDate)+1,0)),113) 
			OR
				CensusDate = (SELECT MAX(CensusDate) FROM ServiceDesk.Census)
		THEN 1
		ELSE 0
	 END
	,CountSourceUniqueID
FROM
(
SELECT
	 CensusDate
	,Calendar.FinancialYear
	,Calendar.TheMonth
	,Calendar.FinancialMonthKey
	,Count(SourceUniqueID) CountSourceUniqueID
FROM
	ServiceDesk.Census
INNER JOIN WH.Calendar Calendar
	ON Census.CensusDate = Calendar.TheDate

WHERE
	 CensusDate >= @StartDate 
 AND CensusDate <= @EndDate


GROUP BY 
	 CensusDate
	,Calendar.FinancialYear
	,Calendar.TheMonth
	,Calendar.FinancialMonthKey
)Census;

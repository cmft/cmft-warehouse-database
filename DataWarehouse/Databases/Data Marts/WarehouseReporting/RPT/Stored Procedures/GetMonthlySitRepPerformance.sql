﻿
create proc RPT.GetMonthlySitRepPerformance
(
	@start date
	,@end date
)

as

select
	id
     , [Site]
      ,[EffectiveDate]
      --,[AEType1Attendances]
      --,[Type1Breaches4hour]
      --,[Type1DTAtoAdmission4to12hours]
      --,[Type1EmergencyAdmissionsViaAE]
      --,[DirectEmergencyAdmissions]
      --,[CancellationsElectiveOps]
      ,[CancellationsUrgentElectiveOps]
      ,[CancellationsUrgentOpsfor2ndPlusTime]
      --,[ElectiveOrdinaryAdmissions]
      --,[DischargesOfEmergencyAdmissions]
      ,[AcuteBedsOpenAtMidnight]
      ,[AcuteBedsOccupiedAtMidnight]
      ,[MedicalBedsAtMidnight]
      ,[MedicalOutliersAtMidnight]
      --,[AdultCriticalCareOpenAtMidnight]
      --,[AdultCriticalCareOccupiedAtMidnight]
      --,[DelayedTransfersOfCare]
      --,[WardClosures]
      ,[NonMedicalCCTransfers]
      ,[NonMedicalCCTransfers_OO_ApprovedCCXferGroup]
      ,[Pressures]
      ,[IssuesWithStaff]
      ,[WardClosureComments]
      ,[CriticalCareIssues]
      ,[OtherComments]
      --,[ts]
      --,[isActive]
      ,InfoPathID
from
	[Information].Collections.SitRepDailyDataCollectionActive
where
	[EffectiveDate] between @start and @end

﻿


CREATE procedure [RPT].[CasenoteNewOutpatient]
(
	 @FromDate date
	,@ToDate date
	,@ClinicCode varchar(20)
)
as

----debug
--declare
--	 @FromDate date = '1 Mar 2013'
--	,@ToDate date = '31 Mar 2013'

select
	 Encounter.Surname
	,Encounter.Forenames
	,Encounter.DateOfBirth
	,Encounter.DistrictNo
	,Encounter.EncounterTypeCode
	,Encounter.BookingDate
	,Encounter.BookingTime
	,Encounter.SourcePatientNo
	,Encounter.SourceEntityRecno
	,Encounter.EncounterDate
	,Encounter.EncounterTime
	,Encounter.BookingCasenoteNo
	,Encounter.ClinicCode
	,Clinic.Clinic
	,Encounter.CasenoteNo
	,Encounter.AllocatedDate
	,Encounter.CasenoteLocationCode
	,CasenoteLocation.CasenoteLocation
	,Encounter.CasenoteLocationDate
	,Encounter.CasenoteStatus
	,Encounter.WithdrawnDate
	,Encounter.Comment
	,Encounter.CurrentBorrowerCode
	,CurrentBorrower = CasenoteBorrower.Borrower
from
	WarehouseOLAP.dbo.BasePASCasenoteBooking Encounter

left join Warehouse.PAS.CasenoteLocation
on	CasenoteLocation.CasenoteLocationCode = Encounter.CasenoteLocationCode

left join Warehouse.PAS.CasenoteBorrower
on	CasenoteBorrower.BorrowerCode = Encounter.CurrentBorrowerCode


left join Warehouse.PAS.Clinic
on	Clinic.ClinicCode = Encounter.ClinicCode

where
	EncounterDate between @FromDate and @ToDate
and	(
		Encounter.ClinicCode = @ClinicCode
	or	@ClinicCode is null
	)
and	Encounter.EncounterTypeCode = 'NEW'

and	Encounter.OtherNonMNumberOPWaits = 0
and	Encounter.OtherNonMNumberAPCWaits = 0
and	Encounter.IsCurrentInpatient = 0

order by
	 Encounter.EncounterTime
	,Encounter.Surname
	,Encounter.Forenames


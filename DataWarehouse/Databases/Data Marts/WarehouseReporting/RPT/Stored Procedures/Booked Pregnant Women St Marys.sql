﻿







CREATE PROCEDURE [RPT].[Booked Pregnant Women St Marys] 

  @FromDate    Date
 ,@ToDate      Date

AS  

--DECLARE @FromDate AS DATE        = '01-DEC-2013'
--DECLARE @ToDate   AS DATE        = '31-DEC-2013'

SELECT 
  PREGNANCY.[M_NUMBER]                         AS CasenoteNo
 ,PREGNANCY.[M_SURNAME]                        AS MaternalSurname
 ,MOTHER_REG.[F_FORENAMES]                     AS MaternalForename
 ,CONVERT(varchar,MOTHER_REG.[M_DOB],103)      AS MaternalDoB
 ,PREGNANCY.[OCCURRENCE]
 ,PREGNANCY.[STATUS]
 ,PREGNANCY.[TYPE]
 ,UNBOOKED.[DESCRIPTION]                       AS UnbookedDescription
 ,MaritalStatus =
   CASE  PREGNANCY.[MARITAL]
     WHEN 'M' THEN 'Married/Civil Partner'
     WHEN 'W' THEN 'Widowed/Surviving Civil Partner'
     WHEN 'D' THEN 'Divorced/Civil Partnership dissolved'
     WHEN 'S' THEN 'Separated'
     WHEN 'V' THEN 'Other/UK'
     WHEN 'N' THEN 'Single'
     WHEN 'U' THEN 'Unknown'
     WHEN 'X' THEN 'Not disclosed'
     ELSE 'Not Specified'
   END  
 
 ,ISNULL(PREGNANCY.[M_OCCUPATION],'')           AS MaternalOccupation
 ,ISNULL(PREGNANCY.[M_TELEPHONE_WORK],'')       AS TelephoneWork
 ,ReferredBy = 
   CASE PREGNANCY.[REFERRED_BY]
     WHEN 'M' THEN 'Midwife'
     WHEN 'S' THEN 'GP'
     WHEN 'G' THEN 'Self'
     WHEN 'O' THEN 'Other'
     ELSE 'Not Specified'
   END 
 ,MIDWIFE_TEAM.[TEAM]                           AS MidwifeTeam  
 ,ISNULL(CONVERT(varchar,PREGNANCY.[REFERRAL_GEST_WKS]),'')      AS ReferralGestationWeeks
 ,ISNULL(CONVERT(varchar,PREGNANCY.[REFERRAL_GEST_DAYS]),'')     AS ReferralGestationDays
 ,PREGNANCY.[GP_SURNAME]
 ,PREGNANCY.[CON_CODE]
 ,CONSULTANT.[C_INITIALS] + ' - ' + CONSULTANT.[C_SURNAME]       AS Consultant
 ,HospReferredTo = 
   CASE PREGNANCY.[HOSP_REFERRED_TO]
     WHEN 'S' THEN 'Salford Birthing Centre'
     WHEN 'M' THEN 'St Marys Hospital Delivery Unit'
     WHEN 'L' THEN 'St Marys Hospital MLU'
     ELSE 'Other'
   END
 ,ISNULL(CONVERT(varchar,PREGNANCY.[REFERRAL_DATE],103),'')         AS ReferralDate
 ,ISNULL(CONVERT(varchar,PREGNANCY.[FIRST_CONTACT_DATE],103),'')    AS FirstContactDate
 ,ISNULL(CONVERT(varchar,PREGNANCY.[LMP_AT_REFERRAL],103),'')       AS LMPatReferral
 ,ISNULL(CONVERT(varchar,PREGNANCY.[EDD_AT_REFERRAL],103),'')       AS EDDatReferral
 ,ISNULL(CONVERT(varchar,PREGNANCY.[CREATE_DATE],103),'')           AS CreateDate
 ,CONVERT(varchar,PREGNANCY.[DATE_STAMP],103)                       AS DateStamp
 ,ISNULL(INCOMPLETE_RECS.[REASON],'')                               AS IncompleteReason
 ,[REASON_COMPLETION]
 ,PREGNANCY.[REFERRAL_DATE]                                         AS ReferralDateSort

FROM warehousesql.[CMISStaging].[SMMIS].[PREGNANCY]
 
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[MOTHER_REG]
  ON PREGNANCY.[M_NUMBER] = MOTHER_REG.[M_NUMBER]
 AND PREGNANCY.[OCCURRENCE] = MOTHER_REG.[CURRENT_OCCURRENCE]
  
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[BIRTH_REG]      AS BirthReg
  ON PREGNANCY.[M_NUMBER] = BirthReg.[M_NUMBER]
 AND PREGNANCY.[OCCURRENCE] = BirthReg.[OCCURRENCE]

LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[INCOMPLETE_RECS]
  ON PREGNANCY.[M_NUMBER] = INCOMPLETE_RECS.[M_NUMBER]
 AND PREGNANCY.[OCCURRENCE] = INCOMPLETE_RECS.[OCCURRENCE]
   
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[MIDWIFE_TEAM]
  ON PREGNANCY.[MIDWIFE_TEAM] = MIDWIFE_TEAM.[TEAM_INITIAL]
   
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[CONSULTANT] 
  ON PREGNANCY.[CON_CODE] = CONSULTANT.[CON_CODE]
  
LEFT OUTER JOIN warehousesql.[CMISStaging].[SMMIS].[LIST_OPTIONS]   AS UNBOOKED
  ON ASCII(PREGNANCY.[UNBOOKED]) = ASCII(UNBOOKED.[KEY_CHAR])  
 AND UNBOOKED.[COLUMN_NAME] = 'UNBOOKED' 
  
  
WHERE BirthReg.[I_NUMBER] IS NULL
   AND PREGNANCY.[REASON_COMPLETION] IS NULL
   AND PREGNANCY.[REFERRAL_DATE] BETWEEN @FromDate AND @ToDate 
   



﻿

CREATE proc 
		[RPT].[GetIncidentDivision]

as

select
		DivisionCode
		,DivisionDescription
		,MixedCaseCode
from	
		Warehouse.Incident.Division Division

where	
		MixedCaseCode collate latin1_general_cs_as not in ('A','D') --Old Divisions: Division of Medicine, Childrens Services                                

union

select
		DivisionCode = null
		,DivisionDescription = 'Trust'
		,MixedCaseCode = null
		
order by
		DivisionDescription

﻿



CREATE proc [RPT].[GetACHDTrackingDischarged]

@DateRange varchar(20)

as

--this query shows you all patients that were diagnosed with a Congenital Heart Defect (FROM beginning of time!)
--we then grab the records that were discharged in the past 12 months
--finally we only show the last spell in the report

--DECLARE Variables
DECLARE @DischargeEndDate smalldatetime

--SET Variables
SET @DischargeEndDate = CASE @DateRange
	WHEN 'Today' THEN  convert(DATE,getdate())
	WHEN 'Previous 3 days' THEN DATEADD(DAY, -3, convert(DATE,getdate()))
	WHEN 'Previous 7 days' THEN DATEADD(DAY, -7, convert(DATE,getdate()))
	WHEN 'Previous 28 days' THEN DATEADD(DAY, -28, convert(DATE,getdate()))
	WHEN 'Previous 12 mONths' THEN DATEADD(MONTH, -12, convert(DATE,getdate()))
END

--3. now grab all other relevant details
SELECT 
	NHSNumber
	,DistrictNo
	,InternalNo = Encounter.SourcePatientNo
	,SpellNo = Encounter.SourceSpellNo
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,ConsultantCode
	,SpecialtyCode
	,AdmissionDate
	,DateOfDeath

FROM APC.Encounter Encounter

INNER JOIN  (
				--3. now only bring back last encounter (episode)
				SELECT
				Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
				,SourceEncounterNo = max(Encounter.SourceEncounterNo)

				FROM apc.Encounter Encounter

				INNER JOIN (
								--2. now only bring back the last spell
								SELECT 
									SourcePatientNo
									,SourceSpellNo = max(SourceSpellNo)
		
									FROM (
											SELECT
												myqry.SourcePatientNo
												,myqry.SourceSpellNo
												,DischargeDate
				
												FROM
													(
													--1. query PrimaryDiagnosis (check all records for a Congenital Heart Defect)
													SELECT 
														Encounter.SourcePatientNo
														,Encounter.SourceSpellNo
														,Encounter.DischargeDate

													FROM 
														WarehouseReporting.APC.Encounter Encounter								 
					
													WHERE 
														Encounter.PrimaryDiagnosisCode in ('Q20.0','Q20.1','Q20.2','Q20.3','Q20.4','Q20.5','Q20.6','Q20.8','Q20.9','Q21.0','Q21.1','Q21.2',
																	'Q21.3','Q21.4','Q21.8','Q21.9','Q22.0','Q22.1','Q22.2','Q22.3','Q22.4','Q22.5','Q22.6','Q22.8',
																	'Q22.9','Q23.0','Q23.1','Q23.2','Q23.3','Q23.4','Q23.8','Q23.9','Q24.0','Q24.1','Q24.2','Q24.3',
																	'Q24.4','Q24.5','Q24.6','Q24.8','Q24.9','Q25.0','Q25.1','Q25.2','Q25.3','Q25.4','Q25.5','Q25.6',
																	'Q25.7','Q25.8','Q25.9','Q26.0','Q26.1','Q26.2','Q26.3','Q26.4','Q26.5','Q26.6','Q26.8','Q26.9',
																	'Q79.6','Q87.4','Q89.3','Q90.0','Q90.1','Q90.2','Q90.9','Q91.0','Q91.3','Q96.9')
								
													UNION

													--1. query all other Diagnosis  (check all records for a Congenital Heart Defect)
													SELECT 
														Encounter.SourcePatientNo
														,Encounter.SourceSpellNo
														,Encounter.DischargeDate

													FROM 
														WarehouseReporting.APC.Encounter Encounter 
					
													INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
													ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
													AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
													AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
													AND Diagnosis.DiagnosisCode in ('Q20.0','Q20.1','Q20.2','Q20.3','Q20.4','Q20.5','Q20.6','Q20.8','Q20.9','Q21.0','Q21.1','Q21.2',
																	'Q21.3','Q21.4','Q21.8','Q21.9','Q22.0','Q22.1','Q22.2','Q22.3','Q22.4','Q22.5','Q22.6','Q22.8',
																	'Q22.9','Q23.0','Q23.1','Q23.2','Q23.3','Q23.4','Q23.8','Q23.9','Q24.0','Q24.1','Q24.2','Q24.3',
																	'Q24.4','Q24.5','Q24.6','Q24.8','Q24.9','Q25.0','Q25.1','Q25.2','Q25.3','Q25.4','Q25.5','Q25.6',
																	'Q25.7','Q25.8','Q25.9','Q26.0','Q26.1','Q26.2','Q26.3','Q26.4','Q26.5','Q26.6','Q26.8','Q26.9',
																	'Q79.6','Q87.4','Q89.3','Q90.0','Q90.1','Q90.2','Q90.9','Q91.0','Q91.3','Q96.9')
													)myqry
												WHERE
													--now only bring back records that have been discharged in past 12 months
													DischargeDate>@DischargeEndDate	
										)mainqry
										--WHERE SourcePatientNo = '3278204'
									GROUP BY
										SourcePatientNo
						)MaxSpell

				ON Encounter.SourcePatientNo = MaxSpell.SourcePatientNo
				AND Encounter.SourceSpellNo = MaxSpell.SourceSpellNo

				GROUP BY
				Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
			)q
ON Encounter.SourcePatientNo = q.SourcePatientNo
AND Encounter.SourceSpellNo = q.SourceSpellNo 
AND Encounter.SourceEncounterNo = q.SourceEncounterNo 
ORDER BY 
	nhsnumber
﻿










CREATE PROCEDURE [RPT].[SCRMDTStaffAttendance] 

	 @FromDate        Date
	,@ToDate          Date
	,@MeetingType     varchar(20)
	,@RoleDescription varchar(50)

as


/************************************************
**
**  MDT Meeting Staff Attendance by
**	Role, Meeting Date and Meeting Type
**
************************************************/


----debug
--DECLARE @FromDate        AS DATE        = '01-JUL-2013'
--DECLARE @ToDate          AS DATE        = '24-JUL-2013'
--DECLARE @MeetingType      AS varchar(20) = 'Upper GI'
--DECLARE @RoleDescription AS varchar(50) = 'All'

select
	 MeetingType
	,MeetingSubType
	,MeetingDate
	,MDTRole
	,Cases = sum(Cases)
from
	(
	select
		 MeetingType =[MEETING_TYPE_DESC]
		,MeetingSubType = [SUB_DESC]
		--,MeetingSubType = replace([SUB_DESC] , '-' , 'N/A')
		,MeetingDate = cast([MDT_DATE] as date)
		,MDTRole = [SPECIALTY]
		,Cases = [PRESENT]
	from
		warehousesql.[CancerRegister].[dbo].[vwMDT_ATTENDED]
		
	where
		MDT_DATE between @FromDate and @ToDate
	and	[PRESENT] = 1
	and	[MEETING_TYPE_DESC] like
			(
			case
			when @MeetingType = 'All' then '%' 
			else @MeetingType 
			end
			)
			
	and [SPECIALTY] like
			(
			case
			when @RoleDescription = 'All' then '%' 
			else @RoleDescription 
			end
			)
	) Meeting
  
group by
	 MeetingType
	,MeetingSubType
	,MeetingDate
	,MDTRole

order by
	 MeetingType
	,MeetingSubType
	,MeetingDate
	,MDTRole






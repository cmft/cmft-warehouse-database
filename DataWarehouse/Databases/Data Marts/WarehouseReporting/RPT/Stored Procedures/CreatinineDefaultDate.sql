﻿CREATE procedure [RPT].[CreatinineDefaultDate]
as

select
	 DefaultFromDate = dateadd(day , -7 , max(LatestAdmissionDate))
	,DefaultToDate = max(LatestAdmissionDate)
from
	WarehouseReporting.OCM.CodedResult Result
where
	Result.CurrentInpatient = 1
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE RPT.InpatientCensus 
	@CensusDate Date = NULL
AS

BEGIN
SET NOCOUNT ON;

DECLARE @RptCensusDate Datetime = Convert(Date, COALESCE(@CensusDate
														,(SELECT MAX(CensusDate) FROM APC.InpatientCensus)
														)
														,113)

SELECT 
	 SourceUniqueID = InpatientCensus.SourceUniqueID
	,SourcePatientNo = InpatientCensus.SourcePatientNo
	,SourceSpellNo = InpatientCensus.SourceSpellNo
	,ProviderSpellNo = InpatientCensus.ProviderSpellNo
	,NHSNumber = Encounter.NHSNumber
	,CasenoteNumber = Encounter.CasenoteNumber
	,SiteCode = InpatientCensus.SiteCode
	,[Site] = [Site].[Site]
	,SiteColour = [Site].Colour
	,WardCode = ISNULL(InpatientCensus.WardCode, 'Not Recorded')
	,Ward = ISNULL(Ward.Ward, 'Not Recorded')
	,ConsultantCode = ISNULL(InpatientCensus.ConsultantCode, 'Not Recorded')
	,Consultant = ISNULL(Consultant.Consultant, 'Not Recorded')
	,SpecialtyCode = InpatientCensus.SpecialtyCode
	,Specialty = Specialty.NationalSpecialtyCode + ' - ' + Specialty.Specialty
	,InpatientCensus.SourceAdminCategoryCode
	,InpatientCensus.AdmissionTime
	,CensusDate = @RptCensusDate
	,LastAdmissionDate = MaxInpatientCensus.LastAdmissionDate
FROM 
	APC.InpatientCensus InpatientCensus
INNER JOIN APC.Encounter Encounter
	ON Encounter.SourceUniqueID = InpatientCensus.SourceUniqueID
LEFT OUTER JOIN PAS.Specialty Specialty
	ON InpatientCensus.SpecialtyCode = Specialty.SpecialtyCode
LEFT OUTER JOIN PAS.ward Ward
	ON InpatientCensus.WardCode = Ward.WardCode
LEFT OUTER JOIN PAS.Consultant Consultant
	ON InpatientCensus.ConsultantCode = Consultant.ConsultantCode
LEFT OUTER JOIN PAS.[Site] [Site]
	ON InpatientCensus.SiteCode = [Site].SiteCode
LEFT OUTER JOIN (SELECT MAX(AdmissionDate)LastAdmissionDate, CensusDate FROM APC.InpatientCensus GROUP BY CensusDate) MaxInpatientCensus
	ON Convert(Date,MaxInpatientCensus.CensusDate,113) = @RptCensusDate
	

WHERE
	Convert(Date,InpatientCensus.CensusDate,113) = @RptCensusDate
END

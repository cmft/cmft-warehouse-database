﻿






CREATE proc [RPT].[CHAMPParticipationDrillthrough]
(
 @AssessmentDateStart datetime2(0)
,@AssessmentDateEnd datetime2(0)
,@SchoolCode varchar(20) = null
,@YearGroup varchar(20) = null
,@CentileClassification varchar(50) = null
,@IsMeasured bit = null
,@IsAccountRegistered bit = null
)

as

/*===============================================================================
When		Who			What
30/01/2015	Paul Egan	Initial Coding for SSRS report
20/02/2015	Paul Egan	Changed SchoolYear to YearGroup
03/03/2015	Paul Egan	Added IsAccountRegistered and ClassificationOrder
===============================================================================*/


/*============== Debug only ========================*/
--declare
-- @AssessmentDateStart datetime2(0) = '20140901'
--,@AssessmentDateEnd datetime2(0) = '20151231' --cast(getdate() - 1 as date);
--,@SchoolCode varchar(20) = null
--,@YearGroup varchar(20) = null
--,@CentileClassification varchar(50) = null
--,@IsMeasured bit = null
--,@IsAccountRegistered bit = null
--;
/*==================================================*/

select
	CHAMPEncounter.SchoolCode
	,School.School
	,CHAMPEncounter.YearGroup
	,CHAMPEncounter.MckessonID
	,CHAMPEncounter.NHSNumber
	,CHAMPEncounter.SexCode
	,CHAMPEncounter.AssessmentTime
	,CHAMPEncounter.OptedOut
	,CHAMPEncounter.IsMeasured
	,CHAMPEncounter.IsMailed
	,CHAMPEncounter.IsAccountRegistered
	,CHAMPEncounter.Height
	,CHAMPEncounter.Weight
	,CHAMPEncounter.BMI
	,CHAMPEncounter.Classification
	,CHAMPEncounter.ClassificationOrder
from
	Warehouse.COM.CHAMPEncounter

	inner join Warehouse.ChildHealth.School
	on School.SchoolCode = CHAMPEncounter.SchoolCode
where
	cast(CHAMPEncounter.AssessmentTime as date) between @AssessmentDateStart and @AssessmentDateEnd
	and CHAMPEncounter.SchoolCode = isnull(@SchoolCode, CHAMPEncounter.SchoolCode)
	and CHAMPEncounter.YearGroup = isnull(@YearGroup, CHAMPEncounter.YearGroup)
	and CHAMPEncounter.Classification = isnull(@CentileClassification, CHAMPEncounter.Classification)
	and CHAMPEncounter.IsMeasured = isnull(@IsMeasured, CHAMPEncounter.IsMeasured)
	and CHAMPEncounter.IsAccountRegistered = isnull(@IsAccountRegistered, CHAMPEncounter.IsAccountRegistered)
;






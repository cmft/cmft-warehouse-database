﻿
CREATE proc [RPT].[GetLoSPatients]
(
	@unit varchar(50) = null
	,@losgroup varchar(50) = null
	,@date datetime = null
	,@parent varchar(50) = null
	,@user varchar(50) = null
)

as

set @unit = coalesce (@unit, '(Select All)')

set @losgroup = coalesce (@losgroup, '(Select All)')

set @parent = coalesce (@parent, '(Select All)')

--set @user = @user

--execute as user = @user

set @user = suser_name()

select 
	[SnapshotDate]
	,[LoSGroup]
	,[Division]
	,[AdmissionMethodType]
	,[WardCode]
	,[Ward]
	,[LoS]
	,[SpecialtyCode]
	,[Specialty]
	,[AdmissionDate]
	,[WardStartDate]
	--,[CaseNoteNo] = 
	--				case
	--					when is_rolemember(sm.[Role]) = 1
	--					or is_rolemember('PID - Information', @user) = 1
	--					or is_rolemember('db_owner', @user) = 1
	--					then [CaseNoteNo]
	--					else 'Unauthorised'
	--				end
	,[CaseNoteNo] = [CaseNoteNo]
					--case
					--	when exists 
					--				(
					--				select 1
					--				from RPT.SecurityMap sm
					--				where [DomainLogin] = @user
					--				and sm.DivisionCode = pld.DivisionCode
					--				)
					--	then [CaseNoteNo]
					--	else 'Unauthorised'
					--end
     -- ,PatientName = case
					--	when is_member(sm.[Role]) = 1
					--	or is_rolemember('PID - Information', @user) = 1
					--	or is_rolemember('db_owner', @user) = 1
					--	then ([PatientFirstName] + ' ' + [PatientSurname]) 
					--	else 'Unauthorised'
					--end 
					
	,[PatientName] = [PatientFirstName] + ' ' + [PatientSurname]
					--case
					--	when exists 
					--				(
					--				select 1
					--				from RPT.SecurityMap sm
					--				where [DomainLogin] = @user
					--				and sm.DivisionCode = pld.DivisionCode
					--				)
					--	then ([PatientFirstName] + ' ' + [PatientSurname]) 
					--	else 'Unauthorised'
					--end
      ,[PatientAge]
      ,[Consultant]
      ,[IntendedManagement]
      ,[ExpectedLoS]
from 
	LOS.PatientLevelData pld
	
	--left outer join (
	--				select distinct Name
	--								,[Role]
	--				from ETL.SecurityMap
	--				) sm
	--on @user = sm.Name
	
	
where 
		([WardCode] = @unit or @unit = '(Select All)')
	and ([Division] = @parent or @parent = '(Select All)')
	and ([LOSGroup] = @losgroup or @losgroup = '(Select All)')
	and ([SnapshotDate] = @Date Or @Date is null)
	

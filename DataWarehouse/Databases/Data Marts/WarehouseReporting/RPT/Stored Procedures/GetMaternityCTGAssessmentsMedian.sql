﻿

CREATE Procedure [RPT].[GetMaternityCTGAssessmentsMedian] --'20130401', '20140331'

(@StartDate date
,@EndDate date
)

As



declare
@LocalStartDate   date
,@LocalEndDate    date

set @LocalStartDate = @StartDate
set @LocalEndDate = @EndDate



IF OBJECT_ID(N'tempdb..#CTGMedian') IS NOT NULL
BEGIN
     DROP TABLE #CTGMedian
END


select 
      SessionNumber
      ,FetusNumber
      ,CTGStartDate
      ,TimeBetweenAssessments
      ,[MonitoringType]
      ,Sequence = row_number ( ) over (partition by [MonitoringType] order by TimeBetweenAssessments,SessionNumber asc)
into 
      #CTGMedian
from
      (Select
            SessionNumber
            ,FetusNumber
            ,CTGStartDate
            ,TimeBetweenAssessments
            ,[MonitoringType] = '60mins'
      from
            Warehouse.Maternity.CTGAssessment
      where
            CheckWithinAnHour in ('Yes','No')
      and FetusNumber in ('0','1')
      and CTGStartDate between @LocalStartDate and @LocalEndDate

      union
      
      Select
            SessionNumber
            ,FetusNumber
            ,CTGStartDate
            ,TimeBetweenAssessments
            ,[MonitoringType] = '30mins'
      from 
            Warehouse.Maternity.CTGAssessment
      where
            CheckWithin30Mins in ('Yes','No')
      and FetusNumber in ('0','1')
      and CTGStartDate between @LocalStartDate and @LocalEndDate
      ) A
;




With CTGAssTimeCTE 
      (MonitoringType
      ,MedianValue
      ,LowerQValue
      ,UpperQValue
      )
As
      (select 
            MonitoringType
            ,MedianValue = cast(round(((max(Sequence)+0.5)/2),0) as int)
            ,LowerQValue = cast(round(((max(Sequence)+0.5)/4),0) as int)
            ,UpperQValue = cast(round((((max(Sequence)+0.5)/4)*3),0) as int)
      from 
            #CTGMedian  
      group by
            MonitoringType    
      )
            

Select
      MonitoringType = AllCTGs.MonitoringType
      ,MedianTime = Med.TimeBetweenAssessments
      ,LowerTime = Low.TimeBetweenAssessments
      ,UpperTime = Upp.TimeBetweenAssessments
      ,AverageTime = Avg(AllCTGs.TimeBetweenAssessments)
from 
      #CTGMedian AllCTGs

inner join #CTGMedian Med
on AllCTGs.MonitoringType = Med.MonitoringType

inner join CTGAssTimeCTE Value
on Med.MonitoringType = Value.MonitoringType
and Med.Sequence = Value.MedianValue

inner join #CTGMedian Low
on AllCTGs.MonitoringType = Low.MonitoringType
and Low.MonitoringType = Value.MonitoringType
and Low.Sequence = Value.LowerQValue

inner join #CTGMedian Upp
on AllCTGs.MonitoringType = Upp.MonitoringType
and Upp.MonitoringType = Value.MonitoringType
and Upp.Sequence = Value.UpperQValue

group by
      AllCTGs.MonitoringType
      ,Med.TimeBetweenAssessments
      ,Low.TimeBetweenAssessments
      ,Upp.TimeBetweenAssessments

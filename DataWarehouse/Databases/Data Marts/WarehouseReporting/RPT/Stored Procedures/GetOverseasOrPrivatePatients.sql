﻿




-- =============================================
-- Author:		<Alan Bruce >
-- Create date: <13-Mar-2012>
-- Description:	<Admitted and Non Admitted Patients For 1 week Overseas and Private>
-- =============================================
CREATE PROCEDURE [RPT].[GetOverseasOrPrivatePatients]
	-- Add the parameters for the stored procedure here
	@StartDate Datetime
	
AS

BEGIN	

Declare @EndDate DateTime

Set @EndDate = DATEADD(day,7,@StartDate)

SELECT 'IP' AS IPorOP
, PatientForename
, PatientSurname
, ConsultantCode
, AdmissionDate
,NULL AS AppointmentDate
, DischargeDate
, CasenoteNumber
, NHSNumber
, DistrictNo
, LocalAdminCategoryCode
, ContractSerialNo
,[EpisodeStartDate]
,[EpisodeEndDate]
,NULL AS AppointmentCreatedDate
,[PatientCategoryCode]
FROM  APC.Encounter
WHERE     
	(ContractSerialNo LIKE 'OSV%' OR LocalAdminCategoryCode <> 'NHS') 
	AND (AdmissionDate >= @StartDate AND AdmissionDate < @EndDate)
UNION
SELECT  'OP' AS IPorOP
, PatientForename
, PatientSurname
, ConsultantCode
,NULL AS AdmissionDate
, AppointmentDate
, DischargeDate
, CasenoteNo AS CasenoteNumber
, NHSNumber
, DistrictNo
, LocalAdminCategoryCode
, ContractSerialNo
,NULL AS [EpisodeStartDate]
,NULL AS [EpisodeEndDate]
,AppointmentCreateDate
,NULL AS [PatientCategoryCode]
FROM  OP.Encounter
WHERE 
	(ContractSerialNo LIKE 'OSV%' OR LocalAdminCategoryCode <> 'NHS') 
	AND (AppointmentDate >= @StartDate AND AppointmentDate < @EndDate)
Order By IPorOP

End





﻿





CREATE proc [RPT].[CHAMPUsage]
(
 @UsageDateStart datetime2(0)
,@UsageDateEnd datetime2(0)
)

as

/*===============================================================================
Notes:	This proc is structured to show all dates and all KPIs in the 
		SSRS report, even where data does not exist.

When		Who			What
24/02/2015	Paul Egan	Initial Coding for SSRS report
===============================================================================*/


/*============== Debug only ========================*/
--declare
-- @UsageDateStart datetime2(0) = '20141124'
--,@UsageDateEnd datetime2(0) = '20141130' --cast(getdate() - 1 as date)
--;
/*==================================================*/

With CalendarCTE as  -- To show all dates on report
(
select
	TheDate
from
	WarehouseReportingMerged.WH.Calendar
where
	TheDate between '20130101' and cast(getdate() as date)
	and TheDate <> '99991231'
)
,

allDataCTE as
(
select
	KPI = 'Contact Us Request'
	,KPIEventTime = ContactUsRequestTime
from
	Warehouse.COM.CHAMPContactUsRequest
	
union all

select
	KPI = 'BMI Calculator Usage'
	,KPIEventTime = BMICalculatorTime
from
	Warehouse.COM.CHAMPBMICalculator
	
union all

select
	KPI = 'New Accounts Created'
	,KPIEventTime = AccountCreatedTime
from
	Warehouse.COM.CHAMPAccount where AccountCreatedTime between '20140901' and '20150831'

union all

select
	KPI = 'Households Mailed'
	,KPIEventTime = AssessmentTime
from
	Warehouse.COM.CHAMPEncounter
where
	IsMailed = 1
)


,CalendarDataCTE as		-- Match every date with every KPI so the they don't disappear on the SSRS report when no data
(
select
	CalendarCTE.TheDate
	,DistinctKPI.KPI
from 
	CalendarCTE
	
	cross join
		(
		select distinct KPI
		from allDataCTE
		) DistinctKPI
)

--select * 
--from CalendarDataCTE
--order by
--	TheDate, KPI



,FinalDatasetCTE as
(
select
	KPI
	,KPIEventTime
	,CountOnReport = 1
from
	allDataCTE

	
union all


select
	CalendarDataCTE.KPI
	,CalendarDataCTE.TheDate
	,CountOnReport = 0
from
	CalendarDataCTE
)


select
	FinalDatasetCTE.KPI
	,FinalDatasetCTE.KPIEventTime
	,FinalDatasetCTE.CountOnReport
from
	FinalDatasetCTE
where
	cast(FinalDatasetCTE.KPIEventTime as date) between @UsageDateStart and @UsageDateEnd
order by
	KPIEventTime
	,KPI
;





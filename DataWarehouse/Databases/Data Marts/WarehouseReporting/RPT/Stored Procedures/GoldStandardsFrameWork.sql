﻿
CREATE PROCEDURE RPT.GoldStandardsFrameWork

(
	@thedate DATE = NULL
)

AS

SELECT @thedate = COALESCE (@thedate
						,CONVERT(DATE,DATEADD(DAY, -1,Getdate()))
						)

--4. Number of Emergency Admissions via A&E test
SELECT
	 [4. Number of Emergency Admissions via A&E test]  = SUM(CASE 
																WHEN MetricCode = 'CHKADM' 
																AND AdmissionMethodCode = 'AE' THEN 1
																ELSE 0
															END)
	,[5. Number of Direct Emergency Admissions] = SUM(	CASE WHEN MetricCode = 'CHKADM' 
																AND AdmissionMethodCode IN(
																						'18',
																						'BB',
																						'BM',
																						'DO',
																						'EM',
																						'ET',
																						'GP',
																						'OP'
																						) THEN 1
															ELSE 0
														END)
	,[8. Number of Elective Ordinary Admissions] = SUM(	CASE 
															WHEN MetricCode = ('IEADM') THEN 1
															ELSE 0
														END
														)
	,[9. Number of Discharges of Emergency Admissions] = SUM(	CASE 
																	WHEN MetricCode = ('EMDIS') THEN 1
																	ELSE 0
																END
															)
FROM
	apc.Fact
WHERE
	EncounterDate = @thedate
	AND SiteCode IN ('NRMC'
					,'RMCH'
					,'MRI');



﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [RPT].[GetEddWardPerformanceDT] 
(
	 @startdate smalldatetime =NULL
	,@enddate smalldatetime=NULL
	,@Divisioncode varchar(max)=NULL
	,@WardCode Varchar(MAX)=NULL
	,@EddStatus Varchar(MAX)=NULL
	,@ConsultantType  Varchar(MAX)=NULL
)

AS 

SELECT   
	 DivisionCode = ISNULL(Directorate.DivisionCode, 'Unknown')
	,Division = ISNULL(Division.Division, 'Unknown')
	,WardCode = ISNULL(Ward.WardCode, 'Unknown')
	,Ward = ISNULL(Ward.Ward, 'Unknown')
	,Consultant = ISNULL(Consultant.Consultant, 'Unknown')
	,Specialty = Specialty.Specialty
	,EddStatus =	CASE
						WHEN ExpectedLOS.EddCreatedTime IS NULL THEN 'No EDD'
						WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
							AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))  <=0  
							THEN 'Achieved EDD'
						WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
							AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0  
							THEN 'Failed EDD'
						ELSE 'Unknown'
					END
	,POD = case
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'2'
					,'3' --INTERVAL ADMISSION
					,'4'
					,'5'
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective
			--Non Elective
				else 'NE' --Non Elective
			end
	,CasenoteNumber = ExpectedLOS.CasenoteNumber
	,AdmissionTime = ExpectedLOS.AdmissionTime
	,DischargeTime = ExpectedLOS.DischargeTime
	,EddCreatedTime = ExpectedLOS.EddCreatedTime
	,EnteredWithinTarget =  CASE ExpectedLOS.EnteredWithin48hrs
								WHEN  1 THEN 'Yes'
								WHEN 0 THEN 'No'
								ELSE 'Unknown'
							END
	,ExpectedLOS = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))
	,ActualLOS = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) 
FROM         
	Warehouse.APC.ExpectedLOS ExpectedLOS
INNER JOIN Warehouse.WH.Directorate Directorate
	ON ExpectedLOS.DirectorateCode = Directorate.DirectorateCode 
	AND 
		(
			Directorate.DivisionCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@Divisioncode,','))
		OR
			@DivisionCode IS NULL
		)
INNER JOIN Warehouse.PAS.Ward Ward
	ON ExpectedLOS.Ward = Ward.WardCode
	AND 
		(
			Ward.WardCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@WardCode,','))
		OR
			@WardCode IS NULL
		)
INNER JOIN Warehouse.PAS.Consultant Consultant
	ON ExpectedLOS.Consultant = Consultant.ConsultantCode
INNER JOIN Warehouse.PAS.Specialty Specialty
	ON ExpectedLOS.Specialty =  Specialty.SpecialtyCode 
INNER JOIN Warehouse.WH.Division Division
	ON Directorate.DivisionCode = Division.DivisionCode
INNER JOIN Warehouse.PAS.AdmissionMethod AdmissionMethod
	ON	AdmissionMethod.AdmissionMethodCode = ExpectedLOS.AdmissionMethodCode
INNER JOIN Warehouse.PAS.ManagementIntention ManagementIntention
	ON	ManagementIntention.ManagementIntentionCode = ExpectedLOS.ManagementIntentionCode
WHERE     
		CONVERT(date, ExpectedLOS.DischargeTime, 103) >= @startdate --'14/Aug/2011'
	AND CONVERT(date, ExpectedLOS.DischargeTime, 103) <= @enddate --'14/Aug/2011'
	AND ExpectedLOS.ManagementIntentionCode NOT IN ('R','N') 
	AND NOT (ExpectedLOS.Ward = 'ESTU' AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6)
	AND ArchiveFlag <> 'D'
	AND Specialty.NationalSpecialtyCode <> '501'
	AND ExpectedLOS.Ward <> '76A'
	AND Specialty.Specialty NOT like '%DIALYSIS%'
	AND Specialty.Specialtycode NOT like 'IH%'
	AND ExpectedLOS.Ward NOT like 'SUB%'
	AND 
		(
			@EddStatus	=  CASE
								WHEN ExpectedLOS.EddCreatedTime IS NULL THEN 'No EDD'
								WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
									AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))  <=0  
									THEN 'Achieved EDD'
								WHEN ExpectedLOS.EddCreatedTime IS NOT NULL
									AND DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0  
									THEN 'Failed EDD'
								ELSE 'Unknown'
							END
		OR
			@EddStatus IS NULL
		)
		
	AND 
		(
			Case 
				when ExpectedLOS.Consultant in('IMD','SFD','MPB','SHS','SCO','PKUM','MAGS','CEWI','RLJB','AMD','SKN','EKOT') then 'Secondary'
				else 'Tertiary'
			end = @ConsultantType
		OR
			@ConsultantType IS NULL
		)

 


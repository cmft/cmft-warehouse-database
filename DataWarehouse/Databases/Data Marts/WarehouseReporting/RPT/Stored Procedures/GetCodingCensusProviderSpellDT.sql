﻿
CREATE PROCEDURE [RPT].[GetCodingCensusProviderSpellDT] 
(
	 @ProviderSpellNo Varchar(MAX) = NULL
)

AS

SELECT
	 Encounter.ProviderSpellNo
	,Encounter.SourceEncounterNo
	,Encounter.CasenoteNumber
	,Encounter.EpisodeStartTime
	,Encounter.EpisodeEndTime
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.ClinicalCodingCompleteTime
	,Encounter.ClinicalCodingStatus
FROM 
	APC.Encounter
WHERE 
		(
			ProviderSpellNo = @ProviderSpellNo
		OR
			@ProviderSpellNo IS NULL
		)
	
	
﻿

CREATE PROCEDURE [RPT].[CreatinineAlertDetail] 
(
	 @FromDate date
	,@ToDate date
	,@ReportType varchar(10) -- RATE, CHANGE or VALUE
	,@RateOfChange int
	,@Stage varchar (100) -- (Stage1 (>=1.5 to <2.0); Stage2 (>=2.0 to <3.0); Stage3 (>=3.0) 
	--,@PercentageChange numeric(21 , 10)
	--,@FromPercentageChange numeric(21 , 10)
	--,@ToPercentageChange numeric(21 , 10)
	,@ResultValue int
	,@Summary bit
	,@DistrictNo char(8)
	,@Interval numeric(21 , 10)
	
	,@CurrentInpatient tinyint
)

as

set dateformat dmy

select
	 PatientForename
	,PatientSurname
	,DistrictNo
	,AgeCode
	,Ethnicity 
	,Sex
	,LatestAdmissionDate
	,Consultant
	,Specialty
	,LatestWardCode
	,Result
	,ResultDate
	,ResultTime
	,SequenceNoPatient
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
	,CurrentInpatient
	,Result.LengthOfStay
	,DiedInHospital
	--,[PreviousSpell(12mths)WithRenalEpisode]
	,ReportSequenceNo
	,CasenoteNumber
	,DateOfBirth
	,Stage
	,MergeEpisodeNo
	,ResultAPCLinked
	,InhospitalAKI
	,ResultWardCode
	,SequenceNo
from
	(
	SELECT
		 Result.PatientForename
		,Result.PatientSurname
		,Result.DistrictNo
		,AgeCode = Age.Age
		,Ethnicity = EthnicOrigin.NationalEthnicOrigin
		,Sex = Result.SexCode
		,Result.LatestAdmissionDate
		,Consultant.Consultant
		,Specialty.Specialty
		,LatestWardCode = Result.WardCode
		,Result.Result
		,Result.ResultDate
		,Result.ResultTime
		,Result.SequenceNoPatient

		--,Result.PreviousResult
		--,Result.PreviousResultTime
		--,Result.ResultIntervalDays
		--,Result.ResultChange
		--,Result.CurrentToPreviousResultRatio

		,Result.BaselineResult
		,Result.BaselineResultTime
		,Result.BaselineToCurrentDays
		,Result.BaselineToCurrentChange
		,Result.CurrentToBaselineResultRatio

		--,Result.ResultRateOfChangePer24Hr

		,Result.CurrentInpatient 
		,Result.LengthOfStay
		,Result.DiedInHospital
		--,Result.[PreviousSpell(12mths)WithRenalEpisode]
		,ReportSequenceNo =
			case
			when @ReportType = 'RATE' then row_number () over (partition by Result.DistrictNo order by Result.ResultRateOfChangePer24Hr desc)
			when @ReportType = 'CHANGE' then  row_number () over (partition by Result.DistrictNo,Result.MergeEpisodeNo order by Result.CurrentToBaselineResultRatio desc)
			when @ReportType = 'LATEST' then  row_number () over (partition by Result.DistrictNo order by Result.ResultTime desc)
			when @ReportType = 'VALUE' then  row_number () over (partition by Result.DistrictNo,Result.MergeEpisodeNo order by Result.Result desc)
			end
		,Result.CasenoteNumber
		,Result.DateOfBirth
		,Result.Stage
		,MergeEpisodeNo
		,ResultAPCLinked
		,InhospitalAKI
		,ResultWardCode
		,SequenceNo = row_number () over (partition by Result.DistrictNo order by Result.ResultTime desc)
	FROM
		WarehouseReporting.OCM.CodedResult Result
--63
	left join WarehouseReporting.PAS.Consultant
	on	Consultant.ConsultantCode = Result.ConsultantCode

	left join WarehouseReporting.PAS.Specialty
	on	Specialty.SpecialtyCode = Result.SpecialtyCode

	LEFT JOIN WarehouseReporting.PAS.EthnicOrigin
	ON	EthnicOrigin.EthnicOriginCode = Result.EthnicOriginCode

	left join WarehouseReporting.WH.Age
	on	Age.AgeCode = Result.AgeCode collate Latin1_General_CI_AS

	where
		(
			--@Summary is null
			--and
			(
				--Result.DistrictNo in (Select Value from RPT.SSRS_MultiValueParamSplit(@DistrictNo,','))
				Result.DistrictNo = @DistrictNo
			or	@DistrictNo is null
			)
		)
	) Result

--WHERE 
--	(
--		Result.ResultDate between @FromDate and @ToDate
--	and	Result.AdmissionDate between @FromDate and @ToDate
--	and	(
--			Result.CurrentInpatient = @CurrentInpatient
--		or	@CurrentInpatient is null
--		)
--	and	(
--			--(
--			--	@ReportType = 'RATE'
--			--and	Result.ResultRateOfChangePer24Hr >= @RateOfChange
--			--and	Result.ReportSequenceNo = @Summary
--			--and Result.ResultIntervalDays >= @Interval
--			--)
--		--or	(
--		--		@ReportType = 'CHANGE'
--		--	and	Result.CurrentToPreviousResultRatio >= @PercentageChange
--		--	and	Result.ReportSequenceNo = @Summary
--		--	and Result.ResultIntervalDays >= @Interval
--		--	)
--		--or	
--			(
--				@ReportType = 'CHANGE'
--			and	Result.CurrentToBaselineResultRatio >= @PercentageChange
--			and	Result.ReportSequenceNo = @Summary
--			--and Result.BaselineToCurrentDays >= @Interval
--			)
--		or	(
--				@ReportType = 'VALUE'
--			and	Result.Result >= @ResultValue
--			and	Result.ReportSequenceNo = @Summary
--			)
--		)
--	)

--	-- drillthrough settings
--or
	--(
	--	@Summary is null
	--and	(
	--		Result.DistrictNo = @DistrictNo
	--	or	@DistrictNo is null
	--	)
	--)

--where
--	Result.Stage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
ORDER BY
	 Result.DistrictNo
	,Result.ResultTime desc
	--,Result.PreviousResultTime desc






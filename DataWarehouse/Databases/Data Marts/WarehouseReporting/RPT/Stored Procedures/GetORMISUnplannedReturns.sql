﻿

  
CREATE PROCEDURE [RPT].[GetORMISUnplannedReturns]
  @FromDate Date
 ,@ToDate   Date
 ,@Spec AS NVARCHAR(30) = 'All'

AS  

--DECLARE @FromDate AS DATE 
--DECLARE @ToDate AS DATE
--DECLARE @Spec AS NVARCHAR(20) = 'All'

--SET @FromDate = '01-May-2011'
--SET @ToDate   = '31-May-2011'

SELECT  
	 OP_MRN = OPER.[DistrictNo]                          
	,OP_OPERATION_DATE = OPER.[OperationDate]                       
	,OP_OPERAT_TYPE = OPER.[OperationTypeCode]                   
	,OP_FNAME = OPER.[Forename]                           
	,OP_LNAME = OPER.[Surname]                            
	,OP_DESCRIPTION = OPER.[Operation]                          
	,[UP_DESC] = 
		  CASE
		  WHEN UPLR.[UnplannedReturnReason] IS NULL THEN '***Not Specified***'
		  ELSE UPLR.[UnplannedReturnReason] 
		  END    
	,OP_SEQU = OPER.[SourceUniqueID]                     
	,OP_PA_SEQU = oper.[PatientSourceUniqueNo]              
	,[S1_DESC] = SPEC.[Specialty]                          
	,[TH_DESC] = THEAT.[Theatre]                            
	,OP_RETURN_TO_THEATRE = OPER.[UnplannedReturnToTheatreFlag]        

FROM 
	[Theatre].[EncounterOperation]               AS OPER

LEFT OUTER JOIN [Theatre].[UnplannedReturnReason] AS UPLR
	ON OPER.[UnplannedReturnReasonCode]            =  UPLR.[UnplannedReturnReasonCode]
LEFT OUTER JOIN [Theatre].[Specialty]             AS SPEC
	ON OPER.[SpecialtyCode]                        = SPEC.[SpecialtyCode]
LEFT OUTER JOIN [Theatre].[Theatre]               AS THEAT
	ON OPER.[TheatreCode]                          = THEAT.[TheatreCode]

WHERE 
	(
		OPER.[UnplannedReturnToTheatreFlag] = 1 
	OR 
		OPER.[UnplannedReturnToTheatreFlag] = 1
	)
AND OPER.[OperationDate] BETWEEN  @FromDate AND @ToDate
AND SPEC.[Specialty]     LIKE  ( CASE  WHEN @Spec ='All' THEN '%' 
                                       ELSE @Spec 
                                 END
                                )






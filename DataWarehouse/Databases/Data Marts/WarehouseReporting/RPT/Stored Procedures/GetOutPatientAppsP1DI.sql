﻿

  
CREATE PROCEDURE [RPT].[GetOutPatientAppsP1DI]
  @DateOfAppointment      Date
 ,@DateOfAppointmentStart Date


AS  

--DECLARE @DateOFAppointment AS DATE 
--SET @DateOfAppointment = '18-Jun-2011'


 SELECT 
	  OPE.[DisposalCode]
	 ,AppDate = 
		CONVERT(varchar,OPE.[AppointmentDate],103)
	 ,OPE.[PatientForename]
	 ,OPE.[PatientSurname]
	 ,DoB = 
		CONVERT(varchar,OPE.[DateOfBirth],103) 
	 ,Age = 
		(DATEDIFF(d, OPE.[DateOfBirth], OPE.[AppointmentDate])) / 365.25
	 ,OPE.[NHSNumber]
	 ,OPE.[CaseNoteNo]
	 ,OPE.[DistrictNo]
	 ,OPE.[Postcode]
	 ,OPE.[ConsultantCode]
	 ,ConsName = 
		CON.[Consultant] 
	 ,OPE.[SpecialtyCode]
	 ,SPE.[NationalSpecialtyCode]
	 ,OPE.[SiteCode]
	 ,OPE.[AppointmentStatusCode]
	 ,OPE.[BookingTypeCode]
	 ,OPE.[DerivedFirstAttendanceFlag]
 
 FROM [OP].[Encounter] OPE
	 LEFT OUTER JOIN [PAS].[Consultant] CON ON CON.[ConsultantCode] = OPE.[ConsultantCode] 
	 LEFT OUTER JOIN [PAS].[Specialty]  SPE ON SPE.[SpecialtyCode] = OPE.[SpecialtyCode]
  
 WHERE 
	OPE.[AppointmentDate] BETWEEN @DateOfAppointmentStart AND @DateOFAppointment
 AND left( OPE.[DisposalCode],4) = 'P1DI'
 AND [DirectorateCode] = '0'
 AND OPE.[AppointmentStatusCode] in('ATT','ATTS','SATT','WLK')
    

﻿


CREATE PROCEDURE [RPT].[CQUINPerformance_FNOF]

as

select
	MainQry.Goal
	,MainQry.Indicator
	,MainQry.TheMonth
	,MainQry.FinancialMonthKey
	,MainQry.[Target]
	,MainQry.Actual

from


								(select
									myqry.Goal
									,myqry.Indicator
									,myqry.[Target]
									,myqry.TheMonth
									,myqry.FinancialMonthKey
									,Actual = avg(myqry.LoS)

								from (
											select
												Goal = 'L9.2'
												,Indicator = 'Reduced average LoS #NoF'
												,Period = 'Month'
												,TheMonth
												,FinancialMonthKey
												,[Target] = 21.2
												,SourcePatientNo
												,AdmissionDate
												,DischargeDate
												,LoS = datediff(day, AdmissionDate, DischargeDate)
												,DischargeMonth = cast(TheMonth as datetime)
												,Cases = 1

											from 
												[APC].[Encounter] a
												inner join WH.Calendar
												on DischargeDate = TheDate

											where 
												DischargeDate between '20120401' and '20120531'
												and LastEpisodeInSpellIndicator = 'Y'
												and CodingCompleteDate is not null
												and AdmissionMethodCode in ('AE','EM','TR')
												and exists (
															select 
																1
															from 
																[APC].[Encounter] b
															where 
																(
																b.PrimaryDiagnosisCode like 's72.0%'
																or b.PrimaryDiagnosisCode like 's72.1%'
																or b.PrimaryDiagnosisCode like 's72.2%'
																)
																and a.ProviderSpellNo = b.ProviderSpellNo
															)
															
									)myqry

								group by
									myqry.Goal
									,myqry.Indicator
									,myqry.Period
									,myqry.[Target]
									,myqry.TheMonth
									,myqry.FinancialMonthKey)MainQry


order by FinancialMonthKey asc




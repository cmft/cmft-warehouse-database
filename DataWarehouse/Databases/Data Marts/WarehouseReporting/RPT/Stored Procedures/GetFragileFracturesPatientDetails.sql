﻿

CREATE proc [RPT].[GetFragileFracturesPatientDetails]

@SourcePatientNo varchar(20)
,@SourceSpellNo varchar(20)


as

select distinct
	CasenoteNumber
	,DistrictNo
	,PatientSurname
	,PatientForename
	,DateOfBirth
	,DateOfDeath
	,AdmissionDate
	,AdmissionMethodCode
	,TheMonth

from
	APC.Encounter
	
INNER JOIN WarehouseReporting.WH.Calendar Calendar
ON	Calendar.TheDate = Encounter.DischargeDate

where
	SourcePatientNo = @SourcePatientNo
AND SourceSpellNo = @SourceSpellNo



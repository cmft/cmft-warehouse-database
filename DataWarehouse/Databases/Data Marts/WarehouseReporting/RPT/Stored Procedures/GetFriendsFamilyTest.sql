﻿

create proc 
		[RPT].[GetFriendsFamilyTest]

		(
		 @Month datetime 
		)

as

	SET @Month = dateadd(month, datediff(month,0,@Month),0)
	
--declare @Month datetime
--set @Month = '2014-02-01'

		select			

						FFTMonth =				'feb-2014',
						FFTValue =				case	
														when FFTMain.AwrNbr in (68,696,676,907) then 1 
														when FFTMain.AwrNbr in (69,697) then 2 
														when FFTMain.AwrNbr in (70)then 3 
														when FFTMain.AwrNbr in (71)then 4
														when FFTMain.AwrNbr in (72,238,908)then 5
														when FFTMain.AwrNbr in (910)then 6
														else NULL 
												end,
						FFTDivision =			case	
														when DvnNme in ('N/A') then OnDvnNme
														else DvnNme 
												end,
						FFTLocation =			case	
														when OnLtnNbr is null then [LocMap]
														else OnLtnNbr 
												end,				
						[Count] = count(1)
		from			
						(
								select	
										[XmlFileID],[RspNbr],[AwrNbr],[AwrTxt],[SvyNbr],[SvyDsc],[LtnNbr],[LtnNme],[QtnNbr],[DteCrt],[ChangedDate]
								from	
										[Warehousesql].[PatientExperience].[Staging].[CRTViewdata]
								where	
										[QtnNbr] in (86,236,820,850,1062,1071,1072,1076,1083,1192,1308,1385,1388,1390,1483) --FFT Question --maternity 1385,1388,1390,1483
								and		
										dateadd(month, datediff(month,0,DteCrt),0) = @Month	 
						) FFTMain
						
		inner join		
						(
								select	
										LocMap = LtnNme2,LtnNbr,LtnNme,Type,DvnNme,Rtn
								from	
										[warehouse2\dev].SandBoxCH.dbo.FFTLocationMap --Location Map

						) Location
						
		on				
						FFTMain.LtnNbr = Location.LtnNbr 


		full outer join	
						(
								select	
										RspNbr,AwrNbr,AwrTxt,DteCrt,LtnNbr
								from	
										Warehousesql.PatientExperience.Staging.CRTViewdata
								where	
										QtnNbr in (40,442,711,1064,1143) --Discharge Question
								and		
										AwrNbr in (44)
								and
										dateadd(month, datediff(month,0,DteCrt),0) = @Month	
						) Discharge
						
		on				FFTMain.RspNbr = Discharge.RspNbr and FFTMain.LtnNbr = Discharge.LtnNbr

		left join	
						(
								select  
											[RspNbr],[DteCrt],QtnTxt,OnDivDepart.[LtnNbr],SelectedLocation,OnLtnNbr,OnDvnNme,OnRtn = FFTOnlineLocation.[Rtn], OnDch = 'Yes' , OnTyp = FFTLocationMap.[Type]
								from
											(
												select	
														Department.RspNbr,QtnTxt,Department.DteCrt,Department.LtnNbr,AwrTxtDiv = Division.AwrTxt,AwrTxtDep = Department.AwrTxt,
														SelectedLocation =	case	
																				when Division.AwrTxt is null then Department.AwrTxt
																				when Department.AwrTxt = Division.AwrTxt then Division.AwrTxt
																				else Division.AwrTxt+Department.AwrTxt 
																			end
												
												from	
														Warehousesql.PatientExperience.Staging.CRTViewdata Department
												
														left join	
																	(
																		select	
																				RspNbr,AwrNbr,DteCrt,LtnNbr,
																				AwrTxt =	case 
																								when AwrTxt like 'Sain%' then 'SMH' 
																								else AwrTxt 
																							end --' in Saint Mary's???
																		from	
																				Warehousesql.PatientExperience.Staging.CRTViewdata
																		where	
																				QtnNbr in (1283)  --Division Question
																	) Division
															
												on		Department.[RspNbr] = Division.[RspNbr] and Department.[DteCrt] = Division.[DteCrt] and Department.[LtnNbr]= Division.[LtnNbr]
												
												where	
														(Department.[QtnNbr] in (1181,1248,1249,1250,1251,1253,1286,1190) or ([QtnNbr] in (1283) and Department.[AwrTxt] in ('MRI - HDU','MRI - ICU'))) --Department Question
												and		
														dateadd(month, datediff(month,0,Department.[DteCrt]),0) = @Month
																					
											) OnDivDepart
								
								left join  
											[warehouse2\dev].SandboxCH.dbo.FFTOnlineLocation --Online Location
								on		
											SelectedLocation = OnLtnNme
								
								left join 
											[warehouse2\dev].SandBoxCH.dbo.FFTLocationMap
								on 
											LtnNme2 = OnLtnNbr
								
								where 
											dateadd(month, datediff(month,0,DteCrt),0) = @Month

								group by 
											RspNbr,DteCrt,QtnTxt,OnDivDepart.LtnNbr,SelectedLocation,OnLtnNbr,OnDvnNme,FFTOnlineLocation.Rtn,FFTLocationMap.[Type]
												
						) SelectedLocation
						
		on				FFTMain.RspNbr = SelectedLocation.RspNbr and FFTMain.LtnNbr = SelectedLocation.LtnNbr

		where			
						dateadd(month, datediff(month,0,FFTMain.[DteCrt]),0) = @Month

		and				(case	
							when [Type] = 'MAT' then OnTyp 
							else [Type]
						end) = 'FCE'
		group by
						cast(dateadd(month, datediff(month,0,FFTMain.[DteCrt]),0) as date),
						FFTMain.AwrNbr,
						FFTMain.AwrTxt,
						DvnNme,
						OnDvnNme,
						OnLtnNbr,
						LocMap
						
union

		select
				FFTMonth = Discharge.TheMonth
				,FFTValue = 7
				,FFTDivision = DivMap.[LtnDiv]
				,FFTLocation = EndWardTypeCode
				,[Count] = count(1)
		from
			WarehouseReportingMerged.APC.Encounter

		inner join
			WarehouseReportingMerged.WH.[Specialty] Specialty
		on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID

		full outer join
			[warehouse2\dev].[SandboxCH].[dbo].[FFTLocationSelect] DivMap
		on	[EndWardTypeCode] = DivMap.[LtnNme]

		inner join
			Warehousesql.WarehouseReportingMerged.APC.[AdmissionMethod] AdmissionMethod
		on	AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID

		inner join
			WarehouseReportingMerged.WH.Calendar Admission
		on  Admission.DateID = Encounter.AdmissionDateID
			   
		inner join
			WarehouseReportingMerged.WH.Calendar Discharge
		on  Discharge.DateID = Encounter.DischargeDateID

		inner join
			WarehouseReportingMerged.APC.PatientClassification PatientClassification
		on	PatientClassificationID = PatientClassification.SourcePatientClassificationID

		inner join
			   WarehouseReportingMerged.[APC].[DischargeDestination] DischargeDestination
		on     DischargeDestination.SourceDischargeDestinationID = Encounter.DischargeDestinationID
		where
			   Discharge.TheDate  = @Month
		and
			   Encounter.[NationalLastEpisodeInSpellIndicator] = '1'
		and
				Specialty.NationalSpecialtyCode not in ('424', '501', '422') --Obsetrics, Neonatal & Well Babies	   
		and
			   floor(datediff(day, DateOfBirth, Admission.TheDate) / 365.25) > = 16
		and
				Encounter.EndSiteCode not in ('NRMC')	   
		and
			   NationalPatientClassificationCode not in ('3', '4')						
		and
			   [EndWardTypeCode] not in ('OMU','AMU','MAU2','ERU','UDC','90','55D','ETCD','54','SAL','TH','CRC','DL','DSUN','LNH','OSDU','SUBS','SUBM','SUBE','PDEN','POAU','AIN','WCH','CONS','XRAY')
		and
			   LOS > = 1
		and
			   DateofDeath is null  
		and		
			  NationalDischargeDestinationCode not in ('49','51','53','79')
			  
		group by 
				DivMap.[LtnDiv]
				,EndWardTypeCode
				,Discharge.TheMonth
				 

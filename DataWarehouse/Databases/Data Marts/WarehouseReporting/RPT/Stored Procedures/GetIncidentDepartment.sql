﻿

CREATE proc 
	[RPT].[GetIncidentDepartment]
	
	(
	 @Division int
	)

as

--declare @Division int 
--set @Division = null

select
	DivisionCode
	,DivisionDescription
	,DepartmentCode
	,DepartmentDescription
from
		(
			select 

					Division.DivisionCode
					,DivisionDescription
					,Department.DepartmentCode
					,DepartmentDescription
			from	
					Warehouse.Incident.Incident Incident
			  
			inner join  
					Warehouse.Incident.Division Division
			on
					Incident.DivisionCode collate latin1_general_cs_as = Division.MixedCaseCode collate latin1_general_cs_as
			inner join  
					Warehouse.Incident.Department Department
			on
					Incident.DepartmentCode collate latin1_general_cs_as = Department.MixedCaseCode collate latin1_general_cs_as
					
			group by
			
					Division.DivisionCode
					,DivisionDescription
					,Department.DepartmentCode
					,DepartmentDescription
		union

			select 

					Division.DivisionCode
					,DivisionDescription
					,DepartmentCode = null
					,DepartmentDescription = 'Total'
			from	
					Warehouse.Incident.Incident Incident
			  
			inner join  
					Warehouse.Incident.Division Division
			on
					Incident.DivisionCode collate latin1_general_cs_as = Division.MixedCaseCode collate latin1_general_cs_as
					
			group by
			
					Division.DivisionCode
					,DivisionDescription
					
		union

			select 

					DivisionCode = null
					,DivisionDescription = 'Trust'
					,DepartmentCode = null
					,DepartmentDescription = 'Trust'
				
		) Departs		

where
		coalesce(@Division, 9999) = coalesce(DivisionCode,9999)

			
order by
	DepartmentDescription

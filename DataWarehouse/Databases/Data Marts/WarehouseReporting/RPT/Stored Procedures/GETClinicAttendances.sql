﻿





CREATE PROCEDURE [RPT].[GETClinicAttendances] 
	 @ClinicCode		varchar(10)
	,@SessionCode       varchar(10) 
	,@ClinicDate        Date

AS 

--DECLARE @ClinicCode	AS	varchar(10)= 'CFALIFOL'
--DECLARE @SessionCode  AS  varchar(10)= 'FALIFOL' 
--DECLARE @ClinicDate AS DATE = '23-DEC-2014'


SELECT 
	 Encounter.ClinicCode
	,Clinic.Clinic
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime
	,Encounter.EncounterRecno
	,Encounter.NHSNumber
	,Encounter.CasenoteNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname

	,DoB = 
		CONVERT(VARCHAR(10) , Encounter.DateOfBirth , 103)   

	,Encounter.Postcode
	,Encounter.DNACode
	,Encounter.AttendanceOutcomeCode
     
	--,AttDesc = 
	--	CASE Encounter.AttendanceOutcomeCode
	--	WHEN 'ATT' THEN 'Attended'
	--	WHEN 'H'   THEN 'Hospital Cancellation'
	--	WHEN 'P'   THEN 'Patient Cancellation'
	--	WHEN 'DNA' THEN 'Did Not Attend'
	--	WHEN 'NR'  THEN 'Not Recorded'
	--	ELSE Encounter.AttendanceOutcomeCode
	--	END
  ,AttDesc = 
		CASE Encounter.AppointmentStatusCode
		
		WHEN 'ATT'  THEN 'Attended'
		WHEN 'ATTP' THEN 'Attended By Telephone'
		WHEN 'H'    THEN 'Hospital Cancellation'
		WHEN 'P'    THEN 'Patient Cancellation'
		WHEN 'CND'  THEN 'CND'
		WHEN 'NATT' THEN 'Attended Late Not Seen'
		WHEN 'DNA'  THEN 'Did Not Attend'
		WHEN 'NR'  THEN
		   CASE Encounter.[CancelledByCode]
		   WHEN 'P' THEN 'Patient Cancellation' 
		   WHEN 'H' THEN 'Hospital Cancellation'
		   ELSE 'Not Recorded'
		   END
		ELSE ISNULL(Encounter.AppointmentStatusCode,'Not Recorded')        
		END  
		 
	,AttendYes = 
		CASE Encounter.DNACode
		WHEN 5 THEN 1
		ELSE 0
		END   
      
	,AttendNo = 
		CASE Encounter.DNACode
		WHEN 5 THEN 0
		ELSE 1
		END 
      
	,Encounter.AppointmentTypeCode

FROM 
 WarehouseReportingMerged.OP.Encounter 

LEFT OUTER JOIN Warehousesql.WarehouseReporting.PAS.Clinic 
ON Encounter.ClinicCode = Clinic.ClinicCode
  
WHERE 
	Encounter.AppointmentDate = @ClinicDate
AND Encounter.ClinicCode      = @ClinicCode
AND [Encounter].DoctorCode    = @SessionCode 
  






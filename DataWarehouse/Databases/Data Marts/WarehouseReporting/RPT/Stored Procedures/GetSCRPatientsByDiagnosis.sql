﻿



  
CREATE PROCEDURE [RPT].[GetSCRPatientsByDiagnosis]
  @StartRepDate  Date
 ,@TumourDescGroup   varchar(50) = NULL
 ,@EndRepDate    Date

 
AS  
--DECLARE @StartRepDate   AS DATE 
--DECLARE @EndRepDate     AS DATE
--DECLARE @TumourGroup    AS nvarchar(20)
--SET     @StartRepDate = '01-APR-2012'
--SET     @EndRepDate   = '30-APR-2012'
--SET     @TumourDescGroup  = 'Lung'
  

SELECT 
       DEM.[PATIENT_ID]                                    AS PatId
      ,REF.[CARE_ID]                                       AS CareId
      ,DEM.[N1_1_NHS_NUMBER]                               AS NHS_Num
      ,DEM.[N1_2_HOSPITAL_NUMBER]                          AS Casenote
      ,DEM.[N1_6_FORENAME]                                 AS Forename
      ,DEM.[N1_5_SURNAME]                                  AS Surname
      ,CONVERT(varchar,DEM.[N1_10_DATE_BIRTH],103)         AS DoB
      ,DEM.[N1_13_PCT]                                     AS PCT
      ,REF.[L_CANCER_SITE]                                 AS TumourGroup
      ,CONVERT(varchar,REF.[N2_6_RECEIPT_DATE],103)        AS RefRecdDate
      ,REF.[N4_2_DIAGNOSIS_CODE]                           AS DiagCode
      ,DIA.[DIAG_DESC]                                     AS DiagDesc
      ,REF.[N2_8_SPECIALTY]                                AS SpecCode
      ,SPE.[SPECIALTY_DESC]                                AS SpecDesc
      ,REF.[N2_4_PRIORITY_TYPE]                            AS PriorityTypeCode
      ,PRI.[PRIORITY_DESC]                                 AS PriorityTypeDesc
      ,REF.[N2_1_REFERRAL_SOURCE]                          AS SoRCode
      ,REF.[N2_13_CANCER_STATUS]                           AS StatusCode
      ,STA.[STATUS_DESC]                                   AS StatusDesc
      ,REF.[GP_PRACTICE_CODE]                              AS PracticeCode
      ,CASE
         WHEN REF.[N2_4_PRIORITY_TYPE]  ='03' AND REF.[N4_2_DIAGNOSIS_CODE] IS NULL      THEN 1
         ELSE 0
       END  AS 'HSC-NONDIAG' 
      ,CASE
         WHEN REF.[N2_4_PRIORITY_TYPE]  ='03' AND REF.[N4_2_DIAGNOSIS_CODE] IS NOT NULL  THEN 1
         ELSE 0
       END   AS 'HSC-DIAG'     
      ,CASE
         WHEN REF.[N2_4_PRIORITY_TYPE]  <>'03' AND REF.[N4_2_DIAGNOSIS_CODE] IS NULL     THEN 1
         ELSE 0
       END  AS 'NONHSC-NONDIAG' 
      ,CASE
         WHEN REF.[N2_4_PRIORITY_TYPE]  <>'03' AND REF.[N4_2_DIAGNOSIS_CODE] IS NOT NULL THEN 1
         ELSE 0
       END   AS 'NONHSC-DIAG'  
       ,CASE
         WHEN REF.[N2_4_PRIORITY_TYPE]  ='03'  AND REF.[N4_2_DIAGNOSIS_CODE] IS NULL     THEN 'No Diag HSC'
         WHEN REF.[N2_4_PRIORITY_TYPE]  ='03'  AND REF.[N4_2_DIAGNOSIS_CODE] IS NOT NULL THEN 'Diag HSC'
         WHEN REF.[N2_4_PRIORITY_TYPE]  <>'03' AND REF.[N4_2_DIAGNOSIS_CODE] IS NULL     THEN 'No Diag Non HSC'
         WHEN REF.[N2_4_PRIORITY_TYPE]  <>'03' AND REF.[N4_2_DIAGNOSIS_CODE] IS NOT NULL THEN 'Diag Non HSC'
        END   AS 'DiagType'   
                                                                                                           
FROM            [CancerRegister].[dbo].[tblDEMOGRAPHICS]      AS DEM
INNER JOIN      [CancerRegister].[dbo].[tblMAIN_REFERRALS]    AS REF ON REF.[PATIENT_ID] = DEM.[PATIENT_ID] 
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblSTATUS]           AS STA ON REF.[N2_13_CANCER_STATUS] = STA.[STATUS_CODE] 
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblSPECIALTIES]      AS SPE ON REF.[N2_8_SPECIALTY] = SPE.[SPECIALTY_CODE]
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblDIAGNOSIS]        AS DIA ON DIA.[DIAG_CODE] = REF.[N4_2_DIAGNOSIS_CODE]
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblPRIORITY_TYPE]    AS PRI ON PRI.[PRIORITY_CODE] = REF.[N2_4_PRIORITY_TYPE]  


WHERE  
	REF.[N2_6_RECEIPT_DATE] BETWEEN @StartRepDate AND @EndRepDate
AND    REF.[L_CANCER_SITE] LIKE  (
                                   CASE  WHEN @TumourDescGroup ='All' THEN '%' 
                                   ELSE @TumourDescGroup 
                                   END
                                    )
      









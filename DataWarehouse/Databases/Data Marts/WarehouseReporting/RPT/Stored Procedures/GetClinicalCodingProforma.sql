﻿








CREATE proc [RPT].[GetClinicalCodingProforma]

@starttime smalldatetime
,@endtime smalldatetime
,@waitinglist bit
--,@HospitalCode varchar(15)
--,@division varchar(max)
,@ward varchar(max)
,@consultant varchar(max)


as

--set @starttime = coalesce(@starttime, getdate())
--set @endtime = coalesce(@endtime, dateadd(hour, 7, getdate()))

if @waitinglist = 0

begin

select
	
	DatasetType = 'Admission'
	,encounter.SourcePatientNo
	,encounter.CasenoteNumber
	,PatientName = [PatientSurname] + ', ' + [PatientForename]
	,encounter.DateOfBirth
	,[StartWardTypeCode]
	,ward.Ward
	,ward.SiteCode
	,[Consultant]
	,[AdmissionTime]
	,DiagnosisGroup = longtermdiagnosis.[Diagnosis]
	,IsAlwaysPresent = longtermdiagnosis.[IsAlwaysPresentFlag] 

from
	[APCUpdate].[Encounter] encounter
inner join 
	PAS.Consultant consultant
on	consultant.ConsultantCode = encounter.ConsultantCode
inner join
   PAS.Ward ward
on  ward.WardCode = encounter.StartWardTypeCode
--inner join
--   PAS.Directorate directorate
--on  directorate.DirectorateCode = encounter.StartDirectorateCode

left outer join 

				(
					select 
						distinct
						apcdiagnosis.[SourcePatientNo]
						,diagnosis.[Diagnosis]
						--,longtermdiagnosis.DiagnosisGroup
						,diagnosis.[IsAlwaysPresentFlag] 
					from
						[WarehouseReporting].[APC].[Diagnosis] apcdiagnosis
					--inner join
					--	[WarehouseReporting].[WH].[LongTermDiagnosis] longtermdiagnosis
					--on	apcdiagnosis.[DiagnosisCode] = longtermdiagnosis.[DiagnosisCode]
					inner join
						[WarehouseReporting].[WH].Diagnosis diagnosis
					on	diagnosis.[DiagnosisCode] = apcdiagnosis.[DiagnosisCode]

				) longtermdiagnosis
				on	longtermdiagnosis.[SourcePatientNo] = encounter.[SourcePatientNo]


where
	encounter.[FirstEpisodeInSpellIndicator] = 'Y'
	and AdmissionTime between @starttime AND @endtime
	and [DischargeTime] is null
	--and directorate.Division in (select Value from RPT.SSRS_MultiValueParamSplit(@division,','))
	and encounter.StartWardTypeCode in (select Value from RPT.SSRS_MultiValueParamSplit(@ward,','))
	and encounter.ConsultantCode in (select Value from RPT.SSRS_MultiValueParamSplit(@consultant,','))

	--and (
	--	(
	--	  encounter.[StartWardTypeCode] not in ('76A','76B','76C') 
	--	  and  
	--	directorate.DivisionCode = @division
	--    )
 --   or  (
	--	  encounter.[StartWardTypeCode] in ('MAU','15M') 
	--	  and 
	--	  directorate.DivisionCode = @division
	--	)
	--	)
	--and encounter.[StartSiteCode] = @HospitalCode

end

else if @waitinglist = 1

begin

select
	DatasetType = 'WaitingList'
	,encounter.SourcePatientNo
	,encounter.CasenoteNumber
	,PatientName = [PatientSurname] + ', ' + [PatientForename]
	,encounter.DateOfBirth
	,[StartWardTypeCode] =
				case
					when encounter.WardCode in ('76B','76C')
					then '76B and 76C'
					else encounter.WardCode
				end
	,Ward = 
			case
				when encounter.WardCode in ('76B','76C')
				then '76B and 76C'
				else ward.Ward
			end
	,ward.SiteCode
	,[Consultant]
	,[AdmissionTime] = [TCIDate]
	,DiagnosisGroup = longtermdiagnosis.[Diagnosis]
	,[IsAlwaysPresent] = longtermdiagnosis.[IsAlwaysPresentFlag]
	
from
	[APC].[WaitingList] encounter

inner join 
	PAS.Consultant consultant
on	consultant.ConsultantCode = encounter.ConsultantCode
inner join
   PAS.Ward ward
on  ward.WardCode = encounter.WardCode
--inner join
--   PAS.Directorate directorate
--on  directorate.DirectorateCode = encounter.DirectorateCode


inner join
	(
	select
		CensusDate = max(CensusDate)
	from
		[APC].[WaitingList] encounter
	inner join
	   PAS.Directorate directorate
	on  directorate.DirectorateCode = encounter.DirectorateCode
	where
		TCIDate between dateadd(day, 1, dateadd(dd, datediff(dd, 0, @endtime), 0)) and cast(dateadd(day, 1, dateadd(dd, datediff(dd, 0, @endtime), 0)) + '23:59:59' as datetime)
		--and encounter.[SiteCode] = @HospitalCode
		--and encounter.[WardCode] in ('76A','76B','76C')
	--and directorate.Division in (select Value from RPT.SSRS_MultiValueParamSplit(@division,','))
	and encounter.WardCode in (select Value from RPT.SSRS_MultiValueParamSplit(@ward,','))
	and encounter.ConsultantCode in (select Value from RPT.SSRS_MultiValueParamSplit(@consultant,','))
	) 
	maxcensusdate
on	maxcensusdate.CensusDate = encounter.CensusDate
		

left outer join 

				(
					select 
						distinct
						apcdiagnosis.[SourcePatientNo]
						,diagnosis.[Diagnosis]
						--,longtermdiagnosis.DiagnosisGroup
						,diagnosis.[IsAlwaysPresentFlag] 
					from
						[WarehouseReporting].[APC].[Diagnosis] apcdiagnosis
					--inner join
					--	[WarehouseReporting].[WH].[LongTermDiagnosis] longtermdiagnosis
					--on	apcdiagnosis.[DiagnosisCode] = longtermdiagnosis.[DiagnosisCode]
					inner join
						[WarehouseReporting].[WH].Diagnosis diagnosis
					on	diagnosis.[DiagnosisCode] = apcdiagnosis.[DiagnosisCode]

				) longtermdiagnosis
				on	longtermdiagnosis.[SourcePatientNo] = encounter.[SourcePatientNo]
where
	TCIDate between dateadd(day, 1, dateadd(dd, datediff(dd, 0, @endtime), 0)) and cast(dateadd(day, 1, dateadd(dd, datediff(dd, 0, @endtime), 0)) + '23:59:59' as datetime)
	--and encounter.[SiteCode] in (@HospitalCode)
	--and encounter.[WardCode] in ('76A','76B','76C')
	and encounter.ManagementIntentionCode <> 'N'
	--and @waitinglist = 1

	--and directorate.Division in (select Value from RPT.SSRS_MultiValueParamSplit(@division,','))
	and encounter.WardCode in (select Value from RPT.SSRS_MultiValueParamSplit(@ward,','))
	and encounter.ConsultantCode in (select Value from RPT.SSRS_MultiValueParamSplit(@consultant,','))


--option(recompile)

end









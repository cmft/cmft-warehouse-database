﻿



CREATE proc [RPT].[GetPortalClinicalCoding]
(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
,@nhsnumber varchar(50) = null
,@casenotenumber varchar(50) = null
,@patientsurname varchar(50) = null
,@procedure varchar(50) = null
,@rcodeinprimarydiagnosis varchar(50) = null
)

as

--declare @consultant varchar(50)
--		,@start datetime
--		,@end datetime

set dateformat dmy

set @consultant = --'CMMC\neil.parrott'
					case
						when @consultant in
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\neil.parrott'
						/* for CSS */
						when @consultant in
											(
											'CMMC\John.Butler'
											,'CMMC\Rachael.Challiner'
											,'CMMC\Bernard.Foex'
											,'CMMC\Ruari.Greer'
											,'CMMC\Steve.Jones'
											,'CMMC\Michael.Parker'
											,'CMMC\Srikanth.Amudalapalli'
											,'CMMC\roger.slater'
											,'CMMC\steve.benington'
											,'CMMC\daniel.conway'
											,'CMMC\dougal.atkinson'
											,'CMMC\maheshhan.nirmalan'
											,'CMMC\ magnus.garrioch'
											,'CMMC\John.Moore'
											,'CMMC\hilary.gough' 
											)
						then 'CMMC\Jane.Eddleston'
						else @consultant
					end

--set @start = '1 jan 2012'
--set @end = '30 apr 2012'

--if object_id('tempdb..#test') is not null
--drop table #test

select --top 100
	encounter.EncounterRecno
	,encounter.SourcePatientNo
	,encounter.NHSNumber
	,encounter.CasenoteNumber
	,PatientName = encounter.PatientSurname + ', ' + encounter.PatientForename
	,encounter.AdmissionTime
	,AdmissionMethod.AdmissionMethodType
	,encounter.PatientCategoryCode
	,encounter.DischargeTime
	,PatientDeceased = 
						case
							when DischargeMethodCode in ('DN','DP','SP','ST') then 'Y'
							else null
						end
	,encounter.EpisodeStartTime
	,encounter.EpisodeEndTime
	,encounter.SourceEncounterNo
	,consultant.Consultant
	,consultant.DomainLogin
	,specialty.NationalSpecialty
	,EpisodicHRG = hrg.HRG
	,EpisodicHRGTariff = 
				case
					when PatientCategoryCode = 'DC'
					then coalesce(CombinedDaycaseOrElectiveTariff,DaycaseTariff)
					when PatientCategoryCode = 'EL'
					then coalesce(CombinedDaycaseOrElectiveTariff,ElectiveSpellTariff)
					else NonElectiveSpellTariff
				end			
	,WithComplication = 
						case
							when HRG.HRG like '%with CC%' then 'Y'
							when HRG.HRG like '%with Intermediate CC%' then 'Y'
							when HRG.HRG like '%with Major CC%' then 'Y'
						end
	,PrimaryDiagnosis = primarydiagnosis.Diagnosis
	,RCodeInPrimaryDiagnosis =
							case
								when encounter.PrimaryDiagnosisCode like 'R%'
								then 'Y'
							end
	,SubsidiaryDiagnosis = subsidiarydiagnosis.Diagnosis
	,SecondaryDiagnosis1 = secondarydiagnosis1.Diagnosis
	,SecondaryDiagnosis2 = secondarydiagnosis2.Diagnosis
	,SecondaryDiagnosis3 = secondarydiagnosis3.Diagnosis
	,SecondaryDiagnosis4 = secondarydiagnosis4.Diagnosis
	,SecondaryDiagnosis5 = secondarydiagnosis5.Diagnosis
	,SecondaryDiagnosis6 = secondarydiagnosis6.Diagnosis
	,SecondaryDiagnosis7 = secondarydiagnosis7.Diagnosis
	,SecondaryDiagnosis8 = secondarydiagnosis8.Diagnosis
	,SecondaryDiagnosis9 = secondarydiagnosis9.Diagnosis
	,SecondaryDiagnosis10 = secondarydiagnosis10.Diagnosis
	,SecondaryDiagnosis11 = secondarydiagnosis11.Diagnosis
	,SecondaryDiagnosis12 = secondarydiagnosis12.Diagnosis
	,DischargeSummaryDiagnosis = 
							case
								when dischargesummary.Diagnosis = 'N/A'
								then null
								else dischargesummary.Diagnosis 
							end
	,PreviousComorbidityNotInCurrentEpisode
			= case
				when exists
							(
								select
									1
								from
									APC.Encounter
								unpivot
									(
									Code for [Sequence] in 
									(
									--PrimaryDiagnosisCode
									--,SubsidiaryDiagnosisCode
									SecondaryDiagnosisCode1
									,SecondaryDiagnosisCode2
									,SecondaryDiagnosisCode3
									,SecondaryDiagnosisCode4
									,SecondaryDiagnosisCode5
									,SecondaryDiagnosisCode6
									,SecondaryDiagnosisCode7
									,SecondaryDiagnosisCode8
									,SecondaryDiagnosisCode9
									,SecondaryDiagnosisCode10
									,SecondaryDiagnosisCode11
									,SecondaryDiagnosisCode12
									)
									) previousdiagnosis
									
								inner join WH.Diagnosis Diagnosis
								on	previousdiagnosis.Code = Diagnosis.DiagnosisCode
								and Diagnosis.IsCharlsonFlag = 1
								
								where
									previousdiagnosis.SourcePatientNo = encounter.SourcePatientNo
								and previousdiagnosis.EpisodeStartTime < encounter.EpisodeStartTime
								and previousdiagnosis.Code not in 
																(
																	select 
																		Code
																	from
																		APC.Encounter
																		
																	unpivot
																		(
																		Code for [Sequence] in 
																		(
																		--PrimaryDiagnosisCode
																		--,SubsidiaryDiagnosisCode
																		SecondaryDiagnosisCode1
																		,SecondaryDiagnosisCode2
																		,SecondaryDiagnosisCode3
																		,SecondaryDiagnosisCode4
																		,SecondaryDiagnosisCode5
																		,SecondaryDiagnosisCode6
																		,SecondaryDiagnosisCode7
																		,SecondaryDiagnosisCode8
																		,SecondaryDiagnosisCode9
																		,SecondaryDiagnosisCode10
																		,SecondaryDiagnosisCode11
																		,SecondaryDiagnosisCode12
																		)
																		) currentdiagnosis
																		
																		where 
																			currentdiagnosis.SourcePatientNo = encounter.SourcePatientNo
																		and currentdiagnosis.EpisodeStartTime = encounter.EpisodeStartTime
																		)
																								
																)
				then 'Y'
				else null
			end
	,PrimaryOperation = primaryoperation.Operation
	,SecondaryOperation1 = secondaryoperation1.Operation
	,SecondaryOperation2 = secondaryoperation2.Operation
	,SecondaryOperation3 = secondaryoperation3.Operation
	,SecondaryOperation4 = secondaryoperation4.Operation
	,SecondaryOperation5 = secondaryoperation5.Operation
	,SecondaryOperation6 = secondaryoperation6.Operation
	,SecondaryOperation7 = secondaryoperation7.Operation
	,SecondaryOperation8 = secondaryoperation8.Operation
	,SecondaryOperation9 = secondaryoperation9.Operation
	,SecondaryOperation10 = secondaryoperation10.Operation
	,SecondaryOperation11 = secondaryoperation11.Operation
	,ORMISProcedure = ormisprocedure.ProcedureDescription
	,ConsultantComments = receivedforms.FormXML.value('declare namespace ns1="http://www.cmft.nhs.uk/InfoPath.xsd";   
	(/ns1:InfoPathSubmission[1]/ns1:Review[1]/ns1:Comments[1]/text()[1])','varchar(max)')
	,clinicalcodingforms.ClinicalCodingReviewFormID
				

from
	APC.Encounter encounter
	
inner join PAS.Consultant consultant
on	encounter.ConsultantCode = consultant.ConsultantCode

left outer join PAS.AdmissionMethod admissionmethod
on	encounter.AdmissionMethodCode = AdmissionMethod.AdmissionMethodCode

left outer join Warehouse.APC.HRG4Encounter hrgencounter
on	hrgencounter.EncounterRecno = encounter.EncounterRecno

left outer join WH.HRG hrg
on	HRG.HRGCode = hrgencounter.HRGCode

left outer join PAS.Specialty specialty
on	specialty.SpecialtyCode = encounter.SpecialtyCode

left outer join WH.Diagnosis primarydiagnosis
on	encounter.PrimaryDiagnosisCode = primarydiagnosis.DiagnosisCode

left outer join WH.Diagnosis subsidiarydiagnosis
on	encounter.SubsidiaryDiagnosisCode = subsidiarydiagnosis.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis1
on	encounter.SecondaryDiagnosisCode1 = secondarydiagnosis1.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis2
on encounter.SecondaryDiagnosisCode2 = secondarydiagnosis2.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis3
on	encounter.SecondaryDiagnosisCode3 = secondarydiagnosis3.DiagnosisCode	

left outer join WH.Diagnosis secondarydiagnosis4
on	encounter.SecondaryDiagnosisCode4 = secondarydiagnosis4.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis5
on	encounter.SecondaryDiagnosisCode5 = secondarydiagnosis5.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis6
on	encounter.SecondaryDiagnosisCode6 = secondarydiagnosis6.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis7
on	encounter.SecondaryDiagnosisCode7 = secondarydiagnosis7.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis8
on	encounter.SecondaryDiagnosisCode8 = secondarydiagnosis8.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis9
on	encounter.SecondaryDiagnosisCode9 = secondarydiagnosis9.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis10
on	encounter.SecondaryDiagnosisCode10 = secondarydiagnosis10.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis11
on	encounter.SecondaryDiagnosisCode11 = secondarydiagnosis11.DiagnosisCode

left outer join WH.Diagnosis secondarydiagnosis12
on	encounter.SecondaryDiagnosisCode12 = secondarydiagnosis12.DiagnosisCode

left outer join WH.Operation primaryoperation
on encounter.PrimaryOperationCode = primaryoperation.OperationCode

left outer join WH.Operation secondaryoperation1
on encounter.SecondaryOperationCode1 = secondaryoperation1.OperationCode

left outer join WH.Operation secondaryoperation2
on encounter.SecondaryOperationCode2 = secondaryoperation2.OperationCode

left outer join WH.Operation secondaryoperation3
on encounter.SecondaryOperationCode3 = secondaryoperation3.OperationCode

left outer join WH.Operation secondaryoperation4
on encounter.SecondaryOperationCode4 = secondaryoperation4.OperationCode

left outer join WH.Operation secondaryoperation5
on encounter.SecondaryOperationCode5 = secondaryoperation5.OperationCode

left outer join WH.Operation secondaryoperation6
on encounter.SecondaryOperationCode6 = secondaryoperation6.OperationCode

left outer join WH.Operation secondaryoperation7
on encounter.SecondaryOperationCode7 = secondaryoperation7.OperationCode

left outer join WH.Operation secondaryoperation8
on encounter.SecondaryOperationCode8 = secondaryoperation8.OperationCode

left outer join WH.Operation secondaryoperation9
on encounter.SecondaryOperationCode9 = secondaryoperation9.OperationCode

left outer join WH.Operation secondaryoperation10
on encounter.SecondaryOperationCode10 = secondaryoperation10.OperationCode

left outer join WH.Operation secondaryoperation11
on encounter.SecondaryOperationCode11 = secondaryoperation11.OperationCode

left outer join 
				(
					select 
						SourcePatientNo
						,DischargeDate
						,Diagnosis
					from
						APC.DischargeSummary
					where
						IsFirstDocumentForDischarge = 1
				) 
				dischargesummary
				
on	dischargesummary.SourcePatientNo = encounter.SourcePatientNo
and dischargesummary.DischargeDate = encounter.DischargeDate

outer apply
			(
				select
					top 1 ProcedureDescription
				from
					Warehouse.APC.EncounterProcedureDetail encounterproceduredetail
				inner join Warehouse.Theatre.ProcedureDetail proceduredetail
				on	encounterproceduredetail.ProcedureDetailSourceUniqueID = proceduredetail.SourceUniqueID
				where 
					encounterproceduredetail.EncounterRecno = encounter.EncounterRecno
				order by
					ProcedureStartTime asc
			) 
			ormisprocedure
			
left outer join Warehouse.WH.NationalTariff nationaltariff -- just until we can get the SLAM data combined
on nationaltariff.[HRGCode] = hrgencounter.HRGCode
and encounter.DischargeDate between EffectiveFrom and EffectiveTo 

left outer join [Information].[Information].[ClinicalCodingReviewForms] clinicalcodingforms
on encounter.SourcePatientNo = clinicalcodingforms.SourcePatientNo
and encounter.EpisodeStartTime =clinicalcodingforms.EpisodeStartTime

left outer join InfoPathRepository.InfoPath.ReceivedForms receivedforms
on receivedforms.ID = ClinicalCodingReviewFormID

where
	consultant.DomainLogin = @consultant
	and encounter.DischargeDate between @start and @end
	and encounter.CodingCompleteDate is not null
	and (encounter.NHSNumber = @nhsnumber or @nhsnumber is null)
	and (encounter.CasenoteNumber = @casenotenumber or @casenotenumber is null)
	and (encounter.PatientSurname = @patientsurname or @patientsurname is null)
	and (encounter.PrimaryOperationCode = @procedure or @procedure is null)
	and (
		case
			when encounter.PrimaryDiagnosisCode like 'R%'
			then 'Y'
		end = @rcodeinprimarydiagnosis or @rcodeinprimarydiagnosis is null)
	and encounter.PrimaryDiagnosisCode not like 'O04%'
	and encounter.PrimaryOperationCode not like 'Q11%'
	and encounter.PrimaryOperationCode not like 'Q12%'
	
option (recompile)





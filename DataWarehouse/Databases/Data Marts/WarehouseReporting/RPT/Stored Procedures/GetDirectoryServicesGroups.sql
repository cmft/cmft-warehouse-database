﻿
CREATE proc 
		[RPT].[GetDirectoryServicesGroups]

		(
		 @Group varchar(MAX) 
		)

as

--declare @Group varchar(max)
--set @Group = null

select distinct
		DistributionGroup = @Group
		,Name = case	when LastName + ' ' + FirstName is null 
						then substring(DomainLogin,charindex('\',DomainLogin)+1,100)
						else LastName + ', ' + FirstName
						end
		,DomainLogin
		,EmailAddress
from
		CMFTDataServices.DirectoryServices.AD_Group_Members
		
where
		(
		AD_Group_Members.DistributionGroup = @Group
		or 
		@Group is null 
		)
		
group by
		EmailAddress
		,DomainLogin
		,case	when LastName + ' ' + FirstName is null 
				then substring(DomainLogin,charindex('\',DomainLogin)+1,100)
				else LastName + ', ' + FirstName
				end
﻿
CREATE PROCEDURE [RPT].[GetDivision]

AS


select
	Division
from
	(
	select
		 Divisioncode = '0'
		,Division = '(Select All)'
		,SortOrder = 0

	union

	SELECT  
   		 DivisionCode
   		,Division
   		,SortOrder = ROW_NUMBER() over (order by Division)
	from
		WH.Division
	where
		DivisionCode not in ('9' , 'MHT' , 'NTUSE' , 'RSRCH')
	) Division
order by
	SortOrder

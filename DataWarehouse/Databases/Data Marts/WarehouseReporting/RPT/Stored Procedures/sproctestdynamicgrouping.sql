﻿
CREATE PROCEDURE RPT.sproctestdynamicgrouping 

(
	@groupcolumn1 NVARCHAR(MAX),
	@groupcolumn2 NVARCHAR(MAX),
	@StartDate DATE,
	@EndDate Date
) 

AS

BEGIN

DECLARE @sql NVARCHAR(MAX) = 'SELECT ' 
									+ 'GroupColumn1 = encounter.' + @groupcolumn1 + 
									', GroupColumn2 = encounter.' + @groupcolumn2 + '
									,ReferralDate = ReferralDate
									,CountOfReferrals =  Count(EncounterRecno)
								FROM 
									rf.encounter encounter
								WHERE 
									ReferralDate BETWEEN ''' + CONVERT(CHAR(10), @startdate, 120) + '''
									AND ''' + CONVERT(CHAR(10), @enddate, 120) + '''
								GROUP BY 
									encounter.ReferralDate, encounter.' + @groupcolumn1 + ', encounter.' + @groupcolumn2 + ';';

EXEC sp_executesql @sql; 


END

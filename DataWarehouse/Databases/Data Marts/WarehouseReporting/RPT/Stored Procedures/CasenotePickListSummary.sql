﻿CREATE procedure RPT.CasenotePickListSummary
	 @DaysOffset tinyint = 10
	,@EncounterTypeCode varchar(3)
as

select
	 PickList.EncounterTypeCode

	,EncounterType =
		case EncounterTypeCode
		when 'NEW' then 'New OP'
		when 'FUP' then 'Follow-Up OP'
		when 'TCI' then 'Elective IP'
		when 'NEL' then 'Non-Elective IP'
		end

	,PickList.ServicePointCode
	,PickList.ServicePoint
	,PickList.EncounterDate
	,Bookings = count(distinct DistrictNo)
	,Casenotes = count(*)
from
	(
	select
		 Booking.EncounterTypeCode
		,Booking.ServicePointCode

		,ServicePoint =
			coalesce(Clinic.Clinic , Ward.Ward)

		,Booking.EncounterDate
		,Booking.DistrictNo
	from
		WarehouseOLAP.dbo.BasePASCasenoteBooking Booking

	left join WarehouseOLAP.dbo.OlapClinic Clinic
	on	Clinic.ClinicCode = Booking.ServicePointCode
	and	Booking.EncounterTypeCode in ( 'FUP' , 'NEW' )

	left join WarehouseOLAP.dbo.OlapWard Ward
	on	Ward.WardCode = Booking.ServicePointCode
	and	Booking.EncounterTypeCode in ( 'TCI' , 'NEL' )

	where
		EncounterDate = dateadd(d , @DaysOffset , cast(getdate() as date))
	and	(
			Booking.EncounterTypeCode = @EncounterTypeCode
		or	@EncounterTypeCode is null
		)

	and	Booking.OtherOPWaits + Booking.OtherAPCWaits + Booking.IsCurrentInpatient = 0
	) PickList

group by
	 PickList.EncounterTypeCode
	,PickList.ServicePointCode
	,PickList.ServicePoint
	,PickList.EncounterDate
order by
	 PickList.EncounterTypeCode
	,PickList.ServicePointCode
	,PickList.ServicePoint
	,PickList.EncounterDate


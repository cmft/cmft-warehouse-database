﻿


CREATE proc [RPT].[CurrentInpatients] 

	 @CCG varchar(20) = null
	,@Site varchar(20) = null
	,@Consultant varchar (20) = null

as

--declare @CCG varchar(20) =		'02A'
--declare @Site varchar(20) =		'MRI'

select
	CasenoteNumber
	,DistrictNo
	,PatientName = PatientSurname + ', ' + PatientForename
	,DateOfBirth
	,EpisodeStartTime
	,NationalSpecialty = NationalSpecialtyCode + ' - ' + NationalSpecialty.Specialty
	,Consultant
	,Site.SiteCode
	,Ward = EndWardTypeCode
	,Encounter.Postcode
	,RegisteredGP = Gp.Gp + ' - ' + Gp.NationalCode		--Requested to add GP
	,AdmissionDate										--Requested to add Admission Time
	,Created = 
			(
				select
					max(Created)
				from
					APCUpdate.Encounter
			)
	
from
	APCUpdate.Encounter
	
inner join PAS.Consultant
on	Encounter.ConsultantCode = Consultant.ConsultantCode

inner join PAS.Specialty
on	Encounter.SpecialtyCode = Specialty.SpecialtyCode

inner join WH.TreatmentFunction NationalSpecialty
on	NationalSpecialty.SpecialtyCode = Specialty.NationalSpecialtyCode

inner join PAS.Site 
on	Encounter.EndSiteCode = Site.SiteCode

inner join Organisation.ODS.Postcode
on	Postcode.Postcode = Encounter.Postcode

inner join Warehouse.PAS.Gp
on	Gp.GpCode = Encounter.RegisteredGpCode

where
	DischargeDate is null
and
	not exists
			(
			select
				1
			from
				APCUpdate.Encounter LatestEncounter
			where
				LatestEncounter.ProviderSpellNo = Encounter.ProviderSpellNo
			and
				LatestEncounter.EpisodeStartTime > Encounter.EpisodeStartTime
			)
and
	(
		Postcode.CCGCode = @CCG
	or
		@CCG is null
	)
	
and
	(
		Encounter.EndSiteCode = @Site
	or
		@Site is null
	)

and
	(
		Encounter.ConsultantCode = @Consultant
	or
		@Consultant is null
	)
﻿


CREATE proc [RPT].[GetDictationSuspendedAwaitingTranscription]
(
	@division varchar(100) = null
	,@specialty varchar(100) = null
	,@zerolength int = null
)

as

select
	Document.SourceUniqueID
	,Document
	,[Group].[Group]
	,[Group].Specialty
	,[Group].Division
	,[Length]
	,IsZeroLength = 
					case
						when [Length] = 0
						then 1
						else 0
					end
	,[WorkType].WorkType
	,Transcriptionist = transcriptionist.[User]
	,Author = author.[User]
	,CreationDate
	,DocumentAge = datediff(day, CreationDate, getdate())	
	,CorrectionDate
	,[DocumentType]
	,[status].[Status]
from
	[Dictation].[Document] document
inner join
	[Dictation].[Status] [status] 
on	document.[StatusCode] = [status].[StatusCode]
inner join
	[Dictation].[Group] [group] on document.[GroupCode] = [group].[GroupCode]
left outer join
	[Dictation].[Worktype] worktype 
on	document.[WorktypeCode] = worktype.[WorktypeCode]
left outer join
	Dictation.[User] transcriptionist 
on	document.TranscriptionistCode = transcriptionist.UserCode 
left outer join
	Dictation.[User] author 
on	document.AuthorCode = author.UserCode 
left outer join
	Dictation.DocumentType DocumentType 
on	document.[DocumentTypeCode] = DocumentType.[DocumentTypeCode] 

where
	([group].Division = @division or @division is null)
and 
	([group].SpecialtyCode = @specialty or @specialty is null)
and
	(
	(
	[status].[Status] in
						(
						'Dictation in progress'
						,'Transcription in progress'
						)
and
	datediff(day, CreationDate, getdate()) > 1
	)
	
or
	(
	[status].[Status] in
						(
						'Awaiting dictation'
						)
and
	datediff(day, CreationDate, getdate()) > 2
	)
or
	(
	[status].[Status] in
						(
						'Suspended for transcription'
						,'Awaiting word export'
						,'Error'
						,'Recognition in progress'
						,'Awaiting recognition'
						)
	)
	)

and
	(
	case
		when [Length] = 0
		then 1
		else 0
	end = @zerolength 
	
	or @zerolength is null
	)
	


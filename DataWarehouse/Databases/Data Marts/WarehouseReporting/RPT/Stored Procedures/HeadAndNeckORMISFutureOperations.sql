﻿










create PROCEDURE [RPT].[HeadAndNeckORMISFutureOperations] 

  @FromDate    Date
 ,@ToDate      Date
 --,@SpecialtyCode  varchar(10)
 ,@SessionCode    AS Integer

AS  


--DECLARE @FromDate       AS DATE        = '10-AUG-2013'
--DECLARE @ToDate         AS DATE        = '14-AUG-2013'
--@SpecialtyCode   AS varchar(10)
--DECLARE @SessionCode    AS Integer     = 128346



SELECT
   [SS_SEQU]
  ,[S1_CODE]                                AS SpecialtyCode
  ,[S1_CODE] + ' - ' + [S1_DESC]            AS SpecialtyName
  ,[TH_CODE]
  ,[TH_DESC]   
  ,[SS_START_DATE_TIME]
  ,[PA_EST_START]
  ,[SS_FINISH_DATE_TIME]
  ,[SU_CODE]                                 AS ConsultantCode
  ,[SU_LNAME] + ' - ' + [SU_FNAME]           AS Consultant
  ,[PA_LAST_NAME] + ' - ' + [PA_FIRST_NAME]  AS Patient
  ,REPLACE([PA_MRN],'Y','/')                 AS Casenote
  ,'PatientBooking' =
    CASE 
    WHEN [PA_LAST_NAME] IS NULL THEN 0
    ELSE 1
    END
  ,[SS_SESSION]
  ,[SS_CANCELLED]
  ,[SS_REASON]
  ,[SS_COMMENTS]
  ,[CC_DATE_TIME_CANCELLED]
  ,[CC_DETAILS]

FROM warehousesql.[otprd].[dbo].[F_SessionSumm]

LEFT OUTER JOIN warehousesql.[otprd].[dbo].[FPATS]
  ON [SS_SEQU] = [PA_SS_SEQU]
  
LEFT OUTER JOIN warehousesql.[otprd].[dbo].[FCCITEMS] 
  ON [PA_SEQU] = [CC_PA_SEQU] 

LEFT OUTER JOIN warehousesql.[otprd].[dbo].[FTHEAT]
  ON [SS_TH_SEQU] = [TH_SEQU]
  
LEFT OUTER JOIN warehousesql.[otprd].[dbo].[FS1SPEC]
  ON [SS_S1_SEQU] = [S1_SEQU] 
  
LEFT OUTER JOIN warehousesql.[otprd].[dbo].[FSURGN]
  ON [SS_CONS_SEQU] = [SU_SEQU] 
  
WHERE [SS_START_DATE_TIME] BETWEEN @FromDate AND DATEADD(d,1,@ToDate)
  AND [S1_CODE] in (120, 140, 144)
  AND LEFT([TH_CODE],1) <> 'C'
  AND [SS_CANCELLED] = 0
  AND [CC_DATE_TIME_CANCELLED] IS NULL
  AND ([SS_SEQU] = @SessionCode OR @SessionCode IS NULL) 
  
ORDER BY [S1_DESC],[SS_START_DATE_TIME],[PA_EST_START]





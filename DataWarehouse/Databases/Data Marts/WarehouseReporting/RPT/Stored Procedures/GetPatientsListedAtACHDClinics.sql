﻿


CREATE proc [RPT].[GetPatientsListedAtACHDClinics]

@FinancialMonthKey INT

AS

SELECT 
	Hospital = OPEncounter.SiteCode
	,OPEncounter.SourcePatientNo
	,OPEncounter.SourceEncounterNo
	,OPEncounter.CaseNoteNo
	,OPEncounter.DistrictNo
	,OPEncounter.PatientForename
	,OPEncounter.PatientSurname
	,OPEncounter.ConsultantCode
	,OPEncounter.SpecialtyCode
	,OPEncounter.ClinicCode
	,OPEncounter.AppointmentTime
	,OPEncounter.AppointmentTypeCode
	,OPEncounter.AppointmentStatusCode
	,Calendar.TheMonth
	,Calendar.FirstDayOfWeek
	,Calendar.LastDayOfWeek
FROM 
	WarehouseReporting.OP.Encounter OPEncounter
	
INNER JOIN WarehouseReporting.WH.Calendar Calendar
ON	Calendar.TheDate = OPEncounter.AppointmentDate

WHERE

ClinicCode IN('GUCHF', 'GUCHN', 'VSMFOL', 'VSMNEW', 'JDFOL', 'JDNEW', 'AHYF', 'AHYN', 'ACHDNUR', 
					'ACHDPRE', 'VSMIF', 'VSMIN', 'AISA', 'ADCARD', 'VSMPHT', 'AAC', 'ADCARDN', 'SVBC')
AND Calendar.FinancialMonthKey = @FinancialMonthKey

ORDER BY
	FirstDayOfWeek

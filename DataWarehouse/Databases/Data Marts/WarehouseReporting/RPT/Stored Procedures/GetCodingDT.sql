﻿
CREATE PROCEDURE [RPT].[GetCodingDT]

(
 @StartDate Date 
,@EndDate Date
,@Division Varchar(MAX) = NULL
,@NationalSpecialtyCode Varchar(MAX) = NULL
,@CodingLagDays INT = NULL
,@CodingLagDaysGroup varchar(5) = NULL
,@FinancialYear Varchar(MAX) = NULL
,@TheMonth Varchar(MAX) = NULL
,@ClinicalCodingCompleteDate Varchar(MAX) = NULL
)

AS

SELECT
	 Specialty = Specialty.NationalSpecialty
	,Division = Directorate.Division
	,DischargeDate = encounter.DischargeDate
	,AdmissionDate = encounter.AdmissionDate
	,EpisodeEndDate = encounter.EpisodeEndDate
	,EpisodeStartDate = encounter.EpisodeStartDate
	,ManagementIntentionCode = encounter.ManagementIntentionCode
	,CasenoteNumber = encounter.CasenoteNumber
	,DistrictNo = encounter.DistrictNo
	,PatientCategoryCode = encounter.PatientCategoryCode
	,NHSNumber = encounter.NHSNumber
	,EndWardTypeCode = encounter.EndWardTypeCode
	,ConsultantCode = encounter.ConsultantCode
	,CodingLagDays = Encounter.CodingLagDays
	,CodingLagDaysGroup = CASE 
							WHEN Encounter.CodingLagDays Between 0 AND 6 THEN convert(varchar(5),Encounter.CodingLagDays)
							WHEN Encounter.CodingLagDays BETWEEN 7 AND 13 THEN '07-14' 
							WHEN Encounter.CodingLagDays BETWEEN 14 AND 20 THEN '14-21' 
							WHEN Encounter.CodingLagDays BETWEEN 21 AND 27 THEN '21-28'
							ELSE '28+'
						  END
	,CasesWithinTarget = CASE
							WHEN Encounter.CodingLagDays <=5 THEN 1
							ELSE 0
						 END
	,ClinicalCodingCompleteDate

FROM 
	(
		SELECT
			  CodingLagDays =  CASE 
									WHEN DATEDIFF(D,Encounter.DischargeDate,Encounter.ClinicalCodingCompleteDate) <0 THEN 0 
									ELSE DATEDIFF(D,Encounter.DischargeDate,Encounter.ClinicalCodingCompleteDate) 
							   END
			 ,Cases
			 ,SourceUniqueID
			 ,DischargeDate
			 ,AdmissionDate
			 ,EpisodeEndDate
			 ,EpisodeStartDate
			 ,ManagementIntentionCode
			 ,CasenoteNumber
			 ,DistrictNo
			 ,PatientCategoryCode
			 ,NHSNumber
			 ,SpecialtyCode
			 ,EndWardTypeCode
			 ,ConsultantCode
			 ,EndDirectorateCode
			 ,ClinicalCodingCompleteDate
		FROM
			apc.Encounter
		WHERE 	
				ClinicalCodingStatus = 'C'
			AND ClinicalCodingCompleteDate >= @StartDate
			AND ClinicalCodingCompleteDate <= @EndDate
		) Encounter
LEFT OUTER JOIN PAS.Specialty Specialty
	ON Specialty.SpecialtyCode = Encounter.SpecialtyCode
LEFT OUTER JOIN PAS.Directorate Directorate
	ON Directorate.DirectorateCode = Encounter.EndDirectorateCode
LEFT OUTER JOIN  WH.Calendar 
	ON Encounter.ClinicalCodingCompleteDate  = Calendar.TheDate  

	
WHERE
		(
			Directorate.Division IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
		OR
			@Division IS NULL
		)
	AND 
		(
			Specialty.NationalSpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
		OR
			@NationalSpecialtyCode IS NULL
		)
	AND
		(
			Encounter.CodingLagDays IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@CodingLagDays,','))
		OR
			@CodingLagDays IS NULL
		)
	AND
		(
			CASE 
				WHEN Encounter.CodingLagDays Between 0 AND 6 THEN convert(varchar(5),Encounter.CodingLagDays)
				WHEN Encounter.CodingLagDays BETWEEN 7 AND 13 THEN '07-14' 
				WHEN Encounter.CodingLagDays BETWEEN 14 AND 20 THEN '14-21' 
				WHEN Encounter.CodingLagDays BETWEEN 21 AND 27 THEN '21-28'
				ELSE '28+'
			END IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@CodingLagDaysGroup,','))
		OR
			@CodingLagDaysGroup IS NULL
		)
		AND
		(
			Calendar.FinancialYear COLLATE Latin1_General_CI_AS IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@FinancialYear,','))
		OR
			@FinancialYear IS NULL
		)
		
		AND
		(
			Calendar.TheMonth COLLATE Latin1_General_CI_AS IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@TheMonth,','))
		OR
			@TheMonth IS NULL
		)
		AND
		(
			Encounter.ClinicalCodingCompleteDate IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@ClinicalCodingCompleteDate,','))
		OR
			@ClinicalCodingCompleteDate IS NULL
		)
		
		
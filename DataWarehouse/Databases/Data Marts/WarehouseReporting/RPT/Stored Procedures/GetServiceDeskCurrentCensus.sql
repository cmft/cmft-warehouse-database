﻿
CREATE PROCEDURE [RPT].[GetServiceDeskCurrentCensus]

AS

SELECT
	 Census.CensusDate
	,Census.BreachStatus 
	,Census.RequestStatus
	,Census.Category
	,Census.Workgroup
	,AssignedTo = ISNULL(Census.AssignedTo,'Not Assigned')
	,Census.Priority
	,PriorityKey = 
		CASE Census.Priority
			WHEN '4 Hours' THEN 0
			WHEN '8 Hours' THEN 1
			WHEN '5 Days' THEN 2
			WHEN '2 Weeks' THEN 3
			WHEN '6 Weeks' THEN 4
			ELSE 5
		END
	,Census.CallerOrganization
	,Census.Classification 
	,Calendar.FinancialYear
	,Calendar.TheMonth
	,Calendar.FinancialMonthKey
	,Census.Request
	,Census.StartTime
	,Census.DeadlineTime
	,Census.PredictedBreachStatus
	,Census.CreatedBy
	,Census.Template
	,Census.CallerFullName
	,Census.History
	,Census.Impact
	,Census.SourceUniqueID
FROM
	ServiceDesk.Census
INNER JOIN WH.Calendar Calendar
	ON Census.CensusDate = Calendar.TheDate

WHERE
	Census.CensusDate = (SELECT MAX(CensusDate) FROM ServiceDesk.Census)
	AND Census.RequestStatus NOT IN('Closed','Informed', 'Resolved')


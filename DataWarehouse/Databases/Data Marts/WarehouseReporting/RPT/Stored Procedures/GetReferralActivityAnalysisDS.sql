﻿

CREATE PROCEDURE [RPT].[GetReferralActivityAnalysisDS]


(      
       @PCTCode varchar(max)= 'All'
       ,@Division varchar(max)= NULL
       ,@Specialtycode varchar(max)= NULL 
       ,@GPPractice varchar(max)= 'All'
)

AS

BEGIN
       SET NOCOUNT ON;

DECLARE @TheDate Date = convert(date,getdate(),113)
DECLARE @EndOfWeekRange Date= DATEADD(dd, -datepart(dw, @TheDate)+1, @TheDate)
DECLARE @StartOfWeekRange Date = DATEADD(dd,1,DATEADD(WEEK, -52, @EndOfWeekRange))
DECLARE @EndofDayRange Date = @TheDate
DECLARE @StartOfDayRange Date =  DATEADD(dd,-56,@TheDate)
DECLARE @StartofMonthRange Date =  DATEADD(month, datediff(month, 0, @TheDate) - 25, 0)
DECLARE @EndofMOnthRange Date =  DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@TheDate),0))

SELECT 
        RDay = E.referraldate
       ,RWeek = LastDayOfWeek
       ,Rmonth = C.FinancialMonthKey
       ,TotRefs = COUNT(E.SourceEncounterNo)
       ,Dflag =      case 
                                  when E.referraldate >= @StartOfDayRange 
                                   And E.referraldate < dateadd(dd,1,@EndofDayRange)
                                         then 1        
                                  else 0        
                           end
       ,Wflag =      case 
                                  when E.referraldate >= @StartOfWeekRange 
                                  And E.referraldate < dateadd(dd,1,@EndofWeekRange)
                                          then 1        
                                  else 0
                           end           
       ,Yflag =      case 
                           when E.referraldate >= @StartOfMonthRange 
                            And E.referraldate < dateadd(dd,1,@EndofMonthRange)
                                         then 1        
                                  else 0        
                           end
       ,Div = Directorate.Division
       ,Spec = Specialty.NationalSpecialty
FROM     
       RF.Encounter E 
inner join WH.Calendar as C on E.referraldate = C.thedate
INNER JOIN PAS.Specialty Specialty 
       ON Specialty.SpecialtyCode = E.SpecialtyCode
       AND 
              (
                     NationalSpecialtyCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@specialtycode,','))
              OR
                     @specialtycode IS NULL
              )
INNER JOIN PAS.Directorate Directorate   
       ON Directorate.DirectorateCode = E.DirectorateCode
       AND 
              (
                     Directorate.Division IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
              OR
                     @Division IS NULL
              )
INNER JOIN ORG.PCT PCT 
       ON PCT.PCTCode = E.PCTCode
       AND 
              (
                     PCT.PCTCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@PCTCode,','))
              OR
                     @PCTCode = 'All'
              )
INNER JOIN WH.Practice Practice 
       ON Practice.PracticeCode = E.EpisodicGpPracticeCode
       AND 
              (
                     Practice.PracticeCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@GPPractice,','))
              OR
                     @GPPractice ='All'
              )
WHERE  
       E.referraldate >= @StartofMonthRange
       
GROUP BY 
        E.referraldate
       ,C.LastDayOfWeek
       ,C.FinancialMonthKey
       ,Directorate.Division
       ,Specialty.NationalSpecialty

END;



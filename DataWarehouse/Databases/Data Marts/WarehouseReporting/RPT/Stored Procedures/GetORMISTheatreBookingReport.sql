﻿











  
CREATE PROCEDURE [RPT].[GetORMISTheatreBookingReport]
  @Unit        varchar(25)
 ,@FromDate    Date
 ,@ToDate      Date
 ,@SessionCode Int     = NULL

AS  


--DECLARE @Unit     AS varchar(25)
--DECLARE @FromDate AS DATE 
--DECLARE @ToDate   AS DATE
--SET @FromDate = '06-Aug-2012'
--SET @ToDate   = '12-Aug-2012'
--SET @Unit     = 'C Paediatrics'

SELECT
 [TH].[OperatingSuiteCode]          AS UN_CODE
,[TH].[OperatingSuite]              AS UN_DESC
,[TH].[TheatreCode1]                AS TH_CODE
,[TH].[Theatre]                     AS TH_DESC
,[SESS].[TheatreSessionPeriodCode]  AS SS_SESSION
,[PB].[TheatreSessionSort]          AS PA_TH_SESS_SORT
,SESS_SORT =
   CASE  [SESS].[TheatreSessionPeriodCode]
   WHEN   'AM' THEN 1
   WHEN   'PM' THEN 2
   WHEN   'VN' THEN 3
   ELSE             4
 END
,SESSION =
   CASE  [SESS].[TheatreSessionPeriodCode]
   WHEN   'AM' THEN 'AM'
   WHEN   'PM' THEN 'PM'
   WHEN   'VN' THEN 'EVE'
   ELSE             'OTHER'
 END
,AM =
   CASE  [SESS].[TheatreSessionPeriodCode]
   WHEN   'AM' THEN 1
   ELSE            0
 END 
 ,PM =
   CASE [SESS].[TheatreSessionPeriodCode]
   WHEN   'PM' THEN 1
   ELSE            0
 END 
 ,EVE =
   CASE [SESS].[TheatreSessionPeriodCode]
   WHEN   'VN' THEN 1
   ELSE            0
 END 
  ,OTHER =
   CASE 
   WHEN  [SESS].[TheatreSessionPeriodCode] IN ('AM','PM','VN')  THEN 0
   ELSE            1
 END 

,CONVERT(VARCHAR(10),[SESS].[SessionDate],103)            AS OperationDate
,CONVERT(VARCHAR(17),[PB].[EstimatedStartTime],108)       AS EstStartTime
,[PB].[DistrictNo]                                        AS Casenote
,[PB].[Surname] + ' -' + [PB].[Forename]                  AS PatName
,CONVERT(VARCHAR(10),[PB].[DateOfBirth],103)              AS DateOfBirth
,[PB].[Operation]                                         AS OperationComment
,[ST].[StaffName]                                         AS Surgeon
,[SPEC].[Specialty]                                       AS S1_DESC  
,[TH].[TheatreCode]
,[ST].[StaffCode]
,[PB].[SessionSourceUniqueID]                             AS SessSequ

FROM [Theatre].[EncounterPatientBooking]                AS PB

LEFT OUTER JOIN [Theatre].[FactSession]                 AS SESS
ON [PB].[SessionSourceUniqueID] = [SESS].[SourceUniqueID]
LEFT OUTER JOIN [Theatre].[Theatre]                     AS TH
ON [PB].[TheatreCode]           = [TH].[TheatreCode]
LEFT OUTER JOIN [Theatre].[Staff]                       AS ST
ON [PB].[ConsultantCode]        = [ST].[StaffCode]
LEFT OUTER JOIN [Theatre].[Specialty]                   AS SPEC
ON [SPEC].[SpecialtyCode]       = [SESS].[SpecialtyCode]

WHERE [SESS].[SessionDate] BETWEEN  @FromDate AND @ToDate
AND [TH].[OperatingSuite] = @Unit
AND PB.[PatientBookingStatusCode] <> 2 -- no cancelled patients
AND (
      [PB].[SessionSourceUniqueID] = @SessionCode
      OR
      @SessionCode IS NULL
     )










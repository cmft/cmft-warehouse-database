﻿

create proc [RPT].[PortalComorbidity]

(
@User varchar(50)
)

as

select 
	ClinicalCodingCompleteDate
	,Diagnosis.Diagnosis
	,Cases = 1		
from
	APC.Encounter 
	
		unpivot
			(
			Code for [Sequence] in 
			(
			SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			)
			) Encounter
										
inner join WH.Diagnosis Diagnosis
on Encounter.Code = Diagnosis.DiagnosisCode

inner join PAS.Consultant consultant
on	encounter.ConsultantCode = consultant.ConsultantCode
												
--where
--	consultant.DomainLogin = @consultant
--	and encounter.EpisodeStartDate between @start and @end
--	and Diagnosis.IsCharlsonFlag = 1



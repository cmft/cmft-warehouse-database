﻿



CREATE proc [RPT].[GetPortalMortality]

(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
)

as

set dateformat dmy

set @consultant = --'CMMC\neil.parrott'
					case
						when @consultant in
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\Jane.Eddleston'
						/* for CSS */
						when @consultant in
											(
											'CMMC\John.Butler'
											,'CMMC\Rachael.Challiner'
											,'CMMC\Bernard.Foex'
											,'CMMC\Ruari.Greer'
											,'CMMC\Steve.Jones'
											,'CMMC\Michael.Parker'
											,'CMMC\Srikanth.Amudalapalli'
											,'CMMC\roger.slater'
											,'CMMC\steve.benington'
											,'CMMC\daniel.conway'
											,'CMMC\dougal.atkinson'
											,'CMMC\maheshhan.nirmalan'
											,'CMMC\ magnus.garrioch'
											,'CMMC\John.Moore'
											,'CMMC\hilary.gough' 
											)
						then 'CMMC\Jane.Eddleston'
						else @consultant
					end

select
	Division
	,division.DivisionCode
	,Directorate
	,NationalSpecialty
	,consultant.Consultant
	,encounter.SpecialtyCode
	,encounter.SourcePatientNo
	,CasenoteNumber
	,DistrictNo
	,DateOfDeath
	,PatientAge = floor(datediff(day, DateOfBirth, DateOfDeath)/ 365.25)
	,PatientName = PatientSurname + ', ' + PatientForename
	,AdmissionTime
	,encounter.EpisodeStartTime
	,ReviewPriority = 
					case
						when DischargeMethodCode IN ('ST', 'SP')
						then 'Stillbirth'
						when floor(datediff(day, DateOfBirth, DateOfDeath)/ 365.25) < 18
						then 'Under age of 18'					
					end
	,MortalityReviewFormID
	,MortalityReviewStatus = 
			case
				when MortalityReviewFormID is not null
				then 'Complete'
				else 'Outstanding'
			end
	,Cases = 1
from				
	WarehouseReporting.APC.Encounter encounter
	
	inner join WarehouseReporting.PAS.Consultant consultant
	on encounter.ConsultantCode = consultant.ConsultantCode
		
	inner join WarehouseReporting.WH.Directorate
	on Directorate.DirectorateCode = encounter.EndDirectorateCode

	inner join WarehouseReporting.WH.Division
	on Division.DivisionCode = Directorate.DivisionCode

	inner join WarehouseReporting.PAS.Specialty specialty
	on Specialty.SpecialtyCode = encounter.SpecialtyCode
	
	left outer join Information.Information.MortalityReviewForms mortalityreview
	on encounter.SourcePatientNo = mortalityreview.SourcePatientNo
	and encounter.EpisodeStartTime = mortalityreview.EpisodeStartTime
	
	
where
	encounter.DischargeMethodCode in ('DN', 'DP','ST', 'SP')
	and encounter.LastEpisodeInSpellIndicator = 'Y'
	and encounter.CodingCompleteDate is not null
	and consultant.DomainLogin = @consultant
	and encounter.DateOfDeath between @start and @end
	and encounter.DateOfDeath >= '21 may 2012'
	and encounter.PrimaryDiagnosisCode not like 'O04%'
	and encounter.PrimaryOperationCode not like 'Q11%'
	and encounter.PrimaryOperationCode not like 'Q12%'


option (recompile)



﻿

CREATE procedure 		
		[RPT].[GetDirectoryServicesUser]

as

select distinct
		DomainLabel = case	when LastName + ' ' + FirstName is null 
							then substring(DomainLogin,charindex('\',DomainLogin)+1,100)
							else LastName + ', ' + FirstName
							end
		,DomainLogin

from
		CMFTDataServices.DirectoryServices.AD_Group_Members
		
union

select
		'(All Users)'
		,null      


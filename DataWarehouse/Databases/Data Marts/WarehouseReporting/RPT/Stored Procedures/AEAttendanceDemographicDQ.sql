﻿

CREATE proc [RPT].[AEAttendanceDemographicDQ]

@NHSNumberStatus bit = null
,@RegisteredGP bit = null

as

select
	PatientName = Surname + ', ' + Forenames
	,DistrictNo
	,NHSNumber
	,NHSNumberStatus = NHSNumberStatusId
	,DateOfBirth
	,RegisteredGp = coalesce(RegisteredGpCode, '') + ' - ' + coalesce(Gp, '')
	,RegisteredGpNationalCode = NationalCode
	,RegisteredGpPracticeCode = PracticeCode
	,Patient.Updated
from
	PAS.Patient

left join PAS.Gp
on	RegisteredGpCode = Gp.GpCode
where
	AEAttendanceToday = 1
and (
		(
			case 
			when NHSNumberStatusId <> 'NP' 
			then 1
			else 0
			end = @NHSNumberStatus
		or	@NHSNumberStatus is null
		)
	and	(
			case 
			when coalesce(RegisteredGpCode, '111') in ('111', 'NOGP') 
			then 1
			else 0
			end = @RegisteredGP
		or	@RegisteredGP is null
		)
	)	


--NB	Trace Postponed (Baby Under Six Weeks Old)
--NC	Trace In Progress
--NN	Number Not Present And Trace Not Required
--NP	Number Present And Verified
--RD	Trace Needs To Be Resolved - (Patient Detail Conflict)
--RM	Trace Attempted - Multiple Match Found
--RN	Trace Attempted - No Match Found
--RP	Number Present But Not Traced
--RR	Trace Needs To Be Resolved - (Nhs Number Conflict)
--RT	Trace Required

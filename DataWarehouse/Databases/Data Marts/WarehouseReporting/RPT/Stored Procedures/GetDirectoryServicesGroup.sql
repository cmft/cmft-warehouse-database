﻿
create procedure 
		[RPT].[GetDirectoryServicesGroup]

as

select distinct
      DistributionLabel = substring(DistributionGroup,10,100)
      ,DistributionGroup

from
      CMFTDataServices.DirectoryServices.AD_Group_Members
      
union

select
		'(All Groups)'
		,null   
      

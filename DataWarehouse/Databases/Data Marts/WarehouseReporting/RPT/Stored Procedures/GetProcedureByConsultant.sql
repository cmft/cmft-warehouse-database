﻿
create proc RPT.GetProcedureByConsultant


(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
)

as

set dateformat dmy

set @consultant = 
					case
						when @consultant in 
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\neil.parrott'
						else @consultant
					end

select
	count(Code)
	,count(distinct Code)
	from 
	(
		select
			PrimaryOperationCode
			,SecondaryOperationCode1
			,SecondaryOperationCode2
			,SecondaryOperationCode3
			,SecondaryOperationCode4
			,SecondaryOperationCode5    
			,SecondaryOperationCode6
			,SecondaryOperationCode7
			,SecondaryOperationCode8
			,SecondaryOperationCode9
			,SecondaryOperationCode10
			,SecondaryOperationCode11
		from 
			APC.Encounter
			
		inner join Warehouse.dbo.EntityXref xref
		on xref.EntityCode = encounter.ConsultantCode
		and xref.EntityTypeCode = 'CONSULTANTPASCODE'
		
		where
			EpisodeEndDate between @start and @end
			and ClinicalCodingCompleteDate is not null
			and 
			(
			xref.XrefEntityTypeCode = 'CONSULTANTWINDOWSLOGIN'
			and xref.XrefEntityCode = @consultant
			)
	) apc
		
	unpivot
	(
	Code for Sequence in 
	(
	PrimaryOperationCode
	,SecondaryOperationCode1
	,SecondaryOperationCode2
	,SecondaryOperationCode3
	,SecondaryOperationCode4
	,SecondaryOperationCode5    
	,SecondaryOperationCode6
	,SecondaryOperationCode7
	,SecondaryOperationCode8
	,SecondaryOperationCode9
	,SecondaryOperationCode10
	,SecondaryOperationCode11
	)
	
	) operation
	
	inner join WH.Operation operationlookup
	on Code = OperationCode
	where Code <> '##'

option (recompile)	
	

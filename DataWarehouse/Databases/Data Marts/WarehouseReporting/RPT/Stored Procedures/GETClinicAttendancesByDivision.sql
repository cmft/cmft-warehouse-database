﻿



CREATE PROCEDURE RPT.GETClinicAttendancesByDivision 
 @DivisionCode      varchar(10)
,@FromDate          date
,@ToDate            date

AS 

--DECLARE @DivisionCode	AS	varchar(10)= 'CHILD'
--DECLARE @FromDate   AS DATE = '27-OCT-2012'
--DECLARE @ToDate     AS DATE = '28-OCT-2012'


SELECT 
     Encounter.SiteCode
    ,Directorate.DivisionCode    
    ,Directorate.Directorate
    ,Encounter.ConsultantCode
    ,Consultant.Consultant
	,Encounter.ClinicCode
	,Clinic.Clinic
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime
	,Encounter.EncounterRecno
	,Encounter.NHSNumber
	,Encounter.CasenoteNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname

	,DoB = 
		CONVERT(VARCHAR(10) , Encounter.DateOfBirth , 103)   

	,Encounter.Postcode
	,Encounter.DNACode
	,Encounter.AttendanceOutcomeCode
     
	,AttDesc = 
		CASE Encounter.AttendanceOutcomeCode
		WHEN 'ATT' THEN 'Attended'
		WHEN 'H'   THEN 'Hospital Cancellation'
		WHEN 'P'   THEN 'Patient Cancellation'
		WHEN 'DNA' THEN 'Did Not Attend'
		WHEN 'NR'  THEN 'Not Recorded'
		ELSE Encounter.AttendanceOutcomeCode
		END
     
	,AttendYes = 
		CASE Encounter.DNACode
		WHEN 5 THEN 1
		ELSE 0
		END   
      
	,AttendNo = 
		CASE Encounter.DNACode
		WHEN 5 THEN 0
		ELSE 1
		END 
      
	,Encounter.AppointmentTypeCode

FROM 
	OP.Encounter Encounter

LEFT JOIN PAS.Clinic
ON	Encounter.ClinicCode = Clinic.ClinicCode

LEFT OUTER JOIN PAS.Consultant
ON	Encounter.ConsultantCode = Consultant.ConsultantCode

LEFT OUTER JOIN WarehouseReporting.WH.Directorate
ON	Encounter.DirectorateCode = Directorate.DirectorateCode 
  
WHERE 
	Encounter.AppointmentDate BETWEEN @FromDate AND @ToDate
AND Directorate.DivisionCode = @DivisionCode
  





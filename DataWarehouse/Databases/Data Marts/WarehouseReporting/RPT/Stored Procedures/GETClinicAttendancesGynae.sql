﻿





CREATE PROCEDURE [RPT].[GETClinicAttendancesGynae] 
	 @GroupClinicCode   varchar(50)
	,@FromDate          Date
	,@ToDate            Date

AS 

--DECLARE @GroupClinicCode	AS	varchar(50)= 'Excluded Clinics'
--DECLARE @FromDate           AS DATE        = '04-JUN-2015'
--DECLARE @ToDate             AS DATE        = '04-JUN-2015'


SELECT DISTINCT
     @GroupClinicCode                                       AS ClinicGroup
    ,GynaeClinicCodes.[LDF]                                 AS GynaeClinicCodesLDF
    ,ClinicExclusion.[LDF]                                  AS ClinicExclusionLDF
    ,Clinic.[SpecialtyCode]
    ,Clinic.[ClinicCode]
    ,[Diary].[SessionCode]
    ,[Diary].[DoctorCode]
	,Clinic.[Clinic]
    ,DATENAME(dw, Diary.[SessionDate])                      AS DayOfClinic 
    ,CONVERT(VARCHAR(10), Diary.[SessionStartTime], 103)    AS ClinicStartDate
    ,CONVERT(VARCHAR(5), Diary.[SessionStartTime], 108)     AS ClinicStartTime   
    ,Diary.[SessionStartTime]                               AS SessStartTime
    ,Diary.[SessionEndTime]                                 AS SessEndTime
    ,Encounter.AppointmentTime
	,Diary.[SessionPeriod]                                  AS AMPM
    ,Diary.[Units]
    ,Diary.[UsedUnits]
    ,Diary.[FreeUnits]
    ,Diary.[Units] - (Diary.[UsedUnits]+Diary.[FreeUnits])  AS CancelledUnits
	,CONVERT(VARCHAR(10), Encounter.[AppointmentDate], 103) AS AppointmentDate
	,CONVERT(VARCHAR(5) , Encounter.[AppointmentTime], 108) AS AppTime
	,Encounter.[EncounterRecno]
	,Encounter.[NHSNumber]
	,Encounter.[CasenoteNo]
	,Encounter.[PatientTitle]
	,Encounter.[PatientForename]
	,Encounter.[PatientSurname]
	,CONVERT(VARCHAR(10) , Encounter.[DateOfBirth] , 103)   AS DoB
	,Encounter.[Postcode]
	,Encounter.[DNACode]
	,Encounter.[AppointmentStatusCode]                      AS PatientOutcomeCode
	--,Encounter.[CancelledByCode]
	,PatientOutcomeDescription = 
		CASE 
		WHEN Encounter.AppointmentStatusCode = 'ATT'  THEN 'Attended'
		WHEN Encounter.AppointmentStatusCode = 'ATTP' THEN 'Attended'
		WHEN Encounter.AppointmentStatusCode = 'H'    THEN 'Hospital Cancellation'
		WHEN Encounter.AppointmentStatusCode = 'P'    THEN 'Patient Cancellation'
		WHEN Encounter.AppointmentStatusCode = 'CND'  THEN 'CND'
		WHEN Encounter.AppointmentStatusCode = 'NATT' THEN 'Did Not Attend'
		WHEN Encounter.AppointmentStatusCode = 'DNA'  THEN 'Did Not Attend'
		WHEN Encounter.AppointmentStatusCode IS NULL  THEN 'Empty Clinic'
		WHEN Encounter.AppointmentStatusCode = 'NR'   THEN
		   CASE Encounter.[CancelledByCode]
		   WHEN 'P' THEN 'Patient Cancellation' 
		   WHEN 'H' THEN 'Hospital Cancellation'
		   ELSE 'Not Recorded'
		   END
		ELSE 'Not Recorded'       
		END
	,AttendYes = 
		CASE Encounter.DNACode
		WHEN 5 THEN 1
		ELSE 0
		END   
	,AttendNo = 
		CASE 
		WHEN  Encounter.DNACode = 5 OR Encounter.DNACode IS NULL THEN 0
		ELSE 1
		END 
	,Encounter.[AppointmentTypeCode]
	,Cancelled = 
	    CASE 
	    WHEN  Diary.[ReasonForCancellation] IS NULL THEN 'No'
	    ELSE 'Yes'
	    END
	,Diary.[ReasonForCancellation]                           AS ClinicCancellationCode

FROM WarehouseReporting.[dbo].[GynaeClinicCodes]

INNER JOIN warehousesql.[WarehouseReporting].[PAS].[Clinic]
   ON GynaeClinicCodes.[ClinicCode] = 
     CASE WHEN GynaeClinicCodes.[LDF] = 'N' THEN Clinic.[SpecialtyCode]
          WHEN GynaeClinicCodes.[LDF] = 'Z' THEN Clinic.[ClinicCode]
     END     
  AND (
        GynaeClinicCodes.[ClinicGroup] = @GroupClinicCode
    OR
       GynaeClinicCodes.[LDF] = 
        CASE
          WHEN @GroupClinicCode = 'All Groups'      THEN 'N'
          WHEN @GroupClinicCode = 'Excluded Clinics' THEN 'Z'
        END  
       )


INNER JOIN warehousesql.[Warehouse].[OP].[Diary]
   ON Clinic.[ClinicCode] = Diary.[ClinicCode]
  AND Diary.[SessionDate] BETWEEN @FromDate AND @ToDate    
  
LEFT OUTER JOIN warehousesql.[WarehouseReportingMerged].[OP].[Encounter]
  ON Diary.[ClinicCode] = Encounter.[ClinicCode]
 AND Diary.[DoctorCode] = Encounter.[DoctorCode]
 AND Encounter.[AppointmentTime] BETWEEN Diary.[SessionStartTime] AND Diary.[SessionEndTime]

LEFT OUTER JOIN WarehouseReporting.[dbo].[GynaeClinicCodes]   AS ClinicExclusion
  ON Clinic.[ClinicCode] = ClinicExclusion.[ClinicCode]
  AND ClinicExclusion.[LDF] <>  'Y' 

WHERE (
        (GynaeClinicCodes.[LDF] = 'N' AND ClinicExclusion.[LDF] IS NULL)
       OR 
        (GynaeClinicCodes.[LDF] = 'Z' AND ClinicExclusion.[LDF] = 'Z')
      )

--ORDER BY Clinic.[ClinicCode],Diary.[SessionCode],Diary.[SessionStartTime],Encounter.[AppointmentTime]












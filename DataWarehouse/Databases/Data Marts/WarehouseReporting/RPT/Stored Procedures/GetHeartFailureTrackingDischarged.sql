﻿
CREATE proc [RPT].[GetHeartFailureTrackingDischarged]

@DateRange varchar(20),
@AdultChild varchar(1) = 'B'

as

--this query shows you all patients that were diagnosed with HeartFailure (FROM beginning of time!)
--we then grab the records that were discharged in the past 12 months
--finally we only show the last spell in the report

--DECLARE Variables
DECLARE @DischargeEndDate smalldatetime

--SET Variables
SET @DischargeEndDate = CASE @DateRange
	WHEN 'Today' THEN  convert(DATE,getdate())
	WHEN 'Previous 3 days' THEN DATEADD(DAY, -3, convert(DATE,getdate()))
	WHEN 'Previous 7 days' THEN DATEADD(DAY, -7, convert(DATE,getdate()))
	WHEN 'Previous 28 days' THEN DATEADD(DAY, -28, convert(DATE,getdate()))
	WHEN 'Previous 12 months' THEN DATEADD(MONTH, -12, convert(DATE,getdate()))
END

--3. now grab all other relevant details
SELECT 
	NHSNumber
	,DistrictNo
	,InternalNo = Encounter.SourcePatientNo
	,SpellNo = Encounter.SourceSpellNo
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,ConsultantCode
	,SpecialtyCode
	,AdmissionDate
	,DischargeDate
	,DateOfDeath
	,AgeAtAdmission = dbo.f_GetAge(DateOfBirth, AdmissionDate)
	,Encounter.SiteCode

FROM APC.Encounter Encounter

INNER JOIN  (
				--3. now only bring back last encounter (episode)
				SELECT
				Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
				,SourceEncounterNo = max(Encounter.SourceEncounterNo)

				FROM apc.Encounter Encounter

				INNER JOIN (
								--2. now only bring back the last spell
								SELECT 
									SourcePatientNo
									,SourceSpellNo = max(SourceSpellNo)
		
									FROM (
											SELECT
												myqry.SourcePatientNo
												,myqry.SourceSpellNo
												,DischargeDate
				
												FROM
													(
													--1. query PrimaryDiagnosis (check all records for HeartFailure)
													SELECT 
														Encounter.SourcePatientNo
														,Encounter.SourceSpellNo
														,Encounter.DischargeDate

													FROM 
														WarehouseReporting.APC.Encounter Encounter								 
					
													WHERE 
														Encounter.PrimaryDiagnosisCode in ('I11.0','I13.0','I13.2','I50.0','I50.9','I50.1')
								
													UNION

													--1. query all other Diagnosis  (check all records for HeartFailure)
													SELECT 
														Encounter.SourcePatientNo
														,Encounter.SourceSpellNo
														,Encounter.DischargeDate

													FROM 
														WarehouseReporting.APC.Encounter Encounter 
					
													INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
													ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
													AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
													AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
													AND Diagnosis.DiagnosisCode in ('I11.0','I13.0','I13.2','I50.0','I50.9','I50.1')
													)myqry
												WHERE
													--now only bring back records that have been discharged in past 12 months
													DischargeDate>@DischargeEndDate	
										)mainqry
										--WHERE SourcePatientNo = '3278204'
									GROUP BY
										SourcePatientNo
						)MaxSpell

				ON Encounter.SourcePatientNo = MaxSpell.SourcePatientNo
				AND Encounter.SourceSpellNo = MaxSpell.SourceSpellNo

				GROUP BY
				Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
			)q
ON Encounter.SourcePatientNo = q.SourcePatientNo
AND Encounter.SourceSpellNo = q.SourceSpellNo 
AND Encounter.SourceEncounterNo = q.SourceEncounterNo 
AND dbo.f_GetAge(DateOfBirth, AdmissionDate)
BETWEEN (CASE @AdultChild WHEN 'A' THEN 18 
						  WHEN 'C' THEN 0
						  WHEN 'B' THEN 0 END)
AND (CASE @AdultChild WHEN 'A' THEN 999 
						  WHEN 'C' THEN 17 
						  WHEN 'B' THEN 999 END)


ORDER BY 
	nhsnumber




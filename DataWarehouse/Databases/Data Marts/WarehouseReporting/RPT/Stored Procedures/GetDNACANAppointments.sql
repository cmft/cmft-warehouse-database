﻿
CREATE PROCEDURE [RPT].[GetDNACANAppointments]
(

	
	-- @DateRange varchar(MAX)= 'YTD'
	-- ,
	@StartAppointmentDate Date 
	 ,@EndAppointmentDate Date
	 ,@Division varchar(MAX) = NULL
	 ,@Specialty varchar(MAX)= NULL
	
	
	)
		
AS

--set	@StartAppointmentDate = case when @DateRange = 'YTD' and datepart(m,getdate()) in ('1','2','3') then
--			'01 APR'& datepart(year,getdate())-1
--			else
--			'01 APR'& datepart(year,getdate())
--			end
--set @EndAppointmentDate	=		
			
--			convert(char(10),(getdate()-datepart(dw,getDate())+2),103)
			

--		--	convert(char(10),dateadd(month,-13,dateadd(dd,datepart(d,(getdate()))*-1+1,getDATE())),103)
--		--else
--		--	convert(char(10),dateadd(dd,datepart(dw,(dateadd(year,-1,getdate())))*-1-5,dateadd(year,-1,getDATE())),103)
--		--end	

--set 
--	@EndAppointmentDate = case when @ReportBy = 'Month' then
--			convert(char(10),dateadd(dd,datepart(d,(getdate()))*-1+1,getDATE()),103)
--		else
--			convert(char(10),dateadd(dd,datepart(dw,(getdate()))*-1+2,getDATE()),103)
--		end

SELECT  
   	   Division = Dir.Division   
   	  ,NationalReferringSpecialtyCode = RefSpec.NationalSpecialtyCode
   	  ,NationalSpecialty = NationalSpecialty
    --  ,PASReferringSpecialty = RefSpec.Specialty
   	  ,AttendanceType =
						case
							when OPAct.FirstAttendanceFlag = 1 then 'New'
							else 'Follow Up'
						end
      ,FinancialYear = FinancialYear
      ,FinancialMonthKey = FinancialMonthKey
      ,TheMonth = TheMonth
      
				
      ,ActType = Case 
					when OPAct.AttendanceOutcomeCode = 'DNA' then 'DNA' 
					when OPAct.AttendanceOutcomeCode = 'CND' then 'Patient Cancelled on Day'
					when OPAct.DNACode = 4 then 'Hospital Cancellation'
					when OPAct.DNACode = 2 and OPAct.CancelledByCode = 'P' then 'Patient Cancellation'
					when OPAct.DNACode in (5,6) then 'Attendances'
				else 'Unknown Outcome'
					
				end
      , Activity = COUNT(SourceUniqueID)
      , AttPlusDNA = SUM(	Case
								when OPAct.AttendanceOutcomeCode = 'DNA' then 1
								when OPAct.DNACode in (5,6) then 1
							else 0
							end)
	, NewAttDNA = SUM( case
								when ( OPAct.DNACode in (3,5,6) and OPAct.FirstAttendanceFlag = 1)
							then 1
							else 0
						end)
						
	, FolAttDNA = SUM( case
								when (OPAct.DNACode in (3,5,6) and OPAct.FirstAttendanceFlag <> 1)
							then 1
							else 0
						end)
									
FROM 
	WarehouseReporting.OP.Encounter AS OPAct
INNER JOIN WarehouseReporting.PAS.Specialty AS RefSpec 
	ON OPAct.ReferringSpecialtyCode = RefSpec.SpecialtyCode
INNER JOIN WarehouseReporting.PAS.Directorate as Dir 
	on OPAct.DirectorateCode = Dir.DirectorateCode
INNER JOIN WarehouseReporting.WH.Calendar as Cal 
	on OPAct.AppointmentDate  = Cal.TheDate
WHERE	
	AppointmentDate >= @StartAppointmentDate 
	and AppointmentDate <= @EndAppointmentDate
	and 
		(
				Division IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
			or 
				@Division IS NULL
		)
	and 
		(
				RefSpec.NationalSpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Specialty,','))
			or 
				@Specialty IS NULL
		)
	and DNACode in (2,3,4,5,6)
	and ReferringSpecialtyTypeCode = 'I'
	and AdminCategoryCode <> '02'
	and IsWardAttender = 0
Group By
	 Dir.Division   
	,RefSpec.NationalSpecialtyCode
	,NationalSpecialty
--	,RefSpec.Specialty
	,case
		when FirstAttendanceFlag = 1 then 'New'
		else 'Follow Up'
	end
	
	,Case 
		when AttendanceOutcomeCode = 'DNA' then 'DNA' 
		when AttendanceOutcomeCode = 'CND' then 'Patient Cancelled on Day'
		when DNACode = 4 then 'Hospital Cancellation'
		when DNACode = 2 and CancelledByCode = 'P' then 'Patient Cancellation'
		when DNACode in (5,6) then 'Attendances'
	else 'Unknown Outcome'
				
	end
	,CONVERT(char(8),Cal.FirstDayOfWeek,3)
	,FinancialYear
	,FinancialMonthKey
	,TheMonth
	
     
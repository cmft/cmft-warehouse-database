﻿-- =============================================
-- Author:		Gareth Summerfield
-- Create date: 03/08/2011
-- =============================================
CREATE PROCEDURE [RPT].[GetEddNotRecorded]
	 @startdate AS Date
	,@enddate AS Date
	,@Division varchar(max)
AS

BEGIN
	SET NOCOUNT ON;

SELECT  
	ExpectedLOS.SourceUniqueID, 
	ExpectedLOS.LoadTime, 
	ExpectedLOS.LoadModifiedTime, 
	ExpectedLOS.SourceSystem, 
	CONVERT(date, ExpectedLOS.AdmissionTime, 103) AS AdmissionDate, 
	ExpectedLOS.DischargeTime, 
	ExpectedLOS.AdmissionTime,
	ExpectedLOS.SourcePatientNo, 
	ExpectedLOS.SourceSpellNo,
	Ward = CASE 
			WHEN Ward.Ward =  WardCode THEN	Ward.Ward
			ELSE Ward.WardCode + ' - ' + ward.Ward
			END,
	ExpectedLOS.Consultant, 
	ExpectedLOS.CasenoteNumber,
	ExpectedLOS.ExpectedLOS, 
	EnteredWithin48hrs = CASE 
							WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) < 1440 THEN 1
							ELSE 0
						 END, 
	ExpectedLOS.CreateEddDaysDuration, 
	ExpectedLOS.SiteCode, 
	CASE WHEN ExpectedLOS IS NULL 
		THEN 'Not Recorded' 
		ELSE 'Recorded' 
	END AS HasEDD, 
	ISNULL(Division.Division, 'Not Known') AS Division, 
	WHSpecialty.Specialty AS Specialty, 
	1 AS Admissions, 
	ManagementIntention =  CASE 
								WHEN ExpectedLOS.ManagementIntentionCode = 'D' THEN 'DayCase'
								WHEN ExpectedLOS.ManagementIntentionCode = 'I' THEN 'Inpatient'
								WHEN ExpectedLOS.ManagementIntentionCode = 'B' THEN 'Born in Hospital or on way'
								WHEN ExpectedLOS.ManagementIntentionCode = 'V' THEN 'Intveral Admission'
								WHEN ExpectedLOS.ManagementIntentionCode = 'N' THEN 'Regular Night Admission'
								WHEN ExpectedLOS.ManagementIntentionCode IS NULL THEN 
										CASE 
											WHEN WardBase.TimeAvailable = '7/0' THEN 'DayCase (Derived from Ward)'
											ELSE 'Unavailable'
										END
								ELSE ManagementIntentionCode 
							END
FROM
	Warehouse.APC.ExpectedLOS ExpectedLOS
LEFT OUTER JOIN Warehouse.PAS.Ward Ward
	ON ExpectedLOS.Ward = Ward.WardCode
LEFT OUTER JOIN warehouse.PAS.WardBase WardBase
	ON ExpectedLOS.Ward = WardBase.WARDID
LEFT OUTER JOIN Warehouse.WH.Directorate Directorate
	ON ExpectedLOS.DirectorateCode = Directorate.DirectorateCode 
LEFT OUTER JOIN Warehouse.WH.Division Division
	ON Directorate.DivisionCode = Division.DivisionCode
LEFT OUTER JOIN Warehouse.PAS.Specialty
	ON ExpectedLOS.Specialty =  Specialty.SpecialtyCode 
LEFT OUTER JOIN Warehouse.WH.TreatmentFunction WHSpecialty
	ON	WHSpecialty.SpecialtyCode = Specialty.NationalSpecialtyCode
	
WHERE 
	ISNULL(Division.Division, 'Not Known') IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))
	AND CONVERT(date, ExpectedLOS.AdmissionTime, 103) >= @startdate
	AND CONVERT(date, ExpectedLOS.AdmissionTime, 103) <= @enddate
	AND NOT (ExpectedLOS.Ward = 'ESTU' AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6)
	AND ISNULL(ManagementIntentionCode,'NA')NOT IN ('R','N')
	AND ArchiveFlag <> 'D'
	AND Specialty.NationalSpecialtyCode <> '501'
	AND ExpectedLOS.Ward <> '76A'
	AND Specialty.Specialty NOT like '%DIALYSIS%'
	AND Specialty.Specialtycode NOT like 'IH%'

END
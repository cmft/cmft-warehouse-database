﻿







create PROCEDURE [RPT].[GETClinicAttendancesPatientLevel] 
   @ClinicCode	     varchar(10)
  ,@SessionCode      varchar(10) 
  ,@SessionStartTime DATETIME
  ,@SessionEndTime   DATETIME

AS 

--DECLARE @ClinicCode       AS varchar(10)= 'UDSFRI'
--DECLARE @SessionCode      AS varchar(10)= 'UDSFRI' 
--DECLARE @SessionStartTime AS DATETIME = '06-FEB-2015 08:00:00'
--DECLARE @SessionEndTime   AS DATETIME = '06-FEB-2015 12:30:00'

SELECT DISTINCT
	 Encounter.ClinicCode
 	,Diary.[ClinicCode]              AS Clinic
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime
	,Encounter.EncounterRecno
	,Encounter.NHSNumber
	,Encounter.CasenoteNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname

	,DoB = 
		CONVERT(VARCHAR(10) , Encounter.DateOfBirth , 103)   

	,Encounter.Postcode
	,Encounter.DNACode
	,Encounter.AttendanceOutcomeCode

  ,AttDesc = 
		CASE Encounter.AppointmentStatusCode
		WHEN 'ATT'  THEN 'Attended'
		WHEN 'ATTP' THEN 'Attended By Telephone'
		WHEN 'H'    THEN 'Hospital Cancellation'
		WHEN 'P'    THEN 'Patient Cancellation'
		WHEN 'CND'  THEN 'CND'
		WHEN 'NATT' THEN 'Attended Late Not Seen'
		WHEN 'DNA'  THEN 'Did Not Attend'
		WHEN 'NR'  THEN
		   CASE Encounter.[CancelledByCode]
		   WHEN 'P' THEN 'Patient Cancellation' 
		   WHEN 'H' THEN 'Hospital Cancellation'
		   ELSE 'Not Recorded'
		   END
		ELSE ISNULL(Encounter.AppointmentStatusCode,'Not Recorded')        
		END  
		 
	,AttendYes = 
		CASE Encounter.DNACode
		WHEN 5 THEN 1
		ELSE 0
		END   
      
	,AttendNo = 
		CASE Encounter.DNACode
		WHEN 5 THEN 0
		ELSE 1
		END 
      
	,Encounter.AppointmentTypeCode

FROM [WarehouseReporting].[PAS].[Clinic]

INNER JOIN [Warehouse].[OP].[Diary]
   ON Clinic.[ClinicCode] = Diary.[ClinicCode]
  
LEFT OUTER JOIN [WarehouseReportingMerged].[OP].[Encounter]
  ON Diary.[ClinicCode] = Encounter.[ClinicCode]
 AND Diary.[DoctorCode] = Encounter.[DoctorCode]
  

 WHERE Clinic.[ClinicCode]      = @ClinicCode 
   AND Diary.[SessionCode]      = @SessionCode
   AND Diary.[SessionStartTime] = @SessionStartTime
   AND Encounter.[AppointmentTime] BETWEEN @SessionStartTime AND @SessionEndTime







﻿

CREATE proc [RPT].[GetACHDTrackingAdmitted]

as


--this query shows you all patients (that have been discharged in the past 12 months and were diagnosed with Congenital Heart Defect) 
--that have been admitted in the past 7 days

--DECLARE Variables
DECLARE @EndDate smalldatetime
DECLARE @Patients TABLE 
(SourcePatientNo varchar(20))
DECLARE @Dates TABLE 
(DateField DATE)

--SET Variables
SET @EndDate = DATEADD(DAY, -7, convert(DATE,getdate()))


--1.
--need a subquery to bring back patients that have been diagnosed with Congenital Heart Defect

insert into @Patients(SourcePatientNo)

	SELECT 
		Encounter.SourcePatientNo
	FROM 
		--primarydiagnosis code matches
		[WarehouseReporting].APC.Encounter Encounter

	WHERE 
		Encounter.PrimaryDiagnosisCode in ('Q20.0','Q20.1','Q20.2','Q20.3','Q20.4','Q20.5','Q20.6','Q20.8','Q20.9','Q21.0','Q21.1','Q21.2',
					'Q21.3','Q21.4','Q21.8','Q21.9','Q22.0','Q22.1','Q22.2','Q22.3','Q22.4','Q22.5','Q22.6','Q22.8',
					'Q22.9','Q23.0','Q23.1','Q23.2','Q23.3','Q23.4','Q23.8','Q23.9','Q24.0','Q24.1','Q24.2','Q24.3',
					'Q24.4','Q24.5','Q24.6','Q24.8','Q24.9','Q25.0','Q25.1','Q25.2','Q25.3','Q25.4','Q25.5','Q25.6',
					'Q25.7','Q25.8','Q25.9','Q26.0','Q26.1','Q26.2','Q26.3','Q26.4','Q26.5','Q26.6','Q26.8','Q26.9',
					'Q79.6','Q87.4','Q89.3','Q90.0','Q90.1','Q90.2','Q90.9','Q91.0','Q91.3','Q96.9')
	
	UNION
	
	SELECT 
		Encounter.SourcePatientNo
	FROM 
		--query all other Diagnosis (use apc.diagnosis)
		[WarehouseReporting].APC.Encounter Encounter
			
	INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
	ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
	AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
	AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
	AND Diagnosis.DiagnosisCode in ('Q20.0','Q20.1','Q20.2','Q20.3','Q20.4','Q20.5','Q20.6','Q20.8','Q20.9','Q21.0','Q21.1','Q21.2',
						'Q21.3','Q21.4','Q21.8','Q21.9','Q22.0','Q22.1','Q22.2','Q22.3','Q22.4','Q22.5','Q22.6','Q22.8',
						'Q22.9','Q23.0','Q23.1','Q23.2','Q23.3','Q23.4','Q23.8','Q23.9','Q24.0','Q24.1','Q24.2','Q24.3',
						'Q24.4','Q24.5','Q24.6','Q24.8','Q24.9','Q25.0','Q25.1','Q25.2','Q25.3','Q25.4','Q25.5','Q25.6',
						'Q25.7','Q25.8','Q25.9','Q26.0','Q26.1','Q26.2','Q26.3','Q26.4','Q26.5','Q26.6','Q26.8','Q26.9',
						'Q79.6','Q87.4','Q89.3','Q90.0','Q90.1','Q90.2','Q90.9','Q91.0','Q91.3','Q96.9')
--2. insert 7days
insert into @Dates(DateField)
SELECT convert(DATE,getdate())
UNION
SELECT DATEADD(DAY, -1, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -2, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -3, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -4, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -5, convert(DATE,getdate()))
UNION
SELECT DATEADD(DAY, -6, convert(DATE,getdate()))



--2. Get data for report

SELECT
	AdmissionDate = DateField
	,InternalNo = Admissions.SourcePatientNo
	,Admissions.DistrictNo
	,Admissions.CasenoteNumber
	,Admissions.PatientForename
	,Admissions.PatientSurname
	,Admissions.DateOfBirth
	,Admissions.ConsultantCode
	,Admissions.SpecialtyCode
	,Admissions.DischargeDate
	,AdmitWard = Admissions.StartWardTypeCode
	,LOSAdmit = Admissions.LOS
FROM
	@Dates d
LEFT JOIN (
			SELECT 
				Encounter.SourcePatientNo
				,Encounter.DistrictNo
				,Encounter.CasenoteNumber
				,Encounter.PatientForename
				,Encounter.PatientSurname
				,Encounter.DateOfBirth
				,Encounter.ConsultantCode
				,Encounter.SpecialtyCode
				,Encounter.AdmissionDate
				,Encounter.DischargeDate
				,Encounter.StartWardTypeCode
				,Encounter.LOS

			FROM 
				[WarehouseReporting].APC.Encounter Encounter 

			WHERE 
				AdmissionDate>@EndDate
			and Encounter.SourcePatientNo in (SELECT SourcePatientNo FROM @Patients)
		)Admissions
ON Admissions.AdmissionDate = d.DateField

ORDER BY
	d.DateField DESC







﻿  
CREATE PROCEDURE [RPT].[GetDQPatientLevelError]

@from date,
@to date,
@PointOfDelivery varchar(max),
@Division varchar(max),
@ResponsibleTeam varchar(max),
@Error varchar(max),
@InterfaceGroupCode varchar(max)

AS

SELECT
	 PrimaryIdentifier = PrimaryIdentifier
	,PrimaryIdentifierLabel = PrimaryIdentifierLabel
	,SecondaryIdentifier = SecondaryIdentifier
	,SecondaryIdentifierLabel = SecondaryIdentifierLabel
	,PrimaryDate = PrimaryDate
	,PrimaryDateLabel = PrimaryDateLabel
	,SecondaryDate = SecondaryDate
	,SecondaryDateLabel = SecondaryDateLabel
	,NHSNumber = ISNULL(NHSNumber,'Not Recorded')
	,PointOfDelivery = PointOfDelivery.PointOfDelivery
	,Specialty = TreatmentFunction.Specialty
	,[Site] = SiteCode
	,Error.InterfaceCode 
	,Division = Division.Division
	,ResponsibleTeam = ResponsibleTeam
	,[Platform] = Interface.Interface
	,Error = [Type].Name
	,Value = Error.Value
FROM
	DQ.Error Error
INNER JOIN DQ.[Type]
	ON Error.TypeCode = [Type].Code
INNER JOIN DQ.PointofDelivery
	ON Error.PointOfDelivery = PointOfDelivery.PointofDeliveryCode
INNER JOIN WH.Directorate
	ON convert(varchar(max),Error.DirectorateCode) = Directorate.DirectorateCode
INNER JOIN WH.Division
	ON Directorate.DivisionCode = Division.DivisionCode
INNER JOIN WH.Interface
	ON Error.InterfaceCode = Interface.InterfaceCode
INNER JOIN WH.TreatmentFunction
	ON Error.SpecialtyCode = TreatmentFunction.SpecialtyCode
INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@ResponsibleTeam,',')) R
	ON [Type].ResponsibleTeam = R.Value
INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@Division,',')) D
    ON Division.Division = D.Value
INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@PointOfDelivery,',')) POD
	ON error.PointOfDelivery = POD.Value
INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@Error,',')) E
    ON [Type].Code = E.Value
INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@InterfaceGroupCode,',')) IGC
	ON Interface.InterfaceGroupCode = IGC.Value
WHERE  [Date] >= @from
AND [Date] <= @to
AND PrimaryIdentifier IS NOT NULL;


	
﻿


CREATE proc [RPT].[GetPortalVTE]


(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
,@breach int = null
)

as

/* Copy of dbo.OlapCalendar to reduce contention and blocking */

declare @OlapCalendar table 
(
	TheDate date not null primary key
	,TheMonth nvarchar(34) null
);

insert into @OlapCalendar
(
	TheDate
	,TheMonth
)

select 
	TheDate
	,TheMonth
from 
	WarehouseOLAP.dbo.OlapCalendar with (nolock);

--set @end = isnull(@end, warehouse.dbo.f_Last_of_Last_Month(getdate()))
--set @start = isnull(@start,dateadd(month, -13, @end))


set @consultant = --'CMMC\neil.parrott'
					case
						when @consultant in
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\neil.parrott'
						/* for CSS */
						when @consultant in
											(
											'CMMC\John.Butler'
											,'CMMC\Rachael.Challiner'
											,'CMMC\Bernard.Foex'
											,'CMMC\Ruari.Greer'
											,'CMMC\Steve.Jones'
											,'CMMC\Michael.Parker'
											,'CMMC\Srikanth.Amudalapalli'
											,'CMMC\roger.slater'
											,'CMMC\steve.benington'
											,'CMMC\daniel.conway'
											,'CMMC\dougal.atkinson'
											,'CMMC\maheshhan.nirmalan'
											,'CMMC\ magnus.garrioch'
											,'CMMC\John.Moore'
											,'CMMC\hilary.gough' 
											)
						then 'CMMC\Jane.Eddleston'
						else @consultant
					end

select
	--[DirectorateCode]
	[Directorate]
	,division.[DivisionCode]
	,[Division]
	,Specialty.NationalSpecialty
	--,ConsultantCode = encounter.ConsultantCode
	,Consultant = Consultant.Consultant
	,CasenoteNumber
	,DistrictNo
	,PatientSurname
	,PatientForename
	,AdmissionTime
	,DischargeTime
	,VTEBreachStatus = 	case 
							when VTESpell.VTECategoryCode = 'I' then 'Incomplete'
							when VTESpell.VTECategoryCode = 'M' then 'Missing'
						end
	,VTEMonth = cast(TheMonth as date)
	
	
	,Cases = 1
	
	,IsCodedExclusion =
	
		case
		when
				Encounter.CodingCompleteDate is not null
			and VTESpell.VTEExclusionReasonCode is not null 
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
			then 1
		else 0
		end
		

	,IsUncodedExclusion =
	
		case
		when
				Encounter.CodingCompleteDate IS NULL
			and VTESpell.VTEExclusionReasonCode is not null
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
			then 1
		else 0
		end
	
	,IsVTEComplete = 
	
		case VTESpell.VTECategoryCode
			when 'C' 
			then 1
			else 0
		end
	
	,IsVTEIncomplete = 
	
		case VTESpell.VTECategoryCode
			when 'I' 
			then 1
			else 0
		end
	
	,IsVTEMissing = 
	
		case VTESpell.VTECategoryCode
			when 'M' 
			then 1
			else 0
		end
		
	,IsCodingComplete = 
	
		case
			when Encounter.CodingCompleteDate is not null 
			then 1
			else 0
		end
		
	,IsCodingIncomplete = 
	
		case
			when Encounter.CodingCompleteDate IS NULL 
			then 1
			else 0
		end
	
	--,Breach = 
	--case
	--	when 
	--		(
	--		case
	--			when
	--					Encounter.CodingCompleteDate is not null
	--				and VTESpell.VTEExclusionReasonCode is not null 
	--				and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
	--				then 1
	--				else 0
	--		end = 0
	--	and
	--		case
	--			when
	--					Encounter.CodingCompleteDate IS NULL
	--				and VTESpell.VTEExclusionReasonCode is not null
	--				and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
	--				then 1
	--				else 0
	--		end = 0
	--		)
	--	and
	--		(
	--		case VTESpell.VTECategoryCode
	--			when 'I' 
	--			then 1
	--			else 0
	--		end = 1
	--	or
	--		case VTESpell.VTECategoryCode
	--			when 'M' 
	--			then 1
	--			else 0
	--		end = 1
	--		)
	--then 1
	--end
	
from
	WarehouseReporting.APC.Encounter

inner join @OlapCalendar calendar
on	Calendar.TheDate = Encounter.AdmissionDate

inner join WarehouseReporting.PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join WarehouseReporting.APC.VTESpell
on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

left join WarehouseReporting.WH.Directorate
on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

left join WarehouseReporting.WH.Division
on	Division.DivisionCode = Directorate.DivisionCode

left join WarehouseReporting.PAS.Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join WarehouseReporting.PAS.Ward
on	Ward.WardCode = Encounter.StartWardTypeCode



where
	Encounter.FirstEpisodeInSpellIndicator = 1
and Encounter.AdmissionDate between @start and @end	
and	Encounter.PatientCategoryCode != 'RD'
and	(
		(
			Encounter.AgeCode like '%Years'
		and	left(Encounter.AgeCode , 2) > '17'
		)
	or	Encounter.AgeCode = '99+'
	)
	
and consultant.DomainLogin = @consultant

and 
	(
	@breach =
				case
					when 
						(
						case
							when
									Encounter.CodingCompleteDate is not null
								and VTESpell.VTEExclusionReasonCode is not null 
								and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
								then 1
								else 0
						end = 0
					and
						case
							when
									Encounter.CodingCompleteDate IS NULL
								and VTESpell.VTEExclusionReasonCode is not null
								and coalesce(VTESpell.VTECategoryCode , '') != 'C' 
								then 1
								else 0
						end = 0
						)
					and
						(
						case VTESpell.VTECategoryCode
							when 'I' 
							then 1
							else 0
						end = 1
					or
						case VTESpell.VTECategoryCode
							when 'M' 
							then 1
							else 0
						end = 1
						)
				then 1
			end
			
	or @breach is null
	)
	
	and encounter.PrimaryDiagnosisCode not like 'O04%'
	and encounter.PrimaryOperationCode not like 'Q11%'
	and encounter.PrimaryOperationCode not like 'Q12%'
	
option (recompile)




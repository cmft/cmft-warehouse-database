﻿
create Procedure [RPT].[CreatinineAlertNational] 

(
	 @ReportType varchar(10) 
	,@NationalAlert varchar(100) 
	,@Summary bit
	
	,@Stage varchar (100) 
	
	,@FromDate date
	,@ToDate date
	,@CurrentInpatient tinyint
)	


as

		
select
	ResultRecno
	,MergeEpisodeNo
	,Surname
	,Forenames
	,DistrictNo
	,DeathIndicator
	,DateOfDeath
	,C1Result
	,ResultTime
	,RVRatio
	,NationalAlert
	,RatioDescription
	,AgeCode
	,Ethnicity
	,Sex 
	,LocalRatio
	,LocalStage
	,LocalExclusion
	,LatestAdmissionDate
	,LatestPatientCategory 
	,LatestWardCode 
	,Specialty
	,Consultant
from
	(select 
		ResultRecno
		,MergeEpisodeNo
		,Surname
		,Forenames
		,DistrictNo
		,DeathIndicator
		,DateOfDeath
		,C1Result
		,ResultTime
		,RVRatio
		,NationalAlert
		,RatioDescription
		,AgeCode = Age.Age
		,Ethnicity = EthnicOrigin.NationalEthnicOrigin
		,Sex = SexCode
		,LocalRatio
		,LocalStage
		,LocalExclusion
		,LatestAdmissionDate
		,LatestPatientCategory 
		,LatestWardCode 
		,Specialty.Specialty
		,Consultant.Consultant
		
		,ReportSequenceNo =
				case
				when @ReportType = 'CHANGE' then  row_number () over (partition by NationalResult.DistrictNo,NationalResult.MergeEpisodeNo order by NationalResult.RVRatio desc)
				when @ReportType = 'LATEST' then  row_number () over (partition by NationalResult.DistrictNo order by NationalResult.ResultTime desc)
				when @ReportType = 'VALUE' then  row_number () over (partition by NationalResult.DistrictNo,NationalResult.MergeEpisodeNo order by NationalResult.C1Result desc)
				end
		
	from 
		OCM.NationalResult

	--left join WarehouseReporting.OCM.CodedResult Result
	--on OCM.NationalResult.ResultRecno = Result.ResultRecno
		
	left join WarehouseReporting.WH.Age
	on	Age.AgeCode = NationalResult.AgeCode collate Latin1_General_CI_AS

	left join WarehouseReporting.PAS.Consultant
	on	Consultant.ConsultantCode = NationalResult.ConsultantCode

	left join WarehouseReporting.PAS.Specialty
	on	Specialty.SpecialtyCode = NationalResult.SpecialtyCode

	left join WarehouseReporting.PAS.EthnicOrigin
	ON	EthnicOrigin.EthnicOriginCode = NationalResult.EthnicOriginCode

	) Result
where 
	ResultTime between @FromDate and @ToDate
and Result.NationalAlert in (Select Value from RPT.SSRS_MultiValueParamSplit(@NationalAlert,','))
and Result.LocalStage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
and (
		(@ReportType = 'Change' 
		and	Result.ReportSequenceNo = @Summary)
		or
		(@ReportType = 'Latest' 
		and	Result.ReportSequenceNo = @Summary)
		or
		(@ReportType = 'Value' 
		and	Result.ReportSequenceNo = @Summary)
	)
		
		

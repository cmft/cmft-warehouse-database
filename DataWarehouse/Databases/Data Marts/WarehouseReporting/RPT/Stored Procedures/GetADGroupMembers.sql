﻿
create proc RPT.GetADGroupMembers

as

select 
	--Name = LastName + ', ' + FirstName
	Name = DomainLogin
	,[Group] = substring(DistributionGroup, charindex('sec - sp', DistributionGroup) + 9, len(DistributionGroup))
	,*
from
	CMFTDataServices.DirectoryServices.AD_Group_Members
--where
--	substring(DistributionGroup, charindex('sec - sp', DistributionGroup) + 9, len(DistributionGroup)) not in 
--																												(
--																												'Anaesthetics Consultants'
--																												,'Anaesthetics Trainees'
--																												,'THT Informatics'
--																												)
	

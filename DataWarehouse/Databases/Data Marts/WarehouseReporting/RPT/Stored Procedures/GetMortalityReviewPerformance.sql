﻿
CREATE proc [RPT].[GetMortalityReviewPerformance]

(

	@start datetime
	,@end datetime
	,@division varchar(50) = null
	,@directorate varchar(50) = null
	,@specialty varchar(50) = null
	,@consultant varchar(50) = null	
)

as

set @division = 
				coalesce(
				(
				select
					chodmap.DivisionCode
				from
				(
					select
						ConsultantCode = 'RRD'
						,DivisionCode = 'MEDSP'	
					union all
					select 
						ConsultantCode = 'AH'
						,DivisionCode = 'MEDSP'	
					union all	
					select
						ConsultantCode = 'JCGS'
						
						,DivisionCode = 'MEDAC'
					union all	
					select
						ConsultantCode = 'PLS'
						,DivisionCode = 'MEDAC'	
					union all
					select
						ConsultantCode = 'JME'
						,DivisionCode = 'CLSCN'
					union all
					select
						ConsultantCode = 'JB'
						,DivisionCode = 'CHILD'
					union all
					select
						ConsultantCode = 'MPB'
						,DivisionCode = 'CHILD'	
					union all
					select
						ConsultantCode = 'JHILL'
						,DivisionCode = 'SURG'
					union all
					select
						ConsultantCode = 'NRP'
						,DivisionCode = 'SURG'	
					union all
					select
						ConsultantCode = 'MPE'
						,DivisionCode = 'REH'	
					union all
					select
						ConsultantCode = 'KBM'
						,DivisionCode = 'REH'
					union all
					select
						ConsultantCode = 'DDO'
						,DivisionCode = 'SMH'
				) chodmap
				
			inner join Warehouse.PAS.Consultant consultant
			on consultant.ConsultantCode = chodmap.ConsultantCode
			
			where consultant.DomainLogin = @consultant
			)
			,@division
			)
	

select
	division.DivisionCode
	,division.Division
	,directorate.DirectorateCode
	,directorate.Directorate
	,specialty.SpecialtyCode
	,specialty.Specialty
	,specialty.NationalSpecialty
	,consultant.ConsultantCode
	,Consultant = 
				consultant.Consultant
				+ 
				case
					when consultant.DomainLogin is null
					then ' (UNMAPPED)'
					else ''
				end
	,AdmissionTime		
	,encounter.EpisodeStartTime
	,encounter.SourcePatientNo		
	,CasenoteNumber
	,DistrictNo
	,DateOfDeath
	,PatientAge = floor(datediff(day, DateOfBirth, DateOfDeath)/ 365.25)
	,PatientName = PatientSurname + ', ' + PatientForename
	,ReviewPriority = 
					case
						when DischargeMethodCode IN ('ST', 'SP')
						then 'Stillbirth'
						when floor(datediff(day, DateOfBirth, DateOfDeath)/ 365.25) < 18
						then 'Under age of 18'
						else ''				
					end
	,ReviewOutstanding = 
			case
				when MortalityReviewFormID is null
				then 1
				else 0
			end
	,ReviewComplete = 
			case
				when MortalityReviewFormID is not null
				then 1
				else 0
			end
	,MortalityReviewFormID
	,Cases = 1
from				
	WarehouseReporting.APC.Encounter encounter
	
	inner join WarehouseReporting.PAS.Consultant consultant
	on encounter.ConsultantCode = consultant.ConsultantCode
		
	left join WarehouseReporting.WH.Directorate
	on Directorate.DirectorateCode = encounter.EndDirectorateCode

	left join WarehouseReporting.WH.Division
	on Division.DivisionCode = Directorate.DivisionCode

	left join WarehouseReporting.PAS.Specialty specialty
	on Specialty.SpecialtyCode = encounter.SpecialtyCode
	
	left outer join Information.Information.MortalityReviewForms mortalityreview
	on encounter.SourcePatientNo = mortalityreview.SourcePatientNo
	and encounter.EpisodeStartTime = mortalityreview.EpisodeStartTime
	
	
where
	encounter.DischargeMethodCode in ('DN', 'DP','ST', 'SP')
	and encounter.LastEpisodeInSpellIndicator = 'Y'
	--and encounter.CodingCompleteDate is not null
	and encounter.DateOfDeath between @start and @end
	and encounter.DateOfDeath >= '21 may 2012'
	and (division.DivisionCode = @division or @division is null) 
	and (directorate.DirectorateCode = @directorate or @directorate is null)													
	and (encounter.SpecialtyCode = @specialty or @specialty is null)
	and (encounter.ConsultantCode = @consultant or @consultant is null)


﻿

CREATE proc 
		[RPT].[GetIncidentsWeeks]

		(
		@FinancialYear varchar(4)
		)

as
			

		--declare @FinancialYear varchar(9)

		--set @FinancialYear  = '2013'
		
						
select 
		FinancialYearKey = left(FinancialMonthKey,4)
		,WeekNoKey
		,TheWeek = convert(varchar(10),cast(FirstDayOfWeek as date))
from	
		WarehouseReporting.WH.Calendar
where	
		left(FinancialMonthKey,4) = @FinancialYear
and
		TheDate < getdate()
group by
		left(FinancialMonthKey,4)
		,FinancialYear
		,WeekNoKey
		,cast(FirstDayOfWeek as date)
		
union

select 
		FinancialYearKey = @FinancialYear
		,WeekNoKey = null
		,TheWeek = 'Total'

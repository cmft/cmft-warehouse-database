﻿

CREATE Procedure [RPT].[MaternityCTGAssessmentsMedianCalculation] 

(@LocalStartDate date
,@LocalEndDate date
)

As

Truncate Table CTGAssTime
Insert into CTGAssTime 
select 
	SessionNumber
	,FetusNumber
	,CTGStartDate
	,TimeBetweenAssessments
	,[MonitoringType]
	,Sequence = row_number ( ) over (partition by [MonitoringType] order by TimeBetweenAssessments,SessionNumber asc)
from
	(Select
		SessionNumber
		,FetusNumber
		,CTGStartDate
		,TimeBetweenAssessments
		,[MonitoringType] = '60mins'
	from
		Warehouse.Maternity.CTGAssessment
	where
		CheckWithinAnHour in ('Yes','No')
	and FetusNumber in ('0','1')
	and CTGStartDate between @LocalStartDate and @LocalEndDate

	union
	
	Select
		SessionNumber
		,FetusNumber
		,CTGStartDate
		,TimeBetweenAssessments
		,[MonitoringType] = '30mins'
	from 
		Warehouse.Maternity.CTGAssessment
	where
		CheckWithin30Mins in ('Yes','No')
	and FetusNumber in ('0','1')
	and CTGStartDate between @LocalStartDate and @LocalEndDate
	) A



	

﻿------------------------------------------------------------------
-- Results

--Use WarehouseReporting


create Procedure [RPT].[GetMaternityCTGAssessmentsForAudit]  --'20130401', '20140331'


(@StartDate	date
,@EndDate	date
)

As


declare
@LocalStartDate	date
,@LocalEndDate	date

set @LocalStartDate = @StartDate
set @LocalEndDate = @EndDate


select 
	Main.SessionNumber
	,Main.FetusNumber
	,Main.CTGStartDate
	,Main.CTGStartTime	
	,StaffGroup
	,StaffInitials
	,Labours			=	sum(LabourCount)
	,CTGAssessments		=	count(*)
	,CTGAss60minDenom	=	Coalesce(HrAss.Denom1,0) 
	,CTGAss60min		=	Coalesce(HrAss.Num1,0 )
	,CTGAss30min		=	Coalesce([30minsAss].[Num2],0)
	,CTGAss30minDenom	=	Coalesce([30minsAss].[Denom2], 0 )
	,BuddySig10mins		=	Coalesce(BudSig.[Buddy], 0 )
	,BuddySigConsDr		=	Coalesce(BudSigCons.[BuddyCons], 0)
	,BuddySigMidwife	=	Coalesce(BudSigMidwife.[BuddyMidwife], 0 )
	,BuddySigUnableToIdentify =	Coalesce(BudSigNoGroup.[BuddyNoGroup] , 0 )
from 
	Warehouse.Maternity.CTGAssessment Main

left join		

-- Table for 60 minute Assessment Monitoring, identifying those applicable and those carried out
	(
	select 
		SessionNumber	=	[60minsDenom].SessionNumber
		,FetusNumber	=	[60minsDenom].FetusNumber
		,[Denom1]		=	count(*)
		,[Num1]			=	coalesce(Num1,0)
	from 
		Warehouse.Maternity.CTGAssessment [60minsDenom]

	left join
		(select 
			SessionNumber
			,FetusNumber
			,Num1 = count(*)
		from 
			Warehouse.Maternity.CTGAssessment 
		where 
			CheckWithinAnHour = 'Yes'	
		group by
			SessionNumber
			,FetusNumber
		) 
	[60minsNum]

	on [60minsNum].SessionNumber = [60minsDenom].SessionNumber
	and [60minsNum].FetusNumber = [60minsDenom].FetusNumber

	where 
		CheckWithinAnHour in ('Yes','No')
	group by 
		[60minsDenom].SessionNumber
		,[60minsDenom].FetusNumber
		,Num1
	) 
	HrAss
	
on Main.SessionNumber = HrAss.SessionNumber 
and Main.FetusNumber = HrAss.FetusNumber



left join	
-- Table for 30 minute Assessment Monitoring, identifying those applicable and those carried out
	(
	select 
		[30minsDenom].SessionNumber
		,[30minsDenom].FetusNumber
		,[Denom2] = count(*)
		,Num2 = Coalesce(Num2,0)
	from 
		Warehouse.Maternity.CTGAssessment [30minsDenom]

	left join
		(select 
			SessionNumber
			,FetusNumber
			,Num2 = count(*)
		from 
			Warehouse.Maternity.CTGAssessment [30minsNum]
		where 
			CheckWithin30mins = 'Yes'
		group by
			SessionNumber
			,FetusNumber
		) 
	[30minsNum]

	on [30minsDenom].SessionNumber = [30minsNum].SessionNumber 
	and [30minsDenom].FetusNumber = [30minsNum].FetusNumber	

	where 
		CheckWithin30mins in ('Yes','No')
	group by 
		[30minsDenom].SessionNumber
		,[30minsDenom].FetusNumber
		,Coalesce(Num2,0)
	) 
	[30minsAss]

on Main.SessionNumber = [30minsAss].SessionNumber 
and Main.FetusNumber = [30minsAss].FetusNumber

-- Table for Buddy Signature Recording,

left join 
	(
	select 
		SessionNumber
		,FetusNumber
		,BuddySigWithin10mins
		,Buddy = count(*)
	from 
		Warehouse.Maternity.CTGAssessment
	where 
		BuddySigWithin10mins = 'Y'
	group by 
		SessionNumber
		,FetusNumber
		,BuddySigWithin10mins
	) 
	BudSig 

on Main.SessionNumber = BudSig.SessionNumber 
and Main.FetusNumber = BudSig.FetusNumber

left join 
	(
	select 
		SessionNumber
		,FetusNumber
		,BuddyCons = count(*)
	from 
		Warehouse.Maternity.CTGAssessment
	where 
		BuddySigWithin10mins = 'Y'
	and BuddyStaffGroup in ('Doctor','Consultant')
	group by 
		SessionNumber
		,FetusNumber
	) 
	BudSigCons 

on Main.SessionNumber = BudSigCons.SessionNumber 
and Main.FetusNumber = BudSigCons.FetusNumber

left join 
	(
	select 
		SessionNumber
		,FetusNumber
		,BuddyMidwife = count(*)
	from 
		Warehouse.Maternity.CTGAssessment
	where 
		BuddySigWithin10mins = 'Y'
	and BuddyStaffGroup in ('Midwife','StudentMidwife')
	group by 
		SessionNumber
		,FetusNumber
	) 
	BudSigMidwife 

on Main.SessionNumber = BudSigMidwife.SessionNumber 
and Main.FetusNumber = BudSigMidwife.FetusNumber

left join 
	(
	select 
		SessionNumber
		,FetusNumber
		,BuddyNoGroup = count(*)
	from 
		Warehouse.Maternity.CTGAssessment
	where 
		BuddySigWithin10mins = 'Y'
	and BuddyStaffGroup in ('Unable to Group')
	group by 
		SessionNumber
		,FetusNumber
	) 
	BudSigNoGroup 

on Main.SessionNumber = BudSigNoGroup.SessionNumber 
and Main.FetusNumber = BudSigNoGroup.FetusNumber

where 
	Main.FetusNumber in ('0','1') 
and Main.CTGStartDate between @LocalStartDate and @LocalEndDate

group by
	Main.SessionNumber
	,Main.FetusNumber
	,Main.CTGStartDate
	,Main.CTGStartTime
	,StaffGroup
	,StaffInitials
	,Coalesce(HrAss.Denom1,0) 
	,Coalesce(HrAss.Num1,0 )
	,Coalesce([30minsAss].[Num2],0)
	,Coalesce([30minsAss].[Denom2], 0 )
	,Coalesce(BudSig.[Buddy], 0 )
	,Coalesce(BudSigCons.[BuddyCons], 0)
	,Coalesce(BudSigMidwife.[BuddyMidwife], 0 )
	,Coalesce(BudSigNoGroup.[BuddyNoGroup] , 0 )




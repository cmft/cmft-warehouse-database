﻿



CREATE proc [RPT].[GetPortalDischargeSummary]
(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
,@breach int = null
)

as

set @consultant = --'CMMC\neil.parrott'
					case
						when @consultant in
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\neil.parrott'
						/* for CSS */
						when @consultant in
											(
											'CMMC\John.Butler'
											,'CMMC\Rachael.Challiner'
											,'CMMC\Bernard.Foex'
											,'CMMC\Ruari.Greer'
											,'CMMC\Steve.Jones'
											,'CMMC\Michael.Parker'
											,'CMMC\Srikanth.Amudalapalli'
											,'CMMC\roger.slater'
											,'CMMC\steve.benington'
											,'CMMC\daniel.conway'
											,'CMMC\dougal.atkinson'
											,'CMMC\maheshhan.nirmalan'
											,'CMMC\ magnus.garrioch'
											,'CMMC\John.Moore'
											,'CMMC\hilary.gough' 
											)
						then 'CMMC\Jane.Eddleston'
						else @consultant
					end

set dateformat dmy

/* Copy of dbo.OlapCalendar to reduce contention and blocking */

declare @OlapCalendar table 
(
	TheDate date not null primary key
	,TheMonth nvarchar(34) null
);

insert into @OlapCalendar
(
	TheDate
	,TheMonth
)

select 
	TheDate
	,TheMonth
from 
	WarehouseOLAP.dbo.OlapCalendar with (nolock);


select
	directorate.DirectorateCode
	,directorate.Directorate
	,division.DivisionCode
	,division.Division
	,specialty.NationalSpecialty
	,dischargesummary.ConsultantCode
	,consultant.Consultant
	,dischargesummary.CasenoteNumber
	,dischargesummary.AdmissionTime
	,dischargesummary.DischargeTime
	,dischargesummary.SignedTime
	,DocumentType = 
		coalesce(doctype.DocumentType, 'N/A')
	,Status = 
		coalesce(docstatus.[Status], 'N/A')
	,DocumentProgress = 
		coalesce(docprogress.DocumentProgress, 'N/A')
	,BreachStatus = 
					case
						when dischargesummary.StatusCode  not in ('09') 
							and coalesce(dischargesummary.DocumentProgressCode, '') not in ('N','X')
							and RequestOrderForDischarge = 1
							and DocumentSentGPForDischarge = 0
						then 'No Letter to GP'
						when datediff(minute, DischargeTime, SignedTime) / 60 >= 24
							and dischargesummary.StatusCode  not in ('09') 
							and dischargesummary.DocumentProgressCode in ('G','P')
							and IsFirstDocumentForDischarge = 1
						then 'Letter not in time'
						when datediff(minute, DischargeTime, SignedTime) / 60 < 24
							and dischargesummary.StatusCode  not in ('09') 
							and dischargesummary.DocumentProgressCode in ('G','P')
							and IsFirstDocumentForDischarge = 1
						then 'Letter in time'
						else 'Letter not required'
						end
	,DischargeMonth = cast(calendar.TheMonth as date)
	,LetterRequired =
			case
			when
					dischargesummary.StatusCode not in ('09') 
				and coalesce(dischargesummary.DocumentProgressCode, '') not in ('N','X') 
				and RequestOrderForDischarge = 1 
				then Cases
			else 0
			end
		

	,LetterProduced =
			case
			when
					dischargesummary.StatusCode not in ('09') 
				and IsFirstDocumentForDischarge = 1
				and dischargesummary.DocumentProgressCode in ('G','P')
				then Cases
			else 0
			end


	,[Within24Hours] = 
			case
			when 
					datediff(minute, DischargeTime, SignedTime) / 60 < 24
				and dischargesummary.StatusCode not in ('09')
				and dischargesummary.DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
				then Cases
			else 0
			end
		
		
		,Breach = 
			case
			when 
				(
				datediff(minute, DischargeTime, SignedTime) / 60 >= 24
				and dischargesummary.StatusCode not in ('09') 
				and dischargesummary.DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
				)
				or
				(
				dischargesummary.StatusCode not in ('09') 
				and coalesce(dischargesummary.DocumentProgressCode, '') not in ('N','X')
				and RequestOrderForDischarge = 1
				and DocumentSentGPForDischarge = 0
				)
				then Cases	
			else 0
			end
						
from
	APC.DischargeSummary dischargesummary
	
inner join WarehouseReporting.PAS.Consultant consultant
on consultant.ConsultantCode = dischargesummary.ConsultantCode
	
inner join Warehouse.WH.Directorate directorate
on directorate.DirectorateCode = dischargesummary.EndDirectorateCode

inner join Warehouse.WH.Division division
on directorate.DivisionCode = division.DivisionCode
	
inner join @OlapCalendar calendar
on dischargesummary.DischargeDate = calendar.TheDate
	
left outer join warehouse.Medisec.DocumentType doctype 
on dischargesummary.DocumentTypeCode = doctype.DocumentTypeCode
	
left outer join warehouse.Medisec.[Status] docstatus
on docstatus.StatusCode  = dischargesummary.StatusCode
	
left outer join warehouse.Medisec.DocumentProgress docprogress
on docprogress.DocumentProgressCode  = dischargesummary.DocumentProgressCode
	
inner join WarehouseReporting.PAS.Specialty specialty
on Specialty.SpecialtyCode = dischargesummary.SpecialtyCode
	


where 
	dischargesummary.DischargeDate between @start and @end
	and consultant.DomainLogin = @consultant	
	and (
		@breach =
				(
				case
					when 
						(
						datediff(minute, DischargeTime, SignedTime) / 60 >= 24
						and dischargesummary.StatusCode not in ('09') 
						and dischargesummary.DocumentProgressCode in ('G','P')
						and IsFirstDocumentForDischarge = 1
						)
						or
						(
						dischargesummary.StatusCode not in ('09') 
						and coalesce(dischargesummary.DocumentProgressCode, '') not in ('N','X')
						and RequestOrderForDischarge = 1
						and DocumentSentGPForDischarge = 0
						)
						then 1
					end
				)
		or @breach is null
		)
		
	and dischargesummary.PrimaryDiagnosisCode not like 'O04%'
	and dischargesummary.PrimaryOperationCode not like 'Q11%'
	and dischargesummary.PrimaryOperationCode not like 'Q12%'
		
option (recompile)





﻿
CREATE PROCEDURE [RPT].[GetCQUINPerformanceForMonth]
(
	 @FinancialMonthKey int
)
as

Declare @PreviousFinancialMonthKey int
Declare @PreviousFinancialDate date

select @PreviousFinancialDate = min(TheDate) from wh.Calendar where FinancialMonthKey = @FinancialMonthKey
select @PreviousFinancialDate = dateadd(m,-1,@PreviousFinancialDate)

select @PreviousFinancialMonthKey = FinancialMonthKey from wh.Calendar where TheDate = @PreviousFinancialDate




SELECT
	qry.Goal
	,qry.TheMonth
	,qry.Indicator
	,qry.Period
	,qry.[Target]
	,qry.Admissions
	,Actual = (isnull(qry.CodedExclusionCases*1.00,0)+isnull(qry.VTECompleteCases*1.00,0)+isnull(qry.UncodedExclusionCases*1.00,0))/qry.Admissions*1.00
	,PreviousActual = (isnull(prevmonth.CodedExclusionCases*1.00,0)+isnull(prevmonth.VTECompleteCases*1.00,0)+isnull(prevmonth.UncodedExclusionCases*1.00,0))/prevmonth.Admissions*1.00
FROM (

			SELECT
				TheMonth
				,Goal = 'N3.1'
				,Indicator = 'VTE Risk Assessment'
				,Period = 'Month'
				,[Target] = 0.9
				,Admissions = sum(Cases)
				,CodedExclusionCases = sum(CodedExclusionCases)
				,VTECompleteCases = sum(VTECompleteCases)
				,UncodedExclusionCases = sum(UncodedExclusionCases)

			FROM
				(

					SELECT
						Cases = 1
						,TheMonth
						,CodedExclusionCases =
							case
							when
									Encounter.CodingCompleteDate IS NOT NULL
								AND VTESpell.VTEExclusionReasonCode is not null 
								and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
							else 0
							end

						,VTECompleteCases = 
							case VTESpell.VTECategoryCode
							when 'C' then 1
							else 0
							end

						,UncodedExclusionCases =
							case
							when
									Encounter.CodingCompleteDate IS NULL
								AND VTESpell.VTEExclusionReasonCode is not null
								and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
							else 0
							end

						,VTEIncompleteCases = 
							case VTESpell.VTECategoryCode
							when 'I' then 1
							else 0
							end

						,VTEMissingCases = 
							case VTESpell.VTECategoryCode
							when 'M' then 1
							else 0
							end
		
						,CodingCompleteCases = 
							CASE
							WHEN Encounter.CodingCompleteDate IS NOT NULL THEN 1
							ELSE 0
							END
		
						,CodingIncompleteCases = 
							CASE
							WHEN Encounter.CodingCompleteDate IS NULL THEN 1
							ELSE 0
							END
					FROM
						WarehouseReporting.APC.Encounter

					inner join WarehouseReporting.WH.Calendar
					on	Calendar.TheDate = Encounter.AdmissionDate

					left join WarehouseReporting.APC.VTESpell
					on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

					left join WarehouseReporting.WH.Directorate
					on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

					left join WarehouseReporting.WH.Division
					on	Division.DivisionCode = Directorate.DivisionCode

					left join WarehouseReporting.PAS.Specialty
					on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

					left join WarehouseReporting.PAS.Ward
					on	Ward.WardCode = Encounter.StartWardTypeCode

					left join WarehouseReporting.PAS.Consultant
					on	Consultant.ConsultantCode = Encounter.ConsultantCode

					where
						Encounter.FirstEpisodeInSpellIndicator = 1
					and	Calendar.FinancialMonthKey = @FinancialMonthKey
					and	Encounter.PatientCategoryCode != 'RD'
					and	(
							(
								Encounter.AgeCode like '%Years'
							and	left(Encounter.AgeCode , 2) > '17'
							)
						or	Encounter.AgeCode = '99+'
						)
			)myqry

			GROUP BY
				TheMonth
)qry



left join (

	--now get the previous month
			SELECT
				TheMonth
				,Goal = 'N3.1'
				,Indicator = 'VTE Risk Assessment'
				,Period = 'Month'
				,[Target] = 0.9
				,Admissions = sum(Cases)
				,CodedExclusionCases = sum(CodedExclusionCases)
				,VTECompleteCases = sum(VTECompleteCases)
				,UncodedExclusionCases = sum(UncodedExclusionCases)

			FROM
				(

					SELECT
						Cases = 1
						,TheMonth
						,CodedExclusionCases =
							case
							when
									Encounter.CodingCompleteDate IS NOT NULL
								AND VTESpell.VTEExclusionReasonCode is not null 
								and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
							else 0
							end

						,VTECompleteCases = 
							case VTESpell.VTECategoryCode
							when 'C' then 1
							else 0
							end

						,UncodedExclusionCases =
							case
							when
									Encounter.CodingCompleteDate IS NULL
								AND VTESpell.VTEExclusionReasonCode is not null
								and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
							else 0
							end

						,VTEIncompleteCases = 
							case VTESpell.VTECategoryCode
							when 'I' then 1
							else 0
							end

						,VTEMissingCases = 
							case VTESpell.VTECategoryCode
							when 'M' then 1
							else 0
							end
		
						,CodingCompleteCases = 
							CASE
							WHEN Encounter.CodingCompleteDate IS NOT NULL THEN 1
							ELSE 0
							END
		
						,CodingIncompleteCases = 
							CASE
							WHEN Encounter.CodingCompleteDate IS NULL THEN 1
							ELSE 0
							END
					FROM
						WarehouseReporting.APC.Encounter

					inner join WarehouseReporting.WH.Calendar
					on	Calendar.TheDate = Encounter.AdmissionDate

					left join WarehouseReporting.APC.VTESpell
					on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

					left join WarehouseReporting.WH.Directorate
					on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

					left join WarehouseReporting.WH.Division
					on	Division.DivisionCode = Directorate.DivisionCode

					left join WarehouseReporting.PAS.Specialty
					on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

					left join WarehouseReporting.PAS.Ward
					on	Ward.WardCode = Encounter.StartWardTypeCode

					left join WarehouseReporting.PAS.Consultant
					on	Consultant.ConsultantCode = Encounter.ConsultantCode

					where
						Encounter.FirstEpisodeInSpellIndicator = 1
					and	Calendar.FinancialMonthKey = @PreviousFinancialMonthKey
					and	Encounter.PatientCategoryCode != 'RD'
					and	(
							(
								Encounter.AgeCode like '%Years'
							and	left(Encounter.AgeCode , 2) > '17'
							)
						or	Encounter.AgeCode = '99+'
						)
			)myqry

			GROUP BY
				TheMonth
		)prevmonth on prevmonth.Indicator = qry.Indicator




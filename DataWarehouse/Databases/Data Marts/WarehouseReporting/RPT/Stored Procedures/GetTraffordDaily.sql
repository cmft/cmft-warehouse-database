﻿


create proc [RPT].[GetTraffordDaily]

(
@start datetime = null
,@end datetime = null
)

as

SELECT	convert(char(11), [AdmissionTime], 113) as 'Date', 'Elec IP Admissions' as 'Type', 
		COUNT([providerspellno]) as 'Total'
  FROM [warehousesql].[TGHInformation].[dbo].[TraffordIPActivity] 
  --WHERE [AdmissionTime] >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
  --AND [AdmissionTime] < cast(convert(char(11), getdate(), 113) as datetime)
  WHERE [AdmissionTime] >= @start
  AND [AdmissionTime] < @end  
  AND [TYPE] = 'EL'
  GROUP BY convert(char(11), [AdmissionTime], 113)
  
  UNION
  
SELECT	convert(char(11), [DischargeTime], 113) as 'Date', 'Emerg Discharges', 
		COUNT([providerspellno])
  FROM [warehousesql].[TGHInformation].[dbo].[TraffordIPActivity]
  --WHERE [dischargeTime] >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
  --AND [DischargeTime] < cast(convert(char(11), getdate(), 113) as datetime)
  WHERE [dischargeTime] >= @start
  AND [dischargeTime] < @end    
  AND [nationaladmissionmethodcode] >= 20
  GROUP BY convert(char(11), [DischargeTime], 113)
  
  UNION
  
SELECT	convert(char(11), [AdmissionTime], 113) as 'Date', 'Emerg Other Adms',
		COUNT([providerspellno])
  FROM [warehousesql].[TGHInformation].[dbo].[TraffordIPActivity]
  --WHERE [AdmissionTime] >= DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0)
  --AND [AdmissionTime] < cast(convert(char(11), getdate(), 113) as datetime)
  WHERE [AdmissionTime] >= @start
  AND [AdmissionTime] < @end  
  AND [nationaladmissionmethodcode] in ('22','24','28','81')
  GROUP BY convert(char(11), [AdmissionTime], 113)




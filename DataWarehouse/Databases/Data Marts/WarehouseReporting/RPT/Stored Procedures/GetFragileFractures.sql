﻿

CREATE proc [RPT].[GetFragileFractures]

@FinancialMonthKey INT

as

Declare @FracturePatients TABLE
(
EncounterRecno int
,SourcePatientNo varchar(20)
,SourceSpellNo varchar(20)
)


--get list of FracturePatients and the Spell
insert into @FracturePatients(EncounterRecno,SourcePatientNo,SourceSpellNo)

SELECT
	EncounterRecno
	,SourcePatientNo
	,SourceSpellNo
FROM
	(SELECT 
		EncounterRecno,SourcePatientNo,SourceSpellNo
	FROM 
		WarehouseReporting.apc.Encounter Encounter

	INNER JOIN WarehouseReporting.WH.Calendar Calendar
	ON	Calendar.TheDate = Encounter.DischargeDate

	WHERE 
		(PrimaryDiagnosisCode like 'S62.8%' 
		or PrimaryDiagnosisCode like 'S72%' 
		or PrimaryDiagnosisCode like 'S42.2%'
		or PrimaryDiagnosisCode like 'S42.3%'
		or PrimaryDiagnosisCode like 'S42.4%'
		or PrimaryDiagnosisCode like 'S42.7%'
		or PrimaryDiagnosisCode like 'S42.8%'
		or PrimaryDiagnosisCode like 'S12%'
		or PrimaryDiagnosisCode like 'S32.0%'
		or PrimaryDiagnosisCode like 'S32.1%'
		or PrimaryDiagnosisCode like 'S32.2%'
		or PrimaryDiagnosisCode like 'S32.7%'
		or PrimaryDiagnosisCode like 'S32.8%'
		or PrimaryDiagnosisCode like 'S22.0%'
		or PrimaryDiagnosisCode like 'S22.1%'
		or PrimaryDiagnosisCode like 'T08%'
		or PrimaryDiagnosisCode like 'T02.1%'
		or PrimaryDiagnosisCode like 'S82%'
		or PrimaryDiagnosisCode like 'S52%')
		--get all patients who were 55 and over at the time of their admission
		AND (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionDate)))))-1900)>=55
		AND Calendar.FinancialMonthKey = @FinancialMonthKey	


	UNION	

	SELECT 
		EncounterRecno,Encounter.SourcePatientNo,Encounter.SourceSpellNo
	FROM
		WarehouseReporting.apc.Encounter Encounter

	INNER JOIN WarehouseReporting.WH.Calendar Calendar
	ON	Calendar.TheDate = Encounter.DischargeDate

	INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
	ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
	AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
	AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
	AND (Diagnosis.DiagnosisCode like 'S62.8%' 
		or Diagnosis.DiagnosisCode like 'S72%' 
		or Diagnosis.DiagnosisCode like 'S42.2%'
		or Diagnosis.DiagnosisCode like 'S42.3%'
		or Diagnosis.DiagnosisCode like 'S42.4%'
		or Diagnosis.DiagnosisCode like 'S42.7%'
		or Diagnosis.DiagnosisCode like 'S42.8%'
		or Diagnosis.DiagnosisCode like 'S12%'
		or Diagnosis.DiagnosisCode like 'S32.0%'
		or Diagnosis.DiagnosisCode like 'S32.1%'
		or Diagnosis.DiagnosisCode like 'S32.2%'
		or Diagnosis.DiagnosisCode like 'S32.7%'
		or Diagnosis.DiagnosisCode like 'S32.8%'
		or Diagnosis.DiagnosisCode like 'S22.0%'
		or Diagnosis.DiagnosisCode like 'S22.1%'
		or Diagnosis.DiagnosisCode like 'T08%'
		or Diagnosis.DiagnosisCode like 'T02.1%'
		or Diagnosis.DiagnosisCode like 'S82%'
		or Diagnosis.DiagnosisCode like 'S52%')
	--get all patients who were 55 and over at the time of their admission
	AND (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionDate)))))-1900)>=55
	AND Calendar.FinancialMonthKey = @FinancialMonthKey
	)s

--now bring back data for report

SELECT
	Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.AdmissionDate
	,Encounter.PatientCategoryCode
	,Encounter.AdmissionMethodCode
	,Encounter.AdmissionSourceCode
	,Encounter.StartWardTypeCode
	,AdmissionConsultant = Encounter.ConsultantCode
	,EpisodeWard = Encounter.StartWardTypeCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.DischargeDate
	,Encounter.DischargeMethodCode
	,Encounter.DischargeDestinationCode
	,Encounter.EndWardTypeCode
	,DischargeConsultant = Encounter.ConsultantCode
	,AE.ArrivalTime
	,AE.DepartureTime
	,Calendar.TheMonth

FROM
	APC.Encounter Encounter
	
INNER JOIN @FracturePatients FracturePatients
ON FracturePatients.SourcePatientNo = Encounter.SourcePatientNo
AND FracturePatients.SourceSpellNo = Encounter.SourceSpellNo
AND FracturePatients.EncounterRecno = Encounter.EncounterRecno

INNER JOIN WarehouseReporting.WH.Calendar Calendar
ON	Calendar.TheDate = Encounter.DischargeDate

--LOS in AE
LEFT JOIN (	
				SELECT distinct
					APCEncounter.DistrictNo,
					APCEncounter.AdmissionTime,
					AEEncounter.ArrivalTime,
					AEEncounter.DepartureTime
				FROM
					WarehouseReporting.APC.Encounter APCEncounter

				INNER JOIN WarehouseReporting.AE.Encounter AEEncounter
				ON AEEncounter.DistrictNo = APCEncounter.DistrictNo
				and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR,-2,APCEncounter.AdmissionTime) and DATEADD(HOUR,+2,APCEncounter.AdmissionTime)
			) AE 	
ON AE.DistrictNo = Encounter.DistrictNo
AND AE.AdmissionTime = Encounter.AdmissionTime


Order by SourcePatientNo asc




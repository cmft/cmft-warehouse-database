﻿

CREATE proc 
		[RPT].[GetDirectoryServicesUsers]
		
		(
		@Login varchar(max) 
		)

as

select 
		[Group] = substring(DistributionGroup,10,100)
		,Name = case	when LastName + ' ' + FirstName is null 
						then substring(DomainLogin,charindex('\',DomainLogin)+1,100)
						else LastName + ', ' + FirstName
						end
		,DomainLogin
		,EmailAddress

from
		CMFTDataServices.DirectoryServices.AD_Group_Members
		
where
		(
		AD_Group_Members.DomainLogin = @Login
		or 
		@Login is null 
		)

	


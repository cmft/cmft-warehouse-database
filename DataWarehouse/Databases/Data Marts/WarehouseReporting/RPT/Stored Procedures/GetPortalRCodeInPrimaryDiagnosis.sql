﻿


CREATE proc [RPT].[GetPortalRCodeInPrimaryDiagnosis]


(
@consultant varchar(50)
,@start datetime = null
,@end datetime = null
)

as

set @consultant = --'CMMC\neil.parrott'
					case
						when @consultant in
											(					
											select
												DomainLogin
											from
												CMFTDataServices.DirectoryServices.AD_Group_Members
											where
												DistributionGroup = 'sec - sp information'
											)
						then 'CMMC\neil.parrott'
						/* for CSS */
						when @consultant in
											(
											'CMMC\John.Butler'
											,'CMMC\Rachael.Challiner'
											,'CMMC\Bernard.Foex'
											,'CMMC\Ruari.Greer'
											,'CMMC\Steve.Jones'
											,'CMMC\Michael.Parker'
											,'CMMC\Srikanth.Amudalapalli'
											,'CMMC\roger.slater'
											,'CMMC\steve.benington'
											,'CMMC\daniel.conway'
											,'CMMC\dougal.atkinson'
											,'CMMC\maheshhan.nirmalan'
											,'CMMC\ magnus.garrioch'
											,'CMMC\John.Moore'
											,'CMMC\hilary.gough' 
											)
						then 'CMMC\Jane.Eddleston'
						else @consultant
					end

					
/* Copy of dbo.OlapCalendar to reduce contention and blocking */

declare @OlapCalendar table 
(
	TheDate date not null primary key
	,TheMonth nvarchar(34) null
);

insert into @OlapCalendar
(
	TheDate
	,TheMonth
)

select 
	TheDate
	,TheMonth
from 
	WarehouseOLAP.dbo.OlapCalendar with (nolock);
	
	
select
	SourceEncounterNo
	,ProviderSpellNo
	,EpisodeEndDate
	,EpisodeMonth = cast(calendar.TheMonth as date)
	,IsRCode = 
				cast(
					case
						when PrimaryDiagnosisCode like 'r%'
						then 1
						else 0
					end
				as int)
	,IsRCodeAndLoEUnder5 = 
				cast(
					case
						when PrimaryDiagnosisCode like 'r%'
						and case
								when datediff(day, EpisodeStartDate, EpisodeEndDate) >= 10
								then '10+'
								when datediff(day, EpisodeStartDate, EpisodeEndDate) between 5 and 9
								then '5-9'
								when datediff(day, EpisodeStartDate, EpisodeEndDate) between 0 and 4
								then '0-4'	
							end = '0-4'	
						then 1
						else 0
					end
				as int)
	,IsRCodeAndLoEBetween5And9 = 
				cast(
					case
						when PrimaryDiagnosisCode like 'r%'
						and case
								when datediff(day, EpisodeStartDate, EpisodeEndDate) >= 10
								then '10+'
								when datediff(day, EpisodeStartDate, EpisodeEndDate) between 5 and 9
								then '5-9'
								when datediff(day, EpisodeStartDate, EpisodeEndDate) between 0 and 4
								then '0-4'	
							end = '5-9'
						then 1
						else 0
					end
				as int)
	,IsRCodeAndLoE10AndOver = 
				cast(
					case
						when PrimaryDiagnosisCode like 'r%'
						and case
								when datediff(day, EpisodeStartDate, EpisodeEndDate) >= 10
								then '10+'
								when datediff(day, EpisodeStartDate, EpisodeEndDate) between 5 and 9
								then '5-9'
								when datediff(day, EpisodeStartDate, EpisodeEndDate) between 0 and 4
								then '0-4'	
							end = '10+'
						then 1
						else 0
					end
				as int)
	,IsSameRCodeInPreviousEpisodeInSpell = 
			cast(
				case
					when
						(
							select top 1 
								encounter1.PrimaryDiagnosisCode
							from
								[APC].[Encounter] encounter1
							where 	
								encounter1.ProviderSpellNo = encounter.ProviderSpellNo
								and encounter1.EpisodeStartTime < encounter.EpisodeStartTime	
							order by
								encounter1.EpisodeStartTime
						) = PrimaryDiagnosisCode
					and PrimaryDiagnosisCode like 'r%'
				then 1
				else 0
				end
			as int)
	,LoE = datediff(day, EpisodeStartDate, EpisodeEndDate)
	,LoEGroup = 
			cast(
					case
						when datediff(day, EpisodeStartDate, EpisodeEndDate) >= 10
						then '10+'
						when datediff(day, EpisodeStartDate, EpisodeEndDate) between 5 and 9
						then '5-9'
						when datediff(day, EpisodeStartDate, EpisodeEndDate) between 0 and 4
						then '0-4'				
				end
			as varchar)
	,EpisodeCount = cast(1 as int)

from
	@OlapCalendar calendar
	
	inner join APC.Encounter encounter 
	on encounter.EpisodeEndDate = calendar.TheDate
		
	inner join PAS.Consultant consultant
	on consultant.ConsultantCode = encounter.ConsultantCode
	

	
where
	encounter.EpisodeEndDate between @start and @end
	and DomainLogin = @consultant
	--and LastEpisodeInSpellIndicator = 'Y'

option(recompile)




﻿




CREATE PROCEDURE [RPT].[GETAPCHaeamtology]
 @FromDate       Date
,@ToDate         Date

AS 

--DECLARE @FromDate AS DATE = '13-aug-2012'
--DECLARE @ToDate   AS DATE = '13-aug-2012'

SELECT DISTINCT
	 APC.[SiteCode]
	,DIRECT.[Directorate]
	,DIRECT.[Division]
	,APC.[StartWardTypeCode]
	,APC.[EndWardTypeCode]
	,APC.[ProviderSpellNo]
	,APC.[CasenoteNumber]
	,APC.[PatientForename]
	,APC.[PatientSurname]
	,Gender = 
	CASE APC.[SexCode]
		WHEN 1     THEN 'M'
		ELSE 'F'
	END       

	,DOB = 
	          CONVERT(VARCHAR(10),APC.[DateOfBirth],103) 
	,APC.[Postcode]
	,APC.[ConsultantCode]
	,APC.[SpecialtyCode]
	,SPEC.[Specialty]
	,SPEC.[NationalSpecialtyCode]
	,APC.[AdmissionTime]
	,APC.[EpisodeStartTime]
	,APC.[EpisodeEndTime]  
	,APC.[DischargeTime]
	,APC.[PrimaryDiagnosisCode]
	,APC.[PrimaryOperationCode]
	,PrimProcDesc= 
	          PRODESC.[Operation]      
	,APC.[SecondaryOperationCode1]
     + ' - ' +
     APC.[SecondaryOperationCode2]
     + ' - ' +
     APC.[SecondaryOperationCode3]
     + ' - ' +
     APC.[SecondaryOperationCode4]
     + ' - ' +
     APC.[SecondaryOperationCode5]
     + ' - ' +
     APC.[SecondaryOperationCode6]
     + ' - ' +
     APC.[SecondaryOperationCode7]
     + ' - ' +
     APC.[SecondaryOperationCode8]
     + ' - ' +
     APC.[SecondaryOperationCode9]
     + ' - ' +
     APC.[SecondaryOperationCode10]                    AS SecProcs
	                                   
	,CodingCompleteDate = 
		      CONVERT(VARCHAR(10),APC.[ClinicalCodingCompleteDate],103)    
              
FROM
	[warehousereporting].[APC].[Encounter]             AS APC
  
LEFT OUTER JOIN [warehousereporting].[APC].[Operation]      AS OPER
ON APC.[SourceUniqueID] = OPER.[APCSourceUniqueID]
  
LEFT OUTER JOIN [warehousereporting].[WH].[Operation]       AS PRODESC
ON APC.[PrimaryOperationCode] = PRODESC.[OperationCode]
  
LEFT OUTER JOIN [warehousereporting].[PAS].[Specialty]      AS SPEC
ON APC.[SpecialtyCode] = SPEC.[SpecialtyCode]
  
LEFT OUTER JOIN [warehousereporting].[PAS].[Directorate]    AS DIRECT
ON APC.[StartDirectorateCode] = DIRECT.[DirectorateCode]
  
WHERE 
	APC.[DischargeDate] BETWEEN @FromDate AND @ToDate
and apc.EpisodeStartTime = AdmissionTime
  AND 
 (
 (
	CHARINDEX(LEFT(APC.[PrimaryOperationCode],3),'W34-W99') > 0
  OR	
	APC.[PrimaryOperationCode] IN('X33.4','X33.5','X33.6') 
  )
  OR
  (
	CHARINDEX(LEFT(OPER.[OperationCode],3),'W34-W99') > 0
  OR	
	OPER.[OperationCode] IN('X33.4','X33.5','X33.6') 
  )
  )






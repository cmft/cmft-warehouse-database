﻿
-- use WarehouseReporting

CREATE Procedure [RPT].[GetMaternityCTGAssessmentsByDescription]  --'20130401', '20140331'


(@StartDate	date
,@EndDate	date
)

As


--use WarehouseReporting
declare
@LocalStartDate	date
,@LocalEndDate	date

set @LocalStartDate = @StartDate
set @LocalEndDate = @EndDate
-- 3 secs -- 55,004 rows each CTG Assessment and details so they can be grouped in report

--120598
select 
	--FinancialYear
	--,[Month] = left(convert(nvarchar,Ass.TimeEntered,120),7)
	--,WeekEnding = WeekNo,
	Ass.HospitalPatientID
	,Surname
	,NHSNumber
	,Ass.SessionNumber
	,Ass.FetusNumber 
	,CTGAssessmentDate = Ass.DateEntered
	,CTGAssessmentTime = Ass.TimeEntered
	,StaffGroup
	,StaffInitials
	,Ass.CTGDescriptionID
	,Ass.CTGDescription
	,Ass.BuddySigWithin10mins
	,Ass.BuddyDescription
	,Ass.BuddyStaffGroup
	,MaternalPulseRecorded
	,DilatationRecorded
	,Dilatation
	,CTGEventType
	,CTGAction
	,CTGCount =		case 
						when
							(row_number() over 
								(partition by Ass.SessionNumber,Ass.FetusNumber order by Ass.TimeEntered))
							= '1' then 1
						else 0
					end
	,CTGAssessmentCount =	case 
						when
							(row_number() over 
								(partition by Ass.SessionNumber,Ass.FetusNumber,Ass.TimeEntered order by Ass.TimeEntered))
							= '1' then 1
						else 0
					end
	,CTGActionCount =	case 
							when CTGEventType = 'A' then 1 
							else 0 
						end				
	,CTGLiquorCount =	case 
							when CTGEventType = 'L' then 1 
							else 0 
						end				
from Warehouse.Maternity.CTGAssessment Ass

--69970

left join
(select 
	*
	,CTGEventType = 'A' 
from 
	Warehouse.ETL.TLoadK2CTGActions
union
select 
	* 
	,CTGEventType = 'L' 
from
	Warehouse.ETL.TLoadK2CTGLiquor
) Act
on Ass.SessionNumber = Act.SessionNumber
and Ass.FetusNumber = Act.FetusNumber
and Ass.TimeEntered = Act.TimeEntered
--63769 Actions
--39859 Liquor

--left join WarehouseOLAPMergedV2.WH.Calendar Cal
--on cast(Ass.TimeEntered as date) = Cal.TheDate

where 
	Ass.FetusNumber in ('0','1') 
and Ass.DateEntered between @LocalStartDate and @LocalEndDate



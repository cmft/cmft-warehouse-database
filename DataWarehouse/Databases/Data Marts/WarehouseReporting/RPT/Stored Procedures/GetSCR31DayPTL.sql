﻿





 
CREATE PROCEDURE [RPT].[GetSCR31DayPTL]
 @FirstTreatDate    Date
,@TumourDescGroup   varchar(50) = NULL

AS  
--DECLARE @FirstTreatDate AS DATE 
--DECLARE @TumourGroup    AS nvarchar(20)
--SET     @FirstTreatDate = '01-FEB-2012'
--SET     @TumourDescGroup  = 'Lung'

SELECT 
       DEM.[PATIENT_ID]                                    AS PatId
      ,REF.[CARE_ID]                                       AS CareId
      ,DEM.[N1_1_NHS_NUMBER]                               AS NHS_Num
      ,DEM.[N1_2_HOSPITAL_NUMBER]                          AS Casenote
      ,DEM.[N1_5_SURNAME]                                  AS Surname
      ,DEM.[N1_6_FORENAME]                                 AS Forename
      ,DEM.[N1_8_POSTCODE]                                 AS PostCode
      ,CONVERT(varchar,DEM.[N1_10_DATE_BIRTH],103)         AS DoB
      ,DEM.[N1_13_PCT]                                     AS PCT
      ,REF.[L_CANCER_SITE]                                 AS TumourGroup
      ,CONVERT(varchar,REF.[N2_6_RECEIPT_DATE],103)        AS RefRecdDate
      ,[N2_14_ADJ_TIME]                                    AS WTA_1stSeen
      ,CONVERT(varchar,REF.[N2_9_FIRST_SEEN_DATE],103)     AS FirstAppDate
      ,CONVERT(varchar,RPT.udfDTRDate(REF.[CARE_ID]),103) AS DTR_Date
      ,CONVERT(varchar,REF.[N4_1_DIAGNOSIS_DATE],103)      AS DiagnosisDate
      ,CONVERT(varchar,SUR.[N7_9_SURGERY_DATE],103)        AS SurgeryDate
      ,CONVERT(varchar,DATEADD(d,(31+[N2_14_ADJ_TIME]),RPT.udfDTRDate(REF.[CARE_ID])), 103) AS BreachDate
      ,REF.[N4_2_DIAGNOSIS_CODE]                           AS DiagCode
      ,DIA.[DIAG_DESC]                                     AS DiagDesc
      ,REF.[N2_8_SPECIALTY]                                AS SpecCode
      ,SPE.[SPECIALTY_DESC]                                AS SpecDesc
      ,REF.[N2_7_CONSULTANT]                               AS ConsCode
      ,[CON_CODE] + ' - ' + [CON_DESC]                     AS ConsName
      ,REF.[N2_4_PRIORITY_TYPE]                            AS PriorityTypeCode
      ,PRI.[PRIORITY_DESC]                                 AS PriorityTypeDesc
      ,REF.[N2_13_CANCER_STATUS]                           AS StatusCode
      ,STA.[STATUS_DESC]                                   AS StatusDesc
      ,SNO.[M_CODE]                                        AS SNOMED_Code
      ,SNO.[M_DESC]                                        AS SNOMED_Desc
      ,CONVERT( NVARCHAR(250), TC.[COMMENTS])              AS Tracking_Comment

FROM            [CancerRegister].[dbo].[tblDEMOGRAPHICS]      AS DEM
INNER JOIN      [CancerRegister].[dbo].[tblMAIN_REFERRALS]    AS REF ON REF.[PATIENT_ID] = DEM.[PATIENT_ID] 
INNER JOIN      [CancerRegister].[dbo].[ltblSTATUS]           AS STA ON REF.[N2_13_CANCER_STATUS] = STA.[STATUS_CODE] 
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblSPECIALTIES]      AS SPE ON REF.[N2_8_SPECIALTY] = SPE.[SPECIALTY_CODE]
LEFT OUTER JOIN [CancerRegister].[dbo].[tblMAIN_SURGERY]      AS SUR ON REF.[CARE_ID] = SUR.[CARE_ID]
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblCONSULTANTS]      AS CON ON CON.[NATIONAL_CODE] = REF.[N2_7_CONSULTANT]
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblDIAGNOSIS]        AS DIA ON DIA.[DIAG_CODE] = REF.[N4_2_DIAGNOSIS_CODE]
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblPRIORITY_TYPE]    AS PRI ON PRI.[PRIORITY_CODE] = REF.[N2_4_PRIORITY_TYPE]  
LEFT OUTER JOIN [CancerRegister].[dbo].[ltblSNOMed]           AS SNO ON SNO.[M_CODE] = REF.[N4_5_HISTOLOGY]
LEFT OUTER JOIN [CancerRegister].[dbo].[tblTRACKING_COMMENTS] AS TC
   ON TC.[CARE_ID] = REF.[CARE_ID]
   AND (TC.[COM_ID] = (
                        SELECT MAX([COM_ID] )
                        FROM [CancerRegister].[dbo].[tblTRACKING_COMMENTS]
                        WHERE REF.[CARE_ID] = [CARE_ID]
                      )
       )

WHERE 
	(
	  (REF.[N2_13_CANCER_STATUS] NOT IN ('03', '07', '08', '18', '21')
      AND REF.[N2_4_PRIORITY_TYPE] <> '03')
  OR
      (
      REF.[N2_13_CANCER_STATUS] IN ('12', '13', '19', '20')
      AND REF.[N2_4_PRIORITY_TYPE] = '03'
      )
   )
      
AND (
		RPT.udfDTRDate(REF.[CARE_ID])  IS NULL 
	OR 
		RPT.udfDTRDate(REF.[CARE_ID]) >= @FirstTreatDate
	)
AND    REF.[L_CANCER_SITE] LIKE  (
                                   CASE  WHEN @TumourDescGroup ='All' THEN '%' 
                                   ELSE @TumourDescGroup 
                                   END
                                    )









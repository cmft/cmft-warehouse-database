﻿
CREATE procedure [RPT].CasenoteNonElectiveAdmission
(
	 @FromDate date
	,@ToDate date
	,@WardCode varchar(20)
	,@SiteCode varchar(4)
)
as

	/******************************************************************************
	**  Name: RPT.CasenoteNonElectiveAdmission
	**  Purpose: 
	**
	**  Casenotes of Non Elective Admissions
	**	JS requested 23/08/2013. Combines latest consultant episode for each non
	**	elective admission. Links to BasePASCasenoteBooking for details of
	**	multiple casenotes. Apparently needs to be based on near to real time
	**	as possible so based on APCUpdate.Encounter. Will need more work on the model
	**	as this will only pick casenotes that are already in the process (WL or OP)
	**
	**	NOTE - Please maintain this stored procedure via the Visual Studio 
	**         PatientMasterIndex solution
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	**             DG         Initial Coding
	******************************************************************************/

----debug
--declare
--	 @FromDate date = '1 Mar 2013'
--	,@ToDate date = '31 Mar 2013'

select --top 10 
	 Encounter.Surname
	,Encounter.Forenames
	,Encounter.DateOfBirth
	,Encounter.DistrictNo
	,EncounterTypeCode = PatientCategoryCode
	,BookingDate = null
	,BookingTime = null
	,Encounter.SourcePatientNo
	,SourceEntityRecno = SourceEncounterNo
	,EncounterDate = AdmissionDate
	,EncounterTime = AdmissionTime
	,BookingCasenoteNo = APCUpdateEncounter.CasenoteNumber
	,APCUpdateEncounter.StartWardTypeCode
	,Ward.Ward
	,ClinicCode = null
	,Clinic = null
	,Encounter.CasenoteNo
	,Encounter.AllocatedDate
	,Encounter.CasenoteLocationCode
	,CasenoteLocation.CasenoteLocation
	,Encounter.CasenoteLocationDate
	,Encounter.CasenoteStatus
	,Encounter.WithdrawnDate
	,Comment = 
				case
					when Encounter.CurrentLoanComment is null
					then Encounter.CurrentLoanReason
					else coalesce(Encounter.CurrentLoanComment, '') + '. ' + coalesce(Encounter.CurrentLoanReason,'')
				end
	,CurrentBorrowerCode = Encounter.CurrentLoanBorrowerCode
	,CurrentBorrower = CasenoteBorrower.Borrower
from
	(
		select distinct -- just get distinct list of multiple casenotes irrespective of Encounter type
			Encounter.Surname
			,Encounter.Forenames
			,Encounter.DateOfBirth
			,Encounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.CasenoteNo
			,Encounter.AllocatedDate
			,Encounter.CasenoteLocationCode
			,Encounter.CasenoteLocationDate
			,Encounter.CasenoteStatus
			,Encounter.WithdrawnDate
			,Encounter.CurrentLoanComment 
			,Encounter.CurrentLoanReason
			,Encounter.CurrentLoanBorrowerCode
		from
			WarehouseOLAP.dbo.BasePASCasenoteBooking Encounter
	) Encounter

inner join APCUpdate.Encounter APCUpdateEncounter
on Encounter.SourcePatientNo = APCUpdateEncounter.SourcePatientNo

left join Warehouse.PAS.CasenoteLocation
on	CasenoteLocation.CasenoteLocationCode = Encounter.CasenoteLocationCode

left join Warehouse.PAS.CasenoteBorrower
on	CasenoteBorrower.BorrowerCode = Encounter.CurrentLoanBorrowerCode

left join Warehouse.PAS.Ward
on	Ward.WardCode = APCUpdateEncounter.StartWardTypeCode

where
	PatientCategoryCode = 'NE'
and
	/* just get the latest FCE in spell */
	not exists
				(
				select
					1
				from
					APCUpdate.Encounter APCUpdateEncounterNext
				where
					APCUpdateEncounterNext.SourcePatientNo = APCUpdateEncounter.SourcePatientNo
				and
					APCUpdateEncounterNext.ProviderSpellNo = APCUpdateEncounter.ProviderSpellNo
				and
					APCUpdateEncounterNext.EpisodeStartTime > APCUpdateEncounter.EpisodeStartTime
				)

and
	not exists
		(
		select
			1
		from
			Warehouse.APC.WaitingListActivity LaterActivity
		where
			--don't care which episode
			LaterActivity.SourcePatientNo = APCUpdateEncounter.SourcePatientNo
		and	LaterActivity.ActivityTime > APCUpdateEncounter.AdmissionTime
		)

and
	AdmissionDate between @FromDate and @ToDate
and	(
		APCUpdateEncounter.StartWardTypeCode = @WardCode
	or	@WardCode is null
	)
and	(
		APCUpdateEncounter.StartSiteCode = @SiteCode
	or	@SiteCode is null
	)
			
					

--select
--	*
--from
--	WarehouseOLAP.dbo.BasePASCasenoteBooking
--where
--	SourcePatientNo = 162748
	
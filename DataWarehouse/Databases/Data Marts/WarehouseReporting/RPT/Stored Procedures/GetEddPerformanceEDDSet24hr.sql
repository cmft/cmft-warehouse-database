﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [RPT].[GetEddPerformanceEDDSet24hr]
	@startdate smalldatetime
	,@enddate smalldatetime
	,@specialtycode varchar(max)
	,@Division varchar(max)
	,@patientcategory varchar(max)
AS 


--DECLARE @startdate  smalldatetime= '28/November/2011'
--DECLARE @enddate smalldatetime= '04/December/2011'


SELECT   
	 ExpectedLOS.SourcePatientNo
	,ExpectedLOS.SourceSpellNo
	,ExpectedLOS.ManagementIntentionCode
	,DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))
	,DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime))
	,ISNULL(division, 'Unknown') AS Division
	,ISNULL(Specialty.NationalSpecialtyCode, 'Unknown') AS SpecialtyCode
	,ISNULL(WHSpecialty.Specialty, 'Unknown') AS Specialty
	,ISNULL(Ward.WardCode, 'Unknown') AS WardCode
	,ISNULL(Ward.Ward, 'Unknown') AS Ward
	,ISNULL(Consultant.ConsultantCode,'Unknown') AS ConsultantCode
	,ISNULL(Consultant.Consultant,'Unknown') AS Consultant
	,POD = case
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'2'
					,'3' --INTERVAL ADMISSION
					,'4'
					,'5'
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective
			--Non Elective
				else 'NE' --Non Elective
			end
	,ISNULL(ExpectedLOS.RTTPathwayCondition,'Unknown') AS PathwayCondition
	,1 as Discharges
	,ExpectedLOS.AdmissionTime
	,AdmissionDate = CONVERT(DATE, ExpectedLOS.AdmissionTime)
	,DischargeDate = CONVERT(DATE, ExpectedLOS.DischargeTime)
	,EDDDate = DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime))
	,ExpectedLOS.EddCreatedTime
	,ExpectedLOS.ExpectedLOS
	,Exp_LOS = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))
	,Actual_LOS = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) 
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))  <0 THEN 1 ELSE 0 END AS [varless0days]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   =0 THEN 1 ELSE 0 END AS [varequal0days]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   IN(1,2) THEN 1 ELSE 0 END AS [var1-2days]	
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >2 THEN 1 ELSE 0 END AS [var3plusdays]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0 THEN 1 ELSE 0 END AS [vargreater0days]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0 THEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   ELSE 0 END AS [sumover0EDD]

FROM         
	Warehouse.APC.ExpectedLOS ExpectedLOS
	
	LEFT OUTER JOIN Warehouse.WH.Directorate Directorate
		  ON ExpectedLOS.DirectorateCode = Directorate.DirectorateCode 
	
	LEFT OUTER JOIN Warehouse.WH.Division Division
		ON Directorate.DivisionCode = Division.DivisionCode
	
	LEFT OUTER JOIN Warehouse.PAS.Specialty
		ON ExpectedLOS.Specialty =  Specialty.SpecialtyCode 

	left join Warehouse.WH.TreatmentFunction WHSpecialty
	on	WHSpecialty.SpecialtyCode = Specialty.NationalSpecialtyCode

	LEFT OUTER JOIN Warehouse.PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = ExpectedLOS.AdmissionMethodCode

	LEFT OUTER JOIN Warehouse.PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = ExpectedLOS.ManagementIntentionCode
	
	LEFT OUTER JOIN Warehouse.PAS.Consultant Consultant
	ON ExpectedLOS.Consultant = Consultant.ConsultantCode
	
	LEFT OUTER JOIN Warehouse.PAS.Ward Ward
	ON ExpectedLOS.Ward = Ward.WardCode

WHERE     
		CONVERT(date, ExpectedLOS.DischargeTime, 103) >= @startdate --'14/Aug/2011'
	AND CONVERT(date, ExpectedLOS.DischargeTime, 103) <= @enddate --'14/Aug/2011'
	AND CASE 
			WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) < 1440 THEN 1
			ELSE 0
		 END = 1
	--AND ExpectedLOS.Ward NOT IN ('47', '64', '64A', '64B', '67', '67A', 'HM')
	AND ExpectedLOS.ManagementIntentionCode NOT IN ('R','N') 
	AND NOT (ExpectedLOS.Ward = 'ESTU' AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6)
	AND Specialty.NationalSpecialtyCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@specialtycode,','))
    AND Division IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))
	AND ArchiveFlag <> 'D'
	AND Specialty.NationalSpecialtyCode <> '501'
	AND ExpectedLOS.Ward <> '76A'
	AND Specialty.Specialty NOT like '%DIALYSIS%'
	AND Specialty.Specialtycode NOT like 'IH%'
	AND ExpectedLOS.Ward NOT like 'SUB%'
	AND
		case
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'2'
					,'3' --INTERVAL ADMISSION
					,'4'
					,'5'
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective
			--Non Elective
				else 'NE' --Non Elective
			end IN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@patientcategory,','))

 


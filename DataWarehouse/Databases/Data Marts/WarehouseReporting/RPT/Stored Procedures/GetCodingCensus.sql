﻿
CREATE PROCEDURE [RPT].[GetCodingCensus] 
(
	 @CensusStartDate Date = NULL
	,@Division varchar(MAX)
	,@NationalSpecialtyCode varchar(MAX)
)

AS

SELECT 
	 CensusDate
	,ProviderSpellNo
	,CodingLagDays
	,CodingLagDaysGroup = CASE 
							WHEN CodingLagDays BETWEEN 0 AND 6 THEN convert(varchar(5),CodingLagDays)
							WHEN CodingLagDays BETWEEN 7 AND 13 THEN '07-14' 
							WHEN CodingLagDays BETWEEN 14 AND 20 THEN '14-21' 
							WHEN CodingLagDays BETWEEN 21 AND 27 THEN '21-28'
							WHEN CodingLagDays <0 THEN '0'
							ELSE '28+'
						  END
	,CasesWithinTarget = CASE
							WHEN CodingLagDays <7 THEN 1
							ELSE 0
						 END
FROM 
	WarehouseReporting.APC.CodingCensus CodingCensus
INNER JOIN PAS.Specialty Specialty
	ON Specialty.SpecialtyCode = CodingCensus.SpecialtyCode
INNER JOIN PAS.Directorate Directorate
	ON Directorate.DirectorateCode = CodingCensus.EndDirectorateCode
WHERE 
	(
		CensusDate >= @CensusStartDate
	OR
		@CensusStartDate IS NULL
	)
	AND	Directorate.Division IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
	AND Specialty.NationalSpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))

GROUP BY 	
	 CensusDate
	,CodingLagDays
	,ProviderSpellNo


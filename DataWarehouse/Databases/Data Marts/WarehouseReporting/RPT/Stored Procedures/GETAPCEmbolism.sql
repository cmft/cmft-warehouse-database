﻿






CREATE PROCEDURE [RPT].[GETAPCEmbolism] 
 @FromDate       Date
,@ToDate         Date

AS 

--DECLARE @FromDate AS DATE 
--DECLARE @ToDate   AS DATE 
--SET     @FromDate = '01-AUG-2012'
--SET     @ToDate   = '31-AUG-2012'


SELECT
       APC.[SiteCode]
      ,APC.[StartWardTypeCode]
      ,APC.[ProviderSpellNo]
      ,APC.[CasenoteNumber]
      ,APC.[NHSNumber]
      ,APC.[PatientForename]
      ,APC.[PatientSurname]
      ,APC.[DateOfBirth]
      ,APC.[Postcode]
      ,APC.[AdmissionDate]
      ,APC.[AdmissionTime]  
      ,APC.[DischargeDate]
      ,APC.[DischargeTime]
      ,APC.[PrimaryDiagnosisCode]                     AS DiagCode
      ,DIAG.[Diagnosis]                               AS DiagDesc
        
  FROM [APC].[Encounter]         AS APC
  INNER JOIN [WH].[Diagnosis]    AS DIAG
  ON APC.[PrimaryDiagnosisCode] = DIAG.[DiagnosisCode]
  
  WHERE APC.[AdmissionDate]  BETWEEN @FromDate AND @ToDate
  AND 
  (
  CHARINDEX(LEFT(APC.[PrimaryDiagnosisCode],3),'I26-I72-I740-I80-I81-I82-I83-O22') > 0
  OR APC.[PrimaryDiagnosisCode] = 'O08.7'
  )
  
  GROUP BY
       APC.[SiteCode]
      ,APC.[StartWardTypeCode]
      ,APC.[ProviderSpellNo]
      ,APC.[CasenoteNumber]
      ,APC.[NHSNumber]
      ,APC.[PatientForename]
      ,APC.[PatientSurname]
      ,APC.[DateOfBirth]
      ,APC.[Postcode]
      ,APC.[AdmissionDate]
      ,APC.[AdmissionTime]  
      ,APC.[DischargeDate]
      ,APC.[DischargeTime]
      ,APC.[PrimaryDiagnosisCode]                    
      ,DIAG.[Diagnosis]                           
  
   
  UNION 
  
  SELECT
       APC.[SiteCode]
      ,APC.[StartWardTypeCode]
      ,APC.[ProviderSpellNo]
      ,APC.[CasenoteNumber]
      ,APC.[NHSNumber]
      ,APC.[PatientForename]
      ,APC.[PatientSurname]
      ,APC.[DateOfBirth]
      ,APC.[Postcode]
      ,APC.[AdmissionDate]
      ,APC.[AdmissionTime]  
      ,APC.[DischargeDate]
      ,APC.[DischargeTime]
      ,APC.[SecondaryDiagnosisCode1]                  AS DiagCode
      ,DIAG.[Diagnosis]                               AS DiagDesc
        
  FROM [APC].[Encounter]         AS APC
  INNER JOIN [WarehouseReporting].[WH].[Diagnosis]    AS DIAG
  ON APC.[SecondaryDiagnosisCode1] = DIAG.[DiagnosisCode]
  
   WHERE APC.[AdmissionDate]  BETWEEN @FromDate AND @ToDate
  AND 
  (
  CHARINDEX(LEFT(APC.[SecondaryDiagnosisCode1],3),'I26-I72-I740-I80-I81-I82-I83-O22') > 0
  OR APC.[SecondaryDiagnosisCode1] = 'O08.7'
  )
  
  GROUP BY 
       APC.[SiteCode]
      ,APC.[StartWardTypeCode]
      ,APC.[ProviderSpellNo]
      ,APC.[CasenoteNumber]
      ,APC.[NHSNumber]
      ,APC.[PatientForename]
      ,APC.[PatientSurname]
      ,APC.[DateOfBirth]
      ,APC.[Postcode]
      ,APC.[AdmissionDate]
      ,APC.[AdmissionTime]  
      ,APC.[DischargeDate]
      ,APC.[DischargeTime]
      ,APC.[SecondaryDiagnosisCode1]                   
      ,DIAG.[Diagnosis]                           
  
   

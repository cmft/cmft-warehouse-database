﻿



-- =============================================
-- Author:		<Alan Bruce >
-- Create date: <06-Mar-2012>
-- Description:	<Admitted Patients in Childrens discharged in the week following
--				the start date where the admission and procedure is not on the same day>
-- =============================================
CREATE PROCEDURE [RPT].[GetAdmittedProcedureDelays]
	-- Add the parameters for the stored procedure here
	@StartDate Datetime, @Division varchar(5)
	
AS

BEGIN	

Declare @EndDate DateTime

Set @EndDate = DATEADD(day,7,@StartDate)

SELECT        
  b.EncounterRecno
, b.PatientForename
, b.PatientSurname
, b.StartWardTypeCode
, b.ConsultantCode
, b.AdmissionDate
, b.DischargeDate
, WH.Operation.Operation
, WH.Operation.OperationGroup
, WH.Operation.OperationChapter
, WH.Operation.OperationCode
, b.PrimaryOperationCode
, b.PrimaryOperationDate
, b.CasenoteNumber
, b.StartDirectorateCode
, b.EndDirectorateCode
, b.PatientCategoryCode
FROM         
(SELECT 
	APC.Encounter.*, a.OpDate, a.CaseNote
	FROM   APC.Encounter INNER JOIN
	 (SELECT MIN(PrimaryOperationDate) AS OpDate, CasenoteNumber AS CaseNote
		FROM APC.Encounter AS Encounter_1
		WHERE (PrimaryOperationCode IS NOT NULL) AND (DischargeDate >= @StartDate) 
		AND (DischargeDate < @EndDate) 
		AND (DATEPART(DY, AdmissionDate) <> DATEPART(DY, PrimaryOperationDate)) 
		AND (AdmissionMethodCode in (SELECT  AdmissionMethodCode
        FROM [WarehouseReporting].[PAS].[AdmissionMethod]
		Where AdmissionMethodType = 'Elective')) AND (FirstEpisodeInSpellIndicator = 1)
		AND (Encounter_1.StartDirectorateCode = @division or @division = '- 1')
		GROUP BY CasenoteNumber) AS a 
		ON a.CaseNote = APC.Encounter.CasenoteNumber 
		AND a.OpDate = APC.Encounter.PrimaryOperationDate) AS b 
		INNER JOIN WH.Operation ON b.PrimaryOperationCode = WH.Operation.OperationCode

End




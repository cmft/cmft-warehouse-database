﻿
CREATE PROCEDURE [RPT].[GetCodingPerformance]

(
 @StartDate Date 
,@EndDate Date
,@Division varchar(MAX)
,@NationalSpecialtyCode varchar(MAX)
)

AS

SELECT
	 Specialty = Specialty.NationalSpecialty
	,SpecialtyCode = Specialty.NationalSpecialtyCode
	,Division = Directorate.Division
	,CodingLagDays = Encounter.CodingLagDays
	,CodingLagDaysGroup = CASE 
							WHEN Encounter.CodingLagDays Between 0 AND 6 THEN convert(varchar(5),Encounter.CodingLagDays)
							WHEN Encounter.CodingLagDays BETWEEN 7 AND 13 THEN '07-14' 
							WHEN Encounter.CodingLagDays BETWEEN 14 AND 20 THEN '14-21' 
							WHEN Encounter.CodingLagDays BETWEEN 21 AND 27 THEN '21-28'
							ELSE '28+'
						  END
	,CasesWithinTarget = CASE
							WHEN Encounter.CodingLagDays <7 THEN Count(Encounter.Cases)
							ELSE 0
						 END
	,Cases = Count(Encounter.Cases)
FROM 
	(
		SELECT
			  CodingLagDays =  CASE 
									WHEN DATEDIFF(D,Encounter.DischargeDate,Encounter.ClinicalCodingCompleteDate) <0 THEN 0 
									ELSE DATEDIFF(D,Encounter.DischargeDate,Encounter.ClinicalCodingCompleteDate) 
							   END
			 ,Cases
			 ,SourceUniqueID
			 ,DischargeDate
			 ,SpecialtyCode
			 ,EndDirectorateCode
		FROM
			apc.Encounter
		WHERE 	
				ClinicalCodingStatus = 'C'
			AND ClinicalCodingCompleteDate >= @StartDate
			AND ClinicalCodingCompleteDate <= @EndDate
		) Encounter
LEFT OUTER JOIN PAS.Specialty Specialty
	ON Specialty.SpecialtyCode = Encounter.SpecialtyCode
LEFT OUTER JOIN PAS.Directorate Directorate
	ON Directorate.DirectorateCode = Encounter.EndDirectorateCode
WHERE
	Directorate.Division IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
	AND Specialty.NationalSpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
GROUP BY 	 
	 Specialty.NationalSpecialtyCode
	,Specialty.NationalSpecialty
	,Directorate.Division
	,Encounter.CodingLagDays

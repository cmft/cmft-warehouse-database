﻿


CREATE proc [RPT].[GetStrokeReportsCombined] 

@FinancialMonthKey INT

as

declare @FirstDischargeDate date

select
	@FirstDischargeDate = min(TheDate)
from
	WH.Calendar
where
	FinancialMonthKey = @FinancialMonthKey



DECLARE @StrokeTable TABLE 
(	
	 PCTDescription varchar(100)
	,InternalNo  varchar(20)
	,EpisodeNo varchar(20)
	,PrimaryDiagnosisIndicator  varchar(11)
	,OtherDiagnosisIndicator   varchar(11)
	,CasenoteNumber varchar(20)
	,PatientForename varchar(20)
	,PatientSurname varchar(30)
	,DateOfBirth datetime
	,DateOfDeath smalldatetime
	,AdmissionTime smalldatetime
	,DischargeTime smalldatetime
	,Numerator int
	,DenominatorIncAE int
	,PercentageOnStrokeWardIncAE float
	,TheMonth nvarchar(34)
	,AppointmentDate datetime
	,AttendanceOutcomeCode varchar(5)
	,DNACount int
	,PCTCode varchar(10) 
	,ArrivalTime smalldatetime
	,DepartureTime smalldatetime
	,AdmissionMethodCode varchar(2)
	,PatientCategoryCode varchar(2) 
	,FirstAdmissionWard varchar(10)
)


insert into @StrokeTable
(
	 PCTDescription
	,InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator
	,OtherDiagnosisIndicator
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,AdmissionTime
	,DischargeTime
	,Numerator
	,DenominatorIncAE
	,PercentageOnStrokeWardIncAE
	,TheMonth
	,PCTCode
	,ArrivalTime
	,DepartureTime
	,AdmissionMethodCode
	,PatientCategoryCode
)


SELECT
	 PCTDescription
	,InternalNo
	,EpisodeNo

	,PrimaryDiagnosisIndicator = PrimaryPosition
		--CASE 
		--WHEN DiagnosisIndicator = 'Both' then 'Yes' 
		--WHEN DiagnosisIndicator = 'Primary' then 'Yes'
		--ELSE 'No' 
		--END

	,OtherDiagnosisIndicator = SecondaryPosition
		--CASE 
		--WHEN DiagnosisIndicator = 'Both' then 'Yes'
		--WHEN DiagnosisIndicator = 'Secondary' then 'Yes' 
		--ELSE 'No'
		--end	

	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,AdmissionTime
	,DischargeTime
	
	,Numerator =
		LOSStrokeWard

	,DenominatorIncAE = 
		Denominator + ISNULL(AETime,0)

	,PercentageOnStrokeWardIncAE = 
		((LOSStrokeWard * 1.00) / (Denominator + ISNULL(AETime , 0)) * 1.00) * 100

	,myqry.TheMonth

	,PCTCode
	,ArrivalTime
	,DepartureTime
	,AdmissionMethodCode
	,PatientCategoryCode
FROM
	(
	SELECT 
		 PCTDescription = 
			PCT.[PCT Description]

		,InternalNo = Encounter.SourcePatientNo
		,EpisodeNo = Encounter.SourceSpellNo
		,CasenoteNumber
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.AdmissionTime
		,Encounter.DischargeTime

		,LOSStrokeWard =
			ISNULL(LOSStrokeWard , 0)

		,Denominator =
			SUM(
				DATEDIFF(
					 mi
					,Encounter.EpisodeStartTime
					,Encounter.EpisodeEndTime
				)
			)

		,AETime = TimeSpentInAE

		,StrokePatients.DiagnosisIndicator
		,StrokePatients.PrimaryPosition
		,StrokePatients.SecondaryPosition

		,Calendar.TheMonth
		,Encounter.PCTCode
		,LOSAE.ArrivalTime
		,LOSAE.DepartureTime
		,Encounter.AdmissionMethodCode
		,Encounter.PatientCategoryCode
	FROM 
		WarehouseReporting.apc.Encounter Encounter


-- CCB 2012-06-29 what is this and why are we using it?
	LEFT JOIN Information.dbo.PCT PCT 
	ON	PCT.[PCT Code] = Encounter.PCTCode


	--LOS on Stroke Ward
	LEFT JOIN
		(
		SELECT 
			 ProviderSpellNo

			,LOSStrokeWard =
				SUM(
					DATEDIFF(
						 mi
						,WardStay.StartTime
						,WardStay.EndTime
					)
				) 

		FROM 
			WarehouseReporting.APC.WardStay
		WHERE 
			WardCode in ( '30M' , '31M' , '31R') 
		GROUP BY 
			ProviderSpellNo
		) WardStay 
	ON WardStay.ProviderSpellNo = Encounter.ProviderSpellNo


	--LOS in AE
	LEFT JOIN 
		(	
		SELECT distinct
			 APCEncounter.DistrictNo
			,APCEncounter.AdmissionTime
			,TimeSpentInAE = StageDurationMinutes
			,AEEncounter.ArrivalTime
			,AEEncounter.DepartureTime
		FROM
			WarehouseReporting.APC.Encounter APCEncounter

		INNER JOIN WarehouseReporting.AE.Encounter AEEncounter
		ON	AEEncounter.DistrictNo = APCEncounter.DistrictNo
		and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR , -2 , APCEncounter.AdmissionTime) and DATEADD(HOUR , 2 , APCEncounter.AdmissionTime)

		INNER JOIN WarehouseReporting.AE.Fact Fact 
		ON	Fact.SourceUniqueID = AEEncounter.SourceUniqueID

		WHERE
			Fact.StageCode = 'INDEPARTMENTADJUSTED'
		) LOSAE 	
	ON	LOSAE.DistrictNo = Encounter.DistrictNo
	AND	LOSAE.AdmissionTime = Encounter.AdmissionTime


	--only return stroke patients
	INNER JOIN 
		(	
		SELECT 
			 SourcePatientNo
			,SourceSpellNo

			,DiagnosisIndicator = 
				CASE 
				WHEN [Primary] is not null AND [Secondary] is not null then 'Both'
				WHEN [Primary] is not null then 'Primary'
				WHEN [Secondary] is not null then 'Secondary'
				ELSE NULL
				END

			,PrimaryPosition = [Primary]
			,SecondaryPosition = [Secondary]

		FROM
			(
			--pivot data so we can compare diagnosis columns as a primary and secondary can be the same
			SELECT
				 SourcePatientNo
				,SourceSpellNo
				,[Primary]
				,[Secondary]
			FROM
				(
				SELECT 
					 SourcePatientNo
					,SourceSpellNo
					,Position = 'Primary'
					,DiagnosisType = 
						case
						when
								PrimaryDiagnosisCode like 'I61%' --stroke
							or	PrimaryDiagnosisCode like 'I63%' --stroke
							or	PrimaryDiagnosisCode like 'I64%' --stroke 
							then 'Stroke'
						else 'Stroke-Like'
						end
				FROM 
					WarehouseReporting.APC.Encounter Encounter
				WHERE 
					PrimaryDiagnosisCode like 'I61%' --stroke
				or	PrimaryDiagnosisCode like 'I63%' --stroke
				or	PrimaryDiagnosisCode like 'I64%' --stroke 
				or	PrimaryDiagnosisCode like 'I62%' --stroke-like
				or	PrimaryDiagnosisCode like 'H34%' --stroke-like

				UNION	
										
				SELECT 
					 Encounter.SourcePatientNo
					,Encounter.SourceSpellNo
					,Position = 'Secondary'
					,DiagnosisType = 
						case
						when
								PrimaryDiagnosisCode like 'I61%' --stroke
							or	PrimaryDiagnosisCode like 'I63%' --stroke
							or	PrimaryDiagnosisCode like 'I64%' --stroke 
							then 'Stroke'
						else 'Stroke-Like'
						end
				FROM
					WarehouseReporting.APC.Encounter Encounter

				INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
				ON	Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
				AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
				AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
				AND (
						Diagnosis.DiagnosisCode like 'I61%' --stroke  
					or	Diagnosis.DiagnosisCode like 'I63%' --stroke 
					or	Diagnosis.DiagnosisCode like 'I64%' --stroke 
					or	Diagnosis.DiagnosisCode like 'I62%' --stroke-like  
					or	Diagnosis.DiagnosisCode like 'H34%' --stroke-like
					)
				)s
			PIVOT
				(
				max(DiagnosisType)
				FOR Position IN ([Primary] , [Secondary])
				) as pvt
			)qry
		) StrokePatients
	ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
	AND StrokePatients.SourceSpellNo = Encounter.SourceSpellNo
					
	INNER JOIN WarehouseReporting.WH.Calendar Calendar
	ON	Calendar.TheDate = Encounter.DischargeDate

	WHERE
		Calendar.FinancialMonthKey = @FinancialMonthKey
	AND
		CASE 
		WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionDate)))))-1900)<19
		THEN 'C'
		ELSE 'A'
		END = 'A'
										

	GROUP BY 
		 PCT.[PCT Description]
		,Encounter.SourcePatientNo
		,Encounter.SourceSpellNo
		,CasenoteNumber
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.AdmissionTime
		,Encounter.DischargeTime
		,LOSStrokeWard
		,TimeSpentInAE
		,StrokePatients.DiagnosisIndicator
		,StrokePatients.PrimaryPosition
		,StrokePatients.SecondaryPosition
		,Calendar.TheMonth
		,Encounter.PCTCode
		,LOSAE.ArrivalTime
		,LOSAE.DepartureTime
		,Encounter.AdmissionMethodCode
		,Encounter.PatientCategoryCode


	) myqry


----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------  Now update Table Variable  -----------------------------

--1. update DNA Count for a patients spell

DECLARE @DNACount Table
(	
	 DistrictNo varchar(20)
	,SourcePatientNo  varchar(20)
	,SourceSpellNo  varchar(20)
	,DNA int
)

insert into @DNACount
(
	 DistrictNo
	,SourcePatientNo
	,SourceSpellNo
	,DNA
)

select
	 DistrictNo
	,SourcePatientNo
	,SourceSpellNo
	,DNA = Max(RowInd)
from
	(
	select 
		 OpEncounter.DistrictNo
		,Encounter.SourcePatientNo
		,Encounter.SourceSpellNo
		,OpEncounter.AppointmentTime

		,RowInd =
			row_number() over 
				(
				partition by 
					 OpEncounter.DistrictNo
					,Encounter.SourceSpellNo 
				order by 
					 OpEncounter.DistrictNo
					,Encounter.SourceSpellNo
					, OpEncounter.AppointmentTime
				)

	from
		OP.Encounter OpEncounter

	left join APC.Encounter Encounter
	on Encounter.DistrictNo = OpEncounter.DistrictNo
	--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
	AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

	where 
		Encounter.SourceSpellNo is not null
	AND OpEncounter.AttendanceOutcomeCode = ('DNA')
	AND Encounter.DischargeDate >= @FirstDischargeDate

	group by
		 OpEncounter.DistrictNo
		,Encounter.SourcePatientNo
		,Encounter.SourceSpellNo
		,OpEncounter.AppointmentTime
	)myqry

group by
	 DistrictNo
	,SourcePatientNo
	,SourceSpellNo



--now update Table Variable Stroke
update s
set 
	DNACount = fa.DNA
from
	@StrokeTable s

inner join 	@DNACount fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo


--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
----2.
update s
set 
	DNACount = 0
from
	@StrokeTable s
where
	DNACount is null
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
----Rule3.1: Show details of first attendance (but not if the DNA count is > 1)

	DECLARE @FirstAttendance Table
	(	
		 DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,SourceSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @FirstAttendance
		(
			 DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd
		)

		select
			 DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd
		from
			(
			select 
				 OpEncounter.DistrictNo
				,Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
				,OpEncounter.AppointmentTime
				,AttendanceOutcomeCode

				,RowInd =
					row_number() over 
						(
						partition by 
							 OpEncounter.DistrictNo
							,Encounter.SourceSpellNo 
						order by 
							 OpEncounter.DistrictNo
							,Encounter.SourceSpellNo
							,OpEncounter.AppointmentTime
						)
			from
				OP.Encounter OpEncounter

			left join APC.Encounter Encounter
			on Encounter.DistrictNo = OpEncounter.DistrictNo
			--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
			AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

			where 
					Encounter.SourceSpellNo is not null
				and AttendanceOutcomeCode in ('ATT','SATT','WLK')
				AND Encounter.DischargeDate >= @FirstDischargeDate

			group by
				 OpEncounter.DistrictNo
				,Encounter.SourcePatientNo
				,Encounter.SourceSpellNo
				,OpEncounter.AppointmentTime
				,AttendanceOutcomeCode
			)myqry

		where
			RowInd = 1

--now update Table Variable Stroke
update s
set 
	 AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode

from
	@StrokeTable s

inner join @FirstAttendance fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo

where
	DNACount < 2

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----Rule3.2: If DNA count is > 1 then show 2nd DNA details
	DECLARE @SecondDNADetails Table
	(	
		 DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,SourceSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @SecondDNADetails
	(
		 DistrictNo
		,SourcePatientNo
		,SourceSpellNo
		,AppointmentTime
		,AttendanceOutcomeCode
		,RowInd
	)

	select
		 DistrictNo
		,SourcePatientNo
		,SourceSpellNo
		,AppointmentTime
		,AttendanceOutcomeCode
		,RowInd
	from
		(
		select 
			 OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.SourceSpellNo
			,OpEncounter.AppointmentTime
			,AttendanceOutcomeCode

			,RowInd = 
				row_number() over 
					(
					partition by 
						 OpEncounter.DistrictNo
						,Encounter.SourceSpellNo 
					order by 
						 OpEncounter.DistrictNo
						,Encounter.SourceSpellNo
						,OpEncounter.AppointmentTime
					)
		from
			OP.Encounter OpEncounter

		left join APC.Encounter Encounter
		on	Encounter.DistrictNo = OpEncounter.DistrictNo
		--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
		AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

		where 
			Encounter.SourceSpellNo is not null
		AND OpEncounter.AttendanceOutcomeCode = ('DNA')
		AND Encounter.DischargeDate >= @FirstDischargeDate

		group by
			 OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.SourceSpellNo
			,OpEncounter.AppointmentTime
			,AttendanceOutcomeCode
		)myqry

	where 
		RowInd = 2

	group by 	
		 DistrictNo
		,SourcePatientNo
		,SourceSpellNo
		,AppointmentTime
		,AttendanceOutcomeCode
		,AppointmentTime
		,RowInd

--now update Table Variable Stroke
update s
set 
	 AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode
from
	@StrokeTable s

inner join @SecondDNADetails fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo

Where
	DNACount > 1

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----Rule3.3: If there has been only 1 DNA and no attendance then show the first DNA
	DECLARE @FirstDNADetails Table
	(	
		 DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,SourceSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @FirstDNADetails
	(
		 DistrictNo
		,SourcePatientNo
		,SourceSpellNo
		,AppointmentTime
		,AttendanceOutcomeCode
		,RowInd
	)

	select
		 DistrictNo
		,SourcePatientNo
		,SourceSpellNo
		,AppointmentTime
		,AttendanceOutcomeCode
		,RowInd
	from
		(
		select 
			OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.SourceSpellNo
			,OpEncounter.AppointmentTime
			,AttendanceOutcomeCode
		
			,RowInd =
				row_number() over 
					(
					partition by
						 OpEncounter.DistrictNo
						,Encounter.SourceSpellNo 
					order by
						 OpEncounter.DistrictNo
						,Encounter.SourceSpellNo
						,OpEncounter.AppointmentTime
					)
		from
			OP.Encounter OpEncounter

		left join APC.Encounter Encounter
		on	Encounter.DistrictNo = OpEncounter.DistrictNo
		--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
		AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

		where 
			Encounter.SourceSpellNo is not null
		AND OpEncounter.AttendanceOutcomeCode = ('DNA')
		AND Encounter.DischargeDate >= @FirstDischargeDate

		group by
			 OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.SourceSpellNo
			,OpEncounter.AppointmentTime
			,AttendanceOutcomeCode
		)myqry

		where 
			RowInd = 1

		group by 	
			 DistrictNo
			,SourcePatientNo
			,SourceSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd

--now update Table Variable Stroke
update s
set 
	 AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode

from
	@StrokeTable s

inner join @FirstDNADetails fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.SourceSpellNo = s.EpisodeNo

Where
	DNACount = 1
AND AppointmentDate is null


----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
--Rule 4 - Get First Episode Admission Ward
--we need to get the minimum admission date as the WardStay query brings back too much data

declare @MinAdmissionDate smalldatetime

select
	@MinAdmissionDate = MIN(AdmissionTime) 
from 
	@StrokeTable

DECLARE @FirstEpisodeWardTable TABLE 
(	 SourcePatientNo  varchar(20)
	,SourceSpellNo varchar(20)
	,WardCode varchar(10)
)

INSERT INTO @FirstEpisodeWardTable
(
	 SourcePatientNo
	,SourceSpellNo
	,WardCode
)

select
	 SourcePatientNo
	,SourceSpellNo
	,WardCode
from
	(
	select 
		 SourcePatientNo
		,SourceSpellNo
		,WardCode

		,RowInd = 
			row_number() over 
				(
				partition by 
					 SourcePatientNo
					,SourceSpellNo 
				order by 
					 StartTime asc
				)

	from
		APC.WardStay
	where
		StartTime >= @MinAdmissionDate
	)myqry
where
	RowInd = 1


update s
set FirstAdmissionWard = FirstEpisodeWard.WardCode

from
	@StrokeTable s

inner join @FirstEpisodeWardTable FirstEpisodeWard
on	FirstEpisodeWard.SourcePatientNo = s.InternalNo
AND FirstEpisodeWard.SourceSpellNo = s.EpisodeNo


--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--Now bring back data to the report :o)
select 
	 PCTDescription
	,InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator
	,OtherDiagnosisIndicator  
	,CasenoteNumber 
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,AdmissionTime
	,DischargeTime
	,Numerator
	,DenominatorIncAE 
	,PercentageOnStrokeWardIncAE 
	,TheMonth 
	,AppointmentDate 
	,AttendanceOutcomeCode
	,DNACount 
	,PCTCode  
	,ArrivalTime 
	,DepartureTime 
	,AdmissionMethodCode
	,PatientCategoryCode
	,FirstAdmissionWard

	,Timediff = 
		case 
		when ((DATEDIFF(hour,DischargeTime,AppointmentDate))/168)=0 then
			case 
			when (((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7)=0 then '' 
			else CAST(((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7 as varchar) + ' day' 
			end
		else CAST((DATEDIFF(hour,DischargeTime,AppointmentDate))/168 as varchar) + ' week ' +
			case
			when (((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7)=0 then '' 
			else CAST(((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7 as varchar) + ' day'
			end
		end 
from 
	@StrokeTable

order by 
	DischargeTime asc


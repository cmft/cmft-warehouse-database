﻿
CREATE proc RPT.GetIncomeAPCHRGQuality

@period varchar(50) --= 'Month 2 2012/2013'
,@position varchar(50) --= 'Test'
,@version int --= 1
,@susidentifier varchar(50) = null-- = 'BMCHT 201104011530*3178286*2'

as

select
	Period
	,Position
	,Version
	,SUSIdentifier
	,[QualityTypeCode]
	,[QualityCode]
	,[QualityMessage]

from
	Income.APCHRGQuality encounter
	
where
	(
	SUSIdentifier = @susidentifier 
	or @susidentifier is null
	)
and 
	Period = @period
and
	Position = @position
and
	Version = @version


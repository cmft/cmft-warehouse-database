﻿/****** Script for SelectTopNRows command from SSMS  ******/
CREATE procedure [RPT].[PatientSurvey] as

--SELECT
--	 CensusDate
--	,NHSNumber
--	,CasenoteNumber
--	,WardCode
--	,Ward
--	,Forenames
--	,Surname
--	,IntendedProcedureCode
--	,ProcedureType
--FROM
--	WarehouseReporting.APC.PatientSurvey


select
	 CensusDate = convert(date , (select DateValue from Warehouse.Utility.Parameter where Parameter = 'LOADAPCDATE'))
	,NHSNumber
	,CasenoteNumber
	,WardCode = WardStay.WardCode
	,Ward = Ward.Ward
	,Forenames = PatientForename
	,Surname = PatientSurname
	,IntendedProcedureCode = Encounter.IntendedPrimaryOperationCode
	,OPCS.ProcedureType
from
	Warehouse.APC.Encounter

left join Warehouse.APC.WardStay
on	WardStay.ProviderSpellNo = Encounter.ProviderSpellNo
and	WardStay.EndTime is null

inner join Warehouse.PAS.Ward
on	Ward.WardCode = WardStay.WardCode

inner join 
	--Hip Replacement
	(
		  select ProcedureCode = 'W37' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W38' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W39' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W46' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W47' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W48' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W93' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W94' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W95' , ProcedureType = 'Hip'

	--Knee Replacement
	union select ProcedureCode = 'W40' , ProcedureType = 'Knee'
	union select ProcedureCode = 'W41' , ProcedureType = 'Knee'
	union select ProcedureCode = 'W42' , ProcedureType = 'Knee'

	--CABG 
	union select ProcedureCode = 'K40' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K41' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K42' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K43' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K44' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K45' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K46' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K47' , ProcedureType = 'CABG'
	) OPCS
on	OPCS.ProcedureCode = left(Encounter.IntendedPrimaryOperationCode , 3)

where
	DischargeDate is null
and	EpisodeEndDate is null
﻿

create procedure [RPT].[GetCQUiNCardiac] 

 @FromDate   AS Date  
,@ToDate     AS Date  
 
--DECLARE @FromDate AS DATE        = '01-Sep-2011'
--DECLARE @ToDate   AS DATE        = '30-Sep-2011'

as

select 
      OperationDate = CONVERT(varchar,EncounterOperation.[OperationDate],103)  
      ,Theatre.[Theatre]
      ,ConsCode = Staff.[StaffCode1]                                        
      ,Consultant = Staff.[StaffName]                                            
      ,Casenote = REPLACE(EncounterOperation.[DistrictNo],'Y','/')           
      ,EncounterOperation.[Forename]
      ,EncounterOperation.[Surname]
      ,OperationComment = EncounterOperation.[Operation]                             
      ,EncounterOperation.[PrimaryProcedureCode]
      ,ProcedureDesc = Operation.[Operation]                                      
     
from 
	[WarehouseReporting].[Theatre].[EncounterOperation] EncounterOperation

left outer join
	[WarehouseReporting].[Theatre].[Theatre] Theatre
on	EncounterOperation.[TheatreCode] = Theatre.[TheatreCode]

left outer join 
	[WarehouseReporting].[WH].[Operation] Operation
on	EncounterOperation.[PrimaryProcedureCode] = Operation.[OperationCode]

left outer join
	[WarehouseReporting].[Theatre].[Staff] Staff
on	EncounterOperation.[Surgeon1Code] = Staff.[StaffCode]
  
where
	[OperationDate] BETWEEN @FromDate AND @ToDate
and Staff.[StaffCode1] IN ('MHABLCONS','MHABULCONS','RIRH','TV','KEM','DJMK','AHY')

--ORDER BY Staff.[StaffCode1]

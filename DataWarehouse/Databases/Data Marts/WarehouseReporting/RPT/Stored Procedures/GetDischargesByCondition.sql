﻿
create proc RPT.GetDischargesByCondition
(
@proc varchar (10) = null
,@start datetime = null
,@end datetime = null
)

with recompile

as

set dateformat dmy
set nocount on


--declare @start datetime
--		,@end datetime
		

set @end = isnull(@end, warehouse.dbo.f_Last_of_Last_Month(getdate()))
set @start = isnull(@start,dateadd(month, -13, @end))

--set @proc = isnull(@proc, 'FNOF')

if @proc = 'FNOF'

begin

select
	SourcePatientNo
	,AdmissionDate
	,DischargeDate
	,LoS = datediff(day, AdmissionDate, DischargeDate)
	,DischargeMonth = cast(TheMonth as datetime)
	,Cases = 1
	--,AvgLOS = avg(datediff(day, AdmissionDate, DischargeDate))
	--,MinLOS = min(datediff(day, AdmissionDate, DischargeDate))
	--,MaxLOS = max(datediff(day, AdmissionDate, DischargeDate))
	--,Discharges = count(*)
from 
	[APC].[Encounter] a
	inner join WH.Calendar
	on DischargeDate = TheDate

--where 
--	DischargeDate between @start and @end
--	and LastEpisodeInSpellIndicator = 'Y'
--	and CodingCompleteDate is not null
--	and exists (
--				select 
--					1
--				from 
--					[APC].[Encounter] b
--				where 
--					b.PrimaryDiagnosisCode like 's72%'
--					and a.ProviderSpellNo = b.ProviderSpellNo
--				)
--	--and floor(datediff(day, DateOfBirth, AdmissionTime) / 365.25) >= 16
	
where 
	DischargeDate between @start and @end
	and LastEpisodeInSpellIndicator = 'Y'
	and CodingCompleteDate is not null
	and AdmissionMethodCode in ('AE','EM','TR')
	and exists (
				select 
					1
				from 
					[APC].[Encounter] b
				where 
					(
					b.PrimaryDiagnosisCode like 's72.0%'
					or b.PrimaryDiagnosisCode like 's72.1%'
					or b.PrimaryDiagnosisCode like 's72.2%'
					)
					and a.ProviderSpellNo = b.ProviderSpellNo
				)
		
	--and floor(datediff(day, DateOfBirth, AdmissionTime) / 365.25) >= 16
	
--group by 
--	TheMonth
	
--order by
--	[Month]

end

else if @proc = 'HK'

begin

select
	SourcePatientNo
	,AdmissionDate
	,DischargeDate
	,LoS = datediff(day, AdmissionDate, DischargeDate)
	,DischargeMonth = cast(TheMonth as datetime)
	--,AvgLOS = avg(datediff(day, AdmissionDate, DischargeDate))
	--,MinLOS = min(datediff(day, AdmissionDate, DischargeDate))
	--,MaxLOS = max(datediff(day, AdmissionDate, DischargeDate))
	--,Discharges = count(*)
from 
	[APC].[Encounter] a
	inner join WH.Calendar
	on DischargeDate = TheDate
	
where
	DischargeDate between @start and @end
	and LastEpisodeInSpellIndicator = 'Y'
	and CodingCompleteDate is not null
  	and floor(datediff(day, DateOfBirth, AdmissionTime) / 365.25) >= 16
  	
	and exists 
			(
			select 
				1
			from 
				[APC].[Encounter] b
			where 
				(
				PrimaryOperationCode like 'W37%'
				or PrimaryOperationCode like 'W38%'
				or PrimaryOperationCode like 'W39%'
				or PrimaryOperationCode like 'W40%'
				or PrimaryOperationCode like 'W41%'
				or PrimaryOperationCode like 'W42%'
				)
				and a.ProviderSpellNo = b.ProviderSpellNo
			)
--group by 
--	TheMonth
	
--order by
--	[Month]
	
end

else if @proc = 'CABG'

begin

select
	SourcePatientNo
	,AdmissionDate
	,DischargeDate
	,LoS = datediff(day, AdmissionDate, DischargeDate)
	,DischargeMonth = cast(TheMonth as datetime)
	--,AvgLOS = avg(datediff(day, AdmissionDate, DischargeDate))
	--,MinLOS = min(datediff(day, AdmissionDate, DischargeDate))
	--,MaxLOS = max(datediff(day, AdmissionDate, DischargeDate))
	--,Discharges = count(*)
from 
	[APC].[Encounter] a
	inner join WH.Calendar
	on DischargeDate = TheDate

where 

	DischargeDate between @start and @end
	and LastEpisodeInSpellIndicator = 'Y'
	and CodingCompleteDate is not null
	and floor(datediff(day, DateOfBirth, AdmissionTime) / 365.25) >= 16
	
	and exists 
			(
			select 
				1
			from 
				[APC].[Encounter] b
			where 
				(
				PrimaryOperationCode like 'K40%'
				or PrimaryOperationCode like 'K41%'
				or PrimaryOperationCode like 'K42%'
				or PrimaryOperationCode like 'K43%'
				or PrimaryOperationCode like 'K44%'
				or PrimaryOperationCode like 'K45%'
				)
				and a.ProviderSpellNo = b.ProviderSpellNo
			)
  
--group by 
--	TheMonth
	
--order by
--	[Month]

end


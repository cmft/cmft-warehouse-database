﻿






CREATE PROCEDURE [RPT].[HomeFromHospitalAE] 

  @FromDate    Date
 ,@ToDate      Date

AS  


--DECLARE @FromDate AS DATE        = '23-OCT-2013'
--DECLARE @ToDate   AS DATE        = '23-OCT-2013'

SELECT 
  'AE'                                                   AS DataSource
 ,CONVERT(varchar,Attendance_View.[atd_id])              AS ProviderSpellNo
 ,pat_details_view.[pat_pid]                             AS DistrictNo
 ,pat_details_view.[pat_surname]                         AS PatientSurname
 ,pat_details_view.[pat_forename]                        AS PatientForename
 ,CONVERT(varchar,pat_details_view.[pat_dob],103)        AS DateOfBirth 
 ,' '                                                    AS NHSNumber
 ,Attendance_View.[Episode_No]                           AS CasenoteNumber 
 ,pat_details_view.[add_line1]                           AS PatientAddress1
 ,pat_details_view.[add_line2]                           AS PatientAddress2
 ,pat_details_view.[add_line3]                           AS PatientAddress3
 ,pat_details_view.[add_line4]                           AS PatientAddress4
 ,pat_details_view.[add_postcode]                        AS PatientPostCode
 ,Attendance_View.[pcg]                                  AS CCgCode
 ,ISNULL(Telephone.[tel_home],'')                        AS PatientHomePhone
 ,ISNULL(Telephone.[tel_mobile],'')                      AS PatientMobilePhone
 ,FLOOR(DATEDIFF(d,pat_details_view.[pat_dob],Attendance_View.[Date_Discharged])/365.25)      AS DischargeAge
 ,LEFT(pat_details_view.[Sex],1)                         AS SexCode
 ,CONVERT(varchar,Attendance_View.[Arrival_date],103)    AS AdmissionDate
 ,CONVERT(varchar,Attendance_View.[Date_Discharged],103) AS DischargeDate
 ,999                                                    AS DischargeDateID
 ,SiteCode =
  CASE LEFT(Attendance_View.[Episode_No],1)
    WHEN 'M' THEN 'MRI'
    WHEN 'E' THEN 'REH'
    ELSE 'OTHER'
  END  
 ,'NE'                                                   AS PatientCategoryCode  
 ,'AE'                                                   AS AdmissionMethodCode
 ,'AE'                                                   AS Specialty 
 ,'AE'                                                   AS EndWard  
 ,Attendance_View.[Discharge_Destination]                AS DischargeDestination
 ,[Discharge_Outcome]

--,Attendance_View.[Discharge_Outcome]
 --,Attendance_View.[atd_epdid]
 --,Telephone.[tel_linkid]
--,Attendance_View.[epd_pid]

FROM [CMMC_Reports].[dbo].[Attendance_View]      AS Attendance_View

JOIN [CMMC_Reports].[dbo].[pat_details_view]     AS pat_details_view
  ON Attendance_View.[epd_pid] = pat_details_view.[pat_pid]

LEFT OUTER JOIN [CMMC_Reports].[dbo].[Telephone] AS Telephone
  ON Attendance_View.[epd_pid] = Telephone.[tel_linkid]

WHERE Attendance_View.[Date_Discharged] BETWEEN @FromDate AND DATEADD(d,1,@ToDate)
AND LEFT(Attendance_View.[Discharge_Outcome],10) = 'Discharged'
AND FLOOR(DATEDIFF(d,pat_details_view.[pat_dob],Attendance_View.[Date_Discharged])/365.25) > 59
AND Attendance_View.[pcg] = '5NT'

  



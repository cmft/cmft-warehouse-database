﻿










CREATE PROCEDURE [RPT].[HomeFromHospitalIP] 

  @FromDate    Date
 ,@ToDate      Date

AS  


--DECLARE @FromDate AS DATE        = '10-AUG-2013'
--DECLARE @ToDate   AS DATE        = '14-AUG-2013'

SELECT 
 'IP'                                                                    AS DataSource
 ,Encounter.[ProviderSpellNo]
 ,Encounter.[DistrictNo]
 ,Encounter.[PatientSurname] 
 ,Encounter.[PatientForename]
 ,CONVERT(varchar,Encounter.[DateOfBirth],103)                           AS DateOfBirth
 ,Encounter.[NHSNumber]
 ,Encounter.[CasenoteNumber]
 ,ISNULL(Encounter.[PatientAddress1],'')                                 AS PatientAddress1
 ,ISNULL(Encounter.[PatientAddress2],'')                                 AS PatientAddress2
 ,ISNULL(Encounter.[PatientAddress3],'')                                 AS PatientAddress3
 ,ISNULL(Encounter.[PatientAddress4],'')                                 AS PatientAddress4
 ,ISNULL(Encounter.[Postcode],'')                                        AS PatientPostCode
 ,Encounter.[CCgCode]                                      
 ,ISNULL(PatientBase.[PtHomePhone],'')                                   AS PatientHomePhone
 ,ISNULL(PatientBase.[PtMobilePhone],'')                                 AS PatientMobilePhone
 ,FLOOR(DATEDIFF(d,Encounter.[DateOfBirth],[AdmissionDate])/365.25)      AS DischargeAge
 ,Encounter.[SexCode]
 ,CONVERT(varchar,Encounter.[AdmissionDate],103)                         AS AdmissionDate 
 ,CONVERT(varchar,Encounter.[DischargeDate],103)                         AS DischargeDate 
 ,Encounter.[DischargeDateID]
 ,Encounter.[SiteCode]
 ,Encounter.[PatientCategoryCode]
 ,Encounter.[AdmissionMethodCode]
 ,Specialty.[SourceSpecialtyCode] + ' - ' + Specialty.[SourceSpecialty]  AS Specialty
 ,Ward.[SourceWardCode]                                                  AS EndWard
 ,DischargeDestination.[DischargeDestination]
  
FROM [WarehouseReportingMerged].[APC].[Encounter]                   AS Encounter

INNER JOIN [WarehouseReportingMerged].[WH].[Calendar]               AS DischDate
   ON Encounter.[DischargeDateID] = DischDate.[DateID]
   
LEFT OUTER JOIN [Warehouse].[PAS].[PatientBase]                     AS PatientBase  
  ON Encounter.[DistrictNo] = PatientBase.[DistrictNumber]

LEFT OUTER JOIN [WarehouseReportingMerged].[WH].[Ward]              AS Ward
  ON Encounter.[EndWardID] = Ward.[SourceWardID]

LEFT OUTER JOIN [WarehouseReportingMerged].[WH].[Specialty]         AS Specialty
  ON Encounter.[SpecialtyID] = Specialty.[SourceSpecialtyID]
  
LEFT OUTER JOIN [WarehouseOLAPMergedV2].[WH].[DischargeDestination] AS DischargeDestination
  ON Encounter.[DischargeDestinationID] = DischargeDestination.[SourceValueID]
  
WHERE FLOOR(DATEDIFF(d,[DateOfBirth],[AdmissionDate])/365.25)  > 59
  AND Encounter.[DateOfDeath] IS NULL
  AND (
       LEFT(Specialty.[SourceSpecialtyCode],3) = 'UAM'
       OR
       Ward.[SourceWardCode] IN('AM1', 'AM2', 'AM3')
       )
  AND DischargeDestination.[DischargeDestinationID] 
    NOT IN ('2000662','9421','9427','9432','9433','NH','LA','NH','NN','NR','NS',',ZP','ZZ','79-ZZ')
  AND [CCgCode]  IN('00W','01M','01N')
  AND Encounter.[NationalLastEpisodeInSpellIndicator] = 1
  AND DischDate.[TheDate] BETWEEN @FromDate AND @ToDate
  
ORDER BY [DischargeDate],Encounter.[PatientSurname],Encounter.[PatientForename]
  
  







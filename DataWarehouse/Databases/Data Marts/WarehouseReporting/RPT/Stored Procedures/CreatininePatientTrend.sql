﻿
CREATE PROCEDURE [RPT].[CreatininePatientTrend] 
(
	 @ReportType varchar(10) -- RATE, CHANGE or VALUE
	,@RateOfChange int
	,@PercentageChange numeric(21 , 10)
	--,@FromPercentageChange numeric(21 , 10)
	--,@ToPercentageChange numeric(21 , 10)
	,@ResultValue int
	,@Interval numeric(21 , 10)
)

as

----debug start
--DECLARE @RC int
--DECLARE @ReportType varchar(10)
--DECLARE @RateOfChange int
--DECLARE @PercentageChange numeric(21,10)
--DECLARE @ResultValue int
--DECLARE @Summary bit
--DECLARE @DistrictNo char(8)
--DECLARE @Interval numeric(21,10)
--DECLARE @Census date


--select
--	 @ReportType = 'CHANGE'
--	,@RateOfChange = null
--	,@PercentageChange = '1.25'
--	,@ResultValue = '300'
--	,@Summary = 1
--	,@DistrictNo = null
--	,@Interval = 0.5
--	,@Census = '1 May 2012'
----debug end


set dateformat dmy

	
select
	 APCEncounter.EncounterRecno
	,APCEncounter.DischargeDate

	,DischargeMonth =
		Calendar.TheMonth

	,DischargeMonthKey =
		Calendar.FinancialMonthKey

	,APCEncounter.LOS
	,Discharges = 1

	,Deaths = 
		case
		when APCEncounter.DischargeMethodCode in ( 'DN' , 'DP' , 'SP' , 'ST' ) then 1
		else 0
		end
from
	(
	SELECT
		-- Result.PatientForename
		--,Result.PatientSurname
		--,Result.DistrictNo
		--,AgeCode = Age.Age
		--,Ethnicity = EthnicOrigin.NationalEthnicOrigin
		--,Sex = Result.SexCode
		--,Result.AdmissionDate
		--,Consultant.Consultant
		--,Specialty.Specialty
		--,Result.WardCode
		 Result.DistrictNo
		,Result.Result
		,Result.ResultDate
		,Result.ResultTime
		,Result.SequenceNoPatient
		,Result.PreviousResult
		,Result.PreviousResultTime
		,Result.ResultIntervalDays
		,Result.ResultChange
		,Result.CurrentToPreviousResultRatio
		,Result.ResultRateOfChangePer24Hr
		,Result.CurrentInpatient
		,ReportSequenceNo =
			case
			when @ReportType = 'RATE' then row_number () over (partition by Result.DistrictNo order by Result.ResultRateOfChangePer24Hr desc)
			when @ReportType = 'CHANGE' then  row_number () over (partition by Result.DistrictNo,Result.MergeEpisodeNo order by Result.CurrentToBaselineResultRatio desc)
			when @ReportType = 'LATEST' then  row_number () over (partition by Result.DistrictNo order by Result.ResultTime desc)
			when @ReportType = 'VALUE' then  row_number () over (partition by Result.DistrictNo,Result.MergeEpisodeNo order by Result.Result desc)
			end
	FROM
		WarehouseReporting.OCM.CodedResult Result

	--left join WarehouseReporting.PAS.Consultant
	--on	Consultant.ConsultantCode = Result.ConsultantCode

	--left join WarehouseReporting.PAS.Specialty
	--on	Specialty.SpecialtyCode = Result.SpecialtyCode

	--LEFT JOIN WarehouseReporting.PAS.EthnicOrigin
	--ON	EthnicOrigin.EthnicOriginCode = Result.EthnicOriginCode

	--left join WarehouseReporting.WH.Age
	--on	Age.AgeCode = Result.AgeCode collate Latin1_General_CI_AS

	where
		Result.CurrentInpatient = 0
	) Result

inner join WarehouseReporting.APC.Encounter APCEncounter
on	APCEncounter.DistrictNo = Result.DistrictNo
and	APCEncounter.LastEpisodeInSpellIndicator = 'Y'

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = APCEncounter.DischargeDate

WHERE 
	(
		APCEncounter.DischargeDate >= '1 Apr 2009'
	and	(
			(
				@ReportType = 'RATE'
			and	Result.ReportSequenceNo = 1
			and	Result.ResultRateOfChangePer24Hr >= @RateOfChange
			and Result.ResultIntervalDays >= @Interval
			)
		or	(
				@ReportType = 'CHANGE'
			-- 20140811 RR - PH wants to be able to select within a cohort so added a To
			--V1
			and	Result.CurrentToPreviousResultRatio >= @PercentageChange
			-- 20140905 RR - following discussion reverted back to original
			--and	
			--	(Result.CurrentToPreviousResultRatio between @FromPercentageChange and @ToPercentageChange
			--	or
			--		(Result.CurrentToPreviousResultRatio >= @FromPercentageChange 
			--		and @ToPercentageChange is null
			--		)
			--	)
			
			and	Result.ReportSequenceNo = 1
			and Result.ResultIntervalDays >= @Interval
			)
		or	(
				@ReportType = 'VALUE'
			and	Result.ReportSequenceNo = 1
			and	Result.Result >= @ResultValue
			)
		or	(
				@ReportType = 'LATEST'
			and	Result.ReportSequenceNo = 1
			and	Result.Result >= @ResultValue
			)
		)
	)



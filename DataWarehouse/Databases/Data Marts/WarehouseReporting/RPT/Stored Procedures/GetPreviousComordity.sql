﻿



CREATE proc [RPT].[GetPreviousComordity]
(
@sourcepatientno int
,@episodestarttime datetime
)

as


select
	PatientName = encounter.PatientSurname + ', ' + encounter.PatientForename
	,encounter.NHSNumber
	,encounter.EpisodeStartTime
	,PreviousEpisodeStartTime = previousencounter.EpisodeStartTime
	,Diagnosis.Diagnosis
		
from
	APC.Encounter encounter

cross apply

	(
		select
			EpisodeStartTime
			,Code
		from
			APC.Encounter
		unpivot
			(
			Code for [Sequence] in 
			(
			--PrimaryDiagnosisCode
			--,SubsidiaryDiagnosisCode
			SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			)
			) previousdiagnosis
			
		inner join WH.Diagnosis Diagnosis
		on	previousdiagnosis.Code = Diagnosis.DiagnosisCode
		and Diagnosis.IsCharlsonFlag = 1
		
		where
			previousdiagnosis.SourcePatientNo = encounter.SourcePatientNo
		and previousdiagnosis.EpisodeStartTime < encounter.EpisodeStartTime
		and previousdiagnosis.Code not in 
										(
											select 
												Code
											from
												APC.Encounter
												
											unpivot
												(
												Code for [Sequence] in 
												(
												--PrimaryDiagnosisCode
												--,SubsidiaryDiagnosisCode
												SecondaryDiagnosisCode1
												,SecondaryDiagnosisCode2
												,SecondaryDiagnosisCode3
												,SecondaryDiagnosisCode4
												,SecondaryDiagnosisCode5
												,SecondaryDiagnosisCode6
												,SecondaryDiagnosisCode7
												,SecondaryDiagnosisCode8
												,SecondaryDiagnosisCode9
												,SecondaryDiagnosisCode10
												,SecondaryDiagnosisCode11
												,SecondaryDiagnosisCode12
												)
												) currentdiagnosis
												
												where 
													currentdiagnosis.SourcePatientNo = encounter.SourcePatientNo
												and currentdiagnosis.EpisodeStartTime = encounter.EpisodeStartTime
												)
																		
										) previousencounter
										
inner join WH.Diagnosis Diagnosis
on previousencounter.Code = Diagnosis.DiagnosisCode

												
where 
	encounter.SourcePatientNo = @sourcepatientno
	and encounter.EpisodeStartTime = @episodestarttime





﻿

CREATE procedure [RPT].[AcuteMedMajorTamPerformanceExtract]

@startDate datetime = null
,@endDate datetime = null
,@PatientType varchar(5) = null

as

WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)					

select
	 SourcePatientID 	
	,CaseNoteNumber			
	,AENumber 		
	,PatientIdentifier			
	,NHSNumber			
	,Forename 
	,Surname
	,DateOfBirth 
	,TraumaStartTime 
	,PatientStatus
	,HospitalArrivalTime
	,TraumaTeamLeaderGrade
	,PreAlert 
	,TraumaTeamAlert
	,SeenTime
	,SeenBy
	,SeenByGrade
	,SeenByType
	,SeenWithinMinutes = 
		datediff(MINUTE, TraumaStartTime, SeenTime)
	,SeenWithin5mins = 
		case	
			when datediff(MINUTE, TraumaStartTime,SeenTime) <5 then 'Y'
			when datediff(MINUTE, TraumaStartTime,SeenTime) >=5 then 'N'
			else null
		end	
	,SeenWithin30mins = 
		case	
			when datediff(MINUTE, TraumaStartTime,SeenTime) <30 then 'Y'
			when datediff(MINUTE, TraumaStartTime,SeenTime) >=30 then 'N'
			else null
		end	
	,SeenByConsultantTime
	,SeenByConsultant
	,SeenByConsultantMinutes = 
		datediff(MINUTE, TraumaStartTime,SeenByConsultantTime)
	,SeenByConsultantWithin5mins = 
		case	
			when datediff(MINUTE, TraumaStartTime,SeenByConsultantTime) <5 then 'Y'
			when datediff(MINUTE, TraumaStartTime,SeenByConsultantTime) >=5 then 'N'
			else null
		end	
	,SeenByConsultantWithin30mins = 
		case	
			when datediff(MINUTE, TraumaStartTime,SeenByConsultantTime) <30 then 'Y'
			when datediff(MINUTE, TraumaStartTime,SeenByConsultantTime) >=30 then 'N'
			else null
		end	
	,CTScanRequired
	,CTScanTime
	,CTScanMinutes =
		datediff(MINUTE, TraumaStartTime,CTScanTime)
	,CTScanWithin60mins = 
		case	
			when datediff(MINUTE, TraumaStartTime,CTScanTime) <60 then 'Y'
			when datediff(MINUTE, TraumaStartTime,CTScanTime) >=60 then 'N'
			else null
		end	
	,TranexamicAcidRequired
	,TranexamicAcidTime
	,TranexamicAcidMinutes = 
		datediff(MINUTE, TraumaStartTime,TranexamicAcidTime)
	,TranexamicAcidWithin3Hours =			
		case	
			when datediff(MINUTE, TraumaStartTime,TranexamicAcidTime) <180 then 'Y'
			when datediff(MINUTE, TraumaStartTime,TranexamicAcidTime) >=180 then 'N'
			else null
		end	
	,RehabPrescriptionRequired
	,RehabPrescriptionCompletedWithin48Hours
	,CandidatePositive
	,Level1BPT 		
	,Level2BPT 			
	,Comment 	
	,IncidentInjuryType
	,InjuryMechanism
from
(				
			
select 	
	 SourcePatientID = 
		Tarncandidate.CandidateID		
	,CaseNoteNumber = 
		Tarncandidate.CaseNoteNumber			
	,AENumber = 
		Tarncandidate.AENumber			
	,PatientIdentifier = 
		Tarncandidate.DistrictNumber			
	,NHSNumber = 
		Tarncandidate.NHSNumber			
	,Forename = 
		upper(left(Tarncandidate.Forenames	,1)) +  lower(right(Tarncandidate.Forenames	,len(Tarncandidate.Forenames	)-1))
	,Surname = 
		upper(left(Tarncandidate.Surname,1)) +  lower(right(Tarncandidate.Surname	,len(Tarncandidate.Surname	)-1))
	,DateOfBirth = 
		DOB		
	,TraumaStartTime = 
		case
		when  FirstAttendant.PatientSeenTime < Tarncandidate.DateHospitalArrival then FirstAttendant.PatientSeenTime
		else Tarncandidate.DateHospitalArrival
		end
	,PatientStatus = 
		Patientstatus.StatusDesc
	,HospitalArrivalTime = 
		Tarncandidate.DateHospitalArrival	
	,TraumaTeamLeaderGrade =		
		TraumaTeamLeaderGrade.Description
	,PreAlert = 
		EDOpeningSection.PreAlert
	,TraumaTeamAlert = 
		EDOpeningSection.TraumaTeamAlert
	,SeenTime =		
		FirstAttendant.PatientSeenTime
	,SeenBy =		
		replace(replace(FirstAttendant.OtherNotes,char(13),''),char(10),'')
	,SeenByGrade = 
		SeenByGrade.Description
	,SeenByType = 
		SeenByType.Description
	
	,SeenByConsultantTime =		
		FirstConsultantAttendant.PatientSeenTime	
	,SeenByConsultant =		
		replace(replace(FirstConsultantAttendant.OtherNotes,char(13),''),char(10),'')
	,CTScanRequired = 
		case
		when Tarncandidate.StatusID in(3) then 'N'
		when ImagingOpeningSection.ConsultantDecisionCTScanNotRequired is null then 'NR'
		else 
			case 
			when ImagingOpeningSection.ConsultantDecisionCTScanNotRequired = 'Y' then 'N'
			when ImagingOpeningSection.ConsultantDecisionCTScanNotRequired = 'N' then 'Y'
			when ImagingOpeningSection.ConsultantDecisionCTScanNotRequired = 'ND' then 'Y'
			else 'N'
			end
		end
	,CTScanTime =
		CTScan.CTScanSeenTime
	,TranexamicAcidRequired = 
		case
		when Tarncandidate.StatusID in(3) then 'N'
		when TranexamicAcid.TranexamicAcid is null then 'NR'
		else TranexamicAcid.TranexamicAcid
		end

	,TranexamicAcidTime = 
		TranexamicAcid.TranexamicAcidTime
	,RehabPrescriptionRequired = 
		RehabPrescription.RehabPrescriptionRequired
	,RehabPrescriptionCompletedWithin48Hours = 
		RehabPrescription.RehabPrescriptionCompletedWithin48Hours
	,CandidatePositive = 
		null
	,Level1BPT = Null			
	,Level2BPT = Null			
	,Comment = null			
	,IncidentInjuryType = 
		case 
		when Incident.IncidentInjuryType= -1 then 'Not Specified'
		else IncidentInjuryType.Description
		end
	,InjuryMechanism =
		case 
		when Incident.IncidentInjuryMechanism = -1 then 'Not Specified'
		else IncidentInjuryMechanism.Description
		end

	
from 
	majortam.dbo.tarncandidate Tarncandidate
		
inner join majortam.dbo.[Status] Patientstatus
on Patientstatus.StatusID = Tarncandidate.StatusID


left outer join
(
select
	 ID
	,CandidateID
	,sectionID
	,actionID
	,ConsultantDecisionCTScanNotRequired = 
		encounter.details.value('(/ImagingOpeningSectionDTO/ConsultantDecisionCTScanNotRequired)[1]', 'varchar(20)') 
from
	MAJORTAM.dbo.TARNCandidateData	encounter
where
	encounter.SectionID = 30
)ImagingOpeningSection
on ImagingOpeningSection.CandidateID = Tarncandidate.CandidateID



left outer join
(
select
	 ID
	,CandidateID
	,sectionID
	,actionID
	,RehabPrescriptionRequired = 
		case 
		when encounter.details.value('(/AtDischargeDTO/RehabPrescriptionRequired)[1]', 'varchar(20)')  = '' then null 
		else encounter.details.value('(/AtDischargeDTO/RehabPrescriptionRequired)[1]', 'varchar(20)') 
		end
	,RehabPrescriptionCompletedWithin48Hours= 
		case 
		when encounter.details.value('(/AtDischargeDTO/RehabPrescriptionCompletedWithin48Hours)[1]', 'varchar(20)') = '' then null 
		else encounter.details.value('(/AtDischargeDTO/RehabPrescriptionCompletedWithin48Hours)[1]', 'varchar(20)') 
		end

from
	MAJORTAM.dbo.TARNCandidateData	encounter
where
	encounter.SectionID = 4
and encounter.ActionID = 99
)RehabPrescription
on RehabPrescription.CandidateID = Tarncandidate.CandidateID

outer apply
(
	select top 1
		 ID
		,CandidateID
		,sectionID
		,actionID
		,PatientSeenTime = 
			nullif(
				convert(datetime,left(encounter.details.value('(/AttendantDTO/PatientSeenDate)[1]', 'varchar(max)'),10) 
				+ ' ' 
				+ encounter.details.value('(/AttendantDTO/PatientSeenTime)[1]', 'varchar(20)') )
			,' ' 
			)
		,AttendantGrade = 
			encounter.details.value('(/AttendantDTO/AttendantGrade)[1]', 'varchar(20)')
		,AttendantType = 
			encounter.details.value('(/AttendantDTO/AttendantType)[1]', 'varchar(20)') 
		,OtherNotes	= 
			encounter.details.value('(/AttendantDTO/OtherNotes)[1]', 'varchar(20)') 
	from
		MAJORTAM.dbo.TARNCandidateData	encounter	
	where
		encounter.ActionID = 9
	and encounter.SectionID = 1

	and encounter.CandidateID = Tarncandidate.CandidateID
	and encounter.details.value('(/AttendantDTO/PatientSeenTime)[1]', 'varchar(20)') is not null
	order by 
		 PatientSeenTime 
		,DateTimeCreated 
)FirstAttendant

outer apply
(
	select top 1
		 ID
		,CandidateID
		,sectionID
		,actionID
		,PatientSeenTime = 
			nullif(
				convert(datetime,left(encounter.details.value('(/AttendantDTO/PatientSeenDate)[1]', 'varchar(max)'),10) 
				+ ' ' 
				+ encounter.details.value('(/AttendantDTO/PatientSeenTime)[1]', 'varchar(20)') )
			,' ' 
			)
		,AttendantGrade = 
			encounter.details.value('(/AttendantDTO/AttendantGrade)[1]', 'varchar(20)')
		,AttendantType = 
			encounter.details.value('(/AttendantDTO/AttendantType)[1]', 'varchar(20)') 
		,OtherNotes	= 
			encounter.details.value('(/AttendantDTO/OtherNotes)[1]', 'varchar(20)') 
	from
		MAJORTAM.dbo.TARNCandidateData	encounter	
	where
		encounter.ActionID = 9
	and encounter.CandidateID = Tarncandidate.CandidateID
	and encounter.details.value('(/AttendantDTO/AttendantGrade)[1]', 'varchar(20)') = 1		
	and encounter.details.value('(/AttendantDTO/PatientSeenTime)[1]', 'varchar(20)') is not null
	order by 
		 PatientSeenTime 
		,DateTimeCreated 
)FirstConsultantAttendant


outer apply
(
	select top 1
		 ID
		,CandidateID
		,sectionID
		,actionID
		,CTScanSeenTime = 
			nullif(
				convert(datetime,left(encounter.details.value('(/ImagingDTO/ImageDate)[1]', 'varchar(max)'),10) 
				+ ' ' 
				+ encounter.details.value('(/ImagingDTO/ImageTime)[1]', 'varchar(20)') )
			,' ' 
			)
	from
		MAJORTAM.dbo.TARNCandidateData	encounter	
	where
		encounter.SectionID = 3
	and encounter.ActionID = 5
	and encounter.CandidateID = Tarncandidate.CandidateID
	order by 
		 CTScanSeenTime 
		,DateTimeCreated 
)CTScan


outer apply
(
	select top 1
		 ID
		,CandidateID
		,sectionID
		,actionID
		,IncidentInjuryType = 
			encounter.details.value('(/IncidentDTO/InjuryType)[1]', 'varchar(max)')
		,IncidentInjuryMechanism = 
			encounter.details.value('(/IncidentDTO/InjuryMechanism)[1]', 'varchar(max)')	
	from
		MAJORTAM.dbo.TARNCandidateData	encounter	
	where
		encounter.SectionID = 9
	and encounter.ActionID = 99
	and encounter.CandidateID = Tarncandidate.CandidateID
	order by 
		 DateTimeCreated 
)Incident


outer apply
(
	select top 1
		TranexamicAcid = 
			TARNCandidateData.details.value('(/InterventionsDTO/TranexamicAcid)[1]', 'varchar(20)')
		,TranexamicAcidTime = 
			nullif(
				convert(datetime,left(TARNCandidateData.details.value('(/InterventionsDTO/TranexamicAcidDate)[1]', 'varchar(max)'),10) 
				+ ' ' 
				+ TARNCandidateData.details.value('(/InterventionsDTO/TranexamicAcidTime)[1]', 'varchar(20)') )
			,' ' 
			)
	from
		MAJORTAM.dbo.TARNCandidateData 			
	where
		TARNCandidateData.ActionID = 2 		
	and TARNCandidateData.CandidateID = Tarncandidate.CandidateID		
	and TARNCandidateData.details.value('(/InterventionsDTO/TranexamicAcid)[1]', 'varchar(20)') in ('Y','N')	
	order by 
		 TranexamicAcid desc
		,TranexamicAcidTime
		,DateTimeCreated 		
)TranexamicAcid	

left outer join
(
	select 
	 ID
	,CandidateID
	,sectionID
	,actionID
	,PreAlert = 
		encounter.details.value('(/EDOpeningSectionDTO/PreAlertTriageTool)[1]', 'varchar(20)') 
	,TraumaTeamAlert = 
		encounter.details.value('(/EDOpeningSectionDTO/TraumaTeamActivated)[1]', 'varchar(20)') 
	,TraumaTeamLeaderGrade = 
		encounter.details.value('(/EDOpeningSectionDTO/TraumaTeamLeaderGrade)[1]', 'varchar(20)') 
	
	from
		MAJORTAM.dbo.TARNCandidateData	encounter	
	where
		encounter.sectionid = 10
	and encounter.actionID = 99
)EDOpeningSection
on  EDOpeningSection.CandidateID = Tarncandidate.CandidateID

left outer join MAJORTAM.dbo.TraumaTeamLeaderGrade
on TraumaTeamLeaderGrade.ID = EDOpeningSection.TraumaTeamLeaderGrade

left outer join MAJORTAM.dbo.AttendantGrade SeenByGrade
on SeenByGrade.ID = FirstAttendant.AttendantGrade

left outer join MAJORTAM.dbo.AttendantType SeenByType
on SeenByType.ID = FirstAttendant.AttendantType

left outer join [MAJORTAM].[dbo].[IncidentInjuryType]
on IncidentInjuryType.ID = Incident.IncidentInjuryType

left outer join [MAJORTAM].[dbo].[IncidentInjuryMechanism]
on IncidentInjuryMechanism.ID = Incident.IncidentInjuryMechanism
) encounter
where

 --TraumaStartTime between '01 mar 2013' and getdate ()
 	convert(date,TraumaStartTime) >= @startDate
and convert(date,TraumaStartTime) <= @endDate 
and 
	(
		@PatientType is null
	or
		@PatientType  = 
				case
				when cast(datediff(DAY, encounter.DateOfBirth, encounter.TraumaStartTime) / (365.23076923074) as int) <16 then 'Child'
				else 'Adult'
				end
	)
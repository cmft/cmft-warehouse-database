﻿
create proc RPT.[MedisecPatients_Get]
(	
	@start datetime = null
	,@end datetime = null
	,@division varchar(50) = null
	,@specialty varchar(50) = null
	,@consultant varchar(20) = null
	,@medisecuser varchar(20) = null

	,@letterrequired varchar(20) = null		
	,@letterproduced varchar(20) = null
	,@priortodischarge varchar(20) = null
	,@within24hours varchar(20) = null
	,@between24and48hours varchar(20) = null
	,@over48hours varchar(20) = null
	,@breach varchar(20) = null
	
	,@dnfproduced varchar(20) = null
	,@admissionreason varchar(20) = null
	,@diagnosis varchar(20) = null
	,@investigation varchar(20) = null
	,@recommendation varchar(20) = null
	,@information varchar(20) = null
	,@medication varchar(20) = null
	
)


as

set dateformat dmy

set @start = coalesce(@start, '01/04/2011')
set @end = coalesce(@end, '30/04/2011')

set @specialty = coalesce(@specialty, '(Select All)')
set @division = coalesce(@division, '(Select All)')
set @consultant = coalesce(@consultant, '(Select All)')

set @medisecuser = coalesce(@medisecuser, '(Select All)')

set @letterrequired = coalesce(@letterrequired, '(Select All)')
set @letterproduced = coalesce(@letterproduced, '(Select All)')
set @priortodischarge = coalesce(@priortodischarge, '(Select All)')
set @within24hours = coalesce(@within24hours, '(Select All)')
set @between24and48hours = coalesce(@between24and48hours, '(Select All)')
set @over48hours = coalesce(@over48hours, '(Select All)')
set @breach = coalesce(@breach, '(Select All)')

set @dnfproduced = coalesce(@dnfproduced, '(Select All)')
set @admissionreason = coalesce(@admissionreason, '(Select All)')
set @diagnosis = coalesce(@diagnosis, '(Select All)')
set @investigation = coalesce(@investigation, '(Select All)')
set @recommendation = coalesce(@recommendation, '(Select All)')
set @information = coalesce(@information, '(Select All)')
set @medication = coalesce(@medication, '(Select All)')


select
	dischargesummary.*
	,DocumentType = 
		coalesce(doctype.DocumentType, 'N/A')
	,[Status] = 
		coalesce(docstatus.[Status], 'N/A')
	,DocumentProgress = 
		coalesce(docprogress.DocumentProgress, 'N/A')
	,BreachStatus = 
					case
						when dischargesummary.StatusCode  not in ('09') 
							and coalesce(dischargesummary.DocumentProgressCode, '') not in ('N','X')
							and RequestOrderForDischarge = 1
							and DocumentSentGPForDischarge = 0
						then 'No Letter to GP'
						when datediff(minute, DischargeTime, SignedTime) / 60 >= 24
							and dischargesummary.StatusCode  not in ('09') 
							and dischargesummary.DocumentProgressCode in ('G','P')
							and IsFirstDocumentForDischarge = 1
						then 'Letter not in time'
						when datediff(minute, DischargeTime, SignedTime) / 60 < 24
							and dischargesummary.StatusCode  not in ('09') 
							and dischargesummary.DocumentProgressCode in ('G','P')
							and IsFirstDocumentForDischarge = 1
						then 'Letter in time'
						else 'Letter not required'
						end
		,division.Division
		,Specialty = Specialty.NationalSpecialty
						
from
	WarehouseOLAP.dbo.BaseDischargeSummary dischargesummary
inner join 
	Warehouse.WH.Directorate directorate
	on directorate.DirectorateCode = dischargesummary.EndDirectorateCode
inner join 
	Warehouse.WH.Division division
	on directorate.DivisionCode = division.DivisionCode
left outer join 
	warehouse.Medisec.DocumentType doctype 
	on dischargesummary.DocumentTypeCode = doctype.DocumentTypeCode
left outer join 
	warehouse.Medisec.[Status] docstatus
	on docstatus.StatusCode  = dischargesummary.StatusCode
left outer join 
	warehouse.Medisec.DocumentProgress docprogress
	on docprogress.DocumentProgressCode  = dischargesummary.DocumentProgressCode
	
left join WarehouseReporting.PAS.Specialty
	on Specialty.SpecialtyCode = dischargesummary.SpecialtyCode

where 
	(dischargesummary.DischargeDate between @start and @end)
	
	and (dischargesummary.NationalSpecialtyCode = @specialty or @specialty = '(Select All)')
	and (dischargesummary.DischargeDivisionCode = @division or @division = '(Select All)')
	and (dischargesummary.ConsultantCode = @consultant or @consultant = '(Select All)')
	and (cast(dischargesummary.RegisteredGpPracticeIsMedisecUser as varchar(10)) = @medisecuser or @medisecuser = '(Select All)')
	
	and (
			case
				when
						dischargesummary.StatusCode not in ('09') 
					and coalesce(dischargesummary.DocumentProgressCode, '') not in ('N','X') 
					and RequestOrderForDischarge = 1 
					then '1'
				end = @letterrequired
				or @letterrequired = '(Select All)'
		)
	
	
	and (
			case
				when
						dischargesummary.StatusCode not in ('09') 
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					then '1'
			end = @letterproduced
			or @letterproduced = '(Select All)'
		)
	
	
	and (
			case
				when
						SignedTime < DischargeTime 
					and dischargesummary.StatusCode not in ('09')
					and dischargesummary.DocumentProgressCode in ('G','P')
					and IsFirstDocumentForDischarge = 1
					then '1'
			end = @priortodischarge
			or @priortodischarge = '(Select All)'
		)
		
	and (
			case
				when 
						datediff(minute, DischargeTime, SignedTime) / 60 < 24 
					and dischargesummary.StatusCode not in ('09')
					and dischargesummary.DocumentProgressCode in ('G','P')
					and IsFirstDocumentForDischarge = 1
				then '1'
			end = @within24hours
			or @within24hours = '(Select All)'
		)
		
	and (
			case
				when
						datediff(minute, DischargeTime, SignedTime) / 60 between 24 and 48
					and dischargesummary.StatusCode not in ('09')
					and dischargesummary.DocumentProgressCode in ('G','P')
					and IsFirstDocumentForDischarge = 1
				then '1'
			end = @between24and48hours
			or @between24and48hours = '(Select All)'
		)
		
	and (
			case
				when	
						datediff(minute, DischargeTime, SignedTime) / 60 > 48
					and dischargesummary.StatusCode not in ('09')
					and dischargesummary.DocumentProgressCode in ('G','P')
					and IsFirstDocumentForDischarge = 1
				then '1'
			end = @over48hours
			or @over48hours = '(Select All)'
		)
		
	and 
		(
		case
			when 
				(
				datediff(minute, DischargeTime, SignedTime) / 60 >= 24
				and dischargesummary.StatusCode not in ('09') 
				and dischargesummary.DocumentProgressCode in ('G','P')
				and IsFirstDocumentForDischarge = 1
				)
				or
				(
				dischargesummary.StatusCode not in ('09') 
				and coalesce(dischargesummary.DocumentProgressCode, '') not in ('N','X')
				and RequestOrderForDischarge = 1
				and DocumentSentGPForDischarge = 0
				)
				then '1'	
			end = @breach
			or @breach = '(Select All)'
		)	
		
	and 
		(
			case
				when 
						dischargesummary.StatusCode not in ('09') 
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					and dischargesummary.DocumentTypeCode in ('F')
				then '1'
			end = @dnfproduced
			or @dnfproduced = '(Select All)'
		)
		
	and
		(
			case
				when 
						[AdmissionReason] <> ''
					and dischargesummary.StatusCode not in ('09')
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					and dischargesummary.DocumentTypeCode in ('F')
					then '1'
			end = @admissionreason
			or @admissionreason = '(Select All)'
		)
		
	and
		(
			case
				when 
						[Diagnosis] <> '' 
					and dischargesummary.StatusCode not in ('09')
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					and dischargesummary.DocumentTypeCode in ('F')
				then '1'
			end = @diagnosis
			or @diagnosis = '(Select All)'
		)
		
	and
		(
			case
				when 
						[Investigations] <> ''
					and dischargesummary.StatusCode not in ('09')
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					and dischargesummary.DocumentTypeCode in ('F')
				then '1'
			end = @investigation
			or @investigation = '(Select All)'
		)
		
	and
		(
			case
				when 
						[RecommendationsOnFutureManagement] <> ''
					and dischargesummary.StatusCode not in ('09')
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					and dischargesummary.DocumentTypeCode in ('F')
				then '1'
			end = @recommendation
			or @recommendation = '(Select All)'
		)
		
	and
		(
			case
				when 
						InformationToPatientOrRelatives <> ''
					and dischargesummary.StatusCode not in ('09')
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					and dischargesummary.DocumentTypeCode in ('F')
				then '1'
			end = @information
			or @information = '(Select All)'
		)
				
	and
		(
			case
				when 	
						[ChangesToMedication] <> ''
					and dischargesummary.StatusCode not in ('09')
					and IsFirstDocumentForDischarge = 1
					and dischargesummary.DocumentProgressCode in ('G','P')
					and dischargesummary.DocumentTypeCode in ('F')	
				then '1'
			end = @medication
			or @medication = '(Select All)'	
		)

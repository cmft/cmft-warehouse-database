﻿
CREATE PROCEDURE [RPT].[GetDNADivision]

AS




	SELECT  
   		 DivisionCode
   		,Division
   		,SortOrder = ROW_NUMBER() over (order by Division)
	from
		WH.Division
	where
		DivisionCode not in ('9' , 'MHT' , 'NTUSE' , 'RSRCH')
	
order by
	SortOrder

﻿CREATE PROCEDURE [RPT].[GetServiceDeskEncounterSummary]
(
 @StartDate Date
,@EndDate Date
,@Metric varchar(max)
,@BreachStatusCode varchar(max)
)

AS

SELECT 
	 SourceUniqueID
	,TheDate
	,Metric
	,ResolvedDurationMinutes
	,ResolvedDurationHours
	,ResolvedDurationDays
	,ResolvedFlag
	,BreachStatusCode
	,BreachStatus
	,BreachValue
	,RequestStatus
	,RequestStatusGroup
	,Category
	,Workgroup
	,Classification
	,AssignedTo
	,Priority
	,PriorityKey
	,CallerOrganization
	,FinancialYear
	,FinancialQuarter
	,FinancialQuarterkey
	,TheMonth
	,FinancialMonthKey
	,RequestAddition
FROM
(
SELECT
	 SourceUniqueID
	,TheDate = Convert(Date,StartTime,113) 
	,Metric = 'Received'
	,ResolvedDurationMinutes
	,ResolvedDurationHours = ceiling(cast(ResolvedDurationMinutes as float)/60)
	,ResolvedDurationDays = ceiling(cast(ResolvedDurationMinutes as float)/1440)
	,ResolvedFlag
	,BreachStatusCode = BreachStatus
	,BreachStatus = 
			CASE BreachStatus
				WHEN 0 THEN 'Not Breached'
				WHEN 1 THEN 'Breached'
				WHEN -1 THEN 'Not Recorded'
			END
	,BreachValue
	,RequestStatus
	,RequestStatusGroup
	,Category
	,Workgroup
	,Classification
	,AssignedTo
	,Priority
	,PriorityKey = 
		CASE Priority
			WHEN '4 Hours' THEN 0
			WHEN '8 Hours' THEN 1
			WHEN '5 Days' THEN 2
			WHEN '2 Weeks' THEN 3
			WHEN '6 Weeks' THEN 4
			ELSE 5
		END
	,CallerOrganization
	,Calendar.FinancialYear
	,Calendar.FinancialQuarter
	,Calendar.FinancialQuarterkey
	,Calendar.TheMonth
	,Calendar.FinancialMonthKey
	,RequestAddition = 1
FROM
	ServiceDesk.Encounter
INNER JOIN WH.Calendar
	ON Calendar.TheDate = Convert(Date,StartTime,113)
WHERE 
	StartTime >= @StartDate
AND StartTime <= @EndDate

UNION

SELECT
	 SourceUniqueID
	,TheDate = Convert(Date,EndTime,113) 
	,Metric = 'Closed'
	,ResolvedDurationMinutes
	,ResolvedDurationHours = ceiling(cast(ResolvedDurationMinutes as float)/60)
	,ResolvedDurationDays = ceiling(cast(ResolvedDurationMinutes as float)/1440)
	,ResolvedFlag
	,BreachStatusCode = BreachStatus
	,BreachStatus = 
			CASE BreachStatus
				WHEN 0 THEN 'Not Breached'
				WHEN 1 THEN 'Breached'
				WHEN -1 THEN 'Not Recorded'
			END
	,BreachValue
	,RequestStatus
	,RequestStatusGroup
	,Category
	,Workgroup
	,Classification
	,AssignedTo
	,Priority
	,PriorityKey = 
		CASE Priority
			WHEN '4 Hours' THEN 0
			WHEN '8 Hours' THEN 1
			WHEN '5 Days' THEN 2
			WHEN '2 Weeks' THEN 3
			WHEN '6 Weeks' THEN 4
			ELSE 5
		END
	,CallerOrganization
	,Calendar.FinancialYear
	,Calendar.FinancialQuarter
	,Calendar.FinancialQuarterkey
	,Calendar.TheMonth
	,Calendar.FinancialMonthKey
	,RequestAddition = -1
FROM
	ServiceDesk.Encounter
INNER JOIN WH.Calendar
	ON Calendar.TheDate = Convert(Date,EndTime,113)
WHERE 
	EndTime >= @StartDate
AND EndTime <= @EndDate
AND RequestStatusGroup = 'Closed'
) encounter
WHERE
	(
			Encounter.Metric IN (SELECT VALUE FROM dbo.SSRS_MultiValueParamSplit(@Metric,','))
		OR 
			@Metric IS NULL
	)	
	AND
	(
			convert(varchar(2),Encounter.BreachStatusCode) IN (SELECT VALUE FROM dbo.SSRS_MultiValueParamSplit(@BreachStatusCode,','))
		OR 
			@BreachStatusCode IS NULL
	)
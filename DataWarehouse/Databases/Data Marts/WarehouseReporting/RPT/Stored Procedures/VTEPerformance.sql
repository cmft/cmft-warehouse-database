﻿/****** Script for SelectTopNRows command from SSMS  ******/

CREATE PROCEDURE [RPT].[VTEPerformance]
(
	 @FinancialMonthKey int
)
as



SELECT
	 Exclusion = VTESpell.VTEExclusionReasonCode
	,DivisionCode = Directorate.DivisionCode
	,Division = Division.Division
	,SpecialtyCode = Specialty.NationalSpecialtyCode
	,Specialty = Specialty.NationalSpecialty
	,WardCode = Ward.WardCode
	,Ward = Ward.Ward
	,ConsultantCode = Encounter.ConsultantCode
	,Consultant = Consultant.Consultant
	,Cases = 1

	,CodedExclusionCases =
		case
		when
				Encounter.CodingCompleteDate IS NOT NULL
			AND VTESpell.VTEExclusionReasonCode is not null 
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
		else 0
		end

	,UncodedExclusionCases =
		case
		when
				Encounter.CodingCompleteDate IS NULL
			AND VTESpell.VTEExclusionReasonCode is not null
			and coalesce(VTESpell.VTECategoryCode , '') != 'C' then 1
		else 0
		end

	,VTECompleteCases = 
		case VTESpell.VTECategoryCode
		when 'C' then 1
		else 0
		end

	,VTEIncompleteCases = 
		case VTESpell.VTECategoryCode
		when 'I' then 1
		else 0
		end

	,VTEMissingCases = 
		case VTESpell.VTECategoryCode
		when 'M' then 1
		else 0
		end
		
	,CodingCompleteCases = 
		CASE
		WHEN Encounter.CodingCompleteDate IS NOT NULL THEN 1
		ELSE 0
		END
		
	,CodingIncompleteCases = 
		CASE
		WHEN Encounter.CodingCompleteDate IS NULL THEN 1
		ELSE 0
		END
FROM
	WarehouseReporting.APC.Encounter

inner join WarehouseReporting.WH.Calendar
on	Calendar.TheDate = Encounter.AdmissionDate

left join WarehouseReporting.APC.VTESpell
on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

left join WarehouseReporting.WH.Directorate
on	Directorate.DirectorateCode = Encounter.StartDirectorateCode

left join WarehouseReporting.WH.Division
on	Division.DivisionCode = Directorate.DivisionCode

left join WarehouseReporting.PAS.Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join WarehouseReporting.PAS.Ward
on	Ward.WardCode = Encounter.StartWardTypeCode

left join WarehouseReporting.PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

where
	Encounter.FirstEpisodeInSpellIndicator = 1
and	Calendar.FinancialMonthKey = @FinancialMonthKey
and	Encounter.PatientCategoryCode != 'RD'
and	(
		(
			Encounter.AgeCode like '%Years'
		and	left(Encounter.AgeCode , 2) > '17'
		)
	or	Encounter.AgeCode = '99+'
	)

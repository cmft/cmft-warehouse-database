﻿
CREATE PROCEDURE [RPT].[GetCOPDTrackingList] 
(
	 @NationalSpecialtyCode varchar(MAX) = NULL
	,@WardCode varchar(MAX) = NULL
	,@PracticeCode varchar(MAX) = NULL
	,@PatientCategoryCode varchar(MAX) = NULL
	,@DateRange  varchar(MAX) = NULL
)

AS

SELECT 
	 ProviderSpellNo
	,AdmissionDate
	,AdmissionTime
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,Postcode
	,Gender = SexCode
	,Ward = COALESCE(EndWard.WardCode + ' - ' + EndWard.Ward
					,StartWard.WardCode + ' - ' + StartWard.Ward
					)
	,NHSNumber
	,CaseNoteNumber
	,DistrictNo
	,PatientCategoryCode
	,ExpectedLOS
	,Specialty = Specialty.Specialty
	,Consultant = Consultant.Consultant
	,RegisteredGPPracticeCode = COALESCE(Practice.Practice
										,RegisteredGPPracticeCode
										)
FROM
	APCUpdate.encounter
INNER JOIN Warehouse.PAS.Consultant Consultant
ON encounter.ConsultantCode = Consultant.ConsultantCode
LEFT OUTER JOIN Warehouse.PAS.Specialty
ON encounter.SpecialtyCode =  Specialty.SpecialtyCode 
LEFT OUTER JOIN Warehouse.PAS.Ward EndWard
ON encounter.EndWardTypeCode = EndWard.WardCode
LEFT OUTER JOIN Warehouse.PAS.Ward StartWard
ON encounter.StartWardTypeCode = StartWard.WardCode
LEFT OUTER JOIN WH.Practice Practice 
ON  encounter.EpisodicGpPracticeCode = Practice.PracticeCode 

WHERE 
	encounter.EpisodeEndDate is null
AND 
	(
		encounter.PatientCategoryCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@PatientCategoryCode,','))
	OR	@PatientCategoryCode IS NULL
	)
AND(
		encounter.EpisodicGpPracticeCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@PracticeCode,','))
	OR	@PracticeCode IS NULL
	)
AND
	(
		encounter.EndWardTypeCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@WardCode,','))
	OR	@WardCode IS NULL
	)
AND
	(
		encounter.SpecialtyCode IN (SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
	OR	@NationalSpecialtyCode IS NULL
	)
AND 
	(
		AdmissionDate >=	CASE @DateRange
								WHEN 'Today' THEN  convert(DATE,getdate())
								WHEN 'Previous 3 days' THEN DATEADD(DAY, -3, convert(DATE,getdate()))
								WHEN 'Previous 7 days' THEN DATEADD(DAY, -7, convert(DATE,getdate()))
								WHEN 'Previous 28 days' THEN DATEADD(DAY, -28, convert(DATE,getdate()))
								WHEN 'Previous 12 months' THEN DATEADD(DAY, -52, convert(DATE,getdate()))
							END
	OR	@DateRange IS NULL
	)

and exists
		(
		select
			1
		from
			WH.PatientFlag
		where
			PatientFlag.DistrictNo = Encounter.DistrictNo
		and PatientFlag.FlagCode = 'COPD'
		)

--and exists
--		(
--		select
--			1
--		from
--			(
--			select
--				DistrictNo
--			from
--				(
--				SELECT --DISTINCT
--					DistrictNo
--				FROM
--					OP.Encounter oe
--				WHERE 
--					oe.ClinicCode IN
--									(
--									 'COPDC'
--									,'COPDCH'
--									,'COPDCR'
--									,'COPDFOL'
--									,'COPDGF'
--									,'COPDGT'
--									,'COPDL'
--									,'COPDLS'
--									,'COPDMS'
--									,'COPDR'
--									,'COPDRH'
--									,'COPDVC'
--									,'COPDVR'
--									,'HOMEOX'
--									,'HOMEV'
--									,'OXYFOL'
--									,'OXYLS'
--									,'OXYMON'
--									,'OXYREV'
--									,'PREHAB'
--									,'PULASS'
--									,'AMBCL'
--									)
--				UNION all
						
--				SELECT --DISTINCT
--					DistrictNo
--				FROM 
--					AE.Encounter aee

--				INNER JOIN AE.Diagnosis aed
--				ON aee.SourceUniqueID = aed.AESourceUniqueID

--				WHERE
--					aed.SourceDiagnosisCode in 
--												(
--												 9992
--												,9793
--												,9642
--												,9554
--												,8610
--												,11785
--												,11786
--												,11486
--												,11487
--												,16508
--												,8262
--												,16929
--												)
--				UNION all

--				SELECT --DISTINCT
--					DistrictNo
--				FROM
--					APC.Encounter apce

--				INNER JOIN APC.Diagnosis apcd
--				ON apce.SourceUniqueID = apcd.APCSourceUniqueID

--				WHERE 
--					apcd.DiagnosisCode IN (
--									 'J44.0' 
--									,'J44.1' 
--									,'J44.8' 
--									,'J44.9' 
--									,'J43.9'
--									)
--				) DistrictNo

--			where
--				DistrictNo.DistrictNo = encounter.DistrictNo

--			) DistrictNoComparison
--		)

		

--AND 
--	DistrictNo IN
--	( 
--	SELECT DISTINCT
--		DistrictNo
--	FROM
--		OP.Encounter oe
--	WHERE 
--		oe.ClinicCode IN( 'COPDC'
--							,'COPDCH'
--							,'COPDCR'
--							,'COPDFOL'
--							,'COPDGF'
--							,'COPDGT'
--							,'COPDL'
--							,'COPDLS'
--							,'COPDMS'
--							,'COPDR'
--							,'COPDRH'
--							,'COPDVC'
--							,'COPDVR'
--							,'HOMEOX'
--							,'HOMEV'
--							,'OXYFOL'
--							,'OXYLS'
--							,'OXYMON'
--							,'OXYREV'
--							,'PREHAB'
--							,'PULASS'
--							,'AMBCL'
--							)
--		UNION		
--			SELECT DISTINCT
--				DistrictNo
--			FROM 
--				AE.Encounter aee
--			INNER JOIN AE.Diagnosis aed
--			ON aee.SourceUniqueID = aed.AESourceUniqueID
--			WHERE aed.SourceDiagnosisCode in (
--													 9992
--													,9793
--													,9642
--													,9554
--													,8610
--													,11785
--													,11786
--													,11486
--													,11487
--													,16508
--													,8262
--													,16929
--													)
--		UNION
--			SELECT DISTINCT
--				DistrictNo
--			FROM
--				APC.Encounter apce
--			INNER JOIN APC.Diagnosis apcd
--			ON apce.SourceUniqueID = apcd.APCSourceUniqueID
--			WHERE 
--				apcd.DiagnosisCode IN (
--								 'J44.0' 
--								,'J44.1' 
--								,'J44.8' 
--								,'J44.9' 
--								,'J43.9'
--								)
			
--			)

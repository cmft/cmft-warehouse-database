﻿
CREATE proc [RPT].[StrokeValidationList]

@FinancialMonthKey INT

as

SELECT
	PCTDescription
	,InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator = CASE WHEN DiagnosisIndicator = 'Primary' then 'Yes' else 'No' end
	,OtherDiagnosisIndicator = 'No'
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,AdmissionTime
	,DischargeTime
	,Numerator = LOSStrokeWard
	,DenominatorIncAE = Denominator + ISNULL(AETime,0)
	,PercentageOnStrokeWardIncAE = ((LOSStrokeWard*1.00)/(Denominator + ISNULL(AETime,0))*1.00)*100
	,myqry.TheMonth
FROM (
					SELECT 
						PCTDescription = PCT.[PCT Description]
						,InternalNo = Encounter.SourcePatientNo
						,EpisodeNo = Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard = ISNULL(LOSStrokeWard,0)
						,Denominator = SUM(DATEDIFF(mi,Encounter.EpisodeStartTime,Encounter.EpisodeEndTime))
						,AETime = TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth

					FROM 
						WarehouseReporting.apc.Encounter Encounter

					LEFT JOIN Information.dbo.PCT PCT 
					ON PCT.[PCT Code] = Encounter.PCTCode

					--LOS on Stroke Ward
					LEFT JOIN (		SELECT 
										ProviderSpellNo,
										LOSStrokeWard = SUM(DATEDIFF(mi,WardStay.[StartTime],WardStay.EndTime)) 
									FROM 
										[WarehouseReporting].[APC].[WardStay] 
									WHERE 
										[WardCode] in ('30M','31M','31R') 
									GROUP BY ProviderSpellNo) WardStay 
					ON WardStay.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

					--LOS in AE
					LEFT JOIN (	
									SELECT distinct
										APCEncounter.DistrictNo,
										APCEncounter.AdmissionTime,
										TimeSpentInAE = StageDurationMinutes
									FROM
										WarehouseReporting.APC.Encounter APCEncounter

									INNER JOIN WarehouseReporting.AE.Encounter AEEncounter
									ON AEEncounter.DistrictNo = APCEncounter.DistrictNo
									and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR,-2,APCEncounter.AdmissionTime) and DATEADD(HOUR,+2,APCEncounter.AdmissionTime)

									INNER JOIN WarehouseReporting.AE.Fact Fact 
									ON Fact.SourceUniqueID = AEEncounter.SourceUniqueID

									WHERE Fact.StageCode = 'INDEPARTMENTADJUSTED') LOSAE 	
					ON LOSAE.DistrictNo = Encounter.DistrictNo
					AND LOSAE.AdmissionTime = Encounter.AdmissionTime
					
					--only bring back stroke patients
					INNER JOIN (	
								SELECT 
									SourcePatientNo
									,SourceSpellNo
									,DiagnosisIndicator = CASE 
										WHEN [Primary] is not null AND [Secondary] is not null then 'Primary'
										WHEN [Primary] is not null then 'Primary'
										WHEN [Secondary] is not null then 'Secondary'
									ELSE
										NULL
									END
								FROM (
										--pivot data so we can compare diagnosis columns as a primary and secondary can be the same
										SELECT
											SourcePatientNo
											,SourceSpellNo
											,[Primary]
											,[Secondary]
										FROM
												(SELECT 
													SourcePatientNo,SourceSpellNo ,DiagnosisIndicator = 'Primary'
												FROM 
													WarehouseReporting.apc.Encounter Encounter
												WHERE 
													(PrimaryDiagnosisCode like 'I61%' or PrimaryDiagnosisCode like 'I63%' or PrimaryDiagnosisCode like 'I64%')

												UNION	
										
												SELECT 
													Encounter.SourcePatientNo,Encounter.SourceSpellNo,DiagnosisIndicator = 'Secondary'
												FROM
													WarehouseReporting.apc.Encounter Encounter
												INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
																ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
																AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
																AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
																AND (Diagnosis.DiagnosisCode like 'I61%'  or Diagnosis.DiagnosisCode like 'I63%' or Diagnosis.DiagnosisCode like 'I64%')										
												)s
										PIVOT
										(
										max(DiagnosisIndicator)
										FOR DiagnosisIndicator IN ([Primary],[Secondary])
										) as pvt
								)qry
					) StrokePatients
					ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
					AND StrokePatients.SourceSpellNo = Encounter.SourceSpellNo
					
					INNER JOIN WarehouseReporting.WH.Calendar Calendar
					ON	Calendar.TheDate = Encounter.DischargeDate

					WHERE
						Calendar.FinancialMonthKey = @FinancialMonthKey
					--and Encounter.sourcepatientno = '3257952'
					--AgeCategory must equal A
					AND CASE 
							WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionDate)))))-1900)<19
							THEN 'C'
							ELSE 'A'
						END = 'A'
										

					GROUP BY 
						PCT.[PCT Description]
						,Encounter.SourcePatientNo
						,Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard
						,TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth


)myqry

UNION

SELECT
	PCTDescription
	,InternalNo
	,EpisodeNo
	,DiagnosisIndicator
	,OtherDiagnosisIndicator = 'Yes'
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,AdmissionTime
	,DischargeTime
	,Numerator = LOSStrokeWard
	,DenominatorIncAE = Denominator + ISNULL(AETime,0)
	,PercentageOnStrokeWardIncAE = ((LOSStrokeWard*1.00)/(Denominator + ISNULL(AETime,0))*1.00)*100
	,myqry.TheMonth
FROM (
					SELECT 
						PCTDescription = PCT.[PCT Description]
						,InternalNo = Encounter.SourcePatientNo
						,EpisodeNo = Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard = ISNULL(LOSStrokeWard,0)
						,Denominator = SUM(DATEDIFF(mi,Encounter.EpisodeStartTime,Encounter.EpisodeEndTime))
						,AETime = TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth

					FROM 
						WarehouseReporting.apc.Encounter Encounter

					LEFT JOIN Information.dbo.PCT PCT 
					ON PCT.[PCT Code] = Encounter.PCTCode

					--LOS on Stroke Ward
					LEFT JOIN (		SELECT 
										ProviderSpellNo,
										LOSStrokeWard = SUM(DATEDIFF(mi,WardStay.[StartTime],WardStay.EndTime)) 
									FROM 
										[WarehouseReporting].[APC].[WardStay] 
									WHERE 
										[WardCode] in ('30M','31M','31R') 
									GROUP BY ProviderSpellNo) WardStay 
					ON WardStay.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

					--LOS in AE
					LEFT JOIN (	
									SELECT distinct
										APCEncounter.DistrictNo,
										APCEncounter.AdmissionTime,
										TimeSpentInAE = StageDurationMinutes
									FROM
										WarehouseReporting.APC.Encounter APCEncounter

									INNER JOIN WarehouseReporting.AE.Encounter AEEncounter
									ON AEEncounter.DistrictNo = APCEncounter.DistrictNo
									and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR,-2,APCEncounter.AdmissionTime) and DATEADD(HOUR,+2,APCEncounter.AdmissionTime)

									INNER JOIN WarehouseReporting.AE.Fact Fact 
									ON Fact.SourceUniqueID = AEEncounter.SourceUniqueID

									WHERE Fact.StageCode = 'INDEPARTMENTADJUSTED') LOSAE 	
					ON LOSAE.DistrictNo = Encounter.DistrictNo
					AND LOSAE.AdmissionTime = Encounter.AdmissionTime
					
					--only bring back stroke patients
					INNER JOIN (	
										SELECT
											SourcePatientNo
											,SourceSpellNo
											,DiagnosisIndicator
										FROM
												(SELECT 
													SourcePatientNo,SourceSpellNo ,DiagnosisIndicator = 'No'
												FROM 
													WarehouseReporting.apc.Encounter Encounter
												WHERE 
													(PrimaryDiagnosisCode like 'I62%' or PrimaryDiagnosisCode like 'H34%')

												UNION	
										
												SELECT 
													Encounter.SourcePatientNo,Encounter.SourceSpellNo,DiagnosisIndicator = 'No'
												FROM
													WarehouseReporting.apc.Encounter Encounter
												INNER JOIN WarehouseReporting.APC.Diagnosis Diagnosis 
																ON Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
																AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
																AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
																AND (Diagnosis.DiagnosisCode like 'I62%'  or Diagnosis.DiagnosisCode like 'H34%')										
										)s

					) StrokePatients
					ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
					AND StrokePatients.SourceSpellNo = Encounter.SourceSpellNo
					
					INNER JOIN WarehouseReporting.WH.Calendar Calendar
					ON	Calendar.TheDate = Encounter.DischargeDate

					WHERE
						Calendar.FinancialMonthKey = @FinancialMonthKey
					--and Encounter.sourcepatientno = '3257952'
					--AgeCategory must equal A
					AND CASE 
							WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionDate),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionDate)))))-1900)<19
							THEN 'C'
							ELSE 'A'
						END = 'A'
										

					GROUP BY 
						PCT.[PCT Description]
						,Encounter.SourcePatientNo
						,Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard
						,TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth


)myqry


﻿CREATE Procedure Rpt.RTTPathwayValidation
	@date datetime = '20150319'
	,@Responsibility varchar(20) = 'Division'
	,@Outcome varchar(20)= 'All'
	,@RTTPathwayID varchar(50) = 'All'
	,@DistrictNo varchar(20) = 'All'
as
	


Select 
	Census
	, EventDate
	, DistrictNo
	, RTTPathwayID
	, OrderID
	, PathwayStatus
	, Reportable
	, EvRTTPathwayID
	, EventTime
	, EventType
	, ReportingStatus
	, PASRTTStatus
	, SpecialtyCode
	, ConsultantCode
	, RTTPathwayCondition
	, SourceofReferralCode
	, ApptType
	, ClinicCode
	, OutcomeCode
	, DisposalCode
	, DisposalReason
	, RTTPathwayStartDate
	, RTTWksWaiting
	, OutcomeID = ValidationOutcome.ID
	, ValidationOutcome.Outcome
	, ValidationOutcome.Responsibility
	, DateValidated
	, UserName
	, Comments
	, DocumentID
	, TypedTime
	, SignedTime
	, NotApplicable
	, UserAdded
	, TransactionTime
	, UserLastRevised
	, RevisionTime
	, Cases = 1
from 
	RTT.PathwayValidation 

left join RTT.RTT.ValidationOutcome 
on PathwayValidation.OutcomeID = ValidationOutcome.ID

where 
	EventDate = @Date 
and 
	(	NotApplicable Not In ('OtherKeyEventOnSameDate','EventNoLongerExists')
	or
		NotApplicable is null 
	)
and 
	(	Responsibility = @Responsibility
	or
		@Responsibility = 'All'
	)
and
	(	Outcome = @Outcome
	or
		@Outcome = 'All'
	)
and
	(	RTTPathwayID = @RTTPathwayID
	or
		@RTTPathwayID = 'All'
	)
and
	(	DistrictNo = @DistrictNo
	or
		@DistrictNo = 'All'
	)

﻿
create proc RPT.GetLongLoSPatients

as

select
	AdmissionMethodType = upper(AdmissionMethodType)
	,AdmissionDate
	,NationalSpecialty
	,WardCode = 				
				(
				select top 1 WardCode
				from Warehouse.APC.WardStay wardstay 
				where wardstay.ProviderSpellNo = encounter.ProviderSpellNo
				order by wardstay.StartTime desc
				)
	,NHSNumber
	,PatientName = PatientSurname + ', ' + PatientForename
	,LoS = datediff(day, Admissiondate, getdate())
	,PCTCode = -- taken from the CDS derivaton for purchaser code - agreed with Tim 19/06/2012
		left(
		case
			when AdminCategoryCode = '02' then 'VPP00'
			when coalesce(									-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
						patientaddressatadmission.Postcode
						,encounter.Postcode
						) like 'ZZ99%'
			or ContractSerialNo = 'OSV00A'
			then 'OSV'
			when ContractSerialNo <> 'OSV00A'
			and ContractSerialNo like 'OSV%'
			then 'YAC'
			else
				coalesce(
						PCT.OrganisationCode -- joined from PurchaserCode
					,Practice.ParentOrganisationCode
					,Postcode.PCTCode
					,'5NT'
				)	
		end
		, 3)
	,GpCodeAtAdmission = RegisteredGpPracticeAtAdmission.GpCode
	,GpPracticeCodeAtAdmission = RegisteredGpPracticeAtAdmission.GpPracticeCode
	,PostCodeAtAdmission = patientaddressatadmission.Postcode
	
from
	WarehouseReporting.APCUpdate.Encounter

left outer join Warehouse.PAS.PatientAddress patientaddressatadmission
on patientaddressatadmission.SourcePatientNo = encounter.SourcePatientNo
and encounter.AdmissionDate between patientaddressatadmission.EffectiveFromDate and coalesce(patientaddressatadmission.EffectiveToDate, getdate())
		
left outer join Warehouse.PAS.PatientGp RegisteredGpPracticeAtAdmission
on RegisteredGpPracticeAtAdmission.SourcePatientNo = Encounter.SourcePatientNo
and Encounter.AdmissionDate between RegisteredGpPracticeAtAdmission.EffectiveFromDate and coalesce(RegisteredGpPracticeAtAdmission.EffectiveToDate, getdate())
	
/* to cleanse the PurchaserCode values - we have old PCT codes in the column - requested by Tim 28/06/2012 */
left outer join Organisation.dbo.PCT PCT
on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode

left outer join Organisation.dbo.Practice Practice
on Practice.OrganisationCode = RegisteredGpPracticeAtAdmission.GpPracticeCode

inner join WarehouseReporting.PAS.AdmissionMethod admissionmethod
on admissionmethod.AdmissionMethodCode = encounter.AdmissionMethodCode

inner join WarehouseReporting.PAS.Specialty specialty
on specialty.SpecialtyCode = encounter.SpecialtyCode
		
left outer join Organisation.dbo.Postcode Postcode 
on Postcode.Postcode = 
					case
						when len(patientaddressatadmission.Postcode) = 8 then patientaddressatadmission.Postcode
						else left(patientaddressatadmission.Postcode, 3) + ' ' + right(patientaddressatadmission.Postcode, 4) 
					end

where
	EpisodeEndTime is null
and
	DischargeDate is null
and
	datediff(day, Admissiondate, getdate()) >= 30


﻿
create proc RPT.GetCurrentInpatientsByReligion

(
@pasreligioncode varchar(50) = null
,@religiongroupcode varchar(50) = null
,@wardcode varchar(50) = null
)

as

set @pasreligioncode = coalesce(@pasreligioncode, '(Select All)')
set @religiongroupcode = coalesce(@religiongroupcode, '(Select All)')
set @wardcode = coalesce(@wardcode, '(Select All)')

select
	PatientName = PatientSurname + ', ' + PatientForename
	,AdmissionDate
	,PASReligionCode = encounter.ReligionCode
	,PASReligion = pasreligion.Religion
	,NationalReligionCode = pasreligion.ReligionNationalCode
	,Religion = religion.Religion
	,ReligionGroup = religiongroup.ReligionGroup
	,[WardCode] = 				
				(
				select top 1 WardCode
				from Warehouse.APC.WardStay wardstay 
				where wardstay.ProviderSpellNo = encounter.ProviderSpellNo
				order by wardstay.StartTime desc
				)
from
	APC.Encounter encounter
	left outer join PAS.Religion pasreligion
	on encounter.ReligionCode = pasreligion.ReligionCode
	left outer join Warehouse.WH.Religion religion
	on religion.ReligionCode = pasreligion.ReligionNationalCode
	left outer join Warehouse.WH.ReligionGroup religiongroup
	on religiongroup.ReligionGroupCode = religion.ReligionGroupCode
	
where 
	EpisodeEndDate is null
and DischargeDate is null
and (encounter.ReligionCode = @pasreligioncode or @pasreligioncode = '(Select All)')
--and (encounter.ReligionCode in (select value from warehouse.dbo.SSRS_MultiValueParamSplit(@pasreligioncode,',')) or @pasreligioncode = '(Select All)')
and (religion.ReligionGroupCode = @religiongroupcode or @religiongroupcode = '(Select All)')
--and (religion.ReligionGroupCode in (select value from warehouse.dbo.SSRS_MultiValueParamSplit(@religiongroupcode,',')) or @religiongroupcode = '(Select All)')
and ((
	select top 1 WardCode
	from Warehouse.APC.WardStay wardstay 
	where wardstay.ProviderSpellNo = encounter.ProviderSpellNo
	order by wardstay.StartTime desc
	) = @wardcode or @wardcode = '(Select All)')

option (recompile)


﻿CREATE TABLE [RPT].[VTECategoryCodeCaseTypes] (
    [VTECategoryCode]    CHAR (1) NOT NULL,
    [VTECompleteCases]   INT      NOT NULL,
    [VTEIncompleteCases] INT      NOT NULL,
    [VTEMissingCases]    INT      NOT NULL,
    PRIMARY KEY CLUSTERED ([VTECategoryCode] ASC)
);


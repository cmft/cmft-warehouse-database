﻿








--set dateformat dmy

CREATE View [RPT].[AESMSData] as

/* 
Purpose:	AE Friends and Family Test - mobile numbers to Healthcare Communications, once daily.
			This view is used in SSIS package scheduled every morning.
			
When		Who			What
24/01/2014	GC & PE		Initial coding.
03/02/2015	Paul Egan	Added manual dates to WHERE clause in both CTEs to run this script manually
						for missing extracts.
*/

With WarehouseReportingData as
(
select distinct
	DistrictNo = Encounter.DistrictNo
	--,DistrictNoSeq = ROW_NUMBER() over (partition by Encounter.DistrictNo order by Encounter.DepartureTime desc)
	,Discharge_ref = Encounter.SourceUniqueID
	,discharge_date = CONVERT(VARCHAR(10), Encounter.DepartureTime, 103) --M
	,discharge_time = left(cast(Encounter.DepartureTime as time),5) --M
	,location = 'RW3' + '|' + Site.NationalSiteCode + '|' + 'A+E' --M
	,Lead_specialty_1 = '180' --M
	,Lead_specialty_2 = ''
	,Email_address = ''
	,title = ''						--coalesce(upper(Encounter.PatientTitle),'')
	,first_name = 'FirstName'		--upper(Encounter.PatientForename) --M
	,last_name = 'LastName'			--upper(Encounter.PatientSurname) --M
	,Address_1 = ''					--coalesce(Encounter.PatientAddress1,'')
	,Address_2 = ''					--coalesce(Encounter.PatientAddress2,'')
	,Address_3 = ''					--coalesce(Encounter.PatientAddress3,'')
	,Address_4 = ''					--coalesce(Encounter.PatientAddress4,'')
	,Address_5 = ''					--''
	,Postcode =	''					--coalesce(Encounter.Postcode,'')
	,Country = ''
from
	   Warehousesql.WarehouseReportingMerged.AE.Encounter Encounter
inner join
	   Warehousesql.WarehouseReportingMerged.AE.AttendanceDisposal AttendanceDisposal
on
	   Encounter.AttendanceDisposalID = AttendanceDisposal.SourceAttendanceDisposalID
inner join
	   Warehousesql.WarehouseReportingMerged.WH.Site Site
on
	   Encounter.SiteID = Site.SourceSiteID
	   
inner join 
		Warehousesql.Warehouse.PAS.Patient Patient
		on Patient.DistrictNo = Encounter.DistrictNo 
							       
where
	   AttendanceDisposal.NationalAttendanceDisposalCode not in ('01','12','13','14','15','10','20')  --admitted to hospital
and
		cast(Encounter.DepartureTime as date) = dateadd(day, -1, cast(getdate() as date)) -- Yesterday's discharges
	   --Encounter.DepartureTime  > dateadd(month,datediff(month,0,getdate()-10),0) and Encounter.DepartureTime  < dateadd(month,datediff(month,0,getdate()),0) -- Initial run
		--cast(Encounter.DepartureTime as date) between 
		--	dateadd(day, -6, cast(getdate() as date)) and dateadd(day, -1, cast(getdate() as date)) -- Manual run if any dates missed
and
		floor(datediff(day, Encounter.DateOfBirth, Encounter.DepartureTime) / 365.25) > = 16
--								   (case when (floor(datediff(day, DateOfBirth, [ArrivalDate]) / 365.25) > = 16) or [NationalSiteCode] in ('RW3RC') then 1 else 0 end) = 1
--and
--	   Reportable = 1
and
	   ContextCode = 'CEN||SYM'
and
	   [NationalSiteCode] = 'RW3MR'
and
	   Encounter.DateofDeath is null --or Encounter.DateofDeath > [ArrivalDate]) 
--and
--	(
--		MobilePhone like '07%'
--		--isnumeric(dbo.ExtractContactNumber(MobilePhone)) = 1 
--	or
--		HomePhone like '07%'
--		--isnumeric(dbo.ExtractContactNumber(HomePhone)) = 1 
--	or
--		WorkPhone like '07%'
--		--isnumeric(dbo.ExtractContactNumber(WorkPhone)) = 1 
--	)
)




,SymphonyStagingData as		-- Need this to grab opt-out and mobile number (until it can be ETL'd)
(
select distinct
	DistrictNo.DistrictNo
	,DistrictNoSeq = ROW_NUMBER() over (partition by DistrictNo.DistrictNo order by Encounter.atd_dischdate desc)
	,patient_ref = Patient.pat_pid
	,SMSOptOutResult = SMSOptOutResult.Lkp_Name
	,mobile_number = 
		case
			when dbo.ExtractContactNumber(Telephone.tel_mobile) like '07%' then WarehouseReporting.dbo.ExtractContactNumber(Telephone.tel_mobile)
			when dbo.ExtractContactNumber(Telephone.tel_home) like '07%' then WarehouseReporting.dbo.ExtractContactNumber(Telephone.tel_home)
			-- Decided not to use work mobile as these devices can potentially change users often
		end
from 
	warehousesql.CMMC_Reports.dbo.Patient Patient
	
	inner join warehousesql.CMMC_Reports.dbo.Episodes Episode
	on	Patient.pat_pid = Episode.epd_pid 
		
	inner join warehousesql.CMMC_Reports.dbo.Attendance_Details Encounter
	on Episode.epd_id = Encounter.atd_epdid
		
	left join warehousesql.CMMC_Reports.dbo.Telephone Telephone
	on	Patient.pat_pid = Telephone.tel_linkid		-- From Majid Ali & Liam Orrell
		
	left join warehousesql.CMMC_Reports.dbo.Patient_Details_ExtraFields SMSOptOut
	on	Patient.pat_pid = SMSOptOut.pdt_EF_pdtpid
		and	SMSOptOut.pdt_EF_FieldID = 2118		-- From Gareth Cunnah
	
	left join warehousesql.[CMMC_Reports].[dbo].[Lookups] SMSOptOutResult
	on SMSOptoutResult.Lkp_ID = SMSOptOut.pdt_EF_Value		-- From Gareth Cunnah

	inner join	-- Get District No
	(
	select distinct
			 DistrictNo = patient_system_ids.psi_system_id 
			,psi_pid
		from
			warehousesql.CMMC_Reports.dbo.patient_system_ids patient_system_ids
		where
			patient_system_ids.psi_system_name = 
				(
				select
					lkp_id
				from
					warehousesql.CMMC_Reports.dbo.Lookups
				where
					lkp_name = 'District Number'
				)
		and	not exists
			(
			select
				1
			from
				warehousesql.CMMC_Reports.dbo.patient_system_ids PreviousDistrictNo
			where
				PreviousDistrictNo.psi_system_name = patient_system_ids.psi_system_name
			and	PreviousDistrictNo.psi_pid = patient_system_ids.psi_pid
			and	PreviousDistrictNo.psi_system_id > patient_system_ids.psi_system_id
			)
	) DistrictNo
	on	DistrictNo.psi_pid = Patient.pat_pid
where 
	cast(Encounter.atd_dischdate as date) = dateadd(day, -1, cast(getdate() as date))
	--cast(Encounter.atd_dischdate as date) between 
	--	dateadd(day, -6, cast(getdate() as date)) and dateadd(day, -1, cast(getdate() as date))	-- Manual run if any dates missed
)


--========================== Get final data set ===============================
select
	--VALIDATIONDistrictNo  = WarehouseReportingData.DistrictNo
	--,VALIDATIONDistrictNoSeq = WarehouseReportingData.DistrictNoSeq
	--,VALIDATIONSMSOptOutResult = SymphonyStagingData.SMSOptOutResult
	SymphonyStagingData.Patient_Ref
	,WarehouseReportingData.Discharge_ref
	,WarehouseReportingData.discharge_date
	,WarehouseReportingData.discharge_time
	,WarehouseReportingData.location
	,WarehouseReportingData.Lead_specialty_1
	,WarehouseReportingData.Lead_specialty_2
	,Mobile_Number = SymphonyStagingData.mobile_number
	,Landline_Number = ''
	,WarehouseReportingData.Email_address
	,WarehouseReportingData.title
	,WarehouseReportingData.first_name
	,WarehouseReportingData.last_name
	,WarehouseReportingData.Address_1
	,WarehouseReportingData.Address_2
	,WarehouseReportingData.Address_3
	,WarehouseReportingData.Address_4
	,WarehouseReportingData.Address_5
	,WarehouseReportingData.Postcode
	,WarehouseReportingData.Country
	
from 
	WarehouseReportingData
	
	left join SymphonyStagingData
	on	WarehouseReportingData.DistrictNo = SymphonyStagingData.DistrictNo collate Latin1_General_CI_AS
		and SymphonyStagingData.DistrictNoSeq = 1	-- Remove any duplicates due to Symphony Telephone join

where 
	(SymphonyStagingData.SMSOptOutResult <> 'Yes' or SymphonyStagingData.SMSOptOutResult is null)	-- New field added in Symphony
	and SymphonyStagingData.Mobile_Number is not null
	and len(SymphonyStagingData.Mobile_Number) = 11		--Some mobile nos don't have the correct number of digits






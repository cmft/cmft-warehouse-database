﻿


CREATE view [RPT].[SecurityMap]

as

--select
--	[DirectorateCode]
--	,DivisionCode
--	,WindowsGroup
--	,Name
--	,Type
--	,Privilege 
--	,MappedLoginName 
--	,PermissionPath 
	
--from 
--	DBA.DBA.PIDGroupMembers pgm
--	inner join DBA.DBA.SecurityMap sm
--	on pgm.PermissionPath = sm.WindowsGroup
	
select
	adgroupmembers.[DistributionGroup] 
	,adgroupmembers.[FirstName]
	,adgroupmembers.[LastName]
	,adgroupmembers.[EmailAddress]
	,adgroupmembers.[DomainLogin] 
	,securitymap.DirectorateCode
	,securitymap.DivisionCode
from
	CMFTDataServices.DirectoryServices.AD_Group_Members adgroupmembers
	inner join DBA.DBA.SecurityMap securitymap
	on adgroupmembers.[DistributionGroup] = securitymap.[DistributionGroup]




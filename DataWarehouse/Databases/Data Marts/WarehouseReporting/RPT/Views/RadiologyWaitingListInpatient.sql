﻿







create view [RPT].[RadiologyWaitingListInpatient]
as

/*
Author:		Paul Egan
Created:	06/02/2014
Purpose:	Radiology inpatient waiting list Excel report from CRIS stat
*/

/* Get latest census */
select
	CensusTime
	,[Best Hosp No] = DistrictNo
	,[CRIS No] = SourcePatientNo
	,[Surname] = PatientSurname
	,[Forenames] = PatientForenames
	,[DOB] = cast(PatientDateOfBirth as datetime) -- Need datetime (instead of date) to show correct date format in Excel when using the odc data connection
	,[Ward at Time of Referral] = ReferralLocationName
	,[Current Ward Code] = CurrentWardCode
	,[Current Ward Name] = CurrentWardName
	,[Exam Name] = ExamName
	,[Request date] = cast(RequestDate as datetime)	-- Need datetime (instead of date) to show correct date format in Excel when using the odc data connection
	,[Event Date] = cast(EventDate as datetime)	-- Need datetime (instead of date) to show correct date format in Excel when using the odc data connection
	,[Referrer Name] = ReferrerName
	,[Referrer Speciality] = ReferrerSpecialtyName
from 
	Warehouse.rad.WaitingListInpatient
where
	CensusTime = (select max(CensusTime) from Warehouse.rad.WaitingListInpatient)







﻿




CREATE VIEW [RPT].[EddSpells]

AS 

SELECT 
	 EDD.[SourceSpellNo]
	,[SourcePatientID]
	,[EddHistoryID]
	,[EddSequence]
	,CASE 
		WHEN [HasEddFlag] = 1 THEN 'Yes'
		WHEN [HasEddFlag] = 0 THEN 'No'
		ELSE NULL
	END [HasEddFlag]
	,CASE 
		WHEN [HasEddBedmanFlag] = 1 THEN 'Yes'
		WHEN [HasEddBedmanFlag] = 0 THEN 'No'
		ELSE NULL
	 END [HasEddBedmanFlag]
	,CASE 
		WHEN [HasEddPASFlag] = 1 THEN 'Yes'
		WHEN [HasEddPASFlag] = 0 THEN 'No'
		ELSE NULL
	 END [HasEddPASFlag]
	,[CurrentRecordFlag]
	,[AdmitDate]
	,[DischargeDate]
	,Calendar.FinancialYear AS DischargeFinancialYear
	,Calendar.FinancialQuarter AS DischargeFinancialQuarter
	,Calendar.TheMonth AS DischargeMonth
	,Calendar.WeekNo AS DischargeWeekNo
	,EDD.[SiteCode]
	,[FirstEdd] AS FirstEddRecorded
	,[LastEdd] AS LastEddRecorded
	,PrevEdd AS PreviousEDDRecorded
	,[Edd]
	,[EddCreated]
	,PddReasons.PDDGroup AS EddReasonGroup
	,CASE WHEN PddReasons.PDDReason = '-' THEN PddReasons.PDDGroup
	ELSE  PddReasons.PDDReason 
	END AS EddReasonForChange
	,[Actual_LOS]
	,[Exp_LOS]
	,[Actual_LOS]-[Exp_LOS] AS Var_ActualExp	
	,[varSpellFistLastEDD]
	,[varFromPrevEDD]
	,[varDischargeEDDCreated]
	,CASE WHEN [EddEnteredWithin24hrs] = 1 THEN 'Yes'
			WHEN [EddEnteredWithin24hrs] = 0 THEN 'No'
			ELSE 'NA'
	END [EddEnteredWithin24hrs]
	,Directorate.Directorate
	,CASE 
		WHEN sourcepatientcategorycode = 'NE' 
		 AND Specialty.SpecialtyCode = 'GMED' THEN 'Acute Medical' 
		ELSE Directorate.Division
	END AS Division
	,Specialty.Specialtycode AS SpecialtyCode
	,Specialty.Specialty AS Specialty
	,Specialty.NationalSpecialty AS NationalSpecialty
	,Specialty.NationalSpecialtyCode AS NationalSpecialtyCode
	,[CurrentWard] AS CurrentWardCode
	,Ward.Ward
	,[CurrentConsulant] AS ConsultantCode
	,[Consultant].Consultant
	,sourcepatientcategorycode
	,SourceRTTPathwayCondition
	,SourceEncounterNo
FROM 
	WarehouseOLAP.dbo.FactEDD EDD
INNER JOIN WH.Calendar 
	ON EDD.DischargeDate = Calendar.TheDate
INNER JOIN PAS.Specialty
	ON EDD.CurrentSpecialty =  Specialty.SpecialtyCode 
INNER JOIN PAS.Ward 
	ON EDD.CurrentWard = Ward.WardCode 
INNER JOIN PAS.Consultant
	ON EDD.CurrentConsulant = Consultant.ConsultantCode
INNER JOIN PAS.Directorate Directorate
  	ON EDD.DirectorateCode = Directorate.DirectorateCode 
LEFT OUTER JOIN warehouse.bedman.PddReasons PddReasons
	ON EDD.EddReasonCode = PddReasons.PDDReasonID;


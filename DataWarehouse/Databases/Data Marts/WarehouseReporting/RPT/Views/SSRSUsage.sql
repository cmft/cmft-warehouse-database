﻿


CREATE view [RPT].[SSRSUsage] as
select
	 ExecutionLog.UserName
	,Report = Catalog.[Name]
	,ReportPath = Catalog.[Path]
	,ExecutionLog.TimeStart
	,ExecutionLog.TimeEnd
	,ExecutionDuration = datediff(millisecond , ExecutionLog.TimeStart , ExecutionLog.TimeEnd)
	,ExecutionLog.TimeDataRetrieval
	,ExecutionLog.TimeProcessing
	,ExecutionLog.TimeRendering
	,ExecutionLog.ByteCount
	,ExecutionLog.[RowCount]
	,StartDayOfWeek = datename(dw , ExecutionLog.TimeStart)
	,StartHourOfDay = datepart(hh , ExecutionLog.TimeStart)
	,StartYear = datepart(yy , ExecutionLog.TimeStart)
	,StartMonth = datename(m , ExecutionLog.TimeStart)
	,StartWeek = datepart(wk , ExecutionLog.TimeStart)
	,Cases = 1
from
	WarehouseSQL.ReportServerSPIntegrated.dbo.ExecutionLogStorage ExecutionLog with (nolock)

left join WarehouseSQL.ReportServerSPIntegrated.dbo.Catalog Catalog with (nolock)
on	ExecutionLog.ReportID = Catalog.ItemID


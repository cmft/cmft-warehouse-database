﻿


CREATE procedure [Parameter].[CasenoteType]

as

/****************************************************************************************
	Stored procedure : [Parameter].[CasenoteType]
	Description		 : SSRS parameter proc for the Visual Studio report
	
	$/DataWarehouse/Casenote/SSRS Casenote/Casenote Destruction List - Inactive Patients.rdl

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Initial Coding
	17/07/2014	Paul Egan		Copied from LIVE to DEV (different)
*****************************************************************************************/

Select  

	CasenoteTypeCode = 'All'
	
	union all

select
	CasenoteTypeCode
from
	WarehouseOLAP.dbo.OlapCasenoteType
where
	(
	left(CasenoteTypeCode , 1) in ('M','E','R','S','B')
	OR
	left(CasenoteTypeCode , 2) = 'SP'
	)
	and 

	exists 

		(
		select 1 from WarehouseOLAP.dbo.BasePASCasenoteInactive Casenote
		where Casenote.CasenoteTypeCode = WarehouseOLAP.dbo.OlapCasenoteType.CasenoteTypeCode
		)

		and left(CasenoteTypeCode , 3) <> 'BCG'
		
		
		




	


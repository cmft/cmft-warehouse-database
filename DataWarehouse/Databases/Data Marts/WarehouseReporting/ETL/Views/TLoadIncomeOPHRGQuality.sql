﻿
create view ETL.TLoadIncomeOPHRGQuality

as

select --top 10 
	--MonthNumber = datepart(Month, dateadd(month, -3, encounter.DischargeDate)) 
	SnapshotRecno
	,encounter.Period
	,encounter.Position
	,encounter.[Version]  
	,SUSIdentifier = 
					--'BMCHT ' 
					--+ cast(encounter.SourcePatientNo as varchar)
					--+ '*' 
					--+ cast(encounter.SourceEncounterNo as varchar)
					--+ '*'
					--+ cast(year(AppointmentTime) as varchar)
					--+ case
					--		when len(datepart(month, AppointmentTime)) < 2
					--		then '0' + cast(datepart(month, AppointmentTime) as varchar)
					--		else (cast(month(AppointmentTime) as varchar))
					--	end
					--+ case
					--		when len(datepart(day, AppointmentTime)) < 2
					--		then '0' + cast(datepart(day, AppointmentTime) as varchar)
					--		else cast(datepart(day, AppointmentTime) as varchar)
					--	end
					--+ case
					--		when datepart(hour, AppointmentTime) = 0 -- needed as ITS does not return 00:00 if no time
					--		then ''
					--		when len(datepart(hour, AppointmentTime)) < 2
					--		then '0' + cast(datepart(hour, AppointmentTime) as varchar)
					--		else cast(datepart(hour, AppointmentTime) as varchar)
					--	end	
					--+ case
					--		when datepart(hour, AppointmentTime) = 0  -- needed as ITS does not return 00:00 if no time
					--		and datepart(minute, AppointmentTime) = 0
					--		then ''
					--		when len(datepart(minute, AppointmentTime)) < 2
					--		then '0' + cast(datepart(minute, AppointmentTime) as varchar)
					--		else cast(datepart(minute, AppointmentTime) as varchar)
					--	end	
					--+ '*'
					--+ encounter.DoctorCode
				'BRW300' +	
				cast(Encounter.SourcePatientNo as varchar) + 
				'*' + 
				CAST(Encounter.SourceEncounterNo as varchar) + 
				'*' +
				convert(varchar , Encounter.AppointmentDate , 112) +
				'*' +
				cast(
					ROW_NUMBER() over 
						(
						partition by 
							 Encounter.SourcePatientNo
							,Encounter.SourceEncounterNo
						order by
							 Encounter.AppointmentTime
							,Encounter.DoctorCode
						)
					as varchar
				)
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	
	
	
from 
	WarehouseReporting.ETL.TImportIncomeOP encounter

	inner join WarehouseReporting.ETL.HRG4OPEncounter hrgencounter
	on encounter.EncounterRecNo = hrgencounter.EncounterRecNo
	
	inner join WarehouseReporting.ETL.HRG4OPQuality hrgquality
	on hrgquality.RowNo = hrgencounter.RowNo

	inner join [Income].[OPSnapshot] OPsnapshot
	on encounter.SourceUniqueID = OPsnapshot.SourceUniqueID
	and encounter.Period = OPsnapshot.Period 

	and encounter.Position = OPsnapshot.Position
	and encounter.[Version] = OPsnapshot.[Version]

	
		

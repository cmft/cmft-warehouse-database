﻿
create view ETL.HRG4OPInputFile

as

select
	 AgeOnAppointment =
		--datediff(
		--	 year
		--	,Encounter.DateOfBirth
		--	,Encounter.AppointmentDate
		--)
		floor(
		datediff(
			 day
			,Encounter.DateOfBirth
			,Encounter.AppointmentDate
		)/365.25
		)

	,SexCode =
		case
		when Encounter.SexCode = 'M' then '1' 
		when Encounter.SexCode = 'F' then '2' 
		else '9'
		end
	--,main = Consultant.MainSpecialtyCode
	,MainSpecialtyCode =
		MainSpecialty.NationalSpecialtyCode

	,TreatmentFunctionCode =
		Specialty.NationalSpecialtyCode

	,FirstAttendanceCode =
		DerivedFirstAttendanceFlag

	,Operation1 =
	left(
		replace(replace(Encounter.PrimaryOperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation2 =
	left(
		replace(replace(Encounter.SecondaryOperationCode1, '.', ''), ',', '')
		, 4
	) 
	,Operation3 =
	left(
		replace(replace(Encounter.SecondaryOperationCode2, '.', ''), ',', '')
		, 4
	) 
	,Operation4 =
	left(
		replace(replace(Encounter.SecondaryOperationCode3, '.', ''), ',', '')
		, 4
	) 
	,Operation5 =
	left(
		replace(replace(Encounter.SecondaryOperationCode4, '.', ''), ',', '')
		, 4
	) 
	,Operation6 =
	left(
		replace(replace(Encounter.SecondaryOperationCode5, '.', ''), ',', '')
		, 4
	) 
	,Operation7 =
	left(
		replace(replace(Encounter.SecondaryOperationCode6, '.', ''), ',', '')
		, 4
	) 
	,Operation8 =
	left(
		replace(replace(Encounter.SecondaryOperationCode7, '.', ''), ',', '')
		, 4
	) 
	,Operation9 =
	left(
		replace(replace(Encounter.SecondaryOperationCode8, '.', ''), ',', '')
		, 4
	) 
	,Operation10 =
	left(
		replace(replace(Encounter.SecondaryOperationCode9, '.', ''), ',', '')
		, 4
	) 
	,Operation11 =
	left(
		replace(replace(Encounter.SecondaryOperationCode10, '.', ''), ',', '')
		, 4
	) 
	,Operation12 =
	left(
		replace(replace(Encounter.SecondaryOperationCode11, '.', ''), ',', '')
		, 4
	) 

	,Encounter.EncounterRecno
from
	ETL.TImportIncomeOP Encounter
	
left join Warehouse.PAS.Specialty Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join Warehouse.PAS.Consultant Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join Warehouse.PAS.Specialty MainSpecialty
on	MainSpecialty.SpecialtyCode = Consultant.MainSpecialtyCode

where
	Encounter.ExcludeFlag is null
	and Encounter.ExcludePatientFlag is null

--where 
--	Encounter.AppointmentDate > dateadd(month, -3, GETDATE())

--and	Encounter.PrimaryOperationCode is not null


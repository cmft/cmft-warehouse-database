﻿
create view ETL.TLoadIncomeOP

as

select
	SnapshotRecno
	,encounter.Period
	,encounter.Position
	,encounter.[Version]
	,[MonthNumber] = datepart(Month, dateadd(month, -3, encounter.AppointmentDate))
	,PCTCode = 	
			encounter.PCTCode
			+ 
			-- concatenates PCT code with non core report flag
			+ 
			case
				when opprocclinic.HRGCode = 'COMMID_S'
				and encounter.PCTCode = '5F5'
				then '_MLU'
				--when PCTCode = '5F5' -- removed as now in specialist commissioning
				--and specialty.NationalSpecialtyCode in (
				--										'656'
				--										,'711'
				--										)
				--then '_CAMHS'
				when hrgencounter.HRGCode = 'COMMID'
				then ''			
				when specialistcommissioning.SpecialistCommissioningCode is not null
				then '_' + specialistcommissioning.SpecialistCommissioningCode
				else ''
			end
	,TrustCode = 'RW3'
	,encounter.SiteCode
	,SpecialtyCode = specialty.NationalSpecialtyCode
	,HRGCode = 
			case 
				when specialty.NationalSpecialtyCode = '361'
				and encounter.PrimaryOperationCode = 'X29.2'
				then 'IRON'
				when encounter.ClinicCode in -- OP 41 - Update Lucentis Clinics WF01A (OPFUP)
											(
											'LUAV'
											,'MAPASS'
											,'OCTLUAV' 
											,'PDTFFA'
											,'PDTPES'
											,'PDTSJC'
											,'MTCAM'
											,'MTCPM'
											,'LUAVEXT'
											)
				and case
						when encounter.ClinicCode = 'MODREP' 
						then 'OPFUP'
						when encounter.DerivedFirstAttendanceFlag = 1 
						then 'OPFA'
						when encounter.DerivedFirstAttendanceFlag = 2 
						then 'OPFUP'
						else null
					end = 'OPFUP'
				then 'WF01A'
				
				when encounter.ClinicCode in -- OP 41 - Update Lucentis Clinics WF01B (!= OPFUP)
											(
											'LUAV'
											,'MAPASS'
											,'OCTLUAV' 
											,'PDTFFA'
											,'PDTPES'
											,'PDTSJC'
											,'MTCAM'
											,'MTCPM'
											,'LUAVEXT'
											)
				and case
						when encounter.ClinicCode = 'MODREP' 
						then 'OPFUP'
						when encounter.DerivedFirstAttendanceFlag = 1 
						then 'OPFA'
						when encounter.DerivedFirstAttendanceFlag = 2 
						then 'OPFUP'
						else null
					end <> 'OPFUP'
				then 'WF01B'
				
				--when -- OP 38c - Flag OPLASER removed 12 Oct 2012 DG for logic below
					--coalesce(
					--		case 
					--			when 
					--				hrgencounter.HRGCode in -- OP 38a - Update Local HRGs (OPPROC Clinics)
					--										(
					--										'WF01A'
					--										,'WF01B'
					--										,'UZ01Z'
					--										)
					--			and opprocclinic.HRGCode is not null
					--			then opprocclinic.HRGCode			
					--		end
					--		,''
					--		)
					--		not in (
					--				'WF02A'
					--				,'WF02B'
					--				)
					--		and 
					--			(
					--			encounter.PrimaryOperationCode in
					--									(
					--									--'C61.1'
					--									--,'C62.3'
					--									--,'C73.3'
					--									--,'C82.1'
					--									--,'C82.2'
					--									--,'C82.3'
					--									'C82.5' -- requested by Sam 11/07/2012
					--									,'C82.6'
					--									,'C82.8'
					--									,'C85.5'
					--									,'C61.1'
					--									,'C62.3'
					--									)
					--			or encounter.PrimaryOperationCode = 'C73.3'
					--			and encounter.SecondaryOperationCode1 = 'Y08.6' -- Sam confirmed second sequence only
					--			)						
						
					--	then 'OPLASER'

				when -- Flag OPLASER 'new process' as instructed by Phil H /Marianne
					hrgencounter.HRGCode in 
											(
											'BZ04Z'
											,'BZ19Z'
											,'BZ23Z'
											)
				and
					Encounter.ClinicCode in
											(
											'CALAS'
											,'EECLAS'
											,'LAEXT'
											,'LASAZIZ'
											,'LASCHTH'
											,'LASDEKFR'
											,'LASDEKMO'
											,'LASEXT'
											,'LASFRIPM'
											,'LASGENMO'
											,'LASGLAMO'
											,'LASGLATH'
											,'LASGST'
											,'LASIK'
											,'LASMCKTH'
											,'LASMJL'
											,'LASMONPM'
											,'LASRITOP'
											,'LASSJC'
											,'LASTUEAM'
											,'LASTUEPM'
											,'LASWEDAM'
											,'RET1LAS'
											,'RETLAS'
											,'SAJMLAS'
											,'TALAS'
											,'TCUDLAS'
											,'YDSLAS'
											)
				then 'OPLASER'
				
				when hrgencounter.HRGCode in -- OP 38a - Update Local HRGs (OPPROC Clinics)
											(
											'WF01A'
											,'WF01B'
											,'UZ01Z'
											)
				and opprocclinic.HRGCode is not null						
				then opprocclinic.HRGCode
					
				else hrgencounter.HRGCode -- returns default HRG if none of the above conditions met
			end
	,PODCode = 
			coalesce
			(
			case
				when encounter.ClinicCode = 'MODREP' 
				then 'OPFUP'
				when encounter.ClinicCode in -- added for LUCENTIS 17/07/2012 note from Linda Roberts
											(
											'MTCAM'
											,'MTCPM'
											,'MTCEVE'
											)
				then 'OPFUP' 
				when encounter.DerivedFirstAttendanceFlag = 1 
				then 'OPFA'
				when encounter.DerivedFirstAttendanceFlag = 2 
				then 'OPFUP'
				else null
			end
			,
			''
			)
			+
			case -- OP 24 - Flagging Multi Professional
				when mpclinic.ConsultantCode is not null 
				and encounter.DirectorateCode not in 
													(
													'12'
													,'20'
													,'24'
													,'26'
													,'90'
													)	
				then 'MPCL'									
				-- OP 38b - Update Multi Professional PODs
				when 
					case
						when hrgencounter.HRGCode in -- OP 38a - Update Local HRGs (OPPROC Clinics)
										(
										'WF01A'
										,'WF01B'
										,'UZ01Z'
										)
						and opprocclinic.HRGCode is not null						
						then opprocclinic.HRGCode
						else hrgencounter.HRGCode -- returns default HRG if none of the above conditions met
					end 
					in (
						'WF02A'
						,'WF02B'
						)															
				then 'MPCL'
				else 'SPCL'
			end
	,Activity = 1
	,[SpecialServiceCode1] = null
	,[SpecialServiceCode2] = null
	,[SpecialServiceCode3] = null
	,GPPracticeCode = coalesce(
							encounter.GpPracticeCodeAtAppointment
							,encounter.RegisteredGpPracticeCode
							)

	,PatientCode = encounter.NHSNumber
	,DateOfBirth = encounter.DateOfBirth
	,ConsultantCode = encounter.ConsultantCode
	,GpCode = coalesce(
					encounter.GpCodeAtAppointment
					,encounter.RegisteredGpCode
					)
	,LoS = null
	,ReportMasterID = encounter.SourcePatientNo + '*' +  encounter.SourceEncounterNo
	,UserField1 = encounter.DirectorateCode
	,UserField2 = encounter.DistrictNo
	,UserField3 = encounter.AppointmentTime
	,UserField4 = coalesce(
						encounter.PostCodeAtAppointment
						,encounter.Postcode
						)
	,UserField5 = 
				'BRW300' +	
				cast(Encounter.SourcePatientNo as varchar) + 
				'*' + 
				CAST(Encounter.SourceEncounterNo as varchar) + 
				'*' +
				convert(varchar , Encounter.AppointmentDate , 112) +
				'*' +
				cast(
					ROW_NUMBER() over 
						(
						partition by 
							 Encounter.SourcePatientNo
							,Encounter.SourceEncounterNo
						order by
							 Encounter.AppointmentTime
							,Encounter.DoctorCode
						)
					as varchar
				)
	,AdmissionDate = null
	,DischargeDate = null
	,GroupingMethodFlag = hrgencounter.GroupingMethodFlag

	--,SUSIdentifier =
					--= 'BMCHT ' 
					--+ cast(encounter.SourcePatientNo as varchar)
					--+ '*' 
					--+ cast(encounter.SourceEncounterNo as varchar)
					--+ '*'
					--+ cast(year(AppointmentTime) as varchar)
					--+ case
					--		when len(datepart(month, AppointmentTime)) < 2
					--		then '0' + cast(datepart(month, AppointmentTime) as varchar)
					--		else (cast(month(AppointmentTime) as varchar))
					--	end
					--+ case
					--		when len(datepart(day, AppointmentTime)) < 2
					--		then '0' + cast(datepart(day, AppointmentTime) as varchar)
					--		else cast(datepart(day, AppointmentTime) as varchar)
					--	end
					--+ case
					--		when datepart(hour, AppointmentTime) = 0 -- needed as ITS does not return 00:00 if no time
					--		then ''
					--		when len(datepart(hour, AppointmentTime)) < 2
					--		then '0' + cast(datepart(hour, AppointmentTime) as varchar)
					--		else cast(datepart(hour, AppointmentTime) as varchar)
					--	end	
					--+ case
					--		when datepart(hour, AppointmentTime) = 0  -- needed as ITS does not return 00:00 if no time
					--		and datepart(minute, AppointmentTime) = 0
					--		then ''
					--		when len(datepart(minute, AppointmentTime)) < 2
					--		then '0' + cast(datepart(minute, AppointmentTime) as varchar)
					--		else cast(datepart(minute, AppointmentTime) as varchar)
					--	end	
					--+ '*'
					--+ encounter.DoctorCode		
				
	--,AttendType = 
	--			case -- OP 24 - Flagging Multi Professional
	--				when mpclinic.ConsultantCode is not null 
	--				and encounter.DirectorateCode not in 
	--													(
	--													'12'
	--													,'20'
	--													,'24'
	--													,'26'
	--													,'90'
	--													)	
	--				then 'MPCL'									
	--				-- OP 38b - Update Multi Professional PODs
	--				when 
	--					case
	--						when hrgencounter.HRGCode in -- OP 38a - Update Local HRGs (OPPROC Clinics)
	--										(
	--										'WF01A'
	--										,'WF01B'
	--										,'UZ01Z'
	--										)
	--						and opprocclinic.HRGCode is not null						
	--						then opprocclinic.HRGCode
	--						else hrgencounter.HRGCode -- returns default HRG if none of the above conditions met
	--					end 
	--					in (
	--						'WF02A'
	--						,'WF02B'
	--						)															
	--				then 'MPCL'
	--				else 'SPCL'
	--			end
	--,ReportFlag = 
	--			case
				
	--/* CAMHS */
	--				when specialty.NationalSpecialtyCode in 
	--														(
	--														'656' 
	--														,'711' 
	--														)
	--				and encounter.PCTCode = '5NT'
	--				then 'CAMHS'
					
	--/* CS */
	--				when 
	--				(
	--				specialty.NationalSpecialtyCode in
	--													(
	--													'320'
	--													,'321'
	--													,'170' -- added as in contract definitions document but not ITS. ITS amends 170 to 172
	--													,'172'
	--													,'361'
	--													,'102' 
	--													,'370'
	--													,'303'
	--													,'253'
	--													)
	--				and encounter.PCTCode in 	
	--										(
	--										'5LH'
	--										,'5JX'
	--										,'5J5'
	--										,'5J4'
	--										,'5J2'
	--										,'5HQ'
	--										,'5HP'
	--										,'5F7'
	--										,'5NE'
	--										,'5NR'
	--										,'5F5'
	--										,'5HG'
	--										,'5NF'
	--										,'5NG'
	--										,'5NH'
	--										,'5NJ'
	--										,'5NK'
	--										,'5NL'
	--										,'5NM'
	--										,'5NN'
	--										,'5NQ'
	--										,'5NT'
	--										,'5CC'
	--										,'5NP'	
	--										,'TAP'
	--										)
	--				and floor(datediff(day, encounter.DateOfBirth, encounter.AppointmentDate)/365.25) < 19 -- child
	--				)
	--				or 
	--				(
	--				specialty.NationalSpecialtyCode in
	--													(
	--													'160' 
	--													,'150' 
	--													,'219' 
	--													,'218' 
	--													,'220'
	--													)
	--				and encounter.PCTCode in 	
	--										(
	--										'5LH'
	--										,'5JX'
	--										,'5J5'
	--										,'5J4'
	--										,'5J2'
	--										,'5HQ'
	--										,'5HP'
	--										,'5F7'
	--										,'5NE'
	--										,'5NR'
	--										,'5F5'
	--										,'5HG'
	--										,'5NF'
	--										,'5NG'
	--										,'5NH'
	--										,'5NJ'
	--										,'5NK'
	--										,'5NL'
	--										,'5NM'
	--										,'5NN'
	--										,'5NQ'
	--										,'5NT'
	--										,'5CC'
	--										,'5NP'	
	--										,'TAP'
	--										)
	--				)
	--				then 'CS'
				
	--/* GENE */
	--				when encounter.PCTCode in 	
	--										(
	--										'5LH'
	--										,'5JX'
	--										,'5J5'
	--										,'5J4'
	--										,'5J2'
	--										,'5HQ'
	--										,'5HP'
	--										,'5F7'
	--										,'5NE'
	--										,'5NR'
	--										,'5F5'
	--										,'5HG'
	--										,'5NF'
	--										,'5NG'
	--										,'5NH'
	--										,'5NJ'
	--										,'5NK'
	--										,'5NL'
	--										,'5NM'
	--										,'5NN'
	--										,'5NQ'
	--										,'5NT'
	--										,'5CC'
	--										,'5NP'	
	--										,'TAP'
	--										)
	--				and specialty.NationalSpecialtyCode = '311'
	--				then 'GENE'
					
	--/* RT */
	--				when encounter.PCTCode in 	
	--										(
	--										'5LH'
	--										,'5JX'
	--										,'5J5'
	--										,'5J4'
	--										,'5J2'
	--										,'5HQ'
	--										,'5HP'
	--										,'5F7'
	--										,'5NE'
	--										,'5NR'
	--										,'5F5'
	--										,'5HG'
	--										,'5NF'
	--										,'5NG'
	--										,'5NH'
	--										,'5NJ'
	--										,'5NK'
	--										,'5NL'
	--										,'5NM'
	--										,'5NN'
	--										,'5NQ'
	--										,'5NT'
	--										,'5CC'
	--										,'5NP'	
	--										,'TAP'
	--										)
	--				and specialty.NationalSpecialtyCode in
	--														(
	--														'102' 
	--														,'361'
	--														)
	--				and floor(datediff(day, encounter.DateOfBirth, encounter.AppointmentDate)/365.25) >= 19 -- adult
	--				then 'RT'
				
	--/* OTHER */
	--				when encounter.PCTCode in 
	--										(												
	--										'5CC'
	--										,'TAP' -- added as replaced 5CC
	--										,'5HP'
	--										,'5J2'
	--										,'5J4'
	--										,'5NE'
	--										,'5NF'
	--										,'5NG'
	--										,'5NH'
	--										,'5NJ'
	--										,'5NK'
	--										,'5NL'
	--										,'5NM'
	--										,'5NN'
	--										,'5NP'
	--										)
	--				and specialty.NationalSpecialtyCode in
	--														(
	--														'102' 
	--														,'361'
	--														) 

	--				then 'OTHER'
					
	--/* CARD */
	--				when encounter.PCTCode in 
	--										(												
	--										'5CC'
	--										,'TAP' -- added as replaced 5CC
	--										,'5HP'
	--										,'5J2'
	--										,'5J4'
	--										,'5NE'
	--										,'5NF'
	--										,'5NG'
	--										,'5NH'
	--										,'5NJ'
	--										,'5NK'
	--										,'5NL'
	--										,'5NM'
	--										,'5NN'
	--										,'5NP'
	--										)
	--				and specialty.NationalSpecialtyCode in
	--														(
	--														'320'
	--														,'170' -- added as in contract definitions document but not ITS. ITS amends 170 to 172
	--														,'172'
	--														) 
	--				and floor(datediff(day, encounter.DateOfBirth, encounter.AppointmentDate)/365.25) >= 19 -- adult
	--				then 'CARD'
					
	--/* CS METABOLICS */
	--				when encounter.PCTCode in 
	--										(												
	--										'5CC'
	--										,'TAP' -- added as replaced 5CC
	--										,'5HP'
	--										,'5J2'
	--										,'5J4'
	--										,'5NE'
	--										,'5NF'
	--										,'5NG'
	--										,'5NH'
	--										,'5NJ'
	--										,'5NK'
	--										,'5NL'
	--										,'5NM'
	--										,'5NN'
	--										,'5NP'
	--										)
	--				and encounter.ClinicCode in 
	--											(
	--											'CJEWMET'
	--											,'CJHWMET'	
	--											,'CAAMMETT'	
	--											) 
	--				and floor(datediff(day, encounter.DateOfBirth, encounter.AppointmentDate)/365.25) < 19 -- child
	--				then 'CS'
					
	----/* EXCLUDE */ -- removed as now part of import process
	----				when 
	----				encounter.ContractSerialNo like '9%' 
	----				or encounter.ContractSerialNo = 'OSV00A' 
	----				or encounter.ContractSerialNo like 'YPP%' 
	----				or encounter.ContractSerialNo like '%CF1'
	----				or encounter. ContractSerialNo like '%GY1' 
	----				or encounter.LocalAdminCategoryCode = 'PAY'					
	----				then 'EXCLUDE'				

	----/* EXCLUDED PATIENTS */
	----				when
	----					exists (
	----							select
	----								1
	----							from
	----								Income.ExcludedPatient excludedpatient
	----							where
	----								excludedpatient.SourcePatientNo = encounter.SourcePatientNo
	----							)
	----				and encounter.PCTCode <> 'WALES'
	----				and encounter.PCTCode not like '6%'
	----				then 'EXCLUDE - EXCLUDED PATIENTS'
	
	----/* TRAFFORD MIDWIFERY CLINICS */
	----				when
	----					exists (
	----							select
	----								1
	----							from
	----								Income.TraffordMidwiferyClinic traffordmidwifery
	----							where
	----								traffordmidwifery.ClinicCode = encounter.ClinicCode
	----							)
	----				and encounter.PCTCode = '5NR'
	----				then 'EXCLUDE - TRAFFORD MIDWIFERY'
					
	----/* HOPE & RENAL CLINICS */				
	----				when encounter.ClinicCode in 
	----											(
	----											'JTL-HOPE'
	----											,'NNG-HOPE'
	----											,'SMMHD'
	----											)
	----				then 'EXCLUDE - HOPE & RENAL'
		
	----/* DIAGNOSTICS */					
	----				when divisionmap.Diagnostic = 'I'
	----				and specialty.NationalSpecialtyCode not in 
	----														(
	----														'320'
	----														,'654'
	----														)
	----				then 'EXCLUDE - DIAGNOSTICS'
					
	----				when divisionmap.Diagnostic = 'X'
	----				and specialty.NationalSpecialtyCode not in 
	----														(
	----														'656' -- not in ITS but requested by Sam 21/05/2012
	----														)
	----				then 'EXCLUDE - DIAGNOSTICS'
					
	----/* BOWL */					
	----				when encounter.SpecialtyCode = 'BOWL'
	----				then 'EXCLUDE - BOWL' 				
	----/* GYN1 */					
	----				when encounter.SpecialtyCode = 'GYN1'
	----				then 'EXCLUDE - GYN1'							
					
	--				else 'CORE'			
	--			end

	,SpecialistCommissioningCode = specialistcommissioning.SpecialistCommissioningCode
from 
	WarehouseReporting.ETL.TImportIncomeOP encounter
	
	--inner join Warehouse.WH.DivisionMap divisionmap
	--on	divisionmap.SiteCode = encounter.SiteCode
	--and	divisionmap.SpecialtyCode = encounter.ReferralSpecialtyCode -- for diagnostic flag
		
	--left outer join Organisation.dbo.[Extended Postcode] postcode
	--on case
	--		when len(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
	--		when len(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
	--		else Encounter.Postcode
	--	end = postcode.postcode
						
	inner join Warehouse.PAS.Specialty specialty
	on specialty.SpecialtyCode = encounter.ReferralSpecialtyCode -- changed 18/07/2012 requested by Lee
	
	left outer join WarehouseReporting.Income.MultiProfessionalClinic mpclinic
	on	mpclinic.ConsultantCode = encounter.DoctorCode
	and mpclinic.TreatmentFunctionCode = specialty.NationalSpecialtyCode
	and mpclinic.ClinicCode = encounter.ClinicCode
	
	--left outer join Organisation.dbo.[Primary Care Organisation] pct
	--on encounter.PCTCode = pct.[Organisation Code]
	
	inner join WarehouseReporting.ETL.HRG4OPEncounter  hrgencounter
	on encounter.EncounterRecno = hrgencounter.EncounterRecno
	
	left outer join WarehouseReporting.WH.[SpecialistHRG] specialisthrg
	on specialisthrg.HRGCode = hrgencounter.HRGCode
	
	left outer join WarehouseReporting.[Income].[OPProcedureClinic] opprocclinic
	on encounter.ClinicCode = opprocclinic.ClinicCode


	inner join [Income].[OPSnapshot] OPsnapshot
	on encounter.SourceUniqueID = OPsnapshot.SourceUniqueID
	and encounter.Period = OPsnapshot.Period 
	and encounter.Position = OPsnapshot.Position
	and encounter.[Version] = OPsnapshot.[Version]
	
	--left outer join PAS.PatientGp patientgp
	--on patientgp.SourcePatientNo = encounter.SourcePatientNo
	--and encounter.AppointmentTime between patientgp.EffectiveFromDate and coalesce(patientgp.EffectiveToDate, getdate())	
	
	/* Specialist Commissioning */	
		
	outer apply 
	
	(
		select top 1
			SpecialistCommissioningCode
		from	
			WarehouseReporting.Income.OPSpecialistCommissioningRuleBase specialistcommissioning
		--from	
		--	WarehouseReporting.Income.OPSpecialistCommissioningRuleBaseDoH specialistcommissioning
		where
			specialistcommissioning.ContextCode = 'CEN||PAS'
			and (encounter.PCTCode = specialistcommissioning.PCTCode or specialistcommissioning.PCTCode is null)
			and
				(
				case
					when floor(datediff(day, encounter.DateOfBirth, encounter.AppointmentDate)/365.25) < 19 then 'C'
					when floor(datediff(day, encounter.DateOfBirth, encounter.AppointmentDate)/365.25) >= 19 then 'A'
				end = specialistcommissioning.AgeCategoryCode
				or specialistcommissioning.AgeCategoryCode is null
				)
			and (specialty.NationalSpecialtyCode = specialistcommissioning.NationalSpecialtyCode or specialistcommissioning.NationalSpecialtyCode is null)
			and (encounter.ClinicCode = specialistcommissioning.ClinicCode or specialistcommissioning.ClinicCode is null)
			and (encounter.ContractSerialNo = specialistcommissioning.CommissioningSerialNo or specialistcommissioning.CommissioningSerialNo is null)
			and (encounter.AppointmentDate between specialistcommissioning.EffectiveFromDate and specialistcommissioning.EffectiveToDate)
	
		order by
			case
				when SpecialistCommissioningCode = 'NSCAG' -- to temporarily handle NSCAG vs GENE or any others (NSCAG takes precedence)
				then 0
				else 1
			end
	
		) specialistcommissioning

		where
			coalesce(encounter.IsWardAttender, 0) = 0


		--where
		--	'BRW300' +	
		--		cast(Encounter.SourcePatientNo as varchar) + 
		--		'*' + 
		--		CAST(Encounter.SourceEncounterNo as varchar) + 
		--		'*' +
		--		convert(varchar , Encounter.AppointmentDate , 112) +
		--		'*' +
		--		cast(
		--			1
		--			as varchar
		--		) = 'BRW3001002785*50*20120419*1'
	
--where -- removed as now part of import process
--	AppointmentStatusCode in 
--							(
--							'ATT'
--							,'1' -- pending investigation why this code is brought back for BMCHT 1007195*11*20110419*AH
--							,'SATT'
--							,'WLK'
--							)
--	and divisionmap.Diagnostic in
--								(
--								'I'
--								,'N'
--								,'X'
--								)
								
	--and encounter.ReferringSpecialtyTypeCode in
	--								(
	--								'I'
	--								,'N'
	--								,'X'
	--								)

								

﻿
create view ETL.HRG4AEInputFile

as

/*	Coalesce used to handle nulls as contracting are passing our A&E CDS 
	from the Warehouse into the Grouper - this mirrors that logic
	
	19/06/2012 repointed at AE.Procedure and Investigations as 'distinct' tables no longer updated
*/

-- 24 none
-- 99 other

select
	Age = -- needed for 2012/13
		floor(
		 datediff(
			 day
			,Encounter.DateOfBirth
			,Encounter.ArrivalDate
		)/365.25	
		)
	,Investigation1 = coalesce(Investigation1.InvestigationCode, '24')
	,Investigation2 = Investigation2.InvestigationCode -- coalesce(Investigation2.InvestigationCode, '24')
	,Investigation3 = Investigation3.InvestigationCode -- coalesce(Investigation3.InvestigationCode, '24')
	,Investigation4 = Investigation4.InvestigationCode -- coalesce(Investigation4.InvestigationCode, '24')
	,Investigation5 = Investigation5.InvestigationCode -- coalesce(Investigation5.InvestigationCode, '24')
	,Investigation6 = Investigation6.InvestigationCode -- coalesce(Investigation6.InvestigationCode, '24')
	,Investigation7 = Investigation7.InvestigationCode -- coalesce(Investigation7.InvestigationCode, '24')
	,Investigation8 = Investigation8.InvestigationCode -- coalesce(Investigation8.InvestigationCode, '24')
	,Investigation9 = Investigation9.InvestigationCode -- coalesce(Investigation9.InvestigationCode, '24')
	,Investigation10 = Investigation10.InvestigationCode -- coalesce(Investigation10.InvestigationCode, '24')
	,Investigation11 = Investigation11.InvestigationCode -- coalesce(Investigation11.InvestigationCode, '24')
	,Investigation12 = Investigation12.InvestigationCode -- coalesce(Investigation12.InvestigationCode, '24')

	,Treatment1 = coalesce(Treatment1.ProcedureCode, '99')
	,Treatment2 = Treatment2.ProcedureCode -- coalesce(Treatment2.TreatmentCode, '99') Treatment2.TreatmentCode
	,Treatment3 = Treatment3.ProcedureCode -- coalesce(Treatment3.TreatmentCode, '99')
	,Treatment4 = Treatment4.ProcedureCode -- coalesce(Treatment4.TreatmentCode, '99')
	,Treatment5 = Treatment5.ProcedureCode -- coalesce(Treatment5.TreatmentCode, '99')
	,Treatment6 = Treatment6.ProcedureCode -- coalesce(Treatment6.TreatmentCode, '99')
	,Treatment7 = Treatment7.ProcedureCode -- coalesce(Treatment7.TreatmentCode, '99')
	,Treatment8 = Treatment8.ProcedureCode -- coalesce(Treatment8.TreatmentCode, '99')
	,Treatment9 = Treatment9.ProcedureCode -- coalesce(Treatment9.TreatmentCode, '99')
	,Treatment10 = Treatment10.ProcedureCode -- coalesce(Treatment10.TreatmentCode, '99')
	,Treatment11 = Treatment11.ProcedureCode -- coalesce(Treatment11.TreatmentCode, '99')
	,Treatment12 = Treatment12.ProcedureCode -- oalesce(Treatment12.TreatmentCode, '99')

--	,Diagnosis1 = Diagnosis1.DiagnosisCode
--	,Diagnosis2 = Diagnosis2.DiagnosisCode
--	,Diagnosis3 = Diagnosis3.DiagnosisCode
--	,Diagnosis4 = Diagnosis4.DiagnosisCode
--	,Diagnosis5 = Diagnosis5.DiagnosisCode
--	,Diagnosis6 = Diagnosis6.DiagnosisCode
--	,Diagnosis7 = Diagnosis7.DiagnosisCode
--	,Diagnosis8 = Diagnosis8.DiagnosisCode
--	,Diagnosis9 = Diagnosis9.DiagnosisCode
--	,Diagnosis10 = Diagnosis10.DiagnosisCode
--	,Diagnosis11 = Diagnosis11.DiagnosisCode
--	,Diagnosis12 = Diagnosis12.DiagnosisCode
--	,Diagnosis13 = Diagnosis13.DiagnosisCode
--	,Diagnosis14 = Diagnosis14.DiagnosisCode
--
	,Disposal =
		Encounter.AttendanceDisposalCode

	,Encounter.EncounterRecno

from
	ETL.TImportIncomeAE Encounter
	
	--AEInvestigation.AESourceUniqueID = Encounter.SourceUniqueID
	--and	AEInvestigation.SequenceNo = 1

left join warehouse.AE.Investigation Investigation1	
on	Investigation1.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation1.SequenceNo = 1

left join warehouse.AE.Investigation Investigation2	
on	Investigation2.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation2.SequenceNo = 2

left join warehouse.AE.Investigation Investigation3	
on	Investigation3.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation3.SequenceNo = 3

left join warehouse.AE.Investigation Investigation4	
on	Investigation4.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation4.SequenceNo = 4

left join warehouse.AE.Investigation Investigation5	
on	Investigation5.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation5.SequenceNo = 5

left join warehouse.AE.Investigation Investigation6	
on	Investigation6.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation6.SequenceNo = 6

left join warehouse.AE.Investigation Investigation7	
on	Investigation7.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation7.SequenceNo = 7

left join warehouse.AE.Investigation Investigation8	
on	Investigation8.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation8.SequenceNo = 8

left join warehouse.AE.Investigation Investigation9	
on	Investigation9.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation9.SequenceNo = 9

left join warehouse.AE.Investigation Investigation10	
on	Investigation10.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation10.SequenceNo = 10

left join warehouse.AE.Investigation Investigation11	
on	Investigation11.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation11.SequenceNo = 11

left join warehouse.AE.Investigation Investigation12	
on	Investigation12.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation12.SequenceNo = 12

/* temp fix while sorting out sequence number issue in AE procedure */
outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment1	
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 1
			) Treatment1

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment2	
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 2
			) Treatment2

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment3
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 3
			) Treatment3

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment4
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 4
			) Treatment4

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment5
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 5
			) Treatment5

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment6
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 6
			) Treatment6

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment7
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 7
			) Treatment7

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment8
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 8
			) Treatment8

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment9
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 9
			) Treatment9
		
outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment10
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 10
			) Treatment10

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment11
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 11
			) Treatment11

outer apply (
			select 
				top 1 *
			from
				warehouse.AE.[Procedure] Treatment12
			where Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
			and	Treatment1.SequenceNo = 12
			) Treatment12


--left join AE.[Procedure] Treatment1	
--on	Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment1.SequenceNo = 1

--left join AE.[Procedure] Treatment2	
--on	Treatment2.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment2.SequenceNo = 2

--left join AE.[Procedure] Treatment3	
--on	Treatment3.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment3.SequenceNo = 3

--left join AE.[Procedure] Treatment4	
--on	Treatment4.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment4.SequenceNo = 4

--left join AE.[Procedure] Treatment5	
--on	Treatment5.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment5.SequenceNo = 5

--left join AE.[Procedure] Treatment6	
--on	Treatment6.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment6.SequenceNo = 6

--left join AE.[Procedure] Treatment7	
--on	Treatment7.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment7.SequenceNo = 7

--left join AE.[Procedure] Treatment8	
--on	Treatment8.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment8.SequenceNo = 8

--left join AE.[Procedure] Treatment9	
--on	Treatment9.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment9.SequenceNo = 9

--left join AE.[Procedure] Treatment10	
--on	Treatment10.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment10.SequenceNo = 10

--left join AE.[Procedure] Treatment11	
--on	Treatment11.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment11.SequenceNo = 11

--left join AE.[Procedure] Treatment12	
--on	Treatment12.AESourceUniqueID = Encounter.SourceUniqueID
--and	Treatment12.SequenceNo = 12

--left join AE.DiagnosisDistinct Diagnosis1	
--on	Diagnosis1.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis1.SequenceNo = 1
--
--left join AE.DiagnosisDistinct Diagnosis2	
--on	Diagnosis2.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis2.SequenceNo = 2
--
--left join AE.DiagnosisDistinct Diagnosis3	
--on	Diagnosis3.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis3.SequenceNo = 3
--
--left join AE.DiagnosisDistinct Diagnosis4	
--on	Diagnosis4.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis4.SequenceNo = 4
--
--left join AE.DiagnosisDistinct Diagnosis5	
--on	Diagnosis5.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis5.SequenceNo = 5
--
--left join AE.DiagnosisDistinct Diagnosis6	
--on	Diagnosis6.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis6.SequenceNo = 6
--
--left join AE.DiagnosisDistinct Diagnosis7	
--on	Diagnosis7.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis7.SequenceNo = 7
--
--left join AE.DiagnosisDistinct Diagnosis8	
--on	Diagnosis8.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis8.SequenceNo = 8
--
--left join AE.DiagnosisDistinct Diagnosis9	
--on	Diagnosis9.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis9.SequenceNo = 9
--
--left join AE.DiagnosisDistinct Diagnosis10	
--on	Diagnosis10.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis10.SequenceNo = 10
--
--left join AE.DiagnosisDistinct Diagnosis11	
--on	Diagnosis11.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis11.SequenceNo = 11
--
--left join AE.DiagnosisDistinct Diagnosis12	
--on	Diagnosis12.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis12.SequenceNo = 12
--
--left join AE.DiagnosisDistinct Diagnosis13	
--on	Diagnosis13.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis13.SequenceNo = 13
--
--left join AE.DiagnosisDistinct Diagnosis14
--on	Diagnosis14.AESourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis14.SequenceNo = 14

--where
--	Encounter.ArrivalDate between
--	(
--	select
--		min(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)
--and
--	(
--	select
--		max(ArrivalDate)
--	from
--		dbo.TLoadAEEncounter
--	)


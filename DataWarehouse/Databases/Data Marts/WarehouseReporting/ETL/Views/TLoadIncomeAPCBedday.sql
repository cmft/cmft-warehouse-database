﻿
CREATE view ETL.TLoadIncomeAPCBedday

as

select
	importbedday.[Period]
	,importbedday.[Position]
	,importbedday.[Version]
	,MonthNumber
	,PCTCode
			= 
			importbedday.PCTCode
				-- concatenates PCT code with non core report flag
				+ 
				case
					when specialistcommissioning.SpecialistCommissioningCode is not null
					then '_' + specialistcommissioning.SpecialistCommissioningCode
					else ''
				end
	,TrustCode = 'RW3'
	,SiteCode = importbedday.SiteCode
	,SpecialtyCode = importbedday.SpecialtyCode
	,HRGCode = importbedday.HRGCode
	,[PODCode]
	,Activity = Bedday
	,CensusDate

	,GpPracticeCode = coalesce(
								importbedday.GpPracticeCode
								,encounter.RegisteredGpPracticeCode
								)
	,PatientCode = encounter.NHSNumber
	,encounter.DateOfBirth
	,encounter.ConsultantCode
	,GpCode = coalesce(
						importbedday.GpCode
						,encounter.RegisteredGpCode
						)
	,LoS = null
	,ReportMasterID = encounter.SourcePatientNo + '*' + encounter.SourceSpellNo + '*' + encounter.SourceEncounterNo
	,Userfield1 = DirectorateCode
	,UserField2 = encounter.DistrictNo
	,UserField3 = encounter.DischargeTime
	,UserField4 = coalesce(
							importbedday.Postcode
							,encounter.Postcode
							--,'ZZ99 3WZ'
							)
	,UserField5 = 
	--				'BMCHT ' 
	--				+ cast(encounter.SourcePatientNo as varchar)
	--				+ '*' 
	--				+ cast(encounter.SourceSpellNo as varchar)
	--				+ '*'
	--				+ cast(encounter.SourceEncounterNo as varchar)
				left(
					'BMCHT ' +
					replace(
						 Encounter.SourceUniqueID
						,'||'
						,'*'
					)
					,50
				)
		,encounter.AdmissionTime
		,encounter.DischargeTime
		,encounter.PatientClassificationCode
		,encounter.[AdmissionMethodCode]

--into Income.Bedday
from
	[ETL].[TImportIncomeBedday] importbedday

inner join 
	APC.Encounter encounter
on	importbedday.ProviderSpellNo = encounter.ProviderSpellNo
and	importbedday.SourceEncounterNo = encounter.SourceEncounterNo

	/* specialist commissioning */
		
	outer apply
	
	(
		select top 1
			SpecialistCommissioningCode
		from 
			WarehouseReporting.Income.APCSpecialistCommissioningRuleBase specialistcommissioning

		--from
		--	WarehouseReporting.Income.APCSpecialistCommissioningRuleBaseDoH specialistcommissioning
		where
		specialistcommissioning.ContextCode = 'CEN||PAS'
		and (importbedday.PCTCode = specialistcommissioning.PCTCode or specialistcommissioning.PCTCode is null)
		and (specialistcommissioning.AgeCategoryCode is null)
		and (specialistcommissioning.PatientCategoryCode is null)
		and (specialistcommissioning.NationalSpecialtyCode is null)
		and (specialistcommissioning.HRGCode is null)
		and (importbedday.PODCode = specialistcommissioning.PODCode or specialistcommissioning.PODCode is null)
		and (specialistcommissioning.ProcedureCode is null)
		and (specialistcommissioning.DiagnosisCode is null)
		and (specialistcommissioning.CommissioningSerialNo is null)
		and (coalesce(encounter.DischargeDate, encounter.AdmissionDate) between specialistcommissioning.EffectiveFromDate and specialistcommissioning.EffectiveToDate)
		
		--order by
		--	case
		--		when SpecialistCommissioningCode like 'BMT%' -- to temporarily handle CS vs BMT
		--		then 0
		--		else 1
		--	end
		) specialistcommissioning

﻿
create view ETL.TLoadIncomeAPCSpell

as

select --top 10 
	SnapshotRecno
	,encounter.Period
	,encounter.Position
	,encounter.[Version]
	,MonthNumber = datepart(Month, dateadd(month, -3, encounter.DischargeDate)) 
	,PCTCode =
				case
					when hrgspell.HRGCode like 'CZ25%' -- IP 113 - Update Staffs Cochlears to Stoke
					and encounter.PCTCode = '5PH'
					then '5PJ'
					else encounter.PCTCode
				end
				-- concatenates PCT code with non core report flag
				+ 
				case
					when encounter.EndWardTypeCode in 
											(
											'BCS' 
											,'BCSC' 
											)
					and encounter.PCTCode = '5F5'
					then '_MLU'
					when specialistcommissioning.SpecialistCommissioningCode is not null
					then '_' + specialistcommissioning.SpecialistCommissioningCode
					else ''
				end
				
	,TrustCode = 'RW3'
	,SiteCode = encounter.SiteCode
	,SpecialtyCode = specialty.NationalSpecialtyCode
	,HRGCode = hrgspell.HRGCode	
	,PatientClassificationCode = encounter.PatientClassificationCode
	,AdmissionMethodCode = NationalAdmissionMethodCode
	,LoS = hrgspell.LOS
	,DischargeDate = encounter.DischargeTime
	,SpecialServiceCode1 = flag.[1]
	,SpecialServiceCode2 = flag.[2]
	,SpecialServiceCode3 = flag.[3]
	,SpecialServiceCode4 = flag.[4]
	,SpecialServiceCode5 = flag.[5]
	,SpecialServiceCode6 = flag.[6]
	,SpecialServiceCode7 = flag.[7]
	,SpecialServiceCode8 = flag.[8]
	,SpecialServiceCode9 = flag.[9]
	,SpecialServiceCode10 = flag.[10]
	,SpecialServiceCode11 = flag.[11]
	,SpecialServiceCode12 = flag.[12]
	,SpecialServiceCode13 = flag.[13]
	,SpecialServiceCode14 = flag.[14]
	,SpecialServiceCode15 = flag.[15]
	,SpecialServiceCode16 = flag.[16]
	,SpecialServiceCode17 = flag.[17]
	,SpecialServiceCode18 = flag.[18]
	,SpecialServiceCode19 = flag.[19]
	,SpecialServiceCode20 = flag.[20]
	,SpecialServiceCode21 = flag.[21]

	,GpPracticeCode = coalesce(
								encounter.GpPracticeCodeAtDischarge
								,encounter.RegisteredGpPracticeCode
								)
	,PatientCode = encounter.NHSNumber

	,encounter.ConsultantCode
	,GpCode = coalesce(
							encounter.GpCodeAtDischarge
							,gp.[NationalCode]
							)
	,encounter.DateOfBirth
	,ReportMasterID = encounter.SourcePatientNo + '*' + encounter.SourceSpellNo + '*' + encounter.SourceEncounterNo
	,UserField1 = 
					case
						when hrgspell.HRGCode like 'CZ25%' --IP 116 - Update Childrens Cochelars to Surg
						and encounter.EndDirectorateCode = 0
						then 10
						else encounter.EndDirectorateCode
					end
	,UserField2 = encounter.DistrictNo
	,UserField3 = encounter.DischargeTime
	,UserField4 = coalesce(
					encounter.PostcodeAtDischarge
					,encounter.Postcode
					--,'ZZ99 3WZ'
					) -- encounter.Postcode
	,UserField5 = 
	--				'BMCHT ' 
	--				+ cast(encounter.SourcePatientNo as varchar)
	--				+ '*' 
	--				+ cast(encounter.SourceSpellNo as varchar)
	--				+ '*'
	--				+ cast(encounter.SourceEncounterNo as varchar)
				left(
					'BMCHT ' +
					replace(
						 Encounter.SourceUniqueID
						,'||'
						,'*'
					)
					,50
				)
			
	,AdmissionDate = encounter.AdmissionTime
	,GroupingMethodFlag
	,PODCode = 
				case
					when specialisthrg.[SpecialistHRGGroupCode] = 'BMT' -- IP 082 - Flag BMT POD
					then specialisthrg.[SpecialistHRGGroupCode]  + case
														when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 -- child
														then 'C'
														else 'A'
													end
					when hrgspell.HRGCode like 'LA01%' -- IP 081 - Flag RT POD
					or hrgspell.HRGCode like 'LA02%' 
					or hrgspell.HRGCode like 'LA03%' 
					then 
						case
							when encounter.EndDirectorateCode = 0 then 'RTP'
							else 'RT'
						end
				end
	
		,SpecialistCommissioningCode = specialistcommissioning.SpecialistCommissioningCode
	
from 
	WarehouseReporting.ETL.TImportIncomeAPC encounter

	inner join WarehouseReporting.ETL.HRG4APCEncounter hrgencounter
	on encounter.EncounterRecNo = hrgencounter.EncounterRecNo
	
	inner join WarehouseReporting.ETL.HRG4APCSpell hrgspell
	on hrgspell.RowNo = hrgencounter.RowNo
	
	inner join Warehouse.PAS.Specialty specialty
	on specialty.SpecialtyCode = encounter.SpecialtyCode
	
	inner join Warehouse.PAS.AdmissionMethod admissionmethod
	on admissionmethod.AdmissionMethodCode = encounter.AdmissionMethodCode

	left outer join Warehouse.[PAS].gp
	on gp.[GpCode] = encounter.RegisteredGpCode

	inner join [Income].[APCSnapshot] apcsnapshot
	on encounter.SourceUniqueID = apcsnapshot.SourceUniqueID
	and encounter.Period = apcsnapshot.Period 
	and encounter.Position = apcsnapshot.Position
	and encounter.[Version] = apcsnapshot.[Version]

	left outer join
				(
				select
					RowNo
					,[1]
					,[2]
					,[3]
					,[4]
					,[5]
					,[6]
					,[7]
					,[8]
					,[9]
					,[10]
					,[11]
					,[12]
					,[13]
					,[14]
					,[15]
					,[16]
					,[17]
					,[18]
					,[19]
					,[20]
					,[21]
				from
					(
					select
						*
					from
						[ETL].[HRG4APCFlag] 
					) flag

					pivot
					(
					min(SpellFlag)
					for SequenceNo in
									(
									[1]
									,[2]
									,[3]
									,[4]
									,[5]
									,[6]
									,[7]
									,[8]
									,[9]
									,[10]
									,[11]
									,[12]
									,[13]
									,[14]
									,[15]
									,[16]
									,[17]
									,[18]
									,[19]
									,[20]
									,[21]
									)
					) sequencepivot
					) flag
					on flag.RowNo = hrgencounter.RowNo 

	left outer join WarehouseReporting.WH.[SpecialistHRG] specialisthrg
	on specialisthrg.HRGCode = hrgspell.HRGCode

	
	/* specialist commissioning */
		
	outer apply
	
	(
		select top 1
			SpecialistCommissioningCode
		from 
			WarehouseReporting.Income.APCSpecialistCommissioningRuleBase specialistcommissioning
		--from 
		--	WarehouseReporting.Income.APCSpecialistCommissioningRuleBaseDoH specialistcommissioning
		where
		specialistcommissioning.ContextCode = 'CEN||PAS'
		and (encounter.PCTCode = specialistcommissioning.PCTCode or specialistcommissioning.PCTCode is null)
		and
			(
			case
				when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 then 'C'
				when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) >= 19 then 'A'
			end = specialistcommissioning.AgeCategoryCode
			or specialistcommissioning.AgeCategoryCode is null
			)
		and (encounter.PatientCategoryCode = specialistcommissioning.PatientCategoryCode or specialistcommissioning.PatientCategoryCode is null)
		and (specialty.NationalSpecialtyCode = cast(specialistcommissioning.NationalSpecialtyCode as varchar) or specialistcommissioning.NationalSpecialtyCode is null)
		and (hrgspell.HRGCode = specialistcommissioning.HRGCode or specialistcommissioning.HRGCode is null)
		and (
			case
				when specialisthrg.[SpecialistHRGGroupCode] = 'BMT' -- IP 082 - Flag BMT POD
				then specialisthrg.[SpecialistHRGGroupCode]  + case
													when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 -- child
													then 'C'
													else 'A'
												end
				when hrgspell.HRGCode like 'LA01%' -- IP 081 - Flag RT POD
				or hrgspell.HRGCode like 'LA02%' 
				or hrgspell.HRGCode like 'LA03%' 
				then 
					case
						when encounter.EndDirectorateCode = 0 then 'RTP'
						else 'RT'
					end
			end = specialistcommissioning.PODCode or specialistcommissioning.PODCode is null)
		and (encounter.PrimaryOperationCode = specialistcommissioning.ProcedureCode or specialistcommissioning.ProcedureCode is null)
		and (encounter.PrimaryDiagnosisCode = specialistcommissioning.DiagnosisCode or specialistcommissioning.DiagnosisCode is null)
		and (encounter.ContractSerialNo = specialistcommissioning.CommissioningSerialNo or specialistcommissioning.CommissioningSerialNo is null)
		and (encounter.DischargeDate between specialistcommissioning.EffectiveFromDate and specialistcommissioning.EffectiveToDate)
		
		order by
			case
				when SpecialistCommissioningCode like 'BMT%' -- to temporarily handle CS vs BMT
				then 0
				else 1
			end
		) specialistcommissioning
		
		
	/* Exclusions */
	
	outer apply 
	
	(
		select 
			top 1 ExclusionCode
		from	
			WarehouseReporting.Income.APCExclusionRuleBase exclusionrulebase
		where
			exclusionrulebase.ContextCode = 'CEN||PAS'
			and (encounter.SiteCode = exclusionrulebase.SiteCode or exclusionrulebase.SiteCode is null)
			and (encounter.PCTCode = exclusionrulebase.PCTCode or exclusionrulebase.PCTCode is null)
			and (
			case
				when specialisthrg.[SpecialistHRGGroupCode] = 'BMT' -- IP 082 - Flag BMT POD
				then specialisthrg.[SpecialistHRGGroupCode]  + case
													when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 -- child
													then 'C'
													else 'A'
												end
				when hrgspell.HRGCode like 'LA01%' -- IP 081 - Flag RT POD
				or hrgspell.HRGCode like 'LA02%' 
				or hrgspell.HRGCode like 'LA03%' 
				then 
					case
						when encounter.EndDirectorateCode = 0 then 'RTP'
						else 'RT'
					end
			end = exclusionrulebase.PODCode or exclusionrulebase.PODCode is null)
			and (hrgspell.HRGCode = exclusionrulebase.HRGCode or exclusionrulebase.HRGCode is null)
			and (encounter.ManagementIntentionCode = exclusionrulebase.ManagementIntentionCode or exclusionrulebase.ManagementIntentionCode is null)
			and (encounter.AdmissionMethodCode = exclusionrulebase.PASAdmissionMethodCode or exclusionrulebase.PASAdmissionMethodCode is null)
			and (encounter.SpecialtyCode = exclusionrulebase.PASSpecialtyCode or exclusionrulebase.PASSpecialtyCode is null)
			and (specialty.NationalSpecialtyCode = exclusionrulebase.NationalSpecialtyCode or exclusionrulebase.NationalSpecialtyCode is null)
			and (coalesce(encounter.NeonatalLevelOfCare, '0') = exclusionrulebase.NeonatalLevelOfCareCode or exclusionrulebase.NeonatalLevelOfCareCode is null)
			and (encounter.PrimaryDiagnosisCode = exclusionrulebase.PrimaryDiagnosisCode or exclusionrulebase.PrimaryDiagnosisCode is null)
			and (encounter.PrimaryOperationCode = exclusionrulebase.PrimaryProcedureCode or exclusionrulebase.PrimaryProcedureCode is null)								
			and (encounter.ContractSerialNo = exclusionrulebase.CommissioningSerialNo or exclusionrulebase.CommissioningSerialNo is null)
			and (encounter.LocalAdminCategoryCode = exclusionrulebase.AdminCategoryCode or exclusionrulebase.AdminCategoryCode is null)			
			and (encounter.FirstEpisodeInSpellIndicator = exclusionrulebase.FirstEpisodeInSpellIndicator or exclusionrulebase.FirstEpisodeInSpellIndicator is null)
			and (datediff(day, encounter.AdmissionDate, encounter.DischargeDate) = exclusionrulebase.LoS or exclusionrulebase.LoS is null)				
			and (encounter.DischargeDate between exclusionrulebase.EffectiveFromDate and coalesce(exclusionrulebase.EffectiveToDate, getdate()))
	
		) exclusionrulebase
		
		where 
			ExclusionCode is null

	/* union needed for adding unbundled HRGs for Chemo */

	union all

	select distinct -- distinct needed as using hrg unbundled can have more than row - can only charge once
	SnapshotRecno
	,encounter.Period
	,encounter.Position
	,encounter.[Version]
	,MonthNumber = datepart(Month, dateadd(month, -3, encounter.DischargeDate)) 
	,PCTCode =
				case
					when hrgspell.HRGCode like 'CZ25%' -- IP 113 - Update Staffs Cochlears to Stoke
					and encounter.PCTCode = '5PH'
					then '5PJ'
					else encounter.PCTCode
				end
				-- concatenates PCT code with non core report flag
				+ 
				case
					when encounter.EndWardTypeCode in 
											(
											'BCS' 
											,'BCSC' 
											)
					and encounter.PCTCode = '5F5'
					then '_MLU'
					when specialistcommissioning.SpecialistCommissioningCode is not null
					then '_' + specialistcommissioning.SpecialistCommissioningCode
					else ''
				end
				
	,TrustCode = 'RW3'
	,SiteCode = encounter.SiteCode
	,SpecialtyCode = specialty.NationalSpecialtyCode
	,HRGCode = hrgunbundled.HRGCode	
	,PatientClassificationCode = encounter.PatientClassificationCode
	,AdmissionMethodCode = NationalAdmissionMethodCode
	,LoS = hrgspell.LOS
	,DischargeDate = encounter.DischargeTime
	,SpecialServiceCode1 = flag.[1]
	,SpecialServiceCode2 = flag.[2]
	,SpecialServiceCode3 = flag.[3]
	,SpecialServiceCode4 = flag.[4]
	,SpecialServiceCode5 = flag.[5]
	,SpecialServiceCode6 = flag.[6]
	,SpecialServiceCode7 = flag.[7]
	,SpecialServiceCode8 = flag.[8]
	,SpecialServiceCode9 = flag.[9]
	,SpecialServiceCode10 = flag.[10]
	,SpecialServiceCode11 = flag.[11]
	,SpecialServiceCode12 = flag.[12]
	,SpecialServiceCode13 = flag.[13]
	,SpecialServiceCode14 = flag.[14]
	,SpecialServiceCode15 = flag.[15]
	,SpecialServiceCode16 = flag.[16]
	,SpecialServiceCode17 = flag.[17]
	,SpecialServiceCode18 = flag.[18]
	,SpecialServiceCode19 = flag.[19]
	,SpecialServiceCode20 = flag.[20]
	,SpecialServiceCode21 = flag.[21]
	,GpPracticeCode = coalesce(
								encounter.GpPracticeCodeAtDischarge
								,encounter.RegisteredGpPracticeCode
								)
	,PatientCode = encounter.NHSNumber

	,encounter.ConsultantCode
	,GpCode = coalesce(
							encounter.GpCodeAtDischarge
							,gp.[NationalCode]
							)
	,encounter.DateOfBirth
	,ReportMasterID = encounter.SourcePatientNo + '*' + encounter.SourceSpellNo + '*' + encounter.SourceEncounterNo
	,UserField1 = 
					case
						when hrgspell.HRGCode like 'CZ25%' --IP 116 - Update Childrens Cochelars to Surg
						and encounter.EndDirectorateCode = 0
						then 10
						else encounter.EndDirectorateCode
					end
	,UserField2 = encounter.DistrictNo
	,UserField3 = encounter.DischargeTime
	,UserField4 = coalesce(
					encounter.PostcodeAtDischarge
					,encounter.Postcode
					) -- encounter.Postcode
	,UserField5 = 
	--				'BMCHT ' 
	--				+ cast(encounter.SourcePatientNo as varchar)
	--				+ '*' 
	--				+ cast(encounter.SourceSpellNo as varchar)
	--				+ '*'
	--				+ cast(encounter.SourceEncounterNo as varchar)
				left(
					'BMCHT ' +
					replace(
						 Encounter.SourceUniqueID
						,'||'
						,'*'
					)
					,50
				)
			
	,AdmissionDate = encounter.AdmissionTime
	,GroupingMethodFlag = coalesce(GroupingMethodFlag,'')
	,PODCode = 
				case
					when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 -- child
					then 'CHEMO_P'
					else 'CHEMO_A'
				end

	,SpecialistCommissioningCode = specialistcommissioning.SpecialistCommissioningCode
		--,hrgunbundled.[SequenceNo]
	
from 
	WarehouseReporting.ETL.TImportIncomeAPC encounter

	inner join WarehouseReporting.ETL.HRG4APCEncounter hrgencounter
	on encounter.EncounterRecNo = hrgencounter.EncounterRecNo
	
	inner join WarehouseReporting.ETL.HRG4APCSpell hrgspell
	on hrgspell.RowNo = hrgencounter.RowNo
	
	inner join Warehouse.PAS.Specialty specialty
	on specialty.SpecialtyCode = encounter.SpecialtyCode
	
	inner join Warehouse.PAS.AdmissionMethod admissionmethod
	on admissionmethod.AdmissionMethodCode = encounter.AdmissionMethodCode

	left outer join Warehouse.[PAS].gp
	on gp.[GpCode] = encounter.RegisteredGpCode

	inner join WarehouseReporting.ETL.HRG4APCUnbundled hrgunbundled
	on hrgunbundled.RowNo = hrgencounter.RowNo 
	--and hrgspell.HRGCode in 
	--						(
	--						'SB97Z'
	--						,'SB97Z/23'
	--						)
	and hrgunbundled.HRGCode in 
								(
								'SB11Z'
								,'SB12Z'
								,'SB13Z'
								,'SB14Z'
								,'SB15Z'
								,'SB17Z'
								)

	left outer join WarehouseReporting.WH.[SpecialistHRG] specialisthrg
	on specialisthrg.HRGCode = hrgspell.HRGCode

	inner join [Income].[APCSnapshot] apcsnapshot
	on encounter.SourceUniqueID = apcsnapshot.SourceUniqueID
	and encounter.Period = apcsnapshot.Period 
	and encounter.Position = apcsnapshot.Position
	and encounter.[Version] = apcsnapshot.[Version]

	left outer join
				(
				select
					RowNo
					,[1]
					,[2]
					,[3]
					,[4]
					,[5]
					,[6]
					,[7]
					,[8]
					,[9]
					,[10]
					,[11]
					,[12]
					,[13]
					,[14]
					,[15]
					,[16]
					,[17]
					,[18]
					,[19]
					,[20]
					,[21]
				from
					(
					select
						*
					from
						[ETL].[HRG4APCFlag] 
					) flag

					pivot
					(
					min(SpellFlag)
					for SequenceNo in
									(
									[1]
									,[2]
									,[3]
									,[4]
									,[5]
									,[6]
									,[7]
									,[8]
									,[9]
									,[10]
									,[11]
									,[12]
									,[13]
									,[14]
									,[15]
									,[16]
									,[17]
									,[18]
									,[19]
									,[20]
									,[21]
									)
					) sequencepivot
					) flag
					on flag.RowNo = hrgencounter.RowNo 

	
	/* specialist commissioning */
		
	outer apply
	
	(
		select top 1
			SpecialistCommissioningCode
		from 
			WarehouseReporting.Income.APCSpecialistCommissioningRuleBase specialistcommissioning
		--from 
		--	WarehouseReporting.Income.APCSpecialistCommissioningRuleBaseDoH specialistcommissioning
		where
		specialistcommissioning.ContextCode = 'CEN||PAS'
		and (encounter.PCTCode = specialistcommissioning.PCTCode or specialistcommissioning.PCTCode is null)
		and
			(
			case
				when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 then 'C'
				when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) >= 19 then 'A'
			end = specialistcommissioning.AgeCategoryCode
			or specialistcommissioning.AgeCategoryCode is null
			)
		and (encounter.PatientCategoryCode = specialistcommissioning.PatientCategoryCode or specialistcommissioning.PatientCategoryCode is null)
		and (specialty.NationalSpecialtyCode = cast(specialistcommissioning.NationalSpecialtyCode as varchar) or specialistcommissioning.NationalSpecialtyCode is null)
		and (hrgspell.HRGCode = specialistcommissioning.HRGCode or specialistcommissioning.HRGCode is null)
		and (
				case
					when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 -- child
					then 'CHEMO_P'
					else 'CHEMO_A'
				end = specialistcommissioning.PODCode or specialistcommissioning.PODCode is null)
		and (encounter.PrimaryOperationCode = specialistcommissioning.ProcedureCode or specialistcommissioning.ProcedureCode is null)
		and (encounter.PrimaryDiagnosisCode = specialistcommissioning.DiagnosisCode or specialistcommissioning.DiagnosisCode is null)
		and (encounter.ContractSerialNo = specialistcommissioning.CommissioningSerialNo or specialistcommissioning.CommissioningSerialNo is null)
		and (encounter.DischargeDate between specialistcommissioning.EffectiveFromDate and specialistcommissioning.EffectiveToDate)
		
		order by
			case
				when SpecialistCommissioningCode like 'BMT%' -- to temporarily handle CS vs BMT
				then 0
				else 1
			end
		) specialistcommissioning
		
		
	/* Exclusions */
	
	outer apply 
	
	(
		select 
			top 1 ExclusionCode
		from	
			WarehouseReporting.Income.APCExclusionRuleBase exclusionrulebase
		where
			exclusionrulebase.ContextCode = 'CEN||PAS'
			and (encounter.SiteCode = exclusionrulebase.SiteCode or exclusionrulebase.SiteCode is null)
			and (encounter.PCTCode = exclusionrulebase.PCTCode or exclusionrulebase.PCTCode is null)
			and (
				case
					when floor(datediff(day, encounter.DateOfBirth, encounter.AdmissionDate)/365.25) < 19 -- child
					then 'CHEMO_P'
					else 'CHEMO_A'
				end = exclusionrulebase.PODCode or exclusionrulebase.PODCode is null)
			and (hrgspell.HRGCode = exclusionrulebase.HRGCode or exclusionrulebase.HRGCode is null)
			and (encounter.ManagementIntentionCode = exclusionrulebase.ManagementIntentionCode or exclusionrulebase.ManagementIntentionCode is null)
			and (encounter.AdmissionMethodCode = exclusionrulebase.PASAdmissionMethodCode or exclusionrulebase.PASAdmissionMethodCode is null)
			and (encounter.SpecialtyCode = exclusionrulebase.PASSpecialtyCode or exclusionrulebase.PASSpecialtyCode is null)
			and (specialty.NationalSpecialtyCode = exclusionrulebase.NationalSpecialtyCode or exclusionrulebase.NationalSpecialtyCode is null)
			and (coalesce(encounter.NeonatalLevelOfCare, '0') = exclusionrulebase.NeonatalLevelOfCareCode or exclusionrulebase.NeonatalLevelOfCareCode is null)
			and (encounter.PrimaryDiagnosisCode = exclusionrulebase.PrimaryDiagnosisCode or exclusionrulebase.PrimaryDiagnosisCode is null)
			and (encounter.PrimaryOperationCode = exclusionrulebase.PrimaryProcedureCode or exclusionrulebase.PrimaryProcedureCode is null)								
			and (encounter.ContractSerialNo = exclusionrulebase.CommissioningSerialNo or exclusionrulebase.CommissioningSerialNo is null)
			and (encounter.LocalAdminCategoryCode = exclusionrulebase.AdminCategoryCode or exclusionrulebase.AdminCategoryCode is null)			
			and (encounter.FirstEpisodeInSpellIndicator = exclusionrulebase.FirstEpisodeInSpellIndicator or exclusionrulebase.FirstEpisodeInSpellIndicator is null)
			and (datediff(day, encounter.AdmissionDate, encounter.DischargeDate) = exclusionrulebase.LoS or exclusionrulebase.LoS is null)				
			and (encounter.DischargeDate between exclusionrulebase.EffectiveFromDate and coalesce(exclusionrulebase.EffectiveToDate, getdate()))
	
		) exclusionrulebase
		
		where 
			ExclusionCode is null
			and hrgspell.HRGCode in 
									(
									'SB97Z'
									,'SB97Z/23'
									)


		

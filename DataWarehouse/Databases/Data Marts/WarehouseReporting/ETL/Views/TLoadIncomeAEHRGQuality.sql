﻿
create view ETL.TLoadIncomeAEHRGQuality

as

select --top 10 
	--MonthNumber = datepart(Month, dateadd(month, -3, encounter.DischargeDate)) 
	SnapshotRecno
	,encounter.Period
	,encounter.Position
	,encounter.[Version]
	,ReportMasterID = case
							when encounter.InterfaceCode = 'SYM'
							then encounter.AttendanceNumber
							else encounter.AttendanceNumber
						end
	,QualityTypeCode
	,QualityCode
	,QualityMessage
	

	
from 
	WarehouseReporting.ETL.TImportIncomeAE encounter

	inner join WarehouseReporting.ETL.HRG4AEEncounter hrgencounter
	on encounter.EncounterRecNo = hrgencounter.EncounterRecNo
	
	inner join WarehouseReporting.ETL.HRG4AEQuality hrgquality
	on hrgquality.RowNo = hrgencounter.RowNo

	inner join [Income].[AESnapshot] AEsnapshot
	on encounter.SourceUniqueID = AEsnapshot.SourceUniqueID
	and encounter.Period = AEsnapshot.Period 
	and encounter.Position = AEsnapshot.Position
	and encounter.[Version] = AEsnapshot.[Version]

	
		

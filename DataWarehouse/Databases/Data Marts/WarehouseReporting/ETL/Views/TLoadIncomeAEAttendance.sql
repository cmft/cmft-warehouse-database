﻿
create view ETL.TLoadIncomeAEAttendance

as

select
	SnapshotRecno
	,encounter.Period
	,encounter.Position
	,encounter.[Version]
	,MonthNumber = datepart(Month, dateadd(month, -3, encounter.ArrivalDate))
	,encounter.PCTCode
	,TrustCode = 'RW3'
	,SiteCode = 
				case
					when encounter.SiteCode = 'RW3MR'
					then 'MRI'
					when encounter.SiteCode = 'RW3DH'
					then 'DENT'
					when encounter.SiteCode = 'RW3RE' 
					then 'REH'
					when encounter.SiteCode = 'RW3RC'
					then 'CHILD'
					when encounter.SiteCode = 'RW3SM'
					then 'SMH'
					else encounter.SiteCode
				end

	,SpecialtyCode = '180'
	,HRGCode = 
				case
					when encounter.InterfaceCode = 'ADAS'
					then 'PCEC'
					else hrgencounter.HRGCode
				end
	--,HRGBandCode = entityxref.XrefEntityCode
	,PODCode = 'AandE'
	,Activity = 1
	,[SpecialServiceCode1] = '99'
	,[SpecialServiceCode2] = null
	,[SpecialServiceCode3] = null
	,GPPracticeCode = 
					coalesce(
							encounter.[GpPracticeCodeAtArrival]
							,encounter.RegisteredGpPracticeCode
							)
	,PatientCode = 
				coalesce(
					left(encounter.NHSNumber, 3) + ' ' + substring(encounter.NHSNumber, 4, 3) + ' ' + right(encounter.NHSNumber, 4)
					,
					patient.NHSNumber
					)
	,DateofBirth = encounter.DateofBirth	
	,ConsultantCode = 
					case
						when encounter.InterfaceCode = 'SYM'
						then left(StaffMember.LoginId, 3)
						else encounter.StaffMemberCode
					end
	,GpCode = 
			coalesce(
				encounter.[GpCodeAtArrival]
				,encounter.RegisteredGpCode
				)
	,LoS = null
	,ReportMasterID = case
							when encounter.InterfaceCode = 'SYM'
							then encounter.AttendanceNumber
							else encounter.AttendanceNumber
						end
	,Userfield1 = case
					when encounter.InterfaceCode = 'SYM'
					then 
						case
							when left(encounter.AttendanceNumber, 1) = 'P' then '0'
							when left(encounter.AttendanceNumber, 1) = 'E' then '40'
							when left(encounter.AttendanceNumber, 1) = 'M' then '90'
							when left(encounter.AttendanceNumber, 3) = 'SMH' then '30'
							when left(encounter.AttendanceNumber, 3) = 'DEN' then '50'
							else '9'
						end
					when encounter.InterfaceCode = 'PAS'
					then
						case
							when left(encounter.AttendanceNumber, 3) = 'SMH' then '30'
							when left(encounter.AttendanceNumber, 3) = 'DEN' then '50'
							else '9'
						end
					when encounter.InterfaceCode = 'ADAS'
					then '90'
					else '9'
				end					
	,Userfield2 = encounter.DistrictNo
	,Userfield3 = encounter.ArrivalTime
	,Userfield4 = 
				coalesce(
				encounter.[PostCodeAtArrival]
				,encounter.PostCode
				)
	,Userfield5 =
		left('BRW300' + Encounter.SourceUniqueID, 35)
	--,InterfaceCode = encounter.InterfaceCode
from 
	WarehouseReporting.ETL.TImportIncomeAE encounter
	
	inner join WarehouseReporting.ETL.HRG4AEEncounter hrgencounter
	on encounter.EncounterRecno = hrgencounter.EncounterRecno
	
	left outer join Warehouse.dbo.EntityXref entityxref
	on entityxref.EntityCode = hrgencounter.HRGCode
	and entityxref.EntityTypeCode = 'AEHRGCODE'
	and entityxref.XrefEntityTypeCode = 'AEHRGBAND'
	
	left outer join Warehouse.AE.StaffMember StaffMember
	on cast(StaffMember.StaffMemberCode as varchar) = Encounter.StaffMemberCode	

	left outer join Warehouse.PAS.Patient
	on	Patient.DistrictNo = Encounter.DistrictNo

	inner join [Income].[AESnapshot] AEsnapshot
	on encounter.SourceUniqueID = AEsnapshot.SourceUniqueID
	and encounter.Period = AEsnapshot.Period 
	and encounter.Position = AEsnapshot.Position
	and encounter.[Version] = AEsnapshot.[Version]
	
	--left outer join warehouse.PAS.PatientAddress patientaddressatarrival
	--on patientaddressatarrival.SourcePatientNo = Patient.SourcePatientNo
	--and encounter.ArrivalTime between patientaddressatarrival.EffectiveFromDate and coalesce(patientaddressatarrival.EffectiveToDate, getdate())
	
	--left outer join  warehouse.PAS.PatientGp EpisodicGpAtArrival
	--on EpisodicGpAtArrival.SourcePatientNo = Patient.SourcePatientNo
	--and Encounter.ArrivalDate between EpisodicGpAtArrival.EffectiveFromDate and coalesce(EpisodicGpAtArrival.EffectiveToDate,getdate())
	

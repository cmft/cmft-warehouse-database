﻿
create view ETL.TLoadIncomeAPCHRGFlag

as

select --top 10 
	--MonthNumber = datepart(Month, dateadd(month, -3, encounter.DischargeDate)) 
	SnapshotRecno
	,encounter.Period
	,encounter.Position
	,encounter.[Version]
	,SUSIdentifier = 
					--'BMCHT ' 
					--+ cast(encounter.SourcePatientNo as varchar)
					--+ '*' 
					--+ cast(encounter.SourceSpellNo as varchar)
					--+ '*'
					--+ cast(encounter.SourceEncounterNo as varchar)
					
				left(
						'BMCHT ' +
						replace(
							 Encounter.SourceUniqueID
							,'||'
							,'*'
						)
						,50
					)
	,hrgflag.[SequenceNo]
	,hrgflag.SpellFlag
	
	
from 
	WarehouseReporting.ETL.TImportIncomeAPC encounter

	inner join WarehouseReporting.ETL.HRG4APCEncounter hrgencounter
	on encounter.EncounterRecNo = hrgencounter.EncounterRecNo
	
	inner join WarehouseReporting.[ETL].[HRG4APCFlag] hrgflag
	on hrgflag.RowNo = hrgencounter.RowNo
	
	inner join [Income].[APCSnapshot] apcsnapshot
	on encounter.SourceUniqueID = apcsnapshot.SourceUniqueID
	and encounter.Period = apcsnapshot.Period 
	and encounter.Position = apcsnapshot.Position
	and encounter.[Version] = apcsnapshot.[Version]
	

﻿
create view ETL.HRG4APCInputFile

as


select
	 Encounter.ProviderCode
	,Encounter.ProviderSpellNo

	,EpisodeNo =
		Encounter.SourceEncounterNo
	,EpisodeAge =
		--datediff(
		--	 year
		--	,Encounter.DateOfBirth
		--	,Encounter.EpisodeStartDate
		--)
		
		 floor(
		 datediff(
			 day
			,Encounter.DateOfBirth
			,Encounter.EpisodeStartDate
		)/365.25	
		)
	

	,SexCode =
		case
		when Encounter.SexCode = 'M' then '1' 
		when Encounter.SexCode = 'F' then '2' 
		else '9'
		end

	,Encounter.PatientClassificationCode

	,AdmissionSourceCode =
		AdmissionSource.NationalAdmissionSourceCode

	,AdmissionMethodCode =
		AdmissionMethod.NationalAdmissionMethodCode

	,DischargeDestinationCode =
		coalesce(
		DischargeDestination.NationalDischargeDestinationCode
		,'99'
		)

	,DischargeMethodCode =
		DischargeMethod.NationalDischargeMethodCode

	,EpisodeDuration =
		LoEAdjusted -- just to validate against ITS process
		--case
		--when LoEAdjusted <= 0 then 1
		--else LoEAdjusted
		--end
		
		--case
		--when datediff(day, EpisodeStartDate, EpisodeEndDate) <= 0 then 1
		--else datediff(day, EpisodeStartDate, EpisodeEndDate)
		--end

	,MainSpecialtyCode =
		MainSpecialty.NationalSpecialtyCode

	--,NeonatalLevelOfCareCode =
	--	case
	--	when Encounter.NeonatalLevelOfCare is null then '0'
	--	when Encounter.NeonatalLevelOfCare = 'N' then '0'
	--	when Encounter.NeonatalLevelOfCare = 'S' then '1'
	--	when Encounter.NeonatalLevelOfCare = 'H' then '2'
	--	when Encounter.NeonatalLevelOfCare = 'M' then '3'
	--	else '0'
	--	end
	,NeonatalLevelOfCareCode = -- we have a combination of codes in the warehouse
		case
		when Encounter.NeonatalLevelOfCare is null then '0'
		when Encounter.NeonatalLevelOfCare in ('0', 'N') then '0'
		when Encounter.NeonatalLevelOfCare in ('1', 'S') then '1'
		when Encounter.NeonatalLevelOfCare in ('2', 'H') then '2'
		when Encounter.NeonatalLevelOfCare in ('3', 'M') then '3'
		else '0'
		end

--National Codes:

--0 Normal Care: Care given by the mother or mother substitute with medical and neonatal nursing advice if needed. 
--1 Special Care: Care given in a special care nursery, transitional care ward or postnatal ward which provides care and treatment exceeding normal routine care. Some aspects of special care can be undertaken by a mother supervised by qualified nursing staff. Special nursing care includes support and education of the infant's parent(s). 
--2 Level 2 Intensive Care (High Dependency Intensive Care): Care given in an intensive or special care nursery which provides continuous skilled supervision by qualified and specially trained nursing staff who may care for more babies than in Level 1 Intensive Care. Medical supervision is not so immediate as in Level 1 Intensive Care. Care includes support of the infant's parent(s). 
--3 Level 1 Intensive Care (Maximal Intensive Care): Care given in an intensive care nursery which provides continuous skilled supervision by qualified and specially trained nursing and medical staff. Such care includes support of the infant's parent(s). 
 
	,TreatmentFunctionCode =
		TreatmentSpecialty.NationalSpecialtyCode

	

	,Diagnosis1 =
	left(
		replace(replace(Encounter.PrimaryDiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis2 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode1, '.', ''), ',', '')
		replace(replace(Encounter.SubsidiaryDiagnosisCode, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis3 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode2, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode1, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis4 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode3, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode2, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis5 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode4, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode3, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis6 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode5, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode4, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis7 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode6, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode5, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis8 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode7, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode6, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis9 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode8, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode7, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis10 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode9, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode8, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis11 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode10, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode9, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis12 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode11, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode10, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis13 =
	left(
		--replace(replace(Encounter.SecondaryDiagnosisCode12, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode11, '.', ''), ',', '')
		, 5
	) 

	,Diagnosis14 =
	left(
		--replace(replace(Diagnosis14.DiagnosisCode, '.', ''), ',', '')
		replace(replace(Encounter.SecondaryDiagnosisCode12, '.', ''), ',', '')
		, 5
	) 

	,Operation1 =
	left(
		replace(replace(Encounter.PrimaryOperationCode, '.', ''), ',', '')
		, 4
	) 
	,Operation2 =
	left(
		replace(replace(Encounter.SecondaryOperationCode1, '.', ''), ',', '')
		, 4
	) 
	,Operation3 =
	left(
		replace(replace(Encounter.SecondaryOperationCode2, '.', ''), ',', '')
		, 4
	) 
	,Operation4 =
	left(
		replace(replace(Encounter.SecondaryOperationCode3, '.', ''), ',', '')
		, 4
	) 
	,Operation5 =
	left(
		replace(replace(Encounter.SecondaryOperationCode4, '.', ''), ',', '')
		, 4
	) 
	,Operation6 =
	left(
		replace(replace(Encounter.SecondaryOperationCode5, '.', ''), ',', '')
		, 4
	) 
	,Operation7 =
	left(
		replace(replace(Encounter.SecondaryOperationCode6, '.', ''), ',', '')
		, 4
	) 
	,Operation8 =
	left(
		replace(replace(Encounter.SecondaryOperationCode7, '.', ''), ',', '')
		, 4
	) 
	,Operation9 =
	left(
		replace(replace(Encounter.SecondaryOperationCode8, '.', ''), ',', '')
		, 4
	) 
	,Operation10 =
	left(
		replace(replace(Encounter.SecondaryOperationCode9, '.', ''), ',', '')
		, 4
	) 
	,Operation11 =
	left(
		replace(replace(Encounter.SecondaryOperationCode10, '.', ''), ',', '')
		, 4
	) 
	,Operation12 =
	left(
		replace(replace(Encounter.SecondaryOperationCode11, '.', ''), ',', '')
		, 4
	) 
	
	,CriticalCareDays = 0

	,RehabilitationDays = 0

	,SpecialistPalliativeCareDays = 0

--	,CriticalCareDays =
--	case
--	when Encounter.InterfaceCode = 'PAS'
--	then
--		case
--		when Encounter.StartWardTypeCode = 'CCU'
--		then datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)
--		else 0
--		end
--	when CriticalCare.CriticalCareDays is null then 0
--	when CriticalCare.CriticalCareDays = 0 then 1 --if there is CC < 1 whole day then set to 1 (see guidance)
--	else CriticalCare.CriticalCareDays
--	end
--
--	,RehabilitationDays =
--	case
--	when
--	Encounter.InterfaceCode = 'PAS'
--	and	Encounter.StartWardTypeCode = 'SEYM'
--	then datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)
--	else 0
--	end
--
--	,SpecialistPalliativeCareDays = 0

	,Encounter.EncounterRecno
	,Encounter.EpisodeEndDate

from
	ETL.TImportIncomeAPC Encounter

--left join APC.Operation Operation2
--on	Operation2.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation2.SequenceNo = 1

--left join APC.Operation Operation3
--on	Operation3.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation3.SequenceNo = 2

--left join APC.Operation Operation4
--on	Operation4.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation4.SequenceNo = 3

--left join APC.Operation Operation5
--on	Operation5.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation5.SequenceNo = 4

--left join APC.Operation Operation6
--on	Operation6.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation6.SequenceNo = 5

--left join APC.Operation Operation7
--on	Operation7.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation7.SequenceNo = 6

--left join APC.Operation Operation8
--on	Operation8.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation8.SequenceNo = 7

--left join APC.Operation Operation9
--on	Operation9.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation9.SequenceNo = 8

--left join APC.Operation Operation10
--on	Operation10.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation10.SequenceNo = 9

--left join APC.Operation Operation11
--on	Operation11.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation11.SequenceNo = 10

--left join APC.Operation Operation12
--on	Operation12.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Operation12.SequenceNo = 11

--left join APC.Diagnosis Diagnosis2
--on	Diagnosis2.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis2.SequenceNo = 1

--left join APC.Diagnosis Diagnosis3
--on	Diagnosis3.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis3.SequenceNo = 2

--left join APC.Diagnosis Diagnosis4
--on	Diagnosis4.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis4.SequenceNo = 3

--left join APC.Diagnosis Diagnosis5
--on	Diagnosis5.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis5.SequenceNo = 4

--left join APC.Diagnosis Diagnosis6
--on	Diagnosis6.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis6.SequenceNo = 5

--left join APC.Diagnosis Diagnosis7
--on	Diagnosis7.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis7.SequenceNo = 6

--left join APC.Diagnosis Diagnosis8
--on	Diagnosis8.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis8.SequenceNo = 7

--left join APC.Diagnosis Diagnosis9
--on	Diagnosis9.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis9.SequenceNo = 8

--left join APC.Diagnosis Diagnosis10
--on	Diagnosis10.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis10.SequenceNo = 9

--left join APC.Diagnosis Diagnosis11
--on	Diagnosis11.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis11.SequenceNo = 10

--left join APC.Diagnosis Diagnosis12
--on	Diagnosis12.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis12.SequenceNo = 11

--left join APC.Diagnosis Diagnosis13
--on	Diagnosis13.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis13.SequenceNo = 12

--left join APC.Diagnosis Diagnosis14
--on	Diagnosis14.APCSourceUniqueID = Encounter.SourceUniqueID
--and	Diagnosis14.SequenceNo = 13

left join Warehouse.PAS.Specialty TreatmentSpecialty
on	TreatmentSpecialty.SpecialtyCode = Encounter.SpecialtyCode

left join Warehouse.PAS.Consultant Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

left join Warehouse.PAS.Specialty MainSpecialty
on	MainSpecialty.SpecialtyCode = Consultant.MainSpecialtyCode
--
--left join WH.Specialty WHSpecialty
--on	WHSpecialty.SpecialtyCode = Specialty.NationalSpecialtyCode

left join Warehouse.PAS.AdmissionSource AdmissionSource
on	AdmissionSource.AdmissionSourceCode = Encounter.AdmissionSourceCode

left join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

left join Warehouse.PAS.DischargeDestination DischargeDestination
on	DischargeDestination.DischargeDestinationCode = Encounter.DischargeDestinationCode

left join Warehouse.PAS.DischargeMethod DischargeMethod
on	DischargeMethod.DischargeMethodCode = Encounter.DischargeMethodCode

--left join 
--	(
--	select
--		WardStay.EncounterRecno
--		,sum(
--			datediff(day,
--				case
--				when WardStay.EpisodeStartDate > WardStay.WardStayStartDate
--				then WardStay.EpisodeStartDate
--				else WardStay.WardStayStartDate
--				end
--				,case
--				when WardStay.EpisodeEndDate > WardStay.WardStayEndDate
--				then WardStay.WardStayEndDate
--				else WardStay.EpisodeEndDate
--				end
--			)
--		) CriticalCareDays
--	from
--		APC.WardStay WardStay
--
--	inner join APC.CriticalCareWard
--	on	CriticalCareWard.WardCode = WardStay.WardCode
--
--	group by
--		WardStay.EncounterRecno
--
--	) CriticalCare
--on	CriticalCare.EncounterRecno = Encounter.EncounterRecno

--where 
--	exists
--		(
--		select
--			1
--		from
--			APC.Encounter Spell
--		where
--			Spell.ProviderSpellNo = Encounter.ProviderSpellNo
--		and	Encounter.EpisodeEndDate > dateadd(year, -1, GETDATE())
--		)

where
	Encounter.ExcludeFlag is null
	and ExcludeBedDayFlag is null
	and ExcludePatientFlag is null
		

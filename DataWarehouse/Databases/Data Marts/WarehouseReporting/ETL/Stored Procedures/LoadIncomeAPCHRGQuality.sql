﻿
create proc ETL.LoadIncomeAPCHRGQuality

as

insert into WarehouseReporting.[Income].[APCHRGQuality]

(
SnapshotRecno
,[Period]
,[Position]
,[Version]
,[SUSIdentifier]
,[QualityTypeCode]
,[QualityCode]
,[QualityMessage]
)

select
	SnapshotRecno
	,[Period]
	,[Position]
	,[Version]
	,[SUSIdentifier]
	,[QualityTypeCode]
	,[QualityCode]
	,[QualityMessage]
from
	WarehouseReporting.[ETL].[TLoadIncomeAPCHRGQuality]
	


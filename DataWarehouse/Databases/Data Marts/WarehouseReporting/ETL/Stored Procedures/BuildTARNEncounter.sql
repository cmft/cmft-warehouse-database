﻿CREATE PROCEDURE [ETL].[BuildTARNEncounter]
AS
BEGIN

	TRUNCATE TABLE WarehouseReporting.TARN.Invalid_Values 
	TRUNCATE TABLE WarehouseReporting.TARN.Encounter
	TRUNCATE TABLE WarehouseReporting.TARN.Interventions
	TRUNCATE TABLE WarehouseReporting.TARN.Observations

	EXEC TARN.CreateTARNEncounter Null --20min

	EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Encounter'; --20min
	EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Interventions';
	EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Observations';

 -- SET WHETHER IT'S AN INVALID OBSERVATION
 -- If there is an Observation with no invalid columns, then set as a valid column
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Observation_type = 3 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Observations]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('Observations')  	 

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Observation_type = 1 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Observations]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDObservations')  

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Observation_type = 2 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Observations]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCObservations')   


 ---- SET WHETHER IT'S AN INVALID INTERVENTION
 ---- If there is an Intervention with no invalid columns, then set as a valid column
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Intervention_type = 3 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Interventions]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('Interventions')  

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Intervention_type = 1 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Interventions]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDInterventions')  
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Intervention_type = 2 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Interventions]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCInterventions')  

		
END

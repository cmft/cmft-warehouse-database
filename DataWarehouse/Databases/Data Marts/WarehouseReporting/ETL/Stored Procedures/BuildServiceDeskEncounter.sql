﻿

CREATE PROCEDURE [ETL].[BuildServiceDeskEncounter]

AS

set transaction isolation level read uncommitted;
set dateformat dmy

--declare @StartTime datetime
--declare @Elapsed int
--declare @RowsDeleted Int
--declare @RowsInserted Int
--declare @Stats varchar(255)
--declare @from datetime
--declare @to datetime

--select @StartTime = getdate()


TRUNCATE TABLE ETL.BaseServiceDeskEncounter

-- Insert new rows into destination table
INSERT INTO ETL.BaseServiceDeskEncounter
(
	 EncounterRecNo
	,SourceUniqueID
	,Request
	,StartTime
	,DeadlineTime
	,ResolvedTime
	,EndTime
	,RequestStatus
	,RequestStatusGroup
	,ResolvedFlag
	,BreachStatus
	,PredictedBreachStatus
	,ResolvedDurationMinutes
	,RequestDurationMinutes
	,TargetDurationMinutes
	,BreachValue
	,Category
	,Workgroup
	,CreatedBy
	,AssignedTo
	,CreatedTime
	,Priority
	,Template
	,Classification
	,ClosureDescription
	,AssignmentCount
	,CallerFullName
	,CallerEmail
	,CallerOrganization
	,SolutionNotes
	,History
	,Impact
	,PriorityCode
)
SELECT 
	 EncounterRecNo
	,SourceUniqueID
	,Request
	,StartTime
	,DeadlineTime
	,ResolvedTime
	,EndTime
	,RequestStatus
	,RequestStatusGroup
	,ResolvedFlag
	,BreachStatus
	,PredictedBreachStatus
	,ResolvedDurationMinutes
	,RequestDurationMinutes
	,TargetDurationMinutes
	,BreachValue
	,Category
	,Workgroup
	,CreatedBy
	,AssignedTo
	,CreatedTime
	,Priority
	,Template
	,Classification
	,ClosureDescription
	,AssignmentCount
	,CallerFullName
	,CallerEmail
	,CallerOrganization
	,SolutionNotes
	,History
	,Impact
	,PriorityCode
FROM 
	warehouse.ServiceDesk.Encounter Encounter

--select @RowsInserted = @@rowcount

--select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

--select @Stats = 
--	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
--	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

--exec WriteAuditLogEvent 'LoadBaseServiceDesk', @Stats, @StartTime

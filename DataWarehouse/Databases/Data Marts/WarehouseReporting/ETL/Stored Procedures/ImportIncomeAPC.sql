﻿
create proc ETL.ImportIncomeAPC

(
	@start datetime
	,@end datetime
	,@period varchar(20) -- format example 2012/2013 Month 1
	,@position varchar(50) -- flex/freeze
	,@version int 
)

--with recompile

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255)

declare	@calendarstart smalldatetime
declare @calendarend smalldatetime
		
select @StartTime = getdate()
			

/* get range for bedday calendar table */

--select @calendarstart =
--						(
--						select 
--							min(AdmissionTime)
--						from 
--							warehousereporting.[APC].[Encounter]
--						where
--							DischargeDate between @start and @end
--							--and (ProviderSpellNo = @providerspellno or @providerspellno is null)
--						)

--select @calendarend =
--						(
--						select 
--							max(DischargeTime)
--						from 
--							warehousereporting.[APC].[Encounter]
--						where
--							DischargeDate between @start and @end
--							--and (ProviderSpellNo = @providerspellno or @providerspellno is null)
--						)

--select 
--	@calendarstart	
--	,@calendarend;
	
/* Create calendar table that will help to apportion beddays to consultant episodes */

--truncate table Income.Calendar;

--if object_id('Income.Calendar') is not null
--drop table Income.Calendar;

--create table Income.Calendar
--(
--	TheDateTime smalldatetime not null primary key
--);

--with cte_dategenerator
--as
--(
--  select 
--	TheDateTime = @calendarstart
--  union all
--  select
--	dateadd(minute, 1, TheDateTime)
--  from
--	cte_dategenerator
--  where
--	TheDateTime <= @calendarend
--)

--insert into Income.Calendar

--select
--	TheDateTime 
--from cte_dategenerator

--option (maxrecursion 0)


--if object_id('ETL.TImportIncomeAPC') is not null
--drop table ETL.TImportIncomeAPC
	
truncate table ETL.TImportIncomeAPC


insert into WarehouseReporting.[ETL].[TImportIncomeAPC]

   (
	[Period]
   ,[Position]
   ,[Version]
   ,[EncounterRecno]
   ,[SourceUniqueID]
   ,[SourcePatientNo]
   ,[SourceSpellNo]
   ,[SourceEncounterNo]
   ,[ProviderSpellNo]
   ,[PatientForename]
   ,[PatientSurname]
   ,[DateOfBirth]
   ,[DateOfDeath]
   ,[SexCode]
   ,[NHSNumber]
   ,[Postcode]
   ,[AdmissionDate]
   ,[DischargeDate]
   ,[EpisodeStartDate]
   ,[EpisodeEndDate]
   ,[EndWardTypeCode]
   ,[RegisteredGpCode]
   ,[RegisteredGpPracticeCode]
   ,[SiteCode]
   ,[AdmissionMethodCode]
   ,[AdmissionSourceCode]
   ,[PatientClassificationCode]
   ,[ManagementIntentionCode]
   ,[DischargeMethodCode]
   ,[DischargeDestinationCode]
   ,[AdminCategoryCode]
   ,[ConsultantCode]
   ,[SpecialtyCode]
   ,[NeonatalLevelOfCare]
   ,[IsWellBabyFlag]
   ,[ClinicalCodingStatus]
   ,[ClinicalCodingCompleteDate]
   ,[PrimaryDiagnosisCode]
   ,[SubsidiaryDiagnosisCode]
   ,[SecondaryDiagnosisCode1]
   ,[SecondaryDiagnosisCode2]
   ,[SecondaryDiagnosisCode3]
   ,[SecondaryDiagnosisCode4]
   ,[SecondaryDiagnosisCode5]
   ,[SecondaryDiagnosisCode6]
   ,[SecondaryDiagnosisCode7]
   ,[SecondaryDiagnosisCode8]
   ,[SecondaryDiagnosisCode9]
   ,[SecondaryDiagnosisCode10]
   ,[SecondaryDiagnosisCode11]
   ,[SecondaryDiagnosisCode12]
   ,[PrimaryOperationCode]
   ,[SecondaryOperationCode1]
   ,[SecondaryOperationCode2]
   ,[SecondaryOperationCode3]
   ,[SecondaryOperationCode4]
   ,[SecondaryOperationCode5]
   ,[SecondaryOperationCode6]
   ,[SecondaryOperationCode7]
   ,[SecondaryOperationCode8]
   ,[SecondaryOperationCode9]
   ,[SecondaryOperationCode10]
   ,[SecondaryOperationCode11]
   ,[ContractSerialNo]
   ,[CodingCompleteDate]
   ,[PurchaserCode]
   ,[ProviderCode]
   ,[EpisodeStartTime]
   ,[EpisodeEndTime]
   ,[InterfaceCode]
   ,[CasenoteNumber]
   ,[NHSNumberStatusCode]
   ,[AdmissionTime]
   ,[DischargeTime]
   ,[TransferFrom]
   ,[DistrictNo]
   ,[IntendedPrimaryOperationCode]
   ,[LocalAdminCategoryCode]
   ,[EpisodicGpPracticeCode]
   ,[PCTCode]
   ,[PatientCategoryCode]
   ,[EndDirectorateCode]
   ,[FirstEpisodeInSpellIndicator]
   ,[GpCodeAtDischarge]
   ,[GpPracticeCodeAtDischarge]
	,[PostCodeAtDischarge]
   --,[Exclude]
   ,[ExcludeFlag]
   ,[ExcludeBedDayFlag]
   ,[ExcludePatientFlag]
   ,[LoEDays]
   ,[LoEMinutes]
   ,[BedDayDays]
   ,[BedDayMinutes]
   ,[AugmentedCareDays]
   ,[AugmentedCareMinutes]
   ,[LoEAdjusted]
   ,[DateAdded]
   )

select 
	Period = @period
	,Position = @position
	,[Version] = @version
	,encounter.[EncounterRecno]
	,[SourceUniqueID]
	,encounter.[SourcePatientNo]
	,encounter.[SourceSpellNo]
	,encounter.[SourceEncounterNo]
	,encounter.[ProviderSpellNo]
	,[PatientForename]
	,[PatientSurname]
	,[DateOfBirth]
	,[DateOfDeath]
	,[SexCode]
	,[NHSNumber]
	,encounter.[Postcode]
	,[AdmissionDate]
	,[DischargeDate]
	,[EpisodeStartDate]
	,[EpisodeEndDate]
	,[EndWardTypeCode]
	,[RegisteredGpCode]
	,[RegisteredGpPracticeCode]
	,[SiteCode]
	,[AdmissionMethodCode]
	,[AdmissionSourceCode]
	--,[PatientClassificationCode] = case
	--									when encounter.ManagementIntentionCode = 'R'  -- IP 045 - REG to EL
	--									and datediff(day, encounter.[AdmissionDate],encounter.DischargeDate) > 0
	--									then 1
	--									else [PatientClassificationCode]
	--								end
	,[PatientClassificationCode]
	,[ManagementIntentionCode]
	,[DischargeMethodCode]
	,[DischargeDestinationCode]
	,[AdminCategoryCode]
	,[ConsultantCode]
	,encounter.[SpecialtyCode]
	,[NeonatalLevelOfCare]
	,[IsWellBabyFlag]
	,[ClinicalCodingStatus]
	,[ClinicalCodingCompleteDate]
	,[PrimaryDiagnosisCode]
	,[SubsidiaryDiagnosisCode]
	,[SecondaryDiagnosisCode1]
	,[SecondaryDiagnosisCode2]
	,[SecondaryDiagnosisCode3]
	,[SecondaryDiagnosisCode4]
	,[SecondaryDiagnosisCode5]
	,[SecondaryDiagnosisCode6]
	,[SecondaryDiagnosisCode7]
	,[SecondaryDiagnosisCode8]
	,[SecondaryDiagnosisCode9]
	,[SecondaryDiagnosisCode10]
	,[SecondaryDiagnosisCode11]
	,[SecondaryDiagnosisCode12]
	,[PrimaryOperationCode]
	,[SecondaryOperationCode1]
	,[SecondaryOperationCode2]
	,[SecondaryOperationCode3]
	,[SecondaryOperationCode4]
	,[SecondaryOperationCode5]
	,[SecondaryOperationCode6]
	,[SecondaryOperationCode7]
	,[SecondaryOperationCode8]
	,[SecondaryOperationCode9]
	,[SecondaryOperationCode10]
	,[SecondaryOperationCode11]
	,[ContractSerialNo]
	,[CodingCompleteDate]
	,[PurchaserCode]
	,[ProviderCode]
	,[EpisodeStartTime]
	,[EpisodeEndTime]
	--,[EpisodicGpCode]
	,[InterfaceCode]
	,[CasenoteNumber]
	,[NHSNumberStatusCode]
	,[AdmissionTime]
	,[DischargeTime]
	,[TransferFrom]
	,encounter.[DistrictNo]
	,[IntendedPrimaryOperationCode]
	,[LocalAdminCategoryCode]
	,[EpisodicGpPracticeCode]
	--,PCTCode = 
	--	coalesce(
	--			pctsimplemerger.NewPCTCode
	--			,pctcomplexmerger.NewPCTCode
	--			,postcode.PCGOfResidence
	--			,'5NT'
	--			)
	,PCTCode = -- taken from the CDS derivaton for purchaser code - agreed with Tim 19/06/2012
			left(
			case
				when AdminCategoryCode = '02' then 'VPP00'
				when coalesce(									-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
							patientaddressatdischarge.Postcode
							,encounter.Postcode
							) like 'ZZ99%'
				or ContractSerialNo = 'OSV00A'
				then 'OSV'
				when ContractSerialNo <> 'OSV00A'
				and ContractSerialNo like 'OSV%'
				then 'YAC'
				else
					coalesce(
						--PCT.OrganisationCode -- joined from PurchaserCode -- removed at request of LR/LR
						Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,'5NT'
					)	
			end
			, 3)
	,[PatientCategoryCode]
	,[EndDirectorateCode]
	,FirstEpisodeInSpellIndicator
	,GpCodeAtDischarge = RegisteredGpPracticeAtDischarge.GpCode
	,[GpPracticeCodeAtDischarge] = RegisteredGpPracticeAtDischarge.GpPracticeCode
	,[PostCodeAtDischarge] = patientaddressatdischarge.Postcode
	--,[Exclude] = 
	--			case
	--				when 
	--					beddayminutes.BedDayMinutes >= datediff(minute, encounter.EpisodeStartTime, encounter.EpisodeEndTime)
	--				then 'Bedday'
						
	--				when encounter.ManagementIntentionCode = 'R'  -- IP 045 - REG to EL
	--				and	datediff(day, encounter.[AdmissionDate],encounter.DischargeDate) > 0 
	--				then ''
					
	--				when encounter.SpecialtyCode = 'BOWL' -- IP 044c - Flagging BOWEL Exclusions
	--				then 'Bowel'
					
	--				when encounter.[ContractSerialNo] in ('5NHBUR','5N6BUR') -- IP 044b - Flagging BURNS exclusions
	--				then 'Burns'
					
	--				when encounter.LocalAdminCategoryCode = 'PAY' -- IP 044 - Flagging Pay
	--				or coalesce(
	--							pctsimplemerger.NewPCTCode
	--							,pctcomplexmerger.NewPCTCode
	--							,postcode.PCGOfResidence
	--							) = 'PAY'
	--				or encounter.[ContractSerialNo] like 'YPP%'
	--				then 'Private'
					
	--				when (coalesce(NeonatalLevelOfCare, '0') in ('0', 'N') -- IP 043 - Flagging well babies
	--				and encounter.AdmissionMethodCode in ('BT', 'BH') 
	--				and encounter.AdmissionDate = encounter.EpisodeStartDate)
	--				or (coalesce(NeonatalLevelOfCare, '0') in ('0', 'N') 
	--				and encounter.AdmissionDate = encounter.EpisodeStartDate
	--				and encounter.PrimaryDiagnosisCode like 'Z38%')
	--				or (coalesce(NeonatalLevelOfCare, '0') in ('0', 'N')
	--				and specialty.NationalSpecialtyCode = '424')
	--				then 'Well Babies'
					
	--				when encounter.[ContractSerialNo] like '%VNS' -- IP 042 - Flagging Cost per Case
	--				or encounter.[ContractSerialNo] like '%BAR'
	--				or encounter.[ContractSerialNo] like '%ICD'
	--				or encounter.[ContractSerialNo] like '%CIM'
	--				or encounter.[ContractSerialNo] like '%MUD'
	--				or encounter.[ContractSerialNo] like '%CAM'
	--				or encounter.[ContractSerialNo] like '%BMT'
	--				or encounter.[ContractSerialNo] like '%CF1'
	--				or encounter.[ContractSerialNo] like '%GY1'
	--				then 'Cost Per Case'
					
	--				when excludedpatient.SourcePatientNo is not null -- IP 110 - Repflag Excluded Patients
	--				and coalesce(
	--							pctsimplemerger.NewPCTCode
	--							,pctcomplexmerger.NewPCTCode
	--							,postcode.PCGOfResidence
	--							,'5NT'
	--							) <> 'Wales'
	--				and coalesce(
	--							pctsimplemerger.NewPCTCode
	--							,pctcomplexmerger.NewPCTCode
	--							,postcode.PCGOfResidence
	--							,'5NT'
	--							) not like '6%'
	--				and coalesce(
	--							pctsimplemerger.NewPCTCode
	--							,pctcomplexmerger.NewPCTCode
	--							,postcode.PCGOfResidence
	--							,'5NT'
	--							) not like '7%'
	--				then excludedpatient.[ExclusionType]
					
	--				when -- IP 109 - Exclude Renal Dialysis FCEs
	--					(
	--					encounter.PrimaryOperationCode = 'X40.3'
	--					and encounter.ManagementIntentionCode not in
	--												(
	--												'D'
	--												,'R'
	--												)
	--					and specialty.NationalSpecialtyCode = '361'
	--					and datediff(day, encounter.AdmissionDate, encounter.DischargeDate) = 0
	--					)
	--				then 'Dialysis FCE'
													
	--				when encounter.ManagementIntentionCode in ('N', 'R') -- IP 039 - Flagging Misc - added 'N' requested by Sam - 10/05/2012)
	--				then 'Regular Day'
					
	--				when encounter.[NeonatalLevelOfCare] in
	--											(
	--											'1'
	--											,'2'
	--											,'3'
	--											,'H'
	--											,'M'
	--											,'S'
	--											)
	--				then 'Neonatal'
					
	--				when [ContractSerialNo] like '9%' -- IP 033 - Flagging PCT NSCAG
	--				then 'NSCAG'
					
	--				else null
	--			end
		,ExcludeFlag = 
				case
					when ExclusionCode is not null
					then 1
				end
		,ExcludeBedDay = 
				case
					when bedday.BeddayTotal >= datediff(day, encounter.EpisodeStartTime, encounter.EpisodeEndTime)
					then 1
				end
				--case
				--	when 
				--		beddayminutes.BedDayMinutes >= datediff(minute, encounter.EpisodeStartTime, encounter.EpisodeEndTime)
				--	then 1
				--end
		,ExcludePatient = 
				case
					when excludedpatient.SourcePatientNo is not null
					then 1
				end
		,LoEDays = datediff(day, encounter.EpisodeStartDate, encounter.EpisodeEndDate)
		,LoEMinutes = null -- datediff(minute, encounter.EpisodeStartTime, encounter.EpisodeEndTime)
		,BedDayDays =  bedday.BeddayTotal-- cast(beddayminutes.BedDayMinutes / 1440.0 as float)
		,BedDayMinutes = null -- beddayminutes.BedDayMinutes
		,AugmentedCareDays = null --cast(augmentedcareminutes.AugmentedCareMinutes / 1440.0 as float)
		,AugmentedCareMinutes = null --augmentedcareminutes.AugmentedCareMinutes	
		,LoEAdjusted =
					case
						when 
							cast(
								round(					
										datediff(day, encounter.EpisodeStartDate, encounter.EpisodeEndDate) 
										- 
										(
										coalesce(bedday.BeddayTotal, 0)
										--coalesce(beddayminutes.BedDayMinutes / 1440.0, 0)
										--+ 
										--coalesce(augmentedcareminutes.AugmentedCareMinutes / 1440.0, 0)
										)
									, 0)
								as int
								) < 0
						then 0
						else cast(
								round(					
										datediff(day, encounter.EpisodeStartDate, encounter.EpisodeEndDate) 
										- 
										(
										coalesce(bedday.BeddayTotal, 0)
										--coalesce(beddayminutes.BedDayMinutes / 1440.0, 0)
										--+ 
										--coalesce(augmentedcareminutes.AugmentedCareMinutes / 1440.0, 0)
										)
									, 0)
								as int
								)
					end
		,DateAdded = cast(getdate() as smalldatetime)

from 
	Warehouse.[APC].[Encounter] encounter
	
	left outer join Warehouse.PAS.PatientAddress patientaddressatdischarge
	on patientaddressatdischarge.SourcePatientNo = encounter.SourcePatientNo
	and encounter.DischargeDate between patientaddressatdischarge.EffectiveFromDate and coalesce(patientaddressatdischarge.EffectiveToDate, getdate())
		
	left outer join Warehouse.PAS.PatientGp RegisteredGpPracticeAtDischarge
	on RegisteredGpPracticeAtDischarge.SourcePatientNo = Encounter.SourcePatientNo
	and Encounter.DischargeDate between RegisteredGpPracticeAtDischarge.EffectiveFromDate and coalesce(RegisteredGpPracticeAtDischarge.EffectiveToDate, getdate())

	/* to cleanse the PurchaserCode values - we have old PCT codes in the column - requested by Tim 28/06/2012 */
	--left outer join Organisation.dbo.PCT PCT
	--on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode
	
	left outer join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = RegisteredGpPracticeAtDischarge.GpPracticeCode

	left outer join Warehouse.PAS.Specialty specialty
	on specialty.SpecialtyCode = encounter.SpecialtyCode
		
	left outer join Organisation.dbo.Postcode Postcode 
	on Postcode.Postcode = 
						case
							when len(patientaddressatdischarge.Postcode) = 8 then patientaddressatdischarge.Postcode
							else left(patientaddressatdischarge.Postcode, 3) + ' ' + right(patientaddressatdischarge.Postcode, 4) 
						end
	
	left outer join Income.ExcludedPatient excludedpatient
	on excludedpatient.SourcePatientNo = encounter.SourcePatientNo
	and encounter.PCTCode not like '6%'
	and encounter.PCTCode not like '7%'
	and encounter.DischargeDate between excludedpatient.EffectiveFromDate and coalesce(excludedpatient.EffectiveToDate, getdate())
	
	/* Exclusions  - needed to be applied before grouping */
	
	outer apply 
	
	(
		select 
			top 1 ExclusionCode
		from	
			Income.APCExclusionRuleBase exclusionrulebase
		where
			exclusionrulebase.ContextCode = 'CEN||PAS'
			and (encounter.SiteCode = exclusionrulebase.SiteCode or exclusionrulebase.SiteCode is null)
			and (encounter.PCTCode = exclusionrulebase.PCTCode or exclusionrulebase.PCTCode is null)
			and (exclusionrulebase.PODCode is null)
			and (exclusionrulebase.HRGCode is null)
			and (encounter.ManagementIntentionCode = exclusionrulebase.ManagementIntentionCode or exclusionrulebase.ManagementIntentionCode is null)
			and (encounter.AdmissionMethodCode = exclusionrulebase.PASAdmissionMethodCode or exclusionrulebase.PASAdmissionMethodCode is null)
			and (encounter.SpecialtyCode = exclusionrulebase.PASSpecialtyCode or exclusionrulebase.PASSpecialtyCode is null)
			and (specialty.NationalSpecialtyCode = exclusionrulebase.NationalSpecialtyCode or exclusionrulebase.NationalSpecialtyCode is null)
			and (coalesce(encounter.NeonatalLevelOfCare, '0') = exclusionrulebase.NeonatalLevelOfCareCode or exclusionrulebase.NeonatalLevelOfCareCode is null)
			and (encounter.PrimaryDiagnosisCode = exclusionrulebase.PrimaryDiagnosisCode or exclusionrulebase.PrimaryDiagnosisCode is null)
			and (encounter.PrimaryOperationCode = exclusionrulebase.PrimaryProcedureCode or exclusionrulebase.PrimaryProcedureCode is null)								
			and (encounter.ContractSerialNo = exclusionrulebase.CommissioningSerialNo or exclusionrulebase.CommissioningSerialNo is null)
			and (encounter.LocalAdminCategoryCode = exclusionrulebase.AdminCategoryCode or exclusionrulebase.AdminCategoryCode is null)			
			and (encounter.FirstEpisodeInSpellIndicator = exclusionrulebase.FirstEpisodeInSpellIndicator or exclusionrulebase.FirstEpisodeInSpellIndicator is null)
			and (datediff(day, encounter.EpisodeStartDate, encounter.EpisodeEndDate) = exclusionrulebase.LoS or exclusionrulebase.LoS is null)				
			and (encounter.DischargeDate between exclusionrulebase.EffectiveFromDate and coalesce(exclusionrulebase.EffectiveToDate, getdate()))
	
		) exclusionrulebase

		
	--/* Beddays */
	
	--outer apply
	--(
	--	select
	--		BedDayMinutes = count(*)
	--	from 
	--		Income.Calendar calendar

	--		inner join Warehouse.APC.WardStay wardstay
	--		on 	calendar.TheDateTime >= wardstay.StartTime
	--		and calendar.TheDateTime <
	--								case
	--									when EndActivityCode = 'ADS'
	--									then (
	--											select 
	--												top 1 StartTime
	--											from
	--												Warehouse.APC.WardStay wardstay1
	--											where
	--												wardstay1.ProviderSpellNo = wardstay.ProviderSpellNo
	--												and wardstay1.StartTime > wardstay.StartTime
	--											order by 
	--												StartTime asc	
	--										)
	--									else wardstay.EndTime
	--								end

	--	inner join Income.BedDayPODAndHRG beddayandhrg
	--	on	wardstay.WardCode = beddayandhrg.WardCode

	--	where 
	--		calendar.[TheDateTime] >= encounter.[EpisodeStartTime]
	--		and calendar.[TheDateTime] < encounter.[EpisodeEndTime]
	--		and encounter.ProviderSpellNo = wardstay.ProviderSpellNo

	--) beddayminutes

	/* includes bed days and augmented care period data */

	left outer join
					(
					select
						EncounterRecno
						--[ProviderSpellNo]
						--,[SourceEncounterNo]
						,BeddayTotal = sum(Bedday)
					from
						[ETL].[TImportIncomeBedday]
					group by
						EncounterRecno
						--,[ProviderSpellNo]
						--,[SourceEncounterNo]
					) bedday
					on bedday.EncounterRecno = encounter.EncounterRecno


		--/* Augmented Care */
			
	--outer apply
	--		(
	--		select
	--			AugmentedCareMinutes = count(*)
	--		from 
	--			Income.Calendar calendar
	--		inner join
	--			Warehouse.APC.AugmentedCarePeriod augmentedcareperiod
	--			on	calendar.TheDateTime >= augmentedcareperiod.StartTime
	--			and calendar.TheDateTime < augmentedcareperiod.EndTime
				
	--			inner join Warehouse.PAS.AugmentedCareLocation augmentedcarelocation
	--			on augmentedcareperiod.LocationCode = augmentedcarelocation.LocationCode
								
	--			left outer join Income.B[sysutility_mdw]edDayPODAndHRG beddayandhrg
	--			on augmentedcarelocation.WardCode = beddayandhrg.WardCode
				
	--		where
	--			calendar.TheDateTime >= encounter.[EpisodeStartTime]
	--			and calendar.TheDateTime < encounter.[EpisodeEndTime]
	--			and encounter.ProviderSpellNo = augmentedcareperiod.ProviderSpellNo
	--			and (
	--				(
	--				augmentedcarelocation.SiteCode = 'BHCH' 
	--				and augmentedcarelocation.LocationCode <> '81'
	--				)
	--				or augmentedcarelocation.SiteCode = 'RMCH'
	--				or augmentedcarelocation.SiteCode = 'NRMC'
	--				)
	--			and beddayandhrg.WardCode is null

	--		) augmentedcareminutes


where
	encounter.DischargeDate between @start and @end
	--and encounter.SiteCode <> 'YORK'
	--and encounter.ProviderSpellNo = '1864516/23' -- loe = 0
	--and encounter.ProviderSpellNo = '2044483/11' -- good example of ward stay over multiple episodes
	--and encounter.ProviderSpellNo = '2795271/9'
	--and encounter.ProviderSpellNo = '1013224/25'
	--and encounter.ProviderSpellNo = '1093315/22' -- example of when ward carries over into two episodes - need to sort EpisodeStartTime
	--and encounter.ProviderSpellNo = '3262881/2'

	--and encounter.ProviderSpellNo = '2427959/72'
	--and encounter.ProviderSpellNo = '3143169/1'
	--and encounter.ProviderSpellNo = '2783955/3'
	--and encounter.ProviderSpellNo = '3246026/1' -- well babies
	--and encounter.ProviderSpellNo = '2349453/16' -- lsd
	
	--and encounter.ProviderSpellNo = '3096813/4' -- dups caused by including pod

	--and encounter.ProviderSpellNo = '1453430/9' -- where sum of ward stay entries does not equal overall LOS - patient sent home
	
select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid;

--exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;

--truncate table Income.Calendar


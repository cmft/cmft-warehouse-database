﻿
create proc ETL.BuildLoSSPC

	
as

declare	@start datetime = null
		,@end datetime = null
		,@level varchar(20) = null
		,@parent varchar(20) = null
		,@losgroup varchar(20) = null

set @end = coalesce(@end,(select max([SnapshotDate])
							 from ETL.LoSPatientLevelData))
set @start = coalesce(@start,dateadd(wk, -14, @end))

set @level = coalesce(@level, 'Division')

set @parent = coalesce (@parent, '(Select All)')

set @losgroup = coalesce (@losgroup, '(Select All)')

/* build and populate table to store consolidated data (from different sources) */

if object_id('tempdb..#data') is not null
drop table #data

create table #data

(
ID int identity (0,1)
,[Level] varchar(50)
,Unit varchar(50)
,Parent varchar(50)
,LosGroup varchar(50)
,[UnitID] int
,[Week] varchar(50)
,SnapshotDate datetime
,FinancialYear varchar(50)
,Cases int
)

insert into #data

(
[Level]
,Unit 
,Parent 
,LosGroup
,[Week]
,SnapshotDate 
,FinancialYear 
,Cases 
)

select	
		[Level] = 'Division'
		,Unit = Division
		,Parent = 'CMFT'
		,LosGroup
		,[Week]
		,SnapshotDate
		,FinancialYear
		,sum(Cases)
from ETL.LoSSummarisedReportData
where
	SnapshotDate < '07/07/2011'
group by 
		Division
		,LosGroup
		,[Week]
		,SnapshotDate
		,FinancialYear

/* ward data */

insert into #data

(
[Level]
,Unit 
,Parent
,LosGroup
,[Week]
,SnapshotDate 
,FinancialYear 
,Cases 
)

select
		[Level] = 'Ward'
		,Unit = coalesce(WardCode, 'Not Recorded')
		,Parent = coalesce(Division,'N/A')
		,LosGroup
		,[Week] = coalesce((select top 1 [Week]
							from ETL.LoSSummarisedReportData s
							where p.SnapshotDate = s.SnapshotDate), 'Wk' + ' ' + cast(datepart(week, dateadd(month, -3, p.SnapshotDate)) - 1 as char(2)))
		,SnapshotDate
		,[FinancialYear] = (select top 1 [FinancialYear]
							from WH.Calendar c
							where p.SnapshotDate = c.TheDate)
		,count(*)
from 
	ETL.LoSPatientLevelData p

group by 
		WardCode
		,Division
		,LosGroup
		,SnapshotDate
		
/* division roll up from ward */
		
insert into #data

(
[Level]
,Unit 
,Parent
,LosGroup
,[Week]
,SnapshotDate 
,FinancialYear 
,Cases 
)

select	
		[Level] = 'Division'
		,Unit = Parent
		,Parent = 'CMFT'
		,LosGroup
		,[Week]
		,SnapshotDate
		,FinancialYear
		,sum(Cases)
from 
	#data
where 
	SnapshotDate >= '07/07/2011' 
group by 
		Parent
		,LosGroup
		,[Week]
		,SnapshotDate
		,FinancialYear
		
		
/* trust roll up from division */	
		
insert into #data

(
[Level]
,Unit 
,Parent 
,LosGroup
,[Week]
,SnapshotDate 
,FinancialYear 
,Cases
)

select	
		[Level] = 'CMFT'
		,Parent = 'CMFT'
		,Parent = 'CMFT'
		,LosGroup
		,[Week]
		,[SnapshotDate]
		,[FinancialYear]
		,sum(Cases)
from 
	#data
where
	[Level] = 'Division'
group by 
		LosGroup
		,[Week]
		,LosGroup
		,[SnapshotDate]
		,[FinancialYear]
		
--select * from #data
		
/* Build and populate unit table for loop */

if object_id('tempdb..#unit') is not null
drop table #unit

create table #unit

(
ID int identity (0,1) primary key
,[Level] varchar(50)
,Unit varchar(50)
,Parent varchar(50)
)

insert into #unit

select distinct
		[Level]
		,Unit 
		,Parent 
from #data

order by
		[Level]
		,Parent
		,Unit 

		
--select *
--from #unit	
--where Parent = 'Surgery'

update d
set d.UnitID = u.ID
from #data d
inner join #unit u
on d.[Level] = u.[Level]
and d.Unit = u.Unit
and d.Parent = u.Parent


/* cartesian for all units and all weeks - needed to handle groups that have no data for a given timescale */

if object_id('tempdb..#dateunit') is not null
drop table #dateunit

create table #dateunit

(
[UnitID] int not null
,Unit varchar(50) not null
,[Week] varchar(50) not null
,SnapshotDate datetime not null
,FinancialYear varchar(50) not null
)

insert into #dateunit

select distinct 
		d.UnitID
		,d.Unit
		,sd.[Week]
		,sd.SnapshotDate 
		,sd.FinancialYear 
from #data d

cross join
(select 
	distinct 
		[Week]
		,SnapshotDate 
		,FinancialYear 
from 
	#data) sd

where
	d.Level <> 'Ward'
	

/* 

Ward specific values for cartesian as we only have ward level data from 07/07/2011.
Adding ward null values back for every week for the last 2-3 years creates unecessary 
overhead

*/
	
insert into #dateunit

select 
	distinct 
		d.UnitID
		,d.Unit
		,sd.[Week]
		,sd.SnapshotDate 
		,sd.FinancialYear 
from 
	#data d

cross join
(select distinct 
		[Week]
		,SnapshotDate 
		,FinancialYear 
from 
	#data) sd

where
	d.Level = 'Ward'
	and sd.SnapshotDate >= '07/07/2011'

--select * from #dateunit
		
declare @unitcounter int = 0
declare @unitid int = null

if object_id('tempdb..#losspc') is not null
drop table #losspc

create table #losspc
(
	[ID] int --identity (0,1)
	,[UnitID] int
	,[Unit] varchar(50)
	,[Week] varchar(20)
	,[SnapshotDate] datetime
	,[FinancialYear] varchar(20)
	,StepChangeStartID int
	,[Cases] float
	,[Current] float
	,[Current + 1] float
	,[Current + 2] float
	,[Current + 3] float
	,[Current + 4] float
	,[Current + 5] float
	,[Current + 6] float
	,[Counter] float
	,[MeanStart] float
	,[MeanEnd] float
	,[Mean] float
	,[MovingRange] float
)

while @unitcounter <= (select max(ID) from #unit)

begin

set @unitid = (select ID from #unit where ID = @unitcounter)

print 'unitcounter' + ' ' + cast(@unitcounter as char)
print 'unit' + ' ' + cast(@unitid as char)

if object_id('tempdb..#los') is not null
drop table #los

create table #los
(
	[ID] int identity (0,1)
	,[UnitID] varchar(50)
	,[Unit] varchar(50)
	,[Week] varchar(20)
	,[SnapshotDate] datetime
	,[FinancialYear] varchar(20)
	,[StepChangeStartID] int default 0
	,[Cases] float
)


insert into #los
(
	[UnitID]
	,Unit
	,[Week]
	,[SnapshotDate]
	,[FinancialYear]
	,[Cases]
)
	
select 
	du.[UnitID]
	,du.Unit
	,du.[Week]
	,du.[SnapshotDate]
	,du.[FinancialYear]
	,isnull(sum(d.[Cases]),0)
from 
	#dateunit du

left outer join #data d
on d.UnitID = du.UnitID
and d.SnapshotDate = du.SnapshotDate

where 
	du.UnitID = @unitid
	
group by du.[UnitID]
		,du.Unit
		,du.[Week]
		,du.[SnapshotDate]
		,du.[FinancialYear]
		
order by 
	du.UnitID
	,du.SnapshotDate
	
	
--select * from #los
	
/* update spc */

declare @meanstart int = 0
		,@meanend int = 0
		,@mean float 
		,@counter int = 0
		,@currentrow int = 0
		,@stepchange int = 0
		,@currentminus1 int = 0
		,@current float
		,@current1 float
		,@current2 float
		,@current3 float
		,@current4 float
		,@current5 float
		,@current6 float
			
while @counter <= 
				(
					select
						count(*) 
					from 
						#dateunit
					where 
						UnitID = @unitid
				)

begin

print 'new loop'

set @current =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow
			)
print 'Current row' + ' ' + cast(@currentrow as char)			
print 'Current cases' + ' ' + cast(@current as char)

-- scan current and next 6 values for deviation from mean

set @currentminus1 =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow - 1
			)
		
set @current1 =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow + 1
			)
			
set @current2 =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow + 2
			)
			
set @current3 =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow + 3
			)

set @current4 =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow + 4
			)
			
set @current5 =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow + 5
			)
			
set @current6 =	
			(
			select Cases
			from #los l2
			where l2.ID = @currentrow + 6
			)
			
set @stepchange	= 
	case
		when 
			((
			@current > @mean
			and @current1 > @mean
			and @current2 > @mean
			and @current3 > @mean
			and @current4 > @mean
			and @current5 > @mean
			and @current6 > @mean
			)
			or 
			(
			@current < @mean
			and @current1 < @mean
			and @current2 < @mean
			and @current3 < @mean
			and @current4 < @mean
			and @current5 < @mean
			and @current6 < @mean
			))
		and not exists
		(
			select 1
			from #los l2
			where l2.StepChangeStartID = l2.ID
			and l2.ID between @currentrow - 13 and @currentrow
		)
		-- allow only after 14 week lag after previous step change
		then @currentrow
		else @stepchange
	end	

-- update los table to hold step change values

update #los
set StepChangeStartID = @stepchange
where ID >= @currentrow
	
set @meanstart =	
			(
			select max(ID)
			from #los l2
			where l2.ID <= @counter
			and StepChangeStartID = l2.ID
			)
			
print 'mean start' + ' ' + cast(@meanstart as char)

set @meanend =	
			(
			select max(ID)
			from #los l2
			where l2.ID <= @counter
			)	
			
print 'mean end' + ' ' + cast(@meanend as char)
							
set @mean = 
			(
			select avg(Cases)
			from #los l2
			where l2.ID between @meanstart
			and @meanend
			)
	
print 'Mean' + ' ' + cast(@mean as char) 

print 'insert row'		
insert into #losspc

select 
	[ID]
	,[UnitID]
	,[Unit]
	,[Week]
	,[SnapshotDate] 
	,[FinancialYear]
	,[StepChangeStartID]
	,[Cases] 
	,[Current] = @current
	,[Current + 1] = @current1
	,[Current + 2] = @current2
	,[Current + 3] = @current3
	,[Current + 4] = @current4
	,[Current + 5] = @current5
	,[Current + 6] = @current6
	,[Counter] = @counter
	,[MeanStart] = @meanstart
	,[MeanEnd] = @meanend
	,[Mean] = @mean
	,MovingRange = coalesce(abs(@current - @currentminus1), 0)

from 
	#los l1
where
	ID = @counter

set @counter = @counter + 1
set @currentrow = @counter	

end

set @unitcounter = @unitcounter + 1

end

/* result set */

truncate table ETL.LoSSPC	

insert into ETL.LoSSPC	

select 
		ReportGroup = 'SPC'
		,UnitID 
		,RowGroup = (dense_rank() over (partition by u.Parent order by l1.UnitID) 
					- case
							when dense_rank() over (partition by u.Parent order by l1.UnitID) % 2 = 0
							then 2
							else 1
						end) /2
		,ColumnGroup = case
							when dense_rank() over (partition by u.Parent order by l1.UnitID) % 2 = 0
							then 2
							else 1
						end

		,u.[Level]
		,u.Parent
		,l1.[Unit]
		,[LoSGroup] = 'N/A'
		,[Week]
		,[SnapshotDate]
		,[FinancialYear]
		,[Cases]
		,[Mean] = (
					select Mean
					from #losspc l2
					where l2.ID = 
								(
								select max(ID) from #losspc l3
								where l2.MeanStart = l3.MeanStart
								and l2.UnitID = l3.UnitID
								)
					and l2.MeanStart = l1.MeanStart
					and l2.[UnitID] = l1.[UnitID]
					)		
		,[AverageMovingRange] = (
								select avg(MovingRange)
								from #losspc l2
								where l2.ID between l1.MeanStart and (
																		select max(ID) from #losspc l3
																		where l2.MeanStart = l3.MeanStart
																		and l2.[UnitID] = l3.[UnitID]
																	)
								and l2.[UnitID] = l1.[UnitID]
								)
		,UCL = (
					select Mean
					from #losspc l2
					where l2.ID = 
								(
								select max(ID) from #losspc l3
								where l2.MeanStart = l3.MeanStart
								and l2.[UnitID] = l3.[UnitID]
								)
					and l2.MeanStart = l1.MeanStart
					and l2.[UnitID] = l1.[UnitID]
					) 
					+
					(
					3*
					(
					select avg(MovingRange)
					from #losspc l2
					where l2.ID between l1.MeanStart and (
															select max(ID) from #losspc l3
															where l2.MeanStart = l3.MeanStart
															and l2.[UnitID] = l3.[UnitID]
														)
					and l2.[UnitID] = l1.[UnitID]
					)/1.128
					)
		,LCL = (
					select Mean
					from #losspc l2
					where l2.ID = 
								(
								select max(ID) from #losspc l3
								where l2.MeanStart = l3.MeanStart
								and l2.[UnitID] = l3.[UnitID]
								)
					and l2.MeanStart = l1.MeanStart
					and l2.[UnitID] = l1.[UnitID]
					) 
					-
					(
					3*
					(
					select avg(MovingRange)
					from #losspc l2
					where l2.ID between l1.MeanStart and (
															select max(ID) from #losspc l3
															where l2.MeanStart = l3.MeanStart
															and l2.[UnitID] = l3.[UnitID]
														)
					and l2.[UnitID] = l1.[UnitID]
					)/1.128
					)
		
from 
	#losspc l1
inner join #unit u
on l1.UnitID = u.ID
 
	
union all

select 

	ReportGroup = 'LoSGroup'
	,UnitID
	--,row_number() over (partition by u.Parent order by UnitID)
	,0
	,0
	,u.[Level]
	,u.Parent
	,d.[Unit]
	,[LoSGroup]
	,[Week]
	,[SnapshotDate]
	,[FinancialYear]
	,[Cases]
	,0
	,0
	,0
	,0
	
from 
	#data d
inner join #unit u
on d.UnitID = u.ID

--select * from #losspc


﻿
create proc ETL.LoadIncomeAPCHRGUnbundled

as


insert into [Income].[APCHRGUnbundled]

(
SnapshotRecno
,[Period]
,[Position]
,[Version]
,[SUSIdentifier]
,[SequenceNo]
,[HRGCode]
)

select
	SnapshotRecno
    , [Period]
      ,[Position]
	  ,[Version]
      ,[SUSIdentifier]
      ,[SequenceNo]
      ,[HRGCode]
from
	ETL.TLoadIncomeAPCHRGUnbundled




﻿


CREATE PROCEDURE [ETL].[BuildDQCensus]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseDQCensus

	INSERT INTO  ETL.BaseDQCensus

	SELECT 
		   [LoadStartDate]
		  ,[LoadEndDate]
		  ,[Date]
		  ,[Type]
		  ,[PointOfDelivery]
		  ,[InterfaceCode]
		  ,[DirectorateCode]
		  ,[Denominator]
		  ,[Numerator]

	FROM 
		 Warehouse.dq.Census

END




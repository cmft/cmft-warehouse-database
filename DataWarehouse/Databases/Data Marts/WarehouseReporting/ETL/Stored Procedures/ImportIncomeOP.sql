﻿
create proc ETL.ImportIncomeOP

(
	@start datetime
	,@end datetime
	,@period varchar(20) -- format example 2012/2013 Month 1
	,@position varchar(50) -- flex/freeze
	,@version int
)

with recompile

as

--if object_id('ETL.TImportIncomeOP') is not null 
--drop table ETL.TImportIncomeOP;


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255)
select @StartTime = getdate();

truncate table ETL.TImportIncomeOP

insert into ETL.TImportIncomeOP

   (
   [Period]
   ,[Position]
   ,[Version]
   ,[EncounterRecno]
   ,[SourceUniqueID]
   ,[SourcePatientNo]
   ,[SourceEncounterNo]
   ,[PatientForename]
   ,[PatientSurname]
   ,[DateOfBirth]
   ,[DateOfDeath]
   ,[SexCode]
   ,[NHSNumber]
   ,[DistrictNo]
   ,[Postcode]
   ,[EthnicOriginCode]
	,[RegisteredGpCode]
   ,[RegisteredGpPracticeCode]
   ,[SiteCode]
   ,[AppointmentDate]
   ,[AppointmentTime]
   ,[ClinicCode]
   ,[AdminCategoryCode]
   ,[SourceOfReferralCode]
   ,[ReasonForReferralCode]
   ,[PriorityCode]
   ,[FirstAttendanceFlag]
   ,[DNACode]
   ,[AppointmentStatusCode]
   ,[CancelledByCode]
   ,[TransportRequiredFlag]
   ,[AttendanceOutcomeCode]
   ,[AppointmentTypeCode]
   ,[DisposalCode]
   ,[ConsultantCode]
   ,[SpecialtyCode]
   ,[ReferringConsultantCode]
   ,[ReferringSpecialtyCode]
   ,[BookingTypeCode]
   ,[CasenoteNo]
   ,[AppointmentCreateDate]
   ,[EpisodicGpPracticeCode]
   ,[DoctorCode]
   ,[PrimaryDiagnosisCode]
   ,[SubsidiaryDiagnosisCode]
   ,[SecondaryDiagnosisCode1]
   ,[SecondaryDiagnosisCode2]
   ,[SecondaryDiagnosisCode3]
   ,[SecondaryDiagnosisCode4]
   ,[SecondaryDiagnosisCode5]
   ,[SecondaryDiagnosisCode6]
   ,[SecondaryDiagnosisCode7]
   ,[SecondaryDiagnosisCode8]
   ,[SecondaryDiagnosisCode9]
   ,[SecondaryDiagnosisCode10]
   ,[SecondaryDiagnosisCode11]
   ,[SecondaryDiagnosisCode12]
   ,[PrimaryOperationCode]
   ,[PrimaryOperationDate]
   ,[SecondaryOperationCode1]
   ,[SecondaryOperationDate1]
   ,[SecondaryOperationCode2]
   ,[SecondaryOperationDate2]
   ,[SecondaryOperationCode3]
   ,[SecondaryOperationDate3]
   ,[SecondaryOperationCode4]
   ,[SecondaryOperationDate4]
   ,[SecondaryOperationCode5]
   ,[SecondaryOperationDate5]
   ,[SecondaryOperationCode6]
   ,[SecondaryOperationDate6]
   ,[SecondaryOperationCode7]
   ,[SecondaryOperationDate7]
   ,[SecondaryOperationCode8]
   ,[SecondaryOperationDate8]
   ,[SecondaryOperationCode9]
   ,[SecondaryOperationDate9]
   ,[SecondaryOperationCode10]
   ,[SecondaryOperationDate10]
   ,[SecondaryOperationCode11]
   ,[SecondaryOperationDate11]
   ,[PurchaserCode]
   ,[ProviderCode]
   ,[ContractSerialNo]
   ,[ReferralDate]
   ,[AppointmentCategoryCode]
   ,[AppointmentCancelDate]
   ,[LastRevisedDate]
   ,[LastRevisedBy]
   ,[OverseasStatusFlag]
   ,[PatientChoiceCode]
   ,[ScheduledCancelReasonCode]
   ,[PatientCancelReason]
   ,[DischargeDate]
   ,[DestinationSiteCode]
   ,[InterfaceCode]
   ,[LocalAdminCategoryCode]
   ,[PCTCode]
   ,[LocalityCode]
   ,[IsWardAttender]
   ,[SourceOfReferralGroupCode]
   ,[WardCode]
   ,[DirectorateCode]
   ,[ReferringSpecialtyTypeCode]
   ,[ReferredByCode]
   ,[ReferrerCode]
   ,[LastDNAorPatientCancelledDate]
   ,[StaffGroupCode]
	,DerivedFirstAttendanceFlag
		,GpCodeAtAppointment
	,GpPracticeCodeAtAppointment
	,PostcodeAtAppointment
   --,Exclude
   --,ExclusionCode
   ,ExcludeFlag
   ,ExcludePatientFlag
   ,ReferralSpecialtyCode
   )

select
	Period = @period
	,Position = @position
	,[version] = @version
	,encounter.[EncounterRecno]
	,encounter.[SourceUniqueID]
	,encounter.[SourcePatientNo]
	,encounter.[SourceEncounterNo]
	--,[PatientTitle]
	,encounter.[PatientForename]
	,encounter.[PatientSurname]
	,encounter.[DateOfBirth]
	,encounter.[DateOfDeath]
	,encounter.[SexCode]
	,encounter.[NHSNumber]
	,encounter.[DistrictNo]
	,[Postcode] = Postcode.Postcode
	--,[PatientAddress1]
	--,[PatientAddress2]
	--,[PatientAddress3]
	--,[PatientAddress4]
	--,[DHACode]
	,encounter.[EthnicOriginCode]
	--,[MaritalStatusCode]
	--,[ReligionCode]
	,encounter.[RegisteredGpCode]
	,encounter.[RegisteredGpPracticeCode]
	,encounter.[SiteCode]
	,[AppointmentDate]
	,[AppointmentTime]
	,[ClinicCode]
	,encounter.[AdminCategoryCode]
	,encounter.[SourceOfReferralCode]
	,encounter.[ReasonForReferralCode]
	,encounter.[PriorityCode]
	,[FirstAttendanceFlag]
	,[DNACode]
	,[AppointmentStatusCode]
	,[CancelledByCode]
	,[TransportRequiredFlag]
	,[AttendanceOutcomeCode]
	,[AppointmentTypeCode]
	,[DisposalCode]
	,encounter.[ConsultantCode]
	,encounter.[SpecialtyCode]
	,[ReferringConsultantCode]
	,[ReferringSpecialtyCode]
	,[BookingTypeCode]
	,encounter.[CasenoteNo]
	,[AppointmentCreateDate]
	--,[EpisodicGpCode]
	,encounter.[EpisodicGpPracticeCode]
	,[DoctorCode]
	,[PrimaryDiagnosisCode]
	,[SubsidiaryDiagnosisCode]
	,[SecondaryDiagnosisCode1]
	,[SecondaryDiagnosisCode2]
	,[SecondaryDiagnosisCode3]
	,[SecondaryDiagnosisCode4]
	,[SecondaryDiagnosisCode5]
	,[SecondaryDiagnosisCode6]
	,[SecondaryDiagnosisCode7]
	,[SecondaryDiagnosisCode8]
	,[SecondaryDiagnosisCode9]
	,[SecondaryDiagnosisCode10]
	,[SecondaryDiagnosisCode11]
	,[SecondaryDiagnosisCode12]
	,[PrimaryOperationCode]
	,[PrimaryOperationDate]
	,[SecondaryOperationCode1]
	,[SecondaryOperationDate1]
	,[SecondaryOperationCode2]
	,[SecondaryOperationDate2]
	,[SecondaryOperationCode3]
	,[SecondaryOperationDate3]
	,[SecondaryOperationCode4]
	,[SecondaryOperationDate4]
	,[SecondaryOperationCode5]
	,[SecondaryOperationDate5]
	,[SecondaryOperationCode6]
	,[SecondaryOperationDate6]
	,[SecondaryOperationCode7]
	,[SecondaryOperationDate7]
	,[SecondaryOperationCode8]
	,[SecondaryOperationDate8]
	,[SecondaryOperationCode9]
	,[SecondaryOperationDate9]
	,[SecondaryOperationCode10]
	,[SecondaryOperationDate10]
	,[SecondaryOperationCode11]
	,[SecondaryOperationDate11]
	,[PurchaserCode]
	,[ProviderCode]
	,encounter.[ContractSerialNo]
	,encounter.[ReferralDate]
	--,[RTTPathwayID]
	--,[RTTPathwayCondition]
	--,[RTTStartDate]
	--,[RTTEndDate]
	--,[RTTSpecialtyCode]
	--,[RTTCurrentProviderCode]
	--,[RTTCurrentStatusCode]
	--,[RTTCurrentStatusDate]
	--,[RTTCurrentPrivatePatientFlag]
	--,[RTTOverseasStatusFlag]
	--,[RTTPeriodStatusCode]
	,[AppointmentCategoryCode]
	--,[AppointmentCreatedBy]
	,[AppointmentCancelDate]
	,[LastRevisedDate]
	,[LastRevisedBy]
	,[OverseasStatusFlag]
	,[PatientChoiceCode]
	,[ScheduledCancelReasonCode]
	,[PatientCancelReason]
	,encounter.[DischargeDate]
	--,[QM08StartWaitDate]
	--,[QM08EndWaitDate]
	,[DestinationSiteCode]
	--,[EBookingReferenceNo]
	,encounter.[InterfaceCode]
	--,[Created]
	--,[Updated]
	--,[ByWhom]
	,[LocalAdminCategoryCode]
	--,PCTCode = 
	--	coalesce(
	--			pctsimplemerger.NewPCTCode
	--			,pctcomplexmerger.NewPCTCode
	--			,postcode.PCGOfResidence
	--			,'NOPCT'
	--			)
	,PCTCode = -- taken from the CDS derivaton for purchaser code - agreed with Tim 19/06/2012
		left(
		case
			when encounter.AdminCategoryCode = '02' then 'VPP00'
			when coalesce(									-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
						patientaddressatappointment.Postcode
						,encounter.Postcode
						) like 'ZZ99%'
			or encounter.ContractSerialNo = 'OSV00A'
			then 'OSV'
			when encounter.ContractSerialNo <> 'OSV00A'
			and encounter.ContractSerialNo like 'OSV%'
			then 'YAC'
			else
				coalesce(
						--PCT.OrganisationCode -- joined from PurchaserCode
						Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,'5NT'
				)	
		end
		, 3)
	--,ContractSerialNo
	,[LocalityCode]
	,[IsWardAttender]
	--,[ClockStartDate]
	--,[RTTBreachDate]
	--,[HomePhone]
	--,[WorkPhone]
	,encounter.[SourceOfReferralGroupCode]
	,[WardCode]
	,encounter.[DirectorateCode]
	,[ReferringSpecialtyTypeCode]
	,[ReferredByCode]
	,[ReferrerCode]
	,[LastDNAorPatientCancelledDate]
	,[StaffGroupCode]
	,DerivedFirstAttendanceFlag
	,[GpCodeAtAppointment] = patientgpatappointment.GpCode
	,[GpPracticeCodeAtAppointment] = patientgpatappointment.GpPracticeCode
	,[PostCodeAtAppointment] = patientaddressatappointment.Postcode


	--,Exclude = 
	--			case
	--/* EXCLUDE */
	--				when 
	--				encounter.ContractSerialNo like '9%' 
	--				or encounter.ContractSerialNo = 'OSV00A' 
	--				or encounter.ContractSerialNo like 'YPP%' 
	--				or encounter.ContractSerialNo like '%CF1'
	--				or encounter. ContractSerialNo like '%GY1' 
	--				or encounter.LocalAdminCategoryCode = 'PAY'					
	--				then 'EXCLUDE'				

	--/* EXCLUDED PATIENTS */
	--				when
	--					exists (
	--							select
	--								1
	--							from
	--								Income.ExcludedPatient excludedpatient
	--							where
	--								excludedpatient.SourcePatientNo = encounter.SourcePatientNo
	--							)
	--				and encounter.PCTCode <> 'WALES'
	--				and encounter.PCTCode not like '6%'
	--				then 'EXCLUDE - EXCLUDED PATIENTS'
	
	--/* TRAFFORD MIDWIFERY CLINICS */
	--				when
	--					exists (
	--							select
	--								1
	--							from
	--								Income.TraffordMidwiferyClinic traffordmidwifery
	--							where
	--								traffordmidwifery.ClinicCode = encounter.ClinicCode
	--							)
	--				and encounter.PCTCode = '5NR'
	--				then 'EXCLUDE - TRAFFORD MIDWIFERY'
					
	--/* HOPE & RENAL CLINICS */				
	--				when encounter.ClinicCode in 
	--											(
	--											'JTL-HOPE'
	--											,'NNG-HOPE'
	--											,'SMMHD'
	--											)
	--				then 'EXCLUDE - HOPE & RENAL'
		
	--/* DIAGNOSTICS */					
	--				when divisionmap.Diagnostic = 'I'
	--				and specialty.NationalSpecialtyCode not in 
	--														(
	--														'320'
	--														,'654'
	--														)
	--				then 'EXCLUDE - DIAGNOSTICS I'
					
	--				when divisionmap.Diagnostic = 'X'
	--				and specialty.NationalSpecialtyCode not in 
	--														(
	--														'656' -- not in ITS but requested by Sam 21/05/2012
	--														)
	--				then 'EXCLUDE - DIAGNOSTICS X'
					
	--/* BOWL */					
	--				when encounter.SpecialtyCode = 'BOWL'
	--				then 'EXCLUDE - BOWL' 				
	--/* GYN1 */					
	--				when encounter.SpecialtyCode = 'GYN1'
	--				then 'EXCLUDE - GYN1'	
	--			else null
	--		end	
		--,exclusionrulebase.ExclusionCode
		,ExcludeFlag = 
				case
					when exclusionrulebase.ExclusionCode is not null
					then 1
				end
		,ExcludePatientFlag = 
				case
					when excludedpatient.SourcePatientNo is not null
					then 1
				end
		--,ReferralSpecialtyCode = referral.SpecialtyCode
		,ReferralSpecialtyCode = encounter.ReferringSpecialtyCode
--into ETL.TImportIncomeOP
from
	Warehouse.[OP].[Encounter] encounter

	--inner join  warehouse.[RF].[Encounter] referral
	--on encounter.SourcePatientNo = referral.SourcePatientNo
	--and encounter.SourceEncounterNo = referral.SourceEncounterNo
	
	--inner join  Warehouse.WH.DivisionMap divisionmap
	--on	divisionmap.SiteCode = encounter.SiteCode
	--and	divisionmap.SpecialtyCode = referral.SpecialtyCode -- for diagnostic flag

	inner join  Warehouse.WH.DivisionMap divisionmap
	on	divisionmap.SiteCode = encounter.EpisodicSiteCode
	and	divisionmap.SpecialtyCode = encounter.ReferringSpecialtyCode
	and encounter.AppointmentDate between divisionmap.FromDate and coalesce(divisionmap.ToDate, getdate()) -- for diagnostic flag

	and 
	(
		DivisionMap.ConsultantCode = Encounter.ConsultantCode
	or
		DivisionMap.ConsultantCode is null
	)
	
	--inner join Warehouse.PAS.Specialty specialty
	--on specialty.SpecialtyCode = referral.SpecialtyCode

	inner join Warehouse.PAS.Specialty specialty
	on specialty.SpecialtyCode = encounter.ReferringSpecialtyCode
	
	left outer join Warehouse.PAS.PatientGp patientgpatappointment
	on patientgpatappointment.SourcePatientNo = encounter.SourcePatientNo
	and encounter.AppointmentDate between patientgpatappointment.EffectiveFromDate and coalesce(patientgpatappointment.EffectiveToDate, getdate())

	
	/* to bring cleanse the PurchaserCode values - we have old PCT codes in the column - requested by Tim 28/06/2012 */
	--left outer join Organisation.dbo.PCT PCT
	--on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode

	left outer join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = patientgpatappointment.GpPracticeCode
	
	left outer join Warehouse.PAS.PatientAddress patientaddressatappointment
	on patientaddressatappointment.SourcePatientNo = encounter.SourcePatientNo
	and encounter.AppointmentDate between patientaddressatappointment.EffectiveFromDate and coalesce(patientaddressatappointment.EffectiveToDate, getdate())
		
	left outer join Organisation.dbo.Postcode Postcode 
	on Postcode.Postcode = 
						case
							when len(patientaddressatappointment.Postcode) = 8 then patientaddressatappointment.Postcode
							else left(patientaddressatappointment.Postcode, 3) + ' ' + right(patientaddressatappointment.Postcode, 4) 
						end
	
	
	--left outer join Income.Postcode postcode
	--on patientaddress.Postcode = postcode.Postcode
	
	--left outer join Income.PCTSimpleMerger pctsimplemerger -- needed as DQ issues with ContractSerialNo not being aligned with PCT
	--on coalesce
	--	(
	--	case
	--		when left(encounter.ContractSerialNo,3) = '9%'
	--		then 'NSCAG'
	--		when encounter.ContractSerialNo = 'OSV00A' 
	--		then 'OSV'
	--		when encounter.ContractSerialNo like 'YPP%'  
	--		then 'PAY'
	--		when encounter.ContractSerialNo like 'OSV%' 
	--		and encounter.ContractSerialNo <> 'OSV00A'
	--		then 'YAC'
	--		else left(encounter.ContractSerialNo,3)
	--	end
	--	,
	--	postcode.PCGOfResidence -- mirrors using PCT of Residence in ITS?
	--	,'5NT'
	--	)			
	--	= pctsimplemerger.OldPCTCode
		
	--left outer join Income.PCTComplexMerger pctcomplexmerger -- needed as DQ issues with ContractSerialNo not being aligned with PCT
	--on patientgp.GpPracticeCode = pctcomplexmerger.PracticeCode
	
	left outer join Income.ExcludedPatient excludedpatient
	on excludedpatient.SourcePatientNo = encounter.SourcePatientNo
	and encounter.PCTCode not like '6%'
	and encounter.PCTCode not like '7%'
	and encounter.AppointmentDate between excludedpatient.EffectiveFromDate and coalesce(excludedpatient.EffectiveToDate, getdate())
	
	
	/* Exclusions */
	
	outer apply 
	
	(
		select top 1
			ExclusionCode
		from	
			Income.OPExclusionRuleBase exclusionrulebase
		where
			exclusionrulebase.ContextCode = 'CEN||PAS'
			and (encounter.PCTCode = exclusionrulebase.PCTCode or exclusionrulebase.PCTCode is null)
			and (encounter.ReferringSpecialtyCode = exclusionrulebase.PASSpecialtyCode or exclusionrulebase.PASSpecialtyCode is null)
			and (specialty.NationalSpecialtyCode = exclusionrulebase.NationalSpecialtyCode or exclusionrulebase.NationalSpecialtyCode is null)
			and (encounter.ClinicCode = exclusionrulebase.ClinicCode or exclusionrulebase.ClinicCode is null)
			and (encounter.DerivedFirstAttendanceFlag = exclusionrulebase.FirstAttendanceFlag or exclusionrulebase.FirstAttendanceFlag is null)
			and (encounter.PrimaryOperationCode = exclusionrulebase.PrimaryProcedureCode or exclusionrulebase.PrimaryProcedureCode is null)
			and (divisionmap.Diagnostic = exclusionrulebase.DiagnosisFlagCode or exclusionrulebase.DiagnosisFlagCode is null)								
			and (encounter.ContractSerialNo = exclusionrulebase.CommissioningSerialNo or exclusionrulebase.CommissioningSerialNo is null)
			and (encounter.LocalAdminCategoryCode = exclusionrulebase.LocalAdminCategoryCode or exclusionrulebase.LocalAdminCategoryCode is null)
			and (encounter.AppointmentDate between exclusionrulebase.EffectiveFromDate and coalesce(exclusionrulebase.EffectiveToDate, getdate()))
	
		) exclusionrulebase
	
where
	encounter.AppointmentDate between @start and @end
	
and
	AppointmentStatusCode in 
							(
							'ATT'
							,'ATTC'
							,'ATTP'
							,'ATTU'
							,'1'
							--,'3'
							,'SATT'
							,'WLK'
							)
	and divisionmap.Diagnostic in
								(
								'I'
								,'N'
								,'X'
								)

and
	not exists
		(
		select
			1
		from
			Warehouse.WH.DivisionMap Previous
		where
			Previous.SiteCode = Encounter.EpisodicSiteCode
		and	Previous.SpecialtyCode = Encounter.ReferringSpecialtyCode


		and 
			(
				Previous.ConsultantCode = Encounter.ConsultantCode
			or
				Previous.ConsultantCode is null
			)

		and
			Encounter.AppointmentDate between Previous.FromDate and coalesce(Previous.ToDate, '31 dec 9999')

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end

				>

				case
				when DivisionMap.ConsultantCode is not null
				then 4
				else 0
				end
	
			--or
			--	(
			--		case
			--		when Previous.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end

			--		=

			--		case
			--		when DivisionRuleBase.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end 

				)
			)
								
	--and SourceUniqueID = '1000212||5||201104070830||MJNAC'						
			
	--and encounter.SourcePatientNo = '1555675'
	--and encounter.SourceEncounterNo = 8
	--and AppointmentDate = '21 april 2011'

--PCT Variance
--BMCHT 3039740*2*201104061020*JDFOL
--BMCHT 3169414*1*201104111500*PSOH
--BMCHT 64591*3*201104140830*NUTHAM
--BMCHT 1984878*16*201104181430*SVMWF
--BMCHT 3169898*9*201104260830*MJNAC

--Specialty Variance
--BMCHT 1555675*8*201104211000*ETCPRE


select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid;

--exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


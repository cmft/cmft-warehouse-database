﻿
create proc ETL.ImportIncomeAE

(
	@start datetime
	,@end datetime
	,@period varchar(20) -- format example 2012/2013 Month 1
	,@position varchar(50) -- flex/freeze
	,@version int
)

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255)
select @StartTime = getdate();

--if object_id('ETL.TImportIncomeAE') is not null 
--drop table ETL.TImportIncomeAE
	
truncate table ETL.TImportIncomeAE

insert into WarehouseReporting.[ETL].[TImportIncomeAE]
(
			[Period]
           ,[Position]
		   ,[Version]
          , EncounterRecno
           ,[SourceUniqueID]
           ,[DistrictNo]
           ,[DistrictNoOrganisationCode]
           ,[NHSNumber]
           ,[NHSNumberStatusId]
           ,[PatientForename]
           ,[PatientSurname]
           ,[Postcode]
           ,[DateOfBirth]
           ,[DateOfDeath]
           ,[SexCode]
		   ,[RegisteredGpCode]
           ,[RegisteredGpPracticeCode]
           ,[AttendanceNumber]
           ,[ArrivalModeCode]
           ,[AttendanceCategoryCode]
           ,[AttendanceDisposalCode]
           ,[IncidentLocationTypeCode]
           ,[PatientGroupCode]
           ,[SourceOfReferralCode]
           ,[ArrivalDate]
           ,[ArrivalTime]
           ,[AgeOnArrival]
           ,[CommissioningSerialNo]
           ,[NHSServiceAgreementLineNo]
           ,[ProviderReferenceNo]
           ,[CommissionerReferenceNo]
           ,[ProviderCode]
           ,[CommissionerCode]
           ,[StaffMemberCode]
           ,[InvestigationCodeFirst]
           ,[InvestigationCodeSecond]
           ,[DiagnosisCodeFirst]
           ,[DiagnosisCodeSecond]
           ,[TreatmentCodeFirst]
           ,[TreatmentCodeSecond]
           ,[SiteCode]
           ,[InterfaceCode]
           ,[PCTCode]
           ,[ResidencePCTCode]
           ,[TriageCategoryCode]
           ,[PresentingProblem]
           ,[ReferredToSpecialtyCode]
           ,[DischargeDestinationCode]
           ,[SourceAttendanceDisposalCode]
           ,[Reportable]
		   ,[GpCodeAtArrival]
		   ,[GpPracticeCodeAtArrival]
		   ,[PostCodeAtArrival]

)

select
	Period = @period
	,Position = @position
	,[Version] = @version
	,EncounterRecno
      ,[SourceUniqueID]
      ,encounter.[DistrictNo]
      ,[DistrictNoOrganisationCode]
      ,encounter.[NHSNumber]
      ,encounter.[NHSNumberStatusId]
      ,[PatientForename]
      ,[PatientSurname]
      ,encounter.[Postcode]
      ,encounter.[DateOfBirth]
      ,encounter.[DateOfDeath]
      ,encounter.[SexCode]
	  ,encounter.[RegisteredGpCode]
      ,[RegisteredGpPracticeCode]
      ,[AttendanceNumber]
      ,[ArrivalModeCode]
      ,[AttendanceCategoryCode]
      ,[AttendanceDisposalCode]
      ,[IncidentLocationTypeCode]
      ,[PatientGroupCode]
      ,[SourceOfReferralCode]
      ,[ArrivalDate]
      ,[ArrivalTime]
      ,[AgeOnArrival]
      ,[CommissioningSerialNo]
      ,[NHSServiceAgreementLineNo]
      ,[ProviderReferenceNo]
      ,[CommissionerReferenceNo]
      ,[ProviderCode]
      ,[CommissionerCode]
      ,[StaffMemberCode]
      ,[InvestigationCodeFirst]
      ,[InvestigationCodeSecond]
      ,[DiagnosisCodeFirst]
      ,[DiagnosisCodeSecond]
      ,[TreatmentCodeFirst]
      ,[TreatmentCodeSecond]
      ,[SiteCode]
      ,[InterfaceCode]
      ,[PCTCode] -- amended following conversation with Tim
				=
					case
						when encounter.InterfaceCode = 'ADAS'
						then '5NT'
						when coalesce(-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
								patientaddressatarrival.Postcode
								,encounter.Postcode
								) like 'ZZ99%'
						or CommissioningSerialNo = 'OSV00A'
						then 'OSV'
						when CommissioningSerialNo <> 'OSV00A'
						and CommissioningSerialNo like 'OSV%'
						then 'YAC'
						else
							coalesce(
								 --PCT.OrganisationCode -- joined from PurchaserCode
								Practice.ParentOrganisationCode
								,Postcode.PCTCode
								,'5NT'
							)
					end
						

	  /* taken from CommissionerCode from A%E CDS */
			--= 
			--case

			--when Encounter.PCTCode like '0%' --scottish
			--	then Postcode.DistrictOfResidence
			--when Encounter.PatientAddress1 like 'N%F%A%' --no fixed abode
			--then '5NT'
			--	--case
			--	--when Encounter.ContextCode like 'TRA||%' then '5NR'
			--	--when Encounter.ContextCode like 'CEN||%' then '5NT'
			--	--end

			--when
			--	Encounter.CommissioningSerialNo is null
			--or	Encounter.CommissionerCode = 'X98'
			--then
			--	case
			--	when Encounter.CommissionerCode = 'TDH' then 'OSV'
			--	when PatientAddress1 like 'Visit%' then 'OSV'
			--	else '5NT'
			--		--case
			--		--when Encounter.ContextCode like 'TRA||%' then '5NR'
			--		--when Encounter.ContextCode like 'CEN||%' then '5NT'
			--		--end
			--	end
			--else left(Encounter.CommissioningSerialNo, 3)
			--end
      ,[ResidencePCTCode]
      ,[TriageCategoryCode]
      ,[PresentingProblem]
      ,[ReferredToSpecialtyCode]
      ,[DischargeDestinationCode]
      ,[SourceAttendanceDisposalCode]
      ,[Reportable],
	   [GpCodeAtArrival] = 		
						coalesce(
						 CASE WHEN EpisodicGpAtArrival.GpCode = 'G9999981' THEN NULL ELSE EpisodicGpAtArrival.GpCode END
						,Encounter.RegisteredGpCode
						,'G9999998'
								)
	  ,[GpPracticeCodeAtArrival] = -- taken from ReferenceMap derivation
					coalesce(
						 CASE WHEN EpisodicGpAtArrival.GpPracticeCode = 'V81998' THEN NULL ELSE EpisodicGpAtArrival.GpPracticeCode END
						,Encounter.[RegisteredGpPracticeCode]
						,'V81999'
								)
	,[PostCodeAtArrival] = patientaddressatarrival.Postcode
from 
	Warehouse.AE.Encounter encounter

	left outer join Warehouse.PAS.Patient
	on	Patient.DistrictNo = Encounter.DistrictNo

	left outer join Warehouse.PAS.PatientAddress patientaddressatarrival
	on patientaddressatarrival.SourcePatientNo = Patient.SourcePatientNo
	and ArrivalTime between patientaddressatarrival.EffectiveFromDate and coalesce(patientaddressatarrival.EffectiveToDate, getdate())
	
	left outer join Warehouse.PAS.PatientGp EpisodicGpAtArrival
	on EpisodicGpAtArrival.SourcePatientNo = Patient.SourcePatientNo
	and Encounter.ArrivalDate between EpisodicGpAtArrival.EffectiveFromDate and coalesce(EpisodicGpAtArrival.EffectiveToDate,getdate())

	left outer join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = EpisodicGpAtArrival.GpPracticeCode

	left outer join Organisation.dbo.Postcode Postcode 
	on Postcode.Postcode = 
						case
							when len(patientaddressatarrival.Postcode) = 8 then patientaddressatarrival.Postcode
							else left(patientaddressatarrival.Postcode, 3) + ' ' + right(patientaddressatarrival.Postcode, 4) 
						end

	left outer join       
					(
					select
						[OrganisationCode]
					from
						[Organisation].[dbo].[PCT]
					where 
						ROCode like 'Y%'
					) englishpcts

	on englishpcts.[OrganisationCode] = encounter.PCTCode

where
	[ArrivalDate] between @start and @end
	and Reportable = 1
	--and EncounterRecno = 3224445

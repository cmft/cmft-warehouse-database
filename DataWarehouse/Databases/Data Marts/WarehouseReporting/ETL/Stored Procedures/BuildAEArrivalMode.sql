﻿CREATE PROCEDURE [ETL].[BuildAEArrivalMode]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseAEArrivalMode

	INSERT INTO  ETL.BaseAEArrivalMode

	SELECT 
		 ArrivalModeCode
		,ArrivalMode

	FROM 
		 WarehouseOLAP.dbo.OlapAEArrivalMode

END

﻿
create proc ETL.ImportIncomeBedday
(
	@start datetime
	,@end datetime
	,@period varchar(20) -- format example 2012/2013 Month 1
	,@position varchar(50) -- flex/freeze
	,@version int
)

as

--if object_id('ETL.TImportIncomeBedday') is not null
--drop table ETL.TImportIncomeBedday

truncate table [ETL].[TImportIncomeBedday]

insert into	[ETL].[TImportIncomeBedday]
(
	[Period]
	,[Position]
	,[Version]
	,[MonthNumber]
	,[StartDate]
	,[EndDate]
	,[StartTime]
	,[EndTime]
	,[SiteCode]
	,[WardCode]
	,[PODCode]
	,[HRGCode]
	,[SpecialtyCode]
	,[DirectorateCode]
	,[EncounterRecno]
	,[EpisodeStartDate]
	,[EpisodeEndDate]
	,[ProviderSpellNo]
	,[SourceEncounterNo]
	,SourcePatientNo
	,PCTCode
	,GpCode
	,[GpPracticeCode] 
	,[PostCode]
	,[CensusDate]
	,[Bedday]
)


/* pods excluding realtime - these are calculated between admission and discharge dates */

select
	Period = @period
	,Position = @position
	,Version = @version
	,MonthNumber = datepart(Month, dateadd(month, -3, @start)) 
	,wardstay.[StartDate]
	,wardstay.[EndDate]
	,wardstay.[StartTime]
	,wardstay.[EndTime]
	,wardstay.[SiteCode]
	,wardstay.[WardCode]
	,beddayandhrg.[PODCode]
	,beddayandhrg.[HRGCode]
	,beddayandhrg.[SpecialtyCode]
	,beddayandhrg.[DirectorateCode]
	,encounter.EncounterRecno
	,encounter.EpisodeStartDate
	,encounter.EpisodeEndDate
	,encounter.[ProviderSpellNo]
	,encounter.[SourceEncounterNo]
	,encounter.SourcePatientNo
	,PCTCode = -- taken from the CDS derivaton for purchaser code - agreed with Tim 19/06/2012
			left(
			case
				when AdminCategoryCode = '02' then 'VPP00'
				when coalesce(									-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
							patientaddressatdischarge.Postcode
							,encounter.Postcode
							) like 'ZZ99%'
				or ContractSerialNo = 'OSV00A'
				then 'OSV'
				when ContractSerialNo <> 'OSV00A'
				and ContractSerialNo like 'OSV%'
				then 'YAC'
				else
					coalesce(
						 PCT.OrganisationCode -- joined from PurchaserCode
						,Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,'5NT'
					)	
			end
			, 3)
	,GpCode = RegisteredGpPracticeAtDischarge.GpCode
	,[GpPracticeCode] = RegisteredGpPracticeAtDischarge.GpPracticeCode
	,[PostCode] = patientaddressatdischarge.Postcode
	,CensusDate = calendar.TheDate
	,Bedday = 1
--into 
--	ETL.TImportIncomeBedday
from 
	WH.[Calendar] calendar

	inner join APC.WardStay wardstay
	on 	calendar.TheDate >= wardstay.StartDate
	and calendar.TheDate <= 
							case
								when EndActivityCode = 'ADS'
								then (
										select 
											top 1 dateadd(day, -1, StartDate)
										from
											Warehouse.APC.WardStay wardstay1
										where
											wardstay1.ProviderSpellNo = wardstay.ProviderSpellNo
											and wardstay1.StartDate > wardstay.StartDate
										order by 
											StartDate asc	
									)
								else wardstay.EndDate
							end

	inner join Income.BedDayPODAndHRG beddayandhrg
	on	wardstay.WardCode = beddayandhrg.WardCode

	inner join [APC].[Encounter] encounter
	on 	calendar.TheDate >= encounter.[EpisodeStartDate]
	and calendar.TheDate <= encounter.[EpisodeEndDate]
	and encounter.ProviderSpellNo = wardstay.ProviderSpellNo

	left outer join Warehouse.PAS.PatientAddress patientaddressatdischarge
	on patientaddressatdischarge.SourcePatientNo = encounter.SourcePatientNo
	and encounter.DischargeDate between patientaddressatdischarge.EffectiveFromDate and coalesce(patientaddressatdischarge.EffectiveToDate, getdate())
		
	left outer join Warehouse.PAS.PatientGp RegisteredGpPracticeAtDischarge
	on RegisteredGpPracticeAtDischarge.SourcePatientNo = Encounter.SourcePatientNo
	and Encounter.DischargeDate between RegisteredGpPracticeAtDischarge.EffectiveFromDate and coalesce(RegisteredGpPracticeAtDischarge.EffectiveToDate, getdate())

	/* to cleanse the PurchaserCode values - we have old PCT codes in the column - requested by Tim 28/06/2012 */
	left outer join Organisation.dbo.PCT PCT
	on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode
	
	left outer join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = RegisteredGpPracticeAtDischarge.GpPracticeCode
		
	left outer join Organisation.dbo.Postcode Postcode 
	on Postcode.Postcode = 
						case
							when len(patientaddressatdischarge.Postcode) = 8 then patientaddressatdischarge.Postcode
							else left(patientaddressatdischarge.Postcode, 3) + ' ' + right(patientaddressatdischarge.Postcode, 4) 
						end


where
	beddayandhrg.WardCode not in 
								(
								'83'
								,'87'
								,'28M'
								)
	and encounter.DischargeDate between @start and @end
	
	/*	remove potential to double count at episode junctions - 
		where one episode ends and another starts on same day
		the bedday is apportioned to the later episode
	*/

	and
		not exists
				(
				select
					1
				from 
					WH.[Calendar] calendarnext

				inner join APC.WardStay wardstaynext
				on 	calendarnext.TheDate >= wardstaynext.StartDate
				and calendarnext.TheDate <= 
										case
											when EndActivityCode = 'ADS'
											then (
													select 
														top 1 dateadd(day, -1, StartDate)
													from
														Warehouse.APC.WardStay wardstaynext1
													where
														wardstaynext1.ProviderSpellNo = wardstaynext.ProviderSpellNo
														and wardstaynext1.StartDate > wardstaynext.StartDate
													order by 
														StartDate asc	
												)
											else wardstaynext.EndDate
										end

				inner join Income.BedDayPODAndHRG beddayandhrgnext
				on	wardstaynext.WardCode = beddayandhrgnext.WardCode

				inner join [APC].[Encounter] encounternext
				on 	calendarnext.TheDate >= encounternext.[EpisodeStartDate]
				and calendarnext.TheDate <= encounternext.[EpisodeEndDate]
				and encounternext.ProviderSpellNo = wardstaynext.ProviderSpellNo

				where
					calendarnext.TheDate = calendar.TheDate
					and encounternext.SourceEncounterNo > encounter.SourceEncounterNo
					and encounternext.ProviderSpellNo = encounter.ProviderSpellNo

				)

/* realtime PODs - calculated between the month extracted */

insert into ETL.TImportIncomeBedday
(
	[Period]
	,[Position]
	,[Version]
	,[MonthNumber]
	,[StartDate]
	,[EndDate]
	,[StartTime]
	,[EndTime]
	,[SiteCode]
	,[WardCode]
	,[PODCode]
	,[HRGCode]
	,[SpecialtyCode]
	,[DirectorateCode]
	,[EncounterRecno]
	,[EpisodeStartDate]
	,[EpisodeEndDate]
	,[ProviderSpellNo]
	,[SourceEncounterNo]
	,SourcePatientNo
	,PCTCode
	,GpCode
	,[GpPracticeCode] 
	,[PostCode]
	,[CensusDate]
	,[Bedday]
)
select
	Period = @period
	,Position = @position
	,Version = @version
	,MonthNumber = datepart(Month, dateadd(month, -3, @start)) 
	,wardstay.[StartDate]
	,wardstay.[EndDate]
	,wardstay.[StartTime]
	,wardstay.[EndTime]
	,wardstay.[SiteCode]
	,wardstay.[WardCode]
	,beddayandhrg.[PODCode]
	,beddayandhrg.[HRGCode]
	,beddayandhrg.[SpecialtyCode]
	,beddayandhrg.[DirectorateCode]
	,encounter.EncounterRecno
	,encounter.EpisodeStartDate
	,encounter.EpisodeEndDate
	,encounter.[ProviderSpellNo]
	,encounter.[SourceEncounterNo]
	,encounter.SourcePatientNo
	,PCTCode = -- taken from the CDS derivaton for purchaser code - agreed with Tim 19/06/2012
			left(
			case
				when AdminCategoryCode = '02' then 'VPP00'
				when coalesce(									-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
							patientaddressatadmission.Postcode
							,encounter.Postcode
							) like 'ZZ99%'
				or ContractSerialNo = 'OSV00A'
				then 'OSV'
				when ContractSerialNo <> 'OSV00A'
				and ContractSerialNo like 'OSV%'
				then 'YAC'
				else
					coalesce(
						 PCT.OrganisationCode -- joined from PurchaserCode
						,Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,'5NT'
					)	
			end
			, 3)
	,GpCodeAtDischarge = RegisteredGpPracticeAtAdmission.GpCode
	,[GpPracticeCodeAtDischarge] = RegisteredGpPracticeAtAdmission.GpPracticeCode
	,[PostCodeAtDischarge] = patientaddressatAdmission.Postcode
	,CensusDate = calendar.TheDate
	,Bedday = 1
from 
	WH.[Calendar] calendar

	inner join APC.WardStay wardstay
	on 	calendar.TheDate >= wardstay.StartDate
	and calendar.TheDate <= 
							coalesce(
							case
								when EndActivityCode = 'ADS'
								then (
										select 
											top 1 dateadd(day, -1, StartDate)
										from
											Warehouse.APC.WardStay wardstay1
										where
											wardstay1.ProviderSpellNo = wardstay.ProviderSpellNo
											and wardstay1.StartDate > wardstay.StartDate
										order by 
											StartDate asc	
									)
								else wardstay.EndDate
							end
							,
							@end
							)
									

	inner join Income.BedDayPODAndHRG beddayandhrg
	on	wardstay.WardCode = beddayandhrg.WardCode

	inner join [APC].[Encounter] encounter
	on 	calendar.TheDate >= encounter.[EpisodeStartDate]
	and calendar.TheDate <= coalesce(
									encounter.[EpisodeEndDate]
									,@end
									)
	and encounter.ProviderSpellNo = wardstay.ProviderSpellNo


	left outer join Warehouse.PAS.PatientAddress patientaddressatadmission
	on patientaddressatadmission.SourcePatientNo = encounter.SourcePatientNo
	and encounter.AdmissionDate between patientaddressatadmission.EffectiveFromDate and coalesce(patientaddressatadmission.EffectiveToDate, getdate())
		
	left outer join Warehouse.PAS.PatientGp RegisteredGpPracticeAtAdmission
	on RegisteredGpPracticeAtAdmission.SourcePatientNo = Encounter.SourcePatientNo
	and Encounter.AdmissionDate between RegisteredGpPracticeAtAdmission.EffectiveFromDate and coalesce(RegisteredGpPracticeAtAdmission.EffectiveToDate, getdate())

	/* to cleanse the PurchaserCode values - we have old PCT codes in the PurchaserCode column - requested by Tim 28/06/2012 */
	left outer join Organisation.dbo.PCT PCT
	on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode
	
	left outer join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = RegisteredGpPracticeAtAdmission.GpPracticeCode
		
	left outer join Organisation.dbo.Postcode Postcode 
	on Postcode.Postcode = 
						case
							when len(patientaddressatadmission.Postcode) = 8 then patientaddressatadmission.Postcode
							else left(patientaddressatadmission.Postcode, 3) + ' ' + right(patientaddressatadmission.Postcode, 4) 
						end

where
	beddayandhrg.WardCode in 
							(
							'83' -- lttu
							,'87' --camhs
							,'28M' --rehabt
							)
	and calendar.[TheDate] between @start and @end -- restricts to only the month of extraction
	
	/*	remove potential to double count at episode junctions - 
		where one episode ends and another starts on same day
		the bedday is apportioned to the later episode
	*/

	and
		not exists
				(
				select
					1
				from 
					WH.[Calendar] calendarnext

				inner join APC.WardStay wardstaynext
				on 	calendarnext.TheDate >= wardstaynext.StartDate
				and calendarnext.TheDate <= 
											coalesce(
													case
														when EndActivityCode = 'ADS'
														then (
																select 
																	top 1 dateadd(day, -1, StartDate)
																from
																	Warehouse.APC.WardStay wardstaynext1
																where
																	wardstaynext1.ProviderSpellNo = wardstaynext.ProviderSpellNo
																	and wardstaynext1.StartDate > wardstaynext.StartDate
																order by 
																	StartDate asc	
															)
														else wardstaynext.EndDate
													end
													,@end
													)


				inner join Income.BedDayPODAndHRG beddayandhrgnext
				on	wardstaynext.WardCode = beddayandhrgnext.WardCode

				inner join [APC].[Encounter] encounternext
				on 	calendarnext.TheDate >= encounternext.[EpisodeStartDate]
				and calendarnext.TheDate <= encounternext.[EpisodeEndDate]
				and encounternext.ProviderSpellNo = wardstaynext.ProviderSpellNo

				where
					calendarnext.TheDate = calendar.TheDate
					and encounternext.SourceEncounterNo > encounter.SourceEncounterNo
					and encounternext.ProviderSpellNo = encounter.ProviderSpellNo

				)

/* Neonate */

insert into ETL.TImportIncomeBedday
(
	[Period]
	,[Position]
	,[Version]
	,[MonthNumber]
	,[StartDate]
	,[EndDate]
	,[StartTime]
	,[EndTime]
	,[SiteCode]
	,[WardCode]
	,[PODCode]
	,[HRGCode]
	,[SpecialtyCode]
	,[DirectorateCode]
	,[EncounterRecno]
	,[EpisodeStartDate]
	,[EpisodeEndDate]
	,[ProviderSpellNo]
	,[SourceEncounterNo]
	,SourcePatientNo
	,PCTCode
	,GpCode
	,[GpPracticeCode] 
	,[PostCode]
	,[CensusDate]
	,[Bedday]
)

select
	Period = @period
	,Position = @position
	,Version = @version
	,MonthNumber = datepart(Month, dateadd(month, -3, @start)) 
	,null --wardstay.[StartDate]
	,null --wardstay.[EndDate]
	,null --wardstay.[StartTime]
	,null --wardstay.[EndTime]
	,null --wardstay.[SiteCode]
	,null --wardstay.[WardCode]
	,beddayandhrg.[PODCode]
	,beddayandhrg.[HRGCode]
	,beddayandhrg.[SpecialtyCode]
	,encounter.EndDirectorateCode
	,encounter.EncounterRecno
	,encounter.EpisodeStartDate
	,encounter.EpisodeEndDate
	,encounter.[ProviderSpellNo]
	,encounter.[SourceEncounterNo]
	,encounter.SourcePatientNo
	,PCTCode = -- taken from the CDS derivaton for purchaser code - agreed with Tim 19/06/2012
			left(
			case
				when AdminCategoryCode = '02' then 'VPP00'
				when coalesce(									-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
							patientaddressatdischarge.Postcode
							,encounter.Postcode
							) like 'ZZ99%'
				or ContractSerialNo = 'OSV00A'
				then 'OSV'
				when ContractSerialNo <> 'OSV00A'
				and ContractSerialNo like 'OSV%'
				then 'YAC'
				else
					coalesce(
						 PCT.OrganisationCode -- joined from PurchaserCode
						,Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,'5NT'
					)	
			end
			, 3)
	,GpCode = RegisteredGpPracticeAtDischarge.GpCode
	,[GpPracticeCode] = RegisteredGpPracticeAtDischarge.GpPracticeCode
	,[PostCode] = patientaddressatdischarge.Postcode
	,CensusDate = calendar.TheDate
	,Bedday = 1
from 
	WH.[Calendar] calendar

	--inner join APC.WardStay wardstay
	--on 	calendar.TheDate >= wardstay.StartDate
	--and calendar.TheDate <= 
	--						case
	--							when EndActivityCode = 'ADS'
	--							then (
	--									select 
	--										top 1 dateadd(day, -1, StartDate)
	--									from
	--										Warehouse.APC.WardStay wardstay1
	--									where
	--										wardstay1.ProviderSpellNo = wardstay.ProviderSpellNo
	--										and wardstay1.StartDate > wardstay.StartDate
	--									order by 
	--										StartDate asc	
	--								)
	--							else wardstay.EndDate
	--						end

	inner join [APC].[Encounter] encounter
	on 	calendar.TheDate >= encounter.[EpisodeStartDate]
	and calendar.TheDate <= encounter.[EpisodeEndDate]
	--and encounter.ProviderSpellNo = wardstay.ProviderSpellNo

	inner join Income.BedDayPODAndHRG beddayandhrg
	on	case
			when encounter.[NeonatalLevelOfCare] in ('1','S') 
			then 'SPECIAL'
			when encounter.[NeonatalLevelOfCare] in ('2','H') 
			then 'HIGH'
			when encounter.[NeonatalLevelOfCare] in ('3','M') 
			then 'MAXIMUM'
		end = beddayandhrg.WardCode

	left outer join Warehouse.PAS.PatientAddress patientaddressatdischarge
	on patientaddressatdischarge.SourcePatientNo = encounter.SourcePatientNo
	and encounter.DischargeDate between patientaddressatdischarge.EffectiveFromDate and coalesce(patientaddressatdischarge.EffectiveToDate, getdate())
		
	left outer join Warehouse.PAS.PatientGp RegisteredGpPracticeAtDischarge
	on RegisteredGpPracticeAtDischarge.SourcePatientNo = Encounter.SourcePatientNo
	and Encounter.DischargeDate between RegisteredGpPracticeAtDischarge.EffectiveFromDate and coalesce(RegisteredGpPracticeAtDischarge.EffectiveToDate, getdate())

	/* to cleanse the PurchaserCode values - we have old PCT codes in the column - requested by Tim 28/06/2012 */
	left outer join Organisation.dbo.PCT PCT
	on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode
	
	left outer join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = RegisteredGpPracticeAtDischarge.GpPracticeCode
		
	left outer join Organisation.dbo.Postcode Postcode 
	on Postcode.Postcode = 
						case
							when len(patientaddressatdischarge.Postcode) = 8 then patientaddressatdischarge.Postcode
							else left(patientaddressatdischarge.Postcode, 3) + ' ' + right(patientaddressatdischarge.Postcode, 4) 
						end

where
	encounter.DischargeDate between @start and @end
	and encounter.[NeonatalLevelOfCare] in
										(
										'1'
										,'2'
										,'3'
										,'H'
										,'M'
										,'S'
										)
	
	/*	remove potential to double count at episode junctions - 
		where one episode ends and another starts on same day
		the bedday is apportioned to the later episode
	*/

	and
		not exists
				(
				select
					1
				from 
					WH.[Calendar] calendarnext

				--inner join APC.WardStay wardstaynext
				--on 	calendarnext.TheDate >= wardstaynext.StartDate
				--and calendarnext.TheDate <= 
				--						case
				--							when EndActivityCode = 'ADS'
				--							then (
				--									select 
				--										top 1 dateadd(day, -1, StartDate)
				--									from
				--										Warehouse.APC.WardStay wardstaynext1
				--									where
				--										wardstaynext1.ProviderSpellNo = wardstaynext.ProviderSpellNo
				--										and wardstaynext1.StartDate > wardstaynext.StartDate
				--									order by 
				--										StartDate asc	
				--								)
				--							else wardstaynext.EndDate
				--						end



				inner join [APC].[Encounter] encounternext
				on 	calendarnext.TheDate >= encounternext.[EpisodeStartDate]
				and calendarnext.TheDate <= encounternext.[EpisodeEndDate]
				--and encounternext.ProviderSpellNo = wardstaynext.ProviderSpellNo

				inner join Income.BedDayPODAndHRG beddayandhrgnext
				on	case
						when encounternext.[NeonatalLevelOfCare] in ('1','S') 
						then 'SPECIAL'
						when encounternext.[NeonatalLevelOfCare] in ('2','H') 
						then 'HIGH'
						when encounternext.[NeonatalLevelOfCare] in ('3','M') 
						then 'MAXIMUM'
					end = beddayandhrgnext.WardCode

				where
					calendarnext.TheDate = calendar.TheDate
					and encounternext.SourceEncounterNo > encounter.SourceEncounterNo
					and encounternext.ProviderSpellNo = encounter.ProviderSpellNo

				)

/* ACP (HDPUO) */

insert into	[ETL].[TImportIncomeBedday]
(
	[Period]
	,[Position]
	,[Version]
	,[MonthNumber]
	,[StartDate]
	,[EndDate]
	,[StartTime]
	,[EndTime]
	,[SiteCode]
	,[WardCode]
	,[PODCode]
	,[HRGCode]
	,[SpecialtyCode]
	,[DirectorateCode]
	,[EncounterRecno]
	,[EpisodeStartDate]
	,[EpisodeEndDate]
	,[ProviderSpellNo]
	,[SourceEncounterNo]
	,SourcePatientNo
	,PCTCode
	,GpCode
	,[GpPracticeCode] 
	,[PostCode]
	,[CensusDate]
	,[Bedday]
)

select
	Period = @period
	,Position = @position
	,Version = @version
	,MonthNumber = datepart(Month, dateadd(month, -3, @start)) 
	,augmentedcareperiod.[StartDate]
	,augmentedcareperiod.[EndDate]
	,augmentedcareperiod.[StartTime]
	,augmentedcareperiod.[EndTime]
	,augmentedcarelocation.SiteCode
	,augmentedcarelocation.LocationCode
	,PODCode = 'HDUPO'
	,[HRGCode] = 'HDUPO'
	,[SpecialtyCode] = 192
	,[DirectorateCode] = 0
	,encounter.EncounterRecno
	,encounter.EpisodeStartDate
	,encounter.EpisodeEndDate
	,encounter.[ProviderSpellNo]
	,encounter.[SourceEncounterNo]
	,encounter.SourcePatientNo
	,PCTCode = -- taken from the CDS derivaton for purchaser code - agreed with Tim 19/06/2012
			left(
			case
				when AdminCategoryCode = '02' then 'VPP00'
				when coalesce(									-- added 12/07/2012 as OSV and YAC no longer excluded from process but need to be flagged in PCT column for SLAM 
							patientaddressatdischarge.Postcode
							,encounter.Postcode
							) like 'ZZ99%'
				or ContractSerialNo = 'OSV00A'
				then 'OSV'
				when ContractSerialNo <> 'OSV00A'
				and ContractSerialNo like 'OSV%'
				then 'YAC'
				else
					coalesce(
						 PCT.OrganisationCode -- joined from PurchaserCode
						,Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,'5NT'
					)	
			end
			, 3)
	,GpCode = RegisteredGpPracticeAtDischarge.GpCode
	,[GpPracticeCode] = RegisteredGpPracticeAtDischarge.GpPracticeCode
	,[PostCode] = patientaddressatdischarge.Postcode
	,CensusDate = calendar.TheDate
	,Bedday = 1
from 
	WH.Calendar calendar

	inner join Warehouse.APC.AugmentedCarePeriod augmentedcareperiod
	on	calendar.TheDate >= augmentedcareperiod.StartDate
	and calendar.TheDate <= augmentedcareperiod.EndDate
				
	inner join Warehouse.PAS.AugmentedCareLocation augmentedcarelocation
	on augmentedcareperiod.LocationCode = augmentedcarelocation.LocationCode

	inner join [APC].[Encounter] encounter
	on 	calendar.TheDate >= encounter.[EpisodeStartDate]
	and calendar.TheDate <= encounter.[EpisodeEndDate]
	and encounter.ProviderSpellNo = augmentedcareperiod.ProviderSpellNo
								
	left outer join Income.BedDayPODAndHRG beddayandhrg
	on augmentedcarelocation.WardCode = beddayandhrg.WardCode

	left outer join Warehouse.PAS.PatientAddress patientaddressatdischarge
	on patientaddressatdischarge.SourcePatientNo = encounter.SourcePatientNo
	and encounter.DischargeDate between patientaddressatdischarge.EffectiveFromDate and coalesce(patientaddressatdischarge.EffectiveToDate, getdate())
		
	left outer join Warehouse.PAS.PatientGp RegisteredGpPracticeAtDischarge
	on RegisteredGpPracticeAtDischarge.SourcePatientNo = Encounter.SourcePatientNo
	and Encounter.DischargeDate between RegisteredGpPracticeAtDischarge.EffectiveFromDate and coalesce(RegisteredGpPracticeAtDischarge.EffectiveToDate, getdate())

	/* to cleanse the PurchaserCode values - we have old PCT codes in the column - requested by Tim 28/06/2012 */
	left outer join Organisation.dbo.PCT PCT
	on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode
	
	left outer join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = RegisteredGpPracticeAtDischarge.GpPracticeCode
		
	left outer join Organisation.dbo.Postcode Postcode 
	on Postcode.Postcode = 
						case
							when len(patientaddressatdischarge.Postcode) = 8 then patientaddressatdischarge.Postcode
							else left(patientaddressatdischarge.Postcode, 3) + ' ' + right(patientaddressatdischarge.Postcode, 4) 
						end
				
	where
		encounter.DischargeDate between @start and @end
	and
		(
		augmentedcarelocation.LocationCode <> '81'
		)
	and 
		(
		augmentedcarelocation.SiteCode = 'BHCH' 
		or augmentedcarelocation.SiteCode = 'RMCH'
		or augmentedcarelocation.SiteCode = 'NRMC'
		)	
	and 
		beddayandhrg.WardCode is null
	and
		augmentedcareperiod.EndDate is not null
	

	/*	remove potential to double count at episode junctions - 
		where one episode ends and another starts on same day
		the bedday is apportioned to the later episode
	*/

	and
		not exists
				(
				select
					1
				from 
					WH.Calendar calendarnext

				inner join Warehouse.APC.AugmentedCarePeriod augmentedcareperiodnext
				on	calendarnext.TheDate >= augmentedcareperiodnext.StartDate
				and calendarnext.TheDate <= augmentedcareperiodnext.EndDate
				
				inner join Warehouse.PAS.AugmentedCareLocation augmentedcarelocationnext
				on augmentedcareperiodnext.LocationCode = augmentedcarelocationnext.LocationCode

				inner join [APC].[Encounter] encounternext
				on 	calendarnext.TheDate >= encounternext.[EpisodeStartDate]
				and calendarnext.TheDate <= encounternext.[EpisodeEndDate]
				and encounternext.ProviderSpellNo = augmentedcareperiodnext.ProviderSpellNo
								
				left outer join Income.BedDayPODAndHRG beddayandhrgnext
				on augmentedcarelocationnext.WardCode = beddayandhrgnext.WardCode
				
				where	
					(
					augmentedcarelocationnext.LocationCode <> '81'
					)
				and 
					(
					augmentedcarelocationnext.SiteCode = 'BHCH' 
					or augmentedcarelocationnext.SiteCode = 'RMCH'
					or augmentedcarelocationnext.SiteCode = 'NRMC'
					)	
				and 
					beddayandhrgnext.WardCode is null
				and
					calendarnext.TheDate = calendar.TheDate
					and encounternext.SourceEncounterNo > encounter.SourceEncounterNo
					and encounternext.ProviderSpellNo = encounter.ProviderSpellNo
				)




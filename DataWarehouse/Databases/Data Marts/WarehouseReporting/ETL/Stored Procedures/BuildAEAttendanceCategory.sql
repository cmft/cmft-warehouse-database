﻿CREATE PROCEDURE [ETL].[BuildAEAttendanceCategory]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseAEAttendanceCategory

	INSERT INTO  ETL.BaseAEAttendanceCategory

	SELECT 
		 AttendanceCategoryCode
        ,AttendanceCategory
        ,AttendanceCategoryGroup

	FROM 
		 WarehouseOLAP.dbo.OlapAEAttendanceCategory

END

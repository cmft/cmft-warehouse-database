﻿

CREATE procedure [ETL].[LoadAPCPatientSurvey] as

truncate table APC.PatientSurvey

insert into	APC.PatientSurvey

select
	 CensusDate = convert(date , StatisticsDate)
	,NHSNumber = Patient.NHSNumber
	,CasenoteNumber = AdmitDisch.CaseNoteNumber
	,WardCode = MBS.Ward
	,Ward = Ward.WardName
	,Patient.Forenames
	,Patient.Surname
	,IntendedProcedureCode = WLEntry.IntendedProc
	,OPCS.ProcedureType
from
	PAS.Inquire.MIDNIGHTBEDSTATE MBS

inner join PAS.Inquire.PATDATA Patient
on	Patient.InternalPatientNumber = MBS.InternalPatientNumber

inner join PAS.Inquire.WARD Ward
on	Ward.WARDID = MBS.Ward

inner join PAS.Inquire.ADMITDISCH AdmitDisch
on	AdmitDisch.InternalPatientNumber = MBS.InternalPatientNumber
and	AdmitDisch.EpisodeNumber = MBS.EpisodeNumber

inner join PAS.Inquire.WLENTRY WLEntry
on	WLEntry.InternalPatientNumber = MBS.InternalPatientNumber
and	WLEntry.EpisodeNumber = MBS.EpisodeNumber

inner join 
	--Hip Replacement
	(
		  select ProcedureCode = 'W37' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W38' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W39' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W46' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W47' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W48' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W93' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W94' , ProcedureType = 'Hip'
	union select ProcedureCode = 'W95' , ProcedureType = 'Hip'

	--Knee Replacement
	union select ProcedureCode = 'W40' , ProcedureType = 'Knee'
	union select ProcedureCode = 'W41' , ProcedureType = 'Knee'
	union select ProcedureCode = 'W42' , ProcedureType = 'Knee'

	--CABG 
	union select ProcedureCode = 'K40' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K41' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K42' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K43' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K44' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K45' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K46' , ProcedureType = 'CABG'
	union select ProcedureCode = 'K47' , ProcedureType = 'CABG'
	) OPCS
on	OPCS.ProcedureCode = left(WLEntry.IntendedProc , 3)

where
	StatisticsDate =
		(
		select
			max(StatisticsDate)
		from
			PAS.Inquire.MIDNIGHTBEDSTATE
		)

﻿

create proc [ETL].LoadIncomeAEQuality

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()


insert into WarehouseReporting.[Income].[AEHRGQuality]
           (
			SnapshotRecno
           ,[Period]
           ,[Position]
		   ,[Version]
           ,[ReportMasterID]
           ,[QualityTypeCode]
           ,[QualityCode]
           ,[QualityMessage]
           )
    
select
	SnapshotRecno
	,[Period]
	,[Position]
	,[Version]
	,[ReportMasterID]
	,[QualityTypeCode]
	,[QualityCode]
	,[QualityMessage]
from
	ETL.TLoadIncomeAEHRGQuality
	



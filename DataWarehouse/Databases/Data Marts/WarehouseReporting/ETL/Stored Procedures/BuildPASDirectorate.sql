﻿CREATE PROCEDURE [ETL].[BuildPASDirectorate]

AS

BEGIN
	TRUNCATE TABLE  ETL.BasePASDirectorate

	INSERT INTO  ETL.BasePASDirectorate

	SELECT 
		   DirectorateCode
		  ,Directorate
		  ,Division = OlapDivision.Division
	
	FROM WarehouseOLAP.dbo.OlapDirectorate
	left outer join WarehouseOLAP.dbo.OlapDivision
		on OlapDirectorate.DivisionCode = OlapDivision.DivisionCode

END

﻿



CREATE PROCEDURE [ETL].[BuildWHDiagnosis]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseWHDiagnosis

	INSERT INTO  ETL.BaseWHDiagnosis

	SELECT 
	   DiagnosisCode
      ,Diagnosis
      ,DiagnosisChapterCode
      ,DiagnosisChapter
	,[IsCharlsonFlag]
		,[IsConnectingForHealthMandatoryFlag]
		,[IsAlwaysPresentFlag]
		,[IsLongTermFlag]

	FROM WarehouseOLAP.dbo.OlapDiagnosis

END




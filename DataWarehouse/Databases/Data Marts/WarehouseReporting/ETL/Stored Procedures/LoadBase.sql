﻿CREATE PROCEDURE [ETL].[LoadBase]

AS

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)


EXEC ETL.BuildAEArrivalMode
EXEC ETL.BuildAEAttendanceCategory
EXEC ETL.BuildAEAttendanceDisposal
EXEC ETL.BuildAEDiagnosis
EXEC ETL.BuildPASAdmissionMethod
EXEC ETL.BuildPASConsultant
EXEC ETL.BuildPASDirectorate
EXEC ETL.BuildPASSite
EXEC ETL.BuildPASSpecialty
EXEC ETL.BuildWHDiagnosis
EXEC ETL.BuildWHHRG
EXEC ETL.BuildWHOperation
EXEC ETL.BuildWHPractice
EXEC ETL.BuildDQError
EXEC ETL.BuildDQCensus;



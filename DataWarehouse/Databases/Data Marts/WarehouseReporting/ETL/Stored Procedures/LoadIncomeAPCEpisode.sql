﻿

create proc [ETL].LoadIncomeAPCEpisode

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()


insert into WarehouseReporting.[Income].[APCEpisode]
(
	[Period]
	,[Position]
	,[MonthNumber]
	,[PCTCode]
	,[TrustCode]
	,[SiteCode]
	,[SpecialtyCode]
	,[HRGCode]
	,[PatientClassificationCode]
	,[AdmissionMethodCode]
	,[LoS]
	,[DischargeDate]
	,[SpecialServiceCode1]
	,[SpecialServiceCode2]
	,[SpecialServiceCode3]
	,[GpPracticeCode]
	,[PatientCode]
	,[ConsultantCode]
	,[GpCode]
	,[DateOfBirth]
	,[ReportMasterID]
	,[UserField1]
	,[UserField2]
	,[UserField3]
	,[UserField4]
	,[UserField5]
	,[SpellAdmissionDate]
	,[GroupingMethodFlag]
	,[PODCode]
	,[SpecialistCommissioningCode]
)

select
	[Period]
	,[Position]
	,[MonthNumber]
	,[PCTCode]
	,[TrustCode]
	,[SiteCode]
	,[SpecialtyCode]
	,[HRGCode]
	,[PatientClassificationCode]
	,[AdmissionMethodCode]
	,[LoS]
	,[DischargeDate]
	,[SpecialServiceCode1]
	,[SpecialServiceCode2]
	,[SpecialServiceCode3]
	,[GpPracticeCode]
	,[PatientCode]
	,[ConsultantCode]
		,[GpCode]
	,[DateOfBirth]
	,[ReportMasterID]
	,[UserField1]
	,[UserField2]
	,[UserField3]
	,[UserField4]
	,[UserField5]
	,[AdmissionDate]
	,[GroupingMethodFlag]
	,[PODCode]
	,[SpecialistCommissioningCode]
from
	WarehouseReporting.[ETL].[TLoadIncomeAPCEpisode]
	

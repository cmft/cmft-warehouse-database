﻿
create proc ETL.LoadIncomeOPAppointment

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

insert into WarehouseReporting.[Income].[OPAppointment]

(
	SnapshotRecno
	,[Period]
	,[Position]
	,[Version]
	,[MonthNumber]
	,[PCTCode]
	,TrustCode
	,SiteCode
	,[SpecialtyCode]
	,HRGCode
	,[PODCode]
	,[GPPracticeCode]
	,GPCode
	,[PatientCode]
	,[ConsultantCode]
	,DateOfBirth
	,[ReportMasterID]
	,[UserField1]
	,[UserField2]
	,[UserField3]
	,[UserField4]
	,[UserField5]
	,Activity
	,SpecialistCommissioningCode
)

select 
	SnapshotRecno
	,[Period]
	,[Position]
	,[Version]
	,[MonthNumber]
	,[PCTCode]
	,TrustCode
	,SiteCode
	,[SpecialtyCode]
	,encounter.HRGCode
	,encounter.[PODCode]
	,[GPPracticeCode]
	,GPCode
	,[PatientCode]
	,[ConsultantCode]
	,DateOfBirth
	,[ReportMasterID]
	,[UserField1]
	,[UserField2]
	,[UserField3]
	,[UserField4]
	,[UserField5]
	,Activity
	,SpecialistCommissioningCode
from 
	ETL.TLoadIncomeOP encounter
	left outer join [Income].[ZeroPriceHRG] ophrg
	on ophrg.[HRGCode] = encounter.[HRGCode]
where
	encounter.HRGCode in 
					(
					'WF01A'
					,'WF01B'
					,'WF02A'
					,'WF02B'
					,'UZ01Z'
					)

 or ophrg.HRGCode is not null -- hrgs which attract no price so need to be factored as attendance
	
select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @sproc = name from sysobjects where id = @@procid

--exec WriteAuditLogEvent @sproc, @Stats, @StartTime
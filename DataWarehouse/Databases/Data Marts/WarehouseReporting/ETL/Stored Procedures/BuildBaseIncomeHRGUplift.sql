﻿

CREATE proc [ETL].[BuildBaseIncomeHRGUplift]

as

truncate table [ETL].[BaseIncomeHRGUplift]


insert into [ETL].[BaseIncomeHRGUplift]
	
select
	APCSnapshot.Period
	,APCSnapshot.Position
	,APCSnapshot.Version
	,APCSnapshot.SourceUniqueID
	--,APCSnapshot.SnapshotRecno
	,APCSpell.HRGCode
from
	Income.APCSnapshot APCSnapshot

inner join
	Income.APCSpell APCSpell
on	APCSnapshot.SnapshotRecno = APCSpell.SnapshotRecno
where
	APCSnapshot.Period = 'Month 8 2012/2013'
and
	APCSnapshot.Position = 'FLEX'
and
	APCSnapshot.Version = 1


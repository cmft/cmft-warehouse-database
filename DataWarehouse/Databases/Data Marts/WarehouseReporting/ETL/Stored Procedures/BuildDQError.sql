﻿


CREATE PROCEDURE [ETL].[BuildDQError]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseDQError

	INSERT INTO  ETL.BaseDQError

	SELECT 
		 LoadStartDate
		,LoadEndDate 
		,[Date] 
		,PrimaryDate 
		,PrimaryDateLabel
		,SecondaryDate
		,SecondaryDateLabel
		,PrimaryIdentifier 
		,PrimaryIdentifierLabel
		,SecondaryIdentifier 
		,SecondaryIdentifierLabel 
		,TypeCode 
		,InterfaceCode 
		,PointOfDelivery 
		,EncounterSourceUniqueID
		,SourcePatientNo
		,DirectorateCode
		,SiteCode
		,SpecialtyCode
		,NHSNumber
		,CreatedBy
		,Value	

	FROM 
		 Warehouse.dq.error

END




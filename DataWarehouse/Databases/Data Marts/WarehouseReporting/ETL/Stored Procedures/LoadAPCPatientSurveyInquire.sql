﻿

CREATE procedure [ETL].[LoadAPCPatientSurveyInquire] as


declare @sql varchar(8000)
declare @census varchar(8)

select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)

select
	@sql = 
'
truncate table APC.PatientSurvey

insert into	APC.PatientSurvey

select
	 CensusDate = convert(date , StatisticsDate)
	,NHSNumber
	,CasenoteNumber = CaseNoteNumber
	,WardCode = Ward
	,Ward = WardName
	,Forenames
	,Surname
	,IntendedProcedureCode = IntendedProc
	,OPCS.ProcedureType
from
	(
	select
		 StatisticsDate
		,NHSNumber
		,CaseNoteNumber
		,Ward
		,WardName
		,Forenames
		,Surname
		,IntendedProc
	from
		openquery(INQUIRE , ''
	select
		 MBS.StatisticsDate
		,Patient.NHSNumber
		,AdmitDisch.CaseNoteNumber
		,MBS.Ward
		,Ward.WardName
		,Patient.Forenames
		,Patient.Surname
		,WLEntry.IntendedProc
	from
		MIDNIGHTBEDSTATE MBS

	inner join PATDATA Patient
	on	Patient.InternalPatientNumber = MBS.InternalPatientNumber

	inner join WARD Ward
	on	Ward.WARDID = MBS.Ward

	inner join ADMITDISCH AdmitDisch
	on	AdmitDisch.InternalPatientNumber = MBS.InternalPatientNumber
	and	AdmitDisch.EpisodeNumber = MBS.EpisodeNumber

	inner join WLENTRY WLEntry
	on	WLEntry.InternalPatientNumber = MBS.InternalPatientNumber
	and	WLEntry.EpisodeNumber = MBS.EpisodeNumber

	where
		MBS.StatisticsDate = ' + @census + '
	'')
	) Survey

inner join 
	--Hip Replacement
	(
		  select ProcedureCode = ''W37'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W38'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W39'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W46'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W47'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W48'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W93'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W94'' , ProcedureType = ''Hip''
	union select ProcedureCode = ''W95'' , ProcedureType = ''Hip''

	--Knee Replacement
	union select ProcedureCode = ''W40'' , ProcedureType = ''Knee''
	union select ProcedureCode = ''W41'' , ProcedureType = ''Knee''
	union select ProcedureCode = ''W42'' , ProcedureType = ''Knee''

	--CABG 
	union select ProcedureCode = ''K40'' , ProcedureType = ''CABG''
	union select ProcedureCode = ''K41'' , ProcedureType = ''CABG''
	union select ProcedureCode = ''K42'' , ProcedureType = ''CABG''
	union select ProcedureCode = ''K43'' , ProcedureType = ''CABG''
	union select ProcedureCode = ''K44'' , ProcedureType = ''CABG''
	union select ProcedureCode = ''K45'' , ProcedureType = ''CABG''
	union select ProcedureCode = ''K46'' , ProcedureType = ''CABG''
	union select ProcedureCode = ''K47'' , ProcedureType = ''CABG''
	) OPCS
on	OPCS.ProcedureCode = left(Survey.IntendedProc , 3)
'

print (@sql)
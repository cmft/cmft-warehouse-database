﻿
CREATE procedure [ETL].[BuildAPCVTESpellTrafford] as


--initialise table
truncate table WarehouseReporting.APC.VTESpellTrafford 

insert into WarehouseReporting.APC.VTESpellTrafford 
(
	ProviderSpellNo
)

select distinct
	ProviderSpellNo
from
	TraffordWarehouse.dbo.APCEncounter
where
	AdmissionDate >= '1 Apr 2012' --Jan 2008 seems to be the earliest occurrence of VTE coding


--VTE Categorisation

--update WarehouseReporting.APC.VTESpellTrafford
--set
--	VTECategoryCode = 'C'
--from
--	WarehouseReporting.APC.VTESpellTrafford VTESpell

--inner join	TraffordWarehouse.dbo.APCEncounter
--on	Encounter.ProviderSpellNo = VTESpell.ProviderSpellNo

--where
--	left(Encounter.Research2 , 1) = 'V'
--and	charindex('C' , Encounter.Research2) > 1


--update WarehouseReporting.APC.VTESpellTrafford
--set
--	VTECategoryCode = 'I'
--from
--	WarehouseReporting.APC.VTESpellTrafford VTESpell

--inner join	TraffordWarehouse.dbo.APCEncounter
--on	Encounter.ProviderSpellNo = VTESpell.ProviderSpellNo
--and	VTESpell.VTECategoryCode is null

--where
--	left(Encounter.Research2 , 1) = 'V'
--and	charindex('I' , Encounter.Research2) > 1


--update WarehouseReporting.APC.VTESpellTrafford
--set
--	VTECategoryCode = 'M'
--from
--	WarehouseReporting.APC.VTESpellTrafford VTESpell

--inner join	TraffordWarehouse.dbo.APCEncounter
--on	Encounter.ProviderSpellNo = VTESpell.ProviderSpellNo
--and	VTESpell.VTECategoryCode is null

--where
--	left(Encounter.Research2 , 1) = 'V'
--and	charindex ('M' , Research2) > 1
--and charindex ('C' , Research2) = 0
--and charindex ('I' , Research2) = 0




--Build work tables for 'any position' seeks

create table #Diagnosis
	(
	 ProviderSpellNo varchar(20) not null
	,SourceUniqueID bigint not null
	,DiagnosisCode varchar(10) not null
	,SequenceNo int not null
	)

CREATE NONCLUSTERED INDEX IX_wkDiagnosis ON #Diagnosis 
	(
	 ProviderSpellNo asc
	,SourceUniqueID asc
	,SequenceNo asc
	)

create table #Operation
	(
	 ProviderSpellNo varchar(20) not null
	,SourceUniqueID bigint not null
	,OperationCode varchar(10) not null
	,SequenceNo int not null
	)

CREATE NONCLUSTERED INDEX IX_wkOperation ON #Operation 
	(
	 ProviderSpellNo asc
	,SourceUniqueID asc
	,SequenceNo asc
	)








-- Diagnosis
insert #Diagnosis
select
	 Diagnosis.ProviderSpellNo
	,Diagnosis.SourceUniqueID
	,Diagnosis.DiagnosisCode
	,Diagnosis.SequenceNo
from
	(
	select
		 Encounter.ProviderSpellNo
		,Encounter.SourceUniqueID
		,DiagnosisCode = Encounter.PrimaryDiagnosisCode
		,SequenceNo = 0
	from
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

	union

	select
		 e.ProviderSpellNo
		,e.SourceUniqueID
		,Diagnosis.DiagnosisCode
		,Diagnosis.SequenceNo
	from
		TraffordWarehouse.dbo.APCDiagnosis Diagnosis
	inner join [TraffordWarehouse].[dbo].[APCEncounter] e
		on substring(diagnosis.sourceuniqueid,1,7) = e.sourceuniqueid
	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = e.ProviderSpellNo
	) Diagnosis
where
	Diagnosis.DiagnosisCode is not null








-- Operation
insert #Operation
select
	 Operation.ProviderSpellNo
	,Operation.SourceUniqueID
	,Operation.OperationCode
	,Operation.SequenceNo
from
	(
	select
		 Encounter.ProviderSpellNo
		,Encounter.SourceUniqueID
		,OperationCode = Encounter.PrimaryProcedureCode 
		,SequenceNo = 0
	from
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

	union

	select
		 e.ProviderSpellNo
		,e.SourceUniqueID
		,Operation.ProcedureCode OperationCode 
		,Operation.SequenceNo
	from
		TraffordWarehouse.dbo.APCProcedure Operation
		
	inner join [TraffordWarehouse].[dbo].[APCEncounter] e
		on substring(Operation.sourceuniqueid,1,7) = e.sourceuniqueid
	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = e.ProviderSpellNo
	) Operation
where
	Operation.OperationCode is not null

-- finish building work tables




--Exclusion Categorisation

--1 Exclude on the basis of Primary Procedure
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join
	(
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartdate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join Warehouse.dbo.EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXPRIMPROCWITHOUTLACODE'
	and	Exclusion.EntityCode = Encounter.PrimaryProcedureCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo
and	Exclusion.SequenceNo = 1



-- 2 Exclude on basis of Diagnosis Codes found in any position
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join
	(  
	select
		 #Diagnosis.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Diagnosis.ProviderSpellNo order by #Diagnosis.SourceUniqueID , #Diagnosis.SequenceNo)
	FROM
		#Diagnosis

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = #Diagnosis.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join Warehouse.dbo.EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXANYDIAGNOSISCODE'
	and	Exclusion.EntityCode = #Diagnosis.DiagnosisCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1








-- 3 Exclude on basis of Procedure Codes found in any position
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join
	(  
	select
		 #Operation.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Operation.ProviderSpellNo order by #Operation.SourceUniqueID , #Operation.SequenceNo)
	FROM
		#Operation

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = #Operation.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join Warehouse.dbo.EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXANYPROCEDURECODE'
	and	Exclusion.EntityCode = #Operation.OperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1




-- 4 Exclude on basis of Procedure Codes found in any position, for Chemotherapy patients only

update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join
	(
	select
		 #Operation.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Operation.ProviderSpellNo order by #Operation.SourceUniqueID , #Operation.SequenceNo)
	FROM
		#Operation

	inner join 
		(
		select distinct
			#Diagnosis.ProviderSpellNo
		from
			#Diagnosis

		inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
		on	VTESpell.ProviderSpellNo = #Diagnosis.ProviderSpellNo
		and	VTESpell.VTEExclusionReasonCode is null

		where
			#Diagnosis.DiagnosisCode in ( 'Z51.1' , 'Z51.2' )
		) Diagnosis	
	on	Diagnosis.ProviderSpellNo = #Operation.ProviderSpellNo

	inner join Warehouse.dbo.EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXCHEMOANYPROCEDURECODE'
	and	Exclusion.EntityCode = #Operation.OperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



-- 5 Exclude Day Cases on the basis of Procedures in any position
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join
	(  
	select
		 #Operation.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Operation.ProviderSpellNo order by #Operation.SourceUniqueID , #Operation.SequenceNo)
	FROM
		#Operation

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = #Operation.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join TraffordWarehouse.dbo.APCEncounter Encounter
	on	Encounter.ProviderSpellNo = #Operation.ProviderSpellNo
	and	Encounter.SourceUniqueID = #Operation.SourceUniqueID
	and	Encounter.PatientClassificationCode = 2 -- DAY CASE

	inner join Warehouse.dbo.EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXDAYCASEANYPROCEDURECODE'
	and	Exclusion.EntityCode = #Operation.OperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



--6 Exclude spells on the basis of TOPS categorisation and admission ward equals Ward 63





-- Dental Day Case Exclusions 1st Test   
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(  
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'DN' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
	--	Encounter.StartSiteCode = 'DENT'
	--AND Encounter.EndSiteCode = 'DENT'
	encounter.specialtycode = '140'
	AND (
			Encounter.PatientClassificationCode = 2 -- DAY CASE
		 OR datediff(minute , Encounter.EpisodeStartDate , Encounter.EpisodeEndDate) < 721
		)
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo     
and	Exclusion.SequenceNo = 1







-- --Day Case Exclusions Already signed by consultant by procedure
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	( 

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'SIGNEDDC' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null
	
	inner join #Operation on VTESpell.ProviderSpellNo = #Operation.ProviderSpellNo

	WHERE
		--StartDirectorateCode = '50' AND
	 Encounter.PatientClassificationCode = 2 -- DAY CASE
	 AND #Operation.OperationCode IN 
	 ('A57.7',
'A61.1',
'A67.1',
'A69.9',
'A73.3',
'B08.3',
'B10.1',
'B28.3',
'D01.3',
'D04.1',
'D06.2',
'D07.3',
'D12.2',
'D12.8',
'D14.3',
'D16.2',
'D17.3',
'D20.2',
'D20.3',
'D28.2',
'E02.4',
'E03.2',
'E03.3',
'E03.6',
'E04.1',
'E04.8',
'E08.1',
'E08.2',
'E08.8',
'E09.8',
'E10.1',
'E13.3',
'E17.8',
'E20.1',
'F02.1',
'F09.3',
'F10.4',
'F10.9',
'F12.1',
'F18.1',
'F23.1',
'F34.6',
'F34.8',
'F34.9',
'F36.2',
'F36.3',
'F44.1',
'H01.1',
'H41.2',
'H48.1',
'H48.2',
'H48.3',
'H51.1',
'H55.1',
'H55.3',
'H55.4',
'H55.8',
'H58.2',
'H59.4',
'H59.8',
'H59.9',
'H60.3',
'J18.3',
'M47.2',
'M47.9',
'M53.6',
'M72.3',
'N11.3',
'N30.3',
'O27.2',
'O27.3',
'O27.4',
'O29.1',
'P05.4',
'P06.3',
'P11.1',
'P13.2',
'P20.1',
'P23.2',
'P23.4',
'P26.3',
'P31.3',
'Q01.4',
'Q03.3',
'Q03.8',
'Q07.4',
'Q10.9',
'Q11.1',
'Q11.3',
'Q12.3',
'Q12.4',
'Q16.1',
'Q23.1',
'Q23.3',
'Q35.2',
'Q43.2',
'Q49.1',
'Q49.2',
'Q49.3',
'Q55.1',
'Q55.2',
'Q55.8',
'Q55.9',
'S06.8',
'S08.2',
'S36.1',
'S40.4',
'S41.1',
'S44.2',
'S44.4',
'S45.4',
'S45.6',
'S47.2',
'S47.4',
'S57.1',
'S57.8',
'T20.2',
'T21.2',
'T22.2',
'T24.2',
'T27.2',
'T31.3',
'T42.2',
'T42.3',
'T43.9',
'T46.2',
'T52.1',
'T52.5',
'T52.6',
'T59.1',
'T59.2',
'T59.4',
'T64.5',
'T69.1',
'T72.3',
'T79.1',
'T87.3',
'T87.7',
'T96.2',
'T96.8',
'U05.1',
'U21.2',
'W08.2',
'W08.3',
'W09.2',
'W09.3',
'W15.3',
'W16.4',
'W19.6',
'W19.8',
'W20.1',
'W20.5',
'W24.8',
'W28.3',
'W33.8',
'W38.1',
'W57.2',
'W59.3',
'W59.5',
'W59.6',
'W71.2',
'W74.2',
'W75.2',
'W77.1',
'W78.1',
'W78.4',
'W78.5',
'W79.1',
'W79.2',
'W80.3',
'W82.2',
'W82.3',
'W83.6',
'W84.4',
'W84.5',
'W84.8',
'W85.2',
'W87.9',
'W89.1',
'W90.1',
'W90.3',
'W91.9',
'X38.2')
	 
	 
	 
	 
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
--and	Exclusion.SequenceNo = 1



-- --Day Case Exclusions Already signed by consultant by Diagnosis
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	( 

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'SIGNEDDC' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null
	
	inner join #Diagnosis on VTESpell.ProviderSpellNo = #Diagnosis.ProviderSpellNo

	WHERE
		--StartDirectorateCode = '50' AND
	 Encounter.PatientClassificationCode = 2 -- DAY CASE
	 AND #Diagnosis.DiagnosisCode IN 
	 ('C18.6',
'C44.2',
'D04.3',
'D17.1',
'D68.0',
'G56.0',
'G56.2',
'H91.9',
'J34.2',
'J38.4',
'K01.1',
'K02.9',
'K08.3',
'K40.9',
'K42.9',
'K52.9',
'K62.5',
'K80.2',
'K81.1',
'K91.4',
'K92.2',
'L72.0',
'L72.1',
'L73.2',
'L98.9',
'M18.9',
'M22.8',
'M23.2',
'M24.0',
'M25.5',
'M25.7',
'M54.5',
'M67.4',
'M72.0',
'M75.1',
'M75.4',
'M79.5',
'M79.8',
'N39.3',
'N70.1',
'N89.8',
'N92.0',
'N92.1',
'N93.9',
'N95.0',
'N97.9',
'O03.9',
'O04.1',
'O26.8',
'R10.4',
'R13.X',
'R32.X',
'R79.8',
'S00.0',
'S00.2',
'S01.0',
'S01.8',
'S62.6',
'S73.1',
'S82.6',
'T84.6',
'Z03.8',
'Z30.3',


'M54.56',
'M25.50'

)
	 
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
--and	Exclusion.SequenceNo = 1









-- ESTU Length of Stay Less Than 8 hours  Exclusions  
update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(   

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'ESTU' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartWardTypeCode IN ( 'ESTU' , 'MAU' , 'CLDU' ,'PIU' )
	and	datediff(minute , Encounter.EpisodeStartDate , Encounter.EpisodeEndDate) < 481   
	and	Encounter.EpisodeNo = 1
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(
	-- Clinical Haematology zero LOS Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'HAEMZERO' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null


	--inner join PAS.Specialty
	--on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

	WHERE 
		datediff(day , Encounter.AdmissionDate , Encounter.DischargeDate) = 0
	AND left(SpecialtyCode , 3)  IN ( '303' , '253' )
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(
	-- St Mary's GYN1 Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'GYN1' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		--StartDirectorateCode = '30' AND
		(
			(
				Encounter.SpecialtyCode = 'G502'
			and left(Encounter.PrimaryProcedureCode , 3 ) != 'Q48'
			)
	   or  (
				Encounter.SpecialtyCode = '502'
			and Encounter.PrimaryProcedureCode = 'Q13.1'
			)
	  )
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(

	-- Endoscopy room Exclusions     
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'ENDOR' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartWardTypeCode = 'ENDO'
	AND Encounter.EndWardTypeCode = 'ENDO'
	and	Encounter.EpisodeNo = 1
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	( 
	-- Ophthalmology Day Case Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'OPHTHDC' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	--inner join PAS.Specialty
	--on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

	WHERE
		--Encounter.StartDirectorateCode = '40' and
	 left(SpecialtyCode , 3) IN ( '130' , '216' )
	and Encounter.PatientClassificationCode = 2
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(
	-- Ophthalmology Laser Day Case Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'EDC' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		--Encounter.StartDirectorateCode = '40'
	Encounter.StartWardTypeCode IN ( 'EDCA' , 'EDCC' , 'EDCD' )
	and	Encounter.EpisodeNo = 1
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(
	-- Admission Method AN Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'AN' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null


	--inner join PAS.Specialty
	--on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

	WHERE
		Encounter.AdmissionMethodCode = 'AN' 
	AND LEFT(SpecialtyCode , 3) = '501'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1






-------------------------------------------------------------
--CHECK THESE!!!!!!!!!!!!!!!!!!!!
-- Should be included next month so can use Theatre data
-------------------------------------------------------------

--------------------update WarehouseReporting.APC.VTESpellTrafford
--------------------set
--------------------	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
--------------------from
--------------------	WarehouseReporting.APC.VTESpellTrafford VTESpell
--------------------inner join	
--------------------	(
--------------------	-- Check Anaesthetic in Theatre data
--------------------	-- CCB 2012-01-10 update Theatre/APC matching to include casenote and name matching
--------------------	select
--------------------		 Encounter.ProviderSpellNo
--------------------		,VTEExclusionReasonCode =
--------------------			case
--------------------			when 
--------------------				datediff(
--------------------					 minute
--------------------					,coalesce(AnaestheticInductionTime , InAnaestheticTime)
--------------------					,InRecoveryTime
--------------------				) < 90 then '90'
--------------------			when Anaesthetic.AnaestheticCode1 = 'LA' and Encounter.PatientClassificationCode = 2 then 'LA'

--------------------			--when ExclusionProcedure.LA = 'SE' and Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' ) then ExclusionProcedure.Type
--------------------			when Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' ) then Exclusion.XrefEntityCode

--------------------			end
--------------------		,SequenceNo =
--------------------			row_number() over (partition by Encounter.ProviderSpellNo order by ProcedureDetail.ProcedureStartTime)
--------------------	from
--------------------		TraffordWarehouse.dbo.APCEncounter Encounter

--------------------	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
--------------------	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
--------------------	and	VTESpell.VTEExclusionReasonCode is null

--------------------	inner join TraffordWarehouse.dbo.APCEncounterProcedureDetail
--------------------	on	EncounterProcedureDetail.EncounterRecno = Encounter.EncounterRecno

--------------------	inner join Theatre.ProcedureDetail
--------------------	on	ProcedureDetail.SourceUniqueID = EncounterProcedureDetail.ProcedureDetailSourceUniqueID

--------------------	inner join Theatre.OperationDetail
--------------------	on	OperationDetail.SourceUniqueID = ProcedureDetail.OperationDetailSourceUniqueID

--------------------	inner join Theatre.PatientBooking
--------------------	on	PatientBooking.SourceUniqueID = OperationDetail.PatientBookingSourceUniqueID

--------------------	left join Theatre.Anaesthetic
--------------------	on	Anaesthetic.AnaestheticCode = PatientBooking.AnaestheticCode

--------------------	--inner join PAS.Specialty
--------------------	--on	Encounter.SpecialtyCode = Specialty.SpecialtyCode

--------------------	--LEFT JOIN SandboxPeterHoyle.dbo.tblExcPrimProc ExclusionProcedure
--------------------	--ON ExclusionProcedure.OPCS = Encounter.PrimaryProcedureCode

--------------------	inner join Warehouse.dbo.EntityXref Exclusion
--------------------	on	Exclusion.EntityTypeCode = 'VTEXPRIMPROCWITHSECODE'
--------------------	and	Exclusion.EntityCode = Encounter.PrimaryProcedureCode
--------------------	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'


--------------------	where
--------------------		(
--------------------			Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' )
--------------------		and	SpecialtyCode in ( '160' , '140' , '120' )
--------------------		and	left(Encounter.PrimaryProcedureCode , 1) not in ('C' , 'D')
--------------------		)
--------------------	or
--------------------		(
--------------------			Anaesthetic.AnaestheticCode1 = 'LA'
--------------------		and	(
--------------------				SpecialtyCode not in ( '160' , '140' , '120' )
--------------------			or	Encounter.PatientClassificationCode = 2
--------------------			)
--------------------		)
--------------------	) Exclusion
--------------------on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
--------------------and	Exclusion.SequenceNo = 1

-------------------------------------------------------------
--CHECK THESE!!!!!!!!!!!!!!!!!!!!
-------------------------------------------------------------
----------------------------------------------------------------------------


--Process the REH hospital exclusions in the following pieces of code

update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(
	--Process the REH Withington Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'WH' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartWardTypeCode = 'WCH' 
	and	Encounter.EpisodeNo = 1
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



----now done in the earlier generic procedure step

--left join
--	(
--	--Process The Opthalmology PRIMARY Procedure Exclusions 

--	select
--		 Encounter.ProviderSpellNo
--		,VTEExclusionReasonCode = ExclusionsLookup.ReturnValue 
--		,SequenceNo =
--			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
--	FROM
--		TraffordWarehouse.dbo.APCEncounter
		
--	INNER JOIN SandboxPeterHoyle.dbo.ExclusionsLookup
--	ON ExclusionsLookup.Code = Encounter.PrimaryProcedureCode
	     
--	WHERE
--		ExclusionsLookup.DiagProc = 'EYE'
--	) Exclusion15
--on	Exclusion15.ProviderSpellNo = Encounter.ProviderSpellNo
--and	Exclusion15.SequenceNo = 1


---- is this now redundant?

--update WarehouseReporting.APC.VTESpellTrafford
--set
--	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
--from
--	WarehouseReporting.APC.VTESpellTrafford VTESpell
--inner join	
--	(
--	--Process The Opthalmology ANY Procedure Exclusions 

--	select
--		 Encounter.ProviderSpellNo
--		,VTEExclusionReasonCode = ExclusionsLookup.ReturnValue 
--		,SequenceNo =
--			row_number() over (partition by Encounter.ProviderSpellNo order by Operation.SourceUniqueID , Operation.SequenceNo)
--	FROM
--		TraffordWarehouse.dbo.APCEncounter

--	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
--	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
--	and	VTESpell.VTEExclusionReasonCode is null
		
--	INNER JOIN APC.Operation
--	ON	Operation.APCSourceUniqueID = Encounter.SourceUniqueID

--	INNER JOIN SandboxPeterHoyle.dbo.ExclusionsLookup
--	ON	ExclusionsLookup.Code = Operation.OperationCode
	     
--	WHERE
--		ExclusionsLookup.DiagProc = 'EYE'
--	AND Operation.OperationCode  IN ( 'C64.7' , 'X85.1' )
--	) Exclusion
--on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
--and	Exclusion.SequenceNo = 1


update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(
	--Process The Opthalmology DOUBLE Procedure Exclusions 
	 
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 
			case
			when Encounter.PrimaryProcedureCode = 'C75.1' and Operation.ProcedureCode = 'Y71.2'	then 'SECI'
			when Encounter.PrimaryProcedureCode = 'C79.4' and Operation.ProcedureCode = 'X93.1' then 'LUCI'
			when Encounter.PrimaryProcedureCode = 'C22.6' and Operation.ProcedureCode = 'S60.6' then 'ELEC'
			when Encounter.PrimaryProcedureCode = 'C22.8' and Operation.ProcedureCode = 'S50.3' then 'IGW'
			when Encounter.PrimaryProcedureCode = 'C10.8' and Operation.ProcedureCode = 'S43.3' then 'REMSUT'
			when Encounter.PrimaryProcedureCode = 'C22.8' and Operation.ProcedureCode = 'S43.3' then 'REMSUT'
			when Encounter.PrimaryProcedureCode = 'C12.5' and Operation.ProcedureCode = 'S10.3' then 'DSX'
			when Encounter.PrimaryProcedureCode = 'L67.1' and Operation.ProcedureCode = 'O12.1' then 'TEMPAB'
			when Encounter.PrimaryProcedureCode = 'C75.1' and Operation.ProcedureCode = 'C71.2' then 'PHACOI'
			when Encounter.PrimaryProcedureCode = 'C27.3' and Operation.ProcedureCode = 'C27.5' then 'SYRP'
			end
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Operation.SourceUniqueID , Operation.SequenceNo)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	INNER JOIN TraffordWarehouse.dbo.APCProcedure Operation
	ON	Operation.SourceUniqueID = Encounter.SourceUniqueID
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1
and	Exclusion.VTEExclusionReasonCode is not null



update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	(
	--Process The Opthalmology TRIPLE Procedure Exclusions

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'PHACOS' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartDate)
	FROM
		TraffordWarehouse.dbo.APCEncounter Encounter

	inner join WarehouseReporting.APC.VTESpellTrafford VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.PrimaryProcedureCode    = 'C75.1'
	---------------------------------------------
	--  CHECK
	----------------------------------------------	
	--AND Encounter.SecondaryProcedureCode1 = 'C71.2'
	--AND Encounter.SecondaryProcedureCode1 = 'C61.8'
	--AND Encounter.SecondaryProcedureCode1 = 'Y14.3'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1





----------------------------------
-- GARETH AMBU EXCLUSIONS


update WarehouseReporting.APC.VTESpellTrafford
set
	VTEExclusionReasonCode = 'AMBU'
from
	WarehouseReporting.APC.VTESpellTrafford VTESpell
inner join	
	( 

SELECT 
      Spell.[HISId_Number], PrimaryDiagnosis.[Name] ,StartWardRoom.Name nam2
            ,SpellNote.Text , DischargeNote.Text text3, ProviderSpellNo

  FROM 
      Replica_UltraGendaBedman.dbo.Admission Spell


-- link admission episode
left join 
      (
      select
            *
      from
            Replica_UltraGendaBedman.dbo.BedOccupancy FirstBedOccupancy
      where
            not exists
            (
            select
                  1
            from
                  Replica_UltraGendaBedman.dbo.BedOccupancy BedOccupancy
            where
                  BedOccupancy.AdmissionId = FirstBedOccupancy.AdmissionId
            and   BedOccupancy.StartDate < FirstBedOccupancy.StartDate
            )
      ) AdmissionEpisode
on    AdmissionEpisode.AdmissionId = Spell.Id

-- link discharge episode
left join 
      (
      select
            *
      from
            Replica_UltraGendaBedman.dbo.BedOccupancy LastBedOccupancy
      where
            not exists
            (
            select
                  1
            from
                  Replica_UltraGendaBedman.dbo.BedOccupancy BedOccupancy
            where
                  BedOccupancy.AdmissionId = LastBedOccupancy.AdmissionId
            and   BedOccupancy.EndDate > LastBedOccupancy.EndDate
            )
      ) DischargeEpisode
on    DischargeEpisode.AdmissionId = Spell.Id



left join Replica_UltraGendaBedman.dbo.Bed StartWardBed
on    StartWardBed.Id = AdmissionEpisode.BedId

left join Replica_UltraGendaBedman.dbo.Room StartWardRoom
on    StartWardRoom.Id = StartWardBed.RoomId

left join Replica_UltraGendaBedman.dbo.Ward StartWard
on    StartWard.Id = StartWardRoom.WardId

left join Replica_UltraGendaBedman.dbo.Bed EndWardBed
on    EndWardBed.Id = DischargeEpisode.BedId

left join Replica_UltraGendaBedman.dbo.Room EndWardRoom
on    EndWardRoom.Id = EndWardBed.RoomId

left join Replica_UltraGendaBedman.dbo.Ward EndWard
on    EndWard.Id = EndWardRoom.WardId


left join Replica_UltraGendaBedman.dbo.Note SpellNote
on    SpellNote.Id = Spell.NoteId

left join Replica_UltraGendaBedman.dbo.Note DischargeNote
on DischargeNote.id = spell.DischargeNoteId 

left join Replica_UltraGendaBedman.dbo.AdmissionType PrimaryDiagnosis
on    PrimaryDiagnosis.Id = AdmissionEpisode.AdmissionTypeId

     LEFT JOIN TraffordWarehouse.dbo.apcEncounter APC
      ON
            Spell.HisID_Number = APC.ProviderSpellNo


where


Spell.[IsCurrent] = 1
      and
            (
      PrimaryDiagnosis.[Name] like '%dvt%'
      or
      StartWardRoom.Abbreviation = 'Ambu Care'
            )
 


	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo 

where VTEExclusionReasonCode is null



--------------------------------








--delete uncategorised spells
delete
from
	WarehouseReporting.APC.VTESpellTrafford
where
	VTECategoryCode is null
and	VTEExclusionReasonCode is null



--drop work tables

drop table #Diagnosis
drop table #Operation






--truncate table WarehouseReporting.APC.VTESpellTrafford VTESpell

--insert into 
--WarehouseReporting.APC.VTESpellTrafford VTESpell
--select * from [WAREHOUSE2\DEV].WarehouseReporting.APC.VTESpellTrafford VTESpell
﻿CREATE PROCEDURE [ETL].[BuildPASSpecialty]

AS

BEGIN
	TRUNCATE TABLE  ETL.BasePASSpecialty

	INSERT INTO  ETL.BasePASSpecialty

	SELECT 
		   SpecialtyCode
		  ,Specialty
		  ,NationalSpecialtyCode
		  ,NationalSpecialty
		  ,TreatmentFunctionFlag

	FROM WarehouseOLAP.dbo.OLAPPASSpecialty

END

﻿CREATE PROCEDURE [ETL].[BuildPASSite]

AS

BEGIN
	TRUNCATE TABLE  ETL.BasePASSite

	INSERT INTO  ETL.BasePASSite


SELECT 
	   OlapPASSite.[SiteCode]
      ,OlapPASSite.[Site]
      ,OlapPASSite.[Colour]
      ,OlapPASSite.[Sequence]
      ,OlapPASSite.[MappedSiteCode]
      ,OlapSite.site AS MappedSite

FROM 
	warehouseolap.[dbo].[OlapPASSite]
	LEFT OUTER JOIN warehouseolap.[dbo].[OlapSite]
	ON [OlapPASSite].MappedSiteCode = OlapSite.Sitecode


END

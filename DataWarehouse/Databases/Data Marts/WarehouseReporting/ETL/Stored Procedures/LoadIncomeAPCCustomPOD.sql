﻿
create proc ETL.LoadIncomeAPCCustomPOD

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

insert into WarehouseReporting.[Income].[CustomPOD]

	(
	SnapshotRecno
	,Period
	,Position
	,[Version]
	,MonthNumber
	,[PCTCode] 
	,TrustCode
	,SiteCode
	,[SpecialtyCode]
	,[HRGCode]
	,PODCode
	,Activity
    ,[SpecialServiceCode1]
    ,[SpecialServiceCode2]
    ,[SpecialServiceCode3]
    ,[SpecialServiceCode4]
    ,[SpecialServiceCode5]
    ,[SpecialServiceCode6]
    ,[SpecialServiceCode7]
    ,[SpecialServiceCode8]
    ,[SpecialServiceCode9]
    ,[SpecialServiceCode10]
    ,[SpecialServiceCode11]
    ,[SpecialServiceCode12]
    ,[SpecialServiceCode13]
    ,[SpecialServiceCode14]
    ,[SpecialServiceCode15]
    ,[SpecialServiceCode16]
    ,[SpecialServiceCode17]
    ,[SpecialServiceCode18]
    ,[SpecialServiceCode19]
    ,[SpecialServiceCode20]
    ,[SpecialServiceCode21]
	,[GpPracticeCode]
	,[PatientCode]
	,[DateOfBirth]
	,[ConsultantCode]
	,GPCode
	,LoS
	,ReportMasterID
	,[UserField1]
	,[UserField2]
	,[UserField3]
	,[UserField4]
	,[UserField5]
	,AdmissionDate
	,DischargeDate
	,PatientClassificationCode
	,GroupingMethodFlag
	,SpecialistCommissioningCode
	)

select
	SnapshotRecno
	,Period
	,Position
	,[Version]
	,MonthNumber
	,[PCTCode] 
	,TrustCode
	,SiteCode
	,[SpecialtyCode]
	,[HRGCode]
	,PODCode
	,Activity = 1
    ,[SpecialServiceCode1] = null
    ,[SpecialServiceCode2] = null
    ,[SpecialServiceCode3] = null
    ,[SpecialServiceCode4] = null
    ,[SpecialServiceCode5] = null
    ,[SpecialServiceCode6] = null
    ,[SpecialServiceCode7] = null
    ,[SpecialServiceCode8] = null
    ,[SpecialServiceCode9] = null
    ,[SpecialServiceCode10] = null
    ,[SpecialServiceCode11] = null
    ,[SpecialServiceCode12] = null
    ,[SpecialServiceCode13] = null
    ,[SpecialServiceCode14] = null
    ,[SpecialServiceCode15] = null
    ,[SpecialServiceCode16] = null
    ,[SpecialServiceCode17] = null
    ,[SpecialServiceCode18] = null
    ,[SpecialServiceCode19] = null
    ,[SpecialServiceCode20] = null
    ,[SpecialServiceCode21] = null
	,[GpPracticeCode]
	,[PatientCode]
	,[DateOfBirth]
	,[ConsultantCode]
	,GPCode
	,LoS
	,ReportMasterID
	,[UserField1]
	,[UserField2]
	,[UserField3]
	,[UserField4]
	,[UserField5]
	,AdmissionDate
	,DischargeDate
	,PatientClassificationCode
	,GroupingMethodFlag
	,SpecialistCommissioningCode
from
	WarehouseReporting.ETL.TLoadIncomeAPCSpell
where
	PODCode is not null


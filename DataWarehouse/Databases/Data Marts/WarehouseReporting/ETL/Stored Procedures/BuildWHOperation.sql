﻿CREATE PROCEDURE [ETL].[BuildWHOperation]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseWHOperation

	INSERT INTO  ETL.BaseWHOperation

	SELECT 
		   OperationCode
		  ,Operation
		  ,OperationGroup
		  ,OperationChapter

	FROM WarehouseOLAP.dbo.OlapOperation

END

﻿


CREATE proc [ETL].[AssignIncomeOpportunityFlag]

as 


/* Identify opportunities */

/* Reset */

update
	Income.HRGUplift
set
	[OpportunityFlag] = null


/* Flag */

update
	Income.HRGUplift
set
	[OpportunityFlag] = 

						(
						select top 1
							FlagValue
						from 
							Income.HRGGroupToSplit GroupToSplit
						where
							GroupToSplit.HRGRoot = left(HRGUplift.[HRGCode], 4)
						and
							exists
								(
									select
										1
									from
										Income.HRGGroupToSplit GroupToSplitCC
									where
										HRGValue = HRGUplift.[HRGCode]
									and
										GroupToSplit.SequenceNo > GroupToSplitCC.SequenceNo
									and
										GroupToSplit.FlagValue like '%cc%'
																		
									and
										GroupToSplit.FlagValue not like 
																		case
																			when HRGUplift.PatientAge > 19
																			then '%p_cc%'
																			else ''
																		end
								)
								order by SequenceNo
						)

from
	[Income].[HRGUplift] HRGUplift
--where 
--	exists
--			(
--				select
--					1
--				from 
--					dbo.GroupToSplitPivot GroupToSplitPivot
--				where
--					GroupToSplitPivot.HRGRoot = left(HRGUplift.[HRGCode], 4)
--				and
--					exists
--						(
--							select
--								1
--							from
--								[dbo].[GroupToSplitPivot] GroupToSplitPivotCC
--							where
--								HRGValue = HRGUplift.[HRGCode]
--							and
--								GroupToSplitPivot.SequenceNo > GroupToSplitPivotCC.SequenceNo
--							and
--								GroupToSplitPivot.FlagValue like 
--																case
--																	when HRGUplift.PatientAge < 19
--																	then '%p_cc%'
--																	else '%cc%'
--																end
--							and
--								GroupToSplitPivot.FlagValue not like 
--																case
--																	when HRGUplift.PatientAge > 19
--																	then '%p_cc%'
--																	else ''
--																end
--						)
--				)


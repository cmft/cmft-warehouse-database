﻿
create proc ETL.LoadIncomeAPCSpecialistService

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

insert into WarehouseReporting.[Income].[APCHRGSpecialistService]
(
[Period]
,[Position]
,[SUSIdentifier]
,[SpecialistServiceCode]
)

select
	[Period]
	,[Position]
	,[SUSIdentifier]
	,[SpecialistServiceCode]
from 
	ETL.TLoadIncomeAPCHRGSpecialistService
	

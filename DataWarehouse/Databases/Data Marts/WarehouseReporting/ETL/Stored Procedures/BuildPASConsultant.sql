﻿
CREATE PROCEDURE [ETL].[BuildPASConsultant]

AS

BEGIN
	TRUNCATE TABLE  ETL.BasePASConsultant

	INSERT INTO  ETL.BasePASConsultant

	SELECT 
		   ConsultantCode
		  ,Consultant
		  ,NationalConsultantCode
		  ,SpecialtyCode
		  ,Specialty
		  ,DomainLogin

	FROM 
		   WarehouseOLAP.dbo.OlapPASConsultant

END


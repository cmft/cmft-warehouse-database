﻿CREATE PROCEDURE [ETL].[BuildPASAdmissionMethod]

AS

BEGIN
	TRUNCATE TABLE  ETL.BasePASAdmissionMethod

	INSERT INTO  ETL.BasePASAdmissionMethod

	SELECT 
		   AdmissionMethodCode
		  ,AdmissionMethod
		  ,NationalAdmissionMethodCode
		  ,NationalAdmissionMethod
		  ,AdmissionMethodTypeCode
		  ,AdmissionMethodType

	FROM WarehouseOLAP.dbo.OlapPASAdmissionMethod

END

﻿

CREATE PROCEDURE [ETL].[BuildServiceDeskCensus]

AS

set transaction isolation level read uncommitted;
set dateformat dmy;

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

declare @Calendar TABLE (TheDate date not null primary key);
declare @FromDate date, @ToDate date
--insert @Calendar SELECT TheDate FROM WH.Calendar WITH (NOLOCK);
	select @FromDate = MIN(StartTime)
	from ETL.BaseServiceDeskEncounter;
	
	select @ToDate = CAST(getdate() as date)
	
declare @NumberOfDayIntervals int = datediff(d,@FromDate,@ToDate);

insert @Calendar(TheDate)
select dateadd(d,i.TheInteger,@FromDate) TheDate
FROM WarehouseOLAP.dbo.IntegerBase i
WHERE i.TheInteger <= @NumberOfDayIntervals;


TRUNCATE TABLE ETL.BaseServiceDeskCensus

-- Insert new rows into destination table
INSERT INTO ETL.BaseServiceDeskCensus WITH (TABLOCK)
(
	 CensusDate
	,EncounterRecNo
	,SourceUniqueID
	,Request
	,StartTime
	,DeadlineTime
	,ResolvedTime
	,EndTime
	,RequestStatus
	,RequestStatusGroup
	,ResolvedFlag
	,BreachStatus
	,PredictedBreachStatus
	,ResolvedDurationMinutes
	,RequestDurationMinutes
	,TargetDurationMinutes
	,BreachValue
	,Category
	,Workgroup
	,CreatedBy
	,AssignedTo
	,CreatedTime
	,Priority
	,Template
	,Classification
	,ClosureDescription
	,AssignmentCount
	,CallerFullName
	,CallerEmail
	,CallerOrganization
	,SolutionNotes
	,History
	,Impact
	,PriorityCode
)
SELECT 
	 CensusDate = Calendar.TheDate
	,EncounterRecNo
	,SourceUniqueID
	,Request
	,StartTime
	,DeadlineTime
	,ResolvedTime
	,EndTime
	,RequestStatus
	,RequestStatusGroup
	,ResolvedFlag
	,BreachStatus
	,PredictedBreachStatus
	,ResolvedDurationMinutes
	,RequestDurationMinutes
	,TargetDurationMinutes
	,BreachValue
	,Category
	,Workgroup
	,CreatedBy
	,AssignedTo
	,CreatedTime
	,Priority
	,Template
	,Classification
	,ClosureDescription
	,AssignmentCount
	,CallerFullName
	,CallerEmail
	,CallerOrganization
	,SolutionNotes
	,History
	,Impact
	,PriorityCode
FROM 
	ETL.BaseServiceDeskEncounter Encounter WITH (NOLOCK)
	
inner join @Calendar Calendar 
on	(
		(
			Calendar.TheDate >= CONVERT(Date,Encounter.StartTime)
		and	Calendar.TheDate < CONVERT(Date,Encounter.EndTime)
		)
	or
		(
			Calendar.TheDate >= CONVERT(Date,Encounter.StartTime)
		and	Encounter.EndTime is null
		)
	)


select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec warehouse.dbo.WriteAuditLogEvent 'BuildServiceDeskCensus', @Stats, @StartTime

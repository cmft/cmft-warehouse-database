﻿
create proc ETL.LoadIncomeAPCBedday

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

insert into [Income].[APCBedday]
(
[Period]
,[Position]
,[Version]
,[MonthNumber]
,[PCTCode]
,[TrustCode]
,[SiteCode]
,[SpecialtyCode]
,[HRGCode]
,[PODCode]
,[Activity]
,CensusDate
,[GpPracticeCode]
,[PatientCode]
,[DateOfBirth]
,[ConsultantCode]
,[GpCode]
,[LoS]
,[ReportMasterID]
,[Userfield1]
,[UserField2]
,[UserField3]
,[UserField4]
,[UserField5]
,[AdmissionTime]
,[DischargeTime]
,[PatientClassificationCode]
,[AdmissionMethodCode]
)

select
	[Period]
	,[Position]
	,[Version]
	,[MonthNumber]
	,[PCTCode]
	,[TrustCode]
	,[SiteCode]
	,[SpecialtyCode]
	,[HRGCode]
	,[PODCode]
	,[Activity]
	,CensusDate
	,[GpPracticeCode]
	,[PatientCode]
	,[DateOfBirth]
	,[ConsultantCode]
	,[GpCode]
	,[LoS]
	,[ReportMasterID]
	,[Userfield1]
	,[UserField2]
	,[UserField3]
	,[UserField4]
	,[UserField5]
	,[AdmissionTime]
	,[DischargeTime]
	,[PatientClassificationCode]
	,[AdmissionMethodCode]
from
	[ETL].[TLoadIncomeAPCBedday]

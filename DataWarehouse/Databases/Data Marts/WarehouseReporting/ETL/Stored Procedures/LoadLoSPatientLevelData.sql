﻿

CREATE proc [ETL].[LoadLoSPatientLevelData]

as

declare @censusdate datetime = dateadd(day, -2, dateadd(dd, datediff(dd, 0, getdate()), 0)) -- run every Saturday - based on how PAS looked day previous Thursday

--declare @censusdate datetime = '10 jan 2013'

/* Copy of dbo.OlapCalendar to reduce contention and blocking */

declare @OlapCalendar table 
(
  TheDate date not null primary key
);

insert into @OlapCalendar
(
	TheDate
)

select 
	TheDate
from 
	WarehouseOLAP.dbo.OlapCalendar with (nolock);
	

insert into ETL.LoSPatientLevelData


select 
	CensusDate = @censusdate
	,LoSGroup = 
				case
					when datediff(day, AdmissionDate, @censusdate) between 5 and 9
					then 'Between 5 and 9'
					when datediff(day, AdmissionDate, @censusdate) between 10 and 14
					then 'Between 10 and 14'
					when datediff(day, AdmissionDate, @censusdate) >= 15
					then 'Fifteen and over'
				end
	,division.DivisionCode	
	,Division = 
				case
					when division.Division = 'Clinical & Scientific'
					then 'Clinical and Scientific'
					else division.Division
				end
	,AdmissionMethodType = admissionmethod.AdmissionMethodType
	,WardCode = wardstay.WardCode
	,Ward = wardstay.Ward
	,LoS = datediff(day, encounter.AdmissionDate, @censusdate)		
	,SpecialtyCode = specialty.NationalSpecialtyCode
	,Specialty = coalesce(nationalspecialty.Specialty, treatmentfunction.Specialty) -- gets specialty label from treatment function if no match against specialty code
	,AdmissionDate = encounter.AdmissionDate
	,WardStartDate = wardstay.StartTime
	,CasenoteNumber = coalesce(encounter.CasenoteNumber, '')
	,encounter.PatientForename
	,encounter.PatientSurname
	,PatientAge = floor(datediff(d, encounter.DateOfBirth, @censusdate)/365.25)
	,Consultant
	,encounter.ManagementIntentionCode
	,encounter.ExpectedLOS
from
	APC.Encounter encounter

inner join @OlapCalendar OlapCalendar
on	(
		(
			OlapCalendar.TheDate >= encounter.AdmissionDate
		and	OlapCalendar.TheDate < encounter.DischargeDate
		)
	or
		(
			OlapCalendar.TheDate >= encounter.AdmissionDate
		and	encounter.DischargeDate is null
		)
	)
	
and	OlapCalendar.TheDate = @censusdate

left outer join PAS.AdmissionMethod admissionmethod
on admissionmethod.AdmissionMethodCode = encounter.AdmissionMethodCode

/* get latest ward */

outer apply (
			select top 1
				wardstay.WardCode
				,ward.Ward
				,wardstay.StartTime
			from
				Warehouse.APC.WardStay wardstay
				inner join.PAS.Ward ward
				on wardstay.WardCode = ward.WardCode
			where 
				wardstay.ProviderSpellNo = encounter.ProviderSpellNo
			and wardstay.StartTime <= @censusdate	
			order by 
				wardstay.StartTime desc
             ) wardstay
             						
left outer join PAS.Specialty specialty
on specialty.SpecialtyCode = encounter.SpecialtyCode

left outer join Warehouse.WH.Specialty nationalspecialty
on specialty.NationalSpecialtyCode = nationalspecialty.SpecialtyCode

left outer join WH.TreatmentFunction treatmentfunction
on specialty.NationalSpecialtyCode = treatmentfunction.SpecialtyCode

left outer join PAS.Consultant consultant
on encounter.ConsultantCode = consultant.ConsultantCode

left outer join PAS.Directorate directorate
on coalesce(encounter.StartDirectorateCode, '9') = directorate.DirectorateCode

left outer join Warehouse.WH.Division division
on directorate.Division = division.Division

where
	datediff(day, AdmissionDate, @censusdate) >= 5
	and encounter.AdmissionDate >= '01/04/2008' 
	and coalesce(LastEpisodeInSpellIndicator, '') <> 'N'



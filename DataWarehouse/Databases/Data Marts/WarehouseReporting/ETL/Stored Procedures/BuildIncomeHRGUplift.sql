﻿
CREATE proc ETL.BuildIncomeHRGUplift

as

truncate table [Income].[HRGUplift]

insert into [Income].[HRGUplift]

select
	Period
	,Position
	,Version
	,EncounterRecno
	,Encounter.[SourceUniqueID]
	,CasenoteNumber
	,Encounter.AdmissionTime
	,PatientAge = floor(datediff(day, DateOfBirth, AdmissionDate)/365.25)
	,EndDirectorateCode
	,SpecialtyCode
	,ConsultantCode
	,BaseIncomeHRGUplift.[HRGCode]
	,PreviousDiagnosisCode = PreviousDiagnosisSpell.DiagnosisCode -- diagnosis code identified as long term and coded previously but not in FCE
	,PreviousAdmissionTime = PreviousDiagnosisSpell.AdmissionTime -- admission time associated with diagnosis code identified as long term and coded previously but not in FCE
	,[OpportunityFlag] = null

from
	APC.Encounter Encounter

inner join
			(
			select
				Diagnosis.DiagnosisCode
				,Encounter.SourcePatientNo
				,AdmissionTime = max(Encounter.AdmissionTime)
			from
				[APC].[Diagnosis] Diagnosis

			inner join 
				APC.Encounter Encounter
			on	Diagnosis.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

			inner join 
				WarehouseReporting.WH.Diagnosis LongTermCondition
			on	LongTermCondition.[DiagnosisCode] = Diagnosis.[DiagnosisCode]
			and	LongTermCondition.IsLongTermFlag = 1

			where
				SequenceNo > 0

			group by
				Diagnosis.DiagnosisCode
				,Encounter.SourcePatientNo
						
			) PreviousDiagnosisSpell

			on PreviousDiagnosisSpell.SourcePatientNo = Encounter.SourcePatientNo
			and PreviousDiagnosisSpell.AdmissionTime < Encounter.AdmissionTime

inner join
	[ETL].[BaseIncomeHRGUplift] BaseIncomeHRGUplift
on	Encounter.[SourceUniqueID] = BaseIncomeHRGUplift.[SourceUniqueID]


where
	PreviousDiagnosisSpell.DiagnosisCode not in
										(
										select
											Diagnosis.DiagnosisCode
										from
											[APC].[Diagnosis] Diagnosis
										inner join 
											APC.Encounter CurrentDiagnosisSpell
										on	Diagnosis.[ProviderSpellNo] = CurrentDiagnosisSpell.[ProviderSpellNo]

										where 
											CurrentDiagnosisSpell.SourcePatientNo = Encounter.SourcePatientNo
										and CurrentDiagnosisSpell.AdmissionTime = Encounter.AdmissionTime

										union all

										select
											CurrentDiagnosisSpell.PrimaryDiagnosisCode
										from
											APC.Encounter CurrentDiagnosisSpell
										where 
											CurrentDiagnosisSpell.SourcePatientNo = Encounter.SourcePatientNo
										and CurrentDiagnosisSpell.AdmissionTime = Encounter.AdmissionTime

										)

exec ETL.AssignIncomeOpportunityFlag


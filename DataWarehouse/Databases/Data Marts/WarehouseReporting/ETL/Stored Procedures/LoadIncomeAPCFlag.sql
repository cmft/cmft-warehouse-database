﻿
create proc ETL.LoadIncomeAPCFlag

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

insert into WarehouseReporting.[Income].APCHRGFlag
  (

[SnapshotRecno]
,[Period]
,[Position]
,[Version]
,[SUSIdentifier]
,[SequenceNo]
,[SpellFlag]
)

select
	[SnapshotRecno]
	,[Period]
	,[Position]
	,[Version]
	,[SUSIdentifier]
	,[SequenceNo]
	,[SpellFlag]
from 
	ETL.TLoadIncomeAPCHRGFlag
	

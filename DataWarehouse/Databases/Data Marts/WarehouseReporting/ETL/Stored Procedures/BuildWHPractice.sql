﻿CREATE PROCEDURE [ETL].[BuildWHPractice]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseWHPractice

	INSERT INTO  ETL.BaseWHPractice

	SELECT 
		   PracticeCode
		  ,PCTCode
		  ,HealthAuthorityCode
		  ,Practice
		  ,PCT
		  ,HealthAuthority
		  ,LocalPCT

	FROM 
		   WarehouseOLAP.dbo.OlapPractice

END

﻿CREATE PROCEDURE [ETL].[BuildAEDiagnosis]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseAEDiagnosis

	INSERT INTO  ETL.BaseAEDiagnosis

	SELECT 
		   ReferenceCode
		  ,Reference
		  ,ReferenceParentCode
		  ,DisplayOrder

	FROM WarehouseOLAP.dbo.OlapAEDiagnosis

END

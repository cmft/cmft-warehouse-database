﻿create PROCEDURE [ETL].[BuildTARNEncounter_1]
AS
BEGIN

	TRUNCATE TABLE WarehouseReporting.TARN.Invalid_Values 
	TRUNCATE TABLE WarehouseReporting.TARN.Encounter
	TRUNCATE TABLE WarehouseReporting.TARN.Interventions
	TRUNCATE TABLE WarehouseReporting.TARN.Observations

	EXEC TARN.CreateTARNEncounter Null --20min

		
END

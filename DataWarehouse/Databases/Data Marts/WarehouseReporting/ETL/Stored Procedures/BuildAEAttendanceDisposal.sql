﻿CREATE PROCEDURE [ETL].[BuildAEAttendanceDisposal]

AS

BEGIN
	TRUNCATE TABLE  ETL.BaseAEAttendanceDisposal

	INSERT INTO  ETL.BaseAEAttendanceDisposal

	SELECT 
	   AttendanceDisposalCode
      ,AttendanceDisposal
      ,AttendanceDisposalNationalCode

	FROM 
		 WarehouseOLAP.dbo.OlapAEAttendanceDisposal

END

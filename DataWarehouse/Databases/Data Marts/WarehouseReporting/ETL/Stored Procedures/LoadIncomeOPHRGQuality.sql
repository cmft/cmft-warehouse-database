﻿
create proc ETL.LoadIncomeOPHRGQuality

as

insert into WarehouseReporting.[Income].[OPHRGQuality]

(
SnapshotRecno
,[Period]
,[Position]
,[Version]
,[SUSIdentifier]
,[QualityTypeCode]
,[QualityCode]
,[QualityMessage]
)

select
	SnapshotRecno
	,[Period]
	,[Position]
	,[Version]
	,[SUSIdentifier]
	,[QualityTypeCode]
	,[QualityCode]
	,[QualityMessage]
from
	WarehouseReporting.[ETL].[TLoadIncomeOPHRGQuality]
﻿
create proc [ETL].[LoadIncomeAPCSpell]


as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

insert into Income.APCSpell
	

	 (
	 [SnapshotRecno]
    ,[Period]
    ,[Position]
    ,[Version]
    ,[MonthNumber]
    ,[PCTCode]
    ,[TrustCode]
    ,[SiteCode]
    ,[SpecialtyCode]
    ,[HRGCode]
    ,[PatientClassificationCode]
    ,[AdmissionMethodCode]
    ,[LoS]
    ,[DischargeDate]
    ,[SpecialServiceCode1]
    ,[SpecialServiceCode2]
    ,[SpecialServiceCode3]
    ,[SpecialServiceCode4]
    ,[SpecialServiceCode5]
    ,[SpecialServiceCode6]
    ,[SpecialServiceCode7]
    ,[SpecialServiceCode8]
    ,[SpecialServiceCode9]
    ,[SpecialServiceCode10]
    ,[SpecialServiceCode11]
    ,[SpecialServiceCode12]
    ,[SpecialServiceCode13]
    ,[SpecialServiceCode14]
    ,[SpecialServiceCode15]
    ,[SpecialServiceCode16]
    ,[SpecialServiceCode17]
    ,[SpecialServiceCode18]
    ,[SpecialServiceCode19]
    ,[SpecialServiceCode20]
    ,[SpecialServiceCode21]
	,GpPracticeCode
    ,[PatientCode]
    ,[ConsultantCode]
    ,[GpCode]
    ,[DateOfBirth]
    ,[ReportMasterID]
    ,[UserField1]
    ,[UserField2]
    ,[UserField3]
    ,[UserField4]
    ,[UserField5]
    ,[AdmissionDate]
    ,[GroupingMethodFlag]
    ,[PODCode]
    ,[SpecialistCommissioningCode]
		   )



select
 [SnapshotRecno]
    ,[Period]
    ,[Position]
    ,[Version]
    ,[MonthNumber]
    ,[PCTCode]
    ,[TrustCode]
    ,[SiteCode]
    ,[SpecialtyCode]
    ,[HRGCode]
    ,[PatientClassificationCode]
    ,[AdmissionMethodCode]
    ,[LoS]
    ,[DischargeDate]
    ,[SpecialServiceCode1]
    ,[SpecialServiceCode2]
    ,[SpecialServiceCode3]
    ,[SpecialServiceCode4]
    ,[SpecialServiceCode5]
    ,[SpecialServiceCode6]
    ,[SpecialServiceCode7]
    ,[SpecialServiceCode8]
    ,[SpecialServiceCode9]
    ,[SpecialServiceCode10]
    ,[SpecialServiceCode11]
    ,[SpecialServiceCode12]
    ,[SpecialServiceCode13]
    ,[SpecialServiceCode14]
    ,[SpecialServiceCode15]
    ,[SpecialServiceCode16]
    ,[SpecialServiceCode17]
    ,[SpecialServiceCode18]
    ,[SpecialServiceCode19]
    ,[SpecialServiceCode20]
    ,[SpecialServiceCode21]
	,GpPracticeCode
    ,[PatientCode]
    ,[ConsultantCode]
    ,[GpCode]
    ,[DateOfBirth]
    ,[ReportMasterID]
    ,[UserField1]
    ,[UserField2]
    ,[UserField3]
    ,[UserField4]
    ,[UserField5]
    ,[AdmissionDate]
    ,[GroupingMethodFlag]
    ,[PODCode]
    ,[SpecialistCommissioningCode]
from
	ETL.TLoadIncomeAPCSpell
where
	PODCode is null



﻿CREATE TABLE [ETL].[BaseIncomeHRGUplift] (
    [Period]         VARCHAR (50) NULL,
    [Position]       VARCHAR (50) NULL,
    [Version]        INT          NULL,
    [SourceUniqueID] VARCHAR (50) NOT NULL,
    [HRGCode]        VARCHAR (50) NULL
);


﻿CREATE TABLE [ETL].[BaseWHDiagnosis] (
    [DiagnosisCode]                      VARCHAR (10)  NOT NULL,
    [Diagnosis]                          VARCHAR (400) NULL,
    [DiagnosisChapterCode]               VARCHAR (3)   NULL,
    [DiagnosisChapter]                   VARCHAR (66)  NULL,
    [IsCharlsonFlag]                     BIT           NULL,
    [IsConnectingForHealthMandatoryFlag] BIT           NULL,
    [IsAlwaysPresentFlag]                BIT           NULL,
    [IsLongTermFlag]                     BIT           NULL,
    CONSTRAINT [PK_BaseWHDiagnosis] PRIMARY KEY CLUSTERED ([DiagnosisCode] ASC)
);


﻿CREATE TABLE [ETL].[LoSSPC] (
    [ReportGroup]        VARCHAR (8)  NOT NULL,
    [UnitID]             INT          NULL,
    [RowGroup]           BIGINT       NULL,
    [ColumnGroup]        INT          NOT NULL,
    [Level]              VARCHAR (50) NULL,
    [Parent]             VARCHAR (50) NULL,
    [Unit]               VARCHAR (50) NULL,
    [LoSGroup]           VARCHAR (50) NULL,
    [Week]               VARCHAR (50) NULL,
    [SnapshotDate]       DATETIME     NULL,
    [FinancialYear]      VARCHAR (50) NULL,
    [Cases]              FLOAT (53)   NULL,
    [Mean]               FLOAT (53)   NULL,
    [AverageMovingRange] FLOAT (53)   NULL,
    [UCL]                FLOAT (53)   NULL,
    [LCL]                FLOAT (53)   NULL
);


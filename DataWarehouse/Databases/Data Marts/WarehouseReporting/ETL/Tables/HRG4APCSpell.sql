﻿CREATE TABLE [ETL].[HRG4APCSpell] (
    [RowNo]                  INT          NULL,
    [HRGCode]                VARCHAR (50) NULL,
    [DominantOperationCode]  VARCHAR (50) NULL,
    [PrimaryDiagnosisCode]   VARCHAR (50) NULL,
    [SecondaryDiagnosisCode] VARCHAR (50) NULL,
    [EpisodeCount]           VARCHAR (50) NULL,
    [LOS]                    INT          NULL,
    [ReportingSpellLOS]      VARCHAR (50) NULL,
    [Trimpoint]              VARCHAR (50) NULL,
    [ExcessBeddays]          VARCHAR (50) NULL,
    [CCDays]                 VARCHAR (50) NULL
);


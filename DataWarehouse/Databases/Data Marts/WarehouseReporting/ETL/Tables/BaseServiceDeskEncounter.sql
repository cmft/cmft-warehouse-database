﻿CREATE TABLE [ETL].[BaseServiceDeskEncounter] (
    [EncounterRecNo]          DECIMAL (18)    NOT NULL,
    [SourceUniqueID]          DECIMAL (18)    NOT NULL,
    [Request]                 NVARCHAR (80)   NULL,
    [StartTime]               DATETIME        NULL,
    [DeadlineTime]            DATETIME        NULL,
    [ResolvedTime]            DATETIME        NULL,
    [EndTime]                 DATETIME        NULL,
    [RequestStatus]           NVARCHAR (255)  NULL,
    [RequestStatusGroup]      VARCHAR (6)     NOT NULL,
    [ResolvedFlag]            INT             NOT NULL,
    [BreachStatus]            DECIMAL (1)     NULL,
    [PredictedBreachStatus]   DECIMAL (1)     NULL,
    [ResolvedDurationMinutes] INT             NULL,
    [RequestDurationMinutes]  INT             NULL,
    [TargetDurationMinutes]   INT             NULL,
    [BreachValue]             INT             NULL,
    [Category]                NVARCHAR (255)  NULL,
    [Workgroup]               NVARCHAR (50)   NULL,
    [CreatedBy]               NVARCHAR (80)   NULL,
    [AssignedTo]              NVARCHAR (80)   NULL,
    [CreatedTime]             DATETIME        NULL,
    [Priority]                NVARCHAR (255)  NULL,
    [Template]                NVARCHAR (50)   NULL,
    [Classification]          NVARCHAR (255)  NULL,
    [ClosureDescription]      NVARCHAR (255)  NULL,
    [AssignmentCount]         DECIMAL (10)    NULL,
    [CallerFullName]          NVARCHAR (80)   NULL,
    [CallerEmail]             NVARCHAR (255)  NULL,
    [CallerOrganization]      NVARCHAR (50)   NULL,
    [SolutionNotes]           NVARCHAR (4000) NULL,
    [History]                 NTEXT           NULL,
    [Impact]                  NVARCHAR (255)  NULL,
    [PriorityCode]            VARCHAR (2)     NULL,
    CONSTRAINT [PK_BaseServiceDeskEncounter] PRIMARY KEY CLUSTERED ([EncounterRecNo] ASC)
);


GO
CREATE NONCLUSTERED INDEX [INDEXServiceDeskEncounterSummary]
    ON [ETL].[BaseServiceDeskEncounter]([StartTime] ASC)
    INCLUDE([SourceUniqueID], [RequestStatus], [RequestStatusGroup], [ResolvedFlag], [BreachStatus], [ResolvedDurationMinutes], [BreachValue], [Category], [Workgroup], [AssignedTo], [Priority], [Classification]);


GO
CREATE NONCLUSTERED INDEX [<Name of Missing Index, sysname,>]
    ON [ETL].[BaseServiceDeskEncounter]([EndTime] ASC)
    INCLUDE([SourceUniqueID], [RequestStatus], [RequestStatusGroup], [ResolvedFlag], [BreachStatus], [ResolvedDurationMinutes], [BreachValue], [Category], [Workgroup], [AssignedTo], [Priority], [Classification]);


GO
CREATE NONCLUSTERED INDEX [INDEXServiceDeskEndTime]
    ON [ETL].[BaseServiceDeskEncounter]([EndTime] ASC)
    INCLUDE([SourceUniqueID], [RequestStatus], [RequestStatusGroup], [ResolvedFlag], [BreachStatus], [ResolvedDurationMinutes], [BreachValue], [Category], [Workgroup], [AssignedTo], [Priority], [Classification]);


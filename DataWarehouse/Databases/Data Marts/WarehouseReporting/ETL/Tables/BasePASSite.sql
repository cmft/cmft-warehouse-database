﻿CREATE TABLE [ETL].[BasePASSite] (
    [SiteCode]       VARCHAR (50) NOT NULL,
    [Site]           VARCHAR (67) NULL,
    [Colour]         VARCHAR (50) NULL,
    [Sequence]       INT          NULL,
    [MappedSiteCode] VARCHAR (10) NULL,
    [MappedSite]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED ([SiteCode] ASC)
);


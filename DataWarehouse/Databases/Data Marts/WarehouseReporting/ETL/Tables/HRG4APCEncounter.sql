﻿CREATE TABLE [ETL].[HRG4APCEncounter] (
    [RowNo]                     INT          NULL,
    [EncounterRecNo]            INT          NULL,
    [HRGCode]                   VARCHAR (50) NULL,
    [GroupingMethodFlag]        VARCHAR (50) NULL,
    [DominantOperationCode]     VARCHAR (50) NULL,
    [PBCCode]                   VARCHAR (50) NULL,
    [CalculatedEpisodeDuration] VARCHAR (50) NULL,
    [ReportingEpisodeDuration]  VARCHAR (50) NULL,
    [Trimpoint]                 VARCHAR (50) NULL,
    [ExcessBeddays]             VARCHAR (50) NULL,
    [SpellReportFlag]           VARCHAR (50) NULL
);


﻿CREATE TABLE [ETL].[BaseDQError] (
    [LoadStartDate]            DATE         NULL,
    [LoadEndDate]              DATE         NULL,
    [Date]                     DATE         NULL,
    [PrimaryDate]              DATE         NULL,
    [PrimaryDateLabel]         VARCHAR (7)  NOT NULL,
    [SecondaryDate]            DATE         NULL,
    [SecondaryDateLabel]       VARCHAR (9)  NOT NULL,
    [PrimaryIdentifier]        VARCHAR (50) NULL,
    [PrimaryIdentifierLabel]   VARCHAR (13) NOT NULL,
    [SecondaryIdentifier]      VARCHAR (30) NULL,
    [SecondaryIdentifierLabel] VARCHAR (13) NOT NULL,
    [TypeCode]                 VARCHAR (10) NULL,
    [InterfaceCode]            VARCHAR (5)  NULL,
    [PointOfDelivery]          VARCHAR (5)  NULL,
    [EncounterSourceUniqueID]  VARCHAR (50) NULL,
    [SourcePatientNo]          VARCHAR (50) NULL,
    [DirectorateCode]          INT          NULL,
    [SiteCode]                 VARCHAR (10) NULL,
    [SpecialtyCode]            INT          NOT NULL,
    [NHSNumber]                VARCHAR (17) NULL,
    [CreatedBy]                INT          NULL,
    [Value]                    INT          NULL
);


GO
CREATE NONCLUSTERED INDEX [Ix_ETL_BaseDQError_Date_PI]
    ON [ETL].[BaseDQError]([Date] ASC, [PrimaryIdentifier] ASC)
    INCLUDE([PrimaryDate], [PrimaryDateLabel], [SecondaryDate], [SecondaryDateLabel], [PrimaryIdentifierLabel], [SecondaryIdentifier], [SecondaryIdentifierLabel], [TypeCode], [InterfaceCode], [PointOfDelivery], [DirectorateCode], [SiteCode], [SpecialtyCode], [NHSNumber], [Value]) WHERE ([PrimaryIdentifier] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [Ix_ETL_BaseDQError_TC_D_PI]
    ON [ETL].[BaseDQError]([TypeCode] ASC, [Date] ASC, [PrimaryIdentifier] ASC)
    INCLUDE([PrimaryDate], [PrimaryDateLabel], [SecondaryDate], [SecondaryDateLabel], [PrimaryIdentifierLabel], [SecondaryIdentifier], [SecondaryIdentifierLabel], [InterfaceCode], [PointOfDelivery], [DirectorateCode], [SiteCode], [SpecialtyCode], [NHSNumber], [Value]) WHERE ([PrimaryIdentifier] IS NOT NULL);


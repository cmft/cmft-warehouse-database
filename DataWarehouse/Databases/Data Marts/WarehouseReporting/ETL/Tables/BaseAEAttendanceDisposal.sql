﻿CREATE TABLE [ETL].[BaseAEAttendanceDisposal] (
    [AttendanceDisposalCode]         INT           NOT NULL,
    [AttendanceDisposal]             VARCHAR (255) NULL,
    [AttendanceDisposalNationalCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_BaseAEAttendanceDisposal] PRIMARY KEY CLUSTERED ([AttendanceDisposalCode] ASC)
);


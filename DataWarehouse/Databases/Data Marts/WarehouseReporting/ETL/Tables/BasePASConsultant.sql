﻿CREATE TABLE [ETL].[BasePASConsultant] (
    [ConsultantCode]         VARCHAR (10) NOT NULL,
    [Consultant]             VARCHAR (49) NULL,
    [NationalConsultantCode] VARCHAR (8)  NULL,
    [SpecialtyCode]          VARCHAR (5)  NULL,
    [Specialty]              VARCHAR (32) NULL,
    [DomainLogin]            VARCHAR (50) NULL,
    CONSTRAINT [PK_BasePASConsultant] PRIMARY KEY CLUSTERED ([ConsultantCode] ASC)
);


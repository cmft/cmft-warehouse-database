﻿CREATE TABLE [ETL].[BaseWHPractice] (
    [PracticeCode]        VARCHAR (10)  NOT NULL,
    [PCTCode]             VARCHAR (10)  NULL,
    [HealthAuthorityCode] VARCHAR (10)  NULL,
    [Practice]            VARCHAR (255) NULL,
    [PCT]                 VARCHAR (255) NULL,
    [HealthAuthority]     VARCHAR (255) NULL,
    [LocalPCT]            VARCHAR (255) NULL,
    CONSTRAINT [PK_BaseWHPractice] PRIMARY KEY CLUSTERED ([PracticeCode] ASC)
);


﻿CREATE TABLE [ETL].[BaseAEDiagnosis] (
    [ReferenceCode]       INT          NOT NULL,
    [Reference]           VARCHAR (80) NOT NULL,
    [ReferenceParentCode] INT          NULL,
    [DisplayOrder]        SMALLINT     NOT NULL,
    CONSTRAINT [PK_BaseAEDiagnosis] PRIMARY KEY CLUSTERED ([ReferenceCode] ASC)
);


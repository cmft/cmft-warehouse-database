﻿CREATE TABLE [ETL].[HRG4APCFlag] (
    [RowNo]           INT          NULL,
    [ProviderSpellNo] VARCHAR (50) NULL,
    [SequenceNo]      INT          NULL,
    [SpellFlag]       VARCHAR (50) NULL
);


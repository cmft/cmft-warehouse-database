﻿CREATE TABLE [ETL].[BaseWHOperation] (
    [OperationCode]    VARCHAR (10)  NOT NULL,
    [Operation]        VARCHAR (68)  NULL,
    [OperationGroup]   VARCHAR (61)  NULL,
    [OperationChapter] VARCHAR (113) NULL,
    CONSTRAINT [PK_BaseWHOperation] PRIMARY KEY CLUSTERED ([OperationCode] ASC)
);


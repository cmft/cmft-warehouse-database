﻿CREATE TABLE [ETL].[ConsultantCodeMapping] (
    [ID]                     INT          IDENTITY (1, 1) NOT NULL,
    [ConsultantPASCode]      VARCHAR (50) NULL,
    [ConsultantWindowsLogin] VARCHAR (50) NULL
);


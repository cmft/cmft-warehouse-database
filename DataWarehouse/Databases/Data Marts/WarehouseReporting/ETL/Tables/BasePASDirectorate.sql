﻿CREATE TABLE [ETL].[BasePASDirectorate] (
    [DirectorateCode] VARCHAR (5)  NULL,
    [Directorate]     VARCHAR (50) NULL,
    [Division]        VARCHAR (50) NULL
);


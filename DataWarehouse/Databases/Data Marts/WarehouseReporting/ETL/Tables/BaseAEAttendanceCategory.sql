﻿CREATE TABLE [ETL].[BaseAEAttendanceCategory] (
    [AttendanceCategoryCode]  INT           NOT NULL,
    [AttendanceCategory]      VARCHAR (255) NOT NULL,
    [AttendanceCategoryGroup] VARCHAR (15)  NOT NULL,
    CONSTRAINT [PK_BaseAEAttendanceCategory] PRIMARY KEY CLUSTERED ([AttendanceCategoryCode] ASC)
);


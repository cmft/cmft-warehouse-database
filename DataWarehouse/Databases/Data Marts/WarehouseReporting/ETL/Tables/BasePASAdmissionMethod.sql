﻿CREATE TABLE [ETL].[BasePASAdmissionMethod] (
    [AdmissionMethodCode]         VARCHAR (2)   NOT NULL,
    [AdmissionMethod]             VARCHAR (50)  NULL,
    [NationalAdmissionMethodCode] VARCHAR (2)   NOT NULL,
    [NationalAdmissionMethod]     VARCHAR (255) NULL,
    [AdmissionMethodTypeCode]     VARCHAR (3)   NOT NULL,
    [AdmissionMethodType]         VARCHAR (50)  NULL,
    CONSTRAINT [PK_BasePASAdmissionMethod] PRIMARY KEY CLUSTERED ([AdmissionMethodCode] ASC)
);


﻿CREATE TABLE [ETL].[BaseWHHRG] (
    [HRGCode]        VARCHAR (10)  NOT NULL,
    [HRG]            VARCHAR (268) NULL,
    [HRGChapterCode] VARCHAR (1)   NULL,
    [HRGChapter]     VARCHAR (3)   NULL
);


﻿CREATE TABLE [ETL].[HRG4OPEncounter] (
    [EncounterRecno]        INT          NULL,
    [RowNo]                 INT          NULL,
    [HRGCode]               VARCHAR (50) NULL,
    [GroupingMethodFlag]    VARCHAR (50) NULL,
    [DominantOperationCode] VARCHAR (50) NULL
);


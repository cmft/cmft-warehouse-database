﻿CREATE TABLE [ETL].[BaseDQCensus] (
    [LoadStartDate]   DATE         NULL,
    [LoadEndDate]     DATE         NULL,
    [Date]            DATE         NULL,
    [Type]            VARCHAR (10) NULL,
    [PointOfDelivery] VARCHAR (5)  NULL,
    [InterfaceCode]   VARCHAR (5)  NULL,
    [DirectorateCode] INT          NULL,
    [Denominator]     INT          NULL,
    [Numerator]       INT          NULL
);


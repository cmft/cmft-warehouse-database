﻿CREATE TABLE [ETL].[LoSPatientLevelData] (
    [SnapshotDate]        DATETIME      NOT NULL,
    [LoSGroup]            VARCHAR (50)  NULL,
    [DivisionCode]        VARCHAR (5)   NULL,
    [Division]            VARCHAR (50)  NULL,
    [AdmissionMethodType] VARCHAR (50)  NULL,
    [WardCode]            VARCHAR (50)  NULL,
    [Ward]                VARCHAR (100) NULL,
    [LoS]                 FLOAT (53)    NULL,
    [SpecialtyCode]       VARCHAR (50)  NULL,
    [Specialty]           VARCHAR (100) NULL,
    [AdmissionDate]       DATETIME      NULL,
    [WardStartDate]       DATETIME      NULL,
    [CaseNoteNo]          VARCHAR (50)  NOT NULL,
    [PatientFirstName]    VARCHAR (50)  NULL,
    [PatientSurname]      VARCHAR (50)  NULL,
    [PatientAge]          FLOAT (53)    NULL,
    [Consultant]          VARCHAR (50)  NULL,
    [IntendedManagement]  VARCHAR (50)  NULL,
    [ExpectedLoS]         FLOAT (53)    NULL
);


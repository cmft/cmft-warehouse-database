﻿CREATE TABLE [ETL].[LoSSummarisedReportData] (
    [Division]      VARCHAR (50) NULL,
    [LoSGroup]      VARCHAR (50) NULL,
    [Week]          VARCHAR (50) NULL,
    [SnapshotDate]  DATETIME     NULL,
    [FinancialYear] VARCHAR (50) NULL,
    [Cases]         FLOAT (53)   NULL
);


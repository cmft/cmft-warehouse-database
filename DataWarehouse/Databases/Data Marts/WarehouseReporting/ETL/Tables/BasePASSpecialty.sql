﻿CREATE TABLE [ETL].[BasePASSpecialty] (
    [SpecialtyCode]         VARCHAR (5)   NOT NULL,
    [Specialty]             VARCHAR (39)  NULL,
    [NationalSpecialtyCode] VARCHAR (3)   NULL,
    [NationalSpecialty]     VARCHAR (113) NULL,
    [TreatmentFunctionFlag] BIT           NULL,
    CONSTRAINT [PK_Specialty] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


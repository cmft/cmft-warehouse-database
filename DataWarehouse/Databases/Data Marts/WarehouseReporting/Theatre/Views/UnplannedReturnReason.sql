﻿
create view [Theatre].[UnplannedReturnReason]
as
select
	 UnplannedReturnReasonCode
	,UnplannedReturnReason
	,InactiveFlag
from
	Warehouse.Theatre.UnplannedReturnReason

﻿CREATE VIEW [Theatre].[FactOperation]

AS 

SELECT 
	   [SessionSourceUniqueID]
      ,[SourceUniqueID]
      ,[TheatreCode]
      ,[CancelReasonCode]
      ,[ConsultantCode]
      ,[EpisodicConsultantCode]
      ,[SurgeonCode]
      ,[AnaesthetistCode]
      ,[PrimaryProcedureCode]
      ,[AgeCode]
      ,[SexCode]
      ,[AdmissionTypeCode]
      ,[OperationDate]
      ,[OperationTypeCode]
      ,[TheatreSessionPeriodCode]
      ,[SessionConsultantCode]
      ,[TimetableConsultantCode]
      ,[TemplateConsultantCode]
      ,[SessionAnaesthetistCode]
      ,[TimetableAnaesthetistCode]
      ,[TemplateAnaesthetistCode]
      ,[SessionSpecialtyCode]
      ,[TimetableSpecialtyCode]
      ,[TemplateSpecialtyCode]
      ,[Biohazard]
      ,[ASAScoreCode]
      ,[ReceptionTime]
      ,[ReceptionWaitTime]
      ,[AnaestheticWaitTime]
      ,[AnaestheticTime]
      ,[OperationTime]
      ,[RecoveryTime]
      ,[ReturnTime]
      ,[PatientTime]
      ,[Operation]
      ,[TurnaroundTime]
  FROM [WarehouseOLAP].[dbo].[FactTheatreOperation]

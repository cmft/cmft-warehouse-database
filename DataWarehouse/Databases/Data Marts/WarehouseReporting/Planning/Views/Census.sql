﻿



CREATE VIEW [Planning].[Census]

AS

SELECT 
	 CensusDate
	,Region
	,Purchaser
	,Tariff
	,[Site]
	,Category
	,Specialty
	,Hrg
	,Age
	,Los
	,Scenario
	,Step
	,StepSeqno
	,Period
	,Cases
	,BedDays
	,BedsAt90PctOccupancy
	,Price
	,TheatreMins
	,OperationDuration
	,AnaestheticDuration
FROM 
	Warehouse.Planning.Model




﻿

CREATE VIEW [DQ].[Error]

AS

SELECT
	 LoadStartDate
	,LoadEndDate 
	,[Date] 
	,PrimaryDate 
	,PrimaryDateLabel
	,SecondaryDate
	,SecondaryDateLabel
	,PrimaryIdentifier 
	,PrimaryIdentifierLabel
	,SecondaryIdentifier 
	,SecondaryIdentifierLabel 
	,TypeCode 
	,InterfaceCode 
	,PointOfDelivery 
	,EncounterSourceUniqueID
	,SourcePatientNo
	,DirectorateCode
	,SiteCode
	,SpecialtyCode
	,NHSNumber
	,CreatedBy
	,Value
FROM
	warehousereporting.ETL.BaseDQError



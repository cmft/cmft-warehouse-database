﻿

CREATE VIEW [DQ].[Type]

AS

SELECT
	SourceUniqueID
	,Name
	,Code
	,ListOrder
	,Validation
	,Priority
	,Description
	,IncDashboard
	,ResponsibleTeam
	,Parent
	,InformationOnlyFlag
	,RequiredCDSFlag
	,RequiredHRGGrouperFlag
	,RequiredCQIFlag
FROM 
	warehouse.DQ.Type

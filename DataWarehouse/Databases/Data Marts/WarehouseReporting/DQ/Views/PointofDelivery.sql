﻿

CREATE VIEW [DQ].[PointofDelivery]

AS

SELECT
	 SourceUniqueID
	,PointofDeliveryCode
	,PointofDelivery
	,Price
FROM 
	warehouse.DQ.PointOfDelivery

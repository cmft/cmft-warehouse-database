﻿CREATE VIEW ServiceDesk.Encounter

AS


SELECT 
	  EncounterRecNo
      ,SourceUniqueID
      ,Request
      ,StartTime
      ,DeadlineTime
      ,ResolvedTime
      ,EndTime
      ,RequestStatus
      ,RequestStatusGroup
      ,ResolvedFlag
      ,BreachStatus
      ,PredictedBreachStatus
      ,ResolvedDurationMinutes
      ,RequestDurationMinutes
      ,TargetDurationMinutes
      ,BreachValue
      ,Category
      ,Workgroup
      ,CreatedBy
      ,AssignedTo
      ,CreatedTime
      ,Priority
      ,Template
      ,Classification
      ,ClosureDescription
      ,AssignmentCount
      ,CallerFullName
      ,CallerEmail
      ,CallerOrganization
      ,SolutionNotes
      ,History
      ,Impact
      ,PriorityCode
FROM 
	ETL.BaseServiceDeskEncounter

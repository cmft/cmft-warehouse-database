﻿
CREATE VIEW Bedman.Acuity

AS

SELECT 
	 DailyScoreID
	,WardCode
	,ScoreTime
	,TotalPatientsLevel0
	,TotalPatientsLevel0Score
	,AcuityMutiplierLevel0
	,TotalPatientsLevel1a
	,AcuityMutiplierLevel1a
	,TotalPatientsLevel1aScore
	,TotalPatientsLevel1b
	,AcuityMutiplierLevel1b
	,TotalPatientsLevel1bScore
	,TotalPatientsLevel2
	,AcuityMutiplierLevel2
	,TotalPatientsLevel2Score
	,TotalPatientsLevel3
	,AcuityMutiplierLevel3
	,TotalPatientsLevel3Score
	,AvailableBeds
FROM 
	Warehouse.Bedman.Acuity
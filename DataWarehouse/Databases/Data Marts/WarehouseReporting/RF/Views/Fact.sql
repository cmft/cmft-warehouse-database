﻿CREATE VIEW [RF].[Fact]

AS 

SELECT [EncounterRecno]
      ,[EncounterDate]
      ,[ConsultantCode]
      ,[SpecialtyCode]
      ,[PracticeCode]
      ,[PCTCode]
      ,[SiteCode]
      ,[AgeCode]
      ,[SourceOfReferralCode]
      ,[Cases]
      ,[SexCode]
      ,[DirectorateCode]
  FROM [WarehouseOLAP].[dbo].[FactRF]

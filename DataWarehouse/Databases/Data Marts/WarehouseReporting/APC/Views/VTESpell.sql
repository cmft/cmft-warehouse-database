﻿
CREATE view [APC].[VTESpell] as

select
	 VTESpell.ProviderSpellNo
	,VTESpell.VTECategoryCode

	,VTECategory =
		case VTESpell.VTECategoryCode
		when 'C' then 'VTE Complete'
		when 'I' then 'VTE Incomplete'
		when 'M' then 'VTE Missing'
		end
		
	,VTESpell.VTEExclusionReasonCode
	,VTEExclusionReason.VTEExclusionReason
from
	Warehouse.APC.VTESpell

left join Warehouse.APC.VTEExclusionReason
on	VTEExclusionReason.VTEExclusionReasonCode = VTESpell.VTEExclusionReasonCode

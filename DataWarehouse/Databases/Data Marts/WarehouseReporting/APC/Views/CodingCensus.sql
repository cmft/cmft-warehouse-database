﻿



CREATE VIEW [APC].[CodingCensus]

AS 

SELECT 
	 CensusDate
	,EncounterRecNo
	,SourceUniqueID
	,SourcePatientNo
	,ProviderSpellNo
	,SourceSpellNo
	,SourceEncounterNo
	,AdmissionTime
	,EpisodeStartTime
	,EpisodeEndTime
	,DischargeTime
	,ClinicalCodingCompleteTime
	,CodingLagDays
	,SpecialtyCode
	,EndDirectorateCode
	,EndWardTypeCode
	,PatientCategoryCode
	,SpellClinicalCodingCompleteTime
FROM 
	WarehouseOLAP.dbo.BaseAPCCodingCensus;








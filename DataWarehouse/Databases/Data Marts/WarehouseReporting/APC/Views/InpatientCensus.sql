﻿
/****** Script for SelectTopNRows command from SSMS  ******/
CREATE VIEW [APC].[InpatientCensus]

AS

SELECT 
	[SourceUniqueID]
	,[SourcePatientNo]
	,[NHSNumber]
	,[CasenoteNumber]
	,[SourceSpellNo] 
	,[ProviderSpellNo]
	,[DirectorateCode]
	,[SiteCode] 
	,[WardCode] 		
	,[ConsultantCode]
	,[SpecialtyCode]
	,[SourceAdminCategoryCode]
	,[ActivityInCode]
	,[AdmissionTime]
	,[AdmissionDate]
	,[CensusDate]
	,[InterfaceCode]
	,[Created] 
	,[ByWhom] 
FROM 
	Warehouse.APC.InpatientCensus


﻿



CREATE VIEW [APC].[Diagnosis]

AS

SELECT 
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,ProviderSpellNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
	,Created
	,Updated
	,ByWhom
FROM 
	Warehouse.APC.Diagnosis

union all
	
select
	[SourceUniqueID] = SourcePatientNo + '||' + SourceSpellNo + '||' + left([SourceUniqueID],12) + '||0'
	,[SourceSpellNo]
	,[SourcePatientNo]
	,[ProviderSpellNo]
	,[SourceEncounterNo]
	,[SequenceNo] = 0
	,[PrimaryDiagnosisCode]
	,[SourceUniqueID]
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[warehouse].[APC].[Encounter] Encounter
where
	[PrimaryDiagnosisCode] is not null



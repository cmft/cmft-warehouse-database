﻿

CREATE VIEW APC.Operation

AS

SELECT  
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,ProviderSpellNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,APCSourceUniqueID
	,Created
	,Updated
	,ByWhom
FROM
	Warehouse.APC.Operation
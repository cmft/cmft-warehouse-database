﻿
CREATE VIEW APC.ExpectedLOS AS 

SELECT 
	 SourceUniqueID
	,LoadTime
	,LoadModifiedTime
	,SourceCreatedTime
	,EddCreatedTime
	,SourceSystem
	,ModifiedFromSystem
	,AdmissionTime
	,DischargeTime
	,SourcePatientNo
	,SourceSpellNo
	,SiteCode
	,Ward
	,Specialty
	,Consultant
	,ManagementIntentionCode
	,CasenoteNumber
	,ExpectedLOS
	,EnteredWithin48hrs
	,CreateEddDaysDuration
	,DirectorateCode
	,AdmissionMethodCode
	,RTTPathwayCondition
	,ArchiveFlag
FROM 
	Warehouse.APC.ExpectedLOS


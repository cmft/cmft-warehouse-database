﻿CREATE TABLE [APC].[PatientSurvey] (
    [CensusDate]            DATE          NULL,
    [NHSNumber]             NVARCHAR (17) NULL,
    [CasenoteNumber]        NVARCHAR (14) NULL,
    [WardCode]              NVARCHAR (4)  NULL,
    [Ward]                  NVARCHAR (30) NULL,
    [Forenames]             NVARCHAR (20) NULL,
    [Surname]               NVARCHAR (24) NULL,
    [IntendedProcedureCode] NVARCHAR (7)  NULL,
    [ProcedureType]         VARCHAR (4)   NOT NULL
);


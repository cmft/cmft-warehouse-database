﻿CREATE TABLE [APC].[VTESpellTrafford] (
    [ProviderSpellNo]        VARCHAR (20) NOT NULL,
    [VTECategoryCode]        CHAR (1)     NULL,
    [VTEExclusionReasonCode] VARCHAR (10) NULL
);


GO
CREATE NONCLUSTERED INDEX [Idx_APC_VTESpellTrafford_VTEExclusionRC_PSN]
    ON [APC].[VTESpellTrafford]([VTEExclusionReasonCode] ASC)
    INCLUDE([ProviderSpellNo]);


GO
CREATE NONCLUSTERED INDEX [Idx_VTESpellTraff_PSN_i_VTEExclRC]
    ON [APC].[VTESpellTrafford]([ProviderSpellNo] ASC)
    INCLUDE([VTEExclusionReasonCode]);


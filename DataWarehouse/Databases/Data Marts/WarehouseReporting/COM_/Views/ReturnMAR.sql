﻿
CREATE VIEW COM.ReturnMAR AS

SELECT 
	 ReturnPeriod
	,LoadDate
	,[Return]
	,ReturnItem
	,InterfaceCode
	,SourceUniqueNo
	,ItemDate
	,ItemSpecialty
	,NHSNumber
	,ProfessionalCarer
	,CommissionerCode
	,Commissioner
FROM 
	Warehouse.COM.ReturnMAR
﻿CREATE PROCEDURE [TARN].[SP_QUERY_VALID_COLUMNS_CONTROL] (@DistrictNo AS bigint, @DistrictDate as date = NULL)
AS
BEGIN

	DECLARE @SympDate as datetime
	DECLARE @MTDate as datetime
	
	SET @MTDate = Null
	
	
	-- CHECK DATE OF LAST TARN ETL WAS LATER THAN LAST SYMPHONY RUN - IF NOT - REFRESH
	--SELECT @SympDate=MAX(req_created) FROM [CMMC_Reports].[dbo].[Request_Details]
	--SELECT @MTDate=MAX(Run_Date) FROM TARN.Encounter WHERE DistrictNo = @DistrictNo --AND (@DistrictDate IS NULL OR DateCreated= @DistrictDate)
	
	--IF @SympDate<@MTDate OR ISNULL(@DistrictNo,0) = 0 OR @MTDate IS Null
	--BEGIN

	----IF ISNULL(@DistrictNo,0) = 0
	--	--BEGIN
	--	TRUNCATE TABLE WarehouseReporting.TARN.Invalid_Values 
	--	TRUNCATE TABLE WarehouseReporting.TARN.Encounter
	--	TRUNCATE TABLE WarehouseReporting.TARN.Interventions
	--	TRUNCATE TABLE WarehouseReporting.TARN.Observations
		
	--	EXEC TARN.CreateTARNEncounter Null

	--	EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Encounter'
	--	EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Interventions'
	--	EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Observations'
		
	--	--END
	--	--ELSE
	--	--BEGIN
		
	--	--DELETE FROM WarehouseReporting.TARN.Invalid_Values WHERE DistrictNo = @DistrictNo
	--	--DELETE FROM WarehouseReporting.TARN.Encounter WHERE DistrictNo = @DistrictNo
	--	--DELETE FROM WarehouseReporting.TARN.Interventions WHERE DistrictNo = @DistrictNo
	--	--DELETE FROM WarehouseReporting.TARN.Observations WHERE DistrictNo = @DistrictNo
		
	--	--EXEC TARN.CreateTARNEncounter @DistrictNo

	--	--EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Encounter'
	--	--EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Interventions'
	--	--EXEC WarehouseReporting.TARN.SP_QUERY_VALID_COLUMNS 'Observations'
		
	--	--END
	
	--END
	
	SELECT DISTINCT DistrictNo, S.Full_Column_Name, S.Section, S.System, TIV.DateCreated,
		  (SELECT COUNT(*) 
		  FROM TARN.Invalid_Values TIV2 
		  INNER JOIN TARN.Section S2 ON TIV2.Column_Name = S2.Column_Name		  
		  WHERE TIV2.Row_ID = TIV.Row_ID 
		  AND S2.System = S.System AND Table_Name = '[Encounter]'
		AND ISNULL(Invalid_Column,0) =0) AS NullCount
	 FROM WarehouseReporting.TARN.Invalid_Values TIV
	 INNER JOIN WarehouseReporting.TARN.Section S ON TIV.Column_Name = S.Column_Name
	 WHERE ISNULL(Invalid_Column,0) =0 AND Table_Name = '[Encounter]'
	 AND (@DistrictNo IS NULL OR DistrictNo = @DistrictNo) and DistrictNo is not null
	 AND (@DistrictDate IS NULL OR DateCreated= @DistrictDate)
	 order by system, section

END

﻿CREATE PROCEDURE [TARN].[SP_QUERY_VALID_COLUMNS] (@TableNameParam Varchar(50))
    AS
    SET NOCOUNT ON
    
    --BEGIN TRANSACTION TARN
     
    --DECLARE #Temp TABLE(RowId INT IDENTITY(1,1),
    --                    SchemaName sysname,
    --                    TableName sysname,
    --                    ColumnName SysName,
    --                    DataType VARCHAR(100),
    --                    ColumnValue VARCHAR(MAX))
                        
    CREATE TABLE #Temp (RowId INT IDENTITY(1,1),
                        SchemaName sysname,
                        TableName sysname,
                        ColumnName SysName,
                        --DataType VARCHAR(100),
                        ColumnValue VARCHAR(MAX))
                       
                        
	CREATE UNIQUE CLUSTERED INDEX IX_1 on #Temp (RowID)                        
	CREATE INDEX IX_2 on #Temp (ColumnName)
     
        --grab the columns that we care about
        INSERT  INTO #Temp(TableName,SchemaName, ColumnName)--, DataType)
        SELECT  C.Table_Name,C.TABLE_SCHEMA, C.Column_Name--, C.Data_Type
        FROM    Information_Schema.Columns AS C
                INNER Join Information_Schema.Tables AS T
                    ON C.Table_Name = T.Table_Name
            AND C.TABLE_SCHEMA = T.TABLE_SCHEMA
        WHERE   C.Table_Name = @TableNameParam  --only tables, no views
        AND C.Table_Schema = 'TARN'
        AND C.IS_NULLABLE = 'YES'  --obviously only check nullable columns
     
    DECLARE @iCol INT
    DECLARE @iRow INT
    DECLARE @MaxCol INT
    DECLARE @MaxRow INT
    DECLARE @Rownum INT
    DECLARE @TableName sysname
    DECLARE @ColumnName sysname
    DECLARE @SchemaName sysname
    DECLARE @SQL NVARCHAR(4000)
    DECLARE @SQL2 NVARCHAR(4000)
    DECLARE @PARAMETERS NVARCHAR(4000)
    DECLARE @PARAMETERS2 NVARCHAR(4000)
    DECLARE @ColumnValue VARCHAR(MAX)
    DECLARE @SQLTemplate NVARCHAR(4000)
    DECLARE @TableCount INT
    --DECLARE @DataExists INT
    
    
    --SELECT @TableCount = Select COUNT(*) FROM TARN.Encounter
    
     
    --SELECT  @SQLTemplate = 'If Exists(Select 1
    --                                         From   ReplaceTableName
    --                                         Where  [ReplaceColumnName] IS NULL AND Row_ID = @iRow
    --                                         )
    --                                    Set @DataExists = 1
    --                                Else
    --                                    Set @DataExists = 0',
    --        @PARAMETERS = '@ColumnValue VARCHAR(MAX) OUTPUT, @iRow INT', @iCol = 1
    SELECT  @SQLTemplate = '(Select @ColumnValue = [ReplaceColumnName]
                                         From   ReplaceTableName
                                         Where Row_ID = @iRow
                                         )
                                    ',
			@PARAMETERS = '@ColumnValue VARCHAR(MAX) OUTPUT, @iRow INT', @iCol = 1
     
    SELECT @iCol = 1, @MaxCol = MAX(RowId)
    FROM   #Temp
    
    
    
        SET @SQL2 = '(SELECT @MaxRow = MAX(Row_ID) FROM TARN.' + @TableNameParam + ')'
        SET @PARAMETERS2 = '@MaxRow INT OUTPUT'
        --PREPARE statement FROM @qry;
        EXEC SP_EXECUTESQL @SQL2, @PARAMETERS2, @MaxRow = @MaxRow OUTPUT

    
    
    
    --SELECT @MaxRow = MAX(Row_ID) FROM QUOTENAME(TableName)
    
    SELECT @iRow = 1
    
    --TRUNCATE TABLE TARN.Invalid_Values
    
    WHILE @iRow <= @MaxRow
    
    BEGIN
	--loop over all the columns
	WHILE @iCol <= @MaxCol
		BEGIN
			--change the place holder with the real name
			SELECT  @SQL = REPLACE(REPLACE(@SQLTemplate, 'ReplaceTableName',
						   QUOTENAME(SchemaName) + '.' +
						   QUOTENAME(TableName)), 'ReplaceColumnName', ColumnName)
			FROM    #Temp
			WHERE   RowId = @iCol
			
			SET @ColumnValue = Null
     
			EXEC SP_EXECUTESQL @SQL, @PARAMETERS, @ColumnValue = @ColumnValue OUTPUT, @iRow = @iRow
     
			--update result table if a NULL is found
			--IF @DataExists =1
			--	UPDATE #Temp SET DataFound = 1 WHERE RowId = @iCol
                
				INSERT INTO TARN.Invalid_Values
				(Row_ID, Column_Name, Column_Value, Table_Name)
				SELECT @iRow, ColumnName, @ColumnValue, QUOTENAME(TableName) FROM #Temp WHERE RowID = @iCol --AND DataFound = 1
     
				SET @iCol = @iCol + 1
		END
		
		SET @iRow = @iRow + 1
		
		SELECT @iCol = 1
		
		--SELECT @ColumnValue = Null
		
		--UPDATE #Temp SET DataFound = 0
					
	END	


	DROP TABLE #Temp


	-- ENSURE WE HAVE VALID DISTRICT NUMBERS FOR EACH TABLE
					
	 UPDATE TARN.Invalid_Values 
	 SET DistrictNo = E.DistrictNo, attendancenumber = E.attendancenumber, NHSNumber = E.NHSNumber,
	 DateCreated = E.DateCreated
	 FROM TARN.Invalid_Values IV 
	 INNER JOIN TARN.Encounter E ON IV.Row_ID = E.Row_ID
	 WHERE IV.Table_Name = '[Encounter]'
	 
	 UPDATE TARN.Invalid_Values 
	 SET DistrictNo = O.DistrictNo, NHSNumber = O.NHSNumber,
	 DateCreated = E.DateCreated
	 FROM TARN.Invalid_Values IV 
	 INNER JOIN TARN.Observations O ON IV.Row_ID = O.Row_ID
 	 INNER JOIN TARN.Encounter E ON E.Row_ID = O.Encounter_Row_ID
	 WHERE IV.Table_Name = '[Observations]'
	 
	 UPDATE TARN.Invalid_Values 
	 SET DistrictNo = I.DistrictNo, NHSNumber = I.NHSNumber,
	 DateCreated = E.DateCreated
	 FROM TARN.Invalid_Values IV 
	 INNER JOIN TARN.Interventions I ON IV.Row_ID = I.Row_ID
	 INNER JOIN TARN.Encounter E ON E.Row_ID = I.Encounter_Row_ID
	 WHERE IV.Table_Name = '[Interventions]'
     
     -- Update conditional columns
     -- ENCOUNTER
     


if @TableNameParam='Encounter'
BEGIN

    UPDATE TARN.Invalid_Values SET Invalid_Column = Null
    WHERE Table_name = '[Encounter]'
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	WHERE ISNULL(Column_Value,'') <> '' AND Table_name = '[Encounter]'

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.PatientTransfer,'X') NOT IN ('ET','TR') AND E.Row_ID = IV.Row_ID)  --- NEED TO FIND CORRECT TRANSFER CODES
	AND Column_Name in ('TransferInReason','PreviousHospital')   
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE ISNULL(E.PatientTransfer,'X') NOT IN ('ET','TR') AND E.Row_ID = IV.Row_ID) --- NEED TO FIND CORRECT TRANSFER CODES
	AND Column_Name in ('TransferOutReason','FirstHospital','DepartureDate','NextHospital')
	
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.MechanismOfInjury,'X') NOT IN ('Vehicle') AND E.Row_ID = IV.Row_ID) --- NEED TO FIND CORRECT CODES
	AND Column_Name in ('PositionInVI','ProtectionInVehicle')
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.MechanismOfInjury,'X') NOT IN ('Shooting','Stabbing') AND E.Row_ID = IV.Row_ID) --- NEED TO FIND CORRECT CODES
	AND Column_Name in ('Weapon')
	 
	-- If it's Null or not Yes, then mark the columns as valid (1)	 
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE ISNULL(E.TrappedAtScene,'X') NOT IN ('Yes') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('LOTimeTrapped')

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE ISNULL(E.PreHospitalDetails,'X') NOT IN ('Yes') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('ArrivalModeCode','AttendantsAtLocation')

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.ArrivalModeCode,'X') NOT IN ('Ambulance', 'Helicopter', 'Aircraft', 'Ambulance and Helicopter') AND E.Row_ID = IV.Row_ID)  --- NEED TO FIND CORRECT CODES
	AND Column_Name in ('AmbulanceCrewPRF')

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.AttendantsAtLocation,'X'),1,1) NOT IN ('Y') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('DatePatientSeen','TypeOfAttendant')

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.TypeOfAttendant,'X') NOT IN ('Doctor','Nurse') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Grade','Specialty')
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.TypeOfAttendant,'X') NOT IN ('Doctor','Nurse','Doctor (Ambulance)', 'Immediate Care Doctor') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Training')

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.TypeOfAttendant,'X') NOT IN ('Paramedic', 'Technician (ambulance)', 'Doctor (Ambulance)', 'Immediate Care Doctor') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('AmbulanceService')

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.EDStay,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDStay','EDArrivalDate','EDArrivalTime','EDdeparturedate','EDdeparturetime','TraumaTeam','PreAlertIssued')     

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.TraumaTeam,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('TraumaTeam','TeamLeaderGrade','TeamLeaderSpecialty')  

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.PreAlertIssued,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('PreAlertIssued','DateOfPreAlert','TimeOfPreAlert')       
     
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.EDAttendantsAtLocation,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDAttendantsAtLocation','EDDatePatientSeen','EDDatePatientSeen','EDTimePatientSeen')       
     
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE ISNULL(E.EDTypeOfAttendant,'X') NOT IN ('Doctor','Nurse') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDGrade','EDSpecialty')
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.EDTypeOfAttendant,'X') NOT IN ('Doctor','Nurse','Doctor (Ambulance)', 'Immediate Care Doctor') AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDTraining')	     
     
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.Xray,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Xray','XrayDate','XrayTime','XrayBodyRegion','XrayMethodOfImageTransfer','XrayReport','XrayReportedRadiologist') 

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.CTScan,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('CTScan','CTScanDate','CTScanTime','CTScanBodyRegion','CTScanMethodOfImageTransfer','CTScanReport','CTScanReportedRadiologist')   


 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.Ultrasound,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Ultrasound','UltrasoundDate','UltrasoundTime','UltrasoundBodyRegion','UltrasoundMethodOfImageTransfer','UltrasoundReport','UltrasoundReportedByRadiologist')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.FastScan,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('FastScan','FastScanDate','FastScanTime','FastScanReport','FastScanReportedByRadiologist')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.OtherImaging,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('OtherImaging','APJudetOblique')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.APJudetOblique,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('APJudetOblique','APJudetObliqueMethodOfImageTransfer','APJudetObliqueDate','APJudetObliqueTime','APJudetObliqueReport')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.MRIScan,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('MRIScan','MRIScanBodyRegion','MRIScanMethodOfImageTransfer','MRIScanDate','MRIScanTime','MRIScanReport')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.OperationsProcedures,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('OperationsProcedures','OperationCount','OperationDate','OperationTime','OperationDepartureDate','OperationDepartureTime','procedurecode','SurgeonGrade','SurgeonSpecialty','supervisingsurgeon1code','AnaesthetistGrade')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.supervisingsurgeon1code,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('supervisingsurgeon1code','SupervisorGrade')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE SUBSTRING(ISNULL(E.CCCriticalCareStay,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCCriticalCareStay','CCArrivalDate','CCArrivalTime','CCDepartureDate','CCDepartureTime','CCUnitType','CCLOS','ICUReAdmission')   

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.WardStay,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('WardStay','WSArrivalDate','WSArrivalTime','WSDepartureDate','WSDepartureTime','WardType')  
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.Complications,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Complications','DVT','DU','PE','MultiOrganFailure','OtherComplications')  	
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = Null
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE E.Complications IS NULL AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Complications')  	
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.DischargeOutcome,'X') <> 'Alive' AND E.Row_ID = IV.Row_ID)  --- NEED TO FIND CORRECT CODES
	AND Column_Name in ('SelfDischarge')  	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.DischargeOutcome,'X') <> 'Dead' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('DateOfDeath','TimeOfDeath','PostMortem')   
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.PostMortem,'X') NOT IN ('Post Mortem done and copy sent to TARN','Post Mortem done, NOT sent to TARN') AND E.Row_ID = IV.Row_ID)
	AND Column_Name in ('CauseOfDeath')   	
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE ISNULL(E.DischargeOutcome,'No') <> 'Alive' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('ReAdmission')   	
	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.ReAdmission,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('ReAdmission','ReAdmissionRoute','ReAdmissionDischargeDate','ReAdmissionDischargeTime','ReAdmissionDischargeStatus','ReAdmissionDischargeStatus','ReAdmissionDateOfDischarge','ReAdmissionTimeOfDischarge','ReAdmissionDischargedTo')   


	-- BOAST4
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.Severe_open_fracture,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('FractureContaminated','ContaminationType','CombinedOrthopaedicPlan','SystematicAssessment','VascularImpairment','Antibiotics','AntibioticType','WoundDressing','WoundDressingType','LimbSplint','AnkleKneeSplint') 
	
	
	-- Attendants
	
	-- Set missing ones to Valid for now
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE Column_Name in ('WSAttendants','WSDateSeen','WSTimeSeen','WSTypeofAttendant','WSGrade','WSSpecialty','WSTraining','OPAttendants','OPDateSeen','OPTimeSeen','OPTypeofAttendant','OPGrade','OPSpecialty','OPTraining') 
	
	
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E
	WHERE SUBSTRING(ISNULL(E.CCAttendants,'No'),1,1) <> 'Y' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCDateSeen','CCTimeSeen','CCTypeofAttendant')   

	
	
	
	
	
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	WHERE Column_Name in ('PreExistingMedicalConditions')
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE ISNULL(E.GlasgowOutcomeScale,'') <> '' AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('WhenGOSRecorded')   	
	
	--select districtno, GlasgowOutcomeScale, WhenGOSRecorded from tarn.encounter
	
    update tarn.invalid_values
    set column_value = e.WardStay + ' - ' + convert(varchar,column_value,104)
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where iv.column_name in ('WSArrivalDate')	
	
    update tarn.invalid_values
    set column_value = e.WardStay + ' - ' + convert(varchar,column_value,104)
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where iv.column_name in ('WSDepartureDate')		

    update tarn.invalid_values
    set column_value = e.WardStay + ' - ' + substring(column_value,1,5)
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where iv.column_name in ('WSArrivalTime')	
	
    update tarn.invalid_values
    set column_value = e.WardStay + ' - ' + substring(column_value,1,5)
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where iv.column_name in ('WSDepartureTime')		
	
    update tarn.invalid_values
    set column_value = e.WardStay + ' - ' + column_value
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where iv.column_name in ('WardType')	
	
	
	-- ENSURE COLUMN IS FLAGGED WHERE ARRIVAL DATE IS NULL
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = Null
	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Encounter E 
	WHERE E.arrivaldate IS NULL AND E.Row_ID = IV.Row_ID) 
	AND Column_Name in ('ArrivalDate')
		
END


if @TableNameParam='Observations'
BEGIN
     -- Update conditional columns
     -- OBSERVATIONS	
	
    UPDATE TARN.Invalid_Values SET Invalid_Column = Null
    WHERE Table_name = '[Observations]'
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	WHERE ISNULL(Column_Value,'') <> '' AND Table_name = '[Observations]'

	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Airway_status_on_arrival,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Choose_status_of_airway')   
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Blood_pressure,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Enter_systolic_BP_value','Enter_diastolic_BP_value')  
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O 
	WHERE SUBSTRING(ISNULL(O.Circulation_observations,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('CO_Date','CO_Time','Pulse_rate','Blood_pressure')  	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Breathing_status_on_arrival,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Choose_status_of_breathing')  	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Glasgow_Coma_Scale,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Eye_score','Verbal_score','Motor_score','Total_GCS_score')  	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O 
	WHERE SUBSTRING(ISNULL(O.Nervous_System_observations,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('NSO_Date','NSO_Time','Glasgow_Coma_Scale','Pupil_size','Pupil_reactivity')  	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Oxygen_saturation,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Enter_saturation')  		
	 
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Pulse_rate,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Enter_pulse_rate')  	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Pupil_reactivity,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Left_eye_pupil_reactivity','Right_eye_pupil_reactivity')  
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O 
	WHERE SUBSTRING(ISNULL(O.Pupil_size,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Left_eye_pupil_size','Right_eye_pupil_size')       
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Respiratory_observations,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('RO_Date','RO_Time','Airway_status_on_arrival','Breathing_status_on_arrival','Oxygen_saturation','Unassisted_Respiratory_rate')   	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Unassisted_Respiratory_rate,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Enter_unassisted_respiratory_rate')  
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Observations O
	WHERE SUBSTRING(ISNULL(O.Respiratory_observations,'No'),1,1) <> 'Y' AND O.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Circulation_observations','Nervous_System_observations')  	
 
	DELETE FROM TARN.Invalid_Values
	WHERE Table_Name IN ('[Observations]') AND Column_Name in ('NHSNumber')  
 
 
 -- SET WHETHER IT'S AN INVALID OBSERVATION
 -- If there are no Observations, then set as a valid column
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O 
	WHERE Observation_type = 3 AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('Observations')  

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM Observations O
	WHERE Observation_type = 1 AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDObservations')  
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O
	WHERE Observation_type = 2 AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCObservations')  
	
	
	
 -- SET WHETHER IT'S AN INVALID OBSERVATION
 -- If there is an Observation with no invalid columns, then set as a valid column
 
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Observation_type = 3 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Observations]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('Observations')  	 

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Observation_type = 1 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Observations]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDObservations')  

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Observations O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Observation_type = 2 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Observations]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCObservations')    

END


if @TableNameParam='Interventions'
BEGIN
     -- Update conditional columns
     -- INTERVENTIONS	
     
    UPDATE TARN.Invalid_Values SET Invalid_Column = Null
    WHERE Table_name = '[Interventions]'
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	WHERE ISNULL(Column_Value,'') <> '' AND Table_name = '[Interventions]'    
	
	-- Set to valid columns where Critical Care Grades are Null
   	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Interventions I 
	WHERE SUBSTRING(ISNULL(I.Airway_Support,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID)
	AND Column_Name in ('CCGrade','CCSpecialty','CCTraining')	 

	-- Set to valid columns where Airway support is No or not recorded (not null or Y)
   	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Interventions I 
	WHERE SUBSTRING(ISNULL(I.Airway_Support,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID)
	AND Column_Name in ('Date_of_Airway_Support','Time_of_Airway_Support','Type_of_Airway_Support')   	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV	
	WHERE exists (SELECT 1 FROM TARN.Interventions I
	WHERE SUBSTRING(ISNULL(I.Blood_Products_in_the_first_24_hours,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_blood_product','Start_time_of_blood_product','Finish_time_of_blood_product','Blood_Product','Units_given_at_this_time') 
	
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV
	WHERE exists (SELECT 1 FROM TARN.Interventions I 
	WHERE SUBSTRING(ISNULL(I.Breathing_support,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_Breathing_support','Time_of_Breathing_support','Select_type_of_breathing_support') 	
     
 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I 
	WHERE SUBSTRING(ISNULL(I.Chest_Drain_NOT_needle_thoracocentesis,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_chest_drain','Time_of_chest_drain') 	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I 
	WHERE SUBSTRING(ISNULL(I.Airway_Support,'No'),1,1) <> 'Y' AND I.Intervention_Type = 2 AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Extubation') 

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I 
	WHERE SUBSTRING(ISNULL(I.Embolisation_Interventional_Radiology,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_embolisation','Time_of_embolisation') 	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I
	WHERE SUBSTRING(ISNULL(I.Extubation,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_Extubation','Time_of_Extubation') 	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I
	WHERE SUBSTRING(ISNULL(I.Embolisation_Fluid,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_Fluid','Start_time_of_Fluid','Finish_time_of_Fluid','Fluid','Volume_of_fluid_given_ml') 	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I
	WHERE SUBSTRING(ISNULL(I.Spinal_protection,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_Spinal_Protection','Time_of_Spinal_Protection','Type_of_spinal_protection','Spinal_protection_removed') 	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I
	WHERE SUBSTRING(ISNULL(I.Spinal_protection_removed,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_spinal_protection_removed','Time_spinal_protection_removed') 	

 	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
   	FROM TARN.Invalid_Values IV 	
	WHERE exists (SELECT 1 FROM TARN.Interventions I 
	WHERE SUBSTRING(ISNULL(I.Tranexamic_Acid,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	AND Column_Name in ('Date_of_Tranexamic_Acid','Time_of_Tranexamic_Acid') 	


 --	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
 --  	FROM TARN.Invalid_Values IV 	
	--WHERE exists (SELECT 1 FROM TARN.Interventions I
	--WHERE SUBSTRING(ISNULL(I.Airway_Support,'No'),1,1) <> 'Y' AND I.Row_ID = IV.Row_ID) 
	--AND Column_Name in ('Airway_Support','Breathing_support','Spinal_protection','Spinal_protection_removed','Blood_Products_in_the_first_24_hours','Embolisation_Interventional_Radiology','Embolisation_Fluid','Chest_Drain_NOT_needle_thoracocentesis','Tranexamic_Acid') 	

	DELETE FROM TARN.Invalid_Values
	WHERE Table_Name IN ('[Interventions]') AND Column_Name in ('NHSNumber')  

  --SET WHETHER IT'S AN INVALID INTERVENTION
  --If there are no Interventions, then set as a valid column
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	WHERE Intervention_type = 3 AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('Interventions')  

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	WHERE Intervention_type = 1 AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDInterventions')  
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	WHERE Intervention_type = 2 AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCInterventions')  


 ---- SET WHETHER IT'S AN INVALID INTERVENTION
 ---- If there is an Intervention with no invalid columns, then set as a valid column
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Intervention_type = 3 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Interventions]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('Interventions')  

	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Intervention_type = 1 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Interventions]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('EDInterventions')  
	
	UPDATE TARN.Invalid_Values SET Invalid_Column = 1
	FROM TARN.Invalid_Values IV
	WHERE not exists (SELECT 1 FROM TARN.Interventions O
	INNER JOIN TARN.Invalid_Values IV2 ON O.Row_ID = IV2.Row_ID
	WHERE Intervention_type = 2 AND ISNULL(IV2.Invalid_Column,0) = 0 AND IV2.Table_Name = '[Interventions]' AND O.Encounter_Row_ID = IV.Row_ID) 
	AND Column_Name in ('CCInterventions')  	 

END
   
   
	-- Remove all PAS invalid columns if there is no PAS record
	update tarn.invalid_values
	set invalid_column = 1
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	inner join majortam.dbo.tarncandidate tc on tc.districtnumber = iv.districtno
	where not exists (select 1 from warehouse.apc.encounter apce 
					where e.DistrictNo = apce.DistrictNo 
					and apce.AdmissionDate between dateadd(hour , -23 , tc.DateHospitalArrival) and dateadd(hour , 23 , tc.DateHospitalArrival))
	and system = 'PAS'	


	---- BOTTLENECK!!!!
	   
 --	-- Remove all Symphony invalid columns if there is no Symphony record  
	--update tarn.invalid_values
	--set invalid_column = 1
	--from tarn.invalid_values iv
	--inner join TARN.Section s on iv.column_name = s.column_name
	--inner join TARN.Encounter E on iv.row_id = e.row_id
	--where not exists (select 1 from Warehouse.AE.Encounter AE 
 --   INNER JOIN CMMC_Reports.dbo.Result_details R ON AE.SourceUniqueID = CAST(R.res_atdid AS VARCHAR(50))  and AE.DistrictNo = iv.DistrictNo 
 --   )
 --	and system = 'Symphony'	





 	-- Remove all At Discharge invalid records where there is no At Discharge  
	update tarn.invalid_values
	set invalid_column = 1
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where not exists (select 1 from MAJORTAM.dbo.TARNCandidate MT 
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID AND MT.DistrictNumber = e.districtno
	WHERE MT.CandidateID = MTD.CandidateID AND SectionID = 4 AND ActionID = 99)  
 	and system = 'Major TAM' and section = 'At Discharge'

 	-- Remove all Imaging invalid records where there is no Imaging  
	update tarn.invalid_values
	set invalid_column = 1
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where not exists (select 1 from MAJORTAM.dbo.TARNCandidate MT 
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID AND MT.DistrictNumber = e.districtno
	WHERE MT.CandidateID = MTD.CandidateID AND SectionID = 3)  
 	and system = 'Major TAM' and section = 'Imaging' 	
   
 	-- Remove all Operation invalid records where there is no Operation  
	update tarn.invalid_values
	set invalid_column = 1
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where not exists (select 1 from MAJORTAM.dbo.TARNCandidate MT 
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID AND MT.DistrictNumber = e.districtno
	WHERE MT.CandidateID = MTD.CandidateID AND SectionID = 6 AND ActionID = 3)  
 	and system = 'Major TAM' and section = 'Operations'    
 	
 	
 	-- Remove all Attendant invalid records where there is no Attendant  
	update tarn.invalid_values
	set invalid_column = 1
	from tarn.invalid_values iv
	inner join TARN.Section s on iv.column_name = s.column_name
	inner join TARN.Encounter E on iv.row_id = e.row_id
	where not exists (select 1 from MAJORTAM.dbo.TARNCandidate MT 
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID AND MT.DistrictNumber = e.districtno
	WHERE MT.CandidateID = MTD.CandidateID AND SectionID = 2 AND ActionID = 9)  
	AND iv.Column_Name in ('CCAttendants','CCDateSeen','CCTimeSeen','CCTypeofAttendant','CCGrade','CCSpecialty','CCTraining')   	

	update tarn.invalid_values
	set invalid_column = 1
	from tarn.invalid_values iv
	where iv.Column_Name like '%OtherNotes%'
   
   
   --COMMIT TRANSACTION TARN
   



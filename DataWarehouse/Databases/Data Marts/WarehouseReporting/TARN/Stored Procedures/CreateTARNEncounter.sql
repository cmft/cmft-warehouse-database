﻿CREATE PROCEDURE [TARN].[CreateTARNEncounter] (@DistrictNo bigint) AS
 
BEGIN

----------------------------------------------------------------------------------------------------------
-- REMOVE RECORDS READY FOR RELOAD
----------------------------------------------------------------------------------------------------------

--TRUNCATE TABLE TARN.Encounter

----------------------------------------------------------------------------------------------------------
-- INSERT RECORDS FROM APC & AE INTO STAGING TABLE
----------------------------------------------------------------------------------------------------------

--IF @DistrictNo IS NULL
--BEGIN
--TRUNCATE TABLE TARN.Encounter
--DELETE FROM TARN.Observations WHERE (@DistrictNo IS NULL OR DistrictNo = @DistrictNo)
----WHERE DistrictNo = @DistrictNo
--DELETE FROM [WarehouseReporting].[TARN].[Interventions] WHERE (@DistrictNo IS NULL OR DistrictNo = @DistrictNo)
----WHERE DistrictNo = @DistrictNo	
--ELSE
--END	
	
	
--DELETE FROM TARN.Encounter WHERE (@DistrictNo IS NULL OR DistrictNo = @DistrictNo)

INSERT INTO TARN.Encounter

           ([attendancenumber]
           ,[DistrictNo]
           ,[NHSNumber]
           ,SourceUniqueID
           ,DateCreated
           ,[PatientSurname]
           ,[PatientForename]
           ,[SexCode]
           ,[dateofbirth]
           ,[Age]
           ,[PostcodePart1]
           ,[PostcodePart2]
		   ,EstimatedISS
           ,[TARN]
           ,PatientGP
		   ,GPPractice
           ,[Severe_open_fracture]
           ,[arrivaldate]
           ,[arrivaltime]
           ,[PatientTransfer]
           ,[TransferInReason]
           ,[PreviousHospital]
           ,[TransferOutReason]
           ,[FirstHospital]
           ,[DepartureDate]
           ,[NextHospital]
           ,[Location]
           ,[TypeOfInjury]
           ,[MechanismOfInjury]
           ,[PositionInVI]
           ,[ProtectionInVehicle]
           ,[Weapon]
           ,[InjuryIntent]
           ,[AdditionalInjuryInfo]
           ,[TrappedAtScene]
           ,[LOTimeTrapped]
           ,[PreHospitalDetails]
           ,[ArrivalModeCode]
           ,[AmbulanceCrewPRF]
           ,[AttendantsAtLocation]
           ,[DatePatientSeen]
           ,[TypeOfAttendant]
           ,[Grade]
           ,[Specialty]
           ,[Training]
           ,[AmbulanceService]
           ,[Observations]
           ,[Interventions]
           ,[EDStay]
           ,[EDArrivalDate]
           ,[EDArrivalTime]
           ,[EDdeparturedate]
           ,[EDdeparturetime]
           ,[TraumaTeam]
           ,[TeamLeaderGrade]
           ,[TeamLeaderSpecialty]
           ,[PreAlertIssued]
           ,[DateOfPreAlert]
           ,[TimeOfPreAlert]
           ,[EDObservations]
           ,[EDInterventions]
           ,[EDAttendantsAtLocation]
           ,[EDDatePatientSeen]
           ,[EDTimePatientSeen]
           ,[EDTypeOfAttendant]
           ,[EDGrade]
           ,[EDSpecialty]
           ,[EDTraining]
           ,[Xray]
           ,[XrayDate]
           ,[XrayTime]
           ,[XrayBodyRegion]
           ,[XrayMethodOfImageTransfer]
           ,[XrayReport]
           ,[XrayReportedRadiologist]
           ,[CTScan]
           ,[CTScanDate]
           ,[CTScanTime]
           ,[CTScanBodyRegion]
           ,[CTScanMethodOfImageTransfer]
           ,[CTScanReport]
           ,[CTScanReportedRadiologist]
           ,[Ultrasound]
           ,[UltrasoundDate]
           ,[UltrasoundTime]
           ,[UltrasoundBodyRegion]
           ,[UltrasoundMethodOfImageTransfer]
           ,[UltrasoundReport]
           ,[UltrasoundReportedByRadiologist]
           ,[FastScan]
           ,[FastScanDate]
           ,[FastScanTime]
           ,[FastScanReport]
           ,[FastScanReportedByRadiologist]
           ,[OtherImaging]
           ,[APJudetOblique]
           ,[APJudetObliqueMethodOfImageTransfer]
           ,[APJudetObliqueDate]
           ,[APJudetObliqueTime]
           ,[APJudetObliqueReport]
           ,[MRIScan]
           ,[MRIScanBodyRegion]
           ,[MRIScanMethodOfImageTransfer]
           ,[MRIScanDate]
           ,[MRIScanTime]
           ,[MRIScanReport]
           , [OperationsProcedures]
           ,[OperationCount]
           ,[OperationDate]
           ,[OperationTime]
           ,[OperationDepartureDate]
           ,[OperationDepartureTime]
           ,[procedurecode]
           ,[SurgeonGrade]
           ,[SurgeonSpecialty]
           ,[supervisingsurgeon1code]
           , SupervisorGrade
           , AnaesthetistGrade
           ,[CCCriticalCareStay]
           ,[CCArrivalDate]
           ,[CCArrivalTime]
           ,[CCDepartureDate]
           ,[CCDepartureTime]
           ,[CCUnitType]
           ,[CCLOS]
           ,[ICUReAdmission]
           ,[CCObservations]
           ,[CCInterventions]
           ,[WardStay]
           ,[WSArrivalDate]
           ,[WSArrivalTime]
           ,[WSDepartureDate]
           ,[WSDepartureTime]
           , WardType
           ,[DetailedInjuryDescriptions]
           ,[Complications]
           ,[DVT]
           ,[DU]
           ,[PE]
           ,[MultiOrganFailure]
           ,[OtherComplications]
           ,[PreExistingMedicalConditions]
           ,[DischargeOutcome]
           ,[SelfDischarge]
           ,[DateOfDeath]
           ,[TimeOfDeath]
           ,[PostMortem]
           ,[CauseOfDeath]
           ,[LengthOfStay]
           ,[LOSCriticalCare]
           ,[DaysIntubated]
           ,[DischargedTo]
           ,[GlasgowOutcomeScale]
           ,[WhenGOSRecorded]
           ,[ReAdmission]
           ,[ReAdmissionRoute]
           ,[ReAdmissionDischargeDate]
           ,[ReAdmissionDischargeTime]
           ,[ReAdmissionDischargeStatus]
           ,[ReAdmissionDateOfDischarge]
           ,[ReAdmissionTimeOfDischarge]
           ,[ReAdmissionDischargedTo]
           ,[Run_Date]
           ,Status
           ,CaseNoteNumber
           ,AdmissionTime)



				select distinct

						NULL -- aee.attendancenumber
						, tc.DistrictNumber DistrictNo
						, replace(apce.NHSNumber,' ' ,'')
						,NULL --  aee.SourceUniqueID
						, tc.DateCreated
						, ISNULL(apce.PatientSurname,tc.Surname)
						, ISNULL(apce.PatientForename,tc.Forenames)
						, ISNULL(apce.SexCode,Gender)
						, ISNULL(apce.dateofbirth,tc.DOB)
						, datediff(yy,ISNULL(apce.dateofbirth,tc.DOB), getdate()) as Age
						, rtrim(substring(apce.Postcode,1,charindex(' ',apce.Postcode)-1)) as PostcodePart1
						, ltrim(substring(apce.Postcode,charindex(' ',apce.Postcode)+2,len(apce.Postcode))) as PostcodePart2
						, EstimatedISS
						, Null as TARN
						, CASE ISNULL(apce.RegisteredGpPracticeCode,'') WHEN '' THEN 'No' ELSE 'Yes' END
						, apce.RegisteredGpPracticeCode



						, Null as Severe_open_fracture
						, cast (DateHospitalArrival as date)
						, cast (DateHospitalArrival as time)
						, CASE apce.admissionmethodcode WHEN 'TR' THEN 'Yes' ELSE 
							CASE apce.dischargedestinationcode	WHEN 'TG' THEN 'Yes'
																WHEN 'TM' THEN 'Yes'
																WHEN 'TO' THEN 'Yes'
																WHEN 'TP' THEN 'Yes'						
									ELSE 'No' END END as PatientTransfer 
						, Null as TransferInReason
						, Null as PreviousHospital
						, Null as TransferOutReason
						, CASE apce.admissionmethodcode WHEN 'TR' THEN 'No' ELSE 'Yes' END as FirstHospital
						, apce.dischargedate as DepartureDate
						, Null as NextHospital
						, Null as Location
						, Null as TypeOfInjury
						, Null as MechanismOfInjury
						, Null as PositionInVI
						, Null as ProtectionInVehicle
						, Null as Weapon
						, Null as InjuryIntent
						, Null as AdditionalInjuryInfo
						, Null as TrappedAtScene
						, Null as LOTimeTrapped
						, Null as PreHospitalDetails
						, NULL --aee.ArrivalModeCode
						, NULL --aee.AmbulanceCrewPRF
						, Null as AttendantsAtLocation
						, Null as DatePatientSeen
						, Null as TypeOfAttendant
						, Null as Grade
						, Null as Specialty
						, Null as Training
						, Null as AmbulanceService
						, Null as Observations
						, Null as Interventions
						, NULL -- CASE ISNULL(aee.arrivaldate,'') WHEN '' THEN 'No' ELSE 'Yes' END as EDStay
						, NULL -- aee.arrivaldate as EDArrivalDate
						, NULL -- aee.arrivaltime as EDArrivalTime
						, NULL -- cast(aee.departuretime as date) as EDdeparturedate
						, NULL -- cast(aee.departuretime as time) as EDdeparturetime
						, NULL --  TraumaTeam = CASE ISNULL(aee.arrivaldate,9999) WHEN 9999 THEN 'No' ELSE 'Yes' END
						, Null as TeamLeaderGrade
						, Null as TeamLeaderSpecialty
						, Null as PreAlertIssued
						, Null as DateOfPreAlert
						, Null as TimeOfPreAlert
						, Null as EDObservations  
						, Null as EDInterventions 
						, Null as EDAttendantsAtLocation
						, NULL -- cast(aee.seenfortreatmenttime as date) as EDDatePatientSeen
						, NULL -- cast(aee.seenfortreatmenttime as time) as EDTimePatientSeen
						, Null as EDTypeOfAttendant
						, Null as EDGrade
						, NULL -- aee.referredtospecialtycode as EDSpecialty
						, Null as EDTraining
						, Null as Xray
						, Null as XrayDate
						, Null as XrayTime
						, Null as XrayBodyRegion
						, Null as XrayMethodOfImageTransfer
						, Null as XrayReport
						, Null as XrayReportedRadiologist
						, Null as CTScan
						, Null as CTScanDate
						, Null as CTScanTime
						, Null as CTScanBodyRegion
						, Null as CTScanMethodOfImageTransfer
						, Null as CTScanReport
						, Null as CTScanReportedRadiologist
						, Null as Ultrasound
						, Null as UltrasoundDate
						, Null as UltrasoundTime
						, Null as UltrasoundBodyRegion
						, Null as UltrasoundMethodOfImageTransfer
						, Null as UltrasoundReport
						, Null as UltrasoundReportedByRadiologist
						, Null as FastScan
						, Null as FastScanDate
						, Null as FastScanTime
						, Null as FastScanReport
						, Null as FastScanReportedByRadiologist
						, Null as OtherImaging
						, Null as APJudetOblique
						, Null as APJudetObliqueMethodOfImageTransfer
						, Null as APJudetObliqueDate
						, Null as APJudetObliqueTime
						, Null as APJudetObliqueReport
						, Null as MRIScan
						, Null as MRIScanBodyRegion
						, Null as MRIScanMethodOfImageTransfer
						, Null as MRIScanDate
						, Null as MRIScanTime
						, Null as MRIScanReport
						, Null as OperationsProcedures
						--, (select count(*) from 
						--		warehouse.Theatre.PatientBooking pb
						--		inner join warehouse.Theatre.OperationDetail od
						--		on	od.PatientBookingSourceUniqueID = pb.SourceUniqueID
						--		and	dateadd(day, datediff(day, 0, od.OperationStartDate), 0) between apce.EpisodeStartDate and coalesce(apce.EpisodeEndDate, getdate())
						--		where pb.NHSNumber = replace(apce.NHSNumber, ' ', '')
						--		or pb.casenotenumber = replace(tc.casenotenumber,'/','Y')
						--		) as OperationCount
						, Null as OperationCount
						, cast(thod.operationstartdate as date) as OperationDate
						, cast(thod.operationstartdate as time) as OperationTime
						, cast(thod.dischargetime as date) as OperationDepartureDate
						, cast(thod.dischargetime as time) as OperationDepartureTime
						, Null as procedurecode -- Needs mapping as there will be several procedure codes
						, Null as SurgeonGrade
						, Null as SurgeonSpecialty
						, Null as supervisingsurgeon1code -- Needs mapping as there will be several columns
						, Null as SupervisorGrade
						, Null as AnaesthetistGrade
						, CASE WHEN apcws.WardCode IN ('80','82','83','ITU','HDU') THEN 'Yes' ELSE 'No' END as CCCriticalCareStay
						, CASE WHEN apcws.WardCode IN ('80','82','83','ITU','HDU') THEN apcws.StartDate END as CCArrivalDate
						, CASE WHEN apcws.WardCode IN ('80','82','83','ITU','HDU') THEN apcws.StartTime END as CCArrivalTime
						, CASE WHEN apcws.WardCode IN ('80','82','83','ITU','HDU') THEN apcws.EndDate END as CCDepartureDate
						, CASE WHEN apcws.WardCode IN ('80','82','83','ITU','HDU') THEN apcws.EndTime END as CCDepartureTime
						, CASE WHEN apcws.WardCode IN ('80','82','83','ITU','HDU') THEN apcws.WardCode ELSE Null END as CCUnitType
						, CASE WHEN apcws.WardCode IN ('80','82','83','ITU','HDU') THEN datediff(dd, apcws.StartDate, apcws.EndDate) Else 0 END as CCLOS
						, Null as ICUReAdmission --  Derive from earlier stay in CC within same spell
						, Null as CCObservations  
						, Null as CCInterventions  
						, apcws.WardCode as WardStay 
						, apcws.StartDate as WSArrivalDate
						, apcws.StartTime as WSArrivalTime
						, apcws.EndDate as WSDepartureDate
						, apcws.EndTime as WSDepartureTime
						, apcws.SiteCode as WardType
						, Null as DetailedInjuryDescriptions
						, Null as Complications
						, Null as DVT
						, Null as DU
						, Null as PE
						, Null as MultiOrganFailure
						, Null as OtherComplications
						, Null as PreExistingMedicalConditions
						,CASE ISNULL(apce.DischargeMethodCode,'') WHEN 'DN' Then 'Dead' WHEN 'DP' THEN 'Dead' WHEN '' THEN Null ELSE 'Alive' END as DischargeOutcome
						,CASE apce.DischargeMethodCode WHEN 'SD' THEN 'Yes' Else 'No' END as SelfDischarge
						,CASE apce.DischargeMethodCode WHEN 'DN' THEN CAST(apce.DischargeDate AS DATE) WHEN 'DP' THEN CAST(apce.DischargeDate AS DATE) ELSE Null END as DateOfDeath
						,CASE apce.DischargeMethodCode WHEN 'DN' THEN CAST(apce.DischargeDate AS TIME) WHEN 'DP' THEN CAST(apce.DischargeDate AS TIME) ELSE Null END as TimeOfDeath
						,CASE apce.DischargeMethodCode WHEN 'DP' THEN 'Yes' ELSE 'No' END as PostMortem
						, Null as CauseOfDeath
						, datediff(HOUR, apce.admissiontime, apce.dischargetime) as LengthOfStay
						, Null as LOSCriticalCare -- Derive from StartTime/EndTime - total time in CC during spell


						, Null as DaysIntubated 


						, apce.DischargeDestinationCode as DischargedTo
						, Null as GlasgowOutcomeScale
						, Null as WhenGOSRecorded
						, Null as ReAdmission
						, Null as ReAdmissionRoute
						, Null as ReAdmissionDischargeDate
						, Null as ReAdmissionDischargeTime
						, Null as ReAdmissionDischargeStatus
						, apce.dischargedate as ReAdmissionDateOfDischarge
						, apce.dischargetime as ReAdmissionTimeOfDischarge
						, apce.dischargedestinationcode as ReAdmissionDischargedTo
						,getdate()
						,St.StatusDesc
						,replace(tc.casenotenumber,'/','Y')
						,apce.AdmissionTime

from majortam.dbo.TARNCandidate tc

left join warehouse.apc.encounter apce on tc.DistrictNumber = apce.DistrictNo 
and apce.AdmissionDate between dateadd(hour , -48 , tc.DateHospitalArrival) and dateadd(hour , 48 , tc.DateHospitalArrival)

--left join warehouse.ae.encounter aee
--on	aee.DistrictNo = apce.DistrictNo
--and	aee.DepartureTime between dateadd(hour , -6 , apce.AdmissionTime) and dateadd(hour , 6 , apce.AdmissionTime)
--and	aee.ArrivalTime < apce.AdmissionTime
--and	not exists
--	(
--	select
--		1
--	from
--		warehouse.AE.Encounter LaterAEEncounter
--	where
--		LaterAEEncounter.DistrictNo = apce.DistrictNo
--	and	LaterAEEncounter.DepartureTime between dateadd(hour , -6 , apce.AdmissionTime) and dateadd(hour , 6 , apce.AdmissionTime)
--	and	LaterAEEncounter.ArrivalTime < apce.AdmissionTime
--	and	LaterAEEncounter.AttendanceDisposalCode = '01'
--	and LaterAEEncounter.SiteCode = 'RW3MR'
--	and	LaterAEEncounter.ArrivalTime > aee.ArrivalTime
--	)

left join warehouse.Theatre.PatientBooking PatientBooking
on	PatientBooking.NHSNumber = replace(apce.NHSNumber, ' ', '')
or PatientBooking.casenotenumber = replace(tc.casenotenumber,'/','Y')

left join warehouse.Theatre.OperationDetail thod
on	thod.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
and	dateadd(day, datediff(day, 0, thod.OperationStartDate), 0) between apce.EpisodeStartDate and coalesce(apce.EpisodeEndDate, getdate())

--left join warehouse.apc.criticalcareperiod apccc on apccc.CasenoteNumber = apce.CasenoteNumber
--on apccc.sourcespellno = apce.sourcespellno and apccc.sourcepatientno = apce.sourcepatientno

left join warehouse.apc.wardstay apcws on apcws.sourcespellno = apce.sourcespellno and apcws.sourcepatientno = apce.sourcepatientno

left join MAJORTAM.dbo.Status St ON ST.StatusID = tc.statusid

WHERE --(@DistrictNo IS NULL OR apce.districtno = @DistrictNo) AND 
tc.DistrictNumber IS NOT NULL --= '03458114'

--WHERE apce.districtno = @DistrictNo 
--02032147

--UPDATE TARN.Encounter
--SET CCCriticalCareStay = 'Yes'
--FROM TARN.Encounter T 
--WHERE EXISTS (SELECT 1 FROM TARN.Encounter T1 WHERE T1.DistrictNo = T.DistrictNo AND CCCriticalCareStay = 'Yes')






----------------------------------------------------------------------------------------------------------
-- UPDATE EXISTING RECORDS WITH A & E DATA
----------------------------------------------------------------------------------------------------------

UPDATE TARN.Encounter
SET attendancenumber = aee.attendancenumber
,SourceUniqueID = aee.SourceUniqueID
,PreHospitalDetails = aee.ArrivalModeCode
,AmbulanceCrewPRF = aee.AmbulanceCrewPRF
,EDStay = CASE ISNULL(aee.arrivaldate,'') WHEN '' THEN 'No' ELSE 'Yes' END
,EDArrivalDate = aee.arrivaldate
,EDArrivalTime = aee.arrivaltime 
,EDdeparturedate = cast(aee.departuretime as date)
,EDdeparturetime = cast(aee.departuretime as time)
,TraumaTeam = CASE ISNULL(aee.arrivaldate,9999) WHEN 9999 THEN 'No' ELSE 'Yes' END
,EDDatePatientSeen = cast(aee.seenfortreatmenttime as date)
,EDTimePatientSeen = cast(aee.seenfortreatmenttime as time)
,EDSpecialty = aee.referredtospecialtycode

FROM TARN.Encounter TC
INNER join warehouse.ae.encounter aee
on	aee.DistrictNo = tc.DistrictNo
and	aee.DepartureTime between dateadd(hour , -6 , tc.AdmissionTime) and dateadd(hour , 6 , tc.AdmissionTime)
and	aee.ArrivalTime < tc.AdmissionTime
and	not exists
	(
	select
		1
	from
		warehouse.AE.Encounter LaterAEEncounter
	where
		LaterAEEncounter.DistrictNo = tc.DistrictNo
	and	LaterAEEncounter.DepartureTime between dateadd(hour , -6 , tc.AdmissionTime) and dateadd(hour , 6 , tc.AdmissionTime)
	and	LaterAEEncounter.ArrivalTime < tc.AdmissionTime
	and	LaterAEEncounter.AttendanceDisposalCode = '01'
	and LaterAEEncounter.SiteCode = 'RW3MR'
	and	LaterAEEncounter.ArrivalTime > aee.ArrivalTime
	)




UPDATE TARN.Encounter
SET OperationCount = (select count(*) from 
		warehouse.Theatre.PatientBooking pb
		inner join warehouse.Theatre.OperationDetail od
		on	od.PatientBookingSourceUniqueID = pb.SourceUniqueID
		and	dateadd(day, datediff(day, 0, od.OperationStartDate), 0) between apce.EpisodeStartDate and coalesce(apce.EpisodeEndDate, getdate())
		where pb.NHSNumber = tc.NHSNumber
		or pb.casenotenumber = tc.casenotenumber
		)
FROM TARN.Encounter TC
INNER JOIN warehouse.apc.encounter apce on tc.DistrictNo = apce.DistrictNo 
and apce.AdmissionTime = tc.AdmissionTime







----------------------------------------------------------------------------------------------------------
-- UPDATE EXISTING RECORDS WITH SYMPHONY DATA
----------------------------------------------------------------------------------------------------------


UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  Location = R.res_field1,
  TypeOfInjury = R.res_field2,
  MechanismOfInjury = R.res_field3,
  ProtectionInVehicle = R.res_field8,
  InjuryIntent = R.res_field4,
  AdditionalInjuryInfo = R.res_field7,
  TrappedAtScene = R.res_field5
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  --LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid



UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  Severe_open_fracture = CASE X.res_ef_fieldid WHEN 2065 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2065

UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  PositionInVI = CASE X.res_ef_fieldid WHEN 2033 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2033

UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  Weapon = CASE X.res_ef_fieldid WHEN 2056 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2056

UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  LOTimeTrapped = CASE X.res_ef_fieldid WHEN 2032 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2032

UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  PreHospitalDetails = CASE X.res_ef_fieldid WHEN 2024 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2024

UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  AttendantsAtLocation = CASE X.res_ef_fieldid WHEN 2025 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2025
  
UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  TeamLeaderSpecialty = CASE X.res_ef_fieldid WHEN 2063 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2063

UPDATE TARN.Encounter
  SET 
  TARN = 'Yes',
  EDAttendantsAtLocation = CASE X.res_ef_fieldid WHEN 2023 THEN X.res_ef_value END
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid and X.res_ef_fieldid = 2023



--UPDATE TARN.Encounter
--  SET 
--  TARN = 'Yes',
--  Severe_open_fracture = CASE X.res_ef_fieldid WHEN 2065 THEN X.res_ef_value END,
--  Location = R.res_field1,
--  TypeOfInjury = R.res_field2,
--  MechanismOfInjury = R.res_field3,
--  PositionInVI = CASE X.res_ef_fieldid WHEN 2033 THEN X.res_ef_value END,
--  ProtectionInVehicle = R.res_field8,
--  Weapon = CASE X.res_ef_fieldid WHEN 2056 THEN X.res_ef_value END,
--  InjuryIntent = R.res_field4,
--  AdditionalInjuryInfo = R.res_field7,
--  TrappedAtScene = R.res_field5,
--  LOTimeTrapped = CASE X.res_ef_fieldid WHEN 2032 THEN X.res_ef_value END,
--  PreHospitalDetails = CASE X.res_ef_fieldid WHEN 2024 THEN X.res_ef_value END,
--  AttendantsAtLocation = CASE X.res_ef_fieldid WHEN 2025 THEN X.res_ef_value END,
--  TraumaTeam = CASE ISNULL(res_result,9999) WHEN 9999 THEN 'No' ELSE 'Yes' END,
--  --TeamLeaderGrade = res_result,
--  TeamLeaderSpecialty = CASE X.res_ef_fieldid WHEN 2063 THEN X.res_ef_value END,
--  EDAttendantsAtLocation = CASE X.res_ef_fieldid WHEN 2023 THEN X.res_ef_value END
--  FROM TARN.Encounter TE
--  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
--  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
--  LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid








UPDATE TARN.Encounter
  SET PreHospitalDetails = lkp_name
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_Details_ExtraFields R ON TE.SourceUniqueID = R.res_EF_atdid
  INNER JOIN CMMC_Reports.dbo.lookups on res_ef_value =lkp_id
  WHERE res_EF_FieldID = 2024

UPDATE TARN.Encounter
  SET AttendantsAtLocation = lkp_name
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_Details_ExtraFields R ON TE.SourceUniqueID = R.res_EF_atdid
  INNER JOIN CMMC_Reports.dbo.lookups on res_ef_value =lkp_id
  WHERE res_EF_FieldID = 2025
  
  
  UPDATE TARN.Encounter
  SET TeamLeaderGrade = lkp_name
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 86
  INNER JOIN CMMC_Reports.dbo.staff on stf_staffid = res_staff1
  INNER JOIN CMMC_Reports.dbo.lookups on lkp_id = stf_systemproperty
  
  UPDATE TARN.Encounter
  SET 
  DatePatientSeen = res_date,
  TypeOfAttendant = res_result,
  Grade = res_field1,
  Specialty = res_field2,
  DateOfPreAlert = cast(res_date as date),
  TimeOfPreAlert = cast(res_date as time)
           
  FROM TARN.Encounter TE
  --INNER JOIN Warehouse.APC.Encounter APCE ON TE.DistrictNo = APCE.DistrictNo
  --INNER JOIN CMMC_Reports.dbo.Result_details R ON APCE.SourcePatientNo = R.res_atdid AND res_depid = 229
  --INNER JOIN Warehouse.AE.Encounter AE ON TE.DistrictNo = AE.DistrictNo
  INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 229
  AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
 
  
  UPDATE TARN.Encounter
  SET EDTypeOfAttendant = REPLACE(Left(Main.Grade,Len(Main.Grade)-1),'&amp;','&')
  ,EDAttendantsAtLocation = 'Yes'
  FROM TARN.Encounter TE
  INNER JOIN 
(Select distinct TE2.SourceUniqueID, TE2.DistrictNO, R2.Res_date,
          (Select lkp1.lkp_name + ',' AS [text()]
		  FROM TARN.Encounter TE1
		  INNER JOIN CMMC_Reports.dbo.Result_details R1 ON TE1.SourceUniqueID = R1.res_atdid AND R1.res_depid = 228
		  AND te1.arrivaldate between dateadd(hour , -48 , R1.Res_date) and dateadd(hour , 48 , R1.Res_date)
		  INNER JOIN CMMC_Reports.dbo.lookups lkp1 on lkp1.lkp_id = R1.res_field2
--		  INNER JOIN CMMC_Reports.dbo.staff S1 on S1.stf_staffid = R1.res_staff1
          Where TE1.SourceUniqueID = TE2.SourceUniqueID
          ORDER BY TE1.SourceUniqueID
          For XML PATH ('')  
            ) [Grade]
  FROM TARN.Encounter TE2
  INNER JOIN CMMC_Reports.dbo.Result_details R2 ON TE2.SourceUniqueID = R2.res_atdid AND R2.res_depid = 228
  AND te2.arrivaldate between dateadd(hour , -48 , R2.Res_date) and dateadd(hour , 48 , R2.Res_date)
  INNER JOIN CMMC_Reports.dbo.lookups lkp2 on lkp2.lkp_id = R2.res_field2
--  INNER JOIN CMMC_Reports.dbo.staff S2 on S2.stf_staffid = R2.res_staff1
 ) [Main] 
  ON TE.SourceUniqueID = Main.SourceUniqueID
  AND TE.arrivaldate between dateadd(hour , -48 , Main.Res_date) and dateadd(hour , 48 , Main.Res_date)
  
  
    UPDATE TARN.Encounter
  SET EDGrade = REPLACE(Left(Main.Grade,Len(Main.Grade)-1),'&amp;','&')
  FROM TARN.Encounter TE
  INNER JOIN 
(Select distinct TE2.SourceUniqueID, TE2.DistrictNO, R2.Res_date,
          (Select lkp1.lkp_name + ',' AS [text()]
		  FROM TARN.Encounter TE1
		  INNER JOIN CMMC_Reports.dbo.Result_details R1 ON TE1.SourceUniqueID = R1.res_atdid AND R1.res_depid = 228
		  AND te1.arrivaldate between dateadd(hour , -48 , R1.Res_date) and dateadd(hour , 48 , R1.Res_date)
		  INNER JOIN CMMC_Reports.dbo.staff S1 on S1.stf_staffid = R1.res_staff1
		  INNER JOIN CMMC_Reports.dbo.lookups lkp1 on lkp1.lkp_id = S1.stf_systemproperty
          Where TE1.SourceUniqueID = TE2.SourceUniqueID
          ORDER BY TE1.SourceUniqueID
          For XML PATH ('')  
            ) [Grade]
  FROM TARN.Encounter TE2
  INNER JOIN CMMC_Reports.dbo.Result_details R2 ON TE2.SourceUniqueID = R2.res_atdid AND R2.res_depid = 228
  AND te2.arrivaldate between dateadd(hour , -48 , R2.Res_date) and dateadd(hour , 48 , R2.Res_date)
  INNER JOIN CMMC_Reports.dbo.staff S2 on S2.stf_staffid = R2.res_staff1
  INNER JOIN CMMC_Reports.dbo.lookups lkp2 on lkp2.lkp_id = S2.stf_systemproperty) [Main] 
  ON TE.SourceUniqueID = Main.SourceUniqueID
  AND TE.arrivaldate between dateadd(hour , -48 , Main.Res_date) and dateadd(hour , 48 , Main.Res_date)
  
  
  
  --UPDATE TARN.Encounter
  --SET 
  --EDTypeOfAttendant = CASE res_result WHEN 0 THEN NULL ELSE res_result END,
  --EDGrade = CASE res_field1 WHEN 0 THEN NULL ELSE res_field1 END
  ----EDTraining
  --FROM TARN.Encounter TE
  ----INNER JOIN Warehouse.APC.Encounter APCE ON TE.DistrictNo = APCE.DistrictNo
  ----INNER JOIN CMMC_Reports.dbo.Result_details R ON APCE.SourcePatientNo = R.res_atdid AND res_depid = 228
  ----INNER JOIN Warehouse.AE.Encounter AE ON TE.DistrictNo = AE.DistrictNo
  --INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 228
  --AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)








  UPDATE TARN.Encounter
  SET CADNumber = atd_ambcrew
  ,SceneDepartureDate = A.atd_dischdate
  FROM TARN.Encounter TE
  INNER JOIN CMMC_Reports.dbo.Aud_Attendance_Details A ON TE.SourceUniqueID = A.atd_id --AND res_depid = 228
  AND te.arrivaldate between dateadd(hour , -48 , A.atd_arrivaldate) and dateadd(hour , 48 , A.atd_arrivaldate)


----------------------------------------------------------------------------------------------------------
-- UPDATE EXISTING RECORDS WITH MAJORTAM DATA
----------------------------------------------------------------------------------------------------------

	UPDATE TARN.Encounter
	SET
	MajorTraumaTriage =details.value('(/PreHospitalDTO/MajorTraumaTriage)[1]', 'varchar(20)')
	
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 13 AND ActionID = 99


	UPDATE TARN.Encounter
	SET TransferTypeID = Mt.TransferTypeID
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated

	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,
	
	Xray = CASE ActionID WHEN 4 THEN 'Yes' END,
	XrayDate = CASE ActionID WHEN 4 THEN details.value('(/ImagingDTO/ImageDate)[1]', 'date') END,
	XrayTime = CASE ActionID WHEN 4 THEN details.value('(/ImagingDTO/ImageTime)[1]', 'time') END,
	XrayBodyRegion = CASE ActionID WHEN 4 THEN details.value('(/ImagingDTO/BodyRegion)[1]', 'varchar(10)') END,
	XrayMethodOfImageTransfer = CASE ActionID WHEN 4 THEN details.value('(/ImagingDTO/MethodofImageTransfer)[1]', 'varchar(10)') END,
	XrayReport = CASE ActionID WHEN 4 THEN details.value('(/ImagingDTO/Report)[1]', 'varchar(255)') END,
	XrayReportedRadiologist = CASE ActionID WHEN 4 THEN details.value('(/ImagingDTO/ReportedbySeniorRadiologist)[1]', 'varchar(10)') END,
	XrayOtherNotes = CASE ActionID WHEN 4 THEN details.value('(/ImagingDTO/OtherNotes)[1]', 'varchar(MAX)') END
	
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 3 AND ActionID = 4




	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,

	CTScan = CASE ActionID WHEN 5 THEN 'Yes' END,
	CTScanDate = CASE ActionID WHEN 5 THEN details.value('(/ImagingDTO/ImageDate)[1]', 'date') END,
	CTScanTime = CASE ActionID WHEN 5 THEN details.value('(/ImagingDTO/ImageTime)[1]', 'time') END,
	CTScanBodyRegion = CASE ActionID WHEN 5 THEN details.value('(/ImagingDTO/BodyRegion)[1]', 'varchar(10)') END,
	CTScanMethodOfImageTransfer = CASE ActionID WHEN 5 THEN details.value('(/ImagingDTO/MethodofImageTransfer)[1]', 'varchar(10)') END,
	CTScanReport = CASE ActionID WHEN 5 THEN details.value('(/ImagingDTO/Report)[1]', 'varchar(255)') END,
	CTScanReportedRadiologist = CASE ActionID WHEN 5 THEN details.value('(/ImagingDTO/ReportedbySeniorRadiologist)[1]', 'varchar(10)') END,
	CTScanOtherNotes = CASE ActionID WHEN 5 THEN details.value('(/ImagingDTO/OtherNotes)[1]', 'varchar(MAX)') END
	
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 3 AND ActionID = 5


	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,

	Ultrasound = CASE ActionID WHEN 6 THEN 'Yes' END,
	UltrasoundDate = CASE ActionID WHEN 6 THEN details.value('(/ImagingDTO/ImageDate)[1]', 'date') END,
	UltrasoundTime = CASE ActionID WHEN 6 THEN details.value('(/ImagingDTO/ImageTime)[1]', 'time') END,
	UltrasoundBodyRegion = CASE ActionID WHEN 6 THEN details.value('(/ImagingDTO/BodyRegion)[1]', 'varchar(10)') END,
	UltrasoundMethodOfImageTransfer = CASE ActionID WHEN 6 THEN details.value('(/ImagingDTO/MethodofImageTransfer)[1]', 'varchar(10)') END,
	UltrasoundReport = CASE ActionID WHEN 6 THEN details.value('(/ImagingDTO/Report)[1]', 'varchar(255)') END,
	UltrasoundReportedByRadiologist = CASE ActionID WHEN 6 THEN details.value('(/ImagingDTO/ReportedbySeniorRadiologist)[1]', 'varchar(10)') END,	
	UltrasoundOtherNotes = CASE ActionID WHEN 6 THEN details.value('(/ImagingDTO/OtherNotes)[1]', 'varchar(MAX)') END
	
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 3 AND ActionID = 6



	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,

	FastScan = CASE ActionID WHEN 7 THEN 'Yes' END,
	FastScanDate = CASE ActionID WHEN 7 THEN details.value('(/ImagingDTO/ImageDate)[1]', 'date') END,
	FastScanTime = CASE ActionID WHEN 7 THEN details.value('(/ImagingDTO/ImageTime)[1]', 'time') END,
	FastScanReport = CASE ActionID WHEN 7 THEN details.value('(/ImagingDTO/Report)[1]', 'varchar(255)') END,
	FastScanReportedByRadiologist = CASE ActionID WHEN 7 THEN details.value('(/ImagingDTO/ReportedbySeniorRadiologist)[1]', 'varchar(10)') END,	
	FastScanOtherNotes = CASE ActionID WHEN 7 THEN details.value('(/ImagingDTO/OtherNotes)[1]', 'varchar(MAX)') END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 3 AND ActionID = 7	


	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,
	
	OtherImaging = CASE ActionID WHEN 8 THEN 'Yes' END,
	APJudetOblique = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/APJudet)[1]', 'varchar(10)') END,
	APJudetObliqueMethodOfImageTransfer = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/APJudetMethodofImageTransfer)[1]', 'varchar(10)') END,
	APJudetObliqueDate = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/APJudetDate)[1]', 'date') END,
	APJudetObliqueTime = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/APJudetTime)[1]', 'time') END,
	APJudetObliqueReport = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/APJudetReport)[1]', 'varchar(255)') END,
	MRIScan = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/MRIScan)[1]', 'varchar(10)') END,
	MRIScanBodyRegion = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/MRIScanBodyRegion)[1]', 'varchar(10)') END,
	MRIScanMethodOfImageTransfer = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/MRIScanMethodofImageTransfer)[1]', 'varchar(10)') END,
	MRIScanDate = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/MRIScanDate)[1]', 'date') END,
	MRIScanTime = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/MRIScanTime)[1]', 'time') END,
	MRIScanReport = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/Report)[1]', 'varchar(max)') END,
	OtherImagingOtherNotes = CASE ActionID WHEN 8 THEN details.value('(/ImagingDTO/OtherNotes)[1]', 'varchar(MAX)') END

	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 3 AND ActionID = 8




	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,

	DetailedInjuryDescriptions = details.value('(/AtDischargeDTO/DetailedInjuryDesc)[1]', 'varchar(255)'),
	Complications = details.value('(/AtDischargeDTO/Complications)[1]', 'varchar(10)'),
	DVT = details.value('(/AtDischargeDTO/DVT)[1]', 'varchar(10)'),
	DU = details.value('(/AtDischargeDTO/DU)[1]', 'varchar(10)'),
	PE = details.value('(/AtDischargeDTO/PE)[1]', 'varchar(10)'),
	MultiOrganFailure = details.value('(/AtDischargeDTO/MultiOrganFailure)[1]', 'varchar(10)'),
	OtherComplications = details.value('(/AtDischargeDTO/OtherComplications)[1]', 'varchar(10)'),
	PreExistingMedicalConditions = details.value('(/AtDischargeDTO/PreExisting)[1]', 'varchar(10)'),

	CauseOfDeath = details.value('(/AtDischargeDTO/CauseofDeath)[1]', 'varchar(10)'),

	--DaysIntubated

	GlasgowOutcomeScale = details.value('(/AtDischargeDTO/GOS)[1]', 'varchar(10)'),
	WhenGOSRecorded = details.value('(/AtDischargeDTO/GOSRecorded)[1]', 'varchar(10)'),
 	AtDischargeOtherNotes = details.value('(/AtDischargeDTO/OtherNotes)[1]', 'varchar(max)')
 
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 4 AND ActionID = 99





	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,

	SurgeonGrade = details.value('(/OperationDTO/GradeofSurgeon)[1]', 'varchar(10)'),
	SurgeonSpecialty = details.value('(/OperationDTO/SpecialityofSurgeon)[1]', 'varchar(10)'),
	supervisingsurgeon1code = details.value('(/OperationDTO/SupervisorPresent)[1]', 'varchar(10)'),
	SupervisorGrade = details.value('(/OperationDTO/GradeofSupervisor)[1]', 'varchar(10)'),
	AnaesthetistGrade = details.value('(/OperationDTO/GradeofAnaesthetist)[1]', 'varchar(10)'),
	OperationOtherNotes = details.value('(/OperationDTO/OtherNotes)[1]', 'varchar(max)')
	
	--OPAttendants
	--OPDateSeen
	--OPTimeSeen
	--OPTypeofAttendant
	--OPGrade
	--OPSpecialty
	--OPTraining


	--DaysIntubated
  
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 6 AND ActionID = 3
	



	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,


	FractureContaminated = details.value('(/BOAST4DTO/FractureContaminated)[1]', 'varchar(10)'),
	ContaminationType = details.value('(/BOAST4DTO/ContaminationType)[1]', 'varchar(10)'),
	CombinedOrthopaedicPlan = details.value('(/BOAST4DTO/OrthopaedicPlasticSurgery)[1]', 'varchar(10)'),
	SystematicAssessment = details.value('(/BOAST4DTO/VascularNeurologicalStatus)[1]', 'varchar(10)'),
	VascularImpairment = details.value('(/BOAST4DTO/VascularImpairment)[1]', 'varchar(10)'),
	Antibiotics = details.value('(/BOAST4DTO/Antibiotics)[1]', 'varchar(10)'),
	AntibioticType = details.value('(/BOAST4DTO/AntibioticType)[1]', 'varchar(10)'),
	WoundDressing = details.value('(/BOAST4DTO/WoundDressing)[1]', 'varchar(10)'),
	WoundDressingType = details.value('(/BOAST4DTO/DressingType)[1]', 'varchar(10)'),
	LimbSplint = details.value('(/BOAST4DTO/LimbSplintDate)[1]', 'varchar(10)'),
	AnkleKneeSplint = details.value('(/BOAST4DTO/AnkleKneeSplint)[1]', 'varchar(10)')
  
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID = 7 AND ActionID = 99
	




	UPDATE TARN.Encounter
	SET 
	TransferInReason = TransferReasonID,
	PreviousHospital = PreviousHospitalID,
	TransferOutReason = TransferOutReasonID,
	NextHospital = NextHospitalID,
	Observations = Null,
	Interventions =  Null,

	CCAttendants = AT.Description,
	CCDateSeen = details.value('(/AttendantDTO/PatientSeenDate)[1]', 'varchar(10)'),
	CCTimeSeen = details.value('(/AttendantDTO/PatientSeenTime)[1]', 'varchar(10)'),
	CCTypeofAttendant = details.value('(/AttendantDTO/AttendantType)[1]', 'varchar(10)')
	--CCGrade = details.value('(/AttendantDTO/Attendants)[1]', 'varchar(10)'),
	--CCSpecialty = details.value('(/AttendantDTO/Attendants)[1]', 'varchar(10)'),
	--CCTraining = details.value('(/AttendantDTO/Attendants)[1]', 'varchar(10)')
  
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	LEFT JOIN MAJORTAM.dbo.AttendantType AT ON details.value('(/AttendantDTO/AttendantType)[1]', 'int') = AT.ID
	WHERE SectionID = 2 AND ActionID = 9



----------------------------------------------------------------------------------------------------------
-- UPDATE EXISTING RECORDS WITH REHAB PRESCRIPTION DATA
----------------------------------------------------------------------------------------------------------

	;WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)
	UPDATE TARN.Encounter SET
	RehabilitationPrescription = RF.FormXML.value('(DYN:InfoPathSubmission/DYN:TARNMDS/DYN:TARNMDS_RP)[1]', 'VARCHAR(40)')
	,PhysicalFactors = RF.FormXML.value('(DYN:InfoPathSubmission/DYN:TARNMDS/DYN:TARNMDS_PhysFac)[1]', 'VARCHAR(40)')
	,CognitiveMoodFactors = RF.FormXML.value('(DYN:InfoPathSubmission/DYN:TARNMDS/DYN:TARNMDS_CM)[1]', 'VARCHAR(40)')
	,PsychosocialFactors  = RF.FormXML.value('(DYN:InfoPathSubmission/DYN:TARNMDS/DYN:TARNMDS_Psychosocial)[1]', 'VARCHAR(40)')
	,PrescriptionDetails = RF.FormXML.value('(DYN:InfoPathSubmission/DYN:Patient/DYN:RehabilitationPrescription/DYN:RP_ServicesReferredTo)[1]', 'VARCHAR(40)')
	FROM TARN.Encounter TE
	INNER JOIN Information.Information.RehabPrescriptions RP ON TE.NHSNumber = RP.NHSNumber
	INNER JOIN infopathrepository.infopath.ReceivedForms RF ON RP.ID = RF.ID
	WHERE formtypeid = 310

----------------------------------------------------------------------------------------------------------
-- UPDATE OBSERVATIONS
----------------------------------------------------------------------------------------------------------



	INSERT INTO TARN.Observations
           ([Encounter_Row_ID]
           ,[Observation_Type]
           ,[DistrictNo]
           ,[NHSNumber]
           ,DateTimeCreated
           ,[Respiratory_observations]
           ,[RO_Date]
           ,[RO_Time]
           ,[Airway_status_on_arrival]
           ,[Choose_status_of_airway]
           ,[Breathing_status_on_arrival]
           ,[Choose_status_of_breathing]
           ,[Oxygen_saturation]
           ,[Enter_saturation]
           ,[Unassisted_Respiratory_rate]
           ,[Enter_unassisted_respiratory_rate]
           ,[Circulation_observations]
           ,[CO_Date]
           ,[CO_Time]
           ,[Pulse_rate]
           ,[Enter_pulse_rate]
           ,[Blood_pressure]
           ,[Enter_systolic_BP_value]
           ,[Enter_diastolic_BP_value]
           ,[Nervous_System_observations]
           ,[NSO_Date]
           ,[NSO_Time]
           ,[Glasgow_Coma_Scale]
           ,[Eye_score]
           ,[Verbal_score]
           ,[Motor_score]
           ,[Total_GCS_score]
           ,[Pupil_size]
           ,[Left_eye_pupil_size]
           ,[Right_eye_pupil_size]
           ,[Pupil_reactivity]
           ,[Left_eye_pupil_reactivity]
           ,[Right_eye_pupil_reactivity])

	SELECT DISTINCT
	TE.Row_ID
	,SectionID
    --,TE.attendancenumber
    ,TE.DistrictNo
    ,TE.NHSNumber	
    ,MTD.DateTimeCreated
    ,CASE SectionID WHEN 1 THEN EDStay 
					WHEN 2 THEN CCCriticalCareStay END
	,details.value('(/ObservationsDTO/RespirationDate)[1]', 'varchar(50)')
	,details.value('(/ObservationsDTO/RespirationTime)[1]', 'varchar(50)')
	,details.value('(/ObservationsDTO/Airway)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/AirwayStatus)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/Breathing)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/BreathingStatus)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/OxygenSaturation)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/OxygenSaturationVal)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/UnassistedRespiratoryRate)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/UnassistedRespiratoryRateVal)[1]', 'varchar(50)')	
    ,CASE SectionID WHEN 1 THEN EDStay 
					WHEN 2 THEN CCCriticalCareStay END
	,details.value('(/ObservationsDTO/CirculationDate)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/CirculationTime)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PulseRate)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PulseRateVal)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/BloodPressure)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/SystolicVal)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/DiastolicVal)[1]', 'varchar(50)')	
    ,CASE SectionID WHEN 1 THEN EDStay 
					WHEN 2 THEN CCCriticalCareStay END
	,details.value('(/ObservationsDTO/NervousSystemDate)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/NervousSystemTime)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/GlasgowComaStatus)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/GCSEyeScore)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/GCSVerbalScore)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/GCSMotorScore)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/GCSTotalScore)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PupilSize)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PupilSizeLeft)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PupilSizeRight)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PupilReactivity)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PupilReactivityLeft)[1]', 'varchar(50)')	
	,details.value('(/ObservationsDTO/PupilReactivityRight)[1]', 'varchar(50)')	
	

	
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID IN (1,2) AND ActionID = 1
	




----------------------------------------------------------------------------------------------------------
-- UPDATE INTERVENTIONS
----------------------------------------------------------------------------------------------------------




	
	
	INSERT INTO [WarehouseReporting].[TARN].[Interventions]
           ([Encounter_Row_ID]
           ,[Intervention_Type]
           ,[DistrictNo]
           ,[NHSNumber]
           ,DateTimeCreated
           ,[Airway_Support]
           ,[Date_of_Airway_Support]
           ,[Time_of_Airway_Support]
           ,[Type_of_Airway_Support]
           ,[Extubation]
           ,[Date_of_Extubation]
           ,[Time_of_Extubation]
           ,[Breathing_support]
           ,[Date_of_Breathing_support]
           ,[Time_of_Breathing_support]
           ,[Select_type_of_breathing_support]
           ,[Spinal_protection]
           ,[Date_of_Spinal_Protection]
           ,[Time_of_Spinal_Protection]
           ,[Type_of_spinal_protection]
           ,[Spinal_protection_removed]
           ,[Date_spinal_protection_removed]
           ,[Time_spinal_protection_removed]
           ,[Blood_Products_in_the_first_24_hours]
           ,[Date_of_blood_product]
           ,[Start_time_of_blood_product]
           ,[Finish_time_of_blood_product]
           ,[Blood_Product]
           ,[Units_given_at_this_time]
           ,[Embolisation_Interventional_Radiology]
           ,[Date_of_embolisation]
           ,[Time_of_embolisation]
           ,[Embolisation_Fluid]
           ,[Date_of_Fluid]
           ,[Start_time_of_Fluid]
           ,[Finish_time_of_Fluid]
           ,[Fluid]
           ,[Volume_of_fluid_given_ml]
           ,[Chest_Drain_NOT_needle_thoracocentesis]
           ,[Date_of_chest_drain]
           ,[Time_of_chest_drain]
           ,[Tranexamic_Acid]
           ,[Date_of_Tranexamic_Acid]
           ,[Time_of_Tranexamic_Acid])
           
	SELECT DISTINCT
	TE.Row_ID
	,SectionID
    ,TE.DistrictNo
    ,TE.NHSNumber	
    ,MTD.DateTimeCreated
    --,CASE SectionID WHEN 1 THEN EDStay 
				--	WHEN 2 THEN CCCriticalCareStay END
	,details.value('(/InterventionsDTO/AirwaySupport)[1]', 'varchar(50)')				
	,CASE details.value('(/InterventionsDTO/AirwaySupportDate)[1]', 'varchar(50)') WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/AirwaySupportDate)[1]', 'varchar(50)') END
	,details.value('(/InterventionsDTO/AirwaySupportTime)[1]', 'varchar(50)')
	,details.value('(/InterventionsDTO/AirwaySupportType)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/Extubation)[1]', 'varchar(50)')	
	,CASE details.value('(/InterventionsDTO/ExtubationDate)[1]', 'varchar(50)')	WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/ExtubationDate)[1]', 'varchar(50)') END 
	,CASE details.value('(/InterventionsDTO/ExtubationDate)[1]', 'varchar(50)') WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/ExtubationDate)[1]', 'varchar(50)') END
	,details.value('(/InterventionsDTO/BreathingSupport)[1]', 'varchar(50)')	
	,CASE details.value('(/InterventionsDTO/BreathingSupportDate)[1]', 'varchar(50)') WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/BreathingSupportDate)[1]', 'varchar(50)') END
	,details.value('(/InterventionsDTO/BreathingSupportTime)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/BreathingSupportType)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/SpinalProtection)[1]', 'varchar(50)')	
	,CASE details.value('(/InterventionsDTO/SpinalProtectionDate)[1]', 'varchar(50)')	WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/SpinalProtectionDate)[1]', 'varchar(50)') END
	,details.value('(/InterventionsDTO/SpinalProtectionTime)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/SpinalProtectionTypes)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/SpinalProtectionRemoved)[1]', 'varchar(50)')	
	,CASE details.value('(/InterventionsDTO/SpinalProtectionRemovedDate)[1]', 'varchar(50)') WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/SpinalProtectionRemovedDate)[1]', 'varchar(50)') END
	,details.value('(/InterventionsDTO/SpinalProtectionRemovedTime)[1]', 'varchar(50)')	
		
	,details.value('(/InterventionsDTO/BloodProduct)[1]', 'varchar(50)')	
	,CASE details.value('(/InterventionsDTO/BloodProductDate)[1]', 'varchar(50)') WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/BloodProductDate)[1]', 'varchar(50)') END
	,details.value('(/InterventionsDTO/BloodProductStartTime)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/BloodProductFinishTime)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/BloodProductTypes)[1]', 'varchar(50)')		
	,details.value('(/InterventionsDTO/BloodProductUnits)[1]', 'int')			
	
	,details.value('(/InterventionsDTO/Embolisation)[1]', 'varchar(50)')	
	,CASE details.value('(/InterventionsDTO/EmbolisationDate)[1]', 'varchar(50)') WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/EmbolisationDate)[1]', 'varchar(50)') END
	,details.value('(/InterventionsDTO/EmbolisationTime)[1]', 'varchar(50)')	

	,details.value('(/InterventionsDTO/Fluid)[1]', 'varchar(50)')	
	,CASE details.value('(/InterventionsDTO/FluidDate)[1]', 'varchar(50)')  WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/FluidDate)[1]', 'varchar(50)')  END	
	,details.value('(/InterventionsDTO/FluidStartTime)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/FluidFinishTime)[1]', 'varchar(50)')	
	,details.value('(/InterventionsDTO/FluidTypes)[1]', 'varchar(50)')		
	,details.value('(/InterventionsDTO/FluidVolume)[1]', 'int')
	
	,details.value('(/InterventionsDTO/ChestDrain)[1]', 'varchar(50)')
	,CASE details.value('(/InterventionsDTO/ChestDrainDate)[1]', 'varchar(50)')  WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/ChestDrainDate)[1]', 'varchar(50)')  END
	,details.value('(/InterventionsDTO/ChestDrainTime)[1]', 'varchar(50)')	
	
	,details.value('(/InterventionsDTO/TranexamicAcid)[1]', 'varchar(50)')
	,CASE details.value('(/InterventionsDTO/TranexamicAcidDate)[1]', 'varchar(50)')  WHEN '' THEN NULL
	ELSE details.value('(/InterventionsDTO/TranexamicAcidDate)[1]', 'varchar(50)')  END
	,details.value('(/InterventionsDTO/TranexamicAcidTime)[1]', 'varchar(50)')	           

	
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	WHERE SectionID IN (1,2) AND ActionID = 2




	-- SET Days Intubated from Interventions
	UPDATE TARN.Encounter
	SET DaysIntubated = ISNULL(DATEDIFF(dd, (SELECT MIN(Date_of_Airway_Support) FROM TARN.Interventions I1
						WHERE E.DistrictNo = I1.DistrictNo AND Type_of_Airway_Support = 3),
						(SELECT MIN(Date_of_Extubation) FROM TARN.Interventions I2
						WHERE E.DistrictNo = I2.DistrictNo)),0)
	FROM TARN.Encounter E

	-- SET Critical Care Length of Stay
	--UPDATE TARN.Encounter
	--SET CCLOS = ISNULL(DATEDIFF(dd, (SELECT MIN(WSArrivalDate) FROM TARN.Encounter E1
	--					WHERE E.DistrictNo = E1.DistrictNo),
	--					(SELECT MAX(WSDepartureDate) FROM TARN.Encounter E2
	--					WHERE E.DistrictNo = E2.DistrictNo)),0)
	--FROM TARN.Encounter E
	
	-- SET Critical Care Length of Stay
	UPDATE TARN.Encounter
	SET LOSCriticalCare = ISNULL(DATEDIFF(dd, (SELECT MIN(WSArrivalDate) FROM TARN.Encounter E1
						WHERE E.DistrictNo = E1.DistrictNo),
						(SELECT MAX(WSDepartureDate) FROM TARN.Encounter E2
						WHERE E.DistrictNo = E2.DistrictNo)),0)
	FROM TARN.Encounter E	





	-- APPLY CODING
	
	UPDATE TARN.Encounter
	SET ArrivalModeCode = AM.ArrivalMode
	FROM TARN.Encounter E
	INNER JOIN Warehouse.AE.ArrivalMode AM ON E.ArrivalModeCode = AM.ArrivalModeCode

	--UPDATE TARN.Encounter
	--SET GPPractice = GP.Address
	--FROM TARN.Encounter E
	--INNER JOIN Warehouse.PAS.GpBase GP ON E.GPPractice = GP.PracticeCode

	UPDATE TARN.Encounter
	SET AmbulanceCrewPRF = CASE AmbulanceCrewPRF WHEN Null THEN 'No' ELSE 'Yes' END
	FROM TARN.Encounter E
	WHERE ArrivalModeCode in ('Ambulance','Helicopter')

	UPDATE TARN.Encounter
	SET AmbulanceCrewPRF = 'N/A'
	FROM TARN.Encounter E
	WHERE ArrivalModeCode not in ('Ambulance','Helicopter')
	
	UPDATE TARN.Encounter
	SET AmbulanceCrewPRF = 'N/A'
	FROM TARN.Encounter E
	WHERE ArrivalModeCode not in ('Ambulance','Helicopter')	
	
  	UPDATE TARN.Encounter
	SET Specialty = LK.Lkp_Name
	FROM TARN.Encounter E
	INNER JOIN CMMC_Reports.dbo.Lookups LK ON E.Specialty = LK.Lkp_ID
	WHERE Lkp_ParentID = 675
	
  	UPDATE TARN.Encounter
	SET EDSpecialty = LK.Lkp_Name
	FROM TARN.Encounter E
	INNER JOIN CMMC_Reports.dbo.Lookups LK ON E.EDSpecialty = LK.Lkp_ID
	WHERE Lkp_ParentID = 675	

  	UPDATE TARN.Encounter
	SET Severe_open_fracture = LK.Lkp_Name
	FROM TARN.Encounter E
	INNER JOIN CMMC_Reports.dbo.Lookups LK ON E.Severe_open_fracture = LK.Lkp_ID
	WHERE Lkp_ParentID = 500

	
  	UPDATE TARN.Encounter
	SET --AdditionalInjuryInfo = LK1.Lkp_Name
	InjuryIntent = LK2.Lkp_Name
	,Location = LK3.Lkp_Name
	,MechanismOfInjury = LK4.Lkp_Name
	,TrappedAtScene = LK5.Lkp_Name
	,TypeOfInjury = LK6.Lkp_Name
	,TypeOfAttendant = LK7.Lkp_Name
	,Grade = LK8.Lkp_Name
	--,TeamLeaderGrade = LK9.Lkp_Name
	FROM TARN.Encounter E
	--LEFT JOIN CMMC_Reports.dbo.Lookups LK1 ON E.AdditionalInjuryInfo = LK1.Lkp_ID AND LK1.Lkp_ParentID = 18001
	LEFT JOIN CMMC_Reports.dbo.Lookups LK2 ON E.InjuryIntent = LK2.Lkp_ID AND LK2.Lkp_ParentID = 18095
	LEFT JOIN CMMC_Reports.dbo.Lookups LK3 ON E.Location = LK3.Lkp_ID AND LK3.Lkp_ParentID = 18080	
	LEFT JOIN CMMC_Reports.dbo.Lookups LK4 ON E.MechanismOfInjury = LK4.Lkp_ID AND LK4.Lkp_ParentID = 18119
	LEFT JOIN CMMC_Reports.dbo.Lookups LK5 ON E.TrappedAtScene = LK5.Lkp_ID AND LK5.Lkp_ParentID = 18170
	LEFT JOIN CMMC_Reports.dbo.Lookups LK6 ON E.TypeOfInjury = LK6.Lkp_ID AND LK6.Lkp_ParentID = 18104
	LEFT JOIN CMMC_Reports.dbo.Lookups LK7 ON E.TypeOfAttendant = LK7.Lkp_ID AND LK7.Lkp_ParentID = 18033			
	LEFT JOIN CMMC_Reports.dbo.Lookups LK8 ON E.Grade = LK8.Lkp_ID AND LK8.Lkp_ParentID = 2026				
	--LEFT JOIN CMMC_Reports.dbo.Lookups LK9 ON E.TeamLeaderGrade = LK9.Lkp_ID AND LK9.Lkp_ParentID = 2026

			
	UPDATE TARN.Encounter
	SET AdditionalInjuryInfo = LK1.Lkp_Name
	FROM TARN.Encounter TE
	INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
	AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
	LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid
	INNER JOIN CMMC_Reports.dbo.Lookups LK1 ON substring(replace(R.res_field7,'|',''),1,5) = LK1.Lkp_ID AND LK1.Lkp_ParentID = 18001			

	UPDATE TARN.Encounter
	SET AdditionalInjuryInfo = AdditionalInjuryInfo + N', ' + LK1.Lkp_Name
	FROM TARN.Encounter TE
	INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
	AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
	LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid
	INNER JOIN CMMC_Reports.dbo.Lookups LK1 ON substring(replace(R.res_field7,'|',''),6,5) = LK1.Lkp_ID AND LK1.Lkp_ParentID = 18001
	

	UPDATE TARN.Encounter
	SET AdditionalInjuryInfo = AdditionalInjuryInfo + N', ' + LK1.Lkp_Name
	FROM TARN.Encounter TE
	INNER JOIN CMMC_Reports.dbo.Result_details R ON TE.SourceUniqueID = R.res_atdid AND res_depid = 227
	AND te.arrivaldate between dateadd(hour , -48 , R.Res_date) and dateadd(hour , 48 , R.Res_date)
	LEFT JOIN CMMC_Reports.dbo.Result_Details_ExtraFields X ON R.res_resid = X.res_ef_recordid
	INNER JOIN CMMC_Reports.dbo.Lookups LK1 ON substring(replace(R.res_field7,'|',''),11,5) = LK1.Lkp_ID AND LK1.Lkp_ParentID = 18001


----- XRay Body Region multiple entries ----------------			
  	UPDATE TARN.Encounter
	SET XrayBodyRegion = CASE ActionID WHEN 4 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[1]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 4

  	UPDATE TARN.Encounter
	SET XrayBodyRegion = XrayBodyRegion  + ', ' + CASE ActionID WHEN 4 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[2]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 4 AND details.value('(/ImagingDTO/BodyRegion/int)[2]', 'int') IS NOT NULL
	
	UPDATE TARN.Encounter
	SET XrayBodyRegion = XrayBodyRegion  + ', ' + CASE ActionID WHEN 4 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[3]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 4 AND details.value('(/ImagingDTO/BodyRegion/int)[3]', 'int') IS NOT NULL
	
	UPDATE TARN.Encounter
	SET XrayBodyRegion = XrayBodyRegion  + ', ' + CASE ActionID WHEN 4 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[4]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 4 AND details.value('(/ImagingDTO/BodyRegion/int)[4]', 'int') IS NOT NULL

	UPDATE TARN.Encounter
	SET XrayBodyRegion = XrayBodyRegion  + ', ' + CASE ActionID WHEN 4 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[5]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 4 AND details.value('(/ImagingDTO/BodyRegion/int)[5]', 'int') IS NOT NULL



----- CT Scan Body Region multiple entries ----------------			
  	UPDATE TARN.Encounter
	SET CTScanBodyRegion = CASE ActionID WHEN 5 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[1]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 5

  	UPDATE TARN.Encounter
	SET CTScanBodyRegion = CTScanBodyRegion  + ', ' + CASE ActionID WHEN 5 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[2]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 5 AND details.value('(/ImagingDTO/BodyRegion/int)[2]', 'int') IS NOT NULL
	
	UPDATE TARN.Encounter
	SET CTScanBodyRegion = CTScanBodyRegion  + ', ' + CASE ActionID WHEN 5 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[3]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 5 AND details.value('(/ImagingDTO/BodyRegion/int)[3]', 'int') IS NOT NULL
	
	UPDATE TARN.Encounter
	SET CTScanBodyRegion = CTScanBodyRegion  + ', ' + CASE ActionID WHEN 5 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[4]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 5 AND details.value('(/ImagingDTO/BodyRegion/int)[4]', 'int') IS NOT NULL

	UPDATE TARN.Encounter
	SET CTScanBodyRegion = CTScanBodyRegion  + ', ' + CASE ActionID WHEN 5 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[5]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 5 AND details.value('(/ImagingDTO/BodyRegion/int)[5]', 'int') IS NOT NULL
	
	
	
----- Ultrasound Body Region multiple entries ----------------			
  	UPDATE TARN.Encounter
	SET UltrasoundBodyRegion = CASE ActionID WHEN 6 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[1]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 6

  	UPDATE TARN.Encounter
	SET UltrasoundBodyRegion = UltrasoundBodyRegion  + ', ' + CASE ActionID WHEN 6 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[2]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 6 AND details.value('(/ImagingDTO/BodyRegion/int)[2]', 'int') IS NOT NULL
	
	UPDATE TARN.Encounter
	SET UltrasoundBodyRegion = UltrasoundBodyRegion  + ', ' + CASE ActionID WHEN 6 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[3]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 6 AND details.value('(/ImagingDTO/BodyRegion/int)[3]', 'int') IS NOT NULL
	
	UPDATE TARN.Encounter
	SET UltrasoundBodyRegion = UltrasoundBodyRegion  + ', ' + CASE ActionID WHEN 6 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[4]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 6 AND details.value('(/ImagingDTO/BodyRegion/int)[4]', 'int') IS NOT NULL

	UPDATE TARN.Encounter
	SET UltrasoundBodyRegion = UltrasoundBodyRegion  + ', ' + CASE ActionID WHEN 6 THEN BR.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgBodyRegion BR ON details.value('(/ImagingDTO/BodyRegion/int)[5]', 'int') = BR.ID
	WHERE SectionID = 3 AND ActionID = 6 AND details.value('(/ImagingDTO/BodyRegion/int)[5]', 'int') IS NOT NULL	






















  	UPDATE TARN.Encounter
	SET XrayMethodOfImageTransfer = CASE ActionID WHEN 4 THEN IT.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgMethodofImageTransfer IT ON details.value('(/ImagingDTO/MethodofImageTransfer)[1]', 'int') = IT.ID
	WHERE SectionID = 3	AND ActionID = 4

  	UPDATE TARN.Encounter
	SET CTScanMethodOfImageTransfer = CASE ActionID WHEN 5 THEN IT.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgMethodofImageTransfer IT ON details.value('(/ImagingDTO/MethodofImageTransfer)[1]', 'int') = IT.ID
	WHERE SectionID = 3	AND ActionID = 5
	
  	UPDATE TARN.Encounter
	SET UltrasoundMethodOfImageTransfer = CASE ActionID WHEN 6 THEN IT.Description ELSE NULL END
	FROM TARN.Encounter TE
	INNER JOIN MAJORTAM.dbo.TARNCandidate MT ON TE.DistrictNo = MT.DistrictNumber AND TE.DateCreated = MT.DateCreated
	INNER JOIN MAJORTAM.dbo.TARNCandidateData MTD ON MT.CandidateID = MTD.CandidateID
	INNER JOIN MAJORTAM.dbo.ImgMethodofImageTransfer IT ON details.value('(/ImagingDTO/MethodofImageTransfer)[1]', 'int') = IT.ID
	WHERE SectionID = 3	AND ActionID = 6


  	UPDATE TARN.Encounter
	SET TransferInReason = TIR.Description
	,PreviousHospital = PH.Description
	,TransferOutReason = TOR.Description
	,NextHospital = NH.Description
	FROM TARN.Encounter TE
	LEFT JOIN MAJORTAM.dbo.Hospital PH ON PreviousHospital = PH.ID
	LEFT JOIN MAJORTAM.dbo.Hospital NH ON PreviousHospital = NH.ID
	LEFT JOIN MAJORTAM.dbo.TransferReason TIR ON TransferInReason = TIR.ID
	LEFT JOIN MAJORTAM.dbo.TransferReason TOR ON TransferOutReason = TOR.ID

  	UPDATE TARN.Encounter
	SET SurgeonGrade = SurG.Description,
	SurgeonSpecialty = SurS.Description,
	SupervisorGrade = SupG.Description,
	AnaesthetistGrade = AG.Description
	FROM TARN.Encounter TE
	LEFT JOIN MAJORTAM.dbo.OpsGrade SurG ON SurgeonGrade = SurG.ID
	LEFT JOIN MAJORTAM.dbo.OpsSpeciality SurS ON SurgeonSpecialty = SurS.ID
	LEFT JOIN MAJORTAM.dbo.OpsGrade SupG ON SupervisorGrade = SupG.ID
	LEFT JOIN MAJORTAM.dbo.OpsGrade AG ON AnaesthetistGrade = AG.ID
	--WHERE SectionID = 4 AND ActionID = 99
	


  	UPDATE TARN.Encounter
	SET DischargedTo = DD.Description
	,ReAdmissionDischargedTo = RDD.Description
	FROM TARN.Encounter TE
	LEFT JOIN Warehouse.PAS.DischargeDestinationBase DD ON DischargedTo = DD.DODID
	LEFT JOIN Warehouse.PAS.DischargeDestinationBase RDD ON ReAdmissionDischargedTo = RDD.DODID


  	UPDATE TARN.Encounter
	SET GlasgowOutcomeScale = GOS.Description
	,WhenGOSRecorded = GOSRec.Description
	FROM TARN.Encounter TE
	LEFT JOIN MAJORTAM.dbo.ImgGlasgowOutcomeScale GOS ON GlasgowOutcomeScale = GOS.TarnID
	LEFT JOIN MAJORTAM.dbo.ImgGlasgowOutcomeScaleRecorded GOSRec ON WhenGOSRecorded = GOSRec.TarnID
	
	UPDATE TARN.Encounter
	SET TeamLeaderSpecialty = 'A&E'
	FROM TARN.Encounter TE
	WHERE TraumaTeam = 'Yes' AND TeamLeaderSpecialty IS NULL


	UPDATE TARN.Observations
	SET Choose_status_of_airway = ORAS.Description
	,Choose_status_of_breathing = ORBS.Description
	FROM TARN.Observations O
	LEFT JOIN MAJORTAM.dbo.ObsRespAirwayStatus ORAS ON Choose_status_of_airway = ORAS.TarnID
	LEFT JOIN MAJORTAM.dbo.ObsRespBreathStatus ORBS ON Choose_status_of_breathing = ORBS.TarnID 


	UPDATE TARN.Interventions
	SET Type_of_spinal_protection = TSP.Description
	,Select_type_of_breathing_support = TBS.Description
	,Type_of_Airway_Support = TAS.Description
	FROM TARN.Interventions I
	LEFT JOIN MAJORTAM.dbo.IntervHostDefSProtType TSP ON Type_of_spinal_protection = TSP.TarnID
	LEFT JOIN MAJORTAM.dbo.IntervBreathingSupp TBS ON Select_type_of_breathing_support = TBS.TarnID 
	LEFT JOIN MAJORTAM.dbo.IntervAirwaySupp TAS ON Type_of_Airway_Support = TAS.TarnID 
	

END

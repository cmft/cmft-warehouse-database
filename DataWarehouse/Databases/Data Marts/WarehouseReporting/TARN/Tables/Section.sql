﻿CREATE TABLE [TARN].[Section] (
    [Column_Name]      VARCHAR (50)  NOT NULL,
    [Section]          VARCHAR (50)  NULL,
    [System]           VARCHAR (50)  NULL,
    [Full_Column_Name] VARCHAR (100) NULL,
    [Sort_Order]       INT           NULL,
    [Column_Order]     INT           NULL,
    CONSTRAINT [PK_TARN_Section] PRIMARY KEY CLUSTERED ([Column_Name] ASC)
);


﻿CREATE TABLE [TARN].[Interventions] (
    [Row_ID]                                 BIGINT        IDENTITY (1, 1) NOT NULL,
    [Encounter_Row_ID]                       BIGINT        NULL,
    [Intervention_Type]                      INT           NULL,
    [DistrictNo]                             VARCHAR (20)  NULL,
    [NHSNumber]                              VARCHAR (17)  NULL,
    [DateTimeCreated]                        DATETIME      NULL,
    [Airway_Support]                         VARCHAR (50)  NULL,
    [Date_of_Airway_Support]                 DATE          CONSTRAINT [DF_Interventions_Date_of_Airway_Support] DEFAULT (NULL) NULL,
    [Time_of_Airway_Support]                 TIME (7)      NULL,
    [Type_of_Airway_Support]                 VARCHAR (50)  NULL,
    [Extubation]                             VARCHAR (50)  NULL,
    [Date_of_Extubation]                     DATE          NULL,
    [Time_of_Extubation]                     TIME (7)      NULL,
    [Breathing_support]                      VARCHAR (50)  NULL,
    [Date_of_Breathing_support]              DATE          NULL,
    [Time_of_Breathing_support]              TIME (7)      NULL,
    [Select_type_of_breathing_support]       VARCHAR (50)  NULL,
    [Spinal_protection]                      VARCHAR (50)  NULL,
    [Date_of_Spinal_Protection]              DATE          NULL,
    [Time_of_Spinal_Protection]              TIME (7)      NULL,
    [Type_of_spinal_protection]              VARCHAR (50)  NULL,
    [Spinal_protection_removed]              VARCHAR (50)  NULL,
    [Date_spinal_protection_removed]         DATE          NULL,
    [Time_spinal_protection_removed]         TIME (7)      NULL,
    [Blood_Products_in_the_first_24_hours]   VARCHAR (50)  NULL,
    [Date_of_blood_product]                  DATE          NULL,
    [Start_time_of_blood_product]            TIME (7)      NULL,
    [Finish_time_of_blood_product]           TIME (7)      NULL,
    [Blood_Product]                          VARCHAR (50)  NULL,
    [Units_given_at_this_time]               VARCHAR (50)  NULL,
    [Embolisation_Interventional_Radiology]  VARCHAR (50)  NULL,
    [Date_of_embolisation]                   DATE          NULL,
    [Time_of_embolisation]                   TIME (7)      NULL,
    [Embolisation_Fluid]                     VARCHAR (50)  NULL,
    [Date_of_Fluid]                          DATE          NULL,
    [Start_time_of_Fluid]                    TIME (7)      NULL,
    [Finish_time_of_Fluid]                   TIME (7)      NULL,
    [Fluid]                                  VARCHAR (50)  NULL,
    [Volume_of_fluid_given_ml]               VARCHAR (50)  NULL,
    [Chest_Drain_NOT_needle_thoracocentesis] VARCHAR (50)  NULL,
    [Date_of_chest_drain]                    DATE          NULL,
    [Time_of_chest_drain]                    TIME (7)      NULL,
    [Tranexamic_Acid]                        VARCHAR (50)  NULL,
    [Date_of_Tranexamic_Acid]                DATE          NULL,
    [Time_of_Tranexamic_Acid]                TIME (7)      NULL,
    [IntOtherNotes]                          VARCHAR (MAX) NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_Interventions]
    ON [TARN].[Interventions]([Row_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Interventions_1]
    ON [TARN].[Interventions]([Encounter_Row_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Interventions_2]
    ON [TARN].[Interventions]([DistrictNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_Tarn_Interventions]
    ON [TARN].[Interventions]([Intervention_Type] ASC)
    INCLUDE([Encounter_Row_ID], [Row_ID], [Airway_Support]);


GO
CREATE NONCLUSTERED INDEX [IX-T_int]
    ON [TARN].[Interventions]([Intervention_Type] ASC)
    INCLUDE([Row_ID], [Airway_Support]);


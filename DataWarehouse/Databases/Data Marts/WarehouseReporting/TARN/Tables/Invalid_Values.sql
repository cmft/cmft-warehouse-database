﻿CREATE TABLE [TARN].[Invalid_Values] (
    [attendancenumber] VARCHAR (255) NULL,
    [DistrictNo]       VARCHAR (20)  NULL,
    [NHSNumber]        VARCHAR (17)  NULL,
    [Column_Name]      VARCHAR (255) NULL,
    [Column_Value]     VARCHAR (MAX) NULL,
    [Row_ID]           INT           NULL,
    [Invalid_Column]   INT           NULL,
    [Table_Name]       VARCHAR (50)  NULL,
    [DateCreated]      DATE          NULL,
    CONSTRAINT [IX_TARN_Invalid_Values_2] UNIQUE NONCLUSTERED ([Row_ID] ASC, [Column_Name] ASC, [Table_Name] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_ivalues_cn_tn]
    ON [TARN].[Invalid_Values]([Column_Name] ASC, [Table_Name] ASC)
    INCLUDE([DistrictNo], [Row_ID], [Column_Value], [Invalid_Column]);


GO
CREATE NONCLUSTERED INDEX [IX_TARN_Invalid_Values_1]
    ON [TARN].[Invalid_Values]([DistrictNo] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_ivalues_rid_tn]
    ON [TARN].[Invalid_Values]([Row_ID] ASC, [Table_Name] ASC)
    INCLUDE([Invalid_Column]);


GO
CREATE NONCLUSTERED INDEX [IDX_Tarn_INvalidValues]
    ON [TARN].[Invalid_Values]([Table_Name] ASC)
    INCLUDE([DistrictNo], [Row_ID]);


GO
CREATE NONCLUSTERED INDEX [IX-T_invalid_values]
    ON [TARN].[Invalid_Values]([Column_Name] ASC, [Table_Name] ASC);


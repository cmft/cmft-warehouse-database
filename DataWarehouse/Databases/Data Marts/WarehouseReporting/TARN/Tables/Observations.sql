﻿CREATE TABLE [TARN].[Observations] (
    [Row_ID]                            BIGINT        IDENTITY (1, 1) NOT NULL,
    [Encounter_Row_ID]                  BIGINT        NULL,
    [Observation_Type]                  INT           NULL,
    [DistrictNo]                        VARCHAR (20)  NULL,
    [NHSNumber]                         VARCHAR (17)  NULL,
    [DateTimeCreated]                   DATETIME      NULL,
    [Respiratory_observations]          VARCHAR (50)  NULL,
    [RO_Date]                           DATE          NULL,
    [RO_Time]                           TIME (7)      NULL,
    [Airway_status_on_arrival]          VARCHAR (50)  NULL,
    [Choose_status_of_airway]           VARCHAR (50)  NULL,
    [Breathing_status_on_arrival]       VARCHAR (50)  NULL,
    [Choose_status_of_breathing]        VARCHAR (50)  NULL,
    [Oxygen_saturation]                 VARCHAR (50)  NULL,
    [Enter_saturation]                  VARCHAR (50)  NULL,
    [Unassisted_Respiratory_rate]       VARCHAR (50)  NULL,
    [Enter_unassisted_respiratory_rate] VARCHAR (50)  NULL,
    [Circulation_observations]          VARCHAR (50)  NULL,
    [CO_Date]                           DATE          NULL,
    [CO_Time]                           TIME (7)      NULL,
    [Pulse_rate]                        VARCHAR (50)  NULL,
    [Enter_pulse_rate]                  VARCHAR (50)  NULL,
    [Blood_pressure]                    VARCHAR (50)  NULL,
    [Enter_systolic_BP_value]           VARCHAR (50)  NULL,
    [Enter_diastolic_BP_value]          VARCHAR (50)  NULL,
    [Nervous_System_observations]       VARCHAR (50)  NULL,
    [NSO_Date]                          DATE          NULL,
    [NSO_Time]                          TIME (7)      NULL,
    [Glasgow_Coma_Scale]                VARCHAR (50)  NULL,
    [Eye_score]                         VARCHAR (50)  NULL,
    [Verbal_score]                      VARCHAR (50)  NULL,
    [Motor_score]                       VARCHAR (50)  NULL,
    [Total_GCS_score]                   VARCHAR (50)  NULL,
    [Pupil_size]                        VARCHAR (50)  NULL,
    [Left_eye_pupil_size]               VARCHAR (50)  NULL,
    [Right_eye_pupil_size]              VARCHAR (50)  NULL,
    [Pupil_reactivity]                  VARCHAR (50)  NULL,
    [Left_eye_pupil_reactivity]         VARCHAR (50)  NULL,
    [Right_eye_pupil_reactivity]        VARCHAR (50)  NULL,
    [ObsOtherNotes]                     VARCHAR (MAX) NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_Observations]
    ON [TARN].[Observations]([Row_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Observations_1]
    ON [TARN].[Observations]([Encounter_Row_ID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Observations_2]
    ON [TARN].[Observations]([DistrictNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IDX_Tarn_Obs_ObsType]
    ON [TARN].[Observations]([Observation_Type] ASC)
    INCLUDE([Encounter_Row_ID]);


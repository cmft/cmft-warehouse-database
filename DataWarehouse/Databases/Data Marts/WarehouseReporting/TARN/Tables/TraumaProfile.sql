﻿CREATE TABLE [TARN].[TraumaProfile] (
    [FinancialQuarter] VARCHAR (34) NULL,
    [TheMonth]         VARCHAR (34) NULL,
    [WeekNo]           VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Adult]            INT          NULL,
    [Child]            INT          NULL,
    [Total]            INT          NULL,
    CONSTRAINT [PK_TraumaProfile] PRIMARY KEY CLUSTERED ([WeekNo] ASC)
);


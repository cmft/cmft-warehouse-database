﻿


CREATE View [OCM].[NationalResult]

as

select 
	NationalAlgorithm.ResultRecno
	,NationalAlgorithm.MergeEpisodeNo
	,NationalAlgorithm.Surname
	,NationalAlgorithm.Forenames
	,NationalAlgorithm.DistrictNo
	,NationalAlgorithm.AgeCode
	,NationalAlgorithm.SexCode
	,NationalAlgorithm.EthnicOriginCode
	,NationalAlgorithm.DeathIndicator
	,NationalAlgorithm.DateOfDeath
	,NationalAlgorithm.C1Result
	,NationalAlgorithm.ResultTime
	,NationalAlgorithm.RVRatio
	,NationalAlert = 
		case
			when NationalAlgorithm.RatioDescription = 'Baseline' then
				case
					when NationalAlgorithm.C1Result < NationalAlgorithm.RVRatio then 'Low'
					when NationalAlgorithm.C1Result = NationalAlgorithm.RVRatio then 'NoFlag'
					else 'Suggest'
				end
			when NationalAlgorithm.RatioDescription in ('Lowest0-7days','Median8-365days') then
				case
					when NationalAlgorithm.RVRatio >=3.0 then 'AKI 3'
					when NationalAlgorithm.RVRatio >=1.5 then
						case
							when NationalAlgorithm.Age <18 and NationalAlgorithm.SexCode = 'M' and NationalAlgorithm.C1Result > (80*3) then 'AKI 3'
							when Age <18 and NationalAlgorithm.SexCode = 'F' and NationalAlgorithm.C1Result > (60*3) then 'AKI 3'
							when Age >=18 and NationalAlgorithm.C1Result > 354 then 'AKI 3'
							when NationalAlgorithm.RVRatio >=2.0 then 'AKI 2'
							else 'AKI 1'
						end
					when Alert is not null then Alert
					when NationalAlgorithm.RatioDescription = 'Lowest0-7days' and NationalAlgorithm.C1Result-NationalAlgorithm.ResultForRatio >26 then 'No Alert Repeat'
					else 'No Alert'
				end
			else null
		end
	
	,NationalAlgorithm.ResultForRatio
	,NationalAlgorithm.RatioDescription
	,LocalRatio = TLoad.CurrentToBaselineResultRatio
	,LocalStage = CodedResult.Stage
	,LocalExclusion = 
		case
			when TLoad.ResultRecno is null
			then 'NoAPClast2yrs'
			when TLoad.AgeCode like '%Day%'
				or TLoad.AgeCode like '%Month%'
				or 
					(TLoad.AgeCode like '%Year%'
					and left(TLoad.AgeCode , 2) < '18' 
					)
			then 'Age'
			when TLoad.RenalPatient = 1
			then 'Renal'
			else null
		end
	,TLoad.LatestAdmissionDate
	,TLoad.LatestPatientCategory
	,LatestWardCode = TLoad.WardCode
	,TLoad.LatestAPCSpellNo
	,TLoad.SpecialtyCode
	,TLoad.ConsultantCode
	,TLoad.CurrentInpatient

from
	WarehouseOLAP.OCMTest.CreatinineNationalAlgorithm NationalAlgorithm
	
left join WarehouseOLAP.OCMTest.TLoad
on NationalAlgorithm.ResultRecno = TLoad.ResultRecno
and TLoad.Sequence = 1

left join WarehouseReporting.OCM.CodedResult
on NationalAlgorithm.ResultRecno = CodedResult.ResultRecno

	


﻿



CREATE view [OCM].[CodedResult] as


SELECT 
       ResultRecno
      ,SourceUniqueID
      ,SourcePatientNo
      ,SourceEpisodeNo
      ,MergeEpisodeNo
      ,ResultAPCLinked
      ,SequenceNoPatient
      ,SequenceNoSpell
      
      ,LatestPatientCategory
      ,LatestAdmissionDate
      ,PatientForename
      ,PatientSurname
      ,DistrictNo
      ,CasenoteNumber
      ,AgeCode
      ,EthnicOriginCode
      ,SexCode
      ,ConsultantCode
      ,SpecialtyCode
      ,WardCode
      ,CurrentInpatient
      ,LengthOfStay
      ,DiedInHospital
      
      ,Result
      ,ResultDate
      ,ResultTime
      ,PreviousResult
      ,PreviousResultTime
      ,ResultIntervalDays
      ,ResultChange
      ,CurrentToPreviousResultRatio
      ,ResultRateOfChangePer24Hr
      
      ,BaselineResultRecno
      ,BaselineResult
      ,BaselineResultTime
      ,BaselineToCurrentDays
      ,BaselineToCurrentChange
      ,CurrentToBaselineResultRatio
      ,Stage = case
                        when CurrentToBaselineResultRatio <1.5 then 'Stage0'
                        when CurrentToBaselineResultRatio <2.0 then 
                              case
                                    when BaselineToCurrentDays >= 181 then 'Exclusion'
                                    when SexCode = 'F' and BaselineResult < 44 then 'Exclusion'
                                    when SexCode = 'M' and BaselineResult < 58 then 'Exclusion'
                                    else 'Stage1'
                              end
                        when CurrentToBaselineResultRatio <3.0 then 'Stage2'
                        else 'Stage3'
                  end
      
      ,C1Result   
      ,RatioDescription 
      ,ResultForRatio   
      ,RVRatio    
      ,NationalAlert
      ,InHospitalAKI
      ,DateOfBirth
      ,ResultWardCode
      
FROM
      WarehouseOLAP.dbo.BaseOCMCreatinineResult







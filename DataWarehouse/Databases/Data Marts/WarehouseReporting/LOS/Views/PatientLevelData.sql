﻿
create view LOS.PatientLevelData

as

select [SnapshotDate]
      ,[LoSGroup]
      ,pld.[DivisionCode]
      ,[Division]
      ,[AdmissionMethodType]
      ,[WardCode]
      ,[Ward]
      ,[LoS]
      ,[SpecialtyCode]
      ,[Specialty]
      ,[AdmissionDate]
      ,[WardStartDate]
      ,[CaseNoteNo] 
      ,[PatientFirstName]
      ,[PatientSurname]
      ,[PatientAge]
      ,[Consultant]
      ,[IntendedManagement]
      ,[ExpectedLoS]
from
	[ETL].[LoSPatientLevelData] pld


﻿

create view LOS.SummarisedReportData

as

select [Division]
      ,[LoSGroup]
      ,[Week]
      ,[SnapshotDate]
      ,[FinancialYear]
      ,[Cases]
from
	[ETL].[LoSSummarisedReportData]
	
	


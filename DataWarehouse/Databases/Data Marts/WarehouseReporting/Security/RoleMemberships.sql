﻿EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\peter.graham';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'cmmc\liam.murphy';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Delwyn.Jones';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\peter.graham';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Philip.Huitson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Dusia.Romaniuk';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'tgh.information';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Helen.Shackleton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Wendy.Collier';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\gordon.fenton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Mohamed.Athman';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'THT\WCollier';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Nick.Bell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'THT\KBurns';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\gareth.jones';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\James.Watson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Ian.Connolly';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\peter.hoyle';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Malcom.hodson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\prinu.merrythomas';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Suman.Sidda';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmftslamlink';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Central Infopath Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\adam.black';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Colin.Hunter';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmmc\liam.murphy';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmmc\tim.nelson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\paul.miles';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Gary.Billington';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\Sec - Warehouse BI Developers';


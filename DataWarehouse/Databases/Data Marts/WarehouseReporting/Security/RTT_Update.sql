﻿CREATE ROLE [RTT_Update]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'RTT_Update', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'RTT_Update', @membername = N'cmmc\liam.murphy';


GO
EXECUTE sp_addrolemember @rolename = N'RTT_Update', @membername = N'cmmc\tim.nelson';


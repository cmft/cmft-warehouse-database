﻿CREATE LOGIN [CMMC\gordon.fenton]
    FROM WINDOWS WITH DEFAULT_DATABASE = [WarehouseReporting], DEFAULT_LANGUAGE = [us_english];


GO
ALTER LOGIN [CMMC\gordon.fenton] DISABLE;


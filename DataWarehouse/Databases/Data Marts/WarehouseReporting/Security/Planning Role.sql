﻿CREATE ROLE [Planning Role]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'Planning Role', @membername = N'CMMC\Sec - SP Planning';


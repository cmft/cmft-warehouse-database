﻿CREATE ROLE [CMFT Analyst]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\sec - sp information';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Delwyn.Jones';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'THT\KBurns';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\gareth.jones';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\prinu.merrythomas';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\adam.black';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Colin.Hunter';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'cmmc\tim.nelson';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'adamblack';


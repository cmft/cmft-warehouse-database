﻿
CREATE VIEW [OP].[WaitingListFact]

AS 

SELECT [EncounterRecno]
      ,[Cases]
      ,[LengthOfWait]
      ,[AgeCode]
      ,[CensusDate]
      ,[ConsultantCode]
      ,[SpecialtyCode]
      ,[DurationCode]
      ,[PracticeCode]
      ,[SiteCode]
      ,[SourceOfReferralCode]
      ,[WithAppointment]
      ,[BookedBeyondBreach]
      ,[NearBreach]
      ,[RTTActivity]
      ,[RTTBreachStatusCode]
      ,[AppointmentDate]
      ,[SexCode]
      ,[DirectorateCode]
  FROM [WarehouseOLAP].[dbo].[FactOPWaitingList]


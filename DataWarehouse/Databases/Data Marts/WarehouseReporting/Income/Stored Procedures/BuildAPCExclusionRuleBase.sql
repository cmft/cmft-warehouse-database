﻿
create proc Income.BuildAPCExclusionRuleBase

as

--if object_id('Income.APCExclusionRuleBase') is not null
--drop table Income.APCExclusionRuleBase

--go

--create table Income.APCExclusionRuleBase
--(
--	ExclusionRecno int identity(1,1) NOT NULL primary key,
--	ContextCode varchar(10) ,
--	PCTCode char(3),
--	PODCode varchar(10),
--	SiteCode varchar(5),
--	WardCode varchar(5),
--	HRGCode varchar(10),
--	--HRGGroupCode varchar(10),
--	ManagementIntentionCode char(2),
--	PASAdmissionMethodCode  char(2),
--	PASSpecialtyCode char(5) ,
--	NationalSpecialtyCode char(3),
--	NeonatalLevelOfCareCode char(1),
--	PrimaryDiagnosisCode varchar(10),
--	PrimaryProcedureCode varchar(10) ,
--	CommissioningSerialNo varchar(20) ,
--	AdminCategoryCode varchar(20) ,
--	FirstEpisodeInSpellIndicator char(1),
--	LoS int,
--	ExclusionCode varchar(20) ,
--	EffectiveFromDate date default '01/01/1900',
--	EffectiveToDate date,
--	DateAdded date default getdate()
--)

truncate table [Income].APCExclusionRuleBase

insert into Income.APCExclusionRuleBase
(
ContextCode
,SiteCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'YORK'
,'EXCLUDE - YORK'
)

/* Bowel */

insert into Income.APCExclusionRuleBase
(
ContextCode
,PASSpecialtyCode
,ExclusionCode
)
values 
(
'CEN||PAS'
,'BOWL'
,'BOWEL'
)


/* Burns */

insert into Income.APCExclusionRuleBase
(
ContextCode
,CommissioningSerialNo
,ExclusionCode
)
values
(
'CEN||PAS'
,'5NHBUR'
,'BURNS'
)
,
(
'CEN||PAS'
,'5N6BUR'
,'BURNS'
)

/* Private */

insert into Income.APCExclusionRuleBase
(
ContextCode
,AdminCategoryCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'PAY'
,'PRIVATE'
)
	
insert into Income.APCExclusionRuleBase
(
ContextCode
,CommissioningSerialNo
,ExclusionCode
)

select distinct
	'CEN||PAS'
	,ContractSerialNo
	,'PRIVATE'
from 
	WarehouseReporting.APC.Encounter
where 
	ContractSerialNo like 'YPP%'
	
/* Well Babies */

insert into Income.APCExclusionRuleBase
(
ContextCode
,PASAdmissionMethodCode
,NeonatalLevelOfCareCode
,PrimaryDiagnosisCode
,FirstEpisodeInSpellIndicator
,ExclusionCode
)


select distinct
	'CEN||PAS'
	,'BT'
	,'0'
	,PrimaryDiagnosisCode
	,'Y'
	,'WELL BABIES'

from 
	WarehouseReporting.APC.Encounter
where 
	PrimaryDiagnosisCode like 'Z38%'


insert into Income.APCExclusionRuleBase
(
ContextCode
,PASAdmissionMethodCode
,NeonatalLevelOfCareCode
,PrimaryDiagnosisCode
,FirstEpisodeInSpellIndicator
,ExclusionCode
)

select distinct
	'CEN||PAS'
	,'BT'
	,'N'
	,PrimaryDiagnosisCode
	,'Y'
	,'WELL BABIES'

from 
	WarehouseReporting.APC.Encounter
where 
	PrimaryDiagnosisCode like 'Z38%'


insert into Income.APCExclusionRuleBase
(
ContextCode
,PASAdmissionMethodCode
,NeonatalLevelOfCareCode
,PrimaryDiagnosisCode
,FirstEpisodeInSpellIndicator
,ExclusionCode
)
	
	
select distinct
	'CEN||PAS'
	,'BH'
	,'0'
	,PrimaryDiagnosisCode
	,'Y'
	,'WELL BABIES'

from 
	WarehouseReporting.APC.Encounter
where 
	PrimaryDiagnosisCode like 'Z38%'


insert into Income.APCExclusionRuleBase
(
ContextCode
,NationalSpecialtyCode
,NeonatalLevelOfCareCode
,ExclusionCode
)

values
(
'CEN||PAS'
,424
,'0'
,'WELL BABIES'
)
,
(
'CEN||PAS'
,424
,'N'
,'WELL BABIES'
)



/* Well Babies */

insert into Income.APCExclusionRuleBase
(
ContextCode
,PASSpecialtyCode
,ExclusionCode
)


values
(
'TRA||UG'
,'424'
,'WELL BABIES'
)

insert into Income.APCExclusionRuleBase
(
ContextCode
,NationalSpecialtyCode
,ExclusionCode
)


values
(
'TRA||UG'
,'424'
,'WELL BABIES'
)

/* Cost Per Case */

insert into Income.APCExclusionRuleBase
(
ContextCode
,CommissioningSerialNo
,ExclusionCode
)


select distinct	
		'CEN||PAS'
		,encounter.[ContractSerialNo]
		,'COST PER CASE'
from WarehouseReporting.apc.encounter
where
	encounter.[ContractSerialNo] like '%VNS'
	or encounter.[ContractSerialNo] like '%BAR'
	or encounter.[ContractSerialNo] like '%ICD'
	or encounter.[ContractSerialNo] like '%CIM'
	or encounter.[ContractSerialNo] like '%MUD'
	or encounter.[ContractSerialNo] like '%CAM'
	or encounter.[ContractSerialNo] like '%BMT'
	--or encounter.[ContractSerialNo] like '%CF1' -- Cystic Fibrosis removed 28/06/2012 - Sam
	or encounter.[ContractSerialNo] like '%GY1'


/* Renal Dialysis FCEs */

insert into Income.APCExclusionRuleBase
(
ContextCode
,ManagementIntentionCode
,NationalSpecialtyCode
,PrimaryProcedureCode
,LoS
,ExclusionCode
)

values
(
'CEN||PAS'
,'B'
,361
,'X40.3'
,0
,'RENAL DIALYSIS FCEs'
)
,
(
'CEN||PAS'
,'I'
,361
,'X40.3'
,0
,'RENAL DIALYSIS FCEs'
)
,
(
'CEN||PAS'
,'N'
,361
,'X40.3'
,0
,'RENAL DIALYSIS FCEs'
)
,
(
'CEN||PAS'
,'V'
,361
,'X40.3'
,0
,'RENAL DIALYSIS FCEs'
)

/* Regular Day/Night Attender */

insert into Income.APCExclusionRuleBase
(
ContextCode
,ManagementIntentionCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'N'
,'REGULAR DAY/NIGHT'
)
,
(
'CEN||PAS'
,'R'
,'REGULAR DAY/NIGHT'
)

/* Neonatal */

insert into Income.APCExclusionRuleBase
(
ContextCode
,NeonatalLevelOfCareCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'1'
,'NEONATAL'	
)
,
(
'CEN||PAS'
,'2'
,'NEONATAL'	
)
,
(
'CEN||PAS'
,'3'
,'NEONATAL'		
)										
,
(
'CEN||PAS'
,'H'
,'NEONATAL'		
)	
,
(
'CEN||PAS'
,'M'
,'NEONATAL'		
)													
,
(
'CEN||PAS'
,'S'
,'NEONATAL'		
)	

/* NSCAG */

insert into Income.APCExclusionRuleBase
(
ContextCode
,CommissioningSerialNo
,ExclusionCode
)


select distinct
	'CEN||PAS'
	,[ContractSerialNo]
	,'NSCAG'
from
	WarehouseReporting.APC.Encounter
where
	[ContractSerialNo] like '9%'									


/* BURNS */

insert into Income.APCExclusionRuleBase
(
ContextCode
,HRGCode
,ExclusionCode
)

	
select 
	'CEN||PAS'
	,HRGCode
	,'BURNS'
from
	Warehouse.WH.HRG
where
	HRGCode like 'JB%'	
	
	
/* IVF */

insert into Income.APCExclusionRuleBase
(
ContextCode
,HRGCode
,ExclusionCode
)


select
	'CEN||PAS'
	,HRGCode
	,'IVF'
from
	WH.HRG
where
	HRGCode like 'MC%'

/* Cystic Fybrosis */

insert into Income.APCExclusionRuleBase
(
ContextCode
,HRGCode
,ExclusionCode
)


select
	'CEN||PAS'
	,HRGCode
	,'CYSTIC FYBROSIS'
from
	Warehouse.WH.HRG
where
	HRGCode like 'PA13%'
	
/* Exclude (need definition) */


insert into Income.APCExclusionRuleBase
(
ContextCode
,PODCode
,PrimaryProcedureCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'RT'
,'J54'
,'EXCLUDE - NSCAG'
)

/* CAMHS - requested by Sam 10/07/2012 */

insert into Income.APCExclusionRuleBase
(
ContextCode
,NationalSpecialtyCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'656'
,'CAMHS'
)

insert into Income.APCExclusionRuleBase
(
ContextCode
,NationalSpecialtyCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'711'
,'CAMHS'
)

/* ======== */
/* Trafford */
/* ======== */


/* Private */

insert into Income.APCExclusionRuleBase
(
ContextCode
,AdminCategoryCode
,ExclusionCode
)

values
(
'TRA||UG'
,'02'
,'PRIVATE'
)



/* CMFT as Specialty */

insert into Income.APCExclusionRuleBase
(
ContextCode
,PASSpecialtyCode
,ExclusionCode
)

values
(
'TRA||UG'
,'MAN'
,'CMFT ACTIVITY'
)


insert into Income.APCExclusionRuleBase
(
ContextCode
,PASSpecialtyCode
,ExclusionCode
)

values
(
'TRA||UG'
,'MCH'
,'CMFT ACTIVITY'
)





/* POAU */

insert into Income.APCExclusionRuleBase
(
ContextCode
,WardCode
,ExclusionCode
)

values
(
'TRA||UG'
,'POAU'
,'POAU'
)


/* Stroke */

declare @hrg table
(
      HRGCode varchar(5)
)

insert into @hrg
values
('AA22Z')
,('AA23Z')


declare @primarydiagnosis table
(
      PrimaryDiagnosisCode varchar(5)
)

insert into @primarydiagnosis
values
('I61.0')
,('I61.1')
,('I61.2')
,('I61.3')
,('I61.4')
,('I61.5')
,('I61.6')
,('I61.8')
,('I61.9')
,('I63.0')
,('I63.1')
,('G63.2')
,('I63.3')
,('I63.4')
,('I63.5')
,('I63.8')
,('I63.9')
,('I64.X')


insert into Income.APCExclusionRuleBase
(
ContextCode
,HRGCode
,PrimaryDiagnosisCode
,ExclusionCode
)

select
	'TRA||UG'
	,HRGCode
	,PrimaryDiagnosisCode
	,'STROKE'
from
	@hrg
	,@primarydiagnosis


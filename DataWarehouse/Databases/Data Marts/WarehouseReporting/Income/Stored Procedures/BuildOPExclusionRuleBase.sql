﻿
create proc Income.BuildOPExclusionRuleBase

as

--if object_id('Income.OPExclusionRuleBase') is not null
--drop table Income.OPExclusionRuleBase

--go

--create table Income.OPExclusionRuleBase
--(
--	ExclusionRecno int IDENTITY(1,1) NOT NULL primary key,
--	ContextCode varchar(10) NULL,
--	PCTCode char(3) NULL,
--	PASSpecialtyCode varchar(10) NULL,
--	NationalSpecialtyCode char(3) NULL,
--	ClinicCode varchar(10) NULL,
--	FirstAttendanceFlag char(1) NULL,
--	PrimaryProcedureCode varchar(10),
--	DiagnosisFlagCode char(1),
--	CommissioningSerialNo varchar(20) NULL,
--	LocalAdminCategoryCode varchar(20) NULL,
--	ExclusionCode varchar(20) NULL,
--	EffectiveFromDate date default '01/01/1900',
--	EffectiveToDate date NULL,
--	DateAdded date default getdate()
--)

truncate table Income.OPExclusionRuleBase

/* Exclude - CommissioningSerialNo - Need definition */

insert into Income.OPExclusionRuleBase
(
ContextCode
,CommissioningSerialNo
,ExclusionCode
)


select
	distinct
	'CEN||PAS'
	,ContractSerialNo
	,'EXCLUDE CONTRACT ID'
from
	WarehouseReporting.OP.Encounter
where
	encounter.ContractSerialNo like '9%' 
	--or encounter.ContractSerialNo = 'OSV00A' -- removed 12/07/2012 as per request from Sam overseas visitors to be flagged using PCT code
	or encounter.ContractSerialNo like 'YPP%' 
	or encounter.ContractSerialNo like '%CF1'
	or encounter.ContractSerialNo like '%GY1'


/* Private */

insert into Income.OPExclusionRuleBase
(
ContextCode
,LocalAdminCategoryCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'PAY'
,'PRIVATE'
)


insert into Income.OPExclusionRuleBase
(
ContextCode
,LocalAdminCategoryCode
,ExclusionCode
)

values
(
'TRA||UG'
,'02'
,'PRIVATE'
)


/* TRAFFORD MIDWIFERY */

insert into Income.OPExclusionRuleBase
(
ContextCode
,PCTCode
,ClinicCode
,ExclusionCode
)

select 
	'CEN||PAS'
	,'5NR'
	,ClinicCode
	,'TRAFFORD MIDWIFERY'
from
	WarehouseReporting.Income.TraffordMidwiferyClinic traffordmidwifery
	
/* HOPE & RENAL */

insert into Income.OPExclusionRuleBase
(
ContextCode
,ClinicCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'JTL-HOPE'
,'HOPE & RENAL'
)
,
(
'CEN||PAS'
,'NNG-HOPE'
,'HOPE & RENAL'
)
,
(
'CEN||PAS'
,'SMMHD'
,'HOPE & RENAL'
)


/* EXCLUDE - DIAGNOSTICS */

insert into Income.OPExclusionRuleBase
(
ContextCode
,NationalSpecialtyCode
,DiagnosisFlagCode
,ExclusionCode
)

select 
	'CEN||PAS'
	,SpecialtyCode

	,'I'
	,'DIAGNOSTICS - I'
	from [warehouse].WH.TreatmentFunction
where  
	SpecialtyCode not in 
						(
						'320'
						,'654'

						)
insert into Income.OPExclusionRuleBase
(
ContextCode
,NationalSpecialtyCode
,DiagnosisFlagCode
,ExclusionCode
)

select 
	'CEN||PAS'
	,SpecialtyCode
	,'X'
	,'DIAGNOSTICS - X'
	from [warehouse].WH.TreatmentFunction
where  
	SpecialtyCode not in 
						(
						'656'
						)
						
/* BOWL */

insert into Income.OPExclusionRuleBase
(
ContextCode
,PASSpecialtyCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'BOWL'
,'BOWEL'
)

/* GYN1 */

insert into Income.OPExclusionRuleBase
(
ContextCode
,PASSpecialtyCode
,FirstAttendanceFlag
,ExclusionCode
)

values
(
'CEN||PAS'
,'GYN1'
,'2'
,'GYN1'
)



--select *
--from Income.OPExclusionRuleBase
--where ExclusionCode = 'DIAGNOSTICS - I'
--and NationalSpecialtyCode = 401


--select 
--	*
--from 
--	Income.OPExclusionRuleBase
--where
--	DiagnosisFlagCode = 'I'
--	and NationalSpecialtyCode = '401'

/* Lucentis - added 17/07/2012 note from Linda */

insert into Income.OPExclusionRuleBase
(
ContextCode
,ClinicCode
,PrimaryProcedureCode
,ExclusionCode
)

values
(
'CEN||PAS'
,'MTCAM'
,'C79.4'
,'LUCENTIS'
)
,
(
'CEN||PAS'
,'MTCPM '
,'C79.4'
,'LUCENTIS'
)
,
(
'CEN||PAS'
,'MTCEVE '
,'C79.4'
,'LUCENTIS'
)



/* OP Exclusions - THT */

insert into Income.OPExclusionRuleBase
(
ContextCode
,PASSpecialtyCode
,ExclusionCode
)
values
(
'TRA||UG'
,'MSS'
,'TRA-MSS'
)
,
(
'TRA||UG'
,'CardResp'
,'TRA-CardResp'
)
,
(
'TRA||UG'
,'711'
,'TRA-711'
)



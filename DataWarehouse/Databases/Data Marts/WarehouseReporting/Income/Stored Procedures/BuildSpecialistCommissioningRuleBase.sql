﻿
create proc Income.BuildSpecialistCommissioningRuleBase

as

--create schema Income

--if object_id('Income.APCSpecialistCommissioningRuleBase') is not null 
--drop table Income.APCSpecialistCommissioningRuleBase

--create table Income.APCSpecialistCommissioningRuleBase

--(
--    SpecialistCommissioningRecno int identity (1,1) primary key
--    ,ContextCode varchar(10)
--    ,PCTCode char(3)
--    ,AgeCategoryCode char(1)
--    ,AgeCategoryThreshold int
--    ,NationalSpecialtyCode varchar(10)
--    ,HRGCode char(8)
--    ,PODCode varchar(10)
--    ,ProcedureCode varchar(5)
--	,DiagnosisCode varchar(5)
--    ,PatientCategoryCode varchar(5)
--    ,CommissioningSerialNo varchar(20)
--    ,SpecialistCommissioningCode varchar(20)
--    ,EffectiveFromDate date
--    ,EffectiveToDate date
--    ,DateAdded date default getdate()
--)

--CREATE NONCLUSTERED INDEX [IX_APCSpecialistCommissioningRuleBase] ON [Income].[APCSpecialistCommissioningRuleBase]
--(
--	[ContextCode] ASC,
--	[PCTCode] ASC,
--	[AgeCategoryCode] ASC,
--	[NationalSpecialtyCode] ASC,
--	[HRGCode] ASC,
--	[PODCode] ASC,
--	[ProcedureCode] ASC,
--	[DiagnosisCode] ASC,
--	[PatientCategoryCode] ASC,
--	[CommissioningSerialNo] ASC,
--	[EffectiveFromDate] ASC,
--	[EffectiveToDate] ASC
--)


--if object_id('Income.OPSpecialistCommissioningRuleBase') is not null 
--drop table Income.OPSpecialistCommissioningRuleBase

--create table Income.OPSpecialistCommissioningRuleBase

--(
--	SpecialistCommissioningRecno int identity (1,1) primary key
--	,ContextCode varchar(10)
--	,PCTCode char(3)
--	,AgeCategoryCode char(1)
--	,AgeCategoryThreshold int 
--	,NationalSpecialtyCode varchar(10) 
--	,MainNationalSpecialtyCode varchar(10)
--	,ClinicCode varchar(10) 
--	,CommissioningSerialNo varchar(20) 
--	,SpecialistCommissioningCode varchar(20) 
--	,EffectiveFromDate date 
--	,EffectiveToDate date 
--	,DateAdded date default getdate()
--)

--CREATE NONCLUSTERED INDEX [IX_OPSpecialistCommissioningRuleBase] ON [Income].[OPSpecialistCommissioningRuleBase]
--(
--	[ContextCode] ASC,
--	[PCTCode] ASC,
--	[AgeCategoryCode] ASC,
--	NationalSpecialtyCode ASC,
--	MainNationalSpecialtyCode ASC,
--	ClinicCode ASC,
--	CommissioningSerialNo ASC,
--	[EffectiveFromDate] ASC,
--	[EffectiveToDate] ASC
--)


/* === */
/* APC */
/* === */

truncate table Income.APCSpecialistCommissioningRuleBase

--temp tables to hold PCT codes

/* nw pcts */

if object_id('tempdb..#nwpct') is not null
drop table #nwpct

create table #nwpct
(
      PCTCode char(3)
)

insert into #nwpct
values
	  	('5CC')
		,('5HP')
		,('5J2')
		,('5J4')
		,('5NE')
		,('5NF')
		,('5NG')
		,('5NH')
		,('5NJ')
		,('5NK')
		,('5NL')
		,('5NM')
		,('5NN')
		,('5NP')
		,('TAP')
		,('5F5')
		,('5F7')
		,('5HG')
		,('5HQ')
		,('5J5')
		,('5JX')
		,('5LH')
		,('5NQ')
		,('5NR')
		,('5NT')

/* q13/q15 pcts */

if object_id('tempdb..#q13q15pct') is not null
drop table #q13q15pct

create table #q13q15pct
(
      PCTCode char(3)
)

insert into #q13q15pct
values

		('5CC')
		,('5HP')
		,('5J2')
		,('5J4')
		,('5NE')
		,('5NF')
		,('5NG')
		,('5NH')
		,('5NJ')
		,('5NK')
		,('5NL')
		,('5NM')
		,('5NN')
		,('5NP')
		,('TAP')


/* q14 pcts */

if object_id('tempdb..#q14pct') is not null
drop table #q14pct

create table #q14pct
(
      PCTCode char(3)
)

insert into #q14pct
values

	('5F5')
	,('5F7')
	,('5HG')
	,('5HQ')
	,('5J5')
	,('5JX')
	,('5LH')
	,('5NQ')
	,('5NR')
	,('5NT')


/* HA = Q31, POD = BMTA or BMTC (see Appendix D) */

declare @hrg table
(
      HRGCode char(5)
)

insert into @hrg
values
      ('SA19A')
      ,('SA20A')
      ,('SA21A')
      ,('SA26A')
      ,('SA28A')
      ,('SA20B')
      ,('SA22B')
      ,('SA28B')
      ,('SA26B')
      ,('SA21B')


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'BMTA'
      ,'BMT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'BMTC'
      ,'BMT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

/* CAMHS  PCT = 5NT, Spec = 711 */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)


values
( 
      'CEN||PAS'
      ,'5NT'
      ,'711'
      ,'CAMHS'
      ,'1 apr 2011'
      ,'31 mar 2012'
)


/* 656 added 28/06/2012 - Sam */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)


values
( 
      'CEN||PAS'
      ,'5NT'
      ,'656'
      ,'CAMHS'
      ,'1 apr 2011'
      ,'31 mar 2012'
)

/* CAMHS POD code added 17/08/2012 - DG */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)


values
( 
      'CEN||PAS'
      ,'5NT'
      ,'CAMHS'
      ,'CAMHS'
      ,'1 apr 2011'
      ,'31 mar 2012'
)



/* Card - HA = Q31, Age >=19, Spec = 320 or 170  */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'170'
      ,'CARD'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct
      
insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'172'
      ,'CARD'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'320'
      ,'CARD'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

/* Card - HA = Q31, Age >=19, Spec = 300 and HRG in Appendix C  */

delete from @hrg

--declare @hrg table
--(
--      HRGCode varchar(5)
--)

insert into @hrg

values 
       ('EA03Z')
      ,('EA04Z')
      ,('EA05Z')
      ,('EA06Z')
      ,('EA07Z')
      ,('EA09Z')
      ,('EA10Z')
      ,('EA11Z')
      ,('EA12Z')
      ,('EA13Z')
      ,('EA14Z')
      ,('EA15Z')
      ,('EA16Z')
      ,('EA17Z')
      ,('EA18Z')
      ,('EA19Z')
      ,('EA20Z')
      ,('EA21Z')
      ,('EA22Z')
      ,('EA27Z')
      ,('EA28Z')
      ,('EA29Z')
      ,('EA30Z')
      ,('EA31Z')
      ,('EA32Z')
      ,('EA33Z')
      ,('EA34Z')
      ,('EA35Z')
      ,('EA36B')
      ,('EA36Z')
      ,('EA39Z')
      ,('EA40Z')
      ,('EA41Z')
      ,('EA42Z')
      ,('EA44Z')
      ,('EA45Z')
      ,('EA46Z')
      ,('EA47Z')


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate

)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'300'
      ,HRGCode
      ,'CARD'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      @hrg
      ,#nwpct



/* Card - POD = CSU or CHDU */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

values
(
      'CEN||PAS'
      ,'CSU'
      ,'CARD'
      ,'1 apr 2011'
      ,'31 mar 2012'
)
,
(
      'CEN||PAS'
      ,'CHDU'
      ,'CARD'
      ,'1 apr 2011'
      ,'31 mar 2012'
)


/* CS - HA = Q31, Spec = 320 or 321 or 170 or 361 or 370 or 303 or 253, Age <19 */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'320'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'321'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'170'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'361'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'370'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'303'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'253'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

/* CS - HA = Q31, Spec = 160 or 150 or 219 or 218 or 220 */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'160'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'150'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'219'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'218'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'220'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct


/* CS - HA = Q31, Spec = 171 or 420, Age <19, HRG in Appendix B */

delete from @hrg

--declare @hrg table
--(
--      HRGCode varchar(5)
--)

insert into @hrg

values
('LA01A')
,('LA01B')
,('LA02A')
,('LA02B')
,('LA03A')
,('LA03B')
,('LA11Z')
,('LA12A')
,('LA12B')
,('LA13A')
,('LA13B')
,('LA14Z')
,('LB02A')
,('LB02B')
,('LB02C')
,('LB02D')
,('LB03Z')
,('LB04A')
,('LB04B')
,('LB05A')
,('LB05B')
,('LB05C')
,('LB05D')
,('LA05A')
,('LA05B')
,('LA07A')
,('LA07B')
,('LA07C')
,('LA08A')
,('LA08B')
,('LA08C')
,('LA08D')
,('LA09A')
,('LA09B')
,('LA09C')
,('QZ13A')
,('QZ13B')
,('QZ14A')
,('QZ14B')

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'171'
      ,HRGCode
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      @hrg
      ,#nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'420'
      ,HRGCode
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      @hrg
      ,#nwpct
      

/* CS - HA = Q31, POD = HDUPO or HDUP or CCCPD or CRDHOSP or CCAPD or SCBU or CF */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'HDUPO'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'HDUP'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'CCCPD'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'CRDHOSP'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'CCAPD'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'SCBU'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'CF'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct


/* GENE HA = Q31, Spec = 311 */ 

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'311'
      ,'GENE'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct



/* RT - HA = Q31, Age >=19, Spec = 361 Or (102(No DC)) */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'361'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct
      
insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,PatientCategoryCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'102'
      ,'NE'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct
      
insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,PatientCategoryCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'102'
      ,'EL'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct
      
insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,PatientCategoryCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'102'
      ,'RD'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

/* RT - HA = Q31, Age >=19, Spec = 100, HRG in Appendix B */

delete from @hrg

--declare @hrg table
--(
--      HRGCode varchar(5)
--)

insert into @hrg
values
      ('LA01A')
      ,('LA01B')
      ,('LA02A')
      ,('LA02B')
      ,('LA03A')
      ,('LA03B')
      ,('LA11Z')
      ,('LA12A')
      ,('LA12B')
      ,('LA13A')
      ,('LA13B')
      ,('LA14Z')
      ,('LB02A')
      ,('LB02B')
      ,('LB02C')
      ,('LB02D')
      ,('LB03Z')
      ,('LB04A')
      ,('LB04B')
      ,('LB05A')
      ,('LB05B')
      ,('LB05C')
      ,('LB05D')
      ,('LA05A')
      ,('LA05B')
      ,('LA07A')
      ,('LA07B')
      ,('LA07C')
      ,('LA08A')
      ,('LA08B')
      ,('LA08C')
      ,('LA08D')
      ,('LA09A')
      ,('LA09B')
      ,('LA09C')
      ,('QZ13A')
      ,('QZ13B')
      ,('QZ14A')
      ,('QZ14B')

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'100'
      ,HRGCode
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      @hrg
      ,#nwpct

/* RT - HA = Q31, Age >=19, Spec = 420 Or 171 HRG in Appendix A */

delete from @hrg

--declare @hrg table
--(
--      HRGCode varchar(5)
--)

insert into @hrg

values 

('LA01A')
,('LA01B')
,('LA02B')
,('LA03A')
,('LA03B')

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'420'
      ,HRGCode
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      @hrg
      ,#nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,NationalSpecialtyCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'A'
      ,19
      ,'171'
      ,HRGCode
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      @hrg
      ,#nwpct
      

/* RT - POD = ACAPD or ARDHOME or ARDHOSP or ARDPRES */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

values
(
      'CEN||PAS'
      ,'ACAPD'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
)
,
(
      'CEN||PAS'
      ,'ARDHOME'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
)
,
(
      'CEN||PAS'
      ,'ARDHOSP'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
)
,
(
      'CEN||PAS'
      ,'ARDPRES'
      ,'RT'
      ,'1 apr 2011'
      ,'31 mar 2012'
)
      

/* NSCAG Contract ID begins with 9 - need routine to get all appropriate contract ids and insert */

declare @contractserialno table
(
      CommissioningSerialNo varchar(10)
)

insert into @contractserialno

select distinct
      ContractSerialNo
from
      WarehouseReporting.APC.Encounter
where
      ContractSerialNo like '9%'

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,CommissioningSerialNo
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate

)

select
      'CEN||PAS'
      ,CommissioningSerialNo
      ,'NSCAG'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      @contractserialno


/* NSCAG Age >=19, HRG in Appendix A, Primary Procedure begins with J54 */


declare @procedure table
(
      ProcedureCode varchar(5)
)

insert into @procedure

values 

('J54.1')
,('J54.2')
,('J54.3')
,('J54.4')
,('J54.5')
,('J54.8')
,('J54.9')

delete from @hrg

--declare @hrg table
--(
--HRGCode varchar(5)
--)

insert into @hrg

values 

('LA01A')
,('LA01B')
,('LA02B')
,('LA03A')
,('LA03B')


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,HRGCode
      ,ProcedureCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)



select  
      'CEN||PAS'
      ,'A'
      ,19
      ,HRGCode
      ,ProcedureCode
      ,'NSCAG'
      ,'1 apr 2011'
      ,'31 mar 2012'
from 
      @procedure
      ,@hrg

      
/* PICU - HA = Q31, POD = PICU or LTTU or PETS */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)
      
select  
      'CEN||PAS'
      ,PCTCode
      ,'PICU'
      ,'PICU'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)
      
select  
      'CEN||PAS'
      ,PCTCode
      ,'LTTU'
      ,'PICU'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)
      
select  
      'CEN||PAS'
      ,PCTCode
      ,'PETS'
      ,'PICU'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct


/* OTHER - HA = Q13 or Q15, Spec = 361 Or 102 or 360 */

-- just uses 360 as per ITS

--insert into Income.APCSpecialistCommissioningRuleBase
--(
--    ContextCode
--    ,PCTCode
--    ,NationalSpecialtyCode
--    ,SpecialistCommissioningCode
--    ,EffectiveFromDate
--    ,EffectiveToDate
--)

--select  
--    'CEN||PAS'
--    ,'APC'
--    ,PCTCode
--    ,'102'
--    ,'OTHER'
--    ,'1 apr 2011'
--    ,'31 mar 2012'
--from
--    #nwpct


-- Added back in 13/07/2012

insert into Income.APCSpecialistCommissioningRuleBase
(
    ContextCode
    ,PCTCode
    ,NationalSpecialtyCode
    ,SpecialistCommissioningCode
    ,EffectiveFromDate
    ,EffectiveToDate
)

select  
    'CEN||PAS'
    ,PCTCode
    ,'360'
    ,'OTHER'
    ,'1 apr 2011'
    ,'31 mar 2012'
from
    #q13q15pct

--insert into Income.APCSpecialistCommissioningRuleBase
--(
--    ContextCode
--    ,DatasetCode
--    ,PCTCode
--    ,NationalSpecialtyCode
--    ,SpecialistCommissioningCode
--    ,EffectiveFromDate
--    ,EffectiveToDate
--)

--select  
--    'CEN||PAS'
--    ,'APC'
--    ,PCTCode
--    ,'361'
--    ,'OTHER'
--    ,'1 apr 2011'
--    ,'31 mar 2012'
--from
--    #q13q15pct

/* OTHER - HA = Q13 or Q15, HRG = CZ26%  28/06/2012 /23 codes added as denote top ups and maybe produced by grouper */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select  
      'CEN||PAS'
      ,PCTCode
      ,'CZ26Z'
      ,'OTHER'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select  
      'CEN||PAS'
      ,PCTCode
      ,'CZ26Z/23'
      ,'OTHER'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct
      
/* OTHER - HA = Q13 or Q15, HRG = CZ26% */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select  
      'CEN||PAS'
      ,PCTCode
      ,'CZ25N'
      ,'OTHER'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select  
      'CEN||PAS'
      ,PCTCode
      ,'CZ25N/23'
      ,'OTHER'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct
      
/* OTHER - HA = Q13 or Q15, HRG = CZ25% */
      
insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select  
      'CEN||PAS'
      ,PCTCode
      ,'CZ25Q'
      ,'OTHER'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,HRGCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select  
      'CEN||PAS'
      ,PCTCode
      ,'CZ25Q/23'
      ,'OTHER'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #q13q15pct



/* == */
/* OP */
/* == */


truncate table Income.OPSpecialistCommissioningRuleBase

/* Clinic specific conditions (not in APC build) */

/* CS - HA = Q31, Age <19, Clinic = CJEWMET Or CJHWMET Or CAAMMETT */

insert into Income.OPSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,ClinicCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'CJEWMET'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct


insert into Income.OPSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,ClinicCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'CJHWMET'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct

insert into Income.OPSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,AgeCategoryCode
      ,AgeCategoryThreshold
      ,ClinicCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate

)
select
      'CEN||PAS'
      ,PCTCode
      ,'C'
      ,19
      ,'CAAMMETT'
      ,'CS'
      ,'1 apr 2011'
      ,'31 mar 2012'
from
      #nwpct
      
/* OTHER Q13 and Q15 as per ITS NOT contract definitions doc */

insert into Income.OPSpecialistCommissioningRuleBase
(
    ContextCode
    ,PCTCode
    ,NationalSpecialtyCode
    ,SpecialistCommissioningCode
    ,EffectiveFromDate
    ,EffectiveToDate
)

select  
    'CEN||PAS'
    ,PCTCode
    ,'255'
    ,'OTHER'
    ,'1 apr 2011'
    ,'31 mar 2012'
from
    #q13q15pct
    
insert into Income.OPSpecialistCommissioningRuleBase
(
    ContextCode
    ,PCTCode
    ,NationalSpecialtyCode
    ,SpecialistCommissioningCode
    ,EffectiveFromDate
    ,EffectiveToDate
)

select  
    'CEN||PAS'
    ,PCTCode
    ,'313'
    ,'OTHER'
    ,'1 apr 2011'
    ,'31 mar 2012'
from
    #q13q15pct

/* add OP context from APCSpecialistCommissioningRuleBase */

insert into Income.OPSpecialistCommissioningRuleBase
(
      ContextCode 
      ,PCTCode
      ,AgeCategoryCode 
      ,AgeCategoryThreshold 
      ,NationalSpecialtyCode 
      ,CommissioningSerialNo 
      ,SpecialistCommissioningCode 
      ,EffectiveFromDate
      ,EffectiveToDate
)

select distinct
      ContextCode
      ,PCTCode 
      ,AgeCategoryCode 
      ,AgeCategoryThreshold 
      ,NationalSpecialtyCode 
      ,CommissioningSerialNo 
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
from
     Income.APCSpecialistCommissioningRuleBase rulebase
where

	(
	SpecialistCommissioningCode = 'RT'
	and NationalSpecialtyCode = 102
	)
or
	(
-- exclude APC specific attributes
	(
	HRGCode is null
and
	PODCode is null
and
	ProcedureCode is null
and
	PatientCategoryCode is null
	)
and
-- exclude APC specific condition for CARD for Q14
not exists
		(
		select 
			1
		from 
			Income.APCSpecialistCommissioningRuleBase rulebase1
		inner join #q14pct q14
		on rulebase1.PCTCode = q14.PCTCode
		where
			rulebase1.SpecialistCommissioningCode = 'CARD'
		and rulebase1.NationalSpecialtyCode in 
									(
									170
									,172
									,320
									)
		and rulebase.ContextCode = rulebase1.ContextCode
		and rulebase.PCTCode = rulebase1.PCTCode
		and rulebase.SpecialistCommissioningCode = rulebase1.SpecialistCommissioningCode
		)
)
---- exclude OTHER as specialty codes differ in ITS and are different to those in the Contract Definitions doc
--and 
--	SpecialistCommissioningCode <> 'OTHER'



/* ==================================== */
/* insert new financial year (2012/2013 */
/* ==================================== */

/* APC */

-- PCT codes removed -- 28/06/2012 English PCTs added back in

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode 
	  ,PCTCode
      ,AgeCategoryCode 
      ,AgeCategoryThreshold 
      ,NationalSpecialtyCode 
      ,HRGCode
      ,PODCode
      ,ProcedureCode
      ,PatientCategoryCode
      ,CommissioningSerialNo 
      ,SpecialistCommissioningCode 
	  ,EffectiveFromDate
      ,EffectiveToDate
)

select distinct
      ContextCode 
	  ,[OrganisationCode]
      ,AgeCategoryCode 
      ,AgeCategoryThreshold 
      ,NationalSpecialtyCode 
      ,HRGCode
      ,PODCode
      ,ProcedureCode
      ,PatientCategoryCode
      ,CommissioningSerialNo 
      ,SpecialistCommissioningCode
	  ,EffectiveFromDate = '1 apr 2012'
      ,EffectiveToDate = '31 mar 2013' 
from
      Income.APCSpecialistCommissioningRuleBase,
(
select
	[OrganisationCode]
from
	[Organisation].[dbo].[PCT]
where 
	ROCode like 'Y%'
) englishpcts

where
	  EffectiveFromDate = '1 apr 2011'
      and EffectiveToDate = '31 mar 2012' 
	  --and SpecialistCommissioningCode <> 'CARD'
      
/* OP */

-- PCT codes removed -- 28/06/2012 English PCTs added back in

insert into Income.OPSpecialistCommissioningRuleBase
(
      ContextCode 
	  ,PCTCode
      ,AgeCategoryCode 
      ,AgeCategoryThreshold 
      ,NationalSpecialtyCode 
      ,ClinicCode
      ,CommissioningSerialNo 
      ,SpecialistCommissioningCode 
	  ,EffectiveFromDate
      ,EffectiveToDate
)

select distinct
      ContextCode 
	  ,[OrganisationCode]
      ,AgeCategoryCode 
      ,AgeCategoryThreshold 
      ,NationalSpecialtyCode 
      ,ClinicCode
      ,CommissioningSerialNo 
      ,SpecialistCommissioningCode 
	  ,EffectiveFromDate = '1 apr 2012'
      ,EffectiveToDate = '31 mar 2013' 
from
      Income.OPSpecialistCommissioningRuleBase,
(
select
	[OrganisationCode]
from
	[Organisation].[dbo].[PCT]
where 
	ROCode like 'Y%'
) englishpcts

where
	  EffectiveFromDate = '1 apr 2011'
      and EffectiveToDate = '31 mar 2012' 
	  --and SpecialistCommissioningCode <> 'CARD'


/* remove SCBU and CF and HRG like cz25% cz25% (cochlear) for Q14 */

delete from
	specialistcommissioningrulebase
from Income.APCSpecialistCommissioningRuleBase specialistcommissioningrulebase
inner join #q14pct q14
on specialistcommissioningrulebase.PCTCode = q14.PCTCode
where
	(
	PODCode in 
				(
				'CF'
				,'SCBU'
				)
	)
or
	HRGCode in
				(
				'CZ25N'
				,'CZ26Z'
				,'CZ25Q'
				,'CZ25N/23'
				,'CZ26Z/23'
				,'CZ25Q/23'
				)
or
	NationalSpecialtyCode = '360'


/* remove OP CARD Q14 */

--delete from
--specialistcommissioningrulebase
--from Income.OPSpecialistCommissioningRuleBase specialistcommissioningrulebase
--inner join #q14pct q14
--on specialistcommissioningrulebase.PCTCode = q14.PCTCode
--where 
--[SpecialistCommissioningCode] = 'CARD'
--and [EffectiveFromDate] = '1 apr 2012'
--and [EffectiveToDate] = '31 mar 2013'

/* remove OP OTHER Q14 */

delete from
	specialistcommissioningrulebase
from Income.OPSpecialistCommissioningRuleBase specialistcommissioningrulebase
inner join #q14pct q14
on specialistcommissioningrulebase.PCTCode = q14.PCTCode
where 
	[SpecialistCommissioningCode] = 'OTHER'
	and [EffectiveFromDate] = '1 apr 2012'
	and [EffectiveToDate] = '31 mar 2013'

/* remove CAMHS for all PCTs bar 5NT */

/* apc */

delete from
	specialistcommissioningrulebase
from Income.APCSpecialistCommissioningRuleBase specialistcommissioningrulebase
where 
	PCTCode <> '5NT'
and
	SpecialistCommissioningCode = 'CAMHS'

/* OP */

delete from
	specialistcommissioningrulebase
from Income.OPSpecialistCommissioningRuleBase specialistcommissioningrulebase
where 
	PCTCode <> '5NT'
and
	PCTCode <> '5F5'
and
	SpecialistCommissioningCode = 'CAMHS'

/* Add 259 and 260 for CS - Childrens now coding paediatric treatment functions */

--APC

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'259'
      ,'CS'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'260'
      ,'CS'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'NICU'
      ,'NICU'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts



/* RT - POD = AKU */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'AKU'
      ,'RT'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'HDUN'
      ,'HDUN'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts

-- OP

insert into Income.OPSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'259'
      ,'CS'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts


insert into Income.OPSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,NationalSpecialtyCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'260'
      ,'CS'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts



/* Flag Chemo_P as CS */


insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
      ,PCTCode
      ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)

select
      'CEN||PAS'
      ,[OrganisationCode]
      ,'CHEMO_P'
      ,'CS'
      ,'1 apr 2012'
      ,'31 mar 2013'
from
      (
		select
			[OrganisationCode]
		from
			[Organisation].[dbo].[PCT]
		where 
			ROCode like 'Y%'
		) englishpcts



/* RT in OP for 2012/2012 now reliant on clinic codes excluding 'NMPICN', 'NMPATN',,'NMPICF','NMPATF' */


--delete from
--	specialistcommissioningrulebase
--from 
--	Income.OPSpecialistCommissioningRuleBase specialistcommissioningrulebase
--where 
--	SpecialistCommissioningCode = 'RT'
--and EffectiveFromDate = '2012-04-01'
--and EffectiveToDate = '2013-03-31'
--and ClinicCode is null



--select *
--from Income.APCSpecialistCommissioningRuleBase
--where SpecialistCommissioningCode = 'camhs'

/* for cardiac specialist commissioning when live */

declare @diag table
(
DiagnosisCode varchar(5)
)
insert into @diag

values

('Q20.1')
,('Q20.2')
,('Q20.3')
,('Q20.4')
,('Q20.5')
,('Q20.6')
,('Q20.8')
,('Q20.9')
,('Q21.0')
,('Q21.1')
,('Q21.2')
,('Q21.3')
,('Q21.4')
,('Q21.8')
,('Q21.9')
,('Q22.0')
,('Q22.1')
,('Q25.8')
,('Q25.9')
,('Q26.0')
,('Q26.2')
,('Q26.3')
,('Q26.4')
,('Q26.5')
,('Q26.6')
,('Q26.8')
,('Q26.9')
,('Q20.0')
,('Q22.2')
,('Q22.3')
,('Q22.4')
,('Q22.5')
,('Q22.6')
,('Q22.8')
,('Q22.9')
,('Q23.0')
,('Q23.1')
,('Q23.2')
,('Q23.3')
,('Q23.4')
,('Q23.8')
,('Q23.9')
,('Q24.0')
,('Q24.1')
,('Q24.2')
,('Q24.3')
,('Q24.4')
,('Q24.5')
,('Q24.6')
,('Q24.8')
,('Q24.9')
,('Q25.0')
,('Q25.1')
,('Q25.2')
,('Q25.3')
,('Q25.4')
,('Q25.5')
,('Q25.6')
,('Q25.7')
,('I42.0')
,('I42.1')
,('I42.2')
,('I42.3')
,('I42.4')
,('I42.5')
,('I42.6')
,('I42.7')
,('I42.8')
,('I42.9')
,('I43.0')
,('I43.8')
,('I43.1')
,('I43.2')
,('I45.6')
,('I47.9')
,('I47.0')
,('I47.1')
,('I47.2')
,('I27.0')
,('I27.8')

/* All PCTs except Q14 */

--insert into Income.APCSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,DiagnosisCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,DiagnosisCode
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from @diag
--      ,(
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]

--		where 
--			ROCode like 'Y%'
--		) englishpcts
--where
--	[OrganisationCode] not in (select PCTCode from #q14pct)



declare @cardspecialty table
(
NationalSpecialtyCode varchar(3)
)

insert into @cardspecialty

values

('170')
,('172')
,('320')
,('300')




/* Q14 with CARD SPEC LOGIC */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
	  ,PCTCode
	  ,NationalSpecialtyCode
	  ,DiagnosisCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)


select 
	'CEN||PAS'
	,PCTCode
	,NationalSpecialtyCode
	,DiagnosisCode
	,'HEART'
	,'1 apr 2012'
	,'31 mar 2013'

from @diag
	,@cardspecialty
	,#q14pct


declare @proc table
(
ProcedureCode varchar(5)
)
insert into @proc
values
('L76.8')
,('L69.1')
,('L69.2')
,('L69.3')
,('L69.4')
,('L69.5')
,('L76.1')
,('L76.2')
,('L76.3')
,('L76.4')
,('L76.5')
,('L76.6')
,('L76.7')
,('L76.9')
,('L69.8')
,('L69.9')
,('L16.2')
,('L16.8')
,('L16.9')
,('L18.1')
,('L18.2')
,('L18.3')
,('L18.5')
,('L18.6')
,('L18.8')
,('L18.9')
,('L18.4')
,('L19.1')
,('L19.2')
,('L19.3')
,('L19.4')
,('L19.5')
,('L19.6')
,('L19.8')
,('L19.9')
,('L20.1')
,('L20.2')
,('L23.6')
,('L23.7')
,('L23.8')
,('L23.9')
,('L25.1')
,('L25.2')
,('L25.3')
,('L25.4')
,('L25.5')
,('L25.8')
,('L25.9')
,('L31.4')
,('K68.8')
,('K68.9')
,('K69.1')
,('K69.2')
,('K69.8')
,('K69.9')
,('K71.1')
,('K71.2')
,('K71.3')
,('K71.4')
,('K71.9')
,('K75.1')
,('K71.8')
,('K75.2')
,('K75.3')
,('K75.4')
,('K75.9')
,('K75.8')
,('L06.1')
,('L06.3')
,('L06.4')
,('L06.2')
,('L06.5')
,('L06.6')
,('L06.7')
,('L06.8')
,('L06.9')
,('L07.1')
,('L07.2')
,('L07.3')
,('L07.8')
,('L07.9')
,('L08.1')
,('L08.2')
,('L08.3')
,('L08.4')
,('L08.6')
,('L08.7')
,('L08.8')
,('L08.9')
,('L09.1')
,('L09.2')
,('L09.8')
,('L09.9')
,('L12.9')
,('L12.6')
,('L12.8')
,('L12.5')
,('L12.3')
,('L12.4')
,('L10.1')
,('L10.2')
,('L10.3')
,('L10.4')
,('L10.8')
,('L10.9')
,('L12.1')
,('L12.2')
,('L13.2')
,('L13.3')
,('L13.4')
,('L13.5')
,('L13.6')
,('L13.8')
,('L13.9')
,('L16.1')
,('L20.3')
,('L20.5')
,('L20.6')
,('L20.8')
,('L20.9')
,('L20.4')
,('L13.1')
,('L21.1')
,('L21.2')
,('L21.3')
,('L21.4')
,('L21.5')
,('L21.6')
,('L21.8')
,('L21.9')
,('L22.1')
,('L22.2')
,('L22.3')
,('L22.4')
,('L22.8')
,('L22.9')
,('L23.1')
,('L23.2')
,('L23.4')
,('L23.5')
,('L23.3')
,('K50.1')
,('K58.8')
,('K58.9')
,('K59.1')
,('K59.2')
,('K59.3')
,('K59.4')
,('K59.9')
,('K59.5')
,('K59.8')
,('K61.1')
,('K61.2')
,('K61.3')
,('K61.4')
,('K61.5')
,('K61.6')
,('K61.7')
,('K61.8')
,('K61.9')
,('K67.1')
,('K67.8')
,('K67.9')
,('K68.1')
,('K68.2')
,('L01.1')
,('L01.2')
,('L01.3')
,('L01.4')
,('L01.8')
,('L01.9')
,('L02.1')
,('L02.2')
,('L02.4')
,('L02.8')
,('L02.3')
,('L02.9')
,('L03.1')
,('L04.1')
,('L04.8')
,('L04.9')
,('L05.2')
,('L05.3')
,('L05.1')
,('L05.8')
,('L05.9')
,('K38.5')
,('K38.6')
,('K38.8')
,('K38.9')
,('K40.1')
,('K40.2')
,('K40.3')
,('K40.4')
,('K40.8')
,('K40.9')
,('K41.1')
,('K41.2')
,('K41.3')
,('K41.4')
,('K41.8')
,('K41.9')
,('K42.1')
,('K42.2')
,('K42.3')
,('K42.4')
,('K42.8')
,('K42.9')
,('K43.1')
,('K43.2')
,('K43.3')
,('K43.4')
,('K43.8')
,('K44.1')
,('K44.2')
,('K43.9')
,('K44.8')
,('K44.9')
,('K45.1')
,('K45.2')
,('K45.3')
,('K45.4')
,('K45.5')
,('K45.6')
,('K45.8')
,('K45.9')
,('K46.1')
,('K46.2')
,('K46.3')
,('K46.4')
,('K46.5')
,('K46.8')
,('K46.9')
,('K47.1')
,('K47.2')
,('K47.3')
,('K47.4')
,('K47.5')
,('K52.1')
,('K52.2')
,('K52.3')
,('K52.4')
,('K52.5')
,('K52.6')
,('K52.8')
,('K52.9')
,('K53.1')
,('K53.2')
,('K53.8')
,('K53.9')
,('K54.1')
,('K54.2')
,('K54.8')
,('K54.9')
,('K55.1')
,('K55.2')
,('K55.3')
,('K55.4')
,('K55.5')
,('K55.6')
,('K55.8')
,('K55.9')
,('K57.1')
,('K57.2')
,('K57.3')
,('K57.4')
,('K57.5')
,('K57.6')
,('K57.7')
,('K57.8')
,('K57.9')
,('K58.2')
,('K58.1')
,('K18.4')
,('K18.5')
,('K18.6')
,('K18.7')
,('K18.8')
,('K18.9')
,('K19.1')
,('K19.2')
,('K19.3')
,('K19.4')
,('K19.5')
,('K19.6')
,('K19.8')
,('K19.9')
,('K20.1')
,('K20.2')
,('K20.3')
,('K20.4')
,('K20.8')
,('K20.9')
,('K22.1')
,('K22.2')
,('K29.1')
,('K29.2')
,('K29.3')
,('K29.4')
,('K29.5')
,('K29.6')
,('K29.7')
,('K29.8')
,('K29.9')
,('K30.1')
,('K30.2')
,('K30.3')
,('K30.4')
,('K30.5')
,('K30.8')
,('K31.1')
,('K31.2')
,('K30.9')
,('K31.3')
,('K31.4')
,('K31.5')
,('K31.8')
,('K31.9')
,('K32.1')
,('K32.2')
,('K32.3')
,('K32.4')
,('K32.8')
,('K32.9')
,('K33.1')
,('K33.2')
,('K33.3')
,('K33.4')
,('K33.5')
,('K33.6')
,('K33.8')
,('K33.9')
,('K34.1')
,('K34.2')
,('K34.3')
,('K34.4')
,('K34.5')
,('K34.6')
,('K35.1')
,('K35.2')
,('K34.8')
,('K34.9')
,('K35.8')
,('K36.1')
,('K36.2')
,('K36.8')
,('K36.9')
,('K37.1')
,('U19.1')
,('U10.3')
,('U07.2')
,('X82.3')
,('X82.1')
,('X82.2')
,('X82.4')
,('E53.1')
,('E53.2')
,('E53.3')
,('E53.8')
,('E53.9')
,('K01.1')
,('K37.2')
,('K37.3')
,('K37.4')
,('K37.5')
,('K37.6')
,('K37.8')
,('K37.9')
,('K38.1')
,('K38.2')
,('K38.3')
,('K38.4')
,('K47.8')
,('K47.9')
,('K48.1')
,('K48.2')
,('K48.3')
,('K48.4')
,('K48.8')
,('K48.9')
,('K11.1')
,('k11.2')
,('K11.3')
,('K11.4')
,('K11.5')
,('K11.6')
,('K11.7')
,('K11.8')
,('K11.9')
,('K12.1')
,('K12.2')
,('K12.3')
,('K12.4')
,('K12.5')
,('K12.8')
,('K12.9')
,('K13.1')
,('K13.2')
,('K13.3')
,('K13.4')
,('K14.1')
,('K14.2')
,('K14.3')
,('K14.4')
,('K14.5')
,('K14.8')
,('K14.9')
,('K15.1')
,('K15.2')
,('K15.8')
,('K15.9')
,('K16.5')
,('K16.6')
,('K17.1')
,('K17.2')
,('K17.3')
,('K17.4')
,('K17.5')
,('K17.6')
,('K17.7')
,('K17.8')
,('K17.9')
,('K18.1')
,('K22.8')
,('K22.9')
,('K23.1')
,('K23.2')
,('K23.3')
,('K23.4')
,('K23.5')
,('K23.6')
,('K23.8')
,('K23.9')
,('K24.1')
,('K24.3')
,('K24.4')
,('K24.2')
,('K24.5')
,('K24.6')
,('K24.7')
,('K24.8')
,('K24.9')
,('K25.1')
,('K25.2')
,('K25.4')
,('K25.5')
,('K25.3')
,('K25.8')
,('K25.9')
,('K26.1')
,('K26.2')
,('K26.3')
,('K26.4')
,('K26.5')
,('K26.8')
,('K26.9')
,('K27.1')
,('K27.2')
,('K27.3')
,('K27.4')
,('K27.5')
,('K27.6')
,('K27.8')
,('K28.1')
,('K28.2')
,('K28.3')
,('K28.4')
,('K28.5')
,('K28.9')
,('K28.8')
,('K01.2')
,('K01.8')
,('K01.9')
,('K02.1')
,('K02.2')
,('K02.3')
,('K02.4')
,('K02.5')
,('K02.6')
,('K02.8')
,('K02.9')
,('K04.1')
,('K04.2')
,('K04.3')
,('K04.4')
,('K04.5')
,('K04.6')
,('K04.8')
,('K04.9')
,('K05.1')
,('K05.2')
,('K05.8')
,('K05.9')
,('K06.1')
,('K06.2')
,('K06.3')
,('K06.4')
,('K06.8')
,('K06.9')
,('K07.1')
,('K07.2')
,('K07.3')
,('K07.8')
,('K07.9')
,('K08.1')
,('K08.2')
,('K08.3')
,('K08.4')
,('K08.8')
,('K08.9')
,('K09.1')
,('K09.2')
,('K09.3')
,('K09.4')
,('K09.5')
,('K09.6')
,('K09.8')
,('K09.9')
,('K10.1')
,('K18.3')
,('K10.2')
,('K10.3')
,('K10.4')
,('K10.5')
,('K10.8')
,('K10.9')
,('K18.2')

/* All PCTs except Q14 */

--insert into Income.APCSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,ProcedureCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,ProcedureCode
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from @proc
--      ,(
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]

--		where 
--			ROCode like 'Y%'
--		) englishpcts
--where
--	[OrganisationCode] not in (select PCTCode from #q14pct)


/* Q14 with CARD specialty logic */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
	  ,PCTCode
	  ,NationalSpecialtyCode
	  ,ProcedureCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)


select 
	'CEN||PAS'
	,PCTCode
	,NationalSpecialtyCode
	,ProcedureCode
	,'HEART'
	,'1 apr 2012'
	,'31 mar 2013'

from @proc
	,@cardspecialty
	,#q14pct


/* Apply heart flagging to Q14 CSU */

insert into Income.APCSpecialistCommissioningRuleBase
(
      ContextCode
	  ,PCTCode
	  ,PODCode
      ,SpecialistCommissioningCode
      ,EffectiveFromDate
      ,EffectiveToDate
)


select 
	'CEN||PAS'
	,PCTCode
	,'CSU'
	,'HEART'
	,'1 apr 2012'
	,'31 mar 2013'
from
	#q14pct


/* OP */

--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,170
--	,170
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts

--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,170
--	,320
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts


--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,172
--	,170
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts


--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,172
--	,320
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts


--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,173
--	,170
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts


--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN||PAS'
--	,PCTCode = [OrganisationCode]
--	,173
--	,320
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts



--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN|PAS'
--	,PCTCode = [OrganisationCode]
--	,174
--	,170
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts


--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN|PAS'
--	,PCTCode = [OrganisationCode]
--	,174
--	,320
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts


--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN|PAS'
--	,PCTCode = [OrganisationCode]
--	,320
--	,170
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts


--insert into Income.OPSpecialistCommissioningRuleBase
--(
--      ContextCode
--	  ,PCTCode
--	  ,NationalSpecialtyCode
--	  ,MainNationalSpecialtyCode
--      ,SpecialistCommissioningCode
--      ,EffectiveFromDate
--      ,EffectiveToDate
--)


--select 
--	'CEN|PAS'
--	,PCTCode = [OrganisationCode]
--	,320
--	,320
--	,'HEART'
--	,'1 apr 2012'
--	,'31 mar 2013'

--from 
--      (
--		select
--			[OrganisationCode]
--		from
--			[Organisation].[dbo].[PCT]
--		where 
--			ROCode like 'Y%'
--		) englishpcts




--if object_id('[Income].[APCSpecialistCommissioningRuleBaseDoH]') is not null
--drop table [Income].[APCSpecialistCommissioningRuleBaseDoH]

--select 
--	*
--into
--	[Income].[APCSpecialistCommissioningRuleBaseDoH]
--from
--	[Income].[APCSpecialistCommissioningRuleBase]
--where
--	[EffectiveFromDate] >= '1 apr 2012'

--update [Income].[APCSpecialistCommissioningRuleBaseDoH]
--set EffectiveFromDate = '1 jan 1900'
--	,EffectiveToDate = getdate()


--if object_id('[Income].[OPSpecialistCommissioningRuleBaseDoH]') is not null
--drop table [Income].[OPSpecialistCommissioningRuleBaseDoH]

--select 
--	*
--into
--	[Income].OPSpecialistCommissioningRuleBaseDoH
--from
--	[Income].[OPSpecialistCommissioningRuleBase]
--where
--	[EffectiveFromDate] >= '1 apr 2012'

--update [Income].[OPSpecialistCommissioningRuleBaseDoH]
--set EffectiveFromDate = '1 jan 1900'
--	,EffectiveToDate = getdate()




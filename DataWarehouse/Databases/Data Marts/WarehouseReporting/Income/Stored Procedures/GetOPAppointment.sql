﻿
create proc Income.GetOPAppointment

(
@period  varchar(50)
,@position varchar(50)
,@version int
)

as


select
	MonthNo = [MonthNumber]
	,PCT_Code = [PCTCode]
	,Trust_code = [TrustCode]
	,Site_code = [SiteCode]
	,Spec_Code = [SpecialtyCode]
	,POD_Code = [PODCode]
	,GPp_Code = [GpPracticeCode]
	,GP_Code = [GPCode]
	,Pat_Code = [PatientCode]
	,Cons_Code = [ConsultantCode]
	,DOB = [DateOfBirth]
	,ReportMasterID
	,UD1Act_Code = [UserField1]
	,UD2Act_Code = [UserField2]
	,UD3Act_Code = [UserField3]
	,UD4Act_Code = [UserField4]
	,UD5Act_Code = [UserField5]
	,Activity = Activity
	,AttendanceDate = [UserField3]

from
	WarehouseReporting.[Income].[OPAppointment]
where
	Period = @period
	and Position = @position
	and [Version] = @version


﻿
create proc Income.GetIPSpell

(
@period  varchar(50)
,@position varchar(50)
,@version int
)

as

select
	--MonthNo = [MonthNumber]
	PCT_Code = [PCTCode]
	,Trust_code = [TrustCode]
	,Site_code = [SiteCode]
	,Spec_Code = [SpecialtyCode]
	,HRG_Code = [HRGCode]
	,PatClass = PatientClassificationCode
	,AdmMethod = AdmissionMethodCode
	,SpellLos = LoS
	,SpellDischargeDate = DischargeDate
	,SpecService_ID = [SpecialServiceCode1]
	,SpecService_ID_2  = [SpecialServiceCode2]
	,SpecService_ID_3  = [SpecialServiceCode3]
	,SpecService_ID_4 = [SpecialServiceCode3]
	,SpecService_ID_5 = [SpecialServiceCode4]
	,SpecService_ID_6 = [SpecialServiceCode5]
	,SpecService_ID_7 = [SpecialServiceCode7]
	,SpecService_ID_8 = [SpecialServiceCode8]
	,SpecService_ID_9 = [SpecialServiceCode9]
	,SpecService_ID_10 = [SpecialServiceCode10]
	,SpecService_ID_11 = [SpecialServiceCode11]
	,SpecService_ID_12 = [SpecialServiceCode12]
	,SpecService_ID_13 = [SpecialServiceCode13]
	,SpecService_ID_14 = [SpecialServiceCode14]
	,SpecService_ID_15 = [SpecialServiceCode15]
	,SpecService_ID_16 = [SpecialServiceCode16]
	,SpecService_ID_17 = [SpecialServiceCode17]
	,SpecService_ID_18 = [SpecialServiceCode18]
	,SpecService_ID_19 = [SpecialServiceCode19]
	,SpecService_ID_20 = [SpecialServiceCode20]
	,SpecService_ID_21 = [SpecialServiceCode21]
	,GPp_Code = [GpPracticeCode]
	,Pat_Code = [PatientCode]
	,Cons_Code = [ConsultantCode]
	,DOB = [DateOfBirth]
	,ReportMasterID
	,UD1Act_Code = [UserField1]
	,UD2Act_Code = [UserField2]
	,UD3Act_Code = [UserField3]
	,UD4Act_Code = [UserField4]
	,UD5Act_Code = [UserField5]
	,SpellAdmissionDate = AdmissionDate
	--,[Grouping Method] = [GroupingMethodFlag]

from
	WarehouseReporting.Income.APCSpell
where
	Period = @period
	and Position = @position
	and [Version] = @version



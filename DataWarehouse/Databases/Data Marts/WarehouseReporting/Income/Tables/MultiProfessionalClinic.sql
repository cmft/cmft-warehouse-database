﻿CREATE TABLE [Income].[MultiProfessionalClinic] (
    [ConsultantCode]        VARCHAR (50) NOT NULL,
    [TreatmentFunctionCode] VARCHAR (50) NOT NULL,
    [ClinicCode]            VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_MultiProfessionalClinic] PRIMARY KEY CLUSTERED ([ConsultantCode] ASC, [TreatmentFunctionCode] ASC, [ClinicCode] ASC)
);


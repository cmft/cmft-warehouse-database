﻿CREATE TABLE [Income].[ZeroPriceHRG] (
    [HRGCode] CHAR (5)     NOT NULL,
    [PODCode] VARCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([HRGCode] ASC)
);


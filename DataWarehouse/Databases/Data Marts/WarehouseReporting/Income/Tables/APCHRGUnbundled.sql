﻿CREATE TABLE [Income].[APCHRGUnbundled] (
    [UnbundledRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SnapshotRecno]  INT           NULL,
    [Period]         VARCHAR (20)  NULL,
    [Position]       VARCHAR (10)  NULL,
    [Version]        INT           NULL,
    [SUSIdentifier]  VARCHAR (50)  NULL,
    [SequenceNo]     VARCHAR (50)  NULL,
    [HRGCode]        VARCHAR (5)   NULL,
    [DateAdded]      SMALLDATETIME DEFAULT (getdate()) NULL
);


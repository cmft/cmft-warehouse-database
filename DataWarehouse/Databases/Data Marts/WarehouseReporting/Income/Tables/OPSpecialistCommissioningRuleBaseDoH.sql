﻿CREATE TABLE [Income].[OPSpecialistCommissioningRuleBaseDoH] (
    [SpecialistCommissioningRecno] INT          IDENTITY (1, 1) NOT NULL,
    [ContextCode]                  VARCHAR (10) NULL,
    [PCTCode]                      CHAR (3)     NULL,
    [AgeCategoryCode]              CHAR (1)     NULL,
    [AgeCategoryThreshold]         INT          NULL,
    [NationalSpecialtyCode]        VARCHAR (10) NULL,
    [MainNationalSpecialtyCode]    VARCHAR (10) NULL,
    [ClinicCode]                   VARCHAR (10) NULL,
    [CommissioningSerialNo]        VARCHAR (20) NULL,
    [SpecialistCommissioningCode]  VARCHAR (20) NULL,
    [EffectiveFromDate]            DATE         NULL,
    [EffectiveToDate]              DATE         NULL,
    [DateAdded]                    DATE         NULL
);


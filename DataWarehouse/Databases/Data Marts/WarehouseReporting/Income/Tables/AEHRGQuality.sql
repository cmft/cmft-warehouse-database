﻿CREATE TABLE [Income].[AEHRGQuality] (
    [QualityRecno]    INT           IDENTITY (1, 1) NOT NULL,
    [SnapshotRecno]   INT           NULL,
    [Period]          VARCHAR (20)  NULL,
    [Position]        VARCHAR (10)  NULL,
    [Version]         INT           NULL,
    [ReportMasterID]  VARCHAR (50)  NULL,
    [QualityTypeCode] VARCHAR (50)  NULL,
    [QualityCode]     VARCHAR (50)  NULL,
    [QualityMessage]  VARCHAR (255) NULL,
    [DateAdded]       SMALLDATETIME DEFAULT (getdate()) NULL
);


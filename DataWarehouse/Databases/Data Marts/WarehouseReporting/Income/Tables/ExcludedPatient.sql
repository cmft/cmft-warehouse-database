﻿CREATE TABLE [Income].[ExcludedPatient] (
    [SourcePatientNo]   VARCHAR (50)  NOT NULL,
    [ExclusionType]     VARCHAR (50)  NULL,
    [EffectiveFromDate] DATE          NULL,
    [EffectiveToDate]   DATE          NULL,
    [DateAdded]         SMALLDATETIME DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC)
);


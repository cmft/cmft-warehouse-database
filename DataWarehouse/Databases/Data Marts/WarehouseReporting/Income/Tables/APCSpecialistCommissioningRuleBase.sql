﻿CREATE TABLE [Income].[APCSpecialistCommissioningRuleBase] (
    [SpecialistCommissioningRecno] INT          IDENTITY (1, 1) NOT NULL,
    [ContextCode]                  VARCHAR (10) NULL,
    [PCTCode]                      CHAR (3)     NULL,
    [AgeCategoryCode]              CHAR (1)     NULL,
    [AgeCategoryThreshold]         INT          NULL,
    [NationalSpecialtyCode]        VARCHAR (10) NULL,
    [HRGCode]                      CHAR (8)     NULL,
    [PODCode]                      VARCHAR (10) NULL,
    [ProcedureCode]                VARCHAR (5)  NULL,
    [DiagnosisCode]                VARCHAR (5)  NULL,
    [PatientCategoryCode]          VARCHAR (5)  NULL,
    [CommissioningSerialNo]        VARCHAR (20) NULL,
    [SpecialistCommissioningCode]  VARCHAR (20) NULL,
    [EffectiveFromDate]            DATE         NULL,
    [EffectiveToDate]              DATE         NULL,
    [DateAdded]                    DATE         DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([SpecialistCommissioningRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_APCSpecialistCommissioningRuleBase]
    ON [Income].[APCSpecialistCommissioningRuleBase]([ContextCode] ASC, [PCTCode] ASC, [AgeCategoryCode] ASC, [NationalSpecialtyCode] ASC, [HRGCode] ASC, [PODCode] ASC, [ProcedureCode] ASC, [DiagnosisCode] ASC, [PatientCategoryCode] ASC, [CommissioningSerialNo] ASC, [EffectiveFromDate] ASC, [EffectiveToDate] ASC);


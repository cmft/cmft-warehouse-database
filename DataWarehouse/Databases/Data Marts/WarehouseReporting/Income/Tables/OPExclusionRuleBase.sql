﻿CREATE TABLE [Income].[OPExclusionRuleBase] (
    [ExclusionRecno]         INT          IDENTITY (1, 1) NOT NULL,
    [ContextCode]            VARCHAR (10) NULL,
    [PCTCode]                CHAR (3)     NULL,
    [PASSpecialtyCode]       VARCHAR (10) NULL,
    [NationalSpecialtyCode]  CHAR (3)     NULL,
    [ClinicCode]             VARCHAR (10) NULL,
    [FirstAttendanceFlag]    CHAR (1)     NULL,
    [PrimaryProcedureCode]   VARCHAR (10) NULL,
    [DiagnosisFlagCode]      CHAR (1)     NULL,
    [CommissioningSerialNo]  VARCHAR (20) NULL,
    [LocalAdminCategoryCode] VARCHAR (20) NULL,
    [ExclusionCode]          VARCHAR (20) NULL,
    [EffectiveFromDate]      DATE         DEFAULT ('01/01/1900') NULL,
    [EffectiveToDate]        DATE         NULL,
    [DateAdded]              DATE         DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([ExclusionRecno] ASC)
);


﻿CREATE TABLE [Income].[HRGUplift] (
    [Period]                VARCHAR (50)  NULL,
    [Position]              VARCHAR (50)  NULL,
    [Version]               INT           NULL,
    [EncounterRecno]        INT           NOT NULL,
    [SourceUniqueID]        VARCHAR (50)  NOT NULL,
    [CasenoteNumber]        VARCHAR (50)  NOT NULL,
    [AdmissionTime]         SMALLDATETIME NULL,
    [PatientAge]            NUMERIC (18)  NULL,
    [EndDirectorateCode]    VARCHAR (10)  NULL,
    [SpecialtyCode]         VARCHAR (10)  NULL,
    [ConsultantCode]        VARCHAR (10)  NULL,
    [HRGCode]               VARCHAR (50)  NULL,
    [PreviousDiagnosisCode] VARCHAR (10)  NULL,
    [PreviousAdmissionTime] SMALLDATETIME NULL,
    [OpportunityFlag]       VARCHAR (50)  NULL
);


﻿CREATE TABLE [Income].[HRGGroupToSplit] (
    [HRGRoot]            VARCHAR (10)  NULL,
    [HRGRootDescription] VARCHAR (255) NULL,
    [HRGValue]           VARCHAR (10)  NULL,
    [FlagValue]          VARCHAR (50)  NULL,
    [SequenceNo]         INT           NULL
);


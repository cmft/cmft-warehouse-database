﻿CREATE TABLE [Income].[OPHRGQuality] (
    [QualityRecno]    INT           IDENTITY (1, 1) NOT NULL,
    [SnapshotRecno]   INT           NULL,
    [Period]          VARCHAR (20)  NULL,
    [Position]        VARCHAR (10)  NULL,
    [Version]         INT           NULL,
    [SUSIdentifier]   VARCHAR (243) NULL,
    [QualityTypeCode] VARCHAR (50)  NULL,
    [QualityCode]     VARCHAR (50)  NULL,
    [QualityMessage]  VARCHAR (255) NULL,
    [DateAdded]       SMALLDATETIME DEFAULT (getdate()) NULL
);


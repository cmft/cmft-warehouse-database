﻿CREATE TABLE [Income].[APCSpecialistCommissioningRuleBaseDoH] (
    [SpecialistCommissioningRecno] INT          IDENTITY (1, 1) NOT NULL,
    [ContextCode]                  VARCHAR (10) NULL,
    [PCTCode]                      CHAR (3)     NULL,
    [AgeCategoryCode]              CHAR (1)     NULL,
    [AgeCategoryThreshold]         INT          NULL,
    [NationalSpecialtyCode]        INT          NULL,
    [HRGCode]                      CHAR (8)     NULL,
    [PODCode]                      VARCHAR (10) NULL,
    [ProcedureCode]                VARCHAR (5)  NULL,
    [PatientCategoryCode]          VARCHAR (5)  NULL,
    [CommissioningSerialNo]        VARCHAR (20) NULL,
    [SpecialistCommissioningCode]  VARCHAR (20) NULL,
    [EffectiveFromDate]            DATE         NULL,
    [EffectiveToDate]              DATE         NULL,
    [DateAdded]                    DATE         NULL
);


﻿CREATE TABLE [Income].[BedDayPODAndHRG] (
    [WardCode]        VARCHAR (50) NOT NULL,
    [PODCode]         VARCHAR (50) NULL,
    [HRGCode]         VARCHAR (10) NULL,
    [SpecialtyCode]   VARCHAR (5)  NULL,
    [DirectorateCode] VARCHAR (5)  NULL,
    CONSTRAINT [PK_Ward] PRIMARY KEY CLUSTERED ([WardCode] ASC)
);


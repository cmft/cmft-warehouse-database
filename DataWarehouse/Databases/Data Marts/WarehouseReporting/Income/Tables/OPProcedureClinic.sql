﻿CREATE TABLE [Income].[OPProcedureClinic] (
    [ClinicCode] VARCHAR (10) NOT NULL,
    [HRGCode]    VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_OPPROCClinic] PRIMARY KEY CLUSTERED ([ClinicCode] ASC)
);


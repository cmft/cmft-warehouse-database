﻿

CREATE VIEW [ORG].[PCT] as

SELECT 
	 PCT.[Organisation Code] PCTCode
	,PCT.[Organisation Name] PCT
	,PCT.[RO Code] ROCode
	,PCT.[HA Code] SHACode
	,SHA.[Organisation Name] SHA
	,PCT.[Address Line 1] Address1
	,PCT.[Address Line 2] Address2
	,PCT.[Address Line 3] Address3
	,PCT.[Address Line 4] Address4
	,PCT.[Address Line 5] Address5
	,PCT.[Postcode] Postcode
	,PCT.[Open Date] OpenDate
	,PCT.[Close Date] CloseDate
	,PCT.[Status Code] StatusCode
	,PCT.[Organisation Sub Type Code] OrganisationSubTypeCode
	,PCT.[Parent Organisation Code] ParentOrganisationCode
	,PCT.[Join Parent Date] JoinParentDate
	,PCT.[Left Parent Date] LeftParentDate
	,PCT.[Contact Telephone Number] ContactTelNo
	,PCT.[Contact Name] ContactName
	,PCT.[Address Type] AddressType
	,PCT.[Amended Record Indicator] AmendedRecordIndicator
	,PCT.[Wave Number] WaveNumber
	,PCT.[Current GPFH or PCG Code] CurrentGPFHorPCGCode
	,PCT.[Current GPFH Type] CurrentGPFHType
FROM
	[Organisation].[dbo].[Primary Care Organisation] PCT
LEFT OUTER JOIN [Organisation].[dbo].[Health Authority] SHA
	ON PCT.[HA Code] = SHA.[Organisation Code]



﻿




CREATE view [AE].[DischargeSummaryICETest]

as
SELECT  [DischargeSummaryRecno]
      ,[DistrictNo]
      ,[PatientName]
      ,[RegisteredGpPracticeCode]
      ,[DepartureTime]
      ,[TargetTime]
      ,[ActualTime]
      ,[Breach]
      ,[DischargeSummaryXML]
      ,[Updated]
  FROM [Warehouse].[AE].[DischargeSummary]

where
	cast(DepartureTime as date) = '24 Jun 2015'


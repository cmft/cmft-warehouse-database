﻿CREATE VIEW [AE].[AttendanceDisposal]

AS 
 
SELECT
	   AttendanceDisposalCode
      ,AttendanceDisposal
      ,AttendanceDisposalNationalCode
FROM
	WarehouseReporting.ETL.BaseAEAttendanceDisposal

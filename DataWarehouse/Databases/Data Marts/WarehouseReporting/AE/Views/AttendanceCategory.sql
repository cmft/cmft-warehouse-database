﻿CREATE VIEW [AE].[AttendanceCategory]

AS 
 
SELECT
	     AttendanceCategoryCode
        ,AttendanceCategory
        ,AttendanceCategoryGroup
FROM
	WarehouseReporting.ETL.BaseAEAttendanceCategory

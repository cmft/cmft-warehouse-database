﻿CREATE VIEW [AE].[ArrivalMode]

AS 
 
SELECT
	     ArrivalModeCode
		,ArrivalMode
FROM
	WarehouseReporting.ETL.BaseAEArrivalMode

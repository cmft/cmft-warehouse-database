﻿CREATE VIEW [AE].[Fact]

AS

SELECT [EncounterRecno]
      ,[SourceUniqueID]
      ,[StageCode]
      ,[ArrivalModeCode]
      ,[AttendanceDisposalCode]
      ,[SiteCode]
      ,[RegisteredPracticeCode]
      ,[AttendanceCategoryCode]
      ,[ReferredToSpecialtyCode]
      ,[LevelOfCareCode]
      ,[EncounterBreachStatusCode]
      ,[EncounterStartTimeOfDay]
      ,[EncounterEndTimeOfDay]
      ,[EncounterStartDate]
      ,[EncounterEndDate]
      ,[StageBreachStatusCode]
      ,[StageDurationMinutes]
      ,[StageBreachMinutes]
      ,[StageStartTimeOfDay]
      ,[StageEndTimeOfDay]
      ,[StageStartDate]
      ,[StageEndDate]
      ,[Cases]
      ,[BreachValue]
      ,[DelayCode]
      ,[EncounterDurationMinutes]
      ,[AgeCode]
      ,[UnplannedReattend7DayCases]
      ,[LeftWithoutBeingSeenCases]
      ,[StageStartTime]
      ,[StageEndTime]
      ,[StageDurationBandCode]
      ,[EncounterDurationBandCode]
      ,[Reportable]
  FROM [WarehouseOLAP].[dbo].[FactAE]

﻿
create view ae.StaffMember as
 
SELECT 
	StaffMemberCode
	,Surname
	,Forename
	,MiddleNames
	,Title
	,JobTypeCode
	,StartDate
	,EndDate
	,SpecialtyCode
	,JobTitle
	,LoginId
FROM
	 Warehouse.AE.StaffMember
﻿
CREATE VIEW [AE].[Diagnosis]

AS 


SELECT
	 DiagnosisRecno
	,SourceUniqueID
	,SequenceNo
	,DiagnosticSchemeInUse
	,DiagnosisCode
	,AESourceUniqueID
	,SourceDiagnosisCode
	,SourceDiagnosisSiteCode
	,SourceDiagnosisSideCode
	,SourceSequenceNo
	,DiagnosisSiteCode
	,DiagnosisSideCode
FROM
	Warehouse.AE.Diagnosis
 
--SELECT
--	   ReferenceCode
--      ,Reference
--      ,ReferenceParentCode
--      ,DisplayOrder
--FROM
--	WarehouseReporting.ETL.BaseAEDiagnosis


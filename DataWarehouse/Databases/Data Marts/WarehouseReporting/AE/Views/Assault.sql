﻿

CREATE VIEW [AE].[Assault]

AS

SELECT [AssaultRecno]
      ,[SourceUniqueID]
      ,[AESourceUniqueID]
      ,[AssaultDate]
      ,[AssaultTime]
      ,[AssaultWeapon]
      ,[AssaultWeaponDetails]
      ,[AssaultLocation]
      ,[AssaultLocationDetails]
      ,[AlcoholConsumed3Hour]
      ,[AssaultRelationship]
      ,[AssaultRelationshipDetails]
  FROM [Warehouse].[AE].[Assault]




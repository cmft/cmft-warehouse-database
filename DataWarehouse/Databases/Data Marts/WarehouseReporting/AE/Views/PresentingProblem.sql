﻿
create view ae.PresentingProblem as

SELECT
 	 PresentingProblemCode
	,PresentingProblem
	,PresentingProblemNationalCode
FROM
	 Warehouse.AE.PresentingProblem
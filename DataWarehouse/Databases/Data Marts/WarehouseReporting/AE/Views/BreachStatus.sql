﻿CREATE VIEW [AE].[BreachStatus]

AS 
 
SELECT
	 BreachStatusCode
	,BreachStatus
FROM
	WarehouseOLAP.dbo.OlapAEBreachStatus

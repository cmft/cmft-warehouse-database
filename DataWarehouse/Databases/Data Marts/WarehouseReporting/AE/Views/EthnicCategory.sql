﻿CREATE VIEW AE.EthnicCategory AS

SELECT
	 EthnicCategoryCode
	,EthnicCategory
	,EthnicCategoryNationalCode
FROM
	warehouse.AE.EthnicCategory
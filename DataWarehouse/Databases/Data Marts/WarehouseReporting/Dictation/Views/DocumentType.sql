﻿create view [Dictation].[DocumentType]

as

select
	 DocumentTypeCode
	,DocumentType
from
	Warehouse.Dictation.DocumentType

﻿create view [Dictation].[Worktype]

as

select
	WorkTypeCode
	,WorkType
from
	Warehouse.Dictation.Worktype

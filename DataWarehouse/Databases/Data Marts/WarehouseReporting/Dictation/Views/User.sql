﻿create view [Dictation].[User]

as

select 
	UserCode
	,[User]
	,HasVRLicence
from 
	Warehouse.Dictation.[User]

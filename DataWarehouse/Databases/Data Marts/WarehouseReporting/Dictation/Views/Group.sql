﻿

create view [Dictation].[Group]

as

select
	 [GroupCode]
	,[Group]
	,[SpecialtyCode]
	,[Specialty]
	,[Division]
from
	Warehouse.Dictation.[Group]
	


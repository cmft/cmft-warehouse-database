﻿create view [Dictation].[Document]

as

select
DocumentRecNo
	,SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length]
	,[DocumentTypeCode]
	,[Priority]
	,[StatusCode]
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate]
	,[CreationTime]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate]
	,[CorrectionTime]
	,[CorrectionTimeTaken]
	,[WaitingTimeAuthorization]
	,[AuthorisationDate]
	,[AuthorisationTime]
	,[PassThroughTimeTaken]
	,[ContextCode]
	,[DictationTimeTaken]
	,[CorrectionRate]
	,WarehouseInterfaceCode
from 
	[WarehouseOLAP].dbo.BaseDictationDocument

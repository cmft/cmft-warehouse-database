﻿create view [Dictation].[Workstation]

as

select
	WorkStationCode
	,Workstation
from
	Warehouse.Dictation.Workstation

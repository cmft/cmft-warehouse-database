﻿create view [Dictation].[Status]

as

select
	StatusCode
	,Status 
from
	Warehouse.Dictation.[Status]

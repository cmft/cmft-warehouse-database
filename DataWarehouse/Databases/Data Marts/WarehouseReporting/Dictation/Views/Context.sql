﻿create view [Dictation].[Context]

as

select
	 ContextCode
	,Context
from
	Warehouse.Dictation.Context

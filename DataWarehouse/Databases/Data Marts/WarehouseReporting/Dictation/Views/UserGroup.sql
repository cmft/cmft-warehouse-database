﻿create view [Dictation].[UserGroup]

as

select
	[user].UserCode
	,[User]
	,usergroup.GroupCode
	,[Group]
	,SpecialtyCode
	,Specialty
	,Division
	
from
	Warehouse.Dictation.[User] [user]
	left join Warehouse.Dictation.UserGroup usergroup
	on [user].UserCode = usergroup.UserCode
	left join Warehouse.Dictation.[Group] [group]
	on [group].GroupCode = usergroup.GroupCode

﻿CREATE VIEW [WH].[Practice]

AS 
 
SELECT
		   PracticeCode
		  ,PCTCode
		  ,HealthAuthorityCode
		  ,Practice
		  ,PCT
		  ,HealthAuthority
		  ,LocalPCT
FROM
	WarehouseReporting.[ETL].[BASEWHPractice]
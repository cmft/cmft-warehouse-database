﻿
create view WH.PatientFlag as

select
	[DistrictNo]
	,[FlagCode]
	,[Created]
	,[Updated]
	,[ByWhom]
from
	 [Warehouse].[WH].[PatientFlag]
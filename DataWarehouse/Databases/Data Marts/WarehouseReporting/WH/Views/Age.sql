﻿CREATE VIEW [WH].[Age]

AS 
 
SELECT
	 AgeCode
	,Age
	,AgeParentCode
FROM
	WarehouseOLAP.dbo.OlapAge

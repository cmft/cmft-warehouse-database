﻿


CREATE VIEW [WH].[Interface]

AS

SELECT 
	 InterfaceCode
	,Interface
	 ,InterfaceGroupCode
	,InterfaceGroup
FROM
	WarehouseOLAP.dbo.OlapInterface


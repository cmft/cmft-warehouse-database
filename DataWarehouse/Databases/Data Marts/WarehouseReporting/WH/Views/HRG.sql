﻿CREATE VIEW [WH].[HRG]

AS 
 
 
SELECT
	   HRGCode
      ,HRG
      ,HRGChapterCode
      ,HRGChapter
FROM
	WarehouseReporting.ETL.BaseWHHRG

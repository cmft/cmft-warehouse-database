﻿CREATE VIEW [WH].[Sex]

AS 
 
SELECT
	 SexCode
	,Sex
FROM
	WarehouseOLAP.dbo.OlapSex

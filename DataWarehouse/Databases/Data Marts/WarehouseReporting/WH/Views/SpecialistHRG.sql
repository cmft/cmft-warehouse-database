﻿
CREATE view WH.[SpecialistHRG]

as

select
	HRGCode = EntityCode
	,[SpecialistHRGGroupCode] = XrefEntityCode
from
	Warehouse.[dbo].[EntityXref]
where
	[EntityTypeCode] = 'HRGCODE'
and
	[XrefEntityTypeCode] = 'SPECIALISTHRGGROUP'
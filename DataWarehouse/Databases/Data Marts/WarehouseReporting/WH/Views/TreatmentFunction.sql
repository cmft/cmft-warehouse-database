﻿

CREATE view [WH].[TreatmentFunction]

as

select
      [SpecialtyCode]
      ,[Specialty]
      --,[SpecialtyParentCode]
from
      Warehouse.WH.TreatmentFunction

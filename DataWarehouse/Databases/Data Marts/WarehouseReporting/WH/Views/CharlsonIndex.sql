﻿


CREATE view [WH].[CharlsonIndex] 


as

select
	DiagnosisCode = CHARLSONINDEXCODE.EntityCode
	,CharlsonConditionCode = CHARLSONINDEXCODE.XrefEntityCode
	,CharlsonCondition = EntityLookup.[Description]
	,NewWeight = CHARLSONINDEXNEWWEIGHT.XrefEntityCode
	,OldWeight = CHARLSONINDEXOLDWEIGHT.XrefEntityCode
	
from
	warehouse.[dbo].[EntityXref] CHARLSONINDEXCODE

inner join
	(

select
	EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
from
	warehouse.[dbo].[EntityXref]
where
	EntityTypeCode = 'CHARLSONINDEXCODE'
and
	[XrefEntityTypeCode] = 'CHARLSONINDEXNEWWEIGHT'

	) CHARLSONINDEXNEWWEIGHT

on	CHARLSONINDEXNEWWEIGHT.EntityCode = CHARLSONINDEXCODE.XrefEntityCode

inner join
	(

select
	EntityTypeCode
	,EntityCode
	,XrefEntityTypeCode
	,XrefEntityCode
from
	warehouse.[dbo].[EntityXref]
where
	EntityTypeCode = 'CHARLSONINDEXCODE'
and
	[XrefEntityTypeCode] = 'CHARLSONINDEXOLDWEIGHT'

	) CHARLSONINDEXOLDWEIGHT

on	CHARLSONINDEXOLDWEIGHT.EntityCode = CHARLSONINDEXCODE.XrefEntityCode


inner join
	warehouse.[dbo].[EntityLookup] EntityLookup
on	EntityLookup.EntityCode = CHARLSONINDEXNEWWEIGHT.EntityCode
and EntityLookup.EntityTypeCode = 'CHARLSONINDEX'
	


where
	CHARLSONINDEXCODE.EntityTypeCode = 'DIAGNOSISCODE'
and
	CHARLSONINDEXCODE.[XrefEntityTypeCode] = 'CHARLSONINDEXCODE'



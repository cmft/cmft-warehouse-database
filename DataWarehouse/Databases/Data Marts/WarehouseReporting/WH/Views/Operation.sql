﻿CREATE VIEW [WH].[Operation]

AS 
 
SELECT
	 OperationCode
	,Operation
	,OperationGroup
	,OperationChapter
FROM
	WarehouseReporting.ETL.BaseWHOperation

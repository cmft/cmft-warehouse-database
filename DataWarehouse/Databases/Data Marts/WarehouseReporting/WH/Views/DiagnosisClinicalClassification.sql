﻿
CREATE view WH.DiagnosisClinicalClassification

as

select
	DiagnosisCode = ClinicalClassificationMap.EntityCode
	,ClinicalClassificationCode = ClinicalClassification.[EntityCode]
	,ClinicalClassification = ClinicalClassification.[Description]
	,SMHICode = ClinicalClassificationSHMIMap.[EntityCode]
from
	Warehouse.[dbo].[EntityXref] ClinicalClassificationMap

inner join
	Warehouse.[dbo].[EntityLookup] ClinicalClassification
on	ClinicalClassificationMap.[XrefEntityCode] = ClinicalClassification.[EntityCode]
and ClinicalClassification.[EntityTypeCode] = 'CCSCATEGORYCODE'

inner join
	Warehouse.[dbo].[EntityXref] ClinicalClassificationSHMIMap
on	ClinicalClassificationSHMIMap.[XRefEntityCode] = ClinicalClassification.[EntityCode] 
and ClinicalClassificationSHMIMap.[EntityTypeCode] = 'SHMICCSCATEGORYCODE'

where 
	ClinicalClassificationMap.[EntityTypeCode] = 'DIAGNOSISCODE'
and 
	ClinicalClassificationMap.[XrefEntityTypeCode] = 'CCSCATEGORYCODE'

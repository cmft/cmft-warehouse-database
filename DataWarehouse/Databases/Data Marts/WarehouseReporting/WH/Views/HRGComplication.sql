﻿
create view WH.HRGComplication

as

select
       HRGListID = [EntityCode]
       ,HRGSubChapter = left([EntityCode],charindex('_',[EntityCode]) - 1)
       ,DiagnosisCode = [XrefEntityCode]

from 
       Warehouse.dbo.EntityXref
where
       [EntityTypeCode] = 'HRGCCLISTID'
and
       [XrefEntityTypeCode] = 'DIAGNOSISCODE'

﻿

CREATE VIEW [WH].[Division]

AS

SELECT 
	 DivisionCode
	,Division
FROM 
	warehouse.WH.Division

﻿CREATE VIEW [WH].[Calendar]

AS 
 
SELECT 
	   [TheDate]
      ,[DayOfWeek]
      ,[DayOfWeekKey]
      ,[LongDate]
      ,[TheMonth]
      ,[FinancialQuarter]
      ,[FinancialYear]
      ,[FinancialMonthKey]
      ,[FinancialQuarterKey]
      ,[CalendarQuarter]
      ,[CalendarSemester]
      ,[CalendarYear]
      ,[LastCompleteMonth]
      ,[WeekNoKey]
      ,[WeekNo]
      ,[FirstDayOfWeek]
      ,[LastDayOfWeek]
      
FROM 
	   [WarehouseOLAP].[dbo].[OlapCalendar]

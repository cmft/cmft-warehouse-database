﻿

CREATE VIEW [WH].[Directorate]

AS

SELECT 
 	 DirectorateCode
	,Directorate
	,DivisionCode
FROM 
	warehouse.WH.Directorate

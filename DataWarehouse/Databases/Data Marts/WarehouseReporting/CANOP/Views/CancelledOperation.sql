﻿


CREATE VIEW CANOP.CancelledOperation

AS 

SELECT [SourceUniqueID]
      ,[CasenoteNumber]
      ,[DirectorateCode]
      ,[Directorate]
      ,[SpecialtyCode]
      ,[Specialty]
      ,[Consultant]
      ,[AdmissionDate]
      ,[ProcedureDate]
      ,[Ward]
      ,[Anaesthetist]
      ,[Surgeon]
      ,[Procedure]
      ,[CancellationDate]
      ,[CancellationReason]
      ,[TCIDate]
      ,[Comments]
      ,[ProcedureCompleted]
      ,[BreachwatchComments]
      ,[Removed]
      ,[RemovedComments]
      ,[ReportableBreach]
  FROM [WarehouseOLAP].[dbo].[BaseCancelledOperation]


﻿/****** Object:  DatabaseRole [NarrativeWriter]    Script Date: 25/08/2015 14:36:55 ******/
CREATE ROLE [NarrativeWriter]
GO
ALTER ROLE [NarrativeWriter] ADD MEMBER [CMMC\peter.graham]
GO
ALTER ROLE [NarrativeWriter] ADD MEMBER [CMMC\Helen.Shackleton]
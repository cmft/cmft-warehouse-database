﻿ALTER ROLE [db_datareader] ADD MEMBER [CMMC\SQL.Service.BA]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Sec - Warehouse SQL Divisional Analysts]
GO
ALTER ROLE [db_owner] ADD MEMBER [CMMC\Sec - Warehouse BI Developers]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Sec - Warehouse BI Developers]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [CMMC\Sec - Warehouse BI Developers]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Sec - SP Information]
GO
ALTER ROLE [db_owner] ADD MEMBER [CMMC\Phil.Orrell]
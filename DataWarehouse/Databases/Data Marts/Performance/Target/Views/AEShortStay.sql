﻿CREATE view [Target].[AEShortStay]


as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.4

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.AEShortStay'

--where
--	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1
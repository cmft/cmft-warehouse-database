﻿CREATE view [Target].[ShelfordGroupSHMIExpectedDeathsBest]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.041

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupSHMIExpectedDeathsBest'

where
	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1
﻿CREATE view [Target].[FFTAEResponseRate]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = case 
				when TheDate <'20150101' then 0.15
				else 0.2
			end
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.FFTAEResponseRate'

where
	TheDate <'20150401' --end dated as taregt doesnt exist CH 11052015
﻿CREATE view [Target].[ElectiveActualVsContractPlan]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ElectiveActualVsContractPlan'


--where
--	TheDate between '1 apr 2009' and getdate()
﻿CREATE view [Target].[CancerTreatedWithin62DaysOfUrgentGPReferral]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.85

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CancerTreatedWithin62DaysOfUrgentGPReferral'

--where
--	TheDate between '1 apr 2009' and getdate()
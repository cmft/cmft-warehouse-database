﻿CREATE view [Target].[RTTNAPC]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.95

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.RTTNAPC'

--where
--	TheDate between '1 apr 2009' and getdate()
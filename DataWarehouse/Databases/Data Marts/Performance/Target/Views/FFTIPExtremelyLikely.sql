﻿CREATE view [Target].[FFTIPExtremelyLikely]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.75
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.FFTIPExtremelyLikely'

--where
--	TheDate between '1 apr 2009' and getdate()
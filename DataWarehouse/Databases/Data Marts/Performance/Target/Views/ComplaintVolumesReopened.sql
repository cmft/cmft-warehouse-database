﻿CREATE view [Target].[ComplaintVolumesReopened]

as

/*	
	Author:	Paul Egan
	Date:	01/09/2014
*/

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 1
from
	dbo.Calendar	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ComplaintVolumesReopened'

--where
--	TheDate between '1 apr 2009' and getdate()
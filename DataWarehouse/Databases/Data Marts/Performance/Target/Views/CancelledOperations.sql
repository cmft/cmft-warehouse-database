﻿CREATE view [Target].[CancelledOperations]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.008
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CancelledOperations'


--where
--	TheDate between '1 apr 2009' and getdate()
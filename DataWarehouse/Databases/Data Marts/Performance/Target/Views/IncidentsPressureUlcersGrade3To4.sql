﻿CREATE view [Target].[IncidentsPressureUlcersGrade3To4]

as

/* Created Paul Egan 22/09/2014 */

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.IncidentsPressureUlcersGrade3To4'

where day(TheDate)=1
--where
--	TheDate between '1 apr 2009' and getdate()
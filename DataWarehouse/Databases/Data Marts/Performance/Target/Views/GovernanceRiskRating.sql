﻿CREATE view [Target].[GovernanceRiskRating]
as

select
	 TargetID = Target.TargetID
	,DateID 
	,Value = 3 -- Green

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.GovernanceRiskRating'
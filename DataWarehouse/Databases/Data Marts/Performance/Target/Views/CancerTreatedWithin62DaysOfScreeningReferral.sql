﻿CREATE view [Target].[CancerTreatedWithin62DaysOfScreeningReferral]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.9

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CancerTreatedWithin62DaysOfScreeningReferral'

--where
--	TheDate between '1 apr 2009' and getdate()
﻿CREATE view [Target].[ShelfordGroupComorbiditesBest]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.338

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupComorbiditesBest'

where
	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1
﻿CREATE view [Target].[RTTIncomplete]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.92

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.RTTIncomplete'

--where
--	TheDate between '1 apr 2009' and getdate()
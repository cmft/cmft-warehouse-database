﻿create view [Target].[EWSResponseRate]

as

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.75

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.EWSResponseRate'

--where
--	TheDate between '1 apr 2009' and getdate()
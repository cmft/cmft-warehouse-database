﻿CREATE view [Target].[CQCRisk]

as

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 5

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CQCRisk'
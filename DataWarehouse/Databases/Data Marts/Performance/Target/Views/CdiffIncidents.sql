﻿CREATE view [Target].[CdiffIncidents]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 5.5
	
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CdiffIncidents'

where day(TheDate) = 1

--where
--	TheDate between '1 apr 2009' and getdate()
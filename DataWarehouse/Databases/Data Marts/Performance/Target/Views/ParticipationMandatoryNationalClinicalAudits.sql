﻿CREATE view [Target].[ParticipationMandatoryNationalClinicalAudits]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 1
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ParticipationMandatoryNationalClinicalAudits'


--where
--	TheDate between '1 apr 2009' and getdate()
﻿CREATE view [Target].[ComplaintsUnresolvedWithin40Days]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.2
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ComplaintsUnresolvedWithin40Days'

--where
--	TheDate between '1 apr 2009' and getdate()
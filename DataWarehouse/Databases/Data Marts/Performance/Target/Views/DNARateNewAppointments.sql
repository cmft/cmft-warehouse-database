﻿CREATE view [Target].[DNARateNewAppointments]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.085
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.DNARateNewAppointments'


--where
--	TheDate between '1 apr 2009' and getdate()
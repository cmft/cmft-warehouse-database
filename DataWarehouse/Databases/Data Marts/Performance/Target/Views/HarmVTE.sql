﻿CREATE view [Target].[HarmVTE]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.HarmVTE'

--where
--	TheDate between '1 apr 2009' and getdate()
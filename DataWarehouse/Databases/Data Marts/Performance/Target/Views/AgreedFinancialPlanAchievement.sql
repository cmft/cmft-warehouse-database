﻿create view [Target].[AgreedFinancialPlanAchievement]
as

select
	 TargetID = Target.TargetID
	,DateID 
	,Value = 3

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.AgreedFinancialPlanAchievement'
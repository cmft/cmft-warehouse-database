﻿CREATE view [Target].[AEAverageLOS]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 5.9

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.AEAAverageLOS'

--where
--	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1
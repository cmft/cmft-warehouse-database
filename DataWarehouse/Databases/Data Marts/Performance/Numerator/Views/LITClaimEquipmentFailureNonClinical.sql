﻿create view [Numerator].[LITClaimEquipmentFailureNonClinical] as

	select
		DatasetID = D.DatasetID
		,DatasetRecno = D.DatasetRecno
		,NumeratorID = Numerator.NumeratorID
		,SiteID = D.EndSiteID		
		,DirectorateID = D.EndDirectorateID
		,SpecialtyID = D.SpecialtyID
		,ClinicianID = D.ClinicianID
		,DateID = D.AdvisedDateID
		,D.ServicePointID
		,Value = 1
	from WrkDataset D

	inner join dbo.Dataset
	on	Dataset.DatasetID = D.DatasetID

	inner join dbo.Numerator
	on	NumeratorLogic = 'Numerator.LITClaimEquipmentFailureNonClinical'
		
	where Dataset.DatasetCode = 'LIT'
	and D.CategoryTypeCode collate Latin1_General_CS_AI = 'g'
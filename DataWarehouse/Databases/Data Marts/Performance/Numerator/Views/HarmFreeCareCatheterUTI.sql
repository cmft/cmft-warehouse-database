﻿CREATE view [Numerator].[HarmFreeCareCatheterUTI] as

-- BedMan extract data includes only Events causing actual harm 

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.SymptomStartDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCareCatheterUTI'
	
where
	DatasetCode = 'UTI'
and WrkDataset.SourceCatheterTypeCode = '2' -- Urethral Catheter
and WrkDataset.SymptomStartDate >= WrkDataset.AdmissionDate
and	WrkDataset.TreatmentStartDate is not null
﻿CREATE view [Numerator].[ExpectedDeathExcludingDayCaseRegularDayNight]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(d.EndSiteID,d.StartSiteID)
	,DirectorateID = coalesce(d.EndDirectorateID,d.StartDirectorateID)
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EpisodeEndDateID
	,ServicePointID
	,Value = RiskScore
from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ExpectedDeathExcludingDayCaseRegularDayNight'

where
	DatasetCode = 'APC'
and
	EpisodeEndDateID is not null
and
	NationalPatientClassificationCode not in ('2','3','4')
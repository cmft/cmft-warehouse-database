﻿CREATE view [Numerator].[CANPatientDiscussedAtMDTUR] as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.MDTDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANPatientDiscussedAtMDTUR'
	
where DatasetCode = 'CAN'
and D.MDTDate is not null -- MDT patients only
and D.WasPatientDiscussedAtMDT = 'Y'

and CancerSite = 'Urology'
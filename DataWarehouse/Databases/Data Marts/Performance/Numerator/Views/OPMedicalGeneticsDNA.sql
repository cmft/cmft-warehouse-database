﻿CREATE view [Numerator].[OPMedicalGeneticsDNA]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID   
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AppointmentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'dbu.NumeratorOPMedicalGeneticsDNA'

where
	Dataset.DatasetCode = 'OP'
and
	d.NationalSpecialtyCode = '311'
and
	d.NationalAttendanceStatusCode = '3' -- DNA
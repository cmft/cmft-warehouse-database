﻿CREATE view [Numerator].[CANNonUrgentReferralDiagnosedWithCancerSK] as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DiagnosisDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANNonUrgentReferralDiagnosedWithCancerSK'
	
where DatasetCode = 'CAN'
and D.PrimaryDiagnosisCode is not null
and D.PriorityTypeCode = '01' -- Routine
and D.DiagnosisDateID is not null -- screen out recs with incorrect early DiagnosisDate

and CancerSite = 'Skin'
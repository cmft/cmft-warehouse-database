﻿CREATE view [Numerator].[HRVacancyWTE]
as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.ClinicalContractedWTE - WrkDataset.ClinicalBudgetWTE

from
	Performance.dbo.WrkDataset 
inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HRVacancyWTE'

where
	Dataset.DatasetCode = 'HR'
and WrkDataset.ClinicalContractedWTE is not null
and WrkDataset.ClinicalBudgetWTE is not null
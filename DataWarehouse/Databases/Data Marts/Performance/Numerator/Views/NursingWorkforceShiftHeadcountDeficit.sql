﻿CREATE view [Numerator].[NursingWorkforceShiftHeadcountDeficit] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NursingWorkforceShiftHeadcountDeficit'
	
where
	Dataset.DatasetCode = 'STAFFLEVEL'
and WrkDataset.SourceShift in
							(
							'Early'
							,'Late'
							,'Night'
							)
and (WrkDataset.RegisteredNursePlan + WrkDataset.NonRegisteredNursePlan) > (WrkDataset.RegisteredNurseActual + WrkDataset.NonRegisteredNurseActual)
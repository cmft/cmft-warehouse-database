﻿CREATE view [Numerator].[IncidentCommunityAdultFallsGrade2OrOver]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentCommunityAdultFallsGrade2OrOver'
	
where
	Dataset.DatasetCode = 'INC'
and	WrkDataset.StartDirectorateCode in ('0','1') -- Community
and	(
		WrkDataset.IncidentCauseGroupCode = '0x3120' 
	or	WrkDataset.IncidentCause1Code in ('0x7042','0x7542','0x7142','0x5220','0x7342','0x7242')
	) -- Falls
and	WrkDataset.IncidentInvolvingAdult = 1
and	WrkDataset.IncidentGradeCode --in ('0x4120','0x4320','0x4420','0x4520','0x4620','0x4720') -- Harm Grade >= 2 
								not in	( 
										'0x4220' --1 No Harm
										,'0x4D20' --1U No Harm Unconfirmed
										,'0x5020' --1B Error DiD Not Reached The Patient
										,'0x4620' --6 Near Miss / No Harm
										,'0x4720' --7 Actual Harm Incident	
										,'0x4E20' --7 Awaiting 72Hr Review
										,'0x4820' --8 External / Safegaurding / Non Psi / Duplicate
										) --CH 07012015 Grading changed
﻿create view [Numerator].[OPERUtilisationMinutes] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.OperationDateID
	,ServicePointID
	,Value = D.UtilisationMinutes

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OPERUtilisationMinutes'
	
where
	DatasetCode = 'OPER'
and coalesce(UtilisationMinutes,0) > 0
﻿CREATE view [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksCO] as

-- Based on SP CancerRegister.dbo.RPT_2WEEK_WAIT

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AppointmentDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANUrgentCancerReferralSeenWithin2WeeksCO'
	
where DatasetCode = 'CAN'
and CancerTypeCode = '07'
and PriorityTypeCode = '03' -- to be seen within 2 weeks
and (InappropriateReferralCode <> 1 or InappropriateReferralCode is null) 
--and ReferralSourceCode in ('03','98') -- GP or Dentist removed after meeting with Ian C 4/4/14
--and datediff(day, ReferralDate, ReceiptOfReferralDate) < 2 
and datediff(d,ReferralDate,AppointmentDate) - coalesce(FirstAppointmentWaitingTimeAdjusted, 0) <= 14
﻿CREATE view [Numerator].[AEAdmission] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ArrivalDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AEAdmission'
	
where Dataset.DatasetCode = 'AE'
and AttendanceDisposalCode = '01' -- Admitted
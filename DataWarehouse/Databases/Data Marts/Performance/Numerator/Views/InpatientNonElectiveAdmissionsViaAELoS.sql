﻿CREATE view [Numerator].[InpatientNonElectiveAdmissionsViaAELoS]

as

select
       DatasetID = WrkDataset.DatasetID
       ,DatasetRecno = WrkDataset.DatasetRecno
       ,NumeratorID = Numerator.NumeratorID
       ,SiteID = WrkDataset.EndSiteID           
       ,DirectorateID = WrkDataset.EndDirectorateID
       ,SpecialtyID = WrkDataset.SpecialtyID
       ,ClinicianID = WrkDataset.ClinicianID
       ,DateID = WrkDataset.DischargeDateID
       ,ServicePointID
       ,Value = datediff(day, WrkDataset.AdmissionDate, WrkDataset.DischargeDate)

from dbo.WrkDataset

inner join dbo.Dataset
on     Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on     NumeratorLogic = 'Numerator.InpatientNonElectiveAdmissionsViaAELoS'
       
where
       DatasetCode = 'APC'
and
       NationalLastEpisodeInSpellCode = 1
and
       PatientCategoryCode = 'NE'
and
       AdmissionMethodCode = 21
and 
       DischargeDate is not null
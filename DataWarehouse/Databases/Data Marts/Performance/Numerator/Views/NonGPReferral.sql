﻿CREATE view [Numerator].[NonGPReferral]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID		
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ReferralDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NonGPReferral'

where
	DatasetCode = 'RF'
and
	d.NationalSourceOfReferralCode not in ('03','92')
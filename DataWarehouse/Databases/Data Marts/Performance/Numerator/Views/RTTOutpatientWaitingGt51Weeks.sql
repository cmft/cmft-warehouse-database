﻿CREATE view [Numerator].[RTTOutpatientWaitingGt51Weeks] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.CensusDateID
	,ServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RTTOutpatientWaitingGt51Weeks'

where Dataset.DatasetCode = 'RTT'
and d.PathwayStatusCode = 'OPW'
and d.AdjustedFlag = 'N'
and WaitDays > 356
﻿CREATE view [Numerator].[AEAmbulanceTimeToInitialAssessment] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EncounterDateID
	,ServicePointID
	,Value = d.Duration

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AEAmbulanceTimeToInitialAssessment'
	
where Dataset.DatasetCode = 'AES'
and d.StageCode = 'ARRIVALTOTRIAGEADJUSTED'
and d.ArrivalModeCode = '1' -- Ambulance
﻿CREATE view [Numerator].[ComplaintTurnaround] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.ReceiptDateID
	,WrkDataset.ServicePointID
	,Value = (	select 
					WorkingDays = sum(cast(WorkingDay as int))
				from 
					WarehouseOLAPMergedv2.WH.Calendar
				where 
					TheDate between coalesce(ConsentDate, ReceiptDate) and ResolutionDate
			  )				

from
	WrkDataset WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ComplaintTurnaround'
	
where
	Dataset.DatasetCode = 'COMP'
and	WrkDataset.CaseTypeCode in ('0x4120','0x4A20') --formal complaint and acute trust complaint
and	WrkDataset.SequenceNo = 1
and	WrkDataset.ResolutionDate is not null
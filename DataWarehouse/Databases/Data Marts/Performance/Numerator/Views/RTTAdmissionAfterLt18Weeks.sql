﻿CREATE view [Numerator].[RTTAdmissionAfterLt18Weeks] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.CensusDateID
	,ServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RTTAdmissionAfterLt18Weeks'

where Dataset.DatasetCode = 'RTT'
and d.PathwayStatusCode = 'ACS'
and d.AdjustedFlag = 'Y'
and WaitDays <= 126 -- 18 weeks
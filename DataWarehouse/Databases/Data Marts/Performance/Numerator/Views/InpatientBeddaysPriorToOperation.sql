﻿CREATE view [Numerator].[InpatientBeddaysPriorToOperation]

as

select
	Template = 'Not in use'

--select
--	DatasetID = Base.DatasetID
--	,DatasetRecno = Base.DatasetRecno
--	,SiteID = Base.StartSiteID		
--	,DirectorateID = Base.StartDirectorateID
--	,SpecialtyID = Base.SpecialtyID
--	,ConsultantID = Base.ConsultantID
--	,DateID = Base.DischargeDateID
--	,ServicePointID
--	,Value = datediff(day, Base.AdmissionDate,FirstProcedure.PrimaryProcedureDate)
--from
--	dbo.Base

--inner join
--	dbo.Dataset
--on	Dataset.DatasetID = Base.DatasetID

--inner join
--	(
--		select
--			ProviderSpellNo
--			,ContextID
--			,PrimaryProcedureDate
--		from
--			dbo.Base

--		inner join
--			dbo.Dataset
--		on	Dataset.DatasetID = Base.DatasetID

--		where
--			DatasetCode = 'APC'
--		--and
--		--	ClinicalCodingCompleteDate is not null
--		and
--			PrimaryProcedureCode is not null

--		/* get first procedure within spell */

--		and	
--			not exists
--						(
--						select
--							1
--						from
--							dbo.Base Earlier
--						where
--							DatasetCode = 'APC'
--						and
--							PrimaryProcedureDate is not null
--						and
--							Base.ProviderSpellNo = Earlier.ProviderSpellNo
--						and	
--							Base.ContextID = Earlier.ContextID
--						and 
--							Base.PrimaryProcedureDate > Earlier.PrimaryProcedureDate
--						)
		
--		/* handles same procedure date on different FCEs ('3263506/1') */

--		and	
--			not exists
--						(
--						select
--							1
--						from
--							dbo.Base Earlier
--						where
--							DatasetCode = 'APC'
--						and
--							PrimaryProcedureDate is not null
--						and
--							Base.ProviderSpellNo = Earlier.ProviderSpellNo
--						and	
--							Base.ContextID = Earlier.ContextID
--						and
--							Base.PrimaryProcedureDate = Earlier.PrimaryProcedureDate
--						and
--							Base.SourceEncounterNo > Earlier.SourceEncounterNo
--						)						
--	) FirstProcedure

--on	Base.ProviderSpellNo = FirstProcedure.ProviderSpellNo
--and	Base.ContextID = FirstProcedure.ContextID


--where
--	DatasetCode = 'APC'
----and
----	ClinicalCodingCompleteDate is not null
--and 
--	FirstEpisodeInSpellIndicator = 1
--and
--	/* gets first FCE only where there is a coded procedure in the spell */

--	exists
--		(
--		select
--			1
--		from
--			dbo.Base Operation
--		inner join
--			dbo.Dataset
--		on	Dataset.DatasetID = Base.DatasetID

--		where
--			DatasetCode = 'APC'
--		and
--			PrimaryProcedureDate is not null
--		and
--			PrimaryProcedureCode is not null
--		and
--			Operation.ProviderSpellNo = Base.ProviderSpellNo
--		and
--			Operation.ContextID = Base.ContextID
--		)
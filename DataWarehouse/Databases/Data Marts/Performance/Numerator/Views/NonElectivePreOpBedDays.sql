﻿CREATE view [Numerator].[NonElectivePreOpBedDays]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID
	,Value = datediff(dd,d.AdmissionTime, d.PrimaryProcedureDate)
from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NonElectivePreOpBedDays'

where
	Dataset.DatasetCode = 'APC'
and
	d.FirstEpisodeInSpellIndicator = 1
and
	d.PatientCategoryCode = 'NE'
and
	datediff(dd,d.AdmissionTime, d.PrimaryProcedureDate) > 0
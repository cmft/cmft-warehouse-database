﻿create view [Numerator].[ComplaintPressureUlcers] as

	select
		DatasetID = WrkDataset.DatasetID
		,DatasetRecno = WrkDataset.DatasetRecno
		,NumeratorID = Numerator.NumeratorID
		,SiteID = WrkDataset.EndSiteID		
		,DirectorateID = WrkDataset.EndDirectorateID
		,SpecialtyID = WrkDataset.SpecialtyID
		,ClinicianID = WrkDataset.ClinicianID
		,DateID = WrkDataset.ReceiptDateID
		,ServicePointID = WrkDataset.ServicePointID
		,Value = 1
	from WrkDataset WrkDataset

	inner join dbo.Dataset
	on	Dataset.DatasetID = WrkDataset.DatasetID

	inner join dbo.Numerator
	on	Numerator.NumeratorLogic = 'Numerator.ComplaintPressureUlcers'
		
	where
		Dataset.DatasetCode = 'COMP'
	and	WrkDataset.CategoryTypeCode = '0x5342'
﻿CREATE view [Numerator].[ElectivePreOpBedDays]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID = d.StartServicePointID
	,Value = datediff(dd,d.AdmissionTime, d.PrimaryProcedureDate)
from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ElectivePreOpBedDays'

where
	Dataset.DatasetCode = 'APC'
and
	d.NationalLastEpisodeInSpellCode = 1
and
	d.PatientCategoryCode = 'EL'
and
	datediff(dd,d.AdmissionTime, d.PrimaryProcedureDate) > 0
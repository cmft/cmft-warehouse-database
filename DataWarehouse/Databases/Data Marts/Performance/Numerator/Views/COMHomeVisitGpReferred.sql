﻿create view [Numerator].[COMHomeVisitGpReferred] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ContactDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.COMHomeVisitGpReferred'
	
where Dataset.DatasetCode = 'COM'
and d.EncounterOutcomeCode = 'ATTD'
and ConsultationMediumTypeGroup = 'Contacts'
and ConsultationMediumTypeGroupChannel = 'Face to Face'
and ReferralSourceCode in ('2002324','2003641','2005590','5300') -- GP referred
﻿CREATE view [Numerator].[SHMIExpectedDeath]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeEndDateID
	,ServicePointID
	,Value = WrkDataset.RiskScore
from
	Performance.dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HSMRExpectedDeath'

where
	DatasetCode = 'APC'
and	EpisodeEndDateID is not null
and	WrkDataset.DominantForDiagnosis = 1
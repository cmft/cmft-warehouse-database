﻿CREATE view [Numerator].[ComplaintResolvedWithin25Days] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.Complaint25DayTargetDateID --WrkDataset.ReceiptDateID
	,WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ComplaintResolvedWithin25Days'

where
	Dataset.DatasetCode = 'COMP'
and	WrkDataset.CaseTypeCode in ('0x4120','0x4A20') --formal complaint and acute trust complaint
and	WrkDataset.SequenceNo = 1
and	WrkDataset.ResponseDate  <= WrkDataset.Complaint25DayTargetDate
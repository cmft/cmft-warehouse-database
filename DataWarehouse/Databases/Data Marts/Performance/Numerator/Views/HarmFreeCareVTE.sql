﻿CREATE view [Numerator].[HarmFreeCareVTE] as

-- Note that the data extracted from BedMan does not include all events
-- but only events requiring treatment or causing actual harm 

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DiagnosisDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCareVTE'
	
where
	DatasetCode = 'VTECOND'
and WrkDataset.DiagnosisDate >= WrkDataset.AdmissionDate
and	WrkDataset.MedicationRequired = 1
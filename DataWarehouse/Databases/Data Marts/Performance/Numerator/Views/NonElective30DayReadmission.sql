﻿CREATE view [Numerator].[NonElective30DayReadmission]

as

-- Select Spell A which has a following Non-Elective Spell B within 30 days for the same Patient
select
	DatasetID = A.DatasetID
	,DatasetRecno = A.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = A.EndSiteID		
	,DirectorateID = A.EndDirectorateID
	,SpecialtyID = A.SpecialtyID
	,ClinicianID = A.ClinicianID
	,DateID = A.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset A

inner join dbo.Dataset
on	Dataset.DatasetID = A.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NonElective30DayReadmission'

where
	Dataset.DatasetCode = 'APC'
and
	A.NationalLastEpisodeInSpellCode = 1 
and 
	A.PatientCategoryCode = 'NE'
	
	-- Look for a later Spell B	
and 
	exists
	(
		select 1 from WrkDataset B
		where B.DatasetID = A.DatasetID 
		and B.FirstEpisodeInSpellIndicator = 1
		and B.SourcePatientNo = A.SourcePatientNo
		and B.PatientCategoryCode = 'NE'
		and B.DischargeTime > A.DischargeTime
		and datediff(d,A.DischargeTime, B.AdmissionTime) <= 30
	)
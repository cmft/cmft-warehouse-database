﻿CREATE view [Numerator].[HRLeaversFTEMonthly]
as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.LeaversHeadcountMonthly
from
	Performance.dbo.WrkDataset 
inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HRLeaversFTEMonthly'

where
	Dataset.DatasetCode = 'HR'
and WrkDataset.LeaversFTEMonthly is not null
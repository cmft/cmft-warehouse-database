﻿/****** Object:  View Numerator.[COMStaffTeamEntered]    Script Date: 09/04/2013 12:04:25 ******/
CREATE view [Numerator].[COMHomeVisit] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ContactDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.COMHomeVisit'
	
where Dataset.DatasetCode = 'COM'
and EncounterOutcomeCode = 'ATTD'
and ConsultationMediumTypeGroup= 'Contacts'
and ConsultationMediumTypeGroupChannel = 'Face to Face'
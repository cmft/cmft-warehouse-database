﻿CREATE view [Numerator].[FinancialExpenditureActualDental] as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.FinanceActual - WrkDataset.FinanceBudget 

from dbo.WrkDataset 

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialExpenditureActualDental'
	
where
	Dataset.DatasetCode = 'FINANCE'
and EndDirectorateID = 10 -- Dental

--select distinct EndDirectorateID,Division from WrkDataset where DatasetID = 39 and Division is not null
﻿CREATE view [Numerator].[AdmissionToMAUAgeOver65]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.WardStayStartDateID
	,ServicePointID
	,Value = 1

from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.AdmissionToMAUAgeOver65'

where
	Dataset.DatasetCode = 'WS'
and	left(d.StartWardCode,3) = 'MAU'
and	convert(int,round(datediff(hour,d.DateOfBirth, d.WardStayStartDate)/8766,0)) >= 65
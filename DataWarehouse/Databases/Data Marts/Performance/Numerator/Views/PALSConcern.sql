﻿CREATE view [Numerator].[PALSConcern] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.ReceiptDateID
	,WrkDataset.ServicePointID
	,Value = 1
from
	WrkDataset WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.PALSConcern'
	
where
	Dataset.DatasetCode = 'COMP'
and	WrkDataset.CaseTypeCode in
								(
								'0x4920' --PALS Concern
								)                               
and Reopened is null
and SequenceNo = 1
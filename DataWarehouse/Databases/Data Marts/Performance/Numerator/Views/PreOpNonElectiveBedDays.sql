﻿CREATE view [Numerator].[PreOpNonElectiveBedDays]

as

-- Count bed days between EpisodeStartDate and PrimaryProcedureDate
select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.StartSiteID		
	,DirectorateID = D.StartDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.AdmissionDateID
	,ServicePointID
	,Value = datediff(day,D.EpisodeStartDate, D.PrimaryProcedureDate)

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.PreOpNonElectiveBedDays'

where	
	D.PrimaryProcedureDate is not null	
and
	Dataset.DatasetCode = 'APC'
and
	datediff(day,D.AdmissionDate, D.PrimaryProcedureDate) > 0 -- Don't write out zero records
and
	D.PatientCategoryCode = 'NE'
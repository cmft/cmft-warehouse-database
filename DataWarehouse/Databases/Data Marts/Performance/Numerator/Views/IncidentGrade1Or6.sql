﻿CREATE view [Numerator].[IncidentGrade1Or6] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentGrade1Or6'
	
where
	Dataset.DatasetCode = 'INC'
and WrkDataset.IncidentGradeCode in --('0x4220','0x4620') -- Harm Grade = 1 or 6
									(
									'0x4220' --1 No Harm
									,'0x4D20' --1U No Harm Unconfirmed
									,'0x4620' --6 Near Miss / No Harm
									) --CH 07012015 Grading changed
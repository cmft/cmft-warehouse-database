﻿CREATE view [Numerator].[IncidentMedicationErrorGrade4AgeOver65]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade4AgeOver65'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		IncidentTypeCode = '0x5120' 
	or	IncidentCauseGroupCode = '0x4620'
	) -- Medication Error

and	WrkDataset.IncidentInvolvingElderly = 1
and	WrkDataset.IncidentGradeCode --= '0x4520' -- Harm Grade = 4
								in	(
									'0x4A20' --4U Major Unconfirmed
									,'0x4920' --4C Major
									,'0x4F20' --4# Nof
									) --CH 07012015 Grading changed
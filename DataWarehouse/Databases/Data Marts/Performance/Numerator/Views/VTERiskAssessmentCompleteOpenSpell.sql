﻿CREATE view [Numerator].[VTERiskAssessmentCompleteOpenSpell] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.VTERiskAssessmentCompleteOpenSpell'

where
	Dataset.DatasetCode = 'APC'
and
	d.FirstEpisodeInSpellIndicator = 1
and
	d.VTECategoryCode = 'C'
and
	d.DischargeDate is null
and
	d.PatientCategoryCode not in ('RD','RN')
and
	datediff(year,d.DateOfBirth,d.AdmissionDate) >= 18
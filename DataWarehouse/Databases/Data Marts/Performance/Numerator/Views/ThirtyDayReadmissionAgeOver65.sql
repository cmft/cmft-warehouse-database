﻿CREATE view [Numerator].[ThirtyDayReadmissionAgeOver65] as

-- Select Spell A which has a later Spell B within 30 days for the same Patient
select
	DatasetID = A.DatasetID
	,DatasetRecno = A.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = A.EndSiteID		
	,DirectorateID = A.EndDirectorateID
	,SpecialtyID = A.SpecialtyID
	,ClinicianID = A.ClinicianID
	,DateID = A.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset A

inner join dbo.Dataset
on	Dataset.DatasetID = A.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ThirtyDayReadmissionAgeOver65'

where
	A.NationalLastEpisodeInSpellCode = 1 

	-- Look for a later Spell B	
	and exists
	(
		select 1 from WrkDataset B
		where A.DatasetID = B.DatasetID
		and B.FirstEpisodeInSpellIndicator = 1
		and B.SourcePatientNo = A.SourcePatientNo
		and B.AdmissionTime > A.DischargeTime
		and datediff(d,A.DischargeTime, B.AdmissionTime) <= 30
	)
and
	Dataset.DatasetCode = 'APC'

and
	convert(int,round(datediff(hour,DateOfBirth,A.AdmissionTime)/8766,0)) >= 65
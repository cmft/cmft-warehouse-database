﻿CREATE view [Numerator].[DeathsInElectiveInfraRenalAneurisms]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1

from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.DeathsInElectiveInfraRenalAneurisms'

where
	Dataset.DatasetCode = 'APC'
and
	d.NationalLastEpisodeInSpellCode = 1
and
	d.PatientCategoryCode = 'EL'
and d.NationalSpecialtyCode = '107' 
and left(d.PrimaryProcedureCode,3) in ('L18', 'L19', 'L27', 'L28')
and d.NationalDischargeMethodCode in ('4','5')
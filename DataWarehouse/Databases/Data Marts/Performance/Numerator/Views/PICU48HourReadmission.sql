﻿CREATE view [Numerator].[PICU48HourReadmission]

as

-- Select PICU Spell A which has a following PICU Spell B within 48 hours for the same Patient
select
	DatasetID = A.DatasetID
	,DatasetRecno = A.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = A.EndSiteID		
	,DirectorateID = A.EndDirectorateID
	,SpecialtyID = A.SpecialtyID
	,ClinicianID = A.ClinicianID
	,DateID = A.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset A

inner join dbo.Dataset
on	Dataset.DatasetID = A.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.PICU48HourReadmission'

where
	Dataset.DatasetCode = 'APC'
and
	A.NationalLastEpisodeInSpellCode = 1 
and
	A.EndWardCode = '80'
	
-- Look for a later Spell B	
and 
	exists
	(
		select 1 from WrkDataset B
		where B.DatasetID = A.DatasetID 
		and B.FirstEpisodeInSpellIndicator = 1
		and B.SourcePatientNo = A.SourcePatientNo
		and B.StartWardCode = '80'
		and B.DischargeTime > A.DischargeTime
		and datediff(hour,A.DischargeTime, B.AdmissionTime) <= 48
	)
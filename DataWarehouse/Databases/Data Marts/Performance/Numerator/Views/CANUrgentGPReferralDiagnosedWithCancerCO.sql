﻿CREATE view [Numerator].[CANUrgentGPReferralDiagnosedWithCancerCO] as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.ReferralDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANUrgentGPReferralDiagnosedWithCancerCO'
	
where DatasetCode = 'CAN'
and D.CancerTypeCode <> '16'
and D.PriorityTypeCode = '03' -- To be seen within 2 weeks
and D.ReferralSourceCode in ('03','98') -- GP or Dentist 
and left(D.PrimaryDiagnosisCode, 1) = 'C'

and CancerSite = 'Colorectal'
﻿CREATE view [Numerator].[HarmFreeCareFallSeverity2OrOver] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.FallDateID
	,ServicePointID
	,Value = 1
from	
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCareFallSeverity2OrOver'
	
where
	Dataset.DatasetCode = 'FALL' 
and	WrkDataset.SourceFallSeverityCode > 1
﻿CREATE view [Numerator].[COMReferralNoContactWithin13Months] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = Calendar.DateID -- should be Census Date
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.COMReferralNoContactWithin13Months'

join -- Create Census Dates: join to list of month end dates from Calendar table
(
	select distinct
		MonthEnd = cast(dateadd(s,-1,dateadd(mm, datediff(m,0,TheDate)+1,0)) as date)
	from WarehouseOLAPMergedV2.WH.Calendar
	where TheDate <= getdate() 	
) MonthEnd 

on d.ReferralDate < dateadd(month, -13, MonthEnd.MonthEnd) -- Take all recs with ReferralDate at least 13 months before Census Date

join WarehouseOLAPMergedV2.WH.Calendar on Calendar.TheDate = MonthEnd.MonthEnd -- Get the DateID for the MonthEnd date

where Dataset.DatasetCode = 'COMR'

and not exists -- exclude referrals where a contact exists between the Referral Date and the Census Date
(
	select 1 from WrkDataset Encounter
	join Dataset on Dataset.DatasetID = Encounter.DatasetID
	where Dataset.DatasetCode = 'COM'
	and Encounter.ReferralID = d.ReferralID
	and Encounter.ContactDate between d.ReferralDate and MonthEnd.MonthEnd
)
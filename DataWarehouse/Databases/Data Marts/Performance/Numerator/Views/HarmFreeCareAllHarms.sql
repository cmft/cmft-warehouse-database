﻿CREATE view [Numerator].[HarmFreeCareAllHarms] as

select
	AllHarms.DatasetID 
	,AllHarms.DatasetRecno 
	,Numerator.NumeratorID
	,AllHarms.SiteID 
	,AllHarms.DirectorateID
	,AllHarms.SpecialtyID 
	,AllHarms.ClinicianID 
	,AllHarms.DateID 
	,AllHarms.ServicePointID
	,AllHarms.Value
from
	(
	select
		 DatasetID
		  ,DatasetRecno
		  ,SiteID
		  ,DirectorateID
		  ,SpecialtyID
		  ,ClinicianID
		  ,DateID
		  ,ServicePointID
		  ,Value
	from
		Numerator.HarmFreeCareCatheterUTI

	union all

	select
		 DatasetID
		  ,DatasetRecno
		  ,SiteID
		  ,DirectorateID
		  ,SpecialtyID
		  ,ClinicianID
		  ,DateID
		  ,ServicePointID
		  ,Value
	from
		Numerator.HarmFreeCareFallSeverity2OrOver
	union all
	
	select
		 DatasetID
		  ,DatasetRecno
		  ,SiteID
		  ,DirectorateID
		  ,SpecialtyID
		  ,ClinicianID
		  ,DateID
		  ,ServicePointID
		  ,Value
	from
		Numerator.HarmFreeCarePressureUlcerCategory2OrOver

	union all
	
	select
		 DatasetID
		  ,DatasetRecno
		  ,SiteID
		  ,DirectorateID
		  ,SpecialtyID
		  ,ClinicianID
		  ,DateID
		  ,ServicePointID
		  ,Value
	from
		Numerator.HarmFreeCareVTE

	) AllHarms

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCareAllHarms'
﻿CREATE view [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysSA] as

-- Based on SP CancerRegister.dbo.RPT_SUB_TREAT_MODALITY

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANTSubsequentSurgeryTreatmentWithin31DaysSA'
	
where Dataset.DatasetCode = 'CANT'
and D.TreatmentEventTypeCode not in ('01','07') -- Exclude first treatment
and (D.InappropriateReferralCode <> 1 or D.InappropriateReferralCode is null) 
and D.InitialTreatmentCode = '01' -- Surgery
and not
(
	coalesce(D.PrimaryDiagnosisCode,'') = 'C44'
	and left(coalesce(D.HistologyCode,''),6) in ('M80903','M80913','M80923','M80933','M80943','M80953','M81103')
)
and not
(
	coalesce(D.PrimaryDiagnosisCode,'') <> 'D05'
	and left(coalesce(D.PrimaryDiagnosisCode,''),1) = 'D'	
)

and
(
	datediff(d,D.DecisionToTreatDate,D.TreatmentDate) - 
	(
		case 
			when D.AdjustmentReasonCode = '8' and D.TreatmentSettingCode in ('01','02') 
			then coalesce(D.WaitingTimeAdjustment,0) 
			else 0 
		end
	) <= 31
)

and CancerSite = 'Sarcoma'
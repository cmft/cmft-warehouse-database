﻿CREATE view [Numerator].[IncidentPressureUlcerGrade2OrOverChild]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentPressureUlcerGrade2OrOverChild'
	
where
	Dataset.DatasetCode = 'INC'
and WrkDataset.IncidentCause1Code in -- Pressure Ulcer
									(
									'0x4C48'
									,'0x4D48'
									,'0x4E48'
									,'0x5449'
									,'0x5549'
									,'0x5649'
									,'0x5749'
									,'0x5849'
									,'0x5949'
									,'0x5A49'
									,'0x6149'
									,'0x614C'
									,'0x624C'
									,'0x634C'
									,'0x644C'
									,'0x704C'
									,'0x714C'
									,'0x724C'
									) 
and	WrkDataset.IncidentInvolvingChild = 1
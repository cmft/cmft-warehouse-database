﻿CREATE view [Numerator].[FinancialExpenditureActualSpecialistMedicine] as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.FinanceActual - WrkDataset.FinanceBudget 

from dbo.WrkDataset 

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialExpenditureActualSpecialistMedicine'
	
where
	Dataset.DatasetCode = 'FINANCE'
and EndDirectorateID = 5 -- SpecialistMedicine

--select distinct EndDirectorateID,Division from WrkDataset where DatasetID = 39 and Division is not null
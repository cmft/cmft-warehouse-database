﻿CREATE view [Numerator].[RCodePrimDiag]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeEndDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RCodePrimDiag'

where
	DatasetCode = 'APC'
and
	WrkDataset.ClinicalCodingCompleteDate is not null
and
	left(WrkDataset.PrimaryDiagnosisCode, 1) = 'R'
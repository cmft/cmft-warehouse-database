﻿CREATE view [Numerator].[VTERiskAssessment]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.VTERiskAssessment'

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	PatientCategoryCode in ('DC','EL','NE')
and
	datediff(yy,d.DateOfBirth,d.AdmissionDate) >= 18
and
	d.VTECategoryCode is not null
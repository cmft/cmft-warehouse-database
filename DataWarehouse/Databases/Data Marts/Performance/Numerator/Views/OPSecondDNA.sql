﻿CREATE view [Numerator].[OPSecondDNA] as

select distinct -- later appointment details
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AppointmentDateID 
	,d.ServicePointID
	,Value = 1

from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.OPSecondDNA'

inner join 
	WrkDataset e -- earlier DNA for same patient
on	e.SourcePatientNo = d.SourcePatientNo -- same patient
and e.AppointmentDate < d.AppointmentDate -- earlier appointment date
and e.AppointmentOutcomeCode = d.AppointmentOutcomeCode -- with outcome = DNA

where
	Dataset.DatasetCode = 'OP'
and
	d.AppointmentOutcomeCode = 'DNA'
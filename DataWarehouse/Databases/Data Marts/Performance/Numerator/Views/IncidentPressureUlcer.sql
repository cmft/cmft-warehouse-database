﻿CREATE view [Numerator].[IncidentPressureUlcer]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentPressureUlcer'
	
where
	Dataset.DatasetCode = 'INC'
and -- Pressure Ulcer
	(
		(
			WrkDataset.IncidentTypeCode = '0x5320' 
		and WrkDataset.IncidentCauseGroupCode = '0x7920' 
		and WrkDataset.IncidentCause1Code <> '0x6348'
		)
	or
		(
			WrkDataset.IncidentCauseGroupCode = '0x6320' 
		 and WrkDataset.IncidentCause1Code in ('0x4B48','0x4C48','0x4D48','0x4D48','0x5449','0x5549','0x5649','0x5749','0x5044','0x5849','0x5949','0x5A49','0x6149','0x5A49','0x5449','0x5549','0x5649','0x5749')
		)
	)
﻿CREATE view [Numerator].[RENQDialysisPrimaryInMinimalCareSetting] as

-- Patients on dialysis in hospital or satellite setting where setting is primary

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on NumeratorLogic = 'Numerator.RENQDialysisPrimaryInMinimalCareSetting'

where DatasetCode = 'RENQ'
and ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55433','90:55435') -- Hospital or Satellite
and EventDetailCode in ('90:8000172','90:8000173') -- HD - Self Care in Unit, HDF - Self Care in Unit
and DateOfDeath is null
and IsPrimary = 1
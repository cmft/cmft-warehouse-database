﻿CREATE view [Numerator].[GovernanceRiskRatingCSS] as


select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDataset.ActivityDateID
	,ServicePointID
	,Value = GovernanceRiskRating
from 
	dbo.WrkDataset 
inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.GovernanceRiskRatingCSS'

where 
	Dataset.DatasetCode = 'GOVERNANCE'
and StartDirectorateID = 17 -- clinical and scientific
and GovernanceRiskRating is not null
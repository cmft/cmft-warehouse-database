﻿CREATE view [Numerator].[CRFReferral]
as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ReferralDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d 

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.CRFReferral'
	
where
	Dataset.DatasetCode = 'COMR'
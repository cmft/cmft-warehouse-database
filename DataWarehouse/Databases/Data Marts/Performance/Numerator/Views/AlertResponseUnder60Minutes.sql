﻿CREATE view [Numerator].[AlertResponseUnder60Minutes] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CreatedDateID
	,WrkDataset.ServicePointID
	,Value = 1
from
	WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AlertResponseUnder60Minutes'
	
where
	Dataset.DatasetCode = 'ALERT'
and	datediff(minute,WrkDataset.CreatedTime, coalesce(WrkDataset.ClosedTime, getdate())) <60
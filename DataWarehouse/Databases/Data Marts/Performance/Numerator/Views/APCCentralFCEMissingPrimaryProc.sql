﻿CREATE view [Numerator].[APCCentralFCEMissingPrimaryProc] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.APCCentralFCEMissingPrimaryProc'

where Dataset.DatasetCode = 'APC'
and d.ClinicalCodingCompleteDate is not null
and d.PrimaryProcedureCode is null
and d.TheatrePrimaryProcedureExists = 1
and datediff(day,d.AdmissionDate,d.DischargeDate) >= 2
and d.NationalSpecialtyCode in ('100','101','102','104','107','110','120','140','142','144','150','160','170','171','172','173','211','214','215','218','219')
and d.ContextCode = 'CEN||PAS' -- Central only: Trafford theatre data not in Warehouse
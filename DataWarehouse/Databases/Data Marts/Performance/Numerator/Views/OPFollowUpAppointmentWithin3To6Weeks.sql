﻿CREATE view [Numerator].[OPFollowUpAppointmentWithin3To6Weeks] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID   
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = Calendar.DateID -- Census Date
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OPFollowUpAppointmentWithin3To6Weeks'

-- Appointment is between 3 and 6 weeks of given FirstDayOvWeek
inner join WarehouseOLAPMergedV2.WH.Calendar on datediff(d,Calendar.FirstDayOfWeek,d.AppointmentDate) between 14 and 41
and Calendar.FirstDayOfWeek = Calendar.TheDate -- Only interested in FirstDayOfWeek record itself

where Dataset.DatasetCode = 'OP'

and NationalFirstAttendanceCode = '2' -- Follow Up Appointment

and datediff(day,Calendar.FirstDayOfWeek, getdate()) between 0 and 42 -- Take last 6 weeks of data
﻿CREATE view [Numerator].[CANDiscussedAtMDTAndWithPerfStatusHN] as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.MDTDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CANDiscussedAtMDTAndWithPerfStatusHN'
	
where DatasetCode = 'CAN'
and D.PrimaryDiagnosisCode is not null
and D.DiagnosisDateID is not null
and D.WasPatientDiscussedAtMDT = 'Y'
and D.PerformanceStatusCode is not null
and CancerSite = 'Head and Neck'
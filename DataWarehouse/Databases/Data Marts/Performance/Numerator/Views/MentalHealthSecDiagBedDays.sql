﻿CREATE view [Numerator].[MentalHealthSecDiagBedDays]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = datediff(dd, AdmissionTime, DischargeTime)

from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.MentalHealthSecDiagBedDays'

where
	Dataset.DatasetCode = 'APC'
and
	d.NationalLastEpisodeInSpellCode = 1
and
	d.PatientCategoryCode in ('EL','NE')
and
	left(d.SecondaryDiagnosisCode1,1) = 'F'
and
	datediff(dd,AdmissionTime,DischargeTime) > 0
﻿Create view [Numerator].[CancelledOperations] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDataset.CancellationDateID
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDataset  

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CancelledOperations'

where 
	Dataset.DatasetCode = 'CANOP'
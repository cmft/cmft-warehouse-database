﻿Create view [Numerator].[CancelledOperation28dayBreach] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDataset.CancellationDateID
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDataset  

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CancelledOperation28dayBreach'

where 
	Dataset.DatasetCode = 'CANOP'
and ReportableBreach = 1
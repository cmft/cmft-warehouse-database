﻿CREATE view [Numerator].[Complaint] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.ReceiptDateID
	,WrkDataset.ServicePointID
	,Value = 1
from
	WrkDataset WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.Complaint'
	
where
	Dataset.DatasetCode = 'COMP'
and	WrkDataset.CaseTypeCode in
								(
								'0x4A20' --formal complaint
								,'0x4120' --Acute Trust Complaint
								)                               
and WrkDataset.Reopened is null
and SequenceNo = 1
﻿create view [Numerator].[OPERTurnaroundTime] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.OperationDateID
	,ServicePointID
	,Value = TurnaroundTime

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OPERTurnaroundTime'
	
where DatasetCode = 'OPER'

and coalesce(TurnaroundTime,0) > 0
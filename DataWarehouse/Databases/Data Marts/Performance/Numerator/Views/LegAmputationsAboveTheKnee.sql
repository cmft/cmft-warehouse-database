﻿CREATE view [Numerator].[LegAmputationsAboveTheKnee]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1

from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.LegAmputationsAboveTheKnee'

where
	Dataset.DatasetCode = 'APC'
and
	d.FirstEpisodeInSpellIndicator = 1
and d.PrimaryProcedureCode =  'X09.3'
﻿create view [Denominator].[APCDischargeUsingFirstEpisodeIpDcOver18] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCDischargeUsingFirstEpisodeIpDcOver18'
	
where
	Dataset.DatasetCode = 'APC'
and
	WrkDataset.FirstEpisodeInSpellIndicator = 1
and
	WrkDataset.DischargeTime is not null

and
	WrkDataset.PatientCategoryCode not in ('RD','RN')
and
	datediff(year,WrkDataset.DateOfBirth,WrkDataset.AdmissionDate) >= 18
﻿CREATE view [Denominator].[ContinenceCatheterQuestion] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.QCRAuditDateID
	,ServicePointID
	,Value = 1
	,QCRCustomListCode

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.ContinenceCatheterQuestion'
	
where
	DatasetCode = 'QCR'
and
	QCRCustomListCode = 4 -- Continence / Catheter
﻿CREATE view [Denominator].[CriticalCareAdmission]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset
inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.CriticalCareAdmission'

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	StartWardCode like 'HDU%' or StartWardCode like 'ITU%'
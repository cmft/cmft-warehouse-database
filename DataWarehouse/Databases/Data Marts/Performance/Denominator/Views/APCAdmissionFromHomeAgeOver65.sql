﻿CREATE view [Denominator].[APCAdmissionFromHomeAgeOver65]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID = d.StartServicePointID
	,Value = 1

from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.APCAdmissionFromHomeAgeOver65'

where
	Dataset.DatasetCode = 'APC'
and
	d.NationalLastEpisodeInSpellCode = 1
and
	d.NationalAdmissionSourceCode in ('19','29')
and
	convert(int,round(datediff(hour,DateOfBirth,AdmissionTime)/8766,0)) >= 65
﻿CREATE view [Denominator].[ConsultantEpisodeDominantForDiagnosisExcludingDayCaseRegularDayNightElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeStartDateID
	,ServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset
inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.ConsultantEpisodeDominantForDiagnosisExcludingDayCaseRegularDayNightElective'

where
	DatasetCode = 'APC'
and	DominantForDiagnosis = 1
and	NationalPatientClassificationCode not in ('2','3','4')
and	PatientCategoryCode = 'EL'
﻿CREATE view [Denominator].[OutpatientFirstAppointment]

as

select
       DatasetID = d.DatasetID
       ,DatasetRecno = d.DatasetRecno
       ,DenominatorID = Denominator.DenominatorID
       ,SiteID = d.StartSiteID   
       ,DirectorateID = d.StartDirectorateID
       ,SpecialtyID = d.SpecialtyID
       ,ClinicianID = d.ClinicianID
       ,DateID = d.AppointmentDateID
       ,ServicePointID
       ,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on     d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on     DenominatorLogic = 'Denominator.OutpatientFirstAppointment'

where
       Dataset.DatasetCode = 'OP'
and
       d.NationalFirstAttendanceCode in ('1','3') -- First Appointment
and
       d.NationalAttendanceStatusCode not in (2,4) --Cancellations
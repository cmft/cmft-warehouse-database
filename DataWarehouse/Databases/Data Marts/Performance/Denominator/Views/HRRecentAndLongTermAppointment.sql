﻿CREATE view [Denominator].[HRRecentAndLongTermAppointment]
as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.RecentAppointment + WrkDataset.LongTermAppointment

from
	Performance.dbo.WrkDataset 
inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.HRRecentAndLongTermAppointment'

where
	Dataset.DatasetCode = 'HR'
and WrkDataset.RecentAppointment is not null
and WrkDataset.LongTermAppointment is not null
﻿CREATE view [Denominator].[CASENewCasenoteExistingRegistration] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AllocatedDateID
	,WrkDataset.AllocatedDate
	,WrkDataset.SourcePatientNo
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.CASENewCasenoteExistingRegistration'
	
where
	Dataset.DatasetCode = 'CASE'
and	WrkDataset.ExistingRegistration = 1

--and exists -- Existing Registration = at least one previously allocated Casenote Number
--(
--	select
--		1
--	from
--		dbo.WrkDataset Earlier
--	where
--		Earlier.DatasetID = WrkDataset.DatasetID
--	and Earlier.SourcePatientNo = WrkDataset.SourcePatientNo
--	and (
--		Earlier.AllocatedDate < WrkDataset.AllocatedDate
--		or (
--				Earlier.AllocatedDate = WrkDataset.AllocatedDate 
--			and	Earlier.DatasetRecno < WrkDataset.DatasetRecno
--			)
--		)
--)
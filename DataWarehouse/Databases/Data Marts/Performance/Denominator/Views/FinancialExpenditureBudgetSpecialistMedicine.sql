﻿CREATE view [Denominator].[FinancialExpenditureBudgetSpecialistMedicine] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.FinanceBudget
from 
	dbo.WrkDataset 

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.FinancialExpenditureBudgetSpecialistMedicine'

where
	Dataset.DatasetCode = 'FINANCE'
and EndDirectorateID = 5 -- Specialist Medicine
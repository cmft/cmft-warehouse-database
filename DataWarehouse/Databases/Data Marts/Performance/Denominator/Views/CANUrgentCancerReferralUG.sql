﻿CREATE view [Denominator].[CANUrgentCancerReferralUG] as

-- Based on SP CancerRegister.dbo.RPT_2WEEK_WAIT

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AppointmentDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.CANUrgentCancerReferralUG'
	
where DatasetCode = 'CAN'
and CancerTypeCode = '06'
and PriorityTypeCode = '03' -- to be seen within 2 weeks
and (InappropriateReferralCode <> 1 or InappropriateReferralCode is null)
--and ReferralSourceCode in ('03','98') -- GP or Dentist removed after meeting with Ian C 4/4/14
and AppointmentDateID is not null -- Count only patients seen within period 
--and datediff(day, ReferralDate, ReceiptOfReferralDate) < 2
﻿CREATE view [Denominator].[FinishedConsultantEpisode]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeEndDateID
	,ServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset

inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.FinishedConsultantEpisode'

where
	DatasetCode = 'APC'
and	EpisodeEndDate is not null
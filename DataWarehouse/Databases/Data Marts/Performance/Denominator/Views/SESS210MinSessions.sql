﻿CREATE view [Denominator].[SESS210MinSessions] as

-- Derive standard 210 minute Sessions
select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.SessionDateID
	,ServicePointID
	,Value = cast(Duration as decimal(19,4)) / 210 -- Actual session length divided by 210

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.SESS210MinSession'
	
where DatasetCode = 'SESS'

and CancelledFlag = 0
﻿CREATE view [Denominator].[COMContactBy6]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ContactDateID
	,ServicePointID
	,Value = 6

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.COMContactBy6'
	
where
	Dataset.DatasetCode = 'COM'
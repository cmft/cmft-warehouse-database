﻿CREATE view [Denominator].[APCOpenAdmissionIpDcOver18] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID --EndSiteID  End contains nulls as this relates to Discharge info, but query relates to admissions
	,DirectorateID = WrkDataset.StartDirectorateID -- EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCOpenAdmissionIpDcOver18'

where
	Dataset.DatasetCode = 'APC'
and
	WrkDataset.FirstEpisodeInSpellIndicator = 1
and
	WrkDataset.DischargeDate is null
and
	WrkDataset.PatientCategoryCode not in ('RD','RN')
and
	datediff(year,WrkDataset.DateOfBirth,WrkDataset.AdmissionDate) >= 18
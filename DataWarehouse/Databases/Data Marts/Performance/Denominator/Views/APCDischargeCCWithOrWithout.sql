﻿CREATE view [Denominator].[APCDischargeCCWithOrWithout]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID  
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCDischargeCCWithOrWithout'

where Dataset.DatasetCode = 'APC'
and d.NationalLastEpisodeInSpellCode = 1
and d.DischargeDate is not null
and d.HRGWithCCCode in ('WITH','WITHOUT') -- HRG with or without complications or co-morbidities (excludes NONE)
and d.PatientClassificationCode in ('1','2')
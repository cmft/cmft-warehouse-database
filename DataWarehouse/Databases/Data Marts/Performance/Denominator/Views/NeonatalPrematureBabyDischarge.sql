﻿CREATE view [Denominator].[NeonatalPrematureBabyDischarge] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID  
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.NeonatalPrematureBabyDischarge'

where DatasetCode = 'NEO'
and 
	(
		GestationWeeks < 32
		or	BirthWeight < 1501
	)

and DischargeDateID is not null
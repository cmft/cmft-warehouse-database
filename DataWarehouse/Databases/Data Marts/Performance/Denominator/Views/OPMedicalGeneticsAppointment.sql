﻿CREATE view [Denominator].[OPMedicalGeneticsAppointment]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.StartSiteID   
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AppointmentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OPMedicalGeneticsAppointment'

where
	Dataset.DatasetCode = 'OP'
and
	d.NationalSpecialtyCode = '311'
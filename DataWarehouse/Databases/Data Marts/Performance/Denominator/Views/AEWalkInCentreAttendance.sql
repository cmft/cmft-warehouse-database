﻿CREATE view [Denominator].[AEWalkInCentreAttendance] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ArrivalDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.AEWalkInCentreAttendance'
	
where Dataset.DatasetCode = 'AE'
and d.ContextCode = 'CEN||ADAS'
and d.EndSiteCode = 'RW3MR'
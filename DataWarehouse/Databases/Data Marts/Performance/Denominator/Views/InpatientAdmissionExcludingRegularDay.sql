﻿CREATE view [Denominator].[InpatientAdmissionExcludingRegularDay]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DirectorateID = WrkDataset.StartDirectorateID
	,SiteID = WrkDataset.StartSiteID   
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1
from
	dbo.WrkDataset
inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	NationalPatientClassificationCode <> '3'
﻿CREATE view [Denominator].[InpatientAdmissionEligibleForEDDRecordedWithin24Hours]

as

select
	Template = 'Not in use'

--select
--	DatasetID = Base.DatasetID
--	,DatasetRecno = Base.DatasetRecno
--	,DirectorateID = Base.StartDirectorateID
--	,SiteID = Base.StartSiteID   
--	,DateID = Base.DischargeDateID
--from
--	Performance.dbo.Base
--inner join
--	dbo.Dataset Dataset
--on	Base.DatasetID = Dataset.DatasetID

--where
--	DatasetCode = 'APC'
--and
--	FirstEpisodeInSpellIndicator = 1
--and
--	NationalIntendedManagementCode not in ('3','4')
--and
--	NationalSpecialtyCode <> '501'
--and
--	SourceSpecialty not like '%DIALYSIS%'
--and
--	left(SourceSpecialtycode, 2) <> 'IH'
--and
--	SourceWardCode <> '76A'	
--and
--	left(SourceWardCode, 3) <> 'SUB'
--and
--	not (
--			SourceWardCode = 'ESTU' 
--			and datediff(hour, AdmissionTime, DischargeTime) < 6
--		)
--and
--	EddCreatedTime is not null
--and
--	datediff(minute, AdmissionTime, EddCreatedTime) < 1440
﻿CREATE view [Denominator].[OutpatientFollowUpAppointment]

as

select
       DatasetID = d.DatasetID
       ,DatasetRecno = d.DatasetRecno
       ,DenominatorID = Denominator.DenominatorID
       ,SiteID = d.StartSiteID   
       ,DirectorateID = d.StartDirectorateID
       ,SpecialtyID = d.SpecialtyID
       ,ClinicianID = d.ClinicianID
       ,DateID = d.AppointmentDateID
       ,ServicePointID
       ,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on     d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on     DenominatorLogic = 'Denominator.OutpatientFollowUpAppointment'

where
       Dataset.DatasetCode = 'OP'
and
       d.NationalFirstAttendanceCode = '2' -- Follow-up Appointment
and
       d.NationalAttendanceStatusCode not in (2,4) --Cancellations
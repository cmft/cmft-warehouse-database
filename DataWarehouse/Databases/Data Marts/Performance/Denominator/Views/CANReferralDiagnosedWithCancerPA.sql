﻿CREATE view [Denominator].[CANReferralDiagnosedWithCancerPA] as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DiagnosisDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.CANReferralDiagnosedWithCancerPA'
	
where DatasetCode = 'CAN'
and D.PrimaryDiagnosisCode is not null
and D.DiagnosisDateID is not null -- screen out recs with incorrect early DiagnosisDate

and CancerSite = 'Paediatric'
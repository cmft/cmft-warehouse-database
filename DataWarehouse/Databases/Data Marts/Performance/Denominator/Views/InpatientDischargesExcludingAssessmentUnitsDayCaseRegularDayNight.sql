﻿CREATE view [Denominator].[InpatientDischargesExcludingAssessmentUnitsDayCaseRegularDayNight]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset
inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientDischargesExcludingAssessmentUnitsDayCaseRegularDayNight'

where
	DatasetCode = 'APC'
and	FirstEpisodeInSpellIndicator = 1  -- To only count the spell LoS, and no duplicates as table is at episode level
and	NationalPatientClassificationCode not in ('2','3','4') -- Exclude DC, RegDay, RegNight
and	DischargeDate is not null -- LoS, only need completed spells
and	NationalDischargeMethodCode not in (5) -- Exclude Stillbirth
and WrkDataset.HasAssessmentUnitEpisode = 0
and	WrkDataset.IsWellBabySpell = 0
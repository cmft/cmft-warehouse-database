﻿CREATE view [Denominator].[APCElectiveEpisodeStart]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeStartDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1
from
	Performance.dbo.WrkDataset
inner join
	dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.APCElectiveEpisodeStart'

where
	DatasetCode = 'APC'
and
	PatientCategoryCode = 'EL'
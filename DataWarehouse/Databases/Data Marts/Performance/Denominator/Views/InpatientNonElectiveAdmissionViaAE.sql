﻿CREATE view [Denominator].[InpatientNonElectiveAdmissionViaAE]

as

select
       DatasetID = WrkDataset.DatasetID
       ,DatasetRecno = WrkDataset.DatasetRecno
       ,DenominatorID = Denominator.DenominatorID
       ,SiteID = WrkDataset.EndSiteID
       ,DirectorateID = WrkDataset.EndDirectorateID
       ,SpecialtyID = WrkDataset.SpecialtyID
       ,ClinicianID = WrkDataset.ClinicianID
       ,DateID = WrkDataset.DischargeDateID
       ,ServicePointID = WrkDataset.StartServicePointID
       ,Value = 1
from
       Performance.dbo.WrkDataset
inner join
       dbo.Dataset Dataset
on     WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on     Denominator.DenominatorLogic = 'Denominator.InpatientNonElectiveAdmissionViaAE'

where
       DatasetCode = 'APC'
and
       NationalLastEpisodeInSpellCode = 1 -- why not first?  Keeping as is to keep consitent with other queries
and
       PatientCategoryCode = 'NE'
and
       AdmissionMethodCode = 21
and
       DischargeDate is not null
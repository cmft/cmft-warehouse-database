﻿CREATE view [Denominator].[APCDischargeExcludingDayCaseRegularDayNightNonElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID  
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCDischargeExcludingDayCaseRegularDayNightNonElective'

where
	DatasetCode = 'APC'
and	NationalLastEpisodeInSpellCode = 1
and	DischargeDateID is not null
and	NationalPatientClassificationCode not in ('2','3','4')
and	PatientCategoryCode = 'NE'
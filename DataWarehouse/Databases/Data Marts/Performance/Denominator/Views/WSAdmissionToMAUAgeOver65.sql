﻿CREATE view [Denominator].[WSAdmissionToMAUAgeOver65] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.StartSiteID
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.WardStayStartDateID
	,ServicePointID
	,Value = 1

from
	Performance.dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.WSAdmissionToMAUAgeOver65'

where Dataset.DatasetCode = 'WS'
and StartWardCode in ('MAU','AMU')
and convert(int,round(datediff(hour,DateOfBirth,AdmissionTime)/8766,0)) >= 65
﻿
USE [master]
GO

/****** Object:  Database [Performance]    Script Date: 25/08/2015 14:36:54 ******/
CREATE DATABASE [Performance] ON  PRIMARY 
( NAME = N'Performance', FILENAME = N'T:\Data\Performance.mdf' , SIZE = 37705984KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10240KB )
 LOG ON 
( NAME = N'Performance_log', FILENAME = N'G:\Log\Performance_log.LDF' , SIZE = 15168000KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [Performance] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Performance].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Performance] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [Performance] SET ANSI_NULLS OFF
GO

ALTER DATABASE [Performance] SET ANSI_PADDING OFF
GO

ALTER DATABASE [Performance] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [Performance] SET ARITHABORT OFF
GO

ALTER DATABASE [Performance] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [Performance] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [Performance] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [Performance] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [Performance] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [Performance] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [Performance] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [Performance] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [Performance] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [Performance] SET  DISABLE_BROKER
GO

ALTER DATABASE [Performance] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [Performance] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [Performance] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [Performance] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [Performance] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [Performance] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [Performance] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [Performance] SET RECOVERY SIMPLE
GO

ALTER DATABASE [Performance] SET  MULTI_USER
GO

ALTER DATABASE [Performance] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [Performance] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'Performance', N'ON'
GO

USE [Performance]
GO

/****** Object:  UserDefinedFunction [dbo].[CapFirst]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[CalendarBase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[CentralPortalClinician]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ClinicianBase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Dataset]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Denominator]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[DirectorateBase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[FactBenchmark]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[FactDenominator]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[FactNumerator]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[FactTarget]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Hierarchy]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Item]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ItemFormat]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ItemType]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Measure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Narrative]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[NarrativeType]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Numerator]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Owner]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Parameter]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ReportColour]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ReportImage]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ReportingPeriod]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[ReportMeasure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SchemaChangeLog]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[ServicePointBase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SiteBase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SpecialtyBase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Target]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TargetItemBase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[TargetTimeSeries]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[TargetType]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[WrkDataset]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [dbo].[Calendar]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ComplaintsResolvedWithin25Days]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ComplaintsUnresolvedWithin40Days]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRStaffRetention]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRSicknessAbsenceFTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRLeaversFTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRClinicalMandatoryTraining]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[Medication4To5Errors]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[NursingWorkforce]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[QCRTarget]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ComplaintVolumesReopened]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRLeaversFTERolling]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CommunityActivityDataCompleteness]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CommunityReferralCompleteness]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CommunityRTTCompleteness]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRCorporateMandatoryTraining]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupComorbiditesBest]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[IncidentsPatientFallsLevel4To5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupComorbiditesAverage]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[RTTSpecialtyNAPC]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[RTTSpecialtyIncomplete]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[RTTSpecialtyAPC]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupRCodesAverage]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HarmPressureUlcersLevel2To4]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupPalliativeCareBest]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HarmPatientFallsLevel2To5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupPalliativeCareAverage]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[IncidentsPressureUlcersGrade3To4]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[VacanciesClinicalPosts]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[SummaryHospitalLevelMortalityIndicator]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupSHMIExpectedDeathsBest]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupSHMIExpectedDeathsAverage]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ShelfordGroupRCodesBest]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ActualHarmIncidents4To5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FFTAEExtremelyLikely]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[ItemHierarchy]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[FactNarrative]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FFTIPExtremelyLikely]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FFTAEIPExtremelyLikely]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[AEShortStay]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[EWSResponseRate]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[GovernanceRiskRating]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[AgreedFinancialPlanAchievement]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CQCRisk]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[AEAverageLOS]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FFTAEResponseRate]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FFTIPResponseRate]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancelledOperations28dayBreaches]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CdiffLapseOfCare]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[DNARateFollowupAppointments]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[DNARateNewAppointments]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ElectiveActualVsContractPlan]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[OutpatientActualVsContractPlan]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRTimeToFill]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HarmAllHarms]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[MethicillinResistantStaphylococcusAureus]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[NeverEvents]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ParticipationMandatoryNationalClinicalAudits]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancelledOperations]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancerSubsequentSurgeryTreatmentWithin31Days]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancerSubsequentDrugTreatmentWithin31Days]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CdiffIncidents]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[ClinicalAuditsReportingLimitedVeryLimitedAssurance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HarmVTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HarmCatheterAssociatedUrinaryTractInfection]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[AEAttendance4Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancerTreatedWithin31DaysOfDiagnosis]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancerTreatedWithin62DaysOfUrgentGPReferral]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancerTreatedWithin62DaysOfScreeningReferral]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HospitalStandardisedMortalityRatio]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[CancerReferralSeenWithin2WeeksOfGPReferral]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[HRAppraisalCompleted]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureActualVBudget]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[RTTAPC]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[RTTNAPC]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[RTTIncomplete]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[DiagnosticPerformance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCareVTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCarePressureUlcerCategory2OrOver]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCareFallSeverity2OrOver]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCareCatheterUTI]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCareAllHarms]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[TargetItem]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[ItemHierarchyFlat]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[DenominatorItem]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[NumeratorItem]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Action]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Clinician]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Directorate]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Headline]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Indicator]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Issue]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[NarrativeDescription]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Progress]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[ServicePoint]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Site]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Specialty]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[YesNo]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ActivityPlanAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ActivityPlanElectiveSpell]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[AdmissionExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[AdmissionExcludingDayCaseRegularDayNightElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[AdmissionExcludingDayCaseRegularDayNightNonElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[AdultInpatientOrDayCaseDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[AEAttendance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[AEWalkInCentreAttendance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[Alert]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[AmbulanceArrival]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCAdmissionFromHomeAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCAdmissionUsingFirstEpisode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCCentralFCESelectedSpecsLosOver1]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCChildDischargeWithAsthmaOrDiabetes]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeCCWithOrWithout]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeExclDayCaseAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeExcludingDayCaseRegularDayNightElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeExcludingDayCaseRegularDayNightNonElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeUsingFirstEpisode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeUsingFirstEpisodeIpDcOver18]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCDischargeWithDiabetesAndHeartFailure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCElectiveEpisodeStart]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCMortalityReviewRequired]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCOpenAdmissionIpDcOver18]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[APCPICUDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANReferralDiagnosedWithCancerUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTFirstDefinitiveTreatmentForCancerUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradePA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTPriorityUpgradeUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTScreeningServiceReferralUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentDrugTreatmentUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTSubsequentSurgeryTreatmentUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANTUrgentGPReferralUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralBR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralLE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentCancerReferralUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CANUrgentGPReferralUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CASENewCasenoteExistingRegistration]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CASENewRegistration]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CASEPASEpisodeStart]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[COMAttendance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[COMAttendedOrDna]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[COMContact]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[COMContactBy6]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[Complaint]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[Complaint25DayTarget]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[Complaint40DayTarget]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ConsultantEpisodeDominantForDiagnosisExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ConsultantEpisodeDominantForDiagnosisExcludingDayCaseRegularDayNightElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ConsultantEpisodeDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ContinenceCatheterQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CRFReferral]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[CriticalCareAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[DementiaAPCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[DementiaSecDiagAPCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[DQAEStandardDisposal]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[DQStandard]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ElectiveAPCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetChildrens]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetCSS]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetDental]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetMedicineCommunity]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetOphthalmology]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetSpecialistMedicine]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetStMarys]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetSurgical]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetTrafford]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinancialExpenditureBudgetTrust]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinishedConsultantEpisode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FinishedConsultantEpisodeExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FriendsAndFamilyTestEligibleRespondersAE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FriendsAndFamilyTestEligibleRespondersIP]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FriendsAndFamilyTestPatient]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FriendsAndFamilyTestResponse]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FriendsAndFamilyTestResponseAE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FriendsAndFamilyTestResponseAEIP]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[FriendsAndFamilyTestResponseIP]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRAppraisalRequired]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRBudgetWTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRClinicalMandatoryTrainingRequired]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRCorporateMandatoryTrainingRequired]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRFTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRFTEMonthly]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRRecentAndLongTermAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HRSicknessEstablishmentFTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[HSMRDeath]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientAdmissionEligibleForEDD]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientAdmissionEligibleForEDDRecordedWithin24Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientAdmissionExcludingRegularDay]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientDischargeEligibleForDischargeSummary]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientDischargesExcludingAssessmentUnitsDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientElectiveAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientNonElectiveAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientNonElectiveAdmissionViaAE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[InpatientSpellWithProcedure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1006]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1007]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1008]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1780]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1781]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1783]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1792]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1802]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1803]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1804]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1805]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1806]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID1830]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID823]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID824]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID830]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID831]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID840]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID841]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID842]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID843]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID849]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID854]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID855]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID856]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID857]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID858]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID859]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID874]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID875]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID876]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID877]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID878]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID879]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID880]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID881]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID882]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID884]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID885]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID886]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID887]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID888]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID889]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID890]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID891]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID892]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID893]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID895]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID896]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID897]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID898]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID899]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID900]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID904]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID905]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID906]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID907]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID909]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID933]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID934]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ManualItemID993]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[MATDelivery]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[MATNormalDelivery]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[MentalHealthPrimDiagAPCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[MentalHealthPrimDiagInpatientSpell]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[MentalHealthSecDiagAPCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[MentalHealthSecDiagInpatientSpell]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NeonatalBabyBornInHospitalNotCooled]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NeonatalBabyInICU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NeonatalPrematureBabyDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NonElectiveAPCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NonElectiveAPCDischargeUsingFirstEpisode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NonElectiveAPCEpisodeStart]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceEarlyShift]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceLateShift]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceNightShift]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceNonRegisteredNursePlanDayHours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceNonRegisteredNursePlanNightHours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforcePlanDayHours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforcePlanHours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforcePlanNightHours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceRegisteredNursePlanDayHours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceRegisteredNursePlanNightHours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[NursingWorkforceShift]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[OPEROperationWithTurnaroundTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[OPMedicalGeneticsAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[OutpatientAttendedAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[OutpatientAttendedFirstAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[OutpatientFirstAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[OutpatientFollowUpAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[PATREGPatientRegistration]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRBedRailAssessmentQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRContinenceCatheterQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCREndOfLifeQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRFallsAssessmentQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRFallsQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRHandHygieneQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRHandHygieneQuestionByQuestionCode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRHydrationQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRInformationQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRMedicationQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRNightTimeQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRNutritionQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRPainManagementQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRPatientIdCheckedQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRPatientIdentificationQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRPatientObservationQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRPatientTransferQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRPressureSoresQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRPrivacyAndDignityQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRResusAndSuctionQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRSafeguardingQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[QCRSodexoFoodQuestion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RENDialysisStart]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RENDialysisStart90DaysAfterStartDate]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RENQDialysisAgeGe70]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RENQDialysisAgeLt70]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RENQDialysisInHospitalOrSatellite]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RENQDialysisPrimaryInAnySetting]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RENQDialysisPrimaryInHospitalOrSatellite]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[ResolvedComplaint]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RTTAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RTTAttendance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RTTInpatientWaiting]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[RTTOutpatientWaiting]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[SESS210MinSessions]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[SESSPlanTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[SESSSession]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[SESSSessionNotCancelled]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[SHMIExpectedDeath]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[SHMIFCE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[UncodedAPCDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Denominator].[WSAdmissionToMAUAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ActivityAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ActivityElectiveSpell]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AdmissionFromHomeAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AdmissionFromHomeDischargeElsewhereAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AdmissionOverstayingExpectedDateOfDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AdmissionToMAUAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AEAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AEAmbulanceTimeToInitialAssessment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AEAttendance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AEAttendanceGt4Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AEAttendanceLt4Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AELeftWithoutBeingSeen]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AETimeToTreatment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AEUnplannedReAttendance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AEWalkInCentreWaitOver4Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[Alert]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[AlertResponseUnder60Minutes]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCAdmissionWithProcedure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCCentralFCEMissingPrimaryProc]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCChildReadmitAfterAsthmaOrDiabetes]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCDelayedDischargeAcuteDivision]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCDischargeCCWith]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCElectiveInpatientAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCMortalityReviewCompleted]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[APCReadmitAfterDiabetesWithHeartFailure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[BedDays]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[BedDaysAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CancelledOperation28dayBreach]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CancelledOperations]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANDiscussedAtMDTAndWithPerfStatusUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANNonUrgentReferralDiagnosedWithCancerUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANPatientDiscussedAtMDTUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentDrugTreatmentWithin31DaysUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTSubsequentSurgeryTreatmentWithin31DaysUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin31DaysOfCancerDiagnosisUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradePA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfPriorityUpgradeUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfScreeningServiceReferralUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANTTreatedWithin62DaysOfUrgentGPReferralUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksBR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksLE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentCancerReferralSeenWithin2WeeksUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerCO]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerGY]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerHA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerHN]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerLU]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerOT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerPA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerSA]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerSK]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerUG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CANUrgentGPReferralDiagnosedWithCancerUR]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CASEBadCasenoteExistingRegistration]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CASEBadCasenoteNewRegistration]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CASEBadCasenotePASEpisode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CharlsonIndexDominantForDiagnosisExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CharlsonIndexDominantForDiagnosisExcludingDayCaseRegularDayNightElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CharlsonIndexDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CharlsonIndexOnAdmissionExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CharlsonIndexOnAdmissionExcludingDayCaseRegularDayNightElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CharlsonIndexOnAdmissionExcludingDayCaseRegularDayNightNonElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CodedDischWithin5DaysUsingFirstEpisode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CodedFCE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMAttendance]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMAttendanceGpReferredFaceToFace]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMCancellation]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMCancellationByProvider]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMContact]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMDna]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMHealthVisitorPrimaryVisit]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMHomeVisit]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMHomeVisitGpReferred]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMPatientIdFieldsEntered]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[Complaint]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintAccessOrADT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintBloodTransfusion]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintClinicalAssessment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintConsent]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintConsentCommunicationOrConfidentiality]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintDocumentation]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintFacilities]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintIndependentSectorPCT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintInfectionControl]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintInfrastructure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintLabTest]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintLearningAndDisability]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintMaternityChildrens]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintMaternityNeonatal]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintMedicalEquipment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintMedication]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintOPAppts]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintOutstanding]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintOutstanding40Days]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintPersonalAccident]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintPolicyAndCommercial]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintPositiveExperience]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintPostMortem]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintPressureUlcers]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintReopened]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintResolvedWithin25Days]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintSafeguarding]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintSecurity]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintStaffAttitude]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintTreatmentOrProcedure]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintTurnaround]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintUnresolvedWithin40Days]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ComplaintViolence]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[Compliment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMReferralNoContactWithin13Months]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMStaffTeamEntered]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[COMWaitDays]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CountOfCharlsonCodes]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CountOfCharlsonCodesExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CountOfInpatientDiagCodes]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CRFReferral]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CRFReferralReasonEntered]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CriticalCare48HourReadmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[CWLWaiting]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DayCase]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DeathsInElectiveInfraRenalAneurisms]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DementiaPrimDiagBedDays]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DementiaPrimDiagDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DementiaPrimDiagDischarge2]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DementiaSecDiagBedDays]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DementiaSecDiagDischarge]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DementiaSecDiagDischarge2]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DischargeOtherThanHomeAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DischargeWithPalliativeCareCoding]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DischargeWithPalliativeCareCodingExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DNAFirstAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DNAFollowUpAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateAmbulanceArrivalTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateAmbulanceInitialAssessmentTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateAmbulanceRegistrationToAssessment15]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateAttendanceConclusionTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateCommissioner]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateDateOnWaitingList]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateDepartureTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateDischargeDestination]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateDischargeMethod]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateDisposalCode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateDisposalInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateEthnicCategory]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateFirstInvestigation]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateFirstTreatment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateGPInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateGPPractice]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateGPPracticeInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateHRG]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateHRGInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateInitialAssessmentTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateNHSNumber]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateNHSNumberStatus]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateOperationStatusInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePatientTitle]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePCT]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePCTInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePostcode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePostcodeInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePracticeDefault]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePrimaryDiagnosis]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidatePrimaryDiagnosisInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateReferrer]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateReferrerInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateRegisteredToSeen60]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateRTTPathwayID]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateRTTStatus]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateSeenForTreatmentTime]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateSpecialty]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[DQValidateTreatmentFunctionInvalid]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[Elective28DayReadmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ElectiveAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ElectiveInfraRenalAneurisms]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ElectivePreOpBedDays]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ExpectedDateOfDischWithin24Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ExpectedDeathExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualChildrens]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualCSS]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualDental]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualMedicineCommunity]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualOphthalmology]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualSpecialistMedicine]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualStMarys]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualSurgical]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualTrafford]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialExpenditureActualTrust]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementChildrens]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementClinicalScientific]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementDental]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementMedicineCommunity]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementOphthalmology]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementSpecialistMedicine]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementStMarys]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementSurgical]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementTrafford]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FinancialPlanAchievementTrust]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FriendsAndFamilyTestExtremelyLikelyAE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FriendsAndFamilyTestExtremelyLikelyAEIP]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FriendsAndFamilyTestExtremelyLikelyIP]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FriendsAndFamilyTestResponseAE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[FriendsAndFamilyTestResponseIP]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingChildrens]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingCSS]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingDental]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingMedicineCommunity]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingOphthalmology]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingSpecialistMedicine]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingStMarys]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingSurgical]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingTrafford]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GovernanceRiskRatingTrust]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[GPReferral]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCare]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCareFall]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCareFallSeverity4Or5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCarePressureUlcer]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HarmFreeCarePressureUlcerCategory3Or4]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRACAgencySpend]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRAppraisalCompleted]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRClinicalMandatoryTrainingCompleted]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRCorporateMandatoryTrainingCompleted]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRLeaversFTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRLeaversFTEMonthly]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRLongTermAppointment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRQualifiedNursingMidwiferyVacancies]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRSicknessAbsenceFTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HRVacancyWTE]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[HSMRExpectedDeath]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentAdultSafeguardingAlerts]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentChildSafeguardingAlerts]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentCommunityAdultFallsGrade2OrOver]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentCommunityChildFallsGrade2OrOver]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentFall]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentFallGrade1To3]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentFallGrade4To5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentGrade1Or6]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentGrade2]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentGrade3]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentGrade4]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentGrade4Or5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentGrade5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentIncident]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentMedicationError]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentMedicationErrorGrade1AgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentMedicationErrorGrade2AgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentMedicationErrorGrade3AgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentMedicationErrorGrade4AgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentMedicationErrorGrade4To5]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentMedicationErrorGrade5AgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentPressureUlcer]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentPressureUlcerGrade2OrOverChild]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IncidentPressureUlcerGrade3To4]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InformationYes]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientBeddaysPriorToOperation]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientDischargeLetterProduced]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientDischargeLetterProducedWithin24Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientEDDCompleted]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientEDDCompletedWithin24Hours]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientInHospitalDeath]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientInHospitalDeathExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientInHospitalDeathExcludingDayCaseRegularDayNightElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientInHospitalDeathExcludingDayCaseRegularDayNightNonElective]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientLengthOfSpell]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientLengthOfSpellExcludingAssessmentUnitsDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientLengthOfSpellUsingFirstEpisode]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientNonElectiveAdmissionsViaAELoS]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientNonElectiveAdmissionsViaAELoS72hrs]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientSpellExclDayCaseAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientSpellInclDayCaseAgeOver65]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[InpatientVTEAssessmentCompleted]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IPWLCTCIWithin2Weeks]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[IPWLCTCIWithin3To6Weeks]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LegAmputationsAboveTheKnee]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LegAmputationsBelowTheKnee]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaim]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimAbscondedAccidentalInjury]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimAbscondedAttemptedSuicide]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimAbscondedSuicide]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimAssaultOnMemberOfPublic]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimAssaultOnPatient]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimAssaultOnStaff]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimBirthInjury]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimBurnsAndScaldsPatientOrPublic]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimBurnsOrScaldsStaff]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimCOSHHPatient]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimCOSHHStaff]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimDamageCannula]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimDiagnosisFailureOrDelay]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimEquipmentFailureClinical]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimEquipmentFailureNonClinical]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimFailureOrDelayInHospitalAdmission]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimFailuretoRecogniseComplications]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimFallFromBed]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimFallsSlipsAndTripsPatientOrPublic]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimFallsSlipsAndTripsStaff]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimInquest]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimManualHandlingNonPatient]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimManualHandlingPatient]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimMedicalProceduresOrOperations]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimMedicalTreatment]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimMedicationError]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimOperationDelay]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimOperationWrongPartOfBody]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimOperationWrongPatient]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimRoadTrafficAccident]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimSelfHarm]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimSharpsOrNeedlestick]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimStressSufferedbyStaff]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimStruckByObject]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimSuicide]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimSurgicalForeignBodyLeftInSitu]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimUnexpectedDeath]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimWrongDiagnosis]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[LITClaimXRayProblems]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1006]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1007]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1008]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1780]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1781]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1783]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1792]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1802]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1803]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1804]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1805]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1806]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID1830]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID823]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID824]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID830]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID831]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID840]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID841]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID842]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID843]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID849]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID854]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID855]    Script Date: 25/08/2015 14:36:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID856]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID857]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID858]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID859]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID874]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID875]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID876]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID877]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID878]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID879]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID880]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID881]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID882]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID884]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID885]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID886]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID887]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID888]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID889]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID890]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID891]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID892]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID893]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID895]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID896]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID897]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID898]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID899]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID900]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID904]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID905]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID906]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID907]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID909]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID933]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID934]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ManualItemID993]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MAT3rd4thDegreeTear]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MATCSection]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MATStillbirth]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MentalHealthPrimDiagBedDays]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MentalHealthPrimDiagDischarge]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MentalHealthPrimDiagInpatientDischarge]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MentalHealthSecDiagBedDays]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MentalHealthSecDiagDischarge]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[MentalHealthSecDiagInpatientDischarge]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NeonatalBirthBefore27WeeksInICU]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NeonatalBirthInHospTempBelow36]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NeonatalFullTermBaby]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NeonatalROPScreen]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NeonatalROPScreenInWindow]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NonElective14DayReadmission]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NonElective28DayReadmission]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NonElective30DayReadmission]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NonElectiveAdmission]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NonElectivePreOpBedDays]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NonGPReferral]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceActualDayHours]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceActualHours]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceActualNightHours]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceEarlyShiftHeadcountDeficit]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceLateShiftHeadcountDeficit]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceNightShiftHeadcountDeficit]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceNonRegisteredNurseActualDayHours]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceNonRegisteredNurseActualNightHours]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceRegisteredNurseActualDayHours]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceRegisteredNurseActualNightHours]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[NursingWorkforceShiftHeadcountDeficit]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPAttendanceWithProcedure]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPEROperation]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPERTurnaroundTime]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPERUtilisationMinutes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPFirstAppointmentWithin2Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPFirstAppointmentWithin3To6Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPFollowUpAppointmentWithin2Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPFollowUpAppointmentWithin3To6Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPMedicalGeneticsDNA]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OPSecondDNA]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OutpatientAttendedFirstAppointment]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OutpatientAttendedFollowUpAppointment]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[OutpatientDNA]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[PALSConcern]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[PATREGDuplicatePatientRegistration]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[PICU48HourReadmission]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[PreOpElectiveBedDays]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[PreOpNonElectiveBedDays]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[PrivateSurgicalAPCEpisode]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRBedRailAssessmentYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRContinenceCatheterYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCREndOfLifeYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRFallsAssessmentYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRFallsYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRHandHygieneYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRHandHygieneYesByQuestionCode]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRHydrationYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRInformationYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRMedicationYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRNightTimeYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRNutritionYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRPainManagementYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRPatientIdCheckedYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRPatientIdYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRPatientObservationYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRPatientTransferYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRPressureSoresYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRPrivacyAndDignityYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRResusAndSuctionYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRSafeguardingYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[QCRSodexoFoodYes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RCodePrimDiag]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RCodePrimDiag0to5Days]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RCodePrimDiag6to10Days]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RCodePrimDiagExcludingDayCaseRegularDayNight]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RCodePrimDiagOver10Days]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[Referral]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENFistulaGraft90DaysAfterHDStart]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENFistulaGraftOnFirstHD]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQBicarbonate20To26]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQBicarbonateGt26]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQBicarbonateLT20]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQCalcium2Point1To2Point5]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQCalciumGt2Point5]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQCalciumLt2Point1]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQDialysisPrimaryInHomeSetting]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQDialysisPrimaryInMinimalCareSetting]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQDialysisSessionDurationGt240]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQFerritin150To500]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQFistulaGraft]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQHB100To120NotOnEP]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQHB100To120OnEP]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQHBGt120NotOnEP]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQHBGt120OnEP]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQHBLt100NotOnEP]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQHBLt100OnEP]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQInterdialyticWeightGainGt2Point5]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPhosphate1Point1To1Point8]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPhosphateGt1Point8]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPhosphateLt1Point1]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPostDialysisBPGt130Over80]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPostDialysisBPLt110Over70]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPth150To800]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPthGt800]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQPthLt150]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQTransplantWaitListAgeGe70]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQTransplantWaitListAgeLt70]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RENQUreaReductionRatioGt65]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTAdmissionAfterLt18Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTAttendanceAfterLt18Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTInpatientWaiting0To25Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTInpatientWaitingGt18Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTInpatientWaitingGt25Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTOutpatientWaiting0To12Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTOutpatientWaiting13To17Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTOutpatientWaiting18To51Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTOutpatientWaitingGt18Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[RTTOutpatientWaitingGt51Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSActual210MinSession]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSAllTurnaroundsLt15Mins]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSCancelledSession]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSFinishingGt5MinsEarly]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSFinishingGt5MinsLate]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSPlanned210MinSession]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSPlannedORMISSession]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSRunTime]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSSessionEndingWithin15MinsOfPlan]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSSessionStartingWithin15MinsOfPlan]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSSessionStartingWithin5MinsOfPlan]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSStartingGt5MinsLate]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSTimeGainedWithLateFinishes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSTimeLostWithEarlyFinishes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SESSTimeLostWithLateStarts]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SHMICountOfCharlsonCodes]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SHMIDischargeWithPalliativeCareCoding]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SHMIExpectedDeath]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SHMIObservedDeath]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[SHMIRCodePrimDiag]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ShortStayNonElectiveDischarge]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[ThirtyDayReadmissionAgeOver65]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[UncodedAPCDischarge]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[UncodedAPCDischargeBreach]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[UncodedAPCDischargeNoBreach]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[VTERiskAssessment]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[VTERiskAssessmentComplete]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[VTERiskAssessmentCompleteOpenSpell]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Numerator].[WSTransferFromMAUToWardAgeOver65]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetChildrens]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetCSS]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetDental]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetMedicineCommunity]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetOphthalmology]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetSpecialistMedicine]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetStMarys]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetSurgical]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetTrafford]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Target].[FinancialExpenditureAnnualBudgetTrust]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANDiscussedAtMDTAndWithPerfStatus]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANDiscussedAtMDTAndWithPerfStatusAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANNonUrgentReferralDiagnosedWithCancer]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANNonUrgentReferralDiagnosedWithCancerAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANPatientDiscussedAtMDT]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANPatientDiscussedAtMDTAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTSubsequentDrugTreatmentWithin31Days]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTSubsequentDrugTreatmentWithin31DaysAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTSubsequentSurgeryTreatmentWithin31Days]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTSubsequentSurgeryTreatmentWithin31DaysAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin31DaysOfCancerDiagnosis]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin31DaysOfCancerDiagnosisAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin62DaysOfPriorityUpgrade]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin62DaysOfPriorityUpgradeAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin62DaysOfScreeningServiceReferral]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin62DaysOfScreeningServiceReferralAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin62DaysOfUrgentGPReferral]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANTTreatedWithin62DaysOfUrgentGPReferralAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANUrgentCancerReferralSeenWithin2Weeks]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANUrgentCancerReferralSeenWithin2WeeksAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANUrgentGPReferralDiagnosedAsCancer]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildCANUrgentGPReferralDiagnosedAsCancerAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildClinicianBase]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildComplaintByCategory]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildComplaintByCategoryAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildDirectorateBase]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildFactDenominator]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildFactNumerator]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildFactTarget]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildLinkedDQValidationView]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildLinkedDQValidationViewAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildLITClaimByCategory]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildLITClaimByCategoryAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildManuallyCollatedDataObjects]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildManuallyCollatedDataObjectsAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildModel]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildReferenceData]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildServicePointBase]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildSiteBase]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildSpecialtyBase]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildWrkDataset]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildWrkDatasetAll]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[CreateDimensionDefaultValues]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[GetIndicator]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[ReportAreasOfConcernItemNarrative]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[ReportItemNarrative]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[xxxBuildWrkDatsetSetDefaultValues]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[xxxCreateCubeDimDefaultValues]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  DdlTrigger [tr_DDL_SchemaChangeLog]    Script Date: 25/08/2015 14:36:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [Performance] SET  READ_WRITE
GO

﻿create proc [dbo].[BuildClinicianBase]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	 dbo.ClinicianBase target
using
	(
select
	 SourceClinicianID = Member.SourceValueID
	,SourceClinicianCode = Member.SourceValueCode
	,SourceClinician = Member.SourceValue
	,SourceContext = Member.SourceContext
	,LocalClinicianID = Member.LocalValueID
	,LocalClinicianCode = Member.LocalValueCode
	,LocalClinician = Member.LocalValue
	,NationalClinicianID = Member.NationalValueID
	,NationalClinicianCode = coalesce(Member.NationalValueCode,'N/A') -- Placeholder for National code and value
	,NationalClinician = 
					coalesce(Member.NationalValueCode,'N/A')
					+ ' - ' + 
								(
								select
									SourceConsultant
								from
									WarehouseOLAPMergedV2.WH.Consultant
								where
									Member.NationalValueCode = Consultant.NationalConsultantCode
								and
									not exists
												(
													select
														1
													from
														WarehouseOLAPMergedV2.WH.Consultant Previous
													where
														Consultant.NationalConsultantCode = Previous.NationalConsultantCode
													and
														Previous.SourceConsultantID < Consultant.SourceConsultantID
												)
														
								)															
	,SourceContextCode = Member.SourceContextCode
	,Consultant.ProviderCode
	,Consultant.MainSpecialtyCode
	,Consultant.MainSpecialty
	,Title = coalesce(Consultant.Title, ProfessionalCarer.Title)
	,Initials = Consultant.Initials
	,Forename = ProfessionalCarer.Forename
	,Surname = coalesce(Consultant.Surname, ProfessionalCarer.Surname)
	,ClinicanType = Member.Attribute
from
	WarehouseOLAPMergedV2.WH.Member

left outer join	WarehouseOLAPMergedV2.WH.Consultant Consultant
on	Consultant.SourceConsultantID = Member.SourceValueID

left outer join WarehouseOLAPMergedV2.COM.ProfessionalCarerBase ProfessionalCarer
on	cast(ProfessionalCarer.ProfessionalCarerID as varchar(255)) = Member.SourceValueCode 

where
	Member.AttributeCode in ('CONSUL', 'PROFCARER', 'STAFFM')

	) source
	on	source.SourceClinicianID = target.SourceClinicianID

	
	when not matched by source

	then delete

	when not matched
	then
		insert
			(
			SourceClinicianID
			,SourceClinicianCode
			,SourceClinician
			,SourceContext
			,LocalClinicianID
			,LocalClinicianCode
			,LocalClinician
			,NationalClinicianID
			,NationalClinicianCode
			,NationalClinician
			,SourceContextCode
			,ProviderCode
			,MainSpecialtyCode
			,MainSpecialty
			,Title
			,Initials
			,Forename
			,Surname
			,ClinicanType
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			SourceClinicianID
			,SourceClinicianCode
			,SourceClinician
			,SourceContext
			,LocalClinicianID
			,LocalClinicianCode
			,LocalClinician
			,NationalClinicianID
			,NationalClinicianCode
			,NationalClinician
			,SourceContextCode
			,ProviderCode
			,MainSpecialtyCode
			,MainSpecialty
			,Title
			,Initials
			,Forename
			,Surname
			,ClinicanType
			,getdate()
			,getdate()
			,suser_name()			
			)

	when matched
	and not
		(	
			isnull(target.SourceClinicianID, 0) = isnull(source.SourceClinicianID, 0)	
		and isnull(target.SourceClinicianCode, 0) = isnull(source.SourceClinicianCode, 0)					
		and isnull(target.SourceClinician, 0) = isnull(source.SourceClinician, 0)	
		and isnull(target.SourceContext, 0) = isnull(source.SourceContext, 0)	
		and	isnull(target.LocalClinicianID, 0) = isnull(source.LocalClinicianID, 0)
		and	isnull(target.LocalClinicianCode, 0) = isnull(source.LocalClinicianCode, 0)	
		and	isnull(target.LocalClinician, 0) = isnull(source.LocalClinician, 0)
		and	isnull(target.NationalClinicianID, 0) = isnull(source.NationalClinicianID, 0)
		and	isnull(target.NationalClinicianCode, 0) = isnull(source.NationalClinicianCode, 0)		
		and isnull(target.NationalClinician, 0) = isnull(source.NationalClinician, 0)
		and isnull(target.SourceContextCode, 0) = isnull(source.SourceContextCode, 0)		
		and isnull(target.ProviderCode, 0) = isnull(source.ProviderCode, 0)
		and isnull(target.MainSpecialtyCode, 0) = isnull(source.MainSpecialtyCode, 0)
		and isnull(target.MainSpecialty, 0) = isnull(source.MainSpecialty, 0)
		and isnull(target.Title, 0) = isnull(source.Title, 0)	
		and isnull(target.Initials, 0) = isnull(source.Initials, 0)
		and isnull(target.Forename, 0) = isnull(source.Forename, 0)
		and isnull(target.Surname, 0) = isnull(source.Surname, 0)
		and isnull(target.ClinicanType, 0) = isnull(source.ClinicanType, 0)
		)
	then
		update
		set
			target.SourceClinicianID = source.SourceClinicianID
			,target.SourceClinicianCode = source.SourceClinicianCode
			,target.SourceClinician = source.SourceClinician
			,target.SourceContext = source.SourceContext	
			,target.LocalClinicianID = source.LocalClinicianID
			,target.LocalClinicianCode = source.LocalClinicianCode							
			,target.LocalClinician = source.LocalClinician
			,target.NationalClinicianID = source.NationalClinicianID	
			,target.NationalClinicianCode = source.NationalClinicianCode
			,target.NationalClinician = source.NationalClinician
			,target.SourceContextCode = source.SourceContextCode

			,target.ProviderCode = source.ProviderCode
			,target.MainSpecialtyCode = source.MainSpecialtyCode
			,target.MainSpecialty = source.MainSpecialty							
			,target.Title = source.Title
			,target.Initials = source.Initials
			,target.Forename = source.Forename
			,target.Surname = source.Surname
			,target.ClinicanType = source.ClinicanType
			
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿CREATE procedure [dbo].[BuildCANUrgentCancerReferralSeenWithin2Weeks] (@type char(2), @site varchar(100), @label varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CAN patient seen < 2 weeks of urgent GP referral for suspected cancer - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CAN patient seen < 2 weeks of urgent GP referral for suspected cancer - ' + @type, 'Numerator.CANUrgentCancerReferralSeenWithin2Weeks' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Denominator record, if it does not exist
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CAN urgent cancer referral - ' + @type)

if @DenominatorID is null
begin
	insert into Denominator (Denominator, DenominatorLogic)
	values ('CAN urgent cancer referral - ' + @type, 'Denominator.CANUrgentCancerReferral' + @type)
	select @DenominatorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = (select ItemID from Item where Item = 'Patients seen within 2 weeks of an urgent GP referral for suspected cancer - ' + @label)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Patients seen within 2 weeks of an urgent GP referral for suspected cancer - ' + @label, 2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end


-- Create view DenominatorCANUrgentCancerReferral with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Denominator.CANUrgentCancerReferral' + @type

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_2WEEK_WAIT

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AppointmentDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and CancerTypeCode = ''' + @site + '''
and PriorityTypeCode = ''03'' -- to be seen within 2 weeks
and (InappropriateReferralCode <> 1 or InappropriateReferralCode is null)
--and ReferralSourceCode in (''03'',''98'') -- GP or Dentist removed after meeting with Ian C 4/4/14
and AppointmentDateID is not null -- Count only patients seen within period 
--and datediff(day, ReferralDate, ReceiptOfReferralDate) < 2 
'
exec(@sql)

-- 21.3.14 - IC requested the datediff(day, ReferralDate, ReceiptOfReferralDate) < 2  change
-- Create view NumeratorCANUrgentCancerReferralSeenWithin2Weeks with a suffix for the given CancerTypeCode

set @view = 'Numerator.CANUrgentCancerReferralSeenWithin2Weeks' + @type

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
	
set @sql = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_2WEEK_WAIT

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AppointmentDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and CancerTypeCode = ''' + @site + '''
and PriorityTypeCode = ''03'' -- to be seen within 2 weeks
and (InappropriateReferralCode <> 1 or InappropriateReferralCode is null) 
--and ReferralSourceCode in (''03'',''98'') -- GP or Dentist removed after meeting with Ian C 4/4/14
--and datediff(day, ReferralDate, ReceiptOfReferralDate) < 2 
and datediff(d,ReferralDate,AppointmentDate) - coalesce(FirstAppointmentWaitingTimeAdjusted, 0) <= 14
'

exec(@sql)
﻿CREATE proc [dbo].[BuildManuallyCollatedDataObjects]

(
	@ItemID varchar(10)
)

as


-- Insert Denominator record, if it does not exist

declare @DenominatorID int = (select DenominatorID from Denominator where DenominatorLogic = 'Denominator.ManualItemID' + cast(@ItemID as varchar))

--select @DenominatorID

if @DenominatorID is null
begin
	insert into Denominator (Denominator,DenominatorLogic)
	values ('Manual - ItemID - ' + cast(@ItemID as varchar), 'Denominator.ManualItemID' + cast(@ItemID as varchar))
	
	select @DenominatorID = scope_identity() -- for use in the ItemID update 
end

-- Insert Numerator record, if it does not exist

declare @NumeratorID int = (select NumeratorID from Numerator where NumeratorLogic = 'Numerator.ManualItemID' + cast(@ItemID as varchar))

--select @NumeratorID

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('Manual - ItemID - ' + cast(@ItemID as varchar), 'Numerator.ManualItemID' + cast(@ItemID as varchar))
	
	select @NumeratorID = scope_identity() -- for use in the ItemID update 
end

-- Update the denominator and numerator config for the indicator

begin
	update Item 
	set NumeratorID = @NumeratorID
		,DenominatorID = @DenominatorID 
	where
		ItemID = @ItemID
end

-- Create Denominator view 

declare @view varchar(100) = 'Denominator.ManualItemID' + cast(@ItemID as varchar)

if exists(select * from sys.views where object_id = OBJECT_ID(@view)) 
exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DateID
	,ServicePointID = D.ServicePointID
	,Value = D.Denominator

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where
	Dataset.DatasetCode = ''MAN''
and ItemID = ''' + cast(@ItemID as varchar) + ''''

exec(@sql)


-- Create Numerator view 

set @view = 'Numerator.ManualItemID' + cast(@ItemID as varchar)

if exists(select * from sys.views where object_id = OBJECT_ID(@view)) 
exec('drop view ' + @view)
	
set @sql = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DateID
	,ServicePointID = D.ServicePointID
	,Value = D.Numerator

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where
	Dataset.DatasetCode = ''MAN''
and ItemID = ''' + cast(@ItemID as varchar) + ''''

exec(@sql)
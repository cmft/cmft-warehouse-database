﻿create procedure [dbo].[BuildCANDiscussedAtMDTAndWithPerfStatusAll] as

exec BuildCANDiscussedAtMDTAndWithPerfStatus 'CO', 'Colorectal'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'GY', 'Gynaecology'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'HA', 'Haematology'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'HN', 'Head and Neck'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'LU', 'Lung'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'OT', 'Other'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'PA', 'Paediatric'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'SA', 'Sarcoma'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'SK', 'Skin'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'UG', 'Upper GI'
exec BuildCANDiscussedAtMDTAndWithPerfStatus 'UR', 'Urology'
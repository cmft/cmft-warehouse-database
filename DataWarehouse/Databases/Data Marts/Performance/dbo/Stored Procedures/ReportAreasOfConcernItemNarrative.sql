﻿CREATE procedure [dbo].[ReportAreasOfConcernItemNarrative]
	@ItemID int
as

/*
exec dbo.ReportAreasOfConcernItemNarrative @ItemID = 901 --Crude Mortality - Elective
*/

select
	 NarrativeType.NarrativeType
	,Narrative.Narrative
from
	dbo.Narrative

inner join dbo.NarrativeType
on	NarrativeType.NarrativeTypeID = Narrative.NarrativeTypeID

where
	NarrativeType.NarrativeTypeID in (2, 3, 4)
and	Narrative.ItemID = @ItemID
and	Narrative.Active = 1
and	not exists
	(
		select
			1
		from
			dbo.Narrative Later
		where
			Later.ItemID = Narrative.ItemID
		and	Later.Active = 1
		and Later.NarrativeTypeID = Narrative.NarrativeTypeID
		and	Later.Updated > Narrative.Updated
	)

order by
	NarrativeType.NarrativeTypeOrder
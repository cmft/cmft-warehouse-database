﻿CREATE procedure [dbo].[BuildCANTSubsequentSurgeryTreatmentWithin31DaysAll] as

exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'CO', 'Colorectal'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'GY', 'Gynaecology'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'HA', 'Haematology'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'HN', 'Head and Neck'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'LU', 'Lung'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'OT', 'Other'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'PA', 'Paediatric'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'SA', 'Sarcoma'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'SK', 'Skin'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'UG', 'Upper GI'
exec BuildCANTSubsequentSurgeryTreatmentWithin31Days 'UR', 'Urology'
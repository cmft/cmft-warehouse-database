﻿create procedure [dbo].[CreateDimensionDefaultValues] 

(
@ContextCode varchar(20)
)

as

-- Create recs in Map.Value with values '-1' and 'Not Specified' for the attributes which form the dimensions of the Performance cube,
-- for each Context in WrkDataset where these recs do not already exist

-- Directorate is excluded, as it is created by an allocation process, which has its own, non-context-specific default

-- This procedure should be run whenever a new Context is introduced to the WrkDataset table

-- (1) Create ContextAttribute records for Contexts in WrkDataset, where these do not exist

insert into ReferenceMapV2.Map.AttributeContext 
(
	AttributeID
	,ContextID
)
select
	AttributeID
	,ContextID
from
	ReferenceMapV2.Map.Attribute

inner join ReferenceMapV2.Map.Context 
on Context.ContextCode = @ContextCode 
	
where	
	Attribute.AttributeCode in (
								'CONSUL'
								,'SPEC'
								,'SITE'
								)
and not exists
	(
		select
			1 
		from
			ReferenceMapV2.Map.AttributeContext
		where
			AttributeContext.AttributeID = Attribute.AttributeID 
		and AttributeContext.ContextID = Context.ContextID
	)

-- (2) Create default recs, where these do not exist

insert into ReferenceMapV2.Map.Value 
(
	AttributeContextID
	,ValueCode
	,Value
	,Created
	,Updated
	,ByWhom
)
select
	AttributeContextID
	,'-1'
	,'Not Specified'
	,getdate()
	,getdate()
	,system_user
from
	ReferenceMapV2.Map.AttributeContext

inner join ReferenceMapV2.Map.Context 
on	Context.ContextID = AttributeContext.ContextID
and Context.ContextCode = @ContextCode 
	
inner join ReferenceMapV2.Map.Attribute 
on	Attribute.AttributeID = AttributeContext.AttributeID
and Attribute.AttributeCode in (
								'CONSUL'
								,'SPEC'
								,'SITE'
								)
and not exists
(
	select
		1
	from
		ReferenceMapV2.Map.Value
	where
		Value.AttributeContextID = AttributeContext.AttributeContextID
	and Value.ValueCode = '-1'
)

if @@ROWCOUNT <> 0
begin

	exec ReferenceMapv2.Map.BuildMissingXrefs
	
	-- Write new values to WH.Member table
	exec WarehouseOLAPMergedV2.WH.BuildMember
end
﻿CREATE proc [dbo].[BuildFactDenominator]

as


	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)


	declare @Template varchar(128)
	declare @WrkSQL varchar(max)
	declare @SQLStartTime datetime

	select @StartTime = getdate()


	truncate table dbo.FactDenominator

	declare TemplateCursor cursor fast_forward for

	select distinct
		DenominatorLogic
	from
		dbo.Denominator
	where
		Active = 1
	-- only load records where the denominator is actually defined in the item hierarchy
	and
		exists
			(
				select
					1
				from
					dbo.ItemHierarchy
				where
					Denominator.DenominatorID = ItemHierarchy.DenominatorID
			)
	order by
		DenominatorLogic

	open TemplateCursor

	fetch next from TemplateCursor

	into
		 @Template

	while @@fetch_status = 0

	begin

		select
			@WrkSQL =
				'insert into dbo.FactDenominator (DatasetID, DatasetRecno, DenominatorID, SiteID, DirectorateID, SpecialtyID, ClinicianID, DateID, ServicePointID, Value)
				select DatasetID, DatasetRecno, DenominatorID, SiteID, DirectorateID, SpecialtyID, ClinicianID, DateID, ServicePointID, Value from ' + @Template + ''

		

		print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL

		set
			@SQLStartTime = getdate()

		execute(@WrkSQL)

		print replicate('* ', datediff(minute, @SQLStartTime, getdate()))

		fetch next from TemplateCursor
		into @Template
	end
	  
	close TemplateCursor
	deallocate TemplateCursor
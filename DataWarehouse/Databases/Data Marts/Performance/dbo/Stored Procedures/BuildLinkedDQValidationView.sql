﻿CREATE procedure [dbo].[BuildLinkedDQValidationView] (@logic varchar(255)) as

-- Build a Performance view by linking to an existing DQ view in WarehouseOLAPMergedV2

-- @logic is name of DQ validation view

declare @view varchar(255) = 'Numerator.DQ' + @logic -- Name of Performance view

-- Drop view if it exists
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

select 
	 DatasetID = v.DatasetID
	,DatasetRecno = v.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EncounterDateID
	,ServicePointID = -1
	,Value = 1

-- Pick up dataset and recno from DQ output table ...
from WarehouseOLAPMergedV2.DQ.DatasetValidation v

-- ... for relevant logic
inner join WarehouseOLAPMergedV2.DQ.Error e
on e.ErrorID = v.ErrorID
and e.ErrorLogic = ''DQ.' + @logic + '''

-- Join to WrkDataset for remaining required fields
inner join WarehouseOLAPMergedV2.DQ.Dataset ds1 on ds1.DatasetID = v.DatasetID
inner join Performance.dbo.Dataset ds2 on ds2.DatasetCode = ds1.DatasetCode

inner join dbo.WrkDataset d on d.DatasetID = ds2.DatasetID
and d.DatasetRecno = v.DatasetRecno

inner join dbo.Numerator
on	Numerator.NumeratorLogic = ''' + @view + ''''

exec(@sql)
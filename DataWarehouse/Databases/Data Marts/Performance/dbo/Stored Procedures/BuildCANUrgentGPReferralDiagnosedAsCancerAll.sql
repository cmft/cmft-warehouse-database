﻿CREATE procedure [dbo].[BuildCANUrgentGPReferralDiagnosedAsCancerAll] as

exec BuildCANUrgentGPReferralDiagnosedAsCancer 'CO', 'Colorectal'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'GY', 'Gynaecology'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'HA', 'Haematology'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'HN', 'Head and Neck'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'LU', 'Lung'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'OT', 'Other'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'PA', 'Paediatric'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'SA', 'Sarcoma'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'SK', 'Skin'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'UG', 'Upper GI'
exec BuildCANUrgentGPReferralDiagnosedAsCancer 'UR', 'Urology'
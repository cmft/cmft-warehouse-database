﻿CREATE proc [dbo].[BuildFactTarget]

as


	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)


	declare @Template varchar(128)
	declare @WrkSQL varchar(max)

	select @StartTime = getdate()


	truncate table dbo.FactTarget

	declare TemplateCursor cursor fast_forward for

	select distinct
		TargetLogic
	from
		dbo.Target
	where
		Active = 1	
	
	order by
		TargetLogic

	open TemplateCursor

	fetch next from TemplateCursor

	into
		 @Template

	while @@fetch_status = 0

	begin

		select
			@WrkSQL =
				'insert into dbo.FactTarget (TargetID, DateID, Value)
				select TargetID, DateID, Value from ' + @Template + ''
		
		print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL

		execute(@WrkSQL)

		fetch next from TemplateCursor
		into @Template
	end
	  
	close TemplateCursor
	deallocate TemplateCursor
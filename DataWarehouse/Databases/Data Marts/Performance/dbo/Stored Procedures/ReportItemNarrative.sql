﻿create procedure [dbo].[ReportItemNarrative]
	 @ItemID int
	,@NarrativeTypeID int
as

/*
exec dbo.ReportItemNarrative @ItemID = 860, @NarrativeTypeID = 1
exec dbo.ReportItemNarrative @ItemID = 12, @NarrativeTypeID = 2
exec dbo.ReportItemNarrative @ItemID = 901, @NarrativeTypeID = 2 --Crude Mortality - Elective
*/

select
	 Narrative.NarrativeID
	,Narrative.ItemID
	,Narrative.NarrativeTypeID
	,NarrativeType.NarrativeType
	,Narrative.Narrative
	,Narrative.Updated
	,Narrative.ByWhom
from
	dbo.Narrative

inner join dbo.NarrativeType
on	NarrativeType.NarrativeTypeID = Narrative.NarrativeTypeID

where
	ItemID = @ItemID
and	Narrative.NarrativeTypeID = @NarrativeTypeID
and	not exists
			(
				select
					1
				from
					Performance.dbo.Narrative Later
				where
					Later.ItemID = Narrative.ItemID
				and Later.NarrativeTypeID = Narrative.NarrativeTypeID
				and	Later.Updated > Narrative.Updated
			)
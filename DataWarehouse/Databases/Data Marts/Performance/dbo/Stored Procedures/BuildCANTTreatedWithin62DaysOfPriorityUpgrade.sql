﻿CREATE procedure [dbo].[BuildCANTTreatedWithin62DaysOfPriorityUpgrade](@type char(2), @site varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CANT Patient treated < 62 days of priority upgrade - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CANT Patient treated < 62 days of priority upgrade - ' + @type, 'Numerator.CANTTreatedWithin62DaysOfPriorityUpgrade' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Denominator record, if it does not exist
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CANT priority upgrade - ' + @type)

if @DenominatorID is null
begin
	insert into Denominator (Denominator, DenominatorLogic)
	values ('CANT priority upgrade - ' + @type, 'Denominator.CANTPriorityUpgrade' + @type)
	select @DenominatorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = 
(select ItemID from Item 
	where Item = 'Patients receiving first definitive treatment for cancer within 62 days of a consultant decision to upgrade their priority status - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Patients receiving first definitive treatment for cancer within 62 days of a consultant decision to upgrade their priority status - ' + @site
		,2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end


-- Create view DenominatorCANTPriorityUpgrade with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Denominator.CANTPriorityUpgrade' + @type

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_UPGRADE_TREAT

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where DatasetCode = ''CANT''
and D.TumourStatusCode not in (''4'', ''5'')
and coalesce(D.TreatmentNo,0) <> 2
and coalesce(D.InappropriateReferralCode,0) <> ''1''
and ConsultantUpgradeDate is not null

and (D.PrimaryDiagnosisCode1 is null or D.PrimaryDiagnosisCode1 not in (''C62'', ''C620'', ''C621'', ''C629'', ''C910'', ''C920'', ''C924'', ''C925'', ''C930'', ''C942'', ''C950''))

and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') = ''C44'' 
	and coalesce(D.HistologyCode,'''') in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'')
)

and not 
(
	coalesce(D.PrimaryDiagnosisCode,'''') <> ''D05'' 
	and left(coalesce(D.PrimaryDiagnosisCode,''''),1) = ''D''
)

and CancerSite = ''' + @Site + ''''

exec(@sql)


-- Create view NumeratorCANTTreatedWithin62DaysOfPriorityUpgrade with a suffix for the given CancerTypeCode

set @view = 'Numerator.CANTTreatedWithin62DaysOfPriorityUpgrade' + @type

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
	
set @sql = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_UPGRADE_TREAT

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CANT''
and D.TumourStatusCode not in (''4'', ''5'')
and coalesce(D.TreatmentNo,0) <> 2
and coalesce(D.InappropriateReferralCode,0) <> ''1''
and ConsultantUpgradeDate is not null

and (D.PrimaryDiagnosisCode1 is null or D.PrimaryDiagnosisCode1 not in (''C62'', ''C620'', ''C621'', ''C629'', ''C910'', ''C920'', ''C924'', ''C925'', ''C930'', ''C942'', ''C950''))

and not
(
	coalesce(D.PrimaryDiagnosisCode,'''') = ''C44'' 
	and coalesce(D.HistologyCode,'''') in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'')
)

and not 
(
	coalesce(D.PrimaryDiagnosisCode,'''') <> ''D05'' 
	and left(coalesce(D.PrimaryDiagnosisCode,''''),1) = ''D''
)

and datediff(d,D.ConsultantUpgradeDate,D.TreatmentDate)
- (
	case 
		when D.TreatmentSettingCode in (''01'',''02'') 
		and D.DecisionToTreatAdjustmentReasonCode = ''8'' 
		then coalesce(D.DecisionToTreatAdjustment,0) 
		else 0 
	end
) 
- coalesce(D.FirstAppointmentWaitingTimeAdjusted,0) 
- (
	case 
		when D.FirstAppointmentAdjustmentReasonCode = ''3'' 
		and D.CancelledAppointmentDate IS NOT NULL 
		then coalesce(datediff(d, D.ConsultantUpgradeDate, D.CancelledAppointmentDate), 0) 
		else 0 
	end
)
<= 62

and CancerSite = ''' + @Site + ''''

exec(@sql)
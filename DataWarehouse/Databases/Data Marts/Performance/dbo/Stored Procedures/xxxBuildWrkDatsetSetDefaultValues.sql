﻿CREATE procedure [dbo].[xxxBuildWrkDatsetSetDefaultValues] (@DatasetCode varchar(100)) as

-- Convert nulls to default values for fields which form the dimensions of the Performance cube

update WrkDataset

set 
	ClinicianID = case when ClinicianID is null then Consultant.SourceValueID else ClinicianID end
	,ClinicianCode = case when ClinicianID is null then Consultant.SourceValueCode else ClinicianCode end
	,StartDirectorateID = case when StartDirectorateID is null then Directorate.DirectorateID else StartDirectorateID end
	,StartDirectorateCode = case when StartDirectorateID is null then Directorate.DirectorateCode else StartDirectorateCode end
	,EndDirectorateID = case when EndDirectorateID is null then Directorate.DirectorateID else EndDirectorateID end
	,EndDirectorateCode = case when EndDirectorateID is null then Directorate.DirectorateCode else EndDirectorateCode end
	,SpecialtyID = case when SpecialtyID is null then Specialty.SourceValueID else SpecialtyID end
	,SpecialtyCode = case when SpecialtyID is null then Specialty.SourceValueCode else SpecialtyCode end
	,StartSiteID = case when StartSiteID is null then Site.SourceValueID else StartSiteID end
	,StartSiteCode = case when StartSiteID is null then Site.SourceValueCode else StartSiteCode end
	,EndSiteID = case when EndSiteID is null then Site.SourceValueID else EndSiteID end
	,EndSiteCode = case when EndSiteID is null then Site.SourceValueCode else EndSiteCode end
	,ServicePointID = coalesce(WrkDataset.ServicePointID, -1)
	,StartServicePointID = coalesce(WrkDataset.StartServicePointID, -1)

from WrkDataset

join Dataset on Dataset.DatasetID = WrkDataset.DatasetID

join WarehouseOLAPMergedV2.WH.Member Consultant
on Consultant.SourceContextCode = WrkDataset.ContextCode
and Consultant.AttributeCode = 'CONSUL'
and Consultant.SourceValueCode = '-1'

join WarehouseOLAPMergedV2.WH.Directorate
on DirectorateCode = '9'
	
join WarehouseOLAPMergedV2.WH.Member Specialty
on Specialty.SourceContextCode = WrkDataset.ContextCode
and Specialty.AttributeCode = 'SPEC'
and Specialty.SourceValueCode = '-1'

join WarehouseOLAPMergedV2.WH.Member Site
on Site.SourceContextCode = WrkDataset.ContextCode
and Site.AttributeCode = 'SITE'
and Site.SourceValueCode = '-1'

where Dataset.DatasetCode = @DatasetCode 
and 
(
	WrkDataset.ClinicianID is null 
	or WrkDataset.StartDirectorateID is null
	or WrkDataset.EndDirectorateID is null
	or WrkDataset.SpecialtyID is null
	or WrkDataset.StartSiteID is null
	or WrkDataset.EndSiteID is null
	or WrkDataset.ServicePointID is null
	or WrkDataset.StartServicePointID is null
)
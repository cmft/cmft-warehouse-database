﻿create proc [dbo].[BuildManuallyCollatedDataObjectsAll]

as	
	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)


	declare @ItemID varchar(10)
	declare @WrkSQL varchar(max)

	select @StartTime = getdate()

	declare ItemCursor cursor fast_forward for

	select
		ItemID
	from
		dbo.Item
	where
		Manual = 1
	and	Active = 1

	order by
		ItemID

	open ItemCursor

	fetch next from ItemCursor

	into
		 @ItemID
		 
	while @@fetch_status = 0

	begin
		select
			@WrkSQL =
				'dbo.BuildManuallyCollatedDataObjects ''' + @ItemID + ''''
		
	print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL
	
	execute(@WrkSQL)
		
	fetch next from ItemCursor
	into @ItemID
	
	end
		 
	close ItemCursor
	deallocate ItemCursor

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿CREATE procedure [dbo].[BuildCANTTreatedWithin62DaysOfUrgentGPReferralAll] as

exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'CO', 'Colorectal'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'GY', 'Gynaecology'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'HA', 'Haematology'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'HN', 'Head and Neck'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'LU', 'Lung'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'OT', 'Other'
-- exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'PA', 'Paediatric' -- Original report RPT_SCREEN_TREAT excluded Paediatrics
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'SA', 'Sarcoma'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'SK', 'Skin'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'UG', 'Upper GI'
exec BuildCANTTreatedWithin62DaysOfUrgentGPReferral 'UR', 'Urology'
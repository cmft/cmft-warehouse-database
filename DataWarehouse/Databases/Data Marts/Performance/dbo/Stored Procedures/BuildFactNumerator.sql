﻿CREATE proc [dbo].[BuildFactNumerator]

as

	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)


	declare @Template varchar(128)
	declare @WrkSQL varchar(max)
	declare @SQLStartTime datetime

	select @StartTime = getdate()


	truncate table dbo.FactNumerator

	declare TemplateCursor cursor fast_forward for

	select distinct
		NumeratorLogic
	from
		dbo.Numerator
	where
		Active = 1
	and
	-- only load records where the numerator is actually defined in the item hierarchy
		exists
			(
				select
					1
				from
					dbo.ItemHierarchy
				where
					Numerator.NumeratorID = ItemHierarchy.NumeratorID
			)
	order by
		NumeratorLogic

	open TemplateCursor

	fetch next from TemplateCursor

	into
		 @Template

	while @@fetch_status = 0

	begin

		select
			@WrkSQL =
				'insert into dbo.FactNumerator (DatasetID, DatasetRecno, NumeratorID, SiteID, DirectorateID, SpecialtyID, ClinicianID, DateID, ServicePointID, Value)
				select DatasetID, DatasetRecno, NumeratorID, SiteID, DirectorateID, SpecialtyID, ClinicianID, DateID, ServicePointID, Value from ' + @Template + ''
		
		print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL

		set
			@SQLStartTime = getdate()

		execute(@WrkSQL)

		print replicate('* ', datediff(minute, @SQLStartTime, getdate()))

		fetch next from TemplateCursor
		into @Template
	end
	  
	close TemplateCursor
	deallocate TemplateCursor
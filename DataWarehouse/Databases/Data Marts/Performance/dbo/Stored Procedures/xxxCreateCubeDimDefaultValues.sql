﻿CREATE procedure [dbo].[xxxCreateCubeDimDefaultValues] as

-- Create recs in Map.Value with values '-1' and 'Not Specified' for the attributes which form the dimensions of the Performance cube,
-- for each Context in WrkDataset where these recs do not already exist

-- Directorate is excluded, as it is created by an allocation process, which has its own, non-context-specific default

-- This procedure should be run whenever a new Context is introduced to the WrkDataset table

-- (1) Create ContextAttribute records for Contexts in WrkDataset, where these do not exist
insert into ReferenceMapV2.Map.AttributeContext (AttributeID, ContextID)
select
	AttributeID
	,ContextID
from ReferenceMapV2.Map.Attribute A
join ReferenceMapV2.Map.Context C on C.ContextCode in 
	(
		'CMFT||PERFMON'
		--select distinct ContextCode from Performance.dbo.WrkDataset
	)
where A.AttributeCode in ('CONSUL','SPEC','SITE')
and not exists
	(
		select 1 
		from ReferenceMapV2.Map.AttributeContext AC
		where AC.AttributeID = A.AttributeID and AC.ContextID = C.ContextID
	)

-- (2) Create default recs, where these do not exist
insert into ReferenceMapV2.Map.Value (AttributeContextID, ValueCode, Value, Created, Updated, ByWhom)
select
	AttributeContextID
	,'-1'
	,'Not Specified'
	,getdate()
	,getdate()
	,system_user
from ReferenceMapV2.Map.AttributeContext AC
join ReferenceMapV2.Map.Context C on C.ContextID = AC.ContextID
and C.ContextCode in
	(
		'CMFT||PERFMON'
		--select distinct ContextCode from Performance.dbo.WrkDataset
	)
join ReferenceMapV2.Map.Attribute A on A.AttributeID = AC.AttributeID
and A.AttributeCode in ('CONSUL','SPEC','SITE')
and not exists
(
	select 1
	from ReferenceMapV2.Map.Value V
	where V.AttributeContextID = AC.AttributeContextID
	and V.ValueCode = '-1'
)

if @@ROWCOUNT <> 0
begin

	exec ReferenceMapv2.Map.BuildMissingXrefs
	
	-- Write new values to WH.Member table
	exec WarehouseOLAPMergedV2.WH.BuildMember
end
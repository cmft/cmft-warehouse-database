﻿CREATE TABLE [dbo].[Narrative](
	[NarrativeID] [int] IDENTITY(1,1) NOT NULL,
	[NarrativeFromDate] [date] NOT NULL,
	[ItemID] [int] NOT NULL,
	[NarrativeTypeID] [int] NOT NULL,
	[NarrativeHTML] [varchar](4000) NULL,
	[Narrative] [varchar](4000) NOT NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_Narrative_Active]  DEFAULT ((1)),
	[Updated] [datetime] NULL CONSTRAINT [DF__Narrative__Updat__09B529F1]  DEFAULT (getdate()),
	[ByWhom] [varchar](50) NULL CONSTRAINT [DF__Narrative__ByWho__0AA94E2A]  DEFAULT (suser_sname()),
 CONSTRAINT [PK__Narrativ__7B97293107CCE17F] PRIMARY KEY CLUSTERED 
(
	[NarrativeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_Narrative]    Script Date: 25/08/2015 14:36:56 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Narrative] ON [dbo].[Narrative]
(
	[ItemID] ASC,
	[NarrativeTypeID] ASC,
	[NarrativeFromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
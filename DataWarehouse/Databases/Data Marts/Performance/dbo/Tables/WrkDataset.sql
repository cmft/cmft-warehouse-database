﻿CREATE TABLE [dbo].[WrkDataset](
	[DatasetID] [int] NOT NULL,
	[DatasetRecno] [int] NOT NULL,
	[ContextCode] [varchar](255) NULL,
	[SourcePatientNo] [varchar](20) NULL,
	[DateOfBirth] [date] NULL,
	[ProviderSpellNo] [varchar](50) NULL,
	[SourceEncounterNo] [varchar](20) NULL,
	[StartDirectorateID] [int] NULL,
	[StartDirectorateCode] [varchar](5) NULL,
	[EndDirectorateID] [int] NULL,
	[EndDirectorateCode] [varchar](5) NULL,
	[SpecialtyID] [int] NULL,
	[SpecialtyCode] [varchar](20) NULL,
	[Specialty] [varchar](50) NULL,
	[NationalSpecialtyCode] [varchar](10) NULL,
	[ClinicianID] [int] NULL,
	[ClinicianCode] [varchar](50) NULL,
	[StartSiteID] [int] NULL,
	[StartSiteCode] [varchar](10) NULL,
	[EndSiteID] [int] NULL,
	[EndSiteCode] [varchar](10) NULL,
	[StartWardCode] [varchar](50) NULL,
	[EndWardCode] [varchar](50) NULL,
	[ReferralDateID] [int] NULL,
	[ReferralDate] [date] NULL,
	[AdmissionDateID] [int] NULL,
	[AdmissionDate] [date] NULL,
	[AdmissionTime] [datetime] NULL,
	[DischargeDateID] [int] NULL,
	[DischargeDate] [date] NULL,
	[DischargeTime] [datetime] NULL,
	[EpisodeStartDateID] [int] NULL,
	[EpisodeStartDate] [smalldatetime] NULL,
	[EpisodeEndDateID] [int] NULL,
	[EpisodeEndDate] [smalldatetime] NULL,
	[FirstEpisodeInSpellIndicator] [bit] NULL,
	[NationalLastEpisodeInSpellCode] [varchar](20) NULL,
	[PrimaryDiagnosisCode] [varchar](50) NULL,
	[SubsidiaryDiagnosisCode] [varchar](50) NULL,
	[SecondaryDiagnosisCode1] [varchar](50) NULL,
	[SecondaryDiagnosisCode2] [varchar](50) NULL,
	[SecondaryDiagnosisCode3] [varchar](50) NULL,
	[SecondaryDiagnosisCode4] [varchar](50) NULL,
	[SecondaryDiagnosisCode5] [varchar](50) NULL,
	[SecondaryDiagnosisCode6] [varchar](50) NULL,
	[SecondaryDiagnosisCode7] [varchar](50) NULL,
	[SecondaryDiagnosisCode8] [varchar](50) NULL,
	[SecondaryDiagnosisCode9] [varchar](50) NULL,
	[SecondaryDiagnosisCode10] [varchar](50) NULL,
	[SecondaryDiagnosisCode11] [varchar](50) NULL,
	[SecondaryDiagnosisCode12] [varchar](50) NULL,
	[SecondaryDiagnosisCode13] [varchar](50) NULL,
	[PrimaryProcedureCode] [varchar](50) NULL,
	[PrimaryProcedureDate] [smalldatetime] NULL,
	[ClinicalCodingCompleteDate] [smalldatetime] NULL,
	[ClinicalCodingCompleteDateID] [int] NULL,
	[AdmissionTypeCode] [varchar](3) NULL,
	[NationalAdmissionSourceCode] [varchar](20) NULL,
	[NationalDischargeMethodCode] [varchar](20) NULL,
	[NationalDischargeDestinationCode] [varchar](20) NULL,
	[NationalPatientClassificationCode] [varchar](20) NULL,
	[NationalIntendedManagementCode] [varchar](20) NULL,
	[EddCreatedTime] [datetime] NULL,
	[ExpectedDateofDischarge] [smalldatetime] NULL,
	[EligibleForDischargeLetter] [bit] NULL,
	[DischargeLetterSignedTime] [datetime] NULL,
	[DischargeLetterSignedByCode] [varchar](255) NULL,
	[VTECategoryCode] [varchar](1) NULL,
	[AppointmentDateID] [int] NULL,
	[AppointmentDate] [datetime] NULL,
	[NationalAttendanceStatusCode] [varchar](20) NULL,
	[NationalFirstAttendanceCode] [varchar](20) NULL,
	[PatientCategoryCode] [varchar](20) NULL,
	[AppointmentOutcomeCode] [varchar](10) NULL,
	[NationalSourceOfReferralCode] [varchar](10) NULL,
	[WardStayStartDate] [datetime] NULL,
	[WardStayStartDateID] [int] NULL,
	[ContactDate] [datetime] NULL,
	[ContactDateID] [int] NULL,
	[ReferralReasonCode] [varchar](50) NULL,
	[NHSNumber] [varchar](50) NULL,
	[Postcode] [varchar](50) NULL,
	[RegisteredGPPracticeCode] [varchar](50) NULL,
	[CommissionerCode] [varchar](50) NULL,
	[SexCode] [varchar](50) NULL,
	[StaffTeamCode] [varchar](50) NULL,
	[IncidentDate] [datetime] NULL,
	[IncidentDateID] [int] NULL,
	[IncidentGradeCode] [varchar](20) NULL,
	[IncidentCauseGroupCode] [varchar](20) NULL,
	[IncidentCause1Code] [varchar](20) NULL,
	[IncidentTypeCode] [varchar](20) NULL,
	[IncidentTypeID] [int] NULL,
	[IncidentInvolvingChild] [bit] NULL,
	[IncidentInvolvingAdult] [bit] NULL,
	[IncidentInvolvingElderly] [bit] NULL,
	[IncidentSeverityCode] [varchar](20) NULL,
	[IncidentSeverityID] [int] NULL,
	[QCRAuditDate] [datetime] NULL,
	[QCRAuditDateID] [int] NULL,
	[QCRQuestionCode] [varchar](20) NULL,
	[QCRCustomListCode] [varchar](20) NULL,
	[QCRAnswer] [int] NULL,
	[HarmFreeCare] [bit] NULL,
	[GestationWeeks] [int] NULL,
	[BirthWeight] [float] NULL,
	[NeonatalReferralType] [varchar](100) NULL,
	[ROPScreenDueDate] [datetime] NULL,
	[ROPScreenDate] [datetime] NULL,
	[AdmissionTemperature] [int] NULL,
	[DischargeDestinationCode] [varchar](100) NULL,
	[DaysCooled] [int] NULL,
	[IntensiveCareDays] [int] NULL,
	[Perineum] [varchar](20) NULL,
	[BirthOrder] [varchar](20) NULL,
	[MethodDelivery] [varchar](20) NULL,
	[InfantDateOfBirth] [datetime] NULL,
	[InfantDateOfBirthID] [int] NULL,
	[BirthOutcome] [varchar](20) NULL,
	[WardStayEndActivityCode] [varchar](20) NULL,
	[Division] [varchar](100) NULL,
	[CensusDate] [datetime] NULL,
	[CensusDateID] [int] NULL,
	[ConsultationMediumTypeGroup] [varchar](100) NULL,
	[ConsultationMediumTypeGroupChannel] [varchar](100) NULL,
	[EncounterOutComeCode] [varchar](100) NULL,
	[WaitDays] [int] NULL,
	[ArrivalTime] [datetime] NULL,
	[DepartureTime] [datetime] NULL,
	[ArrivalDateID] [int] NULL,
	[MortalityReviewStatusCode] [varchar](100) NULL,
	[DiagnosisProcedureCode] [varchar](100) NULL,
	[ReferralID] [varchar](100) NULL,
	[PrimaryVisit] [bit] NULL,
	[ReferralSourceCode] [varchar](100) NULL,
	[DominantForDiagnosis] [bit] NULL,
	[Died] [bit] NULL,
	[RiskScore] [float] NULL,
	[HRGCode] [varchar](100) NULL,
	[TheatrePrimaryProcedureExists] [bit] NULL,
	[HRGWithCCCode] [varchar](100) NULL,
	[PatientClassificationCode] [varchar](100) NULL,
	[PalliativeCare] [bit] NULL,
	[LastDayOfQuarter] [date] NULL,
	[DateofDeath] [datetime] NULL,
	[ModalityCode] [nvarchar](256) NULL,
	[ModalitySettingCode] [nvarchar](256) NULL,
	[DialysisProviderCode] [nvarchar](100) NULL,
	[EventDetailCode] [nvarchar](256) NULL,
	[StartDate] [datetime] NULL,
	[StopDate] [datetime] NULL,
	[ReasonForChangeCode] [nvarchar](256) NULL,
	[VascularAccessCode] [nvarchar](256) NULL,
	[HCO3Num] [float] NULL,
	[PostVitalsSittingBloodPressureSystolic] [int] NULL,
	[PostVitalsSittingBloodPressureDiastolic] [int] NULL,
	[ActualRunTime] [float] NULL,
	[PreVitalsWeightInKg] [float] NULL,
	[PostVitalsWeightInKg] [float] NULL,
	[URRNum] [float] NULL,
	[UltrafiltrationVolume] [float] NULL,
	[HgBNum] [float] NULL,
	[FerritinNum] [float] NULL,
	[OnEPO] [int] NULL,
	[PreviousHDModalityExists] [int] NULL,
	[TimingQualifier] [int] NULL,
	[PhosNum] [float] NULL,
	[CaAdjNum] [float] NULL,
	[PTHIntactNum] [float] NULL,
	[TransplantListStatusCode] [nvarchar](256) NULL,
	[LastDayOfQuarterID] [int] NULL,
	[VascularAccessAt90DaysCode] [varchar](100) NULL,
	[StartDateID] [int] NULL,
	[IsPrimary] [bit] NULL,
	[CancerTypeCode] [varchar](100) NULL,
	[PriorityTypeCode] [varchar](100) NULL,
	[InappropriateReferralCode] [varchar](100) NULL,
	[TreatmentEventTypeCode] [varchar](100) NULL,
	[TumourStatusCode] [varchar](100) NULL,
	[HistologyCode] [varchar](100) NULL,
	[CancerSite] [varchar](100) NULL,
	[TreatmentSettingCode] [varchar](100) NULL,
	[DecisionToTreatAdjustmentReasonCode] [varchar](100) NULL,
	[TreatmentDate] [datetime] NULL,
	[TreatmentDateID] [int] NULL,
	[DecisionToTreatDate] [datetime] NULL,
	[InitialTreatmentCode] [varchar](100) NULL,
	[ReceiptOfReferralDate] [datetime] NULL,
	[PrimaryDiagnosisCode1] [varchar](100) NULL,
	[TreatmentNo] [int] NULL,
	[DiagnosisDate] [date] NULL,
	[SourceForOutpatientCode] [varchar](100) NULL,
	[FirstAppointmentAdjustmentReasonCode] [varchar](100) NULL,
	[CancelledAppointmentDate] [datetime] NULL,
	[FirstAppointmentWaitingTimeAdjusted] [int] NULL,
	[DecisionToTreatAdjustment] [int] NULL,
	[WaitingTimeAdjustment] [int] NULL,
	[AdjustmentReasonCode] [varchar](100) NULL,
	[ConsultantUpgradeDate] [datetime] NULL,
	[DiagnosisDateID] [int] NULL,
	[CasenoteNumber] [varchar](100) NULL,
	[AllocatedDate] [datetime] NULL,
	[AllocatedDateID] [int] NULL,
	[WithdrawnDate] [datetime] NULL,
	[EpisodeNo] [varchar](100) NULL,
	[ActivityDate] [datetime] NULL,
	[ActivityDateID] [int] NULL,
	[EncounterDateID] [int] NULL,
	[SourceUniqueID] [varchar](100) NULL,
	[PathwayStatusCode] [varchar](100) NULL,
	[AdjustedFlag] [varchar](100) NULL,
	[ArrivalModeCode] [varchar](100) NULL,
	[InitialAssessmentTime] [datetime] NULL,
	[AttendanceCategoryCode] [varchar](100) NULL,
	[AttendanceDisposalCode] [varchar](100) NULL,
	[StageCode] [varchar](100) NULL,
	[BreachStatusCode] [varchar](100) NULL,
	[DepartmentTypeCode] [varchar](100) NULL,
	[Duration] [int] NULL,
	[UnplannedReattend7Days] [bit] NULL,
	[LeftWithoutBeingSeen] [bit] NULL,
	[EncounterDate] [datetime] NULL,
	[CharlsonDiagnosisCodeCount] [int] NULL,
	[TCIDate] [datetime] NULL,
	[SessionDate] [datetime] NULL,
	[SessionDateID] [int] NULL,
	[CancelledFlag] [bit] NULL,
	[PlannedDuration] [int] NULL,
	[EarlyStart] [bit] NULL,
	[LateStart] [bit] NULL,
	[EarlyFinish] [bit] NULL,
	[LateFinish] [bit] NULL,
	[EarlyStartMinutes] [int] NULL,
	[LateStartMinutes] [int] NULL,
	[EarlyFinishMinutes] [int] NULL,
	[LateFinishMinutes] [int] NULL,
	[CancelReasonCode] [varchar](100) NULL,
	[TurnaroundTime] [int] NULL,
	[OperationDate] [datetime] NULL,
	[OperationDateID] [int] NULL,
	[AllTurnaroundsWithin15Minutes] [bit] NULL,
	[ServicePointID] [int] NULL,
	[PerformanceStatusCode] [varchar](100) NULL,
	[WasPatientDiscussedAtMDT] [char](1) NULL,
	[MDTDate] [datetime] NULL,
	[MDTDateID] [int] NULL,
	[StartServicePointID] [int] NULL,
	[IncidentDetails] [xml] NULL,
	[CategoryTypeCode] [varchar](50) NULL,
	[ReceiptDate] [datetime] NULL,
	[ReceiptDateID] [int] NULL,
	[AdvisedDate] [datetime] NULL,
	[AdvisedDateID] [int] NULL,
	[ReasonCode] [varchar](100) NULL,
	[RegistrationDate] [datetime] NULL,
	[RegistrationDateID] [int] NULL,
	[IsDuplicate] [bit] NULL,
	[ItemID] [int] NULL,
	[Numerator] [float] NULL,
	[Denominator] [float] NULL,
	[DateID] [int] NULL,
	[Date] [date] NULL,
	[FallDateID] [int] NULL,
	[SourceFallSeverityCode] [int] NULL,
	[Validated] [bit] NULL,
	[IdentifiedDateID] [int] NULL,
	[IdentifiedTime] [datetime] NULL,
	[SourcePressureUlcerCategoryCode] [int] NULL,
	[SymptomStartDateID] [int] NULL,
	[SymptomStartDate] [date] NULL,
	[UrinaryTestRequestedDateID] [int] NULL,
	[TreatmentStartDate] [date] NULL,
	[CatheterInsertedDateID] [int] NULL,
	[SourceCatheterTypeCode] [int] NULL,
	[MedicationRequired] [bit] NULL,
	[MedicationStartDateID] [int] NULL,
	[SourceVTETypeCode] [int] NULL,
	[AdmissionMethodCode] [nvarchar](20) NULL,
	[FinanceRiskRating] [int] NULL,
	[AppraisalRequired] [float] NULL,
	[AppraisalCompleted] [float] NULL,
	[ClinicalMandatoryTrainingRequired] [float] NULL,
	[ClinicalMandatoryTrainingCompleted] [float] NULL,
	[CorporateMandatoryTrainingRequired] [float] NULL,
	[CorporateMandatoryTrainingCompleted] [float] NULL,
	[RecentAppointment] [float] NULL,
	[LongTermAppointment] [float] NULL,
	[SicknessAbsenceFTE] [float] NULL,
	[SicknessEstablishmentFTE] [float] NULL,
	[Headcount] [float] NULL,
	[FTE] [float] NULL,
	[StartersHeadcount] [float] NULL,
	[StartersFTE] [float] NULL,
	[LeaversHeadcount] [float] NULL,
	[LeaversFTE] [float] NULL,
	[ClinicalBudgetWTE] [float] NULL,
	[ClinicalContractedWTE] [float] NULL,
	[CaseTypeCode] [varchar](50) NULL,
	[ConsentDate] [date] NULL,
	[ResolutionDate] [date] NULL,
	[SequenceNo] [int] NULL,
	[RegisteredNursePlan] [int] NULL,
	[RegisteredNurseActual] [int] NULL,
	[NonRegisteredNursePlan] [int] NULL,
	[NonRegisteredNurseActual] [int] NULL,
	[CreatedTime] [datetime] NULL,
	[ClosedTime] [datetime] NULL,
	[CreatedDateID] [int] NULL,
	[ReportableBreach] [bit] NULL,
	[CancellationDate] [datetime] NULL,
	[CancellationDateID] [int] NULL,
	[CancellationReason] [varchar](250) NULL,
	[Reopened] [bit] NULL,
	[SourceShift] [varchar](20) NULL,
	[FFTReturnType] [varchar](10) NULL,
	[ExtremelyLikeley] [int] NULL,
	[Likely] [int] NULL,
	[NeitherLikelyNorUnlikely] [int] NULL,
	[Unlikely] [int] NULL,
	[ExtremelyUnlikely] [int] NULL,
	[DontKnow] [int] NULL,
	[Responses] [int] NULL,
	[EligibleResponders] [int] NULL,
	[Complaint25DayTargetDateID] [int] NULL,
	[Complaint40DayTargetDateID] [int] NULL,
	[Complaint25DayTargetDate] [datetime] NULL,
	[Complaint40DayTargetDate] [datetime] NULL,
	[UtilisationMinutes] [int] NULL,
	[HasAssessmentUnitEpisode] [bit] NULL,
	[IsWellBabySpell] [bit] NULL,
	[GovernanceRiskRating] [int] NULL,
	[NewRegistration] [bit] NULL,
	[ExistingRegistration] [bit] NULL,
	[PatientSafety] [varchar](10) NULL,
	[FinanceActual] [float] NULL,
	[FinanceBudget] [float] NULL,
	[FinanceAnnualBudget] [float] NULL,
	[ACAgencySpend] [float] NULL,
	[CharlsonIndex] [int] NULL,
	[ResponseDate] [date] NULL,
	[HeadcountMonthly] [float] NULL,
	[FTEMonthly] [float] NULL,
	[StartersHeadcountMonthly] [float] NULL,
	[StartersFTEMonthly] [float] NULL,
	[LeaversHeadcountMonthly] [float] NULL,
	[LeaversFTEMonthly] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Index [IX_WD_NFAC]    Script Date: 25/08/2015 14:36:56 ******/
CREATE NONCLUSTERED INDEX [IX_WD_NFAC] ON [dbo].[WrkDataset]
(
	[NationalFirstAttendanceCode] ASC
)
INCLUDE ( 	[DatasetID],
	[DatasetRecno],
	[StartDirectorateID],
	[SpecialtyID],
	[ClinicianID],
	[StartSiteID],
	[AppointmentDateID],
	[NationalAttendanceStatusCode],
	[ServicePointID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_WD_PCCDD]    Script Date: 25/08/2015 14:36:56 ******/
CREATE NONCLUSTERED INDEX [IX_WD_PCCDD] ON [dbo].[WrkDataset]
(
	[PatientCategoryCode] ASC,
	[DischargeDate] ASC
)
INCLUDE ( 	[DatasetID],
	[DatasetRecno],
	[EndDirectorateID],
	[SpecialtyID],
	[ClinicianID],
	[EndSiteID],
	[AdmissionDate],
	[DischargeDateID],
	[NationalLastEpisodeInSpellCode],
	[StartServicePointID],
	[AdmissionMethodCode]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
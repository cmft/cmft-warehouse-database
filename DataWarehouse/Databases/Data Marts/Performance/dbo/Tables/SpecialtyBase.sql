﻿CREATE TABLE [dbo].[SpecialtyBase](
	[SourceContextCode] [varchar](20) NOT NULL,
	[SourceContext] [varchar](100) NOT NULL,
	[SourceSpecialtyID] [int] NOT NULL,
	[SourceSpecialtyCode] [varchar](100) NOT NULL,
	[SourceSpecialty] [varchar](900) NOT NULL,
	[SourceSpecialtyLabel] [varchar](1003) NOT NULL,
	[LocalSpecialtyID] [int] NULL,
	[LocalSpecialtyCode] [varchar](50) NULL,
	[LocalSpecialty] [varchar](200) NULL,
	[NationalSpecialtyID] [int] NULL,
	[NationalSpecialtyCode] [varchar](50) NULL,
	[NationalSpecialty] [varchar](200) NULL,
	[NationalSpecialtyLabel] [varchar](253) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](100) NULL,
 CONSTRAINT [PK__Specialt__31CA60B550B418C4] PRIMARY KEY CLUSTERED 
(
	[SourceSpecialtyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
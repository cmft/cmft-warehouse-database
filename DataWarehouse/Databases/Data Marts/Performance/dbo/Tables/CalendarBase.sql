﻿CREATE TABLE [dbo].[CalendarBase](
	[DateID] [int] NOT NULL,
	[TheDate] [date] NOT NULL,
	[DayOfWeek] [nvarchar](30) NULL,
	[DayOfWeekKey] [int] NULL,
	[LongDate] [varchar](20) NULL,
	[TheMonth] [nvarchar](34) NULL,
	[MonthName] [varchar](3) NULL,
	[FinancialQuarter] [nvarchar](70) NULL,
	[FinancialYear] [nvarchar](35) NULL,
	[FinancialMonthKey] [int] NULL,
	[FinancialQuarterKey] [int] NULL,
	[CalendarQuarter] [varchar](34) NULL,
	[CalendarSemester] [varchar](12) NOT NULL,
	[CalendarYear] [nvarchar](30) NULL,
	[WeekNoKey] [varchar](50) NULL,
	[WeekNo] [varchar](50) NULL,
	[FirstDayOfWeek] [date] NULL,
	[LastDayOfWeek] [date] NULL,
	[FullMonthName] [nvarchar](61) NULL,
 CONSTRAINT [PK__Calendar__A426F253299A4BA3] PRIMARY KEY CLUSTERED 
(
	[DateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[CentralPortalClinician](
	[NationalClinicianCode] [varchar](20) NOT NULL,
 CONSTRAINT [PK__CentralP__C9BDBD421222B4C5] PRIMARY KEY CLUSTERED 
(
	[NationalClinicianCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
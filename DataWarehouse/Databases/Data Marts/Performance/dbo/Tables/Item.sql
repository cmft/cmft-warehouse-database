﻿CREATE TABLE [dbo].[Item](
	[ItemID] [int] IDENTITY(1,1) NOT NULL,
	[Item] [varchar](255) NOT NULL,
	[ItemTypeID] [int] NOT NULL,
	[NumeratorID] [int] NULL,
	[DenominatorID] [int] NULL,
	[MeasureID] [int] NULL,
	[Reversed] [bit] NULL CONSTRAINT [DF_Reversed]  DEFAULT ((0)),
	[Active] [bit] NULL CONSTRAINT [DF_Active]  DEFAULT ((1)),
	[Factor] [int] NOT NULL CONSTRAINT [DF_Factor]  DEFAULT ((1)),
	[ReportMeasureID] [int] NULL DEFAULT ((1)),
	[Manual] [bit] NULL CONSTRAINT [DF__Item__Manual__133E942B]  DEFAULT ((0)),
	[Owner] [varchar](50) NULL,
	[ReportImageID] [int] NULL,
	[ReportColourID] [int] NULL,
	[ReportBackgroundColourID] [int] NULL,
	[ReportingPeriodID] [int] NULL,
	[ExecutiveDirectorOwnerID] [int] NULL,
	[CorporateDirectorOwnerID] [int] NULL,
	[CommitteeOwnerID] [int] NULL,
	[Description] [varchar](4000) NULL,
	[Source] [varchar](4000) NULL,
	[FurtherGuidance] [varchar](4000) NULL,
	[DefaultTargetTypeID] [int] NOT NULL CONSTRAINT [DF_Item_DefaultTargetTypeID]  DEFAULT ((1)),
	[ItemFormatID] [int] NULL,
	[DivisionCompliance] [bit] NULL,
	[ServicePointCompliance] [bit] NULL,
	[DefaultNumeratorValue] [float] NULL,
	[TargetTimeSeriesID] [int] NOT NULL CONSTRAINT [DF_Item_TargetTimeSeriesID]  DEFAULT ((1)),
	[ReportDrillthroughEnabled] [bit] NULL,
	[TargetEnabled] [bit] NULL,
	[LastTargetValueOverride] [bit] NULL,
	[AbsoluteAmberThreshold] [bit] NULL,
	[TargetVisible] [bit] NULL,
 CONSTRAINT [PK__EntityBa__9C892FFD3A81B327] PRIMARY KEY CLUSTERED 
(
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Denominator] FOREIGN KEY([DenominatorID])
REFERENCES [dbo].[Denominator] ([DenominatorID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Denominator]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Item] FOREIGN KEY([ItemID])
REFERENCES [dbo].[Item] ([ItemID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Item]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ItemFormat] FOREIGN KEY([ItemFormatID])
REFERENCES [dbo].[ItemFormat] ([ItemFormatID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ItemFormat]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ItemType] FOREIGN KEY([ItemTypeID])
REFERENCES [dbo].[ItemType] ([ItemTypeID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ItemType]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Measure] FOREIGN KEY([MeasureID])
REFERENCES [dbo].[Measure] ([MeasureID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Measure]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Numerator] FOREIGN KEY([NumeratorID])
REFERENCES [dbo].[Numerator] ([NumeratorID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Numerator]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Owner] FOREIGN KEY([CommitteeOwnerID])
REFERENCES [dbo].[Owner] ([OwnerID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Owner]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Owner1] FOREIGN KEY([CorporateDirectorOwnerID])
REFERENCES [dbo].[Owner] ([OwnerID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Owner1]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_Owner2] FOREIGN KEY([ExecutiveDirectorOwnerID])
REFERENCES [dbo].[Owner] ([OwnerID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_Owner2]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ReportColour] FOREIGN KEY([ReportColourID])
REFERENCES [dbo].[ReportColour] ([ReportColourID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ReportColour]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ReportColour1] FOREIGN KEY([ReportBackgroundColourID])
REFERENCES [dbo].[ReportColour] ([ReportColourID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ReportColour1]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ReportImage] FOREIGN KEY([ReportImageID])
REFERENCES [dbo].[ReportImage] ([ReportImageID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ReportImage]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ReportingPeriod] FOREIGN KEY([ReportingPeriodID])
REFERENCES [dbo].[ReportingPeriod] ([ReportingPeriodID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ReportingPeriod]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_ReportMeasure] FOREIGN KEY([ReportMeasureID])
REFERENCES [dbo].[ReportMeasure] ([ReportMeasureID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_ReportMeasure]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_TargetTimeSeries] FOREIGN KEY([TargetTimeSeriesID])
REFERENCES [dbo].[TargetTimeSeries] ([TargetTimeSeriesID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_TargetTimeSeries]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_Item_TargetType] FOREIGN KEY([DefaultTargetTypeID])
REFERENCES [dbo].[TargetType] ([TargetTypeID])
GO

ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_Item_TargetType]
﻿CREATE TABLE [dbo].[FactNumerator](
	[DatasetID] [int] NOT NULL,
	[DatasetRecno] [bigint] NOT NULL,
	[NumeratorID] [int] NOT NULL,
	[SiteID] [int] NOT NULL,
	[DirectorateID] [int] NOT NULL,
	[SpecialtyID] [int] NOT NULL,
	[ClinicianID] [int] NOT NULL,
	[DateID] [int] NOT NULL,
	[ServicePointID] [int] NOT NULL,
	[Value] [float] NULL
) ON [PRIMARY]
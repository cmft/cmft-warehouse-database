﻿CREATE TABLE [dbo].[ClinicianBase](
	[SourceClinicianID] [int] NOT NULL,
	[SourceClinicianCode] [varchar](100) NOT NULL,
	[SourceClinician] [varchar](900) NOT NULL,
	[SourceContext] [varchar](100) NOT NULL,
	[LocalClinicianID] [int] NULL,
	[LocalClinicianCode] [varchar](50) NULL,
	[LocalClinician] [varchar](200) NULL,
	[NationalClinicianID] [int] NULL,
	[NationalClinicianCode] [varchar](50) NULL,
	[NationalClinician] [varchar](953) NULL,
	[SourceContextCode] [varchar](20) NOT NULL,
	[ProviderCode] [varchar](20) NULL,
	[MainSpecialtyCode] [varchar](20) NULL,
	[MainSpecialty] [varchar](100) NULL,
	[Title] [varchar](80) NULL,
	[Initials] [varchar](6) NULL,
	[Forename] [varchar](30) NULL,
	[Surname] [varchar](50) NULL,
	[ClinicanType] [varchar](100) NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](100) NULL,
 CONSTRAINT [PK__Clinicia__78F301C02F5324F9] PRIMARY KEY CLUSTERED 
(
	[SourceClinicianID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[ServicePointBase](
	[SourceContextCode] [varchar](20) NULL,
	[SourceContext] [varchar](100) NULL,
	[SourceServicePointID] [int] NOT NULL,
	[SourceServicePointCode] [varchar](100) NULL,
	[SourceServicePoint] [varchar](900) NOT NULL,
	[LocalServicePointID] [int] NULL,
	[LocalServicePointCode] [varchar](50) NULL,
	[LocalServicePoint] [varchar](200) NULL,
	[NationalServicePointID] [int] NULL,
	[NationalServicePointCode] [varchar](50) NULL,
	[NationalServicePoint] [varchar](200) NULL,
	[ServicePointType] [varchar](100) NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](100) NULL,
 CONSTRAINT [PK__ServiceP__BAEDA94345426618] PRIMARY KEY CLUSTERED 
(
	[SourceServicePointID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
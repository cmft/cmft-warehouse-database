﻿CREATE TABLE [dbo].[TargetItemBase](
	[HierarchyID] [int] NOT NULL,
	[TargetID] [int] NOT NULL,
 CONSTRAINT [PK_HierarchyTarget] PRIMARY KEY CLUSTERED 
(
	[HierarchyID] ASC,
	[TargetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TargetItemBase]  WITH NOCHECK ADD  CONSTRAINT [FK_HierarchyTarget_Hierarchy] FOREIGN KEY([HierarchyID])
REFERENCES [dbo].[Hierarchy] ([HierarchyID])
GO

ALTER TABLE [dbo].[TargetItemBase] CHECK CONSTRAINT [FK_HierarchyTarget_Hierarchy]
GO
ALTER TABLE [dbo].[TargetItemBase]  WITH CHECK ADD  CONSTRAINT [FK_HierarchyTarget_Target] FOREIGN KEY([TargetID])
REFERENCES [dbo].[Target] ([TargetID])
GO

ALTER TABLE [dbo].[TargetItemBase] CHECK CONSTRAINT [FK_HierarchyTarget_Target]
﻿CREATE TABLE [dbo].[Hierarchy](
	[HierarchyID] [int] IDENTITY(1,1) NOT NULL,
	[ItemID] [int] NULL,
	[ParentID] [int] NULL,
	[Weight] [float] NULL CONSTRAINT [DF__Hierarchy__Weigh__257187A8]  DEFAULT ((1)),
	[SequenceNo] [int] NULL CONSTRAINT [DF_Hierarchy_Order]  DEFAULT ((1)),
	[Active] [bit] NULL DEFAULT ((1)),
	[ReportMeasureID] [int] NULL DEFAULT ((1)),
	[Highlight] [bit] NULL,
	[ReportImageID] [int] NULL,
	[ReportColourID] [int] NULL,
	[ReportBackgroundColourID] [int] NULL,
	[ReportPriority] [int] NULL,
	[ReportNavigation] [varchar](max) NULL,
	[ReportOrder] [int] NULL,
	[ReportColumns] [int] NULL,
	[AmberThresholdUpper] [float] NULL CONSTRAINT [DF_Hierarchy_AmberThresholdUpperPercent]  DEFAULT ((1)),
	[AmberThresholdLower] [float] NULL CONSTRAINT [DF_Hierarchy_AmberThresholdLowerPercent]  DEFAULT ((0.95)),
PRIMARY KEY CLUSTERED 
(
	[HierarchyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
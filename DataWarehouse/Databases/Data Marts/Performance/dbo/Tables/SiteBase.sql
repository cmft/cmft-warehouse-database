﻿CREATE TABLE [dbo].[SiteBase](
	[SourceContextCode] [varchar](20) NOT NULL,
	[SourceContext] [varchar](100) NOT NULL,
	[SourceSiteID] [int] NOT NULL,
	[SourceSiteCode] [varchar](100) NOT NULL,
	[SourceSite] [varchar](900) NOT NULL,
	[LocalSiteID] [int] NULL,
	[LocalSiteCode] [varchar](50) NULL,
	[LocalSite] [varchar](200) NULL,
	[NationalSiteID] [int] NULL,
	[NationalSiteCode] [varchar](50) NULL,
	[NationalSite] [varchar](200) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](100) NULL,
 CONSTRAINT [PK__SiteBase__1FDE9D014A071B35] PRIMARY KEY CLUSTERED 
(
	[SourceSiteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[Denominator](
	[DenominatorID] [int] IDENTITY(1,1) NOT NULL,
	[Denominator] [varchar](4000) NULL,
	[DenominatorLogic] [varchar](100) NULL,
	[Active] [bit] NOT NULL CONSTRAINT [DF_Denominator_Active]  DEFAULT ((1)),
 CONSTRAINT [PK_DQ_Denominator_1] PRIMARY KEY CLUSTERED 
(
	[DenominatorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
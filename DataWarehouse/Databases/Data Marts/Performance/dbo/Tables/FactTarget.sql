﻿CREATE TABLE [dbo].[FactTarget](
	[TargetID] [int] NOT NULL,
	[DateID] [int] NOT NULL,
	[Value] [float] NOT NULL
) ON [PRIMARY]
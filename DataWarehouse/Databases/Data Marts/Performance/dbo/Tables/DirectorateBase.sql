﻿CREATE TABLE [dbo].[DirectorateBase](
	[DirectorateID] [int] NOT NULL,
	[DirectorateCode] [varchar](5) NOT NULL,
	[Directorate] [varchar](50) NOT NULL,
	[Division] [varchar](100) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](100) NULL,
 CONSTRAINT [PK__Director__4E9D6EA238DC8F33] PRIMARY KEY CLUSTERED 
(
	[DirectorateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
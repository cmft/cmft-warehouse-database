﻿CREATE view [dbo].[FactNarrative] as

select distinct
	 Narrative.NarrativeID
	,Calendar.FinancialMonthKey
	,ItemHierarchy.HierarchyID
from
	dbo.Narrative

inner join dbo.ItemHierarchy
on	ItemHierarchy.ItemID = Narrative.ItemID

inner join dbo.Calendar
on	Calendar.TheDate between
	Narrative.NarrativeFromDate
and
	coalesce(
		(
		select
			min(NextNarrative.NarrativeFromDate)
		from
			dbo.Narrative NextNarrative
		where
			NextNarrative.ItemID = Narrative.ItemID
		and	NextNarrative.NarrativeTypeID = Narrative.NarrativeTypeID
		and	NextNarrative.NarrativeID <> Narrative.NarrativeID
		and	NextNarrative.NarrativeFromDate > Narrative.NarrativeFromDate
		and	NextNarrative.Active = 1
		)

		,(
		select
			dateadd(month, 1, LastCompleteMonth.TheDate)
		from
			dbo.Calendar LastCompleteMonth
		where
			LastCompleteMonth.LastCompleteMonth = 1
		)
	)

where
	Narrative.Active = 1
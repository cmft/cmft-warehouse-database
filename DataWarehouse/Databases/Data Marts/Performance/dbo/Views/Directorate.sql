﻿CREATE view [dbo].[Directorate]

as

select
	[DirectorateID]
	,[DirectorateCode]
	,[Directorate]
	,[Division]
from
	dbo.DirectorateBase
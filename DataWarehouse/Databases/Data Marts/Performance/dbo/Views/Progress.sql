﻿create view [dbo].[Progress]
as
select
	NarrativeID
	,ItemID
	,NarrativeTypeID
	,Narrative
	,Updated
	,ByWhom
from
	Performance.dbo.Narrative
where
	NarrativeTypeID = 4
and	not exists
			(
				select
					1
				from
					Performance.dbo.Narrative Later
				where
					Later.ItemID = Narrative.ItemID
				and Later.NarrativeTypeID = Narrative.NarrativeTypeID
				and	Later.Updated > Narrative.Updated
			)
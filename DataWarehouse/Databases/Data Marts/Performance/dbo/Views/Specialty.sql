﻿CREATE view [dbo].[Specialty]

as

select
	[SourceContextCode]
	,[SourceContext]
	,SourceSpecialtyID
	,SourceSpecialtyCode 
	,SourceSpecialty 
	,SourceSpecialtyLabel 
	,LocalSpecialtyID 
	,LocalSpecialtyCode 
	,LocalSpecialty
	,NationalSpecialtyID
	,NationalSpecialtyCode
	,NationalSpecialty
	,NationalSpecialtyLabel
from
	dbo.SpecialtyBase
﻿CREATE view [dbo].[Indicator]

as


select
	IndicatorID = Item.ItemID
	,Indicator = Item.Item
	--,Item.NumeratorID
	,Numerator = Numerator.Numerator + ' (Numerator ID: ' + cast(Item.NumeratorID as varchar) + ')'
	--,Item.DenominatorID
	,Denominator = coalesce(Denominator.Denominator  + ' (Denominator ID: ' + cast(Item.DenominatorID as varchar) + ')','Not Applicable')
	--,Item.MeasureID
	,Measure.Measure
	,IndicatorDirection = 
					case
					when Reversed = 0
					then 'Higher value represents better performance'
					else 'Lower value represents better performance'
					end
	,IndicatorStatus =
					case
					when Item.Active = 1
					then 'Active'
					else 'Inactive'
					end
	,Item.Factor
	--,ReportMeasureID
	,IndicatorDataSource = 
					case
					when Item.Manual = 1
					then 'Manually collated'
					else 'Data Warehouse'
					end
	,Owner = coalesce(Item.Owner, 'Not specified')
	,[Manual]
from
	Performance.dbo.Item

inner join dbo.Measure
on Item.MeasureID = Measure.MeasureID

inner join dbo.Numerator
on	Item.NumeratorID = Numerator.NumeratorID

left join dbo.Denominator
on	Item.DenominatorID = Denominator.DenominatorID


where
	ItemTypeID = 2
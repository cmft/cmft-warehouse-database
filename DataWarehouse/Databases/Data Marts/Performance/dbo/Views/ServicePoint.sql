﻿CREATE view [dbo].[ServicePoint]

as

select
	 SourceContextCode
	,SourceContext
	,SourceServicePointID
	,SourceServicePointCode
	,SourceServicePoint
	,LocalServicePointID
	,LocalServicePointCode
	,LocalServicePoint
	,NationalServicePointID
	,NationalServicePointCode
	,NationalServicePoint
	,ServicePointType
from
	dbo.ServicePointBase
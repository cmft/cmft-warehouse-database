﻿create view [dbo].[NumeratorItem] as

select distinct
	 ItemHierarchy.NumeratorID
	,ItemHierarchy.HierarchyID
from
	dbo.ItemHierarchy
where
	NumeratorID is not null
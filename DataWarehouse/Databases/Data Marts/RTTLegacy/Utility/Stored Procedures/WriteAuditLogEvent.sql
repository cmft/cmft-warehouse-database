﻿
CREATE   PROCEDURE [Utility].[WriteAuditLogEvent] 
	 @ProcessCode varchar(255)
	,@Event varchar(255)
	,@StartTime datetime = null
as
/******************************************************************************
**  Name: Utility.WriteAuditLogEvent
**  Purpose: Insert stored procedure run times to Audit table
**
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control
******************************************************************************/

INSERT INTO Utility.AuditLog 
	(
	 EventTime
	,UserId
	,ProcessCode
	,Event
	,StartTime
	)
VALUES 
	(
	 getdate()
	,Suser_SName()
	,@ProcessCode
	,@Event
	,@StartTime
	)


--insert any unknown process
insert into Utility.AuditProcess
(
	 ProcessCode
	,Process
	,ParentProcessCode
	,ProcessSourceCode
)
select
	 ProcessCode = @ProcessCode
	,Process = @ProcessCode
	,ParentProcessCode = 'OTH'
	,ProcessSourceCode = 'OTH'
where
	not exists
	(
	select
		1
	from
		Utility.AuditProcess
	where
		ProcessCode = @ProcessCode
	)





﻿

--execute [RPT].[ReturnSummary] '30 Sep 2013','N','N'

--execute [RPT].[ReturnSummary] '30 Sep 2013','Y','No'

--execute [RPT].[ReturnSummary] '30 Sep 2013','N','Yes'


CREATE procedure [RPT].[ReturnSummaryNHSEngland]
	(
	 @CensusDate smalldatetime
	,@AdjustedFlag varchar(1)
	,@AudiologyFlag varchar(1)
	)
			
as


create table #spec
			(SpecId  nvarchar(3)
			,name varchar(32)
			)

			insert into #spec
			(SpecId,Name)
			Values ('100','General Surgery')
			,('101','Urology')
			,('110','Trauma & Orthopaedics')
			,('120','Ear, Nose & Throat (ENT)')
			,('130','Ophthalmology')
			,('140','Oral Surgery')
			,('150','Neurosurgery')
			,('160','Plastic Surgery')
			,('170','Cardiothoracic Surgery')
			,('300','General Medicine')
			,('301','Gastroenterology')
			,('320','Cardiology')
			,('330','Dermatology')
			,('340','Thoracic Medicine')
			,('400','Neurology')
			,('410','Rheumatology')
			,('430','Geriatric Medicine')
			,('502','Gynaecology')
			,('999','Other');




with
EncounterCTE as
	(
	select
		 --Encounter.PCTCode

		PathwayStatusCode =
			case PathwayStatus.NationalPathwayStatusCode
				when 'AP' then
						case @AdjustedFlag
							when 'N' then 'IP Closed Unadjusted'
							when 'Y' then 'IP Closed adjusted'
						end
				when 'NP' then 'OP Closed Unajusted'
			else 'Open'
			end
		, RTTSpec = coalesce(ReturnSpecialtyGroup.spec , '999')
			
		,Encounter.WeekBandReturnCode
		,Encounter.Cases
		,Encounter.DaysWaiting
		,DirectAccessAudiologyFlag = case when RTTE.[type] like 'DAA%' then 'Y' else 'N' end
	from
		RTT.Fact Encounter

	left join RTT.Encounter RTTE
	on	RTTE.FactEncounterRecNo = Encounter.EncounterRecno
	and RTTE.Censusdate = Encounter.CensusDate
		

	left outer join dbo.Specs ReturnSpecialtyGroup
	on	ReturnSpecialtyGroup.spec = Encounter.NationalSpecialtyCode

	left join RTT.PathwayStatus
	on	PathwayStatus.PathwayStatusCode = Encounter.PathwayStatusCode

	where
		Encounter.CensusDate = @CensusDate
	and	Encounter.AdjustedFlag = @AdjustedFlag
	and Encounter.ReportableFlag = '1'
	and Encounter.PCTCode <> 'NONC'
 
  

	--and	SpecialtyCode = 'GUM'
	) 
	
	

----deubug
--select
--	*
--from
--	EncounterCTE


Select * 

From


(



select
	 --PCTCode
	PathwayStatusCode
	,RTTSpec
	,name
		--,TotalCases = HoldCases
		--,TotalUnder18
	
	,[0 Weeks]
	,[1 Week]
	,[2 Weeks]
	,[3 Weeks]
	,[4 Weeks]
	,[5 Weeks]
	,[6 Weeks]
	,[7 Weeks]
	,[8 Weeks]
	,[9 Weeks]
	,[10 Weeks]
	,[11 Weeks]
	,[12 Weeks]
	,[13 Weeks]
	,[14 Weeks]
	,[15 Weeks]
	,[16 Weeks]
	,[17 Weeks]
	,[18 Weeks]
	,[19 Weeks]
	,[20 Weeks]
	,[21 Weeks]
	,[22 Weeks]
	,[23 Weeks]
	,[24 Weeks]
	,[25 Weeks]
	,[26 Weeks]
	,[27 Weeks]
	,[28 Weeks]
	,[29 Weeks]
	,[30 Weeks]
	,[31 Weeks]
	,[32 Weeks]
	,[33 Weeks]
	,[34 Weeks]
	,[35 Weeks]
	,[36 Weeks]
	,[37 Weeks]
	,[38 Weeks]
	,[39 Weeks]
	,[40 Weeks]
	,[41 Weeks]
	,[42 Weeks]
	,[43 Weeks]
	,[44 Weeks]
	,[45 Weeks]
	,[46 Weeks]
	,[47 Weeks]
	,[48 Weeks]
	,[49 Weeks]
	,[50 Weeks]
	,[51 Weeks]
	,[52+ Weeks]
	,[Unknown Clock Start Date]
from
	(
	select
		 --PCTCode = ltrim(rtrim(PCTCode))
		 
		PathwayStatusCode
		,RTTSpec
		,name
		,DurationWeek
		--,TotalUnder18 
		--,HoldCases
		,Cases
		
	from
		(
		select
			 --EncounterCTE.PCTCode
			
			EncounterCTE.PathwayStatusCode
			,RTTSpec
			,name
			,DurationWeeks.DurationWeek
			--,TotalUnder18 = sum(case when [DaysWaiting] <127 then 1 else 0 end)
			,Cases = sum(EncounterCTE.Cases)
			--,HoldCases = sum(EncounterCTE.Cases)
		from
			EncounterCTE

		right join RTT.DurationWeeks
		on	DurationWeeks.WaitDurationCode = EncounterCTE.WeekBandReturnCode
		
		right outer join #spec
			on
		#spec.specId = EncounterCTE.RTTSpec

		where
			DirectAccessAudiologyFlag = @AudiologyFlag

		group by
			 --EncounterCTE.PCTCode
			 EncounterCTE.PathwayStatusCode
			
			 ,RTTSpec
			,name
		
			
			,DurationWeeks.DurationWeek
		) Encounter
	) SourceTable
pivot
	(
	sum(Cases)
	for
		DurationWeek in (
			 [0 Weeks]
			,[1 Week]
			,[2 Weeks]
			,[3 Weeks]
			,[4 Weeks]
			,[5 Weeks]
			,[6 Weeks]
			,[7 Weeks]
			,[8 Weeks]
			,[9 Weeks]
			,[10 Weeks]
			,[11 Weeks]
			,[12 Weeks]
			,[13 Weeks]
			,[14 Weeks]
			,[15 Weeks]
			,[16 Weeks]
			,[17 Weeks]
			,[18 Weeks]
			,[19 Weeks]
			,[20 Weeks]
			,[21 Weeks]
			,[22 Weeks]
			,[23 Weeks]
			,[24 Weeks]
			,[25 Weeks]
			,[26 Weeks]
			,[27 Weeks]
			,[28 Weeks]
			,[29 Weeks]
			,[30 Weeks]
			,[31 Weeks]
			,[32 Weeks]
			,[33 Weeks]
			,[34 Weeks]
			,[35 Weeks]
			,[36 Weeks]
			,[37 Weeks]
			,[38 Weeks]
			,[39 Weeks]
			,[40 Weeks]
			,[41 Weeks]
			,[42 Weeks]
			,[43 Weeks]
			,[44 Weeks]
			,[45 Weeks]
			,[46 Weeks]
			,[47 Weeks]
			,[48 Weeks]
			,[49 Weeks]
			,[50 Weeks]
			,[51 Weeks]
			,[52+ Weeks]
			,[Unknown Clock Start Date]
		)
	) AS PivotTable
	
	) alldata
	
	order by PathwayStatusCode,RTTSpec
	
	



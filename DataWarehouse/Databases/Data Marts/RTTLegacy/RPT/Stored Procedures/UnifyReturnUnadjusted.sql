﻿
create procedure [RPT].[UnifyReturnUnadjusted]
(
	@CensusDate date
)
as
/******************************************************************************
**  Name:		RPT.UnifyReturnUnadjusted
**  Purpose:	Unadjusted Unify Return
**
**	Input Parameters
**		Census Date
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		--------	---------------------------------------------------- 
** 2012-10-18   MH          Brought into source control
******************************************************************************/
-- Admitted Complete

SELECT 
	 provcomm = '1'
	,prov_org = 'RW3'

	,PCTr =
		case
		when left(Pathway.pct_new , 1) in ('7' , '6' , 'N' , 'S' , '9' , '' , 'Z' , 'Q') then 'NONC'
		else Pathway.pct_new
		end

	,YGCONCAT =
		case
		when Specs.spec is null or Pathway.path_division = 'Childrens' then 'Part_1A_X01'
		else 'Part_1A_C_' + Specs.spec
		end

	,XG1 = 'XG1'

	,[Weeks Wait] =
		case
		when datediff(day , Pathway.pathway_start_date_current , Pathway.pathway_end_date) >= 365 then 52
		else
			case
			when (datediff(day , Pathway.pathway_start_date_current , Pathway.pathway_end_date) -1) < 0 then 0
			else (datediff(day , Pathway.pathway_start_date_current , Pathway.pathway_end_date) -1) / 7
			end
		end

	,Pathway.InternalNo
	,Pathway.EpisodeNo 
	,Specs.specs
	,Pathway.Hospital
	,Cases = 1
FROM 
	ETL.Specs 

RIGHT JOIN ETL.WkPathwayReturn00 Pathway 
ON	Specs.spec = Pathway.path_doh_spec

WHERE 
	Pathway.type = 'CLOSED'
AND Pathway.treattype = 'IP'
AND Pathway.pathway_start_date_original >= '1 Jan 2007'

AND Pathway.pathway_end_date between '1 Jul 2012' and '31 Jul 2012'

AND coalesce(Pathway.exc , '') = ''
and	coalesce(Pathway.diag , '') in ('' , 'N' , 'I')
AND Pathway.path_doh_spec not in ('960' , '650' , '950')


--GROUP BY '1', 'RW3', IIf(Left([pct_new],1) In ('7','6','N','S','9','','Z','Q'),'NONC',[pct_new]), IIf(([spec] Is Null Or [path_division]='Childrens'),'Part_1A_X01','Part_1A_C_' & [spec]), 'XG1', IIf(DateDiff('d',[pathway_start_date_current],[pathway_end_date])>=365,52,IIf(DateDiff('d',[pathway_start_date_current],[pathway_end_date])-1<0,0,Int((DateDiff('d',[pathway_start_date_current],[pathway_end_date])-1)/7))), PATHWAYS_all.InternalNo, PATHWAYS_all.EpisodeNo;

union all


-- Incomplete
--currently adjusted - needs to be unadjusted

SELECT 
	 provcomm = '2'
	,prov_org = 'RW3'

	,PCTr =
		case
		when left(Pathway.pct_new , 1) in ('7' , '6' , 'N' , 'S' , '9' , '' , 'Z' , 'Q') then 'NONC'
		else Pathway.pct_new
		end

	,YGCONCAT =
		case
		when Specs.spec is null or Pathway.path_division = 'Childrens' then 'Part_2_X01'
		else 'Part_2_C_' + Specs.spec
		end

	,XG1 = 'XG1'

	,[Weeks Wait] =
		case
		when path_open_days_DNA_adjs < 365 then
			case
			when (path_open_days_DNA_adjs -1) < 0 then 0
			else (path_open_days_DNA_adjs -1) / 7
			end
		else 52
		end

	,Pathway.InternalNo
	,Pathway.EpisodeNo 
	,Specs.specs
	,Pathway.Hospital
	,Cases = 1

FROM
	ETL.Specs

RIGHT JOIN ETL.WkPathwayReturn00 Pathway 
ON Specs.spec = Pathway.path_doh_spec

WHERE 
	(
		coalesce(Pathway.exc , '') = ''
	and	coalesce(Pathway.diag , '') in ('' , 'N' , 'I')
	AND Pathway.path_doh_spec not in ('960' , '650' , '950')
	)
and
	(
		(
				Pathway.type = 'OPEN'
			And Pathway.pathway_start_date_current between '1 Jan 2007' and @CensusDate
		)
	or
		(
			Pathway.type = 'CLOSED'
		AND Pathway.pathway_start_date_original >= '1 Jan 2007'

		AND Pathway.pathway_end_date > @CensusDate
		and	Pathway.pathway_start_date_current <= @CensusDate
		)
	)

--GROUP BY '2', 'RW3', IIf(Left([pct_new],1) In ('7','6','N','S','9','','Z','Y','Q','4','1'),'NONC',[pct_new]), IIf(([spec] Is Null Or [path_division]='Childrens'),'Part_2_X01','Part_2_C_' & [spec]), 'XG1', IIf([path_open_days_DNA_adjs]<365,IIf(([path_open_days_DNA_adjs]-1)<0,0,Int((([path_open_days_DNA_adjs]-1))/7)),52), PATHWAYS_all.InternalNo, PATHWAYS_all.EpisodeNo;


union all

-- Non-Admitted Complete
-- should just be only the DNAs that are adjusted.

SELECT 

	 provcomm = '2'
	,prov_org = 'RW3'

	,PCTr =
		case
		when left(Pathway.pct_new , 1) in ('7' , '6' , 'N' , 'S' , '9' , '' , 'Z' , 'Q') then 'NONC'
		else Pathway.pct_new
		end

	,YGCONCAT =
		case
		when Specs.spec is null or Pathway.path_division = 'Childrens' then 'Part_2_X01'
		else 'Part_2_C_' + Specs.spec
		end

	,XG1 = 'XG1'

	,[Weeks Wait] =
		case
		when path_closed_days_DNA_adjs < 365 then
			case
			when path_closed_days_DNA_adjs -1 < 0 then 0
			else (path_closed_days_DNA_adjs -1) / 7
			end
		else 52
		end

	,Pathway.InternalNo
	,Pathway.EpisodeNo 
	,Specs.specs
	,Pathway.Hospital
	,Cases = 1
FROM
	ETL.Specs

RIGHT JOIN ETL.WkPathwayReturn00 Pathway 
ON Specs.spec = Pathway.path_doh_spec

WHERE 
	Pathway.type='CLOSED'
AND Pathway.pathway_start_date_original >= '1 Jan 2007'
And Pathway.pathway_end_date between '1 Jul 2012' and '31 Jul 2012'
AND Pathway.treattype = 'NON IP'
AND coalesce(Pathway.exc , '') = ''
and	coalesce(Pathway.diag , '') in ('' , 'N' , 'I')
AND Pathway.path_doh_spec not in ('960' , '650' , '950')


--GROUP BY '2', 'RW3', IIf(Left([pct_new],1) In ('7','6','N','S','9','','Z','Y','Q'),'NONC',[pct_new]), IIf(([spec] Is Null Or [path_division]='Childrens'),'Part_1B_X01','Part_1B_C_' & [spec]), 'XG1', IIf(([path_closed_days_DNA_adjs]-Nz([daysrem])-nz([sus_days_pat]))<365,IIf(([path_closed_days_DNA_adjs]-1)<0,0,Int(([path_closed_days_DNA_adjs]-Nz([daysrem])-nz([sus_days_pat])-1)/7)),52), PATHWAYS_all.InternalNo, PATHWAYS_all.EpisodeNo, Specs.specs;


--plus GUM

union all

--INSERT INTO tblNadmClosed ( provcomm, prov_org, PCTr, YGCONCAT, XG1, 0 )
SELECT
	 provcomm = '2'
	,prov_org = 'RW3'

	,PCTr =
		case
		when left(GUM.[PCT Code] , 1) in ('7' , '6' , 'N' , 'S' , '9' , '' , 'Z' , 'Q') then 'NONC'
		else GUM.[PCT Code]  
		end

	,YGCONCAT = 'Part_1B_X01'
	,XG1 = 'XG1'
	,[Weeks Wait] = 0
	,InternalNo = null
	,EpisodeNo = null
	,specs = 'GUM'
	,Hospital = 'MRI'
	,Cases = GUM.[GU FIRST ATTENDANCES TOTAL]
FROM
	ETL.GUM
--GROUP BY '2', 'RW3', IIf(Left([ComOrgCode],1) In ('7','6','N','S','9','','Z','Y','Q'),'NONC',[ComOrgCode]), 'Part_1B_X01', 'XG1', GUMAMM.[GU FIRST ATTENDANCES TOTAL];



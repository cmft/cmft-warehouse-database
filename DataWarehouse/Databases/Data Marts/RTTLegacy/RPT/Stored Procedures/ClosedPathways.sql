﻿
CREATE procedure [RPT].[ClosedPathways]
(
	 @CensusDate smalldatetime = NULL
	,@AdjustedFlag varchar(1) = 'N'
	,@Division varchar(max) = 'All'
)

as
/******************************************************************************
**  Name: RPT.ClosedPathways
**  Purpose: Provides a recordset for the RTT Closed Pathways report
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
**            GS          Created
** 07.09.12   CCB         Refactored
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
******************************************************************************/

--declare @Censusdate smalldatetime = '21 aug 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Division varchar(8) = 'All'

declare @localCensusDate smalldatetime = coalesce(@CensusDate, (select max(Censusdate) from RTT.Encounter))


select
	 Division 
	,Specialty
	,ReturnSpecialtyGroup
	,ReportableFlag
	,Admitted = sum(Admitted)
	,AdmittedOver18Weeks = sum(AdmittedOver18Weeks)
	,AdmittedBreached1Week = sum(AdmittedBreached1Week)
	,AdmittedBreachedOver1Week = sum(AdmittedBreachedOver1Week)
	,Admitted52Weeks = sum(Admitted52Weeks)
	,NotAdmitted = sum(NotAdmitted)
	,NotAdmittedOver18Weeks = sum(NotAdmittedOver18Weeks)
	,NotAdmittedBreached1Week = sum(NotAdmittedBreached1Week)
	,NotAdmittedBreachedOver1Week = sum(NotAdmittedBreachedOver1Week)
	,NotAdmitted52Weeks = sum(NotAdmitted52Weeks)

from
(
Select
	 Division = coalesce(Encounter.path_division,'Not Known')
	,Specialty = 
		dbo.[f_ProperCase](coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN'))

	,ReturnSpecialtyGroup =
		case
		when ReturnSpecialtyGroup.Spec IS NULL OR Encounter.path_division = 'Childrens' THEN '999 - Other' 
		else  coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN')
		end
	,ReportableFlag
	,Admitted = 
		case 
		when PathwayStatusCode = 'ACS' then Cases 
		else 0 
		end

	,AdmittedOver18Weeks = 
		case 
		when PathwayStatusCode = 'ACS' 
		and left(BreachBandCode,2) = 'BR' then Cases 
		else 0 
		end

	,AdmittedBreached1Week = 
		case 
		when PathwayStatusCode = 'ACS' 
		and BreachBandCode = 'BR01' then Cases 
		else 0 
		end

	,AdmittedBreachedOver1Week = 
		case 
		when PathwayStatusCode = 'ACS' 
		and BreachBandCode = 'BR02' then Cases 
		else 0 
		end

	,Admitted52Weeks = 
		case 
		when PathwayStatusCode = 'ACS' 
		and WeekBandCode = 52 then Cases 
		else 0 
		end

	,NotAdmitted = 
		case 
		when PathwayStatusCode = 'OPT' then Cases 
		else 0 
		end

	,NotAdmittedOver18Weeks = 
		case 
		when
				PathwayStatusCode = 'OPT' 
			and left(BreachBandCode,2) = 'BR' then Cases
		else 0 
		end

	,NotAdmittedBreached1Week = 
		case 
		when
				PathwayStatusCode = 'OPT' 
			and BreachBandCode = 'BR01' then Cases
		else 0 
		end

	,NotAdmittedBreachedOver1Week = 
		case 
		when
				PathwayStatusCode = 'OPT' 
			and BreachBandCode = 'BR02' then Cases
		else 0 
		end

	,NotAdmitted52Weeks = 
		case 
		when
				PathwayStatusCode = 'OPT' 
			and WeekBandCode = 52 then Cases
		else 0 
		end
from
	RTT.Encounter

left outer join rpt.Specialty
on	coalesce(encounter.path_spec, encounter.specialty) = Specialty.SpecialtyCode

left outer join dbo.Specs ReturnSpecialtyGroup
on	ReturnSpecialtyGroup.spec = Encounter.Specialty

where 
	Encounter.Censusdate =  @localCensusDate
and	(
		(
			encounter.adjustedFlag = @AdjustedFlag
		and encounter.PathwayStatusCode = 'ACS'
		)
	or	(
			encounter.adjustedFlag = 'N'
		and encounter.PathwayStatusCode  = 'OPT'
		)
	)
and (
		Encounter.path_division = @Division
	or	@Division ='All'
	)
--remove Direct Access Audiology
and	left(Encounter.type , 3) != 'DAA'
) Encounter

group by 
	 division 
	,Specialty
	,ReturnSpecialtyGroup
	,ReportableFlag



﻿
create procedure [RPT].[UnifyReturnAdjusted]
(
	@CensusDate date
)
as
/******************************************************************************
**  Name:		RPT.UnifyReturnAdjusted
**  Purpose:	Adjusted Unify Return
**
**	Input Parameters
**		Census Date
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		--------	---------------------------------------------------- 
** 2012-10-18   MH          Brought into source control
******************************************************************************/

--plus separate admitted  - adjusted file

SELECT 
 provcomm = '1'
,prov_org = 'RW3'

	,PCTr =
		case
		when left(Pathway.pct_new , 1) in ('7' , '6' , 'N' , 'S' , '9' , '' , 'Z' , 'Q') then 'NONC'
		else Pathway.pct_new
		end

	,YGCONCAT =
		case
		when Specs.spec is null or Pathway.path_division = 'Childrens' then 'X01'
		else 'C_' + Specs.spec
		end

	,XG1 = 'XG1'

	,[Weeks Wait] =
		case
		when (datediff(day , Pathway.pathway_start_date_current , Pathway.pathway_end_date) -1) / 7 > 3
			then
			case
			when (path_closed_days_DNA_adjs - adjdays) >= 365 then 52
			else
				case
				when (path_closed_days_DNA_adjs - adjdays) < 0 then 0
				else (path_closed_days_DNA_adjs - adjdays) / 7
				end
			end
		else			
			case
			when (datediff(day , Pathway.pathway_start_date_current , Pathway.pathway_end_date) -1) < 0 then 0
			else (datediff(day , Pathway.pathway_start_date_current , Pathway.pathway_end_date) -1) / 7
			end

		end
		
	,Pathway.InternalNo
	,Pathway.EpisodeNo 
	,Specs.specs
	,Pathway.Hospital
	,Cases = 1

--INTO tblADJ_01

FROM 
	ETL.Specs 

RIGHT JOIN ETL.WkPathwayReturn00 Pathway 
ON	Specs.spec = Pathway.path_doh_spec


WHERE 
	Pathway.type = 'CLOSED'
AND Pathway.treattype = 'IP'
AND Pathway.pathway_start_date_original >= '1 Jan 2007'
AND Pathway.pathway_end_date between dateadd(month , -1 , dateadd(day , 1 , @CensusDate)) and @CensusDate
AND coalesce(Pathway.exc , '') = ''
and	coalesce(Pathway.diag , '') in ('' , 'N' , 'I')
AND Pathway.path_doh_spec not in ('960' , '650' , '950')


--GROUP BY '1', 'RW3', IIf(Left([pct_new],1) In ('7','6','N','S','9','','Z','Q'),'NONC',[pct_new]), IIf(([spec] Is Null Or [path_division]='Childrens'),'X01','C_' & [spec]), 'XG1', IIf(DateDiff('d',[pathway_start_date_current],[pathway_end_date])>=365,52,IIf(DateDiff('d',[pathway_start_date_current],[pathway_end_date])-1<0,0,Int((DateDiff('d',[pathway_start_date_current],[pathway_end_date])-1)/7))), PATHWAYS_all.InternalNo, PATHWAYS_all.EpisodeNo;


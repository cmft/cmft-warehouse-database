﻿
CREATE procedure [RPT].[OpenPathwayBookings]
(
	 @Censusdate smalldatetime = NULL
	,@AdjustedFlag varchar(1) = 'Y'
	,@Period varchar(8) = NULL
)

as

/******************************************************************************
**  Name: RPT.OpenPathwayBookings
**  Purpose: 
**
**	NOTE - Please maintain this stored procedure via the Visual Studio 2012 Solution
**         RTT Legacy VS2012
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 07.09.12   CCB         Created
** 17.10.12   MH          Brought into source control 
** 18.11.13   MH          Additional columns to report by breach dates within the next 1-7 days, >1 week, >= 52 weeks
**						  and include the Reporting specialty name
******************************************************************************/
declare @localCensusdate smalldatetime = coalesce(@Censusdate, (select max([rtt].[encounter].[censusdate]) from rtt.encounter))
declare @localPeriod  varchar(8) = coalesce(@Period, (select [wh].[Calendar].[TheMonth] from wh.Calendar where [wh].[Calendar].[thedate] = @localCensusdate))

--declare @Censusdate smalldatetime = '21 aug 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Period varchar(8) = 'Aug 2012'

select
	  Data.[division] 
	 ,Specialty							= dbo.f_ProperCase(Data.Specialty)
	 ,ReturnSpecialty					= dbo.f_ProperCase(Data.ReturnSpecialtyGroup)
	 ,ReportableFlag
	 ,OpenPathways						= sum(Data.[OpenPathways])
	 ,OpenPathwaysPtl					= sum(Data.[OpenPathwaysPtl])
	 ,OpenPathwaysStl					= sum(Data.[OpenPathwaysStl])
	 ,BookedInMonthPTLLessEqual18Weeks	= sum(Data.[BookedInMonthPTLLessEqual18Weeks])
	 ,BookedInMonthPTLGreater18Weeks	= sum(Data.[BookedInMonthPTLGreater18Weeks])
	 ,BookedInMonthStlLessEqual18weeks	= sum(Data.[BookedInMonthStlLessEqual18weeks])
	 ,BookedInMonthStlGreater18weeks	= sum(Data.[BookedInMonthStlGreater18weeks])

	 ,BookedInMonthStlLessEqual1week	= sum(Data.[BookedInMonthStlLessEqual1week])
	 ,BookedInMonthStlGreater1week		= sum(Data.[BookedInMonthStlGreater1week])
	 ,BookedInMonthStlGreater52weeks	= sum(Data.[BookedInMonthStlGreater52weeks])
	 ,BookedInMonth						= sum(Data.[BookedInMonth])

from
(
Select
	 division = coalesce(Encounter.path_division,'Not Known')
	,Specialty = coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN')
	,ReturnSpecialtyGroup =
		case
		when ReturnSpecialtyGroup.Spec IS NULL OR Encounter.path_division = 'Childrens' THEN '999 - Other' 
		else  coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN')
		end
	,ReportableFlag
	,OpenPathways = 
		case 
		when PathwayStatusCode = 'IPW' then 1 
		else 0 
		end

	,OpenPathwaysPtl = 
		case 
		when PathwayStatusCode = 'IPW'
			and calenderRTTBreachDate.TheMonth <> @localPeriod
			then 1 
		else 0 
		end

	,OpenPathwaysStl = 
		case 
		when PathwayStatusCode = 'IPW'
			and calenderRTTBreachDate.TheMonth = @localPeriod
			then 1 
		else 0 
		end

	,BookedInMonthPTLLessEqual18Weeks = 
		case 
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth <> @localPeriod
			and (
					Encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and encounter.TCIBeyond = 0
			then 1
		else 0
		end

	,BookedInMonthPTLGreater18Weeks =
		case 
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth <> @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and encounter.TCIBeyond = 1
			then 1
		else 0
		end

	,BookedInMonthStlLessEqual18weeks = 
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and TCIBeyond = 0
			then 1
		else 0
		end

	,BookedInMonthStlGreater18weeks = 
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and TCIBeyond = 1
			then 1
		else 0
		end

	,BookedInMonthStlLessEqual1week = 
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and DATEDIFF(DAY, encounter.RTTBreachDate, encounter.tci_date) BETWEEN 0 AND 6
		then 1
		else 0
		end

	,BookedInMonthStlGreater1week = 
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and DATEDIFF(DAY, encounter.RTTBreachDate, encounter.tci_date) >= 7
			then 1
		else 0
		end

	,BookedInMonthStlGreater52weeks = 
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and DATEDIFF(DAY, encounter.RTTBreachDate, encounter.tci_date) >= 365
		then 1
		else 0
		end

	,BookedInMonth = 
		case
		when
			calenderTciDate.TheMonth = @localPeriod
		and (
				encounter.WaitingListFlag =1
			or	(
					encounter.WaitingListFlag is null 
				and Hospital = 'Trafford'
				)
			)
		then 1
		else 0
		end
from
	rtt.encounter 

left outer join wh.Calendar calenderRTTBreachDate
	on calenderRTTBreachDate.thedate = convert(date,RTTBreachDate)

left outer join wh.Calendar calenderTciDate
	on calenderTciDate.thedate = convert(date,tci_date)

left outer join rpt.Specialty 
	on	coalesce(encounter.path_spec, encounter.specialty) = Specialty.SpecialtyCode

left outer join dbo.Specs ReturnSpecialtyGroup
	on	ReturnSpecialtyGroup.spec = encounter.Specialty

where 
	encounter.Censusdate =  @localCensusdate
and	(
		(
			encounter.adjustedFlag = @AdjustedFlag
		and encounter.PathwayStatusCode = 'ACS'
		)
	or	(
			encounter.adjustedFlag = 'N'
		and encounter.PathwayStatusCode  = 'IPW'
		)
	)
--remove Direct Access Audiology
and	left(Encounter.type , 3) != 'DAA'
) Data

group by 
	 Data.[division] 
	,Data.Specialty
	,Data.ReturnSpecialtyGroup
	,ReportableFlag


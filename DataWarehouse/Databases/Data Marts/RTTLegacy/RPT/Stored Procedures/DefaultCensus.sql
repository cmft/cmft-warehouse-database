﻿
CREATE procedure [RPT].[DefaultCensus]
(
	@LatestCensus bit
)

as
/******************************************************************************
**  Name: RPT.DefaultCensus
**  Purpose: Provides the default Census Date for the RTT Legacy Report suite
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 07.09.12   CCB         Created
** 17.10.12   MH          Brought into source control 
******************************************************************************/

declare
	 @maxCensusDate smalldatetime = --'16 Oct 2012'
		(
		select
			CensusDate = MAX(Censusdate)
		from
			RTT.Encounter
		)

	,@nextMonthEndDate smalldatetime = --'30 Sep 2012'
		(
		select
			DateValue
		from
			Utility.Parameter
		where
			Parameter = 'RTTNEXTMONTHENDDATE'
		)


select
	CensusDate =
		case @LatestCensus
		when 1 then @maxCensusDate
		else
			case
			when @maxCensusDate > @nextMonthEndDate --data not frozen
				then dateadd(day , -1 * datepart(day , @maxCensusDate) , @maxCensusDate)
			else @maxCensusDate -- data frozen
			end
		end

----if the maximum CensusDate is the last day of the month, use it, otherwise use the last day of the previous month
--		case
--		when datepart(month , dateadd(day , 1 , @maxCensusDate)) != datepart(month , @maxCensusDate) then @maxCensusDate
--		else dateadd(day , -1 * datepart(day , @maxCensusDate) , @maxCensusDate)
--		end

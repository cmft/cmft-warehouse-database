﻿
CREATE procedure [RPT].[OpenPathwayBookingsDrillThrough]
(
	 @Censusdate smalldatetime = NULL
	,@AdjustedFlag varchar(1) = 'Y'
	,@Period varchar(8) = NULL
	,@DrillthroughType varchar(40) 
	,@Division varchar(max) = null
	,@Specialty varchar(max) = null
	,@ReturnSpecialtyFlag char(1) = 'N'
)
as

/******************************************************************************
**  Name: RPT.OpenPathwayBookingsDrillThrough
**  Purpose: Drill through details for OpenPathwayBookings report
**
**	NOTE - Please maintain this stored procedure via the Visual Studio 2012 solution
**         RTT Legacy VS2012
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control 
** 25.11.13   MH          Additional filters to report by breach dates within the next 1-7 days, >1 week, >= 52 weeks
**						  and include the Reporting specialty name
******************************************************************************/

--declare @Censusdate smalldatetime = '21 aug 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Period varchar(8) = 'Aug 2012'
--declare @DrillthroughType varchar(40) = 'BookedInMonthPTLGreater18Weeks'


declare @localCensusdate smalldatetime = coalesce(@Censusdate, (select max(censusdate) from rtt.encounter))
declare @localPeriod  varchar(8) = coalesce(@Period, (select TheMonth from wh.Calendar where thedate = @localCensusdate))



Select
	 division = 
		coalesce(Encounter.path_division,'Not Known')

	,Specialty =
		dbo.[f_ProperCase](coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN'))

	,ReportableFlag

	,pathway_ID = 
		encounter.RTTPathwayID

	,encounter.AdjustedFlag
	,encounter.BreachBandCode
	,encounter.Casenote
	,encounter.Censusdate
	,encounter.Comment1
	,encounter.Comment2
	,Consultant = 
		coalesce(CMFTPASConsultant.consultant, encounter.Consultant,'') + ' (' + isnull(encounter.path_cons,'') + ')'

	,encounter.DecidedToAdmitDate
	,encounter.DiagnosticFlag
	,encounter.ReferralDate
	,encounter.WeekBandCode
	,encounter.DistrictNo
	,encounter.EpisodeNo
	,encounter.pathway_start_date_current
	,encounter.RTTBreachDate
	,encounter.Surname
	,encounter.WL_WLDate
	,encounter.tci_date
	,encounter.TCIBeyond
	,encounter.PauseStartDate
	,encounter.InternalNo
	,encounter.PathwayStatus
	,Encounter.DaysWaiting
from
	rtt.encounter encounter

left outer join wh.Calendar calenderRTTBreachDate
on calenderRTTBreachDate.thedate = convert(date,RTTBreachDate)

left outer join wh.Calendar calenderTciDate
on calenderTciDate.thedate = convert(date,tci_date)

left outer join rpt.Specialty
on	coalesce(encounter.path_spec, encounter.specialty) = Specialty.SpecialtyCode

left outer join dbo.Specs ReturnSpecialtyGroup
on	ReturnSpecialtyGroup.spec = encounter.Specialty

left outer join warehouse.pas.Consultant CMFTPASConsultant
on CMFTPASConsultant.ConsultantCode =  encounter.path_cons 

where 
	encounter.Censusdate =  @localCensusdate
--remove Direct Access Audiology
and	left(Encounter.type , 3) != 'DAA'
and	(
		(
			encounter.adjustedFlag = @AdjustedFlag
		and encounter.PathwayStatusCode = 'ACS'
		)
	or	(
			encounter.adjustedFlag = 'N'
		and encounter.PathwayStatusCode  = 'IPW'
		)
	)
and	
	(
		coalesce(Encounter.path_division,'Not Known') = @Division 
	or
		@Division is null
	)

and	
	(
		CASE WHEN @ReturnSpecialtyFlag = 'Y' 
				THEN 	case
							when ReturnSpecialtyGroup.Spec IS NULL OR Encounter.path_division = 'Childrens' THEN '999 - Other' 
							else  coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN')
						end
				ELSE Specialty.NationalSpecialty
		END = @Specialty
	or
		@Specialty is null
	)

and 
(
	(
	@DrillthroughType = 'OpenPathwaysTotal'
	and case 
		when PathwayStatusCode = 'IPW' then 1 
		else 0 
		end = 1
	)
or
	(
	@DrillthroughType = 'OpenPathwaysPTL'
	and case 
		when PathwayStatusCode = 'IPW'
			and calenderRTTBreachDate.TheMonth <> @localPeriod
			then 1 
		else 0 
		end = 1
	)
or
	(
	@DrillthroughType = 'OpenPathwaysSTL'
	and case 
		when PathwayStatusCode = 'IPW'
			and calenderRTTBreachDate.TheMonth = @localPeriod
			then 1 
		else 0 
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthPTLLessEqual18Weeks'
	and case 
		when calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth <> @localPeriod
			and 
			(
				encounter.WaitingListFlag =1
			or
				(encounter.WaitingListFlag is null and Hospital = 'Trafford')
			)
		and encounter.TCIBeyond = 0
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthPTLGreater18Weeks'
	and 
		case 
		when calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth <> @localPeriod
			and 
			(
				encounter.WaitingListFlag =1
			or
				(encounter.WaitingListFlag is null and Hospital = 'Trafford')
			)
		and encounter.TCIBeyond = 1
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthStlLessEqual18weeks'
	and
		case
		when calenderTciDate.TheMonth = @localPeriod
		and calenderRTTBreachDate.TheMonth = @localPeriod
		and 
			(
				encounter.WaitingListFlag =1
			or
				(
				encounter.WaitingListFlag is null 
				and Hospital = 'Trafford'
				)
			)
		and TCIBeyond = 0
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthStlGreater18weeks'
	and
		case
		when calenderTciDate.TheMonth = @localPeriod
		and calenderRTTBreachDate.TheMonth = @localPeriod
		and 
			(
				encounter.WaitingListFlag =1
			or
				(
				encounter.WaitingListFlag is null 
				and Hospital = 'Trafford'
				)
			)
		and TCIBeyond = 1
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonth'
	and
		case
		when calenderTciDate.TheMonth = @localPeriod
		and 
			(
				encounter.WaitingListFlag =1
			or
				(
					encounter.WaitingListFlag is null 
				and Hospital = 'Trafford'
				)
			)
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthStlLessEqual1week'
	and
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and DATEDIFF(DAY, encounter.RTTBreachDate, encounter.tci_date) BETWEEN 0 AND 6
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthStlGreater1week'
	and
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and DATEDIFF(DAY, encounter.RTTBreachDate, encounter.tci_date) >= 7
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthStlGreater52weeks'
	and
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and DATEDIFF(DAY, encounter.RTTBreachDate, encounter.tci_date) >= 365
		then 1
		else 0
		end = 1
	)
or
	(
	@DrillthroughType = 'BookedInMonthStlTotal'
	and
		case
		when
				calenderTciDate.TheMonth = @localPeriod
			and calenderRTTBreachDate.TheMonth = @localPeriod
			and (
					encounter.WaitingListFlag =1
				or	(
						encounter.WaitingListFlag is null 
					and Hospital = 'Trafford'
					)
				)
			and DATEDIFF(DAY, encounter.RTTBreachDate, encounter.tci_date) >= 0
		then 1
		else 0
		end = 1
	)

)


	
order by
	 Encounter.WeekBandCode desc
	,pathway_start_date_current
	,PathwayStatus
	,Specialty
	,Consultant
	,tci_date
	,Surname

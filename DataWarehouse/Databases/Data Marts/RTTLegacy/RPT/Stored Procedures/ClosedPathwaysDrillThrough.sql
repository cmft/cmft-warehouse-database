﻿
/******************************************************************************
**  Name: RPT.ClosedPathwaysDrillThrough
**  Purpose: Provides a drill through level for the RTT Closed Pathways report
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
******************************************************************************/
CREATE procedure [RPT].[ClosedPathwaysDrillThrough]
(
	 @Censusdate smalldatetime = NULL
	,@AdjustedFlag varchar(1) = 'Y'
	,@DrillthroughType varchar(max) 
	,@Division varchar(max) = 'All'
	,@Specialty varchar(max) = null
	,@ReturnSpecialtyGroup varchar(max) = null
)


as

--declare @Censusdate smalldatetime = '21 aug 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Period varchar(8) = 'Aug 2012'
--declare @DrillthroughType varchar(40) = 'BookedInMonthPTLGreater18Weeks'


declare @localCensusdate smalldatetime = coalesce(@Censusdate, (select max(censusdate) from rtt.encounter))


Select
	 division = 
		coalesce(Encounter.path_division,'Not Known')

	,Specialty = 
		dbo.[f_ProperCase](coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN'))
	
	,ReportableFlag
	
	,pathway_ID = 
		encounter.RTTPathwayID

	,encounter.AdjustedFlag
	,encounter.BreachBandCode
	,encounter.Casenote
	,encounter.Censusdate
	,encounter.Comment1
	,encounter.Comment2

	,Consultant = 
		coalesce(CMFTPASConsultant.consultant, encounter.Consultant,'') + ' (' + isnull(encounter.path_cons,'') + ')'

	,encounter.DecidedToAdmitDate
	,encounter.DiagnosticFlag
	,encounter.ReferralDate
	,encounter.WeekBandCode
	,encounter.DistrictNo
	,encounter.EpisodeNo
	,encounter.pathway_start_date_current
	,encounter.RTTBreachDate
	,encounter.Surname
	,encounter.WL_WLDate
	,encounter.tci_date
	,encounter.TCIBeyond
	,encounter.PauseStartDate
	,encounter.pathway_end_date
	,encounter.DaysWaiting
	,Encounter.PathwayStatus
	,Encounter.LastAttendanceStatusCode
	,Encounter.InternalNo
	,Encounter.RTTPathwayID
	,Encounter.Forename
from
	rtt.encounter encounter

left outer join wh.Calendar calenderRTTBreachDate
on calenderRTTBreachDate.thedate = convert(date,RTTBreachDate)

left outer join wh.Calendar calenderTciDate
on calenderTciDate.thedate = convert(date,tci_date)

left outer join rpt.Specialty
on	coalesce(encounter.path_spec, encounter.specialty) = Specialty.SpecialtyCode

left outer join dbo.Specs ReturnSpecialtyGroup
on encounter.Specialty = ReturnSpecialtyGroup.spec

left outer join warehouse.pas.Consultant CMFTPASConsultant
on CMFTPASConsultant.ConsultantCode =  encounter.path_cons 

where 
	encounter.Censusdate =  @localCensusdate
--remove Direct Access Audiology
and	left(Encounter.type , 3) != 'DAA'
and Encounter.AdjustedFlag =
		case
		when @DrillthroughType like 'Admitted%' then @AdjustedFlag
		else 'N'
		end 
and	(
		coalesce(Encounter.path_division,'Not Known') = @Division 
	or	@Division ='All'
	)

and	(
		coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN') = @Specialty
	or	@Specialty is null
	)
and	(
		case
		when ReturnSpecialtyGroup.Spec IS NULL OR Encounter.path_division = 'Childrens' THEN '999 - Other' 
		else  coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN')
		end = @ReturnSpecialtyGroup
	or	@ReturnSpecialtyGroup is null
	)
and (
		(
		@DrillthroughType = 'Admitted'
		and 
			case 
			when PathwayStatusCode = 'ACS' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'AdmittedOver18Weeks'
		and
			case 
			when PathwayStatusCode = 'ACS' 
			and left(BreachBandCode,2) = 'BR' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'Admitted52Weeks'
		and
			case 
			when PathwayStatusCode = 'ACS' 
			and WeekBandCode = 52 then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'AdmittedBreached1Week'
		and
			case 
			when PathwayStatusCode = 'ACS' 
			and BreachBandCode = 'BR01' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'AdmittedBreachedOver1Week'
		and
			case 
			when PathwayStatusCode = 'ACS' 
			and BreachBandCode = 'BR02' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'NotAdmitted'
		and
			case 
			when PathwayStatusCode = 'OPT' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'NotAdmittedOver18Weeks'
		and
			case 
			when PathwayStatusCode = 'OPT' 
			and left(BreachBandCode,2) = 'BR' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'NotAdmittedBreached1Week'
		and
			case 
			when PathwayStatusCode = 'OPT' 
			and BreachBandCode = 'BR01' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'NotAdmittedBreachedOver1Week'
		and
			case 
			when PathwayStatusCode = 'OPT' 
			and BreachBandCode = 'BR02' then 1 
			else 0 
			end = 1
		)
	or
		(
		@DrillthroughType = 'NotAdmitted52Weeks'
		and
			case 
			when PathwayStatusCode = 'OPT' 
			and WeekBandCode = 52 then 1 
			else 0 
			end = 1
		)
	)

		

	
order by
	 Encounter.WeekBandCode desc
	,pathway_start_date_current
	,PathwayStatus
	,Specialty
	,Consultant
	,tci_date
	,Surname

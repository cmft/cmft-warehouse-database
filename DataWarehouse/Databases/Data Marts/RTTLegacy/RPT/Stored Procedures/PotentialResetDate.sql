﻿/****** Script for SelectTopNRows command from SSMS  ******/
create procedure [RPT].[PotentialResetDate] @Division as varchar(20), @Specialty as nvarchar(40), @PathwayType as varchar(20), @BreachType as varchar(20)

as

	
SELECT *
,PotentialWait = DaysWaiting - datediff(day,[pathway_start_date_current],[actualresetdate])
  FROM [RTT].[PotentialPathwayResetDates] 
  

  
  where  (Division = @Division or @Division is null)
and ( specCode = @Specialty  or @Specialty is null)
and ( (case when PathwayStatusCode in  ('OPW','OPT') then 'NONIP' else 'IP' end ) = @PathwayType or @PathwayType is null)
and  (( Case when [DaysWaiting] > 126  then 'B' else 'NB' end ) = @BreachType or @BreachType is null)
		
﻿CREATE procedure [RPT].[ReturnsImportFile]
	(
	 @CensusDate smalldatetime
	,@AdjustedFlag varchar(1)
	,@AudiologyFlag varchar(1)
	)
as

/******************************************************************************
**  Name:		RPT.ReturnsImportFile
**  Purpose:	Provides data for the SSRS Report: RTT Returns Import File
**
**	Input Parameters
**		Census Date
**		Adjusted Flag - whether to return Adjusted or Unadjusted Data
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		--------	---------------------------------------------------- 
** 2012-09-13	CCB			Created
******************************************************************************/

--select
--	 SourceUniqueID = InternalNo + '||' + EpisodeNo
--	,DirectAccessAudiologyFlag =
--		case
--		when left(type , 3) = 'DAA' then 'Y'
--		else 'N'
--		end
--into
--	#Drill
--from
--	ETL.WkPathwayReturn00

--CREATE CLUSTERED INDEX IX_Drill ON #Drill
--	(
--	SourceUniqueID
--	)
--;
with
EncounterCTE as
	(
	select
		 case when  Encounter.[PCTCode] <> 'NONC' and  [NationalSpecialtyCode] = '140'
		 then 'X24' 
		 
		  when  Encounter.[PCTCode] like '7%'  or  Encounter.[PCTCode] like ('ZC%') then 'NONC'
		  
		 	 
		 else Encounter.PCTCode end PCTCode

		,PathwayStatusCode =
		
		Case when @AudiologyFlag = 'N'
					then Case when @AdjustedFlag = 'N' 
						then
			
							case PathwayStatus.NationalPathwayStatusCode
							when 'AP' then 'Part_1A'
							when 'NP' then 'Part_1B'
							else 'Part_2'
							end
							+
							case 
							when
									coalesce(ReturnSpecialtyGroup.spec , '999') = '999'
								
								then '_X01'
							else '_C_' + ReturnSpecialtyGroup.spec
							end
							
						else
							--case PathwayStatus.NationalPathwayStatusCode
							--when 'AP' then 'C_'
							--when 'NP' then 'NIP_'
							--else 'OPEN_'
							--end
							--+
							case 
							when
									coalesce(ReturnSpecialtyGroup.spec , '999') = '999'
								
								then 'X01'
							else 'C_' + ReturnSpecialtyGroup.spec
							end
							
							
							
							
					end	
			else	
			
						case PathwayStatus.NationalPathwayStatusCode
						when 'AP' then Null
						when 'NP' then '1AD01'
						else '2AD02'
						end
			end		
					
					
				
					
			
		,Encounter.WeekBandReturnCode
		,Encounter.Cases

		,DirectAccessAudiologyFlag = case when RTTE.[type] like 'DAA%' then 'Y' else 'N' end
	from
		RTT.Fact Encounter

	left join RTT.Encounter RTTE
	on	RTTE.FactEncounterRecNo = Encounter.EncounterRecno
	and RTTE.Censusdate = Encounter.CensusDate

	left outer join dbo.Specs ReturnSpecialtyGroup
	on	ReturnSpecialtyGroup.spec = Encounter.NationalSpecialtyCode

	left join RTT.PathwayStatus
	on	PathwayStatus.PathwayStatusCode = Encounter.PathwayStatusCode

	where
	
	
		Encounter.CensusDate = @CensusDate
	and	Encounter.AdjustedFlag = @AdjustedFlag
	and 1 = case 
				when  @AdjustedFlag = 'Y' then
								 case when  Encounter.PathwayStatusCode = 'ACS'  then 1 else 0 end
				else 1
			end		
	--and	SpecialtyCode = 'GUM'
	and Encounter.ReportableFlag = '1'
	) 

----deubug
--select
--	*
--from
--	EncounterCTE


select
	 PCTCode
	,PathwayStatusCode
	,[0 Weeks]
	,[1 Week]
	,[2 Weeks]
	,[3 Weeks]
	,[4 Weeks]
	,[5 Weeks]
	,[6 Weeks]
	,[7 Weeks]
	,[8 Weeks]
	,[9 Weeks]
	,[10 Weeks]
	,[11 Weeks]
	,[12 Weeks]
	,[13 Weeks]
	,[14 Weeks]
	,[15 Weeks]
	,[16 Weeks]
	,[17 Weeks]
	,[18 Weeks]
	,[19 Weeks]
	,[20 Weeks]
	,[21 Weeks]
	,[22 Weeks]
	,[23 Weeks]
	,[24 Weeks]
	,[25 Weeks]
	,[26 Weeks]
	,[27 Weeks]
	,[28 Weeks]
	,[29 Weeks]
	,[30 Weeks]
	,[31 Weeks]
	,[32 Weeks]
	,[33 Weeks]
	,[34 Weeks]
	,[35 Weeks]
	,[36 Weeks]
	,[37 Weeks]
	,[38 Weeks]
	,[39 Weeks]
	,[40 Weeks]
	,[41 Weeks]
	,[42 Weeks]
	,[43 Weeks]
	,[44 Weeks]
	,[45 Weeks]
	,[46 Weeks]
	,[47 Weeks]
	,[48 Weeks]
	,[49 Weeks]
	,[50 Weeks]
	,[51 Weeks]
	,[52+ Weeks]
	,[Unknown Clock Start Date]
from
	(
	select
		 PCTCode = ltrim(rtrim(PCTCode))
		,PathwayStatusCode
		,DurationWeek
		,Cases
	from
		(
		select
			 EncounterCTE.PCTCode
			,EncounterCTE.PathwayStatusCode
			,DurationWeeks.DurationWeek
			,Cases = sum(EncounterCTE.Cases)
		from
			EncounterCTE

		right join RTT.DurationWeeks
		on	DurationWeeks.WaitDurationCode = EncounterCTE.WeekBandReturnCode

		where
			DirectAccessAudiologyFlag = @AudiologyFlag

		group by
			 EncounterCTE.PCTCode
			,EncounterCTE.PathwayStatusCode
			,DurationWeeks.DurationWeek
		) Encounter
	) SourceTable
pivot
	(
	sum(Cases)
	for
		DurationWeek in (
			 [0 Weeks]
			,[1 Week]
			,[2 Weeks]
			,[3 Weeks]
			,[4 Weeks]
			,[5 Weeks]
			,[6 Weeks]
			,[7 Weeks]
			,[8 Weeks]
			,[9 Weeks]
			,[10 Weeks]
			,[11 Weeks]
			,[12 Weeks]
			,[13 Weeks]
			,[14 Weeks]
			,[15 Weeks]
			,[16 Weeks]
			,[17 Weeks]
			,[18 Weeks]
			,[19 Weeks]
			,[20 Weeks]
			,[21 Weeks]
			,[22 Weeks]
			,[23 Weeks]
			,[24 Weeks]
			,[25 Weeks]
			,[26 Weeks]
			,[27 Weeks]
			,[28 Weeks]
			,[29 Weeks]
			,[30 Weeks]
			,[31 Weeks]
			,[32 Weeks]
			,[33 Weeks]
			,[34 Weeks]
			,[35 Weeks]
			,[36 Weeks]
			,[37 Weeks]
			,[38 Weeks]
			,[39 Weeks]
			,[40 Weeks]
			,[41 Weeks]
			,[42 Weeks]
			,[43 Weeks]
			,[44 Weeks]
			,[45 Weeks]
			,[46 Weeks]
			,[47 Weeks]
			,[48 Weeks]
			,[49 Weeks]
			,[50 Weeks]
			,[51 Weeks]
			,[52+ Weeks]
			,[Unknown Clock Start Date]
		)
	) AS PivotTable;


﻿
CREATE procedure [RPT].[OpenPathwayBookingsMonthParameter]
(
	  @CensusDate smalldatetime = NULL
	 ,@PeriodRange int = 3
)

as

/******************************************************************************
**  Name:		RPT.OpenPathwayBookingsMonthParameter
**  Purpose:	Populates the BookingsMonthParameterListing parameter in the
**				RTT Performance Forecast report (RTT Legacy Reports)
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		-------		---------------------------------------------------- 
** ??????????	GS			Created
** 2012-09-07	CCB			Refactored:
**								Capitalisation of object names
**								Layout/Indentation
** 17.10.12     MH          Brought into source control 
******************************************************************************/

--declare @Censusdate smalldatetime = '21 aug 2012'
--declare @PeriodRange int = 3

declare
	 @StartDate smalldatetime =
		--dateadd(m , -1 , coalesce(@CensusDate, (select max(CensusDate) from RTT.Encounter)))
		coalesce(@CensusDate, (select max(Censusdate) from RTT.Encounter))

	,@EndDate smalldatetime =
		dateadd(m , @PeriodRange , coalesce(@CensusDate, (select max(Censusdate) from RTT.Encounter)))

select distinct
	 TheMonth
	,FinancialMonthKey
from
	WH.Calendar
where
	TheDate between @StartDate and @EndDate
order by
	Calendar.FinancialMonthKey


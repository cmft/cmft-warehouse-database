﻿
CREATE procedure [RPT].[OpenPathwaysTrendDrillThrough]
(
	 @Censusdate smalldatetime = NULL
	,@AdjustedFlag varchar(1) = 'Y'
	,@PathwayStatusCode varchar(3) = 'IPW' 
	,@Division varchar(max) = 'All'
	,@Specialty varchar(max) = null
	,@BreachBand varchar(max) = '18 Weeks'
	,@CaseCensusStatus varchar(max) = null
)
as

/******************************************************************************
**  Name:		RPT.OpenPathwaysTrendDrillThrough
**  Purpose:	Drill through details for OpenPathwaysTrend report
**				
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		-------		---------------------------------------------------- 
** 17.10.12     MH          Brought into source control 
** 23.06.14     RR          ReportableFlag
******************************************************************************/

--declare @Censusdate smalldatetime = '20 Sep 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Division varchar(max) = 'All'
--declare @PathwayStatusCode varchar(40) = 'IPW'
--declare @Specialty varchar(max) = null
--declare @BreachBand varchar(max) = '18 Weeks'
--declare @CaseCensusStatus varchar(max) = 'All'

declare @localCensusdate smalldatetime = coalesce(@Censusdate, (select max(Censusdate) from RTT.Encounter))

Select
	 division
	,Specialty
	,ReportableFlag
	,pathway_ID
	,AdjustedFlag
	,BreachBandCode
	,Casenote
	,Censusdate
	,Comment1
	,Comment2
	,Consultant
	,DecidedToAdmitDate
	,DiagnosticFlag
	,ReferralDate
	,WeekBandCode
	,DistrictNo
	,EpisodeNo
	,pathway_start_date_current
	,RTTBreachDate
	,Surname
	,WL_WLDate
	,tci_date
	,TCIBeyond
	,PauseStartDate
	,InternalNo
	,CaseCensusStatus
	,PathwayStatus
	,DaysWaiting
	,fut_appt_date
from
	(
	Select
		 division = 
			coalesce(Encounter.path_division , 'Not Known')

		,Specialty = 
			dbo.[f_ProperCase](coalesce(Specialty.NationalSpecialty , '999 - NOT KNOWN'))

		,ReportableFlag
	
		,pathway_ID = 
			Encounter.RTTPathwayID

		,Encounter.AdjustedFlag
		,Encounter.BreachBandCode
		,Encounter.Casenote
		,Encounter.Censusdate
		,Encounter.Comment1
		,Encounter.Comment2

		,Consultant = 
			coalesce(CMFTPASConsultant.consultant, Encounter.Consultant , '') + ' (' + coalesce(Encounter.path_cons , '') + ')'

		,Encounter.DecidedToAdmitDate
		,Encounter.DiagnosticFlag
		,Encounter.ReferralDate
		,Encounter.WeekBandCode
		,Encounter.DistrictNo
		,Encounter.EpisodeNo
		,Encounter.pathway_start_date_current
		,Encounter.RTTBreachDate
		,Encounter.Surname
		,Encounter.WL_WLDate
		,Encounter.tci_date
		,Encounter.TCIBeyond
		,Encounter.PauseStartDate
		,Encounter.InternalNo

		,CaseCensusStatus = 
			case 
			when ROW_NUMBER() OVER(PARTITION BY InternalNo,EpisodeNo ORDER BY censusdate asc) = 1  then 'New'
			else 'Previously Reported'
			end

		,Encounter.PathwayStatus
		,Encounter.DaysWaiting
		,Encounter.fut_appt_date
	
	from
		RTT.Encounter Encounter

	left outer join RPT.Specialty
	on	coalesce(Encounter.path_spec, Encounter.Specialty) = Specialty.SpecialtyCode

	left outer join Warehouse.PAS.Consultant CMFTPASConsultant
	on CMFTPASConsultant.ConsultantCode =  Encounter.path_cons 

	where 
		Encounter.CensusDate IN  
			(
			select top 14 --top 27
				TheDate
			from 
				WH.Calendar
			where 
				(
					(
						Calendar.DayOfWeekKey = 7
					and	TheDate <= @localCensusdate
					)
				or	TheDate = @localCensusdate 
				or	TheDate = (select DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), -1) )
				)
			order by 
				TheDate desc
			)
	--remove Direct Access Audiology
	and	left(Encounter.type , 3) != 'DAA'
	--and Encounter.adjustedFlag = @AdjustedFlag
	and Encounter.AdjustedFlag = 'N'
	and Encounter.PathwayStatusCode = @PathwayStatusCode
	and	(
			coalesce(Encounter.path_division,'Not Known') = @Division 
		or	@Division = 'All'
		)
	and	(
			coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN') = @Specialty
		or	@Specialty is null
		)

	and (
			(
			@BreachBand = '18 Weeks'
			and case 
				when left(BreachBandCode,2) = 'BR' then 1 
				else 0 
				end = 1
			)
		or
			(
			@BreachBand = '52 Weeks'
			and case 
				when WeekBandCode = 52 then 1 
				else 0 
				end = 1
			)
		)
	) Encounter

where
	(
		CaseCensusStatus = @CaseCensusStatus
	or	@CaseCensusStatus is null
	)
and Encounter.CensusDate =  @localCensusdate
	
order by
	 Encounter.WeekBandCode desc
	,pathway_start_date_current
	,PathwayStatus
	,Specialty
	,Consultant
	,tci_date
	,Surname

﻿
CREATE procedure [RPT].[OpenPathways]
(
	 @Censusdate smalldatetime = NULL
	,@AdjustedFlag varchar(1) = 'N'
	,@Division varchar(max) = 'All'
)

as
/******************************************************************************
**  Name:		RPT.OpenPathways
**  Purpose:	Open pathways report
**				
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		-------		---------------------------------------------------- 
** 17.10.12     MH          Brought into source control 
******************************************************************************/

declare @localCensusdate smalldatetime = coalesce(@Censusdate, (select max(censusdate) from rtt.encounter))

--declare @Censusdate smalldatetime = '21 aug 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Period varchar(8) = 'Aug 2012'

select
	  division 
	 ,Specialty
	 ,ReportableFlag
	 ,ReturnSpecialtyGroup
	 ,Admitted = sum(Admitted)
	 ,AdmittedOver18Weeks = sum(AdmittedOver18Weeks)
	 ,NotAdmitted = sum(NotAdmitted)
	 ,NotAdmittedOver18Weeks = sum(NotAdmittedOver18Weeks)

from
(
Select
	 division = 
		coalesce(Encounter.path_division,'Not Known')

	,Specialty = 
		dbo.[f_ProperCase](coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN'))

	,ReturnSpecialtyGroup =
		case
		when ReturnSpecialtyGroup.Spec IS NULL OR Encounter.path_division = 'Childrens' THEN '999 - Other' 
		else  coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN')
		end
	
	,ReportableFlag
	
	,Admitted = 
		case 
		when PathwayStatusCode = 'IPW' then 1 
		else 0 
		end

	,AdmittedOver18Weeks = 
		case 
		when PathwayStatusCode = 'IPW' 
		and left(BreachBandCode,2) = 'BR' then 1 
		else 0 
		end

	,NotAdmitted = 
		case 
		when PathwayStatusCode = 'OPW' then 1 
		else 0 
		end

	,NotAdmittedOver18Weeks = 
		case 
		when PathwayStatusCode = 'OPW' 
		and left(BreachBandCode,2) = 'BR' then 1 
		else 0 
		end
from
	rtt.encounter

left outer join rpt.Specialty
on	encounter.path_spec = Specialty.SpecialtyCode

left outer join dbo.Specs ReturnSpecialtyGroup
on encounter.Specialty = ReturnSpecialtyGroup.spec

where 
	encounter.Censusdate =  @localCensusdate
--remove Direct Access Audiology
and	left(Encounter.type , 3) != 'DAA'
and encounter.adjustedFlag = 'N'
and encounter.PathwayStatusCode  in ('IPW', 'OPW')
and (
		encounter.path_division = @Division
	or 
		@Division ='All'
	)
)encounter

group by 
	 division 
	,Specialty
	,ReturnSpecialtyGroup
	,ReportableFlag



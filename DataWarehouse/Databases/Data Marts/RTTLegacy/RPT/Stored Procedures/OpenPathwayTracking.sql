﻿
CREATE procedure [RPT].[OpenPathwayTracking]
(
	 @Censusdate smalldatetime = NULL
	,@AdjustedFlag varchar(1) = 'N'
	,@Division varchar(max) = 'All'
	,@BreachBandCode varchar(max)
	,@PathwaysStatusCode varchar(max)
	,@HasTCIDate  varchar(9)  = 'No Filter'
	,@TCIBeyondBreachDate  varchar(9)  = 'No Filter'
)
as

/******************************************************************************
**  Name:		RPT.OpenPathwayTracking
**  Purpose:	
**				
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		-------		---------------------------------------------------- 
** 17.10.12     MH          Brought into source control 
** 23.06.14     JR          ReportableFlag
******************************************************************************/

--declare @Censusdate smalldatetime = '21 aug 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Division varchar(8) = 'All'
--declare @BreachBandCode varchar(4) = 'F01'
--declare @PathwaysStatusCode varchar(max) = 'IPW'
--declare @HasTCIDate varchar(9) = 'No Filter'
--declare @TCIBeyondBreachDate varchar(9)  = 'No Filter'

declare @localCensusdate smalldatetime = coalesce(@Censusdate, (select max(censusdate) from rtt.encounter))

Select
	 division = 
		coalesce(Encounter.path_division,'Not Known')

	,Specialty = 
		dbo.[f_ProperCase](coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN'))
	
	,ReportableFlag
	
	,pathway_ID = 
		encounter.RTTPathwayID

	,encounter.AdjustedFlag
	,encounter.BreachBandCode
	,BreachBand = BreachBand.BreachBand
	,encounter.Casenote
	,encounter.Censusdate
	,encounter.Comment1
	,encounter.Comment2

	,Consultant = 
		coalesce(CMFTPASConsultant.consultant, encounter.Consultant,'') + ' (' + isnull(encounter.path_cons,'') + ')'

	,encounter.DecidedToAdmitDate
	,encounter.DiagnosticFlag
	,encounter.ReferralDate
	,encounter.WeekBandCode
	,encounter.DistrictNo
	,encounter.EpisodeNo
	,encounter.pathway_start_date_current
	,encounter.RTTBreachDate
	,encounter.Surname
	,encounter.WL_WLDate
	,encounter.tci_date
	,encounter.TCIBeyond
	,encounter.PauseStartDate
	,encounter.InternalNo
	,encounter.PathwayStatus
	,encounter.fut_appt_date
from
	rtt.encounter

inner join dbo.OlapBreachBand BreachBand
on encounter.BreachBandCode = BreachBand.BreachBandCode

left outer join rpt.Specialty
on	coalesce(encounter.path_spec, encounter.specialty) = Specialty.SpecialtyCode

left outer join warehouse.pas.Consultant CMFTPASConsultant
	on CMFTPASConsultant.ConsultantCode =  encounter.path_cons 

where 
	encounter.Censusdate =  @localCensusdate
--remove Direct Access Audiology
and	left(Encounter.type , 3) != 'DAA'
and encounter.adjustedFlag = 'N'
and (
		encounter.path_division = @Division
	or	@Division ='All'
	)
and (
		(case when encounter.tci_date is null then '0' else '1' end) = @HasTCIDate
	or	@HasTCIDate ='No Filter'
	)
and (
		isnull(convert(varchar(9),TCIBeyond),0) = @TCIBeyondBreachDate
	or	@TCIBeyondBreachDate = 'No Filter'
	)
and encounter.BreachBandCode in (SELECT VALUE FROM dbo.SSRS_MultiValueParamSplit(@BreachBandCode,','))
and encounter.PathwayStatusCode in (SELECT VALUE FROM dbo.SSRS_MultiValueParamSplit(@PathwaysStatusCode,','))

	



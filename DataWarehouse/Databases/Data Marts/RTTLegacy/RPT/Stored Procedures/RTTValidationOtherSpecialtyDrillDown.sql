﻿
/******************************************************************************
**  Name: [RPT].[RTTValidationOtherSpecialtyDrillDown]
**  Purpose: RTT validation report stored procedure for RTT the National submission
**			 of the "Other Specialty Line" (Specialty 999)
**			 Drill Down report called from [RPT].[RTTValidationOtherSpecialty]
**
**	Input Parameters
**		@CensusDate			Census Date 
**      @AdjustedFlag		Valid values Y - To select RTT Adjusted data, N otherwise
**      @Division			Division name
**		@NationalSpecialtyCode
**		@AdmissionType		Valid Values 'Admitted', 'Non-Admitted'
**		@PathwayType		Valid values 'Open', 'Closed','All'
**		@IsBreached18Wks	Valid value 'Y','N' or NULL
**		@IsBreached52Wks	Valid value 'Y','N' or NULL
**		
**
**	NOTE - Please maintain this stored procedure via Visual Studio 2012 solution
**         RTT Legacy VS2012 >> RTTLegacy
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 04.11.13      MH       Created
******************************************************************************/

CREATE PROCEDURE [RPT].[RTTValidationOtherSpecialtyDrillDown]
(
	 @CensusDate				DATE
	,@AdjustedFlag				NCHAR(1)
	,@Division					NVARCHAR(50)	= NULL
	,@NationalSpecialtyCode		NVARCHAR(10)	= NULL
	,@AdmissionType				NVARCHAR(20)
	,@PathwayType				NVARCHAR(10)
	,@IsBreached18Wks			NCHAR(1)		= NULL
	,@IsBreached52Weeks			NCHAR(1)		= NULL
) 
AS
	SELECT
		 Division							= E.path_division
		,NationalSpecialty					= dbo.[f_ProperCase](COALESCE(SPEC.NationalSpecialty,'999 - NOT KNOWN'))
		,Consultant							= COALESCE(CMFTPASConsultant.Consultant, E.Consultant,'') + ' (' + ISNULL(E.path_cons,'') + ')'
		,CaseNote							= E.CaseNote
		,InternalNo							= E.InternalNo
		,EpisodeNo							= E.EpisodeNo
		,Surname							= E.Surname
		,Forename							= E.Forename
		,Age								= DATEDIFF(YEAR, E.DateOfBirth, @CensusDate)
		,PathwayID							= E.RTTPathwayID
		,PathwayStatus						= E.PathwayStatus
		,LastAttendanceStatus				= E.LastAttendanceStatusCode
		,WeekBandCode						= E.WeekBandCode
		,DaysWaiting						= E.DaysWaiting
		,ReferralDate						= E.ReferralDate
		,PathwayStartDate					= E.pathway_start_date_current
		,PathwayEndDate						= E.pathway_end_date
		,WLDate								= E.WL_WLDate
		,TCIDate							= E.tci_date
		,DateToBeSeenBy						= E.pathway_treat_by_date
		,[18WkBreachDate]					= F.RTTBreachDate
		,PauseStartDate						= E.PauseStartDate
		,IsTCIBeyondBreachDate				= CASE WHEN E.TCIBeyond = 1 THEN 'TRUE' ELSE '' END
		,Comment1							= E.Comment1
		,Comment2							= E.Comment2

	FROM
		RTT.Encounter E

		INNER JOIN RTT.Fact F
			ON E.FactEncounterRecNo = F.EncounterRecno

		LEFT OUTER JOIN RPT.Specialty SPEC
			ON	COALESCE(E.path_spec, E.Specialty) = SPEC.SpecialtyCode		

		LEFT OUTER JOIN Warehouse.PAS.Consultant CMFTPASConsultant
			ON CMFTPASConsultant.ConsultantCode =  E.path_cons 
				
	WHERE
			E.CensusDate					= @CensusDate
		AND	E.AdjustedFlag					= @AdjustedFlag
		AND E.path_division					= COALESCE(@Division, E.path_division)
		AND E.path_doh_spec					= COALESCE(@NationalSpecialtyCode, E.path_doh_spec)
		AND	CASE 
				WHEN E.PathwayStatusCode IN ('IPW','ACS') THEN 'Admitted'
				WHEN E.PathwayStatusCode IN ('OPW','OPT') THEN 'Non-Admitted'
				ELSE 'Unknown'
			END	= @AdmissionType
		AND CASE  
				WHEN @PathwayType = 'All' THEN @PathwayType
				WHEN E.WorkflowStatusCode = 'C' THEN 'Closed' 
				ELSE 'Open' 
			END = @PathwayType
		AND  (
					CASE WHEN E.DaysWaiting >126 THEN 'Y' ELSE 'N' END = @IsBreached18Wks
				OR	@IsBreached18Wks IS NULL
			)
		AND (
					CASE WHEN E.DaysWaiting >364 THEN 'Y' ELSE 'N' END = @IsBreached52Weeks
				OR	@IsBreached52Weeks IS NULL
			)
		AND NOT 
			(
					E.path_doh_spec  IN ('100','101','110','120','130','140','150','160','170','300','301','320','330','340','400','410','430','502')
				AND E.path_division <> 'Childrens'		
			)
		AND F.ReportableFlag					= '1'
		AND COALESCE(LEFT(E.[type],3),'')		<> 'DAA'
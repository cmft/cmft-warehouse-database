﻿
CREATE procedure [RPT].[OpenPathwaysTrend]
(
	 @Censusdate smalldatetime = '20 Oct 2012'
	,@AdjustedFlag varchar(1) = 'Y'
	,@Division varchar(max) = 'All'
	,@PathwayStatusCode varchar(3) = 'IPW'
	,@BreachBand varchar(8) = '18 Weeks'
)

as

/******************************************************************************
**  Name:		RPT.OpenPathwaysTrend
**  Purpose:	
**				
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** --------		-------		---------------------------------------------------- 
** 17.10.12     MH          Brought into source control 
** 23.06.14     RR          Added Reportable Flag 
******************************************************************************/

--declare @Censusdate smalldatetime = '19 aug 2012'
--declare @AdjustedFlag varchar(1) = 'Y'
--declare @Division varchar(max) = 'Childrens'
--declare @PathwayStatusCode varchar(3) = 'IPW'
--declare @BreachBand varchar(8) = '18 Weeks'


declare @localCensusdate smalldatetime = coalesce(@Censusdate, (select max(censusdate) from rtt.encounter))


Select
	 Censusdate
	,Censusdateint = 
		convert(int,(convert(varchar(8),Censusdate,112)))

	,CensusdateLabel = 
		case 
		when Censusdate = (select DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), -1) ) then 'Previous Month - ' +  convert(varchar(11),Censusdate,103)
		when Censusdate = @localCensusdate  then 'Current - ' +  convert(varchar(11),Censusdate,103)
		else convert(varchar(11),Censusdate,103)
		end

	,CensusdateOrder = 
		case 
		when Censusdate = (select DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), -1) ) then 2
		when Censusdate = @localCensusdate  then 3
		else 1
		end
	
	,Division = 
		coalesce(Encounter.path_division,'Not Known')

	,Specialty = 
		dbo.[f_ProperCase](coalesce(Specialty.NationalSpecialty,'999 - NOT KNOWN'))
	
	,ReportableFlag
		 
	,encounter.AdjustedFlag
	,BacklogGraphColours.SeriesColour
	,Cases = 1

	,CaseCensusStatus = 
		case 
		when ROW_NUMBER() OVER(PARTITION BY InternalNo,EpisodeNo ORDER BY censusdate asc) = 1  then 'New'
		else 'Previously Reported'
		end
from
	rtt.encounter encounter

left outer join rpt.Specialty
on	coalesce(encounter.path_spec, encounter.specialty) = Specialty.SpecialtyCode

left outer join dbo.BacklogGraphColours
on encounter.path_division = case when BacklogGraphColours.Division = 'Women And Children' then 'St Mary''''s' else BacklogGraphColours.Division end

where 
	encounter.Censusdate IN  
		(
		select top 14 --top 27
			TheDate
		from 
			wh.Calendar
		where 
			(
				(
					Calendar.DayOfWeekKey = 7
				and	TheDate <= @localCensusdate
				)
			or	TheDate = @localCensusdate 
			or	TheDate = (select DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), -1) )
			)
		order by 
			TheDate desc
		)
--remove Direct Access Audiology
and	left(Encounter.type , 3) != 'DAA'
--and encounter.adjustedFlag = @AdjustedFlag
and encounter.adjustedFlag = 'N'
and encounter.PathwayStatusCode = @PathwayStatusCode
and (
		encounter.path_division = @Division
	or 	@Division ='All'
	)
and (
		(
			@BreachBand = '18 Weeks'
		and case 
			when left(BreachBandCode,2) = 'BR' then 1 
			else 0 
			end = 1
		)
	or
		(
			@BreachBand = '52 Weeks'
		and case 
			when WeekBandCode = 52 then 1 
			else 0 
			end = 1
		)
	)


﻿
/******************************************************************************
**  Name: [RPT].[RTTValidationOtherSpecialty]
**  Purpose: RTT validation report stored procedure for RTT the National submission
**			 of the "Other Specialty Line" (Specialty 999)
**
**	Input Parameters
**		@CensusDate		Census Date 
**      @AdjustedFlag	Y - To select RTT Adjusted data, N otherwise
**      @Division		
**		@PathwayType	Valid values are Open, Closed or All
**
**	NOTE - Please maintain this stored procedure via Visual Studio 2012 solution
**         RTT Legacy VS2012 >> RTTLegacy
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 04.11.13      MH       Created
******************************************************************************/

CREATE PROCEDURE [RPT].[RTTValidationOtherSpecialty]
(
	 @CensusDate	 DATE
	,@AdjustedFlag	 NCHAR(1)
	,@Division		 NVARCHAR(50)
	,@PathwayType	 NVARCHAR(10)
) 
AS
	SELECT
		 Division				= DATA.Division
		,Directorate			= DATA.Directorate
		,WaitType				= DATA.WaitType
		,NationalSpecialty		= dbo.[f_ProperCase](COALESCE(SPEC.NationalSpecialty,'999 - NOT KNOWN'))
		,NationalSpecialtyCode	= DATA.NationalSpecialtyCode
		,WeekBand				= DATA.WeekBandReturnCode
		,AdmissionType			= DATA.AdmissionType
		,PathwayType			= DATA.PathwayType
		,[18PlusWeeks]			= SUM(DATA.Is18Plus) 
		,[52PlusWeeks]			= SUM(DATA.Is52Plus) 
		,[Under18Weeks]			= SUM(DATA.IsUnder18)
		,[All]					= SUM(DATA.[All])

	FROM
	(
		SELECT
			 F.*
			,AdmissionType	= CASE 
								WHEN F.PathwayStatusCode IN ('IPW' ,'ACS') THEN 'Admitted'
								WHEN F.PathwayStatusCode IN ('OPW' ,'OPT') THEN 'Non-Admitted'
								ELSE 'Unknown'
							  END
			,PathwayType	= CASE
								WHEN F.WorkflowStatusCode = 'C' THEN 'Closed'
								ELSE 'Open'
							  END
			,WaitType		= CASE 
								WHEN F.[DaysWaiting] >126  AND F.[DaysWaiting] <=364 
									THEN '18+ to 52 weeks' 
								WHEN F.[DaysWaiting] >364 
									THEN '52+ weeks' 
								ELSE 'Under 18 weeks' 
							  END
			,Is18Plus		= CASE WHEN F.[DaysWaiting] >126 THEN 1 ELSE 0 END
			,Is52Plus		= CASE WHEN F.[DaysWaiting] >364 THEN 1 ELSE 0 END
			,IsUnder18		= CASE WHEN F.[DaysWaiting] <=126 THEN 1 ELSE 0 END
			,[All]			= 1
			,E.path_spec
			,E.Specialty
			
		FROM	
			RTT.Fact  F
			INNER JOIN RTT.Encounter E
				ON F.EncounterRecno = E.FactEncounterRecNo
				
		WHERE
				F.CensusDate						= @CensusDate
			AND F.AdjustedFlag						= @AdjustedFlag
			AND F.Division							= CASE WHEN @Division = 'All' THEN F.Division ELSE @Division END
			AND NOT 
				(
						F.NationalSpecialtyCode  IN ('100','101','110','120','130','140','150','160','170','300','301','320','330','340','400','410','430','502')
					AND F.Division <> 'Childrens'		
				)
			AND F.ReportableFlag					= '1'
			AND COALESCE(LEFT(E.[type],3),'')		<> 'DAA'
	) DATA
	
	LEFT OUTER JOIN RPT.Specialty SPEC
		ON	COALESCE(DATA.path_spec, DATA.Specialty) = SPEC.SpecialtyCode		
	
	WHERE
		DATA.PathwayType	= CASE 
								WHEN @PathwayType = 'All' THEN DATA.PathwayType 
								ELSE @PathwayType 
							  END
		
	GROUP BY
		 DATA.Division
		,DATA.Directorate
		,DATA.WaitType
		,SPEC.NationalSpecialty
		,DATA.NationalSpecialtyCode
		,DATA.WeekBandReturnCode
		,DATA.AdmissionType
		,DATA.PathwayType
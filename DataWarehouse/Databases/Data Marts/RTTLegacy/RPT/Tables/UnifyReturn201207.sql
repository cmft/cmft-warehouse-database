﻿CREATE TABLE [RPT].[UnifyReturn201207] (
    [AdjustedFlag]  VARCHAR (3)    NOT NULL,
    [RecordType]    VARCHAR (21)   NOT NULL,
    [provcomm]      VARCHAR (1)    NOT NULL,
    [prov_org]      VARCHAR (3)    NOT NULL,
    [PCTr]          NVARCHAR (255) NULL,
    [YGCONCAT]      VARCHAR (13)   NULL,
    [XG1]           VARCHAR (3)    NOT NULL,
    [Weeks Wait]    INT            NULL,
    [InternalNo]    NVARCHAR (9)   NULL,
    [EpisodeNo]     NVARCHAR (9)   NULL,
    [specs]         VARCHAR (10)   NULL,
    [Hospital]      NVARCHAR (4)   NULL,
    [Cases]         FLOAT (53)     NULL,
    [SpecialtyCode] VARCHAR (10)   NULL,
    [Specialty]     VARCHAR (100)  NULL,
    [Under18Weeks]  INT            NULL,
    [Over18Weeks]   INT            NULL
);


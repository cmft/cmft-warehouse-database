﻿
create view [RPT].[Specialty]

as

select
	 Specialty.SpecialtyCode
	,Specialty.Specialty
	,Specialty.NationalSpecialtyCode
	,Specialty.NationalSpecialty
from
	WarehouseReporting.ETL.BasePASSpecialty Specialty

union all

select distinct
	 SpecialtyCode = Specialty.NationalSpecialtyCode
	,Specialty = Specialty.NationalSpecialty
	,Specialty.NationalSpecialtyCode
	,Specialty.NationalSpecialty
from
	WarehouseReporting.ETL.BasePASSpecialty Specialty

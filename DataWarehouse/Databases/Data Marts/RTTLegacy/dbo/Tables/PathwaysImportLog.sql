﻿CREATE TABLE [dbo].[PathwaysImportLog] (
    [id]         INT           IDENTITY (1, 1) NOT NULL,
    [UpdateDate] SMALLDATETIME NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


﻿CREATE TABLE [dbo].[1a__Closed pathways] (
    [InternalNo]                  NVARCHAR (9)   NULL,
    [EpisodeNo]                   NVARCHAR (9)   NULL,
    [CaseNoteNo]                  NVARCHAR (16)  NULL,
    [DistrictNo]                  NVARCHAR (16)  NULL,
    [pathway_end_date]            DATETIME       NULL,
    [ReferralDate]                DATETIME       NULL,
    [pathway_start_date_original] DATETIME       NULL,
    [pathway_start_date_current]  DATETIME       NULL,
    [pathway_status]              NVARCHAR (255) NULL,
    [type]                        NVARCHAR (10)  NULL,
    [date_moved]                  DATETIME       NULL,
    [wl_date]                     DATETIME       NULL,
    [WL_epi_no]                   NVARCHAR (50)  NULL,
    [comment]                     NVARCHAR (50)  NULL,
    [spec]                        NVARCHAR (3)   NULL,
    [wl_spec]                     NVARCHAR (3)   NULL,
    [addbackin]                   NVARCHAR (1)   NULL
);


﻿CREATE TABLE [dbo].[DailySpecialtyTargets] (
    [id]         INT             IDENTITY (1, 1) NOT NULL,
    [IPorOP]     NVARCHAR (50)   NULL,
    [BreachDays] INT             NULL,
    [Target]     DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_DailySpecialtyTargets] PRIMARY KEY CLUSTERED ([id] ASC)
);


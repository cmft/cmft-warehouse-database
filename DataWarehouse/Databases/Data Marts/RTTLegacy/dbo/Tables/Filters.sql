﻿CREATE TABLE [dbo].[Filters] (
    [id]              INT            NOT NULL,
    [DirectorateName] NVARCHAR (100) NULL,
    [ShortName]       NVARCHAR (50)  NULL,
    [Division]        NVARCHAR (150) NULL,
    [Directorate]     NVARCHAR (255) NULL
);


﻿CREATE TABLE [dbo].[Backlog] (
    [id]                          INT            IDENTITY (1, 1) NOT NULL,
    [division]                    NVARCHAR (50)  NULL,
    [directorate]                 NVARCHAR (50)  NULL,
    [Doh_Spec]                    NVARCHAR (101) NULL,
    [Wks]                         INT            NULL,
    [CaseNoteNo]                  NVARCHAR (16)  NULL,
    [forename]                    NVARCHAR (50)  NULL,
    [surname]                     NVARCHAR (50)  NULL,
    [Pathway Start Date Original] DATETIME2 (7)  NULL,
    [pathway_status]              NVARCHAR (255) NULL,
    [path_open_days_DNA_adjs]     INT            NULL,
    [days]                        INT            NULL,
    [treattype]                   NVARCHAR (6)   NULL,
    [IPorOP]                      VARCHAR (12)   NOT NULL,
    [WL_WLDate]                   DATETIME2 (7)  NULL,
    [IntendedManagement]          VARCHAR (9)    NULL,
    [Descision to treat date]     VARCHAR (30)   NULL,
    [MaxTreatdate]                VARCHAR (30)   NULL,
    [tci_date]                    DATETIME2 (7)  NULL,
    [InternalNo]                  NVARCHAR (9)   NULL,
    [EpisodeNo]                   NVARCHAR (9)   NULL,
    [RTT_Pathway_ID]              NVARCHAR (50)  NULL,
    [fut_appt_date]               DATETIME2 (7)  NULL,
    [pathway_end_date]            DATETIME2 (7)  NULL,
    [pathway_treat_by_date]       DATETIME2 (7)  NULL,
    [28WK]                        NVARCHAR (50)  NULL,
    [OldTreatByDate]              DATETIME2 (7)  NULL,
    [Comments]                    VARCHAR (250)  NULL,
    [adjdays]                     INT            NULL,
    [WeekEnd]                     NVARCHAR (10)  NULL,
    [ExtractDate]                 NVARCHAR (10)  NULL,
    [TMP]                         NVARCHAR (1)   NULL,
    [NewP]                        NVARCHAR (1)   NULL,
    [PComments]                   NVARCHAR (255) NULL,
    [BreachDays]                  INT            NULL,
    [NT]                          BIT            CONSTRAINT [DF_Backlog_OR] DEFAULT ((0)) NULL,
    [Path_Cons]                   NVARCHAR (50)  NULL,
    [OP_treat_date]               DATETIME2 (7)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [idx_Backlog]
    ON [dbo].[Backlog]([NT] ASC, [IPorOP] ASC, [BreachDays] ASC)
    INCLUDE([id], [division], [WeekEnd], [ExtractDate], [TMP], [NewP]);


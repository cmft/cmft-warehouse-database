﻿CREATE TABLE [dbo].[BacklogGraphCopy] (
    [Total]        INT           NULL,
    [division]     NVARCHAR (50) NULL,
    [ExtractDate]  NVARCHAR (10) NULL,
    [WeekEnd]      NVARCHAR (10) NULL,
    [IPorOP]       VARCHAR (12)  NOT NULL,
    [BreachTarget] INT           NULL,
    [NT]           BIT           NULL
);


﻿CREATE TABLE [dbo].[RttMedian] (
    [path_division]      NVARCHAR (100)  NULL,
    [path_doh_spec]      NVARCHAR (50)   NULL,
    [path_doh_desc]      NVARCHAR (255)  NULL,
    [ReportMonth]        NVARCHAR (50)   NULL,
    [DateRun]            DATETIME        NULL,
    [TotalRecords]       INT             NULL,
    [Median_Value]       INT             NULL,
    [Median_Value_Weeks] DECIMAL (18, 4) NULL,
    [IPNonIP]            NVARCHAR (100)  NULL,
    [Percentile]         DECIMAL (18, 4) NULL,
    [Percentile_Weeks]   DECIMAL (18, 4) NULL,
    [DataUsed]           NVARCHAR (200)  NULL
);


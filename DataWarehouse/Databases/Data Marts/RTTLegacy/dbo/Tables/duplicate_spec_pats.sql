﻿CREATE TABLE [dbo].[duplicate_spec_pats] (
    [path_division]   NVARCHAR (50) NULL,
    [InternalNo]      NVARCHAR (9)  NULL,
    [Spec_use]        NVARCHAR (50) NULL,
    [CountOfSpec_use] INT           NULL,
    [importDate]      DATETIME2 (7) NULL
);


﻿CREATE TABLE [dbo].[tmpReportDataSpecialty] (
    [Doh_Spec]    NVARCHAR (101) NULL,
    [Total]       INT            NULL,
    [Weekend]     DATE           NULL,
    [IPorOP]      VARCHAR (12)   NOT NULL,
    [ExtractDate] NVARCHAR (10)  NULL
);


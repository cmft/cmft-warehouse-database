﻿CREATE TABLE [dbo].[tmpTable] (
    [InternalNo]                 NVARCHAR (9)   NULL,
    [EpisodeNo]                  NVARCHAR (9)   NULL,
    [pathway_start_date_current] VARCHAR (17)   NULL,
    [Division]                   NVARCHAR (50)  NULL,
    [pathway_status]             NVARCHAR (255) NULL,
    [pathway_end_reason]         NVARCHAR (15)  NULL,
    [pathway_open]               NVARCHAR (50)  NULL,
    [pathway_Closed]             NVARCHAR (50)  NULL
);


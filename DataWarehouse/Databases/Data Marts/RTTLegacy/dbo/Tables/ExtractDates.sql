﻿CREATE TABLE [dbo].[ExtractDates] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [EDate]       DATETIME2 (7) NULL,
    [ExtractDate] NVARCHAR (12) NULL,
    [MonthEnd]    NVARCHAR (2)  CONSTRAINT [DF__ExtractDa__MonthEnd] DEFAULT (N'-') NULL,
    [Archived]    BIT           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


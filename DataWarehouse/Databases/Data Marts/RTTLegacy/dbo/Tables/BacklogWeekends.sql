﻿CREATE TABLE [dbo].[BacklogWeekends] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [WeekEnd]     NVARCHAR (10) NULL,
    [ExtractDate] NVARCHAR (10) NULL,
    CONSTRAINT [PK_BacklogWeekends] PRIMARY KEY CLUSTERED ([id] ASC)
);


﻿CREATE TABLE [dbo].[DuplicatesToRemove] (
    [id]          INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo]  NVARCHAR (9)   NULL,
    [EpisodeNo]   NVARCHAR (9)   NULL,
    [CreatedBy]   NVARCHAR (100) NULL,
    [CreatedDate] DATETIME       NULL
);


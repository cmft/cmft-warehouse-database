﻿CREATE TABLE [dbo].[SpecTemp] (
    [Spec_name]      NVARCHAR (30)   NULL,
    [under23wks]     INT             NULL,
    [over23wks]      INT             NULL,
    [Total]          INT             NULL,
    [Days To Breach] INT             NULL,
    [Month Selected] NVARCHAR (20)   NULL,
    [ExtractDate]    DATETIME        NULL,
    [Target]         DECIMAL (18, 2) NULL,
    [desc]           NVARCHAR (30)   NULL,
    [spec]           NVARCHAR (3)    NULL
);


﻿CREATE TABLE [dbo].[tmpReportData] (
    [id]          INT           IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (75)  NOT NULL,
    [Total]       INT           NULL,
    [Weekend]     DATE          NULL,
    [IPorOP]      VARCHAR (12)  NOT NULL,
    [ExtractDate] NVARCHAR (10) NULL,
    [Division]    NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


﻿CREATE TABLE [dbo].[ReportPeriod] (
    [id]            INT           IDENTITY (1, 1) NOT NULL,
    [ExtractDateId] INT           NOT NULL,
    [Monthyr]       NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [FK_ReportPeriod_ExtractDates] FOREIGN KEY ([ExtractDateId]) REFERENCES [dbo].[ExtractDates] ([id])
);


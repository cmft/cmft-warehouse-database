﻿CREATE TABLE [dbo].[BacklogGraphColours] (
    [Division]     NVARCHAR (100) NOT NULL,
    [SeriesColour] NVARCHAR (50)  NULL,
    CONSTRAINT [PK_BacklogGraphColours] PRIMARY KEY CLUSTERED ([Division] ASC)
);


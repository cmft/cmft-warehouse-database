﻿CREATE TABLE [dbo].[BacklogGrowthTarget] (
    [id]           INT           IDENTITY (1, 1) NOT NULL,
    [IPorOP]       NVARCHAR (50) NULL,
    [ClosedTarget] INT           NULL,
    [OpenTarget]   INT           NULL,
    CONSTRAINT [PK_BacklogGrowthTarget] PRIMARY KEY CLUSTERED ([id] ASC)
);


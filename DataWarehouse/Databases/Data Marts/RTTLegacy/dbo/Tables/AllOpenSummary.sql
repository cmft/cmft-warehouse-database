﻿CREATE TABLE [dbo].[AllOpenSummary] (
    [division]                      NVARCHAR (50)  NULL,
    [speciality]                    NVARCHAR (50)  NULL,
    [path_spec]                     NVARCHAR (101) NULL,
    [Admitted]                      INT            NULL,
    [Admitted Over 18 Weeks]        INT            NULL,
    [Non-Admitted]                  INT            NULL,
    [Non-Admitted Over 18 Weeks]    INT            NULL,
    [WeekNo]                        INT            NULL,
    [% Admitted under 18 Weeks]     DECIMAL (5, 4) NULL,
    [% Non-Admitted under 18 Weeks] DECIMAL (5, 4) NULL,
    [% Total under 18 Weeks]        DECIMAL (5, 4) NULL,
    [Performance]                   DECIMAL (5, 4) NULL
);


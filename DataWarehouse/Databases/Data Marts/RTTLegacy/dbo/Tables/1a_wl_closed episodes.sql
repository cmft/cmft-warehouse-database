﻿CREATE TABLE [dbo].[1a_wl_closed episodes] (
    [InternalNo] NVARCHAR (9)  NULL,
    [WL_epi_no]  NVARCHAR (50) NULL,
    [wl_date]    DATETIME      NULL,
    [rem]        NVARCHAR (50) NULL,
    [Comment]    NVARCHAR (50) NULL,
    [addbackin]  NVARCHAR (1)  NULL
);


﻿/******************************************************************************
**  Name: [dbo].[udf_Open_OT]
**  Purpose: RTT find if STL
**
**  Replacement for udf_PTL_Open_OT and udf_STL_Open_OT
**
****	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 25.10.12   MH          Created
******************************************************************************/

CREATE FUNCTION [dbo].[udf_Open_OT]
(
	@Pathway_Start_Date datetime, @type nvarchar(10), @ReportMonth int
)
RETURNS TABLE AS
RETURN
(
	SELECT
		OpenOTType = 
		CASE 
			WHEN	(@Pathway_Start_Date < STL_period_start OR @Pathway_Start_Date > DATEADD(DAY,1,STL_period_end))
				AND (@type = 'Open') 
			  THEN 'PTLOPEN'

			WHEN	(@Pathway_Start_Date >= STL_period_start AND @Pathway_Start_Date < DATEADD(DAY,1,STL_period_end))
				AND (@type = 'Open') 
			  THEN 'STLOPEN'

			ELSE 'NOTOPENOT'
		END
	FROM 
		ManualAdjustment_rtt_PeriodUpdate
	WHERE
	    id = @ReportMonth
)



﻿/******************************************************************************
**  Name: [dbo].[udf_OT]
**  Purpose: RTT find if STL
**
**  Replacement for udf_PTL_OT and udf_STL_OT
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 25.10.12   MH          Created
******************************************************************************/

CREATE FUNCTION [dbo].[udf_OT]
(
	@Pathway_Start_Date datetime, @tci_fut_app_date datetime, @ReportMonth int
)
RETURNS TABLE AS
RETURN
(
	SELECT
		OTType = 
		CASE 
			WHEN	(@Pathway_Start_Date < STL_period_start OR @Pathway_Start_Date > DATEADD(DAY,1,STL_period_end))
				AND (@tci_fut_app_date >= Monitor_month_start AND @tci_fut_app_date < DATEADD(DAY,1,Monitor_month_end)) 
			  THEN 'PTL'

			WHEN	(@Pathway_Start_Date >= STL_period_start AND @Pathway_Start_Date < DATEADD(DAY,1,STL_period_end))
				AND (@tci_fut_app_date >= Monitor_month_start AND @tci_fut_app_date < DATEADD(DAY,1,Monitor_month_end))
			  THEN 'STL'

			ELSE 'NOTOT'
		END
	FROM 
		ManualAdjustment_rtt_PeriodUpdate
	WHERE
	    id = @ReportMonth
)



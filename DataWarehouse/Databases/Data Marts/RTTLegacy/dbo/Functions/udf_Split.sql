﻿CREATE FUNCTION dbo.udf_Split
(
      @Expression NVARCHAR(max)
    , @Delimiter  NVARCHAR(max)
    , @INDEX      INT
)
RETURNS NVARCHAR(max)
AS
BEGIN
    DECLARE @RETURN  NVARCHAR(max)
    DECLARE @Pos     INT
    DECLARE @PrevPos INT
    DECLARE @I       INT
   
    -- SELECT dbo.udfSplit('4.55.108.2','.', 2)
   
    IF @Expression IS NULL OR @Delimiter IS NULL OR LEN(@Delimiter) = 0 OR @INDEX < 1
        SET @RETURN = NULL
    ELSE IF @INDEX = 1 BEGIN
        SET @Pos = CHARINDEX(@Delimiter, @Expression, 1)
        IF @Pos > 0 SET @RETURN = LEFT(@Expression, @Pos - 1)
    END ELSE BEGIN
        SET @Pos = 0
        SET @I = 0
       
        WHILE (@Pos > 0 AND @I < @INDEX) OR @I = 0 BEGIN
            SET @PrevPos = @Pos
            SET @Pos = CHARINDEX(@Delimiter, @Expression, @Pos + LEN(@Delimiter))
           
            SET @I = @I + 1
        END
       
        IF @Pos = 0 AND @I = @INDEX
            SET @RETURN = SUBSTRING(@Expression, @PrevPos + LEN(@Delimiter), LEN(@Expression))
        ELSE IF @Pos = 0 AND @I <> @INDEX
            SET @RETURN = NULL
        ELSE
            SET @RETURN = SUBSTRING(@Expression, @PrevPos + LEN(@Delimiter), @Pos - @PrevPos - LEN(@Delimiter))
    END
   
    RETURN @RETURN
END 

﻿
/******************************************************************************
**  Name: [dbo].[udfSafeDivision]
**  Purpose: 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control
******************************************************************************/
-- CREATE FUNCTION
CREATE FUNCTION [dbo].[udfSafeDivision] 
	(
		 @v1 decimal (10 , 4) 
		,@v2 decimal (10 , 4 )
	)

RETURNS

decimal (10 , 4)

AS

BEGIN

	declare
		@returnValue decimal ( 10 , 4 )

	if (@v2 = 0) -- No Division by Zero

	BEGIN
		select
			@returnValue = 0
	END

	else

	BEGIN
		select
			@returnValue =
				convert(
					 decimal (10 , 4)
					,(
						convert(
							 decimal (10 , 4)
							,@v1
						)
						/
						convert(
							 decimal (10 , 4)
							,@v2
						)
					)
				)
	end

return @returnValue


END

﻿/******************************************************************************
**  Name: [dbo].[udf_TreatType]
**  Purpose: RTT find if Admitted or Non-Admitted
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 11.08.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE FUNCTION [dbo].[udf_TreatType]
(
	@Type nvarchar(12), @tci_date datetime, @Wl_Status nvarchar(12), @treattype nvarchar(12), @diag nvarchar(12)
)
RETURNS nvarchar(12)
AS

BEGIN
Declare @strAdm nvarchar(12)

Set @strAdm = Case 
WHEN @Type = 'CLOSED' And @treattype='IP' THEN 'Admitted'
WHEN (@Type = 'CLOSED' And @treattype<>'IP') THEN 'Non Admitted'
WHEN  (@Type='OPEN' AND Not(@tci_date IS NULL) And @Wl_Status Not Like '%DIAG%')
or (Left(@Wl_Status,2)='WL' And @Wl_Status Not Like '%DIAG%') Then 'Admitted'
--WHEN  (@Type='OPEN' AND @Wl_Status Like '%DIAG%') Then 'Admitted diagnostic'
WHEN  (@Type='OPEN' AND @Wl_Status Like '%DIAG%' AND
 @diag = 'n') Then 'Non Admitted'
WHEN  (@Type='OPEN' AND @Wl_Status Like '%DIAG%' AND
 @diag != 'n') Then 'Non Admitted Diagnostic'
End


Return @strAdm

END


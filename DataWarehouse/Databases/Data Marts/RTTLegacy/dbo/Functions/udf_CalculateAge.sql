﻿/******************************************************************************
**  Name: [dbo].[udf_CalculateAge]
**  Purpose: 
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control
******************************************************************************/
CREATE FUNCTION [dbo].[udf_CalculateAge]
(
@DOB AS DATE,
@EndDate as DATE = '2999-01-01' -- Default is today's date (see below) but any date can be used here
)
RETURNS TINYINT
AS
	BEGIN
	DECLARE @Result as TINYINT

	-- IF DEFAULT VALUE (marked as 2999-01-01 as it doesn't accept functions) IS USED THEN USE TODAY'S DATE
	IF @EndDate = '2999-01-01'
		SET @EndDate = GETDATE()
		IF @DOB >= @EndDate -- trap errors
			SET @Result = 0
		ELSE
			BEGIN
			-- check if the person had its birthday in the specified year and calculate age
				IF (MONTH(@EndDate)*100)+DAY(@EndDate) >= (MONTH(@DOB)*100)+DAY(@DOB)
					SET @Result = DATEDIFF(Year,@DOB,@EndDate)
				ELSE
					SET @Result = DATEDIFF(Year,@DOB,@EndDate)-1
			END

	RETURN @Result

	END

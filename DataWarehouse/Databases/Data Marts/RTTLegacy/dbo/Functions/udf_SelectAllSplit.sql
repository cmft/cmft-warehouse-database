﻿

CREATE FUNCTION [dbo].[udf_SelectAllSplit]
	(
	 @List nvarchar(2000)
	,@SplitOn nvarchar(5)
	) 

RETURNS @RtnValue table
	(
	 Id int identity(1,1)
	,Value nvarchar(100)
	)

AS 

BEGIN

	while (Charindex(@SplitOn , @List) > 0)

	begin

		insert into @RtnValue
		(
			Value
		)

		select
			Value =
				ltrim(
					rtrim(
						substring(
							 @List
							,1
							,charindex(
								 @SplitOn
								,@List
							) -1 
						)
					)
				)

		set
			@List =
				substring(
					 @List
					,charindex(
						 @SplitOn
						,@List
					) + len(@SplitOn)
					,len(@List)
				)

	end

	insert into @RtnValue 
	(
		Value
	)

	select
		Value =
			ltrim(
				rtrim(
					@List
				)
			)

	return

END


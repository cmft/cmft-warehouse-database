﻿/******************************************************************************
**  Name: [dbo].[udf_PrevSunday]
**  Purpose: Return Last Sundays Date
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 11.08.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control
******************************************************************************/
CREATE FUNCTION [dbo].[udf_PrevSunday]
(
	@Today datetime
)
RETURNS Datetime
AS
BEGIN
Declare @PrevSunday Datetime

	set @PrevSunday = DATEADD(day,
               -1 - (DATEPART(dw, @Today) + @@DATEFIRST - 2) % 7,
               @Today
       )  
       --Ensure no time
set @PrevSunday = DATEADD(day, 0, DATEDIFF(day, 0, @PrevSunday))


Return @PrevSunday

END


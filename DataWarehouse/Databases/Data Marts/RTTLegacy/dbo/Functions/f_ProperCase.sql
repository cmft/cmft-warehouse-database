﻿
/******************************************************************************
**  Name: [dbo].[f_ProperCase]
**  Purpose: Convert input text to proper case
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control
** 22.11.13   MH          Modified to handle numbers at the start of the input string
******************************************************************************/

CREATE FUNCTION [dbo].[f_ProperCase](@InputText as varchar(512)) RETURNS varchar(512) as
BEGIN

DECLARE @Reset bit
	DECLARE @Ret varchar(512)
	DECLARE @i int
	DECLARE @c char(1)
	DECLARE @NumberPrefix VARCHAR(200)
	DECLARE @Text varchar(52)
	DECLARE @TextStart INT

	SELECT @Reset = 1, @i=1, @Ret = ''
	SET @TextStart = PATINDEX('%[A-Z]%',@InputText)
	SET @Text = SUBSTRING(@InputText,@TextStart,LEN(@InputText) - @TextStart + 1)
	
	WHILE @i <= LEN(@Text)
		SELECT @c= SUBSTRING(@Text,@i,1),
		@Ret = @Ret + CASE WHEN @Reset=1 THEN UPPER(@c) ELSE LOWER(@c) END,
		@Reset= 
			CASE WHEN 
				CASE WHEN SUBSTRING(@Text,@i-4,5) like '_[a-z] [DOL]''' THEN 1 
				WHEN SUBSTRING(@Text,@i-4,5) like '_[a-z] [D][I]' THEN 1 
				WHEN SUBSTRING(@Text,@i-4,5) like '_[a-z] [M][C]' THEN 1 
				ELSE 0 
				END = 1 
			THEN 1 
			ELSE 
				CASE WHEN @c like '[a-zA-Z]' or @c in ('''') THEN 0 
			ELSE 1 
			END 
	END,
	
	@i = @i +1
	
	RETURN SUBSTRING(@InputText,1, @TextStart -1) + @Ret

end

﻿/******************************************************************************
**  Name: [dbo].[udf_STL_OT]
**  Purpose: RTT find if STL
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 11.08.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
******************************************************************************/

Create FUNCTION [dbo].[udf_STL_OT]
(
	@Pathway_Start_Date datetime, @tci_fut_app_date datetime, @ReportMonth int
)
RETURNS int
AS
BEGIN
Declare @MM_Start Datetime, @MM_End Datetime, @STL_period_start Datetime, @STL_period_end datetime
Declare @intResult int

Set @MM_Start = (SELECT  Monitor_month_start
FROM         ManualAdjustment_rtt_PeriodUpdate
WHERE     (id = @ReportMonth))

Set @MM_End = (SELECT  Monitor_month_end
FROM         ManualAdjustment_rtt_PeriodUpdate
WHERE     (id = @ReportMonth))
Set @MM_End = DATEADD(day,1,@MM_End)


Set @STL_period_start = (SELECT  STL_period_start
FROM         ManualAdjustment_rtt_PeriodUpdate
WHERE     (id = @ReportMonth))

Set @STL_period_end = (SELECT  STL_period_end
FROM         ManualAdjustment_rtt_PeriodUpdate
WHERE     (id = @ReportMonth))
Set @STL_period_end = DateAdd(day,1,@STL_period_end)

Set @intResult = Case 
WHEN  (@Pathway_Start_Date>=@STL_period_start AND @Pathway_Start_Date<@STL_period_end)
AND (@tci_fut_app_date>=@MM_Start AND @tci_fut_app_date<@MM_End) Then 1
Else 0
End

Return @intResult

END


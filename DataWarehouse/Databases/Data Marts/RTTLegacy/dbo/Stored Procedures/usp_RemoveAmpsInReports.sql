﻿
/******************************************************************************
**  Name: [dbo].[usp_RemoveAmpsInReports]
**  Purpose: SSRS Report
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 15.08.11   Alan Bruce  Created
** 08.08.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_RemoveAmpsInReports]
AS
BEGIN
SET NOCOUNT ON;

update Backlog
set
	division =
		replace(division , '&' , 'and')

update SummaryData
set
	path_division =
		replace(path_division , '&' , 'and')

update ClosedSummaryData
set 
	path_division =
		replace(path_division , '&' , 'and')

update BacklogGraph 
set 
	division =
		replace(division , '&' , 'and')


--update dbo.Backlog set division = 'Women and Children'
--Where division = 'Women & Children'

--update dbo.Backlog set division = 'Clinical and Scientific'
--Where division = 'Clinical & Scientific'

--update dbo.SummaryData set path_division = 'Women and Children'
--Where path_division = 'Women & Children'

--update dbo.SummaryData set path_division = 'Clinical and Scientific'
--Where path_division = 'Clinical & Scientific'

--update dbo.ClosedSummaryData set path_division = 'Women and Children'
--Where path_division = 'Women & Children'

--update dbo.ClosedSummaryData set path_division = 'Clinical and Scientific'
--Where path_division = 'Clinical & Scientific'

--update dbo.BacklogGraph set division = 'Clinical and Scientific'
--Where division = 'Clinical & Scientific'

--update dbo.BacklogGraph set division = 'Women and Children'
--Where division = 'Women & Children'


END


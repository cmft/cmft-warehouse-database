﻿
/******************************************************************************
**  Name: [dbo].[usp_UpdateSummaryData]
**  Purpose: RTT Report Summary Report Data Update
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 28.08.11   Alan Bruce  Created
** 11.07.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_UpdateSummaryData]		
AS
BEGIN

declare
	@Extract datetime

set
	@Extract =
		(
		select top 1
			DATEDIFF(day, 0, PATHWAYS_all.datebefore)
		From
		Pathways_All
		)

--Tidy Tables in case the procedure has been run before

delete
from
	dbo.SummaryData
where
	ExtractDate = @Extract


delete
from
	dbo.ClosedSummaryData
where
	ExtractDate = @Extract



--*****************************
--Data for 4th Month Added temporarily.
--EXECUTE [dbo].[usp_PTL_STL_SummaryOT] 
--   5
--  ,127

--EXECUTE [dbo].[usp_PTL_STL_SummaryNT] 
--   5
--  ,162




--*****************************
--Add summary Data for PTL_STLSummaryReport
EXECUTE [dbo].[usp_PTL_STL_SummaryOT] 1 , 127
EXECUTE [dbo].[usp_PTL_STL_SummaryOT] 2 , 127
EXECUTE [dbo].[usp_PTL_STL_SummaryOT] 3 , 127
EXECUTE [dbo].[usp_PTL_STL_SummaryOT] 4 , 127

EXECUTE [dbo].[usp_PTL_STL_SummaryNT] 1 , 162
EXECUTE [dbo].[usp_PTL_STL_SummaryNT] 2 , 162
EXECUTE [dbo].[usp_PTL_STL_SummaryNT] 3 , 162
EXECUTE [dbo].[usp_PTL_STL_SummaryNT] 4 , 162


truncate table dbo.DailySpecialtyData

--Daily Specialty ADM


EXECUTE [dbo].[usp_Daily_Specialty_Adm] 1 , 162
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 2 , 162
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 3 , 162
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 4 , 162  
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 5 , 162
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 6 , 162 

EXECUTE [dbo].[usp_Daily_Specialty_Adm] 1 , 127
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 2 , 127
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 3 , 127
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 4 , 127
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 5 , 127
EXECUTE [dbo].[usp_Daily_Specialty_Adm] 6 , 127 

--Daily Spec Non Adm

EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 1 , 129
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 2 , 129
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 3 , 129
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 4 , 129
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 5 , 129
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 6 , 129

EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 1 , 127
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 2 , 127
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 3 , 127
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 4 , 127
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 5 , 127
EXECUTE [dbo].[usp_Daily_Specialty_NonAdm] 6 , 127

--Make a record of the extract date for report

INSERT INTO dbo.ExtractDates
(
	 ExtractDate
	,EDate
)

select top 1 
	 convert(nvarchar(12) , dateadd(day, -1 , [datebefore]) , 103)
	,EDate = CONVERT(datetime, datebefore)
from
	PATHWAYS_all

--Log it
INSERT INTO PathwaysImportLog
(
	UpdateDate
)

VALUES
(
	DATEADD(day , 0 , DATEDIFF(day , 0 , GETDATE()))
)


--Add closed summary Data for PTL_STLSummaryReport

EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 1 , 162 , N'Admitted'
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 2 , 162 , N'Admitted'  
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 3 , 162 , N'Admitted'
  
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 1 , 129 , N'Non Admitted'
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 2 , 129 , N'Non Admitted'  
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 3 , 129 , N'Non Admitted'

EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 1 , 127 , N'Admitted'
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 2 , 127 , N'Admitted'  
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 3 , 127 , N'Admitted'

EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 1 , 127 , N'Non Admitted'
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 2 , 127 , N'Non Admitted'  
EXECUTE [dbo].[usp_PTL_STL_SummaryClosed] 3 , 127 , N'Non Admitted'



--Set reportMonth period for reporting Month drop down in reports

--Refactor!

declare
	 @MnthYr nvarchar(6)
	,@ExtractDateId int

set
	@ExtractDateId =
		(
		select top 1
			id 
		from
			dbo.ExtractDates
		order by
			id desc
		)

set
	@MnthYr =
		(
		SELECT
			MonthYr
		from
			ManualAdjustment_rtt_Period
		WHERE
			id = 1
		)

INSERT INTO ReportPeriod
(
	 ExtractDateId
	,Monthyr
)

select
	 @ExtractDateId
	,@MnthYr

set
	@MnthYr = 
		(
		SELECT
			MonthYr
		FROM
			ManualAdjustment_rtt_Period
		WHERE
			id = 2
		)

INSERT INTO ReportPeriod
(
	 ExtractDateId
	,Monthyr
)

select
	 @ExtractDateId
	,@MnthYr


set
	@MnthYr =
		(
		SELECT
			MonthYr
		FROM
			ManualAdjustment_rtt_Period
		WHERE
			id = 3
		)


INSERT INTO ReportPeriod
(
	 ExtractDateId
	,Monthyr
)

select
	 @ExtractDateId
	,@MnthYr


set
	@MnthYr =
		(
		SELECT
			MonthYr
		FROM
			ManualAdjustment_rtt_Period
		WHERE
			id = 4
		)


INSERT INTO ReportPeriod
(
	 ExtractDateId
	,Monthyr
)

select
	 @ExtractDateId
	,@MnthYr
END

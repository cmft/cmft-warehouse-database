﻿
/******************************************************************************
**  Name: [dbo].[usp_AllOpenSummaryReport]
**  Purpose: RTT Report All Open Pathways Summary Report
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 13.02.12   Alan Bruce  Created
** 09.07.12   CB          Refactored for readability
**                        Altered Division filtering technique
**               		  Added 52 Week Measures
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_AllOpenSummaryReport]
	 @Division nvarchar(1000)
AS

BEGIN	

declare
	 @AdmittedTarget int = 126
	,@NonAdmittedTarget int = 126
                   
                            
select 
	 a.division
	,a.[speciality]
	,a.path_spec
	,a.[Admitted]
	,a.[Admitted Over 18 Weeks]
	,a.[Admitted Over 52 Weeks]
	,a.[Non-Admitted]
	,a.[Non-Admitted Over 18 Weeks]
	,a.[Non-Admitted Over 52 Weeks]
	,a.WeekNo
	,a.ExtractDate

	,[% Admitted under 18 Weeks] = 
		convert(
			 decimal(5,4)
			,1 - dbo.udfSafeDivision([Admitted Over 18 Weeks] , [Admitted])
		)

	,[% Non-Admitted under 18 Weeks] =
		convert(
			 decimal(5,4)
			,1 - dbo.udfSafeDivision([Non-Admitted Over 18 Weeks] , [Non-Admitted])
		)

	,[% Total under 18 Weeks] =
		convert(
			 decimal(5,4)
			,1 - dbo.udfSafeDivision([Admitted Over 18 Weeks] + [Non-Admitted Over 18 Weeks] , Admitted + [Non-Admitted])
		)

	,[Performance] =
		convert(
			 decimal(5,4)
			,dbo.udfSafeDivision([Admitted Over 18 Weeks] + [Non-Admitted Over 18 Weeks],Admitted + [Non-Admitted])
		)
from
	(
	select
		 division
		,[speciality]
		,path_spec

		,[Admitted] =
			SUM(
				CASE IPorOP 
				WHEN 'Admitted' THEN 1 
				ELSE 0 
				END
			)

		,[Admitted Over 18 Weeks] = 
			SUM(
				CASE 
				WHEN IPorOP = 'Admitted' AND [days] > @AdmittedTarget THEN 1
				ELSE 0 
				END
			)

		,[Admitted Over 52 Weeks] = 
			SUM(
				CASE 
				WHEN IPorOP = 'Admitted' AND [days] > 365 THEN 1
				ELSE 0 
				END
			)
		
		,[Non-Admitted] =
			SUM(
				CASE IPorOP 
				WHEN 'Non-Admitted' THEN 1 
				ELSE 0 
				END
			)

		,[Non-Admitted Over 18 Weeks] =
			SUM(
				CASE 
				WHEN IPorOP = 'Non-Admitted' AND [days] > @NonAdmittedTarget THEN 1 
				ELSE 0 
				END
			)

		,[Non-Admitted Over 52 Weeks] =
			SUM(
				CASE 
				WHEN IPorOP = 'Non-Admitted' AND [days] > 365 THEN 1 
				ELSE 0 
				END
			)

		,WeekNo
		,ExtractDate
	FROM
		[AllOpenPathways] 

	--Where
	--	AllOpenPathways.Division IN
	--		(
	--		SELECT
	--			Value
	--		FROM
	--			[dbo].[udf_SelectAllSplit](@Division , ',') AS Split_1
	--		)

	inner join
		(
		SELECT
			Value
		FROM
			dbo.udf_SelectAllSplit(@Division , ',')
		) Division
	on	Division.Value = AllOpenPathways.division

	Group By
		 division
		,[speciality]
		,path_spec
		,WeekNo
		,ExtractDate
	) a


END




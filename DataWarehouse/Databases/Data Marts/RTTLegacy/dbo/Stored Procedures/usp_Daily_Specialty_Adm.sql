﻿/******************************************************************************
**  Name: [dbo].[usp_Daily_Specialty_Adm]
**  Purpose: RTT Report Summary
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.10.11   Alan Bruce  Created
** 10.07.12   CB          Refactored for readability
**                        Bug fix in WHERE clause:  missing brackets around OR conditions
**                        Refactored WHERE clause
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment.rtt.Period
** 25.10.12   MH          Replacement of udf_PTL_open, udf_STL_Open, udf_STL_Open_OT, udf_PTL_Open_OT
**                        with udf_Open_OT and udf_OT for efficiency gains
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_Daily_Specialty_Adm]
	 @ReportMonth int
	,@Breach_Days int
	
AS

BEGIN

Declare
	 @MM_Start Datetime
	,@MM_End Datetime
	,@MnthYr nvarChar(20)


select
	 @MM_Start =
		(
		SELECT
			Monitor_month_start
		FROM
			ManualAdjustment_rtt_Period
		WHERE
			id = @ReportMonth
		)

	,@MM_End = 
		DATEADD(
			 day
			,1
			,
			(
			SELECT
				Monitor_month_end
			FROM
				ManualAdjustment_rtt_Period
			WHERE
				id = @ReportMonth
			)
		)

	,@MnthYr = 
		(
		SELECT
			MonthYr
		FROM
			ManualAdjustment_rtt_Period
		WHERE
			id = @ReportMonth
		)

--set
--	@MM_End =
--		DATEADD(day , 1 , @MM_End)

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	
SET NOCOUNT ON;

INSERT INTO dbo.DailySpecialtyData 
(
	 Hospital
	,InternalNo
	,ReferralDate
	,EpisodeNo
	,PriorityType
	,DischDate
	,RefSource
	,EpiRefBy
	,EpiCode
	,Category
	,RefReasonCode
	,ConType
	,Consultant
	,Specialty
	,doh_spec
	,EpPostCode
	,CaseNoteNo
	,DistrictNo
	,WLTreatment
	,WLDate
	,WLApptType
	,Summary1
	,Summary2
	,DecToRefDate
	,DischargeReason
	,pathway_updated
	,pathway_start_date_original
	,pathway_start_date_current
	,pathway_treat_by_date
	,days_to_treatment
	,pathway_status
	,pathway_end_date
	,pathway_end_reason
	,pathway_future_reset_date
	,Decision_date_to_treat_in_IP
	,OP_treat_date
	,pathway_ID
	,last_att_date
	,last_att_status
	,last_appt_date
	,last_appt_status
	,fut_appt_date
	,fut_appt_status
	,WL_Hospital
	,WL_EpisodeNo
	,WL_WLDate
	,WL_WLCode
	,WL_IntMan
	,WL_MethAdm
	,WL_RemReason
	,WL_RemDate
	,WL_Cons
	,WL_Spec
	,WL_doh_spec
	,WL_Category
	,WL_PrimProc
	,WL_orig_date
	,WL_admitted
	,WL_lastRev
	,WL_status
	,tci_date
	,tci_outcome
	,tci_canreas
	,path_division
	,path_directorate
	,path_cons
	,path_spec
	,path_doh_spec
	,path_doh_desc
	,pathway_open
	,pathway_closed
	,pathway_not18wks
	,pathway_open_days
	,pathway_closed_days
	,pathway_open_weeks
	,pathway_closed_weeks
	,path_closed_wks_DNA_adjs
	,path_closed_days_DNA_adjs
	,path_open_wks_DNA_adjs
	,path_open_days_DNA_adjs
	,sus_days_rem
	,datebefore
	,surname
	,forename
	,init
	,rad_casenote
	,rad_on_wl
	,rad_exam_date
	,op_act
	,ip_act
	,ContractId
	,currentContractId
	,type
	,pct_old
	,pct_new
	,days_to_breach
	,bandbreach
	,sent
	,treattype
	,clos
	,exc
	,daysrem
	,adjdays
	,sus_days_pat
	,diag
	,tci_fut_app_date
	,disn
	,pat_strst
	,DA_audiology
	,anap
	,OP_plan
	,TCI_canc
	,[Pause?]
	,[Pause Start Date]
	,RTT_Pathway_ID
	,RTT_Pathway_ID_2
	,P2WW
	,OldTreatByDate
	,Treat
	,Total_PTL
	,Total_STL
	,[PTL<=23]
	,[PTL>23]
	,[STL<=23]
	,[STL>23]
	,ExtractDate
	,[Month Selected]
	,[Days To Breach]
	,SpecDesc
	,Specgroup
	,Specname
)                     
SELECT
	 Hospital
	,InternalNo
	,ReferralDate
	,EpisodeNo
	,PriorityType
	,DischDate
	,RefSource
	,EpiRefBy
	,EpiCode
	,Category
	,RefReasonCode
	,ConType
	,Consultant
	,Pathways_All.Specialty
	,doh_spec
	,EpPostCode
	,CaseNoteNo
	,DistrictNo
	,WLTreatment
	,WLDate
	,WLApptType
	,Summary1
	,Summary2
	,DecToRefDate
	,DischargeReason
	,pathway_updated
	,pathway_start_date_original
	,pathway_start_date_current
	,pathway_treat_by_date
	,days_to_treatment
	,pathway_status
	,pathway_end_date
	,pathway_end_reason
	,pathway_future_reset_date
	,Decision_date_to_treat_in_IP
	,OP_treat_date
	,pathway_ID
	,last_att_date
	,last_att_status
	,last_appt_date
	,last_appt_status
	,fut_appt_date
	,fut_appt_status
	,WL_Hospital
	,WL_EpisodeNo
	,WL_WLDate
	,WL_WLCode
	,WL_IntMan
	,WL_MethAdm
	,WL_RemReason
	,WL_RemDate
	,WL_Cons
	,WL_Spec
	,WL_doh_spec
	,WL_Category
	,WL_PrimProc
	,WL_orig_date
	,WL_admitted
	,WL_lastRev
	,WL_status
	,tci_date
	,tci_outcome
	,tci_canreas
	,path_division
	,path_directorate
	,path_cons
	,path_spec
	,path_doh_spec
	,path_doh_desc
	,pathway_open
	,pathway_closed
	,pathway_not18wks
	,pathway_open_days
	,pathway_closed_days
	,pathway_open_weeks
	,pathway_closed_weeks
	,path_closed_wks_DNA_adjs
	,path_closed_days_DNA_adjs
	,path_open_wks_DNA_adjs
	,path_open_days_DNA_adjs
	,sus_days_rem
	,datebefore
	,surname
	,forename
	,init
	,rad_casenote
	,rad_on_wl
	,rad_exam_date
	,op_act
	,ip_act
	,ContractId
	,currentContractId
	,type
	,pct_old
	,pct_new
	,days_to_breach
	,bandbreach
	,sent
	,treattype
	,clos
	,exc
	,daysrem
	,adjdays
	,sus_days_pat
	,diag
	,tci_fut_app_date
	,disn
	,pat_strst
	,DA_audiology
	,anap
	,OP_plan
	,TCI_canc
	,[Pause?]
	,[Pause Start Date]
	,RTT_Pathway_ID
	,RTT_Pathway_ID_2
	,P2WW
	,OldTreatByDate

	,Treat =
		dbo.udf_TreatType([Type] , [tci_date] , [WL_status] , [treattype] , [diag])

	,[Total_PTL] = CASE WHEN OPENOT.OpenOTType = 'PTLOPEN' THEN 1 ELSE 0 END
	,[Total_STL] = CASE WHEN OPENOT.OpenOTType = 'STLOPEN' THEN 1 ELSE 0 END	
	
	,[PTL<=23] = 
		CASE 
		WHEN 
				OT.OTType = 'PTL'  
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) < @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	,[PTL>23] =
		CASE 
		WHEN 
				OT.OTType = 'PTL' 
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) >= @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	,[STL<=23] = 
		CASE 
		WHEN 
				OT.OTType = 'STL' 
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) < @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	,[STL>23] = 
		CASE 
		WHEN 
				OT.OTType = 'STL'  
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) >= @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	--,[Total_PTL] =
	--	CASE
	--	WHEN dbo.udf_PTL_Open([pathway_start_date_current] , PATHWAYS_all.[type] , @ReportMonth) = 1 THEN 1
	--	ELSE 0
	--	END

	--,[Total_STL] =
	--	CASE
	--	WHEN dbo.udf_STL_Open([pathway_start_date_current] , PATHWAYS_all.[type] , @ReportMonth) = 1 THEN 1
	--	ELSE 0
	--	END

	--,[PTL<=23] =
	--	CASE
	--	WHEN
	--			dbo.udf_PTL([pathway_start_date_current] , [tci_fut_app_date] , @ReportMonth) = 1
	--		AND convert(int , (DateDiff(day , [pathway_start_date_current] , [tci_fut_app_date]) - ISNull([adjdays] , 0))) < @Breach_Days
	--		THEN 1
	--	ELSE 0
	--	END

	--,[PTL>23] =
	--	CASE
	--	WHEN 
	--			dbo.udf_PTL([pathway_start_date_current] , [tci_fut_app_date] , @ReportMonth) = 1
	--		AND convert(int , (DateDiff(day , [pathway_start_date_current] , [tci_fut_app_date]) - ISNull([adjdays], 0))) >= @Breach_Days
	--		THEN 1
	--	ELSE 0
	--	END

	--,[STL<=23] =
	--	CASE
	--	WHEN
	--			dbo.udf_STL([pathway_start_date_current] , [tci_fut_app_date] , @ReportMonth) = 1
	--		AND convert(int , (DateDiff(day , [pathway_start_date_current] , [tci_fut_app_date]) - ISNull([adjdays], 0))) < @Breach_Days
	--		THEN 1
	--	ELSE 0
	--	END

	--,[STL>23] =
	--	CASE
	--	WHEN
	--			dbo.udf_STL([pathway_start_date_current] , [tci_fut_app_date] , @ReportMonth) = 1
	--		AND convert(int , (DateDiff(day , [pathway_start_date_current] , [tci_fut_app_date]) - ISNull([adjdays] , 0))) >= @Breach_Days
	--		THEN 1
	--	ELSE 0
	--	END

	,ExtractDate =
		DATEADD(day, 0, DATEDIFF(day, 0, [datebefore]))

	,[Month Selected] = @MnthYr
	,[Days To Breach] = @Breach_Days
	,SpecDesc = WarehouseReporting.ETL.BasePASSpecialty.Specialty

	,Specgroup =
		CASE
		WHEN Specs.Spec IS NULL or path_division = 'Childrens' THEN '999'
		ELSE Specs.Spec
		END

	,Specname =
		CASE
		when Specs.Spec IS NULL or path_division = 'Childrens' THEN 'other'
		ELSE [desc]
		END

FROM
	PATHWAYS_all

LEFT OUTER JOIN Specs
ON PATHWAYS_all.path_doh_spec = Specs.spec

LEFT OUTER JOIN WarehouseReporting.ETL.BasePASSpecialty
ON PATHWAYS_all.path_spec = WarehouseReporting.ETL.BasePASSpecialty.SpecialtyCode

OUTER APPLY dbo.udf_Open_OT ([pathway_start_date_current],PATHWAYS_all.[type] , @ReportMonth) OPENOT
OUTER APPLY dbo.udf_OT ([pathway_start_date_current],[tci_fut_app_date], @ReportMonth) OT

WHERE
	PATHWAYS_all.path_division NOT LIKE 'MH%'
AND (
		PATHWAYS_all.diag IS NULL 
	OR	PATHWAYS_all.diag = 'I'
	OR	PATHWAYS_all.diag = 'N' 
	OR	PATHWAYS_all.diag = ''
	)
AND (
		PATHWAYS_all.exc IS NULL 
	OR	PATHWAYS_all.exc = ''
	)
AND PATHWAYS_all.pathway_start_date_original >= '1/1/2007'
AND PATHWAYS_all.path_doh_spec <> N'650' 
AND PATHWAYS_all.path_doh_spec <> N'950' 
AND PATHWAYS_all.path_doh_spec <> N'960'
and	(
		(
			PATHWAYS_all.type LIKE 'OPEN'
		AND PATHWAYS_all.WL_status LIKE 'WL%'
		AND PATHWAYS_all.WL_status NOT LIKE '%DIAG%'
		AND (
				PATHWAYS_all.WL_status <> 'PLANNED' 
			OR	PATHWAYS_all.WL_status IS NULL
			) 
		)
	OR	(
			PATHWAYS_all.type = 'CLOSED'
		AND PATHWAYS_all.pathway_end_date >= @MM_Start
		AND PATHWAYS_all.pathway_end_date < @MM_End
		AND dbo.udf_TreatType([Type] , [tci_date] , [WL_status] , [treattype] , [diag]) = 'Admitted'
		)
	)

--where
--	(
--		PATHWAYS_all.path_division NOT LIKE 'MH%'
--	AND PATHWAYS_all.type LIKE 'OPEN'
--	AND PATHWAYS_all.WL_status LIKE 'WL%'
--	AND PATHWAYS_all.WL_status NOT LIKE '%DIAG%'
--	AND (
--			PATHWAYS_all.WL_status <> 'PLANNED' 
--		OR	PATHWAYS_all.WL_status IS NULL
--		) 
--	AND (
--			PATHWAYS_all.diag IS NULL 
--		OR	PATHWAYS_all.diag = 'I'
--		OR	PATHWAYS_all.diag = 'N' 
--		OR	PATHWAYS_all.diag = ''
--		)
--	AND (
--			PATHWAYS_all.exc IS NULL 
--		OR	PATHWAYS_all.exc = ''
--		)
--	AND PATHWAYS_all.pathway_start_date_original >= '1/1/2007'
--	AND PATHWAYS_all.path_doh_spec <> N'650' 
--	AND PATHWAYS_all.path_doh_spec <> N'950' 
--	AND PATHWAYS_all.path_doh_spec <> N'960'
--	)
--OR
--	(
--		PATHWAYS_all.path_division NOT LIKE 'MH%'
--	AND PATHWAYS_all.type = 'CLOSED'
--	AND (
--			PATHWAYS_all.diag IS NULL 
--		OR	PATHWAYS_all.diag = 'I' 
--		OR	PATHWAYS_all.diag = 'N'
--		OR	PATHWAYS_all.diag = ''
--		)
--	AND (
--			PATHWAYS_all.exc IS NULL 
--		OR	PATHWAYS_all.exc = ''
--		)
--	AND PATHWAYS_all.pathway_start_date_original >= '1/1/2007'
--	AND PATHWAYS_all.path_doh_spec <> N'650'
--	AND PATHWAYS_all.path_doh_spec <> N'950'
--	AND PATHWAYS_all.path_doh_spec <> N'960'
--	AND PATHWAYS_all.pathway_end_date >= @MM_Start
--	AND PATHWAYS_all.pathway_end_date < @MM_End
--	AND dbo.udf_TreatType([Type] , [tci_date] , [WL_status] , [treattype] , [diag]) = 'Admitted'
--	)

END




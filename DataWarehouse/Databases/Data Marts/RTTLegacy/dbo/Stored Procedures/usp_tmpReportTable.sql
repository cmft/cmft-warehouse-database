﻿
/******************************************************************************
**  Name: [dbo].[usp_tmpReportTable]
**  Purpose: Create table tmpReportData
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control
** 19.10.12   MH          Specify the column order on the insert statements and clear
**                        out table for rows with the derived ExtractDate before Inserting
******************************************************************************/
CREATE PROCEDURE [dbo].[usp_tmpReportTable] 
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @ExtractDate nvarchar(12)

Set @ExtractDate = (Select Top 1 Convert(nvarchar(12),datebefore,103) From PATHWAYS_all)

-- MH 19.10.12 : Delete by this extract date to allow for re-running
DELETE FROM dbo.tmpReportData WHERE ExtractDate = @ExtractDate

--Truncate Table dbo.tmpReportData

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division) 
SELECT     'No of Non Admitted patients who passed their breach date in the last 7 days' AS Description,COUNT(*) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM         PATHWAYS_all_PrevWeek
WHERE     ((OldTreatByDate > DATEADD(day, - 7, CONVERT(Date, WeekEnd, 103))) AND (type = 'Open') OR
                      (OldTreatByDate < CONVERT(Date, pathway_end_date, 103)) AND (type = 'Closed')) AND ExtractDate = @ExtractDate
GROUP BY CONVERT(Date, WeekEnd, 103), IPorOP, division
HAVING      (IPorOP = 'Non-Admitted')

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT     'No of Admitted patients who passed their breach date in the last 7 days' As Description, COUNT(*) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM         PATHWAYS_all_PrevWeek
WHERE     ((OldTreatByDate > DATEADD(day, - 7, CONVERT(Date, WeekEnd, 103))) AND (type = 'Open') OR
                      (OldTreatByDate < CONVERT(Date, pathway_end_date, 103)) AND (type = 'Closed')) AND ExtractDate = @ExtractDate
GROUP BY CONVERT(Date, WeekEnd, 103), IPorOP, division
HAVING      (IPorOP = 'Admitted')

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 
'Open admitted pathways <18 Weeks without TCI date' As Description, Count(Backlog_current.InternalNo) - Sum(Case When [tci_date] > Dateadd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)<=126 OR Backlog_current.days is null)) AND IPorOP = 'Admitted' AND ExtractDate = @ExtractDate
GROUP BY  CONVERT(Date,WeekEnd,103),IPorOP, division;

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 
'Open admitted pathways <18 Weeks with a TCI date' As Description, Sum(Case When [tci_date] > Dateadd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End) AS Total,
 CONVERT(Date,WeekEnd,103) AS 'Weekend', IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)<=126 OR Backlog_current.days is null)) AND IPorOP = 'Admitted' AND ExtractDate = @ExtractDate
GROUP BY  CONVERT(Date,WeekEnd,103),IPorOP, division;

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 
'Open admitted pathways >18 Weeks without TCI date' As Description, Count(Backlog_current.InternalNo) - Sum(Case When [tci_date] > Dateadd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)>126)) AND IPorOP = 'Admitted' AND ExtractDate = @ExtractDate
GROUP BY  CONVERT(Date,WeekEnd,103),IPorOP, division;

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 
'Open admitted pathways >18 Weeks with a TCI date' As Description, Sum(Case When [tci_date] > Dateadd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End) AS Total,
 CONVERT(Date,WeekEnd,103) AS 'Weekend', IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)>126)) AND IPorOP = 'Admitted' AND ExtractDate = @ExtractDate
GROUP BY  CONVERT(Date,WeekEnd,103),IPorOP, division;

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT     'Patients Treated under 18 weeks' AS Description,COUNT(*) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM         PATHWAYS_all_PrevWeek
WHERE     (pathway_end_date >= DateAdd(day,-7,CONVERT(Date, WeekEnd, 103)) AND pathway_end_date <= CONVERT(Date, WeekEnd, 103))
AND (OldTreatByDate > CONVERT(Date, pathway_end_date, 103)) AND (type = 'Closed') AND ExtractDate = @ExtractDate
AND     (IPorOP = 'Admitted') 
GROUP BY CONVERT(Date, WeekEnd, 103), IPorOP, division


Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT     'Patients Treated over 18 weeks' AS Description,COUNT(*) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM         PATHWAYS_all_PrevWeek
WHERE     (pathway_end_date >= DateAdd(day,-7,CONVERT(Date, WeekEnd, 103)) AND pathway_end_date <= CONVERT(Date, WeekEnd, 103)) AND ExtractDate = @ExtractDate
AND (OldTreatByDate < CONVERT(Date, pathway_end_date, 103)) AND (type = 'Closed')
AND     (IPorOP = 'Admitted') 
GROUP BY CONVERT(Date, WeekEnd, 103), IPorOP, division



--********************************
--Open Non-admitted pathways <18 Weeks without  a future appointment date

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 'Open Non-admitted pathways < 18 Weeks without a future appointment date' AS Description, 
Count(Backlog_current.InternalNo) -(
Sum(Case When [fut_appt_date] > DateAdd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End)) As Total,CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)<=126 OR Backlog_current.days is null) AND IPorOP = 'Non-Admitted') AND ExtractDate = @ExtractDate
GROUP BY Backlog_current.IPorOP, CONVERT(Date,WeekEnd,103), division;

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 'Open Non-admitted pathways < 18 Weeks with a future appointment date' AS Description, 
Sum(Case When [fut_appt_date] > DateAdd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End) As Total,CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)<=126 OR Backlog_current.days is null) AND IPorOP = 'Non-Admitted') AND ExtractDate = @ExtractDate
GROUP BY Backlog_current.IPorOP, CONVERT(Date,WeekEnd,103), division;

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 'Open Non-admitted pathways > 18 Weeks without a future appointment date' AS Description, 
Count(Backlog_current.InternalNo) -(
Sum(Case When [fut_appt_date] > DateAdd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End)) As Total,CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)>126) AND IPorOP = 'Non-Admitted') AND ExtractDate = @ExtractDate
GROUP BY Backlog_current.IPorOP, CONVERT(Date,WeekEnd,103), division;


Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT 'Open Non-admitted pathways > 18 Weeks with a future appointment date' AS Description, 
Sum(Case When [fut_appt_date] > DateAdd(day,1,CONVERT(Date,WeekEnd,103))
Then 1
Else 0
End) As Total,CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM Backlog_current
WHERE (((Backlog_current.days)>126) AND IPorOP = 'Non-Admitted') AND ExtractDate = @ExtractDate
GROUP BY Backlog_current.IPorOP, CONVERT(Date,WeekEnd,103), division;



--*******************************************************


Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT     'Patients Treated under 18 weeks' AS Description,COUNT(*) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM         PATHWAYS_all_PrevWeek
WHERE     (pathway_end_date >= DateAdd(day,-7,CONVERT(Date, WeekEnd, 103)) AND pathway_end_date <= CONVERT(Date, WeekEnd, 103))
AND (OldTreatByDate > CONVERT(Date, pathway_end_date, 103)) AND (type = 'Closed')
AND     (IPorOP = 'Non-Admitted') AND ExtractDate = @ExtractDate
GROUP BY CONVERT(Date, WeekEnd, 103), IPorOP, division

Insert Into dbo.tmpReportData (Description, Total, Weekend, IPorOP, ExtractDate, Division)
SELECT     'Patients Treated over 18 weeks' AS Description,COUNT(*) AS Total, CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, @ExtractDate AS ExtractDate, division
FROM         PATHWAYS_all_PrevWeek
WHERE     (pathway_end_date >= DateAdd(day,-7,CONVERT(Date, WeekEnd, 103)) AND pathway_end_date <= CONVERT(Date, WeekEnd, 103))
AND (OldTreatByDate < CONVERT(Date, pathway_end_date, 103)) AND (type = 'Closed')
AND     (IPorOP = 'Non-Admitted') AND ExtractDate = @ExtractDate
GROUP BY CONVERT(Date, WeekEnd, 103), IPorOP, division


INSERT INTO dbo.tmpReportDataSpecialty ([Doh_Spec]
      ,[Total]
      ,[Weekend]
      ,[IPorOP]
      ,[ExtractDate]
)
SELECT   Doh_Spec,Count(*) as Total,
CONVERT(Date, WeekEnd, 103) AS Weekend, IPorOP, ExtractDate
FROM Backlog_current
WHERE ((Backlog_current.days)>126) 
GROUP BY  Doh_Spec, CONVERT(Date,WeekEnd,103),IPorOP, ExtractDate
Order By Doh_Spec

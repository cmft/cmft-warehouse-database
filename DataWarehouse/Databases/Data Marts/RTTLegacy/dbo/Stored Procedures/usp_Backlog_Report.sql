﻿/******************************************************************************
**  Name: [dbo].[usp_Backlog_Report]
**  Purpose: RTT Report Backlog Detail Report
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 03.11.11   Alan Bruce  Created
** 11.07.12   CB          Refactored for readability
** 09.08.12   CB          Modified to return 52 week waiters
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_Backlog_Report]
	 @IPorOP nvarchar(15)
	,@BreachTarget nvarchar(25)		
AS

DECLARE @ExtractDates TABLE
(
	ExtDate nvarchar(20) 
)

INSERT INTO @ExtractDates
(
	ExtDate
) 

select
	a.ExtractDate
from
	(
	SELECT distinct
		ExtractDate
	FROM
		Backlog
	) a

ORDER BY
	CONVERT(Date , ExtractDate , 103)

BEGIN	

IF @BreachTarget = '18 Weeks' 
begin

	SELECT
		 id
		,ExtractDate
		,WeekEnd
		,IPorOP
		,division
		,TMP
		,NewP =ISNULL(NewP,'-')
		,BreachDays
	from
		Backlog
	where
		IPorOP = @IPorOP 
	AND NT = 0 
	--AND BreachDays > 126 

	--CCB 2012-08-09 this is the adjusted wait
	and path_open_days_DNA_adjs - ISNULL(adjdays,0) > 126

	AND ExtractDate IN 
			(
			Select Top 11 
				ExtDate 
			From 
				@ExtractDates 
			ORDER BY
				CONVERT(Date, ExtDate, 103) DESC
			)
	ORDER BY
		CONVERT(Date, ExtractDate, 103) 
End

IF @BreachTarget = '52 Weeks' 
begin

	SELECT
		 id
		,ExtractDate
		,WeekEnd
		,IPorOP
		,division
		,TMP
		,NewP =ISNULL(NewP,'-')
		,BreachDays
	from
		Backlog
	where
		IPorOP = @IPorOP 
	AND NT = 0 
	--AND BreachDays > 126 

	--CCB 2012-08-09 this is the adjusted wait
	and path_open_days_DNA_adjs - ISNULL(adjdays,0) > 365
	AND ExtractDate IN 
			(
			Select Top 11 
				ExtDate 
			From 
				@ExtractDates 
			ORDER BY
				CONVERT(Date, ExtDate, 103) DESC
			)
	ORDER BY
		CONVERT(Date, ExtractDate, 103) 
End


--IF @BreachTarget = '23 Weeks' AND @IPorOP = 'Admitted'
--begin

--	SELECT     
--		 id
--		,ExtractDate
--		,WeekEnd
--		,IPorOP
--		,division
--		,TMP
--		,NewP = ISNULL(NewP,'-')
--		,BreachDays
--	FROM         
--		Backlog
--	where 
--		IPorOP = 'Admitted' 
--	AND BreachDays = 162 
--	AND ExtractDate IN 
--			(
--			Select Top 11 
--				ExtDate 
--			From 
--				@ExtractDates 
--			ORDER BY 
--				CONVERT(Date, ExtDate, 103) DESC
--			)
--	ORDER BY 
--		CONVERT(Date, ExtractDate, 103) 

--End


--IF @BreachTarget = '23 Weeks' AND @IPorOP = 'Non-Admitted'

--begin

--	SELECT     
--		 id
--		,ExtractDate
--		,WeekEnd
--		,IPorOP
--		,division
--		,TMP
--		,NewP = ISNULL(NewP,'-')
--		,BreachDays
--	FROM         
--		Backlog
--	where 
--		IPorOP = 'Non-Admitted' 
--	AND BreachDays > 128 
--	AND ExtractDate IN
--			(
--			select Top 11 
--				ExtDate 
--			From 
--				@ExtractDates 
--			ORDER BY 
--				CONVERT(Date, ExtDate, 103) DESC
--			)
--	ORDER BY
--		CONVERT(Date, ExtractDate, 103) 

--End

END



﻿
/******************************************************************************
**  Name: [dbo].[usp_PopulateAllOpenPathways]
**  Purpose: RTT Report Summary
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.10.11   Alan Bruce  Created
** 20.07.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control and use ManualAdjustment_rtt_BacklogComments synonym
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PopulateAllOpenPathways]
	
AS

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with select statements.
SET NOCOUNT ON;
	
truncate table dbo.allOpenPathways

SET DATEFIRST 1

INSERT INTO AllOpenPathways
(
	 STL
	,[STL 18]
	,division
	,directorate
	,path_spec
	,speciality
	,IPorOP
	,Wks
	,days
	,[Treat By]
	,path_open_wks_DNA_adjs
	,days_to_treatment
	,DistrictNo
	,init
	,psd
	,outpat_hosp
	,Specialty
	,outpat_cons
	,RefSource
	,RefDate
	,lad
	,last_att_status
	,fad
	,fut_appt_status
	,WL_Hospital
	,WL_Spec
	,WL_Cons
	,WLWLD
	,WL_WLCode
	,WL_status
	,WL_PrimProc
	,[WL IntMan]
	,tcid
	,db
	,Comment
	,Comment2
	,InternalNo
	,EpisodeNo
	,CaseNoteNo
	,pathway_start_date_current
	,pathway_treat_by_date
	,Decision_date_to_treat_in_IP
	,WL_EpisodeNo
	,WL_WLDate
	,surname
	,forename
	,tci_fut_app_date
	,TreatBy18Wk
	,pct_old
	,pct_new
	,WeekNo
	,ExtractDate
	,[tci_canreas]
)

select     
	 STL =
		case
		when [Decision_date_to_treat_in_IP] IS NULL then substring(convert(varchar(11), (dateadd(day, 128, [pathway_start_date_current])), 113), 4, 8) 
		else substring(convert(varchar(11), (dateadd(day, 161, [pathway_start_date_current])), 113), 4, 8) 
		end

	,[STL 18] =
		substring(convert(varchar(11), DATEADD(day, 126, PATHWAYS_all.pathway_start_date_current), 113), 4, 8)

	,division = PATHWAYS_all.path_division
	,directorate = PATHWAYS_all.path_directorate
	,path_spec = PATHWAYS_all.path_doh_spec + ' ' + PATHWAYS_all.path_doh_desc
	,speciality = PATHWAYS_all.path_doh_spec

	,IPorOP =
		case
		when Decision_date_to_treat_in_IP IS NULL then 'Non-Admitted' 
		else 'Admitted' 
		end

	,Wks = 
		PATHWAYS_all.path_open_wks_DNA_adjs - convert(integer , PATHWAYS_all.adjdays / 7)

	,days = 
		PATHWAYS_all.path_open_days_DNA_adjs - isnull(PATHWAYS_all.adjdays, 0)

	,[Treat By] =
		case
		when [Decision_date_to_treat_in_IP] IS NULL then (dateadd(day, 128, [pathway_start_date_current])) 
		else (dateadd(day, 161, [pathway_start_date_current])) 
		end

	,PATHWAYS_all.path_open_wks_DNA_adjs
	,PATHWAYS_all.days_to_treatment
	,PATHWAYS_all.DistrictNo

	,init =
		LEFT(PATHWAYS_all.forename , 1) + LEFT(PATHWAYS_all.surname , 1)

	,psd = 
		convert(Char(10) , PATHWAYS_all.pathway_start_date_current , 103)

	,outpat_hosp = PATHWAYS_all.Hospital
	,PATHWAYS_all.Specialty
	,outpat_cons = PATHWAYS_all.Consultant
	,PATHWAYS_all.RefSource

	,RefDate =
		convert(Char(10), PATHWAYS_all.ReferralDate, 103)

	,lad =
		convert(Char(10), PATHWAYS_all.last_att_date, 103)

	,PATHWAYS_all.last_att_status

	,fad =
		convert(Char(10), PATHWAYS_all.fut_appt_date, 103)

	,PATHWAYS_all.fut_appt_status
	,PATHWAYS_all.WL_Hospital
	,PATHWAYS_all.WL_Spec
	,PATHWAYS_all.WL_Cons

	,WLWLD =
		convert(Char(10), PATHWAYS_all.WL_WLDate, 103)

	,PATHWAYS_all.WL_WLCode
	,PATHWAYS_all.WL_status
	,PATHWAYS_all.WL_PrimProc

	,[WL IntMan] =
		case WL_IntMan 
		when 'D' then 'Daycase' 
		when 'I' then 'Inpatient' 
		else NULL 
		end

	,tcid =
		convert(Char(10), PATHWAYS_all.tci_date, 103)

	,db =
		convert(Char(10), PATHWAYS_all.datebefore, 103)

	,a.Comment
	,Comment2 = a.PComment
	,PATHWAYS_all.InternalNo
	,PATHWAYS_all.EpisodeNo
	,PATHWAYS_all.CaseNoteNo
	,PATHWAYS_all.pathway_start_date_current
	,PATHWAYS_all.pathway_treat_by_date
	,PATHWAYS_all.Decision_date_to_treat_in_IP
	,PATHWAYS_all.WL_EpisodeNo
	,PATHWAYS_all.WL_WLDate
	,PATHWAYS_all.surname
	,PATHWAYS_all.forename
	,PATHWAYS_all.tci_fut_app_date
	,PATHWAYS_all.OldTreatByDate
	,PATHWAYS_all.pct_old
	,PATHWAYS_all.pct_new

	,[WeekNo] =
		dbo.udf_FiscalWeek('04' , Convert(date , [PATHWAYS_all].[datebefore] , 103))

	,[ExtractDate] = Getdate()
	,[tci_canreas]
from
	PATHWAYS_all

LEFT OUTER JOIN ManualAdjustment_rtt_BacklogComments AS a 
ON	a.InternalNo = PATHWAYS_all.InternalNo
AND a.EpisodeNo = PATHWAYS_all.EpisodeNo

where
	(
		PATHWAYS_all.diag = 'N' 
	OR	PATHWAYS_all.diag = 'I' 
	OR	PATHWAYS_all.diag = '' 
	OR	PATHWAYS_all.diag IS NULL
	)
AND PATHWAYS_all.type LIKE 'OPEN'
AND (
		PATHWAYS_all.exc IS NULL 
	OR	PATHWAYS_all.exc = ''
	)
AND PATHWAYS_all.path_doh_spec <> '650'
AND PATHWAYS_all.path_doh_spec <> '950' 
AND PATHWAYS_all.path_doh_spec <> '960' 
AND PATHWAYS_all.path_directorate <> 'MH TRUST'

order by
	 directorate
	,division


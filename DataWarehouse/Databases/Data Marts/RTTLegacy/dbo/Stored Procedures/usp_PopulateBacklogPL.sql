﻿/******************************************************************************
**  Name: [dbo].[usp_PopulateBacklogPL]
**  Purpose: Populate Table - Backlog
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.10.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PopulateBacklogPL]
	-- Add the parameters for the stored procedure here
	@tmp nvarchar(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

INSERT INTO BacklogPL
                      (division, directorate, Doh_Spec, Wks, CaseNoteNo, forename, surname, [Pathway Start Date Original], pathway_status, path_open_days_DNA_adjs, 
                      days, treattype, IPorOP, WL_WLDate, IntendedManagement, [Descision to treat date], MaxTreatdate, tci_date, InternalNo, EpisodeNo, 
                      RTT_Pathway_ID, fut_appt_date, pathway_end_date, pathway_treat_by_date, OldTreatByDate, adjdays, WeekEnd, ExtractDate, Comments, [28WK],TMP,PComments)
SELECT     Backlog_current.division, Backlog_current.directorate, Backlog_current.Doh_Spec, Backlog_current.Wks, Backlog_current.CaseNoteNo, 
                      Backlog_current.forename, Backlog_current.surname, Convert(date,Backlog_current.[Pathway Start Date Original],103) AS [Pathway Start Date Original], Backlog_current.pathway_status, 
                      Backlog_current.path_open_days_DNA_adjs, Backlog_current.days, Backlog_current.treattype, Backlog_current.IPorOP, Backlog_current.WL_WLDate, 
                      Backlog_current.IntendedManagement, Backlog_current.[Descision to treat date], Backlog_current.MaxTreatdate, Backlog_current.tci_date, 
                      Backlog_current.InternalNo, Backlog_current.EpisodeNo, Backlog_current.RTT_Pathway_ID, Backlog_current.fut_appt_date, 
                      Backlog_current.pathway_end_date, Backlog_current.pathway_treat_by_date, Backlog_current.OldTreatByDate, Backlog_current.adjdays, 
                      Backlog_current.WeekEnd, Backlog_current.ExtractDate, a.Comment AS Comment, 
                      CASE WHEN Backlog_current.[days] > 196 THEN 'Over 28' ELSE 'Over 18 Less Than 28' END AS '28WK', @tmp, a.PComment AS PComment
FROM         Backlog_current LEFT OUTER JOIN
                      ManualAdjustment_rtt_BacklogComments AS a ON Backlog_current.InternalNo = a.InternalNo AND Backlog_current.EpisodeNo = a.EpisodeNo
WHERE     (Backlog_current.path_open_days_DNA_adjs - ISNULL(Backlog_current.adjdays, 0) > 126)



END


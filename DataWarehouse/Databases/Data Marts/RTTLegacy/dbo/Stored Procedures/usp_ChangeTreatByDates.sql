﻿
/******************************************************************************
**  Name: [dbo].[usp_ChangeTreatByDates]
**  Purpose: Put the treatby dates back to 18 weeks
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 11.08.11   Alan Bruce  Created
** 08.08.12	  CB          Refactor
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_ChangeTreatByDates] 
AS

BEGIN

SET NOCOUNT ON;
    
UPDATE dbo.PATHWAYS_all
SET 
	OldTreatByDate = 
		case treattype
		when 'IP' then dateadd(day , -35 , pathway_treat_by_date)
		else dateadd(day , -2 , pathway_treat_by_date)
		end;
End
	


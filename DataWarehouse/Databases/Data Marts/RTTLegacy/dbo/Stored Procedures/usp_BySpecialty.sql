﻿/******************************************************************************
**  Name: [dbo].[usp_BySpecialty]
**  Purpose: RTT Report Specialty
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 03.11.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_BySpecialty]
	-- Add the parameters for the stored procedure here
	@ExtractDate nvarchar(50)--, @Division nvarchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT [Doh_Spec]
      ,[Total]
      ,[Weekend]
      ,[IPorOP]
      ,[ExtractDate]
  FROM [tmpReportDataSpecialty]
  Where ExtractDate = @ExtractDate
Order By Doh_Spec
  

    
END


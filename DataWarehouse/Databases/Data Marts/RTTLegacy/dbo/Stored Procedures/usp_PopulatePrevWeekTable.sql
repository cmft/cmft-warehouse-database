﻿
/******************************************************************************
**  Name: [dbo].[usp_PopulatePrevWeekTable]
**  Purpose: Populate Tables - Backlog_Current and Pathways_All_PrevWeek
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 11.08.11   Alan Bruce  Created
** 08.08.12	  CB          Refactor
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PopulatePrevWeekTable]
(
	 @LastSunday datetime
	,@PrevMonday Datetime
)
AS

BEGIN
SET NOCOUNT ON;

INSERT INTO PATHWAYS_all_PrevWeek
(
	 Hospital
	,P2WW
	,RTT_Pathway_ID_2
	,RTT_Pathway_ID
	,[Pause Start Date]
	,[Pause?]
	,TCI_canc
	,OP_plan
	,anap
	,DA_audiology
	,pat_strst
	,disn
	,tci_fut_app_date
	,diag
	,sus_days_pat
	,adjdays
	,daysrem
	,exc
	,clos
	,treattype
	,sent
	,bandbreach
	,days_to_breach
	,pct_new
	,pct_old
	,type
	,currentContractId
	,ContractId
	,ip_act
	,op_act
	,rad_exam_date
	,rad_on_wl
	,rad_casenote
	,init
	,forename
	,surname
	,datebefore
	,sus_days_rem
	,path_open_days_DNA_adjs
	,path_open_wks_DNA_adjs
	,path_closed_days_DNA_adjs
	,path_closed_wks_DNA_adjs
	,pathway_closed_weeks
	,pathway_open_weeks
	,pathway_closed_days
	,pathway_open_days
	,pathway_not18wks
	,pathway_closed
	,pathway_open
	,path_doh_desc
	,path_doh_spec
	,path_spec
	,path_cons
	,path_directorate
	,path_division
	,tci_canreas
	,tci_outcome
	,tci_date
	,WL_status
	,WL_lastRev
	,WL_admitted
	,WL_orig_date
	,WL_PrimProc
	,WL_Category
	,WL_doh_spec
	,WL_Spec
	,WL_Cons
	,WL_RemDate
	,WL_RemReason
	,WL_MethAdm
	,WL_IntMan
	,WL_WLCode
	,WL_WLDate
	,WL_EpisodeNo
	,WL_Hospital
	,fut_appt_status
	,fut_appt_date
	,last_appt_status
	,last_appt_date
	,last_att_status
	,last_att_date
	,pathway_ID
	,OP_treat_date
	,Decision_date_to_treat_in_IP
	,pathway_future_reset_date
	,pathway_end_reason
	,pathway_end_date
	,pathway_status
	,days_to_treatment
	,pathway_treat_by_date
	,pathway_start_date_current
	,pathway_start_date_original
	,pathway_updated
	,DischargeReason
	,DecToRefDate
	,Summary2
	,Summary1
	,WLApptType
	,WLDate
	,WLTreatment
	,DistrictNo
	,CaseNoteNo
	,EpPostCode
	,doh_spec
	,Specialty
	,Consultant
	,ConType
	,RefReasonCode
	,Category
	,EpiCode
	,EpiRefBy
	,RefSource
	,DischDate
	,PriorityType
	,EpisodeNo
	,ReferralDate
	,InternalNo
	,[days]
	,OldTreatByDate
	,IPorOP
	,IntendedManagement
	,[Descision to treat date]
	,MaxTreatdate
	,WeekEnd
	,ExtractDate
	,division
	,directorate
)

SELECT
	 Hospital
	,P2WW
	,RTT_Pathway_ID_2
	,RTT_Pathway_ID
	,[Pause Start Date]
	,[Pause?]
	,TCI_canc
	,OP_plan
	,anap
	,DA_audiology
	,pat_strst
	,disn
	,tci_fut_app_date
	,diag
	,sus_days_pat
	,adjdays
	,daysrem
	,exc
	,clos
	,treattype
	,sent
	,bandbreach
	,days_to_breach
	,pct_new
	,pct_old
	,type
	,currentContractId
	,ContractId
	,ip_act
	,op_act
	,rad_exam_date
	,rad_on_wl
	,rad_casenote
	,init
	,forename
	,surname
	,datebefore
	,sus_days_rem
	,path_open_days_DNA_adjs
	,path_open_wks_DNA_adjs
	,path_closed_days_DNA_adjs
	,path_closed_wks_DNA_adjs
	,pathway_closed_weeks
	,pathway_open_weeks
	,pathway_closed_days
	,pathway_open_days
	,pathway_not18wks
	,pathway_closed
	,pathway_open
	,path_doh_desc
	,path_doh_spec
	,path_spec
	,path_cons
	,path_directorate
	,path_division
	,tci_canreas
	,tci_outcome
	,tci_date
	,WL_status
	,WL_lastRev
	,WL_admitted
	,WL_orig_date
	,WL_PrimProc
	,WL_Category
	,WL_doh_spec
	,WL_Spec
	,WL_Cons
	,WL_RemDate
	,WL_RemReason
	,WL_MethAdm
	,WL_IntMan
	,WL_WLCode
	,WL_WLDate
	,WL_EpisodeNo
	,WL_Hospital
	,fut_appt_status
	,fut_appt_date
	,last_appt_status
	,last_appt_date
	,last_att_status
	,last_att_date
	,pathway_ID
	,OP_treat_date
	,Decision_date_to_treat_in_IP
	,pathway_future_reset_date
	,pathway_end_reason
	,pathway_end_date
	,pathway_status
	,days_to_treatment
	,pathway_treat_by_date
	,pathway_start_date_current
	,pathway_start_date_original
	,pathway_updated
	,DischargeReason
	,DecToRefDate
	,Summary2
	,Summary1
	,WLApptType
	,WLDate
	,WLTreatment
	,DistrictNo
	,CaseNoteNo
	,EpPostCode
	,doh_spec
	,Specialty
	,Consultant
	,ConType
	,RefReasonCode
	,Category
	,EpiCode
	,EpiRefBy
	,RefSource
	,DischDate
	,PriorityType
	,EpisodeNo
	,ReferralDate
	,InternalNo

	--CCB this will generate negative waits if pause end is in the future
	,days =
		path_open_days_DNA_adjs - COALESCE (adjdays , 0)

	,OldTreatByDate

	,IPorOP =
		Case
		When Pathways_All.[type] = 'Closed' Then
			Case
			When treattype = 'IP' Then 'Admitted'
			Else 'Non-Admitted'	
			End					
		ELSE
			CASE
			WHEN Decision_date_to_treat_in_IP IS NULL THEN 'Non-Admitted' 
			ELSE 'Admitted' 
			END 
		End

	,IntendedManagement =
		CASE WL_IntMan 
		WHEN 'D' THEN 'Daycase' 
		WHEN 'I' THEN 'Inpatient' 
		ELSE NULL 
		END

	,[Descision to treat date] =
		CONVERT(Date , Decision_date_to_treat_in_IP , 103)

	,[MaxTreatdate] =
		CONVERT(Date , DATEADD(day , 126 , pathway_start_date_current) , 103)

	,Weekend =
		Convert(nvarchar(10) , @LastSunday , 103)

	,ExtractDate =
		Convert(nvarchar(10) , [datebefore] , 103)

	,path_division
	,path_directorate
FROM
	PATHWAYS_all
WHERE
	(
		OldTreatByDate >= @PrevMonday
	AND OldTreatByDate <= @LastSunday
	) 
OR	(
		pathway_end_date <= @LastSunday
	AND pathway_end_date >= @PrevMonday
	)
END


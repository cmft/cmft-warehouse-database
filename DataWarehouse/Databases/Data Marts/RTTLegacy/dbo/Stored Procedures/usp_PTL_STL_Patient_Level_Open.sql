﻿/******************************************************************************
**  Name: [dbo].[usp_PTL_STL_Patient_Level_Open]
**  Purpose: RTT Report Summary
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.10.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PTL_STL_Patient_Level_Open]
	-- Add the parameters for the stored procedure here
	@ReportMonth int, @Division int
	
AS
BEGIN

Declare @MM_Start Datetime, @MM_End Datetime,
@WhereClause NVARCHAR(2000), @SelectStatement NVARCHAR(4000)


Set @MM_Start = (SELECT  Monitor_month_start
FROM         ManualAdjustment_rtt_PeriodUpdate
WHERE     (id = @ReportMonth))

Set @MM_End = (SELECT  Monitor_month_end
FROM         ManualAdjustment_rtt_PeriodUpdate
WHERE     (id = @ReportMonth))



	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT     SUBSTRING(CONVERT(VARCHAR(11), tci_fut_app_date, 113), 4, 8) AS typ, DATEDIFF(day, pathway_start_date_current, tci_fut_app_date) 
                      - ISNULL(adjdays, 0) AS [STL>18], CASE WHEN DateDiff(day, [pathway_start_date_current], [tci_fut_app_date]) - (ISNULL([adjdays], 0)) 
                      > 126 THEN 'STL' ELSE 'PTL' END AS period, path_doh_spec, path_doh_desc, InternalNo, EpisodeNo, path_division AS division, 
                      path_directorate AS directorate, path_doh_spec AS speciality, path_open_wks_DNA_adjs AS Wks, OldTreatByDate AS [Treat By], days_to_treatment, 
                      CaseNoteNo, DistrictNo, LEFT(forename, 1) + LEFT(surname, 1) AS init, forename, surname, CONVERT(Char(10), pathway_start_date_current, 103) 
                      AS psd, pathway_status, Hospital AS outpat_hosp, Specialty, Consultant AS outpat_cons, RefSource, CONVERT(Char(10), ReferralDate, 103) 
                      AS RefDate, CONVERT(Char(10), last_att_date, 103) AS lad, last_att_status, CONVERT(Char(10), fut_appt_date, 103) AS fad, fut_appt_status, 
                      WL_Hospital, WL_Spec, WL_Cons, CONVERT(Char(10), WL_WLDate, 103) AS WLWLD, WL_WLCode, WL_status, WL_PrimProc, 
                      CASE WL_IntMan WHEN 'D' THEN 'Daycase' WHEN 'I' THEN 'Inpatient' ELSE NULL END AS [WL IntMan], CONVERT(Char(10), tci_date, 103) AS tcid, 
                      CONVERT(Char(10), datebefore, 103) AS db, RTT_Pathway_ID, treattype
INTO            [#Paths]
FROM         PATHWAYS_all
WHERE     (path_doh_spec NOT IN ('654', '650', '960', '950')) AND (WL_status LIKE 'WL%') AND (WL_status NOT LIKE '%DIAG%') AND 
                      (WL_status <> 'PLANNED' OR
                      WL_status IS NULL) AND (tci_fut_app_date >= @MM_Start) AND (tci_fut_app_date < DATEADD(day, 1, @MM_End)) AND (type LIKE 'OPEN') AND 
                      (DATEDIFF(day, pathway_start_date_current, tci_fut_app_date) - ISNULL(adjdays, 0) > 126)

Set @WhereClause = 'Where Division ' + (SELECT Division
FROM  Filters Where id = @Division) + ' AND '

Set @WhereClause = @WhereClause + 'Directorate ' + (SELECT Directorate
FROM  Filters Where id = @Division) 

Set @SelectStatement = 'Select * From #Paths ' + @WhereClause

--Print @SelectStatement
EXECUTE sp_executesql @SelectStatement

END

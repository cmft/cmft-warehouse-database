﻿
/******************************************************************************
**  Name: [dbo].[usp_PopulatePrevWeekTable]
**  Purpose: Populate Tables - Backlog_Current and Pathways_All_PrevWeek
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 11.08.11   Alan Bruce  Created
** 19.07.12	  CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PopulateBacklog_CurrentTable]
	 @LastSunday datetime
	,@PrevMonday Datetime
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

	truncate table Backlog_current

	INSERT INTO Backlog_current
	(
		 division
		,directorate
		,Doh_Spec
		,Wks
		,CaseNoteNo
		,forename
		,surname
		,[Pathway Start Date Original]
		,pathway_status
		,path_open_days_DNA_adjs
		,days
		,treattype
		,IPorOP
		,WL_WLDate
		,IntendedManagement
		,[Descision to treat date]
		,MaxTreatdate
		,tci_date
		,InternalNo
		,EpisodeNo
		,RTT_Pathway_ID
		,pathway_end_date
		,pathway_treat_by_date
		,OldTreatByDate
		,fut_appt_date
		,adjdays
		,WeekEnd
		,ExtractDate
		,Path_Cons
		,OP_treat_date
	)

	SELECT
		 division = path_division
		,directorate = path_directorate

		,Doh_Spec =
			path_doh_spec + ' ' + path_doh_desc

		,Wks =
			path_open_wks_DNA_adjs

		,CaseNoteNo
		,forename
		,surname
		,[Pathway Start Date Original] = pathway_start_date_original
		,pathway_status, path_open_days_DNA_adjs

		--CCB this will generate negative values if the end of the pause is in the future
		,days =
			path_open_days_DNA_adjs - COALESCE (adjdays, 0)

		,treattype
		,IPorOP =
			CASE 
			WHEN Decision_date_to_treat_in_IP IS NULL THEN 'Non-Admitted' 
			ELSE 'Admitted' 
			END

		,WL_WLDate

		,IntendedManagement = 
			CASE WL_IntMan 
			WHEN 'D' THEN 'Daycase' 
			WHEN 'I' THEN 'Inpatient' 
			ELSE NULL 
			END

		,[Descision to treat date] = 
			CONVERT(varchar , Decision_date_to_treat_in_IP , 103)

		,[MaxTreatdate] = 
			CONVERT(varchar , DATEADD(day , 126 , pathway_start_date_current), 103)

		,tci_date
		,InternalNo
		,EpisodeNo
		,RTT_Pathway_ID
		,pathway_end_date
		,pathway_treat_by_date
		,OldTreatByDate
		,fut_appt_date
		,adjdays

		,Weekend = 
			CONVERT(nvarchar(10) , @LastSunday, 103)

		,ExtractDate = 
			CONVERT(nvarchar(10) , datebefore , 103)

		,path_cons
		,OP_treat_date
	FROM
		PATHWAYS_all
	WHERE
		(
			diag = 'N'
		OR	diag = 'I'
		OR	diag = ''
		OR	diag IS NULL
		)
	AND type LIKE 'OPEN'
	AND (
			exc IS NULL 
		OR	exc = ''
		)
	AND path_doh_spec <> '650' 
	AND path_doh_spec <> '950' 
	AND path_doh_spec <> '960'

END


﻿/******************************************************************************
**  Name: [dbo].[usp_PTL_STL_SummaryClosed]
**  Purpose: RTT Report Summary
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.10.11   Alan Bruce  Created
** 20.07.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PTL_STL_SummaryClosed]
	 @ReportMonth int
	,@Breach_Days int
	,@IPorOP nvarchar(12)
	
AS

BEGIN

Declare
	 @MM_Start Datetime
	,@MM_End Datetime
	,@MnthYr nvarChar(12)


select
	 @MM_Start = 
		(
		SELECT
			Monitor_month_start
		FROM
			ManualAdjustment_rtt_Period
		WHERE
			id = @ReportMonth
		)

	,@MM_End =
		DATEADD(
			 day
			,1
			,
			(
			SELECT  
				Monitor_month_end
			FROM
				ManualAdjustment_rtt_Period
			WHERE
				id = @ReportMonth
			)
		)

	,@MnthYr = 
		(
		SELECT
			MonthYr
		FROM
			ManualAdjustment_rtt_Period
		WHERE
			id = @ReportMonth
		)


--Set @MM_End = DATEADD(day,0,@MM_End)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
SET NOCOUNT ON;
	--Set @MM_End = DATEADD(day,1,@MM_End)

INSERT INTO dbo.ClosedSummaryData 
(
	 Period
	,Treat
	,path_division
	,path_doh_spec
	,path_doh_desc
	,[< Breach]
	,[0-5 weeks]
	,[06-11 weeks]
	,[12-17 weeks]
	,[18-22 weeks]
	,[12-18 weeks]
	,[18.3-22 weeks]
	,[23-29 weeks]
	,[30+weeks]
	,path_closed_days_DNA_adjs
	,daysrem
	,sus_days_pat
	,Hospital
	,InternalNo
	,EpisodeNo
	,DischDate
	,OldTreatByDate
	,RTT_Pathway_ID_2
	,RTT_Pathway_ID
	,[Pause Start Date]
	,[Pause?]
	,tci_fut_app_date
	,adjdays
	,exc
	,treattype
	,forename
	,surname
	,datebefore
	,pathway_closed
	,pathway_closed_days
	,path_directorate
	,pathway_end_reason
	,pathway_end_date
	,pathway_start_date_current
	,WLDate
	,DistrictNo
	,CaseNoteNo
	,ExtractDate
	,[Month Selected]
	,[Days To Breach]
	,SpecDesc
	,ReferralDate
	,PriorityType
	,RefSource
	,EpiRefBy
	,EpiCode
	,Category
	,RefReasonCode
	,ConType
	,Consultant
	,Specialty
	,doh_spec
	,EpPostCode
	,WLTreatment
	,WLApptType
	,Summary1
	,Summary2
	,DecToRefDate
	,DischargeReason
	,pathway_updated
	,pathway_start_date_original
	,pathway_treat_by_date
	,days_to_treatment
	,pathway_status
	,pathway_future_reset_date
	,Decision_date_to_treat_in_IP
	,OP_treat_date
	,pathway_ID
	,last_att_date
	,last_att_status
	,last_appt_date
	,last_appt_status
	,fut_appt_date
	,fut_appt_status
	,WL_Hospital
	,WL_EpisodeNo
	,WL_WLDate
	,WL_WLCode
	,WL_IntMan
	,WL_MethAdm
	,WL_RemReason
	,WL_RemDate
	,WL_Cons
	,WL_Spec
	,WL_doh_spec
	,WL_Category
	,WL_PrimProc
	,WL_orig_date
	,WL_admitted
	,WL_lastRev
	,WL_status
	,tci_date
	,tci_outcome
	,tci_canreas
	,path_cons
	,path_spec
	,pathway_open
	,pathway_not18wks
	,pathway_open_days
	,pathway_open_weeks
	,pathway_closed_weeks
	,path_closed_wks_DNA_adjs
	,path_open_wks_DNA_adjs
	,path_open_days_DNA_adjs
	,sus_days_rem
	,init
	,rad_casenote
	,rad_on_wl
	,rad_exam_date
	,op_act
	,ip_act
	,ContractId
	,currentContractId
	,type
	,pct_old
	,pct_new
	,days_to_breach
	,bandbreach
	,sent
	,clos
	,diag
	,disn
	,pat_strst
	,DA_audiology
	,anap
	,OP_plan
	,TCI_canc
	,P2WW
)

SELECT     
	 [Period] = @MnthYr

	,[Treat] =
		dbo.udf_TreatType(PATHWAYS_all.type, PATHWAYS_all.tci_date, PATHWAYS_all.WL_status, PATHWAYS_all.treattype, PATHWAYS_all.diag)

	,PATHWAYS_all.path_division
	,PATHWAYS_all.path_doh_spec
	,PATHWAYS_all.path_doh_desc

	,[< Breach] =
		CASE
		WHEN (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < @Breach_Days) THEN 1 
		ELSE 0 
		END

	,[0-5 weeks] =
		CASE
		WHEN (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < 43) THEN 1 
		ELSE 0 
		END

	,[06-11 weeks] =
		CASE
		WHEN
				(([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < 85) 
			AND (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) >= 43)
			THEN 1 
		ELSE 0 
		END

	,[12-17 weeks] =
		CASE
		WHEN 
				(([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < 127) 
			AND (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) >= 85) 
			THEN 1 
		ELSE 0 
		END

	,[18-22 weeks] =
		CASE 
		WHEN 
				(([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < 162) 
			AND (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) >= 127) 
			THEN 1 
		ELSE 0 
		END

	,[12-18 weeks] =
		CASE 
		WHEN 
				(([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < 129) 
			AND (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) >= 85) 
			THEN 1 
		ELSE 0 
		END

	,[18.3-22 weeks] =
		CASE
		WHEN 
				(([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < 162) 
			AND (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) >= 129) 
			THEN 1 
		ELSE 0 
		END

	,[23-29 weeks] =
		CASE 
		WHEN 
				(([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) < 211) 
			AND (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0)) >= 162) 
			THEN 1 
		ELSE 0 
		END

	,[30+weeks] =
		CASE
		WHEN (([path_closed_days_DNA_adjs] - ISNull([daysrem], 0) - ISNull([sus_days_pat], 0) >= 211)) THEN 1 
		ELSE 0 
		END

	,PATHWAYS_all.path_closed_days_DNA_adjs
	,PATHWAYS_all.daysrem
	,PATHWAYS_all.sus_days_pat
	,PATHWAYS_all.Hospital
	,PATHWAYS_all.InternalNo
	,PATHWAYS_all.EpisodeNo
	,PATHWAYS_all.DischDate
	,PATHWAYS_all.OldTreatByDate
	,PATHWAYS_all.RTT_Pathway_ID_2
	,PATHWAYS_all.RTT_Pathway_ID
	,PATHWAYS_all.[Pause Start Date]
	,PATHWAYS_all.[Pause?]
	,PATHWAYS_all.tci_fut_app_date
	,PATHWAYS_all.adjdays
	,PATHWAYS_all.exc
	,PATHWAYS_all.treattype
	,PATHWAYS_all.forename
	,PATHWAYS_all.surname
	,PATHWAYS_all.datebefore
	,PATHWAYS_all.pathway_closed
	,PATHWAYS_all.pathway_closed_days
	,PATHWAYS_all.path_directorate
	,PATHWAYS_all.pathway_end_reason
	,PATHWAYS_all.pathway_end_date
	,PATHWAYS_all.pathway_start_date_current
	,PATHWAYS_all.WLDate
	,PATHWAYS_all.DistrictNo
	,PATHWAYS_all.CaseNoteNo

	,[ExtractDate] =
		DATEADD(day, 0, DATEDIFF(day, 0, PATHWAYS_all.datebefore))

	,[Month Selected] = @MnthYr
	,[Days To Breach] = @Breach_Days
	,SpecDesc = WarehouseReporting.ETL.BasePASSpecialty.Specialty
	,PATHWAYS_all.ReferralDate
	,PATHWAYS_all.PriorityType
	,PATHWAYS_all.RefSource
	,PATHWAYS_all.EpiRefBy
	,PATHWAYS_all.EpiCode
	,PATHWAYS_all.Category
	,PATHWAYS_all.RefReasonCode
	,PATHWAYS_all.ConType
	,PATHWAYS_all.Consultant
	,PATHWAYS_all.Specialty
	,PATHWAYS_all.doh_spec
	,PATHWAYS_all.EpPostCode
	,PATHWAYS_all.WLTreatment
	,PATHWAYS_all.WLApptType
	,PATHWAYS_all.Summary1
	,PATHWAYS_all.Summary2
	,PATHWAYS_all.DecToRefDate
	,PATHWAYS_all.DischargeReason
	,PATHWAYS_all.pathway_updated
	,PATHWAYS_all.pathway_start_date_original
	,PATHWAYS_all.pathway_treat_by_date
	,PATHWAYS_all.days_to_treatment
	,PATHWAYS_all.pathway_status
	,PATHWAYS_all.pathway_future_reset_date
	,PATHWAYS_all.Decision_date_to_treat_in_IP
	,PATHWAYS_all.OP_treat_date
	,PATHWAYS_all.pathway_ID
	,PATHWAYS_all.last_att_date
	,PATHWAYS_all.last_att_status
	,PATHWAYS_all.last_appt_date
	,PATHWAYS_all.last_appt_status
	,PATHWAYS_all.fut_appt_date
	,PATHWAYS_all.fut_appt_status
	,PATHWAYS_all.WL_Hospital
	,PATHWAYS_all.WL_EpisodeNo
	,PATHWAYS_all.WL_WLDate
	,PATHWAYS_all.WL_WLCode
	,PATHWAYS_all.WL_IntMan
	,PATHWAYS_all.WL_MethAdm
	,PATHWAYS_all.WL_RemReason
	,PATHWAYS_all.WL_RemDate
	,PATHWAYS_all.WL_Cons
	,PATHWAYS_all.WL_Spec
	,PATHWAYS_all.WL_doh_spec
	,PATHWAYS_all.WL_Category
	,PATHWAYS_all.WL_PrimProc
	,PATHWAYS_all.WL_orig_date
	,PATHWAYS_all.WL_admitted
	,PATHWAYS_all.WL_lastRev
	,PATHWAYS_all.WL_status
	,PATHWAYS_all.tci_date
	,PATHWAYS_all.tci_outcome
	,PATHWAYS_all.tci_canreas
	,PATHWAYS_all.path_cons
	,PATHWAYS_all.path_spec
	,PATHWAYS_all.pathway_open
	,PATHWAYS_all.pathway_not18wks
	,PATHWAYS_all.pathway_open_days
	,PATHWAYS_all.pathway_open_weeks
	,PATHWAYS_all.pathway_closed_weeks
	,PATHWAYS_all.path_closed_wks_DNA_adjs
	,PATHWAYS_all.path_open_wks_DNA_adjs
	,PATHWAYS_all.path_open_days_DNA_adjs
	,PATHWAYS_all.sus_days_rem
	,PATHWAYS_all.init
	,PATHWAYS_all.rad_casenote
	,PATHWAYS_all.rad_on_wl
	,PATHWAYS_all.rad_exam_date
	,PATHWAYS_all.op_act
	,PATHWAYS_all.ip_act
	,PATHWAYS_all.ContractId
	,PATHWAYS_all.currentContractId
	,PATHWAYS_all.type
	,PATHWAYS_all.pct_old
	,PATHWAYS_all.pct_new
	,PATHWAYS_all.days_to_breach
	,PATHWAYS_all.bandbreach
	,PATHWAYS_all.sent
	,PATHWAYS_all.clos
	,PATHWAYS_all.diag
	,PATHWAYS_all.disn
	,PATHWAYS_all.pat_strst
	,PATHWAYS_all.DA_audiology
	,PATHWAYS_all.anap
	,PATHWAYS_all.OP_plan
	,PATHWAYS_all.TCI_canc
	,PATHWAYS_all.P2WW
FROM
	PATHWAYS_all 

LEFT OUTER JOIN WarehouseReporting.ETL.BasePASSpecialty 
ON	PATHWAYS_all.path_spec = WarehouseReporting.ETL.BasePASSpecialty.SpecialtyCode

WHERE
	(PATHWAYS_all.type = 'CLOSED') 
AND (
		PATHWAYS_all.diag IS NULL 
	OR	PATHWAYS_all.diag = 'I' 
	OR	PATHWAYS_all.diag = 'N' 
	OR	PATHWAYS_all.diag = ''
	)
AND (
		PATHWAYS_all.exc IS NULL 
	OR	PATHWAYS_all.exc = ''
	)
AND (PATHWAYS_all.pathway_start_date_original >= '1/1/2007') 
AND (PATHWAYS_all.pathway_end_date >= @MM_Start) 
AND (PATHWAYS_all.pathway_end_date < @MM_End) 
AND (PATHWAYS_all.path_doh_spec <> N'650') 
AND (PATHWAYS_all.path_doh_spec <> N'950') 
AND (PATHWAYS_all.path_doh_spec <> N'960') 
AND (dbo.udf_TreatType(PATHWAYS_all.type, PATHWAYS_all.tci_date, PATHWAYS_all.WL_status, PATHWAYS_all.treattype, PATHWAYS_all.diag) = @IPorOP)

END





﻿/******************************************************************************
**  Name: [dbo].[usp_PTL_STL_Patient_Level_Closed]
**  Purpose: RTT Report Summary
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.10.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PTL_STL_Patient_Level_Closed]
	-- Add the parameters for the stored procedure here
	@ReportMonth int, @Division int
	
AS
BEGIN

Declare @MM_Start Datetime,
@WhereClause NVARCHAR(2000), @SelectStatement NVARCHAR(4000)


Set @MM_Start = (SELECT  Monitor_month_start
FROM         ManualAdjustment_rtt_PeriodUpdate
WHERE     (id = @ReportMonth))



	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT CONVERT(Char(10),[pathway_end_date], 103) AS [End Month], 
PATHWAYS_all.InternalNo, PATHWAYS_all.EpisodeNo, 
PATHWAYS_all.path_division, PATHWAYS_all.path_directorate AS Directorate, 
PATHWAYS_all.path_doh_spec, 
Case When [treattype]='IP'
Then
'Admitted'
Else
'Non Admitted'
End AS treat, 
PATHWAYS_all.path_spec, PATHWAYS_all.path_closed_wks_DNA_adjs, 
PATHWAYS_all.CaseNoteNo, PATHWAYS_all.RTT_Pathway_ID, 
PATHWAYS_all.DistrictNo, PATHWAYS_all.forename, PATHWAYS_all.surname, 
CONVERT(Char(10),[ReferralDate], 103) AS Ref_Date, CONVERT(Char(10),[DischDate], 103) AS Disch_date, 
CONVERT(Char(10),[pathway_start_date_original], 103) AS psdo, 
CONVERT(Char(10),[pathway_start_date_current], 103) AS p_s_d, 
CONVERT(Char(10),[pathway_treat_by_date], 103) AS P_tb_d, 
CONVERT(Char(10),[pathway_end_date], 103) AS ped, 
CONVERT(Char(10),[last_appt_date], 103) AS lad, PATHWAYS_all.WL_Hospital, 
CONVERT(Char(10),[WL_WLDate], 103) AS wlwld, PATHWAYS_all.WL_WLCode, 
 CASE WL_IntMan WHEN 'D' THEN 'Daycase' WHEN 'I' THEN 'Inpatient' ELSE NULL END
                      [WL IntMan], 
PATHWAYS_all.WL_MethAdm, PATHWAYS_all.WL_RemReason, 
CONVERT(Char(10),[WL_RemDate], 103) AS wlrd, PATHWAYS_all.WL_Cons, 
PATHWAYS_all.WL_Spec, PATHWAYS_all.WL_doh_spec, 
CONVERT(Char(10),[WL_orig_date], 103) AS wlod, 
CONVERT(Char(10),[WL_admitted], 103) AS wla, 
PATHWAYS_all.path_cons, PATHWAYS_all.pathway_status,  
PATHWAYS_all.daysrem, PATHWAYS_all.sus_days_pat INTO #ClosedPaths
FROM PATHWAYS_all
WHERE (PATHWAYS_all.path_doh_spec NOT IN( '654','650','960', '950') 
AND (([path_closed_days_DNA_adjs]-IsNull([daysrem],0)-IsNull([sus_days_pat],0))>126) 
AND ((PATHWAYS_all.type)='CLOSED') AND ((PATHWAYS_all.diag)='N' 
Or (PATHWAYS_all.diag)='I' Or (PATHWAYS_all.diag)='' 
Or (PATHWAYS_all.diag) Is Null) AND ((PATHWAYS_all.exc) Is Null 
Or (PATHWAYS_all.exc)='') 
AND ((PATHWAYS_all.pathway_end_date)>=@MM_Start))
ORDER BY PATHWAYS_all.path_division, PATHWAYS_all.path_directorate


Set @WhereClause = 'Where Path_Division ' + (SELECT Division
FROM  Filters Where id = @Division) + ' AND '

Set @WhereClause = @WhereClause + 'Directorate ' + (SELECT Directorate
FROM  Filters Where id = @Division) 

Set @SelectStatement = 'Select * From #ClosedPaths ' + @WhereClause

--Print @SelectStatement
EXECUTE sp_executesql @SelectStatement

END

﻿
/******************************************************************************
**  Name: [dbo].[usp_MakeDuplicates]
**  Purpose: 
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 11.08.11   Alan Bruce  Created
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_MakeDuplicates]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
    --Gather those not to be Reported
INSERT INTO DuplicatesToRemove
                      (InternalNo, EpisodeNo,CreatedBy,CreatedDate)
SELECT     InternalNo, EpisodeNo, UpdatedBy, GETDATE()
FROM         Duplicates
WHERE     (Adjusted = 'True')


Truncate Table Duplicates

INSERT INTO Duplicates
                      ([Import Date], Hospital, InternalNo, EpisodeNo, CaseNoteNo, DistrictNo, path_doh_desc, path_doh_spec, path_spec, pathway_start_date_current, 
                      pathway_treat_by_date, ReferralDate, tci_fut_app_date, pathway_end_date, pathway_status, pathway_end_reason, Division, Directorate, 
                      pathway_open, pathway_closed, pathway_not18wks, surname, forename, RTT_Pathway_ID, RTT_Pathway_ID_2, type, days_to_treatment, 
                      WL_WLDate, path_cons, Spec_use, WL_Cons, WL_Spec, RefCons, RefSpec, WL_status, pathway_open_weeks, pathway_closed_weeks, exc)
SELECT     CONVERT(Date, duplicate_spec_pats.importDate, 103) AS 'Import Date', PATHWAYS_all.Hospital, PATHWAYS_all.InternalNo, 
                      PATHWAYS_all.EpisodeNo, PATHWAYS_all.CaseNoteNo, PATHWAYS_all.DistrictNo, PATHWAYS_all.path_doh_desc, PATHWAYS_all.path_doh_spec, 
                      PATHWAYS_all.path_spec, CONVERT(varchar(17), PATHWAYS_all.pathway_start_date_current, 109) AS 'pathway_start_date_current', 
                      CONVERT(varchar(17), PATHWAYS_all.pathway_treat_by_date, 109) AS 'pathway_treat_by_date', CONVERT(varchar(17), PATHWAYS_all.ReferralDate, 
                      109) AS 'ReferralDate', CONVERT(varchar(17), PATHWAYS_all.tci_fut_app_date, 109) AS 'tci_fut_app_date', CONVERT(varchar(17), 
                      PATHWAYS_all.pathway_end_date, 109) AS 'pathway_end_date', PATHWAYS_all.pathway_status, PATHWAYS_all.pathway_end_reason, 
                      PATHWAYS_all.path_division AS Division, PATHWAYS_all.path_directorate AS Directorate, PATHWAYS_all.pathway_open, 
                      PATHWAYS_all.pathway_closed, PATHWAYS_all.pathway_not18wks, PATHWAYS_all.surname, PATHWAYS_all.forename, 
                      PATHWAYS_all.RTT_Pathway_ID, PATHWAYS_all.RTT_Pathway_ID_2, PATHWAYS_all.type, PATHWAYS_all.days_to_treatment, CONVERT(varchar(17), 
                      PATHWAYS_all.WL_WLDate, 109) AS 'WL_WLDate', PATHWAYS_all.path_cons, Spec_lookup.Spec_use, PATHWAYS_all.WL_Cons, 
                      PATHWAYS_all.WL_Spec, PATHWAYS_all.Consultant AS RefCons, PATHWAYS_all.Specialty AS RefSpec, PATHWAYS_all.WL_status, 
                      PATHWAYS_all.pathway_open_weeks, PATHWAYS_all.pathway_closed_weeks, PATHWAYS_all.exc
FROM         duplicate_spec_pats INNER JOIN
                      PATHWAYS_all ON duplicate_spec_pats.InternalNo = PATHWAYS_all.InternalNo AND 
                      duplicate_spec_pats.path_division = PATHWAYS_all.path_division INNER JOIN
                      Spec_lookup ON duplicate_spec_pats.Spec_use = Spec_lookup.Spec_use AND PATHWAYS_all.path_doh_spec = Spec_lookup.spec_match
WHERE     (PATHWAYS_all.type = 'Open') OR
                      (PATHWAYS_all.type = 'CLOSED') AND (PATHWAYS_all.diag IS NULL OR
                      PATHWAYS_all.diag = 'N' OR
                      PATHWAYS_all.diag = 'I') AND (PATHWAYS_all.exc IS NULL)


--Delete those recorded in the Duplicates to remove Table 
Delete     Duplicates
FROM         Duplicates RIGHT OUTER JOIN
                      DuplicatesToRemove ON Duplicates.InternalNo = DuplicatesToRemove.InternalNo 
                      AND Duplicates.EpisodeNo = DuplicatesToRemove.EpisodeNo

END


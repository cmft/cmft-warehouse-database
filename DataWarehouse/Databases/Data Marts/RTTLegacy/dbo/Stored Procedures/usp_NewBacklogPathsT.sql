﻿
/******************************************************************************
**  Name: [dbo].[usp_NewBacklogPathsT]
**  Purpose: Set New Paths in Backlog
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 14.11.11   Alan Bruce  Created
** 08.08.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_NewBacklogPathsT]
	 @ExtractDate nvarchar(12)
	,@Weekend nvarchar(12)
AS

BEGIN

SET NOCOUNT ON;

   
DECLARE @OldPaths TABLE 
( 
     EpisodeNo nvarchar(100)
	,InternalNo nvarchar(100) 
)

DECLARE @NewPaths TABLE 
( 
    id int    
)

insert into @OldPaths
(
	 InternalNo
	,EpisodeNo
)

select
	 InternalNo
	,EpisodeNo
FROM
	Backlog                             
WHERE
	TMP IS NULL
AND Backlog.WeekEnd = @Weekend

	               
insert into @NewPaths
(
	id
)

SELECT
	a.id
FROM
	@OldPaths b

RIGHT JOIN 
	(
	 Select
		 id
		,InternalNo
		,EpisodeNo  
	FROM 
		Backlog                             
	WHERE
		TMP = N'T'
	AND ExtractDate = @ExtractDate
	) a
ON	b.EpisodeNo = a.EpisodeNo
AND b.InternalNo = a.InternalNo

WHERE
	b.InternalNo Is Null

update Backlog 
set 
	NewP = 'N' 
where
	id IN
		(
		select
			id
		from 
			@NewPaths
		)
END


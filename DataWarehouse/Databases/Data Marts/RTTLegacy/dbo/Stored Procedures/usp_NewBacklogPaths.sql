﻿/******************************************************************************
**  Name: [dbo].[usp_NewBacklogPaths]
**  Purpose: Set New Paths in Backlog
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 14.11.11   Alan Bruce  Created
** 08.08.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_NewBacklogPaths]
	 @ExtractDate nvarchar(12)
	,@Weekend nvarchar(12)
AS

BEGIN

SET NOCOUNT ON;

   
DECLARE @OldPaths TABLE 
( 
    EpisodeNo nvarchar(100), 
    InternalNo nvarchar(100) 
)

DECLARE @NewPaths TABLE 
( 
    id int    
)

insert into @OldPaths
(
	 InternalNo
	,EpisodeNo
)

Select
	 InternalNo
	,EpisodeNo
FROM
	Backlog                             
WHERE
	TMP IS NULL
AND Backlog.WeekEnd = @Weekend

	               
insert into @NewPaths
(
	id
)

SELECT
	a.id
FROM
	@OldPaths b

RIGHT JOIN 
	(
	select
		 id
		,InternalNo
		,EpisodeNo  
	FROM
		Backlog                             
	WHERE
		ExtractDate = @ExtractDate
	) a
ON	b.EpisodeNo = a.EpisodeNo
AND b.InternalNo = a.InternalNo

WHERE
	b.InternalNo Is Null

update Backlog
set
	NewP = 'N'
where
	id IN
		(
		select
			id 
		from
			@NewPaths
		)


--Update Backlog Graph Data


--INSERT INTO dbo.BacklogGraph
--                      (Total, division, ExtractDate, WeekEnd, IPorOP)
--  Select  COUNT(*) AS Total, a.division, a.ExtractDate, a.WeekEnd, a.IPorOP From(                
--SELECT  Case When
--division Like '%Medical' Then 'Medical' Else division END as division, ExtractDate, WeekEnd, IPorOP
--FROM         Backlog
--WHERE     (WeekEnd = @Weekend) AND (TMP IS NULL) )a
--GROUP BY division, ExtractDate, WeekEnd, IPorOP


END


﻿/******************************************************************************
**  Name: [dbo].[usp_AllOpenSummaryWeekly]
**  Purpose: RTT Report All Open Pathways Summary Weekly Record
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 13.02.12   Alan Bruce  Created
** 19.07.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_AllOpenSummaryWeekly]	 	
AS

BEGIN	

declare
	 @AdmittedTraget int = 18
	,@NonAdmittedTarget int = 18

SET DATEFIRST 1 -- Set the first day of the week to Monday  
               
     
INSERT INTO AllOpenSummary 
(
	 [division]
	,[speciality]
	,[path_spec]
	,[Admitted]
	,[Admitted Over 18 Weeks]
	,[Non-Admitted]
	,[Non-Admitted Over 18 Weeks]
	,[WeekNo]
	,[% Admitted under 18 Weeks]
	,[% Non-Admitted under 18 Weeks]
	,[% Total under 18 Weeks]
	,[Performance]
)                            

select
	 division
	,[speciality]
	,path_spec
	,[Admitted]
	,[Admitted Over 18 Weeks]
	,[Non-Admitted]
	,[Non-Admitted Over 18 Weeks]
	,[WeekNo]

	,[% Admitted under 18 Weeks] = 
		convert(
			 decimal(5 , 4) 
			,1 - dbo.udfSafeDivision([Admitted Over 18 Weeks] , [Admitted])
		)

	,[% Non-Admitted under 18 Weeks] = 
		convert(
			 decimal(5 , 4) 
			,1 - dbo.udfSafeDivision([Non-Admitted Over 18 Weeks] , [Non-Admitted])
		)

	,[% Total under 18 Weeks]  =
		convert(
			 decimal(5 , 4)
			,1 - dbo.udfSafeDivision([Admitted Over 18 Weeks] + [Non-Admitted Over 18 Weeks] , Admitted + [Non-Admitted])
		)

	,[Performance]  = 
		convert(
			 decimal(5 , 4)
			,dbo.udfSafeDivision([Admitted Over 18 Weeks] + [Non-Admitted Over 18 Weeks] , Admitted + [Non-Admitted])
		)
from
	(
	select
		 division
		,[speciality]
		,path_spec

		,[Admitted] = 
			SUM(
				CASE IPorOP 
				WHEN 'Admitted' THEN 1 
				ELSE 0 
				END
			)

		,[Admitted Over 18 Weeks] = 
			SUM(
				CASE 
				WHEN IPorOP = 'Admitted' AND Wks >= @AdmittedTraget THEN 1 
				ELSE 0 
				END
			)

		,[Non-Admitted] = 
			SUM(
				CASE IPorOP 
				WHEN 'Non-Admitted' THEN 1 
				ELSE 0 
				END
			)

		,[Non-Admitted Over 18 Weeks] = 
			SUM(
				CASE 
				WHEN IPorOP = 'Non-Admitted' AND Wks >= @NonAdmittedTarget THEN 1 
				ELSE 0 
				END
			)

		,[WeekNo] =
			dbo.udf_FiscalWeek('04' , convert(date , db ,103))

	FROM
		[AllOpenPathways] 
	group by
		 division
		,[speciality]
		,path_spec
		,dbo.udf_FiscalWeek('04' , convert(date , db , 103))
	) a


END




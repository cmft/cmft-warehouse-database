﻿/******************************************************************************
**  Name: [dbo].[usp_Backlog_Growth]
**  Purpose: RTT Report Backlog Detail Report
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 03.11.11   Alan Bruce  Created
** 19.07.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_Backlog_Growth]
	 @IPorOP nvarchar(15)	
AS

BEGIN	

SELECT
	 [id]

	,ExtractDate =
		dateadd(day , -1 , CONVERT(date , ExtractDate, 103))

	,[division]
	,[28WK]
FROM
	[Backlog] 
WHERE
	IPorOP = @IPorOP
AND TMP IS NULL

ORDER BY
	CONVERT(date , ExtractDate , 103)


END




﻿
/******************************************************************************
**  Name: [dbo].[usp_BacklogDataInsert]
**  Purpose: Set New Paths in Backlog
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 14.11.11   Alan Bruce  Created
** 08.08.12   CB          Refactored for readability
** 17.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_BacklogDataInsert]	
AS

BEGIN

SET NOCOUNT ON;

DECLARE @RC int
DECLARE @ExtractDate nvarchar(12)
DECLARE @Weekend nvarchar(12)
DECLARE @LastSunday Datetime


select
	 @LastSunday =
		dbo.udf_PrevSunday(Getdate()) 
	,@Weekend =
		Convert(nvarchar(10) , (DATEADD(day , 0 , @LastSunday)) , 103)

if exists
	(
	select
		1
	from
		Backlog
	where
		WeekEnd = @Weekend
	)

begin

	--*****************************
	--Do the Daily One
	EXECUTE [dbo].[usp_PopulateBacklog]
		'T'

	select
		 @Weekend =
			Convert(nvarchar(10) , (DATEADD(day , 0 , @LastSunday)) , 103)

		,@ExtractDate =
			(
			select top 1 
				convert(nvarchar(10) , [datebefore] , 103)
			from
				Pathways_All
			)

	EXECUTE @RC = [dbo].[usp_NewBacklogPathsT] 
	   @ExtractDate
	  ,@Weekend

	End

Else

Begin
--**************************
--Do the Weekly One
	EXECUTE [dbo].[usp_PopulateBacklog] 
	   NULL

	select
		 @Weekend =
			convert(nvarchar(10) , (DATEADD(day , -7 , @LastSunday)) , 103)

		,@ExtractDate =
			(
			select top 1 
				convert(nvarchar(10) , [datebefore] , 103)
			from
				Pathways_All
			)

	delete
	from
		dbo.Backlog
	where
		TMP = 'T'

	EXECUTE @RC = [dbo].[usp_NewBacklogPaths] 
	   @ExtractDate
	  ,@Weekend
  



--CCB 2012-08-08
-- redundant?

	  --Backlog Graph Data
 
	select
		 @Weekend =
			convert(nvarchar(10) , (DATEADD(day , 0 , @LastSunday)) , 103)

		,@ExtractDate = 
			(
			select top 1 
				convert(nvarchar(10) , [datebefore] , 103)
			from 
				Pathways_All
			)

	--************** 18 Week Totals

	INSERT INTO dbo.BacklogGraph
	(
		 Total
		,division
		,ExtractDate
		,WeekEnd
		,IPorOP
		,BreachTarget
	)

	select
		 Total = COUNT(1)
		,a.division
		,a.ExtractDate
		,a.WeekEnd
		,a.IPorOP
		,127 
	From
		(                
		SELECT
			division =
				case
				when division like '%Medical' then 'Medical' 
				else division 
				end

			,ExtractDate
			,WeekEnd
			,IPorOP
		FROM
			Backlog
		WHERE
			WeekEnd = @Weekend
		AND TMP IS NULL
		) a
	GROUP BY
		 division
		,ExtractDate
		,WeekEnd
		,IPorOP

	--*************** 23 week Totals
	INSERT INTO dbo.BacklogGraph
	(
		 Total
		,division
		,ExtractDate
		,WeekEnd
		,IPorOP
		,BreachTarget
	)
	select
		 Total = count(1)
		,a.division
		,a.ExtractDate
		,a.WeekEnd
		,a.IPorOP 
		,129 
	From
		(        
		SELECT
			division =
				case 
				when division Like '%Medical' Then 'Medical' 
				else division 
				end
			,ExtractDate
			,WeekEnd
			,IPorOP
		from
			Backlog
		WHERE
			BreachDays = 129 
		AND IPorOP = 'Non-Admitted' 
		AND WeekEnd = @Weekend
		AND TMP IS NULL
		) a
	GROUP BY division, ExtractDate, WeekEnd, IPorOP


	INSERT INTO dbo.BacklogGraph
	(
		 Total
		,division
		,ExtractDate
		,WeekEnd
		,IPorOP
		,BreachTarget
	)
	select
		 Total = COUNT(1)
		,a.division
		,a.ExtractDate
		,a.WeekEnd
		,a.IPorOP 
		,162
	from
		(                
		SELECT
			division =
				case 
				when division Like '%Medical' Then 'Medical' 
				else division 
				end
			,ExtractDate
			,WeekEnd
			,IPorOP
		FROM
			Backlog
		WHERE
			BreachDays = 162 
		AND IPorOP = 'Admitted' 
		AND WeekEnd = @Weekend
		AND TMP IS NULL
		) a
	GROUP BY division, ExtractDate, WeekEnd, IPorOP
	End

	EXECUTE [dbo].[usp_RemoveAmpsInReports] 

	EXECUTE [dbo].[usp_AllOpenSummaryWeekly] 

END

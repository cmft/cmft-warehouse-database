﻿/******************************************************************************
**  Name: [dbo].[usp_PTL_STL_SummaryOT]
**  Purpose: RTT Report Summary
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.10.11   Alan Bruce  Created
** 23.07.12   CB          Refactored for readability
**                        Bug fix in WHERE clause:  missing brackets around OR conditions
** 17.10.12   MH          Brought into source control and used synonym to ManualAdjustment_rtt_Period
** 25.10.12   MH          Replacement of udf_PTL_open, udf_STL_Open, udf_STL_Open_OT, udf_PTL_Open_OT
**                        with udf_Open_OT and udf_OT for efficiency gains
******************************************************************************/

CREATE PROCEDURE [dbo].[usp_PTL_STL_SummaryOT]
	 @ReportMonth int
	,@Breach_Days int
AS

BEGIN

Declare
	 @MM_Start Datetime
	,@MM_End Datetime
	,@MnthYr nvarChar(12)


select
	 @MM_Start = Monitor_month_start
	,@MM_End = DATEADD(day , 1 , Monitor_month_end)
	,@MnthYr = MonthYr
from	
	ManualAdjustment_rtt_PeriodUpdate
WHERE
	id = @ReportMonth

--Set @MM_Start = (SELECT  Monitor_month_start
--FROM         ManualAdjustment_rtt_PeriodUpdate
--WHERE     (id = @ReportMonth))

--Set @MM_End = (SELECT  Monitor_month_end
--FROM         ManualAdjustment_rtt_PeriodUpdate
--WHERE     (id = @ReportMonth))

--Set @MnthYr = (SELECT  MonthYr
--FROM         ManualAdjustment_rtt_PeriodUpdate
--WHERE     (id = @ReportMonth))


--Set @MM_End = DATEADD(day,1,Monitor_month_end)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
SET NOCOUNT ON;
INSERT INTO dbo.SummaryData
(
	 Hospital
	,InternalNo
	,ReferralDate
	,EpisodeNo
	,PriorityType
	,DischDate
	,RefSource
	,EpiRefBy
	,EpiCode
	,Category
	,RefReasonCode
	,ConType
	,Consultant
	,Specialty
	,doh_spec
	,EpPostCode
	,CaseNoteNo
	,DistrictNo
	,WLTreatment
	,WLDate
	,WLApptType
	,Summary1
	,Summary2
	,DecToRefDate
	,DischargeReason
	,pathway_updated
	,pathway_start_date_original
	,pathway_start_date_current
	,pathway_treat_by_date
	,days_to_treatment
	,pathway_status
	,pathway_end_date
	,pathway_end_reason
	,pathway_future_reset_date
	,Decision_date_to_treat_in_IP
	,OP_treat_date
	,pathway_ID
	,last_att_date
	,last_att_status
	,last_appt_date
	,last_appt_status
	,fut_appt_date
	,fut_appt_status
	,WL_Hospital
	,WL_EpisodeNo
	,WL_WLDate
	,WL_WLCode
	,WL_IntMan
	,WL_MethAdm
	,WL_RemReason
	,WL_RemDate
	,WL_Cons
	,WL_Spec
	,WL_doh_spec
	,WL_Category
	,WL_PrimProc
	,WL_orig_date
	,WL_admitted
	,WL_lastRev
	,WL_status
	,tci_date
	,tci_outcome
	,tci_canreas
	,path_division
	,path_directorate
	,path_cons
	,path_spec
	,path_doh_spec
	,path_doh_desc
	,pathway_open
	,pathway_closed
	,pathway_not18wks
	,pathway_open_days
	,pathway_closed_days
	,pathway_open_weeks
	,pathway_closed_weeks
	,path_closed_wks_DNA_adjs
	,path_closed_days_DNA_adjs
	,path_open_wks_DNA_adjs
	,path_open_days_DNA_adjs
	,sus_days_rem
	,datebefore
	,surname
	,forename
	,init
	,rad_casenote
	,rad_on_wl
	,rad_exam_date
	,op_act
	,ip_act
	,ContractId
	,currentContractId
	,type
	,pct_old
	,pct_new
	,days_to_breach
	,bandbreach
	,sent
	,treattype
	,clos
	,exc
	,daysrem
	,adjdays
	,sus_days_pat
	,diag
	,tci_fut_app_date
	,disn
	,pat_strst
	,DA_audiology
	,anap
	,OP_plan
	,TCI_canc
	,[Pause?]
	,[Pause Start Date]
	,RTT_Pathway_ID
	,RTT_Pathway_ID_2
	,P2WW
	,OldTreatByDate
	,Treat
	,Total_PTL
	,Total_STL
	,[PTL<=23]
	,[PTL>23]
	,[STL<=23]
	,[STL>23]
	,ExtractDate
	,[Month Selected]
	,[Days To Breach]
	,SpecDesc
)

SELECT
	 Hospital
	,InternalNo
	,ReferralDate
	,EpisodeNo
	,PriorityType
	,DischDate
	,RefSource
	,EpiRefBy
	,EpiCode
	,Category
	,RefReasonCode
	,ConType
	,Consultant
	,Pathways_All.Specialty
	,doh_spec
	,EpPostCode
	,CaseNoteNo
	,DistrictNo
	,WLTreatment
	,WLDate
	,WLApptType
	,Summary1
	,Summary2
	,DecToRefDate
	,DischargeReason
	,pathway_updated
	,pathway_start_date_original
	,pathway_start_date_current
	,pathway_treat_by_date
	,days_to_treatment
	,pathway_status
	,pathway_end_date
	,pathway_end_reason
	,pathway_future_reset_date
	,Decision_date_to_treat_in_IP
	,OP_treat_date
	,pathway_ID
	,last_att_date
	,last_att_status
	,last_appt_date
	,last_appt_status
	,fut_appt_date
	,fut_appt_status
	,WL_Hospital
	,WL_EpisodeNo
	,WL_WLDate
	,WL_WLCode
	,WL_IntMan
	,WL_MethAdm
	,WL_RemReason
	,WL_RemDate
	,WL_Cons
	,WL_Spec
	,WL_doh_spec
	,WL_Category
	,WL_PrimProc
	,WL_orig_date
	,WL_admitted
	,WL_lastRev
	,WL_status
	,tci_date
	,tci_outcome
	,tci_canreas
	,path_division
	,path_directorate
	,path_cons
	,path_spec
	,path_doh_spec
	,path_doh_desc
	,pathway_open
	,pathway_closed
	,pathway_not18wks
	,pathway_open_days
	,pathway_closed_days
	,pathway_open_weeks
	,pathway_closed_weeks
	,path_closed_wks_DNA_adjs
	,path_closed_days_DNA_adjs
	,path_open_wks_DNA_adjs
	,path_open_days_DNA_adjs
	,sus_days_rem
	,datebefore
	,surname
	,forename
	,init
	,rad_casenote
	,rad_on_wl
	,rad_exam_date
	,op_act
	,ip_act
	,ContractId
	,currentContractId
	,type
	,pct_old
	,pct_new
	,days_to_breach
	,bandbreach
	,sent
	,treattype
	,clos
	,exc
	,daysrem
	,adjdays
	,sus_days_pat
	,diag
	,tci_fut_app_date
	,disn
	,pat_strst
	,DA_audiology
	,anap
	,OP_plan
	,TCI_canc
	,[Pause?]
	,[Pause Start Date]
	,RTT_Pathway_ID
	,RTT_Pathway_ID_2
	,P2WW
	,OldTreatByDate

	,[Treat] = 
		[dbo].[udf_TreatType]([Type],[tci_date],[WL_status],[treattype],[diag])

	,[Total_PTL] = CASE WHEN OPENOT.OpenOTType = 'PTLOPEN' THEN 1 ELSE 0 END
	,[Total_STL] = CASE WHEN OPENOT.OpenOTType = 'STLOPEN' THEN 1 ELSE 0 END	
	
	,[PTL<=23] = 
		CASE 
		WHEN 
				OT.OTType = 'PTL'  
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) < @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	,[PTL>23] =
		CASE 
		WHEN 
				OT.OTType = 'PTL' 
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) >= @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	,[STL<=23] = 
		CASE 
		WHEN 
				OT.OTType = 'STL' 
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) < @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	,[STL>23] = 
		CASE 
		WHEN 
				OT.OTType = 'STL'  
			AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) >= @Breach_Days) 
			THEN 1 
		ELSE 0 
		END

	--,[Total_PTL] = 
	--	CASE 
	--	WHEN ([dbo].[udf_PTL_Open_OT]([pathway_start_date_current],PATHWAYS_all.[type] , @ReportMonth) = 1) THEN 1 
	--	ELSE 0 
	--	END

	--,[Total_STL] = 
	--	CASE 
	--	WHEN ([dbo].[udf_STL_Open_OT]([pathway_start_date_current],PATHWAYS_all.[type] , @ReportMonth) = 1) THEN 1 
	--	ELSE 0 
	--	END

	--,[PTL<=23] = 
	--	CASE 
	--	WHEN 
	--			([dbo].[udf_PTL_OT]([pathway_start_date_current],[tci_fut_app_date], @ReportMonth) = 1) 
	--		AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) < @Breach_Days) 
	--		THEN 1 
	--	ELSE 0 
	--	END

	--,[PTL>23] =
	--	CASE 
	--	WHEN 
	--			([dbo].[udf_PTL_OT]([pathway_start_date_current],[tci_fut_app_date], @ReportMonth) = 1) 
	--		AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) >= @Breach_Days) 
	--		THEN 1 
	--	ELSE 0 
	--	END

	--,[STL<=23] = 
	--	CASE 
	--	WHEN 
	--			([dbo].[udf_STL_OT]([pathway_start_date_current],[tci_fut_app_date], @ReportMonth) = 1) 
	--		AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) < @Breach_Days) 
	--		THEN 1 
	--	ELSE 0 
	--	END

	--,[STL>23] = 
	--	CASE 
	--	WHEN 
	--			([dbo].[udf_STL_OT]([pathway_start_date_current],[tci_fut_app_date], @ReportMonth) = 1) 
	--		AND (convert(int,(DateDiff(day,[pathway_start_date_current],[tci_fut_app_date]) - ISNull([adjdays], 0))) >= @Breach_Days) 
	--		THEN 1 
	--	ELSE 0 
	--	END

	,[ExtractDate] = 
		DATEADD(day, 0, DATEDIFF(day, 0, [Datebefore]))

	,[Month Selected] = @MnthYr
	,[Days To Breach] = @Breach_Days
	,SpecDesc = WarehouseReporting.ETL.BasePASSpecialty.Specialty
FROM
PATHWAYS_all 

LEFT OUTER JOIN WarehouseReporting.ETL.BasePASSpecialty 
ON PATHWAYS_all.path_spec = WarehouseReporting.ETL.BasePASSpecialty.SpecialtyCode

OUTER APPLY dbo.udf_Open_OT ([pathway_start_date_current],PATHWAYS_all.[type] , @ReportMonth) OPENOT
OUTER APPLY dbo.udf_OT ([pathway_start_date_current],[tci_fut_app_date], @ReportMonth) OT

WHERE
	(
		(path_division NOT LIKE 'MH%') 
	AND (type LIKE 'OPEN') 
	AND (WL_status LIKE 'WL%') 
	AND (WL_status NOT LIKE '%DIAG%') 
	AND	(
			WL_status <> 'PLANNED' 
		OR	WL_status IS NULL
		)
	AND (
			diag IS NULL 
		OR	diag = 'I' 
		OR	diag = 'N' 
		OR	diag = ''
		)
	AND (
			exc IS NULL 
		OR	exc = ''
		)
	AND (pathway_start_date_original >= '1/1/2007') 
	AND (
			path_doh_spec <> N'650' 
		AND path_doh_spec <> N'950' 
		AND path_doh_spec <> N'960'
		)
	)
OR	(
		(path_division NOT LIKE 'MH%') 
	AND (type = 'CLOSED') 
	AND (
			diag IS NULL 
		OR	diag = 'I' 
		OR	diag = 'N' 
		OR	diag = ''
		)
	AND (
			exc IS NULL 
		OR	exc = ''
		)
	AND pathway_start_date_original >= '1/1/2007'
	AND pathway_end_date >= @MM_Start
	AND pathway_end_date < @MM_End
	AND path_doh_spec <> N'650' 
	AND path_doh_spec <> N'950' 
	AND path_doh_spec <> N'960'
	)

END

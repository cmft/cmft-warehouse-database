﻿CREATE TABLE [WH].[Calendar] (
    [DateID]              INT           NOT NULL,
    [TheDate]             DATE          NOT NULL,
    [DayOfWeek]           NVARCHAR (30) NULL,
    [DayOfWeekKey]        INT           NULL,
    [LongDate]            VARCHAR (11)  NULL,
    [TheMonth]            NVARCHAR (34) NULL,
    [MonthName]           VARCHAR (3)   NULL,
    [FinancialQuarter]    NVARCHAR (70) NULL,
    [FinancialYear]       NVARCHAR (35) NULL,
    [FinancialMonthKey]   INT           NULL,
    [FinancialQuarterKey] INT           NULL,
    [CalendarQuarter]     VARCHAR (34)  NULL,
    [CalendarSemester]    VARCHAR (12)  NOT NULL,
    [CalendarYear]        NVARCHAR (30) NULL,
    [LastCompleteMonth]   INT           NOT NULL,
    [WeekNoKey]           VARCHAR (50)  NULL,
    [WeekNo]              VARCHAR (50)  NULL,
    [FirstDayOfWeek]      SMALLDATETIME NULL,
    [LastDayOfWeek]       SMALLDATETIME NULL,
    CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED ([DateID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [INDEX_Calender_thedate]
    ON [WH].[Calendar]([TheDate] ASC);


GO
CREATE NONCLUSTERED INDEX [index_thedate_dayofweek]
    ON [WH].[Calendar]([TheDate] ASC)
    INCLUDE([DayOfWeek], [DayOfWeekKey]);


﻿create procedure ETL.RemoveAPCWLBrokerTransfer as

insert dbo.[1a_wl_closed episodes]
(
	 InternalNo
	,WL_epi_no
)

select distinct
	 SourcePatientNo
	,SourceEncounterNo
from
	WarehouseOLAPMergedV2.APCWL.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.APCWL.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

inner join WarehouseOLAPMergedV2.APCWL.WaitingList
on	WaitingList.SourceWaitingListID = Reference.WaitingListID
and	WaitingList.IsOriginator = 0
and	WaitingList.IsTransferWaitingList = 1

where
	not exists
		(
		select
			1
		from
			dbo.[1a_wl_closed episodes] ClosedWL
		where
			ClosedWL.InternalNo = Encounter.SourcePatientNo
		and	ClosedWL.WL_epi_no = Encounter.SourceEncounterNo
		)
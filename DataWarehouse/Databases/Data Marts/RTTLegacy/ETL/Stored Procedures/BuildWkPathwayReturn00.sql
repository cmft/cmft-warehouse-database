﻿
CREATE procedure [ETL].[BuildWkPathwayReturn00]
	--@CensusDate date
as

/******************************************************************************
**  Name: [ETL].[BuildWkPathwayReturn00]
**  Purpose: RTT populates the ETL.WkPathwayReturn00 from Pathways_All
**
**	Input Parameters
**		Census Date to populate 
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 16.08.12      CB       Created
** 18.10.12      MH       Removed and then restored reference to WarehouseSQL
******************************************************************************/

DECLARE @StartTime		DATETIME = GETDATE()
DECLARE @Elapsed		INT
DECLARE @Stats			VARCHAR(MAX)
DECLARE @InsertedRows	INT
DECLARE @Process		VARCHAR(30) = 'BuildWkPathwayReturn00'

set dateformat dmy

truncate table ETL.WkPathwayReturn00

INSERT INTO ETL.WkPathwayReturn00
(
	 [Hospital]
	,[InternalNo]
	,[ReferralDate]
	,[EpisodeNo]
	,[PriorityType]
	,[DischDate]
	,[RefSource]
	,[EpiRefBy]
	,[EpiCode]
	,[Category]
	,[RefReasonCode]
	,[ConType]
	,[Consultant]
	,[Specialty]
	,[doh_spec]
	,[EpPostCode]
	,[CaseNoteNo]
	,[DistrictNo]
	,[WLTreatment]
	,[WLDate]
	,[WLApptType]
	,[Summary1]
	,[Summary2]
	,[DecToRefDate]
	,[DischargeReason]
	,[pathway_updated]
	,[pathway_start_date_original]
	,[pathway_start_date_current]
	,[pathway_treat_by_date]
	,[days_to_treatment]
	,[pathway_status]
	,[pathway_end_date]
	,[pathway_end_reason]
	,[pathway_future_reset_date]
	,[Decision_date_to_treat_in_IP]
	,[OP_treat_date]
	,[pathway_ID]
	,[last_att_date]
	,[last_att_status]
	,[last_appt_date]
	,[last_appt_status]
	,[fut_appt_date]
	,[fut_appt_status]
	,[WL_Hospital]
	,[WL_EpisodeNo]
	,[WL_WLDate]
	,[WL_WLCode]
	,[WL_IntMan]
	,[WL_MethAdm]
	,[WL_RemReason]
	,[WL_RemDate]
	,[WL_Cons]
	,[WL_Spec]
	,[WL_doh_spec]
	,[WL_Category]
	,[WL_PrimProc]
	,[WL_orig_date]
	,[WL_admitted]
	,[WL_lastRev]
	,[WL_status]
	,[tci_date]
	,[tci_outcome]
	,[tci_canreas]
	,[path_division]
	,[path_directorate]
	,[path_cons]
	,[path_spec]
	,[path_doh_spec]
	,[path_doh_desc]
	,[pathway_open]
	,[pathway_closed]
	,[pathway_not18wks]
	,[pathway_open_days]
	,[pathway_closed_days]
	,[pathway_open_weeks]
	,[pathway_closed_weeks]
	,[path_closed_wks_DNA_adjs]
	,[path_closed_days_DNA_adjs]
	,[path_open_wks_DNA_adjs]
	,[path_open_days_DNA_adjs]
	,[sus_days_rem]
	,[datebefore]
	,[surname]
	,[forename]
	,[init]
	,[rad_casenote]
	,[rad_on_wl]
	,[rad_exam_date]
	,[op_act]
	,[ip_act]
	,[ContractId]
	,[currentContractId]
	,[type]
	,[pct_old]
	,[pct_new]
	,[days_to_breach]
	,[bandbreach]
	,[sent]
	,[treattype]
	,[clos]
	,[exc]
	,[daysrem]
	,[adjdays]
	,[sus_days_pat]
	,[diag]
	,[tci_fut_app_date]
	,[disn]
	,[pat_strst]
	,[DA_audiology]
	,[anap]
	,[OP_plan]
	,[TCI_canc]
	,[Pause?]
	,[Pause Start Date]
	,[RTT_Pathway_ID]
	,[RTT_Pathway_ID_2]
	,[P2WW]
	,[OldTreatByDate]
	,[Comment1]
	,[Comment2]
	,DateOfBirth
)

SELECT
	 [Hospital]
	,Pathway.[InternalNo]
	,[ReferralDate]
	,Pathway.[EpisodeNo]
	,[PriorityType]
	,[DischDate]
	,[RefSource]
	,[EpiRefBy]
	,[EpiCode]
	,[Category]
	,[RefReasonCode]
	,[ConType]
	,[Consultant]
	,[Specialty]
	,[doh_spec]
	,[EpPostCode]
	,[CaseNoteNo]
	,Pathway.[DistrictNo]
	,[WLTreatment]
	,[WLDate]
	,[WLApptType]
	,[Summary1]
	,[Summary2]
	,[DecToRefDate]
	,[DischargeReason]
	,[pathway_updated]
	,[pathway_start_date_original]
	,[pathway_start_date_current]
	,[pathway_treat_by_date]
	,[days_to_treatment]
	,[pathway_status]
	,[pathway_end_date]
	,[pathway_end_reason]
	,[pathway_future_reset_date]
	,[Decision_date_to_treat_in_IP]
	,[OP_treat_date]
	,[pathway_ID]
	,[last_att_date]
	,[last_att_status]
	,[last_appt_date]
	,[last_appt_status]
	,[fut_appt_date]
	,[fut_appt_status]
	,[WL_Hospital]
	,[WL_EpisodeNo]
	,[WL_WLDate]
	,[WL_WLCode]
	,[WL_IntMan]
	,[WL_MethAdm]
	,[WL_RemReason]
	,[WL_RemDate]
	,[WL_Cons]
	,[WL_Spec]
	,[WL_doh_spec]
	,[WL_Category]
	,[WL_PrimProc]
	,[WL_orig_date]
	,[WL_admitted]
	,[WL_lastRev]
	,[WL_status]
	,[tci_date]
	,[tci_outcome]
	,[tci_canreas]
	,[path_division]
	,[path_directorate]
	,[path_cons]
	,[path_spec]
	,[path_doh_spec]
	,[path_doh_desc]
	,[pathway_open]
	,[pathway_closed]
	,[pathway_not18wks]
	,[pathway_open_days]
	,[pathway_closed_days]

	--,pathway_open_weeks = 
	--	case
	--	when [pathway_not18wks] is null then
	--		case
	--		when [pathway_end_date] is null then
	--			datediff(
	--				 day
	--				,[pathway_start_date_current]
	--				,@CensusDate
	--			) / 7
	--		end
	--	end
	,pathway_open_weeks


	,[pathway_closed_weeks]
	,[path_closed_wks_DNA_adjs]
	,[path_closed_days_DNA_adjs]
	,[path_open_wks_DNA_adjs]


	--,path_open_days_DNA_adjs =
	--	case
	--	when [pathway_not18wks] Is Null then
	--		case
	--		when [pathway_end_date] Is Null	then
	--			datediff(
	--				 day
	--				,[pathway_start_date_current]
	--				,@CensusDate
	--			)
	--		end
	--	end
	,path_open_days_DNA_adjs


	,[sus_days_rem]
	,DateBefore = 
		DateAdd(
			 d
			,-1
			,[datebefore]
		)
	,Pathway.[surname]
	,[forename]
	,[init]
	,[rad_casenote]
	,[rad_on_wl]
	,[rad_exam_date]
	,[op_act]
	,[ip_act]
	,[ContractId]
	,[currentContractId]
	,[type]

	,pct_old =
	--	coalesce(
		--	 left([currentContractId] , 3)
		--	,Left([contractid] , 3)
		--	,Practice.ParentOrganisationCode
		--	,replace(Postcode.PCTCode , 'X98' , null)
		--	,'NONC'
		--)
		null
	,pct_new = 
		--coalesce(
		--	 left([currentContractId] , 3)
		--	,Left([contractid] , 3)
		--	,Practice.ParentOrganisationCode
		--	,replace(Postcode.PCTCode , 'X98' , null)
		--	,'NONC'
		--)
		coalesce(
			 CCGPractice.ParentOrganisationCode
			,replace(CCGPostcode.CCGCode , 'X98' , '00W')
			,'NONC'
		)

	,[days_to_breach]
	,[bandbreach]
	,[sent]
	,[treattype]
	,[clos]
	,[exc]
	,[daysrem]
	,[adjdays]
	,[sus_days_pat]
	,[diag]
	,[tci_fut_app_date]
	,[disn]
	,[pat_strst]
	,[DA_audiology]
	,[anap]
	,[OP_plan]
	,[TCI_canc]
	,[Pause?]
	,[Pause Start Date]
	,[RTT_Pathway_ID]
	,[RTT_Pathway_ID_2]
	,[P2WW]
	,[OldTreatByDate]
	,BacklogComments.Comment
	,BacklogComments.PComment
	,Patient.DateOfBirth

FROM
	PATHWAYS_all Pathway

LEFT JOIN WarehouseSQL.Warehouse.PAS.Patient
	ON	Patient.SourcePatientNo = Pathway.InternalNo

LEFT JOIN WarehouseSQL.Warehouse.PAS.Gp RegisteredGp 
	ON	Patient.RegisteredGpCode = RegisteredGp.GpCode

--LEFT JOIN WarehouseSQL.Organisation.dbo.Practice Practice
--	ON	Practice.OrganisationCode = RegisteredGp.PracticeCode

left join Organisation.dbo.CCGPractice
on	CCGPractice.OrganisationCode = RegisteredGp.PracticeCode

--LEFT JOIN Organisation.dbo.Postcode Postcode
--	ON	Postcode.Postcode =
--			case
--			when datalength(rtrim(ltrim(Patient.Postcode))) = 6 then left(Patient.Postcode, 2) + '   ' + right(Patient.Postcode, 3)
--			when datalength(rtrim(ltrim(Patient.Postcode))) = 7 then left(Patient.Postcode, 3) + '  ' + right(Patient.Postcode, 3)
--			else Patient.Postcode
--			end

LEFT JOIN Organisation.dbo.CCGPostcode CCGPostcode
	ON	CCGPostcode.Postcode =
			case
			when datalength(rtrim(ltrim(Patient.Postcode))) = 6 then left(Patient.Postcode, 2) + '   ' + right(Patient.Postcode, 3)
			when datalength(rtrim(ltrim(Patient.Postcode))) = 7 then left(Patient.Postcode, 3) + '  ' + right(Patient.Postcode, 3)
			else Patient.Postcode
			end

LEFT OUTER JOIN ManualAdjustment.rtt.BacklogComments AS BacklogComments 
	ON		BacklogComments.InternalNo	= Pathway.InternalNo
		AND BacklogComments.EpisodeNo	= Pathway.EpisodeNo

where
	[Hospital] != 'Traf'

	SELECT @InsertedRows = @@ROWCOUNT

-- Audit Log - Run Times
	SELECT @Elapsed = DATEDIFF(MINUTE, @StartTime, GETDATE())

	SELECT @Stats = 'Time Elapsed ' 
					+ CONVERT(char(3), @Elapsed) 
					+ ' Mins from ' 
					+ CAST(@StartTime AS NVARCHAR(20))
					+ ' Rows Inserted '
					+ CAST(@InsertedRows AS NVARCHAR(10))

	exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime



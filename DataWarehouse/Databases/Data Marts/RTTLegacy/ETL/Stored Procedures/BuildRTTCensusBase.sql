﻿CREATE proc [ETL].[BuildRTTCensusBase]
as

set xact_abort on

begin tran

	truncate table RTT.CensusBase

	set dateformat dmy

	insert into	RTT.CensusBase
	(
		 CensusDate
		,Census
	)

	select
		 CensusDate = Snapshot.TheDate
		,Census =
			convert(varchar , Snapshot.TheDate , 106)
	from
		(
		select
			TheDate = max(TheDate)
		from
			WarehouseOLAPMergedV2.WH.Calendar
		group by
				year(TheDate)
			,month(TheDate)

		union
	--Sunday snapshots
		select
			TheDate
		from
			WarehouseOLAPMergedV2.WH.Calendar
		where
			datename(dw, TheDate) = 'Sunday'

		union

		select
			convert(date , max(CensusDate))
		from
			RTT.Fact
		) Snapshot
	where
		exists
			(
			select
				1
			from
				RTT.Fact
			where
				Fact.CensusDate = Snapshot.TheDate
			)
	order by
		Snapshot.TheDate desc

commit

set xact_abort off
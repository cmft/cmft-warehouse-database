﻿

CREATE procedure [ETL].[LoadRTT]
(
	 @RunDate			DATE = NULL
	,@TraffordRunDate	DATE = NULL
)
as

/******************************************************************************
**  Name: ETL.LoadRTT
**  Purpose: Controlling procedure for populating the RTT fact and drill through tables
**
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 21.08.12      MH       Created
** 17.06.14		 GC		  Added @TraffordRTTNextWeekEndDate
******************************************************************************/

	-- If no date passed as a parameter set the Census date as yesterday
	DECLARE @CensusDate				DATE = COALESCE(@RunDate, DATEADD(DAY, -1, CAST(GETDATE() AS DATE)))
	DECLARE @TraffordCensusDate     DATE = COALESCE(@TraffordRunDate, DATEADD(DAY, 1, @CensusDate)) -- Trafford CensusDate stamped on the date created
	DECLARE @RTTNextMonthEndDate	DATE
	DECLARE @RTTNextWeekEndDate	DATE
	DECLARE @TraffordRTTNextWeekEndDate	DATE
	

	DECLARE @StartTime		DATETIME = GETDATE()
	DECLARE @Elapsed		INT
	DECLARE @Stats			VARCHAR(MAX)
	DECLARE @InsertedRows	INT
	DECLARE @Process		VARCHAR(30) = 'LoadRTT'

	-- Build the CMAN staging table
	EXEC ETL.BuildWkPathwayReturn00 

	-- Build the Fact table - note this will bring in the Trafford and GUM data also
	EXEC RTT.BuildFact @CensusDate, @TraffordCensusDate

	-- Build the RTT.Encounter table from the Fact table
	EXEC RTT.BuildRTTEncounter @CensusDate

	-- Get the next RTT return month end date.
	-- This value will be updated externally to the following month end date once the data is designated as frozen regarding
	-- the current month end date.
	SELECT	
		@RTTNextMonthEndDate = DateValue
	FROM
		Utility.Parameter
	WHERE
		Parameter = 'RTTNEXTMONTHENDDATE'

	-- If the month end date is earlier than the current date, this flags the month end position should be updated
	-- with any changes input after the month end.
	IF (@RTTNextMonthEndDate < @CensusDate)
	  BEGIN
		EXEC RTT.BuildFact @RTTNextMonthEndDate, @RTTNextMonthEndDate
		EXEC RTT.BuildRTTEncounter @RTTNextMonthEndDate
	  END
	  
	  
	  ---Process week end date ---
	  
	  SELECT	
		 @RTTNextWeekEndDate = DateValue
		,@TraffordRTTNextWeekEndDate =  DATEADD(DAY, 1, DateValue)
	FROM
		Utility.Parameter
	WHERE
		Parameter = 'RTTNEXTWEEKENDDATE'

	-- If the month end date is earlier than the current date, this flags the month end position should be updated
	-- with any changes input after the month end.
	IF (@RTTNextWeekEndDate < @CensusDate)
	  BEGIN
		EXEC RTT.BuildFact @RTTNextWeekEndDate, @TraffordRTTNextWeekEndDate
		EXEC RTT.BuildRTTEncounter @RTTNextWeekEndDate
	  END

-- Build Census Table
exec ETL.BuildRTTCensusBase

-- Audit Log - Run Times
	SELECT @Elapsed = DATEDIFF(MINUTE, @StartTime, GETDATE())

	SELECT @Stats = 'Time Elapsed ' 
					+ CONVERT(char(3), @Elapsed) 
					+ ' Mins from ' 
					+ CAST(@StartTime AS NVARCHAR(20))
					+ ' Census Date ' 
					+ CAST(@CensusDate AS NVARCHAR(20))

	exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

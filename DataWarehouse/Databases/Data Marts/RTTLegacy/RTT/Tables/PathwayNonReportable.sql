﻿CREATE TABLE [RTT].[PathwayNonReportable] (
    [ID]         INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo] NVARCHAR (9)   NOT NULL,
    [EpisodeNo]  NVARCHAR (9)   NOT NULL,
    [FromDate]   DATE           NOT NULL,
    [ToDate]     DATE           NULL,
    [Comments]   NVARCHAR (100) NULL,
    CONSTRAINT [PK_RTTPathwayNonReportableID] PRIMARY KEY CLUSTERED ([ID] ASC)
);


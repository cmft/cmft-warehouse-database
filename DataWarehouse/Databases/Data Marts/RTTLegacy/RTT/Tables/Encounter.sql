﻿CREATE TABLE [RTT].[Encounter] (
    [Hospital]                    VARCHAR (50)  NULL,
    [InternalNo]                  VARCHAR (50)  NULL,
    [ReferralDate]                SMALLDATETIME NULL,
    [EpisodeNo]                   VARCHAR (50)  NULL,
    [RefSource]                   VARCHAR (50)  NULL,
    [Consultant]                  VARCHAR (255) NULL,
    [Specialty]                   VARCHAR (10)  NULL,
    [doh_spec]                    VARCHAR (10)  NULL,
    [DistrictNo]                  VARCHAR (50)  NULL,
    [pathway_start_date_original] SMALLDATETIME NULL,
    [pathway_start_date_current]  SMALLDATETIME NULL,
    [pathway_treat_by_date]       SMALLDATETIME NULL,
    [pathway_end_date]            SMALLDATETIME NULL,
    [pathway_ID]                  VARCHAR (50)  NULL,
    [WL_WLDate]                   DATETIME      NULL,
    [WL_IntMan]                   VARCHAR (1)   NULL,
    [WL_cons]                     VARCHAR (4)   NULL,
    [WL_Spec]                     VARCHAR (4)   NULL,
    [WL_doh_spec]                 INT           NULL,
    [WL_PrimProc]                 VARCHAR (8)   NULL,
    [tci_date]                    SMALLDATETIME NULL,
    [DecidedToAdmitDate]          SMALLDATETIME NULL,
    [path_division]               VARCHAR (50)  NULL,
    [path_directorate]            VARCHAR (50)  NULL,
    [path_cons]                   VARCHAR (255) NULL,
    [path_spec]                   VARCHAR (255) NULL,
    [path_doh_spec]               VARCHAR (50)  NULL,
    [path_doh_desc]               VARCHAR (255) NULL,
    [path_closed_wks_DNA_adjs]    INT           NULL,
    [path_closed_days_DNA_adjs]   INT           NULL,
    [path_open_wks_DNA_adjs]      INT           NULL,
    [path_open_days_DNA_adjs]     INT           NULL,
    [Surname]                     VARCHAR (255) NULL,
    [Forename]                    VARCHAR (255) NULL,
    [type]                        VARCHAR (10)  NULL,
    [pct_new]                     VARCHAR (50)  NULL,
    [treattype]                   VARCHAR (10)  NULL,
    [Rundate]                     DATETIME      NULL,
    [Audit]                       VARCHAR (255) NULL,
    [Censusdate]                  DATETIME      NULL,
    [PCTCode]                     VARCHAR (10)  NULL,
    [AdjustedFlag]                VARCHAR (1)   NULL,
    [SourceUniqueID]              VARCHAR (50)  NULL,
    [EncounterRecNo]              INT           IDENTITY (1, 1) NOT NULL,
    [FactEncounterRecNo]          INT           NULL,
    [Cases]                       INT           NULL,
    [Casenote]                    VARCHAR (50)  NULL,
    [WL_WaitingListCode]          VARCHAR (50)  NULL,
    [RTTBreachDate]               SMALLDATETIME NULL,
    [TCIBeyond]                   BIT           NULL,
    [PauseStartDate]              SMALLDATETIME NULL,
    [WeekBandReturnCode]          VARCHAR (5)   NULL,
    [DecisionToTreatFlag]         BIT           NULL,
    [WaitingListFlag]             BIT           NULL,
    [DiagnosticFlag]              BIT           NULL,
    [Comment1]                    VARCHAR (MAX) NULL,
    [Comment2]                    VARCHAR (MAX) NULL,
    [WeekBandCode]                VARCHAR (5)   NULL,
    [PathwayStatusCode]           VARCHAR (3)   NULL,
    [WorkflowStatusCode]          VARCHAR (1)   NULL,
    [BreachBandCode]              VARCHAR (50)  NULL,
    [DaysWaiting]                 INT           NULL,
    [PathwayStatus]               VARCHAR (100) NULL,
    [LastAttendanceStatusCode]    VARCHAR (50)  NULL,
    [RTTPathwayID]                VARCHAR (50)  NULL,
    [DateOfBirth]                 DATETIME      NULL,
    [ContractID]                  VARCHAR (20)  NULL,
    [CurrentContractID]           VARCHAR (20)  NULL,
    [fut_appt_date]               DATETIME2 (7) NULL,
    [ReportableFlag]              NVARCHAR (10) NULL,
    CONSTRAINT [PK_Encounter] PRIMARY KEY NONCLUSTERED ([EncounterRecNo] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [ClusteredIndex-20120828-135751]
    ON [RTT].[Encounter]([EncounterRecNo] ASC) WITH (DATA_COMPRESSION = PAGE);


GO
CREATE NONCLUSTERED INDEX [index_encounter_CensusAdjustedStatusCode]
    ON [RTT].[Encounter]([Censusdate] ASC, [AdjustedFlag] ASC, [PathwayStatusCode] ASC)
    INCLUDE([Hospital], [Specialty], [tci_date], [path_division], [RTTBreachDate], [TCIBeyond], [WaitingListFlag]);


GO
CREATE NONCLUSTERED INDEX [index_rttencounter_backlogcensus]
    ON [RTT].[Encounter]([InternalNo] ASC, [EpisodeNo] ASC, [Censusdate] ASC)
    INCLUDE([WeekBandCode], [BreachBandCode]);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter]
    ON [RTT].[Encounter]([FactEncounterRecNo] ASC);


﻿CREATE TABLE [RTT].[TraffordArchive] (
    [Hospital]                    VARCHAR (50)  NULL,
    [InternalNo]                  VARCHAR (50)  NULL,
    [ReferralDate]                DATETIME      NULL,
    [EpisodeNo]                   VARCHAR (50)  NULL,
    [RefSource]                   VARCHAR (50)  NULL,
    [Consultant]                  VARCHAR (255) NULL,
    [Specialty]                   VARCHAR (10)  NULL,
    [doh_spec]                    VARCHAR (10)  NULL,
    [DistrictNo]                  VARCHAR (50)  NULL,
    [pathway_start_date_original] DATETIME      NULL,
    [pathway_start_date_current]  DATETIME      NULL,
    [pathway_treat_by_date]       DATETIME      NULL,
    [pathway_end_date]            DATETIME      NULL,
    [pathway_ID]                  VARCHAR (50)  NULL,
    [WL_WLDate]                   DATETIME      NULL,
    [WL_IntMan]                   INT           NULL,
    [WL_cons]                     VARCHAR (50)  NULL,
    [WL_Spec]                     INT           NULL,
    [WL_doh_spec]                 INT           NULL,
    [WL_PrimProc]                 INT           NULL,
    [tci_date]                    DATETIME      NULL,
    [DecidedToAdmitDate]          DATETIME      NULL,
    [path_division]               VARCHAR (50)  NULL,
    [path_directorate]            VARCHAR (50)  NULL,
    [path_cons]                   VARCHAR (255) NULL,
    [path_spec]                   VARCHAR (255) NULL,
    [path_doh_spec]               VARCHAR (50)  NULL,
    [path_doh_desc]               VARCHAR (255) NULL,
    [path_closed_wks_DNA_adjs]    INT           NULL,
    [path_closed_days_DNA_adjs]   INT           NULL,
    [path_open_wks_DNA_adjs]      INT           NULL,
    [path_open_days_DNA_adjs]     INT           NULL,
    [Surname]                     VARCHAR (255) NULL,
    [Forename]                    VARCHAR (255) NULL,
    [type]                        VARCHAR (6)   NOT NULL,
    [pct_new]                     VARCHAR (50)  NULL,
    [treattype]                   VARCHAR (10)  NOT NULL,
    [Rundate]                     DATETIME      NULL,
    [Comment1]                    TEXT          NULL,
    [Censusdate]                  DATETIME      NULL,
    [PCTCode]                     VARCHAR (10)  NULL,
    [AdjustedFlag]                VARCHAR (1)   NULL,
    [EncounterRecno]              INT           NULL,
    [SourceUniqueID]              VARCHAR (50)  NULL,
    [DecisionToTreatFlag]         BIT           NULL,
    [WaitingListFlag]             BIT           NULL,
    [DiagnosticFlag]              BIT           NULL,
    [PauseStart]                  DATETIME      NULL,
    [TCIBeyond]                   BIT           NULL,
    [RTTBreachDate]               DATETIME      NULL,
    [WL_waitinglistcode]          VARCHAR (50)  NULL,
    [Casenote]                    VARCHAR (50)  NULL,
    [AppointmentType]             VARCHAR (50)  NULL
);


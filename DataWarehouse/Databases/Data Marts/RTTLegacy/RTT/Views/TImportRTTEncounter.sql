﻿




CREATE VIEW [RTT].[TImportRTTEncounter] AS

/******************************************************************************
**  Name: RTT.TImportRTTEncounter
**  Purpose: View including transformations used to populate the RTT.Encounter table
**
**	NOTE - Please maintain this function via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.08.12      MH       Created
** 03.09.12      CB       Added extra columns
** 15.10.12      MH       Set treat column to CLOSED and treattype to 'NON IP' for GUM data
******************************************************************************/

	SELECT 
	   Hospital								= F.SiteCode
      ,InternalNo							= STCMAN.InternalNo
	  ,Cases								= 1
      ,ReferralDate							= STCMAN.ReferralDate
      ,EpisodeNo							= STCMAN.EpisodeNo
      ,RefSource							= STCMAN.RefSource
      ,Consultant							= CB.Surname + ', ' + CB.Inits
      ,Specialty							= STCMAN.doh_spec
      ,doh_spec								= STCMAN.doh_spec
      ,DistrictNo							= STCMAN.DistrictNo
      ,pathway_start_date_original			= STCMAN.pathway_start_date_original
      ,pathway_start_date_current			= STCMAN.pathway_start_date_current
      ,pathway_treat_by_date				= STCMAN.pathway_treat_by_date
      ,pathway_end_date						= STCMAN.pathway_end_date
      ,pathway_ID							= STCMAN.pathway_ID
      ,WL_WLDate							= STCMAN.WL_WLDate
      ,WL_IntMan							= STCMAN.WL_IntMan
      ,WL_cons								= STCMAN.WL_Cons
      ,WL_Spec								= STCMAN.WL_Spec
      ,WL_doh_spec							= STCMAN.WL_doh_spec
      ,WL_PrimProc							= STCMAN.WL_PrimProc
      ,tci_date								= STCMAN.tci_date
      ,DecidedToAdmitDate					= STCMAN.Decision_date_to_treat_in_IP
      ,path_division						= F.Division
      ,path_directorate						= F.Directorate
      ,path_cons							= STCMAN.path_cons
      ,path_spec							= STCMAN.path_spec
      ,path_doh_spec						= STCMAN.path_doh_spec
      ,path_doh_desc						= STCMAN.path_doh_desc
      ,path_closed_wks_DNA_adjs				= STCMAN.path_closed_wks_DNA_adjs
      ,path_closed_days_DNA_adjs			= STCMAN.path_closed_days_DNA_adjs
      ,path_open_wks_DNA_adjs				= STCMAN.path_open_wks_DNA_adjs
      ,path_open_days_DNA_adjs				= STCMAN.path_open_days_DNA_adjs
      ,Surname								= STCMAN.surname
      ,Forename								= STCMAN.forename
      ,[type]								= STCMAN.[type]
      ,pct_new								= STCMAN.pct_new
      ,treattype							= STCMAN.treattype
      ,Censusdate							= F.CensusDate
	  ,SourceCensusDate						= F.SourceCensusDate
      ,PCTCode								= F.PCTCode
      ,AdjustedFlag							= F.AdjustedFlag
      ,FactEncounterRecno					= F.EncounterRecno
      ,SourceUniqueID						= F.SourceUniqueID
	  ,Casenote								= STCMAN.CaseNoteNo
	  ,WL_WaitingListCode					= STCMAN.WL_WLCode
	  ,RTTBreachDate						= F.RTTBreachDate
	  ,TCIBeyond							= F.TCIBeyondBreach
	  ,PauseStartDate						= STCMAN.[Pause Start Date]
	  ,WeekBandReturnCode					= F.WeekBandReturnCode
	  ,DecisionToTreatFlag					= F.DecisionToTreatFlag
	  ,WaitingListFlag						= F.WaitingListFlag
	  ,DiagnosticFlag						= F.DiagnosticFlag
	  ,Comment1								= STCMAN.Comment1
	  ,Comment2								= STCMAN.Comment2
	  ,WeekBandCode							= F.WeekBandCode
      ,PathwayStatusCode					= F.PathwayStatusCode
      ,WorkflowStatusCode					= F.WorkflowStatusCode
      ,BreachBandCode						= F.BreachBandCode
      ,DaysWaiting							= F.DaysWaiting
	  ,PathwayStatus						= STCMAN.pathway_status
	  ,LastAttendanceStatusCode				= STCMAN.last_att_status
	  ,RTTPathwayID							= STCMAN.RTT_Pathway_ID
	  ,DateOfBirth							= STCMAN.DateOfBirth
	,ContractID = STCMAN.ContractId
	,CurrentContractID = STCMAN.currentContractId
	,fut_appt_date = STCMAN.fut_appt_date
	,F.ReportableFlag

  FROM 
	RTT.Fact F

  INNER JOIN ETL.WkPathwayReturn00 STCMAN
	ON F.SourceUniqueID = STCMAN.InternalNo + '||' + STCMAN.EpisodeNo
  
  LEFT JOIN warehouse.PAS.ConsultantBase CB
	ON STCMAN.Consultant = CB.Consultant

  WHERE
		F.SiteCode != 'Traf'
	AND	COALESCE(F.SpecialtyCode,'') != 'GUM'

  UNION 
 
  SELECT 
	   Hospital								= STTRAFF.[Hospital]
      ,InternalNo							= STTRAFF.[InternalNo]
	  ,Cases								= 1								
      ,ReferralDate							= STTRAFF.[ReferralDate]
      ,EpisodeNo							= STTRAFF.[EpisodeNo]
      ,RefSource							= STTRAFF.[RefSource]
      ,Consultant							= STTRAFF.[Consultant]
      ,Specialty							= STTRAFF.[Specialty]
      ,doh_spec								= STTRAFF.[doh_spec]
      ,DistrictNo							= STTRAFF.[DistrictNo]
      ,pathway_start_date_original			= STTRAFF.[pathway_start_date_original]
      ,pathway_start_date_current			= STTRAFF.[pathway_start_date_current]
      ,pathway_treat_by_date				= STTRAFF.[pathway_treat_by_date]
      ,pathway_end_date						= STTRAFF.[pathway_end_date]
      ,pathway_ID							= STTRAFF.[pathway_ID]
      ,WL_WLDate							= NULL --STTRAFF.[WL_WLDate] 
      ,WL_IntMan							= NULL --STTRAFF.[WL_IntMan]
      ,WL_cons								= NULL --STTRAFF.[WL_cons]
      ,WL_Spec								= NULL --STTRAFF.[WL_Spec]
      ,WL_doh_spec							= NULL --STTRAFF.[WL_doh_spec]
      ,WL_PrimProc							= NULL --STTRAFF.[WL_PrimProc]
      ,tci_date								= STTRAFF.[tci_date]
      ,DecidedToAdmitDate					= STTRAFF.[DecidedToAdmitDate]
      ,path_division						= F.Division
      ,path_directorate						= F.Directorate
      ,path_cons							= STTRAFF.[path_cons]
      ,path_spec							= STTRAFF.[path_spec]
      ,path_doh_spec						= STTRAFF.[path_doh_spec]
      ,path_doh_desc						= STTRAFF.[path_doh_desc]
      ,path_closed_wks_DNA_adjs				= STTRAFF.[path_closed_wks_DNA_adjs]
      ,path_closed_days_DNA_adjs			= STTRAFF.[path_closed_days_DNA_adjs]
      ,path_open_wks_DNA_adjs				= STTRAFF.[path_open_wks_DNA_adjs]
      ,path_open_days_DNA_adjs				= STTRAFF.[path_open_days_DNA_adjs]
      ,Surname								= STTRAFF.[Surname]
      ,Forename								= STTRAFF.[Forename]
      ,type									= STTRAFF.[type]
      ,pct_new								= STTRAFF.[pct_new]
      ,treattype							= STTRAFF.[treattype]
      ,Censusdate							= F.[Censusdate]
	  ,SourceCensusDate						= F.SourceCensusDate
      ,PCTCode								= STTRAFF.[PCTCode]
      ,AdjustedFlag							= STTRAFF.[AdjustedFlag]
      ,FactEncounterRecno					= F.[EncounterRecno]
      ,SourceUniqueID						= F.[SourceUniqueID]
	  ,Casenote								= STTRAFF.Casenote
	  ,WL_WaitingListCode					= STTRAFF.WL_WaitingListCode
	  ,RTTBreachDate						= F.RTTBreachDate
	  ,TCIBeyond							= F.TCIBeyondBreach
	  ,PauseStartDate						= STTRAFF.PauseStart
	  ,WeekBandReturnCode					= F.WeekBandReturnCode
	  ,DecisionToTreatFlag					= F.DecisionToTreatFlag
	  ,WaitingListFlag						= F.WaitingListFlag
	  ,DiagnosticFlag						= F.DiagnosticFlag
	  ,Comment1								= CAST(STTRAFF.Comment1 AS VARCHAR(MAX))
	  ,Comment2								= NULL 
	  ,WeekBandCode							= F.WeekBandCode
      ,PathwayStatusCode					= F.PathwayStatusCode
      ,WorkflowStatusCode					= F.WorkflowStatusCode
      ,BreachBandCode						= F.BreachBandCode
      ,DaysWaiting							= F.DaysWaiting
	  ,PathwayStatus						= null
	  ,LastAttendanceStatusCode				= null
	  ,RTTPathwayID							= STTRAFF.pathway_ID
	  ,DateOfBirth							= null
	,ContractID = null
	,CurrentContractID = null
	,fut_appt_date = STTRAFF.FutureAPPDate
	,F.ReportableFlag

  FROM 
		RTT.Fact F
  INNER JOIN WarehouseSQL.TraffordWarehouse.dbo.TraffordRTT  STTRAFF
	ON		F.SourceUniqueID = STTRAFF.InternalNo + '||' + STTRAFF.EpisodeNo + '||' + CAST(STTRAFF.EncounterRecno AS VARCHAR(12)) + '||' + STTRAFF.AdjustedFlag
		AND F.SourceCensusDate = STTRAFF.CensusDate

  WHERE
		F.SiteCode = 'TRAF'

  UNION 
 
  SELECT 
	   Hospital								= F.[SiteCode]	
      ,InternalNo							= NULL		
	  ,Cases								= F.Cases
      ,ReferralDate							= NULL 
      ,EpisodeNo							= NULL	
      ,RefSource							= NULL	
      ,Consultant							= NULL	
      ,Specialty							= SP.NationalSpecialtyCode	
      ,doh_spec								= SP.NationalSpecialtyCode
      ,DistrictNo							= NULL 
      ,pathway_start_date_original			= NULL 
      ,pathway_start_date_current			= NULL
      ,pathway_treat_by_date				= NULL
      ,pathway_end_date						= NULL
      ,pathway_ID							= NULL 
      ,WL_WLDate							= NULL
      ,WL_IntMan							= NULL 
      ,WL_cons								= NULL
      ,WL_Spec								= NULL
      ,WL_doh_spec							= NULL
      ,WL_PrimProc							= NULL
      ,tci_date								= NULL 
      ,DecidedToAdmitDate					= NULL
      ,path_division						= F.Division
      ,path_directorate						= F.Directorate	
      ,path_cons							= NULL 
      ,path_spec							= F.SpecialtyCode
      ,path_doh_spec						= SP.NationalSpecialtyCode
      ,path_doh_desc						= NULL
      ,path_closed_wks_DNA_adjs				= NULL
      ,path_closed_days_DNA_adjs			= NULL
      ,path_open_wks_DNA_adjs				= NULL
      ,path_open_days_DNA_adjs				= NULL 
      ,Surname								= NULL 
      ,Forename								= NULL
      ,type									= 'CLOSED'
      ,pct_new								= NULL
      ,treattype							= 'NON IP'
      ,Censusdate							= F.Censusdate
	  ,SourceCensusDate						= F.SourceCensusDate
      ,PCTCode								= F.PCTCode
      ,AdjustedFlag							= F.AdjustedFlag
      ,EncounterRecno						= F.EncounterRecno
      ,SourceUniqueID						= F.SourceUniqueID
	  ,Casenote								= NULL
	  ,WL_WaitingListCode					= NULL 
	  ,RTTBreachDate						= F.RTTBreachDate
	  ,TCIBeyond							= F.TCIBeyondBreach
	  ,PauseStartDate						= NULL
	  ,WeekBandReturnCode					= F.WeekBandReturnCode
	  ,DecisionToTreatFlag					= F.DecisionToTreatFlag
	  ,WaitingListFlag						= F.WaitingListFlag
	  ,DiagnosticFlag						= F.DiagnosticFlag
	  ,Comment1								= NULL 
	  ,Comment2								= NULL 
	  ,WeekBandCode							= F.WeekBandCode
      ,PathwayStatusCode					= F.PathwayStatusCode
      ,WorkflowStatusCode					= F.WorkflowStatusCode
      ,BreachBandCode						= F.BreachBandCode
      ,DaysWaiting							= F.DaysWaiting
	  ,PathwayStatus						= null
	  ,LastAttendanceStatusCode				= null
	  ,RTTPathwayID							= null
	  ,DateOfBirth							= null
	,ContractID = null
	,CurrentContractID = null
	,fut_appt_date = null
	,F.ReportableFlag
	
  FROM 
		RTT.Fact F

  LEFT JOIN warehouse.PAS.Specialty SP
	ON F.SpecialtyCode = SP.SpecialtyCode
 
  WHERE
		F.SpecialtyCode = 'GUM'
	AND	F.SiteCode		= 'MRI'






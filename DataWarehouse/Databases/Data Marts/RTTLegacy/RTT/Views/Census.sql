﻿



CREATE view [RTT].[Census]
as

--select
--	 CensusDate
--	,Census =
--		convert(varchar , CensusDate , 106)
--from
--	(
--	select
--		CensusDate = max(CensusDate)
--	from
--		RTT.Fact
--	group by
--		 year(CensusDate)
--		,month(CensusDate)
--	) Census

select
	 CensusDate
	,Census
from
	RTT.CensusBase


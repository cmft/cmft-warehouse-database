﻿
CREATE view RTT.[PathwayStatus] as
select
	PathwayStatusBase.PathwayStatusCode,
	PathwayStatusBase.PathwayStatus,
	NationalPathwayStatusBase.NationalPathwayStatusCode,
	NationalPathwayStatusBase.NationalPathwayStatus
from
	RTT.PathwayStatusBase

inner join RTT.NationalPathwayStatusBase
on	NationalPathwayStatusBase.NationalPathwayStatusCode = PathwayStatusBase.NationalPathwayStatusCode

﻿
CREATE procedure [RTT].[BuildFact]  
(
	  @CensusDate smalldatetime
	 ,@TraffordCensusDate smalldatetime
)
as

/******************************************************************************
**  Name: RTT.BuildFact
**  Purpose: RTT populates the RTT.Fact table
**
**	Input Parameters
**		Census Date to populate 
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 16.08.12      CB       Created
** 17.08.16      GS       Inclusion of Divison, Directorate and TCIDate columns
** 22.08.12      MH       Inclusion of SourceCensusDate to work around the Trafford Census Date
**                        being 1 day in front of the normal census date definition
** 22.08.12      GS       Change to only bring back closed pathways in month of census date
** 29.08.12      MH       Division and Directorate re-mapping for St Marys
** 03.09.12		 CB		  Bug fix in RTTBreachDate and TCIBeyondBreach attributes
** 15.10.12      MH       Inclusion of CensusDate in the ETL.GUM table
** 17.10.12      MH       GUM data now resides in the warehouse database
** 08.11.12      GS       Fix to open days waiting
** 18.06.14		 RR		  Added NonReportable from DR table
******************************************************************************/

declare
	 @localCensusDate smalldatetime
	,@localFromDate date

	DECLARE @StartTime		DATETIME = GETDATE()
	DECLARE @Elapsed		INT
	DECLARE @Stats			VARCHAR(MAX)
	DECLARE @InsertedRows	INT
	DECLARE @Process		VARCHAR(30) = 'BuildFact'
	declare @DaysBefore18weekBreach int = 127

select
		@localCensusDate = dateadd(minute , -1 , dateadd(day , 1 , @CensusDate))
--     ,@localFromDate = dateadd(month , -1 , dateadd(day , 1 , @CensusDate))
       ,@localFromDate = dateadd(dd, -1 * (day(@CensusDate) -1 ) , @CensusDate) --GS20120822 change to only bring back closed pathways in month of census date


delete
from
	RTT.Fact
where
	Fact.CensusDate = @CensusDate


INSERT INTO RTT.Fact
(
	 CensusDate
	,SourceCensusDate
	,SourceUniqueID
	,EncounterTypeCode
	,ConsultantCode
	,SpecialtyCode
	,NationalSpecialtyCode
	,PracticeCode
	,PCTCode
	,SiteCode
	,WeekBandReturnCode
	,WeekBandCode
	,PathwayStatusCode
	,ReferralDate
	,WorkflowStatusCode
	,BreachBandCode
	,DaysWaiting
	,Cases
	,TCIBeyondBreach
	,LastWeeksCases
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,ReportableFlag
	,AdjustedFlag
	,RTTBreachDate
	,KeyDate
	,ReferringProviderCode
	,RTTFactTypeCode
	,Division
	,Directorate
	,TCIDate
	,DecisionToTreatFlag
	,WaitingListFlag
	,DiagnosticFlag
)

SELECT
	 CensusDate = @CensusDate
	,SourceCensusDate
	,SourceUniqueID
	,EncounterTypeCode
	,ConsultantCode
	,SpecialtyCode
	,NationalSpecialtyCode
	,PracticeCode

	--,PCTCode =
	--	case
	--	when left(Encounter.PCTCode , 3) = '' then '5NT'
	--	when left(Encounter.PCTCode , 1) in ('0' , '4' , '7' , '6' , 'N' , 'S' , '9' , '' , 'Z' , 'Q') then 'NONC'
	--	when isnumeric(Encounter.PCTCode) = 1 then 'NONC'
	--	else coalesce(PCT.RTTPCTCode , left(Encounter.PCTCode , 3) , '5NT')
	--	end

	,PCTCode =
		coalesce(Encounter.PCTCode , '00W')

	,SiteCode
	,WeekBandReturnCode
	,WeekBandCode
	,PathwayStatusCode
	,ReferralDate
	,WorkflowStatusCode

	,BreachBandCode =
		case
		when Encounter.DaysWaiting is null then 'UK' -- patients with unknown clock start date
		when (Encounter.DaysWaiting - @DaysBefore18weekBreach ) between 0 and 6 then 'BR01'
		when (Encounter.DaysWaiting - @DaysBefore18weekBreach ) >= 7 then 'BR02'
		when (Encounter.DaysWaiting - @DaysBefore18weekBreach ) between -7 and -1 then 'F01'
		when (Encounter.DaysWaiting - @DaysBefore18weekBreach ) between -14 and -8 then 'F12'
		when (Encounter.DaysWaiting - @DaysBefore18weekBreach ) between -28 and -15 then 'F24'
		when (Encounter.DaysWaiting - @DaysBefore18weekBreach ) between -42 and -29 then 'F46'
		when (Encounter.DaysWaiting - @DaysBefore18weekBreach ) between -56 and -43 then 'F68'
		else 'F99'
		end 

	,DaysWaiting = 
		coalesce(DaysWaiting , 0)

	,Cases

	,TCIBeyondBreach = 
		case 
		when TCIDate is not null then
			case 
			when datediff(
					 d
					,
					case 
					when PathwayStatusCode in('ACS','OPT') then --breach date for closed pathwayscoalesce(
						--breach date for closed pathways
						 dateadd(
							 day
							,@DaysBefore18weekBreach 
							,dateadd(
								 day
								,-1 * coalesce(DaysWaiting , 0)
								,PathwayEndDate
							)
						)
						--breach date for open pathways
					else
						dateadd(
							 d
							,@DaysBefore18weekBreach  - coalesce(DaysWaiting , 0)
							,@CensusDate
						)
					end
					,Encounter.TCIDate
				) >= 0 then 1 
			else 0 
			end
		end

	,LastWeeksCases
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,ReportableFlag =	case			-- 20140618 RR added to update NonReportable
							when PathwayNonReportable.InternalNo is not null then 0
							else ReportableFlag 
						end
	,AdjustedFlag

	,RTTBreachDate = 

	case when 
		PathwayStatusCode in('ACS','OPT') then --breach date for closed pathways
			 dateadd(
				 day
				,@DaysBefore18weekBreach 
				,dateadd(
					 day
					,-1 * coalesce(DaysWaiting , 0)
					,PathwayEndDate
				)
			)

		else --breach date for open pathways
			dateadd(
				 d
				,@DaysBefore18weekBreach  - coalesce(DaysWaiting , 0)
				,@CensusDate
			)
		end


	,KeyDate
	,ReferringProviderCode
	,RTTFactTypeCode

	,Division =
		CASE Division
		WHEN 'Women & Children' THEN  'St Mary''s Hospital'
		ELSE Division
		END

	,Directorate =
		CASE Directorate
		WHEN 'Women & Children' THEN 'St Mary''s Hospital'
		ELSE Directorate
		END

	,TCIDate
	,DecisionToTreatFlag
	,WaitingListFlag
	,DiagnosticFlag
from
	(

	--------------------------
	----CENTRAL MANCHESTER----
	--------------------------

	--Admitted Pathways

	-- Admitted Complete Undjusted

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when Encounter.path_closed_days_DNA_adjs >= 365 then 52
					else
						case
						when (Encounter.path_closed_days_DNA_adjs - 1) < 0 then 0
						else (Encounter.path_closed_days_DNA_adjs - 1) / 7
						end
					end
					as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when Encounter.path_closed_days_DNA_adjs >= 365 then 52
					else
						case
						when (Encounter.path_closed_days_DNA_adjs) < 0 then 0
						else (Encounter.path_closed_days_DNA_adjs) / 7
						end
					end
					as varchar
				)
				,2
			)

		,PathwayStatusCode = 'ACS'
		,ReferralDate = cast(Encounter.ReferralDate as date)
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''
		,DaysWaiting = Encounter.path_closed_days_DNA_adjs
		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'N'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter

	WHERE 
		Encounter.type = 'CLOSED'
	AND Encounter.treattype = 'IP'
	AND Encounter.pathway_start_date_original >= '1 Jan 2007'
	AND Encounter.pathway_end_date between @localFromDate and @localCensusDate
	AND coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')


	union all

	-- Admitted Complete Adjusted

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0) - 1) / 7 > 3
						then
						case
						when (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0) - 1) >= 365 then 52
						else
							case
							when (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0) - 1) < 0 then 0
							else (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0) - 1) / 7
							end
						end
					else			
						case
						when (Encounter.path_closed_days_DNA_adjs - 1) < 0 then 0
						else (Encounter.path_closed_days_DNA_adjs - 1) / 7
						end

					end
					as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0)) / 7 > 3
						then
						case
						when (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0)) >= 365 then 52
						else
							case
							when (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0)) < 0 then 0
							else (Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0)) / 7
							end
						end
					else			
						case
						when (Encounter.path_closed_days_DNA_adjs) < 0 then 0
						else (Encounter.path_closed_days_DNA_adjs) / 7
						end

					end
					as varchar
				)
				,2
			)

		,PathwayStatusCode = 'ACS'
		,ReferralDate = cast(Encounter.ReferralDate as date)
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''

		,DaysWaiting =
			Encounter.path_closed_days_DNA_adjs - COALESCE (Encounter.adjdays, 0)

		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'Y'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter

	WHERE 
		Encounter.type = 'CLOSED'
	AND Encounter.treattype = 'IP'
	AND Encounter.pathway_start_date_original >= '1 Jan 2007'
	AND Encounter.pathway_end_date between @localFromDate and @localCensusDate
	AND coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')


	union all

	--Non-Admitted Complete Pathways

	-- Non-Admitted Complete Undjusted
	-- should just be only the DNAs that are adjusted.

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs - 1 < 0 then 0
						else (path_closed_days_DNA_adjs - 1) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs < 0 then 0
						else (path_closed_days_DNA_adjs) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode = 'OPT'
		,ReferralDate = cast(Encounter.ReferralDate as date)
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''
		,DaysWaiting = Encounter.path_closed_days_DNA_adjs
		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'N'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		Encounter.type='CLOSED'
	AND Encounter.pathway_start_date_original >= '1 Jan 2007'
	And Encounter.pathway_end_date between @localFromDate and @localCensusDate
	AND Encounter.treattype = 'NON IP'
	AND coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')



	union all

	-- Non-Admitted Complete Adjusted
	-- should just be only the DNAs that are adjusted.

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs - 1 < 0 then 0
						else (path_closed_days_DNA_adjs - 1) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs < 0 then 0
						else (path_closed_days_DNA_adjs) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode = 'OPT'
		,ReferralDate = cast(Encounter.ReferralDate as date)
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''
		,DaysWaiting = Encounter.path_closed_days_DNA_adjs
		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'Y'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		Encounter.type='CLOSED'
	AND Encounter.pathway_start_date_original >= '1 Jan 2007'
	And Encounter.pathway_end_date between @localFromDate and @localCensusDate
	AND Encounter.treattype = 'NON IP'
	AND coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')


	union all
	
	--Incomplete Pathways

	-- Incomplete Unadjusted CMFT

	SELECT
		  SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		-- Incomplete Unadjusted CMFT
		--,WeekBandCode =
		--	right(
		--		'0' +
		--		cast(
		--			case
		--			when coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) < 365 then
		--				case
		--				when coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) - 1 < 0 then 0
		--				else (coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) - 1) / 7
		--				end
		--			else 52
		--			end
		--		as varchar
		--		)
		--		,2
		--	)

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'OPEN' then
						case
						when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) >= 365 then 52
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
							else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) / 7
							end
						end
					when Encounter.type = 'CLOSED' then
						case
						when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) >= 365 then 52	
						else
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
							else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) / 7
							end
						end

					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'OPEN' then
						case
						when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) >= 365 then 52
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
							else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) / 7
							end
						end
					when Encounter.type = 'CLOSED' then
						case
						when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) >= 365 then 52	
						else
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
							else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) / 7
							end
						end

					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode =
			case 
			when Encounter.type = 'OPEN' then
				case
				when Decision_date_to_treat_in_IP is null then 'OPW'
				when cast(Decision_date_to_treat_in_IP as date) > @CensusDate then 'OPW'
				else 'IPW'
				end
			when Encounter.type = 'CLOSED' then
				case
				when cast(Decision_date_to_treat_in_IP as date) > @censusdate then 'OPW'
				when Decision_date_to_treat_in_IP is null then 'OPW'
				else 'IPW'
				end
			end

		,ReferralDate = cast(Encounter.ReferralDate as date)

		,WorkflowStatusCode =
			case 
			when Encounter.type = 'OPEN' then
				case
				when Decision_date_to_treat_in_IP is null then 'O'
				when cast(Decision_date_to_treat_in_IP as date) > @CensusDate then 'O'
				else 'I'
				end
			when Encounter.type = 'CLOSED' then
				case
				when cast(Decision_date_to_treat_in_IP as date) > @censusdate then 'O'
				when Decision_date_to_treat_in_IP is null then 'O'
				else 'I'
				end
			end

		,BreachBandCode = ''

	--	,DaysWaiting = coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs)
		,DaysWaiting = 
			case
			when Encounter.type = 'OPEN' then
				case
				when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
				else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore))
				end
			when Encounter.type = 'CLOSED' then
				case
				when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
				else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date))
				end
			end

		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'N'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')
	and	(
			(
				Encounter.type = 'OPEN'
			And Encounter.pathway_start_date_current between '1 Jan 2007' and @localCensusDate
			--remove open pathways without a wait time
			and Encounter.path_open_days_DNA_adjs is not null
			)
		or
			(
				Encounter.type = 'CLOSED'
			AND Encounter.pathway_start_date_original >= '1 Jan 2007'
			AND Encounter.pathway_end_date > @localCensusDate
			and	Encounter.pathway_start_date_current <= @localCensusDate
			)
		)


	union all

	-- Incomplete Adjusted CMFT

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		-- Incomplete Adjusted CMFT
		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'OPEN' then
						case
						--adjust only when adjusted wait is over three weeks
						when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) / 7 > 3
							then
							case
							when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) / 7
								end
							end
						--otherwise do not adjust
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) / 7
								end
							end
						end

					when Encounter.type = 'CLOSED' then
						--adjust only when adjusted wait is over three weeks
						case
						when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) / 7 > 3
							then
							case
							when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) >= 365 then 52
							else
								case
								when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
								else (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) / 7
								end
							end
						--otherwise do not adjust
						else		
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) >= 365 then 52	
							else
								case
								when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
								else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) / 7
								end
							end

						end
					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'OPEN' then
						case
						--adjust only when adjusted wait is over three weeks
						when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) / 7 > 3
							then
							case
							when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) / 7
								end
							end
						--otherwise do not adjust
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) / 7
								end
							end
						end

					when Encounter.type = 'CLOSED' then
						--adjust only when adjusted wait is over three weeks
						case
						when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) / 7 > 3
							then
							case
							when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) >= 365 then 52
							else
								case
								when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
								else (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) / 7
								end
							end
						--otherwise do not adjust
						else		
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) >= 365 then 52	
							else
								case
								when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
								else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) / 7
								end
							end

						end
					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode =
			case 
			when Encounter.type = 'OPEN' then
				case
				when Decision_date_to_treat_in_IP is null then 'OPW'
				when cast(Decision_date_to_treat_in_IP as date) > @CensusDate then 'OPW'
				else 'IPW'
				end
			when Encounter.type = 'CLOSED' then
				case
				when cast(Decision_date_to_treat_in_IP as date) > @CensusDate then 'OPW'
				when Decision_date_to_treat_in_IP is null then 'OPW'
				else 'IPW'
				end
			end

		,ReferralDate = cast(Encounter.ReferralDate as date)

		,WorkflowStatusCode =
			case 
			when Encounter.type = 'OPEN' then
				case
				when Decision_date_to_treat_in_IP is null then 'O'
				when cast(Decision_date_to_treat_in_IP as date) > @CensusDate then 'O'
				else 'I'
				end
			when Encounter.type = 'CLOSED' then
				case
				when cast(Decision_date_to_treat_in_IP as date) > @CensusDate then 'O'
				when Decision_date_to_treat_in_IP is null then 'O'
				else 'I'
				end
			end

		,BreachBandCode = ''

		,DaysWaiting =
			case
			when Encounter.type = 'OPEN' then
				case
				--adjust only when adjusted wait is over three weeks
				when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) / 7 > 3
					then				
					case
					when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) < 0 then 0
					else ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) 
					end
				--otherwise do not adjust
				else
					case
					when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
					else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore))
					end
				end

			when Encounter.type = 'CLOSED' then
				--adjust only when adjusted wait is over three weeks
				case
				when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) / 7 > 3
					then
					case
					when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
					else (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date))
					end
				--otherwise do not adjust
				else		
					case
					when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
					else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date))
					end
				end
			end

		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'Y'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')
	and	(
			(
				Encounter.type = 'OPEN'
			And Encounter.pathway_start_date_current between '1 Jan 2007' and @localCensusDate
			--remove open pathways without a wait time
			and Encounter.path_open_days_DNA_adjs is not null
			)
		or
			(
				Encounter.type = 'CLOSED'
			AND Encounter.pathway_start_date_original >= '1 Jan 2007'
			AND Encounter.pathway_end_date > @localCensusDate
			and	Encounter.pathway_start_date_current <= @localCensusDate
			)
		)



	--GUM Pathways
	/*
	union all

	--GUM Unadjusted Pathways

	SELECT
		 SourceUniqueID = [ClinicCode] + coalesce([PCTCode] , 'X98') + '||' + cast(@CensusDate as varchar)
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = null
		,SpecialtyCode = 'GUM'
		,NationalSpecialtyCode = '999'
		,PracticeCode = null
		,PCTCode = Encounter.[PCTCode]
		,SiteCode = 'MRI'
		,WeekBandReturnCode = '00'
		,WeekBandCode = '00'
		,PathwayStatusCode = 'OPT'
		,ReferralDate = null
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''
		,DaysWaiting = 0
		,Cases = Encounter.FirstAtt
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'N'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = 'Specialist Medical'
		,Directorate = 'Spec Medicine'
		,TCIDate = null

		,DecisionToTreatFlag = null
			--case when Decision_date_to_treat_in_IP <= @CensusDate then 1 else 0 end

		,WaitingListFlag = null
			--case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = null
			--case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = null
	FROM
		Warehouse.GUM.Summary Encounter
	WHERE
		CensusDate=@CensusDate		
				
	union all

	--GUM Adjusted Pathways

	SELECT
		 SourceUniqueID = [ClinicCode] + coalesce([PCTCode] , 'X98') + '||' + cast(@CensusDate as varchar)
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = null
		,SpecialtyCode = 'GUM'
		,NationalSpecialtyCode = '999'
		,PracticeCode = null
		,PCTCode = Encounter.[PCTCode]
		,SiteCode = 'MRI'
		,WeekBandReturnCode = '00'
		,WeekBandCode = '00'
		,PathwayStatusCode = 'OPT'
		,ReferralDate = null
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''
		,DaysWaiting = 0
		,Cases = Encounter.FirstAtt
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'Y'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = 'Specialist Medical'
		,Directorate = 'Spec Medicine'
		,TCIDate = null

		,DecisionToTreatFlag = null
			--case when Decision_date_to_treat_in_IP <= @CensusDate then 1 else 0 end

		,WaitingListFlag = null
			--case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = null
			--case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = null
	FROM
		Warehouse.GUM.Summary Encounter
	WHERE
		CensusDate=@CensusDate

		*/

	----------------
	----TRAFFORD----
	----------------

	union all

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo + '||' + CAST(EncounterRecno AS VARCHAR(12)) + '||' + AdjustedFlag
		,SourceCensusDate = CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = null
		,SpecialtyCode = Specialty
		,NationalSpecialtyCode = path_doh_spec
		,PracticeCode = null

		--,PCTCode = Encounter.pct_new
		,PCTCode =
			coalesce(
				 CCGPractice.ParentOrganisationCode
				,CCGPostCode.CCGCode
				,'00W'
			)

		,SiteCode = 'TRAF'

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) < 365 then
						case
						when coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) < 0 then 0
						else (coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) - 1) / 7
						end
					else 52
					end
					as varchar
				)
				, 2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) < 365 then
						case
						when coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs) < 0 then 0
						else (coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs)) / 7
						end
					else 52
					end
					as varchar
				)
				, 2
			)
			
		,PathwayStatusCode =
			case
			when Encounter.treattype = 'IP' then
				case
				when Encounter.type = 'OPEN' then 'IPW'
				when Encounter.type = 'CLOSED' then 'ACS'
				end
			when Encounter.treattype = 'NON IP' then
				case
				when Encounter.type = 'OPEN' then 'OPW'
				when Encounter.type = 'CLOSED' then 'OPT'
				end
			end

		,ReferralDate = null

		,WorkflowStatusCode =
			case
			when Encounter.treattype = 'IP' then
				case
				when Encounter.type = 'OPEN' then 'I'
				when Encounter.type = 'CLOSED' then 'C'
				end
			when Encounter.treattype = 'NON IP' then
				case
				when Encounter.type = 'OPEN' then 'O'
				when Encounter.type = 'CLOSED' then 'C'
				end
			end
			
		,BreachBandCode = ''

		,DaysWaiting = 
			coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs)

		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,Encounter.AdjustedFlag
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = null
			--case when Decision_date_to_treat_in_IP <= @CensusDate then 1 else 0 end

		,WaitingListFlag = null
			--case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = null
			--case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		WarehouseSQL.TraffordWarehouse.dbo.TraffordRTT Encounter 

	left join Organisation.dbo.CCGPractice
	on	CCGPractice.OrganisationCode = Encounter.RegisteredGpPracticeCode

	left join Organisation.dbo.CCGPostcode
	on	CCGPostcode.Postcode =
			case
			when datalength(rtrim(ltrim(Encounter.PatientPostcode))) = 6 then left(Encounter.PatientPostcode, 2) + '   ' + right(Encounter.PatientPostcode, 3)
			when datalength(rtrim(ltrim(Encounter.PatientPostcode))) = 7 then left(Encounter.PatientPostcode, 3) + '  ' + right(Encounter.PatientPostcode, 3)
			else Encounter.PatientPostcode
			end
	WHERE 
		Encounter.path_doh_spec not in ('960' , '650' , '950')
	and	Encounter.CensusDate =  @TraffordCensusDate


	union all

	----CMFT DIRECT ACCESS AUDIOLOGY

	
	-- Non-Admitted Complete Direct Access Undjusted
	-- should just be only the DNAs that are adjusted.

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs - 1 < 0 then 0
						else (path_closed_days_DNA_adjs - 1) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs < 0 then 0
						else (path_closed_days_DNA_adjs) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode = 'OPT'
		,ReferralDate = cast(Encounter.ReferralDate as date)
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''
		,DaysWaiting = Encounter.path_closed_days_DNA_adjs
		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'N'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		Encounter.type='DAA_CLOSED'
	AND Encounter.pathway_start_date_original >= '1 Jan 2007'
	And Encounter.pathway_end_date between @localFromDate and @localCensusDate
	AND Encounter.treattype = 'NON IP'
	AND coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')



	union all

	-- Non-Admitted Complete Direct Access Adjusted
	-- should just be only the DNAs that are adjusted.

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs - 1 < 0 then 0
						else (path_closed_days_DNA_adjs - 1) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when path_closed_days_DNA_adjs < 365 then
						case
						when path_closed_days_DNA_adjs < 0 then 0
						else (path_closed_days_DNA_adjs) / 7
						end
					else 52
					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode = 'OPT'
		,ReferralDate = cast(Encounter.ReferralDate as date)
		,WorkflowStatusCode = 'C'
		,BreachBandCode = ''
		,DaysWaiting = Encounter.path_closed_days_DNA_adjs
		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'Y'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		Encounter.type='DAA_CLOSED'
	AND Encounter.pathway_start_date_original >= '1 Jan 2007'
	And Encounter.pathway_end_date between @localFromDate and @localCensusDate
	AND Encounter.treattype = 'NON IP'
	AND coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')





	UNION ALL
	--Incomplete Unadjusted CMFT Direct Access Audiology

	SELECT
		  SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'DAA_OPEN' then
						case
						when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) >= 365 then 52
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
							else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) / 7
							end
						end
					when Encounter.type = 'DAA_CLOSED' then
						case
						when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) >= 365 then 52	
						else
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
							else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) / 7
							end
						end

					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'DAA_OPEN' then
						case
						when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) >= 365 then 52
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
							else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) / 7
							end
						end
					when Encounter.type = 'DAA_CLOSED' then
						case
						when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) >= 365 then 52	
						else
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
							else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) / 7
							end
						end

					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode =
			case
			when Encounter.treattype = 'NON IP' then 'OPW'
			when Encounter.treattype = 'IP' then 'IPW'
			else 'OPW'
			end

		,ReferralDate = cast(Encounter.ReferralDate as date)

		,WorkflowStatusCode =
			case
			when Encounter.treattype = 'NON IP' then 'O'
			when Encounter.treattype = 'IP' then 'I'
			else 'O'
			end

		,BreachBandCode = ''

	--	,DaysWaiting = coalesce(path_open_days_DNA_adjs , path_closed_days_DNA_adjs)
		,DaysWaiting = 
			case
			when Encounter.type = 'DAA_OPEN' then
				case
				when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
				else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore))
				end
			when Encounter.type = 'DAA_CLOSED' then
				case
				when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
				else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date))
				end
			end

		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'N'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')
	and	(
			(
				Encounter.type = 'DAA_OPEN'
			And Encounter.pathway_start_date_current between '1 Jan 2007' and @localCensusDate
			--remove open pathways without a wait time
			and Encounter.path_open_days_DNA_adjs is not null
			)
		or
			(
				Encounter.type = 'DAA_CLOSED'
			AND Encounter.pathway_start_date_original >= '1 Jan 2007'
			AND Encounter.pathway_end_date > @localCensusDate
			and	Encounter.pathway_start_date_current <= @localCensusDate
			)
		)


	union all

	-- Incomplete Adjusted CMFT Direct Access Audiology

	SELECT
		 SourceUniqueID = InternalNo + '||' + EpisodeNo
		,SourceCensusDate = @CensusDate
		,EncounterTypeCode = null
		,ConsultantCode = Encounter.path_cons
		,SpecialtyCode = Encounter.path_spec
		,NationalSpecialtyCode = Encounter.path_doh_spec
		,PracticeCode = null
		,PCTCode = Encounter.pct_new
		,SiteCode = Encounter.Hospital

		-- Incomplete Adjusted CMFT
		,WeekBandReturnCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'DAA_OPEN' then
						case
						--adjust only when adjusted wait is over three weeks
						when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) / 7 > 3
							then
							case
							when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore) - 1) / 7
								end
							end
						--otherwise do not adjust
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore) - 1) / 7
								end
							end
						end

					when Encounter.type = 'DAA_CLOSED' then
						--adjust only when adjusted wait is over three weeks
						case
						when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) / 7 > 3
							then
							case
							when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) >= 365 then 52
							else
								case
								when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
								else (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date) - 1) / 7
								end
							end
						--otherwise do not adjust
						else		
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) >= 365 then 52	
							else
								case
								when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
								else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) / 7
								end
							end

						end
					end
				as varchar
				)
				,2
			)

		,WeekBandCode =
			right(
				'0' +
				cast(
					case
					when Encounter.type = 'DAA_OPEN' then
						case
						--adjust only when adjusted wait is over three weeks
						when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) / 7 > 3
							then
							case
							when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) / 7
								end
							end
						--otherwise do not adjust
						else
							case
							when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) >= 365 then 52
							else
								case
								when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
								else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) / 7
								end
							end
						end

					when Encounter.type = 'DAA_CLOSED' then
						--adjust only when adjusted wait is over three weeks
						case
						when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) / 7 > 3
							then
							case
							when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) >= 365 then 52
							else
								case
								when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
								else (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) / 7
								end
							end
						--otherwise do not adjust
						else		
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) >= 365 then 52	
							else
								case
								when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
								else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) / 7
								end
							end

						end
					end
				as varchar
				)
				,2
			)

		,PathwayStatusCode =
			case
			when Encounter.treattype = 'NON IP' then 'OPW'
			when Encounter.treattype = 'IP' then 'IPW'
			when Encounter.treattype is null then 'OPW'
			end

		,ReferralDate = cast(Encounter.ReferralDate as date)

		,WorkflowStatusCode =
			case
			when Encounter.treattype = 'NON IP' then 'O'
			when Encounter.treattype = 'IP' then 'I'
			when Encounter.treattype is null then 'O'
			end

		,BreachBandCode = ''

		,DaysWaiting =
			case
			when Encounter.type = 'DAA_OPEN' then
				case
				--adjust only when adjusted wait is over three weeks
				when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) / 7 > 3
					then				
					case
					when ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) < 0 then 0
					else ((path_open_days_DNA_adjs-1) - COALESCE (adjdays, 0) - datediff(day , @CensusDate , datebefore)) 
					end
				--otherwise do not adjust
				else
					case
					when ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore)) < 0 then 0
					else ((path_open_days_DNA_adjs-1) - datediff(day , @CensusDate , datebefore))
					end
				end

			when Encounter.type = 'DAA_CLOSED' then
				--adjust only when adjusted wait is over three weeks
				case
				when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) / 7 > 3
					then
					case
					when (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
					else (path_closed_days_DNA_adjs - COALESCE (adjdays, 0) - datediff(day , @CensusDate , pathway_end_date))
					end
				--otherwise do not adjust
				else		
					case
					when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date)) < 0 then 0
					else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date))
					end
				end
			end

		,Cases = 1
		,TCIBeyondBreach = null
		,LastWeeksCases = null
		,PrimaryDiagnosisCode = null
		,PrimaryOperationCode = null
		,RTTCurrentStatusCode = null
		,RTTPeriodStatusCode = null
		,ReportableFlag = 1
		,AdjustedFlag = 'Y'
		,RTTBreachDate = null
		,KeyDate = null
		,ReferringProviderCode = null
		,RTTFactTypeCode = null
		,Division = Encounter.path_division
		,Directorate = Encounter.path_directorate
		,TCIDate = tci_date

		,DecisionToTreatFlag = 
			case when cast(Decision_date_to_treat_in_IP as date) <= @CensusDate then 1 else 0 end

		,WaitingListFlag = 
			case when WL_WLDate <= @CensusDate then 1 else 0 end

		,DiagnosticFlag = 
			case when WL_status in('ADMDIAG','WLDIAG') then 1 else 0 end

		,PathwayEndDate = Encounter.pathway_end_date
	from
		ETL.WkPathwayReturn00 Encounter
	WHERE 
		coalesce(Encounter.exc , '') = ''
	and	coalesce(Encounter.diag , '') in ('' , 'N' , 'I')
	AND Encounter.path_doh_spec not in ('960' , '650' , '950')
	and	(
			(
				Encounter.type = 'DAA_OPEN'
			And Encounter.pathway_start_date_current between '1 Jan 2007' and @localCensusDate
			--remove open pathways without a wait time
			and Encounter.path_open_days_DNA_adjs is not null
			)
		or
			(
				Encounter.type = 'DAA_CLOSED'
			AND Encounter.pathway_start_date_original >= '1 Jan 2007'
			AND Encounter.pathway_end_date > @localCensusDate
			and	Encounter.pathway_start_date_current <= @localCensusDate
			)
		)
	) Encounter

left join RTT.PCT
on	PCT.PCTCode = left(Encounter.PCTCode , 3)

left join RTT.PathwayNonReportable -- 20140618 RR added to update NonReportable
on SourceUniqueID = (PathwayNonReportable.InternalNo + '||' + PathwayNonReportable.EpisodeNo)
and (@CensusDate between PathwayNonReportable.FromDate and PathwayNonReportable.ToDate 
	or 
	(PathwayNonReportable.FromDate<= @CensusDate and PathwayNonReportable.ToDate is null)
	)

SELECT @InsertedRows = @@ROWCOUNT

-- Audit Log - Run Times
SELECT @Elapsed = DATEDIFF(MINUTE, @StartTime, GETDATE())

SELECT @Stats = 'Time Elapsed ' 
				+ CONVERT(char(3), @Elapsed) 
				+ ' Mins from ' 
				+ CAST(@StartTime AS NVARCHAR(20))
				+ ' Census Date ' 
				+ CAST(@CensusDate AS NVARCHAR(20))
				+ ' Rows Inserted '
				+ CAST(@InsertedRows AS NVARCHAR(10))

exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

﻿
CREATE Procedure [RTT].[AppendResetToManualAdjustment] 

as

Insert into ManualAdjustment.RTT.Adjust
      (
      [Internal No]
      ,[Episode No]
      ,StartDate 
      ,[Update Date]
      ,Comment 
      )

select distinct
      InternalNo 
      ,EpisodeNo
      ,StartDate = 
            case 
                  when CancelBy = 'P' then CancelDate
                  else ApptDate
            end
      ,UpdateDate = getdate()
      ,Comment = 'NCPR'

from 
      Infocom_PAS.dbo.smsmir_outpatient_appointment ResetDate
where
      CancelBy in ('P','D')
and 
      (
            CancelReason = 'NCPR'
      or
            Disposal= 'NCPR'
      )
and CancelDate <= 
                  (
                  Select
                        DateValue
                  from 
                        RTTLegacy.Utility.Parameter
                  where
                        Parameter = 'RTTNEXTMONTHENDDATE'
                  )
and not exists
      (
      select
            1
      from
            Infocom_PAS.dbo.smsmir_outpatient_appointment Latest
      where
            Latest.InternalNo = ResetDate.InternalNo
      and Latest.EpisodeNo = ResetDate.EpisodeNo
      and Latest.CancelBy in ('P','D')
      and 
            (
                  Latest.CancelReason = 'NCPR'
            or
                  Latest.Disposal = 'NCPR'
            )
      and CancelDate <= 
                  (
                  Select
                        DateValue
                  from 
                        RTTLegacy.Utility.Parameter
                  where
                        Parameter = 'RTTNEXTMONTHENDDATE'
                  )
      and Latest.CancelDate > ResetDate.CancelDate
      ) 
and not exists
      (
      Select
            1
      from
            ManualAdjustment.RTT.Adjust Present
      where
            Present.[Internal No] = ResetDate.InternalNo
      and Present.[Episode No] = ResetDate.EpisodeNo
      )
   



Update adjust
set StartDate = ResetDate.StartDate 
	,[Update Date] = ResetDate.UpdateDate
	,Comment = ResetDate.Comment
from 
	ManualAdjustment.RTT.Adjust 
inner join 

	(
	Select
		ResetDate.InternalNo 
		,ResetDate.EpisodeNo
		,StartDate = 
			case 
			  when ResetDate.CancelBy = 'P' then ResetDate.CancelDate
			  else ResetDate.ApptDate
			end
		,UpdateDate = getdate()
		,Comment = 'NCPR'
	from
		Infocom_PAS.dbo.smsmir_outpatient_appointment ResetDate
	where
		CancelBy in ('P','D')
	and 
		(
			CancelReason = 'NCPR'
		or
			Disposal= 'NCPR'
		)
	and CancelDate <= 
		  (
		  Select
				DateValue
		  from 
				RTTLegacy.Utility.Parameter
		  where
				Parameter = 'RTTNEXTMONTHENDDATE'
		  )
	and not exists
		(
		select
			1
		from
			Infocom_PAS.dbo.smsmir_outpatient_appointment Latest
		where
			Latest.InternalNo = ResetDate.InternalNo
			and Latest.EpisodeNo = ResetDate.EpisodeNo
			and Latest.CancelBy in ('P','D')
		and 
			(
			  Latest.CancelReason = 'NCPR'
			or
			  Latest.Disposal = 'NCPR'
			)
		and CancelDate <= 
		  (
		  Select
				DateValue
		  from 
				RTTLegacy.Utility.Parameter
		  where
				Parameter = 'RTTNEXTMONTHENDDATE'
		  )
		and Latest.CancelDate > ResetDate.CancelDate
		)
	)
	ResetDate 
on Adjust.[Internal No] = ResetDate.InternalNo
and adjust.[Episode No] = ResetDate.EpisodeNo
and (
		cast(adjust.StartDate as date) < cast(ResetDate.StartDate as date)
	or
		Adjust.StartDate is null
	)

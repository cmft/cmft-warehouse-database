﻿
CREATE procedure [RTT].[usp_ArchiveTraffordRTT]  
as

/******************************************************************************
**  Name: usp_ArchiveTraffordRTT
**  Purpose: Copies the Warehousesql RTTTrafford table contents to an archive table
**
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 17.10.12   MH          Brought into source control
** 18.10.12   MH          Removed and then restored reference to WarehouseSQL
******************************************************************************/

	DELETE FROM
		RTT.TraffordArchive
	WHERE
		Censusdate IN
		(
			SELECT	DISTINCT
				Censusdate
			FROM
				[$(TraffordWarehouse)].dbo.TraffordRTT
		)

	INSERT INTO RTT.TraffordArchive
		SELECT * FROM [$(TraffordWarehouse)].dbo.TraffordRTT


﻿
CREATE procedure [RTT].[BuildRTTEncounter]
(
	@CensusDate DATE
)
as

/******************************************************************************
**  Name: RTT.BuildRTTEncounter
**  Purpose: Populating the RTT.Encounter drill through table from RTT.Fact
**
**	Input Parameters :
**		CensusDate
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 20.08.12      MH       Created
******************************************************************************/

	DECLARE @RunDate DATE = GETDATE()
	DECLARE @StartTime		DATETIME = GETDATE()
	DECLARE @Elapsed		INT
	DECLARE @Stats			VARCHAR(MAX)
	DECLARE @InsertedRows	INT
	DECLARE @Process		VARCHAR(30) = 'BuildRTTEncounter'

	IF (@CensusDate > GETDATE())
		RETURN 1

	-- Remove all rows for this census date
	DELETE FROM
		[RTT].[Encounter]
	WHERE
		CensusDate = @CensusDate

	-- Insert rows
	INSERT INTO [RTT].[Encounter]
	(
	   Hospital
      ,InternalNo
      ,ReferralDate
      ,EpisodeNo
      ,RefSource
      ,Consultant
      ,Specialty
      ,doh_spec
      ,DistrictNo
      ,pathway_start_date_original
      ,pathway_start_date_current
      ,pathway_treat_by_date
      ,pathway_end_date
      ,pathway_ID
      ,WL_WLDate
      ,WL_IntMan
      ,WL_cons
      ,WL_Spec
      ,WL_doh_spec
      ,WL_PrimProc
      ,tci_date
      ,DecidedToAdmitDate
      ,path_division
      ,path_directorate
      ,path_cons
      ,path_spec
      ,path_doh_spec
      ,path_doh_desc
      ,path_closed_wks_DNA_adjs
      ,path_closed_days_DNA_adjs
      ,path_open_wks_DNA_adjs
      ,path_open_days_DNA_adjs
      ,Surname
      ,Forename
      ,[type]
      ,pct_new
      ,treattype
      ,Rundate
      ,Censusdate
      ,PCTCode
      ,AdjustedFlag
      ,FactEncounterRecNo
      ,SourceUniqueID
	  ,Casenote	
	  ,WL_WaitingListCode
	  ,RTTBreachDate
	  ,TCIBeyond
	  ,PauseStartDate
	  ,WeekBandReturnCode
	  ,DecisionToTreatFlag
	  ,WaitingListFlag
	  ,DiagnosticFlag
	  ,Comment1
	  ,Comment2
	  ,WeekBandCode
      ,PathwayStatusCode
      ,WorkflowStatusCode
      ,BreachBandCode
      ,DaysWaiting
	  ,PathwayStatus
	  ,LastAttendanceStatusCode
	  ,RTTPathwayID
	  ,Cases
	  ,DateOfBirth
	,ContractID
	,CurrentContractID
	,fut_appt_date
	,ReportableFlag
	)

	SELECT

	   Hospital
      ,InternalNo
      ,ReferralDate
      ,EpisodeNo
      ,RefSource
      ,Consultant
      ,Specialty
      ,doh_spec
      ,DistrictNo
      ,pathway_start_date_original
      ,pathway_start_date_current
      ,pathway_treat_by_date
      ,pathway_end_date
      ,pathway_ID
      ,WL_WLDate
      ,WL_IntMan
      ,WL_cons
      ,WL_Spec
      ,WL_doh_spec
      ,WL_PrimProc
      ,tci_date
      ,DecidedToAdmitDate
      ,path_division
      ,path_directorate
      ,path_cons
      ,path_spec
      ,path_doh_spec
      ,path_doh_desc
      ,path_closed_wks_DNA_adjs
      ,path_closed_days_DNA_adjs
      ,path_open_wks_DNA_adjs
      ,path_open_days_DNA_adjs
      ,Surname
      ,Forename
      ,[type]
      ,pct_new
      ,treattype
      ,@RunDate
      ,Censusdate
      ,PCTCode
      ,AdjustedFlag
      ,FactEncounterRecno
      ,SourceUniqueID
	  ,Casenote	
	  ,WL_WaitingListCode
	  ,RTTBreachDate
	  ,TCIBeyond
	  ,PauseStartDate
	  ,WeekBandReturnCode
	  ,DecisionToTreatFlag
	  ,WaitingListFlag
	  ,DiagnosticFlag
	  ,Comment1
	  ,Comment2
	  ,WeekBandCode
      ,PathwayStatusCode
      ,WorkflowStatusCode
      ,BreachBandCode
      ,DaysWaiting
	  ,PathwayStatus
	  ,LastAttendanceStatusCode
	  ,RTTPathwayID
	  ,Cases
	  ,DateOfBirth
	,ContractID
	,CurrentContractID
	,fut_appt_date
	,ReportableFlag
	  
	FROM
		[RTT].[TImportRTTEncounter]

	WHERE
		Censusdate = @CensusDate

	SELECT @InsertedRows = @@ROWCOUNT

-- Audit Log - Run Times
	SELECT @Elapsed = DATEDIFF(MINUTE, @StartTime, GETDATE())

	SELECT @Stats = 'Time Elapsed ' 
					+ CONVERT(char(3), @Elapsed) 
					+ ' Mins from ' 
					+ CAST(@StartTime AS NVARCHAR(20))
					+ ' Census Date ' 
					+ CAST(@CensusDate AS NVARCHAR(20))
					+ ' Rows Inserted '
					+ CAST(@InsertedRows AS NVARCHAR(10))

	exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

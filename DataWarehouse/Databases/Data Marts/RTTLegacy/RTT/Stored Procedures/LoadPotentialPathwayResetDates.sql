﻿

		  
CREATE proc [RTT].[LoadPotentialPathwayResetDates]
as

truncate table   RTT.PotentialPathwayResetDates
		  
		  		  
Declare @monthendcensusdate as date =(SELECT 
								DateValue
								 FROM [RTTLegacy].[Utility].[Parameter]
								  where Parameter = 'RTTNEXTMONTHENDDATE'
                                    )
            Declare @maxcensusdate  as date =    (select
                                        max(censusdate)
                                        from [RTTLegacy].[RTT].[encounter]
                                          
                                        where censusdate > dateadd(m,-1,@monthendcensusdate )
                                        )                       
     
     Declare @censusdate as date = Case when  @maxcensusdate > @monthendcensusdate then @monthendcensusdate else @maxcensusdate end
     
      --print @censusdate
      --print @monthendcensusdate
      --print @maxcensusdate                                  
                                        
insert into  RTT.PotentialPathwayResetDates  
                           
Select
candata.*
,specCode = [NationalSpecialtyCode] + ' ' + [path_doh_desc]
,breachNONIP = case when dayswaiting > 126 and PathwayStatusCode like 'OP%' then 1 else 0 end
,NonBreachNONIP = case when  dayswaiting < 127 and PathwayStatusCode like 'OP%' then 1 else 0 end
,breachIP = case when dayswaiting > 126 and PathwayStatusCode not like 'OP%' then 1 else 0 end
,NonBreachIP = case when  dayswaiting < 127 and PathwayStatusCode not like 'OP%' then 1 else 0 end 
,Totalbreach = case when dayswaiting > 126 then 1 else 0 end
,TotalNonbreach = case when dayswaiting < 127 then 1 else 0 end
,Total = 1
,actualresetdate = case when maxDNACNDDate > PotentailResetDate  then  maxDNACNDDate  else PotentailResetDate  end
,adj.StartDate
,adj.comment
,[Internal no] = SourcePatientNo
,[Episode No] = SourceEncounterNo
--into #candate  -- drop table #candate  --- select * from #candate where actualresetdate  != StartDate or StartDate is null


from

(                                   
                                    
                                    
                                    
      Select      
            [CensusDate]
            ,[SourceUniqueID]
            ,[ConsultantCode]
            ,[SpecialtyCode]
            ,[path_doh_desc]
                  
            ,Surname
                        ,Forename
                        ,[type]
                        ,pathway_start_date_current
                        ,pathway_end_date
                        ,Casenote
            ,[NationalSpecialtyCode]
      
                                          
            ,Division
            ,[PathwayStatusCode]
            ,[ReferralDate]
            ,[DaysWaiting]
            ,SourcePatientNo
            ,SourceEncounterNo
            ,TotalHospitalCanelaltions = sum(case when CancelledByCode  = 'H' then 1 else 0 end)
            ,TotalPatientCancelaltions = sum(case when CancelledByCode  = 'P' then 1 else 0 end)
            ,TotalDNAsCNDs = sum(case when DNAcode in ('2','3') and CancelledByCode is null  then 1 else 0 end)
            ,maxDNACNDDate = max(case when DNAcode in ('2','3') and CancelledByCode is null then appointmentdate else null end)
            ,lastUserResetdate = max (case	when ScheduledCancelReasonCode  = 'NCPR' or DisposalCode = 'NCPR' then PotentialCanceldate else null end) 
            ,maxPatCanDate= max(case when CancelledByCode = 'P' then AppointmentCancelDate else null end)
            ,PotentailResetDate = max(case when DNAcode in ('2','3') then PotentialCanceldate else null end)
            ,Totalpatientcansdns = sum(case when DNAcode in ('2','3') then 1 else 0 end)

                                          
                                          
                  From                    

                  (
                  
                  
                  
                  
                  SELECT  
                        rtt.[CensusDate]
                        ,rtt.[SourceUniqueID]
                        ,rtt.[ConsultantCode]
                        ,rtt.[SpecialtyCode]
                        ,[path_doh_desc]
            
                        ,rttdet.Surname
                        ,rttdet.Forename
                        ,rttdet.[type]
                        ,pathway_start_date_current
                        ,pathway_end_date
                        ,Casenote
                        ,[NationalSpecialtyCode]
                        

                        ,Division
                        ,rtt.[PathwayStatusCode]
                        ,rtt.[ReferralDate]
                        ,rtt.[DaysWaiting]
                        ,CancelledByCode 
                        ,AttendanceOutcomeCode
                        ,AppointmentStatusCode
                        ,Appointmentdate
                        ,AppointmentCancelDate
                        ,DNACode
                        ,ScheduledCancelReasonCode
                        ,PotentialCanceldate = coalesce([AppointmentCancelDate],[Appointmentdate])
                        ,op.SourcePatientNo
                        ,op.SourceEncounterNo
                        ,op.DisposalCode

                        
                    FROM [RTTLegacy].[RTT].[Fact] RTT
                    
                    join  [RTTLegacy].RTT.Encounter RTTdet
                    on RTTdet.[FactEncounterRecNo] = RTT.EncounterRecno
                    and RTTdet.censusdate = @censusdate
                    
                    join [Warehouse].[OP].[Encounter] OP
                    on rtt.[SourceUniqueID] = op.[SourcePatientNo] + '||' + [SourceEncounterNo] 
                        and coalesce([AppointmentCancelDate],[Appointmentdate]) <= @censusdate 
                        and DNACode in (2,3,4) 
                        and AppointmentTypeCode not like '%Z%'
                              and (ScheduledCancelReasonCode not in ('NCL3') or ScheduledCancelReasonCode is null)
                          --    and op.SourcePatientNo = '3524673' and op.SourceEncounterNo = '1'

                        


                    
                    where
                    
                    rtt.censusdate =  @censusdate
                        and case when rtt.PathwayStatusCode = 'ACS' and rtt.adjustedflag = 'Y' then 1
                                    when rtt.PathwayStatusCode != 'ACS' and rtt.adjustedflag = 'N' then 1
                                    end = 1
                                    
                        and rtt.reportableFlag = 1    
                        and rtt.referraldate is not null
                        and rtt.Division != 'Trafford'      
                        
                        )data
                        
                  group by
                        [CensusDate]
                        ,[SourceUniqueID]
                        ,[ConsultantCode]
                        ,[SpecialtyCode]
                        ,[path_doh_desc]
                  
                        ,Surname
                        ,Forename
                        ,[type]
                        ,pathway_start_date_current
                        ,pathway_end_date
                        ,Casenote
                        ,[NationalSpecialtyCode]
                  
                                          
                        ,Division
                        ,[PathwayStatusCode]
                        ,[ReferralDate]
                        ,[DaysWaiting]
                        ,SourcePatientNo
            ,SourceEncounterNo

                  
                  
                  
) candata   

left join ManualAdjustment.rtt.adjust adj
on adj.[Internal no] + '||' + adj.[Episode No] =  candata.[SourceUniqueID]

where             
                        
(Totalpatientcansdns > 1

or ( [NationalSpecialtyCode] = '311' and Totalpatientcansdns > 0))

and (StartDate  < case when maxDNACNDDate > PotentailResetDate  then  maxDNACNDDate  else PotentailResetDate  end or StartDate is null )



﻿
CREATE procedure [RTT].[AssignManualAdjustment]
	@MonthEnd smalldatetime
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Updated int
	,@Stats varchar(max)
	,@DaysToBreach int = 126


Update Pathway
Set
	pathway_start_date_current	=	Adjust.StartDate
	,pathway_treat_by_date		=	case
										when  pathway_treat_by_date is null then null
										else dateadd(day,@DaysToBreach,Adjust.StartDate)
									end
	,path_closed_days_DNA_adjs	=	case
										when path_closed_days_DNA_adjs is null then null
										else datediff(day,Adjust.StartDate,pathway_end_date)
									end
	,path_closed_wks_DNA_adjs	=	case
										when path_closed_days_DNA_adjs is null then null
										else (datediff(day,Adjust.StartDate,pathway_end_date)/7)
									end
	,path_open_days_DNA_adjs	=	case
										when path_open_days_DNA_adjs is null then null
										else datediff(day,Adjust.StartDate,dateadd(day,1,datebefore))
									end
	,path_open_wks_DNA_adjs		=	case
										when path_open_days_DNA_adjs is null then null
										else (datediff(day,Adjust.StartDate,dateadd(day,1,datebefore))/7)
									end
from 
	ETL.WkPathwayReturn00 Pathway

inner join  [$(ManualAdjustment)].rtt.adjust Adjust
on	Adjust.[Internal No] = cast(Pathway.InternalNo as nvarchar)
and Adjust.[Episode No] = cast(Pathway.EpisodeNo as nvarchar)
and Adjust.StartDate is not null
and (
	Adjust.StartDate between pathway_start_date_current and pathway_end_date
	or
	(Adjust.StartDate >= pathway_start_date_current and pathway_end_date is null)
	)


select
	@Updated = coalesce(@@ROWCOUNT , 0)
	
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Updated: ' + cast(@Updated as varchar) + ', ' +
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

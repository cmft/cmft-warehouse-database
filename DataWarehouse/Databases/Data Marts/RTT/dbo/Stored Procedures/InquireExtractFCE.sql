﻿CREATE                     procedure [dbo].[InquireExtractFCE] @fromDate smalldatetime = null, @toDate smalldatetime = null, @debug bit = 0 as
/*
Load INQUIRE extract directly into the TGpReferralOPWL staging table.
PDO Mar 2006
*/
set dateformat dmy
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
select @StartTime = getdate()
select @RowsInserted = 0
select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'
select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, getdate())), 0), 112) + '2400'
select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)
select @sql = '
insert into TImportIpEncounter
(
	SourceKey, InterfaceCode, DistrictNo, SourcePatientNo, SourceEncounterNo2, SourceSpellNo, SourceEncounterNo, PatientTitle, PatientForename, PatientSurname, DateOfBirth, DateOfDeath, SexCode, NHSNumber, PostCode, Address1, Address2, Address3, DhaCode, EthnicOriginCode, MaritalStatusCode, ReligionCode, DateOnWaitingList, AdmissionDate, DischargeDate, EpisodeEndDate, EpisodeEndTime, EpisodeStartDate, EpisodeStartTime, RegisteredGpCode, HospitalCode, AdmissionMethodCode, AdmissionSourceCode, ManagementIntentionCode, DischargeMethodCode, DischargeDestinationCode, ConsultantCode, SpecialtyCode, PrimaryDiagnosisCode, SubsidiaryDiagnosisCode, PrimaryOperationCode, PrimaryOperationDate, TransferFrom, RegisteredGdpCode, PatientClassificationCode, EpisodicGpCode, CasenoteNumber, NHSNumberStatusCode, SourceAdminCategoryCode, FirstRegDayOrNightAdmit, AdmissionTime, DischargeTime, NationalSpecialtyCode, NeonatalLevelOfCare, SourceHRGCode, LastEpisodeInSpellIndicator, SourceProviderCode, SourcePurchaserCode, SourceContractSerialNo, StartWardType, EndWardType
)
select
	SourceKey,
	''INQ'' InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo2, 
	SourcePatientNo + ''/'' + SourceEncounterNo2 SourceSpellNo, 
	SourceEncounterNo, 
	PatientTitle, PatientForename, PatientSurname, DateOfBirth, DateOfDeath, SexCode, NHSNumber, PostCode, Address1, Address2, Address3, DhaCode, EthnicOriginCode, MaritalStatusCode, ReligionCode, 
	DateOnWaitingList, 
	AdmissionDate, 
	case when DischargeDate = ''//'' then null else DischargeDate end DischargeDate, 
	EpisodeEndDate, 
	case
	when EpisodeEndTime = '':'' then null
	else EpisodeEndDate + '' '' + EpisodeEndTime
	end EpisodeEndTime, 
	EpisodeStartDate, 
	case
	when EpisodeStartTime = '':'' then null
	else EpisodeStartDate + '' '' + EpisodeStartTime
	end EpisodeStartTime, 
	
	RegisteredGpCode, HospitalCode, AdmissionMethodCode, AdmissionSourceCode, ManagementIntentionCode, DischargeMethodCode, DischargeDestinationCode, ConsultantCode, SpecialtyCode, PrimaryDiagnosisCode, SubsidiaryDiagnosisCode, PrimaryOperationCode, PrimaryOperationDate, TransferFrom, RegisteredGdpCode, 
	rtrim(PatientClassificationCode) PatientClassificationCode,
	EpisodicGpCode, CasenoteNumber, NHSNumberStatusCode, SourceAdminCategoryCode, 
	left(FirstRegDayOrNightAdmit, 1) FirstRegDayOrNightAdmit, 
	case
	when AdmissionTime = '':'' then null
	else AdmissionDate + '' '' + AdmissionTime
	end AdmissionTime, 
	case
	when DischargeTime = '':'' then null
	else DischargeDate + '' '' + DischargeTime
	end DischargeTime, 
	NationalSpecialtyCode, NeonatalLevelOfCare, SourceHRGCode, LastEpisodeInSpellIndicator, SourceProviderCode, SourcePurchaserCode, SourceContractSerialNo, StartWardType, EndWardType
from
	openquery(INQUIRE, ''
SELECT 
	FCE.FCEEXTID SourceKey,
	Patient.DistrictNumber DistrictNo,
	FCE.InternalPatientNumber SourcePatientNo,
	FCE.EpisodeNumber SourceEncounterNo2,
	FCE.FceSequenceNo SourceEncounterNo,
	Patient.Title PatientTitle,
	Patient.Forenames PatientForename,
	Patient.Surname PatientSurname,
	Patient.PtDoB DateOfBirth,
	Patient.PtDateOfDeath DateOfDeath,
	Patient.Sex SexCode,
	Patient.NHSNumber,
	Patient.PtAddrPostCode PostCode,
	Patient.PtAddrLine1 Address1,
	Patient.PtAddrLine2 Address2,
	Patient.PtAddrLine3 Address3,
	Patient.DistrictOfResidenceCode DhaCode,
	Patient.EthnicType EthnicOriginCode,
	Patient.MaritalStatus MaritalStatusCode,
	Patient.Religion ReligionCode,
	AdmitDisch.WlDateCcyy DateOnWaitingList,
	AdmitDisch.AdmissionDate AdmissionDate,
	AdmitDisch.DischargeDate DischargeDate,
	FCE.FCEEndDate EpisodeEndDate,
	case when FCE.FCEEndTime = "24:00" then "23:59" else FCE.FCEEndTime end EpisodeEndTime,
	FCE.FCEStartDate EpisodeStartDate,
	case when FCE.FCEStartTime = "24:00" then "23:59" else FCE.FCEStartTime end EpisodeStartTime,
	FCE.GpCode RegisteredGpCode,
	AdmitDisch.HospCode HospitalCode,
	AdmitDisch.MethodOfAdmission AdmissionMethodCode,
	AdmitDisch.SourceOfAdm AdmissionSourceCode,
	AdmitDisch.IntdMgmt ManagementIntentionCode,
	AdmitDisch.MethodOfDischarge DischargeMethodCode,
	AdmitDisch.DestinationOnDischarge DischargeDestinationCode,
	FCE.Consultant ConsultantCode,
	FCE.Specialty SpecialtyCode,
	FCE.KornerEpisodePrimaryDiagnosisCode PrimaryDiagnosisCode,
	FCE.Subsid SubsidiaryDiagnosisCode,
	FCE.KornerEpisodePrimaryProcedureCode PrimaryOperationCode,
	FCE.KornerEpisodePrimaryProcedureDateExternal PrimaryOperationDate,
	AdmitDisch.TransFrom TransferFrom,
	Patient.GdpCode RegisteredGdpCode,
	case
	when FCE.IntdMgmt="I" then "1"
	when FCE.IntdMgmt="B" then "1"
	when FCE.IntdMgmt="D" AND FCE.MethodOfAdmission Not In 
		(
		"WL",
		"EL",
		"BA",
		"BL",
		"BP",
		"PA",
		"PL"
		) then "1"
	when FCE.IntdMgmt="D" AND AdmitDisch.DischargeDate Is Null then "1"
	when FCE.IntdMgmt="D" AND AdmitDisch.AdmissionDate <> AdmitDisch.DischargeDate then "1"
	when FCE.IntdMgmt="D" AND AdmitDisch.AdmissionDate = AdmitDisch.DischargeDate then "2"
	when FCE.IntdMgmt="V" then "1"
	when FCE.IntdMgmt="R" then "3"
	when FCE.IntdMgmt="N" then "4"
	end PatientClassificationCode,
	FCE.EpisodicGP EpisodicGpCode,
	AdmitDisch.CaseNoteNumber CasenoteNumber,
	Patient.NHSNumberStatus NHSNumberStatusCode,
	AdmitDisch.Category SourceAdminCategoryCode,
	AdmitDisch.A1stRegDaynightAdm FirstRegDayOrNightAdmit,
	case when AdmitDisch.AdmissionTime = "24:00" then "23:59" else AdmitDisch.AdmissionTime end AdmissionTime,
	case when AdmitDisch.DischargeTime = "24:00" then "23:59" else AdmitDisch.DischargeTime end DischargeTime,
	SPEC.DohCode NationalSpecialtyCode,
	Episode.AdtNeonatalLevelOfCareInt NeonatalLevelOfCare,
	FCE.HrgCode SourceHRGCode,
	case when FCE.FCEEndDate = AdmitDisch.DischargeDate then "Y" else "N" end LastEpisodeInSpellIndicator,
	FCE.ProviderCode SourceProviderCode,
	FCE.PurchaserCode SourcePurchaserCode,
	FCE.ContractId SourceContractSerialNo,
	FCE.WardCdadmit StartWardType,
	FCE.WardCdend EndWardType
FROM
	FCEEXT FCE
INNER JOIN ADMITDISCH AdmitDisch 
ON	FCE.EpisodeNumber = AdmitDisch.EpisodeNumber
AND	FCE.InternalPatientNumber = AdmitDisch.InternalPatientNumber
INNER JOIN PATDATA Patient 
ON	FCE.InternalPatientNumber = Patient.InternalPatientNumber
INNER JOIN CONSEPISODE Episode 
ON	FCE.FCEStartDTimeInt = Episode.EpsActvDtimeInt
AND	FCE.EpisodeNumber = Episode.EpisodeNumber
AND	FCE.InternalPatientNumber = Episode.InternalPatientNumber
INNER JOIN SPEC 
ON	FCE.Specialty = SPEC.SPECID
WHERE
	FCE.EpsActvDtimeInt between ' + @from + ' and ' + @to + '
'')
'
if @debug = 1 
	print @sql
else
	exec (@sql)
SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT
-- open episodes
select @sql = '
insert into TImportIpEncounter
(
	SourceKey, InterfaceCode, DistrictNo, SourcePatientNo, SourceEncounterNo2, SourceSpellNo, SourceEncounterNo, PatientTitle, PatientForename, PatientSurname, DateOfBirth, DateOfDeath, SexCode, NHSNumber, PostCode, Address1, Address2, Address3, DhaCode, EthnicOriginCode, MaritalStatusCode, ReligionCode, DateOnWaitingList, AdmissionDate, DischargeDate, EpisodeEndDate, EpisodeEndTime, EpisodeStartDate, EpisodeStartTime, RegisteredGpCode, HospitalCode, AdmissionMethodCode, AdmissionSourceCode, ManagementIntentionCode, DischargeMethodCode, DischargeDestinationCode, ConsultantCode, SpecialtyCode, PrimaryDiagnosisCode, SubsidiaryDiagnosisCode, PrimaryOperationCode, PrimaryOperationDate, TransferFrom, RegisteredGdpCode, PatientClassificationCode, EpisodicGpCode, CasenoteNumber, NHSNumberStatusCode, SourceAdminCategoryCode, FirstRegDayOrNightAdmit, AdmissionTime, DischargeTime, NationalSpecialtyCode, NeonatalLevelOfCare, SourceHRGCode, LastEpisodeInSpellIndicator, SourceProviderCode, SourcePurchaserCode, SourceContractSerialNo, StartWardType, EndWardType
)
select
	SourceKey,
	''INQ'' InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo2, 
	SourcePatientNo + ''/'' + SourceEncounterNo2 SourceSpellNo, 
	SourceEncounterNo, 
	PatientTitle, PatientForename, PatientSurname, DateOfBirth, DateOfDeath, SexCode, NHSNumber, PostCode, Address1, Address2, Address3, DhaCode, EthnicOriginCode, MaritalStatusCode, ReligionCode, 
	DateOnWaitingList, 
	AdmissionDate, 
	null DischargeDate, 
	null EpisodeEndDate, 
	null EpisodeEndTime, 
	EpisodeStartDate, 
	case
	when EpisodeStartTime = '':'' then null
	else EpisodeStartDate + '' '' + EpisodeStartTime
	end EpisodeStartTime, 
	
	RegisteredGpCode, HospitalCode, AdmissionMethodCode, AdmissionSourceCode, ManagementIntentionCode, 
	null DischargeMethodCode, 
	null DischargeDestinationCode, 
	ConsultantCode, SpecialtyCode, 
	
	null PrimaryDiagnosisCode, 
	null SubsidiaryDiagnosisCode, 
	null PrimaryOperationCode, 
	null PrimaryOperationDate, 
	TransferFrom, RegisteredGdpCode, 
	rtrim(PatientClassificationCode) PatientClassificationCode,
	null EpisodicGpCode, 
	CasenoteNumber, NHSNumberStatusCode, SourceAdminCategoryCode, 
	left(FirstRegDayOrNightAdmit, 1) FirstRegDayOrNightAdmit, 
	case
	when AdmissionTime = '':'' then null
	else AdmissionDate + '' '' + AdmissionTime
	end AdmissionTime, 
	null DischargeTime, 
	NationalSpecialtyCode, NeonatalLevelOfCare, SourceHRGCode, 
	null LastEpisodeInSpellIndicator, 
	null SourceProviderCode, 
	null SourcePurchaserCode, 
	null SourceContractSerialNo, 
	StartWardType, 
	null EndWardType
from
	openquery(INQUIRE, ''
SELECT  
	Episode.CONSEPISODEID SourceKey,
	Patient.DistrictNumber DistrictNo,
	MidnightBedState.InternalPatientNumber SourcePatientNo,
	MidnightBedState.EpisodeNumber SourceEncounterNo2,
	"999" SourceEncounterNo,
	Patient.Title PatientTitle,
	Patient.Forenames PatientForename,
	Patient.Surname PatientSurname,
	Patient.PtDoB DateOfBirth,
	Patient.PtDateOfDeath DateOfDeath,
	Patient.Sex SexCode,
	Patient.NHSNumber,
	Patient.PtAddrPostCode PostCode,
	Patient.PtAddrLine1 Address1,
	Patient.PtAddrLine2 Address2,
	Patient.PtAddrLine3 Address3,
	Patient.HealthAuthorityCode DhaCode,
	Patient.EthnicType EthnicOriginCode,
	Patient.MaritalStatus MaritalStatusCode,
	Patient.Religion ReligionCode,
	AdmitDisch.WlDateCcyy DateOnWaitingList,
	AdmitDisch.AdmissionDate AdmissionDate,
	Episode.EpisodeStartDate,
	case when Episode.EpisodeStartTime = "24:00" then "23:59" else Episode.EpisodeStartTime end EpisodeStartTime,
	Patient.GpCode RegisteredGpCode,
	AdmitDisch.HospCode HospitalCode,
	AdmitDisch.MethodOfAdmission AdmissionMethodCode,
	AdmitDisch.SourceOfAdm AdmissionSourceCode,
	AdmitDisch.IntdMgmt ManagementIntentionCode,
	AdmitDisch.MethodOfDischarge DischargeMethodCode,
	AdmitDisch.DestinationOnDischarge DischargeDestinationCode,
	MidnightBedState.Consultant ConsultantCode,
	MidnightBedState.Specialty SpecialtyCode,
	AdmitDisch.TransFrom TransferFrom,
	Patient.GdpCode RegisteredGdpCode,
	"1" PatientClassificationCode,
	AdmitDisch.CaseNoteNumber CasenoteNumber,
	Patient.NHSNumberStatus NHSNumberStatusCode,
	MidnightBedState.Category SourceAdminCategoryCode,
	AdmitDisch.A1stRegDaynightAdm FirstRegDayOrNightAdmit,
	case when AdmitDisch.AdmissionTime = "24:00" then "23:59" else AdmitDisch.AdmissionTime end AdmissionTime,
	SPEC.DohCode NationalSpecialtyCode,
	Episode.AdtNeonatalLevelOfCareInt NeonatalLevelOfCare,
	Episode.HRGCode SourceHRGCode,
	AdmitDisch.AdmWard StartWardType
FROM
	MIDNIGHTBEDSTATE MidnightBedState
INNER JOIN PATDATA Patient 
ON	MidnightBedState.InternalPatientNumber = Patient.InternalPatientNumber
INNER JOIN ADMITDISCH AdmitDisch 
ON	MidnightBedState.EpisodeNumber = AdmitDisch.EpisodeNumber
AND	MidnightBedState.InternalPatientNumber =  AdmitDisch.InternalPatientNumber
INNER JOIN CONSEPISODE  Episode 
ON	MidnightBedState.EpisodeNumber = Episode.EpisodeNumber
AND	MidnightBedState.InternalPatientNumber = Episode.InternalPatientNumber
LEFT JOIN SPEC 
ON	MidnightBedState.Specialty = SPEC.SPECID
WHERE
	Episode.EpsActvDtimeInt <= ' + @to + ' 
AND	(
		Episode.ConsultantEpisodeEndDttm > ' + @to + ' 
	or	Episode.ConsultantEpisodeEndDttm Is Null
	)
AND	MidnightBedState.StatisticsDate = ' + @census + '
'')
'
if @debug = 1 
	print @sql
else
	exec (@sql)
SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())
SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'
EXEC WriteAuditLogEvent 'InquireExtractFCE', @Stats


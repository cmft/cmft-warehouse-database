﻿
CREATE procedure [dbo].[BuildEncounterRecnoMap] as


--remove orphaned records
delete
from
	dbo.EncounterRecnoMap
where
	not exists
	(
	select
		1
	from
		Warehouse.APC.Encounter Encounter with (nolock)
	where
		Encounter.AdmissionMethodCode in ( 'BL', 'WL' , '18' )
	and	Encounter.SpecialtyCode not in 
		(
		select
			SpecialtyCode
		from
			dbo.ExcludedSpecialty
		)

	--remove CEA admissions
	and	Encounter.CancelledElectiveAdmissionFlag <> 1

	--spells only
	and	Encounter.AdmissionTime = Encounter.EpisodeStartTime

	and	EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
	)

and	EncounterRecnoMap.EncounterTypeCode = 'IP'


delete
from
	dbo.EncounterRecnoMap
where
	not exists
	(
	select
		1
	from
		Warehouse.OP.Encounter Encounter
	where
		Encounter.SpecialtyCode not in 
		(
		select
			SpecialtyCode
		from
			dbo.ExcludedSpecialty
		)

	--ward attenders
	and	Encounter.IsWardAttender = 0

	--'Other Health Professional'
	and Encounter.ConsultantCode != 'OHP'

	and	EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
	)

and	EncounterRecnoMap.EncounterTypeCode = 'OP'

delete
from
	dbo.EncounterRecnoMap
where
	not exists
	(
	select
		1
	from
		Warehouse.RF.Encounter Encounter with (nolock)
	where
		Encounter.SpecialtyCode not in 
		(
		select
			SpecialtyCode
		from
			dbo.ExcludedSpecialty
		)

	-- exclude 'Other Health Professional'
	and Encounter.ConsultantCode != 'OHP'

	and	EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
	)

and	EncounterRecnoMap.EncounterTypeCode = 'RF'

delete
from
	dbo.EncounterRecnoMap
where
	not exists
	(
	select
		1
	from
		Warehouse.APC.WaitingList WL with (nolock)
	where
		WL.AdmissionMethodCode in ( 'BL', 'WL' , '18' )
	and	WL.SpecialtyCode not in 
		(
		select
			SpecialtyCode
		from
			dbo.ExcludedSpecialty
		)

	and	EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
	)

and	EncounterRecnoMap.EncounterTypeCode = 'IW'

delete
from
	dbo.EncounterRecnoMap
where
	not exists
	(
	select
		1
	from
		Warehouse.OP.WaitingList WL with (nolock)
	where
		WL.SpecialtyCode not in 
		(
		select
			SpecialtyCode
		from
			dbo.ExcludedSpecialty
		)

	-- exclude 'Other Health Professional'
	and WL.ConsultantCode != 'OHP'

	and	EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
	)

and	EncounterRecnoMap.EncounterTypeCode = 'OW'



--inserts


insert into dbo.EncounterRecnoMap
	(
	 EncounterTypeCode
	,SourceEncounterRecno
	)
select
	 EncounterTypeCode = 'IP'
	,SourceEncounterRecno = EncounterRecno
from
	Warehouse.APC.Encounter Encounter with (nolock)
where
	Encounter.AdmissionMethodCode in ( 'BL', 'WL' , '18' )
and AdmissionDate >= '20070101'
and	Encounter.SpecialtyCode not in 
	(
	select SpecialtyCode
	from dbo.ExcludedSpecialty
	)
--remove CEA admissions
and	Encounter.CancelledElectiveAdmissionFlag <> 1
--spells only
and	Encounter.AdmissionTime = Encounter.EpisodeStartTime

and	not exists
	(
	select 1
	from dbo.EncounterRecnoMap
	where EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
	and	EncounterRecnoMap.EncounterTypeCode = 'IP'
	)


insert into dbo.EncounterRecnoMap
	(
	 EncounterTypeCode
	,SourceEncounterRecno
	)
select
	 EncounterTypeCode = 'OP'
	,SourceEncounterRecno = EncounterRecno
from
	Warehouse.OP.Encounter Encounter
where
	Encounter.SpecialtyCode not in 
	(
	select SpecialtyCode
	from dbo.ExcludedSpecialty
	)
and AppointmentDate>= '20070101'
-- ward attenders
and	Encounter.IsWardAttender = 0

-- 'Other Health Professional'
and Encounter.ConsultantCode != 'OHP'

and	not exists
	(
	select
		1
	from
		dbo.EncounterRecnoMap
	where
		EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
	and	EncounterRecnoMap.EncounterTypeCode = 'OP'
	)


insert into dbo.EncounterRecnoMap
	(
	 EncounterTypeCode
	,SourceEncounterRecno
	)
select
	 EncounterTypeCode = 'RF'
	,SourceEncounterRecno = EncounterRecno
from
	Warehouse.RF.Encounter Encounter with (nolock)
where
	Encounter.SpecialtyCode not in 
	(
	select SpecialtyCode
	from dbo.ExcludedSpecialty
	)
and ReferralDate>= '20070101'
-- exclude 'Other Health Professional'
and Encounter.ConsultantCode != 'OHP'

and	not exists
	(
	select
		1
	from
		dbo.EncounterRecnoMap
	where
		EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
	and	EncounterRecnoMap.EncounterTypeCode = 'RF'
	)


insert into dbo.EncounterRecnoMap
	(
	 EncounterTypeCode
	,SourceEncounterRecno
	)
select
	 EncounterTypeCode = 'IW'
	,SourceEncounterRecno = WL.EncounterRecno
from
	Warehouse.APC.WaitingList WL with (nolock)
where
	WL.AdmissionMethodCode in ( 'BL', 'WL' , '18' )
and OriginalDateOnWaitingList>= '20070101'
and	WL.SpecialtyCode not in 
	(
	select
		SpecialtyCode
	from
		dbo.ExcludedSpecialty
	)

and	not exists
	(
	select
		1
	from
		dbo.EncounterRecnoMap
	where
		EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
	and	EncounterRecnoMap.EncounterTypeCode = 'IW'
	)


insert into dbo.EncounterRecnoMap
	(
	 EncounterTypeCode
	,SourceEncounterRecno
	)
select
	 EncounterTypeCode = 'OW'
	,SourceEncounterRecno = WL.EncounterRecno
from
	Warehouse.OP.WaitingList WL with (nolock)
where
	WL.SpecialtyCode not in 
	(
	select SpecialtyCode
	from dbo.ExcludedSpecialty
	)
and ReferralDate>= '20070101'
-- exclude 'Other Health Professional'
and WL.ConsultantCode != 'OHP'

and	not exists
	(
	select
		1
	from
		dbo.EncounterRecnoMap
	where
		EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
	and	EncounterRecnoMap.EncounterTypeCode = 'OW'
	)



﻿CREATE   PROCEDURE [dbo].[WriteAuditLogEvent] @Source char(30), @Event varchar(255) as

INSERT INTO AuditLog 
	(
	EventTime, 
	UserId, 
	Source,
	Event
	)
VALUES 
	(
	getdate(), 
	Suser_SName(),
	@Source,
	@Event
	)


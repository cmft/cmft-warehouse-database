﻿CREATE  PROCEDURE [dbo].[BuildWkOutpatientWL] @CensusDate smalldatetime as

truncate table WkOutpatientWL


insert into WkOutpatientWL
(
	EncounterRecno,
	EncounterEndDate,
	SourcePatientNo,
	SourceEntityRecno,
	HospitalCode,
	ConsultantCode,
	SpecialtyCode,
	ReferralDate,
	RTTPathwayID
)
select
	WL.EncounterRecno,
	WL.CensusDate EncounterEndDate,
	WL.SourcePatientNo,
	WL.SourceEntityRecno,
	WL.SiteCode HospitalCode,
	WL.ConsultantCode,
	WL.SpecialtyCode,
	WL.ReferralDate,
	WL.RTTPathwayID
from
	dbo.OutpatientWL WL
--where
--	WL.CensusDate = 
--	(
--	select
--		max(CensusDate)
--		----add a day on because of new snapshot scheduling
--		--dateadd(dd , 1 , max(CensusDate))
--	from
--		Warehouse.OP.Snapshot Snapshot
--	where
--		Snapshot.CensusDate <= @censusDate
--	)

-- 7 Feb 2008 PDO
-- changed to check for new OP only therefore use OP Waiting List table rather than future OPs

--select
--	WL.EncounterRecno,
--	WL.CensusDate EncounterEndDate,
--	WL.SourcePatientNo,
--	WL.SourceEntityRecno,
--	WL.SiteCode HospitalCode,
--	WL.ConsultantCode,
--	WL.SpecialtyCode,
--	WL.ReferralDate,
--	WL.RTTPathwayID
--from
--	dbo.OPWaiter WL
--where
--	WL.CensusDate = 
--	(
--	select
--		max(CensusDate)
--	from
--		Modernisation.dbo.Snapshot Snapshot
--	where
--		Snapshot.CensusDate <= @censusDate
--	and	Snapshot.WaitingListTypeCode = 'OP'
--	)
--and	WL.SpecialtyCode <> 'OBS'


﻿CREATE procedure [dbo].[BuildInpatientWL_All] 
	@CensusDate smalldatetime
as



-- can we add an index to the EncounterRecNo

truncate table dbo.InpatientWLAll

insert into dbo.InpatientWLAll
(
	 EncounterRecno
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,FuturePatientCancelDate
	,AdditionFlag
	,LocalRegisteredGpCode
	,LocalEpisodicGpCode
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,TheatreKey
	,TheatreInterfaceCode
	,ProcedureDate
	,FutureCancellationDate
	,ProcedureTime
	,TheatreCode
	,AdmissionReason
	,EpisodeNo
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode
	,SourceEntityRecno
	,DirectorateCode
)
select
	 EncounterRecno = EncounterRecnoMap.EncounterRecno
	,WL.SourcePatientNo
	,WL.SourceEncounterNo
--	,CensusDate = dateadd(day , 1 , WL.CensusDate) -- 2010-04-26 CCB - WL snapshots now generated on previous day
	,CensusDate = @CensusDate -- 2011-05-4 PDO - use census date provided as parameter
	,WL.PatientSurname
	,WL.PatientForename
	,WL.PatientTitle
	,WL.SexCode
	,WL.DateOfBirth
	,WL.DateOfDeath
	,WL.NHSNumber
	,WL.DistrictNo
	,WL.MaritalStatusCode
	,WL.ReligionCode
	,WL.Postcode
	,WL.PatientsAddress1
	,WL.PatientsAddress2
	,WL.PatientsAddress3
	,WL.PatientsAddress4
	,WL.DHACode
	,WL.HomePhone
	,WL.WorkPhone
	,WL.EthnicOriginCode
	,WL.ConsultantCode
	,WL.SpecialtyCode
	,WL.PASSpecialtyCode
	,WL.ManagementIntentionCode
	,WL.AdmissionMethodCode
	,WL.PriorityCode
	,WL.WaitingListCode
	,WL.CommentClinical
	,WL.CommentNonClinical
	,WL.IntendedPrimaryOperationCode
	,WL.Operation
	,WL.SiteCode
	,WL.WardCode
	,WL.WLStatus
	,WL.ProviderCode
	,WL.PurchaserCode
	,WL.ContractSerialNumber
	,WL.CancelledBy
	,WL.BookingTypeCode
	,WL.CasenoteNumber
	,WL.OriginalDateOnWaitingList
	,WL.DateOnWaitingList
	,WL.TCIDate
	,WL.KornerWait
	,WL.CountOfDaysSuspended
	,WL.SuspensionStartDate
	,WL.SuspensionEndDate
	,WL.SuspensionReasonCode
	,WL.SuspensionReason
	,WL.InterfaceCode
	,WL.EpisodicGpCode
	,WL.EpisodicGpPracticeCode
	,WL.RegisteredGpCode
	,WL.RegisteredPracticeCode
	,WL.SourceTreatmentFunctionCode
	,WL.TreatmentFunctionCode
	,WL.NationalSpecialtyCode
	,WL.PCTCode
	,WL.BreachDate
	,WL.ExpectedAdmissionDate
	,WL.ReferralDate
	,WL.FuturePatientCancelDate
	,WL.AdditionFlag
	,WL.LocalRegisteredGpCode
	,WL.LocalEpisodicGpCode
	,WL.NextOfKinName
	,WL.NextOfKinRelationship
	,WL.NextOfKinHomePhone
	,WL.NextOfKinWorkPhone
	,WL.ExpectedLOS
	,WL.TheatreKey
	,WL.TheatreInterfaceCode
	,WL.ProcedureDate
	,WL.FutureCancellationDate
	,WL.ProcedureTime
	,WL.TheatreCode
	,WL.AdmissionReason
	,WL.EpisodeNo
	,WL.BreachDays
	,WL.MRSA
	,RTTPathwayID 
	,RTTPathwayCondition 
	,RTTStartDate 
	,RTTEndDate 
	,RTTSpecialtyCode 
	,RTTCurrentProviderCode 
	,RTTCurrentStatusCode 
	,RTTCurrentStatusDate 
	,RTTCurrentPrivatePatientFlag 
	,RTTOverseasStatusFlag 
	,WL.NationalBreachDate
	,WL.NationalBreachDays
	,WL.DerivedBreachDays
	,WL.DerivedClockStartDate
	,WL.DerivedBreachDate
	,WL.RTTBreachDate
	,WL.RTTDiagnosticBreachDate
	,WL.NationalDiagnosticBreachDate
	,WL.SocialSuspensionDays
	,WL.BreachTypeCode
	,WL.AddedToWaitingListTime
	,WL.SourceUniqueID
	,WL.AdminCategoryCode
	,SourceEntityRecno = WL.SourceEncounterNo
	,WL.DirectorateCode
from
	Warehouse.APC.WaitingList WL with (nolock)

inner join dbo.EncounterRecnoMap
on	EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
	and	EncounterRecnoMap.EncounterTypeCode = 'IW'

where
	not exists
		(
		Select 
			1
		from
			Warehouse.APC.WaitingList Latest
		where
			Latest.SourcePatientNo  = WL.SourcePatientNo 
		and Latest.SourceEncounterNo  = WL.SourceEncounterNo
		and Latest.censusdate > WL.censusdate
		)

/*	
	--if rtt details have been added on subsequent snapshots then use them
left join Warehouse.APC.WaitingList LatestWL
on	WL.SourcePatientNo = LatestWL.SourcePatientNo 
and	WL.SourceEncounterNo = LatestWL.SourceEncounterNo 
and	WL.CensusDate < LatestWL.CensusDate 
and	LatestWL.RTTPathwayID is not null
and	WL.RTTPathwayID is null
and	not exists
	(
	select
		1
	from
		Warehouse.APC.WaitingList Previous
	where
		Previous.SourcePatientNo = WL.SourcePatientNo
	and	Previous.SourceEncounterNo = WL.SourceEncounterNo
	and	Previous.CensusDate > WL.CensusDate
	and	Previous.RTTPathwayID is not null
	and	WL.RTTPathwayID is null
	and	(
			Previous.CensusDate > LatestWL.CensusDate
		or	(
				Previous.CensusDate = LatestWL.CensusDate
			and	Previous.EncounterRecno > LatestWL.EncounterRecno
			)
		)
	)

inner join dbo.EncounterRecnoMap
on	EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
and	EncounterRecnoMap.EncounterTypeCode = 'IW'

where
	WL.CensusDate = 
		(
		select
			max(CensusDate)
		from
			Warehouse.APC.WaitingList Latest

		inner join dbo.EncounterRecnoMap
		on	EncounterRecnoMap.SourceEncounterRecno = Latest.EncounterRecno
		and	EncounterRecnoMap.EncounterTypeCode = 'IW'

		where
			Latest.CensusDate <= @CensusDate
		)
*/

﻿CREATE  procedure [dbo].[ExecuteBuildModel]
	 @fromDate smalldatetime = null
as

-- this builds the reporting model for a given date range.
-- a snapshot is generated for the given @toDate.

select
	@fromDate =
		coalesce(
			 @fromDate
			,(select DateValue from dbo.Parameter where Parameter = 'RTTSTARTDATE')
		)

declare @dayOfWeek varchar(50)
declare @dayOfMonth int
declare @workingDayOfTheMonth tinyint
declare @today smalldatetime
declare @toDate smalldatetime
declare @lastDayOfMonth smalldatetime
declare @audit varchar(4000)


select @today = dateadd(day, datediff(day, 0, getdate()), 0)
select @dayOfWeek = datename(dw, @today)
select @dayOfMonth = datepart(day, @today)
select @lastDayOfMonth = dateadd(day, datepart(day, @today) * -1, @today)
select
	@workingDayOfTheMonth =
		0
		--(
		--select
		--	NoOfDays
		--from
		--	Warehouse.DATE.udfGetWorkingDays(@lastDayOfMonth, @today)
		--)


--end of month
if
	@dayOfMonth = 1
	begin

		select
			@toDate =
				dateadd(day, -1, @today)

		select @audit =
			'End of month run.' +
			' From' + cast(@fromDate as varchar) + 
			' to ' + cast(@toDate as varchar)

		print @audit

		EXEC WriteAuditLogEvent 'ExecuteBuildModel', @audit

		exec BuildModel @fromDate, @toDate, 1

	end

--Sunday
if
	@dayOfWeek = 'Sunday'
	begin

		select
			@toDate =
				@today

		select @audit =
			'Sunday run.' +
			' From' + cast(@fromDate as varchar) + 
			' to ' + cast(@toDate as varchar)

		print @audit

		EXEC WriteAuditLogEvent 'ExecuteBuildModel', @audit

		exec BuildModel @fromDate, @toDate, 1

	end


--5th working day
if
	@workingDayOfTheMonth = 5
	begin

		select
			@toDate =
				(select max(CensusDate) from dbo.MatchMaster where CensusDate <= @lastDayOfMonth)

		select @audit =
			'5th working day run.' +
			' From' + cast(@fromDate as varchar) + 
			' to ' + cast(@toDate as varchar)

		print @audit

		EXEC WriteAuditLogEvent 'ExecuteBuildModel', @audit

		exec BuildModel @fromDate, @toDate, 0

	end


--Wednesday
if
	@dayOfWeek = 'Wednesday'
	begin

		select
			@toDate =
				(select max(CensusDate) from dbo.MatchMaster where CensusDate <= dateadd(day, -3, @today))

		select @audit =
			'Wednesday run.' +
			' From' + cast(@fromDate as varchar) + 
			' to ' + cast(@toDate as varchar)

		EXEC WriteAuditLogEvent 'ExecuteBuildModel', @audit

		print @audit

		exec BuildModel @fromDate, @toDate, 0

	end


﻿CREATE procedure [dbo].[MigrateAdjustments] as


insert into RTT.dbo.RTTOPEncounter
select
	 SourcePatientNo = [Internal No]
	,SourceEntityRecno = [Episode No]
	,ReviewedFlag = 1
	,Created = [update date]
	,Updated = [update date]
	,LastUser = 'N/A'
	,SharedBreachFlag = 0
from
	ManualAdjustment.rtt.adjust
where
	[Internal No] <> ''
and	not exists
	(
	select
		1
	from
		RTT.dbo.RTTOPEncounter
	where
		RTTOPEncounter. SourcePatientNo = [Internal No]
	and	RTTOPEncounter.SourceEntityRecno = [Episode No]
	)


insert into RTT.dbo.RTTEncounter
select
	 SourcePatientNo = [Internal No]
	,SourceEntityRecno = [Episode No]
	,ReviewedFlag = 1
	,Created = [update date]
	,Updated = [update date]
	,LastUser = 'N/A'
	,SharedBreachFlag = 0
from
	ManualAdjustment.rtt.adjust
where
	[Internal No] <> ''
and	not exists
	(
	select
		1
	from
		RTT.dbo.RTTEncounter
	where
		RTTEncounter. SourcePatientNo = [Internal No]
	and	RTTEncounter.SourceEntityRecno = [Episode No]
	)

--NOPs
insert into RTT.dbo.RTTOPClockStop
(
	 SourcePatientNo
	,SourceEntityRecno
	,ClockStopDate
	,ClockStopReasonCode
	,ClockStopComment
	,Created
	,Updated
	,LastUser
	,ClockStartDate
	,ClockStartReasonCode
	,WaitReasonCode
	,WaitReasonDate
	,ClockStopReportingDate
	,RTTUserModeCode
)
select
	 SourcePatientNo = [Internal No]
	,SourceEntityRecno = [Episode No]
	,ClockStopDate = enddate
	,ClockStopReasonCode = 'NOP'
	,ClockStopComment = comment
	,Created = [update date]
	,Updated = [update date]
	,LastUser = 'N/A'
	,ClockStartDate = StartDate
	,ClockStartReasonCode = 'NOP'
	,WaitReasonCode = null
	,WaitReasonDate = null
	,ClockStopReportingDate = null --rep_period
	,RTTUserModeCode = 'B'
from
	ManualAdjustment.rtt.adjust
where
	(
		[Not18wks?] is not null
	or	enddate is null
	)
and	not exists
	(
	select
		1
	from
		RTT.dbo.RTTOPClockStop
	where
		RTTOPClockStop.SourcePatientNo = [Internal No]
	and	RTTOPClockStop.SourceEntityRecno = [Episode No]
	)

--Clock stops
insert into RTT.dbo.RTTOPClockStop
(
	 SourcePatientNo
	,SourceEntityRecno
	,ClockStopDate
	,ClockStopReasonCode
	,ClockStopComment
	,Created
	,Updated
	,LastUser
	,ClockStartDate
	,ClockStartReasonCode
	,WaitReasonCode
	,WaitReasonDate
	,ClockStopReportingDate
	,RTTUserModeCode
)
select
	 SourcePatientNo = [Internal No]
	,SourceEntityRecno = [Episode No]
	,ClockStopDate = enddate
	,ClockStopReasonCode = '30'
	,ClockStopComment = comment
	,Created = [update date]
	,Updated = [update date]
	,LastUser = 'N/A'
	,ClockStartDate = StartDate
	,ClockStartReasonCode = null
	,WaitReasonCode = null
	,WaitReasonDate = null
	,ClockStopReportingDate = null --rep_period
	,RTTUserModeCode = 'B'
from
	ManualAdjustment.rtt.adjust
where
	[Internal No] <> ''
and	not exists
	(
	select
		1
	from
		RTT.dbo.RTTOPClockStop
	where
		RTTOPClockStop.SourcePatientNo = [Internal No]
	and	RTTOPClockStop.SourceEntityRecno = [Episode No]
	)
and	Pause_start is null
and	enddate between '1 jan 1980' and DATEADD(year, 1, getdate())


--select
--	 SourcePatientNo = [Internal No]
--	,SourceEntityRecno = [Episode No]
--	,[prev month start]
--	,[Not18wks?]
--	,[days to remove]
--	,[match]
--	,[dup]
--	,[type]
--	,[comment]
--	,[enddate]
--	,[StartDate]
--	,[dir]
--	,[ori_days_rem]
--	,[rep_period]
--	,[copy_prev_month_start]
--	,[treat_]
--	,[delete?]
--	,[update date]
--	,[Pause?]
--	,[Pause_start]
--	,[tci]
--from
--	ManualAdjustment.rtt.adjust
--where
--	[Internal No] = '1549105'


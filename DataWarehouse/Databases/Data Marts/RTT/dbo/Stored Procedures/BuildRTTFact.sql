﻿CREATE  procedure [dbo].[BuildRTTFact] 
	 @CensusDate smalldatetime = null
	,@debug bit = 1
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--removed on AB's instruction 14 Nov 2009

----force clock stops for non-breach specialties
--insert into dbo.RTTOPEncounter
--	(
--	 SourcePatientNo
--	,SourceEntityRecno
--	,ReviewedFlag
--	,Created
--	,LastUser
--	)
--select
--	 Referral.SourcePatientNo
--	,Referral.SourceEncounterNo2
--	,1
--	,getdate()
--	,'TRIGGERED SPECIALTIES'
--from
--	dbo.Referral
--
--inner join dbo.BacklogTriggerSpecialty
--on	BacklogTriggerSpecialty.SpecialtyCode = Referral.SpecialtyCode
--
--where
--	datediff(day, Referral.ReferralDate, @CensusDate) > 126
--
--and	not exists
--	(
--	select
--		1
--	from
--		dbo.RTTOPEncounter
--	where
--		RTTOPEncounter.SourcePatientNo = Referral.SourcePatientNo
--	and	RTTOPEncounter.SourceEntityRecno = Referral.SourceEncounterNo2
--	)
--
--
--insert into dbo.RTTOPClockStop
--(
--	 SourcePatientNo
--	,SourceEntityRecno
--	,ClockStopDate
--	,ClockStopReasonCode
--	,ClockStopComment
--	,Created
--	,LastUser
--	,RTTUserModeCode
--)
--select
--	 Referral.SourcePatientNo
--	,Referral.SourceEncounterNo2
--	,dateadd(day, 127, Referral.ReferralDate)
--	,'SYS'	--System close
--	,'Automatically set clock stop for patients'
--	,getdate()
--	,system_user
--	,RTTUserModeCode = 'S'
--from
--	dbo.Referral
--
--inner join dbo.BacklogTriggerSpecialty
--on	BacklogTriggerSpecialty.SpecialtyCode = Referral.SpecialtyCode
--
--where
--	datediff(day, Referral.ReferralDate, @CensusDate) > 126
----and	Referral.RTTPathwayId is null
--
--and	not exists
--	(
--	select
--		1
--	from
--		dbo.RTTOPClockStop
--	where
--		RTTOPClockStop.SourcePatientNo = Referral.SourcePatientNo
--	and	RTTOPClockStop.SourceEntityRecno = Referral.SourceEncounterNo2
--	)


--repopulate the Snapshot table

if @debug = 1
	print convert(varchar, getdate()) + ' - Prepare OLAPSnapshot'

delete from dbo.OlapSnapshot

insert into dbo.OlapSnapshot
select distinct
	CensusDate,
	left(convert(varchar, CensusDate, 113), 11)
from
	dbo.MatchMaster
where
	CensusDate >= dateadd(day, (select NumericValue*-7 from Parameter where Parameter = 'KEEPSNAPSHOTWEEKS'), getdate())

if @debug = 1
	print convert(varchar, getdate()) + ' - Create TRTTFactBase'

truncate table TRTTFactBase

insert
into
	TRTTFactBase
(
	 CensusDate
	,EncounterRecno
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
	,PCTCode
	,SiteCode
	,PathwayStatusCode
	,ClockStartDate
	,WorkflowStatusCode
	,KeyDate
	,Cases
	,TCIBeyondBreach
	,LastWeeksCases
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,ReportableFlag
	,UnadjustedClockStartDate
	,UnadjustedTCIBeyondBreach
	,RTTBreachDate
	,ReferringProviderCode
	,ClockStopDate
	--,RTTFactTypeCode
)
select distinct
	 CensusDate
	,EncounterRecno
	,ConsultantCode
	,Encounter.SpecialtyCode
	,PracticeCode
	,PCTCode
	,SiteCode
	,PathwayStatusCode
	,ClockStartDate
	,WorkflowStatusCode
	,KeyDate
	,Cases
	,TCIBeyondBreach
	,LastWeeksCases
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,ReportableFlag
	,UnadjustedClockStartDate
	,UnadjustedTCIBeyondBreach
	,RTTBreachDate
	,ReferringProviderCode
	,ClockStopDate
	--,RTTFactTypeCode
from
	dbo.RTTFactBase Encounter
where
	Encounter.KeyDate between '1 Jan 2007' and Encounter.CensusDate

and	coalesce(Encounter.ClockChangeReasonCode, '') <> 'NOP' --Not an 18 week pathway

--only load those encounters that have been waiting since month start or for the past 7 days
and	(
		datediff(day, Encounter.KeyDate, Encounter.CensusDate) < 8
	or	datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0
	)


----Peter - the next 5 lines need to be deleted to back out the latest change (10 Dec 2008)
--and	not
--	(
--		Encounter.WorkflowStatusCode in ('O', 'P') --outpatient waiters or referred only
--	and	Encounter.RTTPathwayID is null
--	)

--and	not
--	(
--		Encounter.WorkflowStatusCode in ('O', 'P') --outpatient waiters or referred only
--	and	Encounter.RTTPathwayID is null
--	)


--option (MAXDOP 1)


if @debug = 1
	print convert(varchar, getdate()) + ' - Create #RTTFactUnknownClockStartBase'

select distinct
	 Encounter.CensusDate
	,Encounter.ReferralRecno 
	,Encounter.EncounterTypeCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.PracticeCode
	,Encounter.PCTCode
	,Encounter.SiteCode
	,Encounter.WeekBandCode
	,Encounter.PathwayStatusCode
	,Encounter.EncounterStartDate
	,Encounter.WorkflowStatusCode
	,Encounter.BreachBandCode
	,coalesce(Encounter.DaysWaiting, 0) DaysWaiting
	,Encounter.Cases
	,Encounter.TCIBeyondBreach
	,Encounter.LastWeeksCases
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.ReportableFlag
	,Encounter.ReferringProviderCode
	--,Encounter.RTTFactTypeCode
into
	#RTTFactUnknownClockStartBase
from
	dbo.RTTFactUnknownClockStartBase Encounter
where
	Encounter.CensusDate = @CensusDate


if @debug = 1
	print convert(varchar, getdate()) + ' - Delete old snapshot'

delete from dbo.RTTFact
where
	CensusDate = @CensusDate

SELECT @RowsDeleted = @@ROWCOUNT

if @debug = 1
	print convert(varchar, getdate()) + ' - Insert into RTTFact'

insert into dbo.RTTFact
(
	CensusDate
	,EncounterRecno
	,EncounterTypeCode
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode
	,PCTCode
	,SiteCode
	,WeekBandCode
	,PathwayStatusCode
	,ReferralDate
	,WorkflowStatusCode
	,BreachBandCode
	,DaysWaiting
	,Cases
	,TCIBeyondBreach
	,LastWeeksCases
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,ReportableFlag
	,AdjustedFlag
	,RTTBreachDate
	,KeyDate
	,ReferringProviderCode
	--,RTTFactTypeCode
)




-- adjusted data

select
	 CensusDate
	,EncounterRecno
	,EncounterTypeCode = 'REFERRAL'
	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
	,PracticeCode
	,PCTCode
	,SiteCode = coalesce(Encounter.SiteCode, '##')

	,WeekBandCode =
		case
		when Encounter.ClockStartDate is null then '53' -- patients with unknown clock start date
		when datediff(day, Encounter.ClockStartDate, Encounter.ClockStopDate) < 0 then '00'
		when datediff(day, Encounter.ClockStartDate, Encounter.ClockStopDate) = 126 then '17'
		when datediff(day, Encounter.ClockStartDate, Encounter.ClockStopDate) / 7 > 51 then '52'
		else right('0' + convert(varchar, datediff(day, Encounter.ClockStartDate, Encounter.ClockStopDate) / 7), 2)
		end
	
	,PathwayStatusCode

	,ClockStartDate =
		coalesce(
			 Encounter.ClockStartDate
			,Encounter.ClockStopDate
		)

	,WorkflowStatusCode

	,BreachBandCode =
		case
		when Encounter.ClockStartDate is null then 'UK' -- patients with unknown clock start date
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.ClockStopDate) between 1 and 7 then 'BR01'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.ClockStopDate) > 7 then 'BR02'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.ClockStopDate) between -7 and 0 then 'F01'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.ClockStopDate) between -14 and -8 then 'F12'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.ClockStopDate) between -28 and -15 then 'F24'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.ClockStopDate) between -42 and -29 then 'F46'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.ClockStopDate) between -56 and -43 then 'F68'
		else 'F99'
		end 

	,DaysWaiting =
		coalesce(datediff(day, Encounter.ClockStartDate, Encounter.ClockStopDate), 0) 

	,Cases =
		case
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then Cases 
		else 0 
		end
	
	,TCIBeyondBreach =
		case
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then TCIBeyondBreach 
		else 0 
		end

	,LastWeeksCases

	,PrimaryDiagnosisCode
	,PrimaryOperationCode

	,ReportableFlag

	,AdjustedFlag = 'Y'

	,RTTBreachDate

	,KeyDate

	,ReferringProviderCode = coalesce(Encounter.ReferringProviderCode, '##')

	--,RTTFactTypeCode

from
	TRTTFactBase Encounter




union all -- Patients admitted for which we have no referral link

select
	 Encounter.CensusDate
	,Encounter.ReferralRecno 
	,Encounter.EncounterTypeCode
	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
	,Encounter.PracticeCode
	,Encounter.PCTCode
	,SiteCode = coalesce(Encounter.SiteCode, '##')
	,Encounter.WeekBandCode
	,Encounter.PathwayStatusCode
	,Encounter.EncounterStartDate
	,Encounter.WorkflowStatusCode
	,Encounter.BreachBandCode
	,coalesce(Encounter.DaysWaiting, 0)
	,Encounter.Cases
	,Encounter.TCIBeyondBreach
	,Encounter.LastWeeksCases
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.ReportableFlag
	,AdjustedFlag = 'Y'
	,RTTBreachDate = null
	,KeyDate = null
	,ReferringProviderCode = coalesce(Encounter.ReferringProviderCode, '##')

	--,RTTFactTypeCode
from
	#RTTFactUnknownClockStartBase Encounter




union all

-- non-adjusted data

select
	 CensusDate
	,EncounterRecno
	,EncounterTypeCode = 'REFERRAL' 
	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
	,Encounter.PracticeCode
	,Encounter.PCTCode
	,SiteCode = coalesce(Encounter.SiteCode, '##')

	,WeekBandCode =
		case
		when Encounter.UnadjustedClockStartDate is null then '53' -- patients with unknown clock start date
		when datediff(day, Encounter.UnadjustedClockStartDate, Encounter.ClockStopDate) < 0 then '00'
		when datediff(day, Encounter.UnadjustedClockStartDate, Encounter.ClockStopDate) = 126 then '17'
		when datediff(day, Encounter.UnadjustedClockStartDate, Encounter.ClockStopDate) / 7 > 51 then '52'
		else right('0' + convert(varchar, datediff(day, Encounter.UnadjustedClockStartDate, Encounter.ClockStopDate) / 7), 2)
		end
	

	,PathwayStatusCode

	,UnadjustedClockStartDate =
		coalesce(
			 Encounter.UnadjustedClockStartDate
			,Encounter.ClockStopDate
		)

	,WorkflowStatusCode

	,BreachBandCode =
		case
		when Encounter.UnadjustedClockStartDate is null then 'UK' -- patients with unknown clock start date
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.ClockStopDate) between 1 and 7 then 'BR01'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.ClockStopDate) > 7 then 'BR02'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.ClockStopDate) between -7 and 0 then 'F01'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.ClockStopDate) between -14 and -8 then 'F12'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.ClockStopDate) between -28 and -15 then 'F24'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.ClockStopDate) between -42 and -29 then 'F46'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.ClockStopDate) between -56 and -43 then 'F68'
		else 'F99'
		end 

	,DaysWaiting =
		coalesce(datediff(day, Encounter.UnadjustedClockStartDate, Encounter.ClockStopDate), 0)

	,Cases =
		case 
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then Cases 
		else 0 
		end

	,TCIBeyondBreach =
		case 
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then UnadjustedTCIBeyondBreach 
		else 0 
		end 

	,LastWeeksCases

	,PrimaryDiagnosisCode
	,PrimaryOperationCode

	,ReportableFlag

	,AdjustedFlag = 'N'

	,RTTBreachDate

	,KeyDate

	,ReferringProviderCode = coalesce(Encounter.ReferringProviderCode, '##')

	--,RTTFactTypeCode

from
	TRTTFactBase Encounter





union all -- Patients admitted for which we have no referral link

select
	 Encounter.CensusDate
	,Encounter.ReferralRecno 
	,Encounter.EncounterTypeCode
	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
	,Encounter.PracticeCode
	,Encounter.PCTCode
	,SiteCode = coalesce(Encounter.SiteCode, '##')
	,Encounter.WeekBandCode
	,Encounter.PathwayStatusCode
	,UnadjustedClockStartDate = Encounter.EncounterStartDate
	,Encounter.WorkflowStatusCode
	,Encounter.BreachBandCode
	,coalesce(Encounter.DaysWaiting, 0)
	,Encounter.Cases
	,Encounter.TCIBeyondBreach
	,Encounter.LastWeeksCases
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryOperationCode
	,Encounter.ReportableFlag
	,AdjustedFlag = 'N'
	,RTTBreachDate = null
	,KeyDate = null
	,ReferringProviderCode = coalesce(Encounter.ReferringProviderCode, '##')

	--,RTTFactTypeCode
from
	#RTTFactUnknownClockStartBase Encounter
where
	Encounter.CensusDate = @CensusDate







--union all

----GUM
--select
--	 @CensusDate
--	,Encounter.EncounterRecno 
--	,Encounter.EncounterTypeCode
--	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
--	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
--	,Encounter.PracticeCode
--	,Encounter.PCTCode
--	,SiteCode = coalesce(Encounter.SiteCode, '##')
--	,Encounter.WeekBandCode
--	,Encounter.PathwayStatusCode
--	,EncounterStartDate = Encounter.TreatmentDate
--	,Encounter.WorkflowStatusCode
--	,Encounter.BreachBandCode
--	,coalesce(Encounter.DaysWaiting, 0)
--	,Encounter.Cases
--	,Encounter.TCIBeyondBreach

--	,LastWeeksCases =
--	case
--	when datediff(day, dateadd(day, -1, TreatmentDate) ,@CensusDate) < 8 
--	then Encounter.Cases
--	else 0
--	end 

--	,Encounter.PrimaryDiagnosisCode
--	,Encounter.PrimaryOperationCode
--	,Encounter.ReportableFlag
--	,AdjustedFlag = 'N'
--	,RTTBreachDate = null
--	,KeyDate = TreatmentDate
--	,ReferringProviderCode = 'RW6'
--from
--	dbo.RTTGUM Encounter
--where
----only load those encounters that have been waiting since month start or for the past 7 days
--	(
--		datediff(day, Encounter.TreatmentDate, @CensusDate) < 8
--	or	datediff(month, Encounter.TreatmentDate, @CensusDate) = 0
--	)
--and	Encounter.TreatmentDate <= @CensusDate

--union all

----GUM
--select
--	 @CensusDate
--	,Encounter.EncounterRecno 
--	,Encounter.EncounterTypeCode
--	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
--	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
--	,Encounter.PracticeCode
--	,Encounter.PCTCode
--	,SiteCode = coalesce(Encounter.SiteCode, '##')
--	,Encounter.WeekBandCode
--	,Encounter.PathwayStatusCode
--	,EncounterStartDate = Encounter.TreatmentDate
--	,Encounter.WorkflowStatusCode
--	,Encounter.BreachBandCode
--	,coalesce(Encounter.DaysWaiting, 0)
--	,Encounter.Cases
--	,Encounter.TCIBeyondBreach

--	,LastWeeksCases =
--	case
--	when datediff(day, dateadd(day, -1, TreatmentDate) ,@CensusDate) < 8 
--	then Encounter.Cases
--	else 0
--	end 

--	,Encounter.PrimaryDiagnosisCode
--	,Encounter.PrimaryOperationCode
--	,Encounter.ReportableFlag
--	,AdjustedFlag = 'Y'
--	,RTTBreachDate = null
--	,KeyDate = Encounter.TreatmentDate
--	,ReferringProviderCode = 'RW6'
--from
--	dbo.RTTGUM Encounter
--where
----only load those encounters that have been waiting since month start or for the past 7 days
--	(
--		datediff(day, Encounter.TreatmentDate, @CensusDate) < 8
--	or	datediff(month, Encounter.TreatmentDate, @CensusDate) = 0
--	)
--and	Encounter.TreatmentDate <= @CensusDate








union all -- Patients admitted following an outpatient treatment

-- adjusted data

select
	 CensusDate
	,EncounterRecno
	,EncounterTypeCode = 'ADMITTED'
	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
	,Encounter.PracticeCode
	,Encounter.PCTCode
	,SiteCode = coalesce(Encounter.SiteCode, '##')

	,WeekBandCode =
		case
		when Encounter.ClockStartDate is null then '53' -- patients with unknown clock start date
		when datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) < 0 then '00'
		when datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) = 126 then '17'
		when datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) / 7 > 51 then '52'
		else right('0' + convert(varchar, datediff(day, Encounter.ClockStartDate, Encounter.KeyDate) / 7), 2)
		end
	
	,PathwayStatusCode

	,ClockStartDate =
		coalesce(
			 Encounter.ClockStartDate
			,Encounter.CensusDate
		)

	,WorkflowStatusCode

	,BreachBandCode =
		case
		when Encounter.ClockStartDate is null then 'UK' -- patients with unknown clock start date
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between 1 and 7 then 'BR01'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) > 7 then 'BR02'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -7 and 0 then 'F01'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -14 and -8 then 'F12'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -28 and -15 then 'F24'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -42 and -29 then 'F46'
		when datediff(day, dateadd(day, 126, Encounter.ClockStartDate), Encounter.KeyDate) between -56 and -43 then 'F68'
		else 'F99'
		end 

	,DaysWaiting =
		coalesce(datediff(day, Encounter.ClockStartDate, Encounter.KeyDate), 0) 

	,Cases =
		case 
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then Cases 
		else 0 
		end 

	,TCIBeyondBreach =
		case 
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then 
		TCIBeyondBreach else 0 
		end 

	,LastWeeksCases

	,PrimaryDiagnosisCode
	,PrimaryOperationCode

	,ReportableFlag

	,AdjustedFlag = 'Y'

	,RTTBreachDate

	,KeyDate

	,ReferringProviderCode = coalesce(Encounter.ReferringProviderCode, '##')

	--,RTTFactTypeCode

from
	dbo.RTTFactMultiEpisodePathwayBase Encounter
where
	Encounter.CensusDate = @CensusDate

--only load those encounters that have been waiting since month start or for the past 7 days
and	(
		datediff(day, Encounter.KeyDate, Encounter.CensusDate) < 8
	or	datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0
	)
and	Encounter.KeyDate <= Encounter.CensusDate









union all -- Patients admitted following an outpatient treatment

-- non-adjusted data

select
	 CensusDate
	,EncounterRecno
	,EncounterTypeCode = 'ADMITTED' 
	,ConsultantCode = coalesce(Encounter.ConsultantCode, '##')
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, '##')
	,Encounter.PracticeCode
	,Encounter.PCTCode
	,SiteCode = coalesce(Encounter.SiteCode, '##')

	,WeekBandCode =
		case
		when Encounter.UnadjustedClockStartDate is null then '53' -- patients with unknown clock start date
		when datediff(day, Encounter.UnadjustedClockStartDate, Encounter.KeyDate) < 0 then '00'
		when datediff(day, Encounter.UnadjustedClockStartDate, Encounter.KeyDate) = 126 then '17'
		when datediff(day, Encounter.UnadjustedClockStartDate, Encounter.KeyDate) / 7 > 51 then '52'
		else right('0' + convert(varchar, datediff(day, Encounter.UnadjustedClockStartDate, Encounter.KeyDate) / 7), 2)
		end
	

	,PathwayStatusCode

	,UnadjustedClockStartDate =
		coalesce(
			 Encounter.UnadjustedClockStartDate
			,Encounter.CensusDate
		)

	,WorkflowStatusCode

	,BreachBandCode =
		case
		when Encounter.UnadjustedClockStartDate is null then 'UK' -- patients with unknown clock start date
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.KeyDate) between 1 and 7 then 'BR01'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.KeyDate) > 7 then 'BR02'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.KeyDate) between -7 and 0 then 'F01'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.KeyDate) between -14 and -8 then 'F12'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.KeyDate) between -28 and -15 then 'F24'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.KeyDate) between -42 and -29 then 'F46'
		when datediff(day, dateadd(day, 126, Encounter.UnadjustedClockStartDate), Encounter.KeyDate) between -56 and -43 then 'F68'
		else 'F99'
		end 

	,DaysWaiting = coalesce(datediff(day, Encounter.UnadjustedClockStartDate, Encounter.KeyDate), 0) 

	,Cases =
		case 
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then Cases 
		else 0 
		end 

	,TCIBeyondBreach =
		case 
		when datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0 then UnadjustedTCIBeyondBreach 
		else 0 
		end 

	,LastWeeksCases

	,PrimaryDiagnosisCode
	,PrimaryOperationCode

	,ReportableFlag

	,AdjustedFlag = 'N'

	,RTTBreachDate

	,KeyDate

	,ReferringProviderCode = coalesce(Encounter.ReferringProviderCode, '##')

	--,RTTFactTypeCode

from
	dbo.RTTFactMultiEpisodePathwayBase Encounter
where
	Encounter.CensusDate = @CensusDate

--only load those encounters that have been waiting since month start or for the past 7 days
and	(
		datediff(day, Encounter.KeyDate, Encounter.CensusDate) < 8
	or	datediff(month, Encounter.KeyDate, Encounter.CensusDate) = 0
	)
and	Encounter.KeyDate <= Encounter.CensusDate


SELECT @RowsInserted = @@ROWCOUNT















--now remove those op discharges that are exclusively DNA or patient cancellations
delete
from
	RTTFact

from
	RTTFact

inner join dbo.MatchMaster
on	MatchMaster.ReferralRecno = RTTFact.EncounterRecno

inner join dbo.Outpatient
on Outpatient.EncounterRecno = MatchMaster.OutpatientRecno

where
	RTTFact.PathwayStatusCode = 'OPD'
and	RTTFact.EncounterTypeCode = 'REFERRAL'
and	RTTFact.CensusDate = MatchMaster.CensusDate
and	not exists
	(
		select
			1
		from
			dbo.Outpatient OutpatientDNA
		where
			OutpatientDNA.SourcePatientNo = Outpatient.SourcePatientNo
		and	OutpatientDNA.SourceEncounterNo2 = Outpatient.SourceEncounterNo2
		and	OutpatientDNA.DNAFlag <> '3'
--20090521 amended to only delete DNAs (3)
--		and	OutpatientDNA.DNAFlag not in ('2', '3')
	)

and	RTTFact.CensusDate = @CensusDate

--adjust the insert count
SELECT @RowsInserted = @RowsInserted - @@ROWCOUNT


if @debug = 1
	print convert(varchar, getdate()) + ' - Build OLAP reference data'

--repopulate OlapPractice table

truncate table dbo.OlapPractice

insert into dbo.OlapPractice
SELECT
	OrganisationCode
	,OrganisationCode + ' - ' + Organisation
	,ROCode
	,HACode
	,Address1
	,Address2
	,Address3
	,Address4
	,Address5
	,Postcode
	,OpenDate
	,CloseDate
	,StatusCode
	,OrganisationSubTypeCode
	,ParentOrganisationCode
	,JoinParentDate
	,LeftParentDate
	,ContactTelNo
	,ContactName
	,AddressType
	,Field0
	,AmendedRecordIndicator
	,WaveNumber
	,CurrentGPFHorPCGCode
	,CurrentGPFHType
	,Field27
	,Field28
FROM
	Organisation.dbo.Practice Practice

where
	exists
	(
	select
		1
	from
		dbo.RTTFact
	where
		RTTFact.PracticeCode = Practice.OrganisationCode
	)

insert into dbo.OlapPractice
(
	OrganisationCode
	,Organisation
)
SELECT distinct
	rtrim(PracticeCode)
	,case when PracticeCode = '##' then 'No Practice' else rtrim(PracticeCode) + ' - No Description' end
from
	dbo.RTTFact
where
	not exists
	(
	select
		1
	from
		dbo.OlapPractice
	where
		OlapPractice.OrganisationCode = RTTFact.PracticeCode
	)

--repopulate OlapPCT table

truncate table dbo.OlapPCT

insert into dbo.OlapPCT
SELECT
	OrganisationCode
	,Organisation
	,ROCode
	,HACode
	,Address1
	,Address2
	,Address3
	,Address4
	,Address5
	,Postcode
	,OpenDate
	,CloseDate
	,StatusCode
	,OrganisationSubTypeCode
	,ParentOrganisationCode
	,JoinParentDate
	,LeftParentDate
	,ContactTelNo
	,ContactName
	,AddressType
	,Field0
	,AmendedRecordIndicator
	,WaveNumber
	,CurrentGPFHorPCGCode
	,CurrentGPFHType
	,Field27
	,Field28
	,case 
	when OrganisationCode = '5AN' then 'NONC' 
	when OrganisationCode like '5%' then OrganisationCode 
	when OrganisationCode like 'T%' then OrganisationCode 
	else 'NONC' 
	end RTTOrganisationCode --sets Non-English code
FROM
	Organisation.dbo.PCT PCT

where
	exists
	(
	select
		1
	from
		dbo.RTTFact
	where
		RTTFact.PCTCode = PCT.OrganisationCode
	)

insert into dbo.OlapPCT
(
	OrganisationCode
	,Organisation
	,RTTOrganisationCode
)
SELECT distinct
	PCTCode
	,case when PCTCode = '##' then 'No PCT' else PCTCode + ' - No Description' end
	,PCTCode
from
	dbo.RTTFact
where
	not exists
	(
	select
		1
	from
		dbo.OlapPCT
	where
		OlapPCT.OrganisationCode = RTTFact.PCTCode
	)


--repopulate OlapConsultant table

truncate table dbo.OlapConsultant

insert into dbo.OlapConsultant
select
	ConsultantLocal.ConsultantCode,
	rtrim(ConsultantLocal.Consultant)
from
	Warehouse.PAS.Consultant ConsultantLocal
where
	exists
	(
	select
		1
	from
		dbo.RTTFact
	where
		RTTFact.ConsultantCode = ConsultantLocal.ConsultantCode
	)


insert into dbo.OlapConsultant
select distinct
	RTTFact.ConsultantCode,

	case
	when RTTFact.ConsultantCode = '##' then 'No Consultant'
	else rtrim(RTTFact.ConsultantCode) + ' - No Description'
	end
from
	dbo.RTTFact
where
	not exists
(
	select
		1
	from
		dbo.OlapConsultant
	where
		OlapConsultant.ConsultantCode = RTTFact.ConsultantCode
	)

update dbo.OlapConsultant
set
	Consultant =  Consultant + ' (' + ConsultantCode + ')'
from
	OlapConsultant
where
	exists
	(
	select
		1
	from
		OlapConsultant DuplicateConsultant
	where
		DuplicateConsultant.ConsultantCode = OlapConsultant.ConsultantCode
		and DuplicateConsultant.Consultant in
		(
			select
				DuplicateConsultant.Consultant
			from
				OlapConsultant DuplicateConsultant
			group by
				DuplicateConsultant.Consultant
			having
				count(*) > 1
		)
	)	

--repopulate OlapSpecialty table

truncate table dbo.OlapSpecialty

insert into dbo.OlapSpecialty
(
	SpecialtyCode,
	Specialty
)
select
	SpecialtyLocal.SpecialtyCode,
	rtrim(SpecialtyLocal.Specialty)
from
	Warehouse.PAS.Specialty SpecialtyLocal
where
	exists
	(
	select
		1
	from
		dbo.RTTFact
	where
		RTTFact.SpecialtyCode = SpecialtyLocal.SpecialtyCode
	)


insert into dbo.OlapSpecialty
(
	SpecialtyCode,
	Specialty
)
select distinct
	RTTFact.SpecialtyCode,

	case
	when RTTFact.SpecialtyCode = '##' then 'No Specialty'
	else rtrim(RTTFact.SpecialtyCode) + ' - No Description'
	end
from
	dbo.RTTFact
where
	not exists
(
	select
		1
	from
		dbo.OlapSpecialty
	where
		OlapSpecialty.SpecialtyCode = RTTFact.SpecialtyCode
	)

update dbo.OlapSpecialty
set
	NationalSpecialtyCode = 
	case
	when SpecialtyLocal.NationalSpecialtyCode is null then '##'
	else SpecialtyLocal.NationalSpecialtyCode
	end,

	NationalSpecialty = 
	case
	when SpecialtyNational.Specialty is null then SpecialtyLocal.NationalSpecialtyCode + ' - No Specialty'
	else SpecialtyNational.Specialty
	end,

	RTTSpecialtyCode = coalesce(RTTSpecialtyMap.XrefEntityCode, SpecialtyLocal.NationalSpecialtyCode, 'X01')

	,RTTSpecialty = 
	case
	when coalesce(RTTSpecialtyMap.XrefEntityCode, SpecialtyLocal.NationalSpecialtyCode, 'X01') = 'X01'
	then 'Other'
	else RTTSpecialty.Specialty
	end

from
	dbo.OlapSpecialty

left join Warehouse.PAS.Specialty SpecialtyLocal
on	SpecialtyLocal.SpecialtyCode = OlapSpecialty.SpecialtyCode

left join Warehouse.WH.Specialty SpecialtyNational
on	SpecialtyNational.SpecialtyCode = SpecialtyLocal.NationalSpecialtyCode

left join dbo.EntityXref RTTSpecialtyMap
on	RTTSpecialtyMap.EntityTypeCode = 'RTTSPECIALTY'
and	RTTSpecialtyMap.XrefEntityTypeCode = 'RTTSPECIALTY'
and	RTTSpecialtyMap.EntityCode = SpecialtyLocal.NationalSpecialtyCode

left join Warehouse.WH.Specialty RTTSpecialty
on	RTTSpecialty.SpecialtyCode = coalesce(RTTSpecialtyMap.XrefEntityCode, SpecialtyLocal.NationalSpecialtyCode, 'X01')



update dbo.OlapSpecialty
set
	Specialty =  Specialty + ' (' + SpecialtyCode + ')'
from
	OlapSpecialty
where
	exists
	(
	select
		1
	from
		OlapSpecialty DuplicateSpecialty
	where
		DuplicateSpecialty.SpecialtyCode = OlapSpecialty.SpecialtyCode
		and DuplicateSpecialty.Specialty in
		(
			select
				DuplicateSpecialty.Specialty
			from
				OlapSpecialty DuplicateSpecialty
			group by
				DuplicateSpecialty.Specialty
			having
				count(*) > 1
		)
	)	

--repopulate OlapSite table

truncate table dbo.OlapSite

insert into dbo.OlapSite
select
	Site.SiteCode,
	Site.Site,
	TrustCode = Site.MappedSiteCode,
	Trust = Trust.Site
from
	Warehouse.PAS.Site Site

inner join Warehouse.WH.Site Trust
on	Trust.SiteCode = Site.MappedSiteCode

where
	exists
	(
	select
		1
	from
		dbo.RTTFact
	where
		RTTFact.SiteCode = Site.SiteCode
	)


insert into dbo.OlapSite
select distinct
	RTTFact.SiteCode,

	case
	when RTTFact.SiteCode = '##' then 'No Site'
	else rtrim(RTTFact.SiteCode) + ' - No Description'
	end,
	'##',
	'No Trust'
from
	dbo.RTTFact
where
	not exists
(
	select
		1
	from
		dbo.OlapSite
	where
		OlapSite.SiteCode = RTTFact.SiteCode
	)


--repopulate the Snapshot table
--
--truncate table dbo.OlapSnapshot
--
--insert into dbo.OlapSnapshot
--select distinct
--	CensusDate,
--	left(convert(varchar, CensusDate, 113), 11)
--from
--	dbo.RTTFact
--



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Census Date ' + CONVERT(varchar, @CensusDate, 103) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'
EXEC WriteAuditLogEvent 'BuildRTTFact', @Stats


﻿CREATE procedure [dbo].[RTTClockStartUpdate]

	 @ClockStartDate smalldatetime
	,@ClockStartReasonCode varchar(10)
	,@ClockStartComment varchar(4000)
	,@RTTClockStartRecno int
as

select
	@ClockStartDate = 
	case
	when @ClockStartDate = '1/1/1980' then null
	when @ClockStartReasonCode in ('NOP', 'AGR') then null --Not 18 week pathway; Agree with current dates
	else @ClockStartDate
	end

	,@ClockStartReasonCode = 
	case
	when @ClockStartReasonCode = '' then null
	else @ClockStartReasonCode
	end


UPDATE RTTClockStart
set
	 ClockStartDate = @ClockStartDate
	,ClockStartReasonCode = @ClockStartReasonCode
	,ClockStartComment = @ClockStartComment
	,Updated = GETDATE()
	,LastUser = SYSTEM_USER
where
	RTTClockStartRecno = @RTTClockStartRecno


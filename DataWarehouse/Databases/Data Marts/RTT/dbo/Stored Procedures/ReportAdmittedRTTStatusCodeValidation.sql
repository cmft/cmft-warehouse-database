﻿CREATE procedure [dbo].[ReportAdmittedRTTStatusCodeValidation]
	 @CensusDate smalldatetime = null
	,@SiteCode varchar(4000) = null
	,@SpecialtyCode varchar(4000) = null
	,@ConsultantCode varchar(4000) = null
	,@PCTCode varchar(4000) = null
as

declare @from smalldatetime
declare @to smalldatetime

select
	@from = 
		'1 ' +
		datename(month, @CensusDate) + ' ' +
		datename(year, @CensusDate)
	,@to = @CensusDate

select
	 Inpatient.EncounterRecno
	,Inpatient.RTTPathwayID
	,SiteCode = Inpatient.EndSiteCode
	,Inpatient.DistrictNo
	,Inpatient.SpecialtyCode
	,Specialty.Specialty
	,Consultant.Consultant
	,Inpatient.PatientSurname
	,OperationCode = Inpatient.PrimaryOperationCode
	,Inpatient.Operation
	,Inpatient.SexCode
	,Inpatient.DateOfBirth
	,Inpatient.ManagementIntentionCode
	,Inpatient.PCTCode
	,Inpatient.DateOnWaitingList
	,Inpatient.RTTStartDate
	,Inpatient.RTTEndDate
	,Inpatient.RTTCurrentStatusCode
	,Inpatient.RTTCurrentStatusDate
	,Inpatient.RTTPeriodStatusCode
from
	dbo.Inpatient

inner join dbo.RTTStatus RTTStatusPeriod
on	RTTStatusPeriod.RTTStatusCode = Inpatient.RTTPeriodStatusCode

inner join dbo.RTTStatus RTTStatusCurrent
on	RTTStatusCurrent.RTTStatusCode = Inpatient.RTTCurrentStatusCode

left join dbo.OlapSpecialty Specialty
on	Specialty.SpecialtyCode = Inpatient.SpecialtyCode

left join dbo.OlapConsultant Consultant
on	Consultant.ConsultantCode = Inpatient.ConsultantCode

inner join dbo.ParamParserFn(@SiteCode) SiteFilter
on	SiteFilter.ItemCode = Inpatient.EndSiteCode

inner join dbo.ParamParserFn(@SpecialtyCode) SpecialtyFilter
on	SpecialtyFilter.ItemCode = Inpatient.SpecialtyCode

inner join dbo.ParamParserFn(@ConsultantCode) ConsultantFilter
on	ConsultantFilter.ItemCode = Inpatient.ConsultantCode

inner join dbo.ParamParserFn(@PCTCode) PCTFilter
on	PCTFilter.ItemCode = Inpatient.PCTCode

where
	RTTStatusPeriod.ClockStopFlag = 1
and	RTTStatusCurrent.ClockStopFlag != 1
and	Inpatient.EncounterStartDate between @from and @to
--and	Inpatient.RTTCurrentStatusDate >= EncounterEndDate

order by
	Inpatient.EncounterStartDate


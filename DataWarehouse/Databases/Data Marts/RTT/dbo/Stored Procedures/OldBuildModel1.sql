﻿CREATE  procedure [dbo].[OldBuildModel1]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@execute int = 0
as

-- this builds the reporting model for a given date range.
-- a snapshot is generated for the given @toDate.

declare @dayOfWeek varchar(50)
declare @dayOfMonth int
declare @censusAdjustment int

select @dayOfWeek = datename(dw, getdate())
select @dayOfMonth = datepart(day, getdate())

--copy over manually entered data from RTT on NM-MALECITE - remove this when old RTT is turned off
--exec MigrateManualData


if @dayOfMonth = 1 or @dayOfWeek = 'Sunday' or @execute = 1
begin
	select @censusAdjustment = case when @dayOfMonth = 1 then -1 else 0 end

	select @fromDate = coalesce(@fromDate, '1 jan 2004')
	select @toDate = coalesce(@toDate, dateadd(day, datediff(day, 0, dateadd(day, @censusAdjustment, getdate())), 0))

	-- Clear down old snapshot data
	delete from dbo.MatchMaster
	where
		CensusDate < dateadd(day, (select NumericValue*-7 from Parameter where Parameter = 'KEEPSNAPSHOTWEEKS'), getdate())
	and	CensusDate not in (select max(CensusDate) from MatchMaster group by datepart(year, CensusDate), datepart(month, CensusDate))

	delete from dbo.RTTFact
	where
		CensusDate < dateadd(day, (select NumericValue*-7 from Parameter where Parameter = 'KEEPSNAPSHOTWEEKS'), getdate())
	and	CensusDate not in (select max(CensusDate) from RTTFact group by datepart(year, CensusDate), datepart(month, CensusDate))


	-- Build Match
	exec BuildMatch @fromDate, @toDate

end

if @dayOfMonth = 1 or @dayOfWeek = 'Sunday' or @dayOfWeek = 'Wednesday' or @execute = 1
begin

--Wednesday is the weekly returns day so regenerate the fact table for the latest census to pick up any manual adjustments
	if @dayOfWeek = 'Wednesday'
		select @toDate = (select max(CensusDate) from dbo.MatchMaster)

	-- Build the fact table
	exec BuildRTTFact @toDate

	--Build the drillthrough table
	exec BuildOlapRTTDrillThroughBase

end


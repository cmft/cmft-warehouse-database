﻿CREATE procedure [dbo].[RebuildRTT] (
	@CensusDate smalldatetime
)
as

exec dbo.BuildModel '1 jan 2004', @CensusDate, 0

	---- Batch Closure of non-admitted backlog pathways with RTT End Date
	--exec BatchCloseBacklog @CensusDate

	---- Rebuild the fact table to incorporate Batch Closures
	--exec BuildRTTFact @CensusDate

	---- Rebuild the drillthrough table to incorporate Batch Closures
	--exec BuildOlapRTTDrillThroughBase @CensusDate

	---- Make Code 21s Non-Reportable
	--exec BatchMake21NonReportable @CensusDate


﻿CREATE procedure [dbo].[BatchMakeWDSNonReportable] (
	@CensusDate smalldatetime
)
as

update RTTFact

set
	 ReportableFlag = 'N - RefSrcWDS' -- 20140423 RR added description
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and	Drill.SourceOfReferralCode = 'WDS'
and ReportableFlag='Y'


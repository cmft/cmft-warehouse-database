﻿Create Procedure [dbo].[BatchMakeNullPathwayIDsNonReportable]  (
	@CensusDate smalldatetime
)
as

update RTTFact
set	 ReportableFlag = 'N - NullPathwayID'
from RTTFact Fact inner join OlapRTTDrillThroughBase Drill
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where Fact.censusdate = @censusDate
and RTTPathwayID is null
and ReportableFlag='Y'
--25228 total rows
--4214 have null pathwayIDs






﻿CREATE   PROCEDURE [dbo].[BuildWkReferral] @fromDate smalldatetime, @toDate smalldatetime as

truncate table WkReferral

insert into WkReferral
(
	EncounterRecno,
	SourcePatientNo,
	SourceEncounterNo2,
	HospitalCode,
	ConsultantCode,
	SpecialtyCode,
	ReferralDate,
	RTTPathwayID
)
select
	Referral.EncounterRecno,
	Referral.SourcePatientNo,
	Referral.SourceEncounterNo2,
	Referral.HospitalCode,
	Referral.ConsultantCode,
	Referral.SpecialtyCode,
	Referral.ReferralDate,
	Referral.RTTPathwayID
from
	Referral
where
	Referral.ReferralDate between @fromDate and @toDate





SET ANSI_NULLS ON


﻿Create Procedure [dbo].[RR_RTTCheck_WLRem5NonReportable] As



-- Update to non reportable where appointment type = 5 in the OPWLEntry
-- 1) where all otherevents are null
update RTTFact
set	 ReportableFlag = 'N'
where EncounterRecNo in
(select distinct ReferralRecNo
from matchmaster inner join Referral 
on ReferralRecNo = EncounterRecNo
inner join [warehousesql].PAS.Inquire.OPWLRemEntry OPWL
on InternalPatientNumber = SourcePatientNo and EpisodeNumber = SourceEncounterNo
where SpecialtyCode = 'Gyn' 
and AppointmentType = '5'
and OutpatientRecNo is null and InpatientWLRecNo is null and InpatientRecNo is null and FutureOPRecNo is null)
--25 rows

--2) where Outpatient is not null and removal is after the OP and Date on List is before OP
update RTTFact
set	 ReportableFlag = 'N'
where EncounterRecNo in
(select distinct B.ReferralRecNo
from Outpatient
inner join 
	(select distinct ReferralRecNo,OutpatientRecNo, AppointmentType, DateonList,OPWLRemDateTimeExt
	from matchmaster inner join Referral 
	on ReferralRecNo = EncounterRecNo
	inner join [warehousesql].PAS.Inquire.OPWLRemEntry OPWL
	on InternalPatientNumber = SourcePatientNo and EpisodeNumber = SourceEncounterNo
	where SpecialtyCode = 'Gyn' 
	and AppointmentType = '5'
	and OutpatientRecNo is not null and InpatientWLRecNo is null and InpatientRecNo is null and FutureOPRecNo is null
	) B
on EncounterRecNo = OutpatientRecNo
where cast((right(DateonList,4)+substring(DateonList,4,2)+left(DateonList,2))as datetime)<= AppointmentDate
and cast((substring(OPWLRemDateTimeExt,7,4)+substring(OPWLRemDateTimeExt,4,2)+left(OPWLRemDateTimeExt,2))as datetime)>=AppointmentDate)
--12 rows

--3) where Outpatient is not null and removal date on list is after the OP
update RTTFact
set	 ReportableFlag = 'N'
where EncounterRecNo in
(select distinct B.ReferralRecNo
from Outpatient
inner join 
	(select distinct ReferralRecNo,OutpatientRecNo, AppointmentType, DateonList,OPWLRemDateTimeExt
	from matchmaster inner join Referral 
	on ReferralRecNo = EncounterRecNo
	inner join [warehousesql].PAS.Inquire.OPWLRemEntry OPWL
	on InternalPatientNumber = SourcePatientNo and EpisodeNumber = SourceEncounterNo
	where SpecialtyCode = 'Gyn' 
	and AppointmentType = '5'
	and OutpatientRecNo is not null and InpatientWLRecNo is null and InpatientRecNo is null and FutureOPRecNo is null
	) B
on EncounterRecNo = OutpatientRecNo
where cast((right(DateonList,4)+substring(DateonList,4,2)+left(DateonList,2))as datetime)>= AppointmentDate)
--3 rows




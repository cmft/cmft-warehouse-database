﻿CREATE  procedure [dbo].[BuildSnapshots]
	 @SnapshotDate smalldatetime = null
as

--copy contents of various datasets to their associated snapshot tables

delete from dbo.InpatientSnapshot
where
	SnapshotDate = @SnapshotDate

-- 20140508 RR alter table InpatientSnapshot add waitinglistcode nvarchar(50)

insert into dbo.InpatientSnapshot
select
	@SnapshotDate
	,*
from
	Inpatient


delete from dbo.OutpatientSnapshot
where
	SnapshotDate = @SnapshotDate

insert into dbo.OutpatientSnapshot
select
	@SnapshotDate
	,*
from
	Outpatient


delete from dbo.ReferralSnapshot
where
	SnapshotDate = @SnapshotDate

insert into dbo.ReferralSnapshot
select
	@SnapshotDate
	,*
from
	Referral


delete from dbo.InpatientWLSnapshot
where
	SnapshotDate = @SnapshotDate
and	CensusDate = 
		(
		select top 1
			CensusDate
		from
			InpatientWL
		)

insert into dbo.InpatientWLSnapshot
select
	@SnapshotDate
	,*
from
	InpatientWL



delete from dbo.OutpatientWLSnapshot
where
	SnapshotDate = @SnapshotDate
and	CensusDate = 
		(
		select top 1
			CensusDate
		from
			OutpatientWL
		)

insert into dbo.OutpatientWLSnapshot
select
	@SnapshotDate
	,*
from
	OutpatientWL


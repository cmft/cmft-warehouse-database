﻿CREATE procedure [dbo].[BatchCloseBacklog] 
(
	@CensusDate smalldatetime
)
as


-- This procedure emulates the behaviour of a user entering manual override
-- clock-stops to pathways appearing as non-admitted breaches, who also have
-- RTT End Dates.  The net result is that the RTT Model will treat these as
-- completed pathways, thus removing them from the 'backlog'.

-- 2010-10-01 CCB
-- Designed under instruction from Helen Houghton, with assistance from Nikki Waddie
-- on the National Intensive Support Team.

-- 2011-03-10 CCB
-- Additional filtering added to remove:
--	Pathways where ReferralSource in (SAE , AE , DAE) and PathwayID is null


-- update existing RTTOPEncounter records to show that the pathways have been touched again
update RTTOPEncounter
set
	-- SourcePatientNo = Drill.SourcePatientNo
	--,SourceEntityRecno = Drill.SourceEncounterNo2
	 ReviewedFlag = 1
	,Updated = getdate()
	,LastUser = 'BATCH BACKLOG REMOVAL'
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and	exists
		(
		select
			1
		from
			dbo.RTTOPEncounter
		where
			RTTOPEncounter.SourcePatientNo = Drill.SourcePatientNo
		and	RTTOPEncounter.SourceEntityRecno = Drill.SourceEncounterNo2
		)

and	(
		(
			Fact.PathwayStatusCode in ( 'AT' , 'OPW' )
		and	ReportableFlag = 'Y'
		and	WorkflowStatusCode in ( 'O' , 'P' )
		and	BreachBandCode in ( 'BR01' , 'BR02' )
		and	Drill.RTTEndDate <= @CensusDate
		)
	or	(
			Drill.RTTPathwayID is null
		and	Drill.SourceOfReferralCode in ('SAE' , 'AE' , 'DAE')
		and	ReportableFlag = 'Y'
		)
	)



-- create RTTOPEncounter records to show that the pathways have been touched
insert into dbo.RTTOPEncounter
	(
	 SourcePatientNo
	,SourceEntityRecno
	,ReviewedFlag
	,Created
	,LastUser
	)

select distinct
	 Drill.SourcePatientNo
	,Drill.SourceEncounterNo2
	,1
	,getdate()
	,'BATCH BACKLOG REMOVAL'
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and	not exists
		(
		select
			1
		from
			dbo.RTTOPEncounter
		where
			RTTOPEncounter.SourcePatientNo = Drill.SourcePatientNo
		and	RTTOPEncounter.SourceEntityRecno = Drill.SourceEncounterNo2
		)

and	(
		(
			Fact.PathwayStatusCode in ( 'AT' , 'OPW' )
		and	ReportableFlag = 'Y'
		and	WorkflowStatusCode in ( 'O' , 'P' )
		and	BreachBandCode in ( 'BR01' , 'BR02' )
		and	Drill.RTTEndDate <= @CensusDate
		)
	or	(
			Drill.RTTPathwayID is null
		and	Drill.SourceOfReferralCode in ('SAE' , 'AE' , 'DAE')
		and	ReportableFlag = 'Y'
		)
	)


-- create clock stop records
insert into dbo.RTTOPClockStop
(
	 SourcePatientNo
	,SourceEntityRecno
	,ClockStopDate
	,ClockStopReasonCode
	,ClockStopComment
	,Created
	,LastUser
	,ClockStopReportingDate
	,RTTUserModeCode
)

	
select distinct
	 Drill.SourcePatientNo
	,Drill.SourceEncounterNo2
	,Drill.RTTEndDate
	,'SYS'	--System close
	,'Batch closure of Non-Admitted Breach Pathways with RTT End Date (Backlog Removal)'
	,getdate()
	,system_user
	,Drill.RTTEndDate
	,'B'
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and	not exists
		(
		select
			1
		from
			dbo.RTTOPClockStop
		where
			RTTOPClockStop.SourcePatientNo = Drill.SourcePatientNo
		and	RTTOPClockStop.SourceEntityRecno = Drill.SourceEncounterNo2
		and	RTTOPClockStop.ClockStopDate is not null
		)

and	Fact.PathwayStatusCode in ( 'AT' , 'OPW' )
and	ReportableFlag = 'Y'
and	WorkflowStatusCode in ( 'O' , 'P' )
and	BreachBandCode in ( 'BR01' , 'BR02' )
and	Drill.RTTEndDate <= @CensusDate

union

select distinct
	 Drill.SourcePatientNo
	,Drill.SourceEncounterNo2
	,Drill.RTTEndDate
	,'NOP'	-- Not On Pathway
	,'Batch removal of Pathways where ReferralSource in (SAE , AE , DAE) and PathwayID is null'
	,getdate()
	,system_user
	,Drill.RTTEndDate
	,'B'
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and	not exists
		(
		select
			1
		from
			dbo.RTTOPClockStop
		where
			RTTOPClockStop.SourcePatientNo = Drill.SourcePatientNo
		and	RTTOPClockStop.SourceEntityRecno = Drill.SourceEncounterNo2
		and	RTTOPClockStop.ClockStopDate is not null
		)

and	Drill.RTTPathwayID is null
and	Drill.SourceOfReferralCode in ('SAE' , 'AE' , 'DAE')
and	ReportableFlag = 'Y'


﻿CREATE procedure [dbo].[LoadData] @fromDate smalldatetime = null, @toDate smalldatetime = null as

declare @dayOfWeek varchar(50)
declare @dayOfMonth int
declare @censusAdjustment int

select @dayOfWeek = datename(dw, getdate())
select @dayOfMonth = datepart(day, getdate())

if @dayOfMonth = 1 or @dayOfWeek = 'Sunday'
begin

	--select @fromDate = coalesce(@fromDate, dateadd(day, datediff(day, 0, dateadd(month, -1, getdate())), 0))

	select @censusAdjustment = case when @dayOfMonth = 1 then -1 else 0 end

	select @toDate = coalesce(@toDate, dateadd(day, datediff(day, 0, dateadd(day, @censusAdjustment, getdate())), 0))

	--exec LoadInpatient @fromDate, @toDate

	--exec LoadOutpatient @fromDate, @toDate

	--exec LoadReferral @fromDate, @toDate

	-- removed 2010-04-27: this dataset was previously used in the matching process but is no longer required
	-- exec LoadOPWaiter @toDate

end


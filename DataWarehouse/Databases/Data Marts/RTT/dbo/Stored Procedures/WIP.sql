﻿CREATE procedure [dbo].[WIP] as

select
	 CensusDate
	,EncounterRecno
	,ConsultantCode
	,SpecialtyCode
	,PracticeCode

	,PCTCode =
		coalesce(
			Practice.ParentOrganisationCode,
			PostcodePCTMap.PCTCode,
			'5NT'
		)

	,SiteCode
	,PathwayStatusCode

	,ClockStartDate = 
		dateadd(
			 day
			,RTT.AdjustmentDays
			,RTT.UnadjustedClockStartDate
		)

	,WorkflowStatusCode
	,KeyDate
	,Cases
	,TCIBeyondBreach

	,LastWeeksCases =
		case
		when
			datediff(
				 day
				,dateadd(
					 day
					,-1 -- take a day off 
					,RTT.KeyDate
				)
				,RTT.CensusDate
			) < 8
		then 1
		else 0
		end 

	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,ReportableFlag
	,UnadjustedClockStartDate
	,UnadjustedTCIBeyondBreach

	,RTTBreachDate =
		dateadd(
			 day
			,coalesce(RTTBreach.BreachValue, 126)

			,dateadd(
				 day
				,RTT.AdjustmentDays
				,RTT.UnadjustedClockStartDate
			)
		)

	,ReferringProviderCode
	,ClockChangeReasonCode
	,RTTPathwayID
	,ClockStopDate
	,RTTFactTypeCode

from
	(
	------------------------------------------------------------------------------------------------------------------------------------------------
	--Fully matched referrals
	------------------------------------------------------------------------------------------------------------------------------------------------

	select
		 CensusDate = MatchMaster.CensusDate
		,EncounterRecno = Referral.EncounterRecno
		,ConsultantCode = Referral.ConsultantCode
		,SpecialtyCode = Referral.SpecialtyCode

		,PracticeCode =
			coalesce(
				 Referral.EpisodicGpPracticeCode
				,Referral.RegisteredGpPracticeCode
				,'##'
			)

		,SiteCode =
			coalesce(
				 Inpatient.HospitalCode
				,Referral.HospitalCode
			)

		,Postcode = Referral.Postcode

		,PathwayStatusCode =
			case

			when
				RTTOPClockStop.RTTClockStopRecno is not null
			then 'OPT' -- Outpatient Treatment

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or	Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'AT' -- Awaiting Treatment

			when
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			then 'ACS' -- Admission Clock Stop

			when
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			then 'ACS' -- Admission Clock Stop

			when
				Outpatient.EncounterRecno is not null
			then 'OPT' -- Outpatient Treatment

			when
				MatchMaster.InpatientRecno is not null
			and	DirectorateActivated.ActiveDate is not null
			then 'ADM' -- Inpatient Admission

			when Referral.DischargeDate is not null then 'OPD' -- Outpatient Discharge

			when
				OutpatientWL.EncounterRecno is not null
			then 'OPW' -- O/P Waiter


			--22 week waiters go into Outpatient Treatment (Backlog)
			--	when datediff(day, Referral.ReferralDate, MatchMaster.CensusDate) > 154 then 'OPB'

			else 'AT' -- Awaiting Treatment

			end

		,AdjustmentDays = coalesce(SocialSuspensionIP.DaysSuspended, RTTAdjustmentIP.AdjustmentDays, 0)


		,WorkflowStatusCode =
			case

			when RTTOPClockStop.RTTClockStopRecno is not null then 'C'

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'P' -- Pending

			when
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			then 'C' -- Admission Clock Stop

			when
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			then 'C' -- Admission Clock Stop

			when
				Outpatient.EncounterRecno is not null
			then 'C' -- Outpatient Clock Stop

			when
				MatchMaster.InpatientRecno is not null
			and DirectorateActivated.ActiveDate is not null
			then 'C' -- Inpatient Admission

			when Referral.DischargeDate is not null
			then 'C' -- Outpatient Discharge

			when
				OutpatientWL.EncounterRecno is not null
			then 'O' -- O/P Waiter

			else 'P' -- Pending

			end


		,KeyDate =
			case

			--18 March 2009
			--if the clock stop has been changed then use the date of the change so we count the close in the current period
			when RTTOPClockStop.RTTClockStopRecno is not null then 
				case
				--when BacklogUser.EntityCode is not null --these users maintain backlog
				when RTTOPClockStop.RTTUserModeCode = 'B' --this is a backlog record
				then RTTOPClockStop.ClockStopDate
				else dateadd(day, datediff(day, 0, coalesce(RTTOPClockStop.ClockStopReportingDate, RTTOPClockStop.ClockStopDate)), 0)
				end

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then MatchMaster.CensusDate

			when
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

			when
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			then
				case
				when
					DirectorateActivated.ActiveDate is null
				then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop
				else Inpatient.AdmissionDate -- Admission Clock Stop
				end

			when
				Outpatient.EncounterRecno is not null
			then coalesce(Outpatient.RTTEndDate, Outpatient.AppointmentDate) -- Non-Admitted Clock Stop

			when
				MatchMaster.InpatientRecno is not null
			and DirectorateActivated.ActiveDate is not null
			then Inpatient.AdmissionDate -- Inpatient Admission

			when Referral.DischargeDate is not null
			then Referral.DischargeDate -- Outpatient Discharge

			else MatchMaster.CensusDate

			end

		,Cases = 1

		,TCIBeyondBreach = 0

		,PrimaryDiagnosisCode = coalesce(Inpatient.PrimaryDiagnosisCode , 'N/A')

		,PrimaryOperationCode = coalesce(Inpatient.PrimaryOperationCode , 'N/A')


		,ReportableFlag =
			case

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'Y' -- Pending

			when
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			then 'Y' -- Admission Clock Stop

			when
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			then 'Y' -- Admission Clock Stop

			when
				Outpatient.EncounterRecno is not null
			then 'Y' -- Non-Admitted Clock Stop

			when
				MatchMaster.InpatientRecno is not null
			and DirectorateActivated.ActiveDate is not null
			then 'Y' -- Inpatient Admission

			when OutpatientWL.ReferralDate < '1 Jan 2007' then 'N' -- O/P Waiter
			when
				OutpatientWL.EncounterRecno is not null
			then 'Y' -- O/P Waiter

			when Referral.DischargeDate is not null
			then 'Y' -- Outpatient Discharge

			when
				Referral.ReferralDate < '1 Jan 2007' then 'N'
			else 'Y' -- Pending
			end

		,UnadjustedClockStartDate = 
			coalesce(

	--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
				case
				when RTTClockStartIP.ClockStartReasonCode = 'OVR'
				then RTTClockStartIP.ClockStartDate
				else null
				end

				,case
				when DirectorateActivated.ActiveDate is null
				then Inpatient.RTTStartDate
				end


				--manually entered starts
				,RTTClockStartIP.ClockStartDate
				,RTTClockStartOP.ClockStartDate

				,case
				when DirectorateActivated.ActiveDate is not null
				then Referral.ReferralDate
				end

				--otherwise get the earliest date
				,(
				select
					min(StartDate)
				from
					(
					select Inpatient.RTTStartDate StartDate union all
					select OutpatientWL.RTTStartDate union all
					select Outpatient.RTTStartDate union all
					select Referral.RTTStartDate union all
					select Referral.ReferralDate
					) Dates
				)
			)

		,UnadjustedTCIBeyondBreach = 0

		,ReferringProviderCode = left(Referral.RTTPathwayID, 3)

		,ClockChangeReasonCode = 
			coalesce(
				 RTTClockStartIP.ClockStartReasonCode
				,RTTClockStartOP.ClockStartReasonCode
				,RTTClockStartOP.ClockStopReasonCode
				,RTTOPClockStopAll.ClockStartReasonCode
				,RTTOPClockStopAll.ClockStopReasonCode
			)

		,RTTPathwayID = Referral.RTTPathwayID

		,ClockStopDate =
			case

			when RTTOPClockStop.RTTClockStopRecno is not null then RTTOPClockStop.ClockStopDate

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then MatchMaster.CensusDate -- Pending

			when
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

			when
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			then
				case
				when
					DirectorateActivated.ActiveDate is null
				then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop
				else Inpatient.AdmissionDate -- Admission Clock Stop
				end

			when
				Outpatient.EncounterRecno is not null
			then coalesce(Outpatient.RTTEndDate, Outpatient.AppointmentDate) -- Non-Admitted Clock Stop

			when
				MatchMaster.InpatientRecno is not null
			and	DirectorateActivated.ActiveDate is not null
			then Inpatient.AdmissionDate -- Inpatient Admission

			when Referral.DischargeDate is not null
			then Referral.DischargeDate -- Outpatient Discharge

			else MatchMaster.CensusDate

			end

		,RTTFactTypeCode = 1

	from
		  dbo.Referral with (nolock)

	inner join dbo.MatchMaster with (nolock)
	on	MatchMaster.ReferralRecno = Referral.EncounterRecno
	and	MatchMaster.CensusDate =
		(
		select
			CensusDate = DateValue
		from
			dbo.Parameter
		where
			 Parameter = 'RTTCENSUSDATE'
		)

	left join dbo.OutpatientWL with (nolock)
	on    OutpatientWL.EncounterRecno = MatchMaster.FutureOPRecno

	left join dbo.Outpatient with (nolock)
	on  Outpatient.EncounterRecno = MatchMaster.OutpatientRecno

	--use OP Disposal Codes to determine OP Clock Stops
	and	exists
		(
		select
			1
		from
			OPDisposal
		where
			OPDisposal.OPDisposalCode = Outpatient.DisposalCode
		and	OPDisposal.ClockStopFlag = 1
		)

	left join dbo.Inpatient with (nolock)
	on    Inpatient.EncounterRecno = MatchMaster.InpatientRecno

	--if the following join succeeds then the inpatient activity fell before the active date.
	--this causes the old (non-RTT pathway), crude derivation of clock stops etc to be used.

	left join dbo.EntityActivatedDate DirectorateActivated
	on	DirectorateActivated.EntityTypeCode = 'DIRECTORATE'
	and	DirectorateActivated.EntityCode = Inpatient.StartDirectorateCode
	and	DirectorateActivated.ActiveDate < Inpatient.AdmissionDate

	--the period status determines whether or not a clock stopped for THIS admission, the current status could happen at any time for any event
	--the Current Status should always be the most up to date Status

	left join dbo.RTTStatus InpatientRTTStatus with (nolock)
	on	InpatientRTTStatus.RTTStatusCode = Inpatient.RTTCurrentStatusCode
	and DirectorateActivated.ActiveDate is null

	left join dbo.RTTClockStart RTTClockStartIP with (nolock)
	on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
	and	not exists
		  (
		  select
				1
		  from
				dbo.RTTClockStart with (nolock)
		  where
				RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
		  and   RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
		  and   (
					  RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
				or
					  (
							RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
					  and   RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
					  )
				)
		  )

	left join 
		(
		select
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
			,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
		from
			dbo.RTTAdjustment with (nolock)
		group by
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
		) RTTAdjustmentIP
	on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

	left join dbo.SocialSuspensionIP SocialSuspensionIP with (nolock)
	on	SocialSuspensionIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	SocialSuspensionIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

	left join dbo.RTTOPClockStop with (nolock)
	on	RTTOPClockStop.SourcePatientNo = Referral.SourcePatientNo
	and	RTTOPClockStop.SourceEntityRecno = Referral.SourceEncounterNo2
	and	RTTOPClockStop.ClockStopDate is not null
	and	not exists
		(
		select
			1
		from
			dbo.RTTOPClockStop Previous
		where
			Previous.SourcePatientNo = Referral.SourcePatientNo
		and Previous.SourceEntityRecno = Referral.SourceEncounterNo2
		and	Previous.ClockStopDate is not null
		and	Previous.RTTClockStopRecno > RTTOPClockStop.RTTClockStopRecno
		)

	left join dbo.RTTOPClockStop RTTClockStartOP with (nolock)
	on    RTTClockStartOP.SourcePatientNo = Referral.SourcePatientNo
	and   RTTClockStartOP.SourceEntityRecno = Referral.SourceEncounterNo2
	and	  RTTClockStartOP.ClockStartDate is not null
	and	not exists
		(
		select
			1
		from
			dbo.RTTOPClockStop Previous
		where
			Previous.SourcePatientNo = Referral.SourcePatientNo
		and Previous.SourceEntityRecno = Referral.SourceEncounterNo2
		and	Previous.ClockStopDate is not null
		and	Previous.RTTClockStopRecno > RTTClockStartOP.RTTClockStopRecno
		)

	left join dbo.RTTOPClockStop RTTOPClockStopAll with (nolock)
	on    RTTOPClockStopAll.SourcePatientNo = Referral.SourcePatientNo
	and   RTTOPClockStopAll.SourceEntityRecno = Referral.SourceEncounterNo2
	and	not exists
		(
		select
			1
		from
			dbo.RTTOPClockStop Previous
		where
			Previous.SourcePatientNo = Referral.SourcePatientNo
		and Previous.SourceEntityRecno = Referral.SourceEncounterNo2
		and	Previous.ClockStopDate is not null
		and	Previous.RTTClockStopRecno > RTTOPClockStopAll.RTTClockStopRecno
		)

	left join dbo.DiagnosticProcedure with (nolock)
	on	DiagnosticProcedure.ProcedureCode =
			coalesce(
				 Inpatient.PrimaryOperationCode
				,Inpatient.IntendedPrimaryOperationCode
			) collate database_default

	where

	--20090924 if this referral has no associated IP WL entry or there is a WL entry AND an inpatient entry
	--then include here as IP WLs are handled in the IP WL insert later in this script
		(
			MatchMaster.InpatientWLRecno is null
		or	(
				MatchMaster.InpatientWLRecno is not null
			and	MatchMaster.InpatientRecno is not null
			)
		)

	------------------------------------------------------------------------------------------------------------------------------------------------
	union all -- Patients admitted for which we have no referral link but DO have a manually entered clock start date but DON'T have a RTT Pathway
	------------------------------------------------------------------------------------------------------------------------------------------------



	-- RTTFactTypeCode = 2


	select
		 CensusDate = Census.CensusDate
		,EncounterRecno = Inpatient.EncounterRecno
		,ConsultantCode = Inpatient.ConsultantCode
		,SpecialtyCode = Inpatient.SpecialtyCode

		,PracticeCode =
			coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode, '##')

		,SiteCode =
			coalesce(Inpatient.HospitalCode, '##')

		,Inpatient.Postcode

		,PathwayStatusCode =
			case

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'AT' -- Awaiting Treatment

			when
				(
					DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
				and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'ACS' -- Admission Clock Stop

			else 'ADM' 
			end

		,AdjustmentDays =
			coalesce(RTTAdjustmentIP.AdjustmentDays, 0)

		,WorkflowStatusCode =
			case

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'P' -- Pending

			else 'C'

			end

		,KeyDate = Inpatient.AdmissionDate

		,Cases = 1

		,TCIBeyondBreach = 0

		,PrimaryDiagnosisCode =
			coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A')

		,PrimaryOperationCode =
			coalesce(Inpatient.PrimaryOperationCode, 'N/A')

		,ReportableFlag = 'Y'

		,UnadjustedClockStartDate = RTTClockStartIP.ClockStartDate

		,UnadjustedTCIBeyondBreach = 0

		,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

		,ClockChangeReasonCode = RTTClockStartIP.ClockStartReasonCode

		,RTTPathwayID = Inpatient.RTTPathwayID

		,ClockStopDate = Inpatient.AdmissionDate

		,RTTFactTypeCode = 2
	from
		  Inpatient with (nolock)

	inner join dbo.RTTClockStart RTTClockStartIP with (nolock)
	on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
	and	not exists
		(
		select
			1
		from
			dbo.RTTClockStart with (nolock)
		where
			RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
		and RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
		and (
				RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
			or	(
					RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
				and   RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
				)
			)
		)

	left join 
		(
		select
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
			,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
		from
			dbo.RTTAdjustment with (nolock)
		group by
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
		) RTTAdjustmentIP
	on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
	and RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

	left join dbo.DiagnosticProcedure with (nolock)
	on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode collate database_default

	cross join
		(
		select
			CensusDate = DateValue
		from
			dbo.Parameter
		where
			 Parameter = 'RTTCENSUSDATE'
		) Census

	where
		not exists
		(
		select
			1
		from
			dbo.MatchMaster with (nolock)
		where
			MatchMaster.InpatientRecno = Inpatient.EncounterRecno
		and MatchMaster.CensusDate = Census.CensusDate
		)

	and	Inpatient.RTTStartDate is null


	------------------------------------------------------------------------------------------------------------------------------------------------
	union all -- Patients admitted for which we have no referral link but DO have a RTT Pathway
	------------------------------------------------------------------------------------------------------------------------------------------------

	select
		 CensusDate = Census.CensusDate
		,EncounterRecno = Inpatient.EncounterRecno
		,ConsultantCode = Inpatient.ConsultantCode
		,SpecialtyCode = Inpatient.SpecialtyCode

		,PracticeCode =
			coalesce(
				 Inpatient.EpisodicGpPracticeCode
				,Inpatient.RegisteredGpPracticeCode
				,'##'
			)

		,SiteCode =
			coalesce(
				 Inpatient.HospitalCode
				,'##'
			)

		,Inpatient.Postcode

		,PathwayStatusCode =
			case

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'AT' -- Awaiting Treatment

			when
				(
					DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
				and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'ACS' -- Admission Clock Stop

			else 'ADM' 
			end

		,AdjustmentDays =
			coalesce(RTTAdjustmentIP.AdjustmentDays, 0)

		,WorkflowStatusCode =
			case

			when
				DiagnosticProcedure.ProcedureCode is not null
			and	(
					DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
				or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
				)
			then 'P' -- Pending

			else 'C'

			end

		,KeyDate = Inpatient.AdmissionDate

		,Cases = 1

		,TCIBeyondBreach = 0

		,PrimaryDiagnosisCode =
			coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A')

		,PrimaryOperationCode =
			coalesce(Inpatient.PrimaryOperationCode, 'N/A')

		,ReportableFlag = 'Y'

		,UnadjustedClockStartDate = 
			coalesce(
				 RTTClockStartIP.ClockStartDate
				,Inpatient.RTTStartDate
			)

		,UnadjustedTCIBeyondBreach = 0

		,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

		,ClockChangeReasonCode = RTTClockStartIP.ClockStartReasonCode

		,Inpatient.RTTPathwayID

		,ClockStopDate = Inpatient.AdmissionDate

		,RTTFactTypeCode = 3
	from
		  Inpatient with (nolock)

	cross join
		(
		select
			CensusDate = DateValue
		from
			dbo.Parameter
		where
			 Parameter = 'RTTCENSUSDATE'
		) Census

	left join dbo.DiagnosticProcedure with (nolock)
	on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode collate database_default

	left join dbo.RTTClockStart RTTClockStartIP with (nolock)
	on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
	and	not exists
		(
		select
			1
		from
			dbo.RTTClockStart with (nolock)
		where
			RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
		and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
		and	(
				RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
			or	(
					RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
				and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
				)
			)
		)

	left join 
		(
		select
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
			,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
		from
			dbo.RTTAdjustment with (nolock)
		group by
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
		) RTTAdjustmentIP
	on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2


	where
		not exists
		(
		select
			1
		from
			dbo.MatchMaster with (nolock)
		where
			MatchMaster.InpatientRecno = Inpatient.EncounterRecno
		and MatchMaster.CensusDate = Census.CensusDate
		)

	and	Inpatient.RTTStartDate is not null

	--only want records from date where RTT coding is reliable
	and	not exists
		(
		select
			1
		from
			dbo.EntityActivatedDate DirectorateActivated
		where
			DirectorateActivated.EntityTypeCode = 'DIRECTORATE'
		and	DirectorateActivated.EntityCode = Inpatient.StartDirectorateCode
		and	DirectorateActivated.ActiveDate < Inpatient.AdmissionDate
		)


	------------------------------------------------------------------------------------------------------------------------------------------------
	union all -- IP waiting list
	------------------------------------------------------------------------------------------------------------------------------------------------

	select
		 CensusDate = Census.CensusDate
		,EncounterRecno = InpatientWL.EncounterRecno -- this will avoid potential duplicates
		,ConsultantCode = InpatientWL.ConsultantCode
		,SpecialtyCode = InpatientWL.SpecialtyCode

		,PracticeCode =
			coalesce(
				 InpatientWL.EpisodicGpPracticeCode
				,InpatientWL.RegisteredPracticeCode
				,'##'
			)

		,SiteCode = 
			coalesce(
				 InpatientWL.SiteCode
				,'##'
			)

		,InpatientWL.Postcode

		,PathwayStatusCode = 'IPW'

		,AdjustmentDays =
			case
			when InpatientWL.WLStatus = 'WL Suspend'
			then 
				SocialSuspension.DaysSuspended -

				datediff(
					 day
					,InpatientWL.CensusDate
					,Suspension.SuspensionEndDate
				)

				else
					coalesce(InpatientWL.SocialSuspensionDays, 0)
				end

		,WorkflowStatusCode = 'I'

		,KeyDate = InpatientWL.CensusDate

		,Cases = 1

		,TCIBeyondBreach =
			case  
			when InpatientWL.TCIDate is null then 1
			when InpatientWL.TCIDate >
					dateadd(
						 day
						,coalesce(RTTBreach.BreachValue, 126)
						,dateadd(
							 day
							,coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
							,RTTClockStartIP.ClockStartDate
						)
					) then 1
			else 0
			end 

		,PrimaryDiagnosisCode = 'N/A' 

		,PrimaryOperationCode = coalesce(InpatientWL.IntendedPrimaryOperationCode, 'N/A') 

		,ReportableFlag = 'Y'

		,UnadjustedClockStartDate =
			coalesce
				(
				 RTTClockStartIP.ClockStartDate
				,InpatientWL.DerivedClockStartDate

				,case
				when DirectorateActivated.ActiveDate is null
				then InpatientWL.RTTStartDate
				end
			)

		,UnadjustedTCIBeyondBreach = 
			case  
			when InpatientWL.TCIDate is null then 1
			when InpatientWL.TCIDate > dateadd(day, coalesce(RTTBreach.BreachValue, 126), RTTClockStartIP.ClockStartDate) -- ClockStartDate
			then 1
			else 0
			end

		,ReferringProviderCode = left(InpatientWL.RTTPathwayID, 3)

		,ClockChangeReasonCode = RTTClockStartIP.ClockStartReasonCode

		,InpatientWL.RTTPathwayID

		,ClockStopDate = InpatientWL.CensusDate

		,RTTFactTypeCode = 4
	from
		InpatientWL with (nolock)

	inner join 
		(
		select
			CensusDate = DateValue
		from
			dbo.Parameter
		where
			 Parameter = 'RTTCENSUSDATE'
		) Census
	on	Census.CensusDate = InpatientWL.CensusDate

	--if the following join succeeds then the inpatient activity fell before the active date.
	--this causes the old (non-RTT pathway), crude derivation of clock stops etc to be used.

	left join dbo.EntityActivatedDate DirectorateActivated
	on	DirectorateActivated.EntityTypeCode = 'DIRECTORATE'
	and	DirectorateActivated.EntityCode = InpatientWL.DirectorateCode
	and	DirectorateActivated.ActiveDate < InpatientWL.CensusDate

	left join dbo.RTTClockStart RTTClockStartIP with (nolock)
	on	RTTClockStartIP.SourcePatientNo = InpatientWL.SourcePatientNo
	and	RTTClockStartIP.SourceEntityRecno = InpatientWL.SourceEntityRecno
	and	not exists
		(
		select
			1
		from
			dbo.RTTClockStart with (nolock)
		where
			RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
		and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
		and	(
				RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
			or	(
					RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
				and   RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
				)
			)
		)

	left join 
		(
		select
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
			,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
		from
			dbo.RTTAdjustment with (nolock)
		group by
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
		) RTTAdjustmentIP
	on	RTTAdjustmentIP.SourcePatientNo = InpatientWL.SourcePatientNo
	and	RTTAdjustmentIP.SourceEntityRecno = InpatientWL.SourceEntityRecno

	left join dbo.EntityBreach RTTBreach with (nolock)
	on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
	and	RTTBreach.EntityCode = 'RTT'
	and	InpatientWL.CensusDate between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

	--Social suspensions only
	left join dbo.SocialSuspension SocialSuspension
	on	SocialSuspension.SourcePatientNo = InpatientWL.SourcePatientNo
	and	SocialSuspension.SourceEntityRecno = InpatientWL.SourceEntityRecno
	and SocialSuspension.CensusDate = InpatientWL.CensusDate

	left join 
		(
		select
			*
		from
			dbo.Suspension Suspension
		where
			upper(Suspension.SuspensionReason) like '%S*%'
		and	not exists
			(
			select
				1
			from
				dbo.Suspension LastSuspension
			where
				LastSuspension.SourcePatientNo = Suspension.SourcePatientNo
			and	upper(LastSuspension.SuspensionReason) like '%S*%'
			and	LastSuspension.SourceEncounterNo = Suspension.SourceEncounterNo
			and	LastSuspension.CensusDate = Suspension.CensusDate
			and
				(
					LastSuspension.SuspensionStartDate > Suspension.SuspensionStartDate
				or
					(
						LastSuspension.SuspensionStartDate = Suspension.SuspensionStartDate
					and	LastSuspension.SuspensionRecno > Suspension.SuspensionRecno
					)
				)
			)
		) Suspension
	on	Suspension.SourcePatientNo = InpatientWL.SourcePatientNo
	and	Suspension.SourceEncounterNo = InpatientWL.SourceEncounterNo
	and	Suspension.CensusDate = InpatientWL.CensusDate


	------------------------------------------------------------------------------------------------------------------------------------------------
	union all -- Patients admitted that have a previous, manually entered, OP clock stop
	------------------------------------------------------------------------------------------------------------------------------------------------

	select
		 CensusDate = Census.CensusDate
		,ReferralRecno = Inpatient.EncounterRecno
		,ConsultantCode = Inpatient.ConsultantCode
		,SpecialtyCode = Inpatient.SpecialtyCode

		,PracticeCode =
			coalesce(
				 Inpatient.EpisodicGpPracticeCode
				,Inpatient.RegisteredGpPracticeCode
				,'##'
			)

		,SiteCode =
			coalesce(
				 Inpatient.HospitalCode
				,'##'
			)

		,Inpatient.Postcode

		,PathwayStatusCode = 'ACS' -- Admission Clock Stop

		,AdjustmentDays =
			coalesce(RTTAdjustmentIP.AdjustmentDays, 0)

		,WorkflowStatusCode = 'C' -- Admission Clock Stop

		,KeyDate = Inpatient.AdmissionDate

		,Cases = 1

		,TCIBeyondBreach = 0

		,PrimaryDiagnosisCode = coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A')

		,PrimaryOperationCode = coalesce(Inpatient.PrimaryOperationCode, 'N/A')

		,ReportableFlag = 'Y'

		,UnadjustedClockStartDate = 
			coalesce(
				RTTClockStartIP.ClockStartDate

				,case
				when DirectorateActivated.ActiveDate is null
				then Inpatient.RTTStartDate
				end
			)


		,UnadjustedTCIBeyondBreach = 0

		,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

		,ClockChangeReasonCode = RTTClockStartIP.ClockStartReasonCode

		,Inpatient.RTTPathwayID

		,ClockStopDate = Inpatient.AdmissionDate

		,RTTFactTypeCode = 5
	from
		Inpatient with (nolock)

	cross join
		(
		select
			CensusDate = DateValue
		from
			dbo.Parameter
		where
			 Parameter = 'RTTCENSUSDATE'
		) Census

	--if the following join succeeds then the inpatient activity fell before the active date.
	--this causes the old (non-RTT pathway), crude derivation of clock stops etc to be used.

	left join dbo.EntityActivatedDate DirectorateActivated
	on	DirectorateActivated.EntityTypeCode = 'DIRECTORATE'
	and	DirectorateActivated.EntityCode = Inpatient.StartDirectorateCode
	and	DirectorateActivated.ActiveDate < Inpatient.AdmissionDate

	left join dbo.DiagnosticProcedure with (nolock)
	on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode collate database_default

	left join dbo.RTTClockStart RTTClockStartIP with (nolock)
	on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
	and	not exists
		(
		select
			1
		from
			dbo.RTTClockStart with (nolock)
		where
			RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
		and   RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
		and (
				RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
			or	(
					RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
				and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
				)
			)
		)

	left join 
		(
		select
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
			,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
		from
			dbo.RTTAdjustment with (nolock)
		group by
			 RTTAdjustment.SourcePatientNo
			,RTTAdjustment.SourceEntityRecno
		) RTTAdjustmentIP
	on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

	where
		exists
		(
		select
			1
		from
			dbo.Outpatient

		inner join dbo.RTTOPClockStop
		on	RTTOPClockStop.SourcePatientNo = Outpatient.SourcePatientNo
		and	RTTOPClockStop.SourceEntityRecno = Outpatient.SourceEncounterNo2
		and	RTTOPClockStop.ClockStopReasonCode like '3%'

		where
			Outpatient.RTTPathwayID = Inpatient.RTTPathwayID
		and	Outpatient.AppointmentDate < Inpatient.EpisodeStartDate
		)


	) RTT


left join dbo.OlapPractice Practice with (nolock)
on    Practice.OrganisationCode = RTT.PracticeCode

left join dbo.PostcodePCTMap with (nolock)
on    PostcodePCTMap.Postcode = RTT.Postcode

inner join dbo.EntityBreach RTTBreach
on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
and	RTTBreach.EntityCode = 'RTT'
and	RTT.KeyDate between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

where
	RTT.KeyDate between '1 Jan 2007' and RTT.CensusDate

and	coalesce(RTT.ClockChangeReasonCode, '') <> 'NOP' --Not an 18 week pathway

--only load those encounters that have been waiting since month start or for the past 7 days
and	(
		datediff(day, RTT.KeyDate, RTT.CensusDate) < 8
	or	datediff(month, RTT.KeyDate, RTT.CensusDate) = 0
	)


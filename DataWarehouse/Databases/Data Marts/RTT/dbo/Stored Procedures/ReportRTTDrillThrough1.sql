﻿create procedure [dbo].[ReportRTTDrillThrough1]
	@mdx varchar(4000)
as

declare @sql varchar(4000)

set @sql = 
'
select
	 RTTPathwayID = "[$RTT Encounter].[RTT Pathway ID]"
	,SiteCode = "[$RTT].[Site Code]"
	,DistrictNo = "[$RTT Encounter].[District No]"
	,PatientSurname = "[$RTT Encounter].[Patient Surname]"
	,DateOfBirth = "[$RTT Encounter].[Date Of Birth]"
	,Consultant = "[$Consultant].[Consultant attribute]"
	,Specialty = "[$Specialty].[Specialty attribute]"
	,OperationCode = "[$PrimaryOperation].[Operation attribute]"
	,Organisation = "[$PCT].[PCT attribute]"
	,ReferralDate = "[$RTT].[Referral Date]"
	,AttendanceDate = "[$RTT Encounter].[Attendance Date]"
	,AdmissionDate = "[$RTT Encounter].[Admission Date]"
	,DurationWeek = "[$Duration].[Duration Week attribute]"
	,WorkflowStatus = "[$WorkflowStatus].[Workflow Status attribute]"
	,DateOnIPWaitingList = "[$RTT Encounter].[Date On IP Waiting List]"
	,BreachDate = "[$RTT Encounter].[Breach Date]"
	,OPProcedureCode = "[$RTT Encounter].[OP Procedure Code]"
	,KeyDate = "[$RTT].[Key Date]"
	,Quality = "[$RTT Encounter].[Quality]"
	,Cases = "[RTT].[Cases]"
	,LastWeeksCases = "[RTT].[Last Weeks Cases]"
	,EncounterRecno = "[$RTT Encounter].[Olap RTT Drill Through]"
from
	openquery(RTT, ''
' + @mdx +
'
''
)
'

print @sql

--drillthrough maxrows 10 
--
--select
--	(  
--	[Adjusted].[Yes],
--	[Reportable].[All].[Reportable]
--	) on 0
--from 
--	[RTT]
--return
--	 [$CensusDate].[CensusDate attribute]
--	,[$RTT Encounter].[Olap RTT Drill Through]
--	,[$RTT].[Consultant Code]
--	,[$RTT].[Specialty Code]
--	,[$RTT].[Practice Code]
--	,[$RTT].[PCT Code]
--	,[$RTT].[Site Code]
--	,[$RTT].[Week Band Code]
--	,[$RTT].[Pathway Status Code]
--	,[$RTT].[Referral Date]
--	,[$RTT].[Workflow Status Code]
--	,[$Measures].[Days Waiting]
--	,[$Measures].[Cases]
--	,[$Measures].[Last Weeks Cases]
--	,[$RTT].[Key Date]
--	,[$PathwayStatus].[Pathway Status attribute]
--	,key([$PathwayStatus].[National Pathway Status attribute])
--	,[$PathwayStatus].[National Pathway Status attribute]
--	,[$Specialty].[Specialty attribute]
--	,key([$Specialty].[National Specialty attribute])
--	,[$Specialty].[National Specialty attribute]
--	,[$Specialty].[National Specialty - RTTSpecialtyCode]
--	,[$Site].[Site attribute]
--	,key([$Site].[Trust attribute])
--	,[$Site].[Trust attribute]
--	,[$Consultant].[Consultant attribute]
--	,[$PCT].[PCT attribute]
--	,key([$PCT].[PCT attribute])
--	,[$Duration].[Duration Week attribute]
--	,key([$Duration].[Duration Band attribute])
--	,[$Duration].[Duration Band attribute]
--	,[$Duration].[Duration Week - Cell Colour]
--	,[$WorkflowStatus].[Workflow Status attribute]
--	,[$BreachBand].[Breach Band attribute]
--	,key([$PrimaryOperation].[Operation attribute])
--	,[$PrimaryOperation].[Operation attribute]	
--	,[$RTT Encounter].[Source Patient No]
--	,[$RTT Encounter].[Source Encounter No2]
--	,[$RTT Encounter].[Patient Forename]
--	,[$RTT Encounter].[Patient Surname]
--	,[$RTT Encounter].[Date Of Birth]
--	,[$RTT Encounter].[Date Of Death]
--	,[$RTT Encounter].[Sex Code]
--	,[$RTT Encounter].[NHS Number]
--	,[$RTT Encounter].[Post Code]
--	,[$RTT Encounter].[Address1]
--	,[$RTT Encounter].[Address2]
--	,[$RTT Encounter].[Address3]
--	,[$RTT Encounter].[Address4]
--	,[$RTT Encounter].[Country]
--	,[$RTT Encounter].[Date On Waiting List]
--	,[$RTT Encounter].[Registered Gp Code]
--	,[$RTT Encounter].[Priority Code]
--	,[$RTT Encounter].[Episodic Gp Code]
--	,[$RTT Encounter].[Casenote Number]
--	,[$RTT Encounter].[District No]
--	,[$RTT Encounter].[Outpatient Recno]
--	,[$RTT Encounter].[Outpatient Procedure Code]
--	,[$RTT Encounter].[Clinic Code]
--	,[$RTT Encounter].[Source Of Referral Code]
--	,[$RTT Encounter].[Attendance Date]
--	,[$RTT Encounter].[First Attendance Flag]
--	,[$RTT Encounter].[DNA Flag]
--	,[$RTT Encounter].[Attendance Outcome Code]
--	,[$RTT Encounter].[Disposal Code]
--	,[$RTT Encounter].[Previous DN As]
--	,[$RTT Encounter].[Patient Cancellations]
--	,[$RTT Encounter].[Previous Appointment Date]
--	,[$RTT Encounter].[Previous Appointment Status Code]
--	,[$RTT Encounter].[Next Appointment Date]
--	,[$RTT Encounter].[Last Dna Or Patient Cancelled Date]
--	,[$RTT Encounter].[Inpatient Recno]
--	,[$RTT Encounter].[Admission Date]
--	,[$RTT Encounter].[Discharge Date]
--	,[$RTT Encounter].[End Site Code]
--	,[$RTT Encounter].[Admission Method Code]
--	,[$RTT Encounter].[Patient Classification Code]
--	,[$RTT Encounter].[Patient Category Code]
--	,[$RTT Encounter].[Management Intention Code]
--	,[$RTT Encounter].[Discharge Method Code]
--	,[$RTT Encounter].[Discharge Destination Code]
--	,[$RTT Encounter].[Admin Category Code]
--	,[$RTT Encounter].[Primary Diagnosis Code]
--	,[$RTT Encounter].[Subsidiary Diagnosis Code]
--	,[$RTT Encounter].[Secondary Diagnosis Code1]
--	,[$RTT Encounter].[Secondary Diagnosis Code2]
--	,[$RTT Encounter].[Secondary Diagnosis Code3]
--	,[$RTT Encounter].[Secondary Diagnosis Code4]
--	,[$RTT Encounter].[Secondary Diagnosis Code5]
--	,[$RTT Encounter].[Primary Operation Code]
--	,[$RTT Encounter].[Primary Operation Date]
--	,[$RTT Encounter].[Count Of Days Waiting]
--	,[$RTT Encounter].[Date On IP Waiting List]
--	,[$RTT Encounter].[Date On Future OP Waiting List]
--	,[$RTT Encounter].[Breach Date]
--	,[$RTT Encounter].[Quality]
--	,[$RTT Encounter].[Dataset]
--	,[$RTT Encounter].[Template]
--	,[$RTT Encounter].[OP Procedure Code]
--	,[$RTT Encounter].[RTT Pathway ID]
--	,[$RTT Encounter].[RTT Pathway Condition]
--	,[$RTT Encounter].[RTT Start Date]
--	,[$RTT Encounter].[RTT End Date]
--	,[$RTT Encounter].[RTT Specialty Code]
--	,[$RTT Encounter].[RTT Current Provider Code]
--	,[$RTT Encounter].[RTT Current Status Code]
--	,[$RTT Encounter].[RTT Current Status Date]
--	,[$RTT Encounter].[RTT Current Private Patient Flag]
--	,[$RTT Encounter].[RTT Overseas Status Flag]


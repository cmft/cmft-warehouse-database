﻿Create procedure [dbo].[BatchMakeCurrStatus90NonReportable] (
	@CensusDate smalldatetime
)
as

update RTTFact

set
	 ReportableFlag = 'N - CurrStatus=90'
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and left(Drill.RTTCurrentStatusCode,2)in('90','91')
and left(convert(nvarchar,Drill.RTTCurrentStatusDate,120),7) < left(convert(nvarchar,Fact.Censusdate,120),7)
and ReportableFlag='Y'



﻿create procedure [dbo].[NOPTheBacklog] as

insert into dbo.RTTOPEncounter
	(
	 SourcePatientNo
	,SourceEntityRecno
	,ReviewedFlag
	,Created
	,LastUser
	)
select distinct
	 Referral.SourcePatientNo
	,Referral.SourceEncounterNo2
	,1
	,getdate()
	,'TRIGGERED SPECIALTIES'
from
	dbo.Referral

inner join dbo.BacklogTriggerSpecialty
on	BacklogTriggerSpecialty.SpecialtyCode = Referral.SpecialtyCode

where
	datediff(day, Referral.ReferralDate, '17 may 2009') > 126

and	not exists
	(
	select
		1
	from
		dbo.RTTOPEncounter
	where
		RTTOPEncounter.SourcePatientNo = Referral.SourcePatientNo
	and	RTTOPEncounter.SourceEntityRecno = Referral.SourceEncounterNo2
	)


insert into dbo.RTTOPClockStop
(
	 SourcePatientNo
	,SourceEntityRecno
	,ClockStopDate
	,ClockStopReasonCode
	,ClockStopComment
	,Created
	,LastUser
)
select distinct
	 Referral.SourcePatientNo
	,Referral.SourceEncounterNo2
	,dateadd(day, 127, Referral.ReferralDate)
	,'NOP'	--System close
	,'Remove backlog patients who have long since breached'
	,getdate()
	,system_user
from
	dbo.Referral

inner join dbo.BacklogTriggerSpecialty
on	BacklogTriggerSpecialty.SpecialtyCode = Referral.SpecialtyCode

where
	datediff(day, Referral.ReferralDate, '17 may 2009') > 126
--and	Referral.RTTPathwayId is null

and	not exists
	(
	select
		1
	from
		dbo.RTTOPClockStop
	where
		RTTOPClockStop.SourcePatientNo = Referral.SourcePatientNo
	and	RTTOPClockStop.SourceEntityRecno = Referral.SourceEncounterNo2
	)


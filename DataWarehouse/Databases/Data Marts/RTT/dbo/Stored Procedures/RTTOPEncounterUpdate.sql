﻿create procedure [dbo].[RTTOPEncounterUpdate]

	 @SourcePatientNo varchar(50) 
	,@SourceEntityRecno varchar(50)
	,@ReviewedFlag bit
	,@SharedBreachFlag bit
	,@RTTUserModeCode varchar(10)
as

if 
	(
	select
		count(*)
	from
		dbo.RTTUser
	where
		RTTUser.RTTUsername = system_user
	) = 1

update
	dbo.RTTUser
set
	RTTUserModeCode = @RTTUserModeCode
where
	RTTUsername = system_user

else

insert into dbo.RTTUser
	(
	 RTTUsername
	,RTTUserModeCode
	)
select
	 RTTUsername = system_user
	,@RTTUserModeCode


UPDATE	RTTOPEncounter
SET
	 ReviewedFlag = @ReviewedFlag
	,Updated = GETDATE()
	,LastUser = SYSTEM_USER
	,SharedBreachFlag = @SharedBreachFlag
WHERE
	SourcePatientNo = @SourcePatientNo
AND	SourceEntityRecno = @SourceEntityRecno


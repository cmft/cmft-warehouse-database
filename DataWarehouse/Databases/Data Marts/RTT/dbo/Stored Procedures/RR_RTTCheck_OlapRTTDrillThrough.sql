﻿create procedure [dbo].[RR_RTTCheck_OlapRTTDrillThrough] As

declare	@censusDate smalldatetime
declare @IPMaintenanceURL varchar(4000)
declare @OPMaintenanceURL varchar(4000)

set @censusdate = '21 Apr 2014'

select
	@OPMaintenanceURL =
	(
	select
		TextValue
	from
		dbo.Parameter
	where
		Parameter = 'OPMAINTENANCEURL'
	)

	,@IPMaintenanceURL =
	(
	select
		TextValue
	from
		dbo.Parameter
	where
		Parameter = 'MAINTENANCEURL'
	)
/*
drop table RR_RTTCheck_OlapRTTDrillThroughBase
select *, cast(null as nvarchar) as 'AppointmentTypeCodeOP',cast(null as nvarchar) as 'AppointmentTypeCodeOPWL'
into RR_RTTCheck_OlapRTTDrillThroughBase
from OlapRTTDrillThroughBase
*/

delete from RR_RTTCheck_OlapRTTDrillThroughBase
where
	CensusDate = @censusDate


insert into dbo.RR_RTTCheck_OlapRTTDrillThroughBase

select
	Referral.EncounterRecno ReferralRecno,
	RTTFact.EncounterTypeCode,
	MatchMaster.CensusDate,
	Referral.SourcePatientNo,
	Referral.SourceEncounterNo2,
	Referral.PatientForename,
	Referral.PatientSurname,
	Referral.DateOfBirth,
	Referral.DateOfDeath,
	Referral.SexCode,
	Referral.NHSNumber,
	Referral.PostCode,
	Referral.Address1,
	Referral.Address2,
	Referral.Address3,
	Referral.Address4,
	Referral.Country,
	Referral.DateOnWaitingList,
	Referral.RegisteredGpCode,
	Referral.PriorityCode,
	Referral.EpisodicGpCode,
	cast(Referral.CasenoteNumber as varchar) CasenoteNumber,
	Referral.DistrictNo,
	Outpatient.EncounterRecno OutpatientRecno,
	Outpatient.PrimaryOperationCode OutpatientProcedureCode,
	Outpatient.ClinicCode,
	Outpatient.SourceOfReferralCode,
	Outpatient.AppointmentDate AttendanceDate,
	Outpatient.FirstAttendanceFlag,
	Outpatient.DNAFlag,
	Outpatient.AttendanceOutcomeCode,
	Outpatient.DisposalCode,
	Outpatient.PreviousDNAs,
	Outpatient.PatientCancellations,
	Outpatient.PreviousAppointmentDate,
	Outpatient.PreviousAppointmentStatusCode,
	Outpatient.NextAppointmentDate,
	Outpatient.LastDnaOrPatientCancelledDate,
	Inpatient.EncounterRecno InpatientRecno,
	Inpatient.AdmissionDate,
	Inpatient.DischargeDate,
	Inpatient.StartSiteCode,
	Inpatient.StartWardType,
	Inpatient.EndSiteCode,
	Inpatient.EndWardType,
	Inpatient.AdmissionMethodCode,
	Inpatient.PatientClassificationCode,
	Inpatient.PatientCategoryCode,
	Inpatient.ManagementIntentionCode,
	Inpatient.DischargeMethodCode,
	Inpatient.DischargeDestinationCode,
	Inpatient.AdminCategoryCode,
	Inpatient.ConsultantCode SpellConsultantCode,
	Inpatient.SpecialtyCode SpellSpecialtyCode,
	Inpatient.PrimaryDiagnosisCode,
	Inpatient.SubsidiaryDiagnosisCode,
	Inpatient.SecondaryDiagnosisCode1,
	Inpatient.SecondaryDiagnosisCode2,
	Inpatient.SecondaryDiagnosisCode3,
	Inpatient.SecondaryDiagnosisCode4,
	Inpatient.SecondaryDiagnosisCode5,
	Inpatient.PrimaryOperationCode,
	Inpatient.PrimaryOperationDate,
	Inpatient.SecondaryOperationCode1,
	Inpatient.SecondaryOperationCode2,
	Inpatient.SecondaryOperationCode3,
	Inpatient.SecondaryOperationCode4,
	Inpatient.SecondaryOperationCode5,
	Inpatient.SecondaryOperationCode6,
	Inpatient.SecondaryOperationCode7,
	Inpatient.SecondaryOperationCode8,
	Inpatient.SecondaryOperationCode9,
	Inpatient.SecondaryOperationCode10,
	Inpatient.SecondaryOperationCode11,
	Inpatient.SecondaryOperationDate1,
	Inpatient.SecondaryOperationDate2,
	Inpatient.SecondaryOperationDate3,
	Inpatient.SecondaryOperationDate4,
	Inpatient.SecondaryOperationDate5,
	Inpatient.SecondaryOperationDate6,
	Inpatient.SecondaryOperationDate7,
	Inpatient.SecondaryOperationDate8,
	Inpatient.SecondaryOperationDate9,
	Inpatient.SecondaryOperationDate10,
	Inpatient.SecondaryOperationDate11,
	Inpatient.CountOfDaysWaiting,
	Inpatient.DateOnWaitingList DateOnIPWaitingList,

--	OPWaiter.QM08StartWaitDate DateOnFutureOPWaitingList,
	OutpatientWL.QM08StartWaitDate DateOnFutureOPWaitingList,


	dateadd(day, 126, Referral.ReferralDate) BreachDate, --18 weeks

	coalesce(AttendanceToIPWL.Priority, AttendanceToSpell.Priority, 1) Quality,

	coalesce(AttendanceToIPWL.MergeDatasetCode, AttendanceToSpell.MergeDatasetCode) Dataset,

	coalesce(AttendanceToIPWL.MergeTemplate, AttendanceToSpell.MergeTemplate) Template,

	OPProcedureCode = Outpatient.PrimaryOperationCode
	
--	(
--	select top 1
--		WL.PrimaryOperationCode
--	from
--		dbo.Outpatient WL
--	where
--		WL.SourcePatientNo = Outpatient.SourcePatientNo
--	and	WL.SourceEncounterNo2 = Outpatient.SourceEncounterNo2
--	and	WL.PrimaryOperationCode is not null
--	and	not exists
--		(
--		select
--			1
--		from
--			dbo.Outpatient LatestOutpatient
--		where
--			LatestOutpatient.SourcePatientNo = WL.SourcePatientNo
--		and	LatestOutpatient.SourceEncounterNo2 = WL.SourceEncounterNo2
--		and	LatestOutpatient.PrimaryOperationCode is not null
----		and not exists
----			(
----			select
----				1
----			from
----				EntityLookup DiagnosticProcedure
----			where
----				DiagnosticProcedure.EntityCode = LatestOutpatient.PrimaryOperationCode
----			and	DiagnosticProcedure.EntityTypeCode = 'DIAGNOSTICPROCEDURE'
----			)
--		and
--			(
--				LatestOutpatient.AttendanceDate < WL.AttendanceDate
--			or
--				(
--					LatestOutpatient.AttendanceDate = WL.AttendanceDate
--				and	LatestOutpatient.EncounterRecno < WL.EncounterRecno
--				)
--			)
--		)
--	) OPProcedureCode

	,RTTFact.AdjustedFlag

	,Referral.RTTPathwayID
	,Referral.RTTPathwayCondition
	,Referral.RTTStartDate
	,Referral.RTTEndDate
	,Referral.RTTSpecialtyCode
	,Referral.RTTCurrentProviderCode
	,Referral.RTTCurrentStatusCode
	,Referral.RTTCurrentStatusDate
	,Referral.RTTCurrentPrivatePatientFlag
	,Referral.RTTOverseasStatusFlag
	,Outpatient.RTTPeriodStatusCode

	,MaintenanceURL =
	case
	when RTTFact.WorkflowStatusCode = 'I' then @IPMaintenanceURL
	when RTTFact.PathwayStatusCode in ('ACS', 'ADM') then @IPMaintenanceURL
	else @OPMaintenanceURL
	end

	+ '?SourcePatientNo=' + Referral.SourcePatientNo
	+ '&SourceEntityRecno=' + 
	case
	when RTTFact.PathwayStatusCode in ('ACS', 'ADM') then Inpatient.SourceEncounterNo2
	else Referral.SourceEncounterNo2
	end + 
	case
	when RTTFact.WorkflowStatusCode = 'I' then ''
	when RTTFact.PathwayStatusCode in ('ACS', 'ADM') then ''
	else '&CensusDate=' + convert(varchar, RTTFact.CensusDate)
	end

	,SourceEntityRecno = 
	case
	when RTTFact.PathwayStatusCode in ('ACS', 'ADM') then Inpatient.SourceEncounterNo2
	else Referral.SourceEncounterNo2
	end

	,SourceTypeCode = 
	case
	when RTTFact.WorkflowStatusCode = 'I' then 'I'
	when RTTFact.PathwayStatusCode in ('ACS', 'ADM') then 'I'
	else 'O'
	end

	,Referral.ReferralDate

	,InpatientWL.IntendedPrimaryOperationCode
,Outpatient.AppointmentTypeCode
,OutpatientWL.AppointmentTypeCode
from
	dbo.MatchMaster

inner join dbo.Referral Referral
on	Referral.EncounterRecno = MatchMaster.ReferralRecno

inner join dbo.RTTFact
on	RTTFact.EncounterRecno = MatchMaster.ReferralRecno
and	RTTFact.CensusDate = MatchMaster.CensusDate

left join dbo.Outpatient Outpatient
on	Outpatient.EncounterRecno = MatchMaster.OutpatientRecno

left join dbo.Inpatient Inpatient
on	Inpatient.EncounterRecno = MatchMaster.InpatientRecno

left join dbo.OutpatientWL OutpatientWL
on	OutpatientWL.EncounterRecno = MatchMaster.FutureOPRecno
--and	OutpatientWL.CensusDate = MatchMaster.CensusDate

--left join dbo.OPWaiter
--on	OPWaiter.EncounterRecno = MatchMaster.FutureOPRecno
--and	OPWaiter.CensusDate = MatchMaster.CensusDate

left join dbo.MergeTemplate AttendanceToIPWL
on	AttendanceToIPWL.MergeTemplateRecno = MatchMaster.AttIPWLTemplateRecno

left join dbo.MergeTemplate AttendanceToSpell
on	AttendanceToSpell.MergeTemplateRecno = MatchMaster.AttSpellTemplateRecno

left join dbo.MergeTemplate SpellToOPWL
on	SpellToOPWL.MergeTemplateRecno = MatchMaster.SpellOPWLTemplateRecno

left join InpatientWL
on	InpatientWL.SourcePatientNo = Inpatient.SourcePatientNo
and	InpatientWL.SourceEntityRecno = Inpatient.SourceEncounterNo2
and	InpatientWL.CensusDate = 
	(
	select
		max(LastCensus.CensusDate)
	from
		InpatientWL LastCensus
	where
		LastCensus.CensusDate <= RTTFact.CensusDate
	and	LastCensus.SourcePatientNo = Inpatient.SourcePatientNo
	and	LastCensus.SourceEntityRecno = Inpatient.SourceEncounterNo2
	)

where
	RTTFact.EncounterTypeCode = 'REFERRAL'
and	MatchMaster.CensusDate = @censusDate

----load last 7 weeks snapshot only
--and	MatchMaster.CensusDate >=
--	(
--	select
--		dateadd(day, -56, max(CensusDate))
--	from
--		dbo.OlapSnapshot
--	)


union all

select
	InpatientWL.EncounterRecno ReferralRecno,
	RTTFact.EncounterTypeCode,
	InpatientWL.CensusDate,
	InpatientWL.SourcePatientNo,
	InpatientWL.SourceEntityRecno,
	InpatientWL.PatientForename,
	InpatientWL.PatientSurname,
	InpatientWL.DateOfBirth,
	InpatientWL.DateOfDeath,
	InpatientWL.SexCode,
	InpatientWL.NHSNumber,
	InpatientWL.PostCode,
	InpatientWL.PatientsAddress1,
	InpatientWL.PatientsAddress2,
	InpatientWL.PatientsAddress3,
	InpatientWL.PatientsAddress4,
	null Country,
	InpatientWL.DateOnWaitingList,
	InpatientWL.RegisteredGpCode,
	InpatientWL.PriorityCode,
	InpatientWL.EpisodicGpCode,
	InpatientWL.CasenoteNumber,
	InpatientWL.DistrictNo,
	null OutpatientRecno,
	null OutpatientProcedureCode,
	null ClinicCode,
	null SourceOfReferralCode,
	null AttendanceDate,
	null FirstAttendanceFlag,
	null DNAFlag,
	null AttendanceOutcomeCode,
	null DisposalCode,
	null PreviousDNAs,
	null PatientCancellations,
	null PreviousAppointmentDate,
	null PreviousAppointmentStatusCode,
	null NextAppointmentDate,
	null LastDnaOrPatientCancelledDate,
	null InpatientRecno,
	null AdmissionDate,
	null DischargeDate,
	null StartSiteCode,
	null StartWardType,
	null EndSiteCode,
	null EndWardType,
	null AdmissionMethodCode,
	null PatientClassificationCode,
	null PatientCategoryCode,
	null ManagementIntentionCode,
	null DischargeMethodCode,
	null DischargeDestinationCode,
	null AdminCategoryCode,
	null SpellConsultantCode,
	null SpellSpecialtyCode,
	null PrimaryDiagnosisCode,
	null SubsidiaryDiagnosisCode,
	null SecondaryDiagnosisCode1,
	null SecondaryDiagnosisCode2,
	null SecondaryDiagnosisCode3,
	null SecondaryDiagnosisCode4,
	null SecondaryDiagnosisCode5,
	null PrimaryOperationCode,
	null PrimaryOperationDate,
	null SecondaryOperationCode1,
	null SecondaryOperationCode2,
	null SecondaryOperationCode3,
	null SecondaryOperationCode4,
	null SecondaryOperationCode5,
	null SecondaryOperationCode6,
	null SecondaryOperationCode7,
	null SecondaryOperationCode8,
	null SecondaryOperationCode9,
	null SecondaryOperationCode10,
	null SecondaryOperationCode11,
	null SecondaryOperationDate1,
	null SecondaryOperationDate2,
	null SecondaryOperationDate3,
	null SecondaryOperationDate4,
	null SecondaryOperationDate5,
	null SecondaryOperationDate6,
	null SecondaryOperationDate7,
	null SecondaryOperationDate8,
	null SecondaryOperationDate9,
	null SecondaryOperationDate10,
	null SecondaryOperationDate11,
	null CountOfDaysWaiting,
	InpatientWL.DateOnWaitingList DateOnIPWaitingList,

	null DateOnFutureOPWaitingList,


	dateadd(day, 126, InpatientWL.ReferralDate) BreachDate, --18 weeks

	null Quality,

	null Dataset,

	null Template,

	null PrimaryOperationCode
	
	,RTTFact.AdjustedFlag

	,InpatientWL.RTTPathwayID
	,InpatientWL.RTTPathwayCondition
	,InpatientWL.RTTStartDate
	,InpatientWL.RTTEndDate
	,InpatientWL.RTTSpecialtyCode
	,InpatientWL.RTTCurrentProviderCode
	,InpatientWL.RTTCurrentStatusCode
	,InpatientWL.RTTCurrentStatusDate
	,InpatientWL.RTTCurrentPrivatePatientFlag
	,InpatientWL.RTTOverseasStatusFlag
	,null RTTPeriodStatusCode

	,MaintenanceURL = @IPMaintenanceURL
	+ '?SourcePatientNo=' + InpatientWL.SourcePatientNo
	+ '&SourceEntityRecno=' + InpatientWL.SourceEntityRecno

	,SourceEntityRecno = InpatientWL.SourceEntityRecno

	,SourceTypeCode = 'I'

	,InpatientWL.ReferralDate

	,InpatientWL.IntendedPrimaryOperationCode
,null
,null
from
	dbo.InpatientWL InpatientWL

inner join dbo.RTTFact
on	RTTFact.EncounterRecno = InpatientWL.EncounterRecno
and	RTTFact.CensusDate = InpatientWL.CensusDate
and	RTTFact.PathwayStatusCode = 'IPW'

where
	InpatientWL.CensusDate = @censusDate

----load last 7 weeks snapshot only
--	InpatientWL.CensusDate >=
--	(
--	select
--		dateadd(day, -56, max(CensusDate))
--	from
--		dbo.OlapSnapshot
--	)

union all

select
	RTTFact.EncounterRecno ReferralRecno,
	RTTFact.EncounterTypeCode,
	RTTFact.CensusDate,
	null,
	null SourceEncounterNo2,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null OutpatientRecno,
	null OutpatientProcedureCode,
	null ClinicCode,
	null SourceOfReferralCode,
	null AttendanceDate,
	null FirstAttendanceFlag,
	null DNAFlag,
	null AttendanceOutcomeCode,
	null DisposalCode,
	null PreviousDNAs,
	null PatientCancellations,
	null PreviousAppointmentDate,
	null PreviousAppointmentStatusCode,
	null NextAppointmentDate,
	null LastDnaOrPatientCancelledDate,
	null InpatientRecno,
	null AdmissionDate,
	null DischargeDate,
	RTTFact.SiteCode StartSiteCode,
	null StartWardType,
	RTTFact.SiteCode EndSiteCode,
	null EndWardType,
	null,
	null PatientClassificationCode,
	null PatientCategoryCode,
	null,
	null DischargeMethodCode,
	null DischargeDestinationCode,
	null AdminCategoryCode,
	RTTFact.ConsultantCode SpellConsultantCode,
	RTTFact.SpecialtyCode SpellSpecialtyCode,
	null PrimaryDiagnosisCode,
	null SubsidiaryDiagnosisCode,
	null SecondaryDiagnosisCode1,
	null SecondaryDiagnosisCode2,
	null SecondaryDiagnosisCode3,
	null SecondaryDiagnosisCode4,
	null SecondaryDiagnosisCode5,
	null PrimaryOperationCode,
	null PrimaryOperationDate,
	null SecondaryOperationCode1,
	null SecondaryOperationCode2,
	null SecondaryOperationCode3,
	null SecondaryOperationCode4,
	null SecondaryOperationCode5,
	null SecondaryOperationCode6,
	null SecondaryOperationCode7,
	null SecondaryOperationCode8,
	null SecondaryOperationCode9,
	null SecondaryOperationCode10,
	null SecondaryOperationCode11,
	null SecondaryOperationDate1,
	null SecondaryOperationDate2,
	null SecondaryOperationDate3,
	null SecondaryOperationDate4,
	null SecondaryOperationDate5,
	null SecondaryOperationDate6,
	null SecondaryOperationDate7,
	null SecondaryOperationDate8,
	null SecondaryOperationDate9,
	null SecondaryOperationDate10,
	null SecondaryOperationDate11,
	null CountOfDaysWaiting,
	null,
	null DateOnFutureOPWaitingList,


	null BreachDate, --18 weeks

	null Quality,

	null Dataset,

	null Template,

	null OPProcedureCode

	,RTTFact.AdjustedFlag,

	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null
	,RTTPeriodStatusCode = null

	,MaintenanceURL = null

	,SourceEntityRecno = null

	,SourceTypeCode = null

	,ReferralDate = null

	,IntendedPrimaryOperationCode = null
,null
,null
from
	dbo.RTTFact

left join dbo.InpatientWL Inpatient
on	Inpatient.EncounterRecno = RTTFact.EncounterRecno
and	Inpatient.CensusDate = RTTFact.CensusDate

where
	RTTFact.EncounterTypeCode = 'GUM'
and	RTTFact.CensusDate = @censusDate


----load last 6 weeks snapshot only
--and	RTTFact.CensusDate >= 
--	(
--	select
--		dateadd(day, -56, max(CensusDate))
--	from
--		dbo.OlapSnapshot
--	)


insert into dbo.RR_RTTCheck_OlapRTTDrillThroughBase

--inpatient
select
	RTTFact.EncounterRecno ReferralRecno,
	RTTFact.EncounterTypeCode,
	RTTFact.CensusDate,
	Encounter.SourcePatientNo,
	Encounter.SourceEncounterNo2,
	Encounter.PatientForename,
	Encounter.PatientSurname,
	Encounter.DateOfBirth,
	Encounter.DateOfDeath,
	Encounter.SexCode,
	Encounter.NHSNumber,
	Encounter.PostCode,
	Encounter.Address1,
	Encounter.Address2,
	Encounter.Address3,
	Encounter.Address4,
	Encounter.Country,
	Encounter.DateOnWaitingList,
	Encounter.RegisteredGpCode,
	Encounter.PriorityCode,
	Encounter.EpisodicGpCode,
	Encounter.CasenoteNumber,
	Encounter.DistrictNo,
	null OutpatientRecno,
	null OutpatientProcedureCode,
	null ClinicCode,
	null SourceOfReferralCode,
	null AttendanceDate,
	null FirstAttendanceFlag,
	null DNAFlag,
	null AttendanceOutcomeCode,
	null DisposalCode,
	null PreviousDNAs,
	null PatientCancellations,
	null PreviousAppointmentDate,
	null PreviousAppointmentStatusCode,
	null NextAppointmentDate,
	null LastDnaOrPatientCancelledDate,
	Encounter.EncounterRecno EncounterRecno,
	Encounter.AdmissionDate,
	Encounter.DischargeDate,
	Encounter.StartSiteCode,
	Encounter.StartWardType,
	Encounter.EndSiteCode,
	Encounter.EndWardType,
	Encounter.AdmissionMethodCode,
	Encounter.PatientClassificationCode,
	Encounter.PatientCategoryCode,
	Encounter.ManagementIntentionCode,
	Encounter.DischargeMethodCode,
	Encounter.DischargeDestinationCode,
	Encounter.AdminCategoryCode,
	Encounter.ConsultantCode SpellConsultantCode,
	Encounter.SpecialtyCode SpellSpecialtyCode,
	Encounter.PrimaryDiagnosisCode,
	Encounter.SubsidiaryDiagnosisCode,
	Encounter.SecondaryDiagnosisCode1,
	Encounter.SecondaryDiagnosisCode2,
	Encounter.SecondaryDiagnosisCode3,
	Encounter.SecondaryDiagnosisCode4,
	Encounter.SecondaryDiagnosisCode5,
	Encounter.PrimaryOperationCode,
	Encounter.PrimaryOperationDate,
	Encounter.SecondaryOperationCode1,
	Encounter.SecondaryOperationCode2,
	Encounter.SecondaryOperationCode3,
	Encounter.SecondaryOperationCode4,
	Encounter.SecondaryOperationCode5,
	Encounter.SecondaryOperationCode6,
	Encounter.SecondaryOperationCode7,
	Encounter.SecondaryOperationCode8,
	Encounter.SecondaryOperationCode9,
	Encounter.SecondaryOperationCode10,
	Encounter.SecondaryOperationCode11,
	Encounter.SecondaryOperationDate1,
	Encounter.SecondaryOperationDate2,
	Encounter.SecondaryOperationDate3,
	Encounter.SecondaryOperationDate4,
	Encounter.SecondaryOperationDate5,
	Encounter.SecondaryOperationDate6,
	Encounter.SecondaryOperationDate7,
	Encounter.SecondaryOperationDate8,
	Encounter.SecondaryOperationDate9,
	Encounter.SecondaryOperationDate10,
	Encounter.SecondaryOperationDate11,
	Encounter.CountOfDaysWaiting,
	Encounter.DateOnWaitingList DateOnIPWaitingList,
	null DateOnFutureOPWaitingList,


	dateadd(day, 126, Encounter.AdmissionDate) BreachDate, --18 weeks

	null Quality,

	null Dataset,

	null Template,

	null OPProcedureCode

	,RTTFact.AdjustedFlag

	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,RTTPeriodStatusCode = null


	,MaintenanceURL =
		@IPMaintenanceURL
		+ '?SourcePatientNo=' + Encounter.SourcePatientNo
		+ '&SourceEntityRecno=' + Encounter.SourceEncounterNo2

	,SourceEntityRecno = Encounter.SourceEncounterNo2

	,SourceTypeCode = 'I'

	,RTTFact.ReferralDate

	,InpatientWL.IntendedPrimaryOperationCode
,null
,null
from
	RTTFact

inner join dbo.Inpatient Encounter
on	Encounter.EncounterRecno = RTTFact.EncounterRecno

left join InpatientWL
on	InpatientWL.SourcePatientNo = Encounter.SourcePatientNo
and	InpatientWL.SourceEntityRecno = Encounter.SourceEncounterNo2
and	InpatientWL.CensusDate = 
	(
	select
		max(LastCensus.CensusDate)
	from
		InpatientWL LastCensus
	where
		LastCensus.CensusDate <= RTTFact.CensusDate
	and	LastCensus.SourcePatientNo = Encounter.SourcePatientNo
	and	LastCensus.SourceEntityRecno = Encounter.SourceEncounterNo2
	)

where
	not exists
	(
	select
		1
	from
		dbo.RR_RTTCheck_OlapRTTDrillThroughBase Drill
	where
		Drill.ReferralRecno = RTTFact.EncounterRecno
	and	Drill.EncounterTypeCode = RTTFact.EncounterTypeCode
	and	Drill.CensusDate = RTTFact.CensusDate
	and	Drill.AdjustedFlag = RTTFact.AdjustedFlag
	)

and	RTTFact.CensusDate = @censusDate


--and	RTTFact.CensusDate >= 
--	(
--	select
--		dateadd(day, -56, max(CensusDate))
--	from
--		dbo.OlapSnapshot
--	)

union all

--outpatient
select
	RTTFact.EncounterRecno ReferralRecno,
	RTTFact.EncounterTypeCode,
	RTTFact.CensusDate,
	Encounter.SourcePatientNo,
	Encounter.SourceEncounterNo2,
	Encounter.PatientForename,
	Encounter.PatientSurname,
	Encounter.DateOfBirth,
	Encounter.DateOfDeath,
	Encounter.SexCode,
	Encounter.NHSNumber,
	Encounter.PostCode,
	Encounter.Address1,
	Encounter.Address2,
	Encounter.Address3,
	Encounter.Address4,
	Encounter.Country,
	DateOnWaitingList = null,
	Encounter.RegisteredGpCode,
	Encounter.PriorityCode,
	Encounter.EpisodicGpCode,
	CasenoteNumber = null,
	Encounter.DistrictNo,
	null OutpatientRecno,
	null OutpatientProcedureCode,
	null ClinicCode,
	null SourceOfReferralCode,
	null AttendanceDate,
	null FirstAttendanceFlag,
	null DNAFlag,
	null AttendanceOutcomeCode,
	null DisposalCode,
	null PreviousDNAs,
	null PatientCancellations,
	null PreviousAppointmentDate,
	null PreviousAppointmentStatusCode,
	null NextAppointmentDate,
	null LastDnaOrPatientCancelledDate,
	Encounter.EncounterRecno EncounterRecno,
	AdmissionDate = null,
	Encounter.DischargeDate,
	StartSiteCode = Encounter.SiteCode,
	StartWardType = null,
	EndSiteCode = Encounter.SiteCode,
	EndWardType = null,
	AdmissionMethodCode = null,
	PatientClassificationCode = null,
	PatientCategoryCode = null,
	ManagementIntentionCode = null,
	DischargeMethodCode = null,
	DischargeDestinationCode = null,
	Encounter.AdminCategoryCode,
	Encounter.ConsultantCode SpellConsultantCode,
	Encounter.SpecialtyCode SpellSpecialtyCode,
	Encounter.PrimaryDiagnosisCode,
	Encounter.SubsidiaryDiagnosisCode,
	Encounter.SecondaryDiagnosisCode1,
	Encounter.SecondaryDiagnosisCode2,
	Encounter.SecondaryDiagnosisCode3,
	Encounter.SecondaryDiagnosisCode4,
	Encounter.SecondaryDiagnosisCode5,
	Encounter.PrimaryOperationCode,
	Encounter.PrimaryOperationDate,
	Encounter.SecondaryOperationCode1,
	Encounter.SecondaryOperationCode2,
	Encounter.SecondaryOperationCode3,
	Encounter.SecondaryOperationCode4,
	Encounter.SecondaryOperationCode5,
	Encounter.SecondaryOperationCode6,
	Encounter.SecondaryOperationCode7,
	Encounter.SecondaryOperationCode8,
	Encounter.SecondaryOperationCode9,
	Encounter.SecondaryOperationCode10,
	Encounter.SecondaryOperationCode11,
	Encounter.SecondaryOperationDate1,
	Encounter.SecondaryOperationDate2,
	Encounter.SecondaryOperationDate3,
	Encounter.SecondaryOperationDate4,
	Encounter.SecondaryOperationDate5,
	Encounter.SecondaryOperationDate6,
	Encounter.SecondaryOperationDate7,
	Encounter.SecondaryOperationDate8,
	Encounter.SecondaryOperationDate9,
	Encounter.SecondaryOperationDate10,
	Encounter.SecondaryOperationDate11,
	CountOfDaysWaiting = null,
	DateOnIPWaitingList = null,
	null DateOnFutureOPWaitingList,


	dateadd(day, 126, Encounter.AppointmentDate) BreachDate, --18 weeks

	null Quality,

	null Dataset,

	null Template,

	null OPProcedureCode

	,RTTFact.AdjustedFlag

	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,RTTPeriodStatusCode = null


	,MaintenanceURL =
		@OPMaintenanceURL
		+ '?SourcePatientNo=' + Encounter.SourcePatientNo
		+ '&SourceEntityRecno=' + Encounter.SourceEncounterNo2
		+ '&CensusDate=' + convert(varchar, RTTFact.CensusDate)

	,SourceEntityRecno = Encounter.SourceEncounterNo2

	,SourceTypeCode = 'O'

	,RTTFact.ReferralDate

	,IntendedPrimaryOperationCode = null
,Encounter.AppointmentTypeCode
,null
from
	RTTFact

inner join dbo.Outpatient Encounter
on	Encounter.EncounterRecno = RTTFact.EncounterRecno

where
	not exists
	(
	select
		1
	from
		dbo.RR_RTTCheck_OlapRTTDrillThroughBase Drill
	where
		Drill.ReferralRecno = RTTFact.EncounterRecno
	and	Drill.EncounterTypeCode = RTTFact.EncounterTypeCode
	and	Drill.CensusDate = RTTFact.CensusDate
	and	Drill.AdjustedFlag = RTTFact.AdjustedFlag
	)

and	RTTFact.CensusDate = @censusDate

--and	RTTFact.CensusDate >= 
--	(
--	select
--		dateadd(day, -56, max(CensusDate))
--	from
--		dbo.OlapSnapshot
--	)



insert into dbo.RR_RTTCheck_OlapRTTDrillThroughBase
(
	 CensusDate
	,ReferralRecno
	,EncounterTypeCode
	,AdjustedFlag
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,RTTCurrentStatusCode
	,RTTPeriodStatusCode
	,ReferralDate
)
select
	 RTTFact.CensusDate
	,RTTFact.EncounterRecno
	,RTTFact.EncounterTypeCode
	,RTTFact.AdjustedFlag
	,RTTFact.PrimaryDiagnosisCode
	,RTTFact.PrimaryOperationCode
	,RTTFact.RTTCurrentStatusCode
	,RTTFact.RTTPeriodStatusCode
	,RTTFact.ReferralDate
from
	RTTFact

inner join dbo.Referral Encounter
on	Encounter.EncounterRecno = RTTFact.EncounterRecno

where
	not exists
	(
	select
		1
	from
		dbo.RR_RTTCheck_OlapRTTDrillThroughBase Drill
	where
		Drill.ReferralRecno = RTTFact.EncounterRecno
	and	Drill.EncounterTypeCode = RTTFact.EncounterTypeCode
	and	Drill.CensusDate = RTTFact.CensusDate
	and	Drill.AdjustedFlag = RTTFact.AdjustedFlag
	)

and	RTTFact.CensusDate = @censusDate

--and	RTTFact.CensusDate >= 
--	(
--	select
--		dateadd(day, -56, max(CensusDate))
--	from
--		dbo.OlapSnapshot
--	)


﻿CREATE procedure [dbo].[BatchMake21NonReportable] (
	@CensusDate smalldatetime
)
as

update RTTFact

set
	 ReportableFlag = 'N - CurrStatus=21A'  -- 20140423 RR added description
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and	Drill.RTTCurrentStatusCode = '21A'
and ReportableFlag='Y'


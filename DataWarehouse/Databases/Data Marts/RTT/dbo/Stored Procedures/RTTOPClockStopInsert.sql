﻿CREATE procedure [dbo].[RTTOPClockStopInsert]

	 @SourcePatientNo varchar(50) 
	,@SourceEntityRecno varchar(50)
	,@ClockStopDate smalldatetime
	,@ClockStopReasonCode varchar(10)
	,@ClockStopComment varchar(4000)
	,@ClockStartDate smalldatetime
	,@ClockStartReasonCode varchar(10)
	,@WaitReasonCode varchar(10)
	,@ClockStopReportingDate smalldatetime
	,@RTTUserModeCode varchar(10)
as

if 
	(
	select
		count(*)
	from
		dbo.RTTUser
	where
		RTTUser.RTTUsername = system_user
	) = 1

update
	dbo.RTTUser
set
	RTTUserModeCode = @RTTUserModeCode
where
	RTTUsername = system_user

else

insert into dbo.RTTUser
	(
	 RTTUsername
	,RTTUserModeCode
	)
select
	 RTTUsername = system_user
	,@RTTUserModeCode


declare @WaitReasonDate smalldatetime

select
	@ClockStopDate = 
	case
	when @ClockStopDate = '1/1/1980' then null
	when @ClockStopReasonCode in ('NOP', 'AGR') then null --Not 18 week pathway; Agree with current dates
	else @ClockStopDate
	end

	,@ClockStopReasonCode = 
	case
	when @ClockStopReasonCode = '' then null
	else @ClockStopReasonCode
	end

	,@ClockStartDate = 
	case
	when @ClockStartDate = '1/1/1980' then null
	when @ClockStartReasonCode in ('NOP', 'AGR') then null --Not 18 week pathway; Agree with current dates
	else @ClockStartDate
	end

	,@ClockStartReasonCode = 
	case
	when @ClockStartReasonCode = '' then null
	else @ClockStartReasonCode
	end

	,@WaitReasonCode = 
	case
	when @WaitReasonCode = '0000' then null
	when @WaitReasonCode = '' then null
	else @WaitReasonCode
	end

	,@WaitReasonDate =
	case
	when @WaitReasonCode = '0000' then null
	when @WaitReasonCode = '' then null
	when @WaitReasonCode is null then null
	else getdate()
	end

INSERT INTO RTTOPClockStop
                         (SourcePatientNo, SourceEntityRecno, ClockStopDate, ClockStopReasonCode, ClockStopComment, Created, Updated, LastUser, ClockStartDate, 
                         ClockStartReasonCode
			,WaitReasonCode
			,WaitReasonDate
			,ClockStopReportingDate
			,RTTUserModeCode
		)
VALUES        (@SourcePatientNo,@SourceEntityRecno,@ClockStopDate,@ClockStopReasonCode,@ClockStopComment, GETDATE(), NULL, 
                         SYSTEM_USER,@ClockStartDate,@ClockStartReasonCode
			,@WaitReasonCode
			,getdate()
			,@ClockStopReportingDate
			,@RTTUserModeCode
		);

select SCOPE_IDENTITY()


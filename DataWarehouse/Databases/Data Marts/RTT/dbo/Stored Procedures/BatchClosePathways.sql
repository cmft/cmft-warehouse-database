﻿create procedure [dbo].[BatchClosePathways] 
as





-- create RTTOPEncounter records to show that the pathways have been touched
insert into dbo.RTTOPEncounter
	(
	 SourcePatientNo
	,SourceEntityRecno
	,ReviewedFlag
	,Created
	,LastUser
	)

select distinct
	 SourcePatientNo = Encounter.InternalNo
	,SourceEntityRecno = Encounter.EpisodeNo
	,1
	,getdate()
	,'BATCH PATHWAY CLOSURE'
from
	RTT2011.[dbo].[1a__Closed pathways] Encounter
where
	not exists
		(
		select
			1
		from
			dbo.RTTOPEncounter
		where
			RTTOPEncounter.SourcePatientNo = Encounter.InternalNo
		and	RTTOPEncounter.SourceEntityRecno = Encounter.EpisodeNo
		)


-- create clock stop records
insert into dbo.RTTOPClockStop
(
	 SourcePatientNo
	,SourceEntityRecno
	,ClockStopDate
	,ClockStopReasonCode
	,ClockStopComment
	,Created
	,LastUser
	,ClockStopReportingDate
	,RTTUserModeCode
)

	
select distinct
	 SourcePatientNo = Encounter.InternalNo
	,SourceEntityRecno = Encounter.EpisodeNo
	,ClockStopDate = getdate()
	,'SYS'	--System close
	,'Batch closure of CMFT Closed Pathways'
	,getdate()
	,system_user
	,'31 Mar 2012'
	,'B'
from
	RTT2011.[dbo].[1a__Closed pathways] Encounter
where
	not exists
		(
		select
			1
		from
			dbo.RTTOPClockStop
		where
			RTTOPClockStop.SourcePatientNo = Encounter.InternalNo
		and	RTTOPClockStop.SourceEntityRecno = Encounter.EpisodeNo
		and	RTTOPClockStop.ClockStopDate is not null
		)



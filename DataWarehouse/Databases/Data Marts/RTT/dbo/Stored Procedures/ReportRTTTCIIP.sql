﻿CREATE procedure [dbo].[ReportRTTTCIIP]

	 @SpecialtyCode varchar (10) = null
	,@HospitalCode varchar (5) = null
	,@ConsultantCode varchar (20) = null
	,@PCTCode varchar (5) = null
	,@ReviewedFlag bit = null
	,@UnknownClockStartFlag bit = null
as

declare @CensusDate smalldatetime
declare @LatestCensusDate smalldatetime
declare @nextMonday smalldatetime

set @CensusDate = (select max(CensusDate) from OlapSnapshot)

set @LatestCensusDate = (select max(CensusDate) from Warehouse.APC.Snapshot)

set @nextMonday = dateadd(day, ((datepart(dw, dateadd(day, datediff(day, 0, getdate()), 0)) - 2) * -1) + 7, dateadd(day, datediff(day, 0, getdate()), 0) )

--select
--	dateadd(day,
--		 coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
--		,RTTClockStartIP.ClockStartDate
--	) AdjustedClockStartDate
--
--
--	,Inpatient.SpecialtyCode
--
--	,Specialty.Specialty
--
--	,Hospital.HospitalAbbreviation HospitalCode
--
--	,case 
--	when Inpatient.SiteCode is null then 'Unknown Hospital' 
--	when Hospital.Hospital is null then Inpatient.SiteCode + ' - No Description' 
--	else Hospital.Hospital
--	end Hospital
--
--	,case 
--    when Inpatient.ConsultantCode is null then 'No Consultant'
--    when Inpatient.ConsultantCode = '' then 'No Consultant'
--    else
--        case 
--            when Consultant.ConsultantSurname is null  then Inpatient.ConsultantCode + ' - No Description'
--            else Consultant.ConsultantSurname + ', ' + coalesce(Consultant.ConsultantForename, '') + ' ' + coalesce(Consultant.ConsultantTitle, '')
--        end
--    end Consultant
--
--	,Inpatient.DistrictNo
--
--	,Inpatient.PatientSurname
--
--	,Inpatient.IntendedPrimaryOperationCode
--
--	,left(Inpatient.IntendedPrimaryOperationCode, 3) OperationCode
--
--	,Inpatient.ManagementIntentionCode
--	,Inpatient.AdmissionMethodCode
--	,Inpatient.PriorityCode
--	,Inpatient.DateOnWaitingList
--	,coalesce(InpatientSinceRTTBuild.TCIDate, Inpatient.TCIDate) TCIDate
--	,Inpatient.PCTCode
--	,PCT.Organisation PCT
--	,Inpatient.SexCode
--	,Inpatient.DateOfBirth
--	,coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredPracticeCode) GpPracticeCode
--
--	,case coalesce(RTTEncounter.ReviewedFlag, convert(bit, 0))
--	when 0 then 'N'
--	else 'Y'
--	end ReviewedFlag
--
--	,MaintenanceURL =
--	(
--	select
--		TextValue
--	from
--		dbo.Parameter
--	where
--		Parameter = 'MAINTENANCEURL'
--	)
--	+ '?SourcePatientNo=' + Inpatient.SourcePatientNo
--	+ '&SourceEntityRecno=' + Inpatient.SourceEntityRecno
--
--	,RTTFact.WeekBandCode
--
--	,WeekBand.DurationBand WeekBand
--
--	,RTTFact.BreachBandCode
--
--	,BreachBand.BreachBand
--
--	,RTTFact.TCIBeyondBreach
--from
--	dbo.RTTFact
--
--inner join dbo.MatchMaster
--on	MatchMaster.ReferralRecno = RTTFact.EncounterRecno
--and	MatchMaster.CensusDate = RTTFact.CensusDate
--
--inner join dbo.InpatientWL Inpatient
--on	Inpatient.EncounterRecno = MatchMaster.InpatientWLRecno
--
--left join dbo.InpatientWL InpatientSinceRTTBuild
--on	InpatientSinceRTTBuild.SourcePatientNo = Inpatient.SourcePatientNo
--and	InpatientSinceRTTBuild.SourceEntityRecno = Inpatient.SourceEntityRecno
--and	InpatientSinceRTTBuild.WaitingListTypeCode = Inpatient.WaitingListTypeCode
--and	InpatientSinceRTTBuild.CensusDate = @LatestCensusDate
--
--left join dbo.RTTEncounter
--on	RTTEncounter.SourcePatientNo = Inpatient.SourcePatientNo
--and	RTTEncounter.SourceEntityRecno = Inpatient.SourceEntityRecno
--
--left join dbo.RTTClockStart RTTClockStartIP
--on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
--and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEntityRecno
--and	not exists
--	(
--	select
--		1
--	from
--		dbo.RTTClockStart
--	where
--		RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
--	and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
--	and	(
--			RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
--		or
--			(
--				RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
--			and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
--			)
--		)
--	)
--
--left join 
--(
--select
--	 RTTAdjustment.SourcePatientNo
--	,RTTAdjustment.SourceEntityRecno
--	,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
--from
--	dbo.RTTAdjustment
--group by
--	 RTTAdjustment.SourcePatientNo
--	,RTTAdjustment.SourceEntityRecno
--) RTTAdjustmentIP
--on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
--and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEntityRecno
--
--left join Pennine..CodeXref NationalConsultantXref 
--on	NationalConsultantXref.XrefEntity = 'PENNINE'
--and	NationalConsultantXref.XrefCode = Inpatient.ConsultantCode
--and	NationalConsultantXref.Entity = 'CONSULTANT'
--
--left join Pennine..Consultant Consultant 
--on	NationalConsultantXref.Code = Consultant.ConsultantCode
--
--left join Modernisation.dbo.Hospital Hospital
--on	Hospital.HospitalCode = Inpatient.SiteCode
--
--left join Modernisation.dbo.SpecialtyPennine Specialty
--on	Specialty.SpecialtyCode = RTTFact.SpecialtyCode
--
--left join Organisation.dbo.PCT PCT
--on	PCT.OrganisationCode = left(Inpatient.PCTCode, 3)
--
--left join dbo.OlapDurationWeeks WeekBand
--on	WeekBand.WaitDurationCode = RTTFact.WeekBandCode
--
--left join dbo.OlapBreachBand BreachBand
--on	BreachBand.BreachBandCode = RTTFact.BreachBandCode
--
--where
--	RTTFact.CensusDate = @CensusDate
----and	RTTFact.EncounterTypeCode = 'UNKIP'
----and	RTTFact.BreachBandCode = 'UK'
--
--and	(
--		RTTFact.SpecialtyCode = @SpecialtyCode
--	or	coalesce(@SpecialtyCode, '') = ''
--	)
--
--and	(
--		Inpatient.SiteCode = @HospitalCode
--	or	coalesce(@HospitalCode, '') = ''
--	)
--
--and	(
--		Inpatient.ConsultantCode = @ConsultantCode
--	or	coalesce(@ConsultantCode, '') = ''
--	)
--
--and	(
--		left(Inpatient.PCTCode, 3) = @PCTCode
--	or	coalesce(@PCTCode, '') = ''
--	)
--
--and	coalesce(InpatientSinceRTTBuild.TCIDate, Inpatient.TCIDate) between @nextMonday and dateadd(day, 6, @nextMonday)
--
--and	RTTFact.WeekBandCode > '15'
--
--and	(
--		coalesce(RTTEncounter.ReviewedFlag, convert(bit, 0)) = @ReviewedFlag
--	or	@ReviewedFlag is null
--	)
--
--and	RTTFact.AdjustedFlag = 'N'
--
--union all

-- Patients admitted for which we have no referral link 

select
	dateadd(day,
		 coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
		,RTTClockStartIP.ClockStartDate
	) AdjustedClockStartDate


	,Inpatient.SpecialtyCode SpecialtyCode

	,Specialty.Specialty

	,HospitalCode =
		Site.Abbreviation

	,Hospital =
	case 
	when Inpatient.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then Inpatient.SiteCode + ' - No Description' 
	else Site.Site
	end

	,Consultant =
	case 
	when upper(Inpatient.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(Inpatient.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
	when Inpatient.ConsultantCode is null then 'No Consultant'
	when Inpatient.ConsultantCode = '' then 'No Consultant'
	else
		case 
			when Consultant.Surname is null  then Inpatient.ConsultantCode + ' - No Description'
			else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
		end
	end

	,Inpatient.DistrictNo DistrictNo

	,Inpatient.PatientSurname PatientSurname

	,Inpatient.IntendedPrimaryOperationCode PrimaryOperationCode

	,left(Inpatient.IntendedPrimaryOperationCode, 3) OperationCode

	,Inpatient.ManagementIntentionCode ManagementIntentionCode
	,Inpatient.AdmissionMethodCode AdmissionMethodCode
	,Inpatient.PriorityCode PriorityCode
	,Inpatient.DateOnWaitingList
	,coalesce(InpatientSinceRTTBuild.TCIDate, Inpatient.TCIDate) TCIDate
	,Inpatient.PCTCode PCTCode
	,PCT.Organisation PCT
	,Inpatient.SexCode SexCode
	,Inpatient.DateOfBirth
	,coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredPracticeCode) GpPracticeCode

	,case coalesce(RTTEncounter.ReviewedFlag, convert(bit, 0))
	when 0 then 'N'
	else 'Y'
	end ReviewedFlag

	,MaintenanceURL =
		(
		select
			TextValue
		from
			dbo.Parameter
		where
			Parameter = 'MAINTENANCEURL'
		)
		+ '?SourcePatientNo=' + Inpatient.SourcePatientNo
		+ '&SourceEntityRecno=' + Inpatient.SourceEntityRecno

	,RTTFact.WeekBandCode

	,WeekBand.DurationBand WeekBand

	,RTTFact.BreachBandCode

	,BreachBand.BreachBand

	,RTTFact.TCIBeyondBreach
from
	dbo.RTTFact

inner join dbo.InpatientWLSnapshot Inpatient
on	Inpatient.EncounterRecno = RTTFact.EncounterRecno
and	Inpatient.CensusDate = RTTFact.CensusDate

left join dbo.InpatientWLSnapshot InpatientSinceRTTBuild
on	InpatientSinceRTTBuild.SourcePatientNo = Inpatient.SourcePatientNo
and	InpatientSinceRTTBuild.SourceEntityRecno = Inpatient.SourceEntityRecno
and	InpatientSinceRTTBuild.CensusDate = @LatestCensusDate

left join dbo.RTTEncounter
on	RTTEncounter.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTEncounter.SourceEntityRecno = Inpatient.SourceEntityRecno

left join dbo.RTTClockStart RTTClockStartIP
on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEntityRecno
and	not exists
	(
	select
		1
	from
		dbo.RTTClockStart
	where
		RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
	and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
	and	(
			RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
		or
			(
				RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
			and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
			)
		)
	)

left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEntityRecno

left join Warehouse.PAS.Consultant ConsultantLocal
on	ConsultantLocal.ConsultantCode = Inpatient.ConsultantCode

left join Warehouse.WH.Consultant Consultant 
on	Consultant.ConsultantCode = ConsultantLocal.NationalConsultantCode

left join Warehouse.PAS.Site PASSite
on	PASSite.SiteCode = Inpatient.SiteCode

left join Warehouse.WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join Warehouse.WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = Inpatient.SpecialtyCode

left join Organisation.dbo.PCT PCT
on	PCT.OrganisationCode = left(Inpatient.PCTCode, 3)

left join dbo.OlapDurationWeeks WeekBand
on	WeekBand.WaitDurationCode = RTTFact.WeekBandCode

left join dbo.OlapBreachBand BreachBand
on	BreachBand.BreachBandCode = RTTFact.BreachBandCode

where
	RTTFact.CensusDate = @CensusDate
and	RTTFact.EncounterTypeCode = 'UNKIPWL'
and	RTTFact.BreachBandCode = 'UK'

and	(
		RTTFact.SpecialtyCode = @SpecialtyCode
	or	coalesce(@SpecialtyCode, '0') = '0'
	)

and	(
		RTTFact.SiteCode = @HospitalCode
	or	coalesce(@HospitalCode, '0') = '0'
	)

and	(
		RTTFact.ConsultantCode = @ConsultantCode
	or	coalesce(@ConsultantCode, '0') = '0'
	)

and	(
		RTTFact.PCTCode = @PCTCode
	or	coalesce(@PCTCode, '0') = '0'
	)

and	(
		coalesce(RTTEncounter.ReviewedFlag, convert(bit, 0)) = @ReviewedFlag
	or	@ReviewedFlag is null
	)

and	coalesce(@UnknownClockStartFlag, 1) = 1

and	coalesce(InpatientSinceRTTBuild.TCIDate, Inpatient.TCIDate) between @nextMonday and dateadd(day, 6, @nextMonday)

and	RTTFact.AdjustedFlag = 'N'


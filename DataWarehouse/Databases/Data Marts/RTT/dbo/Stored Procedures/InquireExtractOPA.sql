﻿CREATE                  procedure [dbo].[InquireExtractOPA] @fromDate smalldatetime = null, @toDate smalldatetime = null, @debug bit = 0 as
set dateformat dmy
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
select @StartTime = getdate()
select @RowsInserted = 0
select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'
select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, getdate())), 0), 112) + '2400'
select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)
select @sql = '
insert into TImportOpEncounter
(
	SourceKey,
	InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo, 
	SourceEncounterNo2, 
	PatientTitle,
	PatientForename, 
	PatientSurname, 
	DateOfBirth, 
	DateOfDeath, 
	SexCode, 
	NHSNumber, 
	PostCode, 
	Address1, 
	Address2, 
	Address3, 
	Address4, 
	DhaCode, 
	EthnicOriginCode, 
	MaritalStatusCode, 
	ReligionCode, 
	RegisteredGPPracticeCode, 
	SourcePurchaserCode,
	SourceProviderCode,
	SourceContractSerialNo,
	SourceAdminCategoryCode,
	ClinicCode,
	AppointmentDate,
	AttendanceTime,
	DNAFlag,
	TransportRequiredFlag,
	AppointmentTypeCode,
	DisposalCode,
	ReferralDate,
	PrimaryOperationCode,
	TextField1,
	ConsultantCode,
	SpecialtyCode,
	ClinicConsultantCode,
	ClinicSpecialtyCode,
	HospitalCode,
	BookingTypeCode,
	CasenoteNo,
	NationalSpecialtyCode,
	AttendanceOutcomeCode,
	AppointmentCreateDate
)
select
	SourceKey,
	''INQ'' InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo, 
	SourceEncounterNo2, 
	PatientTitle, 
	PatientForename, 
	PatientSurname, 
	DateOfBirth, 
	DateOfDeath, 
	SexCode, 
	NHSNumber, 
	PostCode, 
	Address1, 
	Address2, 
	Address3, 
	Address4, 
	DhaCode, 
	EthnicOriginCode, 
	MaritalStatusCode, 
	ReligionCode, 
	RegisteredGPPracticeCode, 
	SourcePurchaserCode,
	SourceProviderCode,
	SourceContractSerialNo,
	SourceAdminCategoryCode,
	ClinicCode,
	AppointmentDate,
	case
	when AppointmentTime = '':'' then null
	else AppointmentDate + '' '' + AppointmentTime
	end AttendanceTime,
	DNAFlag,
	TransportRequiredFlag,
	AppointmentTypeCode,
	DisposalCode,
	ReferralDate,
	PrimaryOperationCode,
	TextField1,
	ClinicConsultantCode ConsultantCode,
	ClinicSpecialtyCode SpecialtyCode,
	ClinicConsultantCode,
	ClinicSpecialtyCode,
	HospitalCode,
	BookingTypeCode,
	CasenoteNo,
	NationalSpecialtyCode,
	AttendanceOutcomeCode,
	AppointmentCreateDate
from
	openquery(INQUIRE, ''
SELECT
	OPA.InternalPatientNumber SourcePatientNo,
	OPA.OPAID SourceKey,
	OPA.OPAID SourceEncounterNo,
	OPA.EpisodeNumber SourceEncounterNo2,
	Patient.Surname PatientSurname,
	Patient.Forenames PatientForename,
	Patient.Title PatientTitle,
	Patient.Sex SexCode,
	Patient.PtDoB DateOfBirth,
	Patient.PtDateOfDeath DateOfDeath,
	Patient.NHSNumber NHSNumber,
	OPA.DistrictNumber DistrictNo,
	Patient.MaritalStatus MaritalStatusCode,
	Patient.Religion ReligionCode,
	Patient.PtAddrPostCode PostCode,
	Patient.PtAddrLine1 Address1,
	Patient.PtAddrLine2 Address2,
	Patient.PtAddrLine3 Address3,
	Patient.PtAddrLine4 Address4,
	OPA.HaCode DhaCode,
	Patient.EthnicType EthnicOriginCode,
	OPA.RegGpCode RegisteredGPPracticeCode,
	OPA.ApptPurchaser SourcePurchaserCode,
	OUTCLNC.ProviderCode SourceProviderCode,
	OPA.ApptConractId SourceContractSerialNo,
	OPA.PtCategory SourceAdminCategoryCode,
	OPA.ClinicCode ClinicCode,
	OPA.ApptDate AppointmentDate,
	case when OPA.ApptTime = "24:00" then "23:59" else OPA.ApptTime end AppointmentTime,
	OPA.RefPriority PriorityCode,
	OPA.RefBy SourceOfReferralCode,
	OPA.EpiGp EpisodicGpCode,
	OPA.PracticeCode EpisodicGPPracticeCode,
	OPA.ReasonForRef ReasonForReferralCode,
	OPA.RefPrimaryDiagnosisCode PrimaryDiagnosisCode,
	OPA.ApptClass FirstAttendanceFlag,
	case
	when ApptStatus = "CND" then "2"
	when ApptStatus = "DNA" then "3"
	when ApptStatus = "ATT" then "5"
	when ApptStatus = "SATT" then "6"
	when ApptStatus = "NATT" then "7"
	when ApptStatus = "WLK" then "5"
	when CancelBy = "P" then "2"
	when CancelBy = "H" then "4"
	else "9"
	end DNAFlag,
	OPA.Transport TransportRequiredFlag,
	OPA.ApptType AppointmentTypeCode,
	OPA.Disposal DisposalCode,
	OPA.ReferralDate ReferralDate,
	OPA.ApptPrimaryProcedureCode PrimaryOperationCode,
	OPA.ResCode TextField1,
	OPA.ClinicConsultant ClinicConsultantCode,
	OPA.ClinicSpecialty ClinicSpecialtyCode,
	OPREFERRAL.HospitalCode HospitalCode,
	OPA.BookingType BookingTypeCode,
	OPA.CaseNoteNumber CasenoteNo,
	SPEC.DohCode NationalSpecialtyCode,
	case
	when ApptStatus = "NR" then
		case
		when CancelBy is Null then ApptStatus
		else CancelBy
		end
	else ApptStatus
	end AttendanceOutcomeCode,
	OPA.ApptBookedDate AppointmentCreateDate
FROM
	OPA
INNER JOIN PATDATA Patient
ON	OPA.InternalPatientNumber = Patient.InternalPatientNumber
INNER JOIN OPREFERRAL 
ON	OPA.InternalPatientNumber = OPREFERRAL.InternalPatientNumber
AND	OPA.EpisodeNumber = OPREFERRAL.EpisodeNumber
AND	OPA.ReferralDateInt = OPREFERRAL.OpRegDtimeInt
INNER JOIN OUTCLNC
ON	OPA.ClinicCode = OUTCLNC.OUTCLNCID
INNER JOIN SPEC 
ON	OUTCLNC.Specialty = SPEC.Specialty
WHERE
	OPA.PtApptStartDtimeInt between ' + @from + ' and ' + @to + '
'')
'
if @debug = 1 
	print @sql
else
	exec (@sql)
SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT
-- ward attenders
select @sql = '
insert into TImportOpEncounter
(
	SourceKey,
	InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo, 
	SourceEncounterNo2, 
	PatientTitle, 
	PatientForename, 
	PatientSurname, 
	DateOfBirth, 
	DateOfDeath, 
	SexCode, 
	NHSNumber, 
	PostCode, 
	Address1, 
	Address2, 
	Address3, 
	Address4, 
	DhaCode, 
	EthnicOriginCode, 
	RegisteredGpCode,
	MaritalStatusCode, 
	ReligionCode, 
	SourcePurchaserCode,
	SourceContractSerialNo,
	SourceAdminCategoryCode,
	ClinicCode,
	AppointmentDate,
	AttendanceTime,
	DNAFlag,
	DisposalCode,
	ReferralDate,
	PrimaryOperationCode,
	ConsultantCode,
	SpecialtyCode,
	ClinicConsultantCode,
	ClinicSpecialtyCode,
	HospitalCode,
	CasenoteNo,
	NationalSpecialtyCode,
	AttendanceOutcomeCode
)
select
	SourceKey,
	''INQ'' InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo, 
	SourceEncounterNo2, 
	PatientTitle, 
	PatientForename, 
	PatientSurname, 
	DateOfBirth, 
	DateOfDeath, 
	SexCode, 
	NHSNumber, 
	PostCode, 
	Address1, 
	Address2, 
	Address3, 
	Address4, 
	DhaCode, 
	EthnicOriginCode, 
	RegisteredGpCode,
	MaritalStatusCode, 
	ReligionCode, 
	SourcePurchaserCode,
	SourceContractSerialNo,
	SourceAdminCategoryCode,
	ClinicCode,
	AppointmentDate,
	AppointmentDate AttendanceTime,
	DNAFlag,
	DisposalCode,
	ReferralDate,
	PrimaryOperationCode,
	ConsultantCode ConsultantCode,
	SpecialtyCode SpecialtyCode,
	ConsultantCode ClinicConsultantCode,
	SpecialtyCode ClinicSpecialtyCode,
	HospitalCode,
	CasenoteNo,
	NationalSpecialtyCode,
	AttendanceOutcomeCode
from
	openquery(INQUIRE, ''
SELECT
	WARDATTENDER.InternalPatientNumber SourcePatientNo,
	WARDATTENDER.WARDATTENDERID SourceKey,
	WARDATTENDER.WARDATTENDERID SourceEncounterNo,
	WARDATTENDER.EpisodeNumber SourceEncounterNo2,
	Patient.Surname PatientSurname,
	Patient.Forenames PatientForename,
	Patient.Title PatientTitle,
	Patient.Sex SexCode,
	Patient.PtDoB DateOfBirth,
	Patient.PtDateOfDeath DateOfDeath,
	Patient.NHSNumber NHSNumber,
	WARDATTENDER.DistrictNumber DistrictNo,
	Patient.MaritalStatus MaritalStatusCode,
	Patient.Religion ReligionCode,
	Patient.PtAddrPostCode PostCode,
	Patient.PtAddrLine1 Address1,
	Patient.PtAddrLine2 Address2,
	Patient.PtAddrLine3 Address3,
	Patient.PtAddrLine4 Address4,
	Patient.DistrictOfResidenceCode DhaCode,
	Patient.EthnicType EthnicOriginCode,
	Patient.GpCode RegisteredGpCode,
	WARDATTENDER.Consultant ConsultantCode,
	WARDATTENDER.Specialty SpecialtyCode,
	WARDATTENDER.PurchaserCode SourcePurchaserCode,
	WARDATTENDER.ContractId SourceContractSerialNo,
	WARDATTENDER.Category SourceAdminCategoryCode,
	WARDATTENDER.Ward ClinicCode,
	WARDATTENDER.AttendanceDate AppointmentDate,
	WARDATTENDER.PriorityType PriorityCode,
	WARDATTENDER.RefBy SourceOfReferralCode,
	Patient.GpCode EpisodicGpCode,
	WARDATTENDER.ReasonForReferralCode ReasonForReferralCode,
	
	case when WARDATTENDER.FirstAttendance = "0" then "2" else "1" end FirstAttendanceFlag,
	case
	when WaWardAttendersAttendanceStatusInternal = "1" then "5"
	when WaWardAttendersAttendanceStatusInternal = "2" then "3"
	when WaWardAttendersAttendanceStatusInternal = "3" then "5"
	else "3"
	end DNAFlag,
	WARDATTENDER.Disposal DisposalCode,
	WARDATTENDER.ReferralDate ReferralDate,
	WARDATTENDER.PrimaryProcedureCode PrimaryOperationCode,
	WARDATTENDER.Consultant ClinicConsultantCode,
	WARDATTENDER.Specialty ClinicSpecialtyCode,
	WARD.HospitalCode HospitalCode,
	WARDATTENDER.CaseNoteNumber CasenoteNo,
	SPEC.DohCode NationalSpecialtyCode,
	case
	when WaWardAttendersAttendanceStatusInternal = "1" then "ATT"
	when WaWardAttendersAttendanceStatusInternal = "2" then "DNA"
	when WaWardAttendersAttendanceStatusInternal = "3" then "WLK"
	else "DNA"
	end AttendanceOutcomeCode
FROM
	WARDATTENDER
INNER JOIN PATDATA Patient
ON	WARDATTENDER.InternalPatientNumber = Patient.InternalPatientNumber
INNER JOIN WARD
ON	WARDATTENDER.Ward = WARD.WARDID
INNER JOIN SPEC 
ON	WARDATTENDER.Specialty = SPEC.Specialty
WHERE
	WARDATTENDER.AttendanceDateInternal between ' + left(@from, 8) + ' and ' + left(@to, 8) + '
and	WARDATTENDER.SeenBy = "DOCTOR"
'') WardAttender
where
	not exists
	(
	select
		1
	from
		dbo.TLoadOpEncounter
	where
		TLoadOpEncounter.SourcePatientNo = WardAttender.SourcePatientNo collate Latin1_General_BIN
	and	TLoadOpEncounter.SourceEncounterNo2 = WardAttender.SourceEncounterNo2 collate Latin1_General_BIN
	)
'
if @debug = 1 
	print @sql
else
	exec (@sql)
SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())
SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'InquireExtractOPA', @Stats


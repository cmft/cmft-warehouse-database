﻿CREATE procedure [dbo].[RTTEncounterSelect] @SourcePatientNo varchar(50), @SourceEntityRecno varchar(50) as

if not exists
	(
	SELECT
		 1
	FROM
		dbo.RTTEncounter
	where
		RTTEncounter.SourcePatientNo = @SourcePatientNo
	and	RTTEncounter.SourceEntityRecno = @SourceEntityRecno
	)

	insert into dbo.RTTEncounter
		(
		 [SourcePatientNo]
		,[SourceEntityRecno]
		,[ReviewedFlag]
		,[Created]
		,[Updated]
		,[LastUser]
		,SharedBreachFlag
		)
	select
		 @SourcePatientNo
		,@SourceEntityRecno
		,convert(bit, 1) ReviewedFlag
		,getdate() Created
		,null Updated
		,system_user LastUser
		,convert(bit, 0) SharedBreachFlag


SELECT
	 [SourcePatientNo]
	,[SourceEntityRecno]
	,[ReviewedFlag]
	,[Created]
	,[Updated]
	,[LastUser]
	,SharedBreachFlag =
		convert(
			bit
			,coalesce(
				SharedBreachFlag
				,0
			)
		)

FROM
	dbo.RTTEncounter
where
	RTTEncounter.SourcePatientNo = @SourcePatientNo
and	RTTEncounter.SourceEntityRecno = @SourceEntityRecno


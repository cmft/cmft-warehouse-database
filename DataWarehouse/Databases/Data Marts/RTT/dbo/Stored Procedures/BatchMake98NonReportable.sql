﻿CREATE procedure [dbo].[BatchMake98NonReportable] (
	@CensusDate smalldatetime
)
as

update RTTFact

set
	 ReportableFlag = 'N - CurrStatus = 98'  --  20140423 RR added description
from
	dbo.OlapRTTDrillThroughBase Drill

inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where
	Fact.CensusDate = @CensusDate
and	Drill.RTTCurrentStatusCode = '98A'
and ReportableFlag='Y'


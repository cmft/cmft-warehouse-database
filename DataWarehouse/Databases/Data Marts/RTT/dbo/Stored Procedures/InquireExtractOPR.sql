﻿CREATE                   procedure [dbo].[InquireExtractOPR] @fromDate smalldatetime = null, @toDate smalldatetime = null, @debug bit = 0 as
set dateformat dmy
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
select @StartTime = getdate()
select @RowsInserted = 0
select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -1, getdate()))), 0), 112) + '0000'
select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, getdate())), 0), 112) + '2400'
select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)
select @sql = '
insert into TImportRfEncounter
(
	SourceKey,
	InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo, 
	SourceEncounterNo2, 
	PatientTitle, 
	PatientForename, 
	PatientSurname, 
	DateOfBirth, 
	DateOfDeath, 
	SexCode, 
	NHSNumber, 
	PostCode, 
	Address1, 
	Address2, 
	Address3, 
	Address4, 
	DhaCode, 
	EthnicOriginCode, 
	MaritalStatusCode, 
	ReligionCode, 
	RegisteredGpCode, 
	ReferralDate,
	ConsultantCode,
	SpecialtyCode,
	EpisodicGPCode,
	EpisodicGpPracticeCode,
	SourceOfReferralCode,
	PriorityCode,
	HospitalCode,
	NationalSpecialtyCode,
	DischargeDate
)
select
	SourceKey,
	''INQ'' InterfaceCode,
	DistrictNo, 
	SourcePatientNo, 
	SourceEncounterNo, 
	SourceEncounterNo2, 
	PatientTitle, 
	PatientForename, 
	PatientSurname, 
	DateOfBirth, 
	DateOfDeath, 
	SexCode, 
	NHSNumber, 
	PostCode, 
	Address1, 
	Address2, 
	Address3, 
	Address4, 
	DhaCode, 
	EthnicOriginCode, 
	MaritalStatusCode, 
	ReligionCode, 
	RegisteredGpCode, 
	ReferralDate,
	ConsultantCode,
	SpecialtyCode,
	EpisodicGPCode,
	EpisodicGpPracticeCode,
	SourceOfReferralCode,
	PriorityCode,
	HospitalCode,
	NationalSpecialtyCode,

	case
	when DischargeDate is null then null
	when right(DischargeDate, 5) = ''24:00'' then left(DischargeDate, 11) + ''23:59''
	else DischargeDate
	end DischargeDate
from
	openquery(INQUIRE, ''
SELECT 
	OPREFERRAL.OPREFERRALID SourceKey,
	OPREFERRAL.InternalPatientNumber SourcePatientNo,
	OPREFERRAL.OPREFERRALID SourceEncounterNo,
	OPREFERRAL.EpisodeNumber SourceEncounterNo2,
	OPREFERRAL.DistrictNumber,
	Patient.Surname PatientSurname,
	Patient.Forenames PatientForename,
	Patient.Title PatientTitle,
	Patient.Sex SexCode,
	Patient.PtDoB DateOfBirth,
	Patient.PtDateOfDeath DateOfDeath,
	Patient.NHSNumber NHSNumber,
	OPREFERRAL.CaseNoteNumber DistrictNo,
	Patient.MaritalStatus MaritalStatusCode,
	Patient.Religion ReligionCode,
	Patient.PtAddrPostCode PostCode,
	Patient.PtAddrLine1 Address1,
	Patient.PtAddrLine2 Address2,
	Patient.PtAddrLine3 Address3,
	Patient.PtAddrLine4 Address4,
	Patient.DistrictOfResidenceCode DhaCode,
	Patient.EthnicType EthnicOriginCode,
	Patient.GpCode RegisteredGpCode,
	OPREFERRAL.EpiGPCode EpisodicGPCode,
	OPREFERRAL.EpiGPPracticeCode EpisodicGpPracticeCode,
	OPREFERRAL.ConsCode ConsultantCode,
	OPREFERRAL.Specialty SpecialtyCode,
	OPREFERRAL.RefBy SourceOfReferralCode,
	OPREFERRAL.ReferralDate,
	OPREFERRAL.PriorityType PriorityCode,
	OPREFERRAL.HospitalCode,
	SPEC.DohCode NationalSpecialtyCode,
	OPDISCHARGE.DischargeDatetime DischargeDate
FROM
	OPREFERRAL
INNER JOIN PATDATA Patient
ON	OPREFERRAL.InternalPatientNumber = Patient.InternalPatientNumber
LEFT JOIN OPDISCHARGE 
ON	OPREFERRAL.InternalPatientNumber = OPDISCHARGE.InternalPatientNumber
AND	OPREFERRAL.EpisodeNumber = OPDISCHARGE.EpisodeNumber
INNER JOIN SPEC 
ON	OPREFERRAL.Specialty = SPEC.Specialty
WHERE
	OPREFERRAL.OpRegDtimeInt between ' + @from + ' and ' + @to + '
'')
'
if @debug = 1 
	print @sql
else
	exec (@sql)
SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())
SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'InquireExtractOPR', @Stats


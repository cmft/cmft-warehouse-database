﻿CREATE procedure [dbo].[ReportRTTUnknownClockStartIP]

	 @SpecialtyCode varchar (10) = null
	,@HospitalCode varchar (5) = null
	,@ConsultantCode varchar (20) = null
	,@PCTCode varchar (5) = null
as

declare @CensusDate smalldatetime

set @CensusDate = (select max(CensusDate) from OlapSnapshot)

-- Patients admitted for which we have no referral link 

select
	dateadd(day,
		 coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
		,RTTClockStartIP.ClockStartDate
	) AdjustedClockStartDate


	,Inpatient.SpecialtyCode

	,Specialty.Specialty

	,HospitalCode =
		Site.Abbreviation

	,Hospital =
	case 
	when Inpatient.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then Inpatient.SiteCode + ' - No Description' 
	else Site.Site
	end

	,Consultant =
	case 
	when upper(Inpatient.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(Inpatient.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
	when Inpatient.ConsultantCode is null then 'No Consultant'
	when Inpatient.ConsultantCode = '' then 'No Consultant'
	else
		case 
			when Consultant.Surname is null  then Inpatient.ConsultantCode + ' - No Description'
			else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
		end
	end

	,Inpatient.DistrictNo

	,Inpatient.PatientSurname

	,Inpatient.PrimaryOperationCode

	,left(Inpatient.PrimaryOperationCode, 3) OperationCode

	,Inpatient.ManagementIntentionCode
	,Inpatient.AdmissionMethodCode
	,Inpatient.PriorityCode
	,Inpatient.DateOnWaitingList
	,Inpatient.AdmissionDate
	,Inpatient.DischargeDate
	,Inpatient.PCTCode
	,PCT.Organisation PCT
	,Inpatient.SexCode
	,Inpatient.DateOfBirth
	,Inpatient.PrimaryOperationDate
	,coalesce(Inpatient.EpisodicGpPracticeCode, RegisteredGpPracticeCode) GpPracticeCode

	,RTTEncounter.ReviewedFlag

	,MaintenanceURL =
	(
	select
		TextValue
	from
		dbo.Parameter
	where
		Parameter = 'MAINTENANCEURL'
	)
	+ '?SourcePatientNo=' + Inpatient.SourcePatientNo
	+ '&SourceEntityRecno=' + Inpatient.SourceEncounterNo2
from
	dbo.RTTFact

inner join dbo.InpatientSnapshot Inpatient
on	Inpatient.EncounterRecno = RTTFact.EncounterRecno
and	Inpatient.SnapshotDate = RTTFact.CensusDate

left join dbo.RTTEncounter
on	RTTEncounter.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTEncounter.SourceEntityRecno = Inpatient.SourceEncounterNo2

left join dbo.RTTClockStart RTTClockStartIP
on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
and	not exists
	(
	select
		1
	from
		dbo.RTTClockStart
	where
		RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
	and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
	and	(
			RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
		or
			(
				RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
			and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
			)
		)
	)

left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

left join Warehouse.PAS.Consultant ConsultantLocal
on	ConsultantLocal.ConsultantCode = Inpatient.ConsultantCode

left join Warehouse.WH.Consultant Consultant 
on	Consultant.ConsultantCode = ConsultantLocal.NationalConsultantCode

left join Warehouse.PAS.Site PASSite
on	PASSite.SiteCode = Inpatient.HospitalCode

left join Warehouse.WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join Warehouse.WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = Inpatient.SpecialtyCode

left join Organisation.dbo.PCT PCT
on	PCT.OrganisationCode = Inpatient.PCTCode

where
	RTTFact.CensusDate = @CensusDate
and	RTTFact.EncounterTypeCode = 'UNKIP'
and	RTTFact.BreachBandCode = 'UK'

and	(
		RTTFact.SpecialtyCode = @SpecialtyCode
	or	coalesce(@SpecialtyCode, '0') = '0'
	)

and	(
		RTTFact.SiteCode = @HospitalCode
	or	coalesce(@HospitalCode, '0') = '0'
	)

and	(
		RTTFact.ConsultantCode = @ConsultantCode
	or	coalesce(@ConsultantCode, '0') = '0'
	)

and	(
		RTTFact.PCTCode = @PCTCode
	or	coalesce(@PCTCode, '0') = '0'
	)

and	RTTFact.AdjustedFlag = 'Y'


﻿CREATE procedure [dbo].[ClearBacklog] 
	@CensusDate smalldatetime = null
as

select
	@CensusDate = 
		coalesce(
			 @CensusDate
			,(
				select
					max(CensusDate)
				from
					dbo.OlapSnapshot
			)
		)

insert into dbo.RTTOPEncounter
	(
	SourcePatientNo
	,SourceEntityRecno
	,ReviewedFlag
	,Created
	,LastUser
	)
select
	 Referral.SourcePatientNo
	,Referral.SourceEncounterNo2
	,1
	,getdate()
	,'REMOVE BACKLOG OVER 22 WEEKS'
from
	dbo.RTTFact

inner join dbo.Referral
on	Referral.EncounterRecno = RTTFact.EncounterRecno

where
	RTTFact.PathwayStatusCode = 'AT'
and	RTTFact.CensusDate = @CensusDate
and	RTTFact.AdjustedFlag = 'Y'
and	RTTFact.EncounterTypeCode = 'REFERRAL'
and	datediff(day, RTTFact.ReferralDate, RTTFact.CensusDate) > 154
--and	Referral.RTTPathwayId is null

and	not exists
	(
	select
		1
	from
		dbo.RTTOPEncounter
	where
		RTTOPEncounter.SourcePatientNo = Referral.SourcePatientNo
	and	RTTOPEncounter.SourceEntityRecno = Referral.SourceEncounterNo2
	)


insert into dbo.RTTOPClockStop
(
	 SourcePatientNo
	,SourceEntityRecno
--	,ClockStopDate
	,ClockStopReasonCode
	,ClockStopComment
	,Created
--	,Updated
	,LastUser
--	,ClockStartDate
--	,ClockStartReasonCode
--	,WaitReasonCode
--	,WaitReasonDate
)
select
	 Referral.SourcePatientNo
	,Referral.SourceEncounterNo2
	,'NOP'	--Not an 18 week pathway
	,'Manually removed backlog of >22 week old referrals with no activity'
	,getdate()
	,'REMOVE BACKLOG OVER 22 WEEKS'
from
	dbo.RTTFact

inner join dbo.Referral
on	Referral.EncounterRecno = RTTFact.EncounterRecno

where
	RTTFact.PathwayStatusCode = 'AT'
and	RTTFact.CensusDate = @CensusDate
and	RTTFact.AdjustedFlag = 'Y'
and	RTTFact.EncounterTypeCode = 'REFERRAL'
and	datediff(day, RTTFact.ReferralDate, RTTFact.CensusDate) > 154
--and	Referral.RTTPathwayId is null

and	not exists
	(
	select
		1
	from
		dbo.RTTOPClockStop
	where
		RTTOPClockStop.SourcePatientNo = Referral.SourcePatientNo
	and	RTTOPClockStop.SourceEntityRecno = Referral.SourceEncounterNo2
	)


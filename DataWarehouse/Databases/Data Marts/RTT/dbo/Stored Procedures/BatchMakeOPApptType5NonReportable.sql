﻿Create Procedure [dbo].[BatchMakeOPApptType5NonReportable]  (
	@CensusDate smalldatetime
)
as

update RTTFact
set	 ReportableFlag = 'N - OPAttApptType = 5'
from dbo.OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where
	Fact.CensusDate = @CensusDate
	and AppointmentTypeCodeOP = '5'
	and ReportableFlag='Y'

update RTTFact
set	 ReportableFlag = 'N - OPWLApptType = 5'
from dbo.OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where
	Fact.CensusDate = @CensusDate
	and AppointmentTypeCodeOPWL = '5'  
	and ReportableFlag='Y'



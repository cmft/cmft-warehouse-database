﻿create procedure [dbo].[RTTOPEncounterInsert]

	 @SourcePatientNo varchar(50) 
	,@SourceEntityRecno varchar(50)
	,@ReviewedFlag bit
	,@SharedBreachFlag bit
	,@RTTUserModeCode varchar(10)
as

if 
	(
	select
		count(*)
	from
		dbo.RTTUser
	where
		RTTUser.RTTUsername = system_user
	) = 1

update
	dbo.RTTUser
set
	RTTUserModeCode = @RTTUserModeCode
where
	RTTUsername = system_user

else

insert into dbo.RTTUser
	(
	 RTTUsername
	,RTTUserModeCode
	)
select
	 RTTUsername = system_user
	,@RTTUserModeCode


INSERT INTO RTTOPEncounter
	(
	 SourcePatientNo
	,SourceEntityRecno
	,ReviewedFlag
	,Created
	,Updated
	,LastUser
	,SharedBreachFlag
	)
VALUES
	(
	 @SourcePatientNo
	,@SourceEntityRecno
	,@ReviewedFlag
	,GETDATE()
	,GETDATE()
	,SYSTEM_USER
	,@SharedBreachFlag
	)


/****** Object:  StoredProcedure [dbo].[RTTOPClockStopInsert]    Script Date: 06/05/2009 11:02:25 ******/
SET ANSI_NULLS ON


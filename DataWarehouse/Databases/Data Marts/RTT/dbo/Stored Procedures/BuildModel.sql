﻿CREATE  procedure [dbo].[BuildModel]
	 @fromDate smalldatetime
	,@toDate smalldatetime
	,@refreshData tinyint
as

/*
***************************************************
**
**
** 20140619		RR	added in new Gynae section
***************************************************
*/


-- This builds the reporting model for a given date range.
-- A snapshot is generated for the given @toDate.
-- (RR query, Snapshots and MatchMaster contain data for CensusDates for last 8 weeks and last censusdate per month.  Should it just keep last 4 weeks and possibly last 2 months in case of queries??)

set @todate = (select cast(cast((getdate()-2)as date)as datetime))

update dbo.Parameter
set
	DateValue = @toDate
where
	 Parameter = 'RTTCENSUSDATE'

if @@rowcount = 0
	insert into dbo.Parameter
		(
		 Parameter
		,DateValue
		)
	select
		 'RTTCENSUSDATE'
		,@toDate


delete from dbo.RTTFact
where
	CensusDate < dateadd(day, (select NumericValue*-7 from Parameter where Parameter = 'KEEPSNAPSHOTWEEKS'), getdate())
and	CensusDate not in (select max(CensusDate) from RTTFact group by datepart(year, CensusDate), datepart(month, CensusDate))


--Build the dataset tables
--copy various datasets into local tables

truncate table dbo.Inpatient
truncate table dbo.Outpatient
truncate table dbo.Referral
truncate table dbo.InpatientWL
truncate table dbo.OutpatientWL

print 'Refresh tables'

if @refreshData = 1
begin
	--ensure the EncounterRecnoMap table is up-to-date
	exec BuildEncounterRecnoMap
Print 'Refresh Encounter table complete'

	--fetch latest activity data from the Warehouse
	exec dbo.BuildInpatient
	exec dbo.BuildOutpatient
	exec dbo.BuildReferral
	exec dbo.BuildInpatientWL @toDate
	exec dbo.BuildOutpatientWL @toDate

	-- 20150205 RR added truncate following conversation with JR
	Truncate table dbo.InpatientSnapshot
	Truncate table dbo.InpatientWLSnapshot
	Truncate table dbo.OutpatientSnapshot
	Truncate table dbo.OutpatientWLSnapshot
	Truncate table dbo.ReferralSnapshot

	exec dbo.BuildSnapshots @toDate

	-- Clear down old snapshot data
	delete from dbo.MatchMaster
	where
		CensusDate < dateadd(day, (select NumericValue*-7 from Parameter where Parameter = 'KEEPSNAPSHOTWEEKS'), getdate())
	and	CensusDate not in (select max(CensusDate) from MatchMaster group by datepart(year, CensusDate), datepart(month, CensusDate))

	-- Build Match
	exec BuildMatch @fromDate, @toDate
	Print 'Refresh Build Match table complete'
end

else
--use snapshot data
begin
	exec dbo.BuildInpatientFromSnapshot @toDate
	exec dbo.BuildOutpatientFromSnapshot @toDate
	exec dbo.BuildReferralFromSnapshot @toDate
	exec dbo.BuildInpatientWLFromSnapshot @toDate
	exec dbo.BuildOutpatientWLFromSnapshot @toDate
end

Print ('Original Process Completed '+cast(cast(getdate()as time)as nvarchar))


---------------------------------------------------------------
--20140619 RR added in new Gynae section

Execute dbo.BuildInpatientWL_All @toDate
Print ('IPWLAll updated ' + cast(cast(getdate()as time)as nvarchar))

Execute RTT_CreateRTTEventsBase @toDate
Print ('RTTEventsBase updated ' + cast(cast(getdate()as time)as nvarchar))

Execute RTT_BuildEventsTable @toDate
Print ('RTTGynae updated ' + cast(cast(getdate()as time)as nvarchar))

Execute RTT_GynaeResults
Print ('GynaeResultsAvailable ' + cast(cast(getdate()as time)as nvarchar))

---------------------------------------------------------------


-- Build the fact table
exec BuildRTTFact @toDate
Print 'Refresh RTTFact table complete'

--Build the drillthrough table
--exec BuildOlapRTTDrillThroughBase @toDate
--exec BuildOlapRTTDrillThrough
--Print 'Refresh Drill Through table complete'

-- the batch closure relies on the Fact and Drill tables being available for a snapshot.
-- look for a more elegant way of doing this during the initial Fact build to avoid reprocessing

-- Batch Closure of non-admitted backlog pathways with RTT End Date
--exec BatchCloseBacklog @toDate










/*  20140507 RR excluded the following, as adopted a different approach
-- Rebuild the fact table to incorporate Batch Closures
exec BuildRTTFact @toDate

-- Rebuild the drillthrough table to incorporate Batch Closures
exec BuildOlapRTTDrillThroughBase @toDate

-- Make Code 21s Non-Reportable
exec BatchMake21NonReportable @toDate

-- Make Pathways with SourceOfReferralCode = 'WDS' non-reportable
exec BatchMakeWDSNonReportable @toDate


--20140422 RR added more reasons for non reportable, these need to be validated.
exec dbo.BatchMake90NonReportable @toDate
exec dbo.BatchMake98NonReportable @toDate
exec dbo.BatchMakeAECNonReportable @toDate

exec BatchMakeIncompletePTLwithPrevEndDateNonReportable @toDate
exec dbo.BatchMakeIPWLwithEndDateNonReportable @toDate
exec BatchMakeStopsInPrevMonthNonReportable @toDate
exec BatchMakeCurrStatus90NonReportable @toDate
exec dbo.BatchMakeOPApptType5NonReportable @toDate
--exec dbo.BatchMakeOPWLRem5NonReportable @toDate
exec dbo.BatchMakeNullPathwayIDsNonReportable @toDate
*/
/*
execute RTT_BuildEventsTable
Print 'Events Proc completed'
execute RTT_UpdatePathwayEvents
Print 'Pathway Events updated'

*/

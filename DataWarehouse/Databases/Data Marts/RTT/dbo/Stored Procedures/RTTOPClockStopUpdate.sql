﻿CREATE procedure [dbo].[RTTOPClockStopUpdate]

	 @ClockStopDate smalldatetime
	,@ClockStopReasonCode varchar(10)
	,@ClockStopComment varchar(4000)
	,@ClockStartDate smalldatetime
	,@ClockStartReasonCode varchar(10)
	,@RTTClockStopRecno int
	,@WaitReasonCode varchar(10)
	,@ClockStopReportingDate smalldatetime
	,@RTTUserModeCode varchar(10)
as

if 
	(
	select
		count(*)
	from
		dbo.RTTUser
	where
		RTTUser.RTTUsername = system_user
	) = 1

update
	dbo.RTTUser
set
	RTTUserModeCode = @RTTUserModeCode
where
	RTTUsername = system_user

else

insert into dbo.RTTUser
	(
	 RTTUsername
	,RTTUserModeCode
	)
select
	 RTTUsername = system_user
	,@RTTUserModeCode


declare @WaitReasonDate smalldatetime

select
	@ClockStopDate = 
	case
	when @ClockStopDate = '1/1/1980' then null
	when @ClockStopReasonCode in ('NOP', 'AGR') then null --Not 18 week pathway; Agree with current dates
	else @ClockStopDate
	end

	,@ClockStopReasonCode = 
	case
	when @ClockStopReasonCode = '' then null
	else @ClockStopReasonCode
	end

	,@ClockStartDate = 
	case
	when @ClockStartDate = '1/1/1980' then null
	when @ClockStartReasonCode in ('NOP', 'AGR') then null --Not 18 week pathway; Agree with current dates
	else @ClockStartDate
	end

	,@ClockStartReasonCode = 
	case
	when @ClockStartReasonCode = '' then null
	else @ClockStartReasonCode
	end

	,@WaitReasonCode = 
	case
	when @WaitReasonCode = '0000' then null
	when @WaitReasonCode = '' then null
	else @WaitReasonCode
	end

	,@WaitReasonDate =
	case
	when @WaitReasonCode = '0000' then null
	when @WaitReasonCode = '' then null
	when @WaitReasonCode is null then null
	else 
	coalesce
	(
		(
		select
			WaitReasonDate
		from
			dbo.RTTOPClockStop
		where
			RTTClockStopRecno = @RTTClockStopRecno
		and	WaitReasonCode = @WaitReasonCode
		)
		,getdate()
	)
	end



UPDATE       RTTOPClockStop
SET                ClockStopDate = @ClockStopDate, ClockStopReasonCode = @ClockStopReasonCode, ClockStopComment = @ClockStopComment, Updated = GETDATE(), 
                         LastUser = SYSTEM_USER, ClockStartDate = @ClockStartDate, ClockStartReasonCode = @ClockStartReasonCode
		,WaitReasonCode = @WaitReasonCode
		,WaitReasonDate = @WaitReasonDate
		,ClockStopReportingDate = @ClockStopReportingDate
		,RTTUserModeCode = @RTTUserModeCode
WHERE        (RTTClockStopRecno = @RTTClockStopRecno)


﻿Create procedure [dbo].[BatchMakeIPWLwithEndDateNonReportable] (
	@CensusDate smalldatetime
)
as

update RTTFact

set
	 ReportableFlag = 'N - IPWLhasPrevEndDate'

from dbo.OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where Fact.CensusDate = @CensusDate
and Fact.PathwayStatusCode = 'IPW'
and RTTEndDate is not null
and Drill.DateonIPWaitingList>RTTEndDate
and left(convert(nvarchar,Drill.DateonIPWaitingList),7)<>left(convert(nvarchar,Drill.censusdate),7)
and ReportableFlag='Y'


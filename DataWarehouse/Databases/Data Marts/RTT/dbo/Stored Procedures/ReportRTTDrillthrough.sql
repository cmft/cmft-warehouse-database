﻿CREATE procedure [dbo].[ReportRTTDrillthrough] 
	@DrillThroughMDX varchar(8000) = null
as

declare @sql varchar(8000)

--select
--	@DrillThroughMDX = 'drillthrough maxrows 10 select ([Adjusted].[Yes],[Reportable].[All].[Reportable]) on 0 from [RTT]'

select
	@sql =
'
select
	 OlapRTTDrillThrough.*
	,DrillThrough.WorkflowStatus
	,DrillThrough.DurationBand
	,DrillThrough.BreachBand
	,DrillThrough.Cases
	,DrillThrough.LastWeeksCases
	,DrillThrough.Specialty
	,DrillThrough.Consultant
	,DrillThrough.WeekBandCode
	,DrillThrough.SiteCode
	,DrillThrough.Organisation
	,DurationWeek.DurationWeek
	,DrillThrough.KeyDate
from
	dbo.OlapRTTDrillThrough

inner join 
	(
	select
		 DrillThroughKey = 
			rtrim(convert(varchar(255), "[$RTT Encounter].[Olap RTT Drill Through]"))

		,WorkflowStatus = 
			"[$WorkflowStatus].[Workflow Status attribute]"

		,DurationBand =
			"[$Duration].[Duration Band attribute]"

		,BreachBand =
			"[$BreachBand].[Breach Band attribute]"

		,Cases =
			"[RTT].[Cases]"

		,LastWeeksCases =
			"[RTT].[Last Weeks Cases]"

		,Specialty =
			"[$Specialty].[Specialty attribute]"

		,Consultant =
			"[$Consultant].[Consultant attribute]"

		,WeekBandCode =
			convert(varchar, "[$Duration].[Duration Week attribute]")

		,SiteCode =
			"[$Site].[Site attribute]"

		,Organisation =
			"[$PCT].[PCT attribute]"

		,KeyDate = 
			convert(smalldatetime, convert(varchar, "[$RTT].[Key Date]"))


	from
		openquery(RTT
		,''
		' + @DrillThroughMDX + ' 

		return
			 key([$RTT Encounter].[Olap RTT Drill Through])
			,[$WorkflowStatus].[Workflow Status attribute]
			,[$Duration].[Duration Band attribute]
			,[$BreachBand].[Breach Band attribute]
			,[$Measures].[Cases]
			,[$Measures].[Last Weeks Cases]
			,[$Specialty].[Specialty attribute]
			,[$Consultant].[Consultant attribute]
			,key([$Duration].[Duration Week attribute])
			,key([$Site].[Site attribute])
			,[$PCT].[PCT attribute]
			,key([$RTT].[Key Date])

		''
		)
	) DrillThrough
on	DrillThrough.DrillThroughKey = 
		''&['' + convert(varchar, OlapRTTDrillThrough.ReferralRecno)
		+ '']&['' + OlapRTTDrillThrough.EncounterTypeCode
		+ '']&['' + convert(varchar, OlapRTTDrillThrough.CensusDate, 126)
		+ '']&['' + OlapRTTDrillThrough.AdjustedFlag
		+ '']''

left join dbo.OlapDurationWeeks DurationWeek
on	DurationWeek.WaitDurationCode = DrillThrough.WeekBandCode
'

exec (@sql)


﻿CREATE procedure [dbo].[ReportRTTInpatientPTL] as

-- Patients admitted for which we have no referral link 

	select
		Inpatient.EncounterRecno ReferralRecno, 

		coalesce(Inpatient.ConsultantCode, '##') ConsultantCode,
		coalesce(Inpatient.SpecialtyCode, '##') SpecialtyCode,

		coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode, '##') PracticeCode,

		coalesce(
			Practice.ParentOrganisationCode,
			PostcodePCTMap.PCTCode,
			'5J5'
		) PCTCode,

		coalesce(Inpatient.HospitalCode, '##') SiteCode,

		'ADM' PathwayStatusCode,

		null ClockStartDate,

		dateadd(day,
			 coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
			,RTTClockStartIP.ClockStartDate
		) AdjustedClockStartDate,

		'C' WorkflowStatusCode,

		Inpatient.AdmissionDate KeyDate,

		1 Cases,

		0 TCIBeyondBreach

--		,case
--		when datediff(week, dateadd(day, -1, -- take a day off 
--			Inpatient.AdmissionDate -- Inpatient Admission
--			), 
--	--		dateadd(day, (datepart(dw, Inpatient.EncounterStartDate) -1) * -1,
--	--		dateadd(week, 1, Inpatient.EncounterStartDate))
--			OlapSnapshot.CensusDate
--			) = 1 then 1
--		else 0
--		end LastWeeksCases

		,coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A') PrimaryDiagnosisCode
		,coalesce(Inpatient.PrimaryOperationCode, 'N/A') PrimaryOperationCode
	from
		Inpatient

	left join dbo.OlapPractice Practice
	on	Practice.OrganisationCode = coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode)

	left join dbo.PostcodePCTMap
	on	PostcodePCTMap.Postcode = Inpatient.Postcode

	left join dbo.RTTEncounter
	on	RTTEncounter.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTEncounter.SourceEntityRecno = Inpatient.SourceEncounterNo2

	left join dbo.RTTClockStart RTTClockStartIP
	on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
	and	not exists
		(
		select
			1
		from
			dbo.RTTClockStart
		where
			RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
		and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
		and	(
				RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
			or
				(
					RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
				and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
				)
			)
		)

	left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
	on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

	where
		Inpatient.EncounterStartDate between '1 sep 2007' and '30 sep 2007'
	and	not exists
		(
		select
			1
		from
			dbo.MatchMaster

		inner join 
			(
			select
				max(OlapSnapshot.CensusDate) CensusDate
			from
				dbo.OlapSnapshot
			) OlapSnapshot
		on	OlapSnapshot.CensusDate = MatchMaster.CensusDate
		and	MatchMaster.InpatientRecno = Inpatient.EncounterRecno
		)

--	and	datediff(month, Inpatient.EncounterStartDate, OlapSnapshot.CensusDate) = 0

--and	Inpatient.SourcePatientNo = '1484644'
--and	Inpatient.SourceEncounterNo2 = '18'


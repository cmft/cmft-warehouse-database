﻿CREATE  PROCEDURE [dbo].[BuildWkInpatientWL] @censusDate smalldatetime as

truncate table WkInpatientWL


insert into WkInpatientWL
(
	EncounterRecno,
	EncounterEndDate,
	SourcePatientNo,
	SourceEntityRecno,
	HospitalCode,
	ConsultantCode,
	SpecialtyCode,
	DateOnWaitingList,
	RTTPathwayID
)
select
	WL.EncounterRecno,
	@censusDate EncounterEndDate,
	WL.SourcePatientNo,
	WL.SourceEntityRecno,
	WL.SiteCode HospitalCode,
	WL.ConsultantCode,
	WL.SpecialtyCode,
	WL.DateOnWaitingList,
	WL.RTTPathwayID
from
	dbo.InpatientWL WL

--where
--	WL.CensusDate = 
--	(
--	select
--		max(CensusDate)
--		----add a day on because of new snapshot scheduling
--		--dateadd(dd , 1 , max(CensusDate))
--	from
--		Warehouse.APC.Snapshot Snapshot
--	where
--		Snapshot.CensusDate <= @censusDate
--	)


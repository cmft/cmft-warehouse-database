﻿Create procedure [dbo].[BatchMakeAECNonReportable] (
	@CensusDate smalldatetime
)
as

update RTTFact
set	ReportableFlag = 'N - RefSrcAEC' -- 20140423 RR added description
from dbo.OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag

where Fact.CensusDate = @CensusDate
and	Drill.SourceOfReferralCode in ('AEC','CAS')
and ReportableFlag='Y'


-- RefSrc EDT, exclude as these are dummy episodes for Trafford referrals.

update RTTFact
set ReportableFlag = 'N - RefSrcEDT' -- 20140423 RR added description
from dbo.OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where Fact.CensusDate = @CensusDate
and	Drill.SourceOfReferralCode in ('EDT')
and ReportableFlag='Y'


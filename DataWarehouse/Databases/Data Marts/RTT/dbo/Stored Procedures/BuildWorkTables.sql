﻿CREATE procedure [dbo].[BuildWorkTables] @fromDate smalldatetime = null, @toDate smalldatetime = null as

select
	@fromDate =
		coalesce(
			 @fromDate
			,(select DateValue from dbo.Parameter where Parameter = 'RTTSTARTDATE')
		)

exec BuildWkOutpatientWL @toDate

exec BuildWkOutpatient @fromDate, @toDate

exec BuildWkInpatientWL @toDate

--exec [BuildWkInpatientWLAll] @toDate -- RR added this 20140521

exec BuildWkSpell @fromDate, @toDate

exec BuildWkReferral @fromDate, @toDate


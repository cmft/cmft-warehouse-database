﻿CREATE   procedure [dbo].[BuildMatchMaster1] @fromDate smalldatetime, @toDate smalldatetime as

declare @fDate smalldatetime
declare @tDate smalldatetime

select @fDate = @fromDate
select @tDate = @toDate

select
	@tDate CensusDate,
	Referral.EncounterRecno ReferralRecno,
	Outpatient.EncounterRecno OutpatientRecno,
	case when coalesce(InpatientByEncounter.EncounterRecno, InpatientByWL.EncounterRecno) is null then InpatientWL.EncounterRecno else null end InpatientWLRecno,
	coalesce(InpatientByEncounter.EncounterRecno, InpatientByWL.EncounterRecno) InpatientRecno,

	null FutureOPRecno,

	IPToFutureOP.EncounterRecno IPToFutureOPRecno,

	ATTIPWL.MergeTemplateRecno AttIPWLTemplateRecno,
	ATTSPELL.MergeTemplateRecno AttSpellTemplateRecno,
	SPELLOPWL.MergeTemplateRecno SpellOPWLTemplateRecno
from
	dbo.WkReferral Referral

-- link to latest O/P
left join dbo.WkOutpatient Outpatient
on	Outpatient.SourcePatientNo = Referral.SourcePatientNo
and	Outpatient.SourceEncounterNo2 = Referral.SourceEncounterNo2

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'ATTIPWL'
		)
	and	MergeData.MatchRecno is not null

	) ATTIPWL
on	ATTIPWL.SourceRecno = Outpatient.EncounterRecno

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'ATTSPELL'
		)
	and	MergeData.MatchRecno is not null

	) ATTSPELL
on	ATTSPELL.SourceRecno = Outpatient.EncounterRecno

left join dbo.WkInpatientWL InpatientWL
on	InpatientWL.EncounterRecno = ATTIPWL.MatchRecno


left join dbo.WkSpell InpatientByWL
on	InpatientByWL.SourcePatientNo = InpatientWL.SourcePatientNo
and	InpatientByWL.SourceEncounterNo = InpatientWL.SourceEntityRecno


left join dbo.WkSpell InpatientByEncounter
on	InpatientByEncounter.EncounterRecno = ATTSPELL.MatchRecno

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'SPELLOPWL'
		)
	and	MergeData.MatchRecno is not null

	) SPELLOPWL
on	SPELLOPWL.SourceRecno = coalesce(InpatientByEncounter.EncounterRecno, InpatientByWL.EncounterRecno)

left join dbo.OPWaiter IPToFutureOP
on	IPToFutureOP.EncounterRecno = SPELLOPWL.MatchRecno

--left join #FutureOP FutureOP
--on	FutureOP.SourcePatientNo = Referral.SourcePatientNo collate Latin1_General_BIN
--and	FutureOP.SourceEntityRecno = Referral.SourceEncounterNo2 collate Latin1_General_BIN

where
	Referral.ReferralDate between @fDate and @tDate

--
--select
--	SourcePatientNo,
--	SourceEntityRecno,
--	EncounterRecno
--into
--	#FutureOP
--from
--	dbo.OPWaiterLatest
--where
--	CensusDate = @toDate
--
--CREATE CLUSTERED INDEX #IX ON #FutureOP
--	(
--	SourcePatientNo,
--	SourceEntityRecno
--	)
--
--update
--	MatchMaster
--set
--	FutureOPRecno = FutureOP.EncounterRecno
--from
--	MatchMaster
--
--inner join dbo.WkReferral Referral
--on	Referral.EncounterRecno = MatchMaster.ReferralRecno
--
--inner join #FutureOP FutureOP
--on	FutureOP.SourcePatientNo = Referral.SourcePatientNo collate Latin1_General_BIN
--and	FutureOP.SourceEntityRecno = Referral.SourceEncounterNo2 collate Latin1_General_BIN
--


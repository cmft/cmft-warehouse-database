﻿
CREATE Procedure [dbo].[RTT_UpdatePathwayEvents] As

--	20140430	RR


Update 
	RTTGynaePathways
set 
	EventDate = null
	,EventTime = null
where 
	EventDate>='20500101'


--Update the RTT Pathway Status
update 
	RTTGynaePathways
set 
	PathwayStatus = 'x'



update 
	RTTGynaePathways
set 
	PathwayStatus=
				case 
					when [Status]='Ticking' then 'Start' 
					when [Status]='Out-Trmnt' then 'Non Active' 
					else [Status] 
				end
where OrderID='1'



update 
	A
set 
	PathwayStatus=[Status]
from 
	RTTGynaePathways A 

inner join
	(select 
		RTTPathwayID,
		[Min] = min(orderID)
	from 
		RTTGynaePathways 
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID	
	and A.OrderID = B.[min] 
	and OrderID<>1



update 
	A
set 
	PathwayStatus='Start'
from 
	RTTGynaePathways A 

inner join
	(select 
		RTTPathwayID
		,[Min] = min(orderID)
	from 
		RTTGynaePathways 
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.[min] 
	and PathwayStatus is null


-- Can we put something in to cancel job if running for more than 2mins??
While 
	(select distinct PathwayStatus from RTTGynaePathways where PathwayStatus = 'x') = 'x'	
Begin	
	
Update 
	A	
set 
	PathwayStatus = D.CurrentPathwayStatus	
From 
	RTTGynaePathways A 

inner join 	
	(select 
		RTTPathwayID
		,[Min] = min (OrderID)
	from RTTGynaePathways 
	where 
		PathwayStatus = 'x' 
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.[Min]	

inner join RTTGynaePathways C
on A.RTTPathwayID = C.RTTPathwayID 
	and A.OrderID = (C.OrderID +1)	

inner join [RTTPathwayStatusComb] D
on A.[Status] = D.CurrentStatus 
	and C.PathwayStatus = D.PreviousPathwayStatus	

End	








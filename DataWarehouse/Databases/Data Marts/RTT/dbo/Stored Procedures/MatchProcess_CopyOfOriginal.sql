﻿Create Procedure [dbo].[MatchProcess_CopyOfOriginal] @MergeDatasetCode varchar(10) as

declare @TemplateRecno varchar(10)
declare @Template varchar(128)
declare @WrkSQL varchar(255)
declare @StartTime smalldatetime
declare @Merged int 
declare @Failed int
declare @Elapsed int
declare @Stats varchar(255)
declare @Source varchar (128)


select @StartTime = getdate()

TRUNCATE TABLE  TMergeData

declare TemplateCursor cursor for

select
	MergeTemplateRecno,
	MergeTemplateCode
from
	MergeTemplate
where  	
	Active = 1 
and	MergeDatasetCode = @MergeDatasetCode

order by
	Priority

OPEN TemplateCursor
  
FETCH NEXT FROM TemplateCursor

INTO 	@TemplateRecno, @Template

WHILE @@FETCH_STATUS = 0

BEGIN

	select @WrkSQL = 'Insert into TMergeData (SourceRecno, MatchRecno, MergeTemplateRecno, Priority) Select SourceRecno, MatchRecno, ' + @TemplateRecno + ', Priority from ' + @Template

--	print @WrkSQL
	execute(@WrkSQL)
	FETCH NEXT FROM TemplateCursor
	INTO @TemplateRecno, @Template
END
  
CLOSE TemplateCursor
DEALLOCATE TemplateCursor


--clear down table
delete from MergeData
where
	exists
	(
	select
		1
	from
		dbo.MergeTemplate
	where
		MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
	and	MergeTemplate.MergeDatasetCode = @MergeDatasetCode
	)


--insert the match with the highest priority template
Insert into MergeData
(
	SourceRecno, MatchRecno, MergeTemplateRecno
)
select
 	SourceRecno, MatchRecno, MergeTemplateRecno
from 
	TMergeData a


where not exists
	(
		select 
			1
		from
			TMergeData b
		where
			a.SourceRecno = b.SourceRecno 
		and	a.MergeDataRecno <> b.MergeDataRecno
		and	(
			b.Priority < a.Priority
			or
				(
					b.Priority = a.Priority 
				and	b.MergeDataRecno > a.MergeDataRecno
				)
			)
	)



-- Audit message
select 
	@Merged = count(*) 
from 
	MergeData
where
	MatchRecno is not null


select
	@Failed = count(*) 
from 
	MergeData
where
	MatchRecno is null

select @Elapsed = ROUND(DATEDIFF(second, @StartTime, getdate()), 0)

select @Stats = 
	'MergeDatasetCode ' + @MergeDatasetCode +
	', Processed ' + CONVERT(varchar(10), @Merged + @Failed) +
    ', Merged ' + CONVERT(varchar(10), @Merged) +
    ', Failed ' + CONVERT(varchar(10), @Failed) +
    ', Time Elapsed ' + CONVERT(varchar(4), @Elapsed) + ' Seconds'

select @Source = 'Merge'

EXEC WriteAuditLogEvent @Source, @Stats


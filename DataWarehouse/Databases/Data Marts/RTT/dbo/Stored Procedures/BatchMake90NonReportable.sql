﻿CREATE Procedure [dbo].[BatchMake90NonReportable]  (
	@CensusDate smalldatetime
)
as

update RTTFact
set	 ReportableFlag = 'N - CurrStatus/DispCod = 90/P4'
from dbo.OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where
	Fact.CensusDate = @CensusDate
	and left(DisposalCode,2)='P4'
	and (left(Drill.RTTCurrentStatusCode,1) = '9' or Drill.RTTCurrentStatusCode is null)
	and ReportableFlag='Y'


/*
select Drill.RTTCurrentStatusCode,DisposalCode,count(*)
from dbo.RR_RTTCheck_OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where
	Fact.CensusDate = '20140421'--@CensusDate
	and SpecialtyCode = 'Gyn'
	and left(DisposalCode,2)<>'P4'
	and left(Drill.RTTCurrentStatusCode,2) in ('90','91','98')
	group by Drill.RTTCurrentStatusCode,DisposalCode
	
	*/

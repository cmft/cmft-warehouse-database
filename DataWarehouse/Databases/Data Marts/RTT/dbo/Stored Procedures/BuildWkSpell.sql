﻿CREATE   PROCEDURE [dbo].[BuildWkSpell] @fromDate smalldatetime, @toDate smalldatetime as

truncate table WkSpell

insert into WkSpell
(
	EncounterRecno,
	EncounterStartDate,
	SourcePatientNo,
	SourceEncounterNo,
	HospitalCode,
	ConsultantCode,
	SpecialtyCode,
	DateOnWaitingList,
	RTTPathwayID
)
select
	Encounter.EncounterRecno,
	Encounter.EncounterStartDate, -- 8 July 2007 PDO - this is the admission date even though it is going into the EncounterEndDate - I would need to change all the match templates otherwise
	Encounter.SourcePatientNo,
	Encounter.SourceSpellNo,
	Encounter.HospitalCode,
	Encounter.ConsultantCode,
	Encounter.SpecialtyCode,
	Encounter.DateOnWaitingList,
	Encounter.RTTPathwayID
from
	Inpatient Encounter

where
	Encounter.EncounterStartDate between @fromDate and @toDate


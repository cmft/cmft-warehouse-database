﻿CREATE Procedure [dbo].[BatchMakeOPWLRem5NonReportable]  (
	@CensusDate smalldatetime
)
as

-- Update to non reportable where appointment type = 5 in the OPWLEntry
-- 1) where all otherevents are null
update RTTFact
set	 ReportableFlag = 'N - OPWLRem = 5'
where 
CensusDate = @CensusDate
and ReportableFlag='Y'
and EncounterRecNo in
	(select ReferralRecNo
	from matchmaster inner join Referral 
	on ReferralRecNo = EncounterRecNo
	inner join PAS.Inquire.OPWLRemEntry OPWL
	on InternalPatientNumber = SourcePatientNo and EpisodeNumber = SourceEncounterNo
	where AppointmentType = '5'
	and OutpatientRecNo is null and InpatientWLRecNo is null and InpatientRecNo is null and FutureOPRecNo is null)

--25 rows

drop table RR_WLRemTemp
select distinct ReferralRecNo,OutpatientRecNo, AppointmentType, DateonList,OPWLRemDateTimeExt
into RR_WLRemTemp
from matchmaster inner join Referral 
on ReferralRecNo = EncounterRecNo inner join PAS.Inquire.OPWLRemEntry OPWL
on InternalPatientNumber = SourcePatientNo and EpisodeNumber = SourceEncounterNo
where OutpatientRecNo is not null and InpatientWLRecNo is null and InpatientRecNo is null and FutureOPRecNo is null
		
--2) where Outpatient is not null and removal is after the Date on List 
update RTTFact
set	 ReportableFlag = 'N - OPWLRem = 5'
where 
CensusDate = @CensusDate
and ReportableFlag='Y'
and EncounterRecNo in
	(select B.ReferralRecNo
	from Outpatient inner join RR_WLRemTemp B
	on EncounterRecNo = OutpatientRecNo
	where (cast((right(DateonList,4)+substring(DateonList,4,2)+left(DateonList,2))as datetime)<= AppointmentDate
	and cast((substring(OPWLRemDateTimeExt,7,4)+substring(OPWLRemDateTimeExt,4,2)+left(OPWLRemDateTimeExt,2))as datetime)>=AppointmentDate)
	or cast((right(DateonList,4)+substring(DateonList,4,2)+left(DateonList,2))as datetime)>= AppointmentDate)
--12 rows





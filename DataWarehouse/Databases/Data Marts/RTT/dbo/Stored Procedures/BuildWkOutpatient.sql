﻿CREATE  PROCEDURE [dbo].[BuildWkOutpatient] @fromDate smalldatetime, @toDate smalldatetime as

truncate table WkOutpatient

insert into WkOutpatient
(
	EncounterRecno,
	EncounterEndDate,
	SourcePatientNo,
	SourceEncounterNo2,
	HospitalCode,
	ConsultantCode,
	SpecialtyCode,
	AppointmentDate,
	RTTPathwayID
)
select
	Outpatient.EncounterRecno,
	Outpatient.EncounterEndDate,
	Outpatient.SourcePatientNo,
	Outpatient.SourceEncounterNo2,
	Outpatient.HospitalCode,
	Outpatient.ConsultantCode,
	Outpatient.SpecialtyCode,
	Outpatient.AppointmentDate,
	Outpatient.RTTPathwayID
from
	Outpatient
where 
	Outpatient.EncounterEndDate between @fromDate and @toDate

--20090923 - to link to the latest non-90 appointment
--20140423 RR Excluded the following criteria, it wasn't doing anything as the code is coming through as the Local Code not the national, ie 90A
--  If this is appropriate need to add the A, or do a left 2. 
-- I think we bring 90's through and if this is the latest appt in the process we can update to non active??
--and	coalesce(Outpatient.RTTPeriodStatusCode, '') <> '90'


and	not exists
	(
	select
		1
	from
		dbo.Outpatient LatestBooked
	where
		LatestBooked.SourcePatientNo = Outpatient.SourcePatientNo
	and	LatestBooked.SourceEncounterNo2 = Outpatient.SourceEncounterNo2
	and	LatestBooked.EncounterEndDate between @fromDate and @toDate
	and
		(
			LatestBooked.AttendanceTime > Outpatient.AttendanceTime
		or
			(
				LatestBooked.AttendanceTime = Outpatient.AttendanceTime
			and	LatestBooked.EncounterRecno > Outpatient.EncounterRecno
			)
		)
	and	coalesce(LatestBooked.RTTPeriodStatusCode, '') <> '90'
	)

--20090923 - used to link to the latest appointment
--and	not exists
--	(
--	select
--		1
--	from
--		dbo.Outpatient LatestBooked
--	where
--		LatestBooked.SourcePatientNo = Outpatient.SourcePatientNo
--	and	LatestBooked.SourceEncounterNo2 = Outpatient.SourceEncounterNo2
--	and	LatestBooked.EncounterEndDate between @fromDate and @toDate
--	and
--		(
--			LatestBooked.AttendanceTime > Outpatient.AttendanceTime
--		or
--			(
--				LatestBooked.AttendanceTime = Outpatient.AttendanceTime
--			and	LatestBooked.EncounterRecno > Outpatient.EncounterRecno
--			)
--		)
--	)


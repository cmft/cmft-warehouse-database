﻿CREATE procedure [dbo].[BatchMakeStopsInPrevMonthNonReportable] (
	@CensusDate smalldatetime
)
as

update RTTFact
set	 ReportableFlag = 'N - StopsInPrevMonth'
from dbo.OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where Fact.CensusDate = @CensusDate
and Fact.PathwayStatusCode in ('ACS','ADM','OPD')
--and RTTEndDate = RTTCurrentStatusDate
and (left(Drill.RTTCurrentStatusCode,1) = '3' or left(Drill.RTTCurrentStatusCode,2) in ('90','91','98'))
and left(convert(nvarchar,RTTCurrentStatusDate,120),7) < left(convert(nvarchar,Fact.Censusdate,120),7)
and ReportableFlag='Y'




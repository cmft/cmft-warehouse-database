﻿create procedure [dbo].[RTTClockStartInsert]

	 @SourcePatientNo varchar(50) 
	,@SourceEntityRecno varchar(50)
	,@ClockStartDate smalldatetime
	,@ClockStartReasonCode varchar(10)
	,@ClockStartComment varchar(4000)
as

select
	@ClockStartDate = 
	case
	when @ClockStartDate = '1/1/1980' then null
	when @ClockStartReasonCode in ('NOP', 'AGR') then null --Not 18 week pathway; Agree with current dates
	else @ClockStartDate
	end

	,@ClockStartReasonCode = 
	case
	when @ClockStartReasonCode = '' then null
	else @ClockStartReasonCode
	end


INSERT INTO RTTClockStart
	(
	 SourcePatientNo
	,SourceEntityRecno
	,ClockStartDate
	,ClockStartReasonCode
	,ClockStartComment
	,Created
	,Updated
	,LastUser
	)
VALUES 
	(
	 @SourcePatientNo
	,@SourceEntityRecno
	,@ClockStartDate
	,@ClockStartReasonCode
	,@ClockStartComment
	,GETDATE()
	,null
	,SYSTEM_USER
	)


select SCOPE_IDENTITY()


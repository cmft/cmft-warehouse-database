﻿CREATE procedure [dbo].[RTTOPEncounterSelect] @SourcePatientNo varchar(50), @SourceEntityRecno varchar(50) as


if not exists
	(
	SELECT
		 1
	FROM
		dbo.RTTOPEncounter
	where
		RTTOPEncounter.SourcePatientNo = @SourcePatientNo
	and	RTTOPEncounter.SourceEntityRecno = @SourceEntityRecno
	)

	insert into dbo.RTTOPEncounter
		(
		 [SourcePatientNo]
		,[SourceEntityRecno]
		,[ReviewedFlag]
		,[Created]
		,[Updated]
		,[LastUser]
		,SharedBreachFlag
		)
	select
		 @SourcePatientNo
		,@SourceEntityRecno
		,convert(bit, 1) ReviewedFlag
		,getdate() Created
		,getdate() Updated
		,system_user LastUser
		,convert(bit, 0) SharedBreachFlag


SELECT
	 [SourcePatientNo]
	,[SourceEntityRecno]
	,[ReviewedFlag]
	,[Created]
	,[Updated]
	,[LastUser]
	,SharedBreachFlag =
		convert(
			bit
			,coalesce(
				SharedBreachFlag
				,0
			)
		)

	,RTTUserModeCode =
		coalesce(
			RTTUser.RTTUserModeCode
			,'S'
		)

	,RTTUserModeBackgroundColour =
		coalesce(
			RTTUserMode.RTTUserModeBackgroundColour
			,'white'
		)
FROM
	dbo.RTTOPEncounter

left join dbo.RTTUser
on	RTTUser.RTTUsername = system_user

left join dbo.RTTUserMode
on	RTTUserMode.RTTUserModeCode = coalesce(RTTUser.RTTUserModeCode, 'S')

where
	RTTOPEncounter.SourcePatientNo = @SourcePatientNo
and	RTTOPEncounter.SourceEntityRecno = @SourceEntityRecno


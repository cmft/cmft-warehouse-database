﻿
CREATE Procedure [dbo].[RTT_GynaeResults] As

-- Results

declare 
	@start datetime
	,@census datetime
	,@MonthEnd datetime
	,@MonthStart datetime


set @start = '20140401'
set @census = (select DateValue from dbo.Parameter where Parameter = 'RTTCENSUSDATE')
set @MonthEnd = dateadd(ss, -1, dateadd(month, datediff(month, 0, getdate()), 0)) 
set @MonthStart = DATEADD(month, DATEDIFF(month, -1, getdate()) - 2, 0) 


If @census between 15 and 22 
Begin

	Delete 
	from 
		RTTGynaePathwaysArchive
	where 
		Census between @Census-7 and @Census

	Insert into RTTGynaePathwaysArchive
	Select 
		* 
	from 
		RTTGynaePathways

End


-- Stops
Truncate table RTTCompletedPathways
Insert into RTTCompletedPathways
select 
	DistrictNo
	,A.SourcePatientNo
	,A.RTTPathwayID
	,SpecCode = NationalSpecialtyCode 
	,SpecialtyCode
	,ConsultantCode
	,ReferralDate = B.EventDate
	,IPWLAdd = C.EventDate
	,StartEncounterNo
	,StartID
	,StartDate
	,StartReason
	,StopEncounterNo
	,StopID
	,StopDate
	,StopReason
	,MonthOfStop = left(convert(nvarchar,StopDate,120),7)
	,EventType
	,Category 
	,Census
from 
	[RTTStops] A

left join 
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTTGynaePathways 
	where EventType = 'OPReferral'
	) B
on A.SourcePatientNo = B.SourcePatientNo
and B.SourceEncounterNo = StartEncounterNo

left join
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTTGynaePathways 
	where left(EventType,7) = 'IPWLAdd'
	) C
on A.SourcePatientNo = C.SourcePatientNo
and C.SourceEncounterNo = StartEncounterNo

where 
	StopDate between @MonthStart and @census
and Reportable = 'y'
and NationalSpecialtyCode = '502'


If @census between 15 and 22 
Begin

	Delete 
	from 
		RTTCompletedPathwaysSnapshot
	where 
		Census between @Census-7 and @Census
		
	Insert into RTTCompletedPathwaysSnapshot
	Select 
		* 
		,SnapshotDate = getdate()
	from 
		RTTCompletedPathways
	where 
		StopDate between @MonthStart and @MonthEnd

End

--3854
-- Ticking Detail
Truncate table RTTIncompletePathways
Insert into RTTIncompletePathways
	(
	DistrictNo
	,A.RTTPathwayID
	,SpecialtyCode
	,ConsultantCode
	,A.SourcePatientNo
	,StartEncounterNo
	,ReferralDate
	,ReferralSource
	,StartDate
	,StartReason
	,BreachDate
	,StartDisposalCode
	,StartClinic
	,StartEvSt
	,LatestEncounterNo
	,LatestDate
	,LatestReason
	,IPWLAddDate 
	,LatestEvSt
	,LatestDisposalCode
	,LatestClinic
	,RTTWeeksWaiting
	,MonthsSinceLastEvent 
	,RTTCurrentStatusCode
	,EventStatus
	,NextEventDate
	,NextEventTime
	,NextEventType
	,NextEventReason
	,Category 
	,Census
	)
select  
	DistrictNo
	,A.RTTPathwayID
	,SpecialtyCode
	,ConsultantCode
	,A.SourcePatientNo
	,StartEncounterNo
	,ReferralDate = B.EventDate
	,ReferralSource
	,StartDate
	,StartReason
	,BreachDate
	,StartDisposalCode
	,StartClinic
	,StartEvSt
	,LatestEncounterNo
	,LatestDate
	,LatestReason
	,IPWLAddDate = C.EventDate
	,LatestEvSt
	,LatestDisposalCode
	,LatestClinic
	,RTTWeeksWaiting
	,MonthsSinceLastEvent = cast((datediff(day,LatestDate,Census)/30.4) as int)
	,RTTCurrentStatusCode
	,EventStatus
	,NextEventDate
	,NextEventTime
	,NextEventType
	,NextEventReason
	,Category 
	,Census
from 
	RTTIncompletes A
	
left join 
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTTGynaePathways 
	where EventType = 'OPReferral'
	) B
on A.SourcePatientNo = B.SourcePatientNo
and B.SourceEncounterNo = StartEncounterNo

left join
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTTGynaePathways 
	where 
		left(EventType,7) = 'IPWLAdd'
	) C
on A.SourcePatientNo = C.SourcePatientNo
and C.SourceEncounterNo = LatestEncounterNo

where 
	NationalSpecialtyCode = '502'
and Reportable = 'Y' 
and (
	NextReportable = 'Y' 
	or NextReportable is null
	)

-- Last Month End

Execute [dbo].[BuildRTTIncompletesCensus] @MonthEnd

Truncate table RTTIncompletePathwaysMonthEnd
Insert into RTTIncompletePathwaysMonthEnd

select  
	DistrictNo
	,A.RTTPathwayID
	,SpecialtyCode
	,ConsultantCode
	,A.SourcePatientNo
	,StartEncounterNo
	,ReferralDate = B.EventDate
	,ReferralSource
	,StartDate
	,StartReason
	,BreachDate
	,StartDisposalCode
	,StartClinic
	,StartEvSt
	,LatestEncounterNo
	,LatestDate
	,LatestReason
	,IPWLAddDate = C.EventDate
	,LatestEvSt
	,LatestDisposalCode
	,LatestClinic
	,RTTWeeksWaiting
	,MonthsSinceLastEvent = cast((datediff(day,LatestDate,Census)/30.4) as int)
	,RTTCurrentStatusCode
	,EventStatus
	,NextEventDate
	,NextEventTime
	,NextEventType
	,NextEventReason
	,Category 
	,Census
from 
	dbo.RTTIncompletesCensus A
	
left join 
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTTGynaePathways 
	where EventType = 'OPReferral'
	) B
on A.SourcePatientNo = B.SourcePatientNo
and B.SourceEncounterNo = StartEncounterNo

left join
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTTGynaePathways 
	where 
		left(EventType,7) = 'IPWLAdd'
	) C
on A.SourcePatientNo = C.SourcePatientNo
and C.SourceEncounterNo = LatestEncounterNo

where 
	NationalSpecialtyCode = '502'
and Reportable = 'Y' 
and (
	NextReportable = 'Y' 
	or NextReportable is null
	)

If @census between 15 and 22 
Begin

	Delete 
	from 
		RTTIncompletePathwaysMonthEndSnapshot
	where 
		Census = @MonthEnd
		
	Insert into RTTIncompletePathwaysMonthEndSnapshot
	Select 
		* 
		,SnapshotDate = getdate()
	from 
		RTTIncompletePathwaysMonthEnd
End



----------------------------------------------------
-- Validation report

--20150213

-- Use RTTGynaePathways
-- Create View RTTValidation

Insert into RTTValidationArchive
	(
	Census
	,RTTPathwayID
	,EventTime
	,EventType
	,OutcomeCode
	,OutcomeID
	)
select
	Census
	,RTTPathwayID
	,EventTime
	,EventType
	,OutcomeCode
	,OutcomeID = 5 -- To be Validated -- DQ need read write on this column
from
	RTTGynaePathways
where
	EventTime between '20150215' and @Census
and not exists
		(
		select
			1
		from 
			RTTValidationArchive NoChange
		where
			RTTGynaePathways.RTTPathwayID = NoChange.RTTPathwayID
		and RTTGynaePathways.EventTime = NoChange.EventTime
		and RTTGynaePathways.EventType = NoChange.EventType
		)

-- Dropped off the PTL, so we don't need to revalidate
Update 
	RTTValidationArchive
set 
	PreviouslyValidated = 'Y'
from 
	RTTValidationArchive

left join RTTGynaePathways
on RTTValidationArchive.RTTPathwayID = RTTGynaePathways.RTTPathwayID
and RTTValidationArchive.EventTime = RTTGynaePathways.EventTime
and RTTValidationArchive.EventType = RTTGynaePathways.EventType

where 
	RTTGynaePathways.RTTPathwayID is null


/*20150211
-- Currently doing the bottom separately, but going forward update to one table with a column for Type - ie Incomplete or Stop
Insert into RTTIncompletePathwaysValidationArchive
	(
	Census
	,RTTPathwayID
	,StartDate
	,LatestDate
	,OutcomeID
	)
select
	Census
	,RTTPathwayID
	,StartDate
	,LatestDate
	,OutcomeID = 5 -- To be Validated -- DQ need read write on this column
from
	RTTIncompletePathways
where
	not exists
		(
		select
			1
		from 
			RTTIncompletePathwaysValidationArchive NoChange
		where
			RTTIncompletePathways.RTTPathwayID = NoChange.RTTPathwayID
		and RTTIncompletePathways.StartDate = NoChange.StartDate
		and RTTIncompletePathways.LatestDate = NoChange.LatestDate
		)

-- Dropped off the PTL, so we don't need to revalidate
Update 
	RTTIncompletePathwaysValidationArchive
set 
	PreviouslyValidated = 'Y'
from 
	RTTIncompletePathwaysValidationArchive

left join RTTIncompletePathways
on RTTIncompletePathwaysValidationArchive.RTTPathwayID = RTTIncompletePathways.RTTPathwayID
and RTTIncompletePathwaysValidationArchive.StartDate = RTTIncompletePathways.StartDate
and RTTIncompletePathwaysValidationArchive.LatestDate = RTTIncompletePathways.LatestDate

where 
	RTTIncompletePathways.RTTPathwayID is null


Insert into RTTCompletePathwaysValidationArchive
	(
	Census
	,RTTPathwayID
	,StartDate
	,StopDate
	,EventType
	,OutcomeID
	)
select
	Census
	,RTTPathwayID
	,StartDate
	,StopDate
	,EventType
	,OutcomeID = 5 -- To be Validated -- DQ need read write on this column
from
	RTTCompletedPathways
where
	not exists
		(
		select
			1
		from 
			RTTCompletePathwaysValidationArchive NoChange
		where
			RTTCompletedPathways.RTTPathwayID = NoChange.RTTPathwayID
		and RTTCompletedPathways.StartDate = NoChange.StartDate
		and RTTCompletedPathways.StopDate = NoChange.StopDate
		and RTTCompletedPathways.EventType = NoChange.EventType
		)

*/

/*

---------------------------------
--New Return

select
	Census
	,Section = 
			case 
				when left(NextEventType,4) = 'IPWL' then 'Section1' 
				else 'Section2' 
			end
	,Category = 
			case 
				when BreachDate <Census then '7) BreachDatePassedLast7Days'
				when datediff(day,Census,BreachDate)<9 then '6) Breach0-1Week'
				when datediff(day,Census,BreachDate)<16 then '5) Breach1-2Week'
				when datediff(day,Census,BreachDate)<23 then '4) Breach2-3Week'
				when datediff(day,Census,BreachDate)<30 then '3) Breach3-4Week'
				when datediff(day,Census,BreachDate)<37 then '2) Breach4-5Week'
				when datediff(day,Census,BreachDate)<44 then '1) Breach5-6Week'
				else null
			end
	,Inclusion =
			case
				when NextEventDate<BreachDate then 'N TCI/ApptBeforeBreachDate'
				when NextEventDate is not null then 'Y TCI/ApptAfterBreachDate'
				else 'Y NoTCI/Appt'
			end
	,Value = count(*)
from RR_RTTTicking
where 
	NationalSpecialtyCode = '502'
and Reportable = 'Y' 
and (
	NextReportable = 'Y' 
	or NextReportable is null
	)
and BreachDate>=dateadd(dd,-7,Census)
and datediff(day,Census,BreachDate)<44
group by
	Census
	,case 
		when left(NextEventType,4) = 'IPWL' then 'Section1' 
		else 'Section2' 
	end
	,case 
		when BreachDate <Census then '7) BreachDatePassedLast7Days'
		when datediff(day,Census,BreachDate)<9 then '6) Breach0-1Week'
		when datediff(day,Census,BreachDate)<16 then '5) Breach1-2Week'
		when datediff(day,Census,BreachDate)<23 then '4) Breach2-3Week'
		when datediff(day,Census,BreachDate)<30 then '3) Breach3-4Week'
		when datediff(day,Census,BreachDate)<37 then '2) Breach4-5Week'
		when datediff(day,Census,BreachDate)<44 then '1) Breach5-6Week'
		else null
	end
	,case
		when NextEventDate<BreachDate then 'N TCI/ApptBeforeBreachDate'
		when NextEventDate is not null then 'Y TCI/ApptAfterBreachDate'
		else 'Y NoTCI/Appt'
	end

Union

select 
	Census
	,Section = 
			case 
				when left(NextEventType,4) = 'IPWL' then 'Section1' 
				else 'Section2' 
			end
	,Category = '8) TotalBreaches'
	,Inclusion =
			case
				when NextEventDate<BreachDate then 'N TCI/ApptBeforeBreachDate'
				when NextEventDate is not null then 'Y TCI/ApptAfterBreachDate'
				else 'Y NoTCI/Appt'
			end
	,Value = count(*) 
from RR_RTTTicking
where 
	NationalSpecialtyCode = '502'
and Reportable = 'Y' 
and (
	NextReportable = 'Y' 
	or NextReportable is null
	)
and BreachDate<Census
group by
	Census
	,case 
		when left(NextEventType,4) = 'IPWL' then 'Section1' 
		else 'Section2' 
	end
	,case
		when NextEventDate<BreachDate then 'N TCI/ApptBeforeBreachDate'
		when NextEventDate is not null then 'Y TCI/ApptAfterBreachDate'
		else 'Y NoTCI/Appt'
	end

Union

select
	Census
	,Section = ('Section3 ' + EventType)
	,Category 
	,Inclusion = 'Y'
	,Value = count(*)
from 
	RR_RTTStops
where 
	StopDate between dateadd(dd,-7,Census) and Census
group by
	Census
	,('Section3 ' + EventType)
	,Category 

Union

select
	Census
	,Section = 'Section 4'	
	,Category =
			case 
				when left(NextEventType,4) = 'IPWL' then 'DTA' 
				else 'NoDTA' 
			end
	,Inclusion = 'Y'
	,Value = count(*)
from 
	RR_RTTTicking
where 
	NationalSpecialtyCode = '502'
and Reportable = 'Y' 
and (
	NextReportable = 'Y' 
	or NextReportable is null
	)

group by
	Census
	,case 
		when left(NextEventType,4) = 'IPWL' then 'DTA' 
		else 'NoDTA' 
	end

	*/	


﻿Create Procedure [dbo].[RR_RTTCheck_WL5NonReportable]  (
	@CensusDate smalldatetime
)
as

update RTTFact
set	 ReportableFlag = 'N'
from dbo.RR_RTTCheck_OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where
	Fact.CensusDate = '20140421'--@CensusDate
	and SpecialtyCode = 'Gyn'
	and AppointmentTypeCodeOP = '5'

update RTTFact
set	 ReportableFlag = 'N'
from dbo.RR_RTTCheck_OlapRTTDrillThroughBase Drill
inner join RTTFact Fact
on	Drill.ReferralRecno = Fact.EncounterRecno
and	Drill.EncounterTypeCode = Fact.EncounterTypeCode
and	Drill.CensusDate = Fact.CensusDate
and	Drill.AdjustedFlag = Fact.AdjustedFlag
where
	Fact.CensusDate = '20140421'--@CensusDate
	and SpecialtyCode = 'Gyn'
	and AppointmentTypeCodeOPWL = '5' and (AppointmentTypeCodeOP<>'5' or AppointmentTypeCodeOP is null)



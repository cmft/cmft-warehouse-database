﻿CREATE   procedure [dbo].[LoadOPWaiter] @censusDate smalldatetime = null as

set dateformat dmy

DECLARE @StartTime datetime
DECLARE @Elapsed int
DECLARE @RowsInserted Int
DECLARE @RowsDeleted Int
DECLARE @Stats varchar(255)
DECLARE @SolverRowsUpdated Int
SELECT @StartTime = getdate()

declare @sql varchar(8000)

set @censusDate = coalesce(@censusDate, dateadd(day, 0, datediff(day, 0, dateadd(day, -1, getdate()))))

truncate table TLoadOPWaiter

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TLoadOPWaiterView]') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW [dbo].[TLoadOPWaiterView]

set @sql =
'
create view TLoadOPWaiterView as
select
	 SourcePatientNo
	,SourceEntityRecno
	,''' + convert(varchar, @censusDate) + ''' CensusDate
	,DistrictNo
	,CasenoteNumber
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,PatientForename
	,PatientSurname
	,Postcode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,AppointmentTypeCode
	,AppointmentStatusCode
	,CancelledBy
	,AppointmentCategoryCode
	,DateOfBirth
	,QM08StartWaitDate
	,QM08EndWaitDate
	,PurchaserCode
	,AppointmentTime
	,SiteCode
	,PriorityCode
	,SexCode
	,HomePhone
	,WorkPhone
	,ClinicCode
	,CommentClinical
	,ResCode
	,ApptPrimaryProcedureCode

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
from
	openquery(INQUIRE, ''

SELECT
	R.InternalPatientNumber SourcePatientNo, 
	R.EpisodeNumber SourceEntityRecno,
	R.DistrictNumber DistrictNo, 
	R.CaseNoteNumber CasenoteNumber, 
	R.ReferralDate, 
	O.ApptBookedDate BookedDate, 
	O.ApptBookedTime BookedTime, 
	ApptDate AppointmentDate, 
	P.Forenames PatientForename, 
	P.Surname PatientSurname, 
	P.PtAddrPostCode Postcode, 
	R.EpiGPCode EpisodicGpCode, 
	R.EpiGPPracticeCode EpisodicGpPracticeCode, 
	R.ConsCode ConsultantCode, 
	R.Specialty SpecialtyCode, 
	R.RefBy SourceOfReferralCode, 
	O.ApptType AppointmentTypeCode, 
	O.ApptStatus AppointmentStatusCode, 
	O.CancelBy CancelledBy, 
	O.ApptCategory AppointmentCategoryCode, 
	P.PtDoB DateOfBirth, 
	R.QM08StartWaitDate, 
	R.QM08EndWtDate QM08EndWaitDate, 
	O.ApptPurchaser PurchaserCode, 
	O.PtApptStartDtimeInt AppointmentTime, 
	R.HospitalCode SiteCode, 
	R.PriorityType PriorityCode, 
	P.Sex SexCode,
	P.PtHomePhone HomePhone,
	P.PtWorkPhone WorkPhone,
	O.ClinicCode,
	O.ApptComment CommentClinical,
	O.ResCode,
	O.ApptPrimaryProcedureCode

	,PW.PathwayNumber RTTPathwayID
	,PW.PathwayCondition RTTPathwayCondition
	,PW.RTTStartDate
	,PW.RTTEndDate
	,PW.RTTSpeciality RTTSpecialtyCode
	,PW.RTTCurProv RTTCurrentProviderCode
	,PW.RTTCurrentStatus RTTCurrentStatusCode
	,PW.RTTCurrentStatusDate
	,PW.RTTPrivatePat RTTCurrentPrivatePatientFlag
	,PW.RTTOSVStatus RTTOverseasStatusFlag

	,O.RTTPeriodStatus RTTPeriodStatusCode

FROM
	 OPA O 

INNER JOIN OPREFERRAL R
ON	R.EpisodeNumber = O.EpisodeNumber
AND	R.InternalPatientNumber = O.InternalPatientNumber

INNER JOIN PATDATA P 
ON	P.InternalPatientNumber = R.InternalPatientNumber

left join RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = R.InternalPatientNumber
and	EPW.EpisodeNumber = R.EpisodeNumber

left join RTTPTPATHWAYCURRENT PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber

WHERE
	O.PtApptStartDtimeInt >=
' + 
		datename(year, @censusDate) + right('0' + convert(varchar, datepart(month, @censusDate)), 2) + right('0' + convert(varchar, datepart(day, @censusDate)), 2) + '0000'
+ '	

'')
'

--amended 29 Jan 2008 - PDO
--WHERE
--	R.DischargeDt Is Null
--AND	R.OpRegDtimeInt > 200401010000
--AND O.PtApptStartDtimeInt >=
--' + 
--		datename(year, @censusDate) + right('0' + convert(varchar, datepart(month, @censusDate)), 2) + right('0' + convert(varchar, datepart(day, @censusDate)), 2) + '0000'
--+ '	

--amended above 29 Oct 2007 - PDO
--no need to select future OPs with no appointment date

--FROM
--	R 
--	
--
--LEFT JOIN O 
--ON	R.EpisodeNumber = O.EpisodeNumber
--AND	R.InternalPatientNumber = O.InternalPatientNumber
--
--INNER JOIN P 
--ON	R.InternalPatientNumber = P.InternalPatientNumber
--
--WHERE
--	R.DischargeDt Is Null
--AND	R.OpRegDtimeInt > 200401010000
--AND 
--	(
--		O.PtApptStartDtimeInt is null
--	or	O.PtApptStartDtimeInt >=

--print (@sql)

exec (@sql)

insert into TLoadOPWaiter
(
       SourcePatientNo
      ,SourceEntityRecno
      ,CensusDate
      ,DistrictNo
      ,CasenoteNumber
      ,ReferralDate
      ,BookedDate
      ,BookedTime
      ,AppointmentDate
      ,PatientForename
      ,PatientSurname
      ,Postcode
      ,EpisodicGpCode
      ,EpisodicGpPracticeCode
      ,ConsultantCode
      ,SpecialtyCode
      ,SourceOfReferralCode
      ,AppointmentTypeCode
      ,AppointmentStatusCode
      ,CancelledBy
      ,AppointmentCategoryCode
      ,DateOfBirth
      ,QM08StartWaitDate
      ,QM08EndWaitDate
      ,PurchaserCode
      ,AppointmentTime
      ,SiteCode
      ,PriorityCode
      ,SexCode
      ,HomePhone
      ,WorkPhone
      ,ClinicCode
      ,CommentClinical
      ,ResCode
      ,ApptPrimaryProcedureCode

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
)
select
       SourcePatientNo
      ,SourceEntityRecno
      ,CensusDate
      ,DistrictNo
      ,CasenoteNumber
      ,ReferralDate
      ,BookedDate
      ,BookedTime
      ,AppointmentDate
      ,PatientForename
      ,PatientSurname
      ,Postcode
      ,EpisodicGpCode
      ,EpisodicGpPracticeCode
      ,ConsultantCode
      ,SpecialtyCode
      ,SourceOfReferralCode
      ,AppointmentTypeCode
      ,AppointmentStatusCode
      ,CancelledBy
      ,AppointmentCategoryCode
      ,DateOfBirth
      ,QM08StartWaitDate
      ,QM08EndWaitDate
      ,PurchaserCode
      ,AppointmentTime
      ,SiteCode
      ,PriorityCode
      ,SexCode
      ,HomePhone
      ,WorkPhone
      ,ClinicCode
      ,CommentClinical
      ,ResCode
      ,ApptPrimaryProcedureCode

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
from
	TLoadOPWaiterView



delete from OPWaiter
where
	CensusDate = @censusDate

SELECT @RowsDeleted = @@Rowcount

insert into OPWaiter
(
       SourcePatientNo
      ,SourceEntityRecno
      ,CensusDate
      ,DistrictNo
      ,CasenoteNumber
      ,ReferralDate
      ,BookedDate
      ,AppointmentDate
      ,PatientForename
      ,PatientSurname
      ,Postcode
      ,EpisodicGpCode
      ,EpisodicGpPracticeCode
      ,ConsultantCode
      ,SpecialtyCode
      ,SourceOfReferralCode
      ,AppointmentTypeCode
      ,AppointmentStatusCode
      ,CancelledBy
      ,AppointmentCategoryCode
      ,DateOfBirth
      ,QM08StartWaitDate
      ,QM08EndWaitDate
      ,PurchaserCode
      ,AppointmentTime
      ,SiteCode
      ,PriorityCode
      ,SexCode
      ,HomePhone
      ,WorkPhone
      ,ClinicCode
      ,CommentClinical
      ,ResCode
      ,ApptPrimaryProcedureCode

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
)


select
       SourcePatientNo
      ,SourceEntityRecno
      ,CensusDate
      ,DistrictNo
      ,CasenoteNumber
      ,ReferralDate
	  ,convert(smalldatetime, BookedDate + ' ' + case when BookedTime = '24:00' then '23:59' else BookedTime end) BookedDate
      ,AppointmentDate
      ,PatientForename
      ,PatientSurname
      ,Postcode
      ,EpisodicGpCode
      ,EpisodicGpPracticeCode
      ,ConsultantCode
      ,SpecialtyCode
      ,SourceOfReferralCode
      ,AppointmentTypeCode
      ,AppointmentStatusCode
      ,CancelledBy
      ,AppointmentCategoryCode
      ,DateOfBirth
      ,QM08StartWaitDate
      ,QM08EndWaitDate
      ,PurchaserCode
      ,AppointmentTime
      ,SiteCode
      ,PriorityCode
      ,SexCode
      ,HomePhone
      ,WorkPhone
      ,ClinicCode
      ,CommentClinical
      ,ResCode
      ,ApptPrimaryProcedureCode

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
from
	dbo.TLoadOPWaiter WL

/*
where
	not exists
	(
	select
		1
	from
		dbo.TLoadOPWaiter LatestBooked
	where
		LatestBooked.SourcePatientNo = WL.SourcePatientNo
	and	LatestBooked.SourceEntityRecno = WL.SourceEntityRecno
	and
		(
			convert(smalldatetime, LatestBooked.BookedDate + ' ' + LatestBooked.BookedTime) < convert(smalldatetime, WL.BookedDate + ' ' + WL.BookedTime)
		or
			(
				convert(smalldatetime, LatestBooked.BookedDate + ' ' + LatestBooked.BookedTime) = convert(smalldatetime, WL.BookedDate + ' ' + WL.BookedTime)
			and	LatestBooked.EncounterRecno < WL.EncounterRecno
			)
		)
	)
*/



SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())
SELECT @Stats = 'Inserted '  + CONVERT(varchar(10), @RowsInserted)
	+ ', Deleted '  + CONVERT(varchar(10), @RowsDeleted)
	+ ', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @censusDate)
EXEC WriteAuditLogEvent 'LoadOPWaiter', @Stats
print @Stats


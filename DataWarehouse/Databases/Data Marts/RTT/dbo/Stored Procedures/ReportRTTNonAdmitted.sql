﻿CREATE procedure [dbo].[ReportRTTNonAdmitted]
	 @CensusDate smalldatetime
	,@SpecialtyCode varchar (10) = null
	,@SiteCode varchar (5) = null
	,@ConsultantCode varchar (20) = null
	,@PCTCode varchar (5) = null
	,@RTTStatusCode varchar (10) = null
	,@ReviewedFlag bit = null
	,@Filter varchar(20) = null

as

--shows pending non-admitted patients
select
	 RTTFact.CensusDate
	,Referral.EncounterRecno ReferralRecno
	,Referral.RTTPathwayID
	,RTTFact.SiteCode
	,Site.Site
	,Referral.DistrictNo
	,RTTFact.SpecialtyCode
	,Specialty.Specialty
	,RTTFact.ConsultantCode
	,Consultant.Consultant
	,Referral.PatientSurname
	,Referral.DateOfBirth
	,RTTFact.PCTCode
	,PCT.Organisation PCT
	,RTTFact.ReferralDate
	,Referral.RTTStartDate
	,Referral.RTTCurrentStatusCode
	,RTTStatus.RTTStatus
	,Referral.RTTCurrentStatusDate
	,RTTFact.DaysWaiting
	,RTTFact.DaysWaiting / 7 WeeksWaiting
	,null FuturePatientCancellationDate
	,RTTFact.RTTBreachDate BreachDate

	,FirstAppointmentDate =
	(
	select top 1
		Outpatient.AppointmentDate
	from
		dbo.Outpatient
	where
		Outpatient.SourcePatientNo = Referral.SourcePatientNo
	and	Outpatient.SourceEncounterNo2 = Referral.SourceEncounterNo2
	order by
		Outpatient.AppointmentDate asc
	) 

	,FirstOutcomeCode =
	(
	select top 1
		Outpatient.AttendanceOutcomeCode
	from
		dbo.Outpatient
	where
		Outpatient.SourcePatientNo = Referral.SourcePatientNo
	and	Outpatient.SourceEncounterNo2 = Referral.SourceEncounterNo2
	order by
		Outpatient.AppointmentDate asc
	) 

	,PreviousAppointmentDate =
	(
	select top 1
		Outpatient.AppointmentDate
	from
		dbo.Outpatient
	where
		Outpatient.SourcePatientNo = Referral.SourcePatientNo
	and	Outpatient.SourceEncounterNo2 = Referral.SourceEncounterNo2
	and	Outpatient.AppointmentDate <= RTTFact.CensusDate
	order by
		Outpatient.AppointmentDate desc
	) 

	,PreviousOutcomeCode =
	(
	select top 1
		Outpatient.AttendanceOutcomeCode
	from
		dbo.Outpatient
	where
		Outpatient.SourcePatientNo = Referral.SourcePatientNo
	and	Outpatient.SourceEncounterNo2 = Referral.SourceEncounterNo2
	and	Outpatient.AppointmentDate <= RTTFact.CensusDate
	order by
		Outpatient.AppointmentDate desc
	)

	,Referral.NextFutureAppointmentDate

	,RTTFact.RTTBreachDate

	,MaintenanceURL =
	(
	select
		TextValue
	from
		dbo.Parameter
	where
		Parameter = 'OPMAINTENANCEURL'
	)
	+ '?SourcePatientNo=' + Referral.SourcePatientNo
	+ '&SourceEntityRecno=' + Referral.SourceEncounterNo2
	+ '&CensusDate=' + convert(varchar, RTTFact.CensusDate)

	,WaitReason.WaitReason

	,RTTOPClockStop.WaitReasonDate

	,RTTOPClockStop.ClockStopComment

	,Referral.SourceOfReferralCode

from
	dbo.RTTFact

inner join dbo.Referral
on	Referral.EncounterRecno = RTTFact.EncounterRecno

left join dbo.OlapPractice Practice
on	Practice.OrganisationCode = coalesce(Referral.EpisodicGpPracticeCode, Referral.RegisteredGpPracticeCode)

left join dbo.PostcodePCTMap
on	PostcodePCTMap.Postcode = Referral.Postcode

left join dbo.OlapConsultant Consultant
on	Consultant.ConsultantCode = RTTFact.ConsultantCode

left join dbo.OlapSpecialty Specialty
on	Specialty.SpecialtyCode = RTTFact.SpecialtyCode

left join dbo.OlapPCT PCT
on	PCT.OrganisationCode = RTTFact.PCTCode

left join dbo.OlapSite Site
on	Site.SiteCode = RTTFact.SiteCode

left join dbo.RTTStatus
on	RTTStatus.RTTStatusCode = Referral.RTTCurrentStatusCode

left join dbo.RTTOPEncounter
on	RTTOPEncounter.SourcePatientNo = Referral.SourcePatientNo
and	RTTOPEncounter.SourceEntityRecno = Referral.SourceEncounterNo2

left join 
	(
	select
		*
	from
		dbo.RTTOPClockStop
	where
		not exists
		(
		select
			1
		from
			dbo.RTTOPClockStop LatestRTTOPClockStop
		where
			LatestRTTOPClockStop.SourcePatientNo = RTTOPClockStop.SourcePatientNo
		and	LatestRTTOPClockStop.SourceEntityRecno = RTTOPClockStop.SourceEntityRecno
		and	LatestRTTOPClockStop.Created > RTTOPClockStop.Created
		)
	) RTTOPClockStop
on	RTTOPClockStop.SourcePatientNo = Referral.SourcePatientNo
and	RTTOPClockStop.SourceEntityRecno = Referral.SourceEncounterNo2

left join dbo.WaitReason
on	WaitReason.WaitReasonCode = RTTOPClockStop.WaitReasonCode

where
	RTTFact.WorkflowStatusCode = 'P'
and	RTTFact.AdjustedFlag = 'Y'
and	RTTFact.EncounterTypeCode = 'REFERRAL'
and	RTTStatus.ClockStopFlag = 0

and	RTTFact.CensusDate = @CensusDate

and	(
		RTTFact.SpecialtyCode = @SpecialtyCode
	or	coalesce(@SpecialtyCode, '0') = '0'
	)

and	(
		RTTFact.SiteCode = @SiteCode
	or	coalesce(@SiteCode, '0') = '0'
	)

and	(
		RTTFact.ConsultantCode = @ConsultantCode
	or	coalesce(@ConsultantCode, '0') = '0'
	)

and	(
		RTTFact.PCTCode = @PCTCode
	or	coalesce(@PCTCode, '0') = '0'
	)

and	(
		Referral.RTTCurrentStatusCode = @RTTStatusCode
	or	@RTTStatusCode is null
	)

and	(
		coalesce(RTTOPEncounter.ReviewedFlag, convert(bit, 0)) = @ReviewedFlag
	or	@ReviewedFlag is null
	)

and	(
		(
			RTTFact.DaysWaiting > 126
		and	@Filter = 'LONGWAITER'
		)
		or	@Filter is null
	)

--and	Referral.ReferralDate >= '1 jan 2007'

order by
	RTTFact.RTTBreachDate


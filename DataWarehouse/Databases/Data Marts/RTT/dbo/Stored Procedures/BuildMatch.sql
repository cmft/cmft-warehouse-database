﻿CREATE procedure [dbo].[BuildMatch]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

select
	@fromDate =
		coalesce(
			 @fromDate
			,(select DateValue from dbo.Parameter where Parameter = 'RTTSTARTDATE')
		)

select @toDate = coalesce(@toDate, dateadd(day, datediff(day, 0, getdate()), 0))


-- Build Work Tables
exec BuildWorkTables @fromDate, @toDate

-- Run Match Processes
exec MatchProcess 'ATTIPWL'
exec MatchProcess 'ATTSPELL'
--	exec MatchProcess 'SPELLOPWL'
exec MatchProcess 'REFSPELL'
exec MatchProcess 'REFIPWL'
--exec MatchProcess 'REFIPWLAll' -- 20140521 RR added, 20140522 This process is set up to pull the latest event but i want all events.  Don't want to change the MatchMasterProcess  so taking this bit out.
--exec MatchProcess 'ATTIPWLAll'	-- 20140521 RR added

-- Build Match Master
exec BuildMatchMaster @fromDate, @toDate





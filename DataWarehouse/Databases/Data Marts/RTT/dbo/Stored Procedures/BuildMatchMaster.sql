﻿CREATE   procedure [dbo].[BuildMatchMaster] @fromDate smalldatetime, @toDate smalldatetime as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

declare @fDate smalldatetime
declare @tDate smalldatetime

select @fDate = @fromDate
select @tDate = @toDate


--CCB 2012-06-15
--take a snapshot of the merge results for auditing of multiple-entity matches

declare @SnapshotTime smalldatetime = sysdatetime()

truncate table MergeSnapshot

INSERT INTO dbo.MergeSnapshot
(
	 SnapshotTime
	,SourceRecno
	,MatchRecno
	,MergeTemplateRecno
)

select
	 SnapshotTime = @SnapshotTime
	,SourceRecno
	,MatchRecno
	,MergeTemplateRecno
from
	MergeData


--this removes matches where >1 referral has been linked to a single match entity e.g. >1 referral to same spell 
delete
from
	dbo.MergeData

from
	dbo.MergeData

inner join dbo.MergeTemplate
on	MergeData.MergeTemplateRecno = MergeTemplate.MergeTemplateRecno

inner join dbo.MergeDataset
on	MergeDataset.MergeDatasetCode = MergeTemplate.MergeDatasetCode

where
	exists
	(
	select
		1
	from
		dbo.MergeData b

	inner join dbo.MergeTemplate mt
	on	b.MergeTemplateRecno = mt.MergeTemplateRecno

	inner join dbo.MergeDataset md
	on	mt.MergeDatasetCode = md.MergeDatasetCode

	where
		b.MatchRecno = MergeData.MatchRecno
	and	(
			md.Priority < MergeDataset.Priority
		or
			(
				md.Priority = MergeDataset.Priority
			and	mt.Priority < MergeTemplate.Priority
			)
		or
			(
				md.Priority = MergeDataset.Priority
			and	mt.Priority = MergeTemplate.Priority
			and	b.MergeDataRecno < MergeData.MergeDataRecno
			)
		)

	and	exists
		(
		select
			1
		from
			(
			select
				 MatchRecno
				,count(*) total
			from
				MergeData c
			group by
				 MatchRecno
			having
				count(*) > 1
			) DistinctList
		where
			b.MatchRecno = DistinctList.MatchRecno
		)
	)


delete from MatchMaster
where
	CensusDate = @tDate

select @RowsDeleted = @@rowcount


insert into MatchMaster
(
	 [CensusDate]
	,[ReferralRecno]
	,[OutpatientRecno]
	,[InpatientWLRecno]
	--,InpatientWLAllRecno -- RR added 20140521
	,[InpatientRecno]
	,[FutureOPRecno]
	--,[IPToFutureOPRecno]
	,[AttIPWLTemplateRecno]
	--,AttIPWLAllTemplateRecno -- RR added 20140521
	,[AttSpellTemplateRecno]
	,[SpellOPWLTemplateRecno]
	,RefIPWLTemplateRecno
	--,RefIPWLAllTemplateRecno -- RR added 20140521
	,RefSpellTemplateRecno
)
select
	@tDate CensusDate
	,Referral.EncounterRecno ReferralRecno
	,Outpatient.EncounterRecno OutpatientRecno

	,InpatientWLRecno =
	coalesce(
--20090924 do not default to inpatient recno!
--		 InpatientByEncounter.EncounterRecno
		 REFIPWL.MatchRecno	 -- 20140521 RR swapped these around
		 ,InpatientByWL.EncounterRecno
		--,REFIPWL.MatchRecno
		,InpatientWL.EncounterRecno 
	)
----
--20140521 RR
	--,InpatientWLAllRecno =
	--coalesce(
	--	 REFIPWLAll.MatchRecno
	--	 ,InpatientbyWLAll.EncounterRecno
	--	,InpatientWLAll.EncounterRecno 
	--)

----
	,InpatientRecno = coalesce(InpatientByEncounter.EncounterRecno, InpatientByWL.EncounterRecno, REFSPELL.MatchRecno)

	,null FutureOPRecno

	--,IPToFutureOPRecno = IPToFutureOP.EncounterRecno 

	,AttIPWLTemplateRecno = ATTIPWL.MergeTemplateRecno 
	--,AttIPWLAllTemplateRecno  = ATTIPWLAll.MergeTemplateRecno -- RR added 20140521
	,AttSpellTemplateRecno = ATTSPELL.MergeTemplateRecno 
	,SpellOPWLTemplateRecno = SPELLOPWL.MergeTemplateRecno 
	,RefIPWLTemplateRecno = REFIPWL.MergeTemplateRecno
	--,RefIPWLAllTemplateRecno = REFIPWLAll.MergeTemplateRecno -- RR added 20140521
	,RefSpellTemplateRecno = REFSPELL.MergeTemplateRecno
from
	dbo.WkReferral Referral

-- link to latest O/P
left join dbo.WkOutpatient Outpatient
on	Outpatient.SourcePatientNo = Referral.SourcePatientNo
and	Outpatient.SourceEncounterNo2 = Referral.SourceEncounterNo2

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'ATTIPWL'
		)
	and	MergeData.MatchRecno is not null

	) ATTIPWL
on	ATTIPWL.SourceRecno = Outpatient.EncounterRecno

left join dbo.WkInpatientWL InpatientWL
on	InpatientWL.EncounterRecno = ATTIPWL.MatchRecno

left join dbo.WkSpell InpatientByWL
on	InpatientByWL.SourcePatientNo = InpatientWL.SourcePatientNo
and	InpatientByWL.SourceEncounterNo = InpatientWL.SourceEntityRecno

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'REFIPWL'
		)
	and	MergeData.MatchRecno is not null

	) REFIPWL
on	REFIPWL.SourceRecno = Referral.EncounterRecno


-------------------------------
--20140521 RR added the following
--left join
--	(
--	select
--		SourceRecno,
--		MatchRecno,
--		MergeTemplateRecno
--	from
--		dbo.MergeData
--	where
--		exists
--		(
--		select
--			1
--		from
--			dbo.MergeTemplate
--		where
--			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
--		and	MergeTemplate.MergeDatasetCode = 'ATTIPWLAll'
--		)
--	and	MergeData.MatchRecno is not null

--	) ATTIPWLAll
--on	ATTIPWLAll.SourceRecno = Outpatient.EncounterRecno

--left join dbo.WkInpatientWLAll InpatientWLAll
--on	InpatientWLAll.EncounterRecno = ATTIPWL.MatchRecno

--left join dbo.WkSpell InpatientByWLAll
--on	InpatientByWLAll.SourcePatientNo = InpatientWLAll.SourcePatientNo
--and	InpatientByWLAll.SourceEncounterNo = InpatientWLAll.SourceEntityRecno

--left join
--	(
--	select
--		SourceRecno,
--		MatchRecno,
--		MergeTemplateRecno
--	from
--		dbo.MergeData
--	where
--		exists
--		(
--		select
--			1
--		from
--			dbo.MergeTemplate
--		where
--			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
--		and	MergeTemplate.MergeDatasetCode = 'REFIPWLAll'
--		)
--	and	MergeData.MatchRecno is not null

--	) REFIPWLAll
--on	REFIPWLAll.SourceRecno = Referral.EncounterRecno



--------------------------------

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'ATTSPELL'
		)
	and	MergeData.MatchRecno is not null

	) ATTSPELL
on	ATTSPELL.SourceRecno = Outpatient.EncounterRecno

left join dbo.WkSpell InpatientByEncounter
on	InpatientByEncounter.EncounterRecno = ATTSPELL.MatchRecno

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'SPELLOPWL'
		)
	and	MergeData.MatchRecno is not null

	) SPELLOPWL
on	SPELLOPWL.SourceRecno = coalesce(InpatientByEncounter.EncounterRecno, InpatientByWL.EncounterRecno)

--left join dbo.OPWaiter IPToFutureOP
--on	IPToFutureOP.EncounterRecno = SPELLOPWL.MatchRecno

left join
	(
	select
		SourceRecno,
		MatchRecno,
		MergeTemplateRecno
	from
		dbo.MergeData
	where
		exists
		(
		select
			1
		from
			dbo.MergeTemplate
		where
			MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno
		and	MergeTemplate.MergeDatasetCode = 'REFSPELL'
		)
	and	MergeData.MatchRecno is not null

	) REFSPELL
on	REFSPELL.SourceRecno = Referral.EncounterRecno

where
	Referral.ReferralDate between @fDate and @tDate
--order by
--	Referral.EncounterRecno

SELECT @RowsInserted = @@ROWCOUNT

-- 7 Feb 2008 PDO
-- Update any known link between referral and OP WL
update
	MatchMaster
set
	FutureOPRecno = FutureOP.EncounterRecno
from
	MatchMaster

inner join dbo.WkReferral Referral
on	Referral.EncounterRecno = MatchMaster.ReferralRecno

inner join WkOutpatientWL FutureOP
on	FutureOP.SourcePatientNo = Referral.SourcePatientNo
and	left(FutureOP.SourceEntityRecno, len(Referral.SourceEncounterNo2)) = Referral.SourceEncounterNo2
--and	FutureOP.SourceEntityRecno = Referral.SourceEncounterNo2

where
	MatchMaster.CensusDate = @tDate


--select
--	@tDate CensusDate,
--	SourcePatientNo,
--	SourceEntityRecno,
--	EncounterRecno
--into
--	#FutureOP
--from
--	dbo.OPWaiterLatest
--where
--	CensusDate = 
--	(
--	select
--		max(CensusDate)
--	from
--		dbo.OPWaiter Snapshot
--	where
--		Snapshot.CensusDate <= @tDate
--	)
--
--alter CLUSTERED INDEX #IX ON #FutureOP
--	(
--	CensusDate,
--	SourcePatientNo,
--	SourceEntityRecno
--	)
--
--update
--	MatchMaster
--set
--	FutureOPRecno = FutureOP.EncounterRecno
--from
--	MatchMaster
--
--inner join dbo.WkReferral Referral
--on	Referral.EncounterRecno = MatchMaster.ReferralRecno
--
--inner join #FutureOP FutureOP
--on	FutureOP.SourcePatientNo = Referral.SourcePatientNo
--and	FutureOP.SourceEntityRecno = Referral.SourceEncounterNo2
--and	FutureOP.CensusDate = MatchMaster.CensusDate


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'From date ' + CONVERT(varchar, @fromDate, 103) + ', '  + 
	'To date ' + CONVERT(varchar, @toDate, 103) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'
EXEC WriteAuditLogEvent 'BuildMatchMaster', @Stats


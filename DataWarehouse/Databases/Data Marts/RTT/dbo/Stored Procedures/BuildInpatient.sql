﻿CREATE procedure [dbo].[BuildInpatient] as

truncate table dbo.Inpatient

--alter table inpatient alter column WaitingListCode nvarchar(50)  -- 20140508 RR

insert into dbo.Inpatient
(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode
	,EpisodicGpPracticeCode
	,PCTCode
	,LocalityCode
	,Created
	,Updated
	,ByWhom
	,EncounterStartDate
	,HospitalCode
	,SourceEncounterNo2
	,StartWardType
	,EndWardType
	,PatientCategoryCode
	,CountOfDaysWaiting
	,Address1
	,Address2
	,Address3
	,Address4
	,Country
	,PriorityCode
	,StartDirectorateCode
	,EndDirectorateCode
	,WaitingListCode
)
select
	 EncounterRecno = EncounterRecnoMap.EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode
	,EpisodicGpPracticeCode
	,PCTCode
	,LocalityCode
	,Created
	,Updated
	,ByWhom

	,EncounterStartDate = AdmissionDate
	,HospitalCode = SiteCode
	,SourceEncounterNo2 = SourceSpellNo
	,StartWardType = StartWardTypeCode
	,EndWardType = EndWardTypeCode
	,PatientCategoryCode = AdminCategoryCode
	,CountOfDaysWaiting = null
	,Address1 = PatientAddress1
	,Address2 = PatientAddress2
	,Address3 = PatientAddress3
	,Address4 = PatientAddress4
	,Country = null
	,PriorityCode = null

	,StartDirectorateCode
	,EndDirectorateCode
	,WaitingListCode
from
	Warehouse.APC.Encounter Encounter with (nolock)
inner join dbo.EncounterRecnoMap
on	EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
and	EncounterRecnoMap.EncounterTypeCode = 'IP'

--where
--	Encounter.AdmissionDate >= (select DateValue from dbo.Parameter where Parameter = 'RTTSTARTDATE')



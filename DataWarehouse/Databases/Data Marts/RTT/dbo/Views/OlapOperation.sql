﻿
CREATE view [dbo].[OlapOperation] as

select
	 OperationCode
	,Operation =
		OperationCode + ' - ' + Operation
from
	WarehouseSQL.Warehouse.PAS.Operation

union all

select distinct
	RTTFact.PrimaryOperationCode
	,Operation = 
		rtrim(RTTFact.PrimaryOperationCode) + ' - No Description'
from
	dbo.RTTFact
where
	not exists
	(
	select
		1
	from
		WarehouseSQL.Warehouse.PAS.Operation Operation
	where
		Operation.OperationCode = RTTFact.PrimaryOperationCode
	)
and	RTTFact.PrimaryOperationCode is not null



﻿CREATE view [dbo].[Match4RTTPathway] AS

SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkSpell.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkSpell ON 
	WkReferral.SourcePatientNo = WkSpell.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkSpell.SourceEncounterNo
AND	WkReferral.RTTPathwayID = WkSpell.RTTPathwayID

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match4RTTPathway'
and	MergeTemplate.Active = 1


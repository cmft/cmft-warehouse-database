﻿CREATE view [dbo].[RTTGUM] as

select
	 EncounterRecno
	,EncounterTypeCode = 'GUM'
	,ConsultantCode = '##'
	,SpecialtyCode = 'GUM'
	,PracticeCode = '##'
	,PCTCode
	,SiteCode
	,WeekBandCode = '00'
	,PathwayStatusCode = 'OPT'
	,ReferralDate = TreatmentDate
	,WorkflowStatusCode = 'C'
	,BreachBandCode = 'F99'
	,DaysWaiting = 0
	,Cases = Patients
	,TCIBeyondBreach = 0
	,TreatmentDate
	,PrimaryDiagnosisCode = 'N/A'
	,PrimaryOperationCode = 'N/A'
	,ReportableFlag = 'Y'
from
	dbo.GUM

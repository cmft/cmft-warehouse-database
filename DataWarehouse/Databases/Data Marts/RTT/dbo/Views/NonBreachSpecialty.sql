﻿create view [dbo].[NonBreachSpecialty] as

select
	SpecialtyCode = EntityCode
from
	dbo.EntityLookup
where
	EntityTypeCode = 'NONBREACHSPECIALTY'


﻿CREATE  view [dbo].[InpatientBase] as

select
	*

	,EncounterStartDate = AdmissionDate
	,HospitalCode = SiteCode
	,SourceEncounterNo2 = SourceSpellNo
	,StartWardType = StartWardTypeCode
	,EndWardType = EndWardTypeCode
	--,PatientCategoryCode = AdminCategoryCode
	,CountOfDaysWaiting = null
	,Address1 = PatientAddress1
	,Address2 = PatientAddress2
	,Address3 = PatientAddress3
	,Address4 = PatientAddress4
	,Country = null
	,PriorityCode = null
from
	Warehouse.APC.Encounter Encounter


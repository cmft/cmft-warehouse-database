﻿CREATE view [dbo].[OlapAdjusted] as

select
	'Y' AdjustedFlag
	,'Yes' Adjusted

union all

select
	'N' AdjustedFlag
	,'No' Adjusted


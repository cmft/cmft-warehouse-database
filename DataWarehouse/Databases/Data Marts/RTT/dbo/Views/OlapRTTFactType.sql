﻿create view [dbo].[OlapRTTFactType] as

SELECT
	 RTTFactTypeCode
	,RTTFactType
	,RTTFactTypeDesc
FROM
	RTTFactType


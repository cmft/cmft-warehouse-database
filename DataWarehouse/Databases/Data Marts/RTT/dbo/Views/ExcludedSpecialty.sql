﻿create view [dbo].[ExcludedSpecialty] as

select
	SpecialtyCode = EntityCode
from
	dbo.EntityLookup
where
	EntityTypeCode = 'EXCLUDEDSPECIALTY'


﻿/****** Script for SelectTopNRows command from SSMS  ******/
create view [dbo].[MatchResult] as

SELECT
	 MergeData.MergeDataRecno
	,MergeData.SourceRecno
	,MergeData.MatchRecno
	,MergeTemplate.MergeTemplateCode
	,MergeTemplate.MergeTemplate
	,MergeTemplatePriority = MergeTemplate.Priority
	,MergeTemplate.Active
	,MergeDataset.MergeDatasetCode
	,MergeDataset.MergeDataset
	,MergeDataPriority = MergeDataset.Priority
FROM
	MergeData

inner join MergeTemplate
on	MergeTemplate.MergeTemplateRecno = MergeData.MergeTemplateRecno

inner join MergeDataset
on	MergeDataset.MergeDatasetCode = MergeTemplate.MergeDatasetCode


﻿create VIEW [dbo].[Match2RTTPathway] AS

SELECT  
	WkSpell.EncounterRecno SourceRecno,
	WkOutpatientWL.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkSpell

inner join dbo.WkOutpatientWL ON 
	WkSpell.SourcePatientNo = WkOutpatientWL.SourcePatientNo
AND	WkSpell.RTTPathwayID = WkOutpatientWL.RTTPathwayID

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match2RTTPathway'
and	MergeTemplate.Active = 1


﻿
CREATE view [dbo].[RTTFactUnknownClockStartBase] as

-- Patients admitted for which we have no referral link

select
	OlapSnapshot.CensusDate,

	Inpatient.EncounterRecno ReferralRecno, 
	'UNKIP' EncounterTypeCode, -- unknown clock start inpatients

	coalesce(Inpatient.ConsultantCode, '##') ConsultantCode,
	coalesce(Inpatient.SpecialtyCode, '##') SpecialtyCode,

	coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode, '##') PracticeCode,

	coalesce(
		Practice.ParentOrganisationCode,
		PostcodePCTMap.PCTCode,
		'5J5'
	) PCTCode,

	coalesce(Inpatient.HospitalCode, '##') SiteCode,

	'53' WeekBandCode,

	'ADM' PathwayStatusCode,

	Inpatient.EncounterStartDate,

	'C' WorkflowStatusCode,

	'UK' BreachBandCode, -- patients with unknown clock start date

	0 DaysWaiting,
	1 Cases,

	0 TCIBeyondBreach,

	case
	when datediff(day, dateadd(day, -1, -- take a day off 
		Inpatient.EncounterStartDate -- Inpatient Admission
		), 
		OlapSnapshot.CensusDate
		) < 8 then 1 -- 14 Jan 2008 - PDO - changed as census run moved from Monday to Sunday
	else 0
	end LastWeeksCases

	,coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A') PrimaryDiagnosisCode
	,coalesce(Inpatient.PrimaryOperationCode, 'N/A') PrimaryOperationCode

	,ReportableFlag = 'Y'

	,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

	,RTTFactTypeCode = 6

from
	Inpatient

left join dbo.OlapPractice Practice
on	Practice.OrganisationCode = coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode)

left join dbo.PostcodePCTMap
on	PostcodePCTMap.Postcode = Inpatient.Postcode

inner join dbo.OlapSnapshot
on	Inpatient.EncounterStartDate between dateadd(month, -1, OlapSnapshot.CensusDate) and OlapSnapshot.CensusDate

where
	Inpatient.EncounterStartDate between '1 jan 2007' and OlapSnapshot.CensusDate
and	not exists
	(
	select
		1
	from
		dbo.MatchMaster
	where
		MatchMaster.InpatientRecno = Inpatient.EncounterRecno
	and	MatchMaster.CensusDate = OlapSnapshot.CensusDate
	)
and	datediff(month, Inpatient.EncounterStartDate, OlapSnapshot.CensusDate) = 0

--if we have an override clock start then do not include in unknown clock starts
and	not exists
	(
	select
		1
	from
		dbo.RTTClockStart
	where
		RTTClockStart.SourcePatientNo = Inpatient.SourcePatientNo
	and	RTTClockStart.SourceEntityRecno = Inpatient.SourceEncounterNo2
	)

and	Inpatient.RTTPathwayID is null



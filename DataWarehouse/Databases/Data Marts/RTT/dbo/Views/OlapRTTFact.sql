﻿

CREATE view [dbo].[OlapRTTFact] as

SELECT 
	 RTTFact.[CensusDate]
	,[EncounterRecno]
	,[EncounterTypeCode]
	,[ConsultantCode]
	,[SpecialtyCode]
	,[PracticeCode]
	,[PCTCode]
	,[SiteCode]
	,[WeekBandCode]
	,[PathwayStatusCode]
	,[ReferralDate]
	,[WorkflowStatusCode]
	,[BreachBandCode]
	,[DaysWaiting]
	,[Cases]
	,[TCIBeyondBreach]
	,[LastWeeksCases]
	,[PrimaryDiagnosisCode]
	,[PrimaryOperationCode]
	,ReportableFlag
	,AdjustedFlag
	,RTTBreachDate
	,ReferringProviderCode
	,KeyDate
FROM
	[dbo].[RTTFact]

inner join dbo.OlapSnapshot
on	OlapSnapshot.CensusDate = RTTFact.CensusDate

--load last 6 weeks snapshot only
where
	RTTFact.CensusDate >= 
		(
		select
			dateadd(day, -56, max(CensusDate))
		from
			dbo.OlapSnapshot
		)




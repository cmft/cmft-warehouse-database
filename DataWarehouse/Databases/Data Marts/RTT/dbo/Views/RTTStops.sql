﻿




CREATE View [dbo].[RTTStops] As

select 
	D.NationalSpecialtyCode
	,D.Census
	,D.SourcePatientNo
	,D.DistrictNo
	,D.RTTPathwayID
	,D.RTTpathwaycondition
	,D.ConsultantCode
	,D.SpecialtyCode
	,StartEncounterNo = E.SourceEncounterNo
	,StartID
	,StartDate
	,StartReason
	,StopEncounterNo = D.SourceEncounterNo
	,StopID
	,StopDate = D.EventDate 
	,StopReason = D.EventType 
	,EventType = case 
					when left(D.EventType,4) = 'IPAd' and D.DisposalReason = 'Treatment' 
					then 'APC' 
					else 'NAPC' 
				end 
	,RTTDaysWaiting = datediff(day,StartDate,D.EventDate) 
	,RTTWeeksWaiting = (datediff(day,StartDate,D.EventDate)/7) 
	,Category = 
				case 
					when (datediff(day,StartDate,D.EventDate))<='126' then '18wks and Under' 
					else 'Over18wks' 
				end
	,D.DisposalCode
	,D.DisposalReason
	,D.RTTcurrentStatusCode
	,D.EventStatus
	,D.Reportable
from 
	(select 
		A.RTTPathwayID
		,StartID = A.OrderID 
		,[StartDate]
		,[StartReason]
		,StopID = min(B.OrderID)
	from 
		(select 
			RTTPathwayID
			,OrderID
			,StartDate = EventDate
			,StartReason = EventType
		from 
			RTTGynaePathways 
		where 
			PathwayStatus = 'Start') A
	
	inner join
		(select 
			RTTPathwayID
			,OrderID
		from 
			RTTGynaePathways 
		where 
			PathwayStatus = 'Stop') B
	
	on A.RTTPathwayID = B.RTTPathwayID
	
	where 
		B.OrderID>A.OrderID	
	group by 
		A.RTTPathwayID
		,A.OrderID
		,StartDate
		,StartReason
	) C

inner join
	RTTGynaePathways D 
on D.RTTPathwayID = C.RTTPathwayID 
	and D.OrderID = C.StopID

inner join
	RTTGynaePathways E 
on E.RTTPathwayID = C.RTTPathwayID 
	and E.OrderID = C.StartID






﻿CREATE VIEW [dbo].[Match3SpecWithin22to28Days] AS
SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkSpell.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkSpell ON 
	WkOutpatient.SourcePatientNo = WkSpell.SourcePatientNo
--AND	WkOutpatient.HospitalCode = WkSpell.HospitalCode
--AND	WkOutpatient.ConsultantCode = WkSpell.ConsultantCode
AND	WkOutpatient.SpecialtyCode = WkSpell.SpecialtyCode
and	DATEDIFF(day, WkOutpatient.AppointmentDate, WkSpell.DateOnWaitingList) BETWEEN 22 AND 28

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match3SpecWithin22to28Days'
and	MergeTemplate.Active = 1


﻿CREATE view [dbo].[OlapPathwayStatus] as
select
	PathwayStatus.PathwayStatusCode,
	PathwayStatus.PathwayStatus,
	NationalPathwayStatus.NationalPathwayStatusCode,
	NationalPathwayStatus.NationalPathwayStatus
from
	dbo.PathwayStatus

inner join dbo.NationalPathwayStatus
on	NationalPathwayStatus.NationalPathwayStatusCode = PathwayStatus.NationalPathwayStatusCode


﻿CREATE view [dbo].[Match5RTTPathway] AS

SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkInpatientWL.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkInpatientWL ON 
	WkReferral.SourcePatientNo = WkInpatientWL.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkInpatientWL.SourceEntityRecno
AND	WkReferral.RTTPathwayID = WkInpatientWL.RTTPathwayID

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match5RTTPathway'
and	MergeTemplate.Active = 1


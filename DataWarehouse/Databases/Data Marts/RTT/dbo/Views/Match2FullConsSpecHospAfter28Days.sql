﻿CREATE VIEW [dbo].[Match2FullConsSpecHospAfter28Days] AS

SELECT  
	WkSpell.EncounterRecno SourceRecno,
	WkOutpatientWL.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkSpell

inner join dbo.WkOutpatientWL ON 
	WkSpell.SourcePatientNo = WkOutpatientWL.SourcePatientNo
AND	WkSpell.HospitalCode = WkOutpatientWL.HospitalCode
AND	WkSpell.ConsultantCode = WkOutpatientWL.ConsultantCode
AND	WkSpell.SpecialtyCode = WkOutpatientWL.SpecialtyCode
and	DATEDIFF(day, WkSpell.EncounterStartDate, WkOutpatientWL.ReferralDate) > 28

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match2FullConsSpecHospAfter28Days'
and	MergeTemplate.Active = 1


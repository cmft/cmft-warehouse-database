﻿
CREATE view [dbo].[SocialSuspension] as

select
	 SourcePatientNo
	,SourceEntityRecno = SourceEncounterNo
	,CensusDate = DATEADD(day , 1 , CensusDate) -- 2010-04-26 CCB - WL snapshots now generated on previous day
	,sum(datediff(day, SuspensionStartDate, SuspensionEndDate)) DaysSuspended
from
	WarehouseSQL.Warehouse.APC.WaitingListSuspension Suspension
where
	upper(SuspensionReason) like '%S*%'
group by
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate



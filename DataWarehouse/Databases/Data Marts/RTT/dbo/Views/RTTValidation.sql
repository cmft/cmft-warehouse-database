﻿


CREATE View [dbo].[RTTValidation]

as


Select 
	RTTGynaePathways.Census
	,NationalSpecialtyCode
	,RTTGynaePathways.RTTPathwayID
	,EvRTTPathwayID
	,RTTPathwayCondition
	,DistrictNo
	,ConsultantCode
	,SpecialtyCode
	,SourceofReferralCode
	,EventDate
	,RTTGynaePathways.EventTime
	,RTTGynaePathways.EventType
	,ApptType
	,ClinicCode
	,RTTGynaePathways.OutcomeCode
	,DisposalCode
	,DisposalReason
	,EventStatus
	,OrderID
	,PathwayStatus
	,Reportable
	,OutcomeID
	,DateValidated
	,UserName
	,Comments
from
	RTTGynaePathways 

inner join RTTValidationArchive ValidationOutcome
on RTTGynaePathways.RTTPathwayID = ValidationOutcome.RTTPathwayID 
and RTTGynaePathways.EventTime = ValidationOutcome.EventTime
and RTTGynaePathways.EventType = ValidationOutcome.EventType
and 
	(
		RTTGynaePathways.OutcomeCode = ValidationOutcome.OutcomeCode
	or
		(
			RTTGynaePathways.OutcomeCode is null
		and ValidationOutcome.OutcomeCode is null
		)
	)

inner join RTT.ValidationOutcome ValidationDescription
on ValidationOutcome.OutcomeID = ValidationDescription.ID

--left join WarehouseOLAPMergedV2.Dictation.BaseDocument
--on  BaseDocument.SourcePatientNo = RTTGynaePathways.SourcePatientNo
--and BaseDocument.DictatedTime = RTTGynaePathways.EventDate
--and DocumentTypeCode = 'O' 


--order by
--	RTTGynaePathways.RTTPathwayID
--	,OrderID







﻿create VIEW [dbo].[Match3RTTPathway] AS

SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkSpell.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkSpell ON 
	WkOutpatient.SourcePatientNo = WkSpell.SourcePatientNo
AND	WkOutpatient.RTTPathwayID = WkSpell.RTTPathwayID

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match3RTTPathway'
and	MergeTemplate.Active = 1


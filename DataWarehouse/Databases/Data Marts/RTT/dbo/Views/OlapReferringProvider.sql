﻿
CREATE view [dbo].[OlapReferringProvider] as

select distinct
	 ReferringProviderCode = coalesce(RTTFact.ReferringProviderCode, '##')

	,ReferringProvider = 
		case
		when coalesce(RTTFact.ReferringProviderCode, '##') = '##' then 'Unknown'
		when RTTFact.ReferringProviderCode = 'X09' then 'Choose and Book'
		else coalesce(Provider.Organisation, RTTFact.ReferringProviderCode + ' - No Description')
		end
from
	dbo.RTTFact

left join WarehouseSQL.Organisation.dbo.Trust Provider
on	Provider.OrganisationCode = RTTFact.ReferringProviderCode



﻿CREATE VIEW [dbo].[MatchHospSpecWithin08to14Days] AS
SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkInpatientWL.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkInpatientWL ON 
	WkOutpatient.SourcePatientNo = WkInpatientWL.SourcePatientNo
AND	WkOutpatient.HospitalCode = WkInpatientWL.HospitalCode
--AND	WkOutpatient.ConsultantCode = WkInpatientWL.ConsultantCode
AND	WkOutpatient.SpecialtyCode = WkInpatientWL.SpecialtyCode
and	DATEDIFF(day, WkOutpatient.AppointmentDate, WkInpatientWL.DateOnWaitingList) BETWEEN 8 AND 14

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'MatchHospSpecWithin08to14Days'
and	MergeTemplate.Active = 1


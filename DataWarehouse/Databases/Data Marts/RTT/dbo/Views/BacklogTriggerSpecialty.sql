﻿create view [dbo].[BacklogTriggerSpecialty] as

--a list of specialties that will be automatically clock stopped either because they should never go over 18 weeks
--or should be reviewed as they arrive off the backlog
select
	SpecialtyCode = EntityCode
from
	dbo.EntityLookup
where
	EntityTypeCode = 'BACKLOGTRIGGERSPECIALTY'


﻿CREATE       view [dbo].[OlapDurationWeeks] as
select
	DurationWeek.EntityCode WaitDurationCode,
	DurationWeek.[Description] DurationWeek,
	DurationBand.EntityCode DurationBandCode,
	DurationBand.[Description] DurationBand,
	coalesce(DurationColour.[Description], 'White') CellColour
from
	EntityLookup DurationWeek

inner join EntityXref DurationWeekToBand
on	DurationWeekToBand.EntityTypeCode = DurationWeek.EntityTypeCode
and	DurationWeekToBand.XrefEntityTypeCode = 'OLAPDURATIONWEEKS.BAND'
and	DurationWeekToBand.EntityCode = DurationWeek.EntityCode

inner join EntityLookup DurationBand
on	DurationBand.EntityTypeCode = DurationWeekToBand.XrefEntityTypeCode
and	DurationBand.EntityCode = DurationWeekToBand.XrefEntityCode

left join EntityLookup DurationColour
on	DurationColour.EntityTypeCode = 'OLAPDURATIONWEEKS.COLOUR'
and	DurationColour.EntityCode = DurationWeekToBand.XrefEntityCode

where
	DurationWeek.EntityTypeCode = 'OLAPDURATIONWEEKS.WEEK'

/*
union all

select
	'NA' WaitDurationCode,
	'N/A' DurationMonth,
	'XBAND' DurationBandCode,
	'N/A' DurationBand,
	'LightGrey'
*/


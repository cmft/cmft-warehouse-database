﻿
create VIEW [dbo].[Match7RTTPathway] AS

SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkInpatientWLAll.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkInpatientWLAll ON 
	WkOutpatient.SourcePatientNo = WkInpatientWLAll.SourcePatientNo
AND	WkOutpatient.RTTPathwayID = WkInpatientWLAll.RTTPathwayID

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match7RTTPathway'
and	MergeTemplate.Active = 1



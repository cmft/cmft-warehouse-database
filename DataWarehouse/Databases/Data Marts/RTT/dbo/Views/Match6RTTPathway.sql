﻿
CREATE view [dbo].[Match6RTTPathway] AS

SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkInpatientWLAll.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkInpatientWLAll ON 
	WkReferral.SourcePatientNo = WkInpatientWLAll.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkInpatientWLAll.SourceEntityRecno
AND	WkReferral.RTTPathwayID = WkInpatientWLAll.RTTPathwayID

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match6RTTPathway'
and	MergeTemplate.Active = 1



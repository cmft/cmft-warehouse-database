﻿create view [dbo].[OlapReportable] as

select
	'Y' ReportableFlag
	,'Reportable' Reportable

union all

select
	'N' ReportableFlag
	,'Non-Reportable' Reportable


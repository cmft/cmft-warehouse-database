﻿CREATE view [dbo].[Match5HospConsWithin15to21Days] AS
SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkInpatientWL.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkInpatientWL ON 
	WkReferral.SourcePatientNo = WkInpatientWL.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkInpatientWL.SourceEntityRecno
AND	WkReferral.HospitalCode = WkInpatientWL.HospitalCode
AND	WkReferral.ConsultantCode = WkInpatientWL.ConsultantCode
--AND	WkReferral.SpecialtyCode = WkInpatientWL.SpecialtyCode
and	DATEDIFF(day, WkReferral.ReferralDate, WkInpatientWL.DateOnWaitingList) BETWEEN 15 AND 21

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match5HospConsWithin15to21Days'
and	MergeTemplate.Active = 1


﻿CREATE VIEW [dbo].[MatchHospConsAfter28Days] AS

SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkInpatientWL.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkInpatientWL ON 
	WkOutpatient.SourcePatientNo = WkInpatientWL.SourcePatientNo
AND	WkOutpatient.HospitalCode = WkInpatientWL.HospitalCode
AND	WkOutpatient.ConsultantCode = WkInpatientWL.ConsultantCode
--AND	WkOutpatient.SpecialtyCode = WkInpatientWL.SpecialtyCode
and	DATEDIFF(day, WkOutpatient.AppointmentDate, WkInpatientWL.DateOnWaitingList) > 28

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'MatchHospConsAfter28Days'
and	MergeTemplate.Active = 1


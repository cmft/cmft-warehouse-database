﻿CREATE VIEW [dbo].[Match3FullConsSpecHospWithin07Days] AS

SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkSpell.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkSpell ON 
	WkOutpatient.SourcePatientNo = WkSpell.SourcePatientNo
AND	WkOutpatient.HospitalCode = WkSpell.HospitalCode
AND	WkOutpatient.ConsultantCode = WkSpell.ConsultantCode
AND	WkOutpatient.SpecialtyCode = WkSpell.SpecialtyCode
and	DATEDIFF(day, WkOutpatient.AppointmentDate, WkSpell.DateOnWaitingList) BETWEEN 0 AND 7

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match3FullConsSpecHospWithin07Days'
and	MergeTemplate.Active = 1


﻿CREATE view [dbo].[OPWaiterLatest] as
select
       EncounterRecno
      ,SourcePatientNo
      ,SourceEntityRecno
      ,CensusDate
      ,DistrictNo
      ,CasenoteNumber
      ,ReferralDate
      ,BookedDate
      ,AppointmentDate
      ,PatientForename
      ,PatientSurname
      ,Postcode
      ,EpisodicGpCode
      ,EpisodicGpPracticeCode
      ,ConsultantCode
      ,SpecialtyCode
      ,SourceOfReferralCode
      ,AppointmentTypeCode
      ,AppointmentStatusCode
      ,CancelledBy
      ,AppointmentCategoryCode
      ,DateOfBirth
      ,QM08StartWaitDate
      ,QM08EndWaitDate
      ,PurchaserCode
      ,AppointmentTime
      ,SiteCode
      ,PriorityCode
      ,SexCode
      ,HomePhone
      ,WorkPhone
      ,ClinicCode
      ,CommentClinical
      ,ResCode
      ,ApptPrimaryProcedureCode
from
	dbo.OPWaiter WL with (nolock)

where
	WL.SpecialtyCode not in ('ANC', 'OBS', 'ANCO', 'AE', 'RADT', 'RAD', 'ITU', 'REHA', 'FRA') -- 'GENE' ?
and	not exists
	(
	select
		1
	from
		dbo.OPWaiter LatestBooked with (nolock)
	where
		LatestBooked.SourcePatientNo = WL.SourcePatientNo
	and	LatestBooked.SourceEntityRecno = WL.SourceEntityRecno
	and	LatestBooked.CensusDate = WL.CensusDate
	and
		(
			LatestBooked.BookedDate < WL.BookedDate
		or
			(
				LatestBooked.BookedDate = WL.BookedDate
			and	LatestBooked.EncounterRecno < WL.EncounterRecno
			)
		)
	)


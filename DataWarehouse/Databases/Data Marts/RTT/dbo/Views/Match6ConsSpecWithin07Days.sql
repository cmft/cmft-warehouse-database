﻿
CREATE view [dbo].[Match6ConsSpecWithin07Days] AS
SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkInpatientWLAll.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkInpatientWLAll ON 
	WkReferral.SourcePatientNo = WkInpatientWLAll.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkInpatientWLAll.SourceEntityRecno
--AND	WkReferral.HospitalCode = WkInpatientWL.HospitalCode
AND	WkReferral.ConsultantCode = WkInpatientWLAll.ConsultantCode
AND	WkReferral.SpecialtyCode = WkInpatientWLAll.SpecialtyCode
and	DATEDIFF(day, WkReferral.ReferralDate, WkInpatientWLAll.DateOnWaitingList) BETWEEN 0 AND 7

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match6ConsSpecWithin07Days'
and	MergeTemplate.Active = 1



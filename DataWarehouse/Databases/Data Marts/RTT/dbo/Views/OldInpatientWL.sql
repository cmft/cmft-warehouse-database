﻿CREATE view [dbo].[OldInpatientWL] as

select
	 EncounterRecno = EncounterRecnoMap.EncounterRecno
	,WL.SourcePatientNo
	,WL.SourceEncounterNo
	,WL.CensusDate --= dateadd(day , 1 , WL.CensusDate) -- 2010-04-26 CCB - WL snapshots now generated on previous day
	,WL.PatientSurname
	,WL.PatientForename
	,WL.PatientTitle
	,WL.SexCode
	,WL.DateOfBirth
	,WL.DateOfDeath
	,WL.NHSNumber
	,WL.DistrictNo
	,WL.MaritalStatusCode
	,WL.ReligionCode
	,WL.Postcode
	,WL.PatientsAddress1
	,WL.PatientsAddress2
	,WL.PatientsAddress3
	,WL.PatientsAddress4
	,WL.DHACode
	,WL.HomePhone
	,WL.WorkPhone
	,WL.EthnicOriginCode
	,WL.ConsultantCode
	,WL.SpecialtyCode
	,WL.PASSpecialtyCode
	,WL.ManagementIntentionCode
	,WL.AdmissionMethodCode
	,WL.PriorityCode
	,WL.WaitingListCode
	,WL.CommentClinical
	,WL.CommentNonClinical
	,WL.IntendedPrimaryOperationCode
	,WL.Operation
	,WL.SiteCode
	,WL.WardCode
	,WL.WLStatus
	,WL.ProviderCode
	,WL.PurchaserCode
	,WL.ContractSerialNumber
	,WL.CancelledBy
	,WL.BookingTypeCode
	,WL.CasenoteNumber
	,WL.OriginalDateOnWaitingList
	,WL.DateOnWaitingList
	,WL.TCIDate
	,WL.KornerWait
	,WL.CountOfDaysSuspended
	,WL.SuspensionStartDate
	,WL.SuspensionEndDate
	,WL.SuspensionReasonCode
	,WL.SuspensionReason
	,WL.InterfaceCode
	,WL.EpisodicGpCode
	,WL.EpisodicGpPracticeCode
	,WL.RegisteredGpCode
	,WL.RegisteredPracticeCode
	,WL.SourceTreatmentFunctionCode
	,WL.TreatmentFunctionCode
	,WL.NationalSpecialtyCode
	,WL.PCTCode
	,WL.BreachDate
	,WL.ExpectedAdmissionDate
	,WL.ReferralDate
	,WL.FuturePatientCancelDate
	,WL.AdditionFlag
	,WL.LocalRegisteredGpCode
	,WL.LocalEpisodicGpCode
	,WL.NextOfKinName
	,WL.NextOfKinRelationship
	,WL.NextOfKinHomePhone
	,WL.NextOfKinWorkPhone
	,WL.ExpectedLOS
	,WL.TheatreKey
	,WL.TheatreInterfaceCode
	,WL.ProcedureDate
	,WL.FutureCancellationDate
	,WL.ProcedureTime
	,WL.TheatreCode
	,WL.AdmissionReason
	,WL.EpisodeNo
	,WL.BreachDays
	,WL.MRSA
	,RTTPathwayID = coalesce(WL.RTTPathwayID, LatestWL.RTTPathwayID)
	,RTTPathwayCondition = coalesce(WL.RTTPathwayCondition, LatestWL.RTTPathwayCondition)
	,RTTStartDate = coalesce(WL.RTTStartDate, LatestWL.RTTStartDate)
	,RTTEndDate = coalesce(WL.RTTEndDate, LatestWL.RTTEndDate)
	,RTTSpecialtyCode = coalesce(WL.RTTSpecialtyCode, LatestWL.RTTSpecialtyCode)
	,RTTCurrentProviderCode = coalesce(WL.RTTCurrentProviderCode, LatestWL.RTTCurrentProviderCode)
	,RTTCurrentStatusCode = coalesce(WL.RTTCurrentStatusCode, LatestWL.RTTCurrentStatusCode)
	,RTTCurrentStatusDate = coalesce(WL.RTTCurrentStatusDate, LatestWL.RTTCurrentStatusDate)
	,RTTCurrentPrivatePatientFlag = coalesce(WL.RTTCurrentPrivatePatientFlag, LatestWL.RTTCurrentPrivatePatientFlag)
	,RTTOverseasStatusFlag = coalesce(WL.RTTOverseasStatusFlag, LatestWL.RTTOverseasStatusFlag)
	,WL.NationalBreachDate
	,WL.NationalBreachDays
	,WL.DerivedBreachDays
	,WL.DerivedClockStartDate
	,WL.DerivedBreachDate
	,WL.RTTBreachDate
	,WL.RTTDiagnosticBreachDate
	,WL.NationalDiagnosticBreachDate
	,WL.SocialSuspensionDays
	,WL.BreachTypeCode
	,WL.AddedToWaitingListTime
	,WL.SourceUniqueID
	,WL.AdminCategoryCode
	,SourceEntityRecno = WL.SourceEncounterNo
from
	Warehouse.APC.WaitingList WL with (nolock)

--if rtt details have been added on subsequent snapshots then use them
left join Warehouse.APC.WaitingList LatestWL
on	LatestWL.SourcePatientNo = WL.SourcePatientNo
and	LatestWL.SourceEncounterNo = WL.SourceEncounterNo
and	LatestWL.CensusDate > WL.CensusDate
and	LatestWL.RTTPathwayID is not null
and	WL.RTTPathwayID is null
and	not exists
	(
	select
		1
	from
		Warehouse.APC.WaitingList Previous
	where
		Previous.SourcePatientNo = WL.SourcePatientNo
	and	Previous.SourceEncounterNo = WL.SourceEncounterNo
	and	Previous.CensusDate > WL.CensusDate
	and	Previous.RTTPathwayID is not null
	and	WL.RTTPathwayID is null
	and	(
			Previous.CensusDate > LatestWL.CensusDate
		or	(
				Previous.CensusDate = LatestWL.CensusDate
			and	Previous.EncounterRecno > LatestWL.EncounterRecno
			)
		)
	)

inner join dbo.EncounterRecnoMap
on	EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
and	EncounterRecnoMap.EncounterTypeCode = 'IW'


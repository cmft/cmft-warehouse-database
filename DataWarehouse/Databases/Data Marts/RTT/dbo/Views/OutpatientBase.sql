﻿CREATE  view [dbo].[OutpatientBase] as

select
	*

	,EncounterEndDate = AppointmentDate
	,AttendanceTime = AppointmentTime
	,SourceEncounterNo2 = SourceEncounterNo
	,HospitalCode = SiteCode
	,DNAFlag = DNACode
	,PreviousDNAs = null
	,PatientCancellations = null
	,PreviousAppointmentDate = null
	,PreviousAppointmentStatusCode = null
	,NextAppointmentDate = null
	--,LastDnaOrPatientCancelledDate = null
	,Address1 = PatientAddress1
	,Address2 = PatientAddress2
	,Address3 = PatientAddress3
	,Address4 = PatientAddress4
	,Country = null
from
	Warehouse.OP.Encounter Encounter


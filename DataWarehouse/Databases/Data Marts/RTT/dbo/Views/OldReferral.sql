﻿CREATE  view [dbo].[OldReferral] as

select
	 EncounterRecno = EncounterRecnoMap.EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,InterfaceCode
	,Created
	,Updated
	,ByWhom

	,SourceEncounterNo2 = SourceEncounterNo
	,HospitalCode = SiteCode
	,Address1 = PatientAddress1
	,Address2 = PatientAddress2
	,Address3 = PatientAddress3
	,Address4 = PatientAddress4
	,Country = null
	,DateOnWaitingList = null
	,CasenoteNumber = null
from
	Warehouse.RF.Encounter Encounter with (nolock)

inner join dbo.EncounterRecnoMap
on	EncounterRecnoMap.SourceEncounterRecno = Encounter.EncounterRecno
and	EncounterRecnoMap.EncounterTypeCode = 'RF'


﻿Create View [dbo].[RTTCompletePathwayValidation]

as


Select
	Latest.Census
	,Latest.RTTPathwayID	
	,DistrictNo	
	,SpecialtyCode	
	,ConsultantCode	
	,ReferralDate
	,IPWLAdd	
	,Latest.StartDate	
	,StartReason
	,Latest.StopDate	
	,StopReason	
	,Latest.EventType
	,RTTWeeksWaiting = cast(datediff(day,Latest.StartDate,Latest.StopDate)/7 as int)
	,Category		
	,OutcomeID	
	,OutcomeDescription
	,DateValidated
	,Comments
	,DocumentDescription
	,SignedTime
	,SignedStatusCode	

from
	RTTCompletedPathways Latest 

inner join RTTCompletePathwaysValidationArchive ValidationOutcome
on Latest.RTTPathwayID = ValidationOutcome.RTTPathwayID 
and Latest.StartDate = ValidationOutcome.StartDate

inner join RTT.ValidationOutcome ValidationDescription
on ValidationOutcome.OutcomeID = ValidationDescription.ID

left join WarehouseOLAPMergedV2.Dictation.BaseDocument
on  BaseDocument.SourcePatientNo = Latest.SourcePatientNo
and BaseDocument.DictatedTime = Latest.StopDate
and DocumentTypeCode = 'O' 




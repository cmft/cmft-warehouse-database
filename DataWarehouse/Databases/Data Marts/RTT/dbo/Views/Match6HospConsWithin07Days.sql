﻿
CREATE view [dbo].[Match6HospConsWithin07Days] AS
SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkInpatientWLAll.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkInpatientWLAll ON 
	WkReferral.SourcePatientNo = WkInpatientWLAll.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkInpatientWLAll.SourceEntityRecno
AND	WkReferral.HospitalCode = WkInpatientWLAll.HospitalCode
AND	WkReferral.ConsultantCode = WkInpatientWLAll.ConsultantCode
--AND	WkReferral.SpecialtyCode = WkInpatientWL.SpecialtyCode
and	DATEDIFF(day, WkReferral.ReferralDate, WkInpatientWLAll.DateOnWaitingList) BETWEEN 0 AND 7

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match6HospConsWithin07Days'
and	MergeTemplate.Active = 1



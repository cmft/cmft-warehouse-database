﻿CREATE view [dbo].[OldOutpatientWL] as

select
	 EncounterRecno = EncounterRecnoMap.EncounterRecno

	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate --= DATEADD(day , 1 , CensusDate) -- 2010-04-26 CCB - WL snapshots now generated on previous day
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,BookedDate
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,FuturePatientCancelDate
	,AdditionFlag
	,LastAppointmentFlag
	,LocalRegisteredGpCode
	,LocalEpisodicGpCode
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,FutureCancellationDate
	,EpisodeNo
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,BreachTypeCode
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode

	,convert(int, coalesce(WL.KornerWait, 0) / 7) WeeksWaiting

	,SourceEntityRecno = SourceEncounterNo
from
	Warehouse.OP.WaitingList WL

inner join Warehouse.OP.WaitingListPTL WaitingListPTL
on	WL.EncounterRecno = WaitingListPTL.EncounterRecno

inner join dbo.EncounterRecnoMap
on	EncounterRecnoMap.SourceEncounterRecno = WL.EncounterRecno
and	EncounterRecnoMap.EncounterTypeCode = 'OW'


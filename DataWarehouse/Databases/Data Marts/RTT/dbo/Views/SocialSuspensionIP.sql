﻿
CREATE view [dbo].[SocialSuspensionIP] as

select
	 SourcePatientNo
	,SourceEntityRecno = SourceEncounterNo
	,sum(datediff(day, SuspensionStartDate, SuspensionEndDate)) DaysSuspended
from
	WarehouseSQL.Warehouse.APC.Suspension Suspension
where
	upper(SuspensionReason) like '%S*%'
group by
	 SourcePatientNo
	,SourceEncounterNo



﻿CREATE  view [dbo].[ReferralBase] as

select
	*

	,SourceEncounterNo2 = SourceEncounterNo
	,HospitalCode = SiteCode
	,Address1 = PatientAddress1
	,Address2 = PatientAddress2
	,Address3 = PatientAddress3
	,Address4 = PatientAddress4
	,Country = null
	,DateOnWaitingList = null
	,CasenoteNumber = null
from
	Warehouse.RF.Encounter Encounter


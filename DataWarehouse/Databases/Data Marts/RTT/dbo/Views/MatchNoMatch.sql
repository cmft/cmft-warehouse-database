﻿CREATE     view [dbo].[MatchNoMatch] as

SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	null MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join MergeTemplate
on	MergeTemplate.MergeTemplateCode = 'MatchNoMatch'
and	MergeTemplate.Active = 1

where
	not exists
	(
	select
		1
	from
		TMergeData
	where
		WkOutpatient.EncounterRecno = TMergeData.SourceRecno	
	)


﻿










CREATE view [dbo].[RTTFactBase20120718] as

-- RTTFactTypeCode = 1


select
	 CensusDate = MatchMaster.CensusDate
	,EncounterRecno = Referral.EncounterRecno
	,ConsultantCode = Referral.ConsultantCode
	,SpecialtyCode = Referral.SpecialtyCode

	,PracticeCode =
		coalesce(
			 Referral.EpisodicGpPracticeCode
			,Referral.RegisteredGpPracticeCode
			,'##'
		)
	
	,PCTCode =
		coalesce(
			Practice.ParentOrganisationCode,
			PostcodePCTMap.PCTCode,
			'5NT'
		)

	,SiteCode =
		coalesce(
			 Inpatient.HospitalCode
			,Referral.HospitalCode
		)

	,PathwayStatusCode =
		case

		when
			RTTOPClockStop.RTTClockStopRecno is not null
		then 'OPT' -- Outpatient Treatment

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'AT' -- Awaiting Treatment

		when
			(
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			)
		or	(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'ACS' -- Admission Clock Stop

		when
			Outpatient.EncounterRecno is not null
		then 'OPT' -- Outpatient Treatment

		--this will be removed once RTTPathways kick in
		when
			MatchMaster.InpatientRecno is not null
		--and	(
		--		(
		--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--		)
		--	)
		then 'ADM' -- Inpatient Admission

		when Referral.DischargeDate is not null then 'OPD' -- Outpatient Discharge

		---- Turn off when support for multiple clock stops is available and use UNION as defined below
		--when
		--	Outpatient.EncounterRecno is not null
		--then 'OPT' -- Outpatient Treatment

		--when MatchMaster.FutureOPRecno is not null then 'OPW' -- O/P Waiter

		when
			OutpatientWL.EncounterRecno is not null
		--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
		then 'OPW' -- O/P Waiter


		--22 week waiters go into Outpatient Treatment (Backlog)
		--	when datediff(day, Referral.ReferralDate, MatchMaster.CensusDate) > 154 then 'OPB'

		else 'AT' -- Awaiting Treatment

		end

	,ClockStartDate = 
		dateadd(
			 day
			,coalesce(SocialSuspensionIP.DaysSuspended, RTTAdjustmentIP.AdjustmentDays, 0)
			,coalesce(

				--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
				case
				when RTTClockStartIP.ClockStartReasonCode = 'OVR'
				then RTTClockStartIP.ClockStartDate
				else null
				end

				--Rochdale/Bury entered starts
				--,case
				--	when
				--	(
				--		(
				--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
				--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
				--		)
				--	)
				--	then null
				--	else Inpatient.RTTStartDate
				--end

				--manually entered starts
				,RTTClockStartIP.ClockStartDate

				,RTTClockStartOP.ClockStartDate

				--otherwise get the earliest date
				,(
					select
						min(StartDate)
					from
						(
						select Inpatient.RTTStartDate StartDate union all
						select OutpatientWL.RTTStartDate union all
						select Outpatient.RTTStartDate union all
						select Referral.RTTStartDate union all
						select Referral.ReferralDate
						) Dates
				)
			)
		)

	,WorkflowStatusCode =
		case

		when RTTOPClockStop.RTTClockStopRecno is not null then 'C'

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'P' -- Pending

		when
			(
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			)
		or	(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'C' -- Admission Clock Stop

		when
			Outpatient.EncounterRecno is not null
		then 'C' -- Outpatient Clock Stop

		when
			MatchMaster.InpatientRecno is not null
		--and	(
		--		(
		--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--		)
		--	)
		then 'C' -- Inpatient Admission

		when Referral.DischargeDate is not null then 'C' -- Outpatient Discharge
		--    when MatchMaster.FutureOPRecno is not null then 'O' -- O/P Waiter
		when
			OutpatientWL.EncounterRecno is not null
		--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
		then 'O' -- O/P Waiter

		--    when Outpatient.EncounterRecno is not null then 'C' -- Outpatient Treatment
		--22 week waiters go into Outpatient Treatment (Backlog)
		--	when datediff(day, Referral.ReferralDate, MatchMaster.CensusDate) > 154 then 'C'

		else 'P' -- Pending

		end

	,KeyDate =
		case

		--18 March 2009
		--if the clock stop has been changed then use the date of the change so we count the close in the current period
		when RTTOPClockStop.RTTClockStopRecno is not null then 
			case
			--when BacklogUser.EntityCode is not null --these users maintain backlog
			when RTTOPClockStop.RTTUserModeCode = 'B' --this is a backlog record
			then RTTOPClockStop.ClockStopDate
			else dateadd(day, datediff(day, 0, coalesce(RTTOPClockStop.ClockStopReportingDate, RTTOPClockStop.ClockStopDate)), 0)
			end

		--when RTTOPClockStop.RTTClockStopRecno is not null then RTTOPClockStop.ClockStopDate

		--when
		--	MatchMaster.InpatientRecno is not null
		--and InpatientRTTStatus.ClockStopFlag = 1
		----then coalesce(Inpatient.AdmissionDate , Inpatient.RTTEndDate) -- Admission Clock Stop
		--then coalesce(Inpatient.RTTEndDate , Inpatient.AdmissionDate) -- Admission Clock Stop

		--when
		--	DiagnosticProcedure.ProcedureCode is not null
		--and	DiagnosticProcedure.ProcedureTypeCode = 'T' -- therapy
		--and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
		--then coalesce(Inpatient.RTTEndDate , Inpatient.AdmissionDate) -- Admission Clock Stop

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then MatchMaster.CensusDate

		when
			(
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			)
		or	(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then coalesce(Inpatient.RTTEndDate , Inpatient.AdmissionDate) -- Admission Clock Stop

		when
			Outpatient.EncounterRecno is not null
		--then coalesce(Outpatient.AppointmentDate, Outpatient.RTTEndDate) -- Non-Admitted Clock Stop
		then coalesce(Outpatient.RTTEndDate , Outpatient.AppointmentDate) -- Non-Admitted Clock Stop

		when
			MatchMaster.InpatientRecno is not null
		--and	(
		--		(
		--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--		)
		--	)
		then Inpatient.AdmissionDate -- Inpatient Admission

		--    when Outpatient.EncounterRecno is not null then Outpatient.RTTEndDate -- Non-Admitted Clock Stop
		when Referral.DischargeDate is not null then Referral.DischargeDate -- Outpatient Discharge

		--when
		--	OutpatientWL.EncounterRecno is not null
		--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
		--then OutpatientWL.CensusDate -- O/P Waiter

		--    when MatchMaster.FutureOPRecno is not null then MatchMaster.CensusDate -- O/P Waiter

		--22 week waiters go into Outpatient Treatment (Backlog)
		--	when datediff(day, Referral.ReferralDate, MatchMaster.CensusDate) > 154
		--	then dateadd(day, 154, Referral.ReferralDate)

		else MatchMaster.CensusDate

		end

	,Cases = 1

	,TCIBeyondBreach = 0

	,


	-- generate a WeekNumber e.g. "1 Week Ago", "2 Weeks Ago" etc for the date the treatment was completed
	LastWeeksCases =
		case
		when datediff(
				 day
				,dateadd(
					 day
					,-1
					, -- take a day off 
					case

					--18 March 2009
					--if the clock stop has been changed then use the date of the change so we count the close in the current period
					when RTTOPClockStop.RTTClockStopRecno is not null then 
						case
						--when BacklogUser.EntityCode is not null --these users maintain backlog
						when RTTOPClockStop.RTTUserModeCode = 'B' --this is a backlog record
						then RTTOPClockStop.ClockStopDate
						else dateadd(day, datediff(day, 0, coalesce(RTTOPClockStop.ClockStopReportingDate, RTTOPClockStop.ClockStopDate)), 0)
						end

					--when RTTOPClockStop.RTTClockStopRecno is not null then RTTOPClockStop.ClockStopDate

					when
						DiagnosticProcedure.ProcedureCode is not null
					and	(
							DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
						or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
						)
					then MatchMaster.CensusDate

					when
						(
							MatchMaster.InpatientRecno is not null
						and	InpatientRTTStatus.ClockStopFlag = 1
						)
					or	(
							DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
						and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
						)
					then coalesce(Inpatient.RTTEndDate , Inpatient.AdmissionDate) -- Admission Clock Stop

					when
						Outpatient.EncounterRecno is not null
					then coalesce(Outpatient.RTTEndDate, Outpatient.AppointmentDate) -- Non-Admitted Clock Stop

					when
						MatchMaster.InpatientRecno is not null
					--and	(
					--		(
					--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
					--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
					--		)
					--	)
					then Inpatient.AdmissionDate -- Inpatient Admission

					when Referral.DischargeDate is not null then Referral.DischargeDate -- Outpatient Discharge

					--when Outpatient.EncounterRecno is not null then Outpatient.RTTEndDate -- Non-Admitted Clock Stop

					--when MatchMaster.FutureOPRecno is not null then null -- O/P Waiter

					--when
					--	OutpatientWL.EncounterRecno is not null
					--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
					--then null -- O/P Waiter

					--22 week waiters go into Outpatient Treatment (Backlog)
					--when datediff(day, Referral.ReferralDate, MatchMaster.CensusDate) > 154
					--then dateadd(day, 154, Referral.ReferralDate)
					else null
					end
				)
				,MatchMaster.CensusDate
			) < 8 then 1 -- 14 Jan 2008 - PDO - changed as census run moved from Monday to Sunday
		else 0
		end 

	,PrimaryDiagnosisCode = coalesce(Inpatient.PrimaryDiagnosisCode , 'N/A')

	,PrimaryOperationCode = coalesce(Inpatient.PrimaryOperationCode , 'N/A')


	,ReportableFlag =
		case

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'Y' -- Pending

		when
			(
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			)
		or	(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'Y' -- Admission Clock Stop

		when
			Outpatient.EncounterRecno is not null
		then 'Y' -- Non-Admitted Clock Stop

		when
			MatchMaster.InpatientRecno is not null
		--and	(
		--		(
		--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--		)
		--	)
		then 'Y' -- Inpatient Admission

		--    when Outpatient.EncounterRecno is not null then 'Y' -- Outpatient Treatment
		when OutpatientWL.ReferralDate < '1 Jan 2007' then 'N' -- O/P Waiter
		--    when MatchMaster.FutureOPRecno is not null then 'Y' -- O/P Waiter
		when
			OutpatientWL.EncounterRecno is not null
		--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
		then 'Y' -- O/P Waiter
		when Referral.DischargeDate is not null then 'Y' -- Outpatient Discharge

		when
			Referral.ReferralDate < '1 Jan 2007' then 'N'
		else 'Y' -- Pending
		end

	,UnadjustedClockStartDate = 
		coalesce(

--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
			case
			when RTTClockStartIP.ClockStartReasonCode = 'OVR'
			then RTTClockStartIP.ClockStartDate
			else null
			end

--Rochdale/Bury entered starts
			--,case
			--when
			--	(
			--		(
			--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
			--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
			--		)
			--	)
			--	then null
			--	else Inpatient.RTTStartDate
			--end

			--manually entered starts
			,RTTClockStartIP.ClockStartDate
			,RTTClockStartOP.ClockStartDate

			--otherwise get the earliest date
			,(
				select
					min(StartDate)
				from
					(
					select Inpatient.RTTStartDate StartDate union all
					select OutpatientWL.RTTStartDate union all
					select Outpatient.RTTStartDate union all
					select Referral.RTTStartDate union all
					select Referral.ReferralDate
					) Dates
			)
		)

	,UnadjustedTCIBeyondBreach = 0

	,RTTBreachDate =
		dateadd(
			 day
			,coalesce(RTTBreach.BreachValue, 126)
			,dateadd(
				 day
				,coalesce(SocialSuspensionIP.DaysSuspended, RTTAdjustmentIP.AdjustmentDays, 0)
				,coalesce(

					--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
					case
					when RTTClockStartIP.ClockStartReasonCode = 'OVR'
					then RTTClockStartIP.ClockStartDate
					else null
					end

					--Rochdale/Bury	entered starts
					--,case
					--	when
					--	(
					--		(
					--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
					--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
					--		)
					--	)
					--	then null
					--	else Inpatient.RTTStartDate
					--end

					--manually entered starts
					,RTTClockStartIP.ClockStartDate
					,RTTClockStartOP.ClockStartDate

					--otherwise get the earliest date
					,(
						select
							min(StartDate)
						from
							(
							select Inpatient.RTTStartDate StartDate union all
							select OutpatientWL.RTTStartDate union all
							select Outpatient.RTTStartDate union all
							select Referral.RTTStartDate union all
							select Referral.ReferralDate
							) Dates
					)
				)
			)
		)

	,ReferringProviderCode = left(Referral.RTTPathwayID, 3)

	,ClockChangeReasonCode = 
		coalesce(
			 RTTClockStartIP.ClockStartReasonCode
			,RTTClockStartOP.ClockStartReasonCode
			,RTTClockStartOP.ClockStopReasonCode
			,RTTOPClockStopAll.ClockStartReasonCode
			,RTTOPClockStopAll.ClockStopReasonCode
			,''
		)

	,RTTPathwayID = Referral.RTTPathwayID

	,ClockStopDate =
		case

		when RTTOPClockStop.RTTClockStopRecno is not null then RTTOPClockStop.ClockStopDate

		--when
		--	MatchMaster.InpatientRecno is not null
		--and   InpatientRTTStatus.ClockStopFlag = 1
		--then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

		--when
		--	DiagnosticProcedure.ProcedureCode is not null
		--and	DiagnosticProcedure.ProcedureTypeCode = 'T' -- therapy
		--and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
		--then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then MatchMaster.CensusDate -- Pending

		when
			(
				MatchMaster.InpatientRecno is not null
			and	InpatientRTTStatus.ClockStopFlag = 1
			)
		or	(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

		when
			Outpatient.EncounterRecno is not null
		then coalesce(Outpatient.RTTEndDate, Outpatient.AppointmentDate) -- Non-Admitted Clock Stop

		when
			MatchMaster.InpatientRecno is not null
		--and	(
		--		(
		--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--		)
		--	)
		then Inpatient.AdmissionDate -- Inpatient Admission

		--when Outpatient.EncounterRecno is not null then Outpatient.RTTEndDate -- Non-Admitted Clock Stop

		when Referral.DischargeDate is not null then Referral.DischargeDate -- Outpatient Discharge

		--when
		--	OutpatientWL.EncounterRecno is not null
		--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
		--then OutpatientWL.CensusDate -- O/P Waiter

		--when MatchMaster.FutureOPRecno is not null then MatchMaster.CensusDate -- O/P Waiter

		--22 week waiters go into Outpatient Treatment (Backlog)
		--	when datediff(day, Referral.ReferralDate, MatchMaster.CensusDate) > 154
		--	then dateadd(day, 154, Referral.ReferralDate)

		else MatchMaster.CensusDate

		end

	,RTTFactTypeCode = 1

from
      dbo.Referral with (nolock)

inner join dbo.MatchMaster with (nolock)
on    MatchMaster.ReferralRecno = Referral.EncounterRecno

left join dbo.OlapPractice Practice with (nolock)
on    Practice.OrganisationCode = coalesce(Referral.EpisodicGpPracticeCode, Referral.RegisteredGpPracticeCode)

left join dbo.PostcodePCTMap with (nolock)
on    PostcodePCTMap.Postcode = Referral.Postcode

left join dbo.OutpatientWL with (nolock)
on    OutpatientWL.EncounterRecno = MatchMaster.FutureOPRecno
--and   OutpatientWL.CensusDate = MatchMaster.CensusDate

left join dbo.Outpatient with (nolock)
on  Outpatient.EncounterRecno = MatchMaster.OutpatientRecno

--and exists
--	(
--    select
--          1
--    from
--          dbo.RTTStatus with (nolock)
--    where
--	--swapped these 5 Mar 2010
--	--the period status determines whether or not a clock stopped for THIS appointment, the current status could happen at any time for any event
--	--swapped back on 23 Jul 2010 CCB
--	--the Current Status should always be the most up to date Status
--          --RTTStatus.RTTStatusCode = Outpatient.RTTPeriodStatusCode
--          RTTStatus.RTTStatusCode = Outpatient.RTTCurrentStatusCode
--    and   RTTStatus.ClockStopFlag = 1
--	)

--use OP Disposal Codes to determine OP Clock Stops
and	exists
	(
	select
		1
	from
		OPDisposal
	where
		OPDisposal.OPDisposalCode = Outpatient.DisposalCode
	and	OPDisposal.ClockStopFlag = 1
	)

left join dbo.Inpatient with (nolock)
on    Inpatient.EncounterRecno = MatchMaster.InpatientRecno

left join dbo.RTTStatus InpatientRTTStatus with (nolock)
--swapped these 26 Feb 2010
--the period status determines whether or not a clock stopped for THIS admission, the current status could happen at any time for any event
--swapped back on 23 Jul 2010 CCB
--the Current Status should always be the most up to date Status
--on    InpatientRTTStatus.RTTStatusCode = Inpatient.RTTPeriodStatusCode
on	InpatientRTTStatus.RTTStatusCode = Inpatient.RTTCurrentStatusCode

inner join dbo.OlapSnapshot with (nolock)
on	OlapSnapshot.CensusDate = MatchMaster.CensusDate

left join dbo.RTTClockStart RTTClockStartIP with (nolock)
on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
and	not exists
      (
      select
            1
      from
            dbo.RTTClockStart with (nolock)
      where
            RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
      and   RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
      and   (
                  RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
            or
                  (
                        RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
                  and   RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
                  )
            )
      )

left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment with (nolock)
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

left join dbo.SocialSuspensionIP SocialSuspensionIP with (nolock)
on	SocialSuspensionIP.SourcePatientNo = Inpatient.SourcePatientNo
and	SocialSuspensionIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

left join dbo.RTTOPClockStop with (nolock)
on	RTTOPClockStop.SourcePatientNo = Referral.SourcePatientNo
and	RTTOPClockStop.SourceEntityRecno = Referral.SourceEncounterNo2
and	RTTOPClockStop.ClockStopDate is not null
and	not exists
	(
	select
		1
	from
		dbo.RTTOPClockStop Previous
	where
		Previous.SourcePatientNo = Referral.SourcePatientNo
	and Previous.SourceEntityRecno = Referral.SourceEncounterNo2
	and	Previous.ClockStopDate is not null
	and	Previous.RTTClockStopRecno > RTTOPClockStop.RTTClockStopRecno
	)

left join dbo.RTTOPClockStop RTTClockStartOP with (nolock)
on    RTTClockStartOP.SourcePatientNo = Referral.SourcePatientNo
and   RTTClockStartOP.SourceEntityRecno = Referral.SourceEncounterNo2
and	  RTTClockStartOP.ClockStartDate is not null
and	not exists
	(
	select
		1
	from
		dbo.RTTOPClockStop Previous
	where
		Previous.SourcePatientNo = Referral.SourcePatientNo
	and Previous.SourceEntityRecno = Referral.SourceEncounterNo2
	and	Previous.ClockStopDate is not null
	and	Previous.RTTClockStopRecno > RTTClockStartOP.RTTClockStopRecno
	)

left join dbo.RTTOPClockStop RTTOPClockStopAll with (nolock)
on    RTTOPClockStopAll.SourcePatientNo = Referral.SourcePatientNo
and   RTTOPClockStopAll.SourceEntityRecno = Referral.SourceEncounterNo2
and	not exists
	(
	select
		1
	from
		dbo.RTTOPClockStop Previous
	where
		Previous.SourcePatientNo = Referral.SourcePatientNo
	and Previous.SourceEntityRecno = Referral.SourceEncounterNo2
	and	Previous.ClockStopDate is not null
	and	Previous.RTTClockStopRecno > RTTOPClockStopAll.RTTClockStopRecno
	)

left join dbo.DiagnosticProcedure with (nolock)
--on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode
--on	DiagnosticProcedure.ProcedureCode = Inpatient.IntendedPrimaryOperationCode
on	DiagnosticProcedure.ProcedureCode = coalesce(Inpatient.PrimaryOperationCode, Inpatient.IntendedPrimaryOperationCode) collate Latin1_General_CI_AS

left join 
	(
	select
		BreachValue = 126
	) RTTBreach
on	1 = 1


--removed 5 june 2009
--left join dbo.EntityLookup BacklogUser
--on	upper(BacklogUser.EntityCode) = upper(RTTOPClockStop.LastUser)
--and	BacklogUser.EntityTypeCode = 'BACKLOGUSER'

--left join dbo.EntityBreach RTTBreach with (nolock)
--on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
--and	RTTBreach.EntityCode = 'RTT'
--and	coalesce(
----Rochdale/Bury entered starts
--		case
--			when
--			(
--				(
--					Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
--				and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
--				)
--			or
--				(
--					Inpatient.HospitalCode in ('FGH', 'G') --Bury and Oldham are using the RTTStatus correctly so admissions are not automatic clock stops
--				and	Inpatient.AdmissionDate < '5 jan 2009' --start of Bury RTT ABC process
--				)
--			)
--			then null
--			else Inpatient.RTTStartDate
--		end
----manually entered starts
--		,RTTClockStartIP.ClockStartDate
--		,RTTClockStartIPWL.ClockStartDate
--		,RTTClockStartOP.ClockStartDate
----otherwise get the earliest date
--		,(
--		select
--			min(StartDate)
--		from
--			(
--			select Inpatient.RTTStartDate StartDate union all
--			select InpatientWL.RTTStartDate union all
--			select OutpatientWL.RTTStartDate union all
--			select Referral.RTTStartDate union all
--			select Referral.ReferralDate
--			) Dates
--		)
--	) -- ClockStartDate
--	between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

where
	case

	--18 March 2009
	--if the clock stop has been changed then use the date of the change so we count the close in the current period
	when RTTOPClockStop.RTTClockStopRecno is not null then 
		case
	--replace 5 june 2009
		--when BacklogUser.EntityCode is not null --these users maintain backlog
		when RTTOPClockStop.RTTUserModeCode = 'B' --this is a backlog record
		then RTTOPClockStop.ClockStopDate
		else dateadd(day, datediff(day, 0, coalesce(RTTOPClockStop.ClockStopReportingDate, RTTOPClockStop.ClockStopDate)), 0)
		end

	--when RTTOPClockStop.RTTClockStopRecno is not null then RTTOPClockStop.ClockStopDate

	when
		DiagnosticProcedure.ProcedureCode is not null
	and	(
			DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
		or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
		)
	then MatchMaster.CensusDate -- Awaiting Treatment

	when
		(
			MatchMaster.InpatientRecno is not null
		and	InpatientRTTStatus.ClockStopFlag = 1
		)
	or	(
			DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
		and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
		)
	then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

	when
		Outpatient.EncounterRecno is not null
	then coalesce(Outpatient.RTTEndDate, Outpatient.AppointmentDate) -- Non-Admitted Clock Stop

	when
		MatchMaster.InpatientRecno is not null
	--and	(
	--		(
	--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
	--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
	--		)
	--	)
	then Inpatient.AdmissionDate -- Inpatient Admission

--	when Outpatient.EncounterRecno is not null then Outpatient.RTTEndDate
--	when MatchMaster.FutureOPRecno is not null then getdate() -- O/P Waiter

	when
		OutpatientWL.EncounterRecno is not null
	--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
	then MatchMaster.CensusDate -- O/P Waiter

	when Referral.DischargeDate is not null then Referral.DischargeDate -- Outpatient Discharge

	else getdate()

	end

	>= '1 Jan 2007'

and   
	case

	when
		DiagnosticProcedure.ProcedureCode is not null
	and	(
			DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
		or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
		)
	then MatchMaster.CensusDate -- Awaiting Treatment

	when
		(
			MatchMaster.InpatientRecno is not null
		and	InpatientRTTStatus.ClockStopFlag = 1
		)
	or	(
			DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
		and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
		)
	then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

	when
		Outpatient.EncounterRecno is not null
	then coalesce(Outpatient.RTTEndDate, Outpatient.AppointmentDate) -- Non-Admitted Clock Stop

	when
		MatchMaster.InpatientRecno is not null
	--and	(
	--		(
	--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
	--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
	--		)
	--	)
	then Inpatient.AdmissionDate -- Inpatient Admission

	--when Outpatient.EncounterRecno is not null then Outpatient.RTTEndDate -- Non-Admitted Clock Stop

	--18 March 2009
	--if the clock stop has been changed then use the date of the change so we count the close in the current period
	when RTTOPClockStop.RTTClockStopRecno is not null then 
		case
		--when BacklogUser.EntityCode is not null --these users maintain backlog
		when RTTOPClockStop.RTTUserModeCode = 'B' --this is a backlog record
		then RTTOPClockStop.ClockStopDate
		else dateadd(day, datediff(day, 0, coalesce(RTTOPClockStop.ClockStopReportingDate, RTTOPClockStop.ClockStopDate)), 0)
		end

	--when RTTOPClockStop.RTTClockStopRecno is not null then RTTOPClockStop.ClockStopDate
	--when MatchMaster.FutureOPRecno is not null then MatchMaster.CensusDate -- O/P Waiter

	when
		OutpatientWL.EncounterRecno is not null
	--and	OutpatientWL.CensusDate = MatchMaster.CensusDate
	then MatchMaster.CensusDate -- O/P Waiter

	when Referral.DischargeDate is not null then Referral.DischargeDate -- Outpatient Discharge

	--22 week waiters go into Outpatient Treatment (Backlog)
	--	when datediff(day, Referral.ReferralDate, MatchMaster.CensusDate) > 154
	--	then dateadd(day, 154, Referral.ReferralDate)

	else MatchMaster.CensusDate

	end

	<= MatchMaster.CensusDate

--20090924 if this referral has no associated IP WL entry or there is a WL entry AND an inpatient entry
--then include here as IP WLs are handled in the IP WL insert later in this script
and	(
		MatchMaster.InpatientWLRecno is null
	or	(
			MatchMaster.InpatientWLRecno is not null
		and	MatchMaster.InpatientRecno is not null
		)
	)

union all -- Patients admitted for which we have no referral link but DO have a manually entered clock start date but DON'T have a RTT Pathway



-- RTTFactTypeCode = 2

select
	 CensusDate = OlapSnapshot.CensusDate
	,EncounterRecno = Inpatient.EncounterRecno
	,ConsultantCode = Inpatient.ConsultantCode
	,SpecialtyCode = Inpatient.SpecialtyCode

	,PracticeCode =
		coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode, '##')

	,PCTCode =
		coalesce(
			Practice.ParentOrganisationCode,
			PostcodePCTMap.PCTCode,
			'5NT'
		)

	,SiteCode =
		coalesce(Inpatient.HospitalCode, '##')

	,PathwayStatusCode =
		case

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'AT' -- Awaiting Treatment

		when
			(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'ACS' -- Admission Clock Stop

		else 'ADM' 
		end

	,ClockStartDate =
		dateadd(day,
			 coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
			,RTTClockStartIP.ClockStartDate
		)

	,WorkflowStatusCode =
		case

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'P' -- Awaiting Treatment

		when
			(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'C' -- Admission Clock Stop

		else 'C' 
		end

	,KeyDate = Inpatient.AdmissionDate

	,Cases = 1

	,TCIBeyondBreach = 0

	,LastWeeksCases =
		case
		when datediff(
				 day
				,dateadd(
					 day
					,-1 -- take a day off 
					,Inpatient.AdmissionDate -- Inpatient Admission
				)
				, 
				--dateadd(day, (datepart(dw, Inpatient.EncounterStartDate) -1) * -1,
				--dateadd(week, 1, Inpatient.EncounterStartDate))
				OlapSnapshot.CensusDate
			) < 8 then 1 -- 14 Jan 2008 - PDO - changed as census run moved from Monday to Sunday
		else 0
		end

	,PrimaryDiagnosisCode =
		coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A')

	,PrimaryOperationCode =
		coalesce(Inpatient.PrimaryOperationCode, 'N/A')

	,ReportableFlag = 'Y'

	,UnadjustedClockStartDate = RTTClockStartIP.ClockStartDate

	,UnadjustedTCIBeyondBreach = 0

	,RTTBreachDate = dateadd(day, coalesce(RTTBreach.BreachValue, 126), RTTClockStartIP.ClockStartDate)

	,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

	,ClockChangeReasonCode = 
		coalesce(
			 RTTClockStartIP.ClockStartReasonCode
			,''
		)

	,RTTPathwayID = Inpatient.RTTPathwayID

	,ClockStopDate = Inpatient.AdmissionDate

	,RTTFactTypeCode = 2
from
      Inpatient with (nolock)

left join dbo.OlapPractice Practice
on    Practice.OrganisationCode = coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode)

left join dbo.PostcodePCTMap with (nolock)
on    PostcodePCTMap.Postcode = Inpatient.Postcode

inner join dbo.OlapSnapshot with (nolock)
on    Inpatient.EncounterStartDate between dateadd(month, -1, OlapSnapshot.CensusDate) and OlapSnapshot.CensusDate

inner join dbo.RTTClockStart RTTClockStartIP with (nolock)
on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
and	not exists
    (
    select
        1
    from
        dbo.RTTClockStart with (nolock)
    where
        RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
    and RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
    and (
			RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
        or	(
                RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
            and   RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
		    )
        )
    )

left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment with (nolock)
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
and RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

left join dbo.DiagnosticProcedure with (nolock)
--on	DiagnosticProcedure.ProcedureCode = coalesce(Inpatient.PrimaryOperationCode, Inpatient.IntendedPrimaryOperationCode)
--on	DiagnosticProcedure.ProcedureCode = Inpatient.IntendedPrimaryOperationCode
on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode collate Latin1_General_CI_AS

left join dbo.EntityBreach RTTBreach with (nolock)
on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
and	RTTBreach.EntityCode = 'RTT'
and	RTTClockStartIP.ClockStartDate between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

where
	Inpatient.EncounterStartDate >= '1 jan 2007'
and not exists
    (
    select
        1
    from
        dbo.MatchMaster with (nolock)
    where
        MatchMaster.InpatientRecno = Inpatient.EncounterRecno
    and MatchMaster.CensusDate = 
        OlapSnapshot.CensusDate
		--dateadd(day, (datepart(dw, Inpatient.EncounterStartDate) -1) * -1,
		--dateadd(week, 1, Inpatient.EncounterStartDate))
    )

--6 Feb 2009 - removed as was preventing records being loaded when census date was 1st of the month
--and   datediff(month, Inpatient.EncounterStartDate, OlapSnapshot.CensusDate) = 0

and	Inpatient.RTTStartDate is null





union all -- Patients admitted for which we have no referral link but DO have a RTT Pathway


-- RTTFactTypeCode = 3


select
	 CensusDate = OlapSnapshot.CensusDate
	,EncounterRecno = Inpatient.EncounterRecno
	,ConsultantCode = Inpatient.ConsultantCode
	,SpecialtyCode = Inpatient.SpecialtyCode

	,PracticeCode =
		coalesce(
			 Inpatient.EpisodicGpPracticeCode
			,Inpatient.RegisteredGpPracticeCode
			,'##'
		)

	,PCTCode =
		coalesce(
			 Practice.ParentOrganisationCode
			,PostcodePCTMap.PCTCode
			,'5NT'
		)

	,SiteCode =
		coalesce(
			 Inpatient.HospitalCode
			,'##'
		)

	,PathwayStatusCode =
		case

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'AT' -- Awaiting Treatment

		when
			(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'ACS' -- Admission Clock Stop

		--when
		--(
		--	(
		--		Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--	and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--	)
		--)
		--then 'ADM' -- Inpatient Admission

		else 'ADM'
		--else 'AT'

		end

	,ClockStartDate =
		dateadd(
			 day
			,coalesce(
				 RTTAdjustmentIP.AdjustmentDays
				,0
			)
			,coalesce(
				--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
				case
				when RTTClockStartIP.ClockStartReasonCode = 'OVR'
				then RTTClockStartIP.ClockStartDate
				else null
				end

				--,case
				--	when
				--	(
				--		(
				--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
				--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
				--		)
				--	)
				--	then null
				--	else Inpatient.RTTStartDate
				--end

				,RTTClockStartIP.ClockStartDate
				,Inpatient.RTTStartDate
			)
		)

	,WorkflowStatusCode =
		case

		when
			DiagnosticProcedure.ProcedureCode is not null
		and	(
				DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
			or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'P' -- Pending

		when
			(
				DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
			and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
			)
		then 'C' -- Conplete

		--when
		--(
		--	(
		--		Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--	and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--	)
		--)
		--then 'C' -- Inpatient Admission

		else 'C'

		--else 'P'

		end

	,KeyDate = Inpatient.AdmissionDate

	,Cases = 1

	,TCIBeyondBreach = 0

	,LastWeeksCases =
		case

		when datediff(
				 day
				,dateadd(
					 day
					,-1 -- take a day off 
					,case

					--when
					--	InpatientRTTStatus.ClockStopFlag = 1
					--then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

					--when
					--	DiagnosticProcedure.ProcedureCode is not null
					--and	DiagnosticProcedure.ProcedureTypeCode = 'T' -- therapy
					--and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
					--then  coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

					when
						DiagnosticProcedure.ProcedureCode is not null
					and	(
							DiagnosticProcedure.ProcedureTypeCode = 'D' -- Diagnostic
						or Inpatient.SiteCode in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
						)
					then Inpatient.AdmissionDate

					when
						(
							DiagnosticProcedure.ProcedureTypeCode = 'T' -- Diagnostic
						and Inpatient.SiteCode not in ('BHCH' , 'NRMC' , 'RMCH') -- all childrens are diagnostic
						)
					then coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) -- Admission Clock Stop

					--when
					--(
					--	(
					--		Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
					--	and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
					--	)
					--)
					--then Inpatient.AdmissionDate -- Inpatient Admission

					else Inpatient.AdmissionDate

					--else null

					end
				)
				,
				--dateadd(day, (datepart(dw, Inpatient.EncounterStartDate) -1) * -1,
				--dateadd(week, 1, Inpatient.EncounterStartDate))
				OlapSnapshot.CensusDate
			) < 8 then 1 -- 14 Jan 2008 - PDO - changed as census run moved from Monday to Sunday

		else 0

		end

	,PrimaryDiagnosisCode = coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A')
	,PrimaryOperationCode = coalesce(Inpatient.PrimaryOperationCode, 'N/A') 

	,ReportableFlag = 'Y'

	,UnadjustedClockStartDate = 
		coalesce
			(
			--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
			case
			when RTTClockStartIP.ClockStartReasonCode = 'OVR'
			then RTTClockStartIP.ClockStartDate
			else null
			end

			--,case
			--	when
			--	(
			--		(
			--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
			--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
			--		)
			--	or
			--		(
			--			Inpatient.HospitalCode in ('FGH', 'G') --Bury and Oldham are using the RTTStatus correctly so admissions are not automatic clock stops
			--		and	Inpatient.AdmissionDate < '5 jan 2009' --start of Bury RTT ABC process
			--		)
			--	or
			--		(
			--			Inpatient.HospitalCode in ('NMGH') --North is using the RTTStatus correctly so admissions are not automatic clock stops
			--		and	Inpatient.AdmissionDate < '09 Feb 2009' --start of North RTT ABC process
			--		)
			--	)
			--	then null
			--	else Inpatient.RTTStartDate
			--end

			,RTTClockStartIP.ClockStartDate
			,Inpatient.RTTStartDate
		)


	,UnadjustedTCIBeyondBreach = 0

	,RTTBreachDate = dateadd(day, coalesce(RTTBreach.BreachValue, 126), Inpatient.RTTStartDate)

	,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

	,ClockChangeReasonCode = RTTClockStartIP.ClockStartReasonCode

	,Inpatient.RTTPathwayID

	,ClockStopDate = Inpatient.AdmissionDate

	,RTTFactTypeCode = 3
from
      Inpatient with (nolock)

left join dbo.OlapPractice Practice with (nolock)
on    Practice.OrganisationCode = coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode)

left join dbo.PostcodePCTMap with (nolock)
on    PostcodePCTMap.Postcode = Inpatient.Postcode

inner join dbo.OlapSnapshot
on    Inpatient.EncounterStartDate between dateadd(month, -1, OlapSnapshot.CensusDate) and OlapSnapshot.CensusDate

left join dbo.DiagnosticProcedure with (nolock)
--on	DiagnosticProcedure.ProcedureCode = coalesce(Inpatient.PrimaryOperationCode, Inpatient.IntendedPrimaryOperationCode)
--on	DiagnosticProcedure.ProcedureCode = Inpatient.IntendedPrimaryOperationCode
on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode collate Latin1_General_CI_AS

left join dbo.EntityBreach RTTBreach with (nolock)
on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
and	RTTBreach.EntityCode = 'RTT'
and	Inpatient.RTTStartDate between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

left join dbo.RTTStatus InpatientRTTStatus with (nolock)
on	InpatientRTTStatus.RTTStatusCode = Inpatient.RTTPeriodStatusCode

left join dbo.RTTClockStart RTTClockStartIP with (nolock)
on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
and	not exists
    (
    select
        1
    from
        dbo.RTTClockStart with (nolock)
    where
        RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
    and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
    and	(
			RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
        or	(
				RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
            and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
            )
        )
    )

left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment with (nolock)
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

where
	Inpatient.EncounterStartDate >= '1 jan 2007'
and	not exists
	(
	select
		1
	from
		dbo.MatchMaster with (nolock)
	where
		MatchMaster.InpatientRecno = Inpatient.EncounterRecno
	and MatchMaster.CensusDate = 
		OlapSnapshot.CensusDate
	--dateadd(day, (datepart(dw, Inpatient.EncounterStartDate) -1) * -1,
	--dateadd(week, 1, Inpatient.EncounterStartDate))
	)

--6 Feb 2009 - removed as was preventing records being loaded when census date was 1st of the month
--and   datediff(month, Inpatient.EncounterStartDate, OlapSnapshot.CensusDate) = 0

and	Inpatient.RTTStartDate is not null




union all -- IP waiting list

-- RTTFactTypeCode = 4

select
	 CensusDate = OlapSnapshot.CensusDate
	,EncounterRecno = InpatientWL.EncounterRecno -- this will avoid potential duplicates
	,ConsultantCode = InpatientWL.ConsultantCode
	,SpecialtyCode = InpatientWL.SpecialtyCode

	,PracticeCode =
		coalesce(
			 InpatientWL.EpisodicGpPracticeCode
			,InpatientWL.RegisteredPracticeCode
			,'##'
		)

	,PCTCode = 
		coalesce(
			 Practice.ParentOrganisationCode
			,PostcodePCTMap.PCTCode
			,'5NT'
		)

	,SiteCode = 
		coalesce(
			 InpatientWL.SiteCode
			,'##'
		)

	,PathwayStatusCode = 'IPW'

	,ClockStartDate =
		dateadd(
			 day
			,case
			when InpatientWL.WLStatus = 'WL Suspend'
			then 
				datediff(
					 day
					,coalesce(InpatientWL.RTTStartDate, InpatientWL.OriginalDateOnWaitingList)
					,coalesce(Suspension.SuspensionEndDate, InpatientWL.CensusDate)
				)
				 - coalesce(SocialSuspension.DaysSuspended, 0)
			else
				datediff(
					 day
					,coalesce(
						--case
						--	when
						--	(
						--		(
						--			InpatientWL.SiteCode not in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
						--		or	InpatientWL.CensusDate < '1 oct 2008' --start of Rochdale RTT ABC process
						--		)
						--	)
						--	then null
						--	else InpatientWL.RTTStartDate
						--end
						 RTTClockStartIP.ClockStartDate
						,InpatientWL.DerivedClockStartDate
						,InpatientWL.RTTStartDate
						)
					,InpatientWL.CensusDate
					) - coalesce(InpatientWL.SocialSuspensionDays, 0)
			end  * -1
			,InpatientWL.CensusDate
		)

--3 May 2011 PDO
--replaced with above derivation as manual clock starts were not coming through at all

	--ClockStartDate =
	--	dateadd(
	--		day
	--		,case
	--		when InpatientWL.WLStatus = 'WL Suspend'
	--		then 
	--			datediff
	--			(
	--				 day
	--				,coalesce(InpatientWL.RTTStartDate, InpatientWL.OriginalDateOnWaitingList)
	--				,coalesce(Suspension.SuspensionEndDate, InpatientWL.CensusDate)
	--			)
	--			 - coalesce(SocialSuspension.DaysSuspended, 0)
	--		else
	--			(datediff(day, InpatientWL.DerivedClockStartDate, InpatientWL.CensusDate) - coalesce(InpatientWL.SocialSuspensionDays, 0))
	--		end  * -1
	--		,InpatientWL.CensusDate
	--	)


--	dateadd(day,
--		coalesce(RTTAdjustmentIP.AdjustmentDays, 0) + coalesce(InpatientWL.SocialSuspensionDays, 0)
--		,coalesce
--			(
--			case
--				when
--				(
--					(
--						InpatientWL.SiteCode not in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
--					or	InpatientWL.CensusDate < '1 oct 2008' --start of Rochdale RTT ABC process
--					)
--				)
--				then null
--				else InpatientWL.RTTStartDate
--			end
--			,RTTClockStartIP.ClockStartDate
--			,InpatientWL.RTTStartDate
--		)
--	)

     ,WorkflowStatusCode = 'I'

	,KeyDate = InpatientWL.CensusDate

	,Cases = 1

	,TCIBeyondBreach =
		case  
		when InpatientWL.TCIDate is null then 1
		when InpatientWL.TCIDate >
				dateadd(
						day
					,coalesce(RTTBreach.BreachValue, 126)
					,dateadd(
							day
						,coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
					,RTTClockStartIP.ClockStartDate
					) -- ClockStartDate
				) then 1
		else 0
		end 

	,LastWeeksCases =
		case
		when datediff(
				 day
				,dateadd(
					 day
					,-1 -- take a day off 
					,InpatientWL.CensusDate -- InpatientWL
				)
				,--dateadd(day, (datepart(dw, InpatientWL.CensusDate) -1) * -1,
				--dateadd(week, 1, InpatientWL.CensusDate))
				OlapSnapshot.CensusDate
			) < 8 then 1 -- 14 Jan 2008 - PDO - changed as census run moved from Monday to Sunday
		else 0
		end 

	,PrimaryDiagnosisCode = 'N/A' 

	,PrimaryOperationCode = coalesce(InpatientWL.IntendedPrimaryOperationCode, 'N/A') 

	,ReportableFlag = 'Y'

	,UnadjustedClockStartDate = 
		coalesce
			(
		--	case
		--		when
		--		(
		--			(
		--				InpatientWL.SiteCode not in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
		--			or	InpatientWL.CensusDate < '1 oct 2008' --start of Rochdale RTT ABC process
		--			)
		--		)
		--		then null
		--		else InpatientWL.RTTStartDate
		--	end

			 RTTClockStartIP.ClockStartDate
			,InpatientWL.RTTStartDate
		)

	,UnadjustedTCIBeyondBreach = 
		case  
		when InpatientWL.TCIDate is null then 1
		when InpatientWL.TCIDate > dateadd(day, coalesce(RTTBreach.BreachValue, 126), RTTClockStartIP.ClockStartDate) -- ClockStartDate
		then 1
		else 0
		end

	,RTTBreachDate = dateadd(day, coalesce(RTTBreach.BreachValue, 126), RTTClockStartIP.ClockStartDate)

	,ReferringProviderCode = left(InpatientWL.RTTPathwayID, 3)

	,ClockChangeReasonCode = 
		coalesce(
			 RTTClockStartIP.ClockStartReasonCode
			,''
		)

	,InpatientWL.RTTPathwayID

	,ClockStopDate = InpatientWL.CensusDate

	,RTTFactTypeCode = 4
from
	InpatientWL with (nolock)

left join dbo.OlapPractice Practice with (nolock)
on	Practice.OrganisationCode = coalesce(InpatientWL.EpisodicGpPracticeCode, InpatientWL.RegisteredPracticeCode)

left join dbo.PostcodePCTMap with (nolock)
on	PostcodePCTMap.Postcode = InpatientWL.Postcode

inner join dbo.OlapSnapshot with (nolock)
on	InpatientWL.CensusDate = OlapSnapshot.CensusDate

left join dbo.RTTClockStart RTTClockStartIP with (nolock)
on	RTTClockStartIP.SourcePatientNo = InpatientWL.SourcePatientNo
and	RTTClockStartIP.SourceEntityRecno = InpatientWL.SourceEntityRecno
and	not exists
    (
	select
		1
	from
		dbo.RTTClockStart with (nolock)
	where
		RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
	and	RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
	and	(
			RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
		or	(
				RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
			and   RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
			)
		)
	)

left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment with (nolock)
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
on	RTTAdjustmentIP.SourcePatientNo = InpatientWL.SourcePatientNo
and	RTTAdjustmentIP.SourceEntityRecno = InpatientWL.SourceEntityRecno

left join dbo.EntityBreach RTTBreach with (nolock)
on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
and	RTTBreach.EntityCode = 'RTT'
and	RTTClockStartIP.ClockStartDate between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

--Social suspensions only
left join dbo.SocialSuspension SocialSuspension
on	SocialSuspension.SourcePatientNo = InpatientWL.SourcePatientNo
and	SocialSuspension.SourceEntityRecno = InpatientWL.SourceEntityRecno
and SocialSuspension.CensusDate = InpatientWL.CensusDate

left join 
	(
	select
		*
	from
		dbo.Suspension Suspension
	where
		upper(Suspension.SuspensionReason) like '%S*%'
	and	not exists
		(
		select
			1
		from
			dbo.Suspension LastSuspension
		where
			LastSuspension.SourcePatientNo = Suspension.SourcePatientNo
		and	upper(LastSuspension.SuspensionReason) like '%S*%'
		and	LastSuspension.SourceEncounterNo = Suspension.SourceEncounterNo
		and	LastSuspension.CensusDate = Suspension.CensusDate
		and
			(
				LastSuspension.SuspensionStartDate > Suspension.SuspensionStartDate
			or
				(
					LastSuspension.SuspensionStartDate = Suspension.SuspensionStartDate
				and	LastSuspension.SuspensionRecno > Suspension.SuspensionRecno
				)
			)
		)
	) Suspension
on	Suspension.SourcePatientNo = InpatientWL.SourcePatientNo
and	Suspension.SourceEncounterNo = InpatientWL.SourceEncounterNo
and	Suspension.CensusDate = InpatientWL.CensusDate

where
	InpatientWL.CensusDate >= '1 jan 2007'


union all -- Patients admitted that have a previous, manually entered, OP clock stop

-- RTTFactTypeCode = 5
select
	 CensusDate = OlapSnapshot.CensusDate
	,ReferralRecno = Inpatient.EncounterRecno
	,ConsultantCode = Inpatient.ConsultantCode
	,SpecialtyCode = Inpatient.SpecialtyCode

	,PracticeCode =
		coalesce(
			 Inpatient.EpisodicGpPracticeCode
			,Inpatient.RegisteredGpPracticeCode
			,'##'
		)

	,PCTCode =
		coalesce(
			 Practice.ParentOrganisationCode
			,PostcodePCTMap.PCTCode
			,'5NT'
		)

	,SiteCode =
		coalesce(
			 Inpatient.HospitalCode
			,'##'
		)

	,PathwayStatusCode = 'ACS' -- Admission Clock Stop

	,ClockStartDate =
		dateadd(
			 day
			,coalesce(RTTAdjustmentIP.AdjustmentDays, 0)
			,coalesce(
				--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
				case
				when RTTClockStartIP.ClockStartReasonCode = 'OVR'
				then RTTClockStartIP.ClockStartDate
				else null
				end

				--,case
				--	when
				--	(
				--		(
				--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
				--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
				--		)
				--	or
				--		(
				--			Inpatient.HospitalCode in ('FGH', 'G') --Bury and Oldham are using the RTTStatus correctly so admissions are not automatic clock stops
				--		and	Inpatient.AdmissionDate < '5 jan 2009' --start of Bury RTT ABC process
				--		)
				--	or
				--		(
				--			Inpatient.HospitalCode in ('NMGH') --North is using the RTTStatus correctly so admissions are not automatic clock stops
				--		and	Inpatient.AdmissionDate < '09 Feb 2009' --start of North RTT ABC process
				--		)
				--	)
				--	then null
				--	else Inpatient.RTTStartDate
				--end

				,RTTClockStartIP.ClockStartDate
				,Inpatient.RTTStartDate
			)
		)

	,WorkflowStatusCode = 'C' -- Admission Clock Stop

	,KeyDate = Inpatient.AdmissionDate

	,Cases = 1

	,TCIBeyondBreach = 0

	,LastWeeksCases =
		case
		when 
			datediff(
				 day
				,dateadd(
					 day
					,-1 -- take a day off 
					,coalesce(
						 Inpatient.RTTEndDate
						,Inpatient.AdmissionDate
					) -- Admission Clock Stop
				)
				,OlapSnapshot.CensusDate
			) < 8
		then 1 -- 14 Jan 2008 - PDO - changed as census run moved from Monday to Sunday
		else 0
		end

	,PrimaryDiagnosisCode = coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A')
	,PrimaryOperationCode = coalesce(Inpatient.PrimaryOperationCode, 'N/A')

	,ReportableFlag = 'Y'

	,UnadjustedClockStartDate = 
		coalesce(
			--if ClockStartReasonCode is Override then use this ClockStartDate in preference to RTT entered date
			case
			when RTTClockStartIP.ClockStartReasonCode = 'OVR'
			then RTTClockStartIP.ClockStartDate
			else null
			end

			--,case
			--	when
			--	(
			--		(
			--			Inpatient.HospitalCode in ('BHH', 'RI') --Rochdale are using the RTTStatus correctly so admissions are not automatic clock stops
			--		and	Inpatient.AdmissionDate < '1 oct 2008' --start of Rochdale RTT ABC process
			--		)
			--	)
			--	then null
			--	else Inpatient.RTTStartDate
			--end

			,RTTClockStartIP.ClockStartDate
			,Inpatient.RTTStartDate
		)


	,UnadjustedTCIBeyondBreach = 0

	,RTTBreachDate = dateadd(day, coalesce(RTTBreach.BreachValue, 126), Inpatient.RTTStartDate)

	,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

	,ClockChangeReasonCode = RTTClockStartIP.ClockStartReasonCode

	,Inpatient.RTTPathwayID

	,ClockStopDate = Inpatient.AdmissionDate

	,RTTFactTypeCode = 5
from
	Inpatient with (nolock)

left join dbo.OlapPractice Practice with (nolock)
on	Practice.OrganisationCode = coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode)

left join dbo.PostcodePCTMap with (nolock)
on	PostcodePCTMap.Postcode = Inpatient.Postcode

inner join dbo.OlapSnapshot
on	Inpatient.EncounterStartDate between dateadd(month, -1, OlapSnapshot.CensusDate) and OlapSnapshot.CensusDate

left join dbo.DiagnosticProcedure with (nolock)
on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode collate Latin1_General_CI_AS

left join dbo.EntityBreach RTTBreach with (nolock)
on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
and	RTTBreach.EntityCode = 'RTT'
and	Inpatient.RTTStartDate between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

inner join dbo.RTTStatus InpatientRTTStatus with (nolock)
on	InpatientRTTStatus.RTTStatusCode = Inpatient.RTTPeriodStatusCode
and	InpatientRTTStatus.ClockStopFlag = 1

left join dbo.RTTClockStart RTTClockStartIP with (nolock)
on	RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
and	not exists
    (
    select
        1
    from
        dbo.RTTClockStart with (nolock)
    where
        RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
    and   RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
    and (
			RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
        or	(
				RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
			and	RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
            )
        )
    )

left join 
	(
	select
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
		,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
	from
		dbo.RTTAdjustment with (nolock)
	group by
		 RTTAdjustment.SourcePatientNo
		,RTTAdjustment.SourceEntityRecno
	) RTTAdjustmentIP
on	RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
and	RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

where
	Inpatient.EncounterStartDate >= '1 jan 2007'

and	exists
	(
	select
		1
	from
		dbo.Outpatient

	inner join dbo.RTTOPClockStop
	on	RTTOPClockStop.SourcePatientNo = Outpatient.SourcePatientNo
	and	RTTOPClockStop.SourceEntityRecno = Outpatient.SourceEncounterNo2
	and	RTTOPClockStop.ClockStopReasonCode like '3%'

	where
		Outpatient.RTTPathwayID = Inpatient.RTTPathwayID
	and	Outpatient.AppointmentDate < Inpatient.EpisodeStartDate
	)













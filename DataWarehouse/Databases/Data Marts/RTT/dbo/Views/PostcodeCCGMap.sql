﻿
/****** Object:  View [dbo].[PostcodePCTMap]    Script Date: 04/17/2014 13:24:26 ******/
Create view [dbo].[PostcodeCCGMap] as

select
	Postcode
	,DOHCode = LocalAuthorityCode
	,CCGCode
from
	Organisation.dbo.CCGPostcode



﻿CREATE view [dbo].[Match4HospSpecWithin22to28Days] AS
SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkSpell.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkSpell ON 
	WkReferral.SourcePatientNo = WkSpell.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkSpell.SourceEncounterNo
AND	WkReferral.HospitalCode = WkSpell.HospitalCode
--AND	WkReferral.ConsultantCode = WkSpell.ConsultantCode
AND	WkReferral.SpecialtyCode = WkSpell.SpecialtyCode
and	DATEDIFF(day, WkReferral.ReferralDate, WkSpell.DateOnWaitingList) BETWEEN 22 AND 28

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match4HospSpecWithin22to28Days'
and	MergeTemplate.Active = 1


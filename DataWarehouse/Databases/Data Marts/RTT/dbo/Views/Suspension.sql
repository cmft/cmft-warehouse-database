﻿
CREATE view [dbo].[Suspension] as

select
	 SuspensionRecno
	,SourcePatientNo
	,SourceEncounterNo
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,CensusDate = DATEADD(day , 1 , CensusDate) -- 2010-04-26 CCB - WL snapshots now generated on previous day
	,SourceUniqueID
from
	WarehouseSQL.Warehouse.APC.WaitingListSuspension Suspension



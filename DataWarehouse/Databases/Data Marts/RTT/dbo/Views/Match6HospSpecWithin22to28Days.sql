﻿
CREATE view [dbo].[Match6HospSpecWithin22to28Days] AS
SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkInpatientWLAll.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkInpatientWLAll ON 
	WkReferral.SourcePatientNo = WkInpatientWLAll.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkInpatientWLAll.SourceEntityRecno
AND	WkReferral.HospitalCode = WkInpatientWLAll.HospitalCode
--AND	WkReferral.ConsultantCode = WkInpatientWLAll.ConsultantCode
AND	WkReferral.SpecialtyCode = WkInpatientWLAll.SpecialtyCode
and	DATEDIFF(day, WkReferral.ReferralDate, WkInpatientWLAll.DateOnWaitingList) BETWEEN 22 AND 28

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match6HospSpecWithin22to28Days'
and	MergeTemplate.Active = 1



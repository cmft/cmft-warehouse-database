﻿CREATE     view [dbo].[Match2NoMatch] as

SELECT  
	WkSpell.EncounterRecno SourceRecno,
	null MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkSpell

inner join MergeTemplate
on	MergeTemplate.MergeTemplateCode = 'Match2NoMatch'
and	MergeTemplate.Active = 1

where
	not exists
	(
	select
		1
	from
		TMergeData
	where
		WkSpell.EncounterRecno = TMergeData.SourceRecno	
	)


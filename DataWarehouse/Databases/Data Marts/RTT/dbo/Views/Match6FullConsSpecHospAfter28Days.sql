﻿
CREATE view [dbo].[Match6FullConsSpecHospAfter28Days] AS

SELECT  
	WkReferral.EncounterRecno SourceRecno,
	WkInpatientWLAll.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join dbo.WkInpatientWLAll ON 
	WkReferral.SourcePatientNo = WkInpatientWLAll.SourcePatientNo
and	WkReferral.SourceEncounterNo2 < WkInpatientWLAll.SourceEntityRecno
AND	WkReferral.HospitalCode = WkInpatientWLAll.HospitalCode
AND	WkReferral.ConsultantCode = WkInpatientWLAll.ConsultantCode
AND	WkReferral.SpecialtyCode = WkInpatientWLAll.SpecialtyCode
and	DATEDIFF(day, WkReferral.ReferralDate, WkInpatientWLAll.DateOnWaitingList) > 28

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match6FullConsSpecHospAfter28Days'
and	MergeTemplate.Active = 1



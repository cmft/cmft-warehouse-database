﻿
CREATE VIEW [dbo].[Match7FullConsSpecHospWithin08to14Days] AS
SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkInpatientWLAll.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkInpatientWLAll ON 
	WkOutpatient.SourcePatientNo = WkInpatientWLAll.SourcePatientNo
AND	WkOutpatient.HospitalCode = WkInpatientWLAll.HospitalCode
AND	WkOutpatient.ConsultantCode = WkInpatientWLAll.ConsultantCode
AND	WkOutpatient.SpecialtyCode = WkInpatientWLAll.SpecialtyCode
and	DATEDIFF(day, WkOutpatient.AppointmentDate, WkInpatientWLAll.DateOnWaitingList) BETWEEN 8 AND 14

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'Match7FullConsSpecHospWithin08to14Days'
and	MergeTemplate.Active = 1



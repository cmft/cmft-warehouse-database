﻿
CREATE view [dbo].[OlapDiagnosis] as

select
	 DiagnosisCode
	,Diagnosis =
		DiagnosisCode + ' - ' + Diagnosis
from
	WarehouseSQL.Warehouse.PAS.Diagnosis

union all

select distinct
	RTTFact.PrimaryDiagnosisCode
	,Diagnosis = 
		rtrim(RTTFact.PrimaryDiagnosisCode) + ' - No Description'
from
	dbo.RTTFact
where
	not exists
	(
	select
		1
	from
		WarehouseSQL.Warehouse.PAS.Diagnosis Diagnosis
	where
		Diagnosis.DiagnosisCode = RTTFact.PrimaryDiagnosisCode
	)
and	RTTFact.PrimaryDiagnosisCode is not null



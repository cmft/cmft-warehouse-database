﻿CREATE view [dbo].[RTTMatch] as

select
	 RTTFact.CensusDate
--	,RTTFact.EncounterRecno
	,EncounterRecno = MappedReferral.SourceEncounterRecno
	,RTTFact.EncounterTypeCode
	,RTTFact.ConsultantCode
	,RTTFact.SpecialtyCode
	,RTTFact.PracticeCode
	,RTTFact.PCTCode
	,RTTFact.SiteCode
	,RTTFact.WeekBandCode
	,RTTFact.PathwayStatusCode
	,RTTFact.ReferralDate
	,RTTFact.WorkflowStatusCode
	,RTTFact.BreachBandCode
	,RTTFact.DaysWaiting
	,RTTFact.Cases
	,RTTFact.TCIBeyondBreach
	,RTTFact.LastWeeksCases
	,RTTFact.PrimaryDiagnosisCode
	,RTTFact.PrimaryOperationCode
	,ReferralRecno = MappedReferral.SourceEncounterRecno
	,OutpatientRecno = MappedOutpatient.SourceEncounterRecno
	,InpatientWLRecno = MappedInpatientWL.SourceEncounterRecno
	,InpatientRecno = MappedInpatient.SourceEncounterRecno
	,FutureOPRecno = MappedFutureOP.SourceEncounterRecno
	,MatchMaster.IPToFutureOPRecno
	,MatchMaster.AttIPWLTemplateRecno
	,MatchMaster.AttSpellTemplateRecno
	,MatchMaster.SpellOPWLTemplateRecno
from
	dbo.RTTFact

inner join dbo.MatchMaster
on	MatchMaster.ReferralRecno = RTTFact.EncounterRecno
and	MatchMaster.CensusDate = RTTFact.CensusDate
and	RTTFact.EncounterTypeCode = 'REFERRAL'

inner join RTT.dbo.EncounterRecnoMap MappedReferral
on	MappedReferral.SourceEncounterRecno = MatchMaster.ReferralRecno
and	MappedReferral.EncounterTypeCode = 'RF'

left join RTT.dbo.EncounterRecnoMap MappedOutpatient
on	MappedOutpatient.SourceEncounterRecno = MatchMaster.OutpatientRecno
and	MappedOutpatient.EncounterTypeCode = 'OP'

left join RTT.dbo.EncounterRecnoMap MappedInpatientWL
on	MappedInpatientWL.SourceEncounterRecno = MatchMaster.InpatientWLRecno
and	MappedInpatientWL.EncounterTypeCode = 'IW'

left join RTT.dbo.EncounterRecnoMap MappedInpatient
on	MappedInpatient.SourceEncounterRecno = MatchMaster.InpatientRecno
and	MappedInpatient.EncounterTypeCode = 'IP'

left join RTT.dbo.EncounterRecnoMap MappedFutureOP
on	MappedFutureOP.SourceEncounterRecno = MatchMaster.FutureOPRecno
and	MappedFutureOP.EncounterTypeCode = 'OW'

where
	RTTFact.AdjustedFlag = 'Y'


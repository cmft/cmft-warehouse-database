﻿create view [dbo].[RTTClockStartCurrent] as

select
	*
from
	dbo.RTTClockStart
where
	not exists
	(
	select
		1
	from
		dbo.RTTClockStart LatestRTTClockStart
	where
		LatestRTTClockStart.SourcePatientNo = RTTClockStart.SourcePatientNo
	and	LatestRTTClockStart.SourceEntityRecno = RTTClockStart.SourceEntityRecno
	and	(
			LatestRTTClockStart.ClockStartDate > RTTClockStart.ClockStartDate
		or
			(
				LatestRTTClockStart.ClockStartDate = RTTClockStart.ClockStartDate
			and	LatestRTTClockStart.RTTClockStartRecno > RTTClockStart.RTTClockStartRecno
			)
		)
	)


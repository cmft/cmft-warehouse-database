﻿CREATE View [dbo].[RTTIncompletePathwayValidation]

as


Select
	Latest.Census
	,Latest.RTTPathwayID	
	,DistrictNo	
	,SpecialtyCode	
	,ConsultantCode	
	,ReferralDate	
	,ReferralSource	
	,Latest.StartDate	
	,StartReason	
	,BreachDate	
	,StartDisposalCode	
	,StartClinic	
	,StartEvSt	
	,LatestEncounterNo	
	,Latest.LatestDate	
	,LatestReason	
	,LatestEvSt	
	,LatestDisposalCode	
	,LatestClinic	
	,RTTWeeksWaiting	
	,DaysWaiting	
	,MonthsSinceLastEvent	
	,RTTCurrentStatusCode	
	,EventStatus	
	,Category		
	,OutcomeID	
	,OutcomeDescription
	,DateValidated
	,Comments
	,DocumentDescription
	,SignedTime
	,SignedStatusCode	
from
	RTTIncompletePathways Latest -- Current Incomplete Pathway

inner join RTTIncompletePathwaysValidationArchive ValidationOutcome
on Latest.RTTPathwayID = ValidationOutcome.RTTPathwayID 
and Latest.StartDate = ValidationOutcome.StartDate
and PreviouslyValidated is null

inner join RTT.ValidationOutcome ValidationDescription
on ValidationOutcome.OutcomeID = ValidationDescription.ID

left join WarehouseOLAPMergedV2.Dictation.BaseDocument
on  BaseDocument.SourcePatientNo = Latest.SourcePatientNo
and BaseDocument.DictatedTime = Latest.LatestDate
and DocumentTypeCode = 'O' 




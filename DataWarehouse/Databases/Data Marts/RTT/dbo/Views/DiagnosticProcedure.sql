﻿



CREATE view [dbo].[DiagnosticProcedure] as

select
	 ProcedureCode = EntityCode
	,ProcedureTypeCode = XrefEntityCode
from
	EntityXref
where
	EntityTypeCode = 'DIAGNOSTICPROCEDURE'
and	XrefEntityTypeCode = 'DIAGNOSTICPROCEDURETYPE'






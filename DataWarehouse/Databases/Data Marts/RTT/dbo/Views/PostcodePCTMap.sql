﻿CREATE view [dbo].[PostcodePCTMap] as

select
	Postcode
	,DOHCode = LocalAuthorityCode
	,PCTCode
from
	Organisation.dbo.Postcode
where
	PCTCode != 'X98'


﻿

CREATE view [dbo].[RTTFactMultiEpisodePathwayBase] as

select
	MatchMaster.CensusDate,
	Inpatient.EncounterRecno,

	coalesce(Inpatient.ConsultantCode, '##') ConsultantCode,
	coalesce(Inpatient.SpecialtyCode, '##') SpecialtyCode,

	coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode, '##') PracticeCode,

	coalesce(
		Practice.ParentOrganisationCode,
		PostcodePCTMap.PCTCode,
		'5J5'
	) PCTCode,

	coalesce(Inpatient.HospitalCode, '##') SiteCode,

	PathwayStatusCode =
	case
	when
		MatchMaster.InpatientRecno is not null
	and	InpatientRTTStatus.ClockStopFlag = 1
	then 'ACS' -- Admission Clock Stop

	--this will be removed once RTTPathways kick in
	else 'ADM' -- Inpatient Admission

	end,

	ClockStartDate = 
	dateadd(day,
		 coalesce(SocialSuspensionIP.DaysSuspended, RTTAdjustmentIP.AdjustmentDays, 0)
		,coalesce(
			 RTTClockStartIP.ClockStartDate
			,Inpatient.RTTStartDate
			,Inpatient.DateOnWaitinglist
		)
	),

	WorkflowStatusCode = 'C',

	KeyDate = coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate), -- Admission Clock Stop

	1 Cases,

	TCIBeyondBreach = 0,

	-- generate a WeekNumber e.g. "1 Week Ago", "2 Weeks Ago" etc for the date the treatment was completed
	LastWeeksCases =
	case
	when datediff(day, dateadd(day, -1, -- take a day off 
		Inpatient.AdmissionDate -- Inpatient Admission
		), MatchMaster.CensusDate) < 8 then 1 -- 14 Jan 2008 - PDO - changed as census run moved from Monday to Sunday
	else 0
	end 

	,coalesce(Inpatient.PrimaryDiagnosisCode, 'N/A') PrimaryDiagnosisCode
	,coalesce(Inpatient.PrimaryOperationCode, 'N/A') PrimaryOperationCode


	,ReportableFlag = 'Y'

	,UnadjustedClockStartDate = 
	coalesce(
		 Inpatient.RTTStartDate
		,RTTClockStartIP.ClockStartDate
		,Inpatient.DateOnWaitingList
		)

	,UnadjustedTCIBeyondBreach = 0

	,RTTBreachDate =
	dateadd(day, coalesce(RTTBreach.BreachValue, 126), 
		dateadd(day,
			coalesce(SocialSuspensionIP.DaysSuspended, RTTAdjustmentIP.AdjustmentDays, 0)
			,coalesce(
			 RTTClockStartIP.ClockStartDate
			,Inpatient.RTTStartDate
			,Inpatient.DateOnWaitingList
			)
		)
	)

	,ReferringProviderCode = left(Inpatient.RTTPathwayID, 3)

	,RTTFactTypeCode = 7

from
	dbo.Inpatient

inner join dbo.MatchMaster
on	MatchMaster.InpatientRecno = Inpatient.EncounterRecno

inner join dbo.RTTFact
on	RTTFact.CensusDate = MatchMaster.CensusDate
and	RTTFact.EncounterRecno = MatchMaster.ReferralRecno
and	RTTFact.EncounterTypeCode = 'REFERRAL'
and	RTTFact.AdjustedFlag = 'Y'
and	RTTFact.PathwayStatusCode = 'OPT'

left join dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = Inpatient.PrimaryOperationCode collate Latin1_General_CI_AS

left join dbo.OlapPractice Practice
on    Practice.OrganisationCode = coalesce(Inpatient.EpisodicGpPracticeCode, Inpatient.RegisteredGpPracticeCode)

left join dbo.PostcodePCTMap
on    PostcodePCTMap.Postcode = Inpatient.Postcode

left join dbo.RTTStatus InpatientRTTStatus
on    InpatientRTTStatus.RTTStatusCode = Inpatient.RTTPeriodStatusCode

inner join dbo.OlapSnapshot
on    OlapSnapshot.CensusDate = MatchMaster.CensusDate

left join dbo.RTTClockStart RTTClockStartIP
on    RTTClockStartIP.SourcePatientNo = Inpatient.SourcePatientNo
and   RTTClockStartIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
and   not exists
      (
      select
            1
      from
            dbo.RTTClockStart
      where
            RTTClockStart.SourcePatientNo = RTTClockStartIP.SourcePatientNo
      and   RTTClockStart.SourceEntityRecno = RTTClockStartIP.SourceEntityRecno
      and   (
                  RTTClockStart.ClockStartDate > RTTClockStartIP.ClockStartDate
            or
                  (
                        RTTClockStart.ClockStartDate = RTTClockStartIP.ClockStartDate
                  and   RTTClockStart.RTTClockStartRecno > RTTClockStartIP.RTTClockStartRecno
                  )
            )
      )

left join 
(
select
       RTTAdjustment.SourcePatientNo
      ,RTTAdjustment.SourceEntityRecno
      ,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
from
      dbo.RTTAdjustment
group by
       RTTAdjustment.SourcePatientNo
      ,RTTAdjustment.SourceEntityRecno
) RTTAdjustmentIP
on    RTTAdjustmentIP.SourcePatientNo = Inpatient.SourcePatientNo
and   RTTAdjustmentIP.SourceEntityRecno = Inpatient.SourceEncounterNo2

left join dbo.SocialSuspension SocialSuspensionIP
on    SocialSuspensionIP.SourcePatientNo = Inpatient.SourcePatientNo
and   SocialSuspensionIP.SourceEntityRecno = Inpatient.SourceEncounterNo2
and   SocialSuspensionIP.CensusDate = MatchMaster.CensusDate

left join dbo.EntityBreach RTTBreach
on	RTTBreach.EntityTypeCode = 'RTTBREACHDAYS'
and	RTTBreach.EntityCode = 'RTT'
and	coalesce(
		 RTTClockStartIP.ClockStartDate
		,Inpatient.RTTStartDate
		,Inpatient.DateOnWaitinglist
	) -- ClockStartDate
	between RTTBreach.FromDate and coalesce(RTTBreach.ToDate, getdate())

where
	Inpatient.AdmissionDate >= '1 Jan 2007'

and coalesce(Inpatient.RTTEndDate, Inpatient.AdmissionDate) <= MatchMaster.CensusDate
--and	RTTFact.KeyDate < Inpatient.AdmissionDate

and	DiagnosticProcedure.ProcedureCode is null --get rid of those admissions that have been reclassified as OPT due to diagnostic procedures




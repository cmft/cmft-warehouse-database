﻿
CREATE     view [dbo].[Match6NoMatch] as

SELECT  
	WkReferral.EncounterRecno SourceRecno,
	null MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkReferral

inner join MergeTemplate
on	MergeTemplate.MergeTemplateCode = 'Match6NoMatch'
and	MergeTemplate.Active = 1

where
	not exists
	(
	select
		1
	from
		TMergeData
	where
		WkReferral.EncounterRecno = TMergeData.SourceRecno	
	)



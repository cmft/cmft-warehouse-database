﻿create VIEW [dbo].[MatchRTTPathway] AS

SELECT  
	WkOutpatient.EncounterRecno SourceRecno,
	WkInpatientWL.EncounterRecno MatchRecno,
	MergeTemplate.MergeTemplateCode,
	MergeTemplate.Priority
FROM
	dbo.WkOutpatient

inner join dbo.WkInpatientWL ON 
	WkOutpatient.SourcePatientNo = WkInpatientWL.SourcePatientNo
AND	WkOutpatient.RTTPathwayID = WkInpatientWL.RTTPathwayID

INNER JOIN MergeTemplate ON
	MergeTemplate.MergeTemplateCode = 'MatchRTTPathway'
and	MergeTemplate.Active = 1


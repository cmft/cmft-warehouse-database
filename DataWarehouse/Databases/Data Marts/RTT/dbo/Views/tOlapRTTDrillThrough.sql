﻿
CREATE  view [dbo].[tOlapRTTDrillThrough]
with schemabinding
as

select
	 OlapRTTDrillThroughBase.ReferralRecno
	,OlapRTTDrillThroughBase.EncounterTypeCode
	,OlapRTTDrillThroughBase.CensusDate
	,OlapRTTDrillThroughBase.SourcePatientNo
	,OlapRTTDrillThroughBase.SourceEncounterNo2
	,OlapRTTDrillThroughBase.PatientForename
	,OlapRTTDrillThroughBase.PatientSurname
	,OlapRTTDrillThroughBase.DateOfBirth
	,OlapRTTDrillThroughBase.DateOfDeath
	,OlapRTTDrillThroughBase.SexCode
	,OlapRTTDrillThroughBase.NHSNumber
	,OlapRTTDrillThroughBase.PostCode
	,OlapRTTDrillThroughBase.Address1
	,OlapRTTDrillThroughBase.Address2
	,OlapRTTDrillThroughBase.Address3
	,OlapRTTDrillThroughBase.Address4
	,OlapRTTDrillThroughBase.Country
	,OlapRTTDrillThroughBase.DateOnWaitingList
	,OlapRTTDrillThroughBase.RegisteredGpCode
	,OlapRTTDrillThroughBase.PriorityCode
	,OlapRTTDrillThroughBase.EpisodicGpCode
	,OlapRTTDrillThroughBase.CasenoteNumber
	,OlapRTTDrillThroughBase.DistrictNo
	,OlapRTTDrillThroughBase.OutpatientRecno
	,OlapRTTDrillThroughBase.OutpatientProcedureCode
	,OlapRTTDrillThroughBase.ClinicCode
	,OlapRTTDrillThroughBase.SourceOfReferralCode
	,OlapRTTDrillThroughBase.AttendanceDate
	,OlapRTTDrillThroughBase.FirstAttendanceFlag
	,OlapRTTDrillThroughBase.DNAFlag
	,OlapRTTDrillThroughBase.AttendanceOutcomeCode
	,OlapRTTDrillThroughBase.DisposalCode
	,OlapRTTDrillThroughBase.PreviousDNAs
	,OlapRTTDrillThroughBase.PatientCancellations
	,OlapRTTDrillThroughBase.PreviousAppointmentDate
	,OlapRTTDrillThroughBase.PreviousAppointmentStatusCode
	,OlapRTTDrillThroughBase.NextAppointmentDate
	,OlapRTTDrillThroughBase.LastDnaOrPatientCancelledDate
	,OlapRTTDrillThroughBase.InpatientRecno
	,OlapRTTDrillThroughBase.AdmissionDate
	,OlapRTTDrillThroughBase.DischargeDate
	,OlapRTTDrillThroughBase.StartSiteCode
	,OlapRTTDrillThroughBase.StartWardType
	,OlapRTTDrillThroughBase.EndSiteCode
	,OlapRTTDrillThroughBase.EndWardType
	,OlapRTTDrillThroughBase.AdmissionMethodCode
	,OlapRTTDrillThroughBase.PatientClassificationCode
	,OlapRTTDrillThroughBase.PatientCategoryCode
	,OlapRTTDrillThroughBase.ManagementIntentionCode
	,OlapRTTDrillThroughBase.DischargeMethodCode
	,OlapRTTDrillThroughBase.DischargeDestinationCode
	,OlapRTTDrillThroughBase.AdminCategoryCode
	,OlapRTTDrillThroughBase.SpellConsultantCode
	,OlapRTTDrillThroughBase.SpellSpecialtyCode
	,OlapRTTDrillThroughBase.PrimaryDiagnosisCode
	,OlapRTTDrillThroughBase.SubsidiaryDiagnosisCode
	,OlapRTTDrillThroughBase.SecondaryDiagnosisCode1
	,OlapRTTDrillThroughBase.SecondaryDiagnosisCode2
	,OlapRTTDrillThroughBase.SecondaryDiagnosisCode3
	,OlapRTTDrillThroughBase.SecondaryDiagnosisCode4
	,OlapRTTDrillThroughBase.SecondaryDiagnosisCode5

	,PrimaryOperationCode = OlapRTTDrillThroughBase.IntendedPrimaryOperationCode

	,OlapRTTDrillThroughBase.PrimaryOperationDate
	,OlapRTTDrillThroughBase.SecondaryOperationCode1
	,OlapRTTDrillThroughBase.SecondaryOperationCode2
	,OlapRTTDrillThroughBase.SecondaryOperationCode3
	,OlapRTTDrillThroughBase.SecondaryOperationCode4
	,OlapRTTDrillThroughBase.SecondaryOperationCode5
	,OlapRTTDrillThroughBase.SecondaryOperationCode6
	,OlapRTTDrillThroughBase.SecondaryOperationCode7
	,OlapRTTDrillThroughBase.SecondaryOperationCode8
	,OlapRTTDrillThroughBase.SecondaryOperationCode9
	,OlapRTTDrillThroughBase.SecondaryOperationCode10
	,OlapRTTDrillThroughBase.SecondaryOperationCode11
	,OlapRTTDrillThroughBase.SecondaryOperationDate1
	,OlapRTTDrillThroughBase.SecondaryOperationDate2
	,OlapRTTDrillThroughBase.SecondaryOperationDate3
	,OlapRTTDrillThroughBase.SecondaryOperationDate4
	,OlapRTTDrillThroughBase.SecondaryOperationDate5
	,OlapRTTDrillThroughBase.SecondaryOperationDate6
	,OlapRTTDrillThroughBase.SecondaryOperationDate7
	,OlapRTTDrillThroughBase.SecondaryOperationDate8
	,OlapRTTDrillThroughBase.SecondaryOperationDate9
	,OlapRTTDrillThroughBase.SecondaryOperationDate10
	,OlapRTTDrillThroughBase.SecondaryOperationDate11
	,OlapRTTDrillThroughBase.CountOfDaysWaiting
	,OlapRTTDrillThroughBase.DateOnIPWaitingList
	,OlapRTTDrillThroughBase.DateOnFutureOPWaitingList
	,OlapRTTDrillThroughBase.BreachDate
	,OlapRTTDrillThroughBase.Quality
	,OlapRTTDrillThroughBase.Dataset
	,OlapRTTDrillThroughBase.Template
	,OlapRTTDrillThroughBase.OPProcedureCode
	,OlapRTTDrillThroughBase.AdjustedFlag
	,OlapRTTDrillThroughBase.RTTPathwayID
	,OlapRTTDrillThroughBase.RTTPathwayCondition
	,OlapRTTDrillThroughBase.RTTStartDate
	,OlapRTTDrillThroughBase.RTTEndDate
	,OlapRTTDrillThroughBase.RTTSpecialtyCode
	,OlapRTTDrillThroughBase.RTTCurrentProviderCode
	,OlapRTTDrillThroughBase.RTTCurrentStatusCode
	,OlapRTTDrillThroughBase.RTTCurrentStatusDate
	,OlapRTTDrillThroughBase.RTTCurrentPrivatePatientFlag
	,OlapRTTDrillThroughBase.RTTOverseasStatusFlag
	,OlapRTTDrillThroughBase.RTTPeriodStatusCode
	,OlapRTTDrillThroughBase.MaintenanceURL
	,OlapRTTDrillThroughBase.SourceEntityRecno
	,OlapRTTDrillThroughBase.SourceTypeCode
	,OlapRTTDrillThroughBase.ReferralDate

	,ReviewedFlag = coalesce(RTTEncounter.ReviewedFlag ,RTTOPEncounter.ReviewedFlag, 0)

	,ReviewedBy = 
		replace(
			coalesce(
				(
				select
					RTTClockStart.LastUser
				from
					dbo.RTTClockStart
				where
					RTTClockStart.SourcePatientNo = RTTEncounter.SourcePatientNo
				and	RTTClockStart.SourceEntityRecno = RTTEncounter.SourceEntityRecno
				and	not exists
					(
					select
						1
					from
						dbo.RTTClockStart Earlier
					where
						(
							Earlier.SourcePatientNo = RTTClockStart.SourcePatientNo
						and	Earlier.SourceEntityRecno = RTTClockStart.SourceEntityRecno
						and	coalesce(Earlier.Updated, Earlier.Created) < coalesce(RTTClockStart.Updated, RTTClockStart.Created)
						)
					or	(
							Earlier.SourcePatientNo = RTTClockStart.SourcePatientNo
						and	Earlier.SourceEntityRecno = RTTClockStart.SourceEntityRecno
						and	coalesce(Earlier.Updated, Earlier.Created) = coalesce(RTTClockStart.Updated, RTTClockStart.Created)
						and	Earlier.RTTClockStartRecno < RTTClockStart.RTTClockStartRecno
						)
					)
				)
				,(
				select
					RTTOPClockStop.LastUser
				from
					dbo.RTTOPClockStop
				where
					RTTOPClockStop.SourcePatientNo = RTTOPEncounter.SourcePatientNo
				and	RTTOPClockStop.SourceEntityRecno = RTTOPEncounter.SourceEntityRecno
				and	not exists
					(
					select
						1
					from
						dbo.RTTOPClockStop Earlier
					where
						(
							Earlier.SourcePatientNo = RTTOPClockStop.SourcePatientNo
						and	Earlier.SourceEntityRecno = RTTOPClockStop.SourceEntityRecno
						and	coalesce(Earlier.Updated, Earlier.Created) < coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
						)
					or	(
							Earlier.SourcePatientNo = RTTOPClockStop.SourcePatientNo
						and	Earlier.SourceEntityRecno = RTTOPClockStop.SourceEntityRecno
						and	coalesce(Earlier.Updated, Earlier.Created) = coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
						and	Earlier.RTTClockStopRecno < RTTOPClockStop.RTTClockStopRecno
						)
					)
				)
				,RTTEncounter.LastUser
				,RTTOPEncounter.LastUser
			)
			,'XPAT\'
			,''
		)

	,ReviewedTime = 
		coalesce(
			(
			select
				coalesce(RTTClockStart.Updated, RTTClockStart.Created)
			from
				dbo.RTTClockStart
			where
				RTTClockStart.SourcePatientNo = RTTEncounter.SourcePatientNo
			and	RTTClockStart.SourceEntityRecno = RTTEncounter.SourceEntityRecno
			and	not exists
				(
				select
					1
				from
					dbo.RTTClockStart Earlier
				where
					(
						Earlier.SourcePatientNo = RTTClockStart.SourcePatientNo
					and	Earlier.SourceEntityRecno = RTTClockStart.SourceEntityRecno
					and	coalesce(Earlier.Updated, Earlier.Created) < coalesce(RTTClockStart.Updated, RTTClockStart.Created)
					)
				or	(
						Earlier.SourcePatientNo = RTTClockStart.SourcePatientNo
					and	Earlier.SourceEntityRecno = RTTClockStart.SourceEntityRecno
					and	coalesce(Earlier.Updated, Earlier.Created) = coalesce(RTTClockStart.Updated, RTTClockStart.Created)
					and	Earlier.RTTClockStartRecno < RTTClockStart.RTTClockStartRecno
					)
				)
			)
			,(
			select
				coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
			from
				dbo.RTTOPClockStop
			where
				RTTOPClockStop.SourcePatientNo = RTTOPEncounter.SourcePatientNo
			and	RTTOPClockStop.SourceEntityRecno = RTTOPEncounter.SourceEntityRecno
			and	not exists
				(
				select
					1
				from
					dbo.RTTOPClockStop Earlier
				where
					(
						Earlier.SourcePatientNo = RTTOPClockStop.SourcePatientNo
					and	Earlier.SourceEntityRecno = RTTOPClockStop.SourceEntityRecno
					and	coalesce(Earlier.Updated, Earlier.Created) < coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
					)
				or	(
						Earlier.SourcePatientNo = RTTOPClockStop.SourcePatientNo
					and	Earlier.SourceEntityRecno = RTTOPClockStop.SourceEntityRecno
					and	coalesce(Earlier.Updated, Earlier.Created) = coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
					and	Earlier.RTTClockStopRecno < RTTOPClockStop.RTTClockStopRecno
					)
				)
			)
			,RTTEncounter.Updated
			,RTTOPEncounter.Updated
			,RTTEncounter.Created
			,RTTOPEncounter.Created
		)

--	,ReviewedBy =  replace(coalesce(RTTEncounter.LastUser ,RTTOPEncounter.LastUser), 'XPAT\', '')
--	,ReviewedTime =  coalesce(RTTEncounter.Updated ,RTTOPEncounter.Updated, RTTEncounter.Created ,RTTOPEncounter.Created)

	,SharedBreachFlag =  
		coalesce(
			 RTTEncounter.SharedBreachFlag
			,RTTOPEncounter.SharedBreachFlag
			,0
		)
from
	dbo.OlapRTTDrillThroughBase

left join dbo.RTTEncounter
on	RTTEncounter.SourcePatientNo = OlapRTTDrillThroughBase.SourcePatientNo
and	RTTEncounter.SourceEntityRecno = OlapRTTDrillThroughBase.SourceEntityRecno
and	OlapRTTDrillThroughBase.SourceTypeCode = 'I'

left join dbo.RTTOPEncounter
on	RTTOPEncounter.SourcePatientNo = OlapRTTDrillThroughBase.SourcePatientNo
and	RTTOPEncounter.SourceEntityRecno = OlapRTTDrillThroughBase.SourceEntityRecno
and	OlapRTTDrillThroughBase.SourceTypeCode = 'O'



﻿create view [dbo].[ExcludedSourceOfReferral] as

select
	SourceOfReferralCode = EntityCode
from
	EntityLookup
where
	EntityTypeCode = 'EXCLUDEDSOR'

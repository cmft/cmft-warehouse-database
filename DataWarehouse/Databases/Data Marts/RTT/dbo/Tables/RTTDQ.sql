﻿CREATE TABLE [dbo].[RTTDQ] (
    [DistrictNo]   VARCHAR (20)   NULL,
    [RTTPathwayID] VARCHAR (25)   NULL,
    [EventDate]    SMALLDATETIME  NULL,
    [EventTime]    DATETIME       NULL,
    [EventType]    NVARCHAR (100) NULL,
    [EventStatus]  NVARCHAR (100) NULL,
    [ClinicCode]   NVARCHAR (100) NULL,
    [Description]  NVARCHAR (200) NULL
);


﻿CREATE TABLE [dbo].[MergeTemplate_CopyOfOriginal] (
    [MergeTemplateRecno] INT          IDENTITY (1, 1) NOT NULL,
    [MergeTemplateCode]  VARCHAR (50) NOT NULL,
    [MergeTemplate]      VARCHAR (50) NULL,
    [Priority]           INT          NOT NULL,
    [Active]             BIT          NOT NULL,
    [MergeDatasetCode]   VARCHAR (10) NOT NULL
);


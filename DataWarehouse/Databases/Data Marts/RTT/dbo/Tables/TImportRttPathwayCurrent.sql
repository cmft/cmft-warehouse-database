﻿CREATE TABLE [dbo].[TImportRttPathwayCurrent] (
    [RTTPTPATHWAYCURRENTID] VARCHAR (254) NOT NULL,
    [InternalPatientNumber] VARCHAR (9)   NOT NULL,
    [PathwayCondition]      VARCHAR (20)  NULL,
    [PathwayNumber]         VARCHAR (25)  NOT NULL,
    [PathwayOrgProv]        VARCHAR (5)   NULL,
    [RttCurProv]            VARCHAR (4)   NULL,
    [RttCurrentStatus]      VARCHAR (4)   NULL,
    [RttCurrentStatusDate]  VARCHAR (10)  NULL,
    [RttEndDate]            VARCHAR (10)  NULL,
    [RttEndDateInt]         VARCHAR (8)   NULL,
    [RttOsvStatus]          VARCHAR (1)   NULL,
    [RttPrivatePat]         VARCHAR (1)   NULL,
    [RttSpeciality]         VARCHAR (4)   NULL,
    [RttStartDate]          VARCHAR (10)  NULL,
    [RttStartDateInt]       VARCHAR (8)   NULL
);


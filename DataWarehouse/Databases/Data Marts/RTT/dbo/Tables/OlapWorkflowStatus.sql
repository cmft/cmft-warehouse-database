﻿CREATE TABLE [dbo].[OlapWorkflowStatus] (
    [WorkflowStatusCode] VARCHAR (10)  NOT NULL,
    [WorkflowStatus]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_OlapWorkflowStatus] PRIMARY KEY CLUSTERED ([WorkflowStatusCode] ASC)
);


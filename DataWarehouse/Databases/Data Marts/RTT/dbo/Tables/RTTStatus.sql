﻿CREATE TABLE [dbo].[RTTStatus] (
    [RTTStatusCode] VARCHAR (10) NOT NULL,
    [RTTStatus]     VARCHAR (30) NULL,
    [InternalCode]  VARCHAR (10) NULL,
    [IsDefaultCode] VARCHAR (10) NULL,
    [ClockStopFlag] BIT          NULL,
    CONSTRAINT [PK_RTTStatus] PRIMARY KEY CLUSTERED ([RTTStatusCode] ASC)
);


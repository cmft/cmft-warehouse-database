﻿CREATE TABLE [dbo].[WkReferral] (
    [EncounterRecno]     INT           NOT NULL,
    [SourcePatientNo]    VARCHAR (20)  NULL,
    [SourceEncounterNo2] INT           NULL,
    [ConsultantCode]     VARCHAR (10)  NULL,
    [SpecialtyCode]      VARCHAR (5)   NULL,
    [HospitalCode]       VARCHAR (5)   NULL,
    [ReferralDate]       SMALLDATETIME NULL,
    [RTTPathwayID]       VARCHAR (25)  NULL,
    CONSTRAINT [PK_WkReferral] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_WkReferral]
    ON [dbo].[WkReferral]([SourcePatientNo] ASC, [SourceEncounterNo2] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WkReferral_1]
    ON [dbo].[WkReferral]([ReferralDate] ASC);


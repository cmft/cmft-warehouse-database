﻿CREATE TABLE [dbo].[TImportRfEncounter] (
    [ImportRfEncounterRecno]        VARCHAR (20)  NULL,
    [SourcePatientNo]               VARCHAR (20)  NULL,
    [SourceSpellNo]                 VARCHAR (20)  NULL,
    [SourceEncounterNo]             VARCHAR (50)  NULL,
    [SourceEncounterNo2]            VARCHAR (50)  NULL,
    [PatientTitle]                  VARCHAR (10)  NULL,
    [PatientForename]               VARCHAR (20)  NULL,
    [PatientSurname]                VARCHAR (30)  NULL,
    [PatientNameFormatCode]         CHAR (1)      NULL,
    [DateOfBirth]                   DATETIME      NULL,
    [DateOfDeath]                   SMALLDATETIME NULL,
    [SexCode]                       CHAR (1)      NULL,
    [NHSNumber]                     CHAR (17)     NULL,
    [PostCode]                      CHAR (10)     NULL,
    [Address1]                      VARCHAR (25)  NULL,
    [Address2]                      VARCHAR (25)  NULL,
    [Address3]                      VARCHAR (25)  NULL,
    [Address4]                      VARCHAR (25)  NULL,
    [Country]                       VARCHAR (15)  NULL,
    [DhaCode]                       CHAR (3)      NULL,
    [EthnicOriginCode]              CHAR (5)      NULL,
    [MaritalStatusCode]             CHAR (2)      NULL,
    [ReligionCode]                  CHAR (5)      NULL,
    [DisabledCode]                  CHAR (1)      NULL,
    [OverseasAreaCode]              CHAR (5)      NULL,
    [ReferringGpCode]               CHAR (8)      NULL,
    [ReferringGpStatusCode]         CHAR (1)      NULL,
    [ReferringOrganisationCode]     CHAR (8)      NULL,
    [RegisteredGpCode]              CHAR (8)      NULL,
    [RegisteredGpStatusCode]        CHAR (1)      NULL,
    [RegisteredGpPracticeCode]      CHAR (8)      NULL,
    [HospitalCode]                  VARCHAR (5)   NULL,
    [PatientCategoryCode]           CHAR (2)      NULL,
    [ChronicallySickCode]           CHAR (1)      NULL,
    [ConsultantCode]                CHAR (10)     NULL,
    [SpecialtyCode]                 CHAR (5)      NULL,
    [PurchaserReferenceNo]          VARCHAR (17)  NULL,
    [RecordType]                    CHAR (2)      NULL,
    [CasemixGroupCode]              CHAR (3)      NULL,
    [DiagnosisClassificationCode]   CHAR (5)      NULL,
    [PrimaryDiagnosisCode]          CHAR (10)     NULL,
    [SubsidiaryDiagnosisCode]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode1]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode2]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode3]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode4]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode5]       VARCHAR (10)  NULL,
    [PrimaryOperationCode]          VARCHAR (10)  NULL,
    [PrimaryOperationDate]          SMALLDATETIME NULL,
    [SecondaryOperationCode1]       VARCHAR (10)  NULL,
    [SecondaryOperationDate1]       SMALLDATETIME NULL,
    [SecondaryOperationCode2]       VARCHAR (10)  NULL,
    [SecondaryOperationDate2]       SMALLDATETIME NULL,
    [SecondaryOperationCode3]       VARCHAR (10)  NULL,
    [SecondaryOperationDate3]       SMALLDATETIME NULL,
    [SecondaryOperationCode4]       VARCHAR (10)  NULL,
    [SecondaryOperationDate4]       SMALLDATETIME NULL,
    [SecondaryOperationCode5]       VARCHAR (10)  NULL,
    [SecondaryOperationDate5]       SMALLDATETIME NULL,
    [SecondaryOperationCode6]       VARCHAR (10)  NULL,
    [SecondaryOperationDate6]       SMALLDATETIME NULL,
    [SecondaryOperationCode7]       VARCHAR (10)  NULL,
    [SecondaryOperationDate7]       SMALLDATETIME NULL,
    [SecondaryOperationCode8]       VARCHAR (10)  NULL,
    [SecondaryOperationDate8]       SMALLDATETIME NULL,
    [SecondaryOperationCode9]       VARCHAR (10)  NULL,
    [SecondaryOperationDate9]       SMALLDATETIME NULL,
    [SecondaryOperationCode10]      VARCHAR (10)  NULL,
    [SecondaryOperationDate10]      SMALLDATETIME NULL,
    [SecondaryOperationCode11]      VARCHAR (10)  NULL,
    [SecondaryOperationDate11]      SMALLDATETIME NULL,
    [OperationStatusCode]           CHAR (1)      NULL,
    [AuthorisationCode]             VARCHAR (25)  NULL,
    [ClinicCode]                    VARCHAR (8)   NULL,
    [SourceOfReferralCode]          VARCHAR (5)   NULL,
    [ReasonForReferralCode]         VARCHAR (5)   NULL,
    [AppointmentRequestDate]        SMALLDATETIME NULL,
    [ReferrerDiagnosis]             VARCHAR (20)  NULL,
    [ReferralTypeCode]              VARCHAR (5)   NULL,
    [GPReferralNo]                  VARCHAR (10)  NULL,
    [AttendanceDate]                SMALLDATETIME NULL,
    [FirstAttendanceFlag]           CHAR (1)      NULL,
    [DNAFlag]                       CHAR (1)      NULL,
    [AttendanceOutcomeCode]         VARCHAR (5)   NULL,
    [TransportRequiredFlag]         CHAR (2)      NULL,
    [ReferralDate]                  SMALLDATETIME NULL,
    [ArrivalTime]                   CHAR (5)      NULL,
    [DisposalTime]                  CHAR (5)      NULL,
    [AppointmentChanges]            CHAR (2)      NULL,
    [AppointmentTypeCode]           VARCHAR (5)   NULL,
    [DisposalCode]                  VARCHAR (5)   NULL,
    [StaffGradeCode]                VARCHAR (5)   NULL,
    [PriorityCode]                  VARCHAR (5)   NULL,
    [AppointmentCreateDate]         SMALLDATETIME NULL,
    [AppointmentCreatedBy]          VARCHAR (10)  NULL,
    [AppointmentDate]               SMALLDATETIME NULL,
    [AppointmentStatusCode]         VARCHAR (5)   NULL,
    [PreviousDNAs]                  CHAR (2)      NULL,
    [PatientCancellations]          CHAR (2)      NULL,
    [PreviousAppointmentDate]       SMALLDATETIME NULL,
    [PreviousAppointmentStatusCode] VARCHAR (5)   NULL,
    [NextAppointmentDate]           SMALLDATETIME NULL,
    [AttendanceLocationTypeCode]    VARCHAR (5)   NULL,
    [CarerSupportCode]              CHAR (2)      NULL,
    [MedicalStaffTypeSeeingPatient] CHAR (2)      NULL,
    [LastDnaOrPatientCancelledDate] SMALLDATETIME NULL,
    [ContractSerialNo]              VARCHAR (6)   NULL,
    [CodingComplete]                CHAR (1)      NULL,
    [UserId]                        SMALLINT      NULL,
    [SourcePurchaserCode]           VARCHAR (10)  NULL,
    [SourceProviderCode]            VARCHAR (5)   NULL,
    [SourceContractSerialNo]        VARCHAR (10)  NULL,
    [SourceServiceCode]             VARCHAR (10)  NULL,
    [ClinicPurposeCode]             VARCHAR (15)  NULL,
    [PurchaserCode]                 VARCHAR (10)  NULL,
    [InvoiceNo]                     VARCHAR (10)  NULL,
    [InvoiceAmount]                 VARCHAR (20)  NULL,
    [ContractPrice]                 VARCHAR (20)  NULL,
    [BaseServiceCode]               VARCHAR (15)  NULL,
    [PurchasedServiceCode]          VARCHAR (15)  NULL,
    [GLChargeCode]                  VARCHAR (10)  NULL,
    [NegotiatingOrganisationCode]   VARCHAR (8)   NULL,
    [PaymentStatusCode]             CHAR (1)      NULL,
    [PaymentDate]                   SMALLDATETIME NULL,
    [Volume]                        VARCHAR (20)  NULL,
    [TextField1]                    VARCHAR (255) NULL,
    [TextField2]                    VARCHAR (255) NULL,
    [TextField3]                    VARCHAR (255) NULL,
    [DateField1]                    SMALLDATETIME NULL,
    [DateField2]                    SMALLDATETIME NULL,
    [DateField3]                    SMALLDATETIME NULL,
    [NumericField1]                 VARCHAR (10)  NULL,
    [NumericField2]                 VARCHAR (10)  NULL,
    [NumericField3]                 VARCHAR (10)  NULL,
    [ProviderCode]                  VARCHAR (8)   NULL,
    [RegisteredGdpCode]             VARCHAR (8)   NULL,
    [AllocationStatusCode]          CHAR (1)      NULL,
    [ClinicConsultantCode]          VARCHAR (6)   NULL,
    [ClinicSpecialtyCode]           VARCHAR (4)   NULL,
    [InterfaceCode]                 CHAR (4)      NULL,
    [SourceAdminCategoryCode]       VARCHAR (5)   NULL,
    [SourceHospitalCode]            VARCHAR (4)   NULL,
    [EpisodicGpCode]                VARCHAR (8)   NULL,
    [EpisodicGpPracticeCode]        VARCHAR (6)   NULL,
    [DistrictNo]                    VARCHAR (12)  NULL,
    [CasenoteNo]                    VARCHAR (12)  NULL,
    [BookingTypeCode]               VARCHAR (3)   NULL,
    [TreatmentFunctionCode]         VARCHAR (4)   NULL,
    [SourceTreatmentFunctionCode]   VARCHAR (4)   NULL,
    [NationalSpecialtyCode]         VARCHAR (4)   NULL,
    [AttendanceTime]                SMALLDATETIME NULL,
    [SourceEthnicOriginCode]        VARCHAR (5)   NULL,
    [DischargeDate]                 SMALLDATETIME NULL,
    [SourceKey]                     VARCHAR (50)  NULL
);


﻿CREATE TABLE [dbo].[EncounterRecnoMap] (
    [EncounterRecno]       INT      IDENTITY (1, 1) NOT NULL,
    [EncounterTypeCode]    CHAR (2) NULL,
    [SourceEncounterRecno] INT      NULL,
    CONSTRAINT [PK_EncounterRecnoMap] PRIMARY KEY NONCLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE UNIQUE CLUSTERED INDEX [IX_EncounterRecnoMap]
    ON [dbo].[EncounterRecnoMap]([SourceEncounterRecno] ASC, [EncounterTypeCode] ASC);


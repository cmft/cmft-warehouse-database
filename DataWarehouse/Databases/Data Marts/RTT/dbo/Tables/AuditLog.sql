﻿CREATE TABLE [dbo].[AuditLog] (
    [EventTime] DATETIME      NOT NULL,
    [UserId]    VARCHAR (30)  NOT NULL,
    [Source]    CHAR (30)     NOT NULL,
    [Event]     VARCHAR (255) NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_AuditLog]
    ON [dbo].[AuditLog]([EventTime] ASC) WITH (FILLFACTOR = 90);


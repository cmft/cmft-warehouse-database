﻿CREATE TABLE [dbo].[WaitReason] (
    [WaitReasonCode] VARCHAR (10)  NOT NULL,
    [WaitReason]     VARCHAR (255) NULL,
    CONSTRAINT [PK_WaitReason] PRIMARY KEY CLUSTERED ([WaitReasonCode] ASC)
);


﻿CREATE TABLE [dbo].[OPDisposal] (
    [OPDisposalCode] VARCHAR (10)  NOT NULL,
    [OPDisposal]     VARCHAR (200) NOT NULL,
    [ClockStopFlag]  BIT           NULL,
    CONSTRAINT [PK_OPDisposal] PRIMARY KEY CLUSTERED ([OPDisposalCode] ASC)
);


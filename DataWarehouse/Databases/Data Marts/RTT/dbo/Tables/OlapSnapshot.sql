﻿CREATE TABLE [dbo].[OlapSnapshot] (
    [CensusDate]          SMALLDATETIME NOT NULL,
    [FormattedCensusDate] VARCHAR (11)  NULL,
    CONSTRAINT [PK_Snapshot] PRIMARY KEY CLUSTERED ([CensusDate] ASC) WITH (FILLFACTOR = 90)
);


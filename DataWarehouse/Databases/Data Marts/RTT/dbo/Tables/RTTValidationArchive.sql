﻿CREATE TABLE [dbo].[RTTValidationArchive] (
    [Census]              DATETIME      NOT NULL,
    [RTTPathwayID]        VARCHAR (25)  NOT NULL,
    [EventTime]           SMALLDATETIME NOT NULL,
    [EventType]           VARCHAR (50)  NULL,
    [OutcomeCode]         VARCHAR (20)  NULL,
    [OutcomeID]           INT           NOT NULL,
    [DateValidated]       DATETIME      NULL,
    [Comments]            VARCHAR (250) NULL,
    [PreviouslyValidated] VARCHAR (1)   NULL,
    [UserName]            VARCHAR (50)  NULL
);


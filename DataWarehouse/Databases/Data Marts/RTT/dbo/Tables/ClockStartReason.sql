﻿CREATE TABLE [dbo].[ClockStartReason] (
    [ClockStartReasonCode] VARCHAR (10) NOT NULL,
    [ClockStartReason]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ClockStartReason] PRIMARY KEY CLUSTERED ([ClockStartReasonCode] ASC)
);


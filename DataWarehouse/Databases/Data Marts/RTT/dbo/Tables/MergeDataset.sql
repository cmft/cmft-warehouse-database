﻿CREATE TABLE [dbo].[MergeDataset] (
    [MergeDatasetCode] VARCHAR (50) NOT NULL,
    [MergeDataset]     VARCHAR (50) NULL,
    [Priority]         INT          NULL,
    CONSTRAINT [PK_MergeDataset] PRIMARY KEY CLUSTERED ([MergeDatasetCode] ASC)
);


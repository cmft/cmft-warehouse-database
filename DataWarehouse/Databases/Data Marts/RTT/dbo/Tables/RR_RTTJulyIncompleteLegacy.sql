﻿CREATE TABLE [dbo].[RR_RTTJulyIncompleteLegacy] (
    [CensusDate]            DATETIME     NOT NULL,
    [SourceUniqueID]        VARCHAR (50) NOT NULL,
    [EncounterTypeCode]     VARCHAR (10) NULL,
    [ConsultantCode]        VARCHAR (10) NULL,
    [SpecialtyCode]         VARCHAR (5)  NULL,
    [NationalSpecialtyCode] VARCHAR (5)  NULL,
    [PracticeCode]          VARCHAR (8)  NULL,
    [PCTCode]               VARCHAR (50) NULL,
    [SiteCode]              VARCHAR (50) NOT NULL,
    [WeekBandCode]          VARCHAR (5)  NOT NULL,
    [PathwayStatusCode]     VARCHAR (3)  NOT NULL,
    [ReferralDate]          DATETIME     NULL,
    [WorkflowStatusCode]    CHAR (1)     NOT NULL,
    [BreachBandCode]        VARCHAR (50) NOT NULL,
    [DaysWaiting]           INT          NOT NULL,
    [Cases]                 INT          NOT NULL,
    [TCIBeyondBreach]       INT          NULL,
    [LastWeeksCases]        SMALLINT     NULL,
    [PrimaryDiagnosisCode]  VARCHAR (10) NULL,
    [PrimaryOperationCode]  VARCHAR (10) NULL,
    [RTTCurrentStatusCode]  VARCHAR (10) NULL,
    [RTTPeriodStatusCode]   VARCHAR (10) NULL,
    [ReportableFlag]        VARCHAR (10) NOT NULL,
    [AdjustedFlag]          VARCHAR (10) NOT NULL,
    [RTTBreachDate]         DATETIME     NULL,
    [KeyDate]               DATETIME     NULL,
    [ReferringProviderCode] VARCHAR (5)  NULL,
    [RTTFactTypeCode]       INT          NULL,
    [Division]              VARCHAR (50) NULL,
    [Directorate]           VARCHAR (50) NULL,
    [TCIDate]               DATETIME     NULL,
    [WeekBandReturnCode]    VARCHAR (5)  NULL,
    [DecisionToTreatFlag]   BIT          NULL,
    [WaitingListFlag]       BIT          NULL,
    [DiagnosticFlag]        BIT          NULL,
    [EncounterRecno]        INT          NOT NULL,
    [SourceCensusDate]      DATETIME     NULL,
    [breach]                VARCHAR (4)  NOT NULL
);


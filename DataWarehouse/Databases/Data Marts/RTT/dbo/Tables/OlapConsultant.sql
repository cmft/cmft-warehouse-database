﻿CREATE TABLE [dbo].[OlapConsultant] (
    [ConsultantCode] VARCHAR (6)  NOT NULL,
    [Consultant]     VARCHAR (39) NULL,
    CONSTRAINT [PK_OlapConsultant] PRIMARY KEY CLUSTERED ([ConsultantCode] ASC)
);


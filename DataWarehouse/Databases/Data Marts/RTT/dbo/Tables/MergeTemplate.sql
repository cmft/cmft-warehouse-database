﻿CREATE TABLE [dbo].[MergeTemplate] (
    [MergeTemplateRecno] FLOAT (53)     NULL,
    [MergeTemplateCode]  NVARCHAR (255) NULL,
    [MergeTemplate]      NVARCHAR (255) NULL,
    [Priority]           FLOAT (53)     NULL,
    [Active]             FLOAT (53)     NULL,
    [MergeDatasetCode]   NVARCHAR (255) NULL
);


﻿CREATE TABLE [dbo].[MatchMaster] (
    [CensusDate]              SMALLDATETIME NOT NULL,
    [ReferralRecno]           INT           NOT NULL,
    [OutpatientRecno]         INT           NULL,
    [InpatientWLRecno]        INT           NULL,
    [InpatientRecno]          INT           NULL,
    [FutureOPRecno]           INT           NULL,
    [IPToFutureOPRecno]       INT           NULL,
    [AttIPWLTemplateRecno]    INT           NULL,
    [AttSpellTemplateRecno]   INT           NULL,
    [SpellOPWLTemplateRecno]  INT           NULL,
    [RefIPWLTemplateRecno]    INT           NULL,
    [RefSpellTemplateRecno]   INT           NULL,
    [InpatientWLAllRecno]     INT           NULL,
    [AttIPWLAllTemplateRecno] INT           NULL,
    [RefIPWLAllTemplateRecno] INT           NULL,
    CONSTRAINT [PK_MatchMaster] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [ReferralRecno] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_MatchMaster]
    ON [dbo].[MatchMaster]([CensusDate] ASC, [OutpatientRecno] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_MatchMaster_1]
    ON [dbo].[MatchMaster]([CensusDate] ASC, [InpatientWLRecno] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_MatchMaster_2]
    ON [dbo].[MatchMaster]([CensusDate] ASC, [InpatientRecno] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_MatchMaster_3]
    ON [dbo].[MatchMaster]([CensusDate] ASC, [IPToFutureOPRecno] ASC) WITH (FILLFACTOR = 90);


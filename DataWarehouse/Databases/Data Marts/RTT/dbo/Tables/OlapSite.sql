﻿CREATE TABLE [dbo].[OlapSite] (
    [SiteCode]  VARCHAR (10)  NOT NULL,
    [Site]      VARCHAR (255) NOT NULL,
    [TrustCode] VARCHAR (5)   NOT NULL,
    [Trust]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_OlapSite] PRIMARY KEY CLUSTERED ([SiteCode] ASC)
);


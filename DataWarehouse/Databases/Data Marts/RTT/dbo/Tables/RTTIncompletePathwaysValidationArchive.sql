﻿CREATE TABLE [dbo].[RTTIncompletePathwaysValidationArchive] (
    [Census]              DATETIME      NOT NULL,
    [RTTPathwayID]        VARCHAR (25)  NOT NULL,
    [StartDate]           SMALLDATETIME NOT NULL,
    [LatestDate]          SMALLDATETIME NULL,
    [OutcomeID]           INT           NOT NULL,
    [DateValidated]       DATETIME      NULL,
    [Comments]            VARCHAR (250) NULL,
    [PreviouslyValidated] VARCHAR (1)   NULL
);


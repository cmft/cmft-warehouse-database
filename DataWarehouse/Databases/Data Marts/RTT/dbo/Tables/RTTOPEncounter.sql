﻿CREATE TABLE [dbo].[RTTOPEncounter] (
    [SourcePatientNo]   VARCHAR (50) NOT NULL,
    [SourceEntityRecno] VARCHAR (50) NOT NULL,
    [ReviewedFlag]      BIT          NULL,
    [Created]           DATETIME     NULL,
    [Updated]           DATETIME     NULL,
    [LastUser]          VARCHAR (50) NULL,
    [SharedBreachFlag]  BIT          NULL,
    CONSTRAINT [PK_RTTOPEncounter] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [SourceEntityRecno] ASC)
);


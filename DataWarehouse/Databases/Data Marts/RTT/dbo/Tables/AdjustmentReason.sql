﻿CREATE TABLE [dbo].[AdjustmentReason] (
    [AdjustmentReasonCode] VARCHAR (10) NOT NULL,
    [AdjustmentReason]     VARCHAR (50) NULL,
    CONSTRAINT [PK_AdjustmentReason] PRIMARY KEY CLUSTERED ([AdjustmentReasonCode] ASC)
);


﻿CREATE TABLE [dbo].[RTTFactType] (
    [RTTFactTypeCode] INT           NOT NULL,
    [RTTFactType]     VARCHAR (100) NULL,
    [RTTFactTypeDesc] VARCHAR (MAX) NULL
);


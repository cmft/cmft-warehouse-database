﻿CREATE TABLE [dbo].[TRTTAdjustment] (
    [SourcePatientNo]   VARCHAR (50) NOT NULL,
    [SourceEntityRecno] VARCHAR (50) NOT NULL,
    [AdjustmentDays]    INT          NULL,
    CONSTRAINT [PK_TRTTAdjustment] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [SourceEntityRecno] ASC)
);


﻿CREATE TABLE [dbo].[OlapSpecialty] (
    [SpecialtyCode]         VARCHAR (6)   NOT NULL,
    [Specialty]             VARCHAR (100) NULL,
    [NationalSpecialtyCode] VARCHAR (6)   NULL,
    [NationalSpecialty]     VARCHAR (100) NULL,
    [RTTSpecialtyCode]      VARCHAR (5)   NULL,
    [RTTSpecialty]          VARCHAR (100) NULL,
    CONSTRAINT [PK_OlapSpecialty] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


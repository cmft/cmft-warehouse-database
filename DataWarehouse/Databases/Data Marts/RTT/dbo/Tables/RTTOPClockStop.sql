﻿CREATE TABLE [dbo].[RTTOPClockStop] (
    [RTTClockStopRecno]      INT            IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]        VARCHAR (50)   NOT NULL,
    [SourceEntityRecno]      VARCHAR (50)   NOT NULL,
    [ClockStopDate]          SMALLDATETIME  NULL,
    [ClockStopReasonCode]    VARCHAR (10)   NULL,
    [ClockStopComment]       VARCHAR (4000) NULL,
    [Created]                DATETIME       NULL,
    [Updated]                DATETIME       NULL,
    [LastUser]               VARCHAR (50)   NULL,
    [ClockStartDate]         SMALLDATETIME  NULL,
    [ClockStartReasonCode]   VARCHAR (10)   NULL,
    [WaitReasonCode]         VARCHAR (10)   NULL,
    [WaitReasonDate]         SMALLDATETIME  NULL,
    [ClockStopReportingDate] SMALLDATETIME  NULL,
    [RTTUserModeCode]        VARCHAR (10)   NOT NULL,
    CONSTRAINT [PK_RTTOPClockStop] PRIMARY KEY NONCLUSTERED ([RTTClockStopRecno] ASC),
    CONSTRAINT [FK_RTTOPClockStop_ClockStopReason] FOREIGN KEY ([ClockStopReasonCode]) REFERENCES [dbo].[ClockStopReason] ([ClockStopReasonCode]),
    CONSTRAINT [FK_RTTOPClockStop_RTTOPEncounter] FOREIGN KEY ([SourcePatientNo], [SourceEntityRecno]) REFERENCES [dbo].[RTTOPEncounter] ([SourcePatientNo], [SourceEntityRecno]),
    CONSTRAINT [FK_RTTOPClockStop_RTTUserMode] FOREIGN KEY ([RTTUserModeCode]) REFERENCES [dbo].[RTTUserMode] ([RTTUserModeCode]),
    CONSTRAINT [FK_RTTOPClockStop_WaitReason] FOREIGN KEY ([WaitReasonCode]) REFERENCES [dbo].[WaitReason] ([WaitReasonCode])
);


GO
CREATE CLUSTERED INDEX [IX_RTTOPClockStop]
    ON [dbo].[RTTOPClockStop]([SourcePatientNo] ASC, [SourceEntityRecno] ASC);


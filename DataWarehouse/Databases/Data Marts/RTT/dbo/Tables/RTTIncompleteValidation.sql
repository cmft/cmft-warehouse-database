﻿CREATE TABLE [dbo].[RTTIncompleteValidation] (
    [Census]                  DATETIME       NULL,
    [RTTPathwayID]            NVARCHAR (255) NULL,
    [StartDate]               DATETIME       NULL,
    [LatestDate]              DATETIME       NULL,
    [LatestReason]            NVARCHAR (255) NULL,
    [Validation]              NVARCHAR (255) NULL,
    [Action]                  NVARCHAR (255) NULL,
    [ValidationDate]          DATETIME       NULL,
    [Comments(1stValidation)] NVARCHAR (255) NULL,
    [Comments(DWValidation)]  NVARCHAR (255) NULL,
    [ValidationType]          VARCHAR (20)   NULL,
    [PreviousValidationDate]  VARCHAR (200)  NULL
);


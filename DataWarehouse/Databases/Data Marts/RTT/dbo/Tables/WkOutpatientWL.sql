﻿CREATE TABLE [dbo].[WkOutpatientWL] (
    [EncounterRecno]    INT           NOT NULL,
    [EncounterEndDate]  SMALLDATETIME NULL,
    [SourcePatientNo]   VARCHAR (20)  NULL,
    [HospitalCode]      VARCHAR (5)   NULL,
    [ConsultantCode]    CHAR (10)     NULL,
    [SpecialtyCode]     CHAR (5)      NULL,
    [ReferralDate]      SMALLDATETIME NULL,
    [SourceEntityRecno] INT           NULL,
    [RTTPathwayID]      VARCHAR (25)  NULL,
    CONSTRAINT [PK_WkOutpatientWL] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


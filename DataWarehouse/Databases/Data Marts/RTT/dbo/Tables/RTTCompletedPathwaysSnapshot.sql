﻿CREATE TABLE [dbo].[RTTCompletedPathwaysSnapshot] (
    [DistrictNo]       VARCHAR (20)  NULL,
    [SourcePatientNo]  VARCHAR (20)  NULL,
    [RTTPathwayID]     VARCHAR (25)  NULL,
    [SpecCode]         VARCHAR (3)   NULL,
    [SpecialtyCode]    VARCHAR (10)  NULL,
    [ConsultantCode]   VARCHAR (10)  NULL,
    [ReferralDate]     SMALLDATETIME NULL,
    [IPWLAdd]          SMALLDATETIME NULL,
    [StartEncounterNo] VARCHAR (50)  NOT NULL,
    [StartID]          BIGINT        NULL,
    [StartDate]        SMALLDATETIME NULL,
    [StartReason]      NVARCHAR (50) NULL,
    [StopEncounterNo]  VARCHAR (50)  NOT NULL,
    [StopID]           BIGINT        NULL,
    [StopDate]         SMALLDATETIME NULL,
    [StopReason]       NVARCHAR (50) NULL,
    [MonthOfStop]      NVARCHAR (7)  NULL,
    [EventType]        VARCHAR (4)   NOT NULL,
    [Category]         VARCHAR (15)  NOT NULL,
    [Census]           DATETIME      NULL,
    [SnapshotDate]     DATETIME      NULL
);


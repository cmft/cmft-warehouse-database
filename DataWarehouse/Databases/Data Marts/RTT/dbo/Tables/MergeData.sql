﻿CREATE TABLE [dbo].[MergeData] (
    [MergeDataRecno]     BIGINT IDENTITY (1, 1) NOT NULL,
    [SourceRecno]        INT    NOT NULL,
    [MatchRecno]         INT    NULL,
    [MergeTemplateRecno] INT    NOT NULL,
    CONSTRAINT [PK_MergeData] PRIMARY KEY CLUSTERED ([MergeDataRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MergeData]
    ON [dbo].[MergeData]([SourceRecno] ASC, [MergeTemplateRecno] ASC);


﻿CREATE TABLE [dbo].[MergeSnapshot] (
    [SnapshotTime]       SMALLDATETIME NULL,
    [MergeDataRecno]     BIGINT        IDENTITY (1, 1) NOT NULL,
    [SourceRecno]        INT           NOT NULL,
    [MatchRecno]         INT           NULL,
    [MergeTemplateRecno] INT           NOT NULL
);


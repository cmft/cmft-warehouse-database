﻿CREATE TABLE [dbo].[ClockStopReason] (
    [ClockStopReasonCode] VARCHAR (10) NOT NULL,
    [ClockStopReason]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ClockStopReason] PRIMARY KEY CLUSTERED ([ClockStopReasonCode] ASC)
);


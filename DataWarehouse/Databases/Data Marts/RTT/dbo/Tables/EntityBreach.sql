﻿CREATE TABLE [dbo].[EntityBreach] (
    [EntityTypeCode] VARCHAR (50)  NOT NULL,
    [EntityCode]     VARCHAR (50)  NOT NULL,
    [FromDate]       SMALLDATETIME NOT NULL,
    [ToDate]         SMALLDATETIME NULL,
    [BreachValue]    INT           NULL,
    CONSTRAINT [PK_EntityBreach] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC, [EntityCode] ASC, [FromDate] ASC),
    CONSTRAINT [FK_EntityBreach_EntityType] FOREIGN KEY ([EntityTypeCode]) REFERENCES [dbo].[EntityType] ([EntityTypeCode])
);


﻿CREATE TABLE [dbo].[InpatientWLSnapshot] (
    [SnapshotDate]                 DATETIME      NOT NULL,
    [EncounterRecno]               INT           NOT NULL,
    [SourcePatientNo]              VARCHAR (20)  NOT NULL,
    [SourceEncounterNo]            VARCHAR (50)  NOT NULL,
    [CensusDate]                   SMALLDATETIME NOT NULL,
    [PatientSurname]               VARCHAR (30)  NULL,
    [PatientForename]              VARCHAR (20)  NULL,
    [PatientTitle]                 VARCHAR (10)  NULL,
    [SexCode]                      VARCHAR (1)   NULL,
    [DateOfBirth]                  DATETIME      NULL,
    [DateOfDeath]                  SMALLDATETIME NULL,
    [NHSNumber]                    VARCHAR (17)  NULL,
    [DistrictNo]                   VARCHAR (14)  NULL,
    [MaritalStatusCode]            VARCHAR (1)   NULL,
    [ReligionCode]                 VARCHAR (4)   NULL,
    [Postcode]                     VARCHAR (10)  NULL,
    [PatientsAddress1]             VARCHAR (25)  NULL,
    [PatientsAddress2]             VARCHAR (25)  NULL,
    [PatientsAddress3]             VARCHAR (25)  NULL,
    [PatientsAddress4]             VARCHAR (25)  NULL,
    [DHACode]                      VARCHAR (3)   NULL,
    [HomePhone]                    VARCHAR (23)  NULL,
    [WorkPhone]                    VARCHAR (23)  NULL,
    [EthnicOriginCode]             VARCHAR (4)   NULL,
    [ConsultantCode]               VARCHAR (6)   NULL,
    [SpecialtyCode]                VARCHAR (4)   NULL,
    [PASSpecialtyCode]             VARCHAR (4)   NULL,
    [ManagementIntentionCode]      VARCHAR (1)   NULL,
    [AdmissionMethodCode]          VARCHAR (2)   NULL,
    [PriorityCode]                 VARCHAR (1)   NULL,
    [WaitingListCode]              VARCHAR (6)   NULL,
    [CommentClinical]              VARCHAR (60)  NULL,
    [CommentNonClinical]           VARCHAR (60)  NULL,
    [IntendedPrimaryOperationCode] VARCHAR (7)   NULL,
    [Operation]                    VARCHAR (60)  NULL,
    [SiteCode]                     VARCHAR (4)   NULL,
    [WardCode]                     VARCHAR (4)   NULL,
    [WLStatus]                     VARCHAR (20)  NULL,
    [ProviderCode]                 VARCHAR (4)   NULL,
    [PurchaserCode]                VARCHAR (5)   NULL,
    [ContractSerialNumber]         VARCHAR (6)   NULL,
    [CancelledBy]                  VARCHAR (3)   NULL,
    [BookingTypeCode]              VARCHAR (4)   NULL,
    [CasenoteNumber]               VARCHAR (14)  NULL,
    [OriginalDateOnWaitingList]    SMALLDATETIME NULL,
    [DateOnWaitingList]            SMALLDATETIME NULL,
    [TCIDate]                      SMALLDATETIME NULL,
    [KornerWait]                   INT           NULL,
    [CountOfDaysSuspended]         VARCHAR (4)   NULL,
    [SuspensionStartDate]          SMALLDATETIME NULL,
    [SuspensionEndDate]            SMALLDATETIME NULL,
    [SuspensionReasonCode]         VARCHAR (4)   NULL,
    [SuspensionReason]             VARCHAR (30)  NULL,
    [InterfaceCode]                VARCHAR (5)   NULL,
    [EpisodicGpCode]               VARCHAR (8)   NULL,
    [EpisodicGpPracticeCode]       VARCHAR (6)   NULL,
    [RegisteredGpCode]             VARCHAR (8)   NULL,
    [RegisteredPracticeCode]       VARCHAR (8)   NULL,
    [SourceTreatmentFunctionCode]  INT           NULL,
    [TreatmentFunctionCode]        INT           NULL,
    [NationalSpecialtyCode]        VARCHAR (10)  NULL,
    [PCTCode]                      VARCHAR (8)   NULL,
    [BreachDate]                   SMALLDATETIME NULL,
    [ExpectedAdmissionDate]        SMALLDATETIME NULL,
    [ReferralDate]                 SMALLDATETIME NULL,
    [FuturePatientCancelDate]      SMALLDATETIME NULL,
    [AdditionFlag]                 BIT           NULL,
    [LocalRegisteredGpCode]        VARCHAR (8)   NULL,
    [LocalEpisodicGpCode]          VARCHAR (8)   NULL,
    [NextOfKinName]                VARCHAR (30)  NULL,
    [NextOfKinRelationship]        VARCHAR (9)   NULL,
    [NextOfKinHomePhone]           VARCHAR (23)  NULL,
    [NextOfKinWorkPhone]           VARCHAR (23)  NULL,
    [ExpectedLOS]                  VARCHAR (10)  NULL,
    [TheatreKey]                   INT           NULL,
    [TheatreInterfaceCode]         CHAR (5)      NULL,
    [ProcedureDate]                SMALLDATETIME NULL,
    [FutureCancellationDate]       SMALLDATETIME NULL,
    [ProcedureTime]                SMALLDATETIME NULL,
    [TheatreCode]                  VARCHAR (10)  NULL,
    [AdmissionReason]              VARCHAR (255) NULL,
    [EpisodeNo]                    SMALLINT      NULL,
    [BreachDays]                   INT           NULL,
    [MRSA]                         VARCHAR (10)  NULL,
    [RTTPathwayID]                 VARCHAR (25)  NULL,
    [RTTPathwayCondition]          VARCHAR (20)  NULL,
    [RTTStartDate]                 SMALLDATETIME NULL,
    [RTTEndDate]                   SMALLDATETIME NULL,
    [RTTSpecialtyCode]             VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]       VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]         VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]         SMALLDATETIME NULL,
    [RTTCurrentPrivatePatientFlag] BIT           NULL,
    [RTTOverseasStatusFlag]        BIT           NULL,
    [NationalBreachDate]           SMALLDATETIME NULL,
    [NationalBreachDays]           INT           NULL,
    [DerivedBreachDays]            INT           NULL,
    [DerivedClockStartDate]        SMALLDATETIME NULL,
    [DerivedBreachDate]            SMALLDATETIME NULL,
    [RTTBreachDate]                SMALLDATETIME NULL,
    [RTTDiagnosticBreachDate]      SMALLDATETIME NULL,
    [NationalDiagnosticBreachDate] SMALLDATETIME NULL,
    [SocialSuspensionDays]         INT           NULL,
    [BreachTypeCode]               VARCHAR (10)  NULL,
    [AddedToWaitingListTime]       SMALLDATETIME NULL,
    [SourceUniqueID]               VARCHAR (255) NULL,
    [AdminCategoryCode]            VARCHAR (10)  NULL,
    [SourceEntityRecno]            VARCHAR (50)  NOT NULL,
    [DirectorateCode]              VARCHAR (5)   NULL,
    CONSTRAINT [PK_InpatientWLSnapshot] PRIMARY KEY CLUSTERED ([SnapshotDate] ASC, [EncounterRecno] ASC, [CensusDate] ASC)
);


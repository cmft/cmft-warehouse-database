﻿CREATE TABLE [dbo].[RR_WLRemTemp] (
    [ReferralRecNo]      INT           NOT NULL,
    [OutpatientRecNo]    INT           NULL,
    [AppointmentType]    NVARCHAR (3)  NULL,
    [DateonList]         NVARCHAR (10) NULL,
    [OPWLRemDateTimeExt] NVARCHAR (16) NULL
);


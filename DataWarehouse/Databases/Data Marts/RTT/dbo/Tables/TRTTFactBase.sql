﻿CREATE TABLE [dbo].[TRTTFactBase] (
    [CensusDate]                SMALLDATETIME NOT NULL,
    [EncounterRecno]            INT           NOT NULL,
    [ConsultantCode]            VARCHAR (50)  NULL,
    [SpecialtyCode]             VARCHAR (50)  NULL,
    [PracticeCode]              VARCHAR (50)  NULL,
    [PCTCode]                   VARCHAR (50)  NULL,
    [SiteCode]                  VARCHAR (50)  NULL,
    [PathwayStatusCode]         VARCHAR (50)  NULL,
    [ClockStartDate]            SMALLDATETIME NULL,
    [WorkflowStatusCode]        VARCHAR (50)  NULL,
    [KeyDate]                   SMALLDATETIME NULL,
    [Cases]                     INT           NULL,
    [TCIBeyondBreach]           INT           NULL,
    [LastWeeksCases]            INT           NULL,
    [PrimaryDiagnosisCode]      VARCHAR (50)  NULL,
    [PrimaryOperationCode]      VARCHAR (50)  NULL,
    [ReportableFlag]            VARCHAR (50)  NULL,
    [UnadjustedClockStartDate]  VARCHAR (50)  NULL,
    [UnadjustedTCIBeyondBreach] INT           NULL,
    [RTTBreachDate]             SMALLDATETIME NULL,
    [ReferringProviderCode]     VARCHAR (5)   NULL,
    [ClockStopDate]             SMALLDATETIME NULL,
    [RTTFactTypeCode]           INT           NULL
);


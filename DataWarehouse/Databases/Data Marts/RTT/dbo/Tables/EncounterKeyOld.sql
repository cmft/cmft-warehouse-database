﻿CREATE TABLE [dbo].[EncounterKeyOld] (
    [EncounterRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [EncounterTypeCode] CHAR (2)     NULL,
    [KeyTypeCode]       CHAR (2)     NULL,
    [SourceKey]         VARCHAR (50) NULL,
    CONSTRAINT [PK_EncounterKey] PRIMARY KEY NONCLUSTERED ([EncounterRecno] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [IX_EncounterKey] UNIQUE CLUSTERED ([KeyTypeCode] ASC, [EncounterTypeCode] ASC, [SourceKey] ASC)
);


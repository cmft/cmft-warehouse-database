﻿CREATE TABLE [dbo].[PathwayNonReportable_RRTest] (
    [ID]         INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo] NVARCHAR (9)   NOT NULL,
    [EpisodeNo]  NVARCHAR (9)   NOT NULL,
    [FromDate]   DATE           NOT NULL,
    [ToDate]     DATE           NULL,
    [Comments]   NVARCHAR (100) NULL
);


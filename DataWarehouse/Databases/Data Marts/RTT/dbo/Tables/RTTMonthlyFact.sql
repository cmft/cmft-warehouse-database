﻿CREATE TABLE [dbo].[RTTMonthlyFact] (
    [CensusDate]         SMALLDATETIME NOT NULL,
    [EncounterRecno]     INT           NOT NULL,
    [ConsultantCode]     VARCHAR (10)  NOT NULL,
    [SpecialtyCode]      VARCHAR (5)   NOT NULL,
    [PracticeCode]       VARCHAR (8)   NOT NULL,
    [PCTCode]            VARCHAR (50)  NOT NULL,
    [SiteCode]           VARCHAR (50)  NOT NULL,
    [WeekBandCode]       VARCHAR (5)   NOT NULL,
    [PathwayStatusCode]  VARCHAR (3)   NOT NULL,
    [ReferralDate]       SMALLDATETIME NOT NULL,
    [WorkflowStatusCode] CHAR (1)      NOT NULL,
    [BreachBandCode]     VARCHAR (50)  NULL,
    [DaysWaiting]        INT           NOT NULL,
    [Cases]              INT           NOT NULL,
    [TCIBeyondBreach]    INT           NULL,
    [LastWeeksCases]     SMALLINT      NULL,
    CONSTRAINT [PK_RTTMonthlyFact] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [EncounterRecno] ASC) WITH (FILLFACTOR = 90)
);


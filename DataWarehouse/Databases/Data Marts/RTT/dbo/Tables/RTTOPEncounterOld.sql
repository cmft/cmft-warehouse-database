﻿CREATE TABLE [dbo].[RTTOPEncounterOld] (
    [SourcePatientNo]   VARCHAR (50) NOT NULL,
    [SourceEntityRecno] VARCHAR (50) NOT NULL,
    [ReviewedFlag]      BIT          NULL,
    [Created]           DATETIME     NULL,
    [Updated]           DATETIME     NULL,
    [LastUser]          VARCHAR (50) NULL,
    [SharedBreachFlag]  BIT          NULL
);


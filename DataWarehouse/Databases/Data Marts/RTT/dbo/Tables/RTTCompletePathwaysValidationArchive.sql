﻿CREATE TABLE [dbo].[RTTCompletePathwaysValidationArchive] (
    [Census]        DATETIME      NULL,
    [RTTPathwayID]  VARCHAR (25)  NULL,
    [StartDate]     SMALLDATETIME NULL,
    [StopDate]      SMALLDATETIME NULL,
    [EventType]     VARCHAR (4)   NOT NULL,
    [OutcomeID]     INT           NOT NULL,
    [DateValidated] DATETIME      NULL,
    [Comments]      VARCHAR (250) NULL
);


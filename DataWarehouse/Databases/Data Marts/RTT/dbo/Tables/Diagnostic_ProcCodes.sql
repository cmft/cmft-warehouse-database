﻿CREATE TABLE [dbo].[Diagnostic_ProcCodes] (
    [Procedure Code]        NVARCHAR (255) NULL,
    [Procedure Description] NVARCHAR (255) NULL,
    [Comments]              NVARCHAR (255) NULL,
    [Scope]                 NVARCHAR (255) NULL,
    [Scope1]                NVARCHAR (255) NULL
);


﻿CREATE TABLE [dbo].[TMatchMaster] (
    [CensusDate]             SMALLDATETIME NOT NULL,
    [ReferralRecno]          INT           NOT NULL,
    [OutpatientRecno]        INT           NULL,
    [InpatientWLRecno]       INT           NULL,
    [InpatientRecno]         INT           NULL,
    [FutureOPRecno]          INT           NULL,
    [IPToFutureOPRecno]      INT           NULL,
    [AttIPWLTemplateRecno]   INT           NULL,
    [AttSpellTemplateRecno]  INT           NULL,
    [SpellOPWLTemplateRecno] INT           NULL,
    [RefIPWLTemplateRecno]   INT           NULL,
    CONSTRAINT [PK_TMatchMaster] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [ReferralRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TMatchMaster]
    ON [dbo].[TMatchMaster]([OutpatientRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TMatchMaster_1]
    ON [dbo].[TMatchMaster]([InpatientWLRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TMatchMaster_2]
    ON [dbo].[TMatchMaster]([InpatientRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TMatchMaster_3]
    ON [dbo].[TMatchMaster]([FutureOPRecno] ASC);


﻿CREATE TABLE [dbo].[GUM] (
    [EncounterRecno] INT           IDENTITY (1, 1) NOT NULL,
    [PCTCode]        VARCHAR (10)  NOT NULL,
    [SiteCode]       VARCHAR (10)  NOT NULL,
    [TreatmentDate]  SMALLDATETIME NOT NULL,
    [Patients]       INT           NOT NULL,
    CONSTRAINT [PK_GUM] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_GUM]
    ON [dbo].[GUM]([PCTCode] ASC, [SiteCode] ASC, [TreatmentDate] ASC);


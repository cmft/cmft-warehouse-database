﻿CREATE TABLE [dbo].[WkOutpatient] (
    [EncounterRecno]     INT           NOT NULL,
    [EncounterEndDate]   SMALLDATETIME NULL,
    [SourcePatientNo]    VARCHAR (20)  NULL,
    [HospitalCode]       VARCHAR (5)   NULL,
    [ConsultantCode]     CHAR (10)     NULL,
    [SpecialtyCode]      CHAR (5)      NULL,
    [AppointmentDate]    SMALLDATETIME NULL,
    [SourceEncounterNo2] INT           NULL,
    [RTTPathwayID]       VARCHAR (25)  NULL,
    CONSTRAINT [PK_WkOutpatient] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_WkOutpatient]
    ON [dbo].[WkOutpatient]([SourcePatientNo] ASC, [SourceEncounterNo2] ASC);


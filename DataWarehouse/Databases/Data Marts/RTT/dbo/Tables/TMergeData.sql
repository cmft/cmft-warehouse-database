﻿CREATE TABLE [dbo].[TMergeData] (
    [MergeDataRecno]     INT IDENTITY (1, 1) NOT NULL,
    [SourceRecno]        INT NOT NULL,
    [MatchRecno]         INT NULL,
    [MergeTemplateRecno] INT NOT NULL,
    [Priority]           INT NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_TMergeData]
    ON [dbo].[TMergeData]([SourceRecno] ASC);


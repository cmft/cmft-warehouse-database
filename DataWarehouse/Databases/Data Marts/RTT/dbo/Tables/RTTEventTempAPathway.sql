﻿CREATE TABLE [dbo].[RTTEventTempAPathway] (
    [SourcePatientNo] VARCHAR (20)  NULL,
    [SpecialtyCode]   VARCHAR (10)  NULL,
    [ConsultantCode]  VARCHAR (10)  NULL,
    [EventTime]       DATETIME      NULL,
    [EventType]       NVARCHAR (50) NULL,
    [AID]             VARCHAR (25)  NULL,
    [IPEvTm]          DATETIME      NULL,
    [IPEvTy]          NVARCHAR (50) NULL,
    [BID]             VARCHAR (25)  NULL,
    [EvRTTPathwayID]  VARCHAR (25)  NULL
);


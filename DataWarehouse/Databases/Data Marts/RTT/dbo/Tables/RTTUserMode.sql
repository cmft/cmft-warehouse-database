﻿CREATE TABLE [dbo].[RTTUserMode] (
    [RTTUserModeCode]             VARCHAR (10) NOT NULL,
    [RTTUserMode]                 VARCHAR (50) NULL,
    [RTTUserModeBackgroundColour] VARCHAR (50) NULL,
    CONSTRAINT [PK_RTTUserMode] PRIMARY KEY CLUSTERED ([RTTUserModeCode] ASC)
);


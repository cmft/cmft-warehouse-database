﻿CREATE TABLE [dbo].[RTTAdjustment] (
    [RTTAdjustmentRecno]   INT            IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]      VARCHAR (50)   NOT NULL,
    [SourceEntityRecno]    VARCHAR (50)   NOT NULL,
    [AdjustmentDays]       INT            NOT NULL,
    [AdjustmentReasonCode] VARCHAR (10)   NULL,
    [AdjustmentComment]    VARCHAR (4000) NULL,
    [Created]              DATETIME       NULL,
    [Updated]              DATETIME       NULL,
    [LastUser]             VARCHAR (50)   NULL,
    CONSTRAINT [PK_RTTAdjustment] PRIMARY KEY CLUSTERED ([RTTAdjustmentRecno] ASC),
    CONSTRAINT [FK_RTTAdjustment_AdjustmentReason] FOREIGN KEY ([AdjustmentReasonCode]) REFERENCES [dbo].[AdjustmentReason] ([AdjustmentReasonCode]),
    CONSTRAINT [FK_RTTAdjustment_RTTEncounter] FOREIGN KEY ([SourcePatientNo], [SourceEntityRecno]) REFERENCES [dbo].[RTTEncounter] ([SourcePatientNo], [SourceEntityRecno])
);


GO
CREATE NONCLUSTERED INDEX [IX_RTTAdjustment]
    ON [dbo].[RTTAdjustment]([SourcePatientNo] ASC, [SourceEntityRecno] ASC);


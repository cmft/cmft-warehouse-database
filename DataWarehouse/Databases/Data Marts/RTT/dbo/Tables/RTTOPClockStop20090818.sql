﻿CREATE TABLE [dbo].[RTTOPClockStop20090818] (
    [RTTClockStopRecno]      INT            IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]        VARCHAR (50)   NOT NULL,
    [SourceEntityRecno]      VARCHAR (50)   NOT NULL,
    [ClockStopDate]          SMALLDATETIME  NULL,
    [ClockStopReasonCode]    VARCHAR (10)   NULL,
    [ClockStopComment]       VARCHAR (4000) NULL,
    [Created]                DATETIME       NULL,
    [Updated]                DATETIME       NULL,
    [LastUser]               VARCHAR (50)   NULL,
    [ClockStartDate]         SMALLDATETIME  NULL,
    [ClockStartReasonCode]   VARCHAR (10)   NULL,
    [WaitReasonCode]         VARCHAR (10)   NULL,
    [WaitReasonDate]         SMALLDATETIME  NULL,
    [ClockStopReportingDate] SMALLDATETIME  NULL,
    [RTTUserModeCode]        VARCHAR (10)   NOT NULL
);


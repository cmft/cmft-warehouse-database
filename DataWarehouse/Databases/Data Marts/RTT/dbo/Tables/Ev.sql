﻿CREATE TABLE [dbo].[Ev] (
    [RTTPathwayID]        VARCHAR (25) NULL,
    [RTTPathwayCondition] VARCHAR (20) NULL,
    [SourcePatientNo]     VARCHAR (20) NULL,
    [SourceEncounterNo]   VARCHAR (50) NOT NULL
);


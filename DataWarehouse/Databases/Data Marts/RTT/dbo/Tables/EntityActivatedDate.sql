﻿CREATE TABLE [dbo].[EntityActivatedDate] (
    [EntityTypeCode] VARCHAR (50) NOT NULL,
    [EntityCode]     VARCHAR (50) NOT NULL,
    [ActiveDate]     DATE         NULL,
    CONSTRAINT [PK_EntityActivatedDate] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC, [EntityCode] ASC),
    CONSTRAINT [FK_EntityActivatedDate_EntityType] FOREIGN KEY ([EntityTypeCode]) REFERENCES [dbo].[EntityType] ([EntityTypeCode])
);


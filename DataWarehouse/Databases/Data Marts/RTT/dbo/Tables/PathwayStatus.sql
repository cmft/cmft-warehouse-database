﻿CREATE TABLE [dbo].[PathwayStatus] (
    [PathwayStatusCode]         VARCHAR (5)  NOT NULL,
    [PathwayStatus]             VARCHAR (50) NULL,
    [NationalPathwayStatusCode] VARCHAR (5)  NULL,
    CONSTRAINT [PK_PathwayStatus] PRIMARY KEY CLUSTERED ([PathwayStatusCode] ASC),
    CONSTRAINT [FK_PathwayStatus_NationalPathwayStatus] FOREIGN KEY ([NationalPathwayStatusCode]) REFERENCES [dbo].[NationalPathwayStatus] ([NationalPathwayStatusCode])
);


﻿CREATE TABLE [dbo].[OlapBreachBand] (
    [BreachBandCode] VARCHAR (10) NOT NULL,
    [BreachBand]     VARCHAR (50) NULL,
    CONSTRAINT [PK_OlapBreachBand] PRIMARY KEY CLUSTERED ([BreachBandCode] ASC)
);


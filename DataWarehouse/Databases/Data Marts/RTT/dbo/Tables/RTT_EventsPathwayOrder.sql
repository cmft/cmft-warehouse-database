﻿CREATE TABLE [dbo].[RTT_EventsPathwayOrder] (
    [RTTPathwayID] VARCHAR (25) NULL,
    [MinNum]       BIGINT       NULL,
    [Status]       VARCHAR (30) NULL,
    [Type]         CHAR (50)    NULL
);


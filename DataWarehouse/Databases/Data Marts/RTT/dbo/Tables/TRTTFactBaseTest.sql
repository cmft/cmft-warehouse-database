﻿CREATE TABLE [dbo].[TRTTFactBaseTest] (
    [CensusDate]                SMALLDATETIME NOT NULL,
    [EncounterRecno]            INT           NOT NULL,
    [ConsultantCode]            CHAR (10)     NULL,
    [SpecialtyCode]             CHAR (5)      NULL,
    [PracticeCode]              CHAR (8)      NULL,
    [PCTCode]                   VARCHAR (8)   NULL,
    [SiteCode]                  VARCHAR (5)   NULL,
    [PathwayStatusCode]         VARCHAR (3)   NOT NULL,
    [ClockStartDate]            SMALLDATETIME NULL,
    [WorkflowStatusCode]        VARCHAR (1)   NOT NULL,
    [KeyDate]                   SMALLDATETIME NULL,
    [Cases]                     INT           NOT NULL,
    [TCIBeyondBreach]           INT           NOT NULL,
    [LastWeeksCases]            INT           NOT NULL,
    [PrimaryDiagnosisCode]      CHAR (10)     NULL,
    [PrimaryOperationCode]      VARCHAR (10)  NULL,
    [ReportableFlag]            VARCHAR (1)   NOT NULL,
    [UnadjustedClockStartDate]  SMALLDATETIME NULL,
    [UnadjustedTCIBeyondBreach] INT           NOT NULL,
    [RTTBreachDate]             SMALLDATETIME NULL,
    [ReferringProviderCode]     VARCHAR (3)   NULL
);


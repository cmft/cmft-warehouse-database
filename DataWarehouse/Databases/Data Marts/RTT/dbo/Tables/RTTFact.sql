﻿CREATE TABLE [dbo].[RTTFact] (
    [CensusDate]            SMALLDATETIME NOT NULL,
    [EncounterRecno]        INT           NOT NULL,
    [EncounterTypeCode]     VARCHAR (10)  NOT NULL,
    [ConsultantCode]        VARCHAR (10)  NOT NULL,
    [SpecialtyCode]         VARCHAR (5)   NOT NULL,
    [PracticeCode]          VARCHAR (8)   NOT NULL,
    [PCTCode]               VARCHAR (50)  NOT NULL,
    [SiteCode]              VARCHAR (50)  NOT NULL,
    [WeekBandCode]          VARCHAR (5)   NOT NULL,
    [PathwayStatusCode]     VARCHAR (3)   NOT NULL,
    [ReferralDate]          SMALLDATETIME NOT NULL,
    [WorkflowStatusCode]    CHAR (1)      NOT NULL,
    [BreachBandCode]        VARCHAR (50)  NOT NULL,
    [DaysWaiting]           INT           NOT NULL,
    [Cases]                 INT           NOT NULL,
    [TCIBeyondBreach]       INT           NULL,
    [LastWeeksCases]        SMALLINT      NULL,
    [PrimaryDiagnosisCode]  VARCHAR (10)  NULL,
    [PrimaryOperationCode]  VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]  VARCHAR (10)  NULL,
    [RTTPeriodStatusCode]   VARCHAR (10)  NULL,
    [ReportableFlag]        VARCHAR (50)  NULL,
    [AdjustedFlag]          VARCHAR (10)  NOT NULL,
    [RTTBreachDate]         SMALLDATETIME NULL,
    [KeyDate]               SMALLDATETIME NULL,
    [ReferringProviderCode] VARCHAR (5)   NULL,
    CONSTRAINT [PK_RTTFact] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [EncounterRecno] ASC, [EncounterTypeCode] ASC, [AdjustedFlag] ASC)
);


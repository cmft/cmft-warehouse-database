﻿CREATE TABLE [dbo].[TSocialSuspension] (
    [SourcePatientNo]   VARCHAR (9) NOT NULL,
    [SourceEntityRecno] VARCHAR (9) NOT NULL,
    [DaysSuspended]     INT         NULL,
    CONSTRAINT [PK_TSocialSuspension] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [SourceEntityRecno] ASC)
);


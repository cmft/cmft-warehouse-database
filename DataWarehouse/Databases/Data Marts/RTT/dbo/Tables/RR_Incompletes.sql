﻿CREATE TABLE [dbo].[RR_Incompletes] (
    [Census]   DATETIME     NULL,
    [Category] VARCHAR (15) NOT NULL,
    [RTTWks]   INT          NULL,
    [Count]    INT          NULL
);


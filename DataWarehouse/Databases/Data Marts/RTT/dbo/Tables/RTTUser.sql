﻿CREATE TABLE [dbo].[RTTUser] (
    [RTTUsername]     VARCHAR (50) NOT NULL,
    [RTTUserModeCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_RTTUser] PRIMARY KEY CLUSTERED ([RTTUsername] ASC),
    CONSTRAINT [FK_RTTUser_RTTUserMode] FOREIGN KEY ([RTTUserModeCode]) REFERENCES [dbo].[RTTUserMode] ([RTTUserModeCode])
);


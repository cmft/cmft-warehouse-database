﻿CREATE TABLE [dbo].[RTTClockStart] (
    [RTTClockStartRecno]   INT            IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]      VARCHAR (50)   NOT NULL,
    [SourceEntityRecno]    VARCHAR (50)   NOT NULL,
    [ClockStartDate]       SMALLDATETIME  NULL,
    [ClockStartReasonCode] VARCHAR (10)   NULL,
    [ClockStartComment]    VARCHAR (4000) NULL,
    [Created]              DATETIME       NULL,
    [Updated]              DATETIME       NULL,
    [LastUser]             VARCHAR (50)   NULL,
    CONSTRAINT [PK_RTTClockStart] PRIMARY KEY CLUSTERED ([RTTClockStartRecno] ASC),
    CONSTRAINT [FK_RTTClockStart_ClockStartReason] FOREIGN KEY ([ClockStartReasonCode]) REFERENCES [dbo].[ClockStartReason] ([ClockStartReasonCode]),
    CONSTRAINT [FK_RTTClockStart_RTTEncounter] FOREIGN KEY ([SourcePatientNo], [SourceEntityRecno]) REFERENCES [dbo].[RTTEncounter] ([SourcePatientNo], [SourceEntityRecno])
);


GO
CREATE NONCLUSTERED INDEX [IX_RTTClockStart]
    ON [dbo].[RTTClockStart]([SourcePatientNo] ASC, [SourceEntityRecno] ASC);


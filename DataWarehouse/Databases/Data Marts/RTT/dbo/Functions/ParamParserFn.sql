﻿create    FUNCTION [dbo].[ParamParserFn]( @delimString varchar(4000) ) 
RETURNS @paramtable 
TABLE ( ItemCode varchar(255)) 
AS BEGIN

DECLARE @len int,
        @index int,
        @nextindex int

SET @len = DATALENGTH(@delimString)
SET @index = 0
SET @nextindex = 0


WHILE (@len >= @index )
BEGIN

SET @nextindex = CHARINDEX(',', @delimString, @index)

if (@nextindex = 0 ) SET @nextindex = @len + 2

 INSERT @paramtable
 SELECT rtrim(ltrim(SUBSTRING( @delimString, @index, @nextindex - @index )))


SET @index = @nextindex + 1

END
 RETURN
END


﻿CREATE procedure [dbo].[ReportRTTPTLAdmittedPredictor]
	 @SpecialtyCode varchar (50) = null
	,@CensusDate smalldatetime
	,@Backlog bit
as

--if @Backlog is true then show breached, backlog patients
--otherwise show non-breached, non-diagnostic patients

select
	 WL.*
	,case when WL.DerivedBreachDate < WL.TCIDate then 1 else 0 end RTTBookedBeyondBreach
	,case when datediff(week, WL.TCIDate, WL.DerivedBreachDate) < 2 then 1 else 0 end RTTNearBreach

	,RTTTCIBackgroundColour = ''

	,RTTDiagnosticBackgroundColour =
	case
	when WL.BreachTypeCode = '2' then
		case
	-- breach
		when WL.CensusDate > WL.DerivedDiagnosticBreachDate then 'red'
	-- booked beyond breach
		when WL.DerivedDiagnosticBreachDate < WL.TCIDate then 'pink'
	-- procedure date
	--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
	-- urgent
		when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.DerivedDiagnosticBreachDate) <= 14 then 'blue'
	-- no tci
		when WL.TCIDate is null then 'deepskyblue'
	--	DN CANCEL is black with white text
	-- default
		else ''
		end 
	else ''
	end 

	,RTTBackgroundColour =
	case
	when WL.BreachTypeCode != '2' then
		case
	-- breach
		when WL.CensusDate > WL.RTTBreachDate then 'red'
	-- booked beyond breach
		when WL.RTTBreachDate < WL.TCIDate then 'pink'
	-- procedure date
	--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
	-- urgent
		when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.RTTBreachDate) <= 28 then 'blue'
	-- no tci
		when WL.TCIDate is null then 'deepskyblue'
	--	DN CANCEL is black with white text
	-- default
		else ''
		end 
	else ''
	end 

	,NewRTTBreachDate = RTTBreachDate

	,NationalBreachDateVisibleFlag =
		case
		when RTTBreachDate < CensusDate
		then 1
		else 0
		end

from
	(
	select
		WL.EncounterRecno,
		WL.SpecialtyCode,
		WL.Specialty,
		WL.SiteCode,
		WL.CensusDate,
		WL.Site,
		WL.Consultant, 
		WL.DistrictNo,
		WL.PatientSurname,
		WL.Operation,
		WL.OperationCode,
		WL.ManagementIntentionCode,
		WL.PriorityCode,
		WL.OriginalDateOnWaitingList,
		WL.KornerWait,
		WL.WeeksWaiting,
		WL.TotalWeeksWaiting,
		WL.TCIDate,
		WL.Comment,
		WL.ChoiceBackgroundColour,
		WL.PCTCode,
		WL.SexCode,
		WL.DateOfBirth,
		WL.ExpectedLOS,
		WL.ProcedureDate,
		WL.OriginalTCIDate,
		WL.FutureCancellationDate,
		WL.PlannedAnaesthetic,
		WL.GpPracticeCode

		,WL.DerivedBreachDays
		,WL.RTTPathwayID
		,WL.RTTPathwayCondition
		,WL.RTTStartDate
		,WL.RTTEndDate
		,WL.RTTSpecialtyCode
		,WL.RTTCurrentProviderCode
		,WL.RTTCurrentStatusCode
		,WL.RTTCurrentStatusDate
		,WL.RTTCurrentPrivatePatientFlag
		,WL.RTTOverseasStatusFlag
		,WL.RTTStatus
		,WL.DerivedClockStartDate

		,datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays RTTKornerWait

		,(datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays) / 7 RTTWeeksWaiting

		,datediff(day, dateadd(day, WL.SocialSuspensionDays, WL.DerivedClockStartDate), WL.CensusDate) / 7 RTTTotalWeeksWaiting

		,WL.RTTBreachDate

		,WL.NationalBreachDate

		,WL.RTTDiagnosticBreachDate

		,NationalBreachBackgroundColour =
		case
		when datediff(day, WL.DerivedClockStartDate, WL.CensusDate) > 
			(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')
		then 'yellow'
		else 'white'
		end

		,WL.DerivedBreachDate

		,DerivedDiagnosticBreachDate =
		case
		when WL.BreachTypeCode = 2 then
			case
			when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate
			then WL.RTTDiagnosticBreachDate
			else WL.NationalDiagnosticBreachDate
			end
		else null
		end

		,WL.SocialSuspensionDays
		,WL.DateOnWaitingList

		,MaintenanceURL =
		(
		select
			TextValue
		from
			RTT.dbo.Parameter
		where
			Parameter = 'MAINTENANCEURL'
		)
		+ '?SourcePatientNo=' + WL.SourcePatientNo
		+ '&SourceEncounterNo=' + WL.SourceEncounterNo

		,BreachTypeCode

		,ClinicalSuspensionFlag

		,SuspensionEndDate

	from
		(
		select
			WL.EncounterRecno,

			WL.SpecialtyCode,

			Specialty.Specialty,

			Site.Abbreviation SiteCode,

			left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

			case 
			when WL.SiteCode is null then 'Unknown Site' 
			when Site.Site is null then WL.SiteCode + ' - No Description' 
			else Site.Site
			end Site,

			case 
			when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
			when upper(WL.Operation) like '%TGL' then '#'
			else '' 
			end +
				case 
					when WL.ConsultantCode is null then 'No Consultant'
					when WL.ConsultantCode = '' then 'No Consultant'
					else
						case 
							when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
							else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
						end
				end Consultant, 

			WL.DistrictNo,

			WL.PatientSurname,

			case
			when WL.IntendedPrimaryOperationCode is null then ''
			when WL.IntendedPrimaryOperationCode = '' then ''
			when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
			else Operation.Operation
			end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

			left(WL.IntendedPrimaryOperationCode, 3) OperationCode,

			WL.ManagementIntentionCode,

			WL.PriorityCode,

			WL.OriginalDateOnWaitingList,

			WL.KornerWait,

			convert(int, coalesce(WL.KornerWait, 0) / 7.0) WeeksWaiting,

			convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 7.0) TotalWeeksWaiting,

			case
			when WL.TCIDate = WL.FutureCancellationDate and WL.CancelledBy = 'P' then null
			when WL.FutureCancellationDate is not null and coalesce(WL.CancelledBy, 'X') != 'P' then null
			else coalesce(WL.TCIDate, WL.ProcedureDate)
			end TCIDate,

			WL.DerivedBreachDate,

			null Comment,

			'white' ChoiceBackgroundColour,

			PCTCode = left(WL.PCTCode, 3),

			WL.SexCode,
			WL.DateOfBirth,
			WL.ExpectedLOS,

			WL.ProcedureDate,
			WL.TCIDate OriginalTCIDate,

			WL.FuturePatientCancelDate FutureCancellationDate,

			TheatreCase.PlannedAnaesthetic,

			coalesce(WL.EpisodicGpPracticeCode, WL.RegisteredPracticeCode) GpPracticeCode,

			WL.DerivedBreachDays

			,WL.RTTPathwayID
			,WL.RTTPathwayCondition
			,WL.RTTStartDate
			,WL.RTTEndDate
			,WL.RTTSpecialtyCode
			,WL.RTTCurrentProviderCode
			,WL.RTTCurrentStatusCode
			,WL.RTTCurrentStatusDate
			,WL.RTTCurrentPrivatePatientFlag
			,WL.RTTOverseasStatusFlag

			,RTTStatus.RTTStatus
			,WL.DerivedClockStartDate
			,WL.RTTBreachDate
			,WL.RTTDiagnosticBreachDate
			,WL.NationalDiagnosticBreachDate
			,WL.NationalBreachDate
			,WL.SocialSuspensionDays
			,WL.DateOnWaitingList

			,WL.SourcePatientNo
			,WL.SourceEncounterNo

			,WL.BreachTypeCode

			,ClinicalSuspensionFlag =
			case when WL.WLStatus = 'WL Suspend' then 1 else 0 end

			,WL.SuspensionEndDate

		from
			APC.WL WITH (NOLOCK)

		left join PAS.Consultant PASConsultant 
		on	PASConsultant.ConsultantCode = WL.ConsultantCode

		left join WH.Consultant Consultant 
		on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

		left join PAS.Site PASSite
		on	PASSite.SiteCode = WL.SiteCode

		left join WH.Site Site
		on	Site.SiteCode = PASSite.MappedSiteCode

		left join WH.Operation Operation
		on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

		left join WH.SpecialtyPennine Specialty
		on	Specialty.SpecialtyCode = WL.SpecialtyCode

		left join Theatre.PatientEpisode TheatreCase
		on	TheatreCase.SourceUniqueID = WL.TheatreKey
		and	TheatreCase.InterfaceCode = WL.TheatreInterfaceCode

		left join WH.RTTStatus
		on	RTTStatus.RTTStatusCode = WL.RTTCurrentStatusCode

		left join 
		(
		select
			 SourcePatientNo
			,SourceEncounterNo
			,SuspensionStartDate
			,SuspensionEndDate
			,CensusDate
		from
			APC.WaitingListSuspension Suspension
		where
			upper(SuspensionReason) like '%S*%'
		and	coalesce(SuspensionEndDate, CensusDate) >= CensusDate
		) SocialSuspension
		on    SocialSuspension.SourcePatientNo = WL.SourcePatientNo
		and   SocialSuspension.SourceEncounterNo = WL.SourceEncounterNo
		and   SocialSuspension.CensusDate = WL.CensusDate


		where
			WL.AdmissionMethodCode in ('WL', 'BL', 'BA', 'EL')

--allow not suspended or clinical suspensions
		and
			(
				WL.WLStatus != 'WL Suspend'
			or	SocialSuspension.SourcePatientNo is null
			)

		and	WL.CensusDate = @CensusDate
		and	(
				WL.SpecialtyCode = @SpecialtyCode
			or	@SpecialtyCode = '[PAS Specialty].[Specialty].[All]'
			)

		) WL

) WL

left join RTT.dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = WL.OperationCode

where
	(
		@Backlog = 'false'
	and	WL.RTTBreachDate > WL.CensusDate
	and	WL.TCIDate is null
	and	DiagnosticProcedure.ProcedureCode is null
	)
or
	(
		@Backlog = 'true'
	and	WL.RTTBreachDate <= WL.CensusDate
	)


order by
	datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays desc,
	WL.Specialty,
	WL.SiteCode,
	WL.Consultant,
	WL.PatientSurname

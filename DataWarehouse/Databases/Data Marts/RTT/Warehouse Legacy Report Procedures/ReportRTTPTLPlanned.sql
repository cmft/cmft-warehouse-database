﻿CREATE      procedure [dbo].[ReportRTTPTLPlanned] 
	 @SpecialtyCode varchar (10) = null
	,@CensusDate smalldatetime = null
	,@InterfaceCode varchar (5) = null
	,@ConsultantCode varchar (20) = null
	,@PCTCode varchar (5) = null
	,@PriorityCode varchar(2) = null
	,@ManagementIntentionCode varchar(2) = null
as


-- formerly known as: ReportPTLPlannedIP

set @CensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))

select
	WL.EncounterRecno,

	WL.SpecialtyCode,

	Specialty.Specialty,

	Site.Abbreviation SiteCode,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when WL.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then WL.SiteCode + ' - No Description' 
	else Site.Site
	end Site,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
		case 
			when WL.ConsultantCode is null then 'No Consultant'
			when WL.ConsultantCode = '' then 'No Consultant'
			else
				case 
					when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
					else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
				end
		end Consultant, 

	WL.DistrictNo,

	WL.PatientSurname,

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
	else Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	left(WL.IntendedPrimaryOperationCode, 3) OperationCode,

	WL.ManagementIntentionCode,

	WL.PriorityCode,

	WL.OriginalDateOnWaitingList DateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 7.0) WeeksWaiting,

	convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 7.0) TotalWeeksWaiting,

	case
	when WL.TCIDate is not null then TCIDate
	else WL.ProcedureDate
	end TCIDate,

	WL.BreachDate,
	null EligibleStatusCode,

	case when WL.BreachDate < WL.TCIDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(week, WL.TCIDate, WL.BreachDate) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	WL.ExpectedAdmissionDate,

	WL.WLStatus,

	WL.AdmissionMethodCode,

	case
-- procedure date
--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
-- no tci
	when WL.TCIDate is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	null ChoiceBackgroundColour,

	WL.PCTCode,

	WL.SexCode,
	WL.DateOfBirth,
	WL.ExpectedLOS,

	WL.ProcedureDate,
	WL.TCIDate OriginalTCIDate

from
	APC.WL

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

where
	WL.CensusDate = @CensusDate
and	WL.AdmissionMethodCode in ('BP', 'PA', 'PL')
and	WL.WLStatus != 'WL Suspend'
and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		Site.SiteCode = @InterfaceCode
	or	@InterfaceCode is null
	)

and	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		left(WL.PCTCode, 3) = @PCTCode
	or	@PCTCode is null
	)

and	(
		WL.PriorityCode = @PriorityCode
	or	@PriorityCode is null
	)

and	(
		WL.ManagementIntentionCode = @ManagementIntentionCode
	or	@ManagementIntentionCode is null
	)

order by
	WL.KornerWait desc,
	WL.SiteCode,

	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end,

	WL.PatientSurname

﻿CREATE procedure [dbo].[ReportRTTPredictor] as

--need to:
--restrict to RTT activity only i.e. filter specialties etc

select
	AdmissionTypeCode = 'ADM'

	,BreachedCode =
		case
		when APCEncounter.RTTBreachDate is null
		then 'U'
		when APCEncounter.RTTBreachDate < APCEncounter.AdmissionDate
		then 'Y'
		else 'N'
		end

	,*
from
	APC.Encounter APCEncounter
where
	APCEncounter.AdmissionTime between '1 jan 2010' and '7 jan 2010'
and	APCEncounter.AdmissionTime = APCEncounter.EpisodeStartTime

and	APCEncounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
and	APCEncounter.SpecialtyCode not in 
	(
	select
		SpecialtyCode
	from
		RTT.dbo.ExcludedSpecialty
	)

-- 'GENE' ?
and	APCEncounter.ConsultantCode <> 'FRAC'

--remove CEA admissions
and	APCEncounter.CancelledElectiveAdmissionFlag <> 1

--remove ICAT
and	APCEncounter.SiteCode != 'FMSK'




select
	AdmissionTypeCode = 'TCI'

	,Breached =
		convert(
			bit
			,case
			when RTTBreachDate < TCIDate
			then 1
			else 0
			end
		)

	,*
from
	APC.WaitingList
where
	CensusDate = '9 feb 2010'
and	TCIDate between '9 feb 2010' and '28 feb 2010'

﻿CREATE procedure [dbo].[ReportRTTPTLAdmittedActive]
	 @SpecialtyCode varchar (10)
	,@CensusDate smalldatetime
	,@InterfaceCode varchar (5)
	,@ConsultantCode varchar (20)
	,@PCTCode varchar (5)
	,@PriorityCode varchar(2)
	,@ManagementIntentionCode varchar(2)
as


--formerly ReportPTLActiveIP

set @CensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))

select
	WL.EncounterRecno,

	WL.SpecialtyCode,

	Specialty.Specialty,

	Site.Abbreviation SiteCode,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when WL.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then WL.SiteCode + ' - No Description' 
	else Site.Site
	end Site,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end Consultant, 

	WL.DistrictNo,

	WL.PatientSurname,

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
	else Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	left(WL.IntendedPrimaryOperationCode, 3) OperationCode,

	WL.ManagementIntentionCode,

	WL.PriorityCode,

	WL.DateOnWaitingList,

	WL.OriginalDateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 7.0) WeeksWaiting,

	convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 7.0) TotalWeeksWaiting,

	case
	when WL.TCIDate = WL.FutureCancellationDate and WL.CancelledBy = 'P' then null
	when WL.FutureCancellationDate is not null and coalesce(WL.CancelledBy, 'X') != 'P' then null
	else coalesce(WL.TCIDate, WL.ProcedureDate)
	end TCIDate,

	WL.BreachDate,
	EligibleStatusCode = null,

	case when WL.BreachDate < WL.TCIDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(week, WL.TCIDate, WL.BreachDate) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	case
-- breach
	when WL.CensusDate > WL.BreachDate then 'red'
-- booked beyond breach
	when WL.BreachDate < WL.TCIDate then 'pink'
-- procedure date
--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
-- urgent
	when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.BreachDate) <= 28 then 'blue'
-- no tci
	when WL.TCIDate is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	ChoiceBackgroundColour = null,

	WL.PCTCode,

	WL.SexCode,
	WL.DateOfBirth,
	WL.ExpectedLOS,

	WL.ProcedureDate,
	WL.TCIDate OriginalTCIDate,

	FuturePatientCancelDate FutureCancellationDate,

	TheatreCase.PlannedAnaesthetic,

	coalesce(WL.EpisodicGpPracticeCode, RegisteredPracticeCode) GpPracticeCode,

	WL.BreachDays
from
	APC.WL WITH (NOLOCK)

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join Theatre.PatientEpisode TheatreCase
on	TheatreCase.SourceUniqueID = WL.TheatreKey
and	TheatreCase.InterfaceCode = WL.TheatreInterfaceCode

where
	WL.CensusDate = @CensusDate
and	WL.AdmissionMethodCode in ('WL', 'BL', 'BA', 'EL')
and	WL.WLStatus != 'WL Suspend'
and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		Site.SiteCode = @InterfaceCode
	or	@InterfaceCode is null
	)

and	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		left(WL.PCTCode, 3) = @PCTCode
	or	@PCTCode is null
	)

and	(
		WL.PriorityCode = @PriorityCode
	or	@PriorityCode is null
	)

and	(
		WL.ManagementIntentionCode = @ManagementIntentionCode
	or	@ManagementIntentionCode is null
	)

order by
	WL.KornerWait desc,
	Specialty.Specialty,
	WL.SiteCode,

	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end,

	WL.PatientSurname

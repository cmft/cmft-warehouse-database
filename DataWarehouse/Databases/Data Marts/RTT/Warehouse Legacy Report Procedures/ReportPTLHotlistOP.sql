﻿CREATE procedure [dbo].[ReportPTLHotlistOP] 
	 @SpecialtyCode varchar (10) = null
	,@CensusDate smalldatetime = null
	,@InterfaceCode varchar (5) = null
	,@ConsultantCode varchar (20) = null
	,@PCTCode varchar (5) = null
--	,@SourceOfReferralTypeCode varchar(10) = null
	,@QuarterStartDate as varchar(50) = null 
as

select 
	WL.BreachWeekNo,

	case WL.BreachWeekNo
	when 0 then 'Red'
	when 1 then 'Orange'
	when 2 then 'Orange'
	when 3 then 'Orange'
	else 'White'
	end RowColour,

	WL.EncounterRecno,

	WL.SpecialtyCode SpecialtyCode,

	Specialty.Specialty,

	WL.ConsultantCode,

	Site.Abbreviation SiteCode,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when WL.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then WL.SiteCode + ' - No Description' 
	else Site.Site
	end Site,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end Consultant, 

	WL.DistrictNo,

	WL.PatientSurname,

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
	else left(WL.IntendedPrimaryOperationCode, 3) + ' - ' + Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	WL.ManagementIntentionCode,

	WL.PriorityCode,

	WL.DateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 7) WeeksWaiting,

-- add the number of days 'til the end of the month
	convert(int, (
		coalesce(WL.KornerWait, 0) + 
		datediff(day, WL.CensusDate, 
			'01 ' + datename(month, dateadd(month, 1, WL.CensusDate)) + ' ' + datename(year, dateadd(month, 1, WL.CensusDate))
		) -1) / 7) WeeksWaitingAtMonthEnd,

	WL.DerivedAppointmentDate AppointmentDate,

	WL.BreachDate,
	EligibleStatusCode = null,

	case when WL.BreachDate < WL.AppointmentDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(month, WL.AppointmentDate, WL.BreachDate) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	WL.AdmissionMethodCode,
	WL.WLStatus,

	case
-- breach
	when WL.CensusDate > WL.BreachDate then 'red'
-- booked beyond breach
	when WL.BreachDate < 
	case
	when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
	when	WL.CancelledBy is not null then null
	when	WL.AppointmentStatusCode in ('DNA', 'CND') then null
	else	WL.AppointmentDate
	end then 'pink'
-- urgent
	when 
	case
	when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
	when	WL.CancelledBy is not null then null
	when	WL.AppointmentStatusCode in ('DNA', 'CND') then null
	else	WL.AppointmentDate
	end is null and datediff(day, WL.CensusDate, WL.BreachDate) <= 28 then 'blue'
-- no tci
	when 
	case
	when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
	when	WL.CancelledBy is not null then null
	when	WL.AppointmentStatusCode in ('DNA', 'CND') then null
	else	WL.AppointmentDate
	end is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	ChoiceBackgroundColour = null,

	WL.PCTCode,

	WL.ReferralDate,

	WL.QM08StartWaitDate,

	WL.CancelledBy,

	WL.FuturePatientCancelDate,

	coalesce(SourceOfReferral.SourceOfReferral, WL.SourceOfReferralCode + ' - No Description') SourceOfReferral,

	WL.DateOfBirth,

	SourceOfReferralTypeCode,

	case WL.SourceOfReferralTypeCode
	when 'GP' then 'GP'
	else 'Other'
	end SourceOfReferralType

	,WL.RTTPathwayID
	,WL.RTTPathwayCondition
	,WL.RTTStartDate
	,WL.RTTEndDate
	,WL.RTTSpecialtyCode
	,WL.RTTCurrentProviderCode
	,WL.RTTCurrentStatusCode
	,WL.RTTCurrentStatusDate
	,WL.RTTCurrentPrivatePatientFlag
	,WL.RTTOverseasStatusFlag

	,RTTStatus.RTTStatus

from
	(
	select
		BreachPeriodDate =
			Calendar.LastDayOfWeek

		,BreachWeekNo =
		case
		when convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)) < 1 then 0 
		else convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate))
		end

		,case when WL.CancelledBy = 'P' then WL.FutureCancellationDate else null end FuturePatientCancellationDate,

		case
		when	WL.FuturePatientCancelDate = WL.AppointmentDate then null
		when	WL.CancelledBy is not null then null
		when	WL.AppointmentStatusCode in ('DNA', 'CND') then null
		else	WL.AppointmentDate
		end DerivedAppointmentDate,

		case when WL.SourceOfReferralCode in ('GDP', 'PGP') then 'GP' else 'OTHER' end SourceOfReferralTypeCode,

		WL.*
		
	from
		OP.PTL WL

	inner join WH.Calendar Calendar
	on	Calendar.TheDate = WL.BreachDate

	where
		WL.CensusDate = @CensusDate

	-- remove non-trust specialties
	and	not exists
		(
		select
			1
		from
			dbo.EntityLookup NonTrustSpecialty
		where
			NonTrustSpecialty.EntityCode = WL.SpecialtyCode
		and	NonTrustSpecialty.EntityTypeCode = 'NONTRUSTSPECIALTY'
		)

	) WL

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join PAS.SourceOfReferral SourceOfReferral
on	SourceOfReferral.SourceOfReferralCode = WL.SourceOfReferralCode

left join WH.RTTStatus RTTStatus
on	RTTStatus.RTTStatusCode = WL.RTTCurrentStatusCode

where
	(
		WL.FuturePatientCancelDate is null
--	or	WL.FuturePatientCancelDate > WL.BreachDate
	or	WL.FuturePatientCancelDate > WL.BreachPeriodDate
	)

and
	(
		WL.DerivedAppointmentDate is null
	or	(
			(
				WL.DerivedAppointmentDate > WL.BreachPeriodDate
			or	WL.DerivedAppointmentDate < dateadd(day, -4, WL.CensusDate)
			or
				(
					WL.DerivedAppointmentDate < dateadd(day, -4, WL.BreachPeriodDate)
				and	datepart(week, WL.DerivedAppointmentDate) = datepart(week, WL.BreachPeriodDate)
				and	datepart(year, WL.DerivedAppointmentDate) = datepart(year, WL.BreachPeriodDate)
				)
			)
		and	datepart(year, WL.DerivedAppointmentDate) * 100 + datepart(week, WL.DerivedAppointmentDate) != datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)
		)
	)

and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		Site.SiteCode = @InterfaceCode
	or	@InterfaceCode is null
	)

and	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		left(WL.PCTCode, 3) = @PCTCode
	or	@PCTCode is null
	)


order by
	Specialty.Specialty,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end,

	WL.BreachDate,
	WL.PatientSurname

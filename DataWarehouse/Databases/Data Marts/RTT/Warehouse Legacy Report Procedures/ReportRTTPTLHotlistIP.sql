﻿CREATE	procedure [dbo].[ReportRTTPTLHotlistIP] 
	 @SpecialtyCode varchar (10) = null
	,@CensusDate smalldatetime = null
	,@InterfaceCode varchar (5) = null
	,@ConsultantCode varchar (20) = null
	,@ManagementIntentionCode varchar(2) = null
	,@QuarterStartDate as varchar(50) = null
as

--formally ReportRTTPTLHotlistIP

--declare @cutOffDate smalldatetime

select
	 @CensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))
--	,@cutOffDate =
--	(
--	select 
--		max(TheDate)
--	from
--		WH.Calendar Calendar
--	where
--		FinancialQuarter = 'Qtr ' + convert(char, datename(quarter, @QuarterStartDate)) 
--	and	CalendarYear = datepart(year, @QuarterStartDate)
--	)

select
	BreachPeriodDate =
		Calendar.LastDayOfWeek
--		(
--		select
--			max(BreachWeek.TheDate)
--		from
--			dbo.Calendar BreachWeek
--		where
--			BreachWeek.WeekNoKey = convert(int, Calendar.WeekNoKey)
--		and	BreachWeek.TheYear = Calendar.TheYear
--		and	BreachWeek.TheMonth = Calendar.TheMonth
--		)

	,BreachWeekNo =
	case
	when convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)) < 1 then 0 
	else convert(int, Calendar.WeekNoKey) - (datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate))
	end

	,FuturePatientCancellationDate =
	case
	when WL.CancelledBy = 'P' 
	then WL.FutureCancellationDate
	else null
	end

	,BreachDate28Day = TheatreSameDayCancellation.BreachDate

	,WL.*

-- 19 Mar 2009 - always show the national breach date
	,NationalBreachDateVisibleFlag = 1

--	,NationalBreachDateVisibleFlag =
--		case
--		when WL.RTTBreachDate < WL.CensusDate
--		then 1
--		else 0
--		end

	,DerivedDiagnosticBreachDate =
	case
	when WL.BreachTypeCode = 2 then
		case
		when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate
		then WL.RTTDiagnosticBreachDate
		else WL.NationalDiagnosticBreachDate
		end
	else null
	end

into
	#WL
from
	APC.WL WITH (NOLOCK)

left join Theatre.SameDayCancellation TheatreSameDayCancellation
on	TheatreSameDayCancellation.EpisodeID = WL.TheatreKey
and	TheatreSameDayCancellation.InterfaceCode = WL.TheatreInterfaceCode

inner join WH.Calendar Calendar
on	Calendar.TheDate = 
	case
	when TheatreSameDayCancellation.BreachDate is null then WL.DerivedBreachDate
	when TheatreSameDayCancellation.BreachDate < WL.DerivedBreachDate
	then TheatreSameDayCancellation.BreachDate
	else WL.DerivedBreachDate
	end

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

where
	WL.CensusDate = @CensusDate
and	WL.AdmissionMethodCode in ('WL', 'BL', 'BA', 'EL')

and	(
		(
			@QuarterStartDate is null
		and	WL.DerivedBreachDate <= dateadd(week, 8, WL.CensusDate)
		)
--	or
--		(
--			@QuarterStartDate is not null
--		and	WL.DerivedBreachDate between @QuarterStartDate and @cutOffDate
--		)
	)

and	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		Site.SiteCode = @InterfaceCode
	or	@InterfaceCode is null
	)

and	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and	(
		WL.ManagementIntentionCode = @ManagementIntentionCode
	or	@ManagementIntentionCode is null
	)




select
	WL.BreachWeekNo,

	case WL.BreachWeekNo
	when 0 then 'Red'
	when 1 then 'Orange'
	when 2 then 'Orange'
	when 3 then 'Orange'
	else 'White'
	end RowColour,

	WL.EncounterRecno,

	WL.SpecialtyCode,

	Specialty.Specialty,

	Site.Abbreviation SiteCode,

	left(convert(varchar, WL.CensusDate, 113), 11) CensusDate,

	case 
	when WL.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then WL.SiteCode + ' - No Description' 
	else Site.Site
	end Site,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end Consultant, 

	WL.DistrictNo,

	WL.PatientSurname,

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
	else Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	left(WL.IntendedPrimaryOperationCode, 3) OperationCode,

	WL.DateOnWaitingList,

	WL.OriginalDateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 7.0) WeeksWaiting,

	convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 7.0) TotalWeeksWaiting,

	case
	when WL.TCIDate = WL.FutureCancellationDate and WL.CancelledBy = 'P' then null
	when WL.FutureCancellationDate is not null and coalesce(WL.CancelledBy, 'X') != 'P' then null
	else coalesce(WL.TCIDate, WL.ProcedureDate)
	end TCIDate,

	WL.DerivedBreachDate,

--	DerivedBreachDate = 
--		dateadd(day
--			,coalesce(SocialSuspension.DaysSuspended, 0)
--			,WL.RTTBreachDate
--		),


--	case when WL.DerivedBreachDate < WL.TCIDate then 1 else 0 end BookedBeyondBreach,
--
--	case when datediff(week, WL.TCIDate, WL.DerivedBreachDate) < 2 then 1 else 0 end NearBreach,

	case when WL.DerivedBreachDate < WL.TCIDate then 1 else 0 end BookedBeyondBreach,

	case when datediff(week, WL.TCIDate, WL.DerivedBreachDate) < 2 then 1 else 0 end NearBreach,

	'' Comment,

	case
-- breach
	when WL.CensusDate > WL.DerivedBreachDate then 'red'
-- booked beyond breach
	when WL.DerivedBreachDate < WL.TCIDate then 'pink'
-- urgent
	when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.DerivedBreachDate) <= 28 then 'blue'
-- no tci
	when WL.TCIDate is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	null ChoiceBackgroundColour,

	WL.PCTCode,

	WL.SexCode,
	WL.DateOfBirth,
	WL.ExpectedLOS,

	WL.ProcedureDate,
	WL.TCIDate OriginalTCIDate,

	WL.FuturePatientCancellationDate FutureCancellationDate,

	WL.SuspensionEndDate,

	WL.BreachDate28Day

	,WL.RTTPathwayID
	,WL.DerivedClockStartDate
	,WL.RTTCurrentStatusCode
	,WL.RTTCurrentStatusDate

	,RTTKornerWait = datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays 
	,RTTWeeksWaiting = (datediff(day, WL.DerivedClockStartDate, WL.CensusDate) - WL.SocialSuspensionDays) / 7 
	,RTTTotalWeeksWaiting = datediff(day, dateadd(day, WL.SocialSuspensionDays, WL.DerivedClockStartDate), WL.CensusDate) / 7 

	,WL.NationalBreachDate
	,WL.NationalBreachDateVisibleFlag
	,WL.RTTBreachDate
	,WL.DerivedDiagnosticBreachDate

from
	#WL WL

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

where
	(
		WL.FuturePatientCancellationDate is null
	or	WL.FuturePatientCancellationDate > WL.BreachPeriodDate
	)
and
	(
		WL.TCIDate is null
	or	(
			(
				WL.TCIDate > WL.BreachPeriodDate
			or	WL.TCIDate < dateadd(day, -4, WL.CensusDate)
			or
				(
					WL.TCIDate < dateadd(day, -4, WL.BreachPeriodDate)
				and	datepart(week, WL.TCIDate) = datepart(week, WL.BreachPeriodDate)
				and	datepart(year, WL.TCIDate) = datepart(year, WL.BreachPeriodDate)
				)
			)
		and	datepart(year, WL.TCIDate) * 100 + datepart(week, WL.TCIDate) != datepart(year, WL.CensusDate) * 100 + datepart(week, WL.CensusDate)
		)
	)

and
	(
		WL.ProcedureDate is null
	or	WL.ProcedureDate > WL.BreachPeriodDate
	or	(
			WL.ProcedureDate < dateadd(day, 4, WL.CensusDate) 
		and WL.TCIDate is null
		)
	)
and
	(
		WL.SuspensionEndDate is null
	or	WL.SuspensionEndDate <= WL.BreachPeriodDate
	)


order by
	Specialty.Specialty,

	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end,

	WL.DerivedBreachDate,
	WL.PatientSurname

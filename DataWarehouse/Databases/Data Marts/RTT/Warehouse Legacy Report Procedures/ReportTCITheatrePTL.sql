﻿CREATE       procedure [dbo].[ReportTCITheatrePTL] 

	 @InterfaceCode varchar(5)
	,@FromDate smalldatetime
	,@ToDate smalldatetime = null
	,@ManagementIntentionCode char(1) = null

as

declare @CensusDate smalldatetime

set @CensusDate = (select coalesce(@CensusDate, max(CensusDate)) from APC.Snapshot)

set @ToDate = coalesce(@ToDate, dateadd(day, 6, @FromDate))

select
	Episode.ProcedureTime,
	FirstOperation.EpisodeID,

	WL.EncounterRecno,
	WL.InterfaceCode,
	Site.Abbreviation SiteCode,
	WL.DistrictNo,
	WL.SexCode,
	WL.PatientSurname,
	WL.DateOfBirth,

	WL.TCIDate,

	datename(dw, coalesce(WL.TCIDate, WL.ProcedureDate)) DayOfWeek,
	Episode.SessionDate,
	Specialty.Specialty,

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
		case 
			when WL.ConsultantCode is null then 'No Consultant'
			when WL.ConsultantCode = '' then 'No Consultant'
			else
				case 
					when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
					else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
				end
		end Consultant, 

	case
	when WL.IntendedPrimaryOperationCode is null then ''
	when WL.IntendedPrimaryOperationCode = '' then ''
	when Operation.Operation is null then left(WL.IntendedPrimaryOperationCode, 3) + ' - No Description'
	else Operation.Operation
	end + ' (' + coalesce(WL.Operation, '') + ')' Operation,

	WL.ManagementIntentionCode,

	WL.DateOnWaitingList,

	WL.OriginalDateOnWaitingList,

	WL.KornerWait,

	convert(int, coalesce(WL.KornerWait, 0) / 30.4) MonthsWaiting,

	convert(int, datediff(day, WL.OriginalDateOnWaitingList, WL.CensusDate) / 30.4) TotalMonthsWaiting,


	case
	when WL.AdmissionMethodCode in ('WL', 'BL', 'BA', 'EL') then 'A'
	else 'P'
	end ActivePlannedFlag,

	WL.BreachDate,

	(
	select
		max(SameDayCancellation.BreachDate) BreachDate
	from
		Theatre.SameDayCancellation SameDayCancellation
	where
		SameDayCancellation.SourcePatientNo = WL.DistrictNo
	and	SameDayCancellation.InterfaceCode = WL.TheatreInterfaceCode
	and	SameDayCancellation.IntendedManagementCode = 'IP'
	and	WL.AdmissionMethodCode in ('WL', 'BL', 'BA', 'EL')
	) SameDayCancellationBreachDate,

	Session.TheatreSessionPeriodCode,

	case
	when Episode.ProcedureTime is null then 'N/R'
	when datediff(minute, Episode.SessionDate, Episode.ProcedureTime) < 720  then 'AM'
	else 'PM'
	end TheatreSessionPeriod,

	WL.WardCode,

	Episode.SourceUniqueID EpisodeEpisodeID,
	WL.PriorityCode,

	case
-- breach
	when WL.CensusDate > WL.BreachDate then 'red'
-- booked beyond breach
	when WL.BreachDate < WL.TCIDate then 'pink'
-- procedure date
--	when WL.TCIDate is null and WL.ProcedureDate is not null then 'yellow'
-- urgent
	when WL.TCIDate is null and datediff(day, WL.CensusDate, WL.BreachDate) <= 28 then 'blue'
-- no tci
	when WL.TCIDate is null then 'deepskyblue'
--	DN CANCEL is black with white text
-- default
	else ''
	end TCIBackgroundColour,

	WL.ExpectedLOS,
	WL.AdmissionReason
	,case when WL.MRSA = 'YES' then 'Y' else '' end MRSA
from
	APC.WaitingList WL WITH (NOLOCK)

left join WHOLAP.dbo.FactTheatreVisit Episode
on	Episode.SourceUniqueID = WL.TheatreKey
and	Episode.InterfaceCode = WL.TheatreInterfaceCode

left join WHOLAP.dbo.FactTheatreSession Session
on	Session.SourceUniqueID = Episode.SessionID
and	Session.InterfaceCode = Episode.InterfaceCode

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.Operation Operation
on	Operation.OperationCode = left(WL.IntendedPrimaryOperationCode, 3)

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join
	(
	select
		FirstOperation.InterfaceCode,
		FirstOperation.SessionID,
		min(FirstOperation.SourceUniqueID) EpisodeID
	from
		WHOLAP.dbo.FactTheatreVisit FirstOperation
	group by
		FirstOperation.InterfaceCode,
		FirstOperation.SessionID
	) FirstOperation
on	FirstOperation.SessionID = Episode.SessionID
and	FirstOperation.EpisodeID = Episode.SourceUniqueID
and	FirstOperation.InterfaceCode = WL.TheatreInterfaceCode

where
	WL.CensusDate = @CensusDate
and	coalesce(WL.TCIDate, WL.ProcedureDate) between @FromDate and @ToDate
--and	coalesce(WL.TCIDate, WL.ProcedureDate) between @FromDate and dateadd(day, 6, @FromDate)
and	(
		PASSite.MappedSiteCode = @InterfaceCode
	or	@InterfaceCode = '##'
	)
and	WL.AdmissionMethodCode in ('WL', 'BL', 'BA', 'EL', 'BP', 'PA', 'PL')
and	WL.WLStatus != 'WL Suspend'

and	(
		WL.ManagementIntentionCode = @ManagementIntentionCode
	or	@ManagementIntentionCode is null
	)

and	(
	WL.FutureCancellationDate != WL.TCIDate
or	WL.FutureCancellationDate is null
	)

--and	WL.CancelledBy is null

order by
	coalesce(WL.TCIDate, WL.ProcedureDate),
	coalesce(Episode.ProcedureTime, WL.BreachDate, dateadd(minute, 1439, WL.TCIDate), dateadd(minute, 1439, WL.ProcedureDate)),

	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
		case 
			when WL.ConsultantCode is null then 'No Consultant'
			when WL.ConsultantCode = '' then 'No Consultant'
			else
				case 
					when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
					else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
				end
		end, 

--the EpisodeID determines the order of cases on TIMS booking screen
	FirstOperation.EpisodeID desc,
	coalesce(Session.TheatreSessionPeriodCode, 'ZZ'),
	WL.PriorityCode,
	WL.BreachDate,
	Session.SessionDate

﻿
CREATE  procedure RTT.[BuildModel]
as

/*
***************************************************
**
**
** 20140619		RR	added in new Gynae section
***************************************************
*/


-- This builds the reporting model for a given date range.
-- A snapshot is generated for the given @toDate.
-- (RR query, Snapshots and MatchMaster contain data for CensusDates for last 8 weeks and last censusdate per month.  Should it just keep last 4 weeks and possibly last 2 months in case of queries??)


declare
	@fromDate smalldatetime = '20110101'
	,@toDate smalldatetime = (select cast(cast((getdate())as date)as datetime))
	,@refreshData tinyint = 1


Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime
	,UserID
	)
select
	getdate()
	,'Start'
	,getdate()
	,suser_name()



update dbo.Parameter
set
	DateValue = @toDate
where
	 Parameter = 'RTTCENSUSDATE'

if @@rowcount = 0
	insert into dbo.Parameter
		(
		 Parameter
		,DateValue
		)
	select
		 'RTTCENSUSDATE'
		,@toDate


--delete from dbo.RTTFact
--where
--	CensusDate < dateadd(day, (select NumericValue*-7 from dbo.Parameter where Parameter = 'KEEPSNAPSHOTWEEKS'), getdate())
--and	CensusDate not in (select max(CensusDate) from dbo.RTTFact group by datepart(year, CensusDate), datepart(month, CensusDate))


--Build the dataset tables
--copy various datasets into local tables

truncate table dbo.Inpatient
truncate table dbo.Outpatient
truncate table dbo.Referral
truncate table dbo.InpatientWL
truncate table dbo.OutpatientWL

Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime

	)
select
	getdate()
	,'TablesTruncated'
	,getdate()


if @refreshData = 1
begin
	--ensure the EncounterRecnoMap table is up-to-date
	exec BuildEncounterRecnoMap
	
	Insert into RTT.[AuditLog]
		(
		EventTime
		,[Event]
		,EndTime
		,UserID
		)
	select
		getdate()
		,'EncounterRecnoMapComplete'
		,getdate()
		,suser_name()

	--fetch latest activity data from the Warehouse
	exec dbo.BuildInpatient
	exec dbo.BuildOutpatient
	exec dbo.BuildReferral
	exec dbo.BuildInpatientWL @toDate
	exec dbo.BuildOutpatientWL @toDate

	Insert into RTT.[AuditLog]
		(
		EventTime
		,[Event]
		,EndTime
		,UserID
		)
	select
		getdate()
		,'BaseTablesCreated'
		,getdate()
		,suser_name()
/*
	-- 20150205 RR added truncate following conversation with JR
	Truncate table dbo.InpatientSnapshot
	Truncate table dbo.InpatientWLSnapshot
	Truncate table dbo.OutpatientSnapshot
	Truncate table dbo.OutpatientWLSnapshot
	Truncate table dbo.ReferralSnapshot

	exec dbo.BuildSnapshots @toDate

*/ 

	-- Clear down old snapshot data
	/* As we're testing and due to performance, JR advised to truncate this table.  I take a copy at a later stage for GynaePathwayHistory which contains all events, we have this for every census date
	delete from dbo.MatchMaster
	where
		CensusDate < dateadd(day, (select NumericValue*-7 from dbo.Parameter where Parameter = 'KEEPSNAPSHOTWEEKS'), getdate())
	and	CensusDate not in (select max(CensusDate) from dbo.MatchMaster group by datepart(year, CensusDate), datepart(month, CensusDate))
	*/

	Truncate table dbo.MatchMaster
	-- Build Match
	exec BuildMatch @fromDate, @toDate
	
	Insert into RTT.[AuditLog]
		(
		EventTime
		,[Event]
		,EndTime
		,UserID
		)
	select
		getdate()
		,'MatchMasterComplete'
		,getdate()
		,suser_name()

end

/*
else
--use snapshot data
begin
	exec dbo.BuildInpatientFromSnapshot @toDate
	exec dbo.BuildOutpatientFromSnapshot @toDate
	exec dbo.BuildReferralFromSnapshot @toDate
	exec dbo.BuildInpatientWLFromSnapshot @toDate
	exec dbo.BuildOutpatientWLFromSnapshot @toDate
end
*/
Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime
	,UserId
	)
select
	getdate()
	,'OriginalProcessComplete'
	,getdate()
	,suser_name()



---------------------------------------------------------------
--20140619 RR added in new Gynae section


Execute dbo.BuildInpatientWLAll @toDate
Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime
	,UserId
	)
select
	getdate()
	,'IPWLAllCreated'
	,getdate()
	,suser_name()

exec [RTT].[CreatePathwayEventBase]
Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime
	,UserID
	)
select
	getdate()
	,'CreatePathwayEventBase'
	,getdate()
	,suser_name()

Execute [RTT].[BuildPathwayEvent]
Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime
	,UserID
	)
select
	getdate()
	,'BuildPathwayEvents'
	,getdate()
	,suser_name()

exec [RTT].[GynaePathwayResults]
Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime
	,UserId
	)
select
	getdate()
	,'GynaeResultsAvailable'
	,getdate()
	,suser_name()
	
exec WarehouseReporting.RTT.BuildPathwayValidation 
Insert into RTT.[AuditLog]
	(
	EventTime
	,[Event]
	,EndTime
	,UserID
	)
select
	getdate()
	,'ValidationReportCreated'
	,getdate()
	,suser_name()
-----------------------------------------------------------------


---- Build the fact table
--exec BuildRTTFact @toDate
--Print 'Refresh RTTFact table complete'

--Build the drillthrough table
--exec BuildOlapRTTDrillThroughBase @toDate
--exec BuildOlapRTTDrillThrough
--Print 'Refresh Drill Through table complete'

-- the batch closure relies on the Fact and Drill tables being available for a snapshot.
-- look for a more elegant way of doing this during the initial Fact build to avoid reprocessing

-- Batch Closure of non-admitted backlog pathways with RTT End Date
--exec BatchCloseBacklog @toDate










/*  20140507 RR excluded the following, as adopted a different approach
-- Rebuild the fact table to incorporate Batch Closures
exec BuildRTTFact @toDate

-- Rebuild the drillthrough table to incorporate Batch Closures
exec BuildOlapRTTDrillThroughBase @toDate

-- Make Code 21s Non-Reportable
exec BatchMake21NonReportable @toDate

-- Make Pathways with SourceOfReferralCode = 'WDS' non-reportable
exec BatchMakeWDSNonReportable @toDate


--20140422 RR added more reasons for non reportable, these need to be validated.
exec dbo.BatchMake90NonReportable @toDate
exec dbo.BatchMake98NonReportable @toDate
exec dbo.BatchMakeAECNonReportable @toDate

exec BatchMakeIncompletePTLwithPrevEndDateNonReportable @toDate
exec dbo.BatchMakeIPWLwithEndDateNonReportable @toDate
exec BatchMakeStopsInPrevMonthNonReportable @toDate
exec BatchMakeCurrStatus90NonReportable @toDate
exec dbo.BatchMakeOPApptType5NonReportable @toDate
--exec dbo.BatchMakeOPWLRem5NonReportable @toDate
exec dbo.BatchMakeNullPathwayIDsNonReportable @toDate
*/
/*
execute RTT_BuildEventsTable
Print 'Events Proc completed'
execute RTT_UpdatePathwayEvents
Print 'Pathway Events updated'

*/

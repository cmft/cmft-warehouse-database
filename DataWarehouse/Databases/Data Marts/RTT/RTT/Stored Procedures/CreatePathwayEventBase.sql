﻿
-- I did have the following as a View, but due to inserting rows for those with no ID, not in MM and but linked on patient and encounter, used census

CREATE Procedure [RTT].[CreatePathwayEventBase] --@census datetime 
As


/*
1)
Create table containing all Events.  
Referrals with and without an RTTPathwayID for last 3yrs; 
Discharges with or without an RTTPathwayID for last 3yrs; 
OPAppts,OPWL,IP,IPWL with an RTTPathwayID
2)
Link to the Events table and identify and OPAppts,OPWL,IP,IPWL which link to the Referral Encounter via Match Master and are not already in the table
3)
Link to the Events table and identify any OPAppts,OPWL which link to the Referral using PatientID and EncounterID and are not already in the table
*/

declare 
	@census datetime
	,@start datetime 
set @census= (
			select DateValue 
			from dbo.parameter 
			where Parameter = 'RTTCENSUSDATE'
			)

set @start= (
			select 
				DateValue 
			from 
				dbo.parameter 
			where 
				Parameter = 'RTTSTARTDATE'
			)

			

-- 1)
Truncate Table RTT.EventBase
Insert Into RTT.EventBase

select 
	OrderID = Null -- row_Number() over (Partition by RTTPathwayID Order by EventTime)
	,*
from
	(
	
	-- Referrals
	select 
		 EncounterRecNo
		,SourcePatientNo
		,SourceEncounterNo
		,DistrictNo
		,ConsultantCode
		,SpecialtyCode
		,SourceofReferralCode
		,EventDate = ReferralDate
		,EventTime = ReferralDate
		,EventType = 'OPReferral' 
		,ApptType = null
		,ClinicCode = cast(null as nvarchar(50))
		,OutcomeCode = null
		,DisposalCode = null
		,DisposalReason = null
		,RTTPathwayID = case 
							when RTTPathwayID is not null then RTTPathwayID
							else ('NoIDOPRW3_'+SourcePatientNo+'_'+SourceEncounterNo)
						end
		,EvRTTPathwayID = case 
							when RTTPathwayID is not null then RTTPathwayID
							else ('NoIDOPRW3_'+SourcePatientNo+'_'+SourceEncounterNo)
						end
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateOfDeath
		,AdminCategoryCode
		,RTTPeriodStatus = Null
	from 
		dbo.Referral
	where
		-- RTTPathwayID is not null -- 20140519 RR - advised by Gareth to include all Referrals whether or not they have a PathwayID
		ReferralDate >= @start
	--526279

	-- Referral Discharges
	union

	select 
		EncounterRecNo
		,SourcePatientNo
		,SourceEncounterNo
		,DistrictNo
		,ConsultantCode
		,SpecialtyCode
		,SourceofReferralCode
		,EventDate = DischargeDate
		,EventTime = case 
						when left(cast(DischargeTime as Time),4) = '23:59' then DischargeTime 
						else dateadd(mi,1,DischargeTime) 
					end
		,EventType = 'OPDischarge'
		,ApptType = null
		,ClinicCode = Null
		,OutcomeCode = null
		,DisposalCode = DischargeReasonCode
		,DisposalReason = DischargeReason
		,RTTPathwayID = case 
							when RTTPathwayID is not null then RTTPathwayID
							else ('NoIDOPRW3_'+SourcePatientNo+'_'+SourceEncounterNo)
						end
		,EvRTTPathwayID = case 
							when RTTPathwayID is not null then RTTPathwayID
							else ('NoIDOPRW3_'+SourcePatientNo+'_'+SourceEncounterNo)
						end
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateofDeath
		,AdminCategoryCode
		,RTTPeriodStatus = Null
	from 
		dbo.Referral
	where 
		--RTTPathwayID is not null and 
		DischargeDate is not null
	and DischargeDate >= @start

	-- OP Appts
	union

	select 
		EncounterRecNo
		,SourcePatientNo
		,SourceEncounterNo
		,DistrictNo
		,ConsultantCode
		,SpecialtyCode
		,SourceofReferralCode
		,EventDate = AppointmentDate
		,EventTime = AppointmentTime
		,EventType = 'OPApp'
		,ApptType = AppointmentTypeCode
		,ClinicCode
		,OutcomeCode = AttendanceOutcomeCode
		,DisposalCode
		,DisposalReason = case 
							when ProcedureTypeCode='T' then 'Treatment' 
							when ProcedureTypeCode = 'D' then 'Diagnostic' 
							when PrimaryOperationCode is not null and left(PrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
							else null 
						end
		,RTTPathwayID
		,EvRTTPathwayID = RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateofDeath
		,AdminCategoryCode
		,RTTPeriodStatusCode
	from 
		dbo.Outpatient 

	left join dbo.DiagnosticProcedure 
	on PrimaryOperationCode = ProcedureCode

	where 
		RTTPathwayID is not null 
	and DNACode in ('5','3')  -- Attendances or DNAs only, not bringing through hospital or patient cancellations
	and	ReferralDate >= @start
	--Inpatient
	union

	select 
		EncounterRecNo
		,SourcePatientNo
		,SourceSpellNo
		,DistrictNo
		,ConsultantCode
		,SpecialtyCode
		,SourceofReferralCode = AdmissionMethodCode
		,EventDate = AdmissionDate
		,EventTime = AdmissionTime
		,EventType = 'IPAdmis'
		,ApptType = PatientClassificationCode
		,ClinicCode = Null
		,OutcomeCode = WaitingListCode
		,DisposalCode = ProcedureTypeCode
		,DisposalReason = case 
							when ProcedureTypeCode='T' then 'Treatment' 
							when ProcedureTypeCode = 'D' then 'Diagnostic' 
							when PrimaryOperationCode is not null and left(PrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
							else null 
						end
		,RTTPathwayID
		,EvRTTPathwayID = RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateofDeath
		,AdminCategoryCode
		,RTTPeriodStatusCode
	from 
		dbo.Inpatient 
		
	left join dbo.DiagnosticProcedure 
	on PrimaryOperationCode = ProcedureCode

	where 
		RTTPathwayID is not null
	and AdmissionDate >= @start
	union

	select 
		EncounterRecNo
		,SourcePatientNo
		,SourceEncounterNo
		,DistrictNo
		,ConsultantCode
		,SpecialtyCode
		,SourceofReferralCode
		,EventDate = case 
						when AppointmentDate is null then dateadd(year,50,referraldate) 
						else AppointmentDate 
					end
		,EventTime = case 
						when AppointmentTime is null then dateadd(year,50,referraldate) 
						else AppointmentTime 
					end
		,EventType = 'OPWL'
		,ApptType = AppointmentTypeCode
		,ClinicCode 
		,OutcomeCode = Null
		,DisposalCode = Null
		,DisposalReason = Null
		,RTTPathwayID
		,EvRTTPathwayID = RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateofDeath
		,AdminCategoryCode
		,RTTPeriodStatus = Null
	from 
		dbo.OutpatientWL 
	where 
		RTTPathwayID is not null
	and ReferralDate >= @start
	
	--IPWLAdd
	union

	select 
		EncounterRecNo
		,SourcePatientNo
		,SourceEncounterNo
		,DistrictNo
		,ConsultantCode
		,SpecialtyCode
		,SourceofReferralCode = AdmissionMethodCode
		,EventDate = OriginalDateOnWaitingList
		,EventTime = OriginalDateOnWaitingList
		,EventType = 'IPWLAdd'
		,ApptType = ManagementIntentionCode
		,ClinicCode = Null
		,OutcomeCode = WaitingListCode
		,DisposalCode = ProcedureTypeCode
		,DisposalReason = case 
							when ProcedureTypeCode='T' then 'Treatment' 
							when ProcedureTypeCode = 'D' then 'Diagnostic' 
							when IntendedPrimaryOperationCode is not null and left(IntendedPrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
							else null 
						end
		,RTTPathwayID
		,EvRTTPathwayID = RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateofDeath
		,AdminCategoryCode
		,RTTPeriodStatus = Null
	from 
		dbo.InpatientWLAll 

	left join dbo.DiagnosticProcedure 
	on IntendedPrimaryOperationCode = ProcedureCode

	where 
		RTTPathwayID is not null 
	and DateOnWaitingList >= @start
	
	--IPWLAdd - TCI dates
	union

	select 
		EncounterRecNo
		,SourcePatientNo
		,SourceEncounterNo
		,DistrictNo
		,ConsultantCode
		,SpecialtyCode
		,SourceofReferralCode = AdmissionMethodCode
		,EventDate = case 
						when TCIDate is null then dateadd(year,50,OriginalDateOnWaitingList) 
						else TCIDate 
					end
		,EventTime = case 
						when TCIDate is null then dateadd(year,50,OriginalDateOnWaitingList) 
						when left(cast(TCIDate as Time),5)='00:00' then dateadd(mi,1,TCIDate)
						else dateadd(mi,-1,TCIDate) 
					end
		,EventType = 'IPWLTCI'
		,ApptType = ManagementIntentionCode
		,ClinicCode = Null
		,OutcomeCode = WaitingListCode
		,DisposalCode = ProcedureTypeCode
		,DisposalReason = case 
							when ProcedureTypeCode='T' then 'Treatment' 
							when ProcedureTypeCode = 'D' then 'Diagnostic' 
							when IntendedPrimaryOperationCode is not null and left(IntendedPrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
							else null 
						end
		,RTTPathwayID
		,EvRTTPathwayID = RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateofDeath
		,AdminCategoryCode
		,RTTPeriodStatus = Null
	from 
		dbo.InpatientWLAll 

	left join dbo.DiagnosticProcedure 
	on IntendedPrimaryOperationCode = ProcedureCode

	where 
		RTTPathwayID is not null 
	and TCIDate is not null
	and DateOnWaitingList >= @start
	
	-- WLRemovals
	union

	select 
		EncounterRecNo = Null -- (Do not have EncounterRecNo for Rem)
		,Rem.SourcePatientNo
		,Rem.SourceEntityRecNo
		,DistrictNo
		,Rem.ConsultantCode
		,Ev.SpecialtyCode
		,SourceofReferralCode = Null
		,EventDate = ActivityDate
		,EventTime = ActivityTime
		,EventType = 'IPWLRem'
		,ApptType = DisposalReason
		,ClinicCode = Null
		,OutcomeCode = WaitingListCode
		,DisposalCode = RemovalReasonCode
		,DisposalReason = RemovalComment
		,RTTPathwayID
		,EvRTTPathwayID = Ev.RTT
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,DateofDeath
		,AdminCategoryCode
		,RTTPeriodStatus = Null
	from 
		Warehouse.APC.WaitingListActivity Rem 

	inner join 
			(select 
				SourcePatientNo
				,SourceEncounterNo
				,SpecialtyCode
				,DistrictNo
				,DisposalReason = case 
									when ProcedureTypeCode='T' then 'Treatment' 
									when ProcedureTypeCode = 'D' then 'Diagnostic' 
									when IntendedPrimaryOperationCode is not null and left(IntendedPrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
									else null 
								end
				,RTTPathwayID
				,RTT = RTTPathwayID
				,RTTPathwayCondition
				,RTTStartDate
				,RTTEndDate
				,RTTCurrentStatusCode
				,RTTCurrentStatusDate
				,DateofDeath
			from 
				dbo.InpatientWLAll 
			
			left join dbo.DiagnosticProcedure 
			on IntendedPrimaryOperationCode = ProcedureCode
			
			where 
				rttpathwayid is not null
		
		union
		
			select 
				IPWL.SourcePatientNo
				,IPWL.SourceEncounterNo
				,IPWL.SpecialtyCode
				,IPWL.DistrictNo
				,DisposalReason = case 
									when ProcedureTypeCode='T' then 'Treatment' 
									when ProcedureTypeCode = 'D' then 'Diagnostic' 
									when IntendedPrimaryOperationCode is not null and left(IntendedPrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
									else null 
								end
				,Ref.RTTPathwayID
				,RTT = IPWL.RTTPathwayID
				,Ref.RTTPathwayCondition
				,RTTStartDate = Null
				,RTTEndDate = Null
				,RTTCurrentStatusCode = Null
				,RTTCurrentStatusDate = Null
				,IPWL.DateofDeath
			from 
				dbo.MatchMaster MM 
			
			inner join dbo.Referral Ref 
			on ReferralRecNo = Ref.EncounterRecNo
		
			inner join dbo.InpatientWL IPWL 
			on InpatientWLRecNo = IPWL.EncounterRecNo
			
			left join dbo.DiagnosticProcedure 
			on IntendedPrimaryOperationCode = ProcedureCode
		
			where 
				Ref.RTTPathwayID is not null 
				and IPWL.RTTPathwayID is null
		) Ev

	on Rem.SourcePatientNo = Ev.SourcePatientNo 
		and Rem.SourceEntityRecNo = Ev.SourceEncounterNo

	where 
		ActivityTypeCode = '4' -- Removals
	and ActivityDate >= @start
	) RTTEv

----------------------------------------------------------

-- 2) Use RTT.MatchMaster
Truncate table RTT.EventTemp
	
Insert into 
	RTT.EventTemp
select 
	RTTPathwayID
	,RTTPathwayCondition
	,EncounterRecNo
from 
	RTT.EventBase 
where 
	EventType = 'OPReferral'


-- OP Appts no pathway ID, but link found

Insert into RTT.EventBase

select 
	OrderID = Null
	,OP.EncounterRecNo
	,OP.SourcePatientNo
	,OP.SourceEncounterNo
	,OP.DistrictNo
	,OP.ConsultantCode
	,OP.SpecialtyCode
	,OP.SourceofReferralCode
	,EventDate = OP.AppointmentDate
	,EventTime = OP.AppointmentTime
	,EventType = 'OPApp(MM)'
	,ApptType = OP.AppointmentTypeCode
	,ClinicCode
	,OutcomeCode = OP.AttendanceOutcomeCode
	,OP.DisposalCode
	,DisposalReason = case 
						when ProcedureTypeCode='T' then 'Treatment' 
						when ProcedureTypeCode = 'D' then 'Diagnostic' 
						when PrimaryOperationCode is not null and left(PrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
						else null 
					end
	,Ev.RTTPathwayID
	,EvRTTPathwayID = OP.RTTPathwayID
	,Ev.RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate = Null
	,RTTCurrentStatusCode = Null
	,RTTCurrentStatusDate = Null
	,OP.DateofDeath
	,OP.AdminCategoryCode
	,OP.RTTPeriodStatusCode
from 
	dbo.MatchMaster MM 
	
inner join dbo.Referral Ref 
on ReferralRecNo = Ref.EncounterRecNo

inner join dbo.Outpatient OP 
on OutpatientRecNo = OP.EncounterRecNo

inner join RTT.EventTemp Ev
on Ref.EncounterRecNo = Ev.EncounterRecNo

left join dbo.DiagnosticProcedure 
on PrimaryOperationCode = ProcedureCode

where 
	MM.Censusdate = @census
and DNACode in ('5','3')  -- Attendances or DNAs
and not exists 
		(select 
			1
		from RTT.EventBase EvApp
		where 
			OP.SourcePatientNo = EvApp.SourcePatientNo 
		and OP.SourceEncounterNo = EvApp.SourceEncounterNo
		and OP.AppointmentTime = EvApp.EventTime
		)

	
-- IP Appts no pathway ID, but link found
Insert into RTT.EventBase

select  
	OrderID = Null
	,IP.EncounterRecNo
	,IP.SourcePatientNo
	,IP.SourceSpellNo
	,IP.DistrictNo
	,IP.ConsultantCode
	,IP.SpecialtyCode
	,SourceofReferralCode = IP.AdmissionMethodCode
	,EventDate = IP.AdmissionDate
	,EventTime = IP.AdmissionTime
	,EventType = 'IPAdmis(MM)'
	,ApptType = IP.PatientClassificationCode
	,ClinicCode = Null
	,OutcomeCode = WaitingListCode
	,DisposalCode = ProcedureTypeCode
	,DisposalReason = case 
						when ProcedureTypeCode='T' then 'Treatment' 
						when ProcedureTypeCode = 'D' then 'Diagnostic' 
						when PrimaryOperationCode is not null and left(PrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
						else null 
					end
	,Ev.RTTPathwayID
	,EvRTTPathwayID = IP.RTTPathwayID
	,Ev.RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate = Null
	,RTTCurrentStatusCode = Null
	,RTTCurrentStatusDate = Null
	,IP.DateofDeath
	,IP.AdminCategoryCode
	,IP.RTTPeriodStatusCode
from 
	dbo.MatchMaster MM 

inner join dbo.Referral Ref 
on ReferralRecNo = Ref.EncounterRecNo

inner join dbo.Inpatient IP 
on InpatientRecNo = IP.EncounterRecNo

inner join RTT.EventTemp Ev
on Ref.EncounterRecNo = Ev.EncounterRecNo

left join dbo.DiagnosticProcedure 
on PrimaryOperationCode = ProcedureCode

where 
	MM.CensusDate = @census
and not exists 
		(select 
			1
		from RTT.EventBase EvApp
		where 
			IP.SourcePatientNo = EvApp.SourcePatientNo 
		and IP.SourceSpellNo = EvApp.SourceEncounterNo
		and IP.AdmissionTime = EvApp.EventTime
		)

-- IP Appts no pathway ID, no link found but join on PatientNo and EncounterNo
	-- can't do this as encounter number different for IP
	-- Discussed with Duscia about matching on Spec, Cons, appt date = date added to WL, but trying to do it in a way they are linked by RTTPathwayID.

	-- OPWL no pathway ID, but link found

Insert into RTT.EventBase

select 
	OrderID = Null
	,OPWL.EncounterRecNo
	,OPWL.SourcePatientNo
	,OPWL.SourceEncounterNo
	,OPWL.DistrictNo
	,OPWL.ConsultantCode
	,OPWL.SpecialtyCode
	,OPWL.SourceofReferralCode
	,EventDate = case 
					when OPWL.AppointmentDate is null then dateadd(year,50,OPWL.referraldate) 
					else OPWL.AppointmentDate 
				end
	,EventTime = case 
					when OPWL.AppointmentTime is null then dateadd(year,50,OPWL.referraldate) 
					else OPWL.AppointmentTime 
				end
	,EventType = 'OPWL(MM)'
	,ApptType = OPWL.AppointmentTypeCode
	,ClinicCode
	,OutcomeCode = Null
	,DisposalCode = Null
	,DisposalReason = Null
	,Ev.RTTPathwayID
	,EvRTTPathwayID = OPWL.RTTPathwayID
	,Ev.RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate = Null
	,RTTCurrentStatusCode = Null
	,RTTCurrentStatusDate = Null
	,OPWL.DateofDeath
	,OPWL.AdminCategoryCode
	,RTTPeriodStatus = Null
from 
	dbo.MatchMaster MM 

inner join dbo.Referral Ref 
on ReferralRecNo = Ref.EncounterRecNo

inner join dbo.OutpatientWL OPWL 
on futureoprecno = OPWL.EncounterRecNo

inner join RTT.EventTemp Ev
on Ref.EncounterRecNo = Ev.EncounterRecNo

where 
	MM.Censusdate = @census
and not exists 
		(select 
			1
		from RTT.EventBase EvApp
		where 
			OPWL.SourcePatientNo = EvApp.SourcePatientNo 
		and OPWL.SourceEncounterNo = EvApp.SourceEncounterNo
		and OPWL.AppointmentTime = EvApp.EventTime
		)

-- IPWL no pathway ID, but link found
-- Need to update to the new InpatientWL, but need the RTT.MatchMaster to use this as well
Insert into RTT.EventBase

select 
	OrderID = Null
	,IPWL.EncounterRecNo
	,IPWL.SourcePatientNo
	,IPWL.SourceEncounterNo
	,IPWL.DistrictNo
	,IPWL.ConsultantCode
	,IPWL.SpecialtyCode
	,SourceofReferralCode = IPWL.AdmissionMethodCode
	,EventDate = IPWL.OriginalDateOnWaitingList
	,EventTime = IPWL.OriginalDateOnWaitingList
	,EventType = 'IPWLAdd(MM)'
	,ApptType = IPWL.ManagementIntentionCode
	,ClinicCode = Null
	,OutcomeCode = WaitingListCode
	,DisposalCode = ProcedureTypeCode
	,DisposalReason = case 
						when ProcedureTypeCode='T' then 'Treatment' 
						when ProcedureTypeCode = 'D' then 'Diagnostic' 
						when IntendedPrimaryOperationCode is not null and left(IntendedPrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
						else null 
					end
	,Ev.RTTPathwayID
	,EvRTTPathwayID = IPWL.RTTPathwayID
	,Ev.RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate = Null
	,RTTCurrentStatusCode = Null
	,RTTCurrentStatusDate = Null
	,IPWL.DateofDeath
	,IPWL.AdminCategoryCode
	,RTTPeriodStatus = Null
from 
	dbo.MatchMaster MM 

inner join dbo.Referral Ref 
on ReferralRecNo = Ref.EncounterRecNo

inner join dbo.InpatientWL IPWL 
on InpatientWLRecNo = IPWL.EncounterRecNo

inner join RTT.EventTemp Ev
on Ref.EncounterRecNo = Ev.EncounterRecNo

left join dbo.DiagnosticProcedure 
on IntendedPrimaryOperationCode = ProcedureCode

where 
	MM.Censusdate = @census
and not exists 
		(select 
			1
		from RTT.EventBase EvApp
		where 
			IPWL.SourcePatientNo = EvApp.SourcePatientNo 
		and IPWL.SourceEncounterNo = EvApp.SourceEncounterNo
		and IPWL.OriginalDateOnWaitingList = EvApp.EventDate
		)

-- IPWLAll
Insert into RTT.EventBase

select 
	OrderID = Null
	,IPWL.EncounterRecNo
	,IPWL.SourcePatientNo
	,IPWL.SourceEncounterNo
	,IPWL.DistrictNo
	,IPWL.ConsultantCode
	,IPWL.SpecialtyCode
	,SourceofReferralCode = IPWL.AdmissionMethodCode
	,EventDate = IPWL.OriginalDateOnWaitingList
	,EventTime = IPWL.OriginalDateOnWaitingList
	,EventType = 'IPWLAdd(MM)'
	,ApptType = IPWL.ManagementIntentionCode
	,ClinicCode = Null
	,OutcomeCode = WaitingListCode
	,DisposalCode = ProcedureTypeCode
	,DisposalReason = case 
						when ProcedureTypeCode='T' then 'Treatment' 
						when ProcedureTypeCode = 'D' then 'Diagnostic' 
						when IntendedPrimaryOperationCode is not null and left(IntendedPrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
						else null 
					end
	,Ev.RTTPathwayID
	,EvRTTPathwayID = IPWL.RTTPathwayID
	,Ev.RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate = Null
	,RTTCurrentStatusCode = Null
	,RTTCurrentStatusDate = Null
	,IPWL.DateofDeath
	,IPWL.AdminCategoryCode
	,RTTPeriodStatus = Null
from 
	dbo.MatchMaster MM 

inner join dbo.Referral Ref 
on ReferralRecNo = Ref.EncounterRecNo

inner join dbo.InpatientWLAll IPWL 
on InpatientWLAllRecNo = IPWL.EncounterRecNo

inner join RTT.EventTemp Ev
on Ref.EncounterRecNo = Ev.EncounterRecNo

left join dbo.DiagnosticProcedure 
on IntendedPrimaryOperationCode = ProcedureCode

where 
	MM.Censusdate = @census
and not exists 
		(select 
			1
		from RTT.EventBase EvApp
		where 
			IPWL.SourcePatientNo = EvApp.SourcePatientNo 
		and IPWL.SourceEncounterNo = EvApp.SourceEncounterNo
		and IPWL.OriginalDateOnWaitingList = EvApp.EventDate
		)
		
-- 3) OP Appts no pathway ID, no link found, but match on PatientNo and EncounterNo
Insert into RTT.EventBase

	select 
		OrderID = Null
		,OP.EncounterRecNo
		,OP.SourcePatientNo
		,OP.SourceEncounterNo
		,OP.DistrictNo
		,OP.ConsultantCode
		,OP.SpecialtyCode
		,OP.SourceofReferralCode
		,EventDate = OP.AppointmentDate
		,EventTime = OP.AppointmentTime
		,EventType = 'OPApp(Lk)'
		,ApptType = OP.AppointmentTypeCode
		,OP.ClinicCode
		,OutcomeCode = OP.AttendanceOutcomeCode
		,OP.DisposalCode
		,DisposalReason = case 
							when ProcedureTypeCode='T' then 'Treatment' 
							when ProcedureTypeCode = 'D' then 'Diagnostic' 
							when PrimaryOperationCode is not null and left(PrimaryOperationCode,1)not in ('y','z','u') then 'Treatment'
							else null 
						end
		,Ev.RTTPathwayID
		,EvRTTPathwayID = OP.RTTPathwayID
		,Ev.RTTPathwayCondition
		,RTTStartDate = Null
		,RTTEndDate = Null
		,RTTCurrentStatusCode = Null
		,RTTCurrentStatusDate = Null
		,OP.DateofDeath
		,OP.AdminCategoryCode
		,OP.RTTPeriodStatusCode
		
	from 
		dbo.Outpatient OP 

	inner join 
		(select distinct 
			RTTPathwayID
			,RTTPathwayCondition
			,SourcePatientNo
			,SourceEncounterNo
		from 
			RTT.EventBase
		) Ev
	on OP.SourcePatientNo = Ev.SourcePatientNo 
	and OP.SourceEncounterNo = Ev.SourceEncounterNo
--654239	
	
	left join dbo.DiagnosticProcedure 
	on PrimaryOperationCode = ProcedureCode

	where 
		DNACode in ('5','3')  -- Attendances or DNAs
	and ReferralDate >= @start
	and not exists 
		(
		select 
			1
		from 
			RTT.EventBase EvApp
		where 
			OP.SourcePatientNo = EvApp.SourcePatientNo 
		and OP.SourceEncounterNo = EvApp.SourceEncounterNo
		and OP.AppointmentTime = EvApp.EventTime
		)


-- Future OP Appts which haven't been picked up by OPWL
-- I don't understand why these aren't in the OutpatientWL table??
-- Need to do as an Insert as need to make sure we don't duplicate??  only insert those future appointments not already in

Insert into RTT.EventBase

select 
	OrderID = '9999'
	,EncounterRecNo
	,SourcePatientNo
	,SourceEncounterNo
	,DistrictNo
	,ConsultantCode
	,SpecialtyCode
	,SourceofReferralCode
	,EventDate = AppointmentDate
	,EventTime = AppointmentTime
	,EventType = 'OPWL(App)'
	,ApptType = AppointmentTypeCode
	,ClinicCode
	,OutcomeCode = AttendanceOutcomeCode
	,DisposalCode
	,DisposalReason = Null
	,OP.RTTPathwayID
	,EvRTTPathwayID = OP.RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,DateofDeath
	,AdminCategoryCode
	,RTTPeriodStatusCode
from
	dbo.Outpatient OP
	
inner join		
	(
	select 
		RTTPathwayID
		,[Min] = min(EncounterRecNo)
	from 
		dbo.Outpatient 
	where 
		RTTPathwayID is not null 
		and DNACode ='9'
		and AppointmentDate>=@Census
		and not Exists 
				(
				Select
					1
				from
					RTT.EventBase FutureAppointment
				where
					EventDate>=@Census
				and	FutureAppointment.SourcePatientNo = Outpatient.SourcePatientNo
				and	FutureAppointment.SourceEncounterNo = Outpatient.SourceEncounterNo
				and	FutureAppointment.EventDate = Outpatient.AppointmentDate
				--and	FutureAppointment.DoctorCode = Outpatient.DoctorCode		
				)
	group by RTTPathwayID	
	)	FOP
on EncounterRecNo = [Min]	

------------------------------------------------
-- Add in Event for Manual Adjustments
-- End Date
Insert into RTT.EventBase

select distinct
	OrderID = Null
	,EncounterRecNo
	,C.SourcePatientNo
	,C.SourceEncounterNo
	,DistrictNo
	,ConsultantCode
	,SpecialtyCode
	,SourceofReferralCode
	,EventDate = EndDate
	,EventTime = (EndDate +'23:59')
	,EventType = 'ManAdjEnd'
	,ApptType = Null
	,ClinicCode = Null
	,OutcomeCode = Null
	,DisposalCode = Null
	,DisposalReason = Null
	,C.RTTPathwayID
	,EvRTTPathwayID = C.RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate  = Null
	,RTTCurrentStatusCode = Null 
	,RTTCurrentStatusDate = Null
	,DateofDeath
	,AdminCategory
	,RTTPeriodStatus = Null
from
	RTT.EventBase C
inner join
	(
	select 
		SourcePatientNo
		,SourceEncounterNo
		,[Max] = max(EventTime)
		,EndDate
	from 
		RTT.EventBase A

	inner join ManualAdjustment.rtt.adjust B
	on A.SourcePatientNo = B.[Internal No]
	and A.SourceEncounterNo = B.[Episode No]

	where 
		EndDate is not null
	and EventDate <=EndDate
	group by 
		SourcePatientNo
		,SourceEncounterNo
		,EndDate
	) D
on 	C.SourcePatientNo = D.SourcePatientNo 
and C.SourceEncounterNo = D.SourceEncounterNo 
and C.EventTime = D.[Max]

-- Start Date
Insert into RTT.EventBase

select distinct
	OrderID = Null
	,EncounterRecNo
	,C.SourcePatientNo
	,C.SourceEncounterNo
	,DistrictNo
	,ConsultantCode
	,SpecialtyCode
	,SourceofReferralCode
	,EventDate = StartDate
	,EventTime = (StartDate +'23:59')
	,EventType = 'ManAdjStart'
	,ApptType = Null
	,ClinicCode = Null
	,OutcomeCode = Null
	,DisposalCode = Null
	,DisposalReason = Null
	,C.RTTPathwayID
	,EvRTTPathwayID = C.RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate  = Null
	,RTTCurrentStatusCode = Null 
	,RTTCurrentStatusDate = Null
	,DateofDeath
	,AdminCategory
	,RTTPeriodStatus = Null
from
	RTT.EventBase C
inner join
	(select 
		SourcePatientNo
		,SourceEncounterNo
		,[Max] = max(EventTime)
		,StartDate
	from 
		RTT.EventBase A

	inner join ManualAdjustment.rtt.adjust B
	on A.SourcePatientNo = B.[Internal No]
	and A.SourceEncounterNo = B.[Episode No]

	where 
		StartDate is not null
	and EventDate <=StartDate
	group by 
		SourcePatientNo
		,SourceEncounterNo
		,StartDate
	) D
on 	C.SourcePatientNo = D.SourcePatientNo 
and C.SourceEncounterNo = D.SourceEncounterNo 
and C.EventTime = D.[Max]

-- Not 18 weeks
Insert into RTT.EventBase

select distinct
	OrderID = Null
	,EncounterRecNo
	,C.SourcePatientNo
	,C.SourceEncounterNo
	,DistrictNo
	,ConsultantCode
	,SpecialtyCode
	,SourceofReferralCode
	,EventDate = [Update Date]
	,EventTime = ([Update Date] +'23:59')
	,EventType = 'ManAdjNonAct'
	,ApptType = Null
	,ClinicCode = Null
	,OutcomeCode = Null
	,DisposalCode = Null
	,DisposalReason = Null
	,C.RTTPathwayID
	,EvRTTPathwayID = C.RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate = Null
	,RTTEndDate  = Null
	,RTTCurrentStatusCode = Null 
	,RTTCurrentStatusDate = Null
	,DateofDeath
	,AdminCategory
	,RTTPeriodStatus = Null
from
	RTT.EventBase C
inner join
	(select 
		SourcePatientNo
		,SourceEncounterNo
		,[Max] = max(EventTime)
		,EndDate
		,[Update Date]
		,[Not18wks?]
	from 
		RTT.EventBase A

	inner join ManualAdjustment.rtt.adjust B
	on A.SourcePatientNo = B.[Internal No]
	and A.SourceEncounterNo = B.[Episode No]

	where 
		[Not18wks?] is not null
	and EventDate <=[Update Date]
	group by 
		SourcePatientNo
		,SourceEncounterNo
		,EndDate
		,[Update Date]
		,[Not18wks?]
	) D
on 	C.SourcePatientNo = D.SourcePatientNo 
and C.SourceEncounterNo = D.SourceEncounterNo 
and C.EventTime = D.[Max]


------------------------------------

-- Update PathwayNumber to latest
Update A
Set	RTTPathwayID = PathwayNumber
from 
	RTT.EventBase A

inner join [PAS].[Inquire].[RTTPTEPIPATHWAY] B
on A.SourcePatientNo = B.[InternalPatientNumber]
and A.SourceEncounterNo = B.[EpisodeNumber]

-- Historically staff where asked to record new RTTPathwayIDs, the process requires them to be the same to understand stops and starts,
-- Going forward 01/04/2014 they should be the same, DQ report set up to check
-- For data prior to 01/04/2014 if addition to WL within 10days of OPApp for same Cons, assume same pathwayID
Drop table RTT.EventTempPathway
select 
	A.SourcePatientNo
	,A.SpecialtyCode
	,A.ConsultantCode
	,A.EventTime
	,A.EventType
	,AID = A.RTTPathwayID 
	,IPEvTm = B.EventTime
	,IPEvTy = B.EventType
	,BID = B.RTTPathwayID
	,B.EvRTTPathwayID
into 
	RTT.EventTempPathway
from 
	RTT.EventBase A

inner join RTT.EventBase B
on A.SourcePatientNo = B.SourcePatientNo
and A.SpecialtyCode = B.SpecialtyCode
and A.ConsultantCode = B.ConsultantCode
and left(A.EventType,1) = 'O' 
and left(B.EventType,1) = 'I'
and datediff(day,A.EventDate,B.EventDate) between -5 and 10
and A.RTTPathwayID <> B.RTTPathwayID

Update A
set RTTPathwayID = B.AID
from 
	RTT.EventBase A
	
inner join RTT.EventTempPathway B
on A.SourcePatientNo = B.SourcePatientNo
and A.SpecialtyCode = B.SpecialtyCode
and A.ConsultantCode = B.ConsultantCode
and A.EventTime = IPEvTm
and A.EventType = IPEvTy


Update D
set RTTPathwayID = C.RTTPathwayID
from 
	RTT.EventBase D

inner join
	(
	select distinct 
		A.RTTPathwayID
		,B.BID
		,B.EvRTTPathwayID
	from 
		RTT.EventBase A
		
	inner join RTT.EventTempPathway B
	on A.SourcePatientNo = B.SourcePatientNo
	and A.SpecialtyCode = B.SpecialtyCode
	and A.ConsultantCode = B.ConsultantCode
	and A.EventTime = IPEvTm
	and A.EventType = IPEvTy
	and B.EvRTTPathwayID is not null 
	) C
on D.RTTPathwayID = C.EvRTTPathwayID


-- Update OrderID
Update A
Set 
	OrderID = SortOrder
from 
	RTT.EventBase A

inner join
	(
	Select
		RTTPathwayID
		,EventTime
		,SortOrder = Row_Number() over (Partition by RTTPathwayID Order by EventTime)
	From 
		RTT.EventBase
	) B
on A.RTTPathwayID = B.RTTPathwayID
and A.EventTime = B.EventTime

-------------------------------------------------------
-- Update PathwayID
/*
Update E
set 
	RTTPathwayID = LatestPathwayID
from
	RR_RTT.EventBase E
inner join
	(select 
		C.SourcePatientNo
		,C.SourceEncounterNo
		,C.RTTPathwayID
		,D.LatestPathwayID
	from
		(select distinct
			SourcePatientNo
			,SourceEncounterNo
			,RTTPathwayID
		from 
			RR_RTT.EventBase
		where 
			left(EventType,2) ='OP'
		) C
	inner join
	-- Latest OP Event to get the RTTPathwayID relating to this
		(select 
			A.SourcePatientNo
			,A.SourceEncounterNo
			,RTTPathwayID as 'LatestPathwayID'
		from 
			RR_RTT.EventBase A 
		inner join 
			(select 
				SourcePatientNo
				,SourceEncounterNo
				,Latest = max(OrderID) 
			from 
				RR_RTT.EventBase
			where 
				left(EventType,2) ='OP'
			group by
				SourcePatientNo
				,SourceEncounterNo
			) B
		on A.SourcePatientNo = B.SourcePatientNo 
		and A.SourceEncounterNo = B.SourceEncounterNo 
		and A.OrderID = B.Latest
		) D
	on C.SourcePatientNo = D.SourcePatientNo
	and C.SourceEncounterNo = D.SourceEncounterNo
	) F
on E.RTTPathwayID = F.RTTPathwayID
*/



--SELECT TOP 1000 [RTTPTEPIPATHWAYID]
--      ,[EpisodeNumber]
--      ,[InternalPatientNumber]
--      ,[PathwayNumber]
--      ,[PathwayOrgProv]
--  FROM [PAS].[Inquire].[RTTPTEPIPATHWAY]

	




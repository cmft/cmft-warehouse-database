﻿


CREATE Procedure [RTT].[BuildIncompletePathwaysCensus] @Census datetime As

Truncate table  RTT.IncompletePathwaysCensus
Insert into RTT.IncompletePathwaysCensus

select 
	Census = @Census
	,G.NationalSpecialtyCode
	,G.DistrictNo
	,G.RTTPathwayID
	,G.RTTpathwaycondition
	,G.ConsultantCode
	,G.SpecialtyCode
	,F.SourcePatientNo 
	,StartEncounterNo = F.SourceEncounterNo
	,ReferralSource = F.SourceofReferralCode
	,StartDate = F.EventDate
	,BreachDate = dateadd(dd,127,F.EventDate)
	,StartReason = F.EventType
	,StartEvSt = F.EventStatus
	,StartDisposalCode = F.DisposalCode
	,StartClinic = F.ClinicCode
	,LatestEncounterNo = G.SourceEncounterNo
	,LatestDate = G.EventDate
	,LatestReason = G.EventType
	,LatestEvSt = G.EventStatus
	,LatestDisposalCode = G.DisposalCode
	,LatestClinic = G.ClinicCode
	,RTTDaysWaiting = datediff(day,F.EventDate,@Census)
	,RTTWeeksWaiting = (datediff(day,F.EventDate,@Census)/7)
	,Category = 
				case 
					when (datediff(day,F.EventDate,@Census))<='126' then '18wks and Under' 
					else 'Over18wks' 
				end
	,G.DisposalCode
	,G.DisposalReason
	,G.RTTcurrentStatusCode
	,G.EventStatus
	,J.NextEventDate
	,J.NextEventTime
	,J.NextEventType
	,J.NextEventReason
	,G.Reportable
	,J.NextReportable
from 
	(select 
		C.RTTPathwayID
		,StartID = max(OrderID)
		,LatestID
	from	
		RTT.GynaePathwayEvent D 
	
	inner join	
		(
		select 
			A.RTTPathwayID
			,LatestID
		from 
			RTT.GynaePathwayEvent A 
		
		inner join	
			(
			select 
				RTTPathwayID
				,LatestID = max(OrderID)
			from 
				RTT.GynaePathwayEvent
			where 
				EventDate<=@Census
			group by 
				RTTPathwayID
			) B
		on A.RTTPathwayID = B.RTTPathwayID 
			and A.OrderID = B.LatestID 
			and PathwayStatus in ('Start','Ticking')
		) C
	on C.RTTPathwayID = D.RTTPathwayID 
		and D.OrderID <= C.LatestID
		and D.PathwayStatus = 'Start'
	
	group by 
		C.RTTPathwayID
		,LatestID
	) E
inner join RTT.GynaePathwayEvent F 
on E.RTTPathwayID = F.RTTPathwayID 
	and E.StartID = F.OrderID

inner join RTT.GynaePathwayEvent G
on E.RTTPathwayID = G.RTTPathwayID 
	and E.LatestID = G.OrderID

left join  
	(select 
		I.RTTPathwayID
		,NextEventDate = I.EventDate
		,NextEventTime = I.EventTime
		,NextEventType = I.EventType
		,NextEventReason = I.DisposalReason
		,NextReportable = I.Reportable
	from 
		RTT.GynaePathwayEvent I 
	
	inner join
		(
		select 
			RTTPathwayID
			,NextID = min(OrderID)
		from 
			RTT.GynaePathwayEvent
		where 
			EventDate > @Census
		group by 
			RTTPathwayID
		) H
	on I.RTTPathwayID = H.RTTPathwayID 
		and I.OrderID = H.NextID
	) J
on E.RTTPathwayID = J.RTTPathwayID 









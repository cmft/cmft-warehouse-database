﻿
CREATE Procedure [RTT].[BuildPathwayEvent]
As
 
declare 
	@census datetime

set @census= (
			select DateValue 
			from dbo.parameter 
			where Parameter = 'RTTCENSUSDATE'
			)

Update RTT.EventBase
set AdminCategory = 
	case 
		when AdminCategory = 'NHS'			then '01'
		when AdminCategory = 'PAY'			then '02'
		when AdminCategory = 'AME'			then '03'
		when AdminCategory in ('OSN','OSP') then null
		else AdminCategory
	end


--Create an RTT.PathwayEvent table containing 
-- a) everything with an rttpathwayid 
-- b) OP/IP/OPWL/IPWL events without a pathwayid but link to a referral which has a pathway ID using the MatchMaster table

-- delete deleted episodes
delete 
from
	RTT.EventBase 
where 
	EventType in ('IPWLAdd','IPWLAdd(MM)','IPWLTCI')
and	not exists
	(select
		1
	from
		RTT.EventBase WLAdd
	where 
		RTT.EventBase.SourcePatientNo = WLAdd.SourcePatientNo	
	and RTT.EventBase.SourceEncounterNo = WLAdd.SourceEncounterNo
	and	WLAdd.EventType in ('IPWLRem','IPAdmis','IPAdmis(MM)')
	)
and not exists
	(Select 
		1
	from
		(
		select 
			CensusDate
			,SourcePatientNo
			,SourceEncounterNo
		from 
			WarehouseReportingMerged.APCWL.Encounter APCWL
		where
			not exists
				(select
					1
				from
					WarehouseReportingMerged.APCWL.Encounter Latest
				where
					APCWL.CensusDate < Latest.CensusDate
				)
		) APCWL
	where 
		RTT.EventBase.SourcePatientNo = APCWL.SourcePatientNo
	and RTT.EventBase.SourceEncounterNo = APCWL.SourceEncounterNo
	)
	

Truncate table RTT.PathwayEvent
Insert into RTT.PathwayEvent 

Select 
	Census = @census
	,EncounterRecNo
	,SourcePatientNo
	,SourceEncounterNo
	,DistrictNo
	,ConsultantCode
	,SpecialtyCode
	,SourceofReferralCode
	,EventDate
	,EventTime
	,EventType
	,[ApptType]
	,ClinicCode
	,[OutcomeCode]
	,[DisposalCode]
	,[DisposalReason]
	,RTTPathwayID
	,EvRTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,DateofDeath
	,EventStatus = Null
	,OrderID = row_Number() over (Partition by RTTPathwayID Order by EventTime)
	,[Status] = Null
	,PathwayStatus = Null
	,Reportable = 'Y'
	,AdminCategory
	,RTTPeriodStatus
from 
(
		select 
			EncounterRecNo
			,SourcePatientNo
			,SourceEncounterNo
			,DistrictNo
			,ConsultantCode
			,SpecialtyCode
			,SourceofReferralCode
			,EventDate
			,EventTime
			,[EventType]
			,[ApptType]
			,ClinicCode
			,[OutcomeCode]
			,[DisposalCode]
			,[DisposalReason]
			,RTTPathwayID
			,EvRTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,DateofDeath
			,AdminCategory 
			,RTTPeriodStatus 
		
		from 
			RTT.EventBase  -- Base table containing all events by pathway epiref and using matchmaster
	union
	-- Bring through the current status as a separate event (some come through several times same status same status date, so set up to only bring through the latest time that current status appeared)
		select 
			EncounterRecNo
			,SourcePatientNo
			,SourceEncounterNo
			,DistrictNo
			,ConsultantCode
			,SpecialtyCode
			,SourceofReferralCode
			,EventDate = C.RTTCurrentStatusDate
			,EventTime = C.RTTCurrentStatusDate
			,EventType = (left([EventType],2)+'RTTCurrSt')
			,[ApptType]
			,ClinicCode
			,[OutcomeCode]
			,[DisposalCode]
			,[DisposalReason]
			,C.RTTPathwayID
			,C.EvRTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTCurrentStatusCode
			,C.RTTCurrentStatusDate
			,DateofDeath
			,AdminCategory 
			,RTTPeriodStatus
		
		from 
			RTT.EventBase C 
		
		inner join
			(
			select 
				A.RTTPathwayID
				,A.RTTCurrentStatusDate
				,ID = max(orderID)
			from 
				RTT.EventBase A 
			where 
				RTTCurrentStatusDate is not null 
				and RTTCurrentStatusCode is not null 
				and left(RTTCurrentStatusCode,2)<>'99'
			group by 
				A.RTTPathwayID
				,A.RTTCurrentStatusDate
			) D
		on C.RTTPathwayID = D.RTTPathwayID
		and C.OrderID = D.ID
		and C.RTTCurrentStatusDate is not null 
		and C.RTTCurrentStatusCode is not null 
		and left(C.RTTCurrentStatusCode,2)<>'99'
	) as RTT

Print 'RTT Events table created'


----------------------------------------

-- Not Reportable

-- Broker Patients - If patient is a Trafford patient but on IPWL here/IP here, make non reportable as they will be managed on the Trafford PTL
-- Private Patients (Admin Category = 02)
Update Ev
set Reportable = 'N'
from 
	RTT.PathwayEvent Ev 

left join WarehouseOLAPMergedV2.APCWL.WaitingList WL
on Ev.OutcomeCode = SourceWaitingListCode

where 
	Ev.AdminCategory = '02'
or
	(
		left([EventType],4) in ('IPWL','IPAd')
	and WL.SourceContextCode = 'CEN||PAS'
	and WL.IsOriginator = '0' 
	and WL.IsTransferWaitingList = '1'
	)


-- Emergency Gynae Clinics
Update D
set Reportable = 'N'
from 
	RTT.PathwayEvent D
	
inner join	
	(select RTTPathwayID,count(*) as 'C'
	from
		(select distinct 
			A.RTTPathwayID
			,Clinic = left(ClinicCode,3)
		from RTT.PathwayEvent A inner join
			(
			select distinct 
				RTTPathwayID
			from 
				RTT.PathwayEvent
			where 
				left(ClinicCode,3) = 'EGU'
			) B
		 on A.RTTPathwayID = B.RTTPathwayID 
		 and A.EventType in ('OPApp','OPApp(MM)','OPWL','OPWL(App)','OPWL(MM)','OPApp(Lk)')
		 where ClinicCode is not null
		) C
	group by RTTPathwayID
	having count(*)='1'
	) E 
on D.RTTPathwayID = E.RTTPathwayID

-- 20141205 RR added following discussion with TN/Liam
Update Ev
set Reportable = 'N'
from 
	RTT.PathwayEvent Ev 
where 
	SpecialtyCode = 'PCON'


----------------------------------------


-- Update Event based on RTTPeriodStatus
Update RTT.PathwayEvent
set EventStatus = RTTPeriodStatus
where 
	RTTPeriodStatus <> 'Ch'

---------------------------------------------
		
-- Need to find a better way to do the below as it takes too long


Truncate table RTT.PeriodStatus
Insert into RTT.PeriodStatus
select 
	RTTPathwayID
	,[Date] = AppointmentDate 
	,AppointmentTime
	,ApptDate
	,RTTPeriodStatus
from 
	dbo.Outpatient 

inner join PAS.Inquire.OPA
on SourcePatientNo = InternalPatientNumber
and SourceEncounterNo = EpisodeNumber
and SourceUniqueID = OPAID
and AppointmentTime = (convert(datetime,ApptDate,103) +convert(datetime,ApptTime,103))


insert into RTT.PeriodStatus
select 
	RTTPathwayID
	,inpatient.AdmissionDate
	,inpatient.AdmissionTime
	,Ad.AdmissionDate
	,RTTPeriodStatus
from 
	dbo.Inpatient 

inner join Warehouse.PAS.Specialty Spec
on Inpatient.SpecialtyCode = Spec.SpecialtyCode

inner join PAS.Inquire.AdmitDisch Ad
on SourcePatientNo = InternalPatientNumber
and SourceEncounterNo = EpisodeNumber
and inpatient.AdmissionDate = convert(datetime,Ad.AdmissionDate,103) 




Update Ev
set EventStatus = left(St.RTTPeriodStatus,2)
from 
	RTT.PathwayEvent Ev 

inner join RTT.PeriodStatus St
on Ev.RTTPathwayID = St.RTTPathwayID
	and Ev.EventTime = St.AppointmentTime

where 
	left(EventType,5) in ('IPAdm','OPApp')
and EventStatus is null
---------------------------------------------



	
-- Update Event for the manual adjustments
Update RTT.PathwayEvent
set EventStatus  = case 
						when EventType = 'ManAdjEnd' then '39'
						when EventType = 'ManAdjStart' then '19'
						when EventType = 'ManAdjNonAct' then '92'
						else EventStatus
					end

-- Update to Planned
-- Advised by Dusica to use these tables this identifies planned referrals and can assume everything for that sourcepatient and encounter is planned
Update C
set 
	ApptType = '5'
from
	RTT.PathwayEvent  C

inner join
	(select 
		RTTPathwayID
		,A.SourcePatientNo
		,A.SourceEncounterNo
		,[Min] = min(EventDate)
		,[DateOnOPWL] = convert(datetime,[DateOnList],103) 
	from
		RTT.PathwayEvent A

	inner join [PAS].[Inquire].[OPWLENTRY] B
	on A.SourcePatientNo = B.[InternalPatientNumber] 
	and A.SourceEncounterNo = B.[EpisodeNumber]
	where 
		[EventDate] > convert(datetime,[DateOnList],103) 
	and convert(datetime,[DateOnList],103) >='20070101'
	and AppointmentType = '5'
	group by
		RTTPathwayID
		,A.SourcePatientNo
		,A.SourceEncounterNo
		,convert(datetime,[DateOnList],103) 
	) D
on C.RTTPathwayID = D.RTTPathwayID 
and C.SourcePatientNo = D.SourcePatientNo
and C.SourceEncounterNo = D.SourceEncounterNo


Update C
set 
	ApptType = '5'
from
	RTT.PathwayEvent  C

inner join
	(select 
		RTTPathwayID
		,A.SourcePatientNo
		,A.SourceEncounterNo
		,[Min] = min(EventDate)
		,[DateOnOPWL] = convert(datetime,[DateOnList],103) 
	from
		RTT.PathwayEvent A

	inner join [PAS].[Inquire].[OPWLREMENTRY] B
	on A.SourcePatientNo = B.[InternalPatientNumber] 
	and A.SourceEncounterNo = B.[EpisodeNumber]

	where 
		[EventDate] > convert(datetime,[DateOnList],103) 
	and convert(datetime,[DateOnList],103) >='20070101'
	and AppointmentType = '5'
	group by
		RTTPathwayID
		,A.SourcePatientNo
		,A.SourceEncounterNo
		,convert(datetime,[DateOnList],103) 
	) D
on C.RTTPathwayID = D.RTTPathwayID 
and C.SourcePatientNo = D.SourcePatientNo
and C.SourceEncounterNo = D.SourceEncounterNo
	


--------------------------
--------------------------
-- Gynae data
Truncate table RTT.GynaePathwayEvent

Insert into RTT.GynaePathwayEvent

select 
	NationalSpecialtyCode
	,Ev.*
from	
	RTT.PathwayEvent Ev 

inner join Warehouse.PAS.Specialty Spec
on Ev.SpecialtyCode = Spec.SpecialtyCode

where 
	nationalspecialtycode ='502'


Insert into RTT.GynaePathwayEvent

select distinct 
	Spec.NationalSpecialtyCode
	,Ev.*
from 
	RTT.PathwayEvent Ev 
inner join
	(select 
		RTTPathwayID
		,orderID 
	from 
		RTT.GynaePathwayEvent
	) B
on Ev.RTTPathwayID = B.RTTPathwayID 
	and Ev.OrderID<>B.OrderID

left join Warehouse.PAS.Specialty Spec
on Ev.SpecialtyCode = Spec.SpecialtyCode

where 
	Spec.nationalspecialtycode <>'502' 
or Spec.nationalspecialtycode is null


-- Data Quality
-- IPAdm with an RTTPathwayID which do not have an Event Status (this could be the way i'm pulling the data through)
/*  Need to agree DQ issues

Truncate Table RTTDQ
Insert into RTTDQ

select 
	DistrictNo
	,RTTPathwayID
	,EventDate
	,EventTime
	,[EventType]
	,[EventStatus]
	,ClinicCode
	,[Description] = ('5) '+left(EventType,5) + ' Events With no Event Status')
from 
	RTT.GynaePathwayEvent  

where 
	left(EventType,5) in ('IPAdm','OPApp')
and EventStatus is null 
and EventDate>='20140401'

	
Insert into RTTDQ 
	(
	EventType
	,[Description]
	)
select 
	EventType = left(EventType,5)
	,[Description] = ('6 '+cast(count(*)as nvarchar) + ' - '+left(EventType,5)+' Events in Period')
from 
	RTT.GynaePathwayEvent  

where 
	left(EventType,5) in ('IPAdm','OPApp')
and EventDate>='20140401'

group by 
	left(EventType,5)
*/
--------------------
 
-- Update EventStatus

Update 
	Ev
set 
	EventStatus = Dis.RTTStatusCode

from 
	RTT.GynaePathwayEvent Ev 

inner join dbo.DisposalCodes Dis
on Ev.DisposalCode = Dis.DisposalCode

where 
	(
		EventStatus is null 
	or 
		left(EventStatus,2)='99'
	) 
and left([EventType],5)='OPApp'
--and (
--	Dis.DisposalCode in ('P1D1','P1DO') -- Restart Codes - advised by DR/TN
--	or
--	left(Dis.DisposalCode,2) in ('P2','P3','P4')
--	)
--and Dis.DisposalCode not in ('P2DI') 
	
	
-- For removals from the IPWL update Status to complete for treatments.
-- If diagnostic then stop clock if not further event within 30 days following removal  (do these need to be validated??  DQ report, poss monthly audit of 10 to see if appropriate)

--Update 
--	RTT.GynaePathwayEvent
--set 
--	EventStatus = case 
--					when ApptType = 'Treatment' then '37'
--					when DisposalCode in ('DIED','DNA','NR','DPT','OPE','PR','REFD') then '37' -- may need to audit some of these
--					else null 
--				end
--where 
--	[EventType]='IPWLRem'



-- WLEntry has been removed, more than 30 days has past and no further event, assume the removal was a stop.
-- These should be on a DQ report and be audited
-- Please note, they could appear back on list as a long waiter if a future activity is given in the future
Update 
	A
set 
	EventStatus = '38'
from 
	RTT.GynaePathwayEvent A 

left join RTT.PathwayEvent B
on A.RTTPathwayID= B.RTTPathwayID 
	and (A.OrderID+1) = B.OrderID

where 
	A.[EventType]='IPWLRem' 
	and A.EventStatus is null 
	and B.RTTPathwayID is null 
	and datediff(day,A.EventDate,A.Census)>30
	
Update 
	A
set 
	EventStatus = '38'

from 
	RTT.GynaePathwayEvent A 

inner join RTT.PathwayEvent B
on A.RTTPathwayID= B.RTTPathwayID 
	and (A.OrderID+1) = B.OrderID

left join RTT.PathwayEvent C
on A.RTTPathwayID= C.RTTPathwayID 
	and (A.OrderID+2) = C.OrderID
where 
	A.[EventType]='IPWLRem' 
	and A.EventStatus is null 
	and B.EventType in ('IPWLTCI','OPDischarge')
	and 
		(C.RTTPathwayID is null 
		or 
		datediff(day,A.EventDate,C.EventDate)>30
		)
	and datediff(day,A.EventDate,A.Census)>30


Update 
	A
set 
	EventStatus = '38'

from 
	RTT.GynaePathwayEvent A 

inner join RTT.PathwayEvent B
on A.RTTPathwayID= B.RTTPathwayID 
	and (A.OrderID+1) = B.OrderID

inner join RTT.PathwayEvent C
on A.RTTPathwayID= C.RTTPathwayID 
	and (A.OrderID+2) = C.OrderID
	
left join RTT.PathwayEvent D
on A.RTTPathwayID= D.RTTPathwayID 
	and (A.OrderID+3) = D.OrderID
where 
	A.[EventType]='IPWLRem' 
	and A.EventStatus is null 
	and B.EventType in ('IPWLTCI','OPDischarge')
	and C.EventType in ('IPWLTCI','OPDischarge')
	and 
		(D.RTTPathwayID is null 
		or 
		datediff(day,A.EventDate,D.EventDate)>30
		)
	and datediff(day,A.EventDate,A.Census)>30

-- The following sources of referral are non active on referral
Update 
	RTT.GynaePathwayEvent
set 
	EventStatus = '90'
where 
	(
	EventType = 'OPReferral' 
	or OrderID = '1'
	) 
and SourceofReferralCode in ('WDS','EDT','CAS','AEC')



Update 
	RTT.GynaePathwayEvent
set 
	EventStatus = RTTCurrentStatusCode
where 
	EventStatus is null 
	and [EventType] like '%RTTCurrSt'


Update 
	RTT.GynaePathwayEvent
set 
	EventStatus =RTTCurrentStatusCode
where 
	EventStatus is null 
	and cast(EventDate as date) = cast(RTTCurrentStatusDate as date)
	and [EventType] not like '%RTTCurrSt'


-------------------------------------------------------------------------------------------

-- Update Status
Update 
	RTT.GynaePathwayEvent
set 
	[status] = null

-- Update to Stop any events which occur after the patient dies, updating the Event Date to the Date of Death.
Update 
	A
set 
	[Status] = 'Stop',
	[EventDate] = DateofDeath,
	[EventTime] = DateofDeath,
	[EventType] = 'Deceased'
from 
	RTT.GynaePathwayEvent A 

inner join
	(select 
		RTTPathwayID
		,[Min] = min(OrderID)
		,[Max] = max(OrderID)
	from 
		RTT.GynaePathwayEvent
	where 
		DateofDeath is not null 
		and DateofDeath<=EventDate
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID	
	and A.OrderID between B.[Min] and B.[Max]



-- Update non RTT related referrals to non active on referral.
-- Update 1st event to non active, if OP Discharge
-- Update to Non Active if appointment was Planned

Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 'Non Active'
where 
	(
	EventType = 'OPReferral' 
	and SourceofReferralCode in ('WDS','EDT','CAS','AEC')
	)
or	( 
	EventType = 'OPDischarge' 
	and OrderID = '1'
	)
or	(ApptType = '5' 
	and [status] is null
	)
       	
		
-- If the 1st appt was planned make the OPReferral non active
Update 
	A
set 
	[Status] = 'Non Active'
from 
	RTT.GynaePathwayEvent A 

inner join	RTT.GynaePathwayEvent B
on A.RTTPathwayID = B.RTTPathwayID 
and (A.OrderID +1) = B.OrderID

where 
	A.OrderID=1 
and A.[EventType] in ('IPRTTCurrSt','OPRTTCurrSt','OPReferral','OPWL','OPWL(App)','OPWL(MM))')
and B.ApptType = '5' 
		

-- Manual Adjustments - Non Active 
-- a) all events prior to update date
Update A 
set EventStatus  = '92'
	,[Status] = 'Non Active'
from 
	RTT.GynaePathwayEvent A 

inner join RTT.GynaePathwayEvent B
on A.RTTPathwayID = B.RTTPathwayID
and B.EventType = 'ManAdjNonAct'
and A.OrderID <= B.OrderID

-- b) WLTCI if WLAdd was before update date 
Update B
set 
	EventStatus = A.EventStatus
	,[Status] = 'Non Active'
from 
	RTT.GynaePathwayEvent A

inner join RTT.GynaePathwayEvent B
on A.RTTPathwayID = B.RTTPathwayID
and A.EncounterRecNo = B.EncounterRecNo

where 
	left(A.EventType,5) = 'IPWLA'
and A.EventStatus  = '92'
and left(B.EventType,4) = 'IPWL'

-- c) WLAdm if WLAdd was before update date
Update B
set 
	EventStatus = A.EventStatus
	,[Status] = 'Non Active'
from 
	RTT.GynaePathwayEvent A

inner join RTT.GynaePathwayEvent B
on A.RTTPathwayID = B.RTTPathwayID

where 
	left(A.EventType,5) = 'IPWLT'
and A.EventStatus  = '92'
and A.EventDate = B.EventDate
and left(B.EventType,3) = 'IPA'


-- Update Status where EventStatus has been populated 
Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 
			case 
				when left(EventStatus,2) in ('90','91','98','92')	then 'Non Active'
				--when left(EventStatus,2) in ('10','11','12','19') 	then 'Start'
				when EventStatus in ('10A','10B','10G','11A','20A','20B','19') then 'Start' -- 19 relates to the starts on the manualadjustment table
				--when left(EventStatus,2) in ('20')				then 'Ticking'
				when left(EventStatus,2) in ('21')				then 'Out-Trmnt'
				when left(EventStatus,2) in ('30','31','32','34','35','36','37','38','39') 
																then 'Stop' -- 37 and 38 are codes i've added for the WLRem.  There needs to be an audit on these to check they are okay to record as a stop
				--when left(EventStatus,2) = '33' and left([EventType],5)in ('OPApp','OPRTT','IPRTT') 
				--												then 'Start'
				else [Status]
			end
where 
	EventStatus is not null 
and [Status] is null



--- DNA Section
-- The users are not using 33A appropriately.  33A is being used as 1st appt DNA nullify clock when patient has actually attended.  Remove 33A as criteria and rely solely on the actual outcome.

-- Update to Non Active if all Events on the Pathway are DNAs
Update 
	C
set 
	[Status] = 'Non Active'
from 
	RTT.GynaePathwayEvent C 
where 
	RTTPathwayID in 
				(select 
					RTTPathwayID
				from
					(select distinct 
						RTTPathwayID
						,OutcomeCode
					from 
						RTT.GynaePathwayEvent 
					where 
						RTTPathwayID in
								(Select distinct 
									RTTPathwayID 
								from 
									RTT.GynaePathwayEvent 
								where 
									[Status] is null 
									and OutcomeCode = 'DNA' 
								)
						and [EventType] not in ('OPReferral','OPDischarge','OPRTTCurrSt','IPRTTCurrSt')
					) A
				group by 
					RTTPathwayID
				having count(*) = 1
				)

-- Been advised to include cancellations and to update the start to the latest appt prior to the first att
-- not sure i agree, fair enough for PC, but for HC???  Discussed with TN, who had an e-mail from NHS England from 2007 who advised you can include cancellation)

Update 
	RTT.GynaePathwayEvent
set 
	[Status] = case 
					when Ref.[Status] = 'Non Active' then 'Non Active'
					else 'Start'
				end
	--'Start'
from
	RTT.GynaePathwayEvent
inner join
	(
	select
		DNA.RTTPathwayID
		,LatestDNA = max(OrderID)
		,FirstActivityDate
	from 
		RTT.GynaePathwayEvent DNA
	inner join
		(
		select
			RTTPathwayID
			,FirstActivityDate = min(OrderID)
		from
			RTT.GynaePathwayEvent
		where
			left(EventType,2) = 'IP'
		or OutcomeCode = 'Att'
		group by
			RTTPathwayID
		) FirstActivity
	on 	DNA.RTTPathwayID = FirstActivity.RTTPathwayID
		and DNA.OrderID < FirstActivityDate
	where DNA.OutcomeCode = 'DNA'	
	group by
		DNA.RTTPathwayID
		,FirstActivityDate
	) DNAs
on RTT.GynaePathwayEvent.RTTPathwayID = DNAs.RTTPathwayID
and RTT.GynaePathwayEvent.OrderID = DNAs.LatestDNA
and RTT.GynaePathwayEvent.[Status] is null

inner join RTT.GynaePathwayEvent Ref
on Ref.RTTPathwayID = RTT.GynaePathwayEvent.RTTPathwayID
and Ref.OrderID = 1

	
Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 'Non Active'
from
	RTT.GynaePathwayEvent
inner join
	(
	select
		DNA.RTTPathwayID
		,LatestDNA = max(OrderID)
		,FirstActivityDate
	from 
		RTT.GynaePathwayEvent DNA
	inner join
		(
		select
			RTTPathwayID
			,FirstActivityDate = min(OrderID)
		from
			RTT.GynaePathwayEvent
		where
			left(EventType,2) = 'IP'
		or OutcomeCode = 'Att'
		group by
			RTTPathwayID
		) FirstActivity
	on 	DNA.RTTPathwayID = FirstActivity.RTTPathwayID
	and DNA.OrderID < FirstActivityDate
	and DNA.OutcomeCode = 'DNA'	
	group by
		DNA.RTTPathwayID
		,FirstActivityDate
	) DNAs
on RTT.GynaePathwayEvent.RTTPathwayID = DNAs.RTTPathwayID
and RTT.GynaePathwayEvent.OrderID < DNAs.LatestDNA
and RTT.GynaePathwayEvent.[Status] is null

Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 'Start'
from
	RTT.GynaePathwayEvent
inner join
	(
	select
		DNA.RTTPathwayID
		,LatestDNA = max(OrderID)
	from 
		RTT.GynaePathwayEvent DNA
	where
		RTTPathwayID
	not in
		(
		select
			distinct RTTPathwayID
		from
			RTT.GynaePathwayEvent
		where
			left(EventType,2) = 'IP'
		or OutcomeCode = 'Att'
		)
	and DNA.OutcomeCode = 'DNA'	
	group by
		DNA.RTTPathwayID
	) DNAs
on RTT.GynaePathwayEvent.RTTPathwayID = DNAs.RTTPathwayID
and RTT.GynaePathwayEvent.OrderID = DNAs.LatestDNA
and RTT.GynaePathwayEvent.[Status] is null

Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 'Non Active'
from
	RTT.GynaePathwayEvent
inner join
	(
	select
		DNA.RTTPathwayID
		,LatestDNA = max(OrderID)
	from 
		RTT.GynaePathwayEvent DNA
	where
		RTTPathwayID
	not in
		(
		select
			distinct RTTPathwayID
		from
			RTT.GynaePathwayEvent
		where
			left(EventType,2) = 'IP'
		or OutcomeCode = 'Att'
		)
	and DNA.OutcomeCode = 'DNA'	
	group by
		DNA.RTTPathwayID
	) DNAs
on RTT.GynaePathwayEvent.RTTPathwayID = DNAs.RTTPathwayID
and RTT.GynaePathwayEvent.OrderID < DNAs.LatestDNA
and RTT.GynaePathwayEvent.[Status] is null

/*
-- If not null and 2nd Event has Outcome DNA update to Start
drop table RTT.GynaePathwayEventDNA
select 
	A.RTTPathwayID
into 
	RTT.GynaePathwayEventDNA
from 
	RTT.GynaePathwayEvent A 

inner join RTT.GynaePathwayEvent B 
on A.RTTPathwayID = B.RTTPathwayID 

inner join RTT.GynaePathwayEvent C
on A.RTTPathwayID = C.RTTPathwayID 

where 
	A.[Status] is null 
and A.OutcomeCode = 'DNA' 
and A.OrderID='2' 
and (B.OutcomeCode <>'DNA' or B.OutcomeCode is null) 
and B.OrderID = '3' 
and C.OrderID = '1' 
and C.[EventType]='OPReferral'



Update 
	A
set 
	[Status] = 'Start'
from 
	RTT.GynaePathwayEvent A 

inner join RTT.GynaePathwayEventDNA B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = '2'


Update 
	A
set 
	[Status] = 'Non Active'
from 
	RTT.GynaePathwayEvent A 
	
inner join RTT.GynaePathwayEventDNA B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = '1'



-- Update if 2nd and 3rd are DNA's, this shouldn't happen as patient should be discharged
drop table RTT.GynaePathwayEventDNA
select 
	A.RTTPathwayID
into 
	RTT.GynaePathwayEventDNA
from 
	RTT.GynaePathwayEvent A 

inner join RTT.GynaePathwayEvent B 
on A.RTTPathwayID = B.RTTPathwayID 

inner join RTT.GynaePathwayEvent C
on A.RTTPathwayID = C.RTTPathwayID 

inner join RTT.GynaePathwayEvent D
on A.RTTPathwayID = D.RTTPathwayID 

where 
	A.[Status] is null 
	and A.OutcomeCode = 'DNA' 
	and A.OrderID='2' 
	and B.OutcomeCode = 'DNA' 
	and B.OrderID='3' 
	and 
		(C.OutcomeCode <>'DNA' 
		or B.OutcomeCode is null
		) 
	and C.OrderID = '4' 
	and D.OrderID = '1' 
	and D.[EventType]='OPReferral'



Update A
set [Status] = 'Start'
from 
	RTT.GynaePathwayEvent A 
inner join RTT.GynaePathwayEventDNA B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = '3'



Update A
set [Status] = 'Non Active'
from 
	RTT.GynaePathwayEvent A 
inner join RTT.GynaePathwayEventDNA B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID in ('1','2')

*/

--- End of DNA Section

-- Update any remaining 1st Events
Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 
		case 
			when [EventType] in ('OPReferral','IPWLAdd','IPWLAdd(MM)','OPWL(MM)','OPWL(App)','OPWL')	
				then 'Start'
			when left([EventType],5)='IPAdm' and DisposalReason = 'Treatment'				
				then 'Non Active' -- 1st event on pathway is treatment, this shouldn't happen, may need to audit some of these
			else 'Start' 
		end 
where 
	OrderID = 1 
and [Status] is null



-- If Status still null and EventType = addition to the WL for treatment make Status Ticking, if previous event is stop or non active this will restart the clock
Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 'Ticking'
where 
	[status] is null
and left([EventType],7) = 'IPWLAdd' 
and DisposalReason = 'Treatment'	



-- If Status is null and patient has been admitted for treatment make this a stop
Update 
	A
set 
	[Status] = 'stop'
from
	RTT.GynaePathwayEvent A

inner join Inpatient B
on A.EncounterRecNo = B.EncounterRecNo

where	
	[status] is null
	and left([EventType],4) = 'IPAd' 
	and (
		DisposalReason = 'Treatment' 
		or
			(
			DisposalReason is null 
			and PrimaryOperationCode is not null 
			)
		)

-- If last event was admission and more than 60 days ago and no event following make this a stop???  Need to check with Tim
-- Going forward these would be picked up in validation as the validation will need to take place within the 60 days for the return
Update 
	A
set 
	[Status] = 'Stop'
from 
	RTT.GynaePathwayEvent A 
inner join
	(select 
		RTTPathwayID
		,ID = max(OrderID)
	from 
		RTT.GynaePathwayEvent
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.ID
where 
	left([EventType],4) = 'IPAd' 
and datediff(day,EventDate,Census)>60 
and EventDate<'20140401'  -- These need validating going forward, but stop the historic activity.
and (
	[Status] in ('Start','Ticking') 
	or [Status] is null
	)

-- If Disposal Code = TC, this means Treatment Commenced, clock stop
Update 
	RTT.GynaePathwayEvent 
set 
	[Status] = 'Stop'
where 
	[EventType]='OPDischarge' 
	and DisposalCode = 'TC'



-- If there is an IPWL or new referral and the previous event was an OPDischarge more than 30 days before new event, update the discharge event to a stop
Update 
	A
set 
	[Status] = 'Stop'
from 
	RTT.GynaePathwayEvent A
inner join
	RTT.GynaePathwayEvent B
on A.RTTPathwayID = B.RTTPathwayID 
	and (A.OrderID +1) = B.OrderID
where 
	A.[Status] is null
	and left(A.[EventType],4) = 'OPDi' 
	and datediff(day,A.EventTime,B.EventTime)>30 
	and 
		(left(B.EventType,5) ='IPWLA'
		or B.EventType = 'OPReferral')


-- 20141204 RR added following discussion with TN and Liam
Update 
	A
set 
	[Status] = 'Stop'
from 
	RTT.GynaePathwayEvent A
inner join
	RTT.GynaePathwayEvent B
on A.RTTPathwayID = B.RTTPathwayID 
	and (A.OrderID +1) = B.OrderID
where 
	A.[Status] is null
	and left(A.[EventType],4) = 'OPDi' 
	and datediff(month,A.EventTime,B.EventTime)>6
		
		
-- If Referral and Discharge on same day and ref is order 1 and discharge order 2 and no order 3, update status to non active
Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 'Non Active'
where 
	[Status] is null 
	and RTTPathwayId in 
				(select distinct 
					A.RTTPathwayID
				from 
					RTT.GynaePathwayEvent A 
				
				inner join RTT.GynaePathwayEvent B
				on A.RTTPathwayID = B.RTTPathwayID 
					and datediff(day,A.EventDate, B.EventDate)between 0 and 14
				
				left join RTT.GynaePathwayEvent C
				on B.RTTPathwayID = C.RTTPathwayID 
					and (B.OrderID+1) =C.OrderID
				
				where 
					A.[EventType] = 'OPReferral' 
					and B.[EventType] = 'OPDischarge' 
					and C.RTTPathwayID is null 
					and B.OrderID = 2
				)

-- If Referral and Discharge and ref is order 1 and discharge order 2 and no order 3, update status to non active
Update 
	RTT.GynaePathwayEvent
set 
	[Status] = 'Non Active'
where 
	[Status] is null 
	and RTTPathwayId in 
				(select distinct 
					A.RTTPathwayID
				from 
					RTT.GynaePathwayEvent A 
				
				inner join RTT.GynaePathwayEvent B
				on A.RTTPathwayID = B.RTTPathwayID 
				and (A.OrderID +1) = B.OrderID
				and datediff(day,A.EventDate, B.EventDate) between 0 and 14
				
				inner join RTT.GynaePathwayEvent C
				on A.RTTPathwayID = C.RTTPathwayID 
				and (A.OrderID +2) = C.OrderID
					
				left join RTT.GynaePathwayEvent D
				on A.RTTPathwayID = D.RTTPathwayID 
					and (A.OrderID+3) =D.OrderID
				
				where 
					A.OrderID = 1
					and A.[EventType] = 'OPReferral' 
					and 
						(
						(B.[EventType] = 'OPDischarge' 
						and 
							(left(C.EventType,4) = 'OPWL' 
							or 
								(left(C.EventType,4) = 'OPAp' 
								and C.OutcomeCode = 'DNA'
								)
							)
						)
						or
						(
							(left(B.EventType,4) = 'OPWL' 
							or 
								(left(B.EventType,4) = 'OPAp' 
								and B.OutcomeCode = 'DNA'
								)
							)
						and C.EventType = 'OPDischarge' 
						)
						)				
					and D.RTTPathwayID is null 
					)
				



-- If the Last Event is OPDischarge and for some reason is coming through as Start or Ticking (possibly from the EventStatus code) change this to a Stop
Update 
	A
set 
	[Status] = 'Stop'
from 
	RTT.GynaePathwayEvent A 

left join RTT.GynaePathwayEvent B
on A.RTTPathwayID = B.RTTPathwayID 
	and (A.OrderID+1) =B.OrderID
where --A.[Status] is not null and
	A.[EventType] = 'OPDischarge' 
	and B.RTTPathwayID is null
	and (A.[Status] in ('Ticking','Start') 
		or A.[Status] is null
		)



-- If there is an IPAdmission on the same day as another event, we want the stop to be picked up on the APC.

Update 
	A
set 
	[Status] = 'Ticking'
from 
	RTT.GynaePathwayEvent A 

inner join
	(select 
		RTTPathwayID
		,OrderID
		,EventDate
	from 
		RTT.GynaePathwayEvent
	where 
		left([EventType],4) = 'IPAd' 
		and (DisposalReason = 'Treatment' or [Status] = 'Stop')
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID<B.OrderID 
	and A.EventDate = B.EventDate



-- If the first event on the pathway is still null, make this a start
Update 
	A
set 
	[Status] = 'Start'
from 
	RTT.GynaePathwayEvent A 

inner join
	(select 
		RTTPathwayID
		,ID = min(OrderID)
	from 
		RTT.GynaePathwayEvent
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
and A.OrderID = B.ID

where 
	[Status] is null



-- Update Status to previous status if null??
Update 
	C
set 
	[Status] = E.[Status]
from 
	RTT.GynaePathwayEvent C 
inner join
	(
	select 
		A.RTTPathwayID 
		,A.OrderID
		,M =  max(B.OrderID)
	from 
		RTT.GynaePathwayEvent A 
	inner join RTT.GynaePathwayEvent B
	on A.RTTPathwayID = B.RTTPathwayID 
		and B.OrderID<A.OrderID
	where 
		A.[Status] is null 
		and B.[Status] is not null
	group by 
		A.RTTPathwayID 
		,A.OrderID
	) D
on C.RTTPathwayID = D.RTTPathwayID 
	and C.OrderID = D.OrderID

inner join RTT.GynaePathwayEvent E
on D.RTTPathwayID = E.RTTPathwayID 
	and D.M = E.OrderID
where 
	C.[Status] is null



-- Update PathwayStatus
execute RTT.UpdatePathwayEventStatus

-- For IVF patients who have had 1st treatment, then come back for 2nd stage within 7 days, don't count the 2nd stage as a stop

Update C
set Reportable = 'N'
from 
	RTT.PathwayStop A

inner join RTT.PathwayStop B
on A.SourcePatientNo = B.SourcePatientNo
and A.StopDate <> B.StopDate
and datediff(day,A.StopDate,B.StopDate) between 0 and 7
and A.SpecialtyCode = B.SpecialtyCode
and datediff(day,B.StartDate,B.StopDate) between 0 and 7
and A.SpecialtyCode = 'Gyn1'

inner join RTT.GynaePathwayEvent C
on B.RTTPathwayID = C.RTTPathwayID 
and C.OrderID between B.StartID and B.StopID
where 
left(A.StopReason,4) = 'IPAd'
and left(B.StopReason,4) = 'IPAd'



Update C
set Reportable = 'N'
from 
	RTT.PathwayStop A

inner join RTT.PathwayStop B
on A.SourcePatientNo = B.SourcePatientNo
and A.StopDate <> B.StopDate
and datediff(day,A.StopDate,B.StopDate) between 0 and 7
and A.SpecialtyCode = B.SpecialtyCode
and datediff(day,A.StartDate, A.StopDate) between 0 and 7
and B.Reportable = 'Y'
and A.SpecialtyCode = 'Gyn1'

inner join RTT.GynaePathwayEvent C
on A.RTTPathwayID = C.RTTPathwayID 
and C.OrderID between A.StartID and A.StopID

where 
left(A.StopReason,4) = 'IPAd'
and left(B.StopReason,4) = 'IPAd'



/* 20140515 met with Sam Evans, felt these shouldn't be excluded should be kept in.
However, only include those with latest start from 01/04/2013

-- If last event was more than 12 months ago and no pending future event, then record last event as stop, reason LegacyPathway.
Update A
set 
	[Status] = 'Stop'
	,DisposalReason = 'LegacyPathway'
from 
	RTT.GynaePathwayEvent A 
inner join
	(select 
		RTTPathwayID,
		ID = max(OrderID)
	from 
		RTT.GynaePathwayEvent
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.ID
where 
	EventDate<(census - 365)
	and [Status] in ('Start','Ticking')


Update A
set 
	[Status] = 'Stop'
	, DisposalReason = 'LegacyPathway(6mthsWL)'
	, EventDate = '20070101'
from 
	RTT.GynaePathwayEvent A 

inner join
		(select 
			RTTPathwayID
			,ID = max(OrderID)
		from 
			RTT.GynaePathwayEvent
		group by 
			RTTPathwayID
		) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.ID

inner join RTT.GynaePathwayEvent C
on C.RTTPathwayID = B.RTTPathwayID 
	and C.OrderID = (B.ID-1)

where 
	B.ID>1 
	and A.EventDate >='20500101' 
	and C.EventDate<(A.census - 182)
	and A.[Status] in ('Start','Ticking')



Update A
set [Status] = 'Stop'
	,DisposalReason = 'LegacyPathway(6mths)'
from 
	RTT.GynaePathwayEvent A 
inner join
	(select 
		RTTPathwayID
		,ID = max(OrderID)
	from 
		RTT.GynaePathwayEvent
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.ID

where 
	EventDate between (census - 365) and (census - 182)
	and [Status] in ('Start','Ticking')



Update A
set 
	[Status] = 'Stop'
	,DisposalReason = 'LegacyPathway(3mths)'
from 
	RTT.GynaePathwayEvent A 
inner join
	(select 
		RTTPathwayID
		,ID = max(OrderID)
	from 
		RTT.GynaePathwayEvent
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.ID
where 
	EventDate between (census - 182) and (census - 91)
	and [Status] in ('Start','Ticking')
*/





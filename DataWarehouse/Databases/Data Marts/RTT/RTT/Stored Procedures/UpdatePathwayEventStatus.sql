﻿

CREATE Procedure [RTT].[UpdatePathwayEventStatus] As

--	20140430	RR


Update 
	RTT.GynaePathwayEvent
set 
	EventDate = null
	,EventTime = null
where 
	EventDate>='20500101'


--Update the RTT Pathway Status
update 
	RTT.GynaePathwayEvent
set 
	PathwayStatus = 'x'



update 
	RTT.GynaePathwayEvent
set 
	PathwayStatus=
				case 
					when [Status]='Ticking' then 'Start' 
					when [Status]='Out-Trmnt' then 'Non Active' 
					else [Status] 
				end
where OrderID='1'



update 
	A
set 
	PathwayStatus=[Status]
from 
	RTT.GynaePathwayEvent A 

inner join
	(select 
		RTTPathwayID,
		[Min] = min(orderID)
	from 
		RTT.GynaePathwayEvent 
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID	
	and A.OrderID = B.[min] 
	and OrderID<>1



update 
	A
set 
	PathwayStatus='Start'
from 
	RTT.GynaePathwayEvent A 

inner join
	(select 
		RTTPathwayID
		,[Min] = min(orderID)
	from 
		RTT.GynaePathwayEvent 
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.[min] 
	and PathwayStatus is null


-- Can we put something in to cancel job if running for more than 2mins??
While 
	(select distinct PathwayStatus from RTT.GynaePathwayEvent where PathwayStatus = 'x') = 'x'	
Begin	
	
Update 
	A	
set 
	PathwayStatus = D.CurrentPathwayStatus	
From 
	RTT.GynaePathwayEvent A 

inner join 	
	(select 
		RTTPathwayID
		,[Min] = min (OrderID)
	from RTT.GynaePathwayEvent 
	where 
		PathwayStatus = 'x' 
	group by 
		RTTPathwayID
	) B
on A.RTTPathwayID = B.RTTPathwayID 
	and A.OrderID = B.[Min]	

inner join RTT.GynaePathwayEvent C
on A.RTTPathwayID = C.RTTPathwayID 
	and A.OrderID = (C.OrderID +1)	

inner join RTT.PathwayStatus D
on A.[Status] = D.CurrentStatus 
	and C.PathwayStatus = D.PreviousPathwayStatus	

End	








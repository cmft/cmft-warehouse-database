﻿

CREATE Procedure [RTT].[GynaePathwayResults] As

-- Results

declare 
	@start datetime
	,@census datetime
	,@MonthEnd datetime
	,@MonthStart datetime


set @start = '20140401'
set @census = (select DateValue from dbo.Parameter where Parameter = 'RTTCENSUSDATE')
set @MonthEnd = dateadd(ss, -1, dateadd(month, datediff(month, 0, getdate()), 0)) 
set @MonthStart = DATEADD(month, DATEDIFF(month, -1, getdate()) - 2, 0) 


If DATEPART(day,@Census) between 15 and 22 
Begin

	Delete 
	from 
		RTT.GynaePathwayEventHistory
	where 
		Census between dateadd(day, -7, @Census) and @Census

	Insert into RTT.GynaePathwayEventHistory
	Select 
		* 
	from 
		RTT.GynaePathwayEvent

End


-- Stops
Truncate table RTT.CompletedPathways
Insert into RTT.CompletedPathways
select 
	DistrictNo
	,A.SourcePatientNo
	,A.RTTPathwayID
	,SpecCode = NationalSpecialtyCode 
	,SpecialtyCode
	,ConsultantCode
	,ReferralDate = B.EventDate
	,IPWLAdd = C.EventDate
	,StartEncounterNo
	,StartID
	,StartDate
	,StartReason
	,StopEncounterNo
	,StopID
	,StopDate
	,StopReason
	,MonthOfStop = left(convert(nvarchar,StopDate,120),7)
	,EventType
	,Category 
	,Census
from 
	RTT.PathwayStop A

left join 
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTT.GynaePathwayEvent
	where EventType = 'OPReferral'
	) B
on A.SourcePatientNo = B.SourcePatientNo
and B.SourceEncounterNo = StartEncounterNo

left join
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTT.GynaePathwayEvent 
	where left(EventType,7) = 'IPWLAdd'
	) C
on A.SourcePatientNo = C.SourcePatientNo
and C.SourceEncounterNo = StartEncounterNo

where 
	StopDate between @MonthStart and @census
and Reportable = 'y'
and NationalSpecialtyCode = '502'


If DATEPART(day,@Census) between 15 and 22 
Begin

	Delete 
	from 
		RTT.CompletedPathwaysSnapshot
	where 
		Census between dateadd(day,-7,@Census) and @Census
		
	Insert into RTT.CompletedPathwaysSnapshot
	Select 
		* 
		,SnapshotDate = getdate()
	from 
		RTT.CompletedPathways
	where 
		StopDate between @MonthStart and @MonthEnd

End

--3854
-- Ticking Detail
Truncate table RTT.IncompletePathways
Insert into RTT.IncompletePathways
	(
	DistrictNo
	,A.RTTPathwayID
	,SpecialtyCode
	,ConsultantCode
	,A.SourcePatientNo
	,StartEncounterNo
	,ReferralDate
	,ReferralSource
	,StartDate
	,StartReason
	,BreachDate
	,StartDisposalCode
	,StartClinic
	,StartEvSt
	,LatestEncounterNo
	,LatestDate
	,LatestReason
	,IPWLAddDate 
	,LatestEvSt
	,LatestDisposalCode
	,LatestClinic
	,RTTWeeksWaiting
	,MonthsSinceLastEvent 
	,RTTCurrentStatusCode
	,EventStatus
	,NextEventDate
	,NextEventTime
	,NextEventType
	,NextEventReason
	,Category 
	,Census
	)
select  
	DistrictNo
	,A.RTTPathwayID
	,SpecialtyCode
	,ConsultantCode
	,A.SourcePatientNo
	,StartEncounterNo
	,ReferralDate = B.EventDate
	,ReferralSource
	,StartDate
	,StartReason
	,BreachDate
	,StartDisposalCode
	,StartClinic
	,StartEvSt
	,LatestEncounterNo
	,LatestDate
	,LatestReason
	,IPWLAddDate = C.EventDate
	,LatestEvSt
	,LatestDisposalCode
	,LatestClinic
	,RTTWeeksWaiting
	,MonthsSinceLastEvent = cast((datediff(day,LatestDate,Census)/30.4) as int)
	,RTTCurrentStatusCode
	,EventStatus
	,NextEventDate
	,NextEventTime
	,NextEventType
	,NextEventReason
	,Category 
	,Census
from 
	RTT.PathwayIncomplete A
	
left join 
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTT.GynaePathwayEvent 
	where EventType = 'OPReferral'
	) B
on A.SourcePatientNo = B.SourcePatientNo
and B.SourceEncounterNo = StartEncounterNo

left join
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTT.GynaePathwayEvent 
	where 
		left(EventType,7) = 'IPWLAdd'
	) C
on A.SourcePatientNo = C.SourcePatientNo
and C.SourceEncounterNo = LatestEncounterNo

where 
	NationalSpecialtyCode = '502'
and Reportable = 'Y' 
and (
	NextReportable = 'Y' 
	or NextReportable is null
	)

-- Last Month End

Execute RTT.BuildIncompletePathwaysCensus @MonthEnd

Truncate table RTT.IncompletePathwaysMonthEnd
Insert into RTT.IncompletePathwaysMonthEnd

select  
	DistrictNo
	,A.RTTPathwayID
	,SpecialtyCode
	,ConsultantCode
	,A.SourcePatientNo
	,StartEncounterNo
	,ReferralDate = B.EventDate
	,ReferralSource
	,StartDate
	,StartReason
	,BreachDate
	,StartDisposalCode
	,StartClinic
	,StartEvSt
	,LatestEncounterNo
	,LatestDate
	,LatestReason
	,IPWLAddDate = C.EventDate
	,LatestEvSt
	,LatestDisposalCode
	,LatestClinic
	,RTTWeeksWaiting
	,MonthsSinceLastEvent = cast((datediff(day,LatestDate,Census)/30.4) as int)
	,RTTCurrentStatusCode
	,EventStatus
	,NextEventDate
	,NextEventTime
	,NextEventType
	,NextEventReason
	,Category 
	,Census
from 
	RTT.IncompletePathwaysCensus A
	
left join 
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTT.GynaePathwayEvent 
	where EventType = 'OPReferral'
	) B
on A.SourcePatientNo = B.SourcePatientNo
and B.SourceEncounterNo = StartEncounterNo

left join
	(select 
		RTTPathwayID
		,SourcePatientNo
		,SourceEncounterNo
		,EventDate
	from
		RTT.GynaePathwayEvent 
	where 
		left(EventType,7) = 'IPWLAdd'
	) C
on A.SourcePatientNo = C.SourcePatientNo
and C.SourceEncounterNo = LatestEncounterNo

where 
	NationalSpecialtyCode = '502'
and Reportable = 'Y' 
and (
	NextReportable = 'Y' 
	or NextReportable is null
	)

If DATEPART(day,@Census) between 15 and 22 
Begin

	Delete 
	from 
		RTT.IncompletePathwaysMonthEndSnapshot
	where 
		Census = @MonthEnd
	
	Insert into RTT.IncompletePathwaysMonthEndSnapshot
	Select 
		* 
		,SnapshotDate = getdate()
	from 
		RTT.IncompletePathwaysMonthEnd
End





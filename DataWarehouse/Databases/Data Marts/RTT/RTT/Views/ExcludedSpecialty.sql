﻿create view [RTT].[ExcludedSpecialty] as

select
	SpecialtyCode = EntityCode
from
	dbo.EntityLookup
where
	EntityTypeCode = 'EXCLUDEDSPECIALTY'


﻿CREATE TABLE [RTT].[WkInpatientWL] (
    [EncounterRecno]    INT           NOT NULL,
    [EncounterEndDate]  SMALLDATETIME NULL,
    [SourcePatientNo]   VARCHAR (20)  NULL,
    [HospitalCode]      VARCHAR (5)   NULL,
    [ConsultantCode]    CHAR (10)     NULL,
    [SpecialtyCode]     CHAR (5)      NULL,
    [DateOnWaitingList] SMALLDATETIME NULL,
    [SourceEntityRecno] INT           NULL,
    [RTTPathwayID]      VARCHAR (25)  NULL
);


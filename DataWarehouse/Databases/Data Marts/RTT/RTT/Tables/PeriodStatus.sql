﻿CREATE TABLE [RTT].[PeriodStatus] (
    [RTTPathwayID]    VARCHAR (25)  NULL,
    [Date]            SMALLDATETIME NULL,
    [AppointmentTime] SMALLDATETIME NULL,
    [ApptDate]        NVARCHAR (10) NULL,
    [RTTPeriodStatus] NVARCHAR (4)  NULL
);


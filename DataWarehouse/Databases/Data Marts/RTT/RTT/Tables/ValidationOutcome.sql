﻿CREATE TABLE [RTT].[ValidationOutcome] (
    [ID]             INT          NOT NULL,
    [Outcome]        VARCHAR (50) NOT NULL,
    [Responsibility] VARCHAR (20) NULL,
    [priority]       INT          NULL,
    CONSTRAINT [PK_RTTValidationOutcome] PRIMARY KEY CLUSTERED ([ID] ASC)
);


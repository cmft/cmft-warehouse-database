﻿CREATE TABLE [RTT].[Parameter] (
    [Parameter]    VARCHAR (128) NOT NULL,
    [TextValue]    VARCHAR (255) NULL,
    [NumericValue] DECIMAL (18)  NULL,
    [DateValue]    DATETIME      NULL
);


﻿CREATE TABLE [RTT].[AuditLog] (
    [AuditLogRecno] INT           IDENTITY (1, 1) NOT NULL,
    [EventTime]     DATETIME      NOT NULL,
    [UserId]        VARCHAR (30)  NULL,
    [Process]       VARCHAR (255) NULL,
    [Event]         VARCHAR (255) NULL,
    [EndTime]       DATETIME      NULL,
    CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED ([AuditLogRecno] ASC)
);


﻿CREATE TABLE [RTT].[MatchMaster] (
    [CensusDate]              SMALLDATETIME NOT NULL,
    [ReferralRecno]           INT           NOT NULL,
    [OutpatientRecno]         INT           NULL,
    [InpatientWLRecno]        INT           NULL,
    [InpatientRecno]          INT           NULL,
    [FutureOPRecno]           INT           NULL,
    [IPToFutureOPRecno]       INT           NULL,
    [AttIPWLTemplateRecno]    INT           NULL,
    [AttSpellTemplateRecno]   INT           NULL,
    [SpellOPWLTemplateRecno]  INT           NULL,
    [RefIPWLTemplateRecno]    INT           NULL,
    [RefSpellTemplateRecno]   INT           NULL,
    [InpatientWLAllRecno]     INT           NULL,
    [AttIPWLAllTemplateRecno] INT           NULL,
    [RefIPWLAllTemplateRecno] INT           NULL
);


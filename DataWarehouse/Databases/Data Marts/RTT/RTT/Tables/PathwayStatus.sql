﻿CREATE TABLE [RTT].[PathwayStatus] (
    [CurrentStatus]         NVARCHAR (255) NULL,
    [PreviousPathwayStatus] NVARCHAR (255) NULL,
    [CurrentPathwayStatus]  NVARCHAR (255) NULL
);


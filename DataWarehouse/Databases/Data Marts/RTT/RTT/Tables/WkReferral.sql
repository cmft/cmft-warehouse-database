﻿CREATE TABLE [RTT].[WkReferral] (
    [EncounterRecno]     INT           NOT NULL,
    [SourcePatientNo]    VARCHAR (20)  NULL,
    [SourceEncounterNo2] INT           NULL,
    [ConsultantCode]     VARCHAR (10)  NULL,
    [SpecialtyCode]      VARCHAR (5)   NULL,
    [HospitalCode]       VARCHAR (5)   NULL,
    [ReferralDate]       SMALLDATETIME NULL,
    [RTTPathwayID]       VARCHAR (25)  NULL
);


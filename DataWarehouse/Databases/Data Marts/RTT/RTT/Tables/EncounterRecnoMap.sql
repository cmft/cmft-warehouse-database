﻿CREATE TABLE [RTT].[EncounterRecnoMap] (
    [EncounterRecno]       INT      IDENTITY (1, 1) NOT NULL,
    [EncounterTypeCode]    CHAR (2) NULL,
    [SourceEncounterRecno] INT      NULL
);


﻿CREATE PROCEDURE Bedman.AcuityScoresForWardDate
	@Ward varchar(10) = null,
	@ScoreDate DateTime = null
AS

select		wardCode,
			ScoresDate,
			ApprovedBy,
			ApprovedAt,
			Total_0,
			Total_1a,
			Total_1b,
			Total_2,
			Total_3,
			TransfersIn,
			TransfersOut,
			Admissions,
			Discharges,
			Deaths,
			Attenders,
			EscortHours,
			EscortEpisodes,
			OpenBeds
from		Bedman.dbo.DailyAcuityScore DAS
where		((DAS.ScoresDate = @ScoreDate) OR (@ScoreDate IS NULL))
and			((DAS.wardCode = @Ward) OR (@Ward IS NULL))
order by DAS.ScoresDate, DAS.wardCode;
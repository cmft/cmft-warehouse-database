﻿CREATE VIEW ORMIS.CancellationReportDaily_WeekWindow
AS
SELECT Left([UN_CODE],1) AS UN_CODE, 
		fu.UN_DESC, 
		ftt.TH_CODE, 
		ftt.TH_DESC, 
		cast(fci.CC_DATE as date) AS Operation_Date, 
		cast([CC_DATE_TIME_CANCELLED] as DATE) AS Date_Cancelled, 
		fcl.CL_CODE, fcl.CL_DESC, 
		Replace([CC_MRN],'Y','/') AS Casenote, 
		[CC_FNAME] + ' - ' + [CC_LNAME] AS 
		Patient_Name, 
		fci.CC_DETAILS, 
		fc.CN_CODE, 
		fc.CN_DESC, 
		fsn.SU_FNAME + ' - ' + fsn.SU_LNAME AS Surgeon, 
		fsn2.SU_FNAME + ' - ' + fsn2.SU_LNAME AS Cancelled_By, 
		fci.CC_PA_SEQU, 
		fci.CC_OP_SEQU, fci.CC_SEQU, f.OP_OPERAT_TYPE 
FROM otprd.dbo.FCCITEMS fci
  LEFT JOIN otprd.dbo.FSURGN fsn ON fci.CC_SU_SEQU = fsn.SU_SEQU 
  LEFT JOIN otprd.dbo.FTHEAT ftt ON fci.CC_TH_SEQU = ftt.TH_SEQU
	LEFT JOIN otprd.dbo.FUNIT fu ON fu.UN_SEQU = ftt.TH_UN_SEQU
  LEFT JOIN otprd.dbo.FCANCEL fc ON fc.CN_SEQU = fci.CC_CN_SEQU
	LEFT JOIN otprd.dbo.F_CANCEL_GROUP fcg ON fc.CN_CG_SEQU = fcg.CG_SEQU
  LEFT JOIN otprd.dbo.FCLASS fcl ON fcl.CL_SEQU = fci.CC_CL_SEQU 
  LEFT JOIN otprd.dbo.FSURGN fsn2 ON fci.cc_cancel_su_sequ = fsn2.SU_SEQU
  LEFT JOIN otprd.dbo.FOPERAT f ON fci.CC_OP_SEQU = f.OP_SEQU 
WHERE 
   ((fci.CC_DATE)>=cast(floor(cast (getdate() as float)-8) as datetime) )
  And (fci.cc_date<DateAdd(day,1,cast(floor(cast (getdate() as float)-1) as datetime))) 
  AND cast([CC_DATE_TIME_CANCELLED] as DATE)=cast([CC_DATE] as DATE)


﻿



CREATE view [Theatre].[Theatre] as

/* 
==============================================================================================
Description:

When		Who			What
?			?			Initial Coding
25/03/2015	Paul Egan	Added SourceAlternativeTheatreCode for theatre booking report
===============================================================================================
*/

select
	SourceContextCode
	,SourceContext
	,SourceTheatreID
	,SourceTheatreCode
	,SourceTheatre
	,LocalTheatreID
	,LocalTheatreCode
	,LocalTheatre
	,NationalTheatreID
	,NationalTheatreCode
	,NationalTheatre
	,OperatingSuiteCode
	,OperatingSuite
	,Colour
	,SourceAlternativeTheatreCode
from
	WarehouseOLAPMergedV2.Theatre.Theatre





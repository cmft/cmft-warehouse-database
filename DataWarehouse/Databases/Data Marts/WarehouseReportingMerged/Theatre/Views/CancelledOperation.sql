﻿
create view Theatre.CancelledOperation as

select
	BaseCancelledOperation.MergeCancelledOperationRecno
	,CancelledOperationRecno
	,SourceUniqueID
	,CasenoteNumber
	,DirectorateCode
	,NationalSpecialtyCode
	,ConsultantCode
	,AdmissionDate
	,ProcedureDate
	,WardCode
	,Anaesthetist
	,Surgeon
	,ProcedureCode
	,CancellationDate
	,CancellationReason
	,TCIDate
	,Comments
	,ProcedureCompleted
	,BreachwatchComments
	,Removed
	,RemovedComments
	,ReportableBreach
	,ContextCode
	,BaseCancelledOperation.Created
	,BaseCancelledOperation.Updated
	,BaseCancelledOperation.ByWhom

	,ContextID
	,WardID
	,CancelledOperationDateID
	,ConsultantID
	,SpecialtyID
from
	WarehouseOLAPMergedV2.Theatre.BaseCancelledOperation

inner join WarehouseOLAPMergedV2.Theatre.BaseCancelledOperationReference
on BaseCancelledOperation.MergeCancelledOperationRecno = BaseCancelledOperationReference.MergeCancelledOperationRecno
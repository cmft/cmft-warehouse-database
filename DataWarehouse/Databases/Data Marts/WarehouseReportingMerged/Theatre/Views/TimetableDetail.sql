﻿
create view Theatre.TimetableDetail as

select
	BaseTimetableDetail.MergeRecno
	,TimetableDetailCode
	,SessionNumber
	,DayNumber
	,StartDate
	,EndDate
	,TheatreCode
	,SessionStartTime
	,SessionEndTime
	,ConsultantCode
	,AnaesthetistCode
	,SpecialtyCode
	,TimetableTemplateCode
	,LogLastUpdated
	,RecordLogDetails
	,SessionMinutes
	,ContextCode

	,ContextID
	,AnaesthetistID
	,ConsultantID
	,SpecialtyID
	,TheatreID

from
	WarehouseOLAPMergedV2.Theatre.BaseTimetableDetail

inner join WarehouseOLAPMergedV2.Theatre.BaseTimetableDetailReference
on	BaseTimetableDetail.MergeRecno = BaseTimetableDetailReference.MergeRecno
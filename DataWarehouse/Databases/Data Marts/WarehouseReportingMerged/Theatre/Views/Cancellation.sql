﻿
create view Theatre.Cancellation as 

select
	BaseCancellation.MergeRecno
	,SourceUniqueID
	,CancellationDate
	,CancelReasonCode
	,CancelReasonCode1
	,CancellationComment
	,OperationDetailSourceUniqueID
	,ConsultantCode
	,SpecialtyCode
	,SurgeonCode
	,SurgeonSpecialtyCode
	,ProposedOperationDate
	,DistrictNo
	,Forename
	,Surname
	,WardCode
	,WardCode1
	,PreMedGivenFlag
	,FastedFlag
	,LastUpdated
	,PatientClassificationCode
	,PatientSourceUniqueID
	,CampusCode
	,TheatreCode
	,TheatreCode1
	,CancellationSurgeonCode
	,InitiatorCode
	,PriorityCode
	,AdmissionDate
	,ContextCode

	,ContextID
    ,CancelReasonID
from
	WarehouseOLAPMergedV2.Theatre.BaseCancellation

inner join WarehouseOLAPMergedV2.Theatre.BaseCancellationReference
on	BaseCancellation.MergeRecno = BaseCancellationReference.MergeRecno
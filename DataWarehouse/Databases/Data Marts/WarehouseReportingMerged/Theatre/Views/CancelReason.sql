﻿
create view [Theatre].[CancelReason] as 

SELECT
	[SourceContextCode]
	,[SourceContext]
	,[SourceCancelReasonID]
	,[SourceCancelReasonCode]
	,[SourceCancelReason]
	,[LocalCancelReasonID]
	,[LocalCancelReasonCode]
	,[LocalCancelReason]
	,[NationalCancelReasonID]
	,[NationalCancelReasonCode]
	,[NationalCancelReason]
	,[CancelReasonGroupCode]
	,[CancelReasonGroup]
from
	[WarehouseOLAPMergedV2].[Theatre].[CancelReason]

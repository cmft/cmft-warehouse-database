﻿


CREATE view [APC].[SHMI]

as

select
	[MergeEncounterRecno]
	,[Survived]
	,[Died]
	,[DateOfDeath]
	,[DischargeDate]
	,[PatientAge]
	,[PatientAgeCategoryCode]
	,[AdmissionMethodCode]
	,[AdmissionMethodCategoryCode]
	,[SexCode]
	,[SexCategoryCode]
	,[PrimaryDiagnosisCode]
	,[DiagnosisGroupCode]
	,[DiagnosisGroupCategoryCode]
	,[CharlsonIndex]
	,[CharlsonIndexCategoryCode]
	,[YearIndexCode]
	,[RiskScore]
from
	[WarehouseOLAPMergedV2].[APC].[BaseSHMI]
where
	DominantForDiagnosis = 1






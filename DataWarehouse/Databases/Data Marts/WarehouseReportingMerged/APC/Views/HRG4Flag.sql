﻿create view [APC].[HRG4Flag]

as
SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[FlagCode]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[HRG4Flag]

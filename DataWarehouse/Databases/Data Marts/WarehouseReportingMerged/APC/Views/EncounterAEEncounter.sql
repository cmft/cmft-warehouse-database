﻿
create view [APC].[EncounterAEEncounter] as

/* 
==============================================================================================
Description:	

When		Who			What
17/03/2015	Paul Egan	Initial Coding
===============================================================================================
*/


select
	AEMergeEncounterRecno
	,APCMergeEncounterRecno
	,APCSourcePatientNo
	,APCSourceSpellNo = APCEpisodeNo
from
	WarehouseOLAPMergedV2.APC.BaseEncounterAEBaseEncounter
﻿

CREATE view [APC].[PatientClassification] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourcePatientClassificationID]
      ,[SourcePatientClassificationCode]
      ,[SourcePatientClassification]
      --,[LocalPatientClassificationID]
      ,[LocalPatientClassificationCode]
      ,[LocalPatientClassification]
      --,[NationalPatientClassificationID]
      ,[NationalPatientClassificationCode]
      ,[NationalPatientClassification]
  FROM [WarehouseOLAPMergedV2].[APC].[PatientClassification]



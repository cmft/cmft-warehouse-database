﻿
create view [APC].[DischargeMethod] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceDischargeMethodID]
      ,[SourceDischargeMethodCode]
      ,[SourceDischargeMethod]
      --,[LocalDischargeMethodID]
      ,[LocalDischargeMethodCode]
      ,[LocalDischargeMethod]
      --,[NationalDischargeMethodID]
      ,[NationalDischargeMethodCode]
      ,[NationalDischargeMethod]
  FROM [WarehouseOLAPMergedV2].[APC].[DischargeMethod]


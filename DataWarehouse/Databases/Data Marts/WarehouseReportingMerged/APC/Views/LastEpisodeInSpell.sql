﻿
create view [APC].[LastEpisodeInSpell] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceLastEpisodeInSpellID]
      ,[SourceLastEpisodeInSpellCode]
      ,[SourceLastEpisodeInSpell]
      --,[LocalLastEpisodeInSpellID]
      ,[LocalLastEpisodeInSpellCode]
      ,[LocalLastEpisodeInSpell]
      --,[NationalLastEpisodeInSpellID]
      ,[NationalLastEpisodeInSpellCode]
      ,[NationalLastEpisodeInSpell]
  FROM [WarehouseOLAPMergedV2].[APC].[LastEpisodeInSpell]


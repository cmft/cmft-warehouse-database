﻿


--use WarehouseReportingMerged

CREATE View [APC].[DuplicateEpisodeValidation] As
      
select 
       Validate.SourceUniqueID
      ,Encounter.ProviderSpellNo
      --,Encounter.FirstEpisodeInSpellIndicator
      ,Encounter.NHSNumber
      ,Encounter.AdmissionDate
      ,Encounter.AdmissionTime 
      ,Encounter.AdmissionMethodCode
      ,Encounter.StartSiteCode
      ,Encounter.SpecialtyCode
      ,Encounter.PatientCategoryCode
      ,Encounter.PrimaryDiagnosisCode
      ,Encounter.PrimaryProcedureCode
      ,Encounter.DateOnWaitingList
      ,Encounter.IntendedPrimaryProcedureCode
      --,Encounter.GlobalProviderSpellNo
      --,Encounter.GlobalEpisodeNo
      ,Validate.ValidationOutcome
      ,Outcome.ReportableDescription
from        
      WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.APC.DuplicateEpisodeValidation Validate
--on Encounter.MergeEncounterRecno = Validate.MergeEncounterRecno
on Encounter.SourceUniqueID = Validate.SourceUniqueID

inner join WarehouseOLAPMergedV2.[APC].[DuplicateEpisodeValidationOutcome] Outcome
on Validate.ValidationOutcome = Outcome.Reportable

--where ValidationOutcome = 3

--order by NHSNumber,AdmissionDate,ProviderSpellNo







﻿
create view [APC].[NeonatalLevelOfCare] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceNeonatalLevelOfCareID]
      ,[SourceNeonatalLevelOfCareCode]
      ,[SourceNeonatalLevelOfCare]
      --,[LocalNeonatalLevelOfCareID]
      ,[LocalNeonatalLevelOfCareCode]
      ,[LocalNeonatalLevelOfCare]
      --,[NationalNeonatalLevelOfCareID]
      ,[NationalNeonatalLevelOfCareCode]
      ,[NationalNeonatalLevelOfCare]
  FROM [WarehouseOLAPMergedV2].[APC].[NeonatalLevelOfCare]


﻿create view [APC].[PSSSpell]

as

SELECT [MergeEncounterRecno]
      ,[NationalProgrammeOfCareServiceCode]
      ,[ServiceLineCode]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[PSSSpell]

﻿
create view [APC].[HRG4Encounter]
as


SELECT [MergeEncounterRecno]
      ,[HRGCode]
      ,[GroupingMethodFlag]
      ,[DominantOperationCode]
      ,[PBCCode]
      ,[LOE]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[HRG4Encounter]

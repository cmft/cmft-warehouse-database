﻿
CREATE view [APC].[IntendedManagement] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceIntendedManagementID]
      ,[SourceIntendedManagementCode]
      ,[SourceIntendedManagement]
      --,[LocalIntendedManagementID]
      ,[LocalIntendedManagementCode]
      ,[LocalIntendedManagement]
      --,[NationalIntendedManagementID]
      ,[NationalIntendedManagementCode]
      ,[NationalIntendedManagement]
  FROM [WarehouseOLAPMergedV2].[APC].[IntendedManagement]


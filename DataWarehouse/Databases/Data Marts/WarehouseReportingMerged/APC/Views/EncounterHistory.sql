﻿

CREATE view [APC].[EncounterHistory] as

select 
	 BaseEncounter.MergeEncounterRecno
	,BaseEncounter.EncounterRecno
	,BaseEncounter.SourceUniqueID
	,BaseEncounter.SourcePatientNo
	,BaseEncounter.SourceSpellNo
	,BaseEncounter.SourceEncounterNo
	,BaseEncounter.ProviderSpellNo
	,BaseEncounter.GlobalProviderSpellNo
	,BaseEncounter.GlobalEpisodeNo
	,BaseEncounter.PatientTitle
	,BaseEncounter.PatientForename
	,BaseEncounter.PatientSurname
	,BaseEncounter.DateOfBirth
	,BaseEncounter.DateOfDeath
	,BaseEncounter.SexCode
	,BaseEncounter.NHSNumber
	,BaseEncounter.Postcode
	,BaseEncounter.PatientAddress1
	,BaseEncounter.PatientAddress2
	,BaseEncounter.PatientAddress3
	,BaseEncounter.PatientAddress4
	,BaseEncounter.DHACode
	,BaseEncounter.EthnicOriginCode
	,BaseEncounter.MaritalStatusCode
	,BaseEncounter.ReligionCode

	,DateOnWaitingList = Admission.DateOnWaitingList
	,AdmissionDate = Admission.AdmissionDate
	,DischargeDate = Discharge.DischargeDate

	,BaseEncounter.EpisodeStartDate
	,BaseEncounter.EpisodeEndDate

	,StartSiteCode = Admission.StartSiteCode

	,BaseEncounter.StartWardTypeCode

	,EndSiteCode = Discharge.EndSiteCode

	,BaseEncounter.EndWardTypeCode

	,BaseEncounter.RegisteredGpCode
	,BaseEncounter.RegisteredGpPracticeCode
	,BaseEncounter.SiteCode

	,AdmissionMethodCode = Admission.AdmissionMethodCode
	,AdmissionSourceCode = Admission.AdmissionSourceCode
	,PatientClassificationCode = Admission.PatientClassificationCode
	,ManagementIntentionCode = Admission.ManagementIntentionCode
	,DischargeMethodCode = Discharge.DischargeMethodCode
	,DischargeDestinationCode = Discharge.DischargeDestinationCode
	,AdminCategoryCode = Admission.AdminCategoryCode

	,BaseEncounter.ConsultantCode
	,BaseEncounter.SpecialtyCode

	,NationalLastEpisodeInSpellIndicator = 
		case
		when Discharge.DischargeDate is null
		then 9

		when BaseEncounter.MergeEncounterRecno = Discharge.MergeEncounterRecno
		then 1

		else 2
		end

	,FirstRegDayOrNightAdmit = Admission.FirstRegDayOrNightAdmit

	,BaseEncounter.NeonatalLevelOfCare
	,BaseEncounter.PASHRGCode
	,BaseEncounter.PrimaryDiagnosisCode
	,BaseEncounter.SubsidiaryDiagnosisCode
	,BaseEncounter.SecondaryDiagnosisCode1
	,BaseEncounter.SecondaryDiagnosisCode2
	,BaseEncounter.SecondaryDiagnosisCode3
	,BaseEncounter.SecondaryDiagnosisCode4
	,BaseEncounter.SecondaryDiagnosisCode5
	,BaseEncounter.SecondaryDiagnosisCode6
	,BaseEncounter.SecondaryDiagnosisCode7
	,BaseEncounter.SecondaryDiagnosisCode8
	,BaseEncounter.SecondaryDiagnosisCode9
	,BaseEncounter.SecondaryDiagnosisCode10
	,BaseEncounter.SecondaryDiagnosisCode11
	,BaseEncounter.SecondaryDiagnosisCode12
	,BaseEncounter.SecondaryDiagnosisCode13
	,BaseEncounter.PrimaryProcedureCode
	,BaseEncounter.PrimaryProcedureDate
	,BaseEncounter.SecondaryProcedureCode1
	,BaseEncounter.SecondaryProcedureDate1
	,BaseEncounter.SecondaryProcedureCode2
	,BaseEncounter.SecondaryProcedureDate2
	,BaseEncounter.SecondaryProcedureCode3
	,BaseEncounter.SecondaryProcedureDate3
	,BaseEncounter.SecondaryProcedureCode4
	,BaseEncounter.SecondaryProcedureDate4
	,BaseEncounter.SecondaryProcedureCode5
	,BaseEncounter.SecondaryProcedureDate5
	,BaseEncounter.SecondaryProcedureCode6
	,BaseEncounter.SecondaryProcedureDate6
	,BaseEncounter.SecondaryProcedureCode7
	,BaseEncounter.SecondaryProcedureDate7
	,BaseEncounter.SecondaryProcedureCode8
	,BaseEncounter.SecondaryProcedureDate8
	,BaseEncounter.SecondaryProcedureCode9
	,BaseEncounter.SecondaryProcedureDate9
	,BaseEncounter.SecondaryProcedureCode10
	,BaseEncounter.SecondaryProcedureDate10
	,BaseEncounter.SecondaryProcedureCode11
	,BaseEncounter.SecondaryProcedureDate11
	,BaseEncounter.OperationStatusCode
	,BaseEncounter.ContractSerialNo
	,BaseEncounter.CodingCompleteDate
	,BaseEncounter.PurchaserCode
	,BaseEncounter.ProviderCode
	,BaseEncounter.EpisodeStartTime
	,BaseEncounter.EpisodeEndTime
	,BaseEncounter.RegisteredGdpCode
	,BaseEncounter.EpisodicGpCode
	,BaseEncounter.InterfaceCode
	,BaseEncounter.CasenoteNumber
	,BaseEncounter.NHSNumberStatusCode

	,AdmissionTime = Admission.AdmissionTime
	,DischargeTime = Discharge.DischargeTime
	,TransferFrom = Admission.TransferFrom

	,BaseEncounter.DistrictNo
	,BaseEncounter.ExpectedLOS
	,BaseEncounter.MRSAFlag
	,BaseEncounter.RTTPathwayID
	,BaseEncounter.RTTPathwayCondition
	,BaseEncounter.RTTStartDate
	,BaseEncounter.RTTEndDate
	,BaseEncounter.RTTSpecialtyCode
	,BaseEncounter.RTTCurrentProviderCode
	,BaseEncounter.RTTCurrentStatusCode
	,BaseEncounter.RTTCurrentStatusDate
	,BaseEncounter.RTTCurrentPrivatePatientFlag
	,BaseEncounter.RTTOverseasStatusFlag
	,BaseEncounter.RTTPeriodStatusCode
	,BaseEncounter.IntendedPrimaryProcedureCode
	,BaseEncounter.Operation
	,BaseEncounter.Research1
	,BaseEncounter.CancelledElectiveAdmissionFlag
	,BaseEncounter.LocalAdminCategoryCode
	,BaseEncounter.EpisodicGpPracticeCode
	,BaseEncounter.PCTCode
	,BaseEncounter.LocalityCode
	,BaseEncounter.AgeCode
	,BaseEncounter.HRGCode
	,BaseEncounter.SpellHRGCode
	,BaseEncounter.CategoryCode
	,BaseEncounter.LOE

	,LOS =
		DATEDIFF(
			 DAY
			,Admission.AdmissionDate
			,Discharge.DischargeDate
		)

	,BaseEncounter.Cases

	,FirstEpisodeInSpellIndicator = 
		cast(
			case
			when BaseEncounter.GlobalEpisodeNo = 1
			then 1
			else 0
			end

			as bit
		)
	,BaseEncounter.RTTActivity
	,BaseEncounter.RTTBreachStatusCode
	,BaseEncounter.DiagnosticProcedure
	,BaseEncounter.RTTTreated
	,BaseEncounter.EpisodeStartTimeOfDay
	,BaseEncounter.EpisodeEndTimeOfDay

	,AdmissionTimeOfDay = Admission.AdmissionTimeOfDay
	,DischargeTimeOfDay = Discharge.DischargeTimeOfDay

	,BaseEncounter.StartDirectorateCode
	,BaseEncounter.EndDirectorateCode
	,BaseEncounter.ResidencePCTCode

	,ISTAdmissionSpecialtyCode = Admission.ISTAdmissionSpecialtyCode
	,ISTAdmissionDemandTime = Admission.ISTAdmissionDemandTime
	,ISTDischargeTime = Discharge.ISTDischargeTime
	,ISTAdmissionDemandTimeOfDay = Admission.ISTAdmissionDemandTimeOfDay
	,ISTDischargeTimeOfDay = Discharge.ISTDischargeTimeOfDay

	,BaseEncounter.PatientCategoryCode
	,BaseEncounter.Research2
	,BaseEncounter.ClinicalCodingStatus
	,BaseEncounter.ClinicalCodingCompleteDate
	,BaseEncounter.ClinicalCodingCompleteTime
	,BaseEncounter.ReferredByCode
	,BaseEncounter.ReferrerCode
	,BaseEncounter.DecidedToAdmitDate
	,BaseEncounter.PsychiatricPatientStatusCode
	,BaseEncounter.LegalStatusClassificationCode
	,BaseEncounter.ReferringConsultantCode
	,BaseEncounter.DurationOfElectiveWait
	,BaseEncounter.CarerSupportIndicator
	,BaseEncounter.DischargeReadyDate
	,BaseEncounter.ContextCode
	,BaseEncounter.EddCreatedTime
	,BaseEncounter.ExpectedDateofDischarge
	,BaseEncounter.EddCreatedByConsultantFlag
	,BaseEncounter.EddInterfaceCode
	,BaseEncounter.VTECategoryCode
	,BaseEncounter.Ambulatory
	,BaseEncounter.Created
	,BaseEncounter.Updated
	,BaseEncounter.ByWhom
	,BaseEncounter.PseudoPostcode
	--,BaseEncounter.CCGCode
	,CCGCode = Discharge.CCGCode
	,BaseEncounter.EpisodicPostCode
	,Discharge.GpCodeAtDischarge
	,Discharge.GpPracticeCodeAtDischarge
	,Discharge.PostcodeAtDischarge
	,BaseEncounter.ResidenceCCGCode

	,BaseEncounterReference.ContextID
	,AdmissionReference.AdmissionMethodID
	,AdmissionReference.AdmissionSourceID
	,DischargeReference.DischargeDestinationID
	,DischargeReference.DischargeMethodID
	,AdmissionReference.IntendedManagementID
	,AdmissionReference.ISTAdmissionSpecialtyID
	,BaseEncounterReference.NeonatalLevelOfCareID
	,AdmissionReference.PatientClassificationID
	,BaseEncounterReference.VTECategoryID
	,BaseEncounterReference.StartWardID
	,BaseEncounterReference.AgeID
	,AdmissionReference.AdmissionDateID
	,DischargeReference.DischargeDateID
	,BaseEncounterReference.EpisodeStartDateID
	,BaseEncounterReference.EpisodeEndDateID
	,BaseEncounterReference.ConsultantID
	,BaseEncounterReference.SexID
	,AdmissionReference.StartSiteID
	,DischargeReference.EndSiteID
	,BaseEncounterReference.SpecialtyID
	,BaseEncounterReference.NHSNumberStatusID
	,BaseEncounterReference.OperationStatusID
	,BaseEncounterReference.EthnicOriginID
	,BaseEncounterReference.MaritalStatusID
	,BaseEncounterReference.SiteID
	,AdmissionReference.AdminCategoryID
	,AdmissionReference.FirstRegDayOrNightAdmitID
	,BaseEncounterReference.RTTCurrentStatusID
	,BaseEncounterReference.RTTPeriodStatusID
	,BaseEncounterReference.ReferrerID
	,BaseEncounterReference.ReferringConsultantID
	,BaseEncounterReference.CarerSupportIndicatorID
	,BaseEncounterReference.EndWardID
	,BaseEncounterReference.ResidenceCCGID

from
	WarehouseOLAPMergedV2.APC.BaseEncounterHistory BaseEncounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReferenceHistory BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
and BaseEncounter.Reportable = 1

inner join WarehouseOLAPMergedV2.APC.BaseEncounterHistory Admission
on	Admission.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReferenceHistory AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterHistory Discharge
on	Discharge.GlobalProviderSpellNo = BaseEncounter.GlobalProviderSpellNo
and Discharge.EpisodeEndTime = discharge.DischargeTime

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReferencehistory DischargeReference
on	DischargeReference.MergeEncounterRecno = Discharge.MergeEncounterRecno








﻿



CREATE View [APC].[DementiaCurrent]

as

select
	GlobalProviderSpellNo
	,Division	
	,AdmissionType	
	,AdmissionDate	
	,AdmissionTime	
	,AdmissionWardCode	
	,DischargeDate	
	,DischargeTime	
	,LocalDischargeMethod	
	,DateOfDeath	
	,PatientSurname	
	,SexCode	
	,NHSNumber	
	,DistrictNo	
	,CasenoteNumber	
	,LatestWard	
	,LatestSpecialtyCode	
	,LatestConsultantCode	
	,AgeOnAdmission	
	,AgeCategory	
	,LoS	
	,OriginalSubmissionTime	
	,OriginalResponse	
	,LatestKnownDementiaRecordedTime	
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse	
	,Assessment =
		case 
			when LatestAssessmentTime is not null then 1 
			else 0
		end
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,AssessmentScore = 
		case	
			when LatestAssessmentScore between 0 and 7 then 1
			when LatestAssessmentScore > 7 then 0
			else null
		end
	,Investigation =
		case
			when LatestInvestigationTime is not null then 1
			else 0
		end
	,LatestInvestigationTime	
	,LatestReferralSentDate	
	,Referral =
		case
			when LatestReferralRecordedTime is not null then 1
			else 0
		end
	,LatestReferralRecordedTime	
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime	
	,CurrentInpatient	
	,CQUIN
	,DementiaUpdated
	,WardUpdated

from
	WarehouseOLAPMergedV2.APC.DementiaCurrent



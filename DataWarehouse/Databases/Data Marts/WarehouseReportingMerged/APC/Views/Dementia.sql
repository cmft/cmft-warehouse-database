﻿



--use WarehouseReportingMerged

CREATE View [APC].[Dementia]

as

select  
	MergeDementiaRecno	
	,DementiaRecno	
	,GlobalProviderSpellNo
	,ProviderSpellNo	
	,SpellID	
	,SourceUniqueID	
	,SourcePatientNo	
	,SourceSpellNo	
	,AdmissionWardCode	
	,OriginalSubmissionTime 
	,OriginalResponse
	,LatestKnownDementiaRecordedTime	
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate	
	,LatestReferralRecordedTime	
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime	
	,InterfaceCode	
	,ContextCode	
	,Created	
	,Updated	
	,ByWhom
from 
	WarehouseOLAPMergedV2.APC.BaseDementia Dementia




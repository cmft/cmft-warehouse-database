﻿
create view [APC].[HRG4CriticalCareEncounter]

as

SELECT [MergeEncounterRecno]
      ,[HRGCode]
      ,[CriticalCareDays]
      ,[CriticalCareWarningFlag]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[HRG4CriticalCareEncounter]

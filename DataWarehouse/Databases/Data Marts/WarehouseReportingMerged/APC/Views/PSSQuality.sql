﻿create view [APC].[PSSQuality]

as

SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[QualityTypeCode]
      ,[QualityCode]
      ,[QualityMessage]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[PSSQuality]

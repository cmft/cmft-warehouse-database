﻿

	
CREATE FUNCTION [APC].[f_GetWardEncounterRecNo]
(
	 @ProviderSpellNo	VARCHAR(20)
	,@WardTime			Datetime
)
RETURNS Bigint 
AS  
BEGIN
DECLARE @EncounterRecNo Bigint = Null

SELECT @EncounterRecNo			= WS.MergeEncounterRecno
FROM APC.WardStay WS
INNER JOIN APC.Ward
	ON WS.WardID = ward.SourceWardID
WHERE @WardTime BETWEEN WS.StartTime AND ISNULL(WS.EndTime,GETDATE())
AND @ProviderSpellNo = WS.ProviderSpellNo

if @EncounterRecNo is null
begin
SELECT TOP 1 @EncounterRecNo			= WS.MergeEncounterRecno
FROM APC.WardStay WS
INNER JOIN APC.Ward
	ON WS.WardID = ward.SourceWardID
WHERE @WardTime BETWEEN WS.StartDate AND ISNULL(WS.EndTime,GETDATE())
AND @ProviderSpellNo = WS.ProviderSpellNo

end

RETURN @EncounterRecNo
END

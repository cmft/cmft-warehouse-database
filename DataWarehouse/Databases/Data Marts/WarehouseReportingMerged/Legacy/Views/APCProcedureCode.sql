﻿



create view [Legacy].[APCProcedureCode] as

SELECT 
	 EncounterRecno
	--,SourceEncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,Sequence
	,ProcedureCode
	--,[ContextID]
FROM 
	(SELECT 
	 EncounterRecno
	--,SourceEncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PrimaryProcedureCode as [1]
	,SecondaryProcedureCode1 as [2]
	,SecondaryProcedureCode2 as [3]
	,SecondaryProcedureCode3 as [4]
	,SecondaryProcedureCode4 as [5]
	,SecondaryProcedureCode5 as [6]
	,SecondaryProcedureCode6 as [7]
	,SecondaryProcedureCode7 as [8]
	,SecondaryProcedureCode8 as [9]
	,SecondaryProcedureCode9 as [10]
	,SecondaryProcedureCode10 as [11]
	,SecondaryProcedureCode11 as [12]
	--,[ContextID]
	FROM WarehouseOLAPMergedV2.APC.BaseEncounter) p
	UNPIVOT
	(ProcedureCode FOR Sequence IN ([1]
	,[2]
	,[3]
	,[4]
	,[5]
	,[6]
	,[7]
	,[8]
	,[9]
	,[10]
	,[11]
	,[12])) as unpvt;












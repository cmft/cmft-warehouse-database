﻿CREATE TABLE [Legacy].[APCReadmission] (
    [EncounterRecno]        INT           NULL,
    [InternalNo]            VARCHAR (20)  NULL,
    [EpisodeNo]             VARCHAR (9)   NULL,
    [CaseNote]              VARCHAR (16)  NULL,
    [DistrictNo]            VARCHAR (50)  NULL,
    [SourceEncounterRecno]  INT           NULL,
    [AdmitDate]             DATETIME2 (7) NULL,
    [DischDate]             DATETIME2 (7) NULL,
    [PrimaryDiag]           VARCHAR (8)   NULL,
    [counter]               BIGINT        NULL,
    [counter2]              BIGINT        NULL,
    [days]                  BIGINT        NULL,
    [re_admit]              INT           NULL,
    [read_admit_date]       DATETIME      NULL,
    [read_casenote]         VARCHAR (16)  NULL,
    [read_EncounterRecNo]   INT           NULL,
    [exclusions_numerator]  VARCHAR (50)  NULL,
    [exclusion_denominator] VARCHAR (50)  NULL,
    [Location]              VARCHAR (1)   NULL,
    [CommunityContact]      BIT           NULL
);


GO
CREATE CLUSTERED INDEX [IX_Readmissions_3]
    ON [Legacy].[APCReadmission]([EncounterRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Readmission]
    ON [Legacy].[APCReadmission]([EncounterRecno] ASC, [re_admit] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Readmissions]
    ON [Legacy].[APCReadmission]([InternalNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Readmissions_1]
    ON [Legacy].[APCReadmission]([EpisodeNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Readmissions_2]
    ON [Legacy].[APCReadmission]([CaseNote] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Readmissions_4]
    ON [Legacy].[APCReadmission]([DistrictNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Readmissions_5]
    ON [Legacy].[APCReadmission]([SourceEncounterRecno] ASC);


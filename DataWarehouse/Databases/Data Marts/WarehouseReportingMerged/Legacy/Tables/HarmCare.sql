﻿CREATE TABLE [Legacy].[HarmCare] (
    [TheDate]            DATE          NULL,
    [WeekNo]             VARCHAR (50)  NULL,
    [TheMonth]           NVARCHAR (34) NULL,
    [FinancialQuarter]   NVARCHAR (70) NULL,
    [SpellID]            INT           NULL,
    [Patient]            VARCHAR (8)   NULL,
    [EncounterRecNo]     BIGINT        NULL,
    [WardEncounterRecNo] BIGINT        NULL,
    [IPEpisode]          VARCHAR (5)   NULL,
    [AdmitDate]          DATETIME      NULL,
    [Division]           VARCHAR (50)  NULL,
    [SourceWardCode]     VARCHAR (100) NULL,
    [Fall]               INT           NULL,
    [PUHFC1]             INT           NULL,
    [PUHFC2]             INT           NULL,
    [UTI]                INT           NULL,
    [VTETreatment]       INT           NULL,
    [PatientCategory]    VARCHAR (2)   NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_HarmCare]
    ON [Legacy].[HarmCare]([Patient] ASC, [TheDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_HarmCare_1]
    ON [Legacy].[HarmCare]([Patient] ASC, [WeekNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_HarmCare_2]
    ON [Legacy].[HarmCare]([Patient] ASC, [TheMonth] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_HarmCare_3]
    ON [Legacy].[HarmCare]([EncounterRecNo] ASC);


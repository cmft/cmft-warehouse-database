﻿CREATE TABLE [Legacy].[SpecComIndicatorParent] (
    [IndicatorParentID]          INT          NOT NULL,
    [IndicatorParentDescription] VARCHAR (50) NULL,
    [SortOrder]                  INT          NULL,
    [IsActive]                   INT          NULL
);


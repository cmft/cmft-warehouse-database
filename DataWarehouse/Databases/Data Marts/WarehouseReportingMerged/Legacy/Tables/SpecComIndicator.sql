﻿CREATE TABLE [Legacy].[SpecComIndicator] (
    [IndicatorID]            INT           NOT NULL,
    [IndicatorParentID]      INT           NOT NULL,
    [MeasureNumber]          VARCHAR (20)  NOT NULL,
    [Name]                   VARCHAR (255) NOT NULL,
    [Description]            VARCHAR (MAX) NULL,
    [KPIName]                VARCHAR (MAX) NULL,
    [NumeratorDescription]   VARCHAR (MAX) NULL,
    [DenominatorDescription] VARCHAR (MAX) NULL,
    [Construction]           VARCHAR (MAX) NULL,
    [PeriodTypeID]           INT           NOT NULL,
    [ConstructionID]         INT           NOT NULL,
    [SortOrder]              INT           NULL
);


﻿


-- =============================================
-- Author:		Derlwyn Jones
-- Create date: 6 Jan 2013
-- Description:	ETL for Specialised Commissioning
-- =============================================
CREATE PROCEDURE [Legacy].[ETLLoadSpecialisedCommissioning]
(@StartDate as datetime,
@EndDate as datetime)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--declare @StartDate datetime
	--declare @EndDate datetime
	
	--set @StartDate = '01 Jul 2012'
	--set @EndDate = '30 Sep 2012'
	
	DELETE FROM [Legacy].[SpecComFact]
	WHERE EncounterDate = @StartDate
	
	
	----------------------------
	----  	Insert Manual Data
	----------------------------	
	
	
  	INSERT INTO [Legacy].[SpecComFact]
			   (IndicatorID
			   ,EncounterDate
			   ,Cases
			   ,Numerator
			   ,Denominator)
	SELECT IndicatorID
		  ,PeriodStartDate
		  ,Cases
		  ,Numerator
		  ,Denominator
	  FROM [Legacy].[SpecComManualData]
	  WHERE PeriodStartDate = @StartDate
	
	
	
	
	
	
	
	----------------------------
	----  	Cardiology
	----------------------------
	
	--------------
	----  	CDY01
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 1, 
	@StartDate, 
	count(distinct eps.[1#02 Patient Case Record Number]), 
	SUM(DATEDIFF(day,admissiontime,[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve])), 
	count(distinct eps.[1#02 Patient Case Record Number])
	 FROM EPS
	 LEFT JOIN apc.Encounter
		ON eps.[1#02 Patient Case Record Number] = Encounter.CasenoteNumber
		AND [3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] between AdmissionTime and COALESCE(dischargetime, GETDATE())
	WHERE eps.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] is not null
	AND eps.[3#02 Procedure Urgency] is not null
	AND eps.[3#09 Ablation procedure(s)] is not null
	AND Encounter.FirstEpisodeInSpellIndicator = 1
	AND [3#02 Procedure Urgency] <> '1. Elective'
	AND [3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] BETWEEN @StartDate AND @EndDate


	--------------
	----  	CDY02
	--------------


	--------------
	----  	CDY03
	--------------

	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 3, 
	@StartDate, 
	COUNT(DISTINCT pmicd.[1#02 Patient Case Record Number]), 
	COUNT(DISTINCT CasenoteNumber),
	COUNT(DISTINCT pmicd.[1#02 Patient Case Record Number])
	FROM [Legacy].[PMICD]
	LEFT JOIN apc.Encounter
		ON Encounter.CasenoteNumber = pmicd.[1#02 Patient Case Record Number]
		AND DateOfDeath IS NOT NULL
	WHERE PMICD.[3#02 Intervention] LIKE '%generator%' and PMICD.[3#37 ICD function] like '%Yes%'	
	AND [3#01 Date/Time arrival at hospital (for MINAP) or Procedure Date] BETWEEN @StartDate AND @EndDate
  	AND EXISTS (SELECT E2.ProviderSpellNo FROM apc.Encounter E2 
				INNER JOIN Legacy.APCProcedureCode ProcedureCode
					ON e2.ProviderSpellNo = ProcedureCode.ProviderSpellNo
				WHERE E2.CasenoteNumber = pmicd.[1#02 Patient Case Record Number]
				AND e2.FirstEpisodeInSpellIndicator = 1
				GROUP BY E2.ProviderSpellNo
				HAVING COUNT(*) < 2)

	  
	--------------
	----  	CDY04
	--------------

	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 4, 
	@StartDate, 
	COUNT(DISTINCT pmicd.[1#02 Patient Case Record Number]), 
	COUNT(DISTINCT enc2.CasenoteNumber),
	COUNT(distinct pmicd.[1#02 Patient Case Record Number])
	FROM [Legacy].[PMICD]
	LEFT JOIN apc.Encounter
		ON Encounter.CasenoteNumber = pmicd.[1#02 Patient Case Record Number]
	LEFT JOIN apc.Encounter Enc2
		ON Encounter.SourcePatientNo = enc2.SourcePatientNo
		AND enc2.DateOfDeath IS NOT NULL 
		AND datediff(YEAR,PMICD.[3#01 Date/Time arrival at hospital (for MINAP) or Procedure Date],enc2.DateOfDeath) < 1
	WHERE PMICD.[3#02 Intervention] LIKE '%generator%' and PMICD.[3#37 ICD function] like '%Yes%'	
	AND [3#01 Date/Time arrival at hospital (for MINAP) or Procedure Date] BETWEEN @StartDate AND @EndDate
  	AND EXISTS (SELECT E2.ProviderSpellNo FROM apc.Encounter E2 
				INNER JOIN Legacy.APCProcedureCode ProcedureCode
					ON e2.ProviderSpellNo = ProcedureCode.ProviderSpellNo
				WHERE E2.CasenoteNumber = pmicd.[1#02 Patient Case Record Number]
				AND e2.FirstEpisodeInSpellIndicator = 1
				GROUP BY E2.ProviderSpellNo
				HAVING COUNT(*) < 2)
				


	--------------
	----  	CDY08
	--------------

	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 8, 
	@StartDate, 
	count(distinct eps.[1#02 Patient Case Record Number]), 
	COUNT(distinct eps_redo.[1#02 Patient Case Record Number]),
	count(distinct eps.[1#02 Patient Case Record Number])
	FROM EPS
	LEFT JOIN WarehouseReportingMerged.apc.Encounter
		ON eps.[1#02 Patient Case Record Number] = Encounter.CasenoteNumber
		AND [3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] between AdmissionTime and COALESCE(dischargetime, GETDATE())
	LEFT JOIN EPS EPS_REDO	
		ON EPS.[1#03 NHS Number] = EPS_REDO.[1#03 NHS Number]
		AND EPS.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] <
				EPS_REDO.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve]	
		AND DATEDIFF(YEAR,EPS.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve],
				EPS_REDO.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve]) < 1
		AND EPS_REDO.[3#09 Ablation procedure(s)] is not null					
	WHERE eps.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] is not null
	AND eps.[3#09 Ablation procedure(s)] is not null
	AND Encounter.FirstEpisodeInSpellIndicator = 1
	AND EPS.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] BETWEEN DATEADD(YEAR,-1,@StartDate) AND DATEADD(YEAR,-1,@EndDate)


	--------------
	----  	CDY09
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 9, 
	@StartDate, 
	count(distinct eps.[1#02 Patient Case Record Number]), 
	SUM(DATEDIFF(day,admissiontime,[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve])), 
	count(distinct eps.[1#02 Patient Case Record Number])
	FROM EPS
	LEFT JOIN apc.Encounter
		ON eps.[1#02 Patient Case Record Number] = Encounter.CasenoteNumber
		AND [3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] between AdmissionTime and COALESCE(dischargetime, GETDATE())
	WHERE eps.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] is not null
	AND eps.[3#09 Ablation procedure(s)] is not null
	AND Encounter.FirstEpisodeInSpellIndicator = 1
	AND [3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] BETWEEN @StartDate AND @EndDate


	--------------
	----  	CDY12
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 11, 
	@StartDate, 
	count(distinct eps.[1#02 Patient Case Record Number]), 
	COUNT(distinct readenc.SourcePatientNo), 
	count(distinct eps.[1#02 Patient Case Record Number])
	 FROM EPS
	 LEFT JOIN apc.Encounter
		ON eps.[1#02 Patient Case Record Number] = Encounter.CasenoteNumber
		AND [3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] between AdmissionTime and COALESCE(dischargetime, GETDATE())
	 LEFT JOIN (SELECT SourcePatientNo, AdmissionTime
				FROM WarehouseReportingMerged.apc.Encounter ReadEnc
				INNER JOIN APC.AdmissionMethod
					ON ReadEnc.AdmissionMethodID = SourceAdmissionMethodID
					AND AdmissionTypeCode IN ('EM','OTH')
				INNER JOIN WH.Specialty 
					ON ReadEnc.SpecialtyID = Specialty.SourceSpecialtyID
					AND NationalSpecialtyCode IN ('172','320')) ReadEnc
		ON Encounter.SourcePatientNo = ReadEnc.SourcePatientNo
		AND DATEDIFF(DAY,Encounter.DischargeTime, ReadEnc.AdmissionTime) <= 30
	WHERE eps.[3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] is not null
	AND eps.[3#09 Ablation procedure(s)] is not null
	AND Encounter.FirstEpisodeInSpellIndicator = 1
	AND [3#01 Date/Time arrival at hospital (for MINAP) or Initiating Eve] BETWEEN @StartDate AND  @EndDate


	----------------------------
	----  	Cystic Fibrosis
	----------------------------
	
	--------------
	----  	CFS04
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 18, @StartDate, count(distinct Encounter.CasenoteNumber) 
	--(SELECT count(distinct hs.HospitalNo)
	--							FROM bedman.dbo.hospspell hs
	--							INNER JOIN bedman.dbo.SpellAudit sa ON hs.SpellID = sa.SpellID
	--							INNER JOIN bedman.dbo.Facilities f ON f.FacilityID = sa.FacilityID
	--							INNER JOIN bedman.dbo.Patients p ON hs.Patient = p.Patient
	--							AND hospitalno IN (SELECT distinct Encounter.CasenoteNumber
	--											FROM APC.Encounter
	--											INNER JOIN APC.Diagnosis
	--												ON Encounter.EncounterRecno = Diagnosis.APCEncounterRecno
	--											WHERE Encounter.AdmissionTime BETWEEN @StartDate AND @EndDate
	--											AND Diagnosis.DiagnosisCode IN ('E84.0','E84.1','E84.8','E84.9')
	--												)
	--							AND 
	--							(
	--							(sa.CurrWard = '85' AND 
	--							f.Description IN ('BED 1','BED 2','BED 3','BED 4','BED 5','BED 5','BED 6','BED 6','BED 7','BED 8','BED 9'
	--							,'BED 18','BED 19','BED 20','BED 21','BED 26','BED 27','BED 28'))
	--							OR (sa.CurrWard = '75' AND 
	--							f.Description IN ('BED 16','BED 17','BED 20','BED 21','BED 24','BED 25'
	--							,'BED 13','BED 14','BED 15','BED 18','BED 19','BED 22','BED 23','BED 26','BED 27'))
	--							OR (sa.CurrWard = '76C' AND 
	--							f.Description IN ('BED 9','BED 10','BED 22','BED 23'))
	--							)
	--							)
			, count(distinct Encounter.CasenoteNumber)	
			, count(distinct Encounter.CasenoteNumber)
				FROM APC.Encounter
				INNER JOIN APC.Diagnosis
					ON Encounter.MergeEncounterRecno = Diagnosis.MergeEncounterRecno
				WHERE Encounter.AdmissionTime BETWEEN @StartDate AND @EndDate
				AND Diagnosis.DiagnosisCode IN ('E84.0','E84.1','E84.8','E84.9')
				AND dbo.f_GetAge(Encounter.DateOfBirth, Encounter.AdmissionTime) < 18


	--------------
	----  	CFS05
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 19, @StartDate, count(distinct Encounter.CasenoteNumber), 
	(SELECT count(distinct Encounter.CasenoteNumber)
		FROM APC.Encounter
		INNER JOIN APC.Diagnosis
			ON Encounter.MergeEncounterRecno = Diagnosis.MergeEncounterRecno
		WHERE Encounter.AdmissionTime BETWEEN @StartDate AND @EndDate
		AND Diagnosis.DiagnosisCode IN ('E84.0','E84.1','E84.8','E84.9')
			AND EXISTS (SELECT 1 FROM APC.WardStay
			INNER JOIN APC.Ward
			ON WardStay.WardID = Ward.SourceWardID
			WHERE WardStay.ProviderSpellNo = Encounter.ProviderSpellNo
			AND (SourceWardCode LIKE '75%' OR SourceWardCode LIKE '76%' OR SourceWardCode LIKE '85%'))), 
	 count(distinct Encounter.CasenoteNumber)
		FROM APC.Encounter
		INNER JOIN APC.Diagnosis
			ON Encounter.MergeEncounterRecno = Diagnosis.MergeEncounterRecno
		WHERE Encounter.AdmissionTime BETWEEN @StartDate AND @EndDate
		AND Diagnosis.DiagnosisCode IN ('E84.0','E84.1','E84.8','E84.9')
				AND dbo.f_GetAge(Encounter.DateOfBirth, Encounter.AdmissionTime) < 18


	--------------
	----  	CFS09
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 20, @StartDate, count(distinct Encounter.NHSNumber), count(distinct Encounter.NHSNumber), count(distinct Encounter.NHSNumber)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		WHERE 
		SourceClinicCode in ('CMONPA', 'CMONNON', 'CCFAR', 'CTUENON', 'CTUEPA', 'CFRINON', 'CFRIPA','CNEWCF','CCFWALK')
		AND Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
			
	
	--------------
	----  	CFS10
	--------------

	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 21, @StartDate, count(distinct Encounter.NHSNumber), count(distinct Encounter.NHSNumber), count(distinct Encounter.NHSNumber)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		WHERE 
		SourceClinicCode in ('CMONPA', 'CMONNON', 'CCFAR', 'CTUENON', 'CTUEPA', 'CFRINON', 'CFRIPA','CNEWCF','CCFWALK')
		AND Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate




	----------------------------
	----  	Renal Dialysis
	----------------------------

	
	
	---------------
	----  	DIAL_06
	---------------	
	
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	14, 
	@StartDate, 
	COUNT(distinct patientMedicalRecordNo)-5, 
	Null, 
	COUNT(distinct patientMedicalRecordNo)-5 
	  FROM [ClinicalVisionReportingViews].[dbo].[PatientRenalModality]
	  LEFT JOIN APC.Encounter
		ON PatientMedicalRecordNo COLLATE Latin1_General_CI_AS = CasenoteNumber
		AND StopDate_Local BETWEEN AdmissionTime AND COALESCE(DischargeTime, GETDATE())
	  WHERE Modality = 'HD' AND ModalitySetting <> 'Acute'
	  AND (StopDate_Local BETWEEN @StartDate AND @EndDate
	  OR StopDate_Local IS NULL)	
	
		
	---------------
	----  	DIAL_07
	---------------	
	
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	15, 
	@StartDate, 
	COUNT(distinct patientMedicalRecordNo), 
	5, 
	COUNT(distinct patientMedicalRecordNo) 
	  FROM [ClinicalVisionReportingViews].[dbo].[PatientRenalModality]
	  LEFT JOIN APC.Encounter
		ON PatientMedicalRecordNo COLLATE Latin1_General_CI_AS = CasenoteNumber
		AND StopDate_Local BETWEEN AdmissionTime AND COALESCE(DischargeTime, GETDATE())
	  WHERE Modality = 'HD' AND ModalitySetting <> 'Acute'
	  AND (StopDate_Local BETWEEN @StartDate AND @EndDate
	  OR StopDate_Local IS NULL)
	

	---------------
	----  	DIAL_08
	---------------
		
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	16, 
	@StartDate, 
	COUNT(distinct patientMedicalRecordNo), 
	13, 
	COUNT(distinct patientMedicalRecordNo) 
	  FROM [ClinicalVisionReportingViews].[dbo].[PatientRenalModality]
	  LEFT JOIN APC.Encounter
		ON PatientMedicalRecordNo COLLATE Latin1_General_CI_AS = CasenoteNumber
		AND StopDate_Local BETWEEN AdmissionTime AND COALESCE(DischargeTime, GETDATE())
	  WHERE Modality = 'HD' AND ModalitySetting <> 'Acute'
	  AND (StopDate_Local BETWEEN @StartDate AND @EndDate
	  OR StopDate_Local IS NULL)



	



	----------------------------
	----  	NICU
	----------------------------


	---------------
	----  	NIC02
	---------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	select 
	142
	,@StartDate
	,Discharges = SUM(1)		
	,ROPDone = SUM(
				case
				when Rectime is not null then 1
				else 0
				end
				)
	,Discharges = SUM(1)	

	From
		NeonatalDetailed_stg.dbo.episodes
		
		inner join
		NeonatalDetailed_stg.dbo.babies
		On 
		NeonatalDetailed_stg.dbo.episodes.BabyNHSNumber
		= NeonatalDetailed_stg.dbo.babies.BabyNHSNumber
		
	
		left join
			(
			select
					BabyNHSNumber
					,episode
					,Min(rectime) As Rectime

			from 
					 NeonatalDetailed_stg.dbo.rop
					
			group By

					 BabyNHSNumber
					,episode
			 ) ROP
			 
		on ROP.BabyNHSNumber 
		= NeonatalDetailed_stg.dbo.episodes.BabyNHSNumber
		and
		ROP.episode 
		= NeonatalDetailed_stg.dbo.episodes.Episode
		
	
	where 
		DischargeTime BETWEEN @StartDate AND @EndDate
		and (
			 BirthWeight < 1501 
			or GAWeeks < 32
			)
		and DischargeDestination in ('1','2','4')
		

	---------------
	----  	NIC13
	---------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	select 
	145, 
	@StartDate, 
	Activity = Sum(1),
	Below_36 =	Sum(
						Case 
						When AdmissionTemp < 20.0 Then 0
						When AdmissionTemp < 36.0 Then 1 
						Else 0
						End)
		
	,Activity = Sum(1)
	
	from 

			NeonatalDetailed_stg.dbo.episodes
			
			inner join
			
			NeonatalDetailed_stg.dbo.babies
			On 
			NeonatalDetailed_stg.dbo.babies.BabyNHSNumber
			=
			NeonatalDetailed_stg.dbo.episodes.BabyNHSNumber
			
			
			left join
				(Select 

						BabyNHSNumber
						,Episode
						,Cooledtoday
						,MIN(Singledaydate) as Singledaydate
			
						from NeonatalDetailed_stg.dbo.dailyData
	  
						where Cooledtoday is not null
	  
						group by
	  
						 BabyNHSNumber
						,Episode
						,Cooledtoday) as Cool
						
			on Cool.BabyNHSNumber 
			= NeonatalDetailed_stg.dbo.episodes.BabyNHSNumber
			and Cool.episode 
			= NeonatalDetailed_stg.dbo.episodes.Episode
			
			
	where 

			AdmissionTime BETWEEN @StartDate AND @EndDate
			and CooledToday is null








	----------------------------
	----  	TARN
	----------------------------
	
	---------------
	----  	MTC01
	---------------

	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	267,
	@StartDate,
	(SELECT count(DISTINCT TC.DistrictNumber)
	FROM MAJORTAM.dbo.TARNCandidate TC
	WHERE DateSubmitted BETWEEN @StartDate AND @EndDate),
	(SELECT count(DISTINCT TC.DistrictNumber)
	FROM MAJORTAM.dbo.TARNCandidate TC
	WHERE DateSubmitted BETWEEN @StartDate AND @EndDate
	AND DATEDIFF(DAY,TC.DateDeparture,TC.DateSubmitted) <=40),
	(SELECT count(DISTINCT TC.DistrictNumber)
	FROM MAJORTAM.dbo.TARNCandidate TC
	WHERE DateSubmitted BETWEEN @StartDate AND @EndDate)



	---------------
	----  	MTC02
	---------------

	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	268,
	@StartDate,
	count(DISTINCT DistrictNo),
	count(DISTINCT DistrictNo),
	count(DISTINCT DistrictNo)
	FROM WarehouseReporting.TARN.Encounter E
	INNER JOIN WarehouseReporting.WH.Calendar C ON COALESCE(arrivaldate,datecreated) = TheDate
	WHERE Thedate BETWEEN @StartDate AND @EndDate
	AND E.MajorTraumaTriage = 'Positive'





	---------------
	----  	MTC03
	---------------

	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	269,
	@StartDate,
	count(DISTINCT DistrictNo),
	count(DISTINCT DistrictNo),
	count(DISTINCT DistrictNo)
	FROM WarehouseReporting.TARN.Encounter E
	INNER JOIN WarehouseReporting.WH.Calendar C ON COALESCE(arrivaldate,datecreated) = TheDate
	WHERE Thedate BETWEEN @StartDate AND @EndDate
	AND E.MajorTraumaTriage = 'Positive'

	



	
	---------------
	----  	MTC05
	---------------
	
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	271,
	@StartDate,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Cases,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL
	AND TimeDiff <= 30) Numerator,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Denominator	





	---------------
	----  	MTC06
	---------------
	
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	272,
	@StartDate,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Cases,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND MajorTraumaTriage = 'Positive'
	AND TimeDiff BETWEEN 31 AND 60) Numerator,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Denominator	




	---------------
	----  	MTC07
	---------------
	
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	273,
	@StartDate,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Cases,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL
	AND TimeDiff > 60) Numerator,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Denominator	



	---------------
	----  	MTC08
	---------------
	
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 
	274,
	@StartDate,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Cases,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Numerator,
	(SELECT COUNT(DistrictNo)
	FROM [Legacy].[TARNFirstCTScan]
	WHERE convert(date,ArrivalTime) BETWEEN @StartDate AND @EndDate
	AND PreviousHospital IS NULL) Denominator	



	-------------------------
	----  	Medical Genetics
	-------------------------

	--------------
	----  	GEN02
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 41, @StartDate, count(distinct Clinic.SourceClinicCode), 0, count(distinct Clinic.SourceClinicCode)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		INNER JOIN WH.Specialty
			ON Encounter.SpecialtyID = Specialty.SourceSpecialtyID			
		WHERE Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
		AND Specialty.NationalSpecialtyCode = '311'


	--------------
	----  	GEN06
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 46, @StartDate, count(distinct Encounter.SourcePatientNo), 0, count(distinct Encounter.SourcePatientNo)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		INNER JOIN WH.Specialty
			ON Encounter.SpecialtyID = Specialty.SourceSpecialtyID	
		INNER JOIN WarehouseReportingMerged.OP.AttendanceStatus
			ON AttendanceStatus.SourceAttendanceStatusID = Encounter.AttendanceStatusID	
		WHERE Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
		AND Specialty.NationalSpecialtyCode = '311'
		AND AttendanceStatus.SourceAttendanceStatusCode = '5'


	--------------
	----  	GEN07
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 47, @StartDate, count(distinct Encounter.SourcePatientNo), 0, count(distinct Encounter.SourcePatientNo)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		INNER JOIN WH.Specialty
			ON Encounter.SpecialtyID = Specialty.SourceSpecialtyID	
		INNER JOIN WarehouseReportingMerged.OP.AttendanceStatus
			ON AttendanceStatus.SourceAttendanceStatusID = Encounter.AttendanceStatusID	
		WHERE Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
		AND Specialty.NationalSpecialtyCode = '311'
		AND AttendanceStatus.SourceAttendanceStatusCode = '5'


	--------------
	----  	GEN09
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 49, @StartDate, 
	(SELECT count(distinct Encounter.SourcePatientNo)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		INNER JOIN WH.Specialty
			ON Encounter.SpecialtyID = Specialty.SourceSpecialtyID	
		INNER JOIN WarehouseReportingMerged.OP.AttendanceStatus
			ON AttendanceStatus.SourceAttendanceStatusID = Encounter.AttendanceStatusID	
		WHERE Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
		AND Specialty.NationalSpecialtyCode = '311'
		AND AttendanceStatus.SourceAttendanceStatusCode = '5'),
	(SELECT count(distinct Encounter.SourcePatientNo)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		INNER JOIN WH.Specialty
			ON Encounter.SpecialtyID = Specialty.SourceSpecialtyID	
		INNER JOIN WarehouseReportingMerged.OP.AttendanceStatus
			ON AttendanceStatus.SourceAttendanceStatusID = Encounter.AttendanceStatusID	
		WHERE Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
		AND Specialty.NationalSpecialtyCode = '311'
		AND AttendanceStatus.SourceAttendanceStatusCode = '3'),
	(SELECT count(distinct Encounter.SourcePatientNo)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		INNER JOIN WH.Specialty
			ON Encounter.SpecialtyID = Specialty.SourceSpecialtyID	
		INNER JOIN WarehouseReportingMerged.OP.AttendanceStatus
			ON AttendanceStatus.SourceAttendanceStatusID = Encounter.AttendanceStatusID	
		WHERE Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
		AND Specialty.NationalSpecialtyCode = '311'
		AND AttendanceStatus.SourceAttendanceStatusCode = '5')		


	--------------
	----  	GEN06
	--------------
	INSERT INTO [Legacy].[SpecComFact]
			   ([IndicatorID]
			   ,[EncounterDate]
			   ,[Cases]
			   ,[Numerator]
			   ,[Denominator])
	SELECT 50, @StartDate, count(distinct Encounter.SourcePatientNo), 0, count(distinct Encounter.SourcePatientNo)
		FROM OP.Encounter
		INNER JOIN OP.Clinic
			ON Encounter.ClinicID = Clinic.SourceClinicID
		INNER JOIN WH.Specialty
			ON Encounter.SpecialtyID = Specialty.SourceSpecialtyID	
		INNER JOIN WarehouseReportingMerged.OP.AttendanceStatus
			ON AttendanceStatus.SourceAttendanceStatusID = Encounter.AttendanceStatusID	
		WHERE Encounter.AppointmentTime BETWEEN @StartDate AND @EndDate
		AND Specialty.NationalSpecialtyCode = '311'
		AND AttendanceStatus.SourceAttendanceStatusCode = '5'

	




	----------------------------
	----  	Haem
	----------------------------




	---------------
	----  	PICU
	---------------


	
	---------------
	----  	PET-CT
	---------------	

	
	
	---------------
	----  	Burns
	---------------
		
		
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
		
END




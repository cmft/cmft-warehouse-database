﻿

 



create PROCEDURE Legacy.ETLLoadReadmissions

AS

declare @StartDate datetime
declare @EndDate datetime

set @StartDate = '1/1/2007'
set @EndDate = getdate()


TRUNCATE TABLE Legacy.APCReadmission

-------------------------------------------------------------------
---- INSERT ALL ADMISSIONS SO WE CAN SELECT THE READMISSIONS ------
---- EXCLUDING SPECIALTIES AS BELOW                          ------
-------------------------------------------------------------------

INSERT
INTO 
	Legacy.APCReadmission
	(
	 EncounterRecno
	,DistrictNo
	,[CaseNote]
	,[AdmitDate]
	,[DischDate]
	,[PrimaryDiag]
	,Location
)
SELECT DISTINCT
	 AD.MergeEncounterRecno
	,AD.DistrictNo
	,AD.CaseNoteNumber
	,AD.AdmissionTime
	,AD.DischargeTime
	,AD.PrimaryDiagnosisCode
	,'M'
FROM
	APC.Encounter AD

LEFT JOIN WH.Specialty S 
ON AD.SpecialtyID = S.SourceSpecialtyID

LEFT JOIN APC.LastEpisodeInSpell LAST 
ON lastepisodeinspellid = SourceLastEpisodeInSpellID

WHERE
	AD.PatientCategoryCode<>'RD' -- DG Changed from DC 15/04/2012 at request of Dusia
AND	(
		AD.DischargeTime>=cast('01 jan 2007' as datetime)
	Or	AD.DischargeTime Is Null
) 
AND --(
	(
		NationalSpecialtyCode Not Between '700%' And '716%' 
	And NationalSpecialtyCode Not Like '501%' 
	And NationalSpecialtyCode Not Like '560%' 
	And NationalSpecialtyCode Not Like '610%'
	)
and (
		NationalLastEpisodeInSpellCode = 1
	OR	AD.DischargeTime Is Null
	)



-------------------------------------------------------------------
---- CREATE CANCER TABLE FOR CANCER EXCLUSIONS               ------
-------------------------------------------------------------------

SELECT AdmissionTime, 
EncounterRecno = Encounter.MergeEncounterRecno,
LastEpisodeInSpellID,
EpisodeStartTime, 
PrimaryDiagnosisCode

INTO #T002_cancer_spells
FROM APC.Encounter Encounter

WHERE AdmissionTime>=@StartDate
AND (PrimaryDiagnosisCode Between 'C00%' And 'C98%'
OR PrimaryDiagnosisCode Between 'D37%' And 'D48%'
OR PrimaryDiagnosisCode ='Z51.1');



UPDATE Legacy.APCReadmission 
SET exclusions_numerator = 'CAN', exclusion_denominator = 'CAN'
FROM Legacy.APCReadmission RE
INNER JOIN #T002_cancer_spells CAN ON RE.EncounterRecno = CAN.EncounterRecno 
WHERE RE.exclusions_numerator Is Null 
AND RE.exclusion_denominator Is Null 
AND CAN.AdmissionTime>=dateadd(d,-365,RE.AdmitDate);




-------------------------------------------------------------------
---- EXLUDE ALL READMISSIONS WITH PRIMARY DIAG STARTING 'O'  ------
-------------------------------------------------------------------


DROP TABLE #T002_cancer_spells

UPDATE Legacy.APCReadmission
SET exclusions_numerator = 'O', exclusion_denominator = 'O'
FROM Legacy.APCReadmission T001_all_admissions
WHERE T001_all_admissions.exclusions_numerator Is Null 
AND T001_all_admissions.exclusion_denominator Is Null 
AND T001_all_admissions.PrimaryDiag Like 'O%';




-------------------------------------------------------------------
---- SET READMISSIONS										 ------
-------------------------------------------------------------------


UPDATE Legacy.APCReadmission
SET counter = internalnorank
FROM Legacy.APCReadmission A1
INNER JOIN (
select districtno, admitdate, Rank() over (Partition BY districtno order by admitdate) as internalnorank
from
Legacy.APCReadmission) A_RANK ON A1.districtno = A_RANK.districtno AND A1.admitdate = A_RANK.admitdate

UPDATE Legacy.APCReadmission
SET counter2 = counter -1

UPDATE T001_all_admissions_1
SET days = DateDiff("d",T001_all_admissions_1.DischDate,T001_all_admissions.AdmitDate), 
re_admit = 1,
read_admit_date = T001_all_admissions.AdmitDate, 
read_casenote = T001_all_admissions.casenote,
read_encounterrecno = T001_all_admissions.encounterrecno
FROM Legacy.APCReadmission T001_all_admissions_1
INNER JOIN Legacy.APCReadmission T001_all_admissions ON (T001_all_admissions_1.districtno = T001_all_admissions.districtno) 
AND (T001_all_admissions_1.counter = T001_all_admissions.counter2) 

WHERE (
((DateDiff(d,T001_all_admissions_1.DischDate,T001_all_admissions.AdmitDate))<300) 
AND ((T001_all_admissions_1.DischDate)<T001_all_admissions.AdmitDate));






-------------------------------------------------------------------
---- SET COMMUNITY CONTACT FLAG FROM IPM     				 ------
-------------------------------------------------------------------

UPDATE Legacy.APCReadmission
SET CommunityContact = 1 
FROM Legacy.APCReadmission R
INNER JOIN APC.Encounter EN
	ON R.EncounterRecno = EN.MergeEncounterRecno
INNER JOIN (select NHS_IDENTIFIER, START_DTTM

from ipm.dbo.Schedule s     
                                     
left join ipm.dbo.Patient p                                             
	on s.patnt_refno = p.patnt_refno                                              
                                                
left join ipm.dbo.Referral r                                            
	on s.refrl_refno = r.refrl_refno

where s.SCTYP_REFNO = 1468

  AND
        s.SCOCM_REFNO NOT IN (
              1459,                                           -- Patient Died
              2001651,                                        -- Cancelled
              1457                                            -- Did Not Attend
        )
  AND
        s.ARRIVED_DTTM IS NOT NULL
  AND
        s.DEPARTED_DTTM IS NOT NULL
) COMM 
	ON EN.NHSNumber = COMM.NHS_IDENTIFIER 
	AND EN.DischargeTime >= COMM.START_DTTM

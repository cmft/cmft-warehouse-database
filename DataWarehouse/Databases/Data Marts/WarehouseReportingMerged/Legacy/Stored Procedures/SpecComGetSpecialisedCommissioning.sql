﻿

CREATE PROCEDURE [Legacy].[SpecComGetSpecialisedCommissioning]
(@StartDate as datetime,
@EndDate as datetime)

AS
set nocount on;

SELECT
MeasureNumber							= MeasureNumber,
IndicatorParentDescription				= IndicatorParentDescription,
IndicatorName							= Name,
IndicatorDescription					= Description,
KPIName									= KPIName,
NumeratorDescription					= NumeratorDescription,
DenominatorDescription					= DenominatorDescription,
PeriodDescription						= PeriodType.PeriodDescription,
ConstructionDescription					= Construction.ConstructionDescription,
IndicatorSortOrder						= Indicator.SortOrder,
ParentSortOrder							= IndicatorParent.SortOrder,
Numerator								= Numerator,
Denominator								= Denominator,
Construction							= Construction

INTO #TempSpecCom
FROM [Legacy].[SpecComIndicator] Indicator
INNER JOIN [Legacy].[SpecComIndicatorParent] IndicatorParent
	ON Indicator.IndicatorParentID = IndicatorParent.IndicatorParentID
INNER JOIN [Legacy].[SpecComPeriodType] PeriodType
	ON Indicator.PeriodTypeID = PeriodType.PeriodTypeID
INNER JOIN [Legacy].[SpecComConstruction] Construction
	ON Indicator.ConstructionID = Construction.ConstructionID
LEFT JOIN [Legacy].[SpecComFact] Fact
	ON Indicator.IndicatorID = Fact.IndicatorID
WHERE COALESCE(EncounterDate,@StartDate) BETWEEN @StartDate AND @EndDate
AND ISNULL(IsActive,0) = 1


INSERT INTO #TempSpecCom
SELECT
MeasureNumber							= MeasureNumber,
IndicatorParentDescription				= IndicatorParentDescription,
IndicatorName							= Name,
IndicatorDescription					= Description,
KPIName									= KPIName,
NumeratorDescription					= NumeratorDescription,
DenominatorDescription					= DenominatorDescription,
PeriodDescription						= PeriodType.PeriodDescription,
ConstructionDescription					= Construction.ConstructionDescription,
IndicatorSortOrder						= Indicator.SortOrder,
ParentSortOrder							= IndicatorParent.SortOrder,
Numerator								= Null,
Denominator								= Null,
Construction							= Construction

FROM [Legacy].[SpecComIndicator] Indicator
INNER JOIN [Legacy].[SpecComIndicatorParent] IndicatorParent
	ON Indicator.IndicatorParentID = IndicatorParent.IndicatorParentID
INNER JOIN [Legacy].[SpecComPeriodType] PeriodType
	ON Indicator.PeriodTypeID = PeriodType.PeriodTypeID
INNER JOIN [Legacy].[SpecComConstruction] Construction
	ON Indicator.ConstructionID = Construction.ConstructionID
WHERE MeasureNumber NOT IN (SELECT MeasureNumber FROM #TempSpecCom)
AND ISNULL(IsActive,0) = 1

SELECT * FROM #TempSpecCom

DROP TABLE #TempSpecCom

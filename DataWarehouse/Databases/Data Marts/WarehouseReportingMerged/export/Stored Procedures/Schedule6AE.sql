﻿

CREATE proc [export].[Schedule6AE]

as

 


declare @start datetime
                                ,@end datetime,
                                @start2 datetime


set @start = dateadd(dd, datediff(dd, 0, dateadd(dd, - 1, getdate())), 0)
set @start2 = dateadd(dd, datediff(dd, 0, dateadd(dd, - 1, getdate()-7)), 0)
set @end = dateadd(mi, -1, dateadd(dd, 1, @start))


--set @start2 = '04 May 2015'
--set @end = '05 May 2015'


                                
select
       [Provider Code] =    
              encounter.SiteCode
       ,[Provider Local ID] = 
              AttendanceNumber
       ,[NHS Number] = 
              NHSNumber
       ,[Date Of Birth] = 
              DateOfBirth
       ,[GP Practice Code] = 
              GpPracticeCodeAtAttendance
       ,[Arrival Date] = 
              ArrivalTimeAdjusted
       ,[Arrival Time] = 
              ArrivalTimeAdjusted
       ,[Source Referral] = 
              SourceOfReferral.NationalSourceOfReferralCode
       ,[Disposal Method] = 
              AttendanceDisposal.NationalAttendanceDisposalCode
       ,[Presenting Complaint] = 
              encounter.PresentingProblem
       ,[Initial Triage Cat] = 
              TriageCategory.NationalTriageCategoryCode
       ,[Departure Date] = 
              DepartureTimeAdjusted
       ,[Departure Time]= 
              DepartureTimeAdjusted
       ,[Commissioner Code] = 
              case
              when contextcode = 'CEN||ADAS'    then '00W' --needs fixing at source!!
              else CCGCode
              end
       ,[WalkInType] = 
              case
              when ContextCode ='CEN||ADAS' then 1
              else 0
              end

from 
       AE.Encounter
left outer join ae.SourceOfReferral on
       SourceOfReferral.SourceSourceOfReferralID = encounter.SourceOfReferralID

left outer join ae.AttendanceDisposal 
on AttendanceDisposal.SourceAttendanceDisposalID = encounter.AttendanceDisposalID

left outer join ae.TriageCategory
on TriageCategory.SourceTriageCategoryID = encounter.TriageCategoryID

where 
       ArrivalTimeAdjusted >= @start2 --based on arrival date
       and ArrivalTimeAdjusted <  @end
       and 
       (
              CCGCode in ('00W','01N','01M') --North, Central and south CCGs only
              or
              contextcode = 'CEN||ADAS'
       )
       and Reportable = 1
       and AttendanceCategoryCode <> 2 --exclude clinic attenders
       and contextcode <> 'TRA||ADAS' --exclude Trafford WIC





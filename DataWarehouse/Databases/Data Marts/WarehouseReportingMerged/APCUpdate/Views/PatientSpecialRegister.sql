﻿create view APCUpdate.PatientSpecialRegister as


SELECT [PMISPECIALREGID]
      ,[ActivityDateTime]
      ,EnteredDate = [DateEntered]
      ,[EpisodeNumber]
      ,SourcePatientno = [InternalPatientNumber]
      ,SpecialRegisterCode = [SpecialRegister]
      ,[SpecialRegisterDescription]
      ,[created]
      ,[updated]
      ,[byWhom]
  FROM [PAS].[InquireUpdate].[PMISPECIALREG]

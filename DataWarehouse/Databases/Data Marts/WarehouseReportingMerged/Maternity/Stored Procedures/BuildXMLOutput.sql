﻿
CREATE proc [Maternity].[BuildXMLOutput]  

as

SET NOCOUNT on

declare
	 @FromDate date
	,@ToDate date
	,@RecordCount int
	,@RunDateTime datetime
	,@SubmissionPeriod varchar (50)
	,@XMLNode1 varchar (max)
	,@XMLNode2 varchar (max)
	,@XMLNode3 varchar (max)
	,@XMLNode4 varchar (max)	
	,@XMLNodeMother varchar (max)	
	,@XMLNodebaby varchar (max)		
	,@XMLNode6 varchar (max)
	,@LocalPatientId varchar (50) = null
	
	set @LocalPatientId = null
 
 set @SubmissionPeriod = (select max(SubmissionPeriod) from WarehouseReportingMerged.Maternity.MAT001MothersDemographics)
 set @RunDateTime = left(getdate (),19)
 
 if exists
	(
	select
		1
	from
		Maternity.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	and	FreezeDate > getdate()
	)
begin
	(
	select
		@FromDate = SubmissionPeriod.FromDate
		,@ToDate = SubmissionPeriod.ToDate
	from
		Maternity.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	)		
end

else

begin
	print 'Invalid submission period'
	return
end



select

	LocalPatientIdMother

	into #SentRecords

from Maternity.SubmittedReocrds

where SubMitted = 1
 and
--Debug by LocalPatientId
(

@LocalPatientId is null
or
@LocalPatientId = LocalPatientIdMother

)


	set @RecordCount = 
	(
		select 

		sum(Total)+1 total

		from
		(

		select count(*) Total from Maternity.MAT001MothersDemographics Encounter where exists (Select 1 from #SentRecords where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother)
		union all
		select count(*) Total from Maternity.MAT003GPPracticeRegistration Encounter where exists (Select 1 from #SentRecords where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother)
		union all
		select count(*) Total from Maternity.MAT101BookingAppointmentDetails Encounter where exists (Select 1 from #SentRecords where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother)
		union all
		select count(*) Total from Maternity.MAT310AntenatalAdmission Encounter where exists (Select 1 from #SentRecords where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother)
		union all
		select count(*) Total from Maternity.MAT404LabourAndDelivery Encounter where exists (Select 1 from #SentRecords where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother)
		union all
		select count(*) Total from Maternity.MAT306AntenatalAppointment Encounter where exists (Select 1 from #SentRecords where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother)
		union all
		select count(*) Total 
		from Maternity.MAT502BabysDemographicsandBirthDetails Encounter 
		where 
		exists 
			(
			Select 1 
			from #SentRecords 
			where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother
			)
		and
			not exists
			(
			select 
				NHSNumberBaby 
			from Maternity.MAT502BabysDemographicsandBirthDetails  DemographicsandBirthDetailsCount
			where DemographicsandBirthDetailsCount.NHSNumberBaby = Encounter.NHSNumberBaby
			group by NHSNumberBaby
			having count (*) > 1
			) 

		) Data
	)




--Create XML Header
	set @XMLNode1 =  '<?xml version="1.0" encoding="UTF-8" ?>' 
	
	set @XMLNode2 = '<MSDS:MSDS xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:MSDS="http://www.datadictionary.nhs.uk/messages/MSDS-v1-0">'

	set @XMLNode3 = '<MATHDRHeader>'

	set @XMLNode4 =  
		 
		
		cast( 
				(
				
				select
					
				 Version = '1.5'
				,OrgCodeProv = 'RW3'
				,OrgCodeSubmitter = 'RW3'
				,RPStartDate = @FromDate
				,RPEndDate = @ToDate
				,FileCreationDateTime = @RunDateTime
				,RecordCount = @RecordCount
				
				for xml PATH(''), TYPE
				
				) as Varchar (max)
			) 
	 

--Create main xml content Mother

Select  

XMLOutPut =
	cast(
			(
			select

				(
					Select

					 LocalPatientIdMother 
					,OrgCodeLocalPatientIdMother = coalesce(OrgCodeLocalPatientIdMother,'')
					,OrgCodeRes = coalesce(OrgCodeRes,'')
					,NHSNumberMother = coalesce(NHSNumberMother,'')
					,NHSNumberStatusMother = coalesce(NHSNumberStatusMother,'')
					,PersonBirthDateMother = coalesce(PersonBirthDateMother,'')
					,Postcode = coalesce(Postcode,'')
					,EthnicCategoryMother = coalesce(EthnicCategoryMother,'')
					,PersonDeathDateTimeMother = coalesce(PersonDeathDateTimeMother,'')


					FROM WarehouseReportingMerged.Maternity.MAT001MothersDemographics MAT001MothersDemographics

					where

					MAT001MothersDemographics. LocalPatientIdMother = Encounter.LocalPatientIdMother

					for xml PATH(''), TYPE

				)
				,
				(
					Select


					 LocalPatientIdMother 
					,OrgCodeGMPMother = coalesce(OrgCodeGMPMother,'')
					,StartDateGMPRegistration = coalesce(StartDateGMPRegistration,'')
					,EndDateGMPRegistration = coalesce(EndDateGMPRegistration,'')
					,OrgCodeCommissioner = coalesce(OrgCodeCommissioner,'')



					from WarehouseReportingMerged.Maternity.MAT003GPPracticeRegistration MAT003GPPracticeRegistration

					where  MAT003GPPracticeRegistration.LocalPatientIdMother = Encounter.LocalPatientIdMother


					for xml PATH('MAT003GPPracticeRegistration'), TYPE

				)
				,
				(
					Select


					 AntenatalAppDate = coalesce(AntenatalAppDate,'')
					,LocalPatientIdMother 
					,EDDAgreed = coalesce(EDDAgreed,'')
					,EDDMethodAgreed = coalesce(EDDMethodAgreed,'')
					,PregnancyFirstContactDate = coalesce(PregnancyFirstContactDate,'')
					,PregnancyFirstContactCareProfessionalType = coalesce(PregnancyFirstContactCareProfessionalType,'')
					,LastMenstrualPeriodDate = coalesce(LastMenstrualPeriodDate,'')
					,PhysicalDisabilityStatusIndMother = coalesce(PhysicalDisabilityStatusIndMother,'')
					,FirstLanguageEnglishIndMother = coalesce(FirstLanguageEnglishIndMother,'')
					,EmploymentStatusMother = coalesce(EmploymentStatusMother,'')
					,SupportStatusMother = coalesce(SupportStatusMother,'')
					,EmploymentStatusPartner = coalesce(EmploymentStatusPartner,'')
					,PreviousCaesareanSections = coalesce(PreviousCaesareanSections,'')
					,PreviousLiveBirths = coalesce(PreviousLiveBirths,'')
					,PreviousStillBirths = coalesce(PreviousStillBirths,'')
					,PreviousLossesLessThan24Weeks = coalesce(PreviousLossesLessThan24Weeks,'')
					,SubstanceUseStatus = coalesce(SubstanceUseStatus,'')
					,SmokingStatus = coalesce(SmokingStatus,'')
					,CigarettesPerDay = coalesce(CigarettesPerDay,'')
					,AlcoholUnitsPerWeek = coalesce(AlcoholUnitsPerWeek,'')
					,FolicAcidSupplement = coalesce(FolicAcidSupplement,'')
					,MHPredictionDetectionIndMother = coalesce(MHPredictionDetectionIndMother,'')
					,PersonWeight = coalesce(PersonWeight,'')
					,PersonHeight = coalesce(PersonHeight,'')
					,ComplexSocialFactorsInd = coalesce(ComplexSocialFactorsInd,'')


					from WarehouseReportingMerged.Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails

					where  MAT101BookingAppointmentDetails.LocalPatientIdMother = Encounter.LocalPatientIdMother


					for xml PATH('MAT101BookingAppointmentDetails'), TYPE

				)
				,
				(
					Select

					 LocalPatientIdMother
					,AntenatalAppDate 			



					from WarehouseReportingMerged.Maternity.MAT306AntenatalAppointment MAT306AntenatalAppointment

					where  MAT306AntenatalAppointment.LocalPatientIdMother = Encounter.LocalPatientIdMother


					for xml PATH('MAT306AntenatalAppointment'), TYPE

				)
				,
				(
					Select

					LocalPatientIdMother
					,HPSAntenatalStartDate
					,HPSAntenatalDischargeDate = coalesce(HPSAntenatalDischargeDate	,'')



					from WarehouseReportingMerged.Maternity.MAT310AntenatalAdmission MAT310AntenatalAdmission

					where  MAT310AntenatalAdmission.LocalPatientIdMother = Encounter.LocalPatientIdMother


					for xml PATH('MAT310AntenatalAdmission'), TYPE

				)
				,
				(
					Select

					LabourOnsetDateTime = coalesce(LabourOnsetDateTime,'')
					,CaesareanDateTime = coalesce(CaesareanDateTime,'')
					,LocalPatientIdMother = coalesce(LocalPatientIdMother,'')
					,LabourOnsetPresentation = coalesce(LabourOnsetPresentation,'')
					,StartDateTimeMotherDeliveryHPS = coalesce(StartDateTimeMotherDeliveryHPS,'')
					,DecisionToDeliverDateTime = coalesce(DecisionToDeliverDateTime,'')
					,ROMDateTime = coalesce(ROMDateTime,'')
					,ROMMethod = coalesce(ROMMethod,'')
					,ROMReason = coalesce(ROMReason,'')
					,LabourOnsetSecondStageDateTime = coalesce(LabourOnsetSecondStageDateTime,'')
					,LabourThirdStageEndDateTime = coalesce(LabourThirdStageEndDateTime,'')
					,EpisiotomyReason = coalesce(EpisiotomyReason,'')
					,PlacentaDeliveryMethod = coalesce(PlacentaDeliveryMethod,'')
					,DischargeDateTimeMotherDeliveryHPS = coalesce(DischargeDateTimeMotherDeliveryHPS,'')
					,OrgCodePostnatalPathLeadProvider = coalesce(OrgCodePostnatalPathLeadProvider,'')


					from WarehouseReportingMerged.Maternity.MAT404LabourAndDelivery MAT404LabourAndDelivery

					where  MAT404LabourAndDelivery.LocalPatientIdMother = Encounter.LocalPatientIdMother


					for xml PATH('MAT404LabourAndDelivery'), TYPE

				)
				
				,		(
			select	
			   LocalPatientIdBaby
			  ,LocalPatientIdMother
			  ,OrgCodeLocalPatientIdBaby 
			  ,BabyBirthDateTime = coalesce(BabyBirthDateTime,'')
			  ,NHSNumberBaby = coalesce(NHSNumberBaby,'')
			  ,NHSNumberStatusBaby = coalesce(NHSNumberStatusBaby,'')
			  ,PersonPhenotypicSex = coalesce(PersonPhenotypicSex,'')
			  ,PersonDeathDateTimeBaby = coalesce(PersonDeathDateTimeBaby,'')
			  ,BirthOrderMaternitySUS = coalesce(BirthOrderMaternitySUS,'')
			  ,BirthWeight = coalesce(BirthWeight,'')
			  ,GestationLengthBirth = coalesce(GestationLengthBirth,'')
			  ,DeliveryMethodBaby = coalesce(DeliveryMethodBaby,'')
			  ,WaterDeliveryInd = coalesce(WaterDeliveryInd,'')
			  ,ApgarScore5 = coalesce(ApgarScore5,'')
			  ,SiteCodeActualDelivery = coalesce(SiteCodeActualDelivery,'')
			  ,PlaceTypeActualDelivery = coalesce(PlaceTypeActualDelivery,'')
			  ,PlaceTypeActualMidwifery = coalesce(PlaceTypeActualMidwifery,'')
			  ,BabyFirstFeedDateTime = coalesce(BabyFirstFeedDateTime,'')
			  ,BabyFirstFeedBreastMilkStatus = coalesce(BabyFirstFeedBreastMilkStatus,'')
			  ,BabyBreastMilkStatusDischarge = coalesce(BabyBreastMilkStatusDischarge,'')
			  ,SkinToSkinContact1Hour = coalesce(SkinToSkinContact1Hour,'')
		     
			--) as xml
		
		  
		  FROM WarehouseReportingMerged.Maternity.MAT502BabysDemographicsandBirthDetails MAT502
		  where MAT502.LocalPatientIdMother = Encounter.LocalPatientIdMother
		  and
		  not exists
			(
			select NHSNumberBaby from Maternity.MAT502BabysDemographicsandBirthDetails  DemographicsandBirthDetailsCount
			where DemographicsandBirthDetailsCount.NHSNumberBaby = MAT502.NHSNumberBaby
			group by NHSNumberBaby
			having count (*) > 1
			) 

		  
		   for xml PATH('MAT502BabysDemographicsAndBirthDetails'), TYPE

		)

			For XML path ('MAT001MothersDemographics')--, Root ('MATHDRHeader')


			) 
	as varchar (max)
	)

	into #XMLOut

	from WarehouseReportingMerged.Maternity.MAT001MothersDemographics Encounter

where
--Remove Records with Duplicate NHS Number as the whole submission would be rejected

exists (Select 1 from #SentRecords where #SentRecords.LocalPatientIdMother = Encounter.LocalPatientIdMother)

--not exists
--(
--select 

--NHSNumberMother

--from WarehouseReportingMerged.Maternity.MAT001MothersDemographics Duplicate

--where 
--Duplicate.NHSNumberMother is not null
--and
--Duplicate.NHSNumberMother = Encounter.NHSNumberMother
--group by Duplicate.NHSNumberMother
--having count (*) > 1

--)


SET @XMLNodeMother = STUFF
      (

            (

        SELECT ''  +  XMLOutPut from #XMLOut 
        
        
        FOR XML PATH ('') , root('MyString'), type 
     ).value('/MyString[1]','varchar(max)') 
   , 1, 0, '')

----Baby's XML

--SELECT


--XMLOutPut =
--cast(
--		(
--			select	
--			   LocalPatientIdBaby
--			  ,LocalPatientIdMother
--			  ,OrgCodeLocalPatientIdBaby 
--			  ,BabyBirthDateTime = coalesce(BabyBirthDateTime,'')
--			  ,NHSNumberBaby = coalesce(NHSNumberBaby,'')
--			  ,NHSNumberStatusBaby = coalesce(NHSNumberStatusBaby,'')
--			  ,PersonPhenotypicSex = coalesce(PersonPhenotypicSex,'')
--			  ,PersonDeathDateTimeBaby = coalesce(PersonDeathDateTimeBaby,'')
--			  ,BirthOrderMaternitySUS = coalesce(BirthOrderMaternitySUS,'')
--			  ,BirthWeight = coalesce(BirthWeight,'')
--			  ,GestationLengthBirth = coalesce(GestationLengthBirth,'')
--			  ,DeliveryMethodBaby = coalesce(DeliveryMethodBaby,'')
--			  ,WaterDeliveryInd = coalesce(WaterDeliveryInd,'')
--			  ,ApgarScore5 = coalesce(ApgarScore5,'')
--			  ,SiteCodeActualDelivery = coalesce(SiteCodeActualDelivery,'')
--			  ,PlaceTypeActualDelivery = coalesce(PlaceTypeActualDelivery,'')
--			  ,PlaceTypeActualMidwifery = coalesce(PlaceTypeActualMidwifery,'')
--			  ,BabyFirstFeedDateTime = coalesce(BabyFirstFeedDateTime,'')
--			  ,BabyFirstFeedBreastMilkStatus = coalesce(BabyFirstFeedBreastMilkStatus,'')
--			  ,BabyBreastMilkStatusDischarge = coalesce(BabyBreastMilkStatusDischarge,'')
--			  ,SkinToSkinContact1Hour = coalesce(SkinToSkinContact1Hour,'')
		     
--			--) as xml
		
		  
--		  FROM WarehouseReportingMerged.Maternity.MAT502BabysDemographicsandBirthDetails MAT502
--		  where MAT502.LocalPatientIdBaby = MAT502BabyDemographicsandBirthDetails.LocalPatientIdBaby
		  
--		   for xml PATH('MAT502BabysDemographicsAndBirthDetails'), TYPE

--		)  
--as varchar (max)
--)
-- 	into #XMLOutBaby  
 	
--   FROM WarehouseReportingMerged.Maternity.MAT502BabysDemographicsandBirthDetails  MAT502BabyDemographicsandBirthDetails


   
   
   
 --SET @XMLNodebaby = STUFF
 --     (

 --           (

 --       SELECT ''  +  XMLOutPut from #XMLOutBaby 
        
        
 --       FOR XML PATH ('') , root('MyString'), type 
 --    ).value('/MyString[1]','varchar(max)') 
 --  , 1, 0, '')  


set @XMLNode6 = '</MATHDRHeader> </MSDS:MSDS>'

select @XMLNode1 + @XMLNode2 + @XMLNode3 + @XMLNode4 + @XMLNodeMother +  @XMLNode6


--select @XMLNode1 + @XMLNode2 + @XMLNode3 + @XMLNode4 + @XMLNodebaby  +  @XMLNode6
--@XMLNodebaby + @XMLNode6


﻿



CREATE proc [Maternity].[BuildMaternityServicesDataSet] 
	@SubmissionPeriod varchar(10)
as

declare
	 @FromDate date
	,@ToDate date


if exists
	(
	select
		1
	from
		Maternity.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	and	FreezeDate > getdate()
	)
begin
	(
	select
		 @FromDate = SubmissionPeriod.FromDate
		,@ToDate = SubmissionPeriod.ToDate 
	from
		Maternity.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	)		
end

else

begin
	print 'Invalid submission period'
	return
end

	 
truncate table Maternity.MAT101BookingAppointmentDetails
truncate table Maternity.MAT306AntenatalAppointment
truncate table Maternity.MAT310AntenatalAdmission
truncate table Maternity.MAT404LabourAndDelivery
truncate table Maternity.MAT003GPPracticeRegistration
truncate table Maternity.MAT001MothersDemographics
truncate table Maternity.MAT502BabysDemographicsAndBirthDetails

--Section to be added
--MAT102 Complicating Medical Diagnosis at Booking
--MAT103 Previous Complicating Obstetric Diagnoses at Booking
--MAT104 Family History Diagnosis at Booking
--MAT112 Dating Scan Procedure
--MAT201 Mother's ABO Blood Group and Rhesus Test Results
--MAT203 Mother's Rubella Susceptibility Test
--MAT205 Mother's Hepatitis B Screening Test
--MAT210 Mother's Asymptomatic Bacteriuria Screening Offer
--MAT211 Mother's Haemoglobinopathy Screening Test
--MAT301 Maternity Care Plan
--MAT303 Downs Syndrome Screening Test
--MAT305 Fetal Anomaly Screening Test
--MAT307 Medical Diagnosis
--MAT309 Maternity Obstetric Diagnosis
--MAT401 Medical Induction Method
--MAT404 Labour and Delivery
--MAT405 Pain Relief in Labour and Delivery
--MAT406 Anaesthesia Type in Labour and Delivery
--MAT408 Maternal Critical Incident
--MAT409 Genital Tract Trauma
--MAT501 Fetus Outcome
--MAT504 Baby Complications at Birth
--MAT506 Neonatal Resuscitation Method
--MAT507 Neonatal Resuscitation Drug or Fluid
--MAT508 Neonatal Critical Care Admission
--MAT510 Neonatal Diagnosis
--MAT511 Neonatal Critical Incident
--MAT513 Newborn Physical Screening Examination
--MAT515 Newborn Hearing Screening Test
--MAT517 Newborn Blood Spot Screening Test
--MAT602 Mother's Postpartum Discharge from Maternity Services
--MAT603 Mother’s Postpartum Readmission
--MAT901 Complicating STIs at Booking
--MAT903 Mother's Syphilis Screening Test
--MAT905 Mother's HIV Screening Test
--MAT906 Maternity STI Diagnosis

	
print 'Clear Tables' 
	 
--MAT101BookingAppointmentDetails
	
INSERT INTO Maternity.MAT101BookingAppointmentDetails
	(
	 AntenatalAppDate
	,LocalPatientIdMother
	,EDDAgreed
	,EDDMethodAgreed
	,PregnancyFirstContactDate
	,PregnancyFirstContactCareProfessionalType
	,LastMenstrualPeriodDate
	,PhysicalDisabilityStatusIndMother
	,FirstLanguageEnglishIndMother
	,EmploymentStatusMother
	,SupportStatusMother
	,EmploymentStatusPartner
	,PreviousCaesareanSections
	,PreviousLiveBirths
	,PreviousStillBirths
	,PreviousLossesLessThan24Weeks
	,SubstanceUseStatus
	,SmokingStatus
	,CigarettesPerDay
	,AlcoholUnitsPerWeek
	,FolicAcidSupplement
	,MHPredictionDetectionIndMother
	,PersonWeight
	,PersonHeight
	,ComplexSocialFactorsInd
	,XMLOutput 	
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod 
	)
	
select

	 AntenatalAppDate
	,LocalPatientIdMother
	,EDDAgreed
	,EDDMethodAgreed
	,PregnancyFirstContactDate
	,PregnancyFirstContactCareProfessionalType
	,LastMenstrualPeriodDate
	,PhysicalDisabilityStatusIndMother
	,FirstLanguageEnglishIndMother
	,EmploymentStatusMother
	,SupportStatusMother
	,EmploymentStatusPartner
	,PreviousCaesareanSections
	,PreviousLiveBirths
	,PreviousStillBirths
	,PreviousLossesLessThan24Weeks
	,SubstanceUseStatus
	,SmokingStatus
	,CigarettesPerDay
	,AlcoholUnitsPerWeek
	,FolicAcidSupplement
	,MHPredictionDetectionIndMother
	,PersonWeight
	,PersonHeight
	,ComplexSocialFactorsInd
	,XMLOutput =
		(
			Select


			 AntenatalAppDate = coalesce(AntenatalAppDate,'')
			,LocalPatientIdMother 
			,EDDAgreed = coalesce(EDDAgreed,'')
			,EDDMethodAgreed = coalesce(EDDMethodAgreed,'')
			,PregnancyFirstContactDate = coalesce(PregnancyFirstContactDate,'')
			,PregnancyFirstContactCareProfessionalType = coalesce(PregnancyFirstContactCareProfessionalType,'')
			,LastMenstrualPeriodDate = coalesce(LastMenstrualPeriodDate,'')
			,PhysicalDisabilityStatusIndMother = coalesce(PhysicalDisabilityStatusIndMother,'')
			,FirstLanguageEnglishIndMother = coalesce(FirstLanguageEnglishIndMother,'')
			,EmploymentStatusMother = coalesce(EmploymentStatusMother,'')
			,SupportStatusMother = coalesce(SupportStatusMother,'')
			,EmploymentStatusPartner = coalesce(EmploymentStatusPartner,'')
			,PreviousCaesareanSections = coalesce(PreviousCaesareanSections,'')
			,PreviousLiveBirths = coalesce(PreviousLiveBirths,'')
			,PreviousStillBirths = coalesce(PreviousStillBirths,'')
			,PreviousLossesLessThan24Weeks = coalesce(PreviousLossesLessThan24Weeks,'')
			,SubstanceUseStatus = coalesce(SubstanceUseStatus,'')
			,SmokingStatus = coalesce(SmokingStatus,'')
			,CigarettesPerDay = coalesce(CigarettesPerDay,'')
			,AlcoholUnitsPerWeek = coalesce(AlcoholUnitsPerWeek,'')
			,FolicAcidSupplement = coalesce(FolicAcidSupplement,'')
			,MHPredictionDetectionIndMother = coalesce(MHPredictionDetectionIndMother,'')
			,PersonWeight = coalesce(PersonWeight,0)
			,PersonHeight = coalesce(PersonHeight,'')
			,ComplexSocialFactorsInd = coalesce(ComplexSocialFactorsInd,'')



			for xml PATH('MAT101BookingAppointmentDetails'), TYPE

		)	
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod 
	
from

(	

	select 
		 AntenatalAppDate = CONVERT(VARCHAR(10),Encounter.VisitDate,120) 
		,LocalPatientIdMother = Replace(Encounter.DistrictNo,' ','')
		,EDDAgreed = CONVERT(VARCHAR(10),Encounter.EstimatedDeliveryDate,120) 
		,EDDMethodAgreed = '01' --Default Agreed with service
		,PregnancyFirstContactDate = CONVERT(VARCHAR(10),Encounter.FirstContactDate,120) 
		,PregnancyFirstContactCareProfessionalType = null
		,LastMenstrualPeriodDate =  CONVERT(VARCHAR(10),Encounter.LastMenstrualPeriodDate,120)  
		,PhysicalDisabilityStatusIndMother = PhysicalDisability
		,FirstLanguageEnglishIndMother = 
			Case	
			when FirstLanguageEnglishCode = 1 then 'Y'
			when FirstLanguageEnglishCode = 0 then 'N'
			else 'Z'
			end

		,EmploymentStatusMother = 
			case
			when EmploymentStatusMotherCode = 'E' then	'01'
			when EmploymentStatusMotherCode = 'U' then	'02'
			when EmploymentStatusMotherCode = 'S' then	'03'
			when EmploymentStatusMotherCode = 'L' then	'04'
			when EmploymentStatusMotherCode = 'H' then	'05'
			when EmploymentStatusMotherCode = 'N' then	'06'
			when EmploymentStatusMotherCode = 'V' then	'07'
			when EmploymentStatusMotherCode = 'Z' then	'ZZ'
			else null
			end	

				  
		,SupportStatusMother = null

		,EmploymentStatusPartner = 
			Case 
			when EmploymentStatusPartnerCode = 'E' then	'01'
			when EmploymentStatusPartnerCode = 'U' then	'02'
			when EmploymentStatusPartnerCode = 'S' then	'03'
			when EmploymentStatusPartnerCode = 'L' then	'04'
			when EmploymentStatusPartnerCode = 'H' then	'05'
			when EmploymentStatusPartnerCode = 'N' then	'06'
			when EmploymentStatusPartnerCode = 'V' then	'07'
			when EmploymentStatusPartnerCode = 'Z' then	'ZZ'
			else null
			end
						
		,PreviousCaesareanSections = coalesce(PreviousCaesareanSections ,0)
		,PreviousLiveBirths = coalesce(PreviousLiveBirths ,0)
		,PreviousStillBirths = coalesce(PreviousStillBirths ,0)
		,PreviousLossesLessThan24Weeks =coalesce(PreviousLossesLessThan24Weeks ,0)

		,SubstanceUseStatus = 
			Case
			when DrugCode = 'N'then '03'
			when DrugCode IN ('P','D','M','S') then  '01'
			else null
			end

		,SmokingStatus = 
			Case 
			when SmokingStatusCode = 'N' then '06'
			when SmokingStatusCode = 'A' then '04'
			when SmokingStatusCode = 'J' then '03'
			when SmokingStatusCode = 'R' then '01'
			when SmokingStatusCode = 'T' then '01'
			when SmokingStatusCode = 'Y' then '01'
			when SmokingStatusCode = 'U' then '09'
			when SmokingStatusCode = 'H' then '06'
			else null
			end	

		,CigarettesPerDay = CigarettesPerDay
		,AlcoholUnitsPerWeek = AlcoholUnitsPerWeek
		,FolicAcidSupplement = null

		,MHPredictionDetectionIndMother = 
			case
			when PsychiatricProblemCode = 'N' then 'N'
			when PsychiatricProblemCode IS null then null
			when PsychiatricProblemCode <> 'N' then 'Y'
			end	

		,PersonWeight = cast(WeightMother as decimal (6,3)) 
		,PersonHeight = left(HeightMother,1) + '.' + right(replace(HeightMother,' ',''),2)
		,ComplexSocialFactorsInd = null
		,DatasetCode = 'Maternity'
		,SourceUniqueID = Encounter.SourceUniqueID
		,SubmissionPeriod = @SubmissionPeriod
	from 			
		WarehouseOLAPMergedV2.Maternity.BaseAntenatalCare Encounter
	where
		Encounter.VisitDate between @FromDate and @ToDate
			
) Encounter					
print 'MAT101BookingAppointmentDetails'
					
--MAT306AntenatalAppointment

INSERT INTO WarehouseReportingMerged.Maternity.MAT306AntenatalAppointment
	(
	LocalPatientIdMother
	,AntenatalAppDate
	,XMLOutput
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod
	)
Select
	 LocalPatientIdMother
	,AntenatalAppDate
	,XMLOutput =
	(
		Select

		 LocalPatientIdMother
		,AntenatalAppDate 			


		for xml PATH('MAT306AntenatalAppointment'), TYPE

	)
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod
	
From 	

(
	
	select 
		LocalPatientIdMother =	Encounter.Districtno
		,AntenatalAppDate =  CONVERT(VARCHAR(10),Encounter.AppointmentDate,120)
		,DatasetCode = 'OP'
		,SourceUniqueID = Encounter.SourceUniqueID
		,SubmissionPeriod = @SubmissionPeriod
	from 
		WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

	inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference EncounterReference
	on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.OP.FirstAttendance FirstAttendance
	on FirstAttendance.SourceFirstAttendanceID = EncounterReference.FirstAttendanceID

	inner join WarehouseOLAPMergedV2.OP.AttendanceStatus AttendanceStatus
	on AttendanceStatus.SourceAttendanceStatusID = EncounterReference.AttendanceStatusID

	inner join WarehouseOLAPMergedV2.OP.AppointmentType AppointmentType
	on AppointmentType.SourceAppointmentTypeID = EncounterReference.AppointmentTypeID

	inner join WarehouseOLAPMergedV2.OP.Clinic Clinic
	on Clinic.SourceClinicID = EncounterReference.ClinicID

	inner join WarehouseOLAPMergedV2.WH.Member Specialty
	on	Specialty.SourceValueID = EncounterReference.SpecialtyID

	where 
		FirstAttendance.NationalFirstAttendanceCode <> '1'
	and	AppointmentDate between @FromDate and @ToDate
	and	AttendanceStatus.NationalAttendanceStatusCode in ('5','6','-1')
	and	Specialty.NationalValueCode = '501'
	and
	exists
	(
		select 
			1 
			from 
			Maternity.EntityLookup AntenatalClinics
		where
		AntenatalClinics.EntityTypeCode = 'AntenatalClinics'
		and 
		AntenatalClinics.ContextCode = 'MAT'
		and 
		AntenatalClinics.EntityCode = Encounter.Cliniccode
	)

) Encounter
		
print 'MAT306AntenatalAppointment'		
	
--	MAT310AntenatalAdmissionBase			

INSERT INTO WarehouseReportingMerged.Maternity.MAT310AntenatalAdmission
	(
	 LocalPatientIdMother
	,HPSAntenatalStartDate
	,HPSAntenatalDischargeDate
	,XMLOutput
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod
	)
Select
	 LocalPatientIdMother
	,HPSAntenatalStartDate
	,HPSAntenatalDischargeDate
	,XMLOutput =
	(
		Select

		LocalPatientIdMother
		,HPSAntenatalStartDate
		,HPSAntenatalDischargeDate = coalesce(HPSAntenatalDischargeDate	,'')



		from WarehouseReportingMerged.Maternity.MAT310AntenatalAdmission MAT310AntenatalAdmission

		where  MAT310AntenatalAdmission.LocalPatientIdMother = Encounter.LocalPatientIdMother


		for xml PATH('MAT310AntenatalAdmission'), TYPE

	)	
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod
from

(

	select 
		LocalPatientIdMother = Encounter.DistrictNo
		,HPSAntenatalStartDate = CONVERT(VARCHAR(10),Encounter.AdmissionDate,120)  
		,HPSAntenatalDischargeDate = CONVERT(VARCHAR(10),Encounter.DischargeDate,120)  
		,DatasetCode = 'APC'
		,SourceUniqueID = Encounter.SourceUniqueID
		,SubmissionPeriod = @SubmissionPeriod
	from
 		WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference Reference
	on Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.Maternity.BaseBirth Birth
	on	Birth.MotherNo = Encounter.CasenoteNumber
	and Birth.InfantDateOfBirth between Encounter.AdmissionDate-30 and Encounter.AdmissionDate	

	where 
		Encounter.DischargeDate between @FromDate and @ToDate
	and	Encounter.Reportable = 1
	and	FirstEpisodeInSpellIndicator = 1
	and	datediff(year,dateofbirth,@ToDate) > 1
	and
		exists
		(
		select 
			ProviderSpellNo
		from
			WarehouseOLAPMergedV2.APC.BaseWardStay AntenatalWard 
		where 
			--Antenatal Ward
			WardCode in ('64a','65')
			  
		and not exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.APC.BaseWardStay MatWard
			where
				AntenatalWard.ProviderSpellNo =  MatWard.ProviderSpellNo
			--Maternity Ward
			and MatWard.WardCode in ('64','47a','65i')
			)
		and
			Encounter.ProviderSpellNo = AntenatalWard.ProviderSpellNo
		)
		
		and
		
		Birth.InfantDateOfBirth is null
) Encounter
			
print 'MAT310AntenatalAdmission'	
			
--MAT404LabourAndDelivery

--List of Mothers CMIS Casenote mapped to Districtno

select  

 CasenoteNumber
,MotherDistrictno = Districtno
,NHSNumber

into #PatientID

from 	Warehouse.PAS.Casenote Casenote

inner join Warehouse.PAS.Patient Patient
on Patient.SourcePatientNo = Casenote.SourcePatientNo

where exists

(
select 
MotherNo
from WarehouseOLAPMergedV2.Maternity.BaseBirth BaseBirth
where
InfantDateOfBirth between @FromDate and dateadd(day,60,@ToDate) 
and  BaseBirth.MotherNo = Casenote.CasenoteNumber
)


INSERT INTO WarehouseReportingMerged.Maternity.MAT404LabourAndDelivery
	(
	 LabourOnsetDateTime
	,CaesareanDateTime
	,LocalPatientIdMother
	,LabourOnsetPresentation
	,StartDateTimeMotherDeliveryHPS
	,DecisionToDeliverDateTime
	,ROMDateTime
	,ROMMethod
	,ROMReason
	,LabourOnsetSecondStageDateTime
	,LabourThirdStageEndDateTime
	,EpisiotomyReason
	,PlacentaDeliveryMethod
	,DischargeDateTimeMotherDeliveryHPS
	,OrgCodePostnatalPathLeadProvider
	,XMLOutput
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod
	)
select
	 LabourOnsetDateTime
	,CaesareanDateTime
	,LocalPatientIdMother
	,LabourOnsetPresentation
	,StartDateTimeMotherDeliveryHPS
	,DecisionToDeliverDateTime
	,ROMDateTime
	,ROMMethod
	,ROMReason
	,LabourOnsetSecondStageDateTime
	,LabourThirdStageEndDateTime
	,EpisiotomyReason
	,PlacentaDeliveryMethod
	,DischargeDateTimeMotherDeliveryHPS
	,OrgCodePostnatalPathLeadProvider
	,XMLOutput =
		(
			Select

			LabourOnsetDateTime = coalesce(LabourOnsetDateTime,'')
			,CaesareanDateTime = coalesce(CaesareanDateTime,'')
			,LocalPatientIdMother = coalesce(LocalPatientIdMother,'')
			,LabourOnsetPresentation = coalesce(LabourOnsetPresentation,'')
			,StartDateTimeMotherDeliveryHPS = coalesce(StartDateTimeMotherDeliveryHPS,'')
			,DecisionToDeliverDateTime = coalesce(DecisionToDeliverDateTime,'')
			,ROMDateTime = coalesce(ROMDateTime,'')
			,ROMMethod = coalesce(ROMMethod,'')
			,ROMReason = coalesce(ROMReason,'')
			,LabourOnsetSecondStageDateTime = coalesce(LabourOnsetSecondStageDateTime,'')
			,LabourThirdStageEndDateTime = coalesce(LabourThirdStageEndDateTime,'')
			,EpisiotomyReason = coalesce(EpisiotomyReason,'')
			,PlacentaDeliveryMethod = coalesce(PlacentaDeliveryMethod,'')
			,DischargeDateTimeMotherDeliveryHPS = coalesce(DischargeDateTimeMotherDeliveryHPS,'')
			,OrgCodePostnatalPathLeadProvider = coalesce(OrgCodePostnatalPathLeadProvider,'')

			for xml PATH('MAT404LabourAndDelivery'), TYPE

		)	
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod
from
(	
	select 
		 LabourOnsetDateTime = left(OnsetFirstDate,10) + 'T' + left(OnsetFirstTime,2) + ':' + right(OnsetFirstTime,2) + ':00'
		,CaesareanDateTime = null
		,LocalPatientIdMother = Encounter.DistrictNo
		,LabourOnsetPresentation = null
		,StartDateTimeMotherDeliveryHPS = Replace(CONVERT(VARCHAR(20),Encounter.Admissiontime,120),' ','T')   
		,DecisionToDeliverDateTime = null
		,ROMDateTime = replace(left(OnsetFirstDate,10) + 'T' + left(RuptureDate,2) + ':' + right(RuptureTime,2) + ':00',' ','0')         
		,ROMMethod = null --Birth.RuptureMethod
		,ROMReason = null
		,LabourOnsetSecondStageDateTime = left(OnsetSecondDate,10) + 'T' + left(OnsetSecondTime,2) + ':' + right(OnsetFirstTime,2) + ':00'
		,LabourThirdStageEndDateTime = null
		,EpisiotomyReason = null
		,PlacentaDeliveryMethod = null
		,DischargeDateTimeMotherDeliveryHPS = Replace(CONVERT(VARCHAR(20),Encounter.DischargeTime,120),' ','T')       
		,OrgCodePostnatalPathLeadProvider = null
		,DatasetCode = 'APC'
		,SourceUniqueID = Encounter.SourceUniqueID
		,SubmissionPeriod = @SubmissionPeriod
	from 
		WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference Reference
	on Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join #PatientID PatientID 
	on PatientID.MotherDistrictno = Encounter.Districtno
		
	inner join WarehouseOLAPMergedV2.Maternity.BaseBirth Birth
	on	Birth.MotherNo = Encounter.CasenoteNumber
	and Birth.InfantDateOfBirth between Encounter.AdmissionDate and Encounter.DischargeDate	

	where 
		Encounter.Reportable = 1
	and	FirstEpisodeInSpellIndicator = 1
	--and	datediff(year,dateofbirth,@ToDate) > 1
	and Encounter.AdmissionDate between @FromDate and @ToDate
--and	exists
--	(
--	select 
--		ProviderSpellNo
--	from
--		WarehouseOLAPMergedV2.APC.BaseWardStay AntenatalWard 
--	where 
--		Encounter.AdmissionDate between @FromDate and @ToDate
--	and	exists
--		(
--		select
--			1
--		from
--			WarehouseOLAPMergedV2.APC.BaseWardStay MatWard
--		where
--			AntenatalWard.ProviderSpellNo =  MatWard.ProviderSpellNo
--		--Maternity Ward
--		and MatWard.WardCode in ('64','47a','65i')
--		)

--	and	Encounter.ProviderSpellNo = AntenatalWard.ProviderSpellNo
--	)
	
--and OnsetFirstDate is not null --Temp untill we can find CaesareanDateTime

) Encounter


			
print 'MAT404LabourAndDelivery'


--MAT502BabyDemographicsandBirthDetails

INSERT INTO WarehouseReportingMerged.Maternity.MAT502BabysDemographicsAndBirthDetails
           (
            LocalPatientIdBaby
           ,LocalPatientIdMother
           ,OrgCodeLocalPatientIdBaby
           ,BabyBirthDateTime
           ,NHSNumberBaby
           ,NHSNumberStatusBaby
           ,PersonPhenotypicSex
           ,PersonDeathDateTimeBaby
           ,BirthOrderMaternitySUS
           ,BirthWeight
           ,GestationLengthBirth
           ,DeliveryMethodBaby
           ,WaterDeliveryInd
           ,ApgarScore5
           ,SiteCodeActualDelivery
           ,PlaceTypeActualDelivery
           ,PlaceTypeActualMidwifery
           ,BabyFirstFeedDateTime
           ,BabyFirstFeedBreastMilkStatus
           ,BabyBreastMilkStatusDischarge
           ,SkinToSkinContact1Hour
           ,XMLOutput
           ,DatasetCode
           ,SourceUniqueID
           ,SubmissionPeriod
           
           )
select

            LocalPatientIdBaby
           ,LocalPatientIdMother
           ,OrgCodeLocalPatientIdBaby
           ,BabyBirthDateTime
           ,NHSNumberBaby
           ,NHSNumberStatusBaby
           ,PersonPhenotypicSex
           ,PersonDeathDateTimeBaby
           ,BirthOrderMaternitySUS
           ,BirthWeight
           ,GestationLengthBirth
           ,DeliveryMethodBaby
           ,WaterDeliveryInd
           ,ApgarScore5
           ,SiteCodeActualDelivery
           ,PlaceTypeActualDelivery
           ,PlaceTypeActualMidwifery
           ,BabyFirstFeedDateTime
           ,BabyFirstFeedBreastMilkStatus
           ,BabyBreastMilkStatusDischarge
           ,SkinToSkinContact1Hour
           ,XMLOutput =
				(
						select	
						   LocalPatientIdBaby
						  ,LocalPatientIdMother
						  ,OrgCodeLocalPatientIdBaby 
						  ,BabyBirthDateTime = coalesce(BabyBirthDateTime,'')
						  ,NHSNumberBaby = coalesce(NHSNumberBaby,'')
						  ,NHSNumberStatusBaby = coalesce(NHSNumberStatusBaby,'')
						  ,PersonPhenotypicSex = coalesce(PersonPhenotypicSex,'')
						  ,PersonDeathDateTimeBaby = coalesce(PersonDeathDateTimeBaby,'')
						  ,BirthOrderMaternitySUS = coalesce(BirthOrderMaternitySUS,'')
						  ,BirthWeight = coalesce(BirthWeight,'')
						  ,GestationLengthBirth = coalesce(GestationLengthBirth,'')
						  ,DeliveryMethodBaby = coalesce(DeliveryMethodBaby,'')
						  ,WaterDeliveryInd = coalesce(WaterDeliveryInd,'')
						  ,ApgarScore5 = coalesce(ApgarScore5,'')
						  ,SiteCodeActualDelivery = coalesce(SiteCodeActualDelivery,'')
						  ,PlaceTypeActualDelivery = coalesce(PlaceTypeActualDelivery,'')
						  ,PlaceTypeActualMidwifery = coalesce(PlaceTypeActualMidwifery,'')
						  ,BabyFirstFeedDateTime = coalesce(BabyFirstFeedDateTime,'')
						  ,BabyFirstFeedBreastMilkStatus = coalesce(BabyFirstFeedBreastMilkStatus,'')
						  ,BabyBreastMilkStatusDischarge = coalesce(BabyBreastMilkStatusDischarge,'')
						  ,SkinToSkinContact1Hour = coalesce(SkinToSkinContact1Hour,'')

					  
					   for xml PATH('MAT502BabysDemographicsAndBirthDetails'), TYPE
						   
				)           
           ,DatasetCode
           ,SourceUniqueID
           ,SubmissionPeriod
from
(
	 select
		--M--
		 LocalPatientIdBaby = BabyPatient.Districtno     
		,LocalPatientIdMother = MotherDistrictno
		,OrgCodeLocalPatientIdBaby = 'RW3'
		,BabyBirthDateTime = replace(left(InfantDateOfBirth,10) + 'T' + left(TimeOfBirth,2) + ':' + right(TimeOfBirth,2) + ':00',' ','0')


		,NHSNumberBaby = InfantNhsNumber
		       
		,NHSNumberStatusBaby  = null
		,PersonPhenotypicSex  = null
		,PersonDeathDateTimeBaby  = Case when Outcome in ('A','I','X') then replace(left(InfantDateOfBirth,10) + 'T' + left(TimeOfBirth,2) + ':' + right(TimeOfBirth,2) + ':00',' ','0') else null end
		,BirthOrderMaternitySUS = null
		,BirthWeight = null
		,GestationLengthBirth = null
		,DeliveryMethodBaby = null
		,WaterDeliveryInd = null
		,ApgarScore5 = null
		,SiteCodeActualDelivery = null
		,PlaceTypeActualDelivery = null
		,PlaceTypeActualMidwifery = null
		,BabyFirstFeedDateTime = null
		,BabyFirstFeedBreastMilkStatus = null
		,BabyBreastMilkStatusDischarge = null
		,SkinToSkinContact1Hour = null
		,DatasetCode = 'Birth'
		,SourceUniqueID = null
		,SubmissionPeriod = @SubmissionPeriod		

	from 
	WarehouseOLAPMergedV2.Maternity.BaseBirth Birth

	inner join Warehouse.PAS.Patient BabyPatient
	on replace(BabyPatient.NhsNumber,' ','') = Birth.InfantNhsNumber  
	and Deleted = 0

	inner join 

	(
		select  
		 CasenoteNumber
		,MotherDistrictno = Districtno
		,NHSNumber

		from 	Warehouse.PAS.Casenote Casenote

		inner join Warehouse.PAS.Patient Patient
		on Patient.SourcePatientNo = Casenote.SourcePatientNo
		and Deleted = 0

	) Mother
	on Mother.CasenoteNumber = Birth.MotherNO

	 

	where 

	Birth.InfantDateOfBirth between @FromDate and @ToDate
	and
	ActualHospital in ('M','L')
	and 
	outcome in ('L','N','A','I','X')
	--and 
	--exists
	--(
	--Select
	--1
	--from Maternity.MAT001MothersDemographics MAT001MothersDemographics
	--where MAT001MothersDemographics.LocalPatientIdMother = Mother.MotherDistrictno

	--)

) Encounter
          

print 'MAT502BabysDemographicsAndBirthDetails'


--Generate ID’s to drive MAT003GPPracticeRegistrationBase and MAT001MothersDemographics

select  
	LocalPatientIdMother
	,LocalPatientIdType

into
	#LocalPatientId
from
	(
	select LocalPatientIdMother, LocalPatientIdType = 'Mother' from Maternity.MAT101BookingAppointmentDetails Encounter
			
	union  all
		
	select LocalPatientIdMother, LocalPatientIdType = 'Mother' from Maternity.MAT306AntenatalAppointment Encounter
			
	union  all
		
	select LocalPatientIdMother, LocalPatientIdType = 'Mother' from Maternity.MAT310AntenatalAdmission Encounter			

	union  all
		
	select LocalPatientIdMother, LocalPatientIdType = 'Mother' from Maternity.MAT404LabourAndDelivery Encounter	
	
	union  all
		
	select LocalPatientIdMother, LocalPatientIdType = 'Mother' from Maternity.MAT502BabysDemographicsAndBirthDetails Encounter					
			
	) Patients

print 'Generate ID’s'		

					
--	Insert Maternity.MAT003GPPracticeRegistration
			
INSERT INTO WarehouseReportingMerged.Maternity.MAT003GPPracticeRegistration
	(
	 LocalPatientIdMother
	,OrgCodeGMPMother
	,StartDateGMPRegistration
	,EndDateGMPRegistration
	,OrgCodeCommissioner
	,XMLOutput
	,SubmissionPeriod 
	)

Select
	 LocalPatientIdMother
	,OrgCodeGMPMother
	,StartDateGMPRegistration
	,EndDateGMPRegistration
	,OrgCodeCommissioner
	,XMLOutput =
	(
	Select
	 LocalPatientIdMother 
	,OrgCodeGMPMother = coalesce(OrgCodeGMPMother,'')
	,StartDateGMPRegistration = coalesce(StartDateGMPRegistration,'')
	,EndDateGMPRegistration = coalesce(EndDateGMPRegistration,'')
	,OrgCodeCommissioner = coalesce(OrgCodeCommissioner,'')

	for xml PATH('MAT003GPPracticeRegistration'), TYPE
	)
	,SubmissionPeriod
from

(

	select
		 LocalPatientIdMother = GPHistory.DistrictNo
		,OrgCodeGMPMother = GPHistory.GpPracticeCode
		,StartDateGMPRegistration = 
			case
				when GPHistory.EffectiveFromDate = '1900-01-01' then 	CONVERT(VARCHAR(10),@FromDate,120) 
				else	CONVERT(VARCHAR(10),GPHistory.EffectiveFromDate,120) 
			end
		,EndDateGMPRegistration = 
			case
				when GPHistory.EffectiveToDate > @ToDate then null
			else CONVERT(VARCHAR(10),GPHistory.EffectiveToDate,120)
			end  
		,OrgCodeCommissioner = ParentOrganisationCode
		,SubmissionPeriod = @SubmissionPeriod
	from
		Warehouse.pas.PatientGp GPHistory 
		
		left join WarehouseOLAPMergedV2.WH.GPPractice GPPractice
		on GPPractice.GpPracticeCode = GPHistory.GpPracticeCode
	where 
		  
		--GPHistory.DistrictNo = '03759619'
		--and
		exists
		(
		select
			1
		from
			#LocalPatientId LocalPatientId
		where
			LocalPatientId.LocalPatientIdMother  = GPHistory.DistrictNo  
		)
			
		and
			(
				(
					GPHistory.EffectiveToDate is null 
				and GPHistory.EffectiveFromDate <= @ToDate
				)
			or	@ToDate between GPHistory.EffectiveFromDate and GPHistory.EffectiveToDate
			)
		
) Encounter		




print 'MAT003GPPracticeRegistration'

--Insert Maternity.MAT001MothersDemographics

select postcode = replace(postcode,' ',''), [PCG of Residence] into #CCG  from [Organisation].[ODS].[Extended Postcode] 

INSERT INTO WarehouseReportingMerged.Maternity.MAT001MothersDemographics
	(
	 LocalPatientIdMother
	,OrgCodeLocalPatientIdMother
	,OrgCodeRes
	,NHSNumberMother
	,NHSNumberStatusMother
	,PersonBirthDateMother
	,Postcode
	,EthnicCategoryMother
	,PersonDeathDateTimeMother
	,XMLOutput
	,SubmissionPeriod
	)
Select

	 LocalPatientIdMother
	,OrgCodeLocalPatientIdMother
	,OrgCodeRes
	,NHSNumberMother
	,NHSNumberStatusMother
	,PersonBirthDateMother
	,Postcode
	,EthnicCategoryMother
	,PersonDeathDateTimeMother
	,XML = 

		(
			Select

			 LocalPatientIdMother 
			,OrgCodeLocalPatientIdMother = coalesce(OrgCodeLocalPatientIdMother,'')
			,OrgCodeRes = coalesce(OrgCodeRes,'')
			,NHSNumberMother = coalesce(NHSNumberMother,'')
			,NHSNumberStatusMother = coalesce(NHSNumberStatusMother,'')
			,PersonBirthDateMother = coalesce(PersonBirthDateMother,'')
			,Postcode = coalesce(Postcode,'')
			,EthnicCategoryMother = coalesce(EthnicCategoryMother,'')
			,PersonDeathDateTimeMother = coalesce(PersonDeathDateTimeMother,'')

			for xml PATH(''), TYPE

		)
	,SubmissionPeriod
From

(

	select 
		 LocalPatientIdMother = Patient.DistrictNo
		,OrgCodeLocalPatientIdMother = 'RW3'
		,OrgCodeRes = [PCG of Residence]
		,NHSNumberMother = replace(NHSNumber,' ','')
		,NHSNumberStatusMother = 
			case
			when NHSNumber is null then '07' --Number not present and trace not required 
			else NHSNumberStatus.NationalNHSNumberStatusCode
			end
		,PersonBirthDateMother = CONVERT(VARCHAR(10),Patient.DateOfBirth,120) 
		,Postcode = PatientAddress.Postcode
		,EthnicCategoryMother = coalesce(EthnicCategory.NationalEthnicCategoryCode,'99')
		,PersonDeathDateTimeMother = CONVERT(VARCHAR(10),Patient.DateOfDeath,120) 
		,SubmissionPeriod = @SubmissionPeriod
	from 
		Warehouse.PAS.Patient Patient

	left join WarehouseOLAPMergedV2.wh.NHSNumberStatus NHSNumberStatus
	on NHSNumberStatus.SourceNHSNumberStatusCode = Patient.NHSNumberStatusId
	and NHSNumberStatus.SourceContextCode = 'CEN||PAS'

	left join WarehouseOLAPMergedV2.wh.EthnicCategory EthnicCategory
	on EthnicCategory.SourceEthnicCategoryCode = Patient.EthnicOriginCode
	and EthnicCategory.SourceContextCode = 'CEN||PAS'

	left join Warehouse.PAS.PatientAddress PatientAddress
	on Patient.SourcePatientNo = PatientAddress.SourcePatientNo
	and
		(
			(
				PatientAddress.EffectiveToDate is null 
			and PatientAddress.EffectiveFromDate <= @ToDate
			)	
		or	@ToDate between PatientAddress.EffectiveFromDate and PatientAddress.EffectiveToDate
		)
		
	left join #CCG CCG
	on  CCG.postcode = replace(PatientAddress.Postcode,' ','')
			
	where
		exists
		(
		select
			1
		from
			#LocalPatientId LocalPatientId
		where
			LocalPatientId.LocalPatientIdMother = Patient.DistrictNo  
		)
		
	and 
	(
	Patient.Deleted = 0
	or
	Patient.Modified between  @FromDate and @ToDate and  Deleted = 1
	)

) Encounter
		
print 'MAT001MothersDemographics'



--MAT101BookingAppointmentDetails records that MAT101 required as other data group being submitted
	
INSERT INTO Maternity.MAT101BookingAppointmentDetails
	(
	 AntenatalAppDate
	,LocalPatientIdMother
	,EDDAgreed
	,EDDMethodAgreed
	,PregnancyFirstContactDate
	,PregnancyFirstContactCareProfessionalType
	,LastMenstrualPeriodDate
	,PhysicalDisabilityStatusIndMother
	,FirstLanguageEnglishIndMother
	,EmploymentStatusMother
	,SupportStatusMother
	,EmploymentStatusPartner
	,PreviousCaesareanSections
	,PreviousLiveBirths
	,PreviousStillBirths
	,PreviousLossesLessThan24Weeks
	,SubstanceUseStatus
	,SmokingStatus
	,CigarettesPerDay
	,AlcoholUnitsPerWeek
	,FolicAcidSupplement
	,MHPredictionDetectionIndMother
	,PersonWeight
	,PersonHeight
	,ComplexSocialFactorsInd
	,XMLOutput 	
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod 
	)
	
select

	 AntenatalAppDate
	,LocalPatientIdMother
	,EDDAgreed
	,EDDMethodAgreed
	,PregnancyFirstContactDate
	,PregnancyFirstContactCareProfessionalType
	,LastMenstrualPeriodDate
	,PhysicalDisabilityStatusIndMother
	,FirstLanguageEnglishIndMother
	,EmploymentStatusMother
	,SupportStatusMother
	,EmploymentStatusPartner
	,PreviousCaesareanSections
	,PreviousLiveBirths
	,PreviousStillBirths
	,PreviousLossesLessThan24Weeks
	,SubstanceUseStatus
	,SmokingStatus
	,CigarettesPerDay
	,AlcoholUnitsPerWeek
	,FolicAcidSupplement
	,MHPredictionDetectionIndMother
	,PersonWeight
	,PersonHeight
	,ComplexSocialFactorsInd
	,XMLOutput =
		(
			Select


			 AntenatalAppDate = coalesce(AntenatalAppDate,'')
			,LocalPatientIdMother 
			,EDDAgreed = coalesce(EDDAgreed,'')
			,EDDMethodAgreed = coalesce(EDDMethodAgreed,'')
			,PregnancyFirstContactDate = coalesce(PregnancyFirstContactDate,'')
			,PregnancyFirstContactCareProfessionalType = coalesce(PregnancyFirstContactCareProfessionalType,'')
			,LastMenstrualPeriodDate = coalesce(LastMenstrualPeriodDate,'')
			,PhysicalDisabilityStatusIndMother = coalesce(PhysicalDisabilityStatusIndMother,'')
			,FirstLanguageEnglishIndMother = coalesce(FirstLanguageEnglishIndMother,'')
			,EmploymentStatusMother = coalesce(EmploymentStatusMother,'')
			,SupportStatusMother = coalesce(SupportStatusMother,'')
			,EmploymentStatusPartner = coalesce(EmploymentStatusPartner,'')
			,PreviousCaesareanSections = coalesce(PreviousCaesareanSections,'')
			,PreviousLiveBirths = coalesce(PreviousLiveBirths,'')
			,PreviousStillBirths = coalesce(PreviousStillBirths,'')
			,PreviousLossesLessThan24Weeks = coalesce(PreviousLossesLessThan24Weeks,'')
			,SubstanceUseStatus = coalesce(SubstanceUseStatus,'')
			,SmokingStatus = coalesce(SmokingStatus,'')
			,CigarettesPerDay = coalesce(CigarettesPerDay,'')
			,AlcoholUnitsPerWeek = coalesce(AlcoholUnitsPerWeek,'')
			,FolicAcidSupplement = coalesce(FolicAcidSupplement,'')
			,MHPredictionDetectionIndMother = coalesce(MHPredictionDetectionIndMother,'')
			,PersonWeight = coalesce(PersonWeight,0)
			,PersonHeight = coalesce(PersonHeight,'')
			,ComplexSocialFactorsInd = coalesce(ComplexSocialFactorsInd,'')



			for xml PATH('MAT101BookingAppointmentDetails'), TYPE

		)	
	,DatasetCode
	,SourceUniqueID
	,SubmissionPeriod 
	
from

(	
	select

		 AntenatalAppDate
		,LocalPatientIdMother
		,EDDAgreed
		,EDDMethodAgreed
		,PregnancyFirstContactDate
		,PregnancyFirstContactCareProfessionalType
		,LastMenstrualPeriodDate
		,PhysicalDisabilityStatusIndMother
		,FirstLanguageEnglishIndMother
		,EmploymentStatusMother
		,SupportStatusMother
		,EmploymentStatusPartner
		,PreviousCaesareanSections
		,PreviousLiveBirths
		,PreviousStillBirths
		,PreviousLossesLessThan24Weeks
		,SubstanceUseStatus
		,SmokingStatus
		,CigarettesPerDay
		,AlcoholUnitsPerWeek
		,FolicAcidSupplement
		,MHPredictionDetectionIndMother
		,PersonWeight
		,PersonHeight
		,ComplexSocialFactorsInd
		,DatasetCode
		,SourceUniqueID
		,SubmissionPeriod  

		from
		
		(

	select 
		 AntenatalAppDate = CONVERT(VARCHAR(10),Encounter.VisitDate,120) 
		,LocalPatientIdMother = Replace(Encounter.DistrictNo,' ','')
		,EDDAgreed = CONVERT(VARCHAR(10),Encounter.EstimatedDeliveryDate,120) 
		,EDDMethodAgreed = '01' --Default Agreed with service
		,PregnancyFirstContactDate = CONVERT(VARCHAR(10),Encounter.FirstContactDate,120) 
		,PregnancyFirstContactCareProfessionalType = null
		,LastMenstrualPeriodDate =  CONVERT(VARCHAR(10),Encounter.LastMenstrualPeriodDate,120)  
		,PhysicalDisabilityStatusIndMother = PhysicalDisability
		,FirstLanguageEnglishIndMother = 
			Case	
			when FirstLanguageEnglishCode = 1 then 'Y'
			when FirstLanguageEnglishCode = 0 then 'N'
			else 'Z'
			end

		,EmploymentStatusMother = 
			case
			when EmploymentStatusMotherCode = 'E' then	'01'
			when EmploymentStatusMotherCode = 'U' then	'02'
			when EmploymentStatusMotherCode = 'S' then	'03'
			when EmploymentStatusMotherCode = 'L' then	'04'
			when EmploymentStatusMotherCode = 'H' then	'05'
			when EmploymentStatusMotherCode = 'N' then	'06'
			when EmploymentStatusMotherCode = 'V' then	'07'
			when EmploymentStatusMotherCode = 'Z' then	'ZZ'
			else null
			end	

				  
		,SupportStatusMother = null

		,EmploymentStatusPartner = 
			Case 
			when EmploymentStatusPartnerCode = 'E' then	'01'
			when EmploymentStatusPartnerCode = 'U' then	'02'
			when EmploymentStatusPartnerCode = 'S' then	'03'
			when EmploymentStatusPartnerCode = 'L' then	'04'
			when EmploymentStatusPartnerCode = 'H' then	'05'
			when EmploymentStatusPartnerCode = 'N' then	'06'
			when EmploymentStatusPartnerCode = 'V' then	'07'
			when EmploymentStatusPartnerCode = 'Z' then	'ZZ'
			else null
			end
						
		,PreviousCaesareanSections = coalesce(PreviousCaesareanSections ,0)
		,PreviousLiveBirths = coalesce(PreviousLiveBirths ,0)
		,PreviousStillBirths = coalesce(PreviousStillBirths ,0)
		,PreviousLossesLessThan24Weeks =coalesce(PreviousLossesLessThan24Weeks ,0)

		,SubstanceUseStatus = 
			Case
			when DrugCode = 'N'then '03'
			when DrugCode IN ('P','D','M','S') then  '01'
			else null
			end

		,SmokingStatus = 
			Case 
			when SmokingStatusCode = 'N' then '06'
			when SmokingStatusCode = 'A' then '04'
			when SmokingStatusCode = 'J' then '03'
			when SmokingStatusCode = 'R' then '01'
			when SmokingStatusCode = 'T' then '01'
			when SmokingStatusCode = 'Y' then '01'
			when SmokingStatusCode = 'U' then '09'
			when SmokingStatusCode = 'H' then '06'
			else null
			end	

		,CigarettesPerDay = CigarettesPerDay
		,AlcoholUnitsPerWeek = AlcoholUnitsPerWeek
		,FolicAcidSupplement = null

		,MHPredictionDetectionIndMother = 
			case
			when PsychiatricProblemCode = 'N' then 'N'
			when PsychiatricProblemCode IS null then null
			when PsychiatricProblemCode <> 'N' then 'Y'
			end	

		,PersonWeight = cast(WeightMother as decimal (6,3)) 
		,PersonHeight = left(HeightMother,1) + '.' + right(replace(HeightMother,' ',''),2)
		,ComplexSocialFactorsInd = null
		,DatasetCode = 'Maternity'
		,SourceUniqueID = Encounter.SourceUniqueID
		,SubmissionPeriod = @SubmissionPeriod
		,LatestRecord = 
			ROW_NUMBER ( ) 
			OVER (PARTITION BY Encounter.DistrictNo  order by Encounter.VisitDate desc)
	from 			
		WarehouseOLAPMergedV2.Maternity.BaseAntenatalCare Encounter
	where

	Encounter.VisitDate <= @ToDate
	and
	not exists
	(
	Select

		MAT101BookingAppointmentDetails.LocalPatientIdMother

	from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails

	where MAT101BookingAppointmentDetails.LocalPatientIdMother = Encounter.DistrictNo

	)
	and
	exists
		(
		select
			1
		from
			#LocalPatientId LocalPatientId
		where
			LocalPatientId.LocalPatientIdMother = Encounter.DistrictNo
			and
			LocalPatientId.LocalPatientIdType = 'Mother'

		)

	) MAT101Booking

	where 
	 
	 LatestRecord = 1
			
) Encounter
					
print 'MAT502BabysDemographicsAndBirthDetails Required as other data goups being sent'

		
drop table #LocalPatientId
drop table #CCG
drop table #PatientID


--maintain SubmissionPeriod
update
	Maternity.SubmissionPeriod
set
	Created = getdate()
	,ByWhom = suser_name()
where
	SubmissionPeriod = @SubmissionPeriod





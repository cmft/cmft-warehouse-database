﻿
CREATE proc Maternity.UpdateFreezeDate

 @SubmissionPeriod varchar(20)
,@FreezeDate date = null

as

set @FreezeDate = coalesce(@FreezeDate, getdate())

update Maternity.SubmissionPeriod

set 
	 FreezeDate = @FreezeDate
	,ByWhom = (Select SYSTEM_USER)
	,Updated = (Select getdate())

where SubmissionPeriod = @SubmissionPeriod
﻿

CREATE view [Maternity].[SubmittedReocrds]

as


select 

 LocalPatientIdMother
,SubMitted = 1

from Maternity.MAT001MothersDemographics MothersEncounter

where 




	NHSNumberMother is not null
	and
	not exists
	(
		select 

		 NHSNumberMother

		from WarehouseReportingMerged.Maternity.MAT001MothersDemographics Duplicate

		where 
		Duplicate.NHSNumberMother is not null
		and
		Duplicate.NHSNumberMother = MothersEncounter.NHSNumberMother
		group by Duplicate.NHSNumberMother

		having count (*) > 1
	)
	and
	exists
		(
		Select 1 

		from Maternity.MAT003GPPracticeRegistration MAT003GPPracticeRegistration
		where 
			MAT003GPPracticeRegistration.LocalPatientIdMother = MothersEncounter.LocalPatientIdMother
		)
	and
	exists
		(
		Select 1 

		from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails
		where 
			MAT101BookingAppointmentDetails.LocalPatientIdMother = MothersEncounter.LocalPatientIdMother
		)	
		
union all

select 

 LocalPatientIdMother
,SubMitted = 0

from Maternity.MAT001MothersDemographics MothersEncounter

where 




	NHSNumberMother is null
	or
	 exists
	(
		select 

		 NHSNumberMother

		from WarehouseReportingMerged.Maternity.MAT001MothersDemographics Duplicate

		where 
		Duplicate.NHSNumberMother is not null
		and
		Duplicate.NHSNumberMother = MothersEncounter.NHSNumberMother
		group by Duplicate.NHSNumberMother

		having count (*) > 1
	)
	or
	not exists
		(
		Select 1 

		from Maternity.MAT003GPPracticeRegistration MAT003GPPracticeRegistration
		where 
			MAT003GPPracticeRegistration.LocalPatientIdMother = MothersEncounter.LocalPatientIdMother
		)
	or
	not exists
		(
		Select 1 

		from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails
		where 
			MAT101BookingAppointmentDetails.LocalPatientIdMother = MothersEncounter.LocalPatientIdMother
		)			
		
		

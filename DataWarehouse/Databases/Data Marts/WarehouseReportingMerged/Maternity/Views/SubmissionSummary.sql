﻿








CREATE view [Maternity].[SubmissionSummary]

as 

select

		  Type
		 ,TotalSent = max(TotalSent)
		 ,TotalNotSent = max(TotalNotSent)
		 ,SubMissionPeriod 

from

(

select
		  Type
		 ,TotalSent = Count (*)
		 ,TotalNotSent = 1
		 ,SubMissionPeriod = ( Select max(SubMissionPeriod) from Maternity.MAT001MothersDemographics )

		from
		(

			select Type = 'MAT001MothersDemographics', LocalPatientIdMother  from Maternity.MAT001MothersDemographics Encounter 
			union all
			select Type = 'MAT003GPPracticeRegistration', LocalPatientIdMother  from Maternity.MAT003GPPracticeRegistration Encounter 
			union all
			select Type = 'MAT101BookingAppointmentDetails', LocalPatientIdMother  from Maternity.MAT101BookingAppointmentDetails Encounter 
			union all
			select Type = 'MAT310AntenatalAdmission', LocalPatientIdMother  from Maternity.MAT310AntenatalAdmission Encounter 
			union all
			select Type = 'MAT404LabourAndDelivery', LocalPatientIdMother from Maternity.MAT404LabourAndDelivery Encounter 
			union all
			select Type = 'MAT306AntenatalAppointment', LocalPatientIdMother from Maternity.MAT306AntenatalAppointment Encounter 
			
			union all
			select Type = 'MAT502BabysDemographicsandBirthDetails', LocalPatientIdMother 
			from Maternity.MAT502BabysDemographicsandBirthDetails Encounter 
			where 
			not exists
			(
			select NHSNumberBaby from Maternity.MAT502BabysDemographicsandBirthDetails  DemographicsandBirthDetailsCount
			where DemographicsandBirthDetailsCount.NHSNumberBaby = Encounter.NHSNumberBaby
			group by NHSNumberBaby
			having count (*) > 1
			) 
			
		) a
		
		
		where
		--A.type <> 'MAT306AntenatalAppointment'
		--and
		exists
		(
		Select 1
		
		from Maternity.SubmittedReocrds SubmittedReocrds
		where SubmittedReocrds.LocalPatientIdMother = a.LocalPatientIdMother
		and SubmittedReocrds.SubMitted = 1
		
		)

		
		group by Type

union all

select
		  Type
		 ,TotalSent = 0
		 ,TotalNotSent = Count (*)
		 ,SubMissionPeriod = ( Select max(SubMissionPeriod) from Maternity.MAT001MothersDemographics )		 
		from
		(

			select Type = 'MAT001MothersDemographics', LocalPatientIdMother  from Maternity.MAT001MothersDemographics Encounter 
			union all
			select Type = 'MAT003GPPracticeRegistration', LocalPatientIdMother  from Maternity.MAT003GPPracticeRegistration Encounter 
			union all
			select Type = 'MAT101BookingAppointmentDetails', LocalPatientIdMother  from Maternity.MAT101BookingAppointmentDetails Encounter 
			union all
			select Type = 'MAT310AntenatalAdmission', LocalPatientIdMother  from Maternity.MAT310AntenatalAdmission Encounter 
			union all
			select Type = 'MAT404LabourAndDelivery', LocalPatientIdMother from Maternity.MAT404LabourAndDelivery Encounter 
			union all
			select Type = 'MAT306AntenatalAppointment', LocalPatientIdMother from Maternity.MAT306AntenatalAppointment Encounter 
			
			union all
			select Type = 'MAT502BabysDemographicsandBirthDetails', LocalPatientIdMother 
			from Maternity.MAT502BabysDemographicsandBirthDetails Encounter 
			where 
			not exists
			(
			select NHSNumberBaby from Maternity.MAT502BabysDemographicsandBirthDetails  DemographicsandBirthDetailsCount
			where DemographicsandBirthDetailsCount.NHSNumberBaby = Encounter.NHSNumberBaby
			group by NHSNumberBaby
			having count (*) > 1
			) 
			
		) a
		
		
		where
		--A.type = 'MAT306AntenatalAppointment'
		--or
		exists
		(
		Select 1
		
		from Maternity.SubmittedReocrds SubmittedReocrds
		where SubmittedReocrds.LocalPatientIdMother = a.LocalPatientIdMother
		and SubmittedReocrds.SubMitted = 0
		
		)
		
		group by Type
		
) SubmittedCount	


group by Type, 	SubMissionPeriod









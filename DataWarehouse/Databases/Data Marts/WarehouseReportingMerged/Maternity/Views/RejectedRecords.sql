﻿



CREATE view [Maternity].[RejectedRecords] as

SELECT [REPORT_TYPE]
      ,[DESCRIPTION] = replace(DESCRIPTION,'mothermother','Mother')
      ,[CODE]
      ,[NUMERATOR]
      ,[DENOMINATOR]
      ,[DATA_ITEM1]
      ,[DATA_VALUE1]
      ,[DATA_ITEM2]
      ,[DATA_VALUE2]
      ,[DATA_ITEM3]
      ,[DATA_VALUE3]
      ,[DATA_ITEM4]
      ,[DATA_VALUE4]
      ,[DATA_ITEM5]
      ,[DATA_VALUE5]
      ,[DATA_ITEM6]
      ,[DATA_VALUE6]
      ,[DATA_ITEM7]
      ,[DATA_VALUE7]
      ,[DATA_ITEM8]
      ,[DATA_VALUE8]
      ,[DATA_ITEM9]
      ,[DATA_VALUE9]
      ,[DATA_ITEM10]
      ,[DATA_VALUE10]
      ,SubMissionPeriod
  FROM [WarehouseReportingMerged].[Maternity].[Rejected]
  
 union all
 
 --SELECT 



	--   [REPORT_TYPE] = 'Not Submitted'
 --     ,[DESCRIPTION] = 'Antenatal appointments on the same day'
 --     ,[CODE] = 'MAT306'
 --     ,[NUMERATOR] = null
 --     ,[DENOMINATOR] = null
 --     ,[DATA_ITEM1] = 'LocalPatientIdMother'
 --     ,[DATA_VALUE1] = [LocalPatientIdMother]
 --     ,[DATA_ITEM2] = 'AntenatalAppDate'
 --     ,[DATA_VALUE2] = [AntenatalAppDate]
 --     ,[DATA_ITEM3] = null
 --     ,[DATA_VALUE3] = null
 --     ,[DATA_ITEM4] = null
 --     ,[DATA_VALUE4] = null
 --     ,[DATA_ITEM5] = null
 --     ,[DATA_VALUE5] = null
 --     ,[DATA_ITEM6] = null
 --     ,[DATA_VALUE6] = null
 --     ,[DATA_ITEM7] = null
 --     ,[DATA_VALUE7] = null
 --     ,[DATA_ITEM8] = null
 --     ,[DATA_VALUE8] = null
 --     ,[DATA_ITEM9] = null
 --     ,[DATA_VALUE9] = null
 --     ,[DATA_ITEM10] = null
 --     ,[DATA_VALUE10] = null
 --     ,SubMissionPeriod

 -- FROM [WarehouseReportingMerged].[Maternity].[MAT306AntenatalAppointment]
  
 -- group by 
  
	--   [LocalPatientIdMother]
 --     ,[AntenatalAppDate]
 --     ,SubMissionPeriod
      
 --     having count (*) > 1 
 
 --union all
 
 SELECT 



	   [REPORT_TYPE] = 'Not Submitted'
      ,[DESCRIPTION] = 'Duplicate NHS Number Baby'
      ,[CODE] = 'MAT502'
      ,[NUMERATOR] = null
      ,[DENOMINATOR] = null
      ,[DATA_ITEM1] = 'LocalPatientIdBaby'
      ,[DATA_VALUE1] = LocalPatientIdBaby
      ,[DATA_ITEM2] = 'NHSNumberBaby'
      ,[DATA_VALUE2] = NHSNumberBaby
      ,[DATA_ITEM3] = null
      ,[DATA_VALUE3] = null
      ,[DATA_ITEM4] = null
      ,[DATA_VALUE4] = null
      ,[DATA_ITEM5] = null
      ,[DATA_VALUE5] = null
      ,[DATA_ITEM6] = null
      ,[DATA_VALUE6] = null
      ,[DATA_ITEM7] = null
      ,[DATA_VALUE7] = null
      ,[DATA_ITEM8] = null
      ,[DATA_VALUE8] = null
      ,[DATA_ITEM9] = null
      ,[DATA_VALUE9] = null
      ,[DATA_ITEM10] = null
      ,[DATA_VALUE10] = null
      ,SubMissionPeriod

  FROM Maternity.MAT502BabysDemographicsandBirthDetails  Encounter

where
 exists

(
select NHSNumberBaby from Maternity.MAT502BabysDemographicsandBirthDetails  MAT502BabysDemographicsandBirthDetails
where MAT502BabysDemographicsandBirthDetails.NHSNumberBaby = Encounter.NHSNumberBaby
group by MAT502BabysDemographicsandBirthDetails.NHSNumberBaby
having count (*) > 1
)  

union all

SELECT 



	   [REPORT_TYPE] = 'Not Submitted'
      ,[DESCRIPTION] = 'Duplicate NHS Number Mother'
      ,[CODE] = 'MAT502'
      ,[NUMERATOR] = null
      ,[DENOMINATOR] = null
      ,[DATA_ITEM1] = 'LocalPatientIdMother'
      ,[DATA_VALUE1] = LocalPatientIdMother
      ,[DATA_ITEM2] = 'NHSNumberMother'
      ,[DATA_VALUE2] = NHSNumberMother
      ,[DATA_ITEM3] = null
      ,[DATA_VALUE3] = null
      ,[DATA_ITEM4] = null
      ,[DATA_VALUE4] = null
      ,[DATA_ITEM5] = null
      ,[DATA_VALUE5] = null
      ,[DATA_ITEM6] = null
      ,[DATA_VALUE6] = null
      ,[DATA_ITEM7] = null
      ,[DATA_VALUE7] = null
      ,[DATA_ITEM8] = null
      ,[DATA_VALUE8] = null
      ,[DATA_ITEM9] = null
      ,[DATA_VALUE9] = null
      ,[DATA_ITEM10] = null
      ,[DATA_VALUE10] = null
      ,SubMissionPeriod

  FROM Maternity.MAT001MothersDemographics  Encounter

where
 exists

(
select NHSNumberMother from Maternity.MAT001MothersDemographics  MAT001MothersDemographics
where MAT001MothersDemographics.NHSNumberMother = Encounter.NHSNumberMother
group by MAT001MothersDemographics.NHSNumberMother
having count (*) > 1
) 

union all

SELECT 



	   [REPORT_TYPE] = 'Not Submitted'
      ,[DESCRIPTION] = 'Missing Core Section MAT003/MT101' + ' - ' +
      
      		case
      		when NHSNumberMother is null then 'Missing NHS number'
			When 
				not exists
				
				(Select 1 from Maternity.MAT003GPPracticeRegistration  MAT003GPPracticeRegistration where MAT003GPPracticeRegistration.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
					
				and not exists
				(Select 1 from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails where MAT101BookingAppointmentDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				
				then 'Both MAT003 GP and MAT101 Booking Missing'
			When 
				not exists
				
				(Select 1 from Maternity.MAT003GPPracticeRegistration  MAT003GPPracticeRegistration where MAT003GPPracticeRegistration.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				
				then 'MAT003 GP Missing'	
			When 	
				not exists
				(Select 1 from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails where MAT101BookingAppointmentDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				and exists
				(select * from Maternity.MAT306AntenatalAppointment  MAT306AntenatalAppointment where MAT306AntenatalAppointment.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
							
				then 'MAT306AntenatalAppointment MAT101 Booking Missing'
			When 	
				not exists
				(Select 1 from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails where MAT101BookingAppointmentDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				and exists
				(select * from Maternity.MAT310AntenatalAdmission  MAT310AntenatalAdmission where MAT310AntenatalAdmission.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
							
				then 'MAT310AntenatalAdmission MAT101 Booking Missing'	
			When 	
				not exists
				(Select 1 from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails where MAT101BookingAppointmentDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				and exists
				(select * from Maternity.MAT404LabourAndDelivery  MAT404LabourAndDelivery where MAT404LabourAndDelivery.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
							
				then 'MAT404LabourAndDelivery MAT101 Booking Missing'					
			When 	
				not exists
				(Select 1 from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails where MAT101BookingAppointmentDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				and exists
				(select * from Maternity.MAT502BabysDemographicsAndBirthDetails  MAT502BabysDemographicsAndBirthDetails where MAT502BabysDemographicsAndBirthDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
							
				then 'MAT502BabysDemographicsAndBirthDetails MAT101 Booking Missing'				
			
			else 'Not Submitted'
			end
      
      ,[CODE] = 'MAT001'
      ,[NUMERATOR] = null
      ,[DENOMINATOR] = null
      ,[DATA_ITEM1] = 'LocalPatientIdMother'
      ,[DATA_VALUE1] = LocalPatientIdMother
      ,[DATA_ITEM2] = 'Section'
      ,[DATA_VALUE2] = 
		case
			When 
				not exists
				
				(Select 1 from Maternity.MAT003GPPracticeRegistration  MAT003GPPracticeRegistration where MAT003GPPracticeRegistration.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
					
				and not exists
				(Select 1 from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails where MAT101BookingAppointmentDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				
				then 'Both MAT003 GP and MAT101 Booking Missing'
			When 
				not exists
				
				(Select 1 from Maternity.MAT003GPPracticeRegistration  MAT003GPPracticeRegistration where MAT003GPPracticeRegistration.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				
				then 'MAT003 GP Missing'	
			When 	
				not exists
				(Select 1 from Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails where MAT101BookingAppointmentDetails.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother)
				then 'MAT101 Booking Missing'
		
	
			else 'Need to Investigate'
			end
      ,[DATA_ITEM3] = null
      ,[DATA_VALUE3] = null
      ,[DATA_ITEM4] = null
      ,[DATA_VALUE4] = null
      ,[DATA_ITEM5] = null
      ,[DATA_VALUE5] = null
      ,[DATA_ITEM6] = null
      ,[DATA_VALUE6] = null
      ,[DATA_ITEM7] = null
      ,[DATA_VALUE7] = null
      ,[DATA_ITEM8] = null
      ,[DATA_VALUE8] = null
      ,[DATA_ITEM9] = null
      ,[DATA_VALUE9] = null
      ,[DATA_ITEM10] = null
      ,[DATA_VALUE10] = null
      ,SubMissionPeriod

  FROM Maternity.MAT001MothersDemographics  MAT001MothersDemographics
  
where
	exists
			
		(
		Select 1
		
		from Maternity.SubmittedReocrds SubmittedReocrds
		where SubmittedReocrds.LocalPatientIdMother = MAT001MothersDemographics.LocalPatientIdMother
		and SubmittedReocrds.SubMitted = 0
		
		) 

--union all

--SELECT 

--	   [REPORT_TYPE] = 'Not Submitted'
--      ,[DESCRIPTION] = 'AntenatalAppointment Not Being Submitted'
--      ,[CODE] = 'MAT306'
--      ,[NUMERATOR] = null
--      ,[DENOMINATOR] = null
--      ,[DATA_ITEM1] = 'LocalPatientIdMother'
--      ,[DATA_VALUE1] = LocalPatientIdMother
--      ,[DATA_ITEM2] = 'AntenatalAppDate'
--      ,[DATA_VALUE2] = AntenatalAppDate
--      ,[DATA_ITEM3] = null
--      ,[DATA_VALUE3] = null
--      ,[DATA_ITEM4] = null
--      ,[DATA_VALUE4] = null
--      ,[DATA_ITEM5] = null
--      ,[DATA_VALUE5] = null
--      ,[DATA_ITEM6] = null
--      ,[DATA_VALUE6] = null
--      ,[DATA_ITEM7] = null
--      ,[DATA_VALUE7] = null
--      ,[DATA_ITEM8] = null
--      ,[DATA_VALUE8] = null
--      ,[DATA_ITEM9] = null
--      ,[DATA_VALUE9] = null
--      ,[DATA_ITEM10] = null
--      ,[DATA_VALUE10] = null
--      ,SubMissionPeriod
      
--      from Maternity.MAT306AntenatalAppointment


  










﻿




CREATE view [Maternity].[DataQualityRecords] as 

select 

	*
	
from
	(	


SELECT 

	
      DataQuality.Code   
     ,Section = left(DataQuality.Code,6)
     ,DQError =   DataQuality.DESCRIPTION  
     , DistrictNo = 
     case
      when left(DataQuality.CODE,6) = 'MAT404' then DataQuality.DATA_VALUE3
      When left(DataQuality.CODE,6) in ('MAT101','MAT001','MAT003','MATWRN') then DataQuality.DATA_VALUE1
     else '' end
      

	  ,Mat001ValueSent = 
     case
      when Code = 'MAT00126' then  MAT001MothersDemographics.EthnicCategoryMother
      when Code = 'MAT00108' then MAT001MothersDemographics.NHSNumberMother
	  end
      
 	  ,Mat003ValueSent = 
     case
      when Code = 'MAT00313' then MAT003GPPracticeRegistration.EndDateGMPRegistration
      when Code = 'MAT00306' then MAT003GPPracticeRegistration.OrgCodeGMPMother
      when Code = 'MAT00315' then MAT003GPPracticeRegistration.OrgCodeCommissioner
	
      end     

      ,Mat101ValueSent = 
     case
     
      when Code = 'MAT10144' then 'Not Collected'--MAT101BookingAppointmentDetails.SupportStatusMother
      when Code = 'MAT10150' then 'Not Extrcated'--MAT101BookingAppointmentDetails.ComplexSocialFactorsInd
      when Code = 'MAT10145' then MAT101BookingAppointmentDetails.EmploymentStatusPartner
      when Code = 'MAT10140' then 'Not Extrcated' --= MAT101BookingAppointmentDetails.PregnancyFirstContactCareProfessionalType
      when Code = 'MAT10148' then 'Not Extrcated' --MAT101BookingAppointmentDetails.FolicAcidSupplement
     end
      
      ,Mat404ValueSent = 
     case
     
      when Code = 'MAT40431' then MAT404LabourAndDelivery.ROMReason
      when Code = 'MAT40430' then MAT404LabourAndDelivery.ROMMethod
      when Code = 'MAT40435' then MAT404LabourAndDelivery.LabourOnsetPresentation
      when Code = 'MAT40428' then MAT404LabourAndDelivery.OrgCodePostnatalPathLeadProvider
      when Code = 'MAT40433' then MAT404LabourAndDelivery.PlacentaDeliveryMethod
	 end
  
     ,DataQuality.SubmissionPeriod
      

      
  FROM [WarehouseReportingMerged].[Maternity].[DataQuality] DataQuality

  left join [WarehouseReportingMerged].Maternity.MAT001MothersDemographics MAT001MothersDemographics
  on MAT001MothersDemographics.LocalPatientIdMother = DataQuality.DATA_VALUE1
  and left(DataQuality.CODE,6) = 'MAT001'

  left join [WarehouseReportingMerged].Maternity.MAT003GPPracticeRegistration MAT003GPPracticeRegistration
  on MAT003GPPracticeRegistration.LocalPatientIdMother = DataQuality.DATA_VALUE1
  and left(DataQuality.CODE,6) = 'MAT003'  
  
  left join [WarehouseReportingMerged].Maternity.MAT404LabourAndDelivery MAT404LabourAndDelivery
  on MAT404LabourAndDelivery.LocalPatientIdMother = DataQuality.DATA_VALUE3
  and left(DataQuality.CODE,6) = 'MAT404'
  
  left join [WarehouseReportingMerged].Maternity.MAT101BookingAppointmentDetails MAT101BookingAppointmentDetails
  on MAT101BookingAppointmentDetails.LocalPatientIdMother = DataQuality.DATA_VALUE1
  and left(DataQuality.CODE,6) = 'MAT101'
  
 --where 
--not (
--left(DataQuality.CODE,6) = 'MAT101'
--or
--left(DataQuality.CODE,6) = 'MAT404'
--or 
--left(DataQuality.CODE,6) = 'MAT001' 
-- )
 

 ) DataQuality
 
 --where 
 
 --DataQuality.DistrictNo = '00000973'
 
 --order by
 
 --DataQuality.DistrictNo, code
  
  




﻿CREATE TABLE [Maternity].[MAT003GPPracticeRegistrationSubmitted] (
    [LocalPatientIdMother]     VARCHAR (10) NULL,
    [OrgCodeGMPMother]         VARCHAR (6)  NULL,
    [StartDateGMPRegistration] VARCHAR (10) NULL,
    [EndDateGMPRegistration]   VARCHAR (10) NULL,
    [OrgCodeCommissioner]      VARCHAR (5)  NULL,
    [XMLOutput]                XML          NULL,
    [SubmissionPeriod]         VARCHAR (10) NULL
);


﻿CREATE TABLE [Maternity].[MAT306AntenatalAppointmentSubmitted] (
    [LocalPatientIdMother] VARCHAR (10) NULL,
    [AntenatalAppDate]     VARCHAR (10) NULL,
    [XMLOutput]            XML          NULL,
    [DatasetCode]          VARCHAR (20) NULL,
    [SourceUniqueID]       VARCHAR (50) NULL,
    [SubmissionPeriod]     VARCHAR (10) NULL
);


﻿CREATE TABLE [Maternity].[SubmissionPeriod] (
    [SubmissionPeriod] VARCHAR (14) NOT NULL,
    [FromDate]         DATE         NULL,
    [ToDate]           DATE         NULL,
    [Created]          DATETIME     NULL,
    [Updated]          DATETIME     NULL,
    [ByWhom]           VARCHAR (50) NULL,
    [FreezeDate]       DATE         NULL,
    CONSTRAINT [PK_SubmissionPeriod] PRIMARY KEY CLUSTERED ([SubmissionPeriod] ASC)
);


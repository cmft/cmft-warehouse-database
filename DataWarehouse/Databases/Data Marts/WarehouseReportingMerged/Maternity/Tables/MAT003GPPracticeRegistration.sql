﻿CREATE TABLE [Maternity].[MAT003GPPracticeRegistration] (
    [LocalPatientIdMother]     VARCHAR (10) NULL,
    [OrgCodeGMPMother]         VARCHAR (6)  NULL,
    [StartDateGMPRegistration] VARCHAR (10) NULL,
    [EndDateGMPRegistration]   VARCHAR (10) NULL,
    [OrgCodeCommissioner]      VARCHAR (5)  NULL,
    [XMLoutput]                XML          NULL,
    [SubmissionPeriod]         VARCHAR (10) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_MAT003GPPracticeRegistration_LocalPatientIdMother]
    ON [Maternity].[MAT003GPPracticeRegistration]([LocalPatientIdMother] ASC);


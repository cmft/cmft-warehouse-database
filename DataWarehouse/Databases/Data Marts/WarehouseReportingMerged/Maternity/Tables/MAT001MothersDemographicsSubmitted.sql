﻿CREATE TABLE [Maternity].[MAT001MothersDemographicsSubmitted] (
    [LocalPatientIdMother]        VARCHAR (10) NULL,
    [OrgCodeLocalPatientIdMother] VARCHAR (5)  NULL,
    [OrgCodeRes]                  VARCHAR (3)  NULL,
    [NHSNumberMother]             VARCHAR (10) NULL,
    [NHSNumberStatusMother]       VARCHAR (2)  NULL,
    [PersonBirthDateMother]       VARCHAR (10) NULL,
    [Postcode]                    VARCHAR (8)  NULL,
    [EthnicCategoryMother]        VARCHAR (2)  NULL,
    [PersonDeathDateTimeMother]   VARCHAR (19) NULL,
    [XMLOutput]                   XML          NULL,
    [SubmissionPeriod]            VARCHAR (10) NULL
);


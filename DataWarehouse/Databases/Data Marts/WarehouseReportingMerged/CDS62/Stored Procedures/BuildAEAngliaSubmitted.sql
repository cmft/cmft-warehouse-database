﻿CREATE procedure [CDS62].[BuildAEAngliaSubmitted]
	@SubmittedDate date = null
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare @RowsInserted Int
declare @Created datetime

select
	@SubmittedDate =
		coalesce(
			 @SubmittedDate
			,getdate()
		)

	,@Created = GETDATE()

insert
into
	CDS62.AEAngliaSubmitted
(
	 PrimeRecipient
	,CopyRecipient1
	,CopyRecipient2
	,CopyRecipient3
	,CopyRecipient4
	,CopyRecipient5
	,Sender
	,CDSGroup
	,CDSType
	,CDSId
	,TestFlag
	,DatetimeCreated
	,UpdateType
	,ProtocolIdentifier
	,BulkStart
	,BulkEnd
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientName
	,PatientAddress
	,Postcode
	,PCTofResidence
	,DateOfBirth
	,SexCode
	,CarerSupportIndicator
	,EthnicCategoryCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,DepartmentTypeCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,ICDDiagnosisSchemeCode
	,ICDDiagnosisCodeFirst
	,ICDDiagnosisCodeSecond
	,ReadDiagnosisSchemeCode
	,ReadDiagnosisCodeFirst
	,ReadDiagnosisCodeSecond
	,DiagnosisSchemeCode
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,DiagnosisCode1
	,DiagnosisCode2
	,DiagnosisCode3
	,DiagnosisCode4
	,DiagnosisCode5
	,DiagnosisCode6
	,DiagnosisCode7
	,DiagnosisCode8
	,DiagnosisCode9
	,DiagnosisCode10
	,DiagnosisCode11
	,DiagnosisCode12
	,InvestigationSchemeCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,InvestigationCode1
	,InvestigationCode2
	,InvestigationCode3
	,InvestigationCode4
	,InvestigationCode5
	,InvestigationCode6
	,InvestigationCode7
	,InvestigationCode8
	,InvestigationCode9
	,InvestigationCode10
	,OPCSProcedureSchemeCode
	,OPCSPrimaryProcedureCode
	,OPCSPrimaryProcedureDate
	,OPCSPrimaryProcedureCodeSecond
	,OPCSPrimaryProcedureDateSecond
	,ReadProcedureSchemeCode
	,ReadPrimaryProcedureCode
	,ReadPrimaryProcedureDate
	,ReadPrimaryProcedureCodeSecond
	,ReadPrimaryProcedureDateSecond
	,TreatmentSchemeCode
	,TreatmentCodeFirst
	,TreatmentDateFirst
	,TreatmentCodeSecond
	,TreatmentDateSecond
	,TreatmentCode1
	,TreatmentDate1
	,TreatmentCode2
	,TreatmentDate2
	,TreatmentCode3
	,TreatmentDate3
	,TreatmentCode4
	,TreatmentDate4
	,TreatmentCode5
	,TreatmentDate5
	,TreatmentCode6
	,TreatmentDate6
	,TreatmentCode7
	,TreatmentDate7
	,TreatmentCode8
	,TreatmentDate8
	,TreatmentCode9
	,TreatmentDate9
	,TreatmentCode10
	,TreatmentDate10

	,ResidenceResponsibilityCode
	,TreatmentSiteCode
	,DiagnosisFirstPresentOnAdmissionIndicator
	,DiagnosisSecondPresentOnAdmissionIndicator
	,Diagnosis1PresentOnAdmissionIndicator
	,Diagnosis2PresentOnAdmissionIndicator
	,Diagnosis3PresentOnAdmissionIndicator
	,Diagnosis4PresentOnAdmissionIndicator
	,Diagnosis5PresentOnAdmissionIndicator
	,Diagnosis6PresentOnAdmissionIndicator
	,Diagnosis7PresentOnAdmissionIndicator
	,Diagnosis8PresentOnAdmissionIndicator
	,Diagnosis9PresentOnAdmissionIndicator
	,Diagnosis10PresentOnAdmissionIndicator
	,Diagnosis11PresentOnAdmissionIndicator
	,Diagnosis12PresentOnAdmissionIndicator
	,MainOperatingProfessionalCode
	,MainOperatingProfessionalRegistrationIssuerCode
	,ResponsibleAnaesthetistProfessionalCode
	,ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,WaitingTimeMeasurementTypeCode
	,WithheldIdentityReasonCode
	,InitialAssessmentDate
	,SeenForTreatmentDate
	,AttendanceConclusionDate
	,DepartureDate
	,CDSOverseasVisitorStatusClassificationCode
	,AmbulanceIncidentNumber
	,ConveyingAmbulanceTrustCode

	,SubmittedDate
	,Created
	,ByWhom
)
select
	 PrimeRecipient
	,CopyRecipient1
	,CopyRecipient2
	,CopyRecipient3
	,CopyRecipient4
	,CopyRecipient5
	,Sender
	,CDSGroup
	,CDSType
	,CDSId
	,TestFlag
	,DatetimeCreated
	,UpdateType
	,ProtocolIdentifier
	,BulkStart
	,BulkEnd
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientName
	,PatientAddress
	,Postcode
	,PCTofResidence
	,DateOfBirth
	,SexCode
	,CarerSupportIndicator
	,EthnicCategoryCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,DepartmentTypeCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,ICDDiagnosisSchemeCode
	,ICDDiagnosisCodeFirst
	,ICDDiagnosisCodeSecond
	,ReadDiagnosisSchemeCode
	,ReadDiagnosisCodeFirst
	,ReadDiagnosisCodeSecond
	,DiagnosisSchemeCode
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,DiagnosisCode1
	,DiagnosisCode2
	,DiagnosisCode3
	,DiagnosisCode4
	,DiagnosisCode5
	,DiagnosisCode6
	,DiagnosisCode7
	,DiagnosisCode8
	,DiagnosisCode9
	,DiagnosisCode10
	,DiagnosisCode11
	,DiagnosisCode12
	,InvestigationSchemeCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,InvestigationCode1
	,InvestigationCode2
	,InvestigationCode3
	,InvestigationCode4
	,InvestigationCode5
	,InvestigationCode6
	,InvestigationCode7
	,InvestigationCode8
	,InvestigationCode9
	,InvestigationCode10
	,OPCSProcedureSchemeCode
	,OPCSPrimaryProcedureCode
	,OPCSPrimaryProcedureDate
	,OPCSPrimaryProcedureCodeSecond
	,OPCSPrimaryProcedureDateSecond
	,ReadProcedureSchemeCode
	,ReadPrimaryProcedureCode
	,ReadPrimaryProcedureDate
	,ReadPrimaryProcedureCodeSecond
	,ReadPrimaryProcedureDateSecond
	,TreatmentSchemeCode
	,TreatmentCodeFirst
	,TreatmentDateFirst
	,TreatmentCodeSecond
	,TreatmentDateSecond
	,TreatmentCode1
	,TreatmentDate1
	,TreatmentCode2
	,TreatmentDate2
	,TreatmentCode3
	,TreatmentDate3
	,TreatmentCode4
	,TreatmentDate4
	,TreatmentCode5
	,TreatmentDate5
	,TreatmentCode6
	,TreatmentDate6
	,TreatmentCode7
	,TreatmentDate7
	,TreatmentCode8
	,TreatmentDate8
	,TreatmentCode9
	,TreatmentDate9
	,TreatmentCode10
	,TreatmentDate10

	,ResidenceResponsibilityCode
	,TreatmentSiteCode
	,DiagnosisFirstPresentOnAdmissionIndicator
	,DiagnosisSecondPresentOnAdmissionIndicator
	,Diagnosis1PresentOnAdmissionIndicator
	,Diagnosis2PresentOnAdmissionIndicator
	,Diagnosis3PresentOnAdmissionIndicator
	,Diagnosis4PresentOnAdmissionIndicator
	,Diagnosis5PresentOnAdmissionIndicator
	,Diagnosis6PresentOnAdmissionIndicator
	,Diagnosis7PresentOnAdmissionIndicator
	,Diagnosis8PresentOnAdmissionIndicator
	,Diagnosis9PresentOnAdmissionIndicator
	,Diagnosis10PresentOnAdmissionIndicator
	,Diagnosis11PresentOnAdmissionIndicator
	,Diagnosis12PresentOnAdmissionIndicator
	,MainOperatingProfessionalCode
	,MainOperatingProfessionalRegistrationIssuerCode
	,ResponsibleAnaesthetistProfessionalCode
	,ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,WaitingTimeMeasurementTypeCode
	,WithheldIdentityReasonCode
	,InitialAssessmentDate
	,SeenForTreatmentDate
	,AttendanceConclusionDate
	,DepartureDate
	,CDSOverseasVisitorStatusClassificationCode
	,AmbulanceIncidentNumber
	,ConveyingAmbulanceTrustCode

	,SubmittedDate = @SubmittedDate
	,Created = @Created
	,ByWhom = system_user
from
	CDS62.AEAngliaNet Submitted

select @RowsInserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + 
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿CREATE procedure [CDS62].[BuildAngliaType060]  --OP
	 @CDSTestIndicator char(1) = ''
	,@CDSProtocolIdentifier char(3) = '020' -- bulk
as

declare @CDSReportPeriodStartDate smalldatetime
declare @CDSReportPeriodEndDate smalldatetime

select
	 @CDSReportPeriodStartDate = min(dateadd(day, datediff(day, 0, Encounter.AppointmentDate), 0))
	,@CDSReportPeriodEndDate = max(dateadd(day, datediff(day, 0, Encounter.AppointmentDate), 0))
from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join CDS62.wrkCDS
on	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	wrkCDS.CDSTypeCode in ('020')


truncate table CDS62.OPAngliaBase

insert into CDS62.OPAngliaBase
(
	 MergeEncounterRecno
	,UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,NameFormatCode
	,PatientsAddress
	,Postcode
	,HAofResidenceCode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,DateOfBirthStatus
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,ReferralCode
	,ReferringOrganisationCode
	,ServiceTypeRequested
	,ReferralRequestReceivedDate
	,ReferralRequestReceivedDateStatus
	,PriorityType
	,SourceOfReferralCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,LocalSubSpecialtyCode
	,ClinicPurpose
	,ConsultantCode
	,AttendanceIdentifier
	,AdminCategoryCode
	,SiteCode
	,MedicalStaffTypeCode
	,AttendanceDate
	,FirstAttendanceFlag
	,DNAFlag
	,AttendanceOutcomeCode
	,LastDNAorPatientCancelledDate
	,LastDNAorPatientCancelledDateStatus
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,ReadPrimaryDiagnosisCode
	,ReadSecondaryDiagnosisCode1
	,OperationStatus
	,PrimaryOperationCode
	,OperationCode2
	,OperationCode3
	,OperationCode4
	,OperationCode5
	,OperationCode6
	,OperationCode7
	,OperationCode8
	,OperationCode9
	,OperationCode10
	,OperationCode11
	,OperationCode12
	,ReadPrimaryOperationCode
	,ReadOperationCode2
	,ReadOperationCode3
	,ReadOperationCode4
	,ReadOperationCode5
	,ReadOperationCode6
	,ReadOperationCode7
	,ReadOperationCode8
	,ReadOperationCode9
	,ReadOperationCode10
	,ReadOperationCode11
	,ReadOperationCode12
	,PatientSexAgeMix
	,IntendedClinicalCareIntensity
	,BroadPatientGroup
	,CasenoteNumber
	,EthnicGroupCode
	,AttendanceCategoryCode
	,AppointmentRequiredDate
	,MaritalStatusCode
	,GPDiagnosisCode
	,ReadGPDiagnosisCode
	,PrimaryOperationGroupCode
	,SecondaryOperationGroupCode1
	,SecondaryOperationGroupCode2
	,SecondaryProcedureGroupCode3
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,ReadSecondaryDiagnosisCode2
	,ReadSecondaryDiagnosisCode3
	,ReadSecondaryDiagnosisCode4
	,FundingType
	,GPFundholderCode
	,AppointmentHospitalCode
	,ReferralHospitalCode
	,NNNStatusIndicator
	,BlankPad
	,CancellationDate
	,CancelledBy
	,DeathIndicator
	,LocalClinicCode
	,LocalWardCode
	,PCTHealthAuthorityCode
	,PCTofResidenceCode
	,ResponsiblePCTHealthAuthorityCode
	,CommissionerCode
	,AgencyActingOnBehalfOfDoH
	,DerivedFirstAttendance
	,PCTResponsible
	,AgeAtCDSActivityDate
	,UniqueBookingReferenceNumber
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,EarliestReasonableOfferDate
	,EndOfRecord

	,ResidenceResponsibilityCode
	,MultiDisciplinaryIndicationCode
	,RehabilitationAssessmentTeamTypeCode
	,ConsultationMediumUsedCode
	,DirectAccessReferralIndicatorCode
	,PrimaryDiagnosisPresentOnAdmissionIndicator
	,SecondaryDiagnosis1PresentOnAdmissionIndicator
	,SecondaryDiagnosis2PresentOnAdmissionIndicator
	,SecondaryDiagnosis3PresentOnAdmissionIndicator
	,SecondaryDiagnosis4PresentOnAdmissionIndicator
	,MainOperatingProfessionalCode
	,MainOperatingProfessionalRegistrationIssuerCode
	,ResponsibleAnaesthetistProfessionalCode
	,ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,WaitingTimeMeasurementTypeCode
	,ActivityLocationTypeCode
	,EarliestClinicallyAppropriateDate
	,WithheldIdentityReasonCode
	,AppointmentTime
	,ExpectedDurationOfAppointment
	,ClinicCode
	,CDSOverseasVisitorStatusClassificationCode
)
select
	Encounter.MergeEncounterRecno

	,UniqueEpisodeSerialNo =
		'BRW300' +
		case
		when Encounter.ContextCode like 'TRA||%' then coalesce(Encounter.SourceUniqueID , null)
		when Encounter.ContextCode like 'CEN||%' then
			cast(Encounter.SourcePatientNo as varchar) + 
			'*' + 
			CAST(Encounter.SourceEncounterNo as varchar) + 
			'*' +
			convert(varchar , Encounter.AppointmentDate , 112) +
			'*' +
			cast(
				ROW_NUMBER() over 
					(
					partition by 
						 Encounter.SourcePatientNo
						,Encounter.SourceEncounterNo
						,cast(Encounter.AppointmentTime as date)
					order by
						 Encounter.AppointmentTime
						,Encounter.DoctorCode
					)
				as varchar
			)
		end

	,UpdateType = '9' --all records are either new or updated

	,CDSUpdateDate =
		CONVERT(varchar, getdate(), 112)

	,CDSUpdateTime =
		replace(
			 CONVERT(varchar, getdate(), 108)
			,':'
			,''
		)

	,CDSType = @CDSProtocolIdentifier

	,ProviderCode =
		 'RW300'
	,PurchaserCode =
		
		--Gareth Cunnah 18/02/2012
		case
		--Added as temp fix for Welsh patients
		when left(Encounter.PurchaserCode , 1) = '7'  then '7A5HC'
		--Specialist commissioning
		when left(Encounter.PurchaserCode , 1)  = 'Q' then Encounter.PurchaserCode
		--else left(Encounter.PurchaserCode , 5)
		else Encounter.CCGCode
		end

	,CommissioningSerialNo =
		Encounter.CommissioningSerialNo

	,NHSServiceAgreementLineNo = 
		
		--Added by gareth Cunnah 16/08/2013
		--specialised commissioning
	
		Case 
			when left(PurchaserCode,1) = 'Q' then  Encounter.CCGCode
			else null
		end

	,PurchaserReferenceNo = null

	,NHSNumber =
		case
		when replace(Encounter.NHSNumber , ' ' , '') in ('1111111111' , '8888888888') then null
		else replace(Encounter.NHSNumber , ' ' , '')
		end
		
	,PatientName = 
		case
		when Encounter.NHSNumber IS NULL --NHSNumberStatus.NationalValueCode <> '01' AND  Encounter.NHSNumber IS NULL
		then ltrim(rtrim(coalesce(Encounter.PatientForename, '') + ' ' + coalesce(Encounter.PatientSurname, '')))
		else null
		end

	,NameFormatCode = '1'

	,PatientAddress =
		case

		when Encounter.NHSNumber IS NULL --NHSNumberStatus.NationalValueCode <> '01' AND  Encounter.NHSNumber IS NULL
		then
			coalesce(Encounter.PatientAddress1 + ' ', '') + 
			coalesce(Encounter.PatientAddress2 + ' ', '') + 
			coalesce(Encounter.PatientAddress3 + ' ', '') + 
			coalesce(Encounter.PatientAddress4, '')
		else null
		end

	,Postcode = 
			case
			when Encounter.Postcode IS NULL THEN 'ZZ99 3WZ'
			when ltrim(rtrim(Encounter.Postcode)) =  '' THEN 'ZZ99 3WZ'
			when len(Encounter.Postcode) = 8 then Encounter.Postcode
			else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
			end

	,HAofResidenceCode =
		Encounter.PCTCode

	,SexCode = 
		case
		when Sex.NationalValueCode = 'N||SEX' then '9'
		else Sex.NationalValueCode 
		end		
	
	,CarerSupportIndicator = null

	,DateOfBirth =
		CONVERT(varchar, Encounter.DateOfBirth, 112)

	,DateOfBirthStatus = 
		case
		when Encounter.DateOfBirth is null
		then null
		else '1'
		end

	,RegisteredGpCode =	
			COALESCE
					(
					 Encounter.RegisteredGpAtAppointmentCode
					,Encounter.RegisteredGpCode
					,'G9999998'
			)

	,RegisteredGpPracticeCode = 
			COALESCE
					(
					 Encounter.RegisteredGpPracticeAtAppointmentCode
					,Encounter.RegisteredGpPracticeCode
					,'V81999'
			)
	,Encounter.DistrictNo

	,ReferrerCode = 
		coalesce(
			case
			when Encounter.ReferredByCode = 'GP' 
				then convert(varchar, Encounter.ReferredByGpCode)
			when Encounter.ReferredByCode = 'GDP'
				then convert(varchar, Encounter.ReferredByGdpCode)
			when Encounter.ReferredByCode = 'CONS'
				then 
					case 
					when  ReferredtoConsultant.NationalConsultantCode = 'N||CONSUL' then 'X9999998'
					else ReferredtoConsultant.NationalConsultantCode
					end
			else 'X9999998'
			end
			,'X9999998'
		)

	,ReferringOrganisationCode =
			left(
			coalesce(
				case
				when Encounter.ReferredByCode = 'GP'
					then ReferringGpOrganisation.ParentOrganisationCode
				when Encounter.ReferredByCode = 'GDP'
					then 'V81998'
				when Encounter.ReferredByCode = 'CONS'
					then ReferringConsultant.ProviderCode
				else 'X99998'
				end
				,'X99998'
			)
			,6
		)

	,ServiceTypeRequested = 
		ReasonForReferral.NationalValueCode
		
	,ReferralRequestReceivedDate =
		CONVERT(varchar, Encounter.ReferralDate, 112)

	,ReferralRequestReceivedDateStatus =
		case
		when Encounter.ReferralDate is null
		then null
		else '1'
		end

	,PriorityType =
		case
		when Priority.NationalValueCode = 'N||PRITYP' then '1'
		else Priority.NationalValueCode  
		end
	
	,SourceOfReferralCode = 
		case
		when SourceOfReferral.NationalValueCode = 'N||SCRFOP' then '97'
		else SourceOfReferral.NationalValueCode
		end
		
	,MainSpecialtyCode = 
		CASE
		when ReferredtoConsultant.NationalConsultantCode IS NULL THEN 
			case
				when Specialty.NationalValueCode = 'N||SPEC' then '999'
				else Specialty.NationalValueCode
			end
		when ReferredtoConsultant.MainSpecialtyCode = 'N||SPEC' then '999'
		else ReferredtoConsultant.MainSpecialtyCode
		END
		
	,TreatmentFunctionCode =
		case
		when TreatmentFunction.NationalValueCode = 'N||SPEC' then '999'
		else TreatmentFunction.NationalValueCode
		end
				
	,LocalSubSpecialtyCode =
		case
		when LocalSpecialty.LocalValueCode = 'L||SPEC' then '999'
		else LocalSpecialty.LocalValueCode
		end

	,ClinicPurpose =
		null -- Need to include as clinic attribute (similar to consultant issue)
		--Clinic.FunctionCode

	,ConsultantCode =
		case 
		when ReferredtoConsultant.NationalConsultantCode = 'N||CONSUL' then 'C9999998'
		else ReferredtoConsultant.NationalConsultantCode
		end
		

	,AttendanceIdentifier =
		AttendanceIdentifier

	,AdminCategoryCode = 
		AdminCategory.NationalValueCode
	
	,SiteCode =
		[Site].NationalValueCode
		
	,MedicalStaffTypeCode = 
		COALESCE(
				 MedicalStaffTypeCode 
				,'08'
		)

	,AttendanceDate =
		CONVERT(varchar, Encounter.AppointmentDate, 112)

	,FirstAttendance = 
		FirstAttendance.NationalValueCode

	,DNAFlag =
		DNA.NationalValueCode

	,AttendanceOutcomeCode =
		case
			when OutcomeOfAttendance.NationalValueCode = 'N||OUTATT' THEN 9
			else OutcomeOfAttendance.NationalValueCode
		end

	,LastDNAorPatientCancelledDate = 
		CONVERT(varchar, Encounter.LastDNAorPatientCancelledDate, 112)

	,LastDNAorPatientCancelledDateStatus =
		case
		when Encounter.LastDNAorPatientCancelledDate is null
		then 8
		else 1
		end

	,PrimaryDiagnosisCode = replace(Encounter.PrimaryDiagnosisCode,'.', '')
	,SecondaryDiagnosisCode1 = replace(Encounter.SecondaryDiagnosisCode1,'.', '')

	,ReadPrimaryDiagnosisCode = null
	,ReadSecondaryDiagnosisCode1 = null

	,OperationStatus =
		case
		when Encounter.PrimaryOperationCode = '##'
		then 8
		else 1
		end
		
	,PrimaryProcedureCode =
		CASE 
		WHEN Encounter.PrimaryOperationCode = '##' THEN null
		ELSE replace(Encounter.PrimaryOperationCode , '.' , '')
		END

	,ProcedureCode2 =
		CASE 
		WHEN Encounter.SecondaryProcedureCode1 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode1 ,'.' , '')
		END

	,ProcedureCode3 =
		CASE 
		WHEN Encounter.SecondaryProcedureCode2 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode2 , '.' , '')
		END

	,ProcedureCode4 = 
		CASE 
		WHEN Encounter.SecondaryProcedureCode3 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode3 , '.' , '')
		END

	,ProcedureCode5 =
		CASE 
		WHEN Encounter.SecondaryProcedureCode4 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode4 , '.' , '')
		END

	,ProcedureCode6 =
		CASE 
		WHEN  Encounter.SecondaryProcedureCode5 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode5 , '.' , '')
		END

	,ProcedureCode7 =
		CASE 
		WHEN  Encounter.SecondaryProcedureCode6 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode6 , '.' , '')
		END

	,ProcedureCode8 = 
		CASE 
		WHEN  Encounter.SecondaryProcedureCode7 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode7 , '.' , '')
		END

	,ProcedureCode9 =
		CASE 
		WHEN  Encounter.SecondaryProcedureCode8 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode8 , '.' , '')
		END
	,ProcedureCode10 = 
		CASE 
		WHEN  Encounter.SecondaryProcedureCode9 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode9 , '.' , '')
		END

	,ProcedureCode11 = 
		CASE 
		WHEN  Encounter.SecondaryProcedureCode10 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode10 , '.' , '')
		END

	,ProcedureCode12 = 
		CASE 
		WHEN  Encounter.SecondaryProcedureCode11 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode11 , '.' , '')
		END

	,ReadPrimaryOperationCode = null
	,ReadOperationCode2 = null
	,ReadOperationCode3 = null
	,ReadOperationCode4 = null
	,ReadOperationCode5 = null
	,ReadOperationCode6 = null
	,ReadOperationCode7 = null
	,ReadOperationCode8 = null
	,ReadOperationCode9 = null
	,ReadOperationCode10 = null
	,ReadOperationCode11 = null
	,ReadOperationCode12 = null

	,PatientSexAgeMix = null --ward attenders only - get from ward table - Local requirement
	,IntendedClinicalCareIntensity = null --ward attenders only - Local requirement
	,BroadPatientGroup = null --ward attenders only - Local requirement

	,CasenoteNumber =
		Encounter.CasenoteNo

	,EthnicGroupCode =
		case
		when EthnicOrigin.NationalValueCode = 'N||ETHCAT' THEN '99'
		else EthnicOrigin.NationalValueCode
		end
		

	,AttendanceCategoryCode =
		AdminCategory.NationalValueCode

	,AppointmentRequiredDate =
		null
		
	,MaritalStatusCode =
		case
		when MaritalStatus.NationalValueCode = 'N||MARSTA' THEN '9'
		else MaritalStatus.NationalValueCode
		end
		

	,GPDiagnosisCode = --Local requirement
		null
		--Encounter.GpDiagnosisCode --get from PAS?

	,ReadGPDiagnosisCode = null

	,PrimaryOperationGroupCode =
		CASE 
		WHEN  Encounter.PrimaryOperationCode = '##' THEN null
		ELSE replace(Encounter.PrimaryOperationCode , '.' , '')
		END
		--PrimaryOperationGroup.OperationGroupCode

	,SecondaryOperationGroupCode1 = 
		CASE 
		WHEN  Encounter.SecondaryProcedureCode1 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode1 , '.' , '')
		END
		--SecondaryOperationGroup1.OperationGroupCode

	,SecondaryOperationGroupCode2 =
		CASE 
		WHEN  Encounter.SecondaryProcedureCode2 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode2 , '.' , '')
		END
		--SecondaryOperationGroup2.OperationGroupCode

	,SecondaryOperationGroupCode3 = 
		CASE 
		WHEN  Encounter.SecondaryProcedureCode1 = '##' THEN null
		ELSE replace(Encounter.SecondaryProcedureCode2 , '.' , '')
		END
		--SecondaryOperationGroup3.OperationGroupCode

	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4

	,ReadSecondaryDiagnosisCode2 = null
	,ReadSecondaryDiagnosisCode3 = null
	,ReadSecondaryDiagnosisCode4 = null

	,FundingType = --how to derive --Local requirement
		null

	,GPFundholderCode =--Local requirement
		null

	,AppointmentHospitalCode =--Local requirement
		right([Site].NationalValueCode , 2)
		--Site.ValueCode

	,ReferralHospitalCode =--Local requirement
		null

	,NNNStatusIndicator =
		case
		when NHSNumberStatus.NationalValueCode = 'N||NHSNST' then '03'
		else NHSNumberStatus.NationalValueCode
		end

	,BlankPad = null

	,CancellationDate = --investigate why blank --Local requirement
		Encounter.AppointmentCancelDate

	,CancelledBy =
		Encounter.CancelledByCode

	,DeathIndicator =
		case
		when Encounter.DateOfDeath is null then 0
		else 1
		end

	,LocalClinicCode =
		NULL
		--left(
		--	case
		--	when Encounter.IsWardAttender is null then null
		--	else Clinic.NationalValueCode
		--	end
		--,8
		--)
	,LocalWardCode =
	null -- DG not in base
		--case
		--when Encounter.IsWardAttender is null
		--then null
		--else Encounter.WardCode
		--end

	,PCTHealthAuthorityCode =
		coalesce(
			PCT.HACode
			,'Q99'
		)

	,PCTofResidenceCode =
		coalesce(
			--Postcode.PCTCode
			Postcode.CCGCode
			,'X98'
		)

	,ResponsiblePCTHealthAuthorityCode =
		coalesce(
			PCTOfResidence.HACode
			,'Q99'
		)

	,CommissionerCode =
		left(Encounter.CommissioningSerialNo,3)

	,AgencyActingOnBehalfOfDoH = --How to derive --Local requirement
		left(Encounter.ContractSerialNo, 5)

	,DerivedFirstAttendanceFlag =
		DerivedFirstAttendance.NationalValueCode

	,PCTResponsibleCode =
		Encounter.PCTCode

	,AgeAtCDSActivityDate = 
		right(
			convert(
				 varchar
				,datediff(
					 yy
					,DateOfBirth
					,AppointmentDate
				)
				-
				case 
				when
						datepart(m, DateOfBirth) > datepart(m, AppointmentDate)
					or	(
							datepart(m, DateOfBirth) = datepart(m, AppointmentDate) 
						and	datepart(d, DateOfBirth) > datepart(d, AppointmentDate)
						) then 1 
				else 0 
				end
			)
			,3
		)

	,UniqueBookingReferenceNumber =
		null

	,RTTPathwayID =
		Right(
		'00000000000000000000' + 
								replace(
									ltrim(
										rtrim(
											Encounter.RTTPathwayID
										)
									)
									,' '
									, ''
								)
		,20
		)
	

	,RTTCurrentProviderCode = 
		CASE
			WHEN Encounter.RTTPathwayID IS NULL THEN NULL
			WHEN left(Encounter.RTTPathwayID, 3) = 'X09' THEN 'X09'
			ELSE COALESCE (
							 Encounter.RTTCurrentProviderCode
							,Trust.OrganisationCode
							,'RW300'
				)
		END

	,RTTPeriodStatusCode =
		CASE
		WHEN Encounter.RTTPathwayID IS NULL THEN NULL
		when RTTStatus.NationalValueCode = 'N||RTTSTA'	then '99'
		else RTTStatus.NationalValueCode
		end
		
	,RTTStartDate =
		CONVERT(varchar, Encounter.RTTStartDate, 112)

	,RTTEndDate =
		CONVERT(varchar, Encounter.RTTEndDate, 112)

	,EarliestReasonableOfferDate = null
	,EndOfRecord = null

--6.2 columns
	,ResidenceResponsibilityCode = Encounter.CCGCode
	,MultiDisciplinaryIndicationCode = '9'
	,RehabilitationAssessmentTeamTypeCode = null
	,ConsultationMediumUsedCode = null
	,DirectAccessReferralIndicatorCode = '9'
	,PrimaryDiagnosisPresentOnAdmissionIndicator = null
	,SecondaryDiagnosis1PresentOnAdmissionIndicator = null
	,SecondaryDiagnosis2PresentOnAdmissionIndicator = null
	,SecondaryDiagnosis3PresentOnAdmissionIndicator = null
	,SecondaryDiagnosis4PresentOnAdmissionIndicator = null
	,MainOperatingProfessionalCode = null
	,MainOperatingProfessionalRegistrationIssuerCode = null
	,ResponsibleAnaesthetistProfessionalCode = null
	,ResponsibleAnaesthetistProfessionalRegistrationIssuerCode = null
	,WaitingTimeMeasurementTypeCode = null
	,ActivityLocationTypeCode = null
	,EarliestClinicallyAppropriateDate = null
	,WithheldIdentityReasonCode = null
	,AppointmentTime = left(convert(varchar, AppointmentTime, 14), 8)
	,ExpectedDurationOfAppointment = null
	,ClinicCode = left(Encounter.ClinicCode, 12)
	,CDSOverseasVisitorStatusClassificationCode = Encounter.OverseasStatusFlag

from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join CDS62.wrkCDS --change to inner 
on	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	wrkCDS.CDSTypeCode in ('020')

inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference EncounterReference
on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join Organisation.dbo.Trust Trust
ON Trust.OrganisationCode = Encounter.RTTCurrentProviderCode

left join WarehouseOLAPMergedV2.WH.Member Sex
on	Sex.SourceValueID = EncounterReference.SexID

left join WarehouseOLAPMergedV2.WH.Member SourceOfReferral
on	SourceOfReferral.SourceValueID = EncounterReference.SourceOfReferralID

left join WarehouseOLAPMergedV2.WH.Member Clinic
on	Clinic.SourceValueID = EncounterReference.ClinicID

left join WarehouseOLAPMergedV2.WH.Member OutcomeOfAttendance
on	OutcomeOfAttendance.SourceValueID = EncounterReference.AttendanceOutcomeID

left join WarehouseOLAPMergedV2.WH.Member AdminCategory
on	AdminCategory.SourceValueID = EncounterReference.AdminCategoryID

left join WarehouseOLAPMergedV2.WH.Member Specialty
on	Specialty.SourceValueID = EncounterReference.SpecialtyID

left join WarehouseOLAPMergedV2.WH.Member LocalSpecialty
on	LocalSpecialty.SourceValueID = EncounterReference.SpecialtyID

left join WarehouseOLAPMergedV2.WH.Member FirstAttendance
on	FirstAttendance.SourceValueID = EncounterReference.FirstAttendanceID

left join WarehouseOLAPMergedV2.WH.Member DerivedFirstAttendance
on	DerivedFirstAttendance.SourceValueID = EncounterReference.DerivedFirstAttendanceID

left join WarehouseOLAPMergedV2.WH.Consultant ReferredToConsultant
ON  ReferredToConsultant.SourceConsultantID = EncounterReference.ReferralConsultantID

left join WarehouseOLAPMergedV2.WH.Consultant Consultant
ON	Consultant.SourceConsultantID = EncounterReference.ConsultantID

left join WarehouseOLAPMergedV2.WH.Consultant ReferringConsultant
ON ReferringConsultant.SourceConsultantID = EncounterReference.ReferringConsultantID

left join WarehouseOLAPMergedV2.WH.Member [Site]
on	[Site].SourceValueID = EncounterReference.SiteID

left join WarehouseOLAPMergedV2.WH.Member EthnicOrigin
on	EthnicOrigin.SourceValueID = EncounterReference.EthnicOriginID

left join WarehouseOLAPMergedV2.WH.Member DNA
on	DNA.SourceValueID = EncounterReference.AttendanceStatusID

left join WarehouseOLAPMergedV2.WH.Member MaritalStatus
on	MaritalStatus.SourceValueID = EncounterReference.MaritalStatusID

left join WarehouseOLAPMergedV2.WH.Member [Priority]
on	[Priority].SourceValueID = EncounterReference.PriorityID

left join Warehouse.PAS.Operation PrimaryOperationGroup
on	PrimaryOperationGroup.OperationCode = Encounter.PrimaryOperationCode

left join Warehouse.PAS.Operation SecondaryOperationGroup1
on	SecondaryOperationGroup1.OperationCode = Encounter.SecondaryProcedureCode1

left join Warehouse.PAS.Operation SecondaryOperationGroup2
on	SecondaryOperationGroup2.OperationCode = Encounter.SecondaryProcedureCode2

left join Warehouse.PAS.Operation SecondaryOperationGroup3
on	SecondaryOperationGroup3.OperationCode = Encounter.SecondaryProcedureCode3

left join WarehouseOLAPMergedV2.WH.Member NHSNumberStatus
on	NHSNumberStatus.SourceValueID = EncounterReference.NHSNumberStatusID

left join Organisation.dbo.PCT
on	PCT.OrganisationCode = Encounter.PCTCode

----PCT
--left join Organisation.dbo.Postcode Postcode
--on	Postcode.Postcode =
--		case
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
--		else Encounter.Postcode
--		end

--left join Organisation.dbo.PCT PCTOfResidence
--on	PCTOfResidence.OrganisationCode = Postcode.PCTCode

--CCG
left join Organisation.ODS.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join Organisation.ODS.CCG PCTOfResidence
on	PCTOfResidence.OrganisationCode = Postcode.CCGCode

left join Organisation.dbo.Gp ReferringGpOrganisation
on	ReferringGpOrganisation.OrganisationCode = Encounter.ReferredByGpCode -- DG not in Base
and	Encounter.ReferredByCode = 'GP'

left join WarehouseOLAPMergedV2.WH.Member ReasonForReferral 
on	ReasonForReferral.SourceValueID = EncounterReference.ReasonForReferralID

left join Warehouse.PAS.Doctor
on	Doctor.DoctorCode = Encounter.DoctorCode

left join WarehouseOLAPMergedV2.WH.Member RTTStatus 
on	RTTStatus.SourceValueID = EncounterReference.RTTCurrentStatusID

left join WarehouseOLAPMergedV2.WH.Member TreatmentFunction 
on	TreatmentFunction.SourceValueID = EncounterReference.TreatmentFunctionID

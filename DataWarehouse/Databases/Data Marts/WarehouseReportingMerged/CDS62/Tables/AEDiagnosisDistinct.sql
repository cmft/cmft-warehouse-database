﻿CREATE TABLE [CDS62].[AEDiagnosisDistinct] (
    [ContextCode]      VARCHAR (10) NOT NULL,
    [AESourceUniqueID] VARCHAR (50) NOT NULL,
    [DiagnosisCode]    VARCHAR (71) NULL,
    [SequenceNo]       BIGINT       NOT NULL,
    CONSTRAINT [PK_AEDiagnosisDistinct] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [AESourceUniqueID] ASC, [SequenceNo] ASC)
);


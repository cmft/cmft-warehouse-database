﻿CREATE TABLE [CDS62].[AEInvestigationDistinct] (
    [ContextCode]       VARCHAR (10) NOT NULL,
    [AESourceUniqueID]  VARCHAR (50) NOT NULL,
    [InvestigationCode] VARCHAR (6)  NULL,
    [SequenceNo]        BIGINT       NOT NULL,
    CONSTRAINT [PK_InvestigationDistinct] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [AESourceUniqueID] ASC, [SequenceNo] ASC)
);


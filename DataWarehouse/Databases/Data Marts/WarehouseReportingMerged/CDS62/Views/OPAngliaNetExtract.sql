﻿
CREATE view [CDS62].[OPAngliaNetExtract] as

SELECT
 	 UniqueEpisodeSerialNo = left(UniqueEpisodeSerialNo , 35)
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,PatientsAddress
	,Postcode
	,HAofResidenceCode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,ReferralCode
	,ReferringOrganisationCode
	,ServiceTypeRequested
	,ReferralRequestReceivedDate
	,PriorityType
	,SourceOfReferralCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,LocalSubSpecialtyCode

	,ConsultantCode
	,AttendanceIdentifier
	,AdminCategoryCode
	,SiteCode
	,MedicalStaffTypeCode
	,AttendanceDate

	,FirstAttendanceFlag =
		left(FirstAttendanceFlag , 1)

	,DNAFlag
	,AttendanceOutcomeCode
	,LastDNAorPatientCancelledDate
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode1
	--,ReadPrimaryDiagnosisCode
	--,ReadSecondaryDiagnosisCode1
	,OperationStatus
	,PrimaryOperationCode
	,OperationCode2
	,OperationCode3
	,OperationCode4
	,OperationCode5
	,OperationCode6
	,OperationCode7
	,OperationCode8
	,OperationCode9
	,OperationCode10
	,OperationCode11
	,OperationCode12
	--,ReadPrimaryOperationCode
	--,ReadOperationCode2
	--,ReadOperationCode3
	--,ReadOperationCode4
	--,ReadOperationCode5
	--,ReadOperationCode6
	--,ReadOperationCode7
	--,ReadOperationCode8
	--,ReadOperationCode9
	--,ReadOperationCode10
	--,ReadOperationCode11
	--,ReadOperationCode12
	,EthnicGroupCode
	--,ReadGPDiagnosisCode
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	--,ReadSecondaryDiagnosisCode2
	--,ReadSecondaryDiagnosisCode3
	--,ReadSecondaryDiagnosisCode4
	
	,NNNStatusIndicator
	,PCTHealthAuthorityCode
	,PCTofResidenceCode
	,ResponsiblePCTHealthAuthorityCode

	,CommissionerCode = 
		space(5)
		--CommissionerCode

	,AgencyActingOnBehalfOfDoH = 
		space(5)
		--AgencyActingOnBehalfOfDoH

	,DerivedFirstAttendance =
		left(DerivedFirstAttendance , 1)

	,PCTResponsible
	,AgeAtCDSActivityDate
	,UniqueBookingReferenceNumber
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,EarliestReasonableOfferDate

	,ResidenceResponsibilityCode
	,MultiDisciplinaryIndicationCode
	,RehabilitationAssessmentTeamTypeCode
	,ConsultationMediumUsedCode
	,DirectAccessReferralIndicatorCode
	,PrimaryDiagnosisPresentOnAdmissionIndicator
	,SecondaryDiagnosis1PresentOnAdmissionIndicator
	,SecondaryDiagnosis2PresentOnAdmissionIndicator
	,SecondaryDiagnosis3PresentOnAdmissionIndicator
	,SecondaryDiagnosis4PresentOnAdmissionIndicator
	,MainOperatingProfessionalCode
	,MainOperatingProfessionalRegistrationIssuerCode
	,ResponsibleAnaesthetistProfessionalCode
	,ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,WaitingTimeMeasurementTypeCode
	,ActivityLocationTypeCode
	,EarliestClinicallyAppropriateDate 
	,WithheldIdentityReasonCode
	,AppointmentTime
	,ExpectedDurationOfAppointment
	,ClinicCode
	,CDSOverseasVisitorStatusClassificationCode

	,EndOfRecord

	,RowDelimiterColumn =
		null
from
	CDS62.OPAngliaNet


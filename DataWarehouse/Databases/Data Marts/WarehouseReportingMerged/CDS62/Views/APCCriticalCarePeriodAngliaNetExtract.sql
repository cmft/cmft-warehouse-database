﻿
-- 201505 GC added criteria ActivityCode1 is not null as advised by Tim and Sunquest



CREATE view [CDS62].[APCCriticalCarePeriodAngliaNetExtract] as

select
	 UniqueEpisodeSerialNo = LEFT(UniqueEpisodeSerialNo, 35)
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,RecordType
	,NHSNumber
	,PeriodID
	,StartDate
	,StartTime
	,UnitFunctionCode
	,GestationLength
	,ActivityDate
	,PersonWeight
	,ActivityCode1
	,ActivityCode2
	,ActivityCode3
	,ActivityCode4
	,ActivityCode5
	,ActivityCode6
	,ActivityCode7
	,ActivityCode8
	,ActivityCode9
	,ActivityCode10
	,ActivityCode11
	,ActivityCode12
	,ActivityCode13
	,ActivityCode14
	,ActivityCode15
	,ActivityCode16
	,ActivityCode17
	,ActivityCode18
	,ActivityCode19
	,ActivityCode20
	,HighCostDrugCode1
	,HighCostDrugCode2
	,HighCostDrugCode3
	,HighCostDrugCode4
	,HighCostDrugCode5
	,HighCostDrugCode6
	,HighCostDrugCode7
	,HighCostDrugCode8
	,HighCostDrugCode9
	,HighCostDrugCode10
	,HighCostDrugCode11
	,HighCostDrugCode12
	,HighCostDrugCode13
	,HighCostDrugCode14
	,HighCostDrugCode15
	,HighCostDrugCode16
	,HighCostDrugCode17
	,HighCostDrugCode18
	,HighCostDrugCode19
	,HighCostDrugCode20
	,BlankPad
	,EndDate
	,EndTime
	,EndOfRecord

	,RowDelimiterColumn = cast(null as varchar(12))
from
	CDS62.APCCriticalCarePeriodAngliaNet
where
	ActivityCode1 is not null








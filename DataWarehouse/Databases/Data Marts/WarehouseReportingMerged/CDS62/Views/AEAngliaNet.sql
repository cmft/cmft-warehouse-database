﻿CREATE view [CDS62].[AEAngliaNet] as

--This view is referenced by both the CDS extract view (xxxAngliaNetExtract) and the build submitted procedure (BuildxxxAngiaSubmitted).
--Performance of these could be improved by changing this into a table.

--Updates - UpdateType = '9'
select
	 Base.PrimeRecipient
	,Base.CopyRecipient1
	,Base.CopyRecipient2
	,Base.CopyRecipient3
	,Base.CopyRecipient4
	,Base.CopyRecipient5
	,Base.Sender
	,Base.CDSGroup
	,Base.CDSType
	,Base.CDSId
	,Base.TestFlag
	,Base.DatetimeCreated
	,UpdateType = '9'
	,Base.ProtocolIdentifier
	,Base.BulkStart
	,Base.BulkEnd
	,Base.UniqueBookingReferenceNo
	,Base.PathwayId
	,Base.PathwayIdIssuerCode
	,Base.RTTStatusCode
	,Base.RTTStartDate
	,Base.RTTEndDate
	,Base.DistrictNo
	,Base.DistrictNoOrganisationCode
	,Base.NHSNumber
	,Base.NHSNumberStatusId
	,Base.PatientName
	,Base.PatientAddress
	,Base.Postcode
	,Base.PCTofResidence
	,Base.DateOfBirth
	,Base.SexCode
	,Base.CarerSupportIndicator
	,Base.EthnicCategoryCode
	,Base.RegisteredGpCode
	,Base.RegisteredGpPracticeCode
	,Base.AttendanceNumber
	,Base.ArrivalModeCode
	,Base.AttendanceCategoryCode
	,Base.AttendanceDisposalCode
	,Base.IncidentLocationTypeCode
	,Base.PatientGroupCode
	,Base.SourceOfReferralCode
	,Base.DepartmentTypeCode
	,Base.ArrivalDate
	,Base.ArrivalTime
	,Base.AgeOnArrival
	,Base.InitialAssessmentTime
	,Base.SeenForTreatmentTime
	,Base.AttendanceConclusionTime
	,Base.DepartureTime
	,Base.CommissioningSerialNo
	,Base.NHSServiceAgreementLineNo
	,Base.ProviderReferenceNo
	,Base.CommissionerReferenceNo
	,Base.ProviderCode
	,Base.CommissionerCode
	,Base.StaffMemberCode
	,Base.ICDDiagnosisSchemeCode
	,Base.ICDDiagnosisCodeFirst
	,Base.ICDDiagnosisCodeSecond
	,Base.ReadDiagnosisSchemeCode
	,Base.ReadDiagnosisCodeFirst
	,Base.ReadDiagnosisCodeSecond
	,Base.DiagnosisSchemeCode
	,Base.DiagnosisCodeFirst
	,Base.DiagnosisCodeSecond
	,Base.DiagnosisCode1
	,Base.DiagnosisCode2
	,Base.DiagnosisCode3
	,Base.DiagnosisCode4
	,Base.DiagnosisCode5
	,Base.DiagnosisCode6
	,Base.DiagnosisCode7
	,Base.DiagnosisCode8
	,Base.DiagnosisCode9
	,Base.DiagnosisCode10
	,Base.DiagnosisCode11
	,Base.DiagnosisCode12
	,Base.InvestigationSchemeCode
	,Base.InvestigationCodeFirst
	,Base.InvestigationCodeSecond
	,Base.InvestigationCode1
	,Base.InvestigationCode2
	,Base.InvestigationCode3
	,Base.InvestigationCode4
	,Base.InvestigationCode5
	,Base.InvestigationCode6
	,Base.InvestigationCode7
	,Base.InvestigationCode8
	,Base.InvestigationCode9
	,Base.InvestigationCode10
	,Base.OPCSProcedureSchemeCode
	,Base.OPCSPrimaryProcedureCode
	,Base.OPCSPrimaryProcedureDate
	,Base.OPCSPrimaryProcedureCodeSecond
	,Base.OPCSPrimaryProcedureDateSecond
	,Base.ReadProcedureSchemeCode
	,Base.ReadPrimaryProcedureCode
	,Base.ReadPrimaryProcedureDate
	,Base.ReadPrimaryProcedureCodeSecond
	,Base.ReadPrimaryProcedureDateSecond
	,Base.TreatmentSchemeCode
	,Base.TreatmentCodeFirst
	,Base.TreatmentDateFirst
	,Base.TreatmentCodeSecond
	,Base.TreatmentDateSecond
	,Base.TreatmentCode1
	,Base.TreatmentDate1
	,Base.TreatmentCode2
	,Base.TreatmentDate2
	,Base.TreatmentCode3
	,Base.TreatmentDate3
	,Base.TreatmentCode4
	,Base.TreatmentDate4
	,Base.TreatmentCode5
	,Base.TreatmentDate5
	,Base.TreatmentCode6
	,Base.TreatmentDate6
	,Base.TreatmentCode7
	,Base.TreatmentDate7
	,Base.TreatmentCode8
	,Base.TreatmentDate8
	,Base.TreatmentCode9
	,Base.TreatmentDate9
	,Base.TreatmentCode10
	,Base.TreatmentDate10

	,Base.ResidenceResponsibilityCode
	,Base.TreatmentSiteCode
	,Base.DiagnosisFirstPresentOnAdmissionIndicator
	,Base.DiagnosisSecondPresentOnAdmissionIndicator
	,Base.Diagnosis1PresentOnAdmissionIndicator
	,Base.Diagnosis2PresentOnAdmissionIndicator
	,Base.Diagnosis3PresentOnAdmissionIndicator
	,Base.Diagnosis4PresentOnAdmissionIndicator
	,Base.Diagnosis5PresentOnAdmissionIndicator
	,Base.Diagnosis6PresentOnAdmissionIndicator
	,Base.Diagnosis7PresentOnAdmissionIndicator
	,Base.Diagnosis8PresentOnAdmissionIndicator
	,Base.Diagnosis9PresentOnAdmissionIndicator
	,Base.Diagnosis10PresentOnAdmissionIndicator
	,Base.Diagnosis11PresentOnAdmissionIndicator
	,Base.Diagnosis12PresentOnAdmissionIndicator
	,Base.MainOperatingProfessionalCode
	,Base.MainOperatingProfessionalRegistrationIssuerCode
	,Base.ResponsibleAnaesthetistProfessionalCode
	,Base.ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,Base.WaitingTimeMeasurementTypeCode
	,Base.WithheldIdentityReasonCode
	,Base.InitialAssessmentDate
	,Base.SeenForTreatmentDate
	,Base.AttendanceConclusionDate
	,Base.DepartureDate
	,Base.CDSOverseasVisitorStatusClassificationCode
	,Base.AmbulanceIncidentNumber
	,Base.ConveyingAmbulanceTrustCode

from
	CDS62.AEAngliaBase Base

inner join CDS62.AEAngliaSubmitted Submitted
on	Submitted.AttendanceNumber = Base.AttendanceNumber
and	not exists
	(
	select
		1
	from
		CDS62.AEAngliaSubmitted Previous
	where
		Previous.AttendanceNumber = Base.AttendanceNumber
	and	Previous.Created > Submitted.Created
	)
and	Submitted.UpdateType = '9' --insert/update

where
	not
		(
			isnull(Base.UniqueBookingReferenceNo, '') = isnull(Submitted.UniqueBookingReferenceNo, '')
		and isnull(Base.PathwayId, '') = isnull(Submitted.PathwayId, '')
		and isnull(Base.PathwayIdIssuerCode, '') = isnull(Submitted.PathwayIdIssuerCode, '')
		and isnull(Base.RTTStatusCode, '') = isnull(Submitted.RTTStatusCode, '')
		and isnull(Base.RTTStartDate, '') = isnull(Submitted.RTTStartDate, '')
		and isnull(Base.RTTEndDate, '') = isnull(Submitted.RTTEndDate, '')
		and isnull(Base.DistrictNo, '') = isnull(Submitted.DistrictNo, '')
		and isnull(Base.DistrictNoOrganisationCode, '') = isnull(Submitted.DistrictNoOrganisationCode, '')
		and isnull(Base.NHSNumber, '') = isnull(Submitted.NHSNumber, '')
		and isnull(Base.NHSNumberStatusId, '') = isnull(Submitted.NHSNumberStatusId, '')
		and isnull(Base.PatientName, '') = isnull(Submitted.PatientName, '')
		and isnull(Base.PatientAddress, '') = isnull(Submitted.PatientAddress, '')
		and isnull(Base.Postcode, '') = isnull(Submitted.Postcode, '')
		and isnull(Base.PCTofResidence, '') = isnull(Submitted.PCTofResidence, '')
		and isnull(Base.DateOfBirth, '') = isnull(Submitted.DateOfBirth, '')
		and isnull(Base.SexCode, '') = isnull(Submitted.SexCode, '')
		and isnull(Base.CarerSupportIndicator, '') = isnull(Submitted.CarerSupportIndicator, '')
		and isnull(Base.EthnicCategoryCode, '') = isnull(Submitted.EthnicCategoryCode, '')
		and isnull(Base.RegisteredGpCode, '') = isnull(Submitted.RegisteredGpCode, '')
		and isnull(Base.RegisteredGpPracticeCode, '') = isnull(Submitted.RegisteredGpPracticeCode, '')
		and isnull(Base.AttendanceNumber, '') = isnull(Submitted.AttendanceNumber, '')
		and isnull(Base.ArrivalModeCode, '') = isnull(Submitted.ArrivalModeCode, '')
		and isnull(Base.AttendanceCategoryCode, '') = isnull(Submitted.AttendanceCategoryCode, '')
		and isnull(Base.AttendanceDisposalCode, '') = isnull(Submitted.AttendanceDisposalCode, '')
		and isnull(Base.IncidentLocationTypeCode, '') = isnull(Submitted.IncidentLocationTypeCode, '')
		and isnull(Base.PatientGroupCode, '') = isnull(Submitted.PatientGroupCode, '')
		and isnull(Base.SourceOfReferralCode, '') = isnull(Submitted.SourceOfReferralCode, '')
		and isnull(Base.DepartmentTypeCode, '') = isnull(Submitted.DepartmentTypeCode, '')
		and isnull(Base.ArrivalDate, '') = isnull(Submitted.ArrivalDate, '')
		and isnull(Base.ArrivalTime, '') = isnull(Submitted.ArrivalTime, '')
		and isnull(Base.AgeOnArrival, '') = isnull(Submitted.AgeOnArrival, '')
		and isnull(Base.InitialAssessmentTime, '') = isnull(Submitted.InitialAssessmentTime, '')
		and isnull(Base.SeenForTreatmentTime, '') = isnull(Submitted.SeenForTreatmentTime, '')
		and isnull(Base.AttendanceConclusionTime, '') = isnull(Submitted.AttendanceConclusionTime, '')
		and isnull(Base.DepartureTime, '') = isnull(Submitted.DepartureTime, '')
		and isnull(Base.CommissioningSerialNo, '') = isnull(Submitted.CommissioningSerialNo, '')
		and isnull(Base.NHSServiceAgreementLineNo, '') = isnull(Submitted.NHSServiceAgreementLineNo, '')
		and isnull(Base.ProviderReferenceNo, '') = isnull(Submitted.ProviderReferenceNo, '')
		and isnull(Base.CommissionerReferenceNo, '') = isnull(Submitted.CommissionerReferenceNo, '')
		and isnull(Base.ProviderCode, '') = isnull(Submitted.ProviderCode, '')
		and isnull(Base.CommissionerCode, '') = isnull(Submitted.CommissionerCode, '')
		and isnull(Base.StaffMemberCode, '') = isnull(Submitted.StaffMemberCode, '')
		and isnull(Base.ICDDiagnosisSchemeCode, '') = isnull(Submitted.ICDDiagnosisSchemeCode, '')
		and isnull(Base.ICDDiagnosisCodeFirst, '') = isnull(Submitted.ICDDiagnosisCodeFirst, '')
		and isnull(Base.ICDDiagnosisCodeSecond, '') = isnull(Submitted.ICDDiagnosisCodeSecond, '')
		and isnull(Base.ReadDiagnosisSchemeCode, '') = isnull(Submitted.ReadDiagnosisSchemeCode, '')
		and isnull(Base.ReadDiagnosisCodeFirst, '') = isnull(Submitted.ReadDiagnosisCodeFirst, '')
		and isnull(Base.ReadDiagnosisCodeSecond, '') = isnull(Submitted.ReadDiagnosisCodeSecond, '')
		and isnull(Base.DiagnosisSchemeCode, '') = isnull(Submitted.DiagnosisSchemeCode, '')
		and isnull(Base.DiagnosisCodeFirst, '') = isnull(Submitted.DiagnosisCodeFirst, '')
		and isnull(Base.DiagnosisCodeSecond, '') = isnull(Submitted.DiagnosisCodeSecond, '')
		and isnull(Base.DiagnosisCode1, '') = isnull(Submitted.DiagnosisCode1, '')
		and isnull(Base.DiagnosisCode2, '') = isnull(Submitted.DiagnosisCode2, '')
		and isnull(Base.DiagnosisCode3, '') = isnull(Submitted.DiagnosisCode3, '')
		and isnull(Base.DiagnosisCode4, '') = isnull(Submitted.DiagnosisCode4, '')
		and isnull(Base.DiagnosisCode5, '') = isnull(Submitted.DiagnosisCode5, '')
		and isnull(Base.DiagnosisCode6, '') = isnull(Submitted.DiagnosisCode6, '')
		and isnull(Base.DiagnosisCode7, '') = isnull(Submitted.DiagnosisCode7, '')
		and isnull(Base.DiagnosisCode8, '') = isnull(Submitted.DiagnosisCode8, '')
		and isnull(Base.DiagnosisCode9, '') = isnull(Submitted.DiagnosisCode9, '')
		and isnull(Base.DiagnosisCode10, '') = isnull(Submitted.DiagnosisCode10, '')
		and isnull(Base.DiagnosisCode11, '') = isnull(Submitted.DiagnosisCode11, '')
		and isnull(Base.DiagnosisCode12, '') = isnull(Submitted.DiagnosisCode12, '')
		and isnull(Base.InvestigationSchemeCode, '') = isnull(Submitted.InvestigationSchemeCode, '')
		and isnull(Base.InvestigationCodeFirst, '') = isnull(Submitted.InvestigationCodeFirst, '')
		and isnull(Base.InvestigationCodeSecond, '') = isnull(Submitted.InvestigationCodeSecond, '')
		and isnull(Base.InvestigationCode1, '') = isnull(Submitted.InvestigationCode1, '')
		and isnull(Base.InvestigationCode2, '') = isnull(Submitted.InvestigationCode2, '')
		and isnull(Base.InvestigationCode3, '') = isnull(Submitted.InvestigationCode3, '')
		and isnull(Base.InvestigationCode4, '') = isnull(Submitted.InvestigationCode4, '')
		and isnull(Base.InvestigationCode5, '') = isnull(Submitted.InvestigationCode5, '')
		and isnull(Base.InvestigationCode6, '') = isnull(Submitted.InvestigationCode6, '')
		and isnull(Base.InvestigationCode7, '') = isnull(Submitted.InvestigationCode7, '')
		and isnull(Base.InvestigationCode8, '') = isnull(Submitted.InvestigationCode8, '')
		and isnull(Base.InvestigationCode9, '') = isnull(Submitted.InvestigationCode9, '')
		and isnull(Base.InvestigationCode10, '') = isnull(Submitted.InvestigationCode10, '')
		and isnull(Base.OPCSProcedureSchemeCode, '') = isnull(Submitted.OPCSProcedureSchemeCode, '')
		and isnull(Base.OPCSPrimaryProcedureCode, '') = isnull(Submitted.OPCSPrimaryProcedureCode, '')
		and isnull(Base.OPCSPrimaryProcedureDate, '') = isnull(Submitted.OPCSPrimaryProcedureDate, '')
		and isnull(Base.OPCSPrimaryProcedureCodeSecond, '') = isnull(Submitted.OPCSPrimaryProcedureCodeSecond, '')
		and isnull(Base.OPCSPrimaryProcedureDateSecond, '') = isnull(Submitted.OPCSPrimaryProcedureDateSecond, '')
		and isnull(Base.ReadProcedureSchemeCode, '') = isnull(Submitted.ReadProcedureSchemeCode, '')
		and isnull(Base.ReadPrimaryProcedureCode, '') = isnull(Submitted.ReadPrimaryProcedureCode, '')
		and isnull(Base.ReadPrimaryProcedureDate, '') = isnull(Submitted.ReadPrimaryProcedureDate, '')
		and isnull(Base.ReadPrimaryProcedureCodeSecond, '') = isnull(Submitted.ReadPrimaryProcedureCodeSecond, '')
		and isnull(Base.ReadPrimaryProcedureDateSecond, '') = isnull(Submitted.ReadPrimaryProcedureDateSecond, '')
		and isnull(Base.TreatmentSchemeCode, '') = isnull(Submitted.TreatmentSchemeCode, '')
		and isnull(Base.TreatmentCodeFirst, '') = isnull(Submitted.TreatmentCodeFirst, '')
		and isnull(Base.TreatmentDateFirst, '') = isnull(Submitted.TreatmentDateFirst, '')
		and isnull(Base.TreatmentCodeSecond, '') = isnull(Submitted.TreatmentCodeSecond, '')
		and isnull(Base.TreatmentDateSecond, '') = isnull(Submitted.TreatmentDateSecond, '')
		and isnull(Base.TreatmentCode1, '') = isnull(Submitted.TreatmentCode1, '')
		and isnull(Base.TreatmentDate1, '') = isnull(Submitted.TreatmentDate1, '')
		and isnull(Base.TreatmentCode2, '') = isnull(Submitted.TreatmentCode2, '')
		and isnull(Base.TreatmentDate2, '') = isnull(Submitted.TreatmentDate2, '')
		and isnull(Base.TreatmentCode3, '') = isnull(Submitted.TreatmentCode3, '')
		and isnull(Base.TreatmentDate3, '') = isnull(Submitted.TreatmentDate3, '')
		and isnull(Base.TreatmentCode4, '') = isnull(Submitted.TreatmentCode4, '')
		and isnull(Base.TreatmentDate4, '') = isnull(Submitted.TreatmentDate4, '')
		and isnull(Base.TreatmentCode5, '') = isnull(Submitted.TreatmentCode5, '')
		and isnull(Base.TreatmentDate5, '') = isnull(Submitted.TreatmentDate5, '')
		and isnull(Base.TreatmentCode6, '') = isnull(Submitted.TreatmentCode6, '')
		and isnull(Base.TreatmentDate6, '') = isnull(Submitted.TreatmentDate6, '')
		and isnull(Base.TreatmentCode7, '') = isnull(Submitted.TreatmentCode7, '')
		and isnull(Base.TreatmentDate7, '') = isnull(Submitted.TreatmentDate7, '')
		and isnull(Base.TreatmentCode8, '') = isnull(Submitted.TreatmentCode8, '')
		and isnull(Base.TreatmentDate8, '') = isnull(Submitted.TreatmentDate8, '')
		and isnull(Base.TreatmentCode9, '') = isnull(Submitted.TreatmentCode9, '')
		and isnull(Base.TreatmentDate9, '') = isnull(Submitted.TreatmentDate9, '')
		and isnull(Base.TreatmentCode10, '') = isnull(Submitted.TreatmentCode10, '')
		and isnull(Base.TreatmentDate10, '') = isnull(Submitted.TreatmentDate10, '')

		and isnull(Base.TreatmentDate10, '') = isnull(Submitted.TreatmentDate10, '')
		and isnull(Base.ResidenceResponsibilityCode, '') = isnull(Submitted.ResidenceResponsibilityCode, '')
		and isnull(Base.TreatmentSiteCode, '') = isnull(Submitted.TreatmentSiteCode, '')
		and isnull(Base.DiagnosisFirstPresentOnAdmissionIndicator, '') = isnull(Submitted.DiagnosisFirstPresentOnAdmissionIndicator, '')
		and isnull(Base.DiagnosisSecondPresentOnAdmissionIndicator, '') = isnull(Submitted.DiagnosisSecondPresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis1PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis1PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis2PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis2PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis3PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis3PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis4PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis4PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis5PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis5PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis6PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis6PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis7PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis7PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis8PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis8PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis9PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis9PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis10PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis10PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis11PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis11PresentOnAdmissionIndicator, '')
		and isnull(Base.Diagnosis12PresentOnAdmissionIndicator, '') = isnull(Submitted.Diagnosis12PresentOnAdmissionIndicator, '')
		and isnull(Base.MainOperatingProfessionalCode, '') = isnull(Submitted.MainOperatingProfessionalCode, '')
		and isnull(Base.MainOperatingProfessionalRegistrationIssuerCode, '') = isnull(Submitted.MainOperatingProfessionalRegistrationIssuerCode, '')
		and isnull(Base.ResponsibleAnaesthetistProfessionalCode, '') = isnull(Submitted.ResponsibleAnaesthetistProfessionalCode, '')
		and isnull(Base.ResponsibleAnaesthetistProfessionalRegistrationIssuerCode, '') = isnull(Submitted.ResponsibleAnaesthetistProfessionalRegistrationIssuerCode, '')
		and isnull(Base.WaitingTimeMeasurementTypeCode, '') = isnull(Submitted.WaitingTimeMeasurementTypeCode, '')
		and isnull(Base.WithheldIdentityReasonCode, '') = isnull(Submitted.WithheldIdentityReasonCode, '')
		and isnull(Base.InitialAssessmentDate, '') = isnull(Submitted.InitialAssessmentDate, '')
		and isnull(Base.SeenForTreatmentDate, '') = isnull(Submitted.SeenForTreatmentDate, '')
		and isnull(Base.AttendanceConclusionDate, '') = isnull(Submitted.AttendanceConclusionDate, '')
		and isnull(Base.DepartureDate, '') = isnull(Submitted.DepartureDate, '')
		and isnull(Base.CDSOverseasVisitorStatusClassificationCode, '') = isnull(Submitted.CDSOverseasVisitorStatusClassificationCode, '')
		and isnull(Base.AmbulanceIncidentNumber, '') = isnull(Submitted.AmbulanceIncidentNumber, '')
		and isnull(Base.ConveyingAmbulanceTrustCode, '') = isnull(Submitted.ConveyingAmbulanceTrustCode, '')
		)

union all

--Inserts - UpdateType = '9'
select
	 Base.PrimeRecipient
	,Base.CopyRecipient1
	,Base.CopyRecipient2
	,Base.CopyRecipient3
	,Base.CopyRecipient4
	,Base.CopyRecipient5
	,Base.Sender
	,Base.CDSGroup
	,Base.CDSType
	,Base.CDSId
	,Base.TestFlag
	,Base.DatetimeCreated
	,UpdateType = '9'
	,Base.ProtocolIdentifier
	,Base.BulkStart
	,Base.BulkEnd
	,Base.UniqueBookingReferenceNo
	,Base.PathwayId
	,Base.PathwayIdIssuerCode
	,Base.RTTStatusCode
	,Base.RTTStartDate
	,Base.RTTEndDate
	,Base.DistrictNo
	,Base.DistrictNoOrganisationCode
	,Base.NHSNumber
	,Base.NHSNumberStatusId
	,Base.PatientName
	,Base.PatientAddress
	,Base.Postcode
	,Base.PCTofResidence
	,Base.DateOfBirth
	,Base.SexCode
	,Base.CarerSupportIndicator
	,Base.EthnicCategoryCode
	,Base.RegisteredGpCode
	,Base.RegisteredGpPracticeCode
	,Base.AttendanceNumber
	,Base.ArrivalModeCode
	,Base.AttendanceCategoryCode
	,Base.AttendanceDisposalCode
	,Base.IncidentLocationTypeCode
	,Base.PatientGroupCode
	,Base.SourceOfReferralCode
	,Base.DepartmentTypeCode
	,Base.ArrivalDate
	,Base.ArrivalTime
	,Base.AgeOnArrival
	,Base.InitialAssessmentTime
	,Base.SeenForTreatmentTime
	,Base.AttendanceConclusionTime
	,Base.DepartureTime
	,Base.CommissioningSerialNo
	,Base.NHSServiceAgreementLineNo
	,Base.ProviderReferenceNo
	,Base.CommissionerReferenceNo
	,Base.ProviderCode
	,Base.CommissionerCode
	,Base.StaffMemberCode
	,Base.ICDDiagnosisSchemeCode
	,Base.ICDDiagnosisCodeFirst
	,Base.ICDDiagnosisCodeSecond
	,Base.ReadDiagnosisSchemeCode
	,Base.ReadDiagnosisCodeFirst
	,Base.ReadDiagnosisCodeSecond
	,Base.DiagnosisSchemeCode
	,Base.DiagnosisCodeFirst
	,Base.DiagnosisCodeSecond
	,Base.DiagnosisCode1
	,Base.DiagnosisCode2
	,Base.DiagnosisCode3
	,Base.DiagnosisCode4
	,Base.DiagnosisCode5
	,Base.DiagnosisCode6
	,Base.DiagnosisCode7
	,Base.DiagnosisCode8
	,Base.DiagnosisCode9
	,Base.DiagnosisCode10
	,Base.DiagnosisCode11
	,Base.DiagnosisCode12
	,Base.InvestigationSchemeCode
	,Base.InvestigationCodeFirst
	,Base.InvestigationCodeSecond
	,Base.InvestigationCode1
	,Base.InvestigationCode2
	,Base.InvestigationCode3
	,Base.InvestigationCode4
	,Base.InvestigationCode5
	,Base.InvestigationCode6
	,Base.InvestigationCode7
	,Base.InvestigationCode8
	,Base.InvestigationCode9
	,Base.InvestigationCode10
	,Base.OPCSProcedureSchemeCode
	,Base.OPCSPrimaryProcedureCode
	,Base.OPCSPrimaryProcedureDate
	,Base.OPCSPrimaryProcedureCodeSecond
	,Base.OPCSPrimaryProcedureDateSecond
	,Base.ReadProcedureSchemeCode
	,Base.ReadPrimaryProcedureCode
	,Base.ReadPrimaryProcedureDate
	,Base.ReadPrimaryProcedureCodeSecond
	,Base.ReadPrimaryProcedureDateSecond
	,Base.TreatmentSchemeCode
	,Base.TreatmentCodeFirst
	,Base.TreatmentDateFirst
	,Base.TreatmentCodeSecond
	,Base.TreatmentDateSecond
	,Base.TreatmentCode1
	,Base.TreatmentDate1
	,Base.TreatmentCode2
	,Base.TreatmentDate2
	,Base.TreatmentCode3
	,Base.TreatmentDate3
	,Base.TreatmentCode4
	,Base.TreatmentDate4
	,Base.TreatmentCode5
	,Base.TreatmentDate5
	,Base.TreatmentCode6
	,Base.TreatmentDate6
	,Base.TreatmentCode7
	,Base.TreatmentDate7
	,Base.TreatmentCode8
	,Base.TreatmentDate8
	,Base.TreatmentCode9
	,Base.TreatmentDate9
	,Base.TreatmentCode10
	,Base.TreatmentDate10

	,Base.ResidenceResponsibilityCode
	,Base.TreatmentSiteCode
	,Base.DiagnosisFirstPresentOnAdmissionIndicator
	,Base.DiagnosisSecondPresentOnAdmissionIndicator
	,Base.Diagnosis1PresentOnAdmissionIndicator
	,Base.Diagnosis2PresentOnAdmissionIndicator
	,Base.Diagnosis3PresentOnAdmissionIndicator
	,Base.Diagnosis4PresentOnAdmissionIndicator
	,Base.Diagnosis5PresentOnAdmissionIndicator
	,Base.Diagnosis6PresentOnAdmissionIndicator
	,Base.Diagnosis7PresentOnAdmissionIndicator
	,Base.Diagnosis8PresentOnAdmissionIndicator
	,Base.Diagnosis9PresentOnAdmissionIndicator
	,Base.Diagnosis10PresentOnAdmissionIndicator
	,Base.Diagnosis11PresentOnAdmissionIndicator
	,Base.Diagnosis12PresentOnAdmissionIndicator
	,Base.MainOperatingProfessionalCode
	,Base.MainOperatingProfessionalRegistrationIssuerCode
	,Base.ResponsibleAnaesthetistProfessionalCode
	,Base.ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,Base.WaitingTimeMeasurementTypeCode
	,Base.WithheldIdentityReasonCode
	,Base.InitialAssessmentDate
	,Base.SeenForTreatmentDate
	,Base.AttendanceConclusionDate
	,Base.DepartureDate
	,Base.CDSOverseasVisitorStatusClassificationCode
	,Base.AmbulanceIncidentNumber
	,Base.ConveyingAmbulanceTrustCode
from
	CDS62.AEAngliaBase Base
where
	not exists
	(
	select
		1
	from
		CDS62.AEAngliaSubmitted Submitted
	where
		Submitted.AttendanceNumber = Base.AttendanceNumber
	)

or	exists --latest update type was delete
	(
	select
		1
	from
		CDS62.AEAngliaSubmitted Submitted
	where
		Submitted.AttendanceNumber = Base.AttendanceNumber
	and	Submitted.UpdateType = '1' --delete
	and	not exists
		(
		select
			1
		from
			CDS62.AEAngliaSubmitted Previous
		where
			Previous.AttendanceNumber = Base.AttendanceNumber
		and	Previous.Created > Submitted.Created
		)
	)

union all

--Deletes - UpdateType = '1'
select
	 Submitted.PrimeRecipient
	,Submitted.CopyRecipient1
	,Submitted.CopyRecipient2
	,Submitted.CopyRecipient3
	,Submitted.CopyRecipient4
	,Submitted.CopyRecipient5
	,Submitted.Sender
	,Submitted.CDSGroup
	,Submitted.CDSType
	,Submitted.CDSId
	,Submitted.TestFlag
	,Submitted.DatetimeCreated
	,UpdateType = '1'
	,Submitted.ProtocolIdentifier
	,Submitted.BulkStart
	,Submitted.BulkEnd
	,Submitted.UniqueBookingReferenceNo
	,Submitted.PathwayId
	,Submitted.PathwayIdIssuerCode
	,Submitted.RTTStatusCode
	,Submitted.RTTStartDate
	,Submitted.RTTEndDate
	,Submitted.DistrictNo
	,Submitted.DistrictNoOrganisationCode
	,Submitted.NHSNumber
	,Submitted.NHSNumberStatusId
	,Submitted.PatientName
	,Submitted.PatientAddress
	,Submitted.Postcode
	,Submitted.PCTofResidence
	,Submitted.DateOfBirth
	,Submitted.SexCode
	,Submitted.CarerSupportIndicator
	,Submitted.EthnicCategoryCode
	,Submitted.RegisteredGpCode
	,Submitted.RegisteredGpPracticeCode
	,Submitted.AttendanceNumber
	,Submitted.ArrivalModeCode
	,Submitted.AttendanceCategoryCode
	,Submitted.AttendanceDisposalCode
	,Submitted.IncidentLocationTypeCode
	,Submitted.PatientGroupCode
	,Submitted.SourceOfReferralCode
	,Submitted.DepartmentTypeCode
	,Submitted.ArrivalDate
	,Submitted.ArrivalTime
	,Submitted.AgeOnArrival
	,Submitted.InitialAssessmentTime
	,Submitted.SeenForTreatmentTime
	,Submitted.AttendanceConclusionTime
	,Submitted.DepartureTime
	,Submitted.CommissioningSerialNo
	,Submitted.NHSServiceAgreementLineNo
	,Submitted.ProviderReferenceNo
	,Submitted.CommissionerReferenceNo
	,Submitted.ProviderCode
	,Submitted.CommissionerCode
	,Submitted.StaffMemberCode
	,Submitted.ICDDiagnosisSchemeCode
	,Submitted.ICDDiagnosisCodeFirst
	,Submitted.ICDDiagnosisCodeSecond
	,Submitted.ReadDiagnosisSchemeCode
	,Submitted.ReadDiagnosisCodeFirst
	,Submitted.ReadDiagnosisCodeSecond
	,Submitted.DiagnosisSchemeCode
	,Submitted.DiagnosisCodeFirst
	,Submitted.DiagnosisCodeSecond
	,Submitted.DiagnosisCode1
	,Submitted.DiagnosisCode2
	,Submitted.DiagnosisCode3
	,Submitted.DiagnosisCode4
	,Submitted.DiagnosisCode5
	,Submitted.DiagnosisCode6
	,Submitted.DiagnosisCode7
	,Submitted.DiagnosisCode8
	,Submitted.DiagnosisCode9
	,Submitted.DiagnosisCode10
	,Submitted.DiagnosisCode11
	,Submitted.DiagnosisCode12
	,Submitted.InvestigationSchemeCode
	,Submitted.InvestigationCodeFirst
	,Submitted.InvestigationCodeSecond
	,Submitted.InvestigationCode1
	,Submitted.InvestigationCode2
	,Submitted.InvestigationCode3
	,Submitted.InvestigationCode4
	,Submitted.InvestigationCode5
	,Submitted.InvestigationCode6
	,Submitted.InvestigationCode7
	,Submitted.InvestigationCode8
	,Submitted.InvestigationCode9
	,Submitted.InvestigationCode10
	,Submitted.OPCSProcedureSchemeCode
	,Submitted.OPCSPrimaryProcedureCode
	,Submitted.OPCSPrimaryProcedureDate
	,Submitted.OPCSPrimaryProcedureCodeSecond
	,Submitted.OPCSPrimaryProcedureDateSecond
	,Submitted.ReadProcedureSchemeCode
	,Submitted.ReadPrimaryProcedureCode
	,Submitted.ReadPrimaryProcedureDate
	,Submitted.ReadPrimaryProcedureCodeSecond
	,Submitted.ReadPrimaryProcedureDateSecond
	,Submitted.TreatmentSchemeCode
	,Submitted.TreatmentCodeFirst
	,Submitted.TreatmentDateFirst
	,Submitted.TreatmentCodeSecond
	,Submitted.TreatmentDateSecond
	,Submitted.TreatmentCode1
	,Submitted.TreatmentDate1
	,Submitted.TreatmentCode2
	,Submitted.TreatmentDate2
	,Submitted.TreatmentCode3
	,Submitted.TreatmentDate3
	,Submitted.TreatmentCode4
	,Submitted.TreatmentDate4
	,Submitted.TreatmentCode5
	,Submitted.TreatmentDate5
	,Submitted.TreatmentCode6
	,Submitted.TreatmentDate6
	,Submitted.TreatmentCode7
	,Submitted.TreatmentDate7
	,Submitted.TreatmentCode8
	,Submitted.TreatmentDate8
	,Submitted.TreatmentCode9
	,Submitted.TreatmentDate9
	,Submitted.TreatmentCode10
	,Submitted.TreatmentDate10

	,Submitted.ResidenceResponsibilityCode
	,Submitted.TreatmentSiteCode
	,Submitted.DiagnosisFirstPresentOnAdmissionIndicator
	,Submitted.DiagnosisSecondPresentOnAdmissionIndicator
	,Submitted.Diagnosis1PresentOnAdmissionIndicator
	,Submitted.Diagnosis2PresentOnAdmissionIndicator
	,Submitted.Diagnosis3PresentOnAdmissionIndicator
	,Submitted.Diagnosis4PresentOnAdmissionIndicator
	,Submitted.Diagnosis5PresentOnAdmissionIndicator
	,Submitted.Diagnosis6PresentOnAdmissionIndicator
	,Submitted.Diagnosis7PresentOnAdmissionIndicator
	,Submitted.Diagnosis8PresentOnAdmissionIndicator
	,Submitted.Diagnosis9PresentOnAdmissionIndicator
	,Submitted.Diagnosis10PresentOnAdmissionIndicator
	,Submitted.Diagnosis11PresentOnAdmissionIndicator
	,Submitted.Diagnosis12PresentOnAdmissionIndicator
	,Submitted.MainOperatingProfessionalCode
	,Submitted.MainOperatingProfessionalRegistrationIssuerCode
	,Submitted.ResponsibleAnaesthetistProfessionalCode
	,Submitted.ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,Submitted.WaitingTimeMeasurementTypeCode
	,Submitted.WithheldIdentityReasonCode
	,Submitted.InitialAssessmentDate
	,Submitted.SeenForTreatmentDate
	,Submitted.AttendanceConclusionDate
	,Submitted.DepartureDate
	,Submitted.CDSOverseasVisitorStatusClassificationCode
	,Submitted.AmbulanceIncidentNumber
	,Submitted.ConveyingAmbulanceTrustCode

from
	CDS62.AEAngliaSubmitted Submitted
where
	not exists
	(
	select
		1
	from
		CDS62.AEAngliaBase Base
	where
		Base.AttendanceNumber = Submitted.AttendanceNumber
	)

and	not exists
	(
	select
		1
	from
		CDS62.AEAngliaSubmitted Previous
	where
		Previous.AttendanceNumber = Submitted.AttendanceNumber
	and	Previous.Created > Submitted.Created
	)

and	exists --latest update type was insert/update
	(
	select
		1
	from
		CDS62.AEAngliaSubmitted Submitted9
	where
		Submitted9.AttendanceNumber = Submitted.AttendanceNumber
	and	Submitted9.UpdateType = '9' --insert/update
	and	not exists
		(
		select
			1
		from
			CDS62.AEAngliaSubmitted Previous
		where
			Previous.AttendanceNumber = Submitted.AttendanceNumber
		and	Previous.Created > Submitted9.Created
		)
	)

﻿
CREATE view [CDS62].[APCCriticalCarePeriodAngliaNet] as

--Updates - UpdateType = '9'
select
	 Base.UniqueEpisodeSerialNo
	,UpdateType = '9'
	,Base.CDSUpdateDate
	,Base.CDSUpdateTime
	,Base.CDSType
	,Base.RecordType
	,Base.NHSNumber
	,Base.PeriodID
	,Base.StartDate
	,Base.StartTime
	,Base.UnitFunctionCode
	,Base.GestationLength
	,Base.ActivityDate
	,Base.PersonWeight
	,Base.ActivityCode1
	,Base.ActivityCode2
	,Base.ActivityCode3
	,Base.ActivityCode4
	,Base.ActivityCode5
	,Base.ActivityCode6
	,Base.ActivityCode7
	,Base.ActivityCode8
	,Base.ActivityCode9
	,Base.ActivityCode10
	,Base.ActivityCode11
	,Base.ActivityCode12
	,Base.ActivityCode13
	,Base.ActivityCode14
	,Base.ActivityCode15
	,Base.ActivityCode16
	,Base.ActivityCode17
	,Base.ActivityCode18
	,Base.ActivityCode19
	,Base.ActivityCode20
	,Base.HighCostDrugCode1
	,Base.HighCostDrugCode2
	,Base.HighCostDrugCode3
	,Base.HighCostDrugCode4
	,Base.HighCostDrugCode5
	,Base.HighCostDrugCode6
	,Base.HighCostDrugCode7
	,Base.HighCostDrugCode8
	,Base.HighCostDrugCode9
	,Base.HighCostDrugCode10
	,Base.HighCostDrugCode11
	,Base.HighCostDrugCode12
	,Base.HighCostDrugCode13
	,Base.HighCostDrugCode14
	,Base.HighCostDrugCode15
	,Base.HighCostDrugCode16
	,Base.HighCostDrugCode17
	,Base.HighCostDrugCode18
	,Base.HighCostDrugCode19
	,Base.HighCostDrugCode20
	,Base.BlankPad
	,Base.EncounterChecksum
	,Action = 'UPDATE'
	,Base.EndDate
	,Base.EndTime
	,Base.EndOfRecord
from
	CDS62.APCCriticalCarePeriodAngliaBase Base

inner join CDS62.APCCriticalCarePeriodAngliaSubmitted Submitted
on	Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
and	Submitted.PeriodID = Base.PeriodID
and	Submitted.ActivityDate = Base.ActivityDate
and	not exists
	(
	select
		1
	from
		CDS62.APCCriticalCarePeriodAngliaSubmitted Previous
	where
		Previous.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
	and	Previous.PeriodID = Base.PeriodID
	and	Previous.ActivityDate = Base.ActivityDate
	and	Previous.Created > Submitted.Created
	)
and	Submitted.UpdateType = '9' --insert/update

where
	Base.EncounterChecksum <> Submitted.EncounterChecksum
and Base.ActivityCode1 is not null

union all

--Inserts - UpdateType = '9'
select
	 Base.UniqueEpisodeSerialNo
	,UpdateType = '9'
	,Base.CDSUpdateDate
	,Base.CDSUpdateTime
	,Base.CDSType
	,Base.RecordType
	,Base.NHSNumber
	,Base.PeriodID
	,Base.StartDate
	,Base.StartTime
	,Base.UnitFunctionCode
	,Base.GestationLength
	,Base.ActivityDate
	,Base.PersonWeight
	,Base.ActivityCode1
	,Base.ActivityCode2
	,Base.ActivityCode3
	,Base.ActivityCode4
	,Base.ActivityCode5
	,Base.ActivityCode6
	,Base.ActivityCode7
	,Base.ActivityCode8
	,Base.ActivityCode9
	,Base.ActivityCode10
	,Base.ActivityCode11
	,Base.ActivityCode12
	,Base.ActivityCode13
	,Base.ActivityCode14
	,Base.ActivityCode15
	,Base.ActivityCode16
	,Base.ActivityCode17
	,Base.ActivityCode18
	,Base.ActivityCode19
	,Base.ActivityCode20
	,Base.HighCostDrugCode1
	,Base.HighCostDrugCode2
	,Base.HighCostDrugCode3
	,Base.HighCostDrugCode4
	,Base.HighCostDrugCode5
	,Base.HighCostDrugCode6
	,Base.HighCostDrugCode7
	,Base.HighCostDrugCode8
	,Base.HighCostDrugCode9
	,Base.HighCostDrugCode10
	,Base.HighCostDrugCode11
	,Base.HighCostDrugCode12
	,Base.HighCostDrugCode13
	,Base.HighCostDrugCode14
	,Base.HighCostDrugCode15
	,Base.HighCostDrugCode16
	,Base.HighCostDrugCode17
	,Base.HighCostDrugCode18
	,Base.HighCostDrugCode19
	,Base.HighCostDrugCode20
	,Base.BlankPad
	,Base.EncounterChecksum
	,Action = 'INSERT'
	,Base.EndDate
	,Base.EndTime
	,Base.EndOfRecord

from
	CDS62.APCCriticalCarePeriodAngliaBase Base
where
	Base.ActivityCode1 is not null
and
	(
		not exists
		(
		select
			1
		from
			CDS62.APCCriticalCarePeriodAngliaSubmitted Submitted
		where
			Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
		and	Submitted.PeriodID = Base.PeriodID
		and	Submitted.ActivityDate = Base.ActivityDate
		)

	or	exists --latest update type was delete
		(
		select
			1
		from
			CDS62.APCCriticalCarePeriodAngliaSubmitted Submitted
		where
			Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
		and	Submitted.PeriodID = Base.PeriodID
		and	Submitted.ActivityDate = Base.ActivityDate
		and	Submitted.UpdateType = '1' --delete
		and	not exists
			(
			select
				1
			from
				CDS62.APCCriticalCarePeriodAngliaSubmitted Previous
			where
				Previous.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
			and	Previous.PeriodID = Base.PeriodID
			and	Previous.ActivityDate = Base.ActivityDate
			and	Previous.Created > Submitted.Created
			)
		)
	)

union all

--Deletes - UpdateType = '1'
select
	 Submitted.UniqueEpisodeSerialNo
	,UpdateType = '1'
	,Submitted.CDSUpdateDate
	,Submitted.CDSUpdateTime
	,Submitted.CDSType
	,Submitted.RecordType
	,Submitted.NHSNumber
	,Submitted.PeriodID
	,Submitted.StartDate
	,Submitted.StartTime
	,Submitted.UnitFunctionCode
	,Submitted.GestationLength
	,Submitted.ActivityDate
	,Submitted.PersonWeight
	,Submitted.ActivityCode1
	,Submitted.ActivityCode2
	,Submitted.ActivityCode3
	,Submitted.ActivityCode4
	,Submitted.ActivityCode5
	,Submitted.ActivityCode6
	,Submitted.ActivityCode7
	,Submitted.ActivityCode8
	,Submitted.ActivityCode9
	,Submitted.ActivityCode10
	,Submitted.ActivityCode11
	,Submitted.ActivityCode12
	,Submitted.ActivityCode13
	,Submitted.ActivityCode14
	,Submitted.ActivityCode15
	,Submitted.ActivityCode16
	,Submitted.ActivityCode17
	,Submitted.ActivityCode18
	,Submitted.ActivityCode19
	,Submitted.ActivityCode20
	,Submitted.HighCostDrugCode1
	,Submitted.HighCostDrugCode2
	,Submitted.HighCostDrugCode3
	,Submitted.HighCostDrugCode4
	,Submitted.HighCostDrugCode5
	,Submitted.HighCostDrugCode6
	,Submitted.HighCostDrugCode7
	,Submitted.HighCostDrugCode8
	,Submitted.HighCostDrugCode9
	,Submitted.HighCostDrugCode10
	,Submitted.HighCostDrugCode11
	,Submitted.HighCostDrugCode12
	,Submitted.HighCostDrugCode13
	,Submitted.HighCostDrugCode14
	,Submitted.HighCostDrugCode15
	,Submitted.HighCostDrugCode16
	,Submitted.HighCostDrugCode17
	,Submitted.HighCostDrugCode18
	,Submitted.HighCostDrugCode19
	,Submitted.HighCostDrugCode20
	,Submitted.BlankPad
	,Submitted.EncounterChecksum
	,Action = 'DELETE'
	,Submitted.EndDate
	,Submitted.EndTime
	,Submitted.EndOfRecord

from
	CDS62.APCCriticalCarePeriodAngliaSubmitted Submitted
where
	not exists -- ie latest Submitted entry for the record
	(
	select
		1
	from
		CDS62.APCCriticalCarePeriodAngliaSubmitted Previous
	where
		Previous.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	and	Previous.PeriodID = Submitted.PeriodID
	and	Previous.ActivityDate = Submitted.ActivityDate
	and	Previous.Created > Submitted.Created
	)
and	exists --latest update type was insert/update
	(
	select
		1
	from
		CDS62.APCCriticalCarePeriodAngliaSubmitted Submitted9
	where
		Submitted9.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	and	Submitted9.PeriodID = Submitted.PeriodID
	and	Submitted9.ActivityDate = Submitted.ActivityDate
	and	Submitted9.UpdateType = '9' --insert/update
	and	not exists
		(
		select
			1
		from
			CDS62.APCCriticalCarePeriodAngliaSubmitted Previous
		where
			Previous.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
		and	Previous.PeriodID = Submitted.PeriodID
		and	Previous.ActivityDate = Submitted.ActivityDate
		and	Previous.Created > Submitted9.Created
		)
	)
and -- It's now no longer in the base table, or it now has no activity in the base table
	(
		not exists
		(
		select
			1
		from
			CDS62.APCCriticalCarePeriodAngliaBase Base
		where
			Base.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
		and	Base.PeriodID = Submitted.PeriodID
		and	Base.ActivityDate = Submitted.ActivityDate
		)
	or
		exists
		(
		select
			1
		from
			CDS62.APCCriticalCarePeriodAngliaBase Base
		where
			Base.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
		and	Base.PeriodID = Submitted.PeriodID
		and	Base.ActivityDate = Submitted.ActivityDate
		and Base.ActivityCode1 is null
		)
	)




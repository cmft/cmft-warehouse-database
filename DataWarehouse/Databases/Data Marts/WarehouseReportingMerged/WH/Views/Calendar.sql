﻿



CREATE view [WH].[Calendar] as

/* 
==============================================================================================
Description:

When		Who				What
13/02/2015	Paul Egan		Added AcademicYear.
05/05/2015	Rachel Royston	Added Financial Week fields
===============================================================================================
*/

select
	DateID
	,TheDate
	,[DayOfWeek]
	,DayOfWeekKey
	,LongDate
	,TheMonth
	,[MonthName]
	,FinancialQuarter
	,FinancialYear
	,FinancialMonthKey
	,FinancialQuarterKey
	,CalendarQuarter
	,CalendarSemester
	,CalendarYear
	,LastCompleteMonth
	,WeekNoKey
	,WeekNo
	,FirstDayOfWeek
	,LastDayOfWeek
	,AcademicYear
	,FinancialWeekStart 
	,FinancialWeekEnd 
	,FinancialWeekNo 
	,FinancialWeekNoKey
	,FinancialWeek 
from 
	[WarehouseOLAPMergedV2].[WH].[Calendar]



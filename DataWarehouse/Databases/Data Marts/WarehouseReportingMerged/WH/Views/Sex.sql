﻿
create view [WH].[Sex] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceSexID]
      ,[SourceSexCode]
      ,[SourceSex]
      --,[LocalSexID]
      ,[LocalSexCode]
      ,[LocalSex]
      --,[NationalSexID]
      ,[NationalSexCode]
      ,[NationalSex]
  FROM [WarehouseOLAPMergedV2].[WH].[Sex]


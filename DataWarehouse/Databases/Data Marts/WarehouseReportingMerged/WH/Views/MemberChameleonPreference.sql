﻿



CREATE view [WH].[MemberChameleonPreference] as

/* 
==============================================================================================
Description: Created for Reference Map SSRS report on CI

When		Who			What
20150515	Paul Egan	Initial Coding
===============================================================================================
*/

select
	 Member.[AttributeCode]
	,Member.[Attribute]
	,Member.[SourceContextID]
	,Member.[SourceContextCode]
	,Member.[SourceContext]
	,Member.[SourceValueID]
	,Member.[SourceValueCode]
	,Member.[SourceValue]
	,Member.[LocalValueID]
	,Member.[LocalValueCode]
	,Member.[LocalValue]
	,Member.[NationalValueID]
	,Member.[NationalValueCode]
	,Member.[NationalValue]
	,Member.[OtherValueID]
	,Member.[OtherValueCode]
	,Member.[OtherValue]
	,ChameleonValueCode =
		case MemberChameleonPreferenceBase.ContextID
			when 28 then Member.LocalValueCode
			when 29 then Member.NationalValueCode
			else null
		end
	,ChameleonValue =
		case MemberChameleonPreferenceBase.ContextID
			when 28 then Member.LocalValue
			when 29 then Member.NationalValue
			else null
		end
	,ChameleonIsMapped = 
		case 
			when MemberChameleonPreferenceBase.AttributeCode is not null then 1
			else 0
		end
from
	WarehouseOLAPMergedV2.WH.Member
	
	left join WarehouseOLAPMergedV2.WH.MemberChameleonPreferenceBase
	on	MemberChameleonPreferenceBase.AttributeCode = Member.AttributeCode
;



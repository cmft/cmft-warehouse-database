﻿
create view [WH].[Site] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceSiteID]
      ,[SourceSiteCode]
      ,[SourceSite]
      --,[LocalSiteID]
      ,[LocalSiteCode]
      ,[LocalSite]
      --,[NationalSiteID]
      ,[NationalSiteCode]
      ,[NationalSite]
  FROM [WarehouseOLAPMergedV2].[WH].[Site]


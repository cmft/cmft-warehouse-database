﻿


CREATE view [WH].[Consultant] as
SELECT 
	 [SourceConsultantID]
	,[SourceConsultantCode]
	,[SourceConsultant]
	,[SourceContext]
	,[LocalConsultantID]
	,[LocalConsultantCode]
	,[LocalConsultant]
	,[NationalConsultantID]
	,[NationalConsultantCode]
	,[NationalConsultant]
	,[SourceContextCode]
	,[ProviderCode]
	,[MainSpecialtyCode]
	,[MainSpecialty]
	,[Title]
	,[Initials]
	,[Surname]
  FROM [WarehouseOLAPMergedV2].[WH].[Consultant]




﻿

CREATE view [WH].[Ward] as
SELECT 
	 SourceContextCode
	,SourceContext
	,SourceWardID 
	,SourceWardCode
	,SourceWard
	,LocalWardID 
	,LocalWardCode
	,LocalWard 
	,NationalWardID 
	,NationalWardCode
	,NationalWard 
  FROM [WarehouseOLAPMergedV2].[WH].[Ward]



﻿create view [WH].[ContractFlag]

as

SELECT 
	ContractFlagID =	AllocationID
   ,ContractFlagCode =	SourceAllocationID
   ,ContractFlag =		Allocation

FROM [WarehouseOLAPMergedV2].[Allocation].[Allocation]
  
where
	AllocationTypeID = 9
and	Active = 1

﻿
create view [WH].[MaritalStatus] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceMaritalStatusID]
      ,[SourceMaritalStatusCode]
      ,[SourceMaritalStatus]
      --,[LocalMaritalStatusID]
      ,[LocalMaritalStatusCode]
      ,[LocalMaritalStatus]
      --,[NationalMaritalStatusID]
      ,[NationalMaritalStatusCode]
      ,[NationalMaritalStatus]
  FROM [WarehouseOLAPMergedV2].[WH].[MaritalStatus]


﻿
create view [WH].[CCG] as

select
	 CCGID
	,CCGCode
	,CCG
	,LocalAreaTeamCode
	,LocalAreaTeam
	,CommissioningRegionCode
	,CommissioningRegion
from
	WarehouseOLAPMergedV2.WH.CCGBase

union

select
	 CCGID = 0
	,CCGCode = 'N/A'
	,CCG = 'N/A'
	,LocalAreaTeamCode = 'N/A'
	,LocalAreaTeam = 'N/A'
	,CommissioningRegionCode = 'N/A'
	,CommissioningRegion = 'N/A'

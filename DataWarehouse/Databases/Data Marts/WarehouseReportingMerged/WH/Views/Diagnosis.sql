﻿


CREATE view [WH].[Diagnosis]

as

select
	[DiagnosisID]
	,[DiagnosisCode]
	,[Diagnosis]
	,[DiagnosisChapterCode]
	,[DiagnosisChapter]
	,[DiagnosisGroupCode]
	,[DiagnosisGroup]
	,DiagnosisGroupSHMICode
	,[IsCharlson]
	,[IsConnectingForHealthMandatory]
	,[IsAlwaysPresent]
	,[IsLongTerm]
	,[IsRCode]
	,[IsUnspecified]
	,[IsRCodeCodingOpportunity]
	,Lifespan
from 
	WarehouseOLAPMergedV2.WH.Diagnosis 







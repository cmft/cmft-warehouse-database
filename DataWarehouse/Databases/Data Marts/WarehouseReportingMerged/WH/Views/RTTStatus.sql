﻿
create view [WH].[RTTStatus] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceRTTStatusID]
      ,[SourceRTTStatusCode]
      ,[SourceRTTStatus]
      --,[LocalRTTStatusID]
      ,[LocalRTTStatusCode]
      ,[LocalRTTStatus]
      --,[NationalRTTStatusID]
      ,[NationalRTTStatusCode]
      ,[NationalRTTStatus]
  FROM [WarehouseOLAPMergedV2].[WH].[RTTStatus]


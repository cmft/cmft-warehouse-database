﻿
Create View WH.Country

as

select 
	CountryID
	,CountryCode
	,Country
from 
	WarehouseOLAPMergedV2.WH.Country

﻿
create view [WH].[NHSNumberStatus] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceNHSNumberStatusID]
      ,[SourceNHSNumberStatusCode]
      ,[SourceNHSNumberStatus]
      --,[LocalNHSNumberStatusID]
      ,[LocalNHSNumberStatusCode]
      ,[LocalNHSNumberStatus]
      --,[NationalNHSNumberStatusID]
      ,[NationalNHSNumberStatusCode]
      ,[NationalNHSNumberStatus]
  FROM [WarehouseOLAPMergedV2].[WH].[NHSNumberStatus]


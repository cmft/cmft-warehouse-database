﻿
create view [WH].[OverseasVisitorStatus] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceOverseasVisitorStatusID]
      ,[SourceOverseasVisitorStatusCode]
      ,[SourceOverseasVisitorStatus]
      --,[LocalOverseasVisitorStatusID]
      ,[LocalOverseasVisitorStatusCode]
      ,[LocalOverseasVisitorStatus]
      --,[NationalOverseasVisitorStatusID]
      ,[NationalOverseasVisitorStatusCode]
      ,[NationalOverseasVisitorStatus]
  FROM [WarehouseOLAPMergedV2].[WH].[OverseasVisitorStatus]


﻿
create view [WH].[EthnicCategory] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceEthnicCategoryID]
      ,[SourceEthnicCategoryCode]
      ,[SourceEthnicCategory]
      --,[LocalEthnicCategoryID]
      ,[LocalEthnicCategoryCode]
      ,[LocalEthnicCategory]
      --,[NationalEthnicCategoryID]
      ,[NationalEthnicCategoryCode]
      ,[NationalEthnicCategory]
  FROM [WarehouseOLAPMergedV2].[WH].[EthnicCategory]


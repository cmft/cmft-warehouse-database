﻿

create view [WH].[Religion] as

SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceReligionID]
      ,[SourceReligionCode]
      ,[SourceReligion]
      ,[LocalReligionID]
      ,[LocalReligionCode]
      ,[LocalReligion]
      ,[NationalReligionID]
      ,[NationalReligionCode]
      ,[NationalReligion]
  FROM [WarehouseOLAPMergedV2].[WH].[Religion]

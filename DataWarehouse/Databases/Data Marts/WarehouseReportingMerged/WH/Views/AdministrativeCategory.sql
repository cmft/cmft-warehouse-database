﻿
create view [WH].[AdministrativeCategory] as


SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAdministrativeCategoryID]
      ,[SourceAdministrativeCategoryCode]
      ,[SourceAdministrativeCategory]
      --,[LocalAdministrativeCategoryID]
      ,[LocalAdministrativeCategoryCode]
      ,[LocalAdministrativeCategory]
      --,[NationalAdministrativeCategoryID]
      ,[NationalAdministrativeCategoryCode]
      ,[NationalAdministrativeCategory]
  FROM [WarehouseOLAPMergedV2].[WH].[AdministrativeCategory]


﻿
CREATE view [WH].[UnmappedValue] as

SELECT
	 AttributeCode
	,Attribute
	,SourceContextID
	,SourceContextCode
	,SourceContext
	,SourceValueID
	,SourceValueCode
	,SourceValue
	--,LocalValueID
	--,LocalValueCode
	--,LocalValue
	--,NationalValueID
	--,NationalValueCode
	--,NationalValue
FROM
	WarehouseOLAPMergedV2.WH.Member
where
	NationalValueCode like 'N||%'

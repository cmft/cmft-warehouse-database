﻿create view [WH].[Service]

as

/****************************************************************************************
	View		: [WH].[Service]
	Description : 

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	10/07/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	 ServiceID
	,SourceServiceID
	,[Service]
	,ServiceGroupID
	,ServiceGroup
from
	WarehouseOLAPMergedV2.WH.[Service]
;

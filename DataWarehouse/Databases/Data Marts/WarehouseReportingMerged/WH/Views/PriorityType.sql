﻿
create view [WH].[PriorityType] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourcePriorityTypeID]
      ,[SourcePriorityTypeCode]
      ,[SourcePriorityType]
      --,[LocalPriorityTypeID]
      ,[LocalPriorityTypeCode]
      ,[LocalPriorityType]
      --,[NationalPriorityTypeID]
      ,[NationalPriorityTypeCode]
      ,[NationalPriorityType]
  FROM [WarehouseOLAPMergedV2].[WH].[PriorityType]


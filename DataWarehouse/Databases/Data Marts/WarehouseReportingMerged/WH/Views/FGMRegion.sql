﻿Create View WH.FGMRegion

as

select 
	FGMRegionID
	,RegionCode
	,Region
	,CountryID
from 
	WarehouseOLAPMergedV2.WH.FGMRegion

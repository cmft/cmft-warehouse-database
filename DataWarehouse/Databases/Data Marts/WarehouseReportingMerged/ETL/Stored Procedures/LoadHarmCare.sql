﻿

CREATE PROCEDURE [ETL].[LoadHarmCare]

AS

--if @StartDate < '01 July 2012'
--BEGIN
--	SET @StartDate = '01 July 2012'
--END


DECLARE @StartDate AS DATE
DECLARE @EndDate as DATE
DECLARE @Hours AS INT

SET @StartDate = '01 July 2012'
SET @EndDate = GETDATE()
SET @Hours = 24


TRUNCATE TABLE RPT.HarmCare

INSERT INTO RPT.HarmCare
SELECT distinct 
		TheDate =			CrossCAL.thedate,
		WeekNo =			Crosscal.weekno,
		TheMonth =			CrossCal.TheMonth,
		FinancialQuarter  = Crosscal.FinancialQuarter,
		
		SpellID =			SpellID, 
		Patient = 			Patient, 
		EncounterRecNo =	EncounterRecNo,
		WardEncounterRecNo = APC.f_GetWardEncounterRecNo(E.ProviderSpellNo,COALESCE(F.FallTS, PUHFC1.IdentifiedTS,PUHFC2.IdentifiedTS, Test.UrinaryTestResultsSentTS, VTECondition.DiagnosisTS)),		
		IPEpisode =			IPEpisode, 
		AdmitDate =			AdmitDate, 
		Division =			DIR.Division, 
		SourceWardCode =	apc.f_GetWardCode(E.ProviderSpellNo,COALESCE(F.FallTS, PUHFC1.IdentifiedTS,PUHFC2.IdentifiedTS, Test.UrinaryTestResultsSentTS, VTECondition.DiagnosisTS)),
		Fall =				CASE WHEN DATEDIFF(HH, FallTS, CrossCAL.TheDate) IS NOT NULL THEN 1 ELSE 0 END,
		--CASE WHEN F.Bid IS NOT NULL THEN 1 ELSE 0 END,  
		PUHFC1 = CASE WHEN PUHFC1.Category IS NOT NULL THEN 1 ELSE 0 END,			
		--CASE WHEN PUHFC1.Bid IS NOT NULL THEN 1 ELSE 0 END,
		PUHFC2 = CASE WHEN PUHFC2.Category IS NOT NULL THEN 1 ELSE 0 END,			
		--CASE WHEN PUHFC2.Bid IS NOT NULL THEN 1 ELSE 0 END,
		UTI =				(SELECT TOP 1 1 FROM SmallDatasets.CQUIN.CatheterIntervention Cat
							 INNER JOIN SmallDatasets.bedman.EventArchive EA2
								ON EA2.Bid = cat.Bid
							 WHERE EA2.HospSpellID = EA.HospSpellID 
							 
							  and								
								(
							  
							 RemovedTS is null
							 and Test.TestResultsPositive = 'true'
							 AND UrinaryTestResultsSentTS >= cat.InsertedTS 
							 or
							 RemovedTS is not null
							 and Test.TestResultsPositive = 'true'
							 and datediff(hour,	RemovedTS, test.UrinaryTestResultsReceivedTS)between 0 and 48

								)
							 
							 
							 --AND cat.InsertedTS <= UrinaryTestResultsSentTS
							 
							 
							 
							 AND cat.CatheterType = 2
							 ),


		VTETreatment =		CASE WHEN VTETypeID IS NOT NULL THEN 1 ELSE 0 END,
		PatientCategory =	E.PatientCategoryCode

  FROM SmallDatasets.bedman.EventArchive EA
  CROSS JOIN (SELECT DISTINCT Thedate, FinancialQuarter, TheMonth, WeekNo, DateID
	FROM WarehouseReportingMerged.WH.Calendar
	WHERE TheDate BETWEEN @StartDate AND @EndDate) CrossCAL  
  
  LEFT JOIN BedMan.dbo.HospSpell HS
	ON EA.HospSpellID = HS.SpellID
  
  LEFT JOIN SmallDatasets.CQUIN.FallIncident F
	ON F.Bid = EA.Bid AND DATEDIFF(HH, FallTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours AND InitialSeverity > '1' -- Fall with harm
  
  LEFT JOIN SmallDatasets.CQUIN.PressureUlcerCondition PUHFC1
	ON PUHFC1.Bid = EA.Bid AND PUHFC1.Category > 1 AND CAST(PUHFC1.IdentifiedTS AS DATE) = CrossCAL.TheDate
  
  LEFT JOIN SmallDatasets.CQUIN.PressureUlcerCondition PUHFC2
	ON PUHFC2.Bid = EA.Bid AND PUHFC2.Category > 1 AND CAST(PUHFC2.IdentifiedTS AS DATE) = CrossCAL.TheDate	
	--AND DATEDIFF(HH, AdmitDate, PUHFC2.IdentifiedTS) > 72
  
  LEFT JOIN SmallDatasets.CQUIN.UTITest Test
	ON Test.Bid = EA.Bid AND DATEDIFF(HH, Test.UrinaryTestResultsSentTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours
  
  LEFT JOIN SmallDatasets.CQUIN.VTECondition
	ON VTECondition.Bid = EA.Bid AND DATEDIFF(HH, DiagnosisTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours	
  
  LEFT JOIN WarehouseReportingMerged.APC.Encounter E
	ON HS.Patient = E.SourcePatientNo AND HS.AdmitDate = E.AdmissionTime AND E.FirstEpisodeInSpellIndicator = 1
  
  LEFT JOIN WarehouseReportingMerged.WH.Directorate DIR 
    ON DIR.DirectorateCode = E.StartDirectorateCode
  --LEFT JOIN WarehouseReportingMerged.WH.Ward WARD
  --  ON WARD.SourceWardID = E.StartWardID
  
   WHERE (F.Bid IS NOT NULL OR PUHFC1.Bid IS NOT NULL OR PUHFC2.Bid IS NOT NULL OR Test.Bid IS NOT NULL OR VTECondition.Bid IS NOT NULL --or cat.Bid is not null
   )
   
   
  UPDATE [WarehouseReportingMerged].[RPT].[HarmCare]
  SET UTI = 0 WHERE UTI IS NULL
  
  DELETE FROM [WarehouseReportingMerged].[RPT].[HarmCare]
  WHERE Fall = 0 AND PUHFC1 = 0 AND PUHFC1 = 0 AND [UTI] = 0 AND VTETreatment = 0

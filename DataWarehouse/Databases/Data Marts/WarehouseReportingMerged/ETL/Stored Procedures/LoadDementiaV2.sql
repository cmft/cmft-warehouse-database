﻿



CREATE PROCEDURE [ETL].[LoadDementiaV2]
 
 
AS 


 /******************************************************************************
**  Name:		ETL.LoadDementia
**  Purpose:	Populates Dementia Assessment Model Base table : ETL.TimportDementiaTable
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** ----------	-------		---------------------------------------------------- 
**
** 2014-05-20	GC			 populate new encounter table
******************************************************************************/


--populate RPT.DementiaEncounter

set dateformat dmy

truncate table RPT.DementiaEncounter


INSERT INTO [WarehouseReportingMerged].[RPT].[DementiaEncounter]
           ([ContextCode]
           ,[MergeEncounterRecno]
           ,[DistrictNo]
           ,[CasenoteNumber]
           ,[PatientSurname]
           ,[PatientForename]
           ,[DateOfBirth]
           ,[AgeCode]
           ,[Los]
           ,[Ward]
           ,[KnownToHaveDementia]
           ,[DementiaAssessmentAnswer]
           ,[DementiaAssessmentAnswerDate]
           ,[AMTRecordedDate]
           ,[AMTScore]
           ,[AMTTestNotPerformedReason]
           ,[InvestigationRecordedDate]
           ,[InvestigationStarted]
           ,[AdmissionDate]
           ,[AdmissionPlus72]
           ,[AdmissionTime]
           ,[DischargeDate]
           ,[DischargeTime]
           ,[DementiaCase]
           ,[Find]
           ,[Asses]
           ,[Refer]
           ,[FindSubmissionDate]
           ,[AssesSubmissiondate]
           ,[ReferSubmissionDate]
           ,[Rundate]
           ,[ExpectedDateofDischarge]
           ,[CurrentForgetfulnessQuestionAnswered])


select

		 ContextCode
		,MergeEncounterRecno 
		,DistrictNo
		,CasenoteNumber
		,PatientSurname
		,PatientForename
		,DateOfBirth
		,AgeCode
		,Los
		,Ward
		,KnownToHaveDementia
		,DementiaAssessmentAnswer
		,DementiaAssessmentAnswerDate
		,AMTRecordedDate
		,AMTScore
		,AMTTestNotPerformedReason
		,InvestigationRecordedDate
		,InvestigationStarted
		,AdmissionDate
		,AdmissionPlus72
		,AdmissionTime
		,DischargeDate
		,DischargeTime
		,DementiaCase 
		,Find
		,Asses
		,Refer
		,FindSubmissionDate -- = case when FindSubmissionDate between @SubmissionStartDate and @SubmissionEndDate then FindSubmissionDate else null end
		,AssesSubmissiondate --= case when AssesSubmissiondate between @SubmissionStartDate and @SubmissionEndDate then AssesSubmissiondate else null end
		,ReferSubmissionDate --= case when ReferSubmissionDate between @SubmissionStartDate and @SubmissionEndDate then ReferSubmissionDate else null end

		,Rundate = getdate ()
		,ExpectedDateofDischarge
		,CurrentForgetfulnessQuestionAnswered

from (


	select

	--	 Encounter.ProviderSpellno

		 Encounter.Contextcode
		,Encounter.MergeEncounterRecno
		,DistrictNo
		,CasenoteNumber
		,PatientSurname
		,PatientForename
	--	,SexCode
		,DateOfBirth
		,AgeCode
		,Los = datediff(hour,AdmissionTime,coalesce(DischargeTime,getdate()))
		,Ward = coalesce(CurrentWard,EndWardTypeCode)
		
		,KnownToHaveDementia = 
			
			case		
			when KnownToHaveDementia = 1 then 'Yes' else null end

		
		,DementiaAssessmentAnswer = 
		
			case 
				when ForgetfulnessQuestionAnswered = '1' then 'Yes'
				when ForgetfulnessQuestionAnswered = '0' then 'No'
				when KnownToHaveDementia = '1' then 'KnownDementia'
				--when KnownToHaveDementia = 'Yes' then 'KnownDementia'				
			else 'Outstanding'
			end
			
		,DementiaAssessmentAnswerDate 
			
		,AMTRecordedDate 
		,AMTScore
		,AMTTestNotPerformedReason
		,InvestigationRecordedDate 
		,InvestigationStarted 
	
		,AdmissionDate
		,AdmissionPlus72 = dateadd(mi,4320,AdmissionTime)
		,AdmissionTime
		,DischargeDate
		,DischargeTime

		
	


		,DementiaCase = 1
		
		,Find = 
		
			case 
				when KnownToHaveDementia = '1' then 1
				when datediff(mi,AdmissionTime ,DementiaAssessmentAnswerDate ) <= 4380 then 1
				when DementiaAssessmentAnswerDate  is null then 0 
				else 0 end
		
		,Asses =
		
			case
				when (DementiaData.CurrentForgetfulnessQuestionAnswered = 1 or ClinicalIndicationsPresent = 1) and AMTScore > 7 then 1
				when (DementiaData.CurrentForgetfulnessQuestionAnswered = 1 or ClinicalIndicationsPresent = 1) and AMTScore < 8 and InvestigationStarted = 'Y' then 1
			else 0	
			end 
			
		,Refer =
			Case when DementiaReferralSentDate is not null then 1 else 0 end
			
		,FindSubmissionDate
		
			= dateadd(mi,4320,AdmissionTime)
			
		--,AssesSubmissiondate =
		
		--	Case
		--		when CurrentForgetfulnessQuestionAnswered = 0 then null
		--		when KnownToHaveDementia = '1' then null
		--		when AMTTestNotPerformedReason is not null and AMTTestNotPerformedReason <> '' then null
		--		when DementiaData.CurrentForgetfulnessQuestionAnswered = 1 and AMTScore > 7 then AMTRecordedDate
		--		When DementiaData.CurrentForgetfulnessQuestionAnswered = 1 and AMTScore < 8  and InvestigationStarted = 'Y' then InvestigationRecordedDate
		--		When DementiaData.CurrentForgetfulnessQuestionAnswered = 1 and DischargeDate is not null and AMTScore is null then DischargeDate
		--		When DementiaData.CurrentForgetfulnessQuestionAnswered = 1 and DischargeDate is not null and AMTScore < 8 and InvestigationStarted is null then DischargeDate
		--	else null
		--	end
			
			,AssesSubmissiondate =
		
			Case
			
				
				when (CurrentForgetfulnessQuestionAnswered = 0 and ClinicalIndicationsPresent is null) then null
				when KnownToHaveDementia = '1' then null
				when AMTTestNotPerformedReason is not null and AMTTestNotPerformedReason <> '' then null
				when AMTScore > 7 then AMTRecordedDate
				When AMTScore < 8  and InvestigationStarted = 'Y' then InvestigationRecordedDate
				When (DementiaData.CurrentForgetfulnessQuestionAnswered = 1 or ClinicalIndicationsPresent = 1) and DischargeDate is not null and AMTScore is null then DischargeDate
				When (DementiaData.CurrentForgetfulnessQuestionAnswered = 1 or ClinicalIndicationsPresent = 1) and DischargeDate is not null and AMTScore < 8 and InvestigationStarted is null then DischargeDate
			else null
			end
					
			
		--,ReferSubmissionDate =
				
		--		Case 
				
		--			when DementiaData.CurrentForgetfulnessQuestionAnswered  = '1' and AMTScore between 1 and 7 and DementiaReferralSentDate is not null then DementiaReferralSentDate 
		--			when DementiaData.CurrentForgetfulnessQuestionAnswered  = '1' and AMTScore between 1 and 7 and DischargeDate is not null and DementiaReferralSentDate is null then DischargeDate
		--		else null
		--		end	
		
				,ReferSubmissionDate =
				
				Case 
				
					when  AMTScore between 1 and 7 and DementiaReferralSentDate is not null then DementiaReferralSentDate 
					when  AMTScore between 1 and 7 and DischargeDate is not null and DementiaReferralSentDate is null then DischargeDate
				else null
				end	
		
		,Encounter.ExpectedDateofDischarge
		,CurrentForgetfulnessQuestionAnswered = 
		
		case 
				when DementiaData.ClinicalIndicationsPresent = 1 then 'Yes'
				when DementiaData.CurrentForgetfulnessQuestionAnswered  = '1' then 'Yes'
				when DementiaData.CurrentForgetfulnessQuestionAnswered  = '0' then 'No'
				when KnownToHaveDementia = '1' then 'KnownDementia'
				--when KnownToHaveDementia = 'Yes' then 'KnownDementia'				
			else 'Outstanding'
			end		
		
		
		
		from 
			APC.Encounter Encounter


						INNER JOIN APC.AdmissionMethod
						ON	Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   
						
						

						left join APC.DischargeMethod
						on DischargeMethod.SourceDischargeMethodID = Encounter.DischargeMethodID

						INNER JOIN APC.PatientClassification
						ON	Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID 


						left join [TrustCache].[Dementia].[Bedman_DataSet] DementiaData
						on DementiaData.ProviderspellNo = Encounter.ProviderSpellno

						where 
						
			
				left(agecode,2) between '75' and '99' 

				and
					(
					DischargeDate is not null
					and
					NationalLastEpisodeInSpellIndicator = 1
					or
					DischargeDate is null 
					and 
					FirstEpisodeInSpellIndicator = 1
					)
				and
			
				( DischargeMethod.NationalDischargeMethodCode is null or  DischargeMethod.NationalDischargeMethodCode not in ('4','5'))
				and
				AdmissionMethod.AdmissionType = 'Emergency'

				AND NationalAdmissionMethodCode <> '81' -- Exclude Transfers

				AND PatientClassification.NationalPatientClassificationCode NOT IN ('3','4') -- Exclude Regular admissions 

				and 
						Encounter.DateOfDeath is null  


	) D



﻿

CREATE FUNCTION [RPT].[fn_DuplicatePatientRegistrations]		-- select * from RPT.fn_DuplicatePatientRegistrations('24 feb 2014','2 mar 2014') order by MasterSourcePatientNo
(
	 @StartDate		DATE
	,@EndDate		DATE
)
RETURNS TABLE AS
RETURN
(
	SELECT
		 Cases						= 1
		,PeriodWeekNo				= CAL.WeekNo
		,PeriodWeekNoKey			= CAL.WeekNoKey
		,PeriodWeekStartDate		= DATEADD(DAY, -6, CAL.LastDayOfWeek)
		,PeriodWeekEndDate			= CAL.LastDayOfWeek
		,PeriodMonth				= CALWEEKEND.TheMonth
		,PeriodMonthKey				= CALWEEKEND.FinancialMonthKey
		,PeriodYear					= CALWEEKEND.FinancialYear

		,RegistrationDate			= DATA.RegistrationDate
		,MasterSourcePatientNo		= DATA.MasterSourcePatientNo
		,SourcePatientNo			= PD.SourcePatientNo
		,DistrictNo					= PAT.DistrictNo
		,NHSNumber					= PAT.NHSNumber
		,Surname					= PAT.Surname
		,Forename					= PAT.Forenames
		,DateOfBirth				= PAT.DateOfBirth
		,Sex						= PAT.SexCode
		,PostCode					= PAT.Postcode
		,AddressLine1				= PAT.AddressLine1
		,AddressLine2				= PAT.AddressLine2
		,AddressLine3				= PAT.AddressLine3
		,AddressLine4				= PAT.AddressLine4
		,GPCode						= PAT.RegisteredGpCode

	FROM
	-- Get the duplicate patient with the latest registration date
	-- This is used for the time dimension
	(
		SELECT 
			 SourcePatientNo
			,MasterSourcePatientNo
			,RegistrationDate
			-- Don't use Rank() in case 2 licates have the same registration date (we only want to return 1 row)
			,Rno = ROW_NUMBER() OVER (Partition By MasterSourcePatientNo Order By RegistrationDate DESC)
		FROM 
			Warehouse.PAS.PatientDuplicationBase
	) DATA
	
	INNER JOIN WH.Calendar CAL
		ON DATA.RegistrationDate = CAL.TheDate

	INNER JOIN WH.Calendar CALWEEKEND
		ON CAL.LastDayOfWeek = CALWEEKEND.TheDate

	INNER JOIN Warehouse.PAS.PatientDuplicationBase PD
		ON		DATA.MasterSourcePatientNo = PD.MasterSourcePatientNo

	INNER JOIN Warehouse.PAS.Patient PAT
		ON PD.SourcePatientNo = PAT.SourcePatientNo
		
	WHERE
			DATA.Rno = 1
		AND	DATA.RegistrationDate BETWEEN @StartDate AND @EndDate
)





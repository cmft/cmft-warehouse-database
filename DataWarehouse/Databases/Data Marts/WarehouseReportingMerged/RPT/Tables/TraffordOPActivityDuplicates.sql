﻿CREATE TABLE [RPT].[TraffordOPActivityDuplicates] (
    [EncounterRecno] INT          NULL,
    [Specialty]      VARCHAR (20) NULL,
    [N_F]            VARCHAR (1)  NULL,
    [Duplicate]      VARCHAR (17) NOT NULL,
    [DerivedNF]      VARCHAR (1)  NULL
);


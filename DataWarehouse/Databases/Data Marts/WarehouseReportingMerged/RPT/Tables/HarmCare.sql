﻿CREATE TABLE [RPT].[HarmCare] (
    [TheDate]            DATE          NULL,
    [WeekNo]             VARCHAR (50)  NULL,
    [TheMonth]           NVARCHAR (34) NULL,
    [FinancialQuarter]   NVARCHAR (70) NULL,
    [SpellID]            INT           NULL,
    [Patient]            VARCHAR (8)   NULL,
    [EncounterRecNo]     BIGINT        NULL,
    [WardEncounterRecNo] BIGINT        NULL,
    [IPEpisode]          VARCHAR (5)   NULL,
    [AdmitDate]          DATETIME      NULL,
    [Division]           VARCHAR (50)  NULL,
    [SourceWardCode]     VARCHAR (100) NULL,
    [Fall]               INT           NULL,
    [PUHFC1]             INT           NULL,
    [PUHFC2]             INT           NULL,
    [UTI]                INT           NULL,
    [VTETreatment]       INT           NULL,
    [PatientCategory]    VARCHAR (2)   NULL
);


﻿

CREATE proc [RPT].[CQUINPDementia] 

 @SubmissionStartDate datetime
,@SubmissionEndDate datetime
,@Unify varchar (1)

as
--declare @SubmissionStartDate datetime
--declare @SubmissionEndDate datetime
--declare @Unify varchar (1)

--set @SubmissionStartDate = '01 may 2014'
--set @SubmissionEndDate = '30 may 2014 23:59'

--SET @Unify = 'N'


select

		 Rundate
		,ContextCode
	
		,DistrictNo
		,CasenoteNumber
		,PatientSurname
		,PatientForename
		--,DateOfBirth 
		,AgeCode
		,Los
		,Ward
		--,KnownToHaveDementia
		,DementiaAssessmentAnswer
		,CurrentDementiaAssessmentAnswer = CurrentForgetfulnessQuestionAnswered
		,DementiaAssessmentAnswerDate
		,AMTRecordedDate
		,AMTScore
		,AMTTestNotPerformedReason
		,InvestigationRecordedDate
		,InvestigationStarted
		,AdmissionDate
		,AdmissionPlus72
		,ExpectedDateofDischarge		
		,AdmissionTime
		,DischargeDate
		,DischargeTime
		,DementiaCase 
				
		
		,FindCase = case when FindSubmissionDate between @SubmissionStartDate and @SubmissionEndDate then 1 else 0 end
		,Find 
		
		,AssesCase = case when AssesSubmissiondate between @SubmissionStartDate and @SubmissionEndDate then 1 else 0 end
		,Asses 
		
		,ReferCase = case when ReferSubmissionDate between @SubmissionStartDate and @SubmissionEndDate then 1 else 0 end
		,Refer 
		
	
		
		,FindSubmissionDate = case when FindSubmissionDate between @SubmissionStartDate and @SubmissionEndDate then FindSubmissionDate else null end
		,AssesSubmissiondate = case when AssesSubmissiondate between @SubmissionStartDate and @SubmissionEndDate then AssesSubmissiondate else null end
		,ReferSubmissionDate = case when ReferSubmissionDate between @SubmissionStartDate and @SubmissionEndDate then ReferSubmissionDate else null end
		,MergeEncounterRecno 
		
		from RPT.DementiaEncounter


				where

			(
				@unify = 'Y'
				and
				Los >= 72
								
				and 
				
					(				
				FindSubmissionDate between @SubmissionStartDate and @SubmissionEndDate
				or 
				--AssesSubmissionDate between @SubmissionStartDate and @SubmissionEndDate
				AssesSubmissionDate between @SubmissionStartDate and @SubmissionEndDate
				and
				DementiaAssessmentAnswer = 'yes'
				and AMTTestNotPerformedReason is null
				or
				ReferSubmissionDate between @SubmissionStartDate and @SubmissionEndDate
					)
			or 
				@Unify = 'N'
				and
				Dischargedate is  null
		 
			)
		
	

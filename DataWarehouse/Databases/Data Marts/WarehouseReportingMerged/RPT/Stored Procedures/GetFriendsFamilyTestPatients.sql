﻿

CREATE proc 
              [RPT].[GetFriendsFamilyTestPatients] --  '1 june 2014'

              (
              @Month datetime 
              )

as
              DECLARE @StartDate DATE
              DECLARE @EndDate DATE
              
              SELECT
                     @StartDate = MIN(TheDate)
                     ,@EndDate   = MAX(TheDate)
              FROM
                     WH.Calendar
              WHERE
                     TheMonth = (SELECT TheMonth FROM WH.Calendar WHERE TheDate = CAST(@Month AS DATE) )

				and TheDate <  dateadd(day,datediff(day,0,GETDATE()),0)
------- Create temp Table of base data
              select  
                      EndWardTypeCode
                     ,AdmissionMethodID
                     ,PatientClassificationID
                     ,SpecialtyID
                     ,DischargeDestinationID
                     ,EndDirectorateCode = CASE
                                         when EndWardTypeCode = 'ESTU' then '10'
                                         when EndWardTypeCode = 'ICU' then '92'
                                         when EndWardTypeCode = '27M' then '26'
                                         else EndDirectorateCode
                     end
                     
                     ,Cases
                     ,LocalAdminCategoryCode
                     ,contextcode
					,DischargeDate 
              INTO #T

              FROM
                     APC.Encounter
              WHERE
                        Encounter.DischargeDate  BETWEEN @StartDate AND @EndDate --BETWEEN '1 may 2014' and '31 may 2014' --
              and       Encounter.[NationalLastEpisodeInSpellIndicator] = 1
              and       floor(datediff(day, Encounter.DateOfBirth, Encounter.AdmissionDate) / 365.25) > = 16
              and           Encounter.EndSiteCode not in ('NRMC')       
              and       [EndWardTypeCode] not in ('OSM', 'TDU', 'SUN', 'ENDO', 'PARK','DC','CATH','PIU','IH','66','16','28m','OMU','AMU','MAU2','ERU','UDC','90','55D','ETCD','54','SAL','TH','CRC','DL','DSUN','LNH','OSDU','SUBS','SUBM','SUBE','PDEN','POAU','AIN','WCH','CONS','XRAY')
              and       (
						(
							[EndWardTypeCode] <> 'ESTU'	
						and	LOS > = 1	
						)	
						or
							(
								[EndWardTypeCode] = 'ESTU'	
							and	datediff(hour, AdmissionTime, DischargeTime) > 24
							)
						)
						

						
              and       (DateofDeath is null or DateofDeath > DischargeDate)   
              --and           ( 
              --                    (contextcode = 'CEN||PAS' 
              --                    and  LocalAdminCategoryCode in( 'NHS')
              --                    )
              --             or
              --                    contextcode = 'TRA||UG' --and  LocalAdminCategoryCode is null
              --             )
              
                     
              SELECT        
                     Ward         = EndWardTypeCode
                     ,Division     = COALESCE(DivMap.Division, DIR.Division)
                    ,Patients     = SUM(Cases)

			into
				#ReportTable
              FROM
                     #T Encounter

              inner join WarehouseReportingMerged.WH.[Specialty] Specialty
              on            Specialty.SourceSpecialtyID = Encounter.SpecialtyID
                     and    Specialty.NationalSpecialtyCode not in ('424', '501', '422') --Obsetrics, Neonatal & Well Babies        

              inner join WarehouseReportingMerged.APC.[AdmissionMethod] AdmissionMethod
              on     AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID

              inner join WarehouseReportingMerged.APC.PatientClassification PatientClassification
              on            Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID
                     and    PatientClassification.NationalPatientClassificationCode not in ('2', '3', '4','5')                                        

              inner join WarehouseReportingMerged.[APC].[DischargeDestination] DischargeDestination
              on            DischargeDestination.SourceDischargeDestinationID = Encounter.DischargeDestinationID
                     and    DischargeDestination.NationalDischargeDestinationCode not in ('49','51','53','79')

              left join  WarehouseReportingMerged.WH.Directorate DIR
              on    Encounter.EndDirectorateCode = DIR.DirectorateCode
              
              left join PatientExperience.dbo.FFTLocationMap WardMap
              on            Encounter.EndWardTypeCode = WardMap.LtnNme
                     and WardMap.Type = 'FCE'
                     
              left join PatientExperience.dbo.FFTDivisionMap DivMap
              on            WardMap.DvnNme = DivMap.FFTDivision
                       
              GROUP BY 
                      EndWardTypeCode
                     ,COALESCE(DivMap.Division, DIR.Division)

              --DROP TABLE #T




			select
				*
				,LatestDischargeDate = (select max(DischargeDate) from #T Encounter)
			from
				#ReportTable
				                     

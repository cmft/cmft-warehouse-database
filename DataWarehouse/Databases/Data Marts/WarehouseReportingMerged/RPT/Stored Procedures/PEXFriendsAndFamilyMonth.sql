﻿

CREATE procedure [RPT].[PEXFriendsAndFamilyMonth]

as

select 
		
		CensusMonth = dateadd(mm, datediff(m,0,[CensusTime]),0)
		,[TheMonth]
	
	from
		[PEX].[Response] Response
				
	inner join
		[PEX].[Question] Question
	on	
		Response.[QuestionID] = Question.[QuestionID]
		
	inner join
		[WarehouseReporting].[WH].[Calendar] Calendar
	on
		Calendar.[TheDate] = dateadd(mm, datediff(m,0,Response.[CensusTime]),0)
		
	where	
		[CensusTime] > '2012-04-01 00:00'
		
group by 
		dateadd(mm, datediff(m,0,[CensusTime]),0)
		,[TheMonth]
order by 
		dateadd(mm, datediff(m,0,[CensusTime]),0) desc

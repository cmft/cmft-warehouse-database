﻿



create procedure [RPT].[HRG4ErrorsData]

	@ErrorType  varchar(15) = null
	,@Site		varchar(50) = null
	,@StartDate date = null
	,@EndDate date = null

as

set @StartDate = coalesce(@StartDate, '01 jan 1900')
set @EndDate = coalesce(@EndDate, getdate())

select 
		H.[MergeEncounterRecno]
		,H.[SequenceNo]
		,ErrorMessage = coalesce(H.[QualityTypeCode],'') + ' | ' + coalesce(H.[QualityCode],'') + ' | ' + H.[QualityMessage]
		,si.NationalSite
		,coalesce(H.[QualityTypeCode],'')
		,E.DistrictNo
		,E.CasenoteNumber
		,E.EpisodeStartTime
		,E.EpisodeEndTime
		,E.DischargeDate
		,e.DischargeDateID	
		,DischargeMonth = CAL.TheMonth
		,MAINSPEF = C.MainSpecialtyCode
		,TRETSPEF = S.NationalSpecialtyCode
	    ,DIAG_01 = isnull(E.PrimaryDiagnosisCode,'')
	    ,DIAG_02 = isnull(E.SecondaryDiagnosisCode1,'')
		,DIAG_03 = isnull(E.SecondaryDiagnosisCode2,'')    
		,DIAG_04 = isnull(E.SecondaryDiagnosisCode3,'')
		,DIAG_05 = isnull(E.SecondaryDiagnosisCode4,'')
		,DIAG_06 = isnull(E.SecondaryDiagnosisCode5,'')
		,DIAG_07 = isnull(E.SecondaryDiagnosisCode6,'')
		,DIAG_08 = isnull(E.SecondaryDiagnosisCode7,'')
		,DIAG_09 = isnull(E.SecondaryDiagnosisCode8,'')
		,DIAG_10 = isnull(E.SecondaryDiagnosisCode9,'')
		,DIAG_11 = isnull(E.SecondaryDiagnosisCode10,'')
		,DIAG_12 = isnull(E.SecondaryDiagnosisCode11,'')
		,DIAG_13 = isnull(E.SecondaryDiagnosisCode12,'')
		,DIAG_14 = isnull(E.SecondaryDiagnosisCode13,'')
		,OPER_01 = isnull(E.PrimaryProcedureCode ,'')
		,OPER_02 = isnull(E.SecondaryProcedureCode1,'')
		,OPER_03 = isnull(E.SecondaryProcedureCode2,'')
		,OPER_04 = isnull(E.SecondaryProcedureCode3,'')
		,OPER_05 = isnull(E.SecondaryProcedureCode4,'')
		,OPER_06 = isnull(E.SecondaryProcedureCode5,'')
		,OPER_07 = isnull(E.SecondaryProcedureCode6,'')
		,OPER_08 = isnull(E.SecondaryProcedureCode7,'')
		,OPER_09 = isnull(E.SecondaryProcedureCode8,'')
		,OPER_10 = isnull(E.SecondaryProcedureCode9,'')
		,OPER_11 = isnull(E.SecondaryProcedureCode10,'')
		,OPER_12 = isnull(E.SecondaryProcedureCode11,'')
FROM 
	[APC].[HRG4Quality] H  
	
inner join apc.[Encounter] E
on (e.MergeEncounterRecno = h.mergeEncounterRecno)

inner join wh.Specialty S
on (E.SpecialtyID = S.SourceSpecialtyID)

inner join wh.consultant C
on (E.Consultantid = C.sourceConsultantid) 

inner join wh.site SI
on (e.StartSiteID = SI.SourceSiteID)

inner join [wh].[calendar] CAL
on (e.DischargeDateID = cal.DateID)
		
where		
	(
		H.[QualityTypeCode] = @ErrorType
		or @ErrorType is null
	)	 
and
	(		
		si.[NationalSite] = @Site
		or @Site is null
	)	

and
	(DischargeDate between @StartDate and @EndDate)
		
and
	QualityMessage not like '%Spell exceeds%'
and
	QualityMessage not like '%Primary diagnosis is blank%'
and
	QualityTypeCode not like '%AGE%'
	



﻿CREATE Procedure [RPT].[DementiaApplicableAdmissionsWard]

as

Select 
	distinct
		[Site/Ward] = (AdmittingSite.NationalSite + ' : ' + WardCode)
from 
	APC.Encounter Encounter

inner join APC.WardStay
on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
and not exists
	(
	select
		1
	from 
		APC.WardStay Latest
	where 
		Latest.ProviderSpellNo = WardStay.ProviderSpellNo
	and	Latest.StartTime > WardStay.StartTime
	)

inner join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   
	
inner join WH.[Site] AdmittingSite
on Encounter.StartSiteCode = AdmittingSite.SourceSiteCode
and AdmittingSite.SourceContextCode in ('TRA||UG','CEN||PAS')

where	
	DischargeDate is null
and cast((datediff(day,DateOfBirth,AdmissionDate)/365.25) as int)>=75 
and	AdmissionMethod.AdmissionType = 'Emergency' -- Non Elective admissions only

union
	
select 
	[Site/Ward] = 'All'
	

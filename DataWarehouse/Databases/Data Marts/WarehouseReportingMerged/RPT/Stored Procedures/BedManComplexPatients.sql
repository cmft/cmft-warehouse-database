﻿	
	
		CREATE Proc RPT.BedManComplexPatients
	
	
	 @AdmissionStatus varchar (1)
	,@AdmissionsFromDate datetime
	,@AdmissionsToDate datetime
	
	as
	
	declare @SetStartDate datetime = coalesce(@AdmissionsFromDate,(SELECT CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-7),getdate()),106) ))
	declare @SetEnddate datetime = coalesce(@AdmissionsToDate,(select DATEADD(day, DATEDIFF(day,0,GETDATE()), 0)))
	
	
	Select		
			
			 Directorate.Directorate

			,CaseNote 
				= Encounter.CasenoteNumber 
			,Districtno 
				= Encounter.DistrictNo
            ,PatientSurname 
				= WarehouseOLAPMergedV2.dbo.f_ProperCase(Encounter.PatientSurname)			
            ,PatientForename = 
				WarehouseOLAPMergedV2.dbo.f_ProperCase(Encounter.PatientForename)			
	        ,Gender	
				= Encounter.sexcode		
            ,DOB 
				= Encounter.DateOfBirth
			,AgeOnAdmission 
				= datediff(year,Encounter.DateOfBirth,Encounter.Admissiondate)	
			,Age
				= datediff(year,Encounter.DateOfBirth,getdate ())			
            ,AdmissionDate 
				= Encounter.Admissiondate
			,EstimatedDischargeDate 
				= ComplexFlag.EDD			
			,DischargeDate 
				= Encounter.DischargeDate
			,AdmissionWard 
				= Encounter.StartWardTypeCode
            ,CurrentWard 
				= 
				case
					when Encounter.DischargeDate is null then ComplexFlag.CurrentWard 
					else null
				end	
			,TimeOnCurrentWard 
				= 
				case
					when Encounter.DischargeDate is null then coalesce(ComplexFlag.TimeOnCurrentWard,Encounter.Admissiondate)	
					else null
				end					
						
  			,Encounter.LOS
            ,MLTC   			
            ,ClinManage 	
            ,KeyProf		
            ,LastUpdated 	
            ,UpdatedBy 		
			
FROM        			
			[TrustCache].COMPLEX.Bedman_Event ComplexFlag
			
			INNER JOIN WarehouseReportingMerged.APC.Encounter Encounter
			ON
			 ComplexFlag.IPEpisode = Encounter.SourceSpellNo
			and ComplexFlag.Patient = Encounter.SourcePatientNo
			AND Encounter.FirstEpisodeInSpellIndicator = 1
			
			inner join WarehouseReportingMerged.WH.Directorate Directorate
			on Directorate.DirectorateCode = Encounter.StartDirectorateCode
			
		where 
			--18 and under only
			datediff(year,Encounter.DateOfBirth,Encounter.Admissiondate) <= 18
			
			and
			
				(
				
				@AdmissionStatus is null
				
				or
				
				@AdmissionStatus = 'D'
				and 
				Encounter.DischargeDate is not null
				or
				
				@AdmissionStatus = 'A'
				and
				Encounter.DischargeDate is null
				
				
				)
				
				and 
				
				(
				
				Encounter.Admissiondate between @AdmissionsFromDate and @AdmissionsToDate
				
				)
			
	
			
   	
				
			
			
			
			

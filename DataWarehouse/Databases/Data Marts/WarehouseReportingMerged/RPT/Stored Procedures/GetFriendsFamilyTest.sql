﻿
CREATE  proc 
              [RPT].[GetFriendsFamilyTest] -- '01 May 2014'

              (
              @Month datetime 
              )

as


       SET @Month = dateadd(month, datediff(month,0,@Month),0)

       -- temp table to hold discharges by Ward, see Union statement ate then end of the query
       CREATE TABLE #t (Ward varchar(50), Division varchar(50), Patients int,latestdischargeDate date)
       INSERT INTO   #t EXEC [RPT].[GetFriendsFamilyTestPatients] @Month

select
       FFTMonth
       ,FFTValue
       ,FFTDivision
       ,FFTLocation
       ,FFTResponseNo
       ,FTTResponses
       ,FTTScore1To5 = CASE WHEN DATA.FFTValue BETWEEN 1 AND 5 THEN 1 ELSE 0 END
       ,FTTScore3To5 = CASE WHEN DATA.FFTValue BETWEEN 3 AND 5 THEN 1 ELSE 0 END
       ,FTTScore1      = CASE WHEN DATA.FFTValue = 1 THEN 1 ELSE 0 END
       
from
(
              select               

                                         FFTMonth =                        @Month,
                                         FFTValue =                        case   
                                                                                                when FFTMain.AwrNbr in (68,696,676,907) then 1 
                                                                                                when FFTMain.AwrNbr in (69,697) then 2 
                                                                                                when FFTMain.AwrNbr in (70)then 3 
                                                                                                when FFTMain.AwrNbr in (71)then 4
                                                                                                when FFTMain.AwrNbr in (72,238,908)then 5
                                                                                                when FFTMain.AwrNbr in (910)then 6
                                                                                                else NULL 
                                                                                  end,
                                         FFTDiscAnswer =                   case 
                                                                                                when Discharge.[AwrTxt] is null then [OnDch] 
                                                                                                else Discharge.[AwrTxt] 
                                                                                  end,                                      
                                         FFTDivision =              COALESCE(FFTDivisionMap.Division , 'Unknown'),
                                         FFTLocation =              case   
                                                                                                when OnLtnNbr is null then FFTLocationMap.LtnNme2
                                                                                                else OnLtnNbr 
                                                                                  end,
                                         FFTResponseNo = FFTMain.[RspNbr],
                                         FTTResponses  = 1
                                         
                                         
                                         --[Count] = count(1)
              from                 
                                         (
                                                       select 
                                                                     [XmlFileID],[RspNbr],[AwrNbr],[AwrTxt],[SvyNbr],[SvyDsc],[LtnNbr],[LtnNme],[QtnNbr],[DteCrt],[ChangedDate]
                                                       from   
                                                                     [PatientExperience].[Staging].[CRTViewdata]
                                                       where  
                                                                     [QtnNbr] in (86,236,820,850,1062,1071,1072,1076,1083,1192,1308,1385,1388,1390,1483) --FFT Question --maternity 1385,1388,1390,1483
                                                       and           
                                                                     dateadd(month, datediff(month,0,DteCrt),0) = @Month     
                                         ) FFTMain
                                         
              inner join    ( select distinct LtnNbr, LtnNme, LtnNme2,DvnNme, Type 
                                                       from [PatientExperience].dbo.FFTLocationMap 
                                                --     where LtnNme2 = 'ESTU'
                                                --     where LtnNme = 'ICU'
                                                where LtnNme not in ('9M(2)', 'ESTU (RU F)', 'SEYMS', 'Seymour STROKE','SUN')
                                                --and 
                       --where LtnNme2 = '62'
                       )FFTLocationMap
                     ON     FFTMain.[LtnNbr] = FFTLocationMap.[LtnNbr] 

              full outer join      
                                         (
                                                       select 
                                                                     RspNbr,AwrNbr,AwrTxt,DteCrt,LtnNbr
                                                       from   
                                                                     PatientExperience.Staging.CRTViewdata
                                                       where  
                                                                     QtnNbr in (40,442,711,1064,1143) --Discharge Question
                                                       and           
                                                                     AwrNbr in (44)
                                                       and
                                                                     dateadd(month, datediff(month,0,DteCrt),0) = @Month     
                                         ) Discharge
                                         
              on                         FFTMain.RspNbr = Discharge.RspNbr and FFTMain.LtnNbr = Discharge.LtnNbr

              left join     
                                         (
                                                       select  
                                                                            [RspNbr],[DteCrt],QtnTxt,OnDivDepart.[LtnNbr],SelectedLocation,OnLtnNbr,OnDvnNme,OnRtn = FFTOnlineLocation.[Rtn], OnDch = 'Yes' , OnTyp = FFTLocationMap.[Type]
                                                       from
                                                                           (
                                                                                  select 
                                                                                                Department.RspNbr,QtnTxt,Department.DteCrt,Department.LtnNbr,AwrTxtDiv = Division.AwrTxt,AwrTxtDep = Department.AwrTxt,
                                                                                                SelectedLocation =   case   
                                                                                                                                         when Division.AwrTxt is null then Department.AwrTxt
                                                                                                                                         when Department.AwrTxt = Division.AwrTxt then Division.AwrTxt
                                                                                                                                        else Division.AwrTxt+Department.AwrTxt 
                                                                                                                                  end
                                                                                  
                                                                                  from   
                                                                                                PatientExperience.Staging.CRTViewdata Department
                                                                                  
                                                                                                left join       
                                                                                                                     (
                                                                                                                            select 
                                                                                                                                         RspNbr,AwrNbr,DteCrt,LtnNbr,
                                                                                                                                         AwrTxt =      case 
                                                                                                                                                                     when AwrTxt like 'Sain%' then 'SMH' 
                                                                                                                                                                     else AwrTxt 
                                                                                                                                                              end --' in Saint Mary's???
                                                                                                                            from   
                                                                                                                                         Warehousesql.PatientExperience.Staging.CRTViewdata
                                                                                                                            where  
                                                                                                                                         QtnNbr in (1283)  --Division Question
                                                                                                                                                                                                                                   
                                                                                                                     ) Division
                                                                                                       
                                                                                  on            Department.[RspNbr] = Division.[RspNbr] and Department.[DteCrt] = Division.[DteCrt] and Department.[LtnNbr]= Division.[LtnNbr]
                                                                                  
                                                                                  where  
                                                                                                (Department.[QtnNbr] in (1181,1248,1249,1250,1251,1253,1286,1190) or ([QtnNbr] in (1283) and Department.[AwrTxt] in ('MRI - HDU','MRI - ICU'))) --Department Question
                                                                                  and           
                                                                                                dateadd(month, datediff(month,0,Department.[DteCrt]),0) = @Month
                                                                                                                                                
                                                                           ) OnDivDepart
                                                       
                                                       left join  PatientExperience.dbo.FFTOnlineLocation --Online Location                                       
                                                       on            SelectedLocation = OnLtnNme
                                                                                         
                                                       left join PatientExperience.dbo.FFTLocationMap                       
                                                       on LtnNme2 = OnLtnNbr
                                                       
                                                where 
                                                                     dateadd(month, datediff(month,0,DteCrt),0) = @Month

                                                group by 
                                                                     RspNbr,DteCrt,QtnTxt,OnDivDepart.LtnNbr,SelectedLocation,OnLtnNbr,OnDvnNme,FFTOnlineLocation.Rtn,FFTLocationMap.[Type]
                                                                                  
                                         ) SelectedLocation
                                         
              on                         FFTMain.RspNbr = SelectedLocation.RspNbr and FFTMain.LtnNbr = SelectedLocation.LtnNbr

              left join  [PatientExperience].dbo.FFTDivisionMap
                     ON     CASE when FFTLocationMap.DvnNme = 'N/A' then SelectedLocation.OnDvnNme ELSE FFTLocationMap.DvnNme END = FFTDivisionMap.FFTDivision
                                  

              where                
                                  dateadd(month, datediff(month,0,FFTMain.[DteCrt]),0) = @Month
                     and           case when [Type] = 'MAT' then OnTyp else [Type] END = 'FCE'
                     and           FFTDivisionMap.Division <> 'Childrens'
       ) DATA
              
where                                                                
              FFTLocation not in ('SUN','CRC','ETCD','47A','47B','63','64','65','66','Online Ward Survey')
       and    FFTDivision   not in ( 'RMCH','CRT','N/A')
       and FFTDiscAnswer = 'Yes'

-- The Union ensures there is a ward included in the dataset for wards with no responses but
-- have Discharges
UNION ALL

SELECT
       FFTMonth            = @Month
       ,FFTValue            = NULL
       ,FFTDivision  = #t.Division
       ,FFTLocation  = #t.Ward
       ,FFTResponseNo       = NULL
       ,FTTResponses = 0
       ,FTTScore1To5 = 0
       ,FTTScore3To5 = 0
       ,FTTScore1           = 0
       
FROM
       #t

DROP TABLE #t

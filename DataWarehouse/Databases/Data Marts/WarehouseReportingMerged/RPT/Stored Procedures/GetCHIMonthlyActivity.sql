﻿


CREATE PROCEDURE [RPT].[GetCHIMonthlyActivity]
	(
	@Startdate Date
	,@EndDate date
	,@SourceSpecialty varchar(10)
	
	)
AS




--USE [WarehouseReportingMerged]
--declare
--@StartDate as date  = '01 Mar 2013'
--,@EndDate as date = '31 Mar 2013'
--,@SourceSpecialty as varchar(10) = 'ENHI'

----  work out start and end dates

	
	
	
	select
			 PatientNumber 
            ,EpisodeNumber 
			,HopsitalNo = CasenoteNumber
            ,Forename = PatientForename
            ,Surname = PatientSurname
            ,Residance
            ,ConsultantCode = SourceConsultantCode
            ,POD = 
					Case 
						when PatientCategoryCode = 'DC' then 'Day Case'
						when PatientCategoryCode = 'NE' then 'Non Elective'
						when PatientCategoryCode = 'RD' then 'Regular Day'
						else 'Elective' 
					end
            ,EncounterDate1 = AdmissionTime
            ,EncounterDate2 = DischargeTime
			,Location = Ward.SourceWardCode
			
			,Duration = datediff(day,[StartTime],
							dateadd(dd,
						case 
						when [EndActivityCode] in ('TR','DSC')  and ([StartActivityCode] <> 'ADM' )
							then	case 
										when [StartDate] <> [EndDate]
										then	
													case 
															when StartDate = Admissiondate and StartActivityCode = 'TR' then 0
															else 
																	case 
																		when datepart(hour,[EndTime]) > 12 then 1 else 0 
																	end
															end		
											 
													else
															case
																when datediff(hour,[StartTime],[EndTime]) > 12 then 1 else 0 
															end
													end		
											
											
							else 1
												
						end
						,[EndTime]))
			
						--cast(EndTime - StartTime as decimal(10,2))
			,SequenceNo =
						row_number()
								 over (partition by WardStay.ProviderSpellNo order by WardStay.StartTime)

 
from
       APC.WardStay

inner join

       (
        
       select

              PatientNumber = SourcePatientNo
              ,EpisodeNumber = SourceEncounterNo
              ,ProviderSpellNo
              ,CasenoteNumber
              ,PatientForename
              ,PatientSurname
              ,Residance = coalesce(PatientAddress4,PatientAddress3,PatientAddress2)
              ,SourceConsultantCode
              ,PatientCategoryCode
              ,AdmissionTime
              ,DischargeTime
              ,Admissiondate = CalendarAdmitDate.TheDate
					
       from

              APC.Encounter

 

       inner join WH.Specialty

       on     Specialty.SourceSpecialtyID = Encounter.SpecialtyID
       and    Specialty.SourceSpecialtyCode = @SourceSpecialty 
       
       Inner join WH.Consultant
       on	Consultant.SourceConsultantID = Encounter.ConsultantID


       inner join WH.Calendar
       on     Calendar.DateID = Encounter.DischargeDateID
       and    TheDate between @Startdate and @EndDate
       
       inner join WH.Calendar CalendarAdmitDate
       on     CalendarAdmitDate.DateID = Encounter.AdmissionDateID
      
      

       where

              DischargeDateID is not null

       and    EpisodeStartDateID = AdmissionDateID

       ) Spell

on     Spell.ProviderSpellNo = WardStay.ProviderSpellNo

 

inner join WH.Ward

on     Ward.SourceWardID = WardStay.WardID

---     Add Outpatient Data

union

--use WarehouseReportingMerged

--declare

--@StartDate as date  = '01 Apr 2012'
--,@EndDate as date = '30 Nov 2012'

Select

	 PatientNumber = OPEncounter.SourcePatientNo
    ,EpisodeNumber = OPEncounter.SourceEncounterNo
	,HospitalNo = OPEncounter.CasenoteNo
	,Forename = OPEncounter.PatientForename
	,Surname = OPEncounter.PatientSurname
	,Residence = coalesce(OPEncounter.PatientAddress4,OPEncounter.PatientAddress3,OPEncounter.PatientAddress2)
	,ConsultantCode = SourceConsultantCode
	,POD = 
			case
				when SourceFirstAttendanceCode  = '1' then 'New' else 'Follow Up' 
			end
	,EncounterDate1 = OPEncounter.AppointmentTime
	,EncounterDate2 = FutureAppointment.AppointmentTime
	,Location  =	(	Select 
							SourceClinicCode
						from OP.Clinic
						where Clinic.[SourceClinicID] = FutureAppointment.ClinicID 
					)
						
	,Duration = Null
	,SequenceNo =
						row_number()
								 over (partition by OPEncounter.SourcePatientNo,OPEncounter.SourceEncounterNo,OPEncounter.AppointmentTime order by OPEncounter.AppointmentTime)

	
	from OP.Encounter OPEncounter
	
              
	
	join WH.Specialty

       on     Specialty.SourceSpecialtyID = OPEncounter.ReferralSpecialtyID
       and    Specialty.SourceSpecialtyCode = @SourceSpecialty 
       
    join WH.Consultant
		on consultant.SourceConsultantID = OPEncounter.ConsultantID
    
    join OP.FirstAttendance on
        FirstAttendance.SourceFirstAttendanceID = OPEncounter.DerivedFirstAttendanceID
		and [SourceFirstAttendanceCode] in ('1','2')
    
    join OP.Clinic on
		clinic.SourceClinicID = OPEncounter.ClinicID
    
	
	left outer join OP.Encounter FutureAppointment
	
		on FutureAppointment.SourcePatientNo = OPEncounter.SourcePatientNo
		and FutureAppointment.SourceEncounterNo = OPEncounter.SourceEncounterNo
		and FutureAppointment.AppointmentTime > OPEncounter.AppointmentTime
		and FutureAppointment.AppointmentStatusCode = 'NR'
		and FutureAppointment.CancelledByCode is Null
		
	
		
	
	
	
	where 
	OPEncounter.AppointmentTime between @StartDate and @EndDate
	and (
			left(OPEncounter.AppointmentStatusCode,3) in ('ATT','WLK') 
			or 
			OPEncounter.AppointmentStatusCode = 'SATT'
		)
	

 













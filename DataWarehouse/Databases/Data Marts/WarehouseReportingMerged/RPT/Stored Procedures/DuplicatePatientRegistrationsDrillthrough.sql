﻿

CREATE PROCEDURE [RPT].[DuplicatePatientRegistrationsDrillthrough]		-- [RPT].[DuplicatePatientRegistrationsDrillthrough] '24 feb 2014','2 mar 2014' 
(
	 @StartDate	DATE
	,@EndDate	DATE
)
AS

	SELECT
		 MasterSourcePatientNo			= DRILL.MasterSourcePatientNo
		,SourcePatientNo				= DUP.SourcePatientNo
		,RegistrationDate				= DUP.RegistrationDate
		,DistrictNo						= DUPPAT.DistrictNo
		,NHSNumber						= DUPPAT.NHSNumber
		,Surname						= DUPPAT.Surname
		,Forename						= DUPPAT.Forenames
		,DateOfBirth					= DUPPAT.DateOfBirth
		,Sex							= DUPPAT.SexCode
		,PostCode						= DUPPAT.Postcode
		,AddressLine1					= DUPPAT.AddressLine1
		,AddressLine2					= DUPPAT.AddressLine2
		,AddressLine3					= DUPPAT.AddressLine3
		,AddressLine4					= DUPPAT.AddressLine4
		,GPCode							= DUPPAT.RegisteredGpCode

	FROM
		(
			SELECT DISTINCT
				 MasterSourcePatientNo
			FROM
				RPT.fn_DuplicatePatientRegistrations(@StartDate, @EndDate) 
		) DRILL
		
		INNER JOIN Warehouse.PAS.PatientDuplicationBase DUP
			ON		DRILL.MasterSourcePatientNo = DUP.MasterSourcePatientNo

		INNER JOIN Warehouse.PAS.Patient DUPPAT
			ON DUP.SourcePatientNo = DUPPAT.SourcePatientNo

	ORDER BY
		 MasterSourcePatientNo
		,RegistrationDate DESC
		,DistrictNo




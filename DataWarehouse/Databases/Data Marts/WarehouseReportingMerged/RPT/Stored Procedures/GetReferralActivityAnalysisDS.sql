﻿


CREATE PROCEDURE [RPT].[GetReferralActivityAnalysisDS]


(      
       @CCGCode VARCHAR(MAX)= 'All'
       ,@Division VARCHAR(MAX)= 'ALL'
	   ,@Directorate VARCHAR(MAX)= 'ALL'
       ,@Specialtycode VARCHAR(MAX)= NULL 
       ,@GPPractice VARCHAR(MAX)= 'All'
)

AS

--DECLARE	 @CCGCode VARCHAR(MAX)= 'All'
--DECLARE	@Division VARCHAR(MAX)= NULL
--DECLARE	@Specialtycode VARCHAR(MAX)= NULL	
--DECLARE	@GPPractice VARCHAR(MAX)= 'All'


BEGIN
       SET NOCOUNT ON;

DECLARE @TheDate Date = convert(date,getdate(),113)
DECLARE @EndOfWeekRange Date= DATEADD(dd, -datepart(dw, @TheDate)+1, @TheDate)
DECLARE @StartOfWeekRange Date = DATEADD(dd,1,DATEADD(WEEK, -52, @EndOfWeekRange))
DECLARE @EndofDayRange Date = @TheDate
DECLARE @StartOfDayRange Date =  DATEADD(dd,-56,@TheDate)
DECLARE @StartofMonthRange Date =  DATEADD(month, datediff(month, 0, @TheDate) - 25, 0)
DECLARE @EndofMOnthRange Date =  DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@TheDate),0))

SELECT 
        RDay = E.referraldate
       ,RWeek = LastDayOfWeek
       ,Rmonth = C.FinancialMonthKey
       ,TotRefs = COUNT(E.SourceEncounterNo)
       ,Dflag =      CASE 
                                  WHEN E.referraldate >= @StartOfDayRange 
                                   AND E.referraldate < DATEADD(dd,1,@EndofDayRange)
                                         THEN 1        
                                  ELSE 0        
                           END
       ,Wflag =      CASE 
                                  WHEN E.referraldate >= @StartOfWeekRange 
                                  AND E.referraldate < DATEADD(dd,1,@EndofWeekRange)
                                          THEN 1        
                                  ELSE 0
                           END           
       ,Yflag =      CASE 
                           WHEN E.referraldate >= @StartOfMonthRange 
                            AND E.referraldate < DATEADD(dd,1,@EndofMonthRange)
                                         THEN 1        
                                  ELSE 0        
                           END
       ,Div = Directorate.Division
       ,Spec = Specialty.NationalSpecialty
FROM     
	RF.Encounter E 
INNER JOIN WH.Calendar AS C ON E.referraldate = C.thedate
LEFT OUTER join wh.Specialty Specialty 
	ON Specialty.SourceSpecialtyID = E.SpecialtyID

LEFT OUTER  join wh.Directorate Directorate	
	ON Directorate.DirectorateCode = E.DirectorateCode

LEFT OUTER join WH.CCG CCG 
	ON CCG.CCGCode = E.CCGCode

LEFT OUTER join  Organisation.dbo.Practice 
	ON Practice.OrganisationCode = E.EpisodicGpPracticeCode

WHERE  
       E.referraldate >= @StartofMonthRange
	   	AND 
		(
			NationalSpecialtyCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@specialtycode,','))
		OR
			@specialtycode IS NULL
		)
			AND 
		(
			Directorate.Division IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
		OR
			@Division ='All'
		)
		
		AND 
		(
			Directorate.DirectorateCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Directorate,','))
		OR
			@Directorate  ='All'
		)

       	AND 
		(
			Practice.OrganisationCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@GPPractice,','))
		OR
			@GPPractice ='All'
		)
			AND 
		(
			CCG.CCGCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@CCGCode,','))
		OR
			@CCGCode = 'All'
		)
GROUP BY 

        E.referraldate
       ,C.LastDayOfWeek
       ,C.FinancialMonthKey
       ,Directorate.Division
       ,Specialty.NationalSpecialty

END;




﻿


CREATE Procedure [RPT].[DementiaPerformanceYear] 

as


Declare @Census datetime =cast(getdate() as date)
Declare @PreviousStart datetime = (
							Select
								Calendar.TheDate
							from
								WH.Calendar
							inner join WH.Calendar Census
							on Census.FinancialYear = Calendar.FinancialYear
							and	Census.TheDate = dateadd(day,-365,@Census)
							where
								not exists
								(
								select
									1
								from
									WH.Calendar Earliest
								where
									Calendar.FinancialYear = Earliest.FinancialYear
								and Earliest.TheDate < Calendar.TheDate
								)
							)
						
Select
	ReportingMonth
	,Q1Performance = 
				cast(
					(
						(cast(sum(Q1Numerator) as float)) 
					/ 
						(cast(sum(Q1Denominator)as float))
					)*100
				as int)
	,Q2Performance = 
				case
					when sum(Q2Denominator) = 0 then 0
					else
						cast(
							(
								(cast(sum(Q2Numerator) as float)) 
							/ 
								(cast(sum(Q2Denominator)as float))
							)*100
						as int)
				end
	,Q3Performance = 
				case
					when sum(Q3Denominator) = 0 then 0
					else
						cast(
							(
								(cast(sum(Q3Numerator) as float)) 
							/ 
								(cast(sum(Q3Denominator)as float))
							)*100
						as int)
				end
from
	APC.DementiaCQUIN

inner join WH.Calendar
on ReportingMonth = TheDate

where
	ReportingMonth between @PreviousStart and @Census

group by
	ReportingMonth
	

	


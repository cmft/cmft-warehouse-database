﻿

create procedure [RPT].[AcuteMedPhysioOPasAE]

as

SELECT 	
	 AppointmentTime
	,SourceConsultant
	,SourceClinic
	,CaseNoteNo
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,NHSNumber
	,DistrictNo
	,NationalSpecialtyCode
	
FROM 	
	OP.Encounter
	
	left join WH.Consultant 
	on ConsultantID = SourceConsultantID
	
	left join OP.clinic
	on ClinicID = SourceClinicID
	
	left join WH.Specialty 
	on TreatmentFunctionID = SourceSpecialtyID
	
WHERE 	
	AppointmentTime >= '01 Jan 12'
	and AppointmentStatusCode in ('ATT', 'SATT', 'WLK')
	and NationalSpecialtyCode = '180'


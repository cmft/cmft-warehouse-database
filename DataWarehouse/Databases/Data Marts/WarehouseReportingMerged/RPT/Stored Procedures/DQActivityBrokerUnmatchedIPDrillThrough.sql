﻿
/******************************************************************************
**  Name: RPT.DQActivityBrokerUnmatchedIPDrillThrough
**  Purpose: Data Quality drill through report for un-matched ActivityBroker
**           IP transfer patients
**
**  Parent Stored procedure : RPT.DQActivityBrokerUnmatchedPatients
**
**	Input Parameters
**	
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 14.11.13      MH       Created
******************************************************************************/

CREATE PROCEDURE RPT.DQActivityBrokerUnmatchedIPDrillThrough
(
	@ReportType		NVARCHAR(10)
)
AS
	DECLARE @SnapshotTime	DATETIME
	DECLARE @NullDate		DATE = '1 Jan 1900'

-- Get the time the data was built	
	SELECT 
		@SnapshotTime = MAX(EventTime)
	FROM
		ActivityBroker.Utility.AuditLog
		
	SELECT 
		  SourceUniqueId				= ORIGAPCSPELL.APCSpellID
		 ,MeasureDifference				= CASE
											WHEN DESTAPCSPELL.APCSpellID IS NULL THEN 'No Match'
											ELSE 'Data Mismatch'
										  END
		 ,[District Number]				= ORIGAPCSPELL.DistrictNo
		 ,[NHS Number]					= PAT.NHSNumber
		 --,Division					= ORIGAPCSPELL.
		 ,[Discharge Site]				= COALESCE(ORIGAPCSPELLSITE.NationalSite,ORIGAPCSPELLSITE2.NationalSite)
		 ,Speciality					= ORIGSPEC.NationalSpecialty
		 ,DischargeDestination			= COALESCE(ORIGDISCHCEN.SourceDischargeDestination, ORIGDISCHTRA.SourceDischargeMethod)
		 ,AdmissionSourceCode			= COALESCE(DESTADMCEN.SourceAdmissionSource, DESTADMTRA.SourceAdmissionMethod)
		 ,DischargeTime					= ORIGAPCSPELL.DischargeTime
		 ,AdmissionTime					= DESTAPCSPELL.AdmissionTime
		 ,DischargeWardCode				= ORIGAPCSPELL.DischargeWardCode
		 ,AdmissionWardCode				= DESTAPCSPELL.AdmissionWardCode
		 ,MismatchDischargeAdmission	= CASE 
											WHEN		DESTAPCSPELL.APCSPELLID IS NOT NULL 
													AND DATEDIFF(HOUR, ORIGAPCSPELL.DischargeTime, DESTAPCSPELL.AdmissionTime) > 2
											THEN DESTAPCSPELL.AdmissionTime
											ELSE NULL
										  END
													 
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		
		INNER JOIN ActivityBroker.AB.Pathway P
			ON ABT.PathwayID = P.PathwayID
			
		LEFT JOIN ActivityBroker.AB.APCSpellTransferBase ORIGAPCSPELL
			ON P.OriginAPCSpellID		= ORIGAPCSPELL.APCSpellID
			
		LEFT JOIN ActivityBroker.AB.APCSpellTransferBase DESTAPCSPELL
			ON P.DestinationAPCSpellID	= DESTAPCSPELL.APCSpellID
			
		LEFT JOIN WH.Specialty ORIGSPEC
			ON		ORIGAPCSPELL.ContextCode	= ORIGSPEC.SourceContextCode
				AND ORIGAPCSPELL.SpecialtyCode	= ORIGSPEC.SourceSpecialtyCode
				
		LEFT JOIN ActivityBroker.SyncEngine.Patient PAT
			ON		ORIGAPCSPELL.SourcePatientNo = PAT.SourcePatientNo
			
		-- The Trafford Discharge Destination code data is mapped to a Discharge Method code, 
		-- whereas the Central data will map to a Discharge Destination code
		LEFT JOIN ActivityBroker.APC.DischargeDestination ORIGDISCHCEN
			ON		ORIGAPCSPELL.DischargeDestinationCode	= ORIGDISCHCEN.SourceDischargeDestinationCode
				AND ORIGAPCSPELL.ContextCode				= ORIGDISCHCEN.SourceContextCode
				
		LEFT JOIN ActivityBroker.APC.DischargeMethod ORIGDISCHTRA
			ON		ORIGAPCSPELL.DischargeDestinationCode	= ORIGDISCHTRA.SourceDischargeMethodCode
				AND ORIGAPCSPELL.ContextCode				= ORIGDISCHTRA.SourceContextCode
				
		-- The Trafford Admission Source Code data is mapped to a Admission Method code, 
		-- whereas the Central data will map to a Admission Source Code
		LEFT JOIN ActivityBroker.APC.AdmissionSource DESTADMCEN
			ON		DESTAPCSPELL.AdmissionSourceCode		= DESTADMCEN.SourceAdmissionSourceCode
				AND DESTAPCSPELL.ContextCode				= DESTADMCEN.SourceContextCode
				
		LEFT JOIN ActivityBroker.APC.AdmissionMethod DESTADMTRA
			ON		DESTAPCSPELL.AdmissionSourceCode		= DESTADMTRA.SourceAdmissionMethodCode
				AND DESTAPCSPELL.ContextCode				= DESTADMTRA.SourceContextCode
				
		LEFT JOIN WH.Site ORIGAPCSPELLSITE
			ON		ORIGAPCSPELL.SiteCode					= ORIGAPCSPELLSITE.SourceSiteCode
				AND ORIGAPCSPELL.ContextCode				= ORIGAPCSPELLSITE.SourceContextCode
				
		-- To handle a mis-match on context code and corresponding site code
		-- eg actual data is context code TRA||UG with Site Code of TRAF
		LEFT JOIN WH.Site ORIGAPCSPELLSITE2
			ON		ORIGAPCSPELL.SiteCode					= ORIGAPCSPELLSITE2.SourceSiteCode
				AND ORIGAPCSPELLSITE2.SourceContextCode		= 'CEN||PAS'

	WHERE
			COALESCE(DESTAPCSPELL.APCSpellID, -1) = CASE WHEN @ReportType = 'IPTNM' THEN -1 ELSE DESTAPCSPELL.APCSpellID END
		AND P.PathwayTypeID = 3
		AND	(
					DESTAPCSPELL.APCSpellID IS NULL
				OR	DATEDIFF(HOUR, ORIGAPCSPELL.DischargeTime, DESTAPCSPELL.AdmissionTime) > 2
			)
			
	ORDER BY
		ORIGAPCSPELL.DistrictNo

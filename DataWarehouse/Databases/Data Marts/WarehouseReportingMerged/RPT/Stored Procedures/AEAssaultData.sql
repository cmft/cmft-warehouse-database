﻿
CREATE proc [RPT].[AEAssaultData]



(
@ArrivalDateStart datetime,
@ArrivalDateEnd datetime
)
as

--declare @ArrivalDateStart datetime = (convert(varchar(6),dateadd(m,-1,getdate()),112) + '01');

--declare @ArrivalDateEnd datetime = dateadd(d,-1,(convert(varchar(6),getdate(),112) + '01'));


SELECT 
	SiteName = Site.SourceSite 
	,Encounter.EncounterRecno
	,LocalPatientID = Encounter.DistrictNo
	,Encounter.AttendanceNumber
	,NHSNumber = ISNULL(Encounter.NHSNumber,NHSNumbers.NHSNumber)
	,Encounter.DateOfBirth
	,Sex =
		CASE
		WHEN Encounter.SexCode = 'M' THEN '01'
		WHEN Encounter.SexCode = 'F' THEN '02'
		ELSE Encounter.SexCode
		END
	,Encounter.Postcode
	,ArrivalModeNationalCode = ISNULL(ArrivalMode.NationalArrivalModeCode,'2')
	,Encounter.AttendanceCategoryCode
	,AttendanceDisposalCode = ISNULL(AttendanceDisposal.NationalAttendanceDisposalCode,'14')
	,IncidentLocationTypeCode = 
		CASE 
		WHEN Encounter.IncidentLocationTypeCode = 0 THEN '91' 
		ELSE ISNULL(Encounter.IncidentLocationTypeCode,'91') 
		END	
	,PatientGroupCode = 
		CASE
		WHEN PatientGroupCode = 0 THEN '80'
		ELSE PatientGroupCode
		END
	,SourceOfReferralCode = 
		CASE
		WHEN Encounter.SourceOfReferralCode = '' THEN '08'
		ELSE ISNULL(Encounter.SourceOfReferralCode,'08')
		END	
	,Encounter.ArrivalDate
	,Encounter.ArrivalTime
	,Encounter.AgeOnArrival
	,NationalEthnicOriginCode = ISNULL(EthnicCategory.NationalEthnicCategoryCode,'99')

	,Encounter.AlcoholLocation
	,Encounter.AlcoholInPast12Hours
	,AssaultDate
	,AssaultTime
	,AssaultWeapon
	,AssaultWeaponDetails
	,AssaultLocation
	,AssaultLocationDetails
	,AlcoholConsumed3Hour
	,AssaultRelationship
	,AssaultRelationshipDetails
	,ReportStartDate = @ArrivalDateStart
	,ReportEndDate = @ArrivalDateEnd
	,CaseCount = 1
	
	
	
		FROM 
			AE.Encounter Encounter

		inner join WH.Site Site
		on Site.SourceSiteID = Encounter.SiteID
		and site.SourceContextCode = encounter.ContextCode
		
		LEFT JOIN AE.ArrivalMode ArrivalMode
		ON	ArrivalMode.SourceArrivalModeID = Encounter.ArrivalModeID

		LEFT JOIN [AE].[AttendanceDisposal] AttendanceDisposal
		ON AttendanceDisposal.SourceAttendanceDisposalID = Encounter.AttendanceDisposalID

		left join WH.EthnicCategory EthnicCategory
		on EthnicCategory.SourceEthnicCategoryID = Encounter.EthnicOriginID
		and EthnicCategory.SourceContextCode = Encounter.ContextCode
 
		LEFT JOIN AE.AssaultEncounter Assault 
		ON	Assault.MergeEncounterRecno = Encounter.MergeEncounterRecno

--this query will get us missing NHS Numbers
--this one is on Live
		LEFT JOIN
				(select 
					NHSNumber
					,PatientSurname
					,PatientForename
					,DateOfBirth
					,PostCode
				from [Information].[dbo].[NHS_number_internal_reference]) NHSNumbers
		ON NHSNumbers.PatientForename = Encounter.PatientForename
		AND NHSNumbers.PatientSurname = Encounter.PatientSurname
		AND NHSNumbers.DateOfBirth = Encounter.DateOfBirth
		AND NHSNumbers.PostCode = Encounter.PostCode



WHERE
	
	
	arrivaldate BETWEEN @ArrivalDateStart AND @ArrivalDateEnd

	
	AND Encounter.reportable = 1



ORDER BY
	Encounter.DistrictNo ASC
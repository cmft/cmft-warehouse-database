﻿


CREATE Procedure [RPT].[DementiaExceptionsWard] 
	@StartDate date 
	,@EndDate date 

as

Declare @LocalStart datetime = cast(@StartDate as datetime)
		,@LocalEnd datetime = (cast(@EndDate as datetime) + '23:59')
		

Select 
	WardCode
	,TimeOnWard = sum(TimeOnWard)
	,TimeOnWardHrs = (sum(TimeOnWard)/60)
	,Sequence = row_number() over (
									order by sum(TimeOnWard) desc
									)
	,[Count] = count(*)
from
	(	
	Select
		DementiaCQUIN.GlobalProviderSpellNo
		,WardCode
		,TimeOnWard = datediff(mi,WardStay.StartTime,
								case
									when WardStay.EndTime < dateadd(mi,4320,DementiaCQUIN.AdmissionTime) 
									then WardStay.EndTime
									else dateadd(mi,4320,DementiaCQUIN.AdmissionTime) 
								end
								)
	from
		APC.DementiaCQUIN

	inner join APC.Encounter
	on DementiaCQUIN.GlobalProviderSpellNo = Encounter.ProviderSpellNo
	and GlobalEpisodeNo = 1

	inner join APC.WardStay
	on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo

	where
		ReportingMonth between @LocalStart and @LocalEnd
	and Q1Numerator = 0
	and WardStay.StartTime between DementiaCQUIN.AdmissionTime and dateadd(mi,4320,DementiaCQUIN.AdmissionTime)
	)
	Dementia

group by
	WardCode



﻿

CREATE Procedure [RPT].[DementiaExceptions] 
	@StartDate date 
	,@EndDate date 

as

Declare @LocalStart datetime = cast(@StartDate as datetime)
		,@LocalEnd datetime = (cast(@EndDate as datetime) + '23:59')
		
		
Select
	DementiaFreeze.GlobalProviderSpellNo
	,WardCode
	,Sequence = row_number() over (partition by DementiaFreeze.GlobalProviderSpellNo
									order by StartTime
									)
	,TimeOnWard = datediff(mi,WardStay.StartTime,
							case
								when WardStay.EndTime < dateadd(mi,4320,DementiaFreeze.AdmissionTime) 
								then WardStay.EndTime
								else dateadd(mi,4320,DementiaFreeze.AdmissionTime) 
							end
							)

from
	APC.DementiaFreeze

inner join APC.Encounter
on DementiaFreeze.GlobalProviderSpellNo = Encounter.ProviderSpellNo
and GlobalEpisodeNo = 1

inner join APC.WardStay
on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo

where
	ReportingMonth between @LocalStart and @LocalEnd
and Q1Numerator = 0
and WardStay.StartTime between DementiaFreeze.AdmissionTime and dateadd(mi,4320,DementiaFreeze.AdmissionTime)




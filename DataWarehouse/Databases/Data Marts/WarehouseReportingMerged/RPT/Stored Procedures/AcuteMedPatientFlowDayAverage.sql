﻿

create procedure [RPT].[AcuteMedPatientFlowDayAverage]

as

Select					
	 Day =DayOfWeek				
	,Average = Count(1)/52				
					
from					
	AE.Encounter				
					
join WH.Site 					
on SiteID= Sourcesiteid					
					
join AE.AttendanceCategory					
on AttendanceCategoryID = SourceAttendanceCategoryID					
					
join WH.Calendar					
on EncounterEndDateID = DateID					
					
where					
	NationalSiteCode = 'RW3MR'				
	and AttendanceCategoryGroup = 'Attender'				
	and Site.SourceContext = 'Symphony (Central)'				
	and TheDate < GetDate()				
	and TheDate > DateAdd(day, -365,GetDate())				
					
Group By					
	 DayOfWeek				
	,DayOfWeekKey				
					
Order by					
	DayOfWeekKey				
					


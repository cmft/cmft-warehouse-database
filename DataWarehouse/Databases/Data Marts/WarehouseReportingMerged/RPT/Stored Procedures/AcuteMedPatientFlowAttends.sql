﻿

create procedure [RPT].[AcuteMedPatientFlowAttends]

As
				
Select					
	 Date = Calendar.TheDate				
	,[ED Attends] = Activity				
	,[Total Admissions]				
	,[MAU Admissions] = Admissions				
	,[MAU Discharges] = Discharges				
	,[ESTU Admissions]				
					
from 					
	WH.Calendar				
					
Join					
					
	(					
	Select					
		 TheDate				
		,[ESTU Admissions] = Count(1)	
	from					
		APC.Encounter				
					
	join WH.Site 					
	on SiteID= Sourcesiteid					
					
	join WH.Calendar					
	on EpisodeStartDateID = DateID					
					
	join APC.Ward					
	on StartWardID = SourceWardID					
					
	join WH.Specialty					
	on SpecialtyID = SourceSpecialtyID					
					
	join WH.Directorate					
	on	Directorate.DirectorateCode	= Encounter.StartDirectorateCode
					
	join APC.AdmissionMethod					
	on AdmissionMethodID = SourceAdmissionMethodID					
					
	where					
		Site.SourceContext = 'Torex PAS (Central)'				
		and TheDate < DateAdd(day, -1,GetDate())				
		and TheDate > DateAdd(day, -22,GetDate())				
		and FirstEpisodeInSpellIndicator = 1				
		and SourceWardCode = 'ESTU' 				
		and NationalAdmissionMethodCode = '21'				
					
					
	Group By					
		TheDate				
	)ESTU					
on Calendar.TheDate = ESTU.TheDate					
					
join					
	(					
	Select					
		 TheDate				
		,Discharges = Count(1)				
					
	from					
		APC.Encounter				
					
	join WH.Site 					
	on SiteID = Sourcesiteid					
					
	join WH.Calendar					
	on DischargeDateID = DateID					
					
	join APC.Ward					
	on StartWardID = SourceWardID					
					
	join WH.Specialty					
	on SpecialtyID = SourceSpecialtyID					
					
	join WH.Directorate					
	on Encounter.StartDirectorateCode = Directorate.DirectorateCode
					
	where					
		Site.SourceContext = 'Torex PAS (Central)'				
		and TheDate < DateAdd(day, -1,GetDate())				
		and TheDate > DateAdd(day, -22,GetDate())				
		and FirstEpisodeInSpellIndicator = 1				
		and SourceWardCode in ('MAU', 'TMAU', '15M', 'MRU', '7M','OMU','AMU') 				
		and NationalSpecialtyCode <> '180'				
		and Division = 'Acute Medical'				
					
	Group By					
		TheDate				
	) Discharges					
on Calendar.TheDate = Discharges.TheDate					
					
join					
(					
Select					
	 TheDate				
	,Activity = Count(1)				
	,[Total Admissions] = sum				
		(			
		case NationalAttendanceDisposalCode 			
			when '01' then 1		
			else 0		
		end			
		)			
					
from					
	AE.Encounter				
					
join WH.Site 					
on SiteID= Sourcesiteid					
					
join AE.AttendanceCategory					
on AttendanceCategoryID = SourceAttendanceCategoryID					
					
join WH.Calendar					
on EncounterEndDateID = DateID					
					
join AE.AttendanceDisposal					
on AttendanceDisposalID = SourceAttendanceDisposalID					
					
where					
	NationalSiteCode = 'RW3MR'				
	and AttendanceCategoryGroup = 'Attender'				
	and Site.SourceContext = 'Symphony (Central)'				
	and TheDate < DateAdd(day, -1,GetDate())				
	and TheDate > DateAdd(day, -22,GetDate())				
					
Group By					
	TheDate				
)Activity					
on Calendar.TheDate = Activity.TheDate					
					
join					
(					
Select					
	 TheDate				
	,Admissions = Count(1)				
					
from					
	APC.Encounter				
					
join WH.Site 					
on SiteID= Sourcesiteid					
					
join WH.Calendar					
on EpisodeStartDateID = DateID					
					
join APC.Ward					
on StartWardID = SourceWardID					
					
join WH.Specialty					
on SpecialtyID = SourceSpecialtyID					
					
join WH.Directorate					
on Encounter.StartDirectorateCode = Directorate.DirectorateCode
					
where					
	Site.SourceContext = 'Torex PAS (Central)'				
	and TheDate < DateAdd(day, -1,GetDate())				
	and TheDate > DateAdd(day, -22,GetDate())				
	and FirstEpisodeInSpellIndicator = 1				
	and SourceWardCode in ('MAU', 'TMAU', '15M', 'MRU', '7M','OMU','AMU') 				
	and NationalSpecialtyCode <> '180'				
	and Division = 'Acute Medical'				
					
Group By					
	TheDate				
					
)Admissions					
on Calendar.TheDate = Admissions.TheDate					


﻿


	

CREATE PROCEDURE [RPT].[CreatinineAlert] 
(
	 @ReportType varchar(10) -- RATE, CHANGE or VALUE, RR added Latest
	,@RateOfChange int -- 
	,@Stage varchar (100) -- (Stage1 (>=1.5 to <2.0); Stage2 (>=2.0 to <3.0); Stage3 (>=3.0) 
	,@NationalAlert varchar(100) -- NationalAlert = AKI 1;AKI 2;AKI 3;No Alert;No Alert - Repeat;Suggest??
	,@ResultValue int
	,@Summary bit
	,@DistrictNo char(8)
	,@Interval numeric(21 , 10)
	,@FromDate date
	,@ToDate date
	,@CurrentInpatient tinyint
	,@PatientCategory varchar(2)
	
	--,@PercentageChange numeric(21 , 10)
	--,@FromPercentageChange numeric(21 , 10)
	--,@ToPercentageChange numeric(21 , 10)
	
)

as

set dateformat dmy

select
	 MergeResultRecno
	 ,PatientForename
	,PatientSurname
	,DistrictNo
	,CasenoteNumber
	,AgeOnAdmission
	,Sex
	,AdmissionTime
	,DischargeTime
	,PatientCategory
	,WardEpisodeStart
	,PatientSequence
	,SpellSequence
	,LinkageType
	,Result
	,Result.EffectiveTime
	,Result.ResultTime
	,Result.PreviousResult
	,Result.PreviousResultTime
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
	,CurrentInpatient
	,CurrentWard
	,LengthOfStay
	,DiedInHospital
	,Result.Stage
	--,Sequence.Pattern
	,ReportSequenceNo
	,C1Result	
	,RatioDescription	
	,ResultForRatio	
	,RVRatio	
	,NationalAlert
	,InHospitalAKI
	,ResultAPCLinked
	,AKIFirstAlertSpellSequence
	,AKIFirstAlertInSpellTime
	,AKIFirstAlertInSpellRatio
	,AKIFirstAlertInSpellStage
	,AKILoS

from
	(
	Select
		MergeResultRecno
		,PatientForename
		,PatientSurname
		,DistrictNo
		,CasenoteNumber
		,AgeOnAdmission
		,Sex = Result.SexCode
		,AdmissionTime
		,DischargeTime
		,PatientCategory = PatientCategoryCode 
		,WardEpisodeStart
		,WardEpisodeEnd
		,PatientSequence
		,SpellSequence
		,LinkageType
		,Result.Result
		,Result.EffectiveTime
		,Result.ResultTime
		,PreviousResult
		,PreviousResultTime
		,BaselineResult
		,BaselineResultTime
		,BaselineToCurrentDays
		,BaselineToCurrentChange
		,CurrentToBaselineResultRatio
		,Stage
		,CurrentInpatient
		,CurrentWard
		,LengthOfStay = DATEDIFF(day,cast(AdmissionTime as date),coalesce(cast(DischargeTime as date),getdate()))
		,DiedInHospital = 
				case
					when exists
						(
						Select
							1
						from
							APC.DischargeMethod
						where
							DischargeMethod.SourceDischargeMethodCode = Result.DischargeMethodCode
						and DischargeMethod.NationalDischargeMethod in ('Patient DiEd','Stillbirth')
						)
					then 1
					else 0
				end
		
		--,Result.[PreviousSpell(12mths)WithRenalEpisode]
		,ReportSequenceNo =
			case
			when @ReportType = 'RATE' then row_number () over (partition by Result.DistrictNo order by Result.ResultRateOfChangePer24Hr desc)
			when @ReportType = 'CHANGE' then row_number () over (partition by Result.DistrictNo,Result.GlobalProviderSpellNo order by Result.CurrentToBaselineResultRatio desc)
			when @ReportType = 'LATEST' then  row_number () over (partition by Result.DistrictNo order by Result.ResultTime desc)
			when @ReportType = 'VALUE' then  row_number () over (partition by Result.DistrictNo,Result.GlobalProviderSpellNo order by Result.Result desc)
			end
		
		,C1Result	
		,RatioDescription	
		,ResultForRatio	
		,RVRatio	
		,NationalAlert
		,InHospitalAKI
		,ResultAPCLinked
		,AKIFirstAlertSpellSequence
		,AKIFirstAlertInSpellTime 
		,AKIFirstAlertInSpellRatio
		,AKIFirstAlertInSpellStage
		,AKILoS
	FROM
		Result.AcuteKidneyInjuryAlert Result
	where
		Result.LinkageType in (
					'AEAPC'
					,'APC'
					,'AE'
					)
	and AgeOnAdmission >=18
	and RenalPatient = 0
	) 
	Result


WHERE 
	cast(Result.ResultTime as date) between @FromDate and @ToDate
and	
	(
		CurrentInpatient = @CurrentInpatient
	or	
		@CurrentInpatient is null
	)
and	
	(
		PatientCategory = @PatientCategory
	or	
		@PatientCategory is null
	)
and 
	(
		NationalAlert in (Select Value from RPT.SSRS_MultiValueParamSplit(@NationalAlert,','))
	or 
		NationalAlert is null
	)
and	
	(
		(
			@ReportType = 'CHANGE'
		and 
			(
				Stage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
			or
				Stage is null
			)
		and	Result.ReportSequenceNo = @Summary
		)
	or	
		(
			@ReportType = 'Latest'
		and	
			(
				Stage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
			or
				Stage is null
			)
		and	Result.ReportSequenceNo = @Summary
		)
	or	
		(
			@ReportType = 'VALUE'
		and	Result >= @ResultValue
		and	ReportSequenceNo = @Summary
		)
	)

ORDER BY
	 Result.DistrictNo
	,Result.ResultTime desc
	




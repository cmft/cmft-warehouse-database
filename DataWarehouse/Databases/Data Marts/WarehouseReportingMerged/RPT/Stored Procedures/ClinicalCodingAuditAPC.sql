﻿



	CREATE PROC	[RPT].[ClinicalCodingAuditAPC] 
	--'1 april 2014' ,'1 may 2014' 
	 @StartDate datetime = null
	,@EndDate datetime = null
	
	as


	SELECT
		
		 PROCODE3 = PROCODET
		,PCT
		,PROVSPNO
		,EPIORDER
		,ADMIDATE
		,DISDATE
		,EPISTART
		,EPIEND
		,AGE = STARTAGE
		,SEX 
		,CLASSPAT
		,ADMISORC = case when isnumeric(ADMISORC) = 1 then ADMISORC else ' ' end
		,ADMIMETH = case when isnumeric(ADMIMETH) = 1 then ADMIMETH else ' ' end
		,DISDEST = case when isnumeric(DISDEST) = 1 then DISDEST else ' ' end
		,DISMETH = case when isnumeric(DISMETH) = 1 then DISMETH else ' ' end
		,EPIDUR
		,MAINSPEF = case when isnumeric(MAINSPEF) = 1 then MAINSPEF else ' ' end
		,NEOCARE
		,TRETSPEF = case when isnumeric(TRETSPEF) = 1 then TRETSPEF else ' ' end
		,LEGLSTAT
		,CONSULT = COALESCE (case when left(CONSULT,1) = 'C' then CONSULT end, ' ')
		,DIAG_01 = COALESCE (DIAG_01,' ')
		,DIAG_02 = COALESCE (DIAG_02,' ')
		,DIAG_03 = COALESCE (DIAG_03,' ')
		,DIAG_04 = COALESCE (DIAG_04,' ')
		,DIAG_05 = COALESCE (DIAG_05,' ')
		,DIAG_06 = COALESCE (DIAG_06,' ')
		,DIAG_07 = COALESCE (DIAG_07,' ')
		,DIAG_08 = COALESCE (DIAG_08,' ')
		,DIAG_09 = COALESCE (DIAG_09,' ')
		,DIAG_10 = COALESCE (DIAG_10,' ')
		,DIAG_11 = COALESCE (DIAG_11,' ')
		,DIAG_12 = COALESCE (DIAG_12,' ')
		,DIAG_13 = COALESCE (DIAG_13,' ')
		,DIAG_14 = COALESCE (DIAG_14,' ')
		
		,OPER_01 = COALESCE (OPER_01,' ')
		,OPER_02 = COALESCE (OPER_02,' ')
		,OPER_03 = COALESCE (OPER_03,' ')
		,OPER_04 = COALESCE (OPER_04,' ')
		,OPER_05 = COALESCE (OPER_05,' ')
		,OPER_06 = COALESCE (OPER_06,' ')
		,OPER_07 = COALESCE (OPER_07,' ')	
		,OPER_08 = COALESCE (OPER_08,' ')	
		,OPER_09 = COALESCE (OPER_09,' ')
		,OPER_10 = COALESCE (OPER_10,' ')
		,OPER_11 = COALESCE (OPER_11,' ')
		,OPER_12 = case when OPER_12 is null then ' ' else OPER_12 end	
		
		,PATIENT_ID = PATIENTID
		,FCEHRG = case when FCEHRG is null then ' ' else FCEHRG end
		,SPELLHRG = case when SepllHRG is null then ' ' else SepllHRG end
		,DOB
		,CasenoteNumber	
	
	
	FROM
	
	(

select

 PROCODET = 
 
 		'RW300'
 
,PCT =
		left(
			Encounter.PurchaserCode
			,5
		) 

,PROVSPNO = 
	left(
			Replace(
				ProviderSpellNo
				,'/'
				,'*'
			)
			,12
		)

,EPIORDER = Encounter.SourceEncounterNo
,ADMIDATE = 
		left(
			CONVERT(varchar, CalendarAdmissionDate.TheDate, 112)
			,8
		)


,DISDATE = 
		left(
			CONVERT(varchar, CalendarDischargeDate.TheDate, 112)
			,8
		)
,EPISTART = 

		left(
			CONVERT(varchar, CalendarEpisodeStartDate.TheDate, 112)
			,8
		)

,EPIEND = 

		left(
			CONVERT(varchar, CalendarEpisodeEndDate.TheDate, 112)
			,8
		)

,STARTAGE = 

		datediff(year, Encounter.DateOfBirth, CalendarEpisodeEndDate.TheDate) -
		case
		when datepart(day, Encounter.DateOfBirth) = datepart(day, CalendarEpisodeEndDate.TheDate)
		and	datepart(month, Encounter.DateOfBirth) = datepart(month,CalendarEpisodeEndDate.TheDate)
		and datepart(year, Encounter.DateOfBirth) <> datepart(year,CalendarEpisodeEndDate.TheDate)
		then 0
		else 
			case
			when Encounter.DateOfBirth > CalendarEpisodeEndDate.TheDate then 0
			when datepart(dy, Encounter.DateOfBirth) > datepart(dy, CalendarEpisodeEndDate.TheDate) 
			then 1
			else 0 
			end 
		end


	,Sex = 
		Left(
			case
				when [NationalSexCode] = 'N||SEX' then '9'
				else [NationalSexCode] 
			end	
			,1
		)

,CLASSPAT = 

		left(
			PatientClassification.[NationalPatientClassificationCode]
			,1
		)

,ADMISORC = 

		left(
			AdmissionSource.[NationalAdmissionSourceCode]
			,2
		)


,ADMIMETH = 
		left(
			AdmissionMethod.NationalAdmissionMethodCode
			,2
		)


,DISDEST = 
		left(
			DischargeDestination.[NationalDischargeDestinationCode]
			,2
		)
,DISMETH = 
		left(
			DischargeMethod.NationalDischargeMethodCode
			, 1
		)
,EPIDUR = 


		DATEDIFF(day,CalendarEpisodeStartDate.TheDate,CalendarEpisodeEndDate.TheDate)

,MAINSPEF = --MAP TO CONSULTANT SPEC
	
		CASE
		WHEN ConsultantSpecialty.MainSpecialtyCode IS null
		THEN
				left(
			Specialty.NationalSpecialtyCode
			,3
					)
		else
		left(
			ConsultantSpecialty.MainSpecialtyCode
			,3
		)
		end	

,NEOCARE = 

		left(
			NeonatalLevelOfCare.NationalNeonatalLevelOfCareCode
			,1
		)

,TRETSPEF = 
		left(
			Specialty.NationalSpecialtyCode
			,3
		)
		
,LEGLSTAT = 

COALESCE (
						left(
							Encounter.LegalStatusClassificationCode
							,2
						)
				,	CASE 
						WHEN Specialty.NationalSpecialtyCode = '711' THEN '09'
						ELSE '08'
					END
		)
,CONSULT = 

		left(
			COALESCE(
				Consultant.NationalConsultantCode
				,'C9999998'
			)
			,8
		)
,DIAG_01 = 

					case
								when Encounter.PrimaryDiagnosisCode = '##'
								then ''
								else REPLACE(Encounter.PrimaryDiagnosisCode, '.', '')
							end

	,Diag_02 = REPLACE(Encounter.SecondaryDiagnosisCode1, '.', '')
	,Diag_03 = REPLACE(Encounter.SecondaryDiagnosisCode2, '.', '')
	,Diag_04 = REPLACE(Encounter.SecondaryDiagnosisCode3, '.', '')
	,Diag_05 = REPLACE(Encounter.SecondaryDiagnosisCode4, '.', '')
	,Diag_06 = REPLACE(Encounter.SecondaryDiagnosisCode5, '.', '')
	,Diag_07 = REPLACE(Encounter.SecondaryDiagnosisCode6, '.', '')
	,Diag_08 = REPLACE(Encounter.SecondaryDiagnosisCode7, '.', '')
	,Diag_09 = REPLACE(Encounter.SecondaryDiagnosisCode8, '.', '')
	,Diag_10 = REPLACE(Encounter.SecondaryDiagnosisCode9, '.', '')
	,Diag_11 = REPLACE(Encounter.SecondaryDiagnosisCode10, '.', '')
	,Diag_12 = REPLACE(Encounter.SecondaryDiagnosisCode11, '.', '')
	,Diag_13 = REPLACE(Encounter.SecondaryDiagnosisCode12, '.', '')
	,Diag_14 = null --REPLACE(Encounter.SecondaryDiagnosisCode13, '.', '')


	,OPER_01 = 	case
				when PrimaryProcedureCode = '##'
				then ''
				else left(replace(PrimaryProcedureCode, '.', ''), 4)
				END
	,OPER_02 = LEFT (replace(Encounter.SecondaryProcedureCode1, '.', ''), 4)
	,OPER_03 = left(replace(Encounter.SecondaryProcedureCode2, '.', ''), 4)
	,OPER_04 = left(replace(Encounter.SecondaryProcedureCode3, '.', ''), 4)
	,OPER_05 = left(replace(Encounter.SecondaryProcedureCode4, '.', ''), 4)
	,OPER_06 = left(replace(Encounter.SecondaryProcedureCode5, '.', ''), 4)
	,OPER_07 = left(replace(Encounter.SecondaryProcedureCode6, '.', ''), 4)
	,OPER_08 = left(replace(Encounter.SecondaryProcedureCode7, '.', ''), 4)
	,OPER_09 = left(replace(Encounter.SecondaryProcedureCode8, '.', ''), 4)
	,OPER_10 = LEFT (replace(Encounter.SecondaryProcedureCode9, '.', ''), 4)
	,OPER_11 = LEFT(replace(Encounter.SecondaryProcedureCode10, '.', ''), 4)
	,OPER_12 = left(replace(Encounter.SecondaryProcedureCode11, '.', ''), 4)

	,PatientID = DistrictNo
	,FCEHRG = null
	,SepllHRG = NULL
	,DOB = 

		left(
			CONVERT(varchar, DateOfBirth, 112)
			,8
		)
	,Encounter.CasenoteNumber



FROM
	APC.Encounter
	
	left outer join WH.Calendar CalendarDischargeDate
	on Encounter.DischargeDateID = CalendarDischargeDate.DateID
	
inner join WH.Calendar CalendarEpisodeStartDate
	on Encounter.EpisodeStartDateID = CalendarEpisodeStartDate.DateID
	
left outer join WH.Calendar CalendarAdmissionDate
	on Encounter.AdmissionDateID = CalendarAdmissionDate.DateID
	
left outer join WH.Calendar CalendarEpisodeEndDate
	on Encounter.EpisodeEndDateID = CalendarEpisodeEndDate.DateID

LEFT OUTER JOIN WH.NHSNumberStatus NHSNumberStatus
	ON	NHSNumberStatus.SourceNHSNumberStatusID = Encounter.NHSNumberStatusID

	
LEFT OUTER JOIN WH.Consultant
	ON	Consultant.SourceConsultantID = Encounter.ConsultantID

LEFT OUTER JOIN WH.Specialty Specialty
	ON	Specialty.SourceSpecialtyID = Encounter.SpecialtyID
	
	LEFT OUTER JOIN [WH].[Sex] Sex
	ON	Sex.[SourceSexID] = Encounter.SexID
	
LEFT OUTER JOIN [APC].AdmissionMethod AdmissionMethod
	ON	AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID
	
LEFT OUTER JOIN [APC].[DischargeMethod] DischargeMethod
	ON	DischargeMethod.[SourceDischargeMethodID] = Encounter.DischargeMethodID
	
LEFT OUTER JOIN [APC].[DischargeDestination] DischargeDestination
	ON	DischargeDestination.[SourceDischargeDestinationID] = Encounter.DischargeDestinationID

LEFT OUTER JOIN [APC].[PatientClassification] PatientClassification
	ON	PatientClassification.[SourcePatientClassificationID] = Encounter.PatientClassificationID
	
LEFT OUTER JOIN [APC].[NeonatalLevelOfCare] NeonatalLevelOfCare
	ON	NeonatalLevelOfCare.[SourceNeonatalLevelOfCareID] = Encounter.NeonatalLevelOfCareID

LEFT OUTER JOIN WH.Consultant ConsultantSpecialty
ON ConsultantSpecialty.SourceConsultantID = Encounter.ConsultantID

LEFT OUTER JOIN [APC].[AdmissionSource] AdmissionSource
	ON	AdmissionSource.[SourceAdmissionSourceID] = Encounter.AdmissionSourceID



WHERE CalendarDischargeDate.TheDate BETWEEN @STARTDATE AND @ENDDATE

AND

 [ClinicalCodingStatus] = 'C' --Marked as complete on system
 
 
	)CodingAudit
 
 ORDER BY PROVSPNO, EPIORDER



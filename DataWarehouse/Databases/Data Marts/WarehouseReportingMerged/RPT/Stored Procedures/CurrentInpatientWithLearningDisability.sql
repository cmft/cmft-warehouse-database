﻿CREATE Procedure [RPT].[CurrentInpatientWithLearningDisability] 
	(
	@DateRange varchar(max) = null
	,@NationalSpecialtyCode varchar(max) = null
	,@WardCode varchar(max) = null
	,@PatientCategoryCode varchar(max) = null
	,@GPPracticeCode varchar(max) = null
	--,@SourceSystemID int = null
	)
as

-- May need to do a Reference proc in WOMV2 which runs hourly and brings through the IDs for Specialty Consultant Age???  It will depend on how long the query below takes???


Select 
	ProviderSpellNo
	,AdmissionDate
	,AdmissionTime
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,Encounter.Postcode
	,Gender = SexCode
	,Ward = coalesce (
					EndWard.LocalWard
					,StartWard.LocalWard
					)
	,CasenoteNumber
	,DistrictNo
	,PatientCategoryCode
	,ExpectedLoS
	,Specialty = (Specialty.NationalSpecialtyCode + ' - ' + Specialty.NationalSpecialty)
	,Consultant = Consultant.SourceConsultant
	,EpisodicGPPracticeCode = coalesce (GPPractice.GpPracticeCode + ' - ' + GPPractice.GPPracticeName,'N/A')
	,APCDiagnosis = 
		case
			when Diagnosis.SourceSystemID IS not null then 1
			else 0
		end
	,Bedman = 
		case
			when Bedman.SourceSystemID IS not null then 1
			else 0
		end
	,SpecialRegister = 
		case
			when SpecialRegister.SourceSystemID IS not null then 1
			else 0
		end
from 
	APCUpdate.Encounter

left join WH.Consultant
on Encounter.ConsultantCode = Consultant.SourceConsultantCode
and Encounter.ContextCode = Consultant.SourceContextCode

left join WH.Specialty
on Encounter.SpecialtyCode = Specialty.SourceSpecialtyCode
and Encounter.ContextCode = Specialty.SourceContextCode

left join WH.Ward StartWard
on Encounter.StartWardTypeCode = StartWard.SourceWardCode
and Encounter.ContextCode = StartWard.SourceContextCode

left join WH.Ward EndWard
on Encounter.EndWardTypeCode = EndWard.SourceWardCode
and Encounter.ContextCode = EndWard.SourceContextCode

left join WH.GPPractice
on coalesce(Encounter.EpisodicGpPracticeCode,'N/A') = GPPractice.GpPracticeCode

left join Flag.CurrentInpatientWithLearningDisabilityBySystem Diagnosis
on Encounter.EncounterRecno = Diagnosis.EncounterRecno
and Diagnosis.SourceSystemID = 1

left join Flag.CurrentInpatientWithLearningDisabilityBySystem Bedman
on Encounter.EncounterRecno = Bedman.EncounterRecno
and Bedman.SourceSystemID = 2

left join Flag.CurrentInpatientWithLearningDisabilityBySystem SpecialRegister
on Encounter.EncounterRecno = SpecialRegister.EncounterRecno
and SpecialRegister.SourceSystemID = 3
--and 
--	(
--		CurrentInpatientWithLearningDisabilityBySystem.SourceSystemID  in (Select value from Rpt.SSRS_MultiValueParamSplit(@SourceSystemID,','))
--	or
--		@SourceSystemID is null
--	)
--and not exists
--	(
--	Select 
--		1
--	from
--		Flag.CurrentInpatientWithLearningDisabilityBySystem Earliest
--	where
--		Earliest.MergeEncounterRecno = CurrentInpatientWithLearningDisabilityBySystem.MergeEncounterRecno
--	and 
--		(
--			Earliest.SourceSystemID in (Select value from Rpt.SSRS_MultiValueParamSplit(@SourceSystemID,','))
--		or
--			@SourceSystemID is null
--		)
--	and Earliest.PatientDatasetFlagRecno < CurrentInpatientWithLearningDisabilityBySystem.PatientDatasetFlagRecno
--	)
		
where 
	EpisodeEndDate is null
and exists
		(
		select 
			1
		from 
			Match.Match.MatchTypeDataset 

		where 
			TargetDatasetRecno = Encounter.EncounterRecno
		and MatchTypeDataset.MatchTypeID = 8	
		)
	
and
	(
		PatientCategoryCode in (Select value from Rpt.SSRS_MultiValueParamSplit(@PatientCategoryCode,','))
	or
		@PatientCategoryCode is null
	)
and
	(
		NationalSpecialtyCode in (Select value from Rpt.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
	or
		@NationalSpecialtyCode is null
	)
and
	(
		coalesce(EndWard.LocalWardCode,StartWard.LocalWardCode) in (Select value from Rpt.SSRS_MultiValueParamSplit(@WardCode,','))
	or
		@WardCode is null
	)
and
	(
		coalesce(EpisodicGpPracticeCode,'N/A') in (Select value from Rpt.SSRS_MultiValueParamSplit(@GPPracticeCode,','))
	or
		@GPPracticeCode is null
	)
and
	(
		AdmissionDate >= 
			Case @DateRange
				when 'Today' then cast(getdate() as date)
				when 'Previous 3 days' then Dateadd(day,-3,cast(getdate() as date))
				when 'Previous 7 days' then Dateadd(day,-7,cast(getdate() as date))
				when 'Previous 28 days' then Dateadd(day,-28,cast(getdate() as date))
				when 'Previous 12 months' then Dateadd(month,-12,cast(getdate() as date))
			End
	or
		@DateRange is null
	)

group by
	ProviderSpellNo
	,AdmissionDate
	,AdmissionTime
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,Encounter.Postcode
	,SexCode
	,coalesce (
					EndWard.LocalWard
					,StartWard.LocalWard
					)
	,CasenoteNumber
	,DistrictNo
	,PatientCategoryCode
	,ExpectedLoS
	,(Specialty.NationalSpecialtyCode + ' - ' + Specialty.NationalSpecialty)
	,Consultant.SourceConsultant
	,coalesce (GPPractice.GpPracticeCode + ' - ' + GPPractice.GPPracticeName,'N/A')
	,	case
			when Diagnosis.SourceSystemID IS not null then 1
			else 0
		end
	,case
			when Bedman.SourceSystemID IS not null then 1
			else 0
		end
	,case
			when SpecialRegister.SourceSystemID IS not null then 1
			else 0
		end

﻿
CREATE PROCEDURE [RPT].[GetChildrenAtRisk]
(@StartAge AS INT
,@EndAge AS INT
,@StartDate AS DATE
,@EndDate AS DATE
,@DiffRows AS INT = 3)

AS 

--DECLARE @StartAge AS INT
--DECLARE @EndAge AS INT
--DECLARE @StartDate AS DATE
--DECLARE @EndDate AS DATE

--SET @StartAge = 12
--SET @EndAge = 19
--SET @StartDate = '01 Mar 2012'
--SET @EndDate = '30 Aug 2012'

CREATE TABLE #ChildrenAtRisk
(DistrictNo VARCHAR(50) NULL
,AttendanceNumber VARCHAR(50) NULL
,NHSNumber VARCHAR(17) NULL
,CasenoteNo VARCHAR(50) NULL
,AttendDate DATETIME NULL
,PatientAddress1 VARCHAR(100) NULL
,PatientAddress2 VARCHAR(100) NULL
,PatientAddress3 VARCHAR(100) NULL
,PatientAddress4 VARCHAR(100) NULL
,Postcode VARCHAR(100) NULL
,DateOfBirth DATE NULL
,Sex VARCHAR(50) NULL
,SourceSiteCode VARCHAR(100) NULL
,SourceClinicCode VARCHAR(100) NULL
,AEAttends INT NULL
,OPAttends INT NULL
,TotalAttends INT NULL
,DiffRows INT NULL
)


INSERT INTO #ChildrenAtRisk

SELECT  DistrictNo					= ISNULL(A.DistrictNo,'')
		,AttendanceNumber			= ISNULL(A.AttendanceNumber,'')
		,NHSNumber					= REPLACE(ISNULL(A.NHSNumber,''),' ','')
		,CasenoteNo					= A.CasenoteNo
		,AttendDate					= A.ArrivalDate
		,PatientAddress1			= A.PatientAddress1
		,PatientAddress2			= A.PatientAddress2
		,PatientAddress3			= A.PatientAddress3
		,PatientAddress4			= A.PatientAddress4
		,Postcode 					= REPLACE(A.Postcode,'  ',' ')
		,DateOfBirth				= A.DateOfBirth
		,Sex						= Sex.NationalSexCode
		,SourceSiteCode				= Site.SourceSiteCode
		,SourceClinicCode			= Site.SourceSiteCode
		,AEAttends					= 1
		,OPAttends					= Null
		,TotalAttends				= 1
		,DiffRows					= 0
FROM AE.Encounter A
INNER JOIN WH.Site	
	ON Site.SourceSiteID = A.SiteID
INNER JOIN WH.Sex
	ON Sex.SourceSexID = A.SexID
WHERE dbo.f_GetAge(A.DateOfBirth,A.ArrivalDate) BETWEEN @StartAge and @EndAge
AND A.ArrivalDate BETWEEN @StartDate AND @EndDate
AND Site.SourceSiteCode IN ('RW3MR','RW3SM','RW3RC')


INSERT INTO #ChildrenAtRisk

SELECT  DistrictNo					= ISNULL(A.DistrictNo,'')
		,AttendanceNumber			= Null
		,NHSNumber					= REPLACE(ISNULL(A.NHSNumber,''),' ','')
		,CasenoteNo					= A.CasenoteNo
		,AttendDate					= CAST(A.AppointmentTime AS DATE)
		,PatientAddress1			= A.PatientAddress1
		,PatientAddress2			= A.PatientAddress2
		,PatientAddress3			= A.PatientAddress3
		,PatientAddress4			= A.PatientAddress4
		,Postcode 					= REPLACE(A.Postcode,'  ',' ')				
		,DateOfBirth				= A.DateOfBirth
		,Sex						= Sex.NationalSexCode
		,SourceSiteCode				= CASE SUBSTRING(Clinic.SourceClinicCode,1,4) 
											WHEN 'WHIT' THEN 'OPWHIT' 
											ELSE Clinic.SourceClinicCode 
									  END
		,SourceClinicCode			=  CASE SUBSTRING(Clinic.SourceClinicCode,1,4) 
											WHEN 'WHIT' THEN 'OPWHIT' 
											ELSE Clinic.SourceClinicCode 
									  END
		,AEAttends					= Null
		,OPAttends					= 1
		,TotalAttends				= 1
		,DiffRows					= 0		
FROM OP.Encounter A
INNER JOIN OP.Clinic
	ON A.ClinicID = Clinic.SourceClinicID
INNER JOIN WH.Sex
	ON Sex.SourceSexID = A.SexID
WHERE dbo.f_GetAge(A.DateOfBirth,A.AppointmentTime) BETWEEN @StartAge and @EndAge
AND A.AppointmentTime BETWEEN @StartDate AND @EndDate
AND Clinic.SourceClinicCode IN ('EGUFUP','WHITCOM','WHITFRI','WHITHOS','WHITHU','WHITTU','WHITWED')


	 
UPDATE C1	
SET DiffRows = internalnorank
FROM #ChildrenAtRisk C1
INNER JOIN (SELECT DistrictNo,NHSNumber,PostCode,DateOfBirth,SourceSiteCode, 
DENSE_RANK() OVER (Partition by DistrictNo,NHSNumber,PostCode,DateOfBirth ORDER BY SourceSiteCode) AS internalnorank
FROM #ChildrenAtRisk) C_RANK 
ON C1.DistrictNo = C_RANK.DistrictNo
AND C1.NHSNumber = C_RANK.NHSNumber
AND C1.PostCode = C_RANK.PostCode
AND C1.DateOfBirth = C_RANK.DateOfBirth
AND C1.SourceSiteCode = C_RANK.SourceSiteCode

UPDATE C1	
SET DiffRows = (SELECT MAX(DiffRows) DiffRows FROM #ChildrenAtRisk C
WHERE C1.DistrictNo = C.DistrictNo
AND C1.NHSNumber = C.NHSNumber
AND C1.PostCode = C.PostCode
AND C1.DateOfBirth = C.DateOfBirth)
FROM #ChildrenAtRisk C1


	 
--SELECT DistrictNo, 
--NHSNumber,
--PostCode,
--DateOfBirth,
--SourceSiteCode,
--COUNT(*)
--FROM #ChildrenAtRisk	
--GROUP BY  DistrictNo, NHSNumber, PostCode, DateOfBirth, SourceSiteCode
--HAVING COUNT(*) > 2
	 
	 
SELECT * FROM #ChildrenAtRisk
WHERE DiffRows >= @DiffRows	
ORDER BY DistrictNo, NHSNumber, PostCode, AttendDate

	 
DROP TABLE #ChildrenAtRisk



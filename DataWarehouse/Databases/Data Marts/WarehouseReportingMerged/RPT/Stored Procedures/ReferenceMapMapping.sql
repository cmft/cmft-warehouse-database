﻿






CREATE proc [RPT].[ReferenceMapMapping]
(
	@AttributeCode varchar(20)
	,@SourceContextID int = null
	,@ChameleonValueCode varchar(50) = null
	,@ChameleonIsMapped bit = null
)

as

/* 
==============================================================================================
Description:

When		Who			What
20150514	Paul Egan	Initial Coding
===============================================================================================
*/

/* ===== Debug only ===== */
--declare
--	@AttributeCode varchar(20) = 'SPEC'
--	,@SourceContextID int = null
--	,@ChameleonValueCode varchar(50) = null
--;
/* ====================== */


select
	 AttributeCode
	,Attribute
	,SourceContextID
	,SourceContextCode
	,SourceContext
	,SourceValueID
	,SourceValueCode
	,SourceValue
	,LocalValueID
	,LocalValueCode
	,LocalValue
	,NationalValueID
	,NationalValueCode
	,NationalValue
	,OtherValueID
	,OtherValueCode
	,OtherValue
	,ChameleonValueCode
	,ChameleonValue
	,ChameleonIsMapped
from
	WH.MemberChameleonPreference		-- View
where
	AttributeCode = @AttributeCode
	and SourceContextID = isnull(@SourceContextID, SourceContextID)
	and isnull(ChameleonValueCode, 0) = isnull(@ChameleonValueCode, isnull(ChameleonValueCode, 0))
	and ChameleonIsMapped = isnull(@ChameleonIsMapped, ChameleonIsMapped)

;






﻿
CREATE procedure [RPT].[CreatinineDefaultDate]
as

select
	 DefaultFromDate = cast(dateadd(day , -7 , max(AdmissionTime)) as date)
	,DefaultToDate = cast(max(AdmissionTime) as date)
from
	Result.AcuteKidneyInjuryAlert Result
where
	Result.CurrentInpatient = 1

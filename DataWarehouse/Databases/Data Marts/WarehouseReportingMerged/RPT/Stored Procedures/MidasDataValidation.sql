﻿

CREATE procedure [RPT].[MidasDataValidation]

as



select
       SourceUniqueID
       ,StartDate
       ,EndDate
       ,CriticalCarePeriodDuration = datediff(day, StartDate, EndDate)
       ,CasenoteNumber
       ,NHSNumber

from
       APC.CriticalCarePeriod

where
       not exists
                           (
							select
									1
							from
									APC.Encounter
							where
								CriticalCarePeriod.CasenoteNumber = Encounter.CasenoteNumber
							or
								CriticalCarePeriod.NHSNumber = Encounter.NHSNumber
                           )



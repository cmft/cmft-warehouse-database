﻿


	CREATE procedure [RPT].[AcuteMedDailyUncodedMultiProf]

	as
				
select 						
	 Clinic = SourceClinicCode					
	,Specialty =SourceSpecialtyCode					
	,AppointmentTime					
	,NHSNumber					
	,DistrictNo					
	,DateOfBirth					
						
from						
	OP.encounter					
						
	join OP.Clinic					
	on ClinicID = SourceClinicID					
						
	join OP.AttendanceStatus					
	on AttendanceStatusID = SourceAttendanceStatusID					
						
	join wh.Specialty					
	on SpecialtyID = SourceSpecialtyID					
						
where						
		AppointmentTime > '01 apr 2012'				
	and SourceClinicCode in('BRONC-CL', 'MWB', 'PBONC') --CHB					
	and PrimaryOperationCode is null		
	and SourceAttendanceStatusCode in ('5', '6')					



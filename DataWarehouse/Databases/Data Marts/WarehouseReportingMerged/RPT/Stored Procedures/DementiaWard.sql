﻿
CREATE Procedure [RPT].[DementiaWard]

as

Select 
	distinct
		Ward = WardCode
from 
	APC.Encounter Encounter

inner join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   

inner join APC.WardStay
on Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
and not exists
	(
	select
		1
	from 
		APC.WardStay Latest
	where 
		Latest.ProviderSpellNo = WardStay.ProviderSpellNo
	and	Latest.StartTime > WardStay.StartTime
	)
	
where	
	AdmissionDate >= DATEADD(month,-6,getdate())
	--DischargeDate is null
and dbo.f_GetAge (DateOfBirth, AdmissionDate)>=18--75 
and	AdmissionMethod.AdmissionType = 'Emergency' -- Non Elective admissions only

union
	
select 
	[Ward] = 'All'
	
	
	
	

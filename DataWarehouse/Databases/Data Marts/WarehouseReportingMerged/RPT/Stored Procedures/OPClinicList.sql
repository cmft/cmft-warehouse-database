﻿


	--Clinic List report

create proc [RPT].[OPClinicList]

	 @ClinicDate datetime = null
	,@Division as varchar(max)

	as

--	declare @ClinicDate datetime = '19 mar 2013'

			select 
				 Division
				,ConsultanatTitle = Consultanat.Title
				,ConsultanatInitials = Consultanat.Initials
				,ConsultanatSurname = Consultanat.Surname
				,Clinic.SourceClinicCode
				,Clinic.SourceClinic

				,DistrictNo 
				,Encounter.NHSNumber
				,Encounter.CasenoteNo
				,PatientSurname
				,PatientForename
				,PatientSex = Sex.NationalSex
				,Encounter.DateOfBirth
				,Encounter.AgeCode
				,RTTPathwayID
				,Encounter.AppointmentTime
				,AppointmentType.SourceAppointmentType
				,Encounter.RTTActivity
				,Encounter.RTTStartDate
				,Encounter.RTTEndDate
				,Encounter.RTTBreachDate


				,CountOfDNAs = coalesce(CountOfDNAs,0)
				,CountOfHospitalCancels = coalesce(CountOfHospitalCancels,0)
				,CountOfPatientCancels = coalesce(CountOfPatientCancels,0)

				,Encounter.AppointmentStatusCode 
				,Encounter.appointmentdateid

				,AMPM = 
				
						Case 
							When CONVERT(TIME,Encounter.AppointmentTime) >= '13:00' then 'PM'
							When CONVERT(TIME,Encounter.AppointmentTime) < '13:00' then 'AM'
						else
						null
						end
	
			

			 from OP.Encounter Encounter

			 left join WH.Sex Sex
			 on Sex.SourceSexID = Encounter.SexID

			 inner join wh.Calendar AppointmentDate
			 on AppointmentDate.DateID = Encounter.AppointmentDateID

			 left join OP.Clinic Clinic
			 on Clinic.SourceClinicID = encounter.ClinicID
			 --and Clinic.SourceContext = Encounter.ContextCode

			 left join WH.Directorate Directorate
			 on Directorate.DirectorateCode = Encounter.DirectorateCode

			 INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@Division,',')) D
			 ON Directorate.Division = D.Value  

			 left join OP.AppointmentType AppointmentType
			 on AppointmentType.SourceAppointmentTypeID = Encounter.AppointmentTypeID

			 left join wh.Consultant Consultanat
			 on Consultanat.SourceConsultantID = Encounter.ConsultantID
		

				left join 
				(
				select	
		
			 
					 SourcePatientNo
					,SourceEncounterNo
					,CountOfDNAs
					,CountOfHospitalCancels
					,CountOfPatientCancels
					,AppointmentDate

				from Warehouse.OP.WaitingList W

				where w.CensusDate = @ClinicDate-1  

				and w.CancelledBy is null

				) CancelDetail
				on CancelDetail.SourcePatientNo = Encounter.SourcePatientNo
				and CancelDetail.SourceEncounterNo = Encounter.SourceEncounterNo
				and CancelDetail.AppointmentDate = AppointmentDate.thedate

			where 
	
 
					AppointmentDate.thedate = @ClinicDate
				and AppointmentCancelDate is null

				--and Division <> 'Trafford'

				--and Consultanat.Surname like 'bow%'

				--and Encounter.AppointmentStatusCode = 'NR'

				--and SourceClinicCode = 'SAAF'

				--and CancelDetail.SourcePatientNo is null

				--and Encounter.AppointmentTime >= '07 mar 2013 13:00' 

				order by

			 Division
			,Consultanat.Surname
			,SourceClinic	
			,Encounter.AppointmentTime asc



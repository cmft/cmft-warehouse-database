﻿    CREATE proc [RPT].[SpecialRegisterExtract] 
	
	 @RegisterStartDate datetime 
	,@RegisterEndDate datetime
	
	as
	

	--Debug
	 --DECLARE @StartDate datetime 
	 --DECLARE @EndDate datetime 

	 --set @StartDate = '25 feb 2015'
	 --set @EndDate = '25 feb 2015'
      Select                                                    
        
             EnteredDate
                                                               
            ,Ward.Providerspellno
                  
            ,CasenoteNumber
            ,PatientForename
            ,PatientSurname
            ,SexCode
            ,AdmissionTime
            ,DischargeTime
            ,LatestWardStartTime = Max (StartTime) 
         
            ,LatestWardEndCode = Max (EndWardTypeCode)
            ,LatestLocalSpecialty = Max (NationalSpecialtyCode)
            ,LatestNationalSpecialtyLabel = Max(NationalSpecialty)
            ,LatestDivision = Max(Directorate.Division)
            ,WardDivision = Max(WardDirectorate.Division)
            ,SpecialRegister = 
				case SpecialRegisterCode
				when 'CPC' then 'CPE Positive (Total)'
				when 'CPCN' then 'CPE NDM'
				when 'CPCO' then 'CPE OXA'
				when 'CPCV' then 'CPC VIM'
				when 'CPCI' then 'CPC IMP'
				else SpecialRegisterCode
				end
			,PatientCount = 
				1                                             
                                                
      FROM 
	  
			APCUpdate.Encounter Encounter 
			
			
	  
			inner join WarehouseOLAPMergedV2.[APC].[BaseWardStay] Ward
			on 	Encounter.ProviderSpellNo = Ward.ProviderSpellNo  
			
			inner join  WarehouseOLAPMergedV2.[APC].[BaseWardStayReference] BaseWardStayReference
			 ON BaseWardStayReference.MergeEncounterRecno = Ward.MergeEncounterRecno
			
			--Inner join APC.WardStay Ward
			--on Encounter.ProviderSpellNo = Ward.ProviderSpellNo

            
			left join APCUpdate.PatientSpecialRegister Special
			on special.SourcePatientNo = Ward.SourcePatientno
              


			left join WH.Specialty Specialty 
			on Specialty.SourceSpecialtyCode = Encounter.SpecialtyCode                                           
                     
			left join WH.Directorate Directorate 
			on Directorate.DirectorateCode = Encounter.StartDirectorateCode
			
			left join WH.Directorate WardDirectorate 
			on WardDirectorate.DirectorateCode = Ward.DirectorateCode
                        
            ,                                                     
                                                      
            (
			
				SELECT                                                     
					 [WeekNo]                                                    
					 ,[TheDate]    
					 
					                                               
              FROM [warehousesql].[WarehouseReportingMerged].[WH].[Calendar] Cal                                                    
                                                                  
              where 
			  
			  cal.TheDate between @RegisterStartDate and @RegisterEndDate ) week                                                      
                                                                  
                                                     
                                                                  
                  where                                           
			 (

			 [StartDate] <= thedate and([Enddate] >= thedate 
			 or 
			 [Enddate] is null)
			 
			 )                                                     
                                                           
     
					    and Episodeenddate is null
                        and SpecialRegisterCode in ('CPC','CPCO','CPCV','CPCN','CPCI')
          
              
            group by
          
                  EnteredDate                                        
            ,Ward.Providerspellno
            ,CasenoteNumber
            ,PatientForename
            ,PatientSurname
            ,SexCode
            ,AdmissionTime
            ,DischargeTime
            ,SpecialRegisterCode                                        



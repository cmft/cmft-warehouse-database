﻿

CREATE procedure [RPT].[DailyMedicalWardOutliers]

as



declare @sitrepday datetime  = CAST(CONVERT(VARCHAR,  getdate(), 102) AS DATETIME)

declare @wardout table 

(
       ward varchar(5) primary key
       ,spec varchar(15)
       ,location varchar(15)
    
)

insert into @wardout

values
('ESTU','Gastro','Surgery')
,('8M','Elderly Care','Surgery')
,('9M','Gastro','Surgery')
,('3M','Respiratory','Heart')
,('11M','Gastro','Surgery')
,('12M','Gastro','Surgery')
,('62','Elderly Care','St Marys')
,('63','Elderly Care','St Marys')
,('64','Elderly Care','St Marys')
,('64A','Elderly Care','St Marys')
,('64B','Elderly Care','St Marys')
,('65','Elderly Care','St Marys')
,('66','Elderly Care','St Marys')
,('67','Elderly Care','St Marys')
,('67A','Elderly Care','St Marys')
,('68','Elderly Care','St Marys')
,('90','Elderly Care','St Marys')
,('55I','Elderly Care','St Marys')
,('54','Elderly Care','St Marys')
,('DC','Elderly Care','St Marys')
,('35','Medical','Royal Eye')
--,('35C','Medical','Royal Eye')
,('6M','Medical','Heart')
,('36','Medical','Renal')
,('37','Respiratory','Renal')
,('37A','Respiratory','Renal')
,('44','Respiratory','Specialist')
,('CLDU','Acute','Acute')
,('36A','Medical','Renal')
,('4M','Respiratory','Acute')
,('10M','Respiratory','Surgery')
,('SAL','g','Surgery')
,('ETCD','g','Surgery')
,('ETCS','g','Surgery')
--,('HDU','g','CSS')
--,('ITU','g','CSS')
,('47B','Medical','Royal Eye')
,('BCS','Medical','Royal Eye')
,('47','Medical','Royal Eye')
,('47A','Medical','Royal Eye')
--,('15M','Surgery','Surgery')





declare @consout table 

(
       cons varchar(5) primary key
       ,spec varchar(15)
     
)

insert into @consout

values
('AIM','Elderly Care')
,('AJM','Gastro')
,('AMH','Medical')
,('CH','Respiratory')
,('CHL','Elderly Care')
,('CMC','Elderly Care')
,('HSN','Medical')
,('JARG','Gastro')
,('JCBR','Acute')
,('JCGS','Respiratory')
,('JWEM','Acute')
,('MD','Acute')
,('MDAT','Elderly Care')
,('MHY','Elderly Care')
,('MIP','Gastro')
,('MW','Respiratory')
,('NAK','Gastro')
,('PB','Elderly Care')
,('PLS','Medical')
,('RBP','Medical')
,('RFE','Respiratory')
,('RMA','Medical')
,('RPW','Gastro')
,('SIB','Respiratory')
,('SSC','Gastro')
,('TAS','Acute')
,('TD','Acute')
,('VNA','Acute')
,('GSU','Acute')
,('PCH','Acute')
,('KSAN','Acute')
,('NAMN','Acute')
,('KCD','Acute')
,('IAM','Acute')

Select
 wardstay.SourcePatientNo
,wardstay.SourceSpellNo
,PatientForename
,PatientSurname
,AdmissionTime
,StartTime
,ward.ward
,ward.location
,cons
,CasenoteNumber
,Directorate.Directorate
,Consultant = 
	Consultant.Consultant
,WardDesc = 
	isnull(Wardbase.Ward,ward.ward)
from warehouse.APC.Encounter apc

join @consout cons on
cons.cons = apc.consultantcode
and EpisodeStartDate < @sitrepday and (EpisodeEndDate >= @sitrepday or EpisodeEndDate is null)

join warehouse.apc.wardstay  wardstay on
wardstay.[ProviderSpellNo] = apc.[ProviderSpellNo]
and startdate < @sitrepday and (endDate >= @sitrepday or endDate is null)
and EpisodeStartDate < @sitrepday and (EpisodeEndDate >= @sitrepday or EpisodeEndDate is null)

inner join warehouse.WH.Directorate
on Directorate.DirectorateCode = apc.StartDirectorateCode

join  @wardout ward on
ward.ward = wardstay.wardcode
and location in ('Royal Eye','Surgery','St Marys','CSS')

left outer join warehouse.pas.Ward Wardbase
on wardbase.WardCode = wardstay.wardCode


left outer join  warehouse.pas.Consultant
on Consultant.ConsultantCode = apc.ConsultantCode

group by
 wardstay.SourcePatientNo
,wardstay.SourceSpellNo
,PatientForename
,PatientSurname
,AdmissionTime
,StartTime
,ward.ward
,ward.location
,cons
,CasenoteNumber
,Directorate.Directorate
,Consultant.Consultant
,Wardbase.Ward









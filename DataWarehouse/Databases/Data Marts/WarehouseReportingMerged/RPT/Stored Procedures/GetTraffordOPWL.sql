﻿

	--Gareth Cunnah 07 Jun 2012
	--Extract OP data for 14 days from the date entered
	--This will need to come from the merged warehouse
	
	--07 Jun 2012 still needs to be validated not getting same numbers as from Trafford Side
	--Also need to look at naming data items
	
		--25 June 2012 Added insert into table TGHInformation temp fix for Ducia



		--23 Jul 2012 amended Spec to use TreatmentFuntionCode as requested by TGH Information

		CREATE proc [RPT].[GetTraffordOPWL]

		@startdate datetime = null

		as

	--declare @SetStartDate datetime = coalesce(@StartDate,(select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0)))
	--declare @SetEnddate datetime = coalesce(@StartDate,(select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0))) + 13

--changed GC 05/10/2012
	declare @SetStartDate datetime = coalesce(@StartDate,(select DATEADD(day, DATEDIFF(day,0,GETDATE()), 0)))
	declare @SetEnddate datetime = coalesce(@StartDate,(select DATEADD(day, DATEDIFF(day,0,GETDATE()), 0))) + 13
	

	drop table TGHInformation.dbo.TraffordOPWL
	
				select
						 Districtno
						,Hospital 
						,InternalNo 
						,EpisodeNo 
						,ApptDate 
						,Doctor 
						,Clinic 
						,ClinicSpecialty 
						,CancelBy 
						,ApptType 
						,refsource 
						,REF_CONS 
						,REF_SPEC 
						,spec_desc = spec_desc + ' ' + Spec.NationalSpecialty
						,Consul 
						,Derived_Appoint_Type 
						,Division = null
						--case when DivisionCode = 'M' then 'Medicine'
						--				 when DivisionCode = 'S' then 'Surgery' else
						--			DivisionCode end
						,primproc 
						,N_F 
						,diag 
						,WeekNo
						,StartDate
						,Enddate

				into  TGHInformation.dbo.TraffordOPWL

				from

					(


						select 

						 Districtno
						,Hospital = null
						,InternalNo = sourceuniqueid
						,EpisodeNo = null
						,ApptDate = AppointmentDate
						,Doctor = null
						,Clinic = null
						,ClinicSpecialty = null
						,CancelBy = null
						,ApptType = null
						,refsource = null
						,REF_CONS = null
						,REF_SPEC =   CASE 
									when ((C.Surname = 'SANDLE') and (A.apptype_name like '%Glucose%')) then '822'
							
									else O.TreatmentFunctionCode end 
									
						,spec_desc = CASE when ((C.Surname = 'SANDLE') and (A.apptype_name like '%Glucose%')) then '822'
									else O.TreatmentFunctionCode end 
						,Consul = null
						,Derived_Appoint_Type = null
						,Division = null --DivisionCode
						,primproc = null
						,N_F = 
							case when FirstAttendanceCode = 1 then 'N' when FirstAttendanceCode = 2 then 'F' else FirstAttendanceCode end
						,diag = null
						,WeekNo =
								case 
									when AppointmentDate between @SetStartDate and @SetStartDate + 6 then 1
									else 2 end
						,StartDate = @SetStartDate
						,EndDate = @SetEndDate
					
						from 
						TraffordWarehouse.dbo.OPWaitingList O 

						left join TraffordReferenceData.dbo.INFODEPT_cab_consultants c
						on c.sds_code = O.ConsultantCode
				 
						left join TraffordReferenceData.dbo.AppointmentType A 
						on O.AppointmentTypeCode = A.apptype_identity 


						left join WarehouseOLAPMergedV2.WH.Specialty Specialty
						on Specialty.SourceSpecialtyCode = o.SpecialtyCode 
						and Specialty.SourceContextCode = 'TRA||UG'	
						


							where 
							CensusDate = @SetStartDate
							
							and 
							O.FirstAttendanceCode in ('1','2')  --,'7') Removed 28 May 2012
							and O.ReferralRequestReceivedDate is not null
	
							and O.TreatmentFunctionCode not in ('143','141')

							and AppointmentDate between @SetStartDate and @SetEnddate 

					
							
							
					) Activity	
							
						left join WH.Specialty Spec
						on Spec.SourceSpecialtyCode = Activity.REF_SPEC
						and Spec.SourceContextCode = 'TRA||UG'
							





﻿
CREATE proc [RPT].[PotentialHRGImprovement]

(
	@start date 
	,@end date 
)

as
	  

/* Get spells without exclusions and seek missing diagnosis codes */


if object_id('tempdb..#WrkPotentialHRGImprovement') is not null
drop table #WrkPotentialHRGImprovement;

--declare 
--	@start date = '1 jul 2013'
--	,@end date = '31 jul 2013'

select
	Encounter.MergeEncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.CasenoteNumber
	,Encounter.AdmissionTime
	,PatientAge = floor(datediff(day, Encounter.DateOfBirth, Encounter.AdmissionDate)/365.25)
	,Spell.HRGCode
	,EndDirectorateCode
	,SpecialtyID
	,ConsultantID
	,PreviousDiagnosisCode = PreviousDiagnosis.DiagnosisCode -- diagnosis code identified as long term and coded in previous spell but not current
	,PreviousAdmissionTime = PreviousDiagnosis.AdmissionTime -- admission time associated with diagnosis code identified
	,OpportunityFlag = cast('' as varchar (20))
into
	#WrkPotentialHRGImprovement
from
	APC.Encounter

inner join
		(
		select
			HRG4Spell.HRGCode
			,Encounter.MergeEncounterRecno

		from APC.Encounter

		inner join APC.HRG4Spell
		on	Encounter.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno

		--inner join WarehouseOLAPMergedV2.Allocation.DatasetAllocation
		--on	DatasetAllocation.DatasetCode = 'APC'
		--and DatasetAllocation.DatasetRecno = Encounter.MergeEncounterRecno
		--and AllocationTypeID = 4 -- exclusion

		--inner join WarehouseOLAPMergedV2.Allocation.Allocation
		--on	Allocation.AllocationID = DatasetAllocation.AllocationID
		--and SourceAllocationID = '-1'

		where
			DischargeDate between @start and @end

		) Spell

on	Spell.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join
			(
			select
				Diagnosis.DiagnosisCode
				,Encounter.SourcePatientNo
				,Encounter.ContextCode
				,AdmissionTime = max(Encounter.AdmissionTime)

			from APC.Diagnosis

			inner join APC.Encounter
			on	Encounter.MergeEncounterRecno = Diagnosis.MergeEncounterRecno

			inner join WarehouseReporting.WH.Diagnosis LongTermCondition
			on	LongTermCondition.DiagnosisCode = Diagnosis.DiagnosisCode
			and	LongTermCondition.IsLongTermFlag = 1

			where
				SequenceNo > 0

			group by
				Diagnosis.DiagnosisCode
				,Encounter.SourcePatientNo
				,Encounter.ContextCode
						
			) PreviousDiagnosis

			on	PreviousDiagnosis.SourcePatientNo = Encounter.SourcePatientNo
			and PreviousDiagnosis.ContextCode = Encounter.ContextCode
			and PreviousDiagnosis.AdmissionTime < Encounter.AdmissionTime
			

where
	not exists (
					select
						1
					from APC.Diagnosis CurrentDiagnosis

					inner join APC.Encounter CurrentSpell
					on	CurrentDiagnosis.MergeEncounterRecno = CurrentSpell.MergeEncounterRecno

					where 
						CurrentSpell.ProviderSpellNo = Encounter.ProviderSpellNo
					and	CurrentSpell.ContextCode = Encounter.ContextCode
					and	PreviousDiagnosis.DiagnosisCode = CurrentDiagnosis.DiagnosisCode
				)

/* Assign Flag based on code to group */

update
	PotentialHRGImprovement
set
	OpportunityFlag = 

						(
						select top 1
							FlagValue
						from 
							WarehouseReporting.Income.HRGGroupToSplit GroupToSplit
						where
							GroupToSplit.HRGRoot = left(PotentialHRGImprovement.HRGCode, 4)
						and
							exists
								(
									select
										1
									from
										WarehouseReporting.Income.HRGGroupToSplit GroupToSplitCC
									where
										HRGValue = PotentialHRGImprovement.HRGCode
									and	GroupToSplit.SequenceNo > GroupToSplitCC.SequenceNo
									and	GroupToSplit.FlagValue like '%cc%'												
									and	GroupToSplit.FlagValue not like 
																		case
																			when PotentialHRGImprovement.PatientAge > 19
																			then '%p_cc%'
																			else ''
																		end
								)
								order by SequenceNo
						)

from
	#WrkPotentialHRGImprovement PotentialHRGImprovement

--select
--	*
--from
--	#WrkIncomeOpportunity
--where
--	OpportunityFlag is not null

select
	SourceUniqueID
	,CasenoteNumber
	,AdmissionTime
	,PatientAge
	,Division
	,NationalSpecialtyLabel
	,Consultant
	,HRGCode
	,OpportunityFlag
	,DiagnosisCode1 = coalesce([1], '')
	,DiagnosisCode2 = coalesce([2], '')
	,DiagnosisCode3 = coalesce([3], '')
	,DiagnosisCode4 = coalesce([4], '')
	,DiagnosisCode5 = coalesce([5],'')
	,DiagnosisCode6 = coalesce([6],'')
	,DiagnosisCode7 = coalesce([7],'')
	,DiagnosisCode8 = coalesce([8],'')
	,DiagnosisCode9 = coalesce([9],'')
	,DiagnosisCode10 = coalesce([10],'')
from

(
select 
	SourceUniqueID
	,CasenoteNumber
	,AdmissionTime
	,PatientAge
	,Division
	,Specialty.NationalSpecialtyLabel
	,Consultant = Consultant.SourceConsultant
	,HRGCode
	,OpportunityFlag
	,Diagnosis = 
			case
				when Diagnosis.IsAlwaysPresentFlag = 1
				then Diagnosis.Diagnosis + ' (' + cast(cast(PreviousAdmissionTime as date) as varchar) + ')  *Always Present*'
				else Diagnosis.Diagnosis + ' (' + cast(cast(PreviousAdmissionTime as date) as varchar) + ')' 
			end
	,SequenceNo = row_number() over (partition by SourceUniqueID order by PreviousDiagnosisCode)

from #WrkPotentialHRGImprovement PotentialHRGImprovement 

inner join WH.Directorate
on	Directorate.DirectorateCode = PotentialHRGImprovement.EndDirectorateCode

inner join WarehouseReporting.WH.Diagnosis Diagnosis
on	Diagnosis.DiagnosisCode = PotentialHRGImprovement.PreviousDiagnosisCode

inner join WH.Specialty Specialty
on	Specialty.SourceSpecialtyID = PotentialHRGImprovement.SpecialtyID

inner join WH.Consultant Consultant
on	Consultant.SourceConsultantID = PotentialHRGImprovement.ConsultantID

inner join WarehouseReporting.Income.HRGComplication HRGComplication
on	ListID = 
				case
					when PatientAge < 19
					then replace(OpportunityFlag, '_p_','_')
					else cast(OpportunityFlag as varchar)
				end 
and	PreviousDiagnosisCode = HRGComplication.DiagnosisCode

) HRGUplift

pivot

(

	min(Diagnosis) for SequenceNo in
									(
										[1]
										,[2]
										,[3]
										,[4]
										,[5]
										,[6]
										,[7]
										,[8]
										,[9]
										,[10]
									)
) DiagnosisPivot


						  
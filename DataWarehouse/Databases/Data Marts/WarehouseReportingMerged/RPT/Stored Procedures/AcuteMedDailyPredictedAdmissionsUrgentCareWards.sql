﻿
			
create procedure [RPT].[AcuteMedDailyPredictedAdmissionsUrgentCareWards]

as		
			
select
	 AdmissionDay 		
	,Gender = 
		case Sex
		when 'F' then 'Female'
		else 'Male'
		end
	,Division
	,AverageDaysAdmissions = 
		Avg(cast([Number of Admissions] as float)) 
from
(
Select			
	 AdmissionDay = DayOfWeek		
	,Sex = SourceSexCode		
	,[Admission Date] = TheDate		
	,Division = 		
			
		CASE Division	
		WHEN 'Surgical' THEN 'Surgical'	
		ELSE 'Medical'	
		END	
	,[Number of Admissions] = count(*)		

			
from			
	APC.Encounter		
			
	join APC.Ward		
	on StartWardID = SourceWardID		
			
	join WH.Sex		
	on SexID = SourceSexID		
			
	join WH.Calendar		
	on AdmissionDateID = DateID		
			
	join WH.Directorate		
	on StartDirectorateCode = DirectorateCode	
			
where			
		FirstEpisodeInSpellIndicator = 1	
	and SourceWardCode in ('MAU', '15M' ,'ESTU','OMU','AMU')	
	and TheDate >= dateadd(dd, datediff(dd, 0, dateadd(d,-datepart(dw,getdate()) -(6*7)+1 ,getdate())), 0)		
	and TheDate < dateadd(dd, datediff(dd, 0, dateadd(d,-datepart(dw,getdate())+1 ,getdate())), 0)		
	and Division <> 'Trafford'		
			
group by			
	 DayOfWeek		
	,SourceSexCode		
	,	CASE Division
		WHEN 'Surgical' THEN 'Surgical'	
		ELSE 'Medical'	
		END	
	,TheDate		
			
) admissions	
group by
	AdmissionDay 		
	,case Sex
		when 'F' then 'Female'
		else 'Male'
		end
	,Division
order by
	case AdmissionDay
	when 'Monday' then 1
	when 'Tuesday' then 2
	when 'Wednesday' then 3
	when 'Thursday' then 4
	when 'friday' then 5
	when 'Saturday' then 6
	else 7
	end

﻿
create procedure [RPT].[ChildHealthPreSchoolBandedReport] as
Select 
       Demo.NHSNumber
      ,Demo.Surname
      ,Demo.Forename
      ,Demo.SexCode
      ,Demo.DateOfBirth
      ,HV.HealthVisitor
      ,Yellow.FlagCode Yellow
      ,Green.FlagCode Green
      ,Blue.FlagCode Blue
      
From
	Warehouse.ChildHealth.Entity Demo

left join Warehouse.ChildHealth.Birth BirthDets
on BirthDets.EntitySourceUniqueID = Demo.SourceUniqueID

left join Warehouse.ChildHealth.HealthVisitor HV
on HV.HealthVisitorCode = BirthDets.HealthVisitorCode

left join Warehouse.ChildHealth.RegistrationStatus RegStat
on RegStat.RegistrationStatusCode = BirthDets.RegistrationStatusCode

left join
      (
      Select
            SourceUniqueID
            ,AgeInDays = datediff(Day, DOB.DateOfBirth, getdate()-1)
      From 
            Warehouse.ChildHealth.Entity DOB
      ) Age
on Age.SourceUniqueID = Demo.SourceUniqueID

left join
      (
      select
            Banding.EntitySourceUniqueID
            ,Banding.Flagcode
            ,Banding.AddedDate 
            ,Banding.RemovedDate
      from
            warehouse.ChildHealth.ChildFlag Banding
      where 
                  Banding.FlagCode = 'Y'
            and Banding.RemovedDate is null
      ) Yellow
      
on yellow.EntitySourceUniqueID = Demo.SourceUniqueID

left join
      (
      select
            Banding.EntitySourceUniqueID
            ,Banding.Flagcode
            ,Banding.AddedDate 
            ,Banding.RemovedDate
      from
            warehouse.ChildHealth.ChildFlag Banding
      where 
                  Banding.FlagCode = 'G'
            and Banding.RemovedDate is null
      ) Green
      
on Green.EntitySourceUniqueID = Demo.SourceUniqueID

left join
      (
      select
            Banding.EntitySourceUniqueID
            ,Banding.Flagcode
            ,Banding.AddedDate 
            ,Banding.RemovedDate
      from
            warehouse.ChildHealth.ChildFlag Banding
      where 
                  Banding.FlagCode = 'B'
            and Banding.RemovedDate is null
      ) Blue
      
on Blue.EntitySourceUniqueID = Demo.SourceUniqueID

where 
            Age.AgeInDays between 1 and 1825
and   RegStat.RegistrationStatusCode in ('0','1','2','3')


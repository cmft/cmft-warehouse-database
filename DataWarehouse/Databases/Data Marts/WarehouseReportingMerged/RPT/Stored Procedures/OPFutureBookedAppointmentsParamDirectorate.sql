﻿
CREATE PROCEDURE [RPT].[OPFutureBookedAppointmentsParamDirectorate]
(
	 @StartBaseDate			DATE		= NULL
	,@Division				VARCHAR(50)	= NULL
	,@NoWeekEnds			INT			= 4		-- Determines the end date of the query and represents the number of 
) AS
	DECLARE @StartDate		DATE
	DECLARE @EndDate		DATE
	
	SET @StartBaseDate = COALESCE(@StartBaseDate, GETDATE())
	
	SET DATEFIRST  1	-- Monday

	SET @StartDate  = DATEADD(DAY, 1 - DATEPART(DW, @StartBaseDate), @StartBaseDate)
	SET @EndDate	= DATEADD(DAY, -1, DATEADD(WEEK, @NoWeekEnds, @StartDate))		-- End Date is a Sunday

	-- Had to use a temp table to get around the query taking a long when the temp table
	-- was replaced by incorporating the query as a sub-query :-(
	SELECT DISTINCT 
		E.DirectorateCode
	INTO #DATA
	FROM
		OP.Encounter E
		INNER JOIN WH.Calendar APPTCAL
			ON		E.AppointmentDateID			 = APPTCAL.DateID
	WHERE
			APPTCAL.TheDate BETWEEN @StartDate AND @EndDate
		AND 
	
			
		(
			 E.AppointmentCreateDate IS NOT NULL
			or
			 ContextCode = 'TRA||UG'
		) 


	SELECT 
		  DIR.DirectorateCode
		 ,DIR.Directorate
	FROM
		#DATA	
	INNER JOIN WH.Directorate DIR
		ON		#DATA.DirectorateCode			= DIR.DirectorateCode
			AND	DIR.Division					= COALESCE(@Division, DIR.Division)
	
	UNION
	
	SELECT
		NULL
		,'[All]'
		
	ORDER BY 		
		Directorate
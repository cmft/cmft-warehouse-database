﻿

CREATE procedure [RPT].[UpdateEnvoyFFTBirthPatient]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
select @StartTime = getdate()

select @RowsUpdated = 0


update	RPT.EnvoyFFTBirthPatient
set		Submitted = getdate()
where	Submitted is null

select @RowsUpdated = @@rowcount

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Updated ' + CONVERT(varchar(10), @RowsUpdated) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Warehouse.dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime






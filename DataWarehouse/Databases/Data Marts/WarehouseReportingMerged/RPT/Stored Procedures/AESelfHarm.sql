﻿
CREATE PROCEDURE [RPT].[AESelfHarm]
(
	 @AttendanceFromDate	DATE
	,@AttendanceToDate		DATE
	,@CommissionerCodes		VARCHAR(MAX)	= NULL
	,@AgeBandType			VARCHAR(10)		= NULL
)
AS
	DECLARE @AgeFrom	INT = 0
	DECLARE @AgeTo		INT = 999
	
	IF (@AgeBandType = 'Child')
		SET @AgeTo = 17
	ELSE IF (@AgeBandType = 'Adult')
		SET @AgeFrom = 18
	
	SELECT
		 CommissionerCode				= COALESCE(Encounter.AttendanceResidenceCCGCode,'x')
		,CommissionerName				= COALESCE(WH.CCG.CCG,'Unknown')
		,NationalSite					= WH.Site.NationalSite
		,NationalTriageCategory			= AE.TriageCategory.NationalTriageCategory
		,ArrivalTime					= Encounter.ArrivalTime
		,AttendanceNumber				= Encounter.AttendanceNumber
		,AgeOnArrival					= Encounter.AgeOnArrival
		,PresentingProblemCode			= CENPP.PresentingProblemCode
		,PresentingProblem				= CENPP.PresentingProblem
		,NationalAttendanceDisposal		= AE.AttendanceDisposal.NationalAttendanceDisposal
		,NationalSex					= WH.Sex.NationalSex
	
	FROM	
		WarehouseOLAPMergedV2.AE.BaseEncounter Encounter 
		
		INNER JOIN Warehouse.AE.PresentingProblem CENPP
			ON		CENPP.PresentingProblemCode							= Encounter.PresentingProblemCode
				AND Encounter.PresentingProblemCode						IN ('6483','6487','6493','6512','6515')
	 
		LEFT JOIN fn_ListToTable(COALESCE(@CommissionerCodes,'||'),',') COMM
			ON		COALESCE(Encounter.AttendanceResidenceCCGCode,'x') = COMM.Item 
--			ON		Encounter.CommissionerCode = COMM.Item 
			
		LEFT JOIN AE.AttendanceDisposal
			ON		AE.AttendanceDisposal.SourceAttendanceDisposalCode	= Encounter.AttendanceDisposalCode
				AND	AE.AttendanceDisposal.SourceContextCode				= Encounter.ContextCode
		
		LEFT JOIN WH.Site 
			ON		WH.Site.SourceSiteCode								= Encounter.SiteCode
				AND WH.Site.SourceContextCode							= Encounter.ContextCode
		
		LEFT JOIN AE.TriageCategory 
			ON		AE.TriageCategory.SourceTriageCategoryCode			= Encounter.TriageCategoryCode
				AND AE.TriageCategory.SourceContextCode					= Encounter.ContextCode
		
		LEFT JOIN WH.Sex 
			ON		WH.Sex.SourceSexCode								= Encounter.SexCode
				AND	WH.Sex.SourceContextCode							= Encounter.ContextCode
				
		LEFT JOIN WH.CCG
			ON		WH.CCG.CCGCode										= Encounter.AttendanceResidenceCCGCode
	
	WHERE	
			Encounter.ArrivalTime			BETWEEN @AttendanceFromDate AND @AttendanceToDate
		AND Encounter.ContextCode			= 'CEN||SYM'
		AND Encounter.AgeOnArrival			BETWEEN @AgeFrom AND @AgeTo
		AND (
				(COMM.Item IS NULL		AND @CommissionerCodes IS NULL)
				OR
				(COMM.Item IS NOT NULL	AND @CommissionerCodes IS NOT NULL) 
			)

	
	
	

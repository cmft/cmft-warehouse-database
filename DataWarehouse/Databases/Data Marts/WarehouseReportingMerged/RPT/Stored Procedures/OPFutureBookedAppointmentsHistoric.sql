﻿


CREATE PROCEDURE [RPT].[OPFutureBookedAppointmentsHistoric]
(
	 @StartBaseDate			DATE			= NULL
	,@Division				VARCHAR(50)		= NULL
	,@DirectorateCode		VARCHAR(20)		= NULL
	,@NationalSpecialtyCode	VARCHAR(1000)	= NULL
	,@PreOp					CHAR(1)			= NULL
)
AS

	DECLARE @StartDate		DATE
	DECLARE @EndDate		DATE
	
	SET @StartBaseDate = COALESCE(@StartBaseDate, DATEADD(WEEK, -8, GETDATE()))
	
	SET DATEFIRST  1	-- Monday

	SET @StartDate  = DATEADD(DAY, 1 - DATEPART(DW, @StartBaseDate), @StartBaseDate)
	SET @EndDate	= DATEADD(DAY, -DATEPART(DW, GETDATE()), GETDATE())			-- Previous week end date based on current date
	
	DECLARE @FCal TABLE 
	(
		 FinancialYearStartWkNo		INT
		,FinancialYearStart			CHAR(4)
		,WeekAdjustment				INT
		,FinancialYear				VARCHAR(15)
	)
	
	-- Create an in-line table of the Financial years calendar week number, this used to determine a financial year week number
	INSERT INTO @FCal
	(
		 FinancialYearStartWkNo
		,FinancialYearStart
		,WeekAdjustment
		,FinancialYear
	)	
	SELECT
		 FinancialYearStartWkNo		= YEAR(C.TheDate) * 52 + DATEPART(WK, C.TheDate)
		,FinancialYearStart			= LEFT(C.FinancialYear,4)
		 -- Adjustment to the derived week number, if the first week is a full week then start at Wk 1, if not start at Wk 0
		,WeekAdjustment				= CASE WHEN C.TheDate = C.FirstDayOfWeek THEN 1 ELSE 0 END
		,FinancialYear				= C.FinancialYear
	FROM
		(
			-- Get the financial year start date
			SELECT 
				 FinancialYearStartID	= MIN(CAL.DateID)
				,CAL.FinancialYear
			FROM 
				(
					SELECT DISTINCT
						FinancialYear
					FROM
						WH.Calendar			
					WHERE 
						TheDate BETWEEN @StartDate AND @EndDate
				) FY				-- Financial Years in the date selection period
				INNER JOIN WH.Calendar CAL
					ON FY.FinancialYear = CAL.FinancialYear
			GROUP BY
				CAL.FinancialYear
				
		) BD
		INNER JOIN WH.Calendar C
			ON BD.FinancialYearStartID = C.DateID

	-- Main query Historic
	SELECT
		 ApptWeekEnding			= APPTCAL.LastDayOfWeek
		,ApptWeekNo				= 'WK' + RIGHT('00' + CAST(YEAR(APPTCAL.TheDate) * 52 + DATEPART(WK, APPTCAL.TheDate) - FCAL.FinancialYearStartWkNo + FCAL.WeekAdjustment AS VARCHAR(2)),2)
		,ApptFYear				= FCAL.FinancialYearStart
		,Specialty				= SPEC.NationalSpecialty
		,NoticePeriod			= DATA.NoticePeriod
		,Cases					= 1
		,Appt = YEAR(APPTCAL.TheDate) * 52 + DATEPART(WK, APPTCAL.TheDate)
		,FY = FCAL.FinancialYearStartWkNo
		
	FROM
		(
			SELECT
				 E.AppointmentDateID
				,NoticePeriod			= DATEDIFF(DAY, E.AppointmentCreateDate, E.AppointmentDate) / 7 
		,IsPreOpAttendance = 
		
		CASE 
		WHEN E.ClinicCode = 'CPREADCL' THEN 'Y' 
		when E.ContextCode = 'TRA||UG' and exists

			(
			select 
				1
			FROM TraffordReferenceData.dbo.AppointmentType AppointmentType

			where
			left(apptype_relid,3) = 'pre'
			and AppointmentType.apptype_identity = E.AppointmentTypeCode

			) then 'Y'
			
		ELSE 'N' END
				,E.DirectorateCode
				,E.ReferralSpecialtyId
				,E.ContextCode
				,E.AppointmentCreateDate
				,E.AppointmentDate
				
			FROM				
				OP.Encounter E
						
			WHERE
					E.AppointmentDate				BETWEEN @StartDate AND @EndDate
				AND E.AppointmentCreateDate			IS NOT NULL
				AND (
							E.AppointmentStatusCode IN ('NR','DNA','CND','D','V') 
						OR	E.DNACode IN ('5','6')
					)
				AND COALESCE(E.IsWardAttender, 0)	= 0 
				AND E.DerivedFirstAttendanceFlag	= 1
		) DATA
		
		INNER JOIN WH.Calendar APPTCAL
			ON DATA.AppointmentDateID = APPTCAL.DateID
			
		INNER JOIN @FCal FCAL
			ON APPTCAL.FinancialYear = FCAL.FinancialYear
			
		INNER JOIN WH.Directorate DIR
			ON		DATA.DirectorateCode			= DIR.DirectorateCode
			
		INNER JOIN WH.Specialty SPEC
			ON		DATA.ReferralSpecialtyId		= SPEC.SourceSpecialtyId
				AND DATA.ContextCode				= SPEC.SourceContextCode

	WHERE
			
						(SPEC.NationalSpecialtyCode	IN (SELECT Item from fn_ListToTable(@NationalSpecialtyCode,',')) or @NationalSpecialtyCode = 'All')
	
		and  DIR.Division					= COALESCE(@Division, DIR.Division)
		and DIR.DirectorateCode				= COALESCE(@DirectorateCode, DIR.DirectorateCode)
		and DATA.IsPreOpAttendance			= COALESCE(@PreOp, DATA.IsPreOpAttendance) 
		


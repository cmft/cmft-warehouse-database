﻿
	CREATE proc [RPT].[PatientCancerSurvey]

	 @StartDate datetime
	,@Enddate datetime

	as


	Declare @DeceasedCheck Table

	(
		 NHSNumber varchar (25)
		

	)


	Insert into @DeceasedCheck (NHSNumber)


	Select NHSNumber = PAT_NATIONALNUMBER1 
				
	from xdatawh.Replica_UltraGendaPro.dbo.Patient Patient
		
	where 
		 Patient.PAT_NATIONALNUMBER1 is not null
		and Patient.PAT_DEATHDATE >= getdate () - 1095




---Extract Records

	--Declare @CancerRecords Table

	--	(
	--		 providerspellno varchar (25)
	--		,ClientCode varchar (25)
	--		,NHSNumber varchar (25)
	--		,Gender  varchar (25)
	--		,Title varchar (25)
	--		,FirstName varchar (50)
	--		,Surname varchar (50)

	--		,Address1 varchar (255)
	--		,Address2 varchar (255)
	--		,Address3 varchar (255)
	--		,Address4 varchar (255)
	--		,Address5 varchar (255)
	--		,Postcode varchar (255)
	--		,DateOfBirth varchar (255)
	--		,Ethnicty varchar (25)
	--		,DayOfAdmission varchar (25)
	--		,MonthofAdmission varchar (25)
	--		,YearOfAdmission varchar (25)
	--		,DayOfDischarge varchar (25)
	--		,MonthofDischarge varchar (25)
	--		,YearOfDischarge varchar (25)
	--		,lenghtofStay varchar (25)
	--		,FCE1DiagCode varchar (25)
	--		,MainSpecialty varchar (25)
	--		,ReferringPCT varchar (25)
	--		,PatientClassification varchar (25)
	--		,[Site] varchar (25)
	--		,Districtno varchar (25)
	--		,NationalValueCode varchar (25)

	--		)

	--Insert into @CancerRecords 
	--(
	--		providerspellno, ClientCode ,NHSNumber ,Gender	,Title ,FirstName ,Surname
	--		,Address1 ,Address2 ,Address3 ,Address4 ,Address5 ,Postcode ,DateOfBirth ,Ethnicty ,DayOfAdmission	,MonthofAdmission ,YearOfAdmission ,DayOfDischarge
	--		,MonthofDischarge, YearOfDischarge ,lenghtofStay ,FCE1DiagCode ,MainSpecialty ,ReferringPCT ,PatientClassification	,[Site]	,Districtno	,NationalValueCode
	--)

		select 
		--distinct

			encounter.providerspellno

			 --PatientRecordNumber = 
			 
				--		1001 + rank() OVER (ORDER BY encounter.Districtno)
						
	
			,ClientCode = null
			,Encounter.NHSNumber
			,Gender = Sex.NationalSexCode
			,Title = Encounter.PatientTitle
			,FirstName = Encounter.PatientForename
			,Surname = Encounter.PatientSurname

			,Address1 = Encounter.PatientAddress1
			,Address2 = Encounter.PatientAddress2
			,Address3 = Encounter.PatientAddress3
			,Address4 = Encounter.PatientAddress4
			,Address5 = null

			,Encounter.Postcode

			,DateOfBirth = 
			
						CONVERT(VARCHAR(8), Encounter.DateOfBirth, 112) 

			,Ethnicty = 
						case
						when EthnicCategory.NationalEthnicCategoryCode = '99' then 'Z'
						else EthnicCategory.NationalEthnicCategoryCode
						end

			,DayOfAdmission = day(AdmissionDate.TheDate)

			,MonthofAdmission = month(AdmissionDate.TheDate)

			,YearOfAdmission = year(AdmissionDate.TheDate)
			

			,DayOfDischarge = day(DischargeDate.TheDate)

			,MonthofDischarge = month(DischargeDate.TheDate)

			,YearOfDischarge = year(DischargeDate.TheDate)

			,lenghtofStay = Encounter.LOS

			,FCE1DiagCode = Diagnosis1.DiagnosisCode + ' - ' + Diagnosis1.[Diagnosis]
			--,FCE1DiagCode2 = Diagnosis1A.DiagnosisCode + ' - ' + Diagnosis1A.[Diagnosis]
			--,FCE1DiagCode3 = Diagnosis1B.DiagnosisCode + ' - ' + Diagnosis1B.[Diagnosis]
			--,FCE1DiagCode4 = Diagnosis1C.DiagnosisCode + ' - ' + Diagnosis1C.[Diagnosis]
			--,FCE1DiagCode5 = Diagnosis1D.DiagnosisCode + ' - ' + Diagnosis1D.[Diagnosis]
			--,FCE1DiagCode6 = Diagnosis1E.DiagnosisCode + ' - ' + Diagnosis1E.[Diagnosis]
			--,FCE1DiagCode7 = Diagnosis1F.DiagnosisCode + ' - ' + Diagnosis1F.[Diagnosis]



			--,FCE2DiagCode = Diagnosis2.DiagnosisCode + ' - ' + Diagnosis2.[Diagnosis]
			--,FCE2DiagCode2 = Diagnosis2A.DiagnosisCode + ' - ' + Diagnosis2A.[Diagnosis]
			--,FCE2DiagCode3 = Diagnosis2B.DiagnosisCode + ' - ' + Diagnosis2B.[Diagnosis]
			--,FCE2DiagCode4 = Diagnosis2C.DiagnosisCode + ' - ' + Diagnosis2C.[Diagnosis]
			--,FCE2DiagCode5 = Diagnosis2D.DiagnosisCode + ' - ' + Diagnosis2D.[Diagnosis]
			--,FCE2DiagCode6 = Diagnosis2E.DiagnosisCode + ' - ' + Diagnosis2E.[Diagnosis]
			--,FCE2DiagCode7 = Diagnosis2F.DiagnosisCode + ' - ' + Diagnosis2F.[Diagnosis]



			,MainSpecialty = Consultant.MainSpecialtyCode

			,ReferringPCT = left(coalesce(Encounter.PCTCode, Encounter.ResidencePCTCode), 6)
			
			
			,PatientClassification = NationalPatientClassificationCode

			,[Site] = [Site].NationalSiteCode

			,Encounter.Districtno

			,NationalValueCode = AdminCategory.NationalAdministrativeCategoryCode

			--,Confirmed =

			--	case

			--		when Encounter.ProviderSpellNo like 'A%' and 

			--			exists
			
			--			(

			--			select 1 FROM TraffordCancer.dbo.tblCanDiagnosis a
			--			where a.NHSNumber = Encounter.NHSNumber
			--			and	 CancerStatusValue like '%conf%'
			--			) then 1
			--		when Encounter.ProviderSpellNo not like 'A%' and 
		
			--			exists
			--				(
			--				select  [N4_2_DIAGNOSIS_CODE] ,p.* 
			--				from [CancerRegister].[dbo].[tblMAIN_REFERRALS] R

			--				inner join [CancerRegister].[dbo].[tblDEMOGRAPHICS] P
			--				on P.PATIENT_ID = R.PATIENT_ID
			--				and left(N4_2_DIAGNOSIS_CODE,1) = 'C'
			--		where p.n1_1_nhs_number = Encounter.NHSNumber
			--) then 1
			--else 0
			--end

			into #CancerRecords

			from
			
			WarehouseReportingMerged.APC.Encounter Encounter

			left join Wh.Diagnosis Diagnosis1
			on	Diagnosis1.DiagnosisCode = Encounter.PrimaryDiagnosisCode

			left join Wh.Diagnosis Diagnosis1A
			on	Diagnosis1A.DiagnosisCode = Encounter.SecondaryDiagnosisCode1

			left join Wh.Diagnosis Diagnosis1B
			on	Diagnosis1B.DiagnosisCode = Encounter.SecondaryDiagnosisCode2

			left join Wh.Diagnosis Diagnosis1C
			on	Diagnosis1C.DiagnosisCode = Encounter.SecondaryDiagnosisCode3

			left join Wh.Diagnosis Diagnosis1D
			on	Diagnosis1D.DiagnosisCode = Encounter.SecondaryDiagnosisCode4

			left join Wh.Diagnosis Diagnosis1E
			on	Diagnosis1E.DiagnosisCode = Encounter.SecondaryDiagnosisCode5

			left join Wh.Diagnosis Diagnosis1F
			on	Diagnosis1F.DiagnosisCode = Encounter.SecondaryDiagnosisCode6

			 inner join WarehouseReportingMerged.wh.Calendar DischargeDate
			 on DischargeDate.DateID = Encounter.DischargeDateID

			 inner join WarehouseReportingMerged.wh.Calendar AdmissionDate
			 on AdmissionDate.DateID = Encounter.AdmissionDateID

			 inner join WarehouseReportingMerged.APC.PatientClassification PatientClassification
			 on PatientClassification.SourcePatientClassificationID = Encounter.PatientClassificationID
			 and PatientClassification.SourceContextCode = Encounter.ContextCode

			 --inner join WarehouseReportingMerged.APC.LastEpisodeInSpell LastEpisodeInSpell
			 --on LastEpisodeInSpell.SourceLastEpisodeInSpellID = Encounter.LastEpisodeInSpellID
			 --and LastEpisodeInSpell.SourceContextCode = Encounter.ContextCode
			 

			 left join WarehouseReportingMerged.wh.EthnicCategory EthnicCategory
			 on EthnicCategory.SourceEthnicCategoryID = Encounter.EthnicOriginID
			 and EthnicCategory.SourceContextCode = Encounter.ContextCode

			 left join WarehouseReportingMerged.wh.Site [Site]
			 on [Site].SourceSiteID = Encounter.siteid


			 left join WarehouseReportingMerged.WH.Consultant Consultant
			 on Consultant.SourceConsultantID = Encounter.ConsultantID
			 and Consultant.SourceContextCode = Encounter.ContextCode

			 left join WarehouseReportingMerged.WH.Sex Sex
			 on Sex.SourceSexID = Encounter.sexid
			 and Sex.SourceContextCode = Encounter.ContextCode

			 LEFT OUTER JOIN WarehouseReportingMerged.WH.AdministrativeCategory AdminCategory
			ON	AdminCategory.SourceAdministrativeCategoryID = Encounter.AdminCategoryID
			and AdminCategory.SourceContextCode = Encounter.ContextCode
			

		left join [APC].[Encounter] FCE2
		on	FCE2.providerspellno = Encounter.ProviderSpellNo
		and FCE2.ContextCode = Encounter.ContextCode 
		and FCE2.SourceEncounterNo = 2

		left join WH.Specialty Specialty2
		on	Specialty2.SourceSpecialtyID = FCE2.SpecialtyID

		left join WH.Consultant Consultant2
		on	Consultant2.SourceConsultantID = FCE2.ConsultantID

		left join Wh.Diagnosis Diagnosis2
		on	Diagnosis2.DiagnosisCode = FCE2.PrimaryDiagnosisCode

			left join Wh.Diagnosis Diagnosis2A
			on	Diagnosis2A.DiagnosisCode = FCE2.SecondaryDiagnosisCode1


			left join Wh.Diagnosis Diagnosis2B
			on	Diagnosis2B.DiagnosisCode = FCE2.SecondaryDiagnosisCode2

			left join Wh.Diagnosis Diagnosis2C
			on	Diagnosis2C.DiagnosisCode = FCE2.SecondaryDiagnosisCode3

			left join Wh.Diagnosis Diagnosis2D
			on	Diagnosis2D.DiagnosisCode = FCE2.SecondaryDiagnosisCode4

			left join Wh.Diagnosis Diagnosis2E
			on	Diagnosis2E.DiagnosisCode = FCE2.SecondaryDiagnosisCode5

			left join Wh.Diagnosis Diagnosis2F
			on	Diagnosis2F.DiagnosisCode = FCE2.SecondaryDiagnosisCode6

			  		 
			 where

		(


			 Encounter.[FirstEpisodeInSpellIndicator] = 1
			 and
			 DischargeDate.TheDate between @StartDate and @Enddate

			--Excude Private Patients
			and 
					(
			
			 AdminCategory.NationalAdministrativeCategoryCode in ('01', '03', '04', '99') 
			 --or AdminCategory.NationalValueCode is null
					)
			
			--Deceased Patients excluded
			and
			Encounter.DateOfDeath is null

			--Include only patients over 16
			and
			Encounter.AgeCode > '16'
			
	)

			

	 ----ICD10 to be included 1st FCE
			 
			 			 and
					
					--(

			--1st FCE
			 

	
			
				(
			 Encounter.PrimaryDiagnosisCode = 'D05'
			 or
			 Encounter.PrimaryDiagnosisCode between 'C00' and 'C99.9'
				)

			--ICD10 Not included
			and 

			(
			Encounter.PrimaryDiagnosisCode not between 'C44.0' and 'C44.9'
			and 
			Encounter.PrimaryDiagnosisCode not between 'C84.0' and 'C84.5'


		

			)
			-- or
			-- Encounter.[FirstEpisodeInSpellIndicator] = 1 and
			-- 			(
			-- FCE2.PrimaryDiagnosisCode = 'D05'
			-- or
			-- FCE2.PrimaryDiagnosisCode between 'C00' and 'C99.9'
			--	)

			----ICD10 Not included
			--and 
			--FCE2.PrimaryDiagnosisCode not between 'C44.0' and 'C44.9'
			--and 
			--FCE2.PrimaryDiagnosisCode not between 'C84.0' and 'C84.5'

			--		)

				--Exclude patients death after activity
			and not exists

						(
			
					Select NHSNumber
				
					from  @DeceasedCheck DeceasedCheck
					
					where 
	
		 				DeceasedCheck.NHSNumber =  Encounter.NHSNumber
		
						)



						drop table #CancerRecords
﻿



/****************************************************************************************
	Stored procedure : [RPT].[FutureTCIDates]
	Description		 : SSRS proc for the Visual Studio report
	
	Reports >> Activity >> Future TCI Dates
	
	Measures
	 Weeks from Preadmission to TCI date for the current week ending
	 TCI date for future week endings

	Modification History
	====================
	
	Date      Person     Description
	====================================================================================
	10.01.14    MH       Intial Coding

*****************************************************************************************/

CREATE PROCEDURE [RPT].[FutureTCIDates]
(
	 @StartBaseDate			DATE			= NULL
	,@Division				VARCHAR(50)		= NULL
	,@DirectorateCode		VARCHAR(20)		= NULL
	,@NationalSpecialtyCode	VARCHAR(1000)	= NULL
	,@NoWeekEnds			INT				= 4		-- Determines the end date of the query and represents the number of 
													-- new and follow-up appts booked by weekending based on the current date
	,@WaitingListDate		DATE			= NULL
)
AS
	DECLARE @StartDate			DATE
	DECLARE @EndDate			DATE
	
	-- Get latest waiting list census date
	IF (@WaitingListDate IS NULL)
		SELECT 
			@WaitingListDate = MAX(CensusDate)
		FROM
			APCWL.Encounter
		WHERE 
			CensusDate > DATEADD(DAY,-7,GETDATE())		
			
	-- Determine week endings and period to query
	SET @StartBaseDate = COALESCE(@StartBaseDate, GETDATE())
	
	SET DATEFIRST  1	-- Monday

	SET @StartDate  = DATEADD(DAY, 1 - DATEPART(DW, @StartBaseDate), @StartBaseDate)
	SET @EndDate	= DATEADD(DAY, -1, DATEADD(WEEK, @NoWeekEnds, @StartDate))		-- End Date is a Sunday

	-- Main data Query
SELECT
	 WeekEnding				= TCICAL.LastDayOfWeek
	,NoticePeriod			= CASE 
								When TCICAL.FirstDayOfWeek = @StartDate and  ContextCode = 'TRA||UG' and TCICAL.TheDate <> '01 jan 1900' then DATEDIFF(DAY, DateOnWaitingList, TCICAL.TheDate) / 7
								WHEN TCICAL.FirstDayOfWeek = @StartDate	-- In the first week of the date range (Notice Given columns in the report)
									THEN DATEDIFF(DAY, PRE.PreAdmitDate1, TCICAL.TheDate) / 7
								ELSE NULL
							  END
	,NoPreAdmission			= CASE 
								When  TCICAL.FirstDayOfWeek = @StartDate AND  TCICAL.TheDate = '01 jan 1900' then 'Y' 
								WHEN TCICAL.FirstDayOfWeek = @StartDate AND PRE.PreAdmitDate1 IS NULL THEN 'Y' ELSE 'N' END
	,E.DirectorateCode
	,E.SpecialtyId
	,E.AdmissionMethodID
	,E.ContextCode
	,PreAdmitDate			= PRE.PreAdmitDate1
	,TCIDate				= TCICAL.TheDate
	,E.ManagementIntentionCode
	--,ContextCode
	
	INTO #DATA	
	FROM
		APCWL.Encounter E
		
		INNER JOIN WH.Calendar TCICAL
			ON		E.TCIDateID			 = TCICAL.DateID
			
		LEFT JOIN  [Infocom_PAS].[dbo].[smsmir_preadmission] PRE 
			ON		E.SourcePatientNo			= PRE.InternalNo
				AND E.SourceEncounterNo			= PRE.EpisodeNo
				AND PRE.PreAdmCancRsn			IS NULL
				--AND E.ExpAdmitDate				BETWEEN @StartDate AND @EndDate
		
	WHERE
			E.CensusDate			= @WaitingListDate
		AND TCICAL.TheDate			BETWEEN @StartDate AND @EndDate
		
		
	SELECT
		 WeekEnding						= #DATA.WeekEnding
		,AdmissionTypeDesc				= CASE	
											when ManagementIntentionCode = 'PlanIP' then 'Plnd'
											when ManagementIntentionCode = 'PlanDC' then 'Plnd'
											when AM.NationalAdmissionMethodCode = '13' THEN 'Plnd'
											ELSE 'WL'
										  END
		,WaitingListDate				= @WaitingListDate
		,Directorate					= DIR.Directorate
		,DirectorateCode				= DIR.DirectorateCode
		,Division						= DIR.Division	
		,Specialty						= SPEC.NationalSpecialtyLabel
		,NationalSpecialtyCode			= SPEC.NationalSpecialtyCode
		,Cases							= 1 
		,NoticePeriod0					= CASE WHEN NoticePeriod = 0 THEN 1 ELSE 0 END
		,NoticePeriod1					= CASE WHEN NoticePeriod = 1 THEN 1 ELSE 0 END
		,NoticePeriod2					= CASE WHEN NoticePeriod = 2 THEN 1 ELSE 0 END
		,NoticePeriod3					= CASE WHEN NoticePeriod = 3 THEN 1 ELSE 0 END
		,NoticePeriod4					= CASE WHEN NoticePeriod = 4 THEN 1 ELSE 0 END
		,NoticePeriod5					= CASE WHEN NoticePeriod = 5 THEN 1 ELSE 0 END
		,NoticePeriod6					= CASE WHEN NoticePeriod = 6 THEN 1 ELSE 0 END
		,NoticePeriod7					= CASE WHEN NoticePeriod = 7 THEN 1 ELSE 0 END
		,NoticePeriod8					= CASE WHEN NoticePeriod = 8 THEN 1 ELSE 0 END
		,NoticePeriod8Plus				= CASE WHEN NoticePeriod > 8 THEN 1 ELSE 0 END
		,NoticePeriodNoPreadmit			= CASE WHEN NoPreAdmission = 'Y' THEN 1 ELSE 0 END
		
	FROM
		#DATA
		
		INNER JOIN WH.Directorate DIR
			ON		#DATA.DirectorateCode			= DIR.DirectorateCode
			
		INNER JOIN WH.Specialty SPEC
			ON		#DATA.SpecialtyId				= SPEC.SourceSpecialtyId
				AND #DATA.ContextCode				= SPEC.SourceContextCode
				
		INNER JOIN APC.AdmissionMethod AM
			ON		#DATA.AdmissionMethodID			= AM.SourceAdmissionMethodId
				AND	#DATA.ContextCode				= AM.SourceContextCode
	
	WHERE
			SPEC.NationalSpecialtyCode	IN (SELECT Item from fn_ListToTable(@NationalSpecialtyCode,','))
		AND DIR.Division				= COALESCE(@Division, DIR.Division)
		AND DIR.DirectorateCode			= COALESCE(@DirectorateCode, DIR.DirectorateCode)






﻿
/******************************************************************************
**  Name: RPT.DQActivityBrokerUnmatchedWLDrillThrough
**  Purpose: Data Quality drill through report for un-matched ActivityBroker
**           APC waiting list patients
**
**  Parent Stored procedure : RPT.DQActivityBrokerUnmatchedPatients
**
**	Input Parameters
**	
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 14.11.13      MH       Created
******************************************************************************/

CREATE PROCEDURE RPT.DQActivityBrokerUnmatchedWLDrillThrough
(
	@ReportType		NVARCHAR(10)
)
AS
	DECLARE @SnapshotTime	DATETIME
	DECLARE @NullDate		DATE = '1 Jan 1900'

-- Get the time the data was built	
	SELECT 
		@SnapshotTime = MAX(EventTime)
	FROM
		ActivityBroker.Utility.AuditLog
		
	SELECT 
		  SourceUniqueId				= ORIGAPCWAIT.APCWaitID
		 ,MeasureDifference	=			CASE
											WHEN DESTAPCWAIT.APCWaitID IS NULL THEN 'No Match'
											ELSE 'Data Mismatch'
										END
		 ,[District Number]				= ORIGAPCWAIT.DistrictNo
		 ,[NHS Number]					= ORIGAPCWAIT.NHSNumber
		 ,[Waiting List Code]			= ORIGAPCWAIT.WaitingListCode
		 --,Division					= ORIGAPCWAIT.
		 ,[Originating Site]			= ORIGAPCWAITSITE.NationalSite
		 ,Speciality					= ORIGAPCWAIT.Specialty
		 ,[Waiting List Date]			= ORIGAPCWAIT.DateOnWaitingList
		 ,[Intended Management]			= ORIGIM.NationalIntendedManagement
		 ,[Intended Management Code]	= ORIGAPCWAIT.IntendedManagementCode
		 ,[Method of Admission]			= ORIGAM.NationalAdmissionMethod
		 ,[AdmissionMethodCode]			= ORIGAPCWAIT.AdmissionMethodcode
		 ,[Intended Procedure]			= ORIGAPCWAIT.IntendedProcedureCode
		 ,[TCI Date]					= ORIGAPCWAIT.TCIDate
		 ,[Consultant]					= ORIGCONSULTANT.SourceConsultant
		 ,MismatchIntendedProcedure		= CASE 
											WHEN		DESTAPCWAIT.APCWaitID IS NOT NULL 
													AND COALESCE(ORIGAPCWAIT.IntendedProcedureCode,'')	<> COALESCE(DESTAPCWAIT.IntendedProcedureCode,'')
											THEN COALESCE(DESTAPCWAIT.IntendedProcedureCode,'No Value')
											ELSE NULL
										  END
		 ,MismatchIntendedManagement	= CASE 
											WHEN		DESTAPCWAIT.APCWaitID IS NOT NULL 
													AND COALESCE(ORIGAPCWAIT.IntendedManagementCode,'')	<> COALESCE(DESTAPCWAIT.IntendedManagementCode,'')
											THEN COALESCE(DESTIM.NationalIntendedManagement,'No Value')
											ELSE NULL
										  END		
		 ,MismatchAdmissionMethod		= CASE 
											WHEN		DESTAPCWAIT.APCWaitID IS NOT NULL 
													AND COALESCE(ORIGAPCWAIT.AdmissionMethodcode,'')	<> COALESCE(DESTAPCWAIT.AdmissionMethodcode,'')
											THEN COALESCE(DESTAM.NationalAdmissionMethod,'No Value')
											ELSE NULL
										  END		
		 ,MismatchTCIDate				= CASE 
											WHEN		DESTAPCWAIT.APCWaitID IS NOT NULL 
													AND COALESCE(ORIGAPCWAIT.TCIDate, @NullDate)		<> COALESCE(DESTAPCWAIT.TCIDate,@NullDate)
											THEN DESTAPCWAIT.TCIDate
											ELSE NULL
										  END
		 ,MismatchConsultant			= CASE
											WHEN		DESTAPCWAIT.APCWaitID IS NOT NULL 
													AND	CASE 
															WHEN ORIGCONSULTANT.NationalConsultant = 'UnAssigned' 
																THEN ORIGCONSULTANT.SourceConsultantCode
																ELSE ORIGCONSULTANT.NationalConsultant
														END
														<>
														CASE 
															WHEN DESTCONSULTANT.NationalConsultant = 'UnAssigned' 
																THEN DESTCONSULTANT.SourceConsultantCode
																ELSE DESTCONSULTANT.NationalConsultant
														END
											 THEN DESTCONSULTANT.SourceConsultantCode
											 ELSE NULL
											END
											
		 
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		INNER JOIN ActivityBroker.AB.Pathway P
			ON ABT.PathwayID = P.PathwayID
		LEFT JOIN ActivityBroker.AB.APCWait ORIGAPCWAIT
			ON P.OriginAPCWaitID		= ORIGAPCWAIT.APCWaitID
		LEFT JOIN ActivityBroker.AB.APCWait DESTAPCWAIT
			ON P.DestinationAPCWaitID	= DESTAPCWAIT.APCWaitID
		LEFT JOIN WH.Consultant ORIGCONSULTANT
			ON		ORIGAPCWAIT.ContextCode			= ORIGCONSULTANT.SourceContextCode
				AND ORIGAPCWAIT.ConsultantCode		= ORIGCONSULTANT.SourceConsultantCode
		LEFT JOIN WH.Consultant DESTCONSULTANT
			ON		DESTAPCWAIT.ContextCode			= DESTCONSULTANT.SourceContextCode
				AND DESTAPCWAIT.ConsultantCode		= DESTCONSULTANT.SourceConsultantCode
		LEFT JOIN [APC].[IntendedManagement] ORIGIM
			ON		ORIGAPCWAIT.IntendedManagementCode	= ORIGIM.SourceIntendedManagementCode
				AND ORIGAPCWAIT.ContextCode				= ORIGIM.SourceContextCode
		LEFT JOIN [APC].[IntendedManagement] DESTIM
			ON		DESTAPCWAIT.IntendedManagementCode	= DESTIM.SourceIntendedManagementCode
				AND DESTAPCWAIT.ContextCode				= DESTIM.SourceContextCode
		LEFT JOIN [APC].[AdmissionMethod] ORIGAM
			ON		ORIGAPCWAIT.AdmissionMethodcode		= ORIGAM.SourceAdmissionMethodCode
				AND ORIGAPCWAIT.ContextCode				= ORIGAM.SourceContextCode
		LEFT JOIN [APC].[AdmissionMethod] DESTAM
			ON		DESTAPCWAIT.AdmissionMethodcode		= DESTAM.SourceAdmissionMethodCode
				AND DESTAPCWAIT.ContextCode				= DESTAM.SourceContextCode
		LEFT JOIN WH.Site ORIGAPCWAITSITE
			ON		ORIGAPCWAIT.SiteCode	= ORIGAPCWAITSITE.SourceSiteCode
				AND ORIGAPCWAIT.ContextCode	= ORIGAPCWAITSITE.SourceContextCode
			
	WHERE
			COALESCE(DESTAPCWAIT.APCWaitID, -1) = CASE WHEN @ReportType = 'WLNM' THEN -1 ELSE DESTAPCWAIT.APCWaitID END
		AND	P.PathwayTypeID IN (1,2)
		AND	(
				COALESCE(ORIGAPCWAIT.IntendedProcedureCode,'')	<> COALESCE(DESTAPCWAIT.IntendedProcedureCode,'')
			OR	COALESCE(ORIGAPCWAIT.IntendedManagementCode,'')	<> COALESCE(DESTAPCWAIT.IntendedManagementCode,'')
			OR	COALESCE(ORIGAPCWAIT.AdmissionMethodcode,'')	<> COALESCE(DESTAPCWAIT.AdmissionMethodcode,'')
			OR	COALESCE(ORIGAPCWAIT.TCIDate, @NullDate)		<> COALESCE(DESTAPCWAIT.TCIDate, @NullDate)
			OR	CASE 
					WHEN ORIGCONSULTANT.NationalConsultant = 'UnAssigned' 
						THEN ORIGCONSULTANT.SourceConsultantCode
						ELSE ORIGCONSULTANT.NationalConsultant
				END
				<>
				CASE 
					WHEN DESTCONSULTANT.NationalConsultant = 'UnAssigned' 
						THEN DESTCONSULTANT.SourceConsultantCode
						ELSE DESTCONSULTANT.NationalConsultant
				END
			)
			
	ORDER BY
		ORIGAPCWAIT.DistrictNo

﻿





CREATE procedure [RPT].[VTECoding] 
(
	@StartDate	date
	,@EndDate	date
	,@CodingComplete bit = null
)

as

/*	
	Created:			Created By:		Created Reason:
	08/01/2014			Paul Egan and Darren Griffiths
	---------------------------------------------------
	
	Modified:			Modified By:	Modified Reason:
	15/01/2014			Paul Egan		
	26/02/2014			Paul Egan		Removed dependency on VTECategory code as this now 
										includes BedMan not just PAS
	---------------------------------------------------
*/

/*================= debug only ================*/
--declare @StartDate	date = '1 jan 1900';
--declare @EndDate	date = '8 jan 2014';
--declare @CodingComplete bit = NULL;
/*=============================================*/

with BedEvent as	-- 1 row per spell, VTE events only
(
	select
		BedManEventRecno
		,SourceUniqueID
		,SourceSpellNo
		,EventDate
		,VTEAssessmentRecordedInBedman = 
			case 
				when EventDate > 0
				then 1
			end
		,EventTypeCode
		,EventDetails
		,SourcePatientNo
		,AdmissionTime
		,DischargeTime
		,Created
		,Updated
		,ByWhom
	from
		Warehouse.Bedman.Event
	where
		EventTypeCode = 4 -- VTE ASSESSMENT
	and
		not exists
				(
					select
						1
					from
						Warehouse.Bedman.Event Later
					where
						Later.SourceSpellNo = Event.SourceSpellNo
					and
						Later.BedManEventRecno > Event.BedManEventRecno
					and
						Later.EventTypeCode = 4 -- VTE ASSESSMENT
				)
)

select
	--bedspell.[HospitalNo]
	CasenoteNumber
	,DistrictNo
	,AdmissionDate
	,DischargeDate
	,CodingCompleteDate
	,CodingComplete = 
		case
			when CodingCompleteDate is not null
			then 1
		end
	,Research1
	,Research2
	,Excluded = 
		case
			when VTEExclusion.Allocation is not null 
			then 1
		end
	,ExclusionReason =  VTEExclusion.Allocation
	,Encounter.PatientCategoryCode		--Field added to check for 'RD' exclusion in WarehouseOLAPMerged.ETL.TLoadCenAPCBaseEncounter. PE 10/01/2014
	,PatientAgeAtEpisodeStart =			--Field added to check for < 18 exclusion in WarehouseOLAPMerged.ETL.TLoadCenAPCBaseEncounter. PE 10/01/2014
		datediff(yy, Encounter.DateOfBirth, Encounter.EpisodeStartDate) - 
			(
			case 
			when
				(datepart(m, Encounter.DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
			or
				(
					datepart(m, Encounter.DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
				and	datepart(d, Encounter.DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
				)
			then 1
			else 0
			end
			)
	,VTEAssessmentRecordedInBedman
	,CodedBedmanNotPAS =		-- NB Cannot use 'VTECategoryCode <> 'C'' as this code definition has changed to include PAS or Bedman, instead of just PAS.
		case					-- Modified PE 22/01/2014
			when VTEAssessmentRecordedInBedman = 1
				and left(Encounter.Research2 , 1) = 'V'
				and charindex('C', Encounter.Research2) > 1
			then null		
				
			when VTEAssessmentRecordedInBedman = 1
			then 1
		end
	,CodedResearch1NotResearch2 =	-- NB Cannot use 'VTECategoryCode <> 'C'' as this code definition has changed to include PAS or Bedman, instead of just PAS.
		case						-- Modified PE 22/01/2014
			when left(Encounter.Research2 , 1) = 'V'
				and charindex('C', Encounter.Research2) > 1
			then null
		
			when left(Encounter.Research1 , 1) = 'V' 
			and	charindex('C' ,Encounter.Research1) > 1
			then 1
		end 
	,CodedResearch2IncorrectSpelling =		-- NB Cannot use 'VTECategoryCode <> 'C'' as this code definition has changed to include PAS or Bedman, instead of just PAS.
		case								-- Modified PE 22/01/2014
			when Encounter.Research2 = 'VTE-C'
			then null
			
			when left(Encounter.Research2 , 1) = 'V'
			and charindex('C', Encounter.Research2) > 1
			then 1
		end 
	--,BedEvent.EventDetails.value('(/VTEAssessment/AssessmentCompleted_TS)[1]', 'datetime')
	--,VTECategoryCode
from
	BedEvent

inner join Warehouse.Bedman.EventType 
on	bedevent.EventTypeCode = EventType.EventTypeCode
	
inner join APC.Encounter
on	BedEvent.SourcePatientNo = Encounter.SourcePatientNo
and	BedEvent.AdmissionTime = Encounter.AdmissionTime
and	Encounter.FirstEpisodeInSpellIndicator = '1'


left join
		(
		select
			DatasetRecno
			,Allocation.SourceAllocationID
			,Allocation.Allocation
		from
			WarehouseOLAPMergedV2.Allocation.DatasetAllocation

		inner join WarehouseOLAPMergedV2.Allocation.Allocation
		on Allocation.AllocationID = DatasetAllocation.AllocationID

		where
			  DatasetAllocation.DatasetCode = 'APC'
		and DatasetAllocation.AllocationTypeID = 3

		) VTEExclusion
		
		
on	VTEExclusion.DatasetRecno = Encounter.MergeEncounterRecno

where
	Encounter.AdmissionDate between @StartDate and @EndDate	
and		
	(
		cast(
			case
				when Encounter.CodingCompleteDate is null -- not coded
				then 'False'
				else 'True'
			end 
			as bit
			) = @CodingComplete
	or
		@CodingComplete is null
	)
		
order by 1, 2, 3, 4







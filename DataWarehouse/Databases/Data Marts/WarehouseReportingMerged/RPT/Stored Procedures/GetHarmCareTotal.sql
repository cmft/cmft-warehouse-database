﻿

CREATE PROCEDURE [RPT].[GetHarmCareTotal]
(@StartDate as datetime,
@EndDate as datetime,
@Hours as int,
@Grouping as varchar(2))
AS

if @StartDate < '01 July 2012'
BEGIN
	SET @StartDate = '01 July 2012'
END


--DECLARE @StartDate AS DATE
--DECLARE @EndDate as DATE
--DECLARE @Hours AS INT
--DECLARE @Grouping as varchar(2)
--SET @StartDate = '17 Sep 2012'
--SET @EndDate = '23 Sep 2012'
--SET @Hours = 24
--SET @Grouping = 'M'

IF @Grouping = 'WW'
BEGIN

SELECT 
		TheDate =			Null,
		WeekNo =			CrossCal.weekno,
		TheMonth =			Null,
		FinancialQuarter  = CrossCal.FinancialQuarter,
		PatientCount =		COUNT(E.MergeEncounterRecno),
		HarmCount =			COUNT(HARM.WardEncounterRecno),
		Division =			DIR.Division, 
		SourceWardCode =	COALESCE(HARM.SourceWardCode, W.SourceWardCode),
--		SourceWardCode =	WARD.SourceWardCode,
		--HarmCount =			Case when HARM.Patient IS NOT NULL THEN 1
		--					ELSE 0
		--					End,
		PatientCategory =	E.PatientCategoryCode,
		SortOrder =			CrossCal.WeekNoKey

  FROM APC.WardStay WS
  CROSS JOIN (SELECT DISTINCT FinancialMonthKey, WeekNo, WeekNoKey, FinancialQuarter, FirstDayOfWeek, LastDayOfWeek 
				FROM WH.Calendar 
				WHERE TheDate BETWEEN @StartDate AND @EndDate) CrossCal  
  INNER JOIN APC.Encounter E
	ON E.SourcePatientNo = WS.SourcePatientNo
	AND E.SourceSpellNo = WS.SourceSpellNo
	AND E.FirstEpisodeInSpellIndicator = 1
  INNER JOIN WH.Directorate DIR 
    ON DIR.DirectorateCode = E.StartDirectorateCode
  INNER JOIN APC.Ward W
	ON WS.WardID = W.SourceWardID  
 -- INNER JOIN BedMan.dbo.HospSpell HS
	--ON HS.Patient = E.SourcePatientNo AND HS.AdmitDate = E.AdmissionTime 
  LEFT JOIN 	
  (SELECT distinct WardEncounterRecno,  WeekNo , SourceWardCode, Division, PatientCategory FROM Legacy.HarmCare
   WHERE (Fall = 1 OR PUHFC2 = 1 OR UTI = 1 OR VTETreatment = 1)) HARM
	ON --E.SourcePatientNo = HARM.Patient AND 
	harm.WeekNo = CrossCal.WeekNo and  WS.MergeEncounterRecno = HARM.WardEncounterRecno AND W.SourceWardCode = HARM.SourceWardCode
  WHERE --E.FirstEpisodeInSpellIndicator = 1 
  
  
  --AND AdmissionTime <= @EndDate AND ISNULL(DischargeTime,GETDATE()) >= @StartDate
  WS.StartDate <= LastDayOfWeek 
  AND ISNULL(WS.EndDate,GETDATE()) >= FirstDayOfWeek

  
  GROUP BY CrossCal.weekno, CrossCal.FinancialQuarter,DIR.Division, 
  COALESCE(HARM.SourceWardCode, W.SourceWardCode), E.PatientCategoryCode, CrossCal.WeekNo, CrossCal.WeekNoKey

END

IF @Grouping = 'M'
BEGIN

SELECT 
		TheDate =			Null,
		WeekNo =			Null,
		TheMonth =			CrossCal.TheMonth,
		FinancialQuarter  = CrossCal.FinancialQuarter,
		PatientCount =		COUNT(E.EncounterRecNo),
		HarmCount =			COUNT(HARM.WardEncounterRecno),
		Division =			DIR.Division, 
		SourceWardCode =	COALESCE(HARM.SourceWardCode, W.SourceWardCode),
		PatientCategory =	E.PatientCategoryCode,
		SortOrder =			CrossCal.FinancialMonthKey

  FROM APC.WardStay WS
  CROSS JOIN (SELECT DISTINCT FinancialMonthKey, TheMonth, FinancialQuarter 
				FROM WH.Calendar 
				WHERE TheDate BETWEEN @StartDate AND @EndDate) CrossCal
  INNER JOIN APC.Encounter E
	ON E.SourcePatientNo = WS.SourcePatientNo
	AND E.SourceSpellNo = WS.SourceSpellNo
	AND E.FirstEpisodeInSpellIndicator = 1
  INNER JOIN WH.Directorate DIR 
    ON DIR.DirectorateCode = E.StartDirectorateCode
  INNER JOIN APC.Ward W
	ON WS.WardID = W.SourceWardID  
 -- INNER JOIN BedMan.dbo.HospSpell HS
	--ON HS.Patient = E.SourcePatientNo AND HS.AdmitDate = E.AdmissionTime 
  LEFT JOIN 	
  (SELECT distinct WardEncounterRecno, TheMonth , SourceWardCode, Division, PatientCategory FROM Legacy.HarmCare
   WHERE (Fall = 1 OR PUHFC2 = 1 OR UTI = 1 OR VTETreatment = 1)) HARM
	ON --E.SourcePatientNo = HARM.Patient AND 
	harm.TheMonth = CrossCal.TheMonth AND WS.MergeEncounterRecno = HARM.WardEncounterRecno AND W.SourceWardCode = HARM.SourceWardCode
  WHERE --E.FirstEpisodeInSpellIndicator = 1 
  
  WS.StartDate <= (SELECT MAX(TheDate) FROM WH.Calendar WHERE FinancialMonthKey = CrossCal.FinancialMonthKey) 
  AND ISNULL(WS.EndDate,GETDATE()) >= (SELECT MIN(TheDate) FROM WH.Calendar WHERE FinancialMonthKey = CrossCal.FinancialMonthKey)
  
  GROUP BY CrossCal.TheMonth, CrossCal.FinancialQuarter,DIR.Division, 
  COALESCE(HARM.SourceWardCode, W.SourceWardCode), E.PatientCategoryCode, CrossCal.FinancialMonthKey

END

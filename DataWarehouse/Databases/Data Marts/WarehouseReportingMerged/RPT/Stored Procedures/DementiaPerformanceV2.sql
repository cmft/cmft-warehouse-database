﻿
--use WarehouseReportingMerged


Create Procedure [RPT].[DementiaPerformanceV2] @PeriodStartDate datetime, @PeriodEndDate datetime

as

Declare @LocalStart datetime = @PeriodStartDate
		,@LocalEnd datetime = (@PeriodEndDate + '23:59')

select 
	Encounter.GlobalProviderSpellNo
	,PatientSurname
	,SexCode
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,AdmissionDate
	,AdmissionTime
	,DischargeDate
	,DischargeTime
	,StartSiteCode
	,StartWardTypeCode
	,SpecialtyCode
	,ConsultantCode
	,PrimaryDiagnosisCode
	,AgeOnAdmission = (datediff(day,DateOfBirth,AdmissionDate)/365.25)
	,LoS = datediff(day,AdmissionDate,coalesce(DischargeDate,getdate()))
	,KnownDementiaRecordedDate	
	,KnownDementiaResponse	
	,ScreeningQuestionFirstResponseTime	
	,ScreeningQuestionFirstResponse	
	,ScreeningQuestionLatestPositiveResponseTime	
	,AssessmentDate	
	,AssessmentTime	
	,AssessmentScore	
	,InvestigationDate	
	,AssessmentNotPerformedReason	
	,ReferralSentDate
	,Q1Denominator =
		case 
			when dateadd(mi,4320,AdmissionTime) between @LocalStart and @LocalEnd
			then 1
			else null
		end
	,Q1Numerator =
		case 
			when dateadd(mi,4320,AdmissionTime) between @LocalStart and @LocalEnd--'20131001' and '20131231 23:59' 
				and
					(KnownDementiaResponse = 1 
					or datediff(mi,AdmissionTime,ScreeningQuestionFirstResponseTime)<=4320
					) 
			then 1
			when dateadd(mi,4320,AdmissionTime) between @LocalStart and @LocalEnd then 0
			else null
		end
	,Q2Denominator =
		case
			when ScreeningQuestionLatestPositiveResponseTime is not null
			and 
				(
				AssessmentDate between @LocalStart and @LocalEnd
				or
					(
					AssessmentDate is null
					and 
					DischargeDate between @LocalStart and @LocalEnd
					)
				)
			and 
				(
				AssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')

				or
				AssessmentNotPerformedReason is null
				)
			then 1
			else 0
		end
	,Q2Numerator =
		case
			when ScreeningQuestionLatestPositiveResponseTime is not null
				and 
					(
					AssessmentDate between @LocalStart and @LocalEnd
					or
						(
						AssessmentDate is null
						and 
						DischargeDate between @LocalStart and @LocalEnd
						)
					)
				and 
					(
					AssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
					or
					AssessmentNotPerformedReason is null
					)
				and 
					(
						(
						AssessmentDate is not null 
						and 
						AssessmentScore>=8
						)
					or
						(
						AssessmentDate is not null 
						and AssessmentScore <=7
						and InvestigationDate is not null
						) 
					)
			then 1
			else 0
		end
	,Q3Denominator =
		case 
			when ScreeningQuestionLatestPositiveResponseTime is not null
				and 
					(
					AssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
					or
					AssessmentNotPerformedReason is null
					)
				and AssessmentScore between 0 and 7 
				and 
					(ReferralSentDate between @LocalStart and @LocalEnd
					or
						(ReferralSentDate is null 
						and DischargeDate between @LocalStart and @LocalEnd
						)
					)
			then 1
			else 0
		end
	,Q3Numerator =
		case 
			when ScreeningQuestionLatestPositiveResponseTime is not null
				and 
					(
					AssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
					or
					AssessmentNotPerformedReason is null
					)
				and AssessmentScore between 0 and 7
				and ReferralSentDate between @LocalStart and @LocalEnd
			then 1
			else 0
		end

from 
	APC.Encounter Encounter

inner join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   
				
left join APC.DischargeMethod
on Encounter.DischargeMethodID = DischargeMethod.SourceDischargeMethodID

inner join APC.PatientClassification
on Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID 

left join APC.BaseDementia Dementia
on Encounter.ProviderSpellNo = Dementia.ProviderSpellNo
where 
	(dateadd(mi,4320,AdmissionTime) between @LocalStart and @LocalEnd
	or
	ScreeningQuestionLatestPositiveResponseTime between @LocalStart and @LocalEnd
	or 
	ReferralSentDate between @LocalStart and @LocalEnd
	or 
	DischargeDate between @LocalStart and @LocalEnd
	)
and cast((datediff(day,DateOfBirth,AdmissionDate)/365.25) as int)>=75 -- Patients 75yrs and over on Admission
and	datediff(mi,AdmissionTime,coalesce(DischargeTime,getdate())) >4320 -- LoS >72hrs
and FirstEpisodeInSpellIndicator = 1 -- Spell level count, not episode
and dateofdeath is null -- not sure if this should be included why should we exclude deceased patients.  
	-- 20141002 e-mailed NJ for advice.  I've suggested, if we exclude deceased there should be some criteria, ie died within eg 1wk of admission
and	AdmissionMethod.AdmissionType = 'Emergency' -- Non Elective admissions only
and NationalAdmissionMethodCode <> '81' -- Exclude Transfers
and PatientClassification.NationalPatientClassificationCode not in ('3','4') -- Exclude Regular admissions 

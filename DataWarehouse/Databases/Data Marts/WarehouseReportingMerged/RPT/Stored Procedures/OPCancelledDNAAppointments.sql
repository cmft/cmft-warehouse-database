﻿


	--Cancelled report

	CREATE proc [RPT].[OPCancelledDNAAppointments]

	

	 @CancelledDate datetime 
	,@Type varchar (10) = null
--	,@Clinic as varchar(max) = null
	,@Division as varchar(max)
	,@AppType as varchar (1) = 'N'
	

	 as

	--declare @CancelledDate datetime = '02 apr 2013'
	--declare @Type varchar (10) = 'D'
	--declare @Division as varchar(max) = 'Childrens'
	--declare @AppType as varchar (1) = 'N'
			

			select 
				 Division
				,ConsultantTitle = Consultant.Title
				,ConsultantInitials = Consultant.Initials
				,ConsultantSurname = Consultant.Surname
				,Clinic.SourceClinicCode
				,Clinic.SourceClinic


				,DateCanceled = Encounter.AppointmentCancelDate
				,Encounter.CancelledByCode
				,AppointmentDNACanceledDate = Encounter.AppointmentTime
				,Encounter.AppointmentOutcomeCode
				,Encounter.AppointmentTime

				,CountOfDNAs = 

				(
				select 
					 count (*)
				from 
				OP.Encounter EncounterCount
				
				left join WH.Sex Sex
				on Sex.SourceSexID = Encounter.SexID
			
				where 
				EncounterCount.SourcePatientNo = Encounter.SourcePatientNo
				and
				EncounterCount.SourceEncounterNo = Encounter.SourceEncounterNo
				and
				EncounterCount.EncounterRecno <> encounter.EncounterRecno
				and
				AppointmentOutcomeCode in ('DNA')

				group by AppointmentOutcomeCode

				)


				,CountOfHospitalCancels = null --coalesce(CountOfHospitalCancels,0)
				,CountOfPatientCancels = null --coalesce(CountOfPatientCancels,0)



				,Encounter.DistrictNo 
				,Encounter.NHSNumber
				,Encounter.CasenoteNo
				,Encounter.PatientSurname
				,Encounter.PatientForename
				,PatientSex = Sex.NationalSex
				,Encounter.PatientAddress1
				,Encounter.PatientAddress2
				,Encounter.PatientAddress3
				,Encounter.PatientAddress4
				,Encounter.Postcode

				,Encounter.DateOfBirth
				,Encounter.AgeCode
				,Encounter.RTTPathwayID

				,AppointmentType.SourceAppointmentType
				--,Encounter.RTTActivity
				--,Encounter.RTTStartDate
				--,Encounter.RTTEndDate
				--,Encounter.RTTBreachDate

				  ,SourceOfReferral.LocalSourceOfReferral

				  ,Encounter.ReferralConsultantID
				  ,ReferringGpCode = Encounter.ReferredByGpCode 
				  ,Encounter.RegisteredGpCode
				  
				  ,GPName = GP.Organisation

				  ,Encounter.RegisteredGpPracticeCode

				  ,PracticeAddress1 = Practice.Address1
				  ,PracticeAddress2 = Practice.Address2
				  ,PracticeAddress3 = Practice.Address3
				  ,PracticeAddress4 = Practice.Address4
				  ,PracticePostCode = Practice.Postcode

				  ,Encounter.RegisteredGpAtAppointmentCode
				  ,Encounter.RegisteredGpPracticeAtAppointmentCode


				,AMPM = 
				
						Case 
							When CONVERT(TIME,Encounter.AppointmentTime) >= '13:00' then 'PM'
							When CONVERT(TIME,Encounter.AppointmentTime) < '13:00' then 'AM'
						else
						null
						end
	
				,Encounter.SourcePatientNo
				,Encounter.SourceEncounterNo

			 from OP.Encounter Encounter

			 left join Organisation.dbo.Practice Practice
			 on Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode

			 left join Organisation.dbo.Gp GP
			 on GP.OrganisationCode = Encounter.RegisteredGpCode

			 left join WH.Sex Sex
			 on Sex.SourceSexID = Encounter.SexID

			 left join OP.SourceOfReferral SourceOfReferral
			 on SourceOfReferral.SourceSourceOfReferralID = Encounter.SourceOfReferralID
			 and SourceOfReferral.SourceContextCode = Encounter.ContextCode

			 inner join wh.Calendar AppointmentDate
			 on AppointmentDate.DateID = Encounter.AppointmentDateID

			 left join OP.Clinic Clinic
			 on Clinic.SourceClinicID = encounter.ClinicID
			 and Clinic.SourceContext = Encounter.ContextCode

			 left join WH.Directorate Directorate
			 on Directorate.DirectorateCode = Encounter.DirectorateCode

			   INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@Division,',')) D
			   ON Directorate.Division = D.Value   


			 left join OP.AppointmentType AppointmentType
			 on AppointmentType.SourceAppointmentTypeID = Encounter.AppointmentTypeID

			 left join wh.Consultant Consultant
			 on Consultant.SourceConsultantID = Encounter.ConsultantID


			 left join 
				(

				select 
				
					
					 FutureEncounter.SourcePatientNo
					,FutureEncounter.AppointmentTime
					,FutureEncounter.SourceEncounterNo
					,FutureEncounter.EncounterRecno
					,AppointmentDate = FutureAppointmentDate.TheDate
					

					from 
			 OP.Encounter FutureEncounter

			 inner join wh.Calendar FutureAppointmentDate
			 on FutureAppointmentDate.DateID = FutureEncounter.AppointmentDateID

			 where FutureEncounter.CancelledByCode is null 
				and FutureEncounter.AppointmentCancelDate is null
			 ) FutureEncounter
			 on FutureEncounter.SourcePatientNo = Encounter.SourcePatientNo
			 and FutureEncounter.AppointmentDate > @CancelledDate
			 
			 and FutureEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
			 and FutureEncounter.EncounterRecno <> Encounter.EncounterRecno
			 

			 --left join 
				--(
				--select	
		
			 
				--	 SourcePatientNo
				--	,SourceEncounterNo
				--	,CountOfDNAs
				--	,CountOfHospitalCancels
				--	,CountOfPatientCancels
				--	,AppointmentDate

				--from Warehouse.OP.WaitingList WaitingList

				--where WaitingList.CensusDate = @CancelledDate-1 
				--and WaitingList.CancelledBy is null 

				
				--) CancelDetail
				--on CancelDetail.SourcePatientNo = Encounter.SourcePatientNo
				--and CancelDetail.SourceEncounterNo = Encounter.SourceEncounterNo
				--and CancelDetail.AppointmentDate = AppointmentDate.thedate
				

			where 


			(
				@Type = 'PC'
				and
				Encounter.AppointmentCancelDate = @CancelledDate
				and
				Encounter.AppointmentOutcomeCode = 'P'
				
				or

				@Type = 'HC'
				and
				Encounter.AppointmentCancelDate = @CancelledDate
				and
				Encounter.AppointmentOutcomeCode = 'H'


				or
					
				@Type = 'D'
				and
				Encounter.AppointmentOutcomeCode = 'DNA'
				and
				AppointmentDate.TheDate = @CancelledDate
				 
				

					
			)
			

			and
			@AppType = 'N'

			and left(AppointmentType.SourceAppointmentType,3) = 'New'




﻿
CREATE proc [RPT].[GPDischargeLetterCheck]

 @Startdate datetime = null
,@Enddate datetime
as
--get working table

 
select
	 Division
	,NationalSpecialty
	,NationalSpecialtyCode
	,PatientCategoryCode
	,PatientCategory
	,Nhsnumber
	,Providerspellno
	,Admissiondate
	,Dischargedate 
	,DischargeMonth 
	,[Report Period] = LEFT(CONVERT(varchar, DischargeMonth,112),6) 
	,DischargeMonthName 
	,DischargeTime
	,[GP Practice Code] = GPPracticeCode
	,[Practice Name] = GPPractice 
	,LetterCreated
	,LetterSent 
	,SignedLetter 
	,SignedWithin48
	,SignedTime
	,SentWithin48 
	,SentAfter48 
	,TransmissionTime 
	,[Case] 
	,[Activity Type] = ActivityType 
	,DocumentType
	,DocumentID
from

(	

select 

	 Directorate.Division
	,NationalSpecialty
	,NationalSpecialtyCode
	,PatientCategory.PatientCategoryCode
	,PatientCategory
	,Nhsnumber
	,Providerspellno
	,Admissiondate
	,Dischargedate = Dischargedate.TheDate
	,DischargeMonth = month(DischargeDate)
	,DischargeMonthName = cast(datename(month,Dischargedate.TheDate) + ' ' + datename(year,Dischargedate.TheDate) as varchar (50))
	,DischargeTime
	,GPPracticeCode = GPPractice.GPPracticeCode
	,GPPractice = GPPractice.GpPracticeName
	,LetterCreated = case when Document.MergeDocumentRecno is not null then 1 else 0 end
	,LetterSent = Case when Document.TransmissionTime is not null then 1 else 0 end
	,SignedLetter = case when Document.SignedTime is not null then 1 else 0 end
	,SignedWithin48 = case when datediff(hour, Encounter.DischargeTime, Document.SignedTime) <= 48 then 1 else 0 end
	,Document.SignedTime
	,SentWithin48 =
		case
		when datediff(day, Encounter.DischargeTime, Document.TransmissionTime) <= 2 then  1 else 0 end
	,SentAfter48 =
		case
		when datediff(Day, Encounter.DischargeTime, Document.TransmissionTime) > 2 then  1 else 0 end		
	,TransmissionTime = datediff(hour, Encounter.DischargeTime, Document.TransmissionTime)
	,TransmissionTimeDocument = Document.TransmissionTime
	,[Case] = 1
	,ActivityType = 'APC'
	,DocumentType = DocumentTypeCode
	,DocumentID

	
	,Exclude = 
		ROW_NUMBER ( ) 
			OVER (PARTITION BY Providerspellno order by TransmissionTime asc)

from 

--using view to exclude none reportable
WarehouseOLAPMergedV2.APC.Encounter Encounter

inner join WarehouseOLAPMergedV2.WH.Calendar Dischargedate
on Dischargedate.DateID = Encounter.DischargeDateID

inner join WarehouseOLAPMergedV2.WH.Specialty  Specialty
on Specialty.SourceSpecialtyID = Encounter.SpecialtyID

inner join WarehouseOLAPMergedV2.APC.PatientCategory PatientCategory
on PatientCategory.PatientCategoryCode = Encounter.PatientCategoryCode

inner join WarehouseOLAPMergedV2.WH.Directorate Directorate
on Directorate.DirectorateCode = Encounter.EndDirectorateCode

inner join WarehouseOLAPMergedV2.WH.GPPractice GPPractice
on	Encounter.GpPracticeCodeAtDischarge = GPPractice.GpPracticeCode
and 
(
		Encounter.EpisodeStartDate between GPPractice.ElectronicCorrespondanceActivatedTime and coalesce(GPPractice.ElectronicCorrespondanceDeactivatedTime, getdate())
	and Encounter.EpisodeStartDate between GPPractice.ElectronicCorrespondanceActivatedTime and coalesce(GPPractice.ElectronicCorrespondanceDeactivatedTime, getdate())
	and not exists
		(
		select
			1
		from
			WarehouseOLAPMergedV2.WH.GPPractice Later
		where 
			Later.GpPracticeCode = GPPractice.GpPracticeCode
		and Later.GpPracticeID > GPPractice.GpPracticeID
		and Encounter.EpisodeStartDate between Later.ElectronicCorrespondanceActivatedTime and coalesce(Later.ElectronicCorrespondanceDeactivatedTime, getdate())
		)

or
GPPractice.GpPracticeCode in

	(
	'P91615'
	,'P91619'
	,'P91629'
	,'P84639'
	)
)

inner join WarehouseOLAPMergedV2.WH.ccg CCG
on CCG.CCGCode = GPPractice.ParentOrganisationCode	
and CCG.CCG = 'NHS CENTRAL MANCHESTER CCG'

inner join WarehouseOLAPMergedV2.Allocation.DatasetAllocation DischargeSummaryExclusion
on	DischargeSummaryExclusion.DatasetRecno= Encounter.MergeEncounterRecno
and DischargeSummaryExclusion.AllocationTypeID = 14
and	DischargeSummaryExclusion.DatasetCode = 'APC'
	

inner join WarehouseOLAPMergedV2.allocation.allocation  Allocation
on allocation.AllocationID = DischargeSummaryExclusion.AllocationID
and allocation.AllocationTypeID = 14

left join WarehouseOLAPMergedV2.APC.BaseEncounterBaseDocument EncounterDocument
on Encounter.MergeEncounterRecno = EncounterDocument.MergeEncounterRecno

left join WarehouseOLAPMergedV2.Dictation.BaseDocument Document
on EncounterDocument.MergeDocumentRecno = Document.MergeDocumentRecno

left join WarehouseOLAPMergedV2.Dictation.BaseDocumentReference DocumentReference
on DocumentReference.MergeDocumentRecno = Document.MergeDocumentRecno



where 
--dateofdeath is null 
Dischargedate.TheDate between @Startdate and @Enddate
and
NationalLastEpisodeInSpellIndicator = '1'
and
Allocation.AllocationID = 2088811 --remove exlcuded discharge
--and
--Encounter.Contextcode = 'CEN||PAS'


) Encounter



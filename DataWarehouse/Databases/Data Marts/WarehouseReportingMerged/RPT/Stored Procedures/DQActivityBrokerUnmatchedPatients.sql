﻿
/******************************************************************************
**  Name: RPT.DQActivityBrokerUnmatchedPatients
**  Purpose: Data Quality report for un-matched ActivityBroker patients
**
**	Input Parameters
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 14.11.13      MH       Created
******************************************************************************/

CREATE PROCEDURE RPT.DQActivityBrokerUnmatchedPatients
AS
	DECLARE @SnapshotTime			DATETIME
	DECLARE @LatestAPCWLCensusDate	DATETIME
	DECLARE @LatestOPWLCensusDate	DATETIME
	DECLARE @NullDate				DATE = '1 Jan 1900'

-- Get the time the data was built	
	SELECT TOP 1
		@SnapshotTime = EventTime
	FROM
		ActivityBroker.Utility.AuditLog
	ORDER BY EventTime DESC
		
-- Get latest APC and OP WL census dates
	SELECT TOP 1
		@LatestAPCWLCensusDate = CensusDate
	FROM
		APCWL.Encounter
	ORDER BY CensusDate DESC
		
	SELECT TOP 1
		@LatestOPWLCensusDate =CensusDate
	FROM
		OPWL.Encounter
	ORDER BY CensusDate DESC

-- Build a temp table mapping an AB.APCWait row to an APCWL.Encounter row	
	SELECT				-- Central Rows
		 W.APCWaitId
		,E.EncounterRecno
	INTO #TAPCWL
	FROM
		ActivityBroker.AB.APCWait W
	INNER JOIN APCWL.Encounter E
		ON		CAST(W.SourcePatientNo AS VARCHAR(20))	= E.SourcePatientNo
			AND W.SourceEntityRecno						= E.SourceEncounterNo
			AND W.ContextCode							= E.ContextCode
			AND E.CensusDate							= @LatestAPCWLCensusDate
	WHERE
		W.EPRApplicationContextID = 1	
			
	UNION
	
	SELECT				-- Trafford Rows
		 W.APCWaitId
		,E.EncounterRecno
	FROM
		ActivityBroker.AB.APCWait W
		INNER JOIN APCWL.Encounter E
			ON		W.ContextCode		= E.ContextCode
				AND W.SourceUniqueID	= E.SourceUniqueID
				AND E.CensusDate		= @LatestAPCWLCensusDate
	WHERE
		W.EPRApplicationContextID = 2

-- Build a temp table mapping an AB.OPWait row to an OPWL.Encounter row	
	SELECT				-- Central Rows
		 W.OPWaitId
		,E.EncounterRecno
	INTO #TOPWL
	FROM
		ActivityBroker.AB.OPWait W
	INNER JOIN OPWL.Encounter E
		ON		CAST(W.SourcePatientNo AS VARCHAR(20))	= E.SourcePatientNo
			AND W.SourceEntityRecno						= E.SourceEncounterNo
			AND W.ContextCode							= E.ContextCode
			AND E.CensusDate							= @LatestOPWLCensusDate
	WHERE
		W.EPRApplicationContextID = 1	
			
	UNION
	
	SELECT				-- Trafford Rows
		 W.OPWaitId
		,E.EncounterRecno
	FROM
		ActivityBroker.AB.OPWait W
		INNER JOIN OPWL.Encounter E
			ON		W.ContextCode		= E.ContextCode
				AND W.SourceUniqueID	= E.SourceUniqueID
				AND E.CensusDate		= @LatestOPWLCensusDate
	WHERE
		W.EPRApplicationContextID = 2
		
-- Build a temp table mapping an AB.APCSpell row to an APC.Encounter row	
	SELECT				-- Central Rows
		 S.APCSpellId
		,E.EncounterRecno
	INTO #TAPC
	FROM
		ActivityBroker.AB.APCSpell S
	INNER JOIN APC.Encounter E
		ON		CAST(S.SourcePatientNo AS VARCHAR(20))	= E.SourcePatientNo
			AND S.SourceEntityRecno						= E.SourceEncounterNo
			AND S.ContextCode							= E.ContextCode
			AND E.FirstEpisodeInSpellIndicator			= 1
	WHERE
		S.EPRApplicationContextID = 1	
			
	UNION
	
	SELECT				-- Trafford Rows
		 S.APCSpellId
		,E.EncounterRecno
	FROM
		ActivityBroker.AB.APCSpell S
		INNER JOIN APC.Encounter E
			ON		S.ContextCode						= E.ContextCode
				AND S.SourceUniqueID					= E.SourceUniqueID
				AND E.FirstEpisodeInSpellIndicator		= 1
	WHERE
		S.EPRApplicationContextID = 2		
						
-- IP Waiting List entry present in the Origin but NOT present in the Destination
	SELECT 
		  Cases			= COUNT(*)
		 ,MeasureType		= 'Waiting List - No Match'
		 ,MeasureTypeCode	= 'WLNM'
		 ,SortOrder	= 10
		
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		INNER JOIN ActivityBroker.AB.Pathway P
			ON ABT.PathwayID			= P.PathwayID
		LEFT JOIN ActivityBroker.AB.APCWait ORIGAPCWAIT
			ON P.OriginAPCWaitID		= ORIGAPCWAIT.APCWaitID
		LEFT JOIN ActivityBroker.AB.APCWait DESTAPCWAIT
			ON P.DestinationAPCWaitID	= DESTAPCWAIT.APCWaitID

	WHERE
			P.PathwayTypeID			IN (1)
		AND	DESTAPCWAIT.APCWaitID	IS NULL	

	UNION
		
	-- IP Waiting List entry present in the Origin AND in the Destination, but differences in some column values
	SELECT 
		  Cases				= COUNT(*)
		 ,MeasureType		= 'Waiting List - Data Mismatch'
		 ,MeasureTypeCode	= 'WLDM'
		 ,SortOrder	= 11
		
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		INNER JOIN ActivityBroker.AB.Pathway P
			ON ABT.PathwayID = P.PathwayID
		LEFT JOIN ActivityBroker.AB.APCWait ORIGAPCWAIT
			ON P.OriginAPCWaitID		= ORIGAPCWAIT.APCWaitID
		LEFT JOIN ActivityBroker.AB.APCWait DESTAPCWAIT
			ON P.DestinationAPCWaitID	= DESTAPCWAIT.APCWaitID
		LEFT JOIN WarehouseOLAPMergedV2.WH.Member ORIGCONSULTANT
			ON		ORIGAPCWAIT.ContextCode			= ORIGCONSULTANT.SourceContextCode
				AND ORIGAPCWAIT.ConsultantCode		= ORIGCONSULTANT.SourceValueCode
				AND ORIGCONSULTANT.AttributeCode	= 'CONSUL'
		LEFT JOIN WarehouseOLAPMergedV2.WH.Member DESTCONSULTANT
			ON		DESTAPCWAIT.ContextCode			= DESTCONSULTANT.SourceContextCode
				AND DESTAPCWAIT.ConsultantCode		= DESTCONSULTANT.SourceValueCode
				AND DESTCONSULTANT.AttributeCode	= 'CONSUL'
			
	WHERE
			P.PathwayTypeID IN (1)
		AND
		(
			(
					DESTAPCWAIT.APCWaitID IS NOT NULL
				AND	
				(
						COALESCE(ORIGAPCWAIT.IntendedProcedureCode,'')	<> COALESCE(DESTAPCWAIT.IntendedProcedureCode,'')
					OR	COALESCE(ORIGAPCWAIT.IntendedManagementCode,'')	<> COALESCE(DESTAPCWAIT.IntendedManagementCode,'')
					OR	COALESCE(ORIGAPCWAIT.AdmissionMethodcode,'')	<> COALESCE(DESTAPCWAIT.AdmissionMethodcode,'')
					OR	COALESCE(ORIGAPCWAIT.TCIDate, @NullDate)		<> COALESCE(DESTAPCWAIT.TCIDate,@NullDate)
					OR	CASE 
							WHEN ORIGCONSULTANT.NationalValue = 'UnAssigned' 
								THEN ORIGCONSULTANT.SourceValueCode
								ELSE ORIGCONSULTANT.NationalValue
						END
						<>
						CASE 
							WHEN DESTCONSULTANT.NationalValue = 'UnAssigned' 
								THEN DESTCONSULTANT.SourceValueCode
								ELSE DESTCONSULTANT.NationalValue
						END
				)
			)
		)
	
	UNION

	-- IP admission entry present in the Origin but NOT present in the Destination
	SELECT 
		  Cases			= COUNT(*)
		 ,MeasureType		= 'Inpatient Transfer - No Match'
		 ,MeasureTypeCode	= 'IPTNM'
		 ,SortOrder	= 20
		
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		INNER JOIN ActivityBroker.AB.Pathway P
			ON ABT.PathwayID = P.PathwayID
		LEFT JOIN ActivityBroker.AB.APCSpellTransferBase ORIGSPELL
			ON P.OriginAPCSpellID		= ORIGSPELL.APCSpellID
		LEFT JOIN ActivityBroker.AB.APCSpellTransferBase DESTSPELL
			ON P.DestinationAPCSpellID	= DESTSPELL.APCSpellID

	WHERE
			P.PathwayTypeID = 3
		AND	DESTSPELL.APCSpellID IS NULL
		
	UNION

	-- IP admission entry present in the Origin AND in the Destination, but differences in some column values
	SELECT 
		  Cases			= COUNT(*)
		 ,MeasureType		= 'Inpatient Transfer - Data Mismatch'
		 ,MeasureTypeCode	= 'IPTDM'
		 ,SortOrder	= 21
		
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		INNER JOIN ActivityBroker.AB.Pathway P
			ON ABT.PathwayID = P.PathwayID
		INNER JOIN ActivityBroker.AB.APCSpellTransferBase ORIGSPELL
			ON P.OriginAPCSpellID		= ORIGSPELL.APCSpellID
		LEFT JOIN ActivityBroker.AB.APCSpellTransferBase DESTSPELL
			ON P.DestinationAPCSpellID	= DESTSPELL.APCSpellID

	WHERE
			P.PathwayTypeID = 3
		AND	DESTSPELL.APCSpellID IS NOT NULL
		AND
		(
			DATEDIFF(HOUR, ORIGSPELL.DischargeTime, DESTSPELL.AdmissionTime) > 2
		)
		
	UNION

-- Un-actioned Activity broker transactions for more than 48 hours

	SELECT
		  Cases			= COUNT(*)
		 ,MeasureType		= '48 Hour Unactioned Broker Activity'
		 ,MeasureTypeCode	= 'OTA'
		 ,SortOrder	= 30
		 
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		
	WHERE
			ActivityType <> 'NoAction'	
		AND DATEDIFF(HOUR, ABT.CreatedTimestamp,@SnapshotTime) > 48
		
	UNION

-- Activity broker transaction with data mis-match on patient GP

	SELECT
		  Cases				= SUM(D.Cases)
		 ,MeasureType		= 'Mismatch on Registered GP Practice'
		 ,MeasureTypeCode	= 'GPDM'
		 ,SortOrder	= 40
		 
	FROM	
	(
		-- APC Waiting List actions
		SELECT
			 Cases  = COUNT(*)
			,Type	= 'APCWL'
			
		FROM
			ActivityBroker.dbo.ActivityBrokerTracker ABT
			INNER JOIN ActivityBroker.AB.Pathway P
				ON		ABT.PathwayID					= P.PathwayID
			--INNER JOIN ActivityBroker.AB.APCWait ORIGAPCWAIT
			--	ON		P.OriginAPCWaitID				= ORIGAPCWAIT.APCWaitID
			INNER JOIN #TAPCWL ORIGTWL
				ON		P.OriginAPCWaitID = ORIGTWL.APCWaitId
			INNER JOIN APCWL.Encounter ORIGAPCWL
				ON		ORIGTWL.EncounterRecNo			= ORIGAPCWL.EncounterRecno
			--INNER JOIN ActivityBroker.AB.APCWait DESTAPCWAIT
			--	ON		P.DestinationAPCWaitID			= DESTAPCWAIT.APCWaitID
			INNER JOIN #TAPCWL DESTTWL
				ON		P.DestinationAPCWaitID = DESTTWL.APCWaitId
			INNER JOIN APCWL.Encounter DESTAPCWL
				ON		DESTTWL.EncounterRecNo			= DESTAPCWL.EncounterRecno
			
		WHERE
				P.PatientIsMatched	= 1
			AND P.PathwayTypeID		= 1
			AND ORIGAPCWL.RegisteredGpPracticeCode <> DESTAPCWL.RegisteredGpPracticeCode

		UNION ALL

	-- OP Waiting List actions
		SELECT
			 Cases  = COUNT(*)
			,Type	= 'APCWL'
			
		FROM
			ActivityBroker.dbo.ActivityBrokerTracker ABT
			INNER JOIN ActivityBroker.AB.Pathway P
				ON		ABT.PathwayID					= P.PathwayID
			--INNER JOIN ActivityBroker.AB.OPWait ORIGOPWAIT
			--	ON		P.OriginOPWaitID				= ORIGOPWAIT.OPWaitID
			INNER JOIN #TOPWL ORIGTWL
				ON		P.OriginOPWaitID = ORIGTWL.OPWaitId
			INNER JOIN OPWL.Encounter ORIGOPWL
				ON		ORIGTWL.EncounterRecNo			= ORIGOPWL.EncounterRecno
			--INNER JOIN ActivityBroker.AB.OPWait DESTOPWAIT
			--	ON		P.DestinationOPWaitID			= DESTOPWAIT.OPWaitID
			INNER JOIN #TOPWL DESTTOPWL
				ON		P.DestinationOPWaitID = DESTTOPWL.OPWaitId
			INNER JOIN OPWL.Encounter DESTOPWL
				ON		DESTTOPWL.EncounterRecNo		= DESTOPWL.EncounterRecno
			
		WHERE
				P.PatientIsMatched	= 1
			AND P.PathwayTypeID		= 2
			AND ORIGOPWL.RegisteredGpPracticeCode <> DESTOPWL.RegisteredGpPracticeCode
		
		UNION ALL

	-- Inpatient spell transfer actions
		SELECT
			 Cases  = COUNT(*)
			,Type	= 'APCSPELLXFER'
			
		FROM
			ActivityBroker.dbo.ActivityBrokerTracker ABT
			INNER JOIN ActivityBroker.AB.Pathway P
				ON		ABT.PathwayID					= P.PathwayID
			--INNER JOIN ActivityBroker.AB.APCSpell ORIGAPCSPELL
			--	ON		P.OriginAPCSpellID				= ORIGAPCSPELL.APCSpellID
			INNER JOIN #TAPC ORIGTAPC
				ON		P.OriginAPCSpellID = ORIGTAPC.APCSpellID
			INNER JOIN APC.Encounter ORIGAPC
				ON		ORIGTAPC.EncounterRecNo			= ORIGAPC.EncounterRecno
			--INNER JOIN ActivityBroker.AB.APCSpell DESTAPCSPELL
			--	ON		P.DestinationAPCSpellID			= DESTAPCSPELL.APCSpellID
			INNER JOIN #TAPC DESTTAPC
				ON		P.DestinationAPCSpellID = DESTTAPC.APCSpellID
			INNER JOIN APC.Encounter DESTAPC
				ON		DESTTAPC.EncounterRecNo		= DESTAPC.EncounterRecno
			
		WHERE
				P.PatientIsMatched	= 1
			AND P.PathwayTypeID		= 3
			AND ORIGAPC.RegisteredGpPracticeCode <> DESTAPC.RegisteredGpPracticeCode
	) D

﻿

CREATE PROCEDURE [RPT].[OPFutureBookedAppointmentsDrillThrough]
(
	 @Division				VARCHAR(50)		= NULL
	,@DirectorateCode		VARCHAR(20)		= NULL
	,@NationalSpecialtyCode	VARCHAR(1000)	= NULL
	,@PreOp					CHAR(1)			= NULL
	,@DrillType				VARCHAR(5)
	,@AttendanceTypeCode	CHAR(1)			= NULL
	,@ApptWeekEnd			DATE			= NULL
	,@NoticeGivenWeeks		VARCHAR(5)		= NULL
)
AS

	DECLARE @Drill INT
	DECLARE @Drill2 int
	SET @Drill = case 
				WHEN @DrillType = 'WKD' THEN 1 
				
				END

					
	SET @Drill2 = case 
				 
				WHEN @DrillType = 'NGN'	THEN 1
				END


	SELECT	--Drill through
		 ApptWeekEnding					= DATA.ApptWeekEnding
		,AttendanceTypeDesc				= CASE	DATA.NationalFirstAttendanceCode
											WHEN '1' THEN 'New'
											ELSE 'Follow-Up'
										  END
		,AttendanceTypeCode				= DATA.NationalFirstAttendanceCode	
		,Directorate					= DIR.Directorate
		,Division						= DIR.Division	
		,Specialty						= SPEC.NationalSpecialtyLabel
		,IsPreOpAttendance				= DATA.IsPreOpAttendance
		,ConsultantSurname				= CONS.Surname
		,Clinic							= DATA.ClinicCode
		,CasenoteNo						= DATA.CasenoteNo
		,AppointmentStatusCode			= DATA.AppointmentStatusCode
		,AppointmentCreateDate			= DATA.AppointmentCreateDate
		,AppointmentDate				= DATA.AppointmentDate
		,NoticeGiven					= DATA.NoticePeriod
		
	FROM
		(
			SELECT
				 ApptWeekEnding			= APPTCAL.LastDayOfWeek
				,ApptWeekStart			= APPTCAL.FirstDayOfWeek
				,NoticePeriod			= DATEDIFF(DAY, E.AppointmentCreateDate, APPTCAL.TheDate) / 7
				,E.DirectorateCode
				,E.ReferralSpecialtyId
				,E.ContextCode
				,FA.NationalFirstAttendanceCode
		,IsPreOpAttendance = 
		
		CASE 
		WHEN E.ClinicCode = 'CPREADCL' THEN 'Y' 
		when E.ContextCode = 'TRA||UG' and exists

			(
			select 
				1
			FROM TraffordReferenceData.dbo.AppointmentType AppointmentType

			where
			left(apptype_relid,3) = 'pre'
			and cast(AppointmentType.apptype_identity as varchar) = E.AppointmentTypeCode

			) then 'Y'
			
		ELSE 'N' END
				,E.ConsultantID
				,E.ClinicCode
				,CasenoteNo = CASE WHEN E.ContextCode = 'TRA||UG' THEN E.DistrictNo ELSE E.CasenoteNo  end
				,E.AppointmentStatusCode
				,E.AppointmentCreateDate
				,E.AppointmentDate
				
			FROM
				OP.Encounter E
				INNER JOIN WH.Calendar APPTCAL
					ON		E.AppointmentDateID			 = APPTCAL.DateID
				INNER JOIN OP.FirstAttendance FA
					ON		E.DerivedFirstAttendanceId	 = FA.SourceFirstAttendanceID
						AND E.ContextCode				 = FA.SourceContextCode
				
			WHERE
					E.AppointmentCancelDate is null		
				AND APPTCAL.LastDayOfWeek			= @ApptWeekEnd
				AND FA.NationalFirstAttendanceCode	= @AttendanceTypeCode
				and E.ContextCode <> 'TRA||UG'

				OR 
				APPTCAL.LastDayOfWeek			= @ApptWeekEnd
				AND FA.NationalFirstAttendanceCode	= @AttendanceTypeCode
				and E.ContextCode = 'TRA||UG'
				AND E.AppointmentStatusCode = ('A')

				----and	E.AppointmentCreateDate	IS NULL
				--AND APPTCAL.LastDayOfWeek			= @ApptWeekEnd
				--AND FA.NationalFirstAttendanceCode	= @AttendanceTypeCode
		) DATA
		
		INNER JOIN WH.Directorate DIR
			ON		DATA.DirectorateCode			= DIR.DirectorateCode
			
		INNER JOIN WH.Specialty SPEC
			ON		DATA.ReferralSpecialtyId		= SPEC.SourceSpecialtyId
				AND DATA.ContextCode				= SPEC.SourceContextCode

		INNER JOIN WH.Consultant CONS
			ON		DATA.ConsultantId				= CONS.SourceConsultantID
				AND DATA.ContextCode				= CONS.SourceContextCode
	
	WHERE
					(SPEC.NationalSpecialtyCode	IN (SELECT Item from fn_ListToTable(@NationalSpecialtyCode,',')) or @NationalSpecialtyCode = 'All')
		AND DIR.Division				= COALESCE(@Division, DIR.Division)
		AND DIR.DirectorateCode			= COALESCE(@DirectorateCode, DIR.DirectorateCode)
		AND DATA.IsPreOpAttendance		= COALESCE(@PreOp,DATA.IsPreOpAttendance)
		AND
			( 
				--(
				--		@Drill2				=  1 --'NGN'			-- Notice Given columns Drill Type
				--	AND CASE WHEN DATA.NoticePeriod	<= 8 THEN CAST(DATA.NoticePeriod AS VARCHAR(5)) ELSE '8+' END	= @NoticeGivenWeeks
				--)
				--OR
				(
						@Drill				= 1		-- Weekend Display columns Drill Type
				)
			)	


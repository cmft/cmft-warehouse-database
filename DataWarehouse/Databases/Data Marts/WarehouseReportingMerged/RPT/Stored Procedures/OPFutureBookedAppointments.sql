﻿


CREATE PROCEDURE [RPT].[OPFutureBookedAppointments]
(
	 @StartBaseDate			DATE			= NULL
	,@Division				VARCHAR(50)		= NULL
	,@DirectorateCode		VARCHAR(20)		= NULL
	,@NationalSpecialtyCode	VARCHAR(1000)	= NULL
	,@PreOp					CHAR(1)			= NULL
	,@NoWeekEnds			INT				= 4		-- Determines the end date of the query and represents the number of 
													-- new and follow-up appts booked by weekending based on the current date
)
AS

	DECLARE @StartDate		DATE
	DECLARE @EndDate		DATE
	
	SET @StartBaseDate = COALESCE(@StartBaseDate, GETDATE())
	
	SET DATEFIRST  1	-- Monday

	SET @StartDate  = DATEADD(DAY, 1 - DATEPART(DW, @StartBaseDate), @StartBaseDate)
	SET @EndDate	= DATEADD(DAY, -1, DATEADD(WEEK, @NoWeekEnds, @StartDate))		-- End Date is a Sunday

	--Summary
	SELECT
		 ApptWeekEnding			= APPTCAL.LastDayOfWeek
		,NoticePeriod			= CASE 
									WHEN FA.NationalFirstAttendanceCode = '1' AND APPTCAL.FirstDayOfWeek = @StartDate	-- New attendance and in the first week of the date range
										THEN DATEDIFF(DAY, E.AppointmentCreateDate, APPTCAL.TheDate) / 7
									ELSE NULL
								  END
		,E.DirectorateCode
		,E.ReferralSpecialtyId
		,E.ContextCode
		,FA.NationalFirstAttendanceCode
		,IsPreOpAttendance = 
		
		CASE 
		WHEN E.ClinicCode = 'CPREADCL' THEN 'Y' 
		when E.ContextCode = 'TRA||UG' and exists

			(
			select 
				1
			FROM TraffordReferenceData.dbo.AppointmentType AppointmentType

			where
			left(apptype_relid,3) = 'pre'
			and cast(AppointmentType.apptype_identity as varchar) = E.AppointmentTypeCode

			) then 'Y'
			
		ELSE 'N' END
	
	INTO #DATA
		
	FROM
		OP.Encounter E
		INNER JOIN WH.Calendar APPTCAL
			ON		E.AppointmentDateID			 = APPTCAL.DateID
		INNER JOIN OP.FirstAttendance FA
			ON		E.DerivedFirstAttendanceId	 = FA.SourceFirstAttendanceID
				AND E.ContextCode				 = FA.SourceContextCode
		
	WHERE
			--E.AppointmentCreateDate IS NOT NULL
		 E.AppointmentCancelDate is null
		AND APPTCAL.TheDate BETWEEN @StartDate AND @EndDate
		AND FA.NationalFirstAttendanceCode IN (1, 2)
		and ContextCode <> 'TRA||UG'
		
		or
		
			ContextCode = 'TRA||UG'
			AND APPTCAL.TheDate BETWEEN @StartDate AND @EndDate

			AND AppointmentStatusCode = 'A'

			AND FA.NationalFirstAttendanceCode IN (1, 2)		

	SELECT
		 ApptWeekEnding					= DATA.ApptWeekEnding
		,AttendanceTypeDesc				= CASE	DATA.NationalFirstAttendanceCode
											WHEN '1' THEN 'New'
											ELSE 'Follow-Up'
										  END
		,AttendanceTypeCode				= DATA.NationalFirstAttendanceCode	
		,Directorate					= DIR.Directorate
		,DirectorateCode				= DIR.DirectorateCode
		,Division						= DIR.Division	
		,Specialty						= SPEC.NationalSpecialtyLabel
		,NationalSpecialtyCode			= SPEC.NationalSpecialtyCode
		,IsPreOpAttendance				= DATA.IsPreOpAttendance
		,Cases							= 1 
		,NoticePeriod0					= CASE WHEN NoticePeriod = 0 THEN 1 ELSE 0 END
		,NoticePeriod1					= CASE WHEN NoticePeriod = 1 THEN 1 ELSE 0 END
		,NoticePeriod2					= CASE WHEN NoticePeriod = 2 THEN 1 ELSE 0 END
		,NoticePeriod3					= CASE WHEN NoticePeriod = 3 THEN 1 ELSE 0 END
		,NoticePeriod4					= CASE WHEN NoticePeriod = 4 THEN 1 ELSE 0 END
		,NoticePeriod5					= CASE WHEN NoticePeriod = 5 THEN 1 ELSE 0 END
		,NoticePeriod6					= CASE WHEN NoticePeriod = 6 THEN 1 ELSE 0 END
		,NoticePeriod7					= CASE WHEN NoticePeriod = 7 THEN 1 ELSE 0 END
		,NoticePeriod8					= CASE WHEN NoticePeriod = 8 THEN 1 ELSE 0 END
		,NoticePeriod8Plus				= CASE WHEN NoticePeriod > 8 THEN 1 ELSE 0 END
		
	FROM
		#DATA DATA
	
		INNER JOIN WH.Directorate DIR
			ON		DATA.DirectorateCode			= DIR.DirectorateCode
			
		INNER JOIN WH.Specialty SPEC
			ON		DATA.ReferralSpecialtyId		= SPEC.SourceSpecialtyId
				AND DATA.ContextCode				= SPEC.SourceContextCode
	
	WHERE
			(SPEC.NationalSpecialtyCode	IN (SELECT Item from fn_ListToTable(@NationalSpecialtyCode,',')) or @NationalSpecialtyCode = 'All')
		AND DIR.Division				= COALESCE(@Division, DIR.Division)
		AND DIR.DirectorateCode			= COALESCE(@DirectorateCode, DIR.DirectorateCode)
		AND DATA.IsPreOpAttendance		= COALESCE(@PreOp,DATA.IsPreOpAttendance) 



﻿


-- [RPT].[GetMortalityPatients] @GlobalProviderSpellNumber='A178329'

CREATE proc [RPT].[GetMortalityPatients]
(
@user varchar(50) = null
,@dischargemonth varchar(20) = null
,@GlobalProviderSpellNumber varchar(50) = null		-- Changed variable name from @ProviderSpellNumber for clarity. Paul Egan 24/06/2014
--,@specialty varchar(50) = null					-- Commented Paul Egan 24/06/2014 - parameter not needed for drill-through
--,@division varchar(50) = null						-- Commented Paul Egan 24/06/2014 - parameter not needed for drill-through

)


as 

/****************************************************************************************
	Stored procedure : [RPT].[GetMortalityPatients]
	Description		 : SSRS proc for the Visual Studio report
	
	Reports >> Mortality >> Mortality Patient Detail.rdl
	
	Measures
	 Drill through list of deaths - episodic detail, for previous week.

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
	19/06/2014	Paul Egan       Copied from LIVE to DEV (did not exist on dev).
	24/06/2014	Paul Egan		Parameters not needed removed: Specialty, Division.
								Joined to APC.Diagnostic instead of using function for efficiency.
								Renamed @ProviderSpellNumber to Global for clarity.
*****************************************************************************************/


/* ================ DEBUG ONLY ================= */
--declare @user varchar(50) = null
--declare @dischargemonth varchar(20) = null
--declare @GlobalProviderSpellNumber varchar(50) = null
----declare @specialty varchar(50) = null		-- Commented Paul Egan 24/06/2014 - parameter not needed for drill-through
----declare @division varchar(50) = null		-- Commented Paul Egan 24/06/2014 - parameter not needed for drill-through
/* =============================================*/


set @dischargemonth = coalesce(@dischargemonth, '(Select All)')
set @user = coalesce(@user, 'cmmc\darren.griffiths')
set @GlobalProviderSpellNumber = coalesce(@GlobalProviderSpellNumber, '(Select All)')
--set @specialty = coalesce(@specialty, '(Select All)')		-- Commented Paul Egan 24/06/2014 - parameter not needed for drill-through
--set @division = coalesce(@division, '(Select All)')		-- Commented Paul Egan 24/06/2014 - parameter not needed for drill-through


select 
		--apc.MergeEncounterRecno		-- Validation only Paul Egan 24/06/2014
		SiteCode
		,Division
		,EndDirectorateCode
		,SpecialtyCode
		,Specialty	= NationalSpecialty
		,SourcePatientNo = 
						case
							when exists 
										(
										select
											1
										from 
											WarehouseReporting.RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then SourcePatientNo
							else 'Unauthorised'
						end
		,CasenoteNumber = 
						case
							when exists 
										(
										select
											1
										from 
											WarehouseReporting.RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then CasenoteNumber
							else 'Unauthorised'
						end
		,DistrictNo =
						case
							when exists 
										(
										select
											1
										from 
											WarehouseReporting.RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then DistrictNo
							else 'Unauthorised'
						end
		,DateOfBirth = 
						case
							when exists 
										(
										select
											1
										from 
											WarehouseReporting.RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then DateOfBirth
							else '01/01/1900'
						end
		,PatientSurname = 
						case
							when exists 
										(
										select
											1
										from 
											WarehouseReporting.RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then PatientSurname
							else 'Unauthorised'
						end
		,PatientForename = 
						case
							when exists 
										(
										select
											1
										from 
											WarehouseReporting.RPT.SecurityMap sm
										where [DomainLogin] = @user
										and EndDirectorateCode = sm.DirectorateCode
										)
							then PatientForename
							else 'Unauthorised'
						end
		,DateOfDeath		= COALESCE(DateOfDeath, DischargeDate)
		,ProviderSpellNo
		,SourceEncounterNo
		,AdmissionMethodCode
		,AdmissionDate
		,DischargeDate	
		,DischargeMethodCode
		,ConsultantLastEpisode
		,Consultant
		,EpisodeStartTime
		,EpisodeEndTime
		,StartWardTypeCode
		,EndWardTypeCode
		,CodingCompleteDate
		,PrimaryOperationCode	= PrimaryProcedureCode
		,PrimaryOperationDate	= PrimaryProcedureDate
		,DiagnosisType = DiagnosisCode
		,Diagnosis = Diagnosis

		from 
		(
		select
			episode.MergeEncounterRecno
			,spell.SiteCode
			,spell.EndDirectorateCode
			,dir.Division
			,episode.SpecialtyCode
			,Spell.SpecialtyID
			,specialtyepisode.NationalSpecialty
			,spell.SourcePatientNo  
			,spell.CasenoteNumber
			,spell.DistrictNo 
			,spell.DateOfBirth  
			,spell.PatientSurname  
			,spell.PatientForename  
			,spell.DateOfDeath
			,spell.ProviderSpellNo
			,episode.SourceEncounterNo
			,spell.AdmissionMethodCode
			,spell.AdmissionDate
			,spell.DischargeDate
			,spell.DischargeMethodCode
			,ConsultantLastEpisode = COALESCE(conslast.Title + ' ', '') + COALESCE(conslast.Initials + ' ', '') + COALESCE(conslast.Surname, conslast.NationalConsultant)
			,Consultant = COALESCE(cons.Title + ' ', '') + COALESCE(cons.Initials + ' ', '') + COALESCE(cons.Surname, cons.NationalConsultant)
			,episode.EpisodeStartTime
			,episode.EpisodeEndTime
			,episode.StartWardTypeCode
			,episode.EndWardTypeCode
			,episode.CodingCompleteDate
			,episode.PrimaryProcedureCode
			,episode.PrimaryProcedureDate
			--,episode.PrimaryDiagnosisCode				-- Commented Paul Egan 24/06/2014
			--,episode.SubsidiaryDiagnosisCode			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode1			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode2			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode3			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode4			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode5			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode6			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode7			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode8			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode9			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode10			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode11			-- Commented Paul Egan 24/06/2014
			--,episode.SecondaryDiagnosisCode12			-- Commented Paul Egan 24/06/2014
			,diag.DiagnosisCode		-- Added Paul Egan 24/06/2014
			,diagBase.Diagnosis		-- Added Paul Egan 24/06/2014
			from 
				  apc.Encounter spell
			inner join
					(
					select
						MergeEncounterRecno 
						,SourceEncounterNo
						,GlobalProviderSpellNo
						,ConsultantID
						,SpecialtyCode
						,SpecialtyID
						,EpisodeStartTime
						,EpisodeEndTime
						,StartWardTypeCode
						,EndWardTypeCode
						,CodingCompleteDate
						,PrimaryProcedureCode
						,PrimaryProcedureDate
						--,PrimaryDiagnosisCode				-- Commented Paul Egan 24/06/2014
						--,SubsidiaryDiagnosisCode			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode1			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode2			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode3			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode4			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode5			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode6			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode7			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode8			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode9			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode10			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode11			-- Commented Paul Egan 24/06/2014
						--,SecondaryDiagnosisCode12			-- Commented Paul Egan 24/06/2014
						,NationalLastEpisodeInSpellIndicator
					from apc.Encounter episode
					) episode 
					on spell.GlobalProviderSpellNo = episode.GlobalProviderSpellNo
					
			inner join APC.DischargeMethod DM
				ON		spell.DischargeMethodID = DM.SourceDischargeMethodID
					AND spell.ContextCode = DM.SourceContextCode
						
			left join WH.Specialty specialtyepisode
			on	specialtyepisode.SourceSpecialtyId = episode.SpecialtyID
			
			left join WH.Specialty specialtyspell
			on	specialtyspell.SourceSpecialtyId = spell.SpecialtyID
			
			left join WH.Directorate dir
			on	dir.DirectorateCode = spell.EndDirectorateCode
			
			left join WH.Consultant cons
			on	cons.SourceConsultantId = episode.ConsultantID
			
			left join WH.Consultant conslast
			on	conslast.SourceConsultantID = spell.ConsultantID
			
			left join APC.Diagnosis diag		-- Added Paul Egan 24/06/2014 - use this instead of function
			on	diag.MergeEncounterRecno = episode.MergeEncounterRecno
			
			left join WH.Diagnosis diagBase		-- Added Paul Egan 24/06/2014 - use this instead of function
			on	diagBase.DiagnosisCode = diag.DiagnosisCode

	
			where 
				--spell.DischargeMethodCode in ('DN', 'DP','ST', 'SP')
				DM.NationalDischargeMethodCode IN ('4','5')
				and spell.NationalLastEpisodeInSpellIndicator = 1
				--and spell.CodingCompleteDate is not null
				and (cast(datename(month, spell.DischargeDate) as char(3)) + ' ' + cast(year(spell.DischargeDate) as char(4)) = @dischargemonth or @dischargemonth = '(Select All)')
				--and (dir.Division = @division or @division = '(Select All)')								-- Commented out Paul Egan 24/06/2014 (param not needed for drill-through report)
				--and (specialtyspell.NationalSpecialtyCode = @specialty or @specialty = '(Select All)')	-- Commented out Paul Egan 24/06/2014 (param not needed for drill-through report)
				and (spell.GlobalProviderSpellNo = @GlobalProviderSpellNumber or @GlobalProviderSpellNumber = '(Select All)')
				
				/* TESTING ONLY Paul Egan 24/06/2014 */
				--and cast(spell.DischargeDate as date) between '20140601' and '20140608'
		) apc

		--OUTER APPLY dbo.fn_EpisodeDiagnosisCodesToTable(apc.MergeEncounterRecno) DIAG			-- Commented out Paul Egan 24/06/2014 (joined to Diag table instead for efficiency)



option (recompile)
 





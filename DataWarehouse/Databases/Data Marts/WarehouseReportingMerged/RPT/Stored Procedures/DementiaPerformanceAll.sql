﻿

CREATE Procedure [RPT].[DementiaPerformanceAll] 

	@PeriodStartDate datetime
	,@PeriodEndDate datetime
	
	,@CurrentInpatient int = null
	,@Division varchar(100) = 'All'
	,@LatestWard varchar(50) = 'All'
	,@AdmissionType varchar(50) = 'All'
	,@AgeCategory varchar(20) = null
	,@CQUIN bit = null
	
	,@KnownDementia bit = null
	,@Find bit = null
	,@Assessment bit = null
	,@AssessmentScore bit = null
	,@Investigation bit = null
	,@Referral bit = null
	
as	

declare @PeriodEnd datetime
set @PeriodEnd = (@PeriodEndDate + '23:59')

Select	
	ReportingMonth = CAST(@PeriodEnd as DATE)
	,GlobalProviderSpellNo
	,Division
	,AdmissionType
	,AdmissionTime
	,AdmissionWardCode
	,DischargeTime
	,LocalDischargeMethod
	,PatientSurname
	,SexCode
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard
	,LatestSpecialtyCode 
	,LatestConsultantCode
	,AgeOnAdmission 
	,AgeCategory
	,LoS 
	,KnownDementiaBedDays =
		Case
			when AdmissionTime < = @PeriodEnd 
				and coalesce(DischargeTime,getdate())>= @PeriodStartDate
				and LatestKnownToHaveDementiaResponse = 1
			then DATEDIFF(day,
				case
					when AdmissionTime < @PeriodStartDate 
					then @PeriodStartDate 
					else AdmissionDate
				end
				,
				case
					when DischargeTime > @PeriodEnd 
					then @PeriodEnd 
					else DischargeDate
				end
				)
			else null
		end
	,OriginalSubmissionTime
	,OriginalResponse
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,Assessment
	,LatestAssessmentTime	
	,AssessmentScore
	,LatestAssessmentScore	
	,Investigation
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,Referral
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	,Q1Numerator =
		case 
			when dateadd(mi,4320,AdmissionTime) between @PeriodStartDate and @PeriodEnd
			then
				case
					when 
						OriginalResponse = 'DementiaKnown'
					or
						datediff(mi,AdmissionTime,OriginalSubmissionTime)<=4320
					then 1
					else 0
				end
			else null
		end
			
	,Q2Denominator =
		case
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and 
					(
						LatestAssessmentTime between @PeriodStartDate and @PeriodEnd
					or
						(
							LatestAssessmentTime is null
						and 
							DischargeDate between @PeriodStartDate and @PeriodEnd
						and
							(
								LatestAssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
							or
								LatestAssessmentNotPerformedReason is null
							)
						)
					)
			then 1
			else 0
		end
	,Q2Numerator =
		case
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and 
					(
					LatestAssessmentTime between @PeriodStartDate and @PeriodEnd
					or
						(
							LatestAssessmentTime is null
						and 
							DischargeDate between @PeriodStartDate and @PeriodEnd
						and
							(
								LatestAssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
							or
								LatestAssessmentNotPerformedReason is null
							)
						)
					)
				then
					case
						when 
							LatestAssessmentScore>=8
						or
							(
								LatestAssessmentScore <=7
							and 
								LatestInvestigationTime is not null
							) 
						then 1
						else 0
					end
				else null
			end
			
	,Q3Denominator =
		case 
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and LatestAssessmentScore between 0 and 7 
				and 
					(
						LatestReferralRecordedTime between @PeriodStartDate and @PeriodEnd
					or
						(
							LatestReferralRecordedTime is null 
						and 
							DischargeDate between @PeriodStartDate and @PeriodEnd
						and
							(
								LatestAssessmentNotPerformedReason not in ('NoSpeech','Physically Cannot Speak')
							or
								LatestAssessmentNotPerformedReason is null
							)
						)
					)
			then 1
			else 0
		end
	,Q3Numerator =
		case 
			when LatestResponse = 1
				and LatestKnownToHaveDementiaResponse = 0
				and LatestAssessmentScore between 0 and 7 
				and LatestReferralRecordedTime between @PeriodStartDate and @PeriodEnd
					
			then 1
			else 0
		end
	,CurrentInpatient
	,CQUIN		
	,DementiaUpdated
	,WardUpdated

from
	APC.DementiaCurrent
where 
		(
		dateadd(mi,4320,AdmissionTime) between @PeriodStartDate and @PeriodEnd
	or
		LatestAssessmentTime between @PeriodStartDate and @PeriodEnd
	or 
		LatestReferralRecordedTime between @PeriodStartDate and @PeriodEnd
	or 
		DischargeTime between @PeriodStartDate and @PeriodEnd
	or 
		DischargeTime is null
	)
and 
	(
		CurrentInpatient = @CurrentInpatient
	or
		@CurrentInpatient is null
	)
and 
	(
		CQUIN = @CQUIN
	or
		@CQUIN is null
	)
and
	(
		LatestWard = @LatestWard
	or
		@LatestWard = 'All'
	)	
and
	(
		AgeCategory = @AgeCategory
	or
		@AgeCategory is null
	)
and	
	(
		AdmissionType = @AdmissionType
	or 
		@AdmissionType = 'All'
	)
and	
	(
		Division = @Division
	or 
		@Division = 'All'
	)

and
	(
		LatestKnownToHaveDementiaResponse = @KnownDementia
	or
		@KnownDementia is null
	)		
and
	(
		LatestResponse = @Find
	or
		@Find is null
	)		
and
	(
		Assessment = @Assessment
	or
		@Assessment is null
	)		
and
	(
		AssessmentScore = @AssessmentScore
	or
		@AssessmentScore is null
	)		
and
	(
		Investigation = @Investigation
	or
		@Investigation is null
	)		
and
	(
		Referral = @Referral
	or
		@Referral is null
	)		


						



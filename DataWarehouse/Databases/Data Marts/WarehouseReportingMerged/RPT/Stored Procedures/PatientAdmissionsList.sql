﻿



CREATE Procedure [RPT].[PatientAdmissionsList]
(
	 @StartDate					DATE
	,@EndDate					DATE
	,@NationalSpecialtyCode		VARCHAR(MAX) = NULL
	,@Division					VARCHAR(MAX) = NULL
	,@PatientCategoryCode		VARCHAR(100)
)
AS

	-- Create temp table of APC.Encounter rows filtered, due to performance gains
	SELECT
		  E.Cases
		 ,E.StartSiteID
		 ,E.SpecialtyID
		 ,E.ContextCode
		 ,E.MergeEncounterRecno
		 ,E.CasenoteNumber
		 ,E.PatientSurname
		 ,E.ConsultantID
		 ,E.AdmissionDate
		 ,E.DischargeDate
		 ,E.PatientCategoryCode	
		 ,E.StartWardTypeCode
		 ,E.StartDirectorateCode
		 
	INTO #T
	FROM
		APC.Encounter E
	WHERE
			E.AdmissionDate							BETWEEN @StartDate AND @EndDate
		AND E.PatientCategoryCode					IN (SELECT Item FROM dbo.fn_ListToTable(@PatientCategoryCode, ','))
		AND E.SpecialtyCode							NOT IN ('BOWL','GYN1','PrivPt')
		AND E.GlobalEpisodeNo						= 1
		AND (
					E.AdminCategoryCode				NOT IN ('2-PAY','02','PrivPt') 
				OR  E.AdminCategoryCode				IS NULL
			)
		AND COALESCE(E.PatientClassificationCode,'1')	IN ('1','2')

---------------
	
	SELECT
		 Cases										= E.Cases
		,Site										= SITE.NationalSite
		,SiteCode									= SITE.SourceSiteCode
		,Division									= DIR.Division
		,NationalSpecialty							= SPEC.NationalSpecialtyLabel
		,LocalSpecialtyCode							= SPEC.SourceSpecialtyCode
		,AllocatedSpecialty							= ALLOSVC.Allocation
		,Casenote									= E.CasenoteNumber
		,PatientSurname								= E.PatientSurname
		,Consultant									= CONS.Surname + ', ' + CONS.Initials
		,AdmissionDate								= E.AdmissionDate
		,DischargeDate								= E.DischargeDate
		,PatientCategoryCode						= E.PatientCategoryCode
		,PatientCategory							= PC.PatientCategory
		,AdmissionWard								= E.StartWardTypeCode
				
	FROM
		#T E
				
		INNER JOIN WH.Specialty SPEC 
			ON		E.SpecialtyID				= SPEC.SourceSpecialtyID
				
		INNER JOIN WH.Directorate DIR
			ON		E.StartDirectorateCode		= DIR.DirectorateCode
			
		INNER JOIN WH.Consultant CONS
			ON		E.ConsultantID				= CONS.SourceConsultantID
				AND	E.ContextCode				= CONS.SourceContextCode
				
		INNER JOIN WH.Site SITE
			ON		E.StartSiteID				= SITE.SourceSiteID
				AND	E.ContextCode				= SITE.SourceContextCode
				
		INNER JOIN APC.PatientCategory PC
			ON		E.PatientCategoryCode		= PC.PatientCategoryCode

		LEFT JOIN [WarehouseOLAPMergedV2].[Allocation].[DatasetAllocation] ALLOREC
			ON		ALLOREC.DatasetRecno		= E.MergeEncounterRecno
				AND ALLOREC.DatasetCode			= 'APC' 
				AND ALLOREC.AllocationTypeID	= 2				-- Service
    
	    LEFT JOIN [WarehouseOLAPMergedV2].[Allocation].[Allocation] ALLOSVC
			ON		ALLOSVC.[AllocationID]		= ALLOREC.[AllocationID]
				AND ALLOSVC.[AllocationTypeID]	= 2

	WHERE
			SPEC.NationalSpecialtyCode				IN (SELECT Item FROM dbo.fn_ListToTable(@NationalSpecialtyCode, ','))					
		AND DIR.Division							IN (SELECT Item FROM dbo.fn_ListToTable(@Division, ','))

	DROP TABLE #T
	

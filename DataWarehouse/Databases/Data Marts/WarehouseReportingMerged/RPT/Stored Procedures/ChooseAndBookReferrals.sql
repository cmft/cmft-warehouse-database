﻿

CREATE PROCEDURE [RPT].[ChooseAndBookReferrals] --RPT.ChooseAndBookReferrals '2 DEc 2013'
(
	 @ActivityDate		DATE
	,@Division			VARCHAR(MAX)
	,@Specialty			VARCHAR(MAX)
)AS
	CREATE TABLE #T (MergeEncounterRecno int, FutureAppointmentTime DATETIME, ActivityType varchar(5))

	-- Build a table of all qualifying OP.Encounter references to go into the report
	INSERT INTO #T
	----- New Referrals
		SELECT 
			 MIN(E.MergeEncounterRecno)
			,NULL
			,ActivityType = 'REF'
		FROM
			OP.Encounter E

		WHERE
				E.ReferralDate = @ActivityDate
			AND	E.AppointmentTypeCode LIKE 'Z%'
			AND E.DischargeDate		IS NULL
		GROUP BY
			 E.SourcePatientNo
			,E.SourceEncounterNo

----- Cancellations
		UNION

		SELECT 
			 E.MergeEncounterRecno
			,FUTUREAPP.AppointmentTime
			,ActivityType = 'CAN'
		FROM
			OP.Encounter E
			OUTER APPLY	-- Future not recorded appointments
			(
				SELECT
					 A.AppointmentTime
					,Rnk = ROW_NUMBER() OVER (ORDER BY A.AppointmentTime ASC)
				FROM
					OP.Encounter A
					WHERE
							A.DistrictNo		= E.DistrictNo
						AND A.AppointmentTime   > E.AppointmentTime
						AND A.SourceEncounterNo = E.SourceEncounterNo
						AND A.DNACode			= '9'
			) FUTUREAPP

		WHERE
				E.AppointmentCancelDate = @ActivityDate
			AND	E.AppointmentTypeCode LIKE 'Z%'
			AND E.DischargeDate		IS NULL
			AND COALESCE(FUTUREAPP.Rnk, 1) = 1		-- Get the next Appointment

----- Discharges With No Attendance
		UNION

		SELECT 
			E.MergeEncounterRecno
			,NULL
			,ActivityType = 'DIS'
		FROM
			OP.Encounter E
			CROSS APPLY		-- Count of Attendances for this referral
			(
				SELECT	
					Attendances = COUNT(*)  
				FROM
					OP.Encounter ALLE
				WHERE
						E.DistrictNo			= ALLE.DistrictNo
					AND	E.SourceEncounterNo		= ALLE.SourceEncounterNo
					AND ALLE.DNACode			IN ('5','6')
			) HASATTENDED

		WHERE
				E.DischargeDate = @ActivityDate
			AND	E.AppointmentTypeCode	LIKE 'Z%'
			AND HASATTENDED.Attendances = 0			-- No attendance

	-- Use the temp table as the driver for getting dimension values
	SELECT
		 #T.ActivityType
		,E.MergeEncounterRecno
		,E.SourcePatientNo
		,E.SourceEncounterNo
		,E.CasenoteNo
		,E.DistrictNo
		,E.NHSNumber
		,E.PatientForename
		,E.PatientSurname
		,E.DateOfBirth
		,E.ReferralDate
		,DIR.Division
		,Consultant					= CON.Surname + ', ' + CON.Initials 
		,E.ClinicCode
		,Specialty					= SPEC.NationalSpecialtyLabel
		,ActivityDate				= @ActivityDate
		,E.AppointmentTime
		,FutureAppointmentTime		= #T.FutureAppointmentTime
		,E.DischargeDate

	FROM
		#T

		INNER JOIN OP.Encounter E
			ON		#T.MergeEncounterRecno		= E.MergeEncounterRecno
		INNER JOIN WH.Directorate DIR
			ON		E.DirectorateCode			= DIR.DirectorateCode
		LEFT JOIN WH.Specialty SPEC
			ON		E.ReferralSpecialtyCode		= SPEC.SourceSpecialtyCode
				AND E.ContextCode				= SPEC.SourceContextCode
		LEFT JOIN WH.Consultant CON
			ON		E.ReferralConsultantCode	= CON.SourceConsultantCode
				AND E.ContextCode				= CON.SourceContextCode
	WHERE
			DIR.Division				IN (SELECT Item FROM dbo.fn_ListToTable(@Division,','))
		AND SPEC.NationalSpecialtyCode	IN (SELECT Item FROM dbo.fn_ListToTable(@Specialty,','))


	DROP TABLE #T



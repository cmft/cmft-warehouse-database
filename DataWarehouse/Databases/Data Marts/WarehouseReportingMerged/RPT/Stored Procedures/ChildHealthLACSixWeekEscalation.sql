﻿
CREATE procedure [RPT].[ChildHealthLACSixWeekEscalation] as

select
	 Exam.EsclationPeriodWeeks
	,Exam.DueDate
	,Exam.ExamCode
	,Demographics.NHSNumber
	,Demographics.Surname
	,Demographics.Forename
	,Demographics.SexCode
	,Demographics.DateOfBirth
	,LeadClinician.LeadClinician
	,School.School
	,LACStatus.LookedAfterStatus
	,LAC.ResidingPCTCode
	,LACA.DelayReason
	,LACA.RequestSentTo
	,SS.SocialServicesDepartment
from 
	Warehouse.ChildHealth.Entity Demographics

inner join
	(
	select
		 EntitySourceUniqueID
		,ExamScheduled.DueDate
		,ExamScheduled.ExamCode
		,EsclationPeriodWeeks = datediff(week , DueDate , getdate())
	from
		Warehouse.ChildHealth.ExamScheduled
	) Exam
on	Exam.EntitySourceUniqueID = Demographics.SourceUniqueID
and	Exam.DueDate <= dateadd(day , -42 , getdate())
and	Exam.ExamCode in ('LR' , 'FA' , 'RL')
			
left join Warehouse.ChildHealth.LookedAfter LAC
on	LAC.EntitySourceUniqueID = Demographics.SourceUniqueID
			
left join Warehouse.ChildHealth.LookedAfterStatus LACStatus
on	LACStatus.LookedAfterStatusCode = LAC.LookedAfterStatusCode

left join Warehouse.ChildHealth.LookedAfterAssessmentSummary LACA
on	LACA.EntitySourceUniqueID = Demographics.SourceUniqueID
and	LACA.ChildSeen = 'No'

left join Warehouse.ChildHealth.Birth 
on	Birth.EntitySourceUniqueID = Demographics.SourceUniqueID

left join Warehouse.ChildHealth.School
on	School.SchoolCode = birth.SchoolCode		

left join Warehouse.ChildHealth.LeadClinician
on	LeadClinician.LeadClinicianCode = LAC.LeadClinicianCode

left join Warehouse.ChildHealth.SocialServicesDepartment SS
on SS.SocialServicesDepartmentCode = LAC.SocialServicesDepartmentCode

order by
	Exam.EsclationPeriodWeeks
	
		
	
		
		

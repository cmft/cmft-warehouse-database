﻿


CREATE procedure [RPT].[HRG4ErrorsSummary] as

select 
		 Site = wh.site.NationalSite
		,ErrorType = coalesce(APC.HRG4Quality.QualityTypeCode,'blank')
		,DischargeMonth = calendar.TheMonth
		,ErrorCount = COUNT(DISTINCT GlobalProviderSpellNo)
from 
		APC.HRG4Quality 
inner join 
		apc.Encounter
	on 
		(apc.Encounter.MergeEncounterRecno = APC.HRG4Quality.mergeEncounterRecno)
inner join
		wh.Specialty
	on	
		(apc.Encounter.SpecialtyID = wh.Specialty.SourceSpecialtyID)
inner join
		wh.consultant
	on 
		(apc.Encounter.Consultantid = wh.consultant.sourceConsultantid) 
inner join
		wh.site
	on 
		(apc.Encounter.StartSiteID = wh.site.SourceSiteID)
inner join
		wh.calendar
	on 
		(apc.Encounter.DischargeDateID = wh.calendar.DateID)
where	
		QualityMessage not like '%Spell exceeds%'
	AND QualityMessage not like '%Primary diagnosis is blank%'
	AND QualityTypeCode not like '%AGE%'
	

	
group by APC.HRG4Quality.QualityTypeCode
		,wh.site.NationalSite
		,wh.calendar.TheMonth	
		
order by APC.HRG4Quality.QualityTypeCode
		,wh.site.NationalSite
		,wh.calendar.TheMonth

---------------------------------------------------------------------------

--ALTER procedure [RPT].[HRG4ErrorsSummary] as

--SELECT 
--		 Site = si.NationalSite
--		,ErrorType = coalesce(H.[QualityTypeCode],'blank')
--		,DischargeMonth = CAL.TheMonth
--		,ErrorCount = COUNT(*)
-- FROM 
--		WAREHOUSESQL.[WarehouseOLAPMergedV2].[APC].[HRG4Quality] H  
--	inner join WAREHOUSESQL.[WarehouseReportingMerged].[apc].[Encounter] E
--		on (e.MergeEncounterRecno = h.mergeEncounterRecno)
--	inner join WAREHOUSESQL.[WarehouseReportingMerged].wh.Specialty S
--		on (E.SpecialtyID = S.SourceSpecialtyID)
--	inner join WAREHOUSESQL.[WarehouseReportingMerged].wh.consultant C
--		on (E.Consultantid = C.sourceConsultantid) 
--	inner join WAREHOUSESQL.[WarehouseReportingMerged].wh.site SI
--		on (e.StartSiteID = SI.SourceSiteID)
--	inner join WAREHOUSESQL.[WarehouseOLAPMergedV2].[wh].[calendar] CAL
--		on (e.DischargeDateID = cal.DateID)
--WHERE	
--		QualityMessage not like '%Spell exceeds%'
--	AND QualityMessage not like '%Primary diagnosis is blank%'
--	AND QualityTypeCode not like '%AGE%'
	
--GROUP BY h.QualityTypeCode, si.NationalSite, CAL.TheMonth	
--ORDER BY h.QualityTypeCode, si.NationalSite, CAL.TheMonth



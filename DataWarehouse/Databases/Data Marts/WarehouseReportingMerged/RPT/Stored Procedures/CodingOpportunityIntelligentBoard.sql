﻿

CREATE PROCEDURE [RPT].[CodingOpportunityIntelligentBoard] 
(
	 @FinancialYear					VARCHAR(10)
	,@PatientClassificationCode		VARCHAR(100)	= NULL
	,@AdmissionMethodCode			VARCHAR(100)	= NULL
	,@Division						VARCHAR(MAX)	= NULL
	,@ComparisonMonth				VARCHAR(10)
)
AS
/****************************************************************************************
	Stored procedure : RPT.CodingOpportunityIntelligentBoard
	Description		 : SSRS proc for the Visual Studio report
	
	Reports >> DQ >> Coding Opportunity Intelligent Board
	
	Measures
	 Analysis of FCE Primary Diagnoses R Codes

	Modification History
	====================
	
	Date      Person     Description
	====================================================================================
	20.01.14    MH       Intial Coding
*****************************************************************************************/

---- The report compares data between the input Financial Year parameter and its previous Financial Year
---- So we derive 2 Start and End dates for each Financial Year upto and including the 
---- @ComparisonMonth parameter for each year
----
---- Example
---- =======
---- @FinancialYear = 2013/2014
---- @ComparisonMonth = Feb
---- StartDate1 = 1/4/2012 EndDate1 = 28/2/2013
---- StartDate2 = 1/4/2013 EndDate2 = 28/2/2014

	DECLARE @StartDate1		DATE
	DECLARE @EndDate1		DATE
	DECLARE @StartDate2		DATE
	DECLARE @EndDate2		DATE
	DECLARE @FinancialYear1	VARCHAR(10)
	
	---- Get previous Financial Year
	SELECT 
		@FinancialYear1 = FinancialYear
	FROM
		WH.Calendar CAL
	WHERE
		TheDate = DATEADD(DAY, -1, (SELECT MIN(TheDate) FROM WH.Calendar WHERE FinancialYear = @FinancialYear))
	
	---- Start and End Dates for previous Financial Year
	SELECT @StartDate1 = MIN(CAL.TheDate)
	FROM
		WH.Calendar CAL
	WHERE
		CAL.FinancialYear = @FinancialYear1
		
	SELECT @EndDate1 = MAX(CAL.TheDate)
	FROM
		WH.Calendar CAL
	WHERE
			CAL.FinancialYear	= @FinancialYear1
		AND CAL.MonthName		= @ComparisonMonth

	---- Start and End Dates for input parameter Financial Year	
	SELECT @StartDate2 = MIN(CAL.TheDate)
	FROM
		WH.Calendar CAL
	WHERE
		CAL.FinancialYear = @FinancialYear
		
	SELECT @EndDate2 = MAX(CAL.TheDate)
	FROM
		WH.Calendar CAL
	WHERE
			CAL.FinancialYear	= @FinancialYear
		AND CAL.MonthName		= @ComparisonMonth
		
				  
	IF @PatientClassificationCode = ''
		SET @PatientClassificationCode = NULL

	IF @AdmissionMethodCode = ''
		SET @AdmissionMethodCode = NULL
				
	SELECT
		 Period					= CAL.TheMonth
		,MonthName				= CAL.MonthName
		,PeriodKey				= CAL.FinancialMonthKey
		,FinancialYear			= CAL.FinancialYear
		,Division				= DIR.Division
		,Admissions				= 1
		,OpportunityCount		= 
								   CASE
									WHEN
										-- Both episodes to have an R Code
											FCE1DIAG.IsRcode								= 1
										AND COALESCE(FCE2DIAG.IsRCode, 1)					= 1
										-- and at least one episode is a Coding Opportunity
										AND
										(
												FCE1DIAG.IsRCodeCodingOpportunity				= 1
											OR	COALESCE(FCE2DIAG.IsRCodeCodingOpportunity, 0)	= 1
										)
									THEN 1
									ELSE 0
								  END
		,SpellLOSBand			= CASE
									WHEN FCE1.LOS BETWEEN 0 AND 5   THEN ' 0 - 5 Days'
									WHEN FCE1.LOS BETWEEN 6 AND 10	THEN ' 6 - 10 Days'	
									WHEN FCE1.LOS > 10				THEN '+10 Days'
									ELSE 'Unknown'
								  END						  
		
	FROM
		WarehouseOLAPMergedV2.APC.BaseEncounter FCE1					-- 1st episode
	
		LEFT JOIN WH.Diagnosis FCE1DIAG
			ON		FCE1.PrimaryDiagnosisCode		= FCE1DIAG.DiagnosisCode 
				
		INNER JOIN WH.Directorate DIR
			ON		FCE1.StartDirectorateCode		= DIR.DirectorateCode
					
		INNER JOIN WH.Calendar CAL
			ON		FCE1.AdmissionDate				= CAL.TheDate
			
		INNER JOIN APC.PatientClassification PC
			ON		FCE1.ContextCode				= PC.SourceContextCode
				AND	FCE1.PatientClassificationCode	= PC.SourcePatientClassificationCode
				
		INNER JOIN APC.AdmissionMethod AM
			ON		FCE1.ContextCode				= AM.SourceContextCode
				AND	FCE1.AdmissionMethodCode		= AM.SourceAdmissionMethodCode
				
		LEFT JOIN WarehouseOLAPMergedV2.APC.BaseEncounter FCE2		-- 2nd episode, if present
			ON		FCE1.GlobalProviderSpellNo		= FCE2.GlobalProviderSpellNo
				AND	FCE2.GlobalEpisodeNo			= 2
				
		LEFT JOIN WH.Diagnosis FCE2DIAG
			ON		FCE2.PrimaryDiagnosisCode		= FCE2DIAG.DiagnosisCode 
				
	WHERE
		(
			(FCE1.AdmissionDate BETWEEN @StartDate1 AND @EndDate1)
			OR
			(FCE1.AdmissionDate BETWEEN @StartDate2 AND @EndDate2)			
		)
		AND	FCE1.GlobalEpisodeNo				= 1									-- 1st episode in spell
		-- 1st and 2nd episodes to be R Codes, or 1st only if single episode spell
		AND 
		(
				@PatientClassificationCode IS NULL
			OR	PC.NationalPatientClassificationCode IN (SELECT Item FROM dbo.fn_ListToTable(@PatientClassificationCode,','))
		)
		AND 
		(
				@AdmissionMethodCode IS NULL
			OR	AM.NationalAdmissionMethodCode IN (SELECT Item FROM dbo.fn_ListToTable(@AdmissionMethodCode,','))
		)
		AND 
		(
				@Division IS NULL
			OR	DIR.Division IN (SELECT Item FROM dbo.fn_ListToTable(@Division,','))
		)



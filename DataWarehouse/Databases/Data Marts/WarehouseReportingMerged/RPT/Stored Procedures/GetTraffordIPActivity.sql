﻿

	--Created 23 May 2012
	--Created by Gareth Cunnah
	--To extract Trafford Inpatient Data for Activity Report

	--25 June 2012 Added insert into table TGHInformation temp fix for Ducia

	CREATE proc [RPT].[GetTraffordIPActivity] 

		
	 @startdate datetime = null 
	,@enddate datetime = null
	


	as

	declare @SetStartDate datetime = coalesce(@StartDate,'01 apr 2012')
	
	--coalesce(@StartDate,(SELECT DATEADD(dd,0, DATEDIFF(dd,0, DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12), getDate() ) - datePart(d,DATEADD( mm, -(((12 + DATEPART(m, getDate())) - 4)%12),getDate() ))+1 ) )))
	
	declare @SetEndDate datetime =  coalesce(@enddate,
			--(select DATEADD(wk, DATEDIFF(wk,7,GETDATE()), 7) )
			getdate ()	
				)
	
	declare @lastMonday Datetime = coalesce(@SetEndDate - 7,(select DATEADD(wk, DATEDIFF(wk,7,GETDATE()), 0) ) )

	drop table TGHInformation.dbo.TraffordIPActivity
	
	
		select distinct
			-- sourceuniqueid
			 Encounter.ProviderSpellNo
			,NationalSpecialtyCode = coalesce(Encounter2Specialty.NationalSpecialtyCode ,Specialty.NationalSpecialtyCode)
			,spec_desc = coalesce(Encounter2Specialty.NationalSpecialtyCode ,Specialty.NationalSpecialtyCode) + ' ' + coalesce(Encounter2Specialty.NationalSpecialty ,Specialty.NationalSpecialty) 
			,Encounter.AdmissionTime
			,Encounter.DischargeTime
			,SourceAdministrativeCategoryCode
			,[NationalAdmissionMethodCode]
			,[NationalAdmissionMethod]
		    
			,[TYPE] =
				case
					when NationalPatientClassificationCode in ('2','3') and NationalAdmissionMethodCode in ('11','12','13') then 'PSD - Day Case'
					when NationalAdmissionMethodCode < '14' then 'EL'
					when NationalAdmissionMethodCode > '14' then 'None Eelctive'
				else 'None Eelctive' end 

			,[TYPE2] =
				case
					when NationalPatientClassificationCode in ('2','3') and NationalAdmissionMethodCode in ('11','12','13') then 'Elective'
					when NationalAdmissionMethodCode < '14' then 'Elective'
					when NationalAdmissionMethodCode > '14' then 'None Eelctive'
				else 'None Eelctive' end 
			,Ward.SourceWardCode
			,LastWeeksCases =
					case
					when DATEADD(dd, 0, DATEDIFF(dd, 0, Encounter.AdmissionTime)) >= @lastMonday then 1
					else 0 end
			,NationalAdministrativeCategoryCode
			,NationalAdministrativeCategory
					
			into TGHInformation.dbo.TraffordIPActivity		
			
			from APC.Encounter Encounter
			 
			
			inner join APC.[AdmissionMethod] AdmissionMethod
			on AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID
			and AdmissionMethod.SourceContextCode = 'TRA||UG'
			
			inner join APC.PatientClassification PatientClassification
			on PatientClassification.SourcePatientClassificationID = Encounter.PatientClassificationID
			and PatientClassification.SourceContextCode = 'TRA||UG'


			
			left join [APC].[Ward] Ward
			on Ward.SourceWardID = Encounter.EndWardID
			
			
				--Gareth Cunnah 15/06/2012 required if emegancy admission then second episode should be used if found
			left join APC.Encounter EncounterSecond
			on EncounterSecond.ProviderSpellNo = Encounter.ProviderSpellNo
			and EncounterSecond.ContextCode = 'TRA||UG'
			and EncounterSecond.SourceEncounterNo = 2
			and AdmissionMethod.NationalAdmissionMethodCode > '14'
			
				left join WH.Specialty Specialty
				on    Specialty.SourceSpecialtyID = Encounter.SpecialtyID
				and Specialty.SourceContextCode = 'TRA||UG'

				left join WH.Specialty Encounter2Specialty
				on    Encounter2Specialty.SourceSpecialtyID = EncounterSecond.SpecialtyID
				and Encounter2Specialty.SourceContextCode = 'TRA||UG'

			
			left join WH.AdministrativeCategory AdministrativeCategory
			on AdministrativeCategory.SourceAdministrativeCategoryID = Encounter.AdminCategoryID
						
				where --TGH Data Only
  				Encounter.ContextCode = 'TRA||UG'
		  			
  				and  
  			--	DATEADD(dd, 0, DATEDIFF(dd, 0, Encounter.DischargeTime)) between @startdate and @enddate
		  		DATEADD(dd, 0, DATEDIFF(dd, 0, Encounter.AdmissionTime)) between @SetStartDate and @SetEndDate
			  		
		  		and AdmissionMethod.NationalAdmissionMethodCode  in ('21','81','11','22','13','24','12','28','99')
 
					--Changed 15 Jun 2012 Gareth Cunnah
  				--and LastEpisodeInSpellID = 1893585 --Gets last Episode
				and Encounter.FirstEpisodeInSpellIndicator = 1 --Gets First Episode
  				
  				and coalesce(Encounter2Specialty.NationalSpecialtyCode ,Specialty.NationalSpecialtyCode) <> '990' --exclude Salford Royal and Central before merger data

				and 
					(
					Ward.SourceWardCode <> 'POAU' --exclude Paediatric Observation and Assessment Unit (TRA||UG:POAU)
					or
					Ward.LocalWardCode is null 
					)

  				order by ProviderSpellNo
		  		
		  		
		  		
print @SetStartDate 
print @SetEndDate
print @lastMonday
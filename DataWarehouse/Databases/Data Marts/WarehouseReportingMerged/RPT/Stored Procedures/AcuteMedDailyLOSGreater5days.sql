﻿

CREATE procedure [RPT].[AcuteMedDailyLOSGreater5days]

 @WardCode  varchar(max) = 'All'

as
	
select		
	 Ward = SourceWardCode	
	,LoS = datediff(d,TheDate,getdate())	
	,Admission = AdmissionTime	
	,[Patient Forename] = PatientForename	
	,[Patient Surname] = PatientSurname	
	,Age = AgeCode	
	,DateOfBirth
	,encounter.ExpectedDateofDischarge
	,ReportDate = convert(varchar(11), GETDATE(), 112)  	
		,dischargedateid
from		
	APC.Encounter	
		
left join WH.Calendar	
	on AdmissionDateID = DateID	
		
left join WH.Directorate	
	on StartDirectorateCode = DirectorateCode
		
left join APC.WardStay	
	on WardStay.ProviderSpellNo = Encounter.ProviderSpellNo	
	and EndDate is null	
		
left join APC.Ward	
	on WardID = SourceWardID	
		
left join WH.Specialty	
	on SpecialtyID = SourceSpecialtyID	
		
		
where		
		DischargeDateID = 1
	and EpisodeEndDateID = 1
	and datediff(d,TheDate,dateadd(dd, datediff(dd, 0,getdate()), 0)) >5	
	and Division ='Medicine & Community'	
	and SourceWardCode is not null
	and SourceWardCode <> 'TEST'
	and	(
			Ward.SourceWardCode = @WardCode
		OR
			@WardCode ='All'
		)
order by		
	 SourceWardCode	
	,LoS desc	




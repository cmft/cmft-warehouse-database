﻿

CREATE procedure [RPT].[PEXFriendsAndFamily]

(
	@month datetime = null

)

as

--declare @month datetime = '2013-01-01 00:00:00.000'

Select

*

from

(

select

	Spells.Site
	,Spells.Ward
	,CensusMonth = SpecDischargeMonth
	,Spells
	,TargetSpells
	,Score
	,Cases

from

(

			select

					Site = Site.[NationalSite]
				   ,Ward = [SourceWardCode] + ' - ' + [SourceWard]
				   ,[SourceWardCode]
				   ,SpecDischargeMonth = @month
				   ,Spells = sum(case when dateadd(month,datediff(month,0,Discharge.TheDate),0) = @month then 1 else 0 end)
				   ,TargetSpells = (sum(case when Discharge.TheDate<@month then 1 else 0 end))/12
			from
				APC.Encounter Encounter

			inner join
				WH.[Specialty] Specialty
			on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID

			inner join
				APC.[AdmissionMethod] AdmissionMethod
			on	AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID

			inner join
				WH.Calendar Admission
			on  Admission.DateID = Encounter.AdmissionDateID
				   
			inner join
				WH.Calendar Discharge
			on  Discharge.DateID = Encounter.DischargeDateID

			inner join
				[APC].[LastEpisodeInSpell] LastEpisodeInSpell
			on  LastEpisodeInSpell.SourceLastEpisodeInSpellID = Encounter.LastEpisodeInSpellID
			
			inner join
				[APC].[DischargeDestination] DischargeDestination
			on  DischargeDestination.SourceDischargeDestinationID = Encounter.DischargeDestinationID
			
			inner join
				WH.Ward Ward
			on	SourceWardID = Encounter.EndWardID

			inner join
				WH.Site Site
			on	Encounter.[EndSiteID] = Site.SourceSiteID
					
			inner join
				APC.PatientClassification PatientClassification
			on	PatientClassificationID = PatientClassification.SourcePatientClassificationID

			where
				   Discharge.TheDate > @month - 365
			and
				   [NationalLastEpisodeInSpellCode] = '1'
			and
				   Specialty.NationalSpecialtyCode not in ('424', '501', '422')
			and
				   Site.SourceSiteCode not in ('NRMC')
			and
				   floor(datediff(day, DateOfBirth, Admission.TheDate) / 365.25) >= 16
			and
				   NationalPatientClassificationCode not in ('3', '4')
			and
				   LOS > = 1
			and
				   [SourceWardCode] not in ('MAU', 'OMU', 'AMU', 'MAU2', '54', 'PDEN', '66')   --Medical Ass, Private and Maternity Wards not included

			and	   
				   [NationalDischargeDestinationCode] not in ('49','51','53','79') --added 13/03/2013: exclusions discharged to other trusts
			and	   
				   DateOfDeath is null
				   
			group by
				   Site.[NationalSite]
				   ,SourceWardCode
				   ,[SourceWardCode] + ' - ' + [SourceWard]

			) Spells
			
left outer join

			 (		
							
							select 

									 SiteCode = case 
													when Location.[Location] in ('Ward 2 Trafford') then 'W2'
													when Location.[Location] in ('Ward 6 Trafford') then 'W6'
													when Location.[Location] in ('Ward 15 Trafford') then 'W15'
													when Location.[Location] in ('Ward 16 Trafford') then 'W16'
													when Location.[Location] in ('Surgical Ward Trafford') then 'SUN'
													when Location.[Location] in ('Orthopaedics Trafford') then 'OUN'
													when Location.[Location] in ('Ward 23 Childrens Resource Centre TRF') then 'CRC'
													when Location.[Location] in ('MAU Trafford') then 'MAU2'
													when Location.[Location] in ('HDU / ITU Trafford') then 'ICU'
													when Location.[Location] in ('ESTU(2)') then 'ESTU'										
													else Location.[Location]
												end
									,Location.[Location]
									,Score =
												case
													when Answer.[Answer] like ('Don%')	then 6
													when Answer.[Answer] in ('Definitely not', 'Extremely unlikely', 'Very unlikely')	then 5
													when Answer.[Answer] in ('Unlikely')	then 4
													when Answer.[Answer] in ('Neither likely nor unlikely')	then 3
													when Answer.[Answer] in ('Maybe','Likely')	then 2
													when Answer.[Answer] in ('Definitely yes', 'Extremely Likely', 'Very likely')	then 1
													else 0
												end
									,ResponseMonth = dateadd(mm, datediff(m,0,Response.[CensusTime]),0)
									,FFTAnswer =	Answer.Answer
									,Cases

							from
								[PEX].[Response] Response

							inner join
								[PEX].[Answer] Answer
							on	Response.[AnswerID] = Answer.[AnswerID]
								
							inner join
								[PEX].[Location] Location
							on	Response.[LocationID] = Location.[LocationID]
										
							inner join
								[PEX].[Question] Question
							on	Response.[QuestionID] = Question.[QuestionID]

							inner join
								[PEX].[Survey] Survey
							on	Response.[SurveyID] = Survey.[ID]
							inner join
												(
																								
													select
													
													Location
													,CensusTime
													,Answer
													
													from
														[PEX].[Response] DisResponse

													inner join
														[PEX].[Answer] DisAnswer
													on	DisResponse.[AnswerID] = DisAnswer.[AnswerID]
														
													inner join
														[PEX].[Location] DisLocation
													on	DisResponse.[LocationID] = DisLocation.[LocationID]
																
													inner join
														[PEX].[Question] DisQuestion
													on	DisResponse.[QuestionID] = DisQuestion.[QuestionID]
													
													where
														Question in (
																	'Are you being discharged in the next 24 hours?'
																	,'Are you being discharged today?'
																	,'Are you being discharged in the next 48 hours?'
																	)
														
												)	Discharged
												
							on	Location.Location = Discharged.Location
							and	Response.CensusTime = Discharged.CensusTime					
												
							where	
								[Question] in	
											(									
											  'How likely are you to recommend our A & E department to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend our A&E department to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend our out patient department to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend our ward to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend this department / area to a friend or loved one?'
											  ,'How likely are you to recommend this unit/area to a friend or loved one?'
											  ,'How likely are you to recommend this ward / area to a friend or loved one if they had to come to hospital?'
											  ,'How likely are you to recommend this ward / area to a friend or loved one?'
											  ,'How likely are you to recommend this ward / department to family and friends if they needed similar care or treatment?'
											  ,'How likely is it that you would recommend participating in clinical research to your friends and family?'
											  ,'How likely would are you to recommend this ward / area to a friend or loved one?'
											)
							and 
								dateadd(month, datediff(month,0,Response.[CensusTime]),0) = @month
								
							and
											Discharged.Answer = 'Yes'
								
			) Responses	
							   
			on Spells.SourceWardCode = Responses.SiteCode
			and SpecDischargeMonth = ResponseMonth 
	
			
	) FriendsAndFamilyResponse	
		
pivot (sum(Cases) for Score in ([1],[2],[3],[4],[5],[6])) as ScoreInCategory

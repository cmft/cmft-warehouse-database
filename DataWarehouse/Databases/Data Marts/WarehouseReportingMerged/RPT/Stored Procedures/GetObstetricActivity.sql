﻿


create PROCEDURE [RPT].[GetObstetricActivity]
	(
	@StartDate Date
	,@EndDate date
	,@ActitivyType as nvarchar(MAX)
	
	
	) 
	
	
	
AS


--Declare @StartDate Datetime
--Declare @EndDate Datetime

--Set @StartDate = '01 Jan 2011'
--Set @EndDate = '31 Dec 2011'


Select 
		ActivityType 
		,DateOfActivity 
		,CasenoteNumber 
		,Surname 
		,Forenames 
		,result 
		,Hospital 
		

From

(


	 (
	Select
			ActivityType = case
						when replace(BLOOD_LOSS,'.','') *1 >=1500 then 'PPH >=1500' else 'PPH 500 to 1499' end
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = BLOOD_LOSS
			,Hospital = DEL_HOSP_NAME
			
		
	From 

			CMISStaging.SMMIS.BIRTH_REG BR 
			
			Inner Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
			
			
			Left Join
			CMISStaging.SMMIS.MOTHER_REG MR
			On BR.M_NUMBER = MR.M_NUMBER

			Left Join
			CMISStaging.SMMIS.LABOUR lab
			On BR.OCCURRENCE = lab.OCCURRENCE
			And BR.M_NUMBER = lab.M_NUMBER
			and BLOOD_LOSS is not null
			and  replace(BLOOD_LOSS,'.','') *1 >= 500

		 where   BLOOD_LOSS is not null
		)	

			
	

	union

	(

	Select
			ActivityType = 'Shoulder Dystocia'
			,DateOfActivity  = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			
			,Result = null
			,Hospital = DEL_HOSP_NAME
			
			
	From 

			CMISStaging.SMMIS.BIRTH_REG BR 
			
			Inner Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and Shoulder_Dystocia = 'Y'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
			
			Left Join
			CMISStaging.SMMIS.MOTHER_REG MR
			On BR.M_NUMBER = MR.M_NUMBER

			
		)
	 union
	 (	
		


	Select distinct

			ActivityType = 'TOP'
		
			,DateOfActivity = PrimaryProcedureDate
			,CasenoteNumber = CasenoteNumber
			,Surname = PatientForename
			,Forenames = PatientSurname
			
			,Result = null
			,Hospital = NationalSite
			
		
			
			from APC.Encounter apc
			
			join WH.[Site] hosp on
			hosp.SourceSiteID = apc.StartSiteID
			
			join WH.Specialty spec on
			 spec.SourceSpecialtyID = apc.SpecialtyID
			 and [NationalSpecialtyCode] = '501'
			 
			join APC.Operation Procs on
			Procs.MergeEncounterRecno = apc.MergeEncounterRecno
			and left(PrimaryProcedureCode,3) = 'Q14'
			and OperationCode in ('Y95.1')
			and PrimaryProcedureDate Between @StartDate and @EndDate
			
		)
		
		Union
		(
		

	Select distinct

			ActivityType = 'Unrine Retention'
		
			,DateOfActivity = TheDate
			,CasenoteNumber = CasenoteNumber
			,Surname = PatientForename
			,Forenames = PatientSurname
			
			,Result = null
			,Hospital = NationalSite
		
		
			
			from APC.Encounter apc
			
			join WH.[Site] hosp on
			hosp.SourceSiteID = apc.StartSiteID
			
			join WH.Calendar cal on
			cal.dateId = apc.AdmissionDateID
			and TheDate Between @StartDate and @EndDate
			
			join WH.Specialty spec on
			 spec.SourceSpecialtyID = apc.SpecialtyID
			 and [NationalSpecialtyCode] = '501'
			 
			join APC.Diagnosis Diag on
			Diag.MergeEncounterRecno = apc.MergeEncounterRecno
			and left(DiagnosisCode,5) = 'R33.X'
			
			where left(PrimaryProcedureCode,3) between 'R17' and 'R25'
			
		
			
		)
		
		
			Union
		(
		

		
	Select

			ActivityType = 'Spontaneous Rupture of membrane'
		
			,DateOfActivity = TheDate
			,CasenoteNumber = CasenoteNumber
			,Surname = PatientForename
			,Forenames = PatientSurname
						,Result = null
			,Hospital = NationalSite
	
		
			
			from APC.Encounter apc
			
			join WH.[Site] hosp on
			hosp.SourceSiteID = apc.StartSiteID
			
			join WH.Calendar cal on
			cal.dateId = apc.AdmissionDateID
			and TheDate Between @StartDate and @EndDate
			
			join WH.Specialty spec on
			 spec.SourceSpecialtyID = apc.SpecialtyID
			 and [NationalSpecialtyCode] = '501'
			 
			join APC.Diagnosis Diag on
			Diag.MergeEncounterRecno = apc.MergeEncounterRecno
			and left(DiagnosisCode,3) = 'O42'
			
		
			
		)
		
		
		union
		
		(
		
		
		Select
		ActivityType = 'ECLAMPSIA'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = null
			,Hospital = DEL_HOSP_NAME
						
			from CMISStaging.SMMIS.LABOUR lab
			
			 Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			and lab.ECLAMPSIA = 'Y'
			
				
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
	
			
		)
		
		Union
		
		(

		
		Select
			ActivityType = case when lab.PERINEUM like '%T%' then '3rd/4th Degree Tear' else '2nd Degree Tear' end
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = lab.PERINEUM
			,Hospital = DEL_HOSP_NAME
				
			from CMISStaging.SMMIS.LABOUR lab
			
			 Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			and (lab.PERINEUM like '%T%' or lab.PERINEUM like '%S%' )
		
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
			
				
		)
		
		
		union
		
		
		(

		
		Select
		ActivityType = 'Induction of labour'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = lab.METHOD_IND_AUG
			,Hospital = DEL_HOSP_NAME
				
			
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			and lab.METHOD_IND_AUG is not null
			
							
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
		
		
		)
		
		union
		
			
		(
		
		
		Select
		ActivityType = 'Fetal Blood Sample'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =MR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = null
			,Hospital = DEL_HOSP_NAME
				
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			and CORD_BLOOD_GAS = 'Y'
			
				
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
		
		
		)
		
	union
	
			

		(
	

		Select
		ActivityType = 'PRE ECLAMPSIA'
			,DateOfActivity = Convert(Date,ANS.DATE_STAMP)
			,CasenoteNumber = MR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = null
			,Hospital = ANS.LOCATION
		
						
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
				
			
			join CMISStaging.SMMIS.AN_SUMMARY ANS on
			ANS.M_NUMBER = lab.M_NUMBER
			and ANS.OCCURRENCE = lab.OCCURRENCE
			and ANS.PRE_ECLAMPTIC = 'Y'
						
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
		
		
		
		)
		
		union
		
		(
		
	
	
	Select
		ActivityType = 'Oxytocin'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = lab.METHOD_IND_AUG
			,Hospital = DEL_HOSP_NAME
					
			
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			and lab.METHOD_IND_AUG in ('O','R')
		
				
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
		
		)
		
		
		union
		
		(
		

			
		Select
		ActivityType = 'Low Risk Care'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = ACTUAL_HOSPITAL
			,Hospital = DEL_HOSP_NAME
		
			
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			and lab.METHOD_IND_AUG is not null
			and ACTUAL_HOSPITAL IN ('S','L')
					
								
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
		
		)
		
		
		union
		
		(

		
		Select distinct
		ActivityType = 'VBAC'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = BR.METHOD_DELIVERY
			,Hospital = DEL_HOSP_NAME
					
			
			from CMISStaging.SMMIS.LABOUR lab
			
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			and BR.METHOD_DELIVERY like '%S%'
			
				 
			 Join CMISStaging.SMMIS.BK_VISIT_DATA vis on
			vis.M_NUMBER = lab.M_NUMBER
			and (BK_PREV_CAES > 0 or BK_PREV_CAES is not null)
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
		
		)
		
		union
		
		(

		select
		ActivityType = 'DVT'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result =   lab.PN_COMPLICATIONS
			,Hospital = DEL_HOSP_NAME
					
			
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
			and lab.PN_COMPLICATIONS = 'V'
			
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			
						
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
			
			)
			
			union
		
		(


		select
		ActivityType = 'Mental Health'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = vis.PSYCHIATRIC_PROBLEMS
			,Hospital = DEL_HOSP_NAME
		
			from CMISStaging.SMMIS.BK_VISIT_DATA vis
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On MR.M_NUMBER = vis.M_NUMBER
			and vis.PSYCHIATRIC_PROBLEMS <> 'N'
						
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = vis.M_NUMBER
			 and BR.OCCURRENCE = vis.OCCURRENCE
						
						
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
			
			)
		
				union
		
		(

		select
		ActivityType = 'Multiple Pregnancies'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = summ.NUMBER_OF_FETUSES
			,Hospital = DEL_HOSP_NAME
						
			
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR
			On lab.M_NUMBER = MR.M_NUMBER
			
			Join CMISStaging.SMMIS.BIRTH_REG BR ON
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			 			
			
			join CMISStaging.SMMIS.AN_SUMMARY summ on
			summ.M_NUMBER = lab.M_NUMBER
			and summ.OCCURRENCE = lab.OCCURRENCE
			and (NUMBER_OF_FETUSES <> '1' and NUMBER_OF_FETUSES is not null)
						
							
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR
			On BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
			
			)
			
			
				union
		
		(

		select
		ActivityType = 'Rubella'
			,DateOfActivity = Convert(Date,DATE_OF_BIRTH)
			,CasenoteNumber =BR.M_Number
			,Surname = MR.CURRENT_SURNAME
			,Forenames = MR.F_FORENAMES
			,result = null
			,Hospital = DEL_HOSP_NAME
				
			
			from CMISStaging.SMMIS.LABOUR lab
			
			Join CMISStaging.SMMIS.MOTHER_REG MR on
			lab.M_NUMBER = MR.M_NUMBER
			
			Join CMISStaging.SMMIS.BIRTH_REG BR on
			 BR.M_NUMBER = lab.M_NUMBER
			 and BR.OCCURRENCE = lab.OCCURRENCE
			
			join CMISStaging.SMMIS.BK_INVESTIGATIONS inv on
			inv.M_NUMBER = lab.M_NUMBER
			and inv.OCCURRENCE = lab.OCCURRENCE
			and inv.Type = '7' and inv.RESULT_MENU = 'N' 
			
					
			
		 Join 
			CMISStaging.SMMIS.INFANT_REG IR on
			 BR.I_NUMBER = IR.I_NUMBER
			and Birth_Order = '1'
			and DATE_OF_BIRTH Between @StartDate and @EndDate
			
			)
		
		
		
		) data
		
	where 	ActivityType in (Select Value from WarehouseReporting.rpt.ssrs_multiValueParamSplit(@ActitivyType,','))
		

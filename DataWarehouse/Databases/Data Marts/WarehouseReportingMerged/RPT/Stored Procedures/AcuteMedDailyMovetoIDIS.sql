﻿


	CREATE procedure [RPT].[AcuteMedDailyMovetoIDIS]

	as

	select 		
		 ReferralDate	
		,Specialty = SourceSpecialtyCode	
		,CasenoteNo	
		,NHSNumber	
		,DistrictNo	
		,DateOfBirth	
			
	from		
		OP.encounter	
			
		join OP.Clinic	
		on ClinicID = SourceClinicID	
			
		join OP.AttendanceStatus	
		on AttendanceStatusID = SourceAttendanceStatusID	
			
		join WH.Specialty	
		on ReferralSpecialtyID = SourceSpecialtyID	
			
	where		
			AppointmentTime > '01 apr 2012'
		and SourceClinicCode in('CHTBFOL', 'CHTBNEW', 'JSTBFOL','JSTBNEW', 'MWTBFOL', 'MWTBNEW', 'SBTBFOL', 'SBTBNEW')	
		and SourceAttendanceStatusCode in ('5', '6')	
		and SourceSpecialtyCode <> 'IDIS'	



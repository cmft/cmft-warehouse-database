﻿

CREATE PROCEDURE [RPT].[GetReferralActivityAnalysis]

(	
	 @CCGCode VARCHAR(MAX)= 'All'
	,@Division VARCHAR(MAX) ='All'
	,@Directorate VARCHAR(MAX) ='All'
	,@Specialtycode VARCHAR(MAX)= NULL	
	,@GPPractice VARCHAR(MAX)= 'All'
)

AS

--DECLARE	 @CCGCode VARCHAR(MAX)= 'All'
--DECLARE	@Division VARCHAR(MAX)= NULL
--DECLARE	@Specialtycode VARCHAR(MAX)= NULL	
--DECLARE	@GPPractice VARCHAR(MAX)= 'All'

BEGIN
	SET NOCOUNT ON;

DECLARE @TheDate DATE = CONVERT(DATE,GETDATE(),113)
DECLARE @EndOfWeekRange DATE= DATEADD(dd, -DATEPART(dw, @TheDate)+1, @TheDate)
DECLARE @StartOfWeekRange DATE = DATEADD(dd,1,DATEADD(WEEK, -52, @EndOfWeekRange))
DECLARE @EndofDayRange DATE = @TheDate
DECLARE @StartOfDayRange DATE =  DATEADD(dd,-56,@TheDate)
DECLARE @StartofMonthRange DATE =  DATEADD(MONTH, DATEDIFF(MONTH, 0, @TheDate) - 25, 0)
DECLARE @EndofMOnthRange DATE =  DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@TheDate),0))

SELECT 
	 RDay = E.referraldate
	,RWeek = C.WeekNoKey
	,Rmonth = C.FinancialMonthKey
	,TotRefs = COUNT(E.SourceEncounterNo)
	,Dflag =	CASE 
					WHEN E.referraldate >= @StartOfDayRange 
					 AND E.referraldate < DATEADD(dd,1,@EndofDayRange)
						THEN 1 	
					ELSE 0 	
				END
	,Wflag =	CASE 
					WHEN E.referraldate >= @StartOfWeekRange 
					AND E.referraldate < DATEADD(dd,1,@EndofWeekRange)
						THEN 1 	
					ELSE 0
				END		
	,Yflag =	CASE 
				WHEN E.referraldate >= @StartOfMonthRange 
				 AND E.referraldate < DATEADD(dd,1,@EndofMonthRange)
						THEN 1 	
					ELSE 0 	
				END
	
FROM     
	RF.Encounter E 
INNER JOIN WH.Calendar AS C ON E.referraldate = C.thedate
LEFT OUTER join wh.Specialty Specialty 
	ON Specialty.SourceSpecialtyID = E.SpecialtyID

LEFT OUTER join wh.Directorate Directorate	
	ON Directorate.DirectorateCode = E.DirectorateCode

LEFT OUTER JOIN WH.CCG CCG 
	ON CCG.CCGCode = E.CCGCode

LEFT OUTER join Organisation.dbo.Practice 
	ON Practice.OrganisationCode = E.EpisodicGpPracticeCode

WHERE	
	E.referraldate >= @StartofMonthRange
		AND 
		(
			Directorate.Division IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Division,','))
		OR
			@Division  ='All'
		)
		AND 
		(
			Directorate.DirectorateCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@Directorate,','))
		OR
			@Directorate  ='All'
		)

		AND 
		(
			NationalSpecialtyCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@specialtycode,','))
		OR
			@specialtycode IS NULL
		)
		AND 
		(
			Practice.OrganisationCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@GPPractice,','))
		OR
			@GPPractice ='All'
		)

			AND 
		(
			CCG.CCGCode IN(SELECT VALUE FROM RPT.SSRS_MultiValueParamSplit(@CCGCode,','))
		OR
			@CCGCode = 'All'
		)
GROUP BY 
	 E.referraldate
	,C.WeekNoKey
	,C.FinancialMonthKey


END;



﻿
CREATE proc [RPT].[GetDateRange]
(@daterange varchar (50))

as

set dateformat dmy
set nocount on

if @daterange = 'Last 7 Days'
	select [Start] = dateadd(day, -7, dateadd(dd, datediff(dd, 0, getdate()), 0)) 
			,[End] = dateadd(dd, datediff(dd, 0, getdate()), 0)
			
			
else if @daterange = 'Last Week'
	select [Start] = dateadd(day, 1, dbo.f_Last_of_Week(dateadd(day, -14, getdate())))
			,[End] = dbo.f_Last_of_Week(dateadd(day, -7, getdate()))		
			
else if @daterange = 'YTD'
	select [Start] = dbo.f_First_of_Financial_Year(getdate())
			,[End] = dbo.f_Last_of_Week(dateadd(day, -7, getdate()))
			
else if @daterange = 'Last 12 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -12, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
				
else if @daterange = 'Last 13 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -13, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
else if @daterange = 'Last 18 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -18, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
else if @daterange = 'Last 6 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -6, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))

else if @daterange = 'Last 4 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -4, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
else if @daterange = 'Last 3 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -3, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
else if @daterange = 'Last Month'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -1, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
			
else if @daterange = 'LoS 14 weeks'
	select [Start] = dateadd(wk, -14, (select max([SnapshotDate]) from ETL.LoSPatientLevelData))	
			,[End] = (select max([SnapshotDate]) from WarehouseReporting.ETL.LoSPatientLevelData)
			
else if @daterange = 'LoS 21 weeks'
	select [Start] = dateadd(wk, -21, (select max([SnapshotDate]) from ETL.LoSPatientLevelData))	
			,[End] = (select max([SnapshotDate]) from WarehouseReporting.ETL.LoSPatientLevelData)
			
			
else if @daterange = 'Last 13 Months 4 IB'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -19, getdate()))
			,[End] = dbo.f_Last_of_Month(dateadd(month, -6, getdate()))
			

else if @daterange = 'Medisec Current Week'
	select [Start] = dateadd(day, - datepart(dw, getdate()) + 2 , dateadd(dd, datediff(dd, 0, getdate()), 0))
			,[End] = case
						when (select max(DischargeDate) from Warehouse.Medisec.Encounter) < dateadd(day, - datepart(dw, getdate()) + 2 , dateadd(dd, datediff(dd, 0, getdate()), 0))
						then dateadd(day, - datepart(dw, getdate()) + 2 , dateadd(dd, datediff(dd, 0, getdate()), 0))
						else (select max(DischargeDate) from Warehouse.Medisec.Encounter)
					end
			
					
else if @daterange = 'Medisec Last Week'
	select [Start] = dateadd(day, 1, dbo.f_Last_of_Week(dateadd(day, -14, getdate())))
			,[End] = case
						when (select max(DischargeDate) from Warehouse.Medisec.Encounter) > dbo.f_Last_of_Week(dateadd(day, -7, getdate()))
						then dbo.f_Last_of_Week(dateadd(day, -7, getdate()))
						else (select max(DischargeDate) from Warehouse.Medisec.Encounter) 
					end
								
else if @daterange = 'Medisec YTD'
	select [Start] = dbo.f_First_of_Financial_Year(getdate())
			,[End] = case
						when (select max(DischargeDate) from Warehouse.Medisec.Encounter) > dbo.f_Last_of_Week(dateadd(day, -7, getdate()))
						then dbo.f_Last_of_Week(dateadd(day, -7, getdate()))
						else (select max(DischargeDate) from Warehouse.Medisec.Encounter) 
					end
--(select max(DischargeDate) from Warehouse.Medisec.Encounter)
			
else if @daterange = 'Medisec Last 6 Months'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -6, getdate()))
			,[End] = case
						when (select max(DischargeDate) from Warehouse.Medisec.Encounter) > dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
						then dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
						else (select max(DischargeDate) from Warehouse.Medisec.Encounter) 
					end 
	
else if @daterange = 'Medisec Last Month'
	select [Start] = dbo.f_First_of_Month(dateadd(month, -1, getdate()))
			,[End] = case
						when (select max(DischargeDate) from Warehouse.Medisec.Encounter) > dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
						then dbo.f_Last_of_Month(dateadd(month, -1, getdate()))
						else (select max(DischargeDate) from Warehouse.Medisec.Encounter) 
					end 

else if @daterange = 'Medisec Calendar YTD'
	select [Start] = cast('1 jan ' +  cast(year(getdate()) as char(4)) as datetime)
			,[End] = (select max(DischargeDate) from Warehouse.Medisec.Encounter)
					
else if @daterange = 'Portal Mortality'
	select [Start] = cast('21 may 2012' as datetime)
			,[End] = dateadd(dd, datediff(dd, 0, getdate()), -1)



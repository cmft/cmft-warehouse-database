﻿

	CREATE proc [RPT].[CQUINPDementiaLiveUpdate] 

	
	 @StartDate as datetime = null
	,@EndDate as datetime = null
	,@Divsion as varchar (50) = null
	,@CurrentlyAdmitted varchar (1) = null
	,@MeasureType varchar (20) = null


	
	--Debug
	,@PatientID varchar (20) = null
	,@UnifyOnly varchar (1) = null


	as

		declare @SetStartDate datetime = coalesce(@StartDate,(SELECT CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(getdate())-1),getdate()),106) ))
		
		declare @SetEnddate datetime = coalesce(@EndDate,(select DATEADD(day, DATEDIFF(day,0,GETDATE()), 0))) 


		select 
		
		 [Known Dementia] 
		,[Find] 
		,[Assess] 
		,[AMTS Score]
		,[Clinical Reason] 
		,[Investigate] 
		,[Clinical Indications] 
		,[Refer] 
		,[Division]
		,[Directorate] 
		,[Ward]
		,[DistricNo]
		,[Patient Forname] 
		,[Patient Surname] 
		,[Admission Date]
		,[Discharge Date]
		,[LOS (Hours)]
		--Count Section
		,FindDenominator 
		,FindNumerator 
		,AssessedDenominator 
		,AssessedNumerator 
		,ReferDenominator 
		,ReferNumerator
		,AntipsychoticDenominator
		,AntipsychoticNumerator
		--Count Section for Unifiy
		,FindDenominator72H  
			--case
			--	when FindDenominator72H <> 1 then 0 else FindDenominator72H end 
		,FindNumerator72H 
		,AssessedDenominator72H 
			--case
			--	when AssessedNumerator72H <> 1 then 0 else AssessedDenominator72H end
		,AssessedNumerator72H 
		,ReferDenominator72H 
		,ReferNumerator72H1 
		,AntipsychoticDenominator72 			
		,AntipsychoticNumerator72 
		,providerspellno
		,rundate 
		,CurrentLos 
		,SourcePatientNo
		,DirectorateID
		,AMTTestNotPerformed


		
		
		 from (

	select
	
		 
		 [Known Dementia] = 
		 
					Case 
						when KnownToHaveDementia = 1 then 'BLUE' 
					--	when PatientDementiaFlag = 1 then 'DARK BLUE'
						else '' 
					end
		,[Find] = 

					Case 

						when KnownToHaveDementia = 1 then 'Green' 
						when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H <= 72 and PatientClinicalIndicationsPresent =1 then   'Dark Green'
						when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H > 72 then 'Purple'
						when FindNumerator = 1  and FindWithin72H <= 72 then 'Green'
						when FindNumerator = 1  and FindWithin72H > 72 then 'Purple'
						when FindNumerator = 0  and CurrentLos > 72 then 'Dark Purple'
						else 'Orange' end

	--	,[FindAnswer] = coalesce(PatientClinicalIndicationsPresent,PatientForgetfulnessQuestionAnswered)
				
		,[Assess] = 

					case

							--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149748') then 'White'

						--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149767','A149065') then 'Green'

						when KnownToHaveDementia = 1 then 'White'
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore is null and AMTTestNotPerformedReason is not null then 'Brown'
						when PatientClinicalIndicationsPresent = 1 and PatientAMTScore is null then 'Orange'
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore is null then 'Orange'
						when KnownToHaveDementia = 0 and PatientClinicalIndicationsPresent = 1 and PatientAMTScore > 7 then 'Green'
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore > 7 then 'Green'
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted = 'Y' then 'Green'
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted = 'N' then 'Orange'
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted is null then 'Orange'
						

									

						else 'White'
					end


			
		,[AMTS Score] = 
					case
						when KnownToHaveDementia = 1 then null
						else PatientAMTScore
					end 
		,[Clinical Reason] = AMTTestNotPerformedReason
		,[Investigate] = 
					case
						when KnownToHaveDementia = 1 then 'N/A'
						when CognitiveInvestigationStarted = 'Y' then 'Green'
						when CognitiveInvestigationStarted = 'N' or PatientAMTScore <=7 and CognitiveInvestigationStarted is null then 'Orange'
						else 'N/A'
					end

		,[Clinical Indications] = 
		
					case 
						when  KnownToHaveDementia = 1 then 'N/A'
						when PatientClinicalIndicationsPresent = 0 then 'No'
						when PatientClinicalIndicationsPresent = 1 then 'Yes'
						else 'N/A'
					end
		,[Refer] = 
					case
						when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.DenominatorRemove = 1
								) then 'N/A'

						when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.NumeratorInclude = 1
								) then 'Referred'

						--when DistrictNo in ('00077782','00029986','00239087','00817420') and datepart(year,AdmissionDate) = '2013' and datepart(month,AdmissionDate) = '01' then 'N/A'
						--when DistrictNo in ('00479790','02589338','03200500','01604717','01925196','00498672','01466055','886911','884523','768674','781381','11080','01727907','03368457','03368457') and datepart(year,AdmissionDate) = '2013' and datepart(month,AdmissionDate) = '01' then 'Referred'
						when PatientAMTScore <= 7 and ReferralNeeded = 1 then 'Referred'
						when KnownToHaveDementia = 1 then 'N/A'
						when PatientAMTScore <= 7 then 'Yes'
						else 'N/A'
					end

		,[Division] = Division
		,[Directorate] = Directorate
		,[Ward] = left(Ward,15)
		,[DistricNo] = DistrictNo
		,[Patient Forname] = PatientForename
		,[Patient Surname] = PatientSurname
		,[Admission Date] = AdmissionDate
		,[Discharge Date] = DischargeDate
		--,[Expected Date of Discharge] = ExpectedDateofDischarge
		--,[EXPLos (Days)] = LOSDays
		,[LOS (Hours)] = CurrentLos


		--Count Section

		,FindDenominator  = 1
		,FindNumerator  = 


							Case 

						when KnownToHaveDementia = 1 then 1 
						when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H <= 72 and PatientClinicalIndicationsPresent =1 then   1
						when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H > 72 then 0
						when FindNumerator = 1  and FindWithin72H <= 72 then 1
						when FindNumerator = 1  and FindWithin72H > 72 then 0
						else 0 end


					--Case 
					--	when KnownToHaveDementia = 1 then 1
					--	when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H <= 72 then 1
					--	when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H > 72 then 0
					--	when FindNumerator = 1  and FindWithin72H <= 72 then 1
					--	when FindNumerator = 1  and FindWithin72H > 72 then 0
					--else 0 end

		,AssessedDenominator = 

					case

							--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149748') then 0

						when AMTTestNotPerformedReason is not null then 0
						when KnownToHaveDementia = 1 then 0
						when KnownToHaveDementia = 0 and PatientClinicalIndicationsPresent = 1  then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 then 1 
						else 0
					end
		,AssessedNumerator =

					case

							--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149748') then 0

						--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149767','A149065','A149748') then 1

						when AMTTestNotPerformedReason is not null then 0
						when KnownToHaveDementia = 1 then 0						 
						when KnownToHaveDementia = 0 and PatientClinicalIndicationsPresent = 1 and PatientAMTScore > 7 then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore > 7 then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted = 'Y' then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted = 'N' then 0
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted is null then 1

						else 0
					end
		,ReferDenominator =

					case
						when KnownToHaveDementia = 1 then 0
						when PatientAMTScore <= 7 then 1
						else 0
					end

		,ReferNumerator = 

					case
						when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.DenominatorRemove = 1
								) then 0

							when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.NumeratorInclude = 1
								) then 1

						
						when KnownToHaveDementia = 1 then 0
						when  CurrentLos < 72 then 0
						when PatientAMTScore <= 7 and ReferralNeeded = 1 then 1
						when PatientAMTScore <= 7 and DementiaReferralSentDate is not null then 1
					else 0
					end

		,AntipsychoticDenominator

		,AntipsychoticNumerator = 0

		--Count Section for Unifiy


		,FindDenominator72H  = 
				
					case
						when CurrentLos >=  72 then 1
						else 0
					end

		,FindNumerator72H  = 

					Case 
						when  CurrentLos < 72 then 0
						when KnownToHaveDementia = 1 then 1
						when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H <= 72 then 1
						when PatientClinicalIndicationsPresent in (1,0) and FindWithin72H > 72 then 0
						when FindNumerator = 1  and FindWithin72H <= 72 then 1
						when FindNumerator = 1  and FindWithin72H > 72 then 0
					else 0 end


		,AssessedDenominator72H = 

					case
						--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149748') then 0

						when AMTTestNotPerformedReason is not null then 0
						when KnownToHaveDementia = 1 then 0
						when  CurrentLos < 72 then 0 
						when KnownToHaveDementia = 0 and PatientClinicalIndicationsPresent = 1  then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 then 1 
						else 0
					end

		,AssessedNumerator72H =

					case

						--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149748') then 0

						--overide due to Trafford form changes required see email from Lisa Elliot
						when providerspellno in ('A149767','A149065','A149748') then 1

						when AMTTestNotPerformedReason is not null then 0
						when KnownToHaveDementia = 1 then 0
						when  CurrentLos < 72 then 0 
						when KnownToHaveDementia = 0 and PatientClinicalIndicationsPresent = 1 and PatientAMTScore > 7 then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore > 7 then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted = 'Y' then 1
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted = 'N' then 0
						when KnownToHaveDementia = 0 and PatientForgetfulnessQuestionAnswered = 1 and PatientAMTScore <= 7 and CognitiveInvestigationStarted is null then 1

						else 0
					end
		,ReferDenominator72H =

					case

							when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.DenominatorRemove = 1
								) then 0

							when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.NumeratorInclude = 1
								) then 1

					--	when DistrictNo in ('00077782','00029986','00239087','00817420') and datepart(year,AdmissionDate) = '2013' and datepart(month,AdmissionDate) = '01' then 0
					--	when DistrictNo in ('00479790','02589338','03200500','01604717','01925196','00498672','01466055','886911','884523','768674','781381','11080','01727907','03368457','03368457') and datepart(year,AdmissionDate) = '2013' and datepart(month,AdmissionDate) = '01' then 1
						when KnownToHaveDementia = 1 then 0
						when  CurrentLos < 72 then 0
						when PatientAMTScore <= 7 then 1
						else 0
					end

		,ReferNumerator72H1 =

					case
						--when DistrictNo in ('00479790','02589338','03200500','01604717','01925196','00498672','01466055','886911','884523','768674','781381','11080','01727907','03368457','03368457') and datepart(year,AdmissionDate) = '2013' and datepart(month,AdmissionDate) = '01' then 1
							when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.DenominatorRemove = 1
								) then 0

							when 

							exists

								(
								select 1 from [Warehouse].[APC].[DementiaOverrideMeasure] DementiaOverrideMeasure
								where DementiaOverrideMeasure.DistrictNo = DementiaAssessment.DistrictNo
								and DementiaOverrideMeasure.MeasureType = 'REF'
								and DementiaOverrideMeasure.SubmissionMonth = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(AdmissionDate)-1),AdmissionDate),101) 
								and DementiaOverrideMeasure.NumeratorInclude = 1
								) then 1

						
						when KnownToHaveDementia = 1 then 0
						when  CurrentLos < 72 then 0
						when PatientAMTScore <= 7 and ReferralNeeded = 1 then 1
						when PatientAMTScore <= 7 and DementiaReferralSentDate is not null then 1
						else 0
					end

			,AntipsychoticDenominator72  =

				case 
					when  CurrentLos >= 72 then AntipsychoticDenominator
					else 0
				end

			,AntipsychoticNumerator72 = 0

			,providerspellno
			,rundate 
			,CurrentLos 
			,SourcePatientNo
			,DirectorateID
			,AMTTestNotPerformed

		--	into [RPT].[DementiaActivity]
		
		from 
		
			(
			
			select
						 Encounter.SourcePatientNo
						,Encounter.DistrictNo

						,Encounter.PatientSurname
						,Encounter.PatientForename			

						,AdmissionDate =
							AdmissionDate.TheDate
		
						,Encounter.AdmissionTime

						,DischargeDate =
							Encounter.DischargeDate
						,Encounter.DischargeTime
						,Encounter.ExpectedDateofDischarge

						,Division = Directorate.Division
							--CASE Directorate.Division 
							--WHEN 'Trafford' 
							--THEN 'Trafford' 
							--ELSE 'Central' 
							--END 

						,Directorate.Directorate

						,LOSDays = DATEDIFF(Day , Encounter.AdmissionTime , Encounter.ExpectedDateofDischarge )
						,CurrentLos = DATEDIFF(HH , Encounter.AdmissionTime ,coalesce(Encounter.DischargeTime, getdate ()) )

							
						,Ward = 
							
						case 
							when Directorate.Division = 'Trafford' then (Select Code from TraffordWarehouse.dbo.CurrentWardAdmission CurrentWardAdmission where CurrentWardAdmission.ProviderSpellNo = Encounter.ProviderSpellNo )
							when Directorate.Division = 'Trafford' and  Encounter.DischargeTime is not null then EndWard.SourceWardCode
							when Directorate.Division <> 'Trafford' then (Select top 1 SourceWardCode from WarehouseReportingMerged.RPT.DementiaTable D where d.ProviderSpellNo = Encounter.ProviderSpellNo order by LastUpdated_TS desc)
						else 'Unknown' 
						end
						
						,KnownToHaveDementia = coalesce(DementiaAssessmentLast.KnownToHaveDementia,0) 

						,FindDenominator = 
										case
											when  DATEDIFF(HOUR , Encounter.AdmissionTime , coalesce(Encounter.DischargeTime , getdate())) >= 72 then 1
											else 0
										end
						,FindNumerator =

						--New
							case
								when	
								
								--DATEDIFF(HOUR , Encounter.AdmissionTime , coalesce(Encounter.DischargeTime , getdate())) >= 72 
										-- DATEDIFF(HOUR , AdmissionTime , DementiaAssessmentFirst.LastUpdated_TS ) <= 72  
										--and
											(
										DementiaAssessmentFirst.PatientForgetfulnessQuestionAnswered in (1,0)
											or
										DementiaAssessmentLast.KnownToHaveDementia = 1 and  DementiaAssessmentFirst.PatientForgetfulnessQuestionAnswered is null
										--	or
										--DementiaAssessmentLast.PatientDementiaFlag = 1 and  DementiaAssessmentFirst.PatientForgetfulnessQuestionAnswered is null
											)
										 then 1

								else 0
							end
						,PatientForgetfulnessQuestionAnswered = coalesce(DementiaAssessmentLast.PatientClinicalIndicationsPresent,DementiaAssessmentLast.PatientForgetfulnessQuestionAnswered,DementiaAssessmentFirst.PatientForgetfulnessQuestionAnswered)
						,DementiaAssessmentLast.PatientAMTScore 
						,CognitiveInvestigationStarted = DementiaAssessmentLast.CognitiveInvestigationStarted
						,AMTTestNotPerformedReason = 
						
								case 
									when DementiaAssessmentLast.AMTTestNotPerformedReason = 'Time' then null 
									when DementiaAssessmentLast.AMTTestNotPerformedReason = 'Time constraints' then null --Trafford
									when DementiaAssessmentFirst.PatientClinicalIndicationsPresent = 1 and DementiaAssessmentLast.PatientAMTScore is null then  DementiaAssessmentLast.AMTTestNotPerformedReason 
									When DementiaAssessmentLast.PatientForgetfulnessQuestionAnswered = 0 then null
									when DementiaAssessmentLast.PatientAMTScore is not null then null
									else DementiaAssessmentLast.AMTTestNotPerformedReason end
					
					,AMTTestNotPerformed = DementiaAssessmentLast.AMTTestNotPerformedReason

						,DementiaAssessmentLast.PatientClinicalIndicationsPresent
						,DementiaAssessmentLast.PatientDementiaFlag

						,DementiaAssessmentProviderSpellNo = DementiaAssessmentFirst.ProviderSpellNo
						,Encounter.ProviderSpellNo
						
						,ForgetfulnessQuestionAnsweredFirst = DementiaAssessmentFirst.ForgetfulnessQuestionAnswered
						,ForgetfulnessQuestionAnsweredLast = DementiaAssessmentLast.ForgetfulnessQuestionAnswered

						,FindWithin72H = DATEDIFF(HOUR , Encounter.AdmissionTime , DementiaAssessmentFirst.LastUpdated_TS )

						,ReferralNeeded =

								case
									WHEN Directorate.Division = 'Trafford' then ReferralNeeded
									else null
								end
						,Rundate = (select max(RunDate) from RPT.DementiaTable)
						,Directorate.DirectorateID
						,DementiaAssessmentLast.DementiaReferralSentDate

						,AntipsychoticDenominator =

									case
										when DementiaAssessmentLast.PsychoticMedsReview = 1 then 1 else 0
									end
			
						FROM
						WarehouseReportingMerged.APC.Encounter

						INNER JOIN WarehouseReportingMerged.WH.Calendar AdmissionDate
						ON	Encounter.AdmissionDateID = AdmissionDate.DateID

						LEFT JOIN WarehouseReportingMerged.WH.Calendar DischargeDate
						ON	Encounter.DischargeDateID = DischargeDate.DateID 
						
						left join WarehouseReportingMerged.APC.Encounter LastEncounter
						on LastEncounter.ProviderSpellNo = Encounter.ProviderSpellNo
						and LastEncounter.SourcePatientNo = Encounter.SourcePatientNo
						and LastEncounter.NationalLastEpisodeInSpellIndicator = '1'
							--exists
							--(select 1
							--  FROM [APC].[LastEpisodeInSpell] LastEpisodeInSpell

							--  where [NationalLastEpisodeInSpellCode] = '1'
						 --and	LastEncounter.LastEpisodeInSpellID = LastEpisodeInSpell.SourceLastEpisodeInSpellID)

						left join WarehouseReportingMerged.wh.Ward EndWard
						on EndWard.SourceWardID = LastEncounter.EndWardID
						and EndWard.SourceContextCode = LastEncounter.ContextCode

						left JOIN WarehouseReportingMerged.WH.Directorate
						ON	Encounter.StartDirectorateCode = Directorate.DirectorateCode

						INNER JOIN WarehouseReportingMerged.APC.PatientClassification
						ON	Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID 


						INNER JOIN WarehouseReportingMerged.APC.AdmissionMethod
						ON	Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID   

						left join WarehouseReportingMerged.APC.DischargeMethod
						on DischargeMethod.SourceDischargeMethodID = Encounter.DischargeMethodID


						

					LEFT JOIN 
					(
					SELECT 
						LastUpdated_TS = (LastUpdated_TS)

						,ForgetfulnessQuestionAnswered=(ForgetfulnessQuestionAnswered)
						,ProviderSpellNo = ProviderSpellNo

						,PatientForgetfulnessQuestionAnswered = (ForgetfulnessQuestionAnswered)
						,PatientClinicalIndicationsPresent = (ClinicalIndicationsPresent)
			
						,AMTTestNotPerformedReason = (AMTTestNotPerformedReason)
	--			
						,FirstRecord = row_number() over (PARTITION BY ProviderSpellNo order by LastUpdated_TS asc )
						,SourceWardCode
						
						,RunDate
					FROM
						RPT.DementiaTable
					 where ForgetfulnessQuestionAnswered in (1,0)
						) DementiaAssessmentFirst
				ON	DementiaAssessmentFirst.ProviderSpellNo = Encounter.ProviderSpellNo  
				and DementiaAssessmentFirst.FirstRecord = 1



				LEFT JOIN 
					(
					SELECT 
						LastUpdated_TS = (LastUpdated_TS)
						,KnownToHaveDementia = (KnownToHaveDementia)
						,RememberMePlan=(RememberMePlan)
						,RememberMePlanCommenced = (RememberMePlanCommenced)
						,RememberMePlanDiscontinued = (RememberMePlanDiscontinued)
						,ForgetfulnessQuestionAnswered=(ForgetfulnessQuestionAnswered)
						,ProviderSpellNo = ProviderSpellNo
						,PatientKnownToHaveDementia	= (KnownToHaveDementia)
						,PatientDementiaFlag = (PatientDementiaFlag)
						,PatientForgetfulnessQuestionAnswered = (ForgetfulnessQuestionAnswered)
						,PatientClinicalIndicationsPresent = (ClinicalIndicationsPresent)
						,PatientAMTTestPerformed = (AMTTestPerformed)
						,PatientAMTScore = (AMTScore)
						,AMTTestPerformed = (AMTTestPerformed)
						,ClinicalIndicationsPresent = (ClinicalIndicationsPresent)
						,AMTScore = (AMTScore)
						,ReferralNeeded = (ReferralNeeded)
						,DementiaReferralSentDate
						,PsychoticMedsReview = (PsychoticMedsReview)
						,PsychoticReasonPresent = (PsychoticReasonPresent)
						,AMTTestNotPerformedReason = (AMTTestNotPerformedReason)
						,CognitiveInvestigationStarted = (CognitiveInvestigationStarted)
						,LastRecord = row_number() over (PARTITION BY ProviderSpellNo order by LastUpdated_TS desc )
					FROM
						RPT.DementiaTable
					
						) DementiaAssessmentLast
				ON	DementiaAssessmentLast.ProviderSpellNo = Encounter.ProviderSpellNo  
				and DementiaAssessmentLast.LastRecord = 1

	
				
				

							WHERE

								AdmissionDate.TheDate BETWEEN @SetStartDate AND @SetEnddate
					
								AND Encounter.FirstEpisodeInSpellIndicator = 1

								and ( DischargeMethod.NationalDischargeMethodCode is null or  DischargeMethod.NationalDischargeMethodCode not in ('4','5'))

								and WarehouseReportingMerged.dbo.f_GetAge(Encounter.DateOfBirth,AdmissionDate.TheDate) >= 75

								and AdmissionMethod.AdmissionType = 'Emergency'

								and
									(

							   DATEDIFF(HOUR , Encounter.AdmissionTime , Encounter.DischargeTime ) >= 72
									or
							   Encounter.DischargeTime is null  

								--and
								--DATEDIFF(HOUR , Encounter.AdmissionTime , coalesce(Encounter.DischargeTime , getdate())) <= 72
									)
								AND NationalAdmissionMethodCode <> '81' -- Exclude Transfers

								AND PatientClassification.NationalPatientClassificationCode NOT IN ('3','4') -- Exclude Regular admissions 

								and 
									 Encounter.DateOfDeath is null  


				and

						(

						@UnifyOnly = 'Y'
						and
						 DischargeDate.TheDate is not null

						or

						@UnifyOnly is null

						)


					
					) DementiaAssessment

					where
					
						(


						DistrictNo = @PatientID
						or
						@PatientID is null
												
						)
						
					) Data	

					where
					
					
						(
						@CurrentlyAdmitted = 'Y'
						and [Discharge Date] is null
						or
						@CurrentlyAdmitted is null
						)
						and
						(
						@Divsion = Division
						or
						@Divsion is null
						)
						and

						(
						@MeasureType is null
						or
						@MeasureType = 'Find'
						and 
						FindNumerator = 0
						or 
						@MeasureType = 'Assess'
						and Assess in ('Orange','Brown')
						or 
						@MeasureType = 'Investigate'
						and Investigate = 'Orange'
						or
						@MeasureType = 'Refer'
						and ReferNumerator72H1 = 0 
						and ReferDenominator72H = 1
						--Refer = 'Yes'
						or
						@MeasureType = 'Known Dementia'
						and [Known Dementia] = 'Blue'
						
						or
						@MeasureType = 'Antipsychotic'
						and 
						AntipsychoticDenominator = 1
						)

						--(

						--@UnifyOnly = 'Y'
						----and
						-- AdmissionDate is not null

						--or

						--@UnifyOnly is null

						--)

						--order by 

						--  Rundate desc
						-- , Division
						-- ,CurrentLos desc
						
		



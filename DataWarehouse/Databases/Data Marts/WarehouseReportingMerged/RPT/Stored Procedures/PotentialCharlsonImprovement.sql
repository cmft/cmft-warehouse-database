﻿

CREATE proc RPT.PotentialCharlsonImprovement

(
	@Start date
	,@End date
)

as

/* temp table for performance reasons (data from xref table with self joins) */

if object_id('tempdb..#Charlson') is not null
drop table #Charlson

create table #Charlson

(
	DiagnosisCode varchar(10) primary key
	,CharlsonConditionCode int
	,CharlsonCondition varchar(50)
	,NewWeight int
)

insert into #Charlson

select
	DiagnosisCode
	,CharlsonConditionCode
	,CharlsonCondition
	,NewWeight
from
	WarehouseReporting.WH.CharlsonIndex	


select
	Division
	,Specialty
	,Consultant
	,CasenoteNumber
	,PatientAge
	,ProviderSpellNo
	,SourceEncounterNo
	,AdmissionTime
	,DischargeTime
	,PrimaryDiagnosisCode
	,DiagnosisGroup
	,CharlsonIndex
	,Charlson1 = [1]
	,Charlson2 = [2]
	,Charlson3 = [3]
	,Charlson4 = [4]
	,Charlson5 = [5]
	,Charlson6 = [6]
	,Charlson7 = [7]
	,Charlson8 = [8]
	,Charlson9 = [9]
	,Charlson10 = [10]
from
	(

select 
	Division
	,Specialty = Specialty.NationalSpecialtyLabel
	,Consultant = Consultant.SourceConsultantCode + ' - ' + Consultant.SourceConsultant
	,CasenoteNumber
	,PatientAge = floor(datediff(day, DateOfBirth, Encounter.AdmissionDate)/365.25)
	,ProviderSpellNo
	,SourceEncounterNo
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.PrimaryDiagnosisCode
	,DiagnosisGroup = DiagnosisGroup.Description
	,CharlsonIndex = coalesce(CharlsonIndex.CharlsonIndex,0)
	,PreviousCharlson = PreviousDiagnosisSpell.DiagnosisCode  + ' - ' + PreviousDiagnosisSpell.CharlsonIndex + ' - ' + cast(cast(PreviousDiagnosisSpell.AdmissionDate as date) as varchar)
	,SequenceNo = row_number() over (partition by Encounter.ContextCode, ProviderSpellNo, SourceEncounterNo order by Encounter.SourceEncounterNo desc, PreviousDiagnosisSpell.AdmissionDate desc)
from
	APC.Encounter Encounter

inner join
			(
			select
				Diagnosis.DiagnosisCode
				,CharlsonConditionCode
				,CharlsonIndex = cast(CharlsonConditionCode as varchar) + ' - ' + CharlsonCondition
				,Encounter.SourcePatientNo
				,AdmissionDate = max(AdmissionDate)
				,Encounter.ContextCode
			from
				APC.Diagnosis Diagnosis

			inner join 
				APC.Encounter Encounter
			on	Diagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno

			inner join 
				#Charlson CharlsonIndex
			on	CharlsonIndex.DiagnosisCode = Diagnosis.DiagnosisCode

			where
				SequenceNo > 0
				
			group by
				Diagnosis.DiagnosisCode
				,CharlsonConditionCode
				,cast(CharlsonConditionCode as varchar) + ' - ' + CharlsonCondition
				,Encounter.SourcePatientNo
				,Encounter.ContextCode
						
			) PreviousDiagnosisSpell

			on	PreviousDiagnosisSpell.SourcePatientNo = Encounter.SourcePatientNo
			and PreviousDiagnosisSpell.AdmissionDate < Encounter.AdmissionDate
			and PreviousDiagnosisSpell.ContextCode = Encounter.ContextCode

inner join
	WH.Directorate Directorate
	on EndDirectorateCode = Directorate.DirectorateCode

inner join
	WH.Specialty Specialty
on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID

inner join
	WH.Consultant Consultant
on	Consultant.SourceConsultantID = Encounter.ConsultantID

inner join
	APC.PatientClassification PatientClassification
on	PatientClassification.SourcePatientClassificationID = Encounter.PatientClassificationID

left outer join 
	Warehouse.dbo.EntityXref ClinicalClassification
on	Encounter.PrimaryDiagnosisCode = ClinicalClassification.EntityCode
and ClinicalClassification.EntityTypeCode = 'DIAGNOSISCODE'
and ClinicalClassification.XrefEntityTypeCode = 'CCSCATEGORYCODE'

left outer join
	Warehouse.dbo.EntityLookup DiagnosisGroup
on	ClinicalClassification.XrefEntityCode = DiagnosisGroup.EntityCode
and DiagnosisGroup.EntityTypeCode = 'CCSCATEGORYCODE'

left outer join
			(
			select
				Diagnosis.MergeEncounterRecno
				,CharlsonIndex = sum(cast(Charlson.NewWeight as int))
			from
				APC.Diagnosis

			inner join Warehouse.WH.CharlsonIndex Charlson
			on	left(Diagnosis.DiagnosisCode, 6) = Charlson.DiagnosisCode

			where
				Diagnosis.SequenceNo > 0
			and	not exists
						(
						select
							1
						from
							APC.Diagnosis DiagnosisLater

						inner join Warehouse.WH.CharlsonIndex CharlsonLater
						on	left(DiagnosisLater.DiagnosisCode, 6) = CharlsonLater.DiagnosisCode
						
						where
							DiagnosisLater.SequenceNo > Diagnosis.SequenceNo	
						and DiagnosisLater.MergeEncounterRecno = Diagnosis.MergeEncounterRecno
						and CharlsonLater.CharlsonConditionCode = Charlson.CharlsonConditionCode
							
						)
			group by
				MergeEncounterRecno
				
			) CharlsonIndex
			on	CharlsonIndex.MergeEncounterRecno = Encounter.MergeEncounterRecno

where
	PreviousDiagnosisSpell.CharlsonConditionCode not in
														(
														select
															CharlsonIndex.CharlsonConditionCode
														from
															APC.Diagnosis Diagnosis

														inner join 
															APC.Encounter CurrentDiagnosisSpell
														on	Diagnosis.MergeEncounterRecno = CurrentDiagnosisSpell.MergeEncounterRecno

														inner join 
															#Charlson CharlsonIndex
														on	CharlsonIndex.DiagnosisCode = Diagnosis.DiagnosisCode

														where 
															CurrentDiagnosisSpell.ProviderSpellNo = Encounter.ProviderSpellNo
														and CurrentDiagnosisSpell.ContextCode = Encounter.ContextCode

														)
and
	DischargeDate between @Start and @End
and
	ClinicalCodingCompleteDate is not null
and
	NationalPatientClassificationCode not in ('2','3','4')
--and
--	ProviderSpellNo = 'A146682'

) Charlson


pivot

(


	min(PreviousCharlson) for SequenceNo in
									(
									[1]
									,[2]
									,[3]
									,[4]
									,[5]
									,[6]
									,[7]
									,[8]
									,[9]
									,[10]
									)
) diagnosispivot


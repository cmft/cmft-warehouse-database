﻿




CREATE PROCEDURE [RPT].[GetBurnsAndPlasticPatients] 

  @FromDate   AS Date  
 ,@ToDate     AS Date  
 
--DECLARE @FromDate AS DATE        = '24-Sep-2012'
--DECLARE @ToDate   AS DATE        = '30-Sep-2012'

AS

SELECT
       [DistrictNo]
      ,[PatientForename]
      ,[PatientSurname]
      ,[CasenoteNumber] 
      ,[AdmissionTime]  
      ,[SourceConsultantCode]       
      ,[NationalSpecialtyCode] 
      ,[SourceSpecialtyCode]     
      ,[NationalSpecialty]
      ,[LocalSpecialty]
      ,[FirstEpisodeInSpellIndicator]
      --,[LastEpisodeInSpellID]
      
  FROM [APC].[Encounter]
  
  INNER JOIN [WH].[Calendar]
  ON [AdmissionDateID] = [DateID]
  
  INNER JOIN [WH].[Specialty] 
  ON [SpecialtyID] = [SourceSpecialtyID]
  
  INNER JOIN [WH].[Consultant]
  ON [ConsultantID] = [SourceConsultantID]
  
  WHERE  [NationalSpecialtyCode] in ('160','219','220')
  AND [TheDate] BETWEEN @FromDate AND @ToDate
  




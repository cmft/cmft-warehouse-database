﻿

CREATE procedure [RPT].[GetEnvoyFFTBirthPatient]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	[RPT].[EnvoyFFTBirthPatient] target
using
	(
	select
		DistrictNo
		,MergeEncounterRecno
		,HospitalDischargeDate
		,HospitalDischargeTime
		,HealthVisitorDischargeDate
		,Location
		,MobileNumber
		,LandlineNumber
		,Title
		,FirstName
		,LastName
		,Address1
		,Address2
		,Address3
		,Address4
		,Postcode	
		,ContextCode
	from
		(	
		select
			[Spell].[DistrictNo]
			,[Spell].[MergeEncounterRecno]
			,HospitalDischargeDate = cast([Spell].[DischargeDate] as date)
			,HospitalDischargeTime = left(cast([Spell].[DischargeTime] as time),5)
			,HealthVisitorDischargeDate = cast(Birth.DischargedToHvDate as date)
			,Location = ('RW3|RW3SM' + '|'+ [Spell].[EndWardTypeCode])
			,MobileNumber = left(
									case
									when left([Patient].HomePhone , 2) = '07' then replace ([Patient].HomePhone,' ','') 
									when left([Patient].MobilePhone , 2)  = '07' then replace ([Patient].MobilePhone,' ','') 
									when left([Patient].WorkPhone , 2)  = '07' then replace ([Patient].WorkPhone  ,' ','')          
									else null
									end
									,11)
			,LandlineNumber = [Patient].HomePhone
			,Title = [Patient].Title
			,FirstName = [Patient].Forenames
			,LastName = [Patient].Surname
			,Address1 = coalesce([Patient].AddressLine1,'')
			,Address2 = coalesce([Patient].AddressLine2,'')
			,Address3 = coalesce([Patient].AddressLine3,'')
			,Address4 = coalesce([Patient].AddressLine4,'')
			,[Patient].Postcode	
			,Spell.ContextCode	

		from 
			[WarehouseReportingMerged].[APC].[Encounter] Spell

		inner join  
			[Warehouse].[PAS].[Patient]
		on	[Patient].[SourcePatientNo] = [Spell].[SourcePatientNo]

		inner join
			[WarehouseReportingMerged].[Maternity].[Birth]
		on	[Birth].[MotherNo] = [Spell].[CasenoteNumber]
		and [Birth].[InfantDateOfBirth] between [Spell].[AdmissionDate] and [Spell].[DischargeDate]
			
		where
			[Spell].[SpecialtyCode] = 'OBS'
		and [Birth].[Outcome] = 'L' --Live Birth
		and [Birth].[BirthOrder] = '1' --1st child in outcome
		and [Spell].[SpellHRGCode] like 'NZ1%' --birth related
		and ([Spell].[DateOfDeath] is null or [Spell].[DateOfDeath] > [Spell].[DischargeDate]) --excludes mothers that died during the spell
		and Birth.DischargedToHvDate >= '1 jan 2015' --Started reporting from
		and not exists (
						--select
						--	1
						--from
						--	[WarehouseReportingMerged].[Neonatal].[Patient]
						--where
						--	[Patient].[NHSNumber] = [Birth].[InfantNhsNumber]  --CH 22/12/2014 this is being removed when the new Neonatal objects go live, New objects could be used when go live
						select
							1
						from
							[Bnet_DSS_business].[bnf_dbsync].[NNUEpisodes]
						where
							[NNUEpisodes].[NationalIDBaby] = [Birth].[InfantNhsNumber] 
						)
		) Discharges
	
	where
		len([MobileNumber]) = 11  --removes nulls and short numbers
					
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno	
	and	source.ContextCode = target.ContextCode
	
	--when not matched by source
	--then delete 
	--Removed this because if something changed about the patient details then we didnt wnat the record deleting

	when not matched
	then
		insert
			(
			DistrictNo
			,MergeEncounterRecno
			,HospitalDischargeDate
			,HospitalDischargeTime
			,HealthVisitorDischargeDate
			,Location
			,Mobilenumber
			,Landlinenumber
			,Title
			,FirstName
			,LastName
			,Address1
			,Address2
			,Address3
			,Address4
			,Postcode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			DistrictNo
			,MergeEncounterRecno
			,HospitalDischargeDate
			,HospitalDischargeTime
			,HealthVisitorDischargeDate
			,Location
			,MobileNumber
			,LandlineNumber
			,Title
			,FirstName
			,LastName
			,Address1
			,Address2
			,Address3
			,Address4
			,Postcode
			,ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)
			
	when matched
	and not
		(
			isnull(target.DistrictNo, 0) = isnull(source.DistrictNo, 0)
		and	isnull(target.MergeEncounterRecno, 0) = isnull(source.MergeEncounterRecno, 0)
		and isnull(target.HospitalDischargeDate, getdate()) = isnull(source.HospitalDischargeDate, getdate())
		and	isnull(target.HospitalDischargeTime, 0) = isnull(source.HospitalDischargeTime, 0)
		and isnull(target.HealthVisitorDischargeDate, getdate()) = isnull(source.HealthVisitorDischargeDate, getdate())
		and	isnull(target.Location, 0) = isnull(source.Location, 0)
		and	isnull(target.MobileNumber, 0) = isnull(source.MobileNumber, 0)
		and	isnull(target.LandlineNumber, 0) = isnull(source.LandlineNumber, 0)	
		and	isnull(target.Title, 0) = isnull(source.Title, 0)
		and	isnull(target.FirstName, 0) = isnull(source.FirstName, 0)
		and	isnull(target.LastName, 0) = isnull(source.LastName, 0)
		and	isnull(target.Address1, 0) = isnull(source.Address1, 0)
		and	isnull(target.Address2, 0) = isnull(source.Address2, 0)		
		and	isnull(target.Address3, 0) = isnull(source.Address3, 0)	
		and	isnull(target.Address4, 0) = isnull(source.Address4, 0)		
		and	isnull(target.Postcode, 0) = isnull(source.Postcode, 0)	
		and	isnull(target.ContextCode, 0) = isnull(source.ContextCode, 0)						
		)
	then
		update --Removed this because if something changed about the patient details then we didnt want the record updated, just a way of knowing it would have been updated
		set
	--		target.DistrictNo = source.DistrictNo
	--		,target.MergeEncounterRecno = source.MergeEncounterRecno
	--		,target.HospitalDischargeDate = source.HospitalDischargeDate
	--		,target.HospitalDischargeTime = source.HospitalDischargeTime
	--		,target.HealthVisitorDischargeDate = source.HealthVisitorDischargeDate			
	--		,target.Location = source.Location
	--		,target.MobileNumber = source.MobileNumber
	--		,target.LandlineNumber = source.LandlineNumber		
	--		,target.Title = source.Title
	--		,target.FirstName = source.FirstName
	--		,target.LastName = source.LastName		
	--		,target.Address1 = source.Address1
	--		,target.Address2 = source.Address2
	--		,target.Address3 = source.Address3
	--		,target.Address4 = source.Address4
	--		,target.Postcode = source.Postcode
	--		,target.ContextCode = source.ContextCode
			target.Updated = getdate()
			--,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


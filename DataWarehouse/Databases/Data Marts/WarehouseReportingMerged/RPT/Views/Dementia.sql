﻿












CREATE VIEW [RPT].[Dementia]

AS

 /******************************************************************************
**  Name:		RPT.Dementia
**  Purpose:	Transformation View for populating table RPT.DementiaTable
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** ----------	-------		---------------------------------------------------- 
** 2012-12-28   CB			Refactored
******************************************************************************/


WITH XMLNAMESPACES (N'http://schemas.microsoft.com/office/infopath/2003/myXSD/2009-11-10T09:47:10' as my)

--trafford
select
	 DementiaID =
		InfopathForm.FormId

	,ProviderSpellNo = 
		APCEncounter.ProviderSpellNo

	,SourcePatientNo =
		APCEncounter.SourcePatientNo

	,PASCode =
		WarehouseReportingMerged.APC.f_GetWardCode(
			 ProviderSpellNo
			,formXML.value('/my:myFields[1]/my:CreatedDate[1]', 'varchar(200)')
		)

	,LastUpdated_TS =
		[OriginalSubmissionDate]
--		formXML.value('/my:myFields[1]/my:CreatedDate[1]', 'varchar(200)')

	,KnownToHaveDementia =
		replace(
			replace(
				 formXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)')
				,'Yes'
				,1
			)
			,'No'
			,0
		)

	,PatientDementiaFlag =
		CASE
		WHEN formXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') = 'Yes' THEN 1
		ELSE
			CASE
			WHEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
				THEN
					CASE
					WHEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 7	THEN 1
					ELSE 0 
					END
			ELSE 0 
			END 
		END

	,RememberMePlan =
		CASE 
		WHEN
			formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed1[1]', 'varchar(200)')
			+
			formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed2[1]', 'varchar(200)')
			+
			formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed3[1]', 'varchar(200)')
			= 'truetruetrue'
			THEN 1 
		ELSE 0 
		END

	,RememberMePlanCommenced =
		formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate1[1]', 'varchar(200)')

	,RememberMePlanDiscontinued = Null

	,AMTTestPerformed =
		replace(
			replace(
				 formXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)')
				,'false'
				,0
			)
			,'true'
			,1
		)

	,ClinicalIndicationsPresent = 

			case 
				when formXML.value('/my:myFields[1]/my:ClinicalIndications[1]/my:ClinInd[1]', 'varchar(200)')  = 'true' then 1
				when formXML.value('/my:myFields[1]/my:ClinicalIndications[1]/my:ClinInd[1]', 'varchar(200)')  = 'False' then 0
				else null
			end
	,ForgetfulnessQuestionAnswered =

		case 
		when formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='Yes' then 1
		when formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='No' then 0
		else null
		end

			--Gareth Cunnah Replaced 02/01/2012 below did not apear to give correct results

		--replace(
		--	replace(
		--		 formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)')
		--		,'Yes'
		--		,1
		--	)
		--	,'No'
		--	,Null
		--)

	,AMTScore =
		CASE 
		WHEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
			THEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)')
		ELSE Null 
		END 
		
	,ReferralNeeded =
		CASE 
		WHEN formXML.value('/my:myFields[1]/my:GPLetterConfirmSection[1]/my:GPLetterConfirmed[1]', 'varchar(200)') = 'true'
			THEN 1
		WHEN formXML.value('/my:myFields[1]/my:GPLetterConfirmSection2[1]/my:GPLetterConfirmed2[1]', 'varchar(200)') = 'true'
			THEN 1			
		ELSE Null 
		END 

	,AMTTestNotPerformedReason = formXML.value('/my:myFields[1]/my:UnableSection[1]/my:UnableReason[1]', 'varchar(200)')

	,CognitiveInvestigationStarted = 

			case
				when formXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)')  = 'true' then 'Y'
				when formXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)')  = 'False' then 'N'
				else null
			end

from
	WarehouseReportingMerged.APC.Encounter APCEncounter

inner join InfoPathForms.dbo.InfopathForm 
on	InfoPathForm.AdmissionNumber = APCEncounter.ProviderSpellNo
and APCEncounter.FirstEpisodeInSpellIndicator = 1
and InfopathForm.FormType = 'Dementia'


UNION

--central
SELECT 
	 DementiaID =
		BedmanEvent.ID

	,ProviderSpellNo
		= ProviderSpellNo

	,SourcePatientNo =
		SourcePatientNo

	,PASCode =
		Spell.CurrWard
		--Location.PASCode

	,LastUpdated_TS =
		LastUpdated_TS

	,KnownToHaveDementia =
		replace(
			replace(
				 BedmanEvent.details.value('(DementiaAssessmentData/KnownToHaveDementia)[1]', 'VARCHAR(60)')
				,'N'
				,0
			)
			,'Y'
			,1
		)

	,PatientDementiaFlag =
		CASE 
		WHEN BedmanEvent.details.value('(DementiaAssessmentData/KnownToHaveDementia)[1]', 'VARCHAR(60)') = 'Y' THEN 1
		ELSE 
			CASE 
			WHEN BedmanEvent.details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[1]', 'VARCHAR(60)') between 0 and 7 THEN 1 
			ELSE
				replace(
					replace(
						 BedmanEvent.details.value('(DementiaAssessmentData/PatientDementiaFlag)[1]', 'VARCHAR(60)')
						,'N'
						,0)
					,'Y'
					,1
				)
			END
		END

	,RememberMePlan =
		replace(
			replace(
				 BedmanEvent.details.value('(DementiaAssessmentData/RememberMePlan)[1]', 'VARCHAR(60)')
				,'false'
				,0
			)
			,'true'
			,1
		)

	,RememberMePlanCommenced =
		BedmanEvent.details.value('(DementiaAssessmentData/RememberMePlanCommenced)[1]', 'VARCHAR(60)')

	,RememberMePlanDiscontinued =
		BedmanEvent.details.value('(DementiaAssessmentData/RememberMePlanDiscontinued)[1]', 'VARCHAR(60)')

	,AMTTestPerformed =
		replace(
			replace(
				BedmanEvent.details.value('(DementiaAssessmentData/AMTTestPerformed)[1]', 'VARCHAR(60)')
				,'N'
				,0
			)
			,'Y'
			,1
		)

	,ClinicalIndicationsPresent =
		case 
			when BedmanEvent.details.value('(DementiaAssessmentData/ClinicalIndicationsPresent)[1]', 'VARCHAR(60)') = 'N' then 0
			when BedmanEvent.details.value('(DementiaAssessmentData/ClinicalIndicationsPresent)[1]', 'VARCHAR(60)') = 'Y' then 1
		else null
		end
		--replace(
		--	replace(
		--		BedmanEvent.details.value('(DementiaAssessmentData/ClinicalIndicationsPresent)[1]', 'VARCHAR(60)')
		--		,'N'
		--		,0
		--	)
		--	,'Y'
		--	,1
		--)

	,ForgetfulnessQuestionAnswered =

	case
	when BedmanEvent.details.value('(DementiaAssessmentData/ForgetfulnessQuestionAnswered)[1]', 'VARCHAR(60)') = 'N' then 0
	when BedmanEvent.details.value('(DementiaAssessmentData/ForgetfulnessQuestionAnswered)[1]', 'VARCHAR(60)') = 'Y' then 1
	else null
	end
		--replace(
		--	replace(
		--		BedmanEvent.details.value('(DementiaAssessmentData/ForgetfulnessQuestionAnswered)[1]', 'VARCHAR(60)')
		--		,'N'
		--		,0
		--	)
		--	,'Y'
		--	,1
		--)

	,AMTScore = 
		BedmanEvent.details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[1]', 'VARCHAR(60)')
		
	,ReferralNeeded = Null

	,AMTTestNotPerformedReason = 
		case	when BedmanEvent.details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[1]', 'VARCHAR(60)') >= 0 then null
				else BedmanEvent.details.value('(DementiaAssessmentData/AMTTestNotPerformedReason)[1]', 'VARCHAR(60)')
		end
			
	,CognitiveInvestigationStarted = BedmanEvent.details.value('(DementiaAssessmentData/CognitiveInvestigationStarted)[1]', 'VARCHAR(60)')
	
FROM
	BedMan.dbo.Event BedmanEvent

INNER JOIN Bedman.dbo.EventTypes EventType
ON	BedmanEvent.EventTypeID = EventType.ID

INNER JOIN BedMan.dbo.Locations Location
ON	BedmanEvent.LocationID = Location.LocationID

INNER JOIN BedMan.dbo.HospSpell Spell
ON	BedmanEvent.HospSpellID = Spell.SpellID

INNER JOIN WarehouseReportingMerged.APC.Encounter APCEncounter
ON	Spell.Patient = APCEncounter.SourcePatientNo
AND Spell.IPEpisode = APCEncounter.SourceSpellNo
AND APCEncounter.FirstEpisodeInSpellIndicator = 1

WHERE
	EventType.Description in ('DementiaAssessment')































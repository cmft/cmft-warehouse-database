﻿create view [COM].[CancellationReasonBase]

as

/****************************************************************************************
	View		: COM.CancellationReasonBase
	Description : Community cancellation reasons from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	20/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	CancellationReasonID
	,CancellationReason
	,CancellationReasonCode
	,NHSCancellationReasonCode
	,NationalCancellationReasonCode
	,ContextCode = 'CEN||IPM'
from
	WarehouseOLAPMergedV2.COM.CancellationReasonBase

﻿-- View

CREATE view [COM].[CIDSReferral] as

/* 
==============================================================================================
Description:	To supply the commissioner with a patient level dataset (Interim MDS)

When		Who			What
23/04/2015	Paul Egan	Initial Coding
===============================================================================================
*/

select
	Referral_Received_Date = Referral.ReferralReceivedDate
	,Org_Code_of_Provider = 'RW3'
	,[Service] = 
		case
			when [service].ServiceGroup = 'Not Specified' then 'NS-' + ReferredToSpecialty.Specialty 
			else [service].ServiceGroup
		end
	,NHS_Number = Referral.PatientNHSNumber
	,Age = dbo.f_GetAge(Referral.PatientDateOfBirth, Referral.ReferralReceivedDate)
	,Gender = Sex.Sex
	,Registered_GP_Practice_Code = null		-- Need to bring through from ipm?
	,CCG_of_Residence = null				-- Need to bring through from ipm?
	,Priority_Type = ReferralPriority.ReferralPriority	-- Description instead of code requested by Mark Land at GMCSU
	--,Priority_Type_Desc = ReferralPriority.ReferralPriority		-- Validation only 
	,Referral_Source = ReferralSource.ReferralSource
	,Referrer_Code = ProfessionalCarer.ProfessionalCarerCode
	,Ethnicity = EthnicCategoryWH.NationalEthnicCategory
	--,Ethnicity_Desc = EthnicCategory.EthnicCategory				-- Validation only 
	,Local_Ref_Source = NULL	-- Not sure what this is
	,Referring_org_Code = GPPractice.GPPracticeCode
	,CAB_FLAG = NULL	-- Not sure where to get this
from
	COM.Referral

	inner join COM.ReferralReference
	on	ReferralReference.MergeEncounterRecno = Referral.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.COM.GPPractice
	on	GPPractice.GPPracticeID = Referral.ReferredByHealthOrgID

	left join WarehouseOLAPMergedV2.COM.ProfessionalCarer
	on	ProfessionalCarer.ProfessionalCarerID = Referral.ReferredByProfessionalCarerID

	left join COM.SexBase Sex
	on	Sex.SexID = Referral.PatientSexID

	left join COM.ReferralPriorityBase ReferralPriority
	on	ReferralPriority.ReferralPriorityID = Referral.[Priority]

	left join COM.ReferralSource
	on	ReferralSource.ReferralSourceID = Referral.SourceofReferralID

	left join COM.EthnicCategoryBase EthnicCategory
	on	EthnicCategory.EthnicCategoryID = Referral.PatientEthnicGroupID

	inner join WH.EthnicCategory EthnicCategoryWH
	on	EthnicCategoryWH.SourceEthnicCategoryCode = EthnicCategory.EthnicCategoryID
	and EthnicCategoryWH.SourceContextCode = 'CEN||IPM'

	left join COM.Specialty ReferredToSpecialty
	on	ReferredToSpecialty.SpecialtyID = Referral.ReferredToSpecialtyID

	--inner join [WarehouseReportingMerged].COM.Allocation ServiceAllocation	-- Credit Darren Griffiths				
	--on	ServiceAllocation.DatasetRecno = Referral.MergeEncounterRecno

	left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation ServiceAllocation
	on	ServiceAllocation.AllocationTypeID = 2
	and	ServiceAllocation.DatasetCode = 'COMR'
	and	ServiceAllocation.DatasetRecno = Referral.MergeEncounterRecno

	left join WH.[Service]							-- Credit Darren Griffiths
	on	[Service].ServiceID = ServiceAllocation.AllocationID

	--inner join 
	--	(
	--	select distinct
	--		ReferralID
	--		--,[Service].ServiceGroup
	--	from
	--		COM.Encounter

	--		left join COM.Allocation ServiceAllocation		-- Credit Darren Griffiths				
	--		on	ServiceAllocation.DatasetRecno = Encounter.MergeEncounterRecno

	--		left join WH.[Service]							-- Credit Darren Griffiths
	--		on	[Service].ServiceID = ServiceAllocation.AllocationID

	--	) Encounter
	--on Encounter.ReferralID = Referral.SourceUniqueID

where
	Referral.ArchiveFlag = 'N'		-- Better to be safe and always use this

--	and Referral.ReferralReceivedDate between '20150301' and '20150331'
--order by
--	ReferralReceivedDate
--	,[Service]
--	,NHS_Number


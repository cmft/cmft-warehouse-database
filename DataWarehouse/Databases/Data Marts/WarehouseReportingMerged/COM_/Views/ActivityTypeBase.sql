﻿create view [COM].[ActivityTypeBase]

as

/****************************************************************************************
	View		: COM.RTTPeriodStatusBase
	Description : Community activity RTT period status from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	23/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	ActivityTypeID
	,ActivityType
	,ActivityTypeCode
	,NHSActivityTypeCode
	,NationalActivityTypeCode
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.ActivityTypeBase
;

﻿CREATE view [COM].[AttendanceStatusBase]

as

/****************************************************************************************
	View : COM.AttendanceStatusBase
	Description : Community attendance status from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	19/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	AttendanceStatusID
	,AttendanceStatus
	,AttendanceStatusCode
	,NHSAttendanceStatusCode
	,NationalAttendanceStatusCode
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.AttendanceStatusBase

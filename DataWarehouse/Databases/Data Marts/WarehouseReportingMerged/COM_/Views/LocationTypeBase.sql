﻿create view [COM].[LocationTypeBase]

as

/****************************************************************************************
	View		: COM.LocationTypeBase
	Description : Community activity location types from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	20/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	LocationTypeID
	,LocationType
	,LocationTypeCode
	,NHSLocationTypeCode
	,NationalLocationTypeCode
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.LocationTypeBase
;

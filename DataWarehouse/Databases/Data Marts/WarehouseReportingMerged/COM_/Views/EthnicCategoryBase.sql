﻿create view [COM].[EthnicCategoryBase]

as

/****************************************************************************************
	View		: COM.EthnicCategoryBase
	Description : Community activity ethnic categories from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	23/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	EthnicCategoryID
	,EthnicCategory
	,EthnicCategoryCode
	,NHSEthnicCategoryCode
	,NationalEthnicCategoryCode
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.EthnicCategoryBase
;

﻿create view [COM].[ReferralPriorityBase]

as

/****************************************************************************************
	View		: [COM].[ReferralPriorityBase]
	Description : Community referral priority from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	26/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	ReferralPriorityID
	,ReferralPriority
	,ReferralPriorityCode
	,NHSReferralPriorityCode
	,NationalReferralPriorityCode
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.ReferralPriorityBase
;

﻿
CREATE view [AE].[DurationBand] as
SELECT [DurationBandCode]
      ,[MinuteBand]
      ,[FifteenMinuteBandCode]
      ,[FifteenMinuteBand]
      ,[ThirtyMinuteBandCode]
      ,[ThirtyMinuteBand]
      ,[HourBandCode]
      ,[HourBand]
      ,[BreachFifteenMinuteBandCode]
      ,[BreachFifteenMinuteBand]
      ,[BreachThirtyMinuteBandCode]
      ,[BreachThirtyMinuteBand]
      ,[BreachHourBandCode]
      ,[BreachHourBand]
      ,[BreachHourBandGroupCode]
      ,[BreachHourBandGroup]
  FROM [WarehouseOLAPMergedV2].[AE].[DurationBand]


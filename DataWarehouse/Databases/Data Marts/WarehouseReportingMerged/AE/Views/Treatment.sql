﻿

CREATE view [AE].[Treatment] as
SELECT 
	 B.MergeEncounterRecno
	,B.SequenceNo
	,B.TreatmentSchemeCode
	,B.TreatmentCode
	,B.TreatmentTime
	,B.ContextCode
	,R.ContextID
	,R.TreatmentID
FROM [WarehouseOLAPMergedV2].[AE].[BaseTreatment] B
JOIN [WarehouseOLAPMergedV2].[AE].[BaseTreatmentReference] R ON R.MergeEncounterRecno = B.MergeEncounterRecno
AND R.SequenceNo = B.SequenceNo


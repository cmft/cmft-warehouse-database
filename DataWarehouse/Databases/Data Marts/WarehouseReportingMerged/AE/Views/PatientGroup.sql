﻿
create view [AE].[PatientGroup] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourcePatientGroupID]
      ,[SourcePatientGroupCode]
      ,[SourcePatientGroup]
      --,[LocalPatientGroupID]
      ,[LocalPatientGroupCode]
      ,[LocalPatientGroup]
      --,[NationalPatientGroupID]
      ,[NationalPatientGroupCode]
      ,[NationalPatientGroup]
  FROM [WarehouseOLAPMergedV2].[AE].[PatientGroup]


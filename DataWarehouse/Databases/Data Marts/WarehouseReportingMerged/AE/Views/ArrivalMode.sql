﻿



CREATE view [AE].[ArrivalMode] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceArrivalModeID]
      ,[SourceArrivalModeCode]
      ,[SourceArrivalMode]
      --,[LocalArrivalModeID]
      ,[LocalArrivalModeCode]
      ,[LocalArrivalMode]
      --,[NationalArrivalModeID]
      ,[NationalArrivalModeCode]
      ,[NationalArrivalMode]
  FROM [WarehouseOLAPMergedV2].[AE].[ArrivalMode]



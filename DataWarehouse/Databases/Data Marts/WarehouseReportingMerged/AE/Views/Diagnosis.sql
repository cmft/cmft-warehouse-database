﻿

CREATE view [AE].[Diagnosis] as
SELECT 
 B.MergeEncounterRecno
,B.SequenceNo
,B.DiagnosticSchemeCode
,B.DiagnosisCode
,B.DiagnosisSiteCode
,B.DiagnosisSideCode
,B.ContextCode
,R.ContextID
,R.DiagnosisID
FROM [WarehouseOLAPMergedV2].[AE].[BaseDiagnosis] B
JOIN [WarehouseOLAPMergedV2].[AE].[BaseDiagnosisReference] R ON R.MergeEncounterRecno = B.MergeEncounterRecno
AND R.SequenceNo = B.SequenceNo


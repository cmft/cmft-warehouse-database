﻿
create view [AE].[SourceOfReferral] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceSourceOfReferralID]
      ,[SourceSourceOfReferralCode]
      ,[SourceSourceOfReferral]
      --,[LocalSourceOfReferralID]
      ,[LocalSourceOfReferralCode]
      ,[LocalSourceOfReferral]
      --,[NationalSourceOfReferralID]
      ,[NationalSourceOfReferralCode]
      ,[NationalSourceOfReferral]
  FROM [WarehouseOLAPMergedV2].[AE].[SourceOfReferral]


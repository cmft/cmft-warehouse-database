﻿
CREATE view [AE].[TriageCategory] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceTriageCategoryID]
      ,[SourceTriageCategoryCode]
      ,[SourceTriageCategory]
      --,[LocalTriageCategoryID]
      ,[LocalTriageCategoryCode]
      ,[LocalTriageCategory]
      --,[NationalTriageCategoryID]
      ,[NationalTriageCategoryCode]
      ,[NationalTriageCategory]
  FROM [WarehouseOLAPMergedV2].[AE].[TriageCategory]


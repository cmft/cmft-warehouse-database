﻿
create view [AE].[HRG4Encounter]

as

/****************************************************************************************
	View : AE.HRG4Encounter
	Description	: View on table WarehouseOLAPMergedV2.AE.HRG4Encounter

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	13/05/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	[MergeEncounterRecno]
	,[HRGCode]
	,[Created]
	,[ByWhom]
from
	WarehouseOLAPMergedV2.AE.HRG4Encounter



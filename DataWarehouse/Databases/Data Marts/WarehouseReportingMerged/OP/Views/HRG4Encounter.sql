﻿create view [OP].[HRG4Encounter]

as

/****************************************************************************************
	View : OP.HRG4Encounter
	Description	: View on table WarehouseOLAPMergedV2.OP.HRG4Encounter

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	13/05/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	[MergeEncounterRecno]
	,[HRGCode]
	,[GroupingMethodFlag]
	,[DominantOperationCode]
	,[Created]
	,[ByWhom]
from
	WarehouseOLAPMergedV2.OP.HRG4Encounter
﻿create view [OP].[PSSQuality]

as

SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[QualityTypeCode]
      ,[QualityCode]
      ,[QualityMessage]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[OP].[PSSQuality]

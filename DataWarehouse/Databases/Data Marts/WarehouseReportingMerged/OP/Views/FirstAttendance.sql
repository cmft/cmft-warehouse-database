﻿
create view [OP].[FirstAttendance] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceFirstAttendanceID]
      ,[SourceFirstAttendanceCode]
      ,[SourceFirstAttendance]
      --,[LocalFirstAttendanceID]
      ,[LocalFirstAttendanceCode]
      ,[LocalFirstAttendance]
      --,[NationalFirstAttendanceID]
      ,[NationalFirstAttendanceCode]
      ,[NationalFirstAttendance]
  FROM [WarehouseOLAPMergedV2].[OP].[FirstAttendance]


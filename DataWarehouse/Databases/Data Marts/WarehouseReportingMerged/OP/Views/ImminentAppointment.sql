﻿




CREATE view [OP].[ImminentAppointment]

as

/****************************************************************************************
View		: [OP].[ImminentAppointment]
Description	: Created for use of appointment reminder data feed(s), including Healthcare
				Communications Envoy system (for Central appointments).

Modification History
====================

Date		Person			Description
====================================================================================
06/08/2014	Paul Egan       Initial Coding
11/09/2014	Paul Egan		Can now lookup PAS Specialty OR Division in EntityLookup.
23/09/2014	Paul Egan		Added CTE and row_number to only return first appointment of the day
							for each patient.
06/03/2014	Paul Egan		Added location
*****************************************************************************************/

with allOPAppointments as
(
select
	Encounter.MergeEncounterRecno					-- Validation only
	,AppointmentCancelDate = convert(varchar(10), Encounter.AppointmentCancelDate, 103)		-- Validation only
	,PASSpecialtyCode = Encounter.SpecialtyCode		-- Validation only
	,EncounterTypeCode = 'OP'						-- Validation only. Added this column as some daycase appointments may come from APC instead of OP.
	,ActiveAppointmentDaySeq =						-- Added 23/09/2014 Paul Egan - enables selecting only first appointment of the day for each patient.
		case
			when Encounter.AppointmentCancelDate is null then		
				row_number() over(partition by Encounter.DistrictNo, Encounter.AppointmentDate order by Encounter.AppointmentTime, Encounter.ClinicCode)  -- Uses clinicCode as tie-breaker.
			else null
		end
	,Division = Directorate.Division
	,patient_id = Encounter.DistrictNo
	,appointment_date = convert(varchar(10), Encounter.AppointmentDate, 103)			-- dd/mm/yyyy format
	,appointment_time = cast(cast(Encounter.AppointmentTime as time) as varchar(5))		-- hh:mm format
	,mobile_number = 
		case
			when left(Patient.MobilePhone , 2) = '07' then replace(Patient.MobilePhone, ' ','')
			when left(Patient.HomePhone , 2) = '07' then replace(Patient.HomePhone, ' ','')
			when left(Patient.WorkPhone , 2) = '07' then replace(Patient.WorkPhone, ' ','')
			else null
		end
	,landline_number = 
		case
			when left(Patient.HomePhone , 2) <> '07' then replace(Patient.HomePhone, ' ','')
			when left(Patient.MobilePhone , 2) <> '07' then replace(Patient.MobilePhone, ' ','')
			else null
		end
	,country_code = '44'
	,contact_tel_no = coalesce(LookupPASSpecialty.[Description], LookupDivision.[Description])
	,first_name = Encounter.PatientForename
	,last_name = Encounter.PatientSurname
	,dob = convert(varchar(10), Encounter.DateOfBirth, 103)			-- dd/mm/yyyy format
	,cancel = 
		case
			when Encounter.AppointmentCancelDate is null then null
			else 'cancel'
		end
	,specialty_code = Specialty.NationalSpecialtyCode
	,clinic_code = Encounter.ClinicCode
	,appt_audit_ref = null
	,appt_type = null
	,location = Location.Location
	,practitioner = null
	,procedure_code = null
	,patient_email = null
	,genericinfo1 = null
	,genericinfo2 = Encounter.SexCode
	,genericinfo3 = Encounter.Postcode
from
	[WarehouseReportingMerged].OP.Encounter Encounter		
	
	/* Join on patient to get mobile phone numbers */
	inner join WarehouseReporting.PAS.Patient			
	on	Patient.SourcePatientNo = Encounter.SourcePatientNo
	
	inner join WarehouseReporting.PAS.Clinic
	on	Clinic.ClinicCode  = Encounter.ClinicCode
	
	inner join WarehouseReporting.PAS.Location
	on	Location.LocationCode = Clinic.ReportToLocationCode
	
	inner join [WarehouseReportingMerged].WH.Directorate	
	on	Directorate.DirectorateCode = Encounter.DirectorateCode
	
	inner join [WarehouseReportingMerged].WH.Specialty	
	on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID
	
	/*	Must use left join so LookupPASSpecialty and LookupDivision do not conflict.
		(Non-matches are removed in the WHERE clause) */
	left join WarehouseOLAPMergedV2.Utility.EntityLookup LookupPASSpecialty
	on	LookupPASSpecialty.EntityCode = Encounter.SpecialtyCode
	and LookupPASSpecialty.EntityTypeCode = 'AppReminderPASSpecialty'
	and Encounter.ContextCode = 'cen||pas'

	/*	Must use left join so LookupPASSpecialty and LookupDivision do not conflict.
		(Non-matches are removed in the WHERE clause) */
	left join WarehouseOLAPMergedV2.Utility.EntityLookup LookupDivision
	on	LookupDivision.EntityCode = Directorate.Division
	and LookupDivision.EntityTypeCode = 'AppReminderDivision'
	and Encounter.ContextCode = 'cen||pas'
	/*	NB This join would create duplicates with any specialties in LookupPASSpecialty 
		under the same division, but these are eliminated in the WHERE clause Paul Egan 11/09/2014 */
		
where
	Encounter.ContextCode = 'cen||pas'		-- for now anyway - I think Trafford is currently using Mjog
	and
	(
		LookupPASSpecialty.EntityCode is not null 
		or 
		LookupDivision.EntityCode is not null  -- This also removes duplicates from any specialties in LookupPASSpecialty under the same division
	)
	and
	(
		/* Active imminent appointments */
		(
		Encounter.AppointmentDate between cast(dateadd(day, 1, getdate()) as date) and cast(dateadd(day, 7, getdate()) as date)
		and
		Encounter.AppointmentCancelDate is null
		)
		or
		/* Cancelled imminent appointments, cancelled recently (we don't want appointments cancelled weeks ago (or more)) */
		(
		Encounter.AppointmentDate between cast(getdate() as date) and cast(dateadd(day, 7, getdate()) as date)
		and
		Encounter.AppointmentCancelDate between cast(dateadd(day, -7, getdate()) as date) and cast(getdate() as date)
		)
	)
)

select
	MergeEncounterRecno
	,AppointmentCancelDate
	,PASSpecialtyCode
	,EncounterTypeCode
	,Division
	,patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name
	,last_name
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,appt_audit_ref
	,appt_type
	,location
	,practitioner
	,procedure_code
	,patient_email
	,genericinfo1
	,genericinfo2
	,genericinfo3
from 
	allOPAppointments
where
	(
	ActiveAppointmentDaySeq = 1				-- First appointment of the day
	or ActiveAppointmentDaySeq is null		-- and all cancellations
	)

--order by		-- Debug only
--	cancel
--	,appointment_date
--	,appointment_time
--	,clinic_code

	--and Encounter.DistrictNo = '02577464'		-- Debug only
	--and Directorate.Division = 'Childrens'	-- Debug only
	--and Encounter.SpecialtyCode = 'GYON'		-- Debug only







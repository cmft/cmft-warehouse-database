﻿create view [OP].[HRG4Flag]

as

/****************************************************************************************
	View : OP.HRG4Flag
	Description	: View on table WarehouseOLAPMergedV2.OP.HRG4Flag

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	13/05/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	[MergeEncounterRecno]
	,[SequenceNo]
	,[FlagCode]
	,[Created]
	,[ByWhom]
from
	WarehouseOLAPMergedV2.OP.HRG4Flag
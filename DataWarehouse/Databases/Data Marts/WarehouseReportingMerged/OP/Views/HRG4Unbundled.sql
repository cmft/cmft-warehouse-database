﻿create view [OP].[HRG4Unbundled]

as

/****************************************************************************************
	View : OP.HRG4Unbundled
	Description	: View on table WarehouseOLAPMergedV2.OP.HRG4Unbundled

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	13/05/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	[MergeEncounterRecno]
	,[SequenceNo]
	,[HRGCode]
	,[Created]
	,[ByWhom]
from
	WarehouseOLAPMergedV2.OP.HRG4Unbundled
﻿






CREATE view [OP].[Encounter] as

select
	Encounter.MergeEncounterRecno
	,Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.NHSNumberStatusCode
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.SiteCode
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime
	,Encounter.ClinicCode
	,Encounter.AdminCategoryCode
	,Encounter.SourceOfReferralCode
	,Encounter.ReasonForReferralCode
	,Encounter.PriorityCode
	,Encounter.FirstAttendanceFlag
	,Encounter.DNACode
	,Encounter.AppointmentStatusCode
	,Encounter.CancelledByCode
	,Encounter.TransportRequiredFlag
	,Encounter.AttendanceOutcomeCode
	,Encounter.AppointmentTypeCode
	,Encounter.DisposalCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferralConsultantCode
	,Encounter.ReferralSpecialtyCode
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNo
	,Encounter.AppointmentCreateDate
	,Encounter.EpisodicGpCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.DoctorCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryProcedureDate
	,Encounter.SecondaryProcedureCode1
	,Encounter.SecondaryProcedureDate1
	,Encounter.SecondaryProcedureCode2
	,Encounter.SecondaryProcedureDate2
	,Encounter.SecondaryProcedureCode3
	,Encounter.SecondaryProcedureDate3
	,Encounter.SecondaryProcedureCode4
	,Encounter.SecondaryProcedureDate4
	,Encounter.SecondaryProcedureCode5
	,Encounter.SecondaryProcedureDate5
	,Encounter.SecondaryProcedureCode6
	,Encounter.SecondaryProcedureDate6
	,Encounter.SecondaryProcedureCode7
	,Encounter.SecondaryProcedureDate7
	,Encounter.SecondaryProcedureCode8
	,Encounter.SecondaryProcedureDate8
	,Encounter.SecondaryProcedureCode9
	,Encounter.SecondaryProcedureDate9
	,Encounter.SecondaryProcedureCode10
	,Encounter.SecondaryProcedureDate10
	,Encounter.SecondaryProcedureCode11
	,Encounter.SecondaryProcedureDate11
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.ReferralDate
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.AppointmentCategoryCode
	,Encounter.AppointmentCreatedBy
	,Encounter.AppointmentCancelDate
	,Encounter.LastRevisedDate
	,Encounter.LastRevisedBy
	,Encounter.OverseasStatusFlag
	,Encounter.PatientChoiceCode
	,Encounter.ScheduledCancelReasonCode
	,Encounter.PatientCancelReason
	,Encounter.DischargeDate
	,Encounter.QM08StartWaitDate
	,Encounter.QM08EndWaitDate
	,Encounter.DestinationSiteCode
	,Encounter.EBookingReferenceNo
	,Encounter.InterfaceCode
	,Encounter.LocalAdminCategoryCode
	,Encounter.PCTCode
	,Encounter.LocalityCode
	,Encounter.AgeCode
	,Encounter.HRGCode
	,Encounter.Cases
	,Encounter.LengthOfWait
	,Encounter.IsWardAttender
	,Encounter.RTTActivity
	,Encounter.RTTBreachStatusCode
	,Encounter.ClockStartDate
	,Encounter.RTTBreachDate
	,Encounter.RTTTreated
	,Encounter.DirectorateCode
	,Encounter.ReferralSpecialtyTypeCode
	,Encounter.LastDNAorPatientCancelledDate
	,Encounter.ReferredByCode
	,Encounter.ReferrerCode
	,Encounter.AppointmentOutcomeCode
	,Encounter.MedicalStaffTypeCode
	,Encounter.ContextCode
	,Encounter.TreatmentFunctionCode
	,Encounter.CommissioningSerialNo
	,Encounter.DerivedFirstAttendanceFlag
	,Encounter.AttendanceIdentifier
	,Encounter.RegisteredGpAtAppointmentCode
	,Encounter.RegisteredGpPracticeAtAppointmentCode
	,Encounter.ReferredByConsultantCode
	,Encounter.ReferredByGpCode
	,Encounter.ReferredByGdpCode
	,Encounter.PseudoPostcode
	,Encounter.CCGCode
	,Encounter.PostcodeAtAppointment
	,Encounter.EpisodicPostcode
	,Encounter.GpCodeAtAppointment
	,Encounter.GpPracticeCodeAtAppointment
	,Encounter.ContextID
	,Encounter.SexID
	,Encounter.AgeID
	,Encounter.NHSNumberStatusID
	,Encounter.EthnicOriginID
	,Encounter.MaritalStatusID
	,Encounter.ReligionID
	,Encounter.SiteID
	,Encounter.AppointmentDateID
	,Encounter.ClinicID
	,Encounter.AdminCategoryID
	,Encounter.SourceOfReferralID
	,Encounter.ReasonForReferralID
	,Encounter.PriorityID
	,Encounter.FirstAttendanceID
	,Encounter.AttendanceStatusID
	,Encounter.AttendanceOutcomeID
	,Encounter.AppointmentTypeID
	,Encounter.ConsultantID
	,Encounter.SpecialtyID
	,Encounter.ReferralConsultantID
	,Encounter.ReferralSpecialtyID
	,Encounter.RTTSpecialtyID
	,Encounter.RTTCurrentStatusID
	,Encounter.ReferringConsultantID
	,Encounter.TreatmentFunctionID
	,Encounter.DerivedFirstAttendanceID
	,Encounter.AppointmentResidenceCCGID
	,Encounter.AppointmentResidenceCCGCode

	,ContractPointOfDeliveryCode = ContractPointOfDelivery.SourceAllocationID
	,ContractFlagCode = ContractFlag.SourceAllocationID
	,ContractHRGCode = ContractHRG.SourceAllocationID
	,ContractSpecialtyCode = ContractSpecialty.SourceAllocationID	
	

from
	(

		SELECT
			 BaseEncounter.MergeEncounterRecno
			,BaseEncounter.EncounterRecno
			,BaseEncounter.SourceUniqueID
			,BaseEncounter.SourcePatientNo
			,BaseEncounter.SourceEncounterNo
			,BaseEncounter.PatientTitle
			,BaseEncounter.PatientForename
			,BaseEncounter.PatientSurname
			,BaseEncounter.DateOfBirth
			,BaseEncounter.DateOfDeath
			,BaseEncounter.SexCode
			,BaseEncounter.NHSNumber
			,BaseEncounter.NHSNumberStatusCode
			,BaseEncounter.DistrictNo
			,BaseEncounter.Postcode
			,BaseEncounter.PatientAddress1
			,BaseEncounter.PatientAddress2
			,BaseEncounter.PatientAddress3
			,BaseEncounter.PatientAddress4
			,BaseEncounter.DHACode
			,BaseEncounter.EthnicOriginCode
			,BaseEncounter.MaritalStatusCode
			,BaseEncounter.ReligionCode
			,BaseEncounter.RegisteredGpCode
			,BaseEncounter.RegisteredGpPracticeCode
			,BaseEncounter.SiteCode
			,BaseEncounter.AppointmentDate
			,BaseEncounter.AppointmentTime
			,BaseEncounter.ClinicCode
			,BaseEncounter.AdminCategoryCode
			,BaseEncounter.SourceOfReferralCode
			,BaseEncounter.ReasonForReferralCode
			,BaseEncounter.PriorityCode
			,BaseEncounter.FirstAttendanceFlag
			,BaseEncounter.DNACode
			,BaseEncounter.AppointmentStatusCode
			,BaseEncounter.CancelledByCode
			,BaseEncounter.TransportRequiredFlag
			,BaseEncounter.AttendanceOutcomeCode
			,BaseEncounter.AppointmentTypeCode
			,BaseEncounter.DisposalCode
			,BaseEncounter.ConsultantCode
			,BaseEncounter.SpecialtyCode
			,BaseEncounter.ReferralConsultantCode
			,BaseEncounter.ReferralSpecialtyCode
			,BaseEncounter.BookingTypeCode
			,BaseEncounter.CasenoteNo
			,BaseEncounter.AppointmentCreateDate
			,BaseEncounter.EpisodicGpCode
			,BaseEncounter.EpisodicGpPracticeCode
			,BaseEncounter.DoctorCode
			,BaseEncounter.PrimaryDiagnosisCode
			,BaseEncounter.SubsidiaryDiagnosisCode
			,BaseEncounter.SecondaryDiagnosisCode1
			,BaseEncounter.SecondaryDiagnosisCode2
			,BaseEncounter.SecondaryDiagnosisCode3
			,BaseEncounter.SecondaryDiagnosisCode4
			,BaseEncounter.SecondaryDiagnosisCode5
			,BaseEncounter.SecondaryDiagnosisCode6
			,BaseEncounter.SecondaryDiagnosisCode7
			,BaseEncounter.SecondaryDiagnosisCode8
			,BaseEncounter.SecondaryDiagnosisCode9
			,BaseEncounter.SecondaryDiagnosisCode10
			,BaseEncounter.SecondaryDiagnosisCode11
			,BaseEncounter.SecondaryDiagnosisCode12
			,BaseEncounter.PrimaryOperationCode
			,BaseEncounter.PrimaryProcedureDate
			,BaseEncounter.SecondaryProcedureCode1
			,BaseEncounter.SecondaryProcedureDate1
			,BaseEncounter.SecondaryProcedureCode2
			,BaseEncounter.SecondaryProcedureDate2
			,BaseEncounter.SecondaryProcedureCode3
			,BaseEncounter.SecondaryProcedureDate3
			,BaseEncounter.SecondaryProcedureCode4
			,BaseEncounter.SecondaryProcedureDate4
			,BaseEncounter.SecondaryProcedureCode5
			,BaseEncounter.SecondaryProcedureDate5
			,BaseEncounter.SecondaryProcedureCode6
			,BaseEncounter.SecondaryProcedureDate6
			,BaseEncounter.SecondaryProcedureCode7
			,BaseEncounter.SecondaryProcedureDate7
			,BaseEncounter.SecondaryProcedureCode8
			,BaseEncounter.SecondaryProcedureDate8
			,BaseEncounter.SecondaryProcedureCode9
			,BaseEncounter.SecondaryProcedureDate9
			,BaseEncounter.SecondaryProcedureCode10
			,BaseEncounter.SecondaryProcedureDate10
			,BaseEncounter.SecondaryProcedureCode11
			,BaseEncounter.SecondaryProcedureDate11
			,BaseEncounter.PurchaserCode
			,BaseEncounter.ProviderCode
			,BaseEncounter.ContractSerialNo
			,BaseEncounter.ReferralDate
			,BaseEncounter.RTTPathwayID
			,BaseEncounter.RTTPathwayCondition
			,BaseEncounter.RTTStartDate
			,BaseEncounter.RTTEndDate
			,BaseEncounter.RTTSpecialtyCode
			,BaseEncounter.RTTCurrentProviderCode
			,BaseEncounter.RTTCurrentStatusCode
			,BaseEncounter.RTTCurrentStatusDate
			,BaseEncounter.RTTCurrentPrivatePatientFlag
			,BaseEncounter.RTTOverseasStatusFlag
			,BaseEncounter.RTTPeriodStatusCode
			,BaseEncounter.AppointmentCategoryCode
			,BaseEncounter.AppointmentCreatedBy
			,BaseEncounter.AppointmentCancelDate
			,BaseEncounter.LastRevisedDate
			,BaseEncounter.LastRevisedBy
			,BaseEncounter.OverseasStatusFlag
			,BaseEncounter.PatientChoiceCode
			,BaseEncounter.ScheduledCancelReasonCode
			,BaseEncounter.PatientCancelReason
			,BaseEncounter.DischargeDate
			,BaseEncounter.QM08StartWaitDate
			,BaseEncounter.QM08EndWaitDate
			,BaseEncounter.DestinationSiteCode
			,BaseEncounter.EBookingReferenceNo
			,BaseEncounter.InterfaceCode
			,BaseEncounter.LocalAdminCategoryCode
			,BaseEncounter.PCTCode
			,BaseEncounter.LocalityCode
			,BaseEncounter.AgeCode
			,BaseEncounter.HRGCode
			,BaseEncounter.Cases
			,BaseEncounter.LengthOfWait
			,BaseEncounter.IsWardAttender
			,BaseEncounter.RTTActivity
			,BaseEncounter.RTTBreachStatusCode
			,BaseEncounter.ClockStartDate
			,BaseEncounter.RTTBreachDate
			,BaseEncounter.RTTTreated
			,BaseEncounter.DirectorateCode
			,BaseEncounter.ReferralSpecialtyTypeCode
			,BaseEncounter.LastDNAorPatientCancelledDate
			,BaseEncounter.ReferredByCode
			,BaseEncounter.ReferrerCode
			,BaseEncounter.AppointmentOutcomeCode
			,BaseEncounter.MedicalStaffTypeCode
			,BaseEncounter.ContextCode
			,BaseEncounter.TreatmentFunctionCode
			,BaseEncounter.CommissioningSerialNo
			,BaseEncounter.DerivedFirstAttendanceFlag
			,BaseEncounter.AttendanceIdentifier
			,BaseEncounter.RegisteredGpAtAppointmentCode
			,BaseEncounter.RegisteredGpPracticeAtAppointmentCode
			,BaseEncounter.ReferredByConsultantCode
			,BaseEncounter.ReferredByGpCode
			,BaseEncounter.ReferredByGdpCode
			,BaseEncounter.PseudoPostcode
			,BaseEncounter.CCGCode
			,BaseEncounter.PostcodeAtAppointment
			,BaseEncounter.EpisodicPostcode
			,BaseEncounter.GpCodeAtAppointment
			,BaseEncounter.GpPracticeCodeAtAppointment
			
			,BaseEncounterReference.ContextID
			,BaseEncounterReference.SexID
			,BaseEncounterReference.AgeID
			,BaseEncounterReference.NHSNumberStatusID
			,BaseEncounterReference.EthnicOriginID
			,BaseEncounterReference.MaritalStatusID
			,BaseEncounterReference.ReligionID
			,BaseEncounterReference.SiteID
			,BaseEncounterReference.AppointmentDateID
			,BaseEncounterReference.ClinicID
			,BaseEncounterReference.AdminCategoryID
			,BaseEncounterReference.SourceOfReferralID
			,BaseEncounterReference.ReasonForReferralID
			,BaseEncounterReference.PriorityID
			,BaseEncounterReference.FirstAttendanceID
			,BaseEncounterReference.AttendanceStatusID
			,BaseEncounterReference.AttendanceOutcomeID
			,BaseEncounterReference.AppointmentTypeID
			,BaseEncounterReference.ConsultantID
			,BaseEncounterReference.SpecialtyID
			,BaseEncounterReference.ReferralConsultantID
			,BaseEncounterReference.ReferralSpecialtyID
			,BaseEncounterReference.RTTSpecialtyID
			,BaseEncounterReference.RTTCurrentStatusID
			,BaseEncounterReference.ReferringConsultantID
			,BaseEncounterReference.TreatmentFunctionID
			,BaseEncounterReference.DerivedFirstAttendanceID

			,BaseEncounterReference.AppointmentResidenceCCGID
			,BaseEncounter.AppointmentResidenceCCGCode

			,ContractPointOfDeliveryID = PointOfDeliveryAllocation.AllocationID
			,ContractFlagID = FlagAllocation.AllocationID
			,ContractHRGID = HRGAllocation.AllocationID
			,ContractSpecialtyID = SpecialtyAllocation.AllocationID
				
		FROM [WarehouseOLAPMergedV2].[OP].[BaseEncounter] BaseEncounter
		JOIN [WarehouseOLAPMergedV2].[OP].[BaseEncounterReference] BaseEncounterReference 
		ON BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

		left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation PointOfDeliveryAllocation
		on	PointOfDeliveryAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
		and PointOfDeliveryAllocation.AllocationTypeID = 5
		and PointOfDeliveryAllocation.DatasetCode = 'OP'

		left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation FlagAllocation
		on	FlagAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
		and FlagAllocation.AllocationTypeID = 9
		and FlagAllocation.DatasetCode = 'OP'

		left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation HRGAllocation
		on	HRGAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
		and HRGAllocation.AllocationTypeID = 6
		and HRGAllocation.DatasetCode = 'OP'

		left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation SpecialtyAllocation
		on	SpecialtyAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
		and SpecialtyAllocation.AllocationTypeID = 7
		and SpecialtyAllocation.DatasetCode = 'OP'

	) Encounter

left join WarehouseOLAPMergedV2.Allocation.Allocation ContractPointOfDelivery
on ContractPointOfDelivery.AllocationID = Encounter.ContractPointOfDeliveryID

left join WarehouseOLAPMergedV2.Allocation.Allocation ContractFlag
on ContractFlag.AllocationID = Encounter.ContractFlagID

left join WarehouseOLAPMergedV2.Allocation.Allocation ContractHRG
on ContractHRG.AllocationID = Encounter.ContractHRGID

left join WarehouseOLAPMergedV2.Allocation.Allocation ContractSpecialty
on  ContractSpecialty.AllocationID = Encounter.ContractSpecialtyID




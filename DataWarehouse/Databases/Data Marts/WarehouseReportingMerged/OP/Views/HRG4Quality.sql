﻿create view [OP].[HRG4Quality]

as

/****************************************************************************************
	View : OP.HRG4Quality
	Description	: View on table WarehouseOLAPMergedV2.OP.HRG4Quality

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	13/05/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	[MergeEncounterRecno]
	,[SequenceNo]
	,[QualityTypeCode]
	,[QualityCode]
	,[QualityMessage]
	,[Created]
	,[ByWhom]
from
	WarehouseOLAPMergedV2.OP.HRG4Quality
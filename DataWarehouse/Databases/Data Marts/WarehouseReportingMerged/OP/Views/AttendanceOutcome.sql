﻿

create view [OP].[AttendanceOutcome] as


SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAttendanceOutcomeID]
      ,[SourceAttendanceOutcomeCode]
      ,[SourceAttendanceOutcome]
      --,[LocalAttendanceOutcomeID]
      ,[LocalAttendanceOutcomeCode]
      ,[LocalAttendanceOutcome]
      --,[NationalAttendanceOutcomeID]
      ,[NationalAttendanceOutcomeCode]
      ,[NationalAttendanceOutcome]
  FROM [WarehouseOLAPMergedV2].[OP].[AttendanceOutcome]


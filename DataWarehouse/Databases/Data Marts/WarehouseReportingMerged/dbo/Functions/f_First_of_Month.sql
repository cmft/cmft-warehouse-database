﻿

create function [dbo].[f_First_of_Month] (
	@date datetime
)
returns datetime
as

begin
	if @date is null return null;
	
	declare @first datetime
	set @first = convert(datetime, '01/' + convert(varchar(2), datepart(month, @date)) + '/' + convert(varchar(4), datepart(year, @date)), 103)
	return(@first)
end



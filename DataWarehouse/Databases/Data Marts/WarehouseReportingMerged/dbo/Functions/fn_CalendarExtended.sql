﻿


CREATE FUNCTION [dbo].[fn_CalendarExtended]
(
	@DateId		INT
)
RETURNS TABLE AS
RETURN
(
/******************************************************************************
**  Name: fn_CalendarExtended
**  Purpose: Additional calendar date columns in addition to the standard
**           Calendar columns
**
**  For this function to perform correctly set a DATEFIRST value in the calling code
**  eg SET DATEFIRST 1 for a Monday start of week
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 06.12.13   MH         Created
******************************************************************************/	

	SELECT 
		 FinancialWeekNo		= YEAR(CAL.TheDate) * 52 + DATEPART(WK, CAL.TheDate) - FCAL.FinancialYearStartWkNo + FCAL.WeekIncAdjustment
		,CAL.*
		
	FROM 
		WH.Calendar CAL
		CROSS APPLY
		(
			SELECT
				 FinancialYearStartWkNo		= YEAR(C.TheDate) * 52 + DATEPART(WK, C.TheDate)
				 -- Adjustment to the derived week number, if the first week is a full week the start at Wk 1, if not start at Wk 0
				,WeekIncAdjustment			= CASE WHEN C.TheDate = C.FirstDayOfWeek THEN 1 ELSE 0 END
			FROM
				(
					-- Get the financial year start date
					SELECT 
						FinancialYearStartID	= MIN(DateID) 
					FROM 
						WH.Calendar
					WHERE 
						FinancialYear = CAL.FinancialYear
				) BD
				INNER JOIN WH.Calendar C
					ON BD.FinancialYearStartID = C.DateID
		) FCAL

	WHERE 
		CAL.DateID  = @DateId 
)



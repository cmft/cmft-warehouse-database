﻿
CREATE FUNCTION [dbo].[fn_EpisodeDiagnosisCodesToTable]
(
	 @MergeEncounterRecNo		INT
)
RETURNS TABLE AS
RETURN
(

	/******************************************************************************
	**  Name: fn_EpisodeDiagnosisCodesToTable
	**  Purpose: 
	**
	**  Returns a row for each diagnosis code in the episode
	**
	**	Parameters
	**
	**	@MergeEncounterRecNo	- Primary key value for the APC.Encounter table
	**
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 24.01.14    MH         Created
	******************************************************************************/ 
		
	SELECT
		 MergeEncounterRecNo		= DC.MergeEncounterRecNo
		,Code						= DC.DiagnosisCode
		,Position					= DC.DiagnosisReference
		,[Name]						= DIAG.Diagnosis
		,FullName					= DIAG.DiagnosisCode + ' - ' + DIAG.Diagnosis
		,IsRCode					= DIAG.IsRCodeASINT
		,IsRCodeCodingOpporunity	= DIAG.IsRCodeCodingOpportunity
	FROM
	(
		SELECT
			 MergeEncounterRecNo
			,D.DiagnosisCode
			,D.DiagnosisReference
		FROM		
			APC.Encounter CE
			CROSS APPLY
			(
				SELECT DiagnosisCode = CE.PrimaryDiagnosisCode, DiagnosisReference = 1
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode1, 2
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode2, 3
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode3, 4
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode4, 5
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode5, 6
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode6, 7
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode7, 8
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode8, 9
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode9, 10
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode10, 11
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode11, 12
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode12, 13
				UNION ALL
				SELECT CE.SecondaryDiagnosisCode13, 14
			) D
		WHERE
				CE.MergeEncounterRecNo = @MergeEncounterRecNo
			AND D.DiagnosisCode IS NOT NULL
	) DC
	CROSS APPLY  dbo.fn_DiagnosisCodeExtension(DC.DiagnosisCode) DIAG
					
)


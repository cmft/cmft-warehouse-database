﻿CREATE FUNCTION [fn_ListToTable]
(
	@List VARCHAR(MAX),
	@Delim CHAR
)
RETURNS
	@ParsedList TABLE
	(
		item VARCHAR(MAX)
	)
AS

	/******************************************************************************
	**  Name: fn_ListToTable
	**  Purpose: Takes a delimited list eg AA,BBB,FFFF and returns rows containing the
	**           the element values
	**           Eg  AA
	**               BB
	**               FFFF
	**
	**			Useful when you want to dynamically use a delimited list within a SQL IN
	**          statement
	**
	**			SELECT * FROM T WHERE T.ID IN (SELECT * FROM fn_ListToTable('AA,BBB,FFFF',','))
	**
	**  Created: 24/06/2010'M Hodson
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------    
	******************************************************************************/

BEGIN
	DECLARE @item VARCHAR(MAX) 
	DECLARE @Pos INT
	
	SET @List = LTRIM(RTRIM(@List))+ @Delim
	SET @Pos = CHARINDEX(@Delim, @List, 1)
	
	WHILE @Pos > 0
	 BEGIN
		SET @item = LTRIM(RTRIM(LEFT(@List, @Pos - 1)))
		
		IF @item <> ''
		BEGIN
		  INSERT INTO @ParsedList (item)
		  VALUES (CAST(@item AS VARCHAR(MAX)))
		END
		
		SET @List = RIGHT(@List, LEN(@List) - @Pos)
		SET @Pos = CHARINDEX(@Delim, @List, 1)
	 END
	
	RETURN
END




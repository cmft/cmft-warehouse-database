﻿

	
create FUNCTION dbo.[f_GetWardCode]
(
	 @ProviderSpellNo	VARCHAR(20)
	,@WardTime			Datetime
)
RETURNS varchar(100) 
AS  
BEGIN
DECLARE @SourceWardCode varchar(100) = Null

SELECT @SourceWardCode			= Ward.SourceWardCode
FROM APC.WardStay WS
INNER JOIN APC.Ward
	ON WS.WardID = ward.SourceWardID
WHERE @WardTime BETWEEN WS.StartTime AND ISNULL(WS.EndTime,GETDATE())
AND @ProviderSpellNo = WS.ProviderSpellNo

if @SourceWardCode is null
begin
SELECT TOP 1 @SourceWardCode			= Ward.SourceWardCode
FROM APC.WardStay WS
INNER JOIN APC.Ward
	ON WS.WardID = ward.SourceWardID
WHERE @WardTime BETWEEN WS.StartDate AND ISNULL(WS.EndTime,GETDATE())
AND @ProviderSpellNo = WS.ProviderSpellNo

end

RETURN @SourceWardCode
END

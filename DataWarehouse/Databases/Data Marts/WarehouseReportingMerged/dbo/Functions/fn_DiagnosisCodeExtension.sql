﻿ 
CREATE FUNCTION [dbo].[fn_DiagnosisCodeExtension]
(
	 @DiagnosisCode		VARCHAR(6)
)
RETURNS TABLE AS
RETURN
(

	/******************************************************************************
	**  Name: fn_DiagnosisCodeExtension
	**  Purpose: 
	**
	**  Returns a row for a diagnosis code with additional derived columns
	**  
	**	Parameters
	**
	**	@DiagnosisCode	- Diagnosis Code to lookup
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 24.01.14    MH         Created
	******************************************************************************/ 
		
	SELECT
		 DIAG.*
		--,IsRCodeCodingOpportunity	= CASE				
		--								WHEN
		--										DIAG.DiagnosisCode LIKE ('R07.[1-4]')				
		--									OR	DIAG.DiagnosisCode LIKE ('R10.[0-4]')				
		--									OR	DIAG.DiagnosisCode LIKE ('R50.[019]')				
		--									OR	DIAG.DiagnosisCode IN   ('R11.X','R53.X','R55.X')	
		--								THEN 1
		--								ELSE 0
		--						     END
		--Already in the view so not required in teh function
		,IsRCodeASINT				= CAST(DIAG.IsRCode AS INT)
	FROM
		WH.Diagnosis DIAG
		
	WHERE
		DIAG.DiagnosisCode		= @DiagnosisCode
)



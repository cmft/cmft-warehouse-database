﻿


CREATE view [Subscription].[DailyMedicalWardOutliers]


as

select 
	   [SubscriptionID]
      ,[ReportName]
      ,[To]
      ,[Cc]
      ,[Bcc]
      ,[ReplyTo]
      ,[IncludeReport]
      ,[RenderFormat]
      ,[Priority]
      ,[Subject]
      ,[Comment]
      ,[IncludeLink]
      ,[Parameter1Value]
	  ,[Parameter2Value]
	  ,[Parameter3Value] 
      ,[Parameter4Value]
      ,[Parameter5Value]
      ,[Parameter6Value]
      ,[Parameter7Value]
      ,[Parameter8Value]
      ,[Parameter9Value]
      ,[Parameter10Value]
from 
	[WarehouseReportingMerged].[WH].[ReportSubscription]
where 
	ReportName = 'Daily Medical Ward Outliers'
and ActiveFlag = 1





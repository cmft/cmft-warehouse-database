﻿



/****** Script for SelectTopNRows command from SSMS  ******/

create view [Subscription].[CommunityServiceOutcomeReport] as

SELECT 
	   [SubscriptionID]
      ,[ReportName]
      ,[To]
      ,[Cc]
      ,[Bcc]
      ,[ReplyTo]
      ,[IncludeReport]
      ,[RenderFormat]
      ,[Priority]
      ,[Subject]
      ,[Comment]
      ,[IncludeLink]
      ,[Parameter1Value]
     -- ,[Parameter2Value] = '[Calendar].[Financial Week Hierarchy].[Financial Year].&[2012/2013].&[201307]'
	  ,[Parameter2Value] = 
					(
					select
						'[Calendar].[Financial Week Hierarchy].[Financial Year].&[' + FinancialYear + '].&[' + WeekNoKey + ']'

					from
						warehousesql.warehousereportingmerged.wh.Calendar 
					where 
						convert(date,thedate) = convert(date,dateadd(day,1-datepart(dw, getdate()), getdate()) ) 
					)
      ,[Parameter3Value]
      ,[Parameter4Value]
      ,[Parameter5Value]
      ,[Parameter6Value]
      ,[Parameter7Value]
      ,[Parameter8Value]
      ,[Parameter9Value]
      ,[Parameter10Value]
FROM 
	[WarehouseReportingMerged].[WH].[ReportSubscription]
where 
	reportname = 'Community Service Outcome Report'
and activeFlag = 1




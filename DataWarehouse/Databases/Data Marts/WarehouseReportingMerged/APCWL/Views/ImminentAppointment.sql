﻿






CREATE view [APCWL].[ImminentAppointment]
as

/******************************************************************************************************
View		: [APCWL].[ImminentAppointment]
Description	: Created for use of appointment reminder data feed(s), including Healthcare
				Communications Envoy system (for Central appointments).

Modification History
====================

Date		Person			Description
=======================================================================================================
05/11/2014	Paul Egan       Initial coding.
19/11/2014	Paul Egan		Corrected Appointment Date format.
25/11/2014	Paul Egan		Added 9am pseudo appointment time so Healthcare Comms template does not fail. (Patients will
							not be sent an appointment time in the text), until TCITime is ETL'd.
01/12/2014	Paul Egan		Added site description to Location field.
05/02/2015	Paul Egan		Corrected TCIDate for cancellations - now using FutureCancellationDate.
23/03/2015	Paul Egan		Added TCITime and FutureCancellationTime.
*******************************************************************************************************/

with allAPCAppointmentsCTE as
(
select
	 patient_id = WLEncounter.DistrictNo
	,appointment_date = 
		case
			when WLEncounter.CancelledBy is null then convert(varchar(10), WLEncounter.TCIDate, 103)
			else convert(varchar(10), WLEncounter.FutureCancellationDate, 103)
		end
	,appointment_time = 
		case
			when WLEncounter.CancelledBy is null then left(cast(WLEncounter.TCITime as time(0)), 5)	
			else left(cast(WLEncounter.FutureCancellationTime as time(0)), 5)
		end
	,ActiveAppointmentDaySeq =		-- Enables selecting only first appointment of the day for each patient.
		case
			when WLEncounter.CancelledBy is null then		
				row_number() over(partition by WLEncounter.DistrictNo, WLEncounter.TCIDate order by WLEncounter.TCITime, WLEncounter.WaitingListCode)  -- Uses WaitingListCode as tie-breaker
			else null
		end
	,mobile_number = 
		case
			when left(Patient.MobilePhone , 2) = '07' then replace(Patient.MobilePhone, ' ','')
			when left(Patient.HomePhone , 2) = '07' then replace(Patient.HomePhone, ' ','')
			when left(Patient.WorkPhone , 2) = '07' then replace(Patient.WorkPhone, ' ','')
			else null
		end
	,landline_number = 
		case
			when left(Patient.HomePhone , 2) <> '07' then replace(Patient.HomePhone, ' ','')
			when left(Patient.MobilePhone , 2) <> '07' then replace(Patient.MobilePhone, ' ','')
			else null
		end
	,country_code = '44'
	,contact_tel_no = NULL --coalesce(LookupPASSpecialty.[Description], LookupDivision.[Description])
	,first_name = WLEncounter.PatientForename
	,last_name = WLEncounter.PatientSurname
	,dob = convert(varchar(10), WLEncounter.DateOfBirth, 103)			-- dd/mm/yyyy format
	,cancel = 
		case
			when WLEncounter.CancelledBy is null then null
			else 'cancel'
		end
	,specialty_code = Specialty.NationalSpecialtyCode
	,clinic_code = WLEncounter.IntendedPrimaryOperationCode
	,appt_audit_ref = WLEncounter.WaitingListCode
	,appt_type = null
	,location = [Site].LocalSite
	,practitioner = Consultant.Title + ' ' + Consultant.Surname
	,procedure_code = null
	,patient_email = null
	,genericinfo1 = null
	,genericinfo2 = WLEncounter.SexCode
	,genericinfo3 = WLEncounter.Postcode
	,CensusDate = cast(WLEncounter.CensusDate as date)	-- Validation only
	,ExtractRunTime = getdate()							-- Validation only
	,WLEncounter.MergeEncounterRecno					-- Validation only
	,WLEncounter.CancelledBy							-- Validation only
	,WLEncounter.FutureCancellationDate					-- Validation only
	,WLEncounter.PASSpecialtyCode						-- Validation only
	,EncounterTypeCode = 'APC'							-- Validation only - Added this column to differentiate OP and APC datasets if we need to union.
	,WLEncounter.ManagementIntentionCode				-- Validation only
	,WLEncounter.AdmissionMethodCode					-- Validation only
	,WLEncounter.WardCode								-- Validation only
	,WLEncounter.SiteCode								-- Validation only
	,Directorate.Division								-- Validation only
from 
	APCWL.Encounter WLEncounter
	
	/* Join on patient to get mobile phone numbers */
	inner join WarehouseReporting.PAS.Patient			
	on	Patient.SourcePatientNo = WLEncounter.SourcePatientNo
	
	inner join WH.Directorate
	on	WLEncounter.DirectorateCode = Directorate.DirectorateCode
	
	inner join WH.Specialty	
	on	WLEncounter.SpecialtyID = Specialty.SourceSpecialtyID
	
	inner join WH.[Site]
	on WLEncounter.SiteID = [Site].SourceSiteID
	
	left join WH.Consultant			-- Inner join performance issue
	on	WLEncounter.ConsultantID = Consultant.SourceConsultantID
	and Consultant.SourceContextCode = 'CEN||PAS'
	
	
	/*	Must use left join so LookupPASSpecialty and LookupDivision do not conflict.
		(Non-matches are removed in the WHERE clause) */
	----------left join WarehouseOLAPMergedV2.Utility.EntityLookup LookupPASSpecialty
	----------on	LookupPASSpecialty.EntityCode = WLEncounter.SpecialtyCode
	----------and LookupPASSpecialty.EntityTypeCode = 'AppReminderPASSpecialty'
	----------and WLEncounter.ContextCode = 'cen||pas'

	----------/*	Must use left join so LookupPASSpecialty and LookupDivision do not conflict.
	----------	(Non-matches are removed in the WHERE clause) */
	----------left join WarehouseOLAPMergedV2.Utility.EntityLookup LookupDivision
	----------on	LookupDivision.EntityCode = Directorate.Division
	----------and LookupDivision.EntityTypeCode = 'AppReminderDivision'
	----------and WLEncounter.ContextCode = 'cen||pas'
	/*	NB This join would create duplicates with any specialties in LookupPASSpecialty 
		under the same division, but these are eliminated in the WHERE clause Paul Egan 11/09/2014 */
		
where
	----------WLEncounter.PASSpecialtyCode = 'GEND'			-- Validation only
	----------and WLEncounter.ManagementIntentionCode = 'D'	-- Validation only (Endoscopy only want Day Cases
	----------Directorate.Division = 'Surgical'
	WLEncounter.ContextCode = 'cen||pas'				-- for now anyway - I think Trafford is currently using Mjog
	and coalesce(WLEncounter.WardCode,'') not like 'SUB%'	-- Use coalesce to not exclude NULL ward code
	----------and
	----------(
	----------	LookupPASSpecialty.EntityCode is not null 
	----------	or 
	----------	LookupDivision.EntityCode is not null  -- This also removes duplicates from any specialties in LookupPASSpecialty under the same division
	----------)
	and
	(
		/* Active imminent appointments */
		(
		WLEncounter.CensusDate = (select max(censusDate) from WarehouseReportingMerged.APCWL.Encounter)
		and
		WLEncounter.TCIDate between cast(dateadd(day, 1, getdate()) as date) and cast(dateadd(day, 14, getdate()) as date)
		and
		WLEncounter.CancelledBy is null
		)
		or
		/* Cancelled imminent appointments, cancelled recently (we don't want appointments cancelled weeks ago (or more)) */
		(
		/* Need multiple census dates for cancellations to make sure we get all recent cancellations ??? */
		WLEncounter.CensusDate = (select max(censusDate) from WarehouseReportingMerged.APCWL.Encounter)
		and
		WLEncounter.FutureCancellationDate between cast(getdate() as date) and cast(dateadd(day, 14, getdate()) as date)
		and
		WLEncounter.CancelledBy is not null
		----------WLEncounter.FutureCancellationDate between cast(dateadd(day, -7, getdate()) as date) and cast(getdate() as date)
		)
	)
----------order by
----------	 WLEncounter.CancelledBy
----------	,WLEncounter.CensusDate
----------	,WLEncounter.TCIDate
----------	,WLEncounter.WaitingListCode
)

select
	 patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name
	,last_name
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,appt_audit_ref
	,appt_type
	,location
	,practitioner
	,procedure_code
	,patient_email
	,genericinfo1
	,genericinfo2
	,genericinfo3
	,CensusDate						-- Validation only
	,ExtractRunTime					-- Validation only
	,MergeEncounterRecno			-- Validation only
	,CancelledBy					-- Validation only
	,FutureCancellationDate			-- Validation only
	,PASSpecialtyCode				-- Validation only
	,EncounterTypeCode				-- Validation only - Added this column to differentiate OP and APC datasets if we need to union.
	,ManagementIntentionCode		-- Validation only
	,AdmissionMethodCode			-- Validation only
	,WardCode						-- Validation only
	,SiteCode						-- Validation only
	,Division						-- Validation only
from 
	allAPCAppointmentsCTE
where
	(
	ActiveAppointmentDaySeq = 1			-- First appointment of the day (waiting for TCITime added to ETL)
	or ActiveAppointmentDaySeq is null		-- All cancellations
	)
--order by
--	 CancelledBy
--	--,CensusDate
--	,appointment_date
--	,appointment_time
--	,clinic_code









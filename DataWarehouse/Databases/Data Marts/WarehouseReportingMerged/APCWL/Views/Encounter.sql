﻿


CREATE view [APCWL].[Encounter] as

/* 
==============================================================================================
Description:

When		Who			TFS		What
?			?					Initial Coding
20150320	Paul Egan			Added TCITime and FutureCancellationTime
20150421	Paul Egan	5487	Added CCGCode and ResidenceCCGCode
===============================================================================================
*/


select
	 BaseEncounter.MergeEncounterRecno
	,BaseEncounter.EncounterRecno
	,BaseEncounter.InterfaceCode
	,BaseEncounter.SourcePatientNo
	,BaseEncounter.SourceEncounterNo
	,BaseEncounter.CensusDate
	,BaseEncounter.PatientSurname
	,BaseEncounter.PatientForename
	,BaseEncounter.PatientTitle
	,BaseEncounter.SexCode
	,BaseEncounter.DateOfBirth
	,BaseEncounter.DateOfDeath
	,BaseEncounter.NHSNumber
	,BaseEncounter.DistrictNo
	,BaseEncounter.MaritalStatusCode
	,BaseEncounter.ReligionCode
	,BaseEncounter.Postcode
	,BaseEncounter.PatientsAddress1
	,BaseEncounter.PatientsAddress2
	,BaseEncounter.PatientsAddress3
	,BaseEncounter.PatientsAddress4
	,BaseEncounter.DHACode
	,BaseEncounter.HomePhone
	,BaseEncounter.WorkPhone
	,BaseEncounter.EthnicOriginCode
	,BaseEncounter.ConsultantCode
	,BaseEncounter.SpecialtyCode
	,BaseEncounter.PASSpecialtyCode
	,BaseEncounter.EpisodeSpecialtyCode
	,BaseEncounter.ManagementIntentionCode
	,BaseEncounter.AdmissionMethodCode
	,BaseEncounter.PriorityCode
	,BaseEncounter.WaitingListCode
	,BaseEncounter.CommentClinical
	,BaseEncounter.CommentNonClinical
	,BaseEncounter.IntendedPrimaryOperationCode
	,BaseEncounter.Operation
	,BaseEncounter.SiteCode
	,BaseEncounter.WardCode
	,BaseEncounter.WLStatus
	,BaseEncounter.ProviderCode
	,BaseEncounter.PurchaserCode
	,BaseEncounter.ContractSerialNumber
	,BaseEncounter.CancelledBy
	,BaseEncounter.BookingTypeCode
	,BaseEncounter.CasenoteNumber
	,BaseEncounter.OriginalDateOnWaitingList
	,BaseEncounter.DateOnWaitingList
	,BaseEncounter.TCIDate
	,BaseEncounter.TCITime
	,BaseEncounter.KornerWait
	,BaseEncounter.CountOfDaysSuspended
	,BaseEncounter.SuspensionStartDate
	,BaseEncounter.SuspensionEndDate
	,BaseEncounter.SuspensionReasonCode
	,BaseEncounter.SuspensionReason
	,BaseEncounter.EpisodicGpCode
	,BaseEncounter.RegisteredGpPracticeCode
	,BaseEncounter.EpisodicGpPracticeCode
	,BaseEncounter.RegisteredGpCode
	,BaseEncounter.RegisteredPracticeCode
	,BaseEncounter.SourceTreatmentFunctionCode
	,BaseEncounter.TreatmentFunctionCode
	,BaseEncounter.NationalSpecialtyCode
	,BaseEncounter.PCTCode
	,BaseEncounter.BreachDate
	,BaseEncounter.ExpectedAdmissionDate
	,BaseEncounter.ReferralDate
	,BaseEncounter.ExpectedLOS
	,BaseEncounter.ProcedureDate
	,BaseEncounter.FutureCancellationDate
	,BaseEncounter.FutureCancellationTime
	,BaseEncounter.TheatreKey
	,BaseEncounter.TheatreInterfaceCode
	,BaseEncounter.EpisodeNo
	,BaseEncounter.BreachDays
	,BaseEncounter.MRSA
	,BaseEncounter.RTTPathwayID
	,BaseEncounter.RTTPathwayCondition
	,BaseEncounter.RTTStartDate
	,BaseEncounter.RTTEndDate
	,BaseEncounter.RTTSpecialtyCode
	,BaseEncounter.RTTCurrentProviderCode
	,BaseEncounter.RTTCurrentStatusCode
	,BaseEncounter.RTTCurrentStatusDate
	,BaseEncounter.RTTCurrentPrivatePatientFlag
	,BaseEncounter.RTTOverseasStatusFlag
	,BaseEncounter.NationalBreachDate
	,BaseEncounter.NationalBreachDays
	,BaseEncounter.DerivedBreachDays
	,BaseEncounter.DerivedClockStartDate
	,BaseEncounter.DerivedBreachDate
	,BaseEncounter.RTTBreachDate
	,BaseEncounter.RTTDiagnosticBreachDate
	,BaseEncounter.NationalDiagnosticBreachDate
	,BaseEncounter.SocialSuspensionDays
	,BaseEncounter.AgeCode
	,BaseEncounter.Cases
	,BaseEncounter.LengthOfWait
	,BaseEncounter.DurationCode
	,BaseEncounter.WaitTypeCode
	,BaseEncounter.StatusCode
	,BaseEncounter.CategoryCode
	,BaseEncounter.AddedToWaitingListTime
	,BaseEncounter.AddedLastWeek
	,BaseEncounter.WaitingListAddMinutes
	,BaseEncounter.OPCSCoded
	,BaseEncounter.WithRTTStartDate
	,BaseEncounter.WithRTTStatusCode
	,BaseEncounter.WithExpectedAdmissionDate
	,BaseEncounter.SourceUniqueID
	,BaseEncounter.AdminCategoryCode
	,BaseEncounter.RTTActivity
	,BaseEncounter.RTTBreachStatusCode
	,BaseEncounter.DiagnosticProcedure
	,BaseEncounter.DirectorateCode
	,BaseEncounter.GuaranteedAdmissionDate
	,BaseEncounter.DurationAtTCIDateCode
	,BaseEncounter.WithTCIDate
	,BaseEncounter.WithGuaranteedAdmissionDate
	,BaseEncounter.WithRTTOpenPathway
	,BaseEncounter.RTTPathwayStartDateCurrent
	,BaseEncounter.RTTWeekBandReturnCode
	,BaseEncounter.RTTDaysWaiting
	,BaseEncounter.AdmissionReason
	,BaseEncounter.ContextCode
	,BaseEncounter.CCGCode				-- Added Paul Egan 20150421
	,BaseEncounter.ResidenceCCGCode		-- Added Paul Egan 20150421

	,BaseEncounterReference.ContextID
	,BaseEncounterReference.AgeID
	,BaseEncounterReference.SexID
	,BaseEncounterReference.EthnicOriginID
	,BaseEncounterReference.MaritalStatusID
	,BaseEncounterReference.SpecialtyID
	,BaseEncounterReference.PASSpecialtyID
	,BaseEncounterReference.EpisodicSpecialtyID
	,BaseEncounterReference.RTTSpecialtyID
	,BaseEncounterReference.IntendedManagementID
	,BaseEncounterReference.AdmissionMethodID
	,BaseEncounterReference.PriorityID
	,BaseEncounterReference.RTTCurrentStatusID
	,BaseEncounterReference.ConsultantID
	,BaseEncounterReference.SiteID
	,BaseEncounterReference.WardID
	,BaseEncounterReference.WaitingListID
	,BaseEncounterReference.AdminCategoryID
	,BaseEncounterReference.CensusDateID
	,BaseEncounterReference.TCIDateID
	,BaseEncounterReference.WaitTypeID
	,BaseEncounterReference.DurationID
	,BaseEncounterReference.TCIDateDurationID
	,BaseEncounterReference.WaitStatusID
from
	WarehouseOLAPMergedV2.APCWL.BaseEncounter

inner join WarehouseOLAPMergedV2.APCWL.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno




﻿
CREATE view [CDS].[OPAnglia] as

SELECT
 	 UniqueEpisodeSerialNo =
		'BRW300' +
		case
		when Encounter.ContextCode like 'TRA||%' then coalesce(Encounter.SourceUniqueID , '')
		when Encounter.ContextCode like 'CEN||%' then
			cast(Encounter.SourcePatientNo as varchar) + 
			'*' + 
			CAST(Encounter.SourceEncounterNo as varchar) + 
			'*' +
			convert(varchar , Encounter.AppointmentDate, 112) +
			'*' +
			cast(
				ROW_NUMBER() over 
					(
					partition by 
						 Encounter.SourcePatientNo
						,Encounter.SourceEncounterNo
					order by
						 Encounter.AppointmentTime
						,Encounter.DoctorCode
					)
				as varchar
			)
		end

	,OPAngliaBase.UpdateType
	,OPAngliaBase.CDSUpdateDate
	,OPAngliaBase.CDSUpdateTime
	,OPAngliaBase.CDSType
	,OPAngliaBase.ProviderCode
	,OPAngliaBase.PurchaserCode
	,OPAngliaBase.CommissioningSerialNo
	,OPAngliaBase.NHSServiceAgreementLineNo
	,OPAngliaBase.PurchaserReferenceNo
	,OPAngliaBase.NHSNumber
	,OPAngliaBase.PatientName
	,OPAngliaBase.NameFormatCode
	,OPAngliaBase.PatientsAddress
	,OPAngliaBase.Postcode
	,OPAngliaBase.HAofResidenceCode
	,OPAngliaBase.SexCode
	,OPAngliaBase.CarerSupportIndicator
	,OPAngliaBase.DateOfBirth
	,OPAngliaBase.DateOfBirthStatus
	,OPAngliaBase.RegisteredGpCode
	,OPAngliaBase.RegisteredGpPracticeCode
	,OPAngliaBase.DistrictNo
	,OPAngliaBase.ReferralCode
	,OPAngliaBase.ReferringOrganisationCode
	,OPAngliaBase.ServiceTypeRequested
	,OPAngliaBase.ReferralRequestReceivedDate
	,OPAngliaBase.ReferralRequestReceivedDateStatus
	,OPAngliaBase.PriorityType
	,OPAngliaBase.SourceOfReferralCode
	,OPAngliaBase.MainSpecialtyCode
	,OPAngliaBase.TreatmentFunctionCode
	,OPAngliaBase.LocalSubSpecialtyCode
	,OPAngliaBase.ClinicPurpose
	,OPAngliaBase.ConsultantCode
	,OPAngliaBase.AttendanceIdentifier
	,OPAngliaBase.AdminCategoryCode
	,OPAngliaBase.LocationTypeCode
	,OPAngliaBase.SiteCode
	,OPAngliaBase.MedicalStaffTypeCode
	,OPAngliaBase.AttendanceDate
	,OPAngliaBase.FirstAttendanceFlag
	,OPAngliaBase.DNAFlag
	,OPAngliaBase.AttendanceOutcomeCode
	,OPAngliaBase.LastDNAorPatientCancelledDate
	,OPAngliaBase.LastDNAorPatientCancelledDateStatus
	,OPAngliaBase.PrimaryDiagnosisCode
	,OPAngliaBase.SubsidiaryDiagnosisCode
	,OPAngliaBase.SecondaryDiagnosisCode1
	,OPAngliaBase.ReadPrimaryDiagnosisCode
	,OPAngliaBase.ReadSubsidiaryDiagnosisCode
	,OPAngliaBase.ReadSecondaryDiagnosisCode1
	,OPAngliaBase.OperationStatus
	,OPAngliaBase.PrimaryOperationCode
	,OPAngliaBase.OperationCode2
	,OPAngliaBase.OperationCode3
	,OPAngliaBase.OperationCode4
	,OPAngliaBase.OperationCode5
	,OPAngliaBase.OperationCode6
	,OPAngliaBase.OperationCode7
	,OPAngliaBase.OperationCode8
	,OPAngliaBase.OperationCode9
	,OPAngliaBase.OperationCode10
	,OPAngliaBase.OperationCode11
	,OPAngliaBase.OperationCode12
	,OPAngliaBase.ReadPrimaryOperationCode
	,OPAngliaBase.ReadOperationCode2
	,OPAngliaBase.ReadOperationCode3
	,OPAngliaBase.ReadOperationCode4
	,OPAngliaBase.ReadOperationCode5
	,OPAngliaBase.ReadOperationCode6
	,OPAngliaBase.ReadOperationCode7
	,OPAngliaBase.ReadOperationCode8
	,OPAngliaBase.ReadOperationCode9
	,OPAngliaBase.ReadOperationCode10
	,OPAngliaBase.ReadOperationCode11
	,OPAngliaBase.ReadOperationCode12
	,OPAngliaBase.PatientSexAgeMix
	,OPAngliaBase.IntendedClinicalCareIntensity
	,OPAngliaBase.BroadPatientGroup
	,OPAngliaBase.CasenoteNumber
	,OPAngliaBase.EthnicGroupCode
	,OPAngliaBase.AttendanceCategoryCode
	,OPAngliaBase.AppointmentRequiredDate
	,OPAngliaBase.MaritalStatusCode
	,OPAngliaBase.GPDiagnosisCode
	,OPAngliaBase.ReadGPDiagnosisCode
	,OPAngliaBase.PrimaryOperationGroupCode
	,OPAngliaBase.SecondaryOperationGroupCode1
	,OPAngliaBase.SecondaryOperationGroupCode2
	,OPAngliaBase.SecondaryProcedureGroupCode3
	,OPAngliaBase.SecondaryDiagnosisCode2
	,OPAngliaBase.SecondaryDiagnosisCode3
	,OPAngliaBase.SecondaryDiagnosisCode4
	,OPAngliaBase.ReadSecondaryDiagnosisCode2
	,OPAngliaBase.ReadSecondaryDiagnosisCode3
	,OPAngliaBase.ReadSecondaryDiagnosisCode4
	,OPAngliaBase.FundingType
	,OPAngliaBase.GPFundholderCode

	,AppointmentHospitalCode = null

	,OPAngliaBase.ReferralHospitalCode
	,OPAngliaBase.NNNStatusIndicator
	,OPAngliaBase.BlankPad
	,OPAngliaBase.CancellationDate
	,OPAngliaBase.CancelledBy
	,OPAngliaBase.DeathIndicator
	,OPAngliaBase.LocalClinicCode
	,OPAngliaBase.LocalWardCode
	,OPAngliaBase.PCTHealthAuthorityCode
	,OPAngliaBase.PCTofResidenceCode
	,OPAngliaBase.ResponsiblePCTHealthAuthorityCode
	,CommissionerCode = 
		NULL
		--OPAngliaBase.CommissionerCode
	,AgencyActingOnBehalfOfDoH = 
		NULL
		--OPAngliaBase.AgencyActingOnBehalfOfDoH
	,OPAngliaBase.HRGCode
	,OPAngliaBase.HRGVersionNumber
	,OPAngliaBase.DGVPCode
	,OPAngliaBase.ReadDGVPCode
	,OPAngliaBase.DerivedFirstAttendance
	,OPAngliaBase.PCTResponsible
	,OPAngliaBase.AgeAtCDSActivityDate
	,OPAngliaBase.UniqueBookingReferenceNumber
	,OPAngliaBase.RTTPathwayID
	,OPAngliaBase.RTTCurrentProviderCode
	,OPAngliaBase.RTTPeriodStatusCode
	,OPAngliaBase.RTTStartDate
	,OPAngliaBase.RTTEndDate
	,OPAngliaBase.EarliestReasonableOfferDate
	,OPAngliaBase.EndOfRecord

from
	CDS.OPAngliaBase

inner join WarehouseOLAPMergedV2.OP.BaseEncounter Encounter
on	Encounter.MergeEncounterRecno = OPAngliaBase.MergeEncounterRecno


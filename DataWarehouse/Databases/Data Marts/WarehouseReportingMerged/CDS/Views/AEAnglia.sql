﻿
CREATE view [CDS].[AEAnglia] as

select
	 CDSMessageType = 'NHSCDS'
	,CDSVersionNo = 'CDS006'
	,CDSMessageReference = (ROW_NUMBER() OVER (ORDER BY MergeEncounterRecno))

	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientName
	,PatientAddress
	,Postcode
	,PCTofResidence
	,DateOfBirth
	,SexCode
	,CarerSupportIndicator
	,EthnicCategoryCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,DepartmentTypeCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,DiagnosisSchemeCode
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,DiagnosisCode1
	,DiagnosisCode2
	,DiagnosisCode3
	,DiagnosisCode4
	,DiagnosisCode5
	,DiagnosisCode6
	,DiagnosisCode7
	,DiagnosisCode8
	,DiagnosisCode9
	,DiagnosisCode10
	,DiagnosisCode11
	,DiagnosisCode12
	,InvestigationSchemeCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,InvestigationCode1
	,InvestigationCode2
	,InvestigationCode3
	,InvestigationCode4
	,InvestigationCode5
	,InvestigationCode6
	,InvestigationCode7
	,InvestigationCode8
	,InvestigationCode9
	,InvestigationCode10
	,TreatmentSchemeCode
	,TreatmentCodeFirst
	,TreatmentDateFirst
	,TreatmentCodeSecond
	,TreatmentDateSecond
	,TreatmentCode1
	,TreatmentDate1
	,TreatmentCode2
	,TreatmentDate2
	,TreatmentCode3
	,TreatmentDate3
	,TreatmentCode4
	,TreatmentDate4
	,TreatmentCode5
	,TreatmentDate5
	,TreatmentCode6
	,TreatmentDate6
	,TreatmentCode7
	,TreatmentDate7
	,TreatmentCode8
	,TreatmentDate8
	,TreatmentCode9
	,TreatmentDate9
	,TreatmentCode10
	,TreatmentDate10
	,HRGCode = null
	,HRGVersionCode = null
	,HRGProcedureSchemeCode
	,DGVPCode

	,CDSMessageReferenceTrailer = (ROW_NUMBER() OVER (ORDER BY MergeEncounterRecno))
from
	CDS.AEAngliaBase


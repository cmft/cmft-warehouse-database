﻿
CREATE view [CDS].[OPAngliaNetExtract] as

SELECT
 	 UniqueEpisodeSerialNo = left(UniqueEpisodeSerialNo , 35)
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,NameFormatCode
	,PatientsAddress
	,Postcode
	,HAofResidenceCode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,DateOfBirthStatus
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,ReferralCode
	,ReferringOrganisationCode
	,ServiceTypeRequested
	,ReferralRequestReceivedDate
	,ReferralRequestReceivedDateStatus
	,PriorityType
	,SourceOfReferralCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,LocalSubSpecialtyCode

	,ClinicPurpose =
		left(ClinicPurpose , 15)

	,ConsultantCode
	,AttendanceIdentifier
	,AdminCategoryCode
	,LocationTypeCode
	,SiteCode
	,MedicalStaffTypeCode
	,AttendanceDate

	,FirstAttendanceFlag =
		left(FirstAttendanceFlag , 1)

	,DNAFlag
	,AttendanceOutcomeCode
	,LastDNAorPatientCancelledDate
	,LastDNAorPatientCancelledDateStatus
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,ReadPrimaryDiagnosisCode
	,ReadSubsidiaryDiagnosisCode
	,ReadSecondaryDiagnosisCode1
	,OperationStatus
	,PrimaryOperationCode
	,OperationCode2
	,OperationCode3
	,OperationCode4
	,OperationCode5
	,OperationCode6
	,OperationCode7
	,OperationCode8
	,OperationCode9
	,OperationCode10
	,OperationCode11
	,OperationCode12
	,ReadPrimaryOperationCode
	,ReadOperationCode2
	,ReadOperationCode3
	,ReadOperationCode4
	,ReadOperationCode5
	,ReadOperationCode6
	,ReadOperationCode7
	,ReadOperationCode8
	,ReadOperationCode9
	,ReadOperationCode10
	,ReadOperationCode11
	,ReadOperationCode12
	,PatientSexAgeMix
	,IntendedClinicalCareIntensity
	,BroadPatientGroup
	,CasenoteNumber
	,EthnicGroupCode
	,AttendanceCategoryCode
	,AppointmentRequiredDate
	,MaritalStatusCode
	,GPDiagnosisCode
	,ReadGPDiagnosisCode
	,PrimaryOperationGroupCode
	,SecondaryOperationGroupCode1
	,SecondaryOperationGroupCode2
	,SecondaryProcedureGroupCode3
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,ReadSecondaryDiagnosisCode2
	,ReadSecondaryDiagnosisCode3
	,ReadSecondaryDiagnosisCode4
	,FundingType
	,GPFundholderCode

	,AppointmentHospitalCode =
		space(2)

	,ReferralHospitalCode
	,NNNStatusIndicator
	,BlankPad
	,CancellationDate
	,CancelledBy
	,DeathIndicator
	,LocalClinicCode
	,LocalWardCode
	,PCTHealthAuthorityCode
	,PCTofResidenceCode
	,ResponsiblePCTHealthAuthorityCode

	,CommissionerCode = 
		space(5)
		--CommissionerCode

	,AgencyActingOnBehalfOfDoH = 
		space(5)
		--AgencyActingOnBehalfOfDoH

	,HRGCode
	,HRGVersionNumber
	,DGVPCode
	,ReadDGVPCode

	,DerivedFirstAttendance =
		left(DerivedFirstAttendance , 1)

	,PCTResponsible
	,AgeAtCDSActivityDate
	,UniqueBookingReferenceNumber
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,EarliestReasonableOfferDate
	,EndOfRecord

	,RowDelimiterColumn =
		null
from
	CDS.OPAngliaNet


﻿




CREATE view [CDS].[OPAngliaNet] as


--Updates - UpdateType = '9'
select
	 Base.UniqueEpisodeSerialNo
	,UpdateType = '9'
	,Base.CDSUpdateDate
	,Base.CDSUpdateTime
	,Base.CDSType
	,Base.ProviderCode
	,Base.PurchaserCode
	,Base.CommissioningSerialNo
	,Base.NHSServiceAgreementLineNo
	,Base.PurchaserReferenceNo
	,Base.NHSNumber
	,Base.PatientName
	,Base.NameFormatCode
	,Base.PatientsAddress
	,Base.Postcode
	,Base.HAofResidenceCode
	,Base.SexCode
	,Base.CarerSupportIndicator
	,Base.DateOfBirth
	,Base.DateOfBirthStatus
	,Base.RegisteredGpCode
	,Base.RegisteredGpPracticeCode
	,Base.DistrictNo
	,Base.ReferralCode
	,Base.ReferringOrganisationCode
	,Base.ServiceTypeRequested
	,Base.ReferralRequestReceivedDate
	,Base.ReferralRequestReceivedDateStatus
	,Base.PriorityType
	,Base.SourceOfReferralCode
	,Base.MainSpecialtyCode
	,Base.TreatmentFunctionCode
	,Base.LocalSubSpecialtyCode
	,Base.ClinicPurpose
	,Base.ConsultantCode
	,Base.AttendanceIdentifier
	,Base.AdminCategoryCode
	,Base.LocationTypeCode
	,Base.SiteCode
	,Base.MedicalStaffTypeCode
	,Base.AttendanceDate
	,Base.FirstAttendanceFlag
	,Base.DNAFlag
	,Base.AttendanceOutcomeCode
	,Base.LastDNAorPatientCancelledDate
	,Base.LastDNAorPatientCancelledDateStatus
	,Base.PrimaryDiagnosisCode
	,Base.SubsidiaryDiagnosisCode
	,Base.SecondaryDiagnosisCode1
	,Base.ReadPrimaryDiagnosisCode
	,Base.ReadSubsidiaryDiagnosisCode
	,Base.ReadSecondaryDiagnosisCode1
	,Base.OperationStatus
	,Base.PrimaryOperationCode
	,Base.OperationCode2
	,Base.OperationCode3
	,Base.OperationCode4
	,Base.OperationCode5
	,Base.OperationCode6
	,Base.OperationCode7
	,Base.OperationCode8
	,Base.OperationCode9
	,Base.OperationCode10
	,Base.OperationCode11
	,Base.OperationCode12
	,Base.ReadPrimaryOperationCode
	,Base.ReadOperationCode2
	,Base.ReadOperationCode3
	,Base.ReadOperationCode4
	,Base.ReadOperationCode5
	,Base.ReadOperationCode6
	,Base.ReadOperationCode7
	,Base.ReadOperationCode8
	,Base.ReadOperationCode9
	,Base.ReadOperationCode10
	,Base.ReadOperationCode11
	,Base.ReadOperationCode12
	,Base.PatientSexAgeMix
	,Base.IntendedClinicalCareIntensity
	,Base.BroadPatientGroup
	,Base.CasenoteNumber
	,Base.EthnicGroupCode
	,Base.AttendanceCategoryCode
	,Base.AppointmentRequiredDate
	,Base.MaritalStatusCode
	,Base.GPDiagnosisCode
	,Base.ReadGPDiagnosisCode
	,Base.PrimaryOperationGroupCode
	,Base.SecondaryOperationGroupCode1
	,Base.SecondaryOperationGroupCode2
	,Base.SecondaryProcedureGroupCode3
	,Base.SecondaryDiagnosisCode2
	,Base.SecondaryDiagnosisCode3
	,Base.SecondaryDiagnosisCode4
	,Base.ReadSecondaryDiagnosisCode2
	,Base.ReadSecondaryDiagnosisCode3
	,Base.ReadSecondaryDiagnosisCode4
	,Base.FundingType
	,Base.GPFundholderCode
	,Base.AppointmentHospitalCode
	,Base.ReferralHospitalCode
	,Base.NNNStatusIndicator
	,Base.BlankPad
	,Base.CancellationDate
	,Base.CancelledBy
	,Base.DeathIndicator
	,Base.LocalClinicCode
	,Base.LocalWardCode
	,Base.PCTHealthAuthorityCode
	,Base.PCTofResidenceCode
	,Base.ResponsiblePCTHealthAuthorityCode
	,Base.CommissionerCode
	,Base.AgencyActingOnBehalfOfDoH
	,Base.HRGCode
	,Base.HRGVersionNumber
	,Base.DGVPCode
	,Base.ReadDGVPCode
	,Base.DerivedFirstAttendance
	,Base.PCTResponsible
	,Base.AgeAtCDSActivityDate
	,Base.UniqueBookingReferenceNumber
	,Base.RTTPathwayID
	,Base.RTTCurrentProviderCode
	,Base.RTTPeriodStatusCode
	,Base.RTTStartDate
	,Base.RTTEndDate
	,Base.EarliestReasonableOfferDate
	,Base.EndOfRecord
from
	CDS.OPAngliaBase Base

inner join CDS.OPAngliaSubmitted Submitted
on	Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
and	not exists
	(
	select
		1
	from
		CDS.OPAngliaSubmitted Previous
	where
		Previous.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
	and	Previous.Created > Submitted.Created
	)
and	Submitted.UpdateType = '9' --insert/update

where
	not
		(
			isnull(Base.UniqueEpisodeSerialNo, '') = isnull(Submitted.UniqueEpisodeSerialNo, '')
		--and isnull(Base.UpdateType, '') = isnull(Submitted.UpdateType, '')
		--and isnull(Base.CDSUpdateDate, '') = isnull(Submitted.CDSUpdateDate, '')
		--and isnull(Base.CDSUpdateTime, '') = isnull(Submitted.CDSUpdateTime, '')
		and isnull(Base.CDSType, '') = isnull(Submitted.CDSType, '')
		and isnull(Base.ProviderCode, '') = isnull(Submitted.ProviderCode, '')
		and isnull(Base.PurchaserCode, '') = isnull(Submitted.PurchaserCode, '')
		and isnull(Base.CommissioningSerialNo, '') = isnull(Submitted.CommissioningSerialNo, '')
		and isnull(Base.NHSServiceAgreementLineNo, '') = isnull(Submitted.NHSServiceAgreementLineNo, '')
		and isnull(Base.PurchaserReferenceNo, '') = isnull(Submitted.PurchaserReferenceNo, '')
		and isnull(Base.NHSNumber, '') = isnull(Submitted.NHSNumber, '')
		and isnull(Base.PatientName, '') = isnull(Submitted.PatientName, '')
		and isnull(Base.NameFormatCode, '') = isnull(Submitted.NameFormatCode, '')
		and isnull(Base.PatientsAddress, '') = isnull(Submitted.PatientsAddress, '')
		and isnull(Base.Postcode, '') = isnull(Submitted.Postcode, '')
		and isnull(Base.HAofResidenceCode, '') = isnull(Submitted.HAofResidenceCode, '')
		and isnull(Base.SexCode, '') = isnull(Submitted.SexCode, '')
		and isnull(Base.CarerSupportIndicator, '') = isnull(Submitted.CarerSupportIndicator, '')
		and isnull(Base.DateOfBirth, '') = isnull(Submitted.DateOfBirth, '')
		and isnull(Base.DateOfBirthStatus, '') = isnull(Submitted.DateOfBirthStatus, '')
		and isnull(Base.RegisteredGpCode, '') = isnull(Submitted.RegisteredGpCode, '')
		and isnull(Base.RegisteredGpPracticeCode, '') = isnull(Submitted.RegisteredGpPracticeCode, '')
		and isnull(Base.DistrictNo, '') = isnull(Submitted.DistrictNo, '')
		and isnull(Base.ReferralCode, '') = isnull(Submitted.ReferralCode, '')
		and isnull(Base.ReferringOrganisationCode, '') = isnull(Submitted.ReferringOrganisationCode, '')
		and isnull(Base.ServiceTypeRequested, '') = isnull(Submitted.ServiceTypeRequested, '')
		and isnull(Base.ReferralRequestReceivedDate, '') = isnull(Submitted.ReferralRequestReceivedDate, '')
		and isnull(Base.ReferralRequestReceivedDateStatus, '') = isnull(Submitted.ReferralRequestReceivedDateStatus, '')
		and isnull(Base.PriorityType, '') = isnull(Submitted.PriorityType, '')
		and isnull(Base.SourceOfReferralCode, '') = isnull(Submitted.SourceOfReferralCode, '')
		and isnull(Base.MainSpecialtyCode, '') = isnull(Submitted.MainSpecialtyCode, '')
		and isnull(Base.TreatmentFunctionCode, '') = isnull(Submitted.TreatmentFunctionCode, '')
		and isnull(Base.LocalSubSpecialtyCode, '') = isnull(Submitted.LocalSubSpecialtyCode, '')
		and isnull(Base.ClinicPurpose, '') = isnull(Submitted.ClinicPurpose, '')
		and isnull(Base.ConsultantCode, '') = isnull(Submitted.ConsultantCode, '')
		and isnull(Base.AttendanceIdentifier, '') = isnull(Submitted.AttendanceIdentifier, '')
		and isnull(Base.AdminCategoryCode, '') = isnull(Submitted.AdminCategoryCode, '')
		and isnull(Base.LocationTypeCode, '') = isnull(Submitted.LocationTypeCode, '')
		and isnull(Base.SiteCode, '') = isnull(Submitted.SiteCode, '')
		and isnull(Base.MedicalStaffTypeCode, '') = isnull(Submitted.MedicalStaffTypeCode, '')
		and isnull(Base.AttendanceDate, '') = isnull(Submitted.AttendanceDate, '')
		and isnull(Base.FirstAttendanceFlag, '') = isnull(Submitted.FirstAttendanceFlag, '')
		and isnull(Base.DNAFlag, '') = isnull(Submitted.DNAFlag, '')
		and isnull(Base.AttendanceOutcomeCode, '') = isnull(Submitted.AttendanceOutcomeCode, '')
		and isnull(Base.LastDNAorPatientCancelledDate, '') = isnull(Submitted.LastDNAorPatientCancelledDate, '')
		and isnull(Base.LastDNAorPatientCancelledDateStatus, '') = isnull(Submitted.LastDNAorPatientCancelledDateStatus, '')
		and isnull(Base.PrimaryDiagnosisCode, '') = isnull(Submitted.PrimaryDiagnosisCode, '')
		and isnull(Base.SubsidiaryDiagnosisCode, '') = isnull(Submitted.SubsidiaryDiagnosisCode, '')
		and isnull(Base.SecondaryDiagnosisCode1, '') = isnull(Submitted.SecondaryDiagnosisCode1, '')
		and isnull(Base.ReadPrimaryDiagnosisCode, '') = isnull(Submitted.ReadPrimaryDiagnosisCode, '')
		and isnull(Base.ReadSubsidiaryDiagnosisCode, '') = isnull(Submitted.ReadSubsidiaryDiagnosisCode, '')
		and isnull(Base.ReadSecondaryDiagnosisCode1, '') = isnull(Submitted.ReadSecondaryDiagnosisCode1, '')
		and isnull(Base.OperationStatus, '') = isnull(Submitted.OperationStatus, '')
		and isnull(Base.PrimaryOperationCode, '') = isnull(Submitted.PrimaryOperationCode, '')
		and isnull(Base.OperationCode2, '') = isnull(Submitted.OperationCode2, '')
		and isnull(Base.OperationCode3, '') = isnull(Submitted.OperationCode3, '')
		and isnull(Base.OperationCode4, '') = isnull(Submitted.OperationCode4, '')
		and isnull(Base.OperationCode5, '') = isnull(Submitted.OperationCode5, '')
		and isnull(Base.OperationCode6, '') = isnull(Submitted.OperationCode6, '')
		and isnull(Base.OperationCode7, '') = isnull(Submitted.OperationCode7, '')
		and isnull(Base.OperationCode8, '') = isnull(Submitted.OperationCode8, '')
		and isnull(Base.OperationCode9, '') = isnull(Submitted.OperationCode9, '')
		and isnull(Base.OperationCode10, '') = isnull(Submitted.OperationCode10, '')
		and isnull(Base.OperationCode11, '') = isnull(Submitted.OperationCode11, '')
		and isnull(Base.OperationCode12, '') = isnull(Submitted.OperationCode12, '')
		and isnull(Base.ReadPrimaryOperationCode, '') = isnull(Submitted.ReadPrimaryOperationCode, '')
		and isnull(Base.ReadOperationCode2, '') = isnull(Submitted.ReadOperationCode2, '')
		and isnull(Base.ReadOperationCode3, '') = isnull(Submitted.ReadOperationCode3, '')
		and isnull(Base.ReadOperationCode4, '') = isnull(Submitted.ReadOperationCode4, '')
		and isnull(Base.ReadOperationCode5, '') = isnull(Submitted.ReadOperationCode5, '')
		and isnull(Base.ReadOperationCode6, '') = isnull(Submitted.ReadOperationCode6, '')
		and isnull(Base.ReadOperationCode7, '') = isnull(Submitted.ReadOperationCode7, '')
		and isnull(Base.ReadOperationCode8, '') = isnull(Submitted.ReadOperationCode8, '')
		and isnull(Base.ReadOperationCode9, '') = isnull(Submitted.ReadOperationCode9, '')
		and isnull(Base.ReadOperationCode10, '') = isnull(Submitted.ReadOperationCode10, '')
		and isnull(Base.ReadOperationCode11, '') = isnull(Submitted.ReadOperationCode11, '')
		and isnull(Base.ReadOperationCode12, '') = isnull(Submitted.ReadOperationCode12, '')
		and isnull(Base.PatientSexAgeMix, '') = isnull(Submitted.PatientSexAgeMix, '')
		and isnull(Base.IntendedClinicalCareIntensity, '') = isnull(Submitted.IntendedClinicalCareIntensity, '')
		and isnull(Base.BroadPatientGroup, '') = isnull(Submitted.BroadPatientGroup, '')
		and isnull(Base.CasenoteNumber, '') = isnull(Submitted.CasenoteNumber, '')
		and isnull(Base.EthnicGroupCode, '') = isnull(Submitted.EthnicGroupCode, '')
		and isnull(Base.AttendanceCategoryCode, '') = isnull(Submitted.AttendanceCategoryCode, '')
		and isnull(Base.AppointmentRequiredDate, '') = isnull(Submitted.AppointmentRequiredDate, '')
		and isnull(Base.MaritalStatusCode, '') = isnull(Submitted.MaritalStatusCode, '')
		and isnull(Base.GPDiagnosisCode, '') = isnull(Submitted.GPDiagnosisCode, '')
		and isnull(Base.ReadGPDiagnosisCode, '') = isnull(Submitted.ReadGPDiagnosisCode, '')
		and isnull(Base.PrimaryOperationGroupCode, '') = isnull(Submitted.PrimaryOperationGroupCode, '')
		and isnull(Base.SecondaryOperationGroupCode1, '') = isnull(Submitted.SecondaryOperationGroupCode1, '')
		and isnull(Base.SecondaryOperationGroupCode2, '') = isnull(Submitted.SecondaryOperationGroupCode2, '')
		and isnull(Base.SecondaryProcedureGroupCode3, '') = isnull(Submitted.SecondaryProcedureGroupCode3, '')
		and isnull(Base.SecondaryDiagnosisCode2, '') = isnull(Submitted.SecondaryDiagnosisCode2, '')
		and isnull(Base.SecondaryDiagnosisCode3, '') = isnull(Submitted.SecondaryDiagnosisCode3, '')
		and isnull(Base.SecondaryDiagnosisCode4, '') = isnull(Submitted.SecondaryDiagnosisCode4, '')
		and isnull(Base.ReadSecondaryDiagnosisCode2, '') = isnull(Submitted.ReadSecondaryDiagnosisCode2, '')
		and isnull(Base.ReadSecondaryDiagnosisCode3, '') = isnull(Submitted.ReadSecondaryDiagnosisCode3, '')
		and isnull(Base.ReadSecondaryDiagnosisCode4, '') = isnull(Submitted.ReadSecondaryDiagnosisCode4, '')
		and isnull(Base.FundingType, '') = isnull(Submitted.FundingType, '')
		and isnull(Base.GPFundholderCode, '') = isnull(Submitted.GPFundholderCode, '')
		and isnull(Base.AppointmentHospitalCode, '') = isnull(Submitted.AppointmentHospitalCode, '')
		and isnull(Base.ReferralHospitalCode, '') = isnull(Submitted.ReferralHospitalCode, '')
		and isnull(Base.NNNStatusIndicator, '') = isnull(Submitted.NNNStatusIndicator, '')
		and isnull(Base.BlankPad, '') = isnull(Submitted.BlankPad, '')
		and isnull(Base.CancellationDate, '') = isnull(Submitted.CancellationDate, '')
		and isnull(Base.CancelledBy, '') = isnull(Submitted.CancelledBy, '')
		and isnull(Base.DeathIndicator, '') = isnull(Submitted.DeathIndicator, '')
		and isnull(Base.LocalClinicCode, '') = isnull(Submitted.LocalClinicCode, '')
		and isnull(Base.LocalWardCode, '') = isnull(Submitted.LocalWardCode, '')
		and isnull(Base.PCTHealthAuthorityCode, '') = isnull(Submitted.PCTHealthAuthorityCode, '')
		and isnull(Base.PCTofResidenceCode, '') = isnull(Submitted.PCTofResidenceCode, '')
		and isnull(Base.ResponsiblePCTHealthAuthorityCode, '') = isnull(Submitted.ResponsiblePCTHealthAuthorityCode, '')
		and isnull(Base.CommissionerCode, '') = isnull(Submitted.CommissionerCode, '')
		and isnull(Base.AgencyActingOnBehalfOfDoH, '') = isnull(Submitted.AgencyActingOnBehalfOfDoH, '')
		and isnull(Base.HRGCode, '') = isnull(Submitted.HRGCode, '')
		and isnull(Base.HRGVersionNumber, '') = isnull(Submitted.HRGVersionNumber, '')
		and isnull(Base.DGVPCode, '') = isnull(Submitted.DGVPCode, '')
		and isnull(Base.ReadDGVPCode, '') = isnull(Submitted.ReadDGVPCode, '')
		and isnull(Base.DerivedFirstAttendance, '') = isnull(Submitted.DerivedFirstAttendance, '')
		and isnull(Base.PCTResponsible, '') = isnull(Submitted.PCTResponsible, '')
		--and isnull(Base.AgeAtCDSActivityDate, '') = isnull(Submitted.AgeAtCDSActivityDate, '')
		and isnull(Base.UniqueBookingReferenceNumber, '') = isnull(Submitted.UniqueBookingReferenceNumber, '')
		and isnull(Base.RTTPathwayID, '') = isnull(Submitted.RTTPathwayID, '')
		and isnull(Base.RTTCurrentProviderCode, '') = isnull(Submitted.RTTCurrentProviderCode, '')
		and isnull(Base.RTTPeriodStatusCode, '') = isnull(Submitted.RTTPeriodStatusCode, '')
		and isnull(Base.RTTStartDate, '') = isnull(Submitted.RTTStartDate, '')
		and isnull(Base.RTTEndDate, '') = isnull(Submitted.RTTEndDate, '')
		and isnull(Base.EarliestReasonableOfferDate, '') = isnull(Submitted.EarliestReasonableOfferDate, '')
		and isnull(Base.EndOfRecord, '') = isnull(Submitted.EndOfRecord, '')
		)

union all

--Inserts - UpdateType = '9'
select
	 Base.UniqueEpisodeSerialNo
	,UpdateType = '9'
	,Base.CDSUpdateDate
	,Base.CDSUpdateTime
	,Base.CDSType
	,Base.ProviderCode
	,Base.PurchaserCode
	,Base.CommissioningSerialNo
	,Base.NHSServiceAgreementLineNo
	,Base.PurchaserReferenceNo
	,Base.NHSNumber
	,Base.PatientName
	,Base.NameFormatCode
	,Base.PatientsAddress
	,Base.Postcode
	,Base.HAofResidenceCode
	,Base.SexCode
	,Base.CarerSupportIndicator
	,Base.DateOfBirth
	,Base.DateOfBirthStatus
	,Base.RegisteredGpCode
	,Base.RegisteredGpPracticeCode
	,Base.DistrictNo
	,Base.ReferralCode
	,Base.ReferringOrganisationCode
	,Base.ServiceTypeRequested
	,Base.ReferralRequestReceivedDate
	,Base.ReferralRequestReceivedDateStatus
	,Base.PriorityType
	,Base.SourceOfReferralCode
	,Base.MainSpecialtyCode
	,Base.TreatmentFunctionCode
	,Base.LocalSubSpecialtyCode
	,Base.ClinicPurpose
	,Base.ConsultantCode
	,Base.AttendanceIdentifier
	,Base.AdminCategoryCode
	,Base.LocationTypeCode
	,Base.SiteCode
	,Base.MedicalStaffTypeCode
	,Base.AttendanceDate
	,Base.FirstAttendanceFlag
	,Base.DNAFlag
	,Base.AttendanceOutcomeCode
	,Base.LastDNAorPatientCancelledDate
	,Base.LastDNAorPatientCancelledDateStatus
	,Base.PrimaryDiagnosisCode
	,Base.SubsidiaryDiagnosisCode
	,Base.SecondaryDiagnosisCode1
	,Base.ReadPrimaryDiagnosisCode
	,Base.ReadSubsidiaryDiagnosisCode
	,Base.ReadSecondaryDiagnosisCode1
	,Base.OperationStatus
	,Base.PrimaryOperationCode
	,Base.OperationCode2
	,Base.OperationCode3
	,Base.OperationCode4
	,Base.OperationCode5
	,Base.OperationCode6
	,Base.OperationCode7
	,Base.OperationCode8
	,Base.OperationCode9
	,Base.OperationCode10
	,Base.OperationCode11
	,Base.OperationCode12
	,Base.ReadPrimaryOperationCode
	,Base.ReadOperationCode2
	,Base.ReadOperationCode3
	,Base.ReadOperationCode4
	,Base.ReadOperationCode5
	,Base.ReadOperationCode6
	,Base.ReadOperationCode7
	,Base.ReadOperationCode8
	,Base.ReadOperationCode9
	,Base.ReadOperationCode10
	,Base.ReadOperationCode11
	,Base.ReadOperationCode12
	,Base.PatientSexAgeMix
	,Base.IntendedClinicalCareIntensity
	,Base.BroadPatientGroup
	,Base.CasenoteNumber
	,Base.EthnicGroupCode
	,Base.AttendanceCategoryCode
	,Base.AppointmentRequiredDate
	,Base.MaritalStatusCode
	,Base.GPDiagnosisCode
	,Base.ReadGPDiagnosisCode
	,Base.PrimaryOperationGroupCode
	,Base.SecondaryOperationGroupCode1
	,Base.SecondaryOperationGroupCode2
	,Base.SecondaryProcedureGroupCode3
	,Base.SecondaryDiagnosisCode2
	,Base.SecondaryDiagnosisCode3
	,Base.SecondaryDiagnosisCode4
	,Base.ReadSecondaryDiagnosisCode2
	,Base.ReadSecondaryDiagnosisCode3
	,Base.ReadSecondaryDiagnosisCode4
	,Base.FundingType
	,Base.GPFundholderCode
	,Base.AppointmentHospitalCode
	,Base.ReferralHospitalCode
	,Base.NNNStatusIndicator
	,Base.BlankPad
	,Base.CancellationDate
	,Base.CancelledBy
	,Base.DeathIndicator
	,Base.LocalClinicCode
	,Base.LocalWardCode
	,Base.PCTHealthAuthorityCode
	,Base.PCTofResidenceCode
	,Base.ResponsiblePCTHealthAuthorityCode
	,Base.CommissionerCode
	,Base.AgencyActingOnBehalfOfDoH
	,Base.HRGCode
	,Base.HRGVersionNumber
	,Base.DGVPCode
	,Base.ReadDGVPCode
	,Base.DerivedFirstAttendance
	,Base.PCTResponsible
	,Base.AgeAtCDSActivityDate
	,Base.UniqueBookingReferenceNumber
	,Base.RTTPathwayID
	,Base.RTTCurrentProviderCode
	,Base.RTTPeriodStatusCode
	,Base.RTTStartDate
	,Base.RTTEndDate
	,Base.EarliestReasonableOfferDate
	,Base.EndOfRecord
from
	CDS.OPAngliaBase Base
where
	not exists
	(
	select
		1
	from
		CDS.OPAngliaSubmitted Submitted
	where
		Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
	)

or	exists --latest update type was delete
	(
	select
		1
	from
		CDS.OPAngliaSubmitted Submitted
	where
		Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
	and	Submitted.UpdateType = '1' --delete
	and	not exists
		(
		select
			1
		from
			CDS.OPAngliaSubmitted Previous
		where
			Previous.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
		and	Previous.Created > Submitted.Created
		)
	)

union all

--Deletes - UpdateType = '1'
select
	 Submitted.UniqueEpisodeSerialNo
	,UpdateType = '1'
	,Submitted.CDSUpdateDate
	,Submitted.CDSUpdateTime
	,Submitted.CDSType
	,Submitted.ProviderCode
	,Submitted.PurchaserCode
	,Submitted.CommissioningSerialNo
	,Submitted.NHSServiceAgreementLineNo
	,Submitted.PurchaserReferenceNo
	,Submitted.NHSNumber
	,Submitted.PatientName
	,Submitted.NameFormatCode
	,Submitted.PatientsAddress
	,Submitted.Postcode
	,Submitted.HAofResidenceCode
	,Submitted.SexCode
	,Submitted.CarerSupportIndicator
	,Submitted.DateOfBirth
	,Submitted.DateOfBirthStatus
	,Submitted.RegisteredGpCode
	,Submitted.RegisteredGpPracticeCode
	,Submitted.DistrictNo
	,Submitted.ReferralCode
	,Submitted.ReferringOrganisationCode
	,Submitted.ServiceTypeRequested
	,Submitted.ReferralRequestReceivedDate
	,Submitted.ReferralRequestReceivedDateStatus
	,Submitted.PriorityType
	,Submitted.SourceOfReferralCode
	,Submitted.MainSpecialtyCode
	,Submitted.TreatmentFunctionCode
	,Submitted.LocalSubSpecialtyCode
	,Submitted.ClinicPurpose
	,Submitted.ConsultantCode
	,Submitted.AttendanceIdentifier
	,Submitted.AdminCategoryCode
	,Submitted.LocationTypeCode
	,Submitted.SiteCode
	,Submitted.MedicalStaffTypeCode
	,Submitted.AttendanceDate
	,Submitted.FirstAttendanceFlag
	,Submitted.DNAFlag
	,Submitted.AttendanceOutcomeCode
	,Submitted.LastDNAorPatientCancelledDate
	,Submitted.LastDNAorPatientCancelledDateStatus
	,Submitted.PrimaryDiagnosisCode
	,Submitted.SubsidiaryDiagnosisCode
	,Submitted.SecondaryDiagnosisCode1
	,Submitted.ReadPrimaryDiagnosisCode
	,Submitted.ReadSubsidiaryDiagnosisCode
	,Submitted.ReadSecondaryDiagnosisCode1
	,Submitted.OperationStatus
	,Submitted.PrimaryOperationCode
	,Submitted.OperationCode2
	,Submitted.OperationCode3
	,Submitted.OperationCode4
	,Submitted.OperationCode5
	,Submitted.OperationCode6
	,Submitted.OperationCode7
	,Submitted.OperationCode8
	,Submitted.OperationCode9
	,Submitted.OperationCode10
	,Submitted.OperationCode11
	,Submitted.OperationCode12
	,Submitted.ReadPrimaryOperationCode
	,Submitted.ReadOperationCode2
	,Submitted.ReadOperationCode3
	,Submitted.ReadOperationCode4
	,Submitted.ReadOperationCode5
	,Submitted.ReadOperationCode6
	,Submitted.ReadOperationCode7
	,Submitted.ReadOperationCode8
	,Submitted.ReadOperationCode9
	,Submitted.ReadOperationCode10
	,Submitted.ReadOperationCode11
	,Submitted.ReadOperationCode12
	,Submitted.PatientSexAgeMix
	,Submitted.IntendedClinicalCareIntensity
	,Submitted.BroadPatientGroup
	,Submitted.CasenoteNumber
	,Submitted.EthnicGroupCode
	,Submitted.AttendanceCategoryCode
	,Submitted.AppointmentRequiredDate
	,Submitted.MaritalStatusCode
	,Submitted.GPDiagnosisCode
	,Submitted.ReadGPDiagnosisCode
	,Submitted.PrimaryOperationGroupCode
	,Submitted.SecondaryOperationGroupCode1
	,Submitted.SecondaryOperationGroupCode2
	,Submitted.SecondaryProcedureGroupCode3
	,Submitted.SecondaryDiagnosisCode2
	,Submitted.SecondaryDiagnosisCode3
	,Submitted.SecondaryDiagnosisCode4
	,Submitted.ReadSecondaryDiagnosisCode2
	,Submitted.ReadSecondaryDiagnosisCode3
	,Submitted.ReadSecondaryDiagnosisCode4
	,Submitted.FundingType
	,Submitted.GPFundholderCode
	,Submitted.AppointmentHospitalCode
	,Submitted.ReferralHospitalCode
	,Submitted.NNNStatusIndicator
	,Submitted.BlankPad
	,Submitted.CancellationDate
	,Submitted.CancelledBy
	,Submitted.DeathIndicator
	,Submitted.LocalClinicCode
	,Submitted.LocalWardCode
	,Submitted.PCTHealthAuthorityCode
	,Submitted.PCTofResidenceCode
	,Submitted.ResponsiblePCTHealthAuthorityCode
	,Submitted.CommissionerCode
	,Submitted.AgencyActingOnBehalfOfDoH
	,Submitted.HRGCode
	,Submitted.HRGVersionNumber
	,Submitted.DGVPCode
	,Submitted.ReadDGVPCode
	,Submitted.DerivedFirstAttendance
	,Submitted.PCTResponsible
	,Submitted.AgeAtCDSActivityDate
	,Submitted.UniqueBookingReferenceNumber
	,Submitted.RTTPathwayID
	,Submitted.RTTCurrentProviderCode
	,Submitted.RTTPeriodStatusCode
	,Submitted.RTTStartDate
	,Submitted.RTTEndDate
	,Submitted.EarliestReasonableOfferDate
	,Submitted.EndOfRecord
from
	CDS.OPAngliaSubmitted Submitted
where

	--Only for this Financial Year

	convert(date,AttendanceDate,103) >= (select dbo.f_First_of_Financial_Year(getdate()))
	and
	not exists
	(
	select
		1
	from
		CDS.OPAngliaBase Base
	where
		Base.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	)

and	not exists
	(
	select
		1
	from
		CDS.OPAngliaSubmitted Previous
	where
		Previous.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	and	Previous.Created > Submitted.Created
	)

and	exists --latest update type was insert/update
	(
	select
		1
	from
		CDS.OPAngliaSubmitted Submitted9
	where
		Submitted9.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	and	Submitted9.UpdateType = '9' --insert/update
	and	not exists
		(
		select
			1
		from
			CDS.OPAngliaSubmitted Previous
		where
			Previous.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
		and	Previous.Created > Submitted9.Created
		)
	)






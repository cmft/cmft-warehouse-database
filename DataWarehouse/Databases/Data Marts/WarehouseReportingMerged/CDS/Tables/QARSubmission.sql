﻿CREATE TABLE [CDS].[QARSubmission] (
    [SubmissionDate]            DATE          NULL,
    [PCTCode]                   VARCHAR (10)  NULL,
    [Division]                  VARCHAR (50)  NULL,
    [NationalSpecialty]         VARCHAR (200) NULL,
    [DecisionsToAdmit]          BIGINT        NULL,
    [PatientsAdmitted]          BIGINT        NULL,
    [PatientsFailedToAttend]    BIGINT        NULL,
    [RemovalsOther]             BIGINT        NULL,
    [GPReferrals]               BIGINT        NULL,
    [OtherReferrals]            BIGINT        NULL,
    [FirstAttendancesSeen]      BIGINT        NULL,
    [FirstAttendancesDNA]       BIGINT        NULL,
    [SubsequentAttendancesSeen] BIGINT        NULL,
    [SubsequentAttendancesDNA]  BIGINT        NULL,
    [RunDate]                   DATETIME      NULL
);


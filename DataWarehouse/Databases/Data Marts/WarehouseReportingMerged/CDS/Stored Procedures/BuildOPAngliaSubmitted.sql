﻿CREATE procedure [CDS].[BuildOPAngliaSubmitted]
	@SubmittedDate date = null
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

select
	@SubmittedDate =
		coalesce(
			 @SubmittedDate
			,getdate()
		)

	,@Created = GETDATE()

insert
into
	CDS.OPAngliaSubmitted
(
	 UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,NameFormatCode
	,PatientsAddress
	,Postcode
	,HAofResidenceCode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,DateOfBirthStatus
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,ReferralCode
	,ReferringOrganisationCode
	,ServiceTypeRequested
	,ReferralRequestReceivedDate
	,ReferralRequestReceivedDateStatus
	,PriorityType
	,SourceOfReferralCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,LocalSubSpecialtyCode
	,ClinicPurpose
	,ConsultantCode
	,AttendanceIdentifier
	,AdminCategoryCode
	,LocationTypeCode
	,SiteCode
	,MedicalStaffTypeCode
	,AttendanceDate
	,FirstAttendanceFlag
	,DNAFlag
	,AttendanceOutcomeCode
	,LastDNAorPatientCancelledDate
	,LastDNAorPatientCancelledDateStatus
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,ReadPrimaryDiagnosisCode
	,ReadSubsidiaryDiagnosisCode
	,ReadSecondaryDiagnosisCode1
	,OperationStatus
	,PrimaryOperationCode
	,OperationCode2
	,OperationCode3
	,OperationCode4
	,OperationCode5
	,OperationCode6
	,OperationCode7
	,OperationCode8
	,OperationCode9
	,OperationCode10
	,OperationCode11
	,OperationCode12
	,ReadPrimaryOperationCode
	,ReadOperationCode2
	,ReadOperationCode3
	,ReadOperationCode4
	,ReadOperationCode5
	,ReadOperationCode6
	,ReadOperationCode7
	,ReadOperationCode8
	,ReadOperationCode9
	,ReadOperationCode10
	,ReadOperationCode11
	,ReadOperationCode12
	,PatientSexAgeMix
	,IntendedClinicalCareIntensity
	,BroadPatientGroup
	,CasenoteNumber
	,EthnicGroupCode
	,AttendanceCategoryCode
	,AppointmentRequiredDate
	,MaritalStatusCode
	,GPDiagnosisCode
	,ReadGPDiagnosisCode
	,PrimaryOperationGroupCode
	,SecondaryOperationGroupCode1
	,SecondaryOperationGroupCode2
	,SecondaryProcedureGroupCode3
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,ReadSecondaryDiagnosisCode2
	,ReadSecondaryDiagnosisCode3
	,ReadSecondaryDiagnosisCode4
	,FundingType
	,GPFundholderCode
	,AppointmentHospitalCode
	,ReferralHospitalCode
	,NNNStatusIndicator
	,BlankPad
	,CancellationDate
	,CancelledBy
	,DeathIndicator
	,LocalClinicCode
	,LocalWardCode
	,PCTHealthAuthorityCode
	,PCTofResidenceCode
	,ResponsiblePCTHealthAuthorityCode
	,CommissionerCode
	,AgencyActingOnBehalfOfDoH
	,HRGCode
	,HRGVersionNumber
	,DGVPCode
	,ReadDGVPCode
	,DerivedFirstAttendance
	,PCTResponsible
	,AgeAtCDSActivityDate
	,UniqueBookingReferenceNumber
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,EarliestReasonableOfferDate
	,EndOfRecord
	,SubmittedDate
	,Created
	,ByWhom
)
select
	 Submitted.UniqueEpisodeSerialNo
	,Submitted.UpdateType
	,Submitted.CDSUpdateDate
	,Submitted.CDSUpdateTime
	,Submitted.CDSType
	,Submitted.ProviderCode
	,Submitted.PurchaserCode
	,Submitted.CommissioningSerialNo
	,Submitted.NHSServiceAgreementLineNo
	,Submitted.PurchaserReferenceNo
	,Submitted.NHSNumber
	,Submitted.PatientName
	,Submitted.NameFormatCode
	,Submitted.PatientsAddress
	,Submitted.Postcode
	,Submitted.HAofResidenceCode
	,Submitted.SexCode
	,Submitted.CarerSupportIndicator
	,Submitted.DateOfBirth
	,Submitted.DateOfBirthStatus
	,Submitted.RegisteredGpCode
	,Submitted.RegisteredGpPracticeCode
	,Submitted.DistrictNo
	,Submitted.ReferralCode
	,Submitted.ReferringOrganisationCode
	,Submitted.ServiceTypeRequested
	,Submitted.ReferralRequestReceivedDate
	,Submitted.ReferralRequestReceivedDateStatus
	,Submitted.PriorityType
	,Submitted.SourceOfReferralCode
	,Submitted.MainSpecialtyCode
	,Submitted.TreatmentFunctionCode
	,Submitted.LocalSubSpecialtyCode
	,Submitted.ClinicPurpose
	,Submitted.ConsultantCode
	,Submitted.AttendanceIdentifier
	,Submitted.AdminCategoryCode
	,Submitted.LocationTypeCode
	,Submitted.SiteCode
	,Submitted.MedicalStaffTypeCode
	,Submitted.AttendanceDate
	,Submitted.FirstAttendanceFlag
	,Submitted.DNAFlag
	,Submitted.AttendanceOutcomeCode
	,Submitted.LastDNAorPatientCancelledDate
	,Submitted.LastDNAorPatientCancelledDateStatus
	,Submitted.PrimaryDiagnosisCode
	,Submitted.SubsidiaryDiagnosisCode
	,Submitted.SecondaryDiagnosisCode1
	,Submitted.ReadPrimaryDiagnosisCode
	,Submitted.ReadSubsidiaryDiagnosisCode
	,Submitted.ReadSecondaryDiagnosisCode1
	,Submitted.OperationStatus
	,Submitted.PrimaryOperationCode
	,Submitted.OperationCode2
	,Submitted.OperationCode3
	,Submitted.OperationCode4
	,Submitted.OperationCode5
	,Submitted.OperationCode6
	,Submitted.OperationCode7
	,Submitted.OperationCode8
	,Submitted.OperationCode9
	,Submitted.OperationCode10
	,Submitted.OperationCode11
	,Submitted.OperationCode12
	,Submitted.ReadPrimaryOperationCode
	,Submitted.ReadOperationCode2
	,Submitted.ReadOperationCode3
	,Submitted.ReadOperationCode4
	,Submitted.ReadOperationCode5
	,Submitted.ReadOperationCode6
	,Submitted.ReadOperationCode7
	,Submitted.ReadOperationCode8
	,Submitted.ReadOperationCode9
	,Submitted.ReadOperationCode10
	,Submitted.ReadOperationCode11
	,Submitted.ReadOperationCode12
	,Submitted.PatientSexAgeMix
	,Submitted.IntendedClinicalCareIntensity
	,Submitted.BroadPatientGroup
	,Submitted.CasenoteNumber
	,Submitted.EthnicGroupCode
	,Submitted.AttendanceCategoryCode
	,Submitted.AppointmentRequiredDate
	,Submitted.MaritalStatusCode
	,Submitted.GPDiagnosisCode
	,Submitted.ReadGPDiagnosisCode
	,Submitted.PrimaryOperationGroupCode
	,Submitted.SecondaryOperationGroupCode1
	,Submitted.SecondaryOperationGroupCode2
	,Submitted.SecondaryProcedureGroupCode3
	,Submitted.SecondaryDiagnosisCode2
	,Submitted.SecondaryDiagnosisCode3
	,Submitted.SecondaryDiagnosisCode4
	,Submitted.ReadSecondaryDiagnosisCode2
	,Submitted.ReadSecondaryDiagnosisCode3
	,Submitted.ReadSecondaryDiagnosisCode4
	,Submitted.FundingType
	,Submitted.GPFundholderCode
	,Submitted.AppointmentHospitalCode
	,Submitted.ReferralHospitalCode
	,Submitted.NNNStatusIndicator
	,Submitted.BlankPad
	,Submitted.CancellationDate
	,Submitted.CancelledBy
	,Submitted.DeathIndicator
	,Submitted.LocalClinicCode
	,Submitted.LocalWardCode
	,Submitted.PCTHealthAuthorityCode
	,Submitted.PCTofResidenceCode
	,Submitted.ResponsiblePCTHealthAuthorityCode
	,Submitted.CommissionerCode
	,Submitted.AgencyActingOnBehalfOfDoH
	,Submitted.HRGCode
	,Submitted.HRGVersionNumber
	,Submitted.DGVPCode
	,Submitted.ReadDGVPCode
	,Submitted.DerivedFirstAttendance
	,Submitted.PCTResponsible
	,Submitted.AgeAtCDSActivityDate
	,Submitted.UniqueBookingReferenceNumber
	,Submitted.RTTPathwayID
	,Submitted.RTTCurrentProviderCode
	,Submitted.RTTPeriodStatusCode
	,Submitted.RTTStartDate
	,Submitted.RTTEndDate
	,Submitted.EarliestReasonableOfferDate
	,Submitted.EndOfRecord

	,SubmittedDate = @SubmittedDate
	,Created = @Created
	,ByWhom = system_user
from
	CDS.OPAngliaNet Submitted

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@SubmittedDate, ''))

exec Warehouse.dbo.WriteAuditLogEvent 'CDS.BuildOPAngliaSubmitted', @Stats, @StartTime

print @Stats

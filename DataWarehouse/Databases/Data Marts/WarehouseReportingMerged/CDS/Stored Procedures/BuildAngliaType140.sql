﻿
CREATE procedure [CDS].[BuildAngliaType140]  --A&E
	 @CDSTestIndicator char(1) = ''
	,@CDSProtocolIdentifier char(3) = '020' -- bulk
as

-- 20150206	RR	MergeEncounterRecno no longer exists in the AE.BaseInvestigationReference table.  The join was previously on MergeEncounter (AE) and SequenceNo, this has now been changed to MergeRecno
--				And Treatment and Diagnosis

/*

CCB 2013-07-12

SEARCH FOR THE FOLLOWING STRING TO FIND THE BITS THAT NEED CHANGING TO USE CCG:


--modify to use CCG

*/



declare @CDSReportPeriodStartDate smalldatetime
declare @CDSReportPeriodEndDate smalldatetime

select
	 @CDSReportPeriodStartDate = min(dateadd(day, datediff(day, 0, ArrivalDate), 0))
	,@CDSReportPeriodEndDate = max(dateadd(day, datediff(day, 0, ArrivalDate), 0))
from
	WarehouseOLAPMergedV2.AE.BaseEncounter Encounter

inner join CDS.wrkCDS
on	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	wrkCDS.CDSTypeCode in ('010')

--prepare a distinct list of diagnoses

--Diagnosis is comprised:
--Diagnosis Condition n2 
--Sub-Analysis n1 
--Accident And Emergency Attendance - ANATOMICAL AREA  n2 
--Accident And Emergency Attendance - ANATOMICAL SIDE  an1 

truncate table CDS.AEDiagnosisDistinct

insert
into
	CDS.AEDiagnosisDistinct

select
	 ContextCode
	,SourceUniqueID
	,Diagnosis.DiagnosisCode

	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY ContextCode , SourceUniqueID ORDER BY DiagnosisCode)
from
	(
	select distinct
		 BaseDiagnosis.ContextCode
		,BaseEncounter.SourceUniqueID
		,DiagnosisCode =
				rtrim(
					Diagnosis.NationalValueCode + ' ' + 
					coalesce(BaseDiagnosis.DiagnosisSiteCode, '  ') +
					coalesce(BaseDiagnosis.DiagnosisSideCode, ' ')
				)
			
	from
		WarehouseOLAPMergedV2.AE.BaseDiagnosis

	inner join WarehouseOLAPMergedV2.AE.BaseDiagnosisReference
	on	BaseDiagnosisReference.MergeRecno = BaseDiagnosis.MergeRecno
	--on	BaseDiagnosisReference.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	--and	BaseDiagnosisReference.SequenceNo = BaseDiagnosis.SequenceNo

	inner join WarehouseOLAPMergedV2.AE.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	
	inner join WarehouseOLAPMergedV2.WH.Member Diagnosis
	on	Diagnosis.SourceValueID = BaseDiagnosisReference.DiagnosisID
	and	Diagnosis.NationalValueCode != 'N||AEDIAG'

	inner join CDS.wrkCDS
	on	wrkCDS.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	and	wrkCDS.CDSTypeCode in ('010')

	) Diagnosis


--prepare a distinct list of investigations 

truncate table CDS.AEInvestigationDistinct

insert
into
	CDS.AEInvestigationDistinct
select
	 ContextCode
	,SourceUniqueID
	,InvestigationCode =
		left(
			 Investigation.InvestigationCode
			,6
		)

	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY ContextCode , SourceUniqueID ORDER BY InvestigationCode)
from
	(
	select distinct
		 BaseInvestigation.ContextCode
		,BaseEncounter.SourceUniqueID
		,InvestigationCode =
			Investigation.NationalValueCode
	from
		WarehouseOLAPMergedV2.AE.BaseInvestigation

	inner join WarehouseOLAPMergedV2.AE.BaseInvestigationReference
	on	BaseInvestigationReference.MergeRecno = BaseInvestigation.MergeRecno
	-- 20150206 RR MergeEncounterRecno no longer exists in the table therefore had to update proc
	--on	BaseInvestigationReference.MergeEncounterRecno = BaseInvestigation.MergeEncounterRecno
	--and	BaseInvestigationReference.SequenceNo = BaseInvestigation.SequenceNo
	
	inner join WarehouseOLAPMergedV2.AE.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = BaseInvestigation.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.WH.Member Investigation
	on	Investigation.SourceValueID = BaseInvestigationReference.InvestigationID
	and	Investigation.NationalValueCode != 'N||AEINVE'

	inner join CDS.wrkCDS
	on	wrkCDS.MergeEncounterRecno = BaseInvestigation.MergeEncounterRecno
	and	wrkCDS.CDSTypeCode in ('010')

	where
		len(Investigation.SourceValueCode) > 1
	) Investigation


--prepare a distinct list of treatments 

truncate table CDS.AETreatmentDistinct

insert
into
	CDS.AETreatmentDistinct
select
	 ContextCode
	,SourceUniqueID
	,TreatmentCode =
		left(
			 Treatment.TreatmentCode
			,6
		)

	,TreatmentDate = Treatment.TreatmentDate

	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY ContextCode , SourceUniqueID ORDER BY TreatmentCode)

from
	(
	select
		 BaseTreatment.ContextCode
		,BaseEncounter.SourceUniqueID

		,TreatmentCode =
			Treatment.NationalValueCode
		
		,TreatmentDate = cast(BaseTreatment.TreatmentTime as date)
	from
		WarehouseOLAPMergedV2.AE.BaseTreatment

	inner join WarehouseOLAPMergedV2.AE.BaseTreatmentReference
	on	BaseTreatmentReference.MergeRecno = BaseTreatment.MergeRecno
	--on	BaseTreatmentReference.MergeEncounterRecno = BaseTreatment.MergeEncounterRecno
	--and	BaseTreatmentReference.SequenceNo = BaseTreatment.SequenceNo

	inner join WarehouseOLAPMergedV2.AE.BaseEncounter
	on	BaseEncounter.MergeEncounterRecno = BaseTreatment.MergeEncounterRecno
		
	inner join WarehouseOLAPMergedV2.WH.Member Treatment
	on	Treatment.SourceValueID = BaseTreatmentReference.TreatmentID
	and	Treatment.NationalValueCode != 'N||AETREA'

	inner join CDS.wrkCDS
	on	wrkCDS.MergeEncounterRecno = BaseTreatment.MergeEncounterRecno
	and	wrkCDS.CDSTypeCode in ('010')

	--where
	--	not exists
	--	(
	--	select
	--		1
	--	from
	--		WarehouseOLAPMergedV2.AE.BaseTreatment Latest

	--	inner join WarehouseOLAPMergedV2.AE.BaseTreatmentReference
	--	on	BaseTreatmentReference.MergeEncounterRecno = Latest.MergeEncounterRecno
	--	and	BaseTreatmentReference.SequenceNo = Latest.SequenceNo

	--	inner join WarehouseOLAPMergedV2.AE.BaseEncounter
	--	on	BaseEncounter.MergeEncounterRecno = Latest.MergeEncounterRecno

	--	inner join CDS.wrkCDS
	--	on	wrkCDS.MergeEncounterRecno = Latest.MergeEncounterRecno
	--	and	wrkCDS.CDSTypeCode in ('010')

	--	where
	--		Latest.ContextCode = BaseTreatment.ContextCode
	--	and	Latest.SourceUniqueID = BaseTreatment.SourceUniqueID
	--	and	Latest.TreatmentID = BaseTreatment.TreatmentID
	--	and	(
	--			Latest.TreatmentDate < BaseTreatment.TreatmentDate
	--		or	(
	--				Latest.TreatmentDate = BaseTreatment.TreatmentDate
	--			and	Latest.AESourceUniqueID < BaseTreatment.AESourceUniqueID
	--			)
	--		)
	--	)
	) Treatment


truncate table CDS.AEAngliaBase

insert into CDS.AEAngliaBase
(
	 MergeEncounterRecno
	,PrimeRecipient
	,CopyRecipient1
	,CopyRecipient2
	,CopyRecipient3
	,CopyRecipient4
	,CopyRecipient5
	,Sender
	,CDSGroup
	,CDSType
	,CDSId
	,TestFlag
	,DatetimeCreated
	,UpdateType
	,ProtocolIdentifier
	,BulkStart
	,BulkEnd
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientName
	,PatientAddress
	,Postcode
	,PCTofResidence
	,DateOfBirth
	,SexCode
	,CarerSupportIndicator
	,EthnicCategoryCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,DepartmentTypeCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,ICDDiagnosisSchemeCode
	,ICDDiagnosisCodeFirst
	,ICDDiagnosisCodeSecond
	,ReadDiagnosisSchemeCode
	,ReadDiagnosisCodeFirst
	,ReadDiagnosisCodeSecond
	,DiagnosisSchemeCode
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,DiagnosisCode1
	,DiagnosisCode2
	,DiagnosisCode3
	,DiagnosisCode4
	,DiagnosisCode5
	,DiagnosisCode6
	,DiagnosisCode7
	,DiagnosisCode8
	,DiagnosisCode9
	,DiagnosisCode10
	,DiagnosisCode11
	,DiagnosisCode12
	,InvestigationSchemeCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,InvestigationCode1
	,InvestigationCode2
	,InvestigationCode3
	,InvestigationCode4
	,InvestigationCode5
	,InvestigationCode6
	,InvestigationCode7
	,InvestigationCode8
	,InvestigationCode9
	,InvestigationCode10
	,OPCSProcedureSchemeCode
	,OPCSPrimaryProcedureCode
	,OPCSPrimaryProcedureDate
	,OPCSPrimaryProcedureCodeSecond
	,OPCSPrimaryProcedureDateSecond
	,ReadProcedureSchemeCode
	,ReadPrimaryProcedureCode
	,ReadPrimaryProcedureDate
	,ReadPrimaryProcedureCodeSecond
	,ReadPrimaryProcedureDateSecond
	,TreatmentSchemeCode
	,TreatmentCodeFirst
	,TreatmentDateFirst
	,TreatmentCodeSecond
	,TreatmentDateSecond
	,TreatmentCode1
	,TreatmentDate1
	,TreatmentCode2
	,TreatmentDate2
	,TreatmentCode3
	,TreatmentDate3
	,TreatmentCode4
	,TreatmentDate4
	,TreatmentCode5
	,TreatmentDate5
	,TreatmentCode6
	,TreatmentDate6
	,TreatmentCode7
	,TreatmentDate7
	,TreatmentCode8
	,TreatmentDate8
	,TreatmentCode9
	,TreatmentDate9
	,TreatmentCode10
	,TreatmentDate10
	,HRGCode
	,HRGVersionCode
	,HRGProcedureSchemeCode
	,DGVPCode
)
select
	 Encounter.MergeEncounterRecno
	,PrimeRecipient = left(coalesce(Encounter.PCTCode, Encounter.ResidencePCTCode), 6)
	,CopyRecipient1 = 
	left(
		case 
		when Encounter.ResidencePCTCode = Encounter.PCTCode then '' 
		else Encounter.ResidencePCTCode 
		end
	, 6)
	,CopyRecipient2 = null
	,CopyRecipient3 = null
	,CopyRecipient4 = null
	,CopyRecipient5 = null
	,Sender = 'RW300'
	,CDSGroup = '010'
	,CDSType = Encounter.CDSTypeCode

	,CDSId =
		left('BRW300' + Encounter.SourceUniqueID, 35)

	,TestFlag = ''

	,DatetimeCreated =
	left(convert(varchar, getdate(), 112), 10) +
	replace(left(convert(varchar, getdate(), 108), 5), ':', '')

	,UpdateType = '9'
	,ProtocolIdentifier = @CDSProtocolIdentifier
	,BulkStart = case when @CDSProtocolIdentifier = '010' then null else left(convert(varchar, @CDSReportPeriodStartDate, 112), 10) end
	,BulkEnd = case when @CDSProtocolIdentifier = '010' then null else left(convert(varchar, @CDSReportPeriodEndDate, 112), 10) end


	,UniqueBookingReferenceNo
	,PathwayId = LEFT(Encounter.PathwayId, 20)
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate = left(convert(varchar, Encounter.RTTStartDate, 103), 10)
	,RTTEndDate = left(convert(varchar, Encounter.RTTEndDate, 103), 10)

	,DistrictNo =
		coalesce(Encounter.DistrictNo , Encounter.EPMINo)

	,DistrictNoOrganisationCode = 'RW3'

	,NHSNumber =
		case
		when NHSNumber in ('1111111111','8888888888') then null
		else NHSNumber
		end

	,NHSNumberStatusId =
		case
		when Encounter.NHSNumberStatusId = 'N||NHSNST' then '03' -- Trace Required
		else Encounter.NHSNumberStatusId
		end

	,PatientName = 
		case
		when Encounter.NHSNumber is null --Encounter.NHSNumberStatusId <> '01' 
		then 
			replace(
				REPLACE(Encounter.PatientForename,',','')
				,'`'
				,''
			) + ' ' + 
			replace(
				REPLACE(Encounter.PatientSurname,',','')
			 	,'`'
				,''
			)
		else null
		end

	,PatientAddress = 
		case
		when Encounter.NHSNumber is null --Encounter.NHSNumberStatusId <> '01' 
		then
			REPLACE(Encounter.PatientAddress1,',','') + ' ' + 
			REPLACE(Encounter.PatientAddress2,',','') + ' ' + 
			REPLACE(Encounter.PatientAddress3,',','') + ' ' + 
			REPLACE(Encounter.PatientAddress4,',','')
		else null
		end

	,Postcode = 
			case
			when Encounter.Postcode IS NULL THEN 'ZZ99 3WZ'
			when ltrim(rtrim(Encounter.Postcode)) =  '' THEN 'ZZ99 3WZ'
			when len(Encounter.Postcode) = 8 then Encounter.Postcode
			else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
			end

	,PCTofResidence = 
		case 
		when 
		--	Encounter.PCTCode like '0%' 
		--or 
			PatientAddress1 like 'Visit%' then 'X98'
		else
			coalesce(
				Encounter.ResidencePCTCode
				,'Q99'
			)
		end

	,DateOfBirth = left(convert(varchar, Encounter.DateOfBirth, 103), 10)

	,SexCode =
		case
		when SexCode = '01' then '1'
		else '2'
		end

	,CarerSupportIndicator

	,EthnicCategoryCode =
			--EthnicCategory.EthnicCategoryNationalCode
		Encounter.EthnicCategoryCode

	,RegisteredGpCode = 
		CASE
		WHEN Encounter.RegisteredGpCode = 'G0' THEN 'G9999998'
		ELSE Encounter.RegisteredGpCode
		END

	,Encounter.RegisteredGpPracticeCode

	--,AttendanceNumber =
	--	case
	--	when isnumeric(LEFT(Encounter.AttendanceNumber, 3)) = 1
	--	then Encounter.AttendanceNumber

	--	else
	--		left(Encounter.AttendanceNumber, 1) +
	--		replace(right(Encounter.AttendanceNumber, LEN(Encounter.AttendanceNumber) - 3), '-', '')
	--	end

	,AttendanceNumber =
		CASE
		WHEN ISNUMERIC(LEFT(Encounter.AttendanceNumber, 3)) = 1	THEN Encounter.AttendanceNumber
		WHEN LEN(REPLACE(AttendanceNumber,'-','')) > 12 THEN LEFT(Encounter.AttendanceNumber, 1) +	REPLACE(RIGHT(Encounter.AttendanceNumber, LEN(Encounter.AttendanceNumber) - 3), '-', '')
		ELSE REPLACE(Encounter.AttendanceNumber, '-', '')
		END

	,ArrivalModeCode =
		 --coalesce(ArrivalMode.ArrivalModeNationalCode, '2')
		case
		when Encounter.ArrivalModeCode = 'N||ARRMOD' then 2
		else Encounter.ArrivalModeCode
		end

	,AttendanceCategoryCode

	,AttendanceDisposalCode=
		CASE
		WHEN AttendanceDisposalCode IN ('','01','02','03','04','05','06','07','10','11','12','13','14') THEN AttendanceDisposalCode
		WHEN AttendanceDisposalCode IS NULL THEN ''
		ELSE '14'
		END

	,IncidentLocationTypeCode =
		case
		when Encounter.IncidentLocationTypeCode = 'N||INCLOC' then '91'
		else coalesce(Encounter.IncidentLocationTypeCode , '91')
		end

	,PatientGroupCode =
		CASE
		when Encounter.PatientGroupCode = 'N||PATGRP' then '80'
		WHEN Encounter.PatientGroupCode = '0' THEN '80'
		ELSE coalesce(Encounter.PatientGroupCode , '80')
		END

	,SourceOfReferralCode =
		CASE
		WHEN SourceOfReferralCode IN ('','00','01','02','03','04','05','06','07','08','92','93') THEN SourceOfReferralCode
		--WHEN SourceOfReferralCode IS NULL THEN ''
		ELSE '08'
		END

	,DepartmentTypeCode = 
		--case
		--when left(Encounter.AttendanceNumber, 1) = 'E'
		--then '02'
		--else '01'
		--end
		CASE  --GS20110830  logic amended to account for PCEC and st marys
			WHEN left(Encounter.AttendanceNumber, 1) IN('E','S') THEN '02'
			WHEN ISNUMERIC(Encounter.AttendanceNumber) = 1 THEN '04'
			WHEN Encounter.ContextCode like 'TRA||%' and left(Encounter.AttendanceNumber, 3) = 'MIU' Then '03'
			When Encounter.ContextCode like 'TRA||%' and  Encounter.ArrivalDate >= '28 nov 2013'  then '03'
			ELSE '01'
		END

	,ArrivalDate = left(convert(varchar, Encounter.ArrivalDate, 103), 10)
	,ArrivalTime = convert(varchar, Encounter.ArrivalTime, 108)
	,AgeOnArrival
	,InitialAssessmentTime = convert(varchar, Encounter.InitialAssessmentTime, 108)
	,SeenForTreatmentTime = convert(varchar, Encounter.SeenForTreatmentTime, 108)
	,AttendanceConclusionTime = convert(varchar, Encounter.AttendanceConclusionTime, 108)
	,DepartureTime = convert(varchar, Encounter.DepartureTime, 108)
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode = 
		'RW300'

	,CommissionerCode =

		left(coalesce(Encounter.CommissionerCode, Encounter.PCTCode, Encounter.ResidencePCTCode), 6)

		--case
		--when left(coalesce(Encounter.CommissionerCode, Encounter.PCTCode, Encounter.ResidencePCTCode) , 3) between '7A1' and '7A7' then '5NT'
		--else left(coalesce(Encounter.CommissionerCode, Encounter.PCTCode, Encounter.ResidencePCTCode), 6)
		--end
		

	,StaffMemberCode = null
		--left(StaffMember.LoginId, 3)
		--Encounter.StaffMemberCode

	,ICDDiagnosisSchemeCode = null
	,ICDDiagnosisCodeFirst = null
	,ICDDiagnosisCodeSecond = null
	,ReadDiagnosisSchemeCode = null
	,ReadDiagnosisCodeFirst = null
	,ReadDiagnosisCodeSecond = null

	,DiagnosisSchemeCode = 
		case
		when Diagnosis1.DiagnosisCode is null 
		then null 
		else '01' 
		end

	,DiagnosisCodeFirst = Diagnosis1.DiagnosisCode
	,DiagnosisCodeSecond = Diagnosis2.DiagnosisCode
	,DiagnosisCode1 = Diagnosis3.DiagnosisCode
	,DiagnosisCode2 = Diagnosis4.DiagnosisCode
	,DiagnosisCode3 = Diagnosis5.DiagnosisCode
	,DiagnosisCode4 = Diagnosis6.DiagnosisCode
	,DiagnosisCode5 = Diagnosis7.DiagnosisCode
	,DiagnosisCode6 = Diagnosis8.DiagnosisCode
	,DiagnosisCode7 = Diagnosis9.DiagnosisCode
	,DiagnosisCode8 = Diagnosis10.DiagnosisCode
	,DiagnosisCode9 = Diagnosis11.DiagnosisCode
	,DiagnosisCode10 = Diagnosis12.DiagnosisCode
	,DiagnosisCode11 = Diagnosis13.DiagnosisCode
	,DiagnosisCode12 = Diagnosis14.DiagnosisCode

	,InvestigationSchemeCode = '01' 
		--case
		--when Investigation1.InvestigationCode is null 
		--then null 
		--else '01' 
		--end

	,InvestigationCodeFirst = 
		coalesce(
			 Investigation1.InvestigationCode
			,'24'
		)

	,InvestigationCodeSecond = Investigation2.InvestigationCode
		--coalesce(
		--	 Investigation2.InvestigationCode
		--	,'24'
		--)

	,InvestigationCode1 = Investigation3.InvestigationCode
	,InvestigationCode2 = Investigation4.InvestigationCode
	,InvestigationCode3 = Investigation5.InvestigationCode
	,InvestigationCode4 = Investigation6.InvestigationCode
	,InvestigationCode5 = Investigation7.InvestigationCode
	,InvestigationCode6 = Investigation8.InvestigationCode
	,InvestigationCode7 = Investigation9.InvestigationCode
	,InvestigationCode8 = Investigation10.InvestigationCode
	,InvestigationCode9 = Investigation11.InvestigationCode
	,InvestigationCode10 = Investigation12.InvestigationCode

	,OPCSProcedureSchemeCode = null --A&E
	,OPCSPrimaryProcedureCode = null
	,OPCSPrimaryProcedureDate = null
	,OPCSPrimaryProcedureCodeSecond = null
	,OPCSPrimaryProcedureDateSecond = null
	,ReadProcedureSchemeCode = null
	,ReadPrimaryProcedureCode = null
	,ReadPrimaryProcedureDate = null
	,ReadPrimaryProcedureCodeSecond = null
	,ReadPrimaryProcedureDateSecond = null

	,TreatmentSchemeCode = '01'
		--case
		--when Treatment1.TreatmentCode is null then null
		--else '01'
		--end

	,TreatmentCodeFirst = 
		coalesce(
			Treatment1.TreatmentCode
			,'99'
	   )

	,TreatmentDateFirst =
		left(convert(varchar, 
			coalesce(Treatment1.TreatmentDate , Encounter.ArrivalDate)
			, 103), 10
		)

	,TreatmentCodeSecond = Treatment2.TreatmentCode
		--coalesce(
		--	Treatment2.TreatmentCode
		--	,'99'
	 --  )

	,TreatmentDateSecond = left(convert(varchar , Treatment2.TreatmentDate , 103), 10)
		--left(convert(varchar, 
		--coalesce(Treatment2.TreatmentDate , Encounter.ArrivalDate)
		--	, 103), 10
		--)

	,TreatmentCode1 = Treatment3.TreatmentCode
	,TreatmentDate1 = left(convert(varchar, Treatment3.TreatmentDate, 103), 10)
	,TreatmentCode2 = Treatment4.TreatmentCode
	,TreatmentDate2 = left(convert(varchar, Treatment4.TreatmentDate, 103), 10)
	,TreatmentCode3 = Treatment5.TreatmentCode
	,TreatmentDate3 = left(convert(varchar, Treatment5.TreatmentDate, 103), 10)
	,TreatmentCode4 = Treatment6.TreatmentCode
	,TreatmentDate4 = left(convert(varchar, Treatment6.TreatmentDate, 103), 10)
	,TreatmentCode5 = Treatment7.TreatmentCode
	,TreatmentDate5 = left(convert(varchar, Treatment7.TreatmentDate, 103), 10)
	,TreatmentCode6 = Treatment8.TreatmentCode
	,TreatmentDate6 = left(convert(varchar, Treatment8.TreatmentDate, 103), 10)
	,TreatmentCode7 = Treatment9.TreatmentCode
	,TreatmentDate7 = left(convert(varchar, Treatment9.TreatmentDate, 103), 10)
	,TreatmentCode8 = Treatment10.TreatmentCode
	,TreatmentDate8 = left(convert(varchar, Treatment10.TreatmentDate, 103), 10)
	,TreatmentCode9 = Treatment11.TreatmentCode
	,TreatmentDate9 = left(convert(varchar, Treatment11.TreatmentDate, 103), 10)
	,TreatmentCode10 = Treatment12.TreatmentCode
	,TreatmentDate10 = left(convert(varchar, Treatment12.TreatmentDate, 103), 10)

	,HRGCode =
		--HRG4Encounter.HRGCode --this is 6 characters wide and the CDS expects 3
		null

	,HRGVersionCode
	,HRGProcedureSchemeCode = null
	,DGVPCode = null
from
	(
	select
		 wrkCDS.CDSTypeCode
		,Encounter.MergeEncounterRecno
		,SourceUniqueID
		,UniqueBookingReferenceNo
		,PathwayId
		,PathwayIdIssuerCode
		,RTTStatusCode
		,RTTStartDate
		,RTTEndDate
		,Encounter.DistrictNo
		,TrustNo
		,CasenoteNo
		,DistrictNoOrganisationCode
		,Encounter.NHSNumber

		,NHSNumberStatusId =
			NHSNumberStatus.NationalValueCode

		,PatientTitle
		,PatientForename
		,PatientSurname

		,PatientAddress1 = 
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
				PatientAddress1
				,','
				,' '
			)
				,''''
				,' '
			)
				,'{'
				,' '
			)
				,'}'
				,' '
			)
				,'`'
				,' '
			)


		,PatientAddress2 = 
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
				PatientAddress2
				,','
				,' '
			)
				,''''
				,' '
			)
				,'{'
				,' '
			)
				,'}'
				,' '
			)
				,'`'
				,' '
			)

		,PatientAddress3 = 
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
				PatientAddress3
				,','
				,' '
			)
				,''''
				,' '
			)
				,'{'
				,' '
			)
				,'}'
				,' '
			)
				,'`'
				,' '
			)

		,PatientAddress4 = 
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
			REPLACE(
				PatientAddress4
				,','
				,' '
			)
				,''''
				,' '
			)
				,'{'
				,' '
			)
				,'}'
				,' '
			)
				,'`'
				,' '
			)

		,Postcode =
			coalesce(
				 Encounter.Postcode
				--,Patient.Postcode
				,'ZZ99 3WZ'
			)

		,Encounter.DateOfBirth
		,Encounter.DateOfDeath

		,SexCode = 
			'0' + Sex.NationalValueCode

		,CarerSupportIndicator
		,Encounter.RegisteredGpCode
		,RegisteredGpPracticeCode = Encounter.RegisteredPracticeCode
		,AttendanceNumber

		,ArrivalModeCode =
			ArrivalMode.NationalValueCode

		,AttendanceCategoryCode =
			AttendanceCategory.NationalValueCode

		,AttendanceDisposalCode =
			AttendanceDisposal.NationalValueCode

		,IncidentLocationTypeCode =
			IncidentLocationType.NationalValueCode

		,PatientGroupCode =
			PatientGroup.NationalValueCode

		,SourceOfReferralCode =
			SourceOfReferral.NationalValueCode

		,Encounter.ArrivalDate
		,ArrivalTime
		,AgeOnArrival
		,InitialAssessmentTime
		,SeenForTreatmentTime
		,AttendanceConclusionTime
		,DepartureTime = 
			case 
				when Encounter.ContextCode like 'TRA||%' then convert(varchar, Encounter.AttendanceConclusionTime, 108)
				else convert(varchar, Encounter.DepartureTime, 108)
			end

		--,CommissioningSerialNo =
		--	case

		--	when Encounter.PCTCode like '0%' --scottish
		--		then Postcode.DistrictOfResidence + '9A'

		--	when Encounter.PatientAddress1 like 'N%F%A%' --no fixed abode
		--		then
		--			case
		--			when Encounter.ContextCode like 'TRA||%' then '5NR000'
		--			else '5NT00A'
		--			end

		--	when
		--		Encounter.CommissioningSerialNo is null
		--	or	Encounter.CommissionerCode = 'X98'
		--		then
		--			case
		--			when Encounter.CommissionerCode = 'TDH' then 'OSV00A'
		--			when PatientAddress1 like 'Visit%' then 'OSV00A'
		--			else 
		--				case
		--				when Encounter.ContextCode like 'TRA||%' then '5NR000'
		--				when Encounter.ContextCode like 'CEN||%' then '5NT00A'
		--				end
		--			end
		--	else Encounter.CommissioningSerialNo
		--	end


--modify to use CCG
		,CommissioningSerialNo =


			case

			when Encounter.PCTCode like '0%' --scottish
				then Postcode.DistrictOfResidence + '9A'

			when Encounter.PatientAddress1 like 'N%F%A%' --no fixed abode
				then
					case
					when Encounter.ContextCode like 'TRA||%' then '5NR000'
					else '5NT00A'
					end

			when
				Encounter.CommissioningSerialNo is null
			or	Encounter.CommissionerCode = 'X98'
				then
					case
					when Encounter.CommissionerCode = 'TDH' then 'OSV00A'
					when PatientAddress1 like 'Visit%' then 'OSV00A'
					else 
						case
						when Encounter.ContextCode like 'TRA||%' then '5NR000'
						when Encounter.ContextCode like 'CEN||%' then '5NT00A'
						end
					end
			else Encounter.CommissioningSerialNo
			end

		,NHSServiceAgreementLineNo
		,ProviderReferenceNo
		,CommissionerReferenceNo
		,ProviderCode

		--,CommissionerCode =
		--	case
		--	when Encounter.PCTCode like '0%' --scottish
		--		then Postcode.DistrictOfResidence
		--	when Encounter.PatientAddress1 like 'N%F%A%' --no fixed abode
		--		then 
		--			case
		--			when Encounter.ContextCode like 'TRA||%' then '5NR'
		--			when Encounter.ContextCode like 'CEN||%' then '5NT'
		--			end

		--	when
		--		Encounter.CommissioningSerialNo is null
		--	or	Encounter.CommissionerCode = 'X98'
		--	then
		--		case
		--		when Encounter.CommissionerCode = 'TDH' then 'OSV'
		--		when PatientAddress1 like 'Visit%' then 'OSV'
		--		else 
		--			case
		--			when Encounter.ContextCode like 'TRA||%' then '5NR'
		--			when Encounter.ContextCode like 'CEN||%' then '5NT'
		--			end
		--		end
		--	else left(Encounter.CommissioningSerialNo, 3)
		--	end

--modify to use CCG
		,CommissionerCode =

		case
			
			when left(Encounter.ResidencePCTCode,1) = '7' and Encounter.ContextCode like 'TRA||%' then '02A'
			when left(Encounter.ResidencePCTCode,1) = '7' and Encounter.ContextCode not like 'TRA||%' then '00W'
			--Scotland
			when left(Encounter.ResidencePCTCode,1) = '0' and Encounter.ContextCode like 'TRA||%' then '02A'  
			when left(Encounter.ResidencePCTCode,1) = '0' and Encounter.ContextCode not like 'TRA||%' then '00W'  
			--NI
			when left(Encounter.ResidencePCTCode,1) =  'Z' and Encounter.ContextCode like 'TRA||%' then '02A'  
			when left(Encounter.ResidencePCTCode,1) =  'Z' and Encounter.ContextCode not like 'TRA||%' then '00W'  
			
				else
				coalesce(
						Encounter.CCGCode, PostCode.CCGCode,

									case
					when Encounter.ContextCode like 'TRA||%' then '02A'
					when Encounter.ContextCode like 'CEN||%' then '00W'
					end
					)
			
			end 
			--case
			--when Encounter.PCTCode like '0%' --scottish
			--	then Postcode.DistrictOfResidence
			--when Encounter.PatientAddress1 like 'N%F%A%' --no fixed abode
			--	then 
			--		case
			--		when Encounter.ContextCode like 'TRA||%' then '5NR'
			--		when Encounter.ContextCode like 'CEN||%' then '5NT'
			--		end

			--when
			--	Encounter.CommissioningSerialNo is null
			--or	Encounter.CommissionerCode = 'X98'
			--then
			--	case
			--	when Encounter.CommissionerCode = 'TDH' then 'OSV'
			--	when PatientAddress1 like 'Visit%' then 'OSV'
			--	else 
			--		case
			--		when Encounter.ContextCode like 'TRA||%' then '5NR'
			--		when Encounter.ContextCode like 'CEN||%' then '5NT'
			--		end
			--	end
			--else left(Encounter.CommissioningSerialNo, 3)
			--end

		,StaffMemberCode =
			StaffMember.LocalValueCode

		--,InvestigationCodeFirst =
		--	InvestigationFirst.ValueCode

		--,InvestigationCodeSecond =
		--	InvestigationSecond.ValueCode

		--,DiagnosisCodeFirst =
		--	DiagnosisFirst.ValueCode

		--,DiagnosisCodeSecond =
		--	DiagnosisSecond.ValueCode

		--,TreatmentCodeFirst =
		--	TreatmentFirst.ValueCode

		--,TreatmentDateFirst

		--,TreatmentCodeSecond =
		--	TreatmentSecond.ValueCode

		--,TreatmentDateSecond
		,PASHRGCode
		,HRGVersionCode
		,PASDGVPCode

		,SiteCode =
			[Site].NationalValueCode

		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
		,InterfaceCode

		--,PCTCode =
		--	case
		--	when
		--		coalesce(
		--			 Encounter.Postcode
		--			--,Patient.Postcode
		--			,'ZZ99 3WZ'
		--		) like 'Z%'
		--		then 'X98'

		--	else Encounter.PCTCode
		--	end

		--,ResidencePCTCode


--modify to use CCG
		,PCTCode =
			case

			when left(Encounter.ResidencePCTCode,1) = '7' and Encounter.ContextCode like 'TRA||%' then '02A'
			when left(Encounter.ResidencePCTCode,1) = '7' and Encounter.ContextCode not like 'TRA||%' then '00W'
			--Scotland
			when left(Encounter.ResidencePCTCode,1) = '0' and Encounter.ContextCode like 'TRA||%' then '02A'  
			when left(Encounter.ResidencePCTCode,1) = '0' and Encounter.ContextCode not like 'TRA||%' then '00W'  
			--NI
			when left(Encounter.ResidencePCTCode,1) =  'Z' and Encounter.ContextCode like 'TRA||%' then '02A'  
			when left(Encounter.ResidencePCTCode,1) =  'Z' and Encounter.ContextCode not like 'TRA||%' then '00W'  

			when
				coalesce(
					 Encounter.CCGCode, PostCode.CCGCode
					 --Encounter.Postcode
					--,Patient.Postcode
					,'ZZ99 3WZ'
				) like 'Z%'
				then 'X98'

			else Encounter.PCTCode
			end

		,ResidencePCTCode =

			coalesce(
						Encounter.CCGCode, PostCode.CCGCode
				,'X98'
				)


		,InvestigationCodeList

		,TriageCategoryCode =
			TriageCategory.NationalValueCode

		,PresentingProblem
		,ToXrayTime
		,FromXrayTime
		,ToSpecialtyTime
		,SeenBySpecialtyTime

		,EthnicCategoryCode =
			EthnicCategory.NationalValueCode


		,ReferredToSpecialtyCode =
			ReferredToSpecialty.NationalValueCode

		,DecisionToAdmitTime
		,Encounter.ContextCode
		,Encounter.EPMINo

	from
		WarehouseOLAPMergedV2.AE.BaseEncounter Encounter

	inner join WarehouseOLAPMergedV2.AE.BaseEncounterReference EncounterReference
	on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join CDS.wrkCDS
	on	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
	and	wrkCDS.CDSTypeCode in ('010')

	--left join Warehouse.PAS.Patient
	--on	Patient.DistrictNo = Encounter.DistrictNo

	left join Organisation.ods.Postcode Postcode on
		Postcode.Postcode = 
			case
			when len(Encounter.Postcode) = 8 then Encounter.Postcode
			else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
			end

	--SEX	Sex
	left join WarehouseOLAPMergedV2.WH.Member Sex
	on	Sex.SourceValueID = EncounterReference.SexID

	--ARRMOD	ArrivalMode
	left join WarehouseOLAPMergedV2.WH.Member ArrivalMode
	on	ArrivalMode.SourceValueID = EncounterReference.ArrivalModeID


	--ATTCAT	Attendance Category
	left join WarehouseOLAPMergedV2.WH.Member AttendanceCategory
	on	AttendanceCategory.SourceValueID = EncounterReference.AttendanceCategoryID


	--ATTDIS	Attendance Disposal
	left join WarehouseOLAPMergedV2.WH.Member AttendanceDisposal
	on	AttendanceDisposal.SourceValueID = EncounterReference.AttendanceDisposalID


	--INCLOC	Incident Location Type
	left join WarehouseOLAPMergedV2.WH.Member IncidentLocationType
	on	IncidentLocationType.SourceValueID = EncounterReference.IncidentLocationTypeID


	--PATGRP	Patient Group
	left join WarehouseOLAPMergedV2.WH.Member PatientGroup
	on	PatientGroup.SourceValueID = EncounterReference.PatientGroupID


	--SCRFAE	Source of Referral for AE
	left join WarehouseOLAPMergedV2.WH.Member SourceOfReferral
	on	SourceOfReferral.SourceValueID = EncounterReference.SourceOfReferralID


	--STAFFM	Staff Member
	left join WarehouseOLAPMergedV2.WH.Member StaffMember
	on	StaffMember.SourceValueID = EncounterReference.StaffMemberID


	--SITE	Site
	left join WarehouseOLAPMergedV2.WH.Member [Site]
	on	[Site].SourceValueID = EncounterReference.SiteID


	--TRICAT	Triage Category
	left join WarehouseOLAPMergedV2.WH.Member TriageCategory
	on	TriageCategory.SourceValueID = EncounterReference.TriageCategoryID


	--ETHCAT	Ethnic Category
	left join WarehouseOLAPMergedV2.WH.Member EthnicCategory
	on	EthnicCategory.SourceValueID = EncounterReference.EthnicOriginID


	--SPEC	Specialty
	left join WarehouseOLAPMergedV2.WH.Member ReferredToSpecialty
	on	ReferredToSpecialty.SourceValueID = EncounterReference.ReferredToSpecialtyID


	--NHSNST	NHS Number Status
	left join WarehouseOLAPMergedV2.WH.Member NHSNumberStatus
	on	NHSNumberStatus.SourceValueID = EncounterReference.NHSNumberStatusID

	) Encounter

left join CDS.AEDiagnosisDistinct Diagnosis1	
on	Diagnosis1.ContextCode = Encounter.ContextCode
and	Diagnosis1.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis1.SequenceNo = 1

left join CDS.AEDiagnosisDistinct Diagnosis2	
on	Diagnosis2.ContextCode = Encounter.ContextCode
and	Diagnosis2.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis2.SequenceNo = 2

left join CDS.AEDiagnosisDistinct Diagnosis3	
on	Diagnosis3.ContextCode = Encounter.ContextCode
and	Diagnosis3.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis3.SequenceNo = 3

left join CDS.AEDiagnosisDistinct Diagnosis4	
on	Diagnosis4.ContextCode = Encounter.ContextCode
and	Diagnosis4.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis4.SequenceNo = 4

left join CDS.AEDiagnosisDistinct Diagnosis5	
on	Diagnosis5.ContextCode = Encounter.ContextCode
and	Diagnosis5.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis5.SequenceNo = 5

left join CDS.AEDiagnosisDistinct Diagnosis6	
on	Diagnosis6.ContextCode = Encounter.ContextCode
and	Diagnosis6.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis6.SequenceNo = 6

left join CDS.AEDiagnosisDistinct Diagnosis7	
on	Diagnosis7.ContextCode = Encounter.ContextCode
and	Diagnosis7.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis7.SequenceNo = 7

left join CDS.AEDiagnosisDistinct Diagnosis8	
on	Diagnosis8.ContextCode = Encounter.ContextCode
and	Diagnosis8.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis8.SequenceNo = 8

left join CDS.AEDiagnosisDistinct Diagnosis9	
on	Diagnosis9.ContextCode = Encounter.ContextCode
and	Diagnosis9.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis9.SequenceNo = 9

left join CDS.AEDiagnosisDistinct Diagnosis10	
on	Diagnosis10.ContextCode = Encounter.ContextCode
and	Diagnosis10.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis10.SequenceNo = 10

left join CDS.AEDiagnosisDistinct Diagnosis11	
on	Diagnosis11.ContextCode = Encounter.ContextCode
and	Diagnosis11.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis11.SequenceNo = 11

left join CDS.AEDiagnosisDistinct Diagnosis12	
on	Diagnosis12.ContextCode = Encounter.ContextCode
and	Diagnosis12.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis12.SequenceNo = 12

left join CDS.AEDiagnosisDistinct Diagnosis13	
on	Diagnosis13.ContextCode = Encounter.ContextCode
and	Diagnosis13.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis13.SequenceNo = 13

left join CDS.AEDiagnosisDistinct Diagnosis14
on	Diagnosis14.ContextCode = Encounter.ContextCode
and	Diagnosis14.AESourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis14.SequenceNo = 14

left join CDS.AEInvestigationDistinct Investigation1	
on	Investigation1.ContextCode = Encounter.ContextCode
and	Investigation1.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation1.SequenceNo = 1

left join CDS.AEInvestigationDistinct Investigation2	
on	Investigation2.ContextCode = Encounter.ContextCode
and	Investigation2.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation2.SequenceNo = 2

left join CDS.AEInvestigationDistinct Investigation3	
on	Investigation3.ContextCode = Encounter.ContextCode
and	Investigation3.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation3.SequenceNo = 3

left join CDS.AEInvestigationDistinct Investigation4	
on	Investigation4.ContextCode = Encounter.ContextCode
and	Investigation4.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation4.SequenceNo = 4

left join CDS.AEInvestigationDistinct Investigation5	
on	Investigation5.ContextCode = Encounter.ContextCode
and	Investigation5.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation5.SequenceNo = 5

left join CDS.AEInvestigationDistinct Investigation6	
on	Investigation6.ContextCode = Encounter.ContextCode
and	Investigation6.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation6.SequenceNo = 6

left join CDS.AEInvestigationDistinct Investigation7	
on	Investigation7.ContextCode = Encounter.ContextCode
and	Investigation7.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation7.SequenceNo = 7

left join CDS.AEInvestigationDistinct Investigation8	
on	Investigation8.ContextCode = Encounter.ContextCode
and	Investigation8.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation8.SequenceNo = 8

left join CDS.AEInvestigationDistinct Investigation9	
on	Investigation9.ContextCode = Encounter.ContextCode
and	Investigation9.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation9.SequenceNo = 9

left join CDS.AEInvestigationDistinct Investigation10	
on	Investigation10.ContextCode = Encounter.ContextCode
and	Investigation10.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation10.SequenceNo = 10

left join CDS.AEInvestigationDistinct Investigation11	
on	Investigation11.ContextCode = Encounter.ContextCode
and	Investigation11.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation11.SequenceNo = 11

left join CDS.AEInvestigationDistinct Investigation12	
on	Investigation12.ContextCode = Encounter.ContextCode
and	Investigation12.AESourceUniqueID = Encounter.SourceUniqueID
and	Investigation12.SequenceNo = 12

left join CDS.AETreatmentDistinct Treatment1	
on	Treatment1.ContextCode = Encounter.ContextCode
and	Treatment1.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment1.SequenceNo = 1

left join CDS.AETreatmentDistinct Treatment2	
on	Treatment2.ContextCode = Encounter.ContextCode
and	Treatment2.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment2.SequenceNo = 2

left join CDS.AETreatmentDistinct Treatment3	
on	Treatment3.ContextCode = Encounter.ContextCode
and	Treatment3.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment3.SequenceNo = 3

left join CDS.AETreatmentDistinct Treatment4	
on	Treatment4.ContextCode = Encounter.ContextCode
and	Treatment4.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment4.SequenceNo = 4

left join CDS.AETreatmentDistinct Treatment5	
on	Treatment5.ContextCode = Encounter.ContextCode
and	Treatment5.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment5.SequenceNo = 5

left join CDS.AETreatmentDistinct Treatment6	
on	Treatment6.ContextCode = Encounter.ContextCode
and	Treatment6.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment6.SequenceNo = 6

left join CDS.AETreatmentDistinct Treatment7	
on	Treatment7.ContextCode = Encounter.ContextCode
and	Treatment7.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment7.SequenceNo = 7

left join CDS.AETreatmentDistinct Treatment8	
on	Treatment8.ContextCode = Encounter.ContextCode
and	Treatment8.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment8.SequenceNo = 8

left join CDS.AETreatmentDistinct Treatment9	
on	Treatment9.ContextCode = Encounter.ContextCode
and	Treatment9.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment9.SequenceNo = 9

left join CDS.AETreatmentDistinct Treatment10	
on	Treatment10.ContextCode = Encounter.ContextCode
and	Treatment10.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment10.SequenceNo = 10

left join CDS.AETreatmentDistinct Treatment11	
on	Treatment11.ContextCode = Encounter.ContextCode
and	Treatment11.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment11.SequenceNo = 11

left join CDS.AETreatmentDistinct Treatment12	
on	Treatment12.ContextCode = Encounter.ContextCode
and	Treatment12.AESourceUniqueID = Encounter.SourceUniqueID
and	Treatment12.SequenceNo = 12


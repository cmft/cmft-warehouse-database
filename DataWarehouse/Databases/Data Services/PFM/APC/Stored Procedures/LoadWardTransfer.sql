﻿CREATE PROCEDURE [APC].[LoadWardTransfer]
AS
BEGIN


SET dateformat dmy


SELECT DISTINCT 
[WARDSTAYID]
,[AdmitDate]
,[AdmitTime]
,[ConsultantCode]
,[EndActivity]
,[EndDtTimeInt]
,[EpisodeNumber]
,[EpsActvDtimeInt]
      

,StartDate =
    case
    when StartTime = ':'
    then StartDate
    else
          cast(
                case
                when StartTime = '24:00'
                      then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
                else convert(smalldatetime , StartDate)
                end
                as date
          )
    end

,EndDate =
    case when EndTime = ':'then EndDate
    else cast( case when EndTime = '24:00'
                    then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
                else convert(smalldatetime , EndDate)
                end
                as date        )
    end


,StartTime =
    case when StartTime = ':' then null
    else case   when StartTime = '24:00'
                then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
          else convert(smalldatetime , StartDate + ' ' + StartTime)
          end
    end

,EndTime =
    case when EndTime = ':' then null
    else  case
          when EndTime = '24:00'
                then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
          else convert(smalldatetime , EndDate + ' ' + EndTime)
          end
    end
    
,[HospitalCode]
,[InternalPatientNumber]
,[IpAdmDtimeInt]
,[Specialty]
,[StartActivity]
,[WardCode]
,[SequenceNo1] = ROW_NUMBER() over(partition by [InternalPatientNumber],[EpisodeNumber] order by [InternalPatientNumber],[EpisodeNumber],[EpsActvDtimeInt] )


INTO #Ward
FROM warehousesql.[PAS].[InquireUpdate].[WARDSTAY]

---------------------------------------------------------------------------------------------- 

SELECT DISTINCT 
WS1.[WARDSTAYID]
,WS1.[AdmitDate]
,WS1.[AdmitTime]
,WS1.[ConsultantCode]
,WS1.[EndActivity]
,WS1.[EndDtTimeInt]
,WS1.[EpisodeNumber]
,WS1.[EpsActvDtimeInt]
,WS1.StartDate
,WS1.EndDate
,WS1.StartTime
,WS1.EndTime
,WS1.[HospitalCode]
,WS1.[InternalPatientNumber]
,WS1.[IpAdmDtimeInt]
,WS1.[Specialty]
,WS1.[StartActivity]
,WS1.[WardCode]
,WS1.[SequenceNo1]
,PreWard=WS2.[WardCode]
,PreSeq=WS2.[SequenceNo1]
,PreStartTime=WS2.StartTime
,PreEndTime=WS2.EndTime
,PreEndAct=WS2.[EndActivity]
,NationalAdmissionMethod
,AdmissionType
,Census= ( SELECT TOP 1 MAX(endtime) FROM #Ward WHERE endtime <= GetDate() )


INTO #ward2
FROM #Ward WS1

  
  
	LEFT JOIN #Ward WS2
	on  WS1.[InternalPatientNumber]=WS2.[InternalPatientNumber]
	and  WS1.[EpisodeNumber]=WS2.[EpisodeNumber]
	and  WS1.[SequenceNo1]=WS2.[SequenceNo1]+1

	LEFT JOIN
			   (SELECT DISTINCT
				apc.[SourcePatientNo],
				apc.[SourceSpellNo],
				[AdmissionMethodCode],
				[NationalAdmissionMethod],
				AdmissionType

				FROM  warehousesql.warehouse.APCUpdate.Encounter apc
				left join  warehousesql.Warehousereportingmerged.apc.AdmissionMethod
				on [SourceAdmissionMethodCode]=[AdmissionMethodCode]
				)Adm

		on Adm.[SourcePatientNo]= WS1.[InternalPatientNumber]
		and Adm.[SourceSpellNo] = WS1.[EpisodeNumber]



WHERE	WS1.StartActivity='TR'
and		WS1.StartDate= cast(GETDATE() as DATE)





DECLARE @NewCensus as datetime
DECLARE @CurrentCensus as datetime

SET @NewCensus = (Select top 1 max(endtime) FROM #Ward where endtime <= GetDate())
SET @CurrentCensus = (Select top 1 max(census) FROM APC.Transfers)

If @NewCensus > dateadd(minute,30,@CurrentCensus) and @CurrentCensus is not null
BEGIN



insert into APC.Transfers




Select
Census
,PreWardBucket=Pre.WardBucket
,PostWardBucket=Post.WardBucket
,[WARDSTAYID]
,[AdmitDate]
,[AdmitTime]
,[ConsultantCode]
,[EndActivity]
,[EndDtTimeInt]
,[EpisodeNumber]
,[EpsActvDtimeInt]
,StartDate
,EndDate
,StartTime
,EndTime
,[HospitalCode]
,[InternalPatientNumber]
,[IpAdmDtimeInt]
,[Specialty]
,[StartActivity]
,PostWard=#Ward2.[WardCode]
,[SequenceNo1]
,PreWard
,PreSeq
,PreStartTime
,PreEndTime
,PreEndAct
,[NationalAdmissionMethod]
,AdmissionType



FROM #Ward2
	LEFT JOIN APC.LKPWard Pre
	ON Preward=Pre.[WardCode]

	LEFT JOIN APC.LKPWard Post 
	ON #ward2.wardcode=Post.[WardCode]


END


drop table #ward
drop table #ward2


end
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [APC].[LoadAdmissions]
AS
BEGIN


DECLARE @NewCensus as datetime
DECLARE @CurrentCensus as datetime

SET @NewCensus = (SELECT MAX(AdmissionTime) FROM warehouseSQL.[Warehouse].[APCUpdate].[Encounter] WHERE AdmissionTime < getdate())
SET @CurrentCensus = (Select top 1 MAX(census) FROM APC.Admissions)


If @NewCensus > dateadd(minute,30,@CurrentCensus) and @CurrentCensus is not null
BEGIN

INSERT INTO APC.Admissions


SELECT 

AdmitType = case when NationalAdmissionmethod = 'Emergency Admission - Accident And Emergency' then 'AE' else 'Direct' end
,WardBucket
,Division
,SourcePatientNo
,SourceSpellNo
,APC.PatientForename
,APC.PatientSurname
,AdmissionTime
,APC.StartWardTypeCode
,APC.CasenoteNumber
,NationalAdmissionMethod
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant = Consultant.Consultant
,DTOC=case when ProviderSpellNo=GlobalProviderSpellNo then 'YES' else null end
,DTOC_MFD=[MFD_Medically_Fit_Date]
,Admissions=count([EncounterRecno])
,Census = (SELECT MAX(AdmissionTime) FROM warehouseSQL.[Warehouse].[APCUpdate].[Encounter] )

FROM warehouseSQL.[Warehouse].[APCUpdate].[Encounter] APC

		LEFT JOIN APC.LKPWard
		ON [StartWardTypeCode]=[WardCode]

		left join	warehousereportingmerged.APC.AdmissionMethod
		on  Admissionmethodcode=sourceAdmissionmethodcode

		left outer join  warehousesql.warehouse.pas.Consultant
		on Consultant.ConsultantCode = apc.ConsultantCode

		inner join warehousesql.warehouse.WH.Directorate 
		on Directorate.DirectorateCode = apc.StartDirectorateCode		  

		left join warehousesql.[Warehouse].[WH].[Division]
		on [Division].[DivisionCode]=[Directorate].[DivisionCode]

		left join [warehouse2\dev].[MRI_Discharge_Egress].[dbo].[CDT List]
		on ProviderSpellNo=GlobalProviderSpellNo

		left join warehousesql.[WarehouseReportingMerged].[WH].[Specialty]
		on SourceSpecialtyCode=Specialtycode



  
WHERE		admissiondate= CAST ( GETDATE() as DATE)
  and		AdmissionTime=EpisodeStartTime
  and		startSiteCode = 'MRI'
  and		patientCategorycode not in ('DC','RD')
  and		ManagementIntentionCode<>'D'
  

  
GROUP BY
  

case when NationalAdmissionmethod = 'Emergency Admission - Accident And Emergency' then 'AE' else 'Direct' end
,WardBucket
,Division
,SourcePatientNo
,SourceSpellNo
,APC.PatientForename
,APC.PatientSurname
,AdmissionTime
,APC.StartWardTypeCode
,APC.CasenoteNumber
,NationalAdmissionMethod
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant.Consultant
,case when ProviderSpellNo=GlobalProviderSpellNo then 'YES' else null end
,MFD_Medically_Fit_Date


END
END
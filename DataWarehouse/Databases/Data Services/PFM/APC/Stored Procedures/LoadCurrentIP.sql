﻿CREATE PROCEDURE [APC].[LoadCurrentIP]
AS
BEGIN
	
	
	
declare @sitrepday datetime  = getdate()


----------------------------------------------------------------------------------------------

set dateformat dmy

SELECT distinct 

[WARDSTAYID]
      ,[AdmitDate]
      ,[AdmitTime]
      ,[ConsultantCode]
      ,[EndActivity]
      ,[EndDtTimeInt]
      ,[EpisodeNumber]
      ,[EpsActvDtimeInt]
      
 
       	,StartDate =
		case
		when StartTime = ':'
		then StartDate
		else
			cast(
				case
				when StartTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
				else convert(smalldatetime , StartDate)
				end
				as date
			)
		end

	,EndDate =
		case
		when EndTime = ':'
		then EndDate
		else
			cast(
				case
				when EndTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
				else convert(smalldatetime , EndDate)
				end
				as date
			)
		end


	,StartTime =
		case
		when StartTime = ':'
		then null
		else
			case
			when StartTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
			else convert(smalldatetime , StartDate + ' ' + StartTime)
			end
		end

	,EndTime =
		case
		when EndTime = ':'
		then null
		else
			case
			when EndTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
			else convert(smalldatetime , EndDate + ' ' + EndTime)
			end
		end
      ,[HospitalCode]
      ,[InternalPatientNumber]
      ,[IpAdmDtimeInt]
      ,[Specialty]
      ,[StartActivity]
  
      ,[WardCode]
  
  INTO #WS
  FROM warehousesql.[PAS].[InquireUpdate].[WARDSTAY]


------------------------------------------------------------------------------------------------
DECLARE @NewCensus as datetime
DECLARE @CurrentCensus as datetime

SET @NewCensus = (Select top 1 max(endtime) FROM #WS where endtime <= GetDate())
SET @CurrentCensus = (Select top 1 max(census) FROM APC.CurrentIP)

If @NewCensus > dateadd(minute,30,@CurrentCensus) and @CurrentCensus is not null
BEGIN

insert       into APC.CurrentIP


Select
OutlierFlag = case when wardstay.wardcode ='OMU' and Directorate <>'Emergency Serv' then 'YES'
when wardstay.wardcode in ('ESTU','SAL','7s','8HPB','8M','9M','10M','11M','12M','14M','ETCD','ETCS')
AND [Directorate].[DivisionCode] <>'Surg' THEN 'YES'

when wardstay.wardcode in ('44','36','37','37A','ACC','CSITU','3m','4m')
AND [Directorate].[DivisionCode] <>'MEDSP' THEN 'YES'

when wardstay.wardcode in ('AM3','AM4')
AND [Directorate].[DivisionCode] not in ('MEDSP','MEDAC') THEN 'YES'

when wardstay.wardcode in ('AMU','AM1','AM2','5M','30M','31R','31M','32M','45','46','15m','27m')
AND [Directorate].[DivisionCode]='Surg' THEN 'YES'
when wardstay.wardcode in ('54','55I','DC','55D')
AND [Directorate].[DivisionCode] <>'REH' THEN 'YES'
when wardstay.wardcode in ('47A','47B','47C','62','63','64','64A','64B','64C','64HD','64RB','64RC','65','65C','65I','66','66C','68','90','BCS','BCSC','HB','HM','NICT','SUBF')

AND [Directorate].[DivisionCode] not in ('SMH','CHILD') THEN 'YES'

else 'NO' end

,OutlierType= case when wardstay.wardcode ='OMU' and Directorate <>'Emergency Serv' then 'Outliers on OMU'
when wardstay.wardcode in ('ESTU','SAL','7s','8HPB','8M','9M','10M','11M','12M','14M','ETCD','ETCS')
AND [Directorate].[DivisionCode] <>'Surg' THEN 'Outliers on Surgical Wards'

when wardstay.wardcode in ('44','36','37','37A','ACC','CSITU','3m','4m')
AND [Directorate].[DivisionCode] <>'MEDSP' THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AM3','AM4')
AND [Directorate].[DivisionCode] not in ('MEDSP','MEDAC') THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AMU','AM1','AM2','5M','30M','31R','31M','32M','45','46','15m','27m')
AND [Directorate].[DivisionCode]='Surg' THEN 'Surgical Outliers on Acute Medical Wards'
when wardstay.wardcode in ('54','55I','DC','55D')
AND [Directorate].[DivisionCode] <>'REH' THEN 'Outliers on Royal Eye Wards'
when wardstay.wardcode in ('47A','47B','47C','62','63','64','64A','64B','64C','64HD','64RB','64RC','65','65C','65I','66','66C','68','90','BCS','BCSC','HB','HM','NICT','SUBF')

AND [Directorate].[DivisionCode] not in ('SMH','CHILD') THEN 'Outliers on St Marys Wards'

else null end

,Division
,WardBucket
,SourcePatientNo
,SourceSpellNo
,apc.PatientForename
,apc.PatientSurname
,AdmissionTime
,StartTime
,wardstay.wardcode
,apc.CasenoteNumber
,[NationalAdmissionMethod]
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant =      Consultant.Consultant
,WardDesc = 
      isnull(Wardbase.Ward,wardstay.wardcode)
      
      ,DTOC=case when ProviderSpellNo=GlobalProviderSpellNo then 'YES' else null end
      ,DTOC_MFD=[MFD_Medically_Fit_Date]
      
      ,Census=
      (Select top 1
      max(endtime)
       FROM #WS
       where endtime <= GetDate()
      )

from warehousesql.warehouse.APCUpdate.Encounter apc

join #WS  wardstay on
		
		WardStay.InternalPatientNumber = apc.[SourcePatientNo]
	and	WardStay.EpisodeNumber = apc.[SourceSpellNo]
	and starttime < @sitrepday and (endtime >= @sitrepday or endtime is null)
	and EpisodeStarttime < @sitrepday and (EpisodeEndtime >= @sitrepday or EpisodeEndtime is null)




inner join warehousesql.warehouse.WH.Directorate 
on Directorate.DirectorateCode = apc.StartDirectorateCode

left join  APC.LKPWard ward on
ward.WardCode = wardstay.wardcode

left outer join warehousesql.warehouse.pas.Ward Wardbase
on wardbase.WardCode = wardstay.wardCode collate Latin1_General_CI_AS

left outer join  warehousesql.warehouse.pas.Consultant
on Consultant.ConsultantCode = apc.ConsultantCode

left join warehousesql.[Warehouse].[WH].[Division]
on [Division].[DivisionCode]=[Directorate].[DivisionCode]

left join [warehouse2\dev].[MRI_Discharge_Egress].[dbo].[CDT List]
on ProviderSpellNo=GlobalProviderSpellNo

left join warehousesql.[WarehouseReportingMerged].[WH].[Specialty]
on SourceSpecialtyCode=Specialtycode

left join  warehousesql.Warehousereportingmerged.apc.AdmissionMethod
on [SourceAdmissionMethodCode]=[AdmissionMethodCode]


group by


case when wardstay.wardcode ='OMU' and Directorate <>'Emergency Serv' then 'YES'
when wardstay.wardcode in ('ESTU','SAL','7s','8HPB','8M','9M','10M','11M','12M','14M','ETCD','ETCS')
AND [Directorate].[DivisionCode] <>'Surg' THEN 'YES'

when wardstay.wardcode in ('44','36','37','37A','ACC','CSITU','3m','4m')
AND [Directorate].[DivisionCode] <>'MEDSP' THEN 'YES'

when wardstay.wardcode in ('AM3','AM4')
AND [Directorate].[DivisionCode] not in ('MEDSP','MEDAC') THEN 'YES'

when wardstay.wardcode in ('AMU','AM1','AM2','5M','30M','31R','31M','32M','45','46','15m','27m')
AND [Directorate].[DivisionCode]='Surg' THEN 'YES'
when wardstay.wardcode in ('54','55I','DC','55D')
AND [Directorate].[DivisionCode] <>'REH' THEN 'YES'
when wardstay.wardcode in ('47A','47B','47C','62','63','64','64A','64B','64C','64HD','64RB','64RC','65','65C','65I','66','66C','68','90','BCS','BCSC','HB','HM','NICT','SUBF')

AND [Directorate].[DivisionCode] not in ('SMH','CHILD') THEN 'YES'

else 'NO' end

,case when wardstay.wardcode ='OMU' and Directorate <>'Emergency Serv' then 'Outliers on OMU'
when wardstay.wardcode in ('ESTU','SAL','7s','8HPB','8M','9M','10M','11M','12M','14M','ETCD','ETCS')
AND [Directorate].[DivisionCode] <>'Surg' THEN 'Outliers on Surgical Wards'

when wardstay.wardcode in ('44','36','37','37A','ACC','CSITU','3m','4m')
AND [Directorate].[DivisionCode] <>'MEDSP' THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AM3','AM4')
AND [Directorate].[DivisionCode] not in ('MEDSP','MEDAC') THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AMU','AM1','AM2','5M','30M','31R','31M','32M','45','46','15m','27m')
AND [Directorate].[DivisionCode]='Surg' THEN 'Surgical Outliers on Acute Medical Wards'
when wardstay.wardcode in ('54','55I','DC','55D')
AND [Directorate].[DivisionCode] <>'REH' THEN 'Outliers on Royal Eye Wards'
when wardstay.wardcode in ('47A','47B','47C','62','63','64','64A','64B','64C','64HD','64RB','64RC','65','65C','65I','66','66C','68','90','BCS','BCSC','HB','HM','NICT','SUBF')

AND [Directorate].[DivisionCode] not in ('SMH','CHILD') THEN 'Outliers on St Marys Wards'

else null end


,DIVISION
,WardBucket


,SourcePatientNo
,SourceSpellNo
,apc.PatientForename
,apc.PatientSurname
,apc.AdmissionTime
,StartTime
,wardstay.wardcode
,apc.CasenoteNumber
,[NationalAdmissionMethod]
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant.Consultant
,Wardbase.Ward
,case when ProviderSpellNo=GlobalProviderSpellNo then 'YES' else null end
,[MFD_Medically_Fit_Date]


END

drop table #WS
	
	
	
	
	
	
END
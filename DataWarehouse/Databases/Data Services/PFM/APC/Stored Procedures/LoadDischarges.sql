﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [APC].[LoadDischarges]
AS
BEGIN

DECLARE @NewCensus as datetime
DECLARE @CurrentCensus as datetime

SET @NewCensus = (SELECT MAX(DischargeTime) FROM warehouseSQL.[Warehouse].[APCUpdate].[Encounter] WHERE DischargeTime <= GETDATE()  )
SET @CurrentCensus = (Select top 1 max(census) FROM APC.Discharges)

SELECT @NewCensus

If @NewCensus > dateadd(minute,30,@CurrentCensus) and @CurrentCensus is not null
BEGIN

INSERT INTO APC.Discharges

SELECT 
WardBucket
,Division
,SourcePatientNo
,SourceSpellNo
,apc.PatientForename
,apc.PatientSurname
,AdmissionTime
,APC.[EndWardTypeCode]
,apc.CasenoteNumber
,[NationalAdmissionMethod]
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant = Consultant.Consultant
,DTOC=case when ProviderSpellNo=GlobalProviderSpellNo then 'YES' else null end
,DTOC_MFD=[MFD_Medically_Fit_Date]
,Discharges=count([EncounterRecno])
,Census = (SELECT MAX(AdmissionTime) FROM warehouseSQL.[Warehouse].[APCUpdate].[Encounter] )

    
     
FROM warehouseSQL.[Warehouse].[APCUpdate].[Encounter] APC
	LEFT JOIN APC.LKPWard 
	ON [EndWardTypeCode]=[WardCode]

	LEFT JOIN  warehousereportingmerged.APC.AdmissionMethod
	on Admissionmethodcode=sourceAdmissionmethodcode


	LEFT JOIN  warehousesql.warehouse.pas.Consultant
	on Consultant.ConsultantCode = apc.ConsultantCode

	LEFT JOIN warehousesql.warehouse.WH.Directorate 
	on Directorate.DirectorateCode = apc.StartDirectorateCode
  
	LEFT JOIN warehousesql.[Warehouse].[WH].[Division]
	on [Division].[DivisionCode]=[Directorate].[DivisionCode]

	LEFT JOIN [warehouse2\dev].[MRI_Discharge_Egress].[dbo].[CDT List]
	on ProviderSpellNo=GlobalProviderSpellNo

	LEFT JOIN warehousesql.[WarehouseReportingMerged].[WH].[Specialty]
	on SourceSpecialtyCode=Specialtycode



  
WHERE	dischargedate=CAST(getdate() as DATE)
  and	DischargeTime=EpisodeEndTime
  and	EndSiteCode = 'MRI'
  and	patientCategorycode not in ('DC','RD')
  and   DischargeTime <= GETDATE()  
  
GROUP BY
 WardBucket
,Division
,SourcePatientNo
,SourceSpellNo
,apc.PatientForename
,apc.PatientSurname
,AdmissionTime
,APC.[EndWardTypeCode]
,apc.CasenoteNumber
,[NationalAdmissionMethod]
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant.Consultant
,case when ProviderSpellNo=GlobalProviderSpellNo then 'YES' else null end
,[MFD_Medically_Fit_Date]
      

ORDER BY
WardBucket
,Division
,SourcePatientNo
,SourceSpellNo
,apc.PatientForename
,apc.PatientSurname
,AdmissionTime
,APC.[EndWardTypeCode]
,apc.CasenoteNumber
,[NationalAdmissionMethod]
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant.Consultant
,case when ProviderSpellNo=GlobalProviderSpellNo then 'YES' else null end
,[MFD_Medically_Fit_Date]

END
END
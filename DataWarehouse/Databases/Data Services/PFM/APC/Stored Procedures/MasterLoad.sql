﻿CREATE PROCEDURE [APC].[MasterLoad] 

AS
BEGIN


EXECUTE APC.LoadCurrentIP
EXECUTE APC.LoadDischarges
EXECUTE APC.LoadAdmissions
EXECUTE APC.LoadWardTransfer


DECLARE @MaxCensus as datetime
DECLARE @MaxCensus_Adm as datetime
DECLARE @MaxCensus_Dis as datetime
DECLARE @MaxCensus_Tx as datetime
DECLARE @MaxCensus_CurrentIP as datetime
DECLARE @Gap as integer


SET @MaxCensus_Adm = (SELECT MAX(Census) FROM APC.Admissions WHERE Census <= GETDATE())
SET @MaxCensus_Dis = (SELECT MAX(Census) FROM APC.Discharges WHERE Census <= GETDATE())
SET @MaxCensus_Tx = (SELECT MAX(Census) FROM APC.Transfers WHERE Census <= GETDATE())
SET @MaxCensus_CurrentIP = (SELECT MAX(Census) FROM APC.CurrentIP WHERE Census <= GETDATE())


SET @MaxCensus = CASE WHEN @MaxCensus_Adm > @MaxCensus_Dis then @MaxCensus_Adm ELSE @MaxCensus_Dis END
SET @MaxCensus = CASE WHEN @MaxCensus > @MaxCensus_Tx then @MaxCensus ELSE @MaxCensus_Tx END
SET @MaxCensus = CASE WHEN @MaxCensus > @MaxCensus_CurrentIP then @MaxCensus ELSE @MaxCensus_CurrentIP END

SET @Gap =	CASE	WHEN datepart(MINUTE,@MaxCensus) between 56 and 59		THEN datepart(MINUTE,@MaxCensus)
					WHEN datepart(MINUTE,@MaxCensus) between 1 and 5		THEN datepart(MINUTE,@MaxCensus) * -1
					ELSE 0
			END


SELECT @Gap
SELECT @MaxCensus_CurrentIP


SET @MaxCensus = DATEADD(MINUTE,@Gap,@MaxCensus)
SELECT @MaxCensus


UPDATE APC.Admissions SET Census = @MaxCensus WHERE Census = @MaxCensus_Adm
UPDATE APC.Discharges SET Census = @MaxCensus WHERE Census = @MaxCensus_Dis
UPDATE APC.Transfers SET Census = @MaxCensus WHERE Census = @MaxCensus_Tx
UPDATE APC.CurrentIP SET Census = @MaxCensus WHERE Census = @MaxCensus_CurrentIP

END
﻿CREATE PROCEDURE [APC].[LoadWardTransferAS]
AS
BEGIN

truncate table APC.TransfersAS


SET dateformat dmy

SELECT distinct

[MergeEncounterRecno]
      ,[ContextCode]
      ,[SourceUniqueID]
      ,[SourcePatientNo]
      ,[SourceSpellNo]
      ,[ProviderSpellNo]
      ,[StartDate]
      ,[EndDate]
      ,[StartTime]
      ,[EndTime]
      ,[SiteCode]
      ,[WardCode]
      ,[StartActivityCode]
      ,[EndActivityCode]
      ,[ContextID]
      ,[SiteID]
      ,[WardID]
,[SequenceNo1] = ROW_NUMBER() over(partition by [ProviderSpellNo] order by [ProviderSpellNo],[StartTime] )


INTO #Ward
FROM [WarehouseReportingMerged].[APC].[WardStay]

---------------------------------------------------------------------------------------------- 

SELECT DISTINCT 
WS1.[MergeEncounterRecno]

,WS1.[EndActivitycode]

,WS1.StartDate
,WS1.EndDate
,WS1.StartTime
,WS1.EndTime
,WS1.[SiteCode]
,WS1.[SourcePatientNo]

   ,WS1.[ProviderSpellNo]
,WS1.[StartActivitycode]
,WS1.[WardCode]
,WS1.[SequenceNo1]
,PreWard=WS2.[WardCode]
,PreSeq=WS2.[SequenceNo1]
,PreStartTime=WS2.StartTime
,PreEndTime=WS2.EndTime
,PreEndAct=WS2.[EndActivitycode]
,NationalAdmissionMethod
,AdmissionType



INTO #ward2
FROM #Ward WS1

  
  
	LEFT JOIN #Ward WS2
	on  WS1.[ProviderSpellNo]=WS2.[ProviderSpellNo]

	and  WS1.[SequenceNo1]=WS2.[SequenceNo1]+1

	LEFT JOIN
			   (SELECT DISTINCT
				apc.[SourcePatientNo],
				apc.[SourceSpellNo],
				[AdmissionMethodCode],
				[NationalAdmissionMethod],
				AdmissionType,
				[ProviderSpellNo]

				  FROM [WarehouseReportingMerged].[APC].[Encounter] APC
				left join  warehousesql.Warehousereportingmerged.apc.AdmissionMethod
				on [SourceAdmissionMethodCode]=[AdmissionMethodCode]
				)Adm

		on Adm.[ProviderSpellNo]= WS1.[ProviderSpellNo]
	



WHERE	WS1.StartActivitycode='TR'
and		WS1.StartDate between '01 Jun 15' and '19 Jun 15'





--DECLARE @NewCensus as datetime
--DECLARE @CurrentCensus as datetime

--SET @NewCensus = (Select top 1 max(endtime) FROM #Ward where endtime <= GetDate())
--SET @CurrentCensus = (Select top 1 max(census) FROM APC.Transfers)

--If @NewCensus > dateadd(minute,30,@CurrentCensus) and @CurrentCensus is not null
--BEGIN



--insert into APC.Transfers


insert into APC.TransfersAS

Select
PreWardBucket=Pre.WardBucket
,PostWardBucket=Post.WardBucket

,[MergeEncounterRecno]

,[EndActivitycode]

,StartDate
,EndDate
,StartTime
,EndTime
,[SiteCode]
,[SourcePatientNo]
   ,[ProviderSpellNo]

,[StartActivitycode]
,#Ward2.[WardCode]
,[SequenceNo1]
,PreWard
,PreSeq
,PreStartTime
,PreEndTime
,PreEndAct
,NationalAdmissionMethod
,AdmissionType



FROM #Ward2
	LEFT JOIN APC.LKPWard Pre
	ON Preward=Pre.[WardCode]

	LEFT JOIN APC.LKPWard Post 
	ON #ward2.wardcode=Post.[WardCode]



end

drop table #ward
drop table #ward2
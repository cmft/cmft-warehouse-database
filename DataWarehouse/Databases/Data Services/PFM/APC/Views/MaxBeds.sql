﻿CREATE VIEW [APC].[MaxBeds]
AS

SELECT WardBucket, MAX(Vol) as MaxBeds
FROM (

SELECT WardBucket, Census, COUNT(1) as Vol
FROM vwCurrentIP
GROUP BY WardBucket, Census) base

GROUP BY WardBucket
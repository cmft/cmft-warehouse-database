﻿
USE [master]
GO

/****** Object:  Database [PFM]    Script Date: 25/08/2015 15:38:18 ******/
CREATE DATABASE [PFM] ON  PRIMARY 
( NAME = N'PFM', FILENAME = N'F:\Data\PFM.mdf' , SIZE = 32768KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PFM_log', FILENAME = N'G:\Log\PFM_log.LDF' , SIZE = 5120KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [PFM] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PFM].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [PFM] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [PFM] SET ANSI_NULLS OFF
GO

ALTER DATABASE [PFM] SET ANSI_PADDING OFF
GO

ALTER DATABASE [PFM] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [PFM] SET ARITHABORT OFF
GO

ALTER DATABASE [PFM] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [PFM] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [PFM] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [PFM] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [PFM] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [PFM] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [PFM] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [PFM] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [PFM] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [PFM] SET  ENABLE_BROKER
GO

ALTER DATABASE [PFM] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [PFM] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [PFM] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [PFM] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [PFM] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [PFM] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [PFM] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [PFM] SET RECOVERY SIMPLE
GO

ALTER DATABASE [PFM] SET  MULTI_USER
GO

ALTER DATABASE [PFM] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [PFM] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'PFM', N'ON'
GO

USE [PFM]
GO

/****** Object:  Table [APC].[Admissions]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[CurrentIP]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[Discharges]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[LKPWard]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [APC].[Transfers]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [APC].[TransfersAS]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[dtproperties]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [dbo].[vwCurrentIP]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [APC].[MaxBeds]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwAdmissions]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwAdmissionsAS]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwDischarges]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwDischargesAS]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwOutstandingTasks]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwTransfers]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[vwTransfersAS]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[LoadAdmissions]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[LoadCurrentIP]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[LoadDischarges]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[LoadWardTransfer]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[LoadWardTransferAS]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[MasterLoad]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_adduserobject]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_droppropertiesbyid]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_dropuserobjectbyid]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_generateansiname]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_getobjwithprop]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_getobjwithprop_u]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_getpropertiesbyid]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_getpropertiesbyid_u]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_setpropertybyid]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_setpropertybyid_u]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_verstamp006]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[dt_verstamp007]    Script Date: 25/08/2015 15:38:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [PFM] SET  READ_WRITE
GO

﻿CREATE VIEW [dbo].[vwDischargesAS]
AS




SELECT 
WardBucket
,Division
,SourcePatientNo
,SourceSpellNo
,apc.PatientForename
,apc.PatientSurname
,AdmissionTime
,APC.[EndWardTypeCode]
,apc.CasenoteNumber
,[NationalAdmissionMethod]
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant = Consultant.Consultant
,DTOC=case when ProviderSpellNo=[CDT List].GlobalProviderSpellNo then 'YES' else null end
,DTOC_MFD=[MFD_Medically_Fit_Date]
,Discharges=count([EncounterRecno])

    
     
FROM warehouseSQL.warehousereportingmerged.[APC].[Encounter] APC
	LEFT JOIN APC.LKPWard 
	ON [EndWardTypeCode]=[WardCode]

	LEFT JOIN  warehousereportingmerged.APC.AdmissionMethod
	on Admissionmethodcode=sourceAdmissionmethodcode


	LEFT JOIN  warehousesql.warehouse.pas.Consultant
	on Consultant.ConsultantCode = apc.ConsultantCode

	LEFT JOIN warehousesql.warehouse.WH.Directorate 
	on Directorate.DirectorateCode = apc.StartDirectorateCode
  
	LEFT JOIN warehousesql.[Warehouse].[WH].[Division]
	on [Division].[DivisionCode]=[Directorate].[DivisionCode]

	LEFT JOIN [warehouse2\dev].[MRI_Discharge_Egress].[dbo].[CDT List]
	on ProviderSpellNo=[CDT List].GlobalProviderSpellNo

	LEFT JOIN warehousesql.[WarehouseReportingMerged].[WH].[Specialty]
	on SourceSpecialtyCode=Specialtycode



  
WHERE	dischargedate between '15 Jun 15' and '19 Jun 15'
  and	DischargeTime=EpisodeEndTime
  and	EndSiteCode = 'MRI'
  and	patientCategorycode not in ('DC','RD')
  and   DischargeTime <= GETDATE()  
  
GROUP BY
 WardBucket
,Division
,SourcePatientNo
,SourceSpellNo
,apc.PatientForename
,apc.PatientSurname
,AdmissionTime
,APC.[EndWardTypeCode]
,apc.CasenoteNumber
,[NationalAdmissionMethod]
,AdmissionType
,Directorate.Directorate
,NationalSpecialty
,Consultant.Consultant
,case when ProviderSpellNo=[CDT List].GlobalProviderSpellNo then 'YES' else null end
,[MFD_Medically_Fit_Date]
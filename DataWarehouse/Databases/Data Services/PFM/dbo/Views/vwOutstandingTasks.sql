﻿CREATE VIEW [dbo].[vwOutstandingTasks]
AS
SELECT     pat.EgressID, pat.GlobalProviderSpellNo, pat.CasenoteNumber, pat.PatientForename, pat.PatientSurname, pat.Date_Admitted_To_The_Trust, 
                      pat.MFD_Medically_Fit_Date, pat.PlannedDestinationID, pat.SITREP_Breach_Reason, pat.DateOfBirth, pat.Age, pat.Locality_PostCode, pat.Ward, 
                      pat.Date_Admitted_To_The_Ward, pat.Discharge_Delay_days, pat.Locality_GPPractice, pat.Outstanding_Tasks, pat.DischargeFlag, pat.MFFlag, pat.DischargeDate, 
                      pat.LA, pat.Postcode, task.EgressID AS Expr1, task.PatientTaskID, task.TaskID, task.StaffID, task.DateTime_Ordered, task.DateTime_Due, task.DateTime_Completed, 
                      task.Reason_for_delay, task.Revised_Date, task.Comments, lkp_task.TaskDesc, wardbucket.WardBucket
FROM         [WAREHOUSE2\DEV].MRI_Discharge_Egress.dbo.Delayed_Discharge_Info_4 AS pat LEFT OUTER JOIN
                      [WAREHOUSE2\DEV].MRI_Discharge_Egress.dbo.PatientTask AS task ON pat.EgressID = task.EgressID LEFT OUTER JOIN
                      [WAREHOUSE2\DEV].MRI_Discharge_Egress.dbo.LKP_Task AS lkp_task ON lkp_task.TaskID = task.TaskID LEFT OUTER JOIN
                      APC.LKPWard AS wardbucket ON wardbucket.WardCode = pat.Ward
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "pat"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 273
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "task"
            Begin Extent = 
               Top = 6
               Left = 311
               Bottom = 125
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "lkp_task"
            Begin Extent = 
               Top = 6
               Left = 540
               Bottom = 95
               Right = 700
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "wardbucket"
            Begin Extent = 
               Top = 6
               Left = 738
               Bottom = 95
               Right = 898
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOutstandingTasks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vwOutstandingTasks'
﻿CREATE TABLE [dbo].[tempTable](
	[Forename] [nvarchar](4000) NULL,
	[Surname] [nvarchar](4000) NULL,
	[Email] [nvarchar](4000) NULL,
	[name] [nvarchar](4000) NULL,
	[logonName] [nvarchar](4000) NULL,
	[altLogonName] [nvarchar](4000) NULL,
	[distinguishedName] [nvarchar](4000) NULL,
	[shortDomain] [varchar](4) NOT NULL,
	[fullLoginName] [nvarchar](4000) NULL
) ON [PRIMARY]
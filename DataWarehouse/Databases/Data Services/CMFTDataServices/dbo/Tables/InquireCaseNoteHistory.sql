﻿CREATE TABLE [dbo].[InquireCaseNoteHistory](
	[Borrower] [varchar](6) NULL,
	[CntActualReturnDateNeutral] [varchar](8) NULL,
	[CntExpectedReturnDateNeutral] [varchar](8) NULL,
	[CntLoanTimeHhmm] [varchar](4) NULL,
	[CntPatientFileLoanDatesequenceNoKey] [varchar](20) NOT NULL,
	[CntPcr4860DatetimeOfTransactionUpdateNeut] [varchar](12) NULL,
	[CntReturnTimeHhmm] [varchar](4) NULL,
	[Comments] [varchar](30) NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[PtCasenoteIdPhyskey] [varchar](14) NOT NULL,
	[ReasonForLoan] [varchar](30) NULL,
	[UserId] [varchar](3) NULL,
	[CorrelationID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
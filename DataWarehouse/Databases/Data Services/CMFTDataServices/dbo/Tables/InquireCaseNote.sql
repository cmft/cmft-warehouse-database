﻿CREATE TABLE [dbo].[InquireCaseNote](
	[Allocated] [varchar](255) NULL,
	[Comment] [varchar](30) NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[Location] [varchar](4) NULL,
	[PtCasenoteIdPhyskey] [varchar](14) NOT NULL,
	[PtCsNtLocDat] [varchar](255) NULL,
	[Status] [varchar](10) NULL,
	[Withdrawn] [varchar](255) NULL,
	[CorrelationID] [uniqueidentifier] NOT NULL DEFAULT ('FAE1A006-EE9C-4AEF-A249-0C1DADD2AC79')
) ON [PRIMARY]
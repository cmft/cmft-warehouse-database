﻿CREATE TABLE [dbo].[CaseNoteTrackingResults](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CaseNoteNumber] [varchar](14) NOT NULL,
	[InternalPatientNumber] [varchar](9) NOT NULL,
	[Location] [varchar](6) NOT NULL,
	[StartDateAtLocation] [varchar](8) NOT NULL,
	[CorrelationID] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
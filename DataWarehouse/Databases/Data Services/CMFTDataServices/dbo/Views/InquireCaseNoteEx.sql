﻿CREATE VIEW [dbo].[InquireCaseNoteEx]
AS
SELECT        dbo.InquireCaseNote.*, dbo.InquireLocations.Locationname
FROM            dbo.InquireCaseNote LEFT OUTER JOIN
                         dbo.InquireLocations ON dbo.InquireCaseNote.Location = dbo.InquireLocations.Location
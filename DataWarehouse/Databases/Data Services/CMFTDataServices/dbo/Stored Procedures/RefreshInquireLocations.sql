﻿CREATE PROC [dbo].[RefreshInquireLocations]
AS
truncate table dbo.InquireLocations;

INSERT dbo.InquireLocations(Location,LocationName,DeleteIndicatorFlag)
SELECT * FROM openquery(INQUIRE,
	'select left(Location,4), left(locationName,25), left(MfRecStsInd,1)
		from CASENLOC       
	')
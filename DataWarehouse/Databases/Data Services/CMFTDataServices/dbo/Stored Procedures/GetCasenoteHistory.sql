﻿CREATE PROC [dbo].[GetCasenoteHistory]
(
	 @InternalPatientNumber varchar(9)
	,@CasenoteNumber varchar(100)
	,@CorrelationID uniqueidentifier = NULL
)
AS
declare @followupQuery varchar(4000) = ' 
select Borrower
	,CntActualReturnDateNeutral
	,CntExpectedReturnDateNeutral
	,CntLoanTimeHhmm
	,CntPatientFileLoanDatesequenceNoKey
	,CntPcr4860DatetimeOfTransactionUpdateNeut
	,CntReturnTimeHhmm
	,Comments
	,InternalPatientNumber
	,PtCasenoteIdPhyskey
	,ReasonForLoan
	,UserId
	,''' + cast(@CorrelationID as varchar(36)) + ''' CorrelationID
 from openquery(Inquire,''select Borrower
		,CntActualReturnDateNeutral
		,CntExpectedReturnDateNeutral
		,CntLoanTimeHhmm
		,CntPatientFileLoanDatesequenceNoKey
		,CntPcr4860DatetimeOfTransactionUpdateNeut
		,CntReturnTimeHhmm
		,Comments
		,InternalPatientNumber
		,PtCasenoteIdPhyskey
		,ReasonForLoan
		,UserId
			from CASENOTELOAN 
			where InternalPatientNumber = ''''' + @InternalPatientNumber + ''''' 
			AND PtCasenoteIdPhyskey=''''' + @CaseNoteNumber + ''''''')';


	INSERT INTO [dbo].[InquireCaseNoteHistory]
           ([Borrower]
           ,[CntActualReturnDateNeutral]
           ,[CntExpectedReturnDateNeutral]
           ,[CntLoanTimeHhmm]
           ,[CntPatientFileLoanDatesequenceNoKey]
           ,[CntPcr4860DatetimeOfTransactionUpdateNeut]
           ,[CntReturnTimeHhmm]
           ,[Comments]
           ,[InternalPatientNumber]
           ,[PtCasenoteIdPhyskey]
           ,[ReasonForLoan]
           ,[UserId], CorrelationID)
		exec (@followupQuery)
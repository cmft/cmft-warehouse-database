﻿create procedure [dbo].[BasicDemographics_ByCaseNote](@CasenoteNumber varchar(14))
as
select top 1 p.Forenames, p.Surname, p.InternalDateOfBirth,
p.InternalPatientNumber, cn.CasenoteNumber, p.DistrictNumber
from warehouse.pas.Casenote cn
inner join warehouse.pas.PatientBase p
  on p.InternalPatientNumber = cn.SourcePatientNo 
where CasenoteNumber = @casenoteNumber;
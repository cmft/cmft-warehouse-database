﻿CREATE PROC [dbo].[GetCasenote] (
	 @InternalPatientNumber varchar(9) = null out
	,@CasenoteNumber varchar(100)
	,@CorrelationID uniqueidentifier = NULL out
	,@debug tinyint = 0
)
as
IF @CorrelationID IS NULL
	SET @CorrelationID = NEWID();

if @InternalPatientNumber is null
  exec CasenoteTracking.GetInternalPatientNumber @CasenoteNumber, @InternalPatientNumber out;

declare @sql1 varchar(max)

select
	@sql1 =
'
select
	 Allocated
	,Comment
	,InternalPatientNumber
	,Location
	,PtCasenoteIdPhyskey
	,PtCsNtLocDat
	,Status
	,Withdrawn
	,'''+ CAST(@CorrelationID as varchar(36))+''' CorrelationID
from
	openquery(INQUIRE, ''

SELECT 
	 left(Allocated , 12) Allocated
	,Comment
	,InternalPatientNumber
	,Location
	,PtCasenoteIdPhyskey
	,left(PtCsNtLocDat , 12) PtCsNtLocDat
	,Status
	,left(Withdrawn , 20) Withdrawn
FROM
	CASENOTEDETS Casenote
where
	InternalPatientNumber = ''''{InternalPatientNumber}''''
and	PtCasenoteIdPhyskey = ''''{CasenoteNumber}''''
'')
'

select
	@sql1 = replace(@sql1 , '{InternalPatientNumber}' , @InternalPatientNumber)
select
	@sql1 = replace(@sql1 , '{CasenoteNumber}' , @CasenoteNumber)


if @debug = 1 
	begin
		print (@sql1)
	end
else
	begin
		INSERT INTO [dbo].[InquireCaseNote]
           ([Allocated]
           ,[Comment]
           ,[InternalPatientNumber]
           ,[Location]
           ,[PtCasenoteIdPhyskey]
           ,[PtCsNtLocDat]
           ,[Status]
           ,[Withdrawn]
           ,[CorrelationID])
		exec(@sql1);
	end
﻿CREATE PROC [CasenoteTracking].[ClearQueryResults](@CorrelationID uniqueidentifier)
AS
DELETE FROM dbo.InquireResults
WHERE CorrelationID = @CorrelationID;
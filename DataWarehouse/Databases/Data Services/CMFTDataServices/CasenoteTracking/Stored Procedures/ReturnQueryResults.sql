﻿CREATE PROC[CasenoteTracking].[ReturnQueryResults](@CorrelationID uniqueidentifier)
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT * FROM dbo.InquireResults 
WHERE CorrelationID = @CorrelationID;
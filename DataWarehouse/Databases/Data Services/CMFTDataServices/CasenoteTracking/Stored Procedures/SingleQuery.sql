﻿CREATE PROC[CasenoteTracking].[SingleQuery]
(
	@CaseNoteNumber varchar(14), 
	@CorrelationID uniqueIdentifier = NULL)
AS

SET NOCOUNT ON;

IF @CorrelationID IS NULL SET @CorrelationID = NEWID();

exec CasenoteTracking.GetCaseNoteInfo @CaseNoteNumber, @CorrelationID
﻿/****** Object:  Schema [CasenoteTracking]    Script Date: 10/31/2011 08:17:56 ******/

CREATE PROC [CasenoteTracking].[GetInternalPatientNumber](
@CaseNoteNumber varchar(20), 
@patientNumber varchar(9) OUTPUT
)
AS

DECLARE @initialQuery nvarchar(4000) = '
select InternalPatientNumber 
	from openquery(INQUIRE, ''select top 1 InternalPatientNumber from CASENOTELINK where ExternalNumberKey=''''' + @CaseNoteNumber + ''''' AND EntityTypeKey = 1'')';

declare @ipnTable TABLE(ipn varchar(9) NOT NULL);
insert @ipnTable
exec (@initialQuery);

select TOP 1 @patientNumber = ipn FROM @ipnTable;
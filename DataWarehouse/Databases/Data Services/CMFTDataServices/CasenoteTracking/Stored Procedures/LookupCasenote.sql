﻿-- =============================================
-- Author:		Dan Forster
-- Create date: 13 September 2011
-- Description:	Case Note Tracking Location Lookup
-- =============================================
CREATE PROC [CasenoteTracking].[LookupCasenote] 
	@CaseNoteNumber varchar(20),
	@InternalPatientNumber  varchar(9) OUTPUT,
	@Location varchar(6) OUTPUT,
	@StartDateAtLocation varchar(8) OUTPUT 
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @CorrelationID uniqueidentifier = newid();
    
EXEC CasenoteTracking.GetInternalPatientNumber @CaseNoteNumber, @InternalPatientNumber OUTPUT;

EXEC [dbo].[GetCasenote] @InternalPatientNumber, @CaseNoteNumber, @CorrelationID;
EXEC [dbo].[GetCasenoteHistory] @InternalPatientNumber, @CaseNoteNumber, @CorrelationID;

SELECT TOP 1 @Location = h.Borrower, @StartDateAtLocation = left(ISNULL(h.CntPcr4860DatetimeOfTransactionUpdateNeut,' '),8) 	
		FROM dbo.InquireCaseNoteHistory h 
		WHERE CorrelationID = @CorrelationID 
		AND h.CntActualReturnDateNeutral IS NULL
IF (@Location IS NULL) BEGIN
	SELECT @Location = cn.Location,
		   @StartDateAtLocation = left(cn.PtCsNtLocDat,8)
	FROM dbo.InquireCaseNote cn
		WHERE CorrelationID = @CorrelationID; 
END
	
END
﻿CREATE PROC [CasenoteTracking].[GetCaseNoteInfo]
(
	@CaseNoteNumber varchar(20),
	@CorrelationID uniqueidentifier = NULL
)
AS  
PRINT '[CasenoteTracking].[GetCaseNoteInfo]';

DECLARE @patientNumber varchar(9)
IF @CorrelationID IS NULL
	SET @CorrelationID = NEWID();
	
exec CasenoteTracking.GetInternalPatientNumber @CaseNoteNumber, @patientNumber OUTPUT;

exec [dbo].[GetCasenote] @patientNumber, @CaseNoteNumber, @CorrelationID;
exec [dbo].[GetCasenoteHistory] @patientNumber, @CaseNoteNumber, @CorrelationID;

select * from dbo.InquireCaseNote where CorrelationID = @CorrelationID;
select * from dbo.InquireCaseNoteHistory where CorrelationID = @CorrelationID
order by cast(dbo.InquireCaseNoteHistory.CntPatientFileLoanDatesequenceNoKey as bigint) DESC;
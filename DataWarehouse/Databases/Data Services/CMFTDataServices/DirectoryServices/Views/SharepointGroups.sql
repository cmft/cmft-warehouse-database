﻿CREATE VIEW [DirectoryServices].[SharepointGroups]
AS
SELECT  CN, distinguishedName DN, ADSPath
FROM OPENQUERY( ADSI, 
     'SELECT CN, distinguishedName, ADSPath 
      FROM ''LDAP:// OU=Warehouse Sharepoint Groups,OU=CMMC - Groups,OU=CMMC - OU,DC=xcmmc,DC=nhs,DC=uk''
      ')
      WHERE CN IS NOT NULL
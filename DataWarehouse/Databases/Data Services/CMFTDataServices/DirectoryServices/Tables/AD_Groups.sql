﻿CREATE TABLE [DirectoryServices].[AD_Groups](
	[CN] [varchar](128) NULL,
	[DN] [varchar](1024) NULL,
	[Email] [varchar](128) NULL,
	[ADSPath] [varchar](1024) NULL
) ON [PRIMARY]
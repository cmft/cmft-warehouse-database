﻿CREATE TABLE [DirectoryServices].[AD_Group_Members](
	[DistributionGroup] [varchar](128) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[EmailAddress] [varchar](128) NULL,
	[DomainLogin] [varchar](255) NULL
) ON [PRIMARY]
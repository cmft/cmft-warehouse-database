﻿CREATE PROC [DirectoryServices].[FindUser]  --'chaist','suz'
(
	@sn varchar(128) = 'forster', @fn varchar(128) = ''
)
AS

--declare @sn varchar(128) = 'chaisty', @fn varchar(128) = 'su'
declare @sql nvarchar(max);
select @sn = replace(@sn,'''',''''''), @fn = replace(@fn,'''','''''');

set @sql = 	'SELECT givenName Forename, sn Surname, mail Email, name, sAMAccountName logonName, userPrincipalName altLogonName, distinguishedName
, case right (distinguishedName, 28) when ''DC=MHC,DC=xCMMC,DC=nhs,DC=uk'' then ''MHC''
else ''CMMC'' end as shortDomain, case right (distinguishedName, 28) when ''DC=MHC,DC=xCMMC,DC=nhs,DC=uk'' then ''MHC'' else ''CMMC'' end + ''\'' + sAMAccountName AS fullLoginName
FROM OpenQuery
	(ADSI, 
		''<GC://MHC.xcmmc.nhs.uk>;(&(objectClass=User)(!objectClass=Computer)(sAMAccountName='+@fn+'*.' +@sn+'*));sAMAccountName,givenName, sn, mail, name, userPrincipalName, distinguishedName;subtree'')
ORDER BY Surname, Forename, Email'


exec sp_executesql @statement = @sql;
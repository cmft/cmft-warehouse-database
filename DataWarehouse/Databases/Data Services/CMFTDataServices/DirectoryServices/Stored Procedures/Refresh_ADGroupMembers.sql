﻿-- Batch submitted through debugger: SQLQuery15.sql|0|0|C:\Documents and Settings\dan.forster\Local Settings\Temp\~vsDC6.sql
CREATE PROC [DirectoryServices].[Refresh_ADGroupMembers]
AS
truncate table DirectoryServices.AD_Group_Members;
exec DirectoryServices.Refresh_WarehouseSharepointGroups;

DECLARE @sql VARCHAR(1024);

DECLARE @CN VARCHAR(128);
DECLARE @DN VARCHAR(1024);

SELECT TOP 1 @CN = CN, @DN = DN FROM DirectoryServices.AD_Groups
ORDER BY DN ASC;
WHILE EXISTS(SELECT DN FROM DirectoryServices.AD_Groups WHERE DN > @DN)

BEGIN
   print @DN

   SET @sql = 'INSERT INTO DirectoryServices.AD_Group_Members (DistributionGroup, Firstname, LastName, EmailAddress, DomainLogin) 
SELECT '''+@CN+''', givenName, sn, mail,''MHC\'' + sAMAccountName FROM OpenQuery
(ADSI, ''<LDAP://MHC.xcmmc.nhs.uk>;(&(objectClass=User)(memberOf='+@DN+'));givenName, sn, mail, sAMAccountName;subtree'')'; 
   
   --print @sql;
BEGIN TRY   
   EXEC(@sql)
END TRY
BEGIN CATCH
  Print 'updating MHC LDAP Query failed';
END CATCH

   SET @sql = 'INSERT INTO DirectoryServices.AD_Group_Members (DistributionGroup, Firstname, LastName, EmailAddress, DomainLogin) 
SELECT '''+@CN+''', givenName, sn, mail,''CMMC\'' + sAMAccountName FROM OpenQuery
(ADSI, ''<LDAP://xcmmc.nhs.uk>;(&(objectClass=User)(memberOf='+@DN+'));givenName, sn, mail, sAMAccountName;subtree'')'; 
   
   --print @sql;
   
   EXEC(@sql)

   SET @sql = 'INSERT INTO DirectoryServices.AD_Group_Members (DistributionGroup, Firstname, LastName, EmailAddress, DomainLogin) 
SELECT '''+@CN+''', givenName, sn, mail,''critical-care\'' + sAMAccountName FROM OpenQuery
(ADSI, ''<LDAP://critical-care.xcmmc.nhs.uk>;(&(objectClass=User)(memberOf='+@DN+'));givenName, sn, mail, sAMAccountName;subtree'')'; 

  
   --print @sql;
   
   EXEC(@sql)


   SELECT TOP 1 @CN = CN, @DN = DN FROM DirectoryServices.AD_Groups WHERE DN > @DN ORDER BY DN ASC;
END
﻿CREATE PROCEDURE [DirectoryServices].[WarehouseSharepointGroups]
AS
SELECT  CN, distinguishedName DN, mail Email, ADSPath
FROM OPENQUERY( ADSI, 
     'SELECT CN, distinguishedName, mail, ADSPath 
      FROM ''LDAP:// OU=Warehouse Sharepoint Groups,OU=CMMC - Groups,OU=CMMC - OU,DC=xcmmc,DC=nhs,DC=uk''
      ')
      WHERE CN IS NOT NULL and CN not like 'Dis -%'
      ORDER BY CN ASC;
﻿create proc [DirectoryServices].[UsersForAnyGroup]
(
	@groupName varchar(128) = 'Sec - Warehouse SQL DBAs'
)
AS
--declare @groupName varchar(128) =
DECLARE @DN varchar(1024);

SELECT @DN = sg.DN 
FROM DirectoryServices.SharepointGroups sg
WHERE sg.CN = @groupName;

declare @sql nvarchar(max) = N'
SELECT  top 1 distinguishedName
FROM OPENQUERY( ADSI, 
     ''SELECT CN, distinguishedName, mail, ADSPath 
      FROM ''''LDAP://DC=xcmmc,DC=nhs,DC=uk'''' where CN = ''''' + @groupName + '''''
 '')
      WHERE CN IS NOT NULL';

print @sql

declare @tbl table (dn varchar(1024) not null);

insert @tbl(dn)
exec (@sql);

select @dn = dn from @tbl;

set @sql = 'SELECT givenName Forename, sn Surname, mail Email, name, sAMAccountName logonName, userPrincipalName altLogonName 
FROM OpenQuery
	(ADSI, 
		''<LDAP://xcmmc.nhs.uk>;(&(objectClass=User)(memberOf='+@DN+'));sAMAccountName,givenName, sn, mail, name, userPrincipalName;subtree'')
		ORDER BY Surname, Forename, Email ';

exec sp_executesql @statement = @sql;
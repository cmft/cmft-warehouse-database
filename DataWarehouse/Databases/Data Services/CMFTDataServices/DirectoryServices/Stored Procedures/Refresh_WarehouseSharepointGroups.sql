﻿CREATE PROCEDURE [DirectoryServices].[Refresh_WarehouseSharepointGroups]
AS
truncate table DirectoryServices.AD_Groups;

INSERT DirectoryServices.AD_Groups(CN, DN, Email, ADSPath)
SELECT  CN, distinguishedName DN, mail Email, ADSPath
FROM OPENQUERY( ADSI, 
     'SELECT CN, distinguishedName, mail, ADSPath 
      FROM ''LDAP:// OU=Warehouse Sharepoint Groups,OU=CMMC - Groups,OU=CMMC - OU,DC=xcmmc,DC=nhs,DC=uk''
      ')
      WHERE CN IS NOT NULL
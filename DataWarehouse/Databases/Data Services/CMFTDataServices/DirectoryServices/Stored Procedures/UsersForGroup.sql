﻿CREATE PROC [DirectoryServices].[UsersForGroup]
(
	@groupName varchar(128) = 'Sec - SP Community'
)
AS

DECLARE @DN varchar(1024);

SELECT @DN = sg.DN 
FROM DirectoryServices.SharepointGroups sg
WHERE sg.CN = @groupName;
declare @sql nvarchar(max);

set @sql = 'SELECT givenName Forename, sn Surname, mail Email, name, sAMAccountName logonName, userPrincipalName altLogonName 
FROM OpenQuery
	(ADSI, 
		''<LDAP://xcmmc.nhs.uk>;(&(objectClass=User)(memberOf='+@DN+'));sAMAccountName,givenName, sn, mail, name, userPrincipalName;subtree'')
		ORDER BY Surname, Forename, Email ';

exec sp_executesql @statement = @sql;
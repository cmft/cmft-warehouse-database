﻿CREATE PROC [Casenotes].[GetLocations](
@Casenotes xml = NULL
)
AS
BEGIN

IF (@Casenotes IS NULL) SET @Casenotes = 
	'<casenotes>
		<casenote>M03/22395</casenote>
		<casenote>S91/4595</casenote>
		<casenote>E88/01350</casenote>
		<casenote>M02/16528</casenote>
		<casenote>M07/02302</casenote>
		<casenote>H12823D</casenote>
		<casenote>M10/053866</casenote>
		<casenote>M74/14926</casenote>
		<casenote>M09/065227</casenote>
		<casenote>E98/03076</casenote>
		<casenote>E93/08664</casenote>
		<casenote>E05/13351</casenote>
		<casenote>M00/03592</casenote>
		<casenote>M00/15296</casenote>
		<casenote>M60/20334</casenote>
		<casenote>M92/21075</casenote>
	</casenotes>';
--select distinct top 1000 '<casenote>'+ CaseNoteNo +'</casenote>' from InfocomStaging.dbo.Admission


DECLARE @CasenotesTable TABLE(idx int identity(1,1) NOT NULL PRIMARY KEY, CaseNoteNumber varchar(20) NOT NULL);

INSERT @CasenotesTable(CaseNoteNumber)
SELECT cast(T.c.query('./text()') AS varchar) 
FROM   @Casenotes.nodes('//casenotes/casenote') T(c);

declare @currentIdx int = 1;
declare @maxIdx int;
select @maxIdx = max(idx) from @CasenotesTable;

DECLARE @currentCasenote varchar(20);

DECLARE @InternalPatientNumber varchar(9)
DECLARE @Location varchar(6)
DECLARE @StartDateAtLocation varchar(8)
DECLARE @CaseNoteLocationLookupTable TABLE(
	CaseNoteNumber varchar(20),
	InternalPatientNumber varchar(9),
	Location varchar(6),
	StartDateAtLocation varchar(8)
	);
	

WHILE (@currentIdx<=@maxIdx)
	BEGIN

		SELECT @currentCasenote = CaseNoteNumber
		FROM @CasenotesTable
		WHERE idx = @currentIdx;

	  EXECUTE [CasenoteTracking].[LookupCasenote] 
		   @currentCasenote
		  ,@InternalPatientNumber OUTPUT
		  ,@Location OUTPUT
		  ,@StartDateAtLocation OUTPUT
	  
	  INSERT @CaseNoteLocationLookupTable
	  VALUES( @currentCasenote
			,@InternalPatientNumber 
			,@Location 
			,@StartDateAtLocation); 
	 
		SET @currentIdx += 1;

	END;

--Do JOINS to get other details...
SELECT cn.CaseNoteNumber, cn.StartDateAtLocation, p.Forenames, p.Surname, ISNULL(loc.LocationName, 
		CASE 
		 WHEN (bb.BorrowerTitleAndInitials IS NULL) THEN bb.BorrowerSurname 
		 ELSE  bb.BorrowerTitleAndInitials + ' ' + bb.BorrowerSurname 
		 END ) Location
FROM @CaseNoteLocationLookupTable cn
  LEFT OUTER JOIN Warehouse.PAS.PatientBase p
    ON p.InternalNo = cn.InternalPatientNumber
  LEFT OUTER JOIN Warehouse.PAS.CasenoteLocationBase loc
    ON cn.Location = loc.Location
  LEFT OUTER JOIN Warehouse.PAS.BorrowerBase bb
    ON cn.Location = bb.BorrowerCode;
    
END;
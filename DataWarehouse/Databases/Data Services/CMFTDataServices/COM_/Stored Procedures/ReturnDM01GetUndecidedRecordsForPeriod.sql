﻿CREATE PROC [COM].[ReturnDM01GetUndecidedRecordsForPeriod] 
(
	@ReturnPeriod int = NULL
)
AS
SELECT WL.SourceUniqueNo
	,WL.ItemDate
	--,ItemTime
	,WL.ItemSpecialty
	,WL.NHSNumber
	,WL.ReferredByProfessionalCarer
	,WL.ScheduleType
	,WL.StaffTeam
	--,SeenByProfessionalCarer
	--,PatientAge
	,WL.Clinic
	--,ReferralSource
	,WL.AppointmentOutcome
	,WL.ReferralClosedDate
	,WL.FirstContact
	,WL.AppointmentSpecialty
	,WL.RecentActivity
	--,StatusTypeCode
	,WL.StatusType
	--,GPPracticeCode
	--,GPPractice
	--,CommissionerCode
	--,Commissioner
	,WLE.IsExcluded
	,WLE.ExclusionReason
	,WLE.ExclusionComments
FROM Warehouse.COM.ReturnDM01WaitingList WL
LEFT OUTER JOIN Warehouse.COM.ReturnDM01WaitingListExclusions WLE
ON WL.SourceUniqueNo = WLE.SourceUniqueNo
WHERE WLE.SourceUniqueNo IS NULL
  AND (ReturnPeriod = @ReturnPeriod OR @ReturnPeriod IS NULL)
  AND (LoadDate = (select MAX(LoadDate) from Warehouse.COM.ReturnDM01WaitingList DWL
										WHERE WL.ReturnPeriod = DWL.ReturnPeriod
										AND WL.SourceUniqueNo = DWL.SourceUniqueNo))

--FOR XML AUTO;
﻿CREATE PROC [COM].[ReturnDM01GetReturnPeriods] 
AS
SELECT ReturnPeriod, LEFT(vReturnPeriod,4) + '-' + RIGHT(vReturnPeriod,2) DisplayPeriod
FROM
	(
	SELECT DISTINCT  ReturnPeriod, CAST( ReturnPeriod as varchar) vReturnPeriod
	FROM Warehouse.COM.ReturnDM01WaitingList
	) IQ
ORDER BY ReturnPeriod DESC
﻿CREATE PROCEDURE [COM].[GetDM01WaitingListAdjustments]

AS

BEGIN

SELECT
	 WL.ReturnPeriod
	,WL.LoadDate
	--,[Return]
	--,ReturnItem
	--,InterfaceCode
	 ,WL.SourceUniqueNo
	,WL.ItemDate
	--,ItemTime
	,WL.ItemSpecialty
	,WL.NHSNumber
	,WL.ReferredByProfessionalCarer
	,WL.ScheduleType
	,WL.StaffTeam
	--,SeenByProfessionalCarer
	--,PatientAge
	,WL.Clinic
	--,ReferralSource
	,WL.AppointmentOutcome
	,WL.ReferralClosedDate
	,WL.FirstContact
	,WL.AppointmentSpecialty
	,WL.RecentActivity
	--,StatusTypeCode
	,WL.StatusType
	--,GPPracticeCode
	--,GPPractice
	--,CommissionerCode
	--,Commissioner
	,WL.IsExcluded
	,WL.ExclusionReason
	,WL.ExclusionComments

FROM  
	Warehouse.COM.ReturnDM01WaitingList WL
INNER JOIN (
			SELECT 
				 MAX(LoadDate)AS LoadDate
				,MAX(ReturnPeriod) AS ReturnPeriod
			FROM
				Warehouse.COM.ReturnDM01WaitingList
			)MaxWLDate
	ON WL.LoadDate = MaxWLDate.loaddate
	AND WL.ReturnPeriod = MaxWLDate.ReturnPeriod

WHERE WL.IsExcluded IS NULL;

END;
﻿CREATE FUNCTION [String].[Split](@stringToSplit [nvarchar](max), @splitChar [nvarchar](1))
RETURNS  TABLE (
	[string] [nvarchar](max) NULL
) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[TVF_Split].[Split]
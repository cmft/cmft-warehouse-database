﻿CREATE FUNCTION [String].[Contains](@sourceString [nvarchar](max), @stringToFind [nvarchar](4000))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[Contains]
﻿CREATE FUNCTION [String].[Replace](@sourceString [nvarchar](max), @searchString [nvarchar](4000), @replaceString [nvarchar](4000))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[Replace]
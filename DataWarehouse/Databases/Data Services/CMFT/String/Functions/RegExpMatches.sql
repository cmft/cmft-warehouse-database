﻿CREATE FUNCTION [String].[RegExpMatches](@text [nvarchar](max), @pattern [nvarchar](255))
RETURNS  TABLE (
	[Index] [int] NULL,
	[Length] [int] NULL,
	[Value] [nvarchar](255) NULL
) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlRegularExpressions].[SqlRegularExpressions].[GetMatches]
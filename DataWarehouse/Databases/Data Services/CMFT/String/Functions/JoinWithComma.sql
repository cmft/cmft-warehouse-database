﻿CREATE AGGREGATE [String].[JoinWithComma]
(@Value [nvarchar](max))
RETURNS[nvarchar](max)
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[JoinWithComma]
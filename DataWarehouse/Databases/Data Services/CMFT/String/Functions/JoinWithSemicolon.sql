﻿CREATE AGGREGATE [String].[JoinWithSemicolon]
(@Value [nvarchar](max))
RETURNS[nvarchar](max)
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[JoinWithSemicolon]
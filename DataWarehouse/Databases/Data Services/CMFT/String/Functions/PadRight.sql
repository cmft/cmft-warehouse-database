﻿CREATE FUNCTION [String].[PadRight](@sourceString [nvarchar](max), @totalWidth [int], @padWidth [nchar](1))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[PadRight]
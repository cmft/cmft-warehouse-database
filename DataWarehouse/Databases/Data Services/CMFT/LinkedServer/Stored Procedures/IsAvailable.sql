﻿CREATE procedure [LinkedServer].[IsAvailable] (@serverName sysname, @available bit output)
as
begin
set @available = 1;
BEGIN TRY
    EXEC sp_testlinkedserver @serverName; 
END TRY 
BEGIN CATCH
    set @available = 0;
END CATCH

end
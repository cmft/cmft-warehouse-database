﻿create proc [Version].[Set]
  (
	@major tinyint,
	@minor tinyint,
	@revision tinyint = 0,
	@build tinyint = 0
  )
  as
  begin
  set nocount on;
  
  truncate table [Version].[Current]
  
  insert [Version].[Current](major,minor,revision,build,rolledOutDate)
  values(@major,@minor,@revision,@build,sysutcdatetime())
  
  end;
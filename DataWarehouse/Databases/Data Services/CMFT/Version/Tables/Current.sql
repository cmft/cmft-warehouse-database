﻿CREATE TABLE [Version].[Current](
	[Major] [tinyint] NOT NULL DEFAULT ((0)),
	[Minor] [tinyint] NOT NULL DEFAULT ((0)),
	[Revision] [tinyint] NOT NULL DEFAULT ((0)),
	[Build] [tinyint] NOT NULL DEFAULT ((0)),
	[Version]  AS ((((((CONVERT([varchar](20),[major],(0))+'.')+CONVERT([varchar](20),[minor],(0)))+'.')+CONVERT([varchar](20),[revision],(0)))+'.')+CONVERT([varchar](20),[build],(0))),
	[rolledOutDate] [datetime2](7) NOT NULL
) ON [PRIMARY]
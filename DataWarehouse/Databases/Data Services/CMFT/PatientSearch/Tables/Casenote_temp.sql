﻿CREATE TABLE [PatientSearch].[Casenote_temp](
	[_searchID] [uniqueidentifier] NOT NULL,
	[SourcePatientNo] [int] NOT NULL,
	[Casenote] [varchar](14) NOT NULL,
	[Withdrawn] [date] NULL
) ON [PRIMARY]
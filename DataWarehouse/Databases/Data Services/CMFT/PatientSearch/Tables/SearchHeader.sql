﻿CREATE TABLE [PatientSearch].[SearchHeader](
	[_searchID] [uniqueidentifier] NOT NULL,
	[datasourceName] [varchar](64) NOT NULL,
	[datasourceCurrencyType] [varchar](32) NOT NULL,
	[datasourceCurrencyDateTime] [datetimeoffset](7) NULL,
	[datasourceCurrencyDescription] [varchar](64) NOT NULL
) ON [PRIMARY]
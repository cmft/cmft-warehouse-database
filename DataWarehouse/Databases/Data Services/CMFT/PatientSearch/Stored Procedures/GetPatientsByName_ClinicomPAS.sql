﻿CREATE proc [PatientSearch].[GetPatientsByName_ClinicomPAS](
   @searchID uniqueidentifier
,  @error varchar(max) out
,  @lastName varchar(35) = null
,  @firstName varchar(35) = null
,  @dateOfBirth date = null
,  @gender char(1) = null
,  @INQUIRELinkedServerName varchar(32) = 'INQUIRE'
,  @debug bit = 0)
as
begin
	set nocount on;

	set @lastname += '%';

	set @firstname += '%';

	if @debug = 1 begin
	  select @lastName lastName,@firstName firstName,@dateOfBirth dateOfBirth,@gender gender
	end;

	insert PatientSearch.SearchHeader(_searchID,dataSourceName, dataSourceCurrencyType,datasourceCurrencyDateTime, datasourceCurrencyDescription)
	values (@searchID,'Clinicom PAS - ' + @INQUIRELinkedServerName, 'R' , null 
	, 'Realtime');

	declare @pasQuery varchar(max)=
	'select '''+ convert(varchar(50),@searchID) +''' searchID, InternalPatientNumber SourcePatientNo, NHSNumber, NHSNumberStatus NHSNumberStatusId, DistrictNumber DistrictNo
	, PtDoB DateOfBirth, ptDateOfDeath, 
	PtDeathIndInt DeathIndicator 
				, Surname 
				, Forenames 
				, Title 
				, PreviousSurname1 
				, PtAddrLine1
				, PtAddrLine2
				, PtAddrLine3
				, PtAddrLine4
				, PtAddrPostCode Postcode 
				, PtHomePhone 
				, PtWorkPhone 
				, PtMobilePhone 
				, Sex SexCode 
				, MaritalStatus MaritalStatusCode
				, NoKName NextOfKin 
				, NokAddrLine1
				, NokAddrLine2
				, NokAddrLine3
				, NokAddrLine4
				, NokPostcode
				, NokRelationship
				, NoKHomePhone
				, NoKWorkPhone
				, EthnicType EthnicOriginCode 
				, Religion ReligionCode  
				, GpCode RegisteredGpCode  
				from  openquery(' + @INQUIRELinkedServerName + ',''select InternalPatientNumber 
				, NHSNumber, NHSNumberStatus , DistrictNumber 
				, PtDoB , ptDateOfDeath 
				, PtDeathIndInt 
				, Surname 
				, Forenames 
				, Title 
				, PreviousSurname1 
				, PtAddrLine1
				, PtAddrLine2
				, PtAddrLine3
				, PtAddrLine4
				, PtAddrPostCode 
				, PtHomePhone 
				, PtWorkPhone 
				, PtMobilePhone 
				, Sex 
				, MaritalStatus 
				, NoKName
				, NokAddrLine1
				, NokAddrLine2
				, NokAddrLine3
				, NokAddrLine4
				, NokPostcode
				, NokRelationship
				, NoKHomePhone
				, NoKWorkPhone
				, EthnicType 
				, Religion 
				, GpCode 
				from PATDATA 
				where Surname like ''''' + upper(@lastname) + ''''' and 
				Forenames like ''''' + upper(@firstname) + ''''' '');';

				if (@debug =1) begin
					PRINT @pasQuery;
				end;

	insert PatientSearch.Patient_temp 
	EXEC (@pasQuery)

	delete from PatientSearch.Patient_temp with (rowlock)
	where _searchid = @searchID
		and sourcepatientno not in (
			select sourcepatientno from PatientSearch.Patient_temp where _searchID = @searchID
				and (convert(date,DateOfBirth,103) = @dateOfBirth or @DateOfBirth is null)
				and (SexCode = @gender or @gender is null)
			);

	declare @ipns nvarchar(max) 

	select @ipns = cmft.string.JoinWithComma(sourcepatientno) from PatientSearch.Patient_temp
	where _searchID = @searchID; 

	if rtrim(@ipns)='' return;

	declare @cnSQL nvarchar(max) = N'select  '''+ convert(varchar(50),@searchID) +''' searchID, InternalPatientNumber,PtCasenoteIdPhyskey,convert(date,WithDrawn,103) from OpenQuery(INQUIREPROD, ''select InternalPatientNumber,PtCasenoteIdPhyskey, WithDrawn from CASENOTEDETS WHERE InternalPatientNumber in (' + @ipns +') '')';

	insert PatientSearch.Casenote_temp(_searchID,SourcePatientNo,Casenote,Withdrawn)
	exec (@cnSQL);

end;
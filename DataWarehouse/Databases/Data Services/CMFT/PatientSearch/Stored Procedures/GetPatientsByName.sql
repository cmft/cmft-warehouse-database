﻿create proc [PatientSearch].[GetPatientsByName] (@lastName varchar(35)
,  @firstName varchar(35) = null
,  @dateOfBirth date = null
,  @gender char(1) = null
,  @resultXML xml out
,  @debug bit = 0)
as
begin
declare @searchID uniqueidentifier = newid();
declare @error varchar(max);

declare @avail bit = 0;
declare @LinkServ sysname=N'INQUIREPROD';


exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;

if @avail=0 begin
  set @LinkServ = 'INQUIRE'
  exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;
end;

if @avail=0 begin
  set @LinkServ = 'WAREHOUSESQL'
end;

if (@debug = 1) begin
	select @LinkServ DataSource;
end;

if @LinkServ='WAREHOUSESQL' begin

	exec PatientSearch.GetPatientsByName_Offline @searchID, @error out, @lastName, @firstName, @dateOfBirth, @gender, @debug

end
else
begin
	exec PatientSearch.GetPatientsByName_ClinicomPAS @searchID, @error out, @lastName, @firstName, @dateOfBirth, @gender,@LinkServ, @debug
end;

exec PatientSearch.FormatResults @searchID,@error,@resultXML out,@debug;

end;
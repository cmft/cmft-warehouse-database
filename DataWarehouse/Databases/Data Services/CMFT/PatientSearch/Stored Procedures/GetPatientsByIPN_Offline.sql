﻿--Patient Search - Name 

create proc [PatientSearch].[GetPatientsByIPN_Offline](
   @searchID uniqueidentifier
,  @error varchar(max) out
,  @ipn int
,  @debug bit = 0)
as
begin
set nocount on;

DECLARE @LASTUPDATED DATETIME2;

select TOP 1 @LASTUPDATED = LASTSTARTED 
from WAREHOUSESQL.cmftetl.ETL.EventTimes_view 
WHERE ID = 6116;

insert PatientSearch.SearchHeader(_searchID,dataSourceName, dataSourceCurrencyType,datasourceCurrencyDateTime, datasourceCurrencyDescription)
values (@searchID,'Data Warehouse', 'T' , convert(datetimeoffset,@lastupdated) 
, 'Current To: ' + CONVERT(varchar(24),@lastupdated,120) );


insert PatientSearch.Patient_temp 
select @searchID, InternalPatientNumber SourcePatientNo, NHSNumber, NHSNumberStatus NHSNumberStatusId, DistrictNumber DistrictNo
, PtDoB DateOfBirth, ptDateOfDeath, 
PtDeathIndInt DeathIndicator 
			, Surname 
			, Forenames 
			, Title 
			, PreviousSurname1 
			, PtAddrLine1
			, PtAddrLine2
			, PtAddrLine3
			, PtAddrLine4
			, PtAddrPostCode Postcode 
			, PtHomePhone 
			, PtWorkPhone 
			, PtMobilePhone 
			, Sex SexCode 
			, MaritalStatus MaritalStatusCode
			, NoKName NextOfKin 
			, NokAddrLine1
			, NokAddrLine2
			, NokAddrLine3
			, NokAddrLine4
			, NokPostcode
			, NokRelationship
			, NoKHomePhone
			, NoKWorkPhone
			, EthnicType EthnicOriginCode 
			, Religion ReligionCode  
			, GpCode RegisteredGpCode  
from warehousesql.pas.inquire.patdata 
where internalpatientnumber = @ipn 

insert PatientSearch.Casenote_temp(_searchID,SourcePatientNo,casenote,Withdrawn)
select @searchID,@ipn,l.PtCasenoteIdPhyskey,convert(date,l.Withdrawn,103)
  from warehousesql.pas.inquire.CASENOTEDETS l
  where l.InternalPatientNumber = convert(nvarchar(9),@ipn)
end;
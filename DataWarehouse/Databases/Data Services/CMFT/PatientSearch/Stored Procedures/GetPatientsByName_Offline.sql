﻿CREATE proc [PatientSearch].[GetPatientsByName_Offline](
   @searchID uniqueidentifier
,  @error varchar(max) out
,  @lastName varchar(35) = null
,  @firstName varchar(35) = null
,  @dateOfBirth date = null
,  @gender char(1) = null
,  @debug bit = 0)
as
begin
set nocount on;

set @firstname += '%';
set @lastname += '%';

DECLARE @LASTUPDATED DATETIME2;

select TOP 1 @LASTUPDATED = LASTSTARTED 
from WAREHOUSESQL.cmftetl.ETL.EventTimes_view 
WHERE ID = 6116;

insert PatientSearch.SearchHeader(_searchID,dataSourceName, dataSourceCurrencyType,datasourceCurrencyDateTime, datasourceCurrencyDescription)
values (@searchID,'Data Warehouse', 'T' , convert(datetimeoffset,@lastupdated) 
, 'Current To: ' + CONVERT(varchar(24),@lastupdated,120) );


insert PatientSearch.Patient_temp 
select @searchID, InternalPatientNumber SourcePatientNo, NHSNumber, NHSNumberStatus NHSNumberStatusId, DistrictNumber DistrictNo
, PtDoB DateOfBirth, ptDateOfDeath, 
PtDeathIndInt DeathIndicator 
			, Surname 
			, Forenames 
			, Title 
			, PreviousSurname1 
			, PtAddrLine1
			, PtAddrLine2
			, PtAddrLine3
			, PtAddrLine4
			, PtAddrPostCode Postcode 
			, PtHomePhone 
			, PtWorkPhone 
			, PtMobilePhone 
			, Sex SexCode 
			, MaritalStatus MaritalStatusCode
			, NoKName NextOfKin 
			, NokAddrLine1
			, NokAddrLine2
			, NokAddrLine3
			, NokAddrLine4
			, NokPostcode
			, NokRelationship
			, NoKHomePhone
			, NoKWorkPhone
			, EthnicType EthnicOriginCode 
			, Religion ReligionCode  
			, GpCode RegisteredGpCode  
from warehousesql.pas.inquire.patdata 
where surname like @lastname
and Forenames like @firstName
and (convert(date,PtDOB,103) = @dateOfBirth or @DateOfBirth is null)
  and (Sex = @gender or @gender is null)

insert PatientSearch.Casenote_temp(_searchID,SourcePatientNo,casenote,Withdrawn)
select @searchID,l.InternalPatientNumber,l.PtCasenoteIdPhyskey,convert(date,l.Withdrawn,103)
from warehousesql.pas.inquire.CASENOTEDETS l
  inner join PatientSearch.Patient_temp p on l.internalpatientnumber = p.SourcePatientNo
  where p._searchID = @searchID;

end;
﻿CREATE proc [PatientSearch].[FormatResults]
(
	@searchID uniqueidentifier,
	@error varchar(max),
	@result xml out, 
	@debug bit = 0
)
as
begin

	set nocount on;

	if (@error is not null) begin

		select @result = (select @error [SearchResult/@error] for xml path(''), type)
		return;

	end;

	if (@debug = 1) begin
		select 'PatientSearch.SearchHeader' tbl, * from PatientSearch.SearchHeader where _searchID = @searchID 
		select 'PatientSearch.Patient_temp' tbl, * from PatientSearch.Patient_temp where _searchID = @searchID 
		select 'PatientSearch.Casenote_temp' tbl, * from PatientSearch.Casenote_temp where _searchID = @searchID 
	end;

	update PatientSearch.Patient_temp 
	set ptDateOfDeath =  right(ptDateOfDeath,4)+'-'+substring(ptDateOfDeath,4,2)+'-'+left(ptDateOfDeath,2) 
	where ptDateOfDeath is not null and ptDateOfDeath != ''
	and _searchID = @searchID;

	declare @ret xml;
	select @ret = ( select convert(varchar(32),sysdatetimeoffset(),120) [@ts],
		   datasourceName [DataSource/Name]
		  ,datasourceCurrencyType [DataSource/Currency/@type]
		  ,datasourceCurrencyDateTime [DataSource/Currency/@datestamp]
		  ,datasourceCurrencyDescription [DataSource/Currency]
	  ,(select Patient.SourcePatientNo [Patient/@SourcePatientNo]
			, coalesce(Patient.NHSNumber,'') [Patient/@NHSNumber]
			, Patient.NHSNumberStatusId [Patient/@NHSNumberStatusId]
			, Patient.DistrictNo [Patient/@DistrictNumber]
			, convert(date,Patient.DateOfBirth,103) [Patient/@DateOfBirth]
			, coalesce(Patient.ptDateOfDeath,'') [Patient/@DateOfDeath]
			, convert(datetime,Patient.DateOfBirth,103) [Patient/@DateTimeOfBirth]
			, coalesce(Patient.ptDateOfDeath,'')  [Patient/@DateTimeOfDeath]
			, Patient.DeathIndicator [Patient/@DeathIndicator]
			, Patient.Surname [Patient/Name/@Surname]
			, cmft.String.ProperCase(Patient.Forenames) [Patient/Name/@Forenames]
			, Patient.Title [Patient/Name/@Title]
			, Patient.PreviousSurname1 [Patient/Name/@PreviousSurname]
			, coalesce(PtAddrLine1,'') [Patient/Address/@Line1]
			, coalesce(PtAddrLine2,'') [Patient/Address/@Line2]
			, coalesce(PtAddrLine3,'') [Patient/Address/@Line3]
			, coalesce(PtAddrLine4,'') [Patient/Address/@Line4]
			, coalesce(Patient.Postcode,'') [Patient/Address/@Postcode]
			, PtHomePhone [Patient/Phone/@Home]
			, PtWorkPhone [Patient/Phone/@Work]
			, PtMobilePhone [Patient/Phone/@Mobile]
			, Patient.SexCode [Patient/Sex/@Code]
			, Sex.Sex [Patient/Sex/@Description]
			, Patient.MaritalStatusCode [Patient/MaritalStatus/@Code]
			, MaritalStatus.MaritalStatus [Patient/MaritalStatus/@Description]
,coalesce(nokrelationship,'')  [Patient/NextOfKin/@Relationship]
,coalesce(NOKHomePhone,NOKWorkPhone,'')	    [Patient/NextOfKin/@Phone]
,coalesce(nextofkin,'')	 [Patient/NextOfKin/@Name]
,coalesce(NOKAddrLine1,'')		 [Patient/NextOfKin/Address/@Line1]
,coalesce(NOKAddrLine2,'')[Patient/NextOfKin/Address/@Line2]
,coalesce(NOKAddrLine3,'')		 [Patient/NextOfKin/Address/@Line3]
,coalesce(NOKAddrLine4,'')		 [Patient/NextOfKin/Address/@Line4]
,coalesce(NOKpostcode,'')      [Patient/NextOfKin/Address/@Postcode]
			, Patient.EthnicOriginCode [Patient/EthnicOrigin/@Code]
			, EthnicOrigin.EthnicOrigin [Patient/EthnicOrigin/@Description]
			, Patient.ReligionCode [Patient/Religion/@Code]
			, Religion.Religion [Patient/Religion/@Description]
			, Patient.RegisteredGpCode  [Patient/GP/@Code]
			, GP.NationalCode [Patient/GP/@NationalCode]
			, GP.GpName [Patient/GP/@Name]
			, GP.PracticeCode [Patient/GP/Practice/@Code]
			, GP.[Address] [Patient/GP/Practice/@Address]
	, 
			coalesce(
			(select Withdrawn [Casenote/@Withdrawn],Casenote [Casenote] from Casenote_temp where Casenote_temp._searchID = @searchID and Casenote_temp.SourcePatientNo = Patient.SourcePatientNo order by withdrawn,casenote for xml path(''), type) 
			,'') [Patient/Casenotes] from PatientSearch.Patient_temp AS Patient
left outer join PAS.Sex
on Patient.SexCode = Sex.SexCode
left outer join pas.Ethnicity EthnicOrigin
on Patient.EthnicOriginCode = EthnicOrigin.EthnicOriginCode 
left outer join pas.MaritalStatus
on Patient.MaritalStatusCode = Maritalstatus.MaritalStatusCode
left outer join PAS.Religion
on Patient.ReligionCode = Religion.ReligionCode 
left outer join PAS.GP on gp.gpcode = patient.RegisteredGpCode 
where Patient._searchID = @searchID for xml path (''), type) [Patients]
from PatientSearch.SearchHeader
where _searchID = @searchID
for xml path('SearchResult') 
) 

set @result = coalesce(@ret,convert(xml,N'<SearchResult />'));

delete from PatientSearch.SearchHeader where _searchID = @searchID

end;
﻿create proc [PatientSearch].[GetPatientsByIPN] (@ipn int
,  @resultXML xml out
,  @debug bit = 0)
as
begin
declare @searchID uniqueidentifier = newid();
declare @error varchar(max);

declare @avail bit = 0;
declare @LinkServ sysname=N'INQUIREPROD';


exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;

if @avail=0 begin
  set @LinkServ = 'INQUIRE'
  exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;
end;

if @avail=0 begin
  set @LinkServ = 'WAREHOUSESQL'
end;

if (@debug = 1) begin
	select @LinkServ DataSource;
end;

if @LinkServ='WAREHOUSESQL' begin

	exec PatientSearch.GetPatientsByIPN_Offline @searchID, @error out, @ipn, @debug

end
else
begin
	exec PatientSearch.GetPatientsByIPN_ClinicomPAS @searchID, @error out, @ipn,@LinkServ, @debug
end;

exec PatientSearch.FormatResults @searchID,@error,@resultXML out,@debug;

end;
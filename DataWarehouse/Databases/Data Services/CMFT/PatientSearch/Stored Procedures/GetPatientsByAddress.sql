﻿create proc [PatientSearch].[GetPatientsByAddress] (
   @postcode varchar(12)
,  @addressLine1 varchar(35)
,  @resultXML xml out
,  @debug bit = 0)
as
begin
declare @searchID uniqueidentifier = newid();
declare @error varchar(max);

exec PatientSearch.GetPatientsByAddress_Offline @searchID, @error out, @postcode,@addressLine1, @debug


exec PatientSearch.FormatResults @searchID,@error,@resultXML out,@debug;

end;
﻿CREATE proc [PatientSearch].[GetPatientsByAddress_Offline](
   @searchID uniqueidentifier
,  @error varchar(max) out
,  @postcode varchar(12)
,  @addressLine1 varchar(35)
,  @debug bit = 0)
as
begin
set nocount on;
if len(rtrim(@postcode)) = 0 set @postcode = null;

if len(rtrim(@addressLine1)) = 0 set @addressLine1 = null;

if @postcode is null and @addressLine1 is null begin

  set @error='NO SEARCH CRITERIA';

  return; 

end;

set @postcode += '%';
select @addressLine1 = rtrim(replace(@addressLine1,' ','% ')) + '%';

if (@debug = 1)
  select @postcode Postcode, @addressLine1 AddressLine1

DECLARE @LASTUPDATED DATETIME2;

select TOP 1 @LASTUPDATED = LASTSTARTED 
from WAREHOUSESQL.cmftetl.ETL.EventTimes_view 
WHERE ID = 6116;

insert PatientSearch.SearchHeader(_searchID,dataSourceName, dataSourceCurrencyType,datasourceCurrencyDateTime, datasourceCurrencyDescription)
values (@searchID,'Data Warehouse', 'T' , convert(datetimeoffset,@lastupdated) 
, 'Current To: ' + CONVERT(varchar(24),@lastupdated,120) );

insert PatientSearch.Patient_temp 
select @searchID, InternalPatientNumber SourcePatientNo, NHSNumber, NHSNumberStatus NHSNumberStatusId, DistrictNumber DistrictNo
, PtDoB DateOfBirth, ptDateOfDeath, 
PtDeathIndInt DeathIndicator 
			, Surname 
			, Forenames 
			, Title 
			, PreviousSurname1 
			, PtAddrLine1
			, PtAddrLine2
			, PtAddrLine3
			, PtAddrLine4
			, PtAddrPostCode Postcode 
			, PtHomePhone 
			, PtWorkPhone 
			, PtMobilePhone 
			, Sex SexCode 
			, MaritalStatus MaritalStatusCode
			, NoKName NextOfKin 
			, NokAddrLine1
			, NokAddrLine2
			, NokAddrLine3
			, NokAddrLine4
			, NokPostcode
			, NokRelationship
			, NoKHomePhone
			, NoKWorkPhone
			, EthnicType EthnicOriginCode 
			, Religion ReligionCode  
			, GpCode RegisteredGpCode  
from warehousesql.pas.inquire.patdata 
where (PtAddrPostCode like @postcode or @postcode is null)
  and (PtAddrLine1 like @addressLine1 or @addressLine1 is null)

insert PatientSearch.Casenote_temp(_searchID,SourcePatientNo,casenote,Withdrawn)
select @searchID,l.InternalPatientNumber,l.PtCasenoteIdPhyskey,convert(date,l.Withdrawn,103)
from warehousesql.pas.inquire.CASENOTEDETS l
  inner join PatientSearch.Patient_temp p on l.internalpatientnumber = p.SourcePatientNo
  where p._searchID = @searchID;

end;
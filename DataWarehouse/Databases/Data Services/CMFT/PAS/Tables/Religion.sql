﻿CREATE TABLE [PAS].[Religion](
	[ReligionCode] [varchar](4) NOT NULL,
	[Religion] [varchar](20) NULL,
	[ReligionNationalCode] [varchar](50) NOT NULL
) ON [PRIMARY]
﻿CREATE TABLE [PAS].[Ethnicity](
	[EthnicOriginCode] [varchar](4) NOT NULL,
	[EthnicOrigin] [varchar](30) NULL,
	[NationalEthnicOriginCode] [varchar](2) NULL,
	[NationalEthnicOrigin] [varchar](40) NULL,
	[DeletedFlag] [varchar](255) NULL
) ON [PRIMARY]
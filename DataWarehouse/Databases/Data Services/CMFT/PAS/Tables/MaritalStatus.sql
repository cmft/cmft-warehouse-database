﻿CREATE TABLE [PAS].[MaritalStatus](
	[MaritalStatusCode] [varchar](1) NOT NULL,
	[MaritalStatus] [varchar](10) NULL,
	[NationalMaritalStatusCode] [varchar](1) NULL
) ON [PRIMARY]
﻿create proc [PAS].[GetPatientByIPN] (@ipn int, @debug bit = 0)
as
begin

declare @avail bit = 0;
declare @LinkServ sysname=N'INQUIREPROD';
declare @proc sysname=N'PAS.GetPatientByIPN_INQUIREPROD';
exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;

if @avail=0 begin
  set @LinkServ = 'INQUIRE'
  set @proc = N'PAS.GetPatientByIPN_INQUIRESHADOW'
  exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;
end;
if @avail=0 begin
  set @LinkServ = 'WAREHOUSESQL'
  set @proc = N'PAS.GetPatientByIPN_Offline'
end;

set @proc+=' ' + CONVERT(varchar(9),@ipn);

exec sp_executesql @proc;

end;
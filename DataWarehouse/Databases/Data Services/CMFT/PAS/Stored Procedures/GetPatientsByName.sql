﻿CREATE proc [PAS].[GetPatientsByName] (@lastName varchar(35)
,  @firstName varchar(35) = null
,  @dateOfBirth date = null
,  @gender char(1) = null
,  @debug bit = 0)
as
begin

declare @avail bit = 0;
declare @LinkServ sysname=N'INQUIREPROD';
declare @proc sysname=N'PAS.GetPatientsByName_INQUIREPROD @lastName,@firstName,@dateOfBirth, @gender,@debug';
exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;

if @avail=0 begin
  set @LinkServ = 'INQUIRE'
  set @proc = N'PAS.GetPatientsByName_INQUIRESHADOW @lastName,@firstName,@dateOfBirth, @gender,@debug'
  exec cmft.LinkedServer.IsAvailable @LinkServ,@avail out;
end;
if @avail=0 begin
  set @LinkServ = 'WAREHOUSESQL'
  set @proc = N'PAS.GetPatientsByName_Offline @lastName,@firstName,@dateOfBirth, @gender,@debug'
end;

if (@debug = 1) begin
	select @LinkServ,@proc ;
end;

exec sp_executesql @proc, 
	N'@lastName varchar(35),  @firstName varchar(35),  @dateOfBirth date ,  @gender char(1),  @debug bit'
	,@lastName,@firstName,@dateOfBirth,@gender,@debug;

end;
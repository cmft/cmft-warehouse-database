﻿create proc [PAS].[GetPatientByCasenote_INQUIREPROD](@casenote varchar(14), @debug bit = 0)
as
begin

declare @ipn varchar(9);


DECLARE @initialQuery nvarchar(4000) = '
select InternalPatientNumber 
	from openquery(INQUIREPROD, ''select top 1 InternalPatientNumber from CASENOTELINK where ExternalNumberKey=''''' + @casenote + ''''' AND EntityTypeKey = 1'')';

declare @ipnTable TABLE(ipn varchar(9) NOT NULL);
insert @ipnTable
exec (@initialQuery);

select TOP 1 @ipn = ipn FROM @ipnTable;


declare @pasQuery varchar(max)=
'select top 1 InternalPatientNumber SourcePatientNo, NHSNumber, NHSNumberStatus NHSNumberStatusId, DistrictNumber DistrictNo
, PtDoB DateOfBirth, ptDateOfDeath, 
PtDeathIndInt DeathIndicator 
			, Surname 
			, Forenames 
			, Title 
			, PreviousSurname1 
			, PtAddrLine1
			, PtAddrLine2
			, PtAddrLine3
			, PtAddrLine4
			, PtAddrPostCode Postcode 
			, PtHomePhone 
			, PtWorkPhone 
			, PtMobilePhone 
			, Sex SexCode 
			, MaritalStatus MaritalStatusCode
			, NoKName NextOfKin 
			, NokAddrLine1
			, NokAddrLine2
			, NokAddrLine3
			, NokAddrLine4
			, NokPostcode
			, NokRelationship
			, NoKHomePhone
			, NoKWorkPhone
			, EthnicType EthnicOriginCode 
			, Religion ReligionCode  
			, GpCode RegisteredGpCode  
			from  openquery(INQUIREPROD,''select InternalPatientNumber 
			, NHSNumber, NHSNumberStatus , DistrictNumber 
			, PtDoB , ptDateOfDeath 
			, PtDeathIndInt 
			, Surname 
			, Forenames 
			, Title 
			, PreviousSurname1 
			, PtAddrLine1
			, PtAddrLine2
			, PtAddrLine3
			, PtAddrLine4
			, PtAddrPostCode 
			, PtHomePhone 
			, PtWorkPhone 
			, PtMobilePhone 
			, Sex 
			, MaritalStatus 
			, NoKName
			, NokAddrLine1
			, NokAddrLine2
			, NokAddrLine3
			, NokAddrLine4
			, NokPostcode
			, NokRelationship
			, NoKHomePhone
			, NoKWorkPhone
			, EthnicType 
			, Religion 
			, GpCode 
			from PATDATA 
			where InternalPatientNumber = ' + @ipn + ''');';



declare @patdata TABLE (
	[SourcePatientNo] [varchar](12) NOT NULL,
	[NHSNumber] [varchar](17) NULL,
	[NHSNumberStatusId] [varchar](2) NULL,
	[DistrictNo] [varchar](14) NULL,
	[DateOfBirth] [varchar](14) NULL,
	[ptDateOfDeath] [varchar](14) NULL,
	[DeathIndicator] [varchar](1) NULL,
	[Surname] [varchar](255) NULL,
	[Forenames] [varchar](255) NULL,
	[Title] [varchar](5) NULL,
	[PreviousSurname1] [varchar](24) NULL,
	[PtAddrLine1] [varchar](20) NULL,
	[PtAddrLine2] [varchar](20) NULL,
	[PtAddrLine3] [varchar](20) NULL,
	[PtAddrLine4] [varchar](20) NULL,
	[Postcode] [varchar](10) NULL,
	[PtHomePhone] [varchar](23) NULL,
	[PtWorkPhone] [varchar](23) NULL,
	[PtMobilePhone] [varchar](23) NULL,
	[SexCode] [varchar](1) NULL,
	[MaritalStatusCode] [varchar](1) NULL,
	[NextOfKin] [varchar](30) NULL,
			 NokAddrLine1 nvarchar(20) NULL
			, NokAddrLine2 nvarchar(20) NULL
			, NokAddrLine3 nvarchar(20) NULL
			, NokAddrLine4 nvarchar(20) NULL
			, NokPostcode nvarchar(255) NULL
			, NokRelationship nvarchar(9) NULL
			, NoKHomePhone nvarchar(23) NULL
			, NoKWorkPhone nvarchar(23) NULL,
	[EthnicOriginCode] [varchar](4) NULL,
	[ReligionCode] [varchar](4) NULL,
	[RegisteredGpCode] [varchar](8) NULL
);

insert @patdata 
EXEC (@pasQuery)

update @patdata 
set ptDateOfDeath =  right(ptDateOfDeath,4)+'-'+substring(ptDateOfDeath,4,2)+'-'+left(ptDateOfDeath,2) 
where ptDateOfDeath is not null and ptDateOfDeath != '';

if @debug = 1
	select * from @patdata 

declare @ret xml;
	select @ret = ( select top 1 @casenote [Patient/@Casenote]
	,Patient.SourcePatientNo [Patient/@SourcePatientNo]
			, coalesce(Patient.NHSNumber,'') [Patient/@NHSNumber]
			, Patient.NHSNumberStatusId [Patient/@NHSNumberStatusId]
			, Patient.DistrictNo [Patient/@DistrictNumber]
			, convert(date,Patient.DateOfBirth,103) [Patient/@DateOfBirth]
			, coalesce(Patient.ptDateOfDeath,'') [Patient/@DateOfDeath]
			, convert(datetime,Patient.DateOfBirth,103) [Patient/@DateTimeOfBirth]
			, coalesce(Patient.ptDateOfDeath,'')  [Patient/@DateTimeOfDeath]
			, Patient.DeathIndicator [Patient/@DeathIndicator]
			, Patient.Surname [Patient/Name/@Surname]
			, cmft.String.ProperCase(Patient.Forenames) [Patient/Name/@Forenames]
			, Patient.Title [Patient/Name/@Title]
			, Patient.PreviousSurname1 [Patient/Name/@PreviousSurname]
			, coalesce(PtAddrLine1,'') [Patient/Address/@Line1]
			, coalesce(PtAddrLine2,'') [Patient/Address/@Line2]
			, coalesce(PtAddrLine3,'') [Patient/Address/@Line3]
			, coalesce(PtAddrLine4,'') [Patient/Address/@Line4]
			, coalesce(Patient.Postcode,'') [Patient/Address/@Postcode]
			, PtHomePhone [Patient/Phone/@Home]
			, PtWorkPhone [Patient/Phone/@Work]
			, PtMobilePhone [Patient/Phone/@Mobile]
			, Patient.SexCode [Patient/Sex/@Code]
			, Sex.Sex [Patient/Sex/@Description]
			, Patient.MaritalStatusCode [Patient/MaritalStatus/@Code]
			, MaritalStatus.MaritalStatus [Patient/MaritalStatus/@Description]
,coalesce(nokrelationship,'')  [Patient/NextOfKin/@Relationship]
,coalesce(NOKHomePhone,NOKWorkPhone,'')	    [Patient/NextOfKin/@Phone]
,coalesce(nextofkin,'')	 [Patient/NextOfKin/@Name]
,coalesce(NOKAddrLine1,'')		 [Patient/NextOfKin/Address/@Line1]
,coalesce(NOKAddrLine2,'')[Patient/NextOfKin/Address/@Line2]
,coalesce(NOKAddrLine3,'')		 [Patient/NextOfKin/Address/@Line3]
,coalesce(NOKAddrLine4,'')		 [Patient/NextOfKin/Address/@Line4]
,coalesce(NOKpostcode,'')      [Patient/NextOfKin/Address/@Postcode]
			, Patient.EthnicOriginCode [Patient/EthnicOrigin/@Code]
			, EthnicOrigin.EthnicOrigin [Patient/EthnicOrigin/@Description]
			, Patient.ReligionCode [Patient/Religion/@Code]
			, Religion.Religion [Patient/Religion/@Description]
			, Patient.RegisteredGpCode  [Patient/GP/@Code]
			, GP.NationalCode [Patient/GP/@NationalCode]
			, GP.GpName [Patient/GP/@Name]
			, GP.PracticeCode [Patient/GP/Practice/@Code]
			, GP.[Address] [Patient/GP/Practice/@Address]
from @patdata AS Patient
left outer join PAS.Sex
on Patient.SexCode = Sex.SexCode
left outer join pas.Ethnicity EthnicOrigin
on Patient.EthnicOriginCode = EthnicOrigin.EthnicOriginCode 
left outer join pas.MaritalStatus
on Patient.MaritalStatusCode = Maritalstatus.MaritalStatusCode
left outer join PAS.Religion
on Patient.ReligionCode = Religion.ReligionCode 
left outer join PAS.GP on gp.gpcode = patient.RegisteredGpCode 
where Patient.SourcePatientNo = @ipn
for xml path('SearchResult') 
) 

select coalesce(@ret,convert(xml,N'<SearchResult />')) SearchResultXML;

end;
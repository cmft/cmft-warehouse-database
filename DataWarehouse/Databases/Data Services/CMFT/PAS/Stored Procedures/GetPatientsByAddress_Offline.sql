﻿CREATE proc [PAS].[GetPatientsByAddress_Offline](
   @postcode varchar(12)
,  @addressLine1 varchar(35)
,  @debug bit = 0)
as
begin
set nocount on;
if len(rtrim(@postcode)) = 0 set @postcode = null;

if len(rtrim(@addressLine1)) = 0 set @addressLine1 = null;

if @postcode is null and @addressLine1 is null begin

  select convert(xml,N'<SearchResult error="NO SEARCH CRITERIA"/>') SearchResultXML;

  return 

end;

set @postcode += '%';
select @addressLine1 = rtrim(replace(@addressLine1,' ','% '));

declare @patdata TABLE (
	[SourcePatientNo] [varchar](12) NOT NULL,
	[NHSNumber] [varchar](17) NULL,
	[NHSNumberStatusId] [varchar](2) NULL,
	[DistrictNo] [varchar](14) NULL,
	[DateOfBirth] [varchar](14) NULL,
	[ptDateOfDeath] [varchar](14) NULL,
	[DeathIndicator] [varchar](1) NULL,
	[Surname] [varchar](255) NULL,
	[Forenames] [varchar](255) NULL,
	[Title] [varchar](5) NULL,
	[PreviousSurname1] [varchar](24) NULL,
	[PtAddrLine1] [varchar](20) NULL,
	[PtAddrLine2] [varchar](20) NULL,
	[PtAddrLine3] [varchar](20) NULL,
	[PtAddrLine4] [varchar](20) NULL,
	[Postcode] [varchar](10) NULL,
	[PtHomePhone] [varchar](23) NULL,
	[PtWorkPhone] [varchar](23) NULL,
	[PtMobilePhone] [varchar](23) NULL,
	[SexCode] [varchar](1) NULL,
	[MaritalStatusCode] [varchar](1) NULL,
	[NextOfKin] [varchar](30) NULL,
			 NokAddrLine1 nvarchar(20) NULL
			, NokAddrLine2 nvarchar(20) NULL
			, NokAddrLine3 nvarchar(20) NULL
			, NokAddrLine4 nvarchar(20) NULL
			, NokPostcode nvarchar(255) NULL
			, NokRelationship nvarchar(9) NULL
			, NoKHomePhone nvarchar(23) NULL
			, NoKWorkPhone nvarchar(23) NULL,
	[EthnicOriginCode] [varchar](4) NULL,
	[ReligionCode] [varchar](4) NULL,
	[RegisteredGpCode] [varchar](8) NULL
);

insert @patdata 
select InternalPatientNumber SourcePatientNo, NHSNumber, NHSNumberStatus NHSNumberStatusId, DistrictNumber DistrictNo
, PtDoB DateOfBirth, ptDateOfDeath, 
PtDeathIndInt DeathIndicator 
			, Surname 
			, Forenames 
			, Title 
			, PreviousSurname1 
			, PtAddrLine1
			, PtAddrLine2
			, PtAddrLine3
			, PtAddrLine4
			, PtAddrPostCode Postcode 
			, PtHomePhone 
			, PtWorkPhone 
			, PtMobilePhone 
			, Sex SexCode 
			, MaritalStatus MaritalStatusCode
			, NoKName NextOfKin 
			, NokAddrLine1
			, NokAddrLine2
			, NokAddrLine3
			, NokAddrLine4
			, NokPostcode
			, NokRelationship
			, NoKHomePhone
			, NoKWorkPhone
			, EthnicType EthnicOriginCode 
			, Religion ReligionCode  
			, GpCode RegisteredGpCode  
from warehousesql.pas.inquire.patdata 
where (PtAddrPostCode like @postcode or @postcode is null)
  and (PtAddrLine1 like @addressLine1 or @addressLine1 is null)

update @patdata 
set ptDateOfDeath =  right(ptDateOfDeath,4)+'-'+substring(ptDateOfDeath,4,2)+'-'+left(ptDateOfDeath,2) 
where ptDateOfDeath is not null and ptDateOfDeath != '';

if @debug = 1
	select * from @patdata 

DECLARE @LASTUPDATED DATETIME2;

select TOP 1 @LASTUPDATED = LASTSTARTED 
from WAREHOUSESQL.cmftetl.etl.[eventtimes_view] 
WHERE ID = 6116;


declare @ret xml;
	select @ret = ( select Patient.SourcePatientNo [Patient/@SourcePatientNo]
			, coalesce(Patient.NHSNumber,'') [Patient/@NHSNumber]
			, Patient.NHSNumberStatusId [Patient/@NHSNumberStatusId]
			, Patient.DistrictNo [Patient/@DistrictNumber]
			, convert(date,Patient.DateOfBirth,103) [Patient/@DateOfBirth]
			, coalesce(Patient.ptDateOfDeath,'') [Patient/@DateOfDeath]
			, convert(datetime,Patient.DateOfBirth,103) [Patient/@DateTimeOfBirth]
			, coalesce(Patient.ptDateOfDeath,'')  [Patient/@DateTimeOfDeath]
			, Patient.DeathIndicator [Patient/@DeathIndicator]
			, Patient.Surname [Patient/Name/@Surname]
			, cmft.String.ProperCase(Patient.Forenames) [Patient/Name/@Forenames]
			, Patient.Title [Patient/Name/@Title]
			, Patient.PreviousSurname1 [Patient/Name/@PreviousSurname]
			, coalesce(PtAddrLine1,'') [Patient/Address/@Line1]
			, coalesce(PtAddrLine2,'') [Patient/Address/@Line2]
			, coalesce(PtAddrLine3,'') [Patient/Address/@Line3]
			, coalesce(PtAddrLine4,'') [Patient/Address/@Line4]
			, coalesce(Patient.Postcode,'') [Patient/Address/@Postcode]
			, PtHomePhone [Patient/Phone/@Home]
			, PtWorkPhone [Patient/Phone/@Work]
			, PtMobilePhone [Patient/Phone/@Mobile]
			, Patient.SexCode [Patient/Sex/@Code]
			, Sex.Sex [Patient/Sex/@Description]
			, Patient.MaritalStatusCode [Patient/MaritalStatus/@Code]
			, MaritalStatus.MaritalStatus [Patient/MaritalStatus/@Description]
,coalesce(nokrelationship,'')  [Patient/NextOfKin/@Relationship]
,coalesce(NOKHomePhone,NOKWorkPhone,'')	    [Patient/NextOfKin/@Phone]
,coalesce(nextofkin,'')	 [Patient/NextOfKin/@Name]
,coalesce(NOKAddrLine1,'')		 [Patient/NextOfKin/Address/@Line1]
,coalesce(NOKAddrLine2,'')[Patient/NextOfKin/Address/@Line2]
,coalesce(NOKAddrLine3,'')		 [Patient/NextOfKin/Address/@Line3]
,coalesce(NOKAddrLine4,'')		 [Patient/NextOfKin/Address/@Line4]
,coalesce(NOKpostcode,'')      [Patient/NextOfKin/Address/@Postcode]
			, Patient.EthnicOriginCode [Patient/EthnicOrigin/@Code]
			, EthnicOrigin.EthnicOrigin [Patient/EthnicOrigin/@Description]
			, Patient.ReligionCode [Patient/Religion/@Code]
			, Religion.Religion [Patient/Religion/@Description]
			, Patient.RegisteredGpCode  [Patient/GP/@Code]
			, GP.NationalCode [Patient/GP/@NationalCode]
			, GP.GpName [Patient/GP/@Name]
			, GP.PracticeCode [Patient/GP/Practice/@Code]
			, GP.[Address] [Patient/GP/Practice/@Address]
from @patdata AS Patient
left outer join PAS.Sex
on Patient.SexCode = Sex.SexCode
left outer join pas.Ethnicity EthnicOrigin
on Patient.EthnicOriginCode = EthnicOrigin.EthnicOriginCode 
left outer join pas.MaritalStatus
on Patient.MaritalStatusCode = Maritalstatus.MaritalStatusCode
left outer join PAS.Religion
on Patient.ReligionCode = Religion.ReligionCode 
left outer join PAS.GP on gp.gpcode = patient.RegisteredGpCode 
for xml path('')
);



declare @retNVC nvarchar(max) =  N'<SearchResult>
<Chameleon ts="'+convert(varchar(50),sysdatetimeoffset())+'">
    <DataSource>
      <Name>Data Warehouse</Name>
      <Currency>Current To: '+CONVERT(varchar(50),@LASTUPDATED)+'</Currency>
    </DataSource>
  </Chameleon>' + convert(nvarchar(max),@ret)+ N'</SearchResult>';
  set @ret = convert(xml,@retNVC);

select coalesce(@ret,convert(xml,N'<SearchResult />')) SearchResultXML;

end;
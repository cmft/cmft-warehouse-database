﻿CREATE TABLE [Utility].[TempFiles](
	[tempFilesGroupGUID] [uniqueidentifier] NOT NULL,
	[filedate] [date] NOT NULL,
	[filetime] [time](7) NOT NULL,
	[filedatetime]  AS (CONVERT([datetime],[filedate],(0))+CONVERT([datetime],[filetime],(0))),
	[filename] [varchar](255) NOT NULL,
	[fileSizeBytes] [bigint] NOT NULL
) ON [PRIMARY]
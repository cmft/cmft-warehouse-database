﻿create function [Utility].[FileListing](@keyGUID uniqueidentifier) returns table
 as
 return (
select [filedatetime]
      ,[fileSizeBytes]
	  ,[filename]
  FROM [Utility].[TempFiles]
  where tempFilesGroupGUID = @keyGUID
)
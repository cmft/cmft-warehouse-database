﻿CREATE FUNCTION [Utility].[Xml_XSLT_ToString](@inputXML [xml], @inputTransform [xml])
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.XSLT].[UserDefinedFunctions].[Xml_XSLT_ToString]
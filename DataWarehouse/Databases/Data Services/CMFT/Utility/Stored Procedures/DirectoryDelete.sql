﻿CREATE PROCEDURE [Utility].[DirectoryDelete]
	@Path [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[DirectoryDelete]
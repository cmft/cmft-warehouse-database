﻿CREATE proc [Utility].[FileListingCreate](@pathSearchExpression varchar(1024), @keyGUID uniqueidentifier output)
as
set nocount on;

--declare @pathSearchExpression varchar(1024)='\\BEDMANSQLPROD\Bedman\*.*'
--declare @keyGUID uniqueidentifier;

declare @keyGUIDout uniqueidentifier = newid();
set @keyGUID = @keyGUIDout;

declare @cmd varchar(255);
declare @ret1 int, @ret2 int;
set @cmd = 'dir "'+ @pathSearchExpression +'"  /O-d'

declare @directoryListingFull table(line varchar(1000))
insert @directoryListingFull
exec @ret1 = xp_cmdshell @cmd 

set @cmd = 'dir "'+ @pathSearchExpression +'" /b /O-d'
declare @directoryFilenameListing table(filename varchar(255))
insert @directoryFilenameListing
exec @ret2 = xp_cmdshell @cmd 

delete from @directoryFilenameListing where filename is null

declare @file varchar(255);

select top 1 @file = [filename] from @directoryFilenameListing;

if @file!='File Not Found' AND @ret1=0 and @ret2=0 begin
	insert Utility.TempFiles(tempFilesGroupGUID,fileSizeBytes,filedate,filetime,filename)
	select @keyGUIDout, convert(bigint,replace(right(left(f.line,len(f.line)-len(fb.filename)),len(left(f.line,len(f.line)-len(fb.filename)))-17),',','')) filesizeBytes, convert(date,left(f.line,10),103) filedate,convert(time,substring(f.line,13,6)) filetime , fb.filename 
	from @directorylistingfull f inner join @directoryFilenameListing fb 
	on fb.filename = right(f.line,len(fb.filename))
end;

return 0;
﻿CREATE proc [Utility].[FileListingCreate2](@path varchar(1024),@searchExpression varchar(1024), @keyGUID uniqueidentifier output)
as
set nocount on;

--declare @pathSearchExpression varchar(1024)='\\BEDMANSQLPROD\Bedman\*.*'
--declare @keyGUID uniqueidentifier;

declare @keyGUIDout uniqueidentifier = newid();

set @keyGUID = @keyGUIDout;

	insert Utility.TempFiles(tempFilesGroupGUID,fileSizeBytes,filedate,filetime,filename)
	select @keyGUIDOut, d.size,d.date_created,d.date_modified,d.name from [CMFT].[Utility].[DirectoryList] (@path,@SearchExpression) d
 order by d.date_modified desc
 

return 0;
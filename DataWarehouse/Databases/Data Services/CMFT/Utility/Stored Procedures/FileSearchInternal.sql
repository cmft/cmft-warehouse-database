﻿CREATE PROCEDURE [Utility].[FileSearchInternal]
	@Pattern [nvarchar](255),
	@Filename [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[FileSearchInternal]
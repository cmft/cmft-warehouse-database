﻿CREATE PROCEDURE [Utility].[FileSearch]
	@Pattern [nvarchar](255),
	@Path [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[FileSearch]
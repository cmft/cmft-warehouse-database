﻿CREATE PROC [Utility].[LatestFile_ForNetworkLocations](@sourcePattern varchar(255), @file varchar(255) OUTPUT, @DEBUG BIT = 0)
AS
BEGIN

DECLARE @files TABLE(line varchar(max)) 
DECLARE @ret varchar(255);
declare @command varchar(255) = 'dir ' + @sourcePattern + ' /b /O-d'

IF @DEBUG = 1 PRINT @command

INSERT @files
EXEC xp_cmdshell @command_string = @command;
SELECT top 1 @file = line FROM @files;
IF @file = 'File Not Found' SET @file = NULL;
END;
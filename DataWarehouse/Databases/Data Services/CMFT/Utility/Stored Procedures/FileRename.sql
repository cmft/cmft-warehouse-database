﻿CREATE PROCEDURE [Utility].[FileRename]
	@FileNewName [nvarchar](255),
	@FileOldName [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[FileRename]
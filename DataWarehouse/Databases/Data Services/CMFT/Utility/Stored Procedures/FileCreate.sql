﻿CREATE PROCEDURE [Utility].[FileCreate]
	@Filename [nvarchar](255),
	@Path [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[FileCreate]
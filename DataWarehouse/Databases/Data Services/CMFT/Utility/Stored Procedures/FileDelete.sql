﻿CREATE PROCEDURE [Utility].[FileDelete]
	@Filename [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[FileDelete]
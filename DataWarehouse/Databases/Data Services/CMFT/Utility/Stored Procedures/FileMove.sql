﻿CREATE PROCEDURE [Utility].[FileMove]
	@Filename [nvarchar](255),
	@FileDestination [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[FileMove]
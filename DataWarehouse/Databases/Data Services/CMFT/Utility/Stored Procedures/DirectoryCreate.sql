﻿CREATE PROCEDURE [Utility].[DirectoryCreate]
	@Path [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[DirectoryCreate]
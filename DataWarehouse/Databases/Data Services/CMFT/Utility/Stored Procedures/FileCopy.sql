﻿CREATE PROCEDURE [Utility].[FileCopy]
	@ExistingFileNamePath [nvarchar](255),
	@NewFileNamePath [nvarchar](255),
	@OverWrite [bit]
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[FileCopy]
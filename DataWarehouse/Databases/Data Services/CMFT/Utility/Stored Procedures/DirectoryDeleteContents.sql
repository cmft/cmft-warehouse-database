﻿CREATE PROCEDURE [Utility].[DirectoryDeleteContents]
	@Path [nvarchar](255),
	@DaysToKeep [smallint],
	@FileExtension [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[DirectoryDeleteContents]
﻿create proc [Utility].[FileListingClear](@keyGUID uniqueidentifier)
as
set nocount on;

delete from Utility.TempFiles 
where tempFilesGroupGUID = @keyGUID;
return 0;
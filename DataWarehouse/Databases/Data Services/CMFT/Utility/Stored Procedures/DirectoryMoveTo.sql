﻿CREATE PROCEDURE [Utility].[DirectoryMoveTo]
	@Path [nvarchar](255),
	@Destination [nvarchar](255)
WITH EXECUTE AS CALLER
AS
EXTERNAL NAME [FileSystemHelper].[StoredProcedures].[DirectoryMoveTo]
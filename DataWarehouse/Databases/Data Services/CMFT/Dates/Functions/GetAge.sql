﻿CREATE function [Dates].[GetAge](@DateOfBirth date, @AsAt as date = null)
 returns smallint
 as
 begin

   declare @result smallint;

   if (@AsAt is null) set @AsAt = getdate();

   	
	if @DateOfBirth >= @AsAt
	  set @result = 0;
	else
		begin
			if (month(@AsAt)*100)+DAY(@AsAt)> = (MONTH(@DateOfBirth)*100) + DAY(@DateOfBirth)
				set @result = datediff(Year,@DateOfBirth,@AsAt);
			else
				set @result = datediff(Year,@DateOfBirth,@AsAt)-1;

		end;

   return @result;
 end
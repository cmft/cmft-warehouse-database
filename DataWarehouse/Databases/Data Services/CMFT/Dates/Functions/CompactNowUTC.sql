﻿CREATE function [Dates].[CompactNowUTC]()
returns varchar(14)
as 
begin
return Dates.CompactDateTime(SYSUTCDATETIME());
end;
﻿CREATE  proc [APC].[PreviousAdmission]
	(
	@DistrictNo					int = 0
	,@AdmittedPreviousDayRange	int = 365			-- Previous days to search for a previous admission
	,@currentDate				datetime = null		-- In case today is not the current admission date
	,@AdmittedPrevious			int = 0 output
	)

with execute as 'cmmc\jabran.rafiq'		-- Added Jabran Rafiq 08/08/2014
as

/****************************************************************************************
	Stored procedure : [Chameleon].[FlagConditionAPC]
	Description		 : Generic APC proc for TIE, querying by district number
	
	This proc is designed to be APC generic. We can add more return variables later (by
	adding more columns to the CTE).

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	08/08/2014	Paul Egan       Initial Coding
	10/12/2014	Paul Egan		Allow dirty reads, to try and fix overnight connection errors
								during ETLs.
*****************************************************************************************/

set nocount on;
set transaction isolation level read uncommitted;		-- Added Paul Egan 10/12/2014. Try and fix overnight connection errors.

if @currentDate is null
	set @currentDate = getdate()	-- Default to today
;



/* Get data for return variable(s) */
with DataForReturnVariables as
(
select
	AdmittedPreviously = 
		case
			when sum(case when datediff(day, Encounter.AdmissionDate, @currentDate) <= @AdmittedPreviousDayRange 
				and PatientClassificationCode not in ('RD', 'RN')	-- Exclude regular days / regular nights
			then 1 else 0 end) > 0 then 1
			else 0
		end
from
	WarehouseReportingMerged.APC.Encounter
where
	Encounter.DistrictNo = @DistrictNo
	and Encounter.ContextCode = 'CEN||PAS'
		
)



/* Populate return variable(s) */
select @AdmittedPrevious = AdmittedPreviously from DataForReturnVariables;

return
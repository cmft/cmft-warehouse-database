﻿CREATE procedure [Graphnet].[CWSResultNumeric]

	 @OcmTransKey int
	 
with execute as 'cmmc\jabran.rafiq'
as

SELECT 
	 OCResult.ResultCode
	,OCResultBase.ResultDescription
	,OCResult.Result
	,OCResult.RangeIndicator
	,OCResult.Comment1
	,OCResult.Comment2
	,OCResult.Comment3
	,OCResult.Comment4
	,OCResult.Comment5
	,OCResult.Comment6
	,OCResultBase.UnitOfMeasurement
	,OCResult.ResultDate
	,OCResult.ResultTime
from 
	PAS.Inquire.OCRESULT OCResult

left join PAS.inquire.OCMFRESULTS OCResultBase
on	OCResultBase.ResultCd = OCResult.ResultCode

WHERE
	OCResult.OcmTransKey = @OcmTransKey
and SessionNumber = 
		(
		SELECT 
			MIN(SessionNumber) 
		FROM 
			PAS.Inquire.OCRESULT 
		WHERE 
			OcmTransKey = @OcmTransKey
		)
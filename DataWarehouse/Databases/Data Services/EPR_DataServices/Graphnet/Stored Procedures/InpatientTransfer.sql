﻿CREATE procedure [Graphnet].[InpatientTransfer]
	 @NHSNumber varchar(12)
	,@ProviderSpellNo varchar(20)

with execute as 'cmmc\jabran.rafiq'
as

--Inpatient Transfers(A02)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;

select
	 NHSNumber = @NHSNumber
	--,SourcePatientNo
	--,DistrictNo
	,WardStay.ProviderSpellNo


	,Ward.SourceWardCode
	,Ward.SourceWard

	,SiteCode = Site.SourceSiteCode
	,Site = Site.SourceSite

	,WardStay.StartDate
	,WardStay.EndDate
	,WardStay.StartTime
	,WardStay.EndTime
	,WardStay.StartActivityCode
	,WardStay.EndActivityCode
	
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceSpellNo,null)	


	--,ConsultantCode = Consultant.SourceConsultantCode
	--,Consultant = Consultant.SourceConsultant
	
	--,SpecialtyCode = Specialty.SourceSpecialtyCode
	--,Specialty = Specialty.SourceSpecialty

FROM 
	WarehouseOLAPMergedV2.APC.BaseWardStay WardStay

inner join WarehouseOLAPMergedV2.APC.BaseWardStayReference Reference
on	Reference.MergeEncounterRecno = WardStay.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounter Encounter
on	Encounter.SourcePatientNo = WardStay.SourcePatientNo
and	Encounter.SourceSpellNo = WardStay.SourceSpellNo
--and 
--Encounter.EpisodeStartTime between WardStay.StartTime and coalesce(WardStay.EndTime,getdate ())
and	Encounter.FirstEpisodeInSpellIndicator = 1

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = BaseEncounterReference.AdmissionDateID
and	Calendar.TheDate >= dateadd(year , -1 , getdate())

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

inner join WarehouseOLAPMergedV2.APC.Ward
on	Ward.SourceWardID = Reference.WardID

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

Left join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = BaseEncounterReference.ConsultantID

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = BaseEncounterReference.SpecialtyID


where
	(
		WardStay.SourcePatientNo = @SourcePatientNo
	or	@SourcePatientNo is null
	)
and	(
		WardStay.ProviderSpellNo = @ProviderSpellNo
	or	@ProviderSpellNo is null
	)


order by
	WardStay.StartTime
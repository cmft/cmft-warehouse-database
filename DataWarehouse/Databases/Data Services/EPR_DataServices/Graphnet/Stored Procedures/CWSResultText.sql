﻿CREATE procedure [Graphnet].[CWSResultText]
	 @OcmTransKey int
	 
with execute as 'cmmc\jabran.rafiq'
as

select
	 OCResultText.OcmTransKey
	,OCResultText.SessionNumber
	,OCResultText.ResultCode
	,OCResultBase.ResultDescription
	,OCResultText.AgpSeqNo
	,OCResultText.Comment
	,OCResultText.SubResultName
	,OCResultText.SubResultValue  
from 
	PAS.Inquire.OCMTEXTRESULTS OCResultText

left join PAS.inquire.OCMFRESULTS OCResultBase
on	OCResultBase.ResultCd = OCResultText.ResultCode

where
	OCResultText.OcmTransKey = @OcmTransKey
and OCResultText.SessionNumber =
		(
		select
			min(SessionNumber) 
		from 
			PAS.Inquire.OCMTEXTRESULTS OCResultText
		where 
			OCResultText.OcmTransKey = @OcmTransKey
		)
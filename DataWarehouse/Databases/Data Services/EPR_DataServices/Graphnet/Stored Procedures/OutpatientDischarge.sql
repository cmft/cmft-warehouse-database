﻿CREATE procedure [Graphnet].[OutpatientDischarge]
	@NHSNumber varchar(12)

with execute as 'cmmc\jabran.rafiq'
as
--Outpatient Discharge (A03)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;

select
	 Encounter.NHSNumber
	,Encounter.SourcePatientNo
	,Encounter.DistrictNo
	,Encounter.SourceEncounterNo
	,ClinicCode = Clinic.SourceClinicCode
	,Clinic = Clinic.SourceClinic
	,SiteCode = Site.SourceSiteCode
	,Site = Site.SourceSite
	,Encounter.SourceUniqueID
	,ConsultantCode = Consultant.SourceConsultantCode
	,Consultant = Consultant.SourceConsultant
	,SpecialtyCode = Specialty.SourceSpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,SourceOfReferralCode = SourceOfReferral.SourceSourceOfReferralCode
	,SourceOfReferral = SourceOfReferral.SourceSourceOfReferral
	,PurchaserCode = CCG.CCGCode
	,Purchaser = CCG.CCG
	,Encounter.AppointmentTime
	,ReasonForReferralCode = ReasonForReferral.SourceReasonForReferralCode
	,ReasonForReferral = ReasonForReferral.SourceReasonForReferral
	,Encounter.AppointmentCreateDate
	,Encounter.DischargeDate
	,RFEncounter.DischargeTime
	
	,DichargeCode = OPDISCHARGE.DischargeCode
	,DichargeReason = OPDISCHARGE.Reason
	,DischargeReasonCode = OPDISCHARGE.ReasonCode
	
	,ActivityRcordedDate = Encounter.AppointmentCreateDate
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceEncounterNo, 22)
	
from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.RF.BaseEncounter RFEncounter
on	RFEncounter.SourcePatientNo = Encounter.SourcePatientNo 
and	RFEncounter.SourceEncounterNo = Encounter.SourceEncounterNo

inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.OP.Clinic
on	Clinic.SourceClinicID = Reference.ClinicID

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

inner join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.OP.ReasonForReferral
on	ReasonForReferral.SourceReasonForReferralID = Reference.ReasonForReferralID

inner join WarehouseOLAPMergedV2.WH.CCG
on	CCG.CCGID = Reference.AppointmentResidenceCCGID

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.AppointmentDateID
and	Calendar.TheDate >= dateadd(year , -1 , getdate())

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

inner join WarehouseOLAPMergedV2.OP.AttendanceStatus
on	AttendanceStatus.SourceAttendanceStatusID = Reference.AttendanceStatusID
and	AttendanceStatus.NationalAttendanceStatusCode in ('5' , '6')

left join PAS.Inquire.OPDISCHARGE OPDISCHARGE
on OPDISCHARGE.InternalPatientNumber = Encounter.SourcePatientNo
and OPDISCHARGE.EpisodeNumber = Encounter.SourceEncounterNo

where
	Encounter.IsWardAttender = '0' 
and	Encounter.SourcePatientNo = @SourcePatientNo

--eliminate all appointments except the discharge appointment
and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.BaseEncounter LaterEncounter
	where
		LaterEncounter.SourcePatientNo = Encounter.SourcePatientNo
	and	LaterEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
	and	(
			LaterEncounter.AppointmentTime > Encounter.AppointmentTime
		or	(
				LaterEncounter.AppointmentTime = Encounter.AppointmentTime
			and	LaterEncounter.DoctorCode > Encounter.DoctorCode
			)
		)
	)
and	RFEncounter.DischargeTime is not null

--order by
--	Encounter.DischargeDate
	
	

	union all
	
	select
	 Encounter.NHSNumber
	,Encounter.SourcePatientNo
	,Encounter.DistrictNo
	,Encounter.SourceEncounterNo
	,ClinicCode = null --Clinic.SourceClinicCode
	,Clinic = null --Clinic.SourceClinic
	,SiteCode = Site.SourceSiteCode
	,Site = Site.SourceSite
	,Encounter.SourceUniqueID
	,ConsultantCode = Consultant.SourceConsultantCode
	,Consultant = Consultant.SourceConsultant
	,SpecialtyCode = Specialty.SourceSpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,SourceOfReferralCode = SourceOfReferral.SourceSourceOfReferralCode
	,SourceOfReferral = SourceOfReferral.SourceSourceOfReferral
	,PurchaserCode = CCG.CCGCode
	,Purchaser = CCG.CCG
	,AppointmentTime = null
	,ReasonForReferralCode = ReasonForReferral.SourceReasonForReferralCode
	,ReasonForReferral = ReasonForReferral.SourceReasonForReferral
	,AppointmentCreateDate = null
	,Encounter.DischargeDate
	,Encounter.DischargeTime
	
	,DichargeCode = OPDISCHARGE.DischargeCode
	,DichargeReason = OPDISCHARGE.Reason
	,DischargeReasonCode = OPDISCHARGE.ReasonCode
	
	
	,ActivityRcordedDate = Encounter.Created	
	
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceEncounterNo, 22)
	
	from
	
		WarehouseOLAPMergedV2.RF.BaseEncounter Encounter
		
inner join WarehouseOLAPMergedV2.RF.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

inner join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.OP.ReasonForReferral
on	ReasonForReferral.SourceReasonForReferralID = Reference.ReasonForReferralID

left join WarehouseOLAPMergedV2.WH.CCG
on	CCG.CCGID = Reference.ResidenceCCGID

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.ReferralDateID
and	Calendar.TheDate >= dateadd(year , -1 , getdate())


inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

left join PAS.Inquire.OPDISCHARGE OPDISCHARGE
on OPDISCHARGE.InternalPatientNumber = Encounter.SourcePatientNo
and OPDISCHARGE.EpisodeNumber = Encounter.SourceEncounterNo
		
		where 
		
		Encounter.DischargeTime is not null
		
		and
		Encounter.SourcePatientNo = @SourcePatientNo
		
		and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.BaseEncounter LaterEncounter
	where
		LaterEncounter.SourcePatientNo = Encounter.SourcePatientNo
		and 
		LaterEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
	

	)
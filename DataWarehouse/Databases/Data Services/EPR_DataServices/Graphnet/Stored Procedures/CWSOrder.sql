﻿CREATE procedure [Graphnet].[CWSOrder]
	 @InternalPatientNumber varchar(9)
	,@EpisodeNumber varchar(4)
	
with execute as 'cmmc\jabran.rafiq'
as

select 
	 OCOrder.StartDateTime
	,OCOrder.OrderDate
	,OCOrder.OrderTime
	,OCOrder.OcmTransKey
	,OCOrder.SampleRef
	,OCOrder.ServiceCode
	,Service = OCService.Description
	,OrderTypeCode = OCService.ScreenType
	,OrderType = OCScreenType.ScreenType_Desc
	,PerformingLocationCode = OCOrder.PerfLocation
	,PerformingLocation = OCLocation.Description
	,OCOrder.LatestSessionNumber
	,OCOrder.Status
	,EnteredByCode = OCOrder.EnteredBy
	,EnteredBy = PASUser.UserIdName
	,OCOrder.OrderedBy
	,SpecialtyCode = OCOrder.Specialty
	,Specialty.Specialty
	,OCOrder.OrderComment1
	,OCOrder.OrderComment2
	,OCOrder.OrderComment3
	,OCOrder.OrderComment4
	,OCOrder.OrderComment5
	,OCOrder.OrderComment6
	
	,CreateDate = convert(datetime,OCOrder.OrderDate)


from
	--PAS.Inquire.OCMPTLINK OCLink

--left join 
PAS.Inquire.OCORDER OCOrder
--on	OCOrder.OcmTransKey = OCLink.OcmTransKey

left join PAS.inquire.OCMSERVICE OCService
on	OCService.Service = OCOrder.ServiceCode

left join PAS.Inquire.OCMFLOCATION OCLocation
on	OCLocation.PerformingLoc = OCOrder.PerfLocation

left join PAS.Inquire.OCMSCREENTYPE OCScreenType
on	OCScreenType.ScreenType = OCService.ScreenType

left join PAS.Inquire.AMSUID PASUser
on	PASUser.UserId = OCOrder.EnteredBy

left join Warehouse.PAS.Specialty
on	Specialty.SpecialtyCode = OCOrder.Specialty

where
	--OCLink.InternalPatientNumber = @InternalPatientNumber
	OCOrder.PatientNumber = @InternalPatientNumber
and 
	OCOrder.EpisodeNumber = @EpisodeNumber
--OCLink.EpisodeNumber = @EpisodeNumber
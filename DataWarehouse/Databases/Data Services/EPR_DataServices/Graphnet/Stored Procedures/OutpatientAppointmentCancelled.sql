﻿CREATE procedure [Graphnet].[OutpatientAppointmentCancelled]
	@NHSNumber varchar(12)

with execute as 'cmmc\jabran.rafiq'
as
--Cancelled outpatient appointments (A38)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;

select
	 Encounter.NHSNumber
	--,SourcePatientNo
	--,DistrictNo
	,ClinicCode = Clinic.SourceClinicCode
	,Clinic = Clinic.SourceClinic
	,SiteCode = Site.SourceSiteCode
	,Site = Site.SourceSite
	,SourceUniqueID
	,ConsultantCode = Consultant.SourceConsultantCode
	,Consultant = Consultant.SourceConsultant
	,SpecialtyCode = Specialty.SourceSpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,SourceOfReferralCode = SourceOfReferral.SourceSourceOfReferralCode
	,SourceOfReferral = SourceOfReferral.SourceSourceOfReferral
	,PurchaserCode = CCG.CCGCode
	,Purchaser = CCG.CCG
	,AppointmentTime
	,ReasonForReferralCode = ReasonForReferral.SourceReasonForReferralCode
	,ReasonForReferral = ReasonForReferral.SourceReasonForReferral
	,AppointmentCreateDate
	,AppointmentCancelDate
	
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceEncounterNo,23)	
from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.OP.Clinic
on	Clinic.SourceClinicID = Reference.ClinicID

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

inner join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.OP.ReasonForReferral
on	ReasonForReferral.SourceReasonForReferralID = Reference.ReasonForReferralID

inner join WarehouseOLAPMergedV2.WH.CCG
on	CCG.CCGID = Reference.AppointmentResidenceCCGID

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.AppointmentDateID
and	Calendar.TheDate >= dateadd(year , -1 , getdate())

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

inner join WarehouseOLAPMergedV2.OP.AttendanceStatus
on	AttendanceStatus.SourceAttendanceStatusID = Reference.AttendanceStatusID
and	AttendanceStatus.NationalAttendanceStatusCode in ('2' , '4')

where
	Encounter.IsWardAttender = '0' 
and	SourcePatientNo = @SourcePatientNo

order by
	Encounter.AppointmentTime
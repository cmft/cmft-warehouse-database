﻿/****** Object:  Schema [CasenoteTracking]    Script Date: 10/31/2011 08:17:56 ******/

CREATE proc [Graphnet].[GetDistrictNumberFromNHSNumber](
@inputNHSNumber varchar(10), 
@patientNumber varchar(9) OUTPUT
)

with execute as 'cmmc\jabran.rafiq'

AS

declare @ipn varchar(9);

DECLARE @initialQuery nvarchar(4000) = '
select InternalPatientNumber 
	from openquery(INQUIRE, ''select top 1 InternalPatientNumber from CASENOTELINK where ExternalNumberKey=''''' + @inputNHSNumber + ''''' AND EntityTypeKey = 3'')';

declare @ipnTable TABLE(ipn varchar(9) NOT NULL);
insert @ipnTable
exec (@initialQuery);

select TOP 1 @ipn = ipn FROM @ipnTable;

DECLARE @secondaryQuery nvarchar(4000) =
'
select
	DistrictNumber
from
	openquery(INQUIRE,
''
select
	DistrictNumber
from
	PATDATA
where
	InternalPatientNumber = ''''' + @ipn + ''''''')';

declare @dnTable TABLE(dn varchar(9) NOT NULL);
insert @dnTable
exec (@secondaryQuery);

select TOP 1 @patientNumber = dn FROM @dnTable;
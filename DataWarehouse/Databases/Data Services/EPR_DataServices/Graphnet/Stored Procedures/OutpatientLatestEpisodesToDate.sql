﻿CREATE procedure [Graphnet].[OutpatientLatestEpisodesToDate]
	@NHSNumber varchar(12)

with execute as 'cmmc\jabran.rafiq'
as
--Oupatient appointments (A05)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;

select
	 Encounter.NHSNumber
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.DistrictNo
	,Encounter.SourceUniqueID
	,Encounter.DNACode
	,ClinicCode = Clinic.SourceClinicCode
	,Clinic = Clinic.SourceClinic
	,SiteCode = Site.SourceSiteCode
	,Site = Site.SourceSite
	,ConsultantCode = Consultant.SourceConsultantCode
	,Consultant = Consultant.SourceConsultant
	,SpecialtyCode = Specialty.SourceSpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,SourceOfReferralCode = SourceOfReferral.SourceSourceOfReferralCode
	,SourceOfReferral = SourceOfReferral.SourceSourceOfReferral
	,PurchaserCode = CCG.CCGCode
	,Purchaser = CCG.CCG
	,Encounter.AppointmentTime
	,ReasonForReferralCode = ReasonForReferral.SourceReasonForReferralCode
	,ReasonForReferral = ReasonForReferral.SourceReasonForReferral
	,Encounter.AppointmentCreateDate
	,Encounter.DischargeDate
	

	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceEncounterNo,null)	
from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.OP.Clinic
on	Clinic.SourceClinicID = Reference.ClinicID

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

inner join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.OP.ReasonForReferral
on	ReasonForReferral.SourceReasonForReferralID = Reference.ReasonForReferralID

inner join WarehouseOLAPMergedV2.WH.CCG
on	CCG.CCGID = Reference.AppointmentResidenceCCGID

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.AppointmentDateID
and	Calendar.TheDate between dateadd(year , -1 , getdate()) and dateadd(day , -1 , getdate())

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

where
	Encounter.IsWardAttender = '0' 
and	SourcePatientNo = @SourcePatientNo

--eliminate all appointments except the last appointment
and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.BaseEncounter LaterEncounter

	inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference LaterReference
	on	LaterReference.MergeEncounterRecno = LaterEncounter.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.WH.Calendar LaterCalendar
	on	LaterCalendar.DateID = LaterReference.AppointmentDateID
	and	LaterCalendar.TheDate between dateadd(year , -1 , getdate()) and dateadd(day , -1 , getdate())

	where
		LaterEncounter.SourcePatientNo = Encounter.SourcePatientNo
	and	LaterEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
	and	(
			LaterEncounter.AppointmentTime > Encounter.AppointmentTime
		or	(
				LaterEncounter.AppointmentTime = Encounter.AppointmentTime
			and	LaterEncounter.DoctorCode > Encounter.DoctorCode
			)
		)
	)


 
	union all
	
	select
	 Encounter.NHSNumber
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.DistrictNo
	,Encounter.SourceUniqueID
	,DNACode = null
	,ClinicCode = null 
	,Clinic = null 
	,SiteCode = Site.SourceSiteCode
	,Site = Site.SourceSite
	,ConsultantCode = Consultant.SourceConsultantCode
	,Consultant = Consultant.SourceConsultant
	,SpecialtyCode = Specialty.SourceSpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,SourceOfReferralCode = SourceOfReferral.SourceSourceOfReferralCode
	,SourceOfReferral = SourceOfReferral.SourceSourceOfReferral
	,PurchaserCode = CCG.CCGCode
	,Purchaser = CCG.CCG
	,AppointmentTime = null
	,ReasonForReferralCode = ReasonForReferral.SourceReasonForReferralCode
	,ReasonForReferral = ReasonForReferral.SourceReasonForReferral
	,AppointmentCreateDate = null
	,Encounter.DischargeDate
	
	
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceEncounterNo,null)
	
	from
	
		WarehouseOLAPMergedV2.RF.BaseEncounter Encounter
		
inner join WarehouseOLAPMergedV2.RF.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

inner join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.OP.ReasonForReferral
on	ReasonForReferral.SourceReasonForReferralID = Reference.ReasonForReferralID

left join WarehouseOLAPMergedV2.WH.CCG
on	CCG.CCGID = Reference.ResidenceCCGID

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.ReferralDateID
and	Calendar.TheDate between dateadd(year , -1 , getdate()) and dateadd(day , -1 , getdate())


inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

		
		where 
		
		Encounter.DischargeTime is not null
		
		and
		Encounter.SourcePatientNo = @SourcePatientNo
		
		and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.BaseEncounter LaterEncounter
	where
		LaterEncounter.SourcePatientNo = Encounter.SourcePatientNo
	and LaterEncounter.SourceEncounterNo = Encounter.SourceEncounterNo

	)

order by
	Encounter.AppointmentTime
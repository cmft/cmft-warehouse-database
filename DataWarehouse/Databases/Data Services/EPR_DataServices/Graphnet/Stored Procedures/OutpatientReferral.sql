﻿CREATE procedure [Graphnet].[OutpatientReferral]
	@NHSNumber varchar(12)

with execute as 'cmmc\jabran.rafiq'
as

--Outpatients Referrals(REF-I12)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;


select
	 Encounter.NHSNumber
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,SiteCode = Site.SourceSiteCode
	,Site = Site.SourceSite
	,Encounter.SourceUniqueID
	,ConsultantCode = Consultant.SourceConsultantCode
	,Consultant = Consultant.SourceConsultant
	,SpecialtyCode = Specialty.SourceSpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,SourceOfReferralCode = SourceOfReferral.SourceSourceOfReferralCode
	,SourceOfReferral = SourceOfReferral.SourceSourceOfReferral
	,ReferralType = SourceOfReferral.ReferralType
	--,PurchaserCode = CCG.CCGCode
	--,Purchaser = CCG.CCG
	,Encounter.ReferralDate
	,ReasonForReferralCode = ReasonForReferral.SourceReasonForReferralCode
	,ReasonForReferral = ReasonForReferral.SourceReasonForReferral
	,Encounter.DischargeDate
	,Encounter.DischargeTime
	,Encounter.SourceOfReferralGroupCode
	
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceEncounterNo,21)

from
	WarehouseOLAPMergedV2.RF.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.RF.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

inner join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.OP.ReasonForReferral
on	ReasonForReferral.SourceReasonForReferralID = Reference.ReasonForReferralID

--inner join WarehouseOLAPMergedV2.WH.CCG
--on	CCG.CCGID = Reference.ResidenceCCGID

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.ReferralDateID
and	Calendar.TheDate >= dateadd(year , -1 , getdate())

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

where
	Encounter.SourcePatientNo = @SourcePatientNo

order by
	Encounter.ReferralDate
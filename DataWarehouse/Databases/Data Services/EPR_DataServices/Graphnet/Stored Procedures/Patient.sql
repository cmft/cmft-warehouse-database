﻿CREATE procedure [Graphnet].[Patient]
	@NHSNumber varchar(12)

with execute as 'cmmc\jabran.rafiq'
as

--Inpatient preadmission (A05)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;

SELECT
	 Patient.SourcePatientNo
	,Patient.NHSNumber
	,Patient.NHSNumberStatusID
	,PatientTitle = Patient.Title
	,PatientForename = Patient.Forenames
	,PatientSurname = Patient.Surname
	,Patient.DateOfBirth
	,Patient.DateOfDeath
	,Patient.SexCode
	,Patient.NHSNumber
	,PatientAddress1 = Patient.AddressLine1
	,PatientAddress2 = Patient.AddressLine2
	,PatientAddress3 = Patient.AddressLine3
	,PatientAddress4 = Patient.AddressLine4
	,Patient.Postcode
	,Patient.EthnicOriginCode
	,Patient.MaritalStatusCode
	,Patient.ReligionCode

	,PATDATA.NoKAddrLine1
	,PATDATA.NoKAddrLine2
	,PATDATA.NoKAddrLine3
	,PATDATA.NoKAddrLine4
	,PATDATA.NoKHomePhone
	,PATDATA.NoKName
	,PATDATA.NoKPostCode
	,PATDATA.NoKRelationship
	,PATDATA.NoKWorkPhone
FROM
	Warehouse.PAS.Patient

left join PAS.Inquire.PATDATA
on	PATDATA.InternalPatientNumber = Patient.SourcePatientNo

where
	Patient.SourcePatientNo = @SourcePatientNo
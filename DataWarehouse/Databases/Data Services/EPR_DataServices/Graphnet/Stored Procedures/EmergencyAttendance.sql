﻿CREATE procedure [Graphnet].[EmergencyAttendance]
	@NHSNumber varchar(12)
	
with execute as 'cmmc\jabran.rafiq'
as

--Inpatient preadmission (A05)

declare
	 @DistrictNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetDistrictNumberFromNHSNumber @NHSNumber , @DistrictNo output
;


select
	 Encounter.NHSNumber
	,Encounter.SourceUniqueID
	,Encounter.AttendanceNumber
	,Site.SourceSiteCode
	,Site.SourceSite
	,Site.NationalSiteCode
	,Site.NationalSite
	,StaffMember.SourceStaffMemberCode
	,StaffMember.SourceStaffMember
	,DepartmentType.DepartmentTypeCode
	,DepartmentType.DepartmentType
	,CommissionerCode = CCG.CCGCode
	,Commissioner = CCG.CCG
	,AttendanceDisposalCode = AttendanceDisposal.NationalAttendanceDisposalCode
	,AttendanceDisposal = AttendanceDisposal.NationalAttendanceDisposal
	,Encounter.DischargeDestinationCode
	,DischargeDestination = DischargeDestination.Lkp_Name
	,Encounter.ArrivalTime
	,Encounter.ArrivalTimeAdjusted
	,Encounter.DepartureTime
	,Encounter.DepartureTimeAdjusted
	,PresentingComplaint = PresentingProblem.PresentingProblem
	,SourceOfReferralCode = SourceOfReferral.NationalSourceOfReferralCode
	,SourceOfReferral = SourceOfReferral.NationalSourceOfReferralCode
	,ArrivalModeCode = ArrivalMode.NationalArrivalModeCode
	,ArrivalMode = ArrivalMode.NationalArrivalMode
	,Diagnosis1Code = Diagnosis.SourceDiagnosisCode
	,Diagnosis1 = Diagnosis.SourceDiagnosis
	,ClinicianSeen = 	First_AEClinician.clinicianname
	
		

from
	WarehouseOLAPMergedV2.AE.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.Ae.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.EncounterStartDateID
and	Calendar.TheDate >= dateadd(year , -1 , getdate())

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.Location = 'Central'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = Reference.SiteID

inner join WarehouseOLAPMergedV2.AE.StaffMember
on	StaffMember.SourceStaffMemberID = Reference.StaffMemberID



inner join WarehouseOLAPMergedV2.AE.DepartmentType
on	DepartmentType.DepartmentTypeCode = Encounter.DepartmentTypeCode

inner join WarehouseOLAPMergedV2.WH.CCG
on	CCG.CCGCode = Encounter.CCGCode

inner join WarehouseOLAPMergedV2.AE.AttendanceDisposal
on	AttendanceDisposal.SourceAttendanceDisposalID = Reference.AttendanceDisposalID

inner join WarehouseOLAPMergedV2.AE.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.AE.ArrivalMode
on	ArrivalMode.SourceArrivalModeID = Reference.ArrivalModeID

inner join WarehouseOLAPMergedV2.AE.Diagnosis
on	Diagnosis.SourceDiagnosisID = Reference.DiagnosisFirstID

left join Warehouse.AE.LookupBase DischargeDestination
on	DischargeDestination.Lkp_ID = Encounter.DischargeDestinationCode

left join Warehouse.AE.PresentingProblem PresentingProblem
on PresentingProblem.PresentingProblemCode = Encounter.PresentingProblemCode

left join CMMC_Reports.dbo.First_AEClinician_View First_AEClinician
on First_AEClinician.res_atdid = Encounter.SourceUniqueID

where
	Encounter.DistrictNo = @DistrictNo

order by
	Encounter.ArrivalTime
﻿CREATE procedure [Graphnet].[InpatientPreadmission]
	@NHSNumber varchar(12)
	
with execute as 'cmmc\jabran.rafiq'
as

--Inpatient preadmission (A05)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;


select
	 NHSNumber = @NHSNumber
	,WL.ActivityTypeCode
	,WLActivityType.ActivityType
	,WL.SourcePatientNo
	,SourceSpellNo = WL.SourceEntityRecno
	,WL.TCITime
	,wl.ActivityTime
	,WL.ConsultantCode
	,Consultant = Consultant.SourceConsultant
	,WL.SpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	--,en.PurchaserCode
	,WL.WardCode
	,Ward.SourceWard
	,WL.SiteCode
	,Site.SourceSite

	,ProviderSpellNo = 
		case
			when Encounter.ProviderSpellNo is null then WL.SourcePatientNo +'/'+WL.SourceEntityRecno
			
			else ProviderSpellNo
		end	
		
	,Encounter.PatientCategoryCode

	,PatientCategory =
		case PatientCategoryCode
		when 'EL' then 'Elective'
		when 'DC' then 'Day Case'
		when 'RD' then 'Regular Day'
		when 'NE' then 'Non-Elective'
		end
	,ReferringConsultantCode = null
	,ReferringConsultant = null

	,SourceAdmissionSourceCode = AdmissionSource.SourceAdmissionSourceCode
	,SourceAdmissionSource = AdmissionSource.SourceAdmissionSource
	,NationalAdmissionSourceCode = AdmissionSource.NationalAdmissionSourceCode
	,NationalAdmissionSource = AdmissionSource.NationalAdmissionSource

	,Encounter.Ambulatory
	,Encounter.ReferrerCode
	,Encounter.ExpectedLOS
	,Encounter.ExpectedDateofDischarge
	
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,wl.SourceEntityRecno,7)	

from
	Warehouse.APC.WaitingListActivity WL

left join WarehouseOLAPMergedV2.APC.BaseEncounter Encounter
on	Encounter.SourcePatientNo = WL.SourcePatientNo
and	Encounter.SourceSpellNo = WL.SourceEntityRecno
and	Encounter.FirstEpisodeInSpellIndicator = 1
and	Encounter.ContextCode = 'CEN||PAS'

left join WarehouseOLAPMergedV2.APC.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.AdmissionSource
on	AdmissionSource.SourceAdmissionSourceID = Reference.AdmissionSourceID

left join Warehouse.WH.WLActivityType
on	WLActivityType.ActivityTypeCode = WL.ActivityTypeCode

left join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantCode = WL.ConsultantCode
and	Consultant.SourceContextCode = 'CEN||PAS'

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyCode = WL.SpecialtyCode
and	Specialty.SourceContextCode = 'CEN||PAS'

left join WarehouseOLAPMergedV2.WH.Ward
on	Ward.SourceWardCode = WL.WardCode
and	Ward.SourceContextCode = 'CEN||PAS'

left join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteCode = WL.SiteCode
and	Site.SourceContextCode = 'CEN||PAS'

where
	WL.ActivityTypeCode = 5
and	WL.SourcePatientNo = @SourcePatientNo
and	WL.ActivityDate >= dateadd(year , -1 , getdate())

order by
	WL.ActivityTime
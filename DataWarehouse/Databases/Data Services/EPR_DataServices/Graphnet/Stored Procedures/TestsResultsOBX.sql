﻿CREATE procedure [Graphnet].[TestsResultsOBX]
		 @InvestigationID int = null

--Results Segment for Orders Mseesage

with execute as 'cmmc\jabran.rafiq'
as

set dateformat dmy


select 


 ResultID = Results.Result_Index


,InvestigationID  = Results.Investigation_Index


,DataType = null --(Text/Numeric)
,TestCode = Results.Result_Code
,TestDescription = Result_Rubric
,Results.Result 
,UnitOfMeasurement = 
	case 
		when Results.UOM_Text = ' ' then Results.UOM_Code 
		else nullif(Results.UOM_Text,'')
	end 
,AbnormalIndicator = Results.Abnormal_Flag
,ReferenceRange = convert(varchar (50),Lower_Range) + ' - ' + convert(varchar (50),Upper_Range)
,ResultStatus = ReportSummary.Report_Status
,ResultDateTime =    CONVERT(VARCHAR(10),DateTime_Of_Report,112) +  REPLACE(CONVERT(VARCHAR(10),DateTime_Of_Report,108),':','') --(YYYMMDDHHMMSS)
,SampleRef = ReportSamples.Sample_Number 
,ResultComment = ResultComments.Comment



from

ICE_Central.dbo.ICEView_ReportSummary ReportSummary

inner join ICE_Central.dbo.ICEView_ReportInvestigations ReportInvestigations
on ReportInvestigations.Service_Report_Index = ReportSummary.Service_Report_Index

--inner join ICE_Central.dbo.ICEView_ReportResults Results
--on Results.Investigation_Index = ReportInvestigations.Investigation_Index


inner join ICE_Central.dbo.service_results Results
on Results.investigation_index = ReportInvestigations.Investigation_Index 
--and ReportResults.Sample_Index = ReportInvestigations.Sample_Index

left join [ICE_Central].[dbo].[ICEView_ReportSamples] ReportSamples
on ReportSamples.Service_Report_Index = ReportInvestigations.Service_Report_Index
and ReportSamples.Sample_Index = ReportInvestigations.Sample_Index

left join [ICE_Central].dbo.Service_Ranges ReferenceRanges
on ReferenceRanges.Result_Index = Results.Result_Index



	left join 
			(
			select 
				ResultComments.Result_Index
				,Comment = 				
					replace(
						(
						select
							stuff((
								select
									'--' + ltrim(rtrim(ResultCommentsText.Comment)) 
								from
									ICE_Central.dbo.ICEView_ResultComments ResultCommentsText
								where
									ResultCommentsText.Result_Index = ResultComments.Result_Index
								for xml path('') 
								), 1, 2, '')
						) 
					,'&amp', '&'
					)
			from
				ICE_Central.dbo.ICEView_ResultComments ResultComments

			group by
				ResultComments.Result_Index
			) ResultComments
	on	Results.Result_Index = ResultComments.Result_Index


where 
Results.Investigation_Index = @InvestigationID
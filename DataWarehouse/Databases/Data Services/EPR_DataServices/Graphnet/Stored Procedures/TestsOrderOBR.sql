﻿CREATE procedure [Graphnet].[TestsOrderOBR]
		
		 @NHSNumber varchar(12)

--Results Segment for Orders Mseesage

with execute as 'cmmc\jabran.rafiq'
as

set dateformat dmy

declare
	 @patientNumber varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetDistrictNumberFromNHSNumber @NHSNumber , @patientNumber output
;




 select 
 
 ReportID  = Service_Request_Index
,InvestigationID = ReportInvestigations.Investigation_Index 
,SampleReference = ReportSamples.Sample_Number 
,ServiceCode = Investigation_Code
,ServiceName = Investigation_Name

,ReportDatetime =  CONVERT(VARCHAR(10),DateTime_Of_Report,112) +  REPLACE(CONVERT(VARCHAR(10),DateTime_Of_Report,108),':','') --(YYYMMDDHHMMSS)
,SampleCollectionDateTime = CONVERT(VARCHAR(10),Collection_DateTime,112) +  REPLACE(CONVERT(VARCHAR(10),Collection_DateTime,108),':','') --(YYYMMDDHHMMSS)
,SampleTypeCode = ReportSamples.Sample_Code 
,SampleType = ReportSamples.Sample_Text
,OrderedByCode = ICEView_ConsultantSummary.National_Code

,ClinicianSurname = ICEView_ConsultantSummary.Surname
,ClinicianForename = ICEView_ConsultantSummary.Forename
,ClinicianTitle = ICEView_ConsultantSummary.Title
,ClinicanNationalCode = case when left(ICEView_ConsultantSummary.National_Code,1) = 'C' then ICEView_ConsultantSummary.National_Code else ' ' end
,ClinicianSpecialty = ICEView_ConsultantSummary.Specialty_Code
,ClinicianSpecialtyDesc  = ICEView_ConsultantSummary.Specialty_Description


,ReportedByCode = null
,ReportedBy = null
,SpecialtyCode = ReportSummary.Specialty
,Specialty = Specialty.Specialty
,ReportStatus = ReportSummary.Report_Status
,LocationCode = ICEView_LocationSummary.National_Code
,Location = ICEView_LocationSummary.Name
,LocationType = ICEView_LocationSummary.Type
,InvestigationComments = Comment
,HasOrder = 

	case	
	when exists 
	(
	select 1 
	from 	
	ICE_Central.dbo.ICEView_OrderSummary ICEView_OrderSummary 
	
	where 
	ICEView_OrderSummary.Service_Request_Index = ReportSummary.Service_Request_Index

	) then 1 
	else 0 end


from
ICE_Central.dbo.ICEView_ReportSummary ReportSummary

inner join ICE_Central.dbo.ICEView_ReportInvestigations ReportInvestigations
on ReportInvestigations.Service_Report_Index = ReportSummary.Service_Report_Index

inner join [ICE_Central].[dbo].[ICEView_ReportSamples] ReportSamples
on ReportSamples.Service_Report_Index = ReportInvestigations.Service_Report_Index
and ReportSamples.Sample_Index = ReportInvestigations.Sample_Index


left join [ICE_Central].dbo.Specialty Specialty
on Specialty.Specialty_Code = ReportSummary.Specialty

left join ICE_Central.dbo.ICEView_LocationSummary ICEView_LocationSummary
on ICEView_LocationSummary.Location_Index = ReportSummary.Location_Index

left join ICE_Central.dbo.ICEView_ConsultantSummary  ICEView_ConsultantSummary
on ICEView_ConsultantSummary.Clinician_Index = ReportSummary.Clinician_Index  

left join 
			(
			select 
				InvestigationComments.Investigation_Index
				,Comment = 				
					replace(
						(
						select
							stuff((
								select
									'--' + ltrim(rtrim(InvestigationCommentsText.Comment)) 
								from
									ICE_Central.dbo.ICEView_InvestigationComments InvestigationCommentsText
								where
									InvestigationCommentsText.Investigation_Index = InvestigationComments.Investigation_Index
								for xml path('') 
								), 1, 2, '')
						) 
					,'&amp;', '&'
					)
					
			from
				ICE_Central.dbo.ICEView_InvestigationComments InvestigationComments 

			group by
				InvestigationComments.Investigation_Index
			) InvestigationComments
	on	InvestigationComments.Investigation_Index = ReportInvestigations.Investigation_Index

WHERE 

HospitalNumber = @patientNumber
--and
--Service_Request_Index = '40002978'
and
DateTime_Of_Report between dateadd(yy, datediff(yy, 0, GETDATE ()) - 1, 0) and GETDATE ()
--based on rpeortd ID's prsented and should give a list of Reult ID's

and

Hidden_Reason_Code is null
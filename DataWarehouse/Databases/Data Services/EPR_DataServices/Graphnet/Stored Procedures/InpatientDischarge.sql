﻿CREATE procedure [Graphnet].[InpatientDischarge]
	@NHSNumber varchar(12)
	
with execute as 'cmmc\jabran.rafiq'
as

--InPatient Discharges (A03)

declare
	 @SourcePatientNo varchar(9)
;

set
	@NHSNumber = replace(@NHSNumber , ' ' , '')
;

exec Graphnet.GetInternalPatientNumberFromNHSNumber @NHSNumber , @SourcePatientNo output
;

select
	 Encounter.NHSNumber
	--,SourcePatientNo
	--,DistrictNo
	,SourceUniqueID

	,EndSiteCode = Site.SourceSiteCode
	,EndSite = Site.SourceSite
	,EndWardCode = EndWard.SourceWardCode
	,EndWard = EndWard.SourceWard

	,RegisteredGpPracticeCode
	,RegisteredGpCode

	,Encounter.PatientCategoryCode

	,PatientCategory =
		case PatientCategoryCode
		when 'EL' then 'Elective'
		when 'DC' then 'Day Case'
		when 'RD' then 'Regular Day'
		when 'NE' then 'Non-Elective'
		end

	,Encounter.ProviderSpellNo
	,Encounter.Ambulatory

	,Encounter.AdmissionTime
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,Encounter.DischargeTime

	,SourceAdmissionSourceCode = AdmissionSource.SourceAdmissionSourceCode
	,SourceAdmissionSource = AdmissionSource.SourceAdmissionSource
	,NationalAdmissionSourceCode = AdmissionSource.NationalAdmissionSourceCode
	,NationalAdmissionSource = AdmissionSource.NationalAdmissionSource

	,SourceDischargeMethodCode = DischargeMethod.SourceDischargeMethodCode
	,SourceDischargeMethod = DischargeMethod.SourceDischargeMethod
	,NationalDischargeMethodCode = DischargeMethod.NationalDischargeMethodCode
	,NationalDischargeMethod = DischargeMethod.NationalDischargeMethod

	,SourceDischargeDestinationCode = DischargeDestination.SourceDischargeDestinationCode
	,SourceDischargeDestination = DischargeDestination.SourceDischargeDestination
	,NationalDischargeDestinationCode = DischargeDestination.NationalDischargeDestinationCode
	,NationalDischargeDestination = DischargeDestination.NationalDischargeDestination

	,Encounter.ExpectedDateofDischarge
	,Encounter.ExpectedLOS
	,Encounter.LOS
	,Encounter.ReferrerCode

	,ConsultantCode = Consultant.SourceConsultantCode
	,Consultant = Consultant.SourceConsultant

	,ReferringConsultantCode = null
	,ReferringConsultant = null

	,SpecialtyCode = Specialty.SourceSpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,PurchaserCode = CCG.CCGCode
	,Purchaser = CCG.CCG
	
	
	,CreateDate = dbo.CreatedDate(Encounter.DistrictNo,Encounter.SourceSpellNo,5)	
from
	WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Reference.DischargeDateID
and	Calendar.TheDate >= dateadd(year , -1 , getdate())

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Reference.ContextID
and	Context.ContextCode = 'CEN||PAS'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

inner join WarehouseOLAPMergedV2.WH.CCG
on	CCG.CCGID = Reference.CCGID

inner join WarehouseOLAPMergedV2.APC.Ward EndWard
on	EndWard.SourceWardID = Reference.EndWardID

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceSiteID = EndWard.SourceSiteID

inner join WarehouseOLAPMergedV2.APC.AdmissionSource
on	AdmissionSource.SourceAdmissionSourceID = Reference.AdmissionSourceID

inner join WarehouseOLAPMergedV2.APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = Reference.AdmissionMethodID

inner join WarehouseOLAPMergedV2.APC.DischargeMethod
on	DischargeMethod.SourceDischargeMethodID = Reference.DischargeMethodID

inner join WarehouseOLAPMergedV2.APC.DischargeDestination
on	DischargeDestination.SourceDischargeDestinationID = Reference.DischargeDestinationID

where
	SourcePatientNo = @SourcePatientNo
and Encounter.LastEpisodeInSpellIndicator = 'Y'

order by
	Encounter.DischargeTime
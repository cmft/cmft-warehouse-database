﻿
USE [master]
GO

/****** Object:  Database [EPR_DataServices]    Script Date: 24/08/2015 15:12:58 ******/
CREATE DATABASE [EPR_DataServices] ON  PRIMARY 
( NAME = N'EPR_DataServices', FILENAME = N'F:\Data\EPR_DataServices.mdf' , SIZE = 20480KB , MAXSIZE = UNLIMITED, FILEGROWTH = 51200KB )
 LOG ON 
( NAME = N'EPR_DataServices_log', FILENAME = N'G:\Log\EPR_DataServices_log.ldf' , SIZE = 11264KB , MAXSIZE = 2048GB , FILEGROWTH = 51200KB )
GO

ALTER DATABASE [EPR_DataServices] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EPR_DataServices].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [EPR_DataServices] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [EPR_DataServices] SET ANSI_NULLS OFF
GO

ALTER DATABASE [EPR_DataServices] SET ANSI_PADDING OFF
GO

ALTER DATABASE [EPR_DataServices] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [EPR_DataServices] SET ARITHABORT OFF
GO

ALTER DATABASE [EPR_DataServices] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [EPR_DataServices] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [EPR_DataServices] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [EPR_DataServices] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [EPR_DataServices] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [EPR_DataServices] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [EPR_DataServices] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [EPR_DataServices] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [EPR_DataServices] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [EPR_DataServices] SET  DISABLE_BROKER
GO

ALTER DATABASE [EPR_DataServices] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [EPR_DataServices] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [EPR_DataServices] SET TRUSTWORTHY ON
GO

ALTER DATABASE [EPR_DataServices] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [EPR_DataServices] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [EPR_DataServices] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [EPR_DataServices] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [EPR_DataServices] SET RECOVERY SIMPLE
GO

ALTER DATABASE [EPR_DataServices] SET  MULTI_USER
GO

ALTER DATABASE [EPR_DataServices] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [EPR_DataServices] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'EPR_DataServices', N'ON'
GO

USE [EPR_DataServices]
GO

/****** Object:  UserDefinedFunction [dbo].[CreatedDate]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [APC].[PreviousAdmission]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Chameleon].[GetAEEncounters]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Chameleon].[GetInpatientEncounters]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[CWSOrder]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[CWSResultNumeric]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[CWSResultText]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[EmergencyAttendance]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[EmergencyDischarge]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[GetDistrictNumberFromNHSNumber]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[GetInternalPatientNumberFromNHSNumber]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[InpatientAdmission]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[InpatientConsultantTransfers]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[InpatientDischarge]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[InpatientPreadmission]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[InpatientPreadmissionCancel]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[InpatientTransfer]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[OutpatientAppointment]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[OutpatientAppointmentAttended]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[OutpatientAppointmentCancelled]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[OutpatientDischarge]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[OutpatientLatestAppointmentToDate]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[OutpatientLatestEpisodesToDate]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[OutpatientReferral]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[Patient]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[TestsOrderOBR]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Graphnet].[TestsResultsOBX]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Symphony].[AEEnrichment]    Script Date: 24/08/2015 15:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [EPR_DataServices] SET  READ_WRITE
GO

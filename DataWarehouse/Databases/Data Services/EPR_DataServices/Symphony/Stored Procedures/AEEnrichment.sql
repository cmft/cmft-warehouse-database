﻿CREATE proc [Symphony].[AEEnrichment] 
(
	@AENumber nvarchar(15) -- e.g. AAA-22-111111-0
)

with execute as 'cmmc\jabran.rafiq'
as
begin
--20141001 - Added Discharge Outcome
--         - Added Discharge Destination

set xact_abort on;
set nocount on;

--declare @AENumber varchar(16) = 'MAE-14-065430-1'
declare @sql nvarchar(max) = N'select * from openquery(CMFTED1,''
with AttendanceReasons as (
  select lkp_id,Lkp_name from cmmc_master.dbo.lookups
  where Lkp_ParentID = 7675
)

select top 1 att.atd_id, Date_Registered,Arrival_Date, Source_Of_Referral, PlaceOfIncident, c.complaint PresentingComplaint,clin.clinicianname_Staff ClinicianSeen ,[Date_Discharged],att.Elapsed_Time TimeOfIncident, det.atd_patcomplaint PatientPresentsWith,n.not_text LocationDetails,AttendanceReasons.Lkp_Name AttendanceReason, disch.lkp_name DischargeOutcome,
  dischdest.Lkp_Name DischargeDestination
from cmmc_master.dbo.Attendance_View att
left join cmmc_master.dbo.Complaint_view c
on att.atd_id = c.atd_id 
left join cmmc_master.dbo.First_AEClinician_View clin
on att.atd_id = clin.res_atdid 
left join cmmc_master.dbo.attendance_details det 
on att.atd_id = det.atd_id
left join cmmc_master.dbo.notes n
on det.atd_locnotes = n.not_noteid 
left join attendancereasons 
on det.atd_reason = AttendanceReasons.Lkp_ID 
left join cmmc_master.dbo.discharge_view disch
  on disch.atd_id = att.atd_id
left join cmmc_master.dbo.lookups DischDest
  on DischDest.Lkp_ID = atd_dischdest 

where att.attendance_no = ''''' + @AENumber +N''''''')';

--print @sql

exec sp_executesql @sql


end;
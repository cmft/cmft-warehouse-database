﻿CREATE function [dbo].[CreatedDate]

(
	 @DistrictNo varchar (20)
	,@SourceSpellNo varchar (20)
	,@ColumnID int 

)

returns datetime



as
begin

	declare @Result datetime
	

		select top 1 @Result = TransactionTime
		from WarehouseOLAPMergedV2.TransactionLog.Base TransactionLog
		
		where cast(TransactionLog.DistrictNo as int) = cast(@DistrictNo as int) 
		and TransactionLog.SourceSpellNo = @SourceSpellNo
			and 
			(@ColumnID is null or @ColumnID = ColumnID)
		
		order by TransactionTime asc
		
		return @Result
		
		end
﻿ALTER ROLE [db_datareader] ADD MEMBER [scc]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Sec - Warehouse BI Developers]
GO
ALTER ROLE [db_owner] ADD MEMBER [CMMC\paul.egan]
GO
ALTER ROLE [db_owner] ADD MEMBER [CMMC\data.warehouse]
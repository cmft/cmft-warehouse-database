﻿-- ===============================================
-- Author:		Paul Gales
-- Create date: 11 Nov 2013
-- Description:	Returns a patients A&E encounters
-- ===============================================
CREATE PROCEDURE [Chameleon].[GetAEEncounters] 
	@DistrictNo VARCHAR(10)
	
with execute as 'cmmc\jabran.rafiq'
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT  E.MergeEncounterRecno,
			E.ArrivalTime,
			E.DepartureTime,
			E.DiagnosisCodeFirst,
			D.LocalDiagnosis
	FROM	WarehouseReportingMerged.AE.Encounter E
			-- Only return the first diagnosis
			LEFT OUTER JOIN WarehouseReportingMerged.AE.DiagnosisLookUp D ON D.LocalDiagnosisCode = E.DiagnosisCodeFirst AND D.SourceContextCode = E.ContextCode
	WHERE	E.DistrictNo = @DistrictNo	
END
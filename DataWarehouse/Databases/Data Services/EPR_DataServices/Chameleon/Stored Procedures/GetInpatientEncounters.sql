﻿-- =========================================================================
-- Author:		Paul Gales
-- Create date: 11 Nov 2013
-- Description:	Returns a patient inpatient encounter details
--				Only returns the last consultant episode for each encounter
-- =========================================================================
CREATE PROCEDURE [Chameleon].[GetInpatientEncounters]
	@PMINo INT = 0
	
with execute as 'cmmc\jabran.rafiq'
AS
BEGIN

	SELECT	E.MergeEncounterRecno,
			E.AdmissionTime,
			E.DischargeTime,		
			E.ConsultantCode,
			E.SpecialtyCode,
			E.PrimaryDiagnosisCode,
			E.PrimaryProcedureCode,
			D.Diagnosis,
			O.Operation
	FROM WarehouseReportingMerged.APC.Encounter E
			-- Only return the primary diagnosis and operation codes
			LEFT OUTER JOIN WarehouseReportingMerged.WH.Diagnosis D ON D.DiagnosisCode = E.PrimaryDiagnosisCode
			LEFT OUTER JOIN WarehouseReportingMerged.WH.Operation O ON O.OperationCode = E.PrimaryProcedureCode
	WHERE	E.DistrictNo = @PMINo AND
			--E.LastEpisodeInSpellIndicator = 'Y'
			E.NationalLastEpisodeInSpellIndicator = '1'
	ORDER BY AdmissionTime

END
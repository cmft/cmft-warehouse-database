﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <12-07-2012>
-- Description:	<Returns a list of patient procedures by NHS Number>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_Procedures] 

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[Procedure Type],
		[Procedure Code],
		[Procedure Date],
		[Procedure Date] as EventDate
	FROM
		tPooB_Procedures
	WHERE
		[New NHS Number] = @NHSNumber AND
		LEN([Procedure Code]) > 0
END
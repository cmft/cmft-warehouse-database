﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <20-06-2012>
-- Description:	<Returns patient critical care record>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_CriticalCare] 
	
	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[Critical Care Start Date 1],
		[Critical Care Discharge date 1],
		[Advanced Respiratory Support Days 1],
		[Basic Respiratory Support Days 1],
		[Advanced Cardiovascular Support Days 1],
		[Basic Cardiovascular Support Days 1],
		[Renal Support Days 1],
		[Neurological Support Days 1],
		[Gastro Intestinal Support Days 1],
		[Dermatological Support Days 1],
		[Liver Support Days 1],
		[Critical Care level 2 Days 1],
		[Critical Care level 3 Days 1],
		[consultant code],
		[specialty function code],
		[Critical Care Start Date 1] as EventDate,
        --[Provider Organisation Code] AS ProviderCode,
        --[Site Code of Treatment] AS SiteCode,
        '' as Priority,
        'Critical Care Stay' as EpisodeType
	
	FROM tPooB_CriticalCare WHERE [New NHS Number] = @NHSNumber


END
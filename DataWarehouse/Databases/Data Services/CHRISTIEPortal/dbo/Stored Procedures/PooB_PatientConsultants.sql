﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <20-06-2012>
-- Description:	<Returns a list of patient consultants>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_PatientConsultants]

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT     
		[Referral Request Received Date], 
		[Discharge Date], 
		[Consultant code], 
        [Specialty Function Code]
FROM    
		tPooB_Clinicians
WHERE
		[NHS Number] = @NHSNumber
END
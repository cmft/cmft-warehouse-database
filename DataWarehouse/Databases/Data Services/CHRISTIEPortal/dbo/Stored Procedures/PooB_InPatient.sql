﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <06-06-12>
-- Description:	<Returns InPatient CDS data for patient based on NHS number>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_InPatient]

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

SELECT     
	[Provider spell start date], 
	[Provider spell end date], 
	[Consultant Code], 
	[Specialty Function Code], 
	[Intended Management],
	[Primary Diagnosis ICD],
	--[Provider Organisation Code] as ProviderCode,
	[Site Code of Treatment] as SiteCode,
	'' AS Priority,
	[Provider spell start date] as EventDate
	
FROM         
	tPooB_InPatient
WHERE
	[New NHS Number] = @NHSNumber
END
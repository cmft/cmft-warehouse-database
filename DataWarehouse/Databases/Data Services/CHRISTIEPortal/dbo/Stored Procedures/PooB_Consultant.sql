﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <15-06-2012>
-- Description:	<Returns a list of organisations consultants>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_Consultant] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		GMC,
		ConsultantName,
		SpecialtyCode
	FROM tPooB_Consultants

END
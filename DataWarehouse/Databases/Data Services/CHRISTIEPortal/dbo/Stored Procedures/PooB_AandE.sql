﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <20-07-12>
-- Description:	<Returns A&E patient events from CDS>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_AandE] 

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	--SELECT 
	--	AAndEArrivalMode,
	--	AAndEAttendanceCategory,
	--	AAndEAttendanceDisposal,
	--	AAndEIncidentLocationType,
	--	AAndEPatientGroup,
	--	SourceOfReferralForAAndE,
	--	ArrivalDate as EventDate,
	--	PrimaryDiagnosis_AAndE,
	--	PrimaryTreatment_AAndE,
 --       --[Provider Organisation Code] AS ProviderCode,
 --       --[Site Code of Treatment] AS SiteCode,
	--	'A&E' as Specialty,
	--	'A&E' as EpisodeType,
	--	'' as Priority
	--FROM
	--	CDS-A&E-DATA-TABLE
END
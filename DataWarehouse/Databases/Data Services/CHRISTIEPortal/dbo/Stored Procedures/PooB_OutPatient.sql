﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <15-06-2012>
-- Description:	<Returns patients outpatient CDS data>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_OutPatient]

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT     
		[Administrative Category], 
		[Attended or did not attend], 
		[Medical Staff Type seeing patient], 
		[First Attendance],
		[Attendance Date], 
		[Consultant code], 
        [Specialty Function Code], 
        [Priority Type],
        [ICD10 Code 1],
        [Attendance Date] AS EventDate,
        [Priority Type] AS Priority,
        --[Provider Organisation Code] AS ProviderCode,
        [Site Code of Treatment] AS SiteCode
FROM    
		tPooB_OutPatient
WHERE
		[NHS Number] = @NHSNumber
END
﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <08-11-12>
-- Description:	<Returns diagnosis data for patient>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_Diagnosis] 

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM tPooB_Diagnosis
	WHERE [NHS Number] = @NHSNumber

END
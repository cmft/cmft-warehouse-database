﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <15-05-2012>
-- Description:	<Returns patient details by NHS number>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_Demographics]

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		HospitalID,
		DateOfBirth,
		DateOfDeath,
		EthnicGroupCode,
		FirstName,
		GPPracticeName,
		NHSNumber,
		NHSNumberStatus,
		PermanentAddress,
		PermanentPostCode,
		Gender,
		Surname,
		GPPracticeCode,
		MaritalStatus,
		PatientTitle
	FROM 
		tPooB_Demographics 
	WHERE 
		[NHSNumber] = @NHSNumber
END
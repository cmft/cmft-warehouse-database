﻿-- =============================================
-- Author:		<David Geran>
-- Create date: <08-11-12>
-- Description:	<Returns waiting list data for patient>
-- =============================================
CREATE PROCEDURE [dbo].[PooB_WaitingList] 

	@NHSNumber varchar(10)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM tPooB_WaitingList
	WHERE [NHS Number] = @NHSNumber

END
﻿CREATE TABLE [dbo].[tPooB_Diagnosis](
	[Local Patient Identifier] [varchar](20) NULL,
	[New NHS Number Status] [varchar](2) NULL,
	[NHS Number] [varchar](10) NULL,
	[Diagnosis Applicable Date] [datetime] NULL,
	[Diagnosis Code] [varchar](20) NULL
) ON [PRIMARY]
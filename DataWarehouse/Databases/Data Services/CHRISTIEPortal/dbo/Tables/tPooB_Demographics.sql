﻿CREATE TABLE [dbo].[tPooB_Demographics](
	[HospitalID] [char](10) NULL,
	[DateOfBirth] [datetime] NULL,
	[DateOfDeath] [datetime] NULL,
	[EthnicGroupCode] [varchar](10) NULL,
	[FirstName] [char](20) NULL,
	[GPPracticeName] [varchar](50) NULL,
	[NHSNumber] [varchar](10) NULL,
	[NHSNumberStatus] [varchar](1) NOT NULL,
	[PermanentAddress] [varchar](8000) NULL,
	[PermanentPostCode] [char](8) NULL,
	[Gender] [varchar](10) NULL,
	[Surname] [char](20) NULL,
	[GPPracticeCode] [varchar](6) NULL,
	[MaritalStatus] [varchar](10) NULL,
	[PatientTitle] [varchar](10) NULL
) ON [PRIMARY]
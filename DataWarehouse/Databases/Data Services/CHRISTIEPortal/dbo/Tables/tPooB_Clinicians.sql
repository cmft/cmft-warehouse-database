﻿CREATE TABLE [dbo].[tPooB_Clinicians](
	[source of referral] [varchar](20) NULL,
	[Local Patient Identifier] [varchar](20) NULL,
	[New NHS Number Status] [varchar](2) NULL,
	[New NHS Number] [varchar](10) NULL,
	[NHS number type] [varchar](1) NULL,
	[NHS Number] [varchar](10) NULL,
	[Consultant code] [varchar](8) NULL,
	[Specialty Function Code] [varchar](3) NULL,
	[Consultant Specialty Function Code] [varchar](3) NULL,
	[Discharge Date] [datetime] NULL,
	[referral request received date] [datetime] NULL
) ON [PRIMARY]
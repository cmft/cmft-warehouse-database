﻿CREATE TABLE [dbo].[tPooB_Procedures](
	[Local Patient Identifier] [varchar](20) NULL,
	[New NHS Number Status] [varchar](2) NULL,
	[New NHS Number] [varchar](10) NULL,
	[Procedure Type] [varchar](10) NOT NULL,
	[Procedure Code] [varchar](20) NULL,
	[Procedure Date] [datetime] NULL
) ON [PRIMARY]
﻿CREATE TABLE [CRP].[Audit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditStart] [datetime] NULL,
	[AuditEnd] [datetime] NULL,
	[StartDateParameter] [datetime] NULL,
	[EndDateParameter] [datetime] NULL
) ON [PRIMARY]
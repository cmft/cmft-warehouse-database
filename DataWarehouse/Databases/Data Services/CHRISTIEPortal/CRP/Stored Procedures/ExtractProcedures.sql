﻿CREATE  Procedure [CRP].[ExtractProcedures] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractProcedures
	**  Purpose: 
	**
	**  Extract proc for Christie Portal Procedures activity data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 04.02.13    MH         Initial coding
	******************************************************************************/

	TRUNCATE TABLE tPooB_Procedures

	INSERT INTO
		tPooB_Procedures
		(
			 [Local Patient Identifier]
			,[New NHS Number Status]
			,[New NHS Number] 
			,[Procedure Type] 
			,[Procedure Code] 
			,[Procedure Date] 
		)

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[Primary Procedure] as [Procedure Code],
		[Primary Procedure Date] as [Procedure Date]
		from dbo.tPooB_OutPatient
		where [Primary Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[2nd Procedure],
		[2nd Procedure Date]
		from dbo.tPooB_OutPatient
		where [2nd Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[3rd Procedure],
		[3rd Procedure Date]
		from dbo.tPooB_OutPatient
		where [3rd Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[4th Procedure],
		[4th Procedure Date]
		from dbo.tPooB_OutPatient
		where [4th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[5th Procedure],
		[5th Procedure Date]
		from dbo.tPooB_OutPatient
		where [5th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[6th Procedure],
		[6th Procedure Date]
		from dbo.tPooB_OutPatient
		where [6th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[7th Procedure],
		[7th Procedure Date]
		from dbo.tPooB_OutPatient
		where [7th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[8th Procedure],
		[8th Procedure Date]
		from dbo.tPooB_OutPatient
		where [8th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[9th Procedure],
		[9th Procedure Date]
		from dbo.tPooB_OutPatient
		where [9th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[10th Procedure],
		[10th Procedure Date]
		from dbo.tPooB_OutPatient
		where [10th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[11th Procedure],
		[11th Procedure Date]
		from dbo.tPooB_OutPatient
		where [11th Procedure] is not null

		union all

		select [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		'Outpatient' AS [Procedure Type],
		[12th Procedure],
		[12th Procedure Date]
		from dbo.tPooB_OutPatient
		where [12th Procedure] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[Primary Procedure OPCS],
		[Primary Op Procedure Date]
		from dbo.tPooB_InPatient
		where [Primary Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[Secondary Procedure OPCS],
		[Secondary Op Procedure Date]
		from dbo.tPooB_InPatient
		where [Secondary Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[3rd Procedure OPCS],
		[3rd Op Procedure Date]
		from dbo.tPooB_InPatient
		where [3rd Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[4th Procedure OPCS],
		[4th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [4th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[5th Procedure OPCS],
		[5th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [5th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[6th Procedure OPCS],
		[6th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [6th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[7th Procedure OPCS],
		[7th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [7th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[8th Procedure OPCS],
		[8th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [8th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[9th Procedure OPCS],
		[9th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [9th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[10th Procedure OPCS],
		[10th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [10th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[11th Procedure OPCS],
		[11th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [11th Procedure OPCS] is not null

		union all

		select [Local Patient Identifier] ,
		[New NHS Number Status],
		[New NHS Number],
		'Inpatient' AS [Procedure Type],
		[12th Procedure OPCS],
		[12th Op Procedure Date]
		from dbo.tPooB_InPatient
		where [12th Procedure OPCS] is not null
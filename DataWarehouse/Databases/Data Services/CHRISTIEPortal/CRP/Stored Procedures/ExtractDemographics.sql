﻿CREATE  Procedure [CRP].[ExtractDemographics] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractDemographics
	**  Purpose: 
	**
	**  Extract proc for Christie Portal Patient details 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 01.02.13    MH         Initial coding
	******************************************************************************/


	TRUNCATE TABLE tPooB_Demographics

	DECLARE @CMFTContext VARCHAR(8) = 'CEN||PAS'
	DECLARE @TraffordContext VARCHAR(8) = 'TRA||UG'

	INSERT INTO tPooB_Demographics
	(
		 [HospitalID] 
		,[DateOfBirth] 
		,[DateOfDeath] 
		,[EthnicGroupCode] 
		,[FirstName] 
		,[GPPracticeName] 
		,[NHSNumber] 
		,[NHSNumberStatus] 
		,[PermanentAddress] 
		,[PermanentPostCode] 
		,[Gender] 
		,[Surname] 
		,[GPPracticeCode] 
		,[MaritalStatus] 
		,[PatientTitle] 
	)
	SELECT
		 [HospitalID]					= PATLIST.DistrictNo
		,[DateOfBirth] 					= COALESCE(CPAT.[DateOfBirth], TPAT.[DateOfBirth])
		,[DateOfDeath] 					= COALESCE(CPAT.[DateOfDeath], TPAT.[DateOfDeath])
		,[EthnicGroupCode] 				= LEFT(COALESCE(CPAT.[EthnicGroupCode], TPAT.[EthnicGroupCode]), 10)
		,[FirstName] 					= LEFT(COALESCE(CPAT.[FirstName], TPAT.[FirstName]), 20)
		,[GPPracticeName] 				= LEFT(COALESCE(CPAT.[GPPracticeName], TPAT.[GPPracticeName]), 50)
		,[NHSNumber] 					= LEFT(COALESCE(CPAT.[NHSNumber], TPAT.[NHSNumber]), 10)
		,[NHSNumberStatus] 				= RIGHT(COALESCE(CPAT.[NHSNumberStatus], TPAT.[NHSNumberStatus], '03'), 1)
		,[PermanentAddress] 			= COALESCE(CPAT.[PermanentAddress], TPAT.[PermanentAddress])
		,[PermanentPostCode] 			= LEFT(COALESCE(CPAT.[PermanentPostCode], TPAT.[PermanentPostCode]), 8)
		,[Gender] 						= LEFT(COALESCE(CPAT.[Gender], TPAT.[Gender]), 10)
		,[Surname] 						= LEFT(COALESCE(CPAT.[Surname], TPAT.[Surname]), 20)
		,[GPPracticeCode] 				= LEFT(COALESCE(CPAT.[GPPracticeCode], TPAT.[GPPracticeCode]), 6)
		,[MaritalStatus] 				= LEFT(COALESCE(CPAT.[MaritalStatus], TPAT.[MaritalStatus]), 10)
		,[PatientTitle] 				= LEFT(COALESCE(CPAT.[PatientTitle], TPAT.[PatientTitle]), 10)

	FROM
	(
		SELECT
			 DistrictNo = COALESCE(PLIST.DistrictNo, '###')
			,ContextCode = PLIST.ContextCode

		FROM
			(
				SELECT --TOP 1000
					 DistrictNo
					,ContextCode = CASE WHEN LEFT(StartSiteCode,4) = 'RW3T' THEN @TraffordContext ELSE @CMFTContext END
				FROM
					WarehouseReportingMerged.CDS.APCAngliaBase
				WHERE
					DistrictNo IS NOT NULL

				UNION

				SELECT --TOP 1000
					 DistrictNo
					,ContextCode = CASE WHEN LEFT(SiteCode,4) = 'RW3T' THEN @TraffordContext ELSE @CMFTContext END
				FROM
					WarehouseReportingMerged.CDS.OPAngliaBase
				WHERE
					DistrictNo IS NOT NULL

				--UNION

				--SELECT --TOP 1000
				--	 AEAngliaBase.DistrictNo
				--	,ContextCode = CASE WHEN AEBase.ProviderCode = 'RM4' THEN @TraffordContext ELSE @CMFTContext END
				--FROM
				--	WarehouseReportingMerged.CDS.AEAngliaBase
				--	LEFT JOIN WarehouseOLAPMerged.AE.Base AEBase
				--		ON AEAngliaBase.EncounterRecno = AEBase.EncounterRecNo
				--WHERE
				--	AEAngliaBase.DistrictNo IS NOT NULL
			) PLIST
		) PATLIST

		OUTER APPLY
		(
			SELECT 
				 [HospitalID]			= PAT.SecondaryHISId_Number
				,[DateOfBirth]			= PSN.BirthDate
				,[DateOfDeath]			= PSN.DeathDate
				,[EthnicGroupCode]		= ETHO.Code
				,[FirstName]			= PSN.GivenName
				,[GPPracticeName]		= PRAC.Name
				,[NHSNumber]			= PAT.NATIONALNUMBER1
				,[NHSNumberStatus]		= NHSS.NationalNHSNumberStatusCode
				,[PermanentAddress]		= COALESCE([ADDRESS].Street1,'') + ', ' + COALESCE([ADDRESS].Street2,'') + ', ' + COALESCE([ADDRESS].City,'') + ', ' + COALESCE([ADDRESS].[State],'') + ', ' + COALESCE([ADDRESS].CountryText,'')
				,[PermanentPostcode]	= [ADDRESS].Postcode
				,[Gender]				= CASE WHEN PSN.Sex = 'M' THEN 'Male' ELSE 'Female' END
				,[Surname]				= PSN.FamilyName
				,[GPPracticeCode]		= PRAC.Code
				,[MaritalStatus]		= MARS.Name
				,[PatientTitle]			= PSN.Title

			FROM
			(
				SELECT  -- Ensure just one row is returned
					 PatientId				= COALESCE(PATSUPER.Id, P.Id) 	
					,PersonId				= COALESCE(PATSUPER.PersonId, P.PersonId)		
					,GeneralPractitionerId	= COALESCE(PATSUPER.GeneralPractitionerId, P.GeneralPractitionerId)
					,PracticeId				= COALESCE(PATSUPER.PracticeId, P.PracticeId)
					,NATIONALNUMBER1		= COALESCE(PATSUPER.NATIONALNUMBER1, P.NATIONALNUMBER1)
					,SecondaryHISId_Number	= COALESCE(PATSUPER.SecondaryHISId_Number, P.SecondaryHISId_Number)

				FROM	
					Replica_UltraGendaFoundation.dbo.Patient P

					LEFT JOIN Replica_UltraGendaFoundation.dbo.Patient PATSUPER
						ON P.SupersededBy = PATSUPER.Id 

				WHERE
						P.SecondaryHISId_Number = PATLIST.DistrictNo 
					AND PATLIST.ContextCode = @TraffordContext
			) PAT
			
			LEFT JOIN Replica_UltraGendaFoundation.dbo.Person PSN
				ON PAT.PersonId = PSN.Id

			LEFT JOIN Replica_UltraGendaFoundation.dbo.Practice PRAC
				ON PAT.PracticeId = PRAC.Id

			LEFT JOIN Replica_UltraGendaFoundation.dbo.EthnicOrigin ETHO
				ON PSN.EthnicOriginId = ETHO.Id

			LEFT JOIN Replica_UltraGendaFoundation.dbo.CareProvider GP
				ON PAT.GeneralPractitionerId = GP.Id

			LEFT JOIN Replica_UltraGendaFoundation.dbo.[Address] [ADDRESS]
				ON PSN.Id = [ADDRESS].PartyId

			LEFT JOIN Replica_UltraGendaFoundation.dbo.MaritalStatus MARS
				ON PSN.MaritalStatusId = MARS.Id

			LEFT JOIN WarehouseReportingMerged.WH.NHSNumberStatus NHSS
				ON		NHSS.SourceNHSNumberStatusCode = 
							CASE
								WHEN COALESCE(PAT.NationalNumber1, 'remove') LIKE '%remove%' THEN '03'
								ELSE '01'
							END
					AND NHSS.SourceContextCode = @TraffordContext

		) TPAT

		OUTER APPLY
		(
			SELECT 
				 [HospitalID]			= PAT.DistrictNo
				,[DateOfBirth]			= PAT.DateOfBirth
				,[DateOfDeath]			= PAT.DateOfDeath
				,[EthnicGroupCode]		= ETHO.EthnicityNationalCode
				,[FirstName]			= PAT.Forenames
				,[GPPracticeName]		= PRAC.[Organisation Name]
				,[NHSNumber]			= REPLACE(PAT.NHSNumber, ' ','')
				,[NHSNumberStatus]		= NHSS.NationalNHSNumberStatusCode
				,[PermanentAddress]		= COALESCE(PAT.AddressLine1,'') + ', ' + COALESCE(PAT.AddressLine2,'') + ', ' + COALESCE(PAT.AddressLine3,'') + ', ' + COALESCE(PAT.AddressLine4,'')
				,[PermanentPostcode]	= PAT.PostCode
				,[Gender]				= S.NationalSex
				,[Surname]				= PAT.Surname
				,[GPPracticeCode]		= GP.PracticeCode
				,[MaritalStatus]		= MARS.NationalMaritalStatus
				,[PatientTitle]			= PAT.Title

			FROM

			Warehouse.PAS.Patient PAT

			LEFT JOIN Warehouse.PAS.EthnicOriginBase ETHO
				ON PAT.EthnicOriginCode = ETHO.EthnicOriginCode

			LEFT JOIN Warehouse.PAS.GPBase GP
				ON PAT.RegisteredGpCode = GP.GPCode

			LEFT JOIN Organisation.dbo.[General Medical Practice] PRAC
				ON GP.PracticeCode = PRAC.[Organisation Code]

			LEFT JOIN WarehouseReportingMerged.WH.NHSNumberStatus NHSS
				ON		PAT.NHSNumberStatusId = NHSS.SourceNHSNumberStatusCode
					AND NHSS.SourceContextCode = @CMFTContext

			LEFT JOIN WarehouseReportingMerged.WH.MaritalStatus MARS
				ON		PAT.MaritalStatusCode = MARS.SourceMaritalStatusCode
					AND MARS.SourceContextCode = @CMFTContext

			LEFT JOIN WarehouseReportingMerged.WH.Sex S
				ON		PAT.SexCode = S.SourceSexCode
					AND S.SourceContextCode = @CMFTContext

			WHERE	
					PAT.DistrictNo = PATLIST.DistrictNo 
				AND PATLIST.ContextCode = @CMFTContext

		) CPAT
﻿CREATE  Procedure [CRP].[ExtractOP] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractOP
	**  Purpose: 
	**
	**  Extract proc for Christie Portal OP activity data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 29.01.13    MH         Initial coding
	** 14.03.13    MH		  Look for specialty codes not in the web app XML static file
	******************************************************************************/

	DECLARE @CMFTContext VARCHAR(8) = 'CEN||PAS'

	TRUNCATE TABLE tPooB_OutPatient

	INSERT INTO
		tPooB_OutPatient
		(
			 [Local Patient Identifier]
			,[New NHS Number Status]
			,[New NHS Number]
			,[NHS number type]
			,[NHS Number]
			,[Birth Date]
			,[Age at CDS Activity Date]
			,[Sex]
			,[Outpatient Attendance Identifier]
			,[Administrative Category]
			,[Attended or did not attend]
			,[First Attendance]
			,[Medical Staff Type seeing patient]
			,[Outcome of Attendance]
			,[Attendance Date]
			,[Primary Procedure]
			,[Primary Procedure Date]
			,[2nd Procedure]
			,[2nd Procedure Date]
			,[3rd Procedure]
			,[3rd Procedure Date]
			,[4th Procedure]
			,[4th Procedure Date]
			,[5th Procedure]
			,[5th Procedure Date]
			,[6th Procedure]
			,[6th Procedure Date]
			,[7th Procedure]
			,[7th Procedure Date]
			,[8th Procedure]
			,[8th Procedure Date]
			,[9th Procedure]
			,[9th Procedure Date]
			,[10th Procedure]
			,[10th Procedure Date]
			,[11th Procedure]
			,[11th Procedure Date]
			,[12th Procedure]
			,[12th Procedure Date]
			,[ICD10 Code 1]
			,[ICD10 Code 2]
			,[Site Code of Treatment]
			,[Site Code of Treatment organisation code type]
			,[Provider organisation code 1]
			,[Provider organisation code type 1]
			,[Consultant code]
			,[Specialty Function Code]
			,[Consultant Specialty Function Code]
			,[Code of registered GP]
			,[Code of GP Practice]
			,[Last DNA PatCancel Date]
			,[Priority Type]
			,[Service Type Requested]
			,[Source of Referral]
			,[Referral Request Received Date]
			,[Referring GP Code]
			,[Referring Organisation Code]
			,[Referrer Code]
			,[Referrer Org Code]
			,[Ethnic Group]
			,[Marital Status]
			,[Organisation Code (PCT of Residence)]
			,[PCT of GP Practice]
			,[EventReference]
			,[Provider organisation code]
	)

	SELECT  --TOP 1000   
			 [Local Patient Identifier]					= OP.ContextCode + '||' + OP.SourcePatientNo
			,[New NHS Number Status]					= NULL
			,[New NHS Number]							= CDS.[NHSNumber]
			,[NHS number type]							= NULL
			,[NHS Number]								= CDS.[NHSNumber]
			,[Birth Date]								= CDS.DateOfBirth
			,[Age at CDS Activity Date]					= CDS.[AgeAtCDSActivityDate]		
			,[Sex]										= CASE CDS.SexCode WHEN '1' THEN 'Male' ELSE 'Female' END
			,[Outpatient Attendance Identifier]			= NULL --CDS.[AttendanceIdentifier] this is a string but requires an int
			,[Administrative Category]					= CDS.[AdminCategoryCode]
			,[Attended or did not attend]				= CDS.[DNAFlag]
			,[First Attendance]							= CDS.[DerivedFirstAttendance]
			,[Medical Staff Type seeing patient]		= CDS.[MedicalStaffTypeCode]
			,[Outcome of Attendance]					= CDS.[AttendanceOutcomeCode]
			,[Attendance Date]							= CDS.[AttendanceDate]
			,[Primary Procedure]						= CDS.[PrimaryOperationCode]
			,[Primary Procedure Date]					= NULL
			,[2nd Procedure]							= CDS.[OperationCode2]
			,[2nd Procedure Date]						= NULL
			,[3rd Procedure]							= CDS.[OperationCode3]
			,[3rd Procedure Date]						= NULL
			,[4th Procedure]							= CDS.[OperationCode4]
			,[4th Procedure Date]						= NULL
			,[5th Procedure]							= CDS.[OperationCode5]
			,[5th Procedure Date]						= NULL
			,[6th Procedure]							= CDS.[OperationCode6]
			,[6th Procedure Date]						= NULL
			,[7th Procedure]							= CDS.[OperationCode7]
			,[7th Procedure Date]						= NULL
			,[8th Procedure]							= CDS.[OperationCode8]
			,[8th Procedure Date]						= NULL
			,[9th Procedure]							= CDS.[OperationCode9]
			,[9th Procedure Date]						= NULL
			,[10th Procedure]							= CDS.[OperationCode10]
			,[10th Procedure Date]						= NULL
			,[11th Procedure]							= CDS.[OperationCode11]
			,[11th Procedure Date]						= NULL
			,[12th Procedure]							= CDS.[OperationCode12]
			,[12th Procedure Date]						= NULL
			,[ICD10 Code 1]								= CDS.[PrimaryDiagnosisCode]
			,[ICD10 Code 2]								= CDS.[SubsidiaryDiagnosisCode]
			,[Site Code of Treatment]					= CDS.[SiteCode]
			,[Site Code of Treatment organisation code type]	= NULL
			,[Provider organisation code 1]				= CDS.[ProviderCode]
			,[Provider organisation code type 1]		= NULL
			,[Consultant code]							= CDS.[ConsultantCode]						
			,[Specialty Function Code]					= PSV.SpecialtyCode
			,[Consultant Specialty Function Code]		= CNC.NationalSpecialtyCode
			,[Code of registered GP]					= CDS.[RegisteredGpCode]
			,[Code of GP Practice]						= CDS.[RegisteredGpPracticeCode]					
			,[Last DNA PatCancel Date]					= CDS.[LastDNAorPatientCancelledDate]
			,[Priority Type]							= CDS.[PriorityType]
			,[Service Type Requested]					= CDS.[ServiceTypeRequested]
			,[Source of Referral]						= CDS.[SourceOfReferralCode]
			,[Referral Request Received Date]			= CDS.[ReferralRequestReceivedDate]
			,[Referring GP Code]						= CDS.[GPFundholderCode]
			,[Referring Organisation Code]				= CDS.[ReferringOrganisationCode]
			,[Referrer Code]							= CDS.[ReferralCode]
			,[Referrer Org Code]						= CDS.[ReferringOrganisationCode]
			,[Ethnic Group]								= CASE WHEN CDS.[EthnicGroupCode] = '99' THEN NULL ELSE LEFT(CDS.[EthnicGroupCode], 1) END
			,[Marital Status]							= LEFT(MARS.SourceMaritalStatus, 20)
			,[Organisation Code (PCT of Residence)]		= CDS.[PCTofResidenceCode]
			,[PCT of GP Practice]						= CDS.[PCTResponsible]
			,[EventReference]							= DATEDIFF(MINUTE,'1900-01-01',OP.AppointmentTime) 
			,[Provider organisation code]				= CDS.[ProviderCode]

	FROM
		WarehouseReportingMerged.CDS.OPAngliaBase CDS

		LEFT JOIN WarehouseOLAPMerged.OP.Encounter OP
			ON CDS.EncounterRecNo = OP.EncounterRecNo

		LEFT JOIN	WarehouseOLAPMerged.[WH].[ConsultantByNationalCode] CNC
			ON		CDS.ConsultantCode = CNC.NationalConsultantCode

		LEFT JOIN
		(
			SELECT DISTINCT
				 SourceMaritalStatusCode
				,SourceMaritalStatus
			FROM
				WarehouseReportingMerged.WH.MaritalStatus
			WHERE
				SourceContextCode = @CMFTContext
		) MARS
			ON	CDS.[MaritalStatusCode] = MARS.SourceMaritalStatusCode
/*
	The web app validates specialty codes via a static XML file, this table is a replica of this file
	If the specialty code is not in the file the app coughs its lunch up :-(
	So if the specialty code is not in the file, set it to a NULL value
*/
		LEFT JOIN CRP.PortalSpecialtyValidation PSV
			ON	CDS.[MainSpecialtyCode] = PSV.SpecialtyCode
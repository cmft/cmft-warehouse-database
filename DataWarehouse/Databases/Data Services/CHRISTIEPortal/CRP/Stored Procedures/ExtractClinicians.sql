﻿CREATE  Procedure [CRP].[ExtractClinicians] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractClinicians
	**  Purpose: 
	**
	**  Extract proc for Christie Portal In Patient Discharge Consultant Episode activity data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 29.01.13    MH         Initial coding
	******************************************************************************/

	TRUNCATE TABLE tPooB_Clinicians

	INSERT INTO tPooB_Clinicians
	(
		[source of referral],
		[Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[NHS number type],
		[NHS Number],
		[Consultant code],
		[Specialty Function Code],
		[Consultant Specialty Function Code],
		[Discharge Date],
		[referral request received date]
	)
	select distinct  
		[source of referral],
		[Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[NHS number type],
		[NHS Number],
		[Consultant code],
		[Specialty Function Code],
		[Consultant Specialty Function Code],
		[Discharge Date],
		[referral request received date]

	from 
	(

		select distinct
			[Outcome of Attendance],
			[source of referral],
			A.[Local Patient Identifier],
			A.[New NHS Number Status],
			A.[New NHS Number],
			A.[NHS number type],
			A.[NHS Number],
			A.[Consultant code],
			A.[Specialty Function Code],
			A.[Consultant Specialty Function Code],
			'' as [Discharge Date],
			tPooB_OutPatient.[referral request received date]

		from

		(
			select 
			[Local Patient Identifier],
			[New NHS Number Status],
			[New NHS Number],
			[NHS number type],
			[NHS Number],
			[Consultant code],
			[Specialty Function Code],
			[Consultant Specialty Function Code],
			max([EventReference]) as [Max EventReference]


			from tPooB_OutPatient 
			where [consultant code] not like 'C9999998'
			and [Attendance Date] < GETDATE()

			group by 
			[Local Patient Identifier],
			[New NHS Number Status],
			[New NHS Number],
			[NHS number type],
			[NHS Number],
			[Consultant code],
			[Specialty Function Code],
			[Consultant Specialty Function Code]
		) A

		inner join tPooB_OutPatient on tPooB_OutPatient.[EventReference] = A.[Max EventReference]
							and tPooB_OutPatient.[Consultant code] = A.[Consultant code]

		where [Outcome of Attendance] <> '1'


		union all

		--discharged patients

		select distinct 
			[Outcome of Attendance],
			[source of referral],
			A.[Local Patient Identifier],
			A.[New NHS Number Status],
			A.[New NHS Number],
			A.[NHS number type],
			A.[NHS Number],
			A.[Consultant code],
			A.[Specialty Function Code],
			A.[Consultant Specialty Function Code],
			[Attendance Date]  as [Discharge Date],
			tPooB_OutPatient.[referral request received date]

		from
		(
			select 
				[Local Patient Identifier],
				[New NHS Number Status],
				[New NHS Number],
				[NHS number type],
				[NHS Number],
				[Consultant code],
				[Specialty Function Code],
				[Consultant Specialty Function Code],
				max([EventReference]) as [Max EventReference]

			from tPooB_OutPatient 
			where [consultant code] not like 'C9999998'
			and [Attendance Date] < GETDATE()

			group by 
			[Local Patient Identifier],
			[New NHS Number Status],
			[New NHS Number],
			[NHS number type],
			[NHS Number],
			[Consultant code],
			[Specialty Function Code],
			[Consultant Specialty Function Code]

		) A

		inner join tPooB_OutPatient on tPooB_OutPatient.[EventReference] = A.[Max EventReference]
							and tPooB_OutPatient.[Consultant code] = A.[Consultant code]

		where [Outcome of Attendance] = '1'

	)B

	group by 
		[Source of Referral] ,
		[Discharge Date], 
		[Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[NHS number type],
		[NHS Number],
		[Consultant code],
		[Specialty Function Code],
		[Consultant Specialty Function Code],
		[referral request received date]

	order by [NHS Number]
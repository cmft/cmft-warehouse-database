﻿CREATE  Procedure [CRP].[ClearTables] 
AS

	/******************************************************************************
	**  Name: CRP.ClearTables
	**  Purpose: 
	**
	**  Utility proc to clear down all Christie Portal data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 05.02.13    MH         Initial coding
	******************************************************************************/

	Truncate table tPooB_InPatient
	Truncate table tPooB_Diagnosis
	Truncate table tPooB_Demographics
	Truncate table tPooB_CriticalCare
	Truncate table tPooB_Consultants
	Truncate table tPooB_Clinicians
	Truncate table tPooB_WaitingList
	Truncate table tPooB_Procedures
	Truncate table tPooB_OutPatient
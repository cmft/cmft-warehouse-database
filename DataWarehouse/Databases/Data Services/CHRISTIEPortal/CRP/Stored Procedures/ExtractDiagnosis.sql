﻿CREATE  Procedure [CRP].[ExtractDiagnosis] 
AS

	/******************************************************************************
	**  Name: CRP.CRP.ExtractDiagnosis
	**  Purpose: 
	**
	**  Extract proc for Christie Portal Diagnosis activity data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 04.02.13    MH         Initial coding
	******************************************************************************/

	TRUNCATE TABLE tPooB_Diagnosis

	INSERT INTO
		tPooB_Diagnosis
		(
			 [Local Patient Identifier]
			,[New NHS Number Status] 
			,[NHS Number]
			,[Diagnosis Applicable Date]
			,[Diagnosis Code]
		)
	SELECT *
	FROM 
	(

		select [Local Patient Identifier],
		[New NHS Number Status],
		[NHS Number],
		[attendance date] as [Diagnosis Applicable Date],
		[ICD10 Code 1] as [Diagnosis Code]
		from [tPooB_OutPatient]
		where [ICD10 Code 1] is not null

		union all 

		select [Local Patient Identifier],
		[New NHS Number Status],
		[NHS Number],
		[attendance date],
		[ICD10 Code 2]
		from [tPooB_OutPatient]
		where [ICD10 Code 2] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[Primary Diagnosis ICD]
		from [tPooB_InPatient] 
		where [Primary Diagnosis ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[1st Secondary ICD]
		from [tPooB_InPatient] 
		where [1st Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[2nd Secondary ICD]
		from [tPooB_InPatient] 
		where [2nd Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[3rd Secondary ICD]
		from [tPooB_InPatient] 
		where [3rd Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[4th Secondary ICD]
		from [tPooB_InPatient] 
		where [4th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[5th Secondary ICD]
		from [tPooB_InPatient] 
		where [5th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[6th Secondary ICD]
		from [tPooB_InPatient] 
		where [6th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[7th Secondary ICD]
		from [tPooB_InPatient] 
		where [7th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[8th Secondary ICD]
		from [tPooB_InPatient] 
		where [8th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[9th Secondary ICD]
		from [tPooB_InPatient] 
		where [9th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[10th Secondary ICD]
		from [tPooB_InPatient] 
		where [10th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[11th Secondary ICD]
		from [tPooB_InPatient] 
		where [11th Secondary ICD] is not null

		union all

		SELECT [Local Patient Identifier],
		[New NHS Number Status],
		[New NHS Number],
		[provider spell start date],
		[12th Secondary ICD]
		from [tPooB_InPatient] 
		where [12th Secondary ICD] is not null

	) a
﻿CREATE  Procedure [CRP].[ExtractIP] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractIP
	**  Purpose: 
	**
	**  Extract proc for Christie Portal IP activity data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 29.01.13    MH         Initial coding
	** 14.03.13    MH		  Look for specialty codes not in the web app XML static file
	******************************************************************************/

	TRUNCATE TABLE tPooB_InPatient

	INSERT INTO
		tPooB_InPatient
		(
			 [local patient identifier]
			,[New NHS Number]
			,[Old NHS Number]
			,[New NHS Number Status]
			,[Birth Date]
			,[Age on Admission]
			,[Age at CDS Activity Date]
			,[Ethnic Group]
			,[Sex]
			,[Post Code]
			,[Site Code of Treatment]
			,[Hospital Provider Spell Number]
			,[Administrative Category]
			,[Admission Method]
			,[Discharge Destination]
			,[Discharge Method]
			,[Patient Classification]
			,[Source of Admission]
			,[Provider spell start date]
			,[Provider spell end date]
			,[Consultant Episode Number]
			,[Consultant Episode Start Date]
			,[Consultant Episode End Date]
			,[Consultant Code]
			,[Specialty Function Code]
			,[Consultant Specialty Function Code]
			,[ICD Version]
			,[Primary Diagnosis ICD]
			,[Subsidiary ICD]
			,[1st Secondary ICD]
			,[2nd Secondary ICD]
			,[3rd Secondary ICD]
			,[4th Secondary ICD]
			,[5th Secondary ICD]
			,[6th Secondary ICD]
			,[7th Secondary ICD]
			,[8th Secondary ICD]
			,[9th Secondary ICD]
			,[10th Secondary ICD]
			,[11th Secondary ICD]
			,[12th Secondary ICD]
			,[OPCS Version]
			,[Primary Procedure OPCS]
			,[Primary Op Procedure Date]
			,[Secondary Procedure OPCS]
			,[Secondary Op Procedure Date]
			,[3rd Procedure OPCS]
			,[3rd Op Procedure Date]
			,[4th Procedure OPCS]
			,[4th Op Procedure Date]
			,[5th Procedure OPCS]
			,[5th Op Procedure Date]
			,[6th Procedure OPCS]
			,[6th Op Procedure Date]
			,[7th Procedure OPCS]
			,[7th Op Procedure Date]
			,[8th Procedure OPCS]
			,[8th Op Procedure Date]
			,[9th Procedure OPCS]
			,[9th Op Procedure Date]
			,[10th Procedure OPCS]
			,[10th Op Procedure Date]
			,[11th Procedure OPCS]
			,[11th Op Procedure Date]
			,[12th Procedure OPCS]
			,[12th Op Procedure Date]
			,[Code of patients registered GP]
			,[GP Practice organisation code]
			,[Code of referrer]
			,[Organisation Code of Referrer]
			,[Duration of Elective Wait]
			,[Intended Management]
			,[Decided to Admit Date]
			,[PCT of GP Practice]
			,[Provider organisation code]
		)

	SELECT  --TOP 6000   
			 [local patient identifier]				= APC.ContextCode + '||' + APC.SourcePatientNo
			,[New NHS Number]						= CDS.[NHSNumber]
			,[Old NHS Number]						= NULL
			,[New NHS Number Status]				= NULL
			,[Birth Date]							= CDS.[DateOfBirth]
			,[Age on Admission]						= CDS.[AgeOnAdmission]
			,[Age at CDS Activity Date]				= CDS.[AgeAtCDSActivityDate]
			,[Ethnic Group]							= CASE WHEN CDS.[EthnicGroupCode] = '99' THEN NULL ELSE LEFT(CDS.[EthnicGroupCode], 1) END
			,[Sex]									= CDS.SexCode
			,[Post Code]							= CDS.[Postcode]
			,[Site Code of Treatment]				= CDS.[StartSiteCode]
			,[Hospital Provider Spell Number]		= APC.SourceSpellNo
			,[Administrative Category]				= CDS.[PatientCategoryCode]
			,[Admission Method]						= CDS.[AdmissionMethodCode]
			,[Discharge Destination]				= CDS.[DischargeDestinationCode]
			,[Discharge Method]						= CDS.[DischargeMethodCode]
			,[Patient Classification]				= CDS.[PatientClassificationCode]
			,[Source of Admission]					= CDS.[SourceOfAdmissionCode]
			,[Provider spell start date]			= CDS.[AdmissionDate]
			,[Provider spell end date]				= CDS.[DischargeDate]
			,[Consultant Episode Number]			= CDS.[EpisodeNo]			
			,[Consultant Episode Start Date]		= CDS.[EpisodeStartDate]
			,[Consultant Episode End Date]			= CDS.[EpisodeEndDate]
			,[Consultant Code]						= CDS.[ConsultantCode]
			,[Specialty Function Code]				= PSV.SpecialtyCode 
			,[Consultant Specialty Function Code]	= CNC.NationalSpecialtyCode
			,[ICD Version]							= NULL
			,[Primary Diagnosis ICD]				= CDS.[PrimaryDiagnosisCode]
			,[Subsidiary ICD]						= NULL
			,[1st Secondary ICD]					= CDS.[SecondaryDiagnosisCode1]				
			,[2nd Secondary ICD]					= CDS.[SecondaryDiagnosisCode2]	
			,[3rd Secondary ICD]					= CDS.[SecondaryDiagnosisCode3]	
			,[4th Secondary ICD]					= CDS.[SecondaryDiagnosisCode4]	
			,[5th Secondary ICD]					= CDS.[SecondaryDiagnosisCode5]	
			,[6th Secondary ICD]					= CDS.[SecondaryDiagnosisCode6]	
			,[7th Secondary ICD]					= CDS.[SecondaryDiagnosisCode7]	
			,[8th Secondary ICD]					= CDS.[SecondaryDiagnosisCode8]	
			,[9th Secondary ICD]					= CDS.[SecondaryDiagnosisCode9]	
			,[10th Secondary ICD]					= CDS.[SecondaryDiagnosisCode10]	
			,[11th Secondary ICD]					= CDS.[SecondaryDiagnosisCode11]	
			,[12th Secondary ICD]					= CDS.[SecondaryDiagnosisCode12]	
			,[OPCS Version]							= NULL
			,[Primary Procedure OPCS]				= CDS.[PrimaryOperationCode]
			,[Primary Op Procedure Date]			= CDS.[PrimaryOperationDate]
			,[Secondary Procedure OPCS]				= CDS.[OperationCode2]
			,[Secondary Op Procedure Date]			= CDS.[OperationDate2]
			,[3rd Procedure OPCS]					= CDS.[OperationCode3]
			,[3rd Op Procedure Date]				= CDS.[OperationDate3]
			,[4th Procedure OPCS]					= CDS.[OperationCode4]
			,[4th Op Procedure Date]				= CDS.[OperationDate4]
			,[5th Procedure OPCS]					= CDS.[OperationCode5]
			,[5th Op Procedure Date]				= CDS.[OperationDate5]
			,[6th Procedure OPCS]					= CDS.[OperationCode6]
			,[6th Op Procedure Date]				= CDS.[OperationDate6]
			,[7th Procedure OPCS]					= CDS.[OperationCode7]
			,[7th Op Procedure Date]				= CDS.[OperationDate7]
			,[8th Procedure OPCS]					= CDS.[OperationCode8]
			,[8th Op Procedure Date]				= CDS.[OperationDate8]
			,[9th Procedure OPCS]					= CDS.[OperationCode9]
			,[9th Op Procedure Date]				= CDS.[OperationDate9]
			,[10th Procedure OPCS]					= CDS.[OperationCode10]
			,[10th Op Procedure Date]				= CDS.[OperationDate10]
			,[11th Procedure OPCS]					= CDS.[OperationCode11]
			,[11th Op Procedure Date]				= CDS.[OperationDate11]
			,[12th Procedure OPCS]					= CDS.[OperationCode12]
			,[12th Op Procedure Date]				= CDS.[OperationDate12]
			,[Code of patients registered GP]		= CDS.[RegisteredGpCode]
			,[GP Practice organisation code]		= CDS.[RegisteredGpPracticeCode]
			,[Code of referrer]						= CDS.[ReferrerCode]
			,[Organisation Code of Referrer]		= CDS.[ReferringOrganisationCode]
			,[Duration of Elective Wait]			= CASE WHEN ISNUMERIC(CDS.[DurationOfElectiveWait]) = 1 THEN CAST(CDS.[DurationOfElectiveWait] AS INT) ELSE NULL END
			,[Intended Management]					= CDS.[ManagementIntentionCode]
			,[Decided to Admit Date]				= CDS.[DecidedToAdmitDate]
			,[PCT of GP Practice]					= CDS.[PCTofResidenceCode]
			,[Provider organisation code]			= CDS.[ProviderCode]

	FROM
		WarehouseReportingMerged.CDS.APCAngliaBase CDS

		LEFT JOIN WarehouseOLAPMerged.APC.Encounter APC
			ON CDS.EncounterRecNo = APC.EncounterRecNo

		LEFT JOIN	WarehouseOLAPMerged.[WH].[ConsultantByNationalCode] CNC
			ON		CDS.ConsultantCode = CNC.NationalConsultantCode

/*
	The web app validates specialty codes via a static XML file, this table is a replica of this file
	If the specialty code is not in the file the app coughs its lunch up :-(
	So if the specialty code is not in the file, set it to a NULL value
*/
		LEFT JOIN CRP.PortalSpecialtyValidation PSV
			ON	CDS.[MainSpecialtyCode] = PSV.SpecialtyCode
﻿CREATE  Procedure [CRP].[ExtractCriticalCare] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractCriticalCare
	**  Purpose: 
	**
	**  Extract proc for Christie Portal Critical Care activity 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 29.01.13    MH         Initial coding
	******************************************************************************/

	TRUNCATE TABLE tPooB_CriticalCare

	INSERT INTO tPooB_CriticalCare
	(
		 [Critical Care Unit Function 1]
		,[Critical Care Start Date 1]
		,[Critical Care Discharge date 1]
		,[Advanced Respiratory Support Days 1]
		,[Basic Respiratory Support Days 1]
		,[Advanced Cardiovascular Support Days 1]
		,[Basic Cardiovascular Support Days 1]
		,[Renal Support Days 1]
		,[Neurological Support Days 1]
		,[Gastro Intestinal Support Days 1]
		,[Dermatological Support Days 1]
		,[Liver Support Days 1]
		,[Critical Care level 2 Days 1]
		,[Critical Care level 3 Days 1]
		,[New NHS Number] 
		,[consultant code]
		,[specialty function code] 
		,[Local Patient Identifier] 
	)

	SELECT
		 CCDATA.[Critical Care Unit Function 1]
		,CCDATA.[Critical Care Start Date 1]
		,CCDATA.[Critical Care Discharge date 1]
		,CCDATA.[Advanced Respiratory Support Days 1]
		,CCDATA.[Basic Respiratory Support Days 1]
		,CCDATA.[Advanced Cardiovascular Support Days 1]
		,CCDATA.[Basic Cardiovascular Support Days 1]
		,CCDATA.[Renal Support Days 1]
		,CCDATA.[Neurological Support Days 1]
		,CCDATA.[Gastro Intestinal Support Days 1]
		,CCDATA.[Dermatological Support Days 1]
		,CCDATA.[Liver Support Days 1]
		,CCDATA.[Critical Care level 2 Days 1]
		,CCDATA.[Critical Care level 3 Days 1]
		,CCDATA.[New NHS Number] 
		,CCDATA.[consultant code]
		,CCDATA.[specialty function code] 
		,[Local Patient Identifier]				= APC.ContextCode + '||' + APC.SourcePatientNo
	FROM
	(
		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP1UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP1StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP1DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP1AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP1BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP1AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP1BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP1RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP1NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP1DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP1LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP1Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP1Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS
				
		WHERE
			CDS.[CCP1DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP2UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP2StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP2DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP2AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP2BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP2AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP2BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP2RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP2NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP2DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP2LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP2Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP2Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP2DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP3UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP3StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP3DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP3AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP3BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP3AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP3BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP3RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP3NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP3DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP3LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP3Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP3Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP3DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP4UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP4StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP4DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP4AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP4BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP4AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP4BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP4RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP4NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP4DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP4LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP4Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP4Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP4DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP5UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP5StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP5DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP5AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP5BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP5AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP5BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP5RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP5NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP5DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP5LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP5Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP5Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP5DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP6UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP6StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP6DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP6AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP6BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP6AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP6BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP6RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP6NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP6DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP6LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP6Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP6Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP6DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP7UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP7StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP7DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP7AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP7BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP7AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP7BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP7RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP7NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP7DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP7LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP7Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP7Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP7DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP8UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP8StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP8DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP8AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP8BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP8AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP8BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP8RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP8NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP8DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP8LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP8Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP8Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP8DischargeDate] IS NOT NULL

		UNION ALL

		SELECT
			 EncounterRecNo								= CDS.EncounterRecNo
			,[Critical Care Unit Function 1]			= CDS.[CCP9UnitFunction]
			,[Critical Care Start Date 1]				= CDS.[CCP9StartDate]
			,[Critical Care Discharge date 1]			= CDS.[CCP9DischargeDate]
			,[Advanced Respiratory Support Days 1]		= CDS.[CCP9AdvancedRespiratorySupportDays]
			,[Basic Respiratory Support Days 1]			= CDS.[CCP9BasicRespiratorySupportDays]
			,[Advanced Cardiovascular Support Days 1]	= CDS.[CCP9AdvancedCardiovascularSupportDays]
			,[Basic Cardiovascular Support Days 1]		= CDS.[CCP9BasicCardiovascularSupportDays]
			,[Renal Support Days 1]						= CDS.[CCP9RenalSupportDays]
			,[Neurological Support Days 1]				= CDS.[CCP9NeurologicalSupportDays]
			,[Gastro Intestinal Support Days 1]			= NULL
			,[Dermatological Support Days 1]			= CDS.[CCP9DermatologicalSupportDays]
			,[Liver Support Days 1]						= CDS.[CCP9LiverSupportDays]
			,[Critical Care level 2 Days 1]				= CDS.[CCP9Level2Days]
			,[Critical Care level 3 Days 1]				= CDS.[CCP9Level3Days]
			,[New NHS Number]							= CDS.NHSNumber
			,[consultant code]							= CDS.[ConsultantCode]
			,[specialty function code]					= CDS.[MainSpecialtyCode]

		FROM
			WarehouseReportingMerged.CDS.APCAngliaBase CDS

		WHERE
			CDS.[CCP9DischargeDate] IS NOT NULL
	) CCDATA

	LEFT JOIN WarehouseOLAPMerged.APC.Encounter APC
		ON CCDATA.EncounterRecNo = APC.EncounterRecNo
﻿CREATE  Procedure [CRP].[ExtractConsultants] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractConsultants
	**  Purpose: 
	**
	**  Extract proc for Christie Portal All Consultants 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 29.01.13    MH         Initial coding
	** 26.02.13    MH         Re-map specialty code for AHP
	** 01.03.13    MH         Introduce PortalSpecialtyValidation table as a lookup for a valid specialty code
	******************************************************************************/

	SET NOCOUNT ON

	TRUNCATE TABLE tPooB_Consultants

	SELECT DISTINCT
			ConsultantCode = [Consultant Code]

	INTO #TCONS

	FROM
		tPooB_InPatient

	UNION

	SELECT DISTINCT
		[Consultant Code]
	FROM
		tPooB_OutPatient


	INSERT INTO tPooB_Consultants
	(
		 GMC
		,ConsultantName
		,SpecialtyCode
	)
	SELECT
		 GMC				= #TCONS.ConsultantCode
		,ConsultantName		= CA.ConsultantName
		,SpecialtyCode		= PSV.SpecialtyCode			-- If IP or OP data has an "invalid" specialty code
														-- via the lookup table set to NULL
	FROM
		#TCONS
		INNER JOIN WarehouseOLAPMerged.[WH].[ConsultantByNationalCode] CA			
			ON #TCONS.ConsultantCode = LEFT(CA.NationalConsultantCode, 8)

		LEFT JOIN CRP.PortalSpecialtyValidation PSV    -- Lookup table of valid specialty codes
			ON CA.NationalSpecialtyCode = PSV.SpecialtyCode
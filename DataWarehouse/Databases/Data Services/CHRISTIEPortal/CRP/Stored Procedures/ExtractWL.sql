﻿CREATE  Procedure [CRP].[ExtractWL] 
AS

	/******************************************************************************
	**  Name: CRP.ExtractWL
	**  Purpose: 
	**
	**  Extract proc for Christie Portal OP activity data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 04.02.13    MH         Initial coding
	** 27.02.13    MH         Set Specialty code to 500 for unknown codes
	******************************************************************************/

	DECLARE @CensusDate DATE
	DECLARE @NULLDate DATE = '1900-01-01'

	SELECT @CensusDate = MAX(CAL.TheDate)
	FROM
		WarehouseOLAPMerged.APC.Wait W
		INNER JOIN WarehouseOLAPMerged.WH.Calendar CAL
			ON W.CensusDateId = CAL.DateId


	TRUNCATE TABLE tPooB_WaitingList

	INSERT INTO
		tPooB_WaitingList
		(
			 [Unique Identifier]
			,[Local Patient Identifier]
			,[NHS Number]
			,[NHS Number Status Indicator]
			,[Date of birth]
			,[Age at CDS Activity Date]
			,[Referrer Code]
			,[Referring Organisation Code]
			,[Days Waiting]
			,[Intended Management]
			,[Date added to Waiting List]
			,[Admin Category]
			,[Count of days suspended]
			,[Admission List Status]
			,[Admission Type]
			,[Intended Procedure Status]
			,[Priority]
			,[Guaranteed Admission Date]
			,[Patient cancelled date date]
			,[Waiting list entry last reviewed date]
			,[Consultant code]
			,[Specialty function code]
			,[Consultant Specialty Function Code]
			,[OPCS code 1]
			,[OPCS code 2]
			,[OPCS code 3]
			,[OPCS code 4]
			,[OPCS code 5]
			,[OPCS code 6]
			,[OPCS code 7]
			,[OPCS code 8]
			,[OPCS code 9]
			,[OPCS code 10]
			,[OPCS code 11]
			,[OPCS code 12]
			,[Elective admission list removal reason]
			,[Elective admission list removal removal Date]
			,[Admission Offer Outcome]
			,[Offered Admission Date]
			,[Original Decided To Admit Date]
			,[Suspension start date]
			,[Suspension end date]
			,[episoderef]
		)

		SELECT

			 [Unique Identifier]							= WL.EncounterRecNo
			,[Local Patient Identifier]						= WL.ContextCode + '||' + WL.SourcePatientNo
			,[NHS Number]									= REPLACE(WL.NHSNumber,' ','')
			,[NHS Number Status Indicator]					= NULL
			,[Date of birth]								= WL.DateOfBirth
			,[Age at CDS Activity Date]						= DATEDIFF(YEAR, WL.DateOfBirth, @CensusDate)
			,[Referrer Code]								= NULL
			,[Referring Organisation Code]					= NULL
			,[Days Waiting]									= WL.LengthOfWait
			,[Intended Management]							= CASE WHEN IsNumeric(IM.NationalIntendedManagementCode) = 1 THEN IM.NationalIntendedManagementCode ELSE -1 END
			,[Date added to Waiting List]					= WL.[AddedToWaitingListTime]
			,[Admin Category]								= AC.NationalAdministrativeCategoryCode					
			,[Count of days suspended]						= WL.[CountOfDaysSuspended]
			,[Admission List Status]						= CASE WL.StatusCode
																WHEN '20' THEN 2			-- Suspended
																ELSE 1
															  END
			,[Admission Type]								= AM.NationalAdmissionMethodCode
			,[Intended Procedure Status]					= NULL
			,[Priority]										= CASE WHEN IsNumeric(PT.NationalPriorityTypeCode) = 1 THEN PT.NationalPriorityTypeCode ELSE -1 END
			,[Guaranteed Admission Date]					= WL.GuaranteedAdmissionDate
			,[Patient cancelled date date]					= NULL
			,[Waiting list entry last reviewed date]		= NULL
			,[Consultant code]								= CONS.NationalConsultantCode
			,[Specialty function code]						= CASE SPEC.NationalSpecialtyCode
																WHEN '960' THEN 500
																WHEN 'N||SPEC' THEN 500
																ELSE SPEC.NationalSpecialtyCode
															  END			
			,[Consultant Specialty Function Code]			= CONS.MainSpecialtyCode
			,[OPCS code 1]									= WL.IntendedPrimaryProcedureCode
			,[OPCS code 2]									= NULL
			,[OPCS code 3]									= NULL
			,[OPCS code 4]									= NULL
			,[OPCS code 5]									= NULL
			,[OPCS code 6]									= NULL
			,[OPCS code 7]									= NULL
			,[OPCS code 8]									= NULL
			,[OPCS code 9]									= NULL
			,[OPCS code 10]									= NULL
			,[OPCS code 11]									= NULL
			,[OPCS code 12]									= NULL
			,[Elective admission list removal reason]		= NULL
			,[Elective admission list removal removal Date]	= NULL
			,[Admission Offer Outcome]						= NULL								
			,[Offered Admission Date]						= TCAL.TheDate
			,[Original Decided To Admit Date]				= CASE 
																WHEN WL.[OriginalDateOnWaitingList] = @NULLDate THEN NULL
																ELSE WL.[OriginalDateOnWaitingList]
															  END
			,[Suspension start date]						= WL.SuspensionStartDate
			,[Suspension end date]							= WL.SuspensionEndDate
			,[episoderef]									= WL.EpisodeNo 

		FROM
			WarehouseOLAPMerged.APC.Wait WL
			INNER JOIN WarehouseOLAPMerged.WH.Calendar CAL
				ON WL.CensusDateId = CAL.DateId

			LEFT JOIN WarehouseOLAPMerged.APC.IntendedManagement IM
				ON		WL.ManagementIntentionId = IM.SourceIntendedManagementId 
					AND WL.ContextCode = IM.SourceContextCode 

			LEFT JOIN WarehouseOLAPMerged.WH.AdministrativeCategory AC
				ON		WL.AdminCategoryID = AC.SourceAdministrativeCategoryID
					AND WL.ContextCode = AC.SourceContextCode

			LEFT JOIN WarehouseOLAPMerged.APC.AdmissionMethod AM
				ON		WL.AdmissionMethodID = AM.SourceAdmissionMethodId
					AND WL.ContextCode = AM.SourceContextCode

			LEFT JOIN WarehouseOLAPMerged.APC.WaitType WLT
				ON		WL.WaitTypeCode = WLT.WaitTypeCode

			LEFT JOIN WarehouseOLAPMerged.WH.PriorityType PT
				ON		WL.PriorityID = PT.SourcePriorityTypeID
					AND	WL.ContextCode = PT.SourceContextCode

			LEFT JOIN WarehouseOLAPMerged.WH.Consultant CONS
				ON		WL.ConsultantID = CONS.SourceConsultantID
					AND	WL.ContextCode = CONS.SourceContextCode

			LEFT JOIN WarehouseOLAPMerged.WH.Specialty SPEC
				ON		WL.SpecialtyID = SPEC.SourceSpecialtyID
					AND	WL.ContextCode = SPEC.SourceContextCode

			LEFT JOIN WarehouseOLAPMerged.WH.Calendar TCAL
				ON WL.TCIDateId = TCAL.DateId

		WHERE
			CAL.TheDate = @CensusDate
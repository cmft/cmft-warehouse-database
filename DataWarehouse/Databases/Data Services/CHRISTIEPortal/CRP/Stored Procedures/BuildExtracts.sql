﻿CREATE  Procedure [CRP].[BuildExtracts] 
(
	 @FromDate DATE = NULL
	,@ToDate DATE   = NULL
)
AS

	/******************************************************************************
	**  Name: CRP.BuildExtracts
	**  Purpose: 
	**
	**  Controlling proc for Christie Portal Extract creation 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 04.02.13    MH         Initial coding
	** 07.03.13    MH         Insert an audit row containing run start and end times
	******************************************************************************/

	DECLARE @Today			DATE    = GETDATE()
	DECLARE @StartRunTime	DATETIME = GETDATE()
	DECLARE @EndRunTime		DATETIME = NULL

	SET @ToDate   = COALESCE(@ToDate, DATEADD(DAY,  -1, @Today))
	SET @FromDate = COALESCE(@FromDate, DATEADD(YEAR, -1, @Today))

	DECLARE @AuditID INT

	-- Insert Audit row
	INSERT INTO CRP.Audit 
	(
		 AuditStart
		,StartDateParameter
		,EndDateParameter
	)
	VALUES
	(
		 @StartRunTime
		,@FromDate
		,@ToDate
	)

	SET @AuditID = SCOPE_IDENTITY()

	DECLARE @MailFrom VARCHAR(200)
	DECLARE @MailTo   VARCHAR(200)
	DECLARE @Subject  VARCHAR(200)
	DECLARE @Body     VARCHAR(MAX)

	SELECT @MailFrom = TextValue FROM Warehouse.Utility.Parameter WHERE Parameter = 'MAILFROM'
	SELECT @MailTo = TextValue FROM Warehouse.Utility.Parameter WHERE Parameter = 'SSISNOTIFICATIONS'

	SET @Subject = 'Christie Portal Process - Run Successful'
	SET @Body = '<P>Extract tables populated between ' + CAST(@FromDate AS VARCHAR(20)) + ' To ' + CAST(@ToDate AS VARCHAR(20)) + '</P>'

	BEGIN TRY

		-- Populate the APC CDS staging table
		EXEC WarehouseReportingMerged.[CDS].[BuildAnglia] 
			 @ToDate = @Today
			,@CDSBulkReplacementGroupCode  = '010'			-- In-Patient
			,@CDSProtocolIdentifier = '999' 
			,@FromDate = @FromDate
			,@UpdateParameterTable = 0

		-- Populate the OP CDS staging table
		EXEC WarehouseReportingMerged.[CDS].[BuildAnglia] 
			 @ToDate = @Today
			,@CDSBulkReplacementGroupCode  = '060'			-- Out-Patient
			,@CDSProtocolIdentifier = '999' 
			,@FromDate = @FromDate
			,@UpdateParameterTable = 0

		-- Build the Christie Portal Extract tables
		EXEC CRP.ExtractIP
		EXEC CRP.ExtractOP
		EXEC CRP.ExtractCriticalCare
		EXEC CRP.ExtractWL
		EXEC CRP.ExtractDemographics

		-- Below must be run after the above
		EXEC CRP.ExtractClinicians	
		EXEC CRP.ExtractConsultants
		EXEC CRP.ExtractDiagnosis
		EXEC CRP.ExtractProcedures

		SET @EndRunTime = GETDATE()

		SET @Body = @Body	+ '<P>Run Time ' 
							+ CAST(DATEDIFF(MINUTE, @StartRunTime, @EndRunTime) AS VARCHAR(5)) 
							+ ' Minutes</P>'

	END TRY

	BEGIN CATCH
		SET @Subject = 'Christie Portal Process - Run FAILED'
		SET @Body = @Body	+ '<P>The following errors occurred in procedure ' 
							+  ERROR_PROCEDURE() 
							+ '</P>'
							+ '<P>Message : '
							+ ERROR_MESSAGE() 
							+ '</P>'
	END CATCH

	-- E-mail results	
	EXEC msdb.dbo.sp_send_dbmail 
			 @Subject		= @Subject
			,@From_Address	= @MailFrom
			,@Body			= @Body
			,@Recipients	= @MailTo
			,@Body_Format	= 'HTML' 

	-- Update the Audit row with the finish time
	UPDATE CRP.Audit
		SET AuditEnd = COALESCE(@EndRunTime, GETDATE())
	WHERE
		ID = @AuditID
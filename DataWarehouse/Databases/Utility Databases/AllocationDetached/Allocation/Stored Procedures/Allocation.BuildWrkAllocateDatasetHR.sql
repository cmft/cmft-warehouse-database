﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetHR] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseSummary.MergeSummaryRecno
	,DatasetCode = 'HR'
	,CensusDate
	,DepartmentCode = convert(varchar,OrganisationCode)
	,SourceContextCode = BaseSummary.ContextCode

from
	WarehouseOLAPMergedV2.HR.BaseSummary BaseSummary

where
     BaseSummary.ContextCode = 'CMFT||ESREXT'
          
	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats


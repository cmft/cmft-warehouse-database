﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetFERTILITY] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset



insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,TreatmentType
	,SourceContextCode
) 
select
	 DatasetRecno = MergeRecno
	,DatasetCode = 'FERTILITY'
	,DatasetStartDate = EncounterDate 
	,TreatmentType
	,SourceContextCode = ContextCode

from
	WarehouseOLAPMergedV2.OP.BaseFertility
where
	EncounterDate is not null

	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetOPDirectorate]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset



insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,SourceClinicCode
	,SourceContextCode
	,PatientCategoryCode
	,NationalSpecialtyCode
	
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'OP'
	,DatasetStartDate = Encounter.AppointmentDate
	,SourceSiteCode =
		coalesce(
			 Encounter.EpisodicSiteCode
			,Encounter.SiteCode
		)

	,SourceSpecialtyCode = Encounter.ReferralSpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.AppointmentDate
	,SourceClinicCode = Encounter.ClinicCode
	,SourceContextCode = Encounter.ContextCode

	,PatientCategoryCode =
		case
		when FirstAttendance.NationalFirstAttendanceCode = '1'
		then 'NEW'
		else 'REVIEW'
		end
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.OP.FirstAttendance FirstAttendance
on	FirstAttendance.SourceFirstAttendanceCode = Encounter.FirstAttendanceFlag
and Encounter.ContextCode = FirstAttendance.SourceContextCode

left outer join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.ReferralSpecialtyID

where
	@ProcessAll = 1

or	exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.ProcessList
	where
		ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	@ProcessAll = 0
	)


	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

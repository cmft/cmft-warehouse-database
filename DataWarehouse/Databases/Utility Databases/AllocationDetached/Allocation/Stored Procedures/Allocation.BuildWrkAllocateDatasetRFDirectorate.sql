﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetRFDirectorate]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset



insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode

	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'RF'

	,SourceSiteCode = Encounter.SiteCode
	,SourceSpecialtyCode = Encounter.SpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.ReferralDate
	,SourceContextCode = Encounter.ContextCode
from
	WarehouseOLAPMergedV2.RF.BaseEncounter Encounter

		
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

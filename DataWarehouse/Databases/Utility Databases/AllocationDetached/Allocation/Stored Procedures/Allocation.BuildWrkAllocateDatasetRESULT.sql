﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetRESULT] as



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,StatusCode
	,ResultTime
	,SourceContextCode
	,ResultSetSourceUniqueID
) 
select
	 DatasetRecno = BaseResult.MergeResultRecno
	,DatasetCode = 'RESULT'
	,BaseResult.ResultSetStatusCode	
	,BaseResult.ResultTime
	,SourceContextCode = BaseResult.ContextCode
	,BaseResult.ResultSetSourceUniqueID

from
	WarehouseOLAPMergedV2.Result.BaseResult

inner join (
			select
				ResultSet.ResultSetSourceUniqueID
				,ResultSet.ContextCode
			from 
				WarehouseOLAPMergedV2.Result.BaseResult ResultSet

			inner join WarehouseOLAPMergedV2.Result.ProcessList
			on ResultSet.MergeResultRecno = ProcessList.MergeRecno
			
			group by
				ResultSet.ResultSetSourceUniqueID
				,ResultSet.ContextCode
			
			) ProcessListResultSet

on	ProcessListResultSet.ResultSetSourceUniqueID = BaseResult.ResultSetSourceUniqueID
and ProcessListResultSet.ContextCode = BaseResult.ContextCode

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats





	









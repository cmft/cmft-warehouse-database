﻿create procedure [Allocation].[BuildWrkAllocateDatasetOPERATION]

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20151006	RR	Created
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

	

insert into Allocation.WrkAllocateDataset
	(
	DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,ActivityDate
	,SourceContextCode
	,Theatre
)
select
	 DatasetRecno = OperationDetail.MergeRecno
	,DatasetCode = 'OPERATION'
	,DatasetStartDate = OperationDate
	,DatasetEndDate = OperationDate
	,ActivityDate = OperationDate
	,SourceContextCode = ContextCode
	,Theatre = OperatingSuite
	

from
	WarehouseOLAPMergedV2.Theatre.BaseOperationDetail OperationDetail
	
inner join WarehouseOLAPMergedV2.Theatre.BaseOperationDetailReference Reference
on OperationDetail.MergeRecno = Reference.MergeRecno

left join WarehouseOLAPMergedV2.Theatre.Theatre
on Reference.TheatreID = Theatre.SourceTheatreID
	
	
	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

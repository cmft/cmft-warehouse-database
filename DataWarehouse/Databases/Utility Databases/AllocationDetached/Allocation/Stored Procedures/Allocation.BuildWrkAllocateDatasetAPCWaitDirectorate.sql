﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCWaitDirectorate]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

	
select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                  WarehouseOLAPMergedV2.APCWL.BaseEncounter 
                           )
                     )

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,PatientCategoryCode
	,WardCode
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APCWAIT'
	,Encounter.CensusDate
	,SourceSiteCode = Encounter.SiteCode
	,SourceSpecialtyCode = Encounter.SpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.CensusDate

	,PatientCategoryCode =
		Warehouse.dbo.f_get_patient_category(
			 AdmissionMethod.InternalCode
			,ManagementIntention.InternalCode
			,Encounter.AddedToWaitingListTime
			,NULL
		)

	,Encounter.WardCode
	,Encounter.ContextCode
from
	WarehouseOLAPMergedV2.APCWL.BaseEncounter Encounter

left join Warehouse.PAS.AdmissionMethod AdmissionMethod
on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

left join Warehouse.PAS.ManagementIntention ManagementIntention
on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

where
	Encounter.CensusDate = @CensusDate


	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

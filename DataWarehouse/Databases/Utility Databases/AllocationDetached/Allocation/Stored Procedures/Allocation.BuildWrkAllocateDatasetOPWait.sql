﻿
CREATE procedure [Allocation].[BuildWrkAllocateDatasetOPWait]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                  WarehouseOLAPMergedV2.OPWL.BaseEncounter  	       
                           )
                     )


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,SourceClinicCode
	,ProcedureCode
	,SourceContextCode
	,Division
	,SourceConsultantCode
	,SourceSourceOfReferralCode
	,NationalFirstAttendanceCode
	,ConsultantNationalMainSpecialtyCode
	,SourceWaitingListCode
	,NationalSiteCode
	,SourceSiteCode
)
select 
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'OPWAIT'
	,DatasetStartDate = Encounter.AppointmentDate
	,SourceSpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,ClinicCode = Encounter.ClinicCode
	,ProcedureCode = Encounter.PrimaryProcedureCode
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,Consultant.SourceConsultantCode
	,SourceOfReferral.SourceSourceOfReferralCode
	,FirstAttendance.NationalFirstAttendanceCode
	,ConsultantNationalMainSpecialtyCode = Consultant.MainSpecialtyCode
	,SourceWaitingListCode = WaitingList.SourceWaitingListCode
	,NationalSiteCode = left(Site.NationalSiteCode, 10)
	,SiteCode = left(Site.SourceSiteCode, 10)

from
	WarehouseOLAPMergedV2.OPWL.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.OPWL.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

left join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

left join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

left join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

left join WarehouseOLAPMergedV2.OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = Reference.FirstAttendanceID

left join WarehouseOLAPMergedV2.OPWL.WaitingList
on	WaitingList.SourceWaitingListID = Reference.WaitingListID

left join WarehouseOLAPMergedV2.WH.Site
on Site.SourceSiteID = Reference.SiteID

where
	Encounter.CensusDate = @CensusDate

	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

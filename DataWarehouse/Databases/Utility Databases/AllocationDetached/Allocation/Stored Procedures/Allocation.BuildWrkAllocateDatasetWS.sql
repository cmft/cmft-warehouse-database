﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetWS] 
	
@ProcessAll bit = 1

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

declare @FromDate date = dateadd(month, -3, getdate())



insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,WardCode
	,SourceContextCode
) 
select
	 DatasetRecno = MergeEncounterRecno
	,DatasetCode = 'WS'
	,DatasetStartDate = StartDate
	,SiteCode = SiteCode
	,WardCode = WardCode
	,SourceContextCode = ContextCode
from
	WarehouseOLAPMergedV2.APC.BaseWardStay

where
	@ProcessAll = 1
or	(
		Updated >= @FromDate
	and @ProcessAll = 0
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats




﻿create procedure [Allocation].[BuildWrkAllocateDatasetCOM]
as

----------------------------------------------------------------------------
---- Copyright Gecko Technologies Ltd. 2003    --
----------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'COM'
	,DatasetStartDate = null
	,DatasetEndDate = null
	,SpecialtyCode = SpecialtyCode
	,ContextCode = Encounter.ContextCode

from
	WarehouseOLAPMergedV2.COM.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.COM.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left outer join WarehouseOLAPMergedV2.COM.Specialty Specialty
on	Specialty.SourceValueID = Reference.SpecialtyID


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

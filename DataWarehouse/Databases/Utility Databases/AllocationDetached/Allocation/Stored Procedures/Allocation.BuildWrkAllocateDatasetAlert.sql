﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAlert] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSpecialtyCode
	,SourceContextCode
	,ActivityDate
) 
select
	 DatasetRecno = Alert.MergeAlertRecno
	,DatasetCode = 'ALERT'
	,DatasetStartDate = Alert.CreatedDate
	,SourceSpecialtyCode = Specialty.SpecialtyID
	,SourceContextCode = Alert.ContextCode
	,ActivityDate = CreatedDate
from
	WarehouseOLAPMergedV2.Observation.BaseAlert Alert

inner join
	Warehouse.Observation.Specialty		
on	Specialty.SpecialtyID = Alert.SpecialtyID

where
      Alert.ContextCode = 'CEN||PTRACK'

	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

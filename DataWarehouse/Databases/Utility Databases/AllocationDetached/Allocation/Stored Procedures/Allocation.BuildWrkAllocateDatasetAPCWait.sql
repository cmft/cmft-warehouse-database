﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCWait]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                  WarehouseOLAPMergedV2.APCWL.BaseEncounter 
                           )
                     )

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,NationalSiteCode
	,SourceContextCode
	,Division
	,AdmissionTypeCode
	,ProcedureCode
	,NationalAdmissionMethodCode
	,SourceIntendedManagementCode
)

select 
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APCWAIT'
	,DatasetStartDate = Encounter.CensusDate
	,DatasetEndDate = null
	,SpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,SiteCode = null
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,AdmissionMethod.AdmissionTypeCode
	,Encounter.IntendedPrimaryOperationCode
	,NationalAdmissionMethodCode = AdmissionMethod.NationalAdmissionMethodCode
	,SourceIntendedManagementCode = Encounter.ManagementIntentionCode
from
	WarehouseOLAPMergedV2.APCWL.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.APCWL.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

left join WarehouseOLAPMergedV2.APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = Reference.AdmissionMethodID

left join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

where
	Encounter.CensusDate = @CensusDate



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

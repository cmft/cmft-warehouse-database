﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetObservationSet] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSpecialtyCode
	,SourceContextCode
	,ActivityDate
) 
select
	 DatasetRecno = Observation.MergeObservationSetRecno
	,DatasetCode = 'OBSSET'
	,DatasetStartDate = Observation.StartDate
	,SourceSpecialtyCode = Specialty.SpecialtyID
	,SourceContextCode = Observation.ContextCode
	,ActivityDate = TakenDate

from
	WarehouseOLAPMergedV2.Observation.BaseObservationSet Observation

inner join
	Warehouse.Observation.Specialty		
on	Specialty.SpecialtyID = Observation.SpecialtyID

where
      Observation.ContextCode = 'CEN||PTRACK'

	  	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPCExclusion]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

-- 20141127	RR	added WardCode


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset
truncate table Allocation.WrkAllocateDatasetOperation
truncate table Allocation.WrkAllocateDatasetDiagnosis


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceUniqueID
	,NHSNumber
	,SourceSpecialtyCode
	,ProviderSpellNo
	,SourceEncounterNo
	,FirstEpisodeInSpellIndicator
	,StartSiteCode
	,AdmissionDate 
	,AdmissionTime
	,AdmissionMethodCode
	,PatientCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryProcedureCode
	,IntendedPrimaryProcedureCode
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,StartWardCode
	,SourceContextCode
	,SourceAdminCategoryCode
)
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APC'
	,EpisodeStartDate
	,SourceUniqueID
	,NHSNumber
	,SourceSpecialtyCode = SpecialtyCode
	,ProviderSpellNo
	,SourceEncounterNo
	,FirstEpisodeInSpellIndicator
	,StartSiteCode
	,AdmissionDate 
	,AdmissionTime
	,AdmissionMethodCode
	,PatientCategoryCode
	,PrimaryDiagnosisCode
	,PrimaryProcedureCode
	,IntendedPrimaryProcedureCode
	,GlobalProviderSpellNo
	,GlobalEpisodeNo	
	,StartWardTypeCode
	,SourceContextCode = ContextCode
	,SourceAdminCategoryCode = AdminCategoryCode	
from
	WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetOP]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

-- 20141119	RR	added field LocalAdminCategory


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset



insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,SourceClinicCode
	,ProcedureCode
	,SourceContextCode
	,Division
	,SourceConsultantCode
	,SourceSourceOfReferralCode
	,NationalFirstAttendanceCode
	,SourceFirstAttendanceCode
	,ConsultantNationalMainSpecialtyCode

	,HRGCode
	,DirectorateCode
	,NationalSexCode
	,NationalAdministrativeCategoryCode
	,ReferralSpecialtyTypeCode
	,CommissionerCode
	,SourceAppointmentTypeCode
	,NationalSourceOfReferralCode
	,Postcode
	,ContractSerialNo
	,PseudoPostcode
	,PostcodeAtDischarge
	,NationalAttendanceStatusCode
	,ProcedureCoded
	,SourceAppointmentStatusCode
	,LocalAdministrativeCategoryCode
	,NationalSiteCode
	,SourceSiteCode
) 
select 
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'OP'
	,cast(Encounter.AppointmentTime as date)
	,SpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,ClinicCode = Clinic.SourceClinicCode
	,ProcedureCode = Encounter.PrimaryOperationCode
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,Consultant.SourceConsultantCode
	,SourceOfReferral.SourceSourceOfReferralCode
	,FirstAttendance.NationalFirstAttendanceCode
	,FirstAttendanceSource.SourceFirstAttendanceCode
	,Consultant.MainSpecialtyCode

	,HRG4Encounter.HRGCode
	,Directorate.DirectorateCode
	,Sex.NationalSexCode
	,AdministrativeCategory.NationalAdministrativeCategoryCode
	,Encounter.ReferralSpecialtyTypeCode
	,CCGCode
	,AppointmentTypeCode
	,NationalSourceOfReferralCode
	,Encounter.Postcode
	,Encounter.ContractSerialNo
	,Encounter.PseudoPostcode
	,Encounter.PostcodeAtAppointment
	,NationalAttendanceStatusCode
	,ProcedureCoded =
						case
						when PrimaryOperationCode is not null
						then 1
						end
	,AppointmentStatusCode
	,LocalAdministrativeCategoryCode = Encounter.LocalAdminCategoryCode 
	,NationalSiteCode = left(Site.NationalSiteCode, 10)
	,SiteCode = left(Site.SourceSiteCode, 10)
from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left outer join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.ReferralSpecialtyID

left outer join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(Encounter.DirectorateCode, 'N/A')

left outer join WarehouseOLAPMergedV2.OP.Clinic
on	Clinic.SourceClinicID = Reference.ClinicID

left outer join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

left outer join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

left outer join WarehouseOLAPMergedV2.OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = Reference.DerivedFirstAttendanceID

left outer join WarehouseOLAPMergedV2.OP.FirstAttendance FirstAttendanceSource
on	FirstAttendanceSource.SourceFirstAttendanceID = Reference.FirstAttendanceID

left outer join WarehouseOLAPMergedV2.OP.HRG4Encounter
on HRG4Encounter.MergeEncounterRecno = Encounter.MergeEncounterRecno

left outer join WarehouseOLAPMergedV2.WH.AdministrativeCategory
on AdministrativeCategory.SourceAdministrativeCategoryID = Reference.AdminCategoryID

left outer join WarehouseOLAPMergedV2.WH.Sex
on Sex.SourceSexID = Reference.SexID

left outer join WarehouseOLAPMergedV2.OP.AttendanceStatus
on AttendanceStatus.SourceAttendanceStatusID = Reference.AttendanceStatusID

left join WarehouseOLAPMergedV2.WH.Site
on Site.SourceSiteID = Reference.SiteID

where
	@ProcessAll = 1

or	exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.ProcessList
	where
		ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	@ProcessAll = 0
	)

--now generate the list of procedures....
insert into Allocation.WrkAllocateDatasetOperation
	(
	 DatasetRecno
	,DatasetCode
	,SequenceNo
	,OperationCode
	)
select
	 DatasetRecno = BaseOperation.MergeEncounterRecno
	,DatasetCode = 'OP'
	,BaseOperation.SequenceNo
	,BaseOperation.OperationCode
from
	WarehouseOLAPMergedV2.OP.BaseOperation

inner join Allocation.WrkAllocateDataset
on	WrkAllocateDataset.DatasetRecno = BaseOperation.MergeEncounterRecno
and	WrkAllocateDataset.DatasetCode = 'OP'


	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats













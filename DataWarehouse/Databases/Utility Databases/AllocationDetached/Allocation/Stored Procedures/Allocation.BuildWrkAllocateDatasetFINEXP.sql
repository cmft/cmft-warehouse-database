﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetFINEXP] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,Division
	,SourceContextCode
) 
select
	 DatasetRecno = BaseExpenditure.MergeExpenditureRecno
	,DatasetCode = 'FINEXP'
	,BaseExpenditure.CensusDate
	,Division = Division
	,SourceContextCode = BaseExpenditure.ContextCode
from
	WarehouseOLAPMergedV2.Finance.BaseExpenditure

 	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats     



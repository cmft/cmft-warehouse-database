﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetINC] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = Incident.MergeIncidentRecno
	,DatasetCode = 'INC'
	,Incident.IncidentDate
	,DepartmentCode = Incident.DepartmentCode 
	,SourceContextCode = Incident.ContextCode

from
	WarehouseOLAPMergedV2.Incident.BaseIncident Incident
where
      ContextCode = 'CMFT||ULYSS'

	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats










﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAE]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset



insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceContextCode
	,StartSiteCode
	,EndSiteCode
	,SourceSiteCode
	,NationalAttendanceDisposalCode
	,SourceAttendanceDisposalCode
	,NHSNumber
	,DepartureTime
	,ArrivalTime
	,ArrivalTimeAdjusted
	,DateOfBirth
	,DepartmentTypeCode
	,NationalAttendanceCategoryCode
	,Age	
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'AE'
	,DatasetStartDate = Encounter.ArrivalDate
	,DatasetEndDate = null
	,ContextCode = Encounter.ContextCode
	,StartSiteCode = Encounter.SiteCode
	,EndSiteCode = Encounter.SiteCode
	,SourceSiteCode = SiteCode
	,AttendanceDisposal.NationalAttendanceDisposalCode
	,AttendanceDisposal.SourceAttendanceDisposalCode
	,NHSNumber
	,DepartureTime
	,ArrivalTime
	,ArrivalTimeAdjusted
	,DateOfBirth
	,DepartmentTypeCode
	,NationalAttendanceCategoryCode
	,Age = CMFT.Dates.GetAge(Encounter.DateOfBirth, Encounter.DepartureTime)
from
	WarehouseOLAPMergedV2.AE.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.AE.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.AE.AttendanceDisposal		
on	AttendanceDisposal.SourceAttendanceDisposalID = Reference.AttendanceDisposalID

inner join WarehouseOLAPMergedV2.AE.AttendanceCategory		
on	AttendanceCategory.SourceAttendanceCategoryID = Reference.AttendanceCategoryID

where
	@ProcessAll = 1

or	exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.AE.ProcessList
	where
		ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	ProcessList.Dataset = 'Encounter'
	and	@ProcessAll = 0
	)


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats


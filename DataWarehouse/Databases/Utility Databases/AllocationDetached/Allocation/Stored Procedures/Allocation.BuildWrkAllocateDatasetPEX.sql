﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetPEX] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,LocationCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseResponse.MergeResponseRecno
	,DatasetCode = 'PEX'
	,BaseResponse.ResponseDate
	,LocationCode = cast(BaseResponse.LocationID as varchar(100))
	,SourceContextCode = BaseResponse.ContextCode

from
	WarehouseOLAPMergedV2.PEX.BaseResponse 
where
      ContextCode in ('CMFT||ENVOY','CMFT||HOSP','CMFT||CRT')

	
	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats








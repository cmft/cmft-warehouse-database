﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetQCR] as



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseAuditAnswer.AuditAnswerRecno
	,DatasetCode = 'QCR'
	,DatasetStartDate = BaseAuditAnswer.AuditDate
	,DepartmentCode = LocationCode 
	,SourceContextCode = BaseAuditAnswer.ContextCode

from
	WarehouseOLAPMergedV2.QCR.BaseAuditAnswer BaseAuditAnswer

where
      BaseAuditAnswer.ContextCode = 'CEN||QCR'
      
   	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
  
﻿CREATE procedure [Allocation].[AllocateAPC]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


exec Allocation.BuildWrkAllocateDatasetAPCStartDirectorate
exec Allocation.ExecuteAllocation 'APCSD'

exec Allocation.BuildWrkAllocateDatasetAPCEndDirectorate
exec Allocation.ExecuteAllocation 'APCED'

exec Allocation.BuildWrkAllocateDatasetAPC
exec Allocation.ExecuteAllocation 'APC'

--now update allocation results where appropriate

--Start Directorate
update
	WarehouseOLAPMergedV2.APC.BaseEncounter
set
	StartDirectorateCode = Allocation.AllocationCode
from
	WarehouseOLAPMergedV2.APC.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APCSD'

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = 10
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.StartDirectorateCode != Allocation.AllocationCode
or	BaseEncounter.StartDirectorateCode is null

--End Directorate
update
	WarehouseOLAPMergedV2.APC.BaseEncounter
set
	EndDirectorateCode = Allocation.AllocationCode
from
	WarehouseOLAPMergedV2.APC.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APCED'

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = 10
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.EndDirectorateCode != Allocation.AllocationCode
or	BaseEncounter.EndDirectorateCode is null

--Purchaser
update
	WarehouseOLAPMergedV2.APC.BaseEncounter
set
	PurchaserCode = Allocation.AllocationCode
from
	WarehouseOLAPMergedV2.APC.BaseEncounter

inner join Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = BaseEncounter.MergeEncounterRecno
and	DatasetAllocation.DatasetCode = 'APC'

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = 11
and	Allocation.AllocationID = DatasetAllocation.AllocationID

where
	BaseEncounter.PurchaserCode != Allocation.AllocationCode
or	BaseEncounter.PurchaserCode is null




select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetOPWaitDirectorate]

@CensusDate date = null

as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


select
       @CensusDate = 
              coalesce
                     (@CensusDate,
                           (
                           select
                                  max(BaseEncounter.CensusDate)
                           from
                                 WarehouseOLAPMergedV2.OPWL.BaseEncounter 
                           )
                     )


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceSiteCode
	,SourceSpecialtyCode
	,SourceConsultantCode
	,ActivityDate
	,SourceClinicCode
	,SourceContextCode
) 
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'OPWAIT'
	,DatasetStartDate = Encounter.CensusDate
	,SourceSiteCode = Encounter.SiteCode
	,SourceSpecialtyCode = Encounter.PASSpecialtyCode
	,SourceConsultantCode = Encounter.ConsultantCode
	,ActivityDate = Encounter.CensusDate
	,SourceClinicCode = Encounter.ClinicCode
	,SourceContextCode = Encounter.ContextCode
from
	WarehouseOLAPMergedV2.OPWL.BaseEncounter Encounter
where
	Encounter.CensusDate = @CensusDate


	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats
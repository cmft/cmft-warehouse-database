﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetGUM] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,SourceClinicCode
	,SourceAppointmentTypeCode
	,ActivityDate
	,SourceContextCode
) 
select
	 DatasetRecno = MergeEncounterRecno
	,DatasetCode = 'GUM'
	,DatasetStartDate = AppointmentDate
	,SourceClinicCode = ClinicID
	,SourceAppointmentTypeCode = AppointmentTypeID
	,ActivityDate = AppointmentDate
	,SourceContextCode = ContextCode

from
	WarehouseOLAPMergedV2.GUM.BaseEncounter 

		
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

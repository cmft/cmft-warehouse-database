﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetAPC]
	@ProcessAll bit = 0
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20141112		RR	Added WrkAllocateDatasetOperation & Diagnosis (replicated the process for VTE)
-- 20141112		RR	Added new field - LocalAdministrativeCategoryCode
-- 20141112		RR	Added new field - NationalAdmissionMethodCode
-- 20141112		RR	Added new field - SpecialServiceCode (Specialist Services Best Practice Tariff to identify POD - NELSD - advised by PH)
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset
truncate table Allocation.WrkAllocateDatasetOperation
truncate table Allocation.WrkAllocateDatasetDiagnosis



insert into Allocation.WrkAllocateDataset
	(
	DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DatasetEndDate
	,SourceSpecialtyCode
	,NationalSpecialtyCode
	,NationalSiteCode
	,SourceContextCode
	,Division
	,AdmissionTypeCode
	,DiagnosisCode
	,ProcedureCode
	,HRGCode
	,NationalAdministrativeCategoryCode
	,ContractSerialNo
	,NationalNeonatalLevelOfCareCode
	,FirstEpisodeInSpellIndicator
	,SourceIntendedManagementCode
	,AdmissionMethodCode
	,StartWardCode
	,EndWardCode
	,CommissionerCode
	,Postcode
	,PseudoPostcode
	,PostcodeAtDischarge
	,EpisodeStartDate
	,DateOfBirth
	,LOS
	
	,DischargeMethodCode
	,DischargeDestinationCode
	,PatientClassificationCode
	,DischargeDate
	,ProviderSpellNo
	,SourceUniqueID
	,AdmissionDate
	,SourceEncounterNo
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,NeonatalWardStay
	,DischargeDivisionCode
	,PatientCategoryCode
	,LastEpisodeInSpellIndicator
	,LocalAdministrativeCategoryCode
	,NationalAdmissionMethodCode
	,EndSiteCode
	,OnNICTWard

	,StartSiteCode
	,StartDirectorateCode
	,Ambulatory
	,AdmissionTime
	,DischargeTime
	,Age
	,AgeOnDischarge
	,EpisodeStartTime
	,EpisodeEndTime
	,AdmissionWardCode
	,DischargeWardCode
)
select
	 DatasetRecno = Encounter.MergeEncounterRecno
	,DatasetCode = 'APC'
	,DatasetStartDate = cast(Encounter.EpisodeStartTime as date)
	,DatasetEndDate = cast(Encounter.EpisodeEndTime as date)
	,SpecialtyCode = left(Specialty.SourceSpecialtyCode, 10)
	,NationalSpecialtyCode = left(Specialty.NationalSpecialtyCode, 3)
	,SiteCode = left(StartSite.NationalSiteCode, 10)
	,ContextCode = Encounter.ContextCode
	,Division = Directorate.Division
	,AdmissionMethod.AdmissionTypeCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.PrimaryProcedureCode

	,HRGCode =
		(
		select
			 HRG4Spell.HRGCode
		from
			WarehouseOLAPMergedV2.APC.HRG4Spell
		
		inner join WarehouseOLAPMergedV2.APC.BaseEncounter Spell
		on	Spell.MergeEncounterRecno = HRG4Spell.MergeEncounterRecno

		where
			Spell.ProviderSpellNo = Encounter.GlobalProviderSpellNo
		)

	,NationalAdministrativeCategoryCode = AdministrativeCategory.NationalAdministrativeCategoryCode
	,Encounter.ContractSerialNo
	,NationalNeonatalLevelOfCareCode = NeonatalLevelOfCare.NationalNeonatalLevelOfCareCode
	,FirstEpisodeInSpellIndicator = CAST(case when Admission.MergeEncounterRecno = Encounter.MergeEncounterRecno then 1 else 0 end as bit)
	,Admission.ManagementIntentionCode
	,Admission.AdmissionMethodCode
	,Encounter.StartWardTypeCode -- Admission.StartWardTypeCode
	,Encounter.EndWardTypeCode -- Discharge.EndWardTypeCode
	,Encounter.CCGCode
	,Encounter.Postcode
	,Encounter.PseudoPostcode
	,Encounter.PostcodeAtDischarge
	,Encounter.EpisodeStartDate
	,Encounter.DateOfBirth
	,Encounter.LOS
	
	,Discharge.DischargeMethodCode
	,Discharge.DischargeDestinationCode
	,Encounter.PatientClassificationCode
	,Discharge.DischargeDate
	,Encounter.ProviderSpellNo	
	,Encounter.SourceUniqueID	
	,Admission.AdmissionDate	
	,Encounter.SourceEncounterNo	
	,Encounter.GlobalProviderSpellNo	
	,Encounter.GlobalEpisodeNo
	,NeonatalWardStay =
			case
				when exists				
					(select
							1
					from
						WarehouseOLAPMergedV2.APC.BaseWardStay
					where
						Encounter.ProviderSpellNo = BaseWardStay.ProviderSpellNo
					and	BaseWardStay.WardCode = '68'
					)
				then 1
			else 0
			end
			
	,DischargeDivisionCode = DischargeDirectorate.DivisionCode
	,Encounter.PatientCategoryCode
	,LastEpisodeInSpellIndicator = 
			CAST(
				case 
					when Discharge.MergeEncounterRecno = Encounter.MergeEncounterRecno then 1 
					else 0 
				end 
			as bit)
	,LocalAdministrativeCategoryCode = Encounter.LocalAdminCategoryCode 
	,NationalAdmissionMethodCode = AdmissionMethod.NationalAdmissionMethodCode
	,Encounter.EndSiteCode

	,OnNICTWard =
		case
		when
		exists 	
			(		
			select
				1
			from
				WarehouseOLAPMergedV2.APC.BaseWardStay
			where	
				BaseWardStay.WardCode = 'NICT'
			and BaseWardStay.ProviderSpellNo = Encounter.ProviderSpellNo
			and	BaseWardStay.ContextCode = Encounter.ContextCode
			)
		then 1
		else 0
		end

	,StartSiteCode = StartSite.SourceSiteCode
	,StartDirectorateCode = StartDirectorate.StartDirectorateCode
	,Encounter.Ambulatory
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Age = CMFT.Dates.GetAge(Encounter.DateOfBirth, Encounter.EpisodeStartDate)
	,AgeOnDischarge = CMFT.Dates.GetAge(Encounter.DateOfBirth, Encounter.DischargeDate)
	,Encounter.EpisodeStartTime
	,Encounter.EpisodeEndTime
	,AdmissionWardCode = Admission.StartWardTypeCode
	,DischargeWardCode = Discharge.EndWardTypeCode

from
	WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

left outer join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

left outer join WarehouseOLAPMergedV2.APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = AdmissionReference.AdmissionMethodID

left outer join WarehouseOLAPMergedV2.WH.Site StartSite
on	StartSite.SourceSiteID = Reference.StartSiteID

left join 
	(
	select
		 MergeEncounterRecno = DatasetAllocation.DatasetRecno
		,StartDirectorateCode = StartDirectorate.AllocationCode
	from
		Allocation.DatasetAllocation

	inner join Allocation.Allocation StartDirectorate
	on	StartDirectorate.AllocationID = DatasetAllocation.AllocationID
	and	StartDirectorate.AllocationTypeID = 10 --start directorate

	where
		DatasetAllocation.DatasetCode = 'APCSD'
	) StartDirectorate
on	StartDirectorate.MergeEncounterRecno = Admission.MergeEncounterRecno

left outer join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = isnull(StartDirectorate.StartDirectorateCode, 'N/A')

left outer join WarehouseOLAPMergedV2.WH.AdministrativeCategory 
on	AdministrativeCategory.SourceAdministrativeCategoryID = Reference.AdminCategoryID

left outer join WarehouseOLAPMergedV2.APC.NeonatalLevelOfCare
on	NeonatalLevelOfCare.SourceNeonatalLevelOfCareID = Reference.NeonatalLevelOfCareID

left outer join WarehouseOLAPMergedV2.APC.IntendedManagement
on	IntendedManagement.SourceIntendedManagementID = Reference.IntendedManagementID

left join 
	(
	select
		 MergeEncounterRecno = DatasetAllocation.DatasetRecno
		,EndDirectorateCode = EndDirectorate.AllocationCode
	from
		Allocation.DatasetAllocation

	inner join Allocation.Allocation EndDirectorate
	on	EndDirectorate.AllocationID = DatasetAllocation.AllocationID
	and	EndDirectorate.AllocationTypeID = 10 --end directorate

	where
		DatasetAllocation.DatasetCode = 'APCED'
	) EndDirectorate
on	EndDirectorate.MergeEncounterRecno = Discharge.MergeEncounterRecno

left outer join WarehouseOLAPMergedV2.WH.Directorate DischargeDirectorate
on	DischargeDirectorate.DirectorateCode = isnull(EndDirectorate.EndDirectorateCode, 'N/A')

left join WarehouseOLAPMergedV2.WH.Site EndSite
on	EndSite.SourceSiteID = DischargeReference.EndSiteID

where
	@ProcessAll = 1

or	exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.APC.ProcessList
	where
		ProcessList.MergeRecno = Encounter.MergeEncounterRecno
	and	@ProcessAll = 0
	)



	
--now generate the list of procedures....
insert into Allocation.WrkAllocateDatasetOperation
	(
	 DatasetRecno
	,DatasetCode
	,SequenceNo
	,OperationCode
	)
select
	 DatasetRecno = BaseOperation.MergeEncounterRecno
	,DatasetCode = 'APC'
	,BaseOperation.SequenceNo
	,BaseOperation.OperationCode
from
	WarehouseOLAPMergedV2.APC.BaseOperation

inner join Allocation.WrkAllocateDataset
on	WrkAllocateDataset.DatasetRecno = BaseOperation.MergeEncounterRecno
and	WrkAllocateDataset.DatasetCode = 'APC'


--now generate the list of diagnoses....
insert into Allocation.WrkAllocateDatasetDiagnosis
	(
	 DatasetRecno
	,DatasetCode
	,SequenceNo
	,DiagnosisCode
	)
select
	 DatasetRecno = BaseDiagnosis.MergeEncounterRecno
	,DatasetCode = 'APC'
	,BaseDiagnosis.SequenceNo
	,BaseDiagnosis.DiagnosisCode
from
	WarehouseOLAPMergedV2.APC.BaseDiagnosis

inner join Allocation.WrkAllocateDataset
on	WrkAllocateDataset.DatasetRecno = BaseDiagnosis.MergeEncounterRecno
and	WrkAllocateDataset.DatasetCode = 'APC'



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

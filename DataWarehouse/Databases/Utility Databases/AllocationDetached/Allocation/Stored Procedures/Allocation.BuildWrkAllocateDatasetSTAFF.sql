﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetSTAFF] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,Division
	,SourceContextCode
) 
select
	 DatasetRecno = BaseStaffingLevel.StaffingLevelRecno
	,DatasetCode = 'STAFF'
	,BaseStaffingLevel.CensusDate
	,Division = DivisionCode
	,SourceContextCode = BaseStaffingLevel.ContextCode

from
	WarehouseOLAPMergedV2.APC.BaseStaffingLevel

where
      BaseStaffingLevel.ContextCode = 'CMFT||IPATH' 
      

		
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

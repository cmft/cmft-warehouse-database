﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetFFTRTN] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset


insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,WardCode
	,SourceSiteCode
	,SourceContextCode
) 
select
	 DatasetRecno = BaseFriendsFamilyTestReturn.FFTRecno
	,DatasetCode = 'FFTRTN'
	,BaseFriendsFamilyTestReturn.CensusDate
	,WardCode
	,SourceSiteCode = HospitalSiteCode
	,SourceContextCode = BaseFriendsFamilyTestReturn.ContextCode

from
	WarehouseOLAPMergedV2.PEX.BaseFriendsFamilyTestReturn

where
      BaseFriendsFamilyTestReturn.ContextCode = 'CMFT||FFTRTN'
--and	WardCode is not null    --was there to not bring AE return data through the process, but removed as rules added to rule base
      
		
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats

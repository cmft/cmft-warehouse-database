﻿CREATE procedure [Allocation].[BuildWrkAllocateDatasetCOMP] as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Allocation.WrkAllocateDataset

insert into Allocation.WrkAllocateDataset
(
	 DatasetRecno
	,DatasetCode
	,DatasetStartDate
	,DepartmentCode
	,SourceContextCode
) 
select
	 DatasetRecno = Complaint.MergeComplaintRecno
	,DatasetCode = 'COMP'
	,Complaint.ReceiptDate
	,DepartmentCode = Complaint.DepartmentCode 
	,SourceContextCode = Complaint.ContextCode

from
	WarehouseOLAPMergedV2.Complaint.BaseComplaint Complaint
where
      ContextCode = 'CMFT||ULYSS'
	
	
select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec [Audit].[WriteLogEvent]
	 @ProcedureName
	,@Stats







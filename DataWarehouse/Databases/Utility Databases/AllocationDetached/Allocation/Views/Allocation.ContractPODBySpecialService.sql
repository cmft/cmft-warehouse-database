﻿CREATE view [Allocation].[ContractPODBySpecialService]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20141112	RR created.  Request by Phil Huitson, to allocate where Specialist Service - PH provided a list of codes from Grouper (best practice)
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.DischargeDate between FromDate and coalesce(ToDate,'31 dec 9999'	) 
and WrkAllocateDataset.LOS = AllocationTemplateDataset.LOS
and exists
	(Select
		1
	from 
		WarehouseOLAPMergedV2.APC.HRG4Flag
	where 
		HRG4Flag.FlagCode = AllocationTemplateDataset.SpecialServiceCode
	and HRG4Flag.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno
	)
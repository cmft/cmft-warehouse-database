﻿CREATE view [Allocation].[ContractFlagGreenCard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,Priority = AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

inner join WarehouseOLAPMergedV2.OP.AppointmentType
on	AppointmentType.SourceAppointmentTypeCode = WrkAllocateDataset.SourceAppointmentTypeCode

where
	upper(AppointmentType.SourceAppointmentType) like '%' + AllocationTemplateDataset.SourceAppointmentType + '%'
and	WrkAllocateDataset.SourceContextCode = AllocationTemplateDataset.SourceContextCode
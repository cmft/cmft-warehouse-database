﻿CREATE view [Allocation].[VTEByAnaesthetic]
as

----------------------------------------------------------------------------
---- Copyright Gecko Technologies Ltd. 2003    --
----------------------------------------------------------------------------

select distinct
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

inner join Allocation.WrkAllocateDatasetOperation
on	WrkAllocateDatasetOperation.DatasetRecno = WrkAllocateDataset.DatasetRecno
and	WrkAllocateDatasetOperation.DatasetCode = WrkAllocateDataset.DatasetCode
and	WrkAllocateDatasetOperation.OperationCode = AllocationTemplateDataset.ProcedureCode

inner join WarehouseOLAPMergedV2.APC.BaseEncounter Base
on	Base.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterProcedureDetail
on	BaseEncounterProcedureDetail.EncounterRecno = Base.EncounterRecno
and	BaseEncounterProcedureDetail.EncounterContextCode = Base.ContextCode

inner join WarehouseOLAPMergedV2.Theatre.BaseProcedureDetail
on	BaseProcedureDetail.SourceUniqueID = BaseEncounterProcedureDetail.ProcedureDetailSourceUniqueID
and	BaseProcedureDetail.ContextCode = BaseEncounterProcedureDetail.ProcedureDetailContextCode

inner join WarehouseOLAPMergedV2.Theatre.BaseOperationDetail
on	BaseOperationDetail.SourceUniqueID = BaseProcedureDetail.OperationDetailSourceUniqueID
and	BaseOperationDetail.ContextCode = BaseEncounterProcedureDetail.ProcedureDetailContextCode

inner join WarehouseOLAPMergedV2.Theatre.BasePatientBooking
on	BasePatientBooking.SourceUniqueID = BaseOperationDetail.PatientBookingSourceUniqueID
and	BasePatientBooking.ContextCode = BaseEncounterProcedureDetail.ProcedureDetailContextCode

left join Warehouse.Theatre.Anaesthetic
on	Anaesthetic.AnaestheticCode = BasePatientBooking.AnaestheticCode

inner join Allocation.Allocation
on	Allocation.AllocationTypeID = 3
and	Allocation.AllocationCode =
	case
	when 
		datediff(
			 minute
			,coalesce(AnaestheticInductionTime , InAnaestheticTime)
			,InRecoveryTime
		) < 90 then '90'
	when Anaesthetic.AnaestheticCode1 = 'LA' and WrkAllocateDataset.PatientCategoryCode = 'DC' then 'LA'

	when Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' ) then --'RALASE'
		(
		select
			RuleBaseAllocation.AllocationCode
		from
			Allocation.Allocation RuleBaseAllocation
		where
			RuleBaseAllocation.AllocationID = AllocationTemplateDataset.AllocationID
		)

	end

where
	WrkAllocateDataset.GlobalEpisodeNo = 1
and
	(
		(
			Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' )
		and	WrkAllocateDataset.NationalSpecialtyCode in ( '160' , '140' , '120' )
		and	left(WrkAllocateDataset.ProcedureCode , 1) not in ('C' , 'D')
		)
	or
		(
			Anaesthetic.AnaestheticCode1 = 'LA'
		and	(
				WrkAllocateDataset.NationalSpecialtyCode not in ( '160' , '140' , '120' )
			or	WrkAllocateDataset.PatientCategoryCode = 'DC'
			)
		)
	)
﻿CREATE view [Allocation].[ContractFlagByTreatmentFunctionSourceAppointmentType]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	(
		AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
	or	AllocationTemplateDataset.NationalSpecialtyCode is null
	)
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

and exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.AppointmentType
	where
		AppointmentType.SourceAppointmentTypeCode = WrkAllocateDataset.SourceAppointmentTypeCode
	and	AppointmentType.SourceAppointmentType like '%' + AllocationTemplateDataset.SourceAppointmentType + '%'
	)

and WrkAllocateDataset.DatasetStartDate between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
	and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	)
﻿CREATE view [Allocation].[ContractHRGFirstAudiologyFitting]

as

with FittingCTE

as

(
	select
		BaseEncounter.MergeEncounterRecno
		,BaseEncounter.SourcePatientNo
		,BaseEncounter.SourceEncounterNo
		,BaseEncounter.ContextCode
		,BaseEncounter.AppointmentTime
	from
		WarehouseOLAPMergedV2.OP.BaseEncounter

	inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference
	on	 BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.OP.AttendanceStatus
	on	AttendanceStatus.SourceAttendanceStatusID = BaseEncounterReference.AttendanceStatusID

	where
		ReferralSpecialtyCode in ('AQP','RAQP')
	and ClinicCode in ('MODUNFIT','MODBIFIT','MODUFITS','MODBFITS') -- fitting appt
	and	AttendanceStatus.NationalAttendanceStatusCode in ('5','6')
)

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset WITH (NOEXPAND)
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode
and	AllocationTemplateDataset.Active = 1
and	AllocationTemplateDataset.Template = 'Allocation.ContractHRGFirstAudiologyFitting'

inner join WarehouseOLAPMergedV2.OP.BaseEncounter
on WrkAllocateDataset.DatasetRecno = BaseEncounter.MergeEncounterRecno

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	AllocationTemplateDataset.NationalAttendanceStatusCode = WrkAllocateDataset.NationalAttendanceStatusCode
and	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	not	exists
			(
				select
					1
				from
					FittingCTE Previous

				where
					Previous.SourcePatientNo = BaseEncounter.SourcePatientNo
				and	Previous.SourceEncounterNo = BaseEncounter.SourceEncounterNo
				and Previous.ContextCode = BaseEncounter.ContextCode
				and	(
						Previous.AppointmentTime < BaseEncounter.AppointmentTime
					or	(
							Previous.AppointmentTime = BaseEncounter.AppointmentTime
						and	Previous.MergeEncounterRecno < BaseEncounter.MergeEncounterRecno
						)
					)
				
			)
and exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.OP.BaseEncounter Assessment

			inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference AssessmentReference
			on	 AssessmentReference.MergeEncounterRecno = Assessment.MergeEncounterRecno

			inner join WarehouseOLAPMergedV2.OP.AttendanceStatus
			on	AttendanceStatus.SourceAttendanceStatusID = AssessmentReference.AttendanceStatusID

			where
				ReferralSpecialtyCode in ('AQP','RAQP')
			and	AttendanceStatus.NationalAttendanceStatusCode in ('5','6')
			and Assessment.ClinicCode in ('MODDRC','MODDRC','MODDRCS','MODEMIS','MODEMNP') -- assessment appt	
			and	Assessment.AppointmentDate >= '1 apr 2014'
			and Assessment.SourcePatientNo = BaseEncounter.SourcePatientNo
			and	Assessment.SourceEncounterNo = BaseEncounter.SourceEncounterNo
			and Assessment.ContextCode = BaseEncounter.ContextCode
			)
﻿CREATE view [Allocation].[ContractPODAudiology]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

inner join WarehouseOLAPMergedV2.OP.BaseEncounter
on WrkAllocateDataset.DatasetRecno = BaseEncounter.MergeEncounterRecno

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	AllocationTemplateDataset.NationalAttendanceStatusCode = WrkAllocateDataset.NationalAttendanceStatusCode
and exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.OP.BaseEncounter Assessment

			inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference AssessmentReference
			on	 AssessmentReference.MergeEncounterRecno = Assessment.MergeEncounterRecno

			inner join WarehouseOLAPMergedV2.OP.AttendanceStatus
			on	AttendanceStatus.SourceAttendanceStatusID = AssessmentReference.AttendanceStatusID

			where
				ReferralSpecialtyCode in ('AQP','RAQP')
			and	AttendanceStatus.NationalAttendanceStatusCode in ('5','6')
			and Assessment.ClinicCode in ('MODDRC','MODDRC','MODDRCS','MODEMIS','MODEMNP') -- assessment appt	
			and	Assessment.AppointmentDate >= '1 apr 2014'
			and Assessment.SourcePatientNo = BaseEncounter.SourcePatientNo
			and	Assessment.SourceEncounterNo = BaseEncounter.SourceEncounterNo
			and Assessment.ContextCode = BaseEncounter.ContextCode
			)
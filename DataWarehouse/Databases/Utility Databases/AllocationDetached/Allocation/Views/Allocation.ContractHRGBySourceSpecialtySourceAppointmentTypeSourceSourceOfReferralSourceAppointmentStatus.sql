﻿CREATE view [Allocation].[ContractHRGBySourceSpecialtySourceAppointmentTypeSourceSourceOfReferralSourceAppointmentStatus]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.OP.AppointmentType
	where
		AppointmentType.SourceAppointmentTypeCode = WrkAllocateDataset.SourceAppointmentTypeCode
	and	AppointmentType.SourceAppointmentType like '%' + AllocationTemplateDataset.SourceAppointmentType + '%'
	)
and	
	(AllocationTemplateDataset.SourceSourceOfReferralCode = WrkAllocateDataset.SourceSourceOfReferralCode
	or -- 20141120 RR changed an and to an or as advised by PH
	AllocationTemplateDataset.SourceAppointmentStatusCode = WrkAllocateDataset.SourceAppointmentStatusCode
	)
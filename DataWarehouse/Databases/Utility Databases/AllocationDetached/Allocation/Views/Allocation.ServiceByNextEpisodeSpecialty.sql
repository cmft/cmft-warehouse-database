﻿CREATE view [Allocation].[ServiceByNextEpisodeSpecialty]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

inner join WarehouseOLAPMergedV2.APC.BaseEncounter Encounter
on	Encounter.MergeEncounterRecno = WrkAllocateDataset.DatasetRecno
and	Encounter.FirstEpisodeInSpellIndicator = 1

inner join WarehouseOLAPMergedV2.APC.BaseEncounter NextEncounter
on	NextEncounter.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	NextEncounter.GlobalEpisodeNo = Encounter.GlobalEpisodeNo + 1
and	NextEncounter.SpecialtyCode <> Encounter.SpecialtyCode

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyCode = NextEncounter.SpecialtyCode
and	Specialty.SourceContextCode = NextEncounter.ContextCode

inner join WarehouseOLAPMergedV2.WH.ServiceBase
on	ServiceBase.NationalSpecialtyCode = Specialty.NationalSpecialtyCode
and	ServiceBase.Active = 1
and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.WH.ServiceBase Previous
	where
		Previous.NationalSpecialtyCode = Specialty.NationalSpecialtyCode
	and	Previous.Active = 1
	and	(
			Previous.Priority < ServiceBase.Priority
		or	(
				Previous.Priority = ServiceBase.Priority
			and	Previous.ServiceID < ServiceBase.ServiceID

			)
		)
	)

inner join Allocation.Allocation
on	Allocation.AllocationCode = ServiceBase.ServiceID
and	Allocation.AllocationTypeID = 2 --Service

where
	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.Division = WrkAllocateDataset.Division
and	AllocationTemplateDataset.AdmissionTypeCode = WrkAllocateDataset.AdmissionTypeCode


﻿CREATE TABLE [prisons].[GPs](
	[Practice Code] [nvarchar](255) NULL,
	[GP Code] [nvarchar](255) NULL,
	[GP Name] [nvarchar](255) NULL,
	[HUB] [nvarchar](255) NULL,
	[Prison name] [nvarchar](255) NULL,
	[Address 1] [nvarchar](255) NULL,
	[Address 2] [nvarchar](255) NULL,
	[Address 3] [nvarchar](255) NULL,
	[Address 4] [nvarchar](255) NULL,
	[Address 5] [nvarchar](255) NULL,
	[Postcode] [nvarchar](255) NULL,
	[ORG_CODE] [nvarchar](255) NULL
) ON [PRIMARY]
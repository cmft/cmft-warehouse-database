﻿
USE [master]
GO

/****** Object:  Database [Organisation]    Script Date: 25/08/2015 14:06:40 ******/
CREATE DATABASE [Organisation] ON  PRIMARY 
( NAME = N'Organisation', FILENAME = N'F:\Data\Organisation.mdf' , SIZE = 1790976KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Organisation_log', FILENAME = N'G:\Log\Organisation.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [Organisation] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Organisation].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO

ALTER DATABASE [Organisation] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [Organisation] SET ANSI_NULLS OFF
GO

ALTER DATABASE [Organisation] SET ANSI_PADDING OFF
GO

ALTER DATABASE [Organisation] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [Organisation] SET ARITHABORT OFF
GO

ALTER DATABASE [Organisation] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [Organisation] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [Organisation] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [Organisation] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [Organisation] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [Organisation] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [Organisation] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [Organisation] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [Organisation] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [Organisation] SET  DISABLE_BROKER
GO

ALTER DATABASE [Organisation] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [Organisation] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [Organisation] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [Organisation] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [Organisation] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [Organisation] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [Organisation] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [Organisation] SET RECOVERY SIMPLE
GO

ALTER DATABASE [Organisation] SET  MULTI_USER
GO

ALTER DATABASE [Organisation] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [Organisation] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'Organisation', N'ON'
GO

USE [Organisation]
GO

/****** Object:  Table [dbo].[Export File Locations]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Extended Postcode]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[General Dental Practice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[General Dental Practitioner]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[General Medical Practice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[General Medical Practitioner]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[GridAll]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Health Authority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Hospice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Local Authority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NHS Trust]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[NHS Trust Site]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Non NHS Organisation]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[OldGP]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[PCT Site]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Primary Care Organisation]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Regional Office]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[ReleaseVersion]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Safe Haven]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[SchemaChangeLog]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Special Health Authority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Export File Locations]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Extended Postcode]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[General Dental Practice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[General Dental Practitioner]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[General Medical Practice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[General Medical Practitioner]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Health Authority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Hospice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Hospital Consultants]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[Import Process List]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Import Settings]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Local Authority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[ngpcur]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[NHS Trust]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[NHS Trust Site]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[niorg]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[Non NHS Organisation]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[npraccur]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[OSLAUA]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[PCT Site]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Primary Care Organisation]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Regional Office]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[ReleaseVersion]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[Safe Haven]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[scotgp]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[scotmem]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[scotorg]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[scotprac]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [ODS].[Special Health Authority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [ODS].[whbs]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [prisons].[GPs]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ODS].[CCG]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[CCGPostcode]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[CCGPractice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[DentalPractice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[DGp]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Gp]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[HealthAuthority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[LocalAuthority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[PCT]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Postcode]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Practice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Site]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[Trust]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ODS].[Gp]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ODS].[HealthAuthority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ODS].[Postcode]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ODS].[Practice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [ODS].[RegionalOffice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateGDPractice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateGDPractitioner]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateGMPractice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateGMPractitioner]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateHAuthority]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateHospice]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateNHSTRust]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateNHSTRustSite]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateNonNHSOrg]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updatePCO]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updatePCTSite]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [ODS].[updateSpecialHA]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  DdlTrigger [tr_DDL_SchemaChangeLog]    Script Date: 25/08/2015 14:06:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [Organisation] SET  READ_WRITE
GO

﻿CREATE TABLE [ODS].[Local Authority](
	[Local Authority Code] [nvarchar](4) NULL,
	[Local Authority Name] [nvarchar](100) NULL
) ON [PRIMARY]
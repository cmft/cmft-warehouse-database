﻿CREATE TABLE [ODS].[Hospital Consultants](
	[GMC Code] [varchar](50) NULL,
	[Practitioner Code] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[Initials] [varchar](50) NULL,
	[Sex] [varchar](50) NULL,
	[Speciality Function Code] [varchar](50) NULL,
	[Practitioner Type] [varchar](50) NULL,
	[Location Organisation Code] [varchar](50) NULL,
	[Column 8] [varchar](50) NULL,
	[Column 9] [varchar](50) NULL,
	[Column 10] [varchar](50) NULL,
	[Column 11] [varchar](50) NULL,
	[Column 12] [varchar](50) NULL
) ON [PRIMARY]
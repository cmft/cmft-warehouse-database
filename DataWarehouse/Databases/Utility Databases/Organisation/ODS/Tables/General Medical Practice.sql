﻿CREATE TABLE [ODS].[General Medical Practice](
	[Organisation Code] [nvarchar](8) NOT NULL,
	[Organisation Name] [nvarchar](100) NULL,
	[RO Code] [nvarchar](3) NULL,
	[HA Code] [nvarchar](3) NULL,
	[Address Line 1] [nvarchar](35) NULL,
	[Address Line 2] [nvarchar](35) NULL,
	[Address Line 3] [nvarchar](35) NULL,
	[Address Line 4] [nvarchar](35) NULL,
	[Address Line 5] [nvarchar](35) NULL,
	[Postcode] [nvarchar](8) NULL,
	[Open Date] [nvarchar](8) NULL,
	[Close Date] [nvarchar](8) NULL,
	[Status Code] [nvarchar](1) NULL,
	[Organisation Sub Type Code] [nvarchar](1) NULL,
	[Parent Organisation Code] [nvarchar](6) NULL,
	[Join Parent Date] [nvarchar](8) NULL,
	[Left Parent Date] [nvarchar](8) NULL,
	[Contact Telephone Number] [nvarchar](12) NULL,
	[Contact Name] [nvarchar](50) NULL,
	[Address Type] [nvarchar](2) NULL,
	[Field0] [nvarchar](10) NULL,
	[Amended Record Indicator] [nvarchar](1) NULL,
	[Wave Number] [nvarchar](2) NULL,
	[Current GPFH or PCG Code] [nvarchar](5) NULL,
	[Current GPFH Type] [nvarchar](1) NULL,
	[Field27] [nvarchar](1) NULL,
	[Field28] [nvarchar](1) NULL,
 CONSTRAINT [PK_General Medical Practice_1] PRIMARY KEY CLUSTERED 
(
	[Organisation Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [ODS].[scotmem](
	[Practitioner Code] [varchar](50) NULL,
	[Parent Organisation Code] [varchar](50) NULL,
	[Parent Organisation Type] [varchar](50) NULL,
	[Join Parent Date] [varchar](50) NULL,
	[Left Parent Date] [varchar](50) NULL,
	[Amended Record Indicator] [varchar](50) NULL
) ON [PRIMARY]
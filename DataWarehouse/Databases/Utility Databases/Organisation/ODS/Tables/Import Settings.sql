﻿CREATE TABLE [ODS].[Import Settings](
	[ReleaseDate] [datetime] NULL,
	[ReleaseCSVRootFolder] [nvarchar](255) NULL,
	[DisplayImportWarnings] [bit] NOT NULL,
	[DisplayGridallWarning] [bit] NOT NULL
) ON [PRIMARY]
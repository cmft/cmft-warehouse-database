﻿CREATE TABLE [ODS].[Safe Haven](
	[Organisation Code] [nvarchar](5) NULL,
	[Organisation Name] [nvarchar](100) NULL,
	[Safe Haven Contact] [nvarchar](40) NULL,
	[Address Line 1] [nvarchar](35) NULL,
	[Address Line 2] [nvarchar](35) NULL,
	[Address Line 3] [nvarchar](35) NULL,
	[Address Line 4] [nvarchar](35) NULL,
	[Postcode] [nvarchar](8) NULL,
	[Telephone Number] [nvarchar](40) NULL,
	[Fax Number] [nvarchar](24) NULL,
	[Safe Haven Comment] [nvarchar](40) NULL
) ON [PRIMARY]
﻿CREATE TABLE [ODS].[Import Process List](
	[ID] [int] NOT NULL,
	[Frequency] [nvarchar](255) NULL,
	[CSV] [nvarchar](255) NULL,
	[Table] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[Source] [nvarchar](255) NULL,
	[Specification] [nvarchar](255) NULL,
	[Import] [bit] NOT NULL
) ON [PRIMARY]
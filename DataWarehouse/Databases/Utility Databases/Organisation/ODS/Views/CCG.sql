﻿CREATE view [ODS].[CCG] as

SELECT 
	 [Organisation Code] OrganisationCode
	,[Organisation Name] Organisation
	,[RO Code] ROCode
	,[HA Code] HACode
	,[Address Line 1] Address1
	,[Address Line 2] Address2
	,[Address Line 3] Address3
	,[Address Line 4] Address4
	,[Address Line 5] Address5
	,[Postcode] Postcode
	,[Open Date] OpenDate
	,[Close Date] CloseDate
	,[Status Code] StatusCode
	,[Organisation Sub Type Code] OrganisationSubTypeCode
	,[Parent Organisation Code] ParentOrganisationCode
	,[Join Parent Date] JoinParentDate
	,[Left Parent Date] LeftParentDate
	,[Contact Telephone Number] ContactTelNo
	,[Contact Name] ContactName
	,[Address Type] AddressType
	,[Field0]
	,[Amended Record Indicator] AmendedRecordIndicator
	,[Wave Number] WaveNumber
	,[Current GPFH or PCG Code] CurrentGPFHorPCGCode
	,[Current GPFH Type] CurrentGPFHType
	,[Field27]
	,[Field28] 
FROM
	[ODS].[Primary Care Organisation]
where
	[Organisation Sub Type Code] = 'C'
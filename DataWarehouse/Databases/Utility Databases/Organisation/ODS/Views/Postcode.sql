﻿CREATE view [ODS].[Postcode] as
SELECT 
	 [Postcode]
	,[Local Authority] LocalAuthorityCode
	,[Health Authority of Residence] DistrictOfResidence
	,[PCG of Residence] CCGCode
FROM
	[ODS].[Extended Postcode]
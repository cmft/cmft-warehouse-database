﻿create proc [ODS].[updateHAuthority]

as



SET DATEFORMAT dmy;

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

 

merge [ODS].[Health Authority] target
using 
	(
	 		SELECT        [Organisation Code], [Organisation Name], [RO Code], [HA Code], [Address Line 1], [Address Line 2], [Address Line 3], [Address Line 4], [Address Line 5], Postcode, 
                         [Open Date], [Close Date], [Status Code], [Organisation Sub Type Code], [Parent Organisation Code], [Join Parent Date], [Left Parent Date], 
                         [Contact Telephone Number], [Contact Name], [Address Type], Field0, [Amended Record Indicator], [Wave Number], [Current GPFH or PCG Code], [Current GPFH Type], 
                         Field27, Field28
			FROM            orgcurr.dbo.[Health Authority] 
			--where [amended record indicator]=1
 
		) source
	on	source.[Organisation Code] = target.[Organisation Code]
--	and source.[Organisation Name] = target.[organisation name]
	and source.[Open Date] = target.[Open Date]
--	and source.[close Date] = target.[close Date]
	 

	when not matched 
	then
		insert
			( [Organisation Code], [Organisation Name], [RO Code], [HA Code], [Address Line 1], [Address Line 2], [Address Line 3], [Address Line 4], [Address Line 5], Postcode, 
                         [Open Date], [Close Date], [Status Code], [Organisation Sub Type Code], [Parent Organisation Code], [Join Parent Date], [Left Parent Date], 
                         [Contact Telephone Number], [Contact Name], [Address Type], Field0, [Amended Record Indicator], [Wave Number], [Current GPFH or PCG Code], [Current GPFH Type], 
                         Field27, Field28
			)

		values
			(
			[Organisation Code], [Organisation Name], [RO Code], [HA Code], [Address Line 1], [Address Line 2], [Address Line 3], [Address Line 4], [Address Line 5], Postcode, 
                         [Open Date], [Close Date], [Status Code], [Organisation Sub Type Code], [Parent Organisation Code], [Join Parent Date], [Left Parent Date], 
                         [Contact Telephone Number], [Contact Name], [Address Type], Field0, [Amended Record Indicator], [Wave Number], [Current GPFH or PCG Code], [Current GPFH Type], 
                         Field27, Field28
			)

  

	when matched
	then 
		update 
		set
		 --[Organisation Code] = source.[Organisation Code],
		 --[Organisation Name]  = source.[Organisation Name],  
		 [RO Code]= source.[RO Code],  
		 [HA Code]= source.[HA Code],  
		 [Address Line 1]= source.[Address Line 1],  
		 [Address Line 2]= source.[Address Line 2],  
		 [Address Line 3]= source.[Address Line 3],  
		 [Address Line 4]= source.[Address Line 4],  
		 [Address Line 5]= source.[Address Line 5],  
		 Postcode= source.Postcode, 
         [Open Date]= source.[Open Date],  
		 [Close Date]= source.[Close Date],  
		 [Status Code]= source.[Status Code],  
		 [Organisation Sub Type Code]= source.[Organisation Sub Type Code],  
		 [Parent Organisation Code]= source.[Parent Organisation Code],
		 [Join Parent Date]= source.[Join Parent Date],  
		 [Left Parent Date]= source.[Left Parent Date], 
         [Contact Telephone Number]= source.[Contact Telephone Number],  
		 [Contact Name]= source.[Contact Name],  
		 [Address Type]= source.[Address Type],  
		 Field0= source.Field0,  
		 [Amended Record Indicator]= source.[Amended Record Indicator], 
		 [Wave Number] = source.[Wave Number], 
		 [Current GPFH or PCG Code] = source.[Current GPFH or PCG Code], 
		 [Current GPFH Type] = source.[Current GPFH Type], 
         Field27= source.Field27, 
		 Field28 = source.Field28



output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary
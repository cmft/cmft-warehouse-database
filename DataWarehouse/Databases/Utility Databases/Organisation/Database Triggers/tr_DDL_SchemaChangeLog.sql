﻿CREATE TRIGGER [tr_DDL_SchemaChangeLog] ON DATABASE 
FOR DDL_DATABASE_LEVEL_EVENTS AS 

    SET NOCOUNT ON

    DECLARE @data XML
    DECLARE @schema sysname
    DECLARE @object sysname
    DECLARE @eventType sysname

    SET @data = EVENTDATA()
    SET @eventType = @data.value('(/EVENT_INSTANCE/EventType)[1]', 'sysname')
    SET @schema = @data.value('(/EVENT_INSTANCE/SchemaName)[1]', 'sysname')
    SET @object = @data.value('(/EVENT_INSTANCE/ObjectName)[1]', 'sysname') 

    INSERT [dbo].[SchemaChangeLog] 
        (
        [CreateDate],
        [LoginName], 
        [ComputerName],
        [DBName],
        [SQLEvent], 
        [Schema], 
        [ObjectName], 
        [SQLCmd], 
        [XmlEvent]
        ) 
    SELECT
        GETDATE(),
        SUSER_NAME(), 
		HOST_NAME(),   
        @data.value('(/EVENT_INSTANCE/DatabaseName)[1]', 'sysname'),
        @eventType, 
        @schema, 
        @object, 
        @data.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'NVARCHAR(MAX)'), 
        @data
;
GO

ENABLE TRIGGER [tr_DDL_SchemaChangeLog] ON DATABASE
﻿/****** Object:  DatabaseRole [CMFT Analyst]    Script Date: 25/08/2015 14:06:40 ******/
CREATE ROLE [CMFT Analyst]
GO
ALTER ROLE [CMFT Analyst] ADD MEMBER [cmmc\tom.smith]
GO
ALTER ROLE [CMFT Analyst] ADD MEMBER [CMMC\Infounit members]
GO
ALTER ROLE [CMFT Analyst] ADD MEMBER [CMMC\Helen.Shackleton]
GO
ALTER ROLE [CMFT Analyst] ADD MEMBER [CMMC\Diane.Thomas]
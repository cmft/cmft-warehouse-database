﻿ALTER ROLE [db_datareader] ADD MEMBER [MHTest]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Wendy.Collier]
GO
ALTER ROLE [db_datareader] ADD MEMBER [cmmc\tom.smith]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Thomas.Drury]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Sec - Warehouse BI Developers]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Sec - SP Commissioning]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Phil.Orrell]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\peter.graham]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Mohamed.Athman]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Helen.Shackleton]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\gordon.fenton]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Dusia.Romaniuk]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\Diane.Thomas]
GO
ALTER ROLE [db_datareader] ADD MEMBER [CMMC\cmftslam]
GO
﻿CREATE TABLE [dbo].[Extended Postcode](
	[Postcode] [nvarchar](8) NOT NULL,
	[Local Authority] [nvarchar](50) NULL,
	[Health Authority of Residence] [nvarchar](3) NULL,
	[PCG of Residence] [nvarchar](5) NULL,
 CONSTRAINT [PK_Extended Postcode] PRIMARY KEY CLUSTERED 
(
	[Postcode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
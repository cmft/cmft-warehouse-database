﻿CREATE TABLE [dbo].[Local Authority](
	[Local Authority Code] [nvarchar](4) NOT NULL,
	[Local Authority Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_Local Authority] PRIMARY KEY CLUSTERED 
(
	[Local Authority Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
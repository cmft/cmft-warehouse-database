﻿CREATE view [dbo].[CCGPostcode] as
SELECT 
	 [Extended Postcode].[Postcode]
	,[Local Authority] LocalAuthorityCode
	,[Health Authority of Residence] DistrictOfResidence
	,[PCG of Residence] CCGCode
FROM
	[ODS].[Extended Postcode]

inner join ODS.CCG
on	CCG.OrganisationCode = [Extended Postcode].[PCG of Residence]
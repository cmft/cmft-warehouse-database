﻿CREATE view [dbo].[Postcode] as
SELECT 
	 [Postcode]
	,[Local Authority] LocalAuthorityCode
	,[Health Authority of Residence] DistrictOfResidence
	,[PCG of Residence] PCTCode
FROM
	[dbo].[Extended Postcode]
﻿
USE [master]
GO

/****** Object:  Database [ReferenceMap]    Script Date: 25/08/2015 16:34:53 ******/
CREATE DATABASE [ReferenceMap] ON  PRIMARY 
( NAME = N'ReferenceMap', FILENAME = N'J:\Data\ReferenceMap.mdf' , SIZE = 4857024KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ReferenceMap_log', FILENAME = N'K:\Logs\ReferenceMap.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [ReferenceMap] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ReferenceMap].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [ReferenceMap] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [ReferenceMap] SET ANSI_NULLS OFF
GO

ALTER DATABASE [ReferenceMap] SET ANSI_PADDING OFF
GO

ALTER DATABASE [ReferenceMap] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [ReferenceMap] SET ARITHABORT OFF
GO

ALTER DATABASE [ReferenceMap] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [ReferenceMap] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [ReferenceMap] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [ReferenceMap] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [ReferenceMap] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [ReferenceMap] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [ReferenceMap] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [ReferenceMap] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [ReferenceMap] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [ReferenceMap] SET  DISABLE_BROKER
GO

ALTER DATABASE [ReferenceMap] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [ReferenceMap] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [ReferenceMap] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [ReferenceMap] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [ReferenceMap] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [ReferenceMap] SET READ_COMMITTED_SNAPSHOT ON
GO

ALTER DATABASE [ReferenceMap] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [ReferenceMap] SET RECOVERY SIMPLE
GO

ALTER DATABASE [ReferenceMap] SET  MULTI_USER
GO

ALTER DATABASE [ReferenceMap] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [ReferenceMap] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'ReferenceMap', N'ON'
GO

USE [ReferenceMap]
GO

/****** Object:  Table [AE].[SourceUniqueIDMap]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[Attribute]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[AttributeContext]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Map].[AttributeXref]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[ColumnBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[ColumnType]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[Context]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[DatabaseBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[DatasetBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[Encounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Map].[SchemaBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[ServerBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[TableBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[TImportMAPValueXref]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [Map].[Value]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [Map].[AttributeValue]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[AttributeXrefValue]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  UserDefinedFunction [Map].[GetAttributeValue]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[AEDiagnosis]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[AEEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[AEInvestigation]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[AETreatment]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCAugmentedCarePeriod]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCCriticalCarePeriod]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCDiagnosis]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCOperation]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCWait]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCWaitingList]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[APCWardStay]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[Consultant]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[OPEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[OPWait]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[RFEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Cen].[xxxxOPWaitingList]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[AttributeContextColumn]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[DebugLookup]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[Schemata]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[Schemata2]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[TExportMAPValueXref]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[ValueHierarchy]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Map].[ValueLookup]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[AEDiagnosis]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[AEEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[AEInvestigation]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[AETreatment]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[APCCriticalCarePeriod]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[APCDiagnosis]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[APCEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[APCOperation]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[APCWait]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[APCWaitingList]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[APCWardStay]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[Consultant]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[OPEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[OPWait]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[OPWaitingList]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Tra].[RFEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  StoredProcedure [Map].[BuildAEEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildAPCEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildAPCWait]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildAPCWaitingList]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildAPCWardStay]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildMissingNonBaseContextValues]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildMissingValues]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildMissingXrefs]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildOPEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildOPWait]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[BuildRFEncounter]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[DeleteColmnBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[DeleteDuplicateXrefs]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[GetInformationSchemaColumns]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[InsertAttributeContext]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[InsertColmnBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[LoadMAP]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[LoadMAPAttribute]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[LoadMAPAttributeContext]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[LoadMAPAttributeXref]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[LoadMAPAttributeXref1]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[LoadMAPContext]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[LoadMAPValue]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Map].[UpdateColmnBase]    Script Date: 25/08/2015 16:34:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [ReferenceMap] SET  READ_WRITE
GO

﻿CREATE view [Tra].[OPEncounter] as


SELECT
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.UniqueBookingReferenceNo
	,Encounter.PathwayId
	,Encounter.PathwayIdIssuerCode
	,Encounter.RTTStatusCode
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.DistrictNo
	,Encounter.TrustNo
	,Encounter.CasenoteNo
	,Encounter.DistrictNoOrganisationCode
	,Encounter.NHSNumber
	,Encounter.NHSNumberStatusId
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.Postcode
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.CarerSupportIndicator
	,Encounter.EthnicCategoryCode
	,Encounter.MaritalStatusCode
	,Encounter.AttendanceID
	,Encounter.AdminCategoryCode
	,Encounter.DNACode
	,CancelledbyCode =
	
		CASE
		WHEN EXISTS
						(
						SELECT
							1
						FROM
							TraffordReferenceData.dbo.UGContactReason
						WHERE
							conrsn_code IN ('T-HSusp', 'T-HosSus','T-WLSus', 'HospCan' ,'HospCanc', 'T-HCan', 'T-HCOD', 'T-HosC', 'T-HRWL', 'T-HBE', 'T-HCanPr', 'T-HDV', 'T-HError', 'T-HNV', 'T-HosNV')
						AND SourceUniqueID = Encounter.SourceUniqueID
						) AND AppointmentStatusCode = 'C'  THEN 'H'
		when EXISTS
						(
						SELECT
							1
						FROM
							TraffordReferenceData.dbo.UGContactReason
						WHERE
							conrsn_code IN ('G-Illnes', 'G-Ill','G-NoProc', 'T-NoProc', 'T-ProcNo', 'T-Susp', 'T-PtSus', 'T-PDNA', 'T-PtDNA', 'MDSPtCan', 'PtCan', 'T-COD', 'T-PCan', 'T-PRWL', 'T-PtCan', 'PatCanc' ,'PtCancWL', 'PtCanOd', 'T-PCOD')
						AND SourceUniqueID = Encounter.SourceUniqueID
						) AND AppointmentStatusCode = 'C'  THEN 'P'
						
						ELSE NULL END
	
	
	,Encounter.FirstAttendanceCode
	,Encounter.MedicalStaffTypeCode
	,Encounter.OperationStatusCode
	,Encounter.AttendanceOutcomeCode
	,Encounter.AppointmentDate
	,Encounter.AppointmentNote
	,Encounter.AgeOnAppointment
	,Encounter.EarliestReasonableOfferDate

	,CommissioningSerialNo = 
		CASE 
		WHEN Encounter.CommissioningSerialNo = 'X98000' THEN '5NR000'
		ELSE Encounter.CommissioningSerialNo
		END

	,Encounter.NHSServiceAgreementLineNo
	,Encounter.ProviderReferenceNo
	,Encounter.CommissionerReferenceNo
	,Encounter.ProviderCode

	,CommissionerCode = 
		CASE 
		WHEN Encounter.CommissionerCode = 'X98' THEN '5NR'
		ELSE Encounter.CommissionerCode
		END

	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.TreatmentFunctionCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.ICDDiagnosisSchemeCode

	,PrimaryDiagnosisCode =
		Diagnosis1.DiagnosisCode

	,Encounter.ReadDiagnosisSchemeCode
	,Encounter.PrimaryDiagnosisReadCode
	,Encounter.OPCSProcedureSchemeCode

	,PrimaryProcedureCode =
		Procedure1.ProcedureCode

	,PrimaryProcedureDate =
		Procedure1.ProcedureDate

	,Encounter.ReadProcedureSchemeCode
	,Encounter.PrimaryProcedureReadCode
	,Encounter.LocationClassCode
	,Encounter.SiteCode
	,Encounter.LocationTypeCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.PriorityTypeCode
	,Encounter.ServiceTypeRequestedCode
	,Encounter.SourceOfReferralCode
	,Encounter.ReferralRequestReceivedDate
	,Encounter.ReferrerCode
	,Encounter.ReferrerOrganisationCode
	,Encounter.ReferralReason
	,Encounter.LastDNAOrCancelledDate
	,Encounter.PASHRGCode
	,Encounter.HRGVersionCode
	,Encounter.PASDGVPCode
	,Encounter.PCTCode
	,Encounter.ClinicCode
	,Encounter.ClinicFunctionCode
	,Encounter.OverseasVisitorCode
	,Encounter.ResidencePCTCode
	,Encounter.DepartmentCode
	,Encounter.AppointmentStatusCode
	,Encounter.IsDeleted
	,Encounter.ContactChangeDate
	,Encounter.EpisodeChangeDate
	,Encounter.ContactChangeTime
	,Encounter.EpisodeChangeTime
	,Encounter.PASUpdateDate
	,Encounter.InterfaceCode
	,Encounter.AppointmentTypeCode
	,Encounter.OriginalPathwayId
	,Encounter.OriginalPathwayIdIssuerCode
	,Encounter.OriginalRTTStatusCode
	,Encounter.OriginalRTTStartDate
	,Encounter.OriginalRTTEndDate
	,Encounter.RTTSection
	,Encounter.AppointmentTime
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.AppTariff
	,Encounter.ProcTariff
	,Encounter.NFFTariff
	,Encounter.EPMINumber
	,Encounter.OutcomeCode
	,Encounter.APPReferralDate
	,Encounter.SourceReferralReasonCode
	,Encounter.SourceAdminCategoryCode

	,SourcePatientNo = DistrictNo

	,SubsidiaryDiagnosisCode = null -- check sequence for all these and check why primary isn't coming through?
	,SecondaryDiagnosisCode1 = Diagnosis2.DiagnosisCode
	,SecondaryDiagnosisCode2 = Diagnosis3.DiagnosisCode
	,SecondaryDiagnosisCode3 = Diagnosis4.DiagnosisCode
	,SecondaryDiagnosisCode4 = Diagnosis5.DiagnosisCode
	,SecondaryDiagnosisCode5 = Diagnosis6.DiagnosisCode
	,SecondaryDiagnosisCode6 = Diagnosis7.DiagnosisCode
	,SecondaryDiagnosisCode7 = Diagnosis8.DiagnosisCode
	,SecondaryDiagnosisCode8 = Diagnosis9.DiagnosisCode
	,SecondaryDiagnosisCode9 = Diagnosis10.DiagnosisCode
	,SecondaryDiagnosisCode10 = Diagnosis11.DiagnosisCode
	,SecondaryDiagnosisCode11 = Diagnosis12.DiagnosisCode
	,SecondaryDiagnosisCode12 = Diagnosis13.DiagnosisCode
	
	,SecondaryProcedureCode1 = Procedure2.ProcedureCode
	,SecondaryProcedureDate1 = Procedure2.ProcedureDate
	,SecondaryProcedureCode2 = Procedure3.ProcedureCode
	,SecondaryProcedureDate2 = Procedure3.ProcedureDate
	,SecondaryProcedureCode3 = Procedure4.ProcedureCode
	,SecondaryProcedureDate3 = Procedure4.ProcedureDate
	,SecondaryProcedureCode4 = Procedure5.ProcedureCode
	,SecondaryProcedureDate4 = Procedure5.ProcedureDate
	,SecondaryProcedureCode5 = Procedure6.ProcedureCode
	,SecondaryProcedureDate5 = Procedure6.ProcedureDate
	,SecondaryProcedureCode6 = Procedure7.ProcedureCode
	,SecondaryProcedureDate6 = Procedure7.ProcedureDate
	,SecondaryProcedureCode7 = Procedure8.ProcedureCode
	,SecondaryProcedureDate7 = Procedure8.ProcedureDate
	,SecondaryProcedureCode8 = Procedure9.ProcedureCode
	,SecondaryProcedureDate8 = Procedure9.ProcedureDate
	,SecondaryProcedureCode9 = Procedure10.ProcedureCode
	,SecondaryProcedureDate9 = Procedure10.ProcedureDate
	,SecondaryProcedureCode10 = Procedure11.ProcedureCode
	,SecondaryProcedureDate10 = Procedure11.ProcedureDate
	,SecondaryProcedureCode11 = Procedure12.ProcedureCode
	,SecondaryProcedureDate11 = Procedure12.ProcedureDate
	
	,AppointmentOutcomeCode =
		CASE 
		WHEN AppointmentStatusCode = 'V' THEN 'ATT'
		WHEN AppointmentStatusCode = 'D' THEN 'DNA'
		WHEN 
				AppointmentStatusCode = 'C' 
			AND	(
					DATEADD(D, 0, DATEDIFF(D, 0, AppointmentTime)) = DATEADD(D, 0, DATEDIFF(D, 0, ContactChangeTime))
				or	EXISTS
						(
						SELECT
							1
						FROM
							TraffordReferenceData.dbo.UGContactReason
						WHERE
							conrsn_code = 'T-PCOD'
						AND SourceUniqueID = Encounter.SourceUniqueID
						)
				) THEN 'CND'
		ELSE 'NR'
		END

	,ReferringConsultantCode = ConsultantCode
	
	,ReferredByCode =
		CASE
		WHEN LEFT(ReferrerCode , 1) = 'G'  THEN 'GP' 
		WHEN LEFT(ReferrerCode , 1) = 'D'  THEN 'GDP'
		WHEN LEFT(ReferrerCode , 1) = 'C'  THEN 'CONS'
		ELSE 'OTH'
		END
	
	,ContextCode = 'TRA||' + Encounter.InterfaceCode

	,ReferralSpecialtyCode = TreatmentFunctionCode /* Changed from SpecialtyCode to align with Trafford SLAM process */
	,ReferralConsultantCode = ConsultantCode

FROM
	TraffordWarehouse.dbo.OPEncounter Encounter
	
left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis1	
on	Diagnosis1.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis1.SequenceNo = 1

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis2	
on	Diagnosis2.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis2.SequenceNo = 2

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis3
on	Diagnosis3.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis3.SequenceNo = 3

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis4
on	Diagnosis4.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis4.SequenceNo = 4

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis5
on	Diagnosis5.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis5.SequenceNo = 5

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis6
on	Diagnosis6.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis6.SequenceNo = 6

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis7
on	Diagnosis7.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis7.SequenceNo = 7

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis8
on	Diagnosis8.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis8.SequenceNo = 8

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis9
on	Diagnosis9.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis9.SequenceNo = 9

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis10
on	Diagnosis10.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis10.SequenceNo = 10

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis11
on	Diagnosis11.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis11.SequenceNo = 11

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis12
on	Diagnosis12.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis12.SequenceNo = 12

left join TraffordWarehouse.dbo.OPDiagnosis Diagnosis13
on	Diagnosis13.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis13.SequenceNo = 13



left join TraffordWarehouse.dbo.OPProcedure Procedure1
on	Procedure1.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure1.SequenceNo = 1

left join TraffordWarehouse.dbo.OPProcedure Procedure2
on	Procedure2.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure2.SequenceNo = 2

left join TraffordWarehouse.dbo.OPProcedure Procedure3
on	Procedure3.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure3.SequenceNo = 3

left join TraffordWarehouse.dbo.OPProcedure Procedure4
on	Procedure4.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure4.SequenceNo = 4

left join TraffordWarehouse.dbo.OPProcedure Procedure5
on	Procedure5.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure5.SequenceNo = 5

left join TraffordWarehouse.dbo.OPProcedure Procedure6
on	Procedure6.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure6.SequenceNo = 6

left join TraffordWarehouse.dbo.OPProcedure Procedure7
on	Procedure7.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure7.SequenceNo = 7

left join TraffordWarehouse.dbo.OPProcedure Procedure8
on	Procedure8.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure8.SequenceNo = 8

left join TraffordWarehouse.dbo.OPProcedure Procedure9
on	Procedure9.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure9.SequenceNo = 9

left join TraffordWarehouse.dbo.OPProcedure Procedure10
on	Procedure10.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure10.SequenceNo = 10

left join TraffordWarehouse.dbo.OPProcedure Procedure11
on	Procedure11.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure11.SequenceNo = 11

left join TraffordWarehouse.dbo.OPProcedure Procedure12
on	Procedure12.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure12.SequenceNo = 12

left join TraffordWarehouse.dbo.OPProcedure Procedure13
on	Procedure13.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure13.SequenceNo = 13
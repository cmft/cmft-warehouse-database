﻿CREATE view [Tra].[APCCriticalCarePeriod] as

select
	 EncounterRecno
	,SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,StartTime
	,EndDate
	,EndTime
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,DermatologicalSupportDays
	,LiverSupportDays
	,NeurologicalSupportDays
	,RenalSupportDays
	,CreatedByUser
	,CreatedByTime
	,LocalIdentifier
	,LocationCode
	,StatusCode
	,TreatmentFunctionCode
	,PlannedAcpPeriod
	,Created
	,ByWhom
	,InterfaceCode
	,CasenoteNumber
	,WardCode
	,AdmissionDate
	,SiteCode
	,NHSNumber
	,UnitFunctionCode
	,ContextCode = 'TRA||MID'
FROM
	Warehouse.APC.CriticalCarePeriod CriticalCarePeriod
where 
	SiteCode IN ('RM401', 'RW3TR')
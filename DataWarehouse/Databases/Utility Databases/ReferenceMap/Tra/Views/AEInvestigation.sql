﻿CREATE view [Tra].[AEInvestigation] AS

SELECT 
	 Investigation.InvestigationRecno
	,Investigation.SourceUniqueID
	,Investigation.InvestigationDate
	,Investigation.SequenceNo
	,Investigation.InvestigationSchemeInUse
	,Investigation.InvestigationCode
	,Investigation.AESourceUniqueID
	,Investigation.SourceInvestigationCode
	,Investigation.SourceSequenceNo
	,Investigation.ResultDate
	,ContextCode = 'TRA||' + Encounter.InterfaceCode
FROM
	TraffordWarehouse.dbo.AEInvestigation Investigation

INNER JOIN TraffordWarehouse.dbo.AEEncounter Encounter
ON Investigation.AESourceUniqueID = Encounter.SourceUniqueID
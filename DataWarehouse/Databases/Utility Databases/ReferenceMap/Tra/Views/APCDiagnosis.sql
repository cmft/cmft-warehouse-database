﻿CREATE view [Tra].[APCDiagnosis]

as

select
       Diagnosis.SourceUniqueID
       --,SourceSpellNo = null
       ,SourcePatientNo = DistrictNo
       ,ProviderSpellNo
       ,SourceEncounterNo = EpisodeNo
       ,SequenceNo = cast(SequenceNo as int) - 1
       ,DiagnosisCode = rtrim(left(DiagnosisCode, 6))
       --,DiagnosisReadCode
       --,DiagnosisReadTermCode
       ,[APCSourceUniqueID] = IPSourceUniqueID
       --,SourceDiagnosisCode
       --,SourceSequenceNo
       --,Diagnosis.InterfaceCode
       --,SequenceNoCheck
       ,ContextCode = 'TRA||UG'
from
       [TraffordWarehouse].[dbo].[APCDiagnosis] Diagnosis
inner join
       [TraffordWarehouse].dbo.[APCEncounter] Encounter
on
       Diagnosis.[IPSourceUniqueID] = Encounter.[SourceUniqueID]

where
       --remove fces that exist in both Central and Trafford activity
not exists
       (
       select
              1
       from
              WarehouseOLAPMerged.APC.EncounterDuplicate
       where
              EncounterDuplicate.DuplicateContextID = (select ContextID from ReferenceMap.Map.Context where ContextCode   = 'TRA||UG')
       and    EncounterDuplicate.DuplicateRecno = Encounter.EncounterRecno
       )
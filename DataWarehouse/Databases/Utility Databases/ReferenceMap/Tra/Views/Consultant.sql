﻿CREATE view [Tra].[Consultant] as

SELECT 
	 ConsultantID = sds_code
	,ConsultantCode = cons_code
	,Consultant = surname
	,ProviderCode = 'RW3TR'
	,MainSpecialtyCode
	,Title = NULL
	,Initials = NULL
	,Surname = surname
	,ContextCode = 'TRA||UG'

FROM 
	TraffordReferenceData.dbo.INFODEPT_cab_consultants
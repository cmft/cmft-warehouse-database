﻿CREATE view [Tra].[APCEncounter] as

select -- top 1000
       Encounter.EncounterRecno
       ,Encounter.SourceUniqueID
       ,Encounter.UniqueBookingReferenceNo
       ,Encounter.PathwayId
       ,Encounter.PathwayIdIssuerCode
       ,Encounter.RTTStatusCode
       ,Encounter.RTTStartDate
       ,Encounter.RTTEndDate
       ,Encounter.DistrictNo
       ,Encounter.TrustNo
       ,CasenoteNumber = 
              isnull(Encounter.EPMINumber,'') + '/' + isnull(Encounter.ProviderSpellNo,'')
       ,Encounter.DistrictNoOrganisationCode
       ,Encounter.NHSNumber
       ,Encounter.NHSNumberStatusId
       ,Encounter.PatientTitle
       ,Encounter.PatientForename
       ,Encounter.PatientSurname
       ,Encounter.PatientAddress1
       ,Encounter.PatientAddress2
       ,Encounter.PatientAddress3
       ,Encounter.PatientAddress4
       ,Encounter.Postcode
       ,Encounter.DateOfBirth
       ,Encounter.DateOfDeath
       ,Encounter.SexCode
       ,Encounter.OverseasVisitorCode
       ,Encounter.CarerSupportIndicator
       ,Encounter.EthnicCategoryCode
       ,Encounter.MaritalStatusCode
       ,Encounter.LegalStatusClassificationCode
       ,Encounter.ProviderSpellNo
       ,Encounter.AdminCategoryCode
       ,Encounter.PatientClassificationCode
       ,Encounter.AdmissionMethodCode
       ,Encounter.AdmissionSourceCode
       ,Encounter.AdmissionDate
       ,Encounter.AgeOnAdmission
       ,Encounter.DischargeDestinationCode
       ,Encounter.DischargeMethodCode
       ,Encounter.DischargeReadyDate
       ,Encounter.DischargeDate
       ,Encounter.PredictedDischargeDate
       ,Encounter.EpisodeNo
       ,Encounter.LastEpisodeInSpellIndicator
       ,Encounter.EpisodicAdminCategoryCode
       ,Encounter.OperationStatusCode
       ,Encounter.NeonatalLevelOfCareCode
       ,Encounter.FirstRegularDayNightAdmissionFlag
       ,Encounter.PsychiatricPatientStatusCode
       ,Encounter.EpisodicLegalStatusClassificationCode
       ,Encounter.EpisodeStartDate
       ,Encounter.EpisodeEndDate
       ,Encounter.EpisodeAge
       ,Encounter.CommissioningSerialNo
       ,Encounter.NHSServiceAgreementLineNo
       ,Encounter.ProviderReferenceNo
       ,Encounter.CommissionerReferenceNo
       ,Encounter.ProviderCode
       ,Encounter.CommissionerCode
       ,Encounter.ConsultantCode
       ,Encounter.SpecialtyCode
       ,Encounter.TreatmentFunctionCode
       ,Encounter.ICDDiagnosisSchemeCode
       ,PrimaryDiagnosisCode = rtrim(left(Encounter.PrimaryDiagnosisCode, 6))
       ,Encounter.ReadDiagnosisSchemeCode
       ,Encounter.PrimaryDiagnosisReadCode
       ,Encounter.OPCSProcedureSchemeCode
       ,Encounter.PrimaryProcedureCode
       ,Encounter.PrimaryProcedureDate
       ,Encounter.ReadProcedureSchemeCode
       ,Encounter.PrimaryProcedureReadCode
       ,Encounter.StartLocationClassCode
       ,Encounter.StartSiteCode
       ,Encounter.StartLocationTypeCode
       ,Encounter.StartWardTypeCode
       ,Encounter.EndLocationClassCode
       ,Encounter.EndSiteCode
       ,Encounter.EndLocationTypeCode
       ,Encounter.EndWardTypeCode
       ,Encounter.RegisteredGpCode
       ,Encounter.RegisteredGpPracticeCode
       ,ReferrerCode =
              Encounter.SourceReferrerCode
       ,Encounter.ReferrerOrganisationCode
       ,DurationOfElectiveWait = 
              case 
                     when DurationOfElectiveWait < 0 then 0
                     else DurationOfElectiveWait
              end
       ,Encounter.ManagementIntentionCode
       ,Encounter.DecidedToAdmitDate

       ,Encounter.EarliestReasonableOfferDate
       ,Encounter.PASHRGCode
       ,Encounter.HRGVersionCode
       ,Encounter.PASDGVPCode
       ,Encounter.PCTCode
       ,Encounter.ResidencePCTCode
       ,Encounter.PASUpdateDate
       ,Encounter.InterfaceCode
       ,Encounter.IsDeleted
       ,Encounter.ConsultantEpisodeStatusCode
       ,Encounter.Created
       ,Encounter.Updated
       ,Encounter.ByWhom
       ,Encounter.OriginalPathwayId
       ,Encounter.OriginalPathwayIdIssuerCode
       ,Encounter.OriginalRTTStatusCode
       ,Encounter.OriginalRTTStartDate
       ,Encounter.OriginalRTTEndDate
       ,Encounter.EpisodeTypeCode
       ,Encounter.AdmitanceType
       ,Encounter.EncounterHRGCode
       ,Encounter.EncounterTariffRaw
       ,Encounter.EncounterTariffRatio
       ,Encounter.SpellHRGCode
       ,Encounter.SpellHRGTariff
       ,Encounter.SpellLOS
       ,Encounter.SpellExcessBedDaysTariff
       ,Encounter.EncounterTariffFinal
       ,Encounter.EPMINumber
       ,Encounter.BookingType
       ,Encounter.OutcomeCode
       ,Encounter.TheatrePatientBookingKey
       ,Encounter.ProcedureTime
       ,Encounter.TheatreCode
       ,Encounter.ChangeDate
       ,Encounter.Admissionchangedate
       ,SourceSexCode = null -- DG Commented out as seems to have disapperared?
       ,Encounter.SourceAdminCategoryCode
       ,Encounter.SourceDischargeDestinationCode
       ,Encounter.SourceDischargeMethodCode
       ,Encounter.SourceManagementIntentionCode
       ,Encounter.SourceAdmissionSourceCode
       ,Encounter.SourceAdmissionMethodCode
       ,Encounter.AttendanceOutcomeCode
       ,SourcePatientNo = DistrictNo
       ,SourceEncounterNo = EpisodeNo
       ,EpisodicGpCode =
              APCSpellHistory.RegisteredGpCode

       ,EpisodicGpPracticeCode =
              APCSpellHistory.RegisteredGpPracticeCode

       ,SecondaryDiagnosisCode1 = rtrim(left(Diagnosis2.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode2 = rtrim(left(Diagnosis3.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode3 = rtrim(left(Diagnosis4.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode4 = rtrim(left(Diagnosis5.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode5 = rtrim(left(Diagnosis6.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode6 = rtrim(left(Diagnosis7.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode7 = rtrim(left(Diagnosis8.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode8 = rtrim(left(Diagnosis9.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode9 = rtrim(left(Diagnosis10.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode10 = rtrim(left(Diagnosis11.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode11 = rtrim(left(Diagnosis12.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode12 = rtrim(left(Diagnosis13.DiagnosisCode, 6))
       ,SecondaryDiagnosisCode13 = rtrim(left(Diagnosis14.DiagnosisCode, 6))


       ,SecondaryProcedureCode1 = Procedure1.ProcedureCode
       ,SecondaryProcedureCode2 = Procedure2.ProcedureCode
       ,SecondaryProcedureCode3 = Procedure3.ProcedureCode
       ,SecondaryProcedureCode4 = Procedure4.ProcedureCode
       ,SecondaryProcedureCode5 = Procedure5.ProcedureCode
       ,SecondaryProcedureCode6 = Procedure6.ProcedureCode
       ,SecondaryProcedureCode7 = Procedure7.ProcedureCode
       ,SecondaryProcedureCode8 = Procedure8.ProcedureCode
       ,SecondaryProcedureCode9 = Procedure9.ProcedureCode
       ,SecondaryProcedureCode10 = Procedure10.ProcedureCode
       ,SecondaryProcedureCode11 = Procedure11.ProcedureCode

       ,SecondaryProcedureDate1 = Procedure1.ProcedureDate
       ,SecondaryProcedureDate2 = Procedure2.ProcedureDate
       ,SecondaryProcedureDate3 = Procedure3.ProcedureDate
       ,SecondaryProcedureDate4 = Procedure4.ProcedureDate
       ,SecondaryProcedureDate5 = Procedure5.ProcedureDate
       ,SecondaryProcedureDate6 = Procedure6.ProcedureDate
       ,SecondaryProcedureDate7 = Procedure7.ProcedureDate
       ,SecondaryProcedureDate8 = Procedure8.ProcedureDate
       ,SecondaryProcedureDate9 = Procedure9.ProcedureDate
       ,SecondaryProcedureDate10 = Procedure10.ProcedureDate
       ,SecondaryProcedureDate11 = Procedure11.ProcedureDate
       ,PatientCategoryCode = case
                     
                     --Elective Inpatient
                           when AdmissionMethodCode in 
                                  (
                                  '11' --Waiting List
                                  ,'12' --Booked
                                  ,'13' --Planned
                                  )
                           and ManagementIntentionCode in 
                                  (
                                  '1' --INPATIENT
                                  ,'3' --INTERVAL ADMISSION
                                  ,'6' --BORN IN HOSP/ON WAY
                                  )
                           then 'EL' --Elective


                     --Elective Inpatients where intended management was daycase but patient stayed overnight
                           when AdmissionMethodCode in 
                                  (
                                  '11'--Waiting List
                                  ,'12'--Booked
                                  ,'13'--Planned
                                  )
                           and ManagementIntentionCode = '2' -- DAY CASE
                           and    Encounter.AdmissionDate < Encounter.DischargeDate
                           then 'EL'--Elective


                     --Elective Daycase
                           when AdmissionMethodCode in 
                                  (
                                  '11'--Waiting List
                                  ,'12'--Booked
                                  ,'13'--Planned
                                  )
                           and ManagementIntentionCode = '2'-- DAY CASE
                           and    Encounter.AdmissionDate = Encounter.DischargeDate
                           then 'DC'--Daycase
                           
                     
                     --Elective Inaptient where intended management was daycase but not discharged
                           when AdmissionMethodCode in 
                                  (
                                  '11'--Waiting List
                                  ,'12'--Booked
                                  ,'13'--Planned
                                  )
                           and ManagementIntentionCode = '2'-- DAY CASE
                           and    Encounter.DischargeDate IS NULL
                           then 'EL'--Elective
                     
                     
                     --Regular Day Case where intended management was Regular but patient stayed overnight
                           when AdmissionMethodCode in 
                                  (
                                  '11'--Waiting List
                                  ,'12'--Booked
                               ,'13' --Regular
                                  )
                           and ManagementIntentionCode IN ('4','5')-- Regular day and night
                           and    Encounter.AdmissionDate < Encounter.DischargeDate
                           then 'EL'--Elective


                     --Regular Day Case
                           when AdmissionMethodCode in 
                                  (
                                  '11'--Waiting List
                                  ,'12'--Booked
                                  ,'13' --Regular
                                  )
                           and ManagementIntentionCode IN ('4','5')-- Regular day and night
                           and    Encounter.AdmissionDate = Encounter.DischargeDate
                           then 'RD'--Regular
                           
                     
                     --Regular Day Case where intended management was Regular but not discharged
                           when AdmissionMethodCode in 
                                  (
                                  '11'--Waiting List
                                  ,'12'--Booked
                                  ,'13' --Regular
                                  )
                           and ManagementIntentionCode IN ('4','5')-- Regular day and night
                           and    Encounter.DischargeDate IS NULL
                           then 'EL'--Elective  
                     
                                         
                     --Non Elective
                           else 'NE' --Non Elective
                     
                     end
       ,ReferredByCode = 
              CASE
                     WHEN left(ReferrerCode,1) = 'C' THEN 'CON'
                     WHEN left(ReferrerCode,1) = 'G' THEN 'GP'
                     WHEN left(ReferrerCode,1) = 'D' THEN 'GDP'
              END
       ,ReferringConsultantCode = 
              CASE
                     WHEN left(ReferrerCode,1) = 'C' THEN Encounter.SourceReferrerCode
                     else null
              END
       ,ExpectedDateOfDischarge = 
              Case
                     when PredictedDischargeDate between '1 jan 1900'and  '6 June 2079' then PredictedDischargeDate
                     else  null
              end
       ,EddCreatedTime = 
              Case
                     when PredictedDischargeDateCreated between '1 jan 1900'and  '6 June 2079' then PredictedDischargeDateCreated
                     else  null
              end
       ,EddCreatedByConsultantFlag = 
              Encounter.PredictedDischargeDateSetByCon
       ,EddInterfaceCode = 
              case when 
                     Case
                     when PredictedDischargeDateCreated between '1 jan 1900'and  '6 June 2079' then PredictedDischargeDateCreated
                     else  null
                     end is not null 
              then 'UG'     
              end
       ,ContextCode = 'TRA||' + Encounter.InterfaceCode
       ,ClinicalCodingStatus
       ,ClinicalCodingCompleteDate
       ,ClinicalCodingCompleteTime
--select top 10000 convert(varchar(20),PredictedDischargeDate,113)

       ,Ambulatory = coalesce(Encounter.Ambulatory, 0)
from
       TraffordWarehouse.dbo.APCEncounter Encounter

left join TraffordWarehouse.dbo.APCSpellHistory
on     APCSpellHistory.ProviderSpellNo = Encounter.ProviderSpellNo
and    APCSpellHistory.Status = 'DI'

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis1
on     Diagnosis1.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis1.SequenceNo = 1

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis2
on     Diagnosis2.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis2.SequenceNo = 2

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis3
on     Diagnosis3.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis3.SequenceNo = 3

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis4
on     Diagnosis4.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis4.SequenceNo = 4

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis5
on     Diagnosis5.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis5.SequenceNo = 5

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis6
on     Diagnosis6.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis6.SequenceNo = 6

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis7
on     Diagnosis7.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis7.SequenceNo = 7

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis8
on     Diagnosis8.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis8.SequenceNo = 8

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis9
on     Diagnosis9.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis9.SequenceNo = 9

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis10
on     Diagnosis10.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis10.SequenceNo = 10

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis11
on     Diagnosis11.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis11.SequenceNo = 11

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis12
on     Diagnosis12.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis12.SequenceNo = 12

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis13
on     Diagnosis13.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis13.SequenceNo = 13

left join TraffordWarehouse.dbo.APCDiagnosis Diagnosis14
on     Diagnosis14.IPSourceUniqueID = Encounter.SourceUniqueID
and    Diagnosis14.SequenceNo = 14


left join TraffordWarehouse.dbo.APCProcedure Procedure1
on     Procedure1.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure1.SequenceNo = 2

left join TraffordWarehouse.dbo.APCProcedure Procedure2
on     Procedure2.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure2.SequenceNo = 3

left join TraffordWarehouse.dbo.APCProcedure Procedure3
on     Procedure3.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure3.SequenceNo = 4

left join TraffordWarehouse.dbo.APCProcedure Procedure4
on     Procedure4.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure4.SequenceNo = 5

left join TraffordWarehouse.dbo.APCProcedure Procedure5
on     Procedure5.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure5.SequenceNo = 6

left join TraffordWarehouse.dbo.APCProcedure Procedure6
on     Procedure6.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure6.SequenceNo = 7

left join TraffordWarehouse.dbo.APCProcedure Procedure7
on     Procedure7.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure7.SequenceNo = 8

left join TraffordWarehouse.dbo.APCProcedure Procedure8
on     Procedure8.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure8.SequenceNo = 9

left join TraffordWarehouse.dbo.APCProcedure Procedure9
on     Procedure9.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure9.SequenceNo = 10

left join TraffordWarehouse.dbo.APCProcedure Procedure10
on     Procedure10.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure10.SequenceNo = 11

left join TraffordWarehouse.dbo.APCProcedure Procedure11
on     Procedure11.IPSourceUniqueID = Encounter.SourceUniqueID
and    Procedure11.SequenceNo = 12

where
       Encounter.IsDeleted = 0

--remove fces that exist in both Central and Trafford activity
and    not exists
       (
       select
              1
       from
              WarehouseOLAPMerged.APC.EncounterDuplicate
       where
              EncounterDuplicate.DuplicateContextID = (select ContextID from ReferenceMap.Map.Context where ContextCode   = 'TRA||UG')
       and    EncounterDuplicate.DuplicateRecno = Encounter.EncounterRecno
       )
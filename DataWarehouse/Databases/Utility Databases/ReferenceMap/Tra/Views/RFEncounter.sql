﻿CREATE VIEW [Tra].[RFEncounter] AS

SELECT
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo = 
		DistrictNo
	,SourceEncounterNo = ROW_NUMBER() OVER(PARTITION BY DistrictNo ORDER BY OriginalRTTStartDate)
	,PatientTitle = 
		case when Title = '' then null
		else convert(varchar(10),Title) 
		end
	,PatientForename = 
		convert(varchar(20),NULL)
	,PatientSurname = 
		convert(varchar(30),Surname)
	,DateOfBirth = 
		convert(datetime,NULL)
	,DateOfDeath = 
		convert(smalldatetime,NULL)
	,SexCode =  
		convert(varchar(1),NULL)
	,NHSNumber = 
		CONVERT(varchar(17),NHSNumber)
	,DistrictNo =
		CONVERT(varchar(12),DistrictNo)
	,Postcode =
		CONVERT(varchar(12),Postcode)
	,PatientAddress1 =  
		CONVERT(varchar(25),NULL)
	,PatientAddress2 =  
		CONVERT(varchar(25),NULL)
	,PatientAddress3 =  
		CONVERT(varchar(25),NULL)
	,PatientAddress4 =  
		CONVERT(varchar(25),NULL)
	,DHACode =  
		CONVERT(varchar(3),NULL)
	,EthnicOriginCode =  
		CONVERT(varchar(4),NULL)
	,MaritalStatusCode =  
		CONVERT(varchar(1),NULL)
	,ReligionCode =  
		CONVERT(varchar(4),NULL)
	,RegisteredGpCode = 'G9999998'
	,RegisteredGpPracticeCode
	,EpisodicGpCode = 'G9999998'
	,EpisodicGpPracticeCode = 
		RegisteredGpPracticeCode
	,EpisodicGdpCode = 'D9999998'
	,SiteCode =  
		CONVERT(varchar(5),SiteCode)
	,ConsultantCode =  
		CONVERT(varchar(50),ConsultantCode)
	,SpecialtyCode =  
		CONVERT(varchar(5),TreatmentFunctionCode)
	,SourceOfReferralCode =  
		CONVERT(varchar(5), SourceOfReferralCode)
	,PriorityCode =  
		CONVERT(varchar(5), PriorityCode)
	,ReferralDate = 
		OriginalRTTStartDate
	,DischargeDate =  
		CONVERT(date,NULL)
	,DischargeTime =  
		CONVERT(datetime,NULL)
	,DischargeReasonCode =  
		CONVERT(varchar(30), NULL)
	,DischargeReason =  
		CONVERT(varchar(30), NULL)
	,AdminCategoryCode =  
		CONVERT(varchar(3), NULL)
	,ContractSerialNo =  
		CONVERT(varchar(6), NULL)
	,RTTPathwayID = 
		PathwayId
	,RTTPathwayCondition =  
		CONVERT(varchar(20), NULL)
	,RTTStartDate = OriginalRTTStartDate
	,RTTEndDate =  
		CONVERT(smalldatetime, NULL)
	,RTTSpecialtyCode =  
		CONVERT(varchar(10), NULL)
	,RTTCurrentProviderCode = 
		'RW3'
	,RTTCurrentStatusCode =  
		CONVERT(varchar(10), NULL)
	,RTTCurrentStatusDate = '1 APr 2012'
	,RTTCurrentPrivatePatientFlag =  
		CONVERT(bit, NULL)
	,RTTOverseasStatusFlag =  
		CONVERT(bit, NULL)
	,NextFutureAppointmentDate = '1 APr 2012'
	,ReferralComment =  
		CONVERT(varchar(25), NULL)
	,InterfaceCode = 'UG'
	,Created = 
		LoadDate
	,Updated = 
		LoadDate
	,ByWhom = NULL
	,ReasonForReferralCode = 
		CONVERT(varchar(10), ReferralReason)
		
	,SourceOfReferralGroupCode =  
		CONVERT(varchar(10), NULL)
	,DirectorateCode = '80'
	,CasenoteNo = EPMI + '/' + districtNo
	,PCTCode =  
		CONVERT(nvarchar(6),PCTCode)
	,ContextCode = 'TRA||' + 'UG' --Referrals.InterfaceCode
FROM
	TraffordWarehouse.dbo.Referrals
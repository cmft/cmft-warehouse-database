﻿create view [Tra].[APCOperation]

as

select
	 Operation.SourceUniqueID
	--,SourceSpellNo = null
	,SourcePatientNo = DistrictNo
	,ProviderSpellNo
	,SourceEncounterNo = EpisodeNo
	,SequenceNo = cast(SequenceNo as int) - 1
	,OperationCode = Operation.ProcedureCode
	--,OperationReadCode
	--,OperationReadTermCode
	,[APCSourceUniqueID] = IPSourceUniqueID
	--,SourceOperationCode
	--,SourceSequenceNo
	--,Operation.InterfaceCode
	--,SequenceNoCheck
	,ContextCode = 'TRA||UG'
from
	[TraffordWarehouse].[dbo].[APCProcedure] Operation
inner join
	[TraffordWarehouse].dbo.[APCEncounter] Encounter
on
	Operation.[IPSourceUniqueID] = Encounter.[SourceUniqueID]

where
	--remove fces that exist in both Central and Trafford activity
not exists
	(
	select
		1
	from
		WarehouseOLAPMerged.APC.EncounterDuplicate
	where
		EncounterDuplicate.DuplicateContextID = (select ContextID from ReferenceMap.Map.Context where ContextCode	= 'TRA||UG')
	and	EncounterDuplicate.DuplicateRecno = Encounter.EncounterRecno
	)
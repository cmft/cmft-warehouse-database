﻿CREATE VIEW [Tra].[APCWait] as

select 
	 EncounterRecno
	,CensusDate = 
		dateadd(d,-1,Encounter.CensusDate)
	,SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,SourcePatientNo = DistrictNo
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,OverseasVisitorCode
	,CarerSupportIndicator
	,EthnicCategoryCode
	,MaritalStatusCode
	,LegalStatusClassificationCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,NHSServiceAgreementChangeDate
	,ElectiveAdmissionListEntryNo
	,AdminCategoryCode
	,CountOfDaysSuspended
	,ElectiveAdmissionListStatusCode
	,ElectiveAdmissionTypeCode
	,ManagementIntentionCode
	,IntendedProcedureStatusCode
	,PriorityTypeCode
	,DecidedToAdmitDate
	,AgeAtCensus
	,GuaranteedAdmissionDate
	,LastDNAOrPatientCancelledDate
	,LastReviewedDate
	,ConsultantCode
	,SpecialtyCode
	,TreatmentFunctionCode
	,OPCSProcedureSchemeCode
	,PrimaryProcedureCode
	,PrimaryProcedureDate
	,ReadProcedureSchemeCode
	,PrimaryProcedureReadCode
	,LocationClassCode
	,IntendedSiteCode
	,LocationTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ReferrerCode
	,ReferrerOrganisationCode
	,AdmissionOfferOutcomeCode
	,OfferedForAdmissionDate
	,EarliestReasonableOfferDate
	,OriginalDecidedToAdmitDate
	,ElectiveAdmissionListRemovalReasonCode
	,ElectiveAdmissionListRemovalReasonDate
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,DurationOfElectiveWait
	,PCTCode
	,ResidencePCTCode
	,PASUpdateDate
	,InterfaceCode
	,IsDeleted
	,ConsultantEpisodeStatusCode
	,Created
	,Updated
	,ByWhom
	,OriginalPathwayId
	,OriginalPathwayIdIssuerCode
	,OriginalRTTStatusCode
	,OriginalRTTStartDate
	,OriginalRTTEndDate
	,AppointmentTypeCode
	,TheatrePatientBookingKey
	,ProcedureTime
	,TheatreCode
	,AppointmentNote
	,SourceManagementIntentionCode
	,KornerWait = 
		case 
		when KornerWait < 0 then 0
		else KornerWait
		end
	,WaitingListCode = 'TRA'
	,WithRTTOpenPathway = 
		coalesce (
				WithRTTOpenPathway
				,0
		)
	,AdmissionReason = null
	,ContextCode = 'TRA||' + InterfaceCode
	,ReferralRequestReceivedDate
from 
	TraffordWarehouse.dbo.APCWaitingList Encounter

where 
	dateadd(d,-1,Encounter.CensusDate) IN
		(
		select 
			Calendar.TheDate
		from
			WarehouseOLAPMerged.WH.Calendar

		left outer join WarehouseOLAPMerged.APC.BaseWait 
		on BaseWait.CensusDateID = Calendar.DateID
		and left(BaseWait.ContextCode,3) =  'TRA'

		where 
			BaseWait.CensusDateID is null --only return missing snapshots
		and	(
				Calendar.TheDate between dateadd(day , -14 , getdate()) and getdate() --snapshots in the last fourteen days
			or	Calendar.TheDate in --month-end snapshots
					(
					select
						max(TheDate)
					from
						WarehouseOLAPMerged.WH.Calendar
					where
						TheDate between '1 May 2011' and getdate()
					group by
						 year(TheDate)
						,month(TheDate)
					)
			)
		)
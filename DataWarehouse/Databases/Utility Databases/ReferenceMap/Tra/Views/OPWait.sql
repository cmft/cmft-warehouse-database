﻿CREATE VIEW [Tra].[OPWait] AS

SELECT
	 EncounterRecno
	,CensusDate = 
		dateadd(d,-1,Encounter.CensusDate)
	,SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,SourcePatientNo = DistrictNo
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,EthnicCategoryCode
	,MaritalStatusCode
	,AttendanceID
	,AdminCategoryCode
	,DNACode
	,FirstAttendanceCode = 
		case 
		when FollowupAppointments.AppointmentTypeID is null
			then FirstAttendanceCode
		else 2
		end
	,MedicalStaffTypeCode
	,OperationStatusCode
	,AttendanceOutcomeCode
	,AppointmentDate
	,AppointmentNote
	,AgeOnAppointment
	,EarliestReasonableOfferDate
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,ConsultantCode
	,SpecialtyCode
	,TreatmentFunctionCode
	,ICDDiagnosisSchemeCode
	,PrimaryDiagnosisCode
	,ReadDiagnosisSchemeCode
	,PrimaryDiagnosisReadCode
	,OPCSProcedureSchemeCode
	,PrimaryProcedureCode
	,PrimaryProcedureDate
	,ReadProcedureSchemeCode
	,PrimaryProcedureReadCode
	,LocationClassCode
	,SiteCode
	,LocationTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,PriorityTypeCode
	,ServiceTypeRequestedCode
	,SourceOfReferralCode
	,ReferralRequestReceivedDate
	,ReferrerCode
	,ReferrerOrganisationCode
	,LastDNAOrCancelledDate
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,PCTCode
	,ClinicCode
	,ClinicFunctionCode
	,OverseasVisitorCode
	,ResidencePCTCode
	,DepartmentCode
	,AppointmentStatusCode
	,IsDeleted
	,ContactChangeDate
	,EpisodeChangeDate
	,ContactChangeTime
	,EpisodeChangeTime
	,PASUpdateDate
	,ContactCreationDate
	,AppointmentTypeCode
	,OriginalPathwayId
	,OriginalPathwayIdIssuerCode
	,OriginalRTTStatusCode
	,OriginalRTTStartDate
	,OriginalRTTEndDate
	,APPReferralDate
	,KornerWait = 
		isnull( 
			case 
			when KornerWait < 0 then 0
			else KornerWait
			end
			,0
		)
	,WaitingListCode = 'TRA'
	,WithRTTOpenPathway = 
		coalesce (
				WithRTTOpenPathway
				,0
		)
	,ContextCode = 'TRA||' + 'UG' --InterfaceCode --NULL Needs fixing



FROM 
	TraffordWarehouse.dbo.OPWaitingList Encounter
left outer join
(
	select 
		AppointmentTypeID =
			apptype_identity
	from
		TraffordReferenceData.dbo.AppointmentType
	where 
	  apptype_code in ('NewWD', 'FNTrust')
 ) FollowupAppointments
 on Encounter.AppointmentTypeCode = FollowupAppointments.AppointmentTypeID

where 


	dateadd(d,-1,Encounter.CensusDate) >= '1 Mar 2012'

	--dateadd(d,-1,Encounter.CensusDate) IN (
	--	select 
	--		Calendar.TheDate
	--	from
	--		WarehouseOLAPMerged.WH.Calendar
	--	left outer join WarehouseOLAPMerged.OP.BaseWait 
	--	on BaseWait.CensusDateID = Calendar.DateID
	--	and left(BaseWait.ContextCode,3) =  'TRA'
	--	where 
	--		Calendar.TheDate between '24 Sep 2012' and getdate()
	--	and BaseWait.CensusDateID is null
	--	)







--GO
﻿CREATE view [Tra].[AEDiagnosis] AS


SELECT 
	 Diagnosis.DiagnosisRecno
	,Diagnosis.SourceUniqueID
	,Diagnosis.SequenceNo
	,Diagnosis.DiagnosticSchemeInUse
	,Diagnosis.DiagnosisCode
	,Diagnosis.AESourceUniqueID
	,Diagnosis.SourceDiagnosisCode
	,SourceDiagnosisSiteCode = null
	,SourceDiagnosisSideCode = null
	,Diagnosis.SourceSequenceNo
	,Diagnosis.DiagnosisSiteCode
	,Diagnosis.DiagnosisSideCode
	,ContextCode = 'TRA||' + Encounter.InterfaceCode
FROM 
	TraffordWarehouse.dbo.AEDiagnosis Diagnosis

INNER JOIN TraffordWarehouse.dbo.AEEncounter Encounter
ON Diagnosis.AESourceUniqueID = Encounter.SourceUniqueID
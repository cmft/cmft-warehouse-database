﻿CREATE view [Tra].[AETreatment] AS

SELECT 
	 Treatment.AESourceUniqueID
	,TreatmentCode = Treatment.ProcedureCode
	,TreatmentDate = Treatment.ProcedureDate
	,Treatment.SequenceNo
	,ContextCode = 'TRA||' + Encounter.InterfaceCode
FROM
	TraffordWarehouse.dbo.AEProcedure Treatment

INNER JOIN TraffordWarehouse.dbo.AEEncounter Encounter
ON Treatment.AESourceUniqueID = Encounter.SourceUniqueID
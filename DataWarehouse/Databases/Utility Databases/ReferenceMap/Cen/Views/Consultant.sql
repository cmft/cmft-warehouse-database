﻿CREATE view [Cen].[Consultant] as

SELECT 
	 ConsultantID = ConsultantCode
	,ConsultantCode = ConsultantCode
	,Consultant = Consultant
	,ProviderCode = 
		Provider.NationalCode
	,MainSpecialtyCode
	,Title
	,Initials
	,Surname
	,ContextCode = 'CEN||PAS'
FROM 
	warehouse.PAS.Consultant
LEFT OUTER JOIN warehouse.pas.provider Provider
	ON Consultant.ProviderCode = Provider.ProviderCode
﻿CREATE VIEW [Cen].[AETreatment] AS

SELECT 
	 Treatment.AESourceUniqueID
	,TreatmentCode = [ProcedureCode]
	,TreatmentDate = [ProcedureDate]
	,Treatment.SequenceNo
	,ContextCode = 'CEN||' + 
		case
		when Encounter.InterfaceCode in ('INFO', 'INQ')
		then 'PAS'
		else Encounter.InterfaceCode
		end

from
	Warehouse.[AE].[Procedure] Treatment

INNER JOIN Warehouse.AE.Encounter Encounter
ON Treatment.AESourceUniqueID = Encounter.SourceUniqueID



--FROM
--	warehouse.AE.TreatmentDistinct Treatment
--INNER JOIN Warehouse.AE.Encounter Encounter
--ON Treatment.AESourceUniqueID = Encounter.SourceUniqueID
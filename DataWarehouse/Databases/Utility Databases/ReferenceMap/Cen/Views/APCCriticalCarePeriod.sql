﻿CREATE view [Cen].[APCCriticalCarePeriod] as

select
	 EncounterRecno
	,SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,StartTime
	,EndDate
	,EndTime
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,DermatologicalSupportDays
	,LiverSupportDays
	,NeurologicalSupportDays
	,RenalSupportDays
	,CreatedByUser
	,CreatedByTime
	,LocalIdentifier
	,LocationCode
	,StatusCode
	,TreatmentFunctionCode
	,PlannedAcpPeriod
	,Created
	,ByWhom
	,InterfaceCode
	,CasenoteNumber
	,WardCode
	,AdmissionDate
	,SiteCode
	,NHSNumber
	,UnitFunctionCode
	,ContextCode = 
		case 
			when InterfaceCode = 'MID' then 'CEN||MID'
			when InterfaceCode = 'INQ' then 'CEN||PAS'
		end
FROM
	Warehouse.APC.CriticalCarePeriod CriticalCarePeriod
where 
	InterfaceCode = 'MID'
and SiteCode NOT IN ('RM401', 'RW3TR')
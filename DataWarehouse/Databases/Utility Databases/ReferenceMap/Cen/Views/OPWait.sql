﻿CREATE view [Cen].[OPWait] as

select
	 EncounterRecno
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList = 
		ReferralDate
	,DateOnWaitingList = 
		QM08StartWaitDate
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,EpisodicGpCode = 
		EpisodicGp.NationalCode

	,EpisodicGpPracticeCode
	,RegisteredGpCode = 
		RegisteredGP.NationalCode

	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,BookedDate
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,FuturePatientCancelDate
	,AdditionFlag
	,LastAppointmentFlag
	,LocalRegisteredGpCode
	,LocalEpisodicGpCode
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,FutureCancellationDate
	,EpisodeNo
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,BreachTypeCode
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode
	,ClockStartDate
	,PatientDeathIndicator
	,AppointmentDoctor
	,PatientChoice
	,CountOfCNDs
	,ReportCategoryCode
	,DirectorateCode
	,SpecialtyTypeCode
	,WeeksWaiting
	,FirstAttendanceCode = 1
	,WithRTTOpenPathway = 
		coalesce (
				WithRTTOpenPathway
				,0
		)	
	,RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode 
	,RTTDaysWaiting 
	,WLAppointmentTypeCode
	,ContextCode = 'CEN||PAS'
from
	Warehouse.OP.PTL Encounter
LEFT OUTER JOIN warehouse.PAS.Gp RegisteredGP
	ON	RegisteredGP.GpCode = Encounter.RegisteredGpCode
LEFT OUTER JOIN warehouse.PAS.Gp EpisodicGP
	ON	EpisodicGP.GpCode = Encounter.EpisodicGpCode

where 
	Encounter.CensusDate IN
		--(
		--select 
		--	Calendar.TheDate
		--from
		--	WarehouseOLAPMerged.WH.Calendar
		--left outer join WarehouseOLAPMerged.OP.BaseWait 
		--on BaseWait.CensusDateID = Calendar.DateID
		--and BaseWait.ContextCode =  'CEN||PAS'
		--where 
		--	Calendar.TheDate between '24 Sep 2012' and getdate()
		--and BaseWait.CensusDateID is null
		--)
		(
		select 
			Calendar.TheDate
		from
			WarehouseOLAPMerged.WH.Calendar

		left join WarehouseOLAPMerged.OP.BaseWait 
		on	BaseWait.CensusDateID = Calendar.DateID
		and BaseWait.ContextCode =  'CEN||PAS'

		where 
			BaseWait.CensusDateID is null --only return missing snapshots
		and	(
				Calendar.TheDate between dateadd(day , -14 , getdate()) and getdate() --snapshots in the last fourteen days
			or	Calendar.TheDate in --month-end snapshots
					(
					select
						max(TheDate)
					from
						WarehouseOLAPMerged.WH.Calendar
					where
						TheDate between '31 Dec 2011' and getdate()
					group by
						 year(TheDate)
						,month(TheDate)
					)
			)
		)
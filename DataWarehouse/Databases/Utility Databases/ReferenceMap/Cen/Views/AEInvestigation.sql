﻿CREATE VIEW [Cen].[AEInvestigation] AS

SELECT 
	 Investigation.InvestigationRecno
	,Investigation.SourceUniqueID
	,Investigation.InvestigationDate
	,Investigation.SequenceNo
	,Investigation.InvestigationSchemeInUse
	,Investigation.InvestigationCode
	,Investigation.AESourceUniqueID
	,Investigation.SourceInvestigationCode
	,Investigation.SourceSequenceNo
	,Investigation.ResultDate
	,ContextCode = 'CEN||' + 
		case
		when Encounter.InterfaceCode in ('INFO', 'INQ')
		then 'PAS'
		else Encounter.InterfaceCode
		end
FROM
	warehouse.AE.Investigation Investigation
INNER JOIN Warehouse.AE.Encounter Encounter
ON Investigation.AESourceUniqueID = Encounter.SourceUniqueID
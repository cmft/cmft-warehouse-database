﻿CREATE VIEW [Cen].[AEDiagnosis] AS


SELECT 
	 Diagnosis.DiagnosisRecno
	,Diagnosis.SourceUniqueID
	,Diagnosis.SequenceNo
	,Diagnosis.DiagnosticSchemeInUse
	,Diagnosis.DiagnosisCode
	,Diagnosis.AESourceUniqueID
	,Diagnosis.SourceDiagnosisCode
	,Diagnosis.SourceDiagnosisSiteCode
	,Diagnosis.SourceDiagnosisSideCode
	,Diagnosis.SourceSequenceNo
	,Diagnosis.DiagnosisSiteCode
	,Diagnosis.DiagnosisSideCode
	,ContextCode = 'CEN||' + 
		case
		when Encounter.InterfaceCode in ('INFO', 'INQ')
		then 'PAS'
		else Encounter.InterfaceCode
		end
FROM 
	Warehouse.AE.Diagnosis Diagnosis
INNER JOIN Warehouse.AE.Encounter Encounter
ON Diagnosis.AESourceUniqueID = Encounter.SourceUniqueID
﻿CREATE view [Cen].[APCWait] as

select
	 EncounterRecno
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,EpisodeSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,EpisodicGpCode = 
		EpisodicGP.NationalCode
	,EpisodicGpPracticeCode 
	,RegisteredGpCode = 
		RegisteredGp.NationalCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,FuturePatientCancelDate
	,AdditionFlag
	,LocalRegisteredGpCode
	,LocalEpisodicGpCode
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,TheatreKey
	,TheatreInterfaceCode
	,ProcedureDate
	,FutureCancellationDate
	,ProcedureTime
	,TheatreCode
	,AdmissionReason
	,EpisodeNo
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays
	,BreachTypeCode
	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode
	,ReferralEncounterRecno
	,PatientDeathIndicator
	,CurrentAdmissionDays
	,LastReviewDate
	,KornerCharterWaitMonths
	,KornerWaitMonths
	,DiagnosticGroupCode
	,IntendedProcedureDate
	,ShortNotice
	,CEAEpisode
	,DirectorateCode
	,GuaranteedAdmissionDate
	,WithRTTOpenPathway = 
		coalesce (
				WithRTTOpenPathway
				,0
		)
	,RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode 
	,RTTDaysWaiting 
	,ContextCode = 'CEN||PAS'
from
	Warehouse.APC.WaitingList Encounter

LEFT OUTER JOIN warehouse.PAS.Gp RegisteredGP
	ON	RegisteredGP.GpCode = Encounter.RegisteredGpCode
	
LEFT OUTER JOIN warehouse.PAS.Gp EpisodicGP
	ON	EpisodicGP.GpCode = Encounter.EpisodicGpCode

where 
	Encounter.CensusDate IN (
		select 
			Calendar.TheDate
		from
			WarehouseOLAPMerged.WH.Calendar

		left join WarehouseOLAPMerged.APC.BaseWait 
		on	BaseWait.CensusDateID = Calendar.DateID
		and BaseWait.ContextCode =  'CEN||PAS'

		where 
			BaseWait.CensusDateID is null --only return missing snapshots
		and	(
				Calendar.TheDate between dateadd(day , -14 , getdate()) and getdate() --snapshots in the last fourteen days
			or	Calendar.TheDate in --month-end snapshots
					(
					select
						max(TheDate)
					from
						WarehouseOLAPMerged.WH.Calendar
					where
						TheDate between '1 Apr 2011' and getdate()
					group by
						 year(TheDate)
						,month(TheDate)
					)
			)
		)
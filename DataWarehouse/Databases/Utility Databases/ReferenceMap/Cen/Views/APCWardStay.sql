﻿CREATE VIEW [Cen].[APCWardStay] AS

SELECT
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,StartDate
	,EndDate
	,StartTime
	,EndTime
	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
	,Created
	,Updated
	,ByWhom
	,ContextCode = 'CEN||PAS'
FROM
	Warehouse.APC.WardStay
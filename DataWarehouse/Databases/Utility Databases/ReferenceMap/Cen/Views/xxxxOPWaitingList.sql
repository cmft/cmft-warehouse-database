﻿CREATE view [Cen].[xxxxOPWaitingList] as

select
	 EncounterRecno
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,EpisodicGpCode = EpisodicGp.NationalCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode = RegisteredGP.NationalCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,BookedDate
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,FuturePatientCancelDate
	,AdditionFlag
	,LastAppointmentFlag
	,LocalRegisteredGpCode
	,LocalEpisodicGpCode
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,FutureCancellationDate
	,EpisodeNo
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,BreachTypeCode
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode
	,ClockStartDate
	,PatientDeathIndicator
	,AppointmentDoctor
	,PatientChoice
	,CountOfCNDs
	,ReportCategoryCode
	,DirectorateCode
	,SpecialtyTypeCode
	,WeeksWaiting
	,ContextCode = 'CEN||PAS'
from
	Warehouse.OP.PTL
LEFT OUTER JOIN warehouse.PAS.Gp RegisteredGP
	ON	RegisteredGP.GpCode = PTL.RegisteredGpCode
LEFT OUTER JOIN warehouse.PAS.Gp EpisodicGP
	ON	EpisodicGP.GpCode = PTL.EpisodicGpCode
﻿CREATE view [Cen].[APCEncounter] as

select
       Encounter.EncounterRecno
       ,Encounter.SourceUniqueID
       ,Encounter.SourcePatientNo
       ,Encounter.SourceSpellNo
       ,Encounter.SourceEncounterNo
       ,Encounter.ProviderSpellNo
       ,PatientTitle = 
                     coalesce(
                           Patient.Title
                           ,Encounter.PatientTitle
              )
       ,PatientForename = 
                     coalesce(
                           Patient.Forenames
                           ,Encounter.PatientForename
              )
       ,PatientSurname = 
                     coalesce(
                           Patient.Surname
                           ,Encounter.PatientSurname
              )
       ,DateOfBirth = 
                     coalesce(
                           Patient.DateOfBirth
                           ,Encounter.DateOfBirth
              )
       ,DateOfDeath =  
                     coalesce(
                           Patient.DateOfDeath
                           ,Encounter.DateOfDeath
              )

       ,SexCode = 
              coalesce(
                           Patient.SexCode
                           ,Encounter.SexCode
              )
       ,NHSNumber =
              replace(
                     coalesce(
                           Patient.NHSNumber

                           ,case
                           when
                                  (
                                         Encounter.NHSNumber = '1234567899'  
                                  or     rtrim(Encounter.NHSNumber) = ''
                                  or Encounter.NHSNumber = 'REMOVE'
                                  or left(Encounter.NHSNumber,4) = 'SLF?' 
                                  or left(Encounter.NHSNumber,4) = 'MAN?'
                                  )
                           then null
                           else Encounter.NHSNumber
                           end
                     )
                     ,' '
                     ,''
              )
       ,Postcode  = 
              coalesce(
                           Postcode.Postcode
                           ,Encounter.Postcode
                           ) 
       ,PatientAddress1 =
              coalesce(
                           patientaddressatdischarge.AddressLine1
                           ,Encounter.PatientAddress1
                           ) 
       ,PatientAddress2 =
              coalesce(
                           patientaddressatdischarge.AddressLine2
                           ,Encounter.PatientAddress2
                           )                    
       ,PatientAddress3 =
              coalesce(
                           patientaddressatdischarge.AddressLine3
                           ,Encounter.PatientAddress3
                           )      
       ,PatientAddress4 =
              coalesce(
                           patientaddressatdischarge.AddressLine4
                           ,Encounter.PatientAddress4
                           )      
       ,Encounter.DHACode
       ,EthnicOriginCode =  
              coalesce(
                           Patient.EthnicOriginCode
                           ,Encounter.EthnicOriginCode
              )
       ,MaritalStatusCode = 
              coalesce(
                           Patient.MaritalStatusCode
                           ,Encounter.MaritalStatusCode
              )
       ,ReligionCode = 
              coalesce(
                           Patient.ReligionCode
                           ,Encounter.ReligionCode
              )
       ,Encounter.DateOnWaitingList
       ,Encounter.AdmissionDate
       ,Encounter.DischargeDate
       ,Encounter.EpisodeStartDate
       ,Encounter.EpisodeEndDate
       ,Encounter.StartSiteCode
       ,Encounter.StartWardTypeCode
       ,Encounter.EndSiteCode
       ,Encounter.EndWardTypeCode
       ,RegisteredGpCode = Gp.NationalCode
       ,Encounter.RegisteredGpPracticeCode
       ,RegisteredGpPracticeAtAdmission = RegisteredGpPracticeAtAdmission.GpPracticeCode
       ,RegisteredGpPracticeAtDischarge = RegisteredGpPracticeAtDischarge.GpPracticeCode
       ,Encounter.SiteCode
       ,Encounter.AdmissionMethodCode
       ,Encounter.AdmissionSourceCode
       ,Encounter.PatientClassificationCode
       ,Encounter.ManagementIntentionCode
       ,Encounter.DischargeMethodCode
       ,Encounter.DischargeDestinationCode
       ,Encounter.AdminCategoryCode
       ,Encounter.ConsultantCode
       ,Encounter.SpecialtyCode
       ,Encounter.LastEpisodeInSpellIndicator
       ,Encounter.FirstRegDayOrNightAdmit
       ,Encounter.NeonatalLevelOfCare
       ,Encounter.IsWellBabyFlag
       ,Encounter.PASHRGCode
       ,Encounter.ClinicalCodingStatus
       ,Encounter.ClinicalCodingCompleteDate
       ,Encounter.ClinicalCodingCompleteTime
       ,PrimaryDiagnosisCode = rtrim(left(Encounter.PrimaryDiagnosisCode, 6))
       ,Encounter.PrimaryDiagnosisDate
       ,SubsidiaryDiagnosisCode = rtrim(left(Encounter.SubsidiaryDiagnosisCode, 6))
       ,SecondaryDiagnosisCode1 = rtrim(left(Encounter.SecondaryDiagnosisCode1, 6))
       ,SecondaryDiagnosisCode2 = rtrim(left(Encounter.SecondaryDiagnosisCode2, 6))
       ,SecondaryDiagnosisCode3 = rtrim(left(Encounter.SecondaryDiagnosisCode3, 6))
       ,SecondaryDiagnosisCode4 = rtrim(left(Encounter.SecondaryDiagnosisCode4, 6))
       ,SecondaryDiagnosisCode5 = rtrim(left(Encounter.SecondaryDiagnosisCode5, 6))
       ,SecondaryDiagnosisCode6 = rtrim(left(Encounter.SecondaryDiagnosisCode6, 6))
       ,SecondaryDiagnosisCode7 = rtrim(left(Encounter.SecondaryDiagnosisCode7, 6))
       ,SecondaryDiagnosisCode8 = rtrim(left(Encounter.SecondaryDiagnosisCode8, 6))
       ,SecondaryDiagnosisCode9 = rtrim(left(Encounter.SecondaryDiagnosisCode9, 6))
       ,SecondaryDiagnosisCode10 = rtrim(left(Encounter.SecondaryDiagnosisCode10, 6))
       ,SecondaryDiagnosisCode11 = rtrim(left(Encounter.SecondaryDiagnosisCode11, 6))
       ,SecondaryDiagnosisCode12 = rtrim(left(Encounter.SecondaryDiagnosisCode12, 6))
       ,SecondaryDiagnosisCode13 = rtrim(left(Encounter.SecondaryDiagnosisCode13, 6))
       ,Encounter.PrimaryOperationCode
       ,Encounter.PrimaryOperationDate
       ,Encounter.SecondaryOperationCode1
       ,Encounter.SecondaryOperationDate1
       ,Encounter.SecondaryOperationCode2
       ,Encounter.SecondaryOperationDate2
       ,Encounter.SecondaryOperationCode3
       ,Encounter.SecondaryOperationDate3
       ,Encounter.SecondaryOperationCode4
       ,Encounter.SecondaryOperationDate4
       ,Encounter.SecondaryOperationCode5
       ,Encounter.SecondaryOperationDate5
       ,Encounter.SecondaryOperationCode6
       ,Encounter.SecondaryOperationDate6
       ,Encounter.SecondaryOperationCode7
       ,Encounter.SecondaryOperationDate7
       ,Encounter.SecondaryOperationCode8
       ,Encounter.SecondaryOperationDate8
       ,Encounter.SecondaryOperationCode9
       ,Encounter.SecondaryOperationDate9
       ,Encounter.SecondaryOperationCode10
       ,Encounter.SecondaryOperationDate10
       ,Encounter.SecondaryOperationCode11
       ,Encounter.SecondaryOperationDate11
       ,Encounter.OperationStatusCode
       ,ContractSerialNo =
                     CASE
                           WHEN AdminCategoryCode = '02' THEN 'YPPPAY'
                           WHEN ContractSerialNo IS NULL 
                           THEN
                           rtrim(
                                         coalesce(     
                                                       Practice.ParentOrganisationCode
                                                       ,Postcode.PCTCode
                                                       ,'5NT'
                                         ) 
                           ) + '00A'
                     ELSE ContractSerialNo
                     END
       ,Encounter.CodingCompleteDate
       ,PurchaserCode = 
                     CASE
                           WHEN AdminCategoryCode = '02' THEN 'VPP00'
                           ELSE
                                  coalesce(
                                         PCT.OrganisationCode -- joined from PurchaserCode
                                         ,Practice.ParentOrganisationCode
                                         ,Postcode.PCTCode
                                         ,'5NT'
                                  )      
                     END
       ,Encounter.ProviderCode
       ,Encounter.EpisodeStartTime
       ,Encounter.EpisodeEndTime
       ,Encounter.RegisteredGdpCode
       ,Encounter.EpisodicGpCode
       ,Encounter.InterfaceCode
       ,Encounter.CasenoteNumber
       ,NHSNumberStatusCode =
              coalesce(
                     Patient.NHSNumberStatusId
                     ,case
                     when 
                           (
                                  Encounter.NHSNumber = '1234567899'  
                           or     Encounter.NHSNumber is null
                           or     rtrim(Encounter.NHSNumber) = ''
                           or Encounter.NHSNumber = 'REMOVE'
                           or left(Encounter.NHSNumber,4) = 'SLF?'
                           or left(Encounter.NHSNumber,4) = 'MAN?' 
                           )                    
                     then 'RT'

                     when 
                           Encounter.NHSNumber is not null
                     then 'NP'

                     else coalesce(
                                  Encounter.NHSNumberStatusCode
                                  ,'RT'
                           )
                     end

                     ,'RT'
              )
       ,Encounter.AdmissionTime
       ,Encounter.DischargeTime
       ,Encounter.TransferFrom
       ,DistrictNo  = 
              coalesce(
                           Patient.DistrictNo
                           ,Encounter.DistrictNo
              )
       ,Encounter.ExpectedLOS
       ,Encounter.MRSAFlag
       ,Encounter.RTTPathwayID
       ,Encounter.RTTPathwayCondition
       ,Encounter.RTTStartDate
       ,Encounter.RTTEndDate
       ,Encounter.RTTSpecialtyCode
       ,RTTCurrentProviderCode = 
              Provider.NationalCode
       ,Encounter.RTTCurrentStatusCode
       ,Encounter.RTTCurrentStatusDate
       ,Encounter.RTTCurrentPrivatePatientFlag
       ,Encounter.RTTOverseasStatusFlag
       ,Encounter.RTTPeriodStatusCode
       ,Encounter.IntendedPrimaryOperationCode
       ,Encounter.Operation
       ,Encounter.Research1
       ,Encounter.CancelledElectiveAdmissionFlag
       ,Encounter.LocalAdminCategoryCode
       ,Encounter.EpisodicGpPracticeCode
       ,Encounter.PCTCode
       ,Encounter.LocalityCode
       ,Encounter.Created
       ,Encounter.Updated
       ,Encounter.ByWhom
       ,Encounter.ClockStartDate
       ,Encounter.BreachDate
       ,Encounter.RTTBreachDate
       ,Encounter.NationalBreachDate
       ,Encounter.NationalDiagnosticBreachDate
       ,Encounter.SocialSuspensionDays
       ,Encounter.BreachTypeCode
       ,Encounter.ReferralEncounterRecno
       ,Encounter.OperationDetailSourceUniqueID
       ,Encounter.Research2
       ,Encounter.AdmissionDivisionCode
       ,Encounter.DischargeDivisionCode
       ,Encounter.PatientCategoryCode
       ,Encounter.TheatrePatientBookingKey
       ,Encounter.StartDirectorateCode
       ,Encounter.EndDirectorateCode
       ,Encounter.DischargeReadyDate
       ,Encounter.DischargeWaitReasonCode
       ,Encounter.ReferredByCode
       ,ReferrerCode =
              case
                     when ReferredByCode = 'GP'
                     then GPReferrerCode.NationalCode
                     when ReferredByCode = 'GDP'
                     then GDPReferrerCode.NationalCode
                     when ReferredByCode = 'CON'
                     then ReferrerCode
              end
       
       ,Encounter.DecidedToAdmitDate
       ,Encounter.ResidencePCTCode
       ,Encounter.WaitingListCode
       ,Encounter.ISTAdmissionSpecialtyCode
       ,Encounter.ISTAdmissionDemandTime
       ,Encounter.ISTDischargeTime
       ,Encounter.FirstEpisodeInSpellIndicator
       ,PsychiatricPatientStatusCode = 
                     (
                     select
                           PsychiatricPatientStatusCode
                     from
                           Warehouse.APC.LegalStatus
                     where
                           LegalStatus.SourceSpellNo = Encounter.SourceSpellNo
                     and    LegalStatus.SourcePatientNo = Encounter.SourcePatientNo
                     and    LegalStatus.StartTime <= Encounter.EpisodeEndTime
                     and    LegalStatus.PsychiatricPatientStatusCode is not null
                     and    not exists
                           (
                           select
                                  1
                           from
                                  Warehouse.APC.LegalStatus Previous
                           where
                                  Previous.SourceSpellNo = Encounter.SourceSpellNo
                           and    Previous.SourcePatientNo = Encounter.SourcePatientNo
                           and    Previous.StartTime <= Encounter.EpisodeEndTime
                           and    Previous.PsychiatricPatientStatusCode is not null
                           and    (
                                         Previous.StartTime > LegalStatus.StartTime
                                  or     (
                                                Previous.StartTime = LegalStatus.StartTime
                                         and    Previous.SourceUniqueID > LegalStatus.SourceUniqueID
                                         )
                                  )
                           )
                     )
       ,LegalStatusClassificationCode = 
              (
              select
                     LegalStatusCode
              from
                     Warehouse.APC.LegalStatus
              where
                     LegalStatus.SourceSpellNo = Encounter.SourceSpellNo
              and    LegalStatus.SourcePatientNo = Encounter.SourcePatientNo
              and    LegalStatus.StartTime <= Encounter.EpisodeEndTime
              and    not exists
                     (
                     select
                           1
                     from
                           Warehouse.APC.LegalStatus Previous
                     where
                           Previous.SourceSpellNo = Encounter.SourceSpellNo
                     and    Previous.SourcePatientNo = Encounter.SourcePatientNo
                     and    Previous.StartTime <= Encounter.EpisodeEndTime
                     and    (
                                  Previous.StartTime > LegalStatus.StartTime
                           or     (
                                         Previous.StartTime = LegalStatus.StartTime
                                  and    Previous.SourceUniqueID > LegalStatus.SourceUniqueID
                                  )
                           )
                     )
              )
       ,ReferringConsultantCode =
              case
                     when ReferredByCode = 'CON'
                     then ReferrerCode
                     else null
              end
       ,DurationOfElectiveWait = 
                     datediff(day, Encounter.DecidedToAdmitDate, Encounter.AdmissionDate) -
                     coalesce(
                           (
                           select
                                  sum(datediff(day, Suspension.SuspensionStartDate, Suspension.SuspensionEndDate))
                           from
                                  Warehouse.APC.Suspension Suspension
                           where
                                  Suspension.SourcePatientNo = Encounter.SourcePatientNo
                           AND Suspension.SourceEncounterNo =  Encounter.SourceEncounterNo
                           )
                           ,0
                     )
       ,CarerSupportIndicator = 
              Encounter.CarerSupportIndicator
       ,EddCreatedTime = 
              ExpectedLOS.EddCreatedTime
       ,ExpectedDateofDischarge = 
              dateadd(D,ExpectedLOS.ExpectedLOS,encounter.AdmissionDate)
       ,EddCreatedByConsultantFlag = 
              NULL
       ,EddInterfaceCode = 
              case when EddCreatedTime is not null then
                     case 
                     when ExpectedLOS.EddInterfaceCode = 'Bedman' then 'Bed'
                     when ExpectedLOS.EddInterfaceCode = 'Inquire' then 'PAS'
                     else ExpectedLOS.EddInterfaceCode
                     end
              end
       ,ContextCode = 'CEN||PAS'

from
       Warehouse.APC.Encounter
       
left outer join warehouse.PAS.Patient Patient
       ON Patient.SourcePatientNo = Encounter.SourcePatientNo
       
left outer join warehouse.PAS.Gp
       ON Gp.GpCode = Encounter.RegisteredGpCode
       
left outer join warehouse.PAS.Gp GPReferrerCode
       ON GPReferrerCode.GpCode = Encounter.ReferrerCode
       AND Encounter.ReferredByCode = 'GP'
       
left outer join warehouse.PAS.Gdp GDPReferrerCode
       ON GDPReferrerCode.GdpCode = Encounter.ReferrerCode
       AND Encounter.ReferredByCode = 'GDP'

left outer join warehouse.PAS.PatientGp RegisteredGpPracticeAtAdmission
       ON RegisteredGpPracticeAtAdmission.SourcePatientNo = Encounter.SourcePatientNo
       AND Encounter.AdmissionDate BETWEEN RegisteredGpPracticeAtAdmission.EffectiveFromDate AND coalesce(RegisteredGpPracticeAtAdmission.EffectiveToDate, getdate())

left outer join warehouse.PAS.PatientGp RegisteredGpPracticeAtDischarge
       ON RegisteredGpPracticeAtDischarge.SourcePatientNo = Encounter.SourcePatientNo
       AND Encounter.DischargeDate BETWEEN RegisteredGpPracticeAtDischarge.EffectiveFromDate AND coalesce(RegisteredGpPracticeAtDischarge.EffectiveToDate, getdate())

left outer join Organisation.dbo.PCT PCT
       on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode

left outer join warehouse.PAS.Provider Provider
       ON Provider.ProviderCode = Encounter.RTTCurrentProviderCode
       
left outer join warehouse.PAS.PatientAddress patientaddressatdischarge
       on patientaddressatdischarge.SourcePatientNo = encounter.SourcePatientNo
       and encounter.DischargeDate between patientaddressatdischarge.EffectiveFromDate and coalesce(patientaddressatdischarge.EffectiveToDate, getdate())

--left outer join Organisation.dbo.Postcode Postcode on
--            Postcode.Postcode = 
--                   case
--                   when len(Encounter.Postcode) = 8 then Encounter.Postcode
--                   else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
--                   end
                     
left outer join Organisation.dbo.Postcode Postcode on -- now linked to address at time of discharge (DG 19/06/2012 agreed with Tim)
              Postcode.Postcode = 
                     case
                     when len(patientaddressatdischarge.Postcode) = 8 then patientaddressatdischarge.Postcode
                     else left(patientaddressatdischarge.Postcode, 3) + ' ' + right(patientaddressatdischarge.Postcode, 4) 
                     end

left outer join Organisation.dbo.Practice Practice
       ON     Practice.OrganisationCode = RegisteredGpPracticeAtDischarge.GpPracticeCode

left outer join 
       (
       select distinct
              SourcePatientNo
              ,SourceSpellNo
              ,EddCreatedTime
              ,ExpectedLOS
              ,EddInterfaceCode = 
                     coalesce(
                           ModifiedFromSystem
                           ,SourceSystem
                     )
       from
              warehouse.apc.ExpectedLOS 
       where 
              ExpectedLOS.ArchiveFlag = 'N'
       )ExpectedLOS
       on ExpectedLOS.SourcePatientNo = encounter.SourcePatientNo
AND ExpectedLOS.SourceSpellNo = encounter.SourceSpellNo





--select distinct PurchaserCode from [Cen].[APCEncounter]
﻿CREATE VIEW [Cen].[OPEncounter] AS

SELECT
	 EncounterRecno
	,SourceUniqueID
	,Encounter.SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,Encounter.NHSNumber
	,NHSNumberStatusCode
	,Encounter.DistrictNo
	,Encounter.Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferralConsultantCode
	,ReferralSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode
	,IsWardAttender
	,ClockStartDate
	,RTTBreachDate
	,HomePhone
	,WorkPhone
	,SourceOfReferralGroupCode
	,WardCode
	,ReferredByCode

	,ReferrerCode = 
		CASE
		WHEN ReferredByCode = 'GP' THEN ReferringGP.NationalCode
		WHEN ReferredByCode = 'GDP' THEN ReferringGDP.NationalCode
		ELSE ReferrerCode
		END

	,LastDNAorPatientCancelledDate
	,DirectorateCode
	,Encounter.StaffGroupCode
	,ReferralSpecialtyTypeCode
	,AppointmentOutcomeCode
	,MedicalStaffTypeCode
	,CommissioningSerialNo
	,DerivedFirstAttendanceFlag
	,AttendanceIdentifier 		
	,RegisteredGpAtAppointmentCode
	,RegisteredGpPracticeAtAppointmentCode
	,ContextCode
FROM
	(
	SELECT 
		 EncounterRecno
		,SourceUniqueID
		,Encounter.SourcePatientNo
		,SourceEncounterNo

		,PatientTitle = 
			coalesce(
				 Patient.Title
				,Encounter.PatientTitle
			)

		,PatientForename = 
			coalesce(
				 Patient.Forenames
				,Encounter.PatientForename
			)

		,PatientSurname = 
			coalesce(
				 Patient.Surname
				,Encounter.PatientSurname
			)

		,DateOfBirth = 
			coalesce(
				 Patient.DateOfBirth
				,Encounter.DateOfBirth
			)

		,DateOfDeath =  
			coalesce(
				 Patient.DateOfDeath
				,Encounter.DateOfDeath
			)

		,SexCode = 
			coalesce(
				 Patient.SexCode
				,Encounter.SexCode
			)

		,Encounter.NHSNumber

		,NHSNumberStatusCode =
			coalesce(
				 Patient.NHSNumberStatusId
				,case
				when 
					(
						Encounter.NHSNumber = '1234567899'  
					or	Encounter.NHSNumber is null
					or	rtrim(Encounter.NHSNumber) = ''
					or  Encounter.NHSNumber = 'REMOVE'
					or  left(Encounter.NHSNumber , 4) = 'SLF?'
					or  left(Encounter.NHSNumber , 4) = 'MAN?' 
					) then 'RT'
				when Encounter.NHSNumber is not null then 'NP'
				--else coalesce( -- DG Commented out  as NHSNumberStatusId not in OP.Encounter
				--		 Encounter.NHSNumberStatusId
				--		,'RT'
				--		)
				end
				,'RT'
			)

		,Encounter.DistrictNo
		,Postcode = 
				coalesce(
						Postcode.Postcode
						,Encounter.Postcode
						)
		,PatientAddress1 = 
				coalesce(
				patientaddressatappointmentdate.AddressLine1
				,Encounter.PatientAddress1
						)
		,PatientAddress2 = 
				coalesce(
				patientaddressatappointmentdate.AddressLine2
				,Encounter.PatientAddress2
						)
		,PatientAddress3 = 
				coalesce(
				patientaddressatappointmentdate.AddressLine3
				,Encounter.PatientAddress3
						)
		,PatientAddress4 = 
				coalesce(
				patientaddressatappointmentdate.AddressLine4
				,Encounter.PatientAddress4
						)
		,DHACode
		,Encounter.EthnicOriginCode
		,Encounter.MaritalStatusCode
		,Encounter.ReligionCode
		,RegisteredGpCode = RegisteredGP.NationalCode
		,RegisteredGpPracticeCode

		,SiteCode = 
			COALESCE(
				 DestinationSiteCode
				,SiteCode
			)

		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode = 
			DisposalCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode

-- this change needs to be pushed back upstream once users start using WarehouseOLAPMerged / WarehouseReportingMerged
		--,ReferringConsultantCode = 
		,ReferralConsultantCode = 
			CASE 
			WHEN AppointmentTypeCode = 'WA' THEN ConsultantCode
			ELSE ReferringConsultantCode
			END

		--,ReferringSpecialtyCode = 
		,ReferralSpecialtyCode = 
			CASE 
			WHEN AppointmentTypeCode = 'WA' THEN SpecialtyCode
			ELSE ReferringSpecialtyCode
			END

		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode = EpisodicGp.NationalCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryOperationDate
		,SecondaryOperationCode1
		,SecondaryOperationDate1
		,SecondaryOperationCode2
		,SecondaryOperationDate2
		,SecondaryOperationCode3
		,SecondaryOperationDate3
		,SecondaryOperationCode4
		,SecondaryOperationDate4
		,SecondaryOperationCode5
		,SecondaryOperationDate5
		,SecondaryOperationCode6
		,SecondaryOperationDate6
		,SecondaryOperationCode7
		,SecondaryOperationDate7
		,SecondaryOperationCode8
		,SecondaryOperationDate8
		,SecondaryOperationCode9
		,SecondaryOperationDate9
		,SecondaryOperationCode10
		,SecondaryOperationDate10
		,SecondaryOperationCode11
		,SecondaryOperationDate11

		,PurchaserCode = 
			CASE 
			WHEN AdminCategoryCode = '02' THEN 'VPP00'
			ELSE
				rtrim(
					coalesce(	
						 PCT.OrganisationCode
						,Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,'5NT'
					) 
				)
			END

		,Encounter.ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode

		,RTTCurrentProviderCode = 
			Provider.NationalCode

		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,Created
		,Updated
		,ByWhom
		,LocalAdminCategoryCode

		,PCTCode =	
				Encounter.PCTCode

		,LocalityCode
		,IsWardAttender
		,ClockStartDate
		,RTTBreachDate
		,HomePhone
		,WorkPhone
		,SourceOfReferralGroupCode
		,WardCode

		,ReferredByCode = 
			CASE
			WHEN ReferredByCode IN('GP', 'GPN')	  THEN 'GP' 
			WHEN ReferredByCode IN('GDP')		  THEN 'GDP'
			WHEN ReferredByCode IN('CNN', 'CON')  THEN 'CONS'
			ELSE 'OTH'
			END

		,ReferrerCode

		,LastDNAorPatientCancelledDate
		,DirectorateCode
		,Encounter.StaffGroupCode

		,ReferralSpecialtyTypeCode = ReferringSpecialtyTypeCode

		,AppointmentOutcomeCode = AttendanceOutcomeCode

		,MedicalStaffTypeCode = 
				StaffGroup.MedicalStaffTypeCode

		,CommissioningSerialNo =
			CASE
				WHEN AdminCategoryCode = '02' 
					THEN 'YPPPAY'
				WHEN ContractSerialNo IS NULL 
				
					THEN rtrim(
							coalesce(	
									 Practice.ParentOrganisationCode
									,Postcode.PCTCode
									,'5NT'
							) 
						 ) + '00A'
				ELSE 
					ContractSerialNo
			END

		,ContextCode = 'CEN||PAS'
		,RegisteredGP.NationalCode

		,DerivedFirstAttendanceFlag =	
			Encounter.DerivedFirstAttendanceFlag
			--case
			--	when exists --there has been a previous attendance
			--		(
			--		select
			--			1
			--		from
			--			Warehouse.OP.Encounter PreviousAttendance

			--		inner join Warehouse.dbo.EntityLookup Attend
			--		on	Attend.EntityTypeCode = 'ATTENDANCEOUTCOMEATTENDED'
			--		and	Attend.EntityCode = PreviousAttendance.AppointmentStatusCode

			--		where
			--			PreviousAttendance.SourcePatientNo = Encounter.SourcePatientNo
			--		and	PreviousAttendance.SourceEncounterNo = Encounter.SourceEncounterNo
			--		and	PreviousAttendance.AppointmentTime < Encounter.AppointmentTime
			--		)
			--	then '2'
			--	else '1'
			--	end

		,AttendanceIdentifier =
			CASE
			WHEN IsWardAttender = 1 THEN Encounter.SourceEncounterNo
			ELSE left(
					replace(
						CONVERT(
							 varchar
							,Encounter.AppointmentTime
							,108
						)
						,':'
						,''
					)
					,4
				 ) 
			END
			+
			Encounter.DoctorCode

		-- this needs checking
		,RegisteredGpAtAppointmentCode = 
			RegisteredGpAtAppointment.GpCode

		,RegisteredGpPracticeAtAppointmentCode = 
			RegisteredGpAtAppointment.GpPracticeCode

	from
		Warehouse.OP.Encounter Encounter

	LEFT OUTER JOIN warehouse.PAS.Gp RegisteredGP
	ON	RegisteredGP.GpCode = Encounter.RegisteredGpCode
		
	LEFT OUTER JOIN warehouse.PAS.Gp EpisodicGP
	ON	EpisodicGP.GpCode = Encounter.EpisodicGpCode
		
	LEFT OUTER JOIN warehouse.PAS.PatientGp RegisteredGpAtAppointment
	ON RegisteredGpAtAppointment.SourcePatientNo = Encounter.SourcePatientNo
	AND Encounter.AppointmentDate BETWEEN RegisteredGpAtAppointment.EffectiveFromDate AND RegisteredGpAtAppointment.EffectiveToDate

	left outer join Organisation.dbo.PCT PCT
	on left(encounter.PurchaserCode, 3) = PCT.OrganisationCode

	LEFT OUTER JOIN Warehouse.PAS.Patient
	ON	Patient.SourcePatientNo = Encounter.SourcePatientNo

	LEFT OUTER JOIN Warehouse.PAS.StaffGroup
	ON StaffGroup.StaffGroupCode = Encounter.StaffGroupCode 
		
	LEFT OUTER JOIN warehouse.PAS.Provider Provider
	ON Provider.ProviderCode = Encounter.RTTCurrentProviderCode
	
	left outer join warehouse.PAS.PatientAddress patientaddressatappointmentdate
	on patientaddressatappointmentdate.SourcePatientNo = encounter.SourcePatientNo
	and encounter.AppointmentDate between patientaddressatappointmentdate.EffectiveFromDate and coalesce(patientaddressatappointmentdate.EffectiveToDate, getdate())

	LEFT OUTER JOIN Organisation.dbo.Postcode Postcode -- DG 19/06/2012 - changed to post code at time of appointment
	on	Postcode.Postcode = 
			case
			when len(patientaddressatappointmentdate.Postcode) = 8 then patientaddressatappointmentdate.Postcode
			else left(patientaddressatappointmentdate.Postcode, 3) + ' ' + right(patientaddressatappointmentdate.Postcode, 4) 
			end

	--LEFT OUTER JOIN Organisation.dbo.Practice Practice
	--ON	Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode
	
	left outer join Organisation.dbo.Practice Practice -- DG 19/06/2012 - changed to GP at time of appointment
	ON	Practice.OrganisationCode = RegisteredGpAtAppointment.GpPracticeCode
	

	) Encounter
LEFT OUTER JOIN warehouse.PAS.Gp ReferringGP
ON	ReferringGP.GpCode = Encounter.ReferrerCode
AND Encounter.ReferredByCode = 'GP'

LEFT OUTER JOIN warehouse.PAS.Gdp ReferringGdp
ON	ReferringGdp.GdpCode = Encounter.ReferrerCode
AND Encounter.ReferredByCode = 'GDP'
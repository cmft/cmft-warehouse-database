﻿CREATE view [Cen].[AEEncounter] as

select
	 EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.UniqueBookingReferenceNo
	,Encounter.PathwayId
	,Encounter.PathwayIdIssuerCode
	,Encounter.RTTStatusCode
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.DistrictNo
	,Encounter.TrustNo
	,Encounter.CasenoteNo
	,Encounter.DistrictNoOrganisationCode

	,NHSNumber =
		replace(
			coalesce(
				Patient.NHSNumber
				,case
				when
					(
						Encounter.NHSNumber = '1234567899'  
					or	rtrim(Encounter.NHSNumber) = ''
					or Encounter.NHSNumber = 'REMOVE'
					or left(Encounter.NHSNumber,4) = 'SLF?' 
					or left(Encounter.NHSNumber,4) = 'MAN?'
					)
				then null
				else Encounter.NHSNumber
				end
			)
			,' '
			,''
		)

	,NHSNumberStatusId =
		coalesce(
			 Patient.NHSNumberStatusId
			,case
			when 
				(
					Encounter.NHSNumber = '1234567899'  
				or	Encounter.NHSNumber is null
				or	rtrim(Encounter.NHSNumber) = ''
				or Encounter.NHSNumber = 'REMOVE'
				or left(Encounter.NHSNumber,4) = 'SLF?'
				or left(Encounter.NHSNumber,4) = 'MAN?' 
				)			
			then 'RT'

			when 
				Encounter.NHSNumber is not null
			then 'NP'

			else coalesce(
					 Encounter.NHSNumberStatusId
					,'RT'
				)
			end
			,'RT'
		)
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.Postcode
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.CarerSupportIndicator
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.AttendanceNumber
	,Encounter.ArrivalModeCode
	,Encounter.AttendanceCategoryCode
	,Encounter.AttendanceDisposalCode
	,Encounter.IncidentLocationTypeCode
	,Encounter.PatientGroupCode
	,Encounter.SourceOfReferralCode
	,Encounter.ArrivalDate
	,Encounter.ArrivalTime
	,Encounter.AgeOnArrival
	,Encounter.InitialAssessmentTime
	,Encounter.SeenForTreatmentTime
	,Encounter.AttendanceConclusionTime
	,Encounter.DepartureTime
	,Encounter.CommissioningSerialNo
	,Encounter.NHSServiceAgreementLineNo
	,Encounter.ProviderReferenceNo
	,Encounter.CommissionerReferenceNo
	,Encounter.ProviderCode
	,Encounter.CommissionerCode
	,Encounter.StaffMemberCode
	,Encounter.InvestigationCodeFirst
	,Encounter.InvestigationCodeSecond
	,Encounter.DiagnosisCodeFirst
	,Encounter.DiagnosisCodeSecond
	,Encounter.TreatmentCodeFirst
	,Encounter.TreatmentCodeSecond
	,Encounter.PASHRGCode
	,Encounter.HRGVersionCode
	,Encounter.PASDGVPCode
	,Encounter.SiteCode
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.InterfaceCode
	,Encounter.PCTCode
	,Encounter.ResidencePCTCode
	,Encounter.InvestigationCodeList
	,Encounter.TriageCategoryCode
	,Encounter.PresentingProblem
	,Encounter.ToXrayTime
	,Encounter.FromXrayTime
	,Encounter.ToSpecialtyTime
	,Encounter.SeenBySpecialtyTime
	,Encounter.EthnicCategoryCode
	,Encounter.ReferredToSpecialtyCode
	,Encounter.DecisionToAdmitTime
	,Encounter.DischargeDestinationCode
	,Encounter.RegisteredTime
	,Encounter.TransportRequestTime
	,Encounter.TransportAvailableTime
	,Encounter.AscribeLeftDeptTime
	,Encounter.CDULeftDepartmentTime
	,Encounter.PCDULeftDepartmentTime
	,Encounter.TreatmentDateFirst
	,Encounter.TreatmentDateSecond
	,Encounter.SourceAttendanceDisposalCode
	,Encounter.AmbulanceArrivalTime
	,Encounter.AmbulanceCrewPRF
	,Encounter.UnplannedReattend7Day
	,Encounter.ArrivalTimeAdjusted
	,Encounter.ClinicalAssessmentTime
	,Encounter.Reportable
	,Encounter.SourceCareGroupCode
	,EpisodicGpCode = 
		coalesce(
			 CASE WHEN EpisodicGpAtArrival.GpCode = 'G9999981' THEN NULL ELSE EpisodicGpAtArrival.GpCode END
			,Encounter.RegisteredGpCode
			,'G9999998'
		)
	,EpisodicGpPracticeCode = 
		coalesce(
			 CASE WHEN EpisodicGpAtArrival.GPPracticeCode = 'V81998' THEN NULL ELSE EpisodicGpAtArrival.GPPracticeCode END
			,Encounter.RegisteredGpPracticeCode
			,'V81999'
		)
	,Encounter.CarePathwayAttendanceConclusionTime
	,Encounter.CarePathwayDepartureTime
	,ContextCode =
		case
		when Encounter.SiteCode = 'RW3TR' and Encounter.InterfaceCode = 'ADAS' then 'TRA||ADAS'
		else
			'CEN||' + 
			case
			when Encounter.InterfaceCode in ('INFO', 'INQ') then 'PAS'
			else Encounter.InterfaceCode
			end
		end
from
	Warehouse.AE.Encounter

left outer join Warehouse.PAS.Patient
	on	Patient.DistrictNo = Encounter.DistrictNo

left outer join  warehouse.PAS.PatientGp EpisodicGpAtArrival
	on EpisodicGpAtArrival.SourcePatientNo = Patient.SourcePatientNo
and Encounter.ArrivalDate between EpisodicGpAtArrival.EffectiveFromDate and isnull(EpisodicGpAtArrival.EffectiveToDate,getdate())
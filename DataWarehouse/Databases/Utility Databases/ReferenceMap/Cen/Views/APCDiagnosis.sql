﻿CREATE  view [Cen].[APCDiagnosis]

as

select
       Diagnosis.[SourceUniqueID]
       --,Diagnosis.[SourceSpellNo]
       ,Diagnosis.[SourcePatientNo]
       ,Diagnosis.[ProviderSpellNo]
       ,Diagnosis.[SourceEncounterNo]
       ,Diagnosis.[SequenceNo]
       ,[DiagnosisCode] = rtrim(left(Diagnosis.[DiagnosisCode],6))
       ,Diagnosis.[APCSourceUniqueID]
       --,[Created]
       --,[Updated]
       --,[ByWhom]
       ,ContextCode = 'CEN||PAS'
from
       [warehouse].[APC].[Diagnosis] Diagnosis
inner join
       [warehouse].[APC].[Encounter] Encounter
on
       Diagnosis.[APCSourceUniqueID] = Encounter.SourceUniqueID

union all
       
select
       [SourceUniqueID]
       --,[SourceSpellNo]
       ,[SourcePatientNo]
       ,[ProviderSpellNo]
       ,[SourceEncounterNo]
       ,[SequenceNo] = 0
       ,rtrim(left([PrimaryDiagnosisCode],6))
       ,[SourceUniqueID]
       --,[Created]
       --,[Updated]
       --,[ByWhom]
       ,ContextCode = 'CEN||PAS'
from
       [warehouse].[APC].[Encounter] Encounter
where
       [PrimaryDiagnosisCode] is not null
﻿create  view [Cen].[APCOperation]
as

select
	Operation.[SourceUniqueID]
	--,Operation.[SourceSpellNo]
	,Operation.[SourcePatientNo]
	,Operation.[ProviderSpellNo]
	,Operation.[SourceEncounterNo]
	,Operation.[SequenceNo]
	,Operation.[OperationCode]
	,Operation.[APCSourceUniqueID]
	--,[Created]
	--,[Updated]
	--,[ByWhom]
	,ContextCode = 'CEN||PAS'
from
	[Warehouse].[APC].[Operation] Operation
inner join
	[Warehouse].[APC].[Encounter] Encounter
on
	Operation.[APCSourceUniqueID] = Encounter.SourceUniqueID

union all
	
select
	[SourceUniqueID]
	--,[SourceSpellNo]
	,[SourcePatientNo]
	,[ProviderSpellNo]
	,[SourceEncounterNo]
	,[SequenceNo] = 0
	,[PrimaryOperationCode]
	,[SourceUniqueID]
	--,[Created]
	--,[Updated]
	--,[ByWhom]
	,ContextCode = 'CEN||PAS'
from
	[Warehouse].[APC].[Encounter] Encounter
where
	[PrimaryOperationCode] is not null
﻿CREATE VIEW [Cen].[RFEncounter] AS

SELECT
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,PatientTitle = 
			coalesce(
				 Patient.Title
				,Encounter.PatientTitle
			)
	,PatientForename = 
			coalesce(
				 Patient.Forenames
				,Encounter.PatientForename
			)
	,PatientSurname = 
			coalesce(
				 Patient.Surname
				,Encounter.PatientSurname
			)
	,DateOfBirth = 
		coalesce(
				Patient.DateOfBirth
			,Encounter.DateOfBirth
		)

	,DateOfDeath =  
		coalesce(
			 Patient.DateOfDeath
			,Encounter.DateOfDeath
		)

	,SexCode = 
			coalesce(
				 Patient.SexCode
				,Encounter.SexCode
			)
	,Encounter.NHSNumber
	,NHSNumberStatusCode =
			coalesce(
				 Patient.NHSNumberStatusId
				,case
				when 
					(
						Encounter.NHSNumber = '1234567899'  
					or	Encounter.NHSNumber is null
					or	rtrim(Encounter.NHSNumber) = ''
					or  Encounter.NHSNumber = 'REMOVE'
					or  left(Encounter.NHSNumber , 4) = 'SLF?'
					or  left(Encounter.NHSNumber , 4) = 'MAN?' 
					) then 'RT'
				when Encounter.NHSNumber is not null then 'NP'
				end
				,'RT'
			)
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,RegisteredGpCode = 
		RegisteredGP.NationalCode
	,Encounter.RegisteredGpPracticeCode
	,EpisodicGpCode = 
		EpisodicGp.NationalCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.EpisodicGdpCode
	,Encounter.SiteCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.SourceOfReferralCode
	,Encounter.PriorityCode
	,Encounter.ReferralDate
	,Encounter.DischargeDate
	,Encounter.DischargeTime
	,Encounter.DischargeReasonCode
	,Encounter.DischargeReason
	,Encounter.AdminCategoryCode
	,Encounter.ContractSerialNo
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = 
			Provider.NationalCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.NextFutureAppointmentDate
	,Encounter.ReferralComment
	,Encounter.InterfaceCode
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.ReasonForReferralCode
	,Encounter.SourceOfReferralGroupCode
	,Encounter.DirectorateCode
	,Encounter.CasenoteNo
	,Encounter.PCTCode
	,ContextCode = 'CEN||PAS'
	,RegisteredGpAtReferralCode = 
		RegisteredGpAtReferral.GpCode
	,RegisteredGpPracticeAtReferralCode = 
		RegisteredGpAtReferral.GpPracticeCode
FROM
	warehouse.RF.Encounter Encounter
	
LEFT OUTER JOIN warehouse.PAS.Gp RegisteredGP
ON	RegisteredGP.GpCode = Encounter.RegisteredGpCode
	
LEFT OUTER JOIN warehouse.PAS.Gp EpisodicGP
ON	EpisodicGP.GpCode = Encounter.EpisodicGpCode
	
LEFT OUTER JOIN warehouse.PAS.PatientGp RegisteredGpAtReferral
ON RegisteredGpAtReferral.SourcePatientNo = Encounter.SourcePatientNo
AND Encounter.ReferralDate BETWEEN RegisteredGpAtReferral.EffectiveFromDate AND RegisteredGpAtReferral.EffectiveToDate

LEFT OUTER JOIN Warehouse.PAS.Patient
ON	Patient.SourcePatientNo = Encounter.SourcePatientNo

LEFT OUTER JOIN warehouse.PAS.Provider Provider
ON Provider.ProviderCode = Encounter.RTTCurrentProviderCode
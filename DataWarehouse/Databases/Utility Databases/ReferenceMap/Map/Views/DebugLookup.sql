﻿CREATE view [Map].[DebugLookup] as

select
	 AttributeXrefID
	,Attribute.AttributeCode
	,Attribute.Attribute
	,Context.ContextCode
	,Context.Context
	,Value.ValueCode
	,Value.Value
	,XrefAttributeCode = XrefAttribute.AttributeCode
	,XrefAttribute = XrefAttribute.Attribute
	,XrefContextCode = XrefContext.ContextCode
	,XrefContext = XrefContext.Context
	,XrefValueCode = XrefValue.ValueCode
	,XrefValue = XrefValue.Value
from
	MAP.AttributeXref

left join MAP.Value
on	Value.ValueID = AttributeXref.ValueID

left join MAP.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

left join MAP.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

left join MAP.Context
on	Context.ContextID = AttributeContext.ContextID



left join MAP.Value XrefValue
on	XrefValue.ValueID = AttributeXref.ValueXrefID

left join MAP.AttributeContext XrefAttributeContext
on	XrefAttributeContext.AttributeContextID = XrefValue.AttributeContextID

left join MAP.Attribute XrefAttribute
on	XrefAttribute.AttributeID = XrefAttributeContext.AttributeID

left join MAP.Context XrefContext
on	XrefContext.ContextID = XrefAttributeContext.ContextID
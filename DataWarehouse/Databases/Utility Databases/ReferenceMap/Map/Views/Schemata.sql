﻿CREATE view [Map].[Schemata] as

select
	 ColumnBase.ColumnID
	,ColumnBase.[Column]
	,TableBase.TableID
	,TableBase.[Table]
	,SchemaBase.SchemaID
	,SchemaBase.[Schema]
	,DatabaseBase.DatabaseID
	,DatabaseBase.[Database]
	,DatabaseBase.DatabaseDescription
	,ServerBase.ServerID
	,ServerBase.Server
	,ServerBase.ServerDecription
from
	Map.ColumnBase

inner join Map.TableBase
on	TableBase.TableID = ColumnBase.TableID

inner join Map.SchemaBase
on	SchemaBase.SchemaID = TableBase.SchemaID

inner join Map.DatabaseBase
on	DatabaseBase.DatabaseID = SchemaBase.DatabaseID

inner join Map.ServerBase
on	ServerBase.ServerID = DatabaseBase.ServerID
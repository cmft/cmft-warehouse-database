﻿create view [Map].[AttributeContextColumn] as
select
	 Attribute
	,Context
	,[Server]
	,[Database]
	,[Schema]
	,[Table]
	,[Column]
	,ColumnType
from
	Map.ColumnBase

inner join Map.ColumnType
on	ColumnType.ColumnTypeID = ColumnBase.ColumnTypeID

inner join Map.AttributeContext
on	AttributeContext.AttributeContextID = ColumnBase.AttributeContextID

inner join Map.Context
on	Context.ContextID = AttributeContext.ContextID

inner join Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join Map.TableBase
on	TableBase.TableID = ColumnBase.TableID

inner join Map.SchemaBase
on	SchemaBase.SchemaID = TableBase.SchemaID

inner join Map.DatabaseBase
on	DatabaseBase.DatabaseID = SchemaBase.DatabaseID

inner join Map.ServerBase
on	ServerBase.ServerID = DatabaseBase.ServerID
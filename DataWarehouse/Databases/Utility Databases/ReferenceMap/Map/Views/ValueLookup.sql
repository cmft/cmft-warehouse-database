﻿create view [Map].[ValueLookup] as
select
	 AttributeNational.AttributeCode
	,AttributeNational.Attribute
	,NationalValueCode = ValueNational.ValueCode
	,NationalValue = ValueNational.Value
	,LocalValueCode = ValueLocal.ValueCode
	,LocalValue = ValueLocal.Value
	,SourceContextCode = ContextSource.ContextCode
	,SourceContext = ContextSource.Context
	,SourceValueCode = ValueSource.ValueCode
	,SourceValue = ValueSource.Value
from
	MAP.AttributeXref XrefNational

inner join MAP.Value ValueNational
on	ValueNational.ValueID = XrefNational.ValueID

inner join MAP.AttributeContext AttributeContextNational
on	AttributeContextNational.AttributeContextID = ValueNational.AttributeContextID

inner join MAP.Attribute AttributeNational
on	AttributeNational.AttributeID = AttributeContextNational.AttributeID

inner join MAP.Context ContextNational
on	ContextNational.ContextID = AttributeContextNational.ContextID
and	ContextNational.ContextCode = 'N'


inner join MAP.AttributeXref XrefLocal
on	XrefLocal.ValueXrefID = XrefNational.ValueID

inner join MAP.Value ValueLocal
on	ValueLocal.ValueID = XrefLocal.ValueID

inner join MAP.AttributeContext AttributeContextLocal
on	AttributeContextLocal.AttributeContextID = ValueLocal.AttributeContextID

inner join MAP.Attribute AttributeLocal
on	AttributeLocal.AttributeID = AttributeContextLocal.AttributeID

inner join MAP.Context ContextLocal
on	ContextLocal.ContextID = AttributeContextLocal.ContextID


inner join MAP.AttributeXref XrefSource
on	XrefSource.ValueXrefID = XrefLocal.ValueID

inner join MAP.Value ValueSource
on	ValueSource.ValueID = XrefSource.ValueID

inner join MAP.AttributeContext AttributeContextSource
on	AttributeContextSource.AttributeContextID = ValueSource.AttributeContextID

inner join MAP.Attribute AttributeSource
on	AttributeSource.AttributeID = AttributeContextSource.AttributeID

inner join MAP.Context ContextSource
on	ContextSource.ContextID = AttributeContextSource.ContextID
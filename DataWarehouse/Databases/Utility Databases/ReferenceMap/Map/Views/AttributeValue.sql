﻿create view [Map].[AttributeValue] as

select
	 Value.ValueID
	,Value.ValueCode
	,Attribute.AttributeCode
	,Context.ContextCode
from
	MAP.Value

inner join MAP.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

inner join MAP.Context
on	Context.ContextID = AttributeContext.ContextID

inner join MAP.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID
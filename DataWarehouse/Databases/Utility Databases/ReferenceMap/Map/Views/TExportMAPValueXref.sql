﻿CREATE view [Map].[TExportMAPValueXref] as


select
	 AttributeCode = Attribute.AttributeCode
	,Attribute = Attribute.Attribute
	,SourceContextCode = Context.ContextCode
	,SourceContext = Context.Context
	,SourceValueCode = Value.ValueCode
	,SourceValue = Value.Value
	,LocalValueCode = LocalValue.ValueCode
	,LocalValue = LocalValue.Value
	,NationalValueCode = NationalValue.ValueCode
	,NationalValue = NationalValue.Value
from
	ReferenceMap.Map.Value

inner join ReferenceMap.Map.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

inner join ReferenceMap.Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join ReferenceMap.Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.BaseContext = 'True'


inner join ReferenceMap.Map.AttributeXref 
on	AttributeXref.ValueID = Value.ValueID

inner join ReferenceMap.Map.Value LocalValue
on	LocalValue.ValueID = AttributeXref.ValueXrefID

inner join ReferenceMap.Map.AttributeContext LocalAttributeContext
on	LocalAttributeContext.AttributeContextID = LocalValue.AttributeContextID

inner join ReferenceMap.Map.Context LocalContext
on	LocalContext.ContextID = LocalAttributeContext.ContextID
and	LocalContext.ContextCode = 'L'
and	Context.BaseContext = 'True'


inner join ReferenceMap.Map.AttributeXref NationalAttributeXref
on	NationalAttributeXref.ValueID = Value.ValueID

inner join ReferenceMap.Map.Value NationalValue
on	NationalValue.ValueID = NationalAttributeXref.ValueXrefID

inner join ReferenceMap.Map.AttributeContext NationalAttributeContext
on	NationalAttributeContext.AttributeContextID = NationalValue.AttributeContextID

inner join ReferenceMap.Map.Context NationalContext
on	NationalContext.ContextID = NationalAttributeContext.ContextID
and	NationalContext.ContextCode = 'N'
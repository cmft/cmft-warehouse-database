﻿--USE [WarehouseOLAPMerged]
--GO

--/****** Object:  View [dbo].[OlapAEArrivalMode]    Script Date: 02/23/2012 15:55:19 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO


CREATE view [Map].[ValueHierarchy] as


select
	 AttributeContext.AttributeContextID
	,AttributeID = Attribute.AttributeID
	,AttributeCode = Attribute.AttributeCode
	,Attribute = Attribute.Attribute
	,SourceContextID = Context.ContextID
	,SourceContextCode = Context.ContextCode
	,SourceContext = Context.Context
	,SourceValueID = Value.ValueID
	,SourceValueCode = Value.ValueCode
	,SourceValue = Value.Value + ' (' + Context.ContextCode + ':' + Value.ValueCode + ')'
	,LocalValueID = LocalValue.ValueID
	,LocalValueCode = LocalValue.ValueCode
	,LocalValue = LocalValue.Value
	,NationalValueID = NationalValue.ValueID
	,NationalValueCode = NationalValue.ValueCode
	,NationalValue = NationalValue.Value
from
	ReferenceMap.Map.Value

inner join ReferenceMap.Map.AttributeContext
on	AttributeContext.AttributeContextID = Value.AttributeContextID

inner join ReferenceMap.Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join ReferenceMap.Map.Context
on	Context.ContextID = AttributeContext.ContextID
and	Context.BaseContext = 'True'


inner join ReferenceMap.Map.AttributeXref 
on	AttributeXref.ValueID = Value.ValueID

inner join ReferenceMap.Map.Value LocalValue
on	LocalValue.ValueID = AttributeXref.ValueXrefID

inner join ReferenceMap.Map.AttributeContext LocalAttributeContext
on	LocalAttributeContext.AttributeContextID = LocalValue.AttributeContextID

inner join ReferenceMap.Map.Context LocalContext
on	LocalContext.ContextID = LocalAttributeContext.ContextID
and	LocalContext.ContextCode = 'L'
and	Context.BaseContext = 'True'


inner join ReferenceMap.Map.AttributeXref NationalAttributeXref
on	NationalAttributeXref.ValueID = Value.ValueID

inner join ReferenceMap.Map.Value NationalValue
on	NationalValue.ValueID = NationalAttributeXref.ValueXrefID

inner join ReferenceMap.Map.AttributeContext NationalAttributeContext
on	NationalAttributeContext.AttributeContextID = NationalValue.AttributeContextID

inner join ReferenceMap.Map.Context NationalContext
on	NationalContext.ContextID = NationalAttributeContext.ContextID
and	NationalContext.ContextCode = 'N'
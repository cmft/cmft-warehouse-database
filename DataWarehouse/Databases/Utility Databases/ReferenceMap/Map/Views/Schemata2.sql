﻿create view [Map].[Schemata2] as

select
	 AttributeContext.AttributeContextID
	,Attribute.AttributeID
	,Attribute.AttributeCode
	,Attribute.Attribute
	,Context.ContextID
	,Context.ContextCode
	,Context.Context
	,ColumnBase.ColumnID
	,ColumnBase.[Column]
	,ColumnBase.ColumnTypeID
	,ColumnType.ColumnType
	,TableBase.TableID
	,TableBase.[Table]
	,SchemaBase.SchemaID
	,SchemaBase.[Schema]
	,DatabaseBase.DatabaseID
	,DatabaseBase.[Database]
	,DatabaseBase.DatabaseDescription
	,ServerBase.ServerID
	,ServerBase.Server
	,ServerBase.ServerDecription
from
	Map.ColumnBase

inner join Map.ColumnType
on	ColumnType.ColumnTypeID = ColumnBase.ColumnTypeID

inner join Map.TableBase
on	TableBase.TableID = ColumnBase.TableID

inner join Map.SchemaBase
on	SchemaBase.SchemaID = TableBase.SchemaID

inner join Map.DatabaseBase
on	DatabaseBase.DatabaseID = SchemaBase.DatabaseID

inner join Map.ServerBase
on	ServerBase.ServerID = DatabaseBase.ServerID

inner join Map.AttributeContext
on	AttributeContext.AttributeContextID = ColumnBase.AttributeContextID

inner join Map.Attribute
on	Attribute.AttributeID = AttributeContext.AttributeID

inner join Map.Context
on	Context.ContextID = AttributeContext.ContextID
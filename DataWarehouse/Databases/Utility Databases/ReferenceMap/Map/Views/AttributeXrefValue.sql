﻿CREATE view [Map].[AttributeXrefValue] as

select
	 AttributeXref.ValueID

	,ValueCode =
		case
		when AttributeValue.ValueCode = '-1'
		then null
		else AttributeValue.ValueCode
		end

	,AttributeValue.AttributeCode
	,AttributeValue.ContextCode
from
	Map.AttributeXref

inner join Map.AttributeValue
on	AttributeValue.ValueID = AttributeXref.ValueXrefID
﻿CREATE TABLE [Map].[TImportMAPValueXref](
	[AttributeCode] [varchar](max) NULL,
	[Attribute] [varchar](max) NULL,
	[ContextCode] [varchar](max) NULL,
	[Context] [varchar](max) NULL,
	[ValueCode] [varchar](max) NULL,
	[Value] [varchar](max) NULL,
	[LocalValueCode] [varchar](max) NULL,
	[LocalValue] [varchar](max) NULL,
	[NationalValueCode] [varchar](max) NULL,
	[NationalValue] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
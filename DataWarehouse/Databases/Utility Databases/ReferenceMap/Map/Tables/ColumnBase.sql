﻿CREATE TABLE [Map].[ColumnBase](
	[ColumnID] [int] IDENTITY(1,1) NOT NULL,
	[AttributeContextID] [int] NOT NULL,
	[ColumnTypeID] [int] NOT NULL,
	[Column] [varchar](max) NOT NULL,
	[TableID] [smallint] NOT NULL,
 CONSTRAINT [PK__ColumnBa__1AA1422F2145C81B] PRIMARY KEY CLUSTERED 
(
	[ColumnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [Map].[ColumnBase]  WITH CHECK ADD  CONSTRAINT [FK_ColumnBase_AttributeContext] FOREIGN KEY([AttributeContextID])
REFERENCES [Map].[AttributeContext] ([AttributeContextID])
GO

ALTER TABLE [Map].[ColumnBase] CHECK CONSTRAINT [FK_ColumnBase_AttributeContext]
GO
ALTER TABLE [Map].[ColumnBase]  WITH CHECK ADD  CONSTRAINT [FK_ColumnBase_ColumnBase] FOREIGN KEY([ColumnTypeID])
REFERENCES [Map].[ColumnType] ([ColumnTypeID])
GO

ALTER TABLE [Map].[ColumnBase] CHECK CONSTRAINT [FK_ColumnBase_ColumnBase]
GO
ALTER TABLE [Map].[ColumnBase]  WITH CHECK ADD  CONSTRAINT [FK_ColumnBase_TableBase] FOREIGN KEY([TableID])
REFERENCES [Map].[TableBase] ([TableID])
GO

ALTER TABLE [Map].[ColumnBase] CHECK CONSTRAINT [FK_ColumnBase_TableBase]
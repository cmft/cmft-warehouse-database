﻿CREATE TABLE [Map].[Encounter](
	[EncounterRecno] [bigint] IDENTITY(1,1) NOT NULL,
	[SchemaID] [tinyint] NOT NULL,
	[DatasetID] [tinyint] NOT NULL,
	[SourceEncounterRecno] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[EncounterRecno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_Encounter]    Script Date: 25/08/2015 16:34:53 ******/
CREATE NONCLUSTERED INDEX [IX_Encounter] ON [Map].[Encounter]
(
	[SourceEncounterRecno] ASC,
	[DatasetID] ASC,
	[SchemaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Map_Encounter_1]    Script Date: 25/08/2015 16:34:53 ******/
CREATE NONCLUSTERED INDEX [IX_Map_Encounter_1] ON [Map].[Encounter]
(
	[DatasetID] ASC
)
INCLUDE ( 	[EncounterRecno],
	[SchemaID],
	[SourceEncounterRecno]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
﻿CREATE TABLE [Map].[AttributeXref](
	[AttributeXrefID] [int] IDENTITY(1,1) NOT NULL,
	[ValueID] [int] NOT NULL,
	[ValueXrefID] [int] NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](255) NULL,
 CONSTRAINT [PK__Attribut__39B97B120C4AAB35] PRIMARY KEY CLUSTERED 
(
	[ValueID] ASC,
	[ValueXrefID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Map].[AttributeXref]  WITH CHECK ADD  CONSTRAINT [FK_AttributeXref_AttributeValue] FOREIGN KEY([ValueID])
REFERENCES [Map].[Value] ([ValueID])
GO

ALTER TABLE [Map].[AttributeXref] CHECK CONSTRAINT [FK_AttributeXref_AttributeValue]
GO
ALTER TABLE [Map].[AttributeXref]  WITH CHECK ADD  CONSTRAINT [FK_AttributeXref_AttributeValue1] FOREIGN KEY([ValueXrefID])
REFERENCES [Map].[Value] ([ValueID])
GO

ALTER TABLE [Map].[AttributeXref] CHECK CONSTRAINT [FK_AttributeXref_AttributeValue1]
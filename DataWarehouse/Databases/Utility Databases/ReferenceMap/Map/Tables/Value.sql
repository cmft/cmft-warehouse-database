﻿CREATE TABLE [Map].[Value](
	[ValueID] [int] IDENTITY(1,1) NOT NULL,
	[AttributeContextID] [int] NOT NULL,
	[ValueCode] [varchar](255) NOT NULL,
	[Value] [varchar](512) NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](255) NULL,
 CONSTRAINT [PK_AttributeValue] PRIMARY KEY CLUSTERED 
(
	[ValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Map].[Value]  WITH CHECK ADD  CONSTRAINT [FK_AttributeValue_AttributeContext] FOREIGN KEY([AttributeContextID])
REFERENCES [Map].[AttributeContext] ([AttributeContextID])
GO

ALTER TABLE [Map].[Value] CHECK CONSTRAINT [FK_AttributeValue_AttributeContext]
GO
/****** Object:  Index [Idx_TIBAPC_AttributeContextID]    Script Date: 25/08/2015 16:34:53 ******/
CREATE NONCLUSTERED INDEX [Idx_TIBAPC_AttributeContextID] ON [Map].[Value]
(
	[AttributeContextID] ASC
)
INCLUDE ( 	[ValueCode],
	[ValueID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_Value]    Script Date: 25/08/2015 16:34:53 ******/
CREATE NONCLUSTERED INDEX [IX_Value] ON [Map].[Value]
(
	[AttributeContextID] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
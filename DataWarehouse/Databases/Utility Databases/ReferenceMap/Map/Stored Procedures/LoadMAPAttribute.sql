﻿create procedure [Map].[LoadMAPAttribute] as
--insert new Attributes
insert into MAP.Attribute
(
	 AttributeCode
	,Attribute
)


select distinct
	 AttributeCode
	,Attribute
from
	MAP.TImportMAPValueXref TImport


where
	not exists
		(
		select
			1
		from
			MAP.Attribute ExistValue
		where
			ExistValue.AttributeCode = TImport.AttributeCode
		)
﻿CREATE procedure [Map].[LoadMAPContext] as
--insert new Contexts
insert into MAP.Context
(
	 ContextCode
	,Context
	,[BaseContext]
)

select
	 ContextCode
	,Context
	,[BaseContext]
from
	(
	select distinct
		 ContextCode
		,Context
		,[BaseContext] = 1

	from
		MAP.TImportMAPValueXref 

	union

	select
		 ContextCode = 'L'
		,Context = 'Local'
		,[BaseContext] = 0

	union

	select
		 ContextCode = 'N'
		,Context = 'National'
		,[BaseContext] = 0
	)TImport

where
	not exists
		(
		select
			1
		from
			MAP.Context ExistValue
		where
			ExistValue.ContextCode = TImport.ContextCode
		)
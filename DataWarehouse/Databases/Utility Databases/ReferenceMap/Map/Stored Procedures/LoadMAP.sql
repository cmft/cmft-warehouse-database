﻿create procedure [Map].[LoadMAP] as

exec MAP.LoadMAPAttribute
exec MAP.LoadMAPContext
exec MAP.LoadMAPAttributeContext
exec MAP.LoadMAPValue
exec MAP.LoadMAPAttributeXref
﻿CREATE procedure [Map].[BuildOPWait]
(
	@SchemaID int
)

as

declare @OPWLDatasetID int = (select DatasetID from Map.DatasetBase where DatasetCode = 'OPWL')
declare @Schema varchar(5) = (select [Schema] from Map.SchemaBase where [SchemaID] = @SchemaID)


--remove orphaned records
Declare @SQLDeleteOrphanedRecords VarChar(1000)

select @SQLDeleteOrphanedRecords = '
	DELETE 
	from 
		Map.Encounter 
	where 
		Encounter.SchemaID = ' + cast(@SchemaID as varchar) + '
	and	Encounter.DatasetID = ' + cast(@OPWLDatasetID as varchar) + '
	and	not exists 
		(
		select 
			1 
		from ' +
			@Schema + '.OPWait with (nolock) 
		where 
			OPWait.EncounterRecno = Encounter.SourceEncounterRecno
		)
	'

exec (@SQLDeleteOrphanedRecords)


----inserts
Declare @SQLInsertRecords VarChar(1000)

select @SQLInsertRecords = '
	insert into Map.Encounter
	(
		 SchemaID 
		,DatasetID 
		,SourceEncounterRecno
	) 
	select 
		 SchemaID = ' + cast(@SchemaID as varchar) + '
		,DatasetID = ' + cast(@OPWLDatasetID as varchar) + '
		,SourceEncounterRecno = EncounterRecno 
	from ' 
		+ @Schema + '.OPWait 
	where 
		not exists 
		(
			select 
				1 
			from 
				Map.Encounter 
			where 
				SchemaID = ' + cast(@SchemaID as varchar) + '
			and	DatasetID = ' + cast(@OPWLDatasetID as varchar) + '
			and	Encounter.SourceEncounterRecno = OPWait.EncounterRecno
		)
	'
	
	
	--SELECT @SQLInsertRecords

exec (@SQLInsertRecords)
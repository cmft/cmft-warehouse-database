﻿CREATE PROCEDURE [Map].[GetInformationSchemaColumns]
	 @ServerName varchar(max)
	,@DatabaseName varchar(max)
	,@SchemaName varchar(max)
	,@TableName varchar(max)

AS
BEGIN
    SET NOCOUNT ON;
  
    BEGIN

        DECLARE @sql nvarchar(max) =
            'SELECT Column_Name' +
            ' FROM ' + QUOTENAME(@ServerName) + '.' + QUOTENAME(@DatabaseName)+ '.INFORMATION_SCHEMA.COLUMNS ' +
            'WHERE TABLE_NAME = ''' + @TableName + '''' +
            ' AND Table_SCHEMA = ''' + @SchemaName + '''' +
            ' AND DATA_TYPE NOT IN( ' + '''smalldatetime''' + ',' + '''datetime''' +  ') ' +
			' ORDER BY Column_Name'
        EXEC sp_executesql @sql    
       print @sql
      END
END
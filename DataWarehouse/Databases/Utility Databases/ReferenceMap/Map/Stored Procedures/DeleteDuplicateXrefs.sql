﻿create procedure [Map].[DeleteDuplicateXrefs] as

begin transaction

delete
from
	Map.AttributeXref
from
	Map.AttributeXref Member

inner join
	(
	select
		 ValueID
		,ValueXrefID
	from
		Map.AttributeXref
	group by
		 ValueID
		,ValueXrefID
	having	
		count(*) > 1
	) Dupes
on	Dupes.ValueID = Member.ValueID
and	Dupes.ValueXrefID = Member.ValueXrefID

where
	not
		(
		not exists
			(
			select
				1
			from
				Map.AttributeXref Xref
			where
				Xref.ValueID = Member.ValueID
			and	Xref.ValueXrefID = Member.ValueXrefID
			and	Xref.AttributeXrefID < Member.AttributeXrefID
			)
		)

--debug
	--select
	--	 ValueID
	--	,ValueXrefID
	--from
	--	Map.AttributeXref
	--group by
	--	 ValueID
	--	,ValueXrefID
	--having	
	--	count(*) > 1

commit
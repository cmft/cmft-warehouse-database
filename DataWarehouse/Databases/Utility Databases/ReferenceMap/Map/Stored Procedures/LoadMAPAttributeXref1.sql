﻿create procedure [Map].[LoadMAPAttributeXref1] as

insert into MAP.AttributeXref
(
	 ValueID
	,ValueXrefID
	,Created
	,ByWhom
)
select
	 NewAttributeXref.ValueID
	,NewAttributeXref.ValueXrefID
	,Created = getdate()
	,ByWhom = suser_name()
from
	(
	--source to local
	select
		 ValueID = SourceValue.ValueID
		,ValueXrefID = LocalValue.ValueID
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode


	--get source ValueID
	inner join MAP.Context SourceContext
	on	SourceContext.ContextCode = TImport.ContextCode

	inner join MAP.AttributeContext SourceAttributeContext
	on	SourceAttributeContext.AttributeID = Attribute.AttributeID
	and	SourceAttributeContext.ContextID = SourceContext.ContextID

	inner join MAP.Value SourceValue
	on	SourceValue.ValueCode = TImport.ValueCode
	and	SourceValue.AttributeContextID = SourceAttributeContext.AttributeContextID


	--get local ValueID
	inner join MAP.Context LocalContext
	on	LocalContext.ContextCode = 'L'

	inner join MAP.AttributeContext LocalAttributeContext
	on	LocalAttributeContext.AttributeID = Attribute.AttributeID
	and	LocalAttributeContext.ContextID = LocalContext.ContextID

	inner join MAP.Value LocalValue
	on	LocalValue.ValueCode = TImport.LocalValueCode
	and	LocalValue.AttributeContextID = LocalAttributeContext.AttributeContextID


	union


	--local to national
	select distinct
		 ValueID = LocalValue.ValueID
		,ValueXrefID = NationalValue.ValueID
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode


	--get local ValueID
	inner join MAP.Context LocalContext
	on	LocalContext.ContextCode = 'L'

	inner join MAP.AttributeContext LocalAttributeContext
	on	LocalAttributeContext.AttributeID = Attribute.AttributeID
	and	LocalAttributeContext.ContextID = LocalContext.ContextID

	inner join MAP.Value LocalValue
	on	LocalValue.ValueCode = TImport.LocalValueCode
	and	LocalValue.AttributeContextID = LocalAttributeContext.AttributeContextID


	--get national ValueID
	inner join MAP.Context NationalContext
	on	NationalContext.ContextCode = 'N'

	inner join MAP.AttributeContext NationalAttributeContext
	on	NationalAttributeContext.AttributeID = Attribute.AttributeID
	and	NationalAttributeContext.ContextID = NationalContext.ContextID

	inner join MAP.Value NationalValue
	on	NationalValue.ValueCode = TImport.NationalValueCode
	and	NationalValue.AttributeContextID = NationalAttributeContext.AttributeContextID


	union


	--national
	select distinct
		 ValueID = NationalValue.ValueID
		,ValueXrefID = null
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode


	--get national ValueID
	inner join MAP.Context NationalContext
	on	NationalContext.ContextCode = 'N'

	inner join MAP.AttributeContext NationalAttributeContext
	on	NationalAttributeContext.AttributeID = Attribute.AttributeID
	and	NationalAttributeContext.ContextID = NationalContext.ContextID

	inner join MAP.Value NationalValue
	on	NationalValue.ValueCode = TImport.NationalValueCode
	and	NationalValue.AttributeContextID = NationalAttributeContext.AttributeContextID

	) NewAttributeXref

where
	not exists
		(
		select
			1
		from
			MAP.AttributeXref
		where
			AttributeXref.ValueID = NewAttributeXref.ValueID
		)
﻿CREATE procedure [Map].[BuildMissingValues]
	 @activityTableID int
	,@NewRows int = 0 output
	,@debug bit = 0
as

--prevent parallel running and consequent duplication of missing values
set transaction isolation level serializable

begin transaction

	declare @activityTable varchar(max)
	declare @activityCodeColumn varchar(max)
	declare @referenceTable varchar(max)
	declare @referenceCodeColumn varchar(max)
	declare @referenceColumn varchar(max)
	declare @attributeContextID int
	declare @contextCode varchar(max)
	declare @sql varchar(max)

	declare @sqlTemplate varchar(max) = 
		'
		insert
		into
			Map.Value
			(
			 AttributeContextID
			,ValueCode
			,Value
			,Created
			,ByWhom
			)
		select distinct
			 AttributeContextID = <attributeContextID>
			,ValueCode = coalesce(cast(ActivityTable.<activityCodeColumn> as varchar(MAX)), ''-1'')

			,Value =
				coalesce(
					 ReferenceTable.<referenceColumn>
					,cast(ActivityTable.<activityCodeColumn> as varchar(MAX)) + '' - No Description''
					,''Not Specified''
				)

			,Created = GETDATE()
			,ByWhom = SUSER_NAME()
		from
			<activityTable> ActivityTable

		left join <referenceTable> ReferenceTable
		on	cast(ReferenceTable.<referenceCodeColumn> as varchar(max)) = cast(ActivityTable.<activityCodeColumn> as varchar(max))

		where
			ActivityTable.ContextCode = ''<contextCode>''

		and	not exists
			(
			select
				1
			from
				Map.Value
			where
				Value.ValueCode = coalesce(cast(ActivityTable.<activityCodeColumn> as varchar(MAX)), ''-1'')
			and	Value.AttributeContextID = <attributeContextID>
			)
		'

	declare @sqlTemplateNoReferenceTable varchar(max) = 
		'
		insert
		into
			Map.Value
			(
			 AttributeContextID
			,ValueCode
			,Value
			,Created
			,ByWhom
			)
		select distinct
			 AttributeContextID = <attributeContextID>
			,ValueCode = coalesce(Cast(ActivityTable.<activityCodeColumn> as varchar(MAX)), ''-1'')

			,Value =
				coalesce(
					 cast(ActivityTable.<activityCodeColumn> as varchar(MAX)) + '' - No Description''
					,''Not Specified''
				)

			,Created = GETDATE()
			,ByWhom = SUSER_NAME()
		from
			<activityTable> ActivityTable
		where
			ActivityTable.ContextCode = ''<contextCode>''

		and	not exists
			(
			select
				1
			from
				Map.Value
			where
				Value.ValueCode = coalesce(cast(ActivityTable.<activityCodeColumn> as varchar(MAX)), ''-1'')
			and	Value.AttributeContextID = <attributeContextID>
			)
		'

	select
		@activityTable = 
			'[' + ServerBase.[Server] + '].' +
			'[' + DatabaseBase.[Database] + '].' +
			'[' + SchemaBase.[Schema] + '].' +
			'[' + TableBase.[Table] + ']'
	from
		Map.TableBase

	inner join Map.SchemaBase
	on	SchemaBase.SchemaID = TableBase.SchemaID

	inner join Map.DatabaseBase
	on	DatabaseBase.DatabaseID = SchemaBase.DatabaseID

	inner join Map.ServerBase
	on	ServerBase.ServerID = DatabaseBase.ServerID

	where
		TableBase.TableID = @activityTableID

	create table #AttributeContext
		(
		AttributeContextID int
		,ContextCode varchar(max)
		)

	--get distinct Attribute/Context from the activity table

	--due to performace issues (specifically with Trafford APC WL data) a left join is used below and
	--any null records are removed in the cursor select statement
	select
		@sql =
			REPLACE(
				'
				insert into #AttributeContext
				select distinct
					 AttributeContext.AttributeContextID
					,Context.ContextCode
				from 
					Map.Attribute

				inner join Map.Context
				on	Context.BaseContext = 1
				and	exists
					(
					select
						1
					from
						<activityTable> ActivityTable
					where
						ActivityTable.ContextCode = Context.ContextCode
					)

				left join Map.AttributeContext
				on	AttributeContext.ContextID = Context.ContextID
				and	AttributeContext.AttributeID = Attribute.AttributeID

				'
				,'<activityTable>'
				,@activityTable
			)

	exec(@sql)

	set @NewRows = 0

	--loop through each context

	declare contextCursor cursor for
	select
		 AttributeContextID
		,ContextCode
	from
		#AttributeContext
	where
		AttributeContextID is not null

	OPEN contextCursor
  
	FETCH NEXT FROM contextCursor
	INTO @attributeContextID, @contextCode

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select
			 @activityCodeColumn = null
			,@referenceTable = null
			,@referenceCodeColumn = null
			,@referenceColumn = null


		--get the reference table code column name and reference table name
		select
			@referenceTable = 
				'[' + ServerBase.[Server] + '].' +
				'[' + DatabaseBase.[Database] + '].' +
				'[' + SchemaBase.[Schema] + '].' +
				'[' + TableBase.[Table] + ']'

			,@referenceCodeColumn =
				'[' + ColumnBase.[Column] + ']'

		from
			Map.ColumnBase

		inner join Map.TableBase
		on	TableBase.TableID = ColumnBase.TableID

		inner join Map.SchemaBase
		on	SchemaBase.SchemaID = TableBase.SchemaID

		inner join Map.DatabaseBase
		on	DatabaseBase.DatabaseID = SchemaBase.DatabaseID

		inner join Map.ServerBase
		on	ServerBase.ServerID = DatabaseBase.ServerID

		where
			ColumnBase.ColumnTypeID = 2	--Reference Code
		and	ColumnBase.AttributeContextID = @attributeContextID

		--get the reference value (description) column name
		select
			@referenceColumn =
				'[' + ColumnBase.[Column] + ']'
		from
			Map.ColumnBase
		where
			ColumnBase.AttributeContextID = @attributeContextID
		and	ColumnBase.ColumnTypeID = 3	--Reference


	--loop through each activity column code

		declare activityCodeColumnCursor cursor for

		--get the activity table column name
		select
			ActivityCodeColumn = '[' + ColumnBase.[Column] + ']'
		from
			Map.ColumnBase
		where
			ColumnBase.TableID = @activityTableID
		and	ColumnBase.AttributeContextID = @attributeContextID
		and	ColumnBase.ColumnTypeID = 1	--Activity Code

		OPEN activityCodeColumnCursor
	  
		FETCH NEXT FROM activityCodeColumnCursor
		INTO @activityCodeColumn

		WHILE @@FETCH_STATUS = 0

		BEGIN


			select @sql = 
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
				REPLACE(
					 @sqlTemplate
					,'<activityTable>'
					,@activityTable
				)
					,'<activityCodeColumn>'
					,@activityCodeColumn
				)	
					,'<attributeContextID>'
					,@attributeContextID
				)
					,'<referenceColumn>'
					,@referenceColumn
				)
					,'<referenceTable>'
					,@referenceTable
				)
					,'<referenceCodeColumn>'
					,@referenceCodeColumn
				)
					,'<contextCode>'
					,@contextCode
				)


			if @sql is null
				select @sql = 
					REPLACE(
					REPLACE(
					REPLACE(
					REPLACE(
						 @sqlTemplateNoReferenceTable
						,'<activityTable>'
						,@activityTable
					)
						,'<activityCodeColumn>'
						,@activityCodeColumn
					)	
						,'<attributeContextID>'
						,@attributeContextID
					)
						,'<contextCode>'
						,@contextCode
					)


			if @sql is not null
				if @debug = 1
					print @sql
				else
				begin
					execute(@sql)
					set @NewRows = @NewRows + @@ROWCOUNT
				end


			FETCH NEXT FROM activityCodeColumnCursor
			INTO @activityCodeColumn
		END
	  
		CLOSE activityCodeColumnCursor
		DEALLOCATE activityCodeColumnCursor


		FETCH NEXT FROM contextCursor
		INTO @attributeContextID, @contextCode
	END
  
	CLOSE contextCursor
	DEALLOCATE contextCursor

commit
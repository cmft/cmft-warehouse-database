﻿CREATE procedure [Map].[BuildAPCEncounter] 
(
	@SchemaID int
)

as


declare @APCDatasetID int = (select DatasetID from Map.DatasetBase where DatasetCode = 'APC')
declare @Schema varchar(5) = (select [Schema] from Map.SchemaBase where [SchemaID] = @SchemaID)


--remove orphaned records
Declare @SQLDeleteOrphanedRecords VarChar(1000)

select @SQLDeleteOrphanedRecords = '
	delete 
	from 
		Map.Encounter 
	where 
		Encounter.SchemaID = ' + cast(@SchemaID as varchar) + '
	and	Encounter.DatasetID = ' + cast(@APCDatasetID as varchar) + '
	and	not exists 
		(
		select 
			1 
		from ' +
			@Schema + '.APCEncounter with (nolock) 
		where 
			APCEncounter.EncounterRecno = Encounter.SourceEncounterRecno
		)
	'

exec (@SQLDeleteOrphanedRecords)


----inserts
Declare @SQLInsertRecords VarChar(1000)

select @SQLInsertRecords = '
	insert into Map.Encounter
	(
		 SchemaID 
		,DatasetID 
		,SourceEncounterRecno
	) 
	select 
		 SchemaID = ' + cast(@SchemaID as varchar) + '
		,DatasetID = ' + cast(@APCDatasetID as varchar) + '
		,SourceEncounterRecno = EncounterRecno 
	from ' 
		+ @Schema + '.APCEncounter 
	where 
		not exists 
		(
			select 
				1 
			from 
				Map.Encounter 
			where 
				SchemaID = ' + cast(@SchemaID as varchar) + '
			and	DatasetID = ' + cast(@APCDatasetID as varchar) + '
			and	Encounter.SourceEncounterRecno = APCEncounter.EncounterRecno
		)
	'

exec (@SQLInsertRecords)
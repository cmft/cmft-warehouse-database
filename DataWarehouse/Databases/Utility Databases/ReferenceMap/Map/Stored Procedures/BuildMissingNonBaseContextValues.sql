﻿CREATE procedure [Map].[BuildMissingNonBaseContextValues] as

--this code is now executed from within stored procedure [Map].[BuildMissingXrefs]

--insert
--into
--	Map.Value
--	(
--	 AttributeContextID
--	,ValueCode
--	,Value
--	,Created
--	,ByWhom
--	)
--select
--	 AttributeContext.AttributeContextID
--	,ValueCode = Context.ContextCode + '||' + Attribute.AttributeCode
--	,Value = 'Unassigned'
--	,Created = GETDATE()
--	,ByWhom = SUSER_NAME()
--from
--	Map.AttributeContext

--inner join Map.Context
--on	Context.ContextID = AttributeContext.ContextID
--and	Context.BaseContext = 0

--inner join Map.Attribute
--on	Attribute.AttributeID = AttributeContext.AttributeID

--where
--	not exists
--	(
--	select
--		1
--	from
--		Map.Value
--	where
--		Value.AttributeContextID = AttributeContext.AttributeContextID
--	and	Value.ValueCode = Context.ContextCode + '||' + Attribute.AttributeCode
--	)
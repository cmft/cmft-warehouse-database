﻿create procedure [Map].[LoadMAPAttributeContext] as

--insert new AttributeContexts
insert into MAP.AttributeContext
(
	 AttributeID
	,ContextID
)

select
	 AttributeID
	,ContextID
from
	(
	--source AttributeContexts
	select distinct
		 AttributeID
		,ContextID
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode

	inner join MAP.Context
	on	Context.ContextCode = TImport.ContextCode

	union

	--Local and National AttributeContexts
	select distinct
		 AttributeID
		,ContextID
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode

	inner join MAP.Context
	on	Context.ContextCode in ('L' , 'N')
	) NewAttributeContext

where
	not exists
		(
		select
			1
		from
			MAP.AttributeContext ExistAttributeContext
		where
			ExistAttributeContext.AttributeID = NewAttributeContext.AttributeID
		and	ExistAttributeContext.ContextID = NewAttributeContext.ContextID
		)
﻿CREATE PROCEDURE [Map].[InsertColmnBase]
	 @AttributeContextID INT
	,@ColumnType INT
	,@Column varchar(max)
	,@TableID INT
AS

INSERT INTO Map.ColumnBase
(
	 [AttributeContextID]
	,[ColumnTypeID]
	,[Column]
	,[TableID]
)
VALUES
(
	 @AttributeContextID 
	,@ColumnType 
	,@Column 
	,@TableID 
)
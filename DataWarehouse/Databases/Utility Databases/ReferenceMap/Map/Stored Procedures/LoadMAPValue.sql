﻿CREATE procedure [Map].[LoadMAPValue] as
--insert new Values
insert into MAP.Value
(
	 AttributeContextID
	,ValueCode
	,Value
)

select
	 NewAttribute.AttributeContextID
	,NewAttribute.ValueCode
	,NewAttribute.Value
from
	(
	--source
	select
		 AttributeContext.AttributeContextID
		,TImport.ValueCode
		,TImport.Value
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode

	inner join MAP.Context
	on	Context.ContextCode = TImport.ContextCode

	inner join MAP.AttributeContext
	on	AttributeContext.ContextID = Context.ContextID
	and	AttributeContext.AttributeID = Attribute.AttributeID

	union

	--local
	select distinct
		 AttributeContext.AttributeContextID
		,ValueCode = TImport.LocalValueCode
		,Value = TImport.LocalValue
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode

	inner join MAP.Context
	on	Context.ContextCode = 'L'

	inner join MAP.AttributeContext
	on	AttributeContext.ContextID = Context.ContextID
	and	AttributeContext.AttributeID = Attribute.AttributeID

	union

	--national
	select distinct
		 AttributeContext.AttributeContextID
		,ValueCode = TImport.NationalValueCode
		,Value =
			--TImport.NationalValueCode + ' - ' +   GS20120308  we don't want the code prefixed
				CASE 
				WHEN TImport.NationalValue = 'N/A' THEN 'No Description'
				ELSE COALESCE
						(
							 TImport.NationalValue
							,'No Description'
						)
				END
	from
		MAP.TImportMAPValueXref TImport

	inner join MAP.Attribute
	on	Attribute.AttributeCode = TImport.AttributeCode

	inner join MAP.Context
	on	Context.ContextCode = 'N'

	inner join MAP.AttributeContext
	on	AttributeContext.ContextID = Context.ContextID
	and	AttributeContext.AttributeID = Attribute.AttributeID
	) NewAttribute

where
	not exists
		(
		select
			1
		from
			MAP.Value ExistValue
		where
			ExistValue.ValueCode = NewAttribute.ValueCode
		and	ExistValue.AttributeContextID = NewAttribute.AttributeContextID
		)
﻿CREATE procedure [Map].[BuildAEEncounter]
(
	@SchemaID int
)

as

declare @AEDatasetID int = (select DatasetID from Map.DatasetBase where DatasetCode = 'AE')
declare @Schema varchar(5) = (select [Schema] from Map.SchemaBase where [SchemaID] = @SchemaID)


--remove orphaned records
Declare @SQLDeleteOrphanedRecords VarChar(1000)

select @SQLDeleteOrphanedRecords = '
	delete 
	from 
		Map.Encounter 
	where 
		Encounter.SchemaID = ' + cast(@SchemaID as varchar) + '
	and	Encounter.DatasetID = ' + cast(@AEDatasetID as varchar) + '
	and	not exists 
		(
		select 
			1 
		from ' +
			@Schema + '.AEEncounter with (nolock) 
		where 
			AEEncounter.EncounterRecno = Encounter.SourceEncounterRecno
		)
	'

exec (@SQLDeleteOrphanedRecords)


----inserts
Declare @SQLInsertRecords VarChar(1000)

select @SQLInsertRecords = '
	insert into Map.Encounter
	(
			SchemaID 
		,DatasetID 
		,SourceEncounterRecno
	) 
	select 
			SchemaID = ' + cast(@SchemaID as varchar) + '
		,DatasetID = ' + cast(@AEDatasetID as varchar) + '
		,SourceEncounterRecno = EncounterRecno 
	from ' 
		+ @Schema + '.AEEncounter 
	where 
		not exists 
		(
			select 
				1 
			from 
				Map.Encounter 
			where 
				SchemaID = ' + cast(@SchemaID as varchar) + '
			and	DatasetID = ' + cast(@AEDatasetID as varchar) + '
			and	Encounter.SourceEncounterRecno = AEEncounter.EncounterRecno
		)
	'

exec (@SQLInsertRecords)
﻿CREATE PROCEDURE [Map].[InsertAttributeContext]
	 @AttributeID INT
	,@ContextID INT
AS

INSERT INTO Map.AttributeContext
(
	 AttributeID
	,ContextID
)
VALUES
(
	 @AttributeID 
	,@ContextID 
)
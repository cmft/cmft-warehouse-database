﻿CREATE procedure [Map].[BuildRFEncounter]

(
       @SchemaID int
)

as


declare @RFDatasetID int = (select DatasetID from Map.DatasetBase where DatasetCode = 'RF')
declare @Schema varchar(5) = (select [Schema] from Map.SchemaBase where [SchemaID] = @SchemaID)


--remove orphaned records

Declare @SQLDeleteOrphanedRecords VarChar(1000)

select @SQLDeleteOrphanedRecords = '
       delete 
       from 
              Map.Encounter 
       where 
              Encounter.SchemaID = ' + cast(@SchemaID as varchar) + '
       and    Encounter.DatasetID = ' + cast(@RFDatasetID as varchar) + '
       and    not exists 
              (
              select 
                     1 
              from ' +
                     @Schema + '.RFEncounter with (nolock) 
              where 
                     RFEncounter.EncounterRecno = Encounter.SourceEncounterRecno
              )
       '

exec (@SQLDeleteOrphanedRecords)



----inserts
Declare @SQLInsertRecords VarChar(1000)

select @SQLInsertRecords = '
       insert into Map.Encounter
       (
              SchemaID 
              ,DatasetID 
              ,SourceEncounterRecno
       ) 
       select 
               SchemaID = ' + cast(@SchemaID as varchar) + '
              ,DatasetID = ' + cast(@RFDatasetID as varchar) + '
              ,SourceEncounterRecno = EncounterRecno 
       from ' 
              + @Schema + '.RFEncounter 
       where 
              not exists 
              (
                     select 
                           1 
                     from 
                           Map.Encounter 
                     where 
                           SchemaID = ' + cast(@SchemaID as varchar) + '
                     and    DatasetID = ' + cast(@RFDatasetID as varchar) + '
                     and    Encounter.SourceEncounterRecno = RFEncounter.EncounterRecno
              )
       '

exec (@SQLInsertRecords)
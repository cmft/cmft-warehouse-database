﻿CREATE procedure [Map].[BuildAPCWaitingList] as

declare @CentralSchemaID int = (select SchemaID from Map.SchemaBase where [Schema] = 'Cen')
declare @TraffordSchemaID int = (select SchemaID from Map.SchemaBase where [Schema] = 'Tra')
declare @APCWLDatasetID int = (select DatasetID from Map.DatasetBase where DatasetCode = 'APCWL')

--remove orphaned records
--Central
delete
from
	Map.Encounter
where
	not exists
	(
	select
		1
	from
		Cen.APCWaitingList with (nolock)
	where
		APCWaitingList.EncounterRecno = Encounter.SourceEncounterRecno
	)

and	Encounter.SchemaID = @CentralSchemaID
and	Encounter.DatasetID = @APCWLDatasetID


--Trafford
delete
from 
	Map.Encounter
where
	not exists
	(
	select
		1
	from
		Tra.APCWaitingList with (nolock)
	where
		 APCWaitingList.EncounterRecno = Encounter.SourceEncounterRecno
	)

and	Encounter.SchemaID = @TraffordSchemaID
and	Encounter.DatasetID = @APCWLDatasetID


--inserts
--Central
insert into Map.Encounter
(
	 SchemaID
	,DatasetID
	,SourceEncounterRecno
)

select
	 SchemaID = @CentralSchemaID
	,DatasetID = @APCWLDatasetID
	,SourceEncounterRecno = EncounterRecno
from
	Cen.APCWaitingList
where
	not exists
		(
		select
			1
		from
			Map.Encounter
		where
			SchemaID = @CentralSchemaID
		and	DatasetID = @APCWLDatasetID
		and	Encounter.SourceEncounterRecno = APCWaitingList.EncounterRecno
		)


--Trafford
insert into Map.Encounter
(
	 SchemaID
	,DatasetID
	,SourceEncounterRecno
)

select
	 SchemaID = @TraffordSchemaID
	,DatasetID = @APCWLDatasetID
	,SourceEncounterRecno = EncounterRecno
from
	Tra.APCWaitingList
where
	not exists
		(
		select
			1
		from
			Map.Encounter
		where
			SchemaID = @TraffordSchemaID
		and	DatasetID = @APCWLDatasetID
		and	Encounter.SourceEncounterRecno = APCWaitingList.EncounterRecno
		)
﻿CREATE PROCEDURE [Map].[UpdateColmnBase]
	 @columnID INT
	,@ColumnTypeID INT
	,@Column varchar(max)
	,@TableID INT
AS

UPDATE Map.ColumnBase
SET 
	 ColumnTypeID = @ColumnTypeID
	,[Column] = @Column
	,TableID = @TableID
WHERE ColumnID = @columnID
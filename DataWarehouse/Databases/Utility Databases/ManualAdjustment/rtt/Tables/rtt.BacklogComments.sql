﻿CREATE TABLE [rtt].[BacklogComments] (
    [id]          INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo]  NVARCHAR (50)  NOT NULL,
    [EpisodeNo]   NVARCHAR (50)  NOT NULL,
    [Comment]     NVARCHAR (MAX) NULL,
    [CreatedDate] DATETIME       DEFAULT (getdate()) NULL,
    [CreatedBy]   NVARCHAR (200) NULL,
    [PComment]    NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


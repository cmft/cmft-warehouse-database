﻿CREATE TABLE [rtt].[AdjustTreatType] (
    [id]              INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo]      NVARCHAR (150) NULL,
    [EpisodeNo]       NVARCHAR (10)  NULL,
    [IP_NON_IP]       NVARCHAR (10)  NULL,
    [Email_date_time] DATETIME       NULL,
    [CreatedBy]       NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


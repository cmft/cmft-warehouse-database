﻿CREATE TABLE [rtt].[Period] (
    [id]                  INT           NOT NULL,
    [STL_A_period_start]  DATETIME      NULL,
    [STL_A_period_end]    DATETIME      NULL,
    [STL_NA_period_start] DATETIME      NULL,
    [STL_NA_period_end]   DATETIME      NULL,
    [Monitor_month_start] DATETIME      NULL,
    [Monitor_month_end]   DATETIME      NULL,
    [Month]               NVARCHAR (50) NULL,
    [Monthyr]             NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


﻿CREATE TABLE [rtt].[Adjustments] (
    [id]             INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo]     NVARCHAR (50)  NOT NULL,
    [EpisodeNo]      NVARCHAR (50)  NOT NULL,
    [PrevMonthStart] NVARCHAR (1)   NULL,
    [NotPathway]     BIT            DEFAULT ((0)) NOT NULL,
    [DaysToRemove]   INT            NULL,
    [Comment]        NVARCHAR (255) NULL,
    [EndDate]        DATETIME       NULL,
    [Pause]          BIT            DEFAULT ((0)) NOT NULL,
    [PauseStartDate] DATETIME       NULL,
    [PauseEndDate]   DATETIME       NULL,
    [UpdatedDate]    DATETIME       NULL,
    [CreatedDate]    DATETIME       DEFAULT (getdate()) NULL,
    [CreatedBy]      NVARCHAR (200) DEFAULT (suser_sname()) NULL,
    [ToClose]        NVARCHAR (1)   DEFAULT ('C') NULL,
    [Admitted]       NVARCHAR (6)   NULL,
    [Division]       NVARCHAR (100) NULL,
    [Actioned]       BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_Adj]
    ON [rtt].[Adjustments]([InternalNo] ASC, [EpisodeNo] ASC)
    INCLUDE([id], [NotPathway], [Comment], [EndDate], [Pause], [PauseStartDate], [PauseEndDate], [ToClose], [Admitted], [Division], [CreatedBy], [Actioned]);


GO
CREATE NONCLUSTERED INDEX [Idx_Adj2]
    ON [rtt].[Adjustments]([InternalNo] ASC, [EpisodeNo] ASC, [CreatedDate] ASC)
    INCLUDE([id], [NotPathway], [Comment], [EndDate], [Pause], [PauseStartDate], [PauseEndDate], [ToClose], [Admitted], [Division], [CreatedBy], [Actioned]);


GO

CREATE TRIGGER [rtt].[trigAdjustments_Delete] ON [rtt].[Adjustments] 
For Delete
AS
		insert into rtt.AdjDeleted
select *
from deleted Where Actioned = 'True'


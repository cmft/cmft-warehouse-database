﻿CREATE TABLE [rtt].[not pw moa18-ward84 pathways] (
    [id]         INT           IDENTITY (1, 1) NOT NULL,
    [MethAdm]    NVARCHAR (2)  NULL,
    [Ward]       NVARCHAR (4)  NULL,
    [AdmitDate]  DATETIME      NULL,
    [Hospital]   NVARCHAR (4)  NULL,
    [InternalNo] NVARCHAR (9)  NULL,
    [EpisodeNo]  NVARCHAR (9)  NULL,
    [Consultant] NVARCHAR (8)  NULL,
    [Specialty]  NVARCHAR (8)  NULL,
    [CaseNoteNo] NVARCHAR (14) NULL,
    [DistrictNo] NVARCHAR (16) NULL,
    [DoHCode]    NVARCHAR (4)  NULL,
    [Surname]    NVARCHAR (60) NULL,
    [Forename]   NVARCHAR (30) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


﻿CREATE TABLE [rtt].[Division] (
    [id]       INT        IDENTITY (1, 1) NOT NULL,
    [Division] NCHAR (50) NULL,
    CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED ([id] ASC)
);


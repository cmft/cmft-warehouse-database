﻿CREATE TABLE [rtt].[DuplicateAdjustments] (
    [id]          INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo]  NVARCHAR (50)  NOT NULL,
    [EpisodeNo]   NVARCHAR (50)  NOT NULL,
    [LEpisodeNo]  NVARCHAR (50)  NOT NULL,
    [CreatedDate] DATETIME       NULL,
    [CreatedBy]   NVARCHAR (200) NULL,
    [Comments]    NVARCHAR (255) NULL,
    [Actioned]    BIT            NOT NULL,
    [UpdatedDate] DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


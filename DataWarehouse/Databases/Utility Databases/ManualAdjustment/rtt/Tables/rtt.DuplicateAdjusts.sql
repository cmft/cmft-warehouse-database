﻿CREATE TABLE [rtt].[DuplicateAdjusts] (
    [id]          INT            IDENTITY (1, 1) NOT NULL,
    [InternalNo]  NVARCHAR (50)  NOT NULL,
    [EpisodeNo]   NVARCHAR (50)  NOT NULL,
    [WL_WLDate]   DATETIME       NULL,
    [WL_status]   NVARCHAR (10)  NULL,
    [LEpisodeNo]  NVARCHAR (50)  NOT NULL,
    [LWL_WLDate]  DATETIME       NULL,
    [LWL_status]  NVARCHAR (10)  NULL,
    [CreatedDate] DATETIME       NULL,
    [CreatedBy]   NVARCHAR (200) NULL,
    [Comments]    NVARCHAR (255) NULL,
    [Actioned]    BIT            NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


﻿CREATE TABLE [rtt].[AdjDeleted] (
    [id]             INT            NULL,
    [InternalNo]     NVARCHAR (50)  NULL,
    [EpisodeNo]      NVARCHAR (50)  NULL,
    [PrevMonthStart] NVARCHAR (1)   NULL,
    [NotPathway]     BIT            NULL,
    [DaysToRemove]   INT            NULL,
    [Comment]        NVARCHAR (255) NULL,
    [EndDate]        DATETIME       NULL,
    [Pause]          BIT            NULL,
    [PauseStartDate] DATETIME       NULL,
    [PauseEndDate]   DATETIME       NULL,
    [UpdatedDate]    DATETIME       NULL,
    [CreatedDate]    DATETIME       NULL,
    [CreatedBy]      NVARCHAR (200) NULL,
    [ToClose]        NVARCHAR (1)   NULL,
    [Admitted]       NVARCHAR (6)   NULL,
    [Division]       NVARCHAR (100) NULL,
    [Actioned]       BIT            NULL
);


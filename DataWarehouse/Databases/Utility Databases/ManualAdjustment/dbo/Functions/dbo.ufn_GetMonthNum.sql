﻿CREATE FUNCTION [dbo].[ufn_GetMonthNum] (@pInputDate DATETIME )
RETURNS int
BEGIN

Declare @intY integer, @intM as integer

     set @intY = Convert(integer,(Year(dbo.ufn_GetFirstDayOfMonth(@pInputDate)) - 1990)) * 100
     set @intM = Convert(integer,(Month(@pInputDate))) 
        
       
       
If @intM < 4 
  Begin
  set @intM = (@intM + 9)
  End     
Else 
Begin   
  set @intM = (@intM - 3)
End    

Return @intY + @intM
   
   End     
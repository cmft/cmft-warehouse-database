﻿
-- =============================================
-- Author:		<Alan Bruce>
-- Create date: <11-Aug-2011>
-- Description:	<Return Last Sundays Date>
-- =============================================
CREATE FUNCTION [dbo].[udf_PrevSunday]
(
	@Today datetime
)
RETURNS Datetime
AS
BEGIN
Declare @PrevSunday Datetime

	set @PrevSunday = DATEADD(day,
               -1 - (DATEPART(dw, @Today) + @@DATEFIRST - 2) % 7,
               @Today
       )  
       --Ensure no time
set @PrevSunday = DATEADD(day, 0, DATEDIFF(day, 0, @PrevSunday))


Return @PrevSunday

END


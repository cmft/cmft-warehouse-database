﻿



CREATE PROCEDURE [dbo].[usp_SearchAdjustedPathways] (
@InternalNo nvarchar(50))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



SELECT     id, InternalNo, EpisodeNo, NotPathway, Comment, EndDate, [Pause], PauseStartDate, PauseEndDate,  ToClose, Admitted, Division, CreatedBy, Actioned
FROM         rtt.Adjustments
WHERE     InternalNo = @InternalNo 





﻿CREATE PROCEDURE [dbo].[usp_Insert_Open_Closed] (
@InternalNo varchar(50), @EpisodeNo varchar(50), 
 @Comment varchar(255), @EndDate datetime, @ToClose varchar (1), @CreatedBy varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ReportDate datetime
DECLARE @PrevMonthStart varchar(1)

Select @ReportDate = (SELECT Monitor_month_start
FROM         rtt.PeriodUpdate
WHERE     (id = 1))

--Set PrevMonthStart
IF @EndDate < @ReportDate
BEGIN
    set @PrevMonthStart = 'Y'
END
ELSE
	set @PrevMonthStart = ''
	
IF @ToClose = 'O'
BEGIN
    set @PrevMonthStart = 'O'
    set @EndDate = null
END


INSERT INTO rtt.Adjustments VALUES(@InternalNo, @EpisodeNo, @PrevMonthStart,0 ,Null,
@Comment, @EndDate, 0,Null,Null,GetDate(),Getdate(),@CreatedBy,@ToClose,Null,Null,0)



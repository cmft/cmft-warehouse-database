﻿



CREATE PROCEDURE [dbo].[usp_IPMain] (
@User varchar(200),@Spec varchar(100))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF @Spec != 'ALL' 
Begin
SELECT     wl.IPMain.Record, wl.IPMain.DistrictNo, wl.IPMain.Casenote, wl.IPMain.Forenames, wl.IPMain.Surname, wl.IPMain.CONS,
                      wl.IPMain.ResetDate, wl.IPMain.TCI,  wl.IPMain.ChangeRequired,  
                      wl.IPMain.Updated, wl.IPMain.Speciality, wl.IPMain.INTPROC, wl.IPMain.ELECDATE, 
                      wl.IPMain.EFWEEKS, wl.IPMain.NewWLREPCAT, wl.IPMain.NewEFWEEKS
FROM         wl.IPMain INNER JOIN
                      wl.NamesList ON wl.IPMain.Dirname = wl.NamesList.Directorate AND wl.IPMain.Speciality = wl.NamesList.Specialty
WHERE     (wl.NamesList.Contact = @User) AND (wl.NamesList.Report = N'Exceptions IP')
AND wl.IPMain.Speciality = @Spec
End
Else
SELECT     wl.IPMain.Record, wl.IPMain.DistrictNo, wl.IPMain.Casenote, wl.IPMain.Forenames, wl.IPMain.Surname, wl.IPMain.CONS,
                      wl.IPMain.ResetDate, wl.IPMain.TCI,  wl.IPMain.ChangeRequired,  
                      wl.IPMain.Updated, wl.IPMain.Speciality, wl.IPMain.INTPROC, wl.IPMain.ELECDATE, 
                      wl.IPMain.EFWEEKS, wl.IPMain.NewWLREPCAT, wl.IPMain.NewEFWEEKS
FROM         wl.IPMain INNER JOIN
                      wl.NamesList ON wl.IPMain.Dirname = wl.NamesList.Directorate AND wl.IPMain.Speciality = wl.NamesList.Specialty
WHERE     (wl.NamesList.Contact = @User) AND (wl.NamesList.Report = N'Exceptions IP')



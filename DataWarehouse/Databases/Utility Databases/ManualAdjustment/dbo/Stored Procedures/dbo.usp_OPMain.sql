﻿



CREATE PROCEDURE [dbo].[usp_OPMain] (
@User varchar(200),@Spec varchar(100))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF @Spec != 'ALL' 
Begin
SELECT     wl.OPMain.Record, wl.OPMain.DistrictNo, wl.OPMain.Casenote, wl.OPMain.Forenames, wl.OPMain.Surname, wl.OPMain.Cons, wl.OPMain.refdate, 
                      wl.OPMain.efrefdt, wl.OPMain.ResetDate, wl.OPMain.TCI, wl.OPMain.effweeks, wl.OPMain.ChangeRequired, wl.OPMain.NewOPWLREPCAT, 
                      wl.OPMain.NewEFFWEEKS, wl.OPMain.Updated, wl.OPMain.Speciality
FROM         wl.OPMain INNER JOIN
                      wl.NamesList ON wl.OPMain.Dirname = wl.NamesList.Directorate AND wl.OPMain.Speciality = wl.NamesList.Specialty
WHERE     (wl.NamesList.Contact = @User) AND (wl.NamesList.Report = N'Exceptions OP')
AND wl.OPMain.Speciality = @Spec
End
Else
SELECT     wl.OPMain.Record, wl.OPMain.DistrictNo, wl.OPMain.Casenote, wl.OPMain.Forenames, wl.OPMain.Surname, wl.OPMain.Cons, wl.OPMain.refdate, 
                      wl.OPMain.efrefdt, wl.OPMain.ResetDate, wl.OPMain.TCI, wl.OPMain.effweeks, wl.OPMain.ChangeRequired, wl.OPMain.NewOPWLREPCAT, 
                      wl.OPMain.NewEFFWEEKS, wl.OPMain.Updated, wl.OPMain.Speciality
FROM         wl.OPMain INNER JOIN
                      wl.NamesList ON wl.OPMain.Dirname = wl.NamesList.Directorate AND wl.OPMain.Speciality = wl.NamesList.Specialty
WHERE     (wl.NamesList.Contact = @User) AND (wl.NamesList.Report = N'Exceptions OP')



﻿-- Batch submitted through debugger: SQLQuery2.sql|7|0|C:\Documents and Settings\alan.bruce\Local Settings\Temp\~vs13.sql
-- =============================================
-- Author:		<Author,,Alan Bruce>
-- Create date: <Create Date,,24/5.2011>
-- Description:	<Description,,To Update Adjustment Table >
-- =============================================
CREATE PROCEDURE [dbo].[usp_Adjustment_Update] (@CreatedDate datetime)
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ReportDate datetime

Select @ReportDate = (SELECT Monitor_month_start
FROM         rtt.PeriodUpdate
WHERE     (id = 1))

		UPDATE rtt.Adjustments
		Set PrevMonthStart = 'Y'
		Where (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Actioned = 'True') AND (Adjustments.EndDate < @ReportDate)
	 
	
		UPDATE rtt.Adjustments
		Set DaysToRemove = DATEDIFF(day, PauseStartDate, PauseEndDate)
		Where CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate AND Actioned = 'True'
		
		
		UPDATE rtt.Adjustments
		Set PrevMonthStart = 'O'
		Where (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Actioned = 'True') AND (Adjustments.ToClose = 'O')
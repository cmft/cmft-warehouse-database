﻿
CREATE PROCEDURE [dbo].[usp_Insert_BacklogComments] (
@InternalNo varchar(50), @EpisodeNo varchar(50), 
 @Comment varchar(Max), @CreatedBy varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Declare @PComment nvarchar(255)


Set @PComment = (Select Comment from rtt.BacklogComments Where InternalNo = @InternalNo AND EpisodeNo = @EpisodeNo)
        
--Delete existing Comments
Delete From rtt.BacklogComments Where InternalNo = @InternalNo AND EpisodeNo = @EpisodeNo


INSERT INTO rtt.BacklogComments (internalNo, EpisodeNo, Comment,CreatedBy,PComment) VALUES(@InternalNo, @EpisodeNo, 
@Comment, @CreatedBy,@PComment)




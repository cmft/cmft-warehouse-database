﻿



CREATE PROCEDURE [dbo].[usp_AdjustedPathways] (
@CreatedDate DateTime, @CreatedBy varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



SELECT     id, InternalNo, EpisodeNo, NotPathway, Comment, EndDate, [Pause], PauseStartDate, PauseEndDate,  ToClose, Admitted, Division, CreatedDate, CreatedBy, Actioned
FROM         rtt.Adjustments
WHERE     (CreatedBy = @Createdby) AND (CONVERT(DateTime, CONVERT(Char(10), CreatedDate, 101)) Between @CreatedDate
AND DATEADD(MONTH,1,@CreatedDate)) 
Order By CreatedDate Asc





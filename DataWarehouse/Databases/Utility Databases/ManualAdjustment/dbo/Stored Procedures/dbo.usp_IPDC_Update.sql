﻿
-- =============================================
-- Author:		<Author,,Alan Bruce>
-- Create date: <Create Date,27/7/2011>
-- Description:	<Description,,To Update IPDiag Table >
-- =============================================
CREATE PROCEDURE [dbo].[usp_IPDC_Update] (
@DistrictNo nvarchar(100),
@Casenote nvarchar(100),
@Forenames nvarchar(100),
@Surname nvarchar(100),
@Specialty nvarchar(100),
@CONS nvarchar(100),
@Test nvarchar(100),@INTPROC nvarchar(100),
@ELECDATE Datetime,
@ResetDate Datetime, @TCI Datetime,
@EFWEEKS int,
@ChangeRequired nvarchar(100),
@NewWLREPCAT nvarchar(100),
@NewEFWEEKS int,
@Record int--, @Updated bit
  )
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Declare @OPWLCAT int, @DiagFlag varchar(100), @NewEfWks int, @LastSunday datetime

Set @OPWLCAT = CASE @ChangeRequired
  WHEN 'Remove From Report' THEN  '6'
  WHEN 'Planned Waiting List' THEN  '14'
  WHEN 'Not Diagnostic' THEN  '1'
  END;

Set @DiagFlag = CASE @ChangeRequired  
  WHEN 'Not Diagnostic' THEN  'Remove Diag_Flag & Diag_Line data'
  ELSE NULL
  END;

set @LastSunday = DATEADD(day,
               -1 - (DATEPART(dw, GETDATE()) + @@DATEFIRST - 2) % 7,
               GETDATE()
       )        

Set @NewEfWks = CASE @ChangeRequired
  WHEN 'Reset Date Column Amended' THEN    
  Datediff(WEEK,@ResetDate,@LastSunday)
  ELSE @EFWEEKS
  END;
  
  IF @NewEfWks <0
  BEGIN
  SET @NewEfWks = @EFWEEKS
  END


UPDATE [wl].[IPDiag] SET [DistrictNo] = @DistrictNo, [Casenote] = @Casenote,
 [Forenames] = @Forenames, [Surname] = @Surname,  
 [Specialty] = @Specialty, [CONS] = @CONS, 
  [Test] = @Test, [INTPROC] = @INTPROC, [ELECDATE] = @ELECDATE,
  [ResetDate] = @ResetDate, [TCI] = @TCI, [EFWEEKS] = @EFWEEKS, 
    [ChangeRequired] = @ChangeRequired, [NewWLREPCAT] = @OPWLCAT,
    [NewEFWEEKS] = @NewEfWks, [DiagFlagLineData] = @DiagFlag, [Updated] = 'True' WHERE [Record] = @Record		
		

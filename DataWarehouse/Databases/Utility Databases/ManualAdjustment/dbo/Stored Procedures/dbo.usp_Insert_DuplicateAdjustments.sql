﻿

CREATE PROCEDURE [dbo].[usp_Insert_DuplicateAdjustments] (
@InternalNo varchar(50), @EpisodeNo varchar(50), @LEpisodeNo varchar(50),
 @Comments varchar(255), @CreatedBy varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



INSERT INTO rtt.DuplicateAdjustments VALUES(@InternalNo, @EpisodeNo, @LEpisodeNo,GetDate(),
@CreatedBy,@Comments,0,Null)




﻿



Create PROCEDURE [dbo].[usp_IPMainSpec] (
@User varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select 'ALL' AS Specialty
Union
SELECT     Specialty
FROM         wl.NamesList
WHERE     (Contact = @User) AND (Report = N'Exceptions IP')



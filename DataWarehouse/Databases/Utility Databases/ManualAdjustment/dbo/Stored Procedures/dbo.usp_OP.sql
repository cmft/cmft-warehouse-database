﻿


CREATE PROCEDURE [dbo].[usp_OP] (
@User varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT     wl.OPDiag.Record, wl.OPDiag.DistrictNo, wl.OPDiag.Casenote, wl.OPDiag.Forenames, wl.OPDiag.Surname, wl.OPDiag.Cons, wl.OPDiag.refdate, 
                      wl.OPDiag.efrefdt, wl.OPDiag.ResetDate, wl.OPDiag.TCI, wl.OPDiag.effweeks, wl.OPDiag.ChangeRequired, wl.OPDiag.NewOPWLREPCAT, 
                      wl.OPDiag.NewEFFWEEKS, wl.OPDiag.DiagFlagLineData, wl.OPDiag.Updated, wl.OPDiag.Speciality
FROM         wl.OPDiag INNER JOIN
                      wl.NamesList ON wl.OPDiag.Dirname = wl.NamesList.Directorate AND wl.OPDiag.Speciality = wl.NamesList.Specialty
WHERE     (wl.NamesList.Contact = @User) AND (wl.NamesList.Report = N'Diagnostics OP')


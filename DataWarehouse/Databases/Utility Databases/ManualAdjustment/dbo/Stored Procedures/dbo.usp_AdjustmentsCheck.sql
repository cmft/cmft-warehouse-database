﻿CREATE Procedure [dbo].[usp_AdjustmentsCheck]
@sendEmailOnFailure bit = 0
AS
Begin

declare @countOfAdj int, @CreatedDate datetime;

--Look at yesterdays data only
Select @CreatedDate = CONVERT(date,dateadd(day,-1,Getdate()))


SELECT     @countOfAdj = COUNT(*) 
FROM         rtt.Adjustments LEFT OUTER JOIN
                      rtt.adjust ON rtt.Adjustments.EpisodeNo = rtt.adjust.[Episode No] AND rtt.Adjustments.InternalNo = rtt.adjust.[Internal No]
WHERE     (rtt.adjust.[Internal No] IS NULL) AND (rtt.adjust.[Episode No] IS NULL) AND (rtt.Adjustments.Admitted IS NULL) AND (rtt.Adjustments.Division IS NULL)
 AND (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate)
 
select @countOfAdj;

IF (@countOfAdj > 0) AND (@sendEmailOnFailure = 1) BEGIN
	--send email
	exec msdb.dbo.sp_send_dbmail @recipients = 'jabran.rafiq@cmft.nhs.uk;dusia.romaniuk@cmft.nhs.uk;', @subject='Manual Adjustments: Check Failed!', @importance='HIGH',@body=N'Check Manual Adjustments as the Check has failed...!';
	
	END;
End
﻿


-- =============================================
-- Author:		<Author,,Alan Bruce>
-- Create date: <Create Date,27/7/2011>
-- Description:	<Description,,To Update OPDiag Table >
-- =============================================

    
CREATE PROCEDURE [dbo].[usp_OPMain_Update] (
@DistrictNo nvarchar(100),
@Casenote nvarchar(100),
@Forenames nvarchar(100),
@Surname nvarchar(100),--@Dirname nvarchar (100),
@Speciality nvarchar(100),
@CONS nvarchar(100),
@refdate Datetime,
@ResetDate Datetime, @TCI Datetime,
@EFFWEEKS int,
@ChangeRequired nvarchar(100),
@NewOPWLREPCAT nvarchar(100),
@NewEFFWEEKS int,@efrefdt datetime,
@Record int, @Updated bit
  )
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Declare @OPWLCAT int,  @NewEfWks int, @LastSunday datetime

Set @OPWLCAT = CASE @ChangeRequired
  WHEN 'Remove From Report' THEN  '11'
  WHEN 'Planned Waiting List' THEN  '3' 
  END;


set @LastSunday = DATEADD(day,
               -1 - (DATEPART(dw, GETDATE()) + @@DATEFIRST - 2) % 7,
               GETDATE()
       )        

Set @NewEfWks = CASE @ChangeRequired
  WHEN 'Reset Date Column Amended' THEN    
  Datediff(WEEK,@ResetDate,@LastSunday)
  ELSE @EFFWEEKS
  END;
  
  IF @NewEfWks <0
  BEGIN
  SET @NewEfWks = @EFFWEEKS
  END


UPDATE [wl].[OPMain] SET [DistrictNo] = @DistrictNo, [Casenote] = @Casenote,
 [Forenames] = @Forenames, [Surname] = @Surname, --[Dirname] = @Dirname, 
[Speciality] = @Speciality, [Cons] = @Cons, [refdate] = @refdate, 
[efrefdt] = @efrefdt, [ResetDate] = @ResetDate, [TCI] = @TCI, 
[effweeks] = @effweeks, [ChangeRequired] = @ChangeRequired, 
[NewOPWLREPCAT] = @OPWLCAT, [NewEFFWEEKS] = @NewEfWks, 
[Updated] = 'True' WHERE [Record] = @Record		
		
		
		





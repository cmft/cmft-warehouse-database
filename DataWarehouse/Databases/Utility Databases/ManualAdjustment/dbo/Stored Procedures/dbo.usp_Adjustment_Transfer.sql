﻿CREATE PROCEDURE [dbo].[usp_Adjustment_Transfer] 
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @ReportDate datetime, @CreatedDate datetime

--Get the first day of the reporting month
Select @ReportDate = (SELECT Monitor_month_start
FROM         rtt.PeriodUpdate
WHERE     (id = 1))

--Look at yesterdays data only
Select @CreatedDate = CONVERT(date,dateadd(day,-1,Getdate()))
--Select @CreatedDate = CONVERT(date,Getdate())
--Ensure relevent fields are updated
-- STEP 1: Start the transaction
--BEGIN TRANSACTION

		UPDATE rtt.Adjustments
		Set PrevMonthStart = 'Y', Actioned = 'True'
		Where (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Actioned = 'False') AND (Adjustments.EndDate < @ReportDate)
	 
	
		UPDATE rtt.Adjustments
		Set DaysToRemove = DATEDIFF(day, PauseStartDate, PauseEndDate),  Actioned = 'True'
		Where CONVERT(date, Adjustments.CreatedDate) = @CreatedDate AND [Pause] = 'True'
		
		
		UPDATE rtt.Adjustments
		Set PrevMonthStart = 'O',  Actioned = 'True'
		Where (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Adjustments.ToClose = 'O')
		
		
		UPDATE rtt.Adjustments
		Set Actioned = 'True'
		Where (CONVERT(date, Adjustments.CreatedDate) = @CreatedDate) AND (Actioned = 'False') 
		AND (Adjustments.ToClose = 'Y') AND (ISDATE(Adjustments.EndDate) = 1)
		
		UPDATE rtt.Adjustments
		Set Actioned = 'True'
		Where (CONVERT(date, Adjustments.CreatedDate) = @CreatedDate) AND (Actioned = 'False') 
		AND (Adjustments.NotPathway = 'True') 

--Prevent Duplicates transfering
Update rtt.Adjustments Set Actioned = 'False'
Where InternalNo IN(
Select InternalNo From (
SELECT InternalNo, EpisodeNo
FROM rtt.Adjustments
Where (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Adjustments.ToClose ='Y' OR Adjustments.ToClose ='O' OR Adjustments.NotPathway = 'True')
GROUP BY InternalNo, EpisodeNo
HAVING count(*) > 1) As t1)

--Add first of duplicates
Update rtt.Adjustments Set Actioned = 'True'
Where id in(
(Select id
FROM (Select * From rtt.Adjustments Where (CONVERT(date, Adjustments.CreatedDate) = @CreatedDate) AND (Adjustments.ToClose ='Y' OR Adjustments.ToClose ='O' OR Adjustments.NotPathway = 'True') ) as Adj
LEFT OUTER JOIN (
   SELECT MIN(Id) as RowId, InternalNo,EpisodeNo
   FROM rtt.Adjustments Where (CONVERT(date, Adjustments.CreatedDate) = @CreatedDate)
   GROUP BY InternalNo,EpisodeNo
) as KeepRows ON
   adj.Id = KeepRows.RowId
WHERE
   KeepRows.RowId IS NULL))
   
 --*************************

-- Rollback the transaction if there were any errors
--IF @@ERROR <> 0
-- BEGIN
--    -- Rollback the transaction
--    ROLLBACK

--    -- Raise an error and return
--    RAISERROR ('Error in setting Actioned Flags.', 16, 1)
--    RETURN
-- END
		--Transfer data
		--Delete records existing
		
DELETE FROM rtt.adjust
FROM         rtt.Adjustments INNER JOIN
                      rtt.adjust ON rtt.Adjustments.InternalNo = rtt.adjust.[Internal No] AND rtt.Adjustments.EpisodeNo = rtt.adjust.[Episode No]
WHERE (CONVERT(date, rtt.Adjustments.CreatedDate)  = @CreatedDate) AND (rtt.Adjustments.Actioned = 'True')

-- Rollback the transaction if there were any errors
--IF @@ERROR <> 0
-- BEGIN
--    -- Rollback the transaction
--    ROLLBACK

--    -- Raise an error and return
--    RAISERROR ('Error in deleting rttAdjustments.', 16, 1)
--    RETURN
-- END

INSERT INTO rtt.adjust
                      ([Internal No], [Episode No], [prev month start], 
                      [Not18wks?], [days to remove], comment, enddate, [Pause?], Pause_start, [update date])
SELECT distinct    InternalNo, EpisodeNo, PrevMonthStart, NotPathway =
        CASE NotPathway
        WHEN 'True' THEN 'Y'
        ELSE  NULL
		END
, DaysToRemove, Comment, EndDate, [Pause]=CASE [Pause]
        WHEN 'True' THEN 'Y'
        ELSE  NULL
		END, PauseStartDate, convert(date,CreatedDate) CreatedDate
FROM         rtt.Adjustments
Where  (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Actioned = 'True') 
AND (Adjustments.Admitted is NULL or Adjustments.Admitted = '') 		
		
		UPDATE rtt.Adjustments
		Set Actioned = 'True'
		Where (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Actioned = 'False') AND (Adjustments.Admitted != '')


DELETE FROM rtt.AdjustTreatType
FROM         rtt.Adjustments INNER JOIN
                      rtt.AdjustTreatType ON rtt.Adjustments.InternalNo = rtt.AdjustTreatType.[InternalNo] AND rtt.Adjustments.EpisodeNo = rtt.AdjustTreatType.[EpisodeNo]
WHERE (CONVERT(date, rtt.Adjustments.CreatedDate)  = @CreatedDate) AND (rtt.Adjustments.Actioned = 'True') AND (Adjustments.Admitted != '')


INSERT INTO rtt.AdjustTreatType
                      (InternalNo, EpisodeNo, IP_NON_IP, Email_date_time, CreatedBy)
SELECT     InternalNo, EpisodeNo, Admitted, CreatedDate, CreatedBy
FROM         rtt.Adjustments
Where (CONVERT(date, Adjustments.CreatedDate)  = @CreatedDate) AND (Actioned = 'True') AND (Adjustments.Admitted != '')


-- Rollback the transaction if there were any errors
--IF @@ERROR <> 0
-- BEGIN
--    -- Rollback the transaction
--    ROLLBACK

--    -- Raise an error and return
--    RAISERROR ('Error in inserting Adjustments.', 16, 1)
--    RETURN
-- END
 
--COMMIT

EXECUTE [dbo].[usp_AdjustmentsCheck] 1

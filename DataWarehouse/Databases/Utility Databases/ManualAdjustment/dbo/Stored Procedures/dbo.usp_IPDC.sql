﻿

CREATE PROCEDURE [dbo].[usp_IPDC] (
@User varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT     wl.IPDiag.Record, wl.IPDiag.DistrictNo, wl.IPDiag.Casenote, wl.IPDiag.Forenames, wl.IPDiag.Surname, wl.IPDiag.Specialty, wl.IPDiag.CONS, 
                      wl.IPDiag.Test, wl.IPDiag.INTPROC, wl.IPDiag.ELECDATE, wl.IPDiag.ResetDate, wl.IPDiag.TCI, wl.IPDiag.EFWEEKS, wl.IPDiag.ChangeRequired, 
                      wl.IPDiag.Updated, wl.IPDiag.NewWLREPCAT, wl.IPDiag.NewEFWEEKS
FROM         wl.IPDiag INNER JOIN
                      wl.NamesList ON wl.IPDiag.Dirname = wl.NamesList.Directorate AND wl.IPDiag.Specialty = wl.NamesList.Specialty
WHERE     (wl.NamesList.Contact = @User) AND (wl.NamesList.Report = N'Diagnostics IP-DC')
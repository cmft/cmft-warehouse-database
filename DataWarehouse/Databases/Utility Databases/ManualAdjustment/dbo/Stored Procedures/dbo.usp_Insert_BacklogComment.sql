﻿
CREATE PROCEDURE [dbo].[usp_Insert_BacklogComment] (
@InternalNo varchar(50), @EpisodeNo varchar(50), 
 @Comment varchar(Max), @CreatedBy varchar(200), @PComment varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;




IF NOT Exists (Select Comment from rtt.BacklogComments Where InternalNo = @InternalNo AND EpisodeNo = @EpisodeNo)
        
Begin

INSERT INTO rtt.BacklogComments (internalNo, EpisodeNo, Comment,CreatedBy,PComment) VALUES(@InternalNo, @EpisodeNo, 
@Comment, @CreatedBy,@PComment)

END


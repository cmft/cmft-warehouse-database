﻿
CREATE PROCEDURE [dbo].[usp_Insert_Pause] (
@InternalNo varchar(50), @EpisodeNo varchar(50), 
 @Comment varchar(255), @PauseStartDate datetime, @PauseEndDate datetime, 
 @EndDate datetime = null, @CreatedBy varchar(200))
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @DaysToRemove int
DECLARE @ToClose varchar(1) = Null

If @EndDate IS NOT Null
Begin
set @ToClose = 'Y'
End


Select @DaysToRemove = DATEDIFF(day, @PauseStartDate, @PauseEndDate)		
		

INSERT INTO rtt.Adjustments VALUES(@InternalNo, @EpisodeNo, Null,0 ,@DaysToRemove,
@Comment, @EndDate, 'True',@PauseStartDate,@PauseEndDate,GetDate(),Getdate(),@CreatedBy,@ToClose,Null,Null,0)




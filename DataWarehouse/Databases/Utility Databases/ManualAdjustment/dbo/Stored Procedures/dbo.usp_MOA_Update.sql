﻿
-- =============================================
-- Author:		<Author,,Alan Bruce>
-- Create date: <Create Date,27/7/2011>
-- Description:	<Description,,To Update MOA 18 Table >
-- =============================================
CREATE PROCEDURE [dbo].[usp_MOA_Update] (
@id int,
	@Hospital varchar(10) ,
	@InternalNo varchar(50) ,
	@EpisodeNo varchar(50) ,
	@Consultant varchar(10) ,
	@Specialty varchar(10) ,
	@CaseNoteNo varchar(50) ,
	@DistrictNo varchar(50) ,
	@RTT_Pathway_ID varchar(255) ,
	@pathway_Start_Date_Current datetime ,
	@Pathway_status varchar(10) ,
	@pathway_end_date datetime ,
	@path_division varchar(100) ,
	@path_directorate varchar(100) ,
	@path_spec varchar(10) ,
	@path_doh_spec varchar(10) ,
	@path_doh_desc varchar(100) ,
	@path_closed_wks_DNA_adjs varchar(255) ,
	@path_closed_days_DNA_adjs varchar(255) ,
	@datebefore datetime ,
	@Surname varchar(60) ,
	@Forename varchar(30) ,
	@type varchar(6) ,
	@treattype varchar(255) ,
	@tci_fut_app_date datetime ,
	@Include varchar(1) 

  )
AS
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	

IF LEN(@RTT_Pathway_ID) >0
Begin
  Set @Include = CASE Left(@RTT_Pathway_ID,2)
  WHEN 'RW' THEN  'Y'   
  ELSE 'N'
  END  
End

   
    
UPDATE rtt.[new moa18-ward84 pathways] SET [Hospital] = @Hospital, 
[InternalNo] = @InternalNo, [EpisodeNo] = @EpisodeNo, 
[Consultant] = @Consultant, [Specialty] = @Specialty, 
[CaseNoteNo] = @CaseNoteNo, [DistrictNo] = @DistrictNo, 
[RTT_Pathway_ID] = @RTT_Pathway_ID, 
[pathway_Start_Date_Current] = @pathway_Start_Date_Current, 
[Pathway_status] = @Pathway_status, [pathway_end_date] = @pathway_end_date, 
[path_division] = @path_division, [path_directorate] = @path_directorate, 
[path_spec] = @path_spec, [path_doh_spec] = @path_doh_spec, 
[path_doh_desc] = @path_doh_desc, [path_closed_wks_DNA_adjs] = @path_closed_wks_DNA_adjs, 
[path_closed_days_DNA_adjs] = @path_closed_days_DNA_adjs, 
[datebefore] = @datebefore, [Surname] = @Surname, 
[Forename] = @Forename, [type] = @type, [treattype] = @treattype, 
[tci_fut_app_date] = @tci_fut_app_date, [Include] = @Include WHERE [id] = @id
		

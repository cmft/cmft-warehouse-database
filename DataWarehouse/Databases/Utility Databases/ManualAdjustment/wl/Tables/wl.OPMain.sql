﻿CREATE TABLE [wl].[OPMain] (
    [Record]         INT            NULL,
    [DistrictNo]     NVARCHAR (255) NULL,
    [Casenote]       NVARCHAR (255) NULL,
    [Forenames]      NVARCHAR (255) NULL,
    [Surname]        NVARCHAR (255) NULL,
    [PCTname]        NVARCHAR (255) NULL,
    [PCT]            NVARCHAR (255) NULL,
    [Dirname]        NVARCHAR (255) NULL,
    [Speciality]     NVARCHAR (255) NULL,
    [Cons]           NVARCHAR (255) NULL,
    [refdate]        DATETIME       NULL,
    [efrefdt]        DATETIME       NULL,
    [ResetDate]      DATETIME       NULL,
    [TCI]            DATETIME       NULL,
    [effweeks]       INT            NULL,
    [Urgent ]        NVARCHAR (255) NULL,
    [For]            NVARCHAR (100) NULL,
    [ChangeRequired] NVARCHAR (255) NULL,
    [NewOPWLREPCAT]  NVARCHAR (255) NULL,
    [NewEFFWEEKS]    INT            NULL,
    [Updated]        BIT            DEFAULT ((0)) NULL
);


﻿CREATE TABLE [wl].[NamesList] (
    [id]          INT            IDENTITY (1, 1) NOT NULL,
    [Directorate] NVARCHAR (255) NULL,
    [Specialty]   NVARCHAR (255) NULL,
    [Contact]     NVARCHAR (255) NULL,
    [Report]      NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


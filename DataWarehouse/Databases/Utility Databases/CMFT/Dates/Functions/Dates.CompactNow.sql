﻿CREATE function [Dates].[CompactNow]()
returns varchar(14)
as 
begin
return Dates.CompactDatetime(SYSDATETIME());
end;
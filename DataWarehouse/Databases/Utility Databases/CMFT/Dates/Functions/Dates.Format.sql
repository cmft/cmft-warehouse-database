﻿CREATE FUNCTION [Dates].[Format](@sourceDate [datetime], @formatString [nvarchar](4000))
RETURNS [nvarchar](4000) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[FormatDate]
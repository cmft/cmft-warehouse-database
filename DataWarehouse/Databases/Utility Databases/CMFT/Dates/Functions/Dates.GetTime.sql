﻿CREATE FUNCTION [Dates].[GetTime](@sourceDate [datetime])
RETURNS [nvarchar](4000) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[GetTime]
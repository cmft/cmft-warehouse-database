﻿CREATE function [Dates].[CompactNowUTC]()
returns varchar(14)
as 
begin
return Dates.CompactDatetime(SYSUTCDATETIME());
end;
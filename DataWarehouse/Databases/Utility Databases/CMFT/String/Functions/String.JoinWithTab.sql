﻿CREATE AGGREGATE [String].[JoinWithTab]
(@Value [nvarchar](max))
RETURNS[nvarchar](max)
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[JoinWithTab]
﻿CREATE FUNCTION [String].[LastIndexOf](@searchString [nvarchar](max), @stringToFind [nvarchar](4000))
RETURNS [int] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[LastIndexOf]
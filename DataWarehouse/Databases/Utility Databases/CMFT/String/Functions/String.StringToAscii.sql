﻿create function [String].[StringToAscii]
(@input varchar(450))
returns varchar(450)
as
begin
	declare @string varchar(100) = @input
	declare @output varchar(100) = ''
	declare @Position int = 1
	
	while @Position <= datalength(@string)
	begin
		set @output = @output + cast(ascii(substring(@string, @Position, 1)) as varchar(100))
		set @Position = @Position + 1
	end


	return @output
end
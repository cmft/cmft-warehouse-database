﻿CREATE AGGREGATE [String].[JoinWithPipe]
(@Value [nvarchar](max))
RETURNS[nvarchar](max)
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[JoinWithPipe]
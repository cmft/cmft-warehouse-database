﻿CREATE FUNCTION [String].[EndsWith](@sourceString [nvarchar](max), @stringToFind [nvarchar](4000))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[EndsWith]
﻿CREATE FUNCTION [String].[StartsWith](@sourceString [nvarchar](max), @stringToFind [nvarchar](4000))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [CMFT.SQLServer.StringFunctions].[UDF_StringAndDate].[StartsWith]
﻿CREATE view [Allocation].[ContractFlagByCommissionerClinic]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.SourceClinicCode = WrkAllocateDataset.SourceClinicCode
and	AllocationTemplateDataset.CommissionerCode = WrkAllocateDataset.CommissionerCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
﻿Create View [Allocation].[DischargeLetterExclusionByDivisionSourceSpecialtyNeonatalWardStay]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and AllocationTemplateDataset.Division = WrkAllocateDataset.DischargeDivisionCode
and AllocationTemplateDataset.NeonatalWardStay = WrkAllocateDataset.NeonatalWardStay
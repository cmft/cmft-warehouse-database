﻿CREATE View [Allocation].[DischargeLetterExclusionBySourceSpecialtyPatientCategory]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and WrkAllocateDataset.SourceSpecialtyCode = AllocationTemplateDataset.SourceSpecialtyCode
and WrkAllocateDataset.PatientCategoryCode = AllocationTemplateDataset.PatientCategoryCode
﻿CREATE view [Allocation].[ContractFlagByStartWard]

as

select
	 WrkAllocateDataset.DatasetRecno 
	,WrkAllocateDataset.DatasetCode 
	,AllocationID = AllocationTemplateDataset.AllocationID 
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.StartWardCode = WrkAllocateDataset.StartWardCode
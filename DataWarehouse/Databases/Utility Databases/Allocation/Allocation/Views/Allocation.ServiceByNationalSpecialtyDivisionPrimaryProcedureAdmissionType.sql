﻿CREATE view Allocation.ServiceByNationalSpecialtyDivisionPrimaryProcedureAdmissionType
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.Template = 'Allocation.ServiceByNationalSpecialtyDivisionPrimaryProcedureAdmissionType'
and	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.Division = WrkAllocateDataset.Division
and	AllocationTemplateDataset.AdmissionTypeCode = WrkAllocateDataset.AdmissionTypeCode
and	AllocationTemplateDataset.ProcedureCode = WrkAllocateDataset.ProcedureCode

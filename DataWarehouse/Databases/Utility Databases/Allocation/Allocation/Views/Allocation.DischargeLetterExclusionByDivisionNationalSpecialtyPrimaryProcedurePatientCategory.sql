﻿CREATE View [Allocation].[DischargeLetterExclusionByDivisionNationalSpecialtyPrimaryProcedurePatientCategory]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and AllocationTemplateDataset.Division = WrkAllocateDataset.DischargeDivisionCode
and AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and AllocationTemplateDataset.ProcedureCode = left(WrkAllocateDataset.ProcedureCode,3)
and AllocationTemplateDataset.PatientCategoryCode <> WrkAllocateDataset.PatientCategoryCode

and	WrkAllocateDataset.DatasetEndDate between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
		and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	)
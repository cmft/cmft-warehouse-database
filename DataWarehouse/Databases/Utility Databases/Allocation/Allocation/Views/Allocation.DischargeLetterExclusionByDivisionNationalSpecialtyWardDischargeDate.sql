﻿CREATE View [Allocation].[DischargeLetterExclusionByDivisionNationalSpecialtyWardDischargeDate]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and WrkAllocateDataset.DischargeDivisionCode = AllocationTemplateDataset.Division
and WrkAllocateDataset.NationalSpecialtyCode = AllocationTemplateDataset.NationalSpecialtyCode
and AllocationTemplateDataset.EndWardCode = left(WrkAllocateDataset.EndWardCode,3)
and WrkAllocateDataset.DischargeDate < AllocationTemplateDataset.DischargeDate	
	
and	WrkAllocateDataset.DatasetEndDate between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
		and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	)
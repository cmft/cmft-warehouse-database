﻿CREATE View [Allocation].[DischargeLetterExclusionByPrimaryProcedurePatientCategory]

as
	

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.LastEpisodeInSpellIndicator = 1
and WrkAllocateDataset.ProcedureCode = AllocationTemplateDataset.ProcedureCode
and WrkAllocateDataset.PatientCategoryCode = AllocationTemplateDataset.PatientCategoryCode
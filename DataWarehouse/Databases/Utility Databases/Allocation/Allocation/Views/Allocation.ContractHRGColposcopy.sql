﻿create view [Allocation].[ContractHRGColposcopy]

as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

inner join Allocation.Allocation
on	Allocation.Active = 1
and	Allocation.AllocationCode = 'COLP'
and	Allocation.AllocationTypeID =  6

where
	WrkAllocateDataset.DatasetCode = 'OP'
and	exists
		(
		select
			1
		from
			Allocation.WrkAllocateDatasetOperation
		where 
			WrkAllocateDataset.DatasetRecno = WrkAllocateDatasetOperation.DatasetRecno
		and WrkAllocateDatasetOperation.DatasetCode = 'OP'
		and WrkAllocateDatasetOperation.OperationCode = 'Q55.4'
		)
and	exists
		(
		select
			1
		from
			Allocation.WrkAllocateDatasetOperation
		where
			WrkAllocateDataset.DatasetRecno = WrkAllocateDatasetOperation.DatasetRecno
		and WrkAllocateDatasetOperation.DatasetCode = 'OP'
		and WrkAllocateDatasetOperation.OperationCode in
														(
														'Q01.4'
														,'Q02.2'
														)
		)





--whereDatasetRecno
--	exists
--	(
--	select
--		1
--	from
--		Allocation.WrkAllocateDatasetOperation
--	where
--		WrkAllocateDatasetOperation.DatasetRecno = WrkAllocateDataset.DatasetRecno
--	and	WrkAllocateDatasetOperation.DatasetCode = WrkAllocateDataset.DatasetCode
--	and	WrkAllocateDatasetOperation.OperationCode = AllocationTemplateDataset.ProcedureCode
--	)
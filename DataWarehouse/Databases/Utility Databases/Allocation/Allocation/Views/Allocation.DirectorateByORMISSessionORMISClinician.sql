﻿CREATE view [Allocation].[DirectorateByORMISSessionORMISClinician]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode


where
	AllocationTemplateDataset.SourceSiteCode = WrkAllocateDataset.SourceSiteCode
and	AllocationTemplateDataset.FirstEpisodeInSpellIndicator = WrkAllocateDataset.GlobalEpisodeNo
and	AllocationTemplateDataset.OperationDayOfWeek = WrkAllocateDataset.OperationDayOfWeek
and	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	AllocationTemplateDataset.AdmissionTypeCode = WrkAllocateDataset.AdmissionTypeCode
and	
	(
		AllocationTemplateDataset.TheatreSurgeonCode = WrkAllocateDataset.TheatreSurgeonCode
	or
		AllocationTemplateDataset.SourceConsultantCode = WrkAllocateDataset.TheatreConsultantCode
	)
and 
	(
		AllocationTemplateDataset.SessionPeriod = WrkAllocateDataset.SessionPeriod
	or
		AllocationTemplateDataset.SessionPeriod is null
	)
and	WrkAllocateDataset.ActivityDate between
		coalesce (AllocationTemplateDataset.FromDate, '1 Jan 1900')
	and	coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999')




	


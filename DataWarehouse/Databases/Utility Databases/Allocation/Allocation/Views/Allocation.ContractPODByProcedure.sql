﻿CREATE view [Allocation].[ContractPODByProcedure]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20141112	RR created.  Request by Phil Huitson, to allocate where procedure in any position
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	exists
	(
	select
		1
	from
		Allocation.WrkAllocateDatasetOperation
	where
		WrkAllocateDatasetOperation.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocateDatasetOperation.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocateDatasetOperation.OperationCode = AllocationTemplateDataset.ProcedureCode
	)
and 
	(WrkAllocateDataset.AdmissionTime < AllocationTemplateDataset.ToDate
	or AllocationTemplateDataset.ToDate is null
	)
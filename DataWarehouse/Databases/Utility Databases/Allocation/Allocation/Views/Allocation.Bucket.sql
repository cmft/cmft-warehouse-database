﻿CREATE view [Allocation].[Bucket]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
--this only works if it runs last. use the Template.Priority to determine this.
	not exists
	(
	select
		1
	from
		Allocation.WrkAllocation
	where
		WrkAllocation.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocation.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocation.AllocationTypeID = AllocationTemplateDataset.AllocationTypeID
	)

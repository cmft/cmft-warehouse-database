﻿CREATE view [Allocation].[ContractFlagByTreatmentFunctionReferralSpecialtyType]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	(
		AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
	or	AllocationTemplateDataset.NationalSpecialtyCode is null
	)
and	AllocationTemplateDataset.ReferralSpecialtyTypeCode = WrkAllocateDataset.ReferralSpecialtyTypeCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
﻿CREATE view [Allocation].[VTEByStartDirectorateStartWard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.GlobalEpisodeNo = 1
and
	WrkAllocateDataset.StartDirectorateCode = AllocationTemplateDataset.StartDirectorateCode
and	WrkAllocateDataset.AdmissionWardCode = AllocationTemplateDataset.StartWardCode
and 
	(WrkAllocateDataset.AdmissionTime < AllocationTemplateDataset.ToDate
	or AllocationTemplateDataset.ToDate is null
	)
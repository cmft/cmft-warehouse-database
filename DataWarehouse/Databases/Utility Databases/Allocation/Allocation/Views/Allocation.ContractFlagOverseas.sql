﻿CREATE view [Allocation].[ContractFlagOverseas]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,Priority = AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.ContractSerialNo in
	(
	 'X98'
	,'OSV00A'
	)

or	(
		WrkAllocateDataset.Postcode like 'ZZ99%'
	and WrkAllocateDataset.Postcode not in 
		(
		 'ZZ99 3WZ'
		,'ZZ99 3VZ'
		,'ZZ99 3CZ'
		)
	)
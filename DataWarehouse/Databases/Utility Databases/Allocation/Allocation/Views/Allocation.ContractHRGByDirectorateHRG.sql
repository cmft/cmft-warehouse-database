﻿CREATE view [Allocation].[ContractHRGByDirectorateHRG]
as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.DirectorateCode = WrkAllocateDataset.DirectorateCode
and	AllocationTemplateDataset.HRGCode = WrkAllocateDataset.HRGCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode


and	WrkAllocateDataset.DatasetStartDate
between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
		and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	)
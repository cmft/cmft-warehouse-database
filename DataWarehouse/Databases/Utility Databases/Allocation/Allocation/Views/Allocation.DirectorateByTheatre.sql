﻿
CREATE view [Allocation].[DirectorateByTheatre]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode


where
	WrkAllocateDataset.Theatre = AllocationTemplateDataset.Theatre

and	(WrkAllocateDataset.ActivityDate between
		AllocationTemplateDataset.FromDate and coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999')
	)


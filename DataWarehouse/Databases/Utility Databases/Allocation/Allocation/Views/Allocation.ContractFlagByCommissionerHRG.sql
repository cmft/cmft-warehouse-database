﻿CREATE view [Allocation].[ContractFlagByCommissionerHRG]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.HRGCode like AllocationTemplateDataset.HRGCode + '%'
and	AllocationTemplateDataset.CommissionerCode = WrkAllocateDataset.CommissionerCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
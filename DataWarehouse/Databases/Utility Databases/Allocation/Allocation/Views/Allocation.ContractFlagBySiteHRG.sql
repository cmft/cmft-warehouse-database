﻿CREATE view [Allocation].[ContractFlagBySiteHRG]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	WrkAllocateDataset.HRGCode like AllocationTemplateDataset.HRGCode + '%' 

and	(
		AllocationTemplateDataset.NationalSiteCode = WrkAllocateDataset.NationalSiteCode
	or	AllocationTemplateDataset.NationalSiteCode is null
	)

and	coalesce(WrkAllocateDataset.DatasetEndDate,WrkAllocateDataset.DatasetStartDate)
between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
		and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	)
﻿CREATE view [Allocation].[ContractFlagByContractSerialNo]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

and	WrkAllocateDataset.ContractSerialNo like

	case

	when left(AllocationTemplateDataset.ContractSerialNo, 1) = '%'
	then '%' + right(AllocationTemplateDataset.ContractSerialNo, LEN(AllocationTemplateDataset.ContractSerialNo) -1)

	when right(AllocationTemplateDataset.ContractSerialNo, 1) = '%'
	then left(AllocationTemplateDataset.ContractSerialNo, LEN(AllocationTemplateDataset.ContractSerialNo) -1) + '%'

	else AllocationTemplateDataset.ContractSerialNo
	end
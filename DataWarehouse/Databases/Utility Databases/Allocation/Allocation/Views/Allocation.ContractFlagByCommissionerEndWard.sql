﻿CREATE view [Allocation].[ContractFlagByCommissionerEndWard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.EndWardCode = WrkAllocateDataset.EndWardCode

and	(
		AllocationTemplateDataset.CommissionerCode = WrkAllocateDataset.CommissionerCode
	or	AllocationTemplateDataset.CommissionerCode is null
	)
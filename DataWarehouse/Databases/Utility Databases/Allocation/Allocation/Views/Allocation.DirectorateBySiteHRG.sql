﻿CREATE view [Allocation].[DirectorateBySiteHRG]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode


where
	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	AllocationTemplateDataset.SourceSiteCode = WrkAllocateDataset.SourceSiteCode
and WrkAllocateDataset.HRGCode like AllocationTemplateDataset.HRGCode + '%'

and	WrkAllocateDataset.ActivityDate between
	isnull(AllocationTemplateDataset.FromDate, WrkAllocateDataset.ActivityDate)
and	isnull(AllocationTemplateDataset.ToDate, WrkAllocateDataset.ActivityDate)


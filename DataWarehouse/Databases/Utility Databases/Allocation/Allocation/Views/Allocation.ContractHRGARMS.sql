﻿CREATE view [Allocation].[ContractHRGARMS]

as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = Allocation.AllocationID
	,Allocation.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

inner join Allocation.Allocation
on	Allocation.Active = 1
and	Allocation.AllocationCode = 'ARMS'
and	Allocation.AllocationTypeID =  6

where
	WrkAllocateDataset.DatasetCode = 'OP'
and WrkAllocateDataset.SourceClinicCode in ('KWARMS','ARMS')
and	exists
		(
		select
			1
		from
			Allocation.WrkAllocateDatasetOperation
		where 
			WrkAllocateDataset.DatasetRecno = WrkAllocateDatasetOperation.DatasetRecno
		and WrkAllocateDatasetOperation.DatasetCode = 'OP'
		and WrkAllocateDatasetOperation.OperationCode in 
														(
														 'H62.4'
														,'H46.3'
														,'A84.2'
														)
		)


﻿CREATE view [Allocation].[ContractFlagBySourceFirstAttendanceFlagNotSourceOfReferral]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.SourceSpecialtyCode = WrkAllocateDataset.SourceSpecialtyCode
and	AllocationTemplateDataset.SourceFirstAttendanceCode = WrkAllocateDataset.SourceFirstAttendanceCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and not exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.List = AllocationTemplateDataset.RuleBaseList1
	and	WrkAllocateDataset.SourceSourceOfReferralCode = RuleBaseList.ListValue
	)
and WrkAllocateDataset.DatasetStartDate between  
		coalesce(AllocationTemplateDataset.FromDate, '01 Jan 1900')
	and	
		coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999'	)
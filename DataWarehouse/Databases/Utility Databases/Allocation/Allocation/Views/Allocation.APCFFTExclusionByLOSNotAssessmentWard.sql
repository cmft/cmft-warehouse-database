﻿CREATE view [Allocation].[APCFFTExclusionByLOSNotAssessmentWard]
as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.LOS > WrkAllocateDataset.LOS
and
	WrkAllocateDataset.DischargeDate		
					between	
						AllocationTemplateDataset.FromDate
					and	coalesce(AllocationTemplateDataset.ToDate,'31 dec 9999')	
and not exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.List = AllocationTemplateDataset.RuleBaseList1
	and	WrkAllocateDataset.EndWardCode = RuleBaseList.ListValue
	)

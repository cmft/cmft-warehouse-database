﻿CREATE view [Allocation].[VTEByAnyOperationPatCat]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.GlobalEpisodeNo = 1
and
	exists
	(
	select
		1
	from
		Allocation.WrkAllocateDatasetOperation
	where
		WrkAllocateDatasetOperation.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocateDatasetOperation.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocateDatasetOperation.OperationCode = AllocationTemplateDataset.ProcedureCode
	)

and	WrkAllocateDataset.PatientCategoryCode = AllocationTemplateDataset.PatientCategoryCode
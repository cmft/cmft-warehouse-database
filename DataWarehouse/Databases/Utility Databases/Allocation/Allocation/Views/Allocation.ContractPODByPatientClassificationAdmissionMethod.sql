﻿CREATE view [Allocation].[ContractPODByPatientClassificationAdmissionMethod]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
-- 20141112	RR created.  Request by Phil Huitson, to allocate where procedure in any position
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.PatientClassificationCode = AllocationTemplateDataset.PatientClassificationCode
and (
		left(WrkAllocateDataset.NationalAdmissionMethodCode,1) = AllocationTemplateDataset.NationalAdmissionMethodCode
	or
		AllocationTemplateDataset.NationalAdmissionMethodCode is null
	)
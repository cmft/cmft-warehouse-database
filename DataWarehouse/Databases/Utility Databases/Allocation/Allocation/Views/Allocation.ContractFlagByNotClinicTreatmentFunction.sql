﻿CREATE view [Allocation].[ContractFlagByNotClinicTreatmentFunction]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	AllocationTemplateDataset.NationalSpecialtyCode = WrkAllocateDataset.NationalSpecialtyCode
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode

and not exists
	(
	select
		1
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.List = AllocationTemplateDataset.RuleBaseList1
	and	WrkAllocateDataset.SourceClinicCode like RuleBaseList.ListValue + '%'
	)
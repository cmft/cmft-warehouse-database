﻿CREATE view [Allocation].[ContractFlagPayableOverseas]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,Priority = AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	left(coalesce(WrkAllocateDataset.PseudoPostcode, WrkAllocateDataset.PostcodeAtDischarge), 4) = AllocationTemplateDataset.Postcode
and	right(coalesce(WrkAllocateDataset.PseudoPostcode, WrkAllocateDataset.PostcodeAtDischarge), 3)
	not in
	(
	select
		ListValue
	from
		Allocation.RuleBaseList
	where
		RuleBaseList.List = AllocationTemplateDataset.RuleBaseList1
	)

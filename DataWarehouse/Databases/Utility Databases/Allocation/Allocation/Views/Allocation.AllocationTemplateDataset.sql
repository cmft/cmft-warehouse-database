﻿CREATE View [Allocation].[AllocationTemplateDataset] WITH SCHEMABINDING
as

select
	 Priority =
		isnull(
			 Allocation.Priority
			,Template.Priority
		)

	,Allocation.Active
	,Allocation.AllocationTypeID
	,RuleBase.RuleBaseRecno
	,TemplateDataset.DatasetCode
	,RuleBase.SourceSpecialtyCode
	,RuleBase.NationalSpecialtyCode
	,RuleBase.SourceClinicCode
	,RuleBase.NationalSiteCode
	,RuleBase.AdmissionTypeCode
	,RuleBase.ProcedureCode
	,RuleBase.Template
	,RuleBase.OldServiceID
	,RuleBase.Division
	,RuleBase.SourceContextCode
	,RuleBase.SourceConsultantCode
	,RuleBase.SourceSourceOfReferralCode
	,RuleBase.NationalFirstAttendanceCode
	,RuleBase.DiagnosticNationalExamCode
	,RuleBase.SourceWaitingListCode
	,RuleBase.ConsultantNationalMainSpecialtyCode
	,RuleBase.AllocationID
	,RuleBase.DiagnosisCode
	,RuleBase.PatientCategoryCode
	,RuleBase.StartWardCode
	,RuleBase.EndWardCode
	,RuleBase.PatientClassificationCode
	,RuleBase.StartSiteCode
	,RuleBase.EndSiteCode
	,RuleBase.Duration
	,RuleBase.StartDirectorateCode
	,RuleBase.AdmissionMethodCode
	,RuleBase.OtherProcedureCode
	,RuleBase.SecondaryOperationCode1
	,RuleBase.SecondaryOperationCode2
	,RuleBase.SecondaryOperationCode3
	,RuleBase.Boolean
	,RuleBase.HRGCode
	,RuleBase.DirectorateCode
	,RuleBase.NationalAdministrativeCategoryCode
	,RuleBase.ReferralSpecialtyTypeCode
	,RuleBase.CommissionerCode
	,RuleBase.NationalSexCode
	,RuleBase.ContractSerialNo
	,RuleBase.NationalNeonatalLevelOfCareCode
	,RuleBase.FirstEpisodeInSpellIndicator
	,RuleBase.SourceIntendedManagementCode
	,RuleBase.SourceAppointmentTypeCode
	,RuleBase.WardCode
	,RuleBase.SourceSiteCode
	,RuleBase.NationalSourceOfReferralCode
	,RuleBase.FromDate
	,RuleBase.ToDate
	,RuleBase.SourceAppointmentType
	,RuleBase.RuleBaseList1
	,RuleBase.RuleBaseList2
	,RuleBase.RuleBaseList3
	,RuleBase.CancerSite
	,RuleBase.DepartmentCode
	,RuleBase.SourceAppointmentStatusCode
	,RuleBase.NationalAttendanceStatusCode
	,RuleBase.ProcedureCoded
	,RuleBase.DischargeMethodCode
	,RuleBase.DischargeDestinationCode
	,RuleBase.DischargeDate
	,RuleBase.NeonatalWardStay
	,RuleBase.DepartmentTypeCode
	,RuleBase.Age
	,RuleBase.NationalAttendanceDisposalCode
	,RuleBase.LOS
	,RuleBase.LocalAdministrativeCategoryCode
	,RuleBase.NationalAdmissionMethodCode
	,RuleBase.SpecialServiceCode
	,RuleBase.NationalDischargeMethodCode
	,RuleBase.NationalAttendanceCategoryCode
	,RuleBase.Reportable
	,RuleBase.OperationDayOfWeek 
	,RuleBase.SessionPeriod 
	,RuleBase.TheatreSurgeonCode 
	,RuleBase.CutOffTime
	,RuleBase.StatusCode
	,RuleBase.SourceAdminCategoryCode 
	,RuleBase.SourceFirstAttendanceCode
	,RuleBase.Theatre
	,RuleBase.LocationCode
	,RuleBase.Postcode
from   	
	Allocation.RuleBase

inner join Allocation.TemplateDataset
on	TemplateDataset.Template =  RuleBase.Template
and	TemplateDataset.Active = 1

inner join Allocation.Allocation
on	Allocation.AllocationID = RuleBase.AllocationID
and	Allocation.Active = 1

inner join Allocation.AllocationTypeDataset
on	AllocationTypeDataset.AllocationTypeID = Allocation.AllocationTypeID
and	AllocationTypeDataset.DatasetCode = TemplateDataset.DatasetCode
and	AllocationTypeDataset.Active = 1

inner join Allocation.Template
on	Template.Template = TemplateDataset.Template

where
	RuleBase.Active = 1

GO

CREATE UNIQUE CLUSTERED INDEX [IX_AllocationTemplateDataset]
    ON [Allocation].[AllocationTemplateDataset](
		[RuleBaseRecno] ASC
		,DatasetCode
);


﻿CREATE view [Allocation].[ContractPODByAttendanceCategory]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

inner join Allocation.Allocation
on AllocationTemplateDataset.AllocationID = Allocation.AllocationID
where
	AllocationTemplateDataset.NationalAttendanceCategoryCode = WrkAllocateDataset.NationalAttendanceCategoryCode
and WrkAllocateDataset.ArrivalTime >= AllocationTemplateDataset.FromDate
and 
	(WrkAllocateDataset.ArrivalTime <= AllocationTemplateDataset.ToDate
	or
	AllocationTemplateDataset.ToDate is null
	)
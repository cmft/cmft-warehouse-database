﻿CREATE view [Allocation].[ContractPODByManagementIntention]

as

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.SourceIntendedManagementCode = AllocationTemplateDataset.SourceIntendedManagementCode
and	WrkAllocateDataset.SourceContextCode = AllocationTemplateDataset.SourceContextCode
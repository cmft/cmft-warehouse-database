﻿CREATE view [Allocation].[VTEByAnyDiagnosisPatCat]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.GlobalEpisodeNo = 1
and
	WrkAllocateDataset.PatientCategoryCode = AllocationTemplateDataset.PatientCategoryCode
and	exists
	(
	select
		1
	from
		Allocation.WrkAllocateDatasetDiagnosis
	where
		WrkAllocateDatasetDiagnosis.DatasetRecno = WrkAllocateDataset.DatasetRecno
	and	WrkAllocateDatasetDiagnosis.DatasetCode = WrkAllocateDataset.DatasetCode
	and	WrkAllocateDatasetDiagnosis.DiagnosisCode = AllocationTemplateDataset.DiagnosisCode
	)
﻿CREATE view [Allocation].[DirectorateByWard]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	 WrkAllocateDataset.DatasetRecno
	,WrkAllocateDataset.DatasetCode
	,AllocationID = AllocationTemplateDataset.AllocationID
	,AllocationTemplateDataset.Priority
	,AllocationTemplateDataset.Template
from
	Allocation.WrkAllocateDataset

inner join Allocation.AllocationTemplateDataset 
on	AllocationTemplateDataset.DatasetCode = WrkAllocateDataset.DatasetCode

where
	WrkAllocateDataset.OnNICTWard = 1	
and	AllocationTemplateDataset.SourceContextCode = WrkAllocateDataset.SourceContextCode
and	(
		WrkAllocateDataset.ActivityDate between AllocationTemplateDataset.FromDate and coalesce(AllocationTemplateDataset.ToDate, '31 dec 9999')
	or	WrkAllocateDataset.ActivityDate is null -- End Directorate, this could be null if patient still an IP, ie Activity Date = Episode End Date, which could be null
	)

﻿CREATE TABLE [Allocation].[Allocation] (
    [AllocationID]       INT           IDENTITY (1, 1) NOT NULL,
    [AllocationCode]	 VARCHAR (20) NOT NULL,
    [Allocation]         VARCHAR (255) NOT NULL,
    [AllocationTypeID]   INT      NOT NULL,
    [Priority]           INT           NULL,
    [Active]             BIT           DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Allocation_Allocation] PRIMARY KEY CLUSTERED ([AllocationID]),
    CONSTRAINT [FK_Allocation_Allocation_AllocationType] FOREIGN KEY ([AllocationTypeID]) REFERENCES [Allocation].[AllocationType] ([AllocationTypeID]) ON UPDATE CASCADE
);

GO

create unique index IX_Allocation_Allocation_01 on Allocation.Allocation (AllocationCode, AllocationTypeID)
GO

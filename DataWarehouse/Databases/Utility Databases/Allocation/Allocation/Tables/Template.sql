﻿CREATE TABLE [Allocation].[Template] (
    [TemplateID]       INT           IDENTITY (1, 1) NOT NULL,
    [Template]         VARCHAR (255) NOT NULL,
    [Remarks]          VARCHAR (255) NULL,
    [Priority]         INT           NOT NULL,
    CONSTRAINT [PK_Allocation_Template] PRIMARY KEY CLUSTERED ([Template] ASC)
);

GO

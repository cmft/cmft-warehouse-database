﻿CREATE TABLE [Allocation].[WrkAllocation] (
    [AllocateServiceRecno] INT          IDENTITY (1, 1) NOT NULL,
    [DatasetCode]          VARCHAR (10) NOT NULL,
    [DatasetRecno]         INT          NOT NULL,
    [AllocationTypeID]     INT          NOT NULL,
    [AllocationID]         INT          NOT NULL,
	[AllowMultipleAllocations] BIT NOT NULL,
    [Priority]             INT          NOT NULL,
    [TemplateID]           INT          NULL, 
    CONSTRAINT [PK_Allocation_WrkAllocation] PRIMARY KEY ([AllocateServiceRecno])
);

GO

CREATE NONCLUSTERED INDEX [IX_Allocation_WrkAllocation_01]
ON [Allocation].[WrkAllocation] ([DatasetCode],[DatasetRecno],[AllocationTypeID])
INCLUDE ([AllocateServiceRecno],[AllocationID],[Priority])

GO

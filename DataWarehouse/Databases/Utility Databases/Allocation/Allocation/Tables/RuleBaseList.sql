﻿CREATE TABLE [Allocation].RuleBaseList (
    List VARCHAR (255) NOT NULL,
    ListValue    VARCHAR (255) NOT NULL,
    ContextCode      VARCHAR (20) NULL,
    CONSTRAINT [PK_Allocation_RuleList] PRIMARY KEY CLUSTERED (List ASC, ListValue ASC)
);
GO

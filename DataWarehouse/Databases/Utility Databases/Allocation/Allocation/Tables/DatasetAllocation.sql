﻿CREATE TABLE [Allocation].[DatasetAllocation] (
    [DatasetRecno]     INT       NOT NULL,
    [DatasetCode]      VARCHAR (10) NOT NULL,
    [AllocationID]     int NOT NULL,
    [TemplateID]       INT          NULL,
    CONSTRAINT [PK_Allocation_DatasetAllocation] PRIMARY KEY CLUSTERED ([DatasetCode] ASC, [DatasetRecno] ASC, AllocationID ASC),
    CONSTRAINT [FK_Allocation_DatasetAllocation_Dataset] FOREIGN KEY ([DatasetCode]) REFERENCES [Allocation].[Dataset] ([DatasetCode]),
    CONSTRAINT [FK_Allocation_DatasetAllocation_Allocation] FOREIGN KEY ([AllocationID]) REFERENCES [Allocation].[Allocation] ([AllocationID])
);

GO

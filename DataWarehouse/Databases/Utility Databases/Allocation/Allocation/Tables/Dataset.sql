﻿CREATE TABLE [Allocation].[Dataset] (
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Dataset]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Allocation_Dataset] PRIMARY KEY CLUSTERED ([DatasetCode] ASC) WITH (FILLFACTOR = 90)
);

GO

﻿CREATE TABLE [Allocation].[AllocationTypeDataset] (
    [AllocationTypeID]    int NOT NULL,
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Active]      BIT           NOT NULL,
    CONSTRAINT [PK_Allocation_AllocationTypeDataset] PRIMARY KEY CLUSTERED ([AllocationTypeID] ASC, [DatasetCode] ASC),
    CONSTRAINT [FK_Allocation_AllocationTypeDataset_AllocationType] FOREIGN KEY ([AllocationTypeID]) REFERENCES [Allocation].[AllocationType] ([AllocationTypeID]) ON UPDATE CASCADE
);

GO

﻿CREATE TABLE [Allocation].[TemplateDataset] (
    [Template]    VARCHAR (255) NOT NULL,
    [DatasetCode] VARCHAR (10)  NOT NULL,
    [Active]      BIT           NOT NULL,
    CONSTRAINT [PK_Allocation_TemplateDataset] PRIMARY KEY CLUSTERED ([Template] ASC, [DatasetCode] ASC),
    CONSTRAINT [FK_Allocation_TemplateDataset_Template] FOREIGN KEY ([Template]) REFERENCES [Allocation].[Template] ([Template]) ON UPDATE CASCADE
);

GO

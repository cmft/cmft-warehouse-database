﻿CREATE TABLE [Allocation].[AllocationType] (
    [AllocationTypeID] INT      IDENTITY (1, 1) NOT NULL,
    [AllocationType]   VARCHAR (255) NOT NULL,
    [ContextCode]      VARCHAR (20)  NULL,
    [AllowMultipleAllocations] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_Allocation_AllocationType_1] PRIMARY KEY CLUSTERED ([AllocationTypeID] ASC),
    CONSTRAINT [FK_Allocation_AllocationType_TemplateType] FOREIGN KEY ([AllocationTypeID]) REFERENCES [Allocation].[AllocationType] ([AllocationTypeID])
);

GO

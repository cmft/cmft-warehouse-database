﻿CREATE procedure [Allocation].[ExecuteAllocation] 
	 @DatasetCode varchar (10)
as

--------------------------------------------------------------------------
-- Copyright  Technologies Ltd. 2003    --
--------------------------------------------------------------------------

--prevent parallel running - isolate per dataset/allocation type
--this allows the truncation of the WrkAllocation table and subsequent reset of identity column
set transaction isolation level serializable

begin transaction

	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)



	declare @TemplateID int
	declare @Template varchar(128)
	declare @WrkSQL varchar(max)
	declare @Processed int
	declare @Allocated int 
	declare @Unallocated int

	select @StartTime = getdate()


	truncate table Allocation.WrkAllocation


	declare TemplateCursor cursor fast_forward for

	select
		Template.TemplateID
		,TemplateDataset.Template
	from
		Allocation.TemplateDataset

	inner join Allocation.Template
	on	TemplateDataset.Template = Template.Template

	where  	
		TemplateDataset.DatasetCode = @DatasetCode
	and TemplateDataset.Active = 1 

	order by
		Template.Priority

	OPEN TemplateCursor

	FETCH NEXT FROM TemplateCursor

	INTO
		@TemplateID
		,@Template

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select
			@WrkSQL =
				'Insert into Allocation.WrkAllocation (DatasetRecno, DatasetCode, AllocationTypeID, AllowMultipleAllocations, AllocationID, Priority, TemplateID) 
				select TemplateResult.DatasetRecno, TemplateResult.DatasetCode, Allocation.AllocationTypeID, AllocationType.AllowMultipleAllocations, TemplateResult.AllocationID, TemplateResult.Priority, TemplateID =' + cast(@TemplateID as varchar) + ' from ' +
				@Template + ' TemplateResult' +
				' inner join Allocation.Allocation' +
				' on Allocation.AllocationID = TemplateResult.AllocationID' +
				' inner join Allocation.AllocationTypeDataset' +
				' on AllocationTypeDataset.DatasetCode = TemplateResult.DatasetCode' +
				' and AllocationTypeDataset.AllocationTypeID = Allocation.AllocationTypeID' +
				' and AllocationTypeDataset.Active = 1' +
				' inner join Allocation.AllocationType' +
				' on AllocationType.AllocationTypeID = AllocationTypeDataset.AllocationTypeID' +
				' where TemplateResult.Template = ''' + @Template + ''' and TemplateResult.DatasetCode = ''' + @DatasetCode + ''''
		
		print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL

		execute(@WrkSQL)

		FETCH NEXT FROM TemplateCursor
		INTO
			@TemplateID
			,@Template

	END
	  
	CLOSE TemplateCursor
	DEALLOCATE TemplateCursor


	delete from Allocation.DatasetAllocation
	where 
		exists
		(
		select
			1 
		from 
			Allocation.WrkAllocateDataset

		inner join Allocation.Allocation
		on	Allocation.AllocationID = DatasetAllocation.AllocationID

		inner join Allocation.AllocationTypeDataset
		on	AllocationTypeDataset.DatasetCode = DatasetAllocation.DatasetCode
		and	AllocationTypeDataset.AllocationTypeID = Allocation.AllocationTypeID
		and	AllocationTypeDataset.Active = 1

		where 
			WrkAllocateDataset.DatasetRecno = DatasetAllocation.DatasetRecno
		and	WrkAllocateDataset.DatasetCode = DatasetAllocation.DatasetCode
		)
	and	DatasetAllocation.DatasetCode = @DatasetCode



	insert into Allocation.DatasetAllocation
		(
		DatasetRecno
		,DatasetCode
		,AllocationID
		,TemplateID
		)
	select
 		WrkAllocation.DatasetRecno
 		,WrkAllocation.DatasetCode
		,WrkAllocation.AllocationID
		,WrkAllocation.TemplateID
	from 
		Allocation.WrkAllocation
	where
		not exists
		(
		select
			1
		from
			Allocation.WrkAllocation Previous
		where
			Previous.DatasetRecno = WrkAllocation.DatasetRecno
		and	Previous.DatasetCode = WrkAllocation.DatasetCode
		and	Previous.AllocationTypeID = WrkAllocation.AllocationTypeID
		and (
				(
					Previous.AllowMultipleAllocations = 1
				and	Previous.AllocationID = WrkAllocation.AllocationID
				)
			or	Previous.AllowMultipleAllocations = 0
			)			

		and	(
				Previous.Priority < WrkAllocation.Priority
			or
				(
					Previous.Priority = WrkAllocation.Priority 
				and	Previous.AllocationID < WrkAllocation.AllocationID
				)
			or
				(
					Previous.Priority = WrkAllocation.Priority 
				and	Previous.AllocationID = WrkAllocation.AllocationID
				and	Previous.AllocateServiceRecno < WrkAllocation.AllocateServiceRecno
				)
			)
		)

	select
		@Processed = count(*)
	from
		Allocation.WrkAllocateDataset
	where
		DatasetCode = @DatasetCode


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())


	declare @AllocationTypeID int
	declare @AllocationType varchar(255)
		
	declare AllocationTypeDatasetCursor cursor fast_forward for

	select
		AllocationType.AllocationTypeID
		,AllocationType.AllocationType
	from
		Allocation.AllocationTypeDataset

	inner join Allocation.AllocationType
	on	AllocationType.AllocationTypeID = AllocationTypeDataset.AllocationTypeID

	where  	
		AllocationTypeDataset.DatasetCode = @DatasetCode
	and AllocationTypeDataset.Active = 1 

	order by
		AllocationType.AllocationType

	OPEN AllocationTypeDatasetCursor

	FETCH NEXT FROM AllocationTypeDatasetCursor

	INTO
		@AllocationTypeID
		,@AllocationType

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select 
			@Allocated = count(*) 
		from 
			Allocation.WrkAllocateDataset
		where
			WrkAllocateDataset.DatasetCode = @DatasetCode
		and	exists 
			(
			select
				1
			from 
				Allocation.DatasetAllocation

			inner join Allocation.Allocation
			on	Allocation.AllocationID = DatasetAllocation.AllocationID

			where
    			DatasetAllocation.DatasetRecno = WrkAllocateDataset.DatasetRecno
			and DatasetAllocation.DatasetCode = WrkAllocateDataset.DatasetCode
			and	Allocation.AllocationTypeID = @AllocationTypeID
			)


		select @Unallocated = @Processed - @Allocated

		select
			@Stats =
				'Dataset ' + @DatasetCode +
				', Allocation type ' + @AllocationType +
				', Processed ' + CONVERT(varchar(10), @Processed) +
				', Allocated ' + CONVERT(varchar(10), @Allocated) +
				', Unallocated ' + CONVERT(varchar(10), @Unallocated) +
				', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


		exec [Audit].[WriteLogEvent]
			 @ProcedureName
			,@Stats


		FETCH NEXT FROM AllocationTypeDatasetCursor
		INTO
			@AllocationTypeID
			,@AllocationType

	END
	  
	CLOSE AllocationTypeDatasetCursor
	DEALLOCATE AllocationTypeDatasetCursor


commit
﻿
USE [WarehouseOLAPMergedV2]
GO

/****** Object:  View [Allocation].[VTEBucket]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAmbulatory]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnaesthetic]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnyDiagnosis]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnyDiagnosisPatCat]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnyOperation]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnyOperationDiagnosis]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnyOperationPatCat]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnyOperationPrimaryOperation]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByAnyOperationStartWard]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByDuration]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByNationalSpecialtyAdmissionMethod]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByNationalSpecialtyPatCatDuration]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByNationalSpecialtyZeroLOS]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByPrimaryOperation]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEBySitePatCatDuration]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByStartDirectorateNationalSpecialtyPatCat]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByStartDirectoratePatCat]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByStartDirectorateSpecialtyNotPrimaryOperation]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByStartDirectorateSpecialtyPrimaryOperation]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByStartDirectorateStartWard]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByStartWardDuration]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByStartWardEndWard]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByTripleOperation]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[VTEByWardEpisode]    Script Date: 04/04/2016 10:05:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [WarehouseOLAPMergedV2]
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionBucket]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByAdministrativeCategory]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDischargeMethod]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDivisionNationalSpecialtyPrimaryProcedurePatientCategory]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDivisionNationalSpecialtyWard]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDivisionNationalSpecialtyWardDischargeDate]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDivisionSourceSpecialty]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDivisionSourceSpecialtyNeonatalLevelOfCare]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDivisionSourceSpecialtyNeonatalWardStay]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByDivisionWard]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByNationalSpecialtyPatientCategory]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByNationalSpecialtyPrimaryProcedure]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByPatientClassification]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByPrimaryProcedurePatientCategory]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByPrimaryProcedurePrimaryDiagnosis]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionBySourceSpecialty]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionBySourceSpecialtyPatientCategory]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DischargeLetterExclusionByWard]    Script Date: 05/04/2016 09:31:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [WarehouseOLAPMergedV2]
GO

/****** Object:  View [Allocation].[ContractFlagBucket]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByAdministrativeCategory]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByAdministrativeCategoryLocal]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByClinic]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByClinicProcedure]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByClinicSex]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByCommissionerClinic]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByCommissionerEndWard]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByCommissionerHRG]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByCommissionerTreatmentFunction]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByContractSerialNo]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByDirectorateTreatmentFunction]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByFirstAttendanceFlag]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByHRG]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByNotClinicTreatmentFunction]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByNotSiteHRG]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByPrimaryProcedure]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByProcedure]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySiteHRG]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySiteTreatmentFunction]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceAppointmentType]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceConsultant]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceContext]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceFirstAttendanceFlag]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceFirstAttendanceFlagNotSourceOfReferral]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceSpecialty]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceSpecialtyHRG]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceSpecialtyNotSourceAppointmentStatus]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceSpecialtyNotSourceAppointmentStatusNotSourceSourceOfReferral]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceSpecialtyNotSourceSourceOfReferral]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagBySourceSpecialtyStartWard]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByStartWard]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByTreatmentFunction]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByTreatmentFunctionAge]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByTreatmentFunctionPrimaryDiagnosis]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByTreatmentFunctionReferralSpecialtyType]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagByTreatmentFunctionSourceAppointmentType]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagDuplicateAppointment]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagGreenCard]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagIsleOfMan]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagNeonatology]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagOverseas]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractFlagPayableOverseas]    Script Date: 05/04/2016 10:05:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [WarehouseOLAPMergedV2]
GO

/****** Object:  View [Allocation].[ContractHRGARMS]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGAudiologyAssessment]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBucket]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGByClinic]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGByClinicFirstAttendanceFlag]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGByClinicHRG]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGByClinicHRGFirstAttendanceFlag]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGByDirectorateHRG]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGByPrimaryProcedure]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBySourceAppointmentType]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBySourceAppointmentTypeFirstAttendanceFlag]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBySourceContext]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBySourceSpecialty]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBySourceSpecialtyAttendanceStatusClinic]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBySourceSpecialtySourceAppointmentTypeSourceSourceOfReferralSourceAppointmentStatus]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGBySourceSpecialtySourceSourceOfReferralSourceAppointmentStatusNotSourceAppointmentType]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGByTreatmentFunctionProcedure]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGColposcopy]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGFirstAudiologyFitting]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractHRGHysteroscopy]    Script Date: 05/04/2016 14:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [WarehouseOLAPMergedV2]
GO

/****** Object:  View [Allocation].[ContractPODAudiology]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODBucket]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByAttendanceCategory]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByClinic]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByClinicFirstAttendanceFlag]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByClinicHRG]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByClinicHRGFirstAttendanceFlag]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByClinicHRGList]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByClinicNotHRG]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByClinicProcedureCoded]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByDirectorateHRGFirstAttendanceFlag]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByFirstAttendanceFlag]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByHRG]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByHRGFirstAttendanceFlagNotClinic]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByManagementIntention]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByNationalAdmissionMethodHRG]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByNotTreatmentFunctionSourceAppointmentType]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByNotTreatmentFunctionSourceAppointmentTypeFirstAttendanceFlag]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByPatientClassificationAdmissionMethod]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByPatientClassificationAdmissionMethodLoSHRGAge]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByPrimaryProcedure]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByPrimaryProcedureFirstAttendance]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByProcedure]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODBySiteHRG]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODBySourceAppointmentType]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODBySourceContext]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODBySourceSpecialtySourceSourceOfReferralSourceAppointmentStatus]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODBySpecialService]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByTreatmentFunctionFirstAttendanceFlag]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODByTreatmentFunctionProcedureFirstAttendanceFlag]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[ContractPODMultiProfessional]    Script Date: 05/04/2016 15:06:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [WarehouseOLAPMergedV2]
GO

/****** Object:  View [Allocation].[DiagnosticNationalExamByConsultantNationalMainSpecialty]    Script Date: 05/04/2016 15:40:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DiagnosticNationalExamByPrimaryProcedure]    Script Date: 05/04/2016 15:40:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DiagnosticNationalExamBySourceClinic]    Script Date: 05/04/2016 15:40:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DiagnosticNationalExamBySourceSpecialty]    Script Date: 05/04/2016 15:40:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Allocation].[DiagnosticNationalExamBySourceWaitingList]    Script Date: 05/04/2016 15:40:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

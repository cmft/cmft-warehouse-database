﻿CREATE FUNCTION [dbo].[DATASET_ID](@DataSet_Name nvarchar(20))
RETURNS int
AS
BEGIN
declare @dataSetID int;

SELECT @dataSetID = DataSetID 
FROM ETL.DataSet
WHERE Name = @DataSet_Name;

RETURN @dataSetID;

END;
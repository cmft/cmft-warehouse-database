﻿CREATE AGGREGATE [dbo].[StringConcat]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [StringLibrary].[StringConcat]
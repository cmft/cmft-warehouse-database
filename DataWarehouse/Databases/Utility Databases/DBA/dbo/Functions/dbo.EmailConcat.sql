﻿CREATE AGGREGATE [dbo].[EmailConcat]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [StringLibrary].[EmailConcat]
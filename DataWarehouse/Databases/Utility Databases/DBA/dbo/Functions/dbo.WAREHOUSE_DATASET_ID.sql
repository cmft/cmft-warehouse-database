﻿CREATE FUNCTION [dbo].[WAREHOUSE_DATASET_ID](@ShortName nvarchar(20))
RETURNS int
AS
BEGIN
declare @dataSetID int;

SELECT @dataSetID = WarehouseDataSetID 
FROM ETL.WarehouseDataSet
WHERE ShortName = @ShortName;

RETURN @dataSetID;

END;
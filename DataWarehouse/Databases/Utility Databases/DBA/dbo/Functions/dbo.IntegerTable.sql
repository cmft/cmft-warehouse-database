﻿CREATE FUNCTION [dbo].[IntegerTable] ( @MaxValue INT )
RETURNS @Integers TABLE ( [IntValue] INT )
AS
BEGIN
    DECLARE @Digits TABLE ( [Digit] INT )
    DECLARE @Counter INT

    SET @Counter = 0
    WHILE @Counter < 10
    BEGIN
        INSERT INTO @Digits ( [Digit] ) VALUES ( @Counter )
        SET @Counter = @Counter + 1
    END
	
	IF @MaxValue<10000 begin
	    INSERT INTO @Integers ( [IntValue] )
		SELECT (Thousands.Digit * 1000) + (Hundreds.Digit * 100) + 
			   (Tens.Digit * 10) + Ones.Digit
		FROM @Digits Thousands, @Digits Hundreds, @Digits Tens, @Digits Ones
		WHERE (Thousands.Digit * 1000) + (Hundreds.Digit * 100) + 
			   (Tens.Digit * 10) + Ones.Digit BETWEEN 1 AND @MaxValue
		ORDER BY 1
	end
	else
	begin
	    INSERT INTO @Integers ( [IntValue] )
		SELECT (HundredThousands.Digit * 100000)+(TenThousands.Digit * 10000)+(Thousands.Digit * 1000) + (Hundreds.Digit * 100) + 
           (Tens.Digit * 10) + Ones.Digit
		FROM @Digits HundredThousands, @Digits TenThousands,@Digits Thousands, @Digits Hundreds, @Digits Tens, @Digits Ones
		WHERE (HundredThousands.Digit * 100000)+(TenThousands.Digit * 10000)+(Thousands.Digit * 1000) + (Hundreds.Digit * 100) + 
           (Tens.Digit * 10) + Ones.Digit BETWEEN 1 AND @MaxValue
    ORDER BY 1
	end;



    RETURN
END
﻿CREATE FUNCTION [dbo].[DATASET_NAME](@DataSetID int)
RETURNS nvarchar(20)
AS
BEGIN
declare @dataSetName nvarchar(20);

SELECT @dataSetName = Name 
FROM ETL.DataSet
WHERE DataSetID = @DataSetID;

RETURN @dataSetName;

END;
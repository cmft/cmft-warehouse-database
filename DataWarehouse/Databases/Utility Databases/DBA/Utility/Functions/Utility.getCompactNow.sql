﻿create function [Utility].[getCompactNow]()
returns varchar(14)
as 
begin
return Utility.getCompactDatetime(SYSUTCDATETIME());
end;
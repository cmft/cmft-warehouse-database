﻿CREATE FUNCTION [BEDMAN].[GetRestoreType]()
RETURNS int
AS
BEGIN
DECLARE @retval int;

	SELECT @retval = LastDBRestoreValue 
	FROM BEDMAN.Settings;

	--If last type was 0 (db only) then this must be type 1 or 2 
	--If last type was 1 or 2 (db + diff) this must be type 0
	IF @retval = 2 SET @retval = 0 ELSE SET @retval = 1;

	RETURN @retval
END;
﻿CREATE FUNCTION [BEDMAN].[NotifyEDD] () RETURNS bit
AS
BEGIN
	DECLARE @val int = -1;
	DECLARE @retval bit;

	SELECT @val = LastDBRestoreValue 
	FROM BEDMAN.Settings;

	IF (@val > 0)
		set @retval = cast (1 as bit)
	else
		set @retval = cast (0 as bit)
		
	RETURN @retval;
END;
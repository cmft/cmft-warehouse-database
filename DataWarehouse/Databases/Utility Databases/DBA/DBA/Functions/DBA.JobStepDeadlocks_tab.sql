﻿CREATE FUNCTION [DBA].[JobStepDeadlocks_tab](	
	@endDatetime datetime = null, --convert(int,convert(varchar(8),dateadd(month,-6,getdate()),112)),
	@monthsBack tinyint = 1 --convert(int,convert(varchar(8),getdate(),112)))
	)
RETURNS @retDeadlocks TABLE 
(
	  [Job Name] sysname
	, [Step Number] int
	, [Step Name] sysname
	, [Number Of Deadlocks] int
	, StartDate int
	, EndDate int
	, firstOccurrence int
	, lastOccurrence int
)
AS

BEGIN
   
   
declare @deadlockMessageID int = 1205;

declare @startDate int, @endDate int;
 
if @endDatetime is null set @endDatetime = getdate();

set @startDate  = convert(int,convert(varchar(8),dateadd(month,@monthsBack * - 1,@endDateTime),112))
set @endDate  = convert(int,convert(varchar(8),@endDateTime,112))


  ; WITH deadlocks([Job Name], [Step Number], [Step Name], [Number Of Deadlocks],StartDate,EndDate,firstOccurrence,lastOccurrence) AS
	  ( select j.name [Job Name]
		, jh.step_id [Step Number] 
		, jh.step_name [Step Name]
		, count(1) [Number Of Deadlocks]
		, @startDate StartDate 
		, @EndDate EndDate
		,min(run_date) firstOccurrence 
		,max(run_date) lastOccurrence
		from msdb.dbo.sysjobhistory jh with (nolock)
			inner join msdb.dbo.sysjobs j with (nolock)
				on jh.job_id = j.job_id 
		where jh.sql_message_id = @deadlockMessageID 
		  and jh.run_date between @startDate and @endDate
		group by [name]
				,[step_id]
				,[step_name]
				)

   INSERT @retDeadlocks([Job Name], [Step Number], [Step Name], [Number Of Deadlocks],StartDate,EndDate,firstOccurrence,lastOccurrence)
   SELECT [Job Name], [Step Number], [Step Name], [Number Of Deadlocks],StartDate,EndDate,firstOccurrence,lastOccurrence
     FROM deadlocks 
   RETURN
END
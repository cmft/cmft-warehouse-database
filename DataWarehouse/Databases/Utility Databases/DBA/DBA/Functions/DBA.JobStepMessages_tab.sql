﻿CREATE function [DBA].[JobStepMessages_tab](
	@endDatetime datetime = null, --convert(int,convert(varchar(8),dateadd(month,-6,getdate()),112)),
	@monthsBack tinyint = 1 --convert(int,convert(varchar(8),getdate(),112)))
	)
returns @ret table
(
	  [Job Name] sysname
	, [Step Number] int
	, [Step Name] sysname
	, [Message Number] int
	, [Message Text] varchar(max)
	, StartDate int
	, EndDate int
)
AS

BEGIN
   
   
declare @startDate int, @endDate int;
 
if @endDatetime is null set @endDatetime = getdate();

set @startDate  = convert(int,convert(varchar(8),dateadd(month,@monthsBack * - 1,@endDateTime),112))
set @endDate  = convert(int,convert(varchar(8),@endDateTime,112))


  ; WITH msgs([Job Name], [Step Number], [Step Name], [Message Number], [Message Text],StartDate,EndDate) AS
	  ( select j.name [Job Name]
		, jh.step_id [Step Number] 
		, jh.step_name [Step Name]
		, jh.sql_message_id, jh.[message]
		, @startDate StartDate 
		, @EndDate EndDate
		from msdb.dbo.sysjobhistory jh with (nolock)
			inner join msdb.dbo.sysjobs j with (nolock)
				on jh.job_id = j.job_id 
		where jh.run_date between @startDate and @endDate
		and jh.sql_message_id != 0
		and not (jh.message like '%The step succeeded.')
				)

   INSERT @ret([Job Name], [Step Number], [Step Name], [Message Number], [Message Text],StartDate,EndDate)
   SELECT [Job Name], [Step Number], [Step Name], [Message Number], [Message Text],StartDate,EndDate
     FROM msgs 
   RETURN
END
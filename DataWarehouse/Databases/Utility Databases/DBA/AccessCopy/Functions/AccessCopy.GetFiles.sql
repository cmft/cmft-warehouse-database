﻿CREATE FUNCTION [AccessCopy].[GetFiles](@DataSetID int)
RETURNS nvarchar(4000)
AS
BEGIN
DECLARE @ret nvarchar(4000)

select @ret = DBA.dbo.EmailConcat ('exec xp_cmdshell N''copy /y "' + SourcePath + [Filename] + '" "' + DestinationPath + '"''')
from AccessCopy.Files 
WHERE RelatedDatasetID = @DataSetID; -- SCAT

RETURN @ret;
END;
﻿CREATE FUNCTION [StringLib].[RegExpLike](@Text [nvarchar](max), @Pattern [nvarchar](255))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlRegularExpressions].[SqlRegularExpressions].[Like]
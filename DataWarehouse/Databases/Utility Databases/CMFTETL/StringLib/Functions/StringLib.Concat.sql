﻿CREATE AGGREGATE [StringLib].[Concat]
(@value [nvarchar](4000))
RETURNS[nvarchar](4000)
EXTERNAL NAME [StringLibrary].[StringConcat]
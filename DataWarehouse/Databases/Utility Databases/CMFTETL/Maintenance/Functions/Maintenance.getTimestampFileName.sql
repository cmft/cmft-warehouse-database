﻿create function [Maintenance].[getTimestampFileName](@stem varchar(255) = NULL)
returns varchar(255)
as begin
declare @ret varchar(255);
	if (@stem IS NULL)
		set @ret = 'file';
	else
		set @ret = @stem;

if (@ret IS NULL)
	set @ret = 'file';
	
set @ret = @ret + '_' + DateTimeLib.CompactNow();

return @ret;
end;
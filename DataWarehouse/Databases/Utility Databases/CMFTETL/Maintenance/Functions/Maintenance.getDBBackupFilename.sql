﻿create function [Maintenance].[getDBBackupFilename](@db_id int = NULL, @stem varchar(255) = NULL)
returns varchar(255)
as begin
declare @ret varchar(255);
if (@db_id is null)
	if (@stem IS NULL)
		set @ret = 'DB';
	else
		set @ret = @stem;
else
	select @ret = d.name 
		from sys.databases d 
		where d.database_id = @db_id;

if (@ret IS NULL)
	set @ret = 'DB';
	
set @ret = @ret + '_' + Utility.getCompactNow();

return @ret;
end;
﻿CREATE FUNCTION [Utility].[DirectorySize](@Path [nvarchar](255))
RETURNS [nvarchar](50) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [FileSystemHelper].[UserDefinedFunctions].[DirectorySize]
﻿CREATE FUNCTION [Utility].[DirectoryList](@Path [nvarchar](255), @Filter [nvarchar](255))
RETURNS  TABLE (
	[name] [nvarchar](max) NULL,
	[directory] [bit] NULL,
	[size] [bigint] NULL,
	[date_created] [datetime] NULL,
	[date_modified] [datetime] NULL,
	[extension] [nvarchar](max) NULL
) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [FileSystemHelper].[UserDefinedFunctions].[DirectoryList]
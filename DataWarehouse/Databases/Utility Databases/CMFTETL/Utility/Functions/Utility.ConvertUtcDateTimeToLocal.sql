﻿Create Function [Utility].[ConvertUtcDateTimeToLocal](@utcDateTime DateTime) 
Returns DateTime 
Begin 
    Declare @utcNow DateTime 
    Declare @localNow DateTime 
    Declare @timeOffSet Int 
 
    -- Figure out the time difference between UTC and Local time 
    Set @utcNow = GetUtcDate() 
    Set @localNow = GetDate() 
    Set @timeOffSet = DateDiff(hh, @utcNow, @localNow)  
 
    DECLARE @localTime datetime  
 
    Set @localTime = DateAdd(hh, @timeOffset, @utcDateTime)  
 
    -- Check Results 
    return @localTime  
 
End
﻿create function [DateTimeLib].[CompactDatetime](@datetime datetime2(7))
returns varchar(14)
as
begin
	declare @ret varchar(14);
	select @ret = (convert(varchar,@datetime,112) + replace(convert(varchar,@datetime,8),':',''));
	return @ret;
end;
﻿CREATE function [DateTimeLib].[CompactNow]()
returns varchar(14)
as 
begin
return DateTimeLib.CompactDateTime(SYSUTCDATETIME());
end;
﻿

CREATE view [Target].[ShelfordGroupAverageNonElectiveLOS]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value =	5.1 --'2014/2015'  --most up to date

	

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupAverageNonElectiveLOS'

where
	TheDate between '1 apr 2009' and getdate()



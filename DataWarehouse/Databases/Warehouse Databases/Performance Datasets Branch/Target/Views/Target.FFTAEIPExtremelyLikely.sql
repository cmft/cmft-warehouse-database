﻿


CREATE view [Target].FFTAEIPExtremelyLikely

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.7
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.FFTAEIPExtremelyLikely'

--where
--	TheDate between '1 apr 2009' and getdate()





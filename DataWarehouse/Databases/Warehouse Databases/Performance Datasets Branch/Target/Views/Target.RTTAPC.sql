﻿

CREATE view [Target].[RTTAPC]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.90

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.RTTAPC'

--where
--	TheDate between '1 apr 2009' and getdate()




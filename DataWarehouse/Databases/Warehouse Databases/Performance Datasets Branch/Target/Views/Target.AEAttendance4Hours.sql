﻿

CREATE view [Target].[AEAttendance4Hours]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.95

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.AEAttendance4Hours'

--where
--	TheDate between '1 apr 2009' and getdate()
--and
--	datepart(day, TheDate) = 1



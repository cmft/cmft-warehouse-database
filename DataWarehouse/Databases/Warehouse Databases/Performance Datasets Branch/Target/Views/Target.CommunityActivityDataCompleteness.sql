﻿





create view Target.CommunityActivityDataCompleteness

as

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.5

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CommunityActivityDataCompleteness'

--where
--	TheDate between '1 apr 2009' and getdate()








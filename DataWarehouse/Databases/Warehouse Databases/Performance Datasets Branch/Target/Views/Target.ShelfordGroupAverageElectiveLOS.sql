﻿

CREATE view [Target].[ShelfordGroupAverageElectiveLOS]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value =	case 
				when Calendar.FinancialYear = '2013/2014'
				then 4.1
				else 4.0 --'2014/2015' --most up to date unless stated above
				end
	

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupAverageElectiveLOS'

where
	TheDate between '1 apr 2009' and getdate()



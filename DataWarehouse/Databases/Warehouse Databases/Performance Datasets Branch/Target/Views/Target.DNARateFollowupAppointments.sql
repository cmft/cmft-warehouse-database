﻿


CREATE view [Target].DNARateFollowupAppointments

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.084
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.DNARateFollowupAppointments'


--where
--	TheDate between '1 apr 2009' and getdate()





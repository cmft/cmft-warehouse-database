﻿



CREATE view [Target].[HRSicknessAbsenceFTE]

as

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 
		case
			when TheDate < '1 Apr 2015' then 0.041
			else 0.036
		end

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.HRSicknessAbsenceFTE'

--where
--	TheDate between '1 apr 2009' and getdate()






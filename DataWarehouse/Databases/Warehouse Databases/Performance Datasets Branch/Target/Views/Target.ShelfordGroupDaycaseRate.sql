﻿

CREATE view [Target].[ShelfordGroupDaycaseRate]

as


select
	--ItemID = Item.ID 
	TargetID = Target.TargetID
	,DateID 
	,Value =	case 
				when Calendar.FinancialYear = '2014/2015' 
				then 0.776
				else 0.783 --'2015/2016' --most up to date unless stated above
				end
	

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ShelfordGroupDaycaseRate'

where
	TheDate between '1 apr 2009' and getdate()



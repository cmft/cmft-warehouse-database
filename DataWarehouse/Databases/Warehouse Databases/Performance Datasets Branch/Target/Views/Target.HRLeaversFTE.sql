﻿




CREATE view [Target].[HRLeaversFTE]

as

select
	TargetID = Target.TargetID
	,DateID 
	,Value = 
		case 
			when TheDate < '20150401' then 0.126
			else 0.0105
		end

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.HRLeaversFTE'

--where
--	TheDate between '1 apr 2009' and getdate()







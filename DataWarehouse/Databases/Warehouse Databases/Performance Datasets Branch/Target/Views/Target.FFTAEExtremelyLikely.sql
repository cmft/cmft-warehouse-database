﻿


CREATE view [Target].FFTAEExtremelyLikely

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.63
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.FFTAEExtremelyLikely'

--where
--	TheDate between '1 apr 2009' and getdate()





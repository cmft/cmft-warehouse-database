﻿
CREATE view [Target].[DischargeSummaryCompletedWithin48Hours]


as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.9

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.DischargeSummaryCompletedWithin48Hours'
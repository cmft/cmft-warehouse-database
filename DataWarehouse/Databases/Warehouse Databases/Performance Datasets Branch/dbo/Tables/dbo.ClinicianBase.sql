﻿CREATE TABLE [dbo].[ClinicianBase] (
    [SourceContextID]             INT           NULL,
    [SourceContextCode]           VARCHAR (20)  NOT NULL,
    [SourceContext]               VARCHAR (100) NOT NULL,
    [SourceClinicianID]           INT           NOT NULL,
    [SourceClinicianCode]         VARCHAR (100) NOT NULL,
    [SourceClinician]             VARCHAR (900) NOT NULL,
    [LocalClinicianID]            INT           NULL,
    [LocalClinicianCode]          VARCHAR (50)  NULL,
    [LocalClinician]              VARCHAR (200) NULL,
    [NationalClinicianID]         INT           NULL,
    [NationalClinicianCode]       VARCHAR (50)  NULL,
    [NationalClinician]           VARCHAR (953) NULL,
    [MainSpecialtyCode]           VARCHAR (20)  NULL,
    [MainSpecialty]               VARCHAR (100) NULL,
    [AppraisalCompletionDate]     DATE          NULL,
    [AppraisalExpiryDate]         DATE          NULL,
    [AppraisalStatus]             VARCHAR (255) NULL,
    [ClinicalTrainingExpiryDate]  DATE          NULL,
    [ClinicalTrainingStatus]      VARCHAR (255) NULL,
    [MandatoryTrainingExpiryDate] DATE          NULL,
    [MandatoryTrainingStatus]     VARCHAR (255) NULL,
    [RevalidationExpiryDate]      DATE          NULL,
    [ClinicanType]                VARCHAR (100) NOT NULL,
    [Created]                     DATETIME      NULL,
    [Updated]                     DATETIME      NULL,
    [ByWhom]                      VARCHAR (100) NULL,
    CONSTRAINT [PK__Clinicia__78F301C02F5324F9] PRIMARY KEY CLUSTERED ([SourceClinicianID] ASC)
);




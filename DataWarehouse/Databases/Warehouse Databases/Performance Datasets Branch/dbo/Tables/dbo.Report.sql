﻿CREATE TABLE [dbo].[Report] (
    [ReportID]  INT           IDENTITY (1, 1) NOT NULL,
    [Report]    VARCHAR (MAX) NOT NULL,
    [ReportURL] VARCHAR (MAX) NOT NULL
);


﻿CREATE TABLE [dbo].[Denominator] (
    [DenominatorID]    INT            IDENTITY (1, 1) NOT NULL,
    [Denominator]      VARCHAR (4000) NULL,
    [DenominatorLogic] VARCHAR (100)  NULL,
    [Active]           BIT            CONSTRAINT [DF_Denominator_Active] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_DQ_Denominator_1] PRIMARY KEY CLUSTERED ([DenominatorID] ASC)
);


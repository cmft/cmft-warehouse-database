﻿CREATE TABLE [dbo].[DirectorateBase] (
    [DirectorateID]   INT           NOT NULL,
    [DirectorateCode] VARCHAR (5)   NOT NULL,
    [Directorate]     VARCHAR (50)  NOT NULL,
    [Division]        VARCHAR (100) NULL,
    [Created]         DATETIME      NULL,
    [Updated]         DATETIME      NULL,
    [ByWhom]          VARCHAR (100) NULL,
    CONSTRAINT [PK__Director__4E9D6EA238DC8F33] PRIMARY KEY CLUSTERED ([DirectorateID] ASC)
);


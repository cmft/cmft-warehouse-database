﻿CREATE TABLE [dbo].[TargetItemBase] (
    [HierarchyID] INT NOT NULL,
    [TargetID]    INT NOT NULL,
    CONSTRAINT [PK_HierarchyTarget] PRIMARY KEY CLUSTERED ([HierarchyID] ASC, [TargetID] ASC),
    CONSTRAINT [FK_HierarchyTarget_Hierarchy] FOREIGN KEY ([HierarchyID]) REFERENCES [dbo].[Hierarchy] ([HierarchyID]),
    CONSTRAINT [FK_HierarchyTarget_Target] FOREIGN KEY ([TargetID]) REFERENCES [dbo].[Target] ([TargetID])
);


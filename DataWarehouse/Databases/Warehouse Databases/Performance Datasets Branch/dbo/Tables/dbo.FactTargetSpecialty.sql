﻿CREATE TABLE [dbo].[FactTargetSpecialty](
    [HierarchyID] INT NOT NULL,
	[TargetID] [int] NOT NULL,
	[DateID] [int] NOT NULL,
	[SpecialtyID] [int] NOT NULL,
	[Value] [float] NOT NULL 
) ON [PRIMARY]

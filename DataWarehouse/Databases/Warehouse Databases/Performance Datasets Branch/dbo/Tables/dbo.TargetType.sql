﻿CREATE TABLE [dbo].[TargetType] (
    [TargetTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [TargetType]   VARCHAR (50) NULL,
    CONSTRAINT [PK_TargetType] PRIMARY KEY CLUSTERED ([TargetTypeID] ASC)
);


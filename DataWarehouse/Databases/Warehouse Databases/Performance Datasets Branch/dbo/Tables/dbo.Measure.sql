﻿CREATE TABLE [dbo].[Measure] (
    [MeasureID] INT          IDENTITY (1, 1) NOT NULL,
    [Measure]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Measure] PRIMARY KEY CLUSTERED ([MeasureID] ASC)
);


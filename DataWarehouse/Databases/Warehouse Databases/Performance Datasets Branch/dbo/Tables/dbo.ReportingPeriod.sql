﻿CREATE TABLE [dbo].[ReportingPeriod] (
    [ReportingPeriodID] INT          IDENTITY (1, 1) NOT NULL,
    [ReportingPeriod]   VARCHAR (50) NULL,
    CONSTRAINT [PK_EntityPeriod] PRIMARY KEY CLUSTERED ([ReportingPeriodID] ASC)
);


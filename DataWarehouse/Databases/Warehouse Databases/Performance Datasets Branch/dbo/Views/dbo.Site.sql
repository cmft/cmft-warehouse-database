﻿



CREATE view [dbo].[Site]

as


select
	SourceContextCode
	,SourceContext
	,SourceSiteID 
	,SourceSiteCode 
	,SourceSite 
	,LocalSiteID
	,LocalSiteCode
	,LocalSite
	,NationalSiteID 
	,NationalSiteCode 
	,NationalSite 
from
	dbo.SiteBase






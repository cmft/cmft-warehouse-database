﻿create view dbo.DenominatorItem as

select distinct
	 ItemHierarchy.DenominatorID
	,ItemHierarchy.HierarchyID
from
	dbo.ItemHierarchy
where
	DenominatorID is not null



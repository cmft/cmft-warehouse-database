﻿








CREATE view [dbo].[Clinician] as

select
	SourceContextID
	,SourceContextCode
	,SourceContext
	,SourceClinicianID
	,SourceClinicianCode 
	,SourceClinician
	,LocalClinicianID 
	,LocalClinicianCode
	,LocalClinician 
	,NationalClinicianID 
	,ClinicianBase.NationalClinicianCode
	,NationalClinician 				
	,MainSpecialtyCode
	,MainSpecialty
	,AppraisalCompletionDate
	,AppraisalExpiryDate
	,AppraisalStatus
	,ClinicalTrainingExpiryDate
	,ClinicalTrainingStatus
	,MandatoryTrainingExpiryDate
	,MandatoryTrainingStatus
	,RevalidationExpiryDate
	,ClinicanType
	,CentralPortalClinician = 
				case
				when CentralPortalClinician.NationalClinicianCode is not null
				then 'Yes'
				else 'No'
				end
from
	dbo.ClinicianBase
left join dbo.CentralPortalClinician
on	ClinicianBase.NationalClinicianCode = CentralPortalClinician.NationalClinicianCode


















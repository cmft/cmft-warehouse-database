﻿create view dbo.TargetItem as

select
	 TargetItemBase.HierarchyID
	,TargetItemBase.TargetID
from
	dbo.TargetItemBase

inner join dbo.ItemHierarchy
on	ItemHierarchy.HierarchyID = TargetItemBase.HierarchyID

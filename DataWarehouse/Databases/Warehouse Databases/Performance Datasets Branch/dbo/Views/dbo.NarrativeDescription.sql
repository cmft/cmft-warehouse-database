﻿

create view [dbo].[NarrativeDescription]
as

select
	 t.Item
	,i.*
from
	Narrative i

inner join Item t
on	i.ItemID = t.ItemID

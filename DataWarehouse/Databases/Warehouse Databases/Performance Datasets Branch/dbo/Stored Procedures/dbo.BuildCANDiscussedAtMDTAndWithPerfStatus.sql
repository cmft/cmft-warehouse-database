﻿CREATE procedure [dbo].[BuildCANDiscussedAtMDTAndWithPerfStatus](@type char(2), @site varchar(100)) as

-- Denomninator should already exist - get its id
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CAN referral diagnosed with cancer - ' + @type)

if (@DenominatorID is null)
begin
	print('Denominator record does not exist. Run SP BuildCANUrgentCancerReferralSeenWithin2WeeksAll to generate denominator records.')
	return 1
end


-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CAN Patient Discussed at MDT and with Performance Status - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CAN Patient Discussed at MDT and with Performance Status - ' + @type, 'Numerator.CANDiscussedAtMDTAndWithPerfStatus' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = 
(select ItemID from Item 
	where Item = 'Percentage of all diagnosed cancer patients discussed at MDT that have a Performance Status recorded - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Percentage of all diagnosed cancer patients discussed at MDT that have a Performance Status recorded - ' + @site
		,2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end



-- Create view Numerator.CANDiscussedAtMDTAndWithPerfStatus with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Numerator.CANDiscussedAtMDTAndWithPerfStatus' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view)

declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.MDTDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and D.PrimaryDiagnosisCode is not null
and D.DiagnosisDateID is not null
and D.WasPatientDiscussedAtMDT = ''Y''
and D.PerformanceStatusCode is not null
and CancerSite = ''' + @site + ''''

exec(@sql)




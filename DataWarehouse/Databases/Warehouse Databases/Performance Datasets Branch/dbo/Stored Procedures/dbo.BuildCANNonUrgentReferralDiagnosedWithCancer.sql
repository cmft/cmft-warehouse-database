﻿CREATE procedure [dbo].[BuildCANNonUrgentReferralDiagnosedWithCancer] (@type char(2), @site varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CAN non-urgent referral diagnosed with cancer - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CAN non-urgent referral diagnosed with cancer - ' + @type, 'Numerator.CANNonUrgentReferralDiagnosedWithCancer' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Denominator record, if it does not exist
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CAN referral diagnosed with cancer - ' + @type)

if @DenominatorID is null
begin
	insert into Denominator (Denominator, DenominatorLogic)
	values ('CAN referral diagnosed with cancer - ' + @type, 'Denominator.CANReferralDiagnosedWithCancer' + @type)
	select @DenominatorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = (select ItemID from Item where Item = 'Percentage of all diagnosed cancer cases not referred in as a two week wait referral - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Percentage of all diagnosed cancer cases not referred in as a two week wait referral - ' + @site, 2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end


-- Create view Denominator.CANReferralDiagnosedWithCancer with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Denominator.CANReferralDiagnosedWithCancer' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)

declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DiagnosisDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and D.PrimaryDiagnosisCode is not null
and D.DiagnosisDateID is not null -- screen out recs with incorrect early DiagnosisDate

and CancerSite = ''' + @site + ''''

exec(@sql)


-- Create view NumeratorCANNonUrgentReferralDiagnosedWithCancer with a suffix for the given CancerTypeCode

set @view = 'Numerator.CANNonUrgentReferralDiagnosedWithCancer' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)

set @sql = 
'
CREATE view ' + @view + ' as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DiagnosisDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CAN''
and D.PrimaryDiagnosisCode is not null
and D.PriorityTypeCode = ''01'' -- Routine
and D.DiagnosisDateID is not null -- screen out recs with incorrect early DiagnosisDate

and CancerSite = ''' + @site + ''''

exec(@sql)





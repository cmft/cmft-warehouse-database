﻿
CREATE proc [dbo].[BuildSiteBase]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	 dbo.SiteBase target
using
	(
	select 
		SourceContextCode
		,SourceContext
		,SourceSiteID = SourceValueID
		,SourceSiteCode = SourceValueCode
		,SourceSite = SourceValue
		,LocalSiteID = LocalValueID
		,LocalSiteCode = LocalValueCode
		,LocalSite = LocalValue
		,NationalSiteID = NationalValueID
		,NationalSiteCode = coalesce(NationalValueCode,'N/A') 
		,NationalSite = coalesce(NationalValue,'N/A')
	from
		WarehouseOLAPMergedV2.WH.Member
	where
		AttributeCode in ('SITE','COMLOC')

	) source
	on	source.SourceSiteID = target.SourceSiteID 

	
	when not matched by source

	then delete

	when not matched
	then
		insert
			(
			SourceContextCode
			,SourceContext
			,SourceSiteID
			,SourceSiteCode
			,SourceSite
			,LocalSiteID
			,LocalSiteCode
			,LocalSite
			,NationalSiteID
			,NationalSiteCode
			,NationalSite
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.SourceContextCode
			,source.SourceContext
			,source.SourceSiteID
			,source.SourceSiteCode
			,source.SourceSite
			,source.LocalSiteID
			,source.LocalSiteCode
			,source.LocalSite
			,source.NationalSiteID
			,source.NationalSiteCode
			,source.NationalSite
			,getdate()
			,getdate()
			,suser_name()			
			)

	when matched
	and not
		(	
			isnull(target.SourceContextCode, '') = isnull(source.SourceContextCode, '')	
		and isnull(target.SourceContext, '') = isnull(source.SourceContext, '')					
		and isnull(target.SourceSiteID, 0) = isnull(source.SourceSiteID, 0)		
		and isnull(target.SourceSiteCode, '') = isnull(source.SourceSiteCode, '')	
		and	isnull(target.SourceSite, '') = isnull(source.SourceSite, '')
		and	isnull(target.LocalSiteID, 0) = isnull(source.LocalSiteID, 0)	
		and	isnull(target.LocalSiteCode, '') = isnull(source.LocalSiteCode, '')
		and	isnull(target.LocalSite, '') = isnull(source.LocalSite, '')
		and	isnull(target.NationalSiteID, 0) = isnull(source.NationalSiteID, 0)		
		and isnull(target.NationalSiteCode, '') = isnull(source.NationalSiteCode, '')
		and isnull(target.NationalSite, '') = isnull(source.NationalSite, '')		
		)
	then
		update
		set
			target.SourceContextCode = source.SourceContextCode
			,target.SourceContext = source.SourceContext
			,target.SourceSiteID = source.SourceSiteID
			,target.SourceSiteCode = source.SourceSiteCode
			,target.SourceSite = source.SourceSite
			,target.LocalSiteID = source.LocalSiteID						
			,target.LocalSiteCode = source.LocalSiteCode
			,target.LocalSite = source.LocalSite
			,target.NationalSiteID = source.NationalSiteID
			,target.NationalSiteCode = source.NationalSiteCode
			,target.NationalSite = source.NationalSite		
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime







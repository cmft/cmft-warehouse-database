﻿CREATE procedure [dbo].[BuildCANTTreatedWithin62DaysOfScreeningServiceReferralAll] as

exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'CO', 'Colorectal'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'GY', 'Gynaecology'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'HA', 'Haematology'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'HN', 'Head and Neck'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'LU', 'Lung'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'OT', 'Other'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'PA', 'Paediatric' -- Original report RPT_REF_TREAT excluded Paediatrics
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'SA', 'Sarcoma'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'SK', 'Skin'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'UG', 'Upper GI'
exec BuildCANTTreatedWithin62DaysOfScreeningServiceReferral 'UR', 'Urology'
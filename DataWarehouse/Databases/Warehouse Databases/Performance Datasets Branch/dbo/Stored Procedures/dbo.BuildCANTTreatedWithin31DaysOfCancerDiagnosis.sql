﻿CREATE procedure [dbo].[BuildCANTTreatedWithin31DaysOfCancerDiagnosis](@type char(2), @site varchar(100)) as

-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where Numerator = 'CANT patient receiving first definitive treatment < 31 Days of cancer diagnosis - ' + @type)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values ('CANT patient receiving first definitive treatment < 31 Days of cancer diagnosis - ' + @type, 'Numerator.CANTTreatedWithin31DaysOfCancerDiagnosis' + @type)
	select @NumeratorID = scope_identity()
end


-- Insert Denominator record, if it does not exist
declare @DenominatorID int = (select DenominatorID from Denominator where Denominator = 'CANT first definitive treatment for cancer - ' + @type)

if @DenominatorID is null
begin
	insert into Denominator (Denominator, DenominatorLogic)
	values ('CANT first definitive treatment for cancer - ' + @type, 'Denominator.CANTFirstDefinitiveTreatmentForCancer' + @type)
	select @DenominatorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = 
(select ItemID from Item 
	where Item = 'Patients receiving first definitive treatment within one month (31 Days) of a cancer diagnosis (measured from ‘date of decision to treat’) - ' + @site)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values ('Patients receiving first definitive treatment within one month (31 Days) of a cancer diagnosis (measured from ‘date of decision to treat’) - ' + @site
		,2, @NumeratorID, @DenominatorID, 1, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = @DenominatorID where ItemID = @ItemID
end


-- Create view DenominatorCANTFirstDefinitiveTreatmentForCancer with a suffix for the given CancerTypeCode

declare @view varchar(100) = 'Denominator.CANTFirstDefinitiveTreatmentForCancer' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
declare @sql varchar(max) = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_DEC_TREAT

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.TreatmentDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = ''' + @view + '''
	
where DatasetCode = ''CANT''
and TreatmentEventTypeCode in (''01'',''07'')
and TumourStatusCode not in (''3'', ''6'', ''7'', ''8'', ''9'')
and (InappropriateReferralCode <> 1 or InappropriateReferralCode is null) 

and 
(
	coalesce(HistologyCode,'''') not in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'') --Exclude basal cell carcinoma
	and -- Exclude Carcinoma in Situ if not breast DO5
		case 
			when PrimaryDiagnosisCode = ''D05'' and HistologyCode = ''M80102'' 
			then 1
			when coalesce(HistologyCode,'''') = ''M80102''
			then 0
			else 1
		end = 1	
)

and	CancerSite <> ''Spinal Cord Compression''

and 
(
	PrimaryDiagnosisCode = ''D05'' or coalesce(PrimaryDiagnosisCode,'''') not like ''D%''
)

and CancerSite = ''' + @site + ''''

exec(@sql)


-- Create view NumeratorCANTTreatedWithin31DaysOfCancerDiagnosis with a suffix for the given CancerTypeCode

set @view = 'Numerator.CANTTreatedWithin31DaysOfCancerDiagnosis' + @type

--IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(@view)) exec('drop view ' + @view)
if object_id(@view) is not null exec('drop view ' + @view) exec('drop view ' + @view)
	
set @sql = 
'
CREATE view ' + @view + ' as

-- Based on SP CancerRegister.dbo.RPT_DEC_TREAT

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.TreatmentDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = ''' + @view + '''
	
where DatasetCode = ''CANT''
and TreatmentEventTypeCode in (''01'',''07'')
and TumourStatusCode not in (''3'', ''6'', ''7'', ''8'', ''9'')
and (InappropriateReferralCode <> 1 or InappropriateReferralCode is null) 

and 
(
	coalesce(HistologyCode,'''') not in (''M80903'',''M80913'',''M80923'',''M80933'',''M80943'',''M80953'',''M81103'') --Exclude basal cell carcinoma
	and -- Exclude Carcinoma in Situ if not breast DO5
		case 
			when PrimaryDiagnosisCode = ''D05'' and HistologyCode = ''M80102'' 
			then 1
			when coalesce(HistologyCode,'''') = ''M80102''
			then 0
			else 1
		end = 1	
)

and	CancerSite <> ''Spinal Cord Compression''

and 
(
	PrimaryDiagnosisCode = ''D05'' or coalesce(PrimaryDiagnosisCode,'''') not like ''D%''
)

and
(
	-- Calculate adjusted days between decision to treat and treatment
	-- If this is less than 0, use 0
	case 
		when datediff(d,DecisionToTreatDate,TreatmentDate) - 
			(
				case 
					when TreatmentSettingCode in (''01'',''02'') and DecisionToTreatAdjustmentReasonCode = 8 
					then coalesce(DecisionToTreatAdjustment,0) 
					else 0 
				end
			) > 0
		then datediff(d,DecisionToTreatDate,TreatmentDate) -  
			(
				case 
					when TreatmentSettingCode in (''01'',''02'') and DecisionToTreatAdjustmentReasonCode = 8 
					then coalesce(DecisionToTreatAdjustment,0) 
					else 0 
				end
			) 
		else 0
	end
) <= 31 -- Must be less than or equal to 31

and CancerSite = ''' + @site + ''''

exec(@sql)




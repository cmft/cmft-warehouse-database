﻿


CREATE proc [dbo].[BuildFactTargetSpecialty]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)



truncate table dbo.FactTargetSpecialty
	

insert into dbo.FactTargetSpecialty
(
	 HierarchyID
	,TargetID
	,DateID
	,SpecialtyID
	,Value
)

select
	 TargetRuleBase.HierarchyID
	,Target.TargetID
	,Calendar.DateID 
	,SpecialtyID = Specialty.SourceSpecialtyID
	,TargetRuleBase.Value
from
	dbo.Calendar
	
inner join dbo.Target
on	Target.TargetLogic is null

inner join dbo.TargetRuleBase
on	TargetRuleBase.TargetID = Target.TargetID
and	TargetRuleBase.StartDate <= Calendar.TheDate
and not exists
		(
		select
			1
		from
			dbo.TargetRuleBase LaterRule
		where
			LaterRule.TargetID = TargetRuleBase.TargetID
		and	LaterRule.NationalSpecialtyCode = TargetRuleBase.NationalSpecialtyCode
		and	LaterRule.HierarchyID = TargetRuleBase.HierarchyID
		and	LaterRule.StartDate <= Calendar.TheDate
		and	LaterRule.StartDate > TargetRuleBase.StartDate
		)

inner join dbo.Specialty
on	Specialty.NationalSpecialtyCode = TargetRuleBase.NationalSpecialtyCode

where
	TheDate < getdate()

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

﻿CREATE proc [dbo].[BuildModel]

as

exec dbo.BuildManuallyCollatedDataObjectsAll

exec dbo.BuildFactDenominator
exec dbo.BuildFactNumerator
exec dbo.BuildFactTarget
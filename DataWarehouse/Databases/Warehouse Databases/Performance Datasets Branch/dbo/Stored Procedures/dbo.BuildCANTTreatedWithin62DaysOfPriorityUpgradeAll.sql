﻿create procedure [dbo].[BuildCANTTreatedWithin62DaysOfPriorityUpgradeAll] as

exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'CO', 'Colorectal'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'GY', 'Gynaecology'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'HA', 'Haematology'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'HN', 'Head and Neck'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'LU', 'Lung'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'OT', 'Other'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'PA', 'Paediatric'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'SA', 'Sarcoma'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'SK', 'Skin'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'UG', 'Upper GI'
exec BuildCANTTreatedWithin62DaysOfPriorityUpgrade 'UR', 'Urology'
﻿create procedure [dbo].[BuildLITClaimByCategory] 
(
	@item varchar(255)
	,@categoryType varchar(255)
	,@categoryTypeCode varchar(255)
)

as

declare @view varchar(255) = 'LITClaim' + @categoryType
declare @numeratorLogic varchar(255) = 'Numerator.' + @view
declare @numerator varchar(255) = 'LIT Claim - ' + @categoryType


-- Insert Numerator record, if it does not exist
declare @NumeratorID int = (select NumeratorID from Numerator where NumeratorLogic = @numeratorLogic)

if @NumeratorID is null
begin
	insert into Numerator (Numerator, NumeratorLogic)
	values (@numerator,@numeratorLogic)
	select @NumeratorID = scope_identity()
end


-- Insert Item, if it does not exist
declare @ItemID int = (select ItemID from Item where Item = @item)

if @ItemID is null
begin
	insert into Item (Item, ItemTypeID, NumeratorID, DenominatorID, MeasureID, Active)
	values (@item, 2, @NumeratorID, null, 4, 1)
end

else
begin
	update Item set NumeratorID = @NumeratorID, DenominatorID = null where ItemID = @ItemID
end


-- Create view
declare @sql varchar(max) = ('if exists (select 1 from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA = ''Numerator'' and TABLE_NAME = ''' + @view + ''')
    drop view Numerator.' + @view)

exec (@sql)

set @sql =
'
	create view Numerator.' + @view + ' as

	select
		DatasetID = D.DatasetID
		,DatasetRecno = D.DatasetRecno
		,NumeratorID = Numerator.NumeratorID
		,SiteID = D.EndSiteID		
		,DirectorateID = D.EndDirectorateID
		,SpecialtyID = D.SpecialtyID
		,ClinicianID = D.ClinicianID
		,DateID = D.AdvisedDateID
		,D.ServicePointID
		,Value = 1
	from WrkDataset D

	inner join dbo.Dataset
	on	Dataset.DatasetID = D.DatasetID

	inner join dbo.Numerator
	on	NumeratorLogic = ''Numerator.' + @view + '''
		
	where Dataset.DatasetCode = ''LIT''
	and D.CategoryTypeCode collate Latin1_General_CS_AI = ''' + @categoryTypeCode + '''
'

exec (@sql)
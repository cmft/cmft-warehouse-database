﻿EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO

EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\SQL.Service.BA';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Sec - Warehouse BI Developers';


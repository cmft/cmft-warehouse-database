﻿CREATE ROLE [NarrativeWriter]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'NarrativeWriter', @membername = N'CMMC\Helen.Shackleton';


GO
EXECUTE sp_addrolemember @rolename = N'NarrativeWriter', @membername = N'CMMC\peter.graham';


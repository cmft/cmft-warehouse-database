﻿

create view Numerator.RTTPathwayClosedAdmittedNonBreach as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RTTPathwayClosedAdmittedNonBreach'

where
	Dataset.DatasetCode = 'RTT'
and WrkDataset.PathwayStatusCode = 'ACS'
and	WrkDataset.BreachStatusCode = 'N'
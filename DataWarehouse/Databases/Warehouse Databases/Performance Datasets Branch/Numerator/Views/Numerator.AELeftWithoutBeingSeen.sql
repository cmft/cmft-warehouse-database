﻿CREATE view [Numerator].[AELeftWithoutBeingSeen] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EncounterDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AELeftWithoutBeingSeen'
	
where Dataset.DatasetCode = 'AES'
and d.StageCode = 'INDEPARTMENTADJUSTED'
and d.LeftWithoutBeingSeen <> 0

﻿
CREATE view [Numerator].[ExpectedDateOfDischWithin24Hours]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID = d.StartServicePointID
	,Value = 1
	
from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ExpectedDateOfDischWithin24Hours'

where
	Dataset.DatasetCode = 'APC'
and
	d.FirstEpisodeInSpellIndicator = 1
and
	d.DischargeTime is null
and
	datediff(hh, getdate(), d.ExpectedDateOfDischarge) between 0 and 24


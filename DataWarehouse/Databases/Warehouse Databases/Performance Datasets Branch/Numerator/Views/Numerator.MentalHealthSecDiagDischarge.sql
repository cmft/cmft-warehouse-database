﻿CREATE view [Numerator].[MentalHealthSecDiagDischarge]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.MentalHealthSecDiagDischarge'

where
	Dataset.DatasetCode = 'APC'
and
	d.NationalLastEpisodeInSpellCode = 1
and
	left(d.SecondaryDiagnosisCode1,1) = 'F'

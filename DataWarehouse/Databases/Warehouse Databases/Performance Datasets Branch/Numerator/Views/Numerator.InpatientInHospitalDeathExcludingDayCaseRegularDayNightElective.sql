﻿

CREATE view [Numerator].[InpatientInHospitalDeathExcludingDayCaseRegularDayNightElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1
from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InpatientInHospitalDeathExcludingDayCaseRegularDayNightElective'

where
	DatasetCode = 'APC'
and	NationalDischargeMethodCode in ('4','5')
and	NationalLastEpisodeInSpellCode = 1
and	NationalPatientClassificationCode not in ('2','3','4')
and	PatientCategoryCode = 'El'



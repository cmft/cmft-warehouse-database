﻿







CREATE view [Numerator].[HRBMELongTermAppointment]
as

-- 20160119	RR	A change was made to the HR file.  Jonathan Hodges advised that the indicator should remain the same but the calculation should be Headcounter Period End / Headcount Period Start

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.BMELongTermAppointment
	--,Value = BMEHeadcountPeriodEnd
from
	dbo.WrkDataset 
inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HRBMELongTermAppointment'

where
	Dataset.DatasetCode = 'HR'
--and BMEHeadcountPeriodEnd is not null
and WrkDataset.BMELongTermAppointment is not null







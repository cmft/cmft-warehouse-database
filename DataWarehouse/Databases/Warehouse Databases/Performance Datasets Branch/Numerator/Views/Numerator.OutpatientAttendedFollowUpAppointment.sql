﻿CREATE view [Numerator].[OutpatientAttendedFollowUpAppointment]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.StartSiteID   
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AppointmentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OutpatientAttendedFollowUpAppointment'

where
	Dataset.DatasetCode = 'OP'
and
	NationalAttendanceStatusCode in ('5','6') -- Attended
and
	NationalFirstAttendanceCode = '2' -- Follow-Up Appointment

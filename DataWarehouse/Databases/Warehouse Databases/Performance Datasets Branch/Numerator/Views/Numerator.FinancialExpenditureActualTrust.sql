﻿




CREATE view [Numerator].[FinancialExpenditureActualTrust] as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.FinanceActual - WrkDataset.FinanceBudget 

/*
	Change requested by S.Maloney 20.10.14. Agreed by G.Summerfield. 
	For some reason the definition of calculation changes at trust level??
*/

from dbo.WrkDataset 

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialExpenditureActualTrust'
	
where
	Dataset.DatasetCode = 'FINANCE'
and EndDirectorateID = 15 -- Trust

--select distinct EndDirectorateID,Division from WrkDataset where DatasetID = 39 and Division is not null





﻿






CREATE view [Numerator].[EmergencyReadmissionWithin28DaysElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1

from dbo.WrkDataset 

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.EmergencyReadmissionWithin28DaysElective'

where
	Dataset.DatasetCode = 'APC'
and WrkDataset.Readmission28Day = 1
and WrkDataset.PatientCategoryCode = 'EL'
﻿

CREATE view [Numerator].[IncidentGrade4Or5] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentGrade4Or5'

where
	Dataset.DatasetCode = 'INC'
and	WrkDataset.IncidentGradeCode in --('0x4520','0x4120') -- Harm Grade = 4 or 5
									(
									'0x4520' --4 Major
									,'0x4A20' --4U Major Unconfirmed
									,'0x4920' --4C Major
									,'0x4F20' --4# Nof
									,'0x4120' --5 Catastrophic
									,'0x4C20' --5U Catastrophic Unconfirmed
									,'0x4B20' --5C Catastophic
									) --CH 07012015 Grading changed
and SourceDepartmentCode not in    (  -- These are community department codes    
                                                '0x6749'
                                                ,'0x204A'
                                                ,'0x3949'
                                                ,'0x674C'
                                                ,'0x6949'
                                                ,'0x5544'
                                                ,'0x414D'
                                                ,'0x424D'
                                                ,'0x434D'
                                                ,'0x4D4E'
                                                )



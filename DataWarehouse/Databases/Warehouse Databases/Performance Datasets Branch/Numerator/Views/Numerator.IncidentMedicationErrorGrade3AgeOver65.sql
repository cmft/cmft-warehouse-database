﻿

CREATE view [Numerator].[IncidentMedicationErrorGrade3AgeOver65]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade3AgeOver65'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		IncidentTypeCode = '0x5120' 
	or	IncidentCauseGroupCode = '0x4620'
	) -- Medication Error

and	WrkDataset.IncidentGradeCode = '0x4420' -- Harm Grade = 3
and	WrkDataset.IncidentInvolvingElderly = 1



﻿




CREATE view [Numerator].[HarmFreeCarePressureUlcerCategory2OrOver] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IdentifiedDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCarePressureUlcerCategory2OrOver'
	
where
	DatasetCode = 'PREULC'
and	datediff(hour, AdmissionTime, IdentifiedTime) > 72
and WrkDataset.SourcePressureUlcerCategoryCode > 1







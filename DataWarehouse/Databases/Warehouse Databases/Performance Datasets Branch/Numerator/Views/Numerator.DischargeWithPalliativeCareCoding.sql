﻿CREATE view [Numerator].[DischargeWithPalliativeCareCoding]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.DischargeWithPalliativeCareCoding'
	
where
	DatasetCode = 'APC'
and
	NationalLastEpisodeInSpellCode = 1
and
	DischargeDate is not null
and
	exists
			(
				select	
					1
				from
					dbo.WrkDataset Spell		
				where
					Spell.PalliativeCare = 1
				and
					Spell.ProviderSpellNo = WrkDataset.ProviderSpellNo
				and
					Spell.ContextCode = WrkDataset.ContextCode
			)
and
	PatientCategoryCode not in (
								'DC'
								,'RD'
								)

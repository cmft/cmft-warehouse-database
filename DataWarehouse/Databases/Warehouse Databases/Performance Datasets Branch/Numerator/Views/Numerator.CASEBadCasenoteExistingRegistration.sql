﻿



CREATE view [Numerator].[CASEBadCasenoteExistingRegistration] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AllocatedDateID
	,WrkDataset.AllocatedDate
	,WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CASEBadCasenoteExistingRegistration'
	
where
	Dataset.DatasetCode = 'CASE'

and -- Bad Casenote Number: does not begin with 'M' or begins with 'M' but earlier extant number exists also beginning with 'M'

	(
		(
		left(WrkDataset.CasenoteNumber,1) <> 'M'
		and WrkDataset.ExistingRegistration = 1
		--and exists -- Existing Registration = at least one previously allocated Casenote Number
		--	(
		--		select 1
		--		from WrkDataset D1
		--		where D1.SourcePatientNo = D.SourcePatientNo
		--		and D1.DatasetRecno <> D.DatasetRecno
		--		and	D1.DatasetID = D.DatasetID
		--		and D1.AllocatedDate < D.AllocatedDate
		--	)
		) 

		or
		-- all this needs work...
		(
			Left(WrkDataset.CasenoteNumber,1) = 'M'
		and exists -- Earlier extant alocated Casenote Number beginning with 'M'
			(
				select 1
				from WrkDataset D1
				where D1.SourcePatientNo = WrkDataset.SourcePatientNo
				and D1.DatasetRecno <> WrkDataset.DatasetRecno
				and	D1.DatasetID = WrkDataset.DatasetID
				and Left(D1.CasenoteNumber,1) = 'M'
				and D1.AllocatedDate < WrkDataset.AllocatedDate -- Allocated before
				and -- Not withdrawn at time of new allocation
				(
					D1.WithdrawnDate is null
					or D1.WithdrawnDate > WrkDataset.AllocatedDate
				)
			)
		)
	)




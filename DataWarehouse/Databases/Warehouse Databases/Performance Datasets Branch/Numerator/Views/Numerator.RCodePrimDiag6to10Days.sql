﻿
CREATE view [Numerator].[RCodePrimDiag6to10Days]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(WrkDataset.EndSiteID,WrkDataset.StartSiteID)
	,DirectorateID = coalesce(WrkDataset.EndDirectorateID,WrkDataset.StartDirectorateID)
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeEndDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RCodePrimDiag6to10Days'

where
	DatasetCode = 'APC'
and
	WrkDataset.ClinicalCodingCompleteDate is not null
and
	left(WrkDataset.PrimaryDiagnosisCode, 1) = 'R'
and
	datediff(dd,EpisodeStartDate, EpisodeEndDate) between 6 and 10


﻿



CREATE view [Numerator].[OccupiedInpatientBeds] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DateID
	,ServicePointID
	,Value = WrkDataset.BedOccupancyNights

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OccupiedInpatientBeds'
	
where 
	Dataset.DatasetCode = 'BEDOCC'
and WrkDataset.BedOccupancyNights is not null
and WrkDataset.AvailableInpatientBeds is null




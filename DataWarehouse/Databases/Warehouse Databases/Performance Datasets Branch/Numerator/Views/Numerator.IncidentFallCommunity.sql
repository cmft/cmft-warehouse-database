﻿

Create view [Numerator].[IncidentFallCommunity]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1
from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentFallCommunity'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		WrkDataset.IncidentCauseGroupCode = '0x3120' 
	or	WrkDataset.IncidentCause1Code in ('0x7042','0x7542','0x7142','0x5220','0x7342','0x7242')
	) -- Falls
and SourceDepartmentCode in    (  -- These are community department codes    
                                                '0x6749'
                                                ,'0x204A'
                                                ,'0x3949'
                                                ,'0x674C'
                                                ,'0x6949'
                                                ,'0x5544'
                                                ,'0x414D'
                                                ,'0x424D'
                                                ,'0x434D'
                                                ,'0x4D4E'
                                                )
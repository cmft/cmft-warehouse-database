﻿




CREATE view [Numerator].[PreoperativeLengthOfStayElective]

as


select 
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = datediff(day, WrkDataset.AdmissionDate, WrkDataset.FirstOperationInSpellTime)
from
	dbo.WrkDataset

inner join	dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.PreoperativeLengthOfStayElective'

where
	Dataset.DatasetCode = 'APC'
and WrkDataset.PatientCategoryCode = 'EL'
and	WrkDataset.FirstEpisodeInSpellIndicator = 1
and WrkDataset.FirstOperationInSpellTime is not null
and WrkDataset.DischargeTime is not null
﻿
CREATE view Numerator.ManualItemID900 as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DateID
	,ServicePointID = D.ServicePointID
	,Value = D.Numerator

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ManualItemID900'
	
where
	Dataset.DatasetCode = 'MAN'
and ItemID = '900'
﻿

CREATE view [Numerator].[NursingWorkforceNonRegisteredNurseActualDayHours] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.NonRegisteredNurseActual

from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NursingWorkforceNonRegisteredNurseActualDayHours'
	
where
	Dataset.DatasetCode = 'STAFFLEVEL'
and WrkDataset.SourceShift = 'Dhours'









﻿CREATE view [Numerator].[COMPatientIdFieldsEntered]
as

-- Check each of 6 patient id fields is entered correctly
select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ContactDateID
	,ServicePointID
	,Value = -- Count 1 for each of the 6 fields correctly filled in
		case when d.NHSNumber is null then 0 else 1 end +
		case when d.DateOfBirth is null then 0 else 1 end +
		case when d.Postcode is null then 0 else 1 end +
		case when d.RegisteredGPPracticeCode is null then 0 else 1 end +
		case when d.CommissionerCode is null then 0 else 1 end +
		case when d.SexCode in ('9269','9270') then 1 else 0 end

from dbo.WrkDataset d 

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.COMPatientIdFieldsEntered'
	
where
	Dataset.DatasetCode = 'COM'

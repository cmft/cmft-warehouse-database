﻿



CREATE view [Numerator].[SpellRCodePrimDiag1stand2ndEpisode]

as

select --top 10
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(WrkDataset.EndSiteID,WrkDataset.StartSiteID)
	,DirectorateID = coalesce(WrkDataset.EndDirectorateID,WrkDataset.StartDirectorateID)
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.EpisodeEndDateID
	,ServicePointID
	,Value = 1

from 
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.SpellRCodePrimDiag1stand2ndEpisode'

where
	DatasetCode = 'APC'
and
	WrkDataset.FirstEpisodeInSpellIndicator = 1
and
	WrkDataset.ClinicalCodingCompleteDate is not null
and
	left(WrkDataset.PrimaryDiagnosisCode, 1) = 'R'
and
	NationalPatientClassificationCode not in ('2','3','4')
and
	exists
		(
		select
			1
		from
			dbo.WrkDataset SecondEpisode
		where
			SecondEpisode.ProviderSpellNo = WrkDataset.ProviderSpellNo
		and 
			SecondEpisode.EpisodeNo = 2
		and
			left(WrkDataset.PrimaryDiagnosisCode, 1) = 'R'
		)



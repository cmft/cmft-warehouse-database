﻿CREATE view [Numerator].[ElectiveInfraRenalAneurisms]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ElectiveInfraRenalAneurisms'

where
	Dataset.DatasetCode = 'APC'
and
	d.FirstEpisodeInSpellIndicator = 1
and
	d.PatientCategoryCode = 'EL'
AND d.NationalSpecialtyCode = '107' 
AND left(d.PrimaryProcedureCode,3) in ('L18', 'L19', 'L27', 'L28')

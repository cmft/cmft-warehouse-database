﻿




CREATE view [Numerator].[IncidentMedicationErrorGrade4To5]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade4To5'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		IncidentTypeCode = '0x5120' 
	or	IncidentCauseGroupCode = '0x4620'
	) -- Medication Error
and	WrkDataset.IncidentGradeCode in --('0x4520', '0x4120') -- Harm Grade = 4 or 5
									(
									'0x4520' --4 Major
									,'0x4A20' --4U Major Unconfirmed
									,'0x4920' --4C Major
									,'0x4F20' --4# Nof
									,'0x4120' --5 Catastrophic
									,'0x4C20' --5U Catastrophic Unconfirmed
									,'0x4B20' --5C Catastophic
									) --CH 07012015 Grading changed

union

--passes a value in as there has been no incidents in the last 2 years CH 15012015

select
	DatasetID
	,DatasetRecno = -1
	,NumeratorID
	,SiteID
	,DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = ( 
				select 
					DateID
				from 
					dbo.CalendarBase 
				where
					TheDate =	cast(DATEADD(ss, -1, DATEADD(month, DATEDIFF(month, 0, getdate()), 0)) as date)
				) 
	,ServicePointID
	,Value
from
	(
	select top 1
		DatasetID = WrkDataset.DatasetID
		,NumeratorID = Numerator.NumeratorID
		,SiteID = WrkDataset.EndSiteID		
		,DirectorateID = WrkDataset.EndDirectorateID
		,SpecialtyID = WrkDataset.SpecialtyID
		,ClinicianID = WrkDataset.ClinicianID
		,ServicePointID
		,Value = 0

	from dbo.WrkDataset WrkDataset

	inner join dbo.Dataset
	on Dataset.DatasetCode = 'INC'
	
	inner join dbo.Numerator
	on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade4To5'


	) Top1



﻿create view [Numerator].[RENQTransplantWaitListAgeGe70] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.RENQTransplantWaitListAgeGe70'
	
where DatasetCode = 'RENQ'
and D.ModalityCode = '90:55425' -- HD
and D.ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and D.EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients', 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and (D.DateOfDeath is null or D.DateOfDeath > D.LastDayOfQuarter) -- Include only patients alive at quarter end
and D.TransplantListStatusCode in ('90:56505', '90:56506', '90:56599') -- 'TransplantList : Active', 'TransplantList: ActiveStatusUpdate', 'TransplantList : Temp Suspended'
and convert(int,round(datediff(hour,D.DateOfBirth,D.LastDayOfQuarter)/8766,0)) >= 70

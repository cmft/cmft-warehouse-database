﻿


CREATE view [Numerator].[InpatientNonElectiveAdmissionsViaAELoS72hrs]

as

select
       DatasetID = WrkDataset.DatasetID
       ,DatasetRecno = WrkDataset.DatasetRecno
       ,NumeratorID = Numerator.NumeratorID
       ,SiteID = WrkDataset.EndSiteID
       ,DirectorateID = WrkDataset.EndDirectorateID
       ,SpecialtyID = WrkDataset.SpecialtyID
       ,ClinicianID = WrkDataset.ClinicianID
       ,DateID = WrkDataset.DischargeDateID
       ,ServicePointID = WrkDataset.StartServicePointID
       ,Value = 1
from
       dbo.WrkDataset
inner join
       dbo.Dataset Dataset
on     WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on     Numerator.NumeratorLogic = 'Numerator.InpatientNonElectiveAdmissionsViaAELoS72hrs'

where
       DatasetCode = 'APC'
and
       NationalLastEpisodeInSpellCode = 1 -- why not first?  Keeping as is to keep consitent with other queries
and
       PatientCategoryCode = 'NE'
and
       AdmissionMethodCode = 21
and 
       datediff(mi,AdmissionDate,DischargeDate)<4320 -- <72hrs ie considered shortStay
and 
       DischargeDate is not null




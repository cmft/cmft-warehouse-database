﻿
CREATE view [Numerator].[NeonatalROPScreenInWindow] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID
	,Value = 1	

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.NeonatalROPScreenInWindow'
	
where DatasetCode = 'NEO'
and 
	(
		GestationWeeks < 32
		or	BirthWeight < 1501
	)
and	ROPScreenDueDate is not null
and ROPScreenDate is not null
and 
	(-- Screening window is between 14 days before due date and 7 days after
		datediff(day, ROPScreenDate, ROPScreenDueDate) between 0 and 14
		or datediff(day, ROPScreenDueDate, ROPScreenDate) between 0 and 7
	)
and DischargeDestinationCode in ('1','2','4') -- Discharged not to another hospital


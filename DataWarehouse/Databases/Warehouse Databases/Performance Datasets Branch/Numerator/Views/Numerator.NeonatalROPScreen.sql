﻿CREATE view [Numerator].[NeonatalROPScreen] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1	

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.NeonatalROPScreen'
	
where DatasetCode = 'NEO'
and 
	(
		GestationWeeks < 32
		or	BirthWeight < 1501
	)
and ROPScreenDate is not null
and DischargeDestinationCode in ('1','2','4') -- Discharged not to another hospital
and DischargeDate is not null

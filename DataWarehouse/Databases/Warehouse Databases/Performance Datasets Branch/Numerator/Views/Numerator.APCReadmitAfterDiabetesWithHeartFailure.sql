﻿
CREATE view [Numerator].[APCReadmitAfterDiabetesWithHeartFailure] as

-- Select Spell A which has a later Spell B within 12 months for the same Patient
select
	DatasetID = A.DatasetID
	,DatasetRecno = A.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = A.EndSiteID		
	,DirectorateID = A.EndDirectorateID
	,SpecialtyID = A.SpecialtyID
	,ClinicianID = A.ClinicianID
	,DateID = A.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset A

inner join dbo.Dataset
on	Dataset.DatasetID = A.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.APCReadmitAfterDiabetesWithHeartFailure'

where
	A.NationalLastEpisodeInSpellCode = 1 
and	Dataset.DatasetCode = 'APC'

	-- Look for a later Spell B
	and exists
	(
		select
			1 
		from
			WrkDataset B
		where
			A.DatasetID = B.DatasetID 
		--and A.NationalLastEpisodeInSpellCode = 1
		and B.SourcePatientNo = A.SourcePatientNo
		and B.AdmissionTime > A.DischargeTime
		and datediff(month,A.DischargeTime,B.AdmissionTime) < 12
	)

and left(A.PrimaryDiagnosisCode,3) = 'I50' -- Heart failure
and
(
	left(A.SecondaryDiagnosisCode1,3) between 'E08' and 'E13' -- Diabetes
	or left(A.SecondaryDiagnosisCode2,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode3,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode4,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode5,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode6,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode7,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode8,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode9,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode10,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode11,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode12,3) between 'E08' and 'E13'
	or left(A.SecondaryDiagnosisCode13,3) between 'E08' and 'E13'			
)


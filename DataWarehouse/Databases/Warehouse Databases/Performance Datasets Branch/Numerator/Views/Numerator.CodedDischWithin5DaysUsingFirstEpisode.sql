﻿CREATE view [Numerator].[CodedDischWithin5DaysUsingFirstEpisode]

as

-- Count Spells in which all Episodes are coded within 5 days of discharge

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CodedDischWithin5DaysUsingFirstEpisode'
	
where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	DischargeTime is not null
and
	ClinicalCodingCompleteDate is not null
and
not exists -- any Episode in Spell not coded within 5 days of discharge
	(
		select 1 from dbo.WrkDataset B
		where B.DatasetID = WrkDataset.DatasetID -- Same Dataset
		and B.DatasetRecno <> WrkDataset.DatasetRecno -- Different Episode
		and B.ProviderSpellNo = WrkDataset.ProviderSpellNo -- Same Spell
		and datediff(day, B.DischargeDate, B.ClinicalCodingCompleteDate) > 5 -- Clin coding took > 5 days
	)

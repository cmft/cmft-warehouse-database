﻿
CREATE view [Numerator].[InpatientLengthOfSpellUsingFirstEpisode]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = datediff(day, WrkDataset.AdmissionDate, WrkDataset.DischargeDate)

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InpatientLengthOfSpellUsingFirstEpisode'
	
where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1
and
	DischargeDate is not null

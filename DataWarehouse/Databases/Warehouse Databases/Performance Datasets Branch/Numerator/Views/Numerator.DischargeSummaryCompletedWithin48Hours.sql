﻿







CREATE view [Numerator].[DischargeSummaryCompletedWithin48Hours]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.DischargeSummaryCompletedWithin48Hours'
	
where
	DatasetCode = 'APC'
and WrkDataset.NationalLastEpisodeInSpellCode = 1
and	WrkDataset.DischargeSummaryRequired = 1
and datediff(minute, WrkDataset.DischargeTime, WrkDataset.DischargeSummarySignedTime) <= 2880
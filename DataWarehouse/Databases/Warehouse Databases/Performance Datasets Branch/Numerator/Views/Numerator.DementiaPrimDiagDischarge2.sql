﻿CREATE view [Numerator].[DementiaPrimDiagDischarge2]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.DementiaPrimDiagDischarge2'

where
	Dataset.DatasetCode = 'APC'
and
	d.NationalLastEpisodeInSpellCode = 1
and
	(
	left(d.PrimaryDiagnosisCode,3) = 'F00' or
	left(d.PrimaryDiagnosisCode,3) = 'F01' or
	left(d.PrimaryDiagnosisCode,3) = 'F02' or
	left(d.PrimaryDiagnosisCode,3) = 'F03' or
	PrimaryDiagnosisCode in ('F10.7', 'F11.7', 'F12.7', 'F13.7', 'F14.7', 'F15.7', 'F16.7', 'F17.7', 'F18.7', 'F19.7')
	)

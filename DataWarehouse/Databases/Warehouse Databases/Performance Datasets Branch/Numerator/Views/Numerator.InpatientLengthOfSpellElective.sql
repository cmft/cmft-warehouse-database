﻿








CREATE view [Numerator].[InpatientLengthOfSpellElective]

as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = datediff(day, WrkDataset.AdmissionDate, WrkDataset.DischargeDate)
from 
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InpatientLengthOfSpellElective'

where
	DatasetCode = 'APC'
and	FirstEpisodeInSpellIndicator = 1  -- To only count the spell LoS, and no duplicates as table is at episode level
--and	NationalPatientClassificationCode not in ('2','3','4') -- Exclude DC, RegDay, RegNight
and	DischargeDate is not null -- LoS, only need completed spells
and	NationalDischargeMethodCode not in (5) -- Exclude Stillbirth
--and WrkDataset.HasAssessmentUnitEpisode = 0 --Not sure why this would be in CH Removed 29/09/2015
and	WrkDataset.IsWellBabySpell = 0
and WrkDataset.AdmissionMethodCode in ('11' , '12' , '13') --Elective







﻿


CREATE view [Numerator].[IncidentFallGrade1To3]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentFallGrade1To3'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		WrkDataset.IncidentCauseGroupCode = '0x3120' 
	or	WrkDataset.IncidentCause1Code in ('0x7042','0x7542','0x7142','0x5220','0x7342','0x7242')
	) -- Falls
and WrkDataset.IncidentGradeCode in --('0x4220','0x4320','0x4420') -- Harm Grade = 1, 2 or 3
									(
									'0x4220' --1 No Harm
									,'0x4D20' --1U No Harm Unconfirmed
									,'0x4320' --2 Slight
									,'0x4420' --3 Moderate
									) --CH 07012015 Grading changed


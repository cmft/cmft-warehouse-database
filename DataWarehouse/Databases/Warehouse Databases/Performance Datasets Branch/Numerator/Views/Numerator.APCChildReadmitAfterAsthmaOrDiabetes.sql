﻿CREATE view [Numerator].[APCChildReadmitAfterAsthmaOrDiabetes] as

-- Select Spell A which has a later Spell B within 12 months for the same Patient
select
	DatasetID = A.DatasetID
	,DatasetRecno = A.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = A.EndSiteID		
	,DirectorateID = A.EndDirectorateID
	,SpecialtyID = A.SpecialtyID
	,ClinicianID = A.ClinicianID
	,DateID = A.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset A

inner join dbo.Dataset
on	Dataset.DatasetID = A.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.APCChildReadmitAfterAsthmaOrDiabetes'

where
	A.NationalLastEpisodeInSpellCode = 1 

	-- Look for a later Spell B
	and exists
	(
		select 1 from WrkDataset B
		where A.DatasetID = B.DatasetID 
		and A.NationalLastEpisodeInSpellCode = 1
		and B.SourcePatientNo = A.SourcePatientNo
		and B.AdmissionTime > A.DischargeTime
		and datediff(month,A.DischargeTime,B.AdmissionTime) < 12
	)
and
	Dataset.DatasetCode = 'APC'

and 
(
	left(A.PrimaryDiagnosisCode,3) = 'J45' -- Asthma
	or left(A.PrimaryDiagnosisCode,3) = 'J46' -- Asthma
	or left(A.PrimaryDiagnosisCode,3) between 'E08' and 'E13' -- Diabetes
)
and
	convert(int,round(datediff(hour,A.DateOfBirth,A.AdmissionTime)/8766,0)) <= 18

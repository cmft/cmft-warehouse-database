﻿CREATE view [Numerator].[CASEBadCasenotePASEpisode] as

select
	D.DatasetID
	,D.DatasetRecno
	,Numerator.NumeratorID
	,D.SiteID	
	,D.DirectorateID
	,D.SpecialtyID
	,D.ClinicianID
	,D.DateID
	,D.ServicePointID
	,D.Value
from 
(
	-- Construct list of PAS Episodes with bad Casenote Numbers as union of ...

	-- OP Referrals
	select
		DatasetID = D.DatasetID
		,D.DatasetRecno
		,SiteID = D.StartSiteID		
		,DirectorateID = D.StartDirectorateID
		,SpecialtyID = D.SpecialtyID
		,ClinicianID = D.ClinicianID
		,DateID = ReferralDateID
		,ServicePointID
	,Value = 1
	from WrkDataset D
	inner join dbo.Dataset
	on	Dataset.DatasetID = D.DatasetID
	where DatasetCode = 'RF'
	and left(CasenoteNumber,1) <> 'M'

	union

	-- IP Waiting List Entries
	select
		DatasetID = D.DatasetID
		,D.DatasetRecno
		,SiteID = D.StartSiteID		
		,DirectorateID = D.StartDirectorateID
		,SpecialtyID = D.SpecialtyID
		,ClinicianID = D.ClinicianID
		,DateID = ActivityDateID
		,ServicePointID
	,Value = 1
	from WrkDataset D
	inner join dbo.Dataset
	on	Dataset.DatasetID = D.DatasetID
	where DatasetCode = 'IPWLE'
	and left(CasenoteNumber,1) <> 'M'

	union

	-- IP Admissions, excluding admissions from waiting list
	select
		DatasetID = D.DatasetID
		,D.DatasetRecno
		,SiteID = D.StartSiteID		
		,DirectorateID = D.StartDirectorateID
		,SpecialtyID = D.SpecialtyID
		,ClinicianID = D.ClinicianID
		,DateID = AdmissionDateID
		,ServicePointID
	,Value = 1
	from WrkDataset D

	inner join dbo.Dataset
	on	Dataset.DatasetID = D.DatasetID
	where Dataset.DatasetCode = 'APC'

	and FirstEpisodeInSpellIndicator = 1

	and not exists -- patient was not on waiting list
	(
		select 1 from WrkDataset W
		inner join dbo.Dataset DW
		on	DW.DatasetID = W.DatasetID
		where DW.DatasetCode = 'IPWLE'
		
		-- Join APC to IPWLE on SourcePatientNo and EpisodeNo
		and W.SourcePatientNo = D.SourcePatientNo
		and W.EpisodeNo = D.EpisodeNo
	)
	
	and left(CasenoteNumber,1) <> 'M'
) D

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CASEBadCasenotePASEpisode'


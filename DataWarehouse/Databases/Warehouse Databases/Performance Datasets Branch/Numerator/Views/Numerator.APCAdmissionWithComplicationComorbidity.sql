﻿
CREATE view [Numerator].[APCAdmissionWithComplicationComorbidity] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.APCAdmissionWithComplicationComorbidity'

where Dataset.DatasetCode = 'APC'
--and d.NationalLastEpisodeInSpellCode = 1
and FirstEpisodeInSpellIndicator = 1
and d.DischargeDate is not null
and d.ComplicationComorbidity = '1'--'WITH' -- HRG with complications or co-morbidities
and d.PatientClassificationCode in ('1','2')
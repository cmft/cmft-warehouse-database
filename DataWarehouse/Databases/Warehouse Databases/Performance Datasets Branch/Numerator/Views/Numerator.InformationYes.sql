﻿CREATE view [Numerator].[InformationYes] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.QCRAuditDateID
	,ServicePointID
	,Value = 1
	,QCRCustomListCode

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InformationYes'
	
where
	DatasetCode = 'QCR'
and
	QCRCustomListCode = 19 -- Information
and
	QCRAnswer = 1

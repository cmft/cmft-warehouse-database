﻿



CREATE view [Numerator].[OPWLCLongestWaiter] as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID   
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID -- Census Date
	,ServicePointID = WrkDataset.ServicePointID
	,Value = LongestWait.Value
from 
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.OPWLCLongestWaiter'

inner join --get longest wait by each attribute
	(
	select
		 WrkDataset.DatasetID
		,WrkDataset.StartSiteID
		,WrkDataset.StartDirectorateID
		,WrkDataset.SpecialtyID
		,WrkDataset.ClinicianID
		,WrkDataset.CensusDateID
		,Value = max(WrkDataset.Duration)
	from
		dbo.WrkDataset

	inner join dbo.Dataset Dataset
	on	WrkDataset.DatasetID = Dataset.DatasetID
	and Dataset.DatasetCode = 'OPWLC'

	group by
		 WrkDataset.DatasetID
		,WrkDataset.StartSiteID
		,WrkDataset.StartDirectorateID
		,WrkDataset.SpecialtyID
		,WrkDataset.ClinicianID
		,WrkDataset.CensusDateID
	) LongestWait

on	WrkDataset.StartSiteID = LongestWait.StartSiteID
and	WrkDataset.StartDirectorateID = LongestWait.StartDirectorateID
and	WrkDataset.SpecialtyID = LongestWait.SpecialtyID
and	WrkDataset.ClinicianID = LongestWait.ClinicianID
and	WrkDataset.CensusDateID = LongestWait.CensusDateID

where
	Dataset.DatasetCode = 'OPWLC'
and WrkDataset.NationalFirstAttendanceCode = '1'
﻿

CREATE view [Numerator].[IncidentPressureUlcerGrade3To4]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.IncidentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentPressureUlcerGrade3To4'
	
where
	Dataset.DatasetCode = 'INC'
and WrkDataset.IncidentCause1Code in -- Pressure Ulcer grade 3 & 4
									(
									'0x4D48'
									,'0x4E48'
									,'0x5649'
									,'0x5749'
									--,'0x5A49' -- Community acquired
									--,'0x6149'
									,'0x634C'
									,'0x644C'
									,'0x714C'
									,'0x724C'
									)
and SourceDepartmentCode not in    (  -- These are community department codes    
                                                '0x6749'
                                                ,'0x204A'
                                                ,'0x3949'
                                                ,'0x674C'
                                                ,'0x6949'
                                                ,'0x5544'
                                                ,'0x414D'
                                                ,'0x424D'
                                                ,'0x434D'
                                                ,'0x4D4E'
                                                )

	




﻿
CREATE view [Numerator].[CountOfInpatientDiagCodes]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(d.EndSiteID,d.StartSiteID)
	,DirectorateID = coalesce(d.EndDirectorateID,d.StartDirectorateID)
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EpisodeEndDateID
	,ServicePointID
	,Value = 
		case when PrimaryDiagnosisCode is not null then 1 else 0 end +
		case when SubsidiaryDiagnosisCode is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode1 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode2 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode3 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode4 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode5 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode6 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode7 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode8 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode9 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode10 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode11 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode12 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode13 is not null then 1 else 0 end
from
	dbo.WrkDataset d
inner join
	dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.CountOfInpatientDiagCodes'

where
	Dataset.DatasetCode = 'APC'


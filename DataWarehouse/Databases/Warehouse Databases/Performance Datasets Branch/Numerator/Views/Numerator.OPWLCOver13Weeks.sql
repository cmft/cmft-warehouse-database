﻿


CREATE view [Numerator].[OPWLCOver13Weeks] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.StartSiteID   
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.OPWLCOver13Weeks'

where 
	Dataset.DatasetCode = 'OPWLC'
and WrkDataset.Duration >= 13
and WrkDataset.NationalFirstAttendanceCode = '1'
﻿


CREATE view [Numerator].[CancelledOperation28dayBreach] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = Calendar.DateID --WrkDataset.CancellationDateID CH 20160112 GS agreed it needs to be Breach month not Cancelled Month
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDataset  

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CancelledOperation28dayBreach'

inner join dbo.Calendar
on dateadd(day,29,WrkDataset.CancellationDate) = Calendar.TheDate

where 
	Dataset.DatasetCode = 'CANOP'
and ReportableBreach = 1





﻿

CREATE view [Denominator].[APCChildDischargeWithAsthmaOrDiabetes] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.APCChildDischargeWithAsthmaOrDiabetes'

where
	D.NationalLastEpisodeInSpellCode = 1 
and Dataset.DatasetCode = 'APC'

and (
		left(D.PrimaryDiagnosisCode,3) = 'J45' -- Asthma
	or left(D.PrimaryDiagnosisCode,3) = 'J46' -- Asthma
	or left(D.PrimaryDiagnosisCode,3) between 'E08' and 'E13'
	) -- Diabetes

and	convert(int,round(datediff(hour,D.DateOfBirth,D.AdmissionTime)/8766,0)) <= 18
and DischargeDateID is not null
	



﻿





CREATE view [Denominator].[InpatientElectiveAdmissionElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID -- 02/02/2016 RR This is being used for LoS, was Admission Date, but numerator discharge date, agreed with ML/GS/DG to use Discharge date and be consistent across numerator and denominator.
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1

from
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientElectiveAdmissionElective'

where
	DatasetCode = 'APC'
and
	NationalLastEpisodeInSpellCode = 1
and
	PatientCategoryCode = 'EL'






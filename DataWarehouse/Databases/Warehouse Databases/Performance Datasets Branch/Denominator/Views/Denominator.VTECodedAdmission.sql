﻿





CREATE view [Denominator].[VTECodedAdmission] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.VTECodedAdmission'

where
	Dataset.DatasetCode = 'APC'
and WrkDataset.ClinicalCodingCompleteDate is not null
and	WrkDataset.FirstEpisodeInSpellIndicator = 1
and WrkDataset.VTE = 1









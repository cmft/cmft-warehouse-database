﻿


CREATE view [Denominator].[OPWLCBacklog] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID   
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.OPWLCBacklog'

where 
	Dataset.DatasetCode = 'OPWLC'
and WrkDataset.NationalFirstAttendanceCode = '1'
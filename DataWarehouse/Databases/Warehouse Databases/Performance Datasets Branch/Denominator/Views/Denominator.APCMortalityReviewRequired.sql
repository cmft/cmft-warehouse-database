﻿


CREATE view [Denominator].[APCMortalityReviewRequired] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.APCMortalityReviewRequired'
	
where Dataset.DatasetCode = 'APC'
and MortalityReviewStatusCode in ('0','1','2') -- Available / In Progress / Completed
and d.DischargeDate >= '1 apr 2014'




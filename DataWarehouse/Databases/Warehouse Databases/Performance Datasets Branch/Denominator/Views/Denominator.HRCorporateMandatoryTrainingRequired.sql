﻿




CREATE view [Denominator].[HRCorporateMandatoryTrainingRequired]
as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.CorporateMandatoryTrainingRequired

from
	dbo.WrkDataset 
inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.HRCorporateMandatoryTrainingRequired'

where
	Dataset.DatasetCode = 'HR'
and	WrkDataset.CorporateMandatoryTrainingRequired is not null







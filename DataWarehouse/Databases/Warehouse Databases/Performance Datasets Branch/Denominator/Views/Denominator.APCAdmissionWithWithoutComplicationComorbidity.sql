﻿
CREATE view [Denominator].[APCAdmissionWithWithoutComplicationComorbidity]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.EndSiteID  
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AdmissionDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCAdmissionWithWithoutComplicationComorbidity'

where Dataset.DatasetCode = 'APC'
and d.FirstEpisodeInSpellIndicator = 1
and d.DischargeDate is not null
--and d.HRGWithCCCode in ('WITH','WITHOUT') -- HRG with or without complications or co-morbidities (excludes NONE) -- 26/02/2016 RR Discussed with DG and IC, and denominator description, need to include none, ie denominator all admissions
and d.PatientClassificationCode in ('1','2')
﻿

CREATE view [Denominator].[ActivityPlanNonElectiveSpell] as

select
	 DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = WrkDataset.Denominator

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.ActivityPlanNonElectiveSpell'
	
where
	Dataset.DatasetCode = 'ACTIVITY'
and WrkDataset.ActivityMetricCode in ('EMSPL'
									 ,'MATSPL'
									 ,'NEOTHSPL') -- Non-Elective Spells
and WrkDataset.Denominator is not null




﻿

CREATE view [Denominator].[APCAdmissionUsingFirstEpisode]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	--,SiteID = WrkDataset.EndSiteID  -- RR this dataset includes patients who are still in, but EndSite,EndDirectorate come from the Discharge, therefore null, so use Start, relating to the Admission
	,SiteID = WrkDataset.StartSiteID  
	--,DirectorateID = WrkDataset.EndDirectorateID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCAdmissionUsingFirstEpisode'

where
	DatasetCode = 'APC'
and
	FirstEpisodeInSpellIndicator = 1



﻿
CREATE view Denominator.ManualItemID849 as

select
	 DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.DateID
	,ServicePointID = D.ServicePointID
	,Value = D.Denominator

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.ManualItemID849'
	
where
	Dataset.DatasetCode = 'MAN'
and ItemID = '849'
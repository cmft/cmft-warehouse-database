﻿




CREATE view [Denominator].[HRRecentAndLongTermAppointment]
as

-- 20160119	RR	A change was made to the HR file.  Jonathan Hodges advised that the indicator should remain the same but the calculation should be Headcounter Period End / Headcount Period Start

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.CensusDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = coalesce(WrkDataset.RecentAppointment,0) + coalesce(WrkDataset.LongTermAppointment,0)
	--,HeadcountPeriodStart
from
	dbo.WrkDataset 
inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.HRRecentAndLongTermAppointment'

where
	Dataset.DatasetCode = 'HR'
--and HeadcountPeriodStart is not null
and 
	(
	WrkDataset.RecentAppointment is not null
or WrkDataset.LongTermAppointment is not null
	)










﻿



create view [Denominator].[EmergencyReadmissionWithin28DaysDenominatorNonElective]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset 

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.EmergencyReadmissionWithin28DaysDenominatorNonElective'

where
	Dataset.DatasetCode = 'APC'
and WrkDataset.Readmission28DayDenominator = 1
and WrkDataset.PatientCategoryCode = 'NE'
﻿



CREATE view [Denominator].[AdmissionExcludingDayCaseRegularDayNight]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.AdmissionExcludingDayCaseRegularDayNight'

where
	DatasetCode = 'APC'
and	FirstEpisodeInSpellIndicator = 1
and	NationalPatientClassificationCode not in ('2','3','4')





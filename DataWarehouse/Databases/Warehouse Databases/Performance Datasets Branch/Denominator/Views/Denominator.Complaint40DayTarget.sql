﻿







create view [Denominator].[Complaint40DayTarget] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.Complaint40DayTargetDateID
	,WrkDataset.ServicePointID
	,Value = 1
from
	WrkDataset WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.Complaint40DayTarget'
	
where
	DatasetCode = 'COMP'
and	WrkDataset.CaseTypeCode in ('0x4120','0x4A20') --formal complaint and acute trust complaint
and	SequenceNo = 1




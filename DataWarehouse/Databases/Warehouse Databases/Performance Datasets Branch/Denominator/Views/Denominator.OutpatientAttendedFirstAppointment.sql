﻿CREATE view [Denominator].[OutpatientAttendedFirstAppointment]

as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = d.StartSiteID   
	,DirectorateID = d.StartDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.AppointmentDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset Dataset
on	d.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OutpatientAttendedFirstAppointment'

where
	Dataset.DatasetCode = 'OP'
and
	NationalAttendanceStatusCode in ('5','6') -- Attended
and
	NationalFirstAttendanceCode in ('1','3') -- First Appointment

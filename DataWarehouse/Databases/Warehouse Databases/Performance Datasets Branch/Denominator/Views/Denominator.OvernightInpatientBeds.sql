﻿



CREATE view [Denominator].[OvernightInpatientBeds] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID		
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DateID
	,ServicePointID
	,Value = WrkDataset.AvailableInpatientBeds

from dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OvernightInpatientBeds'
	
where 
	Dataset.DatasetCode = 'BEDOCC'
and WrkDataset.AvailableInpatientBeds is not null
and WrkDataset.BedOccupancyNights is null





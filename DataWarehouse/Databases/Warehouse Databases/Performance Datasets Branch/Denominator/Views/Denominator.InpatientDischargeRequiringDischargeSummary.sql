﻿


CREATE view [Denominator].[InpatientDischargeRequiringDischargeSummary]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.DischargeDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientDischargeRequiringDischargeSummary'
	
where
	DatasetCode = 'APC'
and WrkDataset.NationalLastEpisodeInSpellCode = 1
and	WrkDataset.DischargeSummaryRequired = 1
and WrkDataset.NationalLastEpisodeInSpellCode = 1
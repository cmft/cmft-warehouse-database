﻿





CREATE view [Denominator].[InpatientElectiveAdmission]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.StartServicePointID
	,Value = 1

from
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientElectiveAdmission'

where
	DatasetCode = 'APC'
and
	NationalLastEpisodeInSpellCode = 1
and
	PatientCategoryCode in ('EL', 'DC')







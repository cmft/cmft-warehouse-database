﻿

CREATE view [Denominator].[InpatientSpellWithProcedure]

as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDataset.StartSiteID	
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AdmissionDateID
	,ServicePointID = WrkDataset.ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset Dataset
on	WrkDataset.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientSpellWithProcedure'

where
	DatasetCode = 'APC'
and	WrkDataset.FirstEpisodeInSpellIndicator = 1
and WrkDataset.FirstOperationInSpellTime is not null
and WrkDataset.DischargeTime is not null




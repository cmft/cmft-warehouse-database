﻿















CREATE procedure [Envoy].[BuildImminentSMSByPASSpecialtyInputFile]
as

/****************************************************************************************
	Stored procedure : [Envoy].[BuildImminentSMSByPASSpecialtyInputFile]
	Description		 : Proc for appointments that meet criteria: All clinics under PAS speciality(s).
		This proc was created as Healthcare Communications (HC) wanted the clinic_code set to 'Default'
		where the appointment reminders are for ALL clinics under required PAS specialty(s), not 
		clinic code specific.
		They also asked for the clinic code to be moved to the appointment ref column.
		
		NB There are a handful of division mappings missed when joining on SourceUniqueID. When this 
		occurs I've tried to work around it	by getting the Division the clinic code has been most 
		allocated to in the past year. Cassian Butterworth said SourceUniqueID is not the best field 
		to join on, as it may change from the PAS value to the WOMv2 value. This needs looking at 
		(maybe just join on	patient id, appointment date, appointment time, clinic code, episode no, 
		clinic code (?))
	Time to run		: DEV 11 min


	Modification History
	====================
	
	Date		Person		Description
	====================================================================================
	26/03/2014	Paul Egan	Intial Coding
	04/04/2014	Paul Egan	Cancellations defined by null cancellation date, instead of 
								AppointmentStatusCode = 'CND' (as this missed other cancellation statuses).
	04/04/2014	Paul Egan	Get which division has been most allocated to a clinic code in the last year.
	07/04/2014	Paul Egan	Changed original query to CTE 'getDivisionAllocation', so could then also include
								all clinics for Childrens Division (hard-coded for now).
	30/04/2014	Paul Egan	Added Childrens contact telephone no (0161 701 9501).
	13/05/2014	Paul Egan	Revert clinic code column, added Haematology contact tel nos.
	10/06/2014	Paul Egan	Added requested clinic-code exclusions (hard coded for now).
	13/06/2014	Paul Egan	Added Endoscopy exclusion fields and filter criteria and clinic-code sexcode appended. (DEV NOT COMPLETED)
	20/06/2014	Paul Egan	Exclude 'SUB__' ward codes as these are Trafford duplicates on PAS and send the wrong site to the patient.
	04/07/2014	Paul Egan	Revert clinic code column for Childrens
	14/09/2015	Paul Egan	Removed test code (it was already commented out)
*****************************************************************************************/

declare @StartTime datetime;
declare @Elapsed int;
declare @RowsDeleted Int;
declare @RowsInserted Int;
declare @RowsUpdated Int;
declare @Stats varchar(255);

set dateformat dmy;

select
	@StartTime = getdate();

with getDivisionAllocation as
(
select 
	 patient_id = ImminentSMS.DistrictNo
	,appointment_date = convert(varchar , ImminentSMS.AppointmentDate , 103)
	,appointment_time = ImminentSMS.AppointmentTime
	,mobile_number =
		case
		when left(ImminentSMS.MobilePhone , 2) = '07' then ImminentSMS.MobilePhone
		when left(ImminentSMS.HomePhone , 2) = '07' then ImminentSMS.HomePhone
		else null
		end
	,landline_number =
		case
		when left(ImminentSMS.HomePhone , 2) != '07' then ImminentSMS.HomePhone
		when left(ImminentSMS.MobilePhone , 2)  != '07' then ImminentSMS.MobilePhone
		else null
		end
	,country_code = '44'
	,contact_tel_no = ''
	,first_name = ImminentSMS.Forenames
	,last_name = ImminentSMS.Surname
	,dob = ImminentSMS.DateOfBirth
	,cancel =
		case
			when ImminentSMS.CancellationDate is not null then 'cancel'
			else null
		end
	,PASSpecialtyCodeMapped = EnvoySpecialtyMap.PASSpecialtyCode
	,specialty_code = PASSpecialty.NationalSpecialtyCode
	,clinic_code = ImminentSMS.ClinicCode
		
	/*	Get post-allocation Division mapping. If cannot find this, get Division for which the 
		clinic code has been most allocated to in the last year */
	,Division =		
		case ImminentSMS.EncounterTypeCode
			when 'APC' then coalesce(DirectorateAPC.Division, DirectorateAPCWL.Division, APCWLClinicDivisionAllocation.Division)
			when 'OP' then coalesce(DirectorateOP.Division, DirectorateOPWL.Division, OPClinicDivisionAllocation.Division)
		end
		
	,appt_audit_ref = null
	,appt_type = null
	,location = null
	,practitioner = null
	,procedure_code = null
	,patient_email = null
	,genericinfo1 = null
	,genericinfo2 = null
	,genericinfo3 = ImminentSMS.Postcode
	,DiagGroup = ImminentSMS.DiagGroup
	,SexCode = ImminentSMS.SexCode
	,Comment = ImminentSMS.Comment
	,WardCode = ImminentSMS.WardCode		-- Added Paul Egan 20/06/2014
FROM
	ETL.TImportOPImminentSMS ImminentSMS

	left join Envoy.SpecialtyMap EnvoySpecialtyMap		--@EnvoySpecialtyMap EnvoySpecialtyMap --DEBUG ONLY
	on	EnvoySpecialtyMap.PASSpecialtyCode = ImminentSMS.SpecialtyCode
	and EnvoySpecialtyMap.FileType = 'SMS'
	
	/* Added Paul Egan 07/04/2014 - to get national specialty for Childrens */
	left join PAS.Specialty PASSpecialty
	on	PASSpecialty.SpecialtyCode = ImminentSMS.SpecialtyCode

	left join PAS.Ward
	on	Ward.WardCode = ImminentSMS.WardCode

	left join PAS.Consultant
	on	Consultant.ConsultantCode = ImminentSMS.ConsultantCode

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.APC.BaseEncounter APCEncounter --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	ImminentSMS.SourceUniqueID = APCEncounter.SourceUniqueID
	and	ImminentSMS.EncounterTypeCode = 'APC'

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.OP.BaseEncounter OPEncounter --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	ImminentSMS.SourceUniqueID = OPEncounter.SourceUniqueID
	and	ImminentSMS.EncounterTypeCode = 'OP'

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.APCWL.BaseEncounter APCWLEncounter --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	ImminentSMS.SourceUniqueID = APCWLEncounter.SourceUniqueID
	and	ImminentSMS.EncounterTypeCode = 'APC'
	and APCWLEncounter.CensusDate = (select max(CensusDate) from WarehouseOLAPMergedV2.APCWL.BaseEncounter)	/* Only need latest Census Date */

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.OPWL.BaseEncounter OPWLEncounter --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	ImminentSMS.SourceUniqueID = OPWLEncounter.SourceUniqueID
	and	ImminentSMS.EncounterTypeCode = 'OP'
	and OPWLEncounter.CensusDate = (select max(CensusDate) from WarehouseOLAPMergedV2.OPWL.BaseEncounter) /* Only need latest Census Date */

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.WH.Directorate DirectorateAPC --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	APCEncounter.StartDirectorateCode = DirectorateAPC.DirectorateCode

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.WH.Directorate DirectorateOP --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	OPEncounter.DirectorateCode = DirectorateOP.DirectorateCode

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.WH.Directorate DirectorateAPCWL --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	APCWLEncounter.DirectorateCode = DirectorateAPCWL.DirectorateCode
	--and APCWLEncounter.CensusDate = (select max(CensusDate) from WarehouseOLAPMergedV2.APCWL.BaseEncounter)	/* Only need latest Census Date */

	/* Get post-allocation Division mapping */
	left join WarehouseOLAPMergedV2.WH.Directorate DirectorateOPWL --with (nolock) /* Avoid being blocked by late WOMv2 - OP build. Paul Egan 25/03/2014 */
	on	OPWLEncounter.DirectorateCode = DirectorateOPWL.DirectorateCode
	--and OPWLEncounter.CensusDate = (select max(CensusDate) from WarehouseOLAPMergedV2.OPWL.BaseEncounter) /* Only need latest Census Date */

	/* Get which division has been most allocated to a clinic code in the last year */
	/* (Use this if fail to find post-allocation Division) */
	left join
		(
		select
			OPEncounter.ClinicCode
			,OPDirectorate.Division
			,ClinicDivisionCount = COUNT(OPDirectorate.Division)
			,DivisionMostAllocatedSequence = row_number() over (partition by OPEncounter.ClinicCode order by COUNT(OPDirectorate.Division) desc)
		from 
			WarehouseReportingMerged.OP.Encounter OPEncounter
			
			left join WarehouseReportingMerged.WH.Directorate OPDirectorate
			on OPEncounter.DirectorateCode = OPDirectorate.DirectorateCode
		where
			OPEncounter.AppointmentDate >= dateadd(year, -1, getdate())
			and OPEncounter.DirectorateCode is not null
		group by
			OPEncounter.ClinicCode
			,OPDirectorate.Division
		) OPClinicDivisionAllocation
	on	ImminentSMS.ClinicCode = OPClinicDivisionAllocation.ClinicCode
	and	OPClinicDivisionAllocation.DivisionMostAllocatedSequence = 1 -- Division Most allocated
	
	/* Get which division has been most allocated to a clinic code in the last year */
	/* (Use this if fail to find post-allocation Division) */
	left join
		(
		select
			APCWLEncounter.WaitingListCode
			,APCWLDirectorate.Division
			,ClinicDivisionCount = COUNT(APCWLDirectorate.Division)
			,DivisionMostAllocatedSequence = row_number() over (partition by APCWLEncounter.WaitingListCode order by COUNT(APCWLDirectorate.Division) desc)
		from 
			WarehouseReportingMerged.APCWL.Encounter APCWLEncounter
			
			left join WarehouseReportingMerged.WH.Directorate APCWLDirectorate
			on APCWLEncounter.DirectorateCode = APCWLDirectorate.DirectorateCode
		where
			APCWLEncounter.TCIDate >= dateadd(year, -1, getdate())
			and APCWLEncounter.DirectorateCode is not null
		group by
			APCWLEncounter.WaitingListCode
			,APCWLDirectorate.Division
		) APCWLClinicDivisionAllocation
	on	ImminentSMS.ClinicCode = APCWLClinicDivisionAllocation.WaitingListCode
	and	APCWLClinicDivisionAllocation.DivisionMostAllocatedSequence = 1 -- Division Most allocated

where
	ImminentSMS.DateOfDeath is null
--and	ImminentSMS.EncounterTypeCode = 'APC'
and	(
		ImminentSMS.AppointmentDate =
			(
			select
				max(AppointmentDate)
			from
				ETL.TImportOPImminentSMS
			where
				TImportOPImminentSMS.DateOfDeath is null
			and	TImportOPImminentSMS.EncounterTypeCode = 'OP'
			and	TImportOPImminentSMS.CancellationDate is null
			)
	or	ImminentSMS.CancellationDate is not null
	)

--order by 1, 2, 3
)


INSERT INTO Envoy.ImminentSMSByPASSpecialty
(
	 patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name
	,last_name
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,Division
	,appt_audit_ref
	,appt_type
	,location
	,practitioner
	,procedure_code
	,patient_email
	,genericinfo1
	,genericinfo2
	,genericinfo3
)

select  
	 patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no =	/* This is a bit messy. Can we use phone number from PAS letter templates instead? Paul Egan 13/05/2014 */
		case 
			when getDivisionAllocation.Division = 'Childrens' then '01617019501'
			when getDivisionAllocation.Division = 'Specialist Medicine' then
				case
					when getDivisionAllocation.PASSpecialtyCodeMapped in ('HAEM', 'HAAC')
						and getDivisionAllocation.clinic_code in ('SCT', 'SCTANT') then '01612743322' /* Clinical Haematology (Sickle Cell Centre) */
					when getDivisionAllocation.PASSpecialtyCodeMapped in ('HAEM', 'HAAC') then '01612764801' /* Clinical Haematology */
					when getDivisionAllocation.PASSpecialtyCodeMapped = 'GEND' then '01612764366'	/* Endoscopy from CMFT Internet */
				end
			else contact_tel_no
		end
	,first_name
	,last_name
	,dob
	,cancel
	,specialty_code
	,clinic_code = 
		case 
			--when getDivisionAllocation.Division = 'Childrens' then 'Default'		-- Removed as requested by Healthcare Communications. Paul Egan 04/07/2014
			when getDivisionAllocation.Division = 'Specialist Medicine'				-- added Paul Egan 13/06/2014
				and getDivisionAllocation.PASSpecialtyCodeMapped = 'GEND'
					then getDivisionAllocation.clinic_code + coalesce(getDivisionAllocation.SexCode, '')
			else clinic_code
		end
	,Division =
		case getDivisionAllocation.Division
			when 'Childrens' then 'ChildrensTest'
			else getDivisionAllocation.Division
		end
	,appt_audit_ref
		--case getDivisionAllocation.Division
		--	when 'Childrens' then clinic_code   -- Removed as requested by Healthcare Communications. Paul Egan 04/07/2014
		--	else appt_audit_ref
		--end
	,appt_type
	,location
	,practitioner
	,procedure_code
	,patient_email
	,genericinfo1
	,genericinfo2
	,genericinfo3
from getDivisionAllocation
where
	/*	Added Paul Egan 07/04/2014. Include all clinics for Childrens division.
	NB This is a bit of a fudge due to time constraints, but I think it works ok. */
	(
		PASSpecialtyCodeMapped is not null	-- Need to use this as replaced inner join to EnvoySpecialtyMap with left join, to get Childrens.
	or	Division = 'Childrens'				-- If need to add all clinics for any other Division(s), add the division as another OR in this WHERE clause.
	)
	
	and clinic_code not in		-- Hard code exclusions for now (Gareth Summerfield). I've added the exclusion reason, see TFS 3646 for details. Paul Egan 10/06/2014
		(
			'CPREADCL',		-- Surgical inpatient pre-admission (held in out-patients)
			'CVEBPRE',		-- Surgical inpatient pre-admission (held in out-patients)
			'CAAMMET',		-- Saint Mary's clinics. They are not (yet) using PAS letter templates, so PAS may be incorrect 
			'CJHWMET',		-- Saint Mary's clinics. They are not (yet) using PAS letter templates, so PAS may be incorrect
			'CSAJMET',		-- Saint Mary's clinics. They are not (yet) using PAS letter templates, so PAS may be incorrect
			'CABMET',		-- Saint Mary's clinics. They are not (yet) using PAS letter templates, so PAS may be incorrect
			'CBSMET',		-- Saint Mary's clinics. They are not (yet) using PAS letter templates, so PAS may be incorrect
			'CEJMET'		-- Saint Mary's clinics. They are not (yet) using PAS letter templates, so PAS may be incorrect
		)
		
	and coalesce(WardCode, '') not like 'SUB%'		-- Added Paul Egan 20/06/2014. Use coalesce to stop NULLS disappearing in the filter
		
	and not		-- Added Paul Egan 13/06/2014 - Endoscopy exclusions as requsted in specification (Bowel Cancer Screening Programme)
		( 
		coalesce(PASSpecialtyCodeMapped, '') = 'GEND'		-- Use coalesce to stop NULLS disappearing in the filter - credit Darren Griffiths
		and coalesce(DiagGroup, '') = 'BOWEL'				-- Use coalesce to stop NULLS disappearing in the filter - credit Darren Griffiths
		and	coalesce(Comment, '') like '%BCSP%'				-- Use coalesce to stop NULLS disappearing in the filter - credit Darren Griffiths
		)

--order by 1, 2, 3

;



select @RowsInserted = @@rowcount;


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate());

SELECT @Stats = 'Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins';

EXEC WriteAuditLogEvent 'Envoy.BuildImminentSMSByPASSpecialtyInputFile', @Stats, @StartTime;

print @Stats;

















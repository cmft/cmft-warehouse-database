﻿

create proc Utility.BuildOPProcessList as


		--update

	INSERT INTO [OP].[ProcessList]
			   ([EncounterRecno]
			   ,[Action])

	select
		 EncounterRecno
		,Action = 'UPDATE'


	from
		OP.Encounter
	where
		 exists
		(
		select
			1
		from
			[WarehouseOLAPMergedV2].OP.BaseEncounter
		where
			Encounter.SourceUniqueID = BaseEncounter.SourceUniqueID
		and	BaseEncounter.ContextCode = 'CEN||PAS'
		and Encounter.Updated  > BaseEncounter.Updated 

		)

--not in Proccess List
	and not
 exists

(
Select 1 from op.ProcessList ProcessList
where ProcessList.EncounterRecno = Warehouse.OP.Encounter.EncounterRecno
)


--inserts
	INSERT INTO [OP].[ProcessList]
			   ([EncounterRecno]
			   ,[Action])

select
	 EncounterRecno
	,Action = 'INSERT'



from
	OP.Encounter
where

	Encounter.AppointmentDate > '31 mar 2009'
	and
	not exists
	(
	select
		1
	from
		[WarehouseOLAPMergedV2].OP.BaseEncounter
	where
		Encounter.SourceUniqueID = BaseEncounter.SourceUniqueID
	and	BaseEncounter.ContextCode = 'CEN||PAS'


	)

	--not in Proccess List
	and not
 exists

(
Select 1 from op.ProcessList ProcessList
where ProcessList.EncounterRecno = Warehouse.OP.Encounter.EncounterRecno
)
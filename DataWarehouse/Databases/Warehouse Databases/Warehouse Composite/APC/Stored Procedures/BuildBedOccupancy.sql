﻿


/* gcBuildBedOccupancyTable (6/5/98, John Orrell)
**
** Build the BedOccupancy table for a date range
**
** Inputs:
**
**  @FromDate   Start date (eg '1 Jan 1998')
**  @ToDate     End date   (eg '1 Dec 1998')
**
**
** Example:
**
**  To build the table for the first quarter of 1998:
**
**  exec gcBuildBedOccupancyTable '1 jan 98','1 mar 98'
*/

CREATE   procedure [APC].[BuildBedOccupancy]
as

declare @to smalldatetime

select
	@to = coalesce(DateValue, getdate())
from
	Utility.Parameter
where
	Parameter = 'LOADAPCDATE'


declare @from smalldatetime

select
	@from = '31 Mar 2012'

truncate table APC.BedOccupancy

insert into APC.BedOccupancy
(
	 ProviderSpellNo
	,WardStayRecno
	,EncounterRecno
	,BedOccupancyDate
	,Nights
	,ZeroLengthStays
)
/* BedOccupancyView (6/5/98, John Orrell)
**
** This view contains combinations of Ward Stay, Episode and Day that overlap each other.
**
** It ignores stays for which there is no corresponding episode and episodes for which
** there is no corresponding stay.
*/

select
	*
from
(
	select
		 APCEncounter.ProviderSpellNo
		,WardStayRecno = WardStay.EncounterRecno
		,APCEncounter.EncounterRecno
		,Calendar.TheDate

		/* Nights = lowest Stay/Episode/Month end date - highest Stay/Episode/Month start date */
		,Nights =
		datediff(
			 day

			,case
			when
				WardStay.StartDate >= APCEncounter.EpisodeStartDate
			and	WardStay.StartDate >= Calendar.TheDate
			then WardStay.StartDate
			when APCEncounter.EpisodeStartDate >= Calendar.TheDate
			then APCEncounter.EpisodeStartDate
			else Calendar.TheDate
			end

			,case
			when
				coalesce(WardStay.EndDate, @to) <= coalesce(APCEncounter.EpisodeEndDate, @to)
			and coalesce(WardStay.EndDate, @to) <= Calendar.TheDate
			then WardStay.EndDate
			when coalesce(APCEncounter.EpisodeEndDate, @to) <= Calendar.TheDate
			then APCEncounter.EpisodeEndDate
			else dateadd(day, 1, Calendar.TheDate)
			end
		)

		/* ZeroLengthStays = 1 or 0
		**  0 when
		**      ward stay is not zero length (end date = start date)
		**  or  ward stay is not part of a zero length spell (spell end = spell start)
		**  or  matching episode is not the first episode of the spell
		*/
		,ZeroLengthStays =
		case
		when coalesce(WardStay.EndDate, @to) <> WardStay.StartDate
		then 0
		when
			(
			select
				max(coalesce(A.EpisodeEndDate,@to))
			from
				APC.Encounter A
			where
				A.ProviderSpellNo = APCEncounter.ProviderSpellNo
			) <>
			(
			select
				min(A.EpisodeStartDate)
			from
				APC.Encounter A
			where
				A.ProviderSpellNo = APCEncounter.ProviderSpellNo
			)
		then 0

		when
			APCEncounter.EncounterRecno <>
			(
			select
				min(EncounterRecno)
			from
				APC.Encounter A
			where
				A.ProviderSpellNo = APCEncounter.ProviderSpellNo
			)
		then 0

		else 1
		end
	from
		APC.Encounter APCEncounter

	inner join APC.WardStay
	on	WardStay.ProviderSpellNo = APCEncounter.ProviderSpellNo

	inner join WarehouseModel.WH.Calendar
	on	(
			(
				Calendar.TheDate <= coalesce(APCEncounter.EpisodeEndDate, @to)
			and Calendar.TheDate >= APCEncounter.EpisodeStartDate
			)
		and (
				Calendar.TheDate <= coalesce(WardStay.EndDate, @to)
			and Calendar.TheDate >= WardStay.StartDate
			)
		and (
				APCEncounter.EpisodeStartDate <= coalesce(WardStay.EndDate, @to)
			and coalesce(APCEncounter.EpisodeEndDate, @to) >= WardStay.StartDate
			)
		)

	where
		Calendar.TheDate > @from
	) Activity
where
	Nights + ZeroLengthStays > 0
--	((Nights <> 0) or (ZeroLengthStays <> 0))




﻿
CREATE PROCEDURE [ETL].[LoadEnvoy]
	 --@AppointmentDate smalldatetime = null
as

/****************************************************************************************
	Stored procedure : [ETL].[LoadEnvoy]
	Description		 : Proc for Envoy Healthcare Communications (HC) appointment reminder data feed(s)
	
	
	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Initial coding
	27/03/2014	Paul Egan       Added proc BuildImminentSMSByPASSpecialtyInputFile 
*****************************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @AppointmentDateNextDay smalldatetime
declare @AppointmentDateNextWeek smalldatetime

--select @AppointmentDateNextDay = dateadd(day, datediff(day, 0, coalesce(@AppointmentDate, dateadd(day , 1 , getdate()))), 0)

--select @AppointmentDateNextWeek = dateadd(day, datediff(day, 0, dateadd(day , 7 , coalesce(@AppointmentDate, getdate()))), 0)

select
	@StartTime = getdate()



truncate table ETL.TImportOPImminentSMS
exec ETL.ExtractInquireOPImminentSMS 

truncate table OP.ImminentSMSInputFile
exec ETL.LoadOPImminentSMSInputFile

truncate table Envoy.AgentInputFile
exec Envoy.BuildAgentInputFile

/*	Added Paul Egan 27/03/2014 */
/*	HC wants to move towards all clinics under national specialty(s), rather than
	defining clinic by clinic */
truncate table Envoy.ImminentSMSByPASSpecialty;
exec Envoy.BuildImminentSMSByPASSpecialtyInputFile;


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadEnvoy', @Stats, @StartTime

print @Stats


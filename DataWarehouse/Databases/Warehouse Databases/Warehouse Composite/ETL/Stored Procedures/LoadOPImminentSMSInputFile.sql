﻿

--2015-11-23 CCB This model needs to move to DataMarts or somewhere....

CREATE procedure [ETL].[LoadOPImminentSMSInputFile]
as

declare @StartTime datetime;
declare @Elapsed int;
declare @RowsDeleted Int;
declare @RowsInserted Int;
declare @RowsUpdated Int;
declare @Stats varchar(255);

set dateformat dmy;

select
	@StartTime = getdate();


With OPImminentSMS as
(
SELECT
	 patient_id = ImminentSMS.DistrictNo
	,appointment_date = convert(varchar , ImminentSMS.AppointmentDate , 103)
	,appointment_time = ImminentSMS.AppointmentTime

	,mobile_number =
		case
		when left(MobilePhone , 2) = '07' then MobilePhone
		when left(ImminentSMS.HomePhone , 2) = '07' then ImminentSMS.HomePhone
		else null
		end
		
	,landline_number =
		case
		when left(ImminentSMS.HomePhone , 2) != '07' then ImminentSMS.HomePhone
		when left(MobilePhone , 2)  != '07' then MobilePhone
		else null
		end

	,country_code = '44'

	,contact_tel_no = ''

	,first_name = Forenames
	,last_name = Surname
	,dob = ImminentSMS.DateOfBirth
	,age = DATEDIFF(Year, ImminentSMS.DateOfBirth, GETDATE())
	,cancel =
		case
		when ImminentSMS.AppointmentStatusCode = 'CND' or  ImminentSMS.CancelledByCode in ( 'P' , 'H' ) then 'cancel'
		else null
		end

	--,specialty_code = NationalSpecialtyCode

	,specialty_code = Clinic.XrefEntityCode

	,clinic_code = -- append 'ZN' or 'ZNU' if those are the appointment types (C&B)
		case
		when ImminentSMS.AppointmentTypeCode = 'ZN' or ImminentSMS.AppointmentTypeCode = 'ZNU' then ImminentSMS.ClinicCode + ImminentSMS.AppointmentTypeCode
		else ImminentSMS.ClinicCode
		end

	--,Division.Division
	,Division =		/* Added 13/03/2014 Paul Egan. Get post-allocation Division mapping */
		case ImminentSMS.EncounterTypeCode
			when 'APC' then coalesce(DirectorateAPC.Division, Division.Division)
			when 'OP' then coalesce(DirectorateOP.Division, Division.Division)
		end
	,ImminentSMS.Postcode
FROM
	ETL.TImportOPImminentSMS ImminentSMS

left join PAS.Specialty PASSpecialty
on	ImminentSMS.SpecialtyCode = PASSpecialty.SpecialtyCode

/* Added 13/03/2014 Paul Egan. Get post-allocation Division mapping */
left join WarehouseOLAPMergedV2.APC.BaseEncounter APCEncounter
on	ImminentSMS.SourceUniqueID = APCEncounter.SourceUniqueID
and	ImminentSMS.EncounterTypeCode = 'APC'

/* Added 13/03/2014 Paul Egan. Get post-allocation Division mapping */
left join WarehouseOLAPMergedV2.OP.BaseEncounter OPEncounter
on	ImminentSMS.SourceUniqueID = OPEncounter.SourceUniqueID
and	ImminentSMS.EncounterTypeCode = 'OP'

/* Added 13/03/2014 Paul Egan. Get post-allocation Division mapping */
left join WarehouseOLAPMergedV2.WH.Directorate DirectorateAPC
on	APCEncounter.StartDirectorateCode = DirectorateAPC.DirectorateCode

/* Added 13/03/2014 Paul Egan. Get post-allocation Division mapping */
left join WarehouseOLAPMergedV2.WH.Directorate DirectorateOP
on	OPEncounter.DirectorateCode = DirectorateOP.DirectorateCode

/* department filter */
inner join dbo.EntityXref Clinic
on	Clinic.EntityTypeCode = 'CLINICCODE'
and	Clinic.EntityCode = ImminentSMS.ClinicCode
and	Clinic.XrefEntityTypeCode = 'HCDEPARTMENTID'

inner join dbo.EntityXref Department
on	Department.EntityTypeCode = 'HCDEPARTMENTID'
and	Department.EntityCode = Clinic.XrefEntityCode
and	Department.XrefEntityTypeCode = 'DIVISIONCODE'

left join WH.Division
on	Division.DivisionCode = Department.XrefEntityCode

where
	ImminentSMS.DateOfDeath is null
and	(
		-- get the first active patient appointment in each department on the last day
		(
			ImminentSMS.AppointmentDate =
				(
				select
					max(AppointmentDate)
				from
					ETL.TImportOPImminentSMS
				where
					EncounterTypeCode = 'OP'
				)
		and CancellationDate is null
		and	not exists
				(
				select
					1
				from
					ETL.TImportOPImminentSMS EarlierAppointment

				inner join dbo.EntityXref EarlierAppointmentClinic
				on	EarlierAppointmentClinic.EntityTypeCode = 'CLINICCODE'
				and	EarlierAppointmentClinic.EntityCode = ImminentSMS.ClinicCode
				and	EarlierAppointmentClinic.XrefEntityTypeCode = 'HCDEPARTMENTID'

				inner join dbo.EntityXref EarlierAppointmentDepartment
				on	EarlierAppointmentDepartment.EntityTypeCode = 'HCDEPARTMENTID'
				and	EarlierAppointmentDepartment.EntityCode = EarlierAppointmentClinic.XrefEntityCode
				and	EarlierAppointmentDepartment.XrefEntityTypeCode = 'DIVISIONCODE'

				where
					EarlierAppointment.DistrictNo = ImminentSMS.DistrictNo
				and	EarlierAppointmentDepartment.XrefEntityCode = Department.XrefEntityCode
				and	EarlierAppointment.AppointmentDate = ImminentSMS.AppointmentDate
				and EarlierAppointment.CancellationDate is null
				and	(
						(
							cast(convert(varchar , EarlierAppointment.AppointmentDate , 103) + ' ' + EarlierAppointment.AppointmentTime as smalldatetime)
							< 
							cast(convert(varchar , ImminentSMS.AppointmentDate , 103) + ' ' + ImminentSMS.AppointmentTime as smalldatetime)
						)
						or
						(
							cast(convert(varchar , EarlierAppointment.AppointmentDate , 103) + ' ' + EarlierAppointment.AppointmentTime as smalldatetime)
							= 
							cast(convert(varchar , ImminentSMS.AppointmentDate , 103) + ' ' + ImminentSMS.AppointmentTime as smalldatetime)
						and EarlierAppointment.SourceUniqueID < ImminentSMS.SourceUniqueID
						)
					)

				)
		)
	or	--	get the first cancelled patient appointment in each department on each day, in the absence of another active appointment
		(
			CancellationDate is not null
		and	not exists -- no earlier cancelled appointments on the day
				(
				select
					1
				from
					ETL.TImportOPImminentSMS EarlierAppointment

				inner join dbo.EntityXref EarlierAppointmentClinic
				on	EarlierAppointmentClinic.EntityTypeCode = 'CLINICCODE'
				and	EarlierAppointmentClinic.EntityCode = ImminentSMS.ClinicCode
				and	EarlierAppointmentClinic.XrefEntityTypeCode = 'HCDEPARTMENTID'

				inner join dbo.EntityXref EarlierAppointmentDepartment
				on	EarlierAppointmentDepartment.EntityTypeCode = 'HCDEPARTMENTID'
				and	EarlierAppointmentDepartment.EntityCode = EarlierAppointmentClinic.XrefEntityCode
				and	EarlierAppointmentDepartment.XrefEntityTypeCode = 'DIVISIONCODE'

				where
					EarlierAppointment.DistrictNo = ImminentSMS.DistrictNo
				and	EarlierAppointmentDepartment.XrefEntityCode = Department.XrefEntityCode
				and	EarlierAppointment.AppointmentDate = ImminentSMS.AppointmentDate
				and EarlierAppointment.CancellationDate is not null
				and	(
						(
							cast(convert(varchar , EarlierAppointment.AppointmentDate , 103) + ' ' + EarlierAppointment.AppointmentTime as smalldatetime)
							< 
							cast(convert(varchar , ImminentSMS.AppointmentDate , 103) + ' ' + ImminentSMS.AppointmentTime as smalldatetime)
						)
						or
						(
							cast(convert(varchar , EarlierAppointment.AppointmentDate , 103) + ' ' + EarlierAppointment.AppointmentTime as smalldatetime)
							= 
							cast(convert(varchar , ImminentSMS.AppointmentDate , 103) + ' ' + ImminentSMS.AppointmentTime as smalldatetime)
						and EarlierAppointment.SourceUniqueID < ImminentSMS.SourceUniqueID
						)
					)

				)
		and	not exists -- no active appointments on the day
				(
				select
					1
				from
					ETL.TImportOPImminentSMS ActiveAppointment

				inner join dbo.EntityXref ActiveAppointmentClinic
				on	ActiveAppointmentClinic.EntityTypeCode = 'CLINICCODE'
				and	ActiveAppointmentClinic.EntityCode = ImminentSMS.ClinicCode
				and	ActiveAppointmentClinic.XrefEntityTypeCode = 'HCDEPARTMENTID'

				inner join dbo.EntityXref ActiveAppointmentDepartment
				on	ActiveAppointmentDepartment.EntityTypeCode = 'HCDEPARTMENTID'
				and	ActiveAppointmentDepartment.EntityCode = ActiveAppointmentClinic.XrefEntityCode
				and	ActiveAppointmentDepartment.XrefEntityTypeCode = 'DIVISIONCODE'

				where
					ActiveAppointment.DistrictNo = ImminentSMS.DistrictNo
				and	ActiveAppointmentDepartment.XrefEntityCode = Department.XrefEntityCode
				and	ActiveAppointment.AppointmentDate = ImminentSMS.AppointmentDate
				and ActiveAppointment.CancellationDate is null
				)
		)
	)
)


INSERT INTO OP.ImminentSMSInputFile
(
	 patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name
	,last_name
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,Division
	,Postcode
)
select
	patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no = 
		case
			when OPImminentSMS.Division = 'Childrens' then '01617019501'
			when OPImminentSMS.Division = 'Dental' then '01613061904'
			when OPImminentSMS.Division = 'Ophthalmology' then '01612765533' --from CMFT website
			when OPImminentSMS.Division = 'Surgical' then ''
			when OPImminentSMS.Division = 'Specialist Medicine' and OPImminentSMS.specialty_code = '303' then '01612764801' --from CMFT website
			else OPImminentSMS.contact_tel_no
		end
	,first_name
	,last_name
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,Division = /* Fudge due to time constraints Paul Egan 14/03/2014 */
		case OPImminentSMS.Division
			when 'Specialist Medical' then 'Specialist Medicine'
			else OPImminentSMS.Division
		end
	,Postcode
from
	OPImminentSMS





--select @RowsInserted = @@rowcount


--SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

--SELECT @Stats = 'Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

--EXEC WriteAuditLogEvent 'LoadOPImminentSMSInputFile', @Stats, @StartTime

--print @Stats




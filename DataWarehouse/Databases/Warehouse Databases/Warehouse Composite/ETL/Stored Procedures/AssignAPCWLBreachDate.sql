﻿CREATE procedure [ETL].[AssignAPCWLBreachDate]
	@CensusDate smalldatetime = null
as

declare @LocalCensusDate smalldatetime
declare @RTTCensusDate smalldatetime

set @LocalCensusDate = coalesce(@CensusDate, (select max(CensusDate) from APC.Snapshot))

select
	@RTTCensusDate =
	(
	select
		max(Snapshot.CensusDate)
	from
		RTT.dbo.OlapSnapshot Snapshot
	where
		Snapshot.CensusDate <= @LocalCensusDate
	)


update APC.WaitingList
set
	 NationalBreachDate = WL.NationalBreachDate
	,DerivedBreachDays = WL.DerivedBreachDays
	,DerivedClockStartDate = WL.DerivedClockStartDate
	,DerivedBreachDate = WL.DerivedBreachDate
	,RTTBreachDate = WL.RTTBreachDate
	,RTTDiagnosticBreachDate = WL.RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate = WL.NationalDiagnosticBreachDate
	,SocialSuspensionDays = WL.SocialSuspensionDays
	,BreachTypeCode = WL.BreachTypeCode
	,ReferralEncounterRecno = WL.ReferralEncounterRecno
from
	APC.WaitingList

inner join
	(
	select
		 WL.EncounterRecno

		,WL.BreachTypeCode

		,DerivedBreachDays = 
		case
		when WL.BreachTypeCode = 2 then 
			case
			when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate 
			then WL.DiagnosticBreachDays
			else (select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS')
			end
		else (select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
		end

--replaced with above 19 June 2008 as we now ignore the National Breach Date logic PDO
--		,DerivedBreachDays = 
--		case
--		when WL.BreachTypeCode = 2 then 
--			case
--			when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate 
--			then WL.DiagnosticBreachDays
--			else (select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS')
--			end
--		when datediff(day, WL.DerivedClockStartDate, WL.CensusDate) > (select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')
--		then (select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS')
--		else (select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
--		end

		,WL.DerivedClockStartDate

		,WL.RTTBreachDate

		,WL.NationalBreachDate

		,WL.RTTDiagnosticBreachDate

		,WL.NationalDiagnosticBreachDate

		,DerivedBreachDate =
		case
		when WL.BreachTypeCode = 2 then
			case
			when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate
			then WL.RTTDiagnosticBreachDate
			else WL.NationalDiagnosticBreachDate
			end
		when WL.BreachTypeCode = 1 then WL.NationalBreachDate
		else WL.RTTBreachDate
		end

--replaced with above 19 June 2008 as we now ignore the National Breach Date logic PDO
--		,DerivedBreachDate =
--		case
--		when WL.BreachTypeCode = 2 then
--			case
--			when WL.RTTDiagnosticBreachDate < WL.NationalDiagnosticBreachDate
--			then WL.RTTDiagnosticBreachDate
--			else WL.NationalDiagnosticBreachDate
--			end
--		when WL.BreachTypeCode = 1 then WL.NationalBreachDate
--		else WL.RTTBreachDate
--		end

		,WL.SocialSuspensionDays

		,ReferralEncounterRecno

	from
		(
		select
			 WL.EncounterRecno

			,WL.CensusDate

			,DerivedClockStartDate =
				coalesce(
					 WL.RTTStartDate
					,dateadd(day,
						 coalesce(RTTAdjustment.AdjustmentDays, 0)
						,RTTClockStart.ClockStartDate
					)
					,dateadd(day,
						 coalesce(RTTAdjustment.AdjustmentDays, 0)
						,RTTMatch.ReferralDate
					)
					,dateadd(day,
						 coalesce(RTTAdjustment.AdjustmentDays, 0)
						,WL.DateOnWaitingList
					)
				) 

			,RTTBreachDate =
				dateadd(day
				,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS') - 
				(
				datediff
					(
						 day
						,coalesce(
							 WL.RTTStartDate
							,dateadd(day,
								 coalesce(RTTAdjustment.AdjustmentDays, 0)
								,RTTClockStart.ClockStartDate
							)
							,dateadd(day,
								 coalesce(RTTAdjustment.AdjustmentDays, 0)
								,RTTMatch.ReferralDate
							)
							,dateadd(day,
								 coalesce(RTTAdjustment.AdjustmentDays, 0)
								,WL.DateOnWaitingList
							)
						)
						,coalesce(WL.SuspensionEndDate, WL.CensusDate)
					)
				 - coalesce(SocialSuspension.DaysSuspended, 0)
				)

				,coalesce(
					case
					when
						(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS') - 
						(
						datediff
							(
								 day
								,coalesce(
									 WL.RTTStartDate
									,dateadd(day,
										 coalesce(RTTAdjustment.AdjustmentDays, 0)
										,RTTClockStart.ClockStartDate
									)
									,dateadd(day,
										 coalesce(RTTAdjustment.AdjustmentDays, 0)
										,RTTMatch.ReferralDate
									)
									,dateadd(day,
										 coalesce(RTTAdjustment.AdjustmentDays, 0)
										,WL.DateOnWaitingList
									)
								)
								,coalesce(WL.SuspensionEndDate, WL.CensusDate)
							)
						 - coalesce(SocialSuspension.DaysSuspended, 0)
						) < 0
					then WL.SuspensionStartDate
					else WL.SuspensionEndDate
					end

					,WL.CensusDate
				)
			)

--			,RTTBreachDate =
--				dateadd(day, 126 + coalesce(SocialSuspension.DaysSuspended, 0),
--					coalesce(
--						 WL.RTTStartDate
--						,dateadd(day,
--							 coalesce(RTTAdjustment.AdjustmentDays, 0)
--							,RTTClockStart.ClockStartDate
--						)
--						,dateadd(day,
--							 coalesce(RTTAdjustment.AdjustmentDays, 0)
--							,RTTMatch.ReferralDate
--						)
--						,dateadd(day,
--							 coalesce(RTTAdjustment.AdjustmentDays, 0)
--							,WL.DateOnWaitingList
--						)
--					)
--				) 

			,RTTDiagnosticBreachDate =
				dateadd(day, RTTDiagnosticBreach.BreachValue + coalesce(Suspension.DaysSuspended, 0),
					WL.DateOnWaitingList
					)

--			,RTTDiagnosticBreachDate =
--				dateadd(day, RTTDiagnosticBreach.BreachValue + coalesce(SocialSuspension.DaysSuspended, 0),
--					coalesce(
--						 WL.RTTStartDate
--						,dateadd(day,
--							 coalesce(RTTAdjustment.AdjustmentDays, 0)
--							,RTTClockStart.ClockStartDate
--						)
--						,dateadd(day,
--							 coalesce(RTTAdjustment.AdjustmentDays, 0)
--							,RTTMatch.ReferralDate
--						)
--						,dateadd(day,
--							 coalesce(RTTAdjustment.AdjustmentDays, 0)
--							,WL.DateOnWaitingList
--						)
--					)
--				) 

			,DiagnosticBreachDays = RTTDiagnosticBreach.BreachValue

			,NationalDiagnosticBreachDate =
				 dateadd(day, (select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS') - WL.KornerWait
				,coalesce(
					 WL.SuspensionEndDate
					,WL.CensusDate
				)
			)

			,NationalBreachDate =
				 dateadd(day, (select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS') - WL.KornerWait
				,coalesce(
					 WL.SuspensionEndDate
					,WL.CensusDate
				)
			)

			,BreachTypeCode =
			case
			when
				DiagnosticProcedure.ProcedureCode is not null
			and	upper(WL.Operation) not like '%$T%' then 2 --diagnostic breach
			when
				datediff(day, 
					coalesce(
						dateadd(day,
							 coalesce(RTTAdjustment.AdjustmentDays, 0)
							,RTTClockStart.ClockStartDate
						)
						,WL.RTTStartDate
						,dateadd(day,
							 coalesce(RTTAdjustment.AdjustmentDays, 0)
							,RTTMatch.ReferralDate
						)
						,dateadd(day,
							 coalesce(RTTAdjustment.AdjustmentDays, 0)
							,WL.DateOnWaitingList
						)
					) 
				, WL.CensusDate) > (select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS')
			then 1 --national breach threshold
			else 0 --RTT breach (18 weeks)
			end

			,coalesce(SocialSuspension.DaysSuspended, 0) SocialSuspensionDays

			,ReferralEncounterRecno = RTTMatch.ReferralRecno

		from
			APC.WaitingList WL WITH (NOLOCK)

		left join 
		(
			select
				 SourcePatientNo
				,SourceEncounterNo
				,sum(datediff(day, SuspensionStartDate, SuspensionEndDate)) DaysSuspended
				,CensusDate
			from
				APC.WaitingListSuspension Suspension
			where
				SuspensionReason like '%S*%'
			group by
				 SourcePatientNo
				,SourceEncounterNo
				,CensusDate
		) SocialSuspension
		on	SocialSuspension.SourcePatientNo = WL.SourcePatientNo
		and	SocialSuspension.SourceEncounterNo = WL.SourceEncounterNo
		and	SocialSuspension.CensusDate = WL.CensusDate

		left join 
		(
			select
				 SourcePatientNo
				,SourceEncounterNo
				,sum(datediff(day, SuspensionStartDate, SuspensionEndDate)) DaysSuspended
				,CensusDate
			from
				APC.WaitingListSuspension Suspension
			group by
				 SourcePatientNo
				,SourceEncounterNo
				,CensusDate
		) Suspension
		on	Suspension.SourcePatientNo = WL.SourcePatientNo
		and	Suspension.SourceEncounterNo = WL.SourceEncounterNo
		and	Suspension.CensusDate = WL.CensusDate

		left join 
			(
			select
				 SourcePatientNo SourcePatientNo
				,SourceEncounterNo = SourceEntityRecno
				,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
			from
				RTT.dbo.RTTAdjustment RTTAdjustment
			group by
				 RTTAdjustment.SourcePatientNo
				,RTTAdjustment.SourceEntityRecno
			) RTTAdjustment
		on	RTTAdjustment.SourcePatientNo = WL.SourcePatientNo
		and	RTTAdjustment.SourceEncounterNo = WL.SourceEncounterNo

		left join 
			(
			select
				 RTTClockStartRecno
				,SourcePatientNo SourcePatientNo
				,SourceEncounterNo = SourceEntityRecno
				,ClockStartDate
			from
				RTT.dbo.RTTClockStart RTTClockStart
			where
				not exists
				(
				select
					1
				from
					RTT.dbo.RTTClockStart LatestRTTClockStart
				where
					LatestRTTClockStart.SourcePatientNo = RTTClockStart.SourcePatientNo
				and	LatestRTTClockStart.SourceEntityRecno = RTTClockStart.SourceEntityRecno
				and	(
						LatestRTTClockStart.ClockStartDate > RTTClockStart.ClockStartDate
					or
						(
							LatestRTTClockStart.ClockStartDate = RTTClockStart.ClockStartDate
						and	LatestRTTClockStart.RTTClockStartRecno > RTTClockStart.RTTClockStartRecno
						)
					)
				)
			) RTTClockStart
		on	RTTClockStart.SourcePatientNo = WL.SourcePatientNo
		and	RTTClockStart.SourceEncounterNo = WL.SourceEncounterNo

		left join APC.WaitingList LastRTTWL
		on	LastRTTWL.SourcePatientNo = WL.SourcePatientNo
		and	LastRTTWL.SourceEncounterNo = WL.SourceEncounterNo
		and	LastRTTWL.CensusDate = @RTTCensusDate

		left join RTT.dbo.RTTMatch RTTMatch
		on	RTTMatch.InpatientWLRecno = LastRTTWL.EncounterRecno

		left join RTT.dbo.RTTStatus RTTStatus
		on	RTTStatus.RTTStatusCode = WL.RTTCurrentStatusCode

		left join RTT.dbo.RTTEncounter RTTEncounter
		on	RTTEncounter.SourcePatientNo = WL.SourcePatientNo
		and	RTTEncounter.SourceEntityRecno = WL.SourceEncounterNo

		left join RTT.dbo.DiagnosticProcedure DiagnosticProcedure
		on	DiagnosticProcedure.ProcedureCode = WL.IntendedPrimaryOperationCode

		left join RTT.dbo.EntityBreach RTTDiagnosticBreach
		on	RTTDiagnosticBreach.EntityTypeCode = 'RTTDIAGBREACHDAYS'
		and	RTTDiagnosticBreach.EntityCode = 'RTT'
		and	
		coalesce
			(
				null
				--	 WL.RTTStartDate
				,dateadd(day,
				 coalesce(RTTAdjustment.AdjustmentDays, 0)
				,RTTClockStart.ClockStartDate
				)
				,dateadd(day,
				 coalesce(RTTAdjustment.AdjustmentDays, 0)
				,RTTMatch.ReferralDate
				)
				,dateadd(day,
					 coalesce(RTTAdjustment.AdjustmentDays, 0)
					,WL.DateOnWaitingList
				)
			)
			between RTTDiagnosticBreach.FromDate and coalesce(RTTDiagnosticBreach.ToDate, getdate())

		where
--		and	WL.AdmissionMethodCode in ('WL', 'BL', 'BA', 'EL')
--		and	WL.WLStatus != 'WL Suspend'
			WL.CensusDate = @LocalCensusDate
		) WL
	) WL
on	WL.EncounterRecno = WaitingList.EncounterRecno

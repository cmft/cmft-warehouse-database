﻿
CREATE  Procedure [ETL].[LoadRAD] 
AS

	/******************************************************************************
	**  Name: LoadRAD
	**  Purpose: 
	**
	**  Source stored procedure for updating all Radiology tables from 
	**  CRIS extracts
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:	Author:			Description:
	** --------  ------------	---------------------------------------------------- 
	** 20120626    MH			Altered for CMFT environment
	** 20150813	Paul Egan		Added call to new proc [ETL].[LoadRADAuditYearMonth]
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ImportDateTimeStamp datetime
declare @StagingDB varchar(20)


select @StartTime = getdate()

-- Reference Data
EXEC [ETL].[LoadRADReferenceData]

-- UserBase
DECLARE ImportCursor CURSOR FOR
	SELECT DISTINCT
		ImportDateTimeStamp
	FROM ETL.TLoadRADUserBase
	ORDER BY
		ImportDateTimeStamp

OPEN ImportCursor  
FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC ETL.LoadRADUserBase @ImportDateTimeStamp
	FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp
END
  
CLOSE ImportCursor
DEALLOCATE ImportCursor

-- Event
DECLARE ImportCursor CURSOR FOR
	SELECT DISTINCT
		ImportDateTimeStamp
	FROM ETL.TLoadRADEvent
	ORDER BY
		ImportDateTimeStamp

OPEN ImportCursor  
FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC ETL.LoadRADEvent @ImportDateTimeStamp
	FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp
END
  
CLOSE ImportCursor
DEALLOCATE ImportCursor


-- Exam
DECLARE ImportCursor CURSOR FOR
	SELECT DISTINCT
		ImportDateTimeStamp
	FROM
		ETL.TLoadRADExam
	ORDER BY
		ImportDateTimeStamp


OPEN ImportCursor  
FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp

WHILE @@FETCH_STATUS = 0
BEGIN
	exec ETL.LoadRADExam @ImportDateTimeStamp
	FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp
END
  
CLOSE ImportCursor
DEALLOCATE ImportCursor


---- Report
DECLARE ImportCursor CURSOR FOR
	SELECT DISTINCT
		ImportDateTimeStamp
	FROM
		ETL.TLoadRADReport
	ORDER BY
		ImportDateTimeStamp

OPEN ImportCursor  
FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp

WHILE @@FETCH_STATUS = 0
BEGIN
	exec ETL.LoadRADReport @ImportDateTimeStamp
	FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp
END
  
CLOSE ImportCursor
DEALLOCATE ImportCursor

-- Status
DECLARE ImportCursor CURSOR FOR
	SELECT DISTINCT
		ImportDateTimeStamp
	FROM
		ETL.TLoadRADStatus
	ORDER BY
		ImportDateTimeStamp


OPEN ImportCursor  
FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp

WHILE @@FETCH_STATUS = 0
BEGIN
	exec ETL.LoadRADStatus @ImportDateTimeStamp
	FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp
END
  
CLOSE ImportCursor
DEALLOCATE ImportCursor

-- WaitingList
EXEC ETL.LoadRADWL

/* Added Paul Egan 20150813 */
exec [ETL].[LoadRADAuditYearMonth]

-- Update Run Statistics
delete from Utility.Parameter
where
	Parameter = 'LOADRADDATE'

insert into Utility.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADRADDATE'
	,DateValue = getdate()
 

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'LoadRAD', @Stats, @StartTime


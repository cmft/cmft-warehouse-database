﻿CREATE   procedure [ETL].[LoadOPWL] as

--declare @run varchar(3)

--select
--	@run =
--	case
--	when datepart(dayofyear, getdate()) = 
--	(
--	select 
--		datepart(dayofyear, DateValue) 
--	from 
--		Utility.Parameter 
--	where 
--		Parameter = 'LASTLOADEDOPWL'
--	) then 'No' else 'Yes'
--	end

--if @run = 'Yes'
--begin
	declare @StartTime datetime
	declare @Elapsed int
	declare @Stats varchar(255)

	select @StartTime = getdate()

	declare @CensusDate smalldatetime

	select @CensusDate = dateadd(day, datediff(day, 0, getdate()), -1) -- adjustment for extract running on the following day

	delete from ETL.TImportOPWaitingList

	exec ETL.ExtractInquireOPWaitingList @CensusDate

	exec ETL.LoadOPWaitingList

	--exec ETL.AssignOPWLDirectorateCode

	exec ETL.BuildOutpatientWaitingListPTL @CensusDate

	exec ETL.AssignOPWLWithRTTOpenPathway @CensusDate

	-- reset snapshot table
	delete from OP.Snapshot

	insert into OP.Snapshot 
		(
		CensusDate
		)
	select distinct 
		CensusDate
	from 
		OP.WaitingList


	--load OP Reviews
	delete from ETL.TImportOPWaitingListReview

	exec ETL.ExtractInquireOPWaitingListReview @CensusDate

	exec ETL.LoadOPWaitingListReview


	-- insert the last date loaded - this is used by the load job to determine whether or not to run the load process
	delete from Utility.Parameter where Parameter = 'LASTLOADEDOPWL'

	insert into Utility.Parameter (Parameter, DateValue) values ('LASTLOADEDOPWL', @StartTime /*getdate()*/ )


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@CensusDate, ''))

	exec Utility.WriteAuditLogEvent 'LoadOPWL', @Stats, @StartTime
--end

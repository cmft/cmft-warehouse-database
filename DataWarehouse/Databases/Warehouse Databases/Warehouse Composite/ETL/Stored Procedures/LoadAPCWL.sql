﻿CREATE   procedure [ETL].[LoadAPCWL] as

set dateformat dmy

--declare @run varchar(3)

--select
--	@run =
--	case
--	when datepart(dayofyear, getdate()) = 
--		(
--		select 
--			datepart(dayofyear, DateValue) 
--		from 
--			Utility.Parameter 
--		where 
--			Parameter = 'LASTLOADEDAPCWL'
--		) then 'No' else 'Yes' 
--	end

--if @run = 'Yes'
--begin
	declare @StartTime datetime
	declare @Elapsed int
	declare @Stats varchar(255)

	select @StartTime = getdate()

	declare @CensusDate smalldatetime

	select @CensusDate = dateadd(day, datediff(day, 0, getdate()), 0)

	delete from ETL.TImportAPCWLCurrent
	delete from ETL.TImportAPCWLPreadmission
	delete from ETL.TImportAPCWLSuspension

	exec ETL.ExtractInquireAPCWLCurrent @CensusDate
	exec ETL.ExtractInquireAPCWLPreadmission @CensusDate
	exec ETL.ExtractInquireAPCWLSuspension @CensusDate

	exec ETL.LoadAPCWaitingListSuspension
	exec ETL.LoadAPCWaitingList

--If a patient was admitted and CancelledElectiveAdmissionFlag (CEA) = 1 then a new WL entry is generated on PAS.
--In these circumstances the suspension details are not carried forward to the new WL entry.
--This code looks for previous CEA admissions for the patient and copies forward the supension episodes.

	insert into APC.WaitingListSuspension
	(
		 SourcePatientNo
		,SourceEncounterNo
		,SuspensionStartDate
		,SuspensionEndDate
		,SuspensionReasonCode
		,SuspensionReason
		,CensusDate
		,SourceUniqueID
	)
	select distinct
		 Suspension.SourcePatientNo
		,WaitingList.SourceEncounterNo
		,Suspension.SuspensionStartDate
		,Suspension.SuspensionEndDate
		,Suspension.SuspensionReasonCode
		,Suspension.SuspensionReason
		,WaitingList.CensusDate
		,Inpatient.SourceUniqueID
	from
		APC.WaitingListSuspension Suspension

	inner join APC.WaitingList
	on	WaitingList.SourcePatientNo = Suspension.SourcePatientNo
	and	WaitingList.SourceEncounterNo > Suspension.SourceEncounterNo

	inner join APC.Encounter Inpatient
	on	Inpatient.SourcePatientNo = Suspension.SourcePatientNo
	and	Inpatient.SourceEncounterNo = Suspension.SourceEncounterNo

	where
		Inpatient.CancelledElectiveAdmissionFlag = 1
	and	Inpatient.DateOnWaitingList = WaitingList.OriginalDateOnWaitingList
	and	Inpatient.SpecialtyCode = WaitingList.SpecialtyCode
	and	WaitingList.CensusDate = @CensusDate

	and	not exists
		(
		select
			1
		from
			APC.WaitingListSuspension WLSuspension
		where
			WLSuspension.SourcePatientNo = Suspension.SourcePatientNo
		and	WLSuspension.SourceEncounterNo = WaitingList.SourceEncounterNo
		and	WLSuspension.SuspensionStartDate = Suspension.SuspensionStartDate
		and	WLSuspension.SuspensionEndDate = Suspension.SuspensionEndDate
		and	WLSuspension.CensusDate = WaitingList.CensusDate
		)

	exec ETL.AssignAPCFutureCancellation


	-- get theatre details
	--update
	--	APC.WaitingList
	--set
	--	WaitingList.TheatreKey = TheatreSlot.TheatreKey,
	--	WaitingList.TheatreInterfaceCode = TheatreSlot.InterfaceCode,
	--	WaitingList.ProcedureDate = TheatreSlot.ProcedureDate,
	--	WaitingList.ProcedureTime = TheatreSlot.ProcedureTime,
	--	WaitingList.TheatreCode = TheatreSlot.TheatreCode
	--from
	--	APC.WaitingList WL

	--inner join
	--	(
	--	select distinct
	--		WL.EncounterRecno,
	--		PatientEpisode.SourceUniqueID TheatreKey,
	--		PatientEpisode.InterfaceCode,
	--		PatientEpisode.ProcedureDate,
	--		PatientEpisode.ProcedureTime,
	--		PatientEpisode.TheatreNumber TheatreCode
	--	from
	--		WH.Theatre.PatientEpisode PatientEpisode

	--	inner join APC.WaitingList WL
	--	on	WL.DistrictNo = PatientEpisode.SourcePatientNo
	--	and	PatientEpisode.ProcedureDate >= WL.CensusDate
	--	and	WL.CensusDate = @CensusDate
	--	and	PatientEpisode.CancelReason is null
	--	and	not exists
	--		(
	--		select
	--			1
	--		from
	--			APC.WaitingList WL1
	--		where
	--			WL1.CensusDate = WL.CensusDate
	--		and	WL1.DistrictNo = WL.DistrictNo 
	--		and	WL1.DistrictNo in 
	--			(
	--			select
	--				DistrictNo
	--			from
	--				(
	--				select
	--					WLCount.DistrictNo,
	--					count(*) Value
	--				from
	--					APC.WaitingList WLCount
	--				where
	--					WLCount.CensusDate = WL1.CensusDate
	--				group by
	--					WLCount.DistrictNo
	--				having
	--					count(*) > 1
	--				) DistrictNumbers
	--			)
	--		)
	--	) TheatreSlot
	--on	TheatreSlot.EncounterRecno = WL.EncounterRecno


	-- reset snapshot table
	delete from APC.Snapshot

	insert into APC.Snapshot 
		(
		CensusDate
		)
	select distinct 
		CensusDate
	from 
		APC.WaitingList

	--work out derived breach dates etc.
	--exec ETL.AssignAPCWLBreachDate

	exec ETL.AssignAPCWLDirectorateCode

	exec ETL.AssignAPCWLWithRTTOpenPathway @CensusDate

	-- insert the last date loaded - this is used by the load job to determine whether or not to run the load process
	delete from Utility.Parameter where Parameter = 'LASTLOADEDAPCWL'

	insert into Utility.Parameter (Parameter, DateValue) values ('LASTLOADEDAPCWL', @StartTime /*getdate()*/ )


	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
	select @Stats = 
		'Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
		CONVERT(varchar(11), coalesce(@CensusDate, ''))

	EXEC Utility.WriteAuditLogEvent 'LoadAPCWL', @Stats, @StartTime

--end

﻿
create proc ETL.LoadRADAuditYearMonth

as

/* 
==============================================================================================
Description:	To create a daily snapshot of row counts on the 4 main CRIS transaction tables 
				so we can track the backload of missing data. 

When		Who			What
20150813	Paul Egan	Initial Coding
===============================================================================================
*/


with CalendarCTE as
(
select
	YearMonth = year(c.TheDate) * 100 + month(c.TheDate)
from
	WarehouseOLAPMergedV2.WH.Calendar c
group by
	year(c.TheDate) * 100 + month(c.TheDate)
)

,EventCTE as
(
select
	YearMonth = year(e.EventDate) * 100 + month(e.EventDate)
	,EventCount = count(*)
from
	RAD.Event e
group by
	year(e.EventDate) * 100 + month(e.EventDate)
)

,ExamCTE as
(
select
	YearMonth = year(ex.ExamDate) * 100 + month(ex.ExamDate)
	,ExamCount = count(*)
from
	RAD.Exam ex
group by
	year(ex.ExamDate) * 100 + month(ex.ExamDate)
)


,StatusCTE as
(
select
	YearMonth = year(s.StatusDate) * 100 + month(s.StatusDate)
	,StatusCount = count(*)
from
	RAD.Status s
group by
	year(s.StatusDate) * 100 + month(s.StatusDate)
)


,ReportCTE as
(
select
	YearMonth = year(r.ReportedDate) * 100 + month(r.ReportedDate)
	,ReportCount = count(*)
from
	RAD.Report r
group by
	year(r.ReportedDate) * 100 + month(r.ReportedDate)
)

insert into RAD.AuditYearMonth
	(
	[YearMonth]
	,[EventCount]
	,[ExamCount]
	,[StatusCount]
	,[ReportCount]
	,[SnapshotTime]
	,[CreatedByWhom]
	)
select
	CalendarCTE.YearMonth
	,EventCTE.EventCount
	,ExamCTE.ExamCount
	,StatusCTE.StatusCount
	,ReportCTE.ReportCount
	,SnapshotTime = cast(getdate() as datetime2(0))
	,CreatedByWhom = suser_name()
from
	CalendarCTE
	
	left join EventCTE
	on	EventCTE.YearMonth = CalendarCTE.YearMonth
	
	left join ExamCTE
	on	ExamCTE.YearMonth = CalendarCTE.YearMonth
	
	left join StatusCTE
	on	StatusCTE.YearMonth = CalendarCTE.YearMonth
	
	left join ReportCTE
	on	ReportCTE.YearMonth = CalendarCTE.YearMonth
order by
	YearMonth
;
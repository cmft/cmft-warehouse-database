﻿--exec [ETL].[AssignOPWLWithRTTOpenPathway] '2014-07-22'



CREATE procedure [ETL].[AssignOPWLWithRTTOpenPathway] 
(
	@CensusDate smalldatetime-- ='2014-07-22'
)
--with recompile --(added to test perfomance jr)
as
--DF Modified to use second working table as it was taking 8.75 hours...
--Should be down to less than a minute again now...

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()
select @RowsUpdated = 0

----debug
--declare @CensusDate smalldatetime = '15 Oct 2012'

declare
	@localCensusDate date = 
		(select dateadd(minute , -1 , dateadd(day , 1 , @CensusDate)))
;

truncate table OP.WkRTTPathway

truncate table OP.wkWLTMP;

insert OP.WkRTTPathway

select
	 InternalNo
	,doh_spec
	,WL_EpisodeNo
	,treattype
	,type
	,pathway_start_date_current
	,path_open_days_DNA_adjs
	,pathway_start_date_original
	,pathway_end_date
	,path_closed_days_DNA_adjs
	,DateBefore
from
	RTTLegacy.[ETL].[WkPathwayReturn00]
where
		treattype = 'NON IP'
	and	coalesce(exc , '') = ''
	and	coalesce(diag , '') in ('' , 'N' , 'I')
	AND path_doh_spec not in ('960' , '650' , '950')

;

with
OpenPW
(
	 InternalNo
	,doh_spec
	,WL_EpisodeNo
	,RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode
	,RTTDaysWaiting
)
as
	(
	select
		 InternalNo
		,doh_spec
		,WL_EpisodeNo
		,RTTPathwayStartDateCurrent = 
			pathway_start_date_current
		,RTTWeekBandReturnCode =	
			right(
				'0' +
				cast(
					case
					when PW.type = 'OPEN' then
						case
						when (path_open_days_DNA_adjs - datediff(day , @CensusDate , datebefore) - 1) >= 365 then 52
						else
							case
							when (path_open_days_DNA_adjs - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
							else (path_open_days_DNA_adjs - datediff(day , @CensusDate , datebefore) - 1) / 7
							end
						end
					when PW.type = 'CLOSED' then
						case
						when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) >= 365 then 52	
						else
							case
							when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
							else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) / 7
							end
						end

					end
				as varchar
				)
				,2
			)
			,RTTDaysWaiting =
				case
				when PW.type = 'OPEN' then
					case
					when (path_open_days_DNA_adjs - datediff(day , @CensusDate , datebefore) - 1) < 0 then 0
					else (path_open_days_DNA_adjs - datediff(day , @CensusDate , datebefore) - 1)
					end
				when PW.type = 'CLOSED' then
					case
					when (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1) < 0 then 0
					else (path_closed_days_DNA_adjs - datediff(day , @CensusDate , pathway_end_date) - 1)
					end
				end
	from
		OP.WkRTTPathway PW
	where

		(
			(
				PW.type = 'OPEN'
			And PW.pathway_start_date_current between '1 Jan 2007' and @localCensusDate
			--remove open pathways without a wait time
			and PW.path_open_days_DNA_adjs is not null
			)
		or
			(
				PW.type = 'CLOSED'
			AND PW.pathway_start_date_original >= '1 Jan 2007'
			AND PW.pathway_end_date > @localCensusDate
			and	PW.pathway_start_date_current <= @localCensusDate
			)
		)
	)

,Matched
(
	 CensusDate
	,SourcePatientNo
	,SourceEncounterNo
	,NationalSpecialtyCode
	,RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode 
	,RTTDaysWaiting 
)

as
	(
	SELECT
		 WL.CensusDate
		,WL.SourcePatientNo
		,WL.SourceEncounterNo
		,WL.NationalSpecialtyCode
		,PW.RTTPathwayStartDateCurrent
		,PW.RTTWeekBandReturnCode 
		,PW.RTTDaysWaiting 
	from
		(
		select
			 Encounter.CensusDate
			,Encounter.SourcePatientNo
			,Encounter.SourceEncounterNo
			,Encounter.NationalSpecialtyCode
		from
			Warehouse.OP.WaitingList Encounter
		where
			CensusDate = @CensusDate
		--and	'10' =
		--		case
		--		when rtrim(Encounter.AdmissionMethodCode) = 'PA' then '20'
		--		when rtrim(Encounter.AdmissionMethodCode) = 'PL' then '20'
		--		when Encounter.WLStatus = 'WL Suspend' then '30'
		--		when Encounter.AdminCategoryCode = 'PAY' then '40' --CCB fixed 2012-05-31
		--		else '10'
		--		end
		) WL

	inner join OpenPW PW
	on	WL.SourcePatientNo = PW.InternalNo
	and	WL.SourceEncounterNo = PW.WL_EpisodeNo
	)

----debug


insert 
into OP.wkWLTMP
select
SourceEncounterNo 
,SourcePatientNo
	 ,WithRTTOpenPathway = coalesce(WithRTTOpenPathway , 0)
	,RTTPathwayStartDateCurrent = OpenPathway.RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode = OpenPathway.RTTWeekBandReturnCode 
	,RTTDaysWaiting = OpenPathway.RTTDaysWaiting 
--into OP.wkWLTMP


/*	 WithRTTOpenPathway = coalesce(OpenPathway.WithRTTOpenPathway , 0)
	,RTTPathwayStartDateCurrent = OpenPathway.RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode = OpenPathway.RTTWeekBandReturnCode 
	,RTTDaysWaiting = OpenPathway.RTTDaysWaiting 
	*/
--	WithRTTOpenPathway = coalesce(OpenPathway.WithRTTOpenPathway , 0)
--update Warehouse.OP.WaitingList
--set
/*
	 WaitingList.WithRTTOpenPathway = coalesce(OpenPathway.WithRTTOpenPathway , 0)
	,WaitingList.RTTPathwayStartDateCurrent = OpenPathway.RTTPathwayStartDateCurrent
	,WaitingList.RTTWeekBandReturnCode = OpenPathway.RTTWeekBandReturnCode 
	,WaitingList.RTTDaysWaiting = OpenPathway.RTTDaysWaiting 
*/
from

	(
	select distinct
		 SourcePatientNo
		,SourceEncounterNo
		,WithRTTOpenPathway = 1
		,RTTPathwayStartDateCurrent
		,RTTWeekBandReturnCode 
		,RTTDaysWaiting 
	from
		(
		select
			 SourcePatientNo
			,SourceEncounterNo
			,RTTPathwayStartDateCurrent
			,RTTWeekBandReturnCode 
			,RTTDaysWaiting 
		from
			Matched

		union

		select
			 Unmatched.SourcePatientNo
			,Unmatched.SourceEncounterNo
			,Unmatched.RTTPathwayStartDateCurrent
			,Unmatched.RTTWeekBandReturnCode 
			,Unmatched.RTTDaysWaiting 
		from
			(
			select
				 WaitingList.SourcePatientNo
				,WaitingList.SourceEncounterNo
				,UnmatchedOpen.RTTPathwayStartDateCurrent
				,UnmatchedOpen.RTTWeekBandReturnCode 
				,UnmatchedOpen.RTTDaysWaiting 
			from
				Warehouse.OP.WaitingList

			inner join
				(
				select distinct
					 SourcePatientNo = InternalNo
					,NationalSpecialtyCode = doh_spec
					,RTTPathwayStartDateCurrent
					,RTTWeekBandReturnCode 
					,RTTDaysWaiting 
				from
					OpenPW PW
				where
					not exists
						(
						select
							1
						from
							Matched
						where
							PW.InternalNo = Matched.SourcePatientNo
						and	PW.WL_EpisodeNo = Matched.SourceEncounterNo
						)
				) UnmatchedOpen
			on	UnmatchedOpen.SourcePatientNo = WaitingList.SourcePatientNo
			--and	UnmatchedOpen.NationalSpecialtyCode = WaitingList.NationalSpecialtyCode
			and	WaitingList.CensusDate = @CensusDate
			--and	'10' =
			--			case
			--			when rtrim(WaitingList.AdmissionMethodCode) = 'PA' then '20'
			--			when rtrim(WaitingList.AdmissionMethodCode) = 'PL' then '20'
			--			when WaitingList.WLStatus = 'WL Suspend' then '30'
			--			when WaitingList.AdminCategoryCode = 'PAY' then '40' --CCB fixed 2012-05-31
			--			else '10'
			--			end
			) Unmatched
		) OpenPathway
	) OpenPathway



	
update Warehouse.OP.WaitingList
set
    WaitingList.WithRTTOpenPathway = coalesce(OpenPathway.WithRTTOpenPathway , 0)
	,WaitingList.RTTPathwayStartDateCurrent = OpenPathway.RTTPathwayStartDateCurrent
	,WaitingList.RTTWeekBandReturnCode = OpenPathway.RTTWeekBandReturnCode 
	,WaitingList.RTTDaysWaiting = OpenPathway.RTTDaysWaiting 
from 
Warehouse.OP.WaitingList
  left join OP.wkWLTMP OpenPathway

on	OpenPathway.SourceEncounterNo = WaitingList.SourceEncounterNo
and	OpenPathway.SourcePatientNo = WaitingList.SourcePatientNo

where
	WaitingList.CensusDate = @CensusDate
	
select
	@RowsUpdated = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

select @Stats;


exec Utility.WriteAuditLogEvent 'AssignOPWLWithRTTOpenPathway', @Stats, @StartTime



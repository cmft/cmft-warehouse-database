﻿CREATE procedure [dbo].[AssignOPBreachDate] as


update
	OP.Encounter
set
-- Set the BreachDate to ClockStartDate + RTT breach days

	RTTBreachDate =
		dateadd(
			day
			,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
			,coalesce(
	--manually entered
				 RTTClockStartOP.ClockStartDate
	--otherwise get the earliest date
				,case
				when Encounter.RTTStartDate < Encounter.ReferralDate
				then Encounter.RTTStartDate
				else Encounter.ReferralDate
				end
			)
		)


	,ClockStartDate =
		coalesce(
--manually entered
			 RTTClockStartOP.ClockStartDate
--otherwise get the earliest date
			,case
			when Encounter.RTTStartDate < Encounter.ReferralDate
			then Encounter.RTTStartDate
			else Encounter.ReferralDate
			end
		)


from
	OP.Encounter

left join RTT.dbo.RTTOPClockStop RTTClockStartOP
on    RTTClockStartOP.SourcePatientNo = Encounter.SourcePatientNo
and   RTTClockStartOP.SourceEntityRecno = Encounter.SourceEncounterNo
and	  RTTClockStartOP.ClockStartDate is not null




--	RTTBreachDate =
--		dateadd(
--			day
--			,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
--			,case
--
--			when Encounter.RTTStartDate is not null
--			then Encounter.QM08StartWaitDate
--
--			when datediff(day, Encounter.QM08StartWaitDate, Encounter.AppointmentDate) < 0
--			then
--			coalesce(
--				(
--				select
--					max(MaxEncounter.AppointmentDate) 
--				from
--					OP.Encounter MaxEncounter
--				where
--					MaxEncounter.SourcePatientNo = Encounter.SourcePatientNo
--				and	MaxEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
--				and	MaxEncounter.DNACode in ('2', '3') --patient cancelled or DNA
--				and	MaxEncounter.FirstAttendanceFlag = 1
--				)
--				,Encounter.ReferralDate
--			)
--			else Encounter.ReferralDate
--			end
--		)
--
--	,ClockStartDate =
--		case
--
--		when Encounter.RTTStartDate is not null
--		then Encounter.QM08StartWaitDate
--
--		when datediff(day, Encounter.QM08StartWaitDate, Encounter.AppointmentDate) < 0
--		then
--		coalesce(
--			(
--			select
--				max(MaxEncounter.AppointmentDate) 
--			from
--				OP.Encounter MaxEncounter
--			where
--				MaxEncounter.SourcePatientNo = Encounter.SourcePatientNo
--			and	MaxEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
--			and	MaxEncounter.DNACode in ('2', '3') --patient cancelled or DNA
--			and	MaxEncounter.FirstAttendanceFlag = 1
--			)
--			,Encounter.ReferralDate
--		)
--		else Encounter.ReferralDate
--		end

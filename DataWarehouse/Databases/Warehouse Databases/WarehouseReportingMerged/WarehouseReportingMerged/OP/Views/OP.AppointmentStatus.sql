﻿
create view [OP].[AppointmentStatus] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAppointmentStatusID]
      ,[SourceAppointmentStatusCode]
      ,[SourceAppointmentStatus]
      --,[LocalAppointmentStatusID]
      ,[LocalAppointmentStatusCode]
      ,[LocalAppointmentStatus]
      --,[NationalAppointmentStatusID]
      ,[NationalAppointmentStatusCode]
      ,[NationalAppointmentStatus]
  FROM [WarehouseOLAPMergedV2].[OP].[AppointmentStatus]


﻿
create view [OP].[AttendanceStatus] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAttendanceStatusID]
      ,[SourceAttendanceStatusCode]
      ,[SourceAttendanceStatus]
      --,[LocalAttendanceStatusID]
      ,[LocalAttendanceStatusCode]
      ,[LocalAttendanceStatus]
      --,[NationalAttendanceStatusID]
      ,[NationalAttendanceStatusCode]
      ,[NationalAttendanceStatus]
  FROM [WarehouseOLAPMergedV2].[OP].[AttendanceStatus]


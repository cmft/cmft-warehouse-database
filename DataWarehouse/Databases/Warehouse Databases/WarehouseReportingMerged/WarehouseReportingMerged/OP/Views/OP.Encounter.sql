﻿


CREATE view OP.Encounter  as


select
	MergeEncounterRecno
	,EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,NHSNumberStatusCode
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferralConsultantCode
	,ReferralSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryProcedureDate
	,SecondaryProcedureCode1
	,SecondaryProcedureDate1
	,SecondaryProcedureCode2
	,SecondaryProcedureDate2
	,SecondaryProcedureCode3
	,SecondaryProcedureDate3
	,SecondaryProcedureCode4
	,SecondaryProcedureDate4
	,SecondaryProcedureCode5
	,SecondaryProcedureDate5
	,SecondaryProcedureCode6
	,SecondaryProcedureDate6
	,SecondaryProcedureCode7
	,SecondaryProcedureDate7
	,SecondaryProcedureCode8
	,SecondaryProcedureDate8
	,SecondaryProcedureCode9
	,SecondaryProcedureDate9
	,SecondaryProcedureCode10
	,SecondaryProcedureDate10
	,SecondaryProcedureCode11
	,SecondaryProcedureDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode
	,AgeCode
	,HRGCode
	,Cases
	,LengthOfWait
	,IsWardAttender
	,RTTActivity
	,RTTBreachStatusCode
	,ClockStartDate
	,RTTBreachDate
	,RTTTreated
	,DirectorateCode
	,ReferralSpecialtyTypeCode
	,LastDNAorPatientCancelledDate
	,ReferredByCode
	,ReferrerCode
	,AppointmentOutcomeCode
	,MedicalStaffTypeCode
	,ContextCode
	,TreatmentFunctionCode
	,CommissioningSerialNo
	,DerivedFirstAttendanceFlag
	,AttendanceIdentifier
	,RegisteredGpAtAppointmentCode
	,RegisteredGpPracticeAtAppointmentCode
	,ReferredByConsultantCode
	,ReferredByGpCode
	,ReferredByGdpCode
	,PseudoPostcode
	,CCGCode
	,PostcodeAtAppointment
	,EpisodicPostcode
	,GpCodeAtAppointment
	,GpPracticeCodeAtAppointment
	,ContextID
	,SexID
	,AgeID
	,NHSNumberStatusID
	,EthnicOriginID
	,MaritalStatusID
	,ReligionID
	,SiteID
	,AppointmentDateID
	,ClinicID
	,AdminCategoryID
	,SourceOfReferralID
	,ReasonForReferralID
	,PriorityID
	,FirstAttendanceID
	,AttendanceStatusID
	,AttendanceOutcomeID
	,AppointmentTypeID
	,ConsultantID
	,SpecialtyID
	,ReferralConsultantID
	,ReferralSpecialtyID
	,RTTSpecialtyID
	,RTTCurrentStatusID
	,ReferringConsultantID
	,TreatmentFunctionID
	,DerivedFirstAttendanceID
	,AppointmentResidenceCCGID
	,AppointmentResidenceCCGCode
	,ContractPointOfDeliveryID
	,ContractFlagID
	,ContractHRGID
	,ContractSpecialtyID
from
	WarehouseOLAPMergedV2.OP.Encounter




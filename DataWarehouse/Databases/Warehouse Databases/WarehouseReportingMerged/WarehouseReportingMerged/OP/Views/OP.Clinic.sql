﻿

CREATE view [OP].[Clinic] as
select
	[SourceContextCode]
	,[SourceContext]
	,[SourceClinicID]
	,[SourceClinicCode]
	,[SourceClinic]
	--,[LocalClinicID]
	,[LocalClinicCode]
	,[LocalClinic]
	--,[NationalClinicID]
	,[NationalClinicCode]
	,[NationalClinic]
	,ReportToLocationCode
from
	[WarehouseOLAPMergedV2].[OP].[Clinic]



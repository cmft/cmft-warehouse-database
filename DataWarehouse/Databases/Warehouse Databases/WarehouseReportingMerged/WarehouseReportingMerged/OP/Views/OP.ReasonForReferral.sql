﻿
create view [OP].[ReasonForReferral] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceReasonForReferralID]
      ,[SourceReasonForReferralCode]
      ,[SourceReasonForReferral]
      --,[LocalReasonForReferralID]
      ,[LocalReasonForReferralCode]
      ,[LocalReasonForReferral]
      --,[NationalReasonForReferralID]
      ,[NationalReasonForReferralCode]
      ,[NationalReasonForReferral]
  FROM [WarehouseOLAPMergedV2].[OP].[ReasonForReferral]


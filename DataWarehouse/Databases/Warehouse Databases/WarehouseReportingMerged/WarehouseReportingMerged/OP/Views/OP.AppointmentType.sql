﻿
create view [OP].[AppointmentType] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAppointmentTypeID]
      ,[SourceAppointmentTypeCode]
      ,[SourceAppointmentType]
      --,[LocalAppointmentTypeID]
      ,[LocalAppointmentTypeCode]
      ,[LocalAppointmentType]
      --,[NationalAppointmentTypeID]
      ,[NationalAppointmentTypeCode]
      ,[NationalAppointmentType]
  FROM [WarehouseOLAPMergedV2].[OP].[AppointmentType]


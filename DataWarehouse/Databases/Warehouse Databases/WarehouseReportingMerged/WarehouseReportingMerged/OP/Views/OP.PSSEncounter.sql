﻿create view [OP].[PSSEncounter]

as

SELECT [MergeEncounterRecno]
      ,[NationalProgrammeOfCareServiceCode]
      ,[ServiceLine]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[OP].[PSSEncounter]

﻿

create Procedure OP.[MissingNHSNo] 
	@FromDate date
	,@ToDate date
as 

-- 20160411	RR created on behalf of Waiken Chan

SELECT [SourcePatientNo]
      ,[PatientTitle]
      ,[PatientForename]
      ,[PatientSurname]
      ,[DateOfBirth]
      ,[DateOfDeath]
      ,[SexCode]
      ,[NHSNumber]
      ,[NHSNumberStatusCode]
      ,[DistrictNo]
      ,[Postcode]
      ,[PatientAddress1]
      ,[PatientAddress2]
      ,[PatientAddress3]
      ,[PatientAddress4]
      ,[RegisteredGpCode]
      ,[RegisteredGpPracticeCode]
      ,[SiteCode]
      ,[AppointmentDate]
      ,[AppointmentTime]
      ,[ClinicCode]
      ,[AppointmentStatusCode]
      ,[ConsultantCode]
      ,[SpecialtyCode]
      ,[CasenoteNo]
      ,[ReferralDate]
      ,[DischargeDate]
      ,[DirectorateCode]
  FROM [OP].[Encounter]
  Where AppointmentDate >= @FromDate
  And AppointmentDate >= @ToDate
  And NHSNumber is NULL
  And AppointmentStatusCode = 'ATT'
  

﻿



CREATE View [APCUpdate].[Encounter]
as


Select 
	--MergeEncounterRecno
	 EncounterRecno
	, SourceUniqueID
	, SourcePatientNo
	, SourceSpellNo
	, SourceEncounterNo
	, ProviderSpellNo
	, PatientTitle
	, PatientForename
	, PatientSurname
	, DateOfBirth
	, DateOfDeath
	, SexCode
	, NHSNumber
	, Postcode
	, PatientAddress1
	, PatientAddress2
	, PatientAddress3
	, PatientAddress4
	, DHACode
	, EthnicOriginCode
	, MaritalStatusCode
	, ReligionCode
	, DateOnWaitingList
	, AdmissionDate
	, DischargeDate
	, EpisodeStartDate
	, EpisodeEndDate
	, StartSiteCode
	, StartWardTypeCode
	, EndSiteCode
	, EndWardTypeCode
	, RegisteredGpCode
	, RegisteredGpPracticeCode
	, SiteCode
	, AdmissionMethodCode
	, AdmissionSourceCode
	, PatientClassificationCode
	, ManagementIntentionCode
	, DischargeMethodCode
	, DischargeDestinationCode
	, AdminCategoryCode
	, ConsultantCode
	, SpecialtyCode
	, FirstRegDayOrNightAdmit
	, NeonatalLevelOfCare
	, PASHRGCode
	, PrimaryDiagnosisCode
	, SubsidiaryDiagnosisCode
	, SecondaryDiagnosisCode1
	, SecondaryDiagnosisCode2
	, SecondaryDiagnosisCode3
	, SecondaryDiagnosisCode4
	, SecondaryDiagnosisCode5
	, SecondaryDiagnosisCode6
	, SecondaryDiagnosisCode7
	, SecondaryDiagnosisCode8
	, SecondaryDiagnosisCode9
	, SecondaryDiagnosisCode10
	, SecondaryDiagnosisCode11
	, SecondaryDiagnosisCode12
	,PrimaryProcedureCode = PrimaryOperationCode
	,PrimaryProcedureDate = PrimaryOperationDate
	,SecondaryProcedureCode1 = SecondaryOperationCode1
	,SecondaryProcedureDate1 = SecondaryOperationDate1
	,SecondaryProcedureCode2 = SecondaryOperationCode2
	,SecondaryProcedureDate2 = SecondaryOperationDate2
	,SecondaryProcedureCode3 = SecondaryOperationCode3
	,SecondaryProcedureDate3 = SecondaryOperationDate3
	,SecondaryProcedureCode4 = SecondaryOperationCode4
	,SecondaryProcedureDate4 = SecondaryOperationDate4
	,SecondaryProcedureCode5 = SecondaryOperationCode5
	,SecondaryProcedureDate5 = SecondaryOperationDate5
	,SecondaryProcedureCode6 = SecondaryOperationCode6
	,SecondaryProcedureDate6 = SecondaryOperationDate6
	,SecondaryProcedureCode7 = SecondaryOperationCode7
	,SecondaryProcedureDate7 = SecondaryOperationDate7
	,SecondaryProcedureCode8 = SecondaryOperationCode8
	,SecondaryProcedureDate8 = SecondaryOperationDate8
	,SecondaryProcedureCode9 = SecondaryOperationCode9
	,SecondaryProcedureDate9 = SecondaryOperationDate9
	,SecondaryProcedureCode10 = SecondaryOperationCode10
	,SecondaryProcedureDate10 = SecondaryOperationDate10
	,SecondaryProcedureCode11 = SecondaryOperationCode11
	,SecondaryProcedureDate11 = SecondaryOperationDate11
	, OperationStatusCode
	, ContractSerialNo
	, CodingCompleteDate
	, PurchaserCode
	, ProviderCode
	, EpisodeStartTime
	, EpisodeEndTime
	, RegisteredGdpCode
	, EpisodicGpCode
	, InterfaceCode
	, CasenoteNumber
	, NHSNumberStatusCode
	, AdmissionTime
	, DischargeTime
	, TransferFrom
	, DistrictNo
	, ExpectedLOS
	, MRSAFlag
	, RTTPathwayID
	, RTTPathwayCondition
	, RTTStartDate
	, RTTEndDate
	, RTTSpecialtyCode
	, RTTCurrentProviderCode
	, RTTCurrentStatusCode
	, RTTCurrentStatusDate
	, RTTCurrentPrivatePatientFlag
	, RTTOverseasStatusFlag
	, RTTPeriodStatusCode
	, IntendedPrimaryProcedureCode = IntendedPrimaryOperationCode
	, Operation
	, Research1
	, CancelledElectiveAdmissionFlag
	, LocalAdminCategoryCode
	, EpisodicGpPracticeCode
	, PCTCode
	, LocalityCode
	 ,AgeCode = WarehouseOLAPMergedV2.Utility.fn_CalcAgeCode (DateOfBirth,GETDATE())
	 ,CategoryCode =
		case
			when ManagementIntentionCode = 'I' then '10'
			when ManagementIntentionCode = 'D' then '20'
			when ManagementIntentionCode = 'R' then '24'
			else '28'
		end
	,LOS = datediff(day, AdmissionDate, GETDATE()	)
	,Cases = 1
	, FirstEpisodeInSpellIndicator = 
		Case
			when FirstEpisodeInSpellIndicator = 'Y' then 1
			else 0
		end
	, StartDirectorateCode
	, EndDirectorateCode
	, ResidencePCTCode
	, ISTAdmissionSpecialtyCode
	, ISTAdmissionDemandTime
	, ISTDischargeTime
	, PatientCategoryCode
	, Research2
	, ReferredByCode
	, ReferrerCode
	, DecidedToAdmitDate
	, DischargeReadyDate
	, ContextCode = 'CEN||PAS'
	, Created
	, Updated
	, ByWhom
from 
	--WarehouseOLAPMergedV2.APCUpdate.BaseEncounter -- Discussed with DG who advised to use WH until the Trafford data was ready to be brought in.
	Warehouse.APCUpdate.Encounter
	



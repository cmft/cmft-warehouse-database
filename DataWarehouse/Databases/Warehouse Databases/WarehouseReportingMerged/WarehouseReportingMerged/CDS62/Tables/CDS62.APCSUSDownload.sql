﻿CREATE TABLE [CDS62].[APCSUSDownload] (
    [Generated Record Identifier]                                      FLOAT (53)     NULL,
    [PBR Spell ID]                                                     FLOAT (53)     NULL,
    [Reason for Access]                                                FLOAT (53)     NULL,
    [CDS Type]                                                         FLOAT (53)     NULL,
    [Protocol Identifier]                                              FLOAT (53)     NULL,
    [Unique CDS Identifier]                                            NVARCHAR (255) NULL,
    [Update Type]                                                      FLOAT (53)     NULL,
    [Bulk Replacement CDS Group]                                       NVARCHAR (255) NULL,
    [Test Indicator]                                                   NVARCHAR (255) NULL,
    [Applicable Date/Time]                                             NVARCHAR (255) NULL,
    [Census Date]                                                      NVARCHAR (255) NULL,
    [Extract Date/Time]                                                NVARCHAR (255) NULL,
    [Report Period Start Date]                                         NVARCHAR (255) NULL,
    [Report Period End Date]                                           NVARCHAR (255) NULL,
    [Organisation Code: Sender of Transaction]                         NVARCHAR (255) NULL,
    [Organisation Code Type of Sender]                                 NVARCHAR (255) NULL,
    [Submission Date]                                                  NVARCHAR (255) NULL,
    [CDS Interchange ID]                                               NVARCHAR (255) NULL,
    [Local Patient Identifier]                                         FLOAT (53)     NULL,
    [Organisation Code (Local Patient Identifier)]                     NVARCHAR (255) NULL,
    [Organisation Code Type (Local Patient Identifier)]                NVARCHAR (255) NULL,
    [NHS Number]                                                       FLOAT (53)     NULL,
    [Date of Birth]                                                    FLOAT (53)     NULL,
    [Birth Weight]                                                     NVARCHAR (255) NULL,
    [Live or Still Birth]                                              NVARCHAR (255) NULL,
    [Carer Support Indicator]                                          NVARCHAR (255) NULL,
    [Legal Status Classification on Admission (Psychiatric Census Onl] FLOAT (53)     NULL,
    [Ethnic Group]                                                     NVARCHAR (255) NULL,
    [Marital Status (Psychiatric Census Only)]                         NVARCHAR (255) NULL,
    [NHS Number Trace Status]                                          FLOAT (53)     NULL,
    [Withheld Identity Reason]                                         NVARCHAR (255) NULL,
    [Sex]                                                              FLOAT (53)     NULL,
    [Pregnancy Total Previous Pregnancies]                             NVARCHAR (255) NULL,
    [Name Format Code]                                                 NVARCHAR (255) NULL,
    [Patient Name]                                                     NVARCHAR (255) NULL,
    [Person Title]                                                     NVARCHAR (255) NULL,
    [Person Given Name]                                                NVARCHAR (255) NULL,
    [Person Family Name]                                               NVARCHAR (255) NULL,
    [Person Name Suffix]                                               NVARCHAR (255) NULL,
    [Person Initials]                                                  NVARCHAR (255) NULL,
    [Address Format Code]                                              NVARCHAR (255) NULL,
    [Patient Usual Address]                                            NVARCHAR (255) NULL,
    [Postcode]                                                         NVARCHAR (255) NULL,
    [Organisation Code (Residence Responsibility)]                     NVARCHAR (255) NULL,
    [PCT of Residence]                                                 NVARCHAR (255) NULL,
    [Organisation Code Type (PCT of Residence)]                        NVARCHAR (255) NULL,
    [OSV Classification at CDS Activity Date]                          NVARCHAR (255) NULL,
    [Overseas Visitor Status Classification 1]                         NVARCHAR (255) NULL,
    [Overseas Visitor Status Start Date 1]                             NVARCHAR (255) NULL,
    [Overseas Visitor Status End Date 1]                               NVARCHAR (255) NULL,
    [Overseas Visitor Status Classification 2]                         NVARCHAR (255) NULL,
    [Overseas Visitor Status Start Date 2]                             NVARCHAR (255) NULL,
    [Overseas Visitor Status End Date 2]                               NVARCHAR (255) NULL,
    [Overseas Visitor Status Classification 3]                         NVARCHAR (255) NULL,
    [Overseas Visitor Status Start Date 3]                             NVARCHAR (255) NULL,
    [Overseas Visitor Status End Date 3]                               NVARCHAR (255) NULL,
    [Overseas Visitor Status Classification 4]                         NVARCHAR (255) NULL,
    [Overseas Visitor Status Start Date 4]                             NVARCHAR (255) NULL,
    [Overseas Visitor Status End Date 4]                               NVARCHAR (255) NULL,
    [Overseas Visitor Status Classification 5]                         NVARCHAR (255) NULL,
    [Overseas Visitor Status Start Date 5]                             NVARCHAR (255) NULL,
    [Overseas Visitor Status End Date 5]                               NVARCHAR (255) NULL,
    [Hospital Provider Spell Number]                                   NVARCHAR (255) NULL,
    [Administrative Category]                                          FLOAT (53)     NULL,
    [Patient Classification]                                           FLOAT (53)     NULL,
    [Admission Method (Hospital Provider Spell)]                       FLOAT (53)     NULL,
    [Discharge Destination (Hospital Provider Spell)]                  FLOAT (53)     NULL,
    [Discharge Method (Hospital Provider Spell)]                       FLOAT (53)     NULL,
    [Source of Admission (Hospital Provider Spell)]                    FLOAT (53)     NULL,
    [Start Date (Hospital Provider Spell)]                             FLOAT (53)     NULL,
    [Start Time (Hospital Provider Spell)]                             DATETIME       NULL,
    [Discharge Date (From Hospital Provider Spell)]                    FLOAT (53)     NULL,
    [Discharge Time (Hospital Provider Spell)]                         DATETIME       NULL,
    [Discharge To Hospital At Home Service Indicator]                  NVARCHAR (255) NULL,
    [Episode Number]                                                   FLOAT (53)     NULL,
    [First Regular Day Night Admission]                                FLOAT (53)     NULL,
    [Last Episode In Spell Indicator]                                  FLOAT (53)     NULL,
    [Neonatal Level of Care]                                           FLOAT (53)     NULL,
    [Operation Status]                                                 FLOAT (53)     NULL,
    [Psychiatric Patient Status]                                       FLOAT (53)     NULL,
    [Start Date (Consultant Episode)]                                  FLOAT (53)     NULL,
    [Start Time (Episode)]                                             DATETIME       NULL,
    [End Date (Consultant Episode)]                                    FLOAT (53)     NULL,
    [End Time (Episode)]                                               DATETIME       NULL,
    [Length Of Stay Adjustment (Rehabilitation)]                       NVARCHAR (255) NULL,
    [Length Of Stay Adjustment (Specialist Palliative Care)]           NVARCHAR (255) NULL,
    [Commissioning Serial Number]                                      NVARCHAR (255) NULL,
    [NHS Service Agreement Line Number]                                NVARCHAR (255) NULL,
    [Provider Reference Number]                                        NVARCHAR (255) NULL,
    [Commissioner Reference Number]                                    FLOAT (53)     NULL,
    [Organisation Code Code of Provider]                               NVARCHAR (255) NULL,
    [Organisation Code Type of Provider]                               NVARCHAR (255) NULL,
    [Organisation Code Code of Commissioner]                           NVARCHAR (255) NULL,
    [Organisation Code Type of Commissioner]                           NVARCHAR (255) NULL,
    [Consultant Code]                                                  NVARCHAR (255) NULL,
    [Main Specialty Code]                                              FLOAT (53)     NULL,
    [Treatment Function Code]                                          FLOAT (53)     NULL,
    [Local Sub Specialty Code]                                         FLOAT (53)     NULL,
    [Multi-Professional Or Multidisciplinary Ind Code]                 FLOAT (53)     NULL,
    [Rehabilitation Assessment Team Type]                              NVARCHAR (255) NULL,
    [Diagnosis Scheme In Use (ICD)]                                    FLOAT (53)     NULL,
    [Diagnosis Primary (ICD)]                                          NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 1]                            NVARCHAR (255) NULL,
    [Diagnosis 1st Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 2]                            NVARCHAR (255) NULL,
    [Diagnosis 2nd Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 3]                            NVARCHAR (255) NULL,
    [Diagnosis 3rd Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 4]                            NVARCHAR (255) NULL,
    [Diagnosis 4th Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 5]                            NVARCHAR (255) NULL,
    [Diagnosis 5th Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 6]                            NVARCHAR (255) NULL,
    [Diagnosis 6th Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 7]                            NVARCHAR (255) NULL,
    [Diagnosis 7th Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 8]                            NVARCHAR (255) NULL,
    [Diagnosis 8th Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 9]                            NVARCHAR (255) NULL,
    [Diagnosis 9th Secondary (ICD)]                                    NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 10]                           NVARCHAR (255) NULL,
    [Diagnosis 10th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 11]                           NVARCHAR (255) NULL,
    [Diagnosis 11th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 12]                           NVARCHAR (255) NULL,
    [Diagnosis 12th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 13]                           NVARCHAR (255) NULL,
    [Diagnosis 13th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 14]                           NVARCHAR (255) NULL,
    [Diagnosis 14th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 15]                           NVARCHAR (255) NULL,
    [Diagnosis 15th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 16]                           NVARCHAR (255) NULL,
    [Diagnosis 16th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 17]                           NVARCHAR (255) NULL,
    [Diagnosis 17th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 18]                           NVARCHAR (255) NULL,
    [Diagnosis 18th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 19]                           NVARCHAR (255) NULL,
    [Diagnosis 19th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 20]                           NVARCHAR (255) NULL,
    [Diagnosis 20th Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 21]                           NVARCHAR (255) NULL,
    [Diagnosis 21st Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 22]                           NVARCHAR (255) NULL,
    [Diagnosis 22nd Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 23]                           NVARCHAR (255) NULL,
    [Diagnosis 23rd Secondary (ICD)]                                   NVARCHAR (255) NULL,
    [Present On Admission Indicator Diag 24]                           NVARCHAR (255) NULL,
    [Procedure Scheme In Use (OPCS)]                                   FLOAT (53)     NULL,
    [Primary Procedure (OPCS)]                                         NVARCHAR (255) NULL,
    [Primary Procedure Date (OPCS)]                                    FLOAT (53)     NULL,
    [Main Operating Healthcare Professional Code Opcs 1]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 1]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 1]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 1]                         NVARCHAR (255) NULL,
    [2nd Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [2nd Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 2]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 2]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 2]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 2]                         NVARCHAR (255) NULL,
    [3rd Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [3rd Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 3]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 3]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 3]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 3]                         NVARCHAR (255) NULL,
    [4th Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [4th Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 4]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 4]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 4]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 4]                         NVARCHAR (255) NULL,
    [5th Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [5th Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 5]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 5]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 5]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 5]                         NVARCHAR (255) NULL,
    [6th Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [6th Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 6]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 6]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 6]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 6]                         NVARCHAR (255) NULL,
    [7th Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [7th Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 7]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 7]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 7]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 7]                         NVARCHAR (255) NULL,
    [8th Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [8th Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 8]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 8]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 8]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 8]                         NVARCHAR (255) NULL,
    [9th Procedure (OPCS)]                                             NVARCHAR (255) NULL,
    [9th Procedure Date (OPCS)]                                        NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 9]               NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 9]                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 9]                             NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 9]                         NVARCHAR (255) NULL,
    [10th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [10th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 10]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 10]                    NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 10]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 10]                        NVARCHAR (255) NULL,
    [11th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [11th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 11]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 11]                    NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 11]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 11]                        NVARCHAR (255) NULL,
    [12th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [12th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 12]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 12]                    NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 12]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 12]                        NVARCHAR (255) NULL,
    [13th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [13th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 13]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 13]                    NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 13]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 13]                        NVARCHAR (255) NULL,
    [14th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [14th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 14]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 14]                    NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 14]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 14]                        NVARCHAR (255) NULL,
    [15th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [15th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 15]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 15]                    NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 15]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 15]                        NVARCHAR (255) NULL,
    [16th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [16th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 16]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 16]                    NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 16]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 16]                        NVARCHAR (255) NULL,
    [17th Procedure (OPCS)]                                            NVARCHAR (255) NULL,
    [17th Procedure Date (OPCS)]                                       NVARCHAR (255) NULL,
    [Main Operating Healthcare Professional Code Opcs 17]              NVARCHAR (255) NULL,
    [Professional Registration Issuer Code Opcs 17]                    NVARCHAR (255) NULL,
    [General Medical Practitioner Code of Registered GMP]              NVARCHAR (255) NULL,
    [Practice Code of Registered GP]                                   NVARCHAR (255) NULL,
    [Labour/Delivery Onset Method]                                     NVARCHAR (255) NULL,
    [Responsible Anaesthetist Code Opcs 17]                            NVARCHAR (255) NULL,
    [Responsible Anaesthetist Reg Body Opcs 17]                        NVARCHAR (255) NULL
);


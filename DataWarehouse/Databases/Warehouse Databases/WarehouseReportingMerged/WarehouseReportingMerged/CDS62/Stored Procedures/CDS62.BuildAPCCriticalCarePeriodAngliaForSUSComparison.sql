﻿
CREATE procedure [CDS62].[BuildAPCCriticalCarePeriodAngliaForSUSComparison]
	@SubmittedDate date = null
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare @RowsInserted Int
declare @Created datetime

select
	@SubmittedDate =
		coalesce(
			 @SubmittedDate
			,getdate()
		)

	,@Created = GETDATE()

insert
into
	CDS62.APCCriticalCarePeriodAngliaForSUSComparison
(
	 UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,RecordType
	,NHSNumber
	,PeriodID
	,StartDate
	,StartTime
	,UnitFunctionCode
	,GestationLength
	,ActivityDate
	,PersonWeight
	,ActivityCode1
	,ActivityCode2
	,ActivityCode3
	,ActivityCode4
	,ActivityCode5
	,ActivityCode6
	,ActivityCode7
	,ActivityCode8
	,ActivityCode9
	,ActivityCode10
	,ActivityCode11
	,ActivityCode12
	,ActivityCode13
	,ActivityCode14
	,ActivityCode15
	,ActivityCode16
	,ActivityCode17
	,ActivityCode18
	,ActivityCode19
	,ActivityCode20
	,HighCostDrugCode1
	,HighCostDrugCode2
	,HighCostDrugCode3
	,HighCostDrugCode4
	,HighCostDrugCode5
	,HighCostDrugCode6
	,HighCostDrugCode7
	,HighCostDrugCode8
	,HighCostDrugCode9
	,HighCostDrugCode10
	,HighCostDrugCode11
	,HighCostDrugCode12
	,HighCostDrugCode13
	,HighCostDrugCode14
	,HighCostDrugCode15
	,HighCostDrugCode16
	,HighCostDrugCode17
	,HighCostDrugCode18
	,HighCostDrugCode19
	,HighCostDrugCode20
	,EncounterChecksum
	,SubmittedDate
	,Created
	,ByWhom

)
select
	 Submitted.UniqueEpisodeSerialNo
	,Submitted.UpdateType
	,Submitted.CDSUpdateDate
	,Submitted.CDSUpdateTime
	,Submitted.CDSType
	,Submitted.RecordType
	,Submitted.NHSNumber
	,Submitted.PeriodID
	,Submitted.StartDate
	,Submitted.StartTime
	,Submitted.UnitFunctionCode
	,Submitted.GestationLength
	,Submitted.ActivityDate
	,Submitted.PersonWeight
	,Submitted.ActivityCode1
	,Submitted.ActivityCode2
	,Submitted.ActivityCode3
	,Submitted.ActivityCode4
	,Submitted.ActivityCode5
	,Submitted.ActivityCode6
	,Submitted.ActivityCode7
	,Submitted.ActivityCode8
	,Submitted.ActivityCode9
	,Submitted.ActivityCode10
	,Submitted.ActivityCode11
	,Submitted.ActivityCode12
	,Submitted.ActivityCode13
	,Submitted.ActivityCode14
	,Submitted.ActivityCode15
	,Submitted.ActivityCode16
	,Submitted.ActivityCode17
	,Submitted.ActivityCode18
	,Submitted.ActivityCode19
	,Submitted.ActivityCode20
	,Submitted.HighCostDrugCode1
	,Submitted.HighCostDrugCode2
	,Submitted.HighCostDrugCode3
	,Submitted.HighCostDrugCode4
	,Submitted.HighCostDrugCode5
	,Submitted.HighCostDrugCode6
	,Submitted.HighCostDrugCode7
	,Submitted.HighCostDrugCode8
	,Submitted.HighCostDrugCode9
	,Submitted.HighCostDrugCode10
	,Submitted.HighCostDrugCode11
	,Submitted.HighCostDrugCode12
	,Submitted.HighCostDrugCode13
	,Submitted.HighCostDrugCode14
	,Submitted.HighCostDrugCode15
	,Submitted.HighCostDrugCode16
	,Submitted.HighCostDrugCode17
	,Submitted.HighCostDrugCode18
	,Submitted.HighCostDrugCode19
	,Submitted.HighCostDrugCode20
	,Submitted.EncounterChecksum

	,SubmittedDate = @SubmittedDate
	,Created = @Created
	,ByWhom = system_user
from
	CDS62.APCCriticalCarePeriodAngliaBase Submitted
where 
	ActivityDate is not null

select @RowsInserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + 
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


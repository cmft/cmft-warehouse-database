﻿
create procedure [CDS62].[BuildOPAngliaForSUSComparison]
	@SubmittedDate date = null
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare @RowsInserted Int
declare @Created datetime

select
	@SubmittedDate =
		coalesce(
			 @SubmittedDate
			,getdate()
		)

	,@Created = GETDATE()

insert
into
	CDS62.OPAngliaForSUSComparison
(
	 UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,NameFormatCode
	,PatientsAddress
	,Postcode
	,HAofResidenceCode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,DateOfBirthStatus
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,ReferralCode
	,ReferringOrganisationCode
	,ServiceTypeRequested
	,ReferralRequestReceivedDate
	,ReferralRequestReceivedDateStatus
	,PriorityType
	,SourceOfReferralCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,LocalSubSpecialtyCode
	,ClinicPurpose
	,ConsultantCode
	,AttendanceIdentifier
	,AdminCategoryCode
	,SiteCode
	,MedicalStaffTypeCode
	,AttendanceDate
	,FirstAttendanceFlag
	,DNAFlag
	,AttendanceOutcomeCode
	,LastDNAorPatientCancelledDate
	,LastDNAorPatientCancelledDateStatus
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,ReadPrimaryDiagnosisCode
	,ReadSecondaryDiagnosisCode1
	,OperationStatus
	,PrimaryOperationCode
	,OperationCode2
	,OperationCode3
	,OperationCode4
	,OperationCode5
	,OperationCode6
	,OperationCode7
	,OperationCode8
	,OperationCode9
	,OperationCode10
	,OperationCode11
	,OperationCode12
	,ReadPrimaryOperationCode
	,ReadOperationCode2
	,ReadOperationCode3
	,ReadOperationCode4
	,ReadOperationCode5
	,ReadOperationCode6
	,ReadOperationCode7
	,ReadOperationCode8
	,ReadOperationCode9
	,ReadOperationCode10
	,ReadOperationCode11
	,ReadOperationCode12
	,PatientSexAgeMix
	,IntendedClinicalCareIntensity
	,BroadPatientGroup
	,CasenoteNumber
	,EthnicGroupCode
	,AttendanceCategoryCode
	,AppointmentRequiredDate
	,MaritalStatusCode
	,GPDiagnosisCode
	,ReadGPDiagnosisCode
	,PrimaryOperationGroupCode
	,SecondaryOperationGroupCode1
	,SecondaryOperationGroupCode2
	,SecondaryProcedureGroupCode3
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,ReadSecondaryDiagnosisCode2
	,ReadSecondaryDiagnosisCode3
	,ReadSecondaryDiagnosisCode4
	,FundingType
	,GPFundholderCode
	,AppointmentHospitalCode
	,ReferralHospitalCode
	,NNNStatusIndicator
	,BlankPad
	,CancellationDate
	,CancelledBy
	,DeathIndicator
	,LocalClinicCode
	,LocalWardCode
	,PCTHealthAuthorityCode
	,PCTofResidenceCode
	,ResponsiblePCTHealthAuthorityCode
	,CommissionerCode
	,AgencyActingOnBehalfOfDoH
	,DerivedFirstAttendance
	,PCTResponsible
	,AgeAtCDSActivityDate
	,UniqueBookingReferenceNumber
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,EarliestReasonableOfferDate
	,EndOfRecord
	,SubmittedDate
	,Created
	,ByWhom
	,ResidenceResponsibilityCode
	,MultiDisciplinaryIndicationCode
	,RehabilitationAssessmentTeamTypeCode
	,ConsultationMediumUsedCode
	,DirectAccessReferralIndicatorCode
	,PrimaryDiagnosisPresentOnAdmissionIndicator
	,SecondaryDiagnosis1PresentOnAdmissionIndicator
	,SecondaryDiagnosis2PresentOnAdmissionIndicator
	,SecondaryDiagnosis3PresentOnAdmissionIndicator
	,SecondaryDiagnosis4PresentOnAdmissionIndicator
	,MainOperatingProfessionalCode
	,MainOperatingProfessionalRegistrationIssuerCode
	,ResponsibleAnaesthetistProfessionalCode
	,ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,WaitingTimeMeasurementTypeCode
	,ActivityLocationTypeCode
	,EarliestClinicallyAppropriateDate
	,WithheldIdentityReasonCode
	,AppointmentTime
	,ExpectedDurationOfAppointment
	,ClinicCode
	,CDSOverseasVisitorStatusClassificationCode

)
select
	 UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,NameFormatCode
	,PatientsAddress
	,Postcode
	,HAofResidenceCode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,DateOfBirthStatus
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,ReferralCode
	,ReferringOrganisationCode
	,ServiceTypeRequested
	,ReferralRequestReceivedDate
	,ReferralRequestReceivedDateStatus
	,PriorityType
	,SourceOfReferralCode
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,LocalSubSpecialtyCode
	,ClinicPurpose
	,ConsultantCode
	,AttendanceIdentifier
	,AdminCategoryCode
	,SiteCode
	,MedicalStaffTypeCode
	,AttendanceDate
	,FirstAttendanceFlag
	,DNAFlag
	,AttendanceOutcomeCode
	,LastDNAorPatientCancelledDate
	,LastDNAorPatientCancelledDateStatus
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,ReadPrimaryDiagnosisCode
	,ReadSecondaryDiagnosisCode1
	,OperationStatus
	,PrimaryOperationCode
	,OperationCode2
	,OperationCode3
	,OperationCode4
	,OperationCode5
	,OperationCode6
	,OperationCode7
	,OperationCode8
	,OperationCode9
	,OperationCode10
	,OperationCode11
	,OperationCode12
	,ReadPrimaryOperationCode
	,ReadOperationCode2
	,ReadOperationCode3
	,ReadOperationCode4
	,ReadOperationCode5
	,ReadOperationCode6
	,ReadOperationCode7
	,ReadOperationCode8
	,ReadOperationCode9
	,ReadOperationCode10
	,ReadOperationCode11
	,ReadOperationCode12
	,PatientSexAgeMix
	,IntendedClinicalCareIntensity
	,BroadPatientGroup
	,CasenoteNumber
	,EthnicGroupCode
	,AttendanceCategoryCode
	,AppointmentRequiredDate
	,MaritalStatusCode
	,GPDiagnosisCode
	,ReadGPDiagnosisCode
	,PrimaryOperationGroupCode
	,SecondaryOperationGroupCode1
	,SecondaryOperationGroupCode2
	,SecondaryProcedureGroupCode3
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,ReadSecondaryDiagnosisCode2
	,ReadSecondaryDiagnosisCode3
	,ReadSecondaryDiagnosisCode4
	,FundingType
	,GPFundholderCode
	,AppointmentHospitalCode
	,ReferralHospitalCode
	,NNNStatusIndicator
	,BlankPad
	,CancellationDate
	,CancelledBy
	,DeathIndicator
	,LocalClinicCode
	,LocalWardCode
	,PCTHealthAuthorityCode
	,PCTofResidenceCode
	,ResponsiblePCTHealthAuthorityCode
	,CommissionerCode
	,AgencyActingOnBehalfOfDoH
	,DerivedFirstAttendance
	,PCTResponsible
	,AgeAtCDSActivityDate
	,UniqueBookingReferenceNumber
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,EarliestReasonableOfferDate
	,EndOfRecord
	,SubmittedDate = @SubmittedDate
	,Created = @Created
	,ByWhom = system_user
	,ResidenceResponsibilityCode
	,MultiDisciplinaryIndicationCode
	,RehabilitationAssessmentTeamTypeCode
	,ConsultationMediumUsedCode
	,DirectAccessReferralIndicatorCode
	,PrimaryDiagnosisPresentOnAdmissionIndicator
	,SecondaryDiagnosis1PresentOnAdmissionIndicator
	,SecondaryDiagnosis2PresentOnAdmissionIndicator
	,SecondaryDiagnosis3PresentOnAdmissionIndicator
	,SecondaryDiagnosis4PresentOnAdmissionIndicator
	,MainOperatingProfessionalCode
	,MainOperatingProfessionalRegistrationIssuerCode
	,ResponsibleAnaesthetistProfessionalCode
	,ResponsibleAnaesthetistProfessionalRegistrationIssuerCode
	,WaitingTimeMeasurementTypeCode
	,ActivityLocationTypeCode
	,EarliestClinicallyAppropriateDate
	,WithheldIdentityReasonCode
	,AppointmentTime
	,ExpectedDurationOfAppointment
	,ClinicCode
	,CDSOverseasVisitorStatusClassificationCode

from
	CDS62.OPAngliaBase Submitted

select @RowsInserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
		', Period: ' + CONVERT(varchar(11), coalesce(@SubmittedDate, ''))


print @Stats


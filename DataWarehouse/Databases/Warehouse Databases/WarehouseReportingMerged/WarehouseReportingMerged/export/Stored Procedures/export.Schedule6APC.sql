﻿
create proc export.Schedule6APC

as
 
set dateformat dmy

declare @start datetime
       ,@end datetime
          ,@start2 datetime
                                
                
set @start = dateadd(dd, datediff(dd, 0, dateadd(dd, - 1, getdate())), 0)
set @start2 = dateadd(dd, datediff(dd, 0, dateadd(dd, - 1, getdate()-7)), 0)
set @end = dateadd(mi, -1, dateadd(dd, 1, @start))
--select @start, @end

select
       [Provider Code] = 
              SiteCode
              ,GlobalProviderSpellNo
              ,GlobalEpisodeNo
       ,[Provider Local ID] = 
              MergeEncounterRecno
       ,[NHS Number] = 
              NHSNumber
       ,[Date Of Birth] = 
              DateOfBirth
       ,[GP Practice Code] = 
              RegisteredGpPracticeCode
       ,[Treatment Function Code] = 
              Specialty.NationalSpecialtyCode
       ,[Admission Method] = 
              AdmissionMethod.NationalAdmissionMethodCode
       ,[Admission Date] = 
              AdmissionDate
       ,[Admission Time] = 
              AdmissionTime
       ,[Discharge Date] = 
              DischargeDate
       ,[Discharge Time] = 
              DischargeTime
       ,[Discharge Destination] = 
              DischargeDestination.NationalDischargeDestinationCode
       ,[Discharge Method] = 
              DischargeMethod.NationalDischargeMethodCode
       ,[Commissioner Code] = 
              CCGCode
       ,NationalLastEpisodeInSpellIndicator

from
       apc.Encounter

left outer join apc.AdmissionMethod
on AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID

left outer join apc.DischargeDestination
on DischargeDestination.SourceDischargeDestinationID = Encounter.DischargeDestinationID

left outer join apc.DischargeMethod
on DischargeMethod.SourceDischargeMethodID = Encounter.DischargeMethodID

left outer join wh.Specialty
on Specialty.SourceSpecialtyID = encounter.SpecialtyID

where
       (
              (
              AdmissionDate >=@start2
              and admissiondate < @end
              and EpisodeStartTime = AdmissionTime
              )
       or
              (
              DischargeDate >= @start2
              and DischargeDate < @end
              and EpisodeEndTime = DischargeTime
              )
       )
and CCGCode in ('00W','01N','01M') --North, Central and south CCGs only
and LocalAdminCategoryCode <> 'PAY'
and AdmissionMethod.NationalAdmissionMethodCode in ('21','22','23','24','25','26','27','28')

﻿

CREATE view [Incident].[EntityType] as
SELECT  [SourceContextCode]
      ,[SourceContext]
      ,[SourceIncidentEntityTypeID]
      ,[SourceIncidentEntityTypeCode]
      ,[SourceIncidentEntityType]

  FROM [WarehouseOLAPMergedV2].[Incident].[EntityType]

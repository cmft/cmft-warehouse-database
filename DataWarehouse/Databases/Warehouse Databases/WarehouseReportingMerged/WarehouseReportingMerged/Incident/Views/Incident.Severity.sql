﻿

CREATE view [Incident].[Severity] as
SELECT  [SourceContextCode]
      ,[SourceContext]
      ,[SourceSeverityID]
      ,[SourceSeverityCode]
      ,[SourceSeverity]
  FROM [WarehouseOLAPMergedV2].[Incident].[Severity]

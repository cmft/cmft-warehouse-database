﻿

CREATE view [Incident].[Cause] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceCauseID]
      ,[SourceCauseCode]
      ,[SourceCause]
  FROM [WarehouseOLAPMergedV2].[Incident].[Cause]

﻿

CREATE view [Incident].[Grade] as
SELECT  [SourceContextCode]
      ,[SourceContext]
      ,[SourceGradeID]
      ,[SourceGradeCode]
      ,[SourceGrade]
  FROM [WarehouseOLAPMergedV2].[Incident].[Grade]

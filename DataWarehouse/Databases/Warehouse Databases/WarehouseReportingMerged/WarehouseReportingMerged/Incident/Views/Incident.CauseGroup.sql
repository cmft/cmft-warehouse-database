﻿

CREATE view [Incident].[CauseGroup] as
SELECT  [SourceContextCode]
      ,[SourceContext]
      ,[SourceCauseGroupID]
      ,[SourceCauseGroupCode]
      ,[SourceCauseGroup]

  FROM [WarehouseOLAPMergedV2].[Incident].[CauseGroup]

﻿
CREATE view [Incident].[IncidentType] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceIncidentTypeID]
      ,[SourceIncidentTypeCode]
      ,[SourceIncidentType]
  FROM [WarehouseOLAPMergedV2].[Incident].[IncidentType]

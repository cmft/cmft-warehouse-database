﻿






CREATE VIEW [RPT].[DementiaLiveUpdate]

AS

 /******************************************************************************
**  Name:		RPT.DementiaLiveUpdate
**  Purpose:	Transformation View for populating table RPT.DementiaTable
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** ----------	-------		---------------------------------------------------- 
** 2012-12-28   CB			Refactored
** 2013-02      GC          Refactored
** 2013-03-08   DF          Changed Bedman section to use TrustCache tables
** 2013-06-28	GC			Chnaged Traff AMTScore based on find question answered as no
******************************************************************************/


WITH XMLNAMESPACES (N'http://schemas.microsoft.com/office/infopath/2003/myXSD/2009-11-10T09:47:10' as my)

--trafford
select
	 DementiaID =
		InfopathForm.FormId

	,ProviderSpellNo = 
		APCEncounter.ProviderSpellNo

	,SourcePatientNo =
		APCEncounter.SourcePatientNo

	,PASCode =
		WarehouseReportingMerged.APC.f_GetWardCode(
			 ProviderSpellNo
			,formXML.value('/my:myFields[1]/my:CreatedDate[1]', 'varchar(200)')
		)

	,LastUpdated_TS =
		[OriginalSubmissionDate]
--		formXML.value('/my:myFields[1]/my:CreatedDate[1]', 'varchar(200)')

	,KnownToHaveDementia =
		replace(
			replace(
				 formXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)')
				,'Yes'
				,1
			)
			,'No'
			,0
		)

	,PatientDementiaFlag =
		CASE
		WHEN formXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') = 'Yes' THEN 1
		ELSE
			CASE
			WHEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
				THEN
					CASE
					WHEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 7	THEN 1
					ELSE 0 
					END
			ELSE 0 
			END 
		END

	,RememberMePlan =
		CASE 
		WHEN
			formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed1[1]', 'varchar(200)')
			+
			formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed2[1]', 'varchar(200)')
			+
			formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed3[1]', 'varchar(200)')
			= 'truetruetrue'
			THEN 1 
		ELSE 0 
		END

	,RememberMePlanCommenced =
		formXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate1[1]', 'varchar(200)')

	,RememberMePlanDiscontinued = Null

	,AMTTestPerformed =
		case
		when  formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') = 'No' then 0
		else replace(
			replace(
				 formXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)')
				,'false'
				,0
			)
			,'true'
			,1
		)
		end

	,ClinicalIndicationsPresent = 

			case 
				when formXML.value('/my:myFields[1]/my:ClinicalIndications[1]/my:ClinInd[1]', 'varchar(200)')  = 'true' then 1
				when formXML.value('/my:myFields[1]/my:ClinicalIndications[1]/my:ClinInd[1]', 'varchar(200)')  = 'False' then 0
				else null
			end
	,ForgetfulnessQuestionAnswered =

		case 
		when formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='Yes' then 1
		when formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='No' then 0
		else null
		end

			--Gareth Cunnah Replaced 02/01/2012 below did not apear to give correct results

		--replace(
		--	replace(
		--		 formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)')
		--		,'Yes'
		--		,1
		--	)
		--	,'No'
		--	,Null
		--)

	,AMTScore =
		CASE 
		WHEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
		and formXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') <> 'No'
			THEN formXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)')
		ELSE Null 
		END 
		
	,ReferralNeeded =
		CASE 
		WHEN formXML.value('/my:myFields[1]/my:GPLetterConfirmSection[1]/my:GPLetterConfirmed[1]', 'varchar(200)') = 'true'
			THEN 1
		WHEN formXML.value('/my:myFields[1]/my:GPLetterConfirmSection2[1]/my:GPLetterConfirmed2[1]', 'varchar(200)') = 'true'
			THEN 1			
		ELSE Null 
		END 

	,DementiaReferralSentDate = null

	,AMTTestNotPerformedReason = formXML.value('/my:myFields[1]/my:UnableSection[1]/my:UnableReason[1]', 'varchar(200)')

	,CognitiveInvestigationStarted = 

			case
				when formXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)')  = 'true' then 'Y'
				when formXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)')  = 'False' then 'N'
				else null
			end


from
	WarehouseReportingMerged.APC.Encounter APCEncounter

inner join TrustCache.Dementia.Trafford_InfoPath_Forms InfoPathForm
on	InfoPathForm.AdmissionNumber = APCEncounter.ProviderSpellNo
and APCEncounter.FirstEpisodeInSpellIndicator = 1



UNION

--central

SELECT 
	 DementiaID =
	coalesce(BedmanEvent.ID,Spell.SpellID)

	,APCEncounter.ProviderSpellNo
		
	,APCEncounter.SourcePatientNo 
	,PASCode = Spell.CurrWard
	,BedmanEvent.LastUpdated_TS 
	,BedmanEvent.KnownToHaveDementia 
	,BedmanEvent.PatientDementiaFlag 
	,BedmanEvent.RememberMePlan 
	,BedmanEvent.RememberMePlanCommenced 
	,BedmanEvent.RememberMePlanDiscontinued 
	,BedmanEvent.AMTTestPerformed 
	,BedmanEvent.ClinicalIndicationsPresent 
	,BedmanEvent.ForgetfulnessQuestionAnswered 
	,BedmanEvent.AMTScore 
	,BedmanEvent.ReferralNeeded 
	,BedmanEvent.DementiaReferralSentDate
	,BedmanEvent.AMTTestNotPerformedReason 
	,BedmanEvent.CognitiveInvestigationStarted 
	
FROM

TrustCache.Dementia.Bedman_Spell_min Spell


 left join TrustCache.Dementia.Bedman_Event_min BedmanEvent
on BedmanEvent.HospSpellID = Spell.SpellID


INNER JOIN WarehouseReportingMerged.APC.Encounter APCEncounter
ON	Spell.Patient = APCEncounter.SourcePatientNo
AND Spell.IPEpisode = APCEncounter.SourceSpellNo
AND APCEncounter.FirstEpisodeInSpellIndicator = 1

where 
(
BedmanEvent.HospSpellID is not null

or 

BedmanEvent.HospSpellID is null
)
and
APCEncounter.AdmissionTime > getdate () - 365
and 
APCEncounter.AgeCode > '74'


-- old way
--SELECT 
--	 DementiaID =
--		BedmanEvent.ID

--	,APCEncounter.ProviderSpellNo
		
--	,APCEncounter.SourcePatientNo 
--	,PASCode = Spell.CurrWard
--	,BedmanEvent.LastUpdated_TS 
--	,BedmanEvent.KnownToHaveDementia 
--	,BedmanEvent.PatientDementiaFlag 
--	,BedmanEvent.RememberMePlan 
--	,BedmanEvent.RememberMePlanCommenced 
--	,BedmanEvent.RememberMePlanDiscontinued 
--	,BedmanEvent.AMTTestPerformed 
--	,BedmanEvent.ClinicalIndicationsPresent 
--	,BedmanEvent.ForgetfulnessQuestionAnswered 
--	,BedmanEvent.AMTScore 
--	,BedmanEvent.ReferralNeeded 
--	,BedmanEvent.AMTTestNotPerformedReason 
--	,BedmanEvent.CognitiveInvestigationStarted 
	
--FROM
--	TrustCache.Dementia.Bedman_Event_min BedmanEvent

--INNER JOIN TrustCache.Dementia.Bedman_Locations_min Location
--ON	BedmanEvent.LocationID = Location.LocationID

--INNER JOIN TrustCache.Dementia.Bedman_Spell_min Spell
--ON	BedmanEvent.HospSpellID = Spell.SpellID

--INNER JOIN WarehouseReportingMergedV2.APC.Encounter APCEncounter
--ON	Spell.Patient = APCEncounter.SourcePatientNo
--AND Spell.IPEpisode = APCEncounter.SourceSpellNo
--AND APCEncounter.FirstEpisodeInSpellIndicator = 1








































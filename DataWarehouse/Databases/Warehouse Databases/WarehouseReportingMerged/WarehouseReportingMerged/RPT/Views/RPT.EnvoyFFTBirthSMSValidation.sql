﻿





CREATE view [RPT].[EnvoyFFTBirthSMSValidation]

as




SELECT [DistrictNo]
      ,[MergeEncounterRecno]
      ,[HospitalDischargeDate]
      ,[HospitalDischargeTime]
      ,[HealthVisitorDischargeDate]
      ,[Location]
      ,[MobileNumber]
      ,[LandlineNumber]
      ,[Title]
      ,[FirstName]
      ,[LastName]
      ,[Address1]
      ,[Address2]
      ,[Address3]
      ,[Address4]
      ,[Postcode]
      ,[ContextCode]
      ,[Created]
      ,[Updated]
      ,[ByWhom]
      ,[Submitted]
  FROM [WarehouseReportingMerged].[RPT].[EnvoyFFTBirthPatient]
  
  





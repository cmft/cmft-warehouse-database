﻿




CREATE view [RPT].[EnvoyFFTBirthSMS]

as

/* 
==============================================================================================
Description:	

When		Who			What
?			?			Initial Coding
17/03/2015	Paul Egan	Formatted [Discharge_Date] as requested by Healthcare Comms
===============================================================================================
*/


select 
	[Patient_Ref] = DistrictNo
	,[Discharge_Ref] = MergeEncounterRecno
	,[Discharge_Date] = CONVERT(VARCHAR(10), HealthVisitorDischargeDate, 103) 
	,[Discharge_Time] = '00:00'
	,[Location]
	,[Lead_Specialty_1] = '501'
	,[Lead_Specialty_2]  = ''
	,[Mobile_Number] = MobileNumber 
	,[Landline_Number]  = ''
	,[Email_address] = ''
	,[Title] = ''
	,[First_Name] = 'FirstName'
	,[Last_Name] = 'Surname'
	,[Address_1] = ''
	,[Address_2] = ''
	,[Address_3] = ''
	,[Address_4] = ''
	,[Address_5] = ''
	,[Postcode] = ''
	,[Country] = ''
from
	 [WarehouseReportingMerged].[RPT].[EnvoyFFTBirthPatient]
  
  
where
	[Submitted] is null	





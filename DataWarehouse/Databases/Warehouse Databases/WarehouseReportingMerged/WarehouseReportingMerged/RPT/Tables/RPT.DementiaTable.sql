﻿CREATE TABLE [RPT].[DementiaTable] (
    [DementiaID]                    INT          NOT NULL,
    [SourceUniqueID]                VARCHAR (50) NULL,
    [ProviderSpellNo]               VARCHAR (50) NULL,
    [SourcePatientNo]               VARCHAR (20) NULL,
    [SourceWardCode]                VARCHAR (20) NULL,
    [LastUpdated_TS]                DATETIME     NULL,
    [KnownToHaveDementia]           INT          NULL,
    [PatientDementiaFlag]           INT          NULL,
    [RememberMePlan]                INT          NULL,
    [RememberMePlanCommenced]       DATETIME     NULL,
    [RememberMePlanDiscontinued]    DATETIME     NULL,
    [AMTTestPerformed]              INT          NULL,
    [ClinicalIndicationsPresent]    INT          NULL,
    [ForgetfulnessQuestionAnswered] INT          NULL,
    [AMTScore]                      INT          NULL,
    [PreviousDementiaAssessment]    INT          NULL,
    [ReferralNeeded]                INT          NULL,
    [PsychoticMedsReview]           INT          NULL,
    [PsychoticReasonPresent]        INT          NULL,
    [AMTTestNotPerformedReason]     VARCHAR (60) NULL,
    [CognitiveInvestigationStarted] VARCHAR (2)  NULL,
    [RunDate]                       DATETIME     NULL,
    [DementiaReferralSentDate]      DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [ProviderSpellNo]
    ON [RPT].[DementiaTable]([ProviderSpellNo] ASC);


GO
CREATE NONCLUSTERED INDEX [DementiaTable_Lastupdated]
    ON [RPT].[DementiaTable]([ForgetfulnessQuestionAnswered] ASC)
    INCLUDE([ProviderSpellNo], [LastUpdated_TS], [ClinicalIndicationsPresent]);


﻿CREATE TABLE [RPT].[EnvoyFFTBirthPatient] (
    [DistrictNo]                 VARCHAR (50)  NULL,
    [MergeEncounterRecno]        INT           NOT NULL,
    [HospitalDischargeDate]      DATE          NULL,
    [HospitalDischargeTime]      VARCHAR (5)   NULL,
    [HealthVisitorDischargeDate] DATE          NULL,
    [Location]                   VARCHAR (20)  NULL,
    [MobileNumber]               VARCHAR (30)  NULL,
    [LandlineNumber]             VARCHAR (30)  NULL,
    [Title]                      NVARCHAR (5)  NULL,
    [FirstName]                  NVARCHAR (30) NULL,
    [LastName]                   NVARCHAR (30) NULL,
    [Address1]                   NVARCHAR (30) NULL,
    [Address2]                   NVARCHAR (30) NULL,
    [Address3]                   NVARCHAR (30) NULL,
    [Address4]                   NVARCHAR (30) NULL,
    [Postcode]                   NVARCHAR (10) NULL,
    [ContextCode]                NVARCHAR (20) NULL,
    [Created]                    DATETIME      NULL,
    [Updated]                    DATETIME      NULL,
    [ByWhom]                     NVARCHAR (20) NULL,
    [Submitted]                  DATETIME      NULL
);


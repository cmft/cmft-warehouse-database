﻿
CREATE Procedure [RPT].[CreatinineAlertNational] 

(
	 @ReportType varchar(10) 
	,@NationalAlert varchar(100) 
	,@Summary bit
	,@Stage varchar (100) 
	,@FromDate date
	,@ToDate date
	,@CurrentInpatient tinyint
)	

as
		
select
	GlobalProviderSpellNo
	,PatientSurname
	,PatientForename
	,DistrictNo
	,DiedInHospital
	,DateOfDeath
	,C1Result
	,ResultTime
	,RVRatio
	,NationalAlert
	,RatioDescription
	,AgeOnAdmission
	,Sex 
	,LocalRatio = CurrentToBaselineResultRatio
	,LocalStage = Stage
	,AdmissionTime
	,PatientCategoryCode
	,CurrentInpatient
	,CurrentWard 
	,WardAtResult = WardEpisodeStart
	,LocalExclusion
from
	(
	select 
		GlobalProviderSpellNo
		,PatientSurname
		,PatientForename
		,DistrictNo
		,DiedInHospital = 
				case
					when exists
						(
						Select
							1
						from
							APC.DischargeMethod
						where
							DischargeMethod.SourceDischargeMethodCode = AcuteKidneyInjuryAlert.DischargeMethodCode
						and DischargeMethod.NationalDischargeMethod in ('Patient DiEd','Stillbirth')
						)
					then 1
					else 0
				end
		,DateOfDeath
		,C1Result
		,ResultTime
		,RVRatio
		,NationalAlert
		,RatioDescription
		,AgeOnAdmission
		,Sex = SexCode
		,CurrentToBaselineResultRatio
		,Stage
		,AdmissionTime
		,PatientCategoryCode
		,WardEpisodeStart
		
		,ReportSequenceNo =
				case
				when @ReportType = 'CHANGE' then  row_number () over (partition by DistrictNo,GlobalProviderSpellNo order by RVRatio desc)
				when @ReportType = 'LATEST' then  row_number () over (partition by DistrictNo order by ResultTime desc)
				when @ReportType = 'VALUE' then  row_number () over (partition by DistrictNo,GlobalProviderSpellNo order by C1Result desc)
				end
		,CurrentInpatient
		,CurrentWard
		,LocalExclusion = 
			Case
				when RenalPatient = 1 then 'Renal'
				when AgeAtTest <18 then 'Under 18'
				else null
			end
			
	from 
		Result.AcuteKidneyInjuryAlert
	where
		LinkageType in (
					'AEAPC'
					,'APC'
					,'AE'
					)
	--and AgeOnAdmission >=18
	--and RenalPatient = 0
	) 
	Result
where 
	ResultTime between @FromDate and @ToDate
and Result.NationalAlert in (Select Value from RPT.SSRS_MultiValueParamSplit(@NationalAlert,','))
and Result.Stage in (Select Value from RPT.SSRS_MultiValueParamSplit(@Stage,','))
and (
		(@ReportType = 'Change' 
		and	Result.ReportSequenceNo = @Summary)
		or
		(@ReportType = 'Latest' 
		and	Result.ReportSequenceNo = @Summary)
		or
		(@ReportType = 'Value' 
		and	Result.ReportSequenceNo = @Summary)
	)
		
		

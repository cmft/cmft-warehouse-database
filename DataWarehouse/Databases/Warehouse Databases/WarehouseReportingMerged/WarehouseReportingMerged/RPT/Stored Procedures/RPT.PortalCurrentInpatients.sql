﻿

CREATE proc [RPT].[PortalCurrentInpatients]

(
@User varchar(50)
)

as

select 
	Consultant = Consultant.SourceConsultant + ' - ' + Consultant.SourceConsultantCode
	,WindowsUserCodeMap.Userid
	,CasenoteNumber
	,DistrictNo
	,Patient = PatientSurname + ', ' + PatientForename
	,AdmissionDate
	,ExpectedDateofDischarge
	,ExpectedLOS
	,CurrentLOS = datediff(day, AdmissionDate, getdate())
	,Cases = 1
from
	APC.Encounter 

inner join	WH.Consultant
on	Consultant.SourceConsultantID = Encounter.ConsultantID	

inner join	GeckoASSecurity.dbo.WindowsUserCodeMap
on	WindowsUserCodeMap.SourceValueCode = Consultant.SourceConsultantCode 
and	SourceContextCode = WindowsUserCodeMap.ContextCode 

inner join GeckoASSecurity.dbo.WindowsUserSecurityGroupMap
on	WindowsUserSecurityGroupMap.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode

inner join GeckoASSecurity.dbo.[SecurityGroupEntityTypeMap]
on	[SecurityGroupEntityTypeMap].SecurityGroupCode = WindowsUserSecurityGroupMap.SecurityGroupCode
and [EntityTypeCode] = 'CONSULTANT'

where
	Encounter.EpisodeEndDate is null
and
	WindowsUserSecurityGroupMap.Userid = @User


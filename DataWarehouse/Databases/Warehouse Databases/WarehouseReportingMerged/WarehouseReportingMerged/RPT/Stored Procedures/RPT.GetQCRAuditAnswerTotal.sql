﻿
CREATE proc [RPT].[GetQCRAuditAnswerTotal]

	(
	 @LocationType int
	,@Division int
	,@Location int
	,@Period date
	)
as

select 
	 AuditAnswer.AuditAnswerRecno
	,AuditAnswer.AuditTime
	,AuditAnswer.Answer
	,TheMonth = FirstDayOfMonth
	,SourceAuditTypeID
	,SourceAuditType
	,SourceQuestion
	,Division = SourceDivision
	,SourceLocation
	,SourceLocationType
from
	(
	select 
		 AuditAnswer.AuditAnswerRecno
		,AuditAnswer.AuditTime
		,AuditAnswer.Answer
		,FirstDayOfMonth
		,AuditType.SourceAuditTypeID
		,AuditType.SourceAuditType
		,Question.SourceQuestion
		,Division.SourceDivisionCode
		,Division.SourceDivision
		,Location.SourceLocationID
		,Location.SourceLocation
		,LocationType.SourceLocationTypeCode
		,LocationType.SourceLocationType
	from
		QCR.AuditAnswer

	inner join QCR.Location
	on	AuditAnswer.LocationID = Location.SourceLocationID

	inner join QCR.LocationType
	on	Location.SourceLocationTypeCode = LocationType.SourceLocationTypeCode

	inner join QCR.Division
	on	Location.SourceDivisionCode = Division.SourceDivisionCode

	inner join QCR.Question
	on	AuditAnswer.QuestionID = Question.SourceQuestionID

	inner join QCR.AuditType
	on	AuditAnswer.AuditTypeID = AuditType.SourceAuditTypeID

	inner join WH.Calendar
	on	AuditAnswer.AuditDateID = Calendar.DateID

	union all

	select 
		 AuditAnswer.AuditAnswerRecno
		,AuditAnswer.AuditTime
		,AuditAnswer.Answer
		,FirstDayOfMonth
		,SourceAuditTypeID = 0
		,SourceAuditType = 'Total'
		,Question.SourceQuestion
		,Division.SourceDivisionCode
		,Division.SourceDivision
		,Location.SourceLocationID
		,Location.SourceLocation
		,LocationType.SourceLocationTypeCode
		,LocationType.SourceLocationType
	from
		QCR.AuditAnswer

	inner join QCR.Location
	on	AuditAnswer.LocationID = Location.SourceLocationID

	inner join QCR.LocationType
	on	Location.SourceLocationTypeCode = LocationType.SourceLocationTypeCode

	inner join QCR.Division
	on	Location.SourceDivisionCode = Division.SourceDivisionCode

	inner join WH.Question
	on	AuditAnswer.QuestionID = Question.SourceQuestionID

	inner join WH.Calendar
	on	AuditAnswer.AuditDateID = Calendar.DateID

	) AuditAnswer

where
		AuditAnswer.FirstDayOfMonth = @Period
and
		(
			@LocationType in (AuditAnswer.SourceLocationTypeCode)
		or
			@LocationType = 0
		)
and
		(
			@Division in (AuditAnswer.SourceDivisionCode)
		or
			@Division = 0
		)
and
		(
			@Location in (AuditAnswer.SourceLocationID)
		or
			@Location = 0
		)

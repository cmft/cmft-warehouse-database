﻿
create procedure RPT.ChooseAndBookReferralsSpecialtyParam
(
	 @Division VARCHAR(MAX)
	,@ActivityDate DATE
)
as
	SELECT DISTINCT
		 SPEC.NationalSpecialtyCode
		,SPEC.NationalSpecialty

	FROM 
		OP.Encounter E
		INNER JOIN WH.Directorate DIR
			ON		E.DirectorateCode = DIR.DirectorateCode
		INNER JOIN WH.Specialty SPEC
			ON		E.ReferralSpecialtyCode = SPEC.SourceSpecialtyCode
				AND E.ContextCode			= SPEC.SourceContextCode

	WHERE
		DIR.Division IN (SELECT Item FROM dbo.fn_ListToTable(@Division,','))
		AND	(
					E.Referraldate			= @ActivityDate
				OR	E.AppointmentCancelDate	= @ActivityDate
				OR	E.DischargeDate			= @ActivityDate
			 )
		AND		E.AppointmentTypeCode LIKE 'Z%'
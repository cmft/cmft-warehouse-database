﻿



CREATE PROCEDURE [RPT].[GetHarmCare]
(@StartDate as datetime,
@EndDate as datetime,
@Hours as int)
AS

if @StartDate < '01 July 2012'
BEGIN
	SET @StartDate = '01 July 2012'
END


--DECLARE @StartDate AS DATE
--DECLARE @EndDate as DATE
--DECLARE @Hours AS INT
--SET @StartDate = '17 Sep 2012'
--SET @EndDate = '23 Sep 2012'
--SET @Hours = 24

DECLARE @NoAdmissions AS INT

SELECT distinct 
		TheDate =			CrossCAL.thedate,
		WeekNo =			Crosscal.weekno,
		TheMonth =			CrossCal.TheMonth,
		FinancialQuarter  = Crosscal.FinancialQuarter,
		
		SpellID =			SpellID, 
		Patient = 			Patient, 
		IPEpisode =			IPEpisode, 
		AdmitDate =			AdmitDate, 
		FallTS = 			FallTS, 
		Division =			DIR.Division, 
		SourceWardCode =	dbo.f_GetWardCode(E.ProviderSpellNo,COALESCE(F.FallTS, PUHFC1.IdentifiedTS,PUHFC2.IdentifiedTS, Test.UrinaryTestResultsSentTS, VTECondition.DiagnosisTS)),
		TimeToFall =		DATEDIFF(HH, FallTS, CrossCAL.TheDate),  
		InitialSeverity =	InitialSeverity,
		PUHFC1_Category =	PUHFC1.Category ,
		PUHFC2_Category =	PUHFC2.Category ,
		TimeToUTI =			

				(
		
							SELECT TOP 1 1 FROM SmallDatasets.CQUIN.CatheterIntervention Cat
							 INNER JOIN SmallDatasets.bedman.EventArchive EA2
								ON EA2.Bid = cat.Bid
							 WHERE EA2.HospSpellID = EA.HospSpellID 
							 and								
								(
							  
							 RemovedTS is null
							 and Test.TestResultsPositive = 'true'
							 AND UrinaryTestResultsSentTS >= cat.InsertedTS 
							 or
							 RemovedTS is not null
							 and Test.TestResultsPositive = 'true'
							 and datediff(hour,	RemovedTS, test.UrinaryTestResultsSentTS)between 0 and 48

								)
							 
							 --AND cat.InsertedTS <= UrinaryTestResultsSentTS
							 AND cat.CatheterType = 2),
		
			--(SELECT TOP 1 1 FROM SmallDatasets.CQUIN.CatheterIntervention Cat
			--				 INNER JOIN SmallDatasets.bedman.EventArchive EA2
			--					ON EA2.Bid = cat.Bid
			--				 WHERE EA2.HospSpellID = EA.HospSpellID 
			--				 AND cat.InsertedTS <= UrinaryTestResultsSentTS
			--				 AND cat.CatheterType = 2),


		VTETreatment =		VTETypeID,
		PatientCategory =	E.PatientCategoryCode
		,F.Bid Fall
		,PUHFC1.Bid PUHFC1
		,PUHFC2.Bid PUHFC2
		,test.Bid test
		,VTECondition.Bid VTECondition

  FROM SmallDatasets.bedman.EventArchive EA
  CROSS JOIN (SELECT DISTINCT Thedate, FinancialQuarter, TheMonth, WeekNo, DateID
	FROM WH.Calendar
	WHERE TheDate BETWEEN @StartDate AND @EndDate) CrossCAL  
  LEFT JOIN BedMan.dbo.HospSpell HS
	ON EA.HospSpellID = HS.SpellID
  LEFT JOIN SmallDatasets.CQUIN.FallIncident F
	ON F.Bid = EA.Bid AND DATEDIFF(HH, FallTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours AND InitialSeverity >1 -- Fall with harm
  LEFT JOIN SmallDatasets.CQUIN.PressureUlcerCondition PUHFC1
	ON PUHFC1.Bid = EA.Bid AND PUHFC1.Category > 1 AND CAST(PUHFC1.IdentifiedTS AS DATE) = CrossCAL.TheDate

  LEFT JOIN SmallDatasets.CQUIN.PressureUlcerCondition PUHFC2
	ON PUHFC2.Bid = EA.Bid AND PUHFC2.Category > 1 AND CAST(PUHFC2.IdentifiedTS AS DATE) = CrossCAL.TheDate	
	--AND DATEDIFF(HH, AdmitDate, PUHFC2.IdentifiedTS) > 72

  LEFT JOIN SmallDatasets.CQUIN.UTITest Test
	ON Test.Bid = EA.Bid AND DATEDIFF(HH, Test.UrinaryTestResultsSentTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours
  LEFT JOIN SmallDatasets.CQUIN.VTECondition
	ON VTECondition.Bid = EA.Bid AND DATEDIFF(HH, DiagnosisTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours	
  LEFT JOIN APC.Encounter E
	ON HS.Patient = E.SourcePatientNo AND HS.AdmitDate = E.AdmissionTime AND E.FirstEpisodeInSpellIndicator = 1
  LEFT JOIN WH.Directorate DIR 
    ON DIR.DirectorateCode = E.StartDirectorateCode
  --LEFT JOIN WarehouseReportingMerged.WH.Ward WARD
  --  ON WARD.SourceWardID = E.StartWardID
   WHERE (F.Bid IS NOT NULL OR PUHFC1.Bid IS NOT NULL OR PUHFC2.Bid IS NOT NULL OR Test.Bid IS NOT NULL OR VTECondition.Bid IS NOT NULL --or cat.Bid is not null
   )



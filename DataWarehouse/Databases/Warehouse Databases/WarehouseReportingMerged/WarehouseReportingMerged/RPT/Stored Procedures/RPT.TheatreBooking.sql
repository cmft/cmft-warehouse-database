﻿
CREATE procedure [RPT].[TheatreBooking]
  @Unit        varchar(25)
 ,@FromDate    Date
 ,@ToDate      Date
 ,@SessionCode Int     = NULL

as

/* 
==============================================================================================
Description:	Ported from  [WarehouseReporting].[RPT].[GetORMISTheatreBookingReport]
				as WarehouseOLAP is no longer being updated.

When		Who			What
24/03/2015	Paul Egan	Initial Coding
===============================================================================================
*/


/*============== DEBUG ONLY ===============*/
--declare @Unit     as varchar(25)
--declare @FromDate as date 
--declare @ToDate   as date
--declare @SessionCode int
--set @FromDate = '20140101'
--set @ToDate   = '20140131'
--set @Unit     = null	--'Eye Hospital Theatres'
--;
/*==========================================*/

with rawDataCTE as
(
select
	Booking.SourceUniqueID
	,UN_CODE = Theatre.OperatingSuiteCode
	,UN_DESC = Theatre.OperatingSuite
	,TH_CODE = Theatre.SourceAlternativeTheatreCode
	,TH_DESC = Theatre.SourceTheatre
	,SS_SESSION =
		case
		when
			left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29'
		and	left(convert(varchar, Session.EndTime, 8), 5) > '16:29' then 'AD' --All Day

		when left(convert(varchar, Session.StartTime, 8), 5) between '00:00' and '12:29' then 'AM' --Morning
		when left(convert(varchar, Session.StartTime, 8), 5) between '12:30' and '16:29' then 'PM' --Afternoon
		when left(convert(varchar, Session.StartTime, 8), 5) between '16:30' and '23:59' then 'VN' --Evening
		else 'NA'
		end
	,PA_TH_SESS_SORT = Booking.TheatreSessionSort
	,OperationDate = convert(varchar(10), session.StartTime, 103)
	,EstStartTime = convert(varchar(17), Booking.EstimatedStartTime, 108)
	,Casenote = Booking.DistrictNo
	,PatName = Booking.Surname + ' -' + Booking.Forename
	,DateOfBirth = convert(varchar(10), Booking.DateOfBirth, 103)
	,OperationComment = Booking.Operation
	,Surgeon = 
		case
		when charindex('(', Staff.SourceStaff) > 0 then rtrim(left(Staff.SourceStaff, charindex('(', Staff.SourceStaff) - 1))
		else Staff.SourceStaff
		end
	,S1_DESC = Specialty.NationalSpecialty
	,TheatreCode = Theatre.SourceTheatreCode
	,StaffCode = Staff.SourceStaffCode
	,SessSequ = Booking.SessionSourceUniqueID
from
	Theatre.PatientBooking Booking
	
	inner join Theatre.[Session]
	on [Session].SourceUniqueID = Booking.SessionSourceUniqueID
	
	inner join Theatre.Theatre
	on Theatre.SourceTheatreID = Booking.TheatreID
	
	inner join Theatre.Staff
	on Staff.SourceStaffID = Booking.ConsultantID
	
	inner join WH.Specialty
	on Specialty.SourceSpecialtyID = [Session].SpecialtyID
where
	Booking.IntendedProcedureDate between @FromDate and @ToDate
	and (
		Theatre.OperatingSuite = @Unit
		or @Unit is null
		)
	and Booking.PatientBookingStatusCode <> 2	-- no cancelled patients
	and (
		Booking.SessionSourceUniqueID = @SessionCode
		or @SessionCode is null
		)
	and Booking.ContextCode = 'CEN||ORMIS'

)

select
	SourceUniqueID	--***VALIDATION ONLY***
	,UN_CODE
	,UN_DESC
	,TH_CODE
	,TH_DESC
	,SS_SESSION
	,PA_TH_SESS_SORT
	,SESS_SORT = 
		case SS_SESSION
		when 'AM' then 1
		when 'PM' then 2
		when 'VN' then 3
		else 4
		end	
	,[SESSION] = 
		case SS_SESSION
		when 'AM' then 'AM'
		when 'PM' then 'PM'
		when 'VN' then 'EVE'
		else 'OTHER'
		end	
	,AM =
		case SS_SESSION
		when 'AM' then 1
		else 0
		end 
	,PM =
		case SS_SESSION
		when 'PM' then 1
		else 0
		end 
	,EVE =
		case SS_SESSION
		when 'VN' then 1
		else 0
		end 
	,OTHER =
		case 
		when SS_SESSION in ('AM', 'PM', 'VN') then 0
		else 1
		end 
	
	
	,OperationDate
	,EstStartTime
	,Casenote
	,PatName
	,DateOfBirth
	,OperationComment
	,Surgeon
	,S1_DESC
	,TheatreCode
	,StaffCode
	,SessSequ
----------into GetORMISTheatreBookingReport20150325TempPE
from
	rawDataCTE
----------order by
------------1, 2, 4, 5
----------TH_CODE
;
﻿
--This procedure identifies potential spells that have been transferred between sites.  It looks for Spells that have had any of the matching criteria completed.  It will then look for potential matches based on the dishcargetime from the transferrin site and the admissiontime in the transfer site being within 2 days.  It will then determine those spell that have been matched and highlight the reasons why others have not been.

--The following steps are carried out

--1.	Create temp working tables to hold the encounters found.  This includes using the chameleon patient information to find the alternative patient identifier for the systems being transferred to or from.  Found the best approach to be a table for each direction of transfer.
--2.	Populate the working  tables with encounters that could be transfers
--3.	Update the MergencounterRecnoTransferOut with potential matches.
--4.	Output results including case statement to determine if match has been found.  Union data sets together.  

--Created 10/03/2015 Gareth Cunnah




CREATE proc [RPT].[SiteTransfers]

	 @StartDate datetime
	,@EndDate datetime
	 

as



	set nocount on
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED	



create table #TransferEncounter

	(
	 TransferInMergeRecno int  null
	,MatchedDistrictno varchar (20)
	,TransferOutMergeRecno int null
	,AdmissionDate datetime 			
	,AdmissionTime datetime
	,DischargeDate datetime
	,DischargeTime datetime
	,Direction  varchar (20)
	)
	
	
create table #TransferEncounterCentral

	(
	 TransferInMergeRecno int  null
	,MatchedDistrictno varchar (20)
	,TransferOutMergeRecno int null
	,AdmissionDate datetime 
	,AdmissionTime datetime
	,DischargeDate datetime		
	,DischargeTime datetime
	,Direction  varchar (20)
	)	
			
	--	Populate the working  tables with encounters that could be transfers from Central to Trafford		
		
		--Insert encounter that have AdmissionSourceCode = '51-TRINCEN' 
		
insert into #TransferEncounter

select

TransferInMergeRecno = BaseEncounter.MergeEncounterRecno


,MatchedDistrictno =

	(
	select 
	DestinationIdentity.PatientApplicationIdentityValue
	from
	Chameleon.Patient.PatientApplicationIdentity SourceIdentity  with (nolock)

	inner join Chameleon.Patient.PatientApplicationIdentity DestinationIdentity with (nolock)
	on SourceIdentity.PatientId = DestinationIdentity.PatientId
	and DestinationIdentity.SourceApplicationIdentifierId = 1 --DistrictNo 


	where SourceIdentity.SourceApplicationIdentifierId = 2 --EPMINumber
	and SourceIdentity.PatientApplicationIdentityValue = BaseEncounter.SourcePatientNo 

	)
,TransferOutMergeRecno = null
,AdmissionDate = AdmissionDate.TheDate
,BaseEncounter.AdmissionTime
,DischargeDate = DischargeDate.TheDate
,DischargeTime
,Direction = 'TRA||UG'
				 

from

WarehouseOLAPMergedV2.APC.BaseEncounter BaseEncounter


inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferReference
on TransferReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Calendar DischargeDate
on DischargeDate.DateID = TransferReference.DischargeDateID

left join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
on AdmissionDate.DateID = TransferReference.AdmissionDateID	
				
where
	BaseEncounter.ContextCode = 'TRA||UG'
and
	(
	BaseEncounter.AdmissionSourceCode = '51-TRINCEN' 
	or
	BaseEncounter.AdmissionMethodCode = '81-TRCent'
	and
	BaseEncounter.AdmissionSourceCode <> '51-TRINCEN'
	)
and 
BaseEncounter.GlobalEpisodeNo = 1
	--BaseEncounter.FirstEpisodeInSpellIndicator = 1	

and

	(
	AdmissionDate.TheDate between @StartDate and @EndDate	
	or
	DischargeDate.TheDate between @StartDate and @EndDate
	)
	
		
	
update #TransferEncounter

set TransferOutMergeRecno =

	(
	select
		max(TransferOut.MergeEncounterRecno)
	from
		WarehouseOLAPMergedV2.APC.BaseEncounter TransferOut


	where
		TransferOut.ContextCode = 'CEN||PAS'
		and #TransferEncounter.MatchedDistrictno = TransferOut.DistrictNo
		and datediff(minute, TransferOut.DischargeTime, #TransferEncounter.AdmissionTime) between -2880 and 2880 --2880 hours 2 days

		)
		
where #TransferEncounter.TransferInMergeRecno is not null
						

--Insert mssing DischargeDestination Code TF
insert into #TransferEncounter

select
		
 TransferInMergeRecno = null
 
 
,MatchedDistrictno =

	(
	select 
		DestinationIdentity.PatientApplicationIdentityValue
	from
		Chameleon.Patient.PatientApplicationIdentity SourceIdentity  with (nolock)
		
		inner join Chameleon.Patient.PatientApplicationIdentity DestinationIdentity with (nolock)
		on SourceIdentity.PatientId = DestinationIdentity.PatientId
		and DestinationIdentity.SourceApplicationIdentifierId =  2 --EPMINumber
		
		
		where SourceIdentity.SourceApplicationIdentifierId = 1 --DistrictNo
		and SourceIdentity.PatientApplicationIdentityValue = BaseEncounter.DistrictNo 

		)
 ,TransferOutMergeRecno = BaseEncounter.MergeEncounterRecno
 ,AdmissionDate = AdmissionDate.TheDate
 ,BaseEncounter.AdmissionTime
 ,DischargeDate = DischargeDate.TheDate
 ,DischargeTime
 ,Direction = 'TRA||UG'
				 
			

from
WarehouseOLAPMergedV2.APC.BaseEncounter BaseEncounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferReference
on TransferReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Calendar DischargeDate
on DischargeDate.DateID = TransferReference.DischargeDateID

left join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
on AdmissionDate.DateID = TransferReference.AdmissionDateID				

where
BaseEncounter.ContextCode  = 'CEN||PAS'
and
BaseEncounter.DischargeDestinationCode = 'TF' 

and LastEpisodeInSpellIndicator = 'Y'

and DischargeDate.TheDate between @StartDate and @EndDate	

and 
not exists
	(

		Select 
		
		1 
		
		from #TransferEncounter
		
		where
		#TransferEncounter.TransferOutMergeRecno = BaseEncounter.MergeEncounterRecno

	)

						
						
update #TransferEncounter

set TransferinMergeRecno =

	(
		select
		min(TransferIn.MergeEncounterRecno)
		from
		WarehouseOLAPMergedV2.APC.BaseEncounter TransferIn


		where
		TransferIn.ContextCode = 'TRA||UG'
		and #TransferEncounter.MatchedDistrictno = TransferIn.SourcePatientNo
		and datediff(minute, TransferIn.AdmissionTime, #TransferEncounter.DischargeTime) between -2880 and 2880 --2880 hours 2 days

	)
	
where #TransferEncounter.TransferInMergeRecno is null

				
					
--	Populate the working  tables with encounters that could be transfers from Trafford to Central	

insert Into #TransferEncounterCentral

select

TransferInMergeRecno = BaseEncounter.MergeEncounterRecno


,MatchedDistrictno =

	(
	select 
		DestinationIdentity.PatientApplicationIdentityValue
	from
		Chameleon.Patient.PatientApplicationIdentity SourceIdentity  with (nolock)
		
		inner join Chameleon.Patient.PatientApplicationIdentity DestinationIdentity with (nolock)
		on SourceIdentity.PatientId = DestinationIdentity.PatientId
		and DestinationIdentity.SourceApplicationIdentifierId =  2 --EPMINumber
		
		
		where SourceIdentity.SourceApplicationIdentifierId = 1 --DistrictNo
		and SourceIdentity.PatientApplicationIdentityValue = BaseEncounter.DistrictNo 

	)
,TransferOutMergeRecno = null
,AdmissionDate = AdmissionDate.TheDate
,BaseEncounter.AdmissionTime
,DischargeDate = DischargeDate.TheDate
,DischargeTime
,Direction = 'CEN||PAS'



from
WarehouseOLAPMergedV2.APC.BaseEncounter BaseEncounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferReference
on TransferReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Calendar DischargeDate
on DischargeDate.DateID = TransferReference.DischargeDateID

left join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
on AdmissionDate.DateID = TransferReference.AdmissionDateID						


where

BaseEncounter.ContextCode  = 'CEN||PAS'
and
BaseEncounter.AdmissionSourceCode = 'TF' 
and 
GlobalEpisodeNo = 1
--FirstEpisodeInSpellIndicator = '1'
and 
DischargeDate.TheDate between @StartDate and @EndDate	


update #TransferEncounterCentral

set TransferOutMergeRecno =

			(
			select
				max(TransferOut.MergeEncounterRecno)
			from
				WarehouseOLAPMergedV2.APC.BaseEncounter TransferOut

	
			where
				TransferOut.ContextCode = 'TRA||UG'
				and #TransferEncounterCentral.MatchedDistrictno = TransferOut.SourcePatientNo
				and datediff(minute, TransferOut.DischargeTime, #TransferEncounterCentral.AdmissionTime) between -2880 and 2880 --2880 hours 2 days
		
				)
				
--records that are marked on the Transfering systems but not yet matched on the Transfer system							
insert Into #TransferEncounterCentral								
				
select

TransferInMergeRecno = null


,MatchedDistrictno =

(
select 
	DestinationIdentity.PatientApplicationIdentityValue
from
	Chameleon.Patient.PatientApplicationIdentity SourceIdentity  with (nolock)
	
	inner join Chameleon.Patient.PatientApplicationIdentity DestinationIdentity with (nolock)
	on SourceIdentity.PatientId = DestinationIdentity.PatientId
	and DestinationIdentity.SourceApplicationIdentifierId = 1 --DistrictNo 
	
	
	where SourceIdentity.SourceApplicationIdentifierId = 2 --EPMINumber
	and SourceIdentity.PatientApplicationIdentityValue = BaseEncounter.SourcePatientNo 

	)
,TransferOutMergeRecno = BaseEncounter.MergeEncounterRecno
,AdmissionDate = AdmissionDate.TheDate
,BaseEncounter.AdmissionTime
,DischargeDate = DischargeDate.TheDate
,DischargeTime
,Direction = 'CEN||PAS'



from
WarehouseOLAPMergedV2.APC.BaseEncounter BaseEncounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferReference
on TransferReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Calendar DischargeDate
on DischargeDate.DateID = TransferReference.DischargeDateID

left join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
on AdmissionDate.DateID = TransferReference.AdmissionDateID					


where
BaseEncounter.ContextCode = 'TRA||UG'
and
BaseEncounter.DischargeDestinationCode = '51-TFTToCE' 
and 
BaseEncounter.LastEpisodeInSpellIndicator = 1	

and 
not exists

(
Select 1
from #TransferEncounterCentral
where #TransferEncounterCentral.TransferOutMergeRecno = BaseEncounter.MergeEncounterRecno
)

and

(
AdmissionDate.TheDate between @StartDate and @EndDate										
or
DischargeDate.TheDate between @StartDate and @EndDate
)		
	
	
update #TransferEncounterCentral

set TransferInMergeRecno =

			(
			select
				min(TransferIn.MergeEncounterRecno)
			from
				WarehouseOLAPMergedV2.APC.BaseEncounter TransferIn

	
			where
				TransferIn.ContextCode = 'CEN||PAS' 
				
				and #TransferEncounterCentral.MatchedDistrictno = TransferIn.Districtno
				and datediff(minute, TransferIn.AdmissionTime, #TransferEncounterCentral.DischargeTime) between -2880 and 2880 --2880 hours 2 days
		
				)	
				
				where  #TransferEncounterCentral.TransferInMergeRecno is null	



-- Output Results

select 

[Transfering Division] = coalesce(TransferringDirectorate.Division,TransferDirectorate.Division,'')
,[Transfering Directorate] = coalesce(TransferringDirectorate.Directorate,TransferDirectorate.Directorate,'')
,[Discharge Ward] = coalesce(DischargeWard.LocalWard,'No Ward Found')
,[Discharge WardCode] = coalesce(TransferringEncounter.EndWardTypeCode,'NOWard')				
,[Discharge Destination Code] = coalesce(TransferringEncounter.DischargeDestinationCode,'')

,[Transfered To Division] = coalesce(TransferDirectorate.Division,'')
,[Transfered To Directorate] =  coalesce(TransferDirectorate.Directorate,'')
,[Admission Ward] = coalesce(AdmissionWard.LocalWard,'No Ward Found')
,[Admission Ward Code] = coalesce(TransferEncounter.StartWardTypeCode,'NOWard')
,[Admission Source Code] = coalesce(TransferEncounter.AdmissionSourceCode,'')	
			
,[Transfer Discharge Time] = TransferringEncounter.DischargeTime
,[Transfered To AdmissionTime] = TransferEncounter.AdmissionTime

,AdmissionDate = TransferEncounter.AdmissionDate

,DischargeDate = TransferEncounter.DischargeDate




--,TransferedToDischargeDate	= TransferEncounter.DischargeDate

,Matched =

Case
     --when TransferEncounter.providerspellno <> TransferEncounter.Globalproviderspellno then 'Patient Not Matched'
    
	when MatchedDistrictno is null then 'Patient Not Matched'
	
	When TransferEncounter.AdmissionMethodCode = '81-TRCent' and TransferEncounter.AdmissionSourceCode <> '51-TRINCEN' then 'Admission Method Code has been used Not AdmissionSource'
	
	when MatchedDistrictno is not null and TransferOutMergeRecno is null  then 'No Discharge Within 2 days'
	
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode = 'TF' 
		and TransferEncounter.ProviderSpellNo <> TransferEncounter.GlobalProviderSpellNo
		and datediff(minute, TransferringEncounter.DischargeTime,TransferEncounter.AdmissionTime) between -360 and 360 --6 hours 
		then 'Matched'	

	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode = 'TF'  and TransferEncounter.AdmissionSourceCode <> '51-TRINCEN'
		and  datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) between -360 and 360
		then 'Wrong Source Admission Code Within of 6 hours'	
		
		
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode = 'TF'  and TransferEncounter.AdmissionSourceCode <> '51-TRINCEN'
		and  datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
		then 'Wrong Source Admission Code Outside of 6 hours'								
		
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode = 'TF' 
		and  datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
		then 'Correct Destination Code Outside of 6 hours'
		
			
	
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode <> 'TF' 
		and   datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) between -360 and 360
		then 'Wrong Destination Code within 6 hours'						
		
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode <> 'TF' 
		and   datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
		then 'Wrong Destination Code Outside of 6 hours'	
		
	When wkd.TransferInMergeRecno is null then 'No Transfer Match'
else null	
end	
							
,[Central DistrictNo] = 
	Case 
		when left(TransferEncounter.DistrictNo,1) = 0 then TransferEncounter.DistrictNo 
		when left(TransferringEncounter.DistrictNo,1) = 0 then TransferringEncounter.DistrictNo 
		when left(MatchedDistrictno,1) = 0 then MatchedDistrictno
	else ' ' end


,[Trafford PatientID] = 
	Case 
		when left(TransferEncounter.DistrictNo,1) <> 0 then TransferEncounter.DistrictNo
		when left(TransferringEncounter.DistrictNo,1) <> 0 then TransferringEncounter.DistrictNo 
		when left(MatchedDistrictno,1) <> 0 then MatchedDistrictno
	else ' ' end


,[NHS Number] = coalesce(TransferEncounter.NHSNumber,TransferringEncounter.NHSNumber)


,Direction = 
	Case when Direction  = 'CEN||PAS' then 'Trafford to Central' else 'Central to Trafford' end

,LOS = 
	DATEDIFF(day,TransferEncounter.AdmissionTime,coalesce(TransferEncounter.DischargeDate,getdate()))				

,[Case] = 1
,[Case Last 2 Months] = 
	Case 
		when DATEPART(Month,getdate ()) = DATEPART(Month,TransferEncounter.DischargeDate) then 1 
		when DATEPART(Month,getdate ())-1 = DATEPART(Month,TransferEncounter.DischargeDate) then 1 
	else 0 end



from

#TransferEncounter wkd

left join WarehouseOLAPMergedV2.APC.BaseEncounter TransferEncounter
on TransferEncounter.MergeEncounterRecno = wkd.TransferInMergeRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferReference
on TransferReference.MergeEncounterRecno = TransferEncounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Calendar DischargeDate
on DischargeDate.DateID = TransferReference.DischargeDateID

left join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
on AdmissionDate.DateID = TransferReference.AdmissionDateID					

left join WarehouseOLAPMergedV2.WH.Ward AdmissionWard
on AdmissionWard.SourceWardID = TransferReference.StartWardID

left join WarehouseOLAPMergedV2.wh.Directorate TransferDirectorate
on TransferDirectorate.DirectorateCode = TransferEncounter.StartDirectorateCode

left join WarehouseOLAPMergedV2.APC.BaseEncounter TransferringEncounter
on TransferringEncounter.MergeEncounterRecno = wkd.TransferOutMergeRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferringReference
on TransferringReference.MergeEncounterRecno = TransferringEncounter.MergeEncounterRecno	

left join WarehouseOLAPMergedV2.WH.Ward DischargeWard
on DischargeWard.SourceWardID = TransferringReference.EndWardID

left join WarehouseOLAPMergedV2.wh.Directorate TransferringDirectorate
on TransferringDirectorate.DirectorateCode = TransferringEncounter.EndDirectorateCode

where Direction = 'TRA||UG'

		and (								
	TransferOutMergeRecno is not null or TransferInMergeRecno is not null

			)


union all

		
				
				
-- Output Results

select 

[Transfering Division] = coalesce(TransferringDirectorate.Division,TransferDirectorate.Division,'')
,[Transfering Directorate] = coalesce(TransferringDirectorate.Directorate,TransferDirectorate.Directorate,'')
,[Discharge Ward] = coalesce(DischargeWard.LocalWard,'No Ward Found')
,[Discharge Ward Code] = coalesce(TransferringEncounter.EndWardTypeCode,'NOWard')								
,[Discharge Destination Code] = coalesce(TransferringEncounter.DischargeDestinationCode,'')

,[Transfered To Division] = coalesce(TransferDirectorate.Division,'')
,[Transfered To Directorate] =  coalesce(TransferDirectorate.Directorate,'')
,[Admission Ward] = coalesce(AdmissionWard.LocalWard,'No Ward Found')
,[Admission Ward Code] = coalesce(TransferEncounter.StartWardTypeCode,'NOWard')
,[Admission Source Code] = coalesce(TransferEncounter.AdmissionSourceCode,'')	
			
,[Transfer Discharge Time] = TransferringEncounter.DischargeTime
,[Transfered To AdmissionTime] = TransferEncounter.AdmissionTime


,AdmissionDate = TransferEncounter.AdmissionDate

,DischargeDate = TransferEncounter.DischargeDate


--	,TransferedToDischargeDate	= TransferEncounter.DischargeDate

,Matched =

Case

     --when TransferEncounter.providerspellno <> TransferEncounter.Globalproviderspellno then 'Patient Not Matched'
	when MatchedDistrictno is null then 'Patient Not Matched'
	
	when MatchedDistrictno is not null and TransferOutMergeRecno is null  
	and TransferEncounter.ProviderSpellNo <> TransferEncounter.GlobalProviderSpellNo then 'Discharge not Detected But match Found'
	
	when MatchedDistrictno is not null and TransferOutMergeRecno is null  then 'No Discharge Within 2 days'
	
	When TransferringEncounter.DischargeDestinationCode <> '51-TFTToCE'  and TransferEncounter.AdmissionSourceCode = 'TF'
	and datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
	then 'Wrong Discharge Destination Code and Outside of 6 hours'
	
	When TransferringEncounter.DischargeDestinationCode <> '51-TFTToCE'  and TransferEncounter.AdmissionSourceCode = 'TF'
	and datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
	then 'Wrong Discharge Destination Code and Within 6 hours'
	
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode = '51-TFTToCE' 
		and datediff(minute, TransferringEncounter.DischargeTime,TransferEncounter.AdmissionTime) between -360 and 3600 --8 hours 
		then 'Matched'	
		
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode = '51-TFTToCE'  and TransferEncounter.AdmissionSourceCode <> '51-TRINCEN'
		and  datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
		then 'Wrong Source Admission Code Outside of 6 hours'								
		
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode = '51-TFTToCE' 
		and  datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
		then 'Correct DestinationCode Outside of 6 hours'
		
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode <> '51-TFTToCE' 
	and TransferEncounter.AdmissionSourceCode <> '51-TRINCEN'
		and   datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) between -360 and 360
		then 'Wrong Source Admission within 6 hours'									
	
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode <> '51-TFTToCE' 
		and   datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) between -360 and 360
		then 'Wrong Destination Code within 6 hours'						
		
	when TransferOutMergeRecno is not null and TransferringEncounter.DischargeDestinationCode <> '51-TFTToCE' 
		and   datediff(minute, TransferringEncounter.DischargeTime, TransferEncounter.AdmissionTime) not between -360 and 360
		then 'Wrong Destination Code Outside of 6 hours'	
		
	When wkd.TransferInMergeRecno is null then 'No Transfer Match'
		
	
		else null	
		end	
		
,[Central DistrictNo] = 
	Case 
		when left(TransferEncounter.DistrictNo,1) = 0 then TransferEncounter.DistrictNo 
		when left(TransferringEncounter.DistrictNo,1) = 0 then TransferringEncounter.DistrictNo 
		when left(MatchedDistrictno,1) = 0 then MatchedDistrictno
	else ' ' end


,[Trafford PatientID] = 
	Case 
		when left(TransferEncounter.DistrictNo,1) <> 0 then TransferEncounter.DistrictNo
		when left(TransferringEncounter.DistrictNo,1) <> 0 then TransferringEncounter.DistrictNo 
		when left(MatchedDistrictno,1) <> 0 then MatchedDistrictno
	else ' ' end

,NHSNumber = coalesce(TransferEncounter.NHSNumber,TransferringEncounter.NHSNumber)


--,TransferInMergeRecno 				
--,TransferOutMergeRecno = TransferOutMergeRecno 
--,TransferEncounter.ProviderSpellNo
--,TransferEncounter.GlobalProviderSpellNo
,Direction = 
Case when Direction  = 'CEN||PAS' then 'Trafford to Central' else 'Central to Trafford' end

,LOS = 
DATEDIFF(day,TransferEncounter.AdmissionTime,coalesce(TransferEncounter.DischargeDate,getdate()))

,[Case] = 1

,[Case Last 2 Months] = 
Case when DATEPART(Month,getdate ()) = DATEPART(Month,TransferEncounter.DischargeDate) then 1 
	 when DATEPART(Month,getdate ())-1 = DATEPART(Month,TransferEncounter.DischargeDate) then 1 


else 0 end




from

#TransferEncounterCentral wkd

left join WarehouseOLAPMergedV2.APC.BaseEncounter TransferEncounter
on TransferEncounter.MergeEncounterRecno = wkd.TransferInMergeRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferReference
on TransferReference.MergeEncounterRecno = TransferEncounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Calendar DischargeDate
on DischargeDate.DateID = TransferReference.DischargeDateID

left join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
on AdmissionDate.DateID = TransferReference.AdmissionDateID						

left join WarehouseOLAPMergedV2.WH.Ward AdmissionWard
on AdmissionWard.SourceWardID = TransferReference.StartWardID

left join WarehouseOLAPMergedV2.wh.Directorate TransferDirectorate
on TransferDirectorate.DirectorateCode = TransferEncounter.StartDirectorateCode

left join WarehouseOLAPMergedV2.APC.BaseEncounter TransferringEncounter
on TransferringEncounter.MergeEncounterRecno = wkd.TransferOutMergeRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounterReference TransferringReference
on TransferringReference.MergeEncounterRecno = TransferringEncounter.MergeEncounterRecno	

left join WarehouseOLAPMergedV2.WH.Ward DischargeWard
on DischargeWard.SourceWardID = TransferringReference.EndWardID

left join WarehouseOLAPMergedV2.wh.Directorate TransferringDirectorate
on TransferringDirectorate.DirectorateCode = TransferringEncounter.EndDirectorateCode	

where
		 Direction = 'CEN||PAS' 
		 
		 and (								
	TransferOutMergeRecno is not null or TransferInMergeRecno is not null

			)

					
				
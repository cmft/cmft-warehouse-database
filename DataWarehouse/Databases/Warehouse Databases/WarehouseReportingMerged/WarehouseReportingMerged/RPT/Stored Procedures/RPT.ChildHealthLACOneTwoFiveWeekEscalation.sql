﻿
CREATE procedure [RPT].[ChildHealthLACOneTwoFiveWeekEscalation] as

--Weekly - LAC 1,2 and 5 Week Escalation

select distinct
	 Exam.EsclationPeriodWeeks
	,Exam.DueDate
	,Exam.ExamCode
	,Demographics.NHSNumber
	,Demographics.Surname
	,Demographics.Forename
	,Demographics.SexCode
	,Demographics.DateOfBirth
	,LeadClinician.LeadClinician
	,LACStatus.LookedAfterStatus
	,LACA.DelayReason
	,LACA.RequestSentTo
from 
	Warehouse.ChildHealth.Entity Demographics

inner join Warehouse.ChildHealth.ChildFlag Flag
on	Flag.EntitySourceUniqueID = Demographics.SourceUniqueID
and	Flag.FlagCode = 'Y'
and	Flag.RemovedDate is null

inner join
	(
	select
		 EntitySourceUniqueID
		,ExamScheduled.DueDate
		,ExamScheduled.ExamCode
		,EsclationPeriodWeeks = datediff(week , DueDate , getdate())
	from
		Warehouse.ChildHealth.ExamScheduled
	) Exam
on	Exam.EntitySourceUniqueID = Demographics.SourceUniqueID
and	Exam.DueDate <= dateadd(DAY , -7 , getdate())
and	Exam.ExamCode in ('LR' , 'FA' , 'RL')
and Exam.EsclationPeriodWeeks in (1 , 2 , 5)
				
left join Warehouse.ChildHealth.LookedAfter LAC
on	LAC.EntitySourceUniqueID = Demographics.SourceUniqueID
			
left join Warehouse.ChildHealth.LookedAfterStatus LACStatus
on	LACStatus.LookedAfterStatusCode = LAC.LookedAfterStatusCode

left join Warehouse.ChildHealth.LookedAfterAssessmentSummary LACA
on	LACA.EntitySourceUniqueID = Demographics.SourceUniqueID
and LACA.ChildSeen = 'No'

left join Warehouse.ChildHealth.LeadClinician 
on	LeadClinician.LeadClinicianCode = LAC.LeadClinicianCode
		

order by
	Exam.EsclationPeriodWeeks	
		
	
		
		

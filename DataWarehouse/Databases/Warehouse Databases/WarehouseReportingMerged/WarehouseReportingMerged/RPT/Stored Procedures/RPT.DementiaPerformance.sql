﻿





CREATE Procedure [RPT].[DementiaPerformance] 
	@PeriodStartDate datetime
	,@PeriodEndDate datetime
	,@Q1Denominator int = null
	,@Q1Numerator int = null
	,@Q2Denominator int = null
	,@Q2Numerator int = null
	,@Q3Denominator int = null
	,@Q3Numerator int = null
	,@Ward varchar(50) = 'All'



as

declare @LocalStart datetime = @PeriodStartDate
declare @LocalEnd datetime = (@PeriodEndDate + '23:59')


select 
	ReportingMonth 
	,GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,DischargeDate
	,DischargeTime
	,LocalDischargeMethod
	,PatientSurname
	,SexCode
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard 
	,LatestSpecialtyCode 
	,LatestConsultantCode 
	,AgeOnAdmission 
	,LoS 
	,OriginalSubmissionTime	
	,OriginalResponse	
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	,Q1Denominator
	,Q1Numerator
	,Q2Denominator
	,Q2Numerator
	,Q3Denominator
	,Q3Numerator
	,DementiaUpdated
	,WardUpdated
from
	APC.DementiaCQUIN

where 
	ReportingMonth between @LocalStart and @LocalEnd
and 
	(
		Q1Denominator = @Q1Denominator 
	or 
		@Q1Denominator is null
	)
and
	(
		Q1Numerator = @Q1Numerator 
	or
		@Q1Numerator is null
	)
and 
	(
		Q2Denominator = @Q2Denominator 
	or 
		@Q2Denominator is null
	)
and
	(
		Q2Numerator = @Q2Numerator 
	or
		@Q2Numerator is null
	)
and 
	(
		Q3Denominator = @Q3Denominator 
	or 
		@Q3Denominator is null
	)
and 
	(
		Q3Numerator = @Q3Numerator
	or 
		@Q3Numerator is null
	)
and
	(
		LatestWard = @Ward
	or
		@Ward = 'All'
	)
		

﻿

CREATE PROCEDURE [RPT].[GetEddPerformance]
	@startdate smalldatetime
	,@enddate smalldatetime
	,@specialtycode varchar(max)
	,@Division varchar(max)
	,@patientcategory varchar(max)
AS 


--DECLARE @startdate  smalldatetime= '30 Apr 2011'
--DECLARE @enddate smalldatetime= '06 May 2012'
--DECLARE @specialtycode nvarchar(max) = N'100'
--DECLARE @Division nvarchar(max) = N'Childrens'
--DECLARE @patientcategory nvarchar(max) = N'EL'

SELECT   
	 ExpectedLOS.SourcePatientNo
	,ExpectedLOS.SourceSpellNo
	,ManagementIntentionCode = ManagementIntention.SourceIntendedManagementCode 
	,ExpectedLOS_Calc = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))
	,ActualLOS = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime))
	,Division = ISNULL(division, 'Unknown') 
	,SpecialtyCode = Specialty.NationalSpecialtyCode 
	,Specialty = Specialty.NationalSpecialty
	,WardCode = Ward.SourceWardCode
	,Ward = Ward.SourceWard
	,ConsultantCode = Consultant.LocalConsultantCode
	,Consultant = isnull(consultant.Title,'') + ' ' +isnull(consultant.Initials,'') + ' ' + isnull(Consultant.Surname,'')
	,POD = case AdmissionMethod.AdmissionTypeCode
				when 'EL' THEN 'EL'
				else 'NE' end
	,PathwayCondition = ISNULL(ExpectedLOS.RTTPathwayCondition,'Unknown')
	,Discharges = 1 
	,ExpectedLOS.AdmissionTime
	,AdmissionDate = CONVERT(DATE, ExpectedLOS.AdmissionTime)
	,DischargeDate = CONVERT(DATE, ExpectedLOS.DischargeTime)
	,EDDDate = DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime))
	,ExpectedLOS.EddCreatedTime
	,ExpectedLOS.ExpectedLOS
	,Exp_LOS = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))
	,Actual_LOS = DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) 
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))  <0 THEN 1 ELSE 0 END AS [varless0days]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   =0 THEN 1 ELSE 0 END AS [varequal0days]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   IN(1,2) THEN 1 ELSE 0 END AS [var1-2days]	
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >2 THEN 1 ELSE 0 END AS [var3plusdays]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0 THEN 1 ELSE 0 END AS [vargreater0days]
	, CASE WHEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   >0 THEN DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),CONVERT(DATE,DischargeTime)) - DATEDIFF(DAY,CONVERT(DATE,AdmissionTime),DATEADD(DAY,ExpectedLOS.ExpectedLOS,CONVERT(DATE,AdmissionTime)))   ELSE 0 END AS [sumover0EDD]
	,CasenoteNumber = ExpectedLOS.CasenoteNumber
	,ExpectedLOS.ProviderSpellNo
	,ExpectedLOS.EddCreatedByConsultantFlag
FROM 
	 APC.Encounter ExpectedLOS
	 
LEFT OUTER JOIN WH.Directorate Directorate
	ON ExpectedLOS.StartDirectorateCode = Directorate.DirectorateCode

LEFT OUTER JOIN WH.Specialty
	ON ExpectedLOS.SpecialtyID =  Specialty.SourceSpecialtyID 

LEFT OUTER JOIN APC.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodID = ExpectedLOS.AdmissionMethodID

LEFT OUTER JOIN APC.IntendedManagement ManagementIntention
	on	ManagementIntention.SourceIntendedManagementID = ExpectedLOS.IntendedManagementID

LEFT OUTER JOIN WH.Consultant Consultant
	ON ExpectedLOS.ConsultantID = Consultant.SourceConsultantID

LEFT OUTER JOIN APC.Ward Ward
	ON ExpectedLOS.StartWardID = Ward.SourceWardID

WHERE     
		CONVERT(date, ExpectedLOS.DischargeTime, 103) >= @startdate --'14/Aug/2011'
	AND CONVERT(date, ExpectedLOS.DischargeTime, 103) <= @enddate --'14/Aug/2011'
	and ExpectedLOS.EpisodeStartTime = ExpectedLOS.admissionTime
	AND CASE 
			WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) < 1440 THEN 1
			ELSE 0
		 END = 1
	AND ManagementIntention.SourceIntendedManagementCode NOT IN ('R','N') 
	AND NOT (Ward.SourceWardCode = 'ESTU' AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6)
	AND Specialty.NationalSpecialtyCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@specialtycode,','))
    AND Division IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))
	AND Specialty.NationalSpecialtyCode <> '501'
	AND Ward.SourceWard <> '76A'
	AND Specialty.SourceSpecialty NOT like '%DIALYSIS%'
	AND Specialty.sourceSpecialtycode NOT like 'IH%'
	AND Ward.SourceWard NOT like 'SUB%'
	AND case AdmissionTypeCode
				when 'EL' THEN 'EL'
				else 'NE' end IN (SELECT VALUE FROM warehousereporting.RPT.SSRS_MultiValueParamSplit(@patientcategory,','))

 






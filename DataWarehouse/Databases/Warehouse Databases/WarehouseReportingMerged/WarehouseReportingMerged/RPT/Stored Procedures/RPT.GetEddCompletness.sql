﻿




/****** Script for SelectTopNRows command from SSMS  ******/

create PROCEDURE [RPT].[GetEddCompletness]
(
	 @startdate smalldatetime
	,@enddate smalldatetime
	,@specialtycode varchar(max)
	,@Division varchar(max)
	,@patientcategory varchar(max)
)

AS 
--DECLARE @startdate smalldatetime = '1 Apr 2012'
--,@enddate smalldatetime= '1 May 2012', @specialtycode varchar(max) = '300'
--,@division varchar(max) = 'Acute Medical', @patientcategory varchar(max) = 'NE'

SELECT 
	 ISNULL(division, 'Unknown') AS Division
	,ISNULL(Specialty.NationalSpecialtyCode, 'Unknown') AS SpecialtyCode
	,ISNULL(Specialty.NationalSpecialty, 'Unknown') AS Specialty
	,ISNULL(Ward.SourceWardCode, 'Unknown') AS WardCode
	,ISNULL(Ward.SourceWard, 'Unknown') AS Ward
	,ISNULL(Consultant.NationalConsultantCode,'Unknown') AS ConsultantCode
	,isnull(consultant.Title, '') + ' ' + isnull(consultant.Initials, '') + ' ' + isnull(Consultant.Surname,'') AS Consultant
	,ISNULL(ExpectedLOS.RTTPathwayCondition,'Unknown') AS PathwayCondition
	,POD = case AdmissionTypeCode
				when 'EL' THEN 'EL'
				else 'NE' end
	,1 as Discharges
	,WithExpectedLOSInPAS = 
		CASE 
			WHEN ExpectedLOS.EddInterfaceCode ='PAS' THEN 1
			ELSE 0 
		END
	,HasEddBedmanFlag = 
		CASE 
			WHEN ExpectedLOS.EddInterfaceCode ='Bed' THEN 1
			ELSE 0 
		END
	,HasEddUGFlag = 
		CASE 
			WHEN ExpectedLOS.EddInterfaceCode ='UG' THEN 1
			ELSE 0 
		END
FROM 
	APC.Encounter ExpectedLOS
	 
LEFT OUTER JOIN WH.Directorate Directorate
	ON ExpectedLOS.StartDirectorateCode = Directorate.DirectorateCode

LEFT OUTER JOIN WH.Specialty
	ON ExpectedLOS.SpecialtyID =  Specialty.SourceSpecialtyID 

LEFT OUTER JOIN APC.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodID = ExpectedLOS.AdmissionMethodID

LEFT OUTER JOIN APC.IntendedManagement ManagementIntention
	on	ManagementIntention.SourceIntendedManagementID = ExpectedLOS.IntendedManagementID

LEFT OUTER JOIN WH.Consultant Consultant
	ON ExpectedLOS.ConsultantID = Consultant.SourceConsultantID

LEFT OUTER JOIN APC.Ward Ward
	ON ExpectedLOS.StartWardID = Ward.SourceWardID
WHERE
		CONVERT(date, ExpectedLOS.DischargeTime, 103) >= @startdate --'14/Aug/2011'
	AND CONVERT(date, ExpectedLOS.DischargeTime, 103) <= @enddate --'14/Aug/2011'
	and ExpectedLOS.EpisodeStartTime = ExpectedLOS.admissionTime
	AND ManagementIntention.SourceIntendedManagementCode NOT IN ('R','N') 
	AND NOT (Ward.SourceWardCode = 'ESTU' AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6)
	AND Specialty.NationalSpecialtyCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@specialtycode,','))
	AND Division IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))
	AND Specialty.NationalSpecialtyCode <> '501'
	AND Ward.SourceWard <> '76A'
	AND Specialty.SourceSpecialty NOT like '%DIALYSIS%'
	AND Specialty.SourceSpecialtycode NOT like 'IH%'
	AND Ward.SourceWard NOT like 'SUB%'
	AND case AdmissionTypeCode
				when 'EL' THEN 'EL'
				else 'NE' end IN (SELECT VALUE FROM warehousereporting.RPT.SSRS_MultiValueParamSplit(@patientcategory,','))
	
	






﻿

CREATE proc [RPT].[SepsisAlert] as

declare @Start date 
declare @End date = cast(getdate() as date)

set @Start = dateadd(month, -1, @End)

if object_id('tempdb..#Sepsis') is not null
drop table #Sepsis

-- Observations linked with A&E attendance

select --top 1000
	Encounter.DistrictNo
	,Encounter.AttendanceNumber
	,Encounter.DateOfBirth
	,Encounter.ArrivalTime
	,BaseObservationSet.SourceUniqueID
	,ResultCode = Measure.MeasureCode
	,ResultName = Measure.Measure
	,Result = 
			coalesce(
				cast(Observation.NumericResult as varchar)
				,Reference.Reference
				)
	,ResultTime = BaseObservationSet.StartTime
	,ResultComment = cast(null as varchar(max))
	,OverallRiskIndexCode
	,CriticalCarePeriod = 0
into
	#Sepsis
from
	AE.Encounter

inner join WarehouseOLAPMergedV2.AE.BaseEncounterBaseObservationSet
on	Encounter.MergeEncounterRecno = BaseEncounterBaseObservationSet.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.Observation.BaseObservationSet
on	BaseObservationSet.MergeObservationSetRecno = BaseEncounterBaseObservationSet.MergeObservationSetRecno

inner join WarehouseOLAPMergedV2.Observation.Observation
on	Observation.ObservationSetSourceUniqueID = BaseObservationSet.SourceUniqueID

inner join WarehouseOLAPMergedV2.Observation.Measure
on	Measure.MeasureID = Observation.MeasureID

left join WarehouseOLAPMergedV2.Observation.Reference
on	Reference.ReferenceID = Observation.ListID
and	Reference.DomainCode =  Measure.ListCode

where
	Encounter.SiteCode = 'RW3MR'
and Encounter.ArrivalDate between @Start and @End
and datediff(hour, Encounter.ArrivalTime, BaseObservationSet.StartTime) < = 48
and	(
		(
			Measure.MeasureCode = 'TMPRT'
		and	(
				Observation.NumericResult < 36
			or	Observation.NumericResult > 38.3
			)
		)
	or (
			Measure.MeasureCode = 'HEART_HR'
		and	Observation.NumericResult > 90
		)
	or	(
			Measure.MeasureCode = 'RESPR'
		and	Observation.NumericResult > 20
		)
	or	(
			Measure.MeasureCode = 'AVPU' 
		and	Reference.Reference != 'Alert'
		)
-- organ failure
	or	(
			Measure.MeasureCode in ('LYGBP_SBP',' STDBP_SBP')
		and	Observation.NumericResult < 90
		)
	or	(
			Measure.MeasureCode in ('O2SAT',' PO2ST')
		and	Observation.NumericResult <= 90
		)
	)

-- Observations linked with APC admission linked with A&E attendance

insert #Sepsis

select --top 1000
	Encounter.DistrictNo
	,Encounter.AttendanceNumber
	,Encounter.DateOfBirth
	,Encounter.ArrivalTime
	,BaseObservationSet.SourceUniqueID
	,ResultCode = Measure.MeasureCode
	,ResultName = Measure.Measure
	,Result = 
			coalesce(
				cast(Observation.NumericResult as varchar)
				,Reference.Reference
				)
	,ResultTime = BaseObservationSet.StartTime
	,ResultComment = null
	,OverallRiskIndexCode
	,CriticalCarePeriod = 
					case
					when exists
						(
						select
							1
						from
							APC.Bedday

						inner join APC.Encounter BeddayEncounter
						on Bedday.MergeAPCEncounterRecno = BeddayEncounter.MergeEncounterRecno

						where
							BeddayEncounter.ProviderSpellNo = APCEncounter.ProviderSpellNo
						and	Bedday.DatasetCode = 'CC'
						)
					then 1
					else 0
					end
from
	AE.Encounter

inner join APC.EncounterAEEncounter
on	Encounter.MergeEncounterRecno = EncounterAEEncounter.AEMergeEncounterRecno

inner join APC.Encounter APCEncounter
on	APCEncounter.MergeEncounterRecno = EncounterAEEncounter.APCMergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseObservationSet
on	EncounterAEEncounter.APCMergeEncounterRecno = BaseEncounterBaseObservationSet.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.Observation.BaseObservationSet
on	BaseObservationSet.MergeObservationSetRecno = BaseEncounterBaseObservationSet.MergeObservationSetRecno

inner join WarehouseOLAPMergedV2.Observation.Observation
on	Observation.ObservationSetSourceUniqueID = BaseObservationSet.SourceUniqueID

inner join WarehouseOLAPMergedV2.Observation.Measure
on	Measure.MeasureID = Observation.MeasureID

left join WarehouseOLAPMergedV2.Observation.Reference
on	Reference.ReferenceID = Observation.ListID
and	Reference.DomainCode =  Measure.ListCode

where
	Encounter.SiteCode = 'RW3MR'
and	Encounter.ArrivalDate between @Start and @End
and datediff(hour, Encounter.ArrivalTime, BaseObservationSet.StartTime) < = 48
and	(
		(
			Measure.MeasureCode = 'TMPRT'
		and	(
				Observation.NumericResult < 36
			or	Observation.NumericResult > 38.3
			)
		)
	or	(
			Measure.MeasureCode = 'HEART_HR'
		and	Observation.NumericResult > 90
		)
	or	(
			Measure.MeasureCode = 'RESPR'
		and	Observation.NumericResult > 20
		)
	or	(
			Measure.MeasureCode = 'AVPU' 
		and	Reference.Reference != 'Alert'
		)
-- organ failure
	or	(
			Measure.MeasureCode in ('LYGBP_SBP',' STDBP_SBP')
		and	Observation.NumericResult < 90
		)
	or	(
			Measure.MeasureCode in ('O2SAT',' PO2ST')
		and	Observation.NumericResult <= 90
		)
	)

-- Results linked with A&E attendance

insert #Sepsis

select --top 1000
	Encounter.DistrictNo
	,Encounter.AttendanceNumber
	,Encounter.DateOfBirth
	,Encounter.ArrivalTime
	,BaseObservationSet.SourceUniqueID
	,ResultCode = Result.LocalResultCode
	,ResultName = Result.LocalResult
	,Result = cast(ReportableResult.Result as varchar)
	,ResultTime = ReportableResult.EffectiveTime
	,ResultComment = ReportableResult.ResultComment 
	,OverallRiskIndexCode
	,CriticalCarePeriod = 0
from
	AE.Encounter

inner join WarehouseOLAPMergedV2.AE.BaseEncounterBaseObservationSet
on	Encounter.MergeEncounterRecno = BaseEncounterBaseObservationSet.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.Observation.BaseObservationSet
on	BaseObservationSet.MergeObservationSetRecno = BaseEncounterBaseObservationSet.MergeObservationSetRecno

inner join WarehouseOLAPMergedV2.AE.BaseEncounterBaseResult
on	Encounter.MergeEncounterRecno = BaseEncounterBaseResult.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.Result.ReportableResult
on	ReportableResult.MergeResultRecno = BaseEncounterBaseResult.MergeResultRecno

inner join WarehouseOLAPMergedV2.Result.Result
on	Result.SourceResultID = ReportableResult.ResultID

where
	SiteCode = 'RW3MR'
and ArrivalDate between @Start and @End
and datediff(hour, Encounter.ArrivalTime, ReportableResult.EffectiveTime) < = 48
and	(
		(
			Result.LocalResultCode = 'WBC'
		and	isnumeric(ReportableResult.Result) = 1
		and	(
				cast(ReportableResult.Result as float) < 4
			or	cast(ReportableResult.Result as float) > 12
			)
		)
	or	(
			Result.LocalResultCode = '187405'
		and	ReportableResult.Result is null
		and	ReportableResult.ResultComment not like '%No Growth%'
		)
--organ failure
	or	(
			Result.LocalResultCode = 'PLT'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) < 100
		)
	or	(
			Result.LocalResultCode = 'CR'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 177
		)
	or	(
			Result.LocalResultCode = 'TBIL'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 34
		)
	or	(
			Result.LocalResultCode = 'INR'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 2
		)
	or	(
			Result.LocalResultCode = 'APTT-D'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 60
		)
	or	(
			Result.LocalResultCode in ('PT5', 'PTP')
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 19
		)
	)

-- Results linked with APC admission linked with A&E attendance

insert #Sepsis

select --top 1000
	Encounter.DistrictNo
	,Encounter.AttendanceNumber
	,Encounter.DateOfBirth
	,Encounter.ArrivalTime
	,BaseObservationSet.SourceUniqueID
	,ResultCode = Result.LocalResultCode
	,ResultName = Result.LocalResult
	,Result = cast(ReportableResult.Result as varchar)
	,ResultTime = ReportableResult.EffectiveTime
	,ReportableResult.ResultComment 
	,OverallRiskIndexCode
	,CriticalCarePeriod = 
					case
					when exists
						(
						select
							1
						from
							APC.Bedday

						inner join APC.Encounter BeddayEncounter
						on Bedday.MergeAPCEncounterRecno = BeddayEncounter.MergeEncounterRecno

						where
							BeddayEncounter.ProviderSpellNo = APCEncounter.ProviderSpellNo
						and	Bedday.DatasetCode = 'CC'
						)
					then 1
					else 0
					end
from
	AE.Encounter

inner join APC.EncounterAEEncounter
on	Encounter.MergeEncounterRecno = EncounterAEEncounter.AEMergeEncounterRecno

inner join APC.Encounter APCEncounter
on	APCEncounter.MergeEncounterRecno = EncounterAEEncounter.APCMergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseObservationSet
on	EncounterAEEncounter.APCMergeEncounterRecno = BaseEncounterBaseObservationSet.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.Observation.BaseObservationSet
on	BaseObservationSet.MergeObservationSetRecno = BaseEncounterBaseObservationSet.MergeObservationSetRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseResult
on	EncounterAEEncounter.APCMergeEncounterRecno = BaseEncounterBaseResult.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.Result.ReportableResult
on	ReportableResult.MergeResultRecno = BaseEncounterBaseResult.MergeResultRecno

inner join WarehouseOLAPMergedV2.Result.Result
on	Result.SourceResultID = ReportableResult.ResultID

where
	Encounter.SiteCode = 'RW3MR'
and	Encounter.ArrivalDate between @Start and @End
and	datediff(hour, Encounter.ArrivalTime, ReportableResult.EffectiveTime) < = 48
and	(
		(
			Result.LocalResultCode = 'WBC'
		and	isnumeric(ReportableResult.Result) = 1
		and	(
				cast(ReportableResult.Result as float) < 4
			or	cast(ReportableResult.Result as float) > 12
			)
		)
	or	(
			Result.LocalResultCode = '187405'
		and	ReportableResult.ResultComment not like '%No Growth%'
		)
--organ failure
	or	(
			Result.LocalResultCode = 'PLT'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) < 100
		)
	or	(
			Result.LocalResultCode = 'CR'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 177
		)
	or	(
			Result.LocalResultCode = 'TBIL'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 34
		)
	or	(
			Result.LocalResultCode = 'INR'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 2
		)
	or	(
			Result.LocalResultCode = 'APTT-D'
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 60
		)
	or	(
			Result.LocalResultCode in ('PT5', 'PTP')
		and	isnumeric(ReportableResult.Result) = 1
		and	cast(ReportableResult.Result as float) > 19
		)
	)

select
	DistrictNo
	,AttendanceNumber
	,DateOfBirth
	,ArrivalTime
	,SourceUniqueID
	,SequenceNo = 
			dense_rank() over (partition by Sepsis.AttendanceNumber order by Sepsis.SourceUniqueID)
	,ResultGroup = 
		case
		when ResultCode in ('LYGBP_SBP', 'STDBP_SBP', 'O2SAT', 'PO2ST', 'PLT', 'CR', 'TBIL', 'INR', 'APTT-D', 'PT5', 'PTP')
		then 'Organ Failure'
		else 'Sepsis Alert'
		end
	,ResultCode
	,ResultName
	,Result
	,ResultTime
	,ResultComment
	,OverallRiskIndexCode
	,SignsOfInfection1 = 
		case
		when exists
			(
			select
				1
			from
				#Sepsis Alert
			where
				Sepsis.SourceUniqueID = Alert.SourceUniqueID
				and	Alert.ResultCode in ('AVPU', 'HEART_HR', 'RESPR', 'TMPRT', 'WBC')
			having
				count(distinct Alert.ResultCode) >=2
			)
		then 1
		else 0
		end
	,SignsOfInfection2 = 
		case
		when Sepsis.ResultCode = 'WBC'
		and	Sepsis.OverallRiskIndexCode >=3
		then 1
		else 0
		end
		--case
		--when exists
		--	(
		--	select
		--		1
		--	from
		--		#Sepsis EWSWBC
		--	where
		--		Sepsis.SourceUniqueID = EWSWBC.SourceUniqueID
		--	and	EWSWBC.ResultCode = 'WBC'
		--	and EWSWBC.OverallRiskIndexCode >=3
		--	)
		--then 1
		--else 0
		--end
	,SignsOfInfection3 = 
		case
		when Sepsis.ResultCode = '187405'
		then 1
		else 0
		end
		--case
		--when exists
		--	(
		--	select
		--		1
		--	from
		--		#Sepsis EWSWBC
		--	where
		--		Sepsis.SourceUniqueID = EWSWBC.SourceUniqueID
		--	and	EWSWBC.ResultCode = '187405'
		--	)
		--then 1
		--else 0
		--end
	,OrganFailure =
		case
		when exists
			(
			select
				1
			from
				#Sepsis OrganFailure
			where
				Sepsis.SourceUniqueID = OrganFailure.SourceUniqueID
			and	OrganFailure.ResultCode in ('LYGBP_SBP', 'STDBP_SBP', 'O2SAT', 'PO2ST', 'PLT', 'CR', 'TBIL', 'INR', 'APTT-D', 'PT5', 'PTP')
			)
		then 1
		else 0
		end
	,CriticalCarePeriod
from
	#Sepsis Sepsis
where
	exists
		(
		select
			1
		from
			#Sepsis Alert
		where
			Sepsis.SourceUniqueID = Alert.SourceUniqueID
			and	Alert.ResultCode in ('AVPU', 'HEART_HR', 'RESPR', 'TMPRT', 'WBC')
		having
			count(distinct Alert.ResultCode) >=2
		)
or	exists
		(
		select
			1
		from
			#Sepsis EWSWBC
		where
			Sepsis.SourceUniqueID = EWSWBC.SourceUniqueID
		and	EWSWBC.ResultCode = 'WBC'
		and EWSWBC.OverallRiskIndexCode >=3
		)
or	exists
		(
		select
			1
		from
			#Sepsis EWSWBC
		where
			Sepsis.SourceUniqueID = EWSWBC.SourceUniqueID
		and	EWSWBC.ResultCode = '187405'
		)

--order by
--	AttendanceNumber
--	,SourceUniqueID
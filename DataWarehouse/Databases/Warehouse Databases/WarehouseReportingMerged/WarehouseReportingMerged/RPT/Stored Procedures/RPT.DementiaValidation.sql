﻿--use WarehouseReportingMerged
--exec Rpt.DementiaResponseNotDischarged


CREATE Procedure [RPT].[DementiaValidation]

	@Admission int = null
	,@Assessment int = null
	,@Referral int = null
	,@Ward varchar(50) = 'All'
	

as

Select
	GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,PatientSurname
	,Sex
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard
	,LatestSpecialtyCode
	,LatestConsultantCode
	,AgeOnAdmission 
	,LoS 
	,[LoS(hrs)]
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	,Admission
	,Assessment
	,Referral
	,DementiaUpdated
	,WardUpdated
from	
	APC.DementiaCQUINValidation
where
	(
		(
			Admission = @Admission
		or
			@Admission is null
		)
	and
		(
			Assessment = @Assessment
		or
			@Assessment is null
		)
	and
		(
			Referral = @Referral
		or
			@Referral is null
		)
	)
and
	(
		LatestWard = @Ward
	or
		@Ward = 'All'
	)
		
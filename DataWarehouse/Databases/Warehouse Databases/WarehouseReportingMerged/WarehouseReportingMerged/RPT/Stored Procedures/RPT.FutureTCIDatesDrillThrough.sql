﻿



CREATE PROCEDURE [RPT].[FutureTCIDatesDrillThrough]
(
	 @Division				VARCHAR(50)		= NULL
	,@DirectorateCode		VARCHAR(20)		= NULL
	,@NationalSpecialtyCode	VARCHAR(1000)	= NULL
	,@DrillType				VARCHAR(5)
	,@AdmissionTypeDesc		VARCHAR(10)		= NULL
	,@ApptWeekEnd			DATE			= NULL
	,@NoticeGivenWeeks		VARCHAR(5)		= NULL
	,@WaitingListDate		DATE			= NULL
)
AS
	
	-- Get latest waiting list census date
	IF (@WaitingListDate IS NULL)
		SELECT 
			@WaitingListDate = MAX(CensusDate)
		FROM
			APCWL.Encounter
		WHERE 
			CensusDate > DATEADD(DAY,-7,GETDATE())	
					
	-- Main data query
	SELECT
		 E.TCIDateID
		,AdmissionMethod				= 
			case 
			when ContextCode = 'TRA||UG' and E.ManagementIntentionCode = 'ActiveDC' then 'Elective Admission - Booked'
			when ContextCode = 'TRA||UG' and E.ManagementIntentionCode = 'ActiveIP' then 'Elective Admission - Booked'
			when ContextCode = 'TRA||UG' and E.ManagementIntentionCode = 'PlanIP' then 'Elective Admission - Planned'
			when ContextCode = 'TRA||UG' and E.ManagementIntentionCode = 'PlanDC' then 'Elective Admission - Booked'
			else AM.NationalAdmissionMethod
			end
		,AdmissionTypeDesc				= CASE	
											when ManagementIntentionCode = 'PlanIP' then 'Plnd'
											when ManagementIntentionCode = 'PlanDC' then 'Plnd'
											when AM.NationalAdmissionMethodCode = '13' THEN 'Plnd'
											
											ELSE 'WL'
										  END
		,NoticePeriod					= 
			case
			when ContextCode = 'TRA||UG' and TCICAL.TheDate <> '01 jan 1900' then DATEDIFF(DAY, DateOnWaitingList, TCICAL.TheDate) / 7
			else DATEDIFF(DAY, PRE.PreAdmitDate1, TCICAL.TheDate) / 7 
			end
		,E.DirectorateCode
		,E.SpecialtyId
		,E.ConsultantId
		,E.ContextCode
		,PRE.PreAdmitDate1
		,TCIDate = TCICAL.TheDate
		,Ward							= E.WardCode
		,E.CasenoteNumber
		,E.DistrictNo
		,ManagementIntention			= IM.NationalIntendedManagement
		,PreAdmissionDate				= 	
					case
					when ContextCode = 'TRA||UG' then DateOnWaitingList
					else PRE.PreAdmitDate1
					end
		
	INTO #DATA	
	FROM				
		APCWL.Encounter E
		
		LEFT JOIN  [Infocom_PAS].[dbo].[smsmir_preadmission] PRE 
			ON		E.SourcePatientNo			= PRE.InternalNo
				AND E.SourceEncounterNo			= PRE.EpisodeNo
				AND PRE.PreAdmCancRsn			IS NULL
				
		INNER JOIN WH.Calendar TCICAL
			ON		E.TCIDateID					= TCICAL.DateID
			
		INNER JOIN APC.AdmissionMethod AM
			ON		E.AdmissionMethodID			= AM.SourceAdmissionMethodId
				AND	E.ContextCode				= AM.SourceContextCode
				
		LEFT JOIN APC.IntendedManagement IM
			ON		E.ManagementIntentionCode	= IM.SourceIntendedManagementCode
				AND	E.ContextCode				= IM.SourceContextCode
		
	WHERE
			E.CensusDate					= @WaitingListDate
		AND	TCICAL.LastDayOfWeek			= @ApptWeekEnd

	-- Lookup tables and filter further
	SELECT
		 Division					= DIR.Division
		,Specialty					= SPEC.NationalSpecialtyLabel
		,ConsultantSurname			= CONS.Surname
		,Ward						= #DATA.Ward
		,Casenote					= #DATA.CasenoteNumber
		,DistrictNo					= #Data.DistrictNo
		,ManagementIntention		= #DATA.ManagementIntention
		,AdmissionMethod			= #DATA.AdmissionMethod
		,PreAdmissionDate			= #DATA.PreAdmissionDate
		,TCIDate					= #DATA.TCIDate
		,NoticePeriod				= #DATA.NoticePeriod
		
	FROM
		#DATA
		
		INNER JOIN WH.Directorate DIR
			ON		#DATA.DirectorateCode			= DIR.DirectorateCode
			
		INNER JOIN WH.Specialty SPEC
			ON		#DATA.SpecialtyId				= SPEC.SourceSpecialtyId
				AND #DATA.ContextCode				= SPEC.SourceContextCode

		INNER JOIN WH.Consultant CONS
			ON		#DATA.ConsultantId				= CONS.SourceConsultantID
				AND #DATA.ContextCode				= CONS.SourceContextCode
	
	WHERE
			SPEC.NationalSpecialtyCode	IN (SELECT Item from fn_ListToTable(@NationalSpecialtyCode,','))
		AND DIR.Division				= COALESCE(@Division, DIR.Division)
		AND DIR.DirectorateCode			= COALESCE(@DirectorateCode, DIR.DirectorateCode)		
		AND
		( 
			(
					@DrillType				= 'NGN'			-- Notice Given columns Drill Type
				AND CASE 
						WHEN #DATA.NoticePeriod	<= 8 THEN CAST(#DATA.NoticePeriod AS VARCHAR(5)) 
						WHEN #DATA.NoticePeriod	> 8  THEN '8+'
						ELSE 'NPA'
					END	= @NoticeGivenWeeks
			)
			OR
			(
					@DrillType				= 'WKD'			-- Weekend Display columns Drill Type
				AND #DATA.AdmissionTypeDesc	= COALESCE(@AdmissionTypeDesc, #DATA.AdmissionTypeDesc)
			)
		)
			
	ORDER BY
		 TCIDate
		,Casenote






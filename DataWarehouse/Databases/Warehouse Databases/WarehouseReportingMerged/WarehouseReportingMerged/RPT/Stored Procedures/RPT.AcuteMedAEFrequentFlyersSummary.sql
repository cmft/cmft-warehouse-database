﻿

create procedure [RPT].[AcuteMedAEFrequentFlyersSummary]

as

SELeCT									
	 DistrictNo								
	,PatientForename								
	,PatientSurname								
	,[PCT Description] = coalesce([PCT Description] , '')								
    ,GP = coalesce(GP , '')									
    ,[GP Address] = coalesce([GP Address] , '')									
	,[1] = coalesce([1] , '')								
	,[2] = coalesce([2] , '')								
	,[3] = coalesce([3] , '')								
	,[4] = coalesce([4] , '')								
	,[5] = coalesce([5] , '')								
	,[6] = coalesce([6] , '')								
	,[7] = coalesce([7] , '')								
	,[8] = coalesce([8] , '')								
	,[9] = coalesce([9] , '')								
	,[10] = coalesce([10] , '')								
	,[11] = coalesce([11] , '')								
	,[12] = coalesce([12] , '')								
	,Total = Cases								
FROM									
	(								
	Select 								
		 Encounter.DistrictNo							
		,PatientForename							
		,PatientSurname							
		,[PCT Description] = max([Primary Care Organisation].[Organisation Name])							
		,GP = max([General Medical Practitioner].[Organisation Name])							
		,[GP Address] = max([General Medical Practitioner].[Address Line 1])							
		,Month = cast(right(FinancialMonthKey,2) as int)							
		,Activity = count(1)							
		,Cases							
									
	From								
		AE.Encounter							
									
	inner Join WH.Calendar								
	on EncounterEndDateID = DateID								
									
	left join Organisation.dbo.[Primary Care Organisation]								
	on CommissionerCode = [Primary Care Organisation].[Organisation Code]								
									
	left join Organisation.dbo.[General Medical Practitioner]								
	on RegisteredGpCode = [General Medical Practitioner].[Organisation Code]								
									
	inner join								
		(							
		Select top 30							
			DistrictNo						
			,Cases = count(1)						
		from							
			AE.Encounter						
									
		inner Join WH.Calendar							
		on EncounterEndDateID = DateID							
									
		inner join WH.Site							
		on	Site.SourceSiteID = Encounter.SiteID						
									
		inner JOin AE.AttendanceCategory							
		On AttendanceCategory.SourceAttendanceCategoryID = Encounter.AttendanceCategoryID							
									
		where							
			Calendar.TheDate >= '01 Apr 2012'						
		and Calendar.TheDate < '01 apr 2013'							
		and	Site.NationalSiteCode = 'RW3MR'						
		and	Encounter.ContextCode = 'CEN||SYM'						
		and NationalAttendanceCategoryCode = '1'							
									
		group by							
			 DistrictNo						
									
		order by							
			count(1) desc						
									
		) TopThirty							
	on TopThirty.DistrictNo = Encounter.DistrictNo								
									
	inner join WH.Site								
	on	Site.SourceSiteID = Encounter.SiteID							
									
	inner JOin AE.AttendanceCategory								
	On AttendanceCategory.SourceAttendanceCategoryID = Encounter.AttendanceCategoryID								
									
	where								
		Calendar.TheDate >= '01 Apr 2012'							
	and Calendar.TheDate < '01 Apr 2013'								
	and	Site.NationalSiteCode = 'RW3MR'							
	and	Encounter.ContextCode = 'CEN||SYM'							
	and NationalAttendanceCategoryCode = '1'								
									
	group by								
		 Encounter.DistrictNo							
		,PatientForename							
		,PatientSurname							
		,FinancialMonthKey							
		,Cases							
									
	) as SourceTable								
PiVOT									
	(								
	sum(Activity)								
	for Month IN ([0], [1], [2], [3], [4]	,[5]	,[6]	,[7]	,[8]	,[9]	,[10]	,[11]	,[12])
	) as PivotTable								
order by									
	Cases desc								


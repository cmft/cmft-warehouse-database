﻿


CREATE PROCEDURE [RPT].[CodingOpportunity]
(
	 @StartDate						DATE
	,@EndDate						DATE
	,@Division						VARCHAR(500)	= NULL
	,@PatientClassificationCode		VARCHAR(500)	= NULL
	,@AdmissionMethodCode			VARCHAR(500)	= NULL
)
AS
/****************************************************************************************
	Stored procedure : RPT.CodingOpportunity
	Description		 : SSRS proc for the Visual Studio report
	
	Reports >> DQ >> Coding Opportunity
	
	Measures
	 Analysis of FCE Primary Diagnoses R Codes

	Modification History
	====================
	
	Date      Person     Description
	====================================================================================
	20.01.14    MH       Intial Coding
*****************************************************************************************/
	
	IF @PatientClassificationCode = ''
		SET @PatientClassificationCode = NULL

	IF @AdmissionMethodCode = ''
		SET @AdmissionMethodCode = NULL
				
	SELECT
		 SpellNumber			= FCE1.GlobalProviderSpellNo
		,DischargeDate			= CAL.TheDate
		,Period					= CAL.TheMonth
		,PeriodKey				= CAL.FinancialMonthKey
		,FinancialYear			= CAL.FinancialYear
		,Division				= DIR.Division
		,FCE1CodingOpportunity	= FCE1DIAG.IsRCodeCodingOpportunity
		,FCE2CodingOpportunity	= COALESCE(FCE2DIAG.IsRCodeCodingOpportunity, 0)
		,SpellAllRCode			= 1
		,EpisodeCount			= CASE WHEN FCE2.MergeEncounterRecNo IS NULL THEN 1 ELSE 2 END
		
	FROM
		APC.Encounter FCE1					-- 1st episode
	
		CROSS APPLY fn_DiagnosisCodeExtension(FCE1.PrimaryDiagnosisCode) FCE1DIAG
				
		INNER JOIN WH.Directorate DIR
			ON		FCE1.StartDirectorateCode		= DIR.DirectorateCode
					
		INNER JOIN WH.Calendar CAL
			ON		FCE1.DischargeDateID			= CAL.DateId
			
		INNER JOIN APC.PatientClassification PC
			ON		FCE1.ContextCode				= PC.SourceContextCode
				AND	FCE1.PatientClassificationCode	= PC.SourcePatientClassificationCode
				
		INNER JOIN APC.AdmissionMethod AM
			ON		FCE1.ContextCode				= AM.SourceContextCode
				AND	FCE1.AdmissionMethodCode		= AM.SourceAdmissionMethodCode
				
		LEFT JOIN APC.Encounter FCE2		-- 2nd episode, if present
			ON		FCE1.GlobalProviderSpellNo		= FCE2.GlobalProviderSpellNo
				AND	FCE2.GlobalEpisodeNo			= 2
				
		OUTER APPLY fn_DiagnosisCodeExtension(FCE2.PrimaryDiagnosisCode) FCE2DIAG
				
	WHERE
			FCE1.DischargeDate BETWEEN @StartDate AND @EndDate	
		AND	FCE1.GlobalEpisodeNo				= 1
		-- 1st and 2nd episodes to be R Codes, or 1st only if single episode spell
		AND FCE1DIAG.IsRCode					= 1
		AND COALESCE(FCE2DIAG.IsRCode, 1)		= 1
		AND DIR.Division						IN (SELECT Item FROM dbo.fn_ListToTable(@Division,','))	
		AND 
		(
				@PatientClassificationCode IS NULL
			OR	PC.NationalPatientClassificationCode IN (SELECT Item FROM dbo.fn_ListToTable(@PatientClassificationCode,','))
		)
		AND 
		(
				@AdmissionMethodCode IS NULL
			OR	AM.NationalAdmissionMethodCode IN (SELECT Item FROM dbo.fn_ListToTable(@AdmissionMethodCode,','))
		)




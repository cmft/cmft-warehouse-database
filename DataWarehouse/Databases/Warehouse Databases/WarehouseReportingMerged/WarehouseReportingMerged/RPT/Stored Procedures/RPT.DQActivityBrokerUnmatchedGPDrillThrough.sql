﻿
/******************************************************************************
**  Name: RPT.DQActivityBrokerUnmatchedGPDrillThrough
**  Purpose: Data Quality report for un-matched ActivityBroker patients
**           where the orrigin and destination GP practice code do not match
**
**	Input Parameters
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 18.11.13      MH       Created
******************************************************************************/

CREATE PROCEDURE RPT.DQActivityBrokerUnmatchedGPDrillThrough
AS
	DECLARE @SnapshotTime			DATETIME
	DECLARE @LatestAPCWLCensusDate	DATETIME
	DECLARE @LatestOPWLCensusDate	DATETIME
	DECLARE @NullDate				DATE = '1 Jan 1900'

-- Get the time the data was built	
	SELECT TOP 1
		@SnapshotTime = EventTime
	FROM
		ActivityBroker.Utility.AuditLog
	ORDER BY EventTime DESC
		
-- Get latest APC and OP WL census dates
	SELECT TOP 1
		@LatestAPCWLCensusDate = CensusDate
	FROM
		APCWL.Encounter
	ORDER BY CensusDate DESC
		
	SELECT TOP 1
		@LatestOPWLCensusDate =CensusDate
	FROM
		OPWL.Encounter
	ORDER BY CensusDate DESC

-- Build a temp table mapping an AB.APCWait row to an APCWL.Encounter row	
	SELECT				-- Central Rows
		 W.APCWaitId
		,E.EncounterRecno
	INTO #TAPCWL
	FROM
		ActivityBroker.AB.APCWait W
	INNER JOIN APCWL.Encounter E
		ON		CAST(W.SourcePatientNo AS VARCHAR(20))	= E.SourcePatientNo
			AND W.SourceEntityRecno						= E.SourceEncounterNo
			AND W.ContextCode							= E.ContextCode
			AND E.CensusDate							= @LatestAPCWLCensusDate
	WHERE
		W.EPRApplicationContextID = 1	
			
	UNION
	
	SELECT				-- Trafford Rows
		 W.APCWaitId
		,E.EncounterRecno
	FROM
		ActivityBroker.AB.APCWait W
		INNER JOIN APCWL.Encounter E
			ON		W.ContextCode		= E.ContextCode
				AND W.SourceUniqueID	= E.SourceUniqueID
				AND E.CensusDate		= @LatestAPCWLCensusDate
	WHERE
		W.EPRApplicationContextID = 2

-- Build a temp table mapping an AB.OPWait row to an OPWL.Encounter row	
	SELECT				-- Central Rows
		 W.OPWaitId
		,E.EncounterRecno
	INTO #TOPWL
	FROM
		ActivityBroker.AB.OPWait W
	INNER JOIN OPWL.Encounter E
		ON		CAST(W.SourcePatientNo AS VARCHAR(20))	= E.SourcePatientNo
			AND W.SourceEntityRecno						= E.SourceEncounterNo
			AND W.ContextCode							= E.ContextCode
			AND E.CensusDate							= @LatestOPWLCensusDate
	WHERE
		W.EPRApplicationContextID = 1	
			
	UNION
	
	SELECT				-- Trafford Rows
		 W.OPWaitId
		,E.EncounterRecno
	FROM
		ActivityBroker.AB.OPWait W
		INNER JOIN OPWL.Encounter E
			ON		W.ContextCode		= E.ContextCode
				AND W.SourceUniqueID	= E.SourceUniqueID
				AND E.CensusDate		= @LatestOPWLCensusDate
	WHERE
		W.EPRApplicationContextID = 2
		
-- Build a temp table mapping an AB.APCSpell row to an APC.Encounter row	
	SELECT				-- Central Rows
		 S.APCSpellId
		,E.EncounterRecno
	INTO #TAPC
	FROM
		ActivityBroker.AB.APCSpell S
	INNER JOIN APC.Encounter E
		ON		CAST(S.SourcePatientNo AS VARCHAR(20))	= E.SourcePatientNo
			AND S.SourceEntityRecno						= E.SourceEncounterNo
			AND S.ContextCode							= E.ContextCode
			AND E.FirstEpisodeInSpellIndicator			= 1
	WHERE
		S.EPRApplicationContextID = 1	
			
	UNION
	
	SELECT				-- Trafford Rows
		 S.APCSpellId
		,E.EncounterRecno
	FROM
		ActivityBroker.AB.APCSpell S
		INNER JOIN APC.Encounter E
			ON		S.ContextCode						= E.ContextCode
				AND S.SourceUniqueID					= E.SourceUniqueID
				AND E.FirstEpisodeInSpellIndicator		= 1
	WHERE
		S.EPRApplicationContextID = 2
		
-- Patient level data on mismatched GP practice code

	SELECT
		 SourceUniqueId		= D.PathwayId
		,OriginSite			= COALESCE(ORIGSITE.NationalSite,ORIGSITE2.NationalSite)
		,OriginSpecialty	= ORIGSPEC.NationalSpecialty
		,NHSNumber			= D.NHSNumber
		,OriginDistrictNo	= D.OrigDistrictNo
		,DestDistrictNo		= D.DestDistrictNo
		,Category			= D.Type
		,CENGP				= D.CENGP
		,TRAGP				= D.TRAGP
		,OriginRecNo		= D.OriginEncounterRecno
		,DestRecNo			= D.DestEncounterRecno
				 
	FROM	
	(
		-- APC Waiting List actions
		SELECT
			  PathwayId			= P.PathwayID
			 ,OrigSiteCode		= ORIGAPCWAIT.SiteCode
			 ,OrigSpecCode		= ORIGAPCWAIT.SpecialtyCode
			 ,OrigContextCode	= ORIGAPCWL.ContextCode
			 ,NHSNumber			= ORIGAPCWL.NHSNumber
			 ,OrigDistrictNo	= ORIGAPCWL.DistrictNo
			 ,DestDistrictNo	= DESTAPCWL.DistrictNo
			 ,Type				= 'Inpatient Waiting List'
			 ,CENGP				= CASE 
									WHEN ORIGAPCWAIT.EPRApplicationContextID = 1 THEN  ORIGAPCWL.RegisteredGpPracticeCode
									ELSE DESTAPCWL.RegisteredGpPracticeCode
								  END
			 ,TRAGP				= CASE 
									WHEN ORIGAPCWAIT.EPRApplicationContextID = 2 THEN  ORIGAPCWL.RegisteredGpPracticeCode
									ELSE DESTAPCWL.RegisteredGpPracticeCode
								  END
			 ,OriginEncounterRecno	= ORIGAPCWL.EncounterRecno
			 ,DestEncounterRecno	= DESTAPCWL.EncounterRecno
			
		FROM
			ActivityBroker.dbo.ActivityBrokerTracker ABT
			INNER JOIN ActivityBroker.AB.Pathway P
				ON		ABT.PathwayID					= P.PathwayID
			INNER JOIN ActivityBroker.AB.APCWait ORIGAPCWAIT
				ON		P.OriginAPCWaitID				= ORIGAPCWAIT.APCWaitID
			INNER JOIN #TAPCWL ORIGTWL
				ON		P.OriginAPCWaitID				= ORIGTWL.APCWaitId
			INNER JOIN APCWL.Encounter ORIGAPCWL
				ON		ORIGTWL.EncounterRecNo			= ORIGAPCWL.EncounterRecno
			INNER JOIN #TAPCWL DESTTWL
				ON		P.DestinationAPCWaitID			= DESTTWL.APCWaitId
			INNER JOIN APCWL.Encounter DESTAPCWL
				ON		DESTTWL.EncounterRecNo			= DESTAPCWL.EncounterRecno
			
		WHERE
				P.PatientIsMatched	= 1
			AND P.PathwayTypeID		= 1
			AND ORIGAPCWL.RegisteredGpPracticeCode <> DESTAPCWL.RegisteredGpPracticeCode

		UNION ALL

	-- OP Waiting List actions
		SELECT
			  PathwayId			= P.PathwayID
			 ,OrigSiteCode		= ORIGOPWL.SiteCode
			 ,OrigSpecCode		= ORIGOPWL.SpecialtyCode
			 ,OrigContextCode	= ORIGOPWL.ContextCode
			 ,NHSNumber			= ORIGOPWL.NHSNumber
			 ,OrigDistrictNo	= ORIGOPWL.DistrictNo
			 ,DestDistrictNo	= DESTOPWL.DistrictNo
			 ,Type				= 'Outpatient Waiting List'
			 ,CENGP				= CASE 
									WHEN ORIGOPWAIT.EPRApplicationContextID = 1 THEN  ORIGOPWL.RegisteredGpPracticeCode
									ELSE DESTOPWL.RegisteredGpPracticeCode
								  END
			 ,TRAGP				= CASE 
									WHEN ORIGOPWAIT.EPRApplicationContextID = 2 THEN  ORIGOPWL.RegisteredGpPracticeCode
									ELSE DESTOPWL.RegisteredGpPracticeCode
								  END
			 ,OriginEncounterRecno	= ORIGOPWL.EncounterRecno
			 ,DestEncounterRecno	= DESTOPWL.EncounterRecno
			
		FROM
			ActivityBroker.dbo.ActivityBrokerTracker ABT
			INNER JOIN ActivityBroker.AB.Pathway P
				ON		ABT.PathwayID					= P.PathwayID
			INNER JOIN ActivityBroker.AB.OPWait ORIGOPWAIT
				ON		P.OriginOPWaitID				= ORIGOPWAIT.OPWaitID
			INNER JOIN #TOPWL ORIGTWL
				ON		P.OriginOPWaitID				= ORIGTWL.OPWaitId
			INNER JOIN OPWL.Encounter ORIGOPWL
				ON		ORIGTWL.EncounterRecNo			= ORIGOPWL.EncounterRecno
			INNER JOIN #TOPWL DESTTOPWL
				ON		P.DestinationOPWaitID			= DESTTOPWL.OPWaitId
			INNER JOIN OPWL.Encounter DESTOPWL
				ON		DESTTOPWL.EncounterRecNo		= DESTOPWL.EncounterRecno
			
		WHERE
				P.PatientIsMatched	= 1
			AND P.PathwayTypeID		= 2
			AND ORIGOPWL.RegisteredGpPracticeCode <> DESTOPWL.RegisteredGpPracticeCode
		
		UNION ALL

	-- Inpatient spell transfer actions
		SELECT
			  PathwayId			= P.PathwayID
			 ,OrigSiteCode		= ORIGAPC.SiteCode
			 ,OrigSpecCode		= ORIGAPC.SpecialtyCode
			 ,OrigContextCode	= ORIGAPC.ContextCode
			 ,NHSNumber			= ORIGAPC.NHSNumber
			 ,OrigDistrictNo	= ORIGAPC.DistrictNo
			 ,DestDistrictNo	= DESTAPC.DistrictNo
			 ,Type				= 'Inpatient Spell Transfers'
			 ,CENGP				= CASE 
									WHEN ORIGAPCSPELL.EPRApplicationContextID = 1 THEN  ORIGAPC.RegisteredGpPracticeCode
									ELSE DESTAPC.RegisteredGpPracticeCode
								  END
			 ,TRAGP				= CASE 
									WHEN ORIGAPCSPELL.EPRApplicationContextID = 2 THEN  ORIGAPC.RegisteredGpPracticeCode
									ELSE DESTAPC.RegisteredGpPracticeCode
								  END
			 ,OriginEncounterRecno	= ORIGAPC.EncounterRecno
			 ,DestEncounterRecno	= DESTAPC.EncounterRecno
			
		FROM
			ActivityBroker.dbo.ActivityBrokerTracker ABT
			INNER JOIN ActivityBroker.AB.Pathway P
				ON		ABT.PathwayID				= P.PathwayID
			INNER JOIN ActivityBroker.AB.APCSpellTransfer ORIGAPCSPELL
				ON		P.OriginAPCSpellID			= ORIGAPCSPELL.APCSpellID
			INNER JOIN #TAPC ORIGTAPC
				ON		P.OriginAPCSpellID			= ORIGTAPC.APCSpellID
			INNER JOIN APC.Encounter ORIGAPC
				ON		ORIGTAPC.EncounterRecNo		= ORIGAPC.EncounterRecno
			INNER JOIN #TAPC DESTTAPC
				ON		P.DestinationAPCSpellID		= DESTTAPC.APCSpellID
			INNER JOIN APC.Encounter DESTAPC
				ON		DESTTAPC.EncounterRecNo		= DESTAPC.EncounterRecno
			
		WHERE
				P.PatientIsMatched	= 1
			AND P.PathwayTypeID		= 3
			AND ORIGAPC.RegisteredGpPracticeCode <> DESTAPC.RegisteredGpPracticeCode
	) D
	LEFT JOIN WH.Site ORIGSITE
		ON		D.OrigSiteCode					= ORIGSITE.SourceSiteCode
			AND D.OrigContextCode				= ORIGSITE.SourceContextCode
	-- To handle a mis-match on context code and corresponding site code
	-- eg actual data is context code TRA||UG with Site Code of TRAF
	LEFT JOIN WH.Site ORIGSITE2
		ON		D.OrigSiteCode					= ORIGSITE2.SourceSiteCode
			AND D.OrigContextCode				= 'CEN||PAS'

	LEFT JOIN WH.Specialty ORIGSPEC
		ON		D.OrigSpecCode					= ORIGSPEC.SourceSpecialtyCode
			AND D.OrigContextCode				= ORIGSPEC.SourceContextCode

	
﻿

create procedure [RPT].[AcuteMedPreviousDaysAdmissions5CL]

as

select 
	 AdmissionMethod =  AdmissionMethod.NationalAdmissionMethod
	,encounter.CasenoteNumber
	,AdmissionDate  = AdmissionDate.TheDate
	,encounter.PatientForename
	,encounter.PatientSurname
	,encounter.DateOfBirth
	,encounter.NHSNumber
	,encounter.DistrictNo
	,WardCode = ward.SourceWardCode
	,Practice.ParentOrganisationCode
from
	APC.Encounter encounter 
inner join organisation.dbo.Practice 
	on encounter.RegisteredGpPracticeCode = Practice.OrganisationCode
inner join wh.Calendar AdmissionDate 
	on AdmissionDate.DateID = Encounter.AdmissionDateID
inner join wh.Ward
	on ward.SourceWardID = encounter.StartWardID
inner join apc.AdmissionMethod
	on AdmissionMethod.SourceAdmissionMethodID = encounter.AdmissionMethodID
where
	AdmissionDate.TheDate = dateadd(dd,-1,convert(date,getDate(),112))
and Ward.SourceWardCode In ('MAU','15M','CLDU','MRU','7M','OMU','AMU')
and encounter.FirstEpisodeInSpellIndicator = 1
and Practice.ParentOrganisationCode ='5CL'



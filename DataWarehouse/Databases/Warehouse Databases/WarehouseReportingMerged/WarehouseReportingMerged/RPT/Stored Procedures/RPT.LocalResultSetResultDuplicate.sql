﻿
CREATE proc [RPT].[LocalResultSetResultDuplicate] 

as

select
	AttributeCode
	,ContextCode
	,ValueCode
	,Value
	,DisciplineCode
	,ChameleonCode = cast(ChameleonCode as bit)

from
	(
	select 
		AttributeCode = 'RESULTSET'
		,ContextCode = 'L'
		,ValueCode = CENICEInvestigationReference.InvestigationCode
		,Value = CENICEInvestigationReference.InvestigationName
		,DisciplineCode = CENICEInvestigationReference.SpecialtyCode
		,ReferenceMapCode = CENICEInvestigationReference.ReferenceMapInvestigationCode 
		,ChameleonCode = 
				case
				when exists 
						(
						select
							1
						from
							WarehouseOLAPMergedV2.Result.ResultSet
						where
							ResultSet.LocalResultSetCode = CENICEInvestigationReference.InvestigationCode
						and ResultSet.LocalResultSet = CENICEInvestigationReference.InvestigationName
						)
				then 1
				else 0
				end	
	from
		Warehouse.Result.CENICEInvestigationReference

	where
		exists
			(
			select
				1
			from
				Warehouse.Result.CENICEInvestigationReference SameCodeDifferentDescription
			where
				SameCodeDifferentDescription.InvestigationCode = CENICEInvestigationReference.InvestigationCode
			and SameCodeDifferentDescription.InvestigationName != CENICEInvestigationReference.InvestigationName
			)

	union

	select 
		AttributeCode = 'RESULT'
		,ContextCode = 'L'
		,ValueCode = CENICEResultReference.ResultCode
		,Value = CENICEResultReference.ResultName
		,DisciplineCode = CENICEResultReference.SpecialtyCode
		,ReferenceMapCode = CENICEResultReference.ReferenceMapResultCode
		,ChameleonCode = 
				case
				when exists 
						(
						select
							1
						from
							WarehouseOLAPMergedV2.Result.Result
						where
							Result.LocalResultCode = CENICEResultReference.ResultCode
						and	Result.LocalResult = CENICEResultReference.ResultName
						)
				then 1
				else 0
				end		
	from
		Warehouse.Result.CENICEResultReference

	where
		exists
			(
			select
				1
			from
				Warehouse.Result.CENICEResultReference SameCodeDifferentDescription
			where
				SameCodeDifferentDescription.ResultCode = CENICEResultReference.ResultCode
			and SameCodeDifferentDescription.ResultName != CENICEResultReference.ResultName
			)	
	) LocalResultSetResult

order by
	AttributeCode
	,ValueCode







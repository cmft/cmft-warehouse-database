﻿




	create proc [RPT].[RenalTransplantSurgery]					
			
			 @StartDate AS DATE = null
			,@EndDate AS DATE = null
									
	as								
 		declare @SetStartDate datetime = coalesce(@StartDate,'01 Apr 2013')
 		declare @SetEnddate datetime = coalesce(@EndDate,(select DATEADD(day, DATEDIFF(day,0,GETDATE()), 0))) 
 								


		select distinct

				NationalSpecialty
				,Surname Consultant
				,DiabeticFlag =
				Case when DiabFlag = 'Yes' then 'Yes'
				else 'No' end
				,apc.Encounter.CasenoteNumber
				,DistrictNo
				,PatientForename
				,PatientSurname
				,NationalEthnicCategory
				,AdmissionTime
				,PrimaryProcedureDate
				,Op.Operation
				,SecondaryProcedureCode1
				,SecondaryProcedureCode2
				,SecondaryProcedureCode3
				,SecondaryProcedureCode4
				,SecondaryProcedureCode5
				,SecondaryProcedureCode6
				,SecondaryProcedureCode7
				,SecondaryProcedureCode8
				,SecondaryProcedureCode9
				,SecondaryProcedureCode10
				,SecondaryProcedureCode11

		from
		
				APC.Encounter
				left join WH.Specialty 
				on WH.Specialty.SourceSpecialtyID = APC.Encounter.SpecialtyID

				left join
				APC.Operation
				on APC.Operation.MergeEncounterRecno = APC.Encounter.MergeEncounterRecno
				left join
				WH.EthnicCategory
				on WH.EthnicCategory.SourceEthnicCategoryID = APC.Encounter.EthnicOriginID
				left join
				WH.Consultant
				on WH.Consultant.SourceConsultantID = APC.Encounter.ConsultantID
				left join
				WH.Site 
				on WH.Site.SourceSiteID = APC.Encounter.siteid 
				left join
				WarehouseReporting.WH.Operation Op
				on  Op.OperationCode = APC.Encounter.PrimaryProcedureCode
				
				left join
					
					(select distinct
					
					casenotenumber
					,'Yes' DiabFlag
					
					from
					APC.Encounter left join
					APC.Diagnosis on
					APC.Diagnosis.MergeEncounterRecno = APC.Encounter.MergeEncounterRecno
			
					where
					
					DischargeDate >= '01 Jan 2013' 
					and diagnosiscode between 'E10' and 'E14'

					) Diab
					on Diab.CasenoteNumber = APC.Encounter.CasenoteNumber

		where

				DischargeDate between @SetStartDate and @SetEnddate

				and wh.specialty.NationalSpecialtyCode = '102'
				and APC.Operation.OperationCode in (
				'X40.3',
				'L74.2',
				'L74.3',
				'L74.5',
				'L74.6',
				'L75.2',
				'L91.2',
				'L91.4',
				'L91.5',
				'S60.4')




		order by 
				CasenoteNumber
				,AdmissionTime asc
									
												
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								
								







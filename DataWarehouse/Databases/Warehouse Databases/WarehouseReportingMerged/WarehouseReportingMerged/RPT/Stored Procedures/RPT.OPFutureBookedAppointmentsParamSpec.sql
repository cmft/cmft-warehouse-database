﻿

CREATE PROCEDURE [RPT].[OPFutureBookedAppointmentsParamSpec]
(
	 @StartBaseDate			DATE		= NULL
	,@Division				VARCHAR(50)	= NULL
	,@DirectorateCode		VARCHAR(20)	= NULL
	,@NoWeekEnds			INT			= 4		-- Determines the end date of the query and represents the number of 
) AS
	DECLARE @StartDate		DATE
	DECLARE @EndDate		DATE
	
	SET @StartBaseDate = COALESCE(@StartBaseDate, GETDATE())
	
	SET DATEFIRST  1	-- Monday

	SET @StartDate  = DATEADD(DAY, 1 - DATEPART(DW, @StartBaseDate), @StartBaseDate)
	SET @EndDate	= DATEADD(DAY, -1, DATEADD(WEEK, @NoWeekEnds, @StartDate))		-- End Date is a Sunday

	SELECT 
		 NationalSpecialtyCode = 'All'
		,NationalSpecialty = 'All'

union all

	SELECT DISTINCT
		 SPEC.NationalSpecialtyCode
		,SPEC.NationalSpecialty

	FROM
	(
		SELECT DISTINCT 
			 E.ReferralSpecialtyId
			,E.ContextCode
			,E.DirectorateCode
		FROM
			OP.Encounter E
			INNER JOIN WH.Calendar APPTCAL
				ON		E.AppointmentDateID			 = APPTCAL.DateID
		WHERE
				APPTCAL.TheDate BETWEEN @StartDate AND @EndDate
			AND E.DirectorateCode = COALESCE(@DirectorateCode, E.DirectorateCode)
			AND 
		
				(
				 E.AppointmentCreateDate IS NOT NULL
				 or
				 ContextCode = 'TRA||UG'
				) 
			
	) DATA
	
	INNER JOIN WH.Specialty SPEC
		ON		DATA.ReferralSpecialtyId		= SPEC.SourceSpecialtyId
			AND DATA.ContextCode				= SPEC.SourceContextCode
			
	INNER JOIN WH.Directorate DIR
		ON		DATA.DirectorateCode			= DIR.DirectorateCode
			AND	DIR.Division					= COALESCE(@Division, DIR.Division)
	
	ORDER BY 		
		NationalSpecialty

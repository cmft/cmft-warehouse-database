﻿

--3.
CREATE proc [RPT].[GetStrokeValidationListDetails]

@StartDate Datetime,
@EndDate datetime

--declare @StartDate Datetime,
--@EndDate datetime

as

--set		@StartDate = N'01 Jun 2012'
--set		@EndDate = N'30 Jun 2012'

SELECT
	myqry.PCTDescription
	,myqry.InternalNo
	,myqry.EpisodeNo
	,PrimaryDiagnosisIndicator = CASE WHEN myqry.DiagnosisIndicator = 'Primary' then 'Yes' else 'No' end
	,OtherDiagnosisIndicator = 'No'
	,myqry.CasenoteNumber
	,myqry.PatientForename
	,myqry.PatientSurname
	,myqry.AdmissionTime
	,myqry.DischargeTime
	,Numerator = myqry.LOSStrokeWard
	,DenominatorIncAE = myqry.Denominator + ISNULL(myqry.AETime,0)
	,PercentageOnStrokeWardIncAE = ((myqry.LOSStrokeWard*1.00)/(myqry.Denominator + ISNULL(myqry.AETime,0))*1.00)*100
	,AllDiagnosisCodes.EpisodeStartTime
	,AllDiagnosisCodes.[PrimaryDiagnosisCode]
	,AllDiagnosisCodes.[SubsidiaryDiagnosisCode]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode1]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode2]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode3]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode4]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode5]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode6]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode7]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode8]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode9]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode10]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode11]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode12]
	,myqry.TheMonth
	,(SELECT DISTINCT Division 
			FROM APC.Encounter E2
			INNER JOIN WH.Directorate D2
				ON E2.StartDirectorateCode = D2.DirectorateCode
		  WHERE FirstEpisodeInSpellIndicator = 1 AND ProviderSpellNo = EpisodeNo) Division

FROM (
					SELECT 
						PCTDescription = PCT.[Organisation]
						,InternalNo = Encounter.SourcePatientNo
						,EpisodeNo = Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard = ISNULL(LOSStrokeWard,0)
						,Denominator = SUM(DATEDIFF(mi,Encounter.EpisodeStartTime,Encounter.EpisodeEndTime))
						,AETime = TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth

					FROM 
						apc.Encounter Encounter

					LEFT JOIN [Organisation].[dbo].[PCT] PCT 
					ON PCT.[OrganisationCode] = Encounter.PCTCode

					--LOS on Stroke Ward
					LEFT JOIN (		SELECT 
										ProviderSpellNo,
										LOSStrokeWard = SUM(DATEDIFF(mi,WardStay.[StartTime],WardStay.EndTime)) 
									FROM 
										[APC].[WardStay] 
									INNER JOIN [APC].[Ward] 
									ON
										WardID = SourceWardID
									WHERE 
										[SourceWardCode] in ('30M','31M','31R') 
									GROUP BY ProviderSpellNo) WardStay 
					ON WardStay.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

					--LOS in AE
					LEFT JOIN (	
									SELECT distinct
										APCEncounter.DistrictNo,
										APCEncounter.AdmissionTime,
										TimeSpentInAE = StageDurationMinutes
									FROM
										APC.Encounter APCEncounter

									INNER JOIN AE.Encounter AEEncounter
									ON AEEncounter.DistrictNo = APCEncounter.DistrictNo
									and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR,-2,APCEncounter.AdmissionTime) and DATEADD(HOUR,+2,APCEncounter.AdmissionTime)

									INNER JOIN AE.Fact Fact 
									ON Fact.MergeEncounterRecno = AEEncounter.MergeEncounterRecno

									--WHERE Fact.StageID = 'INDEPARTMENTADJUSTED'
									WHERE Fact.StageID = 12
									) LOSAE 	
					ON LOSAE.DistrictNo = Encounter.DistrictNo
					AND LOSAE.AdmissionTime = Encounter.AdmissionTime
					
					--only bring back stroke patients
					INNER JOIN (	
								SELECT 
									SourcePatientNo
									,SourceSpellNo
									,DiagnosisIndicator = CASE 
										WHEN [Primary] is not null AND [Secondary] is not null then 'Primary'
										WHEN [Primary] is not null then 'Primary'
										WHEN [Secondary] is not null then 'Secondary'
									ELSE
										NULL
									END
								FROM (
										--pivot data so we can compare diagnosis columns as a primary and secondary can be the same
										SELECT
											SourcePatientNo
											,SourceSpellNo
											,[Primary]
											,[Secondary]
										FROM
												(SELECT 
													SourcePatientNo,SourceSpellNo ,DiagnosisIndicator = 'Primary'
												FROM 
													apc.Encounter Encounter
												WHERE 
													(PrimaryDiagnosisCode like 'I61%' or PrimaryDiagnosisCode like 'I63%' or PrimaryDiagnosisCode like 'I64%')

												UNION	
										
												SELECT 
													Encounter.SourcePatientNo,Encounter.SourceSpellNo,DiagnosisIndicator = 'Secondary'
												FROM
													apc.Encounter Encounter
												INNER JOIN APC.Diagnosis Diagnosis 
																ON 
																--Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
																--AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
																--AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
																Diagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
																AND (Diagnosis.DiagnosisCode like 'I61%'  or Diagnosis.DiagnosisCode like 'I63%' or Diagnosis.DiagnosisCode like 'I64%')										
												)s
										PIVOT
										(
										max(DiagnosisIndicator)
										FOR DiagnosisIndicator IN ([Primary],[Secondary])
										) as pvt
								)qry
					) StrokePatients
					ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
					AND StrokePatients.SourceSpellNo = Encounter.SourceSpellNo
					
					INNER JOIN WH.Calendar Calendar
					ON	Calendar.DateID = Encounter.DischargeDateID

					WHERE
						Encounter.DischargeTime >= @StartDate AND Encounter.DischargeTime <= @EndDate
					--and Encounter.sourcepatientno = '3257952'
					--AgeCategory must equal A
					AND CASE 
							WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionTime))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionTime),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionTime)))))-1900)<19
							THEN 'C'
							ELSE 'A'
						END = 'A'
					--AND Calendar.FinancialMonthKey = @FinancialMonthKey					

					GROUP BY 
						PCT.[Organisation]
						,Encounter.SourcePatientNo
						,Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard
						,TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth

)myqry

inner join apc.encounter AllDiagnosisCodes
on AllDiagnosisCodes.SourcePatientNo = myqry.InternalNo
AND AllDiagnosisCodes.SourceSpellNo = myqry.EpisodeNo

UNION

SELECT
	myqry.PCTDescription
	,myqry.InternalNo
	,myqry.EpisodeNo
	,myqry.DiagnosisIndicator
	,OtherDiagnosisIndicator = 'Yes'
	,myqry.CasenoteNumber
	,myqry.PatientForename
	,myqry.PatientSurname
	,myqry.AdmissionTime
	,myqry.DischargeTime
	,Numerator = myqry.LOSStrokeWard
	,DenominatorIncAE = myqry.Denominator + ISNULL(myqry.AETime,0)
	,PercentageOnStrokeWardIncAE = ((myqry.LOSStrokeWard*1.00)/(myqry.Denominator + ISNULL(myqry.AETime,0))*1.00)*100
	,AllDiagnosisCodes.EpisodeStartTime
	,AllDiagnosisCodes.[PrimaryDiagnosisCode]
	,AllDiagnosisCodes.[SubsidiaryDiagnosisCode]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode1]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode2]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode3]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode4]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode5]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode6]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode7]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode8]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode9]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode10]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode11]
	,AllDiagnosisCodes.[SecondaryDiagnosisCode12]
	,myqry.TheMonth
	,(SELECT DISTINCT Division 
				FROM APC.Encounter E2
				INNER JOIN WH.Directorate D2
					ON E2.StartDirectorateCode = D2.DirectorateCode
			  WHERE FirstEpisodeInSpellIndicator = 1 AND ProviderSpellNo = EpisodeNo) Division
		  
FROM (
					SELECT 
						PCTDescription = PCT.[Organisation]
						,InternalNo = Encounter.SourcePatientNo
						,EpisodeNo = Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard = ISNULL(LOSStrokeWard,0)
						,Denominator = SUM(DATEDIFF(mi,Encounter.EpisodeStartTime,Encounter.EpisodeEndTime))
						,AETime = TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth

					FROM 
						apc.Encounter Encounter

					LEFT JOIN [Organisation].[dbo].[PCT] PCT 
					ON PCT.[OrganisationCode] = Encounter.PCTCode

					--LOS on Stroke Ward
					LEFT JOIN (		SELECT 
										ProviderSpellNo,
										LOSStrokeWard = SUM(DATEDIFF(mi,WardStay.[StartTime],WardStay.EndTime)) 
									FROM 
										[APC].[WardStay] 
									INNER JOIN 
										[APC].[Ward] ON WardID = SourceWardID
									WHERE 
										[SourceWardCode] in ('30M','31M','31R') 
									GROUP BY ProviderSpellNo) WardStay 
					ON WardStay.[ProviderSpellNo] = Encounter.[ProviderSpellNo]

					--LOS in AE
					LEFT JOIN (	
									SELECT distinct
										APCEncounter.DistrictNo,
										APCEncounter.AdmissionTime,
										TimeSpentInAE = StageDurationMinutes
									FROM
										APC.Encounter APCEncounter

									INNER JOIN AE.Encounter AEEncounter
									ON AEEncounter.DistrictNo = APCEncounter.DistrictNo
									and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR,-2,APCEncounter.AdmissionTime) and DATEADD(HOUR,+2,APCEncounter.AdmissionTime)

									INNER JOIN AE.Fact Fact 
									ON Fact.MergeEncounterRecno = AEEncounter.MergeEncounterRecno

									WHERE Fact.StageID = 12 --'INDEPARTMENTADJUSTED'
									) LOSAE 	
					ON LOSAE.DistrictNo = Encounter.DistrictNo
					AND LOSAE.AdmissionTime = Encounter.AdmissionTime
					
					--only bring back stroke patients
					INNER JOIN (	
										SELECT
											SourcePatientNo
											,SourceSpellNo
											,DiagnosisIndicator
										FROM
												(SELECT 
													SourcePatientNo,SourceSpellNo ,DiagnosisIndicator = 'No'
												FROM 
													apc.Encounter Encounter
												WHERE 
													(PrimaryDiagnosisCode like 'I62%' or PrimaryDiagnosisCode like 'H34%')

												UNION	
										
												SELECT 
													Encounter.SourcePatientNo,Encounter.SourceSpellNo,DiagnosisIndicator = 'No'
												FROM
													apc.Encounter Encounter
												INNER JOIN APC.Diagnosis Diagnosis 
																ON 
																--Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
																--AND Diagnosis.SourceSpellNo = Encounter.SourceSpellNo
																--AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
																Diagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
																AND (Diagnosis.DiagnosisCode like 'I62%'  or Diagnosis.DiagnosisCode like 'H34%')										
										)s

					) StrokePatients
					ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
					AND StrokePatients.SourceSpellNo = Encounter.SourceSpellNo
						
					INNER JOIN WarehouseReportingMerged.WH.Calendar Calendar
					ON	Calendar.DateID = Encounter.DischargeDateID

					WHERE
						Encounter.DischargeTime >= @StartDate AND Encounter.DischargeTime <= @EndDate
												
					--and Encounter.sourcepatientno = '3257952'
					--AgeCategory must equal A
					AND CASE 
							WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionTime))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionTime),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionTime)))))-1900)<19
							THEN 'C'
							ELSE 'A'
						END = 'A'
										

					GROUP BY 
						PCT.[Organisation]
						,Encounter.SourcePatientNo
						,Encounter.SourceSpellNo
						,CasenoteNumber
						,Encounter.PatientForename
						,Encounter.PatientSurname
						,Encounter.AdmissionTime
						,Encounter.DischargeTime
						,LOSStrokeWard
						,TimeSpentInAE
						,StrokePatients.DiagnosisIndicator
						,Calendar.TheMonth
)myqry

inner join apc.encounter AllDiagnosisCodes
on AllDiagnosisCodes.SourcePatientNo = myqry.InternalNo
AND AllDiagnosisCodes.SourceSpellNo = myqry.EpisodeNo


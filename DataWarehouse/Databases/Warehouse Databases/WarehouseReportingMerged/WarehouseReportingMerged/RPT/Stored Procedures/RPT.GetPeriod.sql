﻿



CREATE PROCEDURE [RPT].[GetPeriod] -- [RPT].[GetPeriod] 'A'
(
	  @TimeSeries varchar(1)
	 ,@ReturnSingleIndexRow INT = NULL

)

AS 

/* 
==============================================================================================
Description:	For SSRS cascading parameters. 

When		Who			What
?			?			Initial coding
13/02/2015	Paul Egan	Added Academic Year for CHAMP reports.
===============================================================================================
*/

DECLARE @SQL AS NVARCHAR(MAX)

--DECLARE @startdate smalldatetime = '1 Apr 2012'
--,@enddate smalldatetime= '1 May 2012', @specialtycode varchar(max) = '300'
--,@division varchar(max) = 'Acute Medical', @patientcategory varchar(max) = 'NE'

BEGIN

CREATE TABLE #tbl (Period varchar(100), SortOrder varchar(100))


SET @SQL = 
CASE @TimeSeries

WHEN 'W' THEN 

  N'SELECT DISTINCT WeekNo Period, WeekNoKey SortOrder
  FROM WarehouseReportingMerged.WH.Calendar
  WHERE TheDate <= GETDATE() AND TheDate >= ''01 Jan 2006'''

WHEN 'M' THEN

  N'SELECT DISTINCT TheMonth Period, FinancialMonthKey SortOrder
  FROM WarehouseReportingMerged.WH.Calendar
  WHERE TheDate <= GETDATE() AND TheDate >= ''01 Jan 2006'''
  
WHEN 'Q' THEN

  N'SELECT DISTINCT FinancialQuarter Period, FinancialQuarterKey SortOrder
  FROM WarehouseReportingMerged.WH.Calendar
  WHERE TheDate <= GETDATE()'

WHEN 'Y' THEN

  N'SELECT DISTINCT FinancialYear Period, FinancialYear SortOrder
  FROM WarehouseReportingMerged.WH.Calendar
  WHERE TheDate <= GETDATE() AND FinancialYear <> ''Previous Year'''
  
when 'A' then

  N'SELECT DISTINCT AcademicYear Period, AcademicYear SortOrder
  FROM WarehouseReportingMerged.WH.Calendar
  WHERE TheDate <= GETDATE() 
		AND AcademicYear <> ''Previous Year'' 
		AND AcademicYear <> ''1899/1900'''

END

SET @SQL = N'INSERT INTO #tbl ' + @SQL
EXEC SP_EXECUTESQL @SQL


	SELECT 
		 Period
		,SortOrder
	FROM
	(
		SELECT 
			 Period
			,SortOrder
			,RowNo = ROW_NUMBER() OVER(ORDER BY SortOrder DESC) 
		FROM #tbl
	) D
	WHERE  
		RowNo = COALESCE(@ReturnSingleIndexRow, RowNo)
		
	ORDER BY
		SortOrder DESC

	DROP TABLE #tbl
END







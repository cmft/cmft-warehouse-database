﻿




-- [RPT].[GetMortalityPatients] @GlobalProviderSpellNumber='A178329'

create  proc [RPT].[MortalityPatients]
(
@user varchar(50) = null
,@GlobalProviderSpellNumber varchar(50) = null -- Changed variable name from @ProviderSpellNumber for clarity. Paul Egan 24/06/2014
				
)


as 

/****************************************************************************************
	Stored procedure : [RPT].[MortalityPatients]
	Description		 : SSRS proc for the Visual Studio report
	
	Reports >> Mortality >> Mortality Patient Detail.rdl
	
	Measures
	 Drill through list of episodes and diagnoses (1 row per diagnosis) for a particular spell.

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
	19/06/2014	Paul Egan       Copied from LIVE to DEV (did not exist on dev).
	24/06/2014	Paul Egan		Parameters not needed removed: Specialty, Division.
								Joined to APC.Diagnostic instead of using function for efficiency.
								Renamed @ProviderSpellNumber to Global for clarity.
								Added GlobalEpisodeNo.
	25/06/2014	DG/PE			Re-factored for WarehouseReportingMerged - removed derived tables
								for spell and episode, to simplify the proc.
								Re-named proc from GetMortalityPatients to MortalityPatients.
*****************************************************************************************/

/* ================ DEBUG ONLY ================= */
--declare @user varchar(50) = null
--declare @GlobalProviderSpellNumber varchar(50) = '2016803/12'
/* =============================================*/


select 
	SourcePatientNo = 
					case
						when exists 
									(
									select
										1
									from 
										WarehouseReporting.RPT.SecurityMap sm
									where [DomainLogin] = @user
									and EndDirectorateCode = sm.DirectorateCode
									)
						then SourcePatientNo
						else 'Unauthorised'
					end
	,CasenoteNumber = 
					case
						when exists 
									(
									select
										1
									from 
										WarehouseReporting.RPT.SecurityMap sm
									where [DomainLogin] = @user
									and EndDirectorateCode = sm.DirectorateCode
									)
						then CasenoteNumber
						else 'Unauthorised'
					end
	,DistrictNo =
					case
						when exists 
									(
									select
										1
									from 
										WarehouseReporting.RPT.SecurityMap sm
									where [DomainLogin] = @user
									and EndDirectorateCode = sm.DirectorateCode
									)
						then DistrictNo
						else 'Unauthorised'
					end
	,DateOfBirth = 
					case
						when exists 
									(
									select
										1
									from 
										WarehouseReporting.RPT.SecurityMap sm
									where [DomainLogin] = @user
									and EndDirectorateCode = sm.DirectorateCode
									)
						then DateOfBirth
						else '01/01/1900'
					end
	,PatientName = 
					case
						when exists 
									(
									select
										1
									from 
										WarehouseReporting.RPT.SecurityMap sm
									where [DomainLogin] = @user
									and EndDirectorateCode = sm.DirectorateCode
									)
						then PatientSurname + ', ' + PatientForename
						else 'Unauthorised'
					end
	,DateOfDeath = coalesce(DateOfDeath, DischargeDate)
	,GlobalProviderSpellNo
	,GlobalEpisodeNo
	,Division		
	,Specialty = Specialty.NationalSpecialtyLabel
	,AdmissionMethod = AdmissionMethod.NationalAdmissionMethod
	,AdmissionDate
	,DischargeDate	
	,Consultant = Consultant.SourceConsultant
	,EpisodeStartTime
	,EpisodeEndTime
	,StartWardTypeCode
	,EndWardTypeCode
	,CodingCompleteDate
	,PrimaryOperationCode = PrimaryProcedureCode
	,PrimaryOperationDate = PrimaryProcedureDate
	,DiagnosisType = Diagnosis.DiagnosisCode
	,Diagnosis = DiagnosisBase.Diagnosis
from
	APC.Encounter

	inner join APC.AdmissionMethod
	on	Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID

	inner join APC.DischargeMethod
	on	Encounter.DischargeMethodID = DischargeMethod.SourceDischargeMethodID
				
	left join WH.Specialty
	on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID

	inner join WH.Directorate
	on	Directorate.DirectorateCode = Encounter.EndDirectorateCode

	inner join WH.Consultant
	on	Consultant.SourceConsultantID = Encounter.ConsultantID

	left join APC.Diagnosis	
	on	Diagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno

	left join WH.Diagnosis DiagnosisBase	
	on	DiagnosisBase.DiagnosisCode = Diagnosis.DiagnosisCode

where 
	DischargeMethod.NationalDischargeMethodCode IN ('4','5')
	and Encounter.GlobalProviderSpellNo = @GlobalProviderSpellNumber










﻿

CREATE proc [RPT].[PortalComorbidity]

(
@User varchar(50)
)

as

select 
	ClinicalCodingCompleteDate
	,Cases = 1		
from
	[APC].[Diagnosis] APCDiagnosis

inner join APC.Encounter
on APCDiagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
					 					
inner join WH.Consultant
on	Consultant.SourceConsultantID = Encounter.ConsultantID	

inner join WH.Diagnosis WHDiagnosis
on	WHDiagnosis.DiagnosisCode = APCDiagnosis.DiagnosisCode

inner join	GeckoASSecurity.dbo.WindowsUserCodeMap
on	WindowsUserCodeMap.SourceValueCode = Consultant.SourceConsultantCode 
and	SourceContextCode = WindowsUserCodeMap.ContextCode 

inner join GeckoASSecurity.dbo.WindowsUserSecurityGroupMap
on	WindowsUserSecurityGroupMap.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode

inner join GeckoASSecurity.dbo.[SecurityGroupEntityTypeMap]
on	[SecurityGroupEntityTypeMap].SecurityGroupCode = WindowsUserSecurityGroupMap.SecurityGroupCode
and [EntityTypeCode] = 'CONSULTANT'
												
where
	Encounter.ClinicalCodingCompleteDate between dateadd(year, -1,  getdate()) and getdate()
and
	WindowsUserSecurityGroupMap.Userid = @User
and
	WHDiagnosis.IsCharlson = 1


﻿

CREATE PROCEDURE [RPT].[GetEddTimeliness]
	 @startdate AS Date
	,@enddate AS Date
	,@specialtycode varchar(max)
	,@Division varchar(max)
	,@patientcategory varchar(max)
AS

BEGIN
	SET NOCOUNT ON;

SELECT 
	 Ward = Ward.SourceWard
	,Consultant =  isnull(consultant.Title,'') + ' ' +isnull(consultant.Initials,'') + ' ' + isnull(consultant.Surname,'')

	,EnteredWithin48hrs =
		CASE 
		WHEN DATEDIFF(MINUTE , ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) < 1440 THEN 1
		ELSE 0
		END

	,Admissions = 1
	,Division = ISNULL(Directorate.Division , 'Not Known')

	,HourTimeBand =
		CASE 
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) < 1440 THEN '<24'
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) BETWEEN 1440 AND 2880 THEN '24-48'
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) BETWEEN 2880 AND 4320 THEN '48-72'
		WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime,ExpectedLOS.EddCreatedTime) BETWEEN 4320 AND 5760 THEN '72-96'
		WHEN ExpectedLOS.ExpectedLOS IS NULL THEN 'Not Recorded'
		ELSE '96+'
		END
	
	,Specialty = Specialty.NationalSpecialty
	,PathwayCondition = ISNULL(ExpectedLOS.RTTPathwayCondition , 'Unknown')
	
FROM         
	APC.Encounter  ExpectedLOS

INNER JOIN WH.Directorate Directorate
ON	ExpectedLOS.StartDirectorateCode = Directorate.DirectorateCode
AND Directorate.Division IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@division,','))

INNER JOIN WH.Specialty Specialty
ON ExpectedLOS.SpecialtyID =  Specialty.SourceSpecialtyID
AND Specialty.NationalSpecialtyCode IN(SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@specialtycode,','))

INNER JOIN APC.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodID = ExpectedLOS.AdmissionMethodID

INNER JOIN APC.IntendedManagement ManagementIntention
	on	ManagementIntention.SourceIntendedManagementID = ExpectedLOS.IntendedManagementID
	
LEFT OUTER JOIN WH.Consultant Consultant
	ON ExpectedLOS.ConsultantID = Consultant.SourceConsultantID

LEFT OUTER JOIN APC.Ward Ward
	ON ExpectedLOS.StartWardID = Ward.SourceWardID

WHERE     
	CONVERT(date, ExpectedLOS.AdmissionTime, 103) >= @startdate --'14/Aug/2011'
AND CONVERT(date, ExpectedLOS.AdmissionTime, 103) <= @enddate --'14/Aug/2011'
and ExpectedLOS.EpisodeStartTime = ExpectedLOS.admissionTime
AND ManagementIntention.SourceIntendedManagementCode NOT IN ('R','N') 
AND NOT
	(
		Ward.SourceWardCode = 'ESTU'
	AND DATEDIFF(HOUR, ExpectedLOS.AdmissionTime, ExpectedLOS.DischargeTime) < 6
	)
AND Specialty.NationalSpecialtyCode <> '501'
AND Ward.SourceWard <> '76A'
AND Specialty.SourceSpecialty NOT like '%DIALYSIS%'
AND Specialty.SourceSpecialtycode NOT like 'IH%'
AND Ward.SourceWard NOT like 'SUB%'
AND case AdmissionTypeCode
				when 'EL' THEN 'EL'
				else 'NE' end IN (SELECT VALUE FROM warehousereporting.RPT.SSRS_MultiValueParamSplit(@patientcategory,','))
END







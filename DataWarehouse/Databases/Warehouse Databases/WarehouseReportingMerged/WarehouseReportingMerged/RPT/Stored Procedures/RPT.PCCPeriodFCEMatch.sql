﻿




CREATE proc [RPT].[PCCPeriodFCEMatch]
(
 @PICUDischargeDateStart datetime2(0)
,@PICUDischargeDateEnd datetime2(0)
)
as

/******************************************************************************************************
Stored procedure :	[RPT].[PICUPeriodAPCEpisodeStartTimeMatch]
Description		 :	For data quality SSRS report to match paediatric critical care (PCC) periods to APC FCEs.

Modification History
====================

Date		Person			Description
=======================================================================================================
12/12/2014	Paul Egan       Initial coding.
*******************************************************************************************************/

/*============== Debug only ========================*/
--declare
-- @PICUDischargeDateStart datetime2(0) = '20140101'
--,@PICUDischargeDateEnd datetime2(0) = cast(getdate() - 1 as date);
/*==================================================*/

declare @RAGAmberThresholdLower int	= 5;	-- Minutes
declare @RAGAmberThresholdUpper int	= 60;	-- Minutes

with rawDataCTE as
(
select
	PICUEventID = PCCPeriod.PeriodID
	,PCCPeriod.CasenoteNo
	,APCEncounter.DistrictNo
	,PCCPeriod.NHSnumber
	,PICUAdmissionTime = PCCPeriod.admissionTime
	,ClosestEpisodeMatch = 
		/* Check which episode is closer - the end times or the start times, and use the closest one to match */
		row_number() over(partition by PCCPeriod.PeriodID 
			order by 
				case
					when abs(coalesce(datediff(minute, PCCPeriod.admissionTime, APCEncounter.EpisodeStartTime), 99999)) <= abs(coalesce(datediff(minute, PCCPeriod.DischargeTime, APCEncounter.EpisodeEndTime), 99999))
					then abs(coalesce(datediff(minute, PCCPeriod.admissionTime, APCEncounter.EpisodeStartTime), 99999))
					else abs(coalesce(datediff(minute, PCCPeriod.DischargeTime, APCEncounter.EpisodeEndTime), 99999))
				end)
	,APCEpisodeStartTime = APCEncounter.EpisodeStartTime
	,TimeStartDifferenceMinute = coalesce(datediff(minute, PCCPeriod.admissionTime, APCEncounter.EpisodeStartTime), 99999)
	,RAGRatingStart = 
		case
			when abs(coalesce(datediff(minute, PCCPeriod.admissionTime, APCEncounter.EpisodeStartTime), 99999)) >= @RAGAmberThresholdUpper then 1	-- Red
			when abs(coalesce(datediff(minute, PCCPeriod.admissionTime, APCEncounter.EpisodeStartTime), 99999)) >= @RAGAmberThresholdLower then 2	-- Amber
			else 3 -- Green
		end
	,PICUDischargeTime = PCCPeriod.DischargeTime
	,APCEpisodeEndTime = APCEncounter.EpisodeEndTime
	,TimeEndDifferenceMinute = coalesce(datediff(minute, PCCPeriod.DischargeTime, APCEncounter.EpisodeEndTime), 99999)
	,RAGRatingEnd = 
		case
			when abs(coalesce(datediff(minute, PCCPeriod.DischargeTime, APCEncounter.EpisodeEndTime), 99999)) >= @RAGAmberThresholdUpper then 1	-- Red
			when abs(coalesce(datediff(minute, PCCPeriod.DischargeTime, APCEncounter.EpisodeEndTime), 99999)) >= @RAGAmberThresholdLower then 2	-- Amber
			else 3 -- Green
		end

	,APCEncounter.StartWardTypeCode
	,APCEncounter.EndWardTypeCode
	,RAGAmberThresholdLower = @RAGAmberThresholdLower
	,RAGAmberThresholdUpper = @RAGAmberThresholdUpper
	
	/* Check any overlapping episode (may be more than 1 for each period) */
	,CDSMatch =		-- As in proc [WarehouseReportingMerged].[CDS62].[BuildAngliaType010]
		case
			when APCEncounter.MergeEncounterRecno is not null then 1
			else 0
		end
	,FCEOverlapCount = 
		case
		when APCEncounter.MergeEncounterRecno is null then 0
		else count(*) over(partition by PCCPeriod.PeriodID)
		end
	,APCEncounter.GlobalProviderSpellNo
	,FCEConsultantCode = APCEncounter.ConsultantCode
	,FCEConsultant = Consultant.Surname + ' ' + Consultant.Initials + ' ' 
						+ Consultant.Title + ' (' + Consultant.NationalConsultantCode + ')'
	--,FCESpecialtyCode = APCEncounter.SpecialtyCode
	--,FCESpecialty = Specialty.NationalSpecialty
from 
	Warehouse.PCC.Period PCCPeriod
  
	/* Must use left join to also bring back non-matches */
	left join WarehouseReportingMerged.APC.Encounter APCEncounter
	on	PCCPeriod.CasenoteNo = APCEncounter.CasenoteNumber
	/* CDS Match - as in proc [WarehouseReportingMerged].[CDS62].[BuildAngliaType010] */
	and APCEncounter.EpisodeStartTime < PCCPeriod.DischargeTime
	and	APCEncounter.EpisodeEndTime > PCCPeriod.AdmissionTime	
	
	left join WarehouseReportingMerged.WH.Consultant
	on Consultant.SourceConsultantID = APCEncounter.ConsultantID
	
	left join WarehouseReportingMerged.WH.Specialty
	on Specialty.SourceSpecialtyID = APCEncounter.SpecialtyID

 where
	(PCCPeriod.DischargeTime between @PICUDischargeDateStart and @PICUDischargeDateEnd)
	or
	(PCCPeriod.DischargeTime is null)	-- Always return current PCC admissions for reference


 -- order by 
	--e.CasenoteNumber
	--,e.EpisodeStartTime
	--,ClosestEpisodeMatch
)


select
	 PICUEventID
	,CasenoteNo
	,DistrictNo
	,NHSnumber
	,PICUAdmissionTime
	,ClosestEpisodeMatch
	,APCEpisodeStartTime
	,TimeStartDifferenceMinute
	,RAGRatingStart
	,PICUDischargeTime
	,APCEpisodeEndTime
	,TimeEndDifferenceMinute
	,RAGRatingEnd
	,StartWardTypeCode
	,EndWardTypeCode
	,RAGAmberThresholdLower
	,RAGAmberThresholdUpper
	,CDSMatch
	,FCEOverlapCount
	,GlobalProviderSpellNo
	,FCEConsultantCode
	,FCEConsultant
	--,FCESpecialtyCode
	--,FCESpecialty
from
	rawDataCTE
--where
	--(EpisodeMatch = 1) --or EpisodeMatch = 0)
	--ClosestEpisodeMatch = 1
	--CasenoteNo = 'm14/000885'
	--MergeEncounterRecno is null

/*============== Debug only ========================*/
--order by 
--	--abs(TimeStartDifferenceMinute) desc
--	CDSMatch
--	,PICUEventID
--	,CasenoteNo
--	,ClosestEpisodeMatch
/*==================================================*/








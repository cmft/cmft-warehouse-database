﻿
CREATE proc RPT.TraffordOPWLWithMRScan as

/* 
==============================================================================================
Description:

TFS		When		Who			What
		20160412	Paul Egan	Initial Coding
===============================================================================================
*/	

select
	WaitingListCensusDate = cast(OPWLEncounter.CensusDate as date)
	,OPWLEncounter.NHSNumber
	,OPWLEncounter.DistrictNo
	,OPWLEncounter.PatientSurname
	,ClinicAppointmentDate = cast(OPWLEncounter.AppointmentDate as date)
	,ClinicSite = left(Site.NationalSite, charindex(' ', Site.NationalSite) - 1)
	,ClinicConsultant = Consultant.NationalConsultant
	,OPWLEncounter.AppointmentNote
from
	OPWL.Encounter OPWLEncounter
	
	left join WH.Site
	on Site.SourceSiteID = OPWLEncounter.SiteID
	
	left join WH.Consultant
	on OPWLEncounter.ConsultantID = Consultant.SourceConsultantID

where
	OPWLEncounter.CensusDate = (select max(CensusDate) from OPWL.Encounter)
	and OPWLEncounter.AppointmentNote like '%MR SCAN%'
	and OPWLEncounter.CancelledBy is null
;


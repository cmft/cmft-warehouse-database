﻿

CREATE procedure [RPT].[AcuteMedDailyLOSGreater14days]

as
	
select		
	 Ward = SourceWardCode	
	,LoS = datediff(d,TheDate,getdate())	
	,Admission = AdmissionTime	
	,[Patient Forename] = PatientForename	
	,[Patient Surname] = PatientSurname	
	,Age = AgeCode	
	--,ReportDate = dateadd(dd, datediff(dd, 0,getdate()), 0)	
	,convert(varchar(11), GETDATE(), 106) AS 'ReportDate'	
		,DischargeDateID
		,EpisodeEndDateID
		,Division
from		
	APC.Encounter	
		
	left join WH.Calendar	
	on AdmissionDateID = DateID	
		
	left join WH.Directorate	
	on StartDirectorateCode = DirectorateCode
		
	left join APC.WardStay	
	on WardStay.ProviderSpellNo = Encounter.ProviderSpellNo	
	and EndDate is null	
		
	left join APC.Ward	
	on WardID = SourceWardID	
		
	left join WH.Specialty	
	on SpecialtyID = SourceSpecialtyID	
		
		
where		
		DischargeDateID =1
	and EpisodeEndDateID =1
	and 
	datediff(d,TheDate,dateadd(dd, datediff(dd, 0,getdate()), 0)) >14	
	and Division ='Medicine & Community'	
	and SourceWardcode in ('AM1','AM2','W45','W46')	
order by		
	 SourceWardCode	
	,LoS desc	


﻿


CREATE PROCEDURE [RPT].[FutureTCIDatesHistoric]
(
	 @StartBaseDate			DATE			= NULL
	,@Division				VARCHAR(50)		= NULL
	,@DirectorateCode		VARCHAR(20)		= NULL
	,@NationalSpecialtyCode	VARCHAR(1000)	= NULL
)
AS

	DECLARE @StartDate			DATE
	DECLARE @EndDate			DATE
		
	-- Start and End date derivations		
	SET @StartBaseDate = COALESCE(@StartBaseDate, DATEADD(WEEK, -8, GETDATE()))
	
	SET DATEFIRST  1	-- Monday

	SET @StartDate  = DATEADD(DAY, 1 - DATEPART(DW, @StartBaseDate), @StartBaseDate)
	SET @EndDate	= DATEADD(DAY, -DATEPART(DW, GETDATE()), GETDATE())			-- Previous week end date based on current date
	

	DECLARE @FCal TABLE 
	(
		 FinancialYearStartWkNo		INT
		,FinancialYearStart			CHAR(4)
		,WeekAdjustment				INT
		,FinancialYear				VARCHAR(15)
	)
	
	-- Create an in-line table of the Financial years calendar week number, this used to determine a financial year week number
	INSERT INTO @FCal
	(
		 FinancialYearStartWkNo
		,FinancialYearStart
		,WeekAdjustment
		,FinancialYear
	)	
	SELECT
		 FinancialYearStartWkNo		= YEAR(C.TheDate) * 52 + DATEPART(WK, C.TheDate)
		,FinancialYearStart			= LEFT(C.FinancialYear,4)
		 -- Adjustment to the derived week number, if the first week is a full week then start at Wk 1, if not start at Wk 0
		,WeekAdjustment				= CASE WHEN C.TheDate = C.FirstDayOfWeek THEN 1 ELSE 0 END
		,FinancialYear				= C.FinancialYear
	FROM
		(
			-- Get the financial year start date
			SELECT 
				 FinancialYearStartID	= MIN(CAL.DateID)
				,CAL.FinancialYear
			FROM 
				(
					SELECT DISTINCT
						FinancialYear
					FROM
						WH.Calendar			
					WHERE 
						TheDate BETWEEN @StartDate AND @EndDate
				) FY				-- Financial Years in the date selection period
				INNER JOIN WH.Calendar CAL
					ON FY.FinancialYear = CAL.FinancialYear
			GROUP BY
				CAL.FinancialYear
				
		) BD
		INNER JOIN WH.Calendar C
			ON BD.FinancialYearStartID = C.DateID

	-- Main data query
	SELECT
		 E.AdmissionDateID
		,NoticePeriod			= DATEDIFF(DAY, PRE.PreAdmitDate1, E.AdmissionDate) / 7 
		,E.StartDirectorateCode
		,E.SpecialtyId
		,E.ContextCode
		,PRE.PreAdmitDate1
		,E.AdmissionDate
	INTO #DATA	
	FROM				
		APC.Encounter E
		INNER JOIN  [Infocom_PAS].[dbo].[smsmir_preadmission] PRE 
			ON		E.SourcePatientNo		= PRE.InternalNo
				AND E.SourceSpellNo			= PRE.EpisodeNo
				AND PRE.PreAdmCancRsn		IS NULL		
	WHERE
			E.AdmissionDate					BETWEEN @StartDate AND @EndDate	
		AND E.FirstEpisodeInSpellIndicator	= 1					

	-- Join data to lookup tables
	SELECT
		 ApptWeekEnding			= APPTCAL.LastDayOfWeek
		,ApptWeekNo				= 'WK' + RIGHT('00' + CAST(YEAR(APPTCAL.TheDate) * 52 + DATEPART(WK, APPTCAL.TheDate) - FCAL.FinancialYearStartWkNo + FCAL.WeekAdjustment AS VARCHAR(2)),2)
		,ApptFYear				= FCAL.FinancialYearStart
		,Specialty				= SPEC.NationalSpecialty
		,NoticePeriod			= #DATA.NoticePeriod
		,Cases					= 1
		,Appt					= YEAR(APPTCAL.TheDate) * 52 + DATEPART(WK, APPTCAL.TheDate)
		,FY						= FCAL.FinancialYearStartWkNo
		
	FROM
		#DATA
	
		INNER JOIN WH.Calendar APPTCAL
			ON #DATA.AdmissionDateID = APPTCAL.DateID
			
		INNER JOIN @FCal FCAL
			ON APPTCAL.FinancialYear = FCAL.FinancialYear
			
		INNER JOIN WH.Directorate DIR
			ON		#DATA.StartDirectorateCode	= DIR.DirectorateCode
			
		INNER JOIN WH.Specialty SPEC
			ON		#DATA.SpecialtyId			= SPEC.SourceSpecialtyId
				AND #DATA.ContextCode			= SPEC.SourceContextCode

	WHERE
			SPEC.NationalSpecialtyCode		IN (SELECT Item from fn_ListToTable(@NationalSpecialtyCode,','))
		AND DIR.Division					= COALESCE(@Division, DIR.Division)
		AND DIR.DirectorateCode				= COALESCE(@DirectorateCode, DIR.DirectorateCode)



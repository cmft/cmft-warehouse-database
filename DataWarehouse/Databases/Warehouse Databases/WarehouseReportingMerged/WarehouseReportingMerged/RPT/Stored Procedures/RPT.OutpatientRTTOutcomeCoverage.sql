﻿/****************************************************************************************
	Stored procedure : RPT.OutpatientRTTOutcomeCoverage
	Description		 : SSRS proc for the Visual Studio report
	
		SSRS Merged >> OP >> OutpatientRTTOutcomeCoverage.rdl
	
	Measures
	 

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	13.04.2016	M Hodson		Intial Coding	
*****************************************************************************************/

CREATE PROCEDURE RPT.OutpatientRTTOutcomeCoverage
	@PeriodEnd			VARCHAR(50)	= NULL,
	@PreviousPeriods	INT			= 3,
	@PeriodType			VARCHAR(5)	= 'W'
AS

	DECLARE @StartDate			DATE
	DECLARE @EndDate			DATE
	DECLARE @PeriodEndDate		DATE
	DECLARE @PeriodStartDate	DATE
	DECLARE @Today				DATE = GETDATE()
	DECLARE @StartDateID		INT
	DECLARE @EndDateID			INT

	--------------------------------------------------------------------------------------------------------------
	-- Determine Start and End Dates		
	--------------------------------------------------------------------------------------------------------------

	-- Get start and end period based on the @EndDate
	SELECT  
		 @EndDate			= MAX(TheDate)
		,@PeriodStartDate	= MAX(CASE @PeriodType
								WHEN 'W'	THEN FirstDayOfWeek
								WHEN 'M'	THEN MONTHSTART.StartDate
								WHEN 'Q'	THEN MONTHSTART.StartDate
							  END)
	FROM  
		WH.Calendar CAL
		CROSS APPLY
		(
			SELECT 
				 StartDate			= CAST('01 ' + CAL.TheMonth AS DATE)
		) MONTHSTART

	WHERE 
		TheDate <= @Today
		AND CASE @PeriodType
			WHEN 'W'	THEN FinancialWeekNoKey
			WHEN 'M'	THEN FinancialMonthKey
			WHEN 'Q'	THEN FinancialQuarterKey
		END = @PeriodEnd

	-- The StartDate is derived from the @PeriodStartDate after the number of reporing periods is included
	SET @StartDate = CASE @PeriodType
						WHEN 'W'	THEN DATEADD(DAY, 0, DATEADD(WEEK,  -(@PreviousPeriods - 1), @PeriodStartDate))
						WHEN 'M'	THEN DATEADD(DAY, 0, DATEADD(MONTH, -(@PreviousPeriods - 1), @PeriodStartDate))
						WHEN 'Q'	THEN DATEADD(DAY, 0, DATEADD(MONTH, -((@PreviousPeriods * 3) - 1), @PeriodStartDate))
					 END

	-- Use the Calendar table Primary Key DateID for the selection period
	SELECT @StartDateID = DateID FROM WH.Calendar WHERE TheDate = @StartDate	
	SELECT @EndDateID   = DateID FROM WH.Calendar WHERE TheDate = @EndDate	
	
	--------------------------------------------------------------------------------------------------------------
	-- Main Query		
	--------------------------------------------------------------------------------------------------------------
	SELECT 		
			 OPAppointmentDate			= OP.AppointmentDate
			,OPAppointmentStatusCode	= OP.AppointmentStatusCode
			,OPAppointmentTime			= OP.AppointmentTime
			,OPAttendanceOutcomeCode	= OP.AttendanceOutcomeCode
			,OPCancelledByCode			= OP.CancelledByCode
			,OPCasenoteNo				= OP.CasenoteNo
			,OPClinicCode				= OP.ClinicCode
			,OPConsultantCode			= OP.ConsultantCode
			,OPDateOfBirth				= OP.DateOfBirth
			,OPDischargeDate			= OP.DischargeDate
			,OPDisposalCode				= OP.DisposalCode
			,OPDistrictNo				= OP.DistrictNo
			,OPDir						= DIROP.Directorate
			,OPDiv						= DIROP.Division
			,OPDNACode					= OP.DNACode
			,OPLocalSpecialty			= OP.SpecialtyCode
			,OPNationalSpecialty		= SPECOP.NationalSpecialtyLabel
			,OPNHSNumber				= OP.NHSNumber
			,OPReferralConsultantCode	= OP.ReferralConsultantCode
			,OPReferralSpecialtyCode	= OP.ReferralSpecialtyCode
			,OPSiteCode					= OP.SiteCode
			,OPSourceEncounter			= OP.SourceEncounterNo
			,OPSourcePatientNo			= OP.SourcePatientNo
			,OPPatientForename			= OP.PatientForename
			,OPPatientSurname			= OP.PatientSurname 
			,OPReferralDate				= OP.ReferralDate
			,OPRTTPathwayID				= OP.RTTPathwayID

			,CONSSourceConsultant		= CONS.SourceConsultant
		
			,REFDiv						= DIRREF.Division
			,REFNationalSpecialtyspec	= SPECREF.NationalSpecialtyLabel

			,PeriodName					= CASE @PeriodType
											WHEN 'W'	THEN 'Week End ' + CONVERT(VARCHAR(10), CAL.LastDayOfWeek, 103)
											WHEN 'M'	THEN CAL.[MonthName]
											WHEN 'Q'	THEN CAL.FinancialQuarter
										  END

			,PeriodKey					= CASE @PeriodType
											WHEN 'W'	THEN CAL.FinancialWeekNoKey
											WHEN 'M'	THEN CAL.FinancialMonthKey
											WHEN 'Q'	THEN CAL.FinancialQuarterKey
										  END
		
			,RTTCodeNotPresent			= CASE WHEN FILTERS.DisposalCodeIsValid = 0 AND FILTERS.IsAnExclusion = 0 AND FILTERS.IsNoAttendanceOutCome = 0 THEN 1 ELSE 0 END
			,RTTCodePresent				= CASE WHEN FILTERS.DisposalCodeIsValid = 1 AND FILTERS.IsAnExclusion = 0 AND FILTERS.IsNoAttendanceOutCome = 0 THEN 1 ELSE 0 END
			,AppointmentsWithExclusions	= CASE WHEN FILTERS.IsAnExclusion = 0 AND FILTERS.IsNoAttendanceOutCome = 0 THEN 0 ELSE 1 END

			,NoAttendanceStatus			= FILTERS.IsNoAttendanceOutCome
			,AttendedNoDisposal			= FILTERS.IsAttendedNoDisposal
			,Cases						= 1
 					
	 FROM 
		OP.Encounter OP		
  				
		LEFT JOIN RF.Encounter REF ON
				REF.SourcePatientNo		= OP.SourcePatientNo	
			AND REF.SourceEncounterNo	= OP.SourceEncounterNo	
		
		LEFT JOIN WH.Directorate DIRREF	ON
			DIRREF.Directoratecode = REF.DirectorateCode	
				
		LEFT JOIN WH.Directorate DIROP	ON
			DIROP.Directoratecode = OP.DirectorateCode	
				
		INNER JOIN  WH.Calendar CAL ON
			CAL.DateId = OP.AppointmentDateid
	 		
		INNER JOIN WH.Specialty SPECOP ON
			SPECOP.SourceSpecialtyID = OP.SpecialtyID		
	  		
		INNER JOIN WH.Specialty SPECREF	ON	
			SPECREF.SourceSpecialtyID = OP.ReferralSpecialtyID		
	  		
		INNER JOIN WH.Consultant CONS ON	
			CONS.SourceConsultantID = OP.ConsultantID		
 		 		
		INNER JOIN OP.SourceOfReferral SOR	ON	
			SOR.SourceSourceOfReferralID = OP.SourceOfReferralID
	
	--------------------------------------------------------------------------------------------------------------
	-- CROSS APPLY - Used here to prevent sub-queries and use of multiple identical case statements in the SELECT list		
	--------------------------------------------------------------------------------------------------------------			
		CROSS APPLY 
		(
			SELECT 
				DisposalCodeIsValid = 
					CASE 
						WHEN COALESCE(OP.DisposalCode,'') = ''																							THEN 0
						WHEN LEFT(OP.DisposalCode, 1) = 'P'			AND OP.ContextCode = 'CEN||PAS'														THEN 1
						WHEN DIROP.Division = 'Childrens'			AND OP.DisposalCode IN ('ANAE','FIT','PHB','UNFT') AND OP.ContextCode = 'CEN||PAS'	THEN 1
						WHEN OP.DisposalCode = 'NCPR'				AND OP.ContextCode = 'CEN||PAS'														THEN 1 
						WHEN OP.AttendanceOutcomeCode IS NOT NULL	AND OP.ContextCode = 'TRA||UG'														THEN 1
						ELSE 0 
					END	
												
				,IsAnExclusion =
					CASE
						WHEN OP.LocalAdminCategoryCode IN ('PAY','PrivPt')														THEN 1
						WHEN SPECOP.NationalSpecialtyCode IN ('656')															THEN 0
						WHEN LEFT(SPECOP.NationalSpecialtyCode, 1) = '6'														THEN 1
						WHEN SPECOP.NationalSpecialtyCode IN ('501','960','990')												THEN 1
						WHEN SPECOP.SourceSpecialtyCode IN ('RDPS','RPES','RDCS','ANAE','ICAN','ANAS','ANPR','CADE','CADI')		THEN 1
 						WHEN SPECREF.NationalSpecialtyCode = '501'																THEN 1
						WHEN SPECREF.SourceSpecialtyCode IN ('RDPS','RPES','RDCS','ANAE','ICAN','ANAS','ANPR','CADE','CADI')	THEN 1	
						WHEN CONS.MainSpecialtyCode = '960'																		THEN 1
						WHEN SOR.NationalSourceOfReferralCode IN ('01','04')													THEN 1		
						ELSE 0
					END

				,IsNoAttendanceOutCome = 
					CASE	
						WHEN OP.DNACode					= 9		THEN 1
						WHEN OP.AppointmentStatusCode	= 'NR'	THEN 1
						ELSE 0
					END

				,IsAttendedNoDisposal =
					CASE	
						WHEN OP.DNACode IN ('5','6') AND COALESCE(OP.DisposalCode,'') = ''	THEN 1
						ELSE 0
					END

		) FILTERS	
 	
	 WHERE		
			CAL.DateID BETWEEN @StartDateID AND @EndDateID
		AND (
					OP.DNACode IN ('5','6','9')  		
				OR	OP.AppointmentStatusCode = 'NR'
			)



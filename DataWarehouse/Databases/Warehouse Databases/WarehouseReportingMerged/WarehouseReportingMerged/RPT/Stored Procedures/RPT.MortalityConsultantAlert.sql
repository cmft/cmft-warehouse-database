﻿
create procedure [RPT].[MortalityConsultantAlert] 
(
	@StartDate	date
	,@EndDate	date
	,@Division	varchar(1000)	= NULL
)

as

/****************************************************************************************
	Stored procedure : [RPT].[MortalityConsultantAlert]
	Description		 : SSRS proc for the Visual Studio report
	
	Reports >> Mortality >> MortalityConsultantAlert.rdl
	
	Measures
	 Consultant list of deaths by division, for previous week.

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	19/02/2014	Paul Egan       Intial Coding
*****************************************************************************************/



/*================= debug only ================*/
--declare @StartDate	date = '9 dec 2013';
--declare @EndDate	date = '15 dec 2013';
--declare @Division	varchar(500) = 'Allied Health Professional, Childrens, Clinical & Scientific, Dental, Medicine & Community, 
--										Mental Health Trust, N/A, Opthalmology, Research, Saint Mary's';
/*=============================================*/

select
	FCELast.DistrictNo
	,FCELast.GlobalProviderSpellNo
	,FCELast.DischargeDate
	--,DischargeWeekEnding = WarehouseReporting.dbo.f_Last_of_Week(FCELast.DischargeDate)
	,DischargeWeekEnding = DISCHARGECAL.LastDayOfWeek
	--,FCELastDivisionCode = ''		-- Cannot find in WarehouseReportingMerged
	,AdmitConsultant = 
		coalesce(FCE1Consultant.Surname, FCE1Consultant.NationalConsultantCode, '') 
			+ ', ' + coalesce(FCE1Consultant.Initials, '')
			+ ', ' + coalesce(FCE1Consultant.Title, '')
	,FCE2Consultant = 
		coalesce(FCE2Consultant.Surname, FCE2Consultant.NationalConsultantCode, '') 
			+ ', ' + coalesce(FCE2Consultant.Initials, '')
			+ ', ' + coalesce(FCE2Consultant.Title, '')
	,DischargeConsultant =
		coalesce(FCELastConsultant.Surname, FCELastConsultant.NationalConsultantCode, '') 
			+ ', ' + coalesce(FCELastConsultant.Initials, '')
			+ ', ' + coalesce(FCELastConsultant.Title, '')
	,DischargeDivision = FCELastDirectorate.Division
	,SpellLOS = FCELast.LOS
	,DischargeWard = FCELast.EndWardTypeCode
	,FCE1PrimaryDiagnosis = FCE1Diagnosis.Diagnosis
	,FCE2PrimaryDiagnosis = FCE2Diagnosis.Diagnosis
	,SHMIRisk = SHMI.RiskScore
	
from 
	APC.Encounter FCELast
	
	inner join WH.Calendar DISCHARGECAL
		ON FCELast.DischargeDateID = DISCHARGECAL.DateID
		
	left join APC.Encounter FCE1
	on FCE1.GlobalProviderSpellNo = FCELast.GlobalProviderSpellNo
	and FCE1.GlobalEpisodeNo = 1
	
	left join APC.Encounter FCE2
	on	FCE1.GlobalProviderSpellNo = FCE2.GlobalProviderSpellNo
	and	FCE2.GlobalEpisodeNo = 2
	
	left join APC.DischargeMethod Discharge
	on	FCELast.DischargeMethodID = Discharge.SourceDischargeMethodID
	
	left join WH.Consultant FCE1Consultant
	on FCE1.ConsultantID = FCE1Consultant.SourceConsultantID
	
	left join WH.Consultant FCE2Consultant
	on FCE2.ConsultantID = FCE2Consultant.SourceConsultantID
	
	left join WH.Consultant FCELastConsultant
	on	FCELast.ConsultantID = FCELastConsultant.SourceConsultantID
	
	left join WH.Directorate FCELastDirectorate
	on	FCELast.EndDirectorateCode = FCELastDirectorate.DirectorateCode
	
	left join APC.SHMI SHMI
	on	FCE1.MergeEncounterRecno = SHMI.MergeEncounterRecno
	
	left join WH.Diagnosis FCE1Diagnosis
	on	FCE1.PrimaryDiagnosisCode = FCE1Diagnosis.DiagnosisCode
	
	left join WH.Diagnosis FCE2Diagnosis
	on	FCE2.PrimaryDiagnosisCode = FCE2Diagnosis.DiagnosisCode
	
where 
	FCELast.DischargeDate between @StartDate and @EndDate
	and FCELast.NationalLastEpisodeInSpellIndicator = 1
	and Discharge.NationalDischargeMethodCode in ('4', '5')
	and FCELastDirectorate.Division in (SELECT Item FROM dbo.fn_ListToTable(@Division,','))




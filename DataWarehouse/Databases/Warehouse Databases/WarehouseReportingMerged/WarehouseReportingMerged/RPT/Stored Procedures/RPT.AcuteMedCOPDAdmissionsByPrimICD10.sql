﻿
CREATE procedure [RPT].[AcuteMedCOPDAdmissionsByPrimICD10]

as		
			
select 
	 CasenoteNumber							
	,PatientForename						
	,PatientSurname						
	,AdmissionTime
	,AdmissionFinancialYear	= FinancialYear				
	,DischargeTime
	,DateOfBirth = cast(DateOfBirth as date)
	,DateOfDeath = cast(DateOfDeath as date)
	,DiedDuringStay = case 
						when NationalDischargeMethodCode = '4'
						then 'Yes'
						end	
	,LOS						
	,SourceConsultantCode						
	,'Cons Name' = Title + ' ' + Initials + ' ' + Surname					
	,NationalSpecialtyCode                                                						
	,NationalSpecialty						
	,PrimaryDiagnosisCode						
	,EpisodicGpPracticeCode
	,EpisodicGpPractice = Organisation						
	,AdmissionType
	,Diagnosis.Diagnosis						
							
from 
	APC.Encounter							
							
inner join 
	WH.Consultant                                                   							
on	Consultant.SourceConsultantID = Encounter.ConsultantID 							
							
inner join 
	WH.Specialty							
on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID					
							
inner join 
	APC.AdmissionMethod							
on	AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID							

left join 
	WH.Diagnosis
on	Diagnosis.DiagnosisCode = Encounter.PrimaryDiagnosisCode

inner join
	WH.Calendar
on	Calendar.TheDate = Encounter.AdmissionDate

left join
	Organisation.dbo.Practice Practice
on	Practice.OrganisationCode = Encounter.EpisodicGpPracticeCode

inner join
	APC.DischargeMethod
on	DischargeMethod.SourceDischargeMethodID = Encounter.DischargeMethodID

where 
	AdmissionTime>= '2011-02-01'							
and PrimaryDiagnosisCode like '%J44%'							
and FirstEpisodeInSpellIndicator = '1'							
and Consultant.SourceContextCode like 'CEN%'
﻿


CREATE PROCEDURE [RPT].[AdultCommunuityServicesHeartDisease] 

  @FromDate    Date
 ,@ToDate      Date

AS

--DECLARE @FromDate AS DATE        = '01-NOV-2012'
--DECLARE @ToDate   AS DATE        = '31-DEC-2012'

SELECT  
   Encounter.[DistrictNo]
  ,Encounter.[ProviderSpellNo] 
  ,Encounter.[SourceEncounterNo]
  ,Encounter.[CasenoteNumber]
  ,Encounter.[NHSNumber]
  ,Encounter.[PatientForename]
  ,Encounter.[PatientSurname]
  ,CONVERT(VARCHAR,Encounter.[DateOfBirth], 103)                                AS DoB
  ,FLOOR(DATEDIFF(d,Encounter.[DateOfBirth],Encounter.[AdmissionTime])/365.25)  AS AGE
  ,CONVERT(VARCHAR,Encounter.[AdmissionTime], 103)                              AS Admit_Date
  ,CONVERT(VARCHAR,Encounter.[DischargeTime], 103)                              AS Disch_Date
  ,Encounter.[LoS]
  ,Encounter.[Postcode]
  ,Encounter.[PrimaryDiagnosisCode]
  ,Diagnosis.[Diagnosis]
  ,Encounter.[RegisteredGpCode]
  ,Encounter.[RegisteredGpPracticeCode]
  --,Practice.[Practice]
  ,Practice.[Organisation]
  ,AdmissionSource.[LocalAdmissionSource]
  ,AdmissionMethod.[LocalAdmissionMethod]
  ,Consultant.[SourceConsultantCode]
  ,'ConsultantName' =
   CASE 
     WHEN LEFT(Consultant.[SourceContextCode],3) = 'TRA' THEN Consultant.[SourceConsultant]
     ELSE Consultant.[Title] + ' - ' + Consultant.[Initials]+ ' - ' +Consultant.[Surname]
   END 
  ,SPEC.[SourceSpecialtyCode]
  ,SPEC.[LocalSpecialty]
  ,Site.[NationalSiteCode]
  ,Encounter.[PCTCode]
  ,Encounter.[FirstEpisodeInSpellIndicator]
  
FROM [APC].[Encounter]                  AS Encounter
 
INNER JOIN [WH].[Diagnosis]                   AS Diagnosis
ON Encounter.[PrimaryDiagnosisCode] = Diagnosis.[DiagnosisCode]
 
LEFT OUTER JOIN [WH].[Calendar]         AS Calendar
ON Encounter.[AdmissionDateID] = Calendar.[DateID]

LEFT OUTER JOIN  [WH].[Consultant]      AS Consultant
ON Encounter.[ConsultantID] = Consultant.[SourceConsultantID]

LEFT OUTER JOIN  [WH].[Site]            AS SITE
ON  Encounter.[StartSiteId]  = [SourceSiteID]

LEFT OUTER JOIN  [WH].[Specialty]       AS SPEC
ON Encounter.[SpecialtyID] = [SourceSpecialtyID]

LEFT OUTER JOIN [APC].[AdmissionSource] AS AdmissionSource
ON Encounter.[AdmissionSourceID] = AdmissionSource.[SourceAdmissionSourceID]

LEFT OUTER JOIN [APC].[AdmissionMethod] AS AdmissionMethod
ON Encounter.[AdmissionMethodID] = AdmissionMethod.[SourceAdmissionMethodID]

--LEFT OUTER JOIN wh.practice               AS Practice
--ON Encounter.[RegisteredGpPracticeCode] = Practice.[PracticeCode]

LEFT OUTER JOIN [Organisation].[dbo].[Practice]		AS Practice
ON Encounter.[RegisteredGpPracticeCode] = Practice.[OrganisationCode]

WHERE  
	Calendar.[TheDate] BETWEEN  @FromDate AND  @ToDate
AND [FirstEpisodeInSpellIndicator] = 1
AND Encounter.[PrimaryDiagnosisCode] 
IN('I50.0','I50.10','I50.9','I11.0','I11.9','I13.0','I13.1','I13.2','I13.9','I51.5','I25.5','I97.8')
AND Site.[NationalSiteCode] = 'RW3MR'
  

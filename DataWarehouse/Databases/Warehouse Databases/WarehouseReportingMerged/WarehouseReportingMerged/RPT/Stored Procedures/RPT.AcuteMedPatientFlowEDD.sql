﻿

create procedure [RPT].[AcuteMedPatientFlowEDD]

As
		select 								
	 Date = DateAdd(day, coalesce(ExpectedLOS,0), theDate)							
	,EDD = Count(1)							
								
from 								
	APC.Encounter							
								
join WH.Calendar								
on AdmissionDateID = DateID								
								
join WH.Site 								
on SiteID = Sourcesiteid								
								
join APC.Ward								
on StartWardID = SourceWardID								
								
join WH.Specialty								
on SpecialtyID = SourceSpecialtyID								
								
join WH.Directorate								
on StartDirectorateCode = DirectorateCode							
								
where								
	Site.SourceContext = 'Torex PAS (Central)'							
	and DateAdd(day, coalesce(ExpectedLOS,0), theDate) < DateAdd(day, 3,GetDate())							
	and DateAdd(day, coalesce(ExpectedLOS,0), theDate) > GetDate()							
	and FirstEpisodeInSpellIndicator = 1							
	and SourceWardCode in  ('MAU', 'TMAU', '15M', 'MRU', '7M','OMU','AMU') 							
	and NationalSpecialtyCode <> '180'							
	and Division = 'Acute Medical'							
								
group by								
	DateAdd(day, coalesce(ExpectedLOS,0), theDate)							


﻿
CREATE PROCEDURE [RPT].DuplicatePatientRegistrations
(
	 @StartDate		DATE
	,@EndDate		DATE
)
AS

	SELECT
		 SourcePatientNo
		,MasterSourcePatientNo
		,RegistrationDate
		,Cases
		,PeriodWeekNo
		,PeriodWeekNoKey
		,PeriodMonth
		,PeriodMonthKey	
		,PeriodYear
		,PeriodWeekStartDate
		,PeriodWeekEndDate
	FROM
		RPT.fn_DuplicatePatientRegistrations(@StartDate, @EndDate)
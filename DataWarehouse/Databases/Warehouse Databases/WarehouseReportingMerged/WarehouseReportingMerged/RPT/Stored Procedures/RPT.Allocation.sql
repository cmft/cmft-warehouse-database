﻿CREATE proc  [RPT].[Allocation]
(
@Allocation varchar(100) = null
,@AllocationType varchar(100) = null
)

as

select
	*
from
	(
	select
		Allocation.AllocationID
		,Allocation.SourceAllocationID
		,Allocation.Allocation
		,AllocationType.AllocationType
		,AllocationType.AllocationTypeID
		,Priority =
					coalesce(
						 Allocation.Priority
						,Template.Priority
					)
		,Dataset.Dataset
		,RuleBase.Template
		,RuleBase.SourceSpecialtyCode
		,RuleBase.NationalSpecialtyCode
		,RuleBase.SourceClinicCode
		,RuleBase.NationalSiteCode
		,RuleBase.AdmissionTypeCode
		,RuleBase.ProcedureCode
		,RuleBase.OldServiceID
		,RuleBase.Division
		,RuleBase.SourceContextCode
		,RuleBase.SourceConsultantCode
		,RuleBase.SourceSourceOfReferralCode
		,RuleBase.NationalFirstAttendanceCode
		,RuleBase.DiagnosticNationalExamCode
		,RuleBase.SourceWaitingListCode
		,RuleBase.ConsultantNationalMainSpecialtyCode
		,RuleBase.DiagnosisCode
		,RuleBase.PatientCategoryCode
		,RuleBase.StartWardCode
		,RuleBase.EndWardCode
		,RuleBase.PatientClassificationCode
		,RuleBase.StartSiteCode
		,RuleBase.EndSiteCode
		,RuleBase.Duration
		,RuleBase.StartDirectorateCode
		,RuleBase.AdmissionMethodCode
		,RuleBase.OtherProcedureCode
		,RuleBase.SecondaryOperationCode1
		,RuleBase.SecondaryOperationCode2
		,RuleBase.SecondaryOperationCode3
		,RuleBase.Boolean
		,RuleBase.HRGCode
		,RuleBase.DirectorateCode
		,RuleBase.NationalAdministrativeCategoryCode
		,RuleBase.ReferralSpecialtyTypeCode
		,RuleBase.CommissionerCode
		,RuleBase.NationalSexCode
		,RuleBase.ContractSerialNo
		,RuleBase.NationalNeonatalLevelOfCareCode
		,RuleBase.FirstEpisodeInSpellIndicator
		,RuleBase.SourceIntendedManagementCode
		,RuleBase.SourceAppointmentTypeCode
		,RuleBase.WardCode
		,RuleBase.SourceSiteCode
		,RuleBase.FromDate
		,RuleBase.ToDate
		,RuleBase.NationalSourceOfReferralCode
		,RuleBase.SourceAppointmentType
		,RuleBase.CancerSite
		,RuleBase.DepartmentCode
		,RuleBase.SourceAppointmentStatusCode
		,RuleBase.NationalAttendanceStatusCode
		,RuleBase.ProcedureCoded
		,RuleBase.DischargeMethodCode
		,RuleBase.DischargeDestinationCode
		,RuleBase.DischargeDate
		,RuleBase.NeonatalWardStay
		,RuleBaseList1 =
				(
				select
					RuleBaseListValue + ', '
				from
					WarehouseOLAPMergedV2.Allocation.RuleBaseList
				where
					RuleBase.RuleBaseList1 = RuleBaseList.RuleBaseList
				for xml path('') 
				)
		,RuleBaseList2 =
				(
				select
					RuleBaseListValue + ', '
				from
					WarehouseOLAPMergedV2.Allocation.RuleBaseList
				where
					RuleBase.RuleBaseList2 = RuleBaseList.RuleBaseList
				for xml path('') 
				)
		,RuleBaseList3 =
				(
				select
					RuleBaseListValue + ', '
				from
					WarehouseOLAPMergedV2.Allocation.RuleBaseList
				where
					RuleBase.RuleBaseList3 = RuleBaseList.RuleBaseList
				for xml path('') 
				
				)
		,Reportable
		,OperationDayOfWeek
		,SessionPeriod
		,TheatreSurgeonCode		

	from   	
		WarehouseOLAPMergedV2.Allocation.RuleBase

	inner join WarehouseOLAPMergedV2.Allocation.TemplateDataset
	on	TemplateDataset.Template =  RuleBase.Template
	and	TemplateDataset.DatasetCode = RuleBase.DatasetCode
	and	TemplateDataset.Active = 1

	inner join WarehouseOLAPMergedV2.Allocation.Dataset
	on	Dataset.DatasetCode = TemplateDataset.DatasetCode

	inner join WarehouseOLAPMergedV2.Allocation.Allocation
	on	Allocation.AllocationID = RuleBase.AllocationID
	and	Allocation.Active = 1

	inner join WarehouseOLAPMergedV2.Allocation.AllocationType
	on	Allocation.AllocationTypeID = AllocationType.AllocationTypeID

	inner join WarehouseOLAPMergedV2.Allocation.Template
	on	Template.Template = TemplateDataset.Template
	and	Template.AllocationTypeID = Allocation.AllocationTypeID

	union

	select
		Allocation.AllocationID
		,Allocation.SourceAllocationID
		,Allocation.Allocation
		,AllocationType.AllocationType
		,AllocationType.AllocationTypeID
		,Priority =
					coalesce(
						 Allocation.Priority
						,Template.Priority
					)
		,Dataset = RuleBase.DatasetCode
		,RuleBase.Template
		,RuleBase.SourceSpecialtyCode
		,RuleBase.NationalSpecialtyCode
		,RuleBase.SourceClinicCode
		,RuleBase.NationalSiteCode
		,RuleBase.AdmissionTypeCode
		,RuleBase.ProcedureCode
		,RuleBase.OldServiceID
		,RuleBase.Division
		,RuleBase.SourceContextCode
		,RuleBase.SourceConsultantCode
		,RuleBase.SourceSourceOfReferralCode
		,RuleBase.NationalFirstAttendanceCode
		,RuleBase.DiagnosticNationalExamCode
		,RuleBase.SourceWaitingListCode
		,RuleBase.ConsultantNationalMainSpecialtyCode
		,RuleBase.DiagnosisCode
		,RuleBase.PatientCategoryCode
		,RuleBase.StartWardCode
		,RuleBase.EndWardCode
		,RuleBase.PatientClassificationCode
		,RuleBase.StartSiteCode
		,RuleBase.EndSiteCode
		,RuleBase.Duration
		,RuleBase.StartDirectorateCode
		,RuleBase.AdmissionMethodCode
		,RuleBase.OtherProcedureCode
		,RuleBase.SecondaryOperationCode1
		,RuleBase.SecondaryOperationCode2
		,RuleBase.SecondaryOperationCode3
		,RuleBase.Boolean
		,RuleBase.HRGCode
		,RuleBase.DirectorateCode
		,RuleBase.NationalAdministrativeCategoryCode
		,RuleBase.ReferralSpecialtyTypeCode
		,RuleBase.CommissionerCode
		,RuleBase.NationalSexCode
		,RuleBase.ContractSerialNo
		,RuleBase.NationalNeonatalLevelOfCareCode
		,RuleBase.FirstEpisodeInSpellIndicator
		,RuleBase.SourceIntendedManagementCode
		,RuleBase.SourceAppointmentTypeCode
		,RuleBase.WardCode
		,RuleBase.SourceSiteCode
		,RuleBase.FromDate
		,RuleBase.ToDate
		,RuleBase.NationalSourceOfReferralCode
		,RuleBase.SourceAppointmentType
		,RuleBase.CancerSite
		,RuleBase.DepartmentCode
		,RuleBase.SourceAppointmentStatusCode
		,RuleBase.NationalAttendanceStatusCode
		,RuleBase.ProcedureCoded
		,RuleBase.DischargeMethodCode
		,RuleBase.DischargeDestinationCode
		,RuleBase.DischargeDate
		,RuleBase.NeonatalWardStay
		,RuleBaseList1Detail =
				(
				select
					RuleBaseListValue + ', '
				from
					WarehouseOLAPMergedV2.Allocation.RuleBaseList
				where
					RuleBase.RuleBaseList1 = RuleBaseList.RuleBaseList
				for xml path('') 
				)
		,RuleBaseList2Detail =
				(
				select
					RuleBaseListValue + ', '
				from
					WarehouseOLAPMergedV2.Allocation.RuleBaseList
				where
					RuleBase.RuleBaseList2 = RuleBaseList.RuleBaseList
				for xml path('') 
				)
		,RuleBaseList3Detail =
				(
				select
					RuleBaseListValue + ', '
				from
					WarehouseOLAPMergedV2.Allocation.RuleBaseList
				where
					RuleBase.RuleBaseList3 = RuleBaseList.RuleBaseList
				for xml path('') 
				)
				,Reportable
				,OperationDayOfWeek
				,SessionPeriod
				,TheatreSurgeonCode				
	from   	
		WarehouseOLAPMergedV2.Allocation.RuleBase

	inner join WarehouseOLAPMergedV2.Allocation.Allocation
	on	Allocation.AllocationID = RuleBase.AllocationID
	and	Allocation.Active = 1

	inner join WarehouseOLAPMergedV2.Allocation.AllocationType
	on	Allocation.AllocationTypeID = AllocationType.AllocationTypeID

	inner join WarehouseOLAPMergedV2.Allocation.Template
	on	Template.Template = RuleBase.Template
	and	Template.AllocationTypeID = Allocation.AllocationTypeID

	--inner join WarehouseOLAPMergedV2.Allocation.TemplateDataset
	--on	TemplateDataset.Template = RuleBase.Template
	--and	TemplateDataset.Active = 1

	--inner join WarehouseOLAPMergedV2.Allocation.Dataset
	--on	Dataset.DatasetCode = TemplateDataset.DatasetCode

where
	RuleBase.DatasetCode = 'DIRECT' 

	) AllocationTemplateDataset

where
	exists
		(
		select
			1
		from
			WarehouseOLAPMergedV2.Allocation.Allocation
		where
			AllocationTemplateDataset.AllocationID = Allocation.AllocationID
		and (
				Allocation.Allocation like '%' + @Allocation + '%'
			or	@Allocation is null		
			)
		)
		
and	exists
		(
		select
			1
		from
			WarehouseOLAPMergedV2.Allocation.AllocationType
		where
			AllocationTemplateDataset.AllocationTypeID = AllocationType.AllocationTypeID
		and (
				AllocationType.AllocationType like '%' + @AllocationType + '%'
			or	@AllocationType is null		
			)
		)
﻿
/******************************************************************************
**  Name: RPT.DQActivityBrokerUnmatchedUnactionedDrillThrough
**  Purpose: Data Quality drill through report for un-matched ActivityBroker
**           unactioned requests over 48 hours
**
**  Parent Stored procedure : RPT.DQActivityBrokerUnmatchedPatients
**
**	Input Parameters
**	
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 14.11.13      MH       Created
******************************************************************************/

CREATE PROCEDURE RPT.DQActivityBrokerUnmatchedUnactionedDrillThrough
AS
	DECLARE @SnapshotTime	DATETIME
	DECLARE @NullDate		DATE = '1 Jan 1900'

-- Get the time the data was built	
	SELECT 
		@SnapshotTime = MAX(EventTime)
	FROM
		ActivityBroker.Utility.AuditLog

SELECT
		  SourceUniqueId		= P.PathwayID
		 ,Type					= CASE
									WHEN ORIGAPCWAIT.APCWaitID IS NOT NULL THEN COALESCE(APCWLS.ActivityType,'Inpatient WL Activity')
									WHEN ORIGSPELL.APCSpellID IS NOT NULL THEN 'Inpatient Transfer'
									WHEN ORIGOPWAIT.OPWaitID IS NOT NULL THEN 'Outpatient Wait'
									ELSE 'Unknown'
								  END
		 ,DistrictNo			= COALESCE(ORIGAPCWAIT.DistrictNo, ORIGSPELL.DistrictNo, ORIGOPWAIT.DistrictNo)
		 ,Speciality			= COALESCE(ORIGAPCWAIT.Specialty, ORIGSPELL.Specialty, ORIGOPWAIT.Specialty)
		 ,NHSNumber				= COALESCE(ORIGAPCWAIT.NHSNumber, PATSPELL.NHSNumber, ORIGOPWAIT.NHSNumber)
		 ,[Discharge Site]		= COALESCE(ORIGAPCWAITSITE.NationalSite, ORIGAPCSPELLSITE.NationalSite, ORIGOPWAITSITE.NationalSite)
		 ,CreatedInBrokerDate	= ABT.CreatedTimestamp
		 ,WaitingListCode		= COALESCE(ORIGAPCWAIT.WaitingListCode, ORIGOPWAIT.WaitingListCode)
		 ,WaitingListComments	= COALESCE(ORIGAPCWAIT.Comments, ORIGOPWAIT.Comments)
		 		 
	FROM	
		ActivityBroker.dbo.ActivityBrokerTracker ABT
		INNER JOIN ActivityBroker.AB.Pathway P
			ON ABT.PathwayID = P.PathwayID
		LEFT JOIN ActivityBroker.AB.APCWait ORIGAPCWAIT
			ON P.OriginAPCWaitID = ORIGAPCWAIT.APCWaitID
		LEFT JOIN ActivityBroker.AB.APCSpellTransfer ORIGSPELL
			ON P.OriginAPCSpellID = ORIGSPELL.APCSpellID
		LEFT JOIN ActivityBroker.AB.OPWait ORIGOPWAIT
			ON P.OriginOPWaitID = ORIGOPWAIT.OPWaitID
		LEFT JOIN ActivityBroker.SyncEngine.Patient PATSPELL
			ON ORIGSPELL.SourcePatientNo = PATSPELL.SourcePatientNo
		LEFT JOIN ActivityBroker.AB.APCWaitStatusCode APCWLS
			ON ORIGAPCWAIT.APCWaitStatusCode = APCWLS.ActivityTypeCode
		LEFT JOIN WH.Site ORIGAPCWAITSITE
			ON		ORIGAPCWAIT.SiteCode	= ORIGAPCWAITSITE.SourceSiteCode
				AND ORIGAPCWAIT.ContextCode	= ORIGAPCWAITSITE.SourceContextCode
		LEFT JOIN WH.Site ORIGAPCSPELLSITE
			ON		ORIGSPELL.SiteCode		= ORIGAPCSPELLSITE.SourceSiteCode
				AND ORIGSPELL.ContextCode	= ORIGAPCSPELLSITE.SourceContextCode
		LEFT JOIN WH.Site ORIGOPWAITSITE
			ON		ORIGOPWAIT.SiteCode	= ORIGOPWAITSITE.SourceSiteCode
				AND ORIGOPWAIT.ContextCode	= ORIGOPWAITSITE.SourceContextCode
		
	WHERE
			ABT.ActivityType <> 'NoAction'	
		AND DATEDIFF(HOUR, ABT.CreatedTimestamp,@SnapshotTime) > 48
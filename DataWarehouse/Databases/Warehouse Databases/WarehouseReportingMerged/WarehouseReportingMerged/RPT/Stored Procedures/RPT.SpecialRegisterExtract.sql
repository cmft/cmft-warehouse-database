﻿
CREATE proc [RPT].[SpecialRegisterExtract] 
	
	
	
as
	
	-- @RegisterStartDate datetime 
	--,@RegisterEndDate datetime
	--Debug
	 --DECLARE @RegisterStartDate datetime = '04 Sep 2015'
	-- DECLARE @RegisterEndDate datetime = '04 sep 2015'

Select                                                    
     Special.EnteredDate
    ,encounter.Providerspellno
    ,CasenoteNumber
    ,PatientForename
    ,PatientSurname
    ,SexCode
    ,AdmissionTime
    ,DischargeTime
    ,LatestWardStartTime = 
		Max (StartTime) 

    ,LatestWardEndCode = 
		Max (EndWardTypeCode)
    ,LatestLocalSpecialty = Specialty.NationalSpecialtycode
    ,LatestNationalSpecialtyLabel = Specialty.NationalSpecialtyLabel
    ,LatestDivision = WardDirectorate.Division
    ,WardDivision = WardDirectorate.Division
    ,SpecialRegister = 
		case SpecialRegisterCode
		when 'CPC' then 'CPE Positive (Total)'
		when 'CPCN' then 'CPE NDM'
		when 'CPCO' then 'CPE OXA'
		when 'CPCV' then 'CPC VIM'
		when 'CPCI' then 'CPC IMP'
		else SpecialRegisterCode
		end
	,PatientCount = 1
	,Encounter.PatientCategoryCode
			                           
FROM 
	APCUpdate.Encounter Encounter 
			
	left outer join WarehouseOLAPMergedV2.[APC].[BaseWardStay] Ward
	on 	Encounter.ProviderSpellNo = Ward.ProviderSpellNo  
			
	--inner join WarehouseOLAPMergedV2.[APC].[BaseWardStayReference] BaseWardStayReference
	-- ON BaseWardStayReference.MergeEncounterRecno = Ward.MergeEncounterRecno
			
	--Inner join APC.WardStay Ward
	--on Encounter.ProviderSpellNo = Ward.ProviderSpellNo

	left join  APCUpdate.PatientSpecialRegister Special
	on special.SourcePatientNo = Encounter.SourcePatientno
              
	left join WH.Specialty Specialty 
	on Specialty.SourceSpecialtyCode = Encounter.SpecialtyCode                                           
                     
	left join WH.Directorate Directorate 
	on Directorate.DirectorateCode = Encounter.StartDirectorateCode
			
	left join WH.Directorate WardDirectorate 
	on WardDirectorate.DirectorateCode = encounter.endDirectorateCode
                        
    --        ,    (
				--SELECT                                                     
				--	 [WeekNo]                                                    
				--	 ,[TheDate]    
			                                               
    --          FROM [warehousesql].[WarehouseReportingMerged].[WH].[Calendar] Cal                                                    
            
    --          where 
			 -- cal.TheDate between @RegisterStartDate and @RegisterEndDate ) week                                                      
                                             
                                        
			 --(

			 --[StartDate] <= thedate and([Enddate] >= thedate 
			 --or 
			 --[Enddate] is null)
			 
			 --)                                                     
                                                           
     
				--	    and 
where  
	Episodeenddate is null
    and SpecialRegisterCode in ('CPC','CPCO','CPCV','CPCN','CPCI')
       -- and EndWardTypeCode = 'IH'
		and Encounter.PatientCategoryCode <> 'RD'
		--)
group by
	EnteredDate                                        
	,encounter.Providerspellno
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,SexCode
	,AdmissionTime
	,DischargeTime
	,wardDirectorate.Division
	,SpecialRegisterCode   
	,NationalSpecialtyLabel 
	,NationalSpecialtycode                                   
	,Encounter.PatientCategoryCode


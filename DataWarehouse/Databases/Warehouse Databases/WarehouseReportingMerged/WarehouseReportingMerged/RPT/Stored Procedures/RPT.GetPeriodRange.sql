﻿







CREATE PROCEDURE [RPT].[GetPeriodRange] -- [RPT].[GetPeriodRange] 'Q', 'Qtr 1 2015/2016'
(
	 @TimeSeries varchar(1)
	,@Period varchar(max)

)

AS 

/* 
==============================================================================================
Description:

*** The LIVE proc has the following code: ***
-- Permissions
GRANT EXECUTE ON  [RPT].[GetPeriodRange] TO [ReportExecutor]

When		Who			What
13/02/2015	Paul Egan	Added Academic Year for CHAMP reports.
						Repointed to WarehouseReportingMerged (instead of WarehouseReporting).
===============================================================================================
*/

DECLARE @SQL AS NVARCHAR(MAX)
DECLARE @ParmDefinition NVARCHAR(500)

BEGIN

set @ParmDefinition = N'@Period VARCHAR(MAX)'

set @SQL = 
case @TimeSeries

when 'W' then 
  
  N'SELECT MAX(theDate) EndDate, MIN(thedate) StartDate
  from WarehouseReportingMerged.WH.Calendar 
  where WeekNo = @Period'    

when 'M' then

  N'SELECT MAX(theDate) EndDate, MIN(thedate) StartDate
  from WarehouseReportingMerged.WH.Calendar
  where TheMonth = @Period'  
  
when 'Q' then

  N'SELECT MAX(theDate) EndDate, MIN(thedate) StartDate
  from WarehouseReportingMerged.WH.Calendar
  where FinancialQuarter = @Period'

when 'Y' then

  N'SELECT MAX(theDate) EndDate, MIN(thedate) StartDate
  from WarehouseReportingMerged.WH.Calendar
  where FinancialYear = @Period'
  
when 'A' then

  N'SELECT MAX(theDate) EndDate, MIN(thedate) StartDate
  from WarehouseReportingMerged.WH.Calendar
  where AcademicYear = @Period'

END

EXEC SP_EXECUTESQL @SQL, @ParmDefinition, @Period = @Period

END





﻿
CREATE proc [RPT].[ChildrenSpecialtyRequest]

as


SELECT AandE.ArrivalDate
,case when ToSpecialtyTime is null
				      then 'No'
				      else 'Yes'
				      end as [ReferredToSpecialty]
,case when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) is null
				      then 'No'
				      else 'Yes'
				      end as [SeenBySpecialty]
,case when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) is null
				      then 'Not Seen'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '30'
				      then '0-30min'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '60'
				      then '31min-1hr'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '120'
				      then '1hr-2hrs'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '180'
				      then '2hrs-3hrs'
				      else '3+hrs'
				      end as [Group]
,case when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) is null
				      then NULL
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '30'
				      then 'Within Target'
				      else 'Breach'
				      end as [Target]
				      ,count (AttendanceNumber) as [Cases]
				      
FROM            AE.Encounter as AandE

inner join [WH].[Specialty] as spec
on
AandE.[ReferredToSpecialtyID] = spec.[SourceSpecialtyID]

WHERE

(AttendanceCategoryID = '1892653') 
AND (AttendanceNumber LIKE 'PED%') 
AND (ArrivalDate >= '01 Jan 2015')


group by AandE.ArrivalDate
,case when ToSpecialtyTime is null
				      then 'No'
				      else 'Yes'
				      end
,case when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) is null
				      then 'No'
				      else 'Yes'
				      end
,case when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) is null
				      then 'Not Seen'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '30'
				      then '0-30min'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '60'
				      then '31min-1hr'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '120'
				      then '1hr-2hrs'
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '180'
				      then '2hrs-3hrs'
				      else '3+hrs'
				      end
,case when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) is null
				      then NULL
				      when (datediff(mi,ToSpecialtyTime,SeenBySpecialtyTime)) < '30'
				      then 'Within Target'
				      else 'Breach'
				      end
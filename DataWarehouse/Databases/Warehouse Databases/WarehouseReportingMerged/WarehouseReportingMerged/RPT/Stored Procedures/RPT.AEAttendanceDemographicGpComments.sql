﻿
CREATE proc [RPT].[AEAttendanceDemographicGpComments]

@DepartureDate date = null

as

select --top 1000
	AttendanceNumber
	,PatientName
	,DistrictNo
	,NHSNumber = isnull(NHSNumber,'Not Specified')
	,DateOfBirth
	,RegisteredGpCode = isnull(RegisteredGpCode,'Not Specified')
	,RegisteredGpPracticeCode = isnull(RegisteredGpPracticeCode,'Not Specified')
	,ArrivalTime = cast(cast(ArrivalDate as varchar) + ' ' + left(ArrivalTime, 11) as datetime)
	,DepartureTime
	,ClinicianSeen = CMFT.String.Propercase(isnull(ClinicianSeen, 'Not Specified'))
	,InformationForGP = isnull(nullif(InformationForGP, ''),'Not Specified')		
from
	AE.DischargeSummary
where
	cast(DepartureTime as date) = @DepartureDate
and	(
		NHSNumber is null
	or	RegisteredGpCode is null
	or	RegisteredGpCode = 'G9999998'
	or	RegisteredGpCode = 'G9999981'
	or	RegisteredGpPracticeCode is null
	or	RegisteredGpPracticeCode = 'V81999'
	or	RegisteredGpPracticeCode = 'V81998'
	or	ClinicianSeen is null
	or	InformationForGP is null
	)



	



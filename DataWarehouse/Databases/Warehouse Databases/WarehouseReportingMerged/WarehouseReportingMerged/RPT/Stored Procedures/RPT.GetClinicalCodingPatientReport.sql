﻿




CREATE proc [RPT].[GetClinicalCodingPatientReport]

 @CasenoteNumber varchar(50) = null

as



--declare @CasenoteNumber = 'M12/007564'

select
	--distinct
	Encounter.ContextCode
	,Encounter.SourcePatientNo
	,Encounter.NHSNumber
	,Encounter.CasenoteNumber
	,PatientName = Encounter.[PatientSurname] + ', ' + Encounter.[PatientForename]
	,Encounter.DateOfBirth
	,LatestAdmissionDate = Admission.TheDate
	,AlwaysPresentCondition = AlwaysPresent.[Diagnosis]
	,AlwaysPresentConditionLatestAdmissionDate = AlwaysPresent.LatestAdmissionDate

from
	[APC].[Encounter] Encounter

inner join
	[WH].[Calendar] Admission
on	Encounter.AdmissionDateID = Admission.DateID

--inner join
--	[APC].[LastEpisodeInSpell] LastEpisodeInSpell
--on	Encounter.LastEpisodeInSpellID = LastEpisodeInSpell.SourceLastEpisodeInSpellID

left outer join
			(
			select
				AlwaysPresent.Diagnosis
				,Encounter.SourcePatientNo
				,Encounter.ContextCode
				,LatestAdmissionDate = max(Encounter.AdmissionTime)
			from
				[APC].[Diagnosis] Diagnosis

			inner join 
				APC.Encounter Encounter
			on	Diagnosis.[MergeEncounterRecno] = Encounter.[MergeEncounterRecno]

			inner join 
				WH.Diagnosis AlwaysPresent
			on	AlwaysPresent.[DiagnosisCode] = Diagnosis.[DiagnosisCode]
			and	AlwaysPresent.[IsAlwaysPresent] = 1

			group by
				AlwaysPresent.Diagnosis
				,Encounter.SourcePatientNo
				,Encounter.ContextCode
						
			) AlwaysPresent

			on	AlwaysPresent.SourcePatientNo = Encounter.SourcePatientNo
			and AlwaysPresent.ContextCode = Encounter.ContextCode

where
    Encounter.CasenoteNumber = @CasenoteNumber
and
	NationalLastEpisodeInSpellIndicator = '1'
and

	not exists
			(
			select
				1
			from
				[APC].[Encounter] EncounterLatest
			where
				EncounterLatest.SourcePatientNo = Encounter.SourcePatientNo
			and
				EncounterLatest.ContextCode = Encounter.ContextCode
			and
				EncounterLatest.AdmissionTime > Encounter.AdmissionTime
			)






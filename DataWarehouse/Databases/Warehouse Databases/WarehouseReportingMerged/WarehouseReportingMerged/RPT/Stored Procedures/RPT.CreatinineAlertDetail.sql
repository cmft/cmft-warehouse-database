﻿
CREATE PROCEDURE [RPT].[CreatinineAlertDetail] 
(
	 @FromDate date
	,@ToDate date
	,@ReportType varchar(10) -- RATE, CHANGE or VALUE
	,@RateOfChange int
	,@Stage varchar (100) -- (Stage1 (>=1.5 to <2.0); Stage2 (>=2.0 to <3.0); Stage3 (>=3.0) 
	,@ResultValue int
	,@Summary bit
	,@DistrictNo char(8)
	,@Interval numeric(21 , 10)
	,@CurrentInpatient tinyint
)

as

set dateformat dmy

select
	 PatientForename
	,PatientSurname
	,DistrictNo
	,Age
	,Sex
	,AdmissionTime
	,DischargeTime
	,CurrentWard
	,Result
	,EffectiveTime
	,ResultTime
	,PatientSequence
	,BaselineResult
	,BaselineResultTime
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio
	,CurrentInpatient
	,Result.LengthOfStay
	,DiedInHospital
	,ReportSequenceNo
	,CasenoteNumber
	,DateOfBirth
	,Stage
	,GlobalProviderSpellNo
	,ResultAPCLinked
	,InhospitalAKI
	,ResultWardCode
	,SequenceNo
	,RenalPatient
from
	(
	SELECT
		 Result.PatientForename
		,Result.PatientSurname
		,Result.DistrictNo
		,Age = AgeAtTest
		,Sex = Result.SexCode
		,Result.AdmissionTime
		,DischargeTime
		,CurrentWard
		,Result.Result
		,Result.EffectiveTime
		,Result.ResultTime
		,Result.PatientSequence
		,Result.SpellSequence
		,Result.BaselineResult
		,Result.BaselineResultTime
		,Result.BaselineToCurrentDays
		,Result.BaselineToCurrentChange
		,Result.CurrentToBaselineResultRatio
		,Result.CurrentInpatient 
		,LengthOfStay = DATEDIFF(day,cast(AdmissionTime as date),coalesce(cast(DischargeTime as date),getdate()))
		,DiedInHospital = 
				case
					when exists
						(
						Select
							1
						from
							APC.DischargeMethod
						where
							DischargeMethod.SourceDischargeMethodCode = Result.DischargeMethodCode
						and DischargeMethod.NationalDischargeMethod in ('Patient DiEd','Stillbirth')
						)
					then 1
					else 0
				end
		,ReportSequenceNo =
			case
			when @ReportType = 'RATE' then row_number () over (partition by Result.DistrictNo order by Result.ResultRateOfChangePer24Hr desc)
			when @ReportType = 'CHANGE' then row_number () over (partition by Result.DistrictNo,Result.GlobalProviderSpellNo order by Result.CurrentToBaselineResultRatio desc)
			when @ReportType = 'LATEST' then  row_number () over (partition by Result.DistrictNo order by Result.ResultTime desc)
			when @ReportType = 'VALUE' then  row_number () over (partition by Result.DistrictNo,Result.GlobalProviderSpellNo order by Result.Result desc)
			end
		,Result.CasenoteNumber
		,Result.DateOfBirth
		,Result.Stage
		,GlobalProviderSpellNo
		,ResultAPCLinked
		,InhospitalAKI
		,ResultWardCode = WardEpisodeStart
		,SequenceNo = row_number () over (partition by Result.DistrictNo order by Result.ResultTime desc)
		,RenalPatient
	FROM
		Result.AcuteKidneyInjuryAlert Result
	where
		Result.DistrictNo = @DistrictNo
	or	@DistrictNo is null

	) Result

ORDER BY
	 Result.DistrictNo
	,Result.ResultTime desc






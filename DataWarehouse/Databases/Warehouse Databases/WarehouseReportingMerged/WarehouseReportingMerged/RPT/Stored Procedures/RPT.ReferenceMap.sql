﻿

CREATE proc [RPT].[ReferenceMap] 
(
	@AttributeCode varchar(20) = null
	,@ContextCode varchar(20) = null
	,@UnmappedLocalOnly bit = null		
	,@UnmappedNationalOnly bit = null		
)

as

select
	 AttributeCode
	,Attribute
	,SourceContextID
	,SourceContextCode
	,SourceContext
	,SourceValueID
	,SourceValueCode
	,SourceValue
	,LocalValueID
	,LocalValueCode
	,LocalValue
	,NationalValueID
	,NationalValueCode
	,NationalValue
	,OtherValueID
	,OtherValueCode
	,OtherValue
	,LocalMapping
	,NationalMapping
from
	(
	select
		 Member.AttributeCode
		,Member.Attribute
		,Member.SourceContextID
		,Member.SourceContextCode
		,Member.SourceContext
		,Member.SourceValueID
		,Member.SourceValueCode
		,Member.SourceValue
		,Member.LocalValueID
		,Member.LocalValueCode
		,Member.LocalValue
		,Member.NationalValueID
		,Member.NationalValueCode
		,Member.NationalValue
		,Member.OtherValueID
		,Member.OtherValueCode
		,Member.OtherValue
		,LocalMapping = 
			case left(Member.LocalValueCode, 3)
			when 'L||'
			then 0
			else 1
			end
		,NationalMapping = 
			case left(Member.NationalValueCode, 3)
			when 'N||'
			then 0
			else 1
			end

	from
		WarehouseOLAPMergedV2.WH.Member
		
	) ReferenceMap

where
	(
		ReferenceMap.AttributeCode = @AttributeCode
	or	@AttributeCode is null
	)

and (
		ReferenceMap.SourceContextCode = @ContextCode
	or	@ContextCode is null
	)
and ReferenceMap.LocalMapping = 
				case
				when @UnmappedLocalOnly = 1
				then 0
				else ReferenceMap.LocalMapping
				end
and ReferenceMap.NationalMapping = 
				case
				when @UnmappedNationalOnly = 1
				then 0
				else ReferenceMap.NationalMapping
				end







﻿

create procedure [RPT].[AcuteMedRCode3monthrolling]

as


select                                               							
       DistrictNo 							
       ,CasenoteNumber                                          							
       ,PatientForename                                              							
       ,PatientSurname 							
       ,ProviderSpellNo							
       ,SourceEncounterNo							
       ,FirstEpisodeInSpellIndicator							
       ,AdmissionType                                            							
		,EpisodeStartTime					
		,EpisodeEndTime  					
		,LengthOfEpisode =                                               					
              datediff(day , EpisodeStartTime, EpisodeEndTime) 							
 		,NationalSpecialtyCode                                                					
		,NationalSpecialty					
		,FinancialYear					
		,PrimaryDiagnosisCode                          					
		,SourceConsultant					
from 							
       APC.Encounter Encounter    							
                                        							
inner join WH.Site                                                   							
on     Site.SourceSiteID = Encounter.SiteID                                              							
                                                       							
inner join WH.Directorate                                                  							
on     Directorate.DirectorateCode = Encounter.StartDirectorateCode                                              							
                							
inner join WH.Specialty							
on	Specialty.SourceSpecialtyID = Encounter.SpecialtyId						
                                                       							
inner join WH.Ward                                                   							
on     Ward.SourceWardID = Encounter.StartWardID                                                							
    							
inner join WH.Calendar                                                     							
on     Calendar.DateID = Encounter.DischargeDateID                                              							
    							
inner join APC.AdmissionMethod                                                 							
on      AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID							
							
inner join WH.Consultant                                                   							
on     Consultant.SourceConsultantID = Encounter.ConsultantID 							
                                            							
where                                                  							
       DischargeDateID is not null                                              							
		and     EpisodeEndTime >= dateadd(mm,datediff(mm,0,getdate())-3,0)           					
       and    Directorate.Division = 'Acute Medical'  							
		and  PrimaryDiagnosisCode like 'R%'					
		and datediff(day , EpisodeStartTime, EpisodeEndTime)  > 5					


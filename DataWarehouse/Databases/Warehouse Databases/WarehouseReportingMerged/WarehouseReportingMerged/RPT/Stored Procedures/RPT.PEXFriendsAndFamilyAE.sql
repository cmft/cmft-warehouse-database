﻿
CREATE procedure [RPT].[PEXFriendsAndFamilyAE]

(
	@month datetime = null

)

as

--declare @month datetime = '2013-01-01 00:00:00.000'

Select

*

from

(

select

	Attendances.NationalSiteCode
	,Attendances.NationalSite
	,ArrivalMonth
	,Attendances
	,TargetAttendances
	,Score
	,Cases

from

	(

			select
				   [NationalSiteCode]
				   ,[NationalSite]
				   ,ArrivalMonth = @month
				   ,Attendances = sum(case when dateadd(month, datediff(month,0,[ArrivalDate]),0)=@month then 1 else 0 end)
				   ,TargetAttendances = (sum(case when [ArrivalDate]<@month then 1 else 0 end))/12
			from
				  [AE].[Encounter] Encounter
			inner join
				   [AE].[AttendanceDisposal] AttendanceDisposal
			on
				   Encounter.[AttendanceDisposalID] = AttendanceDisposal.SourceAttendanceDisposalID
			inner join
				   [WH].[Site] [Site]
			on
				   Encounter.[SiteID] = [Site].[SourceSiteID]
			       
			where
				   AttendanceDisposal.NationalAttendanceDisposalCode not in ('01','12','13','14','15')  --admitted to hospital
			and
				   [ArrivalDate] > @month - 365.25
			and
				   floor(datediff(day, DateOfBirth, ArrivalDate) / 365.25) >= 16
			and
				   Reportable = 1
			and
				   ContextCode <> 'CEN||ADAS'
			and
				   [NationalSiteCode] <> 'RW3T1'
			       
			group by
				   [NationalSiteCode]
				   ,[NationalSite]
			       
			) Attendances
       
left outer join

				(		

							select 

									 SiteCode = case 
													when [Location] in ('RW3MR(1)',
																		'RW3MR(2)',
																		'RW3MR(3)',
																		'RW3MR(4)',
																		'MAU',
																		'OMU',
																		'ESTU Receiving Unit (Male)',
																		'ESTU Receiving Unit (Female)'
																		)
 
													then 'RW3MR'
													when [Location] in	('A and E Trafford')
													then 'RW3TR'
													when [Location] in	('MREH Emergency Eye Dept')
													then 'RW3RE'
													when [Location] in	('Emergency Gynae')
													then 'RW3SM'				
													else [Location]
												end
									,Score =
												case
													when [Answer] like ('Don%')	then 6
													when [Answer] in ('Definitely not', 'Extremely unlikely', 'Very unlikely')	then 5
													when [Answer] in ('Unlikely')	then 4
													when [Answer] in ('Neither likely nor unlikely')	then 3
													when [Answer] in ('Maybe','Likely')	then 2
													when [Answer] in ('Definitely yes', 'Extremely Likely', 'Very likely')	then 1
													else 0
												end
									,ResponseMonth = dateadd(mm, datediff(m,0,[CensusTime]),0)
									,Cases

							from
								[PEX].[Response] Response

							inner join
								[PEX].[Answer] Answer
							on	Response.[AnswerID] = Answer.[AnswerID]
								
							inner join
								[PEX].[Location] Location
							on	Response.[LocationID] = Location.[LocationID]
										
							inner join
								[PEX].[Question] Question
							on	Response.[QuestionID] = Question.[QuestionID]

							inner join
								[PEX].[Survey] Survey
							on	Response.[SurveyID] = Survey.[ID]

							where	
								[Question] in	
											(
											  'How likely are you to recommend our A & E department to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend our A&E department to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend our out patient department to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend our ward to friends and family if they needed similar care or treatment?'
											  ,'How likely are you to recommend this department / area to a friend or loved one?'
											  ,'How likely are you to recommend this unit/area to a friend or loved one?'
											  ,'How likely are you to recommend this ward / area to a friend or loved one if they had to come to hospital?'
											  ,'How likely are you to recommend this ward / area to a friend or loved one?'
											  ,'How likely are you to recommend this ward / department to family and friends if they needed similar care or treatment?'
											  ,'How likely is it that you would recommend participating in clinical research to your friends and family?'
											  ,'How likely would are you to recommend this ward / area to a friend or loved one?'
											
											
											)
							and 
								dateadd(month, datediff(month,0,[CensusTime]),0) = @month
								
			) Responses	
							   
			on Attendances.NationalSiteCode = Responses.SiteCode
			and ArrivalMonth = ResponseMonth 
	
			
	) FriendsAndFamilyResponse	
		
pivot (sum(Cases) for Score in ([1],[2],[3],[4],[5],[6])) as ScoreInCategory

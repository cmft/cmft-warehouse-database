﻿




create proc [RPT].[PCCPeriodFCEMatchDrillthrough]
(
 @CasenoteNo varchar(50)
,@PICUAdmissionTime datetime2(0)
,@PICUDischargeTime datetime2(0)
,@NumberOfClosestEpisodes int
)
as

/******************************************************************************************************
Stored procedure :	[RPT].[PCCPeriodFCEMatchDrillthrough]
Description		 :	For data quality report to match paediatric critical care (PCC) periods to APC FCEs.
					This proc was written to correctly structure the data to populate the Gantt chart on
					the drillthrough.

Modification History
====================

Date		Person			Description
=======================================================================================================
12/12/2014	Paul Egan       Initial coding.
*******************************************************************************************************/

declare @RAGAmberThresholdLower int	= 5;	-- Minutes
declare @RAGAmberThresholdUpper int	= 60;	-- Minutes

/*============== Debug only =================================================*/
--declare @CasenoteNo varchar(50) = 'm14/012018' --'M09/001256';
--declare @PICUAdmissionTime datetime2(0) = '20141030 12:07' --'20140408 23:50';
--declare @PICUDischargeTime datetime2(0) = '20141102 20:30' --'20140409 19:00';
--declare @NumberOfClosestEpisodes int = 6;
/*===========================================================================*/

with CTERawData as
(
select
	 CasenoteNo = APCEncounter.CasenoteNumber
	,Category = 'APCEpisode'
	,StartTime = cast(APCEncounter.EpisodeStartTime as datetime2(0))
	,EndTime = cast(APCEncounter.EpisodeEndTime as datetime2(0))
	,StartWard = APCEncounter.StartWardTypeCode
	,EndWard = APCEncounter.EndWardTypeCode
	,FCEConsultant = APCEncounter.ConsultantCode
from
	WarehouseReportingMerged.APC.Encounter APCEncounter
where
	APCEncounter.CasenoteNumber = @CasenoteNo
	
union

select
	CasenoteNo = @CasenoteNo
	,Category = 'PCCPeriod'
	,StartTime = @PICUAdmissionTime
	,EndTime = @PICUDischargeTime
	,StartWard = NULL
	,EndWard = NULL
	,FCEConsultant = NULL
)

,CTEAddCalculations as
(
select *
	,TimeStartDifferenceMinute = coalesce(datediff(minute, @PICUAdmissionTime, StartTime), 99999)
	,TimeEndDifferenceMinute = coalesce(datediff(minute, @PICUDischargeTime, EndTime), 99999)
	,ClosestEpisodeMatch =
		/* Check which episode is closer - the end times or the start times, and use the closest one to match */
		row_number() over(
			order by 
				case
					when abs(coalesce(datediff(minute, @PICUAdmissionTime, StartTime), 99999)) <= abs(coalesce(datediff(minute, @PICUDischargeTime, EndTime), 99999))
					then abs(coalesce(datediff(minute, @PICUAdmissionTime, StartTime), 99999))
					else abs(coalesce(datediff(minute, @PICUDischargeTime, EndTime), 99999))
				end)
	,PICUAdmissionTime = @PICUAdmissionTime
	,RAGRatingStart = 
		case
			when abs(coalesce(datediff(minute, @PICUAdmissionTime, StartTime), 99999)) >= @RAGAmberThresholdUpper then 1	-- Red
			when abs(coalesce(datediff(minute, @PICUAdmissionTime, StartTime), 99999)) >= @RAGAmberThresholdLower then 2	-- Amber
			else 3 -- Green
		end
	,RAGRatingEnd = 
		case
			when abs(coalesce(datediff(minute, @PICUDischargeTime, EndTime), 99999)) >= @RAGAmberThresholdUpper then 1	-- Red
			when abs(coalesce(datediff(minute, @PICUDischargeTime, EndTime), 99999)) >= @RAGAmberThresholdLower then 2	-- Amber
			else 3 -- Green
		end


from
	CTERawData
)

select *
from
	CTEAddCalculations
where 
	ClosestEpisodeMatch <= @NumberOfClosestEpisodes + 1  -- + 1 to account for the PICU exact match 
;




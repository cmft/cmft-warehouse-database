﻿


	--Created 21 May 2012
	--Created by Gareth Cunnah
	--To extract Trafford Outpatient Data for Activity Report
	
	
	--25 June 2012 Added insert into table TGHInformation temp fix for Ducia
	--18 Jul 2012 Removed exclusion of 840 as per instruction from TGH Information Team


	CREATE proc [RPT].[GetTraffordOPActivity]
	
		
	 @startdate datetime = null
	,@enddate datetime = null
	,@Duplicate varchar (1)	= 'N'
	
	as
	
	declare @SetStartDate datetime = coalesce(@StartDate,'02 April 2012') --hard coded as only going to be used for a short time
	
		declare @SetEndDate datetime =  coalesce(@enddate,
			--(select DATEADD(wk, DATEDIFF(wk,7,GETDATE()), 7) )	
				(GETDATE())
				)
	
	declare @lastMonday Datetime = coalesce(@SetEndDate - 7,(select DATEADD(wk, DATEDIFF(wk,7,GETDATE()), 0) ) )
	
	drop table TGHInformation.dbo.TraffordOPActivity
	
	
	select * 
	
	into TGHInformation.dbo.TraffordOPActivity

	
	from 
	
		
	(
	
	Select
			
		 op_act_hosp
		,op_act_int_no	
		,ApptType	
		,ApptDate	
		,AttendStat	
		,InternalNo	
		,RefSource	
		,Consultant	
		,Hospital	
		,Specialty	
		,Div	
		,N_F	
		,spec_desc	
		,diag	
		,week_no	
		,PrimProc	
		,can_by	
		,can_date	
		,category	
		,dir
		,wk	
		,appdate
		,Duplicate = 
			Case 
			when Duplicate <> 1 then 'Duplicate Removed' else 'Not Duplicate' end 
		,HRGCode
		,DerivedNF =
			case
			when HRGCode in
			(
			'AB05Z','AB06Z','BZ04Z','BZ07A','BZ07B','BZ10A','BZ10B',
			'BZ18Z','BZ19Z','BZ22Z','BZ23Z','CZ01T','CZ01Y','CZ02T','CZ02Y',
			'CZ08T','CZ08Y','CZ12Y','CZ13Y','CZ30Y','CZ31Y','CZ32U','CZ32Y',
			'CZ33Y','CZ34U','CZ34Y','CZ35U','CZ35Y','CZ36Y','CZ37Y','CZ38U',
			'CZ38Y','CZ39U','CZ40Y','CZ41Y','CZ42Y','DZ31Z','DZ32Z','EA45Z',	
			'EA47Z','FZ23Z','FZ50Z','FZ51Z','FZ52Z','FZ54Z','FZ55Z','FZ57Z',
			'FZ59Z','FZ60Z','FZ61Z','JA15C','JC03C','JC04C','JC05C','JC06C',
			'JC07Z','JC09Z','JC14Z','JC15Z','JC18Z','JC19D','JC20Z','JC29Z',
			'JC32Z','LB14E','LB15E','LB17Z','LB27Z','LB42Z','MA10Z','MA21Z',
			'MA23Z','MA25Z','NZ05C','NZ05D','QZ05B','QZ14B','SA13A','SA13B'

			) then 'N' else N_F end
			 	
			,LastWeeksCases	
			,SourceAdministrativeCategoryCode	
			
	from
	
	(
	
	SELECT 
	
	 --  MatchID = null 
	   [op_act_hosp] = DistrictNo
      ,[op_act_int_no] = SourceUniqueID
      --,[EpisodeNo] = null
      --,[Doctor] = null
      --,[CasenoteNo] = null
      --,[Clinic] = null
      ,[ApptType] = SourceAppointmentType
      ,[ApptDate] = DATEADD(dd, 0, DATEDIFF(dd, 0, AppointmentTime))
      ,[AttendStat] = 'ATT'
      ,[InternalNo] = null
      ,[RefSource] = null
      ,[Consultant] = MainSpecialtyCode + ' ' + SourceConsultant
      ,[Hospital] = null
      

		--Set to macth current way TGH data works      
      
      ,[Specialty] = NationalSpecialtyCode
					--SW advised should be removed 23/05/2012 as not in script for Slam 
					--case
					--when NationalSpecialtyCode = 430 then 300
					--when NationalSpecialtyCode = 104 then 100 --SW advised should be removed 23/05/2012
					--when NationalSpecialtyCode = 191 then 190
					--else NationalSpecialtyCode end
      
      
      ,[Div] = null
      ,[N_F] =  Case 
				when FirstAttendance.NationalFirstAttendanceCode = 1 then 'N'
				when FirstAttendance.NationalFirstAttendanceCode = 2 then 'F'
				else null end
      ,[spec_desc] = NationalSpecialtyCode + ' ' + NationalSpecialty
      
					--SW advised should be removed 23/05/2012 as not in script for Slam
					--case
					--when NationalSpecialtyCode = 430 then '300' + ' ' + 'GENERAL MEDICINE'
					----when NationalSpecialtyCode = 104 then '100' + ' ' + 'GENERAL SURGERY'  --SW advised should be removed 23/05/2012
					--when NationalSpecialtyCode = 191 then '190' + ' ' + 'ANAESTHETICS'
					--else NationalSpecialtyCode + ' ' + NationalSpecialty end
      
      
     --      NationalSpecialtyCode + ' ' + NationalSpecialty
     
      ,[diag] = null
      ,[week_no] = null
      ,[PrimProc] = null
      ,[can_by] = null
      ,[can_date] = null
      ,[category] = null
      ,[dir] = null
      ,[wk] = null
      ,[appdate] = AppointmentTime
      
	  ,Duplicate = row_number() over (
			partition by Districtno, DATEADD(dd, 0, DATEDIFF(dd, 0, AppointmentTime)) ,NationalSpecialtyCode
			order by Districtno ,
			
			--Procedure Codes
			case when HRGCODE in 
			(
			'AB05Z','AB06Z','BZ04Z','BZ07A','BZ07B','BZ10A','BZ10B',
			'BZ18Z','BZ19Z','BZ22Z','BZ23Z','CZ01T','CZ01Y','CZ02T','CZ02Y',
			'CZ08T','CZ08Y','CZ12Y','CZ13Y','CZ30Y','CZ31Y','CZ32U','CZ32Y',
			'CZ33Y','CZ34U','CZ34Y','CZ35U','CZ35Y','CZ36Y','CZ37Y','CZ38U',
			'CZ38Y','CZ39U','CZ40Y','CZ41Y','CZ42Y','DZ31Z','DZ32Z','EA45Z',	
			'EA47Z','FZ23Z','FZ50Z','FZ51Z','FZ52Z','FZ54Z','FZ55Z','FZ57Z',
			'FZ59Z','FZ60Z','FZ61Z','JA15C','JC03C','JC04C','JC05C','JC06C',
			'JC07Z','JC09Z','JC14Z','JC15Z','JC18Z','JC19D','JC20Z','JC29Z',
			'JC32Z','LB14E','LB15E','LB17Z','LB27Z','LB42Z','MA10Z','MA21Z',
			'MA23Z','MA25Z','NZ05C','NZ05D','QZ05B','QZ14B','SA13A','SA13B'

			)
			 then 1 else 0 end desc,
			 FirstAttendance.NationalFirstAttendanceCode 
			 --Ensure SPN Nurse Appointment Slected last
			,case when [LocalAppointmentType] like '%(SpN)%' then 1 else 0 end asc
	
				) 
		,HRGCode	
		,LastWeeksCases =
					case
					when DATEADD(dd, 0, DATEDIFF(dd, 0, Encounter.AppointmentTime)) >= @lastMonday then 1
					else 0 end
					
			,SourceAdministrativeCategoryCode

	
  FROM OP.Encounter Encounter
  
	inner join OP.FirstAttendance FirstAttendance
	on FirstAttendance.SourceFirstAttendanceID = Encounter.FirstAttendanceID  
	and FirstAttendance.SourceContextCode = 'TRA||UG'
	
	inner join WH.Specialty Specialty
	on Specialty.SourceSpecialtyID = Encounter.TreatmentFunctionID
	and Specialty.SourceContextCode = 'TRA||UG'
	
	
	--AppointmentStatusCode may have to be AppointmentStatusID in the future
	--inner join OP.AppointmentStatus AppointmentStatus
	--on AppointmentStatus.SourceAppointmentStatusCode = Encounter.AppointmentStatusCode
	--and AppointmentStatus.SourceContextCode = 'TRA||UG'

	inner join OP.AppointmentType AppointmentType
	on AppointmentType.SourceAppointmentTypeID = Encounter.AppointmentTypeID
	and AppointmentType.SourceContextCode = 'TRA||UG'
	
	left join WH.Consultant Consultant
	on Consultant.SourceConsultantID = Encounter.ConsultantID
	and Consultant.SourceContextCode = 'TRA||UG'
	
		left join WH.AdministrativeCategory AdministrativeCategory
		on AdministrativeCategory.SourceAdministrativeCategoryID = Encounter.AdminCategoryID
						
  
  		where 
  		
  		--TGH Data Only
  		Encounter.ContextCode = 'TRA||UG'
		
		
		and DATEADD(dd, 0, DATEDIFF(dd, 0, Encounter.AppointmentTime)) between @SetStartDate and @SetEndDate
		
		--Activity to include
		--First attendance face to face 
		--CMFT Patient at Trafford First attendance face to face
		--CMFT Patient at Trafford  Follow-up attendance face to face
		--Follow-up attendance face to face
		
		and FirstAttendance.SourceFirstAttendanceCode in (1,2,12,11)
		
		--Activity to exclude
		
		--Excluded Specialty
		-- 143 ORTHODONTICS  (Bridgewater Activity)
		-- 141 RESTORATIVE DENTISTRY (Bridgewater Activity)
		-- 840 AUDIOLOGY
		-- 711 CHILD and ADOLESCENT PSYCHIATRY (Needs to be confirmed With TGH Information)
		-- 960 AHP not a Treatment Function (Hearing Assessment)
		
		and isnumeric(Specialty.NationalSpecialtyCode) = 1
		
		and Specialty.NationalSpecialtyCode not in (141,143,711,960)


		--Include Visit Closed (TRA||UG:V) only
		
		and Encounter.AppointmentStatusCode = 'V'
	
		--Exclude Appointment Types
		--New Green Card (OP appointment whilst Inpatient)
		--Follow Up Green Card (OP appointment whilst Inpatient)
		--Fol Up Ward Visit (SpN)-TRAFF (OP appointment whilst Inpatient)
		--New Ward Visit (SpN)-TRAFF (OP appointment whilst Inpatient)
		--Glucose Tolerance Test
		--Audiolgy Tinnitus

		and [LocalAppointmentType] not like ('%Green Card%')
		and [LocalAppointmentType] not like ('%Glucose Tolerance Test%')
		
		and [LocalAppointmentType] not like ('Fol Up Ward Visit (SpN)-TRAFF%')
		and [LocalAppointmentType] not like ('New Ward Visit (SpN)-TRAFF%')

		and [LocalAppointmentType] not like ('%Tinnitus Review - Adult FU%')
		and [LocalAppointmentType] not like ('%Tinnitus Assessment - Adult New%')


       

			
		

	)activity
	
	where
	
	(
	@Duplicate = 'N'
	
	and Duplicate = 1
	or
	
	@Duplicate = 'Y'
	
	and 
	
	Duplicate <> 1
	
	or 
	
	@Duplicate is null
		
	)
	
	
	union all
		
	
		--A&E Clinic 

			select


					 op_act_hosp = DistrictNo
					,op_act_int_no	= SourceUniqueID
					,ApptType	= 'A&E Clinic'
					,ApptDate	= ArrivalDate
					,AttendStat	= 'ATT'
					,InternalNo	= null
					,RefSource	= null
					,Consultant	= null
					,Hospital = null	
					,Specialty = '180'	
					,Div = null	
					,N_F = 'N'	
					,spec_desc = '180 ACCIDENT & EMERGENCY'	
					,diag = null	
					,week_no = null	
					,PrimProc = null	
					,can_by	= null
					,can_date = null	
					,category = null	
					,dir = null
					,wk	= null
					,appdate = ArrivalTime
					,Duplicate = 'Not Duplicate'
					,HRGCode = null
					,DerivedNF = null
					,LastWeeksCases =
					case
					when DATEADD(dd, 0, DATEDIFF(dd, 0, ArrivalDate)) >= @lastMonday  then 1
					else 0 end	
					,SourceAdministrativeCategoryCode = null




			 from AE.Encounter Encounter

			inner join AE.AttendanceCategory AttendanceCategory
			on AttendanceCategory.SourceAttendanceCategoryID = Encounter.AttendanceCategoryID

			inner join WH.Site [Site]
			on [Site].SourceSiteID = Encounter.siteid

			where site.NationalSiteCode in 
			(
			 'RW3TR'
			,'RW3T1'
			)

			and AttendanceCategory.SourceAttendanceCategoryCode = 2 --Clinic Attender 

			and DATEADD(dd, 0, DATEDIFF(dd, 0, ArrivalDate))  between @SetStartDate and @SetEndDate

		)x
		
print @SetStartDate
print @SetEndDate
print @lastMonday

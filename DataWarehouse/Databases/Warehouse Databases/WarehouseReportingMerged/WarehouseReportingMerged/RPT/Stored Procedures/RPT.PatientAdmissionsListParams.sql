﻿


CREATE Procedure [RPT].[PatientAdmissionsListParams] 
(
	 @StartDate					DATE
	,@EndDate					DATE
	,@ParamType					CHAR(1)
	,@Division					VARCHAR(MAX) = NULL
)
AS
	SELECT 
		 StartDirectorateCode
		,SpecialtyID
		,ContextCode
	INTO #T
	FROM
		APC.Encounter E
	WHERE
				E.AdmissionDate							BETWEEN @Startdate AND @EndDate
			AND E.GlobalEpisodeNo						= 1
			AND E.SpecialtyCode							NOT IN ('BOWL','GYN1','PrivPt')
			AND (
						E.AdminCategoryCode				NOT IN ('2-PAY','02','PrivPt') 
					OR  E.AdminCategoryCode				IS NULL
				)	
			AND E.PatientClassificationCode				IN ('1','2')
 ---------
 
	SELECT DISTINCT
		 Code		= CASE 
						WHEN @ParamType = 'S' THEN SPEC.NationalSpecialtyCode
						ELSE Division 
					  END
		,Label		= CASE 
						WHEN @ParamType = 'S' THEN SPEC.NationalSpecialty
						ELSE Division 
					  END
	FROM	
		#T E
		
		INNER JOIN WH.Specialty SPEC 
			ON		E.SpecialtyID				= SPEC.SourceSpecialtyID
				
		INNER JOIN WH.Directorate DIR
			ON		E.StartDirectorateCode		= DIR.DirectorateCode
				
	WHERE
			DIR.Division					IN (SELECT Item FROM fn_ListToTable(@Division, ','))	
		OR	@Division						IS NULL
			
	ORDER BY 
		Label



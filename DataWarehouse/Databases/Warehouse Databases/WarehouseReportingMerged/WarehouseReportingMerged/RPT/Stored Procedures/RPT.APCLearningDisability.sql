﻿


CREATE Procedure [RPT].[APCLearningDisability] --'20140401','20150331'
	(
	@AdmissionDischarge char 
	,@PeriodStartDate datetime 
	,@PeriodEndDate datetime 
	,@AdmissionType varchar(max) = null
	,@NationalSpecialtyCode varchar(max) = null
	,@WardCode varchar(max) = null
	,@NationalPatientClassificationCode varchar(max) = null
	--,@GPPracticeCode varchar(max) = null
	)
as

declare @LocalStart datetime = @PeriodStartDate
declare @LocalEnd datetime = (@PeriodEndDate + '23:59')

select 
	Encounter.GlobalProviderSpellNo
	,AdmissionDate
	,DischargeDate
	,AdmissionWeek = AdmissionCalendar.WeekNo
	,DischargeWeek = DischargeCalendar.WeekNo
	,AdmissionMonth = left(convert(nvarchar, AdmissionDate,120),7)
	,DischargeMonth = left(convert(nvarchar, DischargeDate,120),7)
	,HospitalSite = 
		case 
			when left (Encounter.ContextCode,3) = 'CEN' then 'Central'
			else 'Trafford'
		end
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.PatientTitle 
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Postcode = Encounter.Postcode
	,Gender = Sex.NationalSex
	,Ward = 
		coalesce(
				Ward.LocalWard
				,Encounter.EndWardTypeCode
				,Encounter.StartWardTypeCode
				)
	,Encounter.CasenoteNumber
	,Encounter.DistrictNo
	,PatientClassification = NationalPatientClassification
	,AdmissionType = 
		case 
			when AdmissionType = 'Other Non-Elective' then 'Emergency'
			else AdmissionType
		end
	,LengthOfStay = DATEDIFF(day,AdmissionDate,DischargeDate)
	,Specialty = 
		coalesce(
				(Specialty.NationalSpecialtyCode + ' - ' + Specialty.NationalSpecialty)
				,Encounter.SpecialtyCode 
				)
	,Consultant = 
		coalesce(
				Consultant.SourceConsultant
				,Encounter.ConsultantCode
				)
	,EpisodicGPPracticeCode = 
			coalesce (
				GPPractice.GpPracticeCode + ' - ' + GPPractice.GPPracticeName
				,'N/A'
				)
				
	,APCDiagnosis = 
		case
			when exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.APC.BaseEncounterBaseEncounterLDDiagnosis
				where
					BaseEncounterBaseEncounterLDDiagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
				)
			then 1
			else 0
		end
	,Bedman = 
		case
			when exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.APC.BaseEncounterBaseLearningDisability
				inner join WarehouseOLAPMergedV2.APC.BaseLearningDisability
				on BaseEncounterBaseLearningDisability.MergeLearningDisabilityRecno = BaseLearningDisability.MergeLearningDisabilityRecno
				where 
					BaseLearningDisability.KnownLearningDisability = 1
				and BaseEncounterBaseLearningDisability.MergeEncounterRecno = Encounter.MergeEncounterRecno
				)
			then 1
			else 0
		end
	,SpecialRegister = 
		case
			when exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.APC.BaseEncounterBaseSpecialRegister

				inner join WarehouseOLAPMergedV2.Patient.BaseSpecialRegister
				on BaseEncounterBaseSpecialRegister.MergeSpecialRegisterRecno = BaseSpecialRegister.MergeSpecialRegisterRecNo

				inner join WarehouseOLAPMergedV2.Patient.BaseSpecialRegisterReference
				on BaseSpecialRegister.MergeSpecialRegisterRecNo = BaseSpecialRegisterReference.MergeSpecialRegisterRecno
				
				inner join WarehouseOLAPMergedV2.Patient.SpecialRegister
				on BaseSpecialRegisterReference.SpecialRegisterID = SpecialRegister.SourceSpecialRegisterID

				where 
					LocalSpecialRegister = 'Learning Disability'
				and BaseEncounterBaseSpecialRegister.MergeEncounterRecno = Encounter.MergeEncounterRecno
				)
			then 1
			else 0
		end
from
	APC.Encounter
	
inner join WH.Calendar DischargeCalendar
on Encounter.DischargeDate = DischargeCalendar.TheDate

inner join WH.Calendar AdmissionCalendar
on Encounter.AdmissionDate = AdmissionCalendar.TheDate

left join APC.PatientClassification
on Encounter.PatientClassificationID = PatientClassification.SourcePatientClassificationID

left join WH.Sex
on Sex.SourceSexID = Encounter.SexID
	
left join APC.AdmissionMethod
on Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID

left join WH.Specialty
on Encounter.SpecialtyID = Specialty.SourceSpecialtyID

left join WH.Consultant
on Encounter.ConsultantID = Consultant.SourceConsultantID

left join WH.Ward
on coalesce(
			Encounter.EndWardID
			,Encounter.StartWardID
			) 
		= Ward.SourceWardID

left join WH.GPPractice
on coalesce(Encounter.RegisteredGPPracticeCode,'N/A') = GPPractice.GpPracticeCode

where 
	Case
		when @AdmissionDischarge = 'D' then DischargeDate 
		else AdmissionDate 
	end
		between @LocalStart and @LocalEnd
and exists
	(
	select
		1
	from
		(
		select
			GlobalProviderSpellNo
			,GlobalEpisodeNo
		from
			WarehouseOLAPMergedV2.APC.BaseEncounterBaseEncounterLDDiagnosis
		
		inner join APC.Encounter
		on BaseEncounterBaseEncounterLDDiagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
		
		union
		
		select
			GlobalProviderSpellNo
			,GlobalEpisodeNo
		from
			WarehouseOLAPMergedV2.APC.BaseEncounterBaseLearningDisability
		
		inner join WarehouseOLAPMergedV2.APC.BaseLearningDisability
		on BaseEncounterBaseLearningDisability.MergeLearningDisabilityRecno = BaseLearningDisability.MergeLearningDisabilityRecno
		
		inner join APC.Encounter
		on BaseEncounterBaseLearningDisability.MergeEncounterRecno = Encounter.MergeEncounterRecno
		
		where 
			BaseLearningDisability.KnownLearningDisability = 1
		
		union
		
		select
			GlobalProviderSpellNo
			,GlobalEpisodeNo
		from
			WarehouseOLAPMergedV2.APC.BaseEncounterBaseSpecialRegister

		inner join WarehouseOLAPMergedV2.Patient.BaseSpecialRegister
		on BaseEncounterBaseSpecialRegister.MergeSpecialRegisterRecno = BaseSpecialRegister.MergeSpecialRegisterRecNo

		inner join WarehouseOLAPMergedV2.Patient.BaseSpecialRegisterReference
		on BaseSpecialRegister.MergeSpecialRegisterRecNo = BaseSpecialRegisterReference.MergeSpecialRegisterRecno
		
		inner join WarehouseOLAPMergedV2.Patient.SpecialRegister
		on BaseSpecialRegisterReference.SpecialRegisterID = SpecialRegister.SourceSpecialRegisterID
		
		inner join APC.Encounter
		on BaseEncounterBaseSpecialRegister.MergeEncounterRecno = Encounter.MergeEncounterRecno
		
		where 
			LocalSpecialRegister = 'Learning Disability'
		)	A
	where
		A.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
	)
and
	not exists
		(
		select 1
		from APC.Encounter Latest
		where Latest.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
		and Latest.GlobalEpisodeNo > Encounter.GlobalEpisodeNo
		)			
and
	(
		case 
			when AdmissionType = 'Other Non-Elective' then 'Emergency'
			else AdmissionType
		end 
			in (Select value from WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@AdmissionType,','))
	or
		@AdmissionType is null
	)
and
	(
		Specialty.NationalSpecialtyCode 
			in (Select value from WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,','))
	or
		@NationalSpecialtyCode is null
	)
and
	(
		Ward.LocalWardCode
				in (Select value from WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@WardCode,','))
	or
		@WardCode is null
	)
and
	(
		PatientClassification.NationalPatientClassificationCode
			in (Select value from WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@NationalPatientClassificationCode,','))
	or
		@NationalPatientClassificationCode is null
	)

		
--group by
--	Encounter.GlobalProviderSpellNo
--	,AdmissionCalendar.WeekNo
--	,DischargeCalendar.WeekNo
--	,left(convert(nvarchar, AdmissionDate,120),7)
--	,left(convert(nvarchar, DischargeDate,120),7)
--	,	case 
--			when left (Encounter.ContextCode,3) = 'CEN' then 'Central'
--			else 'Trafford'
--		end
--	,Encounter.AdmissionDate
--	,Encounter.AdmissionTime
--	,Encounter.DischargeDate
--	,Encounter.DischargeTime
--	,Encounter.PatientTitle 
--	,Encounter.PatientForename
--	,Encounter.PatientSurname
--	,Encounter.DateOfBirth
--	,Encounter.Postcode
--	,Sex.NationalSex
--	,NationalPatientClassification
--	,DATEDIFF(day,AdmissionDate,DischargeDate)
--	,coalesce(
--				Ward.LocalWard
--				,Encounter.EndWardTypeCode
--				,Encounter.StartWardTypeCode
--				)
--	,Encounter.CasenoteNumber
--	,Encounter.DistrictNo
--	,	case 
--			when AdmissionType = 'Other Non-Elective' then 'Emergency'
--			else AdmissionType
--		end
--	--,ExpectedLoS
--	,	coalesce(
--				(Specialty.NationalSpecialtyCode + ' - ' + Specialty.NationalSpecialty)
--				,Encounter.SpecialtyCode 
--				)
--	,	coalesce(
--				Consultant.SourceConsultant
--				,Encounter.ConsultantCode
--				)
--	,	coalesce (
--				GPPractice.GpPracticeCode + ' - ' + GPPractice.GPPracticeName
--				,'N/A'
--				)
				
--	,	case
--			when Diagnosis.SourceSystemID IS not null then 1
--			else 0
--		end
--	,	case
--			when Bedman.SourceSystemID IS not null then 1
--			else 0
--		end
--	,	case
--			when SpecialRegister.SourceSystemID IS not null then 1
--			else 0
--		end
		



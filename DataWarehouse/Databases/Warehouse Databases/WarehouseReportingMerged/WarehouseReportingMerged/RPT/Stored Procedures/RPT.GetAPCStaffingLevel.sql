﻿

CREATE proc 
       [RPT].[GetAPCStaffingLevel]              --'2014-04-27','SGY','CENW8_15',1 
       (
       @WeekEnding as date
       ,@Division as varchar(10)
       ,@Ward as varchar(20)
       ,@ShiftType as int         
       )

as

--declare @WeekEnding as date     = '2014-05-25'
--declare     @Division as varchar(10) = 'REH'
--declare @Ward as varchar(20) = null --'CENW8_15'
--declare @ShiftType as int = 1

select
       Ward.DivisionCode
       ,Ward.Division
       ,Ward.WardCode
       ,Ward.Ward
       ,SourceDateSelected = CensusDate
       ,DateSelected = TheDate
       ,DaySelected = DayOfWeek
       ,WeekEndingSelected = LastDayOfWeek
       ,ShiftID = Shift.ID
       ,Shift = case when Shift.Shift  = 'DHours' then 'NHours'             --Label doesnt marry up to the what they are actually entering
                                  when Shift.Shift  = 'NHours' then 'DHours'
                                  else Shift.Shift
                                  end
       ,RegisteredNursesPlanned = coalesce(RegisteredNursePlan,0)
       ,RegisteredNursesActual = coalesce(RegisteredNurseActual,0)
       ,RegisteredNursesNotMetPlanned =  case 
                                                                     when   RegisteredNurseActual < RegisteredNursePlan
                                                                     then   1
                                                                     else   0
                                                                     end
       ,RegisteredNursesVariance = coalesce(RegisteredNurseActual - RegisteredNursePlan,0)  
       ,APsAndCSWPlanned = coalesce(NonRegisteredNursePlan,0)
       ,APsAndCSWActual = coalesce(NonRegisteredNurseActual,0)
       ,APsAndCSWNotMetPlanned =  case 
                                                                     when   NonRegisteredNurseActual < NonRegisteredNursePlan
                                                                     then   1
                                                                     else   0
                                                                     end
       ,APsAndCSWVariance = coalesce(NonRegisteredNurseActual - NonRegisteredNursePlan,0)
       ,Comments
       ,SeniorComments
from
       APC.StaffingLevel StaffingLevel

inner join
       Warehouse.IPR.ShiftBase Shift
on     Shift.ID = StaffingLevel.ShiftID
and    Shift.ShiftType = @ShiftType

right outer join
       (
                    select
                     WardCode
                     ,Ward
                     ,Division.DivisionCode
                     ,Division
                     ,TheDate
                     ,DayOfWeek
                     ,LastDayOfWeek
              from
                     Warehouse.IPR.WardBase
              inner join
                     Warehouse.IPR.DivisionBase Division
              on     Division.DivisionCode = WardBase.DivisionCode

                       cross join
                     WarehouseReportingMerged.WH.Calendar
              where  
                     LastDayOfWeek = @WeekEnding
              and    WardBase.DivisionCode = @Division
              and    (      @Ward in (WardBase.WardCode)
                     or     @Ward is null
                     ) 
       ) Ward
on     Ward.WardCode = StaffingLevel.WardCode
and    Ward.TheDate = StaffingLevel.CensusDate


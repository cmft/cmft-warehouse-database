﻿/****************************************************************************************
	Stored procedure : [RPT].[OutOfHoursWardTransfers]
	Description		 : SSRS proc for the Visual Studio report
	
	SSRS Merged >> OutOfHoursWardTransfers.rdl
	
	Measures
	 

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	12.04.2016	M Hodson		Intial Coding
	
*****************************************************************************************/

CREATE PROCEDURE [RPT].[OutOfHoursWardTransfers]
	@param1 int = 0,
	@param2 int
AS
	
	SELECT @param1
--		 ws.ProviderSpellNo
--		,Division
--		,ws.[SiteCode]
--		,trasnward = ws.[WardCode]
		
--		,transward = ward.[LocalWard]
--		,transwarddesc = ward.[SourceWard]
		
--		,transcons = apc.ConsultantCode
--		,tarnsspec = apc.SpecialtyCode
--		,toward.WardCode
--		,wardto.[SourceWard]
--		,recievecons = rapc.ConsultantCode
--		,recievespec = rapc.SpecialtyCode
		
--		,apc.PatientCategoryCode
--		,[EndActivityCode] 
--		 = case when ws.EndActivityCode is null and apc.DischargeTime  = ws.endtime then 'DSC' 
		
--		    else ws.EndActivityCode end
--		,Movedate = ws.EndDate 
--		,Dischargehour = DATEPART(hour,ws.EndTime)
--		,movedatetime = ws.endtime
--		,reportingdate = case when Convert (varchar(8),(ws.EndTime),108) >= '21:00:00' then DATEADD(D,1,ws.EndDate) else ws.[EndDate] end
--		,MoveOuts = COUNT(1)

--	FROM 
--		APC.WardStay WS

--		LEFT JOIN WH.Directorate div
--		on ws.DirectorateCode = div.DirectorateCode
		
--		LEFT JOIN APC.Ward ward
--		on ward.SourceWardID = ws.WardID
		
--		LEFT JOIN 
--		on toward.ProviderSpellNo = ws.ProviderSpellNo
--		and ws.EndTime = toward.starttime
		
--		LEFT JOIN APC.Ward wardto
--		on wardto.SourceWardID = toward.WardID
		
		
--		INNER JOIN apc.Encounter apc
--		on apc.ProviderSpellNo = ws.ProviderSpellNo
--		and ws.EndTime between apc.EpisodeStartTime and coalesce (apc.episodeenddate,getdate())
		
--		LEFT JOIN apc.Encounter rapc
--		on rapc.ProviderSpellNo = toward.ProviderSpellNo
--		and toward.starttime between rapc.EpisodeStartTime and coalesce (rapc.episodeenddate,getdate())


--where   Case






--group by
--ws.ProviderSpellNo
--,Division
--,ws.[SiteCode]
--,ws.[WardCode]

--,ward.[LocalWard]
--,ward.[SourceWard]

--,apc.ConsultantCode
--,apc.SpecialtyCode
--,toward.WardCode
--,wardto.[SourceWard]
--,rapc.ConsultantCode
--,rapc.SpecialtyCode
--,apc.PatientCategoryCode
--,case when ws.EndActivityCode is null and apc.DischargeTime  = ws.endtime then 'DSC' 

--    else ws.EndActivityCode end
--,ws.EndDate 
--,DATEPART(hour,ws.EndTime)
--,ws.endtime
--,case when Convert (varchar(8),(ws.EndTime),108) >= '21:00:00' then DATEADD(D,1,ws.EndDate) else ws.[EndDate] end



﻿
CREATE PROCEDURE [RPT].[AESelfHarmParamCCG]
(
	 @AttendanceFromDate	DATE
	,@AttendanceToDate		DATE
)
AS
	/******************************************************************************
	**  Name: RPT.AESelfHarmParamCCG
	**  Purpose: To return a CCG Parameter dataset for the RPT.AESelfHarm report
	**  
	**           This proc returns a list of CCG's for A&E attendances between the
	**			 input parameter dates, but uses the filtering logic of attendances
	**           included in the RPT.AESelfHarm stored procedure.
	**
	**           This ensures a change in the filter logic is only applied 
	**           in the RPT.AESelfHarm proc.
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------   
	** 02.01.14   MH          Initial Coding 
	******************************************************************************/
	
DECLARE @SQL NVARCHAR(2000) = ''
DECLARE @SERVER VARCHAR(100) = @@SERVERNAME
DECLARE @DB VARCHAR(100) = DB_NAME()

-- Get a distinct list of CCG's derived from the logic contained in the RPT.AESelfHarm proc
-- Use OPENROWSET to get a handle on the data set returned by the proc
SET @SQL = 'SELECT DISTINCT CommissionerCode, CommissionerName ' 
			+ 'FROM OPENROWSET(''SQLNCLI'',''Data Source='
			+ @SERVER
			+ ';Trusted_Connection=yes;'''
			+ ',''EXEC '
			+ @DB
			+ '.RPT.AESelfHarm @AttendanceFromDate = ' 
			+ '"' + CAST(@AttendanceFromDate AS VARCHAR(20)) + '"'
			+ ',@AttendanceToDate = ' 
			+ '"' + CAST(@AttendanceToDate AS VARCHAR(20)) + '"'')'
			+ ' ORDER BY CommissionerName'

EXEC(@SQL)


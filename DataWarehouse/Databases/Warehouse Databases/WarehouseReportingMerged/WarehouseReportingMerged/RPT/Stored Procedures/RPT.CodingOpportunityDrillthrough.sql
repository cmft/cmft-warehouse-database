﻿


CREATE PROCEDURE [RPT].[CodingOpportunityDrillthrough]
(
	 @Type							VARCHAR(10)
	,@Period						VARCHAR(20)		= NULL
	,@Division						VARCHAR(1000)	= NULL
	,@EpisodeNo						INT				= NULL
	,@PatientClassificationCode		VARCHAR(100)	= NULL
	,@AdmissionMethodCode			VARCHAR(100)	= NULL
)
AS
/****************************************************************************************
	Stored procedure : RPT.CodingOpportunityDrillthrough
	Description		 : SSRS proc for the Visual Studio report
	
	Reports >> DQ >> Coding Opportunity Drillthrough
	
	Measures
	 Analysis of FCE Primary Diagnoses R Codes

	Modification History
	====================
	
	Date      Person     Description
	====================================================================================
	23.01.14    MH       Intial Coding
	26.02.14    MH       Included Casenote number
	27.02.14    MH       Include episode start ward
*****************************************************************************************/

	-- declare @Type			VARCHAR(10) = 'ROP'
	--,@Period		VARCHAR(20)	= 'May 2013'
	--,@Division		VARCHAR(20)	= 'Medicine & Community'
	--,@EpisodeNo		INT			= 1

	SELECT
		 SpellNumber					= FCE1.GlobalProviderSpellNo
		,DischargeDate					= FCE1.DischargeDate
		,Division						= DIR.Division
		,PatientSurname					= FCE1.PatientSurname
		,PatientForename				= FCE1.PatientForename
		,DOB							= FCE1.DateOfBirth
		,Sex							= FCE1.SexCode
		,HospitalNumber					= FCE1.DistrictNo
		,NHSNumber						= FCE1.NHSNumber
		,AdmissionDate					= FCE1.AdmissionDate
		,SpellLOS						= FCE1.LOS
		,AdmissionMethod				= AM.NationalAdmissionMethod
		,CasenoteNumber					= FCE1.CasenoteNumber
		,FCE1LOS						= DATEDIFF(DAY, FCE1.EpisodeStartTime, FCE1.EpisodeEndTime)
		,FCE1Consultant					= FCE1CONS.SourceConsultant
		,FCE1PrimaryDiagnosisCode		= FCE1DIAG.Diagnosis
		,FCE1StartWard					= FCE1.StartWardTypeCode
		,FCE2LOS						= DATEDIFF(DAY, FCE2.EpisodeStartTime, FCE2.EpisodeEndTime)
		,FCE2Consultant					= FCE2CONS.SourceConsultant
		,FCE2PrimaryDiagnosisCode		= FCE2DIAG.Diagnosis
		,FCE2StartWard					= FCE2.StartWardTypeCode
		
	FROM
		APC.Encounter FCE1
		
		INNER JOIN WH.Calendar CAL
			ON		FCE1.DischargeDateId		= CAL.DateId
			
		CROSS APPLY fn_DiagnosisCodeExtension(FCE1.PrimaryDiagnosisCode) FCE1DIAG
		
		INNER JOIN WH.Directorate DIR
			ON		FCE1.StartDirectorateCode	= DIR.DirectorateCode
					
		INNER JOIN APC.PatientClassification PC
			ON		FCE1.ContextCode				= PC.SourceContextCode
				AND	FCE1.PatientClassificationCode	= PC.SourcePatientClassificationCode
			
		INNER JOIN APC.AdmissionMethod AM
			ON		FCE1.ContextCode			= AM.SourceContextCode
				AND	FCE1.AdmissionMethodCode	= AM.SourceAdmissionMethodCode
				
		INNER JOIN WH.Consultant FCE1CONS
			ON		FCE1.ContextCode			= FCE1CONS.SourceContextCode	
				AND	FCE1.ConsultantCode			= FCE1CONS.SourceConsultantCode

		LEFT JOIN APC.Encounter FCE2
			ON		FCE1.GlobalProviderSpellNo		= FCE2.GlobalProviderSpellNo	
				AND FCE2.GlobalEpisodeNo			= 2
				
		OUTER APPLY fn_DiagnosisCodeExtension(FCE2.PrimaryDiagnosisCode) FCE2DIAG
		
		LEFT JOIN WH.Consultant FCE2CONS
			ON		FCE2.ContextCode			= FCE2CONS.SourceContextCode	
				AND	FCE2.ConsultantCode			= FCE2CONS.SourceConsultantCode
											
	WHERE
			FCE1.GlobalEpisodeNo			= 1
		-- Both episodes must have R Codes
		AND FCE1DIAG.IsRCode				= 1
		AND COALESCE(FCE2DIAG.IsRCode, 1)	= 1
		AND 
		(
				@PatientClassificationCode IS NULL
			OR	PC.NationalPatientClassificationCode IN (SELECT Item FROM dbo.fn_ListToTable(@PatientClassificationCode,','))
		)
		AND 
		(
				@AdmissionMethodCode IS NULL
			OR	AM.NationalAdmissionMethodCode IN (SELECT Item FROM dbo.fn_ListToTable(@AdmissionMethodCode,','))
		)
		AND CAL.TheMonth					= @Period
		AND DIR.Division					IN (SELECT Item FROM dbo.fn_ListToTable(@Division,','))	
		AND	
		(
			(
					@Type		= 'ROP'						-- Spells with the specified episode having an "Opportunity" R Code
				AND	FCE1.GlobalEpisodeNo = CASE WHEN @EpisodeNo = 1 THEN 1 ELSE FCE1.GlobalEpisodeNo END
				AND	COALESCE(FCE2.GlobalEpisodeNo, 9999) 
						= CASE 
							WHEN @EpisodeNo = 2 THEN 2 
							-- Don't want the query to exlude rows for Episode 1 drills for single episode spells 
							-- ie Episode 2 doesn't exist
							ELSE COALESCE(FCE2.GlobalEpisodeNo, 9999)
						  END
				AND FCE1DIAG.IsRCodeCodingOpportunity	= CASE WHEN @EpisodeNo = 1 THEN 1 ELSE FCE1DIAG.IsRCodeCodingOpportunity END
				AND	COALESCE(FCE2DIAG.IsRCodeCodingOpportunity, 9999) 
						= CASE 
							WHEN @EpisodeNo = 2 THEN 1 
							-- Don't want the query to exlude rows for Episode 1 drills for single episode spells 
							-- ie Episode 2 doesn't exist
							ELSE COALESCE(FCE2DIAG.IsRCodeCodingOpportunity, 9999)
						  END
			)
			OR
			(
					@Type		= 'RCOD'					-- Spells with RCode 
			)
		)



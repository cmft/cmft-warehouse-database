﻿



CREATE PROCEDURE [RPT].[Outliers] 

@sitrepday Date, @Division as varchar(20), @location as nvarchar(20), @outliertype as varchar(40), @medicaloutlier as varchar(1)

-- exec rpt.outliers '2014-12-14', null, null, null, null
as


	
	

	
declare @wardout table 	
	
(	
       ward varchar(5) ---primary key	
      -- ,spec varchar(15)	
       ,Location varchar(30)	
       
       ,primary key (ward)
    	
)	
	
insert into @wardout	
	
values	
('10M','Surgery'),
('11M','Surgery'),
('12M','Surgery'),
('15M','Acute Medicine'),
('36','Specialist Medicine'),
('37','Specialist Medicine'),
('3M','Specialist Medicine'),
('44','Specialist Medicine'),
('4M','Specialist Medicine'),
('54','Royal Eye'),
('55I','Royal Eye'),
('8M','Surgery'),
('9M','Surgery'),
--('7S','Surgery'),
('7S','Specialist Medicine'), -- CH Added 26102015
('8HPB','Surgery'),
('DC','Royal Eye'),
('ESTU','Surgery'),
('ETCD','Surgery'),
('ETCS','Surgery'),
('SAL','Surgery'),
('OMU','OMU'),
('AMU','Acute Medicine'),
('AM1','Acute Medicine'),
('AM2','Acute Medicine'),
('5m','Acute Medicine'),
('45','Acute Medicine'),
('46','Acute Medicine'),
('30M','Acute Medicine'),
('31M','Acute Medicine'),
('31R','Acute Medicine'),
('32M','Acute Medicine'),
('14m','Surgery'),
('AM3','Specialist Medicine'),
('AM4','Specialist Medicine'),
('ACC','Specialist Medicine'),
('CSITU','Specialist Medicine'),
('27M','Specialist Medicine'), -- CH Added 26102015

('55D','Royal Eye'),
('47A','St Marys'),
('47B','St Marys'),
('47C','St Marys'),
('62','St Marys'),
('63','St Marys'),
('64','St Marys'),
('64A','St Marys'),
('64B','St Marys'),
('64C','St Marys'),
('64HD','St Marys'),
('64RB','St Marys'),
('64RC','St Marys'),
('65','St Marys'),
('65C','St Marys'),
('65I','St Marys'),
('66','St Marys'),
('66C','St Marys'),
('68','St Marys'),
('90','St Marys'),
('BCS','St Marys'),
('BCSC','St Marys'),
('HB','St Marys'),
('HM','St Marys'),
('NICT','St Marys'),
('SUBF','St Marys')
	
Select
*


From

(	
	
	

	
Select	
Sitrepdate =  convert(varchar, @sitrepday, 106)
,OutlierType= 
case when wardstay.wardcode ='OMU' and Directorate <>'Emergency Serv' then 'Outliers on OMU'
when wardstay.wardcode in ('ESTU','SAL','8HPB','8M','9M','10M','11M','12M','14M','ETCD','ETCS') -- CH removed 7S 26102015
AND [Directorate].[DivisionCode] <>'Surg' THEN 'Outliers on Surgical Wards'

when wardstay.wardcode in ('44','36','37','37A','ACC','CSITU','3m','4m')
AND [Directorate].[DivisionCode] <>'MEDSP' THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AM3','AM4')
AND [Directorate].[DivisionCode] not in ('MEDSP','MEDAC') THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AMU','AM1','AM2','5M','30M','31R','31M','32M','45','46','15m')
AND [Directorate].[DivisionCode]='Surg' THEN 'Surgical Outliers on Acute Medical Wards'
when wardstay.wardcode in ('54','55I','DC','55D')
AND [Directorate].[DivisionCode] <>'REH' THEN 'Outliers on Royal Eye Wards'
when wardstay.wardcode in ('47A','47B','47C','62','63','64','64A','64B','64C','64HD','64RB','64RC','65','65C','65I','66','66C','68','90','BCS','BCSC','HB','HM','NICT','SUBF')

AND [Directorate].[DivisionCode] not in ('SMH','CHILD') THEN 'Outliers on St Marys Wards'

else null end

	
,Division
,Div = case when Division like 'Clin%' then 'Clinical' else Division end	
,Location	
	
,wardstay.SourcePatientNo	
,wardstay.SourceSpellNo	
,PatientForename	
,PatientSurname	
,AdmissionTime	
,StartTime	
,wardstay.wardcode	

,CasenoteNumber	
,Directorate.Directorate	
,Consultant = 	
      Consultant.Consultant	
,WardDesc = 	
      isnull(Wardbase.Ward,wardstay.wardcode)
      
,MedicalOutlier = Case when Division in ('Specialist Medical','Acute Medical') and location  not in ('OMU','Specialist Medicine','Acute Medicine') then 'Y' else 'N' end
from warehousesql.warehouse.APC.Encounter apc	
	
	
	
join warehousesql.warehouse.apc.wardstay  wardstay on	
wardstay.[ProviderSpellNo] = apc.[ProviderSpellNo]	
and startdate < @sitrepday and (endDate >= @sitrepday or endDate is null)	
and EpisodeStartDate < @sitrepday and (EpisodeEndDate >= @sitrepday or EpisodeEndDate is null)	
	
inner join warehousesql.warehouse.WH.Directorate 	
on Directorate.DirectorateCode = apc.StartDirectorateCode	
	
join  @wardout ward on	
ward.ward = wardstay.wardcode	

	
left outer join warehousesql.warehouse.pas.Ward Wardbase	
on wardbase.WardCode = wardstay.wardCode	collate database_default
	
	
left outer join  warehousesql.warehouse.pas.Consultant	
on Consultant.ConsultantCode = apc.ConsultantCode	
	
left join warehousesql.[Warehouse].[WH].[Division]	
on	
[Division].[DivisionCode]=[Directorate].[DivisionCode]	
	
	
where 
 ( case when wardstay.wardcode ='OMU' and Directorate <>'Emergency Serv' then 'Outliers on OMU'
when wardstay.wardcode in ('ESTU','SAL','8HPB','8M','9M','10M','11M','12M','14M','ETCD','ETCS') -- CH removed 7S 26102015
AND [Directorate].[DivisionCode] <>'Surg' THEN 'Outliers on Surgical Wards'

when wardstay.wardcode in ('44','36','37','37A','ACC','CSITU','3m','4m')
AND [Directorate].[DivisionCode] <>'MEDSP' THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AM3','AM4')
AND [Directorate].[DivisionCode] not in ('MEDSP','MEDAC') THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AMU','AM1','AM2','5M','30M','31R','31M','32M','45','46','15m')
AND [Directorate].[DivisionCode]='Surg' THEN 'Surgical Outliers on Acute Medical Wards'
when wardstay.wardcode in ('54','55I','DC','55D')
AND [Directorate].[DivisionCode] <>'REH' THEN 'Outliers on Royal Eye Wards'
when wardstay.wardcode in ('47A','47B','47C','62','63','64','64A','64B','64C','64HD','64RB','64RC','65','65C','65I','66','66C','68','90','BCS','BCSC','HB','HM','NICT','SUBF')

AND [Directorate].[DivisionCode] not in ('SMH','CHILD') THEN 'Outliers on St Marys Wards'

else null end ) 


is not null

and Division <> 'Childrens'

	
	
group by	
	
(case when wardstay.wardcode ='OMU' and Directorate <>'Emergency Serv' then 'Outliers on OMU'
when wardstay.wardcode in ('ESTU','SAL','8HPB','8M','9M','10M','11M','12M','14M','ETCD','ETCS') -- CH removed 7S 26102015
AND [Directorate].[DivisionCode] <>'Surg' THEN 'Outliers on Surgical Wards'

when wardstay.wardcode in ('44','36','37','37A','ACC','CSITU','3m','4m')
AND [Directorate].[DivisionCode] <>'MEDSP' THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AM3','AM4')
AND [Directorate].[DivisionCode] not in ('MEDSP','MEDAC') THEN 'Outliers on Specialist Medical Wards'

when wardstay.wardcode in ('AMU','AM1','AM2','5M','30M','31R','31M','32M','45','46','15m')
AND [Directorate].[DivisionCode]='Surg' THEN 'Surgical Outliers on Acute Medical Wards'
when wardstay.wardcode in ('54','55I','DC','55D')
AND [Directorate].[DivisionCode] <>'REH' THEN 'Outliers on Royal Eye Wards'
when wardstay.wardcode in ('47A','47B','47C','62','63','64','64A','64B','64C','64HD','64RB','64RC','65','65C','65I','66','66C','68','90','BCS','BCSC','HB','HM','NICT','SUBF')

AND [Directorate].[DivisionCode] not in ('SMH','CHILD') THEN 'Outliers on St Marys Wards'

else null end )

	
,DIVISION	
,location	
	
	
,wardstay.SourcePatientNo	
,wardstay.SourceSpellNo	
,PatientForename	
,PatientSurname	
,AdmissionTime	
,StartTime	
,wardstay.wardcode	
	
,CasenoteNumber	
,Directorate.Directorate	
,Consultant.Consultant	
,Wardbase.Ward	


) data

where  (Div = @Division or @Division is null)
and ( Location = @location  or @location is null)
and ( [OutlierType] = @outliertype or @outliertype is null)
and  ( [medicaloutlier] = @medicaloutlier or @medicaloutlier is null)





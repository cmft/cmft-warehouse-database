﻿-- exec [RPT].[FutureTCIDatesParamDirectorate] '27 Jan 2014'

CREATE PROCEDURE [RPT].[FutureTCIDatesParamDirectorate]
(
	 @StartBaseDate			DATE		= NULL
	,@Division				VARCHAR(50)	= NULL
	,@NoWeekEnds			INT			= 4		-- Determines the end date of the query and represents the number of 
) AS
	DECLARE @StartDate			DATE
	DECLARE @EndDate			DATE
	DECLARE @WaitingListDate	DATE 
	
	-- Latest wiaiting list date
	SELECT 
		@WaitingListDate = MAX(CensusDate)
	FROM
		APCWL.Encounter
	WHERE 
		CensusDate > DATEADD(DAY,-7,GETDATE())
	
	-- Weekending derivations	
	SET @StartBaseDate = COALESCE(@StartBaseDate, GETDATE())
	
	SET DATEFIRST  1	-- Monday

	SET @StartDate  = DATEADD(DAY, 1 - DATEPART(DW, @StartBaseDate), @StartBaseDate)
	SET @EndDate	= DATEADD(DAY, -1, DATEADD(WEEK, @NoWeekEnds, @StartDate))		-- End Date is a Sunday

	-- Use Group By to get a distinct list
	SELECT 
		  DIR.DirectorateCode
		 ,DIR.Directorate
		 ,COUNT(*)
	FROM
		APCWL.Encounter E
		INNER JOIN WH.Directorate DIR
			ON		E.DirectorateCode			= DIR.DirectorateCode
				AND	DIR.Division				= COALESCE(@Division, DIR.Division)
	WHERE
			E.CensusDate	= @WaitingListDate
		AND	E.TCIDate		BETWEEN @StartDate AND @EndDate
		
	GROUP BY 
		  DIR.DirectorateCode
		 ,DIR.Directorate
	
	UNION
	
	SELECT
		NULL
		,'[All]'
		,1
		
	ORDER BY 		
		Directorate


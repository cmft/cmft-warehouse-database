﻿



CREATE PROCEDURE [RPT].[GetStroke] 

@StartDate Datetime,
@EndDate datetime

as


declare @FirstDischargeDate date

--declare @StartDate Datetime, @EndDate datetime

--set @StartDate = '01 Jun 2012'
--set @EndDate = '30 Jun 2012'

set @FirstDischargeDate = @StartDate


--select
--	@FirstDischargeDate = min(TheDate)
--from
--	WH.Calendar
--where
--	FinancialMonthKey = @FinancialMonthKey


DECLARE @StrokeTable TABLE 
(	
	 PCTDescription varchar(100)
	,InternalNo  varchar(20)
	,EpisodeNo varchar(20)
	,PrimaryDiagnosisIndicator  varchar(11)
	,OtherDiagnosisIndicator   varchar(11)
	,CasenoteNumber varchar(20)
	,PatientForename varchar(20)
	,PatientSurname varchar(30)
	,DateOfBirth datetime
	,DateOfDeath smalldatetime
	,AdmissionTime smalldatetime
	,DischargeTime smalldatetime
	,Numerator int
	,DenominatorIncAE int
	,PercentageOnStrokeWardIncAE float
	,TheMonth nvarchar(34)
	,AppointmentDate datetime
	,AttendanceOutcomeCode varchar(5)
	,DNACount int
	,PCTCode varchar(10) 
	,ArrivalTime smalldatetime
	,DepartureTime smalldatetime
	,AdmissionMethodCode varchar(2)
	,PatientCategoryCode varchar(2) 
	,FirstAdmissionWard varchar(10)
	,Division varchar(60)
	,DischargeWard varchar(50)
)


insert into @StrokeTable
(
	 PCTDescription
	,InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator
	,OtherDiagnosisIndicator
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,AdmissionTime
	,DischargeTime
	,Numerator
	,DenominatorIncAE
	,PercentageOnStrokeWardIncAE
	,TheMonth
	,PCTCode
	,ArrivalTime
	,DepartureTime
	,AdmissionMethodCode
	,PatientCategoryCode
	,Division
	,DischargeWard
)


SELECT
	 PCTDescription
	,InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator = PrimaryPosition
	,OtherDiagnosisIndicator = SecondaryPosition
	,CasenoteNumber
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,AdmissionTime
	,DischargeTime
	,Numerator =
		LOSStrokeWard
	,DenominatorIncAE = 
		Denominator + ISNULL(AETime,0)
	,PercentageOnStrokeWardIncAE = 
		((LOSStrokeWard * 1.00) / (Denominator + ISNULL(AETime , 0)) * 1.00) * 100

	,myqry.TheMonth

	,PCTCode
	,ArrivalTime
	,DepartureTime
	,AdmissionMethodCode = Left(AdmissionMethodCode,2)
	,PatientCategoryCode = left(PatientCategoryCode,2)
	,(SELECT DISTINCT Division 
		FROM APC.Encounter E2
		INNER JOIN WH.Directorate D2
			ON E2.StartDirectorateCode = D2.DirectorateCode
	  WHERE FirstEpisodeInSpellIndicator = 1 AND ProviderSpellNo = EpisodeNo) Division,
	(SELECT DISTINCT TOP 100 Ward.SourceWardCode  
		FROM APC.Encounter E2
		--INNER JOIN APC.LastEpisodeInSpell LEIS
		--	ON E2.LastEpisodeInSpellID = LEIS.SourceLastEpisodeInSpellID
		INNER JOIN APC.Ward 
			ON E2.EndWardID = Ward.SourceWardID
	  WHERE (NationalLastEpisodeInSpellIndicator = '1' or E2.DischargeTime IS NULL)
	  AND ProviderSpellNo = EpisodeNo) DischargeWard		  
FROM
	(
	SELECT 
		 PCTDescription = 
			PCT.[PCT Description]

		,InternalNo = Encounter.SourcePatientNo
		,EpisodeNo = Encounter.ProviderSpellNo
		,CasenoteNumber
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.AdmissionTime
		,Encounter.DischargeTime

		,LOSStrokeWard =
			ISNULL(LOSStrokeWard , 0)

		,Denominator =
			SUM(
				DATEDIFF(
					 mi
					,Encounter.EpisodeStartTime
					,Encounter.EpisodeEndTime
				)
			)

		,AETime = TimeSpentInAE

		,StrokePatients.DiagnosisIndicator
		,StrokePatients.PrimaryPosition
		,StrokePatients.SecondaryPosition

		,Calendar.TheMonth
		,Encounter.PCTCode
		,LOSAE.ArrivalTime
		,LOSAE.DepartureTime
		,AdmissionMethod.NationalAdmissionMethodCode AdmissionMethodCode
		,Encounter.PatientCategoryCode

	FROM 
		apc.Encounter Encounter

	INNER JOIN APC.AdmissionMethod
		ON AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID
		
	--INNER JOIN WarehouseReportingMerged.WH.Directorate Directorate
	--	ON Directorate.DirectorateID = Encounter.StartDirectorateID

-- CCB 2012-06-29 what is this and why are we using it?
	LEFT JOIN Information.dbo.PCT PCT 
	ON	PCT.[PCT Code] = Encounter.PCTCode


	--LOS on Stroke Ward
	LEFT JOIN
		(
		SELECT 
			 ProviderSpellNo

			,LOSStrokeWard =
				SUM(
					DATEDIFF(
						 mi
						,WardStay.StartTime
						,WardStay.EndTime
					)
				) 

		FROM 
			APC.WardStay
		INNER JOIN 
			APC.Ward
		ON WardID = SourceWardID
		WHERE 
			SourceWardCode in ( '30M' , '31M' , '31R') 
		GROUP BY 
			ProviderSpellNo
		) WardStay 
	ON WardStay.ProviderSpellNo = Encounter.ProviderSpellNo


	--LOS in AE
	LEFT JOIN 
		(	
		SELECT distinct
			 APCEncounter.DistrictNo
			,APCEncounter.AdmissionTime
			,TimeSpentInAE = StageDurationMinutes
			,AEEncounter.ArrivalTime
			,AEEncounter.DepartureTime
		FROM
			APC.Encounter APCEncounter

		INNER JOIN AE.Encounter AEEncounter
		ON	AEEncounter.DistrictNo = APCEncounter.DistrictNo
		and AEEncounter.DepartureTime BETWEEN DATEADD(HOUR , -2 , APCEncounter.AdmissionTime) and DATEADD(HOUR , 2 , APCEncounter.AdmissionTime)

		INNER JOIN AE.Fact Fact 
		ON	Fact.MergeEncounterRecno = AEEncounter.MergeEncounterRecno

		WHERE
			--Fact.StageCode = 'INDEPARTMENTADJUSTED'
			Fact.StageID = 12
		) LOSAE 	
	ON	LOSAE.DistrictNo = Encounter.DistrictNo
	AND	LOSAE.AdmissionTime = Encounter.AdmissionTime


	--only return stroke patients
	INNER JOIN 
		(	
		SELECT 
			 SourcePatientNo
			,ProviderSpellNo

			,DiagnosisIndicator = 
				CASE 
				WHEN [Primary] is not null AND [Secondary] is not null then 'Both'
				WHEN [Primary] is not null then 'Primary'
				WHEN [Secondary] is not null then 'Secondary'
				ELSE NULL
				END

			,PrimaryPosition = [Primary]
			,SecondaryPosition = [Secondary]

		FROM
			(
			--pivot data so we can compare diagnosis columns as a primary and secondary can be the same
			SELECT
				 SourcePatientNo
				,ProviderSpellNo
				,[Primary]
				,[Secondary]
			FROM
				(
				SELECT 
					 SourcePatientNo
					,ProviderSpellNo
					,Position = 'Primary'
					,DiagnosisType = 
						case
						when
								PrimaryDiagnosisCode like 'I61%' --stroke
							or	PrimaryDiagnosisCode like 'I63%' --stroke
							or	PrimaryDiagnosisCode like 'I64%' --stroke 
							then 'Stroke'
						else 'Stroke-Like'
						end
				FROM 
					APC.Encounter Encounter
				WHERE 
					PrimaryDiagnosisCode like 'I61%' --stroke
				or	PrimaryDiagnosisCode like 'I63%' --stroke
				or	PrimaryDiagnosisCode like 'I64%' --stroke 
				or	PrimaryDiagnosisCode like 'I62%' --stroke-like
				or	PrimaryDiagnosisCode like 'H34%' --stroke-like

				UNION	
										
				SELECT 
					 Encounter.SourcePatientNo
					,Encounter.ProviderSpellNo
					,Position = 'Secondary'
					,DiagnosisType = 
						case
						when
								PrimaryDiagnosisCode like 'I61%' --stroke
							or	PrimaryDiagnosisCode like 'I63%' --stroke
							or	PrimaryDiagnosisCode like 'I64%' --stroke 
							then 'Stroke'
						else 'Stroke-Like'
						end
				FROM
					APC.Encounter Encounter

				INNER JOIN APC.Diagnosis Diagnosis 
				ON	Encounter.MergeEncounterRecno = Diagnosis.MergeEncounterRecno
				--	Diagnosis.SourceEncounterNo = Encounter.SourceEncounterNo
				--AND Diagnosis.ProviderSpellNo = Encounter.ProviderSpellNo
				--AND Diagnosis.SourcePatientNo  = Encounter.SourcePatientNo 
				AND (
						Diagnosis.DiagnosisCode like 'I61%' --stroke  
					or	Diagnosis.DiagnosisCode like 'I63%' --stroke 
					or	Diagnosis.DiagnosisCode like 'I64%' --stroke 
					or	Diagnosis.DiagnosisCode like 'I62%' --stroke-like  
					or	Diagnosis.DiagnosisCode like 'H34%' --stroke-like
					)
				)s
			PIVOT
				(
				max(DiagnosisType)
				FOR Position IN ([Primary] , [Secondary])
				) as pvt
			)qry
		) StrokePatients
	ON StrokePatients.SourcePatientNo = Encounter.SourcePatientNo
	AND StrokePatients.ProviderSpellNo = Encounter.ProviderSpellNo
					
	INNER JOIN WH.Calendar Calendar
	ON	Calendar.DateID = Encounter.DischargeDateID

	--WHERE
	--	Calendar.FinancialMonthKey = @FinancialMonthKey
	
	WHERE Encounter.DischargeTime >= @StartDate AND Encounter.DischargeTime <= @EndDate
	
	AND
		CASE 
		WHEN (DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionTime))-(DATEPART(yy,(CONVERT(DATETIME,CONVERT(INT,DATEADD(yy,DATEDIFF(yy,Encounter.DateOfBirth,Encounter.AdmissionTime),Encounter.DateOfBirth))-CONVERT(INT,DATEADD(yy,-1,Encounter.AdmissionTime)))))-1900)<19
		THEN 'C'
		ELSE 'A'
		END = 'A'
										

	GROUP BY 
		 PCT.[PCT Description]
		,Encounter.SourcePatientNo
		,Encounter.ProviderSpellNo
		,CasenoteNumber
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.AdmissionTime
		,Encounter.DischargeTime
		,LOSStrokeWard
		,TimeSpentInAE
		,StrokePatients.DiagnosisIndicator
		,StrokePatients.PrimaryPosition
		,StrokePatients.SecondaryPosition
		,Calendar.TheMonth
		,Encounter.PCTCode
		,LOSAE.ArrivalTime
		,LOSAE.DepartureTime
		,NationalAdmissionMethodCode
		,Encounter.PatientCategoryCode
		--,Division


	) myqry





























----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------  Now update Table Variable  -----------------------------

--1. update DNA Count for a patients spell

DECLARE @DNACount Table
(	
	 DistrictNo varchar(20)
	,SourcePatientNo  varchar(20)
	,ProviderSpellNo  varchar(20)
	,DNA int
)

insert into @DNACount
(
	 DistrictNo
	,SourcePatientNo
	,ProviderSpellNo
	,DNA
)

select
	 DistrictNo
	,SourcePatientNo
	,ProviderSpellNo
	,DNA = Max(RowInd)
from
	(
	select 
		 OpEncounter.DistrictNo
		,Encounter.SourcePatientNo
		,Encounter.ProviderSpellNo
		,OpEncounter.AppointmentTime

		,RowInd =
			row_number() over 
				(
				partition by 
					 OpEncounter.DistrictNo
					,Encounter.ProviderSpellNo 
				order by 
					 OpEncounter.DistrictNo
					,Encounter.ProviderSpellNo
					, OpEncounter.AppointmentTime
				)

	from
		OP.Encounter OpEncounter

	left join APC.Encounter Encounter
	on Encounter.DistrictNo = OpEncounter.DistrictNo
	--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
	AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

	where 
		Encounter.ProviderSpellNo is not null
	--AND OpEncounter.AttendanceOutcomeCode = ('DNA')
	AND OpEncounter.AppointmentOutcomeCode = ('DNA')	
	--AND OpEncounter.AttendanceOutcomeID IN (1893972,1893976,1893991,1893992,1893784)
	AND Encounter.DischargeTime >= @FirstDischargeDate

	group by
		 OpEncounter.DistrictNo
		,Encounter.SourcePatientNo
		,Encounter.ProviderSpellNo
		,OpEncounter.AppointmentTime
	)myqry

group by
	 DistrictNo
	,SourcePatientNo
	,ProviderSpellNo



--now update Table Variable Stroke
update s
set 
	DNACount = fa.DNA
from
	@StrokeTable s

inner join 	@DNACount fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.ProviderSpellNo = s.EpisodeNo


--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
----2.
update s
set 
	DNACount = 0
from
	@StrokeTable s
where
	DNACount is null
--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
----Rule3.1: Show details of first attendance (but not if the DNA count is > 1)

	DECLARE @FirstAttendance Table
	(	
		 DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,ProviderSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @FirstAttendance
		(
			 DistrictNo
			,SourcePatientNo
			,ProviderSpellNo
			,AppointmentTime
			,AttendanceOutcomeCode
			,RowInd
		)

		select
			 DistrictNo
			,SourcePatientNo
			,ProviderSpellNo
			,AppointmentTime
			,AppointmentOutcomeCode
			,RowInd
		from
			(
			select 
				 OpEncounter.DistrictNo
				,Encounter.SourcePatientNo
				,Encounter.ProviderSpellNo
				,OpEncounter.AppointmentTime
				,AppointmentOutcomeCode

				,RowInd =
					row_number() over 
						(
						partition by 
							 OpEncounter.DistrictNo
							,Encounter.ProviderSpellNo 
						order by 
							 OpEncounter.DistrictNo
							,Encounter.ProviderSpellNo
							,OpEncounter.AppointmentTime
						)
			from
				OP.Encounter OpEncounter

			left join APC.Encounter Encounter
			on Encounter.DistrictNo = OpEncounter.DistrictNo
			AND OpEncounter.AppointmentTime between dateadd(week,5,Encounter.DischargeTime) AND dateadd(day,-1,(dateadd(week,9,Encounter.DischargeTime)))
			--AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

			where 
					Encounter.ProviderSpellNo is not null
				and AppointmentOutcomeCode in ('ATT','SATT','WLK')
				--AND OpEncounter.AppointmentOutcomeCode = ('DNA')	
				--and AttendanceOutcomeID in (1893775,1893822,1893825)
				--AND Encounter.DischargeTime >= @FirstDischargeDate

			group by
				 OpEncounter.DistrictNo
				,Encounter.SourcePatientNo
				,Encounter.ProviderSpellNo
				,OpEncounter.AppointmentTime
				,AppointmentOutcomeCode
			)myqry

		where
			RowInd = 1

--now update Table Variable Stroke
update s
set 
	 AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode

from
	@StrokeTable s

inner join @FirstAttendance fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.ProviderSpellNo = s.EpisodeNo

where
	ISNULL(DNACount,0) < 2

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----Rule3.2: If DNA count is > 1 then show 2nd DNA details
	DECLARE @SecondDNADetails Table
	(	
		 DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,ProviderSpellNo  varchar(20)
		,AppointmentTime datetime
		,AppointmentOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @SecondDNADetails
	(
		 DistrictNo
		,SourcePatientNo
		,ProviderSpellNo
		,AppointmentTime
		,AppointmentOutcomeCode
		,RowInd
	)

	select
		 DistrictNo
		,SourcePatientNo
		,ProviderSpellNo
		,AppointmentTime
		,AppointmentOutcomeCode
		,RowInd
	from
		(
		select 
			 OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.ProviderSpellNo
			,OpEncounter.AppointmentTime
			,AppointmentOutcomeCode

			,RowInd = 
				row_number() over 
					(
					partition by 
						 OpEncounter.DistrictNo
						,Encounter.ProviderSpellNo 
					order by 
						 OpEncounter.DistrictNo
						,Encounter.ProviderSpellNo
						,OpEncounter.AppointmentTime
					)
		from
			OP.Encounter OpEncounter

		left join APC.Encounter Encounter
		on	Encounter.DistrictNo = OpEncounter.DistrictNo
		--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
		AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

		where 
			Encounter.ProviderSpellNo is not null
		--AND OpEncounter.AttendanceOutcomeCode = ('DNA')
		--AND OpEncounter.AttendanceOutcomeID IN (1893972,1893976,1893991,1893992,1893784)
		AND OpEncounter.AppointmentOutcomeCode = ('DNA')	
		--AND Encounter.DischargeTime >= @FirstDischargeDate

		group by
			 OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.ProviderSpellNo
			,OpEncounter.AppointmentTime
			,AppointmentOutcomeCode
		)myqry

	where 
		RowInd = 2

	group by 	
		 DistrictNo
		,SourcePatientNo
		,ProviderSpellNo
		,AppointmentTime
		,AppointmentOutcomeCode
		,AppointmentTime
		,RowInd

--now update Table Variable Stroke
update s
set 
	 AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AppointmentOutcomeCode
from
	@StrokeTable s

inner join @SecondDNADetails fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.ProviderSpellNo = s.EpisodeNo

Where
	DNACount > 1

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----Rule3.3: If there has been only 1 DNA and no attendance then show the first DNA
	DECLARE @FirstDNADetails Table
	(	
		 DistrictNo varchar(20)
		,SourcePatientNo  varchar(20)
		,ProviderSpellNo  varchar(20)
		,AppointmentTime datetime
		,AttendanceOutcomeCode varchar(5)
		,RowInd int
	)

	insert into @FirstDNADetails
	(
		 DistrictNo
		,SourcePatientNo
		,ProviderSpellNo
		,AppointmentTime
		,AttendanceOutcomeCode
		,RowInd
	)

	select
		 DistrictNo
		,SourcePatientNo
		,ProviderSpellNo
		,AppointmentTime
		,AppointmentOutcomeCode
		,RowInd
	from
		(
		select 
			OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.ProviderSpellNo
			,OpEncounter.AppointmentTime
			,AppointmentOutcomeCode
		
			,RowInd =
				row_number() over 
					(
					partition by
						 OpEncounter.DistrictNo
						,Encounter.ProviderSpellNo 
					order by
						 OpEncounter.DistrictNo
						,Encounter.ProviderSpellNo
						,OpEncounter.AppointmentTime
					)
		from
			OP.Encounter OpEncounter

		left join APC.Encounter Encounter
		on	Encounter.DistrictNo = OpEncounter.DistrictNo
		--AND OpEncounter.AppointmentTime between Encounter.DischargeTime AND dateadd(week,6,Encounter.DischargeTime)
		AND OpEncounter.AppointmentTime >= Encounter.DischargeTime

		where 
			Encounter.ProviderSpellNo is not null
		--AND OpEncounter.AttendanceOutcomeCode = ('DNA')
		--AND OpEncounter.AttendanceOutcomeID IN (1893972,1893976,1893991,1893992,1893784)
		AND OpEncounter.AppointmentOutcomeCode = ('DNA')		
		AND Encounter.DischargeTime >= @FirstDischargeDate

		group by
			 OpEncounter.DistrictNo
			,Encounter.SourcePatientNo
			,Encounter.ProviderSpellNo
			,OpEncounter.AppointmentTime
			,AppointmentOutcomeCode
		)myqry

		where 
			RowInd = 1

		group by 	
			 DistrictNo
			,SourcePatientNo
			,ProviderSpellNo
			,AppointmentTime
			,AppointmentOutcomeCode
			,RowInd

--now update Table Variable Stroke
update s
set 
	 AppointmentDate = fa.AppointmentTime
	,AttendanceOutcomeCode = fa.AttendanceOutcomeCode

from
	@StrokeTable s

inner join @FirstDNADetails fa
on	fa.SourcePatientNo = s.InternalNo
AND fa.ProviderSpellNo = s.EpisodeNo

Where
	DNACount = 1
AND AppointmentDate is null


----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
--Rule 4 - Get First Episode Admission Ward
--we need to get the minimum admission date as the WardStay query brings back too much data

declare @MinAdmissionTime smalldatetime

select
	@MinAdmissionTime = MIN(AdmissionTime) 
from 
	@StrokeTable

DECLARE @FirstEpisodeWardTable TABLE 
(	 SourcePatientNo  varchar(20)
	,ProviderSpellNo varchar(20)
	,WardCode varchar(10)
)

INSERT INTO @FirstEpisodeWardTable
(
	 SourcePatientNo
	,ProviderSpellNo
	,WardCode
)

select
	 SourcePatientNo
	,ProviderSpellNo
	,WardCode
from
	(
	select 
		 SourcePatientNo
		,ProviderSpellNo
		,SourceWardCode WardCode 

		,RowInd = 
			row_number() over 
				(
				partition by 
					 SourcePatientNo
					,ProviderSpellNo 
				order by 
					 StartTime asc
				)

	from
		APC.WardStay
	inner join APC.Ward 
		on WardID = SourceWardID
	where
		StartTime >= @MinAdmissionTime
	)myqry
where
	RowInd = 1


update s
set FirstAdmissionWard = FirstEpisodeWard.WardCode

from
	@StrokeTable s

inner join @FirstEpisodeWardTable FirstEpisodeWard
on	FirstEpisodeWard.SourcePatientNo = s.InternalNo
AND FirstEpisodeWard.ProviderSpellNo = s.EpisodeNo


--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
--Now bring back data to the report :o)
select 
	 PCTDescription
	,InternalNo
	,EpisodeNo
	,PrimaryDiagnosisIndicator
	,OtherDiagnosisIndicator  
	,CasenoteNumber 
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,AdmissionTime
	,DischargeTime
	,Numerator
	,DenominatorIncAE 
	,PercentageOnStrokeWardIncAE 
	,TheMonth 
	,AppointmentDate 
	,AttendanceOutcomeCode
	,DNACount 
	,PCTCode  
	,ArrivalTime 
	,DepartureTime 
	,AdmissionMethodCode
	,PatientCategoryCode
	,FirstAdmissionWard

	,Timediff = 
		case 
		when ((DATEDIFF(hour,DischargeTime,AppointmentDate))/168)=0 then
			case 
			when (((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7)=0 then '' 
			else CAST(((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7 as varchar) + ' day' 
			end
		else CAST((DATEDIFF(hour,DischargeTime,AppointmentDate))/168 as varchar) + ' week ' +
			case
			when (((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7)=0 then '' 
			else CAST(((DATEDIFF(hour,DischargeTime,AppointmentDate))/24)%7 as varchar) + ' day'
			end
		end 
	,Division
	,DischargeWard
from 
	@StrokeTable

order by 
	DischargeTime asc



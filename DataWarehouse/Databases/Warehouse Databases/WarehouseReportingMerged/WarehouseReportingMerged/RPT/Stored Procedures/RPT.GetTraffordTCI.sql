﻿






	--Gareth Cunnah 07 Jun 2012
	--Extract TCI data for 14 days from the date entered
	--This will need to come from the merged warehouse
	
	--07 Jun 2012 still needs to be validated not getting same numbers as from Trafford Side
	--Also need to look at naming data items
	
	--25 June 2012 Added insert into table TGHInformation temp fix for Ducia
	--23 Jul 2012 amended Spec to use TreatmentFuntionCode as requested by TGH Information
	
	CREATE Proc [RPT].[GetTraffordTCI]
	
	
	 @StartDate datetime = null

	
	as
	
--	declare @SetStartDate datetime = coalesce(@StartDate,(select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0)))
	--declare @SetEnddate datetime = coalesce(@StartDate,(select DATEADD(wk, DATEDIFF(wk,0,GETDATE()), 0))) + 13


	--changed GC 05/10/2012
	declare @SetStartDate datetime = coalesce(@StartDate,(select DATEADD(day, DATEDIFF(day,0,GETDATE()), 0)))
	declare @SetEnddate datetime = coalesce(@StartDate,(select DATEADD(day, DATEDIFF(day,0,GETDATE()), 0))) + 13
	
	drop table TGHInformation.dbo.TraffordTCI

	select
			 Districtno	
			,Hospital 
			,InternalNo 
			,EpisodeNo 
			,ExpAdmitDate 
			,Activity.Specialty 
			,PrimProcedure 
			,Division = null
			--case when DivisionCode = 'M' then 'Medicine'
			--		     when DivisionCode = 'S' then 'Surgery' else
			--					DivisionCode end
			,Directorate 
			,spec 
			,cmraspec = Spec + ' ' + Specialty.NationalSpecialty
			,Consultant 
			,IntMan 
			,ExpLOS 
			,ExpWard 
			,ElectiveAdmissionTypeCode
			,WeekNo
			,StartDate
			,EndDate
			
			into TGHInformation.dbo.TraffordTCI

		from
		(
			select

			 Districtno
			,Hospital = null
			,InternalNo = Sourceuniqueid
			,EpisodeNo = null
			,ExpAdmitDate = offeredforadmissiondate
			,Specialty = null
			,PrimProcedure = null --AppointmentType2Code
			,Division = null
			,Directorate = null
			,spec = case 
						when I.SpecialtyCode is null then
					(case when I.ConsultantCode = 'ENDGEN' THEN '301'
						when I.ConsultantCode = 'UROGEN' THEN '101' 
					else c.MainSpecialtyCode end) 
					else I.TreatmentFunctionCode end
			,cmraspec = null
			,Consultant = null
			,IntMan = I.ManagementIntentionCode
			,ExpLOS = null
			,ExpWard = null
			
			,I.ElectiveAdmissionTypeCode
			,WeekNo =
				case 
					when offeredforadmissiondate between @SetStartDate and @SetStartDate + 6 then 1
					else 2 end
		,StartDate = @SetStartDate
		,EndDate = @SetEnddate

 
		from
		TraffordWarehouse.dbo.APCWaitingList I 
		left join TraffordReferenceData.dbo.INFODEPT_cab_consultants c
		on c.sds_code = I.ConsultantCode
		
		left outer join TraffordReferenceData.dbo.AppointmentType App 
		on I.AppointmentTypeCode = App.apptype_identity

		--left outer join TraffordWarehouse.dbo.ContactReasonLookup R 
		--on R.con_uniquenr = I.SourceUniqueID


		where
		CensusDate = @SetStartDate 

		and
	--	I.ElectiveAdmissionTypeCode in ('13','11') and
		I.CommissionerCode <> 'VPP00' 
		--and
	--	(R.conrsn_name not like ('%Remove from WL%')
	--	and R.conrsn_name not like ('%Patient does not wish to proceed%'))
		and I.ConsultantCode not IN ('CENMAN','HOPECON')
		 

				and offeredforadmissiondate between @SetStartDate and @SetEnddate

				
					) Activity	
			
			left join WH.Specialty Specialty
			on Specialty.SourceSpecialtyCode = Activity.spec
			and Specialty.SourceContextCode = 'TRA||UG'	
		





print @SetStartDate

print @SetEnddate
﻿
--    20150123    RR    Created for an audit.  Only temporary, but maybe required for a few months until an appropriate number of patients are audited.

CREATE Procedure [RPT].[LowSodiumResultCurrentInpatient]

as 

With SodiumCTE
      (
      ResultRecno
      ,SourcePatientNo
      ,SourceEpisodeNo
      ,ResultDate
      ,ResultTime
      ,Result
      ,MergeEncounterRecno
      ,GlobalProviderSpellNo
      )
as
      (
      select 
            CodedResult.ResultRecno
            ,CodedResult.SourcePatientNo
            ,CodedResult.SourceEpisodeNo
            ,CodedResult.ResultDate
            ,CodedResult.ResultTime
            ,CodedResult.Result
            ,Encounter.MergeEncounterRecno
            ,Encounter.GlobalProviderSpellNo
      from 
            Warehouse.OCM.CodedResult 

      inner join APC.Encounter
      on CodedResult.SourcePatientNo = Encounter.SourcePatientNo
      and CodedResult.SourceEpisodeNo = Encounter.SourceSpellNo
      and ResultTime between EpisodeStartTime and coalesce(EpisodeEndTime,getdate())

      where 
            ResultCode = 'NA'
      and ResultDate >='20141201'
      and   isnumeric(CodedResult.Result) = 1
      and Result < '125'
      and Encounter.DischargeDate is null
      and (DATEDIFF(day,DateofBirth,AdmissionDate)/365.25)>18
      and SiteCode = 'MRI'
      )
      
select  
      SodiumCTE.ResultRecno
      ,SodiumCTE.SourcePatientNo
      ,SodiumCTE.SourceEpisodeNo
      ,ResultDate
      ,ResultTime
      ,Result
      ,CurrentEncounter.GlobalProviderSpellNo
      ,CurrentEncounter.PatientSurname
      ,CurrentEncounter.PatientForename
      ,CurrentEncounter.DateOfBirth
      ,CurrentEncounter.SexCode
      ,CurrentEncounter.NHSNumber
      ,CurrentEncounter.CasenoteNumber
      ,CurrentEncounter.DistrictNo
      ,CurrentEncounter.AdmissionDate
      ,CurrentEncounter.AdmissionTime
      ,CurrentEncounter.EpisodeStartTime
      ,CurrentEncounter.SiteCode
      ,CurrentEncounter.StartWardTypeCode
      ,Ward.SourceWardCode
      ,Ward.SourceWard
      ,Ward.LocalWard
      ,Ward.LocalWardCode
      ,CurrentEncounter.PatientCategoryCode
      ,NationalSpecialtyCode
      ,NationalSpecialty
      ,NationalConsultantCode
      ,SourceConsultant
            
from 
      SodiumCTE 

inner join APC.Encounter CurrentEncounter
on SodiumCTE.GlobalProviderSpellNo = CurrentEncounter.GlobalProviderSpellNo
and CurrentEncounter.EpisodeEndDate is null

inner join WarehouseReportingMerged.APC.WardStay
on CurrentEncounter.SourcePatientNo = WardStay.SourcePatientNo
and CurrentEncounter.SourceSpellNo = WardStay.SourceSpellNo

inner join WH.Ward
on WardStay.WardID = Ward.SourceWardID

inner join WH.Specialty
on CurrentEncounter.SpecialtyID = Specialty.SourceSpecialtyID

inner join WH.Consultant
on CurrentEncounter.ConsultantID = Consultant.SourceConsultantID

where 
      not exists
            (
            select
                  1
            from
                  WarehouseReportingMerged.APC.WardStay Latest
            where
                  WardStay.SourcePatientNo = Latest.SourcePatientNo
            and WardStay.SourceSpellNo = Latest.SourceSpellNo
            and Latest.StartTime > WardStay.StartTime
            )
and   
not exists
            (
            select
                  1
            from
                  SodiumCTE Latest
            where
                  SodiumCTE.GlobalProviderSpellNo = Latest.GlobalProviderSpellNo
            and Latest.ResultTime > SodiumCTE.ResultTime
            )


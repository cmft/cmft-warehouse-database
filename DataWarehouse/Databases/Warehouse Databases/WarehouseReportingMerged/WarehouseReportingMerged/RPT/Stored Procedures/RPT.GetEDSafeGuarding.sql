﻿

CREATE PROCEDURE [RPT].[GetEDSafeGuarding]
(@StartDate Datetime,
@EndDate datetime)
AS

--DECLARE @StartDate Datetime,
--@EndDate datetime

--SET @StartDate = '01 Oct 2012'
--SET @EndDate = '07 Oct 2012'


SELECT		atd_num								= Attendance_Details.atd_num, 
			pat_forename						= Patient.pat_forename,
			pat_surname							= Patient.pat_surname, 
			Arrival								= StageTimes_view.Arrival, 
			Left_Dept							= StageTimes_view.Left_Dept,
			dpt_name							= CFG_Dept.dpt_name, 
			atd_arrivaldate						= Attendance_Details.atd_arrivaldate,
			atd_attendancetype					= Attendance_Details.atd_attendancetype, 
			atd_deleted							= Attendance_Details.atd_deleted,
			epd_deptid							= Episodes.epd_deptid, 
			CDU_Location						= StageTimes_view.CDU_Location, 
			pat_dob								= Patient.pat_dob,
			tri_comments						= Triage_view.tri_comments,
			res_depid							= res_depid,
			res_field7							= Result_details.res_field7,
			FormName							= dbo.CMMC_Lookup_String(res_field7),
			PatientAge							= dbo.f_GetAge(Patient.pat_dob,Attendance_Details.atd_arrivaldate),
			AgeCategory							= CASE WHEN dbo.f_GetAge(Patient.pat_dob,Attendance_Details.atd_arrivaldate) >= 18 
													THEN 'Adult'
													ELSE 'Child' 
												  END

FROM CMMC_Reports.dbo.Triage_view 
LEFT OUTER JOIN CMMC_Reports.dbo.Attendance_Details Attendance_Details 
	ON Triage_view.tri_atdid=Attendance_Details.atd_id
LEFT OUTER JOIN CMMC_Reports.dbo.Episodes Episodes 
	ON Attendance_Details.atd_epdid=Episodes.epd_id
LEFT OUTER JOIN CMMC_Reports.dbo.StageTimes_view StageTimes_view 
	ON Attendance_Details.atd_id=StageTimes_view.atd_id		
LEFT OUTER JOIN CMMC_Reports.dbo.Patient Patient 
	ON Episodes.epd_pid=Patient.pat_pid
LEFT OUTER JOIN CMMC_Reports.dbo.CFG_Dept CFG_Dept 
	ON Episodes.epd_deptid=CFG_Dept.dpt_id
LEFT OUTER JOIN CMMC_Reports.dbo.Result_details
	ON Result_details.res_atdid = Attendance_Details.atd_id
	AND res_depid = '231' and res_field7 like '%18292%'
WHERE StageTimes_view.Arrival BETWEEN @StartDate AND @EndDate
AND Attendance_Details.atd_attendancetype<>1 
AND Attendance_Details.atd_deleted=0 
AND Episodes.epd_deptid=1



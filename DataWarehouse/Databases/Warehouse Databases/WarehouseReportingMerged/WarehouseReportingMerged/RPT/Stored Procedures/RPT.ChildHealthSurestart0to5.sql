﻿

CREATE procedure [RPT].[ChildHealthSurestart0to5] as



Select
      Dem.NHSNumber
      ,Dem.Surname
      ,Dem.Forename
      ,Dem.DateOfBirth
      ,Reg.RegistrationStatus
      ,SS.ChildrensCentre
      ,ss.LowerSuperOutputArea
      ,Addr.AddressSeqno
      ,Age = DATEDIFF(hour,Dem.DateofBirth, GetDate())/8766

      
From
            Warehouse.ChildHealth.Entity as Dem

Left Join Warehouse.ChildHealth.Birth as Birth
on Birth.EntitySourceUniqueID = Dem.SourceUniqueID

left join Warehouse.ChildHealth.RegistrationStatus as Reg
on reg.RegistrationStatusCode = Dem.RegistrationStatusCode

left join Warehouse.ChildHealth.EntityAddress as Addr
on addr.EntitySourceUniqueID = Dem.SourceUniqueID

left join Warehouse.ChildHealth.SureStartPostcode as SS
on ss.Postcode = Addr.Postcode

      left join
            (
            Select
                  SourceUniqueID
                  ,AgeInDays = datediff(Day, DOB.DateOfBirth, getdate()-1)
            From 
                  warehouse.ChildHealth.Entity DOB
            ) Age
      on Age.SourceUniqueID = Dem.SourceUniqueID
      
where 

            Age.AgeInDays between 1 and 1825
and   Dem.RegistrationStatusCode in ('1','2','3') 
and   Addr.AddressEndDate is null



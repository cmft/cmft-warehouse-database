﻿


	CREATE proc [RPT].[GetTraffordImmediateDischargeSummary]
	
	 @startdate datetime = null 
	,@enddate datetime = null
	


	as

	declare @SetStartDate datetime = coalesce(@StartDate, '02 April 2012')
	
	declare @SetEndDate datetime =  coalesce(@enddate,
			(select getdate() )	
				)
	
	declare @lastMonday Datetime = coalesce(@SetEndDate - 7,(select DATEADD(wk, DATEDIFF(wk,7,GETDATE()), 0) ) )

			select 
			 NationalSpecialtyCode 
			,spec_desc = NationalSpecialtyCode + ' ' + NationalSpecialty	
			,Encounter.ProviderSpellno
			,Encounter.Districtno
			,[NationalAdmissionMethodCode]
			,[NationalAdmissionMethod]
			,SpelLOS = datediff(day,Encounter.AdmissionTime,Encounter.DischargeTime)
			,Encounter.AdmissionTime
			,Encounter.DischargeTime
			,FormType = case when IDS.FormType is null then 'NoForm' else IDS.FormType end 
			,PrintDate
			,HourPrintedAfterDischare 
					=
				case when PrintDate > Encounter.Dischargetime then datediff(hour,Encounter.DischargeTime,PrintDate) else null end
				
			--,convert(datetime,convert(varchar(max),documentXML.query('data(//AdmissionDate)')),111) FormAdmissionDate
			--,convert(datetime,convert(varchar(max),documentXML.query('data(//DischargeDate)')),111) FormDischargeDate
			----,convert(varchar(max),documentXML.query('data(//DraftIDS)')) Draft
			--,convert(varchar(max),documentXML.query('data(//Version)')) Version
			--,convert(varchar(max),documentXML.query('data(//CurrentState)')) Version
			,documentXML
			
		from WarehouseOLAPMergedV2.APC.Encounter Encounter
			
			left join TraffordWarehouse.dbo.EPRImmediateDischarge IDS
			on IDS.ProviderSpellNo = Encounter.ProviderSpellNo
			 
			 	--deal with duplicate forms separately
					and not exists
							(
								select Count (*), ProviderSpellno from TraffordWarehouse.dbo.EPRImmediateDischarge c
								where c.ProviderSpellno = encounter.ProviderSpellno
								group by ProviderSpellno
								having count (*) > 1
							)
					
			 
			
			inner join WarehouseOLAPMergedV2.APC.[AdmissionMethod] AdmissionMethod
			on AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID
			and AdmissionMethod.SourceContextCode = 'TRA||UG'
			
			inner join WarehouseOLAPMergedV2.APC.PatientClassification PatientClassification
			on PatientClassification.SourcePatientClassificationID = Encounter.PatientClassificationID
			and PatientClassification.SourceContextCode = 'TRA||UG'


			
			left join [WarehouseOLAPMergedV2].[APC].[Ward] Ward
			on Ward.SourceWardID = Encounter.EndWardID
			
			
				--Gareth Cunnah 15/06/2012 required if emegancy admission then second episode should be used if found
			left join WarehouseOLAPMergedV2.APC.Encounter EncounterSecond
			on EncounterSecond.ProviderSpellNo = Encounter.ProviderSpellNo
			and EncounterSecond.ContextCode = 'TRA||UG'
			and EncounterSecond.SourceEncounterNo = 2
			and AdmissionMethod.NationalAdmissionMethodCode > 14
			
			inner join WarehouseOLAPMergedV2.WH.Specialty Specialty
			on Specialty.SourceSpecialtyID = coalesce(EncounterSecond.SpecialtyID ,Encounter.SpecialtyID)
			and Specialty.SourceContextCode = 'TRA||UG'
						
				where --TGH Data Only
  				Encounter.ContextCode = 'TRA||UG'
  				and
  				
  				DATEADD(dd, 0, DATEDIFF(dd, 0, Encounter.DischargeTime)) between @SetStartDate and @SetEndDate
  				
  				and
  				
  					 Encounter.FirstEpisodeInSpellIndicator = 1 --Gets First Episode
  		
  		
  					--gets rid of Deleted records 
  					and
  					not  exists
					(select 1 from TraffordWarehouse.dbo.apcencounter b
					where 
					IsDeleted = 1
					and
					(
 						EpisodeEndDate >= '1 Apr 2009'
					or	EpisodeEndDate is null
					 )
					and Encounter.sourceuniqueid = b.sourceuniqueid)
					
				

					
						order by dischargetime desc, providerspellno
						
						


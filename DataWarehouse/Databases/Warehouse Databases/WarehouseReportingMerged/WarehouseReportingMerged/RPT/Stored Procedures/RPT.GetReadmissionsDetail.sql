﻿

--Created 2 July 2012
--Created by Del Jones
--Get Readmissions

CREATE PROCEDURE [RPT].[GetReadmissionsDetail] 

	
 @startdate datetime = null 
,@enddate datetime = null
,@days int = null


as

declare @SetStartDate datetime = coalesce(@StartDate, '02 April 2012')

declare @SetEndDate datetime =  coalesce(@enddate,
		(select DATEADD(wk, DATEDIFF(wk,7,GETDATE()), 7) )	
			)



SELECT 
	 TheMonth
	,WeekNo
	,WeekNoKey
	,FinancialMonthKey
	,D.Division
	,S.NationalSpecialtyCode
	,S.NationalSpecialty
	,EN.CasenoteNumber
	,EN.ProviderSpellNo
	,PC.NationalPatientClassification
	,C.MainSpecialtyCode
	,C.SourceConsultant
	,EN.AdmissionTime
	,EN.DischargeTime
	,EN.PASHRGCode
	,AD.AdmissionType

	,Elective =
		CASE AD.AdmissionType
		WHEN 'Elective' THEN 1
		WHEN 'Emergency' THEN 0
		WHEN 'Maternity' Then 1
		WHEN 'Other Non-Elective' THEN 0
		ELSE 0
		END

	,R.Days
	,R.Re_admit

	,Readmit_Division = R_D.Division						
	,Readmit_NationalSpecialtyCode = R_S.NationalSpecialtyCode			
	,Readmit_NationalSpecialty = R_S.NationalSpecialty				
	,Readmit_CasenoteNumber = R_EN.CasenoteNumber				
	,Readmit_NationalPatientClassification = R_PC.NationalPatientClassification	
	,Readmit_MainSpecialtyCode = R_C.MainSpecialtyCode				
	,Readmit_SourceConsultant = R_C.SourceConsultant				
	,Readmit_AdmissionTime = R_EN.AdmissionTime					
	,Readmit_DischargeTime = R_EN.DischargeTime					
	,Readmit_PASHRGCode = R_EN.PASHRGCode					
	,Readmit_AdmissionType = R_AD.AdmissionType			
	,dbo.f_GetAge(EN.[DateOfBirth],EN.AdmissionTime) PatientAgeAtAdmission	
	
	,EN.PrimaryDiagnosisCode
	,Diagnosis.Diagnosis
	,NationalEthnicCategory
	,NationalDischargeDestination	
	,CASE WHEN ISNULL(CommunityContact,0) = 1 THEN 'Yes' ELSE 'No' END AS CommunityContact

FROM
	APC.Encounter EN

INNER JOIN WH.Calendar
	ON	EN.DischargeDate = Calendar.TheDate

INNER JOIN WH.Directorate D 
	ON	EN.StartDirectorateCode = D.DirectorateCode

INNER JOIN WH.Specialty S
	ON	EN.SpecialtyID = S.SourceSpecialtyID

INNER JOIN APC.PatientClassification PC 
	ON EN.PatientClassificationID = PC.SourcePatientClassificationID	

INNER JOIN WH.Consultant C 
	ON EN.ConsultantID = C.SourceConsultantID

inner JOIN APC.AdmissionMethod AD 
	ON EN.AdmissionMethodID = AD.SourceAdmissionMethodID
	
left JOIN WH.EthnicCategory
ON EN.EthnicOriginCode = SourceEthnicCategoryCode
and	EN.ContextCode = EthnicCategory.SourceContextCode
	
inner JOIN APC.DischargeDestination
	ON EN.DischargeDestinationID = SourceDischargeDestinationID

inner JOIN Legacy.APCReadmission R 
	ON EN.MergeEncounterRecno = R.EncounterRecno 

inner JOIN APC.Encounter R_EN 
	ON R.read_EncounterRecNo = R_EN.MergeEncounterRecno

LEFT JOIN WH.Directorate R_D 
	ON R_EN.StartDirectorateCode = R_D.DirectorateCode

LEFT JOIN WH.Specialty R_S 
	ON R_EN.SpecialtyID = R_S.SourceSpecialtyID

LEFT JOIN APC.PatientClassification R_PC 
	ON R_EN.PatientClassificationID = R_PC.SourcePatientClassificationID	

LEFT JOIN WH.Consultant R_C 
	ON R_EN.ConsultantID = R_C.SourceConsultantID

LEFT JOIN APC.AdmissionMethod R_AD 
	ON R_EN.AdmissionMethodID = R_AD.SourceAdmissionMethodID

LEFT JOIN WH.Diagnosis 
	ON EN.PrimaryDiagnosisCode = DiagnosisCode
 

WHERE
	EN.DischargeTime BETWEEN @SetStartDate and @SetEndDate
	AND EN.PatientCategoryCode <> 'DC' --AND exclusion_denominator IS NULL
	--AND Re_admit = 1 
	AND (R.Days <= @days OR ISNULL(R.Days,0) = 0 AND exclusion_denominator IS NULL)
	AND (R_AD.AdmissionType IN ('Emergency','Other Non-Elective') OR R_AD.AdmissionType IS NULL)

﻿
CREATE proc [RPT].[PortalRCodeLengthOfEncounter]


(
@User varchar(50)
)

as

select
	ClinicalCodingCompleteDate
	,IsRCode = 
					case
						when left(PrimaryDiagnosisCode, 1) = 'R'
						then 1
						else 0
					end
	,IsRCodeAndLoEUnder5 = 
				cast(
					case
						when left(PrimaryDiagnosisCode, 1) = 'R'
						and datediff(day, EpisodeStartDate, EpisodeEndDate) between 0 and 4
						then 1
						else 0
					end
				as int)
	,IsRCodeAndLoEBetween5And9 = 
				cast(
					case
						when left(PrimaryDiagnosisCode, 1) = 'R'
						and	datediff(day, EpisodeStartDate, EpisodeEndDate) between 5 and 9
						then 1
						else 0
					end
				as int)
	,IsRCodeAndLoE10AndOver =
					case
						when left(PrimaryDiagnosisCode, 1) = 'R'
						and datediff(day, EpisodeStartDate, EpisodeEndDate) >= 10
						then 1
						else 0
					end
	,LoE = datediff(day, EpisodeStartDate, EpisodeEndDate)
	,LoEGroup = 
				case
					when datediff(day, EpisodeStartDate, EpisodeEndDate) >= 10
					then '10+'
					when datediff(day, EpisodeStartDate, EpisodeEndDate) between 5 and 9
					then '5-9'
					when datediff(day, EpisodeStartDate, EpisodeEndDate) between 0 and 4
					then '0-4'				
				end
	,EpisodeCount = 1

from	
	APC.Encounter
	
inner join	WH.Consultant
on	Consultant.SourceConsultantID = Encounter.ConsultantID	

inner join	GeckoASSecurity.dbo.WindowsUserCodeMap
on	WindowsUserCodeMap.SourceValueCode = Consultant.SourceConsultantCode 
and	SourceContextCode = WindowsUserCodeMap.ContextCode 

inner join GeckoASSecurity.dbo.WindowsUserSecurityGroupMap
on	WindowsUserSecurityGroupMap.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode

inner join GeckoASSecurity.dbo.[SecurityGroupEntityTypeMap]
on	[SecurityGroupEntityTypeMap].SecurityGroupCode = WindowsUserSecurityGroupMap.SecurityGroupCode
and [EntityTypeCode] = 'CONSULTANT'
	

where
	Encounter.ClinicalCodingCompleteDate between dateadd(year, -1,  getdate()) and getdate()
and
	WindowsUserSecurityGroupMap.Userid = @User



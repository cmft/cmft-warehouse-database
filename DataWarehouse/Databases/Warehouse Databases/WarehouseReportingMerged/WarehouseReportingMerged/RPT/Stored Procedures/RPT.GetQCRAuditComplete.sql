﻿
create proc [RPT].[GetQCRAuditComplete]

	(
	@Period date
	)
as

select 
	 AuditAnswer.AuditAnswerRecno
	,AuditAnswer.AuditTime
	,TheMonth = FirstDayOfMonth
	,DivisionCode = Division.SourceDivisionCode
	,Division = Division.SourceDivision
	,Location.SourceLocationID
	,Location.SourceLocation
	,LocationType.SourceLocationTypeCode
	,LocationType.SourceLocationType
from
	QCR.AuditAnswer

inner join QCR.Location
on	AuditAnswer.LocationID = Location.SourceLocationID

inner join QCR.LocationType
on	Location.SourceLocationTypeCode = LocationType.SourceLocationTypeCode

inner join QCR.Division
on	Location.SourceDivisionCode = Division.SourceDivisionCode

inner join QCR.Question
on	AuditAnswer.QuestionID = Question.SourceQuestionID

inner join QCR.AuditType
on	AuditAnswer.AuditTypeID = AuditType.SourceAuditTypeID

inner join WH.Calendar
on	AuditAnswer.AuditDateID = Calendar.DateID

where
		Calendar.FirstDayOfMonth between dateadd(month, -7, @Period) and @Period
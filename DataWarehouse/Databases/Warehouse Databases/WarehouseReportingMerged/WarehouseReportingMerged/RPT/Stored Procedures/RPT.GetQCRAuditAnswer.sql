﻿
CREATE proc [RPT].[GetQCRAuditAnswer]

	(
	 @LocationType int
	,@Division int
	,@Location int
	,@Period date
	,@AuditType int
	)
as

select 
	 AuditAnswer.AuditAnswerRecno
	,AuditAnswer.AuditTime
	,AuditAnswer.Answer
	,TheMonth = FirstDayOfMonth
	,AuditType.SourceAuditTypeID
	,AuditType.SourceAuditType
	,Question.SourceQuestion
	,Division = Division.SourceDivision
	,Location.SourceLocation
	,LocationType.SourceLocationType
from
	QCR.AuditAnswer

inner join QCR.Location
on	AuditAnswer.LocationID = Location.SourceLocationID

inner join QCR.LocationType
on	Location.SourceLocationTypeCode = LocationType.SourceLocationTypeCode

inner join QCR.Division
on	Location.SourceDivisionCode = Division.SourceDivisionCode

inner join QCR.Question
on	AuditAnswer.QuestionID = Question.SourceQuestionID

inner join QCR.AuditType
on	AuditAnswer.AuditTypeID = AuditType.SourceAuditTypeID

inner join WH.Calendar
on	AuditAnswer.AuditDateID = Calendar.DateID

where
		Calendar.FirstDayOfMonth between dateadd(month, -13, @Period) and @Period
and
		(
			@LocationType in (LocationType.SourceLocationTypeCode)
		or
			@LocationType = 0
		)
and
		(
			@Division in (Division.SourceDivisionCode)
		or
			@Division = 0
		)
and
		(
			@Location in (Location.SourceLocationID)
		or
			@Location = 0
		)
and
		(
			@AuditType in (AuditType.SourceAuditTypeID)
		or
			@AuditType = 0
		)

﻿CREATE TABLE [Maternity].[SubmissionPeriod] (
    [SubmissionPeriod] VARCHAR (14) NOT NULL,
    [FromDate]         DATETIME     NULL,
    [ToDate]           DATETIME     NULL,
    [Created]          DATETIME     NULL,
    [Updated]          DATETIME     NULL,
    [ByWhom]           VARCHAR (50) NULL,
    [FreezeDate]       DATETIME     NULL,
    CONSTRAINT [PK_SubmissionPeriod] PRIMARY KEY CLUSTERED ([SubmissionPeriod] ASC)
);


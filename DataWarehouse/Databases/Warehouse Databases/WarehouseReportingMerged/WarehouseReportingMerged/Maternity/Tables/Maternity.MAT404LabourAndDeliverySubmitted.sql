﻿CREATE TABLE [Maternity].[MAT404LabourAndDeliverySubmitted] (
    [LabourOnsetDateTime]                VARCHAR (19) NULL,
    [CaesareanDateTime]                  VARCHAR (19) NULL,
    [LocalPatientIdMother]               VARCHAR (10) NOT NULL,
    [LabourOnsetPresentation]            VARCHAR (2)  NULL,
    [StartDateTimeMotherDeliveryHPS]     VARCHAR (19) NULL,
    [DecisionToDeliverDateTime]          VARCHAR (19) NULL,
    [ROMDateTime]                        VARCHAR (19) NULL,
    [ROMMethod]                          VARCHAR (2)  NULL,
    [ROMReason]                          VARCHAR (2)  NULL,
    [LabourOnsetSecondStageDateTime]     VARCHAR (19) NULL,
    [LabourThirdStageEndDateTime]        VARCHAR (19) NULL,
    [EpisiotomyReason]                   VARCHAR (2)  NULL,
    [PlacentaDeliveryMethod]             VARCHAR (2)  NULL,
    [DischargeDateTimeMotherDeliveryHPS] VARCHAR (19) NULL,
    [OrgCodePostnatalPathLeadProvider]   VARCHAR (5)  NULL,
    [XMLOutput]                          XML          NULL,
    [DatasetCode]                        VARCHAR (20) NULL,
    [SourceUniqueID]                     VARCHAR (50) NULL,
    [SubmissionPeriod]                   VARCHAR (20) NULL
);


﻿CREATE TABLE [Maternity].[MAT306AntenatalAppointment] (
    [LocalPatientIdMother] VARCHAR (10) NULL,
    [AntenatalAppDate]     VARCHAR (10) NULL,
    [XMLOutput]            XML          NULL,
    [DatasetCode]          VARCHAR (20) NULL,
    [SourceUniqueID]       VARCHAR (50) NULL,
    [SubmissionPeriod]     VARCHAR (20) NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_MAT306AntenatalAppointment_LocalPatientIdMother]
    ON [Maternity].[MAT306AntenatalAppointment]([LocalPatientIdMother] ASC);


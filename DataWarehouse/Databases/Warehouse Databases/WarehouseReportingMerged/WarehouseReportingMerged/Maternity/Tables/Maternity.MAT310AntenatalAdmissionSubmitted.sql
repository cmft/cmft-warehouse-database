﻿CREATE TABLE [Maternity].[MAT310AntenatalAdmissionSubmitted] (
    [LocalPatientIdMother]      VARCHAR (10) NULL,
    [HPSAntenatalStartDate]     VARCHAR (10) NULL,
    [HPSAntenatalDischargeDate] VARCHAR (10) NULL,
    [XMLOutput]                 XML          NULL,
    [DatasetCode]               VARCHAR (20) NULL,
    [SourceUniqueID]            VARCHAR (50) NULL,
    [SubmissionPeriod]          VARCHAR (20) NULL
);


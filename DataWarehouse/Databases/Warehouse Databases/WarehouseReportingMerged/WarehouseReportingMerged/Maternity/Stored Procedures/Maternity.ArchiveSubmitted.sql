﻿
CREATE proc [Maternity].[ArchiveSubmitted] as

declare @SubmissionPeriod varchar(20)

set @SubmissionPeriod = (Select max(SubmissionPeriod) from Maternity.MAT001MothersDemographics)

if exists
	(
	select
		1
	from
		Maternity.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	and	FreezeDate > getdate()
	)
begin

delete from Maternity.MAT001MothersDemographicsSubmitted where  SubmissionPeriod = @SubmissionPeriod

delete from Maternity.MAT003GPPracticeRegistrationSubmitted where  SubmissionPeriod = @SubmissionPeriod

delete from Maternity.MAT101BookingAppointmentDetailsSubmitted where   SubmissionPeriod = @SubmissionPeriod

delete from Maternity.MAT306AntenatalAppointmentSubmitted  where SubmissionPeriod = @SubmissionPeriod

delete from Maternity.MAT310AntenatalAdmissionSubmitted where   SubmissionPeriod = @SubmissionPeriod

delete from Maternity.MAT404LabourAndDeliverySubmitted  where SubmissionPeriod = @SubmissionPeriod

delete from Maternity.MAT502BabysDemographicsAndBirthDetailsSubmitted where  SubmissionPeriod = @SubmissionPeriod



insert into Maternity.MAT001MothersDemographicsSubmitted select * from Maternity.MAT001MothersDemographics where  not exists (Select 1 from Maternity.MAT001MothersDemographicsSubmitted where SubmissionPeriod = @SubmissionPeriod)

insert into Maternity.MAT003GPPracticeRegistrationSubmitted select * from Maternity.MAT003GPPracticeRegistration where  not exists (Select 1 from Maternity.MAT003GPPracticeRegistrationSubmitted where SubmissionPeriod = @SubmissionPeriod)

insert into Maternity.MAT101BookingAppointmentDetailsSubmitted select * from Maternity.MAT101BookingAppointmentDetails where  not exists (Select 1 from Maternity.MAT101BookingAppointmentDetailsSubmitted where SubmissionPeriod = @SubmissionPeriod)

insert into Maternity.MAT306AntenatalAppointmentSubmitted select * from Maternity.MAT306AntenatalAppointment where  not exists (Select 1 from Maternity.MAT306AntenatalAppointmentSubmitted where SubmissionPeriod = @SubmissionPeriod)

insert into Maternity.MAT310AntenatalAdmissionSubmitted select * from Maternity.MAT310AntenatalAdmission where  not exists (Select 1 from Maternity.MAT310AntenatalAdmissionSubmitted where SubmissionPeriod = @SubmissionPeriod)

insert into Maternity.MAT404LabourAndDeliverySubmitted select * from Maternity.MAT404LabourAndDelivery where  not exists (Select 1 from Maternity.MAT404LabourAndDeliverySubmitted where SubmissionPeriod = @SubmissionPeriod)

insert into Maternity.MAT502BabysDemographicsAndBirthDetailsSubmitted select * from Maternity.MAT502BabysDemographicsAndBirthDetails where  not exists (Select 1 from Maternity.MAT502BabysDemographicsAndBirthDetailsSubmitted where SubmissionPeriod = @SubmissionPeriod)




		
end

else

begin
	print 'Invalid submission period'
	return
end




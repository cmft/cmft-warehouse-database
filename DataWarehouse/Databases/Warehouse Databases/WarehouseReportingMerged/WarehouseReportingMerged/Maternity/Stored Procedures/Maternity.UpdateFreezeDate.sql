﻿
CREATE proc [Maternity].[UpdateFreezeDate]

 @SubmissionPeriod varchar(20)
,@From datetime = null
,@To datetime = null
,@FreezeDate datetime = null

as

set @FreezeDate = coalesce(@FreezeDate, getdate())

update WarehouseReportingMerged.Maternity.SubmissionPeriod

set 
	 FreezeDate = @FreezeDate
	,ByWhom = (Select SYSTEM_USER)
	,Updated = (Select getdate())

where SubmissionPeriod = @SubmissionPeriod

insert into WarehouseReportingMerged.Maternity.SubmissionPeriod

select 

      SubmissionPeriod = @SubmissionPeriod
     ,FromDate = @From
     ,ToDate = @To
     ,Created =(Select getdate())
	,Updated = (Select getdate())
	,ByWhom = (Select SYSTEM_USER)
	,FreezeDate = @FreezeDate
	
	

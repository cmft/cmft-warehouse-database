﻿

CREATE proc [Maternity].[SubmissionAudit] 

  @ReportType varchar (10) = null
 ,@SubmissionPeriod varchar (14) = null

as

--set @ReportType = 'Sent'

if @ReportType = 'Sent'
		begin
		select 
		
		*
	
		 from Maternity.SubmissionSummary
	end

else 
begin
	if 	@ReportType = 'Rejected'
		begin
		select 
			*
			
		
		 from Maternity.RejectedRecords
		 
		 where  
	(
    
    @SubmissionPeriod = SubmissionPeriod
	or
	@SubmissionPeriod is null
	)
end


else

	Select 
	* 
	
	
	from Maternity.DataQualityRecords
	
    where  
    
    (
    
    @SubmissionPeriod = SubmissionPeriod
	or
	@SubmissionPeriod is null
	)
end


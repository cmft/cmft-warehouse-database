﻿
CREATE view [AE].[AttendanceCategory] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAttendanceCategoryID]
      ,[SourceAttendanceCategoryCode]
      ,[SourceAttendanceCategory]
      --,[LocalAttendanceCategoryID]
      ,[LocalAttendanceCategoryCode]
      ,[LocalAttendanceCategory]
      --,[NationalAttendanceCategoryID]
      ,[NationalAttendanceCategoryCode]
      ,[NationalAttendanceCategory]
      ,[AttendanceCategoryGroup]
  FROM [WarehouseOLAPMergedV2].[AE].[AttendanceCategory]


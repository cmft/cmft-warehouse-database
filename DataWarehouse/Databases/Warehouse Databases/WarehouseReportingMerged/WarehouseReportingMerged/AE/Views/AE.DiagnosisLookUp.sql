﻿

create view [AE].[DiagnosisLookUp] 
as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceDiagnosisID]
      ,[SourceDiagnosisCode]
      ,[SourceDiagnosis]
      ,[LocalDiagnosisID]
      ,[LocalDiagnosisCode]
      ,[LocalDiagnosis]
      ,[NationalDiagnosisID]
      ,[NationalDiagnosisCode]
      ,[NationalDiagnosis]
  FROM [WarehouseOLAPMergedV2].[AE].[Diagnosis]


﻿


CREATE view AE.Treatment as

select 
	 BaseTreatment.MergeRecno
	,BaseTreatment.SequenceNo
	,BaseTreatment.TreatmentSchemeCode
	,BaseTreatment.TreatmentCode
	,BaseTreatment.TreatmentTime
	,BaseTreatment.ContextCode
	,BaseTreatmentReference.ContextID
	,BaseTreatmentReference.TreatmentID
from
	WarehouseOLAPMergedV2.AE.BaseTreatment 
inner join WarehouseOLAPMergedV2.AE.BaseTreatmentReference ON BaseTreatmentReference.MergeRecno = BaseTreatment.MergeRecno
--AND R.SequenceNo = BaseTreatment.SequenceNo



﻿


CREATE view [AE].[Diagnosis] as

select 
	 BaseDiagnosis.MergeRecno
	,BaseDiagnosis.SequenceNo
	,BaseDiagnosis.DiagnosticSchemeCode
	,BaseDiagnosis.DiagnosisCode
	,BaseDiagnosis.DiagnosisSiteCode
	,BaseDiagnosis.DiagnosisSideCode
	,BaseDiagnosis.ContextCode
	,BaseDiagnosisReference.ContextID
	,BaseDiagnosisReference.DiagnosisID
from
	WarehouseOLAPMergedV2.AE.BaseDiagnosis
inner join WarehouseOLAPMergedV2.AE.BaseDiagnosisReference ON BaseDiagnosisReference.MergeRecno = BaseDiagnosis.MergeRecno
--AND R.SequenceNo = BaseDiagnosis.SequenceNo



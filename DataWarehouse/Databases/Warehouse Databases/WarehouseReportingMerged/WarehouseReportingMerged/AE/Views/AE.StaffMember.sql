﻿
CREATE view [AE].[StaffMember] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceStaffMemberID]
      ,[SourceStaffMemberCode]
      ,[SourceStaffMember]
      --,[LocalStaffMemberID]
      ,[LocalStaffMemberCode]
      ,[LocalStaffMember]
      --,[NationalStaffMemberID]
      ,[NationalStaffMemberCode]
      ,[NationalStaffMember]
  FROM [WarehouseOLAPMergedV2].[AE].[StaffMember]


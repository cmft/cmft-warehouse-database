﻿
CREATE view [AE].[AttendanceDisposal] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAttendanceDisposalID]
      ,[SourceAttendanceDisposalCode]
      ,[SourceAttendanceDisposal]
      --,[LocalAttendanceDisposalID]
      ,[LocalAttendanceDisposalCode]
      ,[LocalAttendanceDisposal]
      --,[NationalAttendanceDisposalID]
      ,[NationalAttendanceDisposalCode]
      ,[NationalAttendanceDisposal]
  FROM [WarehouseOLAPMergedV2].[AE].[AttendanceDisposal]


﻿
create view AE.AssaultEncounter as

SELECT [MergeEncounterRecno]
      ,[AssaultDate]
      ,[AssaultTime]
      ,[AssaultWeapon]
      ,[AssaultWeaponDetails]
      ,[AssaultLocation]
      ,[AssaultLocationDetails]
      ,[AlcoholConsumed3Hour]
      ,[AssaultRelationship]
      ,[AssaultRelationshipDetails]
      ,[ContextCode]
      ,[Created]
      ,[Updated]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[AE].[BaseAssault]

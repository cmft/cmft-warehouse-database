﻿
CREATE view [AE].[Fact] as

SELECT
	 A.MergeEncounterRecno
	,A.StageID
	,A.ArrivalModeID
	,A.AttendanceDisposalID
	,A.SiteID
	,A.AttendanceCategoryID
	,A.ReferredToSpecialtyID
	,A.LevelOfCareID
	,A.EncounterBreachStatusID
	,A.EncounterStartTimeOfDay
	,A.EncounterEndTimeOfDay
	,A.EncounterStartDateID
	,A.EncounterEndDateID
	,A.StageBreachStatusID
	,A.StageDurationMinutes
	,A.StageBreachMinutes
	,A.StageStartTimeOfDay
	,A.StageEndTimeOfDay
	,A.StageStartDateID
	,A.StageEndDateID
	,A.BreachValue
	,A.DelayID
	,A.EncounterDurationMinutes
	,A.AgeID
	,A.StageDurationBandID
	,A.EncounterDurationBandID
	,A.Reportable
	,A.StageEndTimeIsWorkingHours
	,A.StageStartTimeIsWorkingHours
	,A.SexID
	,A.EthnicCategoryID
	,A.ContextID
	,A.IncidentLocationTypeID
	,A.PatientGroupID
	,A.SourceOfReferralID
	,A.TriageCategoryID
	,A.DepartmentTypeID
	,A.Cases
	,A.UnplannedReattend7DayCases
	,A.LeftWithoutBeingSeenCases
  FROM [WarehouseOLAPMergedV2].[AE].[FactEncounter] A


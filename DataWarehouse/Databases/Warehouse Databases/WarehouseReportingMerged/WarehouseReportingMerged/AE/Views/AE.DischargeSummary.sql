﻿










CREATE view [AE].[DischargeSummary] as

select
	DischargeSummaryRecno
	,AttendanceNumber
	,PatientName
	,DistrictNo
	,NHSNumber
	,DateOfBirth
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ArrivalDate
	,ArrivalTime
	,DepartureTime
	,ClinicianSeen
	,InformationForGP
	,TargetTime
	,ActualTime
	,Breach
	,DischargeSummaryXML
	,Updated
	,Created
	,AttendanceDisposalCode
	,SiteCode
	
	
from
	WarehouseOLAPMergedV2.AE.DischargeSummary
	
	where
	(
	AttendanceDisposalCode  <> '01'
	or
	AttendanceDisposalCode is null
	)
	and
	SiteCode = 'RW3MR'
	










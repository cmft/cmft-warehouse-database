﻿create view AE.Service as

SELECT [ServiceID]
      ,[ServiceCode]
      ,[Service]
  FROM [WarehouseOLAPMergedV2].[AE].[Service]
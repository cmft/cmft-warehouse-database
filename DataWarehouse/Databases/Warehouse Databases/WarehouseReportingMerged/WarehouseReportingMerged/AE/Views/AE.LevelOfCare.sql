﻿

CREATE view [AE].[LevelOfCare] as

SELECT
	 LevelOfCareID
	,LevelOfCareCode
    ,LevelOfCare
FROM
	WarehouseOLAPMergedV2.AE.LevelOfCare



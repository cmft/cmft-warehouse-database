﻿





CREATE view [AE].[Encounter] as

SELECT 
	 B.MergeEncounterRecno
	,B.EncounterRecno
	,B.SourceUniqueID
	,B.UniqueBookingReferenceNo
	,B.PathwayId
	,B.PathwayIdIssuerCode
	,B.RTTStatusCode
	,B.RTTStartDate
	,B.RTTEndDate
	,B.DistrictNo
	,B.TrustNo
	,B.CasenoteNo
	,B.DistrictNoOrganisationCode
	,B.NHSNumber
	,B.NHSNumberStatusCode
	,B.PatientTitle
	,B.PatientForename
	,B.PatientSurname
	,B.PatientAddress1
	,B.PatientAddress2
	,B.PatientAddress3
	,B.PatientAddress4
	,B.Postcode
	,B.DateOfBirth
	,B.DateOfDeath
	,B.SexCode
	,B.CarerSupportIndicator
	,B.RegisteredGpCode
	,B.RegisteredPracticeCode
	,B.AttendanceNumber
	,B.ArrivalModeCode
	,B.AttendanceCategoryCode
	,B.AttendanceDisposalCode
	,B.IncidentLocationTypeCode
	,B.PatientGroupCode
	,B.SourceOfReferralCode
	,B.ArrivalDate
	,B.ArrivalTime
	,B.AgeOnArrival
	,B.InitialAssessmentTime
	,B.SeenForTreatmentTime
	,B.AttendanceConclusionTime
	,B.DepartureTime
	,B.CommissioningSerialNo
	,B.NHSServiceAgreementLineNo
	,B.ProviderReferenceNo
	,B.CommissionerReferenceNo
	,B.ProviderCode
	,B.CommissionerCode
	,B.StaffMemberCode
	,B.InvestigationCodeFirst
	,B.InvestigationCodeSecond
	,B.DiagnosisCodeFirst
	,B.DiagnosisCodeSecond
	,B.TreatmentCodeFirst
	,B.TreatmentCodeSecond
	,B.PASHRGCode
	,B.HRGVersionCode
	,B.PASDGVPCode
	,B.SiteCode
	,B.InterfaceCode
	,B.PCTCode
	,B.ResidencePCTCode
	,B.InvestigationCodeList
	,B.TriageCategoryCode
	,B.PresentingProblemCode
	,B.PresentingProblem
	,B.ToXrayTime
	,B.FromXrayTime
	,B.ToSpecialtyTime
	,B.SeenBySpecialtyTime
	,B.EthnicCategoryCode
	,B.ReligionCode
	,B.ReferredToSpecialtyCode
	,B.DecisionToAdmitTime
	,B.DischargeDestinationCode
	,B.RegisteredTime
	,B.TransportRequestTime
	,B.TransportAvailableTime
	,B.AscribeLeftDeptTime
	,B.CDULeftDepartmentTime
	,B.PCDULeftDepartmentTime
	,B.TreatmentDateFirst
	,B.TreatmentDateSecond
	,B.SourceAttendanceDisposalCode
	,B.AmbulanceArrivalTime
	,B.AmbulanceCrewPRF
	,B.UnplannedReattend7Day
	,B.ArrivalTimeAdjusted
	,B.ClinicalAssessmentTime
	,B.LevelOfCareCode
	,B.EncounterBreachStatusCode
	,B.EncounterStartTimeOfDay
	,B.EncounterEndTimeOfDay
	,B.EncounterStartDate
	,B.EncounterEndDate
	,B.EncounterDurationMinutes
	,B.AgeCode
	,B.LeftWithoutBeingSeenCases
	,B.HRGCode
	,B.Reportable
	,B.CareGroup
	,B.EPMINo
	,B.DepartmentTypeCode
	,B.DepartureTimeAdjusted
	,B.CarePathwayAttendanceConclusionTime
	,B.CarePathwayDepartureTime
	,B.ContextCode
	,B.GpCodeAtAttendance
	,B.GpPracticeCodeAtAttendance
	,B.PostcodeAtAttendance
	,B.CCGCode
	,R.ContextID
	,R.AgeID
	,R.ArrivalModeID
	,R.AttendanceCategoryID
	,R.AttendanceDisposalID
	,R.DiagnosisFirstID
	,R.DiagnosisSecondID
	,R.EncounterStartDateID
	,R.EncounterEndDateID
	,R.EthnicOriginID
	,R.IncidentLocationTypeID
	,R.InvestigationFirstID
	,R.InvestigationSecondID
	,R.NHSNumberStatusID
	,R.PatientGroupID
	,R.ReferredToSpecialtyID
	,R.SexID
	,R.SiteID
	,R.SourceOfReferralID
	,R.StaffMemberID
	,R.TreatmentFirstID
	,R.TreatmentSecondID
	,R.TriageCategoryID
	,B.AlcoholLocation
	,B.AlcoholInPast12Hours

	,R.AttendanceResidenceCCGID
	,B.AttendanceResidenceCCGCode
	,B.ServiceID

  FROM [WarehouseOLAPMergedV2].[AE].[BaseEncounter] B
  
  JOIN [WarehouseOLAPMergedV2].[AE].[BaseEncounterReference] R 
  ON R.MergeEncounterRecno = B.MergeEncounterRecno 








﻿
CREATE view [AE].[IncidentLocationType] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceIncidentLocationTypeID]
      ,[SourceIncidentLocationTypeCode]
      ,[SourceIncidentLocationType]
      --,[LocalIncidentLocationTypeID]
      ,[LocalIncidentLocationTypeCode]
      ,[LocalIncidentLocationType]
      --,[NationalIncidentLocationTypeID]
      ,[NationalIncidentLocationTypeCode]
      ,[NationalIncidentLocationType]
  FROM [WarehouseOLAPMergedV2].[AE].[IncidentLocationType]


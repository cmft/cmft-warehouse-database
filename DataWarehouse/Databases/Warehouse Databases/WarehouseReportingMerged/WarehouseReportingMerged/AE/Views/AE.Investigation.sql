﻿


CREATE view [AE].[Investigation] as

SELECT 
	 BaseInvestigation.MergeEncounterRecno
	,BaseInvestigation.SequenceNo
	,BaseInvestigation.InvestigationSchemeCode
	,BaseInvestigation.InvestigationCode
	,BaseInvestigation.InvestigationTime
	,BaseInvestigation.ResultTime
	,BaseInvestigation.ContextCode
	,BaseInvestigationReference.ContextID
	,BaseInvestigationReference.InvestigationID
	
FROM [WarehouseOLAPMergedV2].[AE].[BaseInvestigation] BaseInvestigation

left JOIN [WarehouseOLAPMergedV2].[AE].[BaseInvestigationReference] BaseInvestigationReference 
ON BaseInvestigation.MergeEncounterRecno = BaseInvestigationReference.MergeRecno



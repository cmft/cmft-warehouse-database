﻿













CREATE view [AE].[DischargeSummaryAll] as

select
	 DischargeSummaryRecno
	,AttendanceNumber
	,PatientName
	,DistrictNo
	,NHSNumber
	,DateOfBirth
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,ArrivalDate
	,ArrivalTime
	,DepartureTime
	,ClinicianSeen
	,InformationForGP
	,TargetTime
	,ActualTime
	,Breach
	,DischargeSummaryXML
	,Updated
	,Created
	,AttendanceDisposalCode
	,SiteCode
	,ContextCode 
	,DocumentID = ContextCode + '|' +cast(DischargeSummary.AESourceUniqueID as varchar (100))
	,SafeguardingConcerns 
from
	WarehouseOLAPMergedV2.AE.DischargeSummary DischargeSummary
	

	













﻿


CREATE View [Flag].[CurrentInpatientWithLearningDisabilityBySystem]

as


Select 
	EncounterRecno -- This will become MergeEncounterRecno once we bring through Trafford, ETL process for APCUpdate started in Dev
	,SourceSystem.SourceSystemID
	,SourceSystem.SourceSystem
	,PatientSourceSystemFlag.PatientDatasetFlagRecno
from 
	APCUpdate.Encounter

inner join Match.Match.MatchTypeDataset 
on Encounter.EncounterRecno = MatchTypeDataset.TargetDatasetRecno
and MatchTypeDataset.MatchTypeID = 8

inner join WarehouseOLAPMergedV2.Flag.PatientSourceSystemFlag
on MatchTypeDataset.SourceDatasetRecno = PatientSourceSystemFlag.PatientDatasetFlagRecno

inner join WarehouseOLAPMergedV2.Flag.SourceSystem
on PatientSourceSystemFlag.SourceSystemID = SourceSystem.SourceSystemID

where 
	EpisodeEndDate is null
	




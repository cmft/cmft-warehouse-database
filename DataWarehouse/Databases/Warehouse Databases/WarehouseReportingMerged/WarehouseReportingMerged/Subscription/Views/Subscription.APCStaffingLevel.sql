﻿



CREATE view [Subscription].[APCStaffingLevel] as

SELECT 
	   [SubscriptionID]
      ,[ReportName]
      ,[To]
      ,[Cc]
      ,[Bcc]
      ,[ReplyTo]
      ,[IncludeReport]
      ,[RenderFormat]
      ,[Priority]
      ,[Subject]
      ,[Comment]
      ,[IncludeLink]
      ,[Parameter1Value]
	  ,[Parameter2Value] = 
					(
					select
						LastDayOfWeek
					from
						WarehouseReportingMerged.WH.Calendar
					where
						TheDate = convert(date,getdate()-7)
					)
      ,[Parameter3Value]
      ,[Parameter4Value]
      ,[Parameter5Value]
      ,[Parameter6Value]
      ,[Parameter7Value]
      ,[Parameter8Value]
      ,[Parameter9Value]
      ,[Parameter10Value]
FROM 
	[WarehouseReportingMerged].[WH].[ReportSubscription]
where 
	ReportName = 'APC Staffing Level'
and ActiveFlag = 1



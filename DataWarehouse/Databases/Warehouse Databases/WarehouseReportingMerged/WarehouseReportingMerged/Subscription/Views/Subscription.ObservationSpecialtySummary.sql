﻿




CREATE view [Subscription].[ObservationSpecialtySummary]


as

select 
	[SubscriptionID]
	,[ReportName]
	,[To]
	,[Cc]
	,[Bcc]
	,[ReplyTo]
	,[IncludeReport]
	,[RenderFormat]
	,[Priority]
	,[Subject]
	,[Comment]
	,[IncludeLink]
	,[Parameter1Value]
	,[Parameter2Value]		
	,[Parameter3Value]	
	,[Parameter4Value]
	,[Parameter5Value] = 
				(
				select
					'[Calendar].[Financial Hierarchy].[Month].&[' + cast(FinancialMonthKey as varchar) + ']'
				from
					WarehouseReportingMerged.WH.Calendar 
				where 
					TheDate = cast(dateadd(month, -1, getdate()) as date)
				)
	,[Parameter6Value]
	,[Parameter7Value]
	,[Parameter8Value]
	,[Parameter9Value]
	,[Parameter10Value]
from 
	[WarehouseReportingMerged].[WH].[ReportSubscription]
where 
	ReportName = 'Observation Specialty Summary'
and ActiveFlag = 1







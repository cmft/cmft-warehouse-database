﻿
CREATE view Subscription.[ReportSubscription] as

select
	Report = Catalog.Name 
	,Subscription= Subscriptions.Description 
	,JobID = Schedule.ScheduleID 
from
	ReportServerSPIntegrated.dbo.Subscriptions

inner join ReportServerSPIntegrated.dbo.ReportSchedule
ON	ReportSchedule.SubscriptionID = Subscriptions.SubscriptionID

inner join ReportServerSPIntegrated.dbo.Schedule
ON	ReportSchedule.ScheduleID = Schedule.ScheduleID

inner join ReportServerSPIntegrated.dbo.Catalog
ON	ReportSchedule.ReportID = Catalog.ItemID
and Subscriptions.Report_OID = Catalog.ItemID

--ORDER BY CAST(Schedule.ScheduleID AS VARCHAR(100))
﻿
create view Theatre.TimetableTemplate as 

select
	BaseTimetableTemplate.MergeRecno
	,TimetableTemplateCode
	,SessionNumber
	,DayNumber
	,TheatreCode
	,SessionStartTime
	,SessionEndTime
	,ConsultantCode
	,AnaesthetistCode
	,SpecialtyCode
	,LogLastUpdated
	,RecordLogTemplates
	,ContextCode

	,ContextID
	,AnaesthetistID
	,ConsultantID
	,SpecialtyID
	,TheatreID

from
	WarehouseOLAPMergedV2.Theatre.BaseTimetableTemplate
inner join WarehouseOLAPMergedV2.Theatre.BaseTimetableTemplateReference
on BaseTimetableTemplate.MergeRecno = BaseTimetableTemplateReference.MergeRecno




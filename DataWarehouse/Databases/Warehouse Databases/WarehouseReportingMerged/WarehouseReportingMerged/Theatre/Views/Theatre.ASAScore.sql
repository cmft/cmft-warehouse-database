﻿

create view [Theatre].[ASAScore] as 

select
	[SourceContextCode]
	,[SourceContext]
	,[SourceASAScoreID]
	,[SourceASAScoreCode]
	,[SourceASAScore]
	,[LocalASAScoreID]
	,[LocalASAScoreCode]
	,[LocalASAScore]
	,[NationalASAScoreID]
	,[NationalASAScoreCode]
	,[NationalASAScore]
from
	[WarehouseOLAPMergedV2].[Theatre].[ASAScore]
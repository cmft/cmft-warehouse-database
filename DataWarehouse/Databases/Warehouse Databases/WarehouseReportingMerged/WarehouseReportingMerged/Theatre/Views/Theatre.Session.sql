﻿

create view Theatre.Session as
select
	BaseSession.MergeRecno
	,SourceUniqueID
	,ActualSessionMinutes
	,PlannedSessionMinutes
	,StartTime
	,EndTime
	,SessionNumber
	,TheatreCode
	,ConsultantCode
	,SpecialtyCode
	,SurgeonCode1
	,SurgeonCode2
	,AnaesthetistCode1
	,AnaesthetistCode2
	,AnaesthetistCode3
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaestheticNurseCode
	,LastUpdated
	,TimetableDetailCode
	,Comment
	,SessionOrder
	,CancelledFlag
	,CancelledMinutes
	,OverrunReason
	,OverrunReasonDate
	,OverrunReasonStaffCode
	,ContextCode

	,ContextID
	,AnaesthetistID
	,ConsultantID
	,SpecialtyID
	,TheatreID

from
	WarehouseOLAPMergedV2.Theatre.BaseSession

inner join WarehouseOLAPMergedV2.Theatre.BaseSessionReference
on	BaseSession.MergeRecno = BaseSessionReference.MergeRecno


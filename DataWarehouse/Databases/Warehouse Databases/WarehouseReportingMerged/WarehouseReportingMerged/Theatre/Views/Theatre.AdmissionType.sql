﻿
create view Theatre.AdmissionType as 

select
	[SourceContextCode]
	,[SourceContext]
	,[SourceAdmissionTypeID]
	,[SourceAdmissionTypeCode]
	,[SourceAdmissionType]
	,[LocalAdmissionTypeID]
	,[LocalAdmissionTypeCode]
	,[LocalAdmissionType]
	,[NationalAdmissionTypeID]
	,[NationalAdmissionTypeCode]
	,[NationalAdmissionType]
from
	[WarehouseOLAPMergedV2].[Theatre].[AdmissionType]
﻿
create view [Theatre].[Staff] as 

select
	[SourceContextCode]
	,[SourceContext]
	,[SourceStaffID]
	,[SourceStaffCode]
	,[SourceStaff]
	,[LocalStaffID]
	,[LocalStaffCode]
	,[LocalStaff]
	,[NationalStaffID]
	,[NationalStaffCode]
	,[NationalStaff]
	,[StaffCategoryCode]
	,[StaffCategory]
	,[StaffType]
	,[StaffCode1]
	,[SourceSpecialtyID]
from
	[WarehouseOLAPMergedV2].[Theatre].[Staff]
﻿
create view [APCWL].[WaitType] as

SELECT [WaitTypeID]
      ,[WaitType]
      ,[ParentWaitTypeID]
  FROM [WarehouseOLAPMergedV2].[APCWL].[WaitType]

﻿



create view APCWL.WaitingList as

SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceWaitingListID]
      ,[SourceWaitingListCode]
      ,[SourceWaitingList]
      ,[LocalWaitingListID]
      ,[LocalWaitingListCode]
      ,[LocalWaitingList]
      ,[NationalWaitingListID]
      ,[NationalWaitingListCode]
      ,[NationalWaitingList]
  FROM [WarehouseOLAPMergedV2].[APCWL].[WaitingList]

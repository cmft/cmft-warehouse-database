﻿
CREATE view CAMHS.MHS001MPI
as


select 
	LocalPatientId	
	,OrgCodeLocalPatientId	
	,OrgCodeResidenceResp	
	,OrgCodeEduEstab	
	,NHSNumber	
	,NHSNumberStatus	
	,PersonBirthDate	
	,Postcode	
	,PostcodeMainVisitor	
	,Gender	
	,EthnicCategory	
	,Religion	
	,LanguageCodePreferred	
	,PersDeathDate	
from 
	CAMHS.MHS001MPIAdd


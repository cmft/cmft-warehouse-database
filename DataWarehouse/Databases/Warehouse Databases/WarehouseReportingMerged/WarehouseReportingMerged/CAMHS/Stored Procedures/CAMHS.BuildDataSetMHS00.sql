﻿
CREATE proc CAMHs.BuildDataSetMHS00
	@FromDate datetime
	,@ToDate datetime 
as

/*==============================================================================================
Description:

When		Who				What
20151223	Paul Egan		Created CYPHS 
20160126	Rachel Royston	Created CAMHs version
20160211	Rachel Royston	Excluded bulk exclusion update (LC confirmed 10/02/2016 result of validation, open referrals which should of been closed >12mths ago)

===============================================================================================*/
	
/* ================== MHS001MPI ================== */
	
truncate table CAMHS.MHS001MPIAdd

insert into CAMHS.MHS001MPIAdd
	(
	LocalPatientId
	,OrgCodeLocalPatientId
	,OrgCodeResidenceResp 
	,OrgCodeEduEstab
	,NHSNumber 
	,NHSNumberStatus 
	,PersonBirthDate
	,Postcode
	,PostcodeMainVisitor
	,Gender 
	,EthnicCategory
	,Religion
	,LanguageCodePreferred 
	,PersDeathDate
	,PASSourcePatientNo
	,ReferralOrAdmissionDate
	,DischargeDate
	,MergeRecno
	,MPIContextCode
	,SourceContextCode
	,SourcePatientNo
	)

select  
	LocalPatientId
	,OrgCodeLocalPatientId = 'RW3'
	,OrgCodeResidenceResp
	,OrgCodeEduEstab 
	,NHSNumber
	,NHSNumberStatus = 
		case
				when Encounter.NHSNumberStatus in ('02','07') then Encounter.NHSNumberStatus
				when left(NHSNumberStatus.NationalNHSNumberStatusCode,3) = 'N||' then '03'
				else NHSNumberStatus.NationalNHSNumberStatusCode
			end
			 -- Guidance http://www.hscic.gov.uk/media/18152/MHSDS-v10-User-Guidance/pdf/MHSDS_v1.0_User_Guidance.pdf
		
	,PersonBirthDate 
	,Postcode 
	,PostcodeMainVisitor 
	,Gender = 
		coalesce(
			Sex.NationalSexCode
			,'X'
			)
	,EthnicCategory = 
		coalesce(
			case
				when EthnicCategory.NationalEthnicCategoryCode = 'N||ETHCAT' then '99'
				else EthnicCategory.NationalEthnicCategoryCode
			end
			,'99'
			)
	,Religion = 
		coalesce(
			left(Religion.NationalReligionCode,1)
			,'N'
			)
	,LanguageCodePreferred 
	,PersDeathDate 
	,PASSourcePatientNo
	,ReferralOrAdmissionDate
	,DischargeDate =
		case
			when DischargeDate >  @ToDate then null 
			else DischargeDate
		end
	,MergeRecno
	,MPIContextCode
	,SourceContextCode = Encounter.SourceContextCode
	,SourcePatientNo = Encounter.SourcePatientNo
from
	(	
	select  
		LocalPatientId = 
			coalesce(
				Patient.DistrictNo  -- Advised to use District No so it is consistent across all data sets.  Issue with CORC in that not everyone is on PAS.  LC confirmed 10/02/2016 we need to bring all CORC through.
				,cast(RFEncounter.SourcePatientNo as varchar)
				)
		,OrgCodeLocalPatientId = RFEncounter.SourcePatientNo 
		,OrgCodeResidenceResp = null
		,OrgCodeEduEstab = null
		,NHSNumber = 
			coalesce(
				replace(Patient.NHSNumber,' ','')
				,RFEncounter.NHSNumber
				)
		,NHSNumberStatus = 
			case 
				when Patient.NHSNumber is null and RFEncounter.NHSNumber is not null then '02'
				when Patient.NHSNumber is null then '07'
				else Patient.NHSNumberStatusId
			end
		,PersonBirthDate = 
			coalesce(
				Patient.DateOfBirth
				,RFEncounter.DateOfBirth
				)
		,Postcode = 
			coalesce(
				Patient.Postcode
				,RFEncounter.Postcode 
				,'ZZ99 3WZ'
				)
		,PostcodeMainVisitor = null
		,SexID = RFReference.SexID
		,EthnicCategoryID = RFReference.EthnicCategoryID
		,ReligionID = RFReference.ReligionID
		,LanguageCodePreferred = null
		,PersDeathDate = Patient.DateOfDeath
		,PASSourcePatientNo
		,ReferralOrAdmissionDate = RFEncounter.ReferralReceivedDate
		,DischargeDate = RFEncounter.DischargeDate
		,MergeRecno = RFEncounter.MergeRecno
		,SourceContextCode = RFEncounter.ContextCode
		,MPIContextCode = 
			case
				when Patient.SourcePatientNo is not null then 'CEN||PAS'
				else RFEncounter.ContextCode
			end
		,SourcePatientNo = RFEncounter.SourcePatientNo
	from 
		WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth RFEncounter

	inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealthReference RFReference
	on RFEncounter.MergeRecno = RFReference.MergeRecno

	left join Warehouse.PAS.Patient
	on RFEncounter.PASSourcePatientNo = Patient.SourcePatientNo
	
	where
		(
			ReferralReceivedDate between @FromDate and @ToDate -- By using this any activity in Jan won't link to a referral if referral in an earlier period. GC to question 12/02/2016
		or 
			DischargeDate between @FromDate and @ToDate
		or 
			exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
				where 
					RFEncounter.SourceUniqueID = OPEncounter.RFSourceUniqueID
				and OPEncounter.AppointmentDate between @FromDate and @ToDate	
				)
		)
		--coalesce(
		--	RFEncounter.DischargeDate
		--	,getdate()
		--	) 
		-->= @FromDate
	
	--and RFEncounter.ReferralReceivedDate <= @ToDate
	
	and coalesce(
			DischargeReasonCode
			,''
			) 
		<> 1383 -- Bulk Exclusion update
	and not exists
		(
		select
			1
		from
			WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth Latest
		where
			coalesce(
				Latest.PASSourcePatientNo
				,Latest.SourcePatientNo
				)
			= coalesce(
				RFEncounter.PASSourcePatientNo
				,RFEncounter.SourcePatientNo
				)
		and coalesce(Latest.DischargeDate,getdate()) >= @FromDate
		and 
			(
				Latest.ReferralReceivedDate between @FromDate and @ToDate
			or 
				Latest.DischargeDate between @FromDate and @ToDate
			or 
				exists
					(
					select
						1
					from
						WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
					where 
						Latest.SourceUniqueID = OPEncounter.RFSourceUniqueID
					and OPEncounter.AppointmentDate between @FromDate and @ToDate	
					)
			)
		and Latest.SourceUniqueID > RFEncounter.SourceUniqueID
		)
	)
	Encounter	

left join WH.NHSNumberStatus
ON	NHSNumberStatus.SourceNHSNumberStatusCode = Encounter.NHSNumberStatus
and NHSNumberStatus.SourceContextCode = 'CEN||PAS'

left join WH.EthnicCategory
on Encounter.EthnicCategoryID = EthnicCategory.SourceEthnicCategoryID

left join WH.Sex Sex
on Encounter.SexID = Sex.SourceSexID

left join WH.Religion
on Encounter.ReligionID = Religion.SourceReligionID

	
-- Append any GH if LocalPatientID not already present

insert into CAMHS.MHS001MPIAdd
	(
	LocalPatientId
	,OrgCodeLocalPatientId
	,OrgCodeResidenceResp 
	,OrgCodeEduEstab
	,NHSNumber 
	,NHSNumberStatus 
	,PersonBirthDate
	,Postcode
	,PostcodeMainVisitor
	,Gender 
	,EthnicCategory
	,Religion
	,LanguageCodePreferred 
	,PersDeathDate
	,PASSourcePatientNo
	,ReferralOrAdmissionDate
	,DischargeDate
	,MergeRecno
	,MPIContextCode
	,SourceContextCode
	,SourcePatientNo
	)
select  
	LocalPatientId
	,OrgCodeLocalPatientId = 'RW3'
	,OrgCodeResidenceResp
	,OrgCodeEduEstab 
	,NHSNumber
	,NHSNumberStatus = 
		coalesce(
			case
				when NHSNumberStatus.NationalNHSNumberStatusCode = 'N||NHSNST' then '03'
				else NHSNumberStatus.NationalNHSNumberStatusCode
			end
			,'07' -- Guidance http://www.hscic.gov.uk/media/18152/MHSDS-v10-User-Guidance/pdf/MHSDS_v1.0_User_Guidance.pdf
			)
	,PersonBirthDate 
	,Postcode 
	,PostcodeMainVisitor 
	,Gender = 
		coalesce(
			Sex.NationalSexCode
			,'X'
			)
	,EthnicCategory = 
		coalesce(
			case
				when EthnicCategory.NationalEthnicCategoryCode = 'N||ETHCAT' then '99'
				else EthnicCategory.NationalEthnicCategoryCode
			end
			,'99'
			)
	,Religion = 
		coalesce(
			left(Religion.NationalReligionCode,1)
			,'N'
			)
	,LanguageCodePreferred 
	,PersDeathDate 
	,PASSourcePatientNo
	,ReferralOrAdmissionDate
	,DischargeDate =
		case
			when DischargeDate >  @ToDate then null 
			else DischargeDate
		end
	,MergeRecno
	,MPIContextCode
	,SourceContextCode = Encounter.SourceContextCode
	,SourcePatientNo = Encounter.SourcePatientNo
from
	(	
	select 
		LocalPatientId = APCEncounter.DistrictNo  -- Advised to use District No so it is consistent across all data sets
		,OrgCodeLocalPatientId = APCEncounter.SourcePatientNo -- SourcePatientNo I've used PAS to make the GP script easier, is this oka.
		,OrgCodeResidenceResp = null
		,OrgCodeEduEstab = null
		,NHSNumber = APCEncounter.NHSNumber
		,NHSNumberStatus = APCEncounter.NHSNumberStatusCode
		,PersonBirthDate = APCEncounter.DateOfBirth
		,Postcode = 
			coalesce(
				APCEncounter.Postcode
				,'ZZ99 3WZ'
				)
		,PostcodeMainVisitor = null
		,SexID = APCReference.SexID
		,EthnicCategoryID = APCReference.EthnicOriginID
		,ReligionID = APCReference.ReligionID
		,LanguageCodePreferred = null
		,PersDeathDate = APCEncounter.DateOfDeath
		,PASSourcePatientNo = SourcePatientNo
		,ReferralOrAdmissionDate = APCEncounter.AdmissionDate
		,DischargeDate = APCEncounter.DischargeDate
		,MergeRecno = APCEncounter.MergeEncounterRecno
		,SourceContextCode = APCEncounter.ContextCode
		,MPIContextCode = APCEncounter.ContextCode
		,SourcePatientNo = APCEncounter.SourcePatientNo
	
	from  
		 WarehouseOLAPMergedV2.APC.BaseEncounter APCEncounter

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseChildrenAdolescentMentalHealth Bridge
	on Bridge.MergeEncounterRecno = APCEncounter.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference APCReference
	on  APCReference.MergeEncounterRecno = APCEncounter.MergeEncounterRecno	

	where
		(
			AdmissionDate between @FromDate and @ToDate
		or DischargeDate between @FromDate and @ToDate
		or exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealthHomeLeave
			
			inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealth
			on BaseChildrenAdolescentMentalHealthHomeLeave.APCSourceUniqueID = BaseChildrenAdolescentMentalHealth.SourceUniqueID
			
			where 
				BaseChildrenAdolescentMentalHealth.MergeRecno = Bridge.MergeRecno
			and
				(
					StartDate between @FromDate and @ToDate
				or ReturnDate between @FromDate and @ToDate
				)
			)
		)
	--	coalesce(APCEncounter.DischargeDate,getdate()) >= @FromDate
	
	--and APCEncounter.AdmissionDate <= @ToDate
	
	and Reportable = 1
	
	)
	Encounter	

left join WH.NHSNumberStatus
ON	NHSNumberStatus.SourceNHSNumberStatusCode = Encounter.NHSNumberStatus
and NHSNumberStatus.SourceContextCode = 'CEN||PAS'

left join WH.EthnicCategory
on Encounter.EthnicCategoryID = EthnicCategory.SourceEthnicCategoryID

left join WH.Sex Sex
on Encounter.SexID = Sex.SourceSexID

left join WH.Religion
on Encounter.ReligionID = Religion.SourceReligionID

where 
	not exists
		(
		Select 
			1
		from 
			CAMHS.MHS001MPIAdd Present
		where 
			Present.LocalPatientId = Encounter.LocalPatientId
		)	
;

print 'MHS001MPI'


/* ================== MHS002GP ================== */

truncate table CAMHS.MHS002GPAdd

insert into CAMHS.MHS002GPAdd
	(
	LocalPatientId
	,GMPCodeReg
	,StartDateGMPRegistration
	,EndDateGMPRegistration
	,OrgCodeGPPrac
	,MPIContextCode 
	)
select 
	LocalPatientId -- FK to MHS001MPI on LocalPatientID (District No)
	,GMPCodeReg = 
		coalesce(
			PatientGp.GpPracticeCode
			,'V81999' -- not known
			)
	,StartDateGMPRegistration = 
		case
			when EffectiveFromDate = '1 Jan 1900' then null
			when EffectiveFromDate < PersonBirthDate then PersonBirthDate -- 3 DQ issues for Jan submission logged with DQ team, would hopefully like to take this out but need to set up a DQ report so these can be dealt with
			else EffectiveFromDate
		end
	,EndDateGMPRegistration = 
		case
			when EffectiveToDate > @ToDate then null
			else EffectiveToDate
		end
	,OrgCodeGPPrac = 
		case
			when ParentOrganisationCode = '5F5' then '01G' -- This is not ideal, need to set something up so we can get the CCG codes instead of the old PCT codes???
			when ParentOrganisationCode = '5NR' then '02A'
			else ParentOrganisationCode
		end
	,MPIContextCode = MHS001MPIAdd.MPIContextCode
from  
	CAMHS.MHS001MPIAdd

left join Warehouse.PAS.PatientGp
on MHS001MPIAdd.PASSourcePatientNo = PatientGp.SourcePatientNo
and MHS001MPIAdd.PASSourcePatientNo is not null
and coalesce(
		EffectiveToDate 
		,getdate()
		)
	>= @FromDate --coalesce(EffectiveToDate,getdate()) >= MHS001MPIAdd.ReferralOrAdmissionDate -- record rejected when including all GPs for the open referral, error message, 
and coalesce(
		EffectiveFromDate
		,'1 Jan 1900'
		) 
	<= @ToDate

left join WH.GPPractice
on PatientGp.GpPracticeCode = GPPractice.GpPracticeCode

;
print 'MHS002GP'


/* ================== MHS005PatInd ================== */

truncate table CAMHS.MHS005PatInd

insert into CAMHS.MHS005PatInd
	(
	LocalPatientId
	,ConstSuperReqDueToDis
	,YoungCarer
	,LACStatus
	,CPP
	,ProPsychDate
	,EmerPsychDate
	,ManPsychDate
	,PsychPrescDate
	,PsychTreatDate
	)
select 
	LocalPatientId -- FK to MHS001MPI on LocalPatientID (District No)
	,ConstSuperReqDueToDis = null
	,YoungCarer = 
		case
			when YoungCarer = 0 then 'N'
			when YoungCarer = 1 then 'Y'
			when YoungCarer = 999 then 'X'
			else null
		end
	,LACStatus =  
		case
			when LookedAfterChild = 0 then 'N'
			when LookedAfterChild = 1 then 'Y'
			when LookedAfterChild = 999 then 'X'
			else null
		end
	,CPP = NationalChildProtectionRegisterCode
	,ProPsychDate = null
	,EmerPsychDate = null
	,ManPsychDate = null
	,PsychPrescDate = null
	,PsychTreatDate = null
from  
	CAMHS.MHS001MPIAdd

left join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth RFEncounter
on MHS001MPIAdd.SourcePatientNo = RFEncounter.SourcePatientNo
and MHS001MPIAdd.SourceContextCode = RFEncounter.ContextCode

left join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealthReference RFReference
on RFEncounter.MergeRecno = RFReference.MergeRecno

left join WarehouseOLAPMergedV2.WH.ChildProtectionRegister
on RFReference.ChildProtectionRegisterID = ChildProtectionRegister.SourceChildProtectionRegisterID

where
	(
		MHS001MPIAdd.SourceContextCode = 'CEN||PAS'
	or
		RFEncounter.ReferralReceivedDate between @FromDate and @ToDate
	or
		RFEncounter.DischargeDate between @FromDate and @ToDate
	or
		exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
			where 
				RFEncounter.SourceUniqueID = OPEncounter.RFSourceUniqueID
			and OPEncounter.AppointmentDate between @FromDate and @ToDate	
			)
			
		--(
		--	coalesce(RFEncounter.DischargeDate,getdate()) >= @FromDate 
		--and RFEncounter.ReferralReceivedDate <= @ToDate
		--)
	)
	
and not exists
	(
	select 
		1
	from 		
		WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth Latest
	where
		Latest.SourcePatientNo = RFEncounter.SourcePatientNo
	and coalesce(Latest.DischargeDate,getdate()) >= @FromDate 
	and (
			Latest.ReferralReceivedDate between @FromDate and @ToDate
		or Latest.DischargeDate between @FromDate and @ToDate
		or
			exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
				where 
					Latest.SourceUniqueID = OPEncounter.RFSourceUniqueID
				and OPEncounter.AppointmentDate between @FromDate and @ToDate	
				)
		)
	and Latest.SourceUniqueID > RFEncounter.SourceUniqueID
	)



;

print 'MHS005PatInd'

/* ================== MHS008CrisisPlan ================== */

truncate table CAMHS.MHS008CrisisPlan

insert into CAMHS.MHS008CrisisPlan
	(
	LocalPatientId
	,MHCrisisPlanCreatDate
	,MHCrisisPlanLastUpdateDate
	)

select 
	LocalPatientId -- FK to MHS001MPI on LocalPatientID (District No)
	,MHCrisisPlanCreatDate = RFEncounter.CrisisPlanCreationDate
	,MHCrisisPlanLastUpdateDate = RFEncounter.CrisisPlanLastUpdatedDate 

from  
	CAMHS.MHS001MPIAdd
	
inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth RFEncounter
on MHS001MPIAdd.SourcePatientNo = RFEncounter.SourcePatientNo
and MHS001MPIAdd.SourceContextCode = RFEncounter.ContextCode

where
	(
		RFEncounter.ReferralReceivedDate between @FromDate and @ToDate
	or
		RFEncounter.DischargeDate between @FromDate and @ToDate
	or
		exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
			where 
				RFEncounter.SourceUniqueID = OPEncounter.RFSourceUniqueID
			and OPEncounter.AppointmentDate between @FromDate and @ToDate	
			)
	)	
and RFEncounter.CrisisPlanCreationDate < @ToDate

;

print 'MHS008CrisisPlan'



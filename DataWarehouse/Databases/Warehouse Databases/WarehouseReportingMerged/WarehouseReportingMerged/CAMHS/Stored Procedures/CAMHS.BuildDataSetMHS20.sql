﻿
CREATE proc CAMHs.BuildDataSetMHS20 
	@FromDate datetime
	,@ToDate datetime 
as

/*==============================================================================================
Description:

When		Who				What
20160126	Rachel Royston	Created CAMHs version
===============================================================================================*/


/* ================== MHS201CareContact ================== */

-- include all appointments including DNAs and cancellations.

truncate table CAMHS.MHS201CareContact

insert into CAMHS.MHS201CareContact
	(
	CareContactId
	,ServiceRequestId
	,CareProfTeamLocalId
	,CareContDate
	,CareContTime
	,OrgCodeComm
	,AdminCatCode
	,ClinContDurOfCareCont
	,ConsType
	,CareContSubj
	,ConsMediumUsed
	,ActLocTypeCode
	,SiteCodeTreat
	,GroupTherapyInd
	,AttendOrDNACode
	,EarliestReasonOfferDate
	,EarliestClinAppDate
	,CareContCancelDate
	,CareContCancelReas
	,RepApptOfferDate
	,RepApptBookDate
	)
select 
	CareContactId = OPEncounter.SourceUniqueID
	,ServiceRequestId = MHS101Referral.ServiceRequestId -- Foreign Key to MHS101Referral
	,CareProfTeamLocalId = ServiceTypeCode -- requests the Local ID
	,CareContDate = AppointmentDate
	,CareContTime = AppointmentTime
	,OrgCodeComm = -- Logic agreed by TN 29/01/2016
		case
			--Scotland
			when left(OrganisationCodeResidence.DistrictOfResidence,1) = 'S' then OrganisationCodeResidence.CCGCode
			--NI
			when left(OrganisationCodeResidence.DistrictOfResidence,1) = 'Z' then OrganisationCodeResidence.CCGCode
			--Wales
			when left(MHS002GP.OrgCodeGPPrac,1) = '7' and left(OrganisationCodeResidence.CCGCode,1) = '7' -- Welsh residence and Welsh practice
			then OrganisationCodeResidence.CCGCode 
			when left(MHS002GP.OrgCodeGPPrac,1) = '7' 
			then MHS002GP.OrgCodeGPPrac
			else
				coalesce(
					MHS002GP.OrgCodeGPPrac -- Organisation code of GP practice
					, OrganisationCodeResidence.CCGCode -- Organisation code of patient's postcode
					,'00W' -- Central Manchester CCG
					)
			end
	,AdminCatCode = '01' -- LC advised default NHS
	,ClinContDurOfCareCont = Duration
	,ConsType = 
		Case
			when FirstAttendance = 1 then '01'
			else '02'
		end
	,CareContSubj = 
		case
			when left(NationalCareContactSubjectCode ,3) = 'N||'
			then null
			else NationalCareContactSubjectCode
		end
	,ConsMediumUsed = 
		case
			when left(AppointmentType.NationalAppointmentTypeCode,2) = 'N|'
			then null
			else AppointmentType.NationalAppointmentTypeCode
		end
	,ActLocTypeCode = -- where not face to face location of patient should be used
		case
			when left(Location.NationalLocationCode ,3) = 'N||'
			then null
			else Location.NationalLocationCode 
		end
	,SiteCodeTreat = null -- LC TOS
	,GroupTherapyInd = 
		coalesce(
			left(YesNo,1)
			,'Z'
			)
	,AttendOrDNACode = 
		case
			when left(NationalAttendanceStatusCode ,3) = 'N||'
			then null
			else NationalAttendanceStatusCode
		end
	,EarliestReasonOfferDate = null -- LC TOS
	,EarliestClinAppDate = null -- LC TOS
	,CareContCancelDate = null -- LC TOS
	,CareContCancelReas = null -- LC TOS
	,RepApptOfferDate = null -- LC TOS
	,RepApptBookDate = null -- LC TOS
from  
	CAMHS.MHS001MPIAdd MHS001MPI

inner join CAMHS.MHS101Referral
on MHS001MPI.LocalPatientId = MHS101Referral.LocalPatientId

inner join CAMHS.MHS102ServiceTypeReferredTo
on MHS101Referral.ServiceRequestId = MHS102ServiceTypeReferredTo.ServiceRequestId
	
inner join WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
on MHS101Referral.ServiceRequestId = 
	(
			OPEncounter.ContextCode 
			+ '||'
			+ cast (OPEncounter.RFSourceUniqueID as varchar)-- Primary Key
			)

inner join WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealthReference OPReference
on OPEncounter.MergeRecno = OPReference.MergeRecno

left join CAMHS.MHS002GPAdd MHS002GP
on MHS001MPI.LocalPatientId = MHS002GP.LocalPatientId
and MHS001MPI.MPIContextCode = MHS002GP.MPIContextCode
and AppointmentDate between 
		coalesce(
			MHS002GP.StartDateGMPRegistration
			,'1 Jan 1900'
			)
	 and 
		coalesce(
			MHS002GP.EndDateGMPRegistration
			,getdate()
			)

left join Organisation.ODS.Postcode OrganisationCodeResidence
on	OrganisationCodeResidence.Postcode =
		case
		when datalength(rtrim(ltrim(MHS001MPI.Postcode))) = 6 then left(MHS001MPI.Postcode, 2) + '   ' + right(MHS001MPI.Postcode, 3)
		when datalength(rtrim(ltrim(MHS001MPI.Postcode))) = 7 then left(MHS001MPI.Postcode, 3) + '  ' + right(MHS001MPI.Postcode, 3)
		else MHS001MPI.Postcode
		end

left join WarehouseOLAPMergedV2.WH.Location
on OPReference.LocationTypeID = Location.SourceLocationID

left join WarehouseOLAPMergedV2.OP.AppointmentType
on OPReference.AppointmentTypeID = AppointmentType.SourceAppointmentTypeID

left join WarehouseOLAPMergedV2.WH.YesNo
on OPEncounter.GroupDiary = YesNo.YesNoFlag

left join WarehouseOLAPMergedV2.OP.AttendanceStatus
on OPReference.AttendanceOutcomeID = AttendanceStatus.SourceAttendanceStatusID

left join WarehouseOLAPMergedV2.WH.CareContactSubject
on OPReference.CareContactSubjectID = CareContactSubject.SourceCareContactSubjectID

where
	AppointmentDate between @FromDate and @ToDate

;

print 'MHS201CareContact'

/* ================== MHS202CareActivity ================== */

truncate table CAMHS.MHS202CareActivity

insert into CAMHS.MHS202CareActivity
	(
	CareActId
	,CareContactId
	,CareProfLocalId
	,ClinContactDurOfCareAct
	,ProcSchemeInUse
	,CodeProc
	,FindSchemeInUse
	,CodeFind
	,ObsSchemeInUse
	,CodeObs
	,ObsValue
	,UnitMeasure
	)
select 
	CareActId = (cast(OPMeasure.SourceUniqueID as varchar) + '||' + cast(OPMeasure.SequenceNo as varchar))-- primary key 
	,CareContactId = MHS201CareContact.CareContactId -- foreign key to Care contact table
	,CareProfLocalId = OPEncounter.ClinicianID
	,ClinContactDurOfCareAct = Duration
	,ProcSchemeInUse = null -- LC TOS
	,CodeProc = null -- LC TOS
	,FindSchemeInUse = null -- LC TOS
	,CodeFind = null -- LC TOS
	,ObsSchemeInUse = null -- LC TOS
	,CodeObs = null -- LC TOS
	,ObsValue = null -- LC TOS
	,UnitMeasure = null -- LC TOS
from  
	CAMHS.MHS201CareContact

inner join WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
on MHS201CareContact.CareContactId = OPEncounter.SourceUniqueID

inner join WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealthOutcomeMeasure OPMeasure
on OPEncounter.SourceUniqueID = OPMeasure.OPSourceUniqueID


;
print 'MHS202CareActivity'



/* ================== MHS607CodedScoreAssessmentCont ================== */

truncate table CAMHS.MHS607CodedScoreAssessmentCont

insert into CAMHS.MHS607CodedScoreAssessmentCont
	(
	CareActId
	,CodedAssToolType
	,PersScore
	)
select 
	CareActId = MHS202CareActivity.CareActId -- foreign key back to MHS202CareActivity
	,CodedAssToolType = SnomedCT
	,PersScore = cast(cast(Score as decimal(5,1)) as varchar)
from  
	CAMHS.MHS202CareActivity
	
inner join WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealthOutcomeMeasure OPMeasure
on MHS202CareActivity.CareActId = (cast(OPMeasure.SourceUniqueID as varchar) + '||' + cast(OPMeasure.SequenceNo as varchar)) 

where
	SnomedCT is not null
;
print 'MHS607CodedScoreAssessmentCont'



/* ================== MHS301GroupSession ================== */

truncate table CAMHS.MHS301GroupSession

insert into CAMHS.MHS301GroupSession
	(
	GroupSessId
	, GroupSessDate
	, OrgCodeComm
	, ClinContDurOfGroupSess
	, GroupSessType
	, NumberOfGroupSessParticip
	, ActLocTypeCode
	, SiteCodeTreat
	, CareProfLocalId
	, ServTeamTypeRefToMH
	, NHSServAgreeLineNum
	)
select 
	GroupSessId = AppointmentGroupCode
	, GroupSessDate = AppointmentDate
	, OrgCodeComm = MHS201CareContact.OrgCodeComm -- as with care activity, pull from GP, or could this create dupls, need to check,
	, ClinContDurOfGroupSess = Duration
	, GroupSessType = '03' -- LC advised to use default
	, NumberOfGroupSessParticip = NumberOfParticipants
	, ActLocTypeCode = 
		case
			when left(Location.NationalLocationCode,3) = 'N||'
			then null
			else Location.NationalLocationCode
		end
	, SiteCodeTreat = null -- advised by LC
	, CareProfLocalId = ClinicianID
	, ServTeamTypeRefToMH = 
		case
			when left(ServiceType.NationalServiceTypeCode,3) = 'N||'
			then null
			else ServiceType.NationalServiceTypeCode
		end
	, NHSServAgreeLineNum = null -- advised by Lc
from  
	CAMHS.MHS201CareContact
	
inner join WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
on MHS201CareContact.CareContactId = OPEncounter.SourceUniqueID

inner join WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealthReference OPReference
on OPEncounter.MergeRecno = OPReference.MergeRecno

left join WarehouseOLAPMergedV2.WH.Location
on OPReference.LocationTypeID = Location.SourceLocationID

left join WarehouseOLAPMergedV2.WH.ServiceType
on OPReference.ServiceTypeID = ServiceType.SourceServiceTypeID

where
	GroupDiary = 1
and AppointmentGroupCode is not null
;

print 'MHS301GroupSession'



﻿
CREATE proc CAMHs.BuildDataSetMHS50
	@FromDate datetime
	,@ToDate datetime 
as

/*==============================================================================================
Description:

When		Who				What
20160126	Rachel Royston	Created CAMHs version
===============================================================================================*/


/* ================== MHS501HospProvSpell ================== */

-- Need to ensure all admissions have a linked referral on CORC, so we can link to MPI/GP/Referrals

truncate table CAMHS.MHS501HospProvSpell

insert into CAMHS.MHS501HospProvSpell
	(
	HospProvSpellNum
	,ServiceRequestId
	,StartDateHospProvSpell
	,StartTimeHospProvSpell
	,SourceAdmCodeHospProvSpell
	,AdmMethCodeHospProvSpell
	,PlannedDischDateHospProvSpell
	,DischDateHospProvSpell
	,DischTimeHospProvSpell
	,DischMethCodeHospProvSpell
	,DischDestCodeHospProvSpell
	)
select 
	HospProvSpellNum = ProviderSpellNo 
	,ServiceRequestId = MHS101Referral.ServiceRequestId
	,StartDateHospProvSpell = APCEncounter.AdmissionDate
	,StartTimeHospProvSpell = APCEncounter.AdmissionTime
	,SourceAdmCodeHospProvSpell = 
		coalesce(
			NationalAdmissionSourceCode
			,'99'
			)
	,AdmMethCodeHospProvSpell = 
		coalesce(
			NationalAdmissionMethodCode
			,'99'
			)
	,PlannedDischDateHospProvSpell = ExpectedDateofDischarge
	,DischDateHospProvSpell = MHS101Referral.ServDischDate
	,DischTimeHospProvSpell = 
		case
			when MHS101Referral.ServDischDate is null then null
			else DischargeTime
		end
	,DischMethCodeHospProvSpell = 
		case
			when MHS101Referral.ServDischDate is null then null
			else coalesce(
					NationalDischargeMethodCode
					,'99'
					)
		end
	,DischDestCodeHospProvSpell = 
		case
			when MHS101Referral.ServDischDate is null then null
			else coalesce(
					NationalDischargeDestinationCode
					,'99'
					)
		end
from  
	 CAMHS.MHS101Referral

inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealth APCCAMHS
on MHS101Referral.ServiceRequestId = 
			(
			APCCAMHS.ContextCode 
			+ '||'
			+ CAST(APCCAMHS.SourceUniqueID as varchar)
			)
			
inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseChildrenAdolescentMentalHealth Bridge
on Bridge.MergeRecno = APCCAMHS.MergeRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounter APCEncounter
on Bridge.MergeEncounterRecno = APCEncounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference APCReference
on APCReference.MergeEncounterRecno = APCEncounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.AdmissionSource
on APCReference.AdmissionSourceID = AdmissionSource.SourceAdmissionSourceID

left join WarehouseOLAPMergedV2.APC.AdmissionMethod
on APCReference.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID

left join WarehouseOLAPMergedV2.APC.DischargeMethod
on APCReference.DischargeMethodID = DischargeMethod.SourceDischargeMethodID

left join WarehouseOLAPMergedV2.APC.DischargeDestination
on APCReference.DischargeDestinationID = DischargeDestination.SourceDischargeDestinationID

;
print 'MHS501HospProvSpell'


/* ================== MHS502WardStay ================== */

truncate table CAMHS.MHS502WardStay

insert into CAMHS.MHS502WardStay
	(
	WardStayId
	,HospProvSpellNum
	,StartDateWardStay
	,StartTimeWardStay
	,EndDateWardStay
	,EndTimeWardStay
	,SiteCodeOfTreat
	,WardType
	,WardSexTypeCode
	,IntendClinCareIntenCodeMH
	,WardSecLevel
	)
select 
	WardStayId = BaseWardStay.MergeEncounterRecno
	,HospProvSpellNum = MHS501HospProvSpell.HospProvSpellNum
	,StartDateWardStay = StartDate
	,StartTimeWardStay = StartTime
	,EndDateWardStay = 
		case
			when EndDate > @ToDate then null
			else EndDate
		end
	,EndTimeWardStay = 
		case
			when EndTime > @ToDate then null
			else EndTime
		end
	,SiteCodeOfTreat = Site.NationalSiteCode
	,WardType = 
		case 
			when WardCode = '87' then '01' -- LC advised 10/02/2016
			else '02'
		end
	,WardSexTypeCode = 8 -- LC advised 10/02/2016
	,IntendClinCareIntenCodeMH = null
	,WardSecLevel = 0 -- LC advised 10/02/2016
from  
	 CAMHS.MHS501HospProvSpell

inner join WarehouseOLAPMergedV2.APC.BaseWardStay
on BaseWardStay.ProviderSpellNo = MHS501HospProvSpell.HospProvSpellNum

left join WarehouseOLAPMergedV2.WH.Site
on BaseWardStay.SiteCode = Site.SourceSiteCode
and BaseWardStay.ContextCode = Site.SourceContextCode

where
	coalesce(EndDate ,getdate()) > @FromDate
and StartDate < @ToDate
;
print 'MHS502WardStay'


/* ================== MHS503AssignedCareProf ================== */

truncate table CAMHS.MHS503AssignedCareProf
-- we may only need to submit the MH episodes only, eg some patients admitted under Gastro then transferred to GH, or Paeds then transferred

insert into CAMHS.MHS503AssignedCareProf
	(
	HospProvSpellNum
	,CareProfLocalId
	,StartDateAssCareProf
	,EndDateAssCareProf
	,TreatFuncCodeMH
	)
select 
	HospProvSpellNum = MHS501HospProvSpell.HospProvSpellNum 
	,CareProfLocalId = BaseEncounter.ConsultantCode
	,StartDateAssCareProf = BaseEncounter.EpisodeStartDate
	,EndDateAssCareProf = 
		case	
			when BaseEncounter.EpisodeEndDate > @ToDate 
			then null
			else BaseEncounter.EpisodeEndDate 
		end
	,TreatFuncCodeMH = Specialty.NationalSpecialtyCode
from  
	 CAMHS.MHS501HospProvSpell

inner join WarehouseOLAPMergedV2.APC.BaseEncounter 
on MHS501HospProvSpell.HospProvSpellNum = BaseEncounter.ProviderSpellNo

left join WarehouseOLAPMergedV2.WH.Specialty
on BaseEncounter.SpecialtyCode = Specialty.SourceSpecialtyCode
and BaseEncounter.ContextCode = Specialty.SourceContextCode

where
	coalesce(EpisodeEndDate ,getdate()) > @FromDate
and EpisodeStartDate < @ToDate	
;
print 'MHS503AssignedCareProf'

/* ================== MHS504DelayedDischarge ================== */

truncate table CAMHS.MHS504DelayedDischarge

insert into CAMHS.MHS504DelayedDischarge
	(
	HospProvSpellNum
	,StartDateDelayDisch
	,EndDateDelayDisch
	,DelayDischReason
	,AttribToIndic
	)
select 
	HospProvSpellNum = MHS501HospProvSpell.HospProvSpellNum 
	,StartDateDelayDisch = CAMHS.DischargeDelayDate
	,EndDateDelayDisch = 
		case
			when CAMHS.DischargeDelayResolvedDate > @ToDate
			then null
			else CAMHS.DischargeDelayResolvedDate 
		end
	,DelayDischReason = 
		coalesce(
			case
				when LEFT(NationalDischargeDelayReasonCode,3) = 'N||'
				then 'Z1'
				else NationalDischargeDelayReasonCode
			end
			,'Z1'
			)
	,AttribToIndic = 
		case
			when LEFT(OtherDischargeDelayReasonCode,3) = 'O||'
			then null
			else OtherDischargeDelayReasonCode
		end
from  
	 CAMHS.MHS501HospProvSpell

inner join  WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealth CAMHS
on MHS501HospProvSpell.ServiceRequestID = 
			(
			CAMHS.ContextCode 
			+ '||'
			+ CAST(CAMHS.SourceUniqueID as varchar)
			)

inner join WarehouseOLAPMergedV2.WH.DischargeDelayReason
on CAMHS.DischargeDelayReason = DischargeDelayReason.SourceDischargeDelayReasonCode
and CAMHS.ContextCode = DischargeDelayReason.SourceContextCode

where
	coalesce(DischargeDelayResolvedDate ,getdate()) > @FromDate
and DischargeDelayDate < @ToDate	

;
print 'MHS504DelayedDischarge'


/* ================== MHS509HomeLeave ================== */

truncate table CAMHS.MHS509HomeLeave

insert into CAMHS.MHS509HomeLeave
	(
	WardStayId
	,StartDateHomeLeave
	,StartTimeHomeLeave
	,EndDateHomeLeave
	,EndTimeHomeLeave
	)
select 
	WardStayId = MHS502WardStay.WardStayId
	,StartDateHomeLeave = CAMHSHome.StartDate
	,StartTimeHomeLeave = null
	,EndDateHomeLeave = 
		case
			when CAMHSHome.ReturnDate > @ToDate
			then null
			else CAMHSHome.ReturnDate 
		end
	,EndTimeHomeLeave = null
from  
	 CAMHS.MHS502WardStay

inner join WarehouseOLAPMergedV2.APC.BaseWardStayBaseChildrenAdolescentMentalHealthHomeLeave Bridge
on MHS502WardStay.WardStayId = Bridge.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealthHomeLeave CAMHSHome
on CAMHSHome.MergeRecno = Bridge.MergeRecno

where
	coalesce(ReturnDate ,getdate()) > @FromDate
and StartDate < @ToDate	
;
print 'MHS509HomeLeave'


/* ================== MHS512HospSpellComm ================== */
/*  Mandatory fields are the first 3, need to see if we can identify the start date of the commissioner
truncate table CAMHS.MHS512HospSpellComm

insert into CAMHS.MHS512HospSpellComm
	(
	HospProvSpellNum
	,OrgCodeComm
	,StartDateOrgCodeComm
	,EndDateOrgCodeComm
	)
select 
	HospProvSpellNum
	,OrgCodeComm = 
		case
			--Scotland
			when left(OrganisationCodeResidence.DistrictOfResidence,1) = 'S' then OrganisationCodeResidence.CCGCode
			--NI
			when left(OrganisationCodeResidence.DistrictOfResidence,1) = 'Z' then OrganisationCodeResidence.CCGCode
			--Wales
			when left(MHS002GP.OrgCodeGPPrac,1) = '7' and left(OrganisationCodeResidence.CCGCode,1) = '7' -- Welsh residence and Welsh practice
			then OrganisationCodeResidence.CCGCode 
			when left(MHS002GP.OrgCodeGPPrac,1) = '7' 
			then MHS002GP.OrgCodeGPPrac
			else
				coalesce(
					MHS002GP.OrgCodeGPPrac 
					, OrganisationCodeResidence.CCGCode 
					,'00W' 
					)
			end
	,StartDateOrgCodeComm = StartDateGMPRegistration -- Don't think this is right, this is start date with GP, not CCG.  Would need to do some fiddly queries which looks at consecutive CCG per GP move to identify when the move commissioner...
	,EndDateOrgCodeComm = EndDateGMPRegistration
from  
	 CAMHS.MHS501HospProvSpell

inner join CAMHS.MHS101Referral
on MHS501HospProvSpell.ServiceRequestId = MHS101Referral.ServiceRequestId

inner join CAMHS.MHS001MPI
on MHS101Referral.LocalPatientId = MHS001MPI.LocalPatientId

left join CAMHS.MHS002GP
on MHS001MPI.LocalPatientId = MHS002GP.LocalPatientId
and StartDateHospProvSpell between 
		coalesce(
			MHS002GP.StartDateGMPRegistration
			,'1 Jan 1900'
			)
	 and 
		coalesce(
			MHS002GP.EndDateGMPRegistration
			,getdate()
			)

left join Organisation.ODS.Postcode OrganisationCodeResidence
on	OrganisationCodeResidence.Postcode =
		case
		when datalength(rtrim(ltrim(MHS001MPI.Postcode))) = 6 then left(MHS001MPI.Postcode, 2) + '   ' + right(MHS001MPI.Postcode, 3)
		when datalength(rtrim(ltrim(MHS001MPI.Postcode))) = 7 then left(MHS001MPI.Postcode, 3) + '  ' + right(MHS001MPI.Postcode, 3)
		else MHS001MPI.Postcode
		end

;
print 'MHS512HospSpellComm'

*/
-- CareProfTeamLocalId = 'GalaxyHouseInpatient'
	

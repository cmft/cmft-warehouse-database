﻿

-- CAMHS.BuildDataSet '1 Jan 2016','31 Jan 2016'

CREATE Procedure CAMHS.BuildDataSet 
	@FromDate datetime 
	,@ToDate datetime
as

exec CAMHS.BuildDataSetMHS00 @FromDate ,@ToDate;
exec CAMHS.BuildDataSetMHS10 @FromDate ,@ToDate;
exec CAMHs.BuildDataSetMHS20 @FromDate ,@ToDate;
exec CAMHs.BuildDataSetMHS50 @FromDate ,@ToDate;
-- exec CAMHs.BuildDataSetMHS50 @FromDate ,@ToDate; -- Staff details, need to think how we do this, CORC, PAS??  
-- CareProfessionalLocalID = ClinicianID for CORC ConsultantCode for PAS



﻿
CREATE proc CAMHs.BuildDataSetMHS10
	@FromDate datetime
	,@ToDate datetime 
as

/*==============================================================================================
Description:

When		Who				What
20160126	Rachel Royston	Created CAMHs version
===============================================================================================*/


/* ================== MHS101Referral ================== */


truncate table CAMHS.MHS101Referral

insert into CAMHS.MHS101Referral
	(
	ServiceRequestId
	,LocalPatientId
	,OrgCodeComm
	,ReferralRequestReceivedDate
	,ReferralRequestReceivedTime
	,NHSServAgreeLineNum
	,SourceOfReferralMH
	,RefOrgCode
	,ReferringCareProfessionalStaffGroup
	,ClinRespPriorityType
	,PrimReasonReferralMH
	,ServDischDate
	,DischLetterIssDate
	)
select 
	ServiceRequestId 
	,LocalPatientId = Encounter.LocalPatientId
	,OrgCodeComm = -- Logic agreed by TN 29/01/2016
		case
			--Scotland
			when left(OrganisationCodeResidence.DistrictOfResidence,1) = 'S' then OrganisationCodeResidence.CCGCode
			--NI
			when left(OrganisationCodeResidence.DistrictOfResidence,1) = 'Z' then OrganisationCodeResidence.CCGCode
			--Wales
			when left(MHS002GP.OrgCodeGPPrac,1) = '7' and left(OrganisationCodeResidence.CCGCode,1) = '7' -- Welsh residence and Welsh practice
			then OrganisationCodeResidence.CCGCode 
			when left(MHS002GP.OrgCodeGPPrac,1) = '7' 
			then MHS002GP.OrgCodeGPPrac
			else
				coalesce(
					MHS002GP.OrgCodeGPPrac -- Organisation code of GP practice
					, OrganisationCodeResidence.CCGCode -- Organisation code of patient's postcode
					,'00W' -- Central Manchester CCG
					)
			end
	,ReferralRequestReceivedDate 
	,ReferralRequestReceivedTime 
	,NHSServAgreeLineNum
	,SourceOfReferralMH 
	,RefOrgCode 
	,ReferringCareProfessionalStaffGroup
	,ClinRespPriorityType
	,PrimReasonReferralMH 
	,ServDischDate = 
		case
			when ServDischDate > @ToDate then null
			else ServDischDate
		end
	,DischLetterIssDate = 
		case
			when DischLetterIssDate > @ToDate then null
			else DischLetterIssDate
		end
from
	(	
	select 
		ServiceRequestId = 
			(
			RFEncounter.ContextCode 
			+ '||'
			+ cast (RFEncounter.SourceUniqueID as varchar)-- Primary Key
			)
		,LocalPatientId = MHS001MPIAdd.LocalPatientId -- FK to MHS001MPI on LocalPatientID (District No)
		,ReferralRequestReceivedDate = ReferralReceivedDate
		,ReferralRequestReceivedTime = null
		,NHSServAgreeLineNum = null 
		,SourceOfReferralMH = 
			case 
				when left(SourceOfReferral.NationalSourceOfReferralCode,3) = 'N||'
				then null
				else SourceOfReferral.NationalSourceOfReferralCode
			end
		,RefOrgCode = null
		,ReferringCareProfessionalStaffGroup = 
			case 
				when left(ReferralGroup.NationalReferralGroupCode,3) = 'N||'
				then null
				else ReferralGroup.NationalReferralGroupCode
			end
		,ClinRespPriorityType = 
			case 
				when left(PriorityType.NationalPriorityTypeCode,3) = 'N||' 
				then null
				else PriorityType.NationalPriorityTypeCode
			end
		,PrimReasonReferralMH = null
		,ServDischDate = RFEncounter.DischargeDate
		,DischLetterIssDate = DischargeLetterDate
		,MHS001MPIAdd.Postcode
		,MHS001MPIAdd.MPIContextCode
	from  
		CAMHS.MHS001MPIAdd
		
	inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth RFEncounter
	on MHS001MPIAdd.SourcePatientNo = RFEncounter.SourcePatientNo
	and MHS001MPIAdd.SourceContextCode = RFEncounter.ContextCode
			
	inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealthReference RFReference
	on RFEncounter.MergeRecno = RFReference.MergeRecno

	left join WarehouseOLAPMergedV2.OP.SourceOfReferral
	on RFReference.SourceOfReferralID = SourceOfReferral.SourceSourceOfReferralID

	left join WarehouseOLAPMergedV2.WH.ReferralGroup
	on RFReference.SourceOfReferralGroupID = ReferralGroup.SourceReferralGroupID 

	left join WarehouseOLAPMergedV2.WH.PriorityType 
	on RFReference.PriorityID = PriorityType.SourcePriorityTypeID
	
	where
		(
			RFEncounter.ReferralReceivedDate between @FromDate and @ToDate
		or
			RFEncounter.DischargeDate between @FromDate and @ToDate
		or
			exists
				(
				select
					1
				from
					WarehouseOLAPMergedV2.OP.BaseChildrenAdolescentMentalHealth OPEncounter
				where 
					RFEncounter.SourceUniqueID = OPEncounter.RFSourceUniqueID
				and OPEncounter.AppointmentDate between @FromDate and @ToDate	
				)
		)
	--	coalesce(RFEncounter.DischargeDate,getdate()) >= @FromDate 
	--and RFEncounter.ReferralReceivedDate <= @ToDate
	
	and coalesce(
			DischargeReasonCode
			,''
			) 
		<> 1383 -- Bulk Exclusion update
	union
	
	select 
		ServiceRequestId = 
			(
			APCCAMHS.ContextCode 
			+ '||'
			+ CAST(APCCAMHS.SourceUniqueID as varchar)
			)
		,LocalPatientId = APCEncounter.DistrictNo -- FK to MHS001MPI on LocalPatientID (District No)
		,ReferralRequestReceivedDate = 
			coalesce(
				APCCAMHS.ReferralReceivedDate
				,APCCAMHS.AdmissionDate
				)
		,ReferralRequestReceivedTime = null
		,NHSServAgreeLineNum = null 
		
		,SourceOfReferralMH = null -- 10/02/2016 sent to LC for mapping - ReferralSourceCAMHS.NationalReferralSourceCAMHSCode
		,RefOrgCode = null
		,ReferringCareProfessionalStaffGroup = null
		,ClinRespPriorityType = 
			case
				when UrgentAdmission = 1 then 2
				else 3
			end
		,PrimReasonReferralMH = null
		,ServDischDate = APCEncounter.DischargeDate
		,DischLetterIssDate = DischargeLetterSentDate
		,APCEncounter.Postcode
		,APCEncounter.ContextCode
	from 
		WarehouseOLAPMergedV2.APC.BaseEncounter APCEncounter
		
	inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseChildrenAdolescentMentalHealth Bridge
	on Bridge.MergeEncounterRecno = APCEncounter.MergeEncounterRecno
	
	inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealth APCCAMHS
	on APCCAMHS.MergeRecno = Bridge.MergeRecno
	
	inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealthReference APCCAMHSReference
	on APCCAMHSReference.MergeRecno = APCCAMHS.MergeRecno
	
	--left join WarehouseOLAPMergedv2.WH.ReferralSourceCAMHS
	--on APCCAMHSReference.ReferralSourceID = ReferralSourceCAMHS.SourceReferralSourceCAMHSID
	
	where
		(
			APCEncounter.AdmissionDate between @FromDate and @ToDate
		or
			APCEncounter.DischargeDate between @FromDate and @ToDate
		or exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealthHomeLeave
			
			inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealth
			on BaseChildrenAdolescentMentalHealthHomeLeave.APCSourceUniqueID = BaseChildrenAdolescentMentalHealth.SourceUniqueID
			
			where 
				BaseChildrenAdolescentMentalHealth.MergeRecno = Bridge.MergeRecno
			and
				(
					StartDate between @FromDate and @ToDate
				or ReturnDate between @FromDate and @ToDate
				)
			)
		)
	--	coalesce(APCEncounter.DischargeDate,getdate()) >= @FromDate
	--and APCEncounter.AdmissionDate <= @ToDate
	and Reportable = 1
	
	) Encounter

left join CAMHS.MHS002GPAdd MHS002GP
on Encounter.LocalPatientId = MHS002GP.LocalPatientId
and Encounter.MPIContextCode = MHS002GP.MPIContextCode
and Encounter.ReferralRequestReceivedDate between 
		coalesce(
			MHS002GP.StartDateGMPRegistration
			,'1 Jan 1900'
			)
	 and 
		coalesce(
			MHS002GP.EndDateGMPRegistration
			,getdate()
			)

left join Organisation.ODS.Postcode OrganisationCodeResidence
on	OrganisationCodeResidence.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end	
;

print 'MHS101Referral'

/* ================== MHS102ServiceTypeReferredTo ================== */

truncate table CAMHS.MHS102ServiceTypeReferredTo

insert into CAMHS.MHS102ServiceTypeReferredTo
	(
	CareProfTeamLocalId
	,ServiceRequestId
	,ServTeamTypeRefToMH
	,CAMHSTier
	,ReferClosureDate
	,ReferRejectionDate
	,ReferClosReason
	,ReferRejectReason
	)
	
select 
	CareProfTeamLocalId = SourceServiceTypeCode
	,ServiceRequestId = MHS101Referral.ServiceRequestId -- Foreign Key to MHS101Referral
	,ServTeamTypeRefToMH = 
		case
			when left(NationalServiceTypeCode,3) = 'N||' 
			then 'A10'
			else
				coalesce(
					left(NationalServiceTypeCode,3)
					,'A10'
					)
		end
	,CAMHSTier = 
		case
			when left(NationalServiceTierCode,3) = 'N||'
			then null
			else NationalServiceTierCode
		end
	,ReferClosureDate = MHS101Referral.ServDischDate
	,ReferRejectionDate = 
		case
			when DischargeReason.SourceDischargeReason = 'Inappropriate referral - FNOTS'
			then MHS101Referral.ServDischDate
			else null
		end
	,ReferClosReason = 
		case
			when MHS101Referral.ServDischDate is null
			then null
			when left(DischargeReason.NationalDischargeReasonCode,3)  = 'N||'
			then null
			else left(DischargeReason.NationalDischargeReasonCode,2)
		end
	,ReferRejectReason = 
		case
			when MHS101Referral.ServDischDate is null
			then null
			when left(RejectionReason.NationalRejectionReasonCode,3)  = 'N||'
			then null
			else left(RejectionReason.NationalRejectionReasonCode,2)
		end
from  
	CAMHS.MHS101Referral
	
inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth RFEncounter
on MHS101Referral.ServiceRequestId = 
			(
			RFEncounter.ContextCode 
			+ '||'
			+ cast (RFEncounter.SourceUniqueID as varchar)-- Primary Key
			)
			
inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealthReference RFReference
on RFEncounter.MergeRecno = RFReference.MergeRecno

left join WarehouseOLAPMergedV2.WH.DischargeReason
on RFReference.DischargeReasonID = DischargeReason.SourceDischargeReasonID

left join WarehouseOLAPMergedV2.WH.ServiceType
on RFReference.ServiceTypeReferredToID = ServiceType.SourceServiceTypeID

left join WarehouseOLAPMergedV2.WH.ServiceTier
on RFReference.ServiceTierReferredToID = ServiceTier.SourceServiceTierID

left join WarehouseOLAPMergedV2.WH.RejectionReason
on RFReference.ReferralRejectionID = RejectionReason.SourceRejectionReasonID


insert into CAMHS.MHS102ServiceTypeReferredTo
	(
	CareProfTeamLocalId
	,ServiceRequestId
	,ServTeamTypeRefToMH
	,CAMHSTier
	,ReferClosureDate
	,ReferRejectionDate
	,ReferClosReason
	,ReferRejectReason
	)
	
select 
	CareProfTeamLocalId = 'GalaxyHouseInpatient'
	,ServiceRequestId = MHS101Referral.ServiceRequestId  -- Foreign Key to MHS101Referral
	,ServTeamTypeRefToMH = 'A10'
	,CAMHSTier = 4
	,ReferClosureDate = MHS101Referral.ServDischDate
	,ReferRejectionDate = null
	,ReferClosReason = null
	,ReferRejectReason = null
from  
	CAMHS.MHS101Referral

inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealth APCCAMHS
on MHS101Referral.ServiceRequestId = 
			(
			APCCAMHS.ContextCode 
			+ '||'
			+ CAST(APCCAMHS.SourceUniqueID as varchar)
			)	
			
inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseChildrenAdolescentMentalHealth Bridge
on APCCAMHS.MergeRecno = Bridge.MergeRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounter APCEncounter
on Bridge.MergeEncounterRecno = APCEncounter.MergeEncounterRecno

;

print 'MHS102ServiceTypeReferredTo'

/* ================== MHS104 Referral to Treatment ================== */



/* ================== MHS105OnwardReferral ================== */

truncate table CAMHS.MHS105OnwardReferral
insert into CAMHS.MHS105OnwardReferral
	(
	ServiceRequestId
	,OnwardReferDate
	,OnwardReferReason 
	,OrgCodeReceiving
	)
select 
	ServiceRequestId = MHS101Referral.ServiceRequestId -- Foreign Key to MHS101Referral
	,OnwardReferDate = MHS101Referral.ServDischDate
	,OnwardReferReason = null
	,OrgCodeReceiving = null
from  
	CAMHS.MHS101Referral
	
inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealth RFEncounter
on MHS101Referral.ServiceRequestId = 
			(
			RFEncounter.ContextCode 
			+ '||'
			+ cast (RFEncounter.SourceUniqueID as varchar)-- Primary Key
			)

inner join WarehouseOLAPMergedV2.RF.BaseChildrenAdolescentMentalHealthReference RFReference
on RFEncounter.MergeRecno = RFReference.MergeRecno

left join WarehouseOLAPMergedV2.WH.DischargeReason
on RFReference.DischargeReasonID = DischargeReason.SourceDischargeReasonID

where
	SourceDischargeReason = 'Transfer to Other Service'
and DischargeDate between @FromDate and @ToDate


insert into CAMHS.MHS105OnwardReferral
	(
	ServiceRequestId
	,OnwardReferDate
	,OnwardReferReason 
	,OrgCodeReceiving
	)
select 
	ServiceRequestId = MHS101Referral.ServiceRequestId -- Foreign Key to MHS101Referral
	,OnwardReferDate = APCCAMHS.DischargeDate
	,OnwardReferReason = null
	,OrgCodeReceiving = null
from  
	CAMHS.MHS101Referral
	
inner join WarehouseOLAPMergedV2.APC.BaseChildrenAdolescentMentalHealth APCCAMHS
on MHS101Referral.ServiceRequestId = 
			(
			APCCAMHS.ContextCode 
			+ '||'
			+ cast (APCCAMHS.SourceUniqueID as varchar)-- Primary Key
			)

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseChildrenAdolescentMentalHealth Bridge
on APCCAMHS.MergeRecno = Bridge.MergeRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference APCReference
on APCReference.MergeEncounterRecno = Bridge.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.DischargeDestination 
on DischargeDestination.SourceDischargeDestinationID = APCReference.DischargeDestinationID

where
	NationalDischargeDestinationCode in ('49','51','52','53','87','88')
and DischargeDate between @FromDate and @ToDate

;
print 'MHS105OnwardReferral'


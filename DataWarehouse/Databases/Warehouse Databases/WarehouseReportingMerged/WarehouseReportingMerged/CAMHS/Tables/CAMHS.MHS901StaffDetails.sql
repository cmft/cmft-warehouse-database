﻿CREATE TABLE [CAMHS].[MHS901StaffDetails] (
    [CareProfLocalId]     NVARCHAR (20) NOT NULL,
    [ProfRegBodyCode]     NVARCHAR (2)  NULL,
    [ProfRegEntryId]      NVARCHAR (32) NULL,
    [CareProfStaffGpMH]   NVARCHAR (2)  NULL,
    [MainSpecCodeMH]      NVARCHAR (3)  NULL,
    [OccCode]             NVARCHAR (3)  NULL,
    [CareProfJobRoleCode] NVARCHAR (5)  NULL
);


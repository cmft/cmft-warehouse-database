﻿CREATE TABLE [CAMHS].[MHS002GPAdd] (
    [LocalPatientId]           NVARCHAR (20) NOT NULL,
    [GMPCodeReg]               NVARCHAR (6)  NOT NULL,
    [StartDateGMPRegistration] DATETIME      NULL,
    [EndDateGMPRegistration]   DATETIME      NULL,
    [OrgCodeGPPrac]            NVARCHAR (3)  NULL,
    [MPIContextCode]           VARCHAR (20)  NULL
);


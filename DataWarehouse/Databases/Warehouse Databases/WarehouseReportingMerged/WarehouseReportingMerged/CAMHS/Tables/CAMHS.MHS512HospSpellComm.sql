﻿CREATE TABLE [CAMHS].[MHS512HospSpellComm] (
    [HospProvSpellNum]     NVARCHAR (12) NOT NULL,
    [OrgCodeComm]          NVARCHAR (5)  NOT NULL,
    [StartDateOrgCodeComm] DATETIME      NOT NULL,
    [EndDateOrgCodeComm]   DATETIME      NULL
);


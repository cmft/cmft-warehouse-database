﻿CREATE TABLE [CAMHS].[MHS008CrisisPlan] (
    [LocalPatientId]             NVARCHAR (20) NOT NULL,
    [MHCrisisPlanCreatDate]      DATETIME      NOT NULL,
    [MHCrisisPlanLastUpdateDate] DATETIME      NULL
);


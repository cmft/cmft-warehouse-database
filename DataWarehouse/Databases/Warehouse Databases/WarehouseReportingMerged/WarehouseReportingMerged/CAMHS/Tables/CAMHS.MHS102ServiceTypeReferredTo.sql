﻿CREATE TABLE [CAMHS].[MHS102ServiceTypeReferredTo] (
    [CareProfTeamLocalId] NVARCHAR (20) NULL,
    [ServiceRequestId]    NVARCHAR (20) NOT NULL,
    [ServTeamTypeRefToMH] NVARCHAR (3)  NOT NULL,
    [CAMHSTier]           NVARCHAR (1)  NULL,
    [ReferClosureDate]    DATETIME      NULL,
    [ReferRejectionDate]  DATETIME      NULL,
    [ReferClosReason]     NVARCHAR (2)  NULL,
    [ReferRejectReason]   NVARCHAR (2)  NULL
);


﻿CREATE TABLE [CAMHS].[MHS202CareActivity] (
    [CareActId]               NVARCHAR (20) NOT NULL,
    [CareContactId]           NVARCHAR (20) NOT NULL,
    [CareProfLocalId]         NVARCHAR (20) NULL,
    [ClinContactDurOfCareAct] INT           NULL,
    [ProcSchemeInUse]         NVARCHAR (2)  NULL,
    [CodeProc]                NVARCHAR (18) NULL,
    [FindSchemeInUse]         NVARCHAR (2)  NULL,
    [CodeFind]                NVARCHAR (18) NULL,
    [ObsSchemeInUse]          NVARCHAR (2)  NULL,
    [CodeObs]                 NVARCHAR (18) NULL,
    [ObsValue]                NVARCHAR (10) NULL,
    [UnitMeasure]             NVARCHAR (10) NULL,
    CONSTRAINT [PK_MHS202CareActivity_CareActID] PRIMARY KEY CLUSTERED ([CareActId] ASC)
);


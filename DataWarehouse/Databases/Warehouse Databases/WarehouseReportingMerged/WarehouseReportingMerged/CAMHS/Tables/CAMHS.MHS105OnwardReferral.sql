﻿CREATE TABLE [CAMHS].[MHS105OnwardReferral] (
    [ServiceRequestId]  NVARCHAR (20) NOT NULL,
    [OnwardReferDate]   DATETIME      NOT NULL,
    [OnwardReferReason] NVARCHAR (2)  NULL,
    [OrgCodeReceiving]  NVARCHAR (5)  NULL
);


﻿CREATE TABLE [CAMHS].[MHS301GroupSession] (
    [GroupSessId]               NVARCHAR (20) NOT NULL,
    [GroupSessDate]             DATETIME      NOT NULL,
    [OrgCodeComm]               NVARCHAR (5)  NOT NULL,
    [ClinContDurOfGroupSess]    INT           NULL,
    [GroupSessType]             NVARCHAR (2)  NULL,
    [NumberOfGroupSessParticip] INT           NULL,
    [ActLocTypeCode]            NVARCHAR (3)  NULL,
    [SiteCodeTreat]             NVARCHAR (9)  NULL,
    [CareProfLocalId]           NVARCHAR (20) NULL,
    [ServTeamTypeRefToMH]       NVARCHAR (3)  NULL,
    [NHSServAgreeLineNum]       NVARCHAR (10) NULL
);


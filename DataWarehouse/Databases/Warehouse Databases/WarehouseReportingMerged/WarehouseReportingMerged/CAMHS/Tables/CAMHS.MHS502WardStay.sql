﻿CREATE TABLE [CAMHS].[MHS502WardStay] (
    [WardStayId]                NVARCHAR (20) NOT NULL,
    [HospProvSpellNum]          NVARCHAR (12) NOT NULL,
    [StartDateWardStay]         DATETIME      NOT NULL,
    [StartTimeWardStay]         DATETIME      NULL,
    [EndDateWardStay]           DATETIME      NULL,
    [EndTimeWardStay]           DATETIME      NULL,
    [SiteCodeOfTreat]           NVARCHAR (9)  NULL,
    [WardType]                  NVARCHAR (2)  NULL,
    [WardSexTypeCode]           NVARCHAR (1)  NULL,
    [IntendClinCareIntenCodeMH] NVARCHAR (2)  NULL,
    [WardSecLevel]              NVARCHAR (1)  NULL
);


﻿CREATE TABLE [CAMHS].[MHS509HomeLeave] (
    [WardStayId]         NVARCHAR (20) NOT NULL,
    [StartDateHomeLeave] DATETIME      NOT NULL,
    [StartTimeHomeLeave] DATETIME      NULL,
    [EndDateHomeLeave]   DATETIME      NULL,
    [EndTimeHomeLeave]   DATETIME      NULL
);


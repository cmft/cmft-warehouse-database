﻿CREATE TABLE [CAMHS].[MHS503AssignedCareProf] (
    [HospProvSpellNum]     NVARCHAR (12) NOT NULL,
    [CareProfLocalId]      NVARCHAR (20) NOT NULL,
    [StartDateAssCareProf] DATETIME      NOT NULL,
    [EndDateAssCareProf]   DATETIME      NULL,
    [TreatFuncCodeMH]      NVARCHAR (3)  NULL
);


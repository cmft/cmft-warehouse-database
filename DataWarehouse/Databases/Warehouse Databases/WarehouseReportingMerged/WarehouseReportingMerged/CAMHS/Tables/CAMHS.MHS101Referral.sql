﻿CREATE TABLE [CAMHS].[MHS101Referral] (
    [ServiceRequestId]                    NVARCHAR (20) NOT NULL,
    [LocalPatientId]                      NVARCHAR (20) NOT NULL,
    [OrgCodeComm]                         NVARCHAR (5)  NOT NULL,
    [ReferralRequestReceivedDate]         DATETIME      NOT NULL,
    [ReferralRequestReceivedTime]         DATETIME      NULL,
    [NHSServAgreeLineNum]                 NVARCHAR (10) NULL,
    [SourceOfReferralMH]                  NVARCHAR (2)  NULL,
    [RefOrgCode]                          NVARCHAR (6)  NULL,
    [ReferringCareProfessionalStaffGroup] NVARCHAR (3)  NULL,
    [ClinRespPriorityType]                NVARCHAR (1)  NULL,
    [PrimReasonReferralMH]                NVARCHAR (2)  NULL,
    [ServDischDate]                       DATETIME      NULL,
    [DischLetterIssDate]                  DATETIME      NULL,
    CONSTRAINT [PK_MHS101Referral_ServiceRequestID] PRIMARY KEY CLUSTERED ([ServiceRequestId] ASC)
);


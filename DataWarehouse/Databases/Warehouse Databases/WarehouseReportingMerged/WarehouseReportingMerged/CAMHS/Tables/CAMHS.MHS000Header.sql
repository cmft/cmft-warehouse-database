﻿CREATE TABLE [CAMHS].[MHS000Header] (
    [DatSetVer]                REAL          NOT NULL,
    [OrgCodeProvider]          NVARCHAR (6)  NOT NULL,
    [OrgCodeSubmit]            NVARCHAR (6)  NOT NULL,
    [PrimSystemInUse]          NVARCHAR (20) NOT NULL,
    [ReportingPeriodStartDate] DATETIME      NOT NULL,
    [ReportingPeriodEndDate]   DATETIME      NOT NULL,
    [DateTimeDatSetCreate]     DATETIME      NOT NULL
);


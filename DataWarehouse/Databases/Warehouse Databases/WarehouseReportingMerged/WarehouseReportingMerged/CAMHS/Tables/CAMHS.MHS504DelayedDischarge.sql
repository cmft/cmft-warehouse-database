﻿CREATE TABLE [CAMHS].[MHS504DelayedDischarge] (
    [HospProvSpellNum]    NVARCHAR (12) NOT NULL,
    [StartDateDelayDisch] DATETIME      NOT NULL,
    [EndDateDelayDisch]   DATETIME      NULL,
    [DelayDischReason]    NVARCHAR (2)  NULL,
    [AttribToIndic]       NVARCHAR (2)  NULL
);


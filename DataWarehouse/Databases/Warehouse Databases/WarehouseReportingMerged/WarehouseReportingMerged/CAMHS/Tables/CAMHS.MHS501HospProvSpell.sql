﻿CREATE TABLE [CAMHS].[MHS501HospProvSpell] (
    [HospProvSpellNum]              NVARCHAR (12) NOT NULL,
    [ServiceRequestId]              NVARCHAR (20) NOT NULL,
    [StartDateHospProvSpell]        DATETIME      NOT NULL,
    [StartTimeHospProvSpell]        DATETIME      NULL,
    [SourceAdmCodeHospProvSpell]    NVARCHAR (2)  NULL,
    [AdmMethCodeHospProvSpell]      NVARCHAR (2)  NULL,
    [PlannedDischDateHospProvSpell] DATETIME      NULL,
    [DischDateHospProvSpell]        DATETIME      NULL,
    [DischTimeHospProvSpell]        DATETIME      NULL,
    [DischMethCodeHospProvSpell]    NVARCHAR (1)  NULL,
    [DischDestCodeHospProvSpell]    NVARCHAR (2)  NULL
);


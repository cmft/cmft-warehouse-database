﻿CREATE TABLE [CAMHS].[MHS005PatInd] (
    [LocalPatientId]        NVARCHAR (20) NOT NULL,
    [ConstSuperReqDueToDis] NVARCHAR (1)  NULL,
    [YoungCarer]            NVARCHAR (1)  NULL,
    [LACStatus]             NVARCHAR (1)  NULL,
    [CPP]                   NVARCHAR (1)  NULL,
    [ProPsychDate]          DATETIME      NULL,
    [EmerPsychDate]         DATETIME      NULL,
    [ManPsychDate]          DATETIME      NULL,
    [PsychPrescDate]        DATETIME      NULL,
    [PsychTreatDate]        DATETIME      NULL
);


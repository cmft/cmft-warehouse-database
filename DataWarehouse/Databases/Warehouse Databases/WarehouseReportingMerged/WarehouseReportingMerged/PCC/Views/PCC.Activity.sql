﻿
Create View PCC.Activity
as

Select 
	ActivityRecno	
	,PeriodID	
	,ActivityDate	
	,ActivityCode	
	,ActivitySequence	
	,Created	
	,Updated	
	,ByWhom
from 
	WarehouseOLAPMergedV2.PCC.Activity


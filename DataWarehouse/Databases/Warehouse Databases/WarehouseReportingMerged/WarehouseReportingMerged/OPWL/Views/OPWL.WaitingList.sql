﻿create view OPWL.WaitingList as

SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceWaitingListID]
      ,[SourceWaitingListCode]
      ,[SourceWaitingList]
      --,[LocalWaitingListID]
      ,[LocalWaitingListCode]
      ,[LocalWaitingList]
      --,[NationalWaitingListID]
      ,[NationalWaitingListCode]
      ,[NationalWaitingList]
  FROM [WarehouseOLAPMergedV2].[OPWL].[WaitingList]

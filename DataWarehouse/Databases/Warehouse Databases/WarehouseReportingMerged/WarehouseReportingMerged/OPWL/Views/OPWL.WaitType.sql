﻿

create view OPWL.WaitType as

SELECT [WaitTypeID]
      ,[WaitType]
      ,[ParentWaitTypeID]
  FROM [WarehouseOLAPMergedV2].[OPWL].[WaitType]

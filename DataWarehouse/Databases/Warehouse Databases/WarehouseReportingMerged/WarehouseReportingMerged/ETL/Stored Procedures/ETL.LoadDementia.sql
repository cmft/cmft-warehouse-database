﻿



CREATE PROCEDURE [ETL].[LoadDementia]
 
 
AS 

 /******************************************************************************
**  Name:		ETL.LoadDementia
**  Purpose:	Populates Dementia Assessment Model Base table : ETL.TimportDementiaTable
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:		Author:		Description:
** ----------	-------		---------------------------------------------------- 
** 2012-12-28   CB			Refactored

******************************************************************************/




TRUNCATE TABLE ETL.TimportDementiaTable
 
INSERT INTO ETL.TimportDementiaTable
(
	 DementiaID
	,ProviderSpellNo
	,SourceWardCode
	,LastUpdated_TS
	,KnownToHaveDementia
	,PatientDementiaFlag
	,RememberMePlan
	,RememberMePlanCommenced
	,RememberMePlanDiscontinued
	,AMTTestPerformed
	,ClinicalIndicationsPresent
	,ForgetfulnessQuestionAnswered
	,AMTScore
	,SourcePatientNo
	,ReferralNeeded
	,DementiaReferralSentDate
	,AMTTestNotPerformedReason
	,CognitiveInvestigationStarted
)

SELECT
	 DementiaID
	,ProviderSpellNo
	,PASCode
	,LastUpdated_TS
	,KnownToHaveDementia
	,PatientDementiaFlag
	,RememberMePlan

	,RememberMePlanCommenced =
		convert(
			 datetime
			,convert(
				 date
				,case
				when (
						ltrim(RememberMePlanCommenced) = ''
					or	RememberMePlanCommenced = '1900-01-01'
					) then null
				else RememberMePlanCommenced 
				end
			)
		)

	,RememberMePlanDiscontinued =
		convert(
			 datetime
			,convert(
				 date
				,case
				when (
						ltrim(RememberMePlanDiscontinued) = ''
					or	RememberMePlanDiscontinued = '1900-01-01'
					) then null
				else RememberMePlanDiscontinued
				end
			)
		)
      
	,AMTTestPerformed
	,ClinicalIndicationsPresent
	,ForgetfulnessQuestionAnswered
	,AMTScore
	,SourcePatientNo  
	,ReferralNeeded
	,DementiaReferralSentDate
	,AMTTestNotPerformedReason
	,CognitiveInvestigationStarted
FROM

	RPT.DementiaLiveUpdate



-- Update any previous dementia assessments based on previous assessments

--this code will arbitrarily update the current record

--Removed 03 March 2014 as unable to process

			--UPDATE ETL.TimportDementiaTable

			--SET
			--	PreviousDementiaAssessment = DementiaTable2.PatientDementiaFlag
			--FROM
			--	ETL.TimportDementiaTable 

			--INNER JOIN ETL.TimportDementiaTable DementiaTable2
			--ON	DementiaTable2.SourcePatientNo = ETL.TimportDementiaTable.SourcePatientNo
			--AND DementiaTable2.LastUpdated_TS < ETL.TimportDementiaTable.LastUpdated_TS

-- Update any previous dementia assessments based on patient number
--UPDATE ETL.TimportDementiaTable
--SET PreviousDementiaAssessment = replace(replace(Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)'),'N',0),'Y',1),
--PatientDementiaFlag = replace(replace(Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)'),'N',0),'Y',1),
--KnownToHaveDementia = replace(replace(Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)'),'N',0),'Y',1)
--FROM ETL.TimportDementiaTable
--INNER JOIN BedMan.dbo.Patients
--ON DementiaTable.SourcePatientNo = Patients.Patient
--AND replace(replace(Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)'),'N',0),'Y',1) = 1


-- Reset the known flag where they have had an assessment (as you can't tell in Bedman when the Patient Dementia Flower flag was set)

--Removed 03 March 2014 as unable to process

		--UPDATE ETL.TimportDementiaTable

		--SET
		--	KnownToHaveDementia = DementiaTable2.KnownToHaveDementia
		--FROM
		--	ETL.TimportDementiaTable

		--INNER JOIN RPT.DementiaLiveUpdate DementiaTable2
		--ON	DementiaTable2.ProviderSpellNo = ETL.TimportDementiaTable.ProviderSpellNo
		--AND DementiaTable2.AMTTestPerformed = ETL.TimportDementiaTable.AMTTestPerformed
		--AND DementiaTable2.KnownToHaveDementia IS NOT NULL

--Slow Find another way
--Gareth C 18/03/2013 10 min
-- Update Medisec data for referrals and anti psych drugs

--Removed 03 March 2014 as unable to process

			--UPDATE ETL.TimportDementiaTable
			--SET


			--	 PsychoticMedsReview =
			--		CASE [Content].value('data((template/Dementia/DemenciaPsyc)[1])', 'varchar(max)')
			--		WHEN 'Not on anti-psychotic meds' THEN 0
			--		WHEN 'NO' THEN 0
			--		WHEN 'YES' THEN 1
			--		ELSE Null
			--		END

			--	,PsychoticReasonPresent	=
			--		CASE [Content].value('data((template/Dementia/DemenciaAnti)[1])', 'varchar(max)')
			--		WHEN '' THEN 0
			--		WHEN Null THEN 0
			--		ELSE 1
			--		END	

			--FROM
			--	ETL.TimportDementiaTable

			--INNER JOIN APC.Encounter
			--ON	Encounter.ProviderSpellNo = ETL.TimportDementiaTable.ProviderSpellNo

			--INNER JOIN Warehouse.Medisec.DocumentContent
			--ON	DocumentContent.PatientNumber = ETL.TimportDementiaTable.sourcepatientno
			--AND DocumentContent.AdmissionDate = CAST(Encounter.AdmissionTime as DATE)						

			--where
			--	DocumentContent.[Content].value('data((template/Dementia/DemenciaAMTS)[1])', 'varchar(max)') <> ''
			--and DocumentContent.TemplateCode in
			--	(
			--	 'Cardiothoracic'
			--	,'DNF'
			--	,'DNF-Comorbidities'
			--	,'Gynae'
			--	,'Surgery'
			--	,'Urology'
			--	,'Vascular'
			--	,'DNF-CriticalCare'
			--	)


-- Insert all Patient Dementia Flower Records from Bedman (those WITHOUT an assessment)
--INSERT INTO ETL.TimportDementiaTable
--(DementiaID, ProviderSpellNo, SourceUniqueID, PatientDementiaFlag, PreviousDementiaAssessment, KnownToHaveDementia)
--SELECT encounter.encounterrecno, encounter.ProviderSpellNo, wardstay.SourceUniqueID,
--REPLACE(REPLACE(Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)'),'N',0),'Y',1), 
--REPLACE(REPLACE(Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)'),'N',0),'Y',1),
--REPLACE(REPLACE(Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)'),'N',0),'Y',1)
--FROM bedman.dbo.HospSpell
--INNER JOIN BedMan.dbo.Patients
--	ON HospSpell.Patient = patients.Patient
--INNER JOIN warehousereportingmerged.apc.Encounter
--	ON patients.Patient = Encounter.SourcePatientNo   
--	AND IPEpisode = SourceSpellNo
--	AND FirstEpisodeInSpellIndicator = 1	
--INNER JOIN apc.WardStay
--	ON WardStay.ProviderSpellNo = Encounter.ProviderSpellNo   
--	AND Encounter.StartWardID = WardStay.WardID    
--WHERE Patients.PatientDetails.value('(PatientDetails/DementiaAssessment/PatientDementiaFlag)[1]', 'VARCHAR(60)') = 'Y'
--AND patients.Patient not in (select SourcePatientNo from ETL.TimportDementiaTable)
--AND AdmissionTime > '01 Jan 2012'



-- Insert all patients from Encounter where they have a previous dementia DIAGNOSIS (those WITHOUT an assessment)

--Removed 03 March 2014 as unable to process

			--INSERT INTO ETL.TimportDementiaTable
			--(
			--	 DementiaID
			--	,ProviderSpellNo
			--	,SourceUniqueID
			--	,PatientDementiaFlag
			--	,PreviousDementiaAssessment
			--	,KnownToHaveDementia
			--)

			--SELECT
			--	 Encounter.EncounterRecno
			--	,Encounter.ProviderSpellNo
			--	,wardstay.SourceUniqueID
			--	,1
			--	,1
			--	,1
			--FROM
			--	WarehouseReportingMerged.APC.Encounter

			--INNER JOIN APC.WardStay
			--ON	WardStay.ProviderSpellNo = Encounter.ProviderSpellNo   
			--AND Encounter.StartWardID = WardStay.WardID    

			--WHERE
			--	Encounter.SourcePatientNo not in
			--		--SourcePatientNo is not unique across Trafford and Central contexts, therefore this could generate incorrect matches
			--		(
			--		select 
			--			SourcePatientNo 
			--		from 
			--			ETL.TimportDementiaTable 
			--		where 
			--			SourcePatientNo is not null
			--		)
			--AND AdmissionTime > '01 Jan 2012'
			--AND FirstEpisodeInSpellIndicator = 1	
			--AND EXISTS
			--	(
			--	SELECT
			--		1 
			--	FROM
			--		APC.Encounter APCEncounter
			--	WHERE
			--		(
			--			(
			--				APCEncounter.PrimaryDiagnosisCode LIKE ('F0[0,1,2,3]%')
			--			OR	APCEncounter.PrimaryDiagnosisCode LIKE ('F1[0,1,2,3,4,5,6,7,8,9].7')
			--			)
			--		OR
			--			(
			--				APCEncounter.SubsidiaryDiagnosisCode LIKE ('F0[0,1,2,3]%')
			--			OR	APCEncounter.SubsidiaryDiagnosisCode LIKE ('F1[0,1,2,3,4,5,6,7,8,9].7')
			--			)
			--		)
			--	AND APCEncounter.SourcePatientNo = Encounter.SourcePatientNo
			--	AND APCEncounter.AdmissionTime < Encounter.AdmissionTime
			--	)


	--Update current Ward Trafford current admitted
	--Gareth Cunnah 18/02/2012
	--2 min

	select 
	 EndWard.Code,  DementiaTable.ProviderSpellNo

	 into #UpDateWard

			FROM
				ETL.TimportDementiaTable DementiaTable

				inner join APC.Encounter Encounter
				on Encounter.ProviderSpellNo = DementiaTable.ProviderSpellNo

				inner join [XDATAWH].[Replica_UltraGendaBedman].dbo.admission Spell
				on Encounter.ProviderSpellNo = Spell.HISId_Number
				and IsCurrent = 1

			-- link discharge episode
			left join 
				(
				select
					*
				from
					[XDATAWH].Replica_UltraGendaBedman.dbo.BedOccupancy LastBedOccupancy
				where
					not exists
					(
					select
						1
					from
						[XDATAWH].Replica_UltraGendaBedman.dbo.BedOccupancy BedOccupancy
					where
						BedOccupancy.AdmissionId = LastBedOccupancy.AdmissionId
					and	BedOccupancy.EndDate > LastBedOccupancy.EndDate
					)
				) DischargeEpisode
			on	DischargeEpisode.AdmissionId = Spell.Id


			left join [XDATAWH].Replica_UltraGendaBedman.dbo.Bed EndWardBed
			on	EndWardBed.Id = DischargeEpisode.BedId

			left join [XDATAWH].Replica_UltraGendaBedman.dbo.Room EndWardRoom
			on	EndWardRoom.Id = EndWardBed.RoomId

			left join [XDATAWH].Replica_UltraGendaBedman.dbo.Ward EndWard
			on	EndWard.Id = EndWardRoom.WardId
	
	
				where
				 Encounter.DischargeTime is null


				and Encounter.FirstEpisodeInSpellIndicator = 1

				and Encounter.ContextCode like '%T%'


	
					update ETL.TimportDementiaTable
					set SourceWardCode = Code

					from #UpDateWard
					where #UpDateWard.ProviderSpellNo = ETL.TimportDementiaTable.ProviderSpellNo

					drop table #UpDateWard
	


	UPDATE ETL.TimportDementiaTable

	set Rundate = getdate ()
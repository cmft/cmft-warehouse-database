﻿CREATE ROLE [CMFT Analyst]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\sec - sp information';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\razia.nazir';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Diane.Thomas';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'THT\WCollier';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Tim.Nelson';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\gareth.jones';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Rachel.Reed';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Samaira.Akram';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Stephen.James';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Janet.Peat';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\paul.westhead';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\James.Watson';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'tgh.information';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Ian.Connolly';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'cmftslamlink';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'cmmc\tom.smith';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'adamblack';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Greg.Tomkins';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\David.Porter';


GO
EXECUTE sp_addrolemember @rolename = N'CMFT Analyst', @membername = N'CMMC\Waiken.Chan';


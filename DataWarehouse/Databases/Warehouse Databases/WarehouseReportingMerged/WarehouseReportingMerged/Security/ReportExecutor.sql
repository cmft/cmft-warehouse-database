﻿CREATE ROLE [ReportExecutor]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Sec - Warehouse Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\sec - sp information';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\razia.nazir';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Diane.Thomas';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'THT\WCollier';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Tim.Nelson';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Rachel.Reed';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Samaira.Akram';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Stephen.James';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Janet.Peat';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\paul.westhead';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'tgh.information';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'ReportExecutor', @membername = N'cmmc\tom.smith';


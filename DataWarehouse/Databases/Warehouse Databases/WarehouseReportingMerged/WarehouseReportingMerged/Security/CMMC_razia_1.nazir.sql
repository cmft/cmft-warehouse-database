﻿CREATE LOGIN [CMMC\razia.nazir]
    FROM WINDOWS WITH DEFAULT_DATABASE = [WarehouseReportingMerged], DEFAULT_LANGUAGE = [us_english];


GO
ALTER LOGIN [CMMC\razia.nazir] DISABLE;


﻿CREATE ROLE [ReportSubscriptionOwner]
    AUTHORIZATION [dbo];


GO
EXECUTE sp_addrolemember @rolename = N'ReportSubscriptionOwner', @membername = N'CMMC\Thomas.Drury';


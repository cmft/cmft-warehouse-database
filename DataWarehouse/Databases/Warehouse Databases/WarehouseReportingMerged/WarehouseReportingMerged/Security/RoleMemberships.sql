﻿EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Helen.Shackleton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\razia.nazir';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'scc';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Colin.Hunter';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\sec - sp information';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\peter.graham';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Helen.Shackleton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\razia.nazir';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Diane.Thomas';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Wendy.Collier';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\gordon.fenton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Mohamed.Athman';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'THT\WCollier';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Tim.Nelson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\gareth.jones';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Rachel.Reed';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Samaira.Akram';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Stephen.James';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Janet.Peat';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\paul.westhead';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Ian.Daniels';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - SP Commissioning';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\cmftslam';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'tgh.information';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Anthony.Smith2';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Ian.Connolly';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Ben.Kenyon2';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Malcom.hodson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmftslamlink';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sandeep.Solipuram';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Central Infopath Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmmc\tom.smith';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'scc';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\paul.miles';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Greg.Tomkins';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Richard.Perrin';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\David.Porter';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Steve.Mosby';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Waiken.Chan';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\razia.nazir';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\Sec - Warehouse BI Developers';


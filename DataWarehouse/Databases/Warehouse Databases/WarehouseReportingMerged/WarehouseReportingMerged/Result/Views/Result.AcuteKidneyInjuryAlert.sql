﻿


CREATE view [Result].[AcuteKidneyInjuryAlert] as


SELECT 
	 MergeResultRecno
	,SourcePatientNo
	,ContextCode
	,DistrictNo
	,CasenoteNumber
	,PatientSurname
	,PatientForename
	,PatientSequence
	,SpellSequence
	,LinkageType 
	,LinkingMergeEncounterRecno 
	,ResultAPCLinked
	,GlobalProviderSpellNo
	,AdmissionTime
	,PatientCategoryCode
	,WardEpisodeStart
	,WardEpisodeEnd
	,DischargeTime
	,DischargeMethodCode
	,DateOfDeath
	,LocalLocationCode
	,LocalLocation
	,SexCode
	,DateOfBirth
	,AgeOnAdmission
	,AgeAtTest
	,ResultTime
	,EffectiveTime
	,Result
	,PreviousResultTime 
	,PreviousResultEffectiveTime 
	,PreviousResult 
	,ResultIntervalDays 
	,ResultChange 
	,CurrentToPreviousResultRatio 
	,ResultRateOfChangePer24Hr 
	,BaselineResultRecno 
	,BaselineResult 
	,BaselineEffectiveTime 
	,BaselineResultTime 
	,BaselineToCurrentDays
	,BaselineToCurrentChange
	,CurrentToBaselineResultRatio			
	,Stage
	,C1Result
	,RatioDescription
	,ResultForRatio
	,RVRatio
	,NationalAlert
	,InHospitalAKI 
	,CurrentInpatient
	,CurrentWard 
	,RenalPatient
	,AKIFirstAlertSpellSequence
	,AKIFirstAlertInSpellTime 
	,AKIFirstAlertInSpellRatio
	,AKIFirstAlertInSpellStage
	,AKILoS
FROM
	WarehouseOLAPMergedV2.Result.AcuteKidneyInjuryAlert

			
	















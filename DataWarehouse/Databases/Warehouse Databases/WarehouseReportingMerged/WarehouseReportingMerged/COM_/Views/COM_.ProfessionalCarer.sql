﻿
create view [COM].[ProfessionalCarer] as

select
	 SourceValueID
	,ProfessionalCarerID
	,ProfessionalCarerCode
	,ProfessionalCarer
	,ProfessionalCarerTypeID
	,Title
	,Forename
	,Surname
	,StartTime
	,EndTime
	,PrimaryStaffTeamID
	,ArchiveFlag
	,HealthOrgOwner
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.ProfessionalCarer
;






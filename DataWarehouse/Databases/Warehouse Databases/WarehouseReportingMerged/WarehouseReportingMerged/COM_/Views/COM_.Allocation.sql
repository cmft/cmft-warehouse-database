﻿create view [COM].[Allocation]

as

/****************************************************************************************
	View		: [COM].[Allocation]
	Description : Community allocations

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	11/07/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select 
	COMAllocation.AllocationTypeID
	,COMAllocation.DatasetRecno
	,COMAllocation.DatasetCode
	,COMAllocation.AllocationID
from
	WarehouseOLAPMergedV2.Allocation.DatasetAllocation COMAllocation
where
	COMAllocation.AllocationTypeID = 2
	and	COMAllocation.DatasetCode = 'COM'					
;

﻿



CREATE view [COM].[CIDSActivity]

as

/****************************************************************************************
	View		: [COM].[CIDSActivity]
	Description : To supply the commissioner with a patient level dataset (Interim MDS)

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	11/07/2014	Paul Egan       Intial Coding
	03/10/2014	Paul Egan		Moved extra fields to aid validation into separate view 
								(COM.CIDSActivityValidationExtraFields)
	20151106	Paul Egan		GP_Practice_Code to first 7 characters (the max submission length)
*****************************************************************************************/

/*	Need to bring field SCHEDULES.CONTP_REFNO through ETL process for Activity_Type.
	[Warehouse].[dbo].[ExtractCOMEncounter] etc...
	--inner join COM.ActivityTypeBase ActivityType
	--on	ActivityType.ActivityTypeID = 
*/

select
	--Encounter.MergeEncounterRecno		-- Validation only 
	Activity_Type_Contact = right('0' + cast(Encounter.DerivedFirstAttendanceFlag as varchar(2)), 2)	-- Added leading zero as indicated in data dictionary
	--,Activity_Type_Contact_Desc =		-- Validation only 
		--case Encounter.DerivedFirstAttendanceFlag
		--	when '1' then 'Initial Contact'
		--	when '2' then 'Follow up CARE CONTACT'
		--	else cast(Encounter.DerivedFirstAttendanceFlag as varchar(50))
		--end
	,Attendance_Status = Encounter.EncounterOutcomeCode		-- To be reference-mapped with Darren if they want national Attendance_Status codes
	,Activity_Date = Encounter.StartDate
	,Activity_Duration = Encounter.EncounterDuration
	,Activity_Group = '12'
	--,Activity_Group_Desc = 'Community'		-- Validation only 
	,Activity_Type = NULL	-- Need to bring field SCHEDULES.CONTP_REFNO through ETL process
	
	/*	01 Cancelled for Clinical Reasons 
		02 Cancelled for Non-clinical Reasons
		NEED CASE STATEMENT ??? */
	,Cancellation_Reason = CancellationReason.NHSCancellationReasonCode
	--,Cancellation_Reason_Desc = CancellationReason.CancellationReason		-- Validation only 
		
	/* NEED CASE STATEMENT ??? - e.g. 'HOME' SHOULD BE 'A01' ??? */
	,Care_Location_Type = LocationType.LocationTypeCode
	--,Care_Location_Type_Desc = LocationType.LocationType			-- Validation only 
	--,Contact_Method_Source_ID = Encounter.ContactTypeID			-- Validation only 
	--,Contact_Method_Source_Desc = CaseType.CaseType				-- Validation only 
	/* NEED CASE STATEMENT ??? - I don't think these codes are meaningful ??? */
	,Contact_Method = 
		case
			when CaseType.CaseType like '%face%' then '01'
			when CaseType.CaseType like '%phone%' then '02'
			when CaseType.CaseType like '%telemedicine web camera%' then '03'
			when CaseType.CaseType like '%talk type%' then '04'
			when CaseType.CaseType like '%email%' then '05'
			when CaseType.CaseType like '%sms%' then '06'
			when CaseType.CaseType like '%group%' then '01'
			when Encounter.ContactTypeID = 1452 then '01'	 -- Group session (LookupID 'SATYP')
			when CaseType.CaseType like '%Other Client linked activity%' then '02'
			else '98' --Other
		end
	--,Contact_Method_Desc = 		-- Validation only 
	--	case
	--		when CaseType.CaseType like '%face%' then 'Face to face'
	--		when CaseType.CaseType like '%phone%' then 'Telephone'
	--		when CaseType.CaseType like '%Telemedicine web camera%' then 'Telemedicine'
	--		when CaseType.CaseType like '%Talk type%' then 'Talk type'
	--		when CaseType.CaseType like '%Email%' then 'Email'
	--		when CaseType.CaseType like '%sms%' then 'SMS'
	--		when CaseType.CaseType like '%group%' then 'Face to face'
	--		when Encounter.ContactTypeID = '1452' then 'Face to face'	-- Group session (LookupID 'SATYP')
	--		when CaseType.CaseType like '%Other Client linked activity%' then 'Telephone'
	--		else 'Other' --Other
	--	end		
	,Contact_With =
		case
			when CaseType.CaseType like '%proxy%' then '02'
			when CaseType.CaseType like '%client%' then '01'
			else '01' -- Default to client
		end
	--,Contact_With_Desc =		-- Validation only 
		--case
		--	when CaseType.CaseType like '%proxy%' then 'Patient Proxy'
		--	when CaseType.CaseType like '%client%' then 'PATIENT'
		--	else 'PATIENT' -- Default to client
		--end
	,DOB = Encounter.PatientDateOfBirth
	,Date_of_Death = Encounter.PatientDateOfDeath
	,Ethnic_Category = EthnicCategory.EthnicCategoryCode
	--,Ethnic_Category_Desc = EthnicCategory.EthnicCategory		-- Validation only 
	,Gender = Sex.NHSSexCode
	--,Gender_Desc = Sex.Sex		-- Validation only 
	,GP_Practice_Code = left(Encounter.PatientCurrentRegisteredPracticeCode, 7)		-- This is the max submission length
	,Patient_ID = Encounter.PatientSourceID
	,NHS_Number = Encounter.PatientNHSNumber
	,Commissioner_Org_Code = Encounter.CommissionerCode
	--,Commissioner_Org_Code_Desc = CCG.CCG		-- Validation only 
	,Provider_Org_Code = 'RW3'
	,Pathway_ID = Referral.RTTPatientPathwayID
	,Postcode = Encounter.PatientPostcode
	,Referral_Closure_Date = Encounter.ReferralClosedDate
	,Referral_Priority = ReferralPriority.NHSReferralPriorityCode
	--,Referral_Priority_Desc = ReferralPriority.ReferralPriority		-- Validation only 
	,Referral_Request_Received_Date = Encounter.ReferralReceivedDate
	,Referral_Source = ReferralSource.ReferralSource
	,RTT_End_Date = NULL -- '#ToDo'
	
	/*	Use Referral or Encounter version? 5,934 rows are different for Apr-May 2014
		There are more matches joining to Encounter than Referral. */
	,RTT_Period_Status = RTTPeriodStatus.RTTPeriodStatusCode
	--,RTT_Period_Status_Desc = RTTPeriodStatus.RTTPeriodStatus		-- Validation only 
	,RTT_Start_Date = Referral.RTTStartTime
	,Staff_Group = Encounter.StaffTeamID
	,REFRL_REFNO = encounter.ReferralID
	,[Service] = --coalesce([Service].ServiceGroup, Specialty.Specialty)
		Case
			when [Service].ServiceGroup = 'Not Specified' then 'NS-' + Specialty.Specialty
			else [Service].ServiceGroup
		end
	--,SpecialtyCode = specialty.SpecialtyCode
	--,Specialty.Specialty

from
	COM.Encounter Encounter
	
	inner join COM.EncounterReference
	on	Encounter.MergeEncounterRecno = EncounterReference.MergeEncounterRecno
	
	left join COM.Referral Referral
	on	Referral.SourceUniqueID = Encounter.ReferralID
	
	left join COM.Specialty Specialty
	on	Specialty.SpecialtyID = Encounter.SpecialtyID
	
	left join COM.CancellationReasonBase CancellationReason
	on	CancellationReason.CancellationReasonID = Encounter.CanceledReasonID
	
	left join COM.LocationTypeBase LocationType
	on	LocationType.LocationTypeID = Encounter.LocationTypeID
	
	left join COM.CaseTypeBase CaseType		-- This is contact method / consultation medium
	on	CaseType.CaseTypeID = Encounter.ContactTypeID
		
	left join COM.EthnicCategoryBase EthnicCategory
	on	EthnicCategory.EthnicCategoryID = Encounter.PatientEthnicGroupID
	
	left join COM.SexBase Sex
	on	Sex.SexID = Encounter.PatientSexID
	
	left join COM.RTTPeriodStatusBase RTTPeriodStatus
	on	RTTPeriodStatus.RTTPeriodStatusID = Encounter.RTTStatusID	-- There are more matches joining to Encounter than Referral.RTTStatusID
	
	left join COM.ReferralPriorityBase ReferralPriority
	on	ReferralPriority.ReferralPriorityID = Referral.[Priority]
	
	left join COM.ReferralSource
	on ReferralSource.ReferralSourceID = Encounter.ReferralSourceID	-- There are more matches joining to Encounter than Referral.SourceofReferralID
	
	left join WH.CCG
	on	CCG.CCGCode = Encounter.CommissionerCode
	
	left join COM.Allocation ServiceAllocation		-- Credit Darren Griffiths				
	on	ServiceAllocation.DatasetRecno = EncounterReference.MergeEncounterRecno
	
	left join WH.[Service]							-- Credit Darren Griffiths
	on	[Service].ServiceID = ServiceAllocation.AllocationID
	
where
	encounter.JointActivityFlag <> 1									-- Excluded for now, as there are missing BaseEncounterReference rows (hence missing in cube). They all seem to be joint activity (at least for 2014Q1)
	and CaseType.CaseType not like '%Other Client linked activity%'		-- As instructed by Gareth Summerfield
	and Encounter.Archived = 'n'										-- Better to be safe and always use this





﻿
create view [COM].[LocationType] as

/* 
==============================================================================================
Description:	Created for CYPHS submission.
TFS:			6537

When		Who			What
20151204	Paul Egan	Initial Coding
===============================================================================================
*/

select 
	 SourceValueID
	,LocationTypeID
	,LocationTypeCode
	,LocationType
	,NationalLocationTypeCode
	,NationalLocationType
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.LocationType
;
﻿create view [COM].[CaseTypeBase]

as

/****************************************************************************************
	View		: [COM].[CaseTypeBase]
	Description : Community case type (contact method / consultation medium) from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	10/07/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	CaseTypeID
	,CaseType
	,CaseTypeGroupChannel
	,CaseTypeGroup
	,CaseTypeCode
	,NHSCaseTypeCode
	,NationalCaseTypeCode
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.CaseTypeBase
;

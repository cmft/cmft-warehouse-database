﻿create view [COM].[SexBase]

as

/****************************************************************************************
	View		: COM.SexBase
	Description : Community activity Sex from lookup table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	23/06/2014	Paul Egan       Intial Coding
*****************************************************************************************/

select
	SexID
	,Sex
	,SexCode
	,NHSSexCode
	,NationalSexCode
	,ContextCode
from
	WarehouseOLAPMergedV2.COM.SexBase
;

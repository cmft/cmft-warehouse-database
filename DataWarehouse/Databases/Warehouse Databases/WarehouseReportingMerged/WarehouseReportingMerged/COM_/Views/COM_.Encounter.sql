﻿


CREATE view [COM].[Encounter] as 
 
select
	 BaseEncounter.*
	,BaseEncounterReference.LocationID
from 
	WarehouseOLAPMergedV2.COM.BaseEncounter
	
	left join WarehouseOLAPMergedV2.COM.BaseEncounterReference
	on BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	
;



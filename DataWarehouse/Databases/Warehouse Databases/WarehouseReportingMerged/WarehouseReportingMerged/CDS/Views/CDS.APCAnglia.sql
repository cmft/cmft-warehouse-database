﻿
CREATE view [CDS].[APCAnglia] as

select
	 UniqueEpisodeSerialNo = LEFT(UniqueEpisodeSerialNo, 35)
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,RecordType 
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,PlaceHolder2 = CAST('' as char(1))
	,PatientsAddress
	,Postcode
	,HaCodeOfResidence = Left(PCTofResidenceCode,3)
	,SexCode
	,PlaceHolder4 = CAST('' as char(1))
	,CarerSupportIndicator
	,DateOfBirth
	,PlaceHolder5 = CAST('' as char(1))
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,EthnicGroupCode
	,ReferrerCode
	,ReferringOrganisationCode
	,ProviderSpellNo
	,PatientCategoryCode
	,DecidedToAdmitDate
	,PlaceHolder6 = CAST('' as char(1))
	,AdmissionDate
	,AdmissionMethodCode
	,DurationOfElectiveWait
	,ManagementIntentionCode
	,SourceOfAdmissionCode
	,DischargeDate
	,PlaceHolder7 = CAST('' as char(1))
	,DischargeMethodCode
	,DischargeDestinationCode
	,PatientClassificationCode
	,EpisodeNo
	,LastEpisodeInSpellIndicator
	,StartSiteCode
	,StartWardTypeCode
	,EpisodeStartDate
	,EpisodeEndDate
	,PlaceHolder8 = CAST('' as char(1))
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,PlaceHolder9 = CAST('' as char(3))
	,ConsultantCode
	,FirstRegularDayNightAdmission
	,NeonatalLevelOfCare
	,PsychiatricPatientStatusCode
	,PrimaryDiagnosisCode
	,PlaceHolder9a = CAST('' as char(6))
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12

	,PlaceHolder10 = CAST('' as char(7))
	,PlaceHolder11 = CAST('' as char(7))
	,PlaceHolder12 = CAST('' as char(7))
	,PlaceHolder13 = CAST('' as char(7))
	,PlaceHolder14 = CAST('' as char(7))
	,PlaceHolder15 = CAST('' as char(7))
	,PlaceHolder16 = CAST('' as char(7))
	,PlaceHolder17 = CAST('' as char(7))
	,PlaceHolder18 = CAST('' as char(7))
	,PlaceHolder19 = CAST('' as char(7))
	,PlaceHolder20 = CAST('' as char(7))
	,PlaceHolder21 = CAST('' as char(7))
	,PlaceHolder22 = CAST('' as char(7))
	,PlaceHolder23 = CAST('' as char(7))

	,OperationStatus
	,PrimaryOperationCode
	,PrimaryOperationDate
	,PlaceHolder24 = CAST('' as char(1))
	,OperationCode2
	,OperationDate2
	,PlaceHolder25 = CAST('' as char(1))
	,OperationCode3
	,OperationDate3
	,PlaceHolder26 = CAST('' as char(1))
	,OperationCode4
	,OperationDate4
	,PlaceHolder27 = CAST('' as char(1))
	,OperationCode5
	,OperationDate5
	,PlaceHolder28 = CAST('' as char(1))
	,OperationCode6
	,OperationDate6
	,PlaceHolder29 = CAST('' as char(1))
	,OperationCode7
	,OperationDate7
	,PlaceHolder30 = CAST('' as char(1))
	,OperationCode8
	,OperationDate8
	,PlaceHolder31 = CAST('' as char(1))
	,OperationCode9
	,OperationDate9
	,PlaceHolder32 = CAST('' as char(1))
	,OperationCode10
	,OperationDate10
	,PlaceHolder33 = CAST('' as char(1))
	,OperationCode11
	,OperationDate11
	,PlaceHolder34 = CAST('' as char(1))
	,OperationCode12
	,OperationDate12
	,PlaceHolder35 = CAST('' as char(1))

	,PlaceHolder36 = CAST('' as char(7))
	,PlaceHolder37 = CAST('' as char(8))
	,PlaceHolder38 = CAST('' as char(1))
	,PlaceHolder39 = CAST('' as char(7))
	,PlaceHolder40 = CAST('' as char(8))
	,PlaceHolder41 = CAST('' as char(1))
	,PlaceHolder42 = CAST('' as char(7))
	,PlaceHolder43 = CAST('' as char(8))
	,PlaceHolder44 = CAST('' as char(1))
	,PlaceHolder45 = CAST('' as char(7))
	,PlaceHolder46 = CAST('' as char(8))
	,PlaceHolder47 = CAST('' as char(1))
	,PlaceHolder48 = CAST('' as char(7))
	,PlaceHolder49 = CAST('' as char(8))
	,PlaceHolder50 = CAST('' as char(1))
	,PlaceHolder51 = CAST('' as char(7))
	,PlaceHolder52 = CAST('' as char(8))
	,PlaceHolder53 = CAST('' as char(1))
	,PlaceHolder54 = CAST('' as char(7))
	,PlaceHolder55 = CAST('' as char(8))
	,PlaceHolder56 = CAST('' as char(1))
	,PlaceHolder57 = CAST('' as char(7))
	,PlaceHolder58 = CAST('' as char(8))
	,PlaceHolder59 = CAST('' as char(1))
	,PlaceHolder60 = CAST('' as char(7))
	,PlaceHolder61 = CAST('' as char(8))
	,PlaceHolder62 = CAST('' as char(1))
	,PlaceHolder63 = CAST('' as char(7))
	,PlaceHolder64 = CAST('' as char(8))
	,PlaceHolder65 = CAST('' as char(1))
	,PlaceHolder66 = CAST('' as char(7))
	,PlaceHolder67 = CAST('' as char(8))
	,PlaceHolder68 = CAST('' as char(1))
	,PlaceHolder69 = CAST('' as char(7))
	,PlaceHolder70 = CAST('' as char(8))
	,PlaceHolder71 = CAST('' as char(1))

	,EpisodeStartWardCode

	,PlaceHolder71a = CAST('' as char(8))
	,PlaceHolder72 = CAST('' as char(8))
	,PlaceHolder73 = CAST('' as char(8))
	,PlaceHolder74 = CAST('' as char(5))
	,PlaceHolder75 = CAST('' as char(5))
	,PlaceHolder76 = CAST('' as char(3))
	,PlaceHolder77 = CAST('' as char(2))
	,PlaceHolder78 = CAST('' as char(2))
	,PlaceHolder79 = CAST('' as char(4))
	,PlaceHolder80 = CAST('' as char(4))
	,PlaceHolder81 = CAST('' as char(5))
	,PlaceHolder82 = CAST('' as char(5))

	,EndWardTypeCode
	,EndSiteCode

	,PlaceHolder83 = CAST('' as char(4))
	,PlaceHolder84 = CAST('' as char(5))
	,PlaceHolder85 = CAST('' as char(6))
	,PlaceHolder86 = CAST('' as char(5))
	,PlaceHolder87 = CAST('' as char(18))
	,PlaceHolder88 = CAST('' as char(8))
	,PlaceHolder89 = CAST('' as char(8))
	,PlaceHolder90 = CAST('' as char(8))

	,AnteNatalGpCode
	,FirstAnteNatalAssessmentDate
	,PlaceHolder91 = CAST('' as char(1))
	,PreviousPregnancies
	,ActualDeliveryPlaceTypeCode
	,IntendedDeliveryPlaceTypeCode
	,DeliveryPlaceChangeReasonCode
	,GestationLength
	,LabourOnsetMethodCode
	,StatusOfPersonConductingDeliveryCode
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,NumberOfBabies

	,Baby1DeliveryDate
	,Baby1DeliveryMethodCode
	,Baby1SexCode
	,Baby1BirthOrder
	,Baby1LiveStillBirth
	,Baby1BirthWeight
	,Baby1MethodOfResuscitationCode
	,Baby1BirthDate
	,Baby1MotherBirthDate
	,PlaceHolder91a = CAST('' as char(1))
	,Baby2DeliveryDate
	,Baby2DeliveryMethodCode
	,Baby2SexCode
	,Baby2BirthOrder
	,Baby2LiveStillBirth
	,Baby2BirthWeight
	,Baby2MethodOfResuscitationCode
	,Baby2BirthDate
	,Baby2MotherBirthDate
	,PlaceHolder91b = CAST('' as char(1))
	,Baby3DeliveryDate
	,Baby3DeliveryMethodCode
	,Baby3SexCode
	,Baby3BirthOrder
	,Baby3LiveStillBirth
	,Baby3BirthWeight
	,Baby3MethodOfResuscitationCode
	,Baby3BirthDate
	,Baby3MotherBirthDate
	,PlaceHolder91c = CAST('' as char(1))
	,Baby4DeliveryDate
	,Baby4DeliveryMethodCode
	,Baby4SexCode
	,Baby4BirthOrder
	,Baby4LiveStillBirth
	,Baby4BirthWeight
	,Baby4MethodOfResuscitationCode
	,Baby4BirthDate
	,Baby4MotherBirthDate
	,PlaceHolder91d = CAST('' as char(1))
	,Baby5DeliveryDate
	,Baby5DeliveryMethodCode
	,Baby5SexCode
	,Baby5BirthOrder
	,Baby5LiveStillBirth
	,Baby5BirthWeight
	,Baby5MethodOfResuscitationCode
	,Baby5BirthDate
	,Baby5MotherBirthDate
	,PlaceHolder91e = CAST('' as char(1))
	,Baby6DeliveryDate
	,Baby6DeliveryMethodCode
	,Baby6SexCode
	,Baby6BirthOrder
	,Baby6LiveStillBirth
	,Baby6BirthWeight
	,Baby6MethodOfResuscitationCode
	,Baby6BirthDate
	,Baby6MotherBirthDate
	,PlaceHolder91f = CAST('' as char(1))
	,NumberOfWardTransfers
	,WardStay1Sequence
	,WardStay1TransferDate
	,WardStay1TransferTime
	,WardStay1HospitalCode
	,WardStay1WardCode
	,WardStay2Sequence
	,WardStay2TransferDate
	,WardStay2TransferTime
	,WardStay2HospitalCode
	,WardStay2WardCode
	,WardStay3Sequence
	,WardStay3TransferDate
	,WardStay3TransferTime
	,WardStay3HospitalCode
	,WardStay3WardCode
	,WardStay4Sequence
	,WardStay4TransferDate
	,WardStay4TransferTime
	,WardStay4HospitalCode
	,WardStay4WardCode
	,WardStay5Sequence
	,WardStay5TransferDate
	,WardStay5TransferTime
	,WardStay5HospitalCode
	,WardStay5WardCode
	,WardStay6Sequence
	,WardStay6TransferDate
	,WardStay6TransferTime
	,WardStay6HospitalCode
	,WardStay6WardCode
	,WardStay7Sequence
	,WardStay7TransferDate
	,WardStay7TransferTime
	,WardStay7HospitalCode
	,WardStay7WardCode
	,WardStay8Sequence
	,WardStay8TransferDate
	,WardStay8TransferTime
	,WardStay8HospitalCode
	,WardStay8WardCode
	,WardStay9Sequence
	,WardStay9TransferDate
	,WardStay9TransferTime
	,WardStay9HospitalCode
	,WardStay9WardCode
	,WardStay10Sequence
	,WardStay10TransferDate
	,WardStay10TransferTime
	,WardStay10HospitalCode
	,WardStay10WardCode
	,WardStay11Sequence
	,WardStay11TransferDate
	,WardStay11TransferTime
	,WardStay11HospitalCode
	,WardStay11WardCode
	,WardStay12Sequence
	,WardStay12TransferDate
	,WardStay12TransferTime
	,WardStay12HospitalCode
	,WardStay12WardCode
	,WardStay13Sequence
	,WardStay13TransferDate
	,WardStay13TransferTime
	,WardStay13HospitalCode
	,WardStay13WardCode
	,WardStay14Sequence
	,WardStay14TransferDate
	,WardStay14TransferTime
	,WardStay14HospitalCode
	,WardStay14WardCode
	,WardStay15Sequence
	,WardStay15TransferDate
	,WardStay15TransferTime
	,WardStay15HospitalCode
	,WardStay15WardCode
	,WardStay16Sequence
	,WardStay16TransferDate
	,WardStay16TransferTime
	,WardStay16HospitalCode
	,WardStay16WardCode
	,WardStay17Sequence
	,WardStay17TransferDate
	,WardStay17TransferTime
	,WardStay17HospitalCode
	,WardStay17WardCode
	,WardStay18Sequence
	,WardStay18TransferDate
	,WardStay18TransferTime
	,WardStay18HospitalCode
	,WardStay18WardCode
	,WardStay19Sequence
	,WardStay19TransferDate
	,WardStay19TransferTime
	,WardStay19HospitalCode
	,WardStay19WardCode
	,WardStay20Sequence
	,WardStay20TransferDate
	,WardStay20TransferTime
	,WardStay20HospitalCode
	,WardStay20WardCode
	,WardStay21Sequence
	,WardStay21TransferDate
	,WardStay21TransferTime
	,WardStay21HospitalCode
	,WardStay21WardCode
	,WardStay22Sequence
	,WardStay22TransferDate
	,WardStay22TransferTime
	,WardStay22HospitalCode
	,WardStay22WardCode
	,WardStay23Sequence
	,WardStay23TransferDate
	,WardStay23TransferTime
	,WardStay23HospitalCode
	,WardStay23WardCode
	,WardStay24Sequence
	,WardStay24TransferDate
	,WardStay24TransferTime
	,WardStay24HospitalCode
	,WardStay24WardCode
	,WardStay25Sequence
	,WardStay25TransferDate
	,WardStay25TransferTime
	,WardStay25HospitalCode
	,WardStay25WardCode
	,WardStay26Sequence
	,WardStay26TransferDate
	,WardStay26TransferTime
	,WardStay26HospitalCode
	,WardStay26WardCode
	,WardStay27Sequence
	,WardStay27TransferDate
	,WardStay27TransferTime
	,WardStay27HospitalCode
	,WardStay27WardCode
	,WardStay28Sequence
	,WardStay28TransferDate
	,WardStay28TransferTime
	,WardStay28HospitalCode
	,WardStay28WardCode
	,WardStay29Sequence
	,WardStay29TransferDate
	,WardStay29TransferTime
	,WardStay29HospitalCode
	,WardStay29WardCode
	,WardStay30Sequence
	,WardStay30TransferDate
	,WardStay30TransferTime
	,WardStay30HospitalCode
	,WardStay30WardCode
	,WardStay31Sequence
	,WardStay31TransferDate
	,WardStay31TransferTime
	,WardStay31HospitalCode
	,WardStay31WardCode
	,WardStay32Sequence
	,WardStay32TransferDate
	,WardStay32TransferTime
	,WardStay32HospitalCode
	,WardStay32WardCode
	,WardStay33Sequence
	,WardStay33TransferDate
	,WardStay33TransferTime
	,WardStay33HospitalCode
	,WardStay33WardCode
	,WardStay34Sequence
	,WardStay34TransferDate
	,WardStay34TransferTime
	,WardStay34HospitalCode
	,WardStay34WardCode
	,WardStay35Sequence
	,WardStay35TransferDate
	,WardStay35TransferTime
	,WardStay35HospitalCode
	,WardStay35WardCode
	,WardStay36Sequence
	,WardStay36TransferDate
	,WardStay36TransferTime
	,WardStay36HospitalCode
	,WardStay36WardCode
	,WardStay37Sequence
	,WardStay37TransferDate
	,WardStay37TransferTime
	,WardStay37HospitalCode
	,WardStay37WardCode
	,WardStay38Sequence
	,WardStay38TransferDate
	,WardStay38TransferTime
	,WardStay38HospitalCode
	,WardStay38WardCode
	,WardStay39Sequence
	,WardStay39TransferDate
	,WardStay39TransferTime
	,WardStay39HospitalCode
	,WardStay39WardCode
	,WardStay40Sequence
	,WardStay40TransferDate
	,WardStay40TransferTime
	,WardStay40HospitalCode
	,WardStay40WardCode
	,WardStay41Sequence
	,WardStay41TransferDate
	,WardStay41TransferTime
	,WardStay41HospitalCode
	,WardStay41WardCode
	,WardStay42Sequence
	,WardStay42TransferDate
	,WardStay42TransferTime
	,WardStay42HospitalCode
	,WardStay42WardCode
	,WardStay43Sequence
	,WardStay43TransferDate
	,WardStay43TransferTime
	,WardStay43HospitalCode
	,WardStay43WardCode
	,WardStay44Sequence
	,WardStay44TransferDate
	,WardStay44TransferTime
	,WardStay44HospitalCode
	,WardStay44WardCode
	,WardStay45Sequence
	,WardStay45TransferDate
	,WardStay45TransferTime
	,WardStay45HospitalCode
	,WardStay45WardCode
	,WardStay46Sequence
	,WardStay46TransferDate
	,WardStay46TransferTime
	,WardStay46HospitalCode
	,WardStay46WardCode
	,WardStay47Sequence
	,WardStay47TransferDate
	,WardStay47TransferTime
	,WardStay47HospitalCode
	,WardStay47WardCode
	,WardStay48Sequence
	,WardStay48TransferDate
	,WardStay48TransferTime
	,WardStay48HospitalCode
	,WardStay48WardCode
	,WardStay49Sequence
	,WardStay49TransferDate
	,WardStay49TransferTime
	,WardStay49HospitalCode
	,WardStay49WardCode
	,WardStay50Sequence
	,WardStay50TransferDate
	,WardStay50TransferTime
	,WardStay50HospitalCode
	,WardStay50WardCode
	,WardStay51Sequence
	,WardStay51TransferDate
	,WardStay51TransferTime
	,WardStay51HospitalCode
	,WardStay51WardCode
	,WardStay52Sequence
	,WardStay52TransferDate
	,WardStay52TransferTime
	,WardStay52HospitalCode
	,WardStay52WardCode
	,WardStay53Sequence
	,WardStay53TransferDate
	,WardStay53TransferTime
	,WardStay53HospitalCode
	,WardStay53WardCode
	,WardStay54Sequence
	,WardStay54TransferDate
	,WardStay54TransferTime
	,WardStay54HospitalCode
	,WardStay54WardCode
	,WardStay55Sequence
	,WardStay55TransferDate
	,WardStay55TransferTime
	,WardStay55HospitalCode
	,WardStay55WardCode
	,WardStay56Sequence
	,WardStay56TransferDate
	,WardStay56TransferTime
	,WardStay56HospitalCode
	,WardStay56WardCode
	,WardStay57Sequence
	,WardStay57TransferDate
	,WardStay57TransferTime
	,WardStay57HospitalCode
	,WardStay57WardCode
	,WardStay58Sequence
	,WardStay58TransferDate
	,WardStay58TransferTime
	,WardStay58HospitalCode
	,WardStay58WardCode
	,WardStay59Sequence
	,WardStay59TransferDate
	,WardStay59TransferTime
	,WardStay59HospitalCode
	,WardStay59WardCode
	,WardStay60Sequence
	,WardStay60TransferDate
	,WardStay60TransferTime
	,WardStay60HospitalCode
	,WardStay60WardCode
	,WardStay61Sequence
	,WardStay61TransferDate
	,WardStay61TransferTime
	,WardStay61HospitalCode
	,WardStay61WardCode
	,WardStay62Sequence
	,WardStay62TransferDate
	,WardStay62TransferTime
	,WardStay62HospitalCode
	,WardStay62WardCode
	,WardStay63Sequence
	,WardStay63TransferDate
	,WardStay63TransferTime
	,WardStay63HospitalCode
	,WardStay63WardCode
	,WardStay64Sequence
	,WardStay64TransferDate
	,WardStay64TransferTime
	,WardStay64HospitalCode
	,WardStay64WardCode
	,WardStay65Sequence
	,WardStay65TransferDate
	,WardStay65TransferTime
	,WardStay65HospitalCode
	,WardStay65WardCode
	,WardStay66Sequence
	,WardStay66TransferDate
	,WardStay66TransferTime
	,WardStay66HospitalCode
	,WardStay66WardCode
	,WardStay67Sequence
	,WardStay67TransferDate
	,WardStay67TransferTime
	,WardStay67HospitalCode
	,WardStay67WardCode
	,WardStay68Sequence
	,WardStay68TransferDate
	,WardStay68TransferTime
	,WardStay68HospitalCode
	,WardStay68WardCode
	,WardStay69Sequence
	,WardStay69TransferDate
	,WardStay69TransferTime
	,WardStay69HospitalCode
	,WardStay69WardCode
	,WardStay70Sequence
	,WardStay70TransferDate
	,WardStay70TransferTime
	,WardStay70HospitalCode
	,WardStay70WardCode
	,WardStay71Sequence
	,WardStay71TransferDate
	,WardStay71TransferTime
	,WardStay71HospitalCode
	,WardStay71WardCode
	,WardStay72Sequence
	,WardStay72TransferDate
	,WardStay72TransferTime
	,WardStay72HospitalCode
	,WardStay72WardCode
	,WardStay73Sequence
	,WardStay73TransferDate
	,WardStay73TransferTime
	,WardStay73HospitalCode
	,WardStay73WardCode
	,WardStay74Sequence
	,WardStay74TransferDate
	,WardStay74TransferTime
	,WardStay74HospitalCode
	,WardStay74WardCode
	,NNNStatusIndicator
	,LegalStatusClassificationCode

	,PlaceHolder92 = CAST('' as char(419))
	,PlaceHolder92a = CAST('' as char(246))

	,HRGCode
	,PlaceHolder93 = CAST('' as char(4))
	,PlaceHolder94 = CAST('' as char(3))
	,PCTofResidenceCode
	,PlaceHolder95 = CAST('' as char(3))
	,PlaceHolder96 = CAST('' as char(5))
	,PlaceHolder97 = CAST('' as char(5))
	,HRGVersionNumber
	,DGVPCode
	,LengthOfGestationAssessment

	,PlaceHolder98 = CAST('' as char(7))
	,PlaceHolder99 = CAST('' as char(5))
	,PlaceHolder100 = CAST('' as char(2))
	,PlaceHolder101 = CAST('' as char(4))

	,CCP1Number
	,CCP1LocalIdentifier
	,CCP1StartDate
	,CCP1UnitFunction
	,CCP1AdvancedRespiratorySupportDays
	,CCP1BasicRespiratorySupportDays
	,CCP1AdvancedCardiovascularSupportDays
	,CCP1BasicCardiovascularSupportDays
	,CCP1RenalSupportDays
	,CCP1NeurologicalSupportDays
	,CCP1DermatologicalSupportDays
	,CCP1LiverSupportDays
	,CCP1Level2Days
	,CCP1Level3Days
	,CCP1DischargeDate
	,CCP1DischargeTime
	,CCP1MaximumNumberOfOrganSupportSystem
	,CCP1DetailsBlankPad = CAST('' as char(6))
	,CCP2Number
	,CCP2LocalIdentifier
	,CCP2StartDate
	,CCP2UnitFunction
	,CCP2AdvancedRespiratorySupportDays
	,CCP2BasicRespiratorySupportDays
	,CCP2AdvancedCardiovascularSupportDays
	,CCP2BasicCardiovascularSupportDays
	,CCP2RenalSupportDays
	,CCP2NeurologicalSupportDays
	,CCP2DermatologicalSupportDays
	,CCP2LiverSupportDays
	,CCP2Level2Days
	,CCP2Level3Days
	,CCP2DischargeDate
	,CCP2DischargeTime
	,CCP2MaximumNumberOfOrganSupportSystem
	,CCP2DetailsBlankPad = CAST('' as char(6))
	,CCP3Number
	,CCP3LocalIdentifier
	,CCP3StartDate
	,CCP3UnitFunction
	,CCP3AdvancedRespiratorySupportDays
	,CCP3BasicRespiratorySupportDays
	,CCP3AdvancedCardiovascularSupportDays
	,CCP3BasicCardiovascularSupportDays
	,CCP3RenalSupportDays
	,CCP3NeurologicalSupportDays
	,CCP3DermatologicalSupportDays
	,CCP3LiverSupportDays
	,CCP3Level2Days
	,CCP3Level3Days
	,CCP3DischargeDate
	,CCP3DischargeTime
	,CCP3MaximumNumberOfOrganSupportSystem
	,CCP3DetailsBlankPad = CAST('' as char(6))
	,CCP4Number
	,CCP4LocalIdentifier
	,CCP4StartDate
	,CCP4UnitFunction
	,CCP4AdvancedRespiratorySupportDays
	,CCP4BasicRespiratorySupportDays
	,CCP4AdvancedCardiovascularSupportDays
	,CCP4BasicCardiovascularSupportDays
	,CCP4RenalSupportDays
	,CCP4NeurologicalSupportDays
	,CCP4DermatologicalSupportDays
	,CCP4LiverSupportDays
	,CCP4Level2Days
	,CCP4Level3Days
	,CCP4DischargeDate
	,CCP4DischargeTime
	,CCP4MaximumNumberOfOrganSupportSystem
	,CCP4DetailsBlankPad = CAST('' as char(6))
	,CCP5Number
	,CCP5LocalIdentifier
	,CCP5StartDate
	,CCP5UnitFunction
	,CCP5AdvancedRespiratorySupportDays
	,CCP5BasicRespiratorySupportDays
	,CCP5AdvancedCardiovascularSupportDays
	,CCP5BasicCardiovascularSupportDays
	,CCP5RenalSupportDays
	,CCP5NeurologicalSupportDays
	,CCP5DermatologicalSupportDays
	,CCP5LiverSupportDays
	,CCP5Level2Days
	,CCP5Level3Days
	,CCP5DischargeDate
	,CCP5DischargeTime
	,CCP5MaximumNumberOfOrganSupportSystem
	,CCP5DetailsBlankPad = CAST('' as char(6))
	,CCP6Number
	,CCP6LocalIdentifier
	,CCP6StartDate
	,CCP6UnitFunction
	,CCP6AdvancedRespiratorySupportDays
	,CCP6BasicRespiratorySupportDays
	,CCP6AdvancedCardiovascularSupportDays
	,CCP6BasicCardiovascularSupportDays
	,CCP6RenalSupportDays
	,CCP6NeurologicalSupportDays
	,CCP6DermatologicalSupportDays
	,CCP6LiverSupportDays
	,CCP6Level2Days
	,CCP6Level3Days
	,CCP6DischargeDate
	,CCP6DischargeTime
	,CCP6MaximumNumberOfOrganSupportSystem
	,CCP6DetailsBlankPad = CAST('' as char(6))
	,CCP7Number
	,CCP7LocalIdentifier
	,CCP7StartDate
	,CCP7UnitFunction
	,CCP7AdvancedRespiratorySupportDays
	,CCP7BasicRespiratorySupportDays
	,CCP7AdvancedCardiovascularSupportDays
	,CCP7BasicCardiovascularSupportDays
	,CCP7RenalSupportDays
	,CCP7NeurologicalSupportDays
	,CCP7DermatologicalSupportDays
	,CCP7LiverSupportDays
	,CCP7Level2Days
	,CCP7Level3Days
	,CCP7DischargeDate
	,CCP7DischargeTime
	,CCP7MaximumNumberOfOrganSupportSystem
	,CCP7DetailsBlankPad = CAST('' as char(6))
	,CCP8Number
	,CCP8LocalIdentifier
	,CCP8StartDate
	,CCP8UnitFunction
	,CCP8AdvancedRespiratorySupportDays
	,CCP8BasicRespiratorySupportDays
	,CCP8AdvancedCardiovascularSupportDays
	,CCP8BasicCardiovascularSupportDays
	,CCP8RenalSupportDays
	,CCP8NeurologicalSupportDays
	,CCP8DermatologicalSupportDays
	,CCP8LiverSupportDays
	,CCP8Level2Days
	,CCP8Level3Days
	,CCP8DischargeDate
	,CCP8DischargeTime
	,CCP8MaximumNumberOfOrganSupportSystem
	,CCP8DetailsBlankPad = CAST('' as char(6))
	,CCP9Number
	,CCP9LocalIdentifier
	,CCP9StartDate
	,CCP9UnitFunction
	,CCP9AdvancedRespiratorySupportDays
	,CCP9BasicRespiratorySupportDays
	,CCP9AdvancedCardiovascularSupportDays
	,CCP9BasicCardiovascularSupportDays
	,CCP9RenalSupportDays
	,CCP9NeurologicalSupportDays
	,CCP9DermatologicalSupportDays
	,CCP9LiverSupportDays
	,CCP9Level2Days
	,CCP9Level3Days
	,CCP9DischargeDate
	,CCP9DischargeTime
	,CCP9MaximumNumberOfOrganSupportSystem
	,CCP9DetailsBlankPad = CAST('' as char(6))
	,AgeAtCDSActivityDate
	,AgeOnAdmission
	,DischargeReadyDate
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,EarliestReasonableOfferDate
	,NCCPPresent
	,Baby1NHSNumberStatus
	,Baby1NHSNumber
	,Baby2NHSNumberStatus
	,Baby2NHSNumber
	,Baby3NHSNumberStatus
	,Baby3NHSNumber
	,Baby4NHSNumberStatus
	,Baby4NHSNumber
	,Baby5NHSNumberStatus
	,Baby5NHSNumber
	,Baby6NHSNumberStatus
	,Baby6NHSNumber
	,EndOfRecord
FROM
	CDS.APCAngliaBase


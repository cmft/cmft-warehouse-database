﻿





CREATE view [CDS].[APCAngliaNet] as

--Updates - UpdateType = '9'
select
	 Base.UniqueEpisodeSerialNo
	,UpdateType = '9'
	,Base.CDSUpdateDate
	,Base.CDSUpdateTime
	,Base.CDSType
	,Base.RecordType
	,Base.ProviderCode
	,Base.PurchaserCode
	,Base.CommissioningSerialNo
	,Base.NHSServiceAgreementLineNo
	,Base.PurchaserReferenceNo
	,Base.NHSNumber
	,Base.PatientName
	,Base.PatientsAddress
	,Base.Postcode
	,Base.SexCode
	,Base.CarerSupportIndicator
	,Base.DateOfBirth
	,Base.RegisteredGpCode
	,Base.RegisteredGpPracticeCode
	,Base.DistrictNo
	,Base.EthnicGroupCode
	,Base.ReferrerCode
	,Base.ReferringOrganisationCode
	,Base.ProviderSpellNo
	,Base.PatientCategoryCode
	,Base.DecidedToAdmitDate
	,Base.AdmissionDate
	,Base.AdmissionMethodCode
	,Base.DurationOfElectiveWait
	,Base.ManagementIntentionCode
	,Base.SourceOfAdmissionCode
	,Base.DischargeDate
	,Base.DischargeMethodCode
	,Base.DischargeDestinationCode
	,Base.PatientClassificationCode
	,Base.EpisodeNo
	,Base.LastEpisodeInSpellIndicator
	,Base.StartSiteCode
	,Base.StartWardTypeCode
	,Base.EpisodeStartDate
	,Base.EpisodeEndDate
	,Base.MainSpecialtyCode
	,Base.TreatmentFunctionCode
	,Base.ConsultantCode
	,Base.FirstRegularDayNightAdmission
	,Base.NeonatalLevelOfCare
	,Base.PsychiatricPatientStatusCode
	,Base.PrimaryDiagnosisCode
	,Base.SecondaryDiagnosisCode1
	,Base.SecondaryDiagnosisCode2
	,Base.SecondaryDiagnosisCode3
	,Base.SecondaryDiagnosisCode4
	,Base.SecondaryDiagnosisCode5
	,Base.SecondaryDiagnosisCode6
	,Base.SecondaryDiagnosisCode7
	,Base.SecondaryDiagnosisCode8
	,Base.SecondaryDiagnosisCode9
	,Base.SecondaryDiagnosisCode10
	,Base.SecondaryDiagnosisCode11
	,Base.SecondaryDiagnosisCode12
	,Base.OperationStatus
	,Base.PrimaryOperationCode
	,Base.PrimaryOperationDate
	,Base.OperationCode2
	,Base.OperationDate2
	,Base.OperationCode3
	,Base.OperationDate3
	,Base.OperationCode4
	,Base.OperationDate4
	,Base.OperationCode5
	,Base.OperationDate5
	,Base.OperationCode6
	,Base.OperationDate6
	,Base.OperationCode7
	,Base.OperationDate7
	,Base.OperationCode8
	,Base.OperationDate8
	,Base.OperationCode9
	,Base.OperationDate9
	,Base.OperationCode10
	,Base.OperationDate10
	,Base.OperationCode11
	,Base.OperationDate11
	,Base.OperationCode12
	,Base.OperationDate12
	,Base.EpisodeStartWardCode
	,Base.EndWardTypeCode
	,Base.EndSiteCode
	,Base.AnteNatalGpCode
	,Base.FirstAnteNatalAssessmentDate
	,Base.PreviousPregnancies
	,Base.ActualDeliveryPlaceTypeCode
	,Base.IntendedDeliveryPlaceTypeCode
	,Base.DeliveryPlaceChangeReasonCode
	,Base.GestationLength
	,Base.LabourOnsetMethodCode
	,Base.StatusOfPersonConductingDeliveryCode
	,Base.AnaestheticGivenDuringCode
	,Base.AnaestheticGivenPostCode
	,Base.NumberOfBabies
	,Base.Baby1DeliveryDate
	,Base.Baby1DeliveryMethodCode
	,Base.Baby1SexCode
	,Base.Baby1BirthOrder
	,Base.Baby1LiveStillBirth
	,Base.Baby1BirthWeight
	,Base.Baby1MethodOfResuscitationCode
	,Base.Baby1BirthDate
	,Base.Baby1MotherBirthDate
	,Base.Baby2DeliveryDate
	,Base.Baby2DeliveryMethodCode
	,Base.Baby2SexCode
	,Base.Baby2BirthOrder
	,Base.Baby2LiveStillBirth
	,Base.Baby2BirthWeight
	,Base.Baby2MethodOfResuscitationCode
	,Base.Baby2BirthDate
	,Base.Baby2MotherBirthDate
	,Base.Baby3DeliveryDate
	,Base.Baby3DeliveryMethodCode
	,Base.Baby3SexCode
	,Base.Baby3BirthOrder
	,Base.Baby3LiveStillBirth
	,Base.Baby3BirthWeight
	,Base.Baby3MethodOfResuscitationCode
	,Base.Baby3BirthDate
	,Base.Baby3MotherBirthDate
	,Base.Baby4DeliveryDate
	,Base.Baby4DeliveryMethodCode
	,Base.Baby4SexCode
	,Base.Baby4BirthOrder
	,Base.Baby4LiveStillBirth
	,Base.Baby4BirthWeight
	,Base.Baby4MethodOfResuscitationCode
	,Base.Baby4BirthDate
	,Base.Baby4MotherBirthDate
	,Base.Baby5DeliveryDate
	,Base.Baby5DeliveryMethodCode
	,Base.Baby5SexCode
	,Base.Baby5BirthOrder
	,Base.Baby5LiveStillBirth
	,Base.Baby5BirthWeight
	,Base.Baby5MethodOfResuscitationCode
	,Base.Baby5BirthDate
	,Base.Baby5MotherBirthDate
	,Base.Baby6DeliveryDate
	,Base.Baby6DeliveryMethodCode
	,Base.Baby6SexCode
	,Base.Baby6BirthOrder
	,Base.Baby6LiveStillBirth
	,Base.Baby6BirthWeight
	,Base.Baby6MethodOfResuscitationCode
	,Base.Baby6BirthDate
	,Base.Baby6MotherBirthDate
	,Base.NumberOfWardTransfers
	,Base.WardStay1Sequence
	,Base.WardStay1TransferDate
	,Base.WardStay1TransferTime
	,Base.WardStay1HospitalCode
	,Base.WardStay1WardCode
	,Base.WardStay2Sequence
	,Base.WardStay2TransferDate
	,Base.WardStay2TransferTime
	,Base.WardStay2HospitalCode
	,Base.WardStay2WardCode
	,Base.WardStay3Sequence
	,Base.WardStay3TransferDate
	,Base.WardStay3TransferTime
	,Base.WardStay3HospitalCode
	,Base.WardStay3WardCode
	,Base.WardStay4Sequence
	,Base.WardStay4TransferDate
	,Base.WardStay4TransferTime
	,Base.WardStay4HospitalCode
	,Base.WardStay4WardCode
	,Base.WardStay5Sequence
	,Base.WardStay5TransferDate
	,Base.WardStay5TransferTime
	,Base.WardStay5HospitalCode
	,Base.WardStay5WardCode
	,Base.WardStay6Sequence
	,Base.WardStay6TransferDate
	,Base.WardStay6TransferTime
	,Base.WardStay6HospitalCode
	,Base.WardStay6WardCode
	,Base.WardStay7Sequence
	,Base.WardStay7TransferDate
	,Base.WardStay7TransferTime
	,Base.WardStay7HospitalCode
	,Base.WardStay7WardCode
	,Base.WardStay8Sequence
	,Base.WardStay8TransferDate
	,Base.WardStay8TransferTime
	,Base.WardStay8HospitalCode
	,Base.WardStay8WardCode
	,Base.WardStay9Sequence
	,Base.WardStay9TransferDate
	,Base.WardStay9TransferTime
	,Base.WardStay9HospitalCode
	,Base.WardStay9WardCode
	,Base.WardStay10Sequence
	,Base.WardStay10TransferDate
	,Base.WardStay10TransferTime
	,Base.WardStay10HospitalCode
	,Base.WardStay10WardCode
	,Base.WardStay11Sequence
	,Base.WardStay11TransferDate
	,Base.WardStay11TransferTime
	,Base.WardStay11HospitalCode
	,Base.WardStay11WardCode
	,Base.WardStay12Sequence
	,Base.WardStay12TransferDate
	,Base.WardStay12TransferTime
	,Base.WardStay12HospitalCode
	,Base.WardStay12WardCode
	,Base.WardStay13Sequence
	,Base.WardStay13TransferDate
	,Base.WardStay13TransferTime
	,Base.WardStay13HospitalCode
	,Base.WardStay13WardCode
	,Base.WardStay14Sequence
	,Base.WardStay14TransferDate
	,Base.WardStay14TransferTime
	,Base.WardStay14HospitalCode
	,Base.WardStay14WardCode
	,Base.WardStay15Sequence
	,Base.WardStay15TransferDate
	,Base.WardStay15TransferTime
	,Base.WardStay15HospitalCode
	,Base.WardStay15WardCode
	,Base.WardStay16Sequence
	,Base.WardStay16TransferDate
	,Base.WardStay16TransferTime
	,Base.WardStay16HospitalCode
	,Base.WardStay16WardCode
	,Base.WardStay17Sequence
	,Base.WardStay17TransferDate
	,Base.WardStay17TransferTime
	,Base.WardStay17HospitalCode
	,Base.WardStay17WardCode
	,Base.WardStay18Sequence
	,Base.WardStay18TransferDate
	,Base.WardStay18TransferTime
	,Base.WardStay18HospitalCode
	,Base.WardStay18WardCode
	,Base.WardStay19Sequence
	,Base.WardStay19TransferDate
	,Base.WardStay19TransferTime
	,Base.WardStay19HospitalCode
	,Base.WardStay19WardCode
	,Base.WardStay20Sequence
	,Base.WardStay20TransferDate
	,Base.WardStay20TransferTime
	,Base.WardStay20HospitalCode
	,Base.WardStay20WardCode
	,Base.WardStay21Sequence
	,Base.WardStay21TransferDate
	,Base.WardStay21TransferTime
	,Base.WardStay21HospitalCode
	,Base.WardStay21WardCode
	,Base.WardStay22Sequence
	,Base.WardStay22TransferDate
	,Base.WardStay22TransferTime
	,Base.WardStay22HospitalCode
	,Base.WardStay22WardCode
	,Base.WardStay23Sequence
	,Base.WardStay23TransferDate
	,Base.WardStay23TransferTime
	,Base.WardStay23HospitalCode
	,Base.WardStay23WardCode
	,Base.WardStay24Sequence
	,Base.WardStay24TransferDate
	,Base.WardStay24TransferTime
	,Base.WardStay24HospitalCode
	,Base.WardStay24WardCode
	,Base.WardStay25Sequence
	,Base.WardStay25TransferDate
	,Base.WardStay25TransferTime
	,Base.WardStay25HospitalCode
	,Base.WardStay25WardCode
	,Base.WardStay26Sequence
	,Base.WardStay26TransferDate
	,Base.WardStay26TransferTime
	,Base.WardStay26HospitalCode
	,Base.WardStay26WardCode
	,Base.WardStay27Sequence
	,Base.WardStay27TransferDate
	,Base.WardStay27TransferTime
	,Base.WardStay27HospitalCode
	,Base.WardStay27WardCode
	,Base.WardStay28Sequence
	,Base.WardStay28TransferDate
	,Base.WardStay28TransferTime
	,Base.WardStay28HospitalCode
	,Base.WardStay28WardCode
	,Base.WardStay29Sequence
	,Base.WardStay29TransferDate
	,Base.WardStay29TransferTime
	,Base.WardStay29HospitalCode
	,Base.WardStay29WardCode
	,Base.WardStay30Sequence
	,Base.WardStay30TransferDate
	,Base.WardStay30TransferTime
	,Base.WardStay30HospitalCode
	,Base.WardStay30WardCode
	,Base.WardStay31Sequence
	,Base.WardStay31TransferDate
	,Base.WardStay31TransferTime
	,Base.WardStay31HospitalCode
	,Base.WardStay31WardCode
	,Base.WardStay32Sequence
	,Base.WardStay32TransferDate
	,Base.WardStay32TransferTime
	,Base.WardStay32HospitalCode
	,Base.WardStay32WardCode
	,Base.WardStay33Sequence
	,Base.WardStay33TransferDate
	,Base.WardStay33TransferTime
	,Base.WardStay33HospitalCode
	,Base.WardStay33WardCode
	,Base.WardStay34Sequence
	,Base.WardStay34TransferDate
	,Base.WardStay34TransferTime
	,Base.WardStay34HospitalCode
	,Base.WardStay34WardCode
	,Base.WardStay35Sequence
	,Base.WardStay35TransferDate
	,Base.WardStay35TransferTime
	,Base.WardStay35HospitalCode
	,Base.WardStay35WardCode
	,Base.WardStay36Sequence
	,Base.WardStay36TransferDate
	,Base.WardStay36TransferTime
	,Base.WardStay36HospitalCode
	,Base.WardStay36WardCode
	,Base.WardStay37Sequence
	,Base.WardStay37TransferDate
	,Base.WardStay37TransferTime
	,Base.WardStay37HospitalCode
	,Base.WardStay37WardCode
	,Base.WardStay38Sequence
	,Base.WardStay38TransferDate
	,Base.WardStay38TransferTime
	,Base.WardStay38HospitalCode
	,Base.WardStay38WardCode
	,Base.WardStay39Sequence
	,Base.WardStay39TransferDate
	,Base.WardStay39TransferTime
	,Base.WardStay39HospitalCode
	,Base.WardStay39WardCode
	,Base.WardStay40Sequence
	,Base.WardStay40TransferDate
	,Base.WardStay40TransferTime
	,Base.WardStay40HospitalCode
	,Base.WardStay40WardCode
	,Base.WardStay41Sequence
	,Base.WardStay41TransferDate
	,Base.WardStay41TransferTime
	,Base.WardStay41HospitalCode
	,Base.WardStay41WardCode
	,Base.WardStay42Sequence
	,Base.WardStay42TransferDate
	,Base.WardStay42TransferTime
	,Base.WardStay42HospitalCode
	,Base.WardStay42WardCode
	,Base.WardStay43Sequence
	,Base.WardStay43TransferDate
	,Base.WardStay43TransferTime
	,Base.WardStay43HospitalCode
	,Base.WardStay43WardCode
	,Base.WardStay44Sequence
	,Base.WardStay44TransferDate
	,Base.WardStay44TransferTime
	,Base.WardStay44HospitalCode
	,Base.WardStay44WardCode
	,Base.WardStay45Sequence
	,Base.WardStay45TransferDate
	,Base.WardStay45TransferTime
	,Base.WardStay45HospitalCode
	,Base.WardStay45WardCode
	,Base.WardStay46Sequence
	,Base.WardStay46TransferDate
	,Base.WardStay46TransferTime
	,Base.WardStay46HospitalCode
	,Base.WardStay46WardCode
	,Base.WardStay47Sequence
	,Base.WardStay47TransferDate
	,Base.WardStay47TransferTime
	,Base.WardStay47HospitalCode
	,Base.WardStay47WardCode
	,Base.WardStay48Sequence
	,Base.WardStay48TransferDate
	,Base.WardStay48TransferTime
	,Base.WardStay48HospitalCode
	,Base.WardStay48WardCode
	,Base.WardStay49Sequence
	,Base.WardStay49TransferDate
	,Base.WardStay49TransferTime
	,Base.WardStay49HospitalCode
	,Base.WardStay49WardCode
	,Base.WardStay50Sequence
	,Base.WardStay50TransferDate
	,Base.WardStay50TransferTime
	,Base.WardStay50HospitalCode
	,Base.WardStay50WardCode
	,Base.WardStay51Sequence
	,Base.WardStay51TransferDate
	,Base.WardStay51TransferTime
	,Base.WardStay51HospitalCode
	,Base.WardStay51WardCode
	,Base.WardStay52Sequence
	,Base.WardStay52TransferDate
	,Base.WardStay52TransferTime
	,Base.WardStay52HospitalCode
	,Base.WardStay52WardCode
	,Base.WardStay53Sequence
	,Base.WardStay53TransferDate
	,Base.WardStay53TransferTime
	,Base.WardStay53HospitalCode
	,Base.WardStay53WardCode
	,Base.WardStay54Sequence
	,Base.WardStay54TransferDate
	,Base.WardStay54TransferTime
	,Base.WardStay54HospitalCode
	,Base.WardStay54WardCode
	,Base.WardStay55Sequence
	,Base.WardStay55TransferDate
	,Base.WardStay55TransferTime
	,Base.WardStay55HospitalCode
	,Base.WardStay55WardCode
	,Base.WardStay56Sequence
	,Base.WardStay56TransferDate
	,Base.WardStay56TransferTime
	,Base.WardStay56HospitalCode
	,Base.WardStay56WardCode
	,Base.WardStay57Sequence
	,Base.WardStay57TransferDate
	,Base.WardStay57TransferTime
	,Base.WardStay57HospitalCode
	,Base.WardStay57WardCode
	,Base.WardStay58Sequence
	,Base.WardStay58TransferDate
	,Base.WardStay58TransferTime
	,Base.WardStay58HospitalCode
	,Base.WardStay58WardCode
	,Base.WardStay59Sequence
	,Base.WardStay59TransferDate
	,Base.WardStay59TransferTime
	,Base.WardStay59HospitalCode
	,Base.WardStay59WardCode
	,Base.WardStay60Sequence
	,Base.WardStay60TransferDate
	,Base.WardStay60TransferTime
	,Base.WardStay60HospitalCode
	,Base.WardStay60WardCode
	,Base.WardStay61Sequence
	,Base.WardStay61TransferDate
	,Base.WardStay61TransferTime
	,Base.WardStay61HospitalCode
	,Base.WardStay61WardCode
	,Base.WardStay62Sequence
	,Base.WardStay62TransferDate
	,Base.WardStay62TransferTime
	,Base.WardStay62HospitalCode
	,Base.WardStay62WardCode
	,Base.WardStay63Sequence
	,Base.WardStay63TransferDate
	,Base.WardStay63TransferTime
	,Base.WardStay63HospitalCode
	,Base.WardStay63WardCode
	,Base.WardStay64Sequence
	,Base.WardStay64TransferDate
	,Base.WardStay64TransferTime
	,Base.WardStay64HospitalCode
	,Base.WardStay64WardCode
	,Base.WardStay65Sequence
	,Base.WardStay65TransferDate
	,Base.WardStay65TransferTime
	,Base.WardStay65HospitalCode
	,Base.WardStay65WardCode
	,Base.WardStay66Sequence
	,Base.WardStay66TransferDate
	,Base.WardStay66TransferTime
	,Base.WardStay66HospitalCode
	,Base.WardStay66WardCode
	,Base.WardStay67Sequence
	,Base.WardStay67TransferDate
	,Base.WardStay67TransferTime
	,Base.WardStay67HospitalCode
	,Base.WardStay67WardCode
	,Base.WardStay68Sequence
	,Base.WardStay68TransferDate
	,Base.WardStay68TransferTime
	,Base.WardStay68HospitalCode
	,Base.WardStay68WardCode
	,Base.WardStay69Sequence
	,Base.WardStay69TransferDate
	,Base.WardStay69TransferTime
	,Base.WardStay69HospitalCode
	,Base.WardStay69WardCode
	,Base.WardStay70Sequence
	,Base.WardStay70TransferDate
	,Base.WardStay70TransferTime
	,Base.WardStay70HospitalCode
	,Base.WardStay70WardCode
	,Base.WardStay71Sequence
	,Base.WardStay71TransferDate
	,Base.WardStay71TransferTime
	,Base.WardStay71HospitalCode
	,Base.WardStay71WardCode
	,Base.WardStay72Sequence
	,Base.WardStay72TransferDate
	,Base.WardStay72TransferTime
	,Base.WardStay72HospitalCode
	,Base.WardStay72WardCode
	,Base.WardStay73Sequence
	,Base.WardStay73TransferDate
	,Base.WardStay73TransferTime
	,Base.WardStay73HospitalCode
	,Base.WardStay73WardCode
	,Base.WardStay74Sequence
	,Base.WardStay74TransferDate
	,Base.WardStay74TransferTime
	,Base.WardStay74HospitalCode
	,Base.WardStay74WardCode
	,Base.NNNStatusIndicator
	,Base.LegalStatusClassificationCode
	,Base.HRGCode
	,Base.PCTofResidenceCode
	,Base.HRGVersionNumber
	,Base.DGVPCode
	,Base.LengthOfGestationAssessment
	,Base.CCP1Number
	,Base.CCP1LocalIdentifier
	,Base.CCP1StartDate
	,Base.CCP1UnitFunction
	,Base.CCP1AdvancedRespiratorySupportDays
	,Base.CCP1BasicRespiratorySupportDays
	,Base.CCP1AdvancedCardiovascularSupportDays
	,Base.CCP1BasicCardiovascularSupportDays
	,Base.CCP1RenalSupportDays
	,Base.CCP1NeurologicalSupportDays
	,Base.CCP1DermatologicalSupportDays
	,Base.CCP1LiverSupportDays
	,Base.CCP1Level2Days
	,Base.CCP1Level3Days
	,Base.CCP1DischargeDate
	,Base.CCP1DischargeTime
	,Base.CCP1MaximumNumberOfOrganSupportSystem
	,Base.CCP2Number
	,Base.CCP2LocalIdentifier
	,Base.CCP2StartDate
	,Base.CCP2UnitFunction
	,Base.CCP2AdvancedRespiratorySupportDays
	,Base.CCP2BasicRespiratorySupportDays
	,Base.CCP2AdvancedCardiovascularSupportDays
	,Base.CCP2BasicCardiovascularSupportDays
	,Base.CCP2RenalSupportDays
	,Base.CCP2NeurologicalSupportDays
	,Base.CCP2DermatologicalSupportDays
	,Base.CCP2LiverSupportDays
	,Base.CCP2Level2Days
	,Base.CCP2Level3Days
	,Base.CCP2DischargeDate
	,Base.CCP2DischargeTime
	,Base.CCP2MaximumNumberOfOrganSupportSystem
	,Base.CCP3Number
	,Base.CCP3LocalIdentifier
	,Base.CCP3StartDate
	,Base.CCP3UnitFunction
	,Base.CCP3AdvancedRespiratorySupportDays
	,Base.CCP3BasicRespiratorySupportDays
	,Base.CCP3AdvancedCardiovascularSupportDays
	,Base.CCP3BasicCardiovascularSupportDays
	,Base.CCP3RenalSupportDays
	,Base.CCP3NeurologicalSupportDays
	,Base.CCP3DermatologicalSupportDays
	,Base.CCP3LiverSupportDays
	,Base.CCP3Level2Days
	,Base.CCP3Level3Days
	,Base.CCP3DischargeDate
	,Base.CCP3DischargeTime
	,Base.CCP3MaximumNumberOfOrganSupportSystem
	,Base.CCP4Number
	,Base.CCP4LocalIdentifier
	,Base.CCP4StartDate
	,Base.CCP4UnitFunction
	,Base.CCP4AdvancedRespiratorySupportDays
	,Base.CCP4BasicRespiratorySupportDays
	,Base.CCP4AdvancedCardiovascularSupportDays
	,Base.CCP4BasicCardiovascularSupportDays
	,Base.CCP4RenalSupportDays
	,Base.CCP4NeurologicalSupportDays
	,Base.CCP4DermatologicalSupportDays
	,Base.CCP4LiverSupportDays
	,Base.CCP4Level2Days
	,Base.CCP4Level3Days
	,Base.CCP4DischargeDate
	,Base.CCP4DischargeTime
	,Base.CCP4MaximumNumberOfOrganSupportSystem
	,Base.CCP5Number
	,Base.CCP5LocalIdentifier
	,Base.CCP5StartDate
	,Base.CCP5UnitFunction
	,Base.CCP5AdvancedRespiratorySupportDays
	,Base.CCP5BasicRespiratorySupportDays
	,Base.CCP5AdvancedCardiovascularSupportDays
	,Base.CCP5BasicCardiovascularSupportDays
	,Base.CCP5RenalSupportDays
	,Base.CCP5NeurologicalSupportDays
	,Base.CCP5DermatologicalSupportDays
	,Base.CCP5LiverSupportDays
	,Base.CCP5Level2Days
	,Base.CCP5Level3Days
	,Base.CCP5DischargeDate
	,Base.CCP5DischargeTime
	,Base.CCP5MaximumNumberOfOrganSupportSystem
	,Base.CCP6Number
	,Base.CCP6LocalIdentifier
	,Base.CCP6StartDate
	,Base.CCP6UnitFunction
	,Base.CCP6AdvancedRespiratorySupportDays
	,Base.CCP6BasicRespiratorySupportDays
	,Base.CCP6AdvancedCardiovascularSupportDays
	,Base.CCP6BasicCardiovascularSupportDays
	,Base.CCP6RenalSupportDays
	,Base.CCP6NeurologicalSupportDays
	,Base.CCP6DermatologicalSupportDays
	,Base.CCP6LiverSupportDays
	,Base.CCP6Level2Days
	,Base.CCP6Level3Days
	,Base.CCP6DischargeDate
	,Base.CCP6DischargeTime
	,Base.CCP6MaximumNumberOfOrganSupportSystem
	,Base.CCP7Number
	,Base.CCP7LocalIdentifier
	,Base.CCP7StartDate
	,Base.CCP7UnitFunction
	,Base.CCP7AdvancedRespiratorySupportDays
	,Base.CCP7BasicRespiratorySupportDays
	,Base.CCP7AdvancedCardiovascularSupportDays
	,Base.CCP7BasicCardiovascularSupportDays
	,Base.CCP7RenalSupportDays
	,Base.CCP7NeurologicalSupportDays
	,Base.CCP7DermatologicalSupportDays
	,Base.CCP7LiverSupportDays
	,Base.CCP7Level2Days
	,Base.CCP7Level3Days
	,Base.CCP7DischargeDate
	,Base.CCP7DischargeTime
	,Base.CCP7MaximumNumberOfOrganSupportSystem
	,Base.CCP8Number
	,Base.CCP8LocalIdentifier
	,Base.CCP8StartDate
	,Base.CCP8UnitFunction
	,Base.CCP8AdvancedRespiratorySupportDays
	,Base.CCP8BasicRespiratorySupportDays
	,Base.CCP8AdvancedCardiovascularSupportDays
	,Base.CCP8BasicCardiovascularSupportDays
	,Base.CCP8RenalSupportDays
	,Base.CCP8NeurologicalSupportDays
	,Base.CCP8DermatologicalSupportDays
	,Base.CCP8LiverSupportDays
	,Base.CCP8Level2Days
	,Base.CCP8Level3Days
	,Base.CCP8DischargeDate
	,Base.CCP8DischargeTime
	,Base.CCP8MaximumNumberOfOrganSupportSystem
	,Base.CCP9Number
	,Base.CCP9LocalIdentifier
	,Base.CCP9StartDate
	,Base.CCP9UnitFunction
	,Base.CCP9AdvancedRespiratorySupportDays
	,Base.CCP9BasicRespiratorySupportDays
	,Base.CCP9AdvancedCardiovascularSupportDays
	,Base.CCP9BasicCardiovascularSupportDays
	,Base.CCP9RenalSupportDays
	,Base.CCP9NeurologicalSupportDays
	,Base.CCP9DermatologicalSupportDays
	,Base.CCP9LiverSupportDays
	,Base.CCP9Level2Days
	,Base.CCP9Level3Days
	,Base.CCP9DischargeDate
	,Base.CCP9DischargeTime
	,Base.CCP9MaximumNumberOfOrganSupportSystem
	,Base.AgeAtCDSActivityDate
	,Base.AgeOnAdmission
	,Base.DischargeReadyDate
	,Base.RTTPathwayID
	,Base.RTTCurrentProviderCode
	,Base.RTTPeriodStatusCode
	,Base.RTTStartDate
	,Base.RTTEndDate
	,Base.EarliestReasonableOfferDate
	,Base.NCCPPresent
	,Base.Baby1NHSNumberStatus
	,Base.Baby1NHSNumber
	,Base.Baby2NHSNumberStatus
	,Base.Baby2NHSNumber
	,Base.Baby3NHSNumberStatus
	,Base.Baby3NHSNumber
	,Base.Baby4NHSNumberStatus
	,Base.Baby4NHSNumber
	,Base.Baby5NHSNumberStatus
	,Base.Baby5NHSNumber
	,Base.Baby6NHSNumberStatus
	,Base.Baby6NHSNumber
	,Base.EndOfRecord
from
	CDS.APCAngliaBase Base

inner join CDS.APCAngliaSubmitted Submitted
on	Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
and	not exists
	(
	select
		1
	from
		CDS.APCAngliaSubmitted Previous
	where
		Previous.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
	and	Previous.Created > Submitted.Created
	)
and	Submitted.UpdateType = '9' --insert/update

where
	not
	(
		isnull(Base.UniqueEpisodeSerialNo, '') = isnull(Submitted.UniqueEpisodeSerialNo, '')
	--and isnull(Base.CDSUpdateDate, '') = isnull(Submitted.CDSUpdateDate, '')
	--and isnull(Base.CDSUpdateTime, '') = isnull(Submitted.CDSUpdateTime, '')
	and isnull(Base.CDSType, '') = isnull(Submitted.CDSType, '')
	--and isnull(Base.RecordType, '') = isnull(Submitted.RecordType, '')
	and isnull(Base.ProviderCode, '') = isnull(Submitted.ProviderCode, '')
	and isnull(Base.PurchaserCode, '') = isnull(Submitted.PurchaserCode, '')
	and isnull(Base.CommissioningSerialNo, '') = isnull(Submitted.CommissioningSerialNo, '')
	and isnull(Base.NHSServiceAgreementLineNo, '') = isnull(Submitted.NHSServiceAgreementLineNo, '')
	and isnull(Base.PurchaserReferenceNo, '') = isnull(Submitted.PurchaserReferenceNo, '')
	and isnull(Base.NHSNumber, '') = isnull(Submitted.NHSNumber, '')
	and isnull(Base.PatientName, '') = isnull(Submitted.PatientName, '')
	and isnull(Base.PatientsAddress, '') = isnull(Submitted.PatientsAddress, '')
	and isnull(Base.Postcode, '') = isnull(Submitted.Postcode, '')
	and isnull(Base.SexCode, '') = isnull(Submitted.SexCode, '')
	and isnull(Base.CarerSupportIndicator, '') = isnull(Submitted.CarerSupportIndicator, '')
	and isnull(Base.DateOfBirth, '') = isnull(Submitted.DateOfBirth, '')
	and isnull(Base.RegisteredGpCode, '') = isnull(Submitted.RegisteredGpCode, '')
	and isnull(Base.RegisteredGpPracticeCode, '') = isnull(Submitted.RegisteredGpPracticeCode, '')
	and isnull(Base.DistrictNo, '') = isnull(Submitted.DistrictNo, '')
	and isnull(Base.EthnicGroupCode, '') = isnull(Submitted.EthnicGroupCode, '')

	--GC removed 13/04/2015 after talking with GS this should be looked at and put back in for next run

		--and isnull(Base.ReferrerCode, '') = isnull(Submitted.ReferrerCode, '')
		--and isnull(Base.ReferringOrganisationCode, '') = isnull(Submitted.ReferringOrganisationCode, '')

	and isnull(Base.ProviderSpellNo, '') = isnull(Submitted.ProviderSpellNo, '')
	and isnull(Base.PatientCategoryCode, '') = isnull(Submitted.PatientCategoryCode, '')
	and isnull(Base.DecidedToAdmitDate, '') = isnull(Submitted.DecidedToAdmitDate, '')
	and isnull(Base.AdmissionDate, '') = isnull(Submitted.AdmissionDate, '')
	and isnull(Base.AdmissionMethodCode, '') = isnull(Submitted.AdmissionMethodCode, '')
	and isnull(Base.DurationOfElectiveWait, '') = isnull(Submitted.DurationOfElectiveWait, '')
	and isnull(Base.ManagementIntentionCode, '') = isnull(Submitted.ManagementIntentionCode, '')
	and isnull(Base.SourceOfAdmissionCode, '') = isnull(Submitted.SourceOfAdmissionCode, '')
	and isnull(Base.DischargeDate, '') = isnull(Submitted.DischargeDate, '')
	and isnull(Base.DischargeMethodCode, '') = isnull(Submitted.DischargeMethodCode, '')
	and isnull(Base.DischargeDestinationCode, '') = isnull(Submitted.DischargeDestinationCode, '')
	and isnull(Base.PatientClassificationCode, '') = isnull(Submitted.PatientClassificationCode, '')
	and isnull(Base.EpisodeNo, '') = isnull(Submitted.EpisodeNo, '')
	and isnull(Base.LastEpisodeInSpellIndicator, '') = isnull(Submitted.LastEpisodeInSpellIndicator, '')
	and isnull(Base.StartSiteCode, '') = isnull(Submitted.StartSiteCode, '')
	and isnull(Base.StartWardTypeCode, '') = isnull(Submitted.StartWardTypeCode, '')
	and isnull(Base.EpisodeStartDate, '') = isnull(Submitted.EpisodeStartDate, '')
	and isnull(Base.EpisodeEndDate, '') = isnull(Submitted.EpisodeEndDate, '')
	and isnull(Base.MainSpecialtyCode, '') = isnull(Submitted.MainSpecialtyCode, '')
	and isnull(Base.TreatmentFunctionCode, '') = isnull(Submitted.TreatmentFunctionCode, '')
	and isnull(Base.ConsultantCode, '') = isnull(Submitted.ConsultantCode, '')
	and isnull(Base.FirstRegularDayNightAdmission, '') = isnull(Submitted.FirstRegularDayNightAdmission, '')
	and isnull(Base.NeonatalLevelOfCare, '') = isnull(Submitted.NeonatalLevelOfCare, '')
	and isnull(Base.PsychiatricPatientStatusCode, '') = isnull(Submitted.PsychiatricPatientStatusCode, '')
	and isnull(Base.PrimaryDiagnosisCode, '') = isnull(Submitted.PrimaryDiagnosisCode, '')
	and isnull(Base.SecondaryDiagnosisCode1, '') = isnull(Submitted.SecondaryDiagnosisCode1, '')
	and isnull(Base.SecondaryDiagnosisCode2, '') = isnull(Submitted.SecondaryDiagnosisCode2, '')
	and isnull(Base.SecondaryDiagnosisCode3, '') = isnull(Submitted.SecondaryDiagnosisCode3, '')
	and isnull(Base.SecondaryDiagnosisCode4, '') = isnull(Submitted.SecondaryDiagnosisCode4, '')
	and isnull(Base.SecondaryDiagnosisCode5, '') = isnull(Submitted.SecondaryDiagnosisCode5, '')
	and isnull(Base.SecondaryDiagnosisCode6, '') = isnull(Submitted.SecondaryDiagnosisCode6, '')
	and isnull(Base.SecondaryDiagnosisCode7, '') = isnull(Submitted.SecondaryDiagnosisCode7, '')
	and isnull(Base.SecondaryDiagnosisCode8, '') = isnull(Submitted.SecondaryDiagnosisCode8, '')
	and isnull(Base.SecondaryDiagnosisCode9, '') = isnull(Submitted.SecondaryDiagnosisCode9, '')
	and isnull(Base.SecondaryDiagnosisCode10, '') = isnull(Submitted.SecondaryDiagnosisCode10, '')
	and isnull(Base.SecondaryDiagnosisCode11, '') = isnull(Submitted.SecondaryDiagnosisCode11, '')
	and isnull(Base.SecondaryDiagnosisCode12, '') = isnull(Submitted.SecondaryDiagnosisCode12, '')
	and isnull(Base.OperationStatus, '') = isnull(Submitted.OperationStatus, '')
	and isnull(Base.PrimaryOperationCode, '') = isnull(Submitted.PrimaryOperationCode, '')
	and isnull(Base.PrimaryOperationDate, '') = isnull(Submitted.PrimaryOperationDate, '')
	and isnull(Base.OperationCode2, '') = isnull(Submitted.OperationCode2, '')
	and isnull(Base.OperationDate2, '') = isnull(Submitted.OperationDate2, '')
	and isnull(Base.OperationCode3, '') = isnull(Submitted.OperationCode3, '')
	and isnull(Base.OperationDate3, '') = isnull(Submitted.OperationDate3, '')
	and isnull(Base.OperationCode4, '') = isnull(Submitted.OperationCode4, '')
	and isnull(Base.OperationDate4, '') = isnull(Submitted.OperationDate4, '')
	and isnull(Base.OperationCode5, '') = isnull(Submitted.OperationCode5, '')
	and isnull(Base.OperationDate5, '') = isnull(Submitted.OperationDate5, '')
	and isnull(Base.OperationCode6, '') = isnull(Submitted.OperationCode6, '')
	and isnull(Base.OperationDate6, '') = isnull(Submitted.OperationDate6, '')
	and isnull(Base.OperationCode7, '') = isnull(Submitted.OperationCode7, '')
	and isnull(Base.OperationDate7, '') = isnull(Submitted.OperationDate7, '')
	and isnull(Base.OperationCode8, '') = isnull(Submitted.OperationCode8, '')
	and isnull(Base.OperationDate8, '') = isnull(Submitted.OperationDate8, '')
	and isnull(Base.OperationCode9, '') = isnull(Submitted.OperationCode9, '')
	and isnull(Base.OperationDate9, '') = isnull(Submitted.OperationDate9, '')
	and isnull(Base.OperationCode10, '') = isnull(Submitted.OperationCode10, '')
	and isnull(Base.OperationDate10, '') = isnull(Submitted.OperationDate10, '')
	and isnull(Base.OperationCode11, '') = isnull(Submitted.OperationCode11, '')
	and isnull(Base.OperationDate11, '') = isnull(Submitted.OperationDate11, '')
	and isnull(Base.OperationCode12, '') = isnull(Submitted.OperationCode12, '')
	and isnull(Base.OperationDate12, '') = isnull(Submitted.OperationDate12, '')
	and isnull(Base.EpisodeStartWardCode, '') = isnull(Submitted.EpisodeStartWardCode, '')
	and isnull(Base.EndWardTypeCode, '') = isnull(Submitted.EndWardTypeCode, '')
	and isnull(Base.EndSiteCode, '') = isnull(Submitted.EndSiteCode, '')
	and isnull(Base.AnteNatalGpCode, '') = isnull(Submitted.AnteNatalGpCode, '')
	and isnull(Base.FirstAnteNatalAssessmentDate, '') = isnull(Submitted.FirstAnteNatalAssessmentDate, '')
	and isnull(Base.PreviousPregnancies, '') = isnull(Submitted.PreviousPregnancies, '')
	and isnull(Base.ActualDeliveryPlaceTypeCode, '') = isnull(Submitted.ActualDeliveryPlaceTypeCode, '')
	and isnull(Base.IntendedDeliveryPlaceTypeCode, '') = isnull(Submitted.IntendedDeliveryPlaceTypeCode, '')
	and isnull(Base.DeliveryPlaceChangeReasonCode, '') = isnull(Submitted.DeliveryPlaceChangeReasonCode, '')
	and isnull(Base.GestationLength, '') = isnull(Submitted.GestationLength, '')
	and isnull(Base.LabourOnsetMethodCode, '') = isnull(Submitted.LabourOnsetMethodCode, '')
	and isnull(Base.StatusOfPersonConductingDeliveryCode, '') = isnull(Submitted.StatusOfPersonConductingDeliveryCode, '')
	and isnull(Base.AnaestheticGivenDuringCode, '') = isnull(Submitted.AnaestheticGivenDuringCode, '')
	and isnull(Base.AnaestheticGivenPostCode, '') = isnull(Submitted.AnaestheticGivenPostCode, '')
	and isnull(Base.NumberOfBabies, '') = isnull(Submitted.NumberOfBabies, '')
	and isnull(Base.Baby1DeliveryDate, '') = isnull(Submitted.Baby1DeliveryDate, '')
	and isnull(Base.Baby1DeliveryMethodCode, '') = isnull(Submitted.Baby1DeliveryMethodCode, '')
	and isnull(Base.Baby1SexCode, '') = isnull(Submitted.Baby1SexCode, '')
	and isnull(Base.Baby1BirthOrder, '') = isnull(Submitted.Baby1BirthOrder, '')
	and isnull(Base.Baby1LiveStillBirth, '') = isnull(Submitted.Baby1LiveStillBirth, '')
	and isnull(Base.Baby1BirthWeight, '') = isnull(Submitted.Baby1BirthWeight, '')
	and isnull(Base.Baby1MethodOfResuscitationCode, '') = isnull(Submitted.Baby1MethodOfResuscitationCode, '')
	and isnull(Base.Baby1BirthDate, '') = isnull(Submitted.Baby1BirthDate, '')
	and isnull(Base.Baby1MotherBirthDate, '') = isnull(Submitted.Baby1MotherBirthDate, '')
	and isnull(Base.Baby2DeliveryDate, '') = isnull(Submitted.Baby2DeliveryDate, '')
	and isnull(Base.Baby2DeliveryMethodCode, '') = isnull(Submitted.Baby2DeliveryMethodCode, '')
	and isnull(Base.Baby2SexCode, '') = isnull(Submitted.Baby2SexCode, '')
	and isnull(Base.Baby2BirthOrder, '') = isnull(Submitted.Baby2BirthOrder, '')
	and isnull(Base.Baby2LiveStillBirth, '') = isnull(Submitted.Baby2LiveStillBirth, '')
	and isnull(Base.Baby2BirthWeight, '') = isnull(Submitted.Baby2BirthWeight, '')
	and isnull(Base.Baby2MethodOfResuscitationCode, '') = isnull(Submitted.Baby2MethodOfResuscitationCode, '')
	and isnull(Base.Baby2BirthDate, '') = isnull(Submitted.Baby2BirthDate, '')
	and isnull(Base.Baby2MotherBirthDate, '') = isnull(Submitted.Baby2MotherBirthDate, '')
	and isnull(Base.Baby3DeliveryDate, '') = isnull(Submitted.Baby3DeliveryDate, '')
	and isnull(Base.Baby3DeliveryMethodCode, '') = isnull(Submitted.Baby3DeliveryMethodCode, '')
	and isnull(Base.Baby3SexCode, '') = isnull(Submitted.Baby3SexCode, '')
	and isnull(Base.Baby3BirthOrder, '') = isnull(Submitted.Baby3BirthOrder, '')
	and isnull(Base.Baby3LiveStillBirth, '') = isnull(Submitted.Baby3LiveStillBirth, '')
	and isnull(Base.Baby3BirthWeight, '') = isnull(Submitted.Baby3BirthWeight, '')
	and isnull(Base.Baby3MethodOfResuscitationCode, '') = isnull(Submitted.Baby3MethodOfResuscitationCode, '')
	and isnull(Base.Baby3BirthDate, '') = isnull(Submitted.Baby3BirthDate, '')
	and isnull(Base.Baby3MotherBirthDate, '') = isnull(Submitted.Baby3MotherBirthDate, '')
	and isnull(Base.Baby4DeliveryDate, '') = isnull(Submitted.Baby4DeliveryDate, '')
	and isnull(Base.Baby4DeliveryMethodCode, '') = isnull(Submitted.Baby4DeliveryMethodCode, '')
	and isnull(Base.Baby4SexCode, '') = isnull(Submitted.Baby4SexCode, '')
	and isnull(Base.Baby4BirthOrder, '') = isnull(Submitted.Baby4BirthOrder, '')
	and isnull(Base.Baby4LiveStillBirth, '') = isnull(Submitted.Baby4LiveStillBirth, '')
	and isnull(Base.Baby4BirthWeight, '') = isnull(Submitted.Baby4BirthWeight, '')
	and isnull(Base.Baby4MethodOfResuscitationCode, '') = isnull(Submitted.Baby4MethodOfResuscitationCode, '')
	and isnull(Base.Baby4BirthDate, '') = isnull(Submitted.Baby4BirthDate, '')
	and isnull(Base.Baby4MotherBirthDate, '') = isnull(Submitted.Baby4MotherBirthDate, '')
	and isnull(Base.Baby5DeliveryDate, '') = isnull(Submitted.Baby5DeliveryDate, '')
	and isnull(Base.Baby5DeliveryMethodCode, '') = isnull(Submitted.Baby5DeliveryMethodCode, '')
	and isnull(Base.Baby5SexCode, '') = isnull(Submitted.Baby5SexCode, '')
	and isnull(Base.Baby5BirthOrder, '') = isnull(Submitted.Baby5BirthOrder, '')
	and isnull(Base.Baby5LiveStillBirth, '') = isnull(Submitted.Baby5LiveStillBirth, '')
	and isnull(Base.Baby5BirthWeight, '') = isnull(Submitted.Baby5BirthWeight, '')
	and isnull(Base.Baby5MethodOfResuscitationCode, '') = isnull(Submitted.Baby5MethodOfResuscitationCode, '')
	and isnull(Base.Baby5BirthDate, '') = isnull(Submitted.Baby5BirthDate, '')
	and isnull(Base.Baby5MotherBirthDate, '') = isnull(Submitted.Baby5MotherBirthDate, '')
	and isnull(Base.Baby6DeliveryDate, '') = isnull(Submitted.Baby6DeliveryDate, '')
	and isnull(Base.Baby6DeliveryMethodCode, '') = isnull(Submitted.Baby6DeliveryMethodCode, '')
	and isnull(Base.Baby6SexCode, '') = isnull(Submitted.Baby6SexCode, '')
	and isnull(Base.Baby6BirthOrder, '') = isnull(Submitted.Baby6BirthOrder, '')
	and isnull(Base.Baby6LiveStillBirth, '') = isnull(Submitted.Baby6LiveStillBirth, '')
	and isnull(Base.Baby6BirthWeight, '') = isnull(Submitted.Baby6BirthWeight, '')
	and isnull(Base.Baby6MethodOfResuscitationCode, '') = isnull(Submitted.Baby6MethodOfResuscitationCode, '')
	and isnull(Base.Baby6BirthDate, '') = isnull(Submitted.Baby6BirthDate, '')
	and isnull(Base.Baby6MotherBirthDate, '') = isnull(Submitted.Baby6MotherBirthDate, '')
	and isnull(Base.NumberOfWardTransfers, '') = isnull(Submitted.NumberOfWardTransfers, '')
	and isnull(Base.WardStay1Sequence, '') = isnull(Submitted.WardStay1Sequence, '')
	and isnull(Base.WardStay1TransferDate, '') = isnull(Submitted.WardStay1TransferDate, '')
	and isnull(Base.WardStay1TransferTime, '') = isnull(Submitted.WardStay1TransferTime, '')
	and isnull(Base.WardStay1HospitalCode, '') = isnull(Submitted.WardStay1HospitalCode, '')
	and isnull(Base.WardStay1WardCode, '') = isnull(Submitted.WardStay1WardCode, '')
	and isnull(Base.WardStay2Sequence, '') = isnull(Submitted.WardStay2Sequence, '')
	and isnull(Base.WardStay2TransferDate, '') = isnull(Submitted.WardStay2TransferDate, '')
	and isnull(Base.WardStay2TransferTime, '') = isnull(Submitted.WardStay2TransferTime, '')
	and isnull(Base.WardStay2HospitalCode, '') = isnull(Submitted.WardStay2HospitalCode, '')
	and isnull(Base.WardStay2WardCode, '') = isnull(Submitted.WardStay2WardCode, '')
	and isnull(Base.WardStay3Sequence, '') = isnull(Submitted.WardStay3Sequence, '')
	and isnull(Base.WardStay3TransferDate, '') = isnull(Submitted.WardStay3TransferDate, '')
	and isnull(Base.WardStay3TransferTime, '') = isnull(Submitted.WardStay3TransferTime, '')
	and isnull(Base.WardStay3HospitalCode, '') = isnull(Submitted.WardStay3HospitalCode, '')
	and isnull(Base.WardStay3WardCode, '') = isnull(Submitted.WardStay3WardCode, '')
	and isnull(Base.WardStay4Sequence, '') = isnull(Submitted.WardStay4Sequence, '')
	and isnull(Base.WardStay4TransferDate, '') = isnull(Submitted.WardStay4TransferDate, '')
	and isnull(Base.WardStay4TransferTime, '') = isnull(Submitted.WardStay4TransferTime, '')
	and isnull(Base.WardStay4HospitalCode, '') = isnull(Submitted.WardStay4HospitalCode, '')
	and isnull(Base.WardStay4WardCode, '') = isnull(Submitted.WardStay4WardCode, '')
	and isnull(Base.WardStay5Sequence, '') = isnull(Submitted.WardStay5Sequence, '')
	and isnull(Base.WardStay5TransferDate, '') = isnull(Submitted.WardStay5TransferDate, '')
	and isnull(Base.WardStay5TransferTime, '') = isnull(Submitted.WardStay5TransferTime, '')
	and isnull(Base.WardStay5HospitalCode, '') = isnull(Submitted.WardStay5HospitalCode, '')
	and isnull(Base.WardStay5WardCode, '') = isnull(Submitted.WardStay5WardCode, '')
	and isnull(Base.WardStay6Sequence, '') = isnull(Submitted.WardStay6Sequence, '')
	and isnull(Base.WardStay6TransferDate, '') = isnull(Submitted.WardStay6TransferDate, '')
	and isnull(Base.WardStay6TransferTime, '') = isnull(Submitted.WardStay6TransferTime, '')
	and isnull(Base.WardStay6HospitalCode, '') = isnull(Submitted.WardStay6HospitalCode, '')
	and isnull(Base.WardStay6WardCode, '') = isnull(Submitted.WardStay6WardCode, '')
	and isnull(Base.WardStay7Sequence, '') = isnull(Submitted.WardStay7Sequence, '')
	and isnull(Base.WardStay7TransferDate, '') = isnull(Submitted.WardStay7TransferDate, '')
	and isnull(Base.WardStay7TransferTime, '') = isnull(Submitted.WardStay7TransferTime, '')
	and isnull(Base.WardStay7HospitalCode, '') = isnull(Submitted.WardStay7HospitalCode, '')
	and isnull(Base.WardStay7WardCode, '') = isnull(Submitted.WardStay7WardCode, '')
	and isnull(Base.WardStay8Sequence, '') = isnull(Submitted.WardStay8Sequence, '')
	and isnull(Base.WardStay8TransferDate, '') = isnull(Submitted.WardStay8TransferDate, '')
	and isnull(Base.WardStay8TransferTime, '') = isnull(Submitted.WardStay8TransferTime, '')
	and isnull(Base.WardStay8HospitalCode, '') = isnull(Submitted.WardStay8HospitalCode, '')
	and isnull(Base.WardStay8WardCode, '') = isnull(Submitted.WardStay8WardCode, '')
	and isnull(Base.WardStay9Sequence, '') = isnull(Submitted.WardStay9Sequence, '')
	and isnull(Base.WardStay9TransferDate, '') = isnull(Submitted.WardStay9TransferDate, '')
	and isnull(Base.WardStay9TransferTime, '') = isnull(Submitted.WardStay9TransferTime, '')
	and isnull(Base.WardStay9HospitalCode, '') = isnull(Submitted.WardStay9HospitalCode, '')
	and isnull(Base.WardStay9WardCode, '') = isnull(Submitted.WardStay9WardCode, '')
	and isnull(Base.WardStay10Sequence, '') = isnull(Submitted.WardStay10Sequence, '')
	and isnull(Base.WardStay10TransferDate, '') = isnull(Submitted.WardStay10TransferDate, '')
	and isnull(Base.WardStay10TransferTime, '') = isnull(Submitted.WardStay10TransferTime, '')
	and isnull(Base.WardStay10HospitalCode, '') = isnull(Submitted.WardStay10HospitalCode, '')
	and isnull(Base.WardStay10WardCode, '') = isnull(Submitted.WardStay10WardCode, '')
	and isnull(Base.WardStay11Sequence, '') = isnull(Submitted.WardStay11Sequence, '')
	and isnull(Base.WardStay11TransferDate, '') = isnull(Submitted.WardStay11TransferDate, '')
	and isnull(Base.WardStay11TransferTime, '') = isnull(Submitted.WardStay11TransferTime, '')
	and isnull(Base.WardStay11HospitalCode, '') = isnull(Submitted.WardStay11HospitalCode, '')
	and isnull(Base.WardStay11WardCode, '') = isnull(Submitted.WardStay11WardCode, '')
	and isnull(Base.WardStay12Sequence, '') = isnull(Submitted.WardStay12Sequence, '')
	and isnull(Base.WardStay12TransferDate, '') = isnull(Submitted.WardStay12TransferDate, '')
	and isnull(Base.WardStay12TransferTime, '') = isnull(Submitted.WardStay12TransferTime, '')
	and isnull(Base.WardStay12HospitalCode, '') = isnull(Submitted.WardStay12HospitalCode, '')
	and isnull(Base.WardStay12WardCode, '') = isnull(Submitted.WardStay12WardCode, '')
	and isnull(Base.WardStay13Sequence, '') = isnull(Submitted.WardStay13Sequence, '')
	and isnull(Base.WardStay13TransferDate, '') = isnull(Submitted.WardStay13TransferDate, '')
	and isnull(Base.WardStay13TransferTime, '') = isnull(Submitted.WardStay13TransferTime, '')
	and isnull(Base.WardStay13HospitalCode, '') = isnull(Submitted.WardStay13HospitalCode, '')
	and isnull(Base.WardStay13WardCode, '') = isnull(Submitted.WardStay13WardCode, '')
	and isnull(Base.WardStay14Sequence, '') = isnull(Submitted.WardStay14Sequence, '')
	and isnull(Base.WardStay14TransferDate, '') = isnull(Submitted.WardStay14TransferDate, '')
	and isnull(Base.WardStay14TransferTime, '') = isnull(Submitted.WardStay14TransferTime, '')
	and isnull(Base.WardStay14HospitalCode, '') = isnull(Submitted.WardStay14HospitalCode, '')
	and isnull(Base.WardStay14WardCode, '') = isnull(Submitted.WardStay14WardCode, '')
	and isnull(Base.WardStay15Sequence, '') = isnull(Submitted.WardStay15Sequence, '')
	and isnull(Base.WardStay15TransferDate, '') = isnull(Submitted.WardStay15TransferDate, '')
	and isnull(Base.WardStay15TransferTime, '') = isnull(Submitted.WardStay15TransferTime, '')
	and isnull(Base.WardStay15HospitalCode, '') = isnull(Submitted.WardStay15HospitalCode, '')
	and isnull(Base.WardStay15WardCode, '') = isnull(Submitted.WardStay15WardCode, '')
	and isnull(Base.WardStay16Sequence, '') = isnull(Submitted.WardStay16Sequence, '')
	and isnull(Base.WardStay16TransferDate, '') = isnull(Submitted.WardStay16TransferDate, '')
	and isnull(Base.WardStay16TransferTime, '') = isnull(Submitted.WardStay16TransferTime, '')
	and isnull(Base.WardStay16HospitalCode, '') = isnull(Submitted.WardStay16HospitalCode, '')
	and isnull(Base.WardStay16WardCode, '') = isnull(Submitted.WardStay16WardCode, '')
	and isnull(Base.WardStay17Sequence, '') = isnull(Submitted.WardStay17Sequence, '')
	and isnull(Base.WardStay17TransferDate, '') = isnull(Submitted.WardStay17TransferDate, '')
	and isnull(Base.WardStay17TransferTime, '') = isnull(Submitted.WardStay17TransferTime, '')
	and isnull(Base.WardStay17HospitalCode, '') = isnull(Submitted.WardStay17HospitalCode, '')
	and isnull(Base.WardStay17WardCode, '') = isnull(Submitted.WardStay17WardCode, '')
	and isnull(Base.WardStay18Sequence, '') = isnull(Submitted.WardStay18Sequence, '')
	and isnull(Base.WardStay18TransferDate, '') = isnull(Submitted.WardStay18TransferDate, '')
	and isnull(Base.WardStay18TransferTime, '') = isnull(Submitted.WardStay18TransferTime, '')
	and isnull(Base.WardStay18HospitalCode, '') = isnull(Submitted.WardStay18HospitalCode, '')
	and isnull(Base.WardStay18WardCode, '') = isnull(Submitted.WardStay18WardCode, '')
	and isnull(Base.WardStay19Sequence, '') = isnull(Submitted.WardStay19Sequence, '')
	and isnull(Base.WardStay19TransferDate, '') = isnull(Submitted.WardStay19TransferDate, '')
	and isnull(Base.WardStay19TransferTime, '') = isnull(Submitted.WardStay19TransferTime, '')
	and isnull(Base.WardStay19HospitalCode, '') = isnull(Submitted.WardStay19HospitalCode, '')
	and isnull(Base.WardStay19WardCode, '') = isnull(Submitted.WardStay19WardCode, '')
	and isnull(Base.WardStay20Sequence, '') = isnull(Submitted.WardStay20Sequence, '')
	and isnull(Base.WardStay20TransferDate, '') = isnull(Submitted.WardStay20TransferDate, '')
	and isnull(Base.WardStay20TransferTime, '') = isnull(Submitted.WardStay20TransferTime, '')
	and isnull(Base.WardStay20HospitalCode, '') = isnull(Submitted.WardStay20HospitalCode, '')
	and isnull(Base.WardStay20WardCode, '') = isnull(Submitted.WardStay20WardCode, '')
	and isnull(Base.WardStay21Sequence, '') = isnull(Submitted.WardStay21Sequence, '')
	and isnull(Base.WardStay21TransferDate, '') = isnull(Submitted.WardStay21TransferDate, '')
	and isnull(Base.WardStay21TransferTime, '') = isnull(Submitted.WardStay21TransferTime, '')
	and isnull(Base.WardStay21HospitalCode, '') = isnull(Submitted.WardStay21HospitalCode, '')
	and isnull(Base.WardStay21WardCode, '') = isnull(Submitted.WardStay21WardCode, '')
	and isnull(Base.WardStay22Sequence, '') = isnull(Submitted.WardStay22Sequence, '')
	and isnull(Base.WardStay22TransferDate, '') = isnull(Submitted.WardStay22TransferDate, '')
	and isnull(Base.WardStay22TransferTime, '') = isnull(Submitted.WardStay22TransferTime, '')
	and isnull(Base.WardStay22HospitalCode, '') = isnull(Submitted.WardStay22HospitalCode, '')
	and isnull(Base.WardStay22WardCode, '') = isnull(Submitted.WardStay22WardCode, '')
	and isnull(Base.WardStay23Sequence, '') = isnull(Submitted.WardStay23Sequence, '')
	and isnull(Base.WardStay23TransferDate, '') = isnull(Submitted.WardStay23TransferDate, '')
	and isnull(Base.WardStay23TransferTime, '') = isnull(Submitted.WardStay23TransferTime, '')
	and isnull(Base.WardStay23HospitalCode, '') = isnull(Submitted.WardStay23HospitalCode, '')
	and isnull(Base.WardStay23WardCode, '') = isnull(Submitted.WardStay23WardCode, '')
	and isnull(Base.WardStay24Sequence, '') = isnull(Submitted.WardStay24Sequence, '')
	and isnull(Base.WardStay24TransferDate, '') = isnull(Submitted.WardStay24TransferDate, '')
	and isnull(Base.WardStay24TransferTime, '') = isnull(Submitted.WardStay24TransferTime, '')
	and isnull(Base.WardStay24HospitalCode, '') = isnull(Submitted.WardStay24HospitalCode, '')
	and isnull(Base.WardStay24WardCode, '') = isnull(Submitted.WardStay24WardCode, '')
	and isnull(Base.WardStay25Sequence, '') = isnull(Submitted.WardStay25Sequence, '')
	and isnull(Base.WardStay25TransferDate, '') = isnull(Submitted.WardStay25TransferDate, '')
	and isnull(Base.WardStay25TransferTime, '') = isnull(Submitted.WardStay25TransferTime, '')
	and isnull(Base.WardStay25HospitalCode, '') = isnull(Submitted.WardStay25HospitalCode, '')
	and isnull(Base.WardStay25WardCode, '') = isnull(Submitted.WardStay25WardCode, '')
	and isnull(Base.WardStay26Sequence, '') = isnull(Submitted.WardStay26Sequence, '')
	and isnull(Base.WardStay26TransferDate, '') = isnull(Submitted.WardStay26TransferDate, '')
	and isnull(Base.WardStay26TransferTime, '') = isnull(Submitted.WardStay26TransferTime, '')
	and isnull(Base.WardStay26HospitalCode, '') = isnull(Submitted.WardStay26HospitalCode, '')
	and isnull(Base.WardStay26WardCode, '') = isnull(Submitted.WardStay26WardCode, '')
	and isnull(Base.WardStay27Sequence, '') = isnull(Submitted.WardStay27Sequence, '')
	and isnull(Base.WardStay27TransferDate, '') = isnull(Submitted.WardStay27TransferDate, '')
	and isnull(Base.WardStay27TransferTime, '') = isnull(Submitted.WardStay27TransferTime, '')
	and isnull(Base.WardStay27HospitalCode, '') = isnull(Submitted.WardStay27HospitalCode, '')
	and isnull(Base.WardStay27WardCode, '') = isnull(Submitted.WardStay27WardCode, '')
	and isnull(Base.WardStay28Sequence, '') = isnull(Submitted.WardStay28Sequence, '')
	and isnull(Base.WardStay28TransferDate, '') = isnull(Submitted.WardStay28TransferDate, '')
	and isnull(Base.WardStay28TransferTime, '') = isnull(Submitted.WardStay28TransferTime, '')
	and isnull(Base.WardStay28HospitalCode, '') = isnull(Submitted.WardStay28HospitalCode, '')
	and isnull(Base.WardStay28WardCode, '') = isnull(Submitted.WardStay28WardCode, '')
	and isnull(Base.WardStay29Sequence, '') = isnull(Submitted.WardStay29Sequence, '')
	and isnull(Base.WardStay29TransferDate, '') = isnull(Submitted.WardStay29TransferDate, '')
	and isnull(Base.WardStay29TransferTime, '') = isnull(Submitted.WardStay29TransferTime, '')
	and isnull(Base.WardStay29HospitalCode, '') = isnull(Submitted.WardStay29HospitalCode, '')
	and isnull(Base.WardStay29WardCode, '') = isnull(Submitted.WardStay29WardCode, '')
	and isnull(Base.WardStay30Sequence, '') = isnull(Submitted.WardStay30Sequence, '')
	and isnull(Base.WardStay30TransferDate, '') = isnull(Submitted.WardStay30TransferDate, '')
	and isnull(Base.WardStay30TransferTime, '') = isnull(Submitted.WardStay30TransferTime, '')
	and isnull(Base.WardStay30HospitalCode, '') = isnull(Submitted.WardStay30HospitalCode, '')
	and isnull(Base.WardStay30WardCode, '') = isnull(Submitted.WardStay30WardCode, '')
	and isnull(Base.WardStay31Sequence, '') = isnull(Submitted.WardStay31Sequence, '')
	and isnull(Base.WardStay31TransferDate, '') = isnull(Submitted.WardStay31TransferDate, '')
	and isnull(Base.WardStay31TransferTime, '') = isnull(Submitted.WardStay31TransferTime, '')
	and isnull(Base.WardStay31HospitalCode, '') = isnull(Submitted.WardStay31HospitalCode, '')
	and isnull(Base.WardStay31WardCode, '') = isnull(Submitted.WardStay31WardCode, '')
	and isnull(Base.WardStay32Sequence, '') = isnull(Submitted.WardStay32Sequence, '')
	and isnull(Base.WardStay32TransferDate, '') = isnull(Submitted.WardStay32TransferDate, '')
	and isnull(Base.WardStay32TransferTime, '') = isnull(Submitted.WardStay32TransferTime, '')
	and isnull(Base.WardStay32HospitalCode, '') = isnull(Submitted.WardStay32HospitalCode, '')
	and isnull(Base.WardStay32WardCode, '') = isnull(Submitted.WardStay32WardCode, '')
	and isnull(Base.WardStay33Sequence, '') = isnull(Submitted.WardStay33Sequence, '')
	and isnull(Base.WardStay33TransferDate, '') = isnull(Submitted.WardStay33TransferDate, '')
	and isnull(Base.WardStay33TransferTime, '') = isnull(Submitted.WardStay33TransferTime, '')
	and isnull(Base.WardStay33HospitalCode, '') = isnull(Submitted.WardStay33HospitalCode, '')
	and isnull(Base.WardStay33WardCode, '') = isnull(Submitted.WardStay33WardCode, '')
	and isnull(Base.WardStay34Sequence, '') = isnull(Submitted.WardStay34Sequence, '')
	and isnull(Base.WardStay34TransferDate, '') = isnull(Submitted.WardStay34TransferDate, '')
	and isnull(Base.WardStay34TransferTime, '') = isnull(Submitted.WardStay34TransferTime, '')
	and isnull(Base.WardStay34HospitalCode, '') = isnull(Submitted.WardStay34HospitalCode, '')
	and isnull(Base.WardStay34WardCode, '') = isnull(Submitted.WardStay34WardCode, '')
	and isnull(Base.WardStay35Sequence, '') = isnull(Submitted.WardStay35Sequence, '')
	and isnull(Base.WardStay35TransferDate, '') = isnull(Submitted.WardStay35TransferDate, '')
	and isnull(Base.WardStay35TransferTime, '') = isnull(Submitted.WardStay35TransferTime, '')
	and isnull(Base.WardStay35HospitalCode, '') = isnull(Submitted.WardStay35HospitalCode, '')
	and isnull(Base.WardStay35WardCode, '') = isnull(Submitted.WardStay35WardCode, '')
	and isnull(Base.WardStay36Sequence, '') = isnull(Submitted.WardStay36Sequence, '')
	and isnull(Base.WardStay36TransferDate, '') = isnull(Submitted.WardStay36TransferDate, '')
	and isnull(Base.WardStay36TransferTime, '') = isnull(Submitted.WardStay36TransferTime, '')
	and isnull(Base.WardStay36HospitalCode, '') = isnull(Submitted.WardStay36HospitalCode, '')
	and isnull(Base.WardStay36WardCode, '') = isnull(Submitted.WardStay36WardCode, '')
	and isnull(Base.WardStay37Sequence, '') = isnull(Submitted.WardStay37Sequence, '')
	and isnull(Base.WardStay37TransferDate, '') = isnull(Submitted.WardStay37TransferDate, '')
	and isnull(Base.WardStay37TransferTime, '') = isnull(Submitted.WardStay37TransferTime, '')
	and isnull(Base.WardStay37HospitalCode, '') = isnull(Submitted.WardStay37HospitalCode, '')
	and isnull(Base.WardStay37WardCode, '') = isnull(Submitted.WardStay37WardCode, '')
	and isnull(Base.WardStay38Sequence, '') = isnull(Submitted.WardStay38Sequence, '')
	and isnull(Base.WardStay38TransferDate, '') = isnull(Submitted.WardStay38TransferDate, '')
	and isnull(Base.WardStay38TransferTime, '') = isnull(Submitted.WardStay38TransferTime, '')
	and isnull(Base.WardStay38HospitalCode, '') = isnull(Submitted.WardStay38HospitalCode, '')
	and isnull(Base.WardStay38WardCode, '') = isnull(Submitted.WardStay38WardCode, '')
	and isnull(Base.WardStay39Sequence, '') = isnull(Submitted.WardStay39Sequence, '')
	and isnull(Base.WardStay39TransferDate, '') = isnull(Submitted.WardStay39TransferDate, '')
	and isnull(Base.WardStay39TransferTime, '') = isnull(Submitted.WardStay39TransferTime, '')
	and isnull(Base.WardStay39HospitalCode, '') = isnull(Submitted.WardStay39HospitalCode, '')
	and isnull(Base.WardStay39WardCode, '') = isnull(Submitted.WardStay39WardCode, '')
	and isnull(Base.WardStay40Sequence, '') = isnull(Submitted.WardStay40Sequence, '')
	and isnull(Base.WardStay40TransferDate, '') = isnull(Submitted.WardStay40TransferDate, '')
	and isnull(Base.WardStay40TransferTime, '') = isnull(Submitted.WardStay40TransferTime, '')
	and isnull(Base.WardStay40HospitalCode, '') = isnull(Submitted.WardStay40HospitalCode, '')
	and isnull(Base.WardStay40WardCode, '') = isnull(Submitted.WardStay40WardCode, '')
	and isnull(Base.WardStay41Sequence, '') = isnull(Submitted.WardStay41Sequence, '')
	and isnull(Base.WardStay41TransferDate, '') = isnull(Submitted.WardStay41TransferDate, '')
	and isnull(Base.WardStay41TransferTime, '') = isnull(Submitted.WardStay41TransferTime, '')
	and isnull(Base.WardStay41HospitalCode, '') = isnull(Submitted.WardStay41HospitalCode, '')
	and isnull(Base.WardStay41WardCode, '') = isnull(Submitted.WardStay41WardCode, '')
	and isnull(Base.WardStay42Sequence, '') = isnull(Submitted.WardStay42Sequence, '')
	and isnull(Base.WardStay42TransferDate, '') = isnull(Submitted.WardStay42TransferDate, '')
	and isnull(Base.WardStay42TransferTime, '') = isnull(Submitted.WardStay42TransferTime, '')
	and isnull(Base.WardStay42HospitalCode, '') = isnull(Submitted.WardStay42HospitalCode, '')
	and isnull(Base.WardStay42WardCode, '') = isnull(Submitted.WardStay42WardCode, '')
	and isnull(Base.WardStay43Sequence, '') = isnull(Submitted.WardStay43Sequence, '')
	and isnull(Base.WardStay43TransferDate, '') = isnull(Submitted.WardStay43TransferDate, '')
	and isnull(Base.WardStay43TransferTime, '') = isnull(Submitted.WardStay43TransferTime, '')
	and isnull(Base.WardStay43HospitalCode, '') = isnull(Submitted.WardStay43HospitalCode, '')
	and isnull(Base.WardStay43WardCode, '') = isnull(Submitted.WardStay43WardCode, '')
	and isnull(Base.WardStay44Sequence, '') = isnull(Submitted.WardStay44Sequence, '')
	and isnull(Base.WardStay44TransferDate, '') = isnull(Submitted.WardStay44TransferDate, '')
	and isnull(Base.WardStay44TransferTime, '') = isnull(Submitted.WardStay44TransferTime, '')
	and isnull(Base.WardStay44HospitalCode, '') = isnull(Submitted.WardStay44HospitalCode, '')
	and isnull(Base.WardStay44WardCode, '') = isnull(Submitted.WardStay44WardCode, '')
	and isnull(Base.WardStay45Sequence, '') = isnull(Submitted.WardStay45Sequence, '')
	and isnull(Base.WardStay45TransferDate, '') = isnull(Submitted.WardStay45TransferDate, '')
	and isnull(Base.WardStay45TransferTime, '') = isnull(Submitted.WardStay45TransferTime, '')
	and isnull(Base.WardStay45HospitalCode, '') = isnull(Submitted.WardStay45HospitalCode, '')
	and isnull(Base.WardStay45WardCode, '') = isnull(Submitted.WardStay45WardCode, '')
	and isnull(Base.WardStay46Sequence, '') = isnull(Submitted.WardStay46Sequence, '')
	and isnull(Base.WardStay46TransferDate, '') = isnull(Submitted.WardStay46TransferDate, '')
	and isnull(Base.WardStay46TransferTime, '') = isnull(Submitted.WardStay46TransferTime, '')
	and isnull(Base.WardStay46HospitalCode, '') = isnull(Submitted.WardStay46HospitalCode, '')
	and isnull(Base.WardStay46WardCode, '') = isnull(Submitted.WardStay46WardCode, '')
	and isnull(Base.WardStay47Sequence, '') = isnull(Submitted.WardStay47Sequence, '')
	and isnull(Base.WardStay47TransferDate, '') = isnull(Submitted.WardStay47TransferDate, '')
	and isnull(Base.WardStay47TransferTime, '') = isnull(Submitted.WardStay47TransferTime, '')
	and isnull(Base.WardStay47HospitalCode, '') = isnull(Submitted.WardStay47HospitalCode, '')
	and isnull(Base.WardStay47WardCode, '') = isnull(Submitted.WardStay47WardCode, '')
	and isnull(Base.WardStay48Sequence, '') = isnull(Submitted.WardStay48Sequence, '')
	and isnull(Base.WardStay48TransferDate, '') = isnull(Submitted.WardStay48TransferDate, '')
	and isnull(Base.WardStay48TransferTime, '') = isnull(Submitted.WardStay48TransferTime, '')
	and isnull(Base.WardStay48HospitalCode, '') = isnull(Submitted.WardStay48HospitalCode, '')
	and isnull(Base.WardStay48WardCode, '') = isnull(Submitted.WardStay48WardCode, '')
	and isnull(Base.WardStay49Sequence, '') = isnull(Submitted.WardStay49Sequence, '')
	and isnull(Base.WardStay49TransferDate, '') = isnull(Submitted.WardStay49TransferDate, '')
	and isnull(Base.WardStay49TransferTime, '') = isnull(Submitted.WardStay49TransferTime, '')
	and isnull(Base.WardStay49HospitalCode, '') = isnull(Submitted.WardStay49HospitalCode, '')
	and isnull(Base.WardStay49WardCode, '') = isnull(Submitted.WardStay49WardCode, '')
	and isnull(Base.WardStay50Sequence, '') = isnull(Submitted.WardStay50Sequence, '')
	and isnull(Base.WardStay50TransferDate, '') = isnull(Submitted.WardStay50TransferDate, '')
	and isnull(Base.WardStay50TransferTime, '') = isnull(Submitted.WardStay50TransferTime, '')
	and isnull(Base.WardStay50HospitalCode, '') = isnull(Submitted.WardStay50HospitalCode, '')
	and isnull(Base.WardStay50WardCode, '') = isnull(Submitted.WardStay50WardCode, '')
	and isnull(Base.WardStay51Sequence, '') = isnull(Submitted.WardStay51Sequence, '')
	and isnull(Base.WardStay51TransferDate, '') = isnull(Submitted.WardStay51TransferDate, '')
	and isnull(Base.WardStay51TransferTime, '') = isnull(Submitted.WardStay51TransferTime, '')
	and isnull(Base.WardStay51HospitalCode, '') = isnull(Submitted.WardStay51HospitalCode, '')
	and isnull(Base.WardStay51WardCode, '') = isnull(Submitted.WardStay51WardCode, '')
	and isnull(Base.WardStay52Sequence, '') = isnull(Submitted.WardStay52Sequence, '')
	and isnull(Base.WardStay52TransferDate, '') = isnull(Submitted.WardStay52TransferDate, '')
	and isnull(Base.WardStay52TransferTime, '') = isnull(Submitted.WardStay52TransferTime, '')
	and isnull(Base.WardStay52HospitalCode, '') = isnull(Submitted.WardStay52HospitalCode, '')
	and isnull(Base.WardStay52WardCode, '') = isnull(Submitted.WardStay52WardCode, '')
	and isnull(Base.WardStay53Sequence, '') = isnull(Submitted.WardStay53Sequence, '')
	and isnull(Base.WardStay53TransferDate, '') = isnull(Submitted.WardStay53TransferDate, '')
	and isnull(Base.WardStay53TransferTime, '') = isnull(Submitted.WardStay53TransferTime, '')
	and isnull(Base.WardStay53HospitalCode, '') = isnull(Submitted.WardStay53HospitalCode, '')
	and isnull(Base.WardStay53WardCode, '') = isnull(Submitted.WardStay53WardCode, '')
	and isnull(Base.WardStay54Sequence, '') = isnull(Submitted.WardStay54Sequence, '')
	and isnull(Base.WardStay54TransferDate, '') = isnull(Submitted.WardStay54TransferDate, '')
	and isnull(Base.WardStay54TransferTime, '') = isnull(Submitted.WardStay54TransferTime, '')
	and isnull(Base.WardStay54HospitalCode, '') = isnull(Submitted.WardStay54HospitalCode, '')
	and isnull(Base.WardStay54WardCode, '') = isnull(Submitted.WardStay54WardCode, '')
	and isnull(Base.WardStay55Sequence, '') = isnull(Submitted.WardStay55Sequence, '')
	and isnull(Base.WardStay55TransferDate, '') = isnull(Submitted.WardStay55TransferDate, '')
	and isnull(Base.WardStay55TransferTime, '') = isnull(Submitted.WardStay55TransferTime, '')
	and isnull(Base.WardStay55HospitalCode, '') = isnull(Submitted.WardStay55HospitalCode, '')
	and isnull(Base.WardStay55WardCode, '') = isnull(Submitted.WardStay55WardCode, '')
	and isnull(Base.WardStay56Sequence, '') = isnull(Submitted.WardStay56Sequence, '')
	and isnull(Base.WardStay56TransferDate, '') = isnull(Submitted.WardStay56TransferDate, '')
	and isnull(Base.WardStay56TransferTime, '') = isnull(Submitted.WardStay56TransferTime, '')
	and isnull(Base.WardStay56HospitalCode, '') = isnull(Submitted.WardStay56HospitalCode, '')
	and isnull(Base.WardStay56WardCode, '') = isnull(Submitted.WardStay56WardCode, '')
	and isnull(Base.WardStay57Sequence, '') = isnull(Submitted.WardStay57Sequence, '')
	and isnull(Base.WardStay57TransferDate, '') = isnull(Submitted.WardStay57TransferDate, '')
	and isnull(Base.WardStay57TransferTime, '') = isnull(Submitted.WardStay57TransferTime, '')
	and isnull(Base.WardStay57HospitalCode, '') = isnull(Submitted.WardStay57HospitalCode, '')
	and isnull(Base.WardStay57WardCode, '') = isnull(Submitted.WardStay57WardCode, '')
	and isnull(Base.WardStay58Sequence, '') = isnull(Submitted.WardStay58Sequence, '')
	and isnull(Base.WardStay58TransferDate, '') = isnull(Submitted.WardStay58TransferDate, '')
	and isnull(Base.WardStay58TransferTime, '') = isnull(Submitted.WardStay58TransferTime, '')
	and isnull(Base.WardStay58HospitalCode, '') = isnull(Submitted.WardStay58HospitalCode, '')
	and isnull(Base.WardStay58WardCode, '') = isnull(Submitted.WardStay58WardCode, '')
	and isnull(Base.WardStay59Sequence, '') = isnull(Submitted.WardStay59Sequence, '')
	and isnull(Base.WardStay59TransferDate, '') = isnull(Submitted.WardStay59TransferDate, '')
	and isnull(Base.WardStay59TransferTime, '') = isnull(Submitted.WardStay59TransferTime, '')
	and isnull(Base.WardStay59HospitalCode, '') = isnull(Submitted.WardStay59HospitalCode, '')
	and isnull(Base.WardStay59WardCode, '') = isnull(Submitted.WardStay59WardCode, '')
	and isnull(Base.WardStay60Sequence, '') = isnull(Submitted.WardStay60Sequence, '')
	and isnull(Base.WardStay60TransferDate, '') = isnull(Submitted.WardStay60TransferDate, '')
	and isnull(Base.WardStay60TransferTime, '') = isnull(Submitted.WardStay60TransferTime, '')
	and isnull(Base.WardStay60HospitalCode, '') = isnull(Submitted.WardStay60HospitalCode, '')
	and isnull(Base.WardStay60WardCode, '') = isnull(Submitted.WardStay60WardCode, '')
	and isnull(Base.WardStay61Sequence, '') = isnull(Submitted.WardStay61Sequence, '')
	and isnull(Base.WardStay61TransferDate, '') = isnull(Submitted.WardStay61TransferDate, '')
	and isnull(Base.WardStay61TransferTime, '') = isnull(Submitted.WardStay61TransferTime, '')
	and isnull(Base.WardStay61HospitalCode, '') = isnull(Submitted.WardStay61HospitalCode, '')
	and isnull(Base.WardStay61WardCode, '') = isnull(Submitted.WardStay61WardCode, '')
	and isnull(Base.WardStay62Sequence, '') = isnull(Submitted.WardStay62Sequence, '')
	and isnull(Base.WardStay62TransferDate, '') = isnull(Submitted.WardStay62TransferDate, '')
	and isnull(Base.WardStay62TransferTime, '') = isnull(Submitted.WardStay62TransferTime, '')
	and isnull(Base.WardStay62HospitalCode, '') = isnull(Submitted.WardStay62HospitalCode, '')
	and isnull(Base.WardStay62WardCode, '') = isnull(Submitted.WardStay62WardCode, '')
	and isnull(Base.WardStay63Sequence, '') = isnull(Submitted.WardStay63Sequence, '')
	and isnull(Base.WardStay63TransferDate, '') = isnull(Submitted.WardStay63TransferDate, '')
	and isnull(Base.WardStay63TransferTime, '') = isnull(Submitted.WardStay63TransferTime, '')
	and isnull(Base.WardStay63HospitalCode, '') = isnull(Submitted.WardStay63HospitalCode, '')
	and isnull(Base.WardStay63WardCode, '') = isnull(Submitted.WardStay63WardCode, '')
	and isnull(Base.WardStay64Sequence, '') = isnull(Submitted.WardStay64Sequence, '')
	and isnull(Base.WardStay64TransferDate, '') = isnull(Submitted.WardStay64TransferDate, '')
	and isnull(Base.WardStay64TransferTime, '') = isnull(Submitted.WardStay64TransferTime, '')
	and isnull(Base.WardStay64HospitalCode, '') = isnull(Submitted.WardStay64HospitalCode, '')
	and isnull(Base.WardStay64WardCode, '') = isnull(Submitted.WardStay64WardCode, '')
	and isnull(Base.WardStay65Sequence, '') = isnull(Submitted.WardStay65Sequence, '')
	and isnull(Base.WardStay65TransferDate, '') = isnull(Submitted.WardStay65TransferDate, '')
	and isnull(Base.WardStay65TransferTime, '') = isnull(Submitted.WardStay65TransferTime, '')
	and isnull(Base.WardStay65HospitalCode, '') = isnull(Submitted.WardStay65HospitalCode, '')
	and isnull(Base.WardStay65WardCode, '') = isnull(Submitted.WardStay65WardCode, '')
	and isnull(Base.WardStay66Sequence, '') = isnull(Submitted.WardStay66Sequence, '')
	and isnull(Base.WardStay66TransferDate, '') = isnull(Submitted.WardStay66TransferDate, '')
	and isnull(Base.WardStay66TransferTime, '') = isnull(Submitted.WardStay66TransferTime, '')
	and isnull(Base.WardStay66HospitalCode, '') = isnull(Submitted.WardStay66HospitalCode, '')
	and isnull(Base.WardStay66WardCode, '') = isnull(Submitted.WardStay66WardCode, '')
	and isnull(Base.WardStay67Sequence, '') = isnull(Submitted.WardStay67Sequence, '')
	and isnull(Base.WardStay67TransferDate, '') = isnull(Submitted.WardStay67TransferDate, '')
	and isnull(Base.WardStay67TransferTime, '') = isnull(Submitted.WardStay67TransferTime, '')
	and isnull(Base.WardStay67HospitalCode, '') = isnull(Submitted.WardStay67HospitalCode, '')
	and isnull(Base.WardStay67WardCode, '') = isnull(Submitted.WardStay67WardCode, '')
	and isnull(Base.WardStay68Sequence, '') = isnull(Submitted.WardStay68Sequence, '')
	and isnull(Base.WardStay68TransferDate, '') = isnull(Submitted.WardStay68TransferDate, '')
	and isnull(Base.WardStay68TransferTime, '') = isnull(Submitted.WardStay68TransferTime, '')
	and isnull(Base.WardStay68HospitalCode, '') = isnull(Submitted.WardStay68HospitalCode, '')
	and isnull(Base.WardStay68WardCode, '') = isnull(Submitted.WardStay68WardCode, '')
	and isnull(Base.WardStay69Sequence, '') = isnull(Submitted.WardStay69Sequence, '')
	and isnull(Base.WardStay69TransferDate, '') = isnull(Submitted.WardStay69TransferDate, '')
	and isnull(Base.WardStay69TransferTime, '') = isnull(Submitted.WardStay69TransferTime, '')
	and isnull(Base.WardStay69HospitalCode, '') = isnull(Submitted.WardStay69HospitalCode, '')
	and isnull(Base.WardStay69WardCode, '') = isnull(Submitted.WardStay69WardCode, '')
	and isnull(Base.WardStay70Sequence, '') = isnull(Submitted.WardStay70Sequence, '')
	and isnull(Base.WardStay70TransferDate, '') = isnull(Submitted.WardStay70TransferDate, '')
	and isnull(Base.WardStay70TransferTime, '') = isnull(Submitted.WardStay70TransferTime, '')
	and isnull(Base.WardStay70HospitalCode, '') = isnull(Submitted.WardStay70HospitalCode, '')
	and isnull(Base.WardStay70WardCode, '') = isnull(Submitted.WardStay70WardCode, '')
	and isnull(Base.WardStay71Sequence, '') = isnull(Submitted.WardStay71Sequence, '')
	and isnull(Base.WardStay71TransferDate, '') = isnull(Submitted.WardStay71TransferDate, '')
	and isnull(Base.WardStay71TransferTime, '') = isnull(Submitted.WardStay71TransferTime, '')
	and isnull(Base.WardStay71HospitalCode, '') = isnull(Submitted.WardStay71HospitalCode, '')
	and isnull(Base.WardStay71WardCode, '') = isnull(Submitted.WardStay71WardCode, '')
	and isnull(Base.WardStay72Sequence, '') = isnull(Submitted.WardStay72Sequence, '')
	and isnull(Base.WardStay72TransferDate, '') = isnull(Submitted.WardStay72TransferDate, '')
	and isnull(Base.WardStay72TransferTime, '') = isnull(Submitted.WardStay72TransferTime, '')
	and isnull(Base.WardStay72HospitalCode, '') = isnull(Submitted.WardStay72HospitalCode, '')
	and isnull(Base.WardStay72WardCode, '') = isnull(Submitted.WardStay72WardCode, '')
	and isnull(Base.WardStay73Sequence, '') = isnull(Submitted.WardStay73Sequence, '')
	and isnull(Base.WardStay73TransferDate, '') = isnull(Submitted.WardStay73TransferDate, '')
	and isnull(Base.WardStay73TransferTime, '') = isnull(Submitted.WardStay73TransferTime, '')
	and isnull(Base.WardStay73HospitalCode, '') = isnull(Submitted.WardStay73HospitalCode, '')
	and isnull(Base.WardStay73WardCode, '') = isnull(Submitted.WardStay73WardCode, '')
	and isnull(Base.WardStay74Sequence, '') = isnull(Submitted.WardStay74Sequence, '')
	and isnull(Base.WardStay74TransferDate, '') = isnull(Submitted.WardStay74TransferDate, '')
	and isnull(Base.WardStay74TransferTime, '') = isnull(Submitted.WardStay74TransferTime, '')
	and isnull(Base.WardStay74HospitalCode, '') = isnull(Submitted.WardStay74HospitalCode, '')
	and isnull(Base.WardStay74WardCode, '') = isnull(Submitted.WardStay74WardCode, '')
	and isnull(Base.NNNStatusIndicator, '') = isnull(Submitted.NNNStatusIndicator, '')
	and isnull(Base.LegalStatusClassificationCode, '') = isnull(Submitted.LegalStatusClassificationCode, '')
	and isnull(Base.HRGCode, '') = isnull(Submitted.HRGCode, '')
	and isnull(Base.PCTofResidenceCode, '') = isnull(Submitted.PCTofResidenceCode, '')
	and isnull(Base.HRGVersionNumber, '') = isnull(Submitted.HRGVersionNumber, '')
	and isnull(Base.DGVPCode, '') = isnull(Submitted.DGVPCode, '')
	and isnull(Base.LengthOfGestationAssessment, '') = isnull(Submitted.LengthOfGestationAssessment, '')
	and isnull(Base.CCP1Number, '') = isnull(Submitted.CCP1Number, '')
	and isnull(Base.CCP1LocalIdentifier, '') = isnull(Submitted.CCP1LocalIdentifier, '')
	and isnull(Base.CCP1StartDate, '') = isnull(Submitted.CCP1StartDate, '')
	and isnull(Base.CCP1UnitFunction, '') = isnull(Submitted.CCP1UnitFunction, '')
	and isnull(Base.CCP1AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP1AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP1BasicRespiratorySupportDays, '') = isnull(Submitted.CCP1BasicRespiratorySupportDays, '')
	and isnull(Base.CCP1AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP1AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP1BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP1BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP1RenalSupportDays, '') = isnull(Submitted.CCP1RenalSupportDays, '')
	and isnull(Base.CCP1NeurologicalSupportDays, '') = isnull(Submitted.CCP1NeurologicalSupportDays, '')
	and isnull(Base.CCP1DermatologicalSupportDays, '') = isnull(Submitted.CCP1DermatologicalSupportDays, '')
	and isnull(Base.CCP1LiverSupportDays, '') = isnull(Submitted.CCP1LiverSupportDays, '')
	and isnull(Base.CCP1Level2Days, '') = isnull(Submitted.CCP1Level2Days, '')
	and isnull(Base.CCP1Level3Days, '') = isnull(Submitted.CCP1Level3Days, '')
	and isnull(Base.CCP1DischargeDate, '') = isnull(Submitted.CCP1DischargeDate, '')
	and isnull(Base.CCP1DischargeTime, '') = isnull(Submitted.CCP1DischargeTime, '')
	and isnull(Base.CCP1MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP1MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP2Number, '') = isnull(Submitted.CCP2Number, '')
	and isnull(Base.CCP2LocalIdentifier, '') = isnull(Submitted.CCP2LocalIdentifier, '')
	and isnull(Base.CCP2StartDate, '') = isnull(Submitted.CCP2StartDate, '')
	and isnull(Base.CCP2UnitFunction, '') = isnull(Submitted.CCP2UnitFunction, '')
	and isnull(Base.CCP2AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP2AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP2BasicRespiratorySupportDays, '') = isnull(Submitted.CCP2BasicRespiratorySupportDays, '')
	and isnull(Base.CCP2AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP2AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP2BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP2BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP2RenalSupportDays, '') = isnull(Submitted.CCP2RenalSupportDays, '')
	and isnull(Base.CCP2NeurologicalSupportDays, '') = isnull(Submitted.CCP2NeurologicalSupportDays, '')
	and isnull(Base.CCP2DermatologicalSupportDays, '') = isnull(Submitted.CCP2DermatologicalSupportDays, '')
	and isnull(Base.CCP2LiverSupportDays, '') = isnull(Submitted.CCP2LiverSupportDays, '')
	and isnull(Base.CCP2Level2Days, '') = isnull(Submitted.CCP2Level2Days, '')
	and isnull(Base.CCP2Level3Days, '') = isnull(Submitted.CCP2Level3Days, '')
	and isnull(Base.CCP2DischargeDate, '') = isnull(Submitted.CCP2DischargeDate, '')
	and isnull(Base.CCP2DischargeTime, '') = isnull(Submitted.CCP2DischargeTime, '')
	and isnull(Base.CCP2MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP2MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP3Number, '') = isnull(Submitted.CCP3Number, '')
	and isnull(Base.CCP3LocalIdentifier, '') = isnull(Submitted.CCP3LocalIdentifier, '')
	and isnull(Base.CCP3StartDate, '') = isnull(Submitted.CCP3StartDate, '')
	and isnull(Base.CCP3UnitFunction, '') = isnull(Submitted.CCP3UnitFunction, '')
	and isnull(Base.CCP3AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP3AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP3BasicRespiratorySupportDays, '') = isnull(Submitted.CCP3BasicRespiratorySupportDays, '')
	and isnull(Base.CCP3AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP3AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP3BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP3BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP3RenalSupportDays, '') = isnull(Submitted.CCP3RenalSupportDays, '')
	and isnull(Base.CCP3NeurologicalSupportDays, '') = isnull(Submitted.CCP3NeurologicalSupportDays, '')
	and isnull(Base.CCP3DermatologicalSupportDays, '') = isnull(Submitted.CCP3DermatologicalSupportDays, '')
	and isnull(Base.CCP3LiverSupportDays, '') = isnull(Submitted.CCP3LiverSupportDays, '')
	and isnull(Base.CCP3Level2Days, '') = isnull(Submitted.CCP3Level2Days, '')
	and isnull(Base.CCP3Level3Days, '') = isnull(Submitted.CCP3Level3Days, '')
	and isnull(Base.CCP3DischargeDate, '') = isnull(Submitted.CCP3DischargeDate, '')
	and isnull(Base.CCP3DischargeTime, '') = isnull(Submitted.CCP3DischargeTime, '')
	and isnull(Base.CCP3MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP3MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP4Number, '') = isnull(Submitted.CCP4Number, '')
	and isnull(Base.CCP4LocalIdentifier, '') = isnull(Submitted.CCP4LocalIdentifier, '')
	and isnull(Base.CCP4StartDate, '') = isnull(Submitted.CCP4StartDate, '')
	and isnull(Base.CCP4UnitFunction, '') = isnull(Submitted.CCP4UnitFunction, '')
	and isnull(Base.CCP4AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP4AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP4BasicRespiratorySupportDays, '') = isnull(Submitted.CCP4BasicRespiratorySupportDays, '')
	and isnull(Base.CCP4AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP4AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP4BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP4BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP4RenalSupportDays, '') = isnull(Submitted.CCP4RenalSupportDays, '')
	and isnull(Base.CCP4NeurologicalSupportDays, '') = isnull(Submitted.CCP4NeurologicalSupportDays, '')
	and isnull(Base.CCP4DermatologicalSupportDays, '') = isnull(Submitted.CCP4DermatologicalSupportDays, '')
	and isnull(Base.CCP4LiverSupportDays, '') = isnull(Submitted.CCP4LiverSupportDays, '')
	and isnull(Base.CCP4Level2Days, '') = isnull(Submitted.CCP4Level2Days, '')
	and isnull(Base.CCP4Level3Days, '') = isnull(Submitted.CCP4Level3Days, '')
	and isnull(Base.CCP4DischargeDate, '') = isnull(Submitted.CCP4DischargeDate, '')
	and isnull(Base.CCP4DischargeTime, '') = isnull(Submitted.CCP4DischargeTime, '')
	and isnull(Base.CCP4MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP4MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP5Number, '') = isnull(Submitted.CCP5Number, '')
	and isnull(Base.CCP5LocalIdentifier, '') = isnull(Submitted.CCP5LocalIdentifier, '')
	and isnull(Base.CCP5StartDate, '') = isnull(Submitted.CCP5StartDate, '')
	and isnull(Base.CCP5UnitFunction, '') = isnull(Submitted.CCP5UnitFunction, '')
	and isnull(Base.CCP5AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP5AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP5BasicRespiratorySupportDays, '') = isnull(Submitted.CCP5BasicRespiratorySupportDays, '')
	and isnull(Base.CCP5AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP5AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP5BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP5BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP5RenalSupportDays, '') = isnull(Submitted.CCP5RenalSupportDays, '')
	and isnull(Base.CCP5NeurologicalSupportDays, '') = isnull(Submitted.CCP5NeurologicalSupportDays, '')
	and isnull(Base.CCP5DermatologicalSupportDays, '') = isnull(Submitted.CCP5DermatologicalSupportDays, '')
	and isnull(Base.CCP5LiverSupportDays, '') = isnull(Submitted.CCP5LiverSupportDays, '')
	and isnull(Base.CCP5Level2Days, '') = isnull(Submitted.CCP5Level2Days, '')
	and isnull(Base.CCP5Level3Days, '') = isnull(Submitted.CCP5Level3Days, '')
	and isnull(Base.CCP5DischargeDate, '') = isnull(Submitted.CCP5DischargeDate, '')
	and isnull(Base.CCP5DischargeTime, '') = isnull(Submitted.CCP5DischargeTime, '')
	and isnull(Base.CCP5MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP5MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP6Number, '') = isnull(Submitted.CCP6Number, '')
	and isnull(Base.CCP6LocalIdentifier, '') = isnull(Submitted.CCP6LocalIdentifier, '')
	and isnull(Base.CCP6StartDate, '') = isnull(Submitted.CCP6StartDate, '')
	and isnull(Base.CCP6UnitFunction, '') = isnull(Submitted.CCP6UnitFunction, '')
	and isnull(Base.CCP6AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP6AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP6BasicRespiratorySupportDays, '') = isnull(Submitted.CCP6BasicRespiratorySupportDays, '')
	and isnull(Base.CCP6AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP6AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP6BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP6BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP6RenalSupportDays, '') = isnull(Submitted.CCP6RenalSupportDays, '')
	and isnull(Base.CCP6NeurologicalSupportDays, '') = isnull(Submitted.CCP6NeurologicalSupportDays, '')
	and isnull(Base.CCP6DermatologicalSupportDays, '') = isnull(Submitted.CCP6DermatologicalSupportDays, '')
	and isnull(Base.CCP6LiverSupportDays, '') = isnull(Submitted.CCP6LiverSupportDays, '')
	and isnull(Base.CCP6Level2Days, '') = isnull(Submitted.CCP6Level2Days, '')
	and isnull(Base.CCP6Level3Days, '') = isnull(Submitted.CCP6Level3Days, '')
	and isnull(Base.CCP6DischargeDate, '') = isnull(Submitted.CCP6DischargeDate, '')
	and isnull(Base.CCP6DischargeTime, '') = isnull(Submitted.CCP6DischargeTime, '')
	and isnull(Base.CCP6MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP6MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP7Number, '') = isnull(Submitted.CCP7Number, '')
	and isnull(Base.CCP7LocalIdentifier, '') = isnull(Submitted.CCP7LocalIdentifier, '')
	and isnull(Base.CCP7StartDate, '') = isnull(Submitted.CCP7StartDate, '')
	and isnull(Base.CCP7UnitFunction, '') = isnull(Submitted.CCP7UnitFunction, '')
	and isnull(Base.CCP7AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP7AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP7BasicRespiratorySupportDays, '') = isnull(Submitted.CCP7BasicRespiratorySupportDays, '')
	and isnull(Base.CCP7AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP7AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP7BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP7BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP7RenalSupportDays, '') = isnull(Submitted.CCP7RenalSupportDays, '')
	and isnull(Base.CCP7NeurologicalSupportDays, '') = isnull(Submitted.CCP7NeurologicalSupportDays, '')
	and isnull(Base.CCP7DermatologicalSupportDays, '') = isnull(Submitted.CCP7DermatologicalSupportDays, '')
	and isnull(Base.CCP7LiverSupportDays, '') = isnull(Submitted.CCP7LiverSupportDays, '')
	and isnull(Base.CCP7Level2Days, '') = isnull(Submitted.CCP7Level2Days, '')
	and isnull(Base.CCP7Level3Days, '') = isnull(Submitted.CCP7Level3Days, '')
	and isnull(Base.CCP7DischargeDate, '') = isnull(Submitted.CCP7DischargeDate, '')
	and isnull(Base.CCP7DischargeTime, '') = isnull(Submitted.CCP7DischargeTime, '')
	and isnull(Base.CCP7MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP7MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP8Number, '') = isnull(Submitted.CCP8Number, '')
	and isnull(Base.CCP8LocalIdentifier, '') = isnull(Submitted.CCP8LocalIdentifier, '')
	and isnull(Base.CCP8StartDate, '') = isnull(Submitted.CCP8StartDate, '')
	and isnull(Base.CCP8UnitFunction, '') = isnull(Submitted.CCP8UnitFunction, '')
	and isnull(Base.CCP8AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP8AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP8BasicRespiratorySupportDays, '') = isnull(Submitted.CCP8BasicRespiratorySupportDays, '')
	and isnull(Base.CCP8AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP8AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP8BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP8BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP8RenalSupportDays, '') = isnull(Submitted.CCP8RenalSupportDays, '')
	and isnull(Base.CCP8NeurologicalSupportDays, '') = isnull(Submitted.CCP8NeurologicalSupportDays, '')
	and isnull(Base.CCP8DermatologicalSupportDays, '') = isnull(Submitted.CCP8DermatologicalSupportDays, '')
	and isnull(Base.CCP8LiverSupportDays, '') = isnull(Submitted.CCP8LiverSupportDays, '')
	and isnull(Base.CCP8Level2Days, '') = isnull(Submitted.CCP8Level2Days, '')
	and isnull(Base.CCP8Level3Days, '') = isnull(Submitted.CCP8Level3Days, '')
	and isnull(Base.CCP8DischargeDate, '') = isnull(Submitted.CCP8DischargeDate, '')
	and isnull(Base.CCP8DischargeTime, '') = isnull(Submitted.CCP8DischargeTime, '')
	and isnull(Base.CCP8MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP8MaximumNumberOfOrganSupportSystem, '')
	and isnull(Base.CCP9Number, '') = isnull(Submitted.CCP9Number, '')
	and isnull(Base.CCP9LocalIdentifier, '') = isnull(Submitted.CCP9LocalIdentifier, '')
	and isnull(Base.CCP9StartDate, '') = isnull(Submitted.CCP9StartDate, '')
	and isnull(Base.CCP9UnitFunction, '') = isnull(Submitted.CCP9UnitFunction, '')
	and isnull(Base.CCP9AdvancedRespiratorySupportDays, '') = isnull(Submitted.CCP9AdvancedRespiratorySupportDays, '')
	and isnull(Base.CCP9BasicRespiratorySupportDays, '') = isnull(Submitted.CCP9BasicRespiratorySupportDays, '')
	and isnull(Base.CCP9AdvancedCardiovascularSupportDays, '') = isnull(Submitted.CCP9AdvancedCardiovascularSupportDays, '')
	and isnull(Base.CCP9BasicCardiovascularSupportDays, '') = isnull(Submitted.CCP9BasicCardiovascularSupportDays, '')
	and isnull(Base.CCP9RenalSupportDays, '') = isnull(Submitted.CCP9RenalSupportDays, '')
	and isnull(Base.CCP9NeurologicalSupportDays, '') = isnull(Submitted.CCP9NeurologicalSupportDays, '')
	and isnull(Base.CCP9DermatologicalSupportDays, '') = isnull(Submitted.CCP9DermatologicalSupportDays, '')
	and isnull(Base.CCP9LiverSupportDays, '') = isnull(Submitted.CCP9LiverSupportDays, '')
	and isnull(Base.CCP9Level2Days, '') = isnull(Submitted.CCP9Level2Days, '')
	and isnull(Base.CCP9Level3Days, '') = isnull(Submitted.CCP9Level3Days, '')
	and isnull(Base.CCP9DischargeDate, '') = isnull(Submitted.CCP9DischargeDate, '')
	and isnull(Base.CCP9DischargeTime, '') = isnull(Submitted.CCP9DischargeTime, '')
	and isnull(Base.CCP9MaximumNumberOfOrganSupportSystem, '') = isnull(Submitted.CCP9MaximumNumberOfOrganSupportSystem, '')
	--and isnull(Base.AgeAtCDSActivityDate, '') = isnull(Submitted.AgeAtCDSActivityDate, '')
	and isnull(Base.AgeOnAdmission, '') = isnull(Submitted.AgeOnAdmission, '')
	and isnull(Base.DischargeReadyDate, '') = isnull(Submitted.DischargeReadyDate, '')
	and isnull(Base.RTTPathwayID, '') = isnull(Submitted.RTTPathwayID, '')
	and isnull(Base.RTTCurrentProviderCode, '') = isnull(Submitted.RTTCurrentProviderCode, '')
	and isnull(Base.RTTPeriodStatusCode, '') = isnull(Submitted.RTTPeriodStatusCode, '')
	and isnull(Base.RTTStartDate, '') = isnull(Submitted.RTTStartDate, '')
	and isnull(Base.RTTEndDate, '') = isnull(Submitted.RTTEndDate, '')
	and isnull(Base.EarliestReasonableOfferDate, '') = isnull(Submitted.EarliestReasonableOfferDate, '')
	and isnull(Base.NCCPPresent, '') = isnull(Submitted.NCCPPresent, '')
	and isnull(Base.Baby1NHSNumberStatus, '') = isnull(Submitted.Baby1NHSNumberStatus, '')
	and isnull(Base.Baby1NHSNumber, '') = isnull(Submitted.Baby1NHSNumber, '')
	and isnull(Base.Baby2NHSNumberStatus, '') = isnull(Submitted.Baby2NHSNumberStatus, '')
	and isnull(Base.Baby2NHSNumber, '') = isnull(Submitted.Baby2NHSNumber, '')
	and isnull(Base.Baby3NHSNumberStatus, '') = isnull(Submitted.Baby3NHSNumberStatus, '')
	and isnull(Base.Baby3NHSNumber, '') = isnull(Submitted.Baby3NHSNumber, '')
	and isnull(Base.Baby4NHSNumberStatus, '') = isnull(Submitted.Baby4NHSNumberStatus, '')
	and isnull(Base.Baby4NHSNumber, '') = isnull(Submitted.Baby4NHSNumber, '')
	and isnull(Base.Baby5NHSNumberStatus, '') = isnull(Submitted.Baby5NHSNumberStatus, '')
	and isnull(Base.Baby5NHSNumber, '') = isnull(Submitted.Baby5NHSNumber, '')
	and isnull(Base.Baby6NHSNumberStatus, '') = isnull(Submitted.Baby6NHSNumberStatus, '')
	and isnull(Base.Baby6NHSNumber, '') = isnull(Submitted.Baby6NHSNumber, '')
	and isnull(Base.EndOfRecord, '') = isnull(Submitted.EndOfRecord, '')
	)


union all

--Inserts - UpdateType = '9'
select
	 Base.UniqueEpisodeSerialNo
	,UpdateType = '9'
	,Base.CDSUpdateDate
	,Base.CDSUpdateTime
	,Base.CDSType
	,Base.RecordType
	,Base.ProviderCode
	,Base.PurchaserCode
	,Base.CommissioningSerialNo
	,Base.NHSServiceAgreementLineNo
	,Base.PurchaserReferenceNo
	,Base.NHSNumber
	,Base.PatientName
	,Base.PatientsAddress
	,Base.Postcode
	,Base.SexCode
	,Base.CarerSupportIndicator
	,Base.DateOfBirth
	,Base.RegisteredGpCode
	,Base.RegisteredGpPracticeCode
	,Base.DistrictNo
	,Base.EthnicGroupCode
	,Base.ReferrerCode
	,Base.ReferringOrganisationCode
	,Base.ProviderSpellNo
	,Base.PatientCategoryCode
	,Base.DecidedToAdmitDate
	,Base.AdmissionDate
	,Base.AdmissionMethodCode
	,Base.DurationOfElectiveWait
	,Base.ManagementIntentionCode
	,Base.SourceOfAdmissionCode
	,Base.DischargeDate
	,Base.DischargeMethodCode
	,Base.DischargeDestinationCode
	,Base.PatientClassificationCode
	,Base.EpisodeNo
	,Base.LastEpisodeInSpellIndicator
	,Base.StartSiteCode
	,Base.StartWardTypeCode
	,Base.EpisodeStartDate
	,Base.EpisodeEndDate
	,Base.MainSpecialtyCode
	,Base.TreatmentFunctionCode
	,Base.ConsultantCode
	,Base.FirstRegularDayNightAdmission
	,Base.NeonatalLevelOfCare
	,Base.PsychiatricPatientStatusCode
	,Base.PrimaryDiagnosisCode
	,Base.SecondaryDiagnosisCode1
	,Base.SecondaryDiagnosisCode2
	,Base.SecondaryDiagnosisCode3
	,Base.SecondaryDiagnosisCode4
	,Base.SecondaryDiagnosisCode5
	,Base.SecondaryDiagnosisCode6
	,Base.SecondaryDiagnosisCode7
	,Base.SecondaryDiagnosisCode8
	,Base.SecondaryDiagnosisCode9
	,Base.SecondaryDiagnosisCode10
	,Base.SecondaryDiagnosisCode11
	,Base.SecondaryDiagnosisCode12
	,Base.OperationStatus
	,Base.PrimaryOperationCode
	,Base.PrimaryOperationDate
	,Base.OperationCode2
	,Base.OperationDate2
	,Base.OperationCode3
	,Base.OperationDate3
	,Base.OperationCode4
	,Base.OperationDate4
	,Base.OperationCode5
	,Base.OperationDate5
	,Base.OperationCode6
	,Base.OperationDate6
	,Base.OperationCode7
	,Base.OperationDate7
	,Base.OperationCode8
	,Base.OperationDate8
	,Base.OperationCode9
	,Base.OperationDate9
	,Base.OperationCode10
	,Base.OperationDate10
	,Base.OperationCode11
	,Base.OperationDate11
	,Base.OperationCode12
	,Base.OperationDate12
	,Base.EpisodeStartWardCode
	,Base.EndWardTypeCode
	,Base.EndSiteCode
	,Base.AnteNatalGpCode
	,Base.FirstAnteNatalAssessmentDate
	,Base.PreviousPregnancies
	,Base.ActualDeliveryPlaceTypeCode
	,Base.IntendedDeliveryPlaceTypeCode
	,Base.DeliveryPlaceChangeReasonCode
	,Base.GestationLength
	,Base.LabourOnsetMethodCode
	,Base.StatusOfPersonConductingDeliveryCode
	,Base.AnaestheticGivenDuringCode
	,Base.AnaestheticGivenPostCode
	,Base.NumberOfBabies
	,Base.Baby1DeliveryDate
	,Base.Baby1DeliveryMethodCode
	,Base.Baby1SexCode
	,Base.Baby1BirthOrder
	,Base.Baby1LiveStillBirth
	,Base.Baby1BirthWeight
	,Base.Baby1MethodOfResuscitationCode
	,Base.Baby1BirthDate
	,Base.Baby1MotherBirthDate
	,Base.Baby2DeliveryDate
	,Base.Baby2DeliveryMethodCode
	,Base.Baby2SexCode
	,Base.Baby2BirthOrder
	,Base.Baby2LiveStillBirth
	,Base.Baby2BirthWeight
	,Base.Baby2MethodOfResuscitationCode
	,Base.Baby2BirthDate
	,Base.Baby2MotherBirthDate
	,Base.Baby3DeliveryDate
	,Base.Baby3DeliveryMethodCode
	,Base.Baby3SexCode
	,Base.Baby3BirthOrder
	,Base.Baby3LiveStillBirth
	,Base.Baby3BirthWeight
	,Base.Baby3MethodOfResuscitationCode
	,Base.Baby3BirthDate
	,Base.Baby3MotherBirthDate
	,Base.Baby4DeliveryDate
	,Base.Baby4DeliveryMethodCode
	,Base.Baby4SexCode
	,Base.Baby4BirthOrder
	,Base.Baby4LiveStillBirth
	,Base.Baby4BirthWeight
	,Base.Baby4MethodOfResuscitationCode
	,Base.Baby4BirthDate
	,Base.Baby4MotherBirthDate
	,Base.Baby5DeliveryDate
	,Base.Baby5DeliveryMethodCode
	,Base.Baby5SexCode
	,Base.Baby5BirthOrder
	,Base.Baby5LiveStillBirth
	,Base.Baby5BirthWeight
	,Base.Baby5MethodOfResuscitationCode
	,Base.Baby5BirthDate
	,Base.Baby5MotherBirthDate
	,Base.Baby6DeliveryDate
	,Base.Baby6DeliveryMethodCode
	,Base.Baby6SexCode
	,Base.Baby6BirthOrder
	,Base.Baby6LiveStillBirth
	,Base.Baby6BirthWeight
	,Base.Baby6MethodOfResuscitationCode
	,Base.Baby6BirthDate
	,Base.Baby6MotherBirthDate
	,Base.NumberOfWardTransfers
	,Base.WardStay1Sequence
	,Base.WardStay1TransferDate
	,Base.WardStay1TransferTime
	,Base.WardStay1HospitalCode
	,Base.WardStay1WardCode
	,Base.WardStay2Sequence
	,Base.WardStay2TransferDate
	,Base.WardStay2TransferTime
	,Base.WardStay2HospitalCode
	,Base.WardStay2WardCode
	,Base.WardStay3Sequence
	,Base.WardStay3TransferDate
	,Base.WardStay3TransferTime
	,Base.WardStay3HospitalCode
	,Base.WardStay3WardCode
	,Base.WardStay4Sequence
	,Base.WardStay4TransferDate
	,Base.WardStay4TransferTime
	,Base.WardStay4HospitalCode
	,Base.WardStay4WardCode
	,Base.WardStay5Sequence
	,Base.WardStay5TransferDate
	,Base.WardStay5TransferTime
	,Base.WardStay5HospitalCode
	,Base.WardStay5WardCode
	,Base.WardStay6Sequence
	,Base.WardStay6TransferDate
	,Base.WardStay6TransferTime
	,Base.WardStay6HospitalCode
	,Base.WardStay6WardCode
	,Base.WardStay7Sequence
	,Base.WardStay7TransferDate
	,Base.WardStay7TransferTime
	,Base.WardStay7HospitalCode
	,Base.WardStay7WardCode
	,Base.WardStay8Sequence
	,Base.WardStay8TransferDate
	,Base.WardStay8TransferTime
	,Base.WardStay8HospitalCode
	,Base.WardStay8WardCode
	,Base.WardStay9Sequence
	,Base.WardStay9TransferDate
	,Base.WardStay9TransferTime
	,Base.WardStay9HospitalCode
	,Base.WardStay9WardCode
	,Base.WardStay10Sequence
	,Base.WardStay10TransferDate
	,Base.WardStay10TransferTime
	,Base.WardStay10HospitalCode
	,Base.WardStay10WardCode
	,Base.WardStay11Sequence
	,Base.WardStay11TransferDate
	,Base.WardStay11TransferTime
	,Base.WardStay11HospitalCode
	,Base.WardStay11WardCode
	,Base.WardStay12Sequence
	,Base.WardStay12TransferDate
	,Base.WardStay12TransferTime
	,Base.WardStay12HospitalCode
	,Base.WardStay12WardCode
	,Base.WardStay13Sequence
	,Base.WardStay13TransferDate
	,Base.WardStay13TransferTime
	,Base.WardStay13HospitalCode
	,Base.WardStay13WardCode
	,Base.WardStay14Sequence
	,Base.WardStay14TransferDate
	,Base.WardStay14TransferTime
	,Base.WardStay14HospitalCode
	,Base.WardStay14WardCode
	,Base.WardStay15Sequence
	,Base.WardStay15TransferDate
	,Base.WardStay15TransferTime
	,Base.WardStay15HospitalCode
	,Base.WardStay15WardCode
	,Base.WardStay16Sequence
	,Base.WardStay16TransferDate
	,Base.WardStay16TransferTime
	,Base.WardStay16HospitalCode
	,Base.WardStay16WardCode
	,Base.WardStay17Sequence
	,Base.WardStay17TransferDate
	,Base.WardStay17TransferTime
	,Base.WardStay17HospitalCode
	,Base.WardStay17WardCode
	,Base.WardStay18Sequence
	,Base.WardStay18TransferDate
	,Base.WardStay18TransferTime
	,Base.WardStay18HospitalCode
	,Base.WardStay18WardCode
	,Base.WardStay19Sequence
	,Base.WardStay19TransferDate
	,Base.WardStay19TransferTime
	,Base.WardStay19HospitalCode
	,Base.WardStay19WardCode
	,Base.WardStay20Sequence
	,Base.WardStay20TransferDate
	,Base.WardStay20TransferTime
	,Base.WardStay20HospitalCode
	,Base.WardStay20WardCode
	,Base.WardStay21Sequence
	,Base.WardStay21TransferDate
	,Base.WardStay21TransferTime
	,Base.WardStay21HospitalCode
	,Base.WardStay21WardCode
	,Base.WardStay22Sequence
	,Base.WardStay22TransferDate
	,Base.WardStay22TransferTime
	,Base.WardStay22HospitalCode
	,Base.WardStay22WardCode
	,Base.WardStay23Sequence
	,Base.WardStay23TransferDate
	,Base.WardStay23TransferTime
	,Base.WardStay23HospitalCode
	,Base.WardStay23WardCode
	,Base.WardStay24Sequence
	,Base.WardStay24TransferDate
	,Base.WardStay24TransferTime
	,Base.WardStay24HospitalCode
	,Base.WardStay24WardCode
	,Base.WardStay25Sequence
	,Base.WardStay25TransferDate
	,Base.WardStay25TransferTime
	,Base.WardStay25HospitalCode
	,Base.WardStay25WardCode
	,Base.WardStay26Sequence
	,Base.WardStay26TransferDate
	,Base.WardStay26TransferTime
	,Base.WardStay26HospitalCode
	,Base.WardStay26WardCode
	,Base.WardStay27Sequence
	,Base.WardStay27TransferDate
	,Base.WardStay27TransferTime
	,Base.WardStay27HospitalCode
	,Base.WardStay27WardCode
	,Base.WardStay28Sequence
	,Base.WardStay28TransferDate
	,Base.WardStay28TransferTime
	,Base.WardStay28HospitalCode
	,Base.WardStay28WardCode
	,Base.WardStay29Sequence
	,Base.WardStay29TransferDate
	,Base.WardStay29TransferTime
	,Base.WardStay29HospitalCode
	,Base.WardStay29WardCode
	,Base.WardStay30Sequence
	,Base.WardStay30TransferDate
	,Base.WardStay30TransferTime
	,Base.WardStay30HospitalCode
	,Base.WardStay30WardCode
	,Base.WardStay31Sequence
	,Base.WardStay31TransferDate
	,Base.WardStay31TransferTime
	,Base.WardStay31HospitalCode
	,Base.WardStay31WardCode
	,Base.WardStay32Sequence
	,Base.WardStay32TransferDate
	,Base.WardStay32TransferTime
	,Base.WardStay32HospitalCode
	,Base.WardStay32WardCode
	,Base.WardStay33Sequence
	,Base.WardStay33TransferDate
	,Base.WardStay33TransferTime
	,Base.WardStay33HospitalCode
	,Base.WardStay33WardCode
	,Base.WardStay34Sequence
	,Base.WardStay34TransferDate
	,Base.WardStay34TransferTime
	,Base.WardStay34HospitalCode
	,Base.WardStay34WardCode
	,Base.WardStay35Sequence
	,Base.WardStay35TransferDate
	,Base.WardStay35TransferTime
	,Base.WardStay35HospitalCode
	,Base.WardStay35WardCode
	,Base.WardStay36Sequence
	,Base.WardStay36TransferDate
	,Base.WardStay36TransferTime
	,Base.WardStay36HospitalCode
	,Base.WardStay36WardCode
	,Base.WardStay37Sequence
	,Base.WardStay37TransferDate
	,Base.WardStay37TransferTime
	,Base.WardStay37HospitalCode
	,Base.WardStay37WardCode
	,Base.WardStay38Sequence
	,Base.WardStay38TransferDate
	,Base.WardStay38TransferTime
	,Base.WardStay38HospitalCode
	,Base.WardStay38WardCode
	,Base.WardStay39Sequence
	,Base.WardStay39TransferDate
	,Base.WardStay39TransferTime
	,Base.WardStay39HospitalCode
	,Base.WardStay39WardCode
	,Base.WardStay40Sequence
	,Base.WardStay40TransferDate
	,Base.WardStay40TransferTime
	,Base.WardStay40HospitalCode
	,Base.WardStay40WardCode
	,Base.WardStay41Sequence
	,Base.WardStay41TransferDate
	,Base.WardStay41TransferTime
	,Base.WardStay41HospitalCode
	,Base.WardStay41WardCode
	,Base.WardStay42Sequence
	,Base.WardStay42TransferDate
	,Base.WardStay42TransferTime
	,Base.WardStay42HospitalCode
	,Base.WardStay42WardCode
	,Base.WardStay43Sequence
	,Base.WardStay43TransferDate
	,Base.WardStay43TransferTime
	,Base.WardStay43HospitalCode
	,Base.WardStay43WardCode
	,Base.WardStay44Sequence
	,Base.WardStay44TransferDate
	,Base.WardStay44TransferTime
	,Base.WardStay44HospitalCode
	,Base.WardStay44WardCode
	,Base.WardStay45Sequence
	,Base.WardStay45TransferDate
	,Base.WardStay45TransferTime
	,Base.WardStay45HospitalCode
	,Base.WardStay45WardCode
	,Base.WardStay46Sequence
	,Base.WardStay46TransferDate
	,Base.WardStay46TransferTime
	,Base.WardStay46HospitalCode
	,Base.WardStay46WardCode
	,Base.WardStay47Sequence
	,Base.WardStay47TransferDate
	,Base.WardStay47TransferTime
	,Base.WardStay47HospitalCode
	,Base.WardStay47WardCode
	,Base.WardStay48Sequence
	,Base.WardStay48TransferDate
	,Base.WardStay48TransferTime
	,Base.WardStay48HospitalCode
	,Base.WardStay48WardCode
	,Base.WardStay49Sequence
	,Base.WardStay49TransferDate
	,Base.WardStay49TransferTime
	,Base.WardStay49HospitalCode
	,Base.WardStay49WardCode
	,Base.WardStay50Sequence
	,Base.WardStay50TransferDate
	,Base.WardStay50TransferTime
	,Base.WardStay50HospitalCode
	,Base.WardStay50WardCode
	,Base.WardStay51Sequence
	,Base.WardStay51TransferDate
	,Base.WardStay51TransferTime
	,Base.WardStay51HospitalCode
	,Base.WardStay51WardCode
	,Base.WardStay52Sequence
	,Base.WardStay52TransferDate
	,Base.WardStay52TransferTime
	,Base.WardStay52HospitalCode
	,Base.WardStay52WardCode
	,Base.WardStay53Sequence
	,Base.WardStay53TransferDate
	,Base.WardStay53TransferTime
	,Base.WardStay53HospitalCode
	,Base.WardStay53WardCode
	,Base.WardStay54Sequence
	,Base.WardStay54TransferDate
	,Base.WardStay54TransferTime
	,Base.WardStay54HospitalCode
	,Base.WardStay54WardCode
	,Base.WardStay55Sequence
	,Base.WardStay55TransferDate
	,Base.WardStay55TransferTime
	,Base.WardStay55HospitalCode
	,Base.WardStay55WardCode
	,Base.WardStay56Sequence
	,Base.WardStay56TransferDate
	,Base.WardStay56TransferTime
	,Base.WardStay56HospitalCode
	,Base.WardStay56WardCode
	,Base.WardStay57Sequence
	,Base.WardStay57TransferDate
	,Base.WardStay57TransferTime
	,Base.WardStay57HospitalCode
	,Base.WardStay57WardCode
	,Base.WardStay58Sequence
	,Base.WardStay58TransferDate
	,Base.WardStay58TransferTime
	,Base.WardStay58HospitalCode
	,Base.WardStay58WardCode
	,Base.WardStay59Sequence
	,Base.WardStay59TransferDate
	,Base.WardStay59TransferTime
	,Base.WardStay59HospitalCode
	,Base.WardStay59WardCode
	,Base.WardStay60Sequence
	,Base.WardStay60TransferDate
	,Base.WardStay60TransferTime
	,Base.WardStay60HospitalCode
	,Base.WardStay60WardCode
	,Base.WardStay61Sequence
	,Base.WardStay61TransferDate
	,Base.WardStay61TransferTime
	,Base.WardStay61HospitalCode
	,Base.WardStay61WardCode
	,Base.WardStay62Sequence
	,Base.WardStay62TransferDate
	,Base.WardStay62TransferTime
	,Base.WardStay62HospitalCode
	,Base.WardStay62WardCode
	,Base.WardStay63Sequence
	,Base.WardStay63TransferDate
	,Base.WardStay63TransferTime
	,Base.WardStay63HospitalCode
	,Base.WardStay63WardCode
	,Base.WardStay64Sequence
	,Base.WardStay64TransferDate
	,Base.WardStay64TransferTime
	,Base.WardStay64HospitalCode
	,Base.WardStay64WardCode
	,Base.WardStay65Sequence
	,Base.WardStay65TransferDate
	,Base.WardStay65TransferTime
	,Base.WardStay65HospitalCode
	,Base.WardStay65WardCode
	,Base.WardStay66Sequence
	,Base.WardStay66TransferDate
	,Base.WardStay66TransferTime
	,Base.WardStay66HospitalCode
	,Base.WardStay66WardCode
	,Base.WardStay67Sequence
	,Base.WardStay67TransferDate
	,Base.WardStay67TransferTime
	,Base.WardStay67HospitalCode
	,Base.WardStay67WardCode
	,Base.WardStay68Sequence
	,Base.WardStay68TransferDate
	,Base.WardStay68TransferTime
	,Base.WardStay68HospitalCode
	,Base.WardStay68WardCode
	,Base.WardStay69Sequence
	,Base.WardStay69TransferDate
	,Base.WardStay69TransferTime
	,Base.WardStay69HospitalCode
	,Base.WardStay69WardCode
	,Base.WardStay70Sequence
	,Base.WardStay70TransferDate
	,Base.WardStay70TransferTime
	,Base.WardStay70HospitalCode
	,Base.WardStay70WardCode
	,Base.WardStay71Sequence
	,Base.WardStay71TransferDate
	,Base.WardStay71TransferTime
	,Base.WardStay71HospitalCode
	,Base.WardStay71WardCode
	,Base.WardStay72Sequence
	,Base.WardStay72TransferDate
	,Base.WardStay72TransferTime
	,Base.WardStay72HospitalCode
	,Base.WardStay72WardCode
	,Base.WardStay73Sequence
	,Base.WardStay73TransferDate
	,Base.WardStay73TransferTime
	,Base.WardStay73HospitalCode
	,Base.WardStay73WardCode
	,Base.WardStay74Sequence
	,Base.WardStay74TransferDate
	,Base.WardStay74TransferTime
	,Base.WardStay74HospitalCode
	,Base.WardStay74WardCode
	,Base.NNNStatusIndicator
	,Base.LegalStatusClassificationCode
	,Base.HRGCode
	,Base.PCTofResidenceCode
	,Base.HRGVersionNumber
	,Base.DGVPCode
	,Base.LengthOfGestationAssessment
	,Base.CCP1Number
	,Base.CCP1LocalIdentifier
	,Base.CCP1StartDate
	,Base.CCP1UnitFunction
	,Base.CCP1AdvancedRespiratorySupportDays
	,Base.CCP1BasicRespiratorySupportDays
	,Base.CCP1AdvancedCardiovascularSupportDays
	,Base.CCP1BasicCardiovascularSupportDays
	,Base.CCP1RenalSupportDays
	,Base.CCP1NeurologicalSupportDays
	,Base.CCP1DermatologicalSupportDays
	,Base.CCP1LiverSupportDays
	,Base.CCP1Level2Days
	,Base.CCP1Level3Days
	,Base.CCP1DischargeDate
	,Base.CCP1DischargeTime
	,Base.CCP1MaximumNumberOfOrganSupportSystem
	,Base.CCP2Number
	,Base.CCP2LocalIdentifier
	,Base.CCP2StartDate
	,Base.CCP2UnitFunction
	,Base.CCP2AdvancedRespiratorySupportDays
	,Base.CCP2BasicRespiratorySupportDays
	,Base.CCP2AdvancedCardiovascularSupportDays
	,Base.CCP2BasicCardiovascularSupportDays
	,Base.CCP2RenalSupportDays
	,Base.CCP2NeurologicalSupportDays
	,Base.CCP2DermatologicalSupportDays
	,Base.CCP2LiverSupportDays
	,Base.CCP2Level2Days
	,Base.CCP2Level3Days
	,Base.CCP2DischargeDate
	,Base.CCP2DischargeTime
	,Base.CCP2MaximumNumberOfOrganSupportSystem
	,Base.CCP3Number
	,Base.CCP3LocalIdentifier
	,Base.CCP3StartDate
	,Base.CCP3UnitFunction
	,Base.CCP3AdvancedRespiratorySupportDays
	,Base.CCP3BasicRespiratorySupportDays
	,Base.CCP3AdvancedCardiovascularSupportDays
	,Base.CCP3BasicCardiovascularSupportDays
	,Base.CCP3RenalSupportDays
	,Base.CCP3NeurologicalSupportDays
	,Base.CCP3DermatologicalSupportDays
	,Base.CCP3LiverSupportDays
	,Base.CCP3Level2Days
	,Base.CCP3Level3Days
	,Base.CCP3DischargeDate
	,Base.CCP3DischargeTime
	,Base.CCP3MaximumNumberOfOrganSupportSystem
	,Base.CCP4Number
	,Base.CCP4LocalIdentifier
	,Base.CCP4StartDate
	,Base.CCP4UnitFunction
	,Base.CCP4AdvancedRespiratorySupportDays
	,Base.CCP4BasicRespiratorySupportDays
	,Base.CCP4AdvancedCardiovascularSupportDays
	,Base.CCP4BasicCardiovascularSupportDays
	,Base.CCP4RenalSupportDays
	,Base.CCP4NeurologicalSupportDays
	,Base.CCP4DermatologicalSupportDays
	,Base.CCP4LiverSupportDays
	,Base.CCP4Level2Days
	,Base.CCP4Level3Days
	,Base.CCP4DischargeDate
	,Base.CCP4DischargeTime
	,Base.CCP4MaximumNumberOfOrganSupportSystem
	,Base.CCP5Number
	,Base.CCP5LocalIdentifier
	,Base.CCP5StartDate
	,Base.CCP5UnitFunction
	,Base.CCP5AdvancedRespiratorySupportDays
	,Base.CCP5BasicRespiratorySupportDays
	,Base.CCP5AdvancedCardiovascularSupportDays
	,Base.CCP5BasicCardiovascularSupportDays
	,Base.CCP5RenalSupportDays
	,Base.CCP5NeurologicalSupportDays
	,Base.CCP5DermatologicalSupportDays
	,Base.CCP5LiverSupportDays
	,Base.CCP5Level2Days
	,Base.CCP5Level3Days
	,Base.CCP5DischargeDate
	,Base.CCP5DischargeTime
	,Base.CCP5MaximumNumberOfOrganSupportSystem
	,Base.CCP6Number
	,Base.CCP6LocalIdentifier
	,Base.CCP6StartDate
	,Base.CCP6UnitFunction
	,Base.CCP6AdvancedRespiratorySupportDays
	,Base.CCP6BasicRespiratorySupportDays
	,Base.CCP6AdvancedCardiovascularSupportDays
	,Base.CCP6BasicCardiovascularSupportDays
	,Base.CCP6RenalSupportDays
	,Base.CCP6NeurologicalSupportDays
	,Base.CCP6DermatologicalSupportDays
	,Base.CCP6LiverSupportDays
	,Base.CCP6Level2Days
	,Base.CCP6Level3Days
	,Base.CCP6DischargeDate
	,Base.CCP6DischargeTime
	,Base.CCP6MaximumNumberOfOrganSupportSystem
	,Base.CCP7Number
	,Base.CCP7LocalIdentifier
	,Base.CCP7StartDate
	,Base.CCP7UnitFunction
	,Base.CCP7AdvancedRespiratorySupportDays
	,Base.CCP7BasicRespiratorySupportDays
	,Base.CCP7AdvancedCardiovascularSupportDays
	,Base.CCP7BasicCardiovascularSupportDays
	,Base.CCP7RenalSupportDays
	,Base.CCP7NeurologicalSupportDays
	,Base.CCP7DermatologicalSupportDays
	,Base.CCP7LiverSupportDays
	,Base.CCP7Level2Days
	,Base.CCP7Level3Days
	,Base.CCP7DischargeDate
	,Base.CCP7DischargeTime
	,Base.CCP7MaximumNumberOfOrganSupportSystem
	,Base.CCP8Number
	,Base.CCP8LocalIdentifier
	,Base.CCP8StartDate
	,Base.CCP8UnitFunction
	,Base.CCP8AdvancedRespiratorySupportDays
	,Base.CCP8BasicRespiratorySupportDays
	,Base.CCP8AdvancedCardiovascularSupportDays
	,Base.CCP8BasicCardiovascularSupportDays
	,Base.CCP8RenalSupportDays
	,Base.CCP8NeurologicalSupportDays
	,Base.CCP8DermatologicalSupportDays
	,Base.CCP8LiverSupportDays
	,Base.CCP8Level2Days
	,Base.CCP8Level3Days
	,Base.CCP8DischargeDate
	,Base.CCP8DischargeTime
	,Base.CCP8MaximumNumberOfOrganSupportSystem
	,Base.CCP9Number
	,Base.CCP9LocalIdentifier
	,Base.CCP9StartDate
	,Base.CCP9UnitFunction
	,Base.CCP9AdvancedRespiratorySupportDays
	,Base.CCP9BasicRespiratorySupportDays
	,Base.CCP9AdvancedCardiovascularSupportDays
	,Base.CCP9BasicCardiovascularSupportDays
	,Base.CCP9RenalSupportDays
	,Base.CCP9NeurologicalSupportDays
	,Base.CCP9DermatologicalSupportDays
	,Base.CCP9LiverSupportDays
	,Base.CCP9Level2Days
	,Base.CCP9Level3Days
	,Base.CCP9DischargeDate
	,Base.CCP9DischargeTime
	,Base.CCP9MaximumNumberOfOrganSupportSystem
	,Base.AgeAtCDSActivityDate
	,Base.AgeOnAdmission
	,Base.DischargeReadyDate
	,Base.RTTPathwayID
	,Base.RTTCurrentProviderCode
	,Base.RTTPeriodStatusCode
	,Base.RTTStartDate
	,Base.RTTEndDate
	,Base.EarliestReasonableOfferDate
	,Base.NCCPPresent
	,Base.Baby1NHSNumberStatus
	,Base.Baby1NHSNumber
	,Base.Baby2NHSNumberStatus
	,Base.Baby2NHSNumber
	,Base.Baby3NHSNumberStatus
	,Base.Baby3NHSNumber
	,Base.Baby4NHSNumberStatus
	,Base.Baby4NHSNumber
	,Base.Baby5NHSNumberStatus
	,Base.Baby5NHSNumber
	,Base.Baby6NHSNumberStatus
	,Base.Baby6NHSNumber
	,Base.EndOfRecord
from
	CDS.APCAngliaBase Base
where
	not exists
	(
	select
		1
	from
		CDS.APCAngliaSubmitted Submitted
	where
		Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
	)

or	exists --latest update type was delete
	(
	select
		1
	from
		CDS.APCAngliaSubmitted Submitted
	where
		Submitted.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
	and	Submitted.UpdateType = '1' --delete
	and	not exists
		(
		select
			1
		from
			CDS.APCAngliaSubmitted Previous
		where
			Previous.UniqueEpisodeSerialNo = Base.UniqueEpisodeSerialNo
		and	Previous.Created > Submitted.Created
		)
	)

union all

--Deletes - UpdateType = '1'
select
	 Submitted.UniqueEpisodeSerialNo
	,UpdateType = '1'
	,Submitted.CDSUpdateDate
	,Submitted.CDSUpdateTime
	,Submitted.CDSType
	,Submitted.RecordType
	,Submitted.ProviderCode
	,Submitted.PurchaserCode
	,Submitted.CommissioningSerialNo
	,Submitted.NHSServiceAgreementLineNo
	,Submitted.PurchaserReferenceNo
	,Submitted.NHSNumber
	,Submitted.PatientName
	,Submitted.PatientsAddress
	,Submitted.Postcode
	,Submitted.SexCode
	,Submitted.CarerSupportIndicator
	,Submitted.DateOfBirth
	,Submitted.RegisteredGpCode
	,Submitted.RegisteredGpPracticeCode
	,Submitted.DistrictNo
	,Submitted.EthnicGroupCode
	,Submitted.ReferrerCode
	,Submitted.ReferringOrganisationCode
	,Submitted.ProviderSpellNo
	,Submitted.PatientCategoryCode
	,Submitted.DecidedToAdmitDate
	,Submitted.AdmissionDate
	,Submitted.AdmissionMethodCode
	,Submitted.DurationOfElectiveWait
	,Submitted.ManagementIntentionCode
	,Submitted.SourceOfAdmissionCode
	,Submitted.DischargeDate
	,Submitted.DischargeMethodCode
	,Submitted.DischargeDestinationCode
	,Submitted.PatientClassificationCode
	,Submitted.EpisodeNo
	,Submitted.LastEpisodeInSpellIndicator
	,Submitted.StartSiteCode
	,Submitted.StartWardTypeCode
	,Submitted.EpisodeStartDate
	,Submitted.EpisodeEndDate
	,Submitted.MainSpecialtyCode
	,Submitted.TreatmentFunctionCode
	,Submitted.ConsultantCode
	,Submitted.FirstRegularDayNightAdmission
	,Submitted.NeonatalLevelOfCare
	,Submitted.PsychiatricPatientStatusCode
	,Submitted.PrimaryDiagnosisCode
	,Submitted.SecondaryDiagnosisCode1
	,Submitted.SecondaryDiagnosisCode2
	,Submitted.SecondaryDiagnosisCode3
	,Submitted.SecondaryDiagnosisCode4
	,Submitted.SecondaryDiagnosisCode5
	,Submitted.SecondaryDiagnosisCode6
	,Submitted.SecondaryDiagnosisCode7
	,Submitted.SecondaryDiagnosisCode8
	,Submitted.SecondaryDiagnosisCode9
	,Submitted.SecondaryDiagnosisCode10
	,Submitted.SecondaryDiagnosisCode11
	,Submitted.SecondaryDiagnosisCode12
	,Submitted.OperationStatus
	,Submitted.PrimaryOperationCode
	,Submitted.PrimaryOperationDate
	,Submitted.OperationCode2
	,Submitted.OperationDate2
	,Submitted.OperationCode3
	,Submitted.OperationDate3
	,Submitted.OperationCode4
	,Submitted.OperationDate4
	,Submitted.OperationCode5
	,Submitted.OperationDate5
	,Submitted.OperationCode6
	,Submitted.OperationDate6
	,Submitted.OperationCode7
	,Submitted.OperationDate7
	,Submitted.OperationCode8
	,Submitted.OperationDate8
	,Submitted.OperationCode9
	,Submitted.OperationDate9
	,Submitted.OperationCode10
	,Submitted.OperationDate10
	,Submitted.OperationCode11
	,Submitted.OperationDate11
	,Submitted.OperationCode12
	,Submitted.OperationDate12
	,Submitted.EpisodeStartWardCode
	,Submitted.EndWardTypeCode
	,Submitted.EndSiteCode
	,Submitted.AnteNatalGpCode
	,Submitted.FirstAnteNatalAssessmentDate
	,Submitted.PreviousPregnancies
	,Submitted.ActualDeliveryPlaceTypeCode
	,Submitted.IntendedDeliveryPlaceTypeCode
	,Submitted.DeliveryPlaceChangeReasonCode
	,Submitted.GestationLength
	,Submitted.LabourOnsetMethodCode
	,Submitted.StatusOfPersonConductingDeliveryCode
	,Submitted.AnaestheticGivenDuringCode
	,Submitted.AnaestheticGivenPostCode
	,Submitted.NumberOfBabies
	,Submitted.Baby1DeliveryDate
	,Submitted.Baby1DeliveryMethodCode
	,Submitted.Baby1SexCode
	,Submitted.Baby1BirthOrder
	,Submitted.Baby1LiveStillBirth
	,Submitted.Baby1BirthWeight
	,Submitted.Baby1MethodOfResuscitationCode
	,Submitted.Baby1BirthDate
	,Submitted.Baby1MotherBirthDate
	,Submitted.Baby2DeliveryDate
	,Submitted.Baby2DeliveryMethodCode
	,Submitted.Baby2SexCode
	,Submitted.Baby2BirthOrder
	,Submitted.Baby2LiveStillBirth
	,Submitted.Baby2BirthWeight
	,Submitted.Baby2MethodOfResuscitationCode
	,Submitted.Baby2BirthDate
	,Submitted.Baby2MotherBirthDate
	,Submitted.Baby3DeliveryDate
	,Submitted.Baby3DeliveryMethodCode
	,Submitted.Baby3SexCode
	,Submitted.Baby3BirthOrder
	,Submitted.Baby3LiveStillBirth
	,Submitted.Baby3BirthWeight
	,Submitted.Baby3MethodOfResuscitationCode
	,Submitted.Baby3BirthDate
	,Submitted.Baby3MotherBirthDate
	,Submitted.Baby4DeliveryDate
	,Submitted.Baby4DeliveryMethodCode
	,Submitted.Baby4SexCode
	,Submitted.Baby4BirthOrder
	,Submitted.Baby4LiveStillBirth
	,Submitted.Baby4BirthWeight
	,Submitted.Baby4MethodOfResuscitationCode
	,Submitted.Baby4BirthDate
	,Submitted.Baby4MotherBirthDate
	,Submitted.Baby5DeliveryDate
	,Submitted.Baby5DeliveryMethodCode
	,Submitted.Baby5SexCode
	,Submitted.Baby5BirthOrder
	,Submitted.Baby5LiveStillBirth
	,Submitted.Baby5BirthWeight
	,Submitted.Baby5MethodOfResuscitationCode
	,Submitted.Baby5BirthDate
	,Submitted.Baby5MotherBirthDate
	,Submitted.Baby6DeliveryDate
	,Submitted.Baby6DeliveryMethodCode
	,Submitted.Baby6SexCode
	,Submitted.Baby6BirthOrder
	,Submitted.Baby6LiveStillBirth
	,Submitted.Baby6BirthWeight
	,Submitted.Baby6MethodOfResuscitationCode
	,Submitted.Baby6BirthDate
	,Submitted.Baby6MotherBirthDate
	,Submitted.NumberOfWardTransfers
	,Submitted.WardStay1Sequence
	,Submitted.WardStay1TransferDate
	,Submitted.WardStay1TransferTime
	,Submitted.WardStay1HospitalCode
	,Submitted.WardStay1WardCode
	,Submitted.WardStay2Sequence
	,Submitted.WardStay2TransferDate
	,Submitted.WardStay2TransferTime
	,Submitted.WardStay2HospitalCode
	,Submitted.WardStay2WardCode
	,Submitted.WardStay3Sequence
	,Submitted.WardStay3TransferDate
	,Submitted.WardStay3TransferTime
	,Submitted.WardStay3HospitalCode
	,Submitted.WardStay3WardCode
	,Submitted.WardStay4Sequence
	,Submitted.WardStay4TransferDate
	,Submitted.WardStay4TransferTime
	,Submitted.WardStay4HospitalCode
	,Submitted.WardStay4WardCode
	,Submitted.WardStay5Sequence
	,Submitted.WardStay5TransferDate
	,Submitted.WardStay5TransferTime
	,Submitted.WardStay5HospitalCode
	,Submitted.WardStay5WardCode
	,Submitted.WardStay6Sequence
	,Submitted.WardStay6TransferDate
	,Submitted.WardStay6TransferTime
	,Submitted.WardStay6HospitalCode
	,Submitted.WardStay6WardCode
	,Submitted.WardStay7Sequence
	,Submitted.WardStay7TransferDate
	,Submitted.WardStay7TransferTime
	,Submitted.WardStay7HospitalCode
	,Submitted.WardStay7WardCode
	,Submitted.WardStay8Sequence
	,Submitted.WardStay8TransferDate
	,Submitted.WardStay8TransferTime
	,Submitted.WardStay8HospitalCode
	,Submitted.WardStay8WardCode
	,Submitted.WardStay9Sequence
	,Submitted.WardStay9TransferDate
	,Submitted.WardStay9TransferTime
	,Submitted.WardStay9HospitalCode
	,Submitted.WardStay9WardCode
	,Submitted.WardStay10Sequence
	,Submitted.WardStay10TransferDate
	,Submitted.WardStay10TransferTime
	,Submitted.WardStay10HospitalCode
	,Submitted.WardStay10WardCode
	,Submitted.WardStay11Sequence
	,Submitted.WardStay11TransferDate
	,Submitted.WardStay11TransferTime
	,Submitted.WardStay11HospitalCode
	,Submitted.WardStay11WardCode
	,Submitted.WardStay12Sequence
	,Submitted.WardStay12TransferDate
	,Submitted.WardStay12TransferTime
	,Submitted.WardStay12HospitalCode
	,Submitted.WardStay12WardCode
	,Submitted.WardStay13Sequence
	,Submitted.WardStay13TransferDate
	,Submitted.WardStay13TransferTime
	,Submitted.WardStay13HospitalCode
	,Submitted.WardStay13WardCode
	,Submitted.WardStay14Sequence
	,Submitted.WardStay14TransferDate
	,Submitted.WardStay14TransferTime
	,Submitted.WardStay14HospitalCode
	,Submitted.WardStay14WardCode
	,Submitted.WardStay15Sequence
	,Submitted.WardStay15TransferDate
	,Submitted.WardStay15TransferTime
	,Submitted.WardStay15HospitalCode
	,Submitted.WardStay15WardCode
	,Submitted.WardStay16Sequence
	,Submitted.WardStay16TransferDate
	,Submitted.WardStay16TransferTime
	,Submitted.WardStay16HospitalCode
	,Submitted.WardStay16WardCode
	,Submitted.WardStay17Sequence
	,Submitted.WardStay17TransferDate
	,Submitted.WardStay17TransferTime
	,Submitted.WardStay17HospitalCode
	,Submitted.WardStay17WardCode
	,Submitted.WardStay18Sequence
	,Submitted.WardStay18TransferDate
	,Submitted.WardStay18TransferTime
	,Submitted.WardStay18HospitalCode
	,Submitted.WardStay18WardCode
	,Submitted.WardStay19Sequence
	,Submitted.WardStay19TransferDate
	,Submitted.WardStay19TransferTime
	,Submitted.WardStay19HospitalCode
	,Submitted.WardStay19WardCode
	,Submitted.WardStay20Sequence
	,Submitted.WardStay20TransferDate
	,Submitted.WardStay20TransferTime
	,Submitted.WardStay20HospitalCode
	,Submitted.WardStay20WardCode
	,Submitted.WardStay21Sequence
	,Submitted.WardStay21TransferDate
	,Submitted.WardStay21TransferTime
	,Submitted.WardStay21HospitalCode
	,Submitted.WardStay21WardCode
	,Submitted.WardStay22Sequence
	,Submitted.WardStay22TransferDate
	,Submitted.WardStay22TransferTime
	,Submitted.WardStay22HospitalCode
	,Submitted.WardStay22WardCode
	,Submitted.WardStay23Sequence
	,Submitted.WardStay23TransferDate
	,Submitted.WardStay23TransferTime
	,Submitted.WardStay23HospitalCode
	,Submitted.WardStay23WardCode
	,Submitted.WardStay24Sequence
	,Submitted.WardStay24TransferDate
	,Submitted.WardStay24TransferTime
	,Submitted.WardStay24HospitalCode
	,Submitted.WardStay24WardCode
	,Submitted.WardStay25Sequence
	,Submitted.WardStay25TransferDate
	,Submitted.WardStay25TransferTime
	,Submitted.WardStay25HospitalCode
	,Submitted.WardStay25WardCode
	,Submitted.WardStay26Sequence
	,Submitted.WardStay26TransferDate
	,Submitted.WardStay26TransferTime
	,Submitted.WardStay26HospitalCode
	,Submitted.WardStay26WardCode
	,Submitted.WardStay27Sequence
	,Submitted.WardStay27TransferDate
	,Submitted.WardStay27TransferTime
	,Submitted.WardStay27HospitalCode
	,Submitted.WardStay27WardCode
	,Submitted.WardStay28Sequence
	,Submitted.WardStay28TransferDate
	,Submitted.WardStay28TransferTime
	,Submitted.WardStay28HospitalCode
	,Submitted.WardStay28WardCode
	,Submitted.WardStay29Sequence
	,Submitted.WardStay29TransferDate
	,Submitted.WardStay29TransferTime
	,Submitted.WardStay29HospitalCode
	,Submitted.WardStay29WardCode
	,Submitted.WardStay30Sequence
	,Submitted.WardStay30TransferDate
	,Submitted.WardStay30TransferTime
	,Submitted.WardStay30HospitalCode
	,Submitted.WardStay30WardCode
	,Submitted.WardStay31Sequence
	,Submitted.WardStay31TransferDate
	,Submitted.WardStay31TransferTime
	,Submitted.WardStay31HospitalCode
	,Submitted.WardStay31WardCode
	,Submitted.WardStay32Sequence
	,Submitted.WardStay32TransferDate
	,Submitted.WardStay32TransferTime
	,Submitted.WardStay32HospitalCode
	,Submitted.WardStay32WardCode
	,Submitted.WardStay33Sequence
	,Submitted.WardStay33TransferDate
	,Submitted.WardStay33TransferTime
	,Submitted.WardStay33HospitalCode
	,Submitted.WardStay33WardCode
	,Submitted.WardStay34Sequence
	,Submitted.WardStay34TransferDate
	,Submitted.WardStay34TransferTime
	,Submitted.WardStay34HospitalCode
	,Submitted.WardStay34WardCode
	,Submitted.WardStay35Sequence
	,Submitted.WardStay35TransferDate
	,Submitted.WardStay35TransferTime
	,Submitted.WardStay35HospitalCode
	,Submitted.WardStay35WardCode
	,Submitted.WardStay36Sequence
	,Submitted.WardStay36TransferDate
	,Submitted.WardStay36TransferTime
	,Submitted.WardStay36HospitalCode
	,Submitted.WardStay36WardCode
	,Submitted.WardStay37Sequence
	,Submitted.WardStay37TransferDate
	,Submitted.WardStay37TransferTime
	,Submitted.WardStay37HospitalCode
	,Submitted.WardStay37WardCode
	,Submitted.WardStay38Sequence
	,Submitted.WardStay38TransferDate
	,Submitted.WardStay38TransferTime
	,Submitted.WardStay38HospitalCode
	,Submitted.WardStay38WardCode
	,Submitted.WardStay39Sequence
	,Submitted.WardStay39TransferDate
	,Submitted.WardStay39TransferTime
	,Submitted.WardStay39HospitalCode
	,Submitted.WardStay39WardCode
	,Submitted.WardStay40Sequence
	,Submitted.WardStay40TransferDate
	,Submitted.WardStay40TransferTime
	,Submitted.WardStay40HospitalCode
	,Submitted.WardStay40WardCode
	,Submitted.WardStay41Sequence
	,Submitted.WardStay41TransferDate
	,Submitted.WardStay41TransferTime
	,Submitted.WardStay41HospitalCode
	,Submitted.WardStay41WardCode
	,Submitted.WardStay42Sequence
	,Submitted.WardStay42TransferDate
	,Submitted.WardStay42TransferTime
	,Submitted.WardStay42HospitalCode
	,Submitted.WardStay42WardCode
	,Submitted.WardStay43Sequence
	,Submitted.WardStay43TransferDate
	,Submitted.WardStay43TransferTime
	,Submitted.WardStay43HospitalCode
	,Submitted.WardStay43WardCode
	,Submitted.WardStay44Sequence
	,Submitted.WardStay44TransferDate
	,Submitted.WardStay44TransferTime
	,Submitted.WardStay44HospitalCode
	,Submitted.WardStay44WardCode
	,Submitted.WardStay45Sequence
	,Submitted.WardStay45TransferDate
	,Submitted.WardStay45TransferTime
	,Submitted.WardStay45HospitalCode
	,Submitted.WardStay45WardCode
	,Submitted.WardStay46Sequence
	,Submitted.WardStay46TransferDate
	,Submitted.WardStay46TransferTime
	,Submitted.WardStay46HospitalCode
	,Submitted.WardStay46WardCode
	,Submitted.WardStay47Sequence
	,Submitted.WardStay47TransferDate
	,Submitted.WardStay47TransferTime
	,Submitted.WardStay47HospitalCode
	,Submitted.WardStay47WardCode
	,Submitted.WardStay48Sequence
	,Submitted.WardStay48TransferDate
	,Submitted.WardStay48TransferTime
	,Submitted.WardStay48HospitalCode
	,Submitted.WardStay48WardCode
	,Submitted.WardStay49Sequence
	,Submitted.WardStay49TransferDate
	,Submitted.WardStay49TransferTime
	,Submitted.WardStay49HospitalCode
	,Submitted.WardStay49WardCode
	,Submitted.WardStay50Sequence
	,Submitted.WardStay50TransferDate
	,Submitted.WardStay50TransferTime
	,Submitted.WardStay50HospitalCode
	,Submitted.WardStay50WardCode
	,Submitted.WardStay51Sequence
	,Submitted.WardStay51TransferDate
	,Submitted.WardStay51TransferTime
	,Submitted.WardStay51HospitalCode
	,Submitted.WardStay51WardCode
	,Submitted.WardStay52Sequence
	,Submitted.WardStay52TransferDate
	,Submitted.WardStay52TransferTime
	,Submitted.WardStay52HospitalCode
	,Submitted.WardStay52WardCode
	,Submitted.WardStay53Sequence
	,Submitted.WardStay53TransferDate
	,Submitted.WardStay53TransferTime
	,Submitted.WardStay53HospitalCode
	,Submitted.WardStay53WardCode
	,Submitted.WardStay54Sequence
	,Submitted.WardStay54TransferDate
	,Submitted.WardStay54TransferTime
	,Submitted.WardStay54HospitalCode
	,Submitted.WardStay54WardCode
	,Submitted.WardStay55Sequence
	,Submitted.WardStay55TransferDate
	,Submitted.WardStay55TransferTime
	,Submitted.WardStay55HospitalCode
	,Submitted.WardStay55WardCode
	,Submitted.WardStay56Sequence
	,Submitted.WardStay56TransferDate
	,Submitted.WardStay56TransferTime
	,Submitted.WardStay56HospitalCode
	,Submitted.WardStay56WardCode
	,Submitted.WardStay57Sequence
	,Submitted.WardStay57TransferDate
	,Submitted.WardStay57TransferTime
	,Submitted.WardStay57HospitalCode
	,Submitted.WardStay57WardCode
	,Submitted.WardStay58Sequence
	,Submitted.WardStay58TransferDate
	,Submitted.WardStay58TransferTime
	,Submitted.WardStay58HospitalCode
	,Submitted.WardStay58WardCode
	,Submitted.WardStay59Sequence
	,Submitted.WardStay59TransferDate
	,Submitted.WardStay59TransferTime
	,Submitted.WardStay59HospitalCode
	,Submitted.WardStay59WardCode
	,Submitted.WardStay60Sequence
	,Submitted.WardStay60TransferDate
	,Submitted.WardStay60TransferTime
	,Submitted.WardStay60HospitalCode
	,Submitted.WardStay60WardCode
	,Submitted.WardStay61Sequence
	,Submitted.WardStay61TransferDate
	,Submitted.WardStay61TransferTime
	,Submitted.WardStay61HospitalCode
	,Submitted.WardStay61WardCode
	,Submitted.WardStay62Sequence
	,Submitted.WardStay62TransferDate
	,Submitted.WardStay62TransferTime
	,Submitted.WardStay62HospitalCode
	,Submitted.WardStay62WardCode
	,Submitted.WardStay63Sequence
	,Submitted.WardStay63TransferDate
	,Submitted.WardStay63TransferTime
	,Submitted.WardStay63HospitalCode
	,Submitted.WardStay63WardCode
	,Submitted.WardStay64Sequence
	,Submitted.WardStay64TransferDate
	,Submitted.WardStay64TransferTime
	,Submitted.WardStay64HospitalCode
	,Submitted.WardStay64WardCode
	,Submitted.WardStay65Sequence
	,Submitted.WardStay65TransferDate
	,Submitted.WardStay65TransferTime
	,Submitted.WardStay65HospitalCode
	,Submitted.WardStay65WardCode
	,Submitted.WardStay66Sequence
	,Submitted.WardStay66TransferDate
	,Submitted.WardStay66TransferTime
	,Submitted.WardStay66HospitalCode
	,Submitted.WardStay66WardCode
	,Submitted.WardStay67Sequence
	,Submitted.WardStay67TransferDate
	,Submitted.WardStay67TransferTime
	,Submitted.WardStay67HospitalCode
	,Submitted.WardStay67WardCode
	,Submitted.WardStay68Sequence
	,Submitted.WardStay68TransferDate
	,Submitted.WardStay68TransferTime
	,Submitted.WardStay68HospitalCode
	,Submitted.WardStay68WardCode
	,Submitted.WardStay69Sequence
	,Submitted.WardStay69TransferDate
	,Submitted.WardStay69TransferTime
	,Submitted.WardStay69HospitalCode
	,Submitted.WardStay69WardCode
	,Submitted.WardStay70Sequence
	,Submitted.WardStay70TransferDate
	,Submitted.WardStay70TransferTime
	,Submitted.WardStay70HospitalCode
	,Submitted.WardStay70WardCode
	,Submitted.WardStay71Sequence
	,Submitted.WardStay71TransferDate
	,Submitted.WardStay71TransferTime
	,Submitted.WardStay71HospitalCode
	,Submitted.WardStay71WardCode
	,Submitted.WardStay72Sequence
	,Submitted.WardStay72TransferDate
	,Submitted.WardStay72TransferTime
	,Submitted.WardStay72HospitalCode
	,Submitted.WardStay72WardCode
	,Submitted.WardStay73Sequence
	,Submitted.WardStay73TransferDate
	,Submitted.WardStay73TransferTime
	,Submitted.WardStay73HospitalCode
	,Submitted.WardStay73WardCode
	,Submitted.WardStay74Sequence
	,Submitted.WardStay74TransferDate
	,Submitted.WardStay74TransferTime
	,Submitted.WardStay74HospitalCode
	,Submitted.WardStay74WardCode
	,Submitted.NNNStatusIndicator
	,Submitted.LegalStatusClassificationCode
	,Submitted.HRGCode
	,Submitted.PCTofResidenceCode
	,Submitted.HRGVersionNumber
	,Submitted.DGVPCode
	,Submitted.LengthOfGestationAssessment
	,Submitted.CCP1Number
	,Submitted.CCP1LocalIdentifier
	,Submitted.CCP1StartDate
	,Submitted.CCP1UnitFunction
	,Submitted.CCP1AdvancedRespiratorySupportDays
	,Submitted.CCP1BasicRespiratorySupportDays
	,Submitted.CCP1AdvancedCardiovascularSupportDays
	,Submitted.CCP1BasicCardiovascularSupportDays
	,Submitted.CCP1RenalSupportDays
	,Submitted.CCP1NeurologicalSupportDays
	,Submitted.CCP1DermatologicalSupportDays
	,Submitted.CCP1LiverSupportDays
	,Submitted.CCP1Level2Days
	,Submitted.CCP1Level3Days
	,Submitted.CCP1DischargeDate
	,Submitted.CCP1DischargeTime
	,Submitted.CCP1MaximumNumberOfOrganSupportSystem
	,Submitted.CCP2Number
	,Submitted.CCP2LocalIdentifier
	,Submitted.CCP2StartDate
	,Submitted.CCP2UnitFunction
	,Submitted.CCP2AdvancedRespiratorySupportDays
	,Submitted.CCP2BasicRespiratorySupportDays
	,Submitted.CCP2AdvancedCardiovascularSupportDays
	,Submitted.CCP2BasicCardiovascularSupportDays
	,Submitted.CCP2RenalSupportDays
	,Submitted.CCP2NeurologicalSupportDays
	,Submitted.CCP2DermatologicalSupportDays
	,Submitted.CCP2LiverSupportDays
	,Submitted.CCP2Level2Days
	,Submitted.CCP2Level3Days
	,Submitted.CCP2DischargeDate
	,Submitted.CCP2DischargeTime
	,Submitted.CCP2MaximumNumberOfOrganSupportSystem
	,Submitted.CCP3Number
	,Submitted.CCP3LocalIdentifier
	,Submitted.CCP3StartDate
	,Submitted.CCP3UnitFunction
	,Submitted.CCP3AdvancedRespiratorySupportDays
	,Submitted.CCP3BasicRespiratorySupportDays
	,Submitted.CCP3AdvancedCardiovascularSupportDays
	,Submitted.CCP3BasicCardiovascularSupportDays
	,Submitted.CCP3RenalSupportDays
	,Submitted.CCP3NeurologicalSupportDays
	,Submitted.CCP3DermatologicalSupportDays
	,Submitted.CCP3LiverSupportDays
	,Submitted.CCP3Level2Days
	,Submitted.CCP3Level3Days
	,Submitted.CCP3DischargeDate
	,Submitted.CCP3DischargeTime
	,Submitted.CCP3MaximumNumberOfOrganSupportSystem
	,Submitted.CCP4Number
	,Submitted.CCP4LocalIdentifier
	,Submitted.CCP4StartDate
	,Submitted.CCP4UnitFunction
	,Submitted.CCP4AdvancedRespiratorySupportDays
	,Submitted.CCP4BasicRespiratorySupportDays
	,Submitted.CCP4AdvancedCardiovascularSupportDays
	,Submitted.CCP4BasicCardiovascularSupportDays
	,Submitted.CCP4RenalSupportDays
	,Submitted.CCP4NeurologicalSupportDays
	,Submitted.CCP4DermatologicalSupportDays
	,Submitted.CCP4LiverSupportDays
	,Submitted.CCP4Level2Days
	,Submitted.CCP4Level3Days
	,Submitted.CCP4DischargeDate
	,Submitted.CCP4DischargeTime
	,Submitted.CCP4MaximumNumberOfOrganSupportSystem
	,Submitted.CCP5Number
	,Submitted.CCP5LocalIdentifier
	,Submitted.CCP5StartDate
	,Submitted.CCP5UnitFunction
	,Submitted.CCP5AdvancedRespiratorySupportDays
	,Submitted.CCP5BasicRespiratorySupportDays
	,Submitted.CCP5AdvancedCardiovascularSupportDays
	,Submitted.CCP5BasicCardiovascularSupportDays
	,Submitted.CCP5RenalSupportDays
	,Submitted.CCP5NeurologicalSupportDays
	,Submitted.CCP5DermatologicalSupportDays
	,Submitted.CCP5LiverSupportDays
	,Submitted.CCP5Level2Days
	,Submitted.CCP5Level3Days
	,Submitted.CCP5DischargeDate
	,Submitted.CCP5DischargeTime
	,Submitted.CCP5MaximumNumberOfOrganSupportSystem
	,Submitted.CCP6Number
	,Submitted.CCP6LocalIdentifier
	,Submitted.CCP6StartDate
	,Submitted.CCP6UnitFunction
	,Submitted.CCP6AdvancedRespiratorySupportDays
	,Submitted.CCP6BasicRespiratorySupportDays
	,Submitted.CCP6AdvancedCardiovascularSupportDays
	,Submitted.CCP6BasicCardiovascularSupportDays
	,Submitted.CCP6RenalSupportDays
	,Submitted.CCP6NeurologicalSupportDays
	,Submitted.CCP6DermatologicalSupportDays
	,Submitted.CCP6LiverSupportDays
	,Submitted.CCP6Level2Days
	,Submitted.CCP6Level3Days
	,Submitted.CCP6DischargeDate
	,Submitted.CCP6DischargeTime
	,Submitted.CCP6MaximumNumberOfOrganSupportSystem
	,Submitted.CCP7Number
	,Submitted.CCP7LocalIdentifier
	,Submitted.CCP7StartDate
	,Submitted.CCP7UnitFunction
	,Submitted.CCP7AdvancedRespiratorySupportDays
	,Submitted.CCP7BasicRespiratorySupportDays
	,Submitted.CCP7AdvancedCardiovascularSupportDays
	,Submitted.CCP7BasicCardiovascularSupportDays
	,Submitted.CCP7RenalSupportDays
	,Submitted.CCP7NeurologicalSupportDays
	,Submitted.CCP7DermatologicalSupportDays
	,Submitted.CCP7LiverSupportDays
	,Submitted.CCP7Level2Days
	,Submitted.CCP7Level3Days
	,Submitted.CCP7DischargeDate
	,Submitted.CCP7DischargeTime
	,Submitted.CCP7MaximumNumberOfOrganSupportSystem
	,Submitted.CCP8Number
	,Submitted.CCP8LocalIdentifier
	,Submitted.CCP8StartDate
	,Submitted.CCP8UnitFunction
	,Submitted.CCP8AdvancedRespiratorySupportDays
	,Submitted.CCP8BasicRespiratorySupportDays
	,Submitted.CCP8AdvancedCardiovascularSupportDays
	,Submitted.CCP8BasicCardiovascularSupportDays
	,Submitted.CCP8RenalSupportDays
	,Submitted.CCP8NeurologicalSupportDays
	,Submitted.CCP8DermatologicalSupportDays
	,Submitted.CCP8LiverSupportDays
	,Submitted.CCP8Level2Days
	,Submitted.CCP8Level3Days
	,Submitted.CCP8DischargeDate
	,Submitted.CCP8DischargeTime
	,Submitted.CCP8MaximumNumberOfOrganSupportSystem
	,Submitted.CCP9Number
	,Submitted.CCP9LocalIdentifier
	,Submitted.CCP9StartDate
	,Submitted.CCP9UnitFunction
	,Submitted.CCP9AdvancedRespiratorySupportDays
	,Submitted.CCP9BasicRespiratorySupportDays
	,Submitted.CCP9AdvancedCardiovascularSupportDays
	,Submitted.CCP9BasicCardiovascularSupportDays
	,Submitted.CCP9RenalSupportDays
	,Submitted.CCP9NeurologicalSupportDays
	,Submitted.CCP9DermatologicalSupportDays
	,Submitted.CCP9LiverSupportDays
	,Submitted.CCP9Level2Days
	,Submitted.CCP9Level3Days
	,Submitted.CCP9DischargeDate
	,Submitted.CCP9DischargeTime
	,Submitted.CCP9MaximumNumberOfOrganSupportSystem
	,Submitted.AgeAtCDSActivityDate
	,Submitted.AgeOnAdmission
	,Submitted.DischargeReadyDate
	,Submitted.RTTPathwayID
	,Submitted.RTTCurrentProviderCode
	,Submitted.RTTPeriodStatusCode
	,Submitted.RTTStartDate
	,Submitted.RTTEndDate
	,Submitted.EarliestReasonableOfferDate
	,Submitted.NCCPPresent
	,Submitted.Baby1NHSNumberStatus
	,Submitted.Baby1NHSNumber
	,Submitted.Baby2NHSNumberStatus
	,Submitted.Baby2NHSNumber
	,Submitted.Baby3NHSNumberStatus
	,Submitted.Baby3NHSNumber
	,Submitted.Baby4NHSNumberStatus
	,Submitted.Baby4NHSNumber
	,Submitted.Baby5NHSNumberStatus
	,Submitted.Baby5NHSNumber
	,Submitted.Baby6NHSNumberStatus
	,Submitted.Baby6NHSNumber
	,Submitted.EndOfRecord
from
	CDS.APCAngliaSubmitted Submitted
where

--Only for this Financial Year

		(
			convert(date,EpisodeEndDate,103) >= (select dbo.f_First_of_Financial_Year(getdate()))
		or	convert(date,DischargeDate,103)  >= (select dbo.f_First_of_Financial_Year(getdate()))
		)

and
	not exists
	(
	select
		1
	from
		CDS.APCAngliaBase Base
	where
		Base.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	)

and	not exists
	(
	select
		1
	from
		CDS.APCAngliaSubmitted Previous
	where
		Previous.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	and	Previous.Created > Submitted.Created
	)

and	exists --latest update type was insert/update
	(
	select
		1
	from
		CDS.APCAngliaSubmitted Submitted9
	where
		Submitted9.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
	and	Submitted9.UpdateType = '9' --insert/update
	and	not exists
		(
		select
			1
		from
			CDS.APCAngliaSubmitted Previous
		where
			Previous.UniqueEpisodeSerialNo = Submitted.UniqueEpisodeSerialNo
		and	Previous.Created > Submitted9.Created
		)
	)







﻿CREATE TABLE [CDS].[MARSubmission] (
    [SubmissionDate]            DATE          NULL,
    [PCTCode]                   VARCHAR (10)  NULL,
    [Division]                  VARCHAR (50)  NULL,
    [NationalSpecialty]         VARCHAR (200) NULL,
    [ElectiveOrdinary]          BIGINT        NULL,
    [ElectiveDaycase]           BIGINT        NULL,
    [ElectiveOrdinaryPlanned]   BIGINT        NULL,
    [ElectiveDaycasePlanned]    BIGINT        NULL,
    [NHSTreatmentCentres]       BIGINT        NULL,
    [TotalNonElective]          BIGINT        NULL,
    [GPAllWrittenReferralsMade] BIGINT        NULL,
    [GPAllWrittenReferralsSeen] BIGINT        NULL,
    [GPGAWrittenReferralsMade]  BIGINT        NULL,
    [GPGAWrittenReferralsSeen]  BIGINT        NULL,
    [OtherReferralsMade]        BIGINT        NULL,
    [AllFirstOutpatients]       BIGINT        NULL,
    [RunDate]                   DATETIME      NULL
);


﻿CREATE TABLE [CDS].[ActivityYTD] (
    [ActivityDate]                DATE         NULL,
    [PatientCategoryCode]         VARCHAR (2)  NULL,
    [NationalFirstAttendanceCode] VARCHAR (10) NULL,
    [DirectorateCode]             VARCHAR (5)  NULL,
    [NationalSpecialtyCode]       VARCHAR (50) NULL,
    [Discharges]                  INT          NULL,
    [InOut]                       VARCHAR (10) NULL
);


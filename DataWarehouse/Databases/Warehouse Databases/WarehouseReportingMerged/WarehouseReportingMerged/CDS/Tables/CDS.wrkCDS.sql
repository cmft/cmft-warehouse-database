﻿CREATE TABLE [CDS].[wrkCDS] (
    [CDSRecno]            BIGINT      IDENTITY (1, 1) NOT NULL,
    [CDSTypeCode]         VARCHAR (3) NOT NULL,
    [MergeEncounterRecno] INT         NOT NULL,
    CONSTRAINT [PK_wrkCDS] PRIMARY KEY CLUSTERED ([CDSRecno] ASC)
);


﻿CREATE TABLE [CDS].[AETreatmentDistinct] (
    [ContextCode]      VARCHAR (10)  NOT NULL,
    [AESourceUniqueID] VARCHAR (50)  NOT NULL,
    [TreatmentCode]    VARCHAR (6)   NULL,
    [TreatmentDate]    SMALLDATETIME NULL,
    [SequenceNo]       BIGINT        NOT NULL,
    CONSTRAINT [PK_TreatmentDistinct] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [AESourceUniqueID] ASC, [SequenceNo] ASC)
);


﻿CREATE procedure [CDS].[BuildAEAngliaSubmitted]
	@SubmittedDate date = null
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @Created datetime

select @StartTime = getdate()

select
	@SubmittedDate =
		coalesce(
			 @SubmittedDate
			,getdate()
		)

	,@Created = GETDATE()

insert
into
	CDS.AEAngliaSubmitted
(
	 PrimeRecipient
	,CopyRecipient1
	,CopyRecipient2
	,CopyRecipient3
	,CopyRecipient4
	,CopyRecipient5
	,Sender
	,CDSGroup
	,CDSType
	,CDSId
	,TestFlag
	,DatetimeCreated
	,UpdateType
	,ProtocolIdentifier
	,BulkStart
	,BulkEnd
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientName
	,PatientAddress
	,Postcode
	,PCTofResidence
	,DateOfBirth
	,SexCode
	,CarerSupportIndicator
	,EthnicCategoryCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,DepartmentTypeCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,ICDDiagnosisSchemeCode
	,ICDDiagnosisCodeFirst
	,ICDDiagnosisCodeSecond
	,ReadDiagnosisSchemeCode
	,ReadDiagnosisCodeFirst
	,ReadDiagnosisCodeSecond
	,DiagnosisSchemeCode
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,DiagnosisCode1
	,DiagnosisCode2
	,DiagnosisCode3
	,DiagnosisCode4
	,DiagnosisCode5
	,DiagnosisCode6
	,DiagnosisCode7
	,DiagnosisCode8
	,DiagnosisCode9
	,DiagnosisCode10
	,DiagnosisCode11
	,DiagnosisCode12
	,InvestigationSchemeCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,InvestigationCode1
	,InvestigationCode2
	,InvestigationCode3
	,InvestigationCode4
	,InvestigationCode5
	,InvestigationCode6
	,InvestigationCode7
	,InvestigationCode8
	,InvestigationCode9
	,InvestigationCode10
	,OPCSProcedureSchemeCode
	,OPCSPrimaryProcedureCode
	,OPCSPrimaryProcedureDate
	,OPCSPrimaryProcedureCodeSecond
	,OPCSPrimaryProcedureDateSecond
	,ReadProcedureSchemeCode
	,ReadPrimaryProcedureCode
	,ReadPrimaryProcedureDate
	,ReadPrimaryProcedureCodeSecond
	,ReadPrimaryProcedureDateSecond
	,TreatmentSchemeCode
	,TreatmentCodeFirst
	,TreatmentDateFirst
	,TreatmentCodeSecond
	,TreatmentDateSecond
	,TreatmentCode1
	,TreatmentDate1
	,TreatmentCode2
	,TreatmentDate2
	,TreatmentCode3
	,TreatmentDate3
	,TreatmentCode4
	,TreatmentDate4
	,TreatmentCode5
	,TreatmentDate5
	,TreatmentCode6
	,TreatmentDate6
	,TreatmentCode7
	,TreatmentDate7
	,TreatmentCode8
	,TreatmentDate8
	,TreatmentCode9
	,TreatmentDate9
	,TreatmentCode10
	,TreatmentDate10
	,HRGCode
	,HRGVersionCode
	,HRGProcedureSchemeCode
	,DGVPCode
	,SubmittedDate
	,Created
	,ByWhom
)
select
	 Submitted.PrimeRecipient
	,Submitted.CopyRecipient1
	,Submitted.CopyRecipient2
	,Submitted.CopyRecipient3
	,Submitted.CopyRecipient4
	,Submitted.CopyRecipient5
	,Submitted.Sender
	,Submitted.CDSGroup
	,Submitted.CDSType
	,Submitted.CDSId
	,Submitted.TestFlag
	,Submitted.DatetimeCreated
	,Submitted.UpdateType
	,Submitted.ProtocolIdentifier
	,Submitted.BulkStart
	,Submitted.BulkEnd
	,Submitted.UniqueBookingReferenceNo
	,Submitted.PathwayId
	,Submitted.PathwayIdIssuerCode
	,Submitted.RTTStatusCode
	,Submitted.RTTStartDate
	,Submitted.RTTEndDate
	,Submitted.DistrictNo
	,Submitted.DistrictNoOrganisationCode
	,Submitted.NHSNumber
	,Submitted.NHSNumberStatusId
	,Submitted.PatientName
	,Submitted.PatientAddress
	,Submitted.Postcode
	,Submitted.PCTofResidence
	,Submitted.DateOfBirth
	,Submitted.SexCode
	,Submitted.CarerSupportIndicator
	,Submitted.EthnicCategoryCode
	,Submitted.RegisteredGpCode
	,Submitted.RegisteredGpPracticeCode
	,Submitted.AttendanceNumber
	,Submitted.ArrivalModeCode
	,Submitted.AttendanceCategoryCode
	,Submitted.AttendanceDisposalCode
	,Submitted.IncidentLocationTypeCode
	,Submitted.PatientGroupCode
	,Submitted.SourceOfReferralCode
	,Submitted.DepartmentTypeCode
	,Submitted.ArrivalDate
	,Submitted.ArrivalTime
	,Submitted.AgeOnArrival
	,Submitted.InitialAssessmentTime
	,Submitted.SeenForTreatmentTime
	,Submitted.AttendanceConclusionTime
	,Submitted.DepartureTime
	,Submitted.CommissioningSerialNo
	,Submitted.NHSServiceAgreementLineNo
	,Submitted.ProviderReferenceNo
	,Submitted.CommissionerReferenceNo
	,Submitted.ProviderCode
	,Submitted.CommissionerCode
	,Submitted.StaffMemberCode
	,Submitted.ICDDiagnosisSchemeCode
	,Submitted.ICDDiagnosisCodeFirst
	,Submitted.ICDDiagnosisCodeSecond
	,Submitted.ReadDiagnosisSchemeCode
	,Submitted.ReadDiagnosisCodeFirst
	,Submitted.ReadDiagnosisCodeSecond
	,Submitted.DiagnosisSchemeCode
	,Submitted.DiagnosisCodeFirst
	,Submitted.DiagnosisCodeSecond
	,Submitted.DiagnosisCode1
	,Submitted.DiagnosisCode2
	,Submitted.DiagnosisCode3
	,Submitted.DiagnosisCode4
	,Submitted.DiagnosisCode5
	,Submitted.DiagnosisCode6
	,Submitted.DiagnosisCode7
	,Submitted.DiagnosisCode8
	,Submitted.DiagnosisCode9
	,Submitted.DiagnosisCode10
	,Submitted.DiagnosisCode11
	,Submitted.DiagnosisCode12
	,Submitted.InvestigationSchemeCode
	,Submitted.InvestigationCodeFirst
	,Submitted.InvestigationCodeSecond
	,Submitted.InvestigationCode1
	,Submitted.InvestigationCode2
	,Submitted.InvestigationCode3
	,Submitted.InvestigationCode4
	,Submitted.InvestigationCode5
	,Submitted.InvestigationCode6
	,Submitted.InvestigationCode7
	,Submitted.InvestigationCode8
	,Submitted.InvestigationCode9
	,Submitted.InvestigationCode10
	,Submitted.OPCSProcedureSchemeCode
	,Submitted.OPCSPrimaryProcedureCode
	,Submitted.OPCSPrimaryProcedureDate
	,Submitted.OPCSPrimaryProcedureCodeSecond
	,Submitted.OPCSPrimaryProcedureDateSecond
	,Submitted.ReadProcedureSchemeCode
	,Submitted.ReadPrimaryProcedureCode
	,Submitted.ReadPrimaryProcedureDate
	,Submitted.ReadPrimaryProcedureCodeSecond
	,Submitted.ReadPrimaryProcedureDateSecond
	,Submitted.TreatmentSchemeCode
	,Submitted.TreatmentCodeFirst
	,Submitted.TreatmentDateFirst
	,Submitted.TreatmentCodeSecond
	,Submitted.TreatmentDateSecond
	,Submitted.TreatmentCode1
	,Submitted.TreatmentDate1
	,Submitted.TreatmentCode2
	,Submitted.TreatmentDate2
	,Submitted.TreatmentCode3
	,Submitted.TreatmentDate3
	,Submitted.TreatmentCode4
	,Submitted.TreatmentDate4
	,Submitted.TreatmentCode5
	,Submitted.TreatmentDate5
	,Submitted.TreatmentCode6
	,Submitted.TreatmentDate6
	,Submitted.TreatmentCode7
	,Submitted.TreatmentDate7
	,Submitted.TreatmentCode8
	,Submitted.TreatmentDate8
	,Submitted.TreatmentCode9
	,Submitted.TreatmentDate9
	,Submitted.TreatmentCode10
	,Submitted.TreatmentDate10
	,Submitted.HRGCode
	,Submitted.HRGVersionCode
	,Submitted.HRGProcedureSchemeCode
	,Submitted.DGVPCode

	,SubmittedDate = @SubmittedDate
	,Created = @Created
	,ByWhom = system_user
from
	CDS.AEAngliaNet Submitted

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@SubmittedDate, ''))

exec Warehouse.dbo.WriteAuditLogEvent 'CDS.BuildAEAngliaSubmitted', @Stats, @StartTime

print @Stats

﻿


--20140915 exclude non reportable
--20150514 RR added Paediatric Critical Care section from Phils CDS62 work

CREATE procedure [CDS].[BuildAngliaType010]  --FCE
	 @CDSTestIndicator char(1) = ''
	,@CDSProtocolIdentifier char(3) = '020' -- bulk
as

--declare @CDSProtocolIdentifier char(3) = '010'
declare @CDSReportPeriodStartDate smalldatetime
declare @CDSReportPeriodEndDate smalldatetime

declare @CDSProtocolIdentifierLocal char(3) = coalesce(@CDSProtocolIdentifier, '020')

truncate table CDS.APCAngliaBase

--Insert into temp table before copying into CDS.APCAnglia
select --TOP 100
	  Encounter.MergeEncounterRecno
	 ,UniqueEpisodeSerialNo =
		left(
			'BMCHT ' +
			replace(
				 Encounter.SourceUniqueID
				,'||'
				,'*'
			)
			
			,50
		)

	,UpdateType = '9' --all records are either new or updated

	,CDSUpdateDate =
		left(
			CONVERT(varchar, getdate(), 112)
			,8
		)

	,CDSUpdateTime =
		left(
			replace(
				 CONVERT(varchar, getdate(), 108)
				,':'
				,''
			)
			,6
		)

	,CDSType = 
		wrkCDS.CDSTypeCode
	
	,RecordType = 
		case wrkCDS.CDSTypeCode
			when '120' then '33'
			when '130' then '13'
			when '140' then '23'
		end

	,ProviderCode = 
		'RW300'
	
	,PurchaserCode = 
		--Added as temp fix for Welsh patients
		--Gareth Cunnah 18/02/2012
		case
		--Added as temp fix for Welsh patients
		when left(Encounter.PurchaserCode , 1) = '7'  then '7A5HC'
		--Specialist commissioning
		when left(Encounter.PurchaserCode , 1)  = 'Q' then Encounter.PurchaserCode
		--else left(Encounter.PurchaserCode , 5)
		else Encounter.CCGCode
		end

	,CommissioningSerialNo =
		left(
			Encounter.ContractSerialNo
			,6
		)

	,NHSServiceAgreementLineNo = 

			--Added by gareth Cunnah 16/08/2013
			--specialised commissioning
	
		Case 
			when left(Encounter.PurchaserCode,1) = 'Q' then  Encounter.CCGCode
			else null
		end

	,PurchaserReferenceNo = '8' --not known

	,NHSNumber =
		left(
			replace(
				 Encounter.NHSNumber
				,' '
				,''
			)
			,17
		)
	,PatientName = 
		left(
			case
				when NHSNumberStatus.NationalValueCode <> '01'  AND  Encounter.NHSNumber IS NULL
				then ltrim(rtrim(coalesce(Encounter.PatientForename, '') + ' ' + coalesce(Encounter.PatientSurname, '')))
				else null
			end
			,70
		)

	,PatientAddress =
		left(
			case
			when NHSNumberStatus.NationalValueCode <> '01' AND  Encounter.NHSNumber IS NULL
			then
				coalesce(Encounter.PatientAddress1 + ' ', '') + 
				coalesce(Encounter.PatientAddress2 + ' ', '') + 
				coalesce(Encounter.PatientAddress3 + ' ', '') + 
				coalesce(Encounter.PatientAddress4, '')
			else null
			end
			,175
		)

	,Postcode = 
			case
				when Encounter.Postcode IS NULL THEN 'ZZ99 3WZ'
				when ltrim(rtrim(Encounter.Postcode)) =  '' THEN 'ZZ99 3WZ'
				when len(Encounter.Postcode) = 8 then Encounter.Postcode
				else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
			end
	

	,SexCode = 
		Left(
			case
				when Sex.NationalValueCode = 'N||SEX' then '9'
				else Sex.NationalValueCode 
			end	
			,1
		)
	
	,CarerSupportIndicator = 
		Left(
			case
				when CarerSupportIndicator.NationalValueCode = 'N||CARSUP' then ''
				else CarerSupportIndicator.NationalValueCode
			end	
			,2
		)
		

	,DateOfBirth =
		left(
			CONVERT(varchar, Encounter.DateOfBirth, 112)
			,8
		)

	,RegisteredGpCode = 
		left(
			COALESCE(
				Encounter.RegisteredGpCode
				,'G9999998'
			)
			,10
		)

	,RegisteredGpPracticeCode =
		left(
			COALESCE(
				Encounter.RegisteredGpPracticeCode
				,'V81999'
			)
			,6
		)

	,DistrictNo =
		left(
			Encounter.DistrictNo
			,10
		)

	,EthnicGroupCode =  
		left(
			EthnicCategory.NationalValueCode
			,2
		)
			
	--,ReferrerCode  = Encounter.ReferredByCode --MISSING FROM BASE TABLE GS
		--coalesce(
		--	case
		--	when Encounter.ReferredByCode = 'GP'
		--	then Encounter.ReferrerCode

		--	when Encounter.ReferredByCode = 'GDP'
		--	then NULL --ReferringGdp.NationalCode

		--	when Encounter.ReferredByCode = 'CONS'
		--	then NULL --ReferringConsultant.NationalConsultantCode

		--	else 'X9999998'
		--	end

		--	,'X9999998'
		--)
		
	,ReferrerCode = 
		coalesce(
			case
			when Encounter.ReferredByCode = 'GP' then convert(varchar, EncounterReference.ReferrerID)
			when Encounter.ReferredByCode = 'GDP' then convert(varchar, EncounterReference.ReferrerID)
			when Encounter.ReferredByCode = 'CON' then 
				case 
				when convert(varchar,ReferrerConsultant.NationalConsultantCode) = 'N||CONSUL' then 'C9999998'
				else convert(varchar,ReferrerConsultant.NationalConsultantCode)
				end
			else 'X9999998'
			end
			,'X9999998'
		)


	--,ReferringOrganisationCode = --MISSING FROM BASE TABLE GS
	--	left(
	--		coalesce(
	--			case
	--			when Encounter.ReferredByCode = 'GP'
	--			then NULL --ReferringOrganisationCode

	--			when Encounter.ReferredByCode = 'GDP'
	--			then 'V81998'

	--			when Encounter.ReferredByCode = 'CONS'
	--			then NULL --ReferringConsultantOrganisation.NationalCode

	--			else 'X99998'
	--			end

	--			,'X99998'
	--		)
	--		,6
	--	)
		
	,ReferringOrganisationCode =
		left(
			coalesce(
				case
				when Encounter.ReferredByCode = 'GP'
					then ReferringGpOrganisation.Parentorganisationcode
				when Encounter.ReferredByCode = 'GDP'
					then 'V81998'
					--then ReferringGDpOrganisation.Parentorganisationcode
				when Encounter.ReferredByCode = 'CON'
					then ReferrerConsultant.ProviderCode
				else 'X99998'
				end
				,'X99998'
			)
			,6
		)

	,ProviderSpellNo =
		left(
			Replace(
				Encounter.GlobalProviderSpellNo
				,'/'
				,'*'
			)
			,12
		)

	,PatientCategoryCode =
		AdminCategory.NationalValueCode -- DG checked with Tim
		--left(
		--	PatientCategoryCode
			
		--	,2
		--)

	,DecidedToAdmitDate =
		left(
			CONVERT(varchar, Encounter.DecidedToAdmitDate, 112)
			,8
		)

	,AdmissionDate =
		left(
			CONVERT(varchar, CalendarAdmissionDate.TheDate, 112)
			,8
		)

	,AdmissionMethodCode = 
		left(
			AdmissionMethod.NationalValueCode
			,2
		)

	,DurationOfElectiveWait = 
		left(
			CASE 
				WHEN AdmissionMethod.NationalValueCode IN ('11','12','13') THEN
				COALESCE(
					 
						Right(
							'0000' + 
							replace(
								ltrim(
									rtrim(
										 CONVERT(varchar, Encounter.DurationofElectiveWait,4)
									)
								)
								,' '
								, ''
							)
						,4
						)
					 
							
					
					,'9999'
				)
				ELSE '9998'
			END
			,4
		)

	,ManagementIntentionCode =
		left(
			ManagementIntention.NationalValueCode
			,1
		)

	,SourceOfAdmissionCode = 
		left(
			AdmissionSource.NationalValueCode
			,2
		)

	,DischargeDate =
		left(
			CONVERT(varchar, CalendarDischargeDate.TheDate, 112)
			,8
		)

	,DischargeMethodCode = 
		left(
			DischargeMethod.NationalValueCode
			, 1
		)

	,DischargeDestinationCode = 
		left(
			DischargeDestination.NationalValueCode
			,2
		)

	,PatientClassificationCode = 
		left(
			PatientClassification.NationalValueCode
			,1
		)

	,EpisodeNo = 
		left(
			Encounter.GlobalEpisodeNo
			,2
		)

	,LastEpisodeInSpellIndicator = 
		case
		when Encounter.MergeEncounterRecno = Discharge.MergeEncounterRecno
		then '1'
		else '2'
		end

		--left(
		--	LastEpisodeInSpell.NationalValueCode
		--	,1
		--)

	,StartSiteCode =
		left(
			StartSite.NationalValueCode
			,5
		)

	,StartWardTypeCode = null --not required according to local CDS definition

	,EpisodeStartDate =  
		left(
			CONVERT(varchar, CalendarEpisodeStartDate.TheDate, 112)
			,8
		)

	,EpisodeEndDate =
		left(
			CONVERT(varchar, CalendarEpisodeEndDate.TheDate, 112)
			,8
		)
	,MainSpecialtyCode = --MAP TO CONSULTANT SPEC
		left(
			ConsultantSpecialty.MainSpecialtyCode
			,3
		)
	
	,TreatmentFunctionCode = 
		left(
			Specialty.NationalValueCode
			,3
		)

	
			
	,ConsultantCode = 
		left(
			COALESCE(
				Consultant.NationalValueCode
				,'C9999998'
			)
			,8
		)
	
	,FirstRegularDayNightAdmission =
		left(
			FirstRegularDayNightAdmission.NationalValueCode
			,1
		)

	,NeonatalLevelOfCare =
		left(
			NeonatalLevelOfCare.NationalValueCode
			,1
		)

	,PsychiatricPatientStatusCode = 
		COALESCE (
					left(
						Encounter.PsychiatricPatientStatusCode
						,1
					)
				,	CASE 
						WHEN Specialty.NationalValueCode = '711' THEN 9
						ELSE 8
					END
		)

	,PrimaryDiagnosisCode = 
							case
								when Encounter.PrimaryDiagnosisCode = '##'
								then null
								else left(REPLACE(Encounter.PrimaryDiagnosisCode, '.', ''), 6)
							end
	,SecondaryDiagnosisCode1 = left(REPLACE(Encounter.SecondaryDiagnosisCode1, '.', ''), 6)
	,SecondaryDiagnosisCode2 = left(REPLACE(Encounter.SecondaryDiagnosisCode2, '.', ''), 6)
	,SecondaryDiagnosisCode3 = left(REPLACE(Encounter.SecondaryDiagnosisCode3, '.', ''), 6)
	,SecondaryDiagnosisCode4 = left(REPLACE(Encounter.SecondaryDiagnosisCode4, '.', ''), 6)
	,SecondaryDiagnosisCode5 = left(REPLACE(Encounter.SecondaryDiagnosisCode5, '.', ''), 6)
	,SecondaryDiagnosisCode6 = left(REPLACE(Encounter.SecondaryDiagnosisCode6, '.', ''), 6)
	,SecondaryDiagnosisCode7 = left(REPLACE(Encounter.SecondaryDiagnosisCode7, '.', ''), 6)
	,SecondaryDiagnosisCode8 = left(REPLACE(Encounter.SecondaryDiagnosisCode8, '.', ''), 6)
	,SecondaryDiagnosisCode9 = left(REPLACE(Encounter.SecondaryDiagnosisCode9, '.', ''), 6)
	,SecondaryDiagnosisCode10 = left(REPLACE(Encounter.SecondaryDiagnosisCode10, '.', ''), 6)
	,SecondaryDiagnosisCode11 = left(REPLACE(Encounter.SecondaryDiagnosisCode11, '.', ''), 6)
	,SecondaryDiagnosisCode12 = left(REPLACE(Encounter.SecondaryDiagnosisCode12, '.', ''), 6)

	,OperationStatus =

		--changed Gareth Cunnah 18/02/2012
			case
				when Encounter.PrimaryProcedureCode is null then 8
				when Encounter.PrimaryProcedureCode <> '##' then 1
			else 8
			end
		--case
		--	when PrimaryProcedureCode is null then 8
		--	when 
		--		case
		--			when PrimaryProcedureCode = '##'
		--			then null
		--			else left(replace(PrimaryProcedureCode, '.', ''), 4)
		--		end = null then 8
		--	else 1
		--end

	,PrimaryOperationCode = 
							case
								when Encounter.PrimaryProcedureCode = '##'
								then null
								else left(replace(Encounter.PrimaryProcedureCode, '.', ''), 4)
							end
	,PrimaryOperationDate = left(CONVERT(varchar, Encounter.PrimaryProcedureDate, 112), 8)
	,SecondaryOperationCode1 = left(replace(Encounter.SecondaryProcedureCode1, '.', ''), 4)
	,SecondaryOperationDate1 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate1, 112), 8)
	,SecondaryOperationCode2 = left(replace(Encounter.SecondaryProcedureCode2, '.', ''), 4)
	,SecondaryOperationDate2 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate2, 112), 8)
	,SecondaryOperationCode3 = left(replace(Encounter.SecondaryProcedureCode3, '.', ''), 4)
	,SecondaryOperationDate3 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate3, 112), 8)
	,SecondaryOperationCode4 = left(replace(Encounter.SecondaryProcedureCode4, '.', ''), 4)
	,SecondaryOperationDate4 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate4, 112), 8)
	,SecondaryOperationCode5 = left(replace(Encounter.SecondaryProcedureCode5, '.', ''), 4)
	,SecondaryOperationDate5 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate5, 112), 8)
	,SecondaryOperationCode6 = left(replace(Encounter.SecondaryProcedureCode6, '.', ''), 4)
	,SecondaryOperationDate6 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate6, 112), 8)
	,SecondaryOperationCode7 = left(replace(Encounter.SecondaryProcedureCode7, '.', ''), 4)
	,SecondaryOperationDate7 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate7, 112), 8)
	,SecondaryOperationCode8 = left(replace(Encounter.SecondaryProcedureCode8, '.', ''), 4)
	,SecondaryOperationDate8 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate8, 112), 8)
	,SecondaryOperationCode9 = left(replace(Encounter.SecondaryProcedureCode9, '.', ''), 4)
	,SecondaryOperationDate9 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate9, 112), 8)
	,SecondaryOperationCode10 = left(replace(Encounter.SecondaryProcedureCode10, '.', ''), 4)
	,SecondaryOperationDate10 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate10, 112), 8)
	,SecondaryOperationCode11 = left(replace(Encounter.SecondaryProcedureCode11, '.', ''), 4)
	,SecondaryOperationDate11 = left(CONVERT(varchar, Encounter.SecondaryProcedureDate11, 112), 8)

	,EpisodeStartWardTypeCode = null --not required according to local CDS definition

	,EndWardTypeCode = null --not required according to local CDS definition

	,EndSiteCode = 
		left(
			EndSite.NationalValueCode
			,5
		)

--need to find this in inquire
	,AnteNatalGpCode = null

	,FirstAnteNatalAssessmentDate =
		left(CONVERT(varchar, coalesce(Delivery.FirstAnteNatalAssessmentDate , BirthDelivery.FirstAnteNatalAssessmentDate) , 112), 8)

	,PreviousPregnancies =
		coalesce(Delivery.PreviousPregnancies , BirthDelivery.PreviousPregnancies)

	,ActualDeliveryPlaceTypeCode =
		coalesce(Delivery.ActualDeliveryPlaceTypeCode , BirthDelivery.ActualDeliveryPlaceTypeCode)

	,IntendedDeliveryPlaceTypeCode =
		coalesce(Delivery.IntendedDeliveryPlaceTypeCode , BirthDelivery.IntendedDeliveryPlaceTypeCode)

	,DeliveryPlaceChangeReasonCode =
		coalesce(Delivery.DeliveryPlaceChangeReasonCode , BirthDelivery.DeliveryPlaceChangeReasonCode)

	,GestationLength =
		coalesce(Delivery.GestationLength , BirthDelivery.GestationLength)

	,LabourOnsetMethodCode = 
		coalesce(Delivery.LabourOnsetMethodCode , BirthDelivery.LabourOnsetMethodCode)

	,StatusOfPersonConductingDeliveryCode =
		case coalesce(Delivery.StatusOfPersonConductingDeliveryCode , BirthDelivery.StatusOfPersonConductingDeliveryCode)
		when '4' then '8' 
		when '5' then '8' 
		else coalesce(Delivery.StatusOfPersonConductingDeliveryCode , BirthDelivery.StatusOfPersonConductingDeliveryCode)
		end

	,AnaestheticGivenDuringCode =
		left(
			coalesce(Delivery.AnaestheticGivenDuringCode , BirthDelivery.AnaestheticGivenDuringCode)
			,1
		)

--need to find this in inquire
	,AnaestheticGivenPostCode = null

	,NumberOfBabies =
		case
		when wrkCDS.CDSTypeCode = '120' then 1
		when
			(
			select
				count(*)
			from
				Warehouse.PAS.Birth
			where
				Birth.SourcePatientNo = Encounter.SourcePatientNo
			and	Birth.SourceSpellNo = Encounter.SourceSpellNo
			) = 0 then null
		else
			(
			select
				count(*)
			from
				Warehouse.PAS.Birth
			where
				Birth.SourcePatientNo = Encounter.SourcePatientNo
			and	Birth.SourceSpellNo = Encounter.SourceSpellNo
			)
		end

	,Baby1DeliveryDate = left(convert(varchar, coalesce(Baby1.DeliveryDate , Birth.DeliveryDate) , 112), 8)
	,Baby1DeliveryMethodCode = coalesce(Baby1DeliveryMethod.NationalCode , BirthDeliveryMethod.NationalCode)
	,Baby1SexCode = case coalesce(Baby1Patient.SexCode , BirthPatient.SexCode) when 'M' then '1' when 'F' then '2' when 'I' then '8' else null end
	,Baby1BirthOrder = coalesce(Baby1.BirthOrder , Birth.BirthOrder)
	,Baby1LiveStillBirth = CASE coalesce(Baby1.LiveStillBirth , Birth.LiveStillBirth) when '0' then '1' when '1' then '2'when '2' then '3'when '3' then '4' end
	,Baby1BirthWeight = CASE coalesce(Baby1.BirthWeight , Birth.BirthWeight) WHEN 'U' THEN NULL ELSE coalesce(Baby1.BirthWeight , Birth.BirthWeight) END

--how to derive
	,Baby1MethodOfResuscitationCode = null

	,Baby1BirthDate = convert(varchar, coalesce(Baby1Patient.DateOfBirth , BirthPatient.DateOfBirth) , 112)
	
	,Baby1MotherBirthDate =
		convert(
			varchar,
				case
				when Baby1.BirthID is not null then Encounter.DateOfBirth
				else BirthMother.DateOfBirth
				end
			,112
		)

	,Baby2DeliveryDate = left(convert(varchar, Baby2.DeliveryDate, 112), 8)
	,Baby2DeliveryMethodCode = Baby2DeliveryMethod.NationalCode
	,Baby2SexCode = case Baby2Patient.SexCode when 'M' then '1' when 'F' then '2' when 'I' then '8' else null end
	,Baby2BirthOrder = Baby2.BirthOrder
	,Baby2LiveStillBirth = CASE Baby2.LiveStillBirth when '0' then '1' when '1' then '2'when '2' then '3'when '3' then '4'  end
	,Baby2BirthWeight = CASE Baby2.BirthWeight WHEN 'U' THEN NULL ELSE Baby2.BirthWeight END

--how to derive
	,Baby2MethodOfResuscitationCode = null

	,Baby2BirthDate = convert(varchar, Baby2Patient.DateOfBirth, 112)
	,Baby2MotherBirthDate = case when Baby2.BirthID is null then null else convert(varchar, Encounter.DateOfBirth, 112) end

	,Baby3DeliveryDate = left(convert(varchar, Baby3.DeliveryDate, 112), 8)
	,Baby3DeliveryMethodCode = Baby3DeliveryMethod.NationalCode
	,Baby3SexCode = case Baby3Patient.SexCode when 'M' then '1' when 'F' then '2' when 'I' then '8' else null end
	,Baby3BirthOrder = Baby3.BirthOrder
	,Baby3LiveStillBirth = CASE Baby3.LiveStillBirth when '0' then '1' when '1' then '2'when '2' then '3'when '3' then '4'  end
	,Baby3BirthWeight = CASE Baby3.BirthWeight WHEN 'U' THEN NULL ELSE Baby3.BirthWeight END

--how to derive
	,Baby3MethodOfResuscitationCode = null

	,Baby3BirthDate = convert(varchar, Baby3Patient.DateOfBirth, 112)
	,Baby3MotherBirthDate = case when Baby3.BirthID is null then null else convert(varchar, Encounter.DateOfBirth, 112) end

	,Baby4DeliveryDate = left(convert(varchar, Baby4.DeliveryDate, 112), 8)
	,Baby4DeliveryMethodCode = Baby4DeliveryMethod.NationalCode
	,Baby4SexCode = case Baby4Patient.SexCode when 'M' then '1' when 'F' then '2' when 'I' then '8' else null end
	,Baby4BirthOrder = Baby4.BirthOrder
	,Baby4LiveStillBirth = CASE Baby4.LiveStillBirth when '0' then '1' when '1' then '2'when '2' then '3'when '3' then '4'  end
	,Baby4BirthWeight = CASE Baby4.BirthWeight WHEN 'U' THEN NULL ELSE Baby4.BirthWeight END

--how to derive
	,Baby4MethodOfResuscitationCode = null

	,Baby4BirthDate = convert(varchar, Baby4Patient.DateOfBirth, 112)
	,Baby4MotherBirthDate = case when Baby4.BirthID is null then null else convert(varchar, Encounter.DateOfBirth, 112) end

	,Baby5DeliveryDate = left(convert(varchar, Baby5.DeliveryDate, 112), 8)
	,Baby5DeliveryMethodCode = Baby5DeliveryMethod.NationalCode
	,Baby5SexCode = case Baby5Patient.SexCode when 'M' then '1' when 'F' then '2' when 'I' then '8' else null end
	,Baby5BirthOrder = Baby5.BirthOrder
	,Baby5LiveStillBirth = CASE Baby5.LiveStillBirth when '0' then '1' when '1' then '2'when '2' then '3'when '3' then '4'  end
	,Baby5BirthWeight = CASE Baby5.BirthWeight WHEN 'U' THEN NULL ELSE Baby5.BirthWeight END

--how to derive
	,Baby5MethodOfResuscitationCode = null

	,Baby5BirthDate = convert(varchar, Baby5Patient.DateOfBirth, 112)
	,Baby5MotherBirthDate = case when Baby5.BirthID is null then null else convert(varchar, Encounter.DateOfBirth, 112) end

	,Baby6DeliveryDate = left(convert(varchar, Baby6.DeliveryDate, 112), 8)
	,Baby6DeliveryMethodCode = Baby6DeliveryMethod.NationalCode
	,Baby6SexCode = case Baby6Patient.SexCode when 'M' then '1' when 'F' then '2' when 'I' then '8' else null end
	,Baby6BirthOrder = Baby6.BirthOrder
	,Baby6LiveStillBirth = CASE Baby6.LiveStillBirth when '0' then '1' when '1' then '2'when '2' then '3'when '3' then '4'  end
	,Baby6BirthWeight = CASE Baby6.BirthWeight WHEN 'U' THEN NULL ELSE Baby6.BirthWeight END

--how to derive
	,Baby6MethodOfResuscitationCode = null

	,Baby6BirthDate = convert(varchar, Baby6Patient.DateOfBirth, 112)
	,Baby6MotherBirthDate = case when Baby6.BirthID is null then null else convert(varchar, Encounter.DateOfBirth, 112) end

	,NNNStatusIndicator =
		left(
			case
				when NHSNumberStatus.NationalValueCode = 'N||NHSNST' then '03'
				else NHSNumberStatus.NationalValueCode
			end
			,2
		)

	,LegalStatusClassificationCode = 
		COALESCE (
						left(
							Encounter.LegalStatusClassificationCode
							,2
						)
				,	CASE 
						WHEN Specialty.NationalValueCode = '711' THEN '09'
						ELSE '08'
					END
		)


	,HRGCode = null --HRG4Encounter.HRGCode --optional on CDS

	,PCTofResidenceCode =
		left(
			coalesce(
				--Postcode.PCTCode
				Postcode.CCGCode
				,'X98'
			)
			,5
		)

	,HRGVersionNumber = null --'4.4' --optional on CDS

--	,DGVPCode = replace(HRG4Encounter.DominantOperationCode, '.', '')

	,LengthOfGestationAssessment = Delivery.GestationLength --not sure about this

	,AgeAtCDSActivityDate =
		datediff(year, Encounter.DateOfBirth, CalendarEpisodeEndDate.TheDate) -
		case
		when datepart(day, Encounter.DateOfBirth) = datepart(day, CalendarEpisodeEndDate.TheDate)
		and	datepart(month, Encounter.DateOfBirth) = datepart(month,CalendarEpisodeEndDate.TheDate)
		and datepart(year, Encounter.DateOfBirth) <> datepart(year,CalendarEpisodeEndDate.TheDate)
		then 0
		else 
			case
			when Encounter.DateOfBirth > CalendarEpisodeEndDate.TheDate then 0
			when datepart(dy, Encounter.DateOfBirth) > datepart(dy, CalendarEpisodeEndDate.TheDate) 
			then 1
			else 0 
			end 
		end
	

	,AgeOnAdmission =
		datediff(year, Encounter.DateOfBirth, CalendarAdmissionDate.TheDate) -
		case
		when datepart(day, Encounter.DateOfBirth) = datepart(day, CalendarAdmissionDate.TheDate)
		and	datepart(month, Encounter.DateOfBirth) = datepart(month,CalendarAdmissionDate.TheDate)
		and datepart(year, Encounter.DateOfBirth) <> datepart(year,CalendarAdmissionDate.TheDate)
		then 0
		else 
			case
			when Encounter.DateOfBirth > CalendarAdmissionDate.TheDate then 0
			when datepart(dy, Encounter.DateOfBirth) > datepart(dy, CalendarAdmissionDate.TheDate) 
			then 1
			else 0 
			end 
		end

	,DischargeReadyDate = --NULL --NEEDS FIXING
		left(
			convert(varchar, Encounter.DischargeReadyDate, 112)
			,8
		)

	,RTTPathwayID = 
			Right(
				'00000000000000000000' + 
				replace(
					ltrim(
						rtrim(
							Encounter.RTTPathwayID
						)
					)
					,' '
					, ''
				)
			,20
			)
	
	--left(
	--					 Encounter.RTTPathwayID
	--					,20
	--				) --this is 25 characters on PAS

	,RTTCurrentProviderCode = 
	CASE
			WHEN Encounter.RTTPathwayID IS NULL THEN NULL
			WHEN left(Encounter.RTTPathwayID, 3) = 'X09' THEN 'X09'
			ELSE COALESCE (
							 Encounter.RTTCurrentProviderCode
							,Trust.OrganisationCode
							,'RW300'
				)
		END
	--NULL --NEEDS FIXING
--		left(
--			RTTCurrentProviderCode.NationalCode
--			,5
--		)

	,RTTPeriodStatusCode = 
		--left(
		--	--RTTStatus.InternalCode
		--	RTTPeriodStatus.ValueCode
		--	,2
		--)
		left(
			CASE
			WHEN Encounter.RTTPathwayID IS NULL THEN NULL
			when RTTPeriodStatus.NationalValueCode = 'N||RTTSTA' then '99'
			else RTTPeriodStatus.NationalValueCode
			end
			,2
		)

	,RTTStartDate = 
		left(
			convert(varchar, Encounter.RTTStartDate, 112)
			,8
		)

	,RTTEndDate = 
		left(
			convert(varchar, Encounter.RTTEndDate, 112)
			,8
		)

	,EarliestReasonableOfferDate = null --optional on CDS definition

--	--Other Baby Stuff

	,Baby1NHSNumberStatus = 
		left(
			Baby1NHSNumberStatus.XrefEntityCode
			,2
		)

	,Baby1NHSNumber = 
		left(
			replace(
				Baby1Patient.NHSNumber
				,' '
				,''
			)
			,17
		)
	,Baby2NHSNumberStatus = 
		left(
			Baby2NHSNumberStatus.XrefEntityCode
			,2
		)

	,Baby2NHSNumber = 
		left(
			replace(
				Baby2Patient.NHSNumber
				,' '
				,''
			)
			,17
		)
	,Baby3NHSNumberStatus = 
		left(
			Baby3NHSNumberStatus.XrefEntityCode
			,2
		)

	,Baby3NHSNumber = 
		left(
			replace(
				Baby3Patient.NHSNumber
				,' '
				,''
			)
			,17
		)
	,Baby4NHSNumberStatus = 
		left(
			Baby4NHSNumberStatus.XrefEntityCode
			,2
		)

	,Baby4NHSNumber = 
		left(
			replace(
				Baby4Patient.NHSNumber
				,' '
				,''
			)
			,17
		)
	
	,Baby5NHSNumberStatus = 
		left(
			Baby5NHSNumberStatus.XrefEntityCode
			,2
		)

	,Baby5NHSNumber = 
		left(
			replace(
				Baby5Patient.NHSNumber
				,' '
				,''
			)
			,17
		)
	,Baby6NHSNumberStatus = 
		left(
			Baby6NHSNumberStatus.XrefEntityCode
			,2
		)

	,Baby6NHSNumber =
	left(
			replace(
				Baby6Patient.NHSNumber
				,' '
				,''
			)
			,17
		)

	,EndOfRecord = null
	,Encounter.Reportable
INTO
	#APCAnglia
FROM
	WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference EncounterReference
on	EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno
	
INNER JOIN CDS.wrkCDS
ON	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
AND	wrkCDS.CDSTypeCode in ('120', '130', '140')

inner join WarehouseOLAPMergedV2.APC.BaseEncounter Admission
on	Admission.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
and	Admission.GlobalEpisodeNo = 1

inner join WarehouseOLAPMergedV2.APC.Spell
on	Spell.AdmissionEpisodeMergeRecno = Admission.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounter Discharge
on	Discharge.MergeEncounterRecno = Spell.DischargeEpisodeMergeRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference AdmissionReference
on	AdmissionReference.MergeEncounterRecno = Admission.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference DischargeReference
on	DischargeReference.MergeEncounterRecno = Discharge.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Calendar CalendarDischargeDate
on	DischargeReference.DischargeDateID = CalendarDischargeDate.DateID
	
inner join WarehouseOLAPMergedV2.WH.Calendar CalendarEpisodeStartDate
on	EncounterReference.EpisodeStartDateID = CalendarEpisodeStartDate.DateID
	
left join WarehouseOLAPMergedV2.WH.Calendar CalendarAdmissionDate
on	AdmissionReference.AdmissionDateID = CalendarAdmissionDate.DateID

left join WarehouseOLAPMergedV2.WH.Calendar CalendarEpisodeEndDate
on	EncounterReference.EpisodeEndDateID = CalendarEpisodeEndDate.DateID

left join WarehouseOLAPMergedV2.WH.Member NHSNumberStatus
ON	NHSNumberStatus.SourceValueID = EncounterReference.NHSNumberStatusID

--SEX	Sex
left join WarehouseOLAPMergedV2.WH.Member Sex
ON	Sex.SourceValueID = EncounterReference.SexID

--inner join Warehouse.PAS.Patient
--on	Patient.SourcePatientNo = Encounter.SourcePatientNo

left join WarehouseOLAPMergedV2.WH.Member EthnicCategory
on	EthnicCategory.SourceValueID = EncounterReference.EthnicOriginID

left join WarehouseOLAPMergedV2.WH.Member AdmissionMethod
on	AdmissionMethod.SourceValueID = AdmissionReference.AdmissionMethodID

left join WarehouseOLAPMergedV2.WH.Member ManagementIntention
on	ManagementIntention.SourceValueID = AdmissionReference.IntendedManagementID

left join WarehouseOLAPMergedV2.WH.Member AdmissionSource
on	AdmissionSource.SourceValueID = AdmissionReference.AdmissionSourceID

left join WarehouseOLAPMergedV2.WH.Member DischargeMethod
on	DischargeMethod.SourceValueID = DischargeReference.DischargeMethodID

left join WarehouseOLAPMergedV2.WH.Member DischargeDestination
on	DischargeDestination.SourceValueID = DischargeReference.DischargeDestinationID

left join WarehouseOLAPMergedV2.WH.Member PatientClassification
on	PatientClassification.SourceValueID = EncounterReference.PatientClassificationID

--left join WarehouseOLAPMergedV2.WH.Member LastEpisodeInSpell
--on	LastEpisodeInSpell.SourceValueID = EncounterReference.LastEpisodeInSpellID

left join WarehouseOLAPMergedV2.WH.Member Consultant
on	Consultant.SourceValueID = EncounterReference.ConsultantID

left join WarehouseOLAPMergedV2.WH.Member Specialty
on	Specialty.SourceValueID = EncounterReference.SpecialtyID

left join WarehouseOLAPMergedV2.WH.Member RTTPeriodStatus
on	RTTPeriodStatus.SourceValueID = EncounterReference.RTTPeriodStatusID

left join WarehouseOLAPMergedV2.WH.Member NeonatalLevelOfCare
on	NeonatalLevelOfCare.SourceValueID = EncounterReference.NeonatalLevelOfCareID

left join WarehouseOLAPMergedV2.WH.Member StartSite
on	StartSite.SourceValueID = EncounterReference.StartSiteID

left join WarehouseOLAPMergedV2.WH.Member EndSite
on	EndSite.SourceValueID = EncounterReference.EndSiteID

left join WarehouseOLAPMergedV2.WH.Member FirstRegularDayNightAdmission
on	FirstRegularDayNightAdmission.SourceValueID = EncounterReference.FirstRegDayOrNightAdmitID

left join WarehouseOLAPMergedV2.WH.Member AdminCategory
on	AdminCategory.SourceValueID = EncounterReference.AdminCategoryID

left join WarehouseOLAPMergedV2.WH.Member CarerSupportIndicator
on	CarerSupportIndicator.SourceValueID = EncounterReference.CarerSupportIndicatorID

LEFT join Warehouse.PAS.Delivery
on	Delivery.SourcePatientNo = Encounter.SourcePatientNo
and Delivery.SourceSpellNo = Encounter.SourceSpellNo
AND Encounter.ContextCode = 'CEN||PAS'

LEFT join Warehouse.PAS.Birth Baby1
on	Baby1.SourcePatientNo = Encounter.SourcePatientNo
and Baby1.SourceSpellNo = Encounter.SourceSpellNo
and	Baby1.BirthOrder = 1
AND Encounter.ContextCode = 'CEN||PAS'

LEFT join Warehouse.PAS.Patient Baby1Patient
on	Baby1Patient.SourcePatientNo = Baby1.BabyPatientNo

left join Warehouse.dbo.EntityXref Baby1NHSNumberStatus
on	Baby1NHSNumberStatus.EntityTypeCode = 'LOCALNHSSTATUSCODE'
and	Baby1NHSNumberStatus.XrefEntityTypeCode = 'NATIONALNHSSTATUSCODE'
and	Baby1NHSNumberStatus.EntityCode = Baby1Patient.NHSNumberStatusId

LEFT join Warehouse.PAS.Birth Baby2
on	Baby2.SourcePatientNo = Encounter.SourcePatientNo
and Baby2.SourceSpellNo = Encounter.SourceSpellNo
and	Baby2.BirthOrder = 2
AND Encounter.ContextCode = 'CEN||PAS'

LEFT join Warehouse.PAS.Patient Baby2Patient
on	Baby2Patient.SourcePatientNo = Baby2.BabyPatientNo

left join Warehouse.dbo.EntityXref Baby2NHSNumberStatus
on	Baby2NHSNumberStatus.EntityTypeCode = 'LOCALNHSSTATUSCODE'
and	Baby2NHSNumberStatus.XrefEntityTypeCode = 'NATIONALNHSSTATUSCODE'
and	Baby2NHSNumberStatus.EntityCode = Baby2Patient.NHSNumberStatusId

LEFT join Warehouse.PAS.Birth Baby3
on	Baby3.SourcePatientNo = Encounter.SourcePatientNo
and Baby3.SourceSpellNo = Encounter.SourceSpellNo
and	Baby3.BirthOrder = 3
AND Encounter.ContextCode = 'CEN||PAS'

LEFT join Warehouse.PAS.Patient Baby3Patient
on	Baby3Patient.SourcePatientNo = Baby3.BabyPatientNo

left join Warehouse.dbo.EntityXref Baby3NHSNumberStatus
on	Baby3NHSNumberStatus.EntityTypeCode = 'LOCALNHSSTATUSCODE'
and	Baby3NHSNumberStatus.XrefEntityTypeCode = 'NATIONALNHSSTATUSCODE'
and	Baby3NHSNumberStatus.EntityCode = Baby3Patient.NHSNumberStatusId

LEFT join Warehouse.PAS.Birth Baby4
on	Baby4.SourcePatientNo = Encounter.SourcePatientNo
and Baby4.SourceSpellNo = Encounter.SourceSpellNo
and	Baby4.BirthOrder = 4
AND Encounter.ContextCode = 'CEN||PAS'

LEFT join Warehouse.PAS.Patient Baby4Patient
on	Baby4Patient.SourcePatientNo = Baby4.BabyPatientNo

left join Warehouse.dbo.EntityXref Baby4NHSNumberStatus
on	Baby4NHSNumberStatus.EntityTypeCode = 'LOCALNHSSTATUSCODE'
and	Baby4NHSNumberStatus.XrefEntityTypeCode = 'NATIONALNHSSTATUSCODE'
and	Baby4NHSNumberStatus.EntityCode = Baby4Patient.NHSNumberStatusId

LEFT join Warehouse.PAS.Birth Baby5
on	Baby5.SourcePatientNo = Encounter.SourcePatientNo
and Baby5.SourceSpellNo = Encounter.SourceSpellNo
and	Baby5.BirthOrder = 5
AND Encounter.ContextCode = 'CEN||PAS'

LEFT join Warehouse.PAS.Patient Baby5Patient
on	Baby5Patient.SourcePatientNo = Baby5.BabyPatientNo

left join Warehouse.dbo.EntityXref Baby5NHSNumberStatus
on	Baby5NHSNumberStatus.EntityTypeCode = 'LOCALNHSSTATUSCODE'
and	Baby5NHSNumberStatus.XrefEntityTypeCode = 'NATIONALNHSSTATUSCODE'
and	Baby5NHSNumberStatus.EntityCode = Baby5Patient.NHSNumberStatusId

LEFT join Warehouse.PAS.Birth Baby6
on	Baby6.SourcePatientNo = Encounter.SourcePatientNo
and Baby6.SourceSpellNo = Encounter.SourceSpellNo
and	Baby6.BirthOrder = 6
AND Encounter.ContextCode = 'CEN||PAS'

LEFT join Warehouse.PAS.Patient Baby6Patient
on	Baby6Patient.SourcePatientNo = Baby6.BabyPatientNo

left join Warehouse.dbo.EntityXref Baby6NHSNumberStatus
on	Baby6NHSNumberStatus.EntityTypeCode = 'LOCALNHSSTATUSCODE'
and	Baby6NHSNumberStatus.XrefEntityTypeCode = 'NATIONALNHSSTATUSCODE'
and	Baby6NHSNumberStatus.EntityCode = Baby6Patient.NHSNumberStatusId

LEFT join Warehouse.PAS.DeliveryMethod Baby1DeliveryMethod
on	Baby1DeliveryMethod.DeliveryMethodCode = Baby1.DeliveryMethodCode

LEFT join Warehouse.PAS.DeliveryMethod Baby2DeliveryMethod
on	Baby2DeliveryMethod.DeliveryMethodCode = Baby2.DeliveryMethodCode

LEFT join Warehouse.PAS.DeliveryMethod Baby3DeliveryMethod
on	Baby3DeliveryMethod.DeliveryMethodCode = Baby3.DeliveryMethodCode

LEFT join Warehouse.PAS.DeliveryMethod Baby4DeliveryMethod
on	Baby4DeliveryMethod.DeliveryMethodCode = Baby4.DeliveryMethodCode

LEFT join Warehouse.PAS.DeliveryMethod Baby5DeliveryMethod
on	Baby5DeliveryMethod.DeliveryMethodCode = Baby5.DeliveryMethodCode

LEFT join Warehouse.PAS.DeliveryMethod Baby6DeliveryMethod
on	Baby6DeliveryMethod.DeliveryMethodCode = Baby6.DeliveryMethodCode


--Maternity attributes for birth records (CDSType 120)

LEFT join Warehouse.PAS.Birth
on	Birth.BabyPatientNo = Encounter.SourcePatientNo
AND Encounter.ContextCode = 'CEN||PAS'
and	wrkCDS.CDSTypeCode = '120'

LEFT join Warehouse.PAS.Patient BirthPatient
on	BirthPatient.SourcePatientNo = Birth.BabyPatientNo

left join Warehouse.PAS.Patient BirthMother
on	BirthMother.SourcePatientNo = Birth.SourcePatientNo

left join Warehouse.dbo.EntityXref BirthNHSNumberStatus
on	BirthNHSNumberStatus.EntityTypeCode = 'LOCALNHSSTATUSCODE'
and	BirthNHSNumberStatus.XrefEntityTypeCode = 'NATIONALNHSSTATUSCODE'
and	BirthNHSNumberStatus.EntityCode = BirthPatient.NHSNumberStatusId

LEFT join Warehouse.PAS.Delivery BirthDelivery
on	BirthDelivery.SourcePatientNo = Birth.SourcePatientNo
and BirthDelivery.SourceSpellNo = Birth.SourceSpellNo

LEFT join Warehouse.PAS.DeliveryMethod BirthDeliveryMethod
on	BirthDeliveryMethod.DeliveryMethodCode = Birth.DeliveryMethodCode

left join Organisation.dbo.Trust Trust
ON Trust.OrganisationCode = Encounter.RTTCurrentProviderCode

--left join PAS.Operation PrimaryOperationGroup
--on	PrimaryOperationGroup.OperationCode = Encounter.PrimaryOperationCode

--left join PAS.Operation SecondaryOperationGroup1
--on	SecondaryOperationGroup1.OperationCode = Encounter.SecondaryOperationCode1

--left join PAS.Operation SecondaryOperationGroup2
--on	SecondaryOperationGroup2.OperationCode = Encounter.SecondaryOperationCode2

--left join PAS.Operation SecondaryOperationGroup3
--on	SecondaryOperationGroup3.OperationCode = Encounter.SecondaryOperationCode3


--left join Organisation.dbo.PCT
--on	PCT.OrganisationCode = Encounter.PCTCode

----PCT
--left join Organisation.dbo.Postcode Postcode
--on	Postcode.Postcode =
--		case
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
--		else Encounter.Postcode
--		end

--CCG
left join Organisation.ODS.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end
		
left join WarehouseOLAPMergedV2.WH.Consultant ReferrerConsultant
ON convert(varchar,ReferrerConsultant.SourceConsultantID) = convert(varchar,EncounterReference.ReferrerID)
AND Encounter.ReferredByCode = 'CON' 

left join WarehouseOLAPMergedV2.WH.Consultant ConsultantSpecialty
ON convert(varchar,ConsultantSpecialty.SourceConsultantID) = convert(varchar,EncounterReference.ConsultantID) 

--left join Organisation.dbo.PCT PCTOfResidence
--on	PCTOfResidence.OrganisationCode = Postcode.PCTCode

--left join PAS.Site RTTCurrentProviderCode
--on	RTTCurrentProviderCode.SiteCode = Encounter.RTTCurrentProviderCode

--left join APC.HRG4Encounter
--on	HRG4Encounter.EncounterRecno = Encounter.EncounterRecno

--left join PAS.Gp ReferringGp
--on	ReferringGp.GpCode = Encounter.ReferrerCode
--and	Encounter.ReferredByCode = 'GP'

--left join PAS.Gdp ReferringGDP
--on	ReferringGDP.GdpCode = Encounter.ReferrerCode
--and	Encounter.ReferredByCode = 'GDP'

--left join PAS.Consultant ReferringConsultant
--on	ReferringConsultant.ConsultantCode = Encounter.ReferrerCode
--and	Encounter.ReferredByCode = 'CONS'

left join Organisation.dbo.Gp ReferringGpOrganisation
on	ReferringGpOrganisation.organisationcode = Admission.ReferrerCode
and	Encounter.ReferredByCode = 'GP'

left join Organisation.dbo.DGp ReferringGDPOrganisation
on	ReferringGDPOrganisation.organisationcode = Admission.ReferrerCode
and	Encounter.ReferredByCode = 'GDP'




--left join PAS.Site ReferringConsultantOrganisation
--on	ReferringConsultantOrganisation.NationalCode = ReferringConsultant.ProviderCode
--and	Encounter.ReferredByCode = 'CONS'

--SELECT DISTINCT EthnicGroupCode FROM #APCAnglia
--DROP TABLE #APCAnglia
--done this way for performance reasons!

insert into CDS.APCAngliaBase
(
	 MergeEncounterRecno
	,UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,RecordType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,PatientsAddress
	,Postcode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,EthnicGroupCode
	,ReferrerCode
	,ReferringOrganisationCode
	,ProviderSpellNo
	,PatientCategoryCode
	,DecidedToAdmitDate
	,AdmissionDate
	,AdmissionMethodCode
	,DurationOfElectiveWait
	,ManagementIntentionCode
	,SourceOfAdmissionCode
	,DischargeDate
	,DischargeMethodCode
	,DischargeDestinationCode
	,PatientClassificationCode
	,EpisodeNo
	,LastEpisodeInSpellIndicator
	,StartSiteCode
	,StartWardTypeCode
	,EpisodeStartDate
	,EpisodeEndDate
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,ConsultantCode
	,FirstRegularDayNightAdmission
	,NeonatalLevelOfCare
	,PsychiatricPatientStatusCode
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,OperationStatus
	,PrimaryOperationCode
	,PrimaryOperationDate
	,OperationCode2
	,OperationDate2
	,OperationCode3
	,OperationDate3
	,OperationCode4
	,OperationDate4
	,OperationCode5
	,OperationDate5
	,OperationCode6
	,OperationDate6
	,OperationCode7
	,OperationDate7
	,OperationCode8
	,OperationDate8
	,OperationCode9
	,OperationDate9
	,OperationCode10
	,OperationDate10
	,OperationCode11
	,OperationDate11
	,OperationCode12
	,OperationDate12
	,EpisodeStartWardCode
	,EndWardTypeCode
	,EndSiteCode
	,AnteNatalGpCode
	,FirstAnteNatalAssessmentDate
	,PreviousPregnancies
	,ActualDeliveryPlaceTypeCode
	,IntendedDeliveryPlaceTypeCode
	,DeliveryPlaceChangeReasonCode
	,GestationLength
	,LabourOnsetMethodCode
	,StatusOfPersonConductingDeliveryCode
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,NumberOfBabies
	,Baby1DeliveryDate
	,Baby1DeliveryMethodCode
	,Baby1SexCode
	,Baby1BirthOrder
	,Baby1LiveStillBirth
	,Baby1BirthWeight
	,Baby1MethodOfResuscitationCode
	,Baby1BirthDate
	,Baby1MotherBirthDate
	,Baby2DeliveryDate
	,Baby2DeliveryMethodCode
	,Baby2SexCode
	,Baby2BirthOrder
	,Baby2LiveStillBirth
	,Baby2BirthWeight
	,Baby2MethodOfResuscitationCode
	,Baby2BirthDate
	,Baby2MotherBirthDate
	,Baby3DeliveryDate
	,Baby3DeliveryMethodCode
	,Baby3SexCode
	,Baby3BirthOrder
	,Baby3LiveStillBirth
	,Baby3BirthWeight
	,Baby3MethodOfResuscitationCode
	,Baby3BirthDate
	,Baby3MotherBirthDate
	,Baby4DeliveryDate
	,Baby4DeliveryMethodCode
	,Baby4SexCode
	,Baby4BirthOrder
	,Baby4LiveStillBirth
	,Baby4BirthWeight
	,Baby4MethodOfResuscitationCode
	,Baby4BirthDate
	,Baby4MotherBirthDate
	,Baby5DeliveryDate
	,Baby5DeliveryMethodCode
	,Baby5SexCode
	,Baby5BirthOrder
	,Baby5LiveStillBirth
	,Baby5BirthWeight
	,Baby5MethodOfResuscitationCode
	,Baby5BirthDate
	,Baby5MotherBirthDate
	,Baby6DeliveryDate
	,Baby6DeliveryMethodCode
	,Baby6SexCode
	,Baby6BirthOrder
	,Baby6LiveStillBirth
	,Baby6BirthWeight
	,Baby6MethodOfResuscitationCode
	,Baby6BirthDate
	,Baby6MotherBirthDate
	,NNNStatusIndicator
	,LegalStatusClassificationCode
	,PCTofResidenceCode
	,AgeAtCDSActivityDate
	,AgeOnAdmission
	,DischargeReadyDate
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,Baby1NHSNumberStatus
	,Baby1NHSNumber
	,Baby2NHSNumberStatus
	,Baby2NHSNumber
	,Baby3NHSNumberStatus
	,Baby3NHSNumber
	,Baby4NHSNumberStatus
	,Baby4NHSNumber
	,Baby5NHSNumberStatus
	,Baby5NHSNumber
	,Baby6NHSNumberStatus
	,Baby6NHSNumber
	,EndOfRecord
)
select
	 MergeEncounterRecno
	,UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,RecordType
	,ProviderCode
	,PurchaserCode
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,PurchaserReferenceNo
	,NHSNumber
	,PatientName
	,PatientAddress
	,Postcode
	,SexCode
	,CarerSupportIndicator
	,DateOfBirth
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,DistrictNo
	,EthnicGroupCode
	,ReferrerCode
	,ReferringOrganisationCode
	,ProviderSpellNo
	,PatientCategoryCode
	,DecidedToAdmitDate
	,AdmissionDate
	,AdmissionMethodCode
	,DurationOfElectiveWait
	,ManagementIntentionCode
	,SourceOfAdmissionCode
	,DischargeDate
	,DischargeMethodCode
	,DischargeDestinationCode
	,PatientClassificationCode
	,EpisodeNo
	,LastEpisodeInSpellIndicator
	,StartSiteCode
	,StartWardTypeCode
	,EpisodeStartDate
	,EpisodeEndDate
	,MainSpecialtyCode
	,TreatmentFunctionCode
	,ConsultantCode
	,FirstRegularDayNightAdmission
	,NeonatalLevelOfCare
	,PsychiatricPatientStatusCode
	,PrimaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,OperationStatus
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,EpisodeStartWardTypeCode
	,EndWardTypeCode
	,EndSiteCode
	,AnteNatalGpCode
	,FirstAnteNatalAssessmentDate
	,PreviousPregnancies
	,ActualDeliveryPlaceTypeCode
	,IntendedDeliveryPlaceTypeCode
	,DeliveryPlaceChangeReasonCode
	,GestationLength
	,LabourOnsetMethodCode
	,StatusOfPersonConductingDeliveryCode
	,AnaestheticGivenDuringCode
	,AnaestheticGivenPostCode
	,NumberOfBabies
	,Baby1DeliveryDate
	,Baby1DeliveryMethodCode
	,Baby1SexCode
	,Baby1BirthOrder
	,Baby1LiveStillBirth
	,Baby1BirthWeight
	,Baby1MethodOfResuscitationCode
	,Baby1BirthDate
	,Baby1MotherBirthDate
	,Baby2DeliveryDate
	,Baby2DeliveryMethodCode
	,Baby2SexCode
	,Baby2BirthOrder
	,Baby2LiveStillBirth
	,Baby2BirthWeight
	,Baby2MethodOfResuscitationCode
	,Baby2BirthDate
	,Baby2MotherBirthDate
	,Baby3DeliveryDate
	,Baby3DeliveryMethodCode
	,Baby3SexCode
	,Baby3BirthOrder
	,Baby3LiveStillBirth
	,Baby3BirthWeight
	,Baby3MethodOfResuscitationCode
	,Baby3BirthDate
	,Baby3MotherBirthDate
	,Baby4DeliveryDate
	,Baby4DeliveryMethodCode
	,Baby4SexCode
	,Baby4BirthOrder
	,Baby4LiveStillBirth
	,Baby4BirthWeight
	,Baby4MethodOfResuscitationCode
	,Baby4BirthDate
	,Baby4MotherBirthDate
	,Baby5DeliveryDate
	,Baby5DeliveryMethodCode
	,Baby5SexCode
	,Baby5BirthOrder
	,Baby5LiveStillBirth
	,Baby5BirthWeight
	,Baby5MethodOfResuscitationCode
	,Baby5BirthDate
	,Baby5MotherBirthDate
	,Baby6DeliveryDate
	,Baby6DeliveryMethodCode
	,Baby6SexCode
	,Baby6BirthOrder
	,Baby6LiveStillBirth
	,Baby6BirthWeight
	,Baby6MethodOfResuscitationCode
	,Baby6BirthDate
	,Baby6MotherBirthDate
	,NNNStatusIndicator
	,LegalStatusClassificationCode
	,PCTofResidenceCode
	,AgeAtCDSActivityDate
	,AgeOnAdmission
	,DischargeReadyDate
	,RTTPathwayID
	,RTTCurrentProviderCode
	,RTTPeriodStatusCode
	,RTTStartDate
	,RTTEndDate
	,Baby1NHSNumberStatus
	,Baby1NHSNumber
	,Baby2NHSNumberStatus
	,Baby2NHSNumber
	,Baby3NHSNumberStatus
	,Baby3NHSNumber
	,Baby4NHSNumberStatus
	,Baby4NHSNumber
	,Baby5NHSNumberStatus
	,Baby5NHSNumber
	,Baby6NHSNumberStatus
	,Baby6NHSNumber
	,EndOfRecord
from
	#APCAnglia
where 
	Reportable = 1


drop table #APCAnglia


----update the ward stay details

select
	 Encounter.MergeEncounterRecno
	,SequenceNo =
		row_number() over (partition by Encounter.GlobalProviderSpellNo, Encounter.GlobalEpisodeNo order by WardStay.StartTime)
	,WardStay.StartDate
	,WardStay.StartTime
	,SiteCode = left(Site.NationalSiteCode,5)
	,WardCode = left(Ward.NationalWardCode,4)
into
	#EncounterWardStay
from
	WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.APC.BaseWardStay WardStay
on	WardStay.ProviderSpellNo = Encounter.ProviderSpellNo
and	WardStay.ContextCode = Encounter.ContextCode
and	WardStay.StartTime between Encounter.EpisodeStartTime and dateadd(minute, -1, Encounter.EpisodeEndTime)

inner join WarehouseOLAPMergedV2.APC.BaseWardStayReference WardStayReference
on	WardStayReference.MergeEncounterRecno = WardStay.MergeEncounterRecno

LEFT JOIN WarehouseOLAPMergedV2.WH.Site Site
ON	Site.SourceSiteID = WardStayReference.WardID

LEFT JOIN WarehouseOLAPMergedV2.WH.Ward Ward
on	Ward.SourceWardID = WardStayReference.WardID

inner join CDS.wrkCDS wrkCDS
on	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	wrkCDS.CDSTypeCode in ('120', '130', '140')

where Encounter.Reportable = 1

CREATE CLUSTERED INDEX [IX_#EncounterWardStay] ON #EncounterWardStay
(
	 MergeEncounterRecno ASC
	,SequenceNo asc
)


declare @sql varchar(4000)

declare @sqlTemplate varchar(4000) =
	'
	update
		CDS.APCAngliaBase
	set
		 WardStay<SequenceNo>Sequence = EncounterWardStay.SequenceNo
		,WardStay<SequenceNo>TransferDate = convert(varchar, EncounterWardStay.StartDate, 112)
		,WardStay<SequenceNo>TransferTime = 
			replace(
				 CONVERT(varchar, EncounterWardStay.StartTime, 108)
				,'':''
				,''''
			)
		,WardStay<SequenceNo>HospitalCode = EncounterWardStay.SiteCode
		,WardStay<SequenceNo>WardCode = EncounterWardStay.WardCode
	from
		CDS.APCAngliaBase

	inner join #EncounterWardStay EncounterWardStay
	on	EncounterWardStay.MergeEncounterRecno = APCAngliaBase.MergeEncounterRecno
	and	EncounterWardStay.SequenceNo = <SequenceNo>
	'

declare @maxWardStayCount int = 
	(
	select
		case 
			when MAX(SequenceNo) > 74 then 74
			else MAX(SequenceNo)
		end
	from
		#EncounterWardStay
	)

declare @wardStay int = 1


while (@wardStay <= @maxWardStayCount)
begin

	select
		@sql =
			REPLACE(
				 @sqlTemplate
				,'<SequenceNo>'
				,CAST(@wardStay as varchar)
			)

	--print @sql
	exec (@sql)

	select
		@wardStay = @wardStay + 1

end


update
	CDS.APCAngliaBase
set
	NumberOfWardTransfers = 
		(
		select
			COUNT(*)
		from
			#EncounterWardStay EncounterWardStay
		where
			EncounterWardStay.MergeEncounterRecno = APCAngliaBase.MergeEncounterRecno
		)

drop table #EncounterWardStay



----update the critical care details

select
	 Encounter.MergeEncounterRecno
	,SequenceNo =
		row_number() over (partition by Encounter.MergeEncounterRecno, CC.CasenoteNumber order by CC.StartTime, CC.EndTime, CC.MergeEncounterRecno)
	,CC.LocalIdentifier
	,CC.StartDate
	,CC.UnitFunctionCode
	,CC.AdvancedRespiratorySupportDays
	,CC.BasicRespiratorySupportDays
	,CC.AdvancedCardiovascularSupportDays
	,CC.BasicCardiovascularSupportDays
	,CC.RenalSupportDays
	,CC.NeurologicalSupportDays
	,CC.DermatologicalSupportDays
	,CC.LiverSupportDays
	,CC.CriticalCareLevel2Days
	,CC.CriticalCareLevel3Days
	,CC.EndDate
into
	#CriticalCareEpisode
from
	WarehouseOLAPMergedV2.APC.BaseCriticalCarePeriod CC

inner join WarehouseOLAPMergedV2.APC.BaseEncounter Encounter
on	Encounter.CasenoteNumber = CC.CasenoteNumber
and	Encounter.AdmissionDate = CC.AdmissionDate
and	Encounter.EpisodeStartDate = CC.StartDate

--Gareth C
--17 Jan 2013
--Chanage required as missing some CC records but not to be impleted till after CDS has been sent
--and CC.StartDate between Encounter.EpisodeStartDate and Encounter.EpisodeEndDate

and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.APC.BaseEncounter Previous
	where
		Previous.CasenoteNumber = CC.CasenoteNumber
	and	Previous.AdmissionDate = CC.AdmissionDate
	and	Previous.EpisodeStartDate = CC.StartDate
	and	
	(
			Previous.EpisodeStartTime > Encounter.EpisodeStartTime
		or	(
				Previous.EpisodeStartTime = Encounter.EpisodeStartTime
			and	Previous.MergeEncounterRecno > Encounter.MergeEncounterRecno
			)
		)
	)

inner join CDS.wrkCDS wrkCDS
on	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	wrkCDS.CDSTypeCode in ('120', '130', '140')

where Encounter.Reportable = 1


CREATE CLUSTERED INDEX [IX_#CriticalCareEpisode] ON #CriticalCareEpisode
(
	 MergeEncounterRecno ASC
	,SequenceNo asc
)


select
	@sqlTemplate =
	'
	update
		CDS.APCAngliaBase
	set
		 CCP<SequenceNo>Number = CriticalCareEpisode.SequenceNo
		,CCP<SequenceNo>LocalIdentifier = CriticalCareEpisode.SequenceNo
		,CCP<SequenceNo>StartDate = convert(varchar, CriticalCareEpisode.StartDate, 112)
		,CCP<SequenceNo>UnitFunction = CriticalCareEpisode.UnitFunctionCode
		,CCP<SequenceNo>AdvancedRespiratorySupportDays = CriticalCareEpisode.AdvancedRespiratorySupportDays
		,CCP<SequenceNo>BasicRespiratorySupportDays = CriticalCareEpisode.BasicRespiratorySupportDays
		,CCP<SequenceNo>AdvancedCardiovascularSupportDays = CriticalCareEpisode.AdvancedCardiovascularSupportDays
		,CCP<SequenceNo>BasicCardiovascularSupportDays = CriticalCareEpisode.BasicCardiovascularSupportDays
		,CCP<SequenceNo>RenalSupportDays = CriticalCareEpisode.RenalSupportDays
		,CCP<SequenceNo>NeurologicalSupportDays = CriticalCareEpisode.NeurologicalSupportDays
		,CCP<SequenceNo>DermatologicalSupportDays = CriticalCareEpisode.DermatologicalSupportDays
		,CCP<SequenceNo>LiverSupportDays = CriticalCareEpisode.LiverSupportDays
		,CCP<SequenceNo>Level2Days = CriticalCareEpisode.CriticalCareLevel2Days
		,CCP<SequenceNo>Level3Days = CriticalCareEpisode.CriticalCareLevel3Days
		,CCP<SequenceNo>DischargeDate = convert(varchar, CriticalCareEpisode.EndDate, 112)

		--,CCP<SequenceNo>DischargeTime = 
		--	replace(
		--		 CONVERT(varchar, CriticalCareEpisode.EndDate, 108)
		--		,'':''
		--		,''''
		--	)

		,CCP<SequenceNo>MaximumNumberOfOrganSupportSystem = null
	from
		CDS.APCAngliaBase

	inner join #CriticalCareEpisode CriticalCareEpisode
	on	CriticalCareEpisode.MergeEncounterRecno = APCAngliaBase.MergeEncounterRecno
	and	CriticalCareEpisode.SequenceNo = <SequenceNo>
	'


declare @maxCriticalCareEpisodeCount int = 
	(
	select
		MAX(SequenceNo)
	from
		#CriticalCareEpisode
	)

declare @criticalCareEpisode int = 1


while (@criticalCareEpisode <= @maxCriticalCareEpisodeCount)
begin

	select
		@sql =
			REPLACE(
				 @sqlTemplate
				,'<SequenceNo>'
				,CAST(@criticalCareEpisode as varchar)
			)

	--print @sql
	exec (@sql)

	select
		@criticalCareEpisode = @criticalCareEpisode + 1

end

drop table 	#CriticalCareEpisode


--GO

-- 20150514 Paediatric Critical Care RR added from Phils CDS62 script for year end submission

--paediatric critical care
truncate table CDS.APCCriticalCarePeriodAngliaBase

insert
into
	CDS.APCCriticalCarePeriodAngliaBase
	(
	 MergeEncounterRecno
	,UniqueEpisodeSerialNo
	,UpdateType
	,CDSUpdateDate
	,CDSUpdateTime
	,CDSType
	,RecordType
	,NHSNumber
	,PeriodID
	,StartDate
	,StartTime
	,UnitFunctionCode
	,GestationLength
	,ActivityDate
	,PersonWeight
	,ActivityCode1
	,ActivityCode2
	,ActivityCode3
	,ActivityCode4
	,ActivityCode5
	,ActivityCode6
	,ActivityCode7
	,ActivityCode8
	,ActivityCode9
	,ActivityCode10
	,ActivityCode11
	,ActivityCode12
	,ActivityCode13
	,ActivityCode14
	,ActivityCode15
	,ActivityCode16
	,ActivityCode17
	,ActivityCode18
	,ActivityCode19
	,ActivityCode20
	,HighCostDrugCode1
	,HighCostDrugCode2
	,HighCostDrugCode3
	,HighCostDrugCode4
	,HighCostDrugCode5
	,HighCostDrugCode6
	,HighCostDrugCode7
	,HighCostDrugCode8
	,HighCostDrugCode9
	,HighCostDrugCode10
	,HighCostDrugCode11
	,HighCostDrugCode12
	,HighCostDrugCode13
	,HighCostDrugCode14
	,HighCostDrugCode15
	,HighCostDrugCode16
	,HighCostDrugCode17
	,HighCostDrugCode18
	,HighCostDrugCode19
	,HighCostDrugCode20
	,EndDate
	,EndTime
	)
select
	  Encounter.MergeEncounterRecno
	 ,UniqueEpisodeSerialNo =
		left(
			'BMCHT ' +
			replace(
				 Encounter.SourceUniqueID
				,'||'
				,'*'
			)
			
			,50
		)

	,UpdateType = '9' --all records are either new or updated

	,CDSUpdateDate =
		left(
			CONVERT(varchar, getdate(), 112)
			,8
		)

	,CDSUpdateTime =
		left(
			replace(
				 CONVERT(varchar, getdate(), 108)
				,':'
				,''
			)
			,6
		)

	,CDSType = '888'
	
	,RecordType = 
		case wrkCDS.CDSTypeCode
			when '120' then '33'
			when '130' then '13'
			when '140' then '23'
		end

	,NHSNumber =
		left(
			replace(
				coalesce(
					 Encounter.NHSNumber
					,Period.NHSNumber
				)
				,' '
				,''
			)
			,17
		)

	,Period.PeriodID

	,StartDate =
		left(
			CONVERT(varchar, Period.AdmissionTime, 112)
			,8
		)

	,StartTime =
		left(
			replace(
				 CONVERT(varchar,
--if the cc admission date is the same as the episode start date then use the time off the episode 
					case
					when cast(Period.AdmissionTime as date) = cast(Encounter.EpisodeStartTime as date)
					then Encounter.EpisodeStartTime
					else Period.AdmissionTime
					end
					,108
				)
				,':'
				,''
			)
			,6
		)

	,Period.UnitFunctionCode

	,GestationLength =
		coalesce(Delivery.GestationLength , BirthDelivery.GestationLength)

	,ActivityDate =
		left(
			CONVERT(varchar, Activity1.ActivityDate, 112)
			,8
		)

	,PersonWeight = cast(null as varchar(7))

	,ActivityCode1 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity1.ActivityCode end
	,ActivityCode2 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity2.ActivityCode end
	,ActivityCode3 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity3.ActivityCode end
	,ActivityCode4 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity4.ActivityCode end
	,ActivityCode5 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity5.ActivityCode end
	,ActivityCode6 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity6.ActivityCode end
	,ActivityCode7 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity7.ActivityCode end
	,ActivityCode8 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity8.ActivityCode end
	,ActivityCode9 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity9.ActivityCode end
	,ActivityCode10 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity10.ActivityCode end
	,ActivityCode11 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity11.ActivityCode end
	,ActivityCode12 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity12.ActivityCode end
	,ActivityCode13 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity13.ActivityCode end
	,ActivityCode14 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity14.ActivityCode end
	,ActivityCode15 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity15.ActivityCode end
	,ActivityCode16 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity16.ActivityCode end
	,ActivityCode17 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity17.ActivityCode end
	,ActivityCode18 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity18.ActivityCode end
	,ActivityCode19 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity19.ActivityCode end
	,ActivityCode20 = case when cast(Period.DischargeTime as date) between cast(Encounter.EpisodeStartTime as date) and cast(Encounter.EpisodeEndTime as date) then Activity20.ActivityCode end

	,HighCostDrugCode1 = null
	,HighCostDrugCode2 = null
	,HighCostDrugCode3 = null
	,HighCostDrugCode4 = null
	,HighCostDrugCode5 = null
	,HighCostDrugCode6 = null
	,HighCostDrugCode7 = null
	,HighCostDrugCode8 = null
	,HighCostDrugCode9 = null
	,HighCostDrugCode10 = null
	,HighCostDrugCode11 = null
	,HighCostDrugCode12 = null
	,HighCostDrugCode13 = null
	,HighCostDrugCode14 = null
	,HighCostDrugCode15 = null
	,HighCostDrugCode16 = null
	,HighCostDrugCode17 = null
	,HighCostDrugCode18 = null
	,HighCostDrugCode19 = null
	,HighCostDrugCode20 = null
	,EndDate = left(
					Convert(
							varchar,Period.DischargeTime,112
							)
					,8)
	,EndTime = left(
			replace(
				 CONVERT(varchar,Period.DischargeTime,108)
				,':'
				,''
			)
			,6
		)
from
	Warehouse.PCC.Period

inner join WarehouseOLAPMergedV2.APC.BaseEncounter Encounter
on	Encounter.CasenoteNumber = Period.CasenoteNo
and	Encounter.EpisodeStartTime < Period.DischargeTime
and	Encounter.EpisodeEndTime > Period.AdmissionTime
and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.APC.BaseEncounter Previous
	where
		Previous.CasenoteNumber = Period.CasenoteNo
	and	Previous.EpisodeStartTime < Period.DischargeTime
	and	Previous.EpisodeEndTime > Period.AdmissionTime
	and	(
			Previous.GlobalEpisodeNo < Encounter.GlobalEpisodeNo
		or	(
				Previous.GlobalEpisodeNo = Encounter.GlobalEpisodeNo		
			and	Previous.MergeEncounterRecno < Encounter.MergeEncounterRecno
			)
		)
	)

left join Warehouse.PCC.Activity Activity1
on	Activity1.PeriodID = Period.PeriodID
and	Activity1.ActivitySequence = 1

left join Warehouse.PCC.Activity Activity2
on	Activity2.PeriodID = Period.PeriodID
and	Activity2.ActivityDate = Activity1.ActivityDate
and	Activity2.ActivitySequence = 2

left join Warehouse.PCC.Activity Activity3
on	Activity3.PeriodID = Period.PeriodID
and	Activity3.ActivityDate = Activity1.ActivityDate
and	Activity3.ActivitySequence = 3

left join Warehouse.PCC.Activity Activity4
on Activity4.PeriodID = Period.PeriodID
and Activity4.ActivityDate = Activity1.ActivityDate
and Activity4.ActivitySequence = 4

left join Warehouse.PCC.Activity Activity5
on Activity5.PeriodID = Period.PeriodID
and Activity5.ActivityDate = Activity1.ActivityDate
and Activity5.ActivitySequence = 5

left join Warehouse.PCC.Activity Activity6
on Activity6.PeriodID = Period.PeriodID
and Activity6.ActivityDate = Activity1.ActivityDate
and Activity6.ActivitySequence = 6

left join Warehouse.PCC.Activity Activity7
on Activity7.PeriodID = Period.PeriodID
and Activity7.ActivityDate = Activity1.ActivityDate
and Activity7.ActivitySequence = 7

left join Warehouse.PCC.Activity Activity8
on Activity8.PeriodID = Period.PeriodID
and Activity8.ActivityDate = Activity1.ActivityDate
and Activity8.ActivitySequence = 8

left join Warehouse.PCC.Activity Activity9
on Activity9.PeriodID = Period.PeriodID
and Activity9.ActivityDate = Activity1.ActivityDate
and Activity9.ActivitySequence = 9

left join Warehouse.PCC.Activity Activity10
on Activity10.PeriodID = Period.PeriodID
and Activity10.ActivityDate = Activity1.ActivityDate
and Activity10.ActivitySequence = 10

left join Warehouse.PCC.Activity Activity11
on Activity11.PeriodID = Period.PeriodID
and Activity11.ActivityDate = Activity1.ActivityDate
and Activity11.ActivitySequence = 11

left join Warehouse.PCC.Activity Activity12
on Activity12.PeriodID = Period.PeriodID
and Activity12.ActivityDate = Activity1.ActivityDate
and Activity12.ActivitySequence = 12

left join Warehouse.PCC.Activity Activity13
on Activity13.PeriodID = Period.PeriodID
and Activity13.ActivityDate = Activity1.ActivityDate
and Activity13.ActivitySequence = 13

left join Warehouse.PCC.Activity Activity14
on Activity14.PeriodID = Period.PeriodID
and Activity14.ActivityDate = Activity1.ActivityDate
and Activity14.ActivitySequence = 14

left join Warehouse.PCC.Activity Activity15
on Activity15.PeriodID = Period.PeriodID
and Activity15.ActivityDate = Activity1.ActivityDate
and Activity15.ActivitySequence = 15

left join Warehouse.PCC.Activity Activity16
on Activity16.PeriodID = Period.PeriodID
and Activity16.ActivityDate = Activity1.ActivityDate
and Activity16.ActivitySequence = 16

left join Warehouse.PCC.Activity Activity17
on Activity17.PeriodID = Period.PeriodID
and Activity17.ActivityDate = Activity1.ActivityDate
and Activity17.ActivitySequence = 17

left join Warehouse.PCC.Activity Activity18
on Activity18.PeriodID = Period.PeriodID
and Activity18.ActivityDate = Activity1.ActivityDate
and Activity18.ActivitySequence = 18

left join Warehouse.PCC.Activity Activity19
on Activity19.PeriodID = Period.PeriodID
and Activity19.ActivityDate = Activity1.ActivityDate
and Activity19.ActivitySequence = 19

left join Warehouse.PCC.Activity Activity20
on Activity20.PeriodID = Period.PeriodID
and Activity20.ActivityDate = Activity1.ActivityDate
and Activity20.ActivitySequence = 20

inner join CDS.wrkCDS
on	wrkCDS.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	wrkCDS.CDSTypeCode in ('120', '130', '140')

left join Warehouse.PAS.Delivery
on	Delivery.SourcePatientNo = Encounter.SourcePatientNo
and Delivery.SourceSpellNo = Encounter.SourceSpellNo
and Encounter.ContextCode = 'CEN||PAS'

left join Warehouse.PAS.Birth
on	Birth.BabyPatientNo = Encounter.SourcePatientNo
and Encounter.ContextCode = 'CEN||PAS'
and	wrkCDS.CDSTypeCode = '120'

left join Warehouse.PAS.Delivery BirthDelivery
on	BirthDelivery.SourcePatientNo = Birth.SourcePatientNo
and BirthDelivery.SourceSpellNo = Birth.SourceSpellNo



--update the paediatric critical care details
select
	@sqlTemplate =
	'
	update
		CDS.APCCriticalCarePeriodAngliaBase
	set
		 HighCostDrugCode<SequenceNo> = HighCostDrug.HighCostDrugCode
	from
		CDS.APCCriticalCarePeriodAngliaBase

	inner join Warehouse.PCC.HighCostDrug HighCostDrug
	on	HighCostDrug.PeriodID = APCCriticalCarePeriodAngliaBase.PeriodID
	and	HighCostDrug.HighCostDrugDate = APCCriticalCarePeriodAngliaBase.ActivityDate
	and	HighCostDrug.HighCostDrugSequence = <SequenceNo>
	'

declare
	 @count int = 1


while (@count <= 20)
begin

	select
		@sql =
			REPLACE(
				 @sqlTemplate
				,'<SequenceNo>'
				,CAST(@count as varchar)
			)

	--print @sql
	exec (@sql)

	select
		@count = @count + 1

end



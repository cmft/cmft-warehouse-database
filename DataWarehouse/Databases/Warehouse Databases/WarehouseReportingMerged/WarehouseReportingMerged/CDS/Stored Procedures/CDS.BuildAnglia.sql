﻿
CREATE procedure [CDS].[BuildAnglia]
      @ToDate date
      ,@CDSBulkReplacementGroupCode varchar (10) = '010'
      ,@CDSProtocolIdentifier varchar (10) = '010' --always submit as net change
      ,@FromDate date --= null
      ,@UpdateParameterTable BIT = 1
      
--with execute as owner 
as

--CODE  CLASSIFICATION  
--010 Finished General, Delivery and Birth Episodes 
--020 Unfinished General, Delivery and Birth Episodes 
--030 Other Delivery 
--040 Other Birth 
--050 Detained and/or Long Term Psychiatric Census 
--060 Outpatient (known as Care Activity in the Schema)  
--070 Standard variation of Elective Admission List End Of Period Census 
--080 New and Old variations of Elective Admission List End Of Period Census 
--090 Add variation of Elective Admission List Event During Period 
--100 Remove variation of Elective Admission List Event During Period 
--110 Offer variation of Elective Admission List Event During Period 
--120 Available/Unavailable variation of Elective Admission List Event During Period 
--130 New and Old variations of Elective Admission List Event During Period 
--140 Accident and Emergency Attendance 
--150 Future Outpatient (introduced in CDS Version 6 - known as Future Care Activity in the Schema)  

--Net change processing works as follows.
--
--The currency used to handle net change is the complete YTD dataset as held in the dataset specific Base table. For net change,
--the from date parameter is not strictly required as we always generate the CDS from the start of the fincancial year.
--
--A complete YTD CDS file is generated and compared with the previously submitted CDS data held in the dataset specific Submitted 
--table. This will throw up discrepencies as follows:
--
--    1. New record
--    2. Amended record
--    3. Deleted record
--
--A new dataset is then generated based on this information containing only new, amended and deleted records
--with the appropriate UpdateType.
-- 
--New records are those that exist in the Base table and do not exist in the Submitted table or the latest submitted transaction
--was a delete.
--
--Updated records are those that exist in both the Base table and Submitted table and have different attribute values and the 
--latest submitted transaction was an insert/update.
--
--Deleted records are those that do not exist in the Base table and do exist in the Submitted table where the latest submitted 
--transaction was an insert/update.
--
--On submission (generation of flat file) the submitted data is copied to the appropriate Submitted table ready for the next run.

-- 20150422 RR    added criteria to OP to exclude email and fax as per correspondance from Karen Burns Data Quality 01/04/2015 and 22/04/2015.

declare @from date
declare @to date


select
      @from =
            --case
            --when @CDSProtocolIdentifier = '010' --net
            --then
            --    case
            --    when datepart(month, @ToDate) < 4
            --    then '1 apr ' + datename(year, dateadd(year, -1, @ToDate))
            --    else '1 apr ' + datename(year, @ToDate)
            --    end

            --else
                  coalesce(
                        @FromDate
                        ,(select DateValue from Warehouse.dbo.Parameter where Parameter = 'LASTCDS' + @CDSBulkReplacementGroupCode + 'EXTRACTDATE')
                        ,getdate()
                  )

            --end

      ,@to = @ToDate


truncate table CDS.wrkCDS

--finished
if @CDSBulkReplacementGroupCode = '010' 
begin

      -- CDSType 120, 130, 140
      insert into CDS.wrkCDS
      (
            CDSTypeCode
            ,MergeEncounterRecno
      )
      select
            CDSTypeCode =

            --CMFT - generate 130s only and leave birth/delivery assignment to Anglia
            case
            when 
                  exists
                        (
                        select
                              1
                        from
                              Warehouse.PAS.Birth Birth
                        where
                              Birth.BabyPatientNo = Encounter.SourcePatientNo
                        and AdmissionMethodNational.NationalValuecode = '82'
                        and Encounter.FirstEpisodeInSpellIndicator = 1
                        and   Encounter.SourceSpellNo = 
                              (
                              select
                                    MIN(FirstSpell.SourceSpellNo)
                              from
                                    WarehouseOLAPMergedV2.APC.BaseEncounter FirstSpell
                              where
                                    FirstSpell.SourcePatientNo = Birth.BabyPatientNo
                              )
                        )
                        
            then '120' --birth
            when 
                  exists
                        (
                        select
                              1
                        from
                              Warehouse.PAS.Delivery Delivery
                        where
                              Delivery.SourcePatientNo = Encounter.SourcePatientNo
                        and   Delivery.SourceSpellNo = Encounter.SourceSpellNo 
                        and delivery.EpisodeNo = Encounter.SourceEncounterNo
                        )
            then '140' --delivery
            else '130'
            end

            ,Encounter.MergeEncounterRecno
      from
            WarehouseOLAPMergedV2.APC.BaseEncounter Encounter

      inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference EncounterReference
      on    EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

      inner join WarehouseOLAPMergedV2.WH.Calendar CalendarEpisodeEndDate
      on    EncounterReference.EpisodeEndDateID = CalendarEpisodeEndDate.DateID

      inner join WarehouseOLAPMergedV2.WH.Calendar CalendarDischargeDate
      on    EncounterReference.DischargeDateID = CalendarDischargeDate.DateID

      left join WarehouseOLAPMergedV2.WH.Member AdmissionMethodNational
      on    AdmissionMethodNational.SourceValueID = EncounterReference.AdmissionMethodID

      where
            (
                  CalendarEpisodeEndDate.TheDate between @from and @to
            or    CalendarDischargeDate.TheDate  between @from and @to
            )

      and Encounter.DateOfBirth is not null
      and Reportable = 1
      
      exec CDS.BuildAngliaType010 @CDSProtocolIdentifier = @CDSProtocolIdentifier

end

----unfinished
--if @CDSBulkReplacementGroupCode = '020' 
--begin
--
--    -- CDSType 190
--    if datepart(day, @to) = 31 and datepart(month, @to) = 3
--    begin
--          insert into wrkCDS
--          (
--                CDSTypeCode
--                ,EncounterRecno
--          )
--          select
--                '190'
--                ,EncounterRecno
--          from
--                dbo.APCEncounter Encounter
--          where
--                Encounter.EpisodeStartDate between 
--                      dateadd(day, 1, dateadd(year, -1, @to)) -- 1st April this financial year
--                and   @to
--          and   (
--                      Encounter.EpisodeEndDate > @to
--                or    Encounter.EpisodeEndDate is null
--                )
--
--          select
--                @rowsInserted = @rowsInserted + @@rowcount
--
--    end
--
--    exec dbo.BuildCDSType190
--end
--
--

--outpatient

if @CDSBulkReplacementGroupCode = '060' 
begin
select @CDSProtocolIdentifier = '020'
      -- CDSType 020
      insert into CDS.wrkCDS
      (
            CDSTypeCode
            ,MergeEncounterRecno
      )
      select
            '020'
            ,Encounter.MergeEncounterRecno
      from
            WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

      inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference EncounterReference
      on    EncounterReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

      inner join WarehouseOLAPMergedV2.WH.Consultant SourceConsultant
      on    SourceConsultant.SourceConsultantID = EncounterReference.ConsultantID

      left join WarehouseOLAPMergedV2.WH.Specialty SourceSpecialty
      on    SourceSpecialty.SourceSpecialtyID = EncounterReference.SpecialtyID

      left join WarehouseOLAPMergedV2.OP.FirstAttendance SourceFirstAttendance
      on    SourceFirstAttendance.SourceFirstAttendanceID = EncounterReference.FirstAttendanceID
      
      inner join WarehouseOLAPMergedV2.WH.Calendar CalendarAppointmentDate
      on    CalendarAppointmentDate.DateID = EncounterReference.AppointmentDateID

      --inner join ReferenceMap.Map.ValueHierarchy DNALocal
      --on  DNALocal.SourceValueID = Encounter.DNAID
      --and DNALocal.LocalValueID not in
      --          (
      --          1634412 --APPOINTMENT cancelled by, or on behalf of, the PATIENT 
      --          ,1634415 --APPOINTMENT cancelled or postponed by the Health Care Provider 
      --          )

      inner join WarehouseOLAPMergedV2.OP.AppointmentType
      on EncounterReference.AppointmentTypeID = AppointmentType.SourceAppointmentTypeID
      
      where
            dateadd(day, datediff(day, 0, CalendarAppointmentDate.TheDate), 0) BETWEEN @from AND @to
      and   Encounter.CancelledByCode is null
      and   Encounter.AppointmentOutcomeCode <> 'NR'
      and   Encounter.DateOfBirth IS NOT NULL
      and   not ( -- Central exclusions
                        Encounter.ContextCode = 'CEN||PAS'
                  and   SourceConsultant.SourceConsultantCode = 'CRF' --CMFT CLINICAL RESEARCH FACILITY
                  )
      and   not   ( -- Trafford exclusions
                        Encounter.ContextCode = 'TRA||UG'
                  and   (
                              SourceFirstAttendance.SourceFirstAttendanceCode not in ('1', '2') -- Include only First and FU Attendances (careful with the double negative!)
                        or    upper(SourceSpecialty.SourceSpecialtyCode) in ('MAN', 'CARDRESP', 'CMFT') --Remove central manchester from Trafford CDS
                        )
                  )
      and SourceAppointmentType not in ('Email','Fax')
      --select
      --    '020'
      --    ,EncounterRecno
      --from
      --    WarehouseOLAPMerged.OP.Base Encounter

      --inner join ReferenceMap.Map.Value Consultant
      --on  Consultant.ValueID = Encounter.ConsultantID

      --inner join ReferenceMap.Map.AttributeXref DNALocal
      --on  DNALocal.ValueID = Encounter.DNAID
      --and DNALocal.ValueXrefID in
      --          (
      --          1634412 --APPOINTMENT cancelled by, or on behalf of, the PATIENT 
      --          ,1634415 --APPOINTMENT cancelled or postponed by the Health Care Provider 
      --          )

      --where
      --    dateadd(day, datediff(day, 0, Encounter.AppointmentDate), 0) between @from and @to
      --and Consultant.ValueCode <> 'CRF' --CLINICAL RESEARCH FACILITY
      ----and     Encounter.CancelledByCode is null
      ----and     AttendanceOutcome.ValueCode <> 'NR'



      exec CDS.BuildAngliaType060 @CDSProtocolIdentifier = @CDSProtocolIdentifier
end

if @CDSBulkReplacementGroupCode = '140' --A&E
begin

      -- CDSType 010
      insert into CDS.wrkCDS
      (
            CDSTypeCode
            ,MergeEncounterRecno
      )
      select
            '010'
            ,Encounter.MergeEncounterRecno
      from
            WarehouseOLAPMergedV2.AE.BaseEncounter Encounter
      where
            cast(Encounter.ArrivalDate as date) between @from and @to
      and   Encounter.InterfaceCode <> 'PAS'
      and   Encounter.Reportable = 1
      and Encounter.DateOfBirth is not null
      and Encounter.DepartureTime is not null
--    and   Encounter.AttendanceCategoryCode <> 1 --remove clinic attenders

      exec CDS.BuildAngliaType140 @CDSProtocolIdentifier = @CDSProtocolIdentifier
end

-- MH 04.02.13 
-- Additional argument (@UpdateParameterTable) to Update the parameter table
-- Introduced due to the Christie Portal process using this procedure and not wishing  
-- to cloud when the "true" CDS extract was run.
if (@UpdateParameterTable = 1)
  BEGIN
      update Warehouse.dbo.Parameter
      set
            DateValue = getdate ()
      where
            Parameter = 'LASTCDS' + @CDSBulkReplacementGroupCode + 'EXTRACTDATE'

      if @@rowcount = 0
            insert into Warehouse.dbo.Parameter
            (
            Parameter
            ,DateValue
            )
            select
                  'LASTCDS' + @CDSBulkReplacementGroupCode + 'EXTRACTDATE'
                  ,DateValue = getdate ()
  END

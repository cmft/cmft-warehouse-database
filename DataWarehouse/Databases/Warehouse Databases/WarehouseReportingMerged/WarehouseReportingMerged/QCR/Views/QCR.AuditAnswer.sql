﻿
CREATE view [QCR].[AuditAnswer] as

SELECT [AuditAnswerRecno]
      ,[SourceUniqueID]
      ,[ContextCode]
      ,[ContextID]
      ,[AuditTime]
      ,[AuditDate]
      ,[AuditDateID]
      ,[LocationCode]
      ,[LocationID]
      ,[WardID]
      ,[WardCode]
      ,[DirectorateCode]
      ,[AuditTypeCode]
      ,[AuditTypeID]
      ,[QuestionCode]
      ,[QuestionID]
      ,[Answer]
from
	WarehouseOLAPMergedV2.QCR.AuditAnswer



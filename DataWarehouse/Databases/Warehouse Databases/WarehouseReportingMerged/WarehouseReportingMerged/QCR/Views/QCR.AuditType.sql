﻿




CREATE view [QCR].[AuditType] as

SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAuditTypeID]
      ,[SourceAuditTypeCode]
      ,[SourceAuditType]
      ,[LocalAuditTypeID]
      ,[LocalAuditTypeCode]
      ,[LocalAuditType]
      ,[NationalAuditTypeID]
      ,[NationalAuditTypeCode]
      ,[NationalAuditType]
  FROM [WarehouseOLAPMergedV2].[QCR].[AuditType]









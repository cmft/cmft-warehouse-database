﻿




CREATE view [QCR].[Location] as

SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceLocationID]
      ,[SourceLocationCode]
      ,[SourceLocation]
      ,[LocalLocationID]
      ,[LocalLocationCode]
      ,[LocalLocation]
      ,[NationalLocationID]
      ,[NationalLocationCode]
      ,[NationalLocation]
      ,[SourceDivisionCode]
      ,[SourceLocationTypeCode]
  FROM [WarehouseOLAPMergedV2].[QCR].[Location]


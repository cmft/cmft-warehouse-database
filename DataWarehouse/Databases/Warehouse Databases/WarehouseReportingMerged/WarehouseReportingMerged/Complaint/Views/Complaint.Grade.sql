﻿


CREATE view [Complaint].[Grade] as
SELECT   [SourceContextCode]
      ,[SourceContext]
      ,[SourceGradeID]
      ,[SourceGradeCode]
      ,[SourceGrade]
  FROM [WarehouseOLAPMergedV2].[Complaint].[Grade]

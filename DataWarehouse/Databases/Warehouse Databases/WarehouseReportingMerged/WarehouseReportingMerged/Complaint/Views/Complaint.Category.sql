﻿

CREATE view [Complaint].[Category] as
SELECT   [SourceContextCode]
      ,[SourceContext]
      ,[SourceCategoryID]
      ,[SourceCategoryCode]
      ,[SourceCategory]
  FROM [WarehouseOLAPMergedV2].[Complaint].[Category]

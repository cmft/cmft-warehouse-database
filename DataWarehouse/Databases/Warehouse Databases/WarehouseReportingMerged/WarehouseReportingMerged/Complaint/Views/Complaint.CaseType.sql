﻿

CREATE view [Complaint].[CaseType] as
SELECT   [SourceContextCode]
      ,[SourceContext]
      ,[SourceCaseTypeID]
      ,[SourceCaseTypeCode]
      ,[SourceCaseType]
  FROM [WarehouseOLAPMergedV2].[Complaint].[CaseType]

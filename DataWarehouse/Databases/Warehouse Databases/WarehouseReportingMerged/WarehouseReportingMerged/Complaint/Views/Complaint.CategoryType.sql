﻿

CREATE view [Complaint].[CategoryType] as
SELECT  [SourceContextCode]
      ,[SourceContext]
      ,[SourceCategoryTypeID]
      ,[SourceCategoryTypeCode]
      ,[SourceCategoryType]
  FROM [WarehouseOLAPMergedV2].[Complaint].[CategoryType]

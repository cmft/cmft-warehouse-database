﻿








create view [WH].[Question] as

SELECT  [SourceContextCode]
      ,[SourceContext]
      ,[SourceQuestionID]
      ,[SourceQuestionCode]
      ,[SourceQuestion]
      ,[LocalQuestionID]
      ,[LocalQuestionCode]
      ,[LocalQuestion]
      ,[NationalQuestionID]
      ,[NationalQuestionCode]
      ,[NationalQuestion]
  FROM [WarehouseOLAPMergedV2].[WH].[Question]











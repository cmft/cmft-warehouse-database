﻿

create view WH.Duration as
SELECT 
      [DurationID]
	,[DurationCode]
      ,[Duration]
      ,[APCWLDurationBandCode]
      ,[APCWLDurationBand]
      ,[APCWLBreach18BandCode]
      ,[APCWLBreach18Band]
      ,[APCWLBreach26BandCode]
      ,[APCWLBreach26Band]
      ,[OPWLDurationBandCode]
      ,[OPWLDurationBand]
      ,[OPWLBreachBandCode]
      ,[OPWLBreachBand]
  FROM [WarehouseOLAPMergedV2].[WH].[Duration]

﻿CREATE view [WH].[ContractSpecialty]

as

SELECT 
	ContractSpecialtyID =		AllocationID
   ,ContractSpecialtyCode =	SourceAllocationID
   ,ContractSpecialty =		Allocation

FROM [WarehouseOLAPMergedV2].[Allocation].[Allocation]
  
where
	AllocationTypeID = 7
and	Active = 1

﻿


CREATE view [WH].[Department] as


SELECT  [SourceContextCode]
      ,[SourceContext]
      ,[SourceDepartmentID]
      ,[SourceDepartmentCode]
      ,[SourceDepartment]
      ,[OtherValueID]
      ,[OtherValueCode]
      ,[OtherValue]
  FROM [WarehouseOLAPMergedV2].[WH].[Department]


﻿create view WH.Operation as

select
	 OperationID
	,OperationCode
	,Operation
	,OperationGroupCode
	,OperationGroup
	,OperationChapter
from
	WarehouseOLAPMergedV2.WH.Operation




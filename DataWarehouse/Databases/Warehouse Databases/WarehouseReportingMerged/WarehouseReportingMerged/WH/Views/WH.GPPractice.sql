﻿-- use WarehouseReportingMerged

Create View WH.GPPractice

as

select 
	GpPracticeID	
	,GpPracticeCode	
	,GpPracticeName	
	,Postcode	
	,PracticeOpenDate	
	,PracticeCloseDate	
	,ParentOrganisationCode	
	,JoinParentDate	
	,LeftParentDate	
	,ElectronicCorrespondanceActivatedTime	
	,ElectronicCorrespondanceDeactivatedTime
from 
	WarehouseOLAPMergedV2.WH.GPPractice

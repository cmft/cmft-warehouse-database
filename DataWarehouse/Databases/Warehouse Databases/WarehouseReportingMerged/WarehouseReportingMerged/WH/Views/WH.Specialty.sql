﻿

CREATE view [WH].[Specialty] as

SELECT 
	SourceContextCode
	,SourceContext
	,SourceSpecialtyID
	,SourceSpecialtyCode
	,SourceSpecialty
	,LocalSpecialtyCode
	,LocalSpecialty
	,NationalSpecialtyCode
	,NationalSpecialty
	,NationalSpecialtyLabel
FROM
	WarehouseOLAPMergedV2.WH.Specialty



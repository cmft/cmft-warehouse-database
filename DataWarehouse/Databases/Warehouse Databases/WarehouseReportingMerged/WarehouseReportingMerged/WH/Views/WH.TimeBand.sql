﻿
create view [WH].[TimeBand] as
SELECT [TimeBandCode]
      ,[MinuteBand]
      ,[FiveMinuteBand]
      ,[FifteenMinuteBand]
      ,[ThirtyMinuteBand]
      ,[HourBand]
  FROM [WarehouseOLAPMergedV2].[WH].[TimeBand]


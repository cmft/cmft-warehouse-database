﻿

create view [WH].[Directorate] as
select
 
	 [DirectorateID]
	,[DirectorateCode]
	,[Directorate]
	,[Division]
FROM 
	[WarehouseOLAPMergedV2].[WH].[Directorate]

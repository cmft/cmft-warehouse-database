﻿Create View WH.OrganisationType

as

select 
	OrganisationTypeID
	,OrganisationTypeCode
	,OrganisationType
from 
	WarehouseOLAPMergedV2.WH.OrganisationType

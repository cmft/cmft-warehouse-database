﻿
create view WH.Attribute as

select
	AttributeID
	,AttributeCode
	,Attribute
from
	WarehouseOLAPMergedV2.WH.Attribute
﻿create view [WH].[ContractHRG]

as

SELECT 
	ContractHRGID =		AllocationID
   ,ContractHRGCode =	SourceAllocationID
   ,ContractHRG =		Allocation

FROM [WarehouseOLAPMergedV2].[Allocation].[Allocation]
  
where
	AllocationTypeID = 6
and	Active = 1

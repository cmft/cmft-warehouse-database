﻿
create view [WH].[Context] as
SELECT [ContextID]
      ,[ContextCode]
      ,[Context]
      ,[Location]
  FROM [WarehouseOLAPMergedV2].[WH].[Context]


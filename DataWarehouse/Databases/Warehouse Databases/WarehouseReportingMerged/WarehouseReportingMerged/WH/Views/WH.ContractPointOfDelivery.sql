﻿CREATE view [WH].[ContractPointOfDelivery]

as

SELECT 
	ContractPointOfDeliveryID =		AllocationID
   ,ContractPointOfDeliveryCode =	SourceAllocationID
   ,ContractPointOfDelivery =		Allocation

FROM [WarehouseOLAPMergedV2].[Allocation].[Allocation]
  
where
	AllocationTypeID = 5
and	Active = 1

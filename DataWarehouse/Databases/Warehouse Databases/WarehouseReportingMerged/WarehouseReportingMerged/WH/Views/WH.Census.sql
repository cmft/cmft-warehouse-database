﻿

create view WH.Census as

SELECT [CensusDateID]
      ,[CensusDate]
      ,[Census]
      ,[LastInMonth]
      ,[CurrentCensus]
  FROM [WarehouseOLAPMergedV2].[WH].[Census]

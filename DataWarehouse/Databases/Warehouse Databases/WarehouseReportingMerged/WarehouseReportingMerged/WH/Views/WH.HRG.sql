﻿
Create view [WH].[HRG]

as

SELECT [HRGID]
      ,[HRGCode]
      ,[HRG]
      ,[HRGChapter]
      ,[HRGSubChapter]
      ,[HighCost]
  FROM [WarehouseOLAPMergedV2].[WH].[HRG]


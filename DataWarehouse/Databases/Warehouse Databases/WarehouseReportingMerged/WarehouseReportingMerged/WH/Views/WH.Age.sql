﻿
create view [WH].[Age] as
SELECT [AgeCode]
      ,[Age]
      ,[AgeParentCode]
  FROM [WarehouseOLAPMergedV2].[WH].[Age]


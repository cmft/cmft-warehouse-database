﻿CREATE TABLE [CYPHS].[SubmissionPeriod] (
    [SubmissionPeriodID] INT           NOT NULL,
    [SubmissionPeriod]   VARCHAR (20)  NOT NULL,
    [FromDate]           DATE          NOT NULL,
    [ToDate]             DATE          NOT NULL,
    [FreezeDate]         DATE          NOT NULL,
    [Created]            DATETIME2 (0) CONSTRAINT [DF_CYPHS_SubmissionPeriod_Created] DEFAULT (getdate()) NULL,
    [Updated]            DATETIME2 (0) NULL,
    [ByWhom]             VARCHAR (50)  CONSTRAINT [DF_CYPHS_SubmissionPeriod_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_SubmissionPeriod] PRIMARY KEY CLUSTERED ([SubmissionPeriodID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CYPHS_SubmissionPeriod_SubmissionPeriod]
    ON [CYPHS].[SubmissionPeriod]([SubmissionPeriod] ASC);


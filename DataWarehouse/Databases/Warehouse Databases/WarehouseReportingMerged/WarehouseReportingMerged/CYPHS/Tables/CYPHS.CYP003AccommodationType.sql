﻿CREATE TABLE [CYPHS].[CYP003AccommodationType] (
    [C003901]          VARCHAR (20) NULL,
    [C003010]          VARCHAR (4)  NULL,
    [C003020]          VARCHAR (10) NULL,
    [SubmissionPeriod] VARCHAR (20) NULL
);


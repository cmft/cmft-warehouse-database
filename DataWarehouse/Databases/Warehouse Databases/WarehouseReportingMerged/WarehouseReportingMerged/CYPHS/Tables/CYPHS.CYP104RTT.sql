﻿CREATE TABLE [CYPHS].[CYP104RTT] (
    [C104902]          VARCHAR (20)  NOT NULL,
    [C104010]          VARCHAR (12)  NULL,
    [C104020]          VARCHAR (20)  NULL,
    [C104030]          VARCHAR (5)   NULL,
    [C104040]          VARCHAR (2)   NULL,
    [C104050]          VARCHAR (10)  NULL,
    [C104060]          VARCHAR (10)  NULL,
    [C104070]          VARCHAR (2)   NULL,
    [SubmissionPeriod] VARCHAR (20)  NULL,
    [Created]          DATETIME2 (0) CONSTRAINT [DF_CYPHS_CYP104RTT_Created] DEFAULT (getdate()) NULL,
    [ByWhom]           VARCHAR (50)  CONSTRAINT [DF_CYPHS_CYP104RTT_ByWhom] DEFAULT (suser_name()) NULL
);


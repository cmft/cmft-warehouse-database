﻿CREATE TABLE [CYPHS].[CYP002GP] (
    [C002901]          VARCHAR (20)  NOT NULL,
    [C002010]          VARCHAR (6)   NOT NULL,
    [C002020]          VARCHAR (10)  NULL,
    [C002030]          VARCHAR (10)  NULL,
    [C002040]          VARCHAR (3)   NULL,
    [SubmissionPeriod] VARCHAR (20)  NULL,
    [Created]          DATETIME2 (0) CONSTRAINT [DF_CYPHS_CYP002GP_Created] DEFAULT (getdate()) NULL,
    [ByWhom]           VARCHAR (50)  CONSTRAINT [DF_CYPHS_CYP002GP_ByWhom] DEFAULT (suser_name()) NULL
);


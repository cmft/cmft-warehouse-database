﻿CREATE TABLE [CYPHS].[CYP301GroupSession] (
    [C301010]          VARCHAR (20)  NOT NULL,
    [C301020]          VARCHAR (10)  NOT NULL,
    [C301912]          VARCHAR (5)   NOT NULL,
    [C301030]          VARCHAR (4)   NULL,
    [C301040]          VARCHAR (2)   NULL,
    [C301050]          VARCHAR (3)   NULL,
    [C301909]          VARCHAR (3)   NULL,
    [C301906]          VARCHAR (9)   NULL,
    [C301060]          VARCHAR (20)  NULL,
    [C301905]          VARCHAR (10)  NULL,
    [SubmissionPeriod] VARCHAR (20)  NULL,
    [Created]          DATETIME2 (0) CONSTRAINT [DF_CYPHS_CYP301GroupSession_Created] DEFAULT (getdate()) NULL,
    [ByWhom]           VARCHAR (50)  CONSTRAINT [DF_CYPHS_CYP301GroupSession_ByWhom] DEFAULT (suser_name()) NULL
);


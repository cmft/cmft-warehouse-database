﻿CREATE TABLE [CYPHS].[CYP101Referral] (
    [C101902]          VARCHAR (20)  NOT NULL,
    [C101901]          VARCHAR (20)  NOT NULL,
    [C101912]          VARCHAR (5)   NOT NULL,
    [C101010]          VARCHAR (10)  NULL,
    [C101020]          VARCHAR (8)   NULL,
    [C101905]          VARCHAR (10)  NULL,
    [C101030]          VARCHAR (2)   NULL,
    [C101040]          VARCHAR (6)   NULL,
    [C101050]          VARCHAR (3)   NULL,
    [C101060]          VARCHAR (1)   NULL,
    [C101070]          VARCHAR (3)   NULL,
    [C101080]          VARCHAR (10)  NULL,
    [C101090]          VARCHAR (10)  NULL,
    [SubmissionPeriod] VARCHAR (20)  NULL,
    [Created]          DATETIME2 (0) CONSTRAINT [DF_CYPHS_CYP101Referral_Created] DEFAULT (getdate()) NULL,
    [ByWhom]           VARCHAR (50)  CONSTRAINT [DF_CYPHS_CYP101Referral_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_CYPHS_CYP101Referral] PRIMARY KEY CLUSTERED ([C101902] ASC)
);


﻿









CREATE view [CYPHS].[SubmissionSummary]

as 

select

		  Type
		 ,TotalSent = max(TotalSent)
		 ,TotalNotSent = max(TotalNotSent)
		 ,SubMissionPeriod 

from

(

select
		  Type
		 ,TotalSent = Count (*)
		 ,TotalNotSent = 1
		 ,SubMissionPeriod = ( Select max(SubMissionPeriod) from CYPHS.CYP001MPI )

		from
		(

			select Type = 'CYP001MPI', LocalPatientId = C001901 from CYPHS.CYP001MPI 
		where exists
		
		(
		Select 1 from CYPHS.CYP002GP CYP002GP
		where CYP002GP.C002901 = CYP001MPI.C001901
		
		)			
		union
			select Type = 'CYP002GP', LocalPatientId = C002901 from CYPHS.CYP002GP
		where exists
		
		(
		Select 1 from CYPHS.CYP001MPI 
		where CYP002GP.C002901 = CYP001MPI.C001901			
		)
				union
		select Type = 'CYP201CareContact', LocalPatientId = null from CYPHS.CYP201CareContact	
		) a
		

		
		
	
	

		
		group by Type

union all

select
		  Type
		 ,TotalSent = 0
		 ,TotalNotSent = Count (*)
		 ,SubMissionPeriod = ( Select max(SubMissionPeriod) from CYPHS.CYP001MPI )		 
		
		from
		(

			select Type = 'CYP001MPI', LocalPatientId = C001901 from CYPHS.CYP001MPI 
		where not exists
		
		(
		Select 1 from CYPHS.CYP002GP CYP002GP
		where CYP002GP.C002901 = CYP001MPI.C001901
		
		)
		union
			select Type = 'CYP002GP', LocalPatientId = C002901 from CYPHS.CYP002GP
		where not exists
		
		(
		Select 1 from CYPHS.CYP001MPI 
		where CYP002GP.C002901 = CYP001MPI.C001901			
		)
		union
		select Type = 'CYP201CareContact', LocalPatientId = null from CYPHS.CYP201CareContact
		
			
		) a
		
		
		
		
		group by Type
		
) SubmittedCount	


group by Type, 	SubMissionPeriod










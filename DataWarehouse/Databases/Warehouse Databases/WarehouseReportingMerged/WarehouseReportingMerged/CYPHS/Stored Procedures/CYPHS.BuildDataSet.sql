﻿




CREATE proc [CYPHS].[BuildDataSet] 
	@SubmissionPeriod varchar(20)
as

/* 
==============================================================================================
Description:

When		Who			What
20151223	Paul Egan	CYP301GroupSession sdded
20160331	Paul Egan	CYP101Referral split into two inserts to improve performance
===============================================================================================
*/

declare
	 @FromDate date
	,@ToDate date


if exists
	(
	select
		1
	from
		CYPHS.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	and	FreezeDate > getdate()
	)
begin
	(
	select
		 @FromDate = SubmissionPeriod.FromDate
		,@ToDate = SubmissionPeriod.ToDate 
	from
		CYPHS.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	)		
end

else

begin
	print 'Invalid submission period'
	return
end






	 
truncate table CYPHS.CYP201CareContact
truncate table CYPHS.CYP104RTT
truncate table CYPHS.CYP101Referral
truncate table CYPHS.CYP001MPI
truncate table CYPHS.CYP002GP
truncate table CYPHS.CYP003AccommodationType
truncate table CYPHS.CYP301GroupSession

/* TempStep to get missing GP info */
select 
	 NHSnumber
	,PracticeCode
	,GPStartDate = PCTStartDate
	
into #ChildHealthGP

from 
	WarehouseSQL.[Warehouse].[ChildHealth].[Entity] Patient
  
	inner join WarehouseSQL.[Warehouse].[ChildHealth].[EntityGP] GP
	on GP.EntitySourceUniqueID = Patient.SourceUniqueID
  
where 
	Patient.TypeCode = 'P'
	and nhsnumber is not null
;

print 'Clear Tables complete' 



/* ================== CYP201CareContact TFS 6538 ==================*/

insert into WarehouseReportingMerged.CYPHS.CYP201CareContact
	(
	 C201903
	,C201902
	,C201010
	,C201020
	,C201030
	,C201912
	,C201040
	,C201050
	,C201060
	,C201070
	,C201080
	,C201909
	,C201906
	,C201090
	,C201100
	,C201110
	,C201120
	,C201130
	,C201140
	,C201150
	,C201160
	,SubmissionPeriod
	)
	
select 
	 C201903 = Encounter.SourceEncounterID	-- NB 253 duplicates in LIVE, needs to be unique for this submission, need to investigate
	,C201902 = Encounter.ReferralID		
	,C201010 = Encounter.StaffTeamID	-- Or Encounter.ReferredToStaffTeamID ?
	,C201020 = cast(Encounter.StartDate as date)
	,C201030 = cast(Encounter.StartTime as time(0))
	,C201912 = Encounter.CommissionerCode
	,C201040 =
		case AdministrativeCategory.NationalAdministrativeCategoryCode		-- ADMINISTRATIVE CATEGORY CODE
			when 'N||ADMCAT' then null
			else AdministrativeCategory.NationalAdministrativeCategoryCode
		end
	,C201050 = Encounter.EncounterDuration
	,C201060 = right('0' + cast(Encounter.DerivedFirstAttendanceFlag as varchar(2)), 2)
	,C201070 = 		
		case
			when CaseType.CaseType like '%proxy%' then '02'
			when CaseType.CaseType like '%client%' then '01'
			else '01'
		end
	,C201080 = 		
		case
			when CaseType.CaseType like '%face%' then '01'
			when CaseType.CaseType like '%phone%' then '02'
			when CaseType.CaseType like '%telemedicine web camera%' then '03'
			when CaseType.CaseType like '%talk type%' then '04'
			when CaseType.CaseType like '%email%' then '05'
			when CaseType.CaseType like '%sms%' then '06'
			when CaseType.CaseType like '%group%' then '01'
			when Encounter.ContactTypeID = 1452 then '01'	 -- Group session (LookupID 'SATYP')
			when CaseType.CaseType like '%Other Client linked activity%' then '02'
			else '98' --Other
		end
	,C201909 = 
		case LocationType.NationalLocationTypeCode -- ACTIVITY LOCATION TYPE CODE  -- Email sent to iPM system manager -- Being mapped by Carol
			when 'N||COMLOCTYPE' then null
			else LocationType.NationalLocationTypeCode
		end
	,C201906 = null		-- SITE CODE (OF TREATMENT)
	,C201090 = 		-- GROUP THERAPY INDICATOR
		case
			when CaseType.CaseTypeGroup is null then 'Z'
			when CaseType.CaseTypeGroup like '%group%' then 'Y'
			else 'N'
		end
	,C201100 = AttendanceStatusBase.NHSAttendanceStatusCode	-- ATTENDED OR DID NOT ATTEND CODE
	,C201110 = null		-- EARLIEST REASONABLE OFFER DATE
	,C201120 = null		-- EARLIEST CLINICALLY APPROPRIATE DATE
	,C201130 = cast(Encounter.CanceledTime as date)			-- CARE CONTACT CANCELLATION DATE
	,C201140 = null		-- CancellationReason.NHSCancellationReasonCode	-- CARE CONTACT CANCELLATION REASON (includes '9's)
	,C201150 = null
	,C201160 = null
	,SubmissionPeriod = @SubmissionPeriod
from
	COM.Encounter Encounter
	
	left join COM.EncounterReference
	on	Encounter.MergeEncounterRecno = EncounterReference.MergeEncounterRecno
	
	left join COM.CaseTypeBase CaseType		-- This is contact method / consultation medium
	on	CaseType.CaseTypeID = Encounter.ContactTypeID
	
	left join COM.AttendanceStatusBase
	on	AttendanceStatusBase.AttendanceStatusID = Encounter.AttendedID

	left join COM.LocationType LocationType
	on	LocationType.LocationTypeID = Encounter.LocationTypeID
	
	left join WH.AdministrativeCategory
	on AdministrativeCategory.SourceAdministrativeCategoryID = EncounterReference.AdministrativeCategoryID

where
	----------encounter.JointActivityFlag <> 1										-- CIDS Excluded for now, as there are missing BaseEncounterReference rows (hence missing in cube). They all seem to be joint activity (at least for 2014Q1)
	----------and CaseType.CaseType not like '%Other Client linked activity%'		-- CIDS As instructed by Gareth Summerfield
	Encounter.StartDate between @FromDate and @ToDate
	and Encounter.Archived = 'n'
	and Encounter.AgeCode <= '18 Years'
;

print 'CYP201CareContact complete'



/* ================== CYP104RTT TFS 6524 ==================*/

insert into WarehouseReportingMerged.CYPHS.CYP104RTT
	(
	 C104902
	,C104010
	,C104020
	,C104030
	,C104040
	,C104050
	,C104060
	,C104070
	,SubmissionPeriod
	)
select
	 C104902 = Referral.SourceUniqueID						--SERVICE REQUEST IDENTIFIER
	,C104010 = null											--UNIQUE BOOKING REFERENCE NUMBER (CONVERTED)
	,C104020 = Referral.RTTPatientPathwayID					--PATIENT PATHWAY IDENTIFIER
	,C104030 = null											--ORGANISATION CODE (PATIENT PATHWAY IDENTIFIER ISSUER)
	,C104040 = null											--WAITING TIME MEASUREMENT TYPE
	,C104050 = cast(Referral.RTTStartTime as date)			--REFERRAL TO TREATMENT PERIOD START DATE
	,C104060 = null											--REFERRAL TO TREATMENT PERIOD END DATE
	,C104070 = RTTPeriodStatusBase.RTTPeriodStatusCode		--REFERRAL TO TREATMENT PERIOD STATUS
	,SubmissionPeriod = @SubmissionPeriod
from
	COM.Referral
	
	left join COM.RTTPeriodStatusBase
	on	RTTPeriodStatusBase.RTTPeriodStatusID = Referral.RTTStatusID
	
where
	Referral.ReferralReceivedDate between @FromDate and @ToDate
	and Referral.AgeCode <= '18 Years'
	-- Need an exists here to Referrals rather than above criteria? 
;


print 'CYP104RTT complete'

	 
/* ================== CYP101Referral ================== */
/* Split into two insert statements for performance reason */

/* Submission period referrals */
insert into WarehouseReportingMerged.CYPHS.CYP101Referral
	(
	 C101902
	,C101901
	,C101912
	,C101010
	,C101020
	,C101905
	,C101030
	,C101040
	,C101050
	,C101060
	,C101070
	,C101080
	,C101090
	,SubmissionPeriod 
	)
select 
	 C101902 = Encounter.SourceUniqueID --	SERVICE REQUEST IDENTIFIER
	,C101901 = Encounter.PatientSourceID --	LOCAL PATIENT IDENTIFIER (EXTENDED)
	,C101912 = 'RW3' --	ORGANISATION CODE (CODE OF COMMISSIONER)
	,C101010 = CONVERT(VARCHAR(10),Encounter.ReferralReceivedDate,120) --	REFERRAL REQUEST RECEIVED DATE
	,C101020 = left(CONVERT(VARCHAR(8),ReferralReceivedTime,108),8)    --	REFERRAL REQUEST RECEIVED TIME
	,C101905 = '' --	NHS SERVICE AGREEMENT LINE NUMBER
	,C101030 = case when ReferralSource.NationalSourceOfReferralCode = 'N||COMREFSRCE' then '99' else ReferralSource.NationalSourceOfReferralCode  end
	--ReferralSourceBase.ReferralSourceCode --	SOURCE OF REFERRAL FOR COMMUNITY --mapping needed
	,C101040 =  
		case
			when ReferralSource.NationalSourceOfReferralCode in ('01') then  isnull(PatientReferralRegisteredPracticeCode,'RW3')
			when ReferralSource.NationalSourceOfReferralCode in ('05','06') then 'RW3'
			else  ''
		end
	--	REFERRING ORGANISATION CODE
	,C101050 = '' --StaffTeam.StaffTeamCode --	REFERRING CARE PROFESSIONAL STAFF GROUP (COMMUNITY CARE) --mapping needed
	,C101060 = left(PriorityType.ReferralPriorityTypeCode,1) --	PRIORITY TYPE CODE
	,C101070 = '' --	PRIMARY REASON FOR REFERRAL (COMMUNITY CARE)
	,C101080 = isnull(cast(CONVERT(VARCHAR(10),Encounter.ReferralClosedDate,120) as varchar),'')--	SERVICE DISCHARGE DATE
	,C101090 = '' --	DISCHARGE LETTER ISSUED DATE (MENTAL HEALTH AND COMMUNITY CARE)
	,SubmissionPeriod = @SubmissionPeriod
from

	WarehouseOLAPMergedV2.COM.BaseReferral Encounter

	inner join WarehouseOLAPMergedV2.COM.BaseReferralReference ReferralReference
	on ReferralReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.COM.ReferralSource ReferralSource
	on ReferralSource.SourceSourceOfReferralID = ReferralReference.ReferralSourceID

	inner join WarehouseOLAPMergedV2.COM.StaffTeam StaffTeam
	on StaffTeam.SourceValueID = ReferralReference.ReferredByStaffTeamID

	inner join WarehouseOLAPMergedV2.Com.PriorityType PriorityType
	on PriorityType.ReferralPriorityTypeID = Encounter.Priority

where 
	(
	AgeCode <= '18 Years'
	and
	ReferralReceivedDate between @FromDate and @ToDate
	and 
	(Encounter.PatientDateOfBirth <= @FromDate or Encounter.PatientDateOfBirth is null)
	)
;

print 'CYP101Referral - Inside submission period complete'

/* Referrals outside submission period with care contact inside submission period */
insert into WarehouseReportingMerged.CYPHS.CYP101Referral
	(
	 C101902
	,C101901
	,C101912
	,C101010
	,C101020
	,C101905
	,C101030
	,C101040
	,C101050
	,C101060
	,C101070
	,C101080
	,C101090
	,SubmissionPeriod 
	)
select 
	 C101902 = Encounter.SourceUniqueID --	SERVICE REQUEST IDENTIFIER
	,C101901 = Encounter.PatientSourceID --	LOCAL PATIENT IDENTIFIER (EXTENDED)
	,C101912 = 'RW3' --	ORGANISATION CODE (CODE OF COMMISSIONER)
	,C101010 = CONVERT(VARCHAR(10),Encounter.ReferralReceivedDate,120) --	REFERRAL REQUEST RECEIVED DATE
	,C101020 = left(CONVERT(VARCHAR(8),ReferralReceivedTime,108),8)    --	REFERRAL REQUEST RECEIVED TIME
	,C101905 = '' --	NHS SERVICE AGREEMENT LINE NUMBER
	,C101030 = case when ReferralSource.NationalSourceOfReferralCode = 'N||COMREFSRCE' then '99' else ReferralSource.NationalSourceOfReferralCode  end
	--ReferralSourceBase.ReferralSourceCode --	SOURCE OF REFERRAL FOR COMMUNITY --mapping needed
	,C101040 =  
		case
			when ReferralSource.NationalSourceOfReferralCode in ('01') then  isnull(PatientReferralRegisteredPracticeCode,'RW3')
			when ReferralSource.NationalSourceOfReferralCode in ('05','06') then 'RW3'
			else  ''
		end
	--	REFERRING ORGANISATION CODE
	,C101050 = '' --StaffTeam.StaffTeamCode --	REFERRING CARE PROFESSIONAL STAFF GROUP (COMMUNITY CARE) --mapping needed
	,C101060 = left(PriorityType.ReferralPriorityTypeCode,1) --	PRIORITY TYPE CODE
	,C101070 = '' --	PRIMARY REASON FOR REFERRAL (COMMUNITY CARE)
	,C101080 = isnull(cast(CONVERT(VARCHAR(10),Encounter.ReferralClosedDate,120) as varchar),'')--	SERVICE DISCHARGE DATE
	,C101090 = '' --	DISCHARGE LETTER ISSUED DATE (MENTAL HEALTH AND COMMUNITY CARE)
	,SubmissionPeriod = @SubmissionPeriod
from

	WarehouseOLAPMergedV2.COM.BaseReferral Encounter

	inner join WarehouseOLAPMergedV2.COM.BaseReferralReference ReferralReference
	on ReferralReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

	inner join WarehouseOLAPMergedV2.COM.ReferralSource ReferralSource
	on ReferralSource.SourceSourceOfReferralID = ReferralReference.ReferralSourceID

	inner join WarehouseOLAPMergedV2.COM.StaffTeam StaffTeam
	on StaffTeam.SourceValueID = ReferralReference.ReferredByStaffTeamID

	inner join WarehouseOLAPMergedV2.Com.PriorityType PriorityType
	on PriorityType.ReferralPriorityTypeID = Encounter.Priority

where 
	exists
	(
	Select 1
	from CYPHS.CYP201CareContact CYP201CareContact
	where 
		CYP201CareContact.C201902 = Encounter.SourceUniqueID 
		and Encounter.ReferralReceivedDate not between @FromDate and @ToDate
	)
;


print 'CYP101Referral - Outside submission period complete'


/* ================== CYP001MPI ================== */

insert into WarehouseReportingMerged.CYPHS.CYP001MPI
	(
	 C001901
	,C001010
	,C001020
	,C001030
	,C001040
	,C001050
	,C001060
	,C001070
	,C001080
	,C001090
	,C001100
	,C001110
	,C001120
	,C001130
	,C001140
	,C001150
	,C001160
	,C001170
	,C001180
	,C001190
	,C001200
	,C001210
	,C001220
	,C001230
	,C001240
	,SubmissionPeriod
	)

select
	 C001901 
	,C001010 
	,C001020 
	,C001030 
	,C001040 
	,C001050 
	,C001060 
	,C001070 
	,C001080
	,C001090 
	,C001100
	,C001110 
	,C001120
	,C001130 
	,C001140
	,C001150 
	,C001160 
	,C001170
	,C001180
	,C001190 
	,C001200 
	,C001210 
	,C001220 
	,C001230 
	,C001240 
	,SubmissionPeriod 
from

	(
	select 
		 C001901 = Encounter.PatientSourceID
		,C001010 = 'RW3'
		,C001020 = ''
		,C001030 = ''
		,C001040 = Encounter.PatientNHSNumber
		,C001050 = ''
		,C001060 = CONVERT(VARCHAR(10),Encounter.PatientDateOfBirth,120) 
		,C001070 = case when left(PatientPostcode,4) = '    ' or len(PatientPostcode) <= 3 then null else PatientPostcode end
		,C001080 = Sex.NationalSexCode
		,C001090 = EthnicCategory.NationalEthnicCategoryCode
		,C001100 = ''
		,C001110 = ''
		,C001120 = ''
		,C001130 = ''
		,C001140 = ''
		,C001150 = ''
		,C001160 = ''
		,C001170 = ''
		,C001180 = ''
		,C001190 = ''
		,C001200 = CONVERT(VARCHAR(10),Encounter.PatientDateOfDeath,120)
		,C001210 = ''
		,C001220 = ''
		,C001230 = ''
		,C001240 = ''
		,SubmissionPeriod = @SubmissionPeriod
		,MostRecent = row_number ()
			over (PARTITION BY Encounter.PatientSourceID order by Encounter.Updated desc)

	from 

		WarehouseOLAPMergedV2.COM.BaseReferral Encounter

		inner join WarehouseOLAPMergedV2.COM.BaseReferralReference BaseReferralReference
		on BaseReferralReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

		inner join WarehouseOLAPMergedV2.WH.Sex Sex
		on Sex.SourceSexID = BaseReferralReference.SexID
		and Sex.SourceContextCode = Encounter.ContextCode

		inner join WarehouseOLAPMergedV2.WH.EthnicCategory EthnicCategory
		on EthnicCategory.SourceEthnicCategoryID = BaseReferralReference.EthnicCategoryID
		and EthnicCategory.SourceContextCode = Encounter.ContextCode

	where
		--ReferralReceivedDate between @FromDate and @ToDate
		--and
		exists

		(
		select 
			1 
		from 
			WarehouseReportingMerged.CYPHS.CYP101Referral CYP101Referral
		where
			CYP101Referral.C101901 = Encounter.PatientSourceID
			and CYP101Referral.C101902 = Encounter.SourceUniqueID 
		)

	) CYP001MPI

where
	MostRecent = 1
;

print 'CYP001MPI complete'


/* ================== CYP002GP ================== */

insert into WarehouseReportingMerged.CYPHS.CYP002GP
	(
	 C002901
	,C002010
	,C002020
	,C002030
	,C002040
	,SubmissionPeriod
	)
select
	 C002901 
	,C002010 
	,C002020 
	,C002030 
	,C002040 
	,@SubmissionPeriod 
from
	(
	select
		 C002901 = Encounter.PatientSourceID --	LOCAL PATIENT IDENTIFIER (EXTENDED)
		,C002010 = isnull(left(Encounter.PatientCurrentRegisteredPracticeCode,6) ,ChildHealthGP.PracticeCode)


		,C002020 = '' --StartDate
		,C002030 = '' -- EndDate
		,C002040 = '' -- ORGANISATION CODE (GP PRACTICE RESPONSIBILITY)
		--,SubmissionPeriod = @SubmissionPeriod
		,MostRecent = row_number () over (PARTITION BY Encounter.PatientSourceID order by Encounter.Updated desc)
	
	from

		WarehouseOLAPMergedV2.COM.BaseReferral Encounter

		inner join WarehouseOLAPMergedV2.COM.BaseReferralReference ReferralReference
		on ReferralReference.MergeEncounterRecno = Encounter.MergeEncounterRecno

		left join #ChildHealthGP ChildHealthGP
		on ChildHealthGP.NHSnumber = Encounter.PatientNHSNumber
		and ChildHealthGP.GPStartDate <= @ToDate

	where 

		isnull(Encounter.PatientCurrentRegisteredPracticeCode,ChildHealthGP.PracticeCode) is not null
		and
		exists
		(
		Select
		1
		from WarehouseReportingMerged.CYPHS.CYP101Referral CYP101Referral
		where CYP101Referral.C101901 = Encounter.PatientSourceID
		)

		and ReferralReceivedDate <= @ToDate

	) CYP002GP

where 
	MostRecent = 1
;

print 'CYP002GP complete'					


/* ================== CYP301GroupSession TFS 6547  ================== */

insert into WarehouseReportingMerged.CYPHS.CYP301GroupSession
	(
	 C301010
	,C301020
	,C301912
	,C301030
	,C301040
	,C301050
	,C301909
	,C301906
	,C301060
	,C301905
	,SubmissionPeriod
	)
select
	 C301010 = Encounter.SourceEncounterID			--GROUP SESSION IDENTIFIER
	,C301020 = cast(Encounter.StartDate as date)	--GROUP SESSION DATE
	,C301912 = Encounter.CommissionerCode --'RW3' --???		--ORGANISATION CODE (CODE OF COMMISSIONER)
	,C301030 = Encounter.EncounterDuration			--CLINICAL CONTACT DURATION OF GROUP SESSION
	,C301040 = null		-- ETL 'SATYP'			--GROUP SESSION TYPE CODE (COMMUNITY CARE)
	,C301050 = Encounter.ActualAttendees			--NUMBER OF GROUP SESSION PARTICIPANTS 
	,C301909 = 			-- ACTIVITY LOCATION TYPE CODE  -- Email sent to iPM system manager -- Being mapped by Carol
		case LocationType.NationalLocationTypeCode	-- (Partially mapped - waiting for Carol)
			when 'N||COMLOCTYPE' then null
			else LocationType.NationalLocationTypeCode
		end			--ACTIVITY LOCATION TYPE CODE
	,C301906 = Encounter.LocationTypeHealthOrgID			--SITE CODE (OF TREATMENT)
	,C301060 = Encounter.ProfessionalCarerID		--CARE PROFESSIONAL LOCAL IDENTIFIER
	,C301905 = null			--NHS SERVICE AGREEMENT LINE NUMBER
	,SubmissionPeriod = @SubmissionPeriod
from
	COM.Encounter
	
	left join COM.LocationType LocationType
	on	LocationType.LocationTypeID = Encounter.LocationTypeID

where
	Encounter.ContactTypeID = 1452		-- 1452 = Group session
	and Encounter.ReferralReceivedDate between @FromDate and @ToDate
	and Encounter.AgeCode <= '18 Years'
	and Encounter.CanceledTime is null		-- Submission does not require cancelled group sessions
;

print 'CYP301GroupSession complete'	


/* ================== Maintain SubmissionPeriod ================== */

update
	CYPHS.SubmissionPeriod
set
	 Updated = getdate()
	----------,ByWhom = suser_name()
where
	SubmissionPeriod = @SubmissionPeriod
;





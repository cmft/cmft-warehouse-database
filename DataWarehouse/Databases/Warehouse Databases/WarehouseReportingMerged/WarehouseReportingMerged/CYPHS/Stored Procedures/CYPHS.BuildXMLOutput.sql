﻿
CREATE proc [CYPHS].[BuildXMLOutput]  

as

SET NOCOUNT on

declare
	 @FromDate date
	,@ToDate date
	,@RecordCount int
	,@RunDateTime datetime
	,@SubmissionPeriod varchar (50)
	,@XMLNode1 varchar (max)
	,@XMLNode2 varchar (max)
	,@XMLNode3 varchar (max)
	,@XMLNode4 varchar (max)	
	,@XMLNodeMain varchar (max)	

	,@XMLNode6 varchar (max)
	,@LocalPatientId varchar (50) = null
	
	set @LocalPatientId = null --150242532
 
 set @SubmissionPeriod = (select max(SubmissionPeriod) from WarehouseReportingMerged.CYPHS.CYP001MPI)
 set @RunDateTime = left(getdate (),19)
 
 if exists
	(
	select
		1
	from
		CYPHS.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	and	FreezeDate > getdate()
	)
begin
	(
	select
		@FromDate = SubmissionPeriod.FromDate
		,@ToDate = SubmissionPeriod.ToDate
	from
		CYPHS.SubmissionPeriod
	where
		SubmissionPeriod = @SubmissionPeriod
	)		
end

else

begin
	print 'Invalid submission period'
	return
end





	set @RecordCount = 
	(
		select 

		sum(Total)+1 total

		from
		(

		select count(*) Total from CYPHS.CYP001MPI Encounter 
		union all
		select count(*) Total from CYPHS.CYP002GP Encounter 
		union all
		select count(*) Total from CYPHS.CYP003AccommodationType Encounter 
		union all
		select count(*) Total from CYPHS.CYP101Referral Encounter 
		union all
		Select count(*) from WarehouseReportingMerged.CYPHS.CYP201CareContact
		

		) Data
	)




--Create XML Header
	set @XMLNode1 =  '<?xml version="1.0" encoding="UTF-8" ?>' 
	
	set @XMLNode2 = '<CYPHS:CYPHS xmlns:CYPHS="http://www.datadictionary.nhs.uk/messages/CYPHS-v1-5" xmlns:xs="http://www.w3.org/2001/XMLSchema-instance">'

	set @XMLNode3 = '<CYP000>'

	set @XMLNode4 =  
		 
		
		cast( 
				(
				
				select
					
				 C000010 = '1.5'
				,C000020 = 'RW3'
				,C000030 = 'RW3'
				,C000040 = @FromDate
				,C000050 = @ToDate
				,C000060 = @RunDateTime
				,C000070 = @RecordCount
				
				for xml PATH(''), TYPE
				
				) as Varchar (max)
			) 
	 

--Create main xml content Mother

Select  

XMLOutPut =
	cast(
			(
			select

				(
				    SELECT 
				      C001901
					,C001010 = coalesce(C001010,'')
					,C001020 = coalesce(C001020,'')
					,C001030 = coalesce(C001030,'')
					,C001040 = coalesce(C001040,'')
					,C001050 = coalesce(C001050,'')
					,C001060 = coalesce(C001060,'')
					,C001070 = coalesce(C001070,'')
					,C001080 = coalesce(C001080,'')
					,C001090 = coalesce(C001090,'')
					,C001100 = coalesce(C001100,'')
					,C001110 = coalesce(C001110,'')
					,C001120 = coalesce(C001120,'')
					,C001130 = coalesce(C001130,'')
					,C001140 = coalesce(C001140,'')
					,C001150 = coalesce(C001150,'')
					,C001160 = coalesce(C001160,'')
					,C001170 = coalesce(C001170,'')
					,C001180 = coalesce(C001180,'')
					,C001190 = coalesce(C001190,'')
					,C001200 = coalesce(C001200,'')
					,C001210 = coalesce(C001210,'')
					,C001220 = coalesce(C001220,'')
					,C001230 = coalesce(C001230,'')
					,C001240 = coalesce(C001240,'')


					FROM WarehouseReportingMerged.CYPHS.CYP001MPI CYP001

					where

					CYP001.C001901 = Encounter.C001901

					for xml PATH(''), TYPE

				)
				,
cast(

(
	select

			

    (
	   SELECT 

		 C101902 = coalesce(C101902,'')
	    ,C101901
	    ,C101912 = coalesce(C101912,'')
	    ,C101010 = coalesce(C101010,'')
	    ,C101020 = coalesce(C101020,'')
	    ,C101905 = coalesce(C101905,'')
	    ,C101030 = coalesce(C101030,'')
	    ,C101040 = coalesce(C101040,'')
	    ,C101050 = coalesce(C101050,'')
	    ,C101060 = coalesce(C101060,'')
	    ,C101070 = coalesce(C101070,'')
	    ,C101080 = coalesce(C101080,'')
	    ,C101090 = coalesce(C101090,'')

--CYP201CareContact nested
		,cast(

			(
			select

			

			(
			SELECT
			 C201903  = coalesce(C201903,'')
			,C201902 = coalesce(C201902,'')
			,C201010 = coalesce(C201010,'')
			,C201020 = coalesce(C201020,'')
			,C201030 = coalesce(C201030,'')
			,C201912 = coalesce(C201912,'')
			,C201040 = coalesce(C201040,'')
			,C201050 = coalesce(C201050,'')
			,C201060 = coalesce(C201060,'')
			,C201070 = coalesce(C201070,'')
			,C201080 = coalesce(C201080,'')
			,C201909 = coalesce(C201909,'')
			,C201906 = coalesce(C201906,'')
			,C201090 = coalesce(C201090,'')
			,C201100 = coalesce(C201100,'')
			,C201110 = coalesce(C201110,'')
			,C201120 = coalesce(C201120,'')
			,C201130 = coalesce(C201130,'')
			,C201140 = coalesce(C201140,'')
			,C201150 = coalesce(C201150,'')
			,C201160 = coalesce(C201160,'')
    	
			FROM [CYPHS].[CYP201CareContact] CYP201
              
          
			where  CYP101.C101902 = CYP201.C201902

			for xml PATH('CYP201'), TYPE

			)

			

			for xml PATH(''), TYPE
			)
    
   
 
			as xml 
		)

	    from WarehouseReportingMerged.CYPHS.CYP101Referral CYP101

	    where  CYP101.C101901 = Encounter.C001901


	    for xml PATH('CYP101'), TYPE

    )

 

 for xml PATH(''), TYPE
    )
    
   
 
 as xml )				
				
				,
				(
				    SELECT 
					 
					 C002901
					,C002010 = coalesce(C002010,'')
					,C002020 = coalesce(C002020,'')
					,C002030 = coalesce(C002030,'')
					,C002040 = coalesce(C002040,'')

					from WarehouseReportingMerged.CYPHS.CYP002GP CYP002

					where  CYP002.C002901 = Encounter.C001901


					for xml PATH('CYP002'), TYPE

				)
				
				,
				(
				    SELECT 
					 
					 C003901
					,C003010 = coalesce(C003010,'')
					,C003020 = coalesce(C003020,'')


					from WarehouseReportingMerged.CYPHS.CYP003AccommodationType CYP003

					where  CYP003.C003901 = Encounter.C001901


					for xml PATH('CYP003'), TYPE

				)
				
				
				

			For XML path ('CYP001')--, Root ('MATHDRHeader')


			) 
	as varchar (max)
	)

	into #XMLOut

	from WarehouseReportingMerged.CYPHS.CYP001MPI Encounter
	
	where

	 @LocalPatientId is null
	 or 
	 @LocalPatientId = Encounter.C001901






SET @XMLNodeMain = STUFF
      (

            (

        SELECT ''  +  XMLOutPut from #XMLOut 
        
        
        FOR XML PATH ('') , root('MyString'), type 
     ).value('/MyString[1]','varchar(max)') 
   , 1, 0, '')



set @XMLNode6 = '</CYP000> </CYPHS:CYPHS>'

select @XMLNode1 + @XMLNode2 + @XMLNode3 + @XMLNode4 + @XMLNodeMain +  @XMLNode6




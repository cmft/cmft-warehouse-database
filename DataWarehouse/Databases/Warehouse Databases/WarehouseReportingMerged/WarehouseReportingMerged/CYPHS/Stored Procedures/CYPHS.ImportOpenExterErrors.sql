﻿

CREATE proc [CYPHS].[ImportOpenExterErrors] as


delete [CYPHS].[DataQuality] where SubMissionPeriod = ( Select max(SubMissionPeriod) from CYPHS.CYP001MPI )
--delete [CYPHS].[Rejected] where SubMissionPeriod = ( Select max(SubMissionPeriod) from Maternity.MAT001MothersDemographics )  


CREATE TABLE #DataQuality(
	[REPORTTYPE] [varchar](max) NULL,
	[DESCRIPTION] [varchar](max) NULL,
	[CODE] [varchar](max) NULL,
	[NUMERATOR] [varchar](max) NULL,
	[DENOMINATOR] [varchar](max) NULL,
	[DATA_ITEM1] [varchar](max) NULL,
	[DATA_VALUE1] [varchar](max) NULL,
	[DATA_ITEM2] [varchar](max) NULL,
	[DATA_VALUE2] [varchar](max) NULL,
	[DATA_ITEM3] [varchar](max) NULL,
	[DATA_VALUE3] [varchar](max) NULL,
	[DATA_ITEM4] [varchar](max) NULL,
	[DATA_VALUE4] [varchar](max) NULL,
	[DATA_ITEM5] [varchar](max) NULL,
	[DATA_VALUE5] [varchar](max) NULL,
	[DATA_ITEM6] [varchar](max) NULL,
	[DATA_VALUE6] [varchar](max) NULL,
	[DATA_ITEM7] [varchar](max) NULL,
	[DATA_VALUE7] [varchar](max) NULL,
	[DATA_ITEM8] [varchar](max) NULL,
	[DATA_VALUE8] [varchar](max) NULL,
	[DATA_ITEM9] [varchar](max) NULL,
	[DATA_VALUE9] [varchar](max) NULL,
	[DATA_ITEM10] [varchar](max) NULL,
	[DATA_VALUE10] [varchar](max) NULL
	
) ON [PRIMARY]

BULK INSERT #DataQuality 
   FROM '\\Information\Public2\Quality & Governance\Data Quality\CYPHS\Rejections\Current\data_quality.txt'
   WITH 
      (
        ROWTERMINATOR ='0x0a',
         FIELDTERMINATOR='|',
         FIRSTROW=2	
      );
      
insert into [CYPHS].[DataQuality]      
Select 
*
,SubMissionPeriod = ( Select max(SubMissionPeriod) from CYPHS.CYP001MPI )
 from #DataQuality 
  

--CREATE TABLE #Rejected (
--	[REPORT_TYPE] [varchar](50) NULL,
--	[DESCRIPTION] [varchar](500) NULL,
--	[CODE] [varchar](50) NULL,
--	[NUMERATOR] [varchar](50) NULL,
--	[DENOMINATOR] [varchar](50) NULL,
--	[DATA_ITEM1] [varchar](50) NULL,
--	[DATA_VALUE1] [varchar](50) NULL,
--	[DATA_ITEM2] [varchar](50) NULL,
--	[DATA_VALUE2] [varchar](50) NULL,
--	[DATA_ITEM3] [varchar](50) NULL,
--	[DATA_VALUE3] [varchar](50) NULL,
--	[DATA_ITEM4] [varchar](50) NULL,
--	[DATA_VALUE4] [varchar](50) NULL,
--	[DATA_ITEM5] [varchar](50) NULL,
--	[DATA_VALUE5] [varchar](50) NULL,
--	[DATA_ITEM6] [varchar](50) NULL,
--	[DATA_VALUE6] [varchar](50) NULL,
--	[DATA_ITEM7] [varchar](50) NULL,
--	[DATA_VALUE7] [varchar](50) NULL,
--	[DATA_ITEM8] [varchar](50) NULL,
--	[DATA_VALUE8] [varchar](50) NULL,
--	[DATA_ITEM9] [varchar](50) NULL,
--	[DATA_VALUE9] [varchar](50) NULL,
--	[DATA_ITEM10] [varchar](50) NULL,
--	[DATA_VALUE10] [varchar](50) NULL
--) ON [PRIMARY]

--BULK INSERT #Rejected
--   FROM '\\Information\imt\Developments\Maternity Minimum Data Set\Data Submission Files\Rejections\Current\validations.txt'
--   WITH 
--      (
--        ROWTERMINATOR ='0x0a',
--         FIELDTERMINATOR='|',
--         FIRSTROW=2	
--      );    
      
--insert into [Maternity].[Rejected]     
--Select 
--*
--,SubMissionPeriod = ( Select max(SubMissionPeriod) from Maternity.MAT001MothersDemographics )
-- from #Rejected  ;  
 
drop table #DataQuality 
--drop table #Rejected  ;  
      
        
 
 
 
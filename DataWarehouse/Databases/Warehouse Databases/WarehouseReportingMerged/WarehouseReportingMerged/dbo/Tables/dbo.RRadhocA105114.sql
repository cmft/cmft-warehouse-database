﻿CREATE TABLE [dbo].[RRadhocA105114] (
    [GlobalProviderSpellNo] VARCHAR (50)  NULL,
    [GlobalEpisodeNo]       INT           NULL,
    [MergeEncounterRecno]   INT           NOT NULL,
    [MergeResultRecno]      INT           NULL,
    [AdmissionDate]         SMALLDATETIME NULL,
    [DischargeDate]         SMALLDATETIME NULL,
    [EpisodeStartTime]      SMALLDATETIME NULL,
    [EpisodeEndTime]        SMALLDATETIME NULL,
    [ResultTime]            DATETIME      NULL,
    [ResultCode]            VARCHAR (10)  NULL,
    [Result]                VARCHAR (70)  NULL,
    [ResultSetCode]         VARCHAR (10)  NULL
);


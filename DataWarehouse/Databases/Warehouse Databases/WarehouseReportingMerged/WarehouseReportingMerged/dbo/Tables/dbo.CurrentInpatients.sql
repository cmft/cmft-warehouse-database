﻿CREATE TABLE [dbo].[CurrentInpatients] (
    [DistrictNo] VARCHAR (20) NOT NULL,
    PRIMARY KEY CLUSTERED ([DistrictNo] ASC)
);


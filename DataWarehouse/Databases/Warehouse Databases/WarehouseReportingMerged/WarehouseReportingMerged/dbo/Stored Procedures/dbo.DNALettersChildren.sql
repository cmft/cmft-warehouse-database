﻿


----------truncate table [dbo].[AutomatedLetters]


CREATE procedure [dbo].[DNALettersChildren] --'03 jan 2013'

 @DateStart datetime = null
,@DateEnd datetime = null

as

/* 
==============================================================================================
Description:
	
When		Who			What
20151021	Paul Egan	Restricted to CEN||PAS. Reason: specialty 420 at Trafford is now being allocated to 
						Childrens division which brought through Trafford records. But we don't currently
						bring through Trafford cancellation date so the proc failed. Liam Orrell said the
						project stalled back in 2013 so the final table is not being used anyway.
===============================================================================================
*/


declare @StartDate datetime = coalesce(@DateStart,(SELECT dateadd(day,datediff(day,7,GETDATE()),0))) 

declare @EndDate datetime = coalesce(@DateEnd,(SELECT dateadd(day,datediff(day,1,GETDATE()),0))) 


select

	[SourceUniqueID]
	,[LetterType]  = 
		case
			when AttendanceStatus.NationalAttendanceStatusCode = '3'  then 'DNA'
			when AttendanceStatus.NationalAttendanceStatusCode = '4' then 'CAN'	
			else null
		end

	,[DateGenerated]  
		= getdate ()
	,[DateProcessed]  = null 
			
	,[SendPatient]  
		= 
		case
			when PatientAddress1 is not null then 1 
			else 0
		end

	,[SendGP]  = 
		case 
			when Practice.Address1 is not null then 1
			else 0
		end

	,[SendReferrer] 
		 =
		case	
			when RegisteredGpCode = ReferredByGpCode then 0
			when RegisteredGpCode <> ReferredByGpCode then 1
		else 0
		end 

	,[DNACancelDate]  
		= Encounter.AppointmentTime
	,[FutureAppDate]  = null
	,[CancelledOn]   
		= 
		case 
			when Encounter.AppointmentOutcomeCode = 'DNA' then 	AppointmentDate.TheDate
			else Encounter.AppointmentCancelDate
		end
	,[CancelledBy]  
		= Encounter.CancelledByCode
	,Encounter.AppointmentOutcomeCode
	,[AppointmentName]  
		= AppointmentType.SourceAppointmentType
	,[AppointmentType]  = 
		case when left(AppointmentType.SourceAppointmentType,3) = 'New' then 'New'
			else 'Fol'
		end

	,[Division]  
		= Directorate.Division
	,[Directorate]  
		= Directorate.Directorate
	,[Specialty]  
		= 
		Specialty.NationalSpecialty

	,[ConsultantTitle]  
		= Consultanat.Title
	,[ConsultantInitials]  
		= Consultanat.Initials
	,[ConsultantSurname]   
		= Consultanat.Surname
	,[ClinicName]  
		= 'Needs adding'
	,[DistrictNo]  
		= Encounter.DistrictNo
	,[NHSNumber]  
		= Encounter.NHSNumber
	,[CasenoteNo]  
		= Encounter.CasenoteNo
	,[PatientTitle]  
		= Encounter.PatientTitle
	,[PatientSurname]  
		= Encounter.PatientSurname
	,[PatientForename]  
		= Encounter.PatientForename 
	,[PatientSex]  
		= left(Sex.SourceSex,1)
	,[PatientAddress1]  
		= Encounter.PatientAddress1
	,[PatientAddress2]  
		= Encounter.PatientAddress2
	,[PatientAddress3]  
		= Encounter.PatientAddress3
	,[PatientAddress4]  
		= Encounter.PatientAddress4
	,[Postcode]  
		= Encounter.Postcode
	,[DateOfBirth]  
		= Encounter.DateOfBirth
	,[Age]  
		= datediff(year, Encounter.DateOfBirth,getdate ())
	,[RegisteredGpCode]  
		= Encounter.RegisteredGpCode
	,[RegisteredGpPracticeCode]  
		= Encounter.RegisteredGpPracticeCode
	,[RegisteredGPName]  
		= GP.Organisation
	,[RegisteredGpPracticeAddress1]  
		= Practice.Address1
	,[RegisteredGpPracticeAddress2]  
		= Practice.Address2
	,[RegisteredGpPracticeAddress3]  
		= Practice.Address3
	,[RegisteredGpPracticeAddress4]  
		= Practice.Address4
	,[RegisteredGpPracticePostCode]  
		= Practice.Postcode
	,[ReferrerType]  
		= SourceOfReferral.LocalSourceOfReferral
	,[ReferrerCode]  
		= 
		case
			when Encounter.ReferredByCode = 'GP' then Encounter.ReferredByGpCode
			when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.NationalConsultantCode
		else null
		end
	,[ReferrerName]  
		= 
		case
			when Encounter.ReferredByCode = 'GP' then ReferrerGP.Organisation
			when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.Surname + ' ' + ReferrerConsultanat.Initials

		else null
		end
	,[ReferrerAddress1]  
		=
					 
		case
			when Encounter.ReferredByCode = 'GP' then ReferrerGP.Address1
			--when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.Surname
		else null
		end 
	,[ReferrerAddress2]  
		= 
		case
			when Encounter.ReferredByCode = 'GP' then ReferrerGP.Address2
			--when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.Surname
		else null
		end 
	,[ReferrerAddress3]  
		=
		case
			when Encounter.ReferredByCode = 'GP' then ReferrerGP.Address3
			--when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.Surname
		else null
		end  
	,[ReferrerAddress4]  
		= 
		case
			when Encounter.ReferredByCode = 'GP' then ReferrerGP.Address4
			--when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.Surname
		else null
		end 
	,[ReferrerPostCode]  
		= 
		case
			when Encounter.ReferredByCode = 'GP' then ReferrerGP.Postcode
			--when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.Surname
		else null
		end 

	,[PatientCancerFlag] 
		=
		case 
			when ReasonForReferral.SourceReasonForReferral like '%Canc%' then 1 
		else 0
		end
	,[RegisteredGPTitle] = 'Dr'
	,[RegisteredGPInitials] =   substring(GP.Organisation,charindex(' ',GP.Organisation)+1 ,1)
	,[RegisteredGPSurname] = SUBSTRING(GP.Organisation, 1, CHARINDEX(' ', GP.Organisation))

	,[ReferrerTitle] = null
	,[ReferrerInitials] = 

		case
			when Encounter.ReferredByCode = 'GP' then substring(ReferrerGP.Organisation,charindex(' ',ReferrerGP.Organisation)+1 ,1)
			
			
			when Encounter.ReferredByCode = 'CONS' then ReferrerConsultanat.Initials


		else null
		end

	,[ReferrerSurname] = 

		case
			when Encounter.ReferredByCode = 'GP' then SUBSTRING(ReferrerGP.Organisation, 1, CHARINDEX(' ', ReferrerGP.Organisation))
			
			
			
			when Encounter.ReferredByCode = 'CONS' then left(ReferrerConsultanat.Surname,50)


		else null
		end

into #Working

from 

	OP.Encounter Encounter

	left join OP.AttendanceStatus AttendanceStatus
	on AttendanceStatus.SourceAttendanceStatusID = Encounter.AttendanceStatusID

	left join OP.ReasonForReferral ReasonForReferral
	on ReasonForReferral.SourceReasonForReferralID = Encounter.ReasonForReferralID
	and ReasonForReferral.SourceContextCode = Encounter.ContextCode

	left join Organisation.dbo.Practice Practice
	on Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode

	left join Organisation.dbo.Gp GP
	on GP.OrganisationCode = Encounter.RegisteredGpCode

	left join WH.Sex Sex
	on Sex.SourceSexID = Encounter.SexID

	left join OP.SourceOfReferral SourceOfReferral
	on SourceOfReferral.SourceSourceOfReferralID = Encounter.SourceOfReferralID
	and SourceOfReferral.SourceContextCode = Encounter.ContextCode

	inner join wh.Calendar AppointmentDate
	on AppointmentDate.DateID = Encounter.AppointmentDateID

	left join OP.Clinic Clinic
	on Clinic.SourceClinicID = encounter.ClinicID
	and Clinic.SourceContext = Encounter.ContextCode

	left join WH.Directorate Directorate
	on Directorate.DirectorateCode = Encounter.DirectorateCode

	--INNER JOIN (SELECT VALUE FROM warehouse.dbo.SSRS_MultiValueParamSplit(@Division,',')) D
	--ON Directorate.Division = D.Value   


	left join OP.AppointmentType AppointmentType
	on AppointmentType.SourceAppointmentTypeID = Encounter.AppointmentTypeID

	left join wh.Consultant Consultanat
	on Consultanat.SourceConsultantID = Encounter.ConsultantID

	left join wh.Consultant ReferrerConsultanat
	on ReferrerConsultanat.SourceConsultantID = Encounter.ReferringConsultantID

	left join Organisation.dbo.Gp ReferrerGP
	on ReferrerGP.OrganisationCode = Encounter.ReferredByGpCode

	left join WH.Specialty Specialty
	on Specialty.SourceSpecialtyID = Encounter.SpecialtyID

where 
	AttendanceStatus.NationalAttendanceStatusCode in ('3','4')	

	and	AppointmentDate.TheDate between @StartDate and @EndDate
	--Removed on request 17/04/2013 by Liam O
	--and 
	--left(AppointmentType.SourceAppointmentType,3) = 'New'

	and	Directorate.Division = 'Childrens' --only for trial 

	and Encounter.ContextCode = 'CEN||PAS'	-- Added Paul Egan 20151021

	and not exists 

	(
	select 1 from

	[dbo].[AutomatedLetters] AutomatedLetters
	where AutomatedLetters.SourceUniqueID = Encounter.SourceUniqueID

	)


insert into [dbo].[AutomatedLetters]
	(
	[SourceUniqueID]
	,[LetterType]
	,[DateGenerated]
	,[DateProcessed]
	,[SendPatient]
	,[SendGP]
	,[SendReferrer]
	,[DNACancelDate]
	,[FutureAppDate]
	,[CancelledOn]
	,[CancelledBy]
	,[AppointmentName]
	,[AppointmentType]
	,[Division]
	,[Directorate]
	,[Specialty]
	,[ConsultantTitle]
	,[ConsultantInitials]
	,[ConsultantSurname]
	,[ClinicName]
	,[DistrictNo]
	,[NHSNumber]
	,[CasenoteNo]
	,[PatientTitle]
	,[PatientSurname]
	,[PatientForename]
	,[PatientSex]
	,[PatientAddress1]
	,[PatientAddress2]
	,[PatientAddress3]
	,[PatientAddress4]
	,[Postcode]
	,[DateOfBirth]
	,[Age]
	,[RegisteredGpCode]
	,[RegisteredGpPracticeCode]
	,[RegisteredGPName]
	,[RegisteredGpPracticeAddress1]
	,[RegisteredGpPracticeAddress2]
	,[RegisteredGpPracticeAddress3]
	,[RegisteredGpPracticeAddress4]
	,[RegisteredGpPracticePostCode]
	,[ReferrerType]
	,[ReferrerCode]
	,[ReferrerName]
	,[ReferrerAddress1]
	,[ReferrerAddress2]
	,[ReferrerAddress3]
	,[ReferrerAddress4]
	,[ReferrerPostCode]
	,[PatientCancerFlag]
	,[RegisteredGPTitle] 
	,[RegisteredGPInitials] 
	,[RegisteredGPSurname]
	,[ReferrerTitle]
	,[ReferrerInitials] 
	,[ReferrerSurname] 
	,[XMLDetails] 
	)

select 
	[SourceUniqueID]
	,[LetterType] 
	,[DateGenerated]  
	,[DateProcessed]  
	,[SendPatient]  
	,[SendGP]  
	,[SendReferrer] 
	,[DNACancelDate]  
	,[FutureAppDate]  
	,[CancelledOn]   
	,[CancelledBy]  
	,[AppointmentName]  
	,[AppointmentType] 
	,[Division]  
	,[Directorate]  
	,[Specialty]  
	,[ConsultantTitle]  
	,[ConsultantInitials]  
	,[ConsultantSurname]   
	,[ClinicName]  
	,[DistrictNo]  
	,[NHSNumber]  
	,[CasenoteNo]  
	,[PatientTitle]  
	,[PatientSurname]  
	,[PatientForename]  
	,[PatientSex]  
	,[PatientAddress1]  
	,[PatientAddress2]  
	,[PatientAddress3]  
	,[PatientAddress4]  
	,[Postcode]  
	,[DateOfBirth]  
	,[Age]  
	,[RegisteredGpCode]  
	,[RegisteredGpPracticeCode]  
	,[RegisteredGPName]  
	,[RegisteredGpPracticeAddress1]  
	,[RegisteredGpPracticeAddress2]  
	,[RegisteredGpPracticeAddress3]  
	,[RegisteredGpPracticeAddress4]  
	,[RegisteredGpPracticePostCode]  
	,[ReferrerType]  
	,[ReferrerCode]  
	,[ReferrerName]  
	,[ReferrerAddress1]  
	,[ReferrerAddress2]  
	,[ReferrerAddress3]  
	,[ReferrerAddress4]  
	,[ReferrerPostCode]  
	,[PatientCancerFlag] 
	,[RegisteredGPTitle] 
	,[RegisteredGPInitials] 
	,[RegisteredGPSurname]
	,[ReferrerTitle]
	,[ReferrerInitials] 
	,[ReferrerSurname] 
	,[XMLDetails] =
		cast(
		(

			select
				
				(
				select 
				  DNACancelDate as DNACancelDate

				 ,FutureAppDate as FutureAppDate



				 ,CancelledOn as CancelledOn
				 ,CancelledBy as CancelledBy
				 ,AppointmentName as AppointmentName
				 ,AppointmentType as AppointmentType
				 ,Division as Division
				 ,Directorate as Directorate
				 ,Specialty as Specialty
				 ,ConsultantTitle as ConsultantTitle
				 ,ConsultantInitials as ConsultantInitials
				 ,WarehouseOLAPMergedv2.dbo.f_ProperCase(ConsultantSurname) as ConsultantSurname
				 ,ClinicName as ClinicName
				 ,PatientCancerFlag as SuspectedCancer
				 ,ReferrerType as ReferrerType
				 ,ReferrerCode as ReferrerCode
				 ,ReferrerTitle as ReferrerTitle
				 ,ReferrerInitials as ReferrerInitials
				 ,ReferrerSurname as ReferrerSurname

				 ,ReferrerAddress1 as ReferrerAddress1
				 ,ReferrerAddress2 as ReferrerAddress2
				 ,ReferrerAddress3 as ReferrerAddress3
				 ,ReferrerAddress4 as ReferrerAddress4
				 ,ReferrerPostCode as ReferrerPostCode
				 

								
				from #Working XMLData
				where XMLData.SourceUniqueID = Working.SourceUniqueID
				for xml path ('XML'), ELEMENTS XSINIL, TYPE
				)						
		
				)
		as xml)

from

	#Working Working



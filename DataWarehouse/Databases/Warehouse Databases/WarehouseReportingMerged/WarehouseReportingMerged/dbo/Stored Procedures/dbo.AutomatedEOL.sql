﻿

CREATE procedure  [dbo].[AutomatedEOL] as

--use [WarehouseReportingMerged]


	


INSERT INTO [dbo].[AutomatedLetters]

		(

		

			[SourceUniqueID]
           ,[LetterType] 
			
			
           ,[DateGenerated] 
        
           ,[SendPatient] 
           ,[SendGP] 
           ,[SendReferrer]

		   ,[DNACancelDate]
		   ,[CancelledOn]
   
           ,[Division]
           ,[Directorate] 
           ,[Specialty] 
           ,[ConsultantTitle]
           ,[ConsultantInitials] 
           ,[ConsultantSurname]

           ,[DistrictNo] 
           ,[NHSNumber] 
           ,[CasenoteNo] 
           ,[PatientTitle]
           ,[PatientSurname] 
           ,[PatientForename]
           ,[PatientSex] 
           ,[PatientAddress1]
           ,[PatientAddress2]
           ,[PatientAddress3] 
           ,[PatientAddress4] 
           ,[Postcode]
           ,[DateOfBirth] 
           ,[Age]
           ,[RegisteredGpCode]
           ,[RegisteredGpPracticeCode]
           ,[RegisteredGPName] 
           ,[RegisteredGpPracticeAddress1]
           ,[RegisteredGpPracticeAddress2]
           ,[RegisteredGpPracticeAddress3]
           ,[RegisteredGpPracticeAddress4]
           ,[RegisteredGpPracticePostCode]
     	   ,[XMLDetails] 

		   )


Select

			[SourceUniqueID]
           ,[LetterType] 
				= 
				Case 

					when LiverpoolCarePathwayStartDate is not null  then 'EOL' 
					when LimitationOfTreatmentStartDate is not null  then 'EOL' 
					when DoNotResuscitateStartDate is not null then 'EOL' 
					else '0'
				end

			
           ,[DateGenerated] 
           --,[DateProcessed] = null
           ,[SendPatient] = 0
           ,[SendGP] = 0
           ,[SendReferrer] = 0
           ,[DNACancelDate] = '01 jan 1900'
           --,[FutureAppDate] = null
           ,[CancelledOn] = '01 jan 1900'
           --,[CancelledBy] = null
           --,[AppointmentName] = null
           --,[AppointmentType] = null
   
           ,[Division]
           ,[Directorate] 
           ,[Specialty] 
           ,[ConsultantTitle]
           ,[ConsultantInitials] 
           ,[ConsultantSurname]
           --,[ClinicName] = null
           ,[DistrictNo] 
           ,[NHSNumber] 
           ,[CasenoteNo] 
           ,[PatientTitle]
           ,[PatientSurname] 
           ,[PatientForename]
           ,[PatientSex] 
           ,[PatientAddress1]
           ,[PatientAddress2]
           ,[PatientAddress3] 
           ,[PatientAddress4] 
           ,[Postcode]
           ,[DateOfBirth] 
           ,[Age]
           ,[RegisteredGpCode]
           ,[RegisteredGpPracticeCode]
           ,[RegisteredGPName] 
           ,[RegisteredGpPracticeAddress1]
           ,[RegisteredGpPracticeAddress2]
           ,[RegisteredGpPracticeAddress3]
           ,[RegisteredGpPracticeAddress4]
           ,[RegisteredGpPracticePostCode]
           --,[ReferrerType] = null
           --,[ReferrerCode] = null
           --,[ReferrerName] = null
           --,[ReferrerAddress1] = null
           --,[ReferrerAddress2] = null
           --,[ReferrerAddress3] = null
           --,[ReferrerAddress4] = null
           --,[ReferrerPostCode] = null


		   ,[XMLDetails] = Details



from
(

select 
      [DateGenerated]  
			= getdate ()

	,Encounter.SourceUniqueID


,Encounter.AdmissionTime
,Encounter.DischargeTime
,Encounter.CasenoteNumber
, id
,hospspellid
,Spell.CurrWard
,locationid
,lastupdated_ts
 


,LastAssessedDate = 

EOL.details.value('(EOLPathwayAssessmentData /LastAssessedDate/text())[1]','datetime')


	,LiverpoolCarePathwayStartDate =

EOL.details.value('(/EOLPathwayAssessmentData/LiverpoolCarePathway/StartDate/text())[1]','datetime')

	,LiverpoolCarePathwayFinishDate  =

EOL.details.value('(EOLPathwayAssessmentData /LiverpoolCarePathway/FinishDate/text())[1]','datetime')

	,LiverpoolCarePathwayPlaceOfDeath  =

EOL.details.value('(EOLPathwayAssessmentData /LiverpoolCarePathway/PlaceOfDeath/text())[1]','varchar (80)')

	,LimitationOfTreatmentStartDate =

EOL.details.value('(EOLPathwayAssessmentData /LimitationOfTreatment/StartDate/text())[1]','datetime')

	,LimitationOfTreatmentFinishDate  =

EOL.details.value('(EOLPathwayAssessmentData /LimitationOfTreatment/FinishDate/text())[1]','datetime')

	,DoNotResuscitateStartDate = 

EOL.details.value('(EOLPathwayAssessmentData /DoNotResuscitate/StartDate/text())[1]','datetime')

	,DoNotResuscitateFinishDate  = 

EOL.details.value('(EOLPathwayAssessmentData /DoNotResuscitate/FinishDate/text())[1]','datetime')
,EOL.Details

,OrderBy = Row_number () over(PARTITION BY Spell.SpellID order by eol.lastupdated_ts desc)


      ,[Division]  
			= Directorate.Division
      ,[Directorate]  
			= Directorate.Directorate
      ,[Specialty]  
			= 
			case 
				when Consultanat.MainSpecialty is null then 'Not found'
				else Consultanat.MainSpecialty
			end
      ,[ConsultantTitle]  
			= Consultanat.Title
      ,[ConsultantInitials]  
			= Consultanat.Initials
      ,[ConsultantSurname]   
			= Consultanat.Surname

	    ,[DistrictNo]  
			= Encounter.DistrictNo
      ,[NHSNumber]  
			= Encounter.NHSNumber
      ,[CasenoteNo]  
			= Encounter.CasenoteNumber
      ,[PatientTitle]  
			= Encounter.PatientTitle
      ,[PatientSurname]  
			= Encounter.PatientSurname
      ,[PatientForename]  
			= Encounter.PatientForename 
      ,[PatientSex]  
			= left(Sex.SourceSex,1)
      ,[PatientAddress1]  
			= Encounter.PatientAddress1
      ,[PatientAddress2]  
			= Encounter.PatientAddress2
      ,[PatientAddress3]  
			= Encounter.PatientAddress3
      ,[PatientAddress4]  
			= Encounter.PatientAddress4
      ,[Postcode]  
			= Encounter.Postcode
      ,[DateOfBirth]  
			= Encounter.DateOfBirth
      ,[Age]  
			= datediff(year, Encounter.DateOfBirth,getdate ())

      ,[RegisteredGpCode]  
			= Encounter.RegisteredGpCode

      ,[RegisteredGpPracticeCode]  
			= Encounter.RegisteredGpPracticeCode

   ,[RegisteredGPName]  
			= GP.Organisation
      ,[RegisteredGpPracticeAddress1]  
			= Practice.Address1
      ,[RegisteredGpPracticeAddress2]  
			= Practice.Address2
      ,[RegisteredGpPracticeAddress3]  
			= Practice.Address3
      ,[RegisteredGpPracticeAddress4]  
			= Practice.Address4
      ,[RegisteredGpPracticePostCode]  
			= Practice.Postcode
	
from 
TrustCache.Dementia.Bedman_Spell_min Spell

left join 
[TrustCache].[EOL].[Bedman_Event] EOL

on EOL.HospSpellID = Spell.SpellID

INNER JOIN APC.Encounter Encounter
ON	Spell.Patient = Encounter.SourcePatientNo
AND Spell.IPEpisode = Encounter.SourceSpellNo
AND Encounter.FirstEpisodeInSpellIndicator = 1



			 left join Organisation.dbo.Practice Practice
			 on Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode

			 left join Organisation.dbo.Gp GP
			 on GP.OrganisationCode = Encounter.RegisteredGpCode

			 left join WH.Sex Sex
			 on Sex.SourceSexID = Encounter.SexID

			 left join WH.Directorate Directorate
			 on Directorate.DirectorateCode = Encounter.StartDirectorateCode

			 left join wh.Consultant Consultanat
			 on Consultanat.SourceConsultantID = Encounter.ConsultantID



	)EOL

	where
 
	orderby = 1

	and DischargeTime is not null

--	and CasenoteNumber in ('M11/081386')
and

	(
		 LiverpoolCarePathwayStartDate is not null  
		or LimitationOfTreatmentStartDate is not null  
		or DoNotResuscitateStartDate is not null 


	)

	and not exists

		(select 1 from [dbo].[AutomatedLetters] AutomatedLetters

		where AutomatedLetters.SourceUniqueID = EOL.SourceUniqueID
		and AutomatedLetters.LetterType = 'EOL'
		--and AutomatedLetters.DateProcessed is not null

		)

	--and 


	--	(
	--	 LiverpoolCarePathwayFinishDate is  null  
	--	or LimitationOfTreatmentFinishDate is null  
	--	or DoNotResuscitateFinishDate is not null 
	--	)



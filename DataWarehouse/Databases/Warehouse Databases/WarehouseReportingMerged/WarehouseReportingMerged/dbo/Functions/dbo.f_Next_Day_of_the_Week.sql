﻿

create function [dbo].[f_Next_Day_of_the_Week] (
	@day varchar(9),
	@date datetime
)
returns datetime
as
begin
	if @date is null return null;
	
	/* if next day name is same as today day name returns today */
	declare @week_day_required int
	declare @week_day_after int
	declare @day_of_the_week datetime
	declare @days int

	set @week_day_required = (select case left(@day, 3)
		when 'sun' then 1
		when 'mon' then 2
		when 'tue' then 3
		when 'wed' then 4
		when 'thu' then 5
		when 'fri' then 6
		when 'sat' then 7
		else 0 end)

	set @week_day_after = (select case left(datename(dw, @date), 3)
		when 'sun' then 1
		when 'mon' then 2
		when 'tue' then 3
		when 'wed' then 4
		when 'thu' then 5
		when 'fri' then 6
		when 'sat' then 7
		else 0 end)

	set @days = @week_day_required - @week_day_after

	if(@days < 0)
	begin
		set @days = @days + 7
	end

	set @day_of_the_week = convert(datetime, convert(char(10), dateadd(day, @days, @date), 103), 103)
	return(@day_of_the_week)
end



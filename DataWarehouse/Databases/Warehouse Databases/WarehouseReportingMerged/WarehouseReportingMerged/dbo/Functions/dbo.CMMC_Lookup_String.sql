﻿




CREATE FUNCTION [dbo].[CMMC_Lookup_String]
(
      @res_field varchar(255)
)
RETURNS varchar(max)
AS
BEGIN
	
	DECLARE @iStart INT, @iLen INT, @vReturn VARCHAR(MAX)
	SET @iStart = 1
	SET @iLen = 5
	SET @vReturn = ''

	IF CHARINDEX('||',@res_field,1) > 0
		SET @iLen = CHARINDEX('||',@res_field,1) - 1


	WHILE @iStart <> 2  -- END LOOK IF 2 (AS CHARINDEX = 0 + 2)
		BEGIN
			SELECT @vReturn = @vReturn + ', ' + Lkp_Name
			FROM CMMC_Reports.dbo.Lookups
			WHERE Lkp_ID = SUBSTRING(@res_field,@iStart,@iLen)
			
			SET @iStart = CHARINDEX('||',@res_field,@iStart) + 2
		END
		

	RETURN SUBSTRING(@vReturn,3,9999)  -- REMOVE 1ST ,
END




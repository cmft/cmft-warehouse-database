﻿


CREATE FUNCTION [dbo].[fn_CalendarExtended]
(
	@DateId		INT
)
RETURNS TABLE AS
RETURN
(
/******************************************************************************
**  Name: fn_CalendarExtended
**  Purpose: Additional calendar date columns in addition to the standard
**           Calendar columns
**
**  For this function to perform correctly set a DATEFIRST value in the calling code
**  eg SET DATEFIRST 1 for a Monday start of week
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 06.12.13   MH         Created
** 13.04.16	  MH         Additional columns MonthStartDate
******************************************************************************/	

	SELECT 
		 MonthStartDate			= MONTHSTART.StartDate
		,MonthEndDate			= DATEADD(DAY, -1, DATEADD(MONTH, 1, MONTHSTART.StartDate))
		,CAL.*
		
	FROM 
		WH.Calendar CAL

		CROSS APPLY
		(
			SELECT 
				 StartDate			= CAST('01 ' + CAL.TheMonth AS DATE)
		) MONTHSTART

	WHERE 
		CAL.DateID  = @DateId 
)



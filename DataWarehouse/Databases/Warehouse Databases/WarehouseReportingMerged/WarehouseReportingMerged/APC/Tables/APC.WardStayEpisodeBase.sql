﻿CREATE TABLE [APC].[WardStayEpisodeBase] (
    [CensusDateTime]         DATETIME NULL,
    [APCEncounterRecno]      BIGINT   NOT NULL,
    [WardStayEncounterRecno] BIGINT   NOT NULL
);


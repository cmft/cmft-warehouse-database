﻿

create Procedure APC.[MissingNHSNo] 
	@FromDate date
	,@ToDate date
as 

-- 20160411	RR created on behalf of Waiken Chan

SELECT [SourcePatientNo]
      ,[PatientTitle]
      ,[PatientForename]
      ,[PatientSurname]
      ,[DateOfBirth]
      ,[DateOfDeath]
      ,[SexCode]
      ,[NHSNumber]
      ,[Postcode]
      ,[PatientAddress1]
      ,[PatientAddress2]
      ,[PatientAddress3]
      ,[PatientAddress4]
      ,[AdmissionDate]
      ,[DischargeDate]
      ,[EpisodeStartDate]
      ,[EpisodeEndDate]
      ,[SiteCode]
      ,[ConsultantCode]
      ,[SpecialtyCode]
      ,[CasenoteNumber]
      ,[NHSNumberStatusCode]
      ,[DistrictNo]
  FROM [APC].[Encounter]
  Where DischargeDate >= @FromDate
  and DischargeDate < @ToDate
  and NHSNumber is NULL
  


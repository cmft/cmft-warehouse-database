﻿
create view APC.[HRG4Spell]

as

SELECT [MergeEncounterRecno]
      ,[HRGCode]
      ,[GroupingMethodFlag]
      ,[DominantOperationCode]
      ,[PrimaryDiagnosisCode]
      ,[SecondaryDiagnosisCode]
      ,[EpisodeCount]
      ,[LOS]
      ,[PBCCode]
  FROM [WarehouseOLAPMergedV2].[APC].[HRG4Spell]

﻿create view [APC].[PSSEncounter]

as

SELECT [MergeEncounterRecno]
      ,[NationalProgrammeOfCareServiceCode]
      ,[ServiceLineCode]
      ,[ServiceLineCodeList]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[PSSEncounter]

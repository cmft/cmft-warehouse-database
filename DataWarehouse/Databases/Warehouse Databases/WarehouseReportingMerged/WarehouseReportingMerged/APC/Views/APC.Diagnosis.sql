﻿

CREATE view [APC].[Diagnosis]

as

select
	 MergeEncounterRecno
	,SequenceNo
	,DiagnosisCode

from
       [WarehouseOLAPMergedV2].[APC].[BaseDiagnosis]



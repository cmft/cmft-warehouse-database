﻿



CREATE View [APC].[DementiaCQUINValidation] as

Select
	GlobalProviderSpellNo
	,LocalAdmissionMethod
	,AdmissionDate
	,AdmissionTime
	,AdmissionWardCode
	,PatientSurname
	,Sex
	,NHSNumber
	,DistrictNo
	,CasenoteNumber
	,LatestWard
	,LatestSpecialtyCode
	,LatestConsultantCode
	,AgeOnAdmission 
	,LoS 
	,[LoS(hrs)]
	,LatestKnownDementiaRecordedTime
	,LatestKnownToHaveDementiaResponse	
	,LatestResponseTime	
	,LatestResponse
	,LatestAssessmentTime	
	,LatestAssessmentScore	
	,LatestInvestigationTime	
	,LatestReferralSentDate
	,LatestReferralRecordedTime
	,LatestAssessmentNotPerformedReason	
	,LatestAssessmentNotPerformedRecordedTime
	,Admission
	,Assessment
	,Referral
	,DementiaUpdated
	,WardUpdated
from	
	WarehouseOLAPMergedV2.APC.DementiaCQUINValidation

﻿
create view [APC].[HRG4CriticalCareQuality]

as
SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[QualityTypeCode]
      ,[QualityCode]
      ,[QualityMessage]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[HRG4CriticalCareQuality]

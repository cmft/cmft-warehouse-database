﻿



CREATE view [APC].[AugmentedCarePeriod]

as
SELECT 
	 B.MergeEncounterRecno
	,B.EncounterRecno
	,B.SourceUniqueID
	,B.SourcePatientNo
	,B.SourceSpellNo
	,B.ProviderSpellNo
	,B.ConsultantCode
	,B.AcpDisposalCode
	,B.LocalIdentifier
	,B.LocationCode
	,B.OutcomeIndicator
	,B.Source
	,B.SpecialtyCode
	,B.Status
	,B.StartDate
	,B.StartTime
	,B.EndDate
	,B.EndTime
	,B.AdvancedRespiratorySystemIndicator
	,B.BasicRespiratorySystemIndicator
	,B.CirculatorySystemIndicator
	,B.NeurologicalSystemIndicator
	,B.RenalSystemIndicator
	,B.NoOfSupportSystemsUsed
	,B.HighDependencyCareLevelDays
	,B.IntensiveCareLevelDays
	,B.PlannedAcpPeriodIndicator
	,B.ReviseByUserId
	,B.ReviseTime
	,B.ContextCode
	,R.ContextID
	,R.ConsultantID
	,R.SpecialtyID
	,R.StartDateID
	,R.EndDateID
  FROM [WarehouseOLAPMergedV2].APC.[BaseAugmentedCarePeriod] B
  JOIN [WarehouseOLAPMergedV2].APC.[BaseAugmentedCarePeriodReference] R ON R.[MergeEncounterRecno] = B.[MergeEncounterRecno]





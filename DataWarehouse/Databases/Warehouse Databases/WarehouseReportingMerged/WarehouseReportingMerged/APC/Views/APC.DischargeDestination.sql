﻿
create view [APC].[DischargeDestination] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceDischargeDestinationID]
      ,[SourceDischargeDestinationCode]
      ,[SourceDischargeDestination]
      --,[LocalDischargeDestinationID]
      ,[LocalDischargeDestinationCode]
      ,[LocalDischargeDestination]
      --,[NationalDischargeDestinationID]
      ,[NationalDischargeDestinationCode]
      ,[NationalDischargeDestination]
  FROM [WarehouseOLAPMergedV2].[APC].[DischargeDestination]


﻿
CREATE view [APC].[AdmissionSource] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceAdmissionSourceID]
      ,[SourceAdmissionSourceCode]
      ,[SourceAdmissionSource]
      --,[LocalAdmissionSourceID]
      ,[LocalAdmissionSourceCode]
      ,[LocalAdmissionSource]
      --,[NationalAdmissionSourceID]
      ,[NationalAdmissionSourceCode]
      ,[NationalAdmissionSource]
  FROM [WarehouseOLAPMergedV2].[APC].[AdmissionSource]


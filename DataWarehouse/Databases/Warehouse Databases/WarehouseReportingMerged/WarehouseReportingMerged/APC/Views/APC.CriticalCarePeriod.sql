﻿


create view [APC].[CriticalCarePeriod]

as

SELECT 
	 B.MergeEncounterRecno
	,B.SourceUniqueID
	,B.SourceSpellNo
	,B.SourcePatientNo
	,B.StartDate
	,B.StartTime
	,B.EndDate
	,B.EndTime
	,B.AdvancedCardiovascularSupportDays
	,B.AdvancedRespiratorySupportDays
	,B.BasicCardiovascularSupportDays
	,B.BasicRespiratorySupportDays
	,B.CriticalCareLevel2Days
	,B.CriticalCareLevel3Days
	,B.DermatologicalSupportDays
	,B.LiverSupportDays
	,B.NeurologicalSupportDays
	,B.RenalSupportDays
	,B.CreatedByUser
	,B.CreatedByTime
	,B.LocalIdentifier
	,B.LocationCode
	,B.StatusCode
	,B.TreatmentFunctionCode
	,B.PlannedAcpPeriod
	,B.InterfaceCode
	,B.CasenoteNumber
	,B.WardCode
	,B.AdmissionDate
	,B.SiteCode
	,B.NHSNumber
	,B.UnitFunctionCode
	,B.ContextCode
	,R.ContextID
	,R.SiteID
	,R.WardID
	,R.TreatmentFunctionID
	,R.StartDateID
	,R.EndDateID
  FROM WarehouseOLAPMergedV2.[APC].[BaseCriticalCarePeriod] B
  JOIN WarehouseOLAPMergedV2.[APC].[BaseCriticalCarePeriodReference] R ON R.MergeEncounterRecno = B.MergeEncounterRecno



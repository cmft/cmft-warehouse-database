﻿create view [APC].[HRG4Unbundled]

as

SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[HRGCode]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[HRG4Unbundled]

﻿
create view APC.OperationDetail as 

SELECT [MergeEncounterRecno]
      ,[OperationDetailSourceUniqueID]
      ,[MergeOperationDetailRecno]
      ,[EncounterContextCode]
      ,[ProcedureDetailContextCode]
      ,[SequenceNo]
  FROM [WarehouseOLAPMergedV2].[APC].[BaseEncounterOperationDetail]

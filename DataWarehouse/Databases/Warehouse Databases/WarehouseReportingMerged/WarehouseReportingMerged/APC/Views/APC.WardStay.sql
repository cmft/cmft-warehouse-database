﻿




CREATE view [APC].[WardStay] as

SELECT 
	 B.MergeEncounterRecno
	,B.ContextCode
	,B.SourceUniqueID
	,B.SourcePatientNo
	,B.SourceSpellNo
	,B.ProviderSpellNo
	,B.StartDate
	,B.EndDate
	,B.StartTime
	,B.EndTime
	,B.SiteCode
	,B.WardCode
	,B.StartActivityCode
	,B.EndActivityCode
	,B.DirectorateCode
	,R.ContextID
	,R.SiteID
	,R.WardID
  FROM WarehouseOLAPMergedV2.[APC].[BaseWardStay] B
  JOIN WarehouseOLAPMergedV2.[APC].[BaseWardStayReference] R ON R.MergeEncounterRecno = B.MergeEncounterRecno




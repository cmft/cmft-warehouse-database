﻿






CREATE view [APC].[Fact] as
SELECT 
	 A.MergeEncounterRecno
	,A.EncounterDateID
	,A.MetricID
	,A.ConsultantID
	,A.SpecialtyID
	,A.SiteID
	,A.AgeID
	,A.AdmissionMethodID
	,A.RTTActivity
	,A.RTTTreated
	,A.EncounterTimeOfDay
	,A.EndDirectorateID
	,A.SexID
	,A.ISTAdmissionSpecialtyID
	,A.ISTAdmissionDemandTimeOfDay
	,A.ISTDischargeTimeOfDay
	,A.ContextID
	,A.AdmissionSourceID
	,A.DischargeDestinationID
	,A.DischargeMethodID
	,A.IntendedManagementID
	,A.NeonatalLevelOfCareID
	,A.PatientClassificationID
	,A.SubSpecialtyID
	,A.CodingComplete
	,A.VTE
	,A.VTECategoryID
	,A.VTEExclusionReasonID
	,A.VTECompleteAndOrExclusion
	--,A.WardID
	,StartWardID
	,EndWardID
	,A.NationalExamID
	,A.ServiceID
	,A.Cases
	,A.LengthOfEncounter
  FROM [WarehouseOLAPMergedV2].[APC].[FactEncounter] A






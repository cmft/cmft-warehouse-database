﻿create view APC.ProcedureDetail as

SELECT [MergeEncounterRecno]
      ,[EncounterRecno]
      ,[OperationDetailSourceUniqueID]
      ,[MergeOperationDetailRecno]
      ,[ProcedureDetailSourceUniqueID]
      ,[MergeProcedureDetailRecno]
      ,[EncounterContextCode]
      ,[ProcedureDetailContextCode]
      ,[OperationStartDate]
  FROM [WarehouseOLAPMergedV2].[APC].[BaseEncounterProcedureDetail]

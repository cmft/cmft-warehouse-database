﻿CREATE VIEW APC.PatientCategory
AS
	SELECT 
	  [PatientCategoryID]
      ,[PatientCategoryCode]
      ,[PatientCategory]
	FROM 
		[WarehouseOLAPMergedV2].[APC].[PatientCategory]
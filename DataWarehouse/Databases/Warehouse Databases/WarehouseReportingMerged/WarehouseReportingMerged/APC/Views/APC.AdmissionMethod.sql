﻿
CREATE view [APC].[AdmissionMethod] as
SELECT 
[SourceContextCode]
      ,[SourceContext]
      ,[SourceAdmissionMethodID]
      ,[SourceAdmissionMethodCode]
      ,[SourceAdmissionMethod]
      --,[LocalAdmissionMethodID]
      ,[LocalAdmissionMethodCode]
      ,[LocalAdmissionMethod]
    --  ,[NationalAdmissionMethodID]
      ,[NationalAdmissionMethodCode]
      ,[NationalAdmissionMethod]
      ,[AdmissionTypeCode]
      ,[AdmissionType]
  FROM [WarehouseOLAPMergedV2].[APC].[AdmissionMethod]



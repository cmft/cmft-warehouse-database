﻿
create view [APC].[OperationStatus] as
SELECT [SourceContextCode]
      ,[SourceContext]
      ,[SourceOperationStatusID]
      ,[SourceOperationStatusCode]
      ,[SourceOperationStatus]
      --,[LocalOperationStatusID]
      ,[LocalOperationStatusCode]
      ,[LocalOperationStatus]
      --,[NationalOperationStatusID]
      ,[NationalOperationStatusCode]
      ,[NationalOperationStatus]
  FROM [WarehouseOLAPMergedV2].[APC].[OperationStatus]


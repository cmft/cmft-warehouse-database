﻿




CREATE view [APC].[StaffingLevel]
as
select
	StaffingLevelRecno
	,DivisionCode
	,WardCode
	,CensusDate
	,ShiftID
	,RegisteredNursePlan 
	,RegisteredNurseActual 
	,NonRegisteredNursePlan 
	,NonRegisteredNurseActual 
	,Comments
	,SeniorComments
from [Warehouse].[APC].[StaffingLevel]











﻿create view[APC].[HRG4Quality]
as

SELECT [MergeEncounterRecno]
      ,[SequenceNo]
      ,[QualityTypeCode]
      ,[QualityCode]
      ,[QualityMessage]
      ,[Created]
      ,[ByWhom]
  FROM [WarehouseOLAPMergedV2].[APC].[HRG4Quality]

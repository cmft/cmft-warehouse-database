﻿
create view [APC].[Bedday]

as

SELECT [MergeAPCEncounterRecno]
      ,[MergeDatasetEncounterRecno]
      ,[DatasetCode]
      ,[PODCode]
      ,[HRGCode]
      ,[CensusDate]
  FROM [WarehouseOLAPMergedV2].[APC].[Bedday]

﻿

CREATE View APC.BaseEncounterWithLearningDisabilityBySystem

as


Select 
	MergeEncounterRecno
	,GlobalProviderSpellNo 
	,GlobalEpisodeNo
	,SourceSystem.SourceSystemID
	,SourceSystem.SourceSystem
	,PatientSourceSystemFlag.PatientDatasetFlagRecno
from 
	APC.Encounter

inner join Match.Match.MatchTypeDataset 
on Encounter.MergeEncounterRecno = MatchTypeDataset.TargetDatasetRecno
and MatchTypeDataset.MatchTypeID = 5

inner join WarehouseOLAPMergedV2.Flag.PatientSourceSystemFlag
on MatchTypeDataset.SourceDatasetRecno = PatientSourceSystemFlag.PatientDatasetFlagRecno

inner join WarehouseOLAPMergedV2.Flag.SourceSystem
on PatientSourceSystemFlag.SourceSystemID = SourceSystem.SourceSystemID



﻿CREATE PROCEDURE [Legacy].[ETLLoadHarmCare]

AS

--if @StartDate < @StartDate
--BEGIN
--	SET @StartDate = @StartDate
--END


DECLARE @StartDate AS DATE
DECLARE @EndDate as DATE
DECLARE @Hours AS INT

SET @StartDate = '01 July 2012'
SET @EndDate = GETDATE()
SET @Hours = 24


--falls
SELECT
	EA.Bid

	,CrossCAL.TheDate
	,CrossCAL.WeekNo
	,CrossCAL.TheMonth
	,CrossCAL.FinancialQuarter
		
	,HS.SpellID
	,HS.Patient

	,EventTimeStamp = F.FallTS

	,HS.IPEpisode
	,HS.AdmitDate

	,Fall =	1
	,PUHFC1 = null
	,PUHFC2 = null
	,UTI = null
	,VTETreatment = null
into
	#Activity
FROM
	SmallDatasets.bedman.EventArchive EA

LEFT JOIN BedMan.dbo.HospSpell HS
ON EA.HospSpellID = HS.SpellID

CROSS JOIN WH.Calendar CrossCAL 

inner JOIN SmallDatasets.CQUIN.FallIncident F
ON	F.Bid = EA.Bid
AND	DATEDIFF(HH, FallTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours
AND	InitialSeverity > '1' -- Fall with harm

WHERE
	CrossCAL.TheDate BETWEEN @StartDate AND @EndDate


--pressure ulcer 1

merge
	#Activity target
using
	(
	SELECT
		EA.Bid

		,CrossCAL.TheDate
		,CrossCAL.WeekNo
		,CrossCAL.TheMonth
		,CrossCAL.FinancialQuarter
		
		,HS.SpellID
		,HS.Patient

		,EventTimeStamp = PUHFC1.IdentifiedTS

		,HS.IPEpisode
		,HS.AdmitDate
	FROM
		SmallDatasets.bedman.EventArchive EA

	LEFT JOIN BedMan.dbo.HospSpell HS
	ON EA.HospSpellID = HS.SpellID

	CROSS JOIN WH.Calendar CrossCAL 

	inner JOIN SmallDatasets.CQUIN.PressureUlcerCondition PUHFC1
	ON	PUHFC1.Bid = EA.Bid
	AND	PUHFC1.Category > 1
	AND	CAST(PUHFC1.IdentifiedTS AS DATE) = CrossCAL.TheDate

	WHERE
		CrossCAL.TheDate BETWEEN @StartDate AND @EndDate
	) source
	on	source.Bid = target.Bid
	and	source.TheDate = Target.TheDate

when not matched
then
	insert
		(
		Bid
		,TheDate
		,WeekNo
		,TheMonth
		,FinancialQuarter	
		,SpellID
		,Patient
		,EventTimeStamp
		,IPEpisode
		,AdmitDate
		,Fall
		,PUHFC1
		)
	values
		(
		 source.Bid
		,source.TheDate
		,source.WeekNo
		,source.TheMonth
		,source.FinancialQuarter	
		,source.SpellID
		,source.Patient
		,source.EventTimeStamp
		,source.IPEpisode
		,source.AdmitDate
		,0
		,1
		)

when matched
then
	update
	set
		target.EventTimeStamp = isnull(target.EventTimeStamp, source.EventTimeStamp)
		,target.PUHFC1 = 1

;

--pressure ulcer 2

merge
	#Activity target
using
	(
	SELECT
		EA.Bid

		,CrossCAL.TheDate
		,CrossCAL.WeekNo
		,CrossCAL.TheMonth
		,CrossCAL.FinancialQuarter
		
		,HS.SpellID
		,HS.Patient

		,EventTimeStamp = PUHFC2.IdentifiedTS

		,HS.IPEpisode
		,HS.AdmitDate
	FROM
		SmallDatasets.bedman.EventArchive EA

	LEFT JOIN BedMan.dbo.HospSpell HS
	ON EA.HospSpellID = HS.SpellID

	CROSS JOIN WH.Calendar CrossCAL 

	inner JOIN SmallDatasets.CQUIN.PressureUlcerCondition PUHFC2
	ON	PUHFC2.Bid = EA.Bid
	AND	PUHFC2.Category > 1
	AND	CAST(PUHFC2.IdentifiedTS AS DATE) = CrossCAL.TheDate	
	AND	DATEDIFF(HH, AdmitDate, PUHFC2.IdentifiedTS) > 72

	WHERE
		CrossCAL.TheDate BETWEEN @StartDate AND @EndDate
	) source
	on	source.Bid = target.Bid
	and	source.TheDate = Target.TheDate

when not matched
then
	insert
		(
		Bid
		,TheDate
		,WeekNo
		,TheMonth
		,FinancialQuarter	
		,SpellID
		,Patient
		,EventTimeStamp
		,IPEpisode
		,AdmitDate
		,Fall
		,PUHFC2
		)
	values
		(
		 source.Bid
		,source.TheDate
		,source.WeekNo
		,source.TheMonth
		,source.FinancialQuarter	
		,source.SpellID
		,source.Patient
		,source.EventTimeStamp
		,source.IPEpisode
		,source.AdmitDate
		,0
		,1
		)

when matched
then
	update
	set
		target.EventTimeStamp = isnull(target.EventTimeStamp, source.EventTimeStamp)
		,target.PUHFC2 = 1

;

--UTI

merge
	#Activity target
using
	(
	SELECT
		EA.Bid

		,CrossCAL.TheDate
		,CrossCAL.WeekNo
		,CrossCAL.TheMonth
		,CrossCAL.FinancialQuarter
		
		,HS.SpellID
		,HS.Patient

		,EventTimeStamp = Test.UrinaryTestResultsSentTS

		,HS.IPEpisode
		,HS.AdmitDate

	FROM
		SmallDatasets.bedman.EventArchive EA

	LEFT JOIN BedMan.dbo.HospSpell HS
	ON EA.HospSpellID = HS.SpellID

	CROSS JOIN WH.Calendar CrossCAL 

	inner JOIN SmallDatasets.CQUIN.UTITest Test
	ON Test.Bid = EA.Bid
	AND	DATEDIFF(HH, Test.UrinaryTestResultsSentTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours


	WHERE
		CrossCAL.TheDate BETWEEN @StartDate AND @EndDate

	and	exists
		(
		SELECT
			1
		FROM
			SmallDatasets.CQUIN.CatheterIntervention Cat

		INNER JOIN SmallDatasets.bedman.EventArchive EA2
		ON EA2.Bid = cat.Bid

		WHERE
			EA2.HospSpellID = EA.HospSpellID 
		AND cat.InsertedTS <= UrinaryTestResultsSentTS
		AND cat.CatheterType = 2
		)

	) source
	on	source.Bid = target.Bid
	and	source.TheDate = Target.TheDate

when not matched
then
	insert
		(
		Bid
		,TheDate
		,WeekNo
		,TheMonth
		,FinancialQuarter	
		,SpellID
		,Patient
		,EventTimeStamp
		,IPEpisode
		,AdmitDate
		,Fall
		,UTI
		)
	values
		(
		 source.Bid
		,source.TheDate
		,source.WeekNo
		,source.TheMonth
		,source.FinancialQuarter	
		,source.SpellID
		,source.Patient
		,source.EventTimeStamp
		,source.IPEpisode
		,source.AdmitDate
		,0
		,1
		)

when matched
then
	update
	set
		target.EventTimeStamp = isnull(target.EventTimeStamp, source.EventTimeStamp)
		,target.UTI = 1

;

--VTETreatment

merge
	#Activity target
using
	(
	SELECT
		EA.Bid

		,CrossCAL.TheDate
		,CrossCAL.WeekNo
		,CrossCAL.TheMonth
		,CrossCAL.FinancialQuarter
		
		,HS.SpellID
		,HS.Patient

		,EventTimeStamp = VTECondition.DiagnosisTS

		,HS.IPEpisode
		,HS.AdmitDate

	FROM
		SmallDatasets.bedman.EventArchive EA

	LEFT JOIN BedMan.dbo.HospSpell HS
	ON EA.HospSpellID = HS.SpellID

	CROSS JOIN WH.Calendar CrossCAL 

	inner JOIN SmallDatasets.CQUIN.VTECondition
	ON	VTECondition.Bid = EA.Bid
	AND	DATEDIFF(HH, DiagnosisTS, CrossCAL.TheDate) BETWEEN 0 AND @Hours	


	WHERE
		CrossCAL.TheDate BETWEEN @StartDate AND @EndDate

	) source
	on	source.Bid = target.Bid
	and	source.TheDate = Target.TheDate

when not matched
then
	insert
		(
		Bid
		,TheDate
		,WeekNo
		,TheMonth
		,FinancialQuarter	
		,SpellID
		,Patient
		,EventTimeStamp
		,IPEpisode
		,AdmitDate
		,Fall
		,VTETreatment
		)
	values
		(
		 source.Bid
		,source.TheDate
		,source.WeekNo
		,source.TheMonth
		,source.FinancialQuarter	
		,source.SpellID
		,source.Patient
		,source.EventTimeStamp
		,source.IPEpisode
		,source.AdmitDate
		,0
		,1
		)

when matched
then
	update
	set
		target.EventTimeStamp = isnull(target.EventTimeStamp, source.EventTimeStamp)
		,target.VTETreatment = 1

;


TRUNCATE TABLE Legacy.HarmCare

INSERT INTO Legacy.HarmCare

select distinct
	 Activity.TheDate
	,Activity.WeekNo
	,Activity.TheMonth
	,Activity.FinancialQuarter
	,Activity.SpellID
	,Activity.Patient
	,EncounterRecNo = Encounter.MergeEncounterRecno
	,WardEncounterRecNo = APC.f_GetWardEncounterRecNo(Encounter.ProviderSpellNo, EventTimeStamp)
	,Activity.IPEpisode
	,Activity.AdmitDate
	,Directorate.Division
	,SourceWardCode = APC.f_GetWardCode(Encounter.ProviderSpellNo, EventTimeStamp)
	,Activity.Fall
	,PUHFC1 = isnull(Activity.PUHFC1, 0)
	,PUHFC2 = isnull(Activity.PUHFC2, 0)
	,UTI = isnull(Activity.UTI, 0)
	,VTETreatment = isnull(Activity.VTETreatment, 0)
	,PatientCategory = Encounter.PatientCategoryCode
from
	#Activity Activity

LEFT JOIN APC.Encounter
ON	Activity.Patient = Encounter.SourcePatientNo
AND	Activity.AdmitDate = Encounter.AdmissionTime
AND	Encounter.FirstEpisodeInSpellIndicator = 1

LEFT JOIN WH.Directorate Directorate 
ON Directorate.DirectorateCode = Encounter.StartDirectorateCode

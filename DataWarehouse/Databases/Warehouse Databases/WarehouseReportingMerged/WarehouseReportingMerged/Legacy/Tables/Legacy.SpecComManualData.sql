﻿CREATE TABLE [Legacy].[SpecComManualData] (
    [IndicatorID]     INT             NULL,
    [MeasureNumber]   VARCHAR (20)    NULL,
    [PeriodStartDate] DATETIME        NULL,
    [Cases]           INT             NULL,
    [Numerator]       DECIMAL (18, 4) NULL,
    [Denominator]     DECIMAL (18, 4) NULL
);


﻿CREATE TABLE [Legacy].[SpecComFact] (
    [IndicatorID]   INT             NOT NULL,
    [EncounterDate] DATETIME        NOT NULL,
    [Cases]         INT             NULL,
    [Numerator]     DECIMAL (18, 4) NULL,
    [Denominator]   DECIMAL (18, 4) NULL
);


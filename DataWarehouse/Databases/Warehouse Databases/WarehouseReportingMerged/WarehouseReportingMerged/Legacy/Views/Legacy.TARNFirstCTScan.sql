﻿
create VIEW [Legacy].[TARNFirstCTScan]

AS

SELECT	DistrictNo				= DistrictNo,
		ArrivalTime				= cast(E.arrivaldate as varchar(10)) + ' ' + cast(E.arrivaltime as varchar(20)),
									MIN(cast(E.ctscandate as varchar(10)) + ' ' + cast(E.ctscantime as varchar(10))) FirstCTScanDate,
		MajorTraumaTriage		= E.MajorTraumaTriage,
		PreviousHospital		= E.PreviousHospital ,
		TimeDiff				= DATEDIFF(MI,cast(E.arrivaldate as varchar(10)) + ' ' + cast(E.arrivaltime as varchar(20)),
									MIN(cast(E.ctscandate as varchar(10)) + ' ' + cast(E.ctscantime as varchar(10)))) 
FROM  WarehouseReporting.TARN.Encounter E
WHERE E.CTScanDate IS NOT NULL
GROUP BY DistrictNo, cast(E.arrivaldate as varchar(10)) + ' ' + cast(E.arrivaltime as varchar(20)),MajorTraumaTriage, PreviousHospital

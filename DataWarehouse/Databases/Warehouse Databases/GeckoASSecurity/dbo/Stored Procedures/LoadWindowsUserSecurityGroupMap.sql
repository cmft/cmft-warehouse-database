﻿CREATE proc [dbo].[LoadWindowsUserSecurityGroupMap]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@inserted int


/* add new records to WindowsUserSecurityGroupMap */

insert into dbo.WindowsUserSecurityGroupMap
(
	Userid
	,SecurityGroupCode
)

select
	Userid
	,SecurityGroupCode

from
	 dbo.WindowsUserCodeMap
where
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode
			and WindowsUserSecurityGroupMap.Userid = WindowsUserCodeMap.Userid
				
			)

union

select
	Userid
	,'MedicineAndCommunityUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Peter.Selby'
				,'CMMC\Handrean.Soran'
				,'CMMC\Anthony.Smith2'
				--,'CMMC\Darren.Griffiths'
				,'CMMC\Gareth.Summerfield'
				,'CMMC\simon.bailey'
				,'CMMC\TaniaA.Syed'
				,'CMMC\Tina.Davies'
				,'CMMC\Yuen.Looi'
				,'CMMC\Ben.Dearden'
				,'CMMC\Davidr.Evans'
				,'CMMC\David.Russell-Evans'
				,'CMMC\Kayliegh.tomany'
				,'CMMC\Rachel.Royston'

				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'MedicineAndCommunityUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'ClinicalAndScientificUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				--'CMMC\Adam.Black'
				'CMMC\Darren.Griffiths'

				,'CMMC\magnus.garrioch'
				,'CMMC\Bernard.Foex'
				,'CMMC\dougal.atkinson'
				,'CMMC\hilary.gough'
				,'CMMC\John.Butler'
				,'CMMC\John.Moore'
				,'CMMC\maheshhan.nirmalan'
				,'CMMC\Michael.Parker'
				,'CMMC\Rachael.Challiner'
				,'CMMC\roger.slater'
				,'CMMC\Ruari.Greer'
				,'CMMC\Srikanth.Amudalapalli'
				,'CMMC\steve.benington'
				,'CMMC\Steve.Jones'
				,'CMMC\Jane.Eddleston'
				--,'CMMC\Oliver.Radford'
				,'CMMC\Jason.Ratcliffe'
				,'CMMC\daniel.conway'
				,'CMMC\John.Schofield'
				,'CMMC\Ben.Dearden'

				)
				
				
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'ClinicalAndScientificUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'ChildrensUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\James.Watson'
				,'CMMC\Ben.Dearden'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'ChildrensUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'DentalUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Glyn.wood'
				,'CMMC\Ben.Dearden'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'DentalUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'DentalUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Glyn.wood'
				,'CMMC\Ben.Dearden'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'DentalUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)
			
union

select
	Userid
	,'SaintMary''sUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ian.Daniels'
				,'CMMC\Ben.Robinson'
				,'CMMC\Ben.Dearden'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'SaintMary''sUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)



union

select
	Userid
	,'SurgicalUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Neil.Parrott'
				,'CMMC\Adam.Black'
				,'CMMC\John.Schofield'
				--,'CMMC\Darren.Griffiths'
				,'CMMC\Gareth.Summerfield'
				,'CMMC\Ben.Dearden'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'SurgicalUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
			)


union

select
	Userid
	,'SpecialistMedicineUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ben.Kenyon2'
				,'CMMC\Reid.Tiley'
				,'CMMC\Alastair.Hutchison'
				,'CMMC\Bushrah.Naz-Tyrer'
				,'CMMC\Dugra.kanigicherla'
				,'CMMC\Ben.Dearden'
				--,'CMMC\Darren.Griffiths'

				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'SpecialistMedicineUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
			)


union

select
	Userid
	,'TraffordUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Gary.White'
				,'CMMC\Ben.Dearden'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'TraffordUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

--union

--select
--	Userid
--	,'CriticalCareMedicineUsers'
--from
--	[dbo].[WindowsUser]
--where
--	[Userid] in 
--				(
--				'CMMC\magnus.garrioch'
--				,'CMMC\Bernard.Foex'
--				,'CMMC\dougal.atkinson'
--				,'CMMC\hilary.gough'
--				,'CMMC\John.Butler'
--				,'CMMC\John.Moore'
--				,'CMMC\maheshhan.nirmalan'
--				,'CMMC\Michael.Parker'
--				,'CMMC\Rachael.Challiner'
--				,'CMMC\roger.slater'
--				,'CMMC\Ruari.Greer'
--				,'CMMC\Srikanth.Amudalapalli'
--				,'CMMC\steve.benington'
--				,'CMMC\Steve.Jones'
--				,'CMMC\Jane.Eddleston'
--				--,'CMMC\Oliver.Radford'
--				,'CMMC\Jason.Ratcliffe'
--				,'CMMC\daniel.conway'
--				,'CMMC\John.Schofield'

--				)
--and
--	not exists
--			(
--			select
--				1
--			from
--				dbo.WindowsUserSecurityGroupMap
--			where
--				WindowsUserSecurityGroupMap.SecurityGroupCode = 'CriticalCareMedicineUsers'
--			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
--			)



union

select
	Userid
	,'GynaecologicalOncologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Cathrine.Holland'
				,'CMMC\henry.kitchener'
				,'CMMC\Richard.Clayton'
				,'CMMC\Saad.Ali'
				,'CMMC\ursula.winters'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'GynaecologicalOncologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


union

select
	Userid
	,'GynaecologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'THT\FMAZARELO'
				,'THT\ELGHAZAWY'
				,'THT\ANYSENBAUM'
				,'THT\SHOTCHKIES'
				,'THT\RHOWELL'
				,'THT\AKARIM'
				,'CMMC\Anthony.Smith'
				,'THT\FABUAMNA2'
				,'CMMC\Cathrine.Holland'
				,'CMMC\Cheryl.Fitzgerald'
				,'CMMC\Edmond.Edi-Osagie'
				,'CMMC\Fiona.Reid'
				,'CMMC\Gail.Busby'
				,'CMMC\henry.kitchener'
				,'CMMC\Joseph.Mechery'
				,'CMMC\kingshuk.majumder'
				,'CMMC\Karen.Ward'
				,'CMMC\Leroy.Edozien'
				,'CMMC\Mourad.Seif'
				,'CMMC\Richard.Clayton'
				,'CMMC\Rosemary.Howell'
				,'CMMC\Kristina.Naidoo'
				,'CMMC\Saad.Ali'
				,'CMMC\ursula.winters'
				,'CMMC\yasmin.sajjad'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'GynaecologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'NeonatologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Anthony.Emmerson'
				,'CMMC\Ajit.Mahaveer'
				,'CMMC\Edward.Gasiorowski'
				,'CMMC\Ian.Dady'
				,'CMMC\Karthik.Ganesan'
				,'CMMC\Michelle.Parr'
				,'CMMC\Minju.Kuruvilla'
				,'CMMC\Mohammad.Sarwar'
				,'CMMC\Ngozi.Edi-Osagie'
				,'CMMC\Ruth.Gottstein'
				,'CMMC\Simon.Mitchell'
				,'CMMC\Suresh.Victor'
				--,'CMMC\darren.griffiths'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'NeonatologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'ObstetricsUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Anthony.Emmerson'
				,'CMMC\Clare.Tower'
				,'CMMC\Edward.Johnstone'
				,'CMMC\Joanna.Gillham'
				,'CMMC\Jenny.Myers'
				,'CMMC\Koon.Chan'
				,'CMMC\Leroy.Edozien'
				,'CMMC\Louise.Byrd'
				,'CMMC\melissa.whitworth'
				,'CMMC\Michael.Maresh'
				,'CMMC\Philip.Bullen'
				,'CMMC\Rosemary.Howell'
				,'CMMC\Rebekah.Samangaya'
				,'CMMC\Sheher.Khizar'
				,'CMMC\Simon.Mitchell'
				,'CMMC\Sarah.Vause'
				,'CMMC\teresa.kelly'
				,'CMMC\ursula.winters'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'ObstetricsUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'GastroenterologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Anthony.moffat'
				,'CMMC\Narendra.Kochar'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'GastroenterologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


union

select
	Userid
	,'NephrologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Iren.Szeki'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'NephrologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)
			
union

select
	Userid
	,'EndocrinologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ashish.Sukthankar'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'EndocrinologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


union

select
	Userid
	,'DiabeticMedicineUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ashish.Sukthankar'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'DiabeticMedicineUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)
union

select
	Userid
	,'ClinicalImmunologyAndAllergyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ashish.Sukthankar'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'ClinicalImmunologyAndAllergyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


union

select
	Userid
	,'DermatologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ashish.Sukthankar'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'DermatologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'NeurologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ashish.Sukthankar'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'NeurologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)
						
union

select
	Userid
	,'RheumatologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ashish.Sukthankar'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'RheumatologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'GenitourinaryMedicineUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMc\Chitra.babu'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'GenitourinaryMedicineUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


union

select
	Userid
	,'CardiacSurgeryUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Edward.McLaughlin'
				,'CMMC\Sahrkaw.muhyaldeen'
				,'CMMC\Suzanne.Chaisty'
				,'MHC\Suzanne.Chaisty'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'CardiacSurgeryUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

union

select
	Userid
	,'CardiologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Edward.McLaughlin'
				,'cmmc\Sahrkaw.muhyaldeen'
				,'CMMC\Suzanne.Chaisty'
				,'MHC\Suzanne.Chaisty'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'CardiologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


union

select
	Userid
	,'ClinicalHaematologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Eleni.Tholouli'
				,'CMMC\lesley.hill'

				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'ClinicalHaematologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)														
select
	@inserted = @@rowcount
	,@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
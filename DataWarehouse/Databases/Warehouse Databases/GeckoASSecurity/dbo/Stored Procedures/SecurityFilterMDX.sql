﻿CREATE procedure [dbo].[SecurityFilterMDX]
	 @UserAccount varchar(50)
	,@EntityTypeCode varchar(255)
as


select distinct
	SecurityGroupEntityTypeMap.Member
from
	dbo.SecurityGroupEntityTypeMap
where
	SecurityGroupEntityTypeMap.EntityTypeCode = upper(@EntityTypeCode)
and	exists
	(
	select
		1
	from
		dbo.WindowsUserSecurityGroupMap
	where
		upper(WindowsUserSecurityGroupMap.Userid) = upper(@UserAccount)
	and	WindowsUserSecurityGroupMap.SecurityGroupCode = SecurityGroupEntityTypeMap.SecurityGroupCode
	)
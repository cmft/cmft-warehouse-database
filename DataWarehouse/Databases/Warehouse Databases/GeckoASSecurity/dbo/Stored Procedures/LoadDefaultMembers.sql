﻿CREATE proc [dbo].[LoadDefaultMembers]

as
	
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@inserted int
	,@deleted int
	
insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

--All members for 1-1 Consultant mapping

--default

select
	SecurityGroupCode
	,'DIRECTORATE'
	,'[Directorate].[Division].[All]'
from
	dbo.SecurityGroupEntityTypeMap	
where
	EntityTypeCode = 'CONSULTANT'
and
	not exists
				(
					select
						1
					from
						dbo.SecurityGroupEntityTypeMap	Populated
					where
						Populated.SecurityGroupCode = SecurityGroupEntityTypeMap.SecurityGroupCode
					and
						Populated.EntityTypeCode = 'DIRECTORATE'
					and
						Populated.Member = '[Directorate].[Division].[All]'
				)	
union

select
	SecurityGroupCode
	,'SPECIALTY'
	,'[Specialty].[National Specialty].[All]'	
from
	dbo.SecurityGroupEntityTypeMap	
where
	EntityTypeCode = 'CONSULTANT'
and
	not exists
				(
					select
						1
					from
						dbo.SecurityGroupEntityTypeMap	Populated
					where
						Populated.SecurityGroupCode = SecurityGroupEntityTypeMap.SecurityGroupCode
					and
						Populated.EntityTypeCode = 'SPECIALTY'
					and
						Populated.Member = '[Specialty].[National Specialty].[All]'	
				)

union

--All members for Directorate mapping

--default

select
	SecurityGroupCode
	,'CONSULTANT'
	,'[Consultant].[Source Consultant].[All]'
from
	dbo.SecurityGroupEntityTypeMap	
where
	EntityTypeCode = 'DIRECTORATE'
and
	not exists
				(
					select
						1
					from
						dbo.SecurityGroupEntityTypeMap	Populated
					where
						Populated.SecurityGroupCode = SecurityGroupEntityTypeMap.SecurityGroupCode
					and
						Populated.EntityTypeCode = 'DIRECTORATE'
					and
						Populated.Member = '[Consultant].[Source Consultant].[All]'
				)		
union

select
	SecurityGroupCode
	,'SPECIALTY'
	,'[Specialty].[National Specialty].[All]'	
from
	dbo.SecurityGroupEntityTypeMap	
where
	EntityTypeCode = 'DIRECTORATE'
and
	not exists
				(
					select
						1
					from
						dbo.SecurityGroupEntityTypeMap	Populated
					where
						Populated.SecurityGroupCode = SecurityGroupEntityTypeMap.SecurityGroupCode
					and
						Populated.EntityTypeCode = 'SPECIALTY'
					and
						Populated.Member = '[Specialty].[National Specialty].[All]'	
				)	
union

--All members for Specialty mapping

--default

select
	SecurityGroupCode
	,'CONSULTANT'
	,'[Consultant].[Source Consultant].[All]'
from
	dbo.SecurityGroupEntityTypeMap	
where
	EntityTypeCode = 'SPECIALTY'
and
	not exists
				(
					select
						1
					from
						dbo.SecurityGroupEntityTypeMap	Populated
					where
						Populated.SecurityGroupCode = SecurityGroupEntityTypeMap.SecurityGroupCode
					and
						Populated.EntityTypeCode = 'CONSULTANT'
					and
						Populated.Member = '[Consultant].[Source Consultant].[All]'
				)		
union

select
	SecurityGroupCode
	,'DIRECTORATE'
	,'[Directorate].[Division].[All]'	
from
	dbo.SecurityGroupEntityTypeMap	
where
	EntityTypeCode = 'SPECIALTY'
and
	not exists
				(
					select
						1
					from
						dbo.SecurityGroupEntityTypeMap	Populated
					where
						Populated.SecurityGroupCode = SecurityGroupEntityTypeMap.SecurityGroupCode
					and
						Populated.EntityTypeCode = 'DIRECTORATE'
					and
						Populated.Member = '[Directorate].[Division].[All]'
				)	

select
	@inserted = @@rowcount


--remove 1-1 mapping where assocaition with group exists				
	
delete from ConsultantMap
from
	[dbo].[WindowsUserSecurityGroupMap] ConsultantMap
	
where
	left(SecurityGroupCode,6) = 'CONS||'
and
	exists
		(
			select
				1
			from
				[dbo].[WindowsUserSecurityGroupMap] GroupMap
			where
				left(SecurityGroupCode,6) <> 'CONS||'
			and
				ConsultantMap.Userid = GroupMap.Userid
		)



select
	@deleted = @@rowcount
	,@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
	
		
--select
--	*
--from
--	[GeckoASSecurity].[dbo].[WindowsUserSecurityGroupEntityTypeMap] 
--where
--	[Userid] like '%pear%'
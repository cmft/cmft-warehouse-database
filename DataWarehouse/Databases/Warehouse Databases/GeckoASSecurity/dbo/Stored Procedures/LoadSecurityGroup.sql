﻿CREATE proc [dbo].[LoadSecurityGroup]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@inserted int



insert into dbo.SecurityGroup
(
	SecurityGroupCode
	,Administrator
)
 
select
	SecurityGroupCode
	,0
from
	 dbo.WindowsUserCodeMap
where
	not exists
			(
			select
				1
			from
				dbo.SecurityGroup
			where
				SecurityGroup.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode
			)


insert into dbo.SecurityGroup
(
	SecurityGroupCode
	,Administrator
)

select
	distinct replace(replace(NationalSpecialty, ' ' ,''),'&','And') + 'Users'
	,0
from
	WarehouseOLAPMergedV2.WH.Specialty
	
where
	NationalSpecialty not like '%Retired%'
and	NationalSpecialty not like '%Not A Treatment Function%'
and	NationalSpecialty not like '%Unassigned%'
and	not exists
			(
			select
				1
			from
				dbo.SecurityGroup
			where
				SecurityGroup.SecurityGroupCode = replace(replace(NationalSpecialty, ' ' ,''),'&','And') + 'Users'
			)

union

select
	distinct replace(replace(Division, ' ' ,''),'&','And') + 'Users'
	,0
from
	WarehouseOLAPMergedV2.WH.Directorate
	
where
	Division not like '%N/A%'
and
	Division not like 'MH TRUST'
and
	not exists
			(
			select
				1
			from
				dbo.SecurityGroup
			where
				SecurityGroup.SecurityGroupCode = replace(replace(Division, ' ' ,''),'&','And') + 'Users'
			)
	
select
	@inserted = @@rowcount
	,@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
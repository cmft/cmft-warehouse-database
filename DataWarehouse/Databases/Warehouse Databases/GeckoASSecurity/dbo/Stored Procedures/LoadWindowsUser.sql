﻿create proc [dbo].[LoadWindowsUser]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@inserted int



insert into dbo.WindowsUser
(
	Userid
	,Username
	,Active
)
 
select
	Userid
	,'CMFTUser'
	,1
from
	 dbo.WindowsUserCodeMap
	  

where
	not exists
			(
			select
				1
			from
				dbo.WindowsUser
			where
				WindowsUser.Userid = WindowsUserCodeMap.Userid
			)

union

select
      Login
      ,'CMFTUser'
	,1
from
	[SHAREPOINT]...[Security Map] SecurityMap
	
where
	not exists
			(
			select
				1
			from
				dbo.WindowsUser
				
			where
				WindowsUser.Userid = SecurityMap.Login
			)
	
	
select
	@inserted = @@rowcount
	,@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
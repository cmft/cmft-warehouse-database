﻿CREATE proc [dbo].[LoadSecurityGroupEntityTypeMap]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@inserted int




/* add new records to SecurityGroupEntityTypeMap */

truncate table [dbo].[SecurityGroupEntityTypeMap]

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'CONSULTANT'
	,Member
from
	 dbo.WindowsUserCodeMap

union 

select
	distinct replace(replace(Division, ' ' ,''),'&','And') + 'Users'
	,'DIRECTORATE'
	,'[Directorate].[Division].&['+Division+']'
from
	WarehouseOLAPMergedV2.WH.Directorate
	
inner join dbo.SecurityGroup
on	replace(replace(Division, ' ' ,''),'&','And') + 'Users' = SecurityGroupCode


--insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
--values ('CriticalCareMedicineUsers', 'DIRECTORATE', '[Directorate].[Directorate].&[17]')

union 

select distinct
	replace(replace(NationalSpecialty, ' ' ,''),'&','And') + 'Users'
	,'SPECIALTY'
	,'[Specialty].[National Specialty].&[' + NationalSpecialtyCode +']'
from
	WarehouseOLAPMergedV2.WH.Specialty
	
inner join dbo.SecurityGroup
on	replace(replace(NationalSpecialty, ' ' ,''),'&','And') + 'Users' = SecurityGroupCode


select
	@inserted = @@rowcount
	,@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
﻿create proc [dbo].[LoadConsultantWarehouseEntityXRef]

as

insert into	Warehouse.dbo.EntityXref
(
EntityTypeCode
,EntityCode
,XrefEntityTypeCode
,XrefEntityCode
)
  
select
	'CONSULTANTPASCODE'
	,SourceValueCode
	,'CONSULTANTWINDOWSLOGIN'
	,Userid
from
	dbo.WindowsUserCodeMap
where
	ContextCode = 'CEN||PAS'
and
	not exists
(
	select
		1
	from
		Warehouse.dbo.EntityXref
	where
		EntityXref.EntityTypeCode = 'CONSULTANTPASCODE'
	and
		WindowsUserCodeMap.SourceValueCode = EntityXref.EntityCode
	and
		EntityXref.XrefEntityTypeCode = 'CONSULTANTWINDOWSLOGIN'
	and
		WindowsUserCodeMap.Userid = EntityXref.XrefEntityCode
)
﻿CREATE proc [dbo].[LoadSecurityModel]

as

exec dbo.LoadWindowsUser
exec dbo.LoadSecurityGroup
exec dbo.LoadSecurityGroupEntityTypeMap
exec dbo.LoadWindowsUserSecurityGroupMap
exec dbo.LoadDefaultMembers


exec dbo.LoadConsultantWarehouseEntityXRef --sync changes with EntityXRef in Warehouse
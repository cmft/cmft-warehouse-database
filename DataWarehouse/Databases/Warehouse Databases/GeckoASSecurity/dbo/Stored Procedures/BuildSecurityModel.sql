﻿CREATE proc [dbo].[BuildSecurityModel]

as

/* add new records to WindowsUser */

insert into dbo.WindowsUser
(
	Userid
	,Username
	,Active
)
 
select
	Userid
	,'CMFTUser'
	,1
from
	 dbo.WindowsUserCodeMap
	  

where
	not exists
			(
			select
				1
			from
				dbo.WindowsUser
			where
				WindowsUser.Userid = WindowsUserCodeMap.Userid
			)

union

select
      Login
      ,'CMFTUser'
	,1
from
	[SHAREPOINT]...[Security Map] SecurityMap
	
where
	not exists
			(
			select
				1
			from
				dbo.WindowsUser
			where
				WindowsUser.Userid = SecurityMap.Login
			)
	
	
/* add new records to SecurityGroup */

insert into dbo.SecurityGroup
(
	SecurityGroupCode
	,Administrator
)
 
select
	SecurityGroupCode
	,0
from
	 dbo.WindowsUserCodeMap
where
	not exists
			(
			select
				1
			from
				dbo.SecurityGroup
			where
				SecurityGroup.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode
			)



/* add new records to WindowsUserSecurityGroupMap */

insert into dbo.WindowsUserSecurityGroupMap
(
	Userid
	,SecurityGroupCode
)

select
	Userid
	,SecurityGroupCode

from
	 dbo.WindowsUserCodeMap
where
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode
			and WindowsUserSecurityGroupMap.Userid = WindowsUserCodeMap.Userid
				
			)


insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'AcuteMedicalUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Peter.Selby'
				,'CMMC\Handrean.Soran'
				,'CMMC\Anthony.Smith2'
				,'CMMC\Darren.Griffiths'

				,'CMMC\simon.bailey'
				,'CMMC\TaniaA.Syed'
				,'CMMC\Tina.Davies'
				,'CMMC\Yuen.Looi'

				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'AcuteMedicalUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'CSSUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Adam.Black'
				--,'CMMC\Darren.Griffiths'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'CSSUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'ChildrensUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\James.Watson'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'ChildrensUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'StMarysUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ian.Daniels'
				,'CMMC\Ben.Robinson'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'StMarysUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)



insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'SurgeryUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Neil.Parrott'
				,'CMMC\Adam.Black'
				,'CMMC\John.Schofield'
				--,'CMMC\Darren.Griffiths'
				,'CMMC\Gareth.Summerfield'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'SurgeryUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
			)


insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'SpecialistMedicineUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Ben.Kenyon'
				,'CMMC\Reid.Tiley'
				,'CMMC\Alastair.Hutchison'
				,'CMMC\Bushrah.Naz-Tyrer'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'SpecialistMedicineUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
			)


insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'TraffordUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Gary.White'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'TraffordUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'CriticalCareUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\magnus.garrioch'
				,'CMMC\Bernard.Foex'
				,'CMMC\dougal.atkinson'
				,'CMMC\hilary.gough'
				,'CMMC\John.Butler'
				,'CMMC\John.Moore'
				,'CMMC\maheshhan.nirmalan'
				,'CMMC\Michael.Parker'
				,'CMMC\Rachael.Challiner'
				,'CMMC\roger.slater'
				,'CMMC\Ruari.Greer'
				,'CMMC\Srikanth.Amudalapalli'
				,'CMMC\steve.benington'
				,'CMMC\Steve.Jones'
				,'CMMC\Jane.Eddleston'
				--,'CMMC\Oliver.Radford'
				,'CMMC\Jason.Ratcliffe'
				,'CMMC\daniel.conway'
				,'CMMC\John.Schofield'

				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'CriticalCareUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)



insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'GynaecologyOncologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Cathrine.Holland'
				,'CMMC\henry.kitchener'
				,'CMMC\Richard.Clayton'
				,'CMMC\Saad.Ali'
				,'CMMC\ursula.winters'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'GynaecologyOncologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)


insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'GynaecologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'THT\FMAZARELO'
				,'THT\ELGHAZAWY'
				,'THT\ANYSENBAUM'
				,'THT\SHOTCHKIES'
				,'THT\RHOWELL'
				,'THT\AKARIM'
				,'CMMC\Anthony.Smith'
				,'THT\FABUAMNA2'
				,'CMMC\Cathrine.Holland'
				,'CMMC\Cheryl.Fitzgerald'
				,'CMMC\Edmond.Edi-Osagie'
				,'CMMC\Fiona.Reid'
				,'CMMC\Gail.Busby'
				,'CMMC\henry.kitchener'
				,'CMMC\Joseph.Mechery'
				,'CMMC\kingshuk.majumder'
				,'CMMC\Karen.Ward'
				,'CMMC\Leroy.Edozien'
				,'CMMC\Mourad.Seif'
				,'CMMC\Richard.Clayton'
				,'CMMC\Rosemary.Howell'
				,'CMMC\Kristina.Naidoo'
				,'CMMC\Saad.Ali'
				,'CMMC\ursula.winters'
				,'CMMC\yasmin.sajjad'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'GynaecologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)

insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'NeonatologyUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Anthony.Emmerson'
				,'CMMC\Ajit.Mahaveer'
				,'CMMC\Edward.Gasiorowski'
				,'CMMC\Ian.Dady'
				,'CMMC\Karthik.Ganesan'
				,'CMMC\Michelle.Parr'
				,'CMMC\Minju.Kuruvilla'
				,'CMMC\Mohammad.Sarwar'
				,'CMMC\Ngozi.Edi-Osagie'
				,'CMMC\Ruth.Gottstein'
				,'CMMC\Simon.Mitchell'
				,'CMMC\Suresh.Victor'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'NeonatologyUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)



insert into [dbo].[WindowsUserSecurityGroupMap]

select
	Userid
	,'ObstetricsUsers'
from
	[dbo].[WindowsUser]
where
	[Userid] in 
				(
				'CMMC\Anthony.Emmerson'
				,'CMMC\Clare.Tower'
				,'CMMC\Edward.Johnstone'
				,'CMMC\Joanna.Gillham'
				,'CMMC\Jenny.Myers'
				,'CMMC\Koon.Chan'
				,'CMMC\Leroy.Edozien'
				,'CMMC\Louise.Byrd'
				,'CMMC\melissa.whitworth'
				,'CMMC\Michael.Maresh'
				,'CMMC\Philip.Bullen'
				,'CMMC\Rosemary.Howell'
				,'CMMC\Rebekah.Samangaya'
				,'CMMC\Sheher.Khizar'
				,'CMMC\Simon.Mitchell'
				,'CMMC\Sarah.Vause'
				,'CMMC\teresa.kelly'
				,'CMMC\ursula.winters'
				)
and
	not exists
			(
			select
				1
			from
				dbo.WindowsUserSecurityGroupMap
			where
				WindowsUserSecurityGroupMap.SecurityGroupCode = 'ObstetricsUsers'
			and WindowsUserSecurityGroupMap.Userid = WindowsUser.Userid
				
			)



/* add new records to SecurityGroupEntityTypeMap */

truncate table [dbo].[SecurityGroupEntityTypeMap]

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'CONSULTANT'
	,Member
from
	 dbo.WindowsUserCodeMap

where
	not exists
			(
			select
				1
			from
				dbo.SecurityGroupEntityTypeMap
			where
				SecurityGroupEntityTypeMap.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode
			and
				SecurityGroupEntityTypeMap.EntityTypeCode = 'CONSULTANT'
			)


insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'MORTALITYCONSULTANT'
	,Member
from
	 dbo.WindowsUserCodeMap

where
	not exists
			(
			select
				1
			from
				dbo.SecurityGroupEntityTypeMap
			where
				SecurityGroupEntityTypeMap.SecurityGroupCode = WindowsUserCodeMap.SecurityGroupCode
			and
				SecurityGroupEntityTypeMap.EntityTypeCode = 'MORTALITYCONSULTANT'
			)


insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('AcuteMedicalUsers', 'DIRECTORATE', '[Directorate].[Division].&[Acute Medical]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('CriticalCareUsers', 'DIRECTORATE', '[Directorate].[Directorate].&[17]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('SurgeryUsers', 'DIRECTORATE', '[Directorate].[Division].&[Surgical]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('CSSUsers', 'DIRECTORATE', '[Directorate].[Division].&[Clinical & Scientific]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('TraffordUsers', 'DIRECTORATE', '[Directorate].[Division].&[Trafford]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('StMarysUsers', 'DIRECTORATE', '[Directorate].[Division].&[St Marys]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('ChildrensUsers', 'DIRECTORATE', '[Directorate].[Division].&[Childrens]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('SpecialistMedicineUsers', 'DIRECTORATE', '[Directorate].[Division].&[Specialist Medical]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('ObstetricsUsers', 'SPECIALTY', '[Specialty].[National Specialty].&[501]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('NeonatologyUsers', 'SPECIALTY', '[Specialty].[National Specialty].&[422]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('GynaecologyUsers', 'SPECIALTY', '[Specialty].[National Specialty].&[502]')

insert into dbo.SecurityGroupEntityTypeMap (SecurityGroupCode, EntityTypeCode, Member) 
values ('GynaecologyOncologyUsers', 'SPECIALTY', '[Specialty].[National Specialty].&[503]')



/* remove 1-1 consultant mapping if user belongs to group */

delete from dbo.SecurityGroupEntityTypeMap

where
	SecurityGroupCode in 
						(
							select
								SecurityGroupCode
							from
								[dbo].[WindowsUserSecurityGroupMap] A
							where
								(
								exists
										(
										select
											[Userid]
										from
											[dbo].[SecurityGroupEntityTypeMap]

										inner join
											[dbo].[WindowsUserSecurityGroupMap] B
										on	[SecurityGroupEntityTypeMap].[SecurityGroupCode] = B.[SecurityGroupCode]

										where
											EntityTypeCode = 'DIRECTORATE' 
										and
											A.Userid = B.Userid
										)
								or exists
										(
										select
											[Userid]
										from
											[dbo].[SecurityGroupEntityTypeMap]

										inner join
											[dbo].[WindowsUserSecurityGroupMap] B
										on	[SecurityGroupEntityTypeMap].[SecurityGroupCode] = B.[SecurityGroupCode]

										where
											EntityTypeCode = 'SPECIALTY' 
										and
											A.Userid = B.Userid
										)
								)						
							and
								exists
									(
									select
										1
									from
										[dbo].[SecurityGroupEntityTypeMap] B

									inner join
										[dbo].[WindowsUserSecurityGroupMap]
									on	B.[SecurityGroupCode] = [WindowsUserSecurityGroupMap].[SecurityGroupCode]

									where
										EntityTypeCode = 'MORTALITYCONSULTANT' 
									and
										A.SecurityGroupCode = B.SecurityGroupCode
									)
							)
and
	EntityTypeCode = 'MORTALITYCONSULTANT' 
	


/* directorate all for 1-1 consultant mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'DIRECTORATE'
	,'[Directorate].[Directorate].[All]'
	
from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'CONSULTANT'
and
	Member <> '[Consultant].[Source Consultant].[All]'



/* specialty all for 1-1 consultant mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'SPECIALTY'
	,'[Specialty].[National Specialty].[All]'
	
from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'CONSULTANT'
and
	Member <> '[Consultant].[Source Consultant].[All]'


/* specialty all for directorate mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'SPECIALTY'
	,'[Specialty].[National Specialty].[All]'
	
from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'DIRECTORATE'
and
	Member <> '[Directorate].[Directorate].[All]'


/* consultant all for directorate mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'CONSULTANT'
	,'[Consultant].[Source Consultant].[All]'
	
from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'DIRECTORATE'
and
	Member <> '[Directorate].[Directorate].[All]'


/* mortality consultant all for directorate mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'MORTALITYCONSULTANT'
	,'[Consultant].[Source Consultant].[All]'
from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'DIRECTORATE'
and
	Member <> '[Directorate].[Directorate].[All]'



/* directorate all for specialty mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'DIRECTORATE'
	,'[Directorate].[Directorate].[All]'
from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'SPECIALTY'
and
	Member <> '[Specialty].[National Specialty].[All]'


/* consultant all for specialty mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'CONSULTANT'
	,'[Consultant].[Source Consultant].[All]'

from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'SPECIALTY'
and
	Member <> '[Specialty].[National Specialty].[All]'

/* mortality consultant all for specialty mapping */

insert into dbo.SecurityGroupEntityTypeMap
(
	SecurityGroupCode
	,EntityTypeCode
	,Member
)

select
	SecurityGroupCode
	,'MORTALITYCONSULTANT'
	,'[Consultant].[Source Consultant].[All]'
	
from
	dbo.SecurityGroupEntityTypeMap Import
where
	EntityTypeCode = 'SPECIALTY'
and
	Member <> '[Specialty].[National Specialty].[All]'


/* All members from dbo.SecurityGroupEntityTypeMap where user has association with directorate group */

delete from dbo.SecurityGroupEntityTypeMap

where
	SecurityGroupCode in 
						(
							select
								SecurityGroupCode
							from
								[dbo].[WindowsUserSecurityGroupMap] A
							where
								(
								exists
										(
										select
											[Userid]
										from
											[dbo].[SecurityGroupEntityTypeMap]

										inner join
											[dbo].[WindowsUserSecurityGroupMap] B
										on	[SecurityGroupEntityTypeMap].[SecurityGroupCode] = B.[SecurityGroupCode]

										where
											EntityTypeCode = 'DIRECTORATE' 
										and
											Member <> '[Directorate].[Directorate].[All]'
										and
											A.Userid = B.Userid
										)
								)						
							and
								exists
									(
									select
										1
									from
										[dbo].[SecurityGroupEntityTypeMap] B

									inner join
										[dbo].[WindowsUserSecurityGroupMap]
									on	B.[SecurityGroupCode] = [WindowsUserSecurityGroupMap].[SecurityGroupCode]

									where
										EntityTypeCode = 'CONSULTANT' 
									and
										A.SecurityGroupCode = B.SecurityGroupCode
									)
							)
and
	EntityTypeCode = 'DIRECTORATE'
and
	Member = '[Directorate].[Directorate].[All]'
and
	left(SecurityGroupCode, 6) = 'CONS||'


/* All members from dbo.SecurityGroupEntityTypeMap where user has association with specialty group */

delete from dbo.SecurityGroupEntityTypeMap

where
	SecurityGroupCode in 
						(
							select
								SecurityGroupCode
							from
								[dbo].[WindowsUserSecurityGroupMap] A
							where
								(
								exists
										(
										select
											[Userid]
										from
											[dbo].[SecurityGroupEntityTypeMap]

										inner join
											[dbo].[WindowsUserSecurityGroupMap] B
										on	[SecurityGroupEntityTypeMap].[SecurityGroupCode] = B.[SecurityGroupCode]

										where
											EntityTypeCode = 'SPECIALTY' 
										and
											Member <> '[Specialty].[National Specialty].[All]'
										and
											A.Userid = B.Userid
										)
								)						
							and
								exists
									(
									select
										1
									from
										[dbo].[SecurityGroupEntityTypeMap] B

									inner join
										[dbo].[WindowsUserSecurityGroupMap]
									on	B.[SecurityGroupCode] = [WindowsUserSecurityGroupMap].[SecurityGroupCode]

									where
										EntityTypeCode = 'CONSULTANT' 
									and
										A.SecurityGroupCode = B.SecurityGroupCode
									)
							)
and
	EntityTypeCode = 'SPECIALTY'
and
	Member = '[Specialty].[National Specialty].[All]'
and
	left(SecurityGroupCode, 6) = 'CONS||'
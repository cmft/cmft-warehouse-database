﻿CREATE view [dbo].[WindowsUserCodeMap]

as

select
	[ContextCode]
	,[SourceValueCode]
	,[Userid]
	,Member = '[Consultant].[Source Consultant].&[' + cast(SourceConsultantID as varchar) + ']'
	,SecurityGroupCode = 'CONS||'+[ContextCode]+'||'+[SourceValueCode]

from
	(
		select
			[ContextCode]
			,[SourceValueCode]
			,[Userid]
			--,Member = '[Consultant].[Source Consultant].&[' + cast(SourceConsultantID as varchar) + ']'
			--,SecurityGroupCode = 'CONS||'+[ContextCode]+'||'+[SourceValueCode]
		from
			[dbo].[WindowsUserCodeMapBase] -- initial dump of logins to PAS codes

		union

		select
			[ContextCode] = [Context Code]
			,[SourceValueCode] = [Consultant Code Mapping]
			,[Userid] = [Login]
			--,Member = '[Consultant].[Source Consultant].&[' + cast(SourceConsultantID as varchar) + ']'
			--,SecurityGroupCode = 'CONS||'+[ContextCode]+'||'+[SourceValueCode]
		from
			[SHAREPOINT]...[Security Map] SecurityMap -- Sharepoint list now maintained by BAs

		where
			[Consultant Code Mapping] <> 'N/A' -- only get consultant mappings

	) ConsultantMap


inner join
		(
		select
			*
		from
			WarehouseOLAPMergedV2.[WH].[Consultant]
		) Consultant
on
	Consultant.SourceConsultantCode = ConsultantMap.SourceValueCode
and Consultant.SourceContextCode = ConsultantMap.ContextCode
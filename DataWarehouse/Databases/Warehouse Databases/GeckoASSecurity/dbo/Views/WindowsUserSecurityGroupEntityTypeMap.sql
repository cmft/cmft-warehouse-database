﻿CREATE view [dbo].[WindowsUserSecurityGroupEntityTypeMap]

as

select
	WindowsUserSecurityGroupMap.[Userid]
	,Username
    ,WindowsUserSecurityGroupMap.[SecurityGroupCode]
    ,EntityTypeCode
    ,Member
from
	 [GeckoASSecurity].dbo.WindowsUserSecurityGroupMap
	 
inner join dbo.WindowsUser
on	WindowsUserSecurityGroupMap.[Userid] = WindowsUser.[Userid]

inner join [GeckoASSecurity].[dbo].[SecurityGroupEntityTypeMap]
on WindowsUserSecurityGroupMap.SecurityGroupCode = [SecurityGroupEntityTypeMap].SecurityGroupCode
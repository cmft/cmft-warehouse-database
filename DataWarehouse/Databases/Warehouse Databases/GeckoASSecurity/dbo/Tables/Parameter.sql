﻿CREATE TABLE [dbo].[Parameter](
	[Parameter] [varchar](128) NOT NULL,
	[TextValue] [varchar](255) NULL,
	[NumericValue] [decimal](18, 0) NULL,
	[DateValue] [datetime] NULL,
 CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED 
(
	[Parameter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
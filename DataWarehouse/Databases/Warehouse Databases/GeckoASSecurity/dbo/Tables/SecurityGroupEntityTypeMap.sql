﻿CREATE TABLE [dbo].[SecurityGroupEntityTypeMap](
	[SecurityGroupCode] [varchar](100) NOT NULL,
	[EntityTypeCode] [varchar](50) NOT NULL,
	[Member] [varchar](512) NOT NULL,
 CONSTRAINT [PK_WindowsGroupEntity_1] PRIMARY KEY CLUSTERED 
(
	[SecurityGroupCode] ASC,
	[EntityTypeCode] ASC,
	[Member] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SecurityGroupEntityTypeMap]  WITH CHECK ADD  CONSTRAINT [FK_SecurityGroupEntityTypeMap_EntityType] FOREIGN KEY([EntityTypeCode])
REFERENCES [dbo].[EntityType] ([EntityTypeCode])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[SecurityGroupEntityTypeMap] CHECK CONSTRAINT [FK_SecurityGroupEntityTypeMap_EntityType]
GO
ALTER TABLE [dbo].[SecurityGroupEntityTypeMap]  WITH CHECK ADD  CONSTRAINT [FK_SecurityGroupEntityTypeMap_SecurityGroup] FOREIGN KEY([SecurityGroupCode])
REFERENCES [dbo].[SecurityGroup] ([SecurityGroupCode])
GO

ALTER TABLE [dbo].[SecurityGroupEntityTypeMap] CHECK CONSTRAINT [FK_SecurityGroupEntityTypeMap_SecurityGroup]
﻿CREATE TABLE [dbo].[WindowsUserCodeMapBase](
	[ContextCode] [varchar](10) NOT NULL,
	[SourceValueCode] [varchar](20) NOT NULL,
	[Userid] [varchar](50) NOT NULL,
 CONSTRAINT [PK_WindowsUserCodeMapBase] PRIMARY KEY CLUSTERED 
(
	[ContextCode] ASC,
	[SourceValueCode] ASC,
	[Userid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
﻿CREATE TABLE [dbo].[WindowsUserSecurityGroupMap](
	[Userid] [varchar](50) NOT NULL,
	[SecurityGroupCode] [varchar](100) NOT NULL,
 CONSTRAINT [PK_WindowsUserGroupMap] PRIMARY KEY CLUSTERED 
(
	[Userid] ASC,
	[SecurityGroupCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WindowsUserSecurityGroupMap]  WITH CHECK ADD  CONSTRAINT [FK_WindowsUserSecurityGroupMap_SecurityGroup] FOREIGN KEY([SecurityGroupCode])
REFERENCES [dbo].[SecurityGroup] ([SecurityGroupCode])
GO

ALTER TABLE [dbo].[WindowsUserSecurityGroupMap] CHECK CONSTRAINT [FK_WindowsUserSecurityGroupMap_SecurityGroup]
GO
ALTER TABLE [dbo].[WindowsUserSecurityGroupMap]  WITH NOCHECK ADD  CONSTRAINT [FK_WindowsUserSecurityGroupMap_WindowsUser] FOREIGN KEY([Userid])
REFERENCES [dbo].[WindowsUser] ([Userid])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[WindowsUserSecurityGroupMap] CHECK CONSTRAINT [FK_WindowsUserSecurityGroupMap_WindowsUser]
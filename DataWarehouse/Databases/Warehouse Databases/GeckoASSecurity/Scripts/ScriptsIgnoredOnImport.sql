﻿
USE [master]
GO

/****** Object:  Database [GeckoASSecurity]    Script Date: 24/08/2015 15:18:36 ******/
CREATE DATABASE [GeckoASSecurity] ON  PRIMARY 
( NAME = N'GeckoASSecurity', FILENAME = N'F:\Data\GeckoASSecurity.mdf' , SIZE = 16384KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GeckoASSecurity_log', FILENAME = N'G:\Log\GeckoASSecurity_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [GeckoASSecurity] SET COMPATIBILITY_LEVEL = 90
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GeckoASSecurity].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO

ALTER DATABASE [GeckoASSecurity] SET ANSI_NULL_DEFAULT OFF
GO

ALTER DATABASE [GeckoASSecurity] SET ANSI_NULLS OFF
GO

ALTER DATABASE [GeckoASSecurity] SET ANSI_PADDING OFF
GO

ALTER DATABASE [GeckoASSecurity] SET ANSI_WARNINGS OFF
GO

ALTER DATABASE [GeckoASSecurity] SET ARITHABORT OFF
GO

ALTER DATABASE [GeckoASSecurity] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [GeckoASSecurity] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [GeckoASSecurity] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [GeckoASSecurity] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [GeckoASSecurity] SET CURSOR_DEFAULT  GLOBAL
GO

ALTER DATABASE [GeckoASSecurity] SET CONCAT_NULL_YIELDS_NULL OFF
GO

ALTER DATABASE [GeckoASSecurity] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [GeckoASSecurity] SET QUOTED_IDENTIFIER OFF
GO

ALTER DATABASE [GeckoASSecurity] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [GeckoASSecurity] SET  DISABLE_BROKER
GO

ALTER DATABASE [GeckoASSecurity] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [GeckoASSecurity] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [GeckoASSecurity] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [GeckoASSecurity] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [GeckoASSecurity] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [GeckoASSecurity] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [GeckoASSecurity] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [GeckoASSecurity] SET RECOVERY SIMPLE
GO

ALTER DATABASE [GeckoASSecurity] SET  MULTI_USER
GO

ALTER DATABASE [GeckoASSecurity] SET PAGE_VERIFY CHECKSUM
GO

ALTER DATABASE [GeckoASSecurity] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'GeckoASSecurity', N'ON'
GO

USE [GeckoASSecurity]
GO

/****** Object:  Table [dbo].[EntityType]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Parameter]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SchemaChangeLog]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[SecurityGroup]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[SecurityGroupEntityTypeMap]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[WindowsUser]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[WindowsUserCodeMapBase]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[WindowsUserSecurityGroupMap]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING OFF
GO

/****** Object:  View [dbo].[WindowsUserCodeMap]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [dbo].[WindowsUserSecurityGroupEntityTypeMap]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[BuildSecurityModel]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LoadConsultantWarehouseEntityXRef]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LoadDefaultMembers]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LoadSecurityGroup]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LoadSecurityGroupEntityTypeMap]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LoadSecurityModel]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LoadWindowsUser]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[LoadWindowsUserSecurityGroupMap]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[SecurityFilterMDX]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  DdlTrigger [tr_DDL_SchemaChangeLog]    Script Date: 24/08/2015 15:18:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [GeckoASSecurity] SET  READ_WRITE
GO

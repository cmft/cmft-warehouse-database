﻿CREATE VIEW [Match].[ByNHSNumber]
as

select
	WrkTargetDataset.MatchTypeID
	,TemplateID = MatchTypeTemplateRuleBase.TemplateID
	,SourceDatasetRecno = WrkSourceDataset.DatasetRecno
	,TargetDatasetRecno = WrkTargetDataset.DatasetRecno
	,Priority = MatchTypeTemplateRuleBase.Priority
from
	Match.WrkSourceDataset

inner join Match.WrkTargetDataset
on	WrkTargetDataset.NHSNumber = WrkSourceDataset.NHSNumber

inner join Match.MatchTypeTemplateRuleBase
on	MatchTypeTemplateRuleBase.MatchTypeID = WrkTargetDataset.MatchTypeID
and	MatchTypeTemplateRuleBase.SourceDatasetCode = WrkSourceDataset.DatasetCode

where
	WrkTargetDataset.StartTime between
	dateadd(
		hour
		,isnull(
			MatchTypeTemplateRuleBase.BeforeLimitHours
			,0
		) * -1
		,WrkSourceDataset.StartTime
	)
and
	dateadd(
		hour
		,isnull(
			MatchTypeTemplateRuleBase.AfterLimitHours
			,0
		)
		,WrkSourceDataset.EndTime
	)
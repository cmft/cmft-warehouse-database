﻿CREATE VIEW [Match].[ByPatientNumberValidDate]
as

select
	WrkTargetDataset.MatchTypeID
	,TemplateID = MatchTypeTemplateRuleBase.TemplateID
	,SourceDatasetRecno = WrkSourceDataset.DatasetRecno
	,TargetDatasetRecno = WrkTargetDataset.DatasetRecno
	,Priority = MatchTypeTemplateRuleBase.Priority

from
	Match.WrkSourceDataset

inner join Match.WrkTargetDataset
on	WrkTargetDataset.PatientNumber = WrkSourceDataset.PatientNumber
and WrkSourceDataset.StartTime <= coalesce (WrkTargetDataset.EndTime,getdate())
and
	(
		WrkSourceDataset.EndTime is null
	or
		WrkSourceDataset.EndTime >= WrkTargetDataset.StartTime
	)

inner join Match.MatchTypeTemplateRuleBase
on	MatchTypeTemplateRuleBase.MatchTypeID = WrkTargetDataset.MatchTypeID
and	MatchTypeTemplateRuleBase.SourceDatasetCode = WrkSourceDataset.DatasetCode
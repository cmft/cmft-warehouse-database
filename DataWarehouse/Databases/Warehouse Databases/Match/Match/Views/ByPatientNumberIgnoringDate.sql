﻿CREATE VIEW [Match].[ByPatientNumberIgnoringDate]
as

select
	WrkTargetDataset.MatchTypeID
	,TemplateID = MatchTypeTemplateRuleBase.TemplateID
	,SourceDatasetRecno = WrkSourceDataset.DatasetRecno
	,TargetDatasetRecno = WrkTargetDataset.DatasetRecno
	,Priority = MatchTypeTemplateRuleBase.Priority
from
	Match.WrkSourceDataset

inner join Match.WrkTargetDataset
on	WrkTargetDataset.PatientNumber = WrkSourceDataset.PatientNumber

inner join Match.MatchTypeTemplateRuleBase
on	MatchTypeTemplateRuleBase.MatchTypeID = WrkTargetDataset.MatchTypeID
and	MatchTypeTemplateRuleBase.SourceDatasetCode = WrkSourceDataset.DatasetCode
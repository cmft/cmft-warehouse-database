﻿CREATE view [Match].[MatchTypeTemplateRuleBase]
as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

select
	MatchTypeTemplate.MatchTypeID
	,Template.Priority
	,Template.TemplateID
	,MatchType.SourceDatasetCode

	,RuleBase.RuleBaseRecno
	,RuleBase.AfterLimitHours
	,RuleBase.BeforeLimitHours
	,RuleBase.BooleanValue
	,RuleBase.RuleListTypeCode
from   
	Match.Template

inner join Match.MatchTypeTemplate
on	MatchTypeTemplate.TemplateID =  Template.TemplateID
and	MatchTypeTemplate.Active = 1

inner join Match.MatchType
on	MatchType.MatchTypeID = MatchTypeTemplate.MatchTypeID

left join Match.RuleBase
on	RuleBase.MatchTypeID = MatchTypeTemplate.MatchTypeID
and	RuleBase.TemplateID = MatchTypeTemplate.TemplateID
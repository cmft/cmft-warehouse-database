﻿CREATE TABLE [Match].[WrkTargetDataset](
	[MatchTypeID] [int] NOT NULL,
	[DatasetRecno] [int] NOT NULL,
	[ContextCode] [varchar](20) NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[DateOfBirth] [date] NULL,
	[Forename] [varchar](255) NULL,
	[NHSNumber] [varchar](17) NULL,
	[PatientID] [int] NULL,
	[PatientNumber] [varchar](20) NULL,
	[Postcode] [varchar](10) NULL,
	[Surname] [varchar](255) NULL,
	[TreatmentFunctionCode] [varchar](10) NULL,
 CONSTRAINT [PK_WrkTargetDataset] PRIMARY KEY CLUSTERED 
(
	[MatchTypeID] ASC,
	[DatasetRecno] ASC,
	[ContextCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
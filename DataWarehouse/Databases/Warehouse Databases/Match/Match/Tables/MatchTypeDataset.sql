﻿CREATE TABLE [Match].[MatchTypeDataset](
	[MatchTypeID] [int] NOT NULL,
	[SourceDatasetRecno] [int] NOT NULL,
	[TargetDatasetRecno] [int] NOT NULL,
	[TemplateID] [int] NOT NULL,
 CONSTRAINT [PK_DatasetMatch] PRIMARY KEY CLUSTERED 
(
	[MatchTypeID] ASC,
	[SourceDatasetRecno] ASC,
	[TargetDatasetRecno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Match].[MatchTypeDataset]  WITH CHECK ADD  CONSTRAINT [FK_MatchTypeDataset_MatchType] FOREIGN KEY([MatchTypeID])
REFERENCES [Match].[MatchType] ([MatchTypeID])
GO

ALTER TABLE [Match].[MatchTypeDataset] CHECK CONSTRAINT [FK_MatchTypeDataset_MatchType]
GO
ALTER TABLE [Match].[MatchTypeDataset]  WITH CHECK ADD  CONSTRAINT [FK_MatchTypeDataset_Template] FOREIGN KEY([TemplateID])
REFERENCES [Match].[Template] ([TemplateID])
GO

ALTER TABLE [Match].[MatchTypeDataset] CHECK CONSTRAINT [FK_MatchTypeDataset_Template]
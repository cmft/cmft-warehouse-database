﻿CREATE TABLE [Match].[TemplateType](
	[TemplateTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateType] [varchar](255) NOT NULL,
	[TemplateTypeCode] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TemplateType_1] PRIMARY KEY CLUSTERED 
(
	[TemplateTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Match].[TemplateType]  WITH CHECK ADD  CONSTRAINT [FK_TemplateType_TemplateType] FOREIGN KEY([TemplateTypeID])
REFERENCES [Match].[TemplateType] ([TemplateTypeID])
GO

ALTER TABLE [Match].[TemplateType] CHECK CONSTRAINT [FK_TemplateType_TemplateType]
GO
/****** Object:  Index [IX_TemplateType_TemplateTypeCode]    Script Date: 25/08/2015 11:52:40 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TemplateType_TemplateTypeCode] ON [Match].[TemplateType]
(
	[TemplateTypeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
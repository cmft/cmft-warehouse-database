﻿CREATE TABLE [Match].[RuleBase](
	[RuleBaseRecno] [int] IDENTITY(1,1) NOT NULL,
	[MatchTypeID] [int] NOT NULL,
	[TemplateID] [int] NOT NULL,
	[AfterLimitHours] [int] NULL,
	[BeforeLimitHours] [int] NULL,
	[BooleanValue] [bit] NULL,
	[RuleListTypeCode] [varchar](50) NULL,
 CONSTRAINT [PK_RuleBase] PRIMARY KEY CLUSTERED 
(
	[RuleBaseRecno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Match].[RuleBase]  WITH CHECK ADD  CONSTRAINT [FK_RuleBase_MatchType] FOREIGN KEY([MatchTypeID])
REFERENCES [Match].[MatchType] ([MatchTypeID])
GO

ALTER TABLE [Match].[RuleBase] CHECK CONSTRAINT [FK_RuleBase_MatchType]
GO
ALTER TABLE [Match].[RuleBase]  WITH CHECK ADD  CONSTRAINT [FK_RuleBase_Template] FOREIGN KEY([TemplateID])
REFERENCES [Match].[Template] ([TemplateID])
GO

ALTER TABLE [Match].[RuleBase] CHECK CONSTRAINT [FK_RuleBase_Template]
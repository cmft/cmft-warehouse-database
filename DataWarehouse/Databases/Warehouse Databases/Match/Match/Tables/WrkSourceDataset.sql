﻿CREATE TABLE [Match].[WrkSourceDataset](
	[DatasetCode] [varchar](10) NOT NULL,
	[DatasetRecno] [int] NOT NULL,
	[ContextCode] [varchar](20) NOT NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[DateOfBirth] [date] NULL,
	[Forename] [varchar](255) NULL,
	[NHSNumber] [varchar](17) NULL,
	[PatientID] [int] NULL,
	[PatientNumber] [varchar](20) NULL,
	[Postcode] [varchar](10) NULL,
	[Surname] [varchar](255) NULL,
	[TreatmentFunctionCode] [varchar](10) NULL,
 CONSTRAINT [PK_WrkSourceDataset] PRIMARY KEY CLUSTERED 
(
	[DatasetCode] ASC,
	[DatasetRecno] ASC,
	[ContextCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_Match_WrkSourceDataset_01]    Script Date: 25/08/2015 11:52:40 ******/
CREATE NONCLUSTERED INDEX [IX_Match_WrkSourceDataset_01] ON [Match].[WrkSourceDataset]
(
	[NHSNumber] ASC
)
INCLUDE ( 	[DatasetCode],
	[DatasetRecno],
	[StartTime],
	[EndTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
﻿CREATE TABLE [Match].[MatchTypeTemplate](
	[MatchTypeID] [int] NOT NULL,
	[TemplateID] [int] NOT NULL,
	[Active] [bit] NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[MatchTypeID] ASC,
	[TemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Match].[MatchTypeTemplate]  WITH CHECK ADD  CONSTRAINT [FK_MatchTypeTemplate_MatchType] FOREIGN KEY([MatchTypeID])
REFERENCES [Match].[MatchType] ([MatchTypeID])
GO

ALTER TABLE [Match].[MatchTypeTemplate] CHECK CONSTRAINT [FK_MatchTypeTemplate_MatchType]
GO
ALTER TABLE [Match].[MatchTypeTemplate]  WITH CHECK ADD  CONSTRAINT [FK_MatchTypeTemplate_Template] FOREIGN KEY([TemplateID])
REFERENCES [Match].[Template] ([TemplateID])
GO

ALTER TABLE [Match].[MatchTypeTemplate] CHECK CONSTRAINT [FK_MatchTypeTemplate_Template]
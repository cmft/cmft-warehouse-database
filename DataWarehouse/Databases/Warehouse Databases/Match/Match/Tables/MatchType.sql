﻿CREATE TABLE [Match].[MatchType](
	[MatchTypeID] [int] IDENTITY(1,1) NOT NULL,
	[MatchType] [varchar](255) NOT NULL,
	[SourceDatasetCode] [varchar](10) NOT NULL,
	[TargetDatasetCode] [varchar](10) NOT NULL,
	[Active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MatchTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Match].[MatchType]  WITH CHECK ADD  CONSTRAINT [FK_Match_MatchType_Match_Dataset_Source] FOREIGN KEY([SourceDatasetCode])
REFERENCES [Match].[Dataset] ([DatasetCode])
GO

ALTER TABLE [Match].[MatchType] CHECK CONSTRAINT [FK_Match_MatchType_Match_Dataset_Source]
GO
ALTER TABLE [Match].[MatchType]  WITH CHECK ADD  CONSTRAINT [FK_Match_MatchType_Match_Dataset_Target] FOREIGN KEY([TargetDatasetCode])
REFERENCES [Match].[Dataset] ([DatasetCode])
GO

ALTER TABLE [Match].[MatchType] CHECK CONSTRAINT [FK_Match_MatchType_Match_Dataset_Target]
GO
/****** Object:  Index [IX_MatchType_01]    Script Date: 25/08/2015 11:52:40 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_MatchType_01] ON [Match].[MatchType]
(
	[SourceDatasetCode] ASC,
	[TargetDatasetCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
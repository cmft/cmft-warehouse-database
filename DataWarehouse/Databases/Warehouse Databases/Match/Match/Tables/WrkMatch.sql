﻿CREATE TABLE [Match].[WrkMatch](
	[MatchRecno] [int] IDENTITY(1,1) NOT NULL,
	[TemplateID] [int] NOT NULL,
	[SourceDatasetRecno] [int] NOT NULL,
	[TargetDatasetRecno] [int] NOT NULL,
	[Priority] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Index [IX_Match_WrkMatch_01]    Script Date: 25/08/2015 11:52:40 ******/
CREATE NONCLUSTERED INDEX [IX_Match_WrkMatch_01] ON [Match].[WrkMatch]
(
	[TemplateID] ASC,
	[SourceDatasetRecno] ASC
)
INCLUDE ( 	[MatchRecno],
	[Priority]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
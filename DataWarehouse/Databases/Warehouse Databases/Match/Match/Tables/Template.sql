﻿CREATE TABLE [Match].[Template](
	[TemplateID] [int] IDENTITY(1,1) NOT NULL,
	[Template] [varchar](128) NOT NULL,
	[Remark] [varchar](255) NULL,
	[TemplateTypeID] [int] NOT NULL,
	[Priority] [int] NOT NULL,
 CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED 
(
	[TemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Match].[Template]  WITH CHECK ADD  CONSTRAINT [FK_Template_TemplateType] FOREIGN KEY([TemplateTypeID])
REFERENCES [Match].[TemplateType] ([TemplateTypeID])
GO

ALTER TABLE [Match].[Template] CHECK CONSTRAINT [FK_Template_TemplateType]
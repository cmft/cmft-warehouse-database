﻿CREATE TABLE [Match].[RuleList](
	[RuleListTypeCode] [varchar](50) NOT NULL,
	[RuleListValue] [varchar](50) NOT NULL,
	[ContextCode] [varchar](10) NULL,
 CONSTRAINT [PK_RuleList] PRIMARY KEY CLUSTERED 
(
	[RuleListTypeCode] ASC,
	[RuleListValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
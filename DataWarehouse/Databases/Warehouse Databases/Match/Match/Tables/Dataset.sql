﻿CREATE TABLE [Match].[Dataset](
	[DatasetCode] [varchar](10) NOT NULL,
	[Dataset] [varchar](255) NOT NULL,
	[TemplateID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DatasetCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
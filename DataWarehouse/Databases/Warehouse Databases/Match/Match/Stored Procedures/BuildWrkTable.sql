﻿CREATE PROCEDURE [Match].[BuildWrkTable] @SourceDatasetCode varchar(10) as

--------------------------------------------------------------------------
-- Copyright Gecko Technologies Ltd. 2003    --
--------------------------------------------------------------------------

declare
	@StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@Priority int

declare @sql varchar(max)

declare
	 @Template varchar(max)
	,@IsSource bit
	,@MatchTypeID int

declare TemplateCursor cursor fast_forward for

	select top 1
		Template.Template + ' @IsSource = 1 ,@MatchTypeID = ' + cast(MatchType.MatchTypeID as varchar) 
		,IsSource = cast(1 as bit)
		,MatchType.MatchTypeID
	from
		Match.Dataset

	inner join Match.Template
	on	Template.TemplateID = Dataset.TemplateID

	inner join Match.MatchType
	on	MatchType.SourceDatasetCode = Dataset.DatasetCode

	where
		MatchType.Active = 1
	and	MatchType.SourceDatasetCode = @SourceDatasetCode

	union

	select
		Template.Template + ' @IsSource = 0 ,@MatchTypeID = ' + cast(MatchType.MatchTypeID as varchar) 
		,IsSource = cast(0 as bit)
		,MatchType.MatchTypeID
	from
		Match.Dataset

	inner join Match.MatchType
	on	MatchType.SourceDatasetCode = Dataset.DatasetCode

	inner join Match.Dataset TargetDataset
	on	TargetDataset.DatasetCode = MatchType.TargetDatasetCode

	inner join Match.Template
	on	Template.TemplateID = TargetDataset.TemplateID

	where
		MatchType.Active = 1
	and	MatchType.SourceDatasetCode = @SourceDatasetCode
	

delete
from
	Match.WrkSourceDataset
where
	WrkSourceDataset.DatasetCode = @SourceDatasetCode


OPEN TemplateCursor

FETCH NEXT FROM TemplateCursor

INTO
	@Template
	,@IsSource
	,@MatchTypeID
	
WHILE @@FETCH_STATUS = 0

BEGIN

	if @IsSource = 0
		delete
		from
			Match.WrkTargetDataset
		where
			WrkTargetDataset.MatchTypeID = @MatchTypeID

	print convert(varchar, getdate(), 113) + ' - ' + @Template

	execute(@Template)

	FETCH NEXT FROM TemplateCursor
	INTO
		@Template
		,@IsSource
		,@MatchTypeID

END
	  
CLOSE TemplateCursor
DEALLOCATE TemplateCursor
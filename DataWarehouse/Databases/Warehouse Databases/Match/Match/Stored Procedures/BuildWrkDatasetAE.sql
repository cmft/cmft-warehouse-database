﻿CREATE PROCEDURE [Match].[BuildWrkDatasetAE]
	@IsSource bit
	,@MatchTypeID int = null
as

if (@IsSource = 1)
begin

	declare @SourceDatasetCode varchar(10) =
		(
		select
			MatchType.SourceDatasetCode
		from
			Match.MatchType
		where
			MatchType.MatchTypeID = @MatchTypeID
		)


	insert
	into
		Match.WrkSourceDataset
		(
		DatasetCode
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,Forename
		,Surname
		,PatientNumber
		)
	select
		DatasetCode = @SourceDatasetCode
		,DatasetRecno = Encounter.MergeRecno
		,ContextCode = Encounter.ContextCode
		,DateOfBirth = cast(Encounter.DateOfBirth as date)
		,StartTime = Encounter.ArrivalTime
		,EndTime = Encounter.LastDepartureTime
		,NHSNumber = replace(Encounter.NHSNumber, ' ', '')
		,Forename = Encounter.PatientForename
		,Surname = Encounter.PatientSurname
		,PatientNumber = Encounter.DistrictNo
	from
		WarehouseOLAP.AE.Encounter


end

else

	insert
	into
		Match.WrkTargetDataset
		(
		MatchTypeID
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,Forename
		,Surname
		,PatientNumber
		)
	select
		MatchTypeID = @MatchTypeID
		,DatasetRecno = Encounter.MergeRecno
		,ContextCode = Encounter.ContextCode
		,DateOfBirth = cast(Encounter.DateOfBirth as date)
		,StartTime = Encounter.ArrivalTime
		,EndTime = Encounter.LastDepartureTime
		,NHSNumber = replace(Encounter.NHSNumber, ' ', '')
		,Forename = Encounter.PatientForename
		,Surname = Encounter.PatientSurname
		,PatientNumber = Encounter.DistrictNo
	from
		WarehouseOLAP.AE.Encounter
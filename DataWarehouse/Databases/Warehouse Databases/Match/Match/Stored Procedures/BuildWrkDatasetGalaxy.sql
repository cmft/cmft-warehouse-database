﻿CREATE PROCEDURE [Match].[BuildWrkDatasetGalaxy]
	@IsSource bit
	,@MatchTypeID int = null
as

if (@IsSource = 1)
begin

	declare @SourceDatasetCode varchar(10) =
		(
		select
			MatchType.SourceDatasetCode
		from
			Match.MatchType
		where
			MatchType.MatchTypeID = @MatchTypeID
		)


	insert
	into
		Match.WrkSourceDataset
		(
		DatasetCode
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,Forename
		,Surname
		,PatientNumber
		)
	select
		DatasetCode = @SourceDatasetCode
		,DatasetRecno = PatientSession.EncounterRecno
		,ContextCode = 'N/A'
		,DateOfBirth = cast(Patient.DOB as date)
		,StartTime = PatientSession.SessionDate
		,EndTime = PatientSession.SessionDate
		,NHSNumber = replace(Patient.PAT_NHS_NO, ' ', '')
		,Forename = Patient.FORENAMES
		,Surname = Patient.SURNAME
		,PatientNumber = PatientAlsoKnownAs.LinkToPatientMasterSystem
	from
		Warehouse.Theatre.PatientSession

	inner join Warehouse.Theatre.PatientAlsoKnownAs
	on	PatientAlsoKnownAs.PatientAlsoKnownAsID = PatientSession.PatientNumber

	inner join Galaxy.dbo.pat_mast Patient
	on	Patient.PAT_SYS = PatientSession.PatientNumber


end

else

	insert
	into
		Match.WrkTargetDataset
		(
		MatchTypeID
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,Forename
		,Surname
		,PatientNumber
		)
	select
		MatchTypeID = @MatchTypeID
		,DatasetRecno = PatientSession.EncounterRecno
		,ContextCode = 'N/A'
		,DateOfBirth = cast(Patient.DOB as date)
		,StartTime = PatientSession.SessionDate
		,EndTime = PatientSession.SessionDate
		,NHSNumber = replace(Patient.PAT_NHS_NO, ' ', '')
		,Forename = Patient.FORENAMES
		,Surname = Patient.SURNAME
		,PatientNumber = PatientAlsoKnownAs.LinkToPatientMasterSystem
	from
		Warehouse.Theatre.PatientSession

	inner join Warehouse.Theatre.PatientAlsoKnownAs
	on	PatientAlsoKnownAs.PatientAlsoKnownAsID = PatientSession.PatientNumber

	inner join Galaxy.dbo.pat_mast Patient
	on	Patient.PAT_SYS = PatientSession.PatientNumber
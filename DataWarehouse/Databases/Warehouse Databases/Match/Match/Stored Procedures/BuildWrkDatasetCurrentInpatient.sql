﻿CREATE PROCEDURE [Match].[BuildWrkDatasetCurrentInpatient]
	@IsSource bit
	,@MatchTypeID int = null
as

if (@IsSource = 1)
begin

	declare @SourceDatasetCode varchar(10) =
		(
		select
			MatchType.SourceDatasetCode
		from
			Match.MatchType
		where
			MatchType.MatchTypeID = @MatchTypeID
		)


	insert
	into
		Match.WrkSourceDataset
		(
		DatasetCode
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,PatientNumber
		,Forename
		,Surname
		,Postcode
		,TreatmentFunctionCode
		)
	select
		DatasetCode = @SourceDatasetCode
		,DatasetRecno = Encounter.EncounterRecno
		,ContextCode = 'CEN||PAS'
		,DateOfBirth = Encounter.DateOfBirth
		,StartTime = Encounter.EpisodeStartTime
		,EndTime = Encounter.EpisodeEndTime
		,NHSNumber = replace(Encounter.NHSNumber, ' ', '')
		,PatientNumber = Encounter.SourcePatientNo
		,Forename = Encounter.PatientForename
		,Surname = Encounter.PatientSurname
		,Postcode = Encounter.Postcode
		,TreatmentFunctionCode = TreatmentFunction.NationalSpecialtyCode
	from
		Warehouse.APCUpdate.Encounter Encounter

	inner join Warehouse.PAS.Specialty TreatmentFunction
	on Encounter.SpecialtyCode = TreatmentFunction.SpecialtyCode
	
end

else

	insert
	into
		Match.WrkTargetDataset
		(
		MatchTypeID
		,DatasetRecno
		,ContextCode
		,DateOfBirth
		,StartTime
		,EndTime
		,NHSNumber
		,PatientNumber
		,Forename
		,Surname
		,Postcode
		,TreatmentFunctionCode
		)
	select
		MatchTypeID = @MatchTypeID
		,DatasetRecno = Encounter.EncounterRecno
		,ContextCode = 'CEN||PAS'
		,DateOfBirth = Encounter.DateOfBirth
		,StartTime = Encounter.EpisodeStartTime
		,EndTime = Encounter.EpisodeEndTime
		,NHSNumber = replace(Encounter.NHSNumber, ' ', '')
		,PatientNumber = Encounter.SourcePatientNo
		,Forename = Encounter.PatientForename
		,Surname = Encounter.PatientSurname
		,Postcode = Encounter.Postcode
		,TreatmentFunctionCode = TreatmentFunction.NationalSpecialtyCode
	from
		Warehouse.APCUpdate.Encounter Encounter

	inner join Warehouse.PAS.Specialty TreatmentFunction
	on Encounter.SpecialtyCode = TreatmentFunction.SpecialtyCode
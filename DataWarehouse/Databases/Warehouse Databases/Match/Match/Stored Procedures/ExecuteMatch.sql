﻿CREATE PROCEDURE [Match].[ExecuteMatch]
	@MatchTypeID int
as

--prevent parallel running - isolate per dataset/match type
--this allows the truncation of the WrkMatch table and subsequent reset of identity column
set transaction isolation level serializable

begin transaction

	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)


	declare
		@Template varchar(128)
		,@TemplateID int
		,@Priority int

	declare @WrkSQL varchar(max)
	declare @Processed int
	declare @Matched int 
	declare @Failed int


	truncate table Match.WrkMatch


	declare TemplateCursor cursor fast_forward for

	select distinct
		 Template.Template
		,Template.TemplateID
		,Template.Priority
	from
		Match.MatchTypeTemplate

	inner join Match.Template
	on	Template.TemplateID = MatchTypeTemplate.TemplateID

	inner join Match.TemplateType
	on	TemplateType.TemplateTypeID = Template.TemplateTypeID

	where  	
		MatchTypeTemplate.MatchTypeID = @MatchTypeID
	and	TemplateType.TemplateTypeCode = 'MATCH'
	and MatchTypeTemplate.Active = 1 

	order by
		Template.Priority

	OPEN TemplateCursor

	FETCH NEXT FROM TemplateCursor

	INTO
		 @Template
		,@TemplateID
		,@Priority

	WHILE @@FETCH_STATUS = 0

	BEGIN

		select
			@WrkSQL =
				'Insert into Match.WrkMatch (TemplateID, SourceDatasetRecno, TargetDatasetRecno, Priority) 
				select TemplateID, SourceDatasetRecno, TargetDatasetRecno, Priority from '
				+ @Template + ' where MatchTypeID = ' + cast(@MatchTypeID as varchar) + ' and TemplateID = ' + cast(@TemplateID as varchar)
		
		print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL

		execute(@WrkSQL)

		FETCH NEXT FROM TemplateCursor
		INTO
			@Template
			,@TemplateID
			,@Priority


	END
	  
	CLOSE TemplateCursor
	DEALLOCATE TemplateCursor


	delete from Match.MatchTypeDataset
	where 
		MatchTypeID = @MatchTypeID


	insert into Match.MatchTypeDataset
		(
		MatchTypeID
		,SourceDatasetRecno
		,TargetDatasetRecno
		,TemplateID
		)
	select
 		@MatchTypeID
		,WrkMatch.SourceDatasetRecno
 		,WrkMatch.TargetDatasetRecno
		,WrkMatch.TemplateID
	from 
		Match.WrkMatch
	where
		not exists
		(
		select
			1
		from
			Match.WrkMatch Previous
		where
			Previous.SourceDatasetRecno = WrkMatch.SourceDatasetRecno
		and	Previous.TargetDatasetRecno = WrkMatch.TargetDatasetRecno
		and	(
				Previous.Priority < WrkMatch.Priority
			or
				(
					Previous.Priority = WrkMatch.Priority 
				and	Previous.TemplateID < WrkMatch.TemplateID
				)
			or
				(
					Previous.Priority = WrkMatch.Priority 
				and	Previous.TemplateID = WrkMatch.TemplateID
				and	Previous.MatchRecno < WrkMatch.MatchRecno
				)
			)
		)


	select
		@Processed = count(*)
	from
		Match.WrkSourceDataset

	select 
		@Matched = count(*) 
	from 
		Match.WrkTargetDataset
	where
		WrkTargetDataset.MatchTypeID = @MatchTypeID
	and	exists 
		(
		select
			1
		from 
			Match.MatchTypeDataset
		where
    		MatchTypeDataset.TargetDatasetRecno = WrkTargetDataset.DatasetRecno
		and	MatchTypeDataset.MatchTypeID = WrkTargetDataset.MatchTypeID
		)


	select @Failed = @Processed - @Matched

	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select
		@Stats =
			'Match Type: ' +
			(
			select
				MatchType.MatchType
			from
				Match.MatchType
			where
				MatchType.MatchTypeID = @MatchTypeID
			)
			 +
			', Processed ' + CONVERT(varchar(10), @Processed) +
			', Matched ' + CONVERT(varchar(10), @Matched) +
			', Failed ' + CONVERT(varchar(10), @Failed) +
			', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


	print @Stats

--exec Warehouse.dbo.WriteAuditLogEvent
--	 @ProcedureName
--	,@Stats

commit
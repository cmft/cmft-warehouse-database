﻿CREATE PROCEDURE [Match].[ProcessDataset] @SourceDatasetCode varchar(10) as

--fill the work tables
exec Match.BuildWrkTable @SourceDatasetCode

--execute matching
declare @MatchTypeID int

declare MatchTypeCursor cursor fast_forward for

	select
		MatchType.MatchTypeID
	from
		Match.MatchType
	where
		MatchType.Active = 1
	and	MatchType.SourceDatasetCode = @SourceDatasetCode
	
OPEN MatchTypeCursor

FETCH NEXT FROM MatchTypeCursor

INTO
	@MatchTypeID
	
WHILE @@FETCH_STATUS = 0

BEGIN

	execute Match.ExecuteMatch @MatchTypeID

	FETCH NEXT FROM MatchTypeCursor
	INTO
		@MatchTypeID

END
	  
CLOSE MatchTypeCursor
DEALLOCATE MatchTypeCursor
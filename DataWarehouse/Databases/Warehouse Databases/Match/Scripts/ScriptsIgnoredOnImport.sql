﻿
USE [master]
GO

/****** Object:  Database [Match]    Script Date: 25/08/2015 11:52:40 ******/
CREATE DATABASE [Match] ON  PRIMARY 
( NAME = N'Match', FILENAME = N'F:\Data\Match_Primary.mdf' , SIZE = 5376KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Match_log', FILENAME = N'G:\Log\Match_Primary.ldf' , SIZE = 6912KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [Match] SET COMPATIBILITY_LEVEL = 100
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Match].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [Match] SET ANSI_NULL_DEFAULT ON
GO

ALTER DATABASE [Match] SET ANSI_NULLS ON
GO

ALTER DATABASE [Match] SET ANSI_PADDING ON
GO

ALTER DATABASE [Match] SET ANSI_WARNINGS ON
GO

ALTER DATABASE [Match] SET ARITHABORT ON
GO

ALTER DATABASE [Match] SET AUTO_CLOSE OFF
GO

ALTER DATABASE [Match] SET AUTO_SHRINK OFF
GO

ALTER DATABASE [Match] SET AUTO_UPDATE_STATISTICS ON
GO

ALTER DATABASE [Match] SET CURSOR_CLOSE_ON_COMMIT OFF
GO

ALTER DATABASE [Match] SET CURSOR_DEFAULT  LOCAL
GO

ALTER DATABASE [Match] SET CONCAT_NULL_YIELDS_NULL ON
GO

ALTER DATABASE [Match] SET NUMERIC_ROUNDABORT OFF
GO

ALTER DATABASE [Match] SET QUOTED_IDENTIFIER ON
GO

ALTER DATABASE [Match] SET RECURSIVE_TRIGGERS OFF
GO

ALTER DATABASE [Match] SET  DISABLE_BROKER
GO

ALTER DATABASE [Match] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO

ALTER DATABASE [Match] SET DATE_CORRELATION_OPTIMIZATION OFF
GO

ALTER DATABASE [Match] SET TRUSTWORTHY OFF
GO

ALTER DATABASE [Match] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE [Match] SET PARAMETERIZATION SIMPLE
GO

ALTER DATABASE [Match] SET READ_COMMITTED_SNAPSHOT OFF
GO

ALTER DATABASE [Match] SET HONOR_BROKER_PRIORITY OFF
GO

ALTER DATABASE [Match] SET RECOVERY SIMPLE
GO

ALTER DATABASE [Match] SET  MULTI_USER
GO

ALTER DATABASE [Match] SET PAGE_VERIFY NONE
GO

ALTER DATABASE [Match] SET DB_CHAINING OFF
GO

EXEC sys.sp_db_vardecimal_storage_format N'Match', N'ON'
GO

USE [Match]
GO

/****** Object:  Table [Match].[Dataset]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Match].[MatchType]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Match].[MatchTypeDataset]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Match].[MatchTypeTemplate]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Match].[RuleBase]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Match].[RuleList]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Match].[Template]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Match].[TemplateType]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Match].[WrkMatch]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [Match].[WrkSourceDataset]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Match].[WrkTargetDataset]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  View [Match].[MatchTypeTemplateRuleBase]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Match].[ByPatientNumberTreatmentFunction]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Match].[ByPatientNumberIgnoringDate]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Match].[ByPatientNumber]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Match].[ByNHSNumber]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Match].[ByNHSNumberDOB]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Match].[ByPatientNumberDOB]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  View [Match].[ByPatientNumberValidDate]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetAE]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetAPC]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetCardiobase]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetCurrentInpatient]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetGalaxy]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetOP]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetOpenWardStay]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkDatasetPatientSourceSystemFlag]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[BuildWrkTable]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[ExecuteMatch]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [Match].[ProcessDataset]    Script Date: 25/08/2015 11:52:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

USE [master]
GO

ALTER DATABASE [Match] SET  READ_WRITE
GO

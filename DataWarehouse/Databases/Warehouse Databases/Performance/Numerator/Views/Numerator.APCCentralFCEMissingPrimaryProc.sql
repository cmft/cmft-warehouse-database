﻿CREATE view [Numerator].[APCCentralFCEMissingPrimaryProc] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.APCCentralFCEMissingPrimaryProc'

where
	WrkDatasetAPC.ClinicalCodingCompleteDate is not null
and WrkDatasetAPC.PrimaryProcedureCode is null
and WrkDatasetAPC.TheatrePrimaryProcedureExists = 1
and datediff(day,WrkDatasetAPC.AdmissionDate,WrkDatasetAPC.DischargeDate) >= 2
and WrkDatasetAPC.NationalSpecialtyCode in ('100','101','102','104','107','110','120','140','142','144','150','160','170','171','172','173','211','214','215','218','219')
and WrkDatasetAPC.ContextCode = 'CEN||PAS' -- Central only: Trafford theatre data not in Warehouse

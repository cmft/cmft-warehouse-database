﻿




CREATE view [Numerator].[ComplaintTurnaround] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetCOMP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetCOMP.SiteID		
	,DirectorateID = WrkDatasetCOMP.DirectorateID
	,SpecialtyID = WrkDatasetCOMP.SpecialtyID
	,ClinicianID = WrkDatasetCOMP.ClinicianID
	,DateID = WrkDatasetCOMP.ReceiptDateID
	,WrkDatasetCOMP.ServicePointID
	,Value = 
			(
			select 
				WorkingDays = sum(cast(WorkingDay as int))
			from 
				WarehouseOLAPMergedV2.WH.Calendar
			where 
				TheDate between coalesce(ConsentDate, ReceiptDate) and ResolutionDate
			)				
from
	dbo.WrkDatasetCOMP

inner join dbo.Dataset
on	Dataset.DatasetCode = 'COMP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ComplaintTurnaround'
	
where
	Dataset.DatasetCode = 'COMP'
and	WrkDatasetCOMP.CaseTypeCode in ('0x4120','0x4A20') --formal complaint and acute trust complaint
and	WrkDatasetCOMP.SequenceNo = 1
and	WrkDatasetCOMP.ResolutionDate is not null




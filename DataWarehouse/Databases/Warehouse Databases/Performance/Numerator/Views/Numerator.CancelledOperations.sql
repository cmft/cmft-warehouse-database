﻿
Create view [Numerator].CancelledOperations as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetCANOP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = SiteID 
	,DirectorateID = DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDatasetCANOP.CancellationDateID
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDatasetCANOP  

inner join dbo.Dataset
on	Dataset.DatasetCode = 'CANOP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CancelledOperations'





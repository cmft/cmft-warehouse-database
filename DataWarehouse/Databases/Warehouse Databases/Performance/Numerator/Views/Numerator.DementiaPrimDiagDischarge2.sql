﻿CREATE view [Numerator].[DementiaPrimDiagDischarge2]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.DementiaPrimDiagDischarge2'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1
and	(
		left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'F00' 
	or	left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'F01' 
	or	left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'F02' 
	or	left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'F03' 
	or	PrimaryDiagnosisCode in ('F10.7', 'F11.7', 'F12.7', 'F13.7', 'F14.7', 'F15.7', 'F16.7', 'F17.7', 'F18.7', 'F19.7')
	)

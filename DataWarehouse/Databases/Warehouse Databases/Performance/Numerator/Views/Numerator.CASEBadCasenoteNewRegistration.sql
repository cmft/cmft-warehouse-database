﻿
CREATE view [Numerator].[CASEBadCasenoteNewRegistration] as

select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDataset.EndSiteID		
	,DirectorateID = WrkDataset.EndDirectorateID
	,SpecialtyID = WrkDataset.SpecialtyID
	,ClinicianID = WrkDataset.ClinicianID
	,DateID = WrkDataset.AllocatedDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDataset

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CASEBadCasenoteNewRegistration'
	
where
	Dataset.DatasetCode = 'CASE'
and WrkDataset.NewRegistration = 1
and left(WrkDataset.CasenoteNumber, 1) <> 'M'


--and not exists -- New Registration = no previously allocated Casenote Number
--(
--	select 1
--	from WrkDataset D1
--	where D1.SourcePatientNo = D.SourcePatientNo
--	and D1.DatasetRecno <> D.DatasetRecno
--	and D1.AllocatedDate < D.AllocatedDate
--)



﻿


CREATE view [Numerator].[CharlsonIndexDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.StartSiteID		
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeStartDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = CharlsonIndex

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CharlsonIndexDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective'

where
	WrkDatasetAPC.DominantForDiagnosis = 1
and	WrkDatasetAPC.NationalPatientClassificationCode not in ('2','3','4')
and	WrkDatasetAPC.PatientCategoryCode = 'NE'




﻿

Create view [Numerator].[HRQualifiedNursingMidwiferyVacanciesBandFive]
as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = (GeneralLedgerEstBand5 - ESRSIPBand5)

from
	dbo.WrkDatasetHR 

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HRQualifiedNursingMidwiferyVacanciesBandFive'

where
	WrkDatasetHR.GeneralLedgerEstBand5 is not null









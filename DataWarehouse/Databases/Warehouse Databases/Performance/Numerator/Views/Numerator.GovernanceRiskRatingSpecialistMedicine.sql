﻿

CREATE view [Numerator].GovernanceRiskRatingSpecialistMedicine as


select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetGOVERNANCE.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = SiteID 
	,DirectorateID = WrkDatasetGOVERNANCE.DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDatasetGOVERNANCE.ActivityDateID
	,ServicePointID
	,Value = GovernanceRiskRating
from 
	dbo.WrkDatasetGOVERNANCE 

inner join dbo.Dataset
on	Dataset.DatasetCode = 'GOVERNANCE'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.GovernanceRiskRatingSpecialistMedicine'

where 
	Dataset.DatasetCode = 'GOVERNANCE'
and WrkDatasetGOVERNANCE.DirectorateID = 5 -- Specialist Medicine
and WrkDatasetGOVERNANCE.GovernanceRiskRating is not null


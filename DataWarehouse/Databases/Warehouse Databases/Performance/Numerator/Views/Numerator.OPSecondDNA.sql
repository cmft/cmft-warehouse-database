﻿CREATE view [Numerator].[OPSecondDNA] as

select distinct -- later appointment details
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOP.SiteID
	,DirectorateID = WrkDatasetOP.DirectorateID
	,SpecialtyID = WrkDatasetOP.SpecialtyID
	,ClinicianID = WrkDatasetOP.ClinicianID
	,DateID = WrkDatasetOP.AppointmentDateID 
	,WrkDatasetOP.ServicePointID
	,Value = 1

from
	dbo.WrkDatasetOP

inner join 	dbo.Dataset
on	Dataset.DatasetCode = 'OP'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.OPSecondDNA'

inner join dbo.WrkDatasetOP Earlier -- earlier DNA for same patient
on	Earlier.SourcePatientNo = WrkDatasetOP.SourcePatientNo -- same patient
and Earlier.AppointmentDate < WrkDatasetOP.AppointmentDate -- earlier appointment date
and Earlier.AppointmentOutcomeCode = WrkDatasetOP.AppointmentOutcomeCode -- with outcome = DNA

where
	WrkDatasetOP.AppointmentOutcomeCode = 'DNA'



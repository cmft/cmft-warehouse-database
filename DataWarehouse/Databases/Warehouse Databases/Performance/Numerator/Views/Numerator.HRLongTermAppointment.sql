﻿

CREATE view [Numerator].[HRLongTermAppointment]
as

-- 20160119	RR	A change was made to the HR file.  Jonathan Hodges advised that the indicator should remain the same but the calculation should be Headcounter Period End / Headcount Period Start

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = WrkDatasetHR.LongTermAppointment -- HeadcountPeriodEnd--

from
	dbo.WrkDatasetHR 

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HRLongTermAppointment'

where
	WrkDatasetHR.LongTermAppointment is not null




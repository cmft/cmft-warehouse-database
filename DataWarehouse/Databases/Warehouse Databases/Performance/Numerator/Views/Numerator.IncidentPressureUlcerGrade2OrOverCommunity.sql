﻿

CREATE view [Numerator].[IncidentPressureUlcerGrade2OrOverCommunity]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentPressureUlcerGrade2OrOverCommunity'
	
where
	(
	WrkDatasetINC.Cause1Code in -- Pressure Ulcer
								(
								--,'0x5849' Grade 1 -- Community aquired 1
								'0x5949' -- Community aquired 2
								,'0x5A49' -- Community aquired 3
								,'0x6149' -- Community aquired 4
								) 

	or
		(
			WrkDatasetINC.Cause1Code in -- Pressure Ulcer
									(
									'0x4C48'
									,'0x4D48'
									,'0x4E48'
									--,'0x5449' Grade 1
									,'0x5549'
									,'0x5649'
									,'0x5749'
									--,'0x614C' Grade 1
									,'0x624C'
									,'0x634C'
									,'0x644C'
									,'0x704C'
									,'0x714C'
									,'0x724C'
									) 
		and SourceDepartmentCode in    (  -- These are community department codes    
													'0x6749'
													,'0x204A'
													,'0x3949'
													,'0x674C'
													,'0x6949'
													,'0x5544'
													,'0x414D'
													,'0x424D'
													,'0x434D'
													,'0x4D4E'
													)
		
		)
	)
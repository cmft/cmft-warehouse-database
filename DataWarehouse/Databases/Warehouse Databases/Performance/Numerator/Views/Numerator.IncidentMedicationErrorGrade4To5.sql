﻿




CREATE view [Numerator].[IncidentMedicationErrorGrade4To5]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade4To5'
	
where
	(
		WrkDatasetINC.IncidentTypeCode = '0x5120' 
	or	WrkDatasetINC.CauseGroupCode = '0x4620'
	) -- Medication Error
and	WrkDatasetINC.IncidentGradeCode in --('0x4520', '0x4120') -- Harm Grade = 4 or 5
									(
									'0x4520' --4 Major
									,'0x4A20' --4U Major Unconfirmed
									,'0x4920' --4C Major
									,'0x4F20' --4# Nof
									,'0x4120' --5 Catastrophic
									,'0x4C20' --5U Catastrophic Unconfirmed
									,'0x4B20' --5C Catastophic
									) --CH 07012015 Grading changed



﻿create view [Numerator].[RENQPostDialysisBPLt110Over70] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRENQ.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetRENQ.SiteID		
	,DirectorateID = WrkDatasetRENQ.DirectorateID
	,SpecialtyID = WrkDatasetRENQ.SpecialtyID
	,ClinicianID = WrkDatasetRENQ.ClinicianID
	,DateID = WrkDatasetRENQ.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetRENQ

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RENQ'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.RENQPostDialysisBPLt110Over70'
	
where
	WrkDatasetRENQ.ModalityCode = '90:55425' -- HD
and WrkDatasetRENQ.ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and WrkDatasetRENQ.EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients' 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and (WrkDatasetRENQ.DateOfDeath is null or WrkDatasetRENQ.DateOfDeath > WrkDatasetRENQ.LastDayOfQuarter) -- Include only patients alive at quarter end
and (WrkDatasetRENQ.PostVitalsSittingBloodPressureSystolic < 100 or WrkDatasetRENQ.PostVitalsSittingBloodPressureDiastolic < 70)

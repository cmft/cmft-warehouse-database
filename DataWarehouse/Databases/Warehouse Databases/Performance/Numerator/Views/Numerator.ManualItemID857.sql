﻿
CREATE view Numerator.ManualItemID857 as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetMAN.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetMAN.SiteID		
	,DirectorateID = WrkDatasetMAN.DirectorateID
	,SpecialtyID = WrkDatasetMAN.SpecialtyID
	,ClinicianID = WrkDatasetMAN.ClinicianID
	,DateID = WrkDatasetMAN.DateID
	,ServicePointID = WrkDatasetMAN.ServicePointID
	,Value = WrkDatasetMAN.Numerator
from
	dbo.WrkDatasetMAN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'MAN'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ManualItemID857'
	
where
	ItemID = '857'
﻿CREATE view [Numerator].[AEUnplannedReAttendance] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAES.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAES.SiteID		
	,DirectorateID = WrkDatasetAES.DirectorateID
	,SpecialtyID = WrkDatasetAES.SpecialtyID
	,ClinicianID = WrkDatasetAES.ClinicianID
	,DateID = WrkDatasetAES.EncounterDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetAES

inner join dbo.Dataset
on	Dataset.DatasetCode = 'AES'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AEUnplannedReAttendance'
	
where
	WrkDatasetAES.StageCode = 'INDEPARTMENTADJUSTED'
and WrkDatasetAES.UnplannedReattend7Days <> 0

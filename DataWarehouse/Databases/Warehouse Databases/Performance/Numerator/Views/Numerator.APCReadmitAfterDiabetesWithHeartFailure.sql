﻿
CREATE view [Numerator].[APCReadmitAfterDiabetesWithHeartFailure] as

-- Select Spell A which has a later Spell B within 12 months for the same Patient
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.APCReadmitAfterDiabetesWithHeartFailure'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 
and	exists
	(
	select
		1 
	from
		dbo.WrkDatasetAPC LaterSpell
	where
		LaterSpell.SourcePatientNo = WrkDatasetAPC.SourcePatientNo
	and LaterSpell.AdmissionTime > WrkDatasetAPC.DischargeTime
	and datediff(month,WrkDatasetAPC.DischargeTime,LaterSpell.AdmissionTime) < 12
	)

and left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'I50' -- Heart failure
and	(
	left(WrkDatasetAPC.SecondaryDiagnosisCode1,3) between 'E08' and 'E13' -- Diabetes
	or left(WrkDatasetAPC.SecondaryDiagnosisCode2,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode3,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode4,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode5,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode6,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode7,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode8,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode9,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode10,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode11,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode12,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode13,3) between 'E08' and 'E13'			
	)


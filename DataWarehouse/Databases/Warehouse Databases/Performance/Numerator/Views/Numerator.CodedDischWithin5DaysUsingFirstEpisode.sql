﻿CREATE view [Numerator].[CodedDischWithin5DaysUsingFirstEpisode]

as

-- Count Spells in which all Episodes are coded within 5 days of discharge

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CodedDischWithin5DaysUsingFirstEpisode'
	
where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.DischargeTime is not null
and	WrkDatasetAPC.ClinicalCodingCompleteDate is not null
and	not exists -- any Episode in Spell not coded within 5 days of discharge
	(
	select
		1
	from dbo.WrkDatasetAPC UncodedSpell
	where
		UncodedSpell.DatasetRecno <> WrkDatasetAPC.DatasetRecno -- Different Episode
	and UncodedSpell.ProviderSpellNo = WrkDatasetAPC.ProviderSpellNo -- Same Spell
	and datediff(day, UncodedSpell.DischargeDate, UncodedSpell.ClinicalCodingCompleteDate) > 5 -- Clin coding took > 5 days
	)

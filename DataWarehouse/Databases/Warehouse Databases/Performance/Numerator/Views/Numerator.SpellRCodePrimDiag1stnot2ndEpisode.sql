﻿



CREATE view [Numerator].[SpellRCodePrimDiag1stnot2ndEpisode]

as

select --top 10
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(WrkDatasetAPC.EndSiteID,WrkDatasetAPC.StartSiteID)
	,DirectorateID = coalesce(WrkDatasetAPC.EndDirectorateID,WrkDatasetAPC.StartDirectorateID)
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeEndDateID
	,ServicePointID = coalesce(WrkDatasetAPC.EndServicePointID,WrkDatasetAPC.StartServicePointID)
	,Value = 1

from 
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.SpellRCodePrimDiag1stnot2ndEpisode'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.ClinicalCodingCompleteDate is not null
and	left(WrkDatasetAPC.PrimaryDiagnosisCode, 1) = 'R'
and	NationalPatientClassificationCode not in ('2','3','4')
and	not exists
		(
		select
			1
		from
			dbo.WrkDatasetAPC SecondEpisode
		where
			SecondEpisode.ProviderSpellNo = WrkDatasetAPC.ProviderSpellNo
		and SecondEpisode.EpisodeNo = 2
		and	left(WrkDatasetAPC.PrimaryDiagnosisCode, 1) = 'R'
		)



﻿



CREATE view [Numerator].[OccupiedInpatientBeds] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetBEDOCC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetBEDOCC.SiteID		
	,DirectorateID = WrkDatasetBEDOCC.DirectorateID
	,SpecialtyID = WrkDatasetBEDOCC.SpecialtyID
	,ClinicianID = WrkDatasetBEDOCC.ClinicianID
	,DateID = WrkDatasetBEDOCC.DateID
	,ServicePointID
	,Value = WrkDatasetBEDOCC.BedOccupancyNights

from
	dbo.WrkDatasetBEDOCC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'BEDOCC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OccupiedInpatientBeds'
	
where 
	WrkDatasetBEDOCC.BedOccupancyNights is not null
and WrkDatasetBEDOCC.AvailableInpatientBeds is null




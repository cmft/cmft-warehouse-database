﻿create view [Numerator].[SESSActual210MinSession] as

-- Derive Actual 210 Minute Sessions from (Actual) Duration / 210 minutes

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSESS.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetSESS.SiteID		
	,DirectorateID = WrkDatasetSESS.DirectorateID
	,SpecialtyID = WrkDatasetSESS.SpecialtyID
	,ClinicianID = WrkDatasetSESS.ClinicianID
	,DateID = WrkDatasetSESS.SessionDateID
	,ServicePointID
	,Value = cast(Duration as decimal(19,4)) / 210

from
	dbo.WrkDatasetSESS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'SESS'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.SESSActual210MinSession'
	
where
	CancelledFlag = 0



﻿CREATE view [Numerator].[OutpatientDNA]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOP.SiteID		
	,DirectorateID = WrkDatasetOP.DirectorateID
	,SpecialtyID = WrkDatasetOP.SpecialtyID
	,ClinicianID = WrkDatasetOP.ClinicianID
	,DateID = WrkDatasetOP.AppointmentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetOP

inner join dbo.Dataset
on	Dataset.DatasetCode = 'OP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OutpatientDNA'
	
where
	NationalAttendanceStatusCode = '3' -- DNA

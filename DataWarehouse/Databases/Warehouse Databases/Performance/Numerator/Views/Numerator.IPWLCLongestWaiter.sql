﻿


CREATE view [Numerator].[IPWLCLongestWaiter] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetIPWLC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetIPWLC.SiteID   
	,DirectorateID = WrkDatasetIPWLC.DirectorateID
	,SpecialtyID = WrkDatasetIPWLC.SpecialtyID
	,ClinicianID = WrkDatasetIPWLC.ClinicianID
	,DateID = WrkDatasetIPWLC.CensusDateID -- Census Date
	,ServicePointID = WrkDatasetIPWLC.ServicePointID
	,Value = LongestWait.Value
from 
	dbo.WrkDatasetIPWLC

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'IPWLC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.IPWLCLongestWaiter'

inner join --get longest wait by each attribute
	(
	select
		 WrkDatasetIPWLC.SiteID
		,WrkDatasetIPWLC.DirectorateID
		,WrkDatasetIPWLC.SpecialtyID
		,WrkDatasetIPWLC.ClinicianID
		,WrkDatasetIPWLC.CensusDateID
		,Value = max(WrkDatasetIPWLC.Duration)
	from
		dbo.WrkDatasetIPWLC
	group by
		 WrkDatasetIPWLC.SiteID
		,WrkDatasetIPWLC.DirectorateID
		,WrkDatasetIPWLC.SpecialtyID
		,WrkDatasetIPWLC.ClinicianID
		,WrkDatasetIPWLC.CensusDateID
	) LongestWait

on	WrkDatasetIPWLC.SiteID = LongestWait.SiteID
and	WrkDatasetIPWLC.DirectorateID = LongestWait.DirectorateID
and	WrkDatasetIPWLC.SpecialtyID = LongestWait.SpecialtyID
and	WrkDatasetIPWLC.ClinicianID = LongestWait.ClinicianID
and	WrkDatasetIPWLC.CensusDateID = LongestWait.CensusDateID

where
	Dataset.DatasetCode = 'IPWLC'
﻿CREATE view [Numerator].[APCChildReadmitAfterAsthmaOrDiabetes] as

-- Select Spell A which has a later Spell B within 12 months for the same Patient
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.APCChildReadmitAfterAsthmaOrDiabetes'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 
and exists
	(
	select
		1
	from 
		WrkDatasetAPC LaterSpell
	where
		WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1
	and LaterSpell.SourcePatientNo = WrkDatasetAPC.SourcePatientNo
	and LaterSpell.AdmissionTime > WrkDatasetAPC.DischargeTime
	and datediff(month,WrkDatasetAPC.DischargeTime, LaterSpell.AdmissionTime) < 12
	)

and 
	(
		left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'J45' -- Asthma
	or left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'J46' -- Asthma
	or left(WrkDatasetAPC.PrimaryDiagnosisCode,3) between 'E08' and 'E13' -- Diabetes
	)
and	convert(int,round(datediff(hour,WrkDatasetAPC.DateOfBirth,WrkDatasetAPC.AdmissionTime)/8766,0)) <= 18

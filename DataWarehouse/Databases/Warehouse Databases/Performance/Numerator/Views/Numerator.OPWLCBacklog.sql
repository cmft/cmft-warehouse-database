﻿

CREATE view [Numerator].[OPWLCBacklog] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOPWLC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOPWLC.SiteID   
	,DirectorateID = WrkDatasetOPWLC.DirectorateID
	,SpecialtyID = WrkDatasetOPWLC.SpecialtyID
	,ClinicianID = WrkDatasetOPWLC.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDatasetOPWLC

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'OPWLC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.OPWLCBacklog'

where 
	WrkDatasetOPWLC.NationalFirstAttendanceCode = '1'
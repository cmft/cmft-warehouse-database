﻿




CREATE view [Numerator].[IncidentChildSafeguardingAlerts] as

-- Count Safeguarding Alerts

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentChildSafeguardingAlerts'
	
where
	(
		WrkDatasetINC.IncidentTypeCode = '0x5420' 
	or	WrkDatasetINC.CauseGroupCode in ('0x6420','0x5320') 
	or	WrkDatasetINC.Cause1Code in ('0x7247','0x7744')
	) -- Safeguarding
and IncidentInvolvingChild = 1






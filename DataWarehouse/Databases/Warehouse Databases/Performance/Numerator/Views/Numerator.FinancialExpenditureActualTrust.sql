﻿




CREATE view [Numerator].[FinancialExpenditureActualTrust] as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFINANCE.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetFINANCE.SiteID		
	,DirectorateID = WrkDatasetFINANCE.DirectorateID
	,SpecialtyID = WrkDatasetFINANCE.SpecialtyID
	,ClinicianID = WrkDatasetFINANCE.ClinicianID
	,DateID = WrkDatasetFINANCE.CensusDateID
	,ServicePointID = WrkDatasetFINANCE.ServicePointID
	,Value = WrkDatasetFINANCE.Actual - WrkDatasetFINANCE.Budget 

/*
Change requested by S.Maloney 20.10.14. Agreed by G.Summerfield. 
For some reason the definition of calculation changes at trust level??
*/

from 
	dbo.WrkDatasetFINANCE

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FINANCE'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialExpenditureActualTrust'
	
where
	WrkDatasetFINANCE.DirectorateID = 15 -- Trust





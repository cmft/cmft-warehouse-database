﻿

CREATE view [Numerator].[NonElective14DayReadmission]

as

-- Select Spell A which has a later Spell B within 14 days for the same Patient
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NonElective14DayReadmission'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 
and	WrkDatasetAPC.PatientCategoryCode = 'NE'
-- Look for a later Spell B	
and exists
	(
	select
		1
	from
		dbo.WrkDatasetAPC LaterSpell
	where 
		LaterSpell.SourcePatientNo = WrkDatasetAPC.SourcePatientNo
	and LaterSpell.FirstEpisodeInSpellIndicator = 1
	and LaterSpell.PatientCategoryCode = 'NE'
	and LaterSpell.DischargeTime > WrkDatasetAPC.DischargeTime
	and datediff(d,WrkDatasetAPC.DischargeTime, LaterSpell.AdmissionTime) <= 14
	)



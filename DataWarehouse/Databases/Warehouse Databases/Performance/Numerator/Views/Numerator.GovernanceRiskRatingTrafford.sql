﻿

CREATE view [Numerator].GovernanceRiskRatingTrafford as


select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetGOVERNANCE.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = SiteID 
	,DirectorateID = WrkDatasetGOVERNANCE.DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDatasetGOVERNANCE.ActivityDateID
	,ServicePointID
	,Value = GovernanceRiskRating
from 
	dbo.WrkDatasetGOVERNANCE 

inner join dbo.Dataset
on	Dataset.DatasetCode = 'GOVERNANCE'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.GovernanceRiskRatingTrafford'

where 
	WrkDatasetGOVERNANCE.DirectorateID = 20 -- Trafford
and WrkDatasetGOVERNANCE.GovernanceRiskRating is not null


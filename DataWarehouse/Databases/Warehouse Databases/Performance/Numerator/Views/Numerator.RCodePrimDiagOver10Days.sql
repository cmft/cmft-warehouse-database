﻿
CREATE view [Numerator].[RCodePrimDiagOver10Days]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(WrkDatasetAPC.EndSiteID,WrkDatasetAPC.StartSiteID)
	,DirectorateID = coalesce(WrkDatasetAPC.EndDirectorateID,WrkDatasetAPC.StartDirectorateID)
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeEndDateID
	,ServicePointID = coalesce(WrkDatasetAPC.EndServicePointID,WrkDatasetAPC.StartServicePointID)
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RCodePrimDiagOver10Days'

where
	WrkDatasetAPC.ClinicalCodingCompleteDate is not null
and	left(WrkDatasetAPC.PrimaryDiagnosisCode, 1) = 'R'
and	datediff(dd,EpisodeStartDate, EpisodeEndDate) > 10


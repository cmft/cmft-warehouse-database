﻿
create view Numerator.ComplaintConsentCommunicationOrConfidentiality as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetCOMP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetCOMP.SiteID		
	,DirectorateID = WrkDatasetCOMP.DirectorateID
	,SpecialtyID = WrkDatasetCOMP.SpecialtyID
	,ClinicianID = WrkDatasetCOMP.ClinicianID
	,DateID = WrkDatasetCOMP.ReceiptDateID
	,ServicePointID = WrkDatasetCOMP.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetCOMP

inner join dbo.Dataset
on	Dataset.DatasetCode = 'COMP'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ComplaintConsentCommunicationOrConfidentiality'
		
where
	Dataset.DatasetCode = 'COMP'
and	WrkDatasetCOMP.CategoryTypeCode = '0x2042'

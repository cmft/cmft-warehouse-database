﻿



CREATE view [Numerator].[IncidentGrade5] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentGrade5'
	
where
	WrkDatasetINC.IncidentGradeCode in	
									(
									'0x4120' --5 Catastrophic
									,'0x4C20' --5U Catastrophic Unconfirmed
									,'0x4B20' --5C Catastophic
									) --CH 07012015 Grading changed






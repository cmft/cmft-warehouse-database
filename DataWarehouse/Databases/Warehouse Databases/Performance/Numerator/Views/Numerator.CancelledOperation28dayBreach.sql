﻿


CREATE view [Numerator].[CancelledOperation28dayBreach] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetCANOP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = SiteID 
	,DirectorateID = DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = Calendar.DateID --WrkDatasetCANOP.CancellationDateID CH 20160112 GS agreed it needs to be Breach month not Cancelled Month
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDatasetCANOP  

inner join dbo.Dataset
on	Dataset.DatasetCode = 'CANOP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CancelledOperation28dayBreach'

inner join dbo.Calendar
on dateadd(day,29,WrkDatasetCANOP.CancellationDate) = Calendar.TheDate

where 
	WrkDatasetCANOP.ReportableBreach = 1





﻿
CREATE view [Numerator].[APCAdmissionWithComplicationComorbidity] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.APCAdmissionWithComplicationComorbidity'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and WrkDatasetAPC.DischargeDate is not null
and WrkDatasetAPC.ComplicationComorbidity = '1'--'WITH' -- HRG with complications or co-morbidities
and WrkDatasetAPC.PatientClassificationCode in ('1','2')
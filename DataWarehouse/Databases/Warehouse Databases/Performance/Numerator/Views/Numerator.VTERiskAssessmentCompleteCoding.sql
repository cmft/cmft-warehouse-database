﻿






CREATE view [Numerator].[VTERiskAssessmentCompleteCoding] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.StartSiteID
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.VTERiskAssessmentCompleteCoding'

where
	WrkDatasetAPC.ClinicalCodingCompleteDate is not null
and	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and WrkDatasetAPC.VTE = 1
and WrkDatasetAPC.VTECompleteAndOrExclusion = 1










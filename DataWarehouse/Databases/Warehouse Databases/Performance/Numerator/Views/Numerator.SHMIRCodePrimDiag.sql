﻿CREATE view [Numerator].[SHMIRCodePrimDiag]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset 
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.SHMIRCodePrimDiag'

where
	WrkDatasetAPC.DominantForDiagnosis = 1
and	DischargeDateID is not null
and	left(WrkDatasetAPC.PrimaryDiagnosisCode, 1) = 'R'

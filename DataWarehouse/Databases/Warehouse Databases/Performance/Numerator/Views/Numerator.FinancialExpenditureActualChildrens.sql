﻿



CREATE view [Numerator].[FinancialExpenditureActualChildrens] as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFINANCE.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetFINANCE.SiteID		
	,DirectorateID = WrkDatasetFINANCE.DirectorateID
	,SpecialtyID = WrkDatasetFINANCE.SpecialtyID
	,ClinicianID = WrkDatasetFINANCE.ClinicianID
	,DateID = WrkDatasetFINANCE.CensusDateID
	,ServicePointID = WrkDatasetFINANCE.ServicePointID
	,Value = WrkDatasetFINANCE.Actual - WrkDatasetFINANCE.Budget 
from 
	dbo.WrkDatasetFINANCE

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FINANCE'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialExpenditureActualChildrens'
	
where
	WrkDatasetFINANCE.DirectorateID = 1 -- Childrens





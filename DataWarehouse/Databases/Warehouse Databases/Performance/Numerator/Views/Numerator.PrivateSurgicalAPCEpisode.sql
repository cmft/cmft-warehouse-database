﻿
CREATE view [Numerator].[PrivateSurgicalAPCEpisode]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeStartDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.PrivateSurgicalAPCEpisode'

where
	WrkDatasetAPC.StartDirectorateCode in (0,62)
and WrkDatasetAPC.StartWardCode in ('SUBA','SUBP')
and WrkDatasetAPC.NationalSpecialtyCode < '272'


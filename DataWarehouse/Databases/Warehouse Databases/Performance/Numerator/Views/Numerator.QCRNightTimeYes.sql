﻿CREATE view [Numerator].[QCRNightTimeYes] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetQCR.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetQCR.SiteID		
	,DirectorateID = WrkDatasetQCR.DirectorateID
	,SpecialtyID = WrkDatasetQCR.SpecialtyID
	,ClinicianID = WrkDatasetQCR.ClinicianID
	,DateID = WrkDatasetQCR.AuditDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetQCR

inner join dbo.Dataset
on	Dataset.DatasetCode = 'QCR'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.QCRNightTimeYes'
	
where
	WrkDatasetQCR.CustomListCode = 3 -- Night Time
and	WrkDatasetQCR.Answer = 1

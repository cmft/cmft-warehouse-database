﻿create view [Numerator].[SESSTimeLostWithLateStarts] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSESS.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetSESS.SiteID		
	,DirectorateID = WrkDatasetSESS.DirectorateID
	,SpecialtyID = WrkDatasetSESS.SpecialtyID
	,ClinicianID = WrkDatasetSESS.ClinicianID
	,DateID = WrkDatasetSESS.SessionDateID
	,ServicePointID
	,Value = LateStartMinutes

from
	dbo.WrkDatasetSESS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'SESS'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.SESSTimeLostWithLateStarts'
	
where
	LateStartMinutes > 0

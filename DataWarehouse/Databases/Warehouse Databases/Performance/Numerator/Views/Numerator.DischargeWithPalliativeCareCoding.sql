﻿CREATE view [Numerator].[DischargeWithPalliativeCareCoding]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.DischargeWithPalliativeCareCoding'
	
where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1
and	WrkDatasetAPC.DischargeDate is not null
and	exists
	(
	select	
		1
	from
		dbo.WrkDatasetAPC Spell		
	where
		Spell.PalliativeCare = 1
	and	Spell.ProviderSpellNo = WrkDatasetAPC.ProviderSpellNo
	and	Spell.ContextCode = WrkDatasetAPC.ContextCode
	)
and	WrkDatasetAPC.PatientCategoryCode not in
						(
						'DC'
						,'RD'
						)

﻿CREATE view [Numerator].[SESSStartingGt5MinsLate] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSESS.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetSESS.SiteID		
	,DirectorateID = WrkDatasetSESS.DirectorateID
	,SpecialtyID = WrkDatasetSESS.SpecialtyID
	,ClinicianID = WrkDatasetSESS.ClinicianID
	,DateID = WrkDatasetSESS.SessionDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetSESS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'SESS'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.SESSStartingGt5MinsLate'
	
where
	LateStartMinutes > 5

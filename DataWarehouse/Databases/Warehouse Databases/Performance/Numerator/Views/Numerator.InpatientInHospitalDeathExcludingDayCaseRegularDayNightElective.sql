﻿

CREATE view [Numerator].[InpatientInHospitalDeathExcludingDayCaseRegularDayNightElective]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InpatientInHospitalDeathExcludingDayCaseRegularDayNightElective'

where
	NationalDischargeMethodCode in ('4','5')
and	NationalLastEpisodeInSpellCode = 1
and	NationalPatientClassificationCode not in ('2','3','4')
and	PatientCategoryCode = 'EL'



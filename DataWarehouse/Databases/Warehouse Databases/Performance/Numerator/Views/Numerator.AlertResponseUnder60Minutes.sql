﻿









CREATE view [Numerator].[AlertResponseUnder60Minutes] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetALERT.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetALERT.SiteID		
	,DirectorateID = WrkDatasetALERT.DirectorateID
	,SpecialtyID = WrkDatasetALERT.SpecialtyID
	,ClinicianID = WrkDatasetALERT.ClinicianID
	,DateID = WrkDatasetALERT.CreatedDateID
	,WrkDatasetALERT.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetALERT

inner join dbo.Dataset
on	Dataset.DatasetCode = 'ALERT'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AlertResponseUnder60Minutes'
	
where
	datediff(minute,WrkDatasetALERT.CreatedTime, coalesce(WrkDatasetALERT.ClosedTime, getdate())) <60









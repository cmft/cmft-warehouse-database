﻿CREATE view [Numerator].[DementiaPrimDiagBedDays]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = datediff(dd, WrkDatasetAPC.AdmissionTime, WrkDatasetAPC.DischargeTime)

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.DementiaPrimDiagBedDays'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1
and	WrkDatasetAPC.PatientCategoryCode in ('EL','NE')
and	left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'F03'
and	datediff(dd, WrkDatasetAPC.AdmissionTime, WrkDatasetAPC.DischargeTime) > 0

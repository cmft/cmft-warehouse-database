﻿










CREATE view [Numerator].[ObservationSetTakenOnTime] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOBSET.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOBSET.SiteID		
	,DirectorateID = WrkDatasetOBSET.DirectorateID
	,SpecialtyID = WrkDatasetOBSET.SpecialtyID
	,ClinicianID = WrkDatasetOBSET.ClinicianID
	,DateID = WrkDatasetOBSET.StartDateID
	,ServicePointID = WrkDatasetOBSET.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetOBSET

inner join dbo.Dataset
on	Dataset.DatasetCode = 'OBSET'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ObservationSetTakenOnTime'
	
where
	WrkDatasetOBSET.ObservationStatus = 'On time'
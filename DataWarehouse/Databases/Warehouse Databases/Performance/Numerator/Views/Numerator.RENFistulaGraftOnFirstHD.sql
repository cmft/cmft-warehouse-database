﻿CREATE view [Numerator].[RENFistulaGraftOnFirstHD] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetREN.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetREN.SiteID		
	,DirectorateID = WrkDatasetREN.DirectorateID
	,SpecialtyID = WrkDatasetREN.SpecialtyID
	,ClinicianID = WrkDatasetREN.ClinicianID
	,DateID = WrkDatasetREN.StartDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetREN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'REN'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.RENFistulaGraftOnFirstHD'
	
where
	ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients' 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and VascularAccessCode in ('90:100126','90:6983','90:6985') -- A/V Fistula Created, A/V Graft Created

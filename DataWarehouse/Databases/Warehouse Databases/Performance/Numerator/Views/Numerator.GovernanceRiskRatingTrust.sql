﻿

CREATE view [Numerator].[GovernanceRiskRatingTrust] as


select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetGOVERNANCE.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = SiteID 
	,DirectorateID = WrkDatasetGOVERNANCE.DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDatasetGOVERNANCE.ActivityDateID
	,ServicePointID
	,Value = GovernanceRiskRating
from 
	dbo.WrkDatasetGOVERNANCE 

inner join dbo.Dataset
on	Dataset.DatasetCode = 'GOVERNANCE'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.GovernanceRiskRatingTrust'

where 
	WrkDatasetGOVERNANCE.DirectorateID = 15 -- Trust
and WrkDatasetGOVERNANCE.GovernanceRiskRating is not null



﻿

CREATE view [Numerator].[NursingWorkforceRegisteredNurseActualDayHours] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSTAFFLEVEL.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetSTAFFLEVEL.SiteID		
	,DirectorateID = WrkDatasetSTAFFLEVEL.DirectorateID
	,SpecialtyID = WrkDatasetSTAFFLEVEL.SpecialtyID
	,ClinicianID = WrkDatasetSTAFFLEVEL.ClinicianID
	,DateID = WrkDatasetSTAFFLEVEL.CensusDateID
	,ServicePointID = WrkDatasetSTAFFLEVEL.ServicePointID
	,Value = RegisteredNurseActual
from
	dbo.WrkDatasetSTAFFLEVEL

inner join dbo.Dataset
on	Dataset.DatasetCode = 'STAFFLEVEL'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NursingWorkforceRegisteredNurseActualDayHours'
	
where
	WrkDatasetSTAFFLEVEL.SourceShift = 'Dhours'









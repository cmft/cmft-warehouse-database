﻿CREATE view [Numerator].[BedDaysAgeOver65]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID =WrkDatasetAPC.EndServicePointID 
	,Value = datediff(dd,WrkDatasetAPC.AdmissionTime, WrkDatasetAPC.DischargeTime)

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.BedDaysAgeOver65'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1
and	datediff(dd,WrkDatasetAPC.AdmissionTime, WrkDatasetAPC.DischargeTime) > 0
and	convert(int,round(datediff(hour, WrkDatasetAPC.DateOfBirth, WrkDatasetAPC.AdmissionTime)/8766,0)) >= 65

﻿

CREATE view [Numerator].[IncidentFallGrade4To5]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentFallGrade4To5'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		WrkDatasetINC.CauseGroupCode = '0x3120' 
	or	WrkDatasetINC.Cause1Code in ('0x7042','0x7542','0x7142','0x5220','0x7342','0x7242')
	) -- Falls
and	WrkDatasetINC.IncidentGradeCode in --('0x4520','0x4120') -- Harm Grade = 4 or 5
									(
									'0x4520' --4 Major
									,'0x4A20' --4U Major Unconfirmed
									,'0x4920' --4C Major
									,'0x4F20' --4# Nof
									,'0x4120' --5 Catastrophic
									,'0x4C20' --5U Catastrophic Unconfirmed
									,'0x4B20' --5C Catastophic
									) --CH 07012015 Grading changed


and SourceDepartmentCode not in    (  -- These are community department codes    
                                                '0x6749'
                                                ,'0x204A'
                                                ,'0x3949'
                                                ,'0x674C'
                                                ,'0x6949'
                                                ,'0x5544'
                                                ,'0x414D'
                                                ,'0x424D'
                                                ,'0x434D'
                                                ,'0x4D4E'
                                                )

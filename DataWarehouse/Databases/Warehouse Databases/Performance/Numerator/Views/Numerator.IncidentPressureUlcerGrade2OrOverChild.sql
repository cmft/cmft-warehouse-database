﻿




CREATE view [Numerator].[IncidentPressureUlcerGrade2OrOverChild]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentPressureUlcerGrade2OrOverChild'
	
where
	WrkDatasetINC.Cause1Code in -- Pressure Ulcer
									(
									'0x4C48'
									,'0x4D48'
									,'0x4E48'
									,'0x5449'
									,'0x5549'
									,'0x5649'
									,'0x5749'
									,'0x5849'
									,'0x5949'
									,'0x5A49'
									,'0x6149'
									,'0x614C'
									,'0x624C'
									,'0x634C'
									,'0x644C'
									,'0x704C'
									,'0x714C'
									,'0x724C'
									) 
and	WrkDatasetINC.IncidentInvolvingChild = 1






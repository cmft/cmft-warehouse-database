﻿CREATE view [Numerator].[RENQDialysisPrimaryInMinimalCareSetting] as

-- Patients on dialysis in hospital or satellite setting where setting is primary

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRENQ.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetRENQ.SiteID		
	,DirectorateID = WrkDatasetRENQ.DirectorateID
	,SpecialtyID = WrkDatasetRENQ.SpecialtyID
	,ClinicianID = WrkDatasetRENQ.ClinicianID
	,DateID = WrkDatasetRENQ.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetRENQ

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RENQ'

inner join dbo.Numerator
on NumeratorLogic = 'Numerator.RENQDialysisPrimaryInMinimalCareSetting'

where
	ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55433','90:55435') -- Hospital or Satellite
and EventDetailCode in ('90:8000172','90:8000173') -- HD - Self Care in Unit, HDF - Self Care in Unit
and DateOfDeath is null
and IsPrimary = 1

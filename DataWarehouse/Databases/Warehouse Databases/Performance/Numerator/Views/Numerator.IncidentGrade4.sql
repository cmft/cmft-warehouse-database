﻿



CREATE view [Numerator].[IncidentGrade4] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentGrade4'
	
where
	WrkDatasetINC.IncidentGradeCode in	
								(
								'0x4A20' --4U Major Unconfirmed
								,'0x4920' --4C Major
								,'0x4F20' --4# Nof
								) --CH 07012015 Grading changed



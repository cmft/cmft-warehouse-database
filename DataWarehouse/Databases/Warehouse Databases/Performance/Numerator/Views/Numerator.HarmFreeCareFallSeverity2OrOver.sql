﻿



CREATE view [Numerator].[HarmFreeCareFallSeverity2OrOver] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFALL.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetFALL.StartSiteID		
	,DirectorateID = WrkDatasetFALL.StartDirectorateID
	,SpecialtyID = WrkDatasetFALL.SpecialtyID
	,ClinicianID = WrkDatasetFALL.ClinicianID
	,DateID = WrkDatasetFALL.FallDateID
	,ServicePointID
	,Value = 1
from	
	dbo.WrkDatasetFALL

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FALL' 

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCareFallSeverity2OrOver'
	
where
	WrkDatasetFALL.SourceFallSeverityCode > 1







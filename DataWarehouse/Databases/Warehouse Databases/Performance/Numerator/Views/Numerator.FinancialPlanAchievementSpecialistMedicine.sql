﻿
CREATE view [Numerator].FinancialPlanAchievementSpecialistMedicine as


select
	DatasetID = WrkDataset.DatasetID
	,DatasetRecno = WrkDataset.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = StartSiteID 
	,DirectorateID = WrkDataset.StartDirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID = WrkDataset.ActivityDateID
	,ServicePointID
	,Value = FinanceRiskRating
from 
	dbo.WrkDataset 

inner join dbo.Dataset
on	Dataset.DatasetID = WrkDataset.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FinancialPlanAchievementSpecialistMedicine'

where 
	Dataset.DatasetCode = 'FINANCE'
and StartDirectorateID = 5 -- Specialist Medicine



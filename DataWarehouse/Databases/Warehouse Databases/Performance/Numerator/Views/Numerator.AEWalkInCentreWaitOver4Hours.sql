﻿CREATE view [Numerator].[AEWalkInCentreWaitOver4Hours] as

select
	DatasetID = d.DatasetID
	,DatasetRecno = d.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.ArrivalDateID
	,ServicePointID
	,Value = 1

from dbo.WrkDataset d

inner join dbo.Dataset
on	Dataset.DatasetID = d.DatasetID

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AEWalkInCentreWaitOver4Hours'
	
where Dataset.DatasetCode = 'AE'
and d.ContextCode = 'CEN||ADAS'
and d.EndSiteCode = 'RW3MR'
and datediff(minute,ArrivalTime,DepartureTime) > 240

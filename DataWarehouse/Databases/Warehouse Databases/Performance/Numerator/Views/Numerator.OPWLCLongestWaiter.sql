﻿



CREATE view [Numerator].[OPWLCLongestWaiter] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOPWLC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOPWLC.SiteID   
	,DirectorateID = WrkDatasetOPWLC.DirectorateID
	,SpecialtyID = WrkDatasetOPWLC.SpecialtyID
	,ClinicianID = WrkDatasetOPWLC.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDatasetOPWLC

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'OPWLC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.OPWLCLongestWaiter'

inner join --get longest wait by each attribute
	(
	select
		WrkDatasetOPWLC.SiteID
		,WrkDatasetOPWLC.DirectorateID
		,WrkDatasetOPWLC.SpecialtyID
		,WrkDatasetOPWLC.ClinicianID
		,WrkDatasetOPWLC.CensusDateID
		,Value = max(WrkDatasetOPWLC.Duration)
	from
		dbo.WrkDatasetOPWLC

	group by
		WrkDatasetOPWLC.SiteID
		,WrkDatasetOPWLC.DirectorateID
		,WrkDatasetOPWLC.SpecialtyID
		,WrkDatasetOPWLC.ClinicianID
		,WrkDatasetOPWLC.CensusDateID
	) LongestWait

on	WrkDatasetOPWLC.SiteID = LongestWait.SiteID
and	WrkDatasetOPWLC.DirectorateID = LongestWait.DirectorateID
and	WrkDatasetOPWLC.SpecialtyID = LongestWait.SpecialtyID
and	WrkDatasetOPWLC.ClinicianID = LongestWait.ClinicianID
and	WrkDatasetOPWLC.CensusDateID = LongestWait.CensusDateID

where
	WrkDatasetOPWLC.NationalFirstAttendanceCode = '1'
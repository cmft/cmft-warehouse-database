﻿

CREATE view [Numerator].[HarmFreeCareVTE] as

-- Note that the data extracted from BedMan does not include all events
-- but only events requiring treatment or causing actual harm 

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetVTECOND.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetVTECOND.StartSiteID		
	,DirectorateID = WrkDatasetVTECOND.StartDirectorateID
	,SpecialtyID = WrkDatasetVTECOND.SpecialtyID
	,ClinicianID = WrkDatasetVTECOND.ClinicianID
	,DateID = WrkDatasetVTECOND.DiagnosisDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDatasetVTECOND

inner join dbo.Dataset
on	Dataset.DatasetCode = 'VTECOND'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCareVTE'
	
where
	WrkDatasetVTECOND.DiagnosisDate >= WrkDatasetVTECOND.AdmissionDate
and	WrkDatasetVTECOND.MedicationRequired = 1



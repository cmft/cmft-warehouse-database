﻿


CREATE view [Numerator].[IncidentMedicationErrorGrade1AgeOver65]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade1AgeOver65'
	
where
	Dataset.DatasetCode = 'INC'
and	(
		WrkDatasetINC.IncidentTypeCode = '0x5120' 
	or	WrkDatasetINC.CauseGroupCode = '0x4620'
	) -- Medication Error
and	WrkDatasetINC.IncidentInvolvingElderly = 1
and	WrkDatasetINC.IncidentGradeCode --= '0x4220' -- Harm Grade = 1
								 in	
								(
								'0x4220' --1 No Harm
								,'0x4D20' --1U No Harm Unconfirmed
								) --CH 07012015 Grading changed


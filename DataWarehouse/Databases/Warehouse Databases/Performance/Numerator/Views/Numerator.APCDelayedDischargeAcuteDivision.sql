﻿CREATE view [Numerator].[APCDelayedDischargeAcuteDivision] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = EndServicePointID
	,Value = 1

from dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.APCDelayedDischargeAcuteDivision'
	
where
	WrkDatasetAPC.Division = 'Acute Medical'
and WrkDatasetAPC.ExpectedDateOfDischarge < WrkDatasetAPC.DischargeDate

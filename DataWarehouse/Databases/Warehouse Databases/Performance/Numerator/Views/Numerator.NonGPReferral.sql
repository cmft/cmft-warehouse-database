﻿CREATE view [Numerator].[NonGPReferral]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRF.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetRF.SiteID		
	,DirectorateID = WrkDatasetRF.DirectorateID
	,SpecialtyID = WrkDatasetRF.SpecialtyID
	,ClinicianID = WrkDatasetRF.ClinicianID
	,DateID = WrkDatasetRF.ReferralDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetRF

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RF'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NonGPReferral'

where
	WrkDatasetRF.NationalSourceOfReferralCode not in ('03','92')

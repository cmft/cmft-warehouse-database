﻿








CREATE view [Numerator].[ComplaintReopened] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetCOMP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetCOMP.SiteID		
	,DirectorateID = WrkDatasetCOMP.DirectorateID
	,SpecialtyID = WrkDatasetCOMP.SpecialtyID
	,ClinicianID = WrkDatasetCOMP.ClinicianID
	,DateID = WrkDatasetCOMP.ReceiptDateID
	,WrkDatasetCOMP.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetCOMP

inner join dbo.Dataset
on	Dataset.DatasetCode = 'COMP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ComplaintReopened'
	
where
	Dataset.DatasetCode = 'COMP'
and	WrkDatasetCOMP.CaseTypeCode in ('0x4120','0x4A20') --formal complaint and acute trust complaint
and WrkDatasetCOMP.Reopened = 1
and ReceiptDate >= '1 apr 2014' 
and SequenceNo = 1









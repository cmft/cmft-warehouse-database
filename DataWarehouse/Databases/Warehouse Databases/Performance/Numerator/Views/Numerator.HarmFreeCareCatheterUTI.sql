﻿



CREATE view [Numerator].[HarmFreeCareCatheterUTI] as

-- BedMan extract data includes only Events causing actual harm 

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetUTI.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetUTI.StartSiteID		
	,DirectorateID = WrkDatasetUTI.StartDirectorateID
	,SpecialtyID = WrkDatasetUTI.SpecialtyID
	,ClinicianID = WrkDatasetUTI.ClinicianID
	,DateID = WrkDatasetUTI.SymptomStartDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDatasetUTI

inner join dbo.Dataset
on	Dataset.DatasetCode = 'UTI'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCareCatheterUTI'
	
where
	WrkDatasetUTI.SourceCatheterTypeCode = '2' -- Urethral Catheter
and WrkDatasetUTI.SymptomStartDate >= WrkDatasetUTI.AdmissionDate
and	WrkDatasetUTI.TreatmentStartDate is not null





﻿
CREATE view [Numerator].[NonElective30DayReadmission]

as

-- Select Spell A which has a following Non-Elective Spell B within 30 days for the same Patient
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NonElective30DayReadmission'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 
and WrkDatasetAPC.PatientCategoryCode = 'NE'
and exists
	(
	select
		1
	from
		dbo.WrkDatasetAPC LaterSpell
	where
		LaterSpell.FirstEpisodeInSpellIndicator = 1
	and LaterSpell.SourcePatientNo = WrkDatasetAPC.SourcePatientNo
	and LaterSpell.PatientCategoryCode = 'NE'
	and LaterSpell.DischargeTime > WrkDatasetAPC.DischargeTime
	and datediff(d,WrkDatasetAPC.DischargeTime, LaterSpell.AdmissionTime) <= 30
	)


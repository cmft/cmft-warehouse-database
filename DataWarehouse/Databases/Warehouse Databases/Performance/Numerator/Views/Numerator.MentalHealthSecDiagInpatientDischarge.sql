﻿CREATE view [Numerator].[MentalHealthSecDiagInpatientDischarge]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID =  WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.MentalHealthSecDiagInpatientDischarge'

where
	NationalLastEpisodeInSpellCode = 1
and	WrkDatasetAPC.PatientCategoryCode in ('EL','NE')
and	left(WrkDatasetAPC.SecondaryDiagnosisCode1,1) = 'F'

﻿create view [Numerator].[RENQUreaReductionRatioGt65] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRENQ.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetRENQ.SiteID		
	,DirectorateID = WrkDatasetRENQ.DirectorateID
	,SpecialtyID = WrkDatasetRENQ.SpecialtyID
	,ClinicianID = WrkDatasetRENQ.ClinicianID
	,DateID = WrkDatasetRENQ.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetRENQ

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RENQ'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.RENQUreaReductionRatioGt65'
	
where DatasetCode = 'RENQ'
and ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients' 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and (DateOfDeath is null or DateOfDeath > LastDayOfQuarter) -- Include only patients alive at quarter end
and URRNum > 65

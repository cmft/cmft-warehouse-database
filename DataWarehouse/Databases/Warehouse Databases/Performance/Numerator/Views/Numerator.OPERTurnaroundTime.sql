﻿create view [Numerator].[OPERTurnaroundTime] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOPER.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOPER.SiteID		
	,DirectorateID = WrkDatasetOPER.DirectorateID
	,SpecialtyID = WrkDatasetOPER.SpecialtyID
	,ClinicianID = WrkDatasetOPER.ClinicianID
	,DateID = WrkDatasetOPER.OperationDateID
	,ServicePointID
	,Value = TurnaroundTime

from
	dbo.WrkDatasetOPER

inner join dbo.Dataset
on	Dataset.DatasetCode = 'OPER'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OPERTurnaroundTime'
	
where
	coalesce(TurnaroundTime,0) > 0

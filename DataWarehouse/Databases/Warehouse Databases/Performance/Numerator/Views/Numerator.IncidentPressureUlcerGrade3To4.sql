﻿

CREATE view [Numerator].[IncidentPressureUlcerGrade3To4]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentPressureUlcerGrade3To4'
	
where
	WrkDatasetINC.Cause1Code in -- Pressure Ulcer grade 3 & 4
							(
							'0x4D48'
							,'0x4E48'
							,'0x5649'
							,'0x5749'
							,'0x634C'
							,'0x644C'
							,'0x714C'
							,'0x724C'
							)
and SourceDepartmentCode not in   
							(  -- These are community department codes    
							'0x6749'
							,'0x204A'
							,'0x3949'
							,'0x674C'
							,'0x6949'
							,'0x5544'
							,'0x414D'
							,'0x424D'
							,'0x434D'
							,'0x4D4E'
							)

	




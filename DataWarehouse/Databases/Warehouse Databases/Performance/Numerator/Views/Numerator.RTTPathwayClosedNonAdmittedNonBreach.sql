﻿

create view Numerator.RTTPathwayClosedNonAdmittedNonBreach as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRTT.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetRTT.SiteID
	,DirectorateID = WrkDatasetRTT.DirectorateID
	,SpecialtyID = WrkDatasetRTT.SpecialtyID
	,ClinicianID = WrkDatasetRTT.ClinicianID
	,DateID = WrkDatasetRTT.CensusDateID
	,WrkDatasetRTT.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetRTT

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RTT'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.RTTPathwayClosedNonAdmittedNonBreach'

where
	WrkDatasetRTT.PathwayStatusCode = 'OPT'
and	WrkDatasetRTT.BreachStatusCode = 'N'
﻿

CREATE view [Numerator].[MedicalOutliers] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetBEDOCC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetBEDOCC.SiteID		
	,DirectorateID = WrkDatasetBEDOCC.DirectorateID
	,SpecialtyID = WrkDatasetBEDOCC.SpecialtyID
	,ClinicianID = WrkDatasetBEDOCC.ClinicianID
	,DateID = WrkDatasetBEDOCC.DateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetBEDOCC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'BEDOCC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.MedicalOutliers'
	
where 
	WrkDatasetBEDOCC.MedicalOutlier = 1
and WrkDatasetBEDOCC.AvailableInpatientBeds is null


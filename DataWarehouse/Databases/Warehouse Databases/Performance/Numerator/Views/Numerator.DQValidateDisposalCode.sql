﻿
CREATE view Numerator.DQValidateDisposalCode as

select 
	 DatasetID = v.DatasetID
	,DatasetRecno = v.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = d.EndSiteID		
	,DirectorateID = d.EndDirectorateID
	,SpecialtyID = d.SpecialtyID
	,ClinicianID = d.ClinicianID
	,DateID = d.EncounterDateID
	,ServicePointID = -1
	,Value = 1

-- Pick up dataset and recno from DQ output table ...
from WarehouseOLAPMergedV2.DQ.DatasetValidation v

-- ... for relevant logic
inner join WarehouseOLAPMergedV2.DQ.Error e
on e.ErrorID = v.ErrorID
and e.ErrorLogic = 'DQ.ValidateDisposalCode'

-- Join to WrkDataset for remaining required fields
inner join WarehouseOLAPMergedV2.DQ.Dataset ds1 on ds1.DatasetID = v.DatasetID
inner join dbo.Dataset ds2 on ds2.DatasetCode = ds1.DatasetCode

inner join dbo.WrkDataset d on d.DatasetID = ds2.DatasetID
and d.DatasetRecno = v.DatasetRecno

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.DQValidateDisposalCode'

﻿
CREATE view [Numerator].[CountOfInpatientDiagCodes]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(WrkDatasetAPC.EndSiteID,WrkDatasetAPC.StartSiteID)
	,DirectorateID = coalesce(WrkDatasetAPC.EndDirectorateID,WrkDatasetAPC.StartDirectorateID)
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeEndDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 
		case when PrimaryDiagnosisCode is not null then 1 else 0 end +
		case when SubsidiaryDiagnosisCode is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode1 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode2 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode3 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode4 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode5 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode6 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode7 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode8 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode9 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode10 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode11 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode12 is not null then 1 else 0 end +
		case when SecondaryDiagnosisCode13 is not null then 1 else 0 end
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.CountOfInpatientDiagCodes'



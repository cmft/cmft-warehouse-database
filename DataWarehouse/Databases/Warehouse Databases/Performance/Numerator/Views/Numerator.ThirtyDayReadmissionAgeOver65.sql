﻿
CREATE view [Numerator].[ThirtyDayReadmissionAgeOver65] as

-- Select Spell A which has a later Spell B within 30 days for the same Patient
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ThirtyDayReadmissionAgeOver65'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 

and exists
	(
	select
		1 
	from
		dbo.WrkDatasetAPC Later
	where
		Later.FirstEpisodeInSpellIndicator = 1
	and Later.SourcePatientNo = WrkDatasetAPC.SourcePatientNo
	and Later.AdmissionTime > WrkDatasetAPC.DischargeTime
	and datediff(d,WrkDatasetAPC.DischargeTime, Later.AdmissionTime) <= 30
	)
and	convert(int,round(datediff(hour,DateOfBirth,WrkDatasetAPC.AdmissionTime)/8766,0)) >= 65


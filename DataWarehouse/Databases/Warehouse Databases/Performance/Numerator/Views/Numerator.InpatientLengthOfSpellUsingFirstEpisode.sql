﻿
CREATE view [Numerator].[InpatientLengthOfSpellUsingFirstEpisode]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = datediff(day, WrkDatasetAPC.AdmissionDate, WrkDatasetAPC.DischargeDate)

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InpatientLengthOfSpellUsingFirstEpisode'
	
where
	FirstEpisodeInSpellIndicator = 1
and	DischargeDate is not null

﻿CREATE view [Numerator].[MentalHealthPrimDiagInpatientDischarge]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.MentalHealthPrimDiagInpatientDischarge'

where
	Dataset.DatasetCode = 'APC'
and	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1
and	WrkDatasetAPC.PatientCategoryCode in ('EL','NE')
and	left(WrkDatasetAPC.PrimaryDiagnosisCode,1) = 'F'

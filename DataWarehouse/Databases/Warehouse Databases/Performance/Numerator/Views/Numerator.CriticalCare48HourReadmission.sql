﻿CREATE view [Numerator].[CriticalCare48HourReadmission]

as

-- Indicator to be redefined using MIDAS not PAS

-- Select Spell A which has a following Spell B in ITU / CDU within 48 hours for the same Patient
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.CriticalCare48HourReadmission'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 
-- Look for a later Spell B in Critical Care, within 48 hours	
and exists
	(
	select 1
		from WrkDatasetAPC LaterSpell
	where
		LaterSpell.FirstEpisodeInSpellIndicator = 1
	and LaterSpell.SourcePatientNo = WrkDatasetAPC.SourcePatientNo
	and LaterSpell.AdmissionTime > WrkDatasetAPC.DischargeTime
	and datediff(hour,WrkDatasetAPC.DischargeTime, LaterSpell.AdmissionTime) <= 48
	and (LaterSpell.StartWardCode like 'HDU%' or LaterSpell.StartWardCode like 'ITU%')
	)

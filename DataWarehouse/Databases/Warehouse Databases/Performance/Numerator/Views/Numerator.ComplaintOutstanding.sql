﻿

CREATE view [Numerator].[ComplaintOutstanding] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOUTCOMP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOUTCOMP.SiteID		
	,DirectorateID = WrkDatasetOUTCOMP.DirectorateID
	,SpecialtyID = WrkDatasetOUTCOMP.SpecialtyID
	,ClinicianID = WrkDatasetOUTCOMP.ClinicianID
	,DateID = WrkDatasetOUTCOMP.CensusDateID
	,WrkDatasetOUTCOMP.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetOUTCOMP

inner join dbo.Dataset
on	Dataset.DatasetCode = 'OUTCOMP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ComplaintOutstanding'
	
where
	Dataset.DatasetCode = 'OUTCOMP'
and	WrkDatasetOUTCOMP.CaseTypeCode in
								(
								'0x4A20' --formal complaint
								,'0x4120' --Acute Trust Complaint
								)                               
and WrkDatasetOUTCOMP.SequenceNo = 1



﻿CREATE view [Numerator].[MAT3rd4thDegreeTear]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetMAT.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetMAT.SiteID		
	,DirectorateID = WrkDatasetMAT.DirectorateID
	,SpecialtyID = WrkDatasetMAT.SpecialtyID
	,ClinicianID = WrkDatasetMAT.ClinicianID
	,DateID = WrkDatasetMAT.InfantDateOfBirthID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetMAT

inner join dbo.Dataset
on	Dataset.DatasetCode = 'MAT'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.MAT3rd4thDegreeTear'
	
where 
	BirthOrder = '1'
and MethodDelivery = 'S'
and Perineum = 'T'

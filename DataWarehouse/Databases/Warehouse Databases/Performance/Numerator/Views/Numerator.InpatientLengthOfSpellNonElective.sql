﻿










CREATE view [Numerator].[InpatientLengthOfSpellNonElective]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = datediff(day, WrkDatasetAPC.AdmissionDate, WrkDatasetAPC.DischargeDate)
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InpatientLengthOfSpellNonElective'

where
	FirstEpisodeInSpellIndicator = 1  -- To only count the spell LoS, and no duplicates as table is at episode level
and	DischargeDate is not null -- LoS, only need completed spells
and	NationalDischargeMethodCode not in (5) -- Exclude Stillbirth
and	WrkDatasetAPC.IsWellBabySpell = 0
and WrkDatasetAPC.AdmissionMethodCode not in ('11', '12', '13')








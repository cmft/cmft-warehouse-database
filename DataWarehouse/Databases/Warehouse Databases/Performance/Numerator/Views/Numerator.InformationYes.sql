﻿CREATE view [Numerator].[InformationYes] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetQCR.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetQCR.SiteID		
	,DirectorateID = WrkDatasetQCR.DirectorateID
	,SpecialtyID = WrkDatasetQCR.SpecialtyID
	,ClinicianID = WrkDatasetQCR.ClinicianID
	,DateID = WrkDatasetQCR.AuditDateID
	,ServicePointID
	,Value = 1
	,CustomListCode

from
	dbo.WrkDatasetQCR

inner join dbo.Dataset
on	DatasetCode = 'QCR'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.InformationYes'
	
where
	CustomListCode = 19 -- Information
and	Answer = 1

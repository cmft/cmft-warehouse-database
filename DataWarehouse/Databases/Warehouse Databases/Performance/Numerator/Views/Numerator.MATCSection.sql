﻿CREATE view [Numerator].[MATCSection]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetMAT.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetMAT.SiteID		
	,DirectorateID = WrkDatasetMAT.DirectorateID
	,SpecialtyID = WrkDatasetMAT.SpecialtyID
	,ClinicianID = WrkDatasetMAT.ClinicianID
	,DateID = WrkDatasetMAT.InfantDateOfBirthID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetMAT

inner join dbo.Dataset
on	Dataset.DatasetCode = 'MAT'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.MATCSection'
	
where
	BirthOrder = '1'
and MethodDelivery in ('U','C','D','E')

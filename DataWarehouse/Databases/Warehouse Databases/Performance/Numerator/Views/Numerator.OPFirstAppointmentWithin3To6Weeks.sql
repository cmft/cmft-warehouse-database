﻿CREATE view [Numerator].[OPFirstAppointmentWithin3To6Weeks] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOP.SiteID   
	,DirectorateID = WrkDatasetOP.DirectorateID
	,SpecialtyID = WrkDatasetOP.SpecialtyID
	,ClinicianID = WrkDatasetOP.ClinicianID
	,DateID = Calendar.DateID -- Census Date
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetOP

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'OP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OPFirstAppointmentWithin3To6Weeks'

-- Appointment is between 3 and 6 weeks of given FirstDayOvWeek
inner join WarehouseOLAPMergedV2.WH.Calendar on datediff(d,Calendar.FirstDayOfWeek,WrkDatasetOP.AppointmentDate) between 14 and 41
and Calendar.FirstDayOfWeek = Calendar.TheDate -- Only interested in FirstDayOfWeek record itself

where 
	NationalFirstAttendanceCode in ('1','3') -- First Appointment
and datediff(day,Calendar.FirstDayOfWeek, getdate()) between 0 and 42 -- Take last 6 weeks of data

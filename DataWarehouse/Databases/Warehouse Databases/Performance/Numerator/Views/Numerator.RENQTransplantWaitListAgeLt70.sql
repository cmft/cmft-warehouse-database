﻿create view [Numerator].[RENQTransplantWaitListAgeLt70] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRENQ.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetRENQ.SiteID		
	,DirectorateID = WrkDatasetRENQ.DirectorateID
	,SpecialtyID = WrkDatasetRENQ.SpecialtyID
	,ClinicianID = WrkDatasetRENQ.ClinicianID
	,DateID = WrkDatasetRENQ.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetRENQ

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RENQ'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.RENQTransplantWaitListAgeLt70'
	
where
	WrkDatasetRENQ.ModalityCode = '90:55425' -- HD
and WrkDatasetRENQ.ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and WrkDatasetRENQ.EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients', 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and (WrkDatasetRENQ.DateOfDeath is null or WrkDatasetRENQ.DateOfDeath > WrkDatasetRENQ.LastDayOfQuarter) -- Include only patients alive at quarter end
and WrkDatasetRENQ.TransplantListStatusCode in ('90:56505', '90:56506', '90:56599') -- 'TransplantList : Active', 'TransplantList: ActiveStatusUpdate', 'TransplantList : Temp Suspended'
and convert(int,round(datediff(hour,WrkDatasetRENQ.DateOfBirth,WrkDatasetRENQ.LastDayOfQuarter)/8766,0)) < 70

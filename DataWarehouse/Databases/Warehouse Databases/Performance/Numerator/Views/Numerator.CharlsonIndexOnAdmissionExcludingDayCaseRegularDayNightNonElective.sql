﻿


CREATE view [Numerator].[CharlsonIndexOnAdmissionExcludingDayCaseRegularDayNightNonElective]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.StartSiteID		
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = CharlsonIndex

from dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.CharlsonIndexOnAdmissionExcludingDayCaseRegularDayNightNonElective'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.NationalPatientClassificationCode not in ('2','3','4')
and	WrkDatasetAPC.PatientCategoryCode = 'NE'




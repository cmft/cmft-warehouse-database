﻿CREATE view [Numerator].[WSTransferFromMAUToWardAgeOver65] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetWS.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetWS.EndSiteID
	,DirectorateID = WrkDatasetWS.EndDirectorateID
	,SpecialtyID = WrkDatasetWS.SpecialtyID
	,ClinicianID = WrkDatasetWS.ClinicianID
	,DateID = WrkDatasetWS.StartDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetWS

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'WS'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.WSTransferFromMAUToWardAgeOver65'

where 
	StartWardCode in ('MAU','AMU')
and EndActivityCode = 'TR'
and convert(int,round(datediff(hour,DateOfBirth,AdmissionTime)/8766,0)) >= 65

﻿CREATE view [Numerator].[QCRBedRailAssessmentYes] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetQCR.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetQCR.SiteID		
	,DirectorateID = WrkDatasetQCR.DirectorateID
	,SpecialtyID = WrkDatasetQCR.SpecialtyID
	,ClinicianID = WrkDatasetQCR.ClinicianID
	,DateID = WrkDatasetQCR.AuditDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetQCR

inner join dbo.Dataset
on	Dataset.DatasetCode = 'QCR'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.QCRBedRailAssessmentYes'
	
where
	WrkDatasetQCR.QuestionCode IN (10973,10974,10975) -- Bed Rail Assessment
and	WrkDatasetQCR.Answer = 1

﻿CREATE view [Numerator].[PICU48HourReadmission]

as

-- Select PICU Spell A which has a following PICU Spell B within 48 hours for the same Patient
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.PICU48HourReadmission'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 
and	WrkDatasetAPC.EndWardCode = '80'
	
-- Look for a later Spell B	
and exists
	(
	select
		1
	from
		dbo.WrkDatasetAPC Later
	where
		Later.FirstEpisodeInSpellIndicator = 1
	and Later.SourcePatientNo = WrkDatasetAPC.SourcePatientNo
	and Later.StartWardCode = '80'
	and Later.DischargeTime > WrkDatasetAPC.DischargeTime
	and datediff(hour,WrkDatasetAPC.DischargeTime, Later.AdmissionTime) <= 48
	)

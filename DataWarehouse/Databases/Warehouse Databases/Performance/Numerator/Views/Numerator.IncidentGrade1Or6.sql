﻿





CREATE view [Numerator].[IncidentGrade1Or6] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentGrade1Or6'
	
where
	WrkDatasetINC.IncidentGradeCode in --('0x4220','0x4620') -- Harm Grade = 1 or 6
									(
									'0x4220' --1 No Harm
									,'0x4D20' --1U No Harm Unconfirmed
									,'0x4620' --6 Near Miss / No Harm
									) --CH 07012015 Grading changed





﻿



CREATE view [Numerator].[NonElectivePreOpBedDays]

as


select
	 DatasetID = Base.DatasetID
	,DatasetRecno = Base.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = Base.StartSiteID		
	,DirectorateID = Base.StartDirectorateID
	,SpecialtyID = Base.SpecialtyID
	,ClinicianID = Base.ClinicianID
	,DateID = Base.DischargeDateID
	,ServicePointID = Base.ServicePointID
	,Value = datediff(day, Base.AdmissionDate,FirstProcedure.PrimaryProcedureDate)
from
	dbo.WrkDataset Base

inner join	dbo.Dataset
on	Dataset.DatasetID = Base.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.NonElectivePreOpBedDays'

inner join
	(
	select
		ProviderSpellNo
		,ContextCode
		,PrimaryProcedureDate
	from
		dbo.WrkDataset Base

	inner join	dbo.Dataset
	on	Dataset.DatasetID = Base.DatasetID

	where
		DatasetCode = 'APC'
	and	PrimaryProcedureCode is not null

	/* get first procedure within spell */

	and	not exists
		(
		select
			1
		from
			dbo.WrkDataset Earlier
		where
			DatasetCode = 'APC'
		and	PrimaryProcedureDate is not null
		and	Base.ProviderSpellNo = Earlier.ProviderSpellNo
		and Base.PrimaryProcedureDate > Earlier.PrimaryProcedureDate
		)
	
	/* handles same procedure date on different FCEs ('3263506/1') */

	and	not exists
		(
		select
			1
		from
			dbo.WrkDataset Earlier
		where
			DatasetCode = 'APC'
		and	PrimaryProcedureDate is not null
		and	Base.ProviderSpellNo = Earlier.ProviderSpellNo
		and	Base.PrimaryProcedureDate = Earlier.PrimaryProcedureDate
		and	Base.SourceEncounterNo > Earlier.SourceEncounterNo
		)						
	) FirstProcedure

on	Base.ProviderSpellNo = FirstProcedure.ProviderSpellNo

where
	DatasetCode = 'APC'
and FirstEpisodeInSpellIndicator = 1
and PatientCategoryCode = 'NE'
and	exists -- gets first FCE only where there is a coded procedure in the spell 
	(
	select
		1
	from
		dbo.WrkDataset Operation

	inner join	dbo.Dataset
	on	Dataset.DatasetID = Base.DatasetID

	where
		DatasetCode = 'APC'
	and	PrimaryProcedureDate is not null
	and	PrimaryProcedureCode is not null
	and	Operation.ProviderSpellNo = Base.ProviderSpellNo
	)

--Old incorrect logic

--select
--	DatasetID = d.DatasetID
--	,DatasetRecno = d.DatasetRecno
--	,NumeratorID = Numerator.NumeratorID
--	,SiteID = d.EndSiteID
--	,DirectorateID = d.EndDirectorateID
--	,SpecialtyID = d.SpecialtyID
--	,ClinicianID = d.ClinicianID
--	,DateID = d.AdmissionDateID
--	,ServicePointID
--	,Value = datediff(dd,d.AdmissionTime, d.PrimaryProcedureDate)
--from
--	dbo.WrkDataset d
--inner join
--	dbo.Dataset Dataset
--on	d.DatasetID = Dataset.DatasetID

--inner join dbo.Numerator
--on	Numerator.NumeratorLogic = 'Numerator.NonElectivePreOpBedDays'

--where
--	Dataset.DatasetCode = 'APC'
--and
--	d.FirstEpisodeInSpellIndicator = 1
--and
--	d.PatientCategoryCode = 'NE'
--and
--	datediff(dd,d.AdmissionTime, d.PrimaryProcedureDate) > 0





﻿CREATE view [Numerator].[DeathsInElectiveInfraRenalAneurisms]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.DeathsInElectiveInfraRenalAneurisms'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1
and	WrkDatasetAPC.PatientCategoryCode = 'EL'
and WrkDatasetAPC.NationalSpecialtyCode = '107' 
and left(WrkDatasetAPC.PrimaryProcedureCode,3) in ('L18', 'L19', 'L27', 'L28')
and WrkDatasetAPC.NationalDischargeMethodCode in ('4','5')

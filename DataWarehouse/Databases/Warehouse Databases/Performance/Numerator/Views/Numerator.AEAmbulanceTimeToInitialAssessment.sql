﻿CREATE view [Numerator].[AEAmbulanceTimeToInitialAssessment] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAES.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAES.SiteID		
	,DirectorateID = WrkDatasetAES.DirectorateID
	,SpecialtyID = WrkDatasetAES.SpecialtyID
	,ClinicianID = WrkDatasetAES.ClinicianID
	,DateID = WrkDatasetAES.EncounterDateID
	,ServicePointID
	,Value = WrkDatasetAES.Duration

from
	dbo.WrkDatasetAES

inner join dbo.Dataset
on	Dataset.DatasetCode = 'AES'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.AEAmbulanceTimeToInitialAssessment'
	
where
	WrkDatasetAES.StageCode = 'ARRIVALTOTRIAGEADJUSTED'
and WrkDatasetAES.ArrivalModeCode = '1' -- Ambulance

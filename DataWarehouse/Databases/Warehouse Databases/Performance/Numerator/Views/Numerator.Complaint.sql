﻿








CREATE view [Numerator].[Complaint] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetCOMP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetCOMP.SiteID		
	,DirectorateID = WrkDatasetCOMP.DirectorateID
	,SpecialtyID = WrkDatasetCOMP.SpecialtyID
	,ClinicianID = WrkDatasetCOMP.ClinicianID
	,DateID = WrkDatasetCOMP.ReceiptDateID
	,WrkDatasetCOMP.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetCOMP 

inner join dbo.Dataset
on	Dataset.DatasetCode = 'COMP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.Complaint'
	
where
	WrkDatasetCOMP.CaseTypeCode in
								(
								'0x4A20' --formal complaint
								,'0x4120' --Acute Trust Complaint
								)                               
and WrkDatasetCOMP.Reopened is null
and WrkDatasetCOMP.SequenceNo = 1










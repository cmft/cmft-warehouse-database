﻿




CREATE view [Numerator].[PreoperativeLengthOfStay]

as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.StartSiteID		
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = datediff(day, WrkDatasetAPC.AdmissionDate,WrkDatasetAPC.FirstOperationInSpellTime)
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.PreoperativeLengthOfStay'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and WrkDatasetAPC.FirstOperationInSpellTime is not null
and WrkDatasetAPC.DischargeTime is not null
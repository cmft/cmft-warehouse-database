﻿




CREATE view [Numerator].[FriendsAndFamilyTestExtremelyLikelyAE] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFFTRTN.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetFFTRTN.SiteID		
	,DirectorateID = WrkDatasetFFTRTN.DirectorateID
	,SpecialtyID = WrkDatasetFFTRTN.SpecialtyID
	,ClinicianID = WrkDatasetFFTRTN.ClinicianID
	,DateID = WrkDatasetFFTRTN.CensusDateID
	,ServicePointID = WrkDatasetFFTRTN.ServicePointID
	,Value = WrkDatasetFFTRTN.ExtremelyLikeley
from
	dbo.WrkDatasetFFTRTN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FFTRTN'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FriendsAndFamilyTestExtremelyLikelyAE'
	
where
	WrkDatasetFFTRTN.FFTReturnType = 'AE'





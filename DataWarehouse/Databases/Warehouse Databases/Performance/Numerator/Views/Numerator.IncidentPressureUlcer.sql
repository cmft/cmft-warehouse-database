﻿
CREATE view [Numerator].[IncidentPressureUlcer]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentPressureUlcer'
	
where
	(
		(
			WrkDatasetINC.IncidentTypeCode = '0x5320' 
		and WrkDatasetINC.CauseGroupCode = '0x7920' 
		and WrkDatasetINC.Cause1Code <> '0x6348'
		)
	or
		(
			WrkDatasetINC.CauseGroupCode = '0x6320' 
		 and WrkDatasetINC.Cause1Code in ('0x4B48','0x4C48','0x4D48','0x4D48','0x5449','0x5549','0x5649','0x5749','0x5044','0x5849','0x5949','0x5A49','0x6149','0x5A49','0x5449','0x5549','0x5649','0x5749')
		)
	)


﻿


CREATE view [Numerator].[InpatientBeddaysPriorToOperation]

as

--select
--	Template = 'Not in use'

select
	 DatasetID = Base.DatasetID
	,DatasetRecno = Base.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = Base.StartSiteID		
	,DirectorateID = Base.StartDirectorateID
	,SpecialtyID = Base.SpecialtyID
	,ClinicianID = Base.ClinicianID
	,DateID = Base.DischargeDateID
	,ServicePointID = Base.ServicePointID
	,Value = datediff(day, Base.AdmissionDate,FirstProcedure.PrimaryProcedureDate)
from
	dbo.WrkDataset Base

inner join	dbo.Dataset
on	Dataset.DatasetID = Base.DatasetID

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.InpatientBeddaysPriorToOperation'

inner join
	(
	select
		ProviderSpellNo
		,ContextCode
		,PrimaryProcedureDate
	from
		dbo.WrkDataset Base

	inner join	dbo.Dataset
	on	Dataset.DatasetID = Base.DatasetID

	where
		DatasetCode = 'APC'
	and	PrimaryProcedureCode is not null

	/* get first procedure within spell */

	and	not exists
		(
		select
			1
		from
			dbo.WrkDataset Earlier
		where
			DatasetCode = 'APC'
		and	PrimaryProcedureDate is not null
		and	Base.ProviderSpellNo = Earlier.ProviderSpellNo
		and Base.PrimaryProcedureDate > Earlier.PrimaryProcedureDate
		)
	
	/* handles same procedure date on different FCEs ('3263506/1') */

	and	not exists
		(
		select
			1
		from
			dbo.WrkDataset Earlier
		where
			DatasetCode = 'APC'
		and	PrimaryProcedureDate is not null
		and	Base.ProviderSpellNo = Earlier.ProviderSpellNo
		and	Base.PrimaryProcedureDate = Earlier.PrimaryProcedureDate
		and	Base.SourceEncounterNo > Earlier.SourceEncounterNo
		)						
	) FirstProcedure

on	Base.ProviderSpellNo = FirstProcedure.ProviderSpellNo

where
	DatasetCode = 'APC'
and FirstEpisodeInSpellIndicator = 1
and PatientCategoryCode = 'EL'
and	exists -- gets first FCE only where there is a coded procedure in the spell 
	(
	select
		1
	from
		dbo.WrkDataset Operation

	inner join	dbo.Dataset
	on	Dataset.DatasetID = Base.DatasetID

	where
		DatasetCode = 'APC'
	and	PrimaryProcedureDate is not null
	and	PrimaryProcedureCode is not null
	and	Operation.ProviderSpellNo = Base.ProviderSpellNo
	)




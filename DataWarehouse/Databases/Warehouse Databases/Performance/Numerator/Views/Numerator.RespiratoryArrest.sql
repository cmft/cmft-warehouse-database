﻿








CREATE view Numerator.RespiratoryArrest as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRESUS.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetRESUS.SiteID		
	,DirectorateID = WrkDatasetRESUS.DirectorateID
	,SpecialtyID = WrkDatasetRESUS.SpecialtyID
	,ClinicianID = WrkDatasetRESUS.ClinicianID
	,DateID = WrkDatasetRESUS.CallOutDateID
	,ServicePointID = WrkDatasetRESUS.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetRESUS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RESUS'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.RespiratoryArrest'
	
where
	WrkDatasetRESUS.CallOutReason = 'Respiratory Arrest'
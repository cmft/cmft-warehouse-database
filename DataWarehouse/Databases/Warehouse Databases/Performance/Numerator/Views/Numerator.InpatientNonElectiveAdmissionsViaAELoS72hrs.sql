﻿


CREATE view [Numerator].[InpatientNonElectiveAdmissionsViaAELoS72hrs]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset Dataset
on DatasetCode = 'APC'

inner join dbo.Numerator
on Numerator.NumeratorLogic = 'Numerator.InpatientNonElectiveAdmissionsViaAELoS72hrs'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 -- why not first?  Keeping as is to keep consitent with other queries
and	WrkDatasetAPC.PatientCategoryCode = 'NE'
and	WrkDatasetAPC.AdmissionMethodCode = 21
and	datediff(mi,WrkDatasetAPC.AdmissionDate, WrkDatasetAPC.DischargeDate) <4320 -- <72hrs ie considered shortStay
and	WrkDatasetAPC.DischargeDate is not null




﻿CREATE view [Numerator].[OutpatientAttendedFirstAppointment]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOP.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetOP.SiteID   
	,DirectorateID = WrkDatasetOP.DirectorateID
	,SpecialtyID = WrkDatasetOP.SpecialtyID
	,ClinicianID = WrkDatasetOP.ClinicianID
	,DateID = WrkDatasetOP.AppointmentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetOP

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'OP'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.OutpatientAttendedFirstAppointment'

where
	NationalAttendanceStatusCode in ('5','6') -- Attended
and	NationalFirstAttendanceCode in ('1','3') -- First Appointment

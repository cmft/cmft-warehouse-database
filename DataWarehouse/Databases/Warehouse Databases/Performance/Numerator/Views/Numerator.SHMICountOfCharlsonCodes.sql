﻿CREATE view [Numerator].[SHMICountOfCharlsonCodes]

as

with CharlsonCode as
(
select
	DiagnosisCode
from
	WarehouseOLAPMergedV2.WH.Diagnosis
where
	IsCharlson = 1
)

select
	DatasetID
	,DatasetRecno
	,NumeratorID
	,SiteID
	,DirectorateID
	,SpecialtyID
	,ClinicianID
	,DateID
	,ServicePointID
	,Value  
from
	(
	select
		DatasetID = Dataset.DatasetID
		,DatasetRecno = WrkDatasetAPC.DatasetRecno
		,NumeratorID = Numerator.NumeratorID
		,SiteID = WrkDatasetAPC.EndSiteID
		,DirectorateID = WrkDatasetAPC.EndDirectorateID
		,SpecialtyID = WrkDatasetAPC.SpecialtyID
		,ClinicianID = WrkDatasetAPC.ClinicianID
		,DateID = WrkDatasetAPC.EpisodeEndDateID
		,ServicePointID = WrkDatasetAPC.EndServicePointID
		,Value = -- Add 1 for each Charlson code
				case when p.DiagnosisCode is not null then 1 else 0 end
				+ case when s.DiagnosisCode is not null then 1 else 0 end
				+ case when s1.DiagnosisCode is not null then 1 else 0 end
				+ case when s2.DiagnosisCode is not null then 1 else 0 end
				+ case when s3.DiagnosisCode is not null then 1 else 0 end
				+ case when s4.DiagnosisCode is not null then 1 else 0 end
				+ case when s5.DiagnosisCode is not null then 1 else 0 end
				+ case when s6.DiagnosisCode is not null then 1 else 0 end
				+ case when s7.DiagnosisCode is not null then 1 else 0 end
				+ case when s8.DiagnosisCode is not null then 1 else 0 end
				+ case when s9.DiagnosisCode is not null then 1 else 0 end
				+ case when s10.DiagnosisCode is not null then 1 else 0 end
				+ case when s11.DiagnosisCode is not null then 1 else 0 end
				+ case when s12.DiagnosisCode is not null then 1 else 0 end
				+ case when s13.DiagnosisCode is not null then 1 else 0 end
	from
		dbo.WrkDatasetAPC

	inner join dbo.Dataset
	on	Dataset.DatasetCode = 'APC'

	inner join dbo.Numerator
	on	Numerator.NumeratorLogic = 'Numerator.SHMICountOfCharlsonCodes'

	left join CharlsonCode p on WrkDatasetAPC.PrimaryDiagnosisCode = p.DiagnosisCode
	left join CharlsonCode s on	WrkDatasetAPC.SubsidiaryDiagnosisCode = s.DiagnosisCode
	left join CharlsonCode s1 on WrkDatasetAPC.SecondaryDiagnosisCode1 = s1.DiagnosisCode
	left join CharlsonCode s2 on WrkDatasetAPC.SecondaryDiagnosisCode2 = s2.DiagnosisCode
	left join CharlsonCode s3 on WrkDatasetAPC.SecondaryDiagnosisCode3 = s3.DiagnosisCode
	left join CharlsonCode s4 on WrkDatasetAPC.SecondaryDiagnosisCode4 = s4.DiagnosisCode
	left join CharlsonCode s5 on WrkDatasetAPC.SecondaryDiagnosisCode5 = s5.DiagnosisCode
	left join CharlsonCode s6 on WrkDatasetAPC.SecondaryDiagnosisCode6 = s6.DiagnosisCode
	left join CharlsonCode s7 on WrkDatasetAPC.SecondaryDiagnosisCode7 = s7.DiagnosisCode
	left join CharlsonCode s8 on WrkDatasetAPC.SecondaryDiagnosisCode8 = s8.DiagnosisCode
	left join CharlsonCode s9 on WrkDatasetAPC.SecondaryDiagnosisCode9 = s9.DiagnosisCode
	left join CharlsonCode s10 on WrkDatasetAPC.SecondaryDiagnosisCode10 = s10.DiagnosisCode
	left join CharlsonCode s11 on WrkDatasetAPC.SecondaryDiagnosisCode11 = s11.DiagnosisCode
	left join CharlsonCode s12 on WrkDatasetAPC.SecondaryDiagnosisCode12 = s12.DiagnosisCode
	left join CharlsonCode s13 on WrkDatasetAPC.SecondaryDiagnosisCode13 = s13.DiagnosisCode

	where
		WrkDatasetAPC.DominantForDiagnosis = 1
	) Charlson
where
	Value > 0 -- Don't write out records with 0 value

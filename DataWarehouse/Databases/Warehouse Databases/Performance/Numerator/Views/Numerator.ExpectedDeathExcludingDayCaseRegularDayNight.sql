﻿

CREATE view [Numerator].[ExpectedDeathExcludingDayCaseRegularDayNight]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = 
			coalesce(
				WrkDatasetAPC.EndSiteID
				,WrkDatasetAPC.StartSiteID
				)
	,DirectorateID = coalesce(WrkDatasetAPC.EndDirectorateID,WrkDatasetAPC.StartDirectorateID)
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeEndDateID
	,ServicePointID =  WrkDatasetAPC.EndServicePointID
	,Value = RiskScore
from
	dbo.WrkDatasetAPC

inner join	dbo.Dataset Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ExpectedDeathExcludingDayCaseRegularDayNight'

where
	WrkDatasetAPC.EpisodeEndDateID is not null
and	WrkDatasetAPC.NationalPatientClassificationCode not in ('2','3','4')


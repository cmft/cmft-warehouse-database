﻿


CREATE view [Numerator].[HRCorporateMandatoryTrainingCompleted]
as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = WrkDatasetHR.CorporateMandatoryTrainingCompleted

from
	dbo.WrkDatasetHR

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HRCorporateMandatoryTrainingCompleted'

where
	WrkDatasetHR.CorporateMandatoryTrainingCompleted is not null




﻿
CREATE view [Numerator].[ExpectedDateOfDischWithin24Hours]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
	
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.ExpectedDateOfDischWithin24Hours'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.DischargeTime is null
and	datediff(hh, getdate(), WrkDatasetAPC.ExpectedDateOfDischarge) between 0 and 24


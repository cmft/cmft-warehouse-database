﻿
Create view [Numerator].[ActivityAppointment] as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetACTIVITY.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetACTIVITY.SiteID		
	,DirectorateID = WrkDatasetACTIVITY.DirectorateID
	,SpecialtyID = WrkDatasetACTIVITY.SpecialtyID
	,ClinicianID = WrkDatasetACTIVITY.ClinicianID
	,DateID = WrkDatasetACTIVITY.ActivityDateID
	,ServicePointID = WrkDatasetACTIVITY.ServicePointID
	,Value = WrkDatasetACTIVITY.Cases

from
	dbo.WrkDatasetACTIVITY

inner join dbo.Dataset
on	Dataset.DatasetCode = 'ACTIVITY'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ActivityAppointment'
	
where
	WrkDatasetACTIVITY.ActivityMetricCode in ('FATT', 'RATT')



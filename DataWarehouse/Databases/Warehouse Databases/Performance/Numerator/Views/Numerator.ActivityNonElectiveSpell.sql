﻿




CREATE view [Numerator].[ActivityNonElectiveSpell]

as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetACTIVITY.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetACTIVITY.SiteID		
	,DirectorateID = WrkDatasetACTIVITY.DirectorateID
	,SpecialtyID = WrkDatasetACTIVITY.SpecialtyID
	,ClinicianID = WrkDatasetACTIVITY.ClinicianID
	,DateID = WrkDatasetACTIVITY.ActivityDateID
	,ServicePointID
	,Value = WrkDatasetACTIVITY.Cases

from
	dbo.WrkDatasetACTIVITY

inner join dbo.Dataset
on	Dataset.DatasetCode = 'ACTIVITY'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.ActivityNonElectiveSpell'
	
where
	WrkDatasetACTIVITY.ActivityMetricCode in ('EMSPL', 'MATSPL', 'NEOTHSPL')







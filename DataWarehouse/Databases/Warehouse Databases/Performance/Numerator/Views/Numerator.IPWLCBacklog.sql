﻿
create view [Numerator].[IPWLCBacklog] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetIPWLC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetIPWLC.SiteID   
	,DirectorateID = WrkDatasetIPWLC.DirectorateID
	,SpecialtyID = WrkDatasetIPWLC.SpecialtyID
	,ClinicianID = WrkDatasetIPWLC.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDatasetIPWLC

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'IPWLC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.IPWLCBacklog'

	
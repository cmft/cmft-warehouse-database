﻿


CREATE view [Numerator].[CountOfCharlsonCodesExcludingDayCaseRegularDayNight]

as


select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = coalesce(WrkDatasetAPC.EndSiteID,WrkDatasetAPC.StartSiteID)
	,DirectorateID = coalesce(WrkDatasetAPC.EndDirectorateID,WrkDatasetAPC.StartDirectorateID)
	,SpecialtyID =WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeEndDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = CharlsonDiagnosisCodeCount
from
	dbo.WrkDatasetAPC
	
inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.CountOfCharlsonCodesExcludingDayCaseRegularDayNight'
	
	
where
	NationalPatientClassificationCode not in ('2','3','4')
and	EpisodeEndDateID is not null	
and	CharlsonDiagnosisCodeCount is not null



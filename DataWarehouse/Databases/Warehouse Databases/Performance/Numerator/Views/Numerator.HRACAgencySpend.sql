﻿




CREATE view [Numerator].[HRACAgencySpend]
as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = WrkDatasetHR.ACAgencySpend
from
	dbo.WrkDatasetHR

inner join dbo.Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.HRACAgencySpend'

where
	WrkDatasetHR.ACAgencySpend is not null








﻿


CREATE view [Numerator].[IncidentFallGrade1To3]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'


inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentFallGrade1To3'
	
where
	(
		WrkDatasetINC.CauseGroupCode = '0x3120' 
	or	WrkDatasetINC.Cause1Code in ('0x7042','0x7542','0x7142','0x5220','0x7342','0x7242')
	) -- Falls
and WrkDatasetINC.IncidentGradeCode in 
								(
								'0x4220' --1 No Harm
								,'0x4D20' --1U No Harm Unconfirmed
								,'0x4320' --2 Slight
								,'0x4420' --3 Moderate
								) 


﻿





CREATE view [Numerator].[HarmFreeCarePressureUlcerCategory3Or4] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetPREULC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetPREULC.StartSiteID		
	,DirectorateID = WrkDatasetPREULC.StartDirectorateID
	,SpecialtyID = WrkDatasetPREULC.SpecialtyID
	,ClinicianID = WrkDatasetPREULC.ClinicianID
	,DateID = WrkDatasetPREULC.IdentifiedDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetPREULC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'PREULC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.HarmFreeCarePressureUlcerCategory2OrOver'
	
where
	datediff(hour, AdmissionTime, IdentifiedTime) > 72
and WrkDatasetPREULC.SourcePressureUlcerCategoryCode in (3, 4)








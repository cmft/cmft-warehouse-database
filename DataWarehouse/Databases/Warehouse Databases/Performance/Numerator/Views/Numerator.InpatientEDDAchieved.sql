﻿


CREATE view [Numerator].[InpatientEDDAchieved]

as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.StartSiteID		
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.InpatientEDDAchieved'

inner join dbo.Specialty
on	Specialty.SourceSpecialtyID = WrkDatasetAPC.SpecialtyID

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.NationalIntendedManagementCode not in ('3','4')
and	WrkDatasetAPC.NationalSpecialtyCode <> '501'
and	Specialty.SourceSpecialty not like '%DIALYSIS%'
and	left(Specialty.SourceSpecialtyCode, 2) <> 'IH'
and	WrkDatasetAPC.StartWardCode <> '76A'	
and	left(WrkDatasetAPC.StartWardCode, 3) <> 'SUB'
and	not 
	(
		WrkDatasetAPC.StartWardCode = 'ESTU' 
	and datediff(hour, WrkDatasetAPC.AdmissionTime, WrkDatasetAPC.DischargeTime) < 6
	)
and	EddCreatedTime is not null
and	datediff(day, WrkDatasetAPC.ExpectedDateOfDischarge, WrkDatasetAPC.DischargeTime) <= 0
and	datediff(minute, WrkDatasetAPC.AdmissionTime, WrkDatasetAPC.EddCreatedTime) < 1440



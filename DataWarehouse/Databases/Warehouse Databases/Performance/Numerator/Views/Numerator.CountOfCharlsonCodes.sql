﻿CREATE view [Numerator].[CountOfCharlsonCodes]

as


select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID =WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeEndDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = CharlsonDiagnosisCodeCount
from
	dbo.WrkDatasetAPC
	
inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.CountOfCharlsonCodesExcludingDayCaseRegularDayNight'
	
where
	WrkDatasetAPC.EpisodeEndDateID is not null	
and	WrkDatasetAPC.CharlsonDiagnosisCodeCount is not null

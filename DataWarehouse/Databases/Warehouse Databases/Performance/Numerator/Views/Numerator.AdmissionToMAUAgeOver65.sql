﻿
CREATE view [Numerator].[AdmissionToMAUAgeOver65]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetWS.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetWS.StartSiteID
	,DirectorateID = WrkDatasetWS.StartDirectorateID
	,SpecialtyID = WrkDatasetWS.SpecialtyID
	,ClinicianID = WrkDatasetWS.ClinicianID
	,DateID = WrkDatasetWS.StartDateID
	,WrkDatasetWS.ServicePointID
	,Value = 1

from
	dbo.WrkDatasetWS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'WS'

inner join dbo.Numerator
on	Numerator.NumeratorLogic = 'Numerator.AdmissionToMAUAgeOver65'

where
	left(WrkDatasetWS.StartWardCode,3) = 'MAU'
and	convert(int,round(datediff(hour,WrkDatasetWS.DateOfBirth, WrkDatasetWS.StartDate)/8766,0)) >= 65


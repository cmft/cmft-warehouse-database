﻿

CREATE view [Numerator].[IncidentMedicationErrorGrade2AgeOver65]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetINC.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetINC.SiteID		
	,DirectorateID = WrkDatasetINC.DirectorateID
	,SpecialtyID = WrkDatasetINC.SpecialtyID
	,ClinicianID = WrkDatasetINC.ClinicianID
	,DateID = WrkDatasetINC.IncidentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetINC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'INC'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.IncidentMedicationErrorGrade2AgeOver65'
	
where
	(
		WrkDatasetINC.IncidentTypeCode = '0x5120' 
	or	WrkDatasetINC.CauseGroupCode = '0x4620'
	) -- Medication Error
and	WrkDatasetINC.IncidentGradeCode = '0x4320' -- Harm Grade = 2
and	WrkDatasetINC.IncidentInvolvingElderly = 1



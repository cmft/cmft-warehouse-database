﻿




CREATE view [Numerator].FriendsAndFamilyTestExtremelyLikelyIP as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFFTRTN.DatasetRecno
	,NumeratorID = Numerator.NumeratorID
	,SiteID = WrkDatasetFFTRTN.SiteID		
	,DirectorateID = WrkDatasetFFTRTN.DirectorateID
	,SpecialtyID = WrkDatasetFFTRTN.SpecialtyID
	,ClinicianID = WrkDatasetFFTRTN.ClinicianID
	,DateID = WrkDatasetFFTRTN.CensusDateID
	,ServicePointID = WrkDatasetFFTRTN.ServicePointID
	,Value = WrkDatasetFFTRTN.Responses
from
	dbo.WrkDatasetFFTRTN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FFTRTN'

inner join dbo.Numerator
on	NumeratorLogic = 'Numerator.FriendsAndFamilyTestExtremelyLikelyIP'
	
where
	WrkDatasetFFTRTN.FFTReturnType = 'IP'





﻿create view [Denominator].[OPEROperationWithTurnaroundTime] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOPER.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetOPER.SiteID		
	,DirectorateID = WrkDatasetOPER.DirectorateID
	,SpecialtyID = WrkDatasetOPER.SpecialtyID
	,ClinicianID = WrkDatasetOPER.ClinicianID
	,DateID = WrkDatasetOPER.OperationDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetOPER

inner join dbo.Dataset
on	Dataset.DatasetCode = 'OPER'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OPEROperationWithTurnaroundTime'
	
where
	coalesce(TurnaroundTime,0) > 0

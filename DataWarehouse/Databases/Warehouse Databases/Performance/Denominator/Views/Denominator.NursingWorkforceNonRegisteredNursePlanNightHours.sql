﻿






CREATE view [Denominator].[NursingWorkforceNonRegisteredNursePlanNightHours] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSTAFFLEVEL.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetSTAFFLEVEL.SiteID		
	,DirectorateID = WrkDatasetSTAFFLEVEL.DirectorateID
	,SpecialtyID = WrkDatasetSTAFFLEVEL.SpecialtyID
	,ClinicianID = WrkDatasetSTAFFLEVEL.ClinicianID
	,DateID = WrkDatasetSTAFFLEVEL.CensusDateID
	,ServicePointID = WrkDatasetSTAFFLEVEL.ServicePointID
	,Value = WrkDatasetSTAFFLEVEL.NonRegisteredNursePlan
from
	dbo.WrkDatasetSTAFFLEVEL

inner join dbo.Dataset
on	Dataset.DatasetCode = 'STAFFLEVEL'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.NursingWorkforceNonRegisteredNursePlanNightHours'
	
where
	WrkDatasetSTAFFLEVEL.SourceShift = 'Nhours'







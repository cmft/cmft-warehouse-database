﻿






CREATE view [Denominator].[InpatientElectiveAdmissionExcludingMentalHealth]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.StartSiteID
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset Dataset
on	DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientElectiveAdmissionExcludingMentalHealth'

where
	NationalLastEpisodeInSpellCode = 1
and PatientCategoryCode in ('EL', 'DC')
and	WrkDatasetAPC.NationalSpecialtyCode not like '7%' 








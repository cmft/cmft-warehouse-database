﻿CREATE view [Denominator].[SESS210MinSessions] as

-- Derive standard 210 minute Sessions
select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSESS.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetSESS.SiteID		
	,DirectorateID = WrkDatasetSESS.DirectorateID
	,SpecialtyID = WrkDatasetSESS.SpecialtyID
	,ClinicianID = WrkDatasetSESS.ClinicianID
	,DateID = WrkDatasetSESS.SessionDateID
	,ServicePointID
	,Value = cast(Duration as decimal(19,4)) / 210 -- Actual session length divided by 210

from
	dbo.WrkDatasetSESS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'SESS'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.SESS210MinSession'
	
where 
	CancelledFlag = 0

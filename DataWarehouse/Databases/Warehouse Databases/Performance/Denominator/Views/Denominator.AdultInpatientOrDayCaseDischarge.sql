﻿
CREATE view [Denominator].[AdultInpatientOrDayCaseDischarge]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.AdultInpatientOrDayCaseDischarge'

where
	NationalLastEpisodeInSpellCode = 1
and	datediff(yy,WrkDatasetAPC.DateOfBirth,WrkDatasetAPC.AdmissionDate) >= 18
and	PatientCategoryCode in ('DC','EL','NE')



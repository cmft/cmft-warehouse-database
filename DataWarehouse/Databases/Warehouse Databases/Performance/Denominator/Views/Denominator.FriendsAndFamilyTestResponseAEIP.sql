﻿




CREATE view [Denominator].[FriendsAndFamilyTestResponseAEIP] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFFTRTN.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetFFTRTN.SiteID		
	,DirectorateID = WrkDatasetFFTRTN.DirectorateID
	,SpecialtyID = WrkDatasetFFTRTN.SpecialtyID
	,ClinicianID = WrkDatasetFFTRTN.ClinicianID
	,DateID = WrkDatasetFFTRTN.CensusDateID
	,ServicePointID = WrkDatasetFFTRTN.ServicePointID
	,Value = WrkDatasetFFTRTN.Responses
from
	dbo.WrkDatasetFFTRTN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FFTRTN'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.FriendsAndFamilyTestResponseAEIP'
	
where
	FFTReturnType in ('AE','IP')





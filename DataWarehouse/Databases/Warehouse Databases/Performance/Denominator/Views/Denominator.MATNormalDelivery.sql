﻿CREATE view [Denominator].[MATNormalDelivery]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetMAT.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetMAT.SiteID		
	,DirectorateID = WrkDatasetMAT.DirectorateID
	,SpecialtyID = WrkDatasetMAT.SpecialtyID
	,ClinicianID = WrkDatasetMAT.ClinicianID
	,DateID = WrkDatasetMAT.InfantDateOfBirthID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetMAT

inner join dbo.Dataset
on	DatasetCode = 'MAT'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.MATNormalDelivery'
	
where 
	BirthOrder = '1'
and MethodDelivery = 'S'

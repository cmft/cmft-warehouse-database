﻿CREATE view [Denominator].[RENQDialysisPrimaryInHospitalOrSatellite] as

-- Patients on dialysis in hospital or satellite setting where setting is primary

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRENQ.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetRENQ.SiteID		
	,DirectorateID = WrkDatasetRENQ.DirectorateID
	,SpecialtyID = WrkDatasetRENQ.SpecialtyID
	,ClinicianID = WrkDatasetRENQ.ClinicianID
	,DateID = WrkDatasetRENQ.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetRENQ

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RENQ'

inner join dbo.Denominator
on DenominatorLogic = 'Denominator.RENQDialysisPrimaryInHospitalOrSatellite'

where 
	ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55433','90:55435') -- Hospital or Satellite
and EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients' 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and DateOfDeath is null
and IsPrimary = 1

﻿






CREATE view [Denominator].HRQualifiedNursingMidwiferyGeneralLedgerEst
as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = WrkDatasetHR.GeneralLedgerEst

from
	dbo.WrkDatasetHR

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.HRQualifiedNursingMidwiferyGeneralLedgerEst'

where
	WrkDatasetHR.GeneralLedgerEst is not null









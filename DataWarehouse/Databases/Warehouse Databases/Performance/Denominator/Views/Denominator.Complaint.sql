﻿






CREATE view [Denominator].[Complaint] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetCOMP.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetCOMP.SiteID		
	,DirectorateID = WrkDatasetCOMP.DirectorateID
	,SpecialtyID = WrkDatasetCOMP.SpecialtyID
	,ClinicianID = WrkDatasetCOMP.ClinicianID
	,DateID = WrkDatasetCOMP.ReceiptDateID
	,WrkDatasetCOMP.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetCOMP

inner join dbo.Dataset
on	Dataset.DatasetCode = 'COMP'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.Complaint'
	
where
	WrkDatasetCOMP.CaseTypeCode in ('0x4120','0x4A20') --formal complaint and acute trust complaint
and	SequenceNo = 1







﻿

CREATE view [Denominator].[APCAdmissionUsingFirstEpisode]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	--,SiteID = WrkDatasetAPC.EndSiteID  -- RR this dataset includes patients who are still in, but EndSite,EndDirectorate come from the Discharge, therefore null, so use Start, relating to the Admission
	,SiteID = WrkDatasetAPC.StartSiteID  
	--,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCAdmissionUsingFirstEpisode'

where
	FirstEpisodeInSpellIndicator = 1



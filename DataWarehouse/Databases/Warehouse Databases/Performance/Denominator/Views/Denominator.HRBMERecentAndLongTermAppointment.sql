﻿



CREATE view [Denominator].[HRBMERecentAndLongTermAppointment]
as

-- 20160119	RR	A change was made to the HR file.  Jonathan Hodges advised that the indicator should remain the same but the calculation should be Headcounter Period End / Headcount Period Start

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = coalesce(WrkDatasetHR.BMERecentAppointment,0) + coalesce(WrkDatasetHR.BMELongTermAppointment,0)

from
	dbo.WrkDatasetHR 

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.HRBMERecentAndLongTermAppointment'

where
	WrkDatasetHR.BMERecentAppointment is not null
or	WrkDatasetHR.BMELongTermAppointment is not null










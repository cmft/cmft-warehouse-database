﻿



CREATE view [Denominator].[OvernightInpatientBeds] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetBEDOCC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetBEDOCC.SiteID		
	,DirectorateID = WrkDatasetBEDOCC.DirectorateID
	,SpecialtyID = WrkDatasetBEDOCC.SpecialtyID
	,ClinicianID = WrkDatasetBEDOCC.ClinicianID
	,DateID = WrkDatasetBEDOCC.DateID
	,ServicePointID
	,Value = WrkDatasetBEDOCC.AvailableInpatientBeds

from
	dbo.WrkDatasetBEDOCC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'BEDOCC'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OvernightInpatientBeds'
	
where	
	WrkDatasetBEDOCC.AvailableInpatientBeds is not null
and WrkDatasetBEDOCC.BedOccupancyNights is null





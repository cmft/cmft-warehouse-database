﻿


CREATE view [Denominator].[FinishedConsultantEpisodeExcludingDayCaseRegularDayNight]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.StartSiteID
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeEndDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.FinishedConsultantEpisodeExcludingDayCaseRegularDayNight'

where
	WrkDatasetAPC.EpisodeEndDate is not null
and	WrkDatasetAPC.NationalPatientClassificationCode not in ('2','3','4')




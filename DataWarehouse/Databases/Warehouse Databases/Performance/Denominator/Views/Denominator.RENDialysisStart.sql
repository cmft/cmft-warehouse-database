﻿CREATE view [Denominator].[RENDialysisStart] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetREN.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetREN.SiteID		
	,DirectorateID = WrkDatasetREN.DirectorateID
	,SpecialtyID = WrkDatasetREN.SpecialtyID
	,ClinicianID = WrkDatasetREN.ClinicianID
	,DateID = WrkDatasetREN.StartDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetREN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'REN'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.RENDialysisStart'
	
where
	ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients' 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'

﻿

create view Denominator.RTTPathwayClosedNonAdmitted as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRTT.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetRTT.SiteID
	,DirectorateID = WrkDatasetRTT.DirectorateID
	,SpecialtyID = WrkDatasetRTT.SpecialtyID
	,ClinicianID = WrkDatasetRTT.ClinicianID
	,DateID = WrkDatasetRTT.CensusDateID
	,WrkDatasetRTT.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetRTT

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RTT'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.RTTPathwayClosedNonAdmitted'

where
	WrkDatasetRTT.PathwayStatusCode = 'OPT'
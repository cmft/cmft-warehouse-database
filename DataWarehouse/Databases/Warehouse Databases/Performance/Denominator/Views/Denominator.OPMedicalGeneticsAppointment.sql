﻿CREATE view [Denominator].[OPMedicalGeneticsAppointment]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOP.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetOP.SiteID   
	,DirectorateID = WrkDatasetOP.DirectorateID
	,SpecialtyID = WrkDatasetOP.SpecialtyID
	,ClinicianID = WrkDatasetOP.ClinicianID
	,DateID = WrkDatasetOP.AppointmentDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetOP

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'OP'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OPMedicalGeneticsAppointment'

where
	WrkDatasetOP.NationalSpecialtyCode = '311'

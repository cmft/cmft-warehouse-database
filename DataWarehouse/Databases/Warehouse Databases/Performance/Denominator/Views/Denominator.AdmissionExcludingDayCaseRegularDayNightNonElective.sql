﻿




CREATE view [Denominator].[AdmissionExcludingDayCaseRegularDayNightNonElective]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.StartSiteID		
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.AdmissionExcludingDayCaseRegularDayNightNonElective'

where
	FirstEpisodeInSpellIndicator = 1
and	NationalPatientClassificationCode not in ('2','3','4')
and	PatientCategoryCode = 'NE'






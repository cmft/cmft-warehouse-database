﻿create view [Denominator].[APCDischargeUsingFirstEpisodeIpDcOver18] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCDischargeUsingFirstEpisodeIpDcOver18'
	
where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.DischargeTime is not null

and	WrkDatasetAPC.PatientCategoryCode not in ('RD','RN')
and	datediff(year,WrkDatasetAPC.DateOfBirth,WrkDatasetAPC.AdmissionDate) >= 18



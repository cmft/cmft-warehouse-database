﻿
CREATE view [Denominator].[WSAdmissionToMAUAgeOver65] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetWS.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetWS.StartSiteID
	,DirectorateID = WrkDatasetWS.StartDirectorateID
	,SpecialtyID = WrkDatasetWS.SpecialtyID
	,ClinicianID = WrkDatasetWS.ClinicianID
	,DateID = WrkDatasetWS.StartDateID
	,ServicePointID = WrkDatasetWS.ServicePointID
	,Value = 1

from
	dbo.WrkDatasetWS

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'WS'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.WSAdmissionToMAUAgeOver65'

where 
	StartWardCode in ('MAU','AMU')
and convert(int,round(datediff(hour,DateOfBirth,AdmissionTime)/8766,0)) >= 65


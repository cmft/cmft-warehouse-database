﻿







CREATE view [Denominator].ObservationSet as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOBSET.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetOBSET.SiteID		
	,DirectorateID = WrkDatasetOBSET.DirectorateID
	,SpecialtyID = WrkDatasetOBSET.SpecialtyID
	,ClinicianID = WrkDatasetOBSET.ClinicianID
	,DateID = WrkDatasetOBSET.StartDateID
	,ServicePointID = WrkDatasetOBSET.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetOBSET

inner join dbo.Dataset
on	DatasetCode = 'OBSET'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.ObservationSet'
	
﻿






CREATE view Denominator.Alert as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetALERT.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetALERT.SiteID		
	,DirectorateID = WrkDatasetALERT.DirectorateID
	,SpecialtyID = WrkDatasetALERT.SpecialtyID
	,ClinicianID = WrkDatasetALERT.ClinicianID
	,DateID = WrkDatasetALERT.CreatedDateID
	,WrkDatasetALERT.ServicePointID
	,Value = 1
from
	dbo.WrkDatasetALERT

inner join dbo.Dataset
on	Dataset.DatasetCode = 'ALERT'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.Alert'
	
	






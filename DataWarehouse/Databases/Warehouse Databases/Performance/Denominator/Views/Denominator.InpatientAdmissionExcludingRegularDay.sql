﻿
CREATE view [Denominator].[InpatientAdmissionExcludingRegularDay]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SiteID = WrkDatasetAPC.StartSiteID   
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.NationalPatientClassificationCode <> '3'


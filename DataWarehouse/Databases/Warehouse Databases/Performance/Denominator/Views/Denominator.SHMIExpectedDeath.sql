﻿CREATE view [Denominator].[SHMIExpectedDeath]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = RiskScore
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset 
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.SHMIExpectedDeath'

where
	WrkDatasetAPC.DominantForDiagnosis = 1

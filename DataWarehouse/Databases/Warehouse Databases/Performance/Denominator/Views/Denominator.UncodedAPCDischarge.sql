﻿CREATE view [Denominator].[UncodedAPCDischarge]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.UncodedAPCDischarge'
	
where
	FirstEpisodeInSpellIndicator = 1
and	DischargeDate is not null
and	ClinicalCodingCompleteDate is null

﻿CREATE view [Denominator].[MATDelivery]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetMAT.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetMAT.SiteID		
	,DirectorateID = WrkDatasetMAT.DirectorateID
	,SpecialtyID = WrkDatasetMAT.SpecialtyID
	,ClinicianID = WrkDatasetMAT.ClinicianID
	,DateID = WrkDatasetMAT.InfantDateOfBirthID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetMAT

inner join dbo.Dataset
on	DatasetCode = 'MAT'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.MATDelivery'
	
where DatasetCode = 'MAT'
and BirthOrder = '1'

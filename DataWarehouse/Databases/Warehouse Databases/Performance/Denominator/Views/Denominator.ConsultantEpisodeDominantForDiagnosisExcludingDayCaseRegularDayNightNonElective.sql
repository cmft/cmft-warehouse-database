﻿


CREATE view [Denominator].[ConsultantEpisodeDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.EpisodeStartDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.ConsultantEpisodeDominantForDiagnosisExcludingDayCaseRegularDayNightNonElective'

where
	WrkDatasetAPC.DominantForDiagnosis = 1
and	WrkDatasetAPC.NationalPatientClassificationCode not in ('2','3','4')
and	WrkDatasetAPC.PatientCategoryCode = 'NE'




﻿

CREATE view [Denominator].[InpatientSpellWithProcedure]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.StartSiteID	
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset Dataset
on	DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientSpellWithProcedure'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and WrkDatasetAPC.FirstOperationInSpellTime is not null
and WrkDatasetAPC.DischargeTime is not null




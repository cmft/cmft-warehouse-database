﻿






CREATE view [Denominator].HRQualifiedNursingMidwiferyGeneralLedgerEstBandFive
as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = WrkDatasetHR.GeneralLedgerEstBand5
from
	dbo.WrkDatasetHR
	 
inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.HRQualifiedNursingMidwiferyGeneralLedgerEstBandFive'

where
	Dataset.DatasetCode = 'HR'
and WrkDatasetHR.GeneralLedgerEstBand5 is not null









﻿





CREATE view [Denominator].[HRBudgetWTE]
as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetHR.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetHR.SiteID
	,DirectorateID = WrkDatasetHR.DirectorateID
	,SpecialtyID = WrkDatasetHR.SpecialtyID
	,ClinicianID = WrkDatasetHR.ClinicianID
	,DateID = WrkDatasetHR.CensusDateID
	,ServicePointID = WrkDatasetHR.ServicePointID
	,Value = WrkDatasetHR.ClinicalBudgetWTE
from
	dbo.WrkDatasetHR

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'HR'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.HRBudgetWTE'

where
	WrkDatasetHR.ClinicalBudgetWTE is not null








﻿


CREATE view [Denominator].[OPWLCBacklog] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetOPWLC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetOPWLC.SiteID   
	,DirectorateID = WrkDatasetOPWLC.DirectorateID
	,SpecialtyID = WrkDatasetOPWLC.SpecialtyID
	,ClinicianID = WrkDatasetOPWLC.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDatasetOPWLC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'OPWLC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.OPWLCBacklog'

where 
	WrkDatasetOPWLC.NationalFirstAttendanceCode = '1'
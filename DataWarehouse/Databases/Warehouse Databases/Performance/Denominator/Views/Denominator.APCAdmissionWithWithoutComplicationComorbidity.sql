﻿
CREATE view [Denominator].[APCAdmissionWithWithoutComplicationComorbidity]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID  
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.AdmissionDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.APCAdmissionWithWithoutComplicationComorbidity'

where Dataset.DatasetCode = 'APC'
and WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and WrkDatasetAPC.DischargeDate is not null
--and WrkDatasetAPC.HRGWithCCCode in ('WITH','WITHOUT') -- HRG with or without complications or co-morbidities (excludes NONE) -- 26/02/2016 RR Discussed with DG and IC, and denominator description, need to include none, ie denominator all admissions
and WrkDatasetAPC.PatientClassificationCode in ('1','2')
﻿
CREATE view Denominator.CANTPriorityUpgradeHA as

-- Based on SP CancerRegister.dbo.RPT_UPGRADE_TREAT

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.TreatmentDateID
	,ServicePointID = D.ServicePointID
	,Value = 1

from dbo.WrkDataset D

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.CANTPriorityUpgradeHA'
	
where DatasetCode = 'CANT'
and D.TumourStatusCode not in ('4', '5')
and coalesce(D.TreatmentNo,0) <> 2
and coalesce(D.InappropriateReferralCode,0) <> '1'
and ConsultantUpgradeDate is not null

and (D.PrimaryDiagnosisCode1 is null or D.PrimaryDiagnosisCode1 not in ('C62', 'C620', 'C621', 'C629', 'C910', 'C920', 'C924', 'C925', 'C930', 'C942', 'C950'))

and not
(
	coalesce(D.PrimaryDiagnosisCode,'') = 'C44' 
	and coalesce(D.HistologyCode,'') in ('M80903','M80913','M80923','M80933','M80943','M80953','M81103')
)

and not 
(
	coalesce(D.PrimaryDiagnosisCode,'') <> 'D05' 
	and left(coalesce(D.PrimaryDiagnosisCode,''),1) = 'D'
)

and CancerSite = 'Haematology'

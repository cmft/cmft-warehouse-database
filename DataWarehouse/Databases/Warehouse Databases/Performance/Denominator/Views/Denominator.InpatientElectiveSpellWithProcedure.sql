﻿


CREATE view [Denominator].[InpatientElectiveSpellWithProcedure]

as

--select
--	Template = 'Not in use'

select
	DatasetID = Base.DatasetID
	,DatasetRecno = Base.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = Base.StartSiteID	
	,DirectorateID = Base.StartDirectorateID
	,SpecialtyID = Base.SpecialtyID
	,ClinicianID = Base.ClinicianID
	,DateID = Base.AdmissionDateID
	,ServicePointID = Base.ServicePointID
	,Value = 1
from
	dbo.WrkDataset Base

inner join dbo.Dataset Dataset
on	Base.DatasetID = Dataset.DatasetID

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientElectiveSpellWithProcedure'

where
	DatasetCode = 'APC'
--and
--	ClinicalCodingCompleteDate is not null
and 
	FirstEpisodeInSpellIndicator = 1
and 
	PatientCategoryCode = 'EL'
and
	/* gets first FCE only where there is a coded procedure in the spell */

	exists
		(
		select
			1
		from
			dbo.WrkDataset Operation
		inner join
			dbo.Dataset
		on	Dataset.DatasetID = Base.DatasetID

		where
			DatasetCode = 'APC'
		and
			PrimaryProcedureDate is not null
		and
			PrimaryProcedureCode is not null
		and
			Operation.ProviderSpellNo = Base.ProviderSpellNo
		)





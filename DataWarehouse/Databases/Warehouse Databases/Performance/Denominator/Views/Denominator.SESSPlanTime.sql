﻿CREATE view [Denominator].[SESSPlanTime] as

-- Planned Duration: excludes cancelled sessions

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSESS.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetSESS.SiteID		
	,DirectorateID = WrkDatasetSESS.DirectorateID
	,SpecialtyID = WrkDatasetSESS.SpecialtyID
	,ClinicianID = WrkDatasetSESS.ClinicianID
	,DateID = WrkDatasetSESS.SessionDateID
	,ServicePointID
	,Value = WrkDatasetSESS.PlannedDuration

from
	dbo.WrkDatasetSESS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'SESS'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.SESSPlanTime'
	
where
	CancelledFlag = 0

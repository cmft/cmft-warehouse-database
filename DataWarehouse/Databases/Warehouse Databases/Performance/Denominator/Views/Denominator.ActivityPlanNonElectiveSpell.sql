﻿

CREATE view [Denominator].[ActivityPlanNonElectiveSpell] as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetPLAN.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetPLAN.SiteID		
	,DirectorateID = WrkDatasetPLAN.DirectorateID
	,SpecialtyID = WrkDatasetPLAN.SpecialtyID
	,ClinicianID = WrkDatasetPLAN.ClinicianID
	,DateID = WrkDatasetPLAN.ActivityDateID
	,ServicePointID = WrkDatasetPLAN.ServicePointID
	,Value = WrkDatasetPLAN.Cases

from
	dbo.WrkDatasetPLAN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'ACTIVITY'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.ActivityPlanNonElectiveSpell'
	
where
	WrkDatasetPLAN.ActivityMetricCode in 
										(
										'EMSPL'
										,'MATSPL'
										,'NEOTHSPL'
										) -- Non-Elective Spells




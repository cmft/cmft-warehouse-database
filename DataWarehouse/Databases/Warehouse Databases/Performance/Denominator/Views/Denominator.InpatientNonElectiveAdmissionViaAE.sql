﻿

CREATE view [Denominator].[InpatientNonElectiveAdmissionViaAE]

as

select
       DatasetID = Dataset.DatasetID
       ,DatasetRecno = WrkDatasetAPC.DatasetRecno
       ,DenominatorID = Denominator.DenominatorID
       ,SiteID = WrkDatasetAPC.EndSiteID
       ,DirectorateID = WrkDatasetAPC.EndDirectorateID
       ,SpecialtyID = WrkDatasetAPC.SpecialtyID
       ,ClinicianID = WrkDatasetAPC.ClinicianID
       ,DateID = WrkDatasetAPC.DischargeDateID
       ,ServicePointID = WrkDatasetAPC.StartServicePointID
       ,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset Dataset
on	DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientNonElectiveAdmissionViaAE'

where 
	NationalLastEpisodeInSpellCode = 1 -- why not first?  Keeping as is to keep consitent with other queries
and	PatientCategoryCode = 'NE'
and	AdmissionMethodCode = 21
and	DischargeDate is not null

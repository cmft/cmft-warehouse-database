﻿


CREATE view [Denominator].[InpatientAdmissionEligibleForEDD]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.StartSiteID   
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID -- 02/02/2016 RR This is being used for LoS, was Admission Date, but numerator discharge date, agreed with ML/GS/DG to use Discharge date and be consistent across numerator and denominator.
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1 
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientAdmissionEligibleForEDD'

where
	WrkDatasetAPC.FirstEpisodeInSpellIndicator = 1
and	WrkDatasetAPC.NationalIntendedManagementCode not in ('3','4')
and	WrkDatasetAPC.NationalSpecialtyCode <> '501'
and	WrkDatasetAPC.Specialty not like '%DIALYSIS%'
and	left(WrkDatasetAPC.SpecialtyCode, 2) <> 'IH'
and	WrkDatasetAPC.StartWardCode <> '76A'	
and	left(WrkDatasetAPC.StartWardCode, 3) <> 'SUB'
and	not 
	(
		WrkDatasetAPC.StartWardCode = 'ESTU' 
	and datediff(hour, WrkDatasetAPC.AdmissionTime, WrkDatasetAPC.DischargeTime) < 6
	)



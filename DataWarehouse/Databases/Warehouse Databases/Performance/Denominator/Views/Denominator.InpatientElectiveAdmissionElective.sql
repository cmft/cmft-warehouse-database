﻿





CREATE view [Denominator].[InpatientElectiveAdmissionElective]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.StartSiteID
	,DirectorateID = WrkDatasetAPC.StartDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID -- 02/02/2016 RR This is being used for LoS, was Admission Date, but numerator discharge date, agreed with ML/GS/DG to use Discharge date and be consistent across numerator and denominator.
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset Dataset
on	DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientElectiveAdmissionElective'

where
	NationalLastEpisodeInSpellCode = 1
and	PatientCategoryCode = 'EL'






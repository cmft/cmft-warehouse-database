﻿CREATE view [Denominator].[QCREndOfLifeQuestion] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetQCR.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetQCR.SiteID		
	,DirectorateID = WrkDatasetQCR.DirectorateID
	,SpecialtyID = WrkDatasetQCR.SpecialtyID
	,ClinicianID = WrkDatasetQCR.ClinicianID
	,DateID = WrkDatasetQCR.AuditDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetQCR

inner join dbo.Dataset
on	Dataset.DatasetCode = 'QCR'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.QCREndOfLifeQuestion'
	
where
	CustomListCode = 5 -- End of Life

﻿CREATE view [Denominator].[PATREGPatientRegistration] as

select
	DatasetID = D.DatasetID
	,DatasetRecno = D.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = D.EndSiteID		
	,DirectorateID = D.EndDirectorateID
	,SpecialtyID = D.SpecialtyID
	,ClinicianID = D.ClinicianID
	,DateID = D.RegistrationDateID
	,ServicePointID
	,Value = 1
from WrkDataset D
--with (index(DatasetID_SourcePatientNo))

inner join dbo.Dataset
on	Dataset.DatasetID = D.DatasetID

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.PATREGPatientRegistration'
	
where DatasetCode = 'PATREG'


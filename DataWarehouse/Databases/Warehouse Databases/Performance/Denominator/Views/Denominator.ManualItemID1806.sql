﻿
CREATE view Denominator.ManualItemID1806 as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetMAN.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetMAN.SiteID		
	,DirectorateID = WrkDatasetMAN.DirectorateID
	,SpecialtyID = WrkDatasetMAN.SpecialtyID
	,ClinicianID = WrkDatasetMAN.ClinicianID
	,DateID = WrkDatasetMAN.DateID
	,ServicePointID = WrkDatasetMAN.ServicePointID
	,Value = WrkDatasetMAN.Denominator

from
	dbo.WrkDatasetMAN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'MAN'
inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.ManualItemID1806'
	
where
	ItemID = '1806'
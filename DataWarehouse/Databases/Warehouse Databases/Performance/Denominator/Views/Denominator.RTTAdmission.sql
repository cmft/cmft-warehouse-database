﻿
CREATE view [Denominator].[RTTAdmission] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRTT.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetRTT.SiteID
	,DirectorateID = WrkDatasetRTT.DirectorateID
	,SpecialtyID = WrkDatasetRTT.SpecialtyID
	,ClinicianID = WrkDatasetRTT.ClinicianID
	,DateID = WrkDatasetRTT.CensusDateID
	,ServicePointID
	,Value = 1
from
	dbo.WrkDatasetRTT

inner join dbo.Dataset Dataset
on Dataset.DatasetCode = 'RTT'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.RTTAdmission'

where 
	WrkDatasetRTT.PathwayStatusCode = 'ACS'
--and WrkDatasetRTT.AdjustedFlag = 'Y'


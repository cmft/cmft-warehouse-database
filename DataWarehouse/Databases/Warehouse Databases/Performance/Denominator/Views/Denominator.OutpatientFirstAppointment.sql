﻿
CREATE view [Denominator].[OutpatientFirstAppointment]

as

select
       DatasetID = Dataset.DatasetID
       ,DatasetRecno = WrkDatasetOP.DatasetRecno
       ,DenominatorID = Denominator.DenominatorID
       ,SiteID = WrkDatasetOP.SiteID   
       ,DirectorateID = WrkDatasetOP.DirectorateID
       ,SpecialtyID = WrkDatasetOP.SpecialtyID
       ,ClinicianID = WrkDatasetOP.ClinicianID
       ,DateID = WrkDatasetOP.AppointmentDateID
       ,ServicePointID
       ,Value = 1

from
	dbo.WrkDatasetOP

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'OP'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OutpatientFirstAppointment'

where
	WrkDatasetOP.NationalFirstAttendanceCode in ('1','3') -- First Appointment
and	WrkDatasetOP.NationalAttendanceStatusCode not in (2,4) --Cancellations

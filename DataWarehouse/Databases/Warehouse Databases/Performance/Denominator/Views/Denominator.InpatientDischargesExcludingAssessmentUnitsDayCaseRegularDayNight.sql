﻿



CREATE view [Denominator].[InpatientDischargesExcludingAssessmentUnitsDayCaseRegularDayNight]

as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.StartServicePointID
	,Value = 1
from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.InpatientDischargesExcludingAssessmentUnitsDayCaseRegularDayNight'

where
	FirstEpisodeInSpellIndicator = 1  -- To only count the spell LoS, and no duplicates as table is at episode level
and	NationalPatientClassificationCode not in ('2','3','4') -- Exclude DC, RegDay, RegNight
and	DischargeDate is not null -- LoS, only need completed spells
and	NationalDischargeMethodCode not in (5) -- Exclude Stillbirth
and WrkDatasetAPC.HasAssessmentUnitEpisode = 0
and	WrkDatasetAPC.IsWellBabySpell = 0



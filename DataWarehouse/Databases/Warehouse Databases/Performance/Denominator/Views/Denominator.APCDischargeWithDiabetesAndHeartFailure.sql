﻿create view [Denominator].[APCDischargeWithDiabetesAndHeartFailure] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetAPC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetAPC.EndSiteID		
	,DirectorateID = WrkDatasetAPC.EndDirectorateID
	,SpecialtyID = WrkDatasetAPC.SpecialtyID
	,ClinicianID = WrkDatasetAPC.ClinicianID
	,DateID = WrkDatasetAPC.DischargeDateID
	,ServicePointID = WrkDatasetAPC.EndServicePointID
	,Value = 1

from
	dbo.WrkDatasetAPC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'APC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.APCDischargeWithDiabetesAndHeartFailure'

where
	WrkDatasetAPC.NationalLastEpisodeInSpellCode = 1 
and left(WrkDatasetAPC.PrimaryDiagnosisCode,3) = 'I50' -- Heart failure
and	(
	left(WrkDatasetAPC.SecondaryDiagnosisCode1,3) between 'E08' and 'E13' -- Diabetes
	or left(WrkDatasetAPC.SecondaryDiagnosisCode2,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode3,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode4,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode5,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode6,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode7,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode8,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode9,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode10,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode11,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode12,3) between 'E08' and 'E13'
	or left(WrkDatasetAPC.SecondaryDiagnosisCode13,3) between 'E08' and 'E13'			
	)

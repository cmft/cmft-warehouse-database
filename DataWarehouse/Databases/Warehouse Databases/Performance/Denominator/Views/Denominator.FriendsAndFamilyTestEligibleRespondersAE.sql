﻿

Create view Denominator.FriendsAndFamilyTestEligibleRespondersAE as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFFTRTN.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetFFTRTN.SiteID		
	,DirectorateID = WrkDatasetFFTRTN.DirectorateID
	,SpecialtyID = WrkDatasetFFTRTN.SpecialtyID
	,ClinicianID = WrkDatasetFFTRTN.ClinicianID
	,DateID = WrkDatasetFFTRTN.CensusDateID
	,ServicePointID = WrkDatasetFFTRTN.ServicePointID
	,Value = WrkDatasetFFTRTN.EligibleResponders
from
	dbo.WrkDatasetFFTRTN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FFTRTN'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.FriendsAndFamilyTestEligibleRespondersAE'
	
where
	FFTReturnType = 'AE'



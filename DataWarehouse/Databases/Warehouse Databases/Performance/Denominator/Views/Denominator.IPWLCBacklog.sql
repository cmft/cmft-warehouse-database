﻿

CREATE view [Denominator].[IPWLCBacklog] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetIPWLC.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetIPWLC.SiteID   
	,DirectorateID = WrkDatasetIPWLC.DirectorateID
	,SpecialtyID = WrkDatasetIPWLC.SpecialtyID
	,ClinicianID = WrkDatasetIPWLC.ClinicianID
	,DateID = CensusDateID -- Census Date
	,ServicePointID
	,Value = 1
from 
	dbo.WrkDatasetIPWLC

inner join dbo.Dataset
on	Dataset.DatasetCode = 'IPWLC'

inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.IPWLCBacklog'

	
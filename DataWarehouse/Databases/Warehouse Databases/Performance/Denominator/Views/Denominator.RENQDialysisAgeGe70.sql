﻿CREATE view [Denominator].[RENQDialysisAgeGe70] as

-- Patients on dialysis at quarter end, age >= 70

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRENQ.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetRENQ.SiteID		
	,DirectorateID = WrkDatasetRENQ.DirectorateID
	,SpecialtyID = WrkDatasetRENQ.SpecialtyID
	,ClinicianID = WrkDatasetRENQ.ClinicianID
	,DateID = WrkDatasetRENQ.LastDayOfQuarterID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetRENQ

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RENQ'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.RENQDialysisAgeGe70'

where 
	ModalityCode = '90:55425' -- HD
and ModalitySettingCode in ('90:55435','90:55433') -- Hospital or Satellite
and EventDetailCode not in ('90:8000168','90:8000169','90:8000170','90:8000171') -- 'HD - Dialysis Inpatients' 'HDF - Dialysis Inpatients', 'HD - Ward - Outside Sector', 'HDF - Ward - Outside sector'
and convert(int,round(datediff(hour,WrkDatasetRENQ.DateOfBirth,WrkDatasetRENQ.LastDayOfQuarter)/8766,0)) >= 70

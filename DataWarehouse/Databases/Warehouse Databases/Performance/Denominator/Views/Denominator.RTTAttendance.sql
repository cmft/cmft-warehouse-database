﻿create view [Denominator].[RTTAttendance] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetRTT.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetRTT.SiteID
	,DirectorateID = WrkDatasetRTT.DirectorateID
	,SpecialtyID = WrkDatasetRTT.SpecialtyID
	,ClinicianID = WrkDatasetRTT.ClinicianID
	,DateID = WrkDatasetRTT.CensusDateID
	,ServicePointID
	,Value = 1
	,CensusDate
from
	dbo.WrkDatasetRTT

inner join dbo.Dataset
on	Dataset.DatasetCode = 'RTT'
inner join dbo.Denominator
on	Denominator.DenominatorLogic = 'Denominator.RTTAttendance'

where
	WrkDatasetRTT.PathwayStatusCode = 'OPT'
--and WrkDatasetRTT.AdjustedFlag = 'N'

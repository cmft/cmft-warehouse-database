﻿CREATE view [Denominator].[SESSSessionNotCancelled] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetSESS.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetSESS.SiteID		
	,DirectorateID = WrkDatasetSESS.DirectorateID
	,SpecialtyID = WrkDatasetSESS.SpecialtyID
	,ClinicianID = WrkDatasetSESS.ClinicianID
	,DateID = WrkDatasetSESS.SessionDateID
	,ServicePointID
	,Value = 1

from
	dbo.WrkDatasetSESS

inner join dbo.Dataset
on	Dataset.DatasetCode = 'SESS'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.SESSSessionNotCancelled'
	
where
	CancelledFlag = 0 -- Exclude cancelled sessions

﻿
CREATE view Denominator.ManualItemID875 as

select
	 DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetMAN.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetMAN.SiteID		
	,DirectorateID = WrkDatasetMAN.DirectorateID
	,SpecialtyID = WrkDatasetMAN.SpecialtyID
	,ClinicianID = WrkDatasetMAN.ClinicianID
	,DateID = WrkDatasetMAN.DateID
	,ServicePointID = WrkDatasetMAN.ServicePointID
	,Value = WrkDatasetMAN.Denominator

from
	dbo.WrkDatasetMAN

inner join dbo.Dataset
on	Dataset.DatasetCode = 'MAN'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.ManualItemID875'
	
where
	ItemID = '875'
﻿
CREATE view [Denominator].[OutpatientFollowUpAppointment]

as

select
       DatasetID = Dataset.DatasetID
       ,DatasetRecno = WrkDatasetOP.DatasetRecno
       ,DenominatorID = Denominator.DenominatorID
       ,SiteID = WrkDatasetOP.SiteID   
       ,DirectorateID = WrkDatasetOP.DirectorateID
       ,SpecialtyID = WrkDatasetOP.SpecialtyID
       ,ClinicianID = WrkDatasetOP.ClinicianID
       ,DateID = WrkDatasetOP.AppointmentDateID
       ,ServicePointID
       ,Value = 1

from
	dbo.WrkDatasetOP

inner join dbo.Dataset Dataset
on	Dataset.DatasetCode = 'OP'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.OutpatientFollowUpAppointment'

where	
	WrkDatasetOP.NationalFirstAttendanceCode = '2' -- Follow-up Appointment
and	WrkDatasetOP.NationalAttendanceStatusCode not in (2,4) --Cancellations

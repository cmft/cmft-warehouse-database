﻿



CREATE view [Denominator].[FinancialExpenditureBudgetTrust] as

select
	DatasetID = Dataset.DatasetID
	,DatasetRecno = WrkDatasetFINANCE.DatasetRecno
	,DenominatorID = Denominator.DenominatorID
	,SiteID = WrkDatasetFINANCE.SiteID		
	,DirectorateID = WrkDatasetFINANCE.DirectorateID
	,SpecialtyID = WrkDatasetFINANCE.SpecialtyID
	,ClinicianID = WrkDatasetFINANCE.ClinicianID
	,DateID = WrkDatasetFINANCE.CensusDateID
	,ServicePointID = WrkDatasetFINANCE.ServicePointID
	,Value = WrkDatasetFINANCE.Budget
from 
	dbo.WrkDatasetFINANCE

inner join dbo.Dataset
on	Dataset.DatasetCode = 'FINANCE'

inner join dbo.Denominator
on	DenominatorLogic = 'Denominator.FinancialExpenditureBudgetTrust'

where
	DirectorateID = 15 -- Trust




﻿

CREATE view [Target].[CancerReferralSeenWithin2WeeksOfGPReferral]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.93

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.CancerReferralSeenWithin2WeeksOfGPReferral'

--where
--	TheDate between '1 apr 2009' and getdate()




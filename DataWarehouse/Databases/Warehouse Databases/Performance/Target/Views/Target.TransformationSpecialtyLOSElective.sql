﻿CREATE VIEW [Target].[TransformationSpecialtyLOSElective]
as


select
	 Target.TargetID
	,Calendar.DateID 
	,Specialty.NationalSpecialtyID
	,TargetRuleBase.Value
from
	dbo.Calendar
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.TransformationSpecialtyLOSElective'

inner join dbo.TargetRuleBase
on	TargetRuleBase.TargetID = Target.TargetID
and	TargetRuleBase.StartDate <= Calendar.TheDate
and not exists
		(
		select
			1
		from
			dbo.TargetRuleBase LaterRule
		where
			LaterRule.TargetID = TargetRuleBase.TargetID
		and	LaterRule.NationalSpecialtyCode = TargetRuleBase.NationalSpecialtyCode
		and	LaterRule.StartDate <= Calendar.TheDate
		and	LaterRule.StartDate > TargetRuleBase.StartDate
		)

inner join 
	(
	select distinct
		 NationalSpecialtyID
		,NationalSpecialtyCode
	from
		Specialty
	) Specialty
on	Specialty.NationalSpecialtyCode = TargetRuleBase.NationalSpecialtyCode

where
	TheDate < getdate()
﻿






CREATE view [Target].[FinancialExpenditureAnnualBudgetOphthalmology]

as

select
	TargetID = Target.TargetID
	,DateID = WrkDatasetFINANCE.CensusDateID
	,Value = WrkDatasetFINANCE.AnnualBudget

from
	dbo.WrkDatasetFINANCE

inner join dbo.Target
on	Target.TargetLogic = 'Target.FinancialExpenditureAnnualBudgetOphthalmology'

where
	DirectorateID = 9 -- Ophthalmology -DG - 21.10.14 - Following suit with the other templates set for these indicators: 'Denominator.FinancialExpenditureBudget<Division>
and AnnualBudget is not null










﻿






CREATE view [Target].[FinancialExpenditureAnnualBudgetSpecialistMedicine]

as

select
	TargetID = Target.TargetID
	,DateID = WrkDatasetFINANCE.CensusDateID
	,Value = WrkDatasetFINANCE.AnnualBudget

from
	dbo.WrkDatasetFINANCE

	
inner join dbo.Target
on	Target.TargetLogic = 'Target.FinancialExpenditureAnnualBudgetSpecialistMedicine'

where
	DirectorateID = 5 -- Specialist Medicine -DG - 21.10.14 - Following suit with the other templates set for these indicators: 'Denominator.FinancialExpenditureBudget<Division>
and AnnualBudget is not null










﻿






CREATE view [Target].[FinancialExpenditureAnnualBudgetTrafford]

as

select
	TargetID = Target.TargetID
	,DateID = WrkDatasetFINANCE.CensusDateID
	,Value = WrkDatasetFINANCE.AnnualBudget

from
	dbo.WrkDatasetFINANCE

inner join dbo.Target
on	Target.TargetLogic = 'Target.FinancialExpenditureAnnualBudgetTrafford'

where
	DirectorateID = 20 -- Trafford -DG - 21.10.14 - Following suit with the other templates set for theese indicators: 'Denominator.FinancialExpenditureBudget<Division>
and AnnualBudget is not null









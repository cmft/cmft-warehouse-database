﻿




CREATE view [Target].[InpatientBedOccupancy]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.87 --2014/15

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.InpatientBedOccupancy'






﻿




CREATE view [Target].[ClinicalAuditsReportingLimitedVeryLimitedAssurance]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0.678
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.ClinicalAuditsReportingLimitedVeryLimitedAssurance'

--where
--	TheDate between '1 apr 2009' and getdate()







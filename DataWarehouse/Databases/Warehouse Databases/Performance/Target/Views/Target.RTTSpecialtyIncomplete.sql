﻿






CREATE view [Target].[RTTSpecialtyIncomplete]

as

/****************************************************************************************
Modification History
====================
	
Date		Person			Description
====================================================================================
27/08/2014	Paul Egan		Initial Coding.
							Instead of hard coded target value, used dynamic value based on
							original denominator logic [Denominator].[ManualItemID879].
							This is because they wanted to see the numbers in the report
							instead of %. The target is dynamic as the number of qualifying
							specialties (> 50 pathways each month) varies.
02/09/2014	Paul Egan		Reverted, as changed to NON-compliant specialties.
*****************************************************************************************/


select
	TargetID = Target.TargetID
	,DateID 
	,Value = 0

from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.RTTSpecialtyIncomplete'

--where
--	TheDate between '1 apr 2009' and getdate()




/* Dynamic target based on number of compliant specialties */
----------select
----------	TargetID = Target.TargetID
----------	,D.DateID 
----------	,Value = D.Denominator
----------from
----------	dbo.WrkDataset D
	
----------	inner join dbo.Target
----------	on	Target.TargetLogic = 'Target.RTTSpecialtyIncomplete'

----------	inner join dbo.Dataset
----------	on	Dataset.DatasetID = D.DatasetID

----------	--inner join dbo.Calendar C
----------	--on C.DateID = D.DateID

----------where
----------	Dataset.DatasetCode = 'MAN'
----------	and D.ItemID = '879'

----------	--and	C.TheDate between '1 apr 2009' and getdate()






﻿



CREATE view [Target].[FFTIPResponseRate]

as


select
	TargetID = Target.TargetID
	,DateID 
	,Value = case 
				when TheDate <'20150101' then 0.25
				when TheDate <'20150301' then 0.3
				else 0.4
			end
from
	[dbo].[Calendar]
	
inner join dbo.Target
on	Target.TargetLogic = 'Target.FFTIPResponseRate'

where
	TheDate <'20150401' --end dated as taregt doesnt exist CH 11052015






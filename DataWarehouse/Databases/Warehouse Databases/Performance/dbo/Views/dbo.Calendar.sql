﻿







CREATE view [dbo].[Calendar] as

select
	 Calendar.DateID
	,Calendar.TheDate
	,Calendar.DayOfWeek
	,Calendar.DayOfWeekKey
	,Calendar.LongDate
	,Calendar.TheMonth
	,Calendar.MonthName
	,Calendar.FinancialQuarter
	,Calendar.FinancialYear
	,Calendar.FinancialMonthKey
	,Calendar.FinancialQuarterKey
	,Calendar.CalendarQuarter
	,Calendar.CalendarSemester
	,Calendar.CalendarYear

	,LastCompleteMonth = 
		case
		when LastCompleteMonth.FirstDayOfMonth = Calendar.TheDate
		then 1
		else 0
		end

		--case
		--when
		--	Calendar.TheDate = 
		--		dateadd(
		--			 month
		--			,-2
		--			,dateadd(
		--				 day
		--				,datepart(
		--					 day
		--					,getdate()
		--				) * -1 + 1
		--				,cast(getdate() as date)
		--			)
		--		) --mid month so go back to start of last month

		--then 1
		--else 0
		--end

	,Calendar.WeekNoKey
	,Calendar.WeekNo
	,Calendar.FirstDayOfWeek
	,Calendar.LastDayOfWeek

	,Calendar.FullMonthName 
		--datename(
		--	 month
		--	,TheDate
		--) + ' ' +
		--datename(
		--	 year
		--	,TheDate
		--)

	,IsFutureDate =
		case

		when Calendar.TheDate > LastCompleteMonth.LastDayOfMonth
		and	Calendar.TheDate <> '9999-12-31'

		then 1
		
		else 0
		end

	,LastCompleteMonthKey = 
		LastCompleteMonth.DateID

from
	dbo.CalendarBase Calendar

cross join
	(
	select
		 FirstDayOfMonth = Parameter.DateValue
		,LastDayOfMonth = dateadd(day, -1, dateadd(month, 1, Parameter.DateValue))
		,DateID = CalendarBase.DateID
	from
		dbo.Parameter
		
	inner join dbo.CalendarBase
	on	CalendarBase.TheDate = Parameter.DateValue
	
	where
		Parameter.Parameter = 'LASTCOMPLETEMONTH'
	) LastCompleteMonth











﻿





CREATE view [dbo].[ItemHierarchyFlat]

as

--ALTER proc [dbo].[BuildItemHierarchyFlat]

--as

--if OBJECT_ID('dbo.ItemHierarchyFlat') is not null
--drop table dbo.ItemHierarchyFlat


--declare @KeyLength int = 3;

with FindNodePaths 

as
	(
        select 
            NodeID = HierarchyID
            ,NodePath = cast (
							' ' 
                              + right(replicate ('0', 4) 
                              + cast(HierarchyID AS varchar (100)), 4) AS varchar (100)
                             )
        from	
			dbo.ItemHierarchy
        where
			ParentID is null
        
        union all
        
        select 
            NodeID = ItemHierarchy.HierarchyID, 
            NodePath = cast (
							NodePath + ' ' 
                              + right (replicate ('0', 4) 
                              + cast (ItemHierarchy.HierarchyID as varchar), 4) as varchar (100)
                              )
        from
			dbo.ItemHierarchy 

		inner join FindNodePaths Parent
		on	Parent.NodeID = ItemHierarchy.ParentID
    )
    
,ComputeParentNodeIdAtLevels 

as
	(
        select 
            NodeID = NodeID
            ,NodePath = NodePath
            ,ParentNodeIdAtLevel1 = substring(NodePath, 2, 4)
            ,ParentNodeIdAtLevel2 = substring(NodePath, (4 + 1) * 1 + 2, 4)
            ,ParentNodeIdAtLevel3 = substring(NodePath, (4 + 1) * 2 + 2, 4)
            ,ParentNodeIdAtLevel4 = substring(NodePath, (4 + 1) * 3 + 2, 4)
            ,ParentNodeIdAtLevel5 = substring(NodePath, (4 + 1) * 4 + 2, 4)
        from
			FindNodePaths
    )
select 
    HierarchyID   = C.NodeID
    ,ItemID = T.ItemID
	,Item = T.Item
    ,ItemType = T.ItemType
	,T.ReportOrder
    ,Level1 = T1.Item
    ,Level2 = T2.Item
    ,Level3 = T3.Item
    ,Level4 = T4.Item
    ,Level5 = T5.Item  
--into
--	dbo.ItemHierarchyFlat
from 
    ComputeParentNodeIdAtLevels C
    
left outer join dbo.ItemHierarchy T 
on	T.HierarchyID = C.NodeID
left outer join dbo.ItemHierarchy T1 
on	T1.HierarchyID = C.ParentNodeIdAtLevel1
left outer join dbo.ItemHierarchy T2 
on	T2.HierarchyID = C.ParentNodeIdAtLevel2
left outer join dbo.ItemHierarchy T3 
on	T3.HierarchyID = C.ParentNodeIdAtLevel3
left outer join dbo.ItemHierarchy T4 
on	T4.HierarchyID = C.ParentNodeIdAtLevel4
left outer join dbo.ItemHierarchy T5 
on	T5.HierarchyID = C.ParentNodeIdAtLevel5






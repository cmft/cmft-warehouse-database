﻿

CREATE proc [dbo].[BuildWrkDatasetUTI] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'UTI'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetUTI

insert into dbo.WrkDatasetUTI
(
	DatasetRecno
	,SymptomStartDate
	,SymptomStartDateID
	,UrinaryTestRequestedDateID
	,TreatmentStartDate
	,CatheterInsertedDateID
	,SourceCatheterTypeCode
	,AdmissionTime
	,AdmissionDate
	,StartDirectorateID
	,EndDirectorateID		
	,SpecialtyID
	,StartSiteID 
	,EndSiteID 
	,ClinicianID
	,ServicePointID
)	

select
	BaseUrinaryTractInfection.UrinaryTractInfectionRecno
	,BaseUrinaryTractInfection.SymptomStartDate
	,BaseUrinaryTractInfectionReference.SymptomStartDateID
	,UrinaryTestRequestedDateID
	,TreatmentStartDate
	,CatheterInsertedDateID
	,CatheterType.SourceCatheterTypeCode
	,AdmissionDate = BaseEncounter.AdmissionDate
	,AdmissionTime = BaseEncounter.AdmissionTime
	,StartDirectorateID = StartDirectorate.DirectorateID
	,EndDirectorateID = EndDirectorate.DirectorateID					
	,SpecialtyID = BaseEncounterReference.SpecialtyID	
	,StartSiteID = BaseEncounterReference.StartSiteID	
	,EndSiteID = BaseEncounterReference.EndSiteID
	,ClinicianID = BaseEncounterReference.ConsultantID
	,ServicePointID = BaseUrinaryTractInfectionReference.WardID
from
	WarehouseOLAPMergedV2.APC.BaseUrinaryTractInfection

inner join WarehouseOLAPMergedV2.APC.BaseUrinaryTractInfectionReference
on	BaseUrinaryTractInfectionReference.MergeUrinaryTractInfectionRecno = BaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno

inner join WarehouseOLAPMergedV2.APC.CatheterType
on BaseUrinaryTractInfectionReference.CatheterTypeID = CatheterType.SourceCatheterTypeID

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseUrinaryTractInfection
on	BaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno = BaseEncounterBaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterBaseUrinaryTractInfection.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode

where
	BaseUrinaryTractInfection.SymptomStartDate > @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
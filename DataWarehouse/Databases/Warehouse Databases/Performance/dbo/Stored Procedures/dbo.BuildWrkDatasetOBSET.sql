﻿

CREATE proc [dbo].[BuildWrkDatasetOBSET] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'OBSET'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetOBSET

insert into dbo.WrkDatasetOBSET
(
	DatasetRecno
	,StartDateID
	,ObservationStatus
	,DirectorateID 
	,SpecialtyID 
	,SiteID
	,ClinicianID
	,ServicePointID 
)
	
select
	BaseObservationSet.MergeObservationSetRecno
	,StartDateID = BaseObservationSetReference.StartDateID
	,ObservationStatus.LocalObservationStatus
	,DirectorateID = Directorate.DirectorateID			
	,SpecialtyID = BaseObservationSetReference.SpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = EmergencyDepartment.SourceEmergencyDepartmentID
from
	WarehouseOLAPMergedV2.Observation.BaseObservationSet

inner join WarehouseOLAPMergedV2.Observation.BaseObservationSetReference
on	BaseObservationSetReference.MergeObservationSetRecno = BaseObservationSet.MergeObservationSetRecno

inner join WarehouseOLAPMergedV2.Observation.ObservationStatus
on	ObservationStatus.SourceObservationStatusID = BaseObservationSetReference.DueTimeStatusID

inner join WarehouseOLAPMergedV2.WH.Directorate
on	coalesce(BaseObservationSet.DirectorateCode, '9') = Directorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseObservationSet.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseObservationSet.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.EmergencyDepartment
on	EmergencyDepartment.SourceContextCode = BaseObservationSet.ContextCode
and	EmergencyDepartment.SourceEmergencyDepartmentCode = cast(BaseObservationSet.LocationID as varchar)

where
	BaseObservationSet.StartDate >= @StartDate
and	exists
	--only include observation sets which were carried out
		(
		select
			1
		from
			WarehouseOLAPMergedV2.Observation.ObservationNotTakenReason
		where
			BaseObservationSetReference.ObservationNotTakenReasonID = SourceObservationNotTakenReasonID
		and ObservationNotTakenReason.SourceObservationNotTakenReasonCode = '-1'
		)

union

select
	BaseObservationSet.MergeObservationSetRecno
	,CreatedDateID = BaseObservationSetReference.StartDateID
	,ObservationStatus.LocalObservationStatus
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = BaseObservationSetReference.SpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = Ward.SourceWardID
from
	WarehouseOLAPMergedV2.Observation.BaseObservationSet

inner join WarehouseOLAPMergedV2.Observation.BaseObservationSetReference
on	BaseObservationSetReference.MergeObservationSetRecno = BaseObservationSet.MergeObservationSetRecno

inner join WarehouseOLAPMergedV2.Observation.ObservationStatus
on	ObservationStatus.SourceObservationStatusID = BaseObservationSetReference.DueTimeStatusID

inner join WarehouseOLAPMergedV2.WH.Directorate
on	coalesce(BaseObservationSet.DirectorateCode, '9') = Directorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseObservationSet.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseObservationSet.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Ward
on	Ward.SourceContextCode = BaseObservationSet.ContextCode
and	Ward.SourceWardCode = cast(BaseObservationSet.LocationID as varchar)

where
	BaseObservationSet.StartDate >= @StartDate
and	exists
	--only include observation sets which were carried out
		(
		select
			1
		from
			WarehouseOLAPMergedV2.Observation.ObservationNotTakenReason
		where
			BaseObservationSetReference.ObservationNotTakenReasonID = SourceObservationNotTakenReasonID
		and ObservationNotTakenReason.SourceObservationNotTakenReasonCode = '-1'
		)


select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
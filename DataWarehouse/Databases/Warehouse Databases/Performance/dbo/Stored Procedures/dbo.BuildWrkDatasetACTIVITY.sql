﻿

CREATE proc [dbo].[BuildWrkDatasetACTIVITY] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'ACTIVITY'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetACTIVITY

insert dbo.WrkDatasetACTIVITY
(
 DatasetRecno
,ContextCode
,ActivityDate
,ActivityDateID
,ActivityMetricCode
,Cases
,Value
,DirectorateID 			
,SpecialtyID 
,SiteID 
,ClinicianID
,ServicePointID 
)
select 
	 DatasetRecno = Activity.MergeEncounterRecno
	,Context.ContextCode
	,ActivityDate = Calendar.TheDate
	,ActivityDateID = Activity.EncounterDateID 
	,ActivityMetricCode = ActivityMetric.MetricCode
	,Cases = Activity.Cases
	,Value = Activity.Value
	,DirectorateID = Activity.DirectorateID
	,SpecialtyID = Activity.SpecialtyID
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = ServicePoint.SourceServicePointID
from
	WarehouseOLAPMergedV2.WH.FactActivity Activity

inner join WarehouseOLAPMergedV2.WH.ActivityMetric
on	ActivityMetric.MetricID = Activity.MetricID
--and	ActivityMetric.MetricParentCode in
--		(
--		 'ATT' -- OP Attenders
--		,'ELSPL' -- Elective Spells
--		,'NESPL' -- Non-Elective Spells
--		)
--and	ActivityMetric.MetricCode not in
--		(
--		 'RDSPL' -- Regular Day Spells
--		,'RNSPL' -- Regular Night Spells
--		)
inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.DateID = Activity.EncounterDateID

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextID = Activity.ContextID
	
inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = Context.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = Context.ContextCode
and Specialty.SourceSpecialtyCode = '-1'
	
inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = Context.ContextCode
and Consultant.SourceConsultantCode = '-1'
	
inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	Activity.ConsolidatedView = 1
and	Activity.Reportable = 1
and	Calendar.TheDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
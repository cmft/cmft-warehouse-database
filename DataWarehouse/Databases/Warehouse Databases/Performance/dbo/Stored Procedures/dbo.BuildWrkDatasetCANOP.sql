﻿

CREATE proc [dbo].[BuildWrkDatasetCANOP] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'CANOP'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetCANOP

insert into dbo.WrkDatasetCANOP
(
	DatasetRecno
	,ContextCode
	,SpecialtyID
	,ClinicianID
	,WardCode
	,CancellationDate
	,CancellationDateID
	,CancellationReason
	,ReportableBreach
	,DirectorateID
	,SiteID
	,ServicePointID
)

select
	DatasetRecno = Base.MergeCancelledOperationRecno
	,ContextCode = Base.ContextCode
	,SpecialtyID = Reference.SpecialtyID
	,ClinicianID = Reference.ConsultantID
	,WardCode = Base.WardCode
	,CancellationDate = Base.CancellationDate
	,CancellationDateID = Reference.CancelledOperationDateID
	,CancellationReason = Base.CancellationReason
	,ReportableBreach = Base.ReportableBreach
	,DirectorateID = Directorate.DirectorateID
	,SiteID = Site.SourceSiteID
	,ServicePointID = Reference.WardID
from 
	WarehouseOLAPMergedV2.Theatre.BaseCancelledOperation Base

inner join WarehouseOLAPMergedV2.Theatre.BaseCancelledOperationReference Reference
on Base.MergeCancelledOperationRecno = Reference.MergeCancelledOperationRecno

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(Base.DirectorateCode, '9')

inner join WarehouseOLAPMergedV2.WH.Site
on Site.SourceContextCode = 'CMFT||CANOP'
and Site.SourceSiteCode = '-1'

where
	(
	Removed is null
and	(
		CancellationReason <> 'Medical reason' 
	or 	CancellationReason is null
	)
and  Base.CancellationDate >= @StartDate
	)


select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
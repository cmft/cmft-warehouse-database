﻿
CREATE proc [dbo].[BuildWrkDatasetHR] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'HR'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetHR 

insert dbo.WrkDatasetHR 
(
	DatasetRecno
	,CensusDateID
	,AppraisalRequired
	,AppraisalCompleted
	,ClinicalMandatoryTrainingRequired
	,ClinicalMandatoryTrainingCompleted
	,CorporateMandatoryTrainingRequired
	,CorporateMandatoryTrainingCompleted
	,RecentAppointment
	,LongTermAppointment
	,SicknessAbsenceFTE
	,SicknessEstablishmentFTE
	,Headcount
	,FTE
	,StartersHeadcount
	,StartersFTE
	,LeaversHeadcount
	,LeaversFTE
	,ClinicalBudgetWTE
	,ClinicalContractedWTE
	,ServicePointID
	,DirectorateID 
	,SiteID 
	,SpecialtyID 
	,ClinicianID 
	,ACAgencySpend
	,HeadcountMonthly 
	,FTEMonthly 
	,StartersHeadcountMonthly 
	,StartersFTEMonthly 
	,LeaversHeadcountMonthly 
	,LeaversFTEMonthly 
	,BMERecentAppointment
	,BMELongTermAppointment
	,ESRSIP	
	,GeneralLedgerEst	
	,ESRSIPBand5	
	,GeneralLedgerEstBand5
	,HeadcountPeriodStart	
	,HeadcountPeriodEnd	
	,FTERetentionIndex	
	,HeadcountRetentionIndex	
	,BMEHeadcountPeriodStart	
	,BMEHeadcountPeriodEnd	
	,BMEFTERetentionIndex	
	,BMEHeadcountRetentionIndex
)

select
	BaseSummary.MergeSummaryRecno
	,BaseSummaryReference.CensusDateID
	,AppraisalRequired
	,AppraisalCompleted
	,ClinicalMandatoryTrainingRequired
	,ClinicalMandatoryTrainingCompleted
	,CorporateMandatoryTrainingRequired
	,CorporateMandatoryTrainingCompleted
	,RecentAppointment
	,LongTermAppointment
	,SicknessAbsenceFTE
	,SicknessEstablishmentFTE
	,Headcount3mthRolling
	,FTE3mthRolling
	,StartersHeadcount3mthRolling
	,StartersFTE3mthRolling
	,LeaversHeadcount3mthRolling
	,LeaversFTE3mthRolling
	,ClinicalBudgetWTE
	,ClinicalContractedWTE
	,BaseSummaryReference.WardID
	,DirectorateID = Directorate.DirectorateID
	,SiteID = Site.SourceSiteID
	,SpecialtyID = Specialty.SourceSpecialtyID
	,ConsultantID = Consultant.SourceConsultantID
	,ACAgencySpend = (ACAgencySpend/1000)
	,HeadcountMonthly 
	,FTEMonthly 
	,StartersHeadcountMonthly 
	,StartersFTEMonthly 
	,LeaversHeadcountMonthly 
	,LeaversFTEMonthly
	,BMERecentAppointment
	,BMELongTermAppointment
	,ESRSIP	
	,GeneralLedgerEst	
	,ESRSIPBand5	
	,GeneralLedgerEstBand5
	,HeadcountPeriodStart	
	,HeadcountPeriodEnd	
	,FTERetentionIndex	
	,HeadcountRetentionIndex	
	,BMEHeadcountPeriodStart	
	,BMEHeadcountPeriodEnd	
	,BMEFTERetentionIndex	
	,BMEHeadcountRetentionIndex
from
	WarehouseOLAPMergedV2.HR.BaseSummary

inner join WarehouseOLAPMergedV2.HR.BaseSummaryReference
on	BaseSummary.MergeSummaryRecno = BaseSummaryReference.MergeSummaryRecno

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(
										BaseSummary.DirectorateCode
										,'9'
										)

inner join WarehouseOLAPMergedV2.WH.Site
on Site.SourceContextCode = 'CMFT||ESREXT'
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = 'CMFT||ESREXT'
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = 'CMFT||ESREXT'
and Consultant.SourceConsultantCode = '-1'

where
	BaseSummary.CensusDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
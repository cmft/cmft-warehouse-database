﻿CREATE procedure [dbo].[BuildWrkDataset] 

@DatasetCode varchar(20) = null

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare @StoredProcedure nvarchar(50) 

select @StartTime = getdate()

if @DatasetCode is not null

begin
	set @StoredProcedure = 'dbo.BuildWrkDataset' + @DatasetCode
	exec sp_executesql @StoredProcedure
end

else

begin
	declare DatasetCursor cursor fast_forward for

	select distinct
		DatasetCode
	from
		dbo.Dataset
	where
		Active = 1
	order by
		DatasetCode

	open DatasetCursor
	fetch next from DatasetCursor
	into
		 @DatasetCode	 
	while
		@@fetch_status = 0
		begin
			set @StoredProcedure = 'dbo.BuildWrkDataset' + @DatasetCode
			
			print convert(varchar, getdate(), 113) + ' - ' + @StoredProcedure
			
			exec sp_executesql @StoredProcedure
				
			fetch next from DatasetCursor
			into @DatasetCode		
		end
		 
	close DatasetCursor
	deallocate DatasetCursor
end

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed











GO


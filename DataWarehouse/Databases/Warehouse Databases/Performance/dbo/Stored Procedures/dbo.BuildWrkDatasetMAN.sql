﻿

CREATE proc [dbo].[BuildWrkDatasetMAN] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'MAN'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetCANOP

insert into dbo.WrkDatasetMAN
(
	DatasetRecno
	,ContextCode
	,ItemID
	,Date
	,DateID
	,Numerator
	,Denominator
	,DirectorateID 				
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
)

select
	DatasetRecno
	,ContextCode
	,ItemID 
	,Date 	
	,DateID = Calendar.DateID
	,Numerator 
	,Denominator 
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Specialty.SourceSpecialtyID
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = ServicePoint.SourceServicePointID
from
	(
	select
		DatasetRecno = IndicatorValues.Id
		,ContextCode = 'CMFT||PERFMON'
		,ItemID = IndicatorIdentifier
		,Date = convert(
						varchar (11),
						cast('1' 
							+ '/' + cast(Month as varchar) 
							+ '/' + cast(
										case 
										when Month < 4   -- temp fix until Perf Mon issue investigated		
										then YearStart + 1 
										else YearStart 
										end 
									as varchar) 
						as date)
						,103)
		,Numerator = 
					case
					when charindex('/', Value) > 0
					then left(Value, charindex('/', Value) -1)
					else Value
					end
		,Denominator = 
					case
					when charindex('/', Value) > 0
					then substring(Value, charindex('/', Value) + 1, len(Value))
					else Value
					end			
	from
		PerformanceMonitoring.dbo.Indicators

	inner join PerformanceMonitoring.dbo.IndicatorValues
	on	Indicators.Id = Indicator

	where
		convert(
			varchar (11),
			cast('1' 
				+ '/' + cast(Month as varchar) 
				+ '/' + cast(
							case 
							when Month < 4   -- temp fix until Perf Mon issue investigated		
							then YearStart + 1 
							else YearStart 
							end 
						as varchar) 
			as date)
			,103) >= @StartDate

	) ManualIndicator

left outer join WarehouseOLAPMergedV2.WH.Calendar 
on	TheDate = ManualIndicator.Date

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = '9'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = 'CMFT||PERFMON'
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = 'CMFT||PERFMON'
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = 'CMFT||PERFMON'
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
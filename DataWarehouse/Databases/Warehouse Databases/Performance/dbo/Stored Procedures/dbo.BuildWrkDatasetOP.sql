﻿
CREATE proc [dbo].[BuildWrkDatasetOP] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'OP'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)
declare @DatasetID int = 
						(
						select
							DatasetID
						from
							dbo.Dataset
						where
							DatasetCode = @DatasetCode
						)

truncate table dbo.WrkDatasetOP

insert into dbo.WrkDatasetOP
(
	 DatasetRecno
	,ContextCode
	,SourcePatientNo
	,DateOfBirth
	,SourceEncounterNo
	,DirectorateCode
	,SpecialtyCode
	,Specialty
	,NationalSpecialtyCode
	,ClinicianCode
	,SiteCode
	,PrimaryDiagnosisCode
	,PrimaryProcedureCode
	,PrimaryProcedureDate
	,AppointmentDateID
	,AppointmentDate
	,NationalAttendanceStatusCode
	,NationalFirstAttendanceCode
	,AppointmentOutcomeCode
	,EncounterDateID
	,DirectorateID 				
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
) 

select
	DatasetRecno = Encounter.MergeEncounterRecno
	,Encounter.ContextCode
	,SourcePatientNo
	,DateOfBirth
	,SourceEncounterNo
	,Encounter.DirectorateCode
	,SpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
	,ConsultantCode
	,SiteCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryProcedureDate
	,AppointmentDateID
	,AppointmentDate
	,NationalAttendanceStatusCode
	,NationalFirstAttendanceCode
	,AppointmentOutcomeCode
	,EncounterDateID = AppointmentDateID
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Reference.ReferralSpecialtyID
	,SiteID = Reference.SiteID
	,ClinicianID = Reference.ConsultantID
	,ServicePointID = Reference.ClinicID	
from
	WarehouseOLAPMergedV2.OP.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Directorate
on Encounter.DirectorateCode = Directorate.DirectorateCode

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.ReferralSpecialtyID

left join WarehouseOLAPMergedV2.OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = Reference.DerivedFirstAttendanceID

left join WarehouseOLAPMergedV2.OP.AttendanceStatus
on	AttendanceStatus.SourceAttendanceStatusID = Reference.AttendanceStatusID

where
	Encounter.AppointmentDate >= @StartDate

	


select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
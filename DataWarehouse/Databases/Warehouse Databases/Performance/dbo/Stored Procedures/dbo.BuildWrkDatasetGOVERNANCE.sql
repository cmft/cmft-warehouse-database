﻿
CREATE proc [dbo].[BuildWrkDatasetGOVERNANCE] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'GOVERNANCE'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetGOVERNANCE

insert into dbo.WrkDatasetGOVERNANCE
(
	DatasetRecno
	,ActivityDate
	,ActivityDateID
	,DirectorateID
	,GovernanceRiskRating
	,SiteID
	,SpecialtyID
	,ClinicianID
	,ServicePointID
)

select
	DatasetRecno = Gov.ID 
	,ActivityDate = Gov.Month
	,ActivityDateID = Calendar.DateID
	,DirectorateID = 
				coalesce(
					Directorate.DirectorateID
					,15
					)				
	,GovernanceRiskRating = 
				case
				when RiskRating = 'R' then 1
				when RiskRating = 'AR' then 2
				when RiskRating = 'AG' then 2
				when RiskRating = 'G' then 3
				else null
				end
	,SiteID = Site.SourceSiteID	
	,SpecialtyID = Specialty.SourceSpecialtyID
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID	= ServicePoint.SourceServicePointID	
from
	SmallDatasets.Governance.RiskRatingByDivision Gov
	
inner join WarehouseOLAPMergedV2.WH.Calendar Calendar
on	Calendar.TheDate = Gov.Month

left join WarehouseOLAPMergedV2.WH.Directorate
on	Gov.Division = Directorate.Directorate
	
inner join WarehouseOLAPMergedV2.WH.Site
on Site.SourceContextCode = 'CMFT||PERFMON'
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = 'CMFT||PERFMON'
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = 'CMFT||PERFMON'
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'
	
where
	Gov.Month >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
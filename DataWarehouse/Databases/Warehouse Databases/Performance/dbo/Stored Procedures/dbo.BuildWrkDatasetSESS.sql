﻿
CREATE proc [dbo].[BuildWrkDatasetSESS] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'SESS'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetSESS 

insert into dbo.WrkDatasetSESS

(
	DatasetRecno
	,ContextCode
	,SessionDate
	,SessionDateID
	,CancelledFlag
	,PlannedDuration
	,Duration
	,EarlyStart
	,LateStart
	,EarlyFinish
	,LateFinish
	,EarlyStartMinutes
	,LateStartMinutes
	,EarlyFinishMinutes
	,LateFinishMinutes
	,AllTurnaroundsWithin15Minutes
	,DirectorateID
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID
	,SourceUniqueID
)

select
	DatasetRecno = F.MergeRecno
	,B.ContextCode
	,SessionDate = C.TheDate
	,SessionDateID = F.SessionDateID
	,F.CancelledFlag
	,PlannedDuration = F.PlannedSessionDuration
	,Duration = F.ActualDuration
	,F.EarlyStart
	,F.LateStart
	,F.EarlyFinish
	,F.LateFinish
	,F.EarlyStartMinutes
	,F.LateStartMinutes
	,F.EarlyFinishMinutes
	,F.LateFinishMinutes
	,F.AllTurnaroundsWithin15Minutes
	,DirectorateID = F.DirectorateID
	,SpecialtyID = F.SpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = F.ConsultantID
	,ServicePointID = SourceServicePointID
	,B.SourceUniqueID
from
	WarehouseOLAPMergedV2.Theatre.FactSession F
	
inner join WarehouseOLAPMergedV2.Theatre.BaseSession B 
on B.MergeRecno = F.MergeRecno
		
inner join WarehouseOLAPMergedV2.WH.Calendar C 
on C.DateID = F.SessionDateID
		
inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = B.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = F.TheatreID

where
	C.TheDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
﻿

CREATE proc [dbo].[BuildWrkDatasetBEDOCC] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'BEDOCC'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetBEDOCC

insert dbo.WrkDatasetBEDOCC
(
	 DatasetRecno
	,ContextCode
	,Date	
	,DateID
	,DirectorateCode	
	,DirectorateID			
	,SpecialtyCode
	,SpecialtyID
	,SiteCode
	,SiteID
	,WardCode
	,ClinicianCode
	,ClinicianID
	,ServicePointID
	,BedOccupancyNights
	,MedicalOutlier
	,AvailableInpatientBeds
)

select
	DatasetRecno = BaseBedOccupancy.MergeBedOccupancyRecno
	,ContextCode = Context.ContextCode
	,Date = BaseBedOccupancy.BedOccupancyDate
	,DateID = Calendar.DateID
	,DirectorateCode = BaseWardStayDirectorate.DirectorateCode --CH 31/12/2015 after discussion with GS
	,DirectorateID = BaseWardStayDirectorate.DirectorateID
	,SpecialtyCode = Specialty.SourceSpecialtyCode  -- default CH 31/12/2015
	,SpecialtyID = Specialty.SourceSpecialtyID  -- default CH 31/12/2015
	,SiteCode = Site.SourceSiteCode
	,SiteID = Site.SourceSiteID
	,WardCode = BaseWardStayWard.SourceWardCode
	,ClinicianCode = Consultant.SourceConsultantCode  -- default CH 31/12/2015
	,ClinicianID = Consultant.SourceConsultantID  -- default CH 31/12/2015
	,ServicePointID = ServicePoint.SourceServicePointID
	,BedOccupancyNights = BaseBedOccupancy.Nights
	,MedicalOutlier =	
				case 
				when BaseEncounterDirectorate.DivisionCode in ('MEDSP','MEDAC') and	BaseWardStayDirectorate.DivisionCode not in ('MEDSP','MEDAC') 
				then 1
				else 0
				end
	,AvailableInpatientBeds = null	
from
	WarehouseOLAPMergedV2.APC.BaseEncounter

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
on	BaseEncounterReference.MergeEncounterRecno = BaseEncounter.MergeEncounterRecno
		
inner join WarehouseOLAPMergedV2.BedOccupancy.Base BaseBedOccupancy
on	BaseBedOccupancy.APCEncounterMergeEncounterRecno = BaseEncounter.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseWardStay 
on BaseWardStay.MergeEncounterRecno = BaseBedOccupancy.WardStayMergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Directorate BaseEncounterDirectorate
on BaseEncounterDirectorate.DirectorateCode = BaseEncounter.StartDirectorateCode	

inner join WarehouseOLAPMergedV2.WH.Directorate BaseWardStayDirectorate
on BaseWardStayDirectorate.DirectorateCode = BaseWardStay.DirectorateCode	

inner join WarehouseOLAPMergedV2.APC.Ward BaseWardStayWard
on	BaseWardStayWard.SourceWardCode = BaseWardStay.WardCode
and BaseWardStayWard.SourceContextCode = BaseWardStay.ContextCode

inner join WarehouseOLAPMergedV2.WH.Site	
on	Site.SourceSiteCode = BaseWardStay.SiteCode	
and Site.SourceContextCode = BaseWardStay.ContextCode
		
--default Consultant and Specialty agreed as Bed Occupancy is about the ward and bed not the Specialty and Consultant (mirrors the configurnation below)

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = 'CEN||PAS'
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = 'CEN||PAS'
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on  ServicePoint.SourceServicePointID = BaseWardStayWard.SourceWardID

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.TheDate = BaseBedOccupancy.BedOccupancyDate

inner join WarehouseOLAPMergedV2.WH.Context
on	Context.ContextCode = BaseEncounter.ContextCode	

where
	BaseBedOccupancy.BedOccupancyDate >= @StartDate

union

--WardBedConfiguration

select
	DatasetRecno = 0
	,ContextCode = WardDirecorate.ContextCode
	,Date = Calendar.TheDate
	,DateID = Calendar.DateID
	,DirectorateCode = WardDirecorate.DirectorateCode
	,DirectorateID = WardDirecorate.DirectorateID
	,SpecialtyCode = Specialty.SourceSpecialtyCode -- default
	,SpecialtyID = Specialty.SourceSpecialtyID -- default
	,SiteCode = WardDirecorate.SiteCode
	,SiteID = WardDirecorate.SiteID
	,WardCode = WardDirecorate.WardCode
	,ClinicianCode = Consultant.SourceConsultantCode -- default
	,ClinicianID = Consultant.SourceConsultantID -- default
	,ServicePointID = ServicePoint.SourceServicePointID
	,BedOccupancyNights = null
	,MedicalOutlier = null
	,AvailableInpatientBeds = WardBedConfiguration.InpatientBeds
from
	WarehouseOLAPMergedV2.BedOccupancy.WardBedConfiguration
		
inner join
	(
	select 
		DirectorateCode = Directorate.DirectorateCode
		,DirectorateID = Directorate.DirectorateID
		,WardCode = Ward.SourceWardCode
		,WardID = Ward.SourceWardID
		,SiteCode = Site.SourceSiteCode
		,SiteID = Site.SourceSiteID
		,ContextCode = Ward.SourceContextCode
	from 
		WarehouseOLAPMergedV2.Allocation.RuleBase

	inner join WarehouseOLAPMergedV2.Allocation.Allocation
	on Allocation.AllocationID = RuleBase.AllocationID

	inner join WarehouseOLAPMergedV2.WH.Ward
	on Ward.SourceWardCode = RuleBase.WardCode
	and Ward.SourceContextCode = RuleBase.SourceContextCode

	inner join WarehouseOLAPMergedV2.WH.Directorate
	on Directorate.DirectorateCode = Allocation.SourceAllocationID

	inner join WarehouseOLAPMergedV2.WH.Site
	on Site.SourceSiteCode = RuleBase.SourceSiteCode
	and Site.SourceContextCode = RuleBase.SourceContextCode

	where
		RuleBase.SourceContextCode in ('CEN||PAS','TRA||UG')
	and RuleBase.WardCode is not null 
	and RuleBase.Template = 'Allocation.DirectorateBySiteWard'
	) WardDirecorate

on	WardBedConfiguration.WardCode = WardDirecorate.WardCode
and WardBedConfiguration.ContextCode = WardDirecorate.ContextCode

inner join WarehouseOLAPMergedV2.WH.Calendar
on	Calendar.TheDate between WardBedConfiguration.StartDate 
and coalesce(
		WardBedConfiguration.EndDate
		, getdate()
		)

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on  ServicePoint.SourceServicePointID = WardDirecorate.WardID

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = 'CEN||PAS'
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = 'CEN||PAS'
and Consultant.SourceConsultantCode = '-1'

where
	Calendar.TheDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
﻿
CREATE proc [dbo].[BuildWrkDatasetREN] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'REN'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetREN

insert into dbo.WrkDatasetREN
(
	DatasetRecno
	,StartDate
	,StartDateID
	,StopDate
	,ContextCode
	,ModalityCode
	,ModalitySettingCode
	,EventDetailCode
	,DialysisProviderCode
	,VascularAccessCode
	,VascularAccessAt90DaysCode
	,DirectorateID 			
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
)

select
	M.PatientRenalModalityRecno
	,M.StartDate
	,Start.DateID
	,M.StopDate
	,M.ContextCode
	,M.ModalityCode
	,M.ModalitySettingCode
	,M.EventDetailCode
	,M.DialysisProviderCode
	,VascularAccessCode = A0.TimelineEventDetailCode
	,VascularAccessAt90DaysCode = A90.TimelineEventDetailCode
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = ServicePoint.SourceServicePointID
from
	WarehouseOLAPMergedV2.Renal.BasePatientRenalModality M

left join WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A0 
on	A0.PatientObjectID = M.PatientObjectID
and A0.StartDate >= M.StartDate
and not exists
		(
		select
			1 
		from
			WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A
		where
			A.PatientObjectID = M.PatientObjectID
		and A.StartDate >= M.StartDate
		and 
			(
				A.StartDate < A0.StartDate 
			or 
				(
					A.StartDate = A0.StartDate 
				and A.SourceUniqueID < A0.SourceUniqueID
				)
			)
		)

left join WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A90 
on	A90.PatientObjectID = M.PatientObjectID
and A90.StartDate >= dateadd(day, 90, M.StartDate)
and not exists
	(
	select
		1 
	from
		WarehouseOLAPMergedV2.Renal.BasePatientVascularAccess A
	where
		A.PatientObjectID = M.PatientObjectID
	and A.StartDate >= dateadd(day, 90, M.StartDate)
	and (
			A.StartDate < A90.StartDate 
		or 
			(
				A.StartDate = A90.StartDate 
			and A.SourceUniqueID < A90.SourceUniqueID
			)
		)
	)

inner join WarehouseOLAPMergedV2.WH.Calendar Start
on cast(M.StartDate as date) = Start.TheDate

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = '9'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = M.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = M.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = M.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	M.StartDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
﻿


CREATE proc [dbo].[BuildWrkDatasetVTECOND] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'VTECOND'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetVTECOND

insert into dbo.WrkDatasetVTECOND
(
	DatasetRecno
	,DiagnosisDateID
	,MedicationRequired
	,MedicationStartDateID
	,SourceVTETypeCode
	,DiagnosisDate
	,AdmissionDate
	,StartDirectorateID
	,EndDirectorateID		
	,SpecialtyID
	,StartSiteID 
	,EndSiteID 
	,ClinicianID
	,ServicePointID
)	

select
	BaseVTECondition.VTEConditionRecno
	,BaseVTEConditionReference.DiagnosisDateID
	,BaseVTECondition.MedicationRequired
	,BaseVTEConditionReference.MedicationStartDateID
	,VTEType.SourceVTETypeCode
	,BaseVTECondition.DiagnosisDate
	,BaseEncounter.AdmissionDate
	,StartDirectorateID = StartDirectorate.DirectorateID
	,EndDirectorateID = EndDirectorate.DirectorateID					
	,SpecialtyID = BaseEncounterReference.SpecialtyID	
	,StartSiteID = BaseEncounterReference.StartSiteID	
	,EndSiteID = BaseEncounterReference.EndSiteID
	,ClinicianID = BaseEncounterReference.ConsultantID
	,ServicePointID = BaseVTEConditionReference.WardID
from
	WarehouseOLAPMergedV2.APC.BaseVTECondition

inner join WarehouseOLAPMergedV2.APC.BaseVTEConditionReference
on	BaseVTEConditionReference.MergeVTEConditionRecno = BaseVTECondition.MergeVTEConditionRecno

inner join WarehouseOLAPMergedV2.APC.VTEType
on BaseVTEConditionReference.VTETypeID = VTEType.SourceVTETypeID

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseVTECondition
on	BaseVTECondition.MergeVTEConditionRecno = BaseEncounterBaseVTECondition.MergeVTEConditionRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterBaseVTECondition.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode

where
	BaseVTECondition.DiagnosisDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
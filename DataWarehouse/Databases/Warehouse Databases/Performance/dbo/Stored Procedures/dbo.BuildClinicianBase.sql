﻿

CREATE proc [dbo].[BuildClinicianBase]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	 dbo.ClinicianBase target
using
	(
	select
		SourceContextID = Member.SourceContextID
		,SourceContextCode = Member.SourceContextCode
		,SourceContext = Member.SourceContext
		,SourceClinicianID = Member.SourceValueID
		,SourceClinicianCode = Member.SourceValueCode
		,SourceClinician = Member.SourceValue
		,LocalClinicianID = Member.LocalValueID
		,LocalClinicianCode = Member.LocalValueCode
		,LocalClinician = Member.LocalValue
		,NationalClinicianID = Member.NationalValueID
		,NationalClinicianCode = Member.NationalValueCode
		,NationalClinician = Member.NationalValue							
		,MainSpecialtyCode = Consultant.MainSpecialtyCode
		,MainSpecialty = Consultant.MainSpecialty
		,AppraisalCompletionDate = Appraisal.CompletionDate
		,AppraisalExpiryDate = Appraisal.ExpiryDate
		,AppraisalStatus = Appraisal.Status
		,ClinicalTrainingExpiryDate = ClinicalTraining.ExpiryDate
		,ClinicalTrainingStatus = ClinicalTraining.Status
		,MandatoryTrainingExpiryDate = MandatoryTraining.ExpiryDate
		,MandatoryTrainingStatus = MandatoryTraining.Status
		,RevalidationExpiryDate = Revalidation.ExpiryDate

		,ClinicanType = Member.Attribute
	from
		WarehouseOLAPMergedV2.WH.Member

	left join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceConsultantID = Member.SourceValueID

	left join WarehouseOLAPMergedV2.HR.BaseClinician Appraisal
	on	Appraisal.ClinicianCode = Member.NationalValueCode
	and	Appraisal.MeasureCode = 'APPRAISAL'

	left join WarehouseOLAPMergedV2.HR.BaseClinician ClinicalTraining
	on	ClinicalTraining.ClinicianCode = Member.NationalValueCode
	and	ClinicalTraining.MeasureCode = 'CLINICALTRAINING'

	left join WarehouseOLAPMergedV2.HR.BaseClinician MandatoryTraining
	on	MandatoryTraining.ClinicianCode = Member.NationalValueCode
	and	MandatoryTraining.MeasureCode = 'MANDATORYTRAINING'

	left join WarehouseOLAPMergedV2.HR.BaseClinician Revalidation
	on	Revalidation.ClinicianCode = Member.NationalValueCode
	and	Revalidation.MeasureCode = 'REVALIDATION'


	where
		Member.AttributeCode in ('CONSUL', 'PROFCARER', 'STAFFM','THEATRESTAFF')

	) source
	on	source.SourceClinicianID = target.SourceClinicianID

	when not matched by source

	then delete

	when not matched
	then
		insert
			(
			SourceContextID
			,SourceContextCode
			,SourceContext
			,SourceClinicianID
			,SourceClinicianCode
			,SourceClinician
			,LocalClinicianID
			,LocalClinicianCode
			,LocalClinician
			,NationalClinicianID
			,NationalClinicianCode
			,NationalClinician
			,MainSpecialtyCode
			,MainSpecialty
			,AppraisalCompletionDate 
			,AppraisalExpiryDate 
			,AppraisalStatus 
			,ClinicalTrainingExpiryDate 
			,ClinicalTrainingStatus 
			,MandatoryTrainingExpiryDate 
			,MandatoryTrainingStatus 
			,RevalidationExpiryDate 
			,ClinicanType
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.SourceContextID
			,source.SourceContextCode
			,source.SourceContext
			,source.SourceClinicianID
			,source.SourceClinicianCode
			,source.SourceClinician
			,source.LocalClinicianID
			,source.LocalClinicianCode
			,source.LocalClinician
			,source.NationalClinicianID
			,source.NationalClinicianCode
			,source.NationalClinician
			,source.MainSpecialtyCode
			,source.MainSpecialty
			,source.AppraisalCompletionDate 
			,source.AppraisalExpiryDate 
			,source.AppraisalStatus 
			,source.ClinicalTrainingExpiryDate 
			,source.ClinicalTrainingStatus 
			,source.MandatoryTrainingExpiryDate 
			,source.MandatoryTrainingStatus 
			,source.RevalidationExpiryDate 
			,source.ClinicanType
			,getdate()
			,getdate()
			,suser_name()			
			)

	when matched
	and not
		(	
			isnull(target.SourceContextID, 0) = isnull(source.SourceContextID, 0)	
		and isnull(target.SourceContextCode, '') = isnull(source.SourceContextCode, '')					
		and isnull(target.SourceContext, '') = isnull(source.SourceContext, '')	
		and	isnull(target.SourceClinicianID, 0) = isnull(source.SourceClinicianID, 0)
		and	isnull(target.SourceClinicianCode, '') = isnull(source.SourceClinicianCode, '')	
		and	isnull(target.SourceClinician, '') = isnull(source.SourceClinician, '')
		and	isnull(target.LocalClinicianID, 0) = isnull(source.LocalClinicianID, 0)
		and	isnull(target.LocalClinicianCode, '') = isnull(source.LocalClinicianCode, '')		
		and isnull(target.LocalClinician, '') = isnull(source.LocalClinician, '')
		and	isnull(target.NationalClinicianID, 0) = isnull(source.NationalClinicianID, 0)
		and	isnull(target.NationalClinicianCode, '') = isnull(source.NationalClinicianCode, '')		
		and isnull(target.NationalClinician, '') = isnull(source.NationalClinician, '')
		and isnull(target.MainSpecialtyCode, '') = isnull(source.MainSpecialtyCode, '')
		and isnull(target.MainSpecialty, '') = isnull(source.MainSpecialty, '')
		and isnull(target.AppraisalCompletionDate, '1 jan 1900') = isnull(source.AppraisalCompletionDate, '1 jan 1900')
		and isnull(target.AppraisalExpiryDate, '1 jan 1900') = isnull(source.AppraisalExpiryDate, '1 jan 1900')
		and isnull(target.AppraisalStatus, '') = isnull(source.AppraisalStatus, '')
		and isnull(target.ClinicalTrainingExpiryDate, '1 jan 1900') = isnull(source.ClinicalTrainingExpiryDate, '1 jan 1900')
		and isnull(target.ClinicalTrainingStatus, '') = isnull(source.ClinicalTrainingStatus, '')
		and isnull(target.MandatoryTrainingExpiryDate, '1 jan 1900') = isnull(source.MandatoryTrainingExpiryDate, '1 jan 1900')
		and isnull(target.MandatoryTrainingStatus, '') = isnull(source.MandatoryTrainingStatus, '')
		and isnull(target.RevalidationExpiryDate, '1 jan 1900') = isnull(source.RevalidationExpiryDate, '1 jan 1900')
		and isnull(target.ClinicanType, '') = isnull(source.ClinicanType, '')
		)
	then
		update
		set
			target.SourceContextID = source.SourceContextID
			,target.SourceContextCode = source.SourceContextCode
			,target.SourceContext = source.SourceContext
			,target.SourceClinicianID = source.SourceClinicianID	
			,target.SourceClinicianCode = source.SourceClinicianCode
			,target.SourceClinician = source.SourceClinician
			,target.LocalClinicianID = source.LocalClinicianID							
			,target.LocalClinicianCode = source.LocalClinicianCode
			,target.LocalClinician = source.LocalClinician	
			,target.NationalClinicianID = source.NationalClinicianID	
			,target.NationalClinicianCode = source.NationalClinicianCode
			,target.NationalClinician = source.NationalClinician
			,target.MainSpecialtyCode = source.MainSpecialtyCode
			,target.MainSpecialty = source.MainSpecialty	
			,target.AppraisalCompletionDate = source.AppraisalCompletionDate
			,target.AppraisalExpiryDate = source.AppraisalExpiryDate
			,target.AppraisalStatus = source.AppraisalStatus
			,target.ClinicalTrainingExpiryDate = source.ClinicalTrainingExpiryDate
			,target.ClinicalTrainingStatus = source.ClinicalTrainingStatus
			,target.MandatoryTrainingExpiryDate = source.MandatoryTrainingExpiryDate
			,target.MandatoryTrainingStatus = source.MandatoryTrainingStatus
			,target.RevalidationExpiryDate = source.RevalidationExpiryDate					
			,target.ClinicanType = source.ClinicanType
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime








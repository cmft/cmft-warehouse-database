﻿
CREATE proc [dbo].[BuildWrkDatasetINC] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'INC'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetINC 

insert into dbo.WrkDatasetINC
(
	 DatasetRecno
	,ContextCode
	,IncidentDate
	,IncidentDateID
	,IncidentGradeCode
	,CauseGroupCode
	,Cause1Code
	,IncidentTypeCode
	,PatientSafety
	,IncidentInvolvingChild
	,IncidentInvolvingAdult
	,IncidentInvolvingElderly
	,DirectorateID 		
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
	,SourceDepartmentCode
) 

select
	DatasetRecno = BaseIncident.MergeIncidentRecno
	,BaseIncident.ContextCode
	,BaseIncident.IncidentDate
	,BaseIncidentReference.IncidentDateID
	,BaseIncident.IncidentGradeCode
	,BaseIncident.CauseGroupCode
	,BaseIncident.CauseCode1
	,BaseIncident.IncidentTypeCode
	,BaseIncident.PatientSafety
	,IncidentInvolvingChild = 
							case 
							when CMFT.Dates.GetAge(BaseIncident.DateOfBirth, BaseIncident.IncidentDate) < 18
							then 1 
							end
	,IncidentInvolvingAdult = 
							case 
							when CMFT.Dates.GetAge(BaseIncident.DateOfBirth, BaseIncident.IncidentDate) > 18
							then 1 
							end
	,IncidentInvolvingElderly = 
							case 
							when CMFT.Dates.GetAge(BaseIncident.DateOfBirth, BaseIncident.IncidentDate) >= 65								
							then 1 
							end
	,DirectorateID = Directorate.DirectorateID					
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = BaseIncidentReference.WardID
	,SourceDepartmentCode = BaseIncident.DepartmentCode
from
	WarehouseOLAPMergedV2.Incident.BaseIncident

inner join WarehouseOLAPMergedV2.Incident.BaseIncidentReference
on	BaseIncidentReference.MergeIncidentRecno = BaseIncident.MergeIncidentRecno

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(BaseIncident.DirectorateCode, '9')

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = BaseIncident.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseIncident.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseIncident.ContextCode
and Consultant.SourceConsultantCode = '-1'

where
	BaseIncident.IncidentDate >= @StartDate
and BaseIncident.IncidentDate <= getdate()
and	BaseIncident.SequenceNo = 1 
and BaseIncident.PatientSafety = 'S'
and BaseIncident.SiteCode not in ('0x4A20','0x4B20') --Non CMFT & PCT incidents
and BaseIncident.IncidentTypeCode not in ('0x5A20') --Excellence Reporting to be excluded

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
﻿

CREATE proc [dbo].[BuildWrkDatasetIPWLE] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'IPWLE'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetIPWLE 

insert into dbo.WrkDatasetIPWLE
(
	DatasetRecno
	,ContextCode
	,SourcePatientNo
	,CasenoteNumber
	,EpisodeNo
	,ActivityDate
	,ActivityDateID
	,DirectorateID 
	,SpecialtyID 
	,SiteID 
	,ClinicianID 
	,ServicePointID 
)

select
	DatasetRecno = W.WaitingListEntryRecno
	,W.ContextCode
	,W.SourcePatientNo
	,W.CasenoteNumber
	,EpisodeNo = W.SourceEntityRecno
	,W.ActivityDate
	,ActivityDate.DateID	
	,DirectorateID = Directorate.DirectorateID			
	,SpecialtyID = Specialty.SourceSpecialtyID
	,SiteID = Site.SourceSiteID
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = ServicePoint.SourceServicePointID	
from
	WarehouseOLAPMergedV2.APCWL.BaseWaitingListEntry W
	
inner join WarehouseOLAPMergedV2.WH.Calendar ActivityDate 
on ActivityDate.TheDate = W.ActivityDate

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = '9'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = W.ContextCode
and Specialty.SourceSpecialtyCode = coalesce(W.SpecialtyCode, '-1')

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = W.ContextCode
and Site.SourceSiteCode = coalesce(W.SiteCode, '-1')

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = W.ContextCode
and Consultant.SourceConsultantCode = coalesce(W.ConsultantCode, '-1')

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	W.ActivityDate >= @StartDate
	

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
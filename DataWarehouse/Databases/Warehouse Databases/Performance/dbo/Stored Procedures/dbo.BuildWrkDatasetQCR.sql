﻿
CREATE proc [dbo].[BuildWrkDatasetQCR] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'QCR'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetQCR 

insert into dbo.WrkDatasetQCR
(
	 DatasetRecno
	,ContextCode
	,AuditDate
	,AuditDateID
	,QuestionCode
	,CustomListCode
	,Answer
	,DirectorateID 				
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
) 
select
	DatasetRecno = Encounter.AuditAnswerRecno
	,Encounter.ContextCode
	,Encounter.AuditDate
	,Reference.AuditDateID
	,Encounter.QuestionCode
	,CustomListQuestion.CustomListCode
	,Encounter.Answer
	,DirectorateID = Directorate.DirectorateID					
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = Reference.WardID
from
	WarehouseOLAPMergedV2.QCR.BaseAuditAnswer Encounter

inner join WarehouseOLAPMergedV2.QCR.BaseAuditAnswerReference Reference
on	Reference.AuditAnswerRecno = Encounter.AuditAnswerRecno

left join WarehouseOLAPMergedV2.QCR.CustomListQuestion 
on CustomListQuestion.QuestionCode = Encounter.QuestionCode

inner join WarehouseOLAPMergedV2.WH.Directorate
on	coalesce (Encounter.DirectorateCode,'9') = Directorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = Encounter.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = Encounter.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = Encounter.ContextCode
and Consultant.SourceConsultantCode = '-1'
	
where
	Encounter.AuditDate >= @StartDate
and Encounter.AuditDate <= getdate()
and	Encounter.Answer in (0,1) -- Take Yes and No responses only: ignore nulls



select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'
print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
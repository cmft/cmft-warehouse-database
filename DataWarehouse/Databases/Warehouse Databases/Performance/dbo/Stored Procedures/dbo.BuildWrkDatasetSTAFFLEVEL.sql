﻿

CREATE proc [dbo].[BuildWrkDatasetSTAFFLEVEL] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'STAFFLEVEL'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetSTAFFLEVEL

insert into dbo.WrkDatasetSTAFFLEVEL
(
	DatasetRecno
	,ContextCode
	,SourceShift
	,RegisteredNursePlan
	,RegisteredNurseActual
	,NonRegisteredNursePlan
	,NonRegisteredNurseActual
	,CensusDateID
	,DirectorateID 
	,SpecialtyID 
	,SiteID 
	,ClinicianID 
	,ServicePointID 
)

select
	DatasetRecno = BaseStaffingLevel.MergeStaffingLevelRecno
	,BaseStaffingLevel.ContextCode
	,SourceShift
	,RegisteredNursePlan
	,RegisteredNurseActual
	,NonRegisteredNursePlan
	,NonRegisteredNurseActual
	,BaseStaffingLevelReference.CensusDateID
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = BaseStaffingLevelReference.WardID
from
	WarehouseOLAPMergedV2.APC.BaseStaffingLevel

inner join WarehouseOLAPMergedV2.APC.BaseStaffingLevelReference
on	BaseStaffingLevel.MergeStaffingLevelRecno = BaseStaffingLevelReference.MergeStaffingLevelRecno

inner join WarehouseOLAPMergedV2.WH.Shift
on Shift.SourceShiftID = BaseStaffingLevelReference.ShiftID
	
inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(BaseStaffingLevel.DirectorateCode, '9')

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = BaseStaffingLevel.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseStaffingLevel.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseStaffingLevel.ContextCode
and Consultant.SourceConsultantCode = '-1'

where
	BaseStaffingLevel.CensusDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
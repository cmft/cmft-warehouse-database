﻿CREATE procedure [dbo].[BuildCANTSubsequentDrugTreatmentWithin31DaysAll] as

exec BuildCANTSubsequentDrugTreatmentWithin31Days 'CO', 'Colorectal'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'GY', 'Gynaecology'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'HA', 'Haematology'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'HN', 'Head and Neck'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'LU', 'Lung'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'OT', 'Other'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'PA', 'Paediatric'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'SA', 'Sarcoma'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'SK', 'Skin'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'UG', 'Upper GI'
exec BuildCANTSubsequentDrugTreatmentWithin31Days 'UR', 'Urology'
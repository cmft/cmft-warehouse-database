﻿

CREATE proc [dbo].[BuildWrkDatasetFFTRTN] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'FFTRTN'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetFFTRTN

insert dbo.WrkDatasetFFTRTN
(
	DatasetRecno
	,ContextCode
	,CensusDate
	,WardCode
	,SiteCode
	,CensusDateID
	,ServicePointID
	,SiteID 
	,SpecialtyID 
	,ClinicianID 
	,DirectorateID 
	,FFTReturnType 
	,ExtremelyLikeley 
	,Likely 
	,NeitherLikelyNorUnlikely 
	,Unlikely 
	,ExtremelyUnlikely 
	,DontKnow 
	,Responses 
	,EligibleResponders 
)

select
	FFT.MergeFFTRecno
	,ContextCode = FFT.ContextCode
	,CensusDate = FFT.CensusDate
	,WardCode = FFT.WardCode
	,SiteCode = FFT.HospitalSiteCode
	,CensusDateID = Reference.CensusDateID
	,ServicePointID  = Reference.WardID
	,SiteID = Reference.SiteID
	,SpecialtyID = Specialty.SourceSpecialtyID -- default
	,ClinicianID = Consultant.SourceConsultantID -- default
	,DirectorateID = Directorate.DirectorateID -- default
	,FFTReturnType = FFT.[Return]
	,ExtremelyLikeley = FFT.[1ExtremelyLikely]
	,Likely = FFT.[2Likely]
	,NeitherLikelyNorUnlikely = FFT.[3NeitherLikelyNorUnlikely]
	,Unlikely = FFT.[4Unlikely]
	,ExtremelyUnlikely = FFT.[5ExtremelyUnlikely]
	,DontKnow = FFT.[6DontKnow]
	,Responses = FFT.Responses
	,EligibleResponders = FFT.EligibleResponders
from
	WarehouseOLAPMergedV2.PEX.BaseFriendsFamilyTestReturn FFT

inner join WarehouseOLAPMergedV2.PEX.BaseFriendsFamilyTestReturnReference Reference
on FFT.MergeFFTRecno = Reference.MergeFFTRecno

inner join WarehouseOLAPMergedV2.WH.Directorate
on	coalesce(FFT.DirectorateCode, '9') = Directorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = 'CMFT||FFTRTN'
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = 'CMFT||FFTRTN'
and Consultant.SourceConsultantCode = '-1'

where
	FFT.CensusDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
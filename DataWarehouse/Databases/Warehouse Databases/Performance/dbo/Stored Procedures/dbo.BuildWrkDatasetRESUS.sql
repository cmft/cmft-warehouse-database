﻿

CREATE proc [dbo].[BuildWrkDatasetRESUS] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'RESUS'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetRESUS


insert into	dbo.WrkDatasetRESUS
(
	DatasetRecno
	,CallOutDateID
	,CallOutReason 
	,AuditType
	,DirectorateID 
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID
)

select
	BaseEncounter.MergeRecno
	,BaseEncounterReference.CallOutDateID
	,SourceCallOutReason
	,AuditType.SourceAuditType
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = SourceServicePointID
from
	WarehouseOLAPMergedV2.Resus.BaseEncounter

inner join 	WarehouseOLAPMergedV2.Resus.BaseEncounterReference
on	BaseEncounterReference.MergeRecno = BaseEncounter.MergeRecno

inner join WarehouseOLAPMergedV2.Resus.CallOutReason
on	CallOutReason.SourceCallOutReasonID = BaseEncounterReference.CallOutReasonID

inner join WarehouseOLAPMergedV2.WH.AuditType
on	AuditType.SourceAuditTypeID = BaseEncounterReference.AuditTypeID

inner join WarehouseOLAPMergedV2.Observation.Location
on	Location.SourceLocationID = BaseEncounterReference.LocationID

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = Location.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = BaseEncounter.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseEncounter.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseEncounter.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	BaseEncounter.CallOutTime >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
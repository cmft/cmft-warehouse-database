﻿

CREATE proc [dbo].[BuildWrkDatasetRENQ] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'RENQ'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetRENQ 

insert into dbo.WrkDatasetRENQ
(
	DatasetRecno
	,ContextCode
	,LastDayOfQuarter
	,LastDayOfQuarterID
	,DateOfBirth
	,DateOfDeath
	,ModalityCode
	,ModalitySettingCode
	,IsPrimary
	,DialysisProviderCode
	,EventDetailCode
	,StartDate
	,StopDate
	,ReasonForChangeCode
	,VascularAccessCode
	,HCO3Num
	,PostVitalsSittingBloodPressureSystolic
	,PostVitalsSittingBloodPressureDiastolic
	,ActualRunTime
	,PreVitalsWeightInKg
	,PostVitalsWeightInKg
	,URRNum
	,UltrafiltrationVolume
	,HgBNum
	,FerritinNum
	,TimingQualifier
	,PhosNum
	,CaAdjNum
	,PTHIntactNum
	,TransplantListStatusCode
	,OnEPO
	,PreviousHDModalityExists
	,DirectorateID 			
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
)

select
	DatasetRecno = R.PatientRenalModalityRecno
	,R.ContextCode
	,R.LastDayOfQuarter
	,LastDayOfQuarterID = QtrEnd.DateID
	,R.DateOfBirth
	,R.DateOfDeath
	,R.ModalityCode
	,R.ModalitySettingCode
	,R.IsPrimary
	,R.DialysisProviderCode
	,R.EventDetailCode
	,R.StartDate
	,R.StopDate
	,R.ReasonForChangeCode
	,R.VascularAccessCode
	,R.HCO3Num
	,R.PostVitalsSittingBloodPressureSystolic
	,R.PostVitalsSittingBloodPressureDiastolic
	,R.ActualRunTime
	,R.PreVitalsWeightInKg
	,R.PostVitalsWeightInKg
	,R.URRNum
	,R.UltrafiltrationVolume
	,R.HgBNum
	,R.FerritinNum
	,R.TimingQualifier
	,R.PhosNum
	,R.CaAdjNum
	,R.PTHIntactNum
	,R.TransplantListStatusCode
	,R.OnEPO
	,R.PreviousHDModalityExists
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = ServicePoint.SourceServicePointID
from
	WarehouseOLAPMergedV2.Renal.QuarterlyCensus R

inner join WarehouseOLAPMergedV2.WH.Calendar QtrEnd -- DG 28.2.14 - changed to inner join
on	R.LastDayOfQuarter = QtrEnd.TheDate

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = '9'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = R.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = R.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = R.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	R.LastDayOfQuarter >= @StartDate


select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
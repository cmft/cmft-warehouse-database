﻿

CREATE proc [dbo].[BuildWrkDatasetPLAN] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'PLAN'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetPLAN

insert dbo.WrkDatasetPLAN
(
 DatasetRecno
,ActivityDate
,ActivityDateID
,ActivityMetricCode
,Cases
,Value
,DirectorateID 			
,SpecialtyID 
,SiteID 
,ClinicianID
,ServicePointID 
)

select
	 DatasetRecno = 0
	,ActivityDate 
	,ActivityDateID 
	,ActivityMetricCode 
	,Cases = sum(Cases) 
	,Value = sum(Value) 
	,DirectorateID 
	,SpecialtyID 
	,SiteID 
	,ClinicianID 
	,ServicePointID
from
	(
	select
		ActivityDate = Calendar.TheDate 
		,ActivityDateID = Activity.DateID 
		,ActivityMetricCode = ActivityMetric.MetricCode
		,Cases = Activity.Cases
		,Value = Activity.Value
		,DirectorateID = Activity.DirectorateID
		,SpecialtyID =
			(
			select top 1 --arbitrarily pick the first value
				SourceSpecialtyID
			from
				WarehouseOLAPMergedV2.WH.Specialty
			where
				Specialty.NationalSpecialtyCode = Activity.NationalSpecialtyCode
			)
		,SiteID = Site.SourceSiteID	
		,ClinicianID = Consultant.SourceConsultantID
		,ServicePointID = ServicePoint.SourceServicePointID
	from
		WarehouseOLAPMergedV2.WH.FactActivityPlan Activity

	inner join WarehouseOLAPMergedV2.WH.ActivityMetric
	on	ActivityMetric.MetricID = Activity.MetricID
	--and	ActivityMetric.MetricParentCode in
	--		(
	--		 'ATT' -- OP Attenders
	--		,'ELSPL' -- Elective Spells
	--		,'NESPL' -- Non-Elective Spells
	--		)
	--and	ActivityMetric.MetricCode not in
	--		(
	--		 'RDSPL' -- Regular Day Spells
	--		,'RNSPL' -- Regular Night Spells
	--		)

	inner join WarehouseOLAPMergedV2.WH.Calendar
	on	Calendar.DateID = Activity.DateID

	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceContextCode = 'CMFT||PERFMON'
	and Site.SourceSiteCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'CMFT||PERFMON'
	and Consultant.SourceConsultantCode = '-1'

	inner join WarehouseOLAPMergedV2.WH.ServicePoint
	on ServicePoint.SourceServicePointID = '-1'

	) ActivityPlan

where
	ActivityPlan.ActivityDate >= @StartDate
and	ActivityPlan.ActivityDate < SYSDATETIME()


group by
	ActivityDate
	,ActivityDateID
	,ActivityMetricCode
	,DirectorateID
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID


select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
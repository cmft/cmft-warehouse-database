﻿


CREATE proc [dbo].[BuildWrkDatasetALERT] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'ALERT'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetALERT

insert into dbo.WrkDatasetALERT
(
	DatasetRecno
	,CreatedTime
	,CreatedDateID
	,ClosedTime
	,DirectorateID 
	,SpecialtyID 
	,SiteID
	,ClinicianID
	,ServicePointID 
)
	
select
	BaseAlert.MergeAlertRecno
	,CreatedTime
	,CreatedDateID = BaseAlertReference.CreatedDateID
	,ClosedTime
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = BaseAlertReference.SpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = EmergencyDepartment.SourceEmergencyDepartmentID
from
	WarehouseOLAPMergedV2.Observation.BaseAlert

inner join WarehouseOLAPMergedV2.Observation.BaseAlertReference
on	BaseAlertReference.MergeAlertRecno = BaseAlert.MergeAlertRecno

inner join WarehouseOLAPMergedV2.WH.Directorate
on	coalesce(BaseAlert.DirectorateCode, '9') = Directorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseAlert.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseAlert.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.EmergencyDepartment
on	EmergencyDepartment.SourceContextCode = BaseAlert.ContextCode
and	EmergencyDepartment.SourceEmergencyDepartmentCode = cast(BaseAlert.LocationID as varchar)

where
	BaseAlert.CreatedDate >= @StartDate

union

select
	BaseAlert.MergeAlertRecno
	,CreatedTime
	,CreatedDateID = BaseAlertReference.CreatedDateID
	,ClosedTime
	,DirectorateID = Directorate.DirectorateID					
	,SpecialtyID = BaseAlertReference.SpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = Ward.SourceWardID
from
	WarehouseOLAPMergedV2.Observation.BaseAlert

inner join WarehouseOLAPMergedV2.Observation.BaseAlertReference
on	BaseAlertReference.MergeAlertRecno = BaseAlert.MergeAlertRecno

inner join WarehouseOLAPMergedV2.WH.Directorate
on	coalesce(BaseAlert.DirectorateCode, '9') = Directorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseAlert.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseAlert.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Ward
on	Ward.SourceContextCode = BaseAlert.ContextCode
and	Ward.SourceWardCode = cast(BaseAlert.LocationID as varchar)

where
	BaseAlert.CreatedDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
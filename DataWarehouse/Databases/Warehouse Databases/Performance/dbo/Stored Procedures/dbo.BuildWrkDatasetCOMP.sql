﻿
CREATE proc [dbo].[BuildWrkDatasetCOMP] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'COMP'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetCOMP

insert into dbo.WrkDatasetCOMP
(
	DatasetRecno
	,ContextCode
	,ReceiptDateID
	,CategoryTypeCode
	,CaseTypeCode
	,DirectorateID
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID
	,Complaint25DayTargetDateID
	,Complaint40DayTargetDateID	
	,Complaint25DayTargetDate
	,Complaint40DayTargetDate	
	,ReceiptDate
	,ConsentDate
	,ResolutionDate
	,SequenceNo
	,Reopened
	,ResponseDate
)

select
	DatasetRecno = BaseComplaint.MergeComplaintRecno
	,BaseComplaint.ContextCode
	,BaseComplaintReference.ReceiptDateID
	,BaseComplaint.CategoryTypeCode
	,BaseComplaint.CaseTypeCode
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = BaseComplaintReference.WardID
	,Complaint25DayTargetDateID = BaseComplaintReference.TargetDate25DayID
	,Complaint40DayTargetDateID = BaseComplaintReference.TargetDate40DayID
	,Complaint25DayTargetDate = BaseComplaint.TargetDate25Day
	,Complaint40DayTargetDate = BaseComplaint.TargetDate40Day
	,BaseComplaint.ReceiptDate
	,BaseComplaint.ConsentDate
	,BaseComplaint.ResolutionDate
	,BaseComplaint.SequenceNo
	,BaseComplaint.Reopened
	,BaseComplaint.ResponseDate
from
	WarehouseOLAPMergedV2.Complaint.BaseComplaint

inner join WarehouseOLAPMergedV2.Complaint.BaseComplaintReference
on	BaseComplaint.MergeComplaintRecno = BaseComplaintReference.MergeComplaintRecno
	
inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = coalesce(BaseComplaint.DirectorateCode, '9')

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = BaseComplaint.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = BaseComplaint.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = BaseComplaint.ContextCode
and Consultant.SourceConsultantCode = '-1'

where
	BaseComplaint.ReceiptDate >= @StartDate
and BaseComplaint.GradeCode not in
							(
							'0x4F20' --De-Escalated
							,'0x5220' --withdrawn
							,'0x5320' --out of time
							,'0x5420' --Consent/Info Not Received  CH 20150401                       
							,'0x5120'
							)
select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
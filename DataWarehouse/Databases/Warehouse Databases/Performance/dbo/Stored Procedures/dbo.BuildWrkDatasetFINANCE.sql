﻿
CREATE proc [dbo].[BuildWrkDatasetFINANCE] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'FINANCE'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetFINANCE 

insert into dbo.WrkDatasetFINANCE
(
	DatasetRecno
	,CensusDate
	,CensusDateID
	,Budget 
	,AnnualBudget
	,Actual 
	,Division
	,DirectorateID
	,SiteID
	,SpecialtyID
	,ClinicianID
	,ServicePointID
)
select
	DatasetRecno = Expenditure.MergeExpenditureRecno 
	,CensusDate = Expenditure.CensusDate
	,CensusDateID = Reference.CensusDateID
	,Budget = Expenditure.Budget
	,AnnualBudget = 
				(Expenditure.AnnualBudget / cast(12 as float)) * cast(right(FinancialMonthKey, 2) as int)
	,Actual = Expenditure.Actual
	,Division = Expenditure.Division
	,DirectorateID = 
				coalesce(
					Directorate.DirectorateID
					,15
					)					
	,SiteID = Site.SourceSiteID	
	,SpecialtyID = Specialty.SourceSpecialtyID
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID	= ServicePoint.SourceServicePointID	
from
	WarehouseOLAPMergedV2.Finance.BaseExpenditure Expenditure

inner join WarehouseOLAPMergedV2.Finance.BaseExpenditureReference Reference
on Expenditure.MergeExpenditureRecno = Reference.MergeExpenditureRecno

inner join WarehouseOLAPMergedV2.WH.Calendar 
on Reference.CensusDateID = Calendar.DateID
	
inner join WarehouseOLAPMergedV2.Allocation.DatasetAllocation
on	DatasetAllocation.DatasetRecno = Expenditure.MergeExpenditureRecno
and	DatasetAllocation.DatasetCode = 'FINEXP'
and	DatasetAllocation.AllocationTypeID = 10

inner join WarehouseOLAPMergedV2.Allocation.Allocation
on	Allocation.AllocationTypeID = DatasetAllocation.AllocationTypeID
and	Allocation.AllocationID = DatasetAllocation.AllocationID

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Allocation.SourceAllocationID = Directorate.DirectorateCode
	
inner join WarehouseOLAPMergedV2.WH.Site
on Site.SourceContextCode = 'CMFT||ORCL'
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = 'CMFT||ORCL'
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = 'CMFT||ORCL'
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'
	
where
	Expenditure.CensusDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
﻿

CREATE proc [dbo].[BuildWrkDatasetFALL] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'FALL'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetFALL

insert into dbo.WrkDatasetFALL
(
	DatasetRecno
	,FallDateID
	,SourceFallSeverityCode
	,Validated
	,StartDirectorateID
	,EndDirectorateID	
	,SpecialtyID
	,StartSiteID 
	,EndSiteID 
	,ClinicianID
	,ServicePointID
)	

select
	BaseFall.MergeFallRecno
	,FallDateID = BaseFallReference.FallDateID
	,FallSeverity.SourceFallSeverityCode
	,Validated
	,StartDirectorateID = StartDirectorate.DirectorateID
	,EndDirectorateID = EndDirectorate.DirectorateID					
	,SpecialtyID = BaseEncounterReference.SpecialtyID	
	,StartSiteID = BaseEncounterReference.StartSiteID	
	,EndSiteID = BaseEncounterReference.EndSiteID
	,ClinicianID = BaseEncounterReference.ConsultantID
	,ServicePointID = BaseFallReference.WardID
from
	WarehouseOLAPMergedV2.APC.BaseFall

inner join WarehouseOLAPMergedV2.APC.BaseFallReference
on	BaseFallReference.MergeFallRecno = BaseFall.MergeFallRecno

inner join WarehouseOLAPMergedV2.APC.FallSeverity
on BaseFallReference.InitialSeverityID = FallSeverity.SourceFallSeverityID

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBaseFall
on	BaseFall.MergeFallRecno = BaseEncounterBaseFall.MergeFallRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterBaseFall.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode

where
	BaseFall.FallDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
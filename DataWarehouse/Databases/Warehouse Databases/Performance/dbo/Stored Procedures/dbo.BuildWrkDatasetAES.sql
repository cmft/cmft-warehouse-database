﻿

CREATE proc [dbo].[BuildWrkDatasetAES] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'AES'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetAES

insert into dbo.WrkDatasetAES
(
	DatasetRecno
	,ContextCode
	,DistrictNo
	,StageCode
	,BreachStatusCode
	,DepartmentTypeCode
	,Duration
	,UnplannedReattend7Days
	,LeftWithoutBeingSeen
	,ArrivalModeCode
	,AttendanceDisposalCode
	,EncounterDate
	,EncounterDateID
	,DirectorateID
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID
)

select
	DatasetRecno = Fact.MergeEncounterRecno
	,Encounter.ContextCode
	,Encounter.DistrictNo
	,Stage.StageCode
	,BreachStatus.BreachStatusCode
	,Encounter.DepartmentTypeCode
	,Duration = Fact.StageDurationMinutes
	,UnplannedReattend7Day = Fact.UnplannedReattend7DayCases
	,LeftWithoutBeingSeen = Fact.LeftWithoutBeingSeenCases
	,ArrivalMode.NationalArrivalModeCode
	,AttendanceDisposal.NationalAttendanceDisposalCode
	,EncounterDate = Calendar.TheDate
	,EncounterDateID = Fact.StageStartDateID
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = Specialty.SourceSpecialtyID
	,SiteID = Site.SourceSiteID
	,ClinicianID = StaffMember.SourceStaffMemberID
	,ServicePointID = ServicePoint.SourceServicePointID	
from
	WarehouseOLAPMergedV2.AE.FactEncounter Fact

inner join WarehouseOLAPMergedV2.AE.BaseEncounter Encounter 
on Encounter.MergeEncounterRecno = Fact.MergeEncounterRecno
 
inner join WarehouseOLAPMergedV2.AE.Stage 
on Stage.StageID = Fact.StageID
 
inner join WarehouseOLAPMergedV2.WH.Calendar 
on Calendar.DateID = Fact.StageStartDateID
 
inner join WarehouseOLAPMergedV2.AE.BreachStatus 
on BreachStatus.BreachStatusID = Fact.StageBreachStatusID
 
inner join WarehouseOLAPMergedV2.AE.ArrivalMode 
on ArrivalMode.SourceArrivalModeID = Fact.ArrivalModeID

inner join WarehouseOLAPMergedV2.AE.AttendanceDisposal 
on AttendanceDisposal.SourceAttendanceDisposalID = Fact.AttendanceDisposalID
 
inner join WarehouseOLAPMergedV2.AE.DepartmentType 
on DepartmentType.DepartmentTypeID = Fact.DepartmentTypeID

-- joins below changed to left for performance reasons

left outer join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = '9'

left outer join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = Encounter.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

left outer join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = Encounter.ContextCode
and Site.SourceSiteCode = '-1'

left outer join WarehouseOLAPMergedV2.AE.StaffMember
on	StaffMember.SourceContextCode = Encounter.ContextCode
and coalesce(Encounter.StaffMemberCode, '-1') =  StaffMember.SourceStaffMemberCode

left outer join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	Stage.StageCode in 
					(
					'ARRIVALTOTRIAGEADJUSTED'
					,'SEENFORTREATMENTADJUSTED'
					,'INDEPARTMENTADJUSTED'
					)
and	Encounter.ArrivalDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Dataset: ' + @DatasetCode +
		', Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
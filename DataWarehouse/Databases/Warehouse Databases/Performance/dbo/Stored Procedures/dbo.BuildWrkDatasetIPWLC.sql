﻿

CREATE proc [dbo].[BuildWrkDatasetIPWLC] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'IPWLC'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetIPWLC 

insert into dbo.WrkDatasetIPWLC
(
	DatasetRecno
	,SourceUniqueID
	,ContextCode
	,DirectorateCode
	,SpecialtyCode
	,ClinicianCode
	,SiteCode
	,CensusDate
	,CensusDateID
	,TCIDate
	,DirectorateID
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID
	,Duration
)

select
	E.MergeEncounterRecno
	,E.SourceUniqueID
	,E.ContextCode
	,DirectorateCode = Directorate.DirectorateCode
	,E.SpecialtyCode
	,ClinicianCode = E.ConsultantCode
	,SiteCode = E.SiteCode
	,E.CensusDate
	,R.CensusDateID
	,E.TCIDate
	,DirectorateID = Directorate.DirectorateID				
	,SpecialtyID = R.SpecialtyID
	,SiteID = R.SiteID
	,ClinicianID = R.ConsultantID
	,ServicePointID = R.WardID	
	,Duration = [LengthOfWait]/7 -- to get weeks	
from
	WarehouseOLAPMergedV2.APCWL.BaseEncounter E
	
inner join WarehouseOLAPMergedV2.APCWL.BaseEncounterReference R 
on R.MergeEncounterRecno = E.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.WaitTypeBase TY 
on TY.WaitTypeID = R.WaitTypeID

inner join WarehouseOLAPMergedV2.WH.Calendar Census 
on Census.TheDate = E.CensusDate and Census.TheDate = Census.LastDayOfMonth -- We only require last day of month as census

inner join WarehouseOLAPMergedV2.WH.Directorate
on	coalesce(E.DirectorateCode, '9') = Directorate.DirectorateCode
	
where
	TY.WaitType = 'Active'
and	E.CensusDate >= @StartDate


select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
﻿
CREATE proc [dbo].[BuildWrkDatasetMAT] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'MAT'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetMAT 

insert into dbo.WrkDatasetMAT
(
	DatasetRecno
	,ContextCode
	,InfantDateOfBirth
	,InfantDateOfBirthID
	,MethodDelivery
	,Perineum
	,BirthOrder
	,Outcome
	,DirectorateID 		
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
)

select
	DatasetRecno = Birth.BirthRecno
	,Birth.ContextCode
	,Birth.InfantDateOfBirth
	,InfantDateOfBirthID = DateOfBirth.DateID
	,Birth.MethodDelivery
	,Birth.Perineum
	,Birth.BirthOrder
	,Birth.Outcome
	,DirectorateID = Directorate.DirectorateID			
	,SpecialtyID = Specialty.SourceSpecialtyID	
	,SiteID = Site.SourceSiteID	
	,ClinicianID = Consultant.SourceConsultantID
	,ServicePointID = ServicePoint.SourceServicePointID
from 
	WarehouseOLAPMergedV2.Maternity.BaseBirth Birth
    
inner join WarehouseOLAPMergedV2.WH.Calendar DateOfBirth -- DG 28.2.14 - changed to inner join
on cast(Birth.InfantDateOfBirth as date) = DateOfBirth.TheDate

inner join WarehouseOLAPMergedV2.WH.Directorate
on	Directorate.DirectorateCode = '101' -- 28/01/2016 RR Was 9 - default, Marianne L advised that the birth data should be under St Mary's not unnassigned (mortality dashboard).  Discussed with DG, ideally the directorate needs to be done in the ETL process.  E-mailed BR to see if there is any reason why it wouldn't be St Mary's?

inner join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceContextCode = Birth.ContextCode
and Specialty.SourceSpecialtyCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = Birth.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.Consultant
on	Consultant.SourceContextCode = Birth.ContextCode
and Consultant.SourceConsultantCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	Birth.InfantDateOfBirth >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
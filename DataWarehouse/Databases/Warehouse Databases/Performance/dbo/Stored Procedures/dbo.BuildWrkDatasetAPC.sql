﻿
CREATE proc [dbo].[BuildWrkDatasetAPC] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'APC'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetAPC

insert into dbo.WrkDatasetAPC
(
	 DatasetRecno
	,ContextCode
	,SourcePatientNo
	,DateOfBirth
	,ProviderSpellNo
	,SourceEncounterNo
	,StartDirectorateCode
	,EndDirectorateCode
	,SpecialtyCode
	,Specialty
	,NationalSpecialtyCode
	,ClinicianCode
	,StartSiteCode
	,EndSiteCode
	,StartWardCode
	,EndWardCode
	,AdmissionDateID
	,AdmissionDate
	,AdmissionTime
	,DischargeDateID
	,DischargeDate
	,DischargeTime
	,EpisodeStartDateID
	,EpisodeStartDate
	,EpisodeEndDateID
	,EpisodeEndDate
	,FirstEpisodeInSpellIndicator
	,NationalLastEpisodeInSpellCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,SecondaryDiagnosisCode13
	,PrimaryProcedureCode
	,PrimaryProcedureDate
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteDateID	
	,AdmissionMethodCode
	,AdmissionTypeCode
	,NationalAdmissionSourceCode
	,NationalDischargeMethodCode
	,NationalDischargeDestinationCode
	,NationalPatientClassificationCode
	,NationalIntendedManagementCode	
	,EddCreatedTime
	,ExpectedDateOfDischarge
	,DischargeSummaryRequired
	,DischargeSummarySignedTime
	,PatientCategoryCode
	,HarmFreeCare
	,Division
	,MortalityReviewStatusCode
	,DominantForDiagnosis
	,Died
	,RiskScore
	,HRGCode
	,ComplicationComorbidity
	,TheatrePrimaryProcedureExists
	,PatientClassificationCode
	,PalliativeCare
	,CasenoteNumber
	,EpisodeNo
	,EncounterDateID
	,CharlsonDiagnosisCodeCount
	,StartDirectorateID 
	,EndDirectorateID 			
	,SpecialtyID 
	,StartSiteID
	,EndSiteID 
	,ClinicianID 
	,StartServicePointID
	,EndServicePointID
	,HasAssessmentUnitEpisode
	,IsWellBabySpell
	,CharlsonIndex
	,VTE
	,VTECompleteAndOrExclusion
	,FirstOperationInSpellTime
	,Readmission28Day
	,Readmission28DayDenominator
	--,ActivityMetricCode
) 
select 
	DatasetRecno = Encounter.MergeEncounterRecno
	,Encounter.ContextCode
	,Encounter.SourcePatientNo
	,Encounter.DateOfBirth
	,Encounter.GlobalProviderSpellNo -- 20140808 This goes in ProviderSpellNo field, may need to rename to Global, but need to understand the impact, ie how many views reference Provider spell no and encounter no.
	,Encounter.GlobalEpisodeNo	-- 20140808 This goes in SourceEncounterNo field, may need to rename to Global, but need to understand the impact, ie how many views reference Provider spell no and encounter no.
	,Encounter.StartDirectorateCode -- DG 21.10.14 - Should be using the value stamped on each particular consultant episode
	,Encounter.EndDirectorateCode --  DG 21.10.14 - Should be using the value stamped on each particular consultant episode
	,Encounter.SpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
	,Encounter.ConsultantCode
	,Encounter.StartSiteCode
	,Encounter.EndSiteCode
	,Encounter.StartWardTypeCode
	,Encounter.EndWardTypeCode
	,Encounter.AdmissionDateID
	,Encounter.AdmissionDate
	,Encounter.AdmissionTime
	,Encounter.DischargeDateID
	,Encounter.DischargeDate
	,Encounter.DischargeTime
	,Encounter.EpisodeStartDateID
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeEndDateID
	,Encounter.EpisodeEndDate
	,Encounter.FirstEpisodeInSpellIndicator
	,NationalLastEpisodeInSpellCode = NationalLastEpisodeInSpellIndicator			
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.SecondaryDiagnosisCode13
	,Encounter.PrimaryProcedureCode
	,Encounter.PrimaryProcedureDate
	,Encounter.ClinicalCodingCompleteDate 
	,ClinicalCodingCompleteDateID = CalendarClinicalCodingComplete.DateID
	,AdmissionMethodCode = AdmissionMethod.NationalAdmissionMethodCode
	,AdmissionTypeCode = AdmissionMethod.AdmissionTypeCode
	,NationalAdmissionSourceCode = AdmissionSource.NationalAdmissionSourceCode
	,NationalDischargeMethodCode = DischargeMethod.NationalDischargeMethodCode
	,NationalDischargeDestinationCode
	,NationalPatientClassificationCode = PatientClassification.NationalPatientClassificationCode
	,NationalIntendedManagementCode = IntendedManagement.NationalIntendedManagementCode		
	,Encounter.EddCreatedTime
	,Encounter.ExpectedDateOfDischarge
	,DischargeSummaryRequired =
				case
				when DischargeLetterExclusion.AllocationID = 
					(
					select
						Allocation.AllocationID
					from
						WarehouseOLAPMergedV2.Allocation.Allocation
					where
						Allocation.SourceAllocationID = '-1'
					and	Allocation.AllocationTypeID = 14
					)
				then 1
				else 0
				end
	,DischargeSummarySignedTime = BaseDocument.SignedTime 
	,Encounter.PatientCategoryCode		
	,HarmFreeCare =
		case
		when not exists
				(
				select
					1
				from
					(
					-- need to work on conditions of each harm type?
					select
						MergeEncounterRecno
					from
						WarehouseOLAPMergedV2.APC.BaseEncounterBaseFall
					union 
					select
						MergeEncounterRecno
					from
						WarehouseOLAPMergedV2.APC.BaseEncounterBasePressureUlcer
					union 
					select
						MergeEncounterRecno
					from
						WarehouseOLAPMergedV2.APC.BaseEncounterBaseUrinaryTractInfection
					union 
					select
						MergeEncounterRecno
					from
						WarehouseOLAPMergedV2.APC.BaseEncounterBaseVTECondition
					) HarmFreeCare
				where
					Encounter.MergeEncounterRecno = HarmFreeCare.MergeEncounterRecno
				)
		then 1
		end	
	,Division = StartDirectorate.Division
	,MortalityReviewStatusCode = ReviewStatus.LocalReviewStatusCode--20160114 RR discussed with GS, changed from SourceReviewStatusCode to Local
	,DominantForDiagnosis = BaseSHMI.DominantForDiagnosis
	,Died = BaseSHMI.Died
	,RiskScore = BaseSHMI.RiskScore
	,HRGCode = HRG4Spell.HRGCode
	,ComplicationComorbidity = HRG.ComplicationComorbidity -- HRG has complications and / or co-morbidities
	,TheatrePrimaryProcedureExists = -- Theatre system primary procedure code exists
		cast(
			case
			when exists 
				(
				select
					1					
				from
					WarehouseOLAPMergedV2.APC.BaseEncounterProcedureDetail 		

				inner join WarehouseOLAPMergedV2.Theatre.BaseProcedureDetail 
				on	BaseEncounterProcedureDetail.ProcedureDetailSourceUniqueID = BaseProcedureDetail.SourceUniqueID
				and BaseProcedureDetail.ContextCode = BaseEncounterProcedureDetail.ProcedureDetailContextCode
				
				where
					Encounter.EncounterRecno = BaseEncounterProcedureDetail.EncounterRecno
				and Encounter.ContextCode = BaseEncounterProcedureDetail.EncounterContextCode
				and BaseProcedureDetail.PrimaryProcedureFlag = '1'				
				) 
			then 1
			else 0
			end
		as bit)
	,Encounter.PatientClassificationCode
	,PalliativeCare = 
					case
					when exists
						(
						select
							1
						from
							WarehouseOLAPMergedV2.APC.BaseDiagnosis
						where
							BaseDiagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
						and BaseDiagnosis.DiagnosisCode = 'Z51.5'											
						)
					then 1
					else 0
					end
	,Encounter.CasenoteNumber
	,EpisodeNo = Encounter.SourceSpellNo
	,EncounterDateID = Encounter.EpisodeStartDateID
	,CharlsonDiagnosisCodeCount = Charlson.CharlsonDiagnosisCodeCount
	,StartDirectorateID = StartDirectorate.DirectorateID
	,EndDirectorateID = EndDirectorate.DirectorateID					
	,SpecialtyID = Encounter.SpecialtyID
	,StartSiteID = Encounter.StartSiteID
	,EndSiteID = Encounter.EndSiteID
	,ClinicianID = Encounter.ConsultantID
	,StartServicePointID = Encounter.StartWardID
	,EndServicePointID = Encounter.EndWardID
	,HasAssessmentUnitEpisode = 
		case
		when exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.APC.BaseWardStay
			where
				BaseWardStay.ProviderSpellNo = Encounter.ProviderSpellNo
			and	BaseWardStay.ContextCode = Encounter.ContextCode
			and BaseWardStay.WardCode in 
										(
										'DASS'
										,'ESTU'
										,'PMAU'
										,'MAU'
										,'MAU2'
										,'AMU'
										,'OMU'
										,'POAU'
										,'SAL'
										,'SAU'
										)	
			)
		then 1
		else 0
		end
	,IsWellBabySpell =
		case
		when exists
			(
			select
				1
			from
				WarehouseOLAPMergedV2.APC.Encounter WellBabyEncounter

			inner join WarehouseOLAPMergedV2.WH.Specialty
			on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID

			where
				WellBabyEncounter.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
			and Specialty.NationalSpecialtyCode != '424'
			)
		then 0
		else 1
		end
	,Encounter.CharlsonIndex
	,VTE =
		case
		when
			(
				(
					Encounter.AgeCode like '%Years'
				and	left(Encounter.AgeCode , 2) > '17'
				)
			or	Encounter.AgeCode = '99+'
			)
		and not --regular day or night
			(
				AdmissionMethod.AdmissionTypeCode = 'EL' --elective
			and	PatientClassification.NationalPatientClassificationCode in ( '3' , '4' )
			)
		then 1
		else 0
		end
	,VTECompleteAndOrExclusion =
		case
		when
			Encounter.VTECategoryCode = 'C'
		or	coalesce(
				VTEExclusion.AllocationID
				,
				(
				select
					Allocation.AllocationID
				from
					WarehouseOLAPMergedV2.Allocation.Allocation
				where
					Allocation.SourceAllocationID = '-1'
				and	Allocation.AllocationTypeID = 3
				)
			) != 
				(
				select
					Allocation.AllocationID
				from
					WarehouseOLAPMergedV2.Allocation.Allocation
				where
					Allocation.SourceAllocationID = '-1'
				and	Allocation.AllocationTypeID = 3
				)			
		then 1
		else 0
		end
	,FirstOperationInSpellTime = FirstOperationInSpell.OperationDate
	,Readmission28Day = 
		case
		when FactReadmission.ReadmissionEncounterMergeEncounterRecno is not null
		then 1
		else 0
		end
	,Readmission28DayDenominator = 
		case
		when FactReadmissionDenominator.SpellEncounterMergeEncounterRecno is not null
		then 1
		else 0
		end
	--,ActivityMetricCode = ActivityMetric.MetricCode
from
	WarehouseOLAPMergedV2.APC.Encounter

left join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
on Encounter.StartDirectorateCode = StartDirectorate.DirectorateCode

left join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
on Encounter.EndDirectorateCode = EndDirectorate.DirectorateCode

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Encounter.SpecialtyID

left join WarehouseOLAPMergedV2.APC.AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = Encounter.AdmissionMethodID

left join WarehouseOLAPMergedV2.APC.AdmissionSource
on	AdmissionSource.SourceAdmissionSourceID = Encounter.AdmissionSourceID

left join WarehouseOLAPMergedV2.APC.DischargeMethod
on	DischargeMethod.SourceDischargeMethodID = Encounter.DischargeMethodID

left join WarehouseOLAPMergedV2.APC.DischargeDestination
on	DischargeDestination.SourceDischargeDestinationID = Encounter.DischargeDestinationID

left join WarehouseOLAPMergedV2.APC.IntendedManagement
on	IntendedManagement.SourceIntendedManagementID = Encounter.IntendedManagementID

left join WarehouseOLAPMergedV2.APC.PatientClassification
on	PatientClassification.SourcePatientClassificationID = Encounter.PatientClassificationID

left join WarehouseOLAPMergedV2.WH.Calendar CalendarClinicalCodingComplete
on CalendarClinicalCodingComplete.TheDate = Encounter.ClinicalCodingCompleteDate

left join WarehouseOLAPMergedV2.APC.BaseSHMI
on Encounter.MergeEncounterRecno = BaseSHMI.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.HRG4Spell 
on HRG4Spell.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.HRG 
on HRG.HRGCode = HRG4Spell.HRGCode

left join
	(
	select
		MergeEncounterRecno
		,CharlsonDiagnosisCodeCount = count(*)
	from
		WarehouseOLAPMergedV2.APC.BaseDiagnosis
		
	inner join WarehouseOLAPMergedV2.WH.Diagnosis
	on	BaseDiagnosis.DiagnosisCode = Diagnosis.DiagnosisCode
	and	IsCharlson = 1

	group by
		MergeEncounterRecno	

	) Charlson
on	Charlson.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.FactMortalityEncounter
on FactMortalityEncounter.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.Mortality.ReviewStatus
on	ReviewStatus.SourceReviewStatusID = FactMortalityEncounter.MortalityReviewStatusID

left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation VTEExclusion
on	VTEExclusion.AllocationTypeID = 3
and	VTEExclusion.DatasetCode = 'APC'
and	VTEExclusion.DatasetRecno= Encounter.MergeEncounterRecno

left outer join
	(
	select
		GlobalProviderSpellNo
		,OperationDate = min(OperationDate)
	from
		WarehouseOLAPMergedV2.APC.Operation

	inner join WarehouseOLAPMergedV2.APC.Encounter
	on Operation.MergeEncounterRecno = Encounter.MergeEncounterRecno

	group by
		GlobalProviderSpellNo

	) FirstOperationInSpell
on	Encounter.GlobalProviderSpellNo = FirstOperationInSpell.GlobalProviderSpellNo

left join WarehouseOLAPMergedV2.Allocation.DatasetAllocation DischargeLetterExclusion
on	DischargeLetterExclusion.AllocationTypeID = 14
and	DischargeLetterExclusion.DatasetCode = 'APC'
and	DischargeLetterExclusion.DatasetRecno= Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.APC.BaseEncounterBaseDocument 
on	BaseEncounterBaseDocument.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	BaseEncounterBaseDocument.SequenceNo = 1

left join	WarehouseOLAPMergedV2.Dictation.BaseDocument
on BaseEncounterBaseDocument.MergeDocumentRecno = BaseDocument.MergeDocumentRecno

left join WarehouseOLAPMergedV2.APC.FactReadmission
on	FactReadmission.InitialAdmissionEncounterMergeEncounterRecno = Encounter.MergeEncounterRecno
and	FactReadmission.ReadmissionTypeID = 2 --CQUIN - 28 day readmission
and	FactReadmission.TrustWide = 1

left join WarehouseOLAPMergedV2.APC.FactReadmissionDenominator
on	FactReadmissionDenominator.SpellEncounterMergeEncounterRecno = Encounter.MergeEncounterRecno
and	FactReadmissionDenominator.ReadmissionTypeID = 2 --CQUIN - 28 day readmission

--left join WarehouseOLAPMergedV2.Activity.Encounter ActivityEncounter
--on	ActivityEncounter.SourceRecno = Encounter.MergeEncounterRecno
--and	ActivityEncounter.SourceSystem = 'Warehouse'
--and	ActivityEncounter.SourceDatasetCode = 'APC'

--left join WarehouseOLAPMergedV2.WH.FactActivity
--on	ActivityEncounter.EncounterRecno = FactActivity.EncounterRecno
--and FactActivity.Reportable = 1

--left join WarehouseOLAPMergedV2.WH.ActivityMetric
--on	FactActivity.MetricID = ActivityMetric.MetricID

where
	Encounter.EpisodeStartDate >= @StartDate
	


select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
﻿
CREATE proc [dbo].[BuildWrkDatasetWS] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'WS'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetWS 

insert into dbo.WrkDatasetWS
(
	 DatasetRecno
	,ContextCode
	,DateOfBirth
	,StartDirectorateCode
	,EndDirectorateCode
	,SpecialtyCode
	,Specialty
	,NationalSpecialtyCode
	,ConsultantCode
	,StartSiteCode
	,EndSiteCode
	,StartWardCode
	,EndWardCode
	,StartDate
	,StartDateID
	,EndActivityCode
	,AdmissionTime
	,StartDirectorateID 
	,EndDirectorateID 				
	,SpecialtyID 
	,StartSiteID 
	,EndSiteID 
	,ClinicianID
	,ServicePointID 
) 
select
	 DatasetRecno = WardStay.MergeEncounterRecno
	,WardStay.ContextCode
	,Episode.DateOfBirth
	,StartDirectorateCode
	,EndDirectorateCode
	,Episode.SpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
	,Episode.ConsultantCode
	,StartSiteCode = WardStay.SiteCode
	,EndSiteCode = WardStay.SiteCode
	,StartWardCode = WardStay.WardCode
	,EndWardCode = WardStay.WardCode
	,WardStay.StartDate
	,CalendarWardStayStart.DateID
	,WardStay.EndActivityCode
	,Episode.AdmissionTime
	,StartDirectorateID = StartDirectorate.DirectorateID
	,EndDirectorateID = EndDirectorate.DirectorateID	
	,SpecialtyID = EpisodeReference.SpecialtyID
	,StartSiteID = WardStayReference.SiteID
	,EndSiteID = WardStayReference.SiteID
	,ClinicianID = EpisodeReference.ConsultantID
	,ServicePointID = WardStayReference.WardID
from
	WarehouseOLAPMergedV2.APC.BaseWardStay WardStay

inner join WarehouseOLAPMergedV2.APC.BaseEncounter Episode
on	Episode.ProviderSpellNo = WardStay.ProviderSpellNo -- in same Spell
and WardStay.StartDate between Episode.EpisodeStartDate and coalesce(Episode.EpisodeEndDate, getdate()) -- within which the Ward Stay start falls
and not exists -- and there exists no later episode which also qualifies
		(
		select
			1 
		from
			WarehouseOLAPMergedV2.APC.BaseEncounter LaterEpisode
		where
			LaterEpisode.ProviderSpellNo = WardStay.ProviderSpellNo -- in same Spell
		and WardStay.StartDate between LaterEpisode.EpisodeStartDate and coalesce(LaterEpisode.EpisodeEndDate, getdate()) -- within which the Ward Stay start falls
		and 
			(
				LaterEpisode.EpisodeStartDate > Episode.EpisodeStartDate -- this one starts late
			or	coalesce(LaterEpisode.EpisodeEndDate, getdate()) > coalesce(Episode.EpisodeEndDate, getdate()) -- or ends later
			or -- (tie-breaker) starts and ends at the same time and has a later Recno
				(
					LaterEpisode.EpisodeStartDate = Episode.EpisodeStartDate
				and	coalesce(LaterEpisode.EpisodeEndDate, getdate()) = coalesce(Episode.EpisodeEndDate, getdate())
				and LaterEpisode.MergeEncounterRecno > Episode.MergeEncounterRecno
				)
			)
		)

inner join WarehouseOLAPMergedV2.APC.BaseWardStayReference WardStayReference
on	WardStayReference.MergeEncounterRecno = WardStay.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference EpisodeReference
on	EpisodeReference.MergeEncounterRecno = Episode.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
on Episode.StartDirectorateCode = StartDirectorate.DirectorateCode

left join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
on Episode.EndDirectorateCode = EndDirectorate.DirectorateCode

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = EpisodeReference.SpecialtyID

left join WarehouseOLAPMergedV2.WH.Calendar CalendarWardStayStart
on CalendarWardStayStart.TheDate = WardStay.StartDate

where
	WardStay.StartDate  >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
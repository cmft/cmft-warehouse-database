﻿
CREATE proc [dbo].[BuildWrkDatasetOPER] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'OPER'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetOPER	

insert into dbo.WrkDatasetOPER	
(
	DatasetRecno
	,ContextCode
	,OperationDate
	,OperationDateID
	,CancelReasonCode
	,TurnaroundTime		
	,TurnaroundTimeCount
	,DirectorateID
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID
	,StartServicePointID
	,UtilisationMinutes
	,AnaesthetistActiveTheatreTime 
	,SurgeonActiveTheatreTime 
	,CancelledOnDay 
	,SessionSourceUniqueID
)
	
select
	DatasetRecno = F.MergeRecno
	,B.ContextCode
	,OperationDate = C.TheDate
	,OperationDateID = C.DateID
	,CancelReasonCode = CR.SourceCancelReasonCode
	,TurnaroundTime = datediff(minute, B.OperationEndDate, B.NextOperationAnaestheticInductionTime) -- Please note this is a different definition to that in the Fact table, Fact table excludes negative results.  The definition provided by the Transformation team  - 'Where a subsequent patient’s anaesthesia is induced prior to the previous patient entering recovery the turnaround time will be a negative numerical value.'
	,TurnaroundTimeCount = 
			case 
			when B.NextOperationAnaestheticInductionTime is not null then 1 
			else 0 
			end
	,DirectorateID = F.DirectorateID
	,SpecialtyID = F.SessionSpecialtyID
	,SiteID = Site.SourceSiteID	
	,ClinicianID = F.SessionConsultantID
	,ServicePointID = SourceServicePointID
	,StartServicePointID = SourceServicePointID

	,UtilisationMinutes =
		datediff(
			 minute
			,coalesce(
				 B.AnaestheticInductionTime
				,B.InAnaestheticTime
			)
			,coalesce(
				 B.OperationEndDate
				,B.InRecoveryTime
				,B.ReadyToDepartTime
			)
		)
		
	,AnaesthetistActiveTheatreTime = 
			case 
			when B.AnaestheticInductionTime is not null 
			and	coalesce(B.InRecoveryTime,B.OperationEndDate) is not null
			then datediff(
						minute
						,B.AnaestheticInductionTime
						,coalesce(
								B.InRecoveryTime
								,B.OperationEndDate
							)
						)
			else null
			end
	
	,SurgeonActiveTheatreTime = 
			case 
			when B.OperationStartDate is not null 
			and coalesce(B.InRecoveryTime,B.OperationEndDate) is not null
			then datediff(
						minute
						,B.OperationStartDate
						,coalesce(
							B.InRecoveryTime
							,B.OperationEndDate
							)
						)
			else null
			end
		
	,CancelledOnDay = 
			case
			when cast(BaseCancellation.CancellationDate as date) = cast(C.TheDate as date) 
			then 1
			else 0
			end
	,B.SessionSourceUniqueID
from
	WarehouseOLAPMergedV2.Theatre.FactOperation F

inner join WarehouseOLAPMergedV2.Theatre.BaseOperationDetail B 
on	B.MergeRecno = F.MergeRecno

inner join WarehouseOLAPMergedV2.Theatre.CancelReason CR 
on	CR.SourceCancelReasonID = F.CancelReasonID

inner join WarehouseOLAPMergedV2.WH.Calendar C 
on	C.DateID = F.OperationDateID

inner join WarehouseOLAPMergedV2.WH.Site
on	Site.SourceContextCode = B.ContextCode
and Site.SourceSiteCode = '-1'

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = F.TheatreID

left join WarehouseOLAPMergedV2.Theatre.BaseCancellation
on	BaseCancellation.OperationDetailSourceUniqueID = B.SourceUniqueID
and	BaseCancellation.ContextCode = B.ContextCode
and	not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.Theatre.BaseCancellation Previous
	where
		Previous.OperationDetailSourceUniqueID = B.SourceUniqueID
	and	Previous.ContextCode = B.ContextCode
	and	(
			Previous.CancellationDate > BaseCancellation.CancellationDate
		or	(
				Previous.CancellationDate = BaseCancellation.CancellationDate
			and	Previous.SourceUniqueID > BaseCancellation.SourceUniqueID
			)
		)
	)
where
	C.TheDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
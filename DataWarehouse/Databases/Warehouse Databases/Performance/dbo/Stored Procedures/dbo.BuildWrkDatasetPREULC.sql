﻿

CREATE proc [dbo].[BuildWrkDatasetPREULC] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'PREULC'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetPREULC

insert into dbo.WrkDatasetPREULC
(
	DatasetRecno
	,IdentifiedTime
	,IdentifiedDateID
	,SourcePressureUlcerCategoryCode
	,Validated
	,AdmissionTime
	,StartDirectorateID
	,EndDirectorateID		
	,SpecialtyID
	,StartSiteID 
	,EndSiteID 
	,ClinicianID
	,ServicePointID
)	

select
	BasePressureUlcer.PressureUlcerRecno
	,BasePressureUlcer.IdentifiedTime
	,BasePressureUlcerReference.IdentifiedDateID
	,PressureUlcerCategory.SourcePressureUlcerCategoryCode
	,Validated
	,BaseEncounter.AdmissionTime
	,StartDirectorateID = StartDirectorate.DirectorateID
	,EndDirectorateID = EndDirectorate.DirectorateID					
	,SpecialtyID = BaseEncounterReference.SpecialtyID	
	,StartSiteID = BaseEncounterReference.StartSiteID	
	,EndSiteID = BaseEncounterReference.EndSiteID
	,ClinicianID = BaseEncounterReference.ConsultantID
	,ServicePointID = BasePressureUlcerReference.WardID
from
	WarehouseOLAPMergedV2.APC.BasePressureUlcer

inner join WarehouseOLAPMergedV2.APC.BasePressureUlcerReference
on	BasePressureUlcerReference.MergePressureUlcerRecno = BasePressureUlcer.MergePressureUlcerRecno

inner join WarehouseOLAPMergedV2.APC.PressureUlcerCategory
on BasePressureUlcerReference.CategoryID = PressureUlcerCategory.SourcePressureUlcerCategoryID

inner join WarehouseOLAPMergedV2.APC.BaseEncounterBasePressureUlcer
on	BasePressureUlcer.MergePressureUlcerRecno = BaseEncounterBasePressureUlcer.MergePressureUlcerRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounter
on	BaseEncounter.MergeEncounterRecno = BaseEncounterBasePressureUlcer.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.APC.BaseEncounterReference
on	BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno

inner join WarehouseOLAPMergedV2.WH.Directorate StartDirectorate
on	coalesce(BaseEncounter.StartDirectorateCode, '9') = StartDirectorate.DirectorateCode

inner join WarehouseOLAPMergedV2.WH.Directorate EndDirectorate
on	coalesce(BaseEncounter.EndDirectorateCode, '9') = EndDirectorate.DirectorateCode

where
	BasePressureUlcer.IdentifiedDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
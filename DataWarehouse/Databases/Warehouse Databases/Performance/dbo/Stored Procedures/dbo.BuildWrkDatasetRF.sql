﻿
CREATE proc [dbo].[BuildWrkDatasetRF] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'RF'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)

truncate table dbo.WrkDatasetRF

insert into dbo.WrkDatasetRF
(
	DatasetRecno
	,ContextCode
	,SourceEncounterNo
	,SiteCode
	,DirectorateCode
	,SpecialtyCode
	,Specialty
	,NationalSpecialtyCode
	,ClinicianCode
	,ReferralDateID
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,NationalSourceOfReferralCode
	,CasenoteNumber
	,DirectorateID 			
	,SpecialtyID 
	,SiteID 
	,ClinicianID
	,ServicePointID 
) 

select 
	 DatasetRecno = Encounter.MergeEncounterRecno
	,Encounter.ContextCode
	,Encounter.SourceEncounterNo
	,SiteCode = Site.SourceSiteCode
	,DirectorateCode = Directorate.DirectorateCode
	,Encounter.SpecialtyCode
	,Specialty = Specialty.SourceSpecialty
	,NationalSpecialtyCode = Specialty.NationalSpecialtyCode
	,Encounter.ConsultantCode
	,Reference.ReferralDateID
	,Encounter.ReferralDate
	,Encounter.DischargeDate
	,Encounter.DischargeTime
	,SourceOfReferral.NationalSourceOfReferralCode
	,CasenoteNumber = Encounter.CasenoteNo
	,DirectorateID = Directorate.DirectorateID
	,SpecialtyID = Reference.SpecialtyID
	,SiteID = Reference.SiteID
	,ClinicianID = Reference.ConsultantID
	,ServicePointID = ServicePoint.SourceServicePointID
from
	WarehouseOLAPMergedV2.RF.BaseEncounter Encounter

inner join WarehouseOLAPMergedV2.RF.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

left join WarehouseOLAPMergedV2.WH.Site Site
on Site.SourceSiteID = Reference.SiteID

left join WarehouseOLAPMergedV2.WH.Directorate Directorate
on coalesce(Encounter.DirectorateCode, '9') = Directorate.DirectorateCode

left join WarehouseOLAPMergedV2.WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.SpecialtyID

left join WarehouseOLAPMergedV2.OP.SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = Reference.SourceOfReferralID

inner join WarehouseOLAPMergedV2.WH.ServicePoint
on ServicePoint.SourceServicePointID = '-1'

where
	Encounter.ReferralDate >= @StartDate
	
select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
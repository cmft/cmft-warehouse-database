﻿
CREATE proc [dbo].[BuildServicePointBase]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	 dbo.ServicePointBase target
using
	(
	select
		 SourceContextCode
		,SourceContext
		,SourceServicePointID
		,SourceServicePointCode
		,SourceServicePoint
		,LocalServicePointID
		,LocalServicePointCode
		,LocalServicePoint
		,NationalServicePointID
		,NationalServicePointCode
		,NationalServicePoint
		,ServicePointType
	from
		WarehouseOLAPMergedV2.WH.ServicePoint

	) source
	on	source.SourceServicePointID = target.SourceServicePointID

	
	when not matched by source

	then delete

	when not matched
	then
		insert
			(
			SourceContextCode
			,SourceContext
			,SourceServicePointID
			,SourceServicePointCode
			,SourceServicePoint
			,LocalServicePointID
			,LocalServicePointCode
			,LocalServicePoint
			,NationalServicePointID
			,NationalServicePointCode
			,NationalServicePoint
			,ServicePointType
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.SourceContextCode
			,source.SourceContext
			,source.SourceServicePointID
			,source.SourceServicePointCode
			,source.SourceServicePoint
			,source.LocalServicePointID
			,source.LocalServicePointCode
			,source.LocalServicePoint
			,source.NationalServicePointID
			,source.NationalServicePointCode
			,source.NationalServicePoint
			,source.ServicePointType
			,getdate()
			,getdate()
			,suser_name()			
			)

	when matched
	and not
		(	
			isnull(target.SourceContextCode, '') = isnull(source.SourceContextCode, '')	
		and isnull(target.SourceContext, '') = isnull(source.SourceContext, '')					
		and isnull(target.SourceServicePointID, 0) = isnull(source.SourceServicePointID, 0)	
		and isnull(target.SourceServicePoint, '') = isnull(source.SourceServicePoint, '')	
		and	isnull(target.LocalServicePointID, 0) = isnull(source.LocalServicePointID, 0)
		and	isnull(target.LocalServicePointCode, '') = isnull(source.LocalServicePointCode, '')		
		and	isnull(target.LocalServicePoint, '') = isnull(source.LocalServicePoint, '')
		and	isnull(target.NationalServicePointID, 0) = isnull(source.NationalServicePointID, 0)
		and	isnull(target.NationalServicePointCode, '') = isnull(source.NationalServicePointCode, '')		
		and isnull(target.NationalServicePoint, '') = isnull(source.NationalServicePoint, '')
		and isnull(target.ServicePointType, '') = isnull(source.ServicePointType, '')
		)
	then
		update
		set
			target.SourceContextCode = source.SourceContextCode
			,target.SourceContext = source.SourceContext
			,target.SourceServicePointID = source.SourceServicePointID
			,target.SourceServicePoint = source.SourceServicePoint	
			,target.LocalServicePointID = source.LocalServicePointID
			,target.LocalServicePointCode = source.LocalServicePointCode								
			,target.LocalServicePoint = source.LocalServicePoint
			,target.NationalServicePointID = source.NationalServicePointID		
			,target.NationalServicePointCode = source.NationalServicePointCode
			,target.NationalServicePoint = source.NationalServicePoint
			,target.ServicePointType = source.ServicePointType
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime







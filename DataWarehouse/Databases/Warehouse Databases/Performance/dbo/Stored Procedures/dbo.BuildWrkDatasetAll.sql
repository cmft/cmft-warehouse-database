﻿CREATE procedure [dbo].[BuildWrkDatasetAll] as

--exec BuildWrkDataset AE
--exec BuildWrkDataset APC
--exec BuildWrkDataset OP
--exec BuildWrkDataset RF
--exec BuildWrkDataset WS
--exec BuildWrkDataset COM
--exec BuildWrkDataset COMR
--exec BuildWrkDataset INC
--exec BuildWrkDataset QCR
--exec BuildWrkDataset INCB
--exec BuildWrkDataset NEO
--exec BuildWrkDataset MAT
--exec BuildWrkDataset CWL
--exec BuildWrkDataset RENQ
--exec BuildWrkDataset REN
--exec BuildWrkDataset CAN
--exec BuildWrkDataset CANT
--exec BuildWrkDataset 'CASE'
--exec BuildWrkDataset IPWLE
--exec BuildWrkDataset FINANCE

/* will replace the above at some point with the below */


truncate table dbo.WrkDataset


	declare
		 @StartTime datetime = getdate()
		,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
		,@Elapsed int
		,@Stats varchar(max)


	declare @Dataset varchar(128)
	declare @WrkSQL varchar(max)

	select @StartTime = getdate()

	declare DatasetCursor cursor fast_forward for

	select distinct
		DatasetCode
	from
		dbo.Dataset
	where
		Active = 1

	order by
		DatasetCode

	open DatasetCursor

	fetch next from DatasetCursor

	into
		 @Dataset
		 
	while @@fetch_status = 0

	begin
		select
			@WrkSQL =
				'exec BuildWrkDataset ''' + @Dataset + ''''
		
	print convert(varchar, getdate(), 113) + ' - ' + @WrkSQL
	
	execute(@WrkSQL)
		
	fetch next from DatasetCursor
	into @Dataset
	
	end
		 
	close DatasetCursor
	deallocate DatasetCursor

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
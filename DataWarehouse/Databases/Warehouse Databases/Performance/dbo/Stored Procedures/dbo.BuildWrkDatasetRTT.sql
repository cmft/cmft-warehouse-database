﻿

CREATE proc [dbo].[BuildWrkDatasetRTT] as

set dateformat dmy

declare	@StartTime datetime = getdate()
								
declare	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare	@Elapsed int
declare	@Stats varchar(max)
declare @Inserted int
declare @DatasetCode varchar(20) = 'RTT'
declare	@StartDate date = 
						coalesce(
							(
							select
								StartDate
							from
								dbo.Dataset
							where
								DatasetCode = @DatasetCode
							)
							,
							(
							select
								DateValue
							from
								dbo.Parameter
							where
								Parameter = 'DEFAULTSTARTDATE'
							)
						)


if object_id('tempdb..#RTTEncounter') is not null
drop table #RTTEncounter
;

with CensusSet
(
	CensusDate
)
as
(
select
	CensusDate = TheDate
from
	WarehouseOLAPMergedV2.WH.Calendar
where
	TheDate = LastDayOfMonth
and	TheDate between @StartDate and getdate()
)

select
	 Encounter.EncounterRecNo
	,Encounter.path_directorate
	,Encounter.PathwayStatusCode
	,Encounter.BreachBandCode
	,Encounter.path_spec
	,Encounter.Specialty
	,Encounter.Censusdate
	,path_cons =
		case
		when path_directorate = 'Trafford' 
		then left(Encounter.path_cons,CHARINDEX(',',Encounter.path_cons)-1)
		else Encounter.path_cons
		end
into
	#RTTEncounter
from
	RTTLegacy.RTT.Encounter

inner join RTTLegacy.RTT.Fact
on Encounter.FactEncounterRecNo = Fact.EncounterRecno

inner join CensusSet
on	CensusSet.CensusDate = Encounter.Censusdate

where
	left(Encounter.type , 3) != 'DAA'
and Encounter.AdjustedFlag = 'N'
and Encounter.PathwayStatusCode in
		(
		 'IPW' --Inpatient Waiters
		,'OPW' --Outpatient Waiters
		,'ACS' --Admitted Clock Stops
		,'OPT' --Outpatient Treatments
		)
and Fact.ReportableFlag = 1
;

truncate table dbo.WrkDatasetRTT

insert into dbo.WrkDatasetRTT
(
	 DatasetRecno
	,ContextCode
	,PathwayStatusCode
	,BreachStatusCode
	,DirectorateCode
	,SpecialtyCode
	,ClinicianCode
	,SiteCode
	,CensusDate
	,CensusDateID
	,DirectorateID
	,SpecialtyID
	,SiteID
	,ClinicianID
	,ServicePointID
)

select
	 Encounter.EncounterRecno
	,ContextCode =
		case
		when Encounter.path_directorate = 'Trafford' then 'TRA||UG'
		else 'CEN||PAS'
		end
	,PathwayStatusCode
	,BreachStatusCode = 
		case
		when left(BreachBandCode , 2) = 'BR' then 'B'
		else 'N'
		end
	,DirectorateCode = Directorate.DirectorateCode
	,SpecialtyCode = coalesce(Encounter.path_spec, Encounter.specialty)
	,ClinicianCode =
		coalesce(
			ConsultantCentral.SourceConsultantCode
			,ConsultantTrafford.SourceConsultantCode
			,ConsultantTraffordNotSpecified.SourceConsultantCode
		)
	,SiteCode =
		coalesce(
			SiteCentral.SourceSiteCode
			,SiteTrafford.SourceSiteCode
		)
	,Encounter.CensusDate
	,CensusDateID = Census.DateID
	,DirectorateID = Directorate.DirectorateID
	,SpecialtyID =
		coalesce(
			SpecialtyCentral.SourceSpecialtyID
			,SpecialtyTrafford.SourceSpecialtyID
			)
	,SiteID =
		coalesce(
			SiteCentral.SourceSiteID,
			SiteTrafford.SourceSiteID
			)
	,ClinicianID = 
		coalesce(
			ConsultantCentral.SourceConsultantID
			,ConsultantTrafford.SourceConsultantID
			,ConsultantTraffordNotSpecified.SourceConsultantID
			)
	,ServicePointID = -1
from
	#RTTEncounter Encounter

inner join WarehouseOLAPMergedV2.WH.Calendar Census 
on Census.TheDate = Encounter.CensusDate

--Directorate
left join
	(
	select
		DirectorateID
		,DirectorateCode
		,Directorate =
			case
			when Directorate.Directorate = 'Saint Mary''s' then 'St Mary''s Hospital'
			when Directorate.Directorate = 'Specialist Surgery' then 'Spec Surgery'
			when Directorate.Directorate = 'Specialist Medicine' then 'Spec Medicine'
			when Directorate.Directorate = 'Acute & Rehabilitation' then 'Acute & Rehab'
			when Directorate.Directorate = 'Clinical & Scientific' then 'Clin & Scient'
			when Directorate.Directorate = 'Emergency Service' then 'Emergency Serv'
			when Directorate.Directorate = 'Mental Health Trust' then 'MH TRUST'
			else Directorate.Directorate
			end
	from
		WarehouseOLAPMergedV2.WH.Directorate Directorate
	where 
		not exists
		(
		select
			1
		from
			WarehouseOLAPMergedV2.WH.Directorate Latest
		where
			Latest.Directorate = Directorate.Directorate
		and Latest.DirectorateID < Directorate.DirectorateID
		)
	) Directorate
on	Directorate.Directorate = coalesce(Encounter.path_directorate,'N/A')

--Consultant
left join WarehouseOLAPMergedV2.WH.Consultant ConsultantCentral
on	Encounter.path_directorate != 'Trafford'
and	ConsultantCentral.SourceContextCode = 'CEN||PAS'
and	ConsultantCentral.SourceConsultantCode = coalesce(Encounter.path_cons , '-1')

left join
	(
	select
		 Consultant.SourceConsultantID
		,Consultant.SourceConsultantCode
		,ConsultantSurname = ConsultantTrafford.surname
	from
		TraffordReferenceData.dbo.INFODEPT_cab_consultants ConsultantTrafford

	left join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceContextCode = 'TRA||UG'
	and	Consultant.SourceConsultantCode = ConsultantTrafford.sds_code

	where
		not exists
			(
			select
				1
			from
				TraffordReferenceData.dbo.INFODEPT_cab_consultants ConsultantTraffordDuplicate
			where
				ConsultantTraffordDuplicate.surname = ConsultantTrafford.surname
			and	ConsultantTraffordDuplicate.sds_code < ConsultantTrafford.sds_code
			)
	) ConsultantTrafford
on	Encounter.path_directorate = 'Trafford'
and	Encounter.path_cons = ConsultantTrafford.ConsultantSurname --(ConsultantTrafford.ConsultantSurname + '%')

left join WarehouseOLAPMergedV2.WH.Consultant ConsultantTraffordNotSpecified
on	ConsultantTraffordNotSpecified.SourceContextCode = 'TRA||UG'
and	ConsultantTraffordNotSpecified.SourceConsultantCode = '-1'

--Specialty
left join WarehouseOLAPMergedV2.WH.Specialty SpecialtyCentral
on	coalesce(Encounter.path_directorate,'') != 'Trafford'
and	SpecialtyCentral.SourceContextCode = 'CEN||PAS'
and	SpecialtyCentral.SourceSpecialtyCode = coalesce(encounter.path_spec, encounter.specialty)

left join WarehouseOLAPMergedV2.WH.Specialty SpecialtyTrafford
on	Encounter.path_directorate = 'Trafford'
and	SpecialtyTrafford.SourceContextCode = 'TRA||UG'
and	SpecialtyTrafford.NationalSpecialtyCode = coalesce(encounter.path_spec, encounter.specialty)
and	not exists
		(
		select
			1
		from
			WarehouseOLAPMergedV2.WH.Specialty SpecialtyDuplicate
		where
			SpecialtyDuplicate.NationalSpecialtyID = SpecialtyTrafford.NationalSpecialtyID
		and	SpecialtyDuplicate.SourceContextCode = SpecialtyTrafford.SourceContextCode
		and	SpecialtyDuplicate.SourceSpecialtyID < SpecialtyTrafford.SourceSpecialtyID
		)

--Site
left join WarehouseOLAPMergedV2.WH.Site SiteCentral
on	SiteCentral.SourceContextCode = 'CEN||PAS'
and SiteCentral.SourceSiteCode = '-1'
and	coalesce(Encounter.path_directorate,'') != 'Trafford'

left join WarehouseOLAPMergedV2.WH.Site SiteTrafford
on	SiteTrafford.SourceContextCode = 'TRA||UG'
and SiteTrafford.SourceSiteCode = '-1'
and	Encounter.path_directorate = 'Trafford'

where
	Encounter.CensusDate >= @StartDate

select
	@Inserted = @@rowcount

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@Inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@Inserted
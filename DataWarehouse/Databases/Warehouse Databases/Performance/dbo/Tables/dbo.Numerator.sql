﻿CREATE TABLE [dbo].[Numerator] (
    [NumeratorID]    INT            IDENTITY (1, 1) NOT NULL,
    [Numerator]      VARCHAR (4000) NULL,
    [NumeratorLogic] VARCHAR (100)  NULL,
    [Active]         BIT            CONSTRAINT [DF__Numerator__Activ__5D2BD0E6] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_DQ_Numerator_1] PRIMARY KEY CLUSTERED ([NumeratorID] ASC)
);


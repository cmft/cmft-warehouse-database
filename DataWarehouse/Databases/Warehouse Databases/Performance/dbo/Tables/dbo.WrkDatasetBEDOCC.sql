﻿CREATE TABLE [dbo].[WrkDatasetBEDOCC] (
    [DatasetRecno]           INT           NOT NULL,
    [ContextCode]            VARCHAR (20)  NOT NULL,
    [Date]                   DATE          NOT NULL,
    [DateID]                 INT           NOT NULL,
    [DirectorateCode]        VARCHAR (5)   NOT NULL,
    [DirectorateID]          INT           NOT NULL,
    [SpecialtyCode]          VARCHAR (100) NOT NULL,
    [SpecialtyID]            INT           NOT NULL,
    [SiteCode]               VARCHAR (100) NOT NULL,
    [SiteID]                 INT           NOT NULL,
    [WardCode]               VARCHAR (100) NOT NULL,
    [ClinicianCode]          VARCHAR (100) NOT NULL,
    [ClinicianID]            INT           NOT NULL,
    [ServicePointID]         INT           NOT NULL,
    [BedOccupancyNights]     INT           NULL,
    [MedicalOutlier]         INT           NULL,
    [AvailableInpatientBeds] INT           NULL
);


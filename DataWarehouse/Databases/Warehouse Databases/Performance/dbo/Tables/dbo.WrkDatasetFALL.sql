﻿CREATE TABLE [dbo].[WrkDatasetFALL] (
    [DatasetRecno]           INT           NOT NULL,
    [FallDateID]             INT           NULL,
    [SourceFallSeverityCode] VARCHAR (100) NOT NULL,
    [Validated]              BIT           NULL,
    [StartDirectorateID]     INT           NOT NULL,
    [EndDirectorateID]       INT           NOT NULL,
    [SpecialtyID]            INT           NULL,
    [StartSiteID]            INT           NULL,
    [EndSiteID]              INT           NULL,
    [ClinicianID]            INT           NULL,
    [ServicePointID]		 INT		   NULL,
);


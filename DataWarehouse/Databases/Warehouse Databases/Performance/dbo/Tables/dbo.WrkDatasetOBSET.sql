﻿CREATE TABLE [dbo].[WrkDatasetOBSET] (
    [DatasetRecno]      INT           NOT NULL,
    [StartDateID]       INT           NULL,
    [ObservationStatus] VARCHAR (200) NULL,
    [DirectorateID]     INT           NOT NULL,
    [SpecialtyID]       INT           NULL,
    [SiteID]            INT           NOT NULL,
    [ClinicianID]       INT           NOT NULL,
    [ServicePointID]    INT           NOT NULL
);


﻿CREATE TABLE [dbo].[WrkDatasetQCR] (
    [DatasetRecno]   INT           NOT NULL,
    [ContextCode]    VARCHAR (255) NOT NULL,
    [AuditDate]      SMALLDATETIME NOT NULL,
    [AuditDateID]    INT           NULL,
    [QuestionCode]   INT           NULL,
    [CustomListCode] INT           NULL,
    [Answer]         INT           NULL,
    [DirectorateID]  INT           NOT NULL,
    [SpecialtyID]    INT           NOT NULL,
    [SiteID]         INT           NOT NULL,
    [ClinicianID]    INT           NOT NULL,
    [ServicePointID] INT           NULL
);


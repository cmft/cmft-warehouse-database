﻿CREATE TABLE [dbo].[ItemFormat] (
    [ItemFormatID] INT          IDENTITY (1, 1) NOT NULL,
    [ItemFormat]   VARCHAR (50) NULL,
    CONSTRAINT [PK_EntityFormat] PRIMARY KEY CLUSTERED ([ItemFormatID] ASC)
);


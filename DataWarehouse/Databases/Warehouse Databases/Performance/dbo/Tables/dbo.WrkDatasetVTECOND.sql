﻿CREATE TABLE [dbo].[WrkDatasetVTECOND] (
    [DatasetRecno]          INT           NOT NULL,
    [DiagnosisDateID]       INT           NULL,
    [MedicationRequired]    BIT           NULL,
    [MedicationStartDateID] INT           NULL,
    [SourceVTETypeCode]     VARCHAR (100) NOT NULL,
    [DiagnosisDate]         DATE          NULL,
    [AdmissionDate]         SMALLDATETIME NULL,
    [StartDirectorateID]    INT           NOT NULL,
    [EndDirectorateID]      INT           NOT NULL,
    [SpecialtyID]           INT           NULL,
    [StartSiteID]           INT           NULL,
    [EndSiteID]             INT           NULL,
    [ClinicianID]           INT           NULL,
    [ServicePointID]		INT			NULL,
);


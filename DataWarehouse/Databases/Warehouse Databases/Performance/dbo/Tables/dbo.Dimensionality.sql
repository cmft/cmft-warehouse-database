﻿CREATE TABLE [dbo].[Dimensionality]
(
	[DimensionalityID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Dimensionality] VARCHAR(200) NOT NULL
)

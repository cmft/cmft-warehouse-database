﻿CREATE TABLE [dbo].[WrkDatasetAES] (
    [DatasetRecno]           INT          NOT NULL,
    [ContextCode]            VARCHAR (10) NOT NULL,
    [DistrictNo]             VARCHAR (50) NULL,
    [StageCode]              VARCHAR (50) NOT NULL,
    [BreachStatusCode]       VARCHAR (10) NOT NULL,
    [DepartmentTypeCode]     VARCHAR (2)  NULL,
    [Duration]               INT          NULL,
    [UnplannedReattend7Days] INT          NOT NULL,
    [LeftWithoutBeingSeen]   INT          NOT NULL,
    [ArrivalModeCode]        VARCHAR (50) NULL,
    [AttendanceDisposalCode] VARCHAR (50) NULL,
    [EncounterDate]          DATE         NOT NULL,
    [EncounterDateID]        INT          NOT NULL,
    [DirectorateID]          INT          NULL,
    [SpecialtyID]            INT          NULL,
    [SiteID]                 INT          NULL,
    [ClinicianID]            INT          NULL,
    [ServicePointID]         INT          NULL
);


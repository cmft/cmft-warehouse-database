﻿CREATE TABLE [dbo].[FactTargetSpecialty](
	[TargetID] [int] NOT NULL,
	[DateID] [int] NOT NULL,
	[NationalSpecialtyID] [int] NOT NULL,
	[Value] [float] NOT NULL 
) ON [PRIMARY]

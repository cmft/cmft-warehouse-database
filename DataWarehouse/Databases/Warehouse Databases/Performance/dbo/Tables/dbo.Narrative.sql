﻿CREATE TABLE [dbo].[Narrative] (
    [NarrativeID]       INT            IDENTITY (1, 1) NOT NULL,
    [NarrativeFromDate] DATE           NOT NULL,
    [ItemID]            INT            NOT NULL,
    [NarrativeTypeID]   INT            NOT NULL,
    [NarrativeHTML]     VARCHAR (4000) NULL,
    [Narrative]         VARCHAR (4000) NOT NULL,
    [Active]            BIT            CONSTRAINT [DF_Narrative_Active] DEFAULT ((1)) NOT NULL,
    [Updated]           DATETIME       CONSTRAINT [DF__Narrative__Updat__09B529F1] DEFAULT (getdate()) NULL,
    [ByWhom]            VARCHAR (50)   CONSTRAINT [DF__Narrative__ByWho__0AA94E2A] DEFAULT (suser_sname()) NULL,
    CONSTRAINT [PK__Narrativ__7B97293107CCE17F] PRIMARY KEY CLUSTERED ([NarrativeID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Narrative]
    ON [dbo].[Narrative]([ItemID] ASC, [NarrativeTypeID] ASC, [NarrativeFromDate] ASC);


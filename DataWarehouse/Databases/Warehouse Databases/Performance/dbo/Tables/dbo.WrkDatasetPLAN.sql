﻿CREATE TABLE [dbo].[WrkDatasetPLAN] (
    [DatasetRecno]       INT          NOT NULL,
    [ActivityDate]       DATE         NOT NULL,
    [ActivityDateID]     INT          NOT NULL,
    [ActivityMetricCode] VARCHAR (20) NOT NULL,
    [Cases]              FLOAT (53)   NULL,
    [Value]              FLOAT (53)   NULL,
    [DirectorateID]      INT          NOT NULL,
    [SpecialtyID]        INT          NULL,
    [SiteID]             INT          NOT NULL,
    [ClinicianID]        INT          NOT NULL,
    [ServicePointID]     INT          NOT NULL
);


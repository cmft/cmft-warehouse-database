﻿CREATE TABLE [dbo].[WrkDatasetALERT] (
    [DatasetRecno]   INT      NOT NULL,
    [CreatedTime]    DATETIME NULL,
    [CreatedDateID]  INT      NULL,
    [ClosedTime]     DATETIME NULL,
    [DirectorateID]  INT      NOT NULL,
    [SpecialtyID]    INT      NULL,
    [SiteID]         INT      NOT NULL,
    [ClinicianID]    INT      NOT NULL,
    [ServicePointID] INT      NOT NULL
);


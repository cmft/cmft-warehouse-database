﻿CREATE TABLE [dbo].[WrkDatasetRESUS] (
    [DatasetRecno]   INT           NOT NULL,
    [CallOutDateID]  INT           NULL,
    [CallOutReason]  VARCHAR (900) NOT NULL,
    [AuditType]      VARCHAR (900) NOT NULL,
    [DirectorateID]  INT           NOT NULL,
    [SpecialtyID]    INT           NOT NULL,
    [SiteID]         INT           NOT NULL,
    [ClinicianID]    INT           NOT NULL,
    [ServicePointID] INT           NOT NULL
);


﻿CREATE TABLE [dbo].[TargetRuleBase](
	[RuleBaseRecno] [int] IDENTITY(1,1) NOT NULL,
	[TargetID] [int] NOT NULL,
	[NationalSpecialtyCode] [varchar](10) NULL,
	[StartDate] [date] NULL,
	[Value] [float] NOT NULL
) ON [PRIMARY]

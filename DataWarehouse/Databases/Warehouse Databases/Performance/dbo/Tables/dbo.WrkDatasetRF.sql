﻿CREATE TABLE [dbo].[WrkDatasetRF] (
    [DatasetRecno]                 INT           NOT NULL,
    [ContextCode]                  VARCHAR (8)   NOT NULL,
    [SourceEncounterNo]            VARCHAR (20)  NOT NULL,
    [SiteCode]                     VARCHAR (100) NULL,
    [DirectorateCode]              VARCHAR (5)   NULL,
    [SpecialtyCode]                VARCHAR (5)   NULL,
    [Specialty]                    VARCHAR (900) NULL,
    [NationalSpecialtyCode]        VARCHAR (50)  NULL,
    [ClinicianCode]                VARCHAR (20)  NULL,
    [ReferralDateID]               INT           NULL,
    [ReferralDate]                 DATE          NULL,
    [DischargeDate]                DATE          NULL,
    [DischargeTime]                DATETIME      NULL,
    [NationalSourceOfReferralCode] VARCHAR (50)  NULL,
    [CasenoteNumber]               VARCHAR (16)  NULL,
    [DirectorateID]                INT           NULL,
    [SpecialtyID]                  INT           NULL,
    [SiteID]                       INT           NULL,
    [ClinicianID]                  INT           NULL,
    [ServicePointID]               INT           NOT NULL
);


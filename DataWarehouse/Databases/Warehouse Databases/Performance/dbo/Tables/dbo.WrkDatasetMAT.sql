﻿CREATE TABLE [dbo].[WrkDatasetMAT] (
    [DatasetRecno]        INT           NOT NULL,
    [ContextCode]         VARCHAR (20)  NULL,
    [InfantDateOfBirth]   DATETIME2 (7) NULL,
    [InfantDateOfBirthID] INT           NOT NULL,
    [MethodDelivery]      NCHAR (1)     NULL,
    [Perineum]            NCHAR (2)     NULL,
    [BirthOrder]          NCHAR (1)     NOT NULL,
    [Outcome]             NCHAR (1)     NULL,
    [DirectorateID]       INT           NOT NULL,
    [SpecialtyID]         INT           NOT NULL,
    [SiteID]              INT           NOT NULL,
    [ClinicianID]         INT           NOT NULL,
    [ServicePointID]      INT           NOT NULL
);


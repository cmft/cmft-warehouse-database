﻿CREATE TABLE [dbo].[FactNumerator] (
    [DatasetID]      INT        NOT NULL,
    [DatasetRecno]   BIGINT     NOT NULL,
    [NumeratorID]    INT        NOT NULL,
    [SiteID]         INT        NOT NULL,
    [DirectorateID]  INT        NOT NULL,
    [SpecialtyID]    INT        NOT NULL,
    [ClinicianID]    INT        NOT NULL,
    [DateID]         INT        NOT NULL,
    [ServicePointID] INT        NOT NULL,
    [Value]          FLOAT (53) NULL
);


﻿CREATE TABLE [dbo].[WrkDatasetFFTRTN] (
    [DatasetRecno]             INT           NOT NULL,
    [ContextCode]              VARCHAR (15)  NULL,
    [CensusDate]               DATE          NULL,
    [WardCode]                 NVARCHAR (15) NULL,
    [SiteCode]                 NVARCHAR (15) NULL,
    [CensusDateID]             INT           NULL,
    [ServicePointID]           INT           NOT NULL,
    [SiteID]                   INT           NOT NULL,
    [SpecialtyID]              INT           NOT NULL,
    [ClinicianID]              INT           NOT NULL,
    [DirectorateID]            INT           NOT NULL,
    [FFTReturnType]            VARCHAR (2)   NULL,
    [ExtremelyLikeley]         INT           NULL,
    [Likely]                   INT           NULL,
    [NeitherLikelyNorUnlikely] INT           NULL,
    [Unlikely]                 INT           NULL,
    [ExtremelyUnlikely]        INT           NULL,
    [DontKnow]                 INT           NULL,
    [Responses]                INT           NULL,
    [EligibleResponders]       INT           NULL
);


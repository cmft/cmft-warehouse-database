﻿CREATE TABLE [dbo].[WrkDatasetOPER] (
    [DatasetRecno]                  INT           NOT NULL,
    [ContextCode]                   VARCHAR (10)  NOT NULL,
    [OperationDate]                 DATE          NOT NULL,
    [OperationDateID]               INT           NOT NULL,
    [CancelReasonCode]              VARCHAR (100) NOT NULL,
    [TurnaroundTime]                INT           NULL,
    [TurnaroundTimeCount]           INT           NOT NULL,
    [DirectorateID]                 INT           NULL,
    [SpecialtyID]                   INT           NULL,
    [SiteID]                        INT           NOT NULL,
    [ClinicianID]                   INT           NULL,
    [ServicePointID]                INT           NOT NULL,
    [StartServicePointID]           INT           NOT NULL,
    [UtilisationMinutes]            INT           NULL,
    [AnaesthetistActiveTheatreTime] INT           NULL,
    [SurgeonActiveTheatreTime]      INT           NULL,
    [CancelledOnDay]                INT           NOT NULL,
    [SessionSourceUniqueID]         NUMERIC (9)   NOT NULL
);


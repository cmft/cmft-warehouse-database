﻿CREATE TABLE [dbo].[ReportImage] (
    [ReportImageID] INT          IDENTITY (1, 1) NOT NULL,
    [ReportImage]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ReportImage] PRIMARY KEY CLUSTERED ([ReportImageID] ASC)
);


﻿CREATE TABLE [dbo].[WrkDatasetSTAFFLEVEL] (
    [DatasetRecno]             INT           NOT NULL,
    [ContextCode]              VARCHAR (11)  NOT NULL,
    [SourceShift]              VARCHAR (900) NOT NULL,
    [RegisteredNursePlan]      INT           NULL,
    [RegisteredNurseActual]    INT           NULL,
    [NonRegisteredNursePlan]   INT           NULL,
    [NonRegisteredNurseActual] INT           NULL,
    [CensusDateID]             INT           NULL,
    [DirectorateID]            INT           NOT NULL,
    [SpecialtyID]              INT           NOT NULL,
    [SiteID]                   INT           NOT NULL,
    [ClinicianID]              INT           NOT NULL,
    [ServicePointID]           INT           NULL
);


﻿CREATE TABLE [dbo].[CentralPortalClinician] (
    [NationalClinicianCode] VARCHAR (20) NOT NULL,
    CONSTRAINT [PK__CentralP__C9BDBD421222B4C5] PRIMARY KEY CLUSTERED ([NationalClinicianCode] ASC)
);


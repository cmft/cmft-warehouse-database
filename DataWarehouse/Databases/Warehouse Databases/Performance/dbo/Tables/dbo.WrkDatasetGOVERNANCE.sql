﻿CREATE TABLE [dbo].[WrkDatasetGOVERNANCE] (
    [DatasetRecno]         BIGINT   NULL,
    [ActivityDate]         DATETIME NULL,
    [ActivityDateID]       INT      NOT NULL,
    [DirectorateID]        INT      NULL,
    [GovernanceRiskRating] INT      NULL,
    [SiteID]               INT      NOT NULL,
    [SpecialtyID]          INT      NOT NULL,
    [ClinicianID]          INT      NOT NULL,
    [ServicePointID]       INT      NOT NULL
);


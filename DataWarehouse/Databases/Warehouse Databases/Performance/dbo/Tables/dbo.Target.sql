﻿CREATE TABLE [dbo].[Target] (
    [TargetID]     INT            IDENTITY (1, 1) NOT NULL,
    [Target]       VARCHAR (4000) NULL,
    [TargetLogic]  VARCHAR (100)  NULL,
    [TargetTypeID] INT            NULL,
    [Active]       BIT            NOT NULL,
    [DimensionalityID] INT NOT NULL DEFAULT 1, 
    CONSTRAINT [PK_Target] PRIMARY KEY CLUSTERED ([TargetID] ASC)
);


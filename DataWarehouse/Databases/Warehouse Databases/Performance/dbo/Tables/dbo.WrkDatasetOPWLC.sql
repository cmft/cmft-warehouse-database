﻿CREATE TABLE [dbo].[WrkDatasetOPWLC] (
    [DatasetRecno]                INT           NOT NULL,
    [SourceUniqueID]              VARCHAR (255) NULL,
    [ContextCode]                 VARCHAR (8)   NOT NULL,
    [DirectorateCode]             VARCHAR (5)   NOT NULL,
    [SpecialtyCode]               VARCHAR (10)  NULL,
    [ClinicianCode]               VARCHAR (20)  NULL,
    [SiteCode]                    VARCHAR (10)  NULL,
    [CensusDate]                  SMALLDATETIME NULL,
    [CensusDateID]                INT           NULL,
    [TCIDate]                     SMALLDATETIME NULL,
    [DirectorateID]               INT           NOT NULL,
    [SpecialtyID]                 INT           NOT NULL,
    [SiteID]                      INT           NOT NULL,
    [ClinicianID]                 INT           NOT NULL,
    [ServicePointID]              INT           NOT NULL,
    [Duration]                    INT           NULL,
    [NationalFirstAttendanceCode] VARCHAR (50)  NULL
);


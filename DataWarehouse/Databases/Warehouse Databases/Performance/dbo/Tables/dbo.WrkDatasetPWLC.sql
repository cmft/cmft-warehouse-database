﻿CREATE TABLE [dbo].[WrkDatasetPWLC] (
    [MergeEncounterRecno] INT           NOT NULL,
    [SourceUniqueID]      VARCHAR (255) NULL,
    [ContextCode]         VARCHAR (8)   NULL,
    [DirectorateCode]     VARCHAR (5)   NOT NULL,
    [SpecialtyCode]       VARCHAR (4)   NULL,
    [ClinicianCode]       VARCHAR (20)  NULL,
    [SiteCode]            VARCHAR (4)   NULL,
    [CensusDate]          SMALLDATETIME NULL,
    [CensusDateID]        INT           NULL,
    [TCIDate]             DATE          NULL,
    [DirectorateID]       INT           NOT NULL,
    [SpecialtyID]         INT           NULL,
    [SiteID]              INT           NULL,
    [ClinicianID]         INT           NULL,
    [ServicePointID]      INT           NULL,
    [Duration]            INT           NULL
);


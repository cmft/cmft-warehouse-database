﻿CREATE TABLE [dbo].[WrkDatasetREN] (
    [DatasetRecno]               INT            NULL,
    [StartDate]                  DATETIME       NULL,
    [StartDateID]                INT            NOT NULL,
    [StopDate]                   DATETIME       NULL,
    [ContextCode]                VARCHAR (100)  NULL,
    [ModalityCode]               NVARCHAR (256) NULL,
    [ModalitySettingCode]        NVARCHAR (256) NULL,
    [EventDetailCode]            NVARCHAR (256) NULL,
    [DialysisProviderCode]       NVARCHAR (100) NULL,
    [VascularAccessCode]         NVARCHAR (256) NULL,
    [VascularAccessAt90DaysCode] NVARCHAR (256) NULL,
    [DirectorateID]              INT            NOT NULL,
    [SpecialtyID]                INT            NOT NULL,
    [SiteID]                     INT            NOT NULL,
    [ClinicianID]                INT            NOT NULL,
    [ServicePointID]             INT            NOT NULL
);


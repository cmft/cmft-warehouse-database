﻿CREATE TABLE [dbo].[WrkDatasetFINANCE] (
    [DatasetRecno]   INT           NOT NULL,
    [CensusDate]     DATE          NOT NULL,
    [CensusDateID]   INT           NULL,
    [Budget]         FLOAT (53)    NULL,
    [AnnualBudget]   FLOAT (53)    NULL,
    [Actual]         FLOAT (53)    NULL,
    [Division]       VARCHAR (100) NOT NULL,
    [DirectorateID]  INT           NOT NULL,
    [SiteID]         INT           NOT NULL,
    [SpecialtyID]    INT           NOT NULL,
    [ClinicianID]    INT           NOT NULL,
    [ServicePointID] INT           NOT NULL
);


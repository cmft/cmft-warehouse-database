﻿CREATE TABLE [dbo].[WrkDatasetUTI] (
    [DatasetRecno]               INT           NOT NULL,
    [SymptomStartDate]           DATE          NULL,
    [SymptomStartDateID]         INT           NULL,
    [UrinaryTestRequestedDateID] INT           NULL,
    [TreatmentStartDate]         DATE          NULL,
    [CatheterInsertedDateID]     INT           NULL,
    [SourceCatheterTypeCode]     VARCHAR (100) NOT NULL,
    [AdmissionDate]              SMALLDATETIME NULL,
    [AdmissionTime]              SMALLDATETIME NULL,
    [StartDirectorateID]         INT           NOT NULL,
    [EndDirectorateID]           INT           NOT NULL,
    [SpecialtyID]                INT           NULL,
    [StartSiteID]                INT           NULL,
    [EndSiteID]                  INT           NULL,
    [ClinicianID]                INT           NULL,
    [ServicePointID]    INT		   NULL,
);


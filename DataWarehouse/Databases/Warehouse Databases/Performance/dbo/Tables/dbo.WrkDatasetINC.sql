﻿CREATE TABLE [dbo].[WrkDatasetINC] (
    [DatasetRecno]             INT          NOT NULL,
    [ContextCode]              VARCHAR (11) NOT NULL,
    [IncidentDate]             DATE         NULL,
    [IncidentDateID]           INT          NULL,
    [IncidentGradeCode]        VARCHAR (50) NULL,
    [CauseGroupCode]           VARCHAR (50) NULL,
    [Cause1Code]               VARCHAR (50) NULL,
    [IncidentTypeCode]         VARCHAR (50) NULL,
    [PatientSafety]            VARCHAR (1)  NULL,
    [IncidentInvolvingChild]   INT          NULL,
    [IncidentInvolvingAdult]   INT          NULL,
    [IncidentInvolvingElderly] INT          NULL,
    [DirectorateID]            INT          NOT NULL,
    [SpecialtyID]              INT          NOT NULL,
    [SiteID]                   INT          NOT NULL,
    [ClinicianID]              INT          NOT NULL,
    [ServicePointID]           INT          NULL,
    [SourceDepartmentCode]     VARCHAR (50) NULL
);


﻿CREATE TABLE [dbo].[WrkDatasetIPWLE] (
    [DatasetRecno]    INT           NOT NULL,
    [ContextCode]     VARCHAR (100) NULL,
    [SourcePatientNo] VARCHAR (9)   NULL,
    [CasenoteNumber]  VARCHAR (14)  NULL,
    [EpisodeNo]       VARCHAR (50)  NULL,
    [ActivityDate]    SMALLDATETIME NULL,
    [ActivityDateID]  INT           NOT NULL,
    [DirectorateID]   INT           NOT NULL,
    [SpecialtyID]     INT           NOT NULL,
    [SiteID]          INT           NOT NULL,
    [ClinicianID]     INT           NOT NULL,
    [ServicePointID]  INT           NOT NULL
);


﻿CREATE TABLE [dbo].[Parameter] (
    [Parameter]    VARCHAR (128)   NOT NULL,
    [TextValue]    VARCHAR (255)   NULL,
    [NumericValue] DECIMAL (18, 5) NULL,
    [DateValue]    DATETIME        NULL,
    [BooleanValue] BIT             NULL,
    CONSTRAINT [PK_Parameter] PRIMARY KEY CLUSTERED ([Parameter] ASC)
);


﻿CREATE TABLE [dbo].[WrkDatasetPREULC] (
    [DatasetRecno]                    INT           NOT NULL,
    [IdentifiedTime]                  DATETIME      NULL,
    [IdentifiedDateID]                INT           NULL,
    [SourcePressureUlcerCategoryCode] VARCHAR (100) NOT NULL,
    [Validated]                       BIT           NULL,
    [AdmissionTime]                   SMALLDATETIME NULL,
    [StartDirectorateID]              INT           NOT NULL,
    [EndDirectorateID]                INT           NOT NULL,
    [SpecialtyID]                     INT           NULL,
    [StartSiteID]                     INT           NULL,
    [EndSiteID]                       INT           NULL,
    [ClinicianID]                     INT           NULL,
    [ServicePointID]    INT		   NULL,
);


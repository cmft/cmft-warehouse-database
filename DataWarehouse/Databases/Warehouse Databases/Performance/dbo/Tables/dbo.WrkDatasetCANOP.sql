﻿CREATE TABLE [dbo].[WrkDatasetCANOP] (
    [DatasetRecno]       INT           NOT NULL,
    [ContextCode]        VARCHAR (15)  NULL,
    [SpecialtyID]        INT           NULL,
    [ClinicianID]        INT           NULL,
    [WardCode]           VARCHAR (107) NULL,
    [CancellationDate]   DATETIME      NULL,
    [CancellationDateID] INT           NULL,
    [CancellationReason] VARCHAR (100) NULL,
    [ReportableBreach]   BIT           NULL,
    [DirectorateID]      INT           NOT NULL,
    [SiteID]             INT           NOT NULL,
    [ServicePointID]     INT           NULL
);


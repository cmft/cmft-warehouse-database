﻿CREATE TABLE [dbo].[WrkDatasetRTT] (
    [DatasetRecno]      INT           NOT NULL,
    [ContextCode]       VARCHAR (8)   NOT NULL,
    [PathwayStatusCode] VARCHAR (3)   NULL,
    [BreachStatusCode]  VARCHAR (1)   NOT NULL,
    [DirectorateCode]   VARCHAR (5)   NULL,
    [SpecialtyCode]     VARCHAR (255) NULL,
    [ClinicianCode]     VARCHAR (100) NULL,
    [SiteCode]          VARCHAR (100) NULL,
    [CensusDate]        DATETIME      NULL,
    [CensusDateID]      INT           NOT NULL,
    [DirectorateID]     INT           NULL,
    [SpecialtyID]       INT           NULL,
    [SiteID]            INT           NULL,
    [ClinicianID]       INT           NULL,
    [ServicePointID]    INT           NOT NULL
);


﻿CREATE TABLE [dbo].[WrkDatasetMAN] (
    [DatasetRecno]   INT            NOT NULL,
    [ContextCode]    VARCHAR (13)   NOT NULL,
    [ItemID]         NVARCHAR (MAX) NULL,
    [Date]           VARCHAR (11)   NULL,
    [DateID]         INT            NULL,
    [Numerator]      NVARCHAR (MAX) NULL,
    [Denominator]    NVARCHAR (MAX) NULL,
    [DirectorateID]  INT            NOT NULL,
    [SpecialtyID]    INT            NOT NULL,
    [SiteID]         INT            NOT NULL,
    [ClinicianID]    INT            NOT NULL,
    [ServicePointID] INT            NOT NULL
);


﻿



create view [WH].[Site] as
SELECT 
	 SourceContextCode
	,SourceContext
	,SourceSiteID 
	,SourceSiteCode
	,SourceSite
	,LocalSiteID 
	,LocalSiteCode
	,LocalSite 
	,NationalSiteID 
	,NationalSiteCode
	,NationalSite 
from
	[WarehouseOLAPMergedV2].[WH].[Site]





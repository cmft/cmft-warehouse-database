﻿


create view [WH].[Ward] as
SELECT 
	 SourceContextCode
	,SourceContext
	,SourceWardID 
	,SourceWardCode
	,SourceWard
	,LocalWardID 
	,LocalWardCode
	,LocalWard 
	,NationalWardID 
	,NationalWardCode
	,NationalWard 
from
	[WarehouseOLAPMergedV2].[WH].[Ward]




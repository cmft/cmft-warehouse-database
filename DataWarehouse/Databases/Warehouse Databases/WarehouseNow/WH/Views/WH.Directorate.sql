﻿


CREATE view [WH].[Directorate] as 

select
	DirectorateID
	,DirectorateCode
	,Directorate
	,DivisionCode
	,Division
	,DivisionLabel
	,DivisionGroup
from
	WarehouseOLAPMergedV2.WH.Directorate



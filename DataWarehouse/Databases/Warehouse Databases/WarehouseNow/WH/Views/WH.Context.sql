﻿

CREATE view [WH].[Context] as

select
	[ContextID]
	,[ContextCode]
	,[Context]
	,[Location]
from
	[WarehouseOLAPMergedV2].[WH].[Context]



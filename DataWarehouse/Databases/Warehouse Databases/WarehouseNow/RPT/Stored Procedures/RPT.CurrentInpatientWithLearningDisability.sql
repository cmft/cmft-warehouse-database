﻿

CREATE Procedure [RPT].[CurrentInpatientWithLearningDisability]  
	(
	@DateRange varchar(max) = null
	,@NationalSpecialtyCode varchar(max) = null
	,@WardCode varchar(max) = null
	,@AdmissionType varchar(max) = null
	,@GPPracticeCode varchar(max) = null
	)

as

--declare 
--	@DateRange varchar(max) = null
--	,@NationalSpecialtyCode varchar(max) = null
--	,@WardCode varchar(max) = null
--	,@AdmissionType varchar(max) = null
--	,@GPPracticeCode varchar(max) = null



select 
	Encounter.ProviderSpellNo
	,HospitalSite = 
		case 
		when left (Encounter.ContextCode,3) = 'CEN' 
		then 'Central'
		else 'Trafford'
		end
	,Encounter.AdmissionTime
	,Encounter.PatientTitle 
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Postcode = Encounter.Postcode
	,Encounter.SexCode 
	,Ward = 
		coalesce(
			Ward.LocalWard
			,Encounter.WardCode
			)
	,Encounter.CasenoteNumber
	,Encounter.DistrictNo
	,SourceAdmissionMethodCode
	,SourceAdmissionMethod
	,AdmissionType = 
		case 
		when AdmissionType = 'Other Non-Elective' then 'Emergency'
		else AdmissionType
		end
	,Specialty = Specialty.NationalSpecialtyLabel
	,Consultant = Consultant.NationalConsultant + ' (' + NationalConsultantCode + ')'
	,EpisodicGPPractice = 
		coalesce(
			GPPractice.GpPracticeCode + ' - ' + GPPractice.GPPracticeName
			,'N/A'
			)

	,APCDiagnosis = 
		case
		when
			exists
				(
				select 
					1
				from 
					Match.Match.MatchTypeDataset 

				inner join WarehouseOLAPMergedV2.Flag.PatientSourceSystemFlag
				on	MatchTypeDataset.SourceDatasetRecno = PatientSourceSystemFlag.PatientDatasetFlagRecno
				and	PatientSourceSystemFlag.SourceSystemID = 1
				and PatientSourceSystemFlag.FlagID = 1

				where
					Encounter.EncounterRecno = MatchTypeDataset.TargetDatasetRecno
				and	MatchTypeDataset.MatchTypeID = 8
				)
		then 1
		else 0
		end

	,Bedman = 
		case
		when
			exists
				(
				select 
					1
				from 
					Match.Match.MatchTypeDataset 

				inner join WarehouseOLAPMergedV2.Flag.PatientSourceSystemFlag
				on	MatchTypeDataset.SourceDatasetRecno = PatientSourceSystemFlag.PatientDatasetFlagRecno
				and	PatientSourceSystemFlag.SourceSystemID = 2
				and PatientSourceSystemFlag.FlagID = 1

				where
					Encounter.EncounterRecno = MatchTypeDataset.TargetDatasetRecno
				and	MatchTypeDataset.MatchTypeID = 8
				)
		then 1
		else 0
		end

	,SpecialRegister = 
		case
		when
			exists
				(
				select 
					1
				from 
					Match.Match.MatchTypeDataset 

				inner join WarehouseOLAPMergedV2.Flag.PatientSourceSystemFlag
				on	MatchTypeDataset.SourceDatasetRecno = PatientSourceSystemFlag.PatientDatasetFlagRecno
				and	PatientSourceSystemFlag.SourceSystemID = 3
				and PatientSourceSystemFlag.FlagID = 1

				where
					Encounter.EncounterRecno = MatchTypeDataset.TargetDatasetRecno
				and	MatchTypeDataset.MatchTypeID = 8
				)
		then 1
		else 0
		end
from 
	APC.Encounter

inner join APC.AdmissionMethod
on	Encounter.AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID

inner join WH.Specialty
on	Encounter.SpecialtyID = Specialty.SourceSpecialtyID

inner join WH.Consultant
on	Encounter.ConsultantID = Consultant.SourceConsultantID

inner join WH.Ward
on	Encounter.WardID = Ward.SourceWardID

left join WarehouseOLAPMergedV2.WH.GPPractice
on coalesce(Encounter.GPPracticeCode, 'N/A') = GPPractice.GpPracticeCode

where 
	exists
		(
		select 
			1
		from 
			Match.Match.MatchTypeDataset 
		where 
			TargetDatasetRecno = Encounter.EncounterRecno
		and MatchTypeDataset.MatchTypeID = 8	
		)
	
and	(
		case 
		when AdmissionType = 'Other Non-Elective' then 'Emergency'
		else AdmissionType
		end in 
			(
			select
				Value
			from
				WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@AdmissionType,',')
			)
	or	@AdmissionType is null
	)
and	(
	coalesce(
		Specialty.NationalSpecialtyCode 
		,Encounter.SpecialtyCode
		) in 
			(
			select
				Value
			from
				WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@NationalSpecialtyCode,',')
			)
	or	@NationalSpecialtyCode is null
	)
and	(
	coalesce(
		Ward.LocalWardCode
		,Encounter.WardCode 
		) in 
			(
			select
				Value
			from
				WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@WardCode,',')
			)
	or	@WardCode is null
	)
and	(
	coalesce(
		GPPractice.GpPracticeCode 
		,'N/A'
		) in 
			(
			select
				Value
			from
				WarehouseReportingMerged.Rpt.SSRS_MultiValueParamSplit(@GPPracticeCode,',')
			)
	or	@GPPracticeCode is null
	)
and	(
		cast(Encounter.AdmissionTime as date) >= 
			case @DateRange
			when 'Today' then cast(getdate() as date)
			when 'Previous 3 days' then Dateadd(day,-3,cast(getdate() as date))
			when 'Previous 7 days' then Dateadd(day,-7,cast(getdate() as date))
			when 'Previous 28 days' then Dateadd(day,-28,cast(getdate() as date))
			when 'Previous 12 months' then Dateadd(month,-12,cast(getdate() as date))
			end
	or	@DateRange is null	
	)

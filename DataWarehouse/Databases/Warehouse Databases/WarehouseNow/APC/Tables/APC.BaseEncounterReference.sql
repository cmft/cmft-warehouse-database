﻿CREATE TABLE [APC].[BaseEncounterReference] (
    [EncounterRecno]    INT          NOT NULL,
    [ContextID]         INT          NULL,
    [AdmissionDateID]   INT          NULL,
    [AdmissionMethodID] INT          NULL,
    [SiteID]            INT          NULL,
    [WardID]            INT          NULL,
    [SpecialtyID]       INT          NULL,
    [ConsultantID]      INT          NULL,
    [SexID]             VARCHAR (10) NULL,
    [Created]           DATETIME     NOT NULL,
    [Updated]           DATETIME     NOT NULL,
    [ByWhom]            VARCHAR (50) NULL,
    CONSTRAINT [PK_BaseEncounterReference] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


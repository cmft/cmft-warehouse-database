﻿










CREATE view [APC].[Encounter] as 

select 
	BaseEncounter.EncounterRecno	
	,BaseEncounter.SourceUniqueID	
	,BaseEncounter.SourcePatientNo	
	,BaseEncounter.SourceSpellNo	
	,BaseEncounter.ProviderSpellNo	
	,BaseEncounter.ContextCode	
	,BaseEncounter.AdmissionTime	
	,BaseEncounter.AdmissionMethodCode
	,BaseEncounter.SiteCode
	,BaseEncounter.WardCode	
	,BaseEncounter.SpecialtyCode	
	,BaseEncounter.ConsultantCode	
	,BaseEncounter.CasenoteNumber	
	,BaseEncounter.DistrictNo	
	,BaseEncounter.NHSNumber	
	,BaseEncounter.PatientTitle	
	,BaseEncounter.PatientSurname	
	,BaseEncounter.PatientForename	
	,BaseEncounter.DateOfBirth	
	,BaseEncounter.SexCode	
	,BaseEncounter.Postcode	
	,BaseEncounter.GPPracticeCode
	--,BaseEncounter.EncounterChecksum	

	,BaseEncounterReference.ContextID
	,BaseEncounterReference.AdmissionDateID
	,BaseEncounterReference.AdmissionMethodID
	,BaseEncounterReference.SiteID
	,BaseEncounterReference.WardID
	,BaseEncounterReference.SpecialtyID
	,BaseEncounterReference.ConsultantID
	,BaseEncounterReference.SexID
	,DirectorateCode = Directorate.SourceAllocationID

	,BaseEncounter.Created	
	,BaseEncounter.Updated	
	,BaseEncounter.ByWhom
from 
	APC.BaseEncounter

inner join APC.BaseEncounterReference
on	BaseEncounter.EncounterRecno = BaseEncounterReference.EncounterRecno

inner join WarehouseOLAPMergedV2.Allocation.DatasetAllocation DirectorateAllocation
on	DirectorateAllocation.DatasetRecno = BaseEncounter.EncounterRecno
and DirectorateAllocation.AllocationTypeID = 10
and DirectorateAllocation.DatasetCode = 'APCNOW'

inner join WarehouseOLAPMergedV2.Allocation.Allocation Directorate
on	Directorate.AllocationTypeID = DirectorateAllocation.AllocationTypeID
and	Directorate.AllocationID = DirectorateAllocation.AllocationID














﻿



CREATE view [APC].[FactEncounter] as


select
	EncounterRecno
	,ContextID
	,AdmissionDateID
	,AdmissionMethodID
	,WardID
	,SpecialtyID
	,ConsultantID
	,SexID
	,DirectorateID
	,LearningDisability = 
			cast(
			case
			when exists
				(
				select 
					1
				from 
					Match.Match.MatchTypeDataset 

				inner join WarehouseOLAPMergedV2.Flag.PatientSourceSystemFlag
				on	MatchTypeDataset.SourceDatasetRecno = PatientSourceSystemFlag.PatientDatasetFlagRecno
				and PatientSourceSystemFlag.FlagID = 1

				where
					Encounter.EncounterRecno = MatchTypeDataset.TargetDatasetRecno
				and	MatchTypeDataset.MatchTypeID = 8
				)
			then 1
			else 0
			end
			as bit)
	,Cases = 1

from
	APC.Encounter

inner join WH.Directorate
on	Directorate.DirectorateCode = Encounter.DirectorateCode




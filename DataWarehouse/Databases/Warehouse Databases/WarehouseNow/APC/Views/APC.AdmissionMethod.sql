﻿

create view [APC].[AdmissionMethod] as

select 
	[SourceContextCode]
	,[SourceContext]
	,[SourceAdmissionMethodID]
	,[SourceAdmissionMethodCode]
	,[SourceAdmissionMethod]
	--,[LocalAdmissionMethodID]
	,[LocalAdmissionMethodCode]
	,[LocalAdmissionMethod]
	--  ,[NationalAdmissionMethodID]
	,[NationalAdmissionMethodCode]
	,[NationalAdmissionMethod]
	,[AdmissionTypeCode]
	,[AdmissionType]
from
	[WarehouseOLAPMergedV2].[APC].[AdmissionMethod]




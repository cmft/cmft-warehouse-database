﻿
CREATE Procedure [ETL].[ImportAPCBaseEncounter] as  

--load data
exec ETL.BuildTLoadAPCBaseEncounter 
exec ETL.LoadAPCBaseEncounter

declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseEncounter]'
	,@NewRows int = 0

--generate reference data
exec WarehouseOLAPMergedV2.ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WarehouseOLAPMergedV2.WH.BuildMember

--Build the BaseReference table
exec ETL.BuildAPCBaseEncounterReference 


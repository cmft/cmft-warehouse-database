﻿

CREATE procedure [ETL].[BuildTLoadAPCBaseEncounter] as

truncate table ETL.TLoadAPCBaseEncounter

insert into ETL.TLoadAPCBaseEncounter
(
	 SourceUniqueID	
	,SourcePatientNo
	,SourceSpellNo 
	,ProviderSpellNo 
	,ContextCode 
	,AdmissionTime 
	,AdmissionMethodCode
	,SiteCode
	,WardCode
	,SpecialtyCode 
	,ConsultantCode	
	,CasenoteNumber	
	,DistrictNo	
	,NHSNumber	
	,PatientTitle
	,PatientSurname	
	,PatientForename 
	,DateOfBirth 
	,SexCode	
	,Postcode	
	,GPPracticeCode
	,Created	
	,Updated	
	,ByWhom
	,EncounterChecksum
)
select
	SourceUniqueID	
	,SourcePatientNo
	,SourceSpellNo 
	,ProviderSpellNo 
	,ContextCode 
	,AdmissionTime 
	,AdmissionMethodCode
	,SiteCode
	,WardCode
	,SpecialtyCode 
	,ConsultantCode	
	,CasenoteNumber	
	,DistrictNo	
	,NHSNumber	
	,PatientTitle
	,PatientSurname	
	,PatientForename 
	,DateOfBirth 
	,SexCode	
	,Postcode	
	,GPPracticeCode
	,Created	
	,Updated	
	,ByWhom
	,EncounterChecksum = 
		checksum(
			SourceUniqueID	
			,SourcePatientNo
			,SourceSpellNo 
			,ProviderSpellNo 
			,ContextCode 
			,AdmissionTime 
			,AdmissionMethodCode
			,SiteCode
			,WardCode
			,SpecialtyCode 
			,ConsultantCode	
			,CasenoteNumber	
			,DistrictNo	
			,NHSNumber	
			,PatientTitle
			,PatientSurname	
			,PatientForename 
			,DateOfBirth 
			,SexCode	
			,Postcode	
			,GPPracticeCode
			)
from
	(
	--Central
	select
		SourceUniqueID	
		,SourcePatientNo
		,SourceSpellNo 
		,ProviderSpellNo 
		,ContextCode 
		,AdmissionTime 
		,AdmissionMethodCode
		,SiteCode
		,WardCode
		,SpecialtyCode 
		,ConsultantCode	
		,CasenoteNumber	
		,DistrictNo	
		,NHSNumber	
		,PatientTitle
		,PatientSurname	
		,PatientForename 
		,DateOfBirth 
		,SexCode	
		,Postcode	
		,GPPracticeCode
		,Created = getdate()
		,Updated = getdate()
		,ByWhom = suser_name()
	from
		ETL.ExtractCenAPCBaseEncounter
	
	union
	
	--Trafford
	select
		SourceUniqueID	
		,SourcePatientNo
		,SourceSpellNo 
		,ProviderSpellNo 
		,ContextCode 
		,AdmissionTime 
		,AdmissionMethodCode
		,SiteCode
		,WardCode
		,SpecialtyCode 
		,ConsultantCode	
		,CasenoteNumber	
		,DistrictNo	
		,NHSNumber	
		,PatientTitle
		,PatientSurname	
		,PatientForename 
		,DateOfBirth 
		,SexCode	
		,Postcode	
		,GPPracticeCode
		,getdate()
		,getdate()
		,suser_name()
	from
		ETL.ExtractTraAPCBaseEncounter

	) Encounter





﻿

CREATE procedure [ETL].[BuildAPCBaseEncounterReference] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

delete 
from
	APC.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		APC.BaseEncounter Encounter
	where
		Encounter.EncounterRecno = BaseEncounterReference.EncounterRecno
	)

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseEncounterReference target
using
	(
	select
		BaseEncounter.EncounterRecno
		,ContextID = Context.ContextID
		,AdmissionDateID =
			coalesce(
				 AdmissionDate.DateID

				,case
				when cast(BaseEncounter.AdmissionTime as date) is null
				then NullDate.DateID

				when cast(BaseEncounter.AdmissionTime as date) < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)	
		,AdmissionMethodID = AdmissionMethod.SourceAdmissionMethodID
		,SiteID = Site.SourceSiteID
		,WardID = Ward.SourceWardID	
		,SpecialtyID = Specialty.SourceSpecialtyID	
		,ConsultantID = Consultant.SourceConsultantID	
		,SexID = Sex.SourceSexID
		,BaseEncounter.Created
		,BaseEncounter.Updated
		,BaseEncounter.ByWhom
	from
		APC.BaseEncounter

	inner join WarehouseOLAPMergedV2.WH.Context
	on	Context.ContextCode = BaseEncounter.ContextCode

	inner join WarehouseOLAPMergedV2.APC.AdmissionMethod
	on	AdmissionMethod.SourceAdmissionMethodCode = coalesce(BaseEncounter.AdmissionMethodCode, '-1')
	and	AdmissionMethod.SourceContextCode = BaseEncounter.ContextCode

	inner join WarehouseOLAPMergedV2.WH.Site
	on	Site.SourceSiteCode = coalesce(BaseEncounter.SiteCode, '-1')
	and	Site.SourceContextCode = BaseEncounter.ContextCode

	inner join WarehouseOLAPMergedV2.WH.Ward
	on	Ward.SourceWardCode = coalesce(BaseEncounter.WardCode, '-1')
	and	Ward.SourceContextCode = BaseEncounter.ContextCode

	inner join WarehouseOLAPMergedV2.WH.Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(BaseEncounter.SpecialtyCode, '-1')
	and	Specialty.SourceContextCode = BaseEncounter.ContextCode

	inner join WarehouseOLAPMergedV2.WH.Consultant
	on	Consultant.SourceConsultantCode = coalesce(BaseEncounter.ConsultantCode, '-1')
	and	Consultant.SourceContextCode = BaseEncounter.ContextCode

	inner join WarehouseOLAPMergedV2.WH.Sex
	on	Sex.SourceSexCode = coalesce(BaseEncounter.SexCode, '-1')
	and	Sex.SourceContextCode = BaseEncounter.ContextCode

	inner join WarehouseOLAPMergedV2.WH.Calendar AdmissionDate
	on	AdmissionDate.TheDate = cast(BaseEncounter.AdmissionTime as date)

	inner join WarehouseOLAPMergedV2.WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	inner join WarehouseOLAPMergedV2.WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	inner join WarehouseOLAPMergedV2.Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	inner join WarehouseOLAPMergedV2.WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	) source
	on	source.EncounterRecno = target.EncounterRecno

	when not matched
	then
		insert
			(
			EncounterRecno
			,ContextID
			,AdmissionDateID
			,AdmissionMethodID
			,SiteID
			,WardID
			,SpecialtyID
			,ConsultantID
			,SexID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.EncounterRecno
			,source.ContextID
			,source.AdmissionDateID
			,source.AdmissionMethodID
			,source.SiteID
			,source.WardID
			,source.SpecialtyID
			,source.ConsultantID
			,source.SexID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and	isnull(target.ContextID, 0) = isnull(source.ContextID, 0)
		and	isnull(target.AdmissionDateID, 0) = isnull(source.AdmissionDateID, 0)
		and	isnull(target.AdmissionMethodID, 0) = isnull(source.AdmissionMethodID, 0)
		and	isnull(target.SiteID, 0) = isnull(source.SiteID, 0)
		and	isnull(target.WardID, 0) = isnull(source.WardID, 0)
		and	isnull(target.SpecialtyID, 0) = isnull(source.SpecialtyID, 0)
		and	isnull(target.ConsultantID, 0) = isnull(source.ConsultantID, 0)
		and	isnull(target.SexID, 0) = isnull(source.SexID, 0)
		and	isnull(target.Created, '1 jan 1900') = isnull(source.Created, '1 jan 1900')
		and	isnull(target.Updated, '1 jan 1900') = isnull(source.Updated, '1 jan 1900')
		and	isnull(target.ByWhom, '') = isnull(source.ByWhom, '')
		)
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.ContextID = source.ContextID
			,target.AdmissionDateID = source.AdmissionDateID
			,target.AdmissionMethodID = source.AdmissionMethodID
			,target.SiteID = source.SiteID
			,target.WardID = source.WardID
			,target.SpecialtyID = source.SpecialtyID
			,target.ConsultantID = source.ConsultantID
			,target.SexID = source.SexID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Warehouse.Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
	,@Elapsed
	,@inserted
	,@updated
	,@deleted






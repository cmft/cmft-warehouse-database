﻿
CREATE procedure [ETL].[LoadAPCBaseEncounter]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.BaseEncounter target
using
	(
	select
		SourceUniqueID	
		,SourcePatientNo
		,SourceSpellNo 
		,ProviderSpellNo 
		,ContextCode 
		,AdmissionTime 
		,AdmissionMethodCode 
		,SiteCode
		,WardCode
		,SpecialtyCode
		,ConsultantCode
		,CasenoteNumber	
		,DistrictNo	
		,NHSNumber	
		,PatientTitle
		,PatientSurname	
		,PatientForename 
		,DateOfBirth 
		,SexCode	
		,Postcode
		,GPPracticeCode
		,EncounterChecksum	
		,Created	
		,Updated	
		,ByWhom
	from
		ETL.TLoadAPCBaseEncounter
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	and Source.ContextCode = target.ContextCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID	
			,SourcePatientNo
			,SourceSpellNo 
			,ProviderSpellNo 
			,ContextCode 
			,AdmissionTime 
			,AdmissionMethodCode
			,SiteCode
			,WardCode
			,SpecialtyCode
			,ConsultantCode
			,CasenoteNumber	
			,DistrictNo	
			,NHSNumber	
			,PatientTitle
			,PatientSurname	
			,PatientForename 
			,DateOfBirth 
			,SexCode	
			,Postcode
			,GPPracticeCode
			,EncounterChecksum	
			,Created	
			,Updated	
			,ByWhom
			)
		values
			(
			source.SourceUniqueID	
			,source.SourcePatientNo
			,source.SourceSpellNo 
			,source.ProviderSpellNo 
			,source.ContextCode 
			,source.AdmissionTime 
			,source.AdmissionMethodCode
			,source.SiteCode
			,source.WardCode
			,source.SpecialtyCode
			,source.ConsultantCode
			,source.CasenoteNumber	
			,source.DistrictNo	
			,source.NHSNumber	
			,source.PatientTitle
			,source.PatientSurname	
			,source.PatientForename 
			,source.DateOfBirth 
			,source.SexCode	
			,source.Postcode
			,source.GPPracticeCode
			,source.EncounterChecksum	
			,source.Created	
			,source.Updated	
			,source.ByWhom
			)

	when matched
	and	target.EncounterChecksum <> source.EncounterChecksum
	then
		update	
		set
			 target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.ProviderSpellNo = source.ProviderSpellNo
			,target.ContextCode = source.ContextCode
			,target.AdmissionTime = source.AdmissionTime
			,target.AdmissionMethodCode = source.AdmissionMethodCode
			,target.SiteCode = source.SiteCode
			,target.WardCode = source.WardCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ConsultantCode = source.ConsultantCode
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DistrictNo = source.DistrictNo
			,target.NHSNumber = source.NHSNumber
			,target.PatientTitle = source.PatientTitle
			,target.PatientSurname = source.PatientSurname
			,target.PatientForename = source.PatientForename
			,target.DateOfBirth = source.DateOfBirth
			,target.SexCode = source.SexCode
			,target.Postcode = source.Postcode
			,target.GPPracticeCode = source.GPPracticeCode
			,target.EncounterChecksum = source.EncounterChecksum
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec Warehouse.dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



﻿

create view [ETL].[ExtractCenObservationBaseObservation] as 

select 
	SourceUniqueID
	,ObservationSetSourceUniqueID 
	,MeasureID
	,NumericResult
	,ListID
	,FlagCode
	,CreatedByUserID
	,CreatedTime
	,ModifiedByUserID
	,LastModifiedTime 
	,ContextCode = 'CEN||PTRACK'

from
	(
	select
		SourceUniqueID = OBSERVATION_SET_DETAIL.OSDET_PK
		,ObservationSetSourceUniqueID = OBSERVATION_SET_DETAIL.OBSET_PK
		,MeasureID = MEASURE_SET_ITEM.OBMSR_PK
		,NumericResult = coalesce(
							OBSERVATION_SET_DETAIL.MEASURE_INT 
							,OBSERVATION_SET_DETAIL.MEASURE_DECIMAL 	
							)
		,ListID = OBSERVATION_SET_DETAIL.MEASURE_LIST 
		,FlagCode = OBSERVATION_SET_DETAIL.MEASURE_FLAG 
		,CreatedByuserID = OBSERVATION_SET_DETAIL.CREATED_BY_USERR_PK
		,CreatedTime = OBSERVATION_SET_DETAIL.CREATED_DTTM
		,ModifiedByUserID = OBSERVATION_SET_DETAIL.LAST_MODIFIED_BY_USERR_PK
		,LastModifiedTime = OBSERVATION_SET_DETAIL.LAST_MODIFIED_DTTM
	from
		PatientrackSS.dbo.OBSERVATION_SET_DETAIL  

	inner join PatientrackSS.dbo.MEASURE_SET_ITEM 
	on	OBSERVATION_SET_DETAIL.MSITM_PK = MEASURE_SET_ITEM.MSITM_PK

	inner join PatientrackSS.dbo.OBSERVATION_SET
	on	OBSERVATION_SET.OBSET_PK = OBSERVATION_SET_DETAIL.OBSET_PK

	inner join PatientrackSS.dbo.ADMISSION
	on	ADMISSION.ADMSN_PK = OBSERVATION_SET.ADMSN_PK

	where
		ADMISSION.DISCHARGED_DTTM is null
		and	coalesce(
				OBSERVATION_SET_DETAIL.Measure_INT 
				,OBSERVATION_SET_DETAIL.MEASURE_DECIMAL 	
				,OBSERVATION_SET_DETAIL.MEASURE_LIST
				,OBSERVATION_SET_DETAIL.MEASURE_FLAG 
				) is not null

	) ObservationSetDetail





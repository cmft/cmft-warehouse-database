﻿



CREATE view [ETL].[ExtractCenAPCBaseEncounter] as 


/****************************************************************

	20151028	RR	Created
					There are DQ issues, patients appearing on Bedman as still an inpatient, but discharged or never admitted on PAS.
					Possible issue with messaging.  Logged a call on 23/10/2015
					
****************************************************************/

select 
	SourceUniqueID = HospSpell.SpellID
	,SourcePatientNo = HospSpell.Patient
	,SourceSpellNo = HospSpell.IPEpisode
	,ProviderSpellNo = HospSpell.Patient + '/' + HospSpell.IPEpisode
	,ContextCode = 'CEN||BEDMAN'
	,AdmissionTime = HospSpell.AdmitDate
	,AdmissionMethodCode = IPAdmits.AdmitMethod
	,SiteCode = Locations.Hospital
	,WardCode = 
			coalesce(
				HospSpell.CurrWard
				,Locations.PASCode
				)
	,SpecialtyCode = HospSpell.CurrSpec
	,ConsultantCode = HospSpell.CurrCon
	,CasenoteNumber = HospSpell.HospitalNo
	,DistrictNo = PASPatient.DistrictNo
	,NHSNumber = 
			replace(
				PASPatient.NHSNumber
				,' '
				,''
				)
	,PatientTitle = PASPatient.Title
	,PatientSurname = 
			coalesce(
				Patients.Surname
				,PASPatient.Surname
				)
	,PatientForename = 
			coalesce(
				Patients.Forename
				,PASPatient.Forenames
				)
    ,DateOfBirth = 
			coalesce(
				Patients.DoB
				,PASPatient.DateOfBirth
				)
    ,SexCode = 
			coalesce(
				Patients.Gender
				,PASPatient.SexCode
				)
    ,Postcode = PatientAddress.Postcode
    ,GPPracticeCode = PatientGp.GpPracticeCode
from
	BedManTest.dbo.HospSpell 

left join BedmanTest.dbo.Patients
on	HospSpell.Patient = Patients.Patient

left join BedmanTest.dbo.IPAdmits
on	IPAdmits.Patient = HospSpell.Patient
and IPAdmits.AdmitDate = HospSpell.AdmitDate

left join Warehouse.PAS.Patient PASPatient
on	HospSpell.Patient = PASPatient.SourcePatientNo

left join Warehouse.PAS.PatientGp
on	PatientGp.SourcePatientNo = HospSpell.Patient
and	HospSpell.AdmitDate between PatientGp.EffectiveFromDate and coalesce(PatientGp.EffectiveToDate, getdate())

left join Warehouse.PAS.PatientAddress
on	PatientAddress.SourcePatientNo = HospSpell.Patient
and	HospSpell.AdmitDate between PatientAddress.EffectiveFromDate and coalesce(PatientAddress.EffectiveToDate, getdate())

left join BedmanTest.dbo.Locations
on	HospSpell.CurrLoc = Locations.LocationID

where 
	HospSpell.AdmitDate is not null 
and HospSpell.DischTime is null
and not exists
	(
	select
		1
	from
		WarehouseOLAPMergedV2.APC.BaseEncounter
	where
		BaseEncounter.SourcePatientNo = HospSpell.Patient
	and BaseEncounter.ContextCode = 'CEN||PAS'
	and BaseEncounter.DischargeTime > HospSpell.AdmitDate
	)







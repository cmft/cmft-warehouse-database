﻿

create view [ETL].[ExtractCenObservationBaseAlert] as 

select
	SourceUniqueID
	,CasenoteNumber
	,DateOfBirth 
	,SpecialtyID
	,LocationID
	,AdmissionSourceUniqueID
	,TypeID
	,ReasonID
	,ObservationSetSourceUniqueID
	,CreatedTime
	,BedsideDueTime
	,EscalationTime
	,AcceptedTime
	,AttendedByUserID
	,AttendanceTypeID
	,Comment
	,SeverityID
	,ClinicianSeniorityID
	,AcceptanceRemindersRemaining
	,ChainSequenceNumber
	,NextReminderTime
	,ClosedTime
	,ClosureReasonID
	,ClosedByUserID
	,CancelledByObservationSetSourceUniqueID
	,LastModifiedTime
	,ContextCode = 'CEN||PTRACK'

from
	(
	select --top 1000 

		SourceUniqueID = Alert.ALERT_PK
		,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
		,DateOfBirth = Admission.BIRTH_DATE
		,SpecialtyID = AdmissionUnit.UNITT_RFVAL
		,LocationID = WardStay.WARDD_RFVAL
		,AdmissionSourceUniqueID = Alert.ADMSN_PK
		,TypeID = ALTYP_RFVAL
		,ReasonID = ALRAR_RFVAL
		,ObservationSetSourceUniqueID = OBSET_PK
		,CreatedTime = Alert.CREATED_DTTM 
		,BedsideDueTime = BEDSIDE_DUE_DTTM 
		,EscalationTime = ESCALATION_DTTM
		,AcceptedTime = ACCEPTED_DTTM 
		,AttendedByUserID = ATTENDED_BY_USERR_PK
		,AttendanceTypeID = ATTYP_RFVAL
		,Comment = nullif(cast(Alert.NOTE as varchar(max)), '')
		,SeverityID = CURR_ALSEV_RFVAL
		,ClinicianSeniorityID = CURR_DRSEN_RFVAL
		,AcceptanceRemindersRemaining = ACCEPT_REMINDERS_REMAINING
		,ChainSequenceNumber = CURR_SEQUENCE_NUMBER
		,NextReminderTime = NEXT_REMINDER_DUE_DTTM 
		,ClosedTime = CLOSED_DTTM
		,ClosureReasonID = ALCLR_RFVAL
		,ClosedByUserID = CLOSED_BY_USERR_PK
		,CancelledByObservationSetSourceUniqueID = CANCEL_OBSET_PK
		,LastModifiedTime = Alert.LAST_MODIFIED_DTTM 
		,ContextCode = 'CEN||PTRACK'

	from
		PatientrackSS.dbo.Alert Alert

	inner join PatientrackSS.dbo.ADMISSION Admission
	on	Admission.ADMSN_PK = Alert.ADMSN_PK
	and	coalesce(Admission.DELETED_FLAG, 0) = 0

	inner join PatientrackSS.dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
	on    PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
	and   PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
	and   coalesce(PatientIdentifier.DELETED_FLAG, 0) = 0 

	inner join
			(
			select
				Alert.ALERT_PK 
				,AdmissionUnit.UNITT_RFVAL
			from
				PatientrackSS.dbo.ADMISSION_UNIT AdmissionUnit
			inner join PatientrackSS.dbo.Alert Alert
			on	Alert.ADMSN_PK = AdmissionUnit.ADMSN_PK
			and Alert.CREATED_DTTM between AdmissionUnit.START_DTTM and coalesce(AdmissionUnit.END_DTTM, getdate())
			
			where
				coalesce(AdmissionUnit.DELETED_FLAG, 0) = 0
			and	not exists
						(
						select
							1
						from
							PatientrackSS.dbo.ADMISSION_UNIT AdmissionUnitNext
						where
							Alert.ADMSN_PK = AdmissionUnitNext.ADMSN_PK
						and coalesce(AdmissionUnitNext.DELETED_FLAG, 0) = 0
						and Alert.CREATED_DTTM between AdmissionUnitNext.START_DTTM and coalesce(AdmissionUnitNext.END_DTTM, getdate())
						and AdmissionUnitNext.ADUNT_PK > AdmissionUnit.ADUNT_PK
						)

			) AdmissionUnit

			on	Alert.ALERT_PK = AdmissionUnit.ALERT_PK
					
	inner join
			(
			select
				Alert.ALERT_PK 
				,WardStay.WARDD_RFVAL
			from
				PatientrackSS.dbo.WARD_STAY WardStay
			inner join PatientrackSS.dbo.Alert Alert
			on	Alert.ADMSN_PK = WardStay.ADMSN_PK
			and Alert.CREATED_DTTM between WardStay.START_DTTM and coalesce(WardStay.END_DTTM, getdate())
			
			where
				coalesce(WardStay.DELETED_FLAG, 0) = 0
			and	not exists
						(
						select
							1
						from
							PatientrackSS.dbo.WARD_STAY WardStayNext
						where
							Alert.ADMSN_PK = WardStayNext.ADMSN_PK
						and coalesce(WardStayNext.DELETED_FLAG, 0) = 0
						and Alert.CREATED_DTTM between WardStayNext.START_DTTM and coalesce(WardStayNext.END_DTTM, getdate())
						and WardStayNext.WRDST_PK > WardStay.WRDST_PK
						)

			) WardStay

			on	Alert.ALERT_PK = WardStay.ALERT_PK

	where
		Admission.DISCHARGED_DTTM is null

	) Alert

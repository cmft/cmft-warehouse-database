﻿

create view [ETL].[ExtractCenObservationBaseObservationSet] as

select
	SourceUniqueID 
	,CasenoteNumber 
	,DateOfBirth 
	,SpecialtyID
	,LocationID 
	,LocationStartTime
	,ReplacedSourceUniqueID 
	,CurrentObservationSetFlag 
	,AdditionalObservationSetFlag 
	,AdmissionSourceUniqueID 
	,EarlyWarningScoreRegimeApplicationID 
	,ObservationProfileApplicationID 
	,ObservationNotTakenReasonID
	,ClinicianPresentSeniorityID 
	,StartTime 
	,TakenTime
	,OverallRiskIndexCode 
	,OverallAssessedStatusID 
	,AlertSeverityID 
	,DueTime 
	,DueTimeStatusID 
	,DueTimeCreatedBySourceUniqueID 
	,LastModifiedTime
	,ContextCode = 'CEN||PTRACK'

from
	(
	select --top 1000 
		SourceUniqueID = ObservationSet.OBSET_PK
		,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
		,DateOfBirth = Admission.BIRTH_DATE
		,SpecialtyID = AdmissionUnit.UNITT_RFVAL
		,LocationID = WardStay.WARDD_RFVAL
		,LocationStartTime = WardStay.START_DTTM
		,ReplacedSourceUniqueID = ObservationSet.PARENT_OBSET_PK
		,CurrentObservationSetFlag = ObservationSet.CURRENT_OBSET_FLAG
		,AdditionalObservationSetFlag = ObservationSet.ADDITIONAL_OBSET_FLAG
		,AdmissionSourceUniqueID = ObservationSet.ADMSN_PK
		,EarlyWarningScoreRegimeApplicationID = ObservationSet.EWSAP_PK
		,ObservationProfileApplicationID = ObservationSet.OBPAP_PK
		,ObservationNotTakenReasonID = ObservationSet.ONTRN_RFVAL
		,ClinicianPresentSeniorityID = ObservationSet.PRESENT_DRSEN_RFVAL
		,StartTime = ObservationSet.OBS_START_DTTM
		,TakenTime = ObservationSet.OBS_TAKEN_DTTM
		,OverallRiskIndexCode = ObservationSet.OVERALL_RISK_INDEX
		,OverallAssessedStatusID = ObservationSet.ASDST_RFVAL
		,AlertSeverityID = ObservationSet.ALSEV_RFVAL
		,DueTime = ObservationDue.OBS_DUE_DTTM 
		,DueTimeStatusID = 
						coalesce( -- additional logic from Sarah Ingleby 22.10.14. Default due time status to on time if we have not due time status
							case
							when ObservationSet.ADDITIONAL_OBSET_FLAG = 1 --treat additional obs as on time
							then 588
							else ObservationDue.ODTST_RFVAL
							end
							, 588
							)
		,DueTimeCreatedBySourceUniqueID =
						coalesce(
							case
							when ObservationSet.ADDITIONAL_OBSET_FLAG = 1 
							then ObservationSet.OBSET_PK
							else ObservationDue.OBDUT_OBSET_PK
							end
							, ObservationSet.OBSET_PK
							)
		,LastModifiedTime = ObservationSet.LAST_MODIFIED_DTTM
	from
		PatientrackSS.dbo.OBSERVATION_SET ObservationSet

	inner join PatientrackSS.dbo.Admission Admission
	on	Admission.ADMSN_PK = ObservationSet.ADMSN_PK
	and	coalesce(Admission.DELETED_FLAG, 0) = 0

	inner join PatientrackSS.dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
	on	PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
	and	PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
	and	coalesce(PatientIdentifier.DELETED_FLAG, 0) = 0	

	inner join
			(
				select
					ObservationSet.OBSET_PK
					,AdmissionUnit.UNITT_RFVAL
				from
					PatientrackSS.dbo.ADMISSION_UNIT AdmissionUnit
				inner join PatientrackSS.dbo.OBSERVATION_SET ObservationSet
				on	ObservationSet.ADMSN_PK = AdmissionUnit.ADMSN_PK
				and ObservationSet.OBS_TAKEN_DTTM between AdmissionUnit.START_DTTM and coalesce(AdmissionUnit.END_DTTM, getdate())
				
				where
					coalesce(AdmissionUnit.DELETED_FLAG, 0) = 0
				and	not exists
							(
							select
								1
							from
								PatientrackSS.dbo.ADMISSION_UNIT AdmissionUnitNext
							where
								ObservationSet.ADMSN_PK = AdmissionUnitNext.ADMSN_PK
							and coalesce(AdmissionUnitNext.DELETED_FLAG, 0) = 0
							and	ObservationSet.OBS_TAKEN_DTTM between AdmissionUnitNext.START_DTTM and coalesce(AdmissionUnitNext.END_DTTM, getdate())
							and	AdmissionUnitNext.ADUNT_PK > AdmissionUnit.ADUNT_PK
							)

			) AdmissionUnit

	on	ObservationSet.OBSET_PK = AdmissionUnit.OBSET_PK
					
	inner join
		(
			select
				ObservationSet.OBSET_PK
				,WardStay.WARDD_RFVAL
				,WardStay.START_DTTM
			from
				PatientrackSS.dbo.WARD_STAY WardStay
			inner join PatientrackSS.dbo.OBSERVATION_SET ObservationSet
			on	ObservationSet.ADMSN_PK = WardStay.ADMSN_PK
			and ObservationSet.OBS_TAKEN_DTTM between wardstay.START_DTTM and coalesce(wardstay.END_DTTM, getdate()) 
			
			where
				coalesce(WardStay.DELETED_FLAG, 0) = 0
			and	not exists
						(
						select
							1
						from
							PatientrackSS.dbo.WARD_STAY WardStayNext
						where
							ObservationSet.ADMSN_PK = WardStayNext.ADMSN_PK
						and	coalesce(WardStayNext.DELETED_FLAG, 0) = 0
						and	ObservationSet.OBS_TAKEN_DTTM between WardStayNext.START_DTTM and coalesce(WardStayNext.END_DTTM, getdate())
						and	WardStayNext.WRDST_PK > WardStay.WRDST_PK
						)

		) WardStay
	on	ObservationSet.OBSET_PK = WardStay.OBSET_PK

	left outer join
				(
					select
						ODTST_OBSET_PK
						,OBS_DUE_DTTM 
						,ODTST_RFVAL
						,OBDUT_OBSET_PK
					from
						PatientrackSS.dbo.OBSERVATION_DUE_TIME ObservationDue
					where
						coalesce(ObservationDue.DELETED_FLAG, 0) = 0
					and ObservationDue.ODTST_OBSET_PK is not null
					--and ObservationDue.ODTST_RFVAL <> 592 -- Originally out as requested by S. Ingleby out have included as numbers seem significant
					and not exists
								(
								select
									1
								from
									PatientrackSS.dbo.OBSERVATION_DUE_TIME ObservationDueNext
								where
									ObservationDue.ODTST_OBSET_PK = ObservationDueNext.ODTST_OBSET_PK
								and coalesce(ObservationDueNext.DELETED_FLAG, 0) = 0
								--and	ObservationDue.ODTST_RFVAL = 592 -- Originally filtered out as requested by S. Ingleby out have included as numbers seem significant
								and	ObservationDueNext.OBDUT_PK > ObservationDue.OBDUT_PK
								) 

				) ObservationDue

	on ObservationDue.ODTST_OBSET_PK = ObservationSet.OBSET_PK
						

	where
		Admission.DISCHARGED_DTTM is null
	and ObservationSet.PARENT_OBSET_PK is null --check

	) ObservationSet


﻿





--drop view [ETL].[TImportTraAPCBaseEncounter]

CREATE view [ETL].[ExtractTraAPCBaseEncounter] as 



/****************************************************************

	20151028	RR	Created
					There are DQ issues, patients appearing on Bedman as still an inpatient, but discharged or never admitted on PAS.
					Possible issue with messaging.  Logged a call on 23/10/2015
					
****************************************************************/
  
 
select 
	SourceUniqueID = HospSpell.SpellID
	,SourcePatientNo = HospSpell.Patient
	,SourceSpellNo = HospSpell.IPEpisode
	,ProviderSpellNo = 'A' + HospSpell.IPEpisode
	,ContextCode = 'TRA||BEDMAN'
	,AdmissionDate = cast(HospSpell.AdmitDate as date)
	,AdmissionTime = HospSpell.AdmitDate
	,AdmissionMethodCode = IPAdmits.AdmitMethod
	,SiteCode = Locations.Hospital
	,WardCode = 
			coalesce(
				CurrWard
				,Locations.PASCode
				)
	,SpecialtyCode = CurrSpec
	,ConsultantCode = CurrCon
	,CasenoteNumber = HospSpell.HospitalNo + '/A' + HospSpell.IPEpisode
	,DistrictNo = PAT_EXTZIS_ID
	,NHSNumber = PAT_NATIONALNUMBER1
	,PatientTitle = Pat_Title
	,PatientSurname = PAT_FAMILYNAME
	,PatientForename = PAT_GIVENNAME
    ,DateOfBirth = PAT_BIRTHDATE
    ,SexCode = PAT_SEX
    ,Postcode = PAT_ZIPCODE
    ,GPPracticeCode = PRACTICE.Prac_Code
from
	Bedman_Trafford.dbo.HospSpell 

inner join xdatawh.Replica_UltraGendaPro.dbo.Patient Patient
on Patient.PAT_INTZIS_ID = HospSpell.Patient
and not exists
		(
		select
			1
		from
			xdatawh.Replica_UltraGendaPro.dbo.Patient Latest
		where 
			Latest.PAT_INTZIS_ID = Patient.PAT_INTZIS_ID
		and Latest.Pat_ID > Patient.Pat_ID
		)

left join xdatawh.Replica_UltraGendaPro.dbo.PRACTICE
on PRACTICE.Prac_ID = Patient.Prac_ID

left join Bedman_Trafford.dbo.IPAdmits
on IPAdmits.Patient = HospSpell.Patient
and IPAdmits.AdmitDate = HospSpell.AdmitDate

left join Bedman_Trafford.dbo.Locations
on HospSpell.CurrLoc = Locations.LocationID

where 
	HospSpell.AdmitDate is not null 
and HospSpell.DischTime is null
and not exists
	(
	Select
		1
	from
		WarehouseOLAPMergedV2.APC.BaseEncounter
	where
		BaseEncounter.SourcePatientNo = HospSpell.Patient
	and BaseEncounter.ContextCode = 'TRA||UG'
	and BaseEncounter.DischargeTime > HospSpell.AdmitDate
	)









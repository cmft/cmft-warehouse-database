﻿
create view [ETL].[ExtractCenObservationBasePatientFlag] as

select
	SourceUniqueID 
	,CasenoteNumber 
	,DateOfBirth 
	,AdmissionSourceUniqueID 
	,PatientFlagID 
	,StartTime 
	,EndTime 
	,Comment
	,CreatedByUserID 
	,CreatedTime 
	,LastModifiedByUserID 
	,LastModifiedTime 
	,EWSDisabled 
	,ContextCode = 'CEN||PTRACK'
from
	(
	select --top 1000 
		SourceUniqueID = ADFLG_PK
		,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
		,DateOfBirth = Admission.BIRTH_DATE
		,AdmissionSourceUniqueID = AdmissionFlag.ADMSN_PK
		,PatientFlagID = PFLAG_RFVAL 
		,StartTime = START_DTTM 
		,EndTime = END_DTTM 
		,Comment = nullif(cast(AdmissionFlag.NOTE as varchar(max)), '')
		,CreatedByUserID = AdmissionFlag.CREATED_BY_USERR_PK 
		,CreatedTime = AdmissionFlag.CREATED_DTTM
		,LastModifiedByUserID = AdmissionFlag.LAST_MODIFIED_BY_USERR_PK 
		,LastModifiedTime = AdmissionFlag.LAST_MODIFIED_DTTM
		,EWSDisabled = NCEWS_FLAG

	from
		PatientrackSS.dbo.ADMISSION_FLAG AdmissionFlag

	inner join PatientrackSS.dbo.ADMISSION Admission
	on	Admission.ADMSN_PK = AdmissionFlag.ADMSN_PK
	and	coalesce(Admission.DELETED_FLAG, 0) = 0

	inner join PatientrackSS.dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
	on    PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
	and   PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
	and   coalesce(PatientIdentifier.DELETED_FLAG, 0) = 0 

	where
		Admission.DISCHARGED_DTTM is null

	) PatientFlag



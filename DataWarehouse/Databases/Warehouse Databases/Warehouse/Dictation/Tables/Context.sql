﻿CREATE TABLE [Dictation].[Context] (
    [ContextCode] INT          NOT NULL,
    [Context]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ContextCode] ASC)
);


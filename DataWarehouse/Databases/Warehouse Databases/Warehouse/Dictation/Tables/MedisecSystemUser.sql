﻿CREATE TABLE [Dictation].[MedisecSystemUser] (
    [UserCode]       VARCHAR (20) NOT NULL,
    [UserName]       VARCHAR (50) NULL,
    [UserType]       CHAR (1)     NULL,
    [UserDepartment] VARCHAR (50) NULL,
    CONSTRAINT [PK_MedisecSystemUser] PRIMARY KEY CLUSTERED ([UserCode] ASC)
);


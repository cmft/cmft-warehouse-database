﻿CREATE TABLE [Dictation].[User] (
    [UserCode]     INT          NOT NULL,
    [User]         VARCHAR (50) NULL,
    [HasVRLicence] BIT          NULL,
    PRIMARY KEY CLUSTERED ([UserCode] ASC)
);


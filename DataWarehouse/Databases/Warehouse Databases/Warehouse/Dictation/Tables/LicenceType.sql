﻿CREATE TABLE [Dictation].[LicenceType] (
    [LicenceCode] INT            NOT NULL,
    [LicenceType] NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([LicenceCode] ASC)
);


﻿CREATE TABLE [Dictation].[Worktype] (
    [WorkTypeCode] INT          NOT NULL,
    [WorkType]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([WorkTypeCode] ASC)
);


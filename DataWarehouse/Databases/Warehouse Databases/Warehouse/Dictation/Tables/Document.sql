﻿CREATE TABLE [Dictation].[Document] (
    [DocumentRecno]            INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]           INT           NOT NULL,
    [Document]                 VARCHAR (MAX) NULL,
    [BatchNumber]              INT           NULL,
    [Length]                   FLOAT (53)    NULL,
    [DocumentTypeCode]         INT           NULL,
    [Priority]                 INT           NULL,
    [StatusCode]               INT           NULL,
    [WorktypeCode]             INT           NULL,
    [WorkStationCode]          INT           NULL,
    [InterfaceCode]            INT           NULL,
    [GroupCode]                INT           NULL,
    [AuthorCode]               INT           NULL,
    [TranscriptionistCode]     INT           NULL,
    [AuthoriserCode]           INT           NULL,
    [CreationDate]             DATETIME      NULL,
    [CreationTime]             DATETIME      NULL,
    [RecognitionRate]          INT           NULL,
    [WaitingTimeCorrection]    FLOAT (53)    NULL,
    [CorrectionDate]           DATETIME      NULL,
    [CorrectionTime]           DATETIME      NULL,
    [CorrectionTimeTaken]      FLOAT (53)    NULL,
    [WaitingTimeAuthorization] FLOAT (53)    NULL,
    [AuthorisationDate]        DATETIME      NULL,
    [AuthorisationTime]        DATETIME      NULL,
    [PassThroughTimeTaken]     FLOAT (53)    NULL,
    [ContextCode]              INT           NULL,
    [DictationTimeTaken]       FLOAT (53)    NULL,
    [CorrectionRate]           INT           NULL,
    [WarehouseInterfaceCode]   VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Document] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC, [WarehouseInterfaceCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DictationDocument_DocumentRecno]
    ON [Dictation].[Document]([DocumentRecno] ASC);


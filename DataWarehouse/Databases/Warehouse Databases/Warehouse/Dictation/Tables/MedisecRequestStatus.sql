﻿CREATE TABLE [Dictation].[MedisecRequestStatus] (
    [StatusCode] CHAR (2)      NOT NULL,
    [Status]     VARCHAR (100) NULL,
    CONSTRAINT [PK__Status__6A7B44FD246B8232] PRIMARY KEY CLUSTERED ([StatusCode] ASC)
);


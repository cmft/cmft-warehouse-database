﻿CREATE TABLE [Dictation].[MedisecDepartment] (
    [DepartmentCode] VARCHAR (100) NOT NULL,
    [Department]     VARCHAR (100) NULL,
    CONSTRAINT [PK__MedisecD__6EA8896C79808AA0] PRIMARY KEY CLUSTERED ([DepartmentCode] ASC)
);


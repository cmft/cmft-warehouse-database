﻿CREATE TABLE [Dictation].[SpeechMagicUser] (
    [UserCode]          INT            NOT NULL,
    [UserName]          NVARCHAR (50)  NOT NULL,
    [AdminRights]       BIT            NULL,
    [RecognitionRights] BIT            NULL,
    [ExcludeFromATR]    BIT            NULL,
    [UserPassword]      NVARCHAR (24)  NULL,
    [UserFullName]      NVARCHAR (255) NULL,
    [Gender]            INT            NULL,
    [DataVersion]       INT            NOT NULL,
    [DeleteVersion]     INT            NOT NULL,
    [HashValue]         INT            NOT NULL,
    [CheckOutTimeStamp] INT            NULL,
    [CheckOutTaskId]    INT            NULL
);


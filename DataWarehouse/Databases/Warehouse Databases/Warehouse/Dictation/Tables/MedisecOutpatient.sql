﻿CREATE TABLE [Dictation].[MedisecOutpatient] (
    [MedisecOutpatientRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]        INT           NOT NULL,
    [AppointmentTime]        DATETIME      NOT NULL,
    [DoctorCode]             VARCHAR (50)  NOT NULL,
    [InterfaceCode]          VARCHAR (4)   NOT NULL,
    [Created]                SMALLDATETIME NOT NULL,
    [Updated]                SMALLDATETIME NULL,
    [ByWhom]                 VARCHAR (50)  NULL,
    CONSTRAINT [PK_MedisecOutpatient] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [AppointmentTime] ASC, [DoctorCode] ASC)
);


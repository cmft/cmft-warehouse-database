﻿CREATE TABLE [Dictation].[UserGroup] (
    [UserCode]  INT NOT NULL,
    [GroupCode] INT NOT NULL,
    CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED ([UserCode] ASC, [GroupCode] ASC)
);


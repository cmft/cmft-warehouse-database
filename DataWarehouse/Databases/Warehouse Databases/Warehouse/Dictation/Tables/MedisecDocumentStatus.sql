﻿CREATE TABLE [Dictation].[MedisecDocumentStatus] (
    [DocumentStatusCode] CHAR (1)      NOT NULL,
    [DocumentStatus]     VARCHAR (100) NULL,
    CONSTRAINT [PK__Document__0F2F669A283C1316] PRIMARY KEY CLUSTERED ([DocumentStatusCode] ASC)
);


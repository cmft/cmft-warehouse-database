﻿CREATE TABLE [Dictation].[Licence] (
    [UserCode]    INT NOT NULL,
    [LicenceCode] INT NOT NULL,
    CONSTRAINT [PK_Licence] PRIMARY KEY CLUSTERED ([UserCode] ASC, [LicenceCode] ASC)
);


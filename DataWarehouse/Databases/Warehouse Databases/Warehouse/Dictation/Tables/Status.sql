﻿CREATE TABLE [Dictation].[Status] (
    [StatusCode]   INT          NOT NULL,
    [Status]       VARCHAR (50) NOT NULL,
    [DisplayIndex] INT          NULL,
    PRIMARY KEY CLUSTERED ([StatusCode] ASC)
);


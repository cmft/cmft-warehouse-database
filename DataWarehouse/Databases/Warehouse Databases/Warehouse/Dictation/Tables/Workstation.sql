﻿CREATE TABLE [Dictation].[Workstation] (
    [WorkStationCode] INT          NOT NULL,
    [Workstation]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([WorkStationCode] ASC)
);


﻿CREATE TABLE [Dictation].[Interface] (
    [InterfaceCode] INT          NOT NULL,
    [Interface]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([InterfaceCode] ASC)
);


﻿CREATE TABLE [Dictation].[TrainingRecord] (
    [id]               INT            IDENTITY (1, 1) NOT NULL,
    [UserCode]         INT            NULL,
    [DateAttended]     DATETIME       NULL,
    [CourseRef]        NVARCHAR (255) NULL,
    [Description]      NVARCHAR (255) NULL,
    [Username]         NVARCHAR (255) NULL,
    [Surname]          NVARCHAR (255) NULL,
    [Firstname]        NVARCHAR (255) NULL,
    [Outcome]          NVARCHAR (255) NULL,
    [TrainerId]        NVARCHAR (255) NULL,
    [TrainerFirstname] NVARCHAR (255) NULL,
    [TrainerSurname]   NVARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([id] ASC)
);


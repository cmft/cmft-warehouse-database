﻿CREATE TABLE [Dictation].[DocumentType] (
    [DocumentTypeCode] INT          NOT NULL,
    [DocumentType]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([DocumentTypeCode] ASC)
);


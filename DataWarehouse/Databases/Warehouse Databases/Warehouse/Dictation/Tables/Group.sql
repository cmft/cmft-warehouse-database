﻿CREATE TABLE [Dictation].[Group] (
    [GroupCode]     INT          NOT NULL,
    [Group]         VARCHAR (50) NULL,
    [SpecialtyCode] VARCHAR (50) NULL,
    [Specialty]     VARCHAR (50) NULL,
    [Division]      VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([GroupCode] ASC)
);


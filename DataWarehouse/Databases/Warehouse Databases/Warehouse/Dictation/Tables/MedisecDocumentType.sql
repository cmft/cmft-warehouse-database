﻿CREATE TABLE [Dictation].[MedisecDocumentType] (
    [DocumentTypeCode] CHAR (1)     NOT NULL,
    [DocumentType]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Document__8952B4CF209AF14E] PRIMARY KEY CLUSTERED ([DocumentTypeCode] ASC)
);


﻿CREATE TABLE [Dictation].[MedisecInpatient] (
    [MedisecInpatientRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]       INT           NOT NULL,
    [AdmissionTime]         DATETIME      NOT NULL,
    [SpellSequenceNo]       INT           NOT NULL,
    [RequestTypeCode]       VARCHAR (2)   NULL,
    [InterfaceCode]         VARCHAR (20)  NULL,
    [ContextCode]           VARCHAR (10)  NULL,
    [Created]               SMALLDATETIME NULL,
    [Updated]               SMALLDATETIME NULL,
    [ByWhom]                VARCHAR (50)  NULL,
    CONSTRAINT [PK_MedisecInpatient] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [AdmissionTime] ASC, [SpellSequenceNo] ASC)
);


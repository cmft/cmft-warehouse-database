﻿CREATE TABLE [ChildHealth].[ReferredTo] (
    [ReferredToCode] VARCHAR (6)  NOT NULL,
    [ReferredTo]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReferredTo] PRIMARY KEY CLUSTERED ([ReferredToCode] ASC)
);


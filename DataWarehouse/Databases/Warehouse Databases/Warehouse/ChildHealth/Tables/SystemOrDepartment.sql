﻿CREATE TABLE [ChildHealth].[SystemOrDepartment] (
    [SystemOrDepartmentCode] VARCHAR (50) NOT NULL,
    [SystemOrDepartment]     VARCHAR (50) NULL,
    CONSTRAINT [PK_SystemOrDepartment] PRIMARY KEY CLUSTERED ([SystemOrDepartmentCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[ChildHealthEncounter] (
    [SourceUniqueID]       INT      NOT NULL,
    [EntitySourceUniqueID] INT      NULL,
    [StartDate]            DATETIME NULL,
    CONSTRAINT [PK_ChildHealthEncounter] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


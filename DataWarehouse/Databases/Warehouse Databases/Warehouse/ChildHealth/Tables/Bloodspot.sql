﻿CREATE TABLE [ChildHealth].[Bloodspot] (
    [EntitySourceUniqueID] INT           NOT NULL,
    [SpecimenDate]         DATE          NOT NULL,
    [SequenceNo]           INT           NOT NULL,
    [TestCode]             VARCHAR (6)   NOT NULL,
    [TestResultCode]       VARCHAR (14)  NULL,
    [ResultLetterDate]     DATE          NULL,
    [SpecimenResultDate]   DATE          NULL,
    [ResultReceivedDate]   DATE          NULL,
    [ResultEnteredDate]    DATE          NULL,
    [NotKnownDateFlag]     VARCHAR (1)   NULL,
    [AcknowledgedDate]     DATE          NULL,
    [SentDate]             DATE          NULL,
    [TestGivenCode]        VARCHAR (50)  NULL,
    [BadSpotIndicator]     VARCHAR (50)  NULL,
    [ReasonForRepeatCode]  VARCHAR (60)  NULL,
    [LabNumber]            VARCHAR (50)  NULL,
    [SpecimenComment]      VARCHAR (116) NULL,
    [TestComment]          VARCHAR (120) NULL,
    CONSTRAINT [PK_Bloodspot] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [SpecimenDate] ASC, [SequenceNo] ASC, [TestCode] ASC)
);


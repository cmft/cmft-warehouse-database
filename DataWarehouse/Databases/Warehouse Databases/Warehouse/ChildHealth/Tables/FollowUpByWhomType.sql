﻿CREATE TABLE [ChildHealth].[FollowUpByWhomType] (
    [FollowUpByWhomTypeCode] VARCHAR (50)  NOT NULL,
    [FollowUpByWhomType]     VARCHAR (250) NULL,
    CONSTRAINT [PK_FollowUpByWhomType] PRIMARY KEY CLUSTERED ([FollowUpByWhomTypeCode] ASC)
);


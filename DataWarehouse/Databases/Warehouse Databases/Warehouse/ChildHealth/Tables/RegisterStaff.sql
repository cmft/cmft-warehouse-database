﻿CREATE TABLE [ChildHealth].[RegisterStaff] (
    [RegisterStaffCode] VARCHAR (50) NOT NULL,
    [RegisterStaff]     VARCHAR (50) NULL,
    CONSTRAINT [PK_RegisterStaff] PRIMARY KEY CLUSTERED ([RegisterStaffCode] ASC)
);


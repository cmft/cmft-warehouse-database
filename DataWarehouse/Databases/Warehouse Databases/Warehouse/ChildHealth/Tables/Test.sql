﻿CREATE TABLE [ChildHealth].[Test] (
    [TestCode] VARCHAR (6)  NOT NULL,
    [Test]     VARCHAR (40) NULL,
    CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED ([TestCode] ASC)
);


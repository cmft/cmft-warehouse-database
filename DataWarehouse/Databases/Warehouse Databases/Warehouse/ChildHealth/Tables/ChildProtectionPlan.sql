﻿CREATE TABLE [ChildHealth].[ChildProtectionPlan] (
    [EntitySourceUniqueID]          INT           NOT NULL,
    [PlacedOnPlanDate]              DATE          NOT NULL,
    [ReasonRemovedFromPlanCode]     VARCHAR (50)  NULL,
    [ResponsibleLocalAuthorityCode] VARCHAR (10)  NULL,
    [SocialWorker]                  VARCHAR (250) NULL,
    [RemovedFromPlanDate]           DATE          NULL,
    [SocialServicesDepartmentCode]  VARCHAR (10)  NULL,
    [SupervisionEndDate]            DATE          NULL,
    [SupervisionStartDate]          DATE          NULL,
    [Supervision]                   VARCHAR (1)   NULL,
    [LeadClinicianCode]             VARCHAR (50)  NULL,
    [LeadClinicianTypeCode]         VARCHAR (50)  NULL,
    [VulnerabilityFactorCode]       VARCHAR (10)  NULL,
    CONSTRAINT [PK_ChildProtectionPlan] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [PlacedOnPlanDate] ASC)
);


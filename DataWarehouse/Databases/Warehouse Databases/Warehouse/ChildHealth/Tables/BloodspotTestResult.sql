﻿CREATE TABLE [ChildHealth].[BloodspotTestResult] (
    [BloodspotTestResultCode] VARCHAR (14) NOT NULL,
    [BloodspotTestResult]     VARCHAR (36) NULL,
    CONSTRAINT [PK_BloodspotTestResult] PRIMARY KEY CLUSTERED ([BloodspotTestResultCode] ASC)
);


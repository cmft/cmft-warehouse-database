﻿CREATE TABLE [ChildHealth].[LookedAfterLegalStatus] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [LookedAfterDate]      DATE         NOT NULL,
    [StartDate]            DATE         NOT NULL,
    [EndDate]              DATE         NULL,
    [LegalStatusCode]      VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_LookedAfterLegalStatus] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [LookedAfterDate] ASC, [StartDate] ASC, [LegalStatusCode] ASC)
);


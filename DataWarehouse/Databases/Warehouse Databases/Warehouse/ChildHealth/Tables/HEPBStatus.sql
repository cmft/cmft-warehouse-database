﻿CREATE TABLE [ChildHealth].[HEPBStatus] (
    [HEPBStatusCode] VARCHAR (1)  NOT NULL,
    [HEPBStatus]     VARCHAR (50) NULL,
    CONSTRAINT [PK_HEPBStatus] PRIMARY KEY CLUSTERED ([HEPBStatusCode] ASC)
);


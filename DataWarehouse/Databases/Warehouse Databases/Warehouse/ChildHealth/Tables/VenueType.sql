﻿CREATE TABLE [ChildHealth].[VenueType] (
    [VenueTypeCode] VARCHAR (50) NOT NULL,
    [VenueType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_VenueType] PRIMARY KEY CLUSTERED ([VenueTypeCode] ASC)
);


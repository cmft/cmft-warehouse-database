﻿CREATE TABLE [ChildHealth].[ReasonRemovedFromPlan] (
    [ReasonRemovedFromPlanCode] VARCHAR (50) NOT NULL,
    [ReasonRemovedFromPlan]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReasonRemovedFromPlan] PRIMARY KEY CLUSTERED ([ReasonRemovedFromPlanCode] ASC)
);


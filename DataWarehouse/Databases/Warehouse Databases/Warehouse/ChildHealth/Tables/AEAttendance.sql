﻿CREATE TABLE [ChildHealth].[AEAttendance] (
    [EntitySourceUniqueID]   INT           NOT NULL,
    [AttendanceDate]         DATE          NOT NULL,
    [SequenceNo]             VARCHAR (50)  NOT NULL,
    [VenueTypeCode]          VARCHAR (50)  NULL,
    [VenueCode]              VARCHAR (50)  NULL,
    [Comment]                VARCHAR (250) NULL,
    [AEDiagnosisCode]        VARCHAR (50)  NULL,
    [ReasonForAttending]     VARCHAR (250) NULL,
    [ReasonForDischargeCode] VARCHAR (50)  NULL,
    [WhereDischarged]        VARCHAR (50)  NULL,
    [FollowUp]               VARCHAR (50)  NULL,
    [FollowUpByWhomCode]     VARCHAR (50)  NULL,
    [FollowUpByWhomTypeCode] VARCHAR (50)  NULL,
    CONSTRAINT [PK_AEAttendance] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [AttendanceDate] ASC, [SequenceNo] ASC)
);


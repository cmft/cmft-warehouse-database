﻿CREATE TABLE [ChildHealth].[ReasonForDischarge] (
    [ReasonForDischargeCode] VARCHAR (50)  NOT NULL,
    [ReasonForDischarge]     VARCHAR (250) NULL,
    CONSTRAINT [PK_ReasonForDischarge] PRIMARY KEY CLUSTERED ([ReasonForDischargeCode] ASC)
);


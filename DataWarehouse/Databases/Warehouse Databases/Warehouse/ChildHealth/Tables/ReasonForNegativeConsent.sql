﻿CREATE TABLE [ChildHealth].[ReasonForNegativeConsent] (
    [ReasonForNegativeConsentCode] VARCHAR (50) NOT NULL,
    [ReasonForNegativeConsent]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReasonForNegativeConsent] PRIMARY KEY CLUSTERED ([ReasonForNegativeConsentCode] ASC)
);


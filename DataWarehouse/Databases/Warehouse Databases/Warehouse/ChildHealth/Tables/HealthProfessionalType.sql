﻿CREATE TABLE [ChildHealth].[HealthProfessionalType] (
    [HealthProfessionalTypeCode] VARCHAR (50)  NOT NULL,
    [HealthProfessionalType]     VARCHAR (250) NULL,
    CONSTRAINT [PK_HealthProfessionalType] PRIMARY KEY CLUSTERED ([HealthProfessionalTypeCode] ASC)
);


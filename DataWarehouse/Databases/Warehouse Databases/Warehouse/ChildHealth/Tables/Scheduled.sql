﻿CREATE TABLE [ChildHealth].[Scheduled] (
    [ScheduledCode] VARCHAR (1)  NOT NULL,
    [Scheduled]     VARCHAR (30) NULL,
    CONSTRAINT [PK_Scheduled] PRIMARY KEY CLUSTERED ([ScheduledCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[Reference] (
    [ReferenceDomainCode] VARCHAR (3)  NOT NULL,
    [ReferenceCode]       VARCHAR (12) NOT NULL,
    [Reference]           VARCHAR (40) NULL,
    [ReservedCode]        VARCHAR (1)  NULL,
    [StartDate]           DATETIME     NULL,
    [EndDate]             DATETIME     NULL,
    [Switch]              VARCHAR (1)  NULL,
    [Alert]               VARCHAR (1)  NULL,
    [EpisodeGroup]        VARCHAR (50) NULL,
    CONSTRAINT [PK_Reference] PRIMARY KEY CLUSTERED ([ReferenceDomainCode] ASC, [ReferenceCode] ASC)
);


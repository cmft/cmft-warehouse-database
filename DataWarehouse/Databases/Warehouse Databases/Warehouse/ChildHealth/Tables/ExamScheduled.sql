﻿CREATE TABLE [ChildHealth].[ExamScheduled] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [ExamCode]             VARCHAR (6)  NOT NULL,
    [DueDate]              DATE         NOT NULL,
    [VenueCode]            VARCHAR (13) NOT NULL,
    CONSTRAINT [PK_ExamScheduled] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [ExamCode] ASC, [DueDate] ASC, [VenueCode] ASC)
);


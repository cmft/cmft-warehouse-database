﻿CREATE TABLE [ChildHealth].[Need] (
    [NeedCode] VARCHAR (50)  NOT NULL,
    [Need]     VARCHAR (250) NULL,
    CONSTRAINT [PK_Need] PRIMARY KEY CLUSTERED ([NeedCode] ASC)
);


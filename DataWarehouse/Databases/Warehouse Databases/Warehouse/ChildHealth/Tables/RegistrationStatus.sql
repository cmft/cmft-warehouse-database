﻿CREATE TABLE [ChildHealth].[RegistrationStatus] (
    [RegistrationStatusCode] VARCHAR (1)  NOT NULL,
    [RegistrationStatus]     VARCHAR (30) NULL,
    CONSTRAINT [PK_RegistrationStatus] PRIMARY KEY CLUSTERED ([RegistrationStatusCode] ASC)
);


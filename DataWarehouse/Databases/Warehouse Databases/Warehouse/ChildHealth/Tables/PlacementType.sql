﻿CREATE TABLE [ChildHealth].[PlacementType] (
    [PlacementTypeCode] VARCHAR (10)  NOT NULL,
    [PlacementType]     VARCHAR (250) NULL,
    CONSTRAINT [PK_PlacementType] PRIMARY KEY CLUSTERED ([PlacementTypeCode] ASC)
);


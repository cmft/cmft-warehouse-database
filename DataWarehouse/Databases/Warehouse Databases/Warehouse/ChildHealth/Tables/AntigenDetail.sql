﻿CREATE TABLE [ChildHealth].[AntigenDetail] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [EventDate]            DATE         NOT NULL,
    [SequenceNo]           INT          NOT NULL,
    [AntigenValue]         VARCHAR (50) NOT NULL,
    [ReasonNotGivenCode]   VARCHAR (6)  NULL,
    [SupplementaryDose]    VARCHAR (1)  NULL,
    [DoseIdentifier]       VARCHAR (2)  NULL,
    [AntigenCode]          VARCHAR (6)  NULL,
    CONSTRAINT [PK_Antigen] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [EventDate] ASC, [SequenceNo] ASC, [AntigenValue] ASC)
);


﻿CREATE TABLE [ChildHealth].[ReasonNotExamined] (
    [ReasonNotExaminedCode] VARCHAR (6)  NOT NULL,
    [ReasonNotExamined]     VARCHAR (30) NULL,
    CONSTRAINT [PK_ReasonNotExamined] PRIMARY KEY CLUSTERED ([ReasonNotExaminedCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[PersonGivingConsentType] (
    [PersonGivingConsentTypeCode] VARCHAR (50)  NOT NULL,
    [PersonGivingConsentType]     VARCHAR (120) NULL,
    CONSTRAINT [PK_PersonGivingConsentType] PRIMARY KEY CLUSTERED ([PersonGivingConsentTypeCode] ASC)
);


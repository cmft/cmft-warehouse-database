﻿CREATE TABLE [ChildHealth].[ExamHistoryReferral] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [ExamDate]             DATE         NOT NULL,
    [ExamCode]             VARCHAR (6)  NOT NULL,
    [ReferredToCode]       VARCHAR (6)  NOT NULL,
    [ClinicCode]           VARCHAR (13) NULL,
    [ReferralDate]         DATE         NULL,
    [SequenceNo]           VARCHAR (50) NULL,
    CONSTRAINT [PK_ExamHistoryReferral] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [ExamDate] ASC, [ExamCode] ASC, [ReferredToCode] ASC)
);


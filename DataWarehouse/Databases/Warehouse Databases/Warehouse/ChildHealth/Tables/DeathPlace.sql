﻿CREATE TABLE [ChildHealth].[DeathPlace] (
    [DeathPlaceCode] VARCHAR (50) NOT NULL,
    [DeathPlace]     VARCHAR (50) NULL,
    CONSTRAINT [PK_DeathPlace] PRIMARY KEY CLUSTERED ([DeathPlaceCode] ASC)
);


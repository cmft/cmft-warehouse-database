﻿CREATE TABLE [ChildHealth].[Death] (
    [EntitySourceUniqueID]          INT           NOT NULL,
    [DateOfDeath]                   DATE          NULL,
    [TimeOfDeath]                   TIME (7)      NULL,
    [DeathPlaceCode]                VARCHAR (50)  NULL,
    [CauseOfDeath]                  VARCHAR (60)  NULL,
    [CauseOfDeathCode1]             VARCHAR (50)  NULL,
    [CauseOfDeathCode2]             VARCHAR (50)  NULL,
    [CauseOfDeathCode3]             VARCHAR (50)  NULL,
    [CauseOfDeathCode4]             VARCHAR (50)  NULL,
    [CauseOfDeathCode5]             VARCHAR (50)  NULL,
    [CauseOfDeathCode6]             VARCHAR (50)  NULL,
    [CauseOfDeathCode7]             VARCHAR (50)  NULL,
    [CauseOfDeathCode8]             VARCHAR (50)  NULL,
    [DeathNotifiedByCode]           VARCHAR (50)  NULL,
    [DeathNotifiedDate]             DATE          NULL,
    [RapidResponseClinicianCode]    VARCHAR (250) NULL,
    [RapidResponseTeamInvolvedCode] VARCHAR (5)   NULL,
    [TypeOfDeathCode]               VARCHAR (50)  NULL,
    CONSTRAINT [PK_Death] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC)
);


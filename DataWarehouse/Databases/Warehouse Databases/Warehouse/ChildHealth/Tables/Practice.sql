﻿CREATE TABLE [ChildHealth].[Practice] (
    [PracticeCode]         VARCHAR (12)  NOT NULL,
    [Practice]             VARCHAR (50)  NULL,
    [Address1]             VARCHAR (50)  NULL,
    [Address2]             VARCHAR (50)  NULL,
    [Address3]             VARCHAR (50)  NULL,
    [Address4]             VARCHAR (50)  NULL,
    [Postcode]             VARCHAR (8)   NULL,
    [TelephoneNo]          VARCHAR (115) NULL,
    [SeniorPartnerCode]    VARCHAR (6)   NULL,
    [NationalCode]         VARCHAR (6)   NULL,
    [PCTCode]              VARCHAR (5)   NULL,
    [Email]                VARCHAR (50)  NULL,
    [FaxNo]                VARCHAR (50)  NULL,
    [MainFacilityTypeCode] VARCHAR (50)  NULL,
    [MainFacilityCode]     VARCHAR (50)  NULL,
    [LocalID]              VARCHAR (50)  NULL,
    [Active]               VARCHAR (3)   NULL,
    [PooledGP]             VARCHAR (50)  NULL,
    CONSTRAINT [PK_Practice] PRIMARY KEY CLUSTERED ([PracticeCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[LookedAfterCarerDetail] (
    [EntitySourceUniqueID] INT           NOT NULL,
    [LookedAfterDate]      DATE          NOT NULL,
    [StartDate]            DATE          NOT NULL,
    [EndDate]              DATE          NULL,
    [CarerDetail]          VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_LookedAfterCarerDetail] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [LookedAfterDate] ASC, [StartDate] ASC, [CarerDetail] ASC)
);


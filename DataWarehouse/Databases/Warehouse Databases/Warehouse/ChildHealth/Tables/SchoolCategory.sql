﻿CREATE TABLE [ChildHealth].[SchoolCategory] (
    [SchoolCategoryCode] VARCHAR (6)  NOT NULL,
    [SchoolCategory]     VARCHAR (30) NULL,
    CONSTRAINT [PK_SchoolCategory] PRIMARY KEY CLUSTERED ([SchoolCategoryCode] ASC)
);


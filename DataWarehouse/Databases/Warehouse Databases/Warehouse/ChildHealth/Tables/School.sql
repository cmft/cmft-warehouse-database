﻿CREATE TABLE [ChildHealth].[School] (
    [SchoolCode]                VARCHAR (10) NOT NULL,
    [School]                    VARCHAR (60) NULL,
    [Address1]                  VARCHAR (40) NULL,
    [Address2]                  VARCHAR (40) NULL,
    [Address3]                  VARCHAR (40) NULL,
    [Address4]                  VARCHAR (40) NULL,
    [Postcode]                  VARCHAR (8)  NULL,
    [TelephoneNo]               VARCHAR (20) NULL,
    [FaxNo]                     VARCHAR (50) NULL,
    [MaximumAge]                INT          NULL,
    [MinimmumAge]               INT          NULL,
    [ClerksName]                VARCHAR (30) NULL,
    [ClerksTelephoneNo]         VARCHAR (6)  NULL,
    [SchoolCategoryCode]        VARCHAR (6)  NULL,
    [SchoolTypeCode]            VARCHAR (1)  NULL,
    [Active]                    VARCHAR (1)  NULL,
    [PCTCode]                   VARCHAR (3)  NULL,
    [LocalID]                   VARCHAR (50) NULL,
    [NationalSchoolCode]        VARCHAR (50) NULL,
    [EmailAddress]              VARCHAR (50) NULL,
    [DepartmentOfEducationCode] VARCHAR (7)  NULL,
    [EmailAppointment]          VARCHAR (1)  NULL,
    CONSTRAINT [PK_School] PRIMARY KEY CLUSTERED ([SchoolCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[HealthVisitor] (
    [HealthVisitorCode] VARCHAR (5)   NOT NULL,
    [HealthVisitor]     VARCHAR (255) NULL,
    CONSTRAINT [PK_HealthVisitor] PRIMARY KEY CLUSTERED ([HealthVisitorCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[NeonatalInitialExam] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [ExamDate]             DATE         NOT NULL,
    [SequenceNo]           INT          NOT NULL,
    [ExamCentreCode]       VARCHAR (8)  NULL,
    [ExamCategoryCode]     VARCHAR (8)  NULL,
    [ExaminerCode]         VARCHAR (7)  NULL,
    [TestNumber]           INT          NULL,
    [RiskFactorPresent]    VARCHAR (1)  NULL,
    [RiskFactorCode]       VARCHAR (6)  NULL,
    [TestMethodCode]       VARCHAR (6)  NULL,
    [EquipmentCode]        VARCHAR (6)  NULL,
    [RightTestID]          INT          NULL,
    [RightResultCode]      VARCHAR (6)  NULL,
    [LeftTestID]           INT          NULL,
    [LeftTestResultCode]   VARCHAR (6)  NULL,
    [OutcomeCode]          VARCHAR (6)  NULL,
    [ReferralCode]         VARCHAR (6)  NULL,
    [Comments1]            VARCHAR (60) NULL,
    [Comments2]            VARCHAR (60) NULL,
    [VenueTypeCode]        VARCHAR (5)  NULL,
    CONSTRAINT [PK_NeonatalInitialExam] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [ExamDate] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [ChildHealth].[EntityXRef] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [SequenceNo]           INT          NOT NULL,
    [XrefTypeCode]         VARCHAR (12) NULL,
    [XRefValue]            VARCHAR (20) NULL,
    [StartDate]            DATETIME     NULL,
    [EndDate]              DATETIME     NULL,
    CONSTRAINT [PK_EntityXRef_1] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [ChildHealth].[DeathNotifiedBy] (
    [DeathNotifiedByCode] VARCHAR (50) NOT NULL,
    [DeathNotifiedBy]     VARCHAR (50) NULL,
    CONSTRAINT [PK_DeathNotifiedBy] PRIMARY KEY CLUSTERED ([DeathNotifiedByCode] ASC)
);


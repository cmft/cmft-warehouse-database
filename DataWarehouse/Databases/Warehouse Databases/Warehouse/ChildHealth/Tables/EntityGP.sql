﻿CREATE TABLE [ChildHealth].[EntityGP] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [GPCode]               VARCHAR (50) NOT NULL,
    [LocalID]              VARCHAR (50) NULL,
    [PCTStartDate]         DATETIME     NOT NULL,
    [PCTCode]              VARCHAR (50) NULL,
    [PracticeCode]         VARCHAR (50) NULL,
    [SurgeryCode]          VARCHAR (50) NULL,
    CONSTRAINT [PK_EntityGP] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [GPCode] ASC, [PCTStartDate] ASC)
);


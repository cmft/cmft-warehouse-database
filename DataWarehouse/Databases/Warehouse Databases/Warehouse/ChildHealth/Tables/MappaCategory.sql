﻿CREATE TABLE [ChildHealth].[MappaCategory] (
    [MappaCategoryCode] VARCHAR (50)  NOT NULL,
    [MappaCategory]     VARCHAR (250) NULL,
    CONSTRAINT [PK_MappaCategory] PRIMARY KEY CLUSTERED ([MappaCategoryCode] ASC)
);


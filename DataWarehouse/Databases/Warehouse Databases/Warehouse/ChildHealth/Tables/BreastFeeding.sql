﻿CREATE TABLE [ChildHealth].[BreastFeeding] (
    [SourceUniqueID]          VARCHAR (254) NOT NULL,
    [EntitySourceUniqueID]    INT           NOT NULL,
    [StageNo]                 INT           NOT NULL,
    [BreastFeedingResultCode] VARCHAR (50)  NULL,
    [StageDate]               DATE          NULL,
    CONSTRAINT [PK_BreastFeeding] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [StageNo] ASC)
);


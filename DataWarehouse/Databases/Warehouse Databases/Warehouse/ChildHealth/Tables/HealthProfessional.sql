﻿CREATE TABLE [ChildHealth].[HealthProfessional] (
    [HealthProfessionalCode] VARCHAR (50)  NOT NULL,
    [HealthProfessional]     VARCHAR (250) NULL,
    CONSTRAINT [PK_HealthProfessional] PRIMARY KEY CLUSTERED ([HealthProfessionalCode] ASC)
);


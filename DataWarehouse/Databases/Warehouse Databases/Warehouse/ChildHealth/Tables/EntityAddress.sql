﻿CREATE TABLE [ChildHealth].[EntityAddress] (
    [EntitySourceUniqueID] INT           NOT NULL,
    [AddressSeqno]         INT           NOT NULL,
    [AddressTypeCode]      VARCHAR (35)  NULL,
    [AddressStartDate]     DATETIME      NULL,
    [AddressEndDate]       DATETIME      NULL,
    [Address1]             VARCHAR (40)  NULL,
    [Address2]             VARCHAR (40)  NULL,
    [City]                 VARCHAR (40)  NULL,
    [Street]               VARCHAR (40)  NULL,
    [Postcode]             VARCHAR (10)  NULL,
    [Country]              VARCHAR (20)  NULL,
    [Location]             VARCHAR (20)  NULL,
    [Directions]           VARCHAR (500) NULL,
    [TimeZone]             INT           NULL,
    [ObsDst]               VARCHAR (4)   NULL,
    [CouID]                VARCHAR (4)   NULL,
    [SHACode]              VARCHAR (10)  NULL,
    [WardOfResidenceCode]  VARCHAR (10)  NULL,
    [PCTOfResidenceCode]   VARCHAR (10)  NULL,
    CONSTRAINT [PK_EntityAddress] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [AddressSeqno] ASC)
);


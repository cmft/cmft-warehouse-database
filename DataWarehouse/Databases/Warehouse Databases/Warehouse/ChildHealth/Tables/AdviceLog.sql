﻿CREATE TABLE [ChildHealth].[AdviceLog] (
    [EntitySourceUniqueID] INT            NOT NULL,
    [AdviceDate]           DATE           NOT NULL,
    [AdviceIndex]          INT            NOT NULL,
    [AdviceGiven]          VARCHAR (1250) NULL,
    [OnBehalfOfCode]       VARCHAR (50)   NULL,
    CONSTRAINT [PK_AdviceLog] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [AdviceDate] ASC, [AdviceIndex] ASC)
);


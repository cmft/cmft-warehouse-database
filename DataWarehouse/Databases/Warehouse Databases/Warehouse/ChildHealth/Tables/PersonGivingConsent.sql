﻿CREATE TABLE [ChildHealth].[PersonGivingConsent] (
    [PersonGivingConsentCode] VARCHAR (50) NOT NULL,
    [PersonGivingConsent]     VARCHAR (50) NULL,
    CONSTRAINT [PK_PersonGivingConsent] PRIMARY KEY CLUSTERED ([PersonGivingConsentCode] ASC)
);


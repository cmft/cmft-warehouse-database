﻿CREATE TABLE [ChildHealth].[LookedAfterStatus] (
    [LookedAfterStatusCode] VARCHAR (50)  NOT NULL,
    [LookedAfterStatus]     VARCHAR (100) NULL,
    CONSTRAINT [PK_LookedAfterStatus] PRIMARY KEY CLUSTERED ([LookedAfterStatusCode] ASC)
);


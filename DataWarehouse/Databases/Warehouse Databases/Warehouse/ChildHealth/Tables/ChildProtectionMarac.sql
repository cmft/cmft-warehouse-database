﻿CREATE TABLE [ChildHealth].[ChildProtectionMarac] (
    [EntitySourceUniqueID]       VARCHAR (50) NOT NULL,
    [ReferredDate]               DATE         NOT NULL,
    [SequenceNo]                 VARCHAR (50) NOT NULL,
    [EndDate]                    DATE         NULL,
    [MaracReasonForReferralCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_ChildProtectionMarac] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [ReferredDate] ASC, [SequenceNo] ASC)
);


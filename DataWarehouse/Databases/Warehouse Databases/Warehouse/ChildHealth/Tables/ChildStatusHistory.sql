﻿CREATE TABLE [ChildHealth].[ChildStatusHistory] (
    [EntitySourceUniqueID]   INT          NOT NULL,
    [ChangedDate]            DATE         NOT NULL,
    [RegistrationStatusCode] VARCHAR (1)  NULL,
    [VIStatusCode]           VARCHAR (1)  NULL,
    [PreSchoolStatusCode]    VARCHAR (1)  NULL,
    [SchoolStatusCode]       VARCHAR (1)  NULL,
    [LostContactReasonCode]  VARCHAR (6)  NULL,
    [LostContactComment]     VARCHAR (30) NULL,
    [LostContactDate]        DATE         NULL,
    [ByWhom]                 VARCHAR (30) NULL,
    [StartDate]              DATE         NULL,
    CONSTRAINT [PK_ChildStatusHistory] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [ChangedDate] ASC)
);


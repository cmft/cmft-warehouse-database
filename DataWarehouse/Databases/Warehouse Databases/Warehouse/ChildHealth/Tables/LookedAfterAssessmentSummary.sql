﻿CREATE TABLE [ChildHealth].[LookedAfterAssessmentSummary] (
    [EntitySourceUniqueID]      INT            NOT NULL,
    [AssessmentTypeCode]        VARCHAR (50)   NOT NULL,
    [RequestDate]               DATE           NOT NULL,
    [ChildSeen]                 VARCHAR (50)   NULL,
    [FirstAppointmentDate]      DATE           NULL,
    [SecondAppointmentDate]     DATE           NULL,
    [ThirdAppointmentDate]      DATE           NULL,
    [EscLetterSentDate]         DATE           NULL,
    [KeyInformationRequestDate] DATE           NULL,
    [PeperworkCompleteDate]     DATE           NULL,
    [ReportReceivedDate]        DATE           NULL,
    [TeamInformedDate]          DATE           NULL,
    [KeyInformationReceived]    VARCHAR (50)   NULL,
    [PaperworkProcessed]        VARCHAR (50)   NULL,
    [PaperworkScanned]          VARCHAR (50)   NULL,
    [DelayReason]               VARCHAR (1250) NULL,
    [RequestSentTo]             VARCHAR (1250) NULL,
    [TeamInformed]              VARCHAR (50)   NULL,
    CONSTRAINT [PK_LookedAfterAssessmentSummary] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [AssessmentTypeCode] ASC, [RequestDate] ASC)
);


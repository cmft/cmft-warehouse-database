﻿CREATE TABLE [ChildHealth].[SocialServicesDepartment] (
    [SocialServicesDepartmentCode] VARCHAR (10)  NOT NULL,
    [SocialServicesDepartment]     VARCHAR (250) NULL,
    CONSTRAINT [PK_SocialServicesDepartment] PRIMARY KEY CLUSTERED ([SocialServicesDepartmentCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[Relationship] (
    [EntitySourceUniqueID]             INT          NOT NULL,
    [RelationshipEntitySourceUniqueID] INT          NOT NULL,
    [RelationshipCode]                 VARCHAR (12) NOT NULL,
    [RoleTypeCode]                     VARCHAR (12) NULL,
    [StartDate]                        DATETIME     NULL,
    [EndDate]                          DATETIME     NULL,
    CONSTRAINT [PK_Relationship] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [RelationshipEntitySourceUniqueID] ASC, [RelationshipCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[ChildFlag] (
    [FlagCode]                 VARCHAR (50) NOT NULL,
    [EntitySourceUniqueID]     INT          NOT NULL,
    [AddedDate]                DATE         NOT NULL,
    [RemovedDate]              DATE         NULL,
    [FlagReasonForRemovalCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_ChildFlag] PRIMARY KEY CLUSTERED ([FlagCode] ASC, [EntitySourceUniqueID] ASC, [AddedDate] ASC)
);


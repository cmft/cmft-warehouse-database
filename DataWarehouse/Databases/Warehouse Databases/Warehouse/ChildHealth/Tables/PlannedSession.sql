﻿CREATE TABLE [ChildHealth].[PlannedSession] (
    [PlannedSessionCode] VARCHAR (10) NOT NULL,
    [PlannedSession]     VARCHAR (50) NULL,
    CONSTRAINT [PK_PlannedSession] PRIMARY KEY CLUSTERED ([PlannedSessionCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[BirthPlace] (
    [BirthPlaceCode] VARCHAR (10) NOT NULL,
    [BirthPlace]     VARCHAR (50) NULL,
    CONSTRAINT [PK_BirthPlace] PRIMARY KEY CLUSTERED ([BirthPlaceCode] ASC)
);


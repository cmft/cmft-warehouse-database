﻿CREATE TABLE [ChildHealth].[Venue] (
    [VenueCode] VARCHAR (50)  NOT NULL,
    [Venue]     VARCHAR (250) NULL,
    CONSTRAINT [PK_Venue] PRIMARY KEY CLUSTERED ([VenueCode] ASC)
);


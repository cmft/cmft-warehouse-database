﻿CREATE TABLE [ChildHealth].[AEDiagnosis] (
    [AEDiagnosisCode] VARCHAR (50)  NOT NULL,
    [AEDiagnosis]     VARCHAR (250) NULL,
    CONSTRAINT [PK_AEDiagnosis] PRIMARY KEY CLUSTERED ([AEDiagnosisCode] ASC)
);


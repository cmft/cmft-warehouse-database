﻿CREATE TABLE [ChildHealth].[Meeting] (
    [EntitySourceUniqueID] INT          NOT NULL,
    [MeetingTypeCode]      VARCHAR (50) NOT NULL,
    [MeetingDate]          DATE         NOT NULL,
    [MeetingTime]          TIME (7)     NULL,
    [LocationCode]         VARCHAR (50) NULL,
    CONSTRAINT [PK_Meeting] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [MeetingTypeCode] ASC, [MeetingDate] ASC)
);


﻿CREATE TABLE [ChildHealth].[CauseOfDeath] (
    [CauseOfDeathCode] VARCHAR (50) NOT NULL,
    [CauseOfDeath]     VARCHAR (80) NULL,
    CONSTRAINT [PK_CauseOfDeath] PRIMARY KEY CLUSTERED ([CauseOfDeathCode] ASC)
);


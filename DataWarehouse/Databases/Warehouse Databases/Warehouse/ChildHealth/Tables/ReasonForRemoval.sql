﻿CREATE TABLE [ChildHealth].[ReasonForRemoval] (
    [ReasonForRemovalCode] VARCHAR (6)   NOT NULL,
    [ReasonForRemoval]     VARCHAR (100) NULL,
    CONSTRAINT [PK_ReasonForRemoval] PRIMARY KEY CLUSTERED ([ReasonForRemovalCode] ASC)
);


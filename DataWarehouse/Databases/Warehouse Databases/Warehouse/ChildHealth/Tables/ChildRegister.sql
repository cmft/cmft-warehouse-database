﻿CREATE TABLE [ChildHealth].[ChildRegister] (
    [RegisterCode]          VARCHAR (6)  NOT NULL,
    [EntitySourceUniqueID]  INT          NOT NULL,
    [OnRegisterDate]        DATE         NOT NULL,
    [CurrentRegisterDate]   DATE         NULL,
    [OffRegisterDate]       DATE         NULL,
    [ReasonForRemovalCode]  VARCHAR (6)  NULL,
    [ClientNumber]          VARCHAR (10) NULL,
    [RegisterStaffCode]     VARCHAR (50) NULL,
    [RegisterStaffTypeCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_ChildRegister] PRIMARY KEY CLUSTERED ([RegisterCode] ASC, [EntitySourceUniqueID] ASC, [OnRegisterDate] ASC)
);


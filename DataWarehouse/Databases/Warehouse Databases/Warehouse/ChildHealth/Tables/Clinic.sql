﻿CREATE TABLE [ChildHealth].[Clinic] (
    [ClinicCode] VARCHAR (13)  NOT NULL,
    [Clinic]     VARCHAR (255) NULL,
    CONSTRAINT [PK_Clinic] PRIMARY KEY CLUSTERED ([ClinicCode] ASC)
);


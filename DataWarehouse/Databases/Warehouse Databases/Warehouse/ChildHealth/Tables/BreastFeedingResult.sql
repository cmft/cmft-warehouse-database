﻿CREATE TABLE [ChildHealth].[BreastFeedingResult] (
    [BreastFeedingResultCode] VARCHAR (50) NOT NULL,
    [BreastFeedingResult]     VARCHAR (50) NULL,
    CONSTRAINT [PK_BreastFeedingResult] PRIMARY KEY CLUSTERED ([BreastFeedingResultCode] ASC)
);


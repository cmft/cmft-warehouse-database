﻿CREATE TABLE [ChildHealth].[EventConsent] (
    [EntitySourceUniqueID]         INT          NOT NULL,
    [AntigenCode]                  VARCHAR (50) NOT NULL,
    [ConsentCode]                  VARCHAR (50) NULL,
    [ConsentDate]                  DATE         NULL,
    [ReasonForNegativeConsentCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_Intent] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [AntigenCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[ChildProtectionCauseForConcern] (
    [EntitySourceUniqueID] VARCHAR (50) NOT NULL,
    [IdentifiedDate]       DATE         NOT NULL,
    [SequenceNo]           VARCHAR (50) NOT NULL,
    [CauseForConcernCode]  VARCHAR (50) NULL,
    [EndDate]              DATE         NULL,
    CONSTRAINT [PK_ChildProtectionCauseForConcern] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [IdentifiedDate] ASC, [SequenceNo] ASC)
);


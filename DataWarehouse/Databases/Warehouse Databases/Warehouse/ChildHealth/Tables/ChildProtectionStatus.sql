﻿CREATE TABLE [ChildHealth].[ChildProtectionStatus] (
    [EntitySourceUniqueID] VARCHAR (50) NOT NULL,
    [PlacedOnPlanDate]     DATE         NOT NULL,
    [RegistrationDate]     DATE         NOT NULL,
    [SequenceNo]           VARCHAR (50) NOT NULL,
    [EndDate]              DATE         NULL,
    [StatusCode]           VARCHAR (50) NULL,
    CONSTRAINT [PK_ChildProtectionStatus] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [PlacedOnPlanDate] ASC, [RegistrationDate] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [ChildHealth].[ChildConsent] (
    [EntitySourceUniqueID]        INT          NOT NULL,
    [SystemOrDepartmentCode]      VARCHAR (50) NOT NULL,
    [ConsentDate]                 DATE         NOT NULL,
    [ConsentSequenceNo]           VARCHAR (50) NOT NULL,
    [ConsentCode]                 VARCHAR (50) NULL,
    [PersonGivingConsentCode]     VARCHAR (50) NULL,
    [PersonGivingConsentTypeCode] VARCHAR (50) NULL,
    [DissentionCode]              VARCHAR (50) NULL,
    CONSTRAINT [PK_ChildConsent] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [SystemOrDepartmentCode] ASC, [ConsentDate] ASC, [ConsentSequenceNo] ASC)
);


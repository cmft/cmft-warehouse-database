﻿CREATE TABLE [ChildHealth].[FlagReasonForRemoval] (
    [FlagReasonForRemovalCode] VARCHAR (50) NOT NULL,
    [FlagReasonForRemoval]     VARCHAR (50) NULL,
    CONSTRAINT [PK_FlagReasonForRemoval] PRIMARY KEY CLUSTERED ([FlagReasonForRemovalCode] ASC)
);


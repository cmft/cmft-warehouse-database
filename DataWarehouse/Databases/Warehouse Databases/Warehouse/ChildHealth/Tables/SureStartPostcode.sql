﻿CREATE TABLE [ChildHealth].[SureStartPostcode] (
    [Postcode]             VARCHAR (8)  NOT NULL,
    [LowerSuperOutputArea] VARCHAR (20) NULL,
    [Ward2004]             VARCHAR (50) NULL,
    [ChildrensCentre]      VARCHAR (50) NULL,
    CONSTRAINT [PK_SurestartPostcode] PRIMARY KEY CLUSTERED ([Postcode] ASC)
);


﻿CREATE TABLE [ChildHealth].[RegisterStaffType] (
    [RegisterStaffTypeCode] VARCHAR (50) NOT NULL,
    [RegisterStaffType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_RegisterStaffType] PRIMARY KEY CLUSTERED ([RegisterStaffTypeCode] ASC)
);


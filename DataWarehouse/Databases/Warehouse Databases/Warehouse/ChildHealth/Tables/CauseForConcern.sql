﻿CREATE TABLE [ChildHealth].[CauseForConcern] (
    [CauseForConcernCode] VARCHAR (50)  NOT NULL,
    [CauseForConcern]     VARCHAR (250) NULL,
    CONSTRAINT [PK_CauseForConcern] PRIMARY KEY CLUSTERED ([CauseForConcernCode] ASC)
);


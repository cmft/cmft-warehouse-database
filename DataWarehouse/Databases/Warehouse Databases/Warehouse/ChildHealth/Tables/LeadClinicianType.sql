﻿CREATE TABLE [ChildHealth].[LeadClinicianType] (
    [LeadClinicianTypeCode] VARCHAR (50)  NOT NULL,
    [LeadClinicianType]     VARCHAR (250) NULL,
    CONSTRAINT [PK_LeadClinicianType] PRIMARY KEY CLUSTERED ([LeadClinicianTypeCode] ASC)
);


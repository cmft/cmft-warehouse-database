﻿CREATE TABLE [ChildHealth].[Outcome] (
    [OutcomeCode] VARCHAR (6)  NOT NULL,
    [Outcome]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Outcome] PRIMARY KEY CLUSTERED ([OutcomeCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[ModeOfEntry] (
    [ModeOfEntryCode] VARCHAR (1)  NOT NULL,
    [ModeOfEntry]     VARCHAR (32) NULL,
    CONSTRAINT [PK_ModeOfEntry] PRIMARY KEY CLUSTERED ([ModeOfEntryCode] ASC)
);


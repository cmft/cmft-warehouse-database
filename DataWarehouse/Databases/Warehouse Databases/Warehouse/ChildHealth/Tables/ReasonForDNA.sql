﻿CREATE TABLE [ChildHealth].[ReasonForDNA] (
    [ReasonForDNACode] VARCHAR (6)  NOT NULL,
    [ReasonForDNA]     VARCHAR (35) NULL,
    CONSTRAINT [PK_ReasonForDNA] PRIMARY KEY CLUSTERED ([ReasonForDNACode] ASC)
);


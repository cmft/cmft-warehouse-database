﻿CREATE TABLE [ChildHealth].[MaracReasonForReferral] (
    [MaracReasonForReferralCode] VARCHAR (50)  NOT NULL,
    [MaracReasonForReferral]     VARCHAR (250) NULL,
    CONSTRAINT [PK_MaracReasonForReferral] PRIMARY KEY CLUSTERED ([MaracReasonForReferralCode] ASC)
);


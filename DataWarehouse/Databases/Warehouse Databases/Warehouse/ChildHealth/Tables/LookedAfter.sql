﻿CREATE TABLE [ChildHealth].[LookedAfter] (
    [EntitySourceUniqueID]         INT           NOT NULL,
    [LookedAfterDate]              DATE          NOT NULL,
    [LookedAfterRemovalReasonCode] VARCHAR (100) NULL,
    [ConfirmedDate]                DATE          NULL,
    [DeliveryDate]                 DATE          NULL,
    [HealthNotifiedDate]           DATE          NULL,
    [RemovedDate]                  DATE          NULL,
    [ImmsUptoDateDecisionDate]     DATE          NULL,
    [ImmsUptoDateClinician]        VARCHAR (250) NULL,
    [ImmsUptoDateFlag]             VARCHAR (10)  NULL,
    [LookedAfterStatusCode]        VARCHAR (100) NULL,
    [LastDentalExamDate]           DATE          NULL,
    [OriginatingPCTCode]           VARCHAR (10)  NULL,
    [PlacementTypeCode]            VARCHAR (10)  NULL,
    [PayingPCTCode]                VARCHAR (50)  NULL,
    [PregnancyOutcomeCode]         VARCHAR (10)  NULL,
    [ResponsibleForPregnancyCode]  VARCHAR (5)   NULL,
    [ResidingPCTCode]              VARCHAR (10)  NULL,
    [SocialWorker]                 VARCHAR (100) NULL,
    [SocialServicesDepartmentCode] VARCHAR (10)  NULL,
    [Supervision]                  VARCHAR (50)  NULL,
    [SupervisionStartDate]         DATE          NULL,
    [SupervisionEndDate]           DATE          NULL,
    [UnaccompaniedAsylumSeeker]    VARCHAR (5)   NULL,
    [LeadClinicianCode]            VARCHAR (10)  NULL,
    [LeadClinicianTypeCode]        VARCHAR (10)  NULL,
    CONSTRAINT [PK_LookedAfter] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [LookedAfterDate] ASC)
);




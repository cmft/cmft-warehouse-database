﻿CREATE TABLE [ChildHealth].[Staff] (
    [ChildHealthEncounterSourceUniqueID] INT          NOT NULL,
    [SequenceNo]                         INT          NOT NULL,
    [TEMSYS]                             INT          NULL,
    [PersonTypeCode]                     VARCHAR (12) NULL,
    [StaffCode]                          INT          NULL,
    [Staff]                              VARCHAR (12) NULL,
    [StartDate]                          DATETIME     NULL,
    [EndDate]                            DATETIME     NULL,
    [PersonOrder]                        INT          NULL,
    [StaffTypeCode]                      VARCHAR (12) NULL,
    CONSTRAINT [PK_Staff_1] PRIMARY KEY CLUSTERED ([ChildHealthEncounterSourceUniqueID] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [ChildHealth].[BirthState] (
    [BirthStateCode] VARCHAR (1)  NOT NULL,
    [BirthState]     VARCHAR (30) NULL,
    CONSTRAINT [PK_BirthState] PRIMARY KEY CLUSTERED ([BirthStateCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[BloodspotTest] (
    [BloodspotTestCode] VARCHAR (6)  NOT NULL,
    [BloodspotTest]     VARCHAR (30) NULL,
    CONSTRAINT [PK_BloodspotTest] PRIMARY KEY CLUSTERED ([BloodspotTestCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[ClientSummary] (
    [EntitySourceUniqueID]             INT          NOT NULL,
    [ScheduleCode]                     VARCHAR (6)  NULL,
    [ClinicCode]                       VARCHAR (13) NULL,
    [OutstandingAppointmentDate]       DATE         NULL,
    [OutstandingAppointmentSequenceNo] INT          NULL,
    [OnMMRList]                        VARCHAR (1)  NULL,
    [ImmunisationsUpToDate]            VARCHAR (50) NULL,
    CONSTRAINT [PK_ClientSummary] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC)
);


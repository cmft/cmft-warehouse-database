﻿CREATE TABLE [ChildHealth].[Antigen] (
    [AntigenCode] VARCHAR (50)  NOT NULL,
    [Antigen]     VARCHAR (128) NULL,
    CONSTRAINT [PK_ScheduledAntiGens] PRIMARY KEY CLUSTERED ([AntigenCode] ASC)
);


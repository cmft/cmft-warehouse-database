﻿CREATE TABLE [ChildHealth].[BreastFeedingStage] (
    [BreastFeedingStageNo] INT          NOT NULL,
    [BreastFeedingStage]   VARCHAR (50) NULL,
    CONSTRAINT [PK_BreastFeedingStage] PRIMARY KEY CLUSTERED ([BreastFeedingStageNo] ASC)
);


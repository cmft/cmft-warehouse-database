﻿CREATE TABLE [ChildHealth].[Register] (
    [RegisterCode] VARCHAR (6)  NOT NULL,
    [Register]     VARCHAR (30) NULL,
    CONSTRAINT [PK_Register] PRIMARY KEY CLUSTERED ([RegisterCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[ReasonNoVaccineGiven] (
    [ReasonNoVaccineGivenCode] VARCHAR (6)  NOT NULL,
    [ReasonNoVaccineGiven]     VARCHAR (64) NULL,
    CONSTRAINT [PK_ReasonNoVaccineGiven] PRIMARY KEY CLUSTERED ([ReasonNoVaccineGivenCode] ASC)
);


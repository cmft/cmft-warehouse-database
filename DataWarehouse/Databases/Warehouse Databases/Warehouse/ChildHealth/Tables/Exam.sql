﻿CREATE TABLE [ChildHealth].[Exam] (
    [ExamCode] VARCHAR (6)  NOT NULL,
    [Exam]     VARCHAR (30) NULL,
    CONSTRAINT [PK_Exam] PRIMARY KEY CLUSTERED ([ExamCode] ASC)
);


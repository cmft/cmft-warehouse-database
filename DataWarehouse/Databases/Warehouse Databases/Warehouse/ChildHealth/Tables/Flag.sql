﻿CREATE TABLE [ChildHealth].[Flag] (
    [FlagCode] VARCHAR (50) NOT NULL,
    [Flag]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Flag] PRIMARY KEY CLUSTERED ([FlagCode] ASC)
);


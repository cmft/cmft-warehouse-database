﻿CREATE TABLE [ChildHealth].[ChildProtectionDomesticAbuse] (
    [EntitySourceUniqueID]       VARCHAR (50)  NOT NULL,
    [IncidentDate]               DATE          NOT NULL,
    [SequenceNo]                 VARCHAR (50)  NOT NULL,
    [CAFDeclined]                VARCHAR (2)   NULL,
    [CAFRecommended]             VARCHAR (2)   NULL,
    [CrimeNumber]                VARCHAR (50)  NULL,
    [LevelCode]                  VARCHAR (50)  NULL,
    [OutcomeCode]                VARCHAR (10)  NULL,
    [ReasonDeclined]             VARCHAR (250) NULL,
    [HealthProfessionalCode]     VARCHAR (50)  NULL,
    [HealthProfessionalTypeCode] VARCHAR (50)  NULL,
    CONSTRAINT [PK_ChildProtectionDomesticAbuse] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [IncidentDate] ASC, [SequenceNo] ASC)
);


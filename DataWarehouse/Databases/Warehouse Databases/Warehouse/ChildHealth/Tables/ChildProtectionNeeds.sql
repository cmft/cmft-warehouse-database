﻿CREATE TABLE [ChildHealth].[ChildProtectionNeeds] (
    [EntitySourceUniqueID] VARCHAR (50) NOT NULL,
    [IdentifiedDate]       DATE         NOT NULL,
    [SequenceNo]           VARCHAR (50) NOT NULL,
    [EndDate]              DATE         NULL,
    [NeedCode]             VARCHAR (50) NULL,
    CONSTRAINT [PK_ChildProtectionNeeds] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [IdentifiedDate] ASC, [SequenceNo] ASC)
);


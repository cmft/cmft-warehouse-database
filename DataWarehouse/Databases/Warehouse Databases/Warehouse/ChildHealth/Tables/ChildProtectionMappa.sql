﻿CREATE TABLE [ChildHealth].[ChildProtectionMappa] (
    [EntitySourceUniqueID] VARCHAR (50) NOT NULL,
    [IncidentDate]         DATE         NOT NULL,
    [SequenceNo]           VARCHAR (50) NOT NULL,
    [EndDate]              DATE         NULL,
    [MappaCategoryCode]    VARCHAR (50) NULL,
    CONSTRAINT [PK_ChildProtectionMappa] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [IncidentDate] ASC, [SequenceNo] ASC)
);


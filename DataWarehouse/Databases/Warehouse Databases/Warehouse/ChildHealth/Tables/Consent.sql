﻿CREATE TABLE [ChildHealth].[Consent] (
    [ConsentCode] VARCHAR (50) NOT NULL,
    [Consent]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Consent] PRIMARY KEY CLUSTERED ([ConsentCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[GP] (
    [GPCode]            INT          NOT NULL,
    [Initials]          VARCHAR (3)  NULL,
    [Surname]           VARCHAR (20) NULL,
    [Address1]          VARCHAR (50) NULL,
    [Address2]          VARCHAR (50) NULL,
    [Address3]          VARCHAR (50) NULL,
    [Address4]          VARCHAR (50) NULL,
    [Postcode]          VARCHAR (8)  NULL,
    [TelephoneNo]       VARCHAR (20) NULL,
    [PracticeCode]      VARCHAR (6)  NULL,
    [FPCCode]           VARCHAR (8)  NULL,
    [FPCContractedCode] VARCHAR (6)  NULL,
    [PooledGP]          VARCHAR (1)  NULL,
    [Active]            VARCHAR (3)  NULL,
    [LocalID]           VARCHAR (50) NULL,
    [NationalCode]      VARCHAR (50) NULL,
    [PASGPCode]         VARCHAR (50) NULL,
    CONSTRAINT [PK_GP] PRIMARY KEY CLUSTERED ([GPCode] ASC)
);


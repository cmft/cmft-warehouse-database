﻿CREATE TABLE [ChildHealth].[ReasonForRepeat] (
    [ReasonForRepeatCode] VARCHAR (60) NOT NULL,
    [ReasonForRepeat]     VARCHAR (60) NULL,
    CONSTRAINT [PK_ReasonForRepeat] PRIMARY KEY CLUSTERED ([ReasonForRepeatCode] ASC)
);


﻿CREATE TABLE [ChildHealth].[FollowUpByWhom] (
    [FollowUpByWhomCode] VARCHAR (50)  NOT NULL,
    [FollowUpByWhom]     VARCHAR (250) NULL,
    CONSTRAINT [PK_FollowUpByWhom] PRIMARY KEY CLUSTERED ([FollowUpByWhomCode] ASC)
);


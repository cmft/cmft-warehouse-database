﻿CREATE TABLE [ChildHealth].[ExamHistoryTestResult] (
    [EntitySourceUniqueID] INT           NOT NULL,
    [ExamDate]             DATE          NOT NULL,
    [ExamCode]             VARCHAR (6)   NOT NULL,
    [TestCode]             VARCHAR (6)   NOT NULL,
    [Result]               VARCHAR (255) NULL,
    [ReferredToCode]       VARCHAR (6)   NULL,
    [ClinicCode]           VARCHAR (50)  NULL,
    CONSTRAINT [PK_ExamHistoryTestResult] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [ExamDate] ASC, [ExamCode] ASC, [TestCode] ASC)
);


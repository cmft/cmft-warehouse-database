﻿CREATE TABLE [ChildHealth].[LeadClinician] (
    [LeadClinicianCode] VARCHAR (50)  NOT NULL,
    [LeadClinician]     VARCHAR (250) NULL,
    CONSTRAINT [PK_LeadClinician] PRIMARY KEY CLUSTERED ([LeadClinicianCode] ASC)
);


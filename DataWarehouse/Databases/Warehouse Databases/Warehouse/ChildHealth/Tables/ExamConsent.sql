﻿CREATE TABLE [ChildHealth].[ExamConsent] (
    [EntitySourceUniqueID]         INT          NOT NULL,
    [ExamCode]                     VARCHAR (50) NOT NULL,
    [ExamTypeCode]                 VARCHAR (50) NULL,
    [ConsentCode]                  VARCHAR (50) NULL,
    [ConsentDate]                  DATE         NULL,
    [ReasonForNegativeConsentCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_ExamConsent] PRIMARY KEY CLUSTERED ([EntitySourceUniqueID] ASC, [ExamCode] ASC)
);


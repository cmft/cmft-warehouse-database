﻿CREATE procedure Utility.AlterTableIndex
	 @AlterOption varchar(20)
	,@table varchar(255)
as

declare @Indexes table
(
	 IndexID int identity(1,1) primary key clustered
	,TableName nvarchar(255)
	,IndexName nvarchar(255)
)

INSERT INTO @Indexes
(
	 TableName
	,IndexName
)
select
	 tableName = schemas.name + '.' + objects.name
	,indexName = indexes.name
from
	sys.indexes

inner join sys.objects
on	indexes.object_id = objects.object_id

inner join sys.schemas
on	schemas.schema_id = objects.schema_id

where
	indexes.type_desc = 'NONCLUSTERED'
and	schemas.name + '.' + objects.name = @table


declare @rows int = @@ROWCOUNT

declare @i int = 1
declare @sql varchar(max)

WHILE @i <= @rows
BEGIN

	select
		@sql =
			'ALTER INDEX ' + IndexName + N' ON ' + TableName + ' ' + @AlterOption + ';'
	from
		@Indexes
	where
		IndexID = @i

    EXEC sp_sqlexec @sql

	print @sql

    set @i = @i + 1

END

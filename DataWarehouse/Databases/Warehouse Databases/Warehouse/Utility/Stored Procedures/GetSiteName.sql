﻿create procedure [Utility].[GetSiteName] as

select
	SiteName = TextValue
from
	dbo.Parameter
where
	Parameter = 'SITENAME'

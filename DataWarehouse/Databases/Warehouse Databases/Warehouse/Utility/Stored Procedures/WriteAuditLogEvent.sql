﻿CREATE   PROCEDURE [Utility].[WriteAuditLogEvent] 
	 @ProcessCode varchar(255)
	,@Event varchar(255) = null
	,@StartTime datetime = null
as

INSERT INTO Utility.AuditLog 
	(
	 EventTime
	,UserId
	,ProcessCode
	,Event
	,StartTime
	)
select
	
	 getdate()
	,Suser_SName()
	,@ProcessCode
	,coalesce(@Event , 'Unknown Event - Debug Stats in Process')
	,@StartTime
	


--insert any unknown process
insert into Utility.AuditProcess
(
	 ProcessCode
	,Process
	,ParentProcessCode
	,ProcessSourceCode
)
select
	 ProcessCode = @ProcessCode
	,Process = @ProcessCode
	,ParentProcessCode = 'OTH'
	,ProcessSourceCode = 'OTH'
where
	not exists
	(
	select
		1
	from
		Utility.AuditProcess
	where
		ProcessCode = @ProcessCode
	)

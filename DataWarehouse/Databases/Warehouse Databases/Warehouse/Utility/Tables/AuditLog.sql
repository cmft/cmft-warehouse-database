﻿CREATE TABLE [Utility].[AuditLog] (
    [AuditLogRecno] INT           IDENTITY (1, 1) NOT NULL,
    [EventTime]     DATETIME      NOT NULL,
    [UserId]        VARCHAR (30)  NOT NULL,
    [ProcessCode]   VARCHAR (255) NOT NULL,
    [Event]         VARCHAR (255) NOT NULL,
    [StartTime]     DATETIME      NULL,
    [ElapsedMinute] INT           NULL,
    [Inserted]      INT           NULL,
    [Deleted]       INT           NULL,
    [Updated]       INT           NULL,
    CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED ([AuditLogRecno] ASC)
);




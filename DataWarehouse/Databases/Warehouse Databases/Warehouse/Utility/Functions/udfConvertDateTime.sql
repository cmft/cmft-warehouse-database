﻿

CREATE FUNCTION [Utility].[udfConvertDateTime]
(
	-- Add the parameters for the function here
	 @sourceDate varchar(100)
	,@sourceTime varchar(100)
)
RETURNS datetime
AS
	/******************************************************************************
	**  Name: udfConvertDateTime
	**  Purpose: Returns a datetime data type from an input string date and time
	**           The time element must be in a 24 hour/minute format and exclude the :
	**           separating the hour and minute
	**
	**	Example : 02/09/2010 1013
	**
	**	NOTE - Please maintain this function via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	**  Created:  C Butterworth
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------
	** 08/06/2011     MH        Was not handling times < 10:00 hours   
	** 10/04/2012	Paul Egan	Was not handling corrupt times in CRIS e.g. '9.3' in 
	**				EXAM.ApptPreviousTime. Added ISDATE function as extra CASE 
	**				statement after time padding and insertion of ':'. NB In order to flag
	**				the corrupt date or time, the plan is to put in a SSIS (or stored 
	**				proc so can run manually?) comparison on the import table and the 
	**				final production table.
	******************************************************************************/

BEGIN
	-- Declare the return variable here
	DECLARE @Time datetime
	DECLARE @SourceTimePadded varchar(4)
	
	-- Handle the case if the time is 3 characters, stripping any : in the string
	SET @SourceTimePadded = RIGHT('0000' + REPLACE(@sourceTime,':',''), 4)
	
	-- Add the T-SQL statements to compute the return value here
	select @Time = 
		CASE
			WHEN ISDATE(@sourceDate) = 0 THEN null
			ELSE
				CASE @sourceDate
					WHEN '' THEN null
					ELSE
						CASE @sourceTime
							WHEN '' THEN convert(datetime , @sourceDate + ' 00:00')
							ELSE 
								CASE WHEN ISDATE(left(@SourceTimePadded,2) + ':' + substring(@SourceTimePadded , 3 , 2)) =1
									 THEN convert(datetime , @sourceDate + ' ' + left(@SourceTimePadded,2) + ':' + substring(@SourceTimePadded , 3 , 2))
								ELSE CONVERT(datetime , @sourceDate + ' 00:00')
								END
						END
				END
		END
	-- Return the result of the function
	RETURN @Time

END



﻿
CREATE FUNCTION [Utility].[fn_GetPatientDetails]
(
	 @SourcePatientNo	varchar(20)
)

RETURNS TABLE AS
RETURN
(	
	/******************************************************************************
	**  Name: Utility.fn_GetPatientDetails
	**  Purpose: Returns various patient related columns
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------  
	** 01.11.12      MH       Created  
	******************************************************************************/
	
	SELECT
		 Surname			= Patient.Surname
		,Forenames			= Patient.Forenames
		,PCTCode			=
			RTRIM(
				LEFT(
					COALESCE(
						Practice.ParentOrganisationCode,
						Postcode.PCTCode
					) + '000'
				, 3
				)
			)
		,DistrictNo

	FROM
		PAS.Patient

		LEFT JOIN PAS.Gp AS RegisteredPractice 
		ON	Patient.RegisteredGpCode = RegisteredPractice.GpCode

		LEFT JOIN [$(Organisation)].dbo.Practice Practice
		ON	Practice.OrganisationCode = RegisteredPractice.PracticeCode

		LEFT JOIN [$(Organisation)].dbo.Postcode Postcode
		ON	Postcode.Postcode = Patient.Postcode

	WHERE
		Patient.SourcePatientNo = @SourcePatientNo
)


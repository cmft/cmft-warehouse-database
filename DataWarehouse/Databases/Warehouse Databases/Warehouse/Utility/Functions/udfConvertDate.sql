﻿

CREATE FUNCTION [Utility].[udfConvertDate]
(
	-- Add the parameters for the function here
	@sourceDate  varchar(100)
)
RETURNS datetime
AS

	/******************************************************************************
	**  Name: udfConvertDate
	**  Purpose: Returns a datetime data type from an input string date 
	**
	**	Example : 02/09/2010 
	**
	**	NOTE - Please maintain this function via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	**  Created:  C Butterworth
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------
	******************************************************************************/

BEGIN
	-- Declare the return variable here
	DECLARE @Date datetime

	-- Add the T-SQL statements to compute the return value here
	select @Date = 
		case
		when ISDATE(@sourceDate) = 0 then null
		else
			case @sourceDate
			when '' then null
			else convert(datetime , rtrim(@sourceDate))
			end
		end
	-- Return the result of the function
	RETURN @Date

END



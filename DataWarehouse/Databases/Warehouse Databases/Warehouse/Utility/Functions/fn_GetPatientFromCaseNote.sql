﻿
CREATE FUNCTION [Utility].[fn_GetPatientFromCaseNote]
(
	 @CaseNoteNumber	varchar(20)
)

RETURNS @PTable TABLE 
(
	 SourcePatientNo VARCHAR(20)
	,CaseNoteNumber VARCHAR(20)
)

AS

BEGIN

	/******************************************************************************
	**  Name: Utility.fn_GetPatientFromCaseNote
	**  Purpose: Derive the PatientNo from a Casenote 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------  
	** 06.11.12      MH       Created  
	** 08.11.12      MH       Re-factored for missing 0 for Mxx/ case note number types
	******************************************************************************/

	DECLARE @CN1 varchar(20)
	DECLARE @CN2 varchar(20)

	-- If case note starts with an M and there is no / in the 4th character insert it
	SET @CN1 = CASE
					WHEN LEFT(@CaseNoteNumber, 1) = 'M' AND SUBSTRING(@CaseNoteNumber,4,1) <> '/'
						THEN	STUFF(@CaseNoteNumber, 4, 0,'/')		-- Stick a / in the case note for these types
					ELSE	@CaseNoteNumber
			  END

	-- If the case note starts with an M with a / in the 4 character then insert a 0 after the /
	-- This is to find for example M11/12345 which should be M11/012345						   
	SET @CN2 = CASE
					WHEN LEFT(@CaseNoteNumber, 1) = 'M' AND SUBSTRING(@CaseNoteNumber,4,1) = '/'
						THEN	STUFF(@CaseNoteNumber, 5, 0,'0')		-- Stick a / in the case note for these types
					ELSE	@CaseNoteNumber
			   END	

	-- Run the query on these 2 search criteria.
	-- They are ordered so if more than 1 row is returned the first criteria will take precedence
	INSERT INTO @PTable
	SELECT
		 SourcePatientNo
		,CasenoteNumber
	FROM
	(
		SELECT	TOP 1
			 SourcePatientNo
			,CasenoteNumber
			,Preference	=	CASE
								WHEN CasenoteNumber = @CN1 THEN 1
								ELSE 2
							END
		FROM
			PAS.Casenote CN
		WHERE
			CasenoteNumber IN (@CN1, @CN2)
		ORDER BY
			Preference
	) T
		
	RETURN

END

﻿


CREATE FUNCTION [Utility].[udfEmptyStringToNull]
(
	-- Add the parameters for the function here
	@sourceString  varchar(max)
)
RETURNS varchar(max)
AS


BEGIN
	-- Declare the return variable here
	DECLARE @String varchar(max)

	-- Add the T-SQL statements to compute the return value here
	select @String = 
		Case 
		when @sourceString = '' then NULL
		else @sourceString
		end
	-- Return the result of the function
	RETURN @String

END




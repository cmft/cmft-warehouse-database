﻿CREATE VIEW CriticalCare.APCEncounterTransfers_view
AS
--INTO CriticalCare.CC.APCEncounterTransfers 
SELECT                e.DistrictNo, e.CasenoteNumber, e.NHSNumber, e.PatientForename, 
                      e.PatientSurname, e.SexCode, e.DateOfBirth, e.PatientAddress1, e.PatientAddress2, 
                      e.PatientAddress3, e.PatientAddress4, e.PCTCode, e.RegisteredGpPracticeCode, 
                      e.RegisteredGpCode, e.AdmissionTime, e.DischargeTime, e.IntendedPrimaryOperationCode, 
                      e.Operation, e.TransferFrom, e.PrimaryOperationCode, e.PrimaryOperationDate, 
                      e.EndWardTypeCode, e.EndSiteCode, e.StartWardTypeCode, e.StartSiteCode, 
                      e.DischargeDate, e.AdmissionDate, 
                      e.DischargeDestinationCode, 
                      ddb.[Description] [Discharge Destination], 
                      e.DischargeMethodCode,
                      dmb.[Description] [Discharge Method] 
FROM        APC.Encounter e LEFT OUTER JOIN
                      PAS.DischargeMethodBase dmb ON e.DischargeMethodCode = dmb.MODID LEFT OUTER JOIN
                      PAS.DischargeDestinationBase ddb ON e.DischargeDestinationCode = ddb.DODID
WHERE    ( (e.StartWardTypeCode IN ('ICU', 'HDU', 'CSU')) OR
                      (e.EndWardTypeCode IN ('ICU', 'HDU', 'CSU')) )
   

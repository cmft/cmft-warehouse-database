﻿CREATE TABLE [DM01].[ExamNationalExamMap] (
    [ExamCode]         VARCHAR (20)  NOT NULL,
    [InterfaceCode]    VARCHAR (10)  NOT NULL,
    [NationalExamCode] VARCHAR (10)  NULL,
    [Created]          DATETIME2 (0) CONSTRAINT [df_DM01_ExamNationalExamMap_Created] DEFAULT (getdate()) NULL,
    [Updated]          DATETIME2 (0) NULL,
    [ByWhom]           VARCHAR (50)  CONSTRAINT [df_DM01_ExamNationalExamMap_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_ExamNationalExamMap] PRIMARY KEY CLUSTERED ([ExamCode] ASC, [InterfaceCode] ASC)
);




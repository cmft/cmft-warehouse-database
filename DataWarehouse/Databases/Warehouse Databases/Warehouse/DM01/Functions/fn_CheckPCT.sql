﻿
CREATE FUNCTION [DM01].[fn_CheckPCT]
(
	   @PCTCode				varchar(5)
	  ,@SourcePatientNo		varchar(20)
)

RETURNS TABLE AS
RETURN
(	
	/******************************************************************************
	**  Name: DM01.fn_CheckPCT
	**  Purpose: Check the input PCTCode is valid, if not try and derive a PCT Code
	**           from the Patient
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio
	** 
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------  
	** 20.11.12      MH       Created  
	******************************************************************************/
	
	SELECT
		PCTCode	= COALESCE(PCT.[Organisation Code], PDATA.PCTCode)

	FROM 
		Utility.fn_GetPatientDetails(@SourcePatientNo) PDATA
		LEFT JOIN [$(Organisation)].dbo.[Primary Care Organisation]	PCT	
			ON PCT.[Organisation Code] = @PCTCode								-- Check PCT code is valid		
)


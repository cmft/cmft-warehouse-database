﻿
CREATE FUNCTION [DM01].[fn_FormatToExportTable]
(
	   @Hospital							varchar(20)
	  ,@DiagnosticReportLineCode			char(5) --varchar(5)
	  ,@PCTCode								varchar(5)
	  ,@WaitWeeks							int
	  ,@ActivityWaitingList					int
	  ,@ActivityPlanned						int
	  ,@ActivityUnscheduled					int
)

RETURNS TABLE AS
RETURN
(	
	/******************************************************************************
	**  Name: DM01.fn_FormatToExportTable
	**  Purpose: Takes the input parameters and formats a row ready for inserting 
	**           into a table of type DM01.EXPORTDATATABLETYPE
	**
	**	NOTE : Any changes to the columsn here must also be refelected in the 
	**         schema of the DM01.EXPORTDATATABLETYPE table type and vice versa
	**
	**         The definition of the table type is contained in the DM01.BuildExportTable 
	**         stored procedure
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ----------------------------------------------------  
	** 27.11.12      MH       Created  
	******************************************************************************/
	
	SELECT
		 Hospital					= @Hospital
		,DiagnosticReportLineCode	= @DiagnosticReportLineCode
		,PCTCode	=
					 CASE
						WHEN LEFT(@PCTCode, 1) IN ('6','7','S') THEN 'NONC'				-- Welsh or Scottish PCT's
						WHEN LEFT(@PCTCode, 3) = '5CC' THEN 'TAP'						-- Blackburn with Darwen PCT
						ELSE COALESCE(LEFT(@PCTCode,3), '5NT')
					  END
		,[0 < 1]	= SUM(CASE WHEN @WaitWeeks >= 0 AND @WaitWeeks < 1 THEN 1 ELSE 0 END)
		,[1 < 2]	= SUM(CASE WHEN @WaitWeeks >= 1 AND  @WaitWeeks < 2 THEN 1 ELSE 0 END)
		,[2 < 3]	= SUM(CASE WHEN @WaitWeeks >= 2 AND  @WaitWeeks < 3 THEN 1 ELSE 0 END)
		,[3 < 4]	= SUM(CASE WHEN @WaitWeeks >= 3 AND  @WaitWeeks < 4 THEN 1 ELSE 0 END)
		,[4 < 5]	= SUM(CASE WHEN @WaitWeeks >= 4 AND  @WaitWeeks < 5 THEN 1 ELSE 0 END)
		,[5 < 6]	= SUM(CASE WHEN @WaitWeeks >= 5 AND  @WaitWeeks < 6 THEN 1 ELSE 0 END)
		,[6 < 7]	= SUM(CASE WHEN @WaitWeeks >= 6 AND  @WaitWeeks < 7 THEN 1 ELSE 0 END)
		,[7 < 8]	= SUM(CASE WHEN @WaitWeeks >= 7 AND  @WaitWeeks < 8 THEN 1 ELSE 0 END)
		,[8 < 9]	= SUM(CASE WHEN @WaitWeeks >= 8 AND  @WaitWeeks < 9 THEN 1 ELSE 0 END)
		,[9 < 10]	= SUM(CASE WHEN @WaitWeeks >= 9 AND  @WaitWeeks < 10 THEN 1 ELSE 0 END)
		,[10 < 11]	= SUM(CASE WHEN @WaitWeeks >= 10 AND  @WaitWeeks < 11 THEN 1 ELSE 0 END)
		,[11 < 12]	= SUM(CASE WHEN @WaitWeeks >= 11 AND  @WaitWeeks < 12 THEN 1 ELSE 0 END)
		,[12 < 13]	= SUM(CASE WHEN @WaitWeeks >= 12 AND  @WaitWeeks < 13 THEN 1 ELSE 0 END)
		,[>= 13]    = SUM(CASE WHEN @WaitWeeks >= 13 THEN 1 ELSE 0 END)	
		,ActWaitingList	= COALESCE(@ActivityWaitingList, 0)
		,ActPlanned		= COALESCE(@ActivityPlanned, 0)
		,ActUnscheduled	= COALESCE(@ActivityUnscheduled, 0)
)


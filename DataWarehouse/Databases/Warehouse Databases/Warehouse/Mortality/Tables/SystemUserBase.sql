﻿CREATE TABLE [Mortality].[SystemUserBase] (
    [SystemUserCode] BIGINT        NOT NULL,
    [SystemUser]     VARCHAR (500) NULL,
    [DomainLogin]    VARCHAR (500) NULL,
    [Email]          VARCHAR (500) NULL,
    [Active]         BIT           NULL
);


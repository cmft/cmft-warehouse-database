﻿CREATE TABLE [Mortality].[FormTypeBase] (
    [FormTypeCode] INT           NOT NULL,
    [FormType]     VARCHAR (MAX) NULL,
    [Category]     VARCHAR (15)  NULL,
    [FromDate]     DATE          NULL,
    [ToDate]       DATE          NULL
);




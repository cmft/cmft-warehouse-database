﻿CREATE TABLE [Mortality].[ReviewStatusArchive] (
    [StatusID]   INT          NOT NULL,
    [StatusCode] INT          NOT NULL,
    [Status]     VARCHAR (50) NULL,
    CONSTRAINT [PK_ReviewStatusArchive] PRIMARY KEY CLUSTERED ([StatusID] ASC)
);


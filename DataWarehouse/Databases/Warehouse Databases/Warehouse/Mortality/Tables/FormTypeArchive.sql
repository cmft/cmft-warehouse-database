﻿CREATE TABLE [Mortality].[FormTypeArchive] (
    [FormTypeID] INT           NOT NULL,
    [FormType]   VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_FormTypeArchive] PRIMARY KEY CLUSTERED ([FormTypeID] ASC)
);


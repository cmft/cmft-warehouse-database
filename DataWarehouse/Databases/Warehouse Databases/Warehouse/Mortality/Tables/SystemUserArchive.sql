﻿CREATE TABLE [Mortality].[SystemUserArchive] (
    [SystemUserCode] INT          IDENTITY (1, 1) NOT NULL,
    [SystemUser]     VARCHAR (50) NULL,
    [DomainLogin]    VARCHAR (50) NULL,
    CONSTRAINT [PK_SystemUserArchive] PRIMARY KEY CLUSTERED ([SystemUserCode] ASC)
);


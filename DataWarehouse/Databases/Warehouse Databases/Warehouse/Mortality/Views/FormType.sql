﻿


CREATE View [Mortality].[FormType] 
as

select
	FormTypeCode
	,FormType
	,Category
	,FromDate
	,ToDate
from 
	Mortality.FormTypeBase

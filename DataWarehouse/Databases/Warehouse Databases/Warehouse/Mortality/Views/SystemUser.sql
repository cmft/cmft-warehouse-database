﻿CREATE View Mortality.SystemUser
as

select
	SystemUserCode 
	,SystemUser
	,DomainLogin
	,Email
	,Active
from 
	Mortality.SystemUserBase

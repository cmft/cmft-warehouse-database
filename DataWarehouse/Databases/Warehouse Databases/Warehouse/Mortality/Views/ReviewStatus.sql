﻿
CREATE View Mortality.ReviewStatus
as

Select 
	ReviewStatusCode
	,ReviewStatus 
from
	Mortality.ReviewStatusBase

﻿create function [DQ].[f_ProcedureswLimitedValue]
(
	@methodology int=null,
	@PrimaryProcedure varchar(10) = null,
	@SecondaryProcedure1 varchar(10) = null,
	@SecondaryProcedure2 varchar(10) = null,
	@SecondaryProcedure3 varchar(10) = null,
	@SecondaryProcedure4 varchar(10) = null,
	@SecondaryProcedure5 varchar(10) = null,
	@SecondaryProcedure6 varchar(10) = null,
	@SecondaryProcedure7 varchar(10) = null,
	@SecondaryProcedure8 varchar(10) = null,
	@SecondaryProcedure9 varchar(10) = null,
	@SecondaryProcedure10 varchar(10) = null,
	@SecondaryProcedure11 varchar(10) = null,
	@PrimaryDiagnosis varchar(10) = null,
	@gender char(1) = null,
	@Hrg varchar(10) = null
)
returns varchar(100)
as
begin
	--default values
	set @methodology = isnull(@methodology, null)
	set @PrimaryProcedure = isnull(@PrimaryProcedure, '')
	set @SecondaryProcedure1 = isnull(@SecondaryProcedure1, '')
	set @SecondaryProcedure2 = isnull(@SecondaryProcedure2, '')
	set @SecondaryProcedure3 = isnull(@SecondaryProcedure3, '')
	set @SecondaryProcedure4 = isnull(@SecondaryProcedure4, '')
	set @SecondaryProcedure5 = isnull(@SecondaryProcedure5, '')
	set @SecondaryProcedure6 = isnull(@SecondaryProcedure6, '')
	set @SecondaryProcedure7 = isnull(@SecondaryProcedure7, '')
	set @SecondaryProcedure8 = isnull(@SecondaryProcedure8, '')
	set @SecondaryProcedure9 = isnull(@SecondaryProcedure9, '')
	set @SecondaryProcedure10 = isnull(@SecondaryProcedure10, '')
	set @SecondaryProcedure11 = isnull(@SecondaryProcedure11, '')
	set @PrimaryDiagnosis = isnull(@PrimaryDiagnosis, '')
	set @gender = isnull(@gender, '')
	set @Hrg = isnull(@Hrg, '')

	declare @ProcedureCategory varchar(100)
	set @ProcedureCategory = ''
	begin
	if (@methodology=1)
	begin
			set @ProcedureCategory = 
           case  when left(@PrimaryProcedure,4) in ('b301','b302','b303','b308',
                                                    'b309','b311','b312','b313',
                                                    'b314','b356')
                      and @Hrg in ('j01','j04','j05','j06',
                                   'j07','j50')
                 then 'aesthetic surgery - breast'
                 when left(@PrimaryProcedure,4) in ('d033','d038','d039','d062',
                                                    'e025','e026','e028','e029',
                                                    'e094')
                      and @Hrg in ('c21','c32','c56')
                 then 'aesthetic surgery - ent'
                 when left(@PrimaryProcedure,4) in ('c131','c132','c133','c134',
                                                    'c181','c182','c184','c186',
                                                    'c188','c189')
                      and @Hrg in ('b17','b18')
                 then 'aesthetic surgery - ophthalmology'
                 when left(@PrimaryProcedure,4) in ('s011','s012','s013','s014',
                                                    's018','s019','s021','s022',
                                                    's028','s029','s031','s032',
                                                    's211','s212','s332','s333')
                      and @Hrg in ('j27','j29','j32')
                 then 'aesthetic surgery - plastics'
                 when left(@PrimaryProcedure,4) in ('h511','h518','h519','h531',
                                                    'h558','h559','h568','h482',
                                                    'h512','h521','h522','h523',
                                                    'h524','h528','h529','h532',
                                                    'h533','h538','h539')
                      and @Hrg in ('f92','f93','f94','f95')
                 then 'anal procedures'
                 when left(@PrimaryProcedure,4) in ('h511','h512','h518','h519','h521','h522','h523','h524','h528','h529','h531','h532','h533','h538','h539')
    then 'Haemorrhoid Surgery'
                 when left(@PrimaryProcedure,4) in ('v333','v335','v336','v371',
                                                    'v372','v373','v374','v378',
                                                    'v379','v381','v383','v384',
                                                    'v388','v389','v221','v241',
                                                    'v251','v253','v231','v261',
                                                    'v391','v391','v392','v393',
                                                    'v394','v395','v398','v399')
                      and @Hrg in ('r03','r04','r07','r09')
                 then 'back pain: injection and fusions'
                 when left(@PrimaryProcedure,4) in ('w371','w378','w379','w381',
                                                    'w388','w389','w391','w398',
                                                    'w399','w431','w438','w439',
                                                    'w441','w448','w449','w451',
                                                    'w458','w459','w461','w468',
                                                    'w469','w471','w478','w479',
                                                    'w481','w488','w489','w521',
                                                    'w528','w529','w531','w538',
                                                    'w539','w541','w548','w549',
                                                    'w581','w588','w589')
                      and @Hrg in ('h01')
                 then 'bilateral hips'
                 when left(@PrimaryProcedure,4) in ('a651','a658','a659')
                      and @Hrg in ('h13')
                 then 'carpal tunnel'
                 when @Hrg in ('b13')
                 then 'cataract surgery'
                 when @Hrg in ('c60')
                 then 'cochlear implants'
                 when left(@PrimaryProcedure,4) in ('q103','q108','q109')
                      and @Hrg in ('m05')
                 then 'dilation & curettage'
                 when left(@PrimaryProcedure,4) in ('t541','t521','t522','t528',
                                                    't529','t548','t549')
                      and @Hrg in ('h13','h16','h17','h14')
                 then 'dupuytrens contracture'
                 when left(@PrimaryProcedure,4) in ('k571','k572','k578','k579')
                 then 'elective cardiac ablation'
                 when @Hrg in ('m13')
                 then 'female genital prolapse/stress incontinence (non-surgical)'
                 when left(@PrimaryProcedure,4) in ('m531','m538','m539','p219',
                                                    'p221','p222','p223','p228',
                                                    'p229','p231','p232','p233',
                                                    'p234','p238','p239','p241',
                                                    'p243','p244','p248','p249',
                                                    'p251')
                      and @Hrg in ('m03')
                 then 'female genital prolapse/stress incontinence (surgical)'
                 when left(@PrimaryProcedure,4) in ('d151','d158','d159','d202',
                                                    'd208','d209','d288','d289')
                 then 'grommets'
                 when @Hrg in ('h05','h06','h07','h71',
                               'h72')
                 then 'hip & knee revision'
                 when left(@PrimaryProcedure,4) in ('q071','q072','q073','q074',
                                                    'q075','q078','q079','q081',
                                                    'q082','q083','q088','q089')
                 then 'hysterectomy for meorrhagia'
                 when @Hrg in ('f71','f72')
                 then 'incisional and ventral hernias'
                 when @Hrg in ('f73','f74')
                 then 'inguinal, umbilical and femoral hernias'
                 when left(@PrimaryProcedure,4) in ('v118','v119','v171','v172',
                                                    'v173','v178','v203','v209',
                                                    'v218','v219','v139','v143',
                                                    'v144','v161','v162','v163',
                                                    'v168','v169','v198','v199',
                                                    'v071','v078','v079','v142',
                                                    'v148')
                 then 'jaw replacement'
                 when left(@PrimaryProcedure,4) in ('w851','w852','w859','w861',
                                                    'w868','w869')
                      and @Hrg in ('h10')
                 then 'knee washouts'
                 when @Hrg in ('h03','h04')
                 then 'knees'
                 when @Hrg in ('j33','j34','j35','j36',
                               'j37')
                 then 'minor skin lesions'
                 when left(@PrimaryProcedure,4) in ('f142','f143','f144','f148',
                                                    'f149')
                      and @Hrg in ('c04')
                 then 'orthodontics'
                 when @Hrg in ('f76','f77')
                 then 'other hernia procedures'
                 when @Hrg in ('h08')
                 then 'other joint prosthetics'
                 when @Hrg in ('h80','h81')
                 then 'primary hip'
                 when left(@PrimaryProcedure,4) in ('a483')
                 then 'spinal cord stimulation'
                 when left(@PrimaryProcedure,4) in ('e201','e208','e209','f341',
                                                    'f342','f344','f345','f346',
                                                    'f348','f349','f361','f368',
                                                    'f369')
                      and @Hrg in ('c58')
                 then 'tonsillectomy'
                 when left(@PrimaryProcedure,4) in ('t705','t708','t709','t718',
                                                    't719','t728','t748','t711',
                                                    't718','t719','t721','t723',
                                                    't724','t728','t729','t743',
                                                    't748','t749','t652','t658',
                                                    't659','t691','t698','t699')
                      and @Hrg in ('h14','h20','h16','h17')
                 then 'trigger finger'
                 when @Hrg in ('q11')
                 then 'varicose veins'
                 when left(@PrimaryProcedure,4) in ('f091','f093','f099')
                      and @Hrg in ('c58')
                 then 'wisdome teeth extraction'
                 else 'other' end
	end
	else if (@methodology=2)
	begin
				set @ProcedureCategory = 
   case 
--         when left(@PrimaryDiagnosis,3) in ('k60')--1
--              and (left(@PrimaryProcedure,3) in ('h50','h54')
--                    or left(@PrimaryProcedure,4) in ('h562','h564'))
--         then 'anal fissure dilation or excision'
         when left(@PrimaryProcedure,4) in ('H501','H502','H503','H504','H508','H509',
                                             'H541','H548','H549','H561','h562','H563','H564','H568','H569')
         then 'anal procedures'
         when left(@PrimaryProcedure,4) in ('h511','h512','h518','h519','h521','h522','h523','h524','h528','h529','h531','h532','h533','h538','h539')
    then 'Haemorrhoid Surgery'
         when coalesce(@PrimaryProcedure,@SecondaryProcedure1,
                           @SecondaryProcedure2,@SecondaryProcedure3,
                           @SecondaryProcedure4,@SecondaryProcedure5,
                           @SecondaryProcedure6,@SecondaryProcedure7,
                           @SecondaryProcedure8,@SecondaryProcedure9,
                           @SecondaryProcedure10,@SecondaryProcedure11) in ('w821','w822','w823','w828',
                                                                            'w829','w831','w832','w833',
                                                                            'w838','w839','w84','w841',
                                                                            'w842','w843','w848','w849',
                                                                            'w861','w868',
                                                                            'w869','w87','w871','w878',
                                                                            'w879','w881','w888','w889')
              and coalesce(@SecondaryProcedure1,
                           @SecondaryProcedure2,@SecondaryProcedure3,
                           @SecondaryProcedure4,@SecondaryProcedure5,
                           @SecondaryProcedure6,@SecondaryProcedure7,
                           @SecondaryProcedure8,@SecondaryProcedure9,
                           @SecondaryProcedure10,@SecondaryProcedure11) not in ('w741','w742','w743','w748',
                                                                                    'w749','w701','w702','w703',
                                                                                    'w708','w709','w283','w783',
                                                                                    'w69','w085')-- in any position
         then 'arthroscopy - excluding knee washouts'
         when left(@PrimaryProcedure,4) in ('w791','w792','w793','w798',
                                               'w799','w591','w592','w593',
                                               'w594','w595','w596','w598',
                                               'w599','w151','w152','w153')
         then 'bunion operations'
         when left(@PrimaryProcedure,4) = 'a651'
         then 'carpal tunnel decompression'
         when left(@PrimaryProcedure,4) in ('n303','n304')
         then 'circumcision'
         when left(@PrimaryProcedure,3) in ('c31','c32','c33','c34',
                                                       'c35')
         then 'correction of squint'
         when left(@PrimaryDiagnosis,3) not in ('o04')--1
              and left(@PrimaryProcedure,4) in ('q103')
         then 'Dilation and Curettage / hysteroscopy'
         when left(@PrimaryProcedure,4) in ('b283')
         then 'excision of breast'
         when left(@PrimaryProcedure,4) in ('t521','t522','t541')
         then 'excision of dupuytren''s contracture'
         when left(@PrimaryProcedure,4) in ('t521','t522','t541')
         then 'excision of dupuytren''s contracture'
         when left(@PrimaryProcedure,3) in ('t59','t60')
         then 'excision of ganglion'
         when (left(@PrimaryDiagnosis,3) in ('h25','h26','h28')
                or @PrimaryDiagnosis in ('q120'))
              and left(@PrimaryProcedure,3) in ('c71','c72','c73','c74',
                                                           'c75','c76','c77')
         then 'extraction of cataract with/without implant'
         when left(@PrimaryProcedure,4) in ('h511')
         then 'haemorrhoidectomy'
         when left(@PrimaryProcedure,3) in ('t19','t20','t23')
              and @PrimaryProcedure not in ('t214')
         then 'inguinal hernia'
         when left(@PrimaryProcedure,4) in ('j183')
              and coalesce(@SecondaryProcedure1,
                           @SecondaryProcedure2,@SecondaryProcedure3,
                           @SecondaryProcedure4,@SecondaryProcedure5,
                           @SecondaryProcedure6,@SecondaryProcedure7,
                           @SecondaryProcedure8,@SecondaryProcedure9,
                           @SecondaryProcedure10,@SecondaryProcedure11) in ('y508','y751','y752','y753',
                                                                                'y754','y755','y758','y759')
         then 'laparascopic cholecystectomy'
         when left(@PrimaryProcedure,4) in ('t42','t43')
               or left(@PrimaryProcedure,3) in ('q17','q35','q36','q38',
                                                           'q39','q49','q50')
         then 'laparoscopy'
         when left(@PrimaryProcedure,3) in ('d15')
              and coalesce(@SecondaryProcedure1,
                           @SecondaryProcedure2,@SecondaryProcedure3,
                           @SecondaryProcedure4,@SecondaryProcedure5,
                           @SecondaryProcedure6,@SecondaryProcedure7,
                           @SecondaryProcedure8,@SecondaryProcedure9,
                           @SecondaryProcedure10,@SecondaryProcedure11) not in ('e081','e201','f291','d191',
                                                                                    'f341','f342','f343','f344',
                                                                                    'f345','f346','f347','f348',
                                                                                    'f349')
         then 'myringotomy with/without grommets'
         when @PrimaryDiagnosis in ('q175')
              and left(@PrimaryProcedure,4) in ('d033')
         then 'operation for bat ears'
         when left(@PrimaryProcedure,3) in ('n08','n09')
              and @PrimaryProcedure not in ('n081','n091')
         then 'orchidopexy'
         when left(@PrimaryProcedure,4) in ('v091','v092')
         then 'reduction of nasal fracture'
         when left(@PrimaryProcedure,4) in ('w283')
         then 'removal of metal ware'
         when left(@PrimaryProcedure,4) in ('e031','e036','e041','e046')
              and coalesce(@PrimaryProcedure,@SecondaryProcedure1,
                           @SecondaryProcedure2,@SecondaryProcedure3,
                           @SecondaryProcedure4,@SecondaryProcedure5,
                           @SecondaryProcedure6,@SecondaryProcedure7,
                           @SecondaryProcedure8,@SecondaryProcedure9,
                           @SecondaryProcedure10,@SecondaryProcedure11) not in ('e021','e022','e023','e024',
                                                                                    'e025','e026','e027','e028',
                                                                                    'e029','e078','e079','e071',
                                                                                    'e07','e081','e201','f291',
                                                                                    'e086','e087','e142','f325',
                                                                                    'f326','f328','f341','f342',
                                                                                    'f343','f344','f345','f346',
                                                                                    'f347','f348','f349')
         then 'submucous resection (septum or turbinate of nose)'
         when @PrimaryDiagnosis in ('o049')
              and left(@PrimaryProcedure,4) in ('q101','q102','q111','q112',
                                                   'q113','q114','q115','q116')
         then 'termination of pregnancy (normal abortion)'
         when left(@PrimaryProcedure,4) in ('f341','f342','f343','f344')
         then 'tonsillectomy'
         when left(@PrimaryProcedure,3) in ('m42')
              and coalesce(@SecondaryProcedure1,
                           @SecondaryProcedure2,@SecondaryProcedure3,
                           @SecondaryProcedure4,@SecondaryProcedure5,
                           @SecondaryProcedure6,@SecondaryProcedure7,
                           @SecondaryProcedure8,@SecondaryProcedure9,
                           @SecondaryProcedure10,@SecondaryProcedure11) not in ('m651','m652','m653','m654',
                                                                                    'm658','m659')
         then 'transurethral resection of bladder tumour'
         when left(@PrimaryProcedure,3) in ('l84','l85','l87','l88')
         then 'varicose vein stripping or ligation'
         when left(@PrimaryProcedure,4) in ('q071','q072','q073','q074','q075','q078','q079') then 'Abdominal excision of uterus'
         when left(@PrimaryProcedure,4) in ('q081','q082','q083','q088','q089') then 'Vaginal excision of uterus'
         when left(@PrimaryProcedure,4) in ('v382','v383','v384','v393','v394','v395','v473','v485','v486','v493') or
         left(@PrimaryProcedure,3) in ('v25','v26','v33','v34') then 'Lumbar spine procedures'
         when left(@PrimaryProcedure,3) in ('A48') then 'Spinal Cord Stimulation'
         when left(@PrimaryProcedure,3) in ('W85') then 'Therapeutic endoscopic operations on cavity of knee join(Knee Washout)'
         when left(@PrimaryProcedure,4) in ('T723') then 'Release of construction of sheet of tendon (Trigger Finger)'
         when left(@PrimaryProcedure,4) in ('t278','t279') then 'Abdominal Hernia'
         when left(@PrimaryProcedure,3) in ('f14','f15') then 'Orthodontics'

         when left(@PrimaryProcedure,4) 
         in ('B312','B301','B302','B303','B308','B309') 
         then 'Breast Augmentation'
         when left(@PrimaryProcedure,4) 
         in ('B311','B313') 
         and @gender in('02','2')
				then 'Breast Reduction or mastopexy (breast lift)'
         when left(@PrimaryProcedure,4) 
         in ('B351','B353','B354','B355','B358','B359') 
         then 'Inverted Nipples'
         when left(@PrimaryProcedure,4) 
         in ('v142','v143','v144','v148','v152','v161','v162','v163','v168','v169','v171','v172','v173','v174','v178','v194','v195','v198','v199','v218','v219','v203','v209') 
         then 'Jaw Replacement'
         when left(@PrimaryProcedure,3) in ('s021','s022') then 'Abdominoplasty /Apronectomy (tummy tuck)'
         when left(@PrimaryDiagnosis,3) in ('L68')
              and left(@PrimaryProcedure,4) in ('S104','S114','S065','S068',
                                                   'S069')
         then 'Abnormally place hair'
         when left(@PrimaryProcedure,4) 
         in ('D033','D038','D039','D062','E025','E026','E028','E029','E094') 
         and @Hrg in ('C21','C32','C56')
         then 'Aesthetic Surgery - ENT'
         when @PrimaryDiagnosis in ('Q825')
              and left(@PrimaryProcedure,4) in ('S065','S068','S069','S091','S092')
         then 'Congenital Vascular abnormalities (eg congenital naevi and port wine stains)'
           when left(@PrimaryProcedure,4) 
         in ('S011','S012','S013') 
         then 'Face lift'
           when left(@PrimaryProcedure,4) 
         in ('S621','S621') 
         then 'Liposuction (the removal of fat from specific areas)'
           when left(@PrimaryProcedure,4) 
         in ('S031','S032','S033') 
         then 'Removal of redundant fat or skin from the arm, buttock or thigh'
           when left(@PrimaryProcedure,4) 
         in ('E023','E024','E025','E026') 
         then 'Rhinoplasty (surgery to reshape the nose)'
         when @PrimaryDiagnosis in ('L818')
              and left(@PrimaryProcedure,4) in ('S091','S092','S065','S068',
                                                   'S069')
         then 'Tattoo Removal'
         when @PrimaryDiagnosis in ('I781','I788')
              and left(@PrimaryProcedure,4) in ('S091','S092')
         then 'Thread veins or telangiectasias'
         when (@PrimaryDiagnosis in ('L720','L82X','H026') or left(@PrimaryDiagnosis,3) in('D23','D17','D22'))
              and left(@PrimaryProcedure,4) in ('S065','S068','S069')
         then 'Benign skin lesions'
           when left(@PrimaryProcedure,4) in ('s014') 
         then 'Browlift (Rhytidectomy)'
         when @PrimaryProcedure 
         in ('C131','C132','C133','C134','C138','C139') 
         then 'Blepharoplasty (repair of drooping eyelids)'
         when @PrimaryProcedure
         in ('c181','c182','c183','c184','c186','c188','c189')
         and @Hrg in ('b17','b18')
         then 'other aesthetic surgery - ophthalmology'
           when left(@PrimaryProcedure,4) in ('B311') 
           and @gender in('01','1')
         then 'Male breast reduction'
           when left(@PrimaryProcedure,4) in ('B314','B302') 
         then 'Revision of mammoplasty'
           when left(@PrimaryProcedure,4) in ('K571','K572','K574','K621','K622') 
         then 'Cardiac Ablation'
         when @Hrg in ('M03','M13')
         then 'Female genital prolapse/stress incontinence'
         when left(@PrimaryProcedure,4) in ('F091','F093','F099') 
         then 'Wisdom teeth extraction'
         when left(@PrimaryProcedure,4) in ('D241','D242') 
         then 'Cochlear Implants'
         
          when left(@PrimaryProcedure,4) in ('W401','W408','W409','W411','W418','W419','W421','W428','W429','W431','W438','W439','W441','W448','W449'
          ,'W451','W458','W459','W521','W528','W529','W531','W538','W539','W541','W548','W549','W581','W588','W589')        
           and  @Hrg in ('H03','H04')
              then 'Knees'   
                 
         when left(@PrimaryProcedure,4) in ('W431','W432','W438','W439','W441','W442','W443','W448','W449',
              'W451','W452','W458','W459','W521','W528','W528','W529','W531','W532','W538','W539','W541','W542','W548','W549','W588','W589')
      and  @Hrg in ('H08')
             then 'other joints'
--          when left(@PrimaryProcedure,4) in ('W851','W852','W859','W861','W868','W869','W443','W448','W449') 
--         then 'Knee Washouts'
          when left(@PrimaryProcedure,4) in ('W931','W941','W951','W371','W381','W391') 
         then 'primary hip replacements'         
          when left(@PrimaryProcedure,4) in ('R171','R172','R178','R179') 
         then 'primary hip replacements'         
         when @Hrg in ('m13')
          then 'female genital prolapse/stress incontinence (non-surgical)'
          when left(@PrimaryProcedure,4) in ('m531','m538','m539','p219',
                                                    'p221','p222','p223','p228',
                                                    'p229','p231','p232','p233',
                                                    'p234','p238','p239','p241',
                                                    'p243','p244','p248','p249',
                                                    'p251')
                      and @Hrg in ('m03')
                 then 'female genital prolapse/stress incontinence (surgical)'
          when left(@PrimaryProcedure,4)  in ('P055','P056','P057') then 'Labial reduction'
          when left(@PrimaryProcedure,4)  in ('f326') then 'Uvuloplasty (for snoring)'
         else 'other'
       end
       end--	else if (@methodology=3)
	return @ProcedureCategory
end
end

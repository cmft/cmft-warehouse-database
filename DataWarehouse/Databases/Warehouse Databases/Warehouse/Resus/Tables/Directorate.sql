﻿CREATE TABLE [Resus].[Directorate] (
    [DirectorateCode] VARCHAR (10) NOT NULL,
    [Directorate]     VARCHAR (50) NULL,
    [DivisionCode]    VARCHAR (50) NULL,
    CONSTRAINT [PK__Director__14D8D53D1EC71DE8] PRIMARY KEY CLUSTERED ([DirectorateCode] ASC)
);


﻿CREATE TABLE [Resus].[AuditType] (
    [AuditTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [AuditType]   VARCHAR (50) NULL,
    CONSTRAINT [PK__AuditTyp__169EC2767510DB3E] PRIMARY KEY CLUSTERED ([AuditTypeID] ASC)
);


﻿CREATE TABLE [Resus].[FinalOutcome] (
    [FinalOutcomeID] INT          NOT NULL,
    [FinalOutcome]   VARCHAR (50) NULL,
    CONSTRAINT [PK__FinalOut__7DEC82F10FF9DBA4] PRIMARY KEY CLUSTERED ([FinalOutcomeID] ASC)
);


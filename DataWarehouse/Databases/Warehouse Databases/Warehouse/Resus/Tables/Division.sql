﻿CREATE TABLE [Resus].[Division] (
    [DivisionCode] VARCHAR (10) NOT NULL,
    [Division]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Division__88DF06562297AECC] PRIMARY KEY CLUSTERED ([DivisionCode] ASC)
);


﻿CREATE TABLE [Resus].[Airway] (
    [AirwayID] INT          NOT NULL,
    [Airway]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Airway__63BF3CC21A776A17] PRIMARY KEY CLUSTERED ([AirwayID] ASC)
);


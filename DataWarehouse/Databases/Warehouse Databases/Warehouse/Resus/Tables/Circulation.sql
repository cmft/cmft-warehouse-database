﻿CREATE TABLE [Resus].[Circulation] (
    [CirculationID] INT          NOT NULL,
    [Circulation]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Circulat__1FDC638315B2B4FA] PRIMARY KEY CLUSTERED ([CirculationID] ASC)
);


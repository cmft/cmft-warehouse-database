﻿CREATE TABLE [Resus].[CallOutReason] (
    [CallOutReasonID] INT          NOT NULL,
    [CallOutReason]   VARCHAR (50) NULL,
    CONSTRAINT [PK__CalloutR__BBECDD19029FE086] PRIMARY KEY CLUSTERED ([CallOutReasonID] ASC)
);


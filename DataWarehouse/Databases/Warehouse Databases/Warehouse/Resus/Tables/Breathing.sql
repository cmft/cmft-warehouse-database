﻿CREATE TABLE [Resus].[Breathing] (
    [BreathingID] INT          NOT NULL,
    [Breathing]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Breathin__C8B9CAAD1F3C1F34] PRIMARY KEY CLUSTERED ([BreathingID] ASC)
);


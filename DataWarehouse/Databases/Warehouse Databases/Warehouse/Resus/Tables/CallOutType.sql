﻿CREATE TABLE [Resus].[CallOutType] (
    [CallOutTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [CallOutType]   VARCHAR (50) NULL,
    CONSTRAINT [PK__CallOutT__682575782A38D094] PRIMARY KEY CLUSTERED ([CallOutTypeID] ASC)
);


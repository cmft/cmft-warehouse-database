﻿CREATE TABLE [Resus].[Location] (
    [LocationID] INT          NOT NULL,
    [Location]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Location__E7FEA4773DC0A654] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);




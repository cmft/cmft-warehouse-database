﻿CREATE TABLE [Resus].[Outcome] (
    [OutcomeID] INT          NOT NULL,
    [Outcome]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Outcome__113E6AFC0B352687] PRIMARY KEY CLUSTERED ([OutcomeID] ASC)
);


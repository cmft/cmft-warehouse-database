﻿CREATE TABLE [Resus].[PresentingECG] (
    [PresentingECGID] INT          NOT NULL,
    [PresentingECG]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Presenti__D0F644383066AB36] PRIMARY KEY CLUSTERED ([PresentingECGID] ASC)
);


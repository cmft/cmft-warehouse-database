﻿create    procedure [dbo].[ReportClinicPTLDetail] 
	 @FromDate smalldatetime
	,@ToDate smalldatetime
	,@SiteCode varchar(10)
	,@SpecialtyCode varchar(10)
as

set dateformat dmy

select
	Diary.SessionDate,

	case 
		when Clinic.ConsultantCode is null then 'No Consultant'
		when Clinic.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then Clinic.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end Consultant, 

	Diary.ClinicCode,
	Diary.SessionCode,
	Clinic.Clinic,
	Diary.ValidAppointmentTypeCode SlotType,
	Diary.Units - Diary.UsedUnits VacantSlots,
	Diary.UsedUnits UsedSlots
from
	OP.Diary

inner join PAS.Clinic
on	Clinic.ClinicCode = Diary.ClinicCode

left join PAS.Site PASSite
on	PASSite.SiteCode = Clinic.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = Clinic.SpecialtyCode

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = Clinic.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

where
	Diary.SessionEndTime between @FromDate and @ToDate
and	Diary.ReasonForCancellation is null
and	Diary.Units - Diary.UsedUnits > 0

and	(
		coalesce(Clinic.SpecialtyCode, '##') = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and	(
		coalesce(Site.SiteCode, '##') = @SiteCode
	or	@SiteCode is null
	)

order by
	Diary.SessionStartTime,
	Diary.ClinicCode,
	Diary.SessionCode

﻿CREATE procedure [dbo].[LoadCOMDiagnosisProcedureEvent]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int


delete
from
	COM.DiagnosisProcedureEvent
where
	exists
	(
	select
		1
	from
		dbo.TImportCOMDiagnosisProcedureEvent
	where
		TImportCOMDiagnosisProcedureEvent.SourceUniqueID = DiagnosisProcedureEvent.SourceUniqueID
	)

select
	@deleted = @@ROWCOUNT


insert
into
	COM.DiagnosisProcedureEvent
(
	 SourceUniqueID
	,DiagnosisProcedureClassificationID
	,SourcePatientNo
	,MedicalProblemLevelID
	,DiagnosisProcedureSubTypeID
	,DiagnosisProcedureID
	,NHSIdentifier
	,Comment
	,DiagnosisProcedure
	,EncounterSourceUniqueID
	,SourceEntityCode
	,DiagnosisProcedureTime
	,DiagnosisProcedureTypeCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,DiagnosisProcedureCode
	,DiagnosisProcedureInternalCode
	,SupplementaryCode
	,SupplementaryInternalCode
	,ArchiveFlag
	,StartTime
	,EndTime
	,UpdateTime
	,StaffTeamID
	,ProfessionalCarerID
	,Duration
	,ConfidentialFlag
	,Dosage
	,DosageUnitID
	,LocationOnBodyID
	,DosageFrequency
	,FrequencyDosageUnitID
	,CancelledTime
	,RulesID
	,Value
	,ProblemFlag
	,LateralityID
	,SourceEpisodeID
	,PeriodAdministeredID
	,ReasonAdministeredID
	,AnaestheticCategoryID
	,CauseOfDeathFlag
	,EventSequenceNo
	,CodingAuthorisationFlag
	,LatchedCodingAuthorisationFlag
	,ActionToTake
	,AlertSeverityID
	,ContractedProcedureFlag
	,SpecialtyID
	,HostitalServiceFlag
	,DiagnosisProcedureEndTime
	,ProcedureLocationID
	,TheatreBodyRegionID
	,IndividualCodingStatusID
	,AnaestheticTypeID
	,CodingOnsetTypeID
	,VisitPurposeID
	,HealthOrganisationOwnerID
	,Created
	,Updated
	,ByWhom
)
select
	 SourceUniqueID
	,DiagnosisProcedureClassificationID
	,SourcePatientNo
	,MedicalProblemLevelID
	,DiagnosisProcedureSubTypeID
	,DiagnosisProcedureID
	,NHSIdentifier
	,Comment
	,DiagnosisProcedure
	,EncounterSourceUniqueID
	,SourceEntityCode
	,DiagnosisProcedureTime
	,DiagnosisProcedureTypeCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,DiagnosisProcedureCode
	,DiagnosisProcedureInternalCode
	,SupplementaryCode
	,SupplementaryInternalCode
	,ArchiveFlag
	,StartTime
	,EndTime
	,UpdateTime
	,StaffTeamID
	,ProfessionalCarerID
	,Duration
	,ConfidentialFlag
	,Dosage
	,DosageUnitID
	,LocationOnBodyID
	,DosageFrequency
	,FrequencyDosageUnitID
	,CancelledTime
	,RulesID
	,Value
	,ProblemFlag
	,LateralityID
	,SourceEpisodeID
	,PeriodAdministeredID
	,ReasonAdministeredID
	,AnaestheticCategoryID
	,CauseOfDeathFlag
	,EventSequenceNo
	,CodingAuthorisationFlag
	,LatchedCodingAuthorisationFlag
	,ActionToTake
	,AlertSeverityID
	,ContractedProcedureFlag
	,SpecialtyID
	,HostitalServiceFlag
	,DiagnosisProcedureEndTime
	,ProcedureLocationID
	,TheatreBodyRegionID
	,IndividualCodingStatusID
	,AnaestheticTypeID
	,CodingOnsetTypeID
	,VisitPurposeID
	,HealthOrganisationOwnerID
	,Created = GETDATE()
	,Updated = GETDATE()
	,ByWhom = SUSER_NAME()
from
	dbo.TImportCOMDiagnosisProcedureEvent

select
	 @inserted = @@ROWCOUNT
	,@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Records Inserted: ' + cast(@inserted as varchar) +
		', Records Deleted: ' + cast(@deleted as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


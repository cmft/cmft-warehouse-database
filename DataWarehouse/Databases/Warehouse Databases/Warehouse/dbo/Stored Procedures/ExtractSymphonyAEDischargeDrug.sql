﻿

CREATE proc [dbo].[ExtractSymphonyAEDischargeDrug]

	 @from smalldatetime
	,@to smalldatetime
	
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int = 0
declare @Stats varchar(255)

select @StartTime = getdate()

--truncate table dbo.TImportAEDischargeDrug

insert into dbo.TImportAEDischargeDrug
(
	SourceUniqueID
	,AESourceUniqueID 
	,DischargeDate
	,DischargeTime
	,DrugCode
	,SequenceNo
)

select
	SourceUniqueID
	,AESourceUniqueID 
	,DischargeDate
	,DischargeTime
	,DrugCode
	,SequenceNo
from
	(
	select
		SourceUniqueID = atd_EF_extrafieldID
		,AESourceUniqueID = atd_EF_atdid
		,DischargeDate = cast(atd_dischdate as date)
		,DischargeTime = atd_dischdate
		,DrugCode = atd_EF_Value
		,SequenceNo = 1
	from
		[$(Symphony)].dbo.Attendance_Details_ExtraFields
		
	inner join [$(Symphony)].dbo.Attendance_Details
	on	Attendance_Details.atd_id = Attendance_Details_ExtraFields.atd_EF_atdid
	
	inner join [$(Symphony)].dbo.Episodes Episode
	on	Episode.epd_id = atd_epdid
		
	where
		atd_EF_FieldID in 
						(
						1472 -- Adult Drugs
						,1604 --EEC Drugs
						,1213 -- Paediatric Drugs
						--,1471 --Paediatric Dose
						)
	and atd_EF_Value not like '%||%' -- only one drug recorded against attendance
	and atd_dischdate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
	and	atd_deleted = 0
	and	epd_deleted = 0
	--and exists
	--		(
	--		select
	--			1
	--		from
	--			dbo.TImportAEEncounter
	--		where
	--			Attendance_Details_ExtraFields.atd_EF_atdid = TImportAEEncounter.SourceUniqueID
	--		) 
	) DischargeDrug
	
	
select @RowsInserted = @@rowcount



declare 
	@SourceUniqueID int 
	,@AESourceUniqueID int
	,@DischargeDate date
	,@DischargeTime datetime
	,@DrugCodeList varchar(max)
	,@DrugCode int
	,@SequenceNo int
	
	declare RecordCursor cursor fast_forward for

	select
		--top 10
		SourceUniqueID = atd_EF_extrafieldID
		,AESourceUniqueID = atd_EF_atdid
		,DischargeDate = cast(atd_dischdate as date)
		,DischargeTime = atd_dischdate
		,DrugCodeList = atd_EF_Value
	from
		[$(Symphony)].dbo.Attendance_Details_ExtraFields
		
	inner join [$(Symphony)].dbo.Attendance_Details
	on	Attendance_Details.atd_id = Attendance_Details_ExtraFields.atd_EF_atdid
	
	inner join [$(Symphony)].dbo.Episodes Episode
	on	Episode.epd_id = atd_epdid
	
	where
		atd_EF_FieldID in 
						(
						1472 -- Adult Drugs
						,1604 --EEC Drugs
						,1213 -- Paediatric Drugs
						--,1471 --Paediatric Dose
						)
	and atd_EF_Value like '%||%' -- multiple drugs recorded against attendance
	and atd_dischdate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
	and	atd_deleted = 0
	and	epd_deleted = 0
	--and exists
	--			(
	--			select
	--				1
	--			from
	--				dbo.TImportAEEncounter
	--			where
	--				Attendance_Details_ExtraFields.atd_EF_atdid = TImportAEEncounter.SourceUniqueID
	--			) 
	
	open RecordCursor
	
	fetch next from RecordCursor
	into
		@SourceUniqueID 
		,@AESourceUniqueID
		,@DischargeDate
		,@DischargeTime
		,@DrugCodeList
		
	while @@fetch_status = 0
	
	begin
								
		select @SequenceNo = 0
		
		declare DrugCursor cursor fast_forward for

		select
			 DrugCode = ItemCode
		from
			dbo.ParamParserFn(@DrugCodeList, '||')	
			
		order by
			ItemCode
			
		open DrugCursor
	
		fetch next from DrugCursor
		into @DrugCode
		
		while @@fetch_status = 0
		
		begin
		
			select @SequenceNo = @SequenceNo + 1
			
			insert into dbo.TImportAEDischargeDrug
			(
			SourceUniqueID
			,AESourceUniqueID
			,DischargeDate
			,DischargeTime
			,DrugCode
			,SequenceNo
			)

			select
				@SourceUniqueID 
				,@AESourceUniqueID
				,@DischargeDate
				,@DischargeTime
				,@DrugCode
				,@SequenceNo
						
			select @RowsInserted = @RowsInserted + 1
				
			fetch next from DrugCursor
			into @DrugCode
			
		end
		
		close DrugCursor
		deallocate DrugCursor		
	
	fetch next from RecordCursor
	into
		@SourceUniqueID 
		,@AESourceUniqueID
		,@DischargeDate
		,@DischargeTime
		,@DrugCodeList
	
	end
		 
	close RecordCursor
	deallocate RecordCursor
	
select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'
	
print @Stats

exec WriteAuditLogEvent 'ExtractSymphonyAEDischargeDrug', @Stats, @StartTime
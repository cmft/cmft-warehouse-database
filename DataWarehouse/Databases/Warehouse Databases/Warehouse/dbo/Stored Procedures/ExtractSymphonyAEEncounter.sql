﻿



CREATE procedure [dbo].[ExtractSymphonyAEEncounter]
	 @from smalldatetime
	,@to smalldatetime
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @localfrom smalldatetime
declare @localto smalldatetime

select @StartTime = getdate()

--DECLARE @from smalldatetime = NULL
--DECLARE @to smalldatetime = NULL
--set @from = '04/01/2011'
--set @to = '04/02/2011'


select
	 @localfrom = @from
	,@localto = dateadd(second, 24 * 60 * 60 -1, @to)


insert into dbo.TImportAEEncounter
(
	 SourceUniqueID
	,DistrictNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,DateOfBirth
	,SexCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,ProviderCode
	,SiteCode
	,CommissionerCode
	,StaffMemberCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	,ResidencePCTCode
	,TriageCategoryCode
	,PresentingProblem
	,PresentingProblemCode 
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,EthnicCategoryCode
	,ReligionCode
	,ReferredToSpecialtyCode
	,DecisionToAdmitTime
	,DischargeDestinationCode
	,RegisteredTime
	,TransportRequestTime
	,TransportAvailableTime
	,SourcePatientNo
	,AscribeLeftDeptTime
	,CDULeftDepartmentTime
	,PCDULeftDepartmentTime
	,SourceAttendanceDisposalCode
	,AmbulanceArrivalTimeInt
	,AmbulanceCrewPRF
	,UnplannedReattend7Day
	,ClinicalAssessmentTime
	,SourceCareGroupCode
	,AlcoholLocation
	,AlcoholInPast12Hours
	,RapidAssessmentTime
	,TriageComment
) 
select
	 Encounter.atd_id SourceUniqueID
	,DistrictNo.DistrictNo DistrictNo
	,DistrictNoOrganisationCode = 'RW3'
	,NHSNumber.NHSNumber NHSNumber
	,NHSNumberStatusId = null
	,PatientTitle = PatientTitle.Lkp_Name
	,PatientForename = Patient.pat_forename
	,PatientSurname = Patient.pat_surname

	,PatientAddress1 =
		coalesce(
			 PermanentAddress.add_line1
			,TemporaryAddress.add_line1
		)

	,PatientAddress2 =
		coalesce(
			 PermanentAddress.add_line2
			,TemporaryAddress.add_line2
		)

	,PatientAddress3 =
		coalesce(
			 PermanentAddress.add_line3
			,TemporaryAddress.add_line3
		)

	,PatientAddress4 =
		coalesce(
			 PermanentAddress.add_line4
			,TemporaryAddress.add_line4
		)

	,Postcode =
		coalesce(
			 PermanentAddress.add_postcode
			,TemporaryAddress.add_postcode
		)

	,Patient.pat_dob DateOfBirth

	,SexCode = Sex.SexCode

	,RegisteredGpCode = Gp.gp_code
	,RegisteredGpPracticeCode = GpPractice.pr_praccode
	,Encounter.atd_num AttendanceNumber

	,ArrivalModeCode = Encounter.atd_arrmode

--	,ArrivalModeCode =
--		case ArrivalMode.lkp_name
--		when 'Ambulance' 
--		then 1 
--		else 2
--		end

	,AttendanceCategoryCode =
		case Encounter.atd_attendancetype
		when 1
		then '2' --planned review
		when 2
		then '3' --unplanned review
		else '1' --attend
		end

	,AttendanceDisposalCode =
		right(
			'0' + case when Disposal.DisposalCode = '' then null else Disposal.DisposalCode end
			,2
		)

	,IncidentLocationTypeCode = IncidentLocationType.IncidentLocationTypeCode
	
	--GS20111110 Updated. Old code defaulting recorded cds codes to 80. Defaults blanks and nulls to 0 
	,PatientGroupCode =
			CASE
				WHEN PatientGroup.PatientGroupCode  = '' THEN '0'
			ELSE
				COALESCE
						(
						 PatientGroup.PatientGroupCode
						,'0'
						)
			END
			
		--case
		--when PatientGroup.PatientGroupCode is null	then PatientGroup.PatientGroupCode

		--when PatientGroup.PatientGroupCode = '0'	then null

		--else '80'
		--end

	,SourceOfReferral.SourceOfReferralCode
	,dateadd(day, datediff(day, 0, Encounter.atd_arrivaldate), 0) ArrivalDate
	,Encounter.atd_arrivaldate ArrivalTime

	,AgeOnArrival =
		datediff(year, Patient.pat_dob, Encounter.atd_arrivaldate) -
		case
		when datepart(day, Patient.pat_dob) = datepart(day, Encounter.atd_arrivaldate)
		and	datepart(month, Patient.pat_dob) = datepart(month,Encounter.atd_arrivaldate)
		and datepart(year, Patient.pat_dob) <> datepart(year,Encounter.atd_arrivaldate)
		then 0 
		else 
			case
			when Patient.pat_dob > Encounter.atd_arrivaldate then 0
			when datepart(dy, Patient.pat_dob) > datepart(dy, Encounter.atd_arrivaldate) 
			then 1
			else 0 
			end 
		end

	,InitialAssessmentTime = Triage.tri_date

	,SeenForTreatmentTime = Result.res_date

	,AttendanceConclusionTime = Encounter.atd_dischdate

	,DepartureTime = Departure.DepartureTime

	,CommissioningSerialNo = Department.CommissioningSerialNo

	,ProviderCode = 'RW3'
	,SiteCode = Department.SiteCode
	,CommissionerCode = Department.PCTCode
	,StaffMemberCode = Result.stf_staffid
	,Created = getdate()
	,Updated = getdate()
	,ByWhom = suser_name()
	,'SYM' InterfaceCode
	,ResidencePCTCode = Department.PCTCode

	,TriageCategoryCode = Triage.tri_category
	,PresentingProblem = left(ComplaintDetails.ComplaintDetails,300)
	,PresentingProblemCode = Encounter.atd_complaintid
	,ToXrayTime = ToXray.InvestigationDate
	,FromXrayTime = FromXray.InvestigationResultDate
	,ToSpecialtyTime = ToSpecialty.InvestigationDate
	,SeenBySpecialtyTime = SeenBySpecialty.InvestigationResultDate
	,EthnicCategoryCode = PatientDetail.pdt_ethnic
	,ReligionCode = PatientDetail.pdt_religion
	--GC changed 09/09/2014 for PED site
    ,ReferredToSpecialtyCode = 
                  case
                        when Department.SiteCode = 'RW3RC' and PEDToSpecialty.SpecialtyCode <> 0 then PEDToSpecialty.SpecialtyCode
                        else ToSpecialty.SpecialtyCode
                  end

	,DecisionToAdmitTime = Admission.DecisionToAdmitTime
	,DischargeDestinationCode = Encounter.atd_dischdest
	,RegisteredTime = Encounter.atd_regdate
	,TransportRequestTime = TransportRequest.InvestigationDate
	,TransportAvailableTime = TransportAvailable.InvestigationResultDate
	,SourcePatientNo = Patient.pat_pid

-- the Ascribe approach to acquiring the Left Department Time
-- adapted from [$(Symphony)].dbo.StageTimes_view
	,AscribeLeftDeptTime =
		(
		SELECT top 1
			cul_locationdate
		FROM
			[$(Symphony)].dbo.Current_Locations Location
		WHERE
			cul_locationid = 31
		AND cul_atdid = Encounter.atd_id
		)

	,CDULeftDepartmentTime =  -- if exists, use this as left dept (adult)
		(
		SELECT top 1
			Location.cul_locationdate
		FROM
			[$(Symphony)].dbo.Current_Locations Location
		WHERE
			Location.cul_locationid in (select Locations.loc_id from [$(Symphony)].dbo.CFG_Locations Locations where Locations.loc_parentid = 202)
		AND Location.cul_atdid = Encounter.atd_id
		)

	,PCDULeftDepartmentTime =  -- if exists, use this as left dept (paed)
		(
		SELECT top 1
			Location.cul_locationdate
		FROM
			[$(Symphony)].dbo.Current_Locations Location
		WHERE
			Location.cul_locationid in (select Locations.loc_id from [$(Symphony)].dbo.CFG_Locations Locations where Locations.loc_parentid = 134)
		AND Location.cul_atdid = Encounter.atd_id
		)

	,SourceAttendanceDisposalCode = Encounter.atd_dischoutcome

	--,AmbulanceArrivalTime = Encounter.atd_arrivaldate --to be changed GS-20110405 Commented out
	
	--,AmbulanceArrivalTime = tbl_AmbulanceArrivalTime.AmbulanceArrivalTime -- GS-20110405 Additional field added

	,AmbulanceArrivalTime.AmbulanceArrivalTimeInt

	
	,AmbulanceCrewPRF =
		CASE
		WHEN Encounter.atd_ambcrew = '' THEN NULL
		ELSE Encounter.atd_ambcrew
		END -- GS-20110405 Additional field added
	
	--,ReattendedWithinSevenDays = tbl_ReattendedWithinSevenDays.ReattendedWithinSevenDays -- GS-20110405 Additional field added

	,UnplannedReattend7Day = UnplannedReattend.UnplannedReattend7Day -- CCB- 2011-04-08 Changed table alias

	,ClinicalAssessment.ClinicalAssessmentTime
	
	,SourceCareGroupCode = Encounter.atd_caregroup
	
	,AlcoholLocation

	,AlcoholInPast12Hours = left(AlcoholInPast12Hours,20)

	,RapidAssessment.RapidAssessmentTime

	,TriageComment = 
		Triage.TriageComment

from
	[$(Symphony)].dbo.Attendance_Details Encounter

inner join [$(Symphony)].dbo.Episodes Episode
on	Episode.epd_id = Encounter.atd_epdid

inner join [$(Symphony)].dbo.Patient Patient
on	Patient.pat_pid = Episode.epd_pid

inner join [$(Symphony)].dbo.Patient_details PatientDetail
on	PatientDetail.pdt_pid = Patient.pat_pid



-- 2010-03-05 CCB get previous patient details

left join [$(Symphony)].dbo.Aud_Patient_Details AuditPatientDetail
on	AuditPatientDetail.pdt_pid = Episode.epd_pid
and	AuditPatientDetail.pdt_update < Encounter.atd_dischdate
and	AuditPatientDetail.pdt_deleted = 0
and	not exists
	(
	select
		1
	from
		[$(Symphony)].dbo.Aud_Patient_Details Previous
	where
		Previous.pdt_pid = AuditPatientDetail.pdt_pid
	and	Previous.pdt_update < Encounter.atd_dischdate
	and	Previous.pdt_identity > AuditPatientDetail.pdt_identity
	and	AuditPatientDetail.pdt_deleted = 0
	)

-- 2010-02-10 CCB modifications to reflect Symphony upgrade to v2.28 BEGIN

/* Removed

inner join [$(Symphony)].dbo.GP GP
on	GP.gp_code = PatientDetail.pdt_gpid
and	GP.gp_practise = PatientDetail.pdt_practise
and	GP.gp_praddid = PatientDetail.pdt_praddid

*/

-- added

-- 2010-05-03 CCB coalesce to ensure we get old details if available and current details if not

left join [$(Symphony)].dbo.GPPracticeAddressLink GPPracticeAddressLinkOld
on	GPPracticeAddressLinkOld.gpa_gpid = AuditPatientDetail.pdt_gpid
and GPPracticeAddressLinkOld.gpa_prid = AuditPatientDetail.pdt_practise
and GPPracticeAddressLinkOld.gpa_praddid = AuditPatientDetail.pdt_praddid

left join [$(Symphony)].dbo.GPPracticeAddressLink GPPracticeAddressLinkCurrent
on	GPPracticeAddressLinkCurrent.gpa_gpid = PatientDetail.pdt_gpid
and GPPracticeAddressLinkCurrent.gpa_prid = PatientDetail.pdt_practise
and GPPracticeAddressLinkCurrent.gpa_praddid = PatientDetail.pdt_praddid

--left join [$(Symphony)].dbo.GPPracticeAddressLink GPPracticeAddressLink 
--on	coalesce(AuditPatientDetail.pdt_gpid , PatientDetail.pdt_gpid) = GPPracticeAddressLink.gpa_gpid 
--and coalesce(AuditPatientDetail.pdt_practise , PatientDetail.pdt_practise) = GPPracticeAddressLink.gpa_prid 
--and coalesce(AuditPatientDetail.pdt_praddid , PatientDetail.pdt_praddid) = GPPracticeAddressLink.gpa_praddid

left join [$(Symphony)].dbo.Gp_Practise GpPractice 
on	GpPractice.Pr_id = coalesce(GPPracticeAddressLinkOld.gpa_prid, GPPracticeAddressLinkCurrent.gpa_prid)

left join [$(Symphony)].dbo.Gp Gp 
on Gp.gp_id = coalesce(GPPracticeAddressLinkOld.gpa_gpid, GPPracticeAddressLinkCurrent.gpa_gpid)

-- 2010-02-10 CCB modifications to reflect Symphony upgrade to v2.28 END





inner join 
	(
	select
		 Department.dpt_id
		,CommissioningSerialNo = '5NT00A'

		,PCTCode = '5NT'

		,SiteCode =
		case
		when Department.dpt_name = 'MRI ED' then 'RW3MR'
		when Department.dpt_name = 'PED' then 'RW3RC'
		when Department.dpt_name = 'EEC' then 'RW3RE'
		when Department.dpt_name = 'St Mary''s' then 'RW3SM'
		when Department.dpt_name = 'Dental' then 'RW3DH'
		end
	from
		[$(Symphony)].dbo.CFG_Dept Department
	where
		Department.dpt_name in (
			 'MRI ED'
			,'PED'
			,'EEC'
			,'St Mary''s'
			,'Dental'
			)
	) Department
on	Department.dpt_id = Episode.epd_deptid

left join
	(
	select distinct
		 DistrictNo = patient_system_ids.psi_system_id 
		,psi_pid
	from
		[$(Symphony)].dbo.Patient_system_ids patient_system_ids
	where
		patient_system_ids.psi_system_name = 
			(
			select
				Lkp_ID
			from
				[$(Symphony)].dbo.Lookups
			where
				Lkp_Name = 'District Number'
			)
	and	not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Patient_system_ids PreviousDistrictNo
		where
			PreviousDistrictNo.psi_system_name = patient_system_ids.psi_system_name
		and	PreviousDistrictNo.psi_pid = patient_system_ids.psi_pid
		and	PreviousDistrictNo.psi_system_id > patient_system_ids.psi_system_id
		)
	) DistrictNo
on	DistrictNo.psi_pid = Patient.pat_pid

left join
	(
	select distinct
		 NHSNumber = replace(patient_system_ids.psi_system_id, ' ', '')
		,psi_pid
		,psi_status
	from
		[$(Symphony)].dbo.Patient_system_ids patient_system_ids
	where
		patient_system_ids.psi_system_name = 
			(
			select
				Lkp_ID
			from
				[$(Symphony)].dbo.Lookups
			where
				Lkp_Name = 'NHS Number'
			)
	and	not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Patient_system_ids PreviousNHSNumber
		where
			PreviousNHSNumber.psi_system_name = patient_system_ids.psi_system_name
		and	PreviousNHSNumber.psi_pid = patient_system_ids.psi_pid
		and	replace(PreviousNHSNumber.psi_system_id, ' ', '') > replace(patient_system_ids.psi_system_id, ' ', '')
		)
	) NHSNumber
on	NHSNumber.psi_pid = Patient.pat_pid

left join
	(
	select
		*
	from
		[$(Symphony)].dbo.Address PermanentAddress
	where
		PermanentAddress.add_linktype = 1
	and	PermanentAddress.add_type = 2673
--	and	PermanentAddress.add_type in 
--		(
--		select
--			Lkp_ID
--		from
--			[$(Symphony)].dbo.lookups AddressType
--		where
--			AddressType.Lkp_Name = 'Permanent address'
--		)
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Address PreviousPermanentAddress
		where
			PreviousPermanentAddress.add_linkid = PermanentAddress.add_linkid
		and	PreviousPermanentAddress.add_linktype = PermanentAddress.add_linktype
		and	PreviousPermanentAddress.add_type = PermanentAddress.add_type
		and	PreviousPermanentAddress.add_update > PermanentAddress.add_update
		)
	) PermanentAddress
on	PermanentAddress.add_linkid = PatientDetail.pdt_pid

left join
	(
	select
		*
	from
		[$(Symphony)].dbo.Address TemporaryAddress
	where
		TemporaryAddress.add_linktype = 1
	and	TemporaryAddress.add_type in 
		(
		 4993
		,10522
--		select
--			Lkp_ID
--		from
--			[$(Symphony)].dbo.lookups AddressType
--		where
--			AddressType.Lkp_Name = 'Permanent address'
		)
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Address PreviousTemporaryAddress
		where
			PreviousTemporaryAddress.add_linkid = TemporaryAddress.add_linkid
		and	PreviousTemporaryAddress.add_linktype = TemporaryAddress.add_linktype
		and	PreviousTemporaryAddress.add_type = TemporaryAddress.add_type
		and	PreviousTemporaryAddress.add_update > TemporaryAddress.add_update
		)
	) TemporaryAddress
on	TemporaryAddress.add_linkid = PatientDetail.pdt_pid

left join
	(
	select distinct
		 SexId = flm_lkpid
		,SexCode = not_text
	from
		[$(Symphony)].dbo.Notes

	inner join [$(Symphony)].dbo.LookupMappings
	on flm_value = not_noteid
	and	flm_mtid = 
		(
		select
			mt_id
		from
			[$(Symphony)].dbo.MappingTypes 
		where
			mt_name = 'CDS'
		)
	) Sex
on	Sex.SexId = Patient.pat_sex


left join [$(Symphony)].dbo.Lookups PatientTitle
on	Patient.pat_title = PatientTitle.Lkp_ID


left join
	(
	select distinct
		 DisposalId = flm_lkpid
		,DisposalCode = not_text
	from
		[$(Symphony)].dbo.Notes

	inner join [$(Symphony)].dbo.LookupMappings
	on	flm_value = not_noteid
	and	flm_mtid = 
		(
		select
			mt_id
		from
			[$(Symphony)].dbo.MappingTypes 
		where
			mt_name = 'CDS'
		)
	) Disposal
on	Disposal.DisposalId = Encounter.atd_dischoutcome

left join
	(
	select distinct
		 IncidentLocationTypeId = flm_lkpid
		,IncidentLocationTypeCode = not_text
	from
		[$(Symphony)].dbo.Notes

	inner join [$(Symphony)].dbo.LookupMappings
	on	flm_value = not_noteid
	and	flm_mtid = 
		(
		select
			mt_id
		from
			[$(Symphony)].dbo.MappingTypes 
		where
			mt_name = 'CDS'
		)
	) IncidentLocationType
on	IncidentLocationType.IncidentLocationTypeId = Encounter.atd_location

left join
	(
	select distinct
		 PatientGroupId = flm_lkpid
		,PatientGroupCode = not_text
	from
		[$(Symphony)].dbo.Notes

	inner join [$(Symphony)].dbo.LookupMappings
	on	flm_value = not_noteid
	and	flm_mtid = 
		(
		select
			mt_id
		from
			[$(Symphony)].dbo.MappingTypes 
		where
			mt_name = 'CDS'
		)
	) PatientGroup
on	PatientGroup.PatientGroupId = Encounter.atd_reason

left join
	(
	select distinct
		 SourceOfReferralId = flm_lkpid
		,SourceOfReferralCode = not_text
	from
		[$(Symphony)].dbo.Notes

	inner join [$(Symphony)].dbo.LookupMappings
	on	flm_value = not_noteid
	and	flm_mtid = 
		(
		select
			mt_id
		from
			[$(Symphony)].dbo.MappingTypes 
		where
			mt_name = 'CDS'
		)
	) SourceOfReferral
on	SourceOfReferral.SourceOfReferralId = Encounter.atd_refsource

left join (
	select
		 Triage.*
		,TriageComment = 
			TriageNotes.not_text
	from
		[$(Symphony)].dbo.Triage Triage

	left outer join [$(Symphony)].dbo.Notes TriageNotes 
	on	Triage.tri_comments = TriageNotes.not_noteid

	where
		Triage.tri_inactive = 0
	and	not exists
			(
			select
				1
			from
				[$(Symphony)].dbo.Triage LatestTriage
			where
				LatestTriage.tri_inactive = 0
			and	LatestTriage.tri_atdid = Triage.tri_atdid
			and	LatestTriage.tri_trid < Triage.tri_trid
			)
	) Triage
on	Triage.tri_atdid = Encounter.atd_id


--(9s)
left join --get the first clinician outcome based result date
	(
	select distinct
		 Result.res_atdid
		,Result.res_date
		,Staff.stf_staffid
	from
		[$(Symphony)].dbo.Result_details Result

	inner join [$(Symphony)].dbo.Staff Staff
	on	Staff.stf_staffid = Result.res_staff1

	where
		Result.res_depid IN ( 86 , 195 ) -- CCB 2011-10-24 the code below is returning DEP 174, which is 'Call MRI'
			--(
			--select
			--	rps_value
			--from
			--	[$(Symphony)].dbo.cfg_reportsettings
			--where
			--	rps_name IN('SeeA+EClinicianOutcome','See A+E ClinicianOutcome') -- = 'SeeA+EClinicianOutcome'  GS20111019  Additional label added to bring through dental depID 195
			--)
	and	Result.res_inactive <> 1
	and	not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Result_details LaterResult

		inner join [$(Symphony)].dbo.Staff Staff
		on	Staff.stf_staffid = LaterResult.res_staff1

		where
			LaterResult.res_depid IN ( 86 , 195 ) 
				--(
				--select
				--	rps_value
				--from
				--	[$(Symphony)].dbo.cfg_reportsettings
				--where
				--	rps_name IN('SeeA+EClinicianOutcome','See A+E ClinicianOutcome') -- = 'SeeA+EClinicianOutcome' GS20111019  Additional label added to bring through dental depID 195
				--)
		and	LaterResult.res_inactive <> 1
		and	LaterResult.res_atdid = Result.res_atdid
		and (
				LaterResult.res_date < Result.res_date
			or	(
					LaterResult.res_date = Result.res_date
				and	LaterResult.res_resid < Result.res_resid
				)
			)
		)
	) Result
on	Result.res_atdid = Encounter.atd_id



--(9s)
left join
	(
	select
		 Location.cul_atdid
		,DepartureTime = Location.cul_locationdate
	from
		[$(Symphony)].dbo.Current_Locations Location
	where
		Location.cul_locationid in 
		(
		select
			loc_id
		from
			[$(Symphony)].dbo.CFG_Locations
		where
			loc_name = 'Left Department'
		)
	and	not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Current_Locations PreviousLocation
		where
			PreviousLocation.cul_locationid in 
			(
			select
				loc_id
			from
				[$(Symphony)].dbo.CFG_Locations
			where
				loc_name = 'Left Department'
			)
		and	PreviousLocation.cul_atdid = Location.cul_atdid
		and (
				--PreviousLocation.cul_locationdate > Location.cul_locationdate
				PreviousLocation.cul_update > Location.cul_update
			or
				(
					--PreviousLocation.cul_locationdate = Location.cul_locationdate
					PreviousLocation.cul_locationdate = Location.cul_update
				and	PreviousLocation.cul_culid > Location.cul_culid
				)
			)
		)
	) Departure
on	Departure.cul_atdid = Encounter.atd_id

--left join [$(Symphony)].dbo.lookups ArrivalMode
--on	ArrivalMode.lkp_id = Encounter.atd_arrmode

left join (
	select
		 SourceUniqueID = Request.req_atdid
		,InvestigationDate = Request.req_date
	from
		[$(Symphony)].dbo.Request_Details Request
	where
		Request.req_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'Radiology'
			)
	and	Request.req_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Request_Details PreviousRequest
		where
			PreviousRequest.req_depid = Request.req_depid
		and	PreviousRequest.req_atdid = Request.req_atdid
		and	PreviousRequest.req_inactive <> 1
		and	(
				PreviousRequest.req_date > Request.req_date
			or	(
					PreviousRequest.req_date = Request.req_date
				and	PreviousRequest.req_reqid > Request.req_reqid
				)
			)
		)
	) ToXray
on	ToXray.SourceUniqueID = Encounter.atd_id

left join (
	select
		 SourceUniqueID = Result.res_atdid
		,InvestigationResultDate = Result.res_date
	from
		[$(Symphony)].dbo.Result_details Result
	where
		Result.res_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'RadiologyOutcome'
			)
	and	Result.res_inactive <> 1

	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Result_details PreviousResult
		where
			PreviousResult.res_depid = Result.res_depid
		and	PreviousResult.res_atdid = Result.res_atdid
		and	PreviousResult.res_inactive <> 1
		and	(
				PreviousResult.res_date > Result.res_date
			or	(
					PreviousResult.res_date = Result.res_date
				and	PreviousResult.res_resid > Result.res_resid
				)
			)
		)
	) FromXray
on	FromXray.SourceUniqueID = Encounter.atd_id


left join (
	select
		 SourceUniqueID = Request.req_atdid
		,InvestigationDate = Request.req_date
		,SpecialtyCode = Request.req_request
	from
		[$(Symphony)].dbo.Request_Details Request
	where
		Request.req_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'Specialty'
			)
	and	Request.req_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Request_Details PreviousRequest
		where
			PreviousRequest.req_depid = Request.req_depid
		and	PreviousRequest.req_atdid = Request.req_atdid
		and	PreviousRequest.req_inactive <> 1
		and	(
				PreviousRequest.req_date > Request.req_date
			or	(
					PreviousRequest.req_date = Request.req_date
				and	PreviousRequest.req_reqid > Request.req_reqid
				)
			)
		)
	) ToSpecialty
on	ToSpecialty.SourceUniqueID = Encounter.atd_id

left join (
      select
            SourceUniqueID = Request.req_atdid
            ,InvestigationDate = Request.req_date
            ,SpecialtyCode =  Request.req_field1
      from
            [$(Symphony)].dbo.Request_Details Request
      where
            Request.req_depid in 
                  (
                  select
                        rps_value
                  from
                        [$(Symphony)].dbo.CFG_ReportSettings
                  where
                        rps_name = 'Specialty'
                  )
      and   Request.req_inactive <> 1
      and not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Request_Details PreviousRequest
            where
                  PreviousRequest.req_depid = Request.req_depid
            and   PreviousRequest.req_atdid = Request.req_atdid
            and   PreviousRequest.req_inactive <> 1
            and   (
                        PreviousRequest.req_date > Request.req_date
                  or    (
                              PreviousRequest.req_date = Request.req_date
                        and   PreviousRequest.req_reqid > Request.req_reqid
                        )
                  )
            )
      ) PEDToSpecialty
on    PEDToSpecialty.SourceUniqueID = Encounter.atd_id




left join (
	select
		 SourceUniqueID = Result.res_atdid
		,InvestigationResultDate = Result.res_date
	from
		[$(Symphony)].dbo.Result_details Result
	where
		Result.res_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'SpecialtyOutcome'
			)
	and	Result.res_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Result_details PreviousResult
		where
			PreviousResult.res_depid = Result.res_depid
		and	PreviousResult.res_atdid = Result.res_atdid
		and	PreviousResult.res_inactive <> 1
		and	(
				PreviousResult.res_date > Result.res_date
			or	(
					PreviousResult.res_date = Result.res_date
				and	PreviousResult.res_resid > Result.res_resid
				)
			)
		)
	) SeenBySpecialty
on	SeenBySpecialty.SourceUniqueID = Encounter.atd_id

/*
left join (
	select
		 SourceUniqueID = Result.res_atdid
		,DecisionToAdmitTime = Result.res_date
	from
		[$(Symphony)].dbo.result_details Result
	where
		Result.res_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.cfg_reportsettings
			where
				rps_name = 'A+E BedRequest'
			)
	and	Result.res_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.result_details PreviousResult
		where
			PreviousResult.res_depid = Result.res_depid
		and	PreviousResult.res_atdid = Result.res_atdid
		and	PreviousResult.res_inactive <> 1
		and	(
				PreviousResult.res_date > Result.res_date
			or	(
					PreviousResult.res_date = Result.res_date
				and	PreviousResult.res_resid > Result.res_resid
				)
			)
		)
	) Admission
on	Admission.SourceUniqueID = Encounter.atd_id
*/

left join (
	select
		 SourceUniqueID = Request.req_atdid
		,DecisionToAdmitTime = Request.req_date
	from
		[$(Symphony)].dbo.Request_Details Request
	where
		Request.req_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'A+E BedRequest'
			)
	and	Request.req_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Request_Details PreviousRequest
		where
			PreviousRequest.req_depid = Request.req_depid
		and	PreviousRequest.req_atdid = Request.req_atdid
		and	PreviousRequest.req_inactive <> 1
		and	(
				PreviousRequest.req_date > Request.req_date
			or	(
					PreviousRequest.req_date = Request.req_date
				and	PreviousRequest.req_reqid > Request.req_reqid
				)
			)
		)
	) Admission
on	Admission.SourceUniqueID = Encounter.atd_id

left join (
	select
		 SourceUniqueID = Request.req_atdid
		,InvestigationDate = Request.req_date
	from
		[$(Symphony)].dbo.Request_Details Request
	where
		Request.req_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'Transport'
			)
	and	Request.req_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Request_Details PreviousRequest
		where
			PreviousRequest.req_depid = Request.req_depid
		and	PreviousRequest.req_atdid = Request.req_atdid
		and	PreviousRequest.req_inactive <> 1
		and	(
				PreviousRequest.req_date > Request.req_date
			or	(
					PreviousRequest.req_date = Request.req_date
				and	PreviousRequest.req_reqid > Request.req_reqid
				)
			)
		)
	) TransportRequest
on	TransportRequest.SourceUniqueID = Encounter.atd_id

left join (
	select
		 SourceUniqueID = Result.res_atdid
		,InvestigationResultDate = Result.res_date
	from
		[$(Symphony)].dbo.Result_details Result
	where
		Result.res_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'TransportOutcome'
			)
	and	Result.res_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Result_details PreviousResult
		where
			PreviousResult.res_depid = Result.res_depid
		and	PreviousResult.res_atdid = Result.res_atdid
		and	PreviousResult.res_inactive <> 1
		and	(
				PreviousResult.res_date > Result.res_date
			or	(
					PreviousResult.res_date = Result.res_date
				and	PreviousResult.res_resid > Result.res_resid
				)
			)
		)
	) TransportAvailable
on	TransportAvailable.SourceUniqueID = Encounter.atd_id

----A+E reattended within 7 days
--LEFT OUTER JOIN
--		(SELECT DISTINCT atd_EF_atdid
--				,CASE atd_EF_Value
--				WHEN 17166 THEN 0 -- Recorded as No
--				WHEN 17167 THEN 1 -- Recorded as Yes
--				ELSE 2  -- Recorded as something else
--				END AS ReattendedWithinSevenDays
				
--		FROM [$(Symphony)].dbo.Attendance_Details_ExtraFields tbl_att_dtl_exfd
			
--	 	WHERE tbl_att_dtl_exfd.atd_EF_FieldID = 1667 -- Code used to identify Attended for same reason within 7 days
--		) tbl_ReattendedWithinSevenDays ON tbl_ReattendedWithinSevenDays.atd_EF_atdid = Encounter.atd_id

--LEFT OUTER JOIN 
--		(SELECT DISTINCT atd_EF_atdid
--				,convert(varchar(5),atd_EF_Value) AS AmbulanceArrivalTime

--		FROM CMMC_REports.dbo.Attendance_Details_ExtraFields tbl_att_dtl_exfd

--		WHERE tbl_att_dtl_exfd.atd_EF_FieldID = 1666 -- Code used to identify Ambulance Arrival Times
--		)tbl_AmbulanceArrivalTime ON tbl_AmbulanceArrivalTime.atd_EF_atdid  = Encounter.atd_id


--A+E reattended within 7 days
--CCB 2012-02-07 Optimised
LEFT JOIN (
	select
		 atd_EF_atdid
		,UnplannedReattend7Day =
			case atd_EF_Value
			when '17166' then 0 -- Recorded as No
			when '17167' then 1 -- Recorded as Yes
			else 2  -- Recorded as something else
			end	
		,SequenceNo =
			row_number() over (partition by atd_EF_atdid order by atd_EF_extrafieldID desc)
	from
		[$(Symphony)].dbo.Attendance_Details_ExtraFields AttendanceDetailsExtraFields
	where
		AttendanceDetailsExtraFields.atd_EF_FieldID IN ( '1667' , '1917' ) -- Code used to identify Attended for same reason within 7 days
	) UnplannedReattend
on	UnplannedReattend.atd_EF_atdid = Encounter.atd_id
and	UnplannedReattend.SequenceNo = 1


--Ambulance Arrival Times
--CCB 2012-02-07 Optimised
LEFT JOIN (
	select
		 atd_EF_atdid
		,AmbulanceArrivalTimeInt = replace(ltrim(rtrim(atd_EF_Value)), ':', '')
		,SequenceNo =
			row_number() over (partition by atd_EF_atdid order by atd_EF_extrafieldID desc)
	from
		[$(Symphony)].dbo.Attendance_Details_ExtraFields AttendanceDetailsExtraFields
	where
		AttendanceDetailsExtraFields.atd_EF_FieldID = 1666 -- Code used to identify Ambulance Arrival Times
	) AmbulanceArrivalTime
ON	AmbulanceArrivalTime.atd_EF_atdid  = Encounter.atd_id
and	Encounter.atd_arrmode = '6477' -- arrived by Ambulance
and	AmbulanceArrivalTime.SequenceNo = 1


--Clinical Assessment Time
left join (
	select
		 SourceUniqueID = Result.res_atdid
		,ClinicalAssessmentTime = Result.res_date
	from
		[$(Symphony)].dbo.Result_details Result
	where
		Result.res_depid = 192
	and	Result.res_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Result_details PreviousResult
		where
			PreviousResult.res_atdid = Result.res_atdid
		and	PreviousResult.res_depid = 192
		and	PreviousResult.res_inactive <> 1
		and	(
				PreviousResult.res_date < Result.res_date
			or	(
					PreviousResult.res_date = Result.res_date
				and	PreviousResult.res_resid < Result.res_resid
				)
			)
		)
	) ClinicalAssessment
on	ClinicalAssessment.SourceUniqueID = Encounter.atd_id



--Rapid Assessment Time
left join (
	select
		 SourceUniqueID = RapidResult.res_atdid
		,RapidAssessmentTime = RapidResult.res_date
	from
		[$(Symphony)].dbo.Result_details  RapidResult
	where
		RapidResult.res_depid = 223
	and	RapidResult.res_inactive <> 1
	and not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Result_details PreviousRapidResult
		where
			PreviousRapidResult.res_atdid = RapidResult.res_atdid
		and	PreviousRapidResult.res_depid = 223
		and	PreviousRapidResult.res_inactive <> 1
		and	(
				PreviousRapidResult.res_date < RapidResult.res_date
			or	(
					PreviousRapidResult.res_date = RapidResult.res_date
				and	PreviousRapidResult.res_resid < RapidResult.res_resid
				)
			)
		)
	) RapidAssessment
on	RapidAssessment.SourceUniqueID = Encounter.atd_id

--drinking location			
left join 
	(
	select 
		AlcoholLocation.atd_EF_atdid
		,AlcoholLocation = l.Lkp_Name
	from 
		[$(Symphony)].dbo.Attendance_Details_ExtraFields AlcoholLocation
	inner join [$(Symphony)].dbo.Lookups l on l.Lkp_ID = AlcoholLocation.atd_EF_Value
	where 
		AlcoholLocation.atd_EF_FieldID = 1912
	and	not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Attendance_Details_ExtraFields LatestAlcoholLocation
		where
			LatestAlcoholLocation.atd_EF_FieldID = 1912
		and	LatestAlcoholLocation.atd_EF_atdid = AlcoholLocation.atd_EF_atdid
		and	LatestAlcoholLocation.atd_EF_extrafieldID > AlcoholLocation.atd_EF_extrafieldID
		)
	)AlcoholLocation 
on AlcoholLocation.atd_EF_atdid = Encounter.atd_id	


--drink past 12 hours
left join 
	(
	select 
		Alcohol.atd_EF_atdid
		,AlcoholInPast12Hours = l.Lkp_Name 
	from 
		[$(Symphony)].dbo.Attendance_Details_ExtraFields Alcohol
	inner join [$(Symphony)].dbo.Lookups l on l.Lkp_ID = atd_EF_Value
	where 
		atd_EF_FieldID = 1621
	and	not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Attendance_Details_ExtraFields LatestAlcohol
		where
			LatestAlcohol.atd_EF_FieldID = 1621
		and	LatestAlcohol.atd_EF_atdid = Alcohol.atd_EF_atdid
		and	LatestAlcohol.atd_EF_extrafieldID > Alcohol.atd_EF_extrafieldID
		)
	)AlcoholInPast12Hours 
on AlcoholInPast12Hours.atd_EF_atdid = Encounter.atd_id	

left join
	(
	select distinct
		 ComplaintDetailsID = res_atdid
		,ComplaintDetails = not_text
	from
		[$(Symphony)].dbo.Notes Notes

		inner join [$(Symphony)].dbo.Result_details  ResultDetails
		on Notes.not_noteid = ResultDetails.res_note
		and ResultDetails.res_inactive <> 1 and ResultDetails.res_depid = 192
	where

		not exists
		(
		select
			1
		from
			[$(Symphony)].dbo.Result_details LastResultDetails
		where
		LastResultDetails.res_inactive <> 1 and LastResultDetails.res_depid = 192
		and	LastResultDetails.res_atdid = ResultDetails.res_atdid
		and	LastResultDetails.res_date < ResultDetails.res_date
		)

	) ComplaintDetails
on	ComplaintDetails.ComplaintDetailsID = Encounter.atd_id

where


	Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to) 
and	Encounter.atd_deleted = 0
and	Episode.epd_deleted = 0

-- only the attendances from after the switchover to Symphony
and (
		(
			Department.SiteCode = 'RW3SM'
		and	Encounter.atd_arrivaldate >= '11 Apr 2011 11:42'
		)
	or	(
			Department.SiteCode = 'RW3RC'
		and	Encounter.atd_arrivaldate >= '20 Oct 2009 09:10'
		)
	or	(
			Department.SiteCode = 'RW3MR'
		and	Encounter.atd_arrivaldate >= '20 Oct 2009 10:56'
		)

	or	(
			Department.SiteCode = 'RW3RE'
		and	Encounter.atd_arrivaldate >= '11 Sep 2010 00:00'
		)
	or	(
			Department.SiteCode = 'RW3DH'
		and	Encounter.atd_arrivaldate >= '11 Oct 2011 00:00'
		)
	)
and   Encounter.atd_id not in (1665028)

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractSymphonyAEEncounter', @Stats, @StartTime





﻿



CREATE proc [dbo].[p_Schedule5ExtractAE]

as

set dateformat dmy

declare @start datetime
		,@end datetime
		
		
set @start = dateadd(dd, datediff(dd, 0, dateadd(dd, - 1, getdate())), 0)
set @end = dateadd(mi, -1, dateadd(dd, 1, @start))

--set @start = '25 august 2012'
--set @end = '26 august 2012'
		
select
	 [Attendance ID] = AttendanceNumber 
	,[NHS Number] = isnull(NHSNumber,'')
	,[Registered GP Practice Code] = isnull(RegisteredGpPracticeCode,'')
	,[Arrival Date] = ArrivalTime 
	,[Provider Code] = SiteCode
	,[Source Of Referral] = isnull(SourceOfReferralCode,'')
	,[Local PAS ID] = isnull(DistrictNo,'') 
	,[Interface Code] = isnull(InterfaceCode,'')

from 
	AE.Encounter
where 
	ArrivalDate between @start and @end
	and PCTCode = '5NT'
	




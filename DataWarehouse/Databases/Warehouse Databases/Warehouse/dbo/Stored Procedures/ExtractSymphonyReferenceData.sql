﻿CREATE procedure [dbo].[ExtractSymphonyReferenceData] as


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

--Lookups
delete from AE.LookupBase

insert into AE.LookupBase
(
	 Lkp_ID
	,Lkp_ParentID
	,Lkp_Name
	,lkp_active
	,Lkp_DisplayOrder
	,Lkp_InAscOrder
	,Lkp_Used
	,Lkp_Datecreated
	,Lkp_System
	,Lkp_FlatLookupID
	,lkp_deptid
	,Lkp_createdby
	,Lkp_update
	,lkp_refreshed
	,lkp_deleted
	,lkp_IsSubAnalysed
	,lkp_TableID
)
select
	 Lkp_ID
	,Lkp_ParentID
	,Lkp_Name
	,lkp_active
	,Lkp_DisplayOrder
	,Lkp_InAscOrder
	,Lkp_Used
	,Lkp_Datecreated
	,Lkp_System
	,Lkp_FlatLookupID
	,lkp_deptid
	,Lkp_createdby
	,Lkp_update
	,lkp_refreshed
	,lkp_deleted
	,lkp_IsSubAnalysed
	,lkp_TableID
from
	[$(Symphony)].dbo.Lookups


--Discharge Destination
delete from AE.DischargeDestination

insert into AE.DischargeDestination
	(
	 DischargeDestinationCode
	,DischargeDestination
	)
select
	 DischargeDestinationCode = DischargeDestination.Lkp_ID
	,DischargeDestination = DischargeDestination.Lkp_Name
from
	[$(Symphony)].dbo.Lookups DischargeDestination
where
	DischargeDestination.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		Lkp_Name like '% Wards'
	)

--Specialty
delete from AE.Specialty

insert into AE.Specialty
	(
	 SpecialtyCode
	,Specialty
	)

select
	 SpecialtyCode = Specialty.Lkp_ID
	,Specialty = Specialty.Lkp_Name
from
	[$(Symphony)].dbo.Lookups Specialty
where
	Specialty.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		Lkp_Name in
			(
			 'Specialties (PED)'
			,'Specialties (ED)'
			)
	)


--is this mental health
update
	AE.Specialty
set
	IsMentalHealth =
	case
	when MentalHealth.EntityCode is null
	then 0
	else 1
	end
from
	AE.Specialty

left join dbo.EntityLookup MentalHealth
on	MentalHealth.EntityCode = Specialty.SpecialtyCode
and	MentalHealth.EntityTypeCode = 'MENTALHEALTHSPECIALTY'


-- Staff

delete from AE.StaffMember

INSERT INTO [AE].[StaffMember]
	(
	 StaffMemberCode
	,Surname
	,Forename
	,MiddleNames
	,Title
	,JobTypeCode
	,StartDate
	,EndDate
	,SpecialtyCode
	,JobTitle
	,LoginId 
	)

select
	 StaffMemberCode = stf_staffid
	,Surname = stf_surname
	,Forename = stf_forename
	,MiddleNames = stf_midnames
	,Title = Title.Lkp_Name
	,JobTypeCode = stf_jobtype
	,StartDate = stf_startdate
	,EndDate = stf_enddate
	,SpecialtyCode = stf_specialty
	,JobTitle = stf_jobtitle
	,LoginId = stf_logonid
from
	[$(Symphony)].dbo.Staff
inner join  [$(Symphony)].dbo.Lookups Title
on	Title.Lkp_ID = Staff.stf_title


--Attendance Disposal
delete from AE.AttendanceDisposal

insert into AE.AttendanceDisposal
	(
	 AttendanceDisposalCode
	,AttendanceDisposal
	,AttendanceDisposalNationalCode
	)
select
	 AttendanceDisposalCode = AttendanceDisposal.Lkp_ID
	,AttendanceDisposal = AttendanceDisposal.Lkp_Name
	,AttendanceDisposalNationalCode = 
		(
		select distinct
			 DisposalCode = Notes.not_text
		from
			[$(Symphony)].dbo.Notes Notes

		inner join [$(Symphony)].dbo.LookupMappings LookupMapping
		on	LookupMapping.flm_value = Notes.not_noteid
		and	LookupMapping.flm_mtid = 
			(
			select
				mt_id
			from
				[$(Symphony)].dbo.MappingTypes 
			where
				mt_name = 'CDS'
			)
		where
			LookupMapping.flm_lkpid = AttendanceDisposal.Lkp_ID
		)
from
	[$(Symphony)].dbo.Lookups AttendanceDisposal
where
	AttendanceDisposal.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		Lkp_Name in 
		(
		 'Disposal'
		,'Ref from A&E'
		,'Disposal (SMH)'  --GS 20110729
		,'Clinic Discharge'--GS 20110729
		,'Rapid Discharge Outcome'--GS 20110729
		,'Disposal (DEN)'
		)
	)


--Attendance Disposal Destination
delete from AE.AttendanceDisposalDestination

insert into AE.AttendanceDisposalDestination
	(
	 AttendanceDisposalDestinationCode
	,AttendanceDisposalDestination
	,AttendanceDisposalDestinationNationalCode
	)
select
	 AttendanceDisposalDestinationCode = AttendanceDisposalDestination.Lkp_ID
	,AttendanceDisposalDestination = AttendanceDisposalDestination.Lkp_Name
	,AttendanceDisposalDestinationNationalCode = 
		(
		select distinct
			 DisposalDestinationCode = Notes.not_text
		from
			[$(Symphony)].dbo.Notes Notes

		inner join [$(Symphony)].dbo.LookupMappings LookupMapping
		on	LookupMapping.flm_value = Notes.not_noteid
		and	LookupMapping.flm_mtid = 
			(
			select
				mt_id
			from
				[$(Symphony)].dbo.MappingTypes 
			where
				mt_name = 'CDS'
			)
		where
			LookupMapping.flm_lkpid = AttendanceDisposalDestination.Lkp_ID
		)
from
	[$(Symphony)].dbo.Lookups AttendanceDisposalDestination
where
	AttendanceDisposalDestination.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		Lkp_Name in
		(
		 'Discharge Destination'
		,'MRI Ward'
		,'RMCH Wards'
		)
	)


--ArrivalMode
delete from AE.ArrivalMode

insert into AE.ArrivalMode
	(
	 ArrivalModeCode
	,ArrivalMode
	,ArrivalModeNationalCode
	)
select
	 ArrivalModeCode = ArrivalMode.Lkp_ID
	,ArrivalMode = ArrivalMode.Lkp_Name
	,ArrivalModeNationalCode = 
		(
		select
			 right(not_text, 1) 
		from
			[$(Symphony)].dbo.Notes

		inner join [$(Symphony)].dbo.LookupMappings
		on flm_value = not_noteid
		and	flm_mtid = 
			(
			select
				mt_id
			from
				[$(Symphony)].dbo.MappingTypes 
			where
				mt_name = 'CDS'
			)
		where
			flm_lkpid = ArrivalMode.Lkp_ID
		)
from
	[$(Symphony)].dbo.Lookups ArrivalMode
where
	ArrivalMode.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		Lkp_Name = 'Arrival Modes'
	)



--EthnicCategory
delete from AE.EthnicCategory

insert into AE.EthnicCategory
	(
	 EthnicCategoryCode
	,EthnicCategory
	,EthnicCategoryNationalCode
	)
select
	 EthnicCategoryCode = EthnicCategory.Lkp_ID
	,EthnicCategory = EthnicCategory.Lkp_Name
	,EthnicCategoryNationalCode = 
		(
		select
			 right(not_text, 1) 
		from
			[$(Symphony)].dbo.Notes

		inner join [$(Symphony)].dbo.LookupMappings
		on flm_value = not_noteid
		and	flm_mtid = 
			(
			select
				mt_id
			from
				[$(Symphony)].dbo.MappingTypes 
			where
				mt_name = 'CDS'
			)
		where
			flm_lkpid = EthnicCategory.Lkp_ID
		)
from
	[$(Symphony)].dbo.Lookups EthnicCategory
where
	EthnicCategory.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		Lkp_Name = 'Ethnic Groups'
	)

--ReligionCategory
delete from AE.Religion

insert into AE.Religion
	(
	 ReligionCode
	,Religion
	,ReligionNationalCode
	)


select
	 ReligionCode = ReligionCategory.Lkp_ID
	,Religion = ReligionCategory.Lkp_Name
	,ReligionNationalCode = 
		(
		select
			 right(not_text, 1) 
		from
			[$(Symphony)].dbo.Notes

		inner join [$(Symphony)].dbo.LookupMappings
		on flm_value = not_noteid
		and	flm_mtid = 
			(
			select
				mt_id
			from
				[$(Symphony)].dbo.MappingTypes 
			where
				mt_name = 'CDS'
			)
		where
			flm_lkpid = ReligionCategory.Lkp_ID
		)
from
	[$(Symphony)].dbo.Lookups ReligionCategory
where
	ReligionCategory.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		Lkp_Name = 'Religions'
	)

	
--PresentingProblem
delete from AE.PresentingProblem

insert into AE.PresentingProblem
	(
	 PresentingProblemCode
	,PresentingProblem
	,PresentingProblemNationalCode
	)

select
	 PresentingProblemCode = PresentingProblem.Lkp_ID
	,PresentingProblem = PresentingProblem.Lkp_Name
	,PresentingProblemNationalCode = 
			(
			select
					right(not_text, 1) 
			from
				[$(Symphony)].dbo.Notes

			inner join [$(Symphony)].dbo.LookupMappings
			on flm_value = not_noteid
			and	flm_mtid = 
				(
				select
					mt_id
				from
					[$(Symphony)].dbo.MappingTypes 
				where
					mt_name = 'CDS'
				)
			where
				flm_lkpid = PresentingProblem.Lkp_ID
			)

from
	[$(Symphony)].dbo.Lookups PresentingProblem
where
	PresentingProblem.Lkp_ParentID in (
	select
		Lkp_ID
	from
		[$(Symphony)].dbo.Lookups
	where
		left(Lkp_Name,10) ='Complaints'
	)



--SourceOfReferral
--delete from AE.SourceOfReferral

--insert into AE.SourceOfReferral
--	(
--	 SourceOfReferralCode
--	,SourceOfReferral
--	,NationalSourceOfReferralCode
--	)
--select
--	 SourceOfReferralCode = SourceOfReferral.Lkp_ID
--	,SourceOfReferral = SourceOfReferral.Lkp_Name
--	,NationalSourceOfReferralCode = 
--								(
--								select
--									 NationalSourceOfReferralCode = Notes.not_text
--								from
--									[$(Symphony)].dbo.notes Notes

--								inner join [$(Symphony)].dbo.LookupMappings LookupMapping
--								on	LookupMapping.flm_value = Notes.not_noteid
--								and	LookupMapping.flm_mtid = 
--									(
--									select
--										mt_id
--									from
--										[$(Symphony)].dbo.MappingTypes 
--									where
--										mt_name = 'CDS'
--									)
--								where
--									LookupMapping.flm_lkpid = SourceOfReferral.Lkp_ID
--								)

--from
--	[$(Symphony)].dbo.Lookups SourceOfReferral
--where
--	SourceOfReferral.Lkp_ParentID in 
--										(
--										select
--											Lkp_ID
--										from
--											[$(Symphony)].dbo.Lookups
--										where
--											Lkp_name = 'Source of Referral'
--										)


;
with SourceDiagnosis
as
(
select
	SourceDiagnosisCode = Lkp_ID
	,SourceDiagnosis = Lkp_Name
from
	[$(Symphony)].dbo.Lookups
where
	Lkp_ParentID in 
					(
					9147
					,11240
					,11540
					,16061
					,16095
					,17334
					,17581
					,17599
					,17606
					)

union all

select
	SourceDiagnosisCode = Lkp_ID
	,SourceDiagnosis = Lkp_Name
from
	[$(Symphony)].dbo.Lookups
inner join SourceDiagnosis
on SourceDiagnosis.SourceDiagnosisCode = Lookups.Lkp_ParentID
)


insert into AE.SourceDiagnosisBase
(
SourceDiagnosisCode
,SourceDiagnosis
)

select
	SourceDiagnosisCode
	,SourceDiagnosis
from
	SourceDiagnosis
where
	not exists
			(
			select
				1
			from
				AE.SourceDiagnosisBase
			where
				SourceDiagnosisBase.SourceDiagnosisCode = SourceDiagnosis.SourceDiagnosisCode
			)

;
with SourceInvestigation
as
(
select
	SourceInvestigationCode = Lkp_ID
	,SourceInvestigation = Lkp_Name
from
	[$(Symphony)].dbo.Lookups
where
	Lkp_ParentID in 
					(
					7681
					,7687
					,17407
					,17794
					,18328
					)
union all

select
	SourceInvestigationCode = Lkp_ID
	,SourceInvestigation = Lkp_Name
from
	[$(Symphony)].dbo.Lookups
inner join SourceInvestigation
on SourceInvestigation.SourceInvestigationCode = Lookups.Lkp_ParentID
)

insert into AE.SourceInvestigationBase
(
SourceInvestigationCode
,SourceInvestigation
)

select
	SourceInvestigationCode
	,SourceInvestigation
from
	SourceInvestigation
where
	not exists
			(
			select
				1
			from
				AE.SourceInvestigationBase
			where
				SourceInvestigationBase.SourceInvestigationCode = SourceInvestigation.SourceInvestigationCode
			)
					
;
with DischargeDrug
as
(
select
	DrugCode = Lkp_ID
	,Drug = Lkp_Name
from
	[$(Symphony)].dbo.Lookups
where
	Lkp_ParentID in 
					(
					10591
					,10891
					,11133
					,15941
					,17676
					)
union all

select
	DrugCode = Lkp_ID
	,Drug = Lkp_Name
from
	[$(Symphony)].dbo.Lookups
inner join DischargeDrug
on DischargeDrug.DrugCode = Lookups.Lkp_ParentID
)

insert into AE.DischargeDrugBase
(
DrugCode
,Drug
)

select
	DrugCode
	,Drug
from
	DischargeDrug
where
	not exists
			(
			select
				1
			from
				AE.DischargeDrugBase
			where
				DischargeDrugBase.DrugCode = DischargeDrug.DrugCode
			)

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'ExtractSymphonyReferenceData', @Stats, @StartTime

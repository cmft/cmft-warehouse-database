﻿


CREATE proc [dbo].[p_Schedule5ExtractAPC]

as

set dateformat dmy

declare @start datetime
		,@end datetime
		
set @start = dateadd(dd, datediff(dd, 0, dateadd(dd, - 2, getdate())), 0)
set @end = dateadd(mi, -1, dateadd(dd, 1, @start))

--set @start = '6 june 2011'
--set @end = '6 june 2011'

--select @start, @end
	

select 
	[Admission ID] = isnull(CasenoteNumber, '')
	,[NHS Number] = isnull(NHSNumber, '')
	,[Registered GP Practice Code] = isnull(RegisteredGpPracticeCode, '')
	,[Specialty Code (Admission)] = isnull(NationalSpecialtyCode, '')
	,[Admission Date] = isnull(AdmissionTime, '')
	,[Discharge Date] = isnull(DischargeTime, '')
	,[Discharge Destination] = isnull(NationalDischargeDestinationCode, '')
	,[Discharge Method] = isnull(NationalDischargeMethodCode, '')
	,[Provider Code] = 'RW3'
	,[Local Patient ID] = isnull(SourcePatientNo, '')
from 
	APC.Encounter e
inner join PAS.AdmissionMethod am
on e.AdmissionMethodCode = am.AdmissionMethodCode
inner join PAS.Specialty s
on e.SpecialtyCode = s.SpecialtyCode
left outer join PAS.DischargeDestination dd
on e.DischargeDestinationCode = dd.DischargeDestinationCode
left outer join PAS.DischargeMethod dm
on e.DischargeMethodCode = dm.DischargeMethodCode

where 
	(AdmissionDate between @start and @end
	or DischargeDate between @start and @end)
	and PCTCode = '5NT'
	and LocalAdminCategoryCode <> 'PAY'
	and NationalAdmissionMethodCode between 21 And 29
	



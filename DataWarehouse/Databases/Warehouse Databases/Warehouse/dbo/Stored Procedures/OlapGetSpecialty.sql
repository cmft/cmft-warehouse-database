﻿CREATE procedure [dbo].[OlapGetSpecialty]
as

declare @openquery as varchar(4000)
declare @select as varchar(4000)

select
	@openquery =
		'
		with 

		member [Measures].[Name] as 
			[PAS Specialty].[Specialty].currentmember.name 

		member [Measures].[Unique Name] as 
			[PAS Specialty].[Specialty].currentmember.uniquename 

		member [Measures].[Unique Key] as 
			[PAS Specialty].[Specialty].currentmember.properties("KEY") 

		set [Measures] as 
			{
			 [Measures].[Name]
			,[Measures].[Unique Name]
			,[Measures].[Unique Key]
			} 

		set [Set] as 
			order(
				filter(
					 [PAS Specialty].[Specialty].members
					,[Measures].[APC Waiters] + [Measures].[OP Cases] > 0
				)
				,[PAS Specialty].[Specialty].currentmember.name
				,asc
			) 

		select 
			 [Measures] on 0
			,[Set] on 1 
		 from 
			[Warehouse]
		where
			(
			[Census].[Census].members
			)
		'

select
	@select =
		'
		select
			 SpecialtyCode = case when "[Measures].[Unique Key]" = ''0'' then null else "[Measures].[Unique Key]" end
			,Specialty = "[Measures].[Name]"
			,SpecialtyUniqueName = "[Measures].[Unique Name]"
		from
			openquery(Warehouse, ''
		'

exec (@select + @openquery + ''')')

﻿
CREATE  procedure [dbo].[LoadBedman] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as 

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


exec dbo.ExtractBedmanAcuityLevelWTE
exec dbo.ExtractBedmanDailyAcuityScore
exec dbo.LoadBedmanAcuity 


exec dbo.ExtractBedmanPDDReasons
exec dbo.LoadBedmanPDDReasons

update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADBEDMANDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADBEDMANDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadBedman', @Stats, @StartTime


﻿
CREATE PROCEDURE [dbo].[LoadCOMLookupBase] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the TImport table

TRUNCATE TABLE COM.LookupBase

-- Insert new rows into encounter destination table
INSERT INTO COM.LookupBase
(
	 LookupID --[RFVAL_REFNO]
	,LookupTypeID --[RFVDM_CODE]
	,LookupDescription --[DESCRIPTION]
	,LookupCode --[MAIN_CODE]
	,LookupNHSCode
	,LookupCDSCode
	,LookupNatCode
	,ArchiveFlag --[ARCHV_FLAG]
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
) 
SELECT
	 LookupID --[RFVAL_REFNO]
	,LookupTypeID --[RFVDM_CODE]
	,LookupDescription --[DESCRIPTION]
	,LookupCode --[MAIN_CODE]
	,LookupNHSCode
	,LookupCDSCode
	,LookupNatCode
	,ArchiveFlag --[ARCHV_FLAG]
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
FROM
	dbo.TLoadCOMLookupBase


select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' 

exec WriteAuditLogEvent 'LoadCOMLookup', @Stats, @StartTime;

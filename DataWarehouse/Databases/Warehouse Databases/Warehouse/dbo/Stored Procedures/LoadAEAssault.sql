﻿
CREATE procedure [dbo].[LoadAEAssault] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()


delete from AE.Assault

select @RowsDeleted = @@rowcount

INSERT INTO AE.Assault
(
		[SourceUniqueID] -- NULL
		,AESourceUniqueID
		,AssaultDate
		,AssaultTime
		,AssaultWeapon
		,AssaultWeaponDetails
		,AssaultLocation
		,AssaultLocationDetails
		,AlcoholConsumed3Hour
		,AssaultRelationship
		,AssaultRelationshipDetails
)
   

SELECT
		[SourceUniqueID]
		,AESourceUniqueID 
		,AssaultDate
		,AssaultTime
		,AssaultWeapon
		,AssaultWeaponDetails
		,AssaultLocation
		,AssaultLocationDetails
		,AlcoholConsumed3Hour
		,AssaultRelationship
		,AssaultRelationshipDetails
FROM
	TLoadAEAssault



select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadAEAssault', @Stats, @StartTime
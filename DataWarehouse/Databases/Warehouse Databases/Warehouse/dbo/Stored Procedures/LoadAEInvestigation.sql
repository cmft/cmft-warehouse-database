﻿CREATE procedure [dbo].[LoadAEInvestigation]
	 @from smalldatetime
	,@to smalldatetime
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

delete from AE.Investigation
where
	exists
	(
	select
		1
	from
		AE.Encounter
	where
		coalesce(Encounter.ArrivalDate, @from) between @from and @to
	and	Encounter.SourceUniqueID = Investigation.AESourceUniqueID
	)

select @RowsDeleted = @@rowcount

delete from AE.Investigation
where
	not exists
	(
	select
		1
	from
		AE.Encounter
	where
		Encounter.SourceUniqueID = Investigation.AESourceUniqueID
	)

select @RowsDeleted = @RowsDeleted + @@rowcount

insert into AE.Investigation
(
	 SourceUniqueID
	,InvestigationDate
	,SequenceNo
	,InvestigationCode
	,AESourceUniqueID
	,SourceInvestigationCode
	,ResultDate
)
select
	 SourceUniqueID
	,InvestigationDate
	,SequenceNo
	,InvestigationCode
	,AESourceUniqueID
	,SourceInvestigationCode
	,ResultDate
from
	dbo.TLoadAEInvestigation

select @RowsInserted = @@rowcount

--populate the AEEncounter Investigation codes
update
	AE.Encounter
set
	InvestigationCodeFirst = 
	(
	select
		case
		when AEInvestigation.InvestigationCode is null then null
		when AEInvestigation.InvestigationCode = '' then null
		else AEInvestigation.InvestigationCode
		end
	from
		AE.Investigation AEInvestigation
	where
		AEInvestigation.AESourceUniqueID = Encounter.SourceUniqueID
	and	AEInvestigation.SequenceNo = 1
	and	not exists
		(
		select
			1
		from
			AE.Investigation Previous
		where
			Previous.AESourceUniqueID = Encounter.SourceUniqueID
		and	Previous.SequenceNo = 1
		and	Previous.SourceUniqueID < AEInvestigation.SourceUniqueID
		)
	)

	,InvestigationCodeSecond = 
	(
	select
		case
		when AEInvestigation.InvestigationCode is null then null
		when AEInvestigation.InvestigationCode = '' then null
		else AEInvestigation.InvestigationCode
		end
	from
		AE.Investigation AEInvestigation
	where
		AEInvestigation.AESourceUniqueID = Encounter.SourceUniqueID
	and	AEInvestigation.SequenceNo = 2
	and	not exists
		(
		select
			1
		from
			AE.Investigation Previous
		where
			Previous.AESourceUniqueID = Encounter.SourceUniqueID
		and	Previous.SequenceNo = 2
		and	Previous.SourceUniqueID < AEInvestigation.SourceUniqueID
		)
	)
from
	dbo.TLoadAEInvestigation AEInvestigation
where
	AEInvestigation.AESourceUniqueID = Encounter.SourceUniqueID


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadAEInvestigation', @Stats, @StartTime

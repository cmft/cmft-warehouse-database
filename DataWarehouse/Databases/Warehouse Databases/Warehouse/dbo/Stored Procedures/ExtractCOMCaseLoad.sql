﻿
CREATE PROCEDURE [dbo].[ExtractCOMCaseLoad]
	
AS


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

SELECT  @lastmodifiedTime = coalesce(MAX(CONVERT(datetime, ModifiedTime)) 
,'01/01/1900'
				)
FROM COM.CaseLoad

TRUNCATE TABLE dbo.TImportCOMCaseLoad

INSERT INTO dbo.TImportCOMCaseLoad
(
	 [CaseLoadID]
	,[ReferralID]
	,[RoleTypeID]
	,[StatusID]
	,AllocationTime
	,AllocationReasonID
	,InterventionlevelID
	,DischargeTime
	,Outcome
	,AllocationProfessionalCarerID
	,AllocationSpecialtyID
	,AllocationStaffTeamID
	,ResponsibleHealthOrganisation
	,DischargeReasonID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,[CreatedTime]
	,[ModifiedTime]
	,[CreatedByID]
	,[ModifiedByID]
	,[ArchiveFlag]
	,[HealthOrgOwnerID]
	) 

SELECT
	 [CaseLoadID] = Caseload.[CLENT_REFNO]
	,[ReferralID] = Caseload.[REFRL_REFNO]
	,[RoleTypeID]  = Caseload.[CRTYP_REFNO]
	,[StatusID] = Caseload.[CLSTA_REFNO]
	,AllocationTime = Caseload.[ALLOC_DTTM]
	,AllocationReasonID = Caseload.[CLRSN_REFNO]
	,InterventionlevelID = Caseload.[INTLV_REFNO]
	,DischargeTime = Caseload.[DISCHARGE_DTTM]
	,Outcome = Caseload.[CLOCM_REFNO]
	,AllocationProfessionalCarerID = Caseload.[PROCA_REFNO]
	,AllocationSpecialtyID = Caseload.[SPECT_REFNO]
	,AllocationStaffTeamID = Caseload.[STEAM_REFNO]
	,ResponsibleHealthOrganisation = Caseload.[RESPTO_HEORG_REFNO]
	,DischargeReasonID = Caseload.[CSDDR_REFNO]
	,PatientSourceID = Caseload.[PATNT_REFNO]
	,PatientSourceSystemUniqueID = Patient.PASID
	,PatientNHSNumber = Patient.NHS_IDENTIFIER
	,PatientNHSNumberStatusIndicator = 1
	,PatientTitleID = Patient.TITLE_REFNO
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,PatientAddress1 = CaseloadPatientAddress.LINE1
	,PatientAddress2 = CaseloadPatientAddress.LINE2
	,PatientAddress3 = CaseloadPatientAddress.LINE3
	,PatientAddress4 = CaseloadPatientAddress.LINE4
	,PatientPostcode = CaseloadPatientAddress.PCODE
	,PatientDateOfBirth = Patient.DATE_OF_BIRTH
	,PatientDateOfDeath = Patient.DATE_OF_DEATH
	,PatientSexID = Patient.SEXXX_REFNO
	,PatientEthnicGroupID = Patient.ETHGR_REFNO
	,PatientCurrentRegisteredPracticeCode =  ProfessionalCarerBase.HealthOrgCode
	,PatientEncounterRegisteredPracticeCode = ProfessionalCarerBase.HealthOrgCode
	,PatientCurrentRegisteredGPCode = ProfessionalCarerBase.ProfessionalCarerCode
	,PatientEncounterRegisteredGPCode = ProfessionalCarerBase.ProfessionalCarerCode
	,[CreatedTime] = Caseload.[CREATE_DTTM]
	,[ModifiedTime] = Caseload.[MODIF_DTTM]
	,[CreatedByID] = Caseload.[USER_CREATE]
	,[ModifiedByID] = Caseload.[USER_MODIF]
	,[ArchiveFlag] = Caseload.[ARCHV_FLAG]
	,[HealthOrgOwnerID] = Caseload.[OWNER_HEORG_REFNO]
FROM 
	[$(ipm)].dbo.Caseload

LEFT OUTER JOIN [$(ipm)].dbo.Patient
ON Caseload.PATNT_REFNO = Patient.PATNT_REFNO

LEFT OUTER JOIN
(
	SELECT DISTINCT
		 EncounterPatientAddress.CLENT_REFNO
		,EncounterPatientAddress.ADDSS_REFNO
		,PatientAddressDetails.LINE1
		,PatientAddressDetails.LINE2
		,PatientAddressDetails.LINE3
		,PatientAddressDetails.LINE4
		,PatientAddressDetails.PCODE
	FROM
		(
		SELECT 
			 CLENT_REFNO
			,MAX(ADDSS_REFNO) AS ADDSS_REFNO
		FROM 
			[$(ipm)].dbo.Caseload

		LEFT JOIN
			(
			SELECT
				 ADDRESS_ROLES_V01.ADDSS_REFNO
				,ADDRESS_ROLES_V01.START_DTTM
				,ADDRESS_ROLES_V01.END_DTTM
				,ADDRESS_ROLES_V01.PATNT_REFNO
					
			FROM 
				[$(ipm)].dbo.ADDRESS_ROLES_V01 
					
			WHERE
				ROTYP_CODE = 'HOME'
			AND ARCHV_FLAG = 'N'
			AND NOT EXISTS
				(
				SELECT
					*
				FROM
					[$(ipm)].dbo.ADDRESS_ROLES_V01 PreviousAddressRole
				WHERE
					PreviousAddressRole.ROLES_REFNO = ADDRESS_ROLES_V01.ROLES_REFNO
				AND PreviousAddressRole.MODIF_DTTM > ADDRESS_ROLES_V01.MODIF_DTTM
				AND ROTYP_CODE = 'HOME'
				AND ARCHV_FLAG = 'N'
				)
			) PatientAddress

		ON	Caseload.PATNT_REFNO = PatientAddress.PATNT_REFNO
		AND PatientAddress.START_DTTM <= Caseload.ALLOC_DTTM 
		AND (
				PatientAddress.END_DTTM IS NULL
			OR PatientAddress.END_DTTM > Caseload.ALLOC_DTTM
			)

		GROUP BY 
			CLENT_REFNO
		) EncounterPatientAddress
	
	LEFT OUTER JOIN 
		(	
		SELECT DISTINCT
			 ADDSS_REFNO
			,LINE1
			,LINE2
			,LINE3
			,LINE4
			,PCODE
		FROM
			[$(ipm)].dbo.ADDRESSES_V01 		 
		WHERE
			ADTYP_CODE = 'POSTL'
		AND ARCHV_FLAG = 'N'
		AND NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.ADDRESSES_V01 previousaddress
				WHERE
					previousaddress.ADDSS_REFNO = ADDRESSES_V01.ADDSS_REFNO
				AND previousaddress.MODIF_DTTM > ADDRESSES_V01.MODIF_DTTM
				AND ADTYP_CODE = 'POSTL'
				AND ARCHV_FLAG = 'N'
				)
		) PatientAddressDetails
	ON EncounterPatientAddress.ADDSS_REFNO = PatientAddressDetails.ADDSS_REFNO
	) CaseloadPatientAddress
ON Caseload.CLENT_REFNO = CaseloadPatientAddress.CLENT_REFNO

LEFT JOIN
	(
	SELECT DISTINCT
		 EncounterPatientProfessionalCarer.CLENT_REFNO
		,EncounterPatientProfessionalCarer.PATPC_REFNO
		,ProfessionalCarerCode
		,HealthOrgCode
	FROM
		(
		SELECT 
			 CLENT_REFNO
			,MAX(PATPC_REFNO) AS PATPC_REFNO
		FROM 
			[$(ipm)].dbo.Caseload

		LEFT JOIN
			(
			SELECT
				 PATIENT_PROF_CARERS_V01.PATPC_REFNO
				,PATIENT_PROF_CARERS_V01.START_DTTM
				,PATIENT_PROF_CARERS_V01.END_DTTM
				,PATIENT_PROF_CARERS_V01.PATNT_REFNO
				,PATIENT_PROF_CARERS_V01.HEORG_REFNO
			FROM 
				[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 		
			WHERE
				ARCHV_FLAG = 'N'
			AND PRTYP_REFNO = '1132'-- General Practitioner
			AND NOT EXISTS
					(
					SELECT
						*
					FROM
						[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
					WHERE
						PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
					AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
					AND ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132' -- General Practitioner
					)
			)PatientProfCarer
		ON	Caseload.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
		AND PatientProfCarer.START_DTTM <= Caseload.ALLOC_DTTM 
		AND (
				PatientProfCarer.END_DTTM IS NULL 
			OR	PatientProfCarer.END_DTTM > Caseload.ALLOC_DTTM
			)

		GROUP BY 
			CLENT_REFNO
		) EncounterPatientProfessionalCarer

	LEFT JOIN
		(
		SELECT
			 PATIENT_PROF_CARERS_V01.PATPC_REFNO
			,PATIENT_PROF_CARERS_V01.START_DTTM
			,PATIENT_PROF_CARERS_V01.END_DTTM
			,PATIENT_PROF_CARERS_V01.PATNT_REFNO
			,PATIENT_PROF_CARERS_V01.HEORG_REFNO
			,PATIENT_PROF_CARERS_V01.PROCA_MAIN_IDENT AS ProfessionalCarerCode
		FROM 
			[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 
		WHERE
			ARCHV_FLAG = 'N'
		AND PRTYP_REFNO = '1132'-- General Practitioner
		AND NOT EXISTS 
				(
				SELECT
					*
				FROM
					[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
				WHERE
					PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
				AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
				AND ARCHV_FLAG = 'N'
				AND PRTYP_REFNO = '1132' -- General Practitioner
				)
		)PatientProfCarerDetail
	ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO

	LEFT JOIN
		(	
		SELECT DISTINCT
			 HEORG_REFNO
			,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS HealthOrgDescription
			,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS HealthOrgCode
		FROM
			[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
		WHERE
			ARCHV_FLAG = 'N'
		AND NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
				WHERE
					previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
				AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
				AND ARCHV_FLAG = 'N'
				)
		) PatientProfCarerHealthOrg
	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO

	) ProfessionalCarerBase
ON	Caseload.CLENT_REFNO = ProfessionalCarerBase.CLENT_REFNO


--WHERE 
--	Caseload.MODIF_DTTM > @lastmodifiedTime;


select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 

exec WriteAuditLogEvent 'ExtractCommunityCaseload', @Stats, @StartTime

﻿



CREATE procedure [dbo].[ExtractBedmanPdd]
	-- @from smalldatetime
	--,@to smalldatetime
	--,@debug bit = 0
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

--Bedman spells
delete from dbo.TImportBedmanPdd

insert into dbo.TImportBedmanPdd
(
		 SourceSpellNo
		,PddHistoryID
		,PddSequence
		,Pdd
		,PddCreated
		,PddPAS
		,PddReasonCode
)


SELECT DISTINCT  SourceSpellNo = tbl_Pdd.SpellID
		,PddHistoryID = tbl_Pdd.PDDHistoryID
		,PddSequence =	ROW_NUMBER() OVER(PARTITION BY tbl_Pdd.SpellID ORDER BY tbl_Pdd.PDDEntered)
		,Pdd = tbl_Pdd.PDD
		,PddCreated = tbl_Pdd.PDDEntered
		,PddPAS =    CASE tbl_Pdd.PDDComment 
						WHEN 'Calculated from PAS length of stay' THEN 1 
						ELSE 0
					 END
		,tbl_Pdd.PDDreason AS PDDReasonCode

FROM    [$(BedmanTest)].dbo.HospSpell tbl_bedmanspell 
		INNER JOIN [$(BedmanTest)].dbo.PDDHistory tbl_Pdd ON tbl_bedmanspell.SpellID = tbl_Pdd.SpellID 

--WHERE  tbl_bedmanspell.DischTime IS NOT NULL

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'ExtractBedmanPdd', @Stats, @StartTime





﻿
CREATE       procedure [dbo].[LoadEDD] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as 

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from dbo.Parameter where Parameter = 'EDDFREEZEDATE')
		,dateadd(month, -3, @to)
	)
/*
truncate table TImportBedmanSpells
exec ExtractBedmanSpells --@from, @to
truncate table bedman.spells
exec LoadBedmanSpells --@from, @to
*/

truncate table TImportEDDSpells
exec ExtractEDDSpells --@from, @to
truncate table EDD.Spells
exec LoadEDDSpells --@from, @to

truncate table TImportBedmanPdd
exec ExtractBedmanPdd --@from, @to
truncate table Bedman.PddHistory
exec LoadBedmanPdd --@from, @to


update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADEDD'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADEDD'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadEDD', @Stats, @StartTime


﻿
CREATE procedure [dbo].[LoadCOMCaseLoad] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the TImport table

DELETE FROM COM.CaseLoad
WHERE EXISTS
  (SELECT CaseLoadID
    FROM dbo.TImportCOMCaseLoad
    WHERE COM.CaseLoad.CaseLoadID = dbo.TImportCOMCaseLoad.CaseLoadID
			AND COM.CaseLoad.ModifiedTime = dbo.TImportCOMCaseLoad.ModifiedTime );

-- Insert new rows into encounter destination table
INSERT INTO COM.CaseLoad
(
		 
	  [CaseLoadID]
	
	,[ReferralID]
	
	,[RoleTypeID]
	
	,[StatusID]
	
	,AllocationTime

	,AllocationDate
	
	,AllocationReasonID
	
	,InterventionlevelID
	
	,DischargeTime

	,DischargeDate
	
	,Outcome
	
	,AllocationProfessionalCarerID
	
	,AllocationSpecialtyID
	
	,AllocationStaffTeamID
	
	,ResponsibleHealthOrganisation
	
	,DischargeReasonID
	
	,PatientSourceID
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator
	
	,PatientTitleID
	
	,PatientForename
	
	,PatientSurname
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,PatientCurrentRegisteredPracticeCode
		
	,PatientEncounterRegisteredPracticeCode
	
	,PatientCurrentRegisteredGPCode
		
	,PatientEncounterRegisteredGPCode
	
	,[CreatedTime]
	
	,[ModifiedTime]
	
	,[CreatedByID]
	
	,[ModifiedByID]
	
	,[ArchiveFlag]
	
	,[HealthOrgOwnerID]
	
	,Created
	,Updated
	,ByWhom
) 
SELECT
	 	 
	  [CaseLoadID]
	
	,[ReferralID]
	
	,[RoleTypeID]
	
	,[StatusID]
	
	,AllocationTime

	,AllocationDate
	
	,AllocationReasonID
	
	,InterventionlevelID
	
	,DischargeTime

	,DischargeDate
	
	,Outcome
	
	,AllocationProfessionalCarerID
	
	,AllocationSpecialtyID
	
	,AllocationStaffTeamID
	
	,ResponsibleHealthOrganisation
	
	,DischargeReasonID
	
	,PatientSourceID
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator
	
	,PatientTitleID
	
	,PatientForename
	
	,PatientSurname
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,PatientCurrentRegisteredPracticeCode
		
	,PatientEncounterRegisteredPracticeCode
	
	,PatientCurrentRegisteredGPCode
		
	,PatientEncounterRegisteredGPCode
	
	,[CreatedTime]
	
	,[ModifiedTime]
	
	,[CreatedByID]
	
	,[ModifiedByID]
	
	,[ArchiveFlag]
	
	,[HealthOrgOwnerID]

	,Created = GETDATE()
	,Updated = GETDATE()
	,ByWhom = SUSER_NAME()

FROM
	dbo.TLoadCOMCaseLoad
	




select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadCOMCaseload', @Stats, @StartTime

﻿
create proc dbo.LoadMedisecInpatientRequest

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table Medisec.InpatientRequest

insert into Medisec.InpatientRequest
(
	 [PatientNumber]
	,[AdmissionDate]
	,[ForcebackNumber]
	,[ConsultantCode]
	,[AdmissionTime]
	,[DischargeDate]
	,[PatientSurname]
	,[PatientInitials]
	,[HospitalCode]
	,[WardCode]
	,[SpecialtyCode]
	,[EpisodeStatusCode]
	,[DocumentTime]
	,[Comment]
	,[StatusCode]
	,[MailFlag]
	,[MailTime]
	,[EmailFlag]
	,[EmailTime]
	,[FaxFlag]
	,[FaxDate]
	,[EDIFlag]
	,[EDIDate]
	,[HospitalNumber]
	,[EpisodeSecurityFlag]
	,[MergedFlag]
	,[MergePatientNumber]
	,[MergeEpisodeNumber]
)

select 
	 [PatientNumber]
	,[AdmissionDate]
	,[ForcebackNumber]
	,[ConsultantCode]
	,[AdmissionTime]
	,[DischargeDate]
	,[PatientSurname]
	,[PatientInitials]
	,[HospitalCode]
	,[WardCode]
	,[SpecialtyCode]
	,[EpisodeStatusCode]
	,[DocumentTime]
	,[Comment]
	,[StatusCode]
	,[MailFlag]
	,[MailTime]
	,[EmailFlag]
	,[EmailTime]
	,[FaxFlag]
	,[FaxDate]
	,[EDIFlag]
	,[EDIDate]
	,[HospitalNumber]
	,[EpisodeSecurityFlag]
	,[MergedFlag]
	,[MergePatientNumber]
	,[MergeEpisodeNumber]
      
from dbo.TLoadMedisecInpatientRequest

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


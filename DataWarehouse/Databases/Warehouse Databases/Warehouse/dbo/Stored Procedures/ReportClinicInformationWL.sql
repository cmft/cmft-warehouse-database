﻿CREATE procedure [dbo].[ReportClinicInformationWL]
	 @AppointmentDate smalldatetime = null
	,@SiteCode varchar(10) = null
	,@ConsultantCode varchar(10) = null
	,@SpecialtyCode varchar(10) = null
	,@NewReview varchar(50) = null
	,@CensusDate smalldatetime = null
as

select
	@CensusDate =
		coalesce(
			@CensusDate
			,(select max(CensusDate) from OP.Snapshot)
		)


select
	 WL.EncounterRecno
	,WL.DistrictNo
	,WL.PatientSurname
	,WL.PatientForename
	,WL.PatientTitle
	,WL.DateOfBirth
	,WL.HomePhone
	,WL.WorkPhone
	,WL.ConsultantCode

	,Consultant =
	case 
	when upper(WL.Operation) like '%PELVIC SERVICE' then '*' 
	when upper(WL.Operation) like '%TGL' then '#'
	else '' 
	end +
	case 
		when WL.ConsultantCode is null then 'No Consultant'
		when WL.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then WL.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end 


	,WL.SpecialtyCode
	,Specialty.Specialty

	,WL.AppointmentTime

	,WL.SiteCode

	,Site =
	case 
	when WL.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then WL.SiteCode + ' - No Description' 
	else Site.Site
	end

	,NewReview =
	case when New.EncounterRecno is null then 'Review' else 'New' end

	,WL.ClinicCode
	,Clinic =
		coalesce(Clinic.Comment, Clinic.Clinic)

	,Age =
	 case
		when datediff(day,DateOfBirth,CensusDate) =0 then '0 Days'
		when datediff(day,DateOfBirth,CensusDate) =1 then '1 Day'
		when datediff(day,DateOfBirth,CensusDate) > 1 and
			datediff(day,DateOfBirth,CensusDate) <= 28 then
				Convert(Varchar,datediff(day,DateOfBirth,CensusDate)) + ' Days'

		when datediff(day,DateOfBirth,CensusDate) > 28 and
			datediff(month,DateOfBirth,CensusDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,CensusDate) then 1 else 0 end
				 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month,DateOfBirth,CensusDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,CensusDate) then 1 else 0 end = 1
				then '1 Month'
		when datediff(month,DateOfBirth,CensusDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,CensusDate) then 1 else 0 end > 1 and
			 datediff(month,DateOfBirth,CensusDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,CensusDate) then 1 else 0 end <= 23
			then
			Convert(varchar,datediff(month,DateOfBirth,CensusDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,CensusDate) then 1 else 0 end) + ' Months'
		when datediff(month,DateOfBirth,CensusDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,CensusDate) then 1 else 0 end > 23
			and datediff(year,DateOfBirth,CensusDate) -

			case  when datepart(dy,DateOfBirth) > datepart(dy,CensusDate) then 1 else 0 end <=100
			then
			Convert(varchar,datediff(year,DateOfBirth,CensusDate) -
			case  when datepart(dy,DateOfBirth) > datepart(dy,CensusDate) then 1 else 0 end) + ' Years'
		when datediff(year,DateOfBirth,CensusDate) -
			case  when datepart(dy,DateOfBirth) > datepart(dy,CensusDate) then 1 else 0 end > 100 then

			'> 100 Years'
		else 'Age Unknown'

		end

from
	OP.WaitingList WL

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = WL.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = WL.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = WL.SpecialtyCode

left join PAS.Clinic
on	Clinic.ClinicCode = WL.ClinicCode

left join OP.WaitingListPTL New
on	New.EncounterRecno = WL.EncounterRecno

where
	WL.CensusDate = @CensusDate
and	WL.AppointmentDate = coalesce(@AppointmentDate, dateadd(day, datediff(day, 0, getdate()), 0))

and
	(
		Site.SiteCode = @SiteCode
	or	@SiteCode is null
	)

and
	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and
	(
		WL.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and
	(
		case when New.EncounterRecno is null then 'Review' else 'New' end = @NewReview
	or	@NewReview is null
	)

order by
	WL.DateOfBirth

﻿


CREATE PROCEDURE [dbo].[LoadCOMEncounter] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the encounter table
DELETE FROM COM.Encounter
WHERE EXISTS
  (SELECT SourceUniqueID
    FROM dbo.TImportCOMEncounter
    WHERE COM.Encounter.SourceUniqueID = dbo.TImportCOMEncounter.SourceUniqueID
			AND COM.Encounter.ModifiedTime = dbo.TImportCOMEncounter.ModifiedTime );


-- Insert new rows into encounter destination table
INSERT INTO COM.Encounter
(
	 SourceEncounterID
    ,SourceUniqueID --[SCHDL_REFNO]
	,SpecialtyID --[SPECT_REFNO]
	,StaffTeamID--[STEAM_REFNO]
	,ProfessionalCarerID--[PROCA_REFNO]
	,SeenByProfessionalCarerID --[SEENBY_PROCA_REFNO]
	,EncounterProfessionalCarerID
	,StartDate
	,StartTime
	,EndTime
	,ArrivedTime  --[ARRIVED_DTTM]
	,SeenTime  --[SEEN_DTTM]
	,DepartedTime --[DEPARTED_DTTM]
	,AttendedID --[ATTND_REFNO] for use in clinic activity
	,OutcomeID  --[SCOCM_REFNO] for use in contact activity
	,EncounterOutcomeCode --Derived calculated field
	,EncounterDuration --Derived calculated field
	,ReferralID  --[REFRL_REFNO]
	,ProfessionalCarerEpisodeID --[PRCAE_REFNO]
	,PatientSourceID --[PATNT_REFNO]
	,PatientSourceSystemUniqueID
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier
	,PatientIDPASNumber
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,VisitID  --[VISIT_REFNO]
	,ContactTypeID
	,EncounterTypeID
	,ScheduleTypeID --[SCTYP_REFNO]  appointments 1470 Contacts 1468 
	,ScheduleReasonID --[REASN_REFNO]
	,CanceledReasonID --[CANCR_REFNO]
	,CanceledTime --[CANCR_DTTM]
	,CancelledByID --[CANCB_REFNO]
	,MoveReasonID  --[MOVRN_REFNO]
	,MoveTime --[MOVE_DTTM]
	,ServicePointID  --[SPONT_REFNO]
	,ServicePointSessionID --[SPSSN_REFNO]
	,MoveCount --[MOVE_COUNT]
	,LocationTypeID --[LOTYP_REFNO]
	,LocationDescription --[LOCATION]
	,LocationTypeHealthOrgID
	,EncounterLocationID
	,ServicePointStaysID --[SSTAY_REFNO]
	,WaitingListID --[WLIST_REFNO]
	,PlannedAttendees --[PLANNED_ATTENDEES]
	,ActualAttendees --[ACTUAL_ATTENDEES]
	,ProviderSpellID --[PRVSP_REFNO]
	,RTTStatusID --[RTTST_REFNO]
	,EarliestReasinableOfferTime --[ERO_DTTM]
	,EncounterInterventionsCount
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedBy --[USER_CREATE]
	,ModifiedBy --[USER_MODIF]
	,Archived --[ARCHV_FLAG]
	,ParentID --[PARNT_REFNO]
	,HealthOrgOwnerID --[OWNER_HEORG_REFNO]
	,RecordStatus  --Status of record either C(current), H(historical increment), d(deleted), or t(test patient)
	,JointActivityFlag --indicates if the encounter record is a shared activity (contacts - shared contact; clinic - participation)
	,Comment
	,ReferralReceivedTime
	,ReferralReceivedDate
	,ReferralClosedTime
	,ReferralClosedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,CommissionerCode 
	,PctOfResidence  
	,LocalAuthorityCode 
	,DistrictOfResidence

	,PatientEncounterRegisteredPracticeID
	,Created
	,Updated
	,ByWhom
	,DNAReasonID
	,CalledTime
)
SELECT
	 SourceEncounterID
	,SourceUniqueID --[SCHDL_REFNO]
	,SpecialtyID --[SPECT_REFNO]
	,StaffTeamID--[STEAM_REFNO]
	,ProfessionalCarerID--[PROCA_REFNO]
	,SeenByProfessionalCarerID --[SEENBY_PROCA_REFNO]
	,EncounterProfessionalCarerID
	,StartDate
	,StartTime
	,EndTime
	,ArrivedTime  --[ARRIVED_DTTM]
	,SeenTime  --[SEEN_DTTM]
	,DepartedTime --[DEPARTED_DTTM]
	,AttendedID --[ATTND_REFNO] for use in clinic activity
	,OutcomeID  --[SCOCM_REFNO] for use in contact activity
	,EncounterOutcomeCode --Derived calculated field
	,EncounterDuration --Derived calculated field
	,ReferralID  --[REFRL_REFNO]
	,ProfessionalCarerEpisodeID --[PRCAE_REFNO]
	,PatientSourceID --[PATNT_REFNO]
	,PatientSourceSystemUniqueID
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier
	,PatientIDPASNumber
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,VisitID  --[VISIT_REFNO]
	,[ContactTypeID]
	,EncounterTypeID
	,ScheduleTypeID --[SCTYP_REFNO]  appointments 1470 Contacts 1468 
	,ScheduleReasonID --[REASN_REFNO]
	,CanceledReasonID --[CANCR_REFNO]
	,CanceledTime --[CANCR_DTTM]
	,CancelledByID --[CANCB_REFNO]
	,MoveReasonID  --[MOVRN_REFNO]
	,MoveTime --[MOVE_DTTM]
	,ServicePointID  --[SPONT_REFNO]
	,ServicePointSessionID --[SPSSN_REFNO]
	,MoveCount --[MOVE_COUNT]
	,LocationTypeID --[LOTYP_REFNO]
	,LocationDescription --[LOCATION]
	,LocationTypeHealthOrgID
	,EncounterLocationID
	,ServicePointStaysID --[SSTAY_REFNO]
	,WaitingListID --[WLIST_REFNO]
	,PlannedAttendees --[PLANNED_ATTENDEES]
	,ActualAttendees --[ACTUAL_ATTENDEES]
	,ProviderSpellID --[PRVSP_REFNO]
	,RTTStatusID --[RTTST_REFNO]
	,EarliestReasinableOfferTime --[ERO_DTTM]
	,EncounterInterventionsCount
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedByID --[USER_CREATE]
	,ModifiedByID --[USER_MODIF]
	,ArchiveFlag --[ARCHV_FLAG]
	,ParentID --[PARNT_REFNO]
	,HealthOrgOwnerID --[OWNER_HEORG_REFNO]
	,RecordStatus ='C' -- Default as 'C' - Current
	,JointActivityFlag = coalesce(JointActivityFlag,0)
	,Comment
	,ReferralReceivedTime
	,ReferralReceivedDate
	,ReferralClosedTime
	,ReferralClosedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,CommissionerCode 
	,PctOfResidence  
	,LocalAuthorityCode 
	,DistrictOfResidence 

	,PatientEncounterRegisteredPracticeID
	,Created = GETDATE()
	,Updated = GETDATE()
	,ByWhom = SUSER_NAME()
	,DNAReasonID
	,CalledTime
FROM
	dbo.TLoadCOMEncounter
	
select @RowsInserted = ISNULL(@@rowcount,0)

-- Update incremental history
UPDATE COM.Encounter

SET RecordStatus = 'H'
FROM 
	COM.Encounter
	
INNER JOIN
				(
					SELECT 
						  SourceUniqueID
						, MAX(ModifiedTime) AS LatestModifiedTime
					FROM 
						COM.Encounter
					GROUP BY 
						SourceUniqueID
				)LatestEncounter
 ON Encounter.SourceUniqueID = LatestEncounter.SourceUniqueID
AND Encounter.ModifiedTime <> LatestEncounter.LatestModifiedTime


--Update where test patient
UPDATE COM.Encounter
SET RecordStatus = 'T'
WHERE	LEFT(PatientNHSNumber, 3) = '999'
	OR
		PatientForename  IN ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')
	OR
		PatientSurname  IN ('xxtestpatientaavp','xxtestpatientaafe')
				

--Update Joint contact encounter details
UPDATE COM.Encounter

SET 
	 SpecialtyID = EncounterParent.SpecialtyID
	,StaffTeamID = EncounterParent.StaffTeamID
	,StartDate = EncounterParent.StartDate
	,StartTime = EncounterParent.StartTime
	,EndTime = EncounterParent.EndTime
	,ArrivedTime = EncounterParent.ArrivedTime
	,SeenTime = EncounterParent.SeenTime
	,DepartedTime = EncounterParent.DepartedTime
	,AttendedID = EncounterParent.AttendedID
	,OutcomeID = EncounterParent.OutcomeID
	,EncounterOutcomeCode = EncounterParent.EncounterOutcomeCode
	,EncounterDuration = EncounterParent.EncounterDuration
	,ReferralID = EncounterParent.ReferralID
	,ProfessionalCarerEpisodeID = EncounterParent.ProfessionalCarerEpisodeID
	,PatientSourceID = EncounterParent.PatientSourceID
	,PatientSourceSystemUniqueID = EncounterParent.PatientSourceSystemUniqueID
	,PatientNHSNumber  = EncounterParent.PatientNHSNumber
	,PatientNHSNumberStatusIndicator  = EncounterParent.PatientNHSNumberStatusIndicator
	,PatientTitleID = EncounterParent.PatientTitleID
	,PatientForename = EncounterParent.PatientForename
	,PatientSurname = EncounterParent.PatientSurname
	,PatientAddress1 = EncounterParent.PatientAddress1
	,PatientAddress2 = EncounterParent.PatientAddress2
	,PatientAddress3 = EncounterParent.PatientAddress3
	,PatientAddress4 = EncounterParent.PatientAddress4
	,PatientPostcode = EncounterParent.PatientPostcode
	,PatientDateOfBirth = EncounterParent.PatientDateOfBirth
	,PatientDateOfDeath  = EncounterParent.PatientDateOfDeath
	,PatientSexID  = EncounterParent.PatientSexID
	,PatientEthnicGroupID  = EncounterParent.PatientEthnicGroupID
	,PatientCurrentRegisteredPracticeCode = EncounterParent.PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode = EncounterParent.PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode = EncounterParent.PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode = EncounterParent.PatientEncounterRegisteredGPCode
	,VisitID = EncounterParent.VisitID
	,ContactTypeID = EncounterParent.ContactTypeID
	,EncounterTypeID = EncounterParent.EncounterTypeID
	,ScheduleTypeID  = EncounterParent.ScheduleTypeID
	,ScheduleReasonID = EncounterParent.ScheduleReasonID
	,CanceledReasonID = EncounterParent.CanceledReasonID
	,CanceledTime = EncounterParent.CanceledTime
	,CancelledByID = EncounterParent.CancelledByID
	,MoveReasonID = EncounterParent.MoveReasonID
	,MoveTime = EncounterParent.MoveTime
	,ServicePointID = EncounterParent.ServicePointID
	,ServicePointSessionID  = EncounterParent.ServicePointSessionID
	,MoveCount = EncounterParent.MoveCount
	,LocationTypeID = EncounterParent.LocationTypeID
	,LocationDescription = EncounterParent.LocationDescription
	,LocationTypeHealthOrgID = EncounterParent.LocationTypeHealthOrgID
	,EncounterLocationID = EncounterParent.EncounterLocationID
	,ServicePointStaysID = EncounterParent.ServicePointStaysID
	,WaitingListID  = EncounterParent.WaitingListID
	,PlannedAttendees = EncounterParent.PlannedAttendees
	,ActualAttendees = EncounterParent.ActualAttendees
	,ProviderSpellID  = EncounterParent.ProviderSpellID
	,RTTStatusID = EncounterParent.RTTStatusID
	,EarliestReasinableOfferTime  = EncounterParent.EarliestReasinableOfferTime
	,EncounterInterventionsCount = EncounterParent.EncounterInterventionsCount
	,JointActivityFlag = 1
	,ReferralReceivedTime  = EncounterParent.ReferralReceivedTime
	,ReferralClosedTime  = EncounterParent.ReferralClosedTime
	,ReferralReasonID  = EncounterParent.ReferralReasonID
	,ReferredToSpecialtyID  = EncounterParent.ReferredToSpecialtyID
	,ReferredToStaffTeamID  = EncounterParent.ReferredToStaffTeamID
	,ReferredToProfCarerID  = EncounterParent.ReferredToProfCarerID
	,ReferralSourceID  = EncounterParent.ReferralSourceID
	,ReferralTypeID  = EncounterParent.ReferralTypeID

	,PatientEncounterRegisteredPracticeID = EncounterParent.PatientEncounterRegisteredPracticeID
	,Created = EncounterParent.Created
	,Updated = EncounterParent.Updated
	,ByWhom = EncounterParent.ByWhom
	,DNAReasonID = EncounterParent.DNAReasonID
	,CalledTime = EncounterParent.CalledTime
FROM
	COM.Encounter
INNER JOIN	
	(
	SELECT
			*
	FROM
			COM.Encounter
	WHERE
			ScheduleTypeID = 1468
		AND RecordStatus = 'C'
		AND Encounter.ParentID IS NULL
	)EncounterParent
	ON EncounterParent.SourceUniqueID = Encounter.ParentID
WHERE 
			Encounter.StartTime IS NULL
		AND Encounter.ParentID IS NOT NULL
		AND Encounter.ScheduleTypeID = 1468

--set up WaitDays
select
	 Encounter.ReferralID
	,Encounter.StartDate
	,Encounter.EncounterRecno
	,RowNo =
		row_number()
		over(
			partition by
				Encounter.ReferralID
			order by
				 Encounter.StartTime
				,Encounter.EncounterRecno
		)
into
	#orderedList
from
	COM.Encounter



update
	COM.Encounter
set
	WaitDays =
		case
		when Encounter.DerivedFirstAttendanceFlag = 1
		then DATEDIFF(
				 day
				,Encounter.ReferralReceivedDate
				,Encounter.StartDate
			)

		else DATEDIFF(
				 day
				,Previous.StartDate
				,Encounter.StartDate
			)

		end

from
	COM.Encounter

inner join #orderedList
on	#orderedList.EncounterRecno = Encounter.EncounterRecno

left join #orderedList Previous
on	Previous.ReferralID = Encounter.ReferralID
and	Previous.RowNo = #orderedList.RowNo - 1




select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	--'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' 
	--+ 	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	--CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadCOMEncounter', @Stats, @StartTime


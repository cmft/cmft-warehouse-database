﻿

CREATE procedure [dbo].[LoadCOMProfessionalCarer] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the TImport table

TRUNCATE TABLE COM.ProfessionalCarer

-- Insert new rows into encounter destination table
INSERT INTO COM.ProfessionalCarer
(
	 ProfessionalCarerID  --[PROCA_REFNO]
	
	,ProfessionalCarerCode --[PROCA_REFNO_MAIN_IDENT]
	
	,ProfessionalCarerTypeID --[PRTYP_REFNO]

	,Title --[TITLE_REFNO_DESCRIPTION]
	
	,Forename --[FORENAME]

	,Surname --[SURNAME]
	
	,FullName

	,StartTime --[START_DTTM]
	
	,EndTime --[END_DTTM]

	,PrimaryStaffTeamID --[PRIMARY_STEAM_REFNO]
	
	,ArchiveFlag --[ARCHV_FLAG]

	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	
) 
SELECT
	 ProfessionalCarerID  --[PROCA_REFNO]
	
	,ProfessionalCarerCode --[PROCA_REFNO_MAIN_IDENT]
	
	,ProfessionalCarerTypeID --[PRTYP_REFNO]

	,Title --[TITLE_REFNO_DESCRIPTION]
	
	,Forename --[FORENAME]

	,Surname --[SURNAME]
	
	,FullName

	,StartTime --[START_DTTM]
	
	,EndTime --[END_DTTM]

	,PrimaryStaffTeamID --[PRIMARY_STEAM_REFNO]
	
	,ArchiveFlag --[ARCHV_FLAG]

	,HealthOrgOwner --[OWNER_HEORG_REFNO]

FROM
	dbo.TLoadCOMProfessionalCarer
	

INSERT INTO COM.ProfessionalCarer
(
	 
	 ProfessionalCarerID  --[PROCA_REFNO]
	
	,ProfessionalCarerCode --[PROCA_REFNO_MAIN_IDENT]
	
	,ProfessionalCarerTypeID --[PRTYP_REFNO]

	,Title --[TITLE_REFNO_DESCRIPTION]
	
	,Forename --[FORENAME]

	,Surname --[SURNAME]

	,StartTime --[START_DTTM]
	
	,EndTime --[END_DTTM]

	,PrimaryStaffTeamID --[PRIMARY_STEAM_REFNO]
	
	,ArchiveFlag --[ARCHV_FLAG]

	,HealthOrgOwner --[OWNER_HEORG_REFNO]
)
VALUES
(
	 -1
	,-1
	,NULL
	,NULL
	,NULL
	,'Not Recorded'
	,'01/01/1900'
	,NULL
	,NULL
	,'N'
	,NULL
)


/*
select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadCOMEncounter', @Stats, @StartTime
*/
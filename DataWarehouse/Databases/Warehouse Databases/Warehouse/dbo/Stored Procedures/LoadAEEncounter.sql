﻿

CREATE procedure [dbo].[LoadAEEncounter] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @from = (select min(convert(smalldatetime, ArrivalDate)) from TLoadAEEncounter)
select @to = (select max(convert(smalldatetime, ArrivalDate)) from TLoadAEEncounter)

delete from AE.Encounter
where
	coalesce(ArrivalDate, @from) between @from and @to
and	Encounter.InterfaceCode = 'SYM'

select @RowsDeleted = @@rowcount

insert into AE.Encounter
(
	 SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,SourceAttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,TreatmentCodeFirst
	,TreatmentCodeSecond
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	,PCTCode
	,ResidencePCTCode
	,InvestigationCodeList
	,TriageCategoryCode
	,PresentingProblem
	,PresentingProblemCode
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,EthnicCategoryCode
	,ReligionCode
	,ReferredToSpecialtyCode
	,DecisionToAdmitTime
	,DischargeDestinationCode
	,RegisteredTime
	,TransportRequestTime
	,TransportAvailableTime
	,AscribeLeftDeptTime
	,CDULeftDepartmentTime
	,PCDULeftDepartmentTime
	,AmbulanceArrivalTime
	,AmbulanceCrewPRF --GS 20110406  New column added
	,UnplannedReattend7Day  --GS 20110406  New column added
	,ArrivalTimeAdjusted --GS 20110406  New column added
	,ClinicalAssessmentTime
	,Reportable
	,SourceCareGroupCode
	,AlcoholLocation
	,AlcoholInPast12Hours
	,RapidAssessmentTime
	,TriageComment
	,GpCodeAtAttendance
	,GpPracticeCodeAtAttendance
	,PostcodeAtAttendance
	,CCGCode
)
select
	 SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,SourceAttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,TreatmentCodeFirst
	,TreatmentCodeSecond
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	,PCTCode
	,ResidencePCTCode
	,InvestigationCodeList
	,TriageCategoryCode
	,PresentingProblem
	,PresentingProblemCode
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,EthnicCategoryCode
	,ReligionCode
	,ReferredToSpecialtyCode
	,DecisionToAdmitTime
	,DischargeDestinationCode
	,RegisteredTime
	,TransportRequestTime
	,TransportAvailableTime
	,AscribeLeftDeptTime
	,CDULeftDepartmentTime
	,PCDULeftDepartmentTime
	,AmbulanceArrivalTime
	,AmbulanceCrewPRF --GS 20110406  New column added
	,UnplannedReattend7Day  --GS 20110406  New column added
	,ArrivalTimeAdjusted --GS 20110406  New column added
	,ClinicalAssessmentTime
	,Reportable
	,SourceCareGroupCode
	,AlcoholLocation
	,AlcoholInPast12Hours
	,RapidAssessmentTime
	,TriageComment
	,GpCodeAtAttendance
	,GpPracticeCodeAtAttendance
	,PostcodeAtAttendance
	,CCGCode
from
	dbo.TLoadAEEncounter
	


---- 2010-02-18 CCB
---- bad records in source - old attendance has been modified rather than new attendance created
--
--where
--	SourceUniqueID != '3237875'
--and	ArrivalTime != '2010-02-17 16:02:00'

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadAEEncounter', @Stats, @StartTime






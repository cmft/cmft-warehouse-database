﻿
CREATE procedure [dbo].[LoadBedmanPDDReasons] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @RowsDeleted = @@rowcount

TRUNCATE TABLE Bedman.PddReasons

insert into Bedman.PddReasons
(
	 PDDReasonID
	,PDDGroup
	,PDDReason
	,Active
)
SELECT 	 
 	 PDDReasonID
	,PDDGroup
	,PDDReason
	,Active
FROM 
	dbo.TLoadBedmanPDDReasons
	
select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadBedmanPddReasons', @Stats, @StartTime




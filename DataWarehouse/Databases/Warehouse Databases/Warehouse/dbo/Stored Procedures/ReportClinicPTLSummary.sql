﻿CREATE procedure [dbo].[ReportClinicPTLSummary] @SiteCode varchar(10) as

set dateformat dmy

select
	datediff(week, getdate(), Diary.SessionDate) WeeksFromNow,
	coalesce(Clinic.SpecialtyCode, '##') SpecialtyCode,

	case 
	when Clinic.SpecialtyCode is null then 'Unknown Specialty' 
	when Specialty.Specialty is null then Clinic.SpecialtyCode + ' - No Description' 
	else Specialty.Specialty 
	end Specialty, 

	min(Diary.SessionEndTime) FromDate,
	max(Diary.SessionEndTime) ToDate,

	sum(Diary.Units) Slots,
	sum(Diary.Units - Diary.UsedUnits) VacantSlots
from
	OP.Diary

inner join PAS.Clinic
on	Clinic.ClinicCode = Diary.ClinicCode

left join PAS.Site PASSite
on	PASSite.SiteCode = Clinic.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = Clinic.SpecialtyCode

where
	Diary.SessionEndTime between getdate() and dateadd(week, 10, getdate())
and	Diary.ReasonForCancellation is null
and	Diary.Units - Diary.UsedUnits > 0

and	(
		Site.SiteCode = @SiteCode
	or	@SiteCode is null
	)

group by
	datediff(week, getdate(), Diary.SessionDate),
	coalesce(Clinic.SpecialtyCode, '##'),
	case 
	when Clinic.SpecialtyCode is null then 'Unknown Specialty' 
	when Specialty.Specialty is null then Clinic.SpecialtyCode + ' - No Description' 
	else Specialty.Specialty 
	end


order by
	3, 1

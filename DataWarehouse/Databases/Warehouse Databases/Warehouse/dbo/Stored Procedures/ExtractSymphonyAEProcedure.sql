﻿CREATE procedure [dbo].[ExtractSymphonyAEProcedure] 
	 @from smalldatetime
	,@to smalldatetime
	,@debug bit = 0
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @localfrom smalldatetime
declare @localto smalldatetime

select @StartTime = getdate()


--select
--	 @localfrom = @from
--	,@localto = dateadd(second, 24 * 60 * 60 -1, @to)


insert into dbo.TImportAEProcedure
(
	 SourceUniqueID
	,SequenceNo
	,ProcedureCode
	,ProcedureDate
	,AESourceUniqueID
	,SourceProcedureCode
)
select
	SourceUniqueID
	,SequenceNo = 
		row_number() over(partition by AESourceUniqueID order by ProcedureDate, SourceUniqueID)
	,ProcedureCode 
	,ProcedureDate 
	,AESourceUniqueID 
	,ProcedureID
from
	(
	select
	-- original 
		 SourceUniqueID = Request.req_reqid
		--,SequenceNo = 
		--	ROW_NUMBER() OVER(PARTITION BY Request.req_atdid ORDER BY Request.req_reqid)
		,ProcedureCode = notes.not_text
		,ProcedureDate = Request.req_date
		,AESourceUniqueID = Request.req_atdid
		,ProcedureID = Request.req_request
	from
		[$(Symphony)].dbo.Request_Details Request

	inner join [$(Symphony)].dbo.LookupMappings lookupmappings
	on	Request.req_request = lookupmappings.flm_lkpid

	inner join [$(Symphony)].dbo.MappingTypes mappingtypes
	on	mappingtypes.mt_id = lookupmappings.flm_mtid
	and	mappingtypes.mt_name = 'CDS'

	left join [$(Symphony)].dbo.Notes notes
	on	notes.not_noteid = lookupmappings.flm_value

	inner join [$(Symphony)].dbo.Attendance_Details Encounter
	on	Encounter.atd_id = Request.req_atdid

	inner join [$(Symphony)].dbo.Episodes Episode
	on	Episode.epd_id = Encounter.atd_epdid

	inner join 
		(
		select
			 Department.dpt_id
			,CommissioningSerialNo = '5NT00A'

			,PCTCode = '5NT'

			,SiteCode =
			case
			when Department.dpt_name = 'MRI ED' then 'RW3MR'
			when Department.dpt_name = 'PED' then 'RW3RC'
			when Department.dpt_name = 'EEC' then 'RW3RE'
			when Department.dpt_name = 'St Mary''s' then 'RW3SM'
			when Department.dpt_name = 'Dental' then 'RW3DH'
			end
		from
			[$(Symphony)].dbo.CFG_Dept Department
		where
			Department.dpt_name in (
				 'MRI ED'
				,'PED'
				,'EEC'
				,'St Mary''s'
				,'Dental'
				)
		) Department
	on	Department.dpt_id = Episode.epd_deptid

	where
	--	Encounter.atd_arrivaldate between @localfrom and @localto
		Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
	and Encounter.atd_deleted = 0
	and	Episode.epd_deleted = 0
	and
		Request.req_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'Treatment'
			)
	and	Request.req_request <> 0

	and	not exists
		(
		select
			1
		from
			AE.InvestigationList
		where
			InvestigationList.InvestigationCode = Request.req_request
		)

	-- only the attendances from after the switchover to Symphony
	and (
			(
				Department.SiteCode = 'RW3SM'
			and	Encounter.atd_arrivaldate >= '11 Apr 2011 11:42'
			)
		or	(
				Department.SiteCode = 'RW3RC'
			and	Encounter.atd_arrivaldate >= '20 Oct 2009 09:10'
			)
		or	(
				Department.SiteCode = 'RW3MR'
			and	Encounter.atd_arrivaldate >= '20 Oct 2009 10:56'
			)

		or	(
				Department.SiteCode = 'RW3RE'
			and	Encounter.atd_arrivaldate >= '11 Sep 2010 00:00'
			)
		or	(
				Department.SiteCode = 'RW3DH'
			and	Encounter.atd_arrivaldate >= '11 Oct 2011 00:00'
			)
		)

	union all

	--Triage Observations - Adult/PAED/REH

	select
		 SourceUniqueID = Triage_ExtraFields.tri_EF_extrafieldID
		,ProcedureCode = notes.not_text
		,ProcedureDate = tri_EF_created -- need triage date from Triage table but unsure how to link?
		,AESourceUniqueID = tri_EF_atdid
		,ProcedureID = lookupmappings.flm_value
	from
		[$(Symphony)].dbo.Triage_ExtraFields

	inner join [$(Symphony)].dbo.Attendance_Details Encounter
	on	Encounter.atd_id = Triage_ExtraFields.tri_EF_atdid

	inner join [$(Symphony)].dbo.Episodes Episode
	on	Episode.epd_id = Encounter.atd_epdid

	inner join [$(Symphony)].dbo.Lookups
	on	Triage_ExtraFields.tri_EF_Value = Lookups.Lkp_ID

	inner join [$(Symphony)].dbo.LookupMappings lookupmappings
	on	Lookups.Lkp_ID = lookupmappings.flm_lkpid

	inner join [$(Symphony)].dbo.MappingTypes mappingtypes
	on	mappingtypes.mt_id = lookupmappings.flm_mtid
	and	mappingtypes.mt_name = 'CDS'

	inner join [$(Symphony)].dbo.Notes notes
	on	notes.not_noteid = lookupmappings.flm_value

	where
		tri_EF_FieldID in
						(
						'1478' --Triage
						,'1446' --pre-reg triage
						)
	and Lookups.Lkp_ID in 
						(
						11208 --Adult
						,11209 --Child
						)

	and	Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
	and	Encounter.atd_deleted = 0
	and	Episode.epd_deleted = 0


	union all

	--Triage Observations - SMH

	select
		 SourceUniqueID = Triage_ExtraFields.tri_EF_extrafieldID
		,ProcedureCode = '21'
		,ProcedureDate = tri_EF_created -- need triage date from Triage table but unsure how to link?
		,AESourceUniqueID = tri_EF_atdid
		,SourceProcedureCode = null
	from
		[$(Symphony)].dbo.Triage_ExtraFields

	inner join [$(Symphony)].dbo.Attendance_Details Encounter
	on	Encounter.atd_id = Triage_ExtraFields.tri_EF_atdid

	inner join [$(Symphony)].dbo.Episodes Episode
	on	Episode.epd_id = Encounter.atd_epdid


	inner join [$(Symphony)].dbo.Lookups
	on	Triage_ExtraFields.tri_EF_Value = Lookups.Lkp_ID

	where
		Triage_ExtraFields.tri_EF_FieldID in
			(
			'1750'
			)
	and Lookups.Lkp_ID in 
			(
			501 --Yes
			)
	and	Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
	and	Encounter.atd_deleted = 0
	and	Episode.epd_deleted = 0

	) Treatment

select @RowsInserted = @@rowcount

--now extract and unbundle the composite field

BEGIN

	/**************************************************************************/
	/* General Declarations                                                   */
	/**************************************************************************/
	declare @FetchStatus int

	declare @SourceUniqueID int
	declare @ProcedureCode varchar(255)
	declare @ProcedureDate smalldatetime
	declare @AESourceUniqueID int
	declare @ProcedureList varchar(8000)
	declare @ResultCode int
	declare @SequenceNo int


	/**************************************************************************/
	/* Declaration for trigger cursor                                         */
	/**************************************************************************/

	declare TableCursor CURSOR fast_forward FOR

		select
			 SourceUniqueID = Result.res_resid
			,ProcedureDate = Result.res_date
			,AESourceUniqueID = Result.res_atdid
			,ProcedureList = Result.res_field6
		from
			[$(Symphony)].dbo.Result_details Result

		inner join [$(Symphony)].dbo.CFG_DEProcedures
		on	Result.res_depid = CFG_DEProcedures.dep_id
		and	CFG_DEProcedures.dep_name = 'Treatment'

		inner join [$(Symphony)].dbo.Attendance_Details Encounter
		on	Encounter.atd_id = Result.res_atdid

		inner join [$(Symphony)].dbo.Episodes Episode
		on	Episode.epd_id = Encounter.atd_epdid

		inner join 
			(
			select
				 Department.dpt_id
				,CommissioningSerialNo = '5NT00A'

				,PCTCode = '5NT'

				,SiteCode =
				case
				when Department.dpt_name = 'MRI ED' then 'RW3MR'
				when Department.dpt_name = 'PED' then 'RW3RC'
				when Department.dpt_name = 'EEC' then 'RW3RE'
				when Department.dpt_name = 'St Mary''s' then 'RW3SM'
				when Department.dpt_name = 'Dental' then 'RW3DH'
				end
			from
				[$(Symphony)].dbo.CFG_Dept Department
			where
				Department.dpt_name in (
					 'MRI ED'
					,'PED'
					,'EEC'
					,'St Mary''s'
					,'Dental'
					)
			) Department
		on	Department.dpt_id = Episode.epd_deptid

		where
			Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
		--Encounter.atd_arrivaldate between '1 apr 2011' and dateadd(second, 24 * 60 * 60 -1, '30 apr 2011')
		and	Encounter.atd_deleted = 0
		and	Episode.epd_deleted = 0

		-- only the attendances from after the switchover to Symphony
		and (
				(
					Department.SiteCode = 'RW3SM'
				and	Encounter.atd_arrivaldate >= '11 Apr 2011 11:42'
				)
			or	(
					Department.SiteCode = 'RW3RC'
				and	Encounter.atd_arrivaldate >= '20 Oct 2009 09:10'
				)
			or	(
					Department.SiteCode = 'RW3MR'
				and	Encounter.atd_arrivaldate >= '20 Oct 2009 10:56'
				)

			or	(
					Department.SiteCode = 'RW3RE'
				and	Encounter.atd_arrivaldate >= '11 Sep 2010 00:00'
				)
			or	(
					Department.SiteCode = 'RW3DH'
				and	Encounter.atd_arrivaldate >= '11 Oct 2011 00:00'
				)
			)

		and	Result.res_field6 <> ''


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	OPEN TableCursor

	FETCH NEXT FROM TableCursor INTO
		 @SourceUniqueID
		,@ProcedureDate
		,@AESourceUniqueID
		,@ProcedureList

	Select @FetchStatus = @@FETCH_STATUS


    /**************************************************************************/
    /* Main Processing Loop                                                   */
    /**************************************************************************/
    WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

	begin

		select
			 @SequenceNo = 0

		declare DetailCursor CURSOR fast_forward FOR

		select
			 ResultCode = convert(int, ItemCode)
			,ProcedureCode = notes.not_text
		from
			dbo.ParamParserFn(@ProcedureList, '||')

		inner join [$(Symphony)].dbo.LookupMappings lookupmappings
		on	lookupmappings.flm_lkpid = convert(int, ItemCode)

		inner join [$(Symphony)].dbo.MappingTypes mappingtypes
		on	mappingtypes.mt_id = lookupmappings.flm_mtid
		and	mappingtypes.mt_name = 'CDS'

		left join [$(Symphony)].dbo.Notes notes
		on	notes.not_noteid = lookupmappings.flm_value

		where
			not exists
			(
			select
				1
			from
				AE.InvestigationList
			where
				InvestigationList.InvestigationCode = convert(int, ItemCode)
			)


		OPEN DetailCursor

		FETCH NEXT FROM DetailCursor INTO
			 @ResultCode
			,@ProcedureCode

		Select @FetchStatus = @@FETCH_STATUS

		/**************************************************************************/
		/* Column Processing Loop                                                 */
		/**************************************************************************/
		WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

		begin

			select
				 @SequenceNo = @SequenceNo + 1

			insert into TImportAEProcedure
			(
				 SourceUniqueID
				,SequenceNo
				,ProcedureCode
				,ProcedureDate
				,AESourceUniqueID
				,SourceProcedureCode
			)
			select
				 @SourceUniqueID
				,@SequenceNo
				,@ProcedureCode
				,@ProcedureDate
				,@AESourceUniqueID
				,@ResultCode

			select @RowsInserted = @RowsInserted + 1


			FETCH NEXT FROM DetailCursor INTO
				 @ResultCode
				,@ProcedureCode

			Select @FetchStatus = @@FETCH_STATUS

		end
		

		CLOSE DetailCursor
		DEALLOCATE DetailCursor


		FETCH NEXT FROM TableCursor INTO
			 @SourceUniqueID
			,@ProcedureDate
			,@AESourceUniqueID
			,@ProcedureList

		Select @FetchStatus = @@FETCH_STATUS

    END

	/**************************************************************************/
	/* Termination                                                            */
	/**************************************************************************/
	CLOSE TableCursor
	DEALLOCATE TableCursor

END

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractSymphonyAEProcedure', @Stats, @StartTime

﻿

CREATE proc [dbo].[ExtractBedmanPDDReasons]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime = NULL
declare @to smalldatetime = NULL

select @StartTime = getdate()

truncate table dbo.TImportBedmanPDDReasons

insert into dbo.TImportBedmanPDDReasons
select 
	 PDDReasonID
	,PDDGroup
	,PDDReason
	,Active
FROM
(
SELECT 
	 PDDReasonID
	,PDDGroup
	,PDDReason
	,Active
FROM 
	[$(BedmanTest)].dbo.vwPDDReasons
) AS PDDReasons

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractBedmanPDDReasons', @Stats, @StartTime

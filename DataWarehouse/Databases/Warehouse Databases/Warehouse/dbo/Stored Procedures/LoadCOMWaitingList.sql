﻿CREATE procedure [dbo].[LoadCOMWaitingList] as

set dateformat dmy

Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsDeleted Int
Declare @RowsInserted Int
Declare @Stats varchar(255)

Select @StartTime = getdate()

declare @CensusDate smalldatetime

select
	@CensusDate =
	(
	select top 1
		CensusDate
	from
		dbo.TImportCOMReferralWait
	)


-- Clear down old snapshots
delete
from
	COM.Wait
where
	CensusDate = @CensusDate


SELECT @RowsDeleted = @@ROWCOUNT

declare @CensusCutOffDate smalldatetime

select @CensusCutOffDate =
	dateadd(
		 day
		,(
		select
			NumericValue * -1
		from
			Utility.Parameter
		where
			Parameter = 'KEEPSNAPSHOTDAYS'
		)
		,@CensusDate
	)

--last snapshot in month
select
	CensusDate = max(CensusDate) 
into
	#OldSnapshot
from
	COM.Wait 
where
	CensusDate < @CensusCutOffDate
group by
	 datepart(year, CensusDate)
	,datepart(month, CensusDate)

--sunday snapshot
--14.1.2014 - DG - Sundays not needed. GS confirmed
--insert into #OldSnapshot

--select distinct
--	CensusDate 
--from
--	COM.Wait

--inner join WarehouseOLAP.dbo.OlapCalendar
--on	Wait.CensusDate = OlapCalendar.TheDate
--and	Wait.CensusDate = OlapCalendar.LastDayOfWeek

--where
--	CensusDate < @CensusCutOffDate
--and	not exists
--		(
--		select
--			1
--		from
--			#OldSnapshot OldSnapshot
--		where
--			OldSnapshot.CensusDate = Wait.CensusDate
--		)

--delete unwanted snapshots
--14.1.2014 - DG - Need last 14 days and end of month only. GS confirmed


delete from COM.Wait
where
	not exists
	(
	select
		1
	from
		#OldSnapshot OldSnapshot
	where
		OldSnapshot.CensusDate = Wait.CensusDate
	)
and	CensusDate < @CensusCutOffDate


--select @RowsDeleted = @RowsDeleted + @@ROWCOUNT
select @RowsDeleted = 0


INSERT INTO COM.Wait
(
	 SourceUniqueID
      ,SourceEncounterNo
      ,CensusDate
      ,ReferredBySpecialtyID
      ,ReferredToSpecialtyID
      ,SourceofReferralID
      ,ReferralReceivedTime
      ,ReasonforReferralID
      ,CancelledTime
      ,CancelledReasonID
      ,ReferralSentTime
      ,UrgencyID
      ,ReferralPriorityID
      ,AuthoriedTime
      ,ReferralClosedTime
      ,ClosureReasonID
      ,ReferredByProfessionalCarerID
      ,ReferredByStaffTeamID
      ,ReferredByHealthOrgID
      ,ReferredToProfessionalCarerID
      ,ReferredToHealthOrgID
      ,ReferredToStaffTeamID
      ,OutcomeOfReferralID
      ,ReferralStatusID
      ,RejectionID
      ,TypeofReferralID
      ,PatientSourceID
      ,PatientSourceSystemUniqueID
      ,PatientNHSNumber
      ,PatientNHSNumberStatusIndicator
      ,PatientLocalIdentifier
      ,PatientPASIdentifier
      ,PatientIdentifier
      ,PatientFacilityIdentifier
      ,PatientTitleID
      ,PatientForename
      ,PatientSurname
      ,PatientAddress1
      ,PatientAddress2
      ,PatientAddress3
      ,PatientAddress4
      ,PatientPostcode
      ,PatientDateOfBirth
      ,PatientDateOfDeath
      ,PatientSexID
      ,PatientEthnicGroupID
      ,PatientSpokenLanguageID
      ,PatientReligionID
      ,PatientMaritalStatusID
      ,ProfessionalCarerCurrentID
      ,ProfessionalCarerHealthOrgCurrentID
      ,ProfessionalCarerParentHealthOrgCurrentID
      ,ProfessionalCarerScheduleID
      ,ProfessionalCarerHealthOrgScheduleID
      ,ProfessionalCarerParentHealthOrgScheduleID
      ,ProfessionalCarerReferralID
      ,ProfessionalCarerHealthOrgReferralID
      ,ProfessionalCarerParentHealthOrgReferralID
      ,RTTStatusDate
      ,RTTPatientPathwayID
      ,RTTStartTime
      ,RTTStartFlag
      ,RTTStatusID
      ,CreatedTime
      ,ModifiedTime
      ,CreatedByID
      ,ModifiedByID
      ,ArchiveFlag
      ,ParentID
      ,HealthOrgOwner
      ,RequestedServiceID
      ,ScheduleID
      ,ScheduleSpecialtyID
      ,ScheduleStaffTeamID
      ,ScheduleProfessionalCarerID
      ,ScheduleSeenByProfessionalCarerID
      ,ScheduleStartTime
      ,ScheduleEndTime
      ,ScheduleArrivedTime
      ,ScheduleSeenTime
      ,ScheduleDepartedTime
      ,ScheduleAttendedID
      ,ScheduleOutcomeID
      ,ScheduleVisitID
      ,ScheduleContactTypeID
      ,ScheduleTypeID
      ,ScheduleReasonID
      ,ScheduleCanceledReasonID
      ,ScheduleCanceledTime
      ,ScheduleCancelledByID
      ,ScheduleMoveReasonID
      ,ScheduleMovetime
      ,ScheduleServicePointID
      ,ScheduleServicePointSessionID
      ,ScheduleMoveCount
      ,ScheduleLocationTypeID
      ,ScheduleLocationDescription
      ,ScheduleLocationTypeHealthOrgID
      ,ScheduleWaitingListID
      ,KornerWait
      ,BreachDate
      ,NationalBreachDate
      ,BreachDays
      ,NationalBreachDays
      ,RTTBreachDate
      ,ClockStartDate
      ,ScheduleOutcomeCode
      ,PatientDeathIndicator
      ,CountOfDNAs
      ,CountOfHospitalCancels
      ,CountOfPatientCancels
      ,LastAppointmentFlag
      ,AdditionFlag
      ,FuturePatientCancelDate
      ,PCTCode
      ,StartWaitDate
      ,EndWaitDate
      ,BookedTime
      ,DerivedFirstAttendanceFlag
	  ,DerivedWaitFlag
	,Created
	,Updated
	,ByWhom
)
select
	 SourceUniqueID
      ,SourceEncounterNo
      ,CensusDate
      ,ReferredBySpecialtyID
      ,ReferredToSpecialtyID
      ,SourceofReferralID
      ,ReferralReceivedTime
      ,ReasonforReferralID
      ,CancelledTime
      ,CancelledReasonID
      ,ReferralSentTime
      ,UrgencyID
      ,ReferralPriorityID
      ,AuthoriedTime
      ,ReferralClosedTime
      ,ClosureReasonID
      ,ReferredByProfessionalCarerID
      ,ReferredByStaffTeamID
      ,ReferredByHealthOrgID
      ,ReferredToProfessionalCarerID
      ,ReferredToHealthOrgID
      ,ReferredToStaffTeamID
      ,OutcomeOfReferralID
      ,ReferralStatusID
      ,RejectionID
      ,TypeofReferralID
      ,PatientSourceID
      ,PatientSourceSystemUniqueID
      ,PatientNHSNumber
      ,PatientNHSNumberStatusIndicator
      ,PatientLocalIdentifier
      ,PatientPASIdentifier
      ,PatientIdentifier
      ,PatientFacilityIdentifier
      ,PatientTitleID
      ,PatientForename
      ,PatientSurname
      ,PatientAddress1
      ,PatientAddress2
      ,PatientAddress3
      ,PatientAddress4
      ,PatientPostcode
      ,PatientDateOfBirth
      ,PatientDateOfDeath
      ,PatientSexID
      ,PatientEthnicGroupID
      ,PatientSpokenLanguageID
      ,PatientReligionID
      ,PatientMaritalStatusID
      ,ProfessionalCarerCurrentID
      ,ProfessionalCarerHealthOrgCurrentID
      ,ProfessionalCarerParentHealthOrgCurrentID
      ,ProfessionalCarerScheduleID
      ,ProfessionalCarerHealthOrgScheduleID
      ,ProfessionalCarerParentHealthOrgScheduleID
      ,ProfessionalCarerReferralID
      ,ProfessionalCarerHealthOrgReferralID
      ,ProfessionalCarerParentHealthOrgReferralID
      ,RTTStatusDate
      ,RTTPatientPathwayID
      ,RTTStartTime
      ,RTTStartFlag
      ,RTTStatusID
      ,CreatedTime
      ,ModifiedTime
      ,CreatedByID
      ,ModifiedByID
      ,ArchiveFlag
      ,ParentID
      ,HealthOrgOwner
      ,RequestedServiceID
      ,ScheduleID
      ,ScheduleSpecialtyID
      ,ScheduleStaffTeamID
      ,ScheduleProfessionalCarerID
      ,ScheduleSeenByProfessionalCarerID
      ,ScheduleStartTime
      ,ScheduleEndTime
      ,ScheduleArrivedTime
      ,ScheduleSeenTime
      ,ScheduleDepartedTime
      ,ScheduleAttendedID
      ,ScheduleOutcomeID
      ,ScheduleVisitID
      ,ScheduleContactTypeID
      ,ScheduleTypeID
      ,ScheduleReasonID
      ,ScheduleCanceledReasonID
      ,ScheduleCanceledTime
      ,ScheduleCancelledByID
      ,ScheduleMoveReasonID
      ,ScheduleMovetime
      ,ScheduleServicePointID
      ,ScheduleServicePointSessionID
      ,ScheduleMoveCount
      ,ScheduleLocationTypeID
      ,ScheduleLocationDescription
      ,ScheduleLocationTypeHealthOrgID
      ,ScheduleWaitingListID
      ,KornerWait
      ,BreachDate
      ,NationalBreachDate
      ,BreachDays
      ,NationalBreachDays
      ,RTTBreachDate
      ,ClockStartDate
      ,ScheduleOutcomeCode
      ,PatientDeathIndicator
      ,CountOfDNAs
      ,CountOfHospitalCancels
      ,CountOfPatientCancels
      ,LastAppointmentFlag
      ,AdditionFlag
      ,FuturePatientCancelDate
      ,PCTCode
      ,StartWaitDate
      ,EndWaitDate
      ,BookedTime
      ,DerivedFirstAttendanceFlag
	  ,DerivedWaitFlag = 1
	,Created = GETDATE()
	,Updated = GETDATE()
	,ByWhom = SUSER_NAME()
FROM
	dbo.TLoadCOMReferralWait


SELECT @RowsInserted = @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())
SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@CensusDate, ''))

EXEC Utility.WriteAuditLogEvent 'LoadCOMWait', @Stats, @StartTime

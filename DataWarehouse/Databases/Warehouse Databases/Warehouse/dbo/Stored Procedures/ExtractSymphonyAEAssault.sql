﻿
	CREATE procedure [dbo].[ExtractSymphonyAEAssault]

	as

	declare @StartTime datetime
	declare @Elapsed int
	declare @RowsDeleted Int
	declare @RowsInserted Int
	declare @Stats varchar(255)


	select @StartTime = getdate()

	--1. AssaultDate
	INSERT INTO [dbo].[TImportAEAssault]
	(
		 AESourceUniqueID
		,AssaultDate
		,AssaultTime
		,AssaultWeapon
		,AssaultWeaponDetails
		,AssaultLocation
		,AssaultLocationDetails
		,AlcoholConsumed3Hour
		,AssaultRelationship
		,AssaultRelationshipDetails
	)


	select 
		 Attendance.AESourceUniqueID
		,AssaultDate = DateOfAssault.AssaultDate
		,TimeOfAssault.AssaultTime
		,Weapon.Weapon
		,WeaponDetails.WeaponDetails
		,ALocation.ALocation
		,ALocationDetails.ALocationDetails
		,Cons3Hour.Cons3Hour
		,Relationship.Relationship
		,RelationshipDetails.RelationshipDetails
	from 
		(
		select distinct
			AESourceUniqueID = atd_EF_atdid
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields
		where 
			atd_EF_FieldID in ( 2014 , 2005 , 2006 , 2012 , 2004 , 2007 , 2010 , 2011 , 2013)
		) Attendance
			
	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,AssaultDate = atd_EF_Value
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields 
		where 
			atd_EF_FieldID = 2013
		) DateOfAssault 
	on DateOfAssault.AESourceUniqueID = Attendance.AESourceUniqueID	
			
	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,AssaultTime = atd_EF_Value
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields 
		where 
			atd_EF_FieldID = 2014
		)TimeOfAssault 
	on TimeOfAssault.AESourceUniqueID = Attendance.AESourceUniqueID		
			
	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,Weapon = Lkp_Name
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields

		inner join [$(Symphony)].dbo.Lookups l
		on	l.Lkp_ID = atd_EF_Value

		where 
			atd_EF_FieldID = 2005
		) Weapon 
	on	Weapon.AESourceUniqueID = Attendance.AESourceUniqueID		

	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,WeaponDetails = atd_EF_Value
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields 
		where 
			atd_EF_FieldID = 2006
		)WeaponDetails 
	on	WeaponDetails.AESourceUniqueID = Attendance.AESourceUniqueID

	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,ALocation = Lkp_Name
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields

		inner join [$(Symphony)].dbo.Lookups l on l.Lkp_ID = atd_EF_Value
		where 
			atd_EF_FieldID = 2012
		)ALocation 
	on	ALocation.AESourceUniqueID = Attendance.AESourceUniqueID

	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,ALocationDetails = atd_EF_Value
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields 
		where 
			atd_EF_FieldID = 2004
		) ALocationDetails 
	on	ALocationDetails.AESourceUniqueID = Attendance.AESourceUniqueID

	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,Cons3Hour = l.Lkp_Name 
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields 

		inner join [$(Symphony)].dbo.Lookups l
		on	l.Lkp_ID = atd_EF_Value

		where 
			atd_EF_FieldID = 2007
		) Cons3Hour 
	on Cons3Hour.AESourceUniqueID = Attendance.AESourceUniqueID

	left join 
		(
		select  
			 AESourceUniqueID = atd_EF_atdid
			,Relationship = l.Lkp_Name
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields

		inner join [$(Symphony)].dbo.Lookups l
		on	l.Lkp_ID = atd_EF_Value

		where 
			atd_EF_FieldID = 2010
		)Relationship 
	on	Relationship.AESourceUniqueID = Attendance.AESourceUniqueID

	left join 
		(
		select 
			 AESourceUniqueID = atd_EF_atdid
			,RelationshipDetails = atd_EF_Value
		from 
			[$(Symphony)].dbo.Attendance_Details_ExtraFields
		where 
			atd_EF_FieldID = 2011
		) RelationshipDetails 
	on	RelationshipDetails.AESourceUniqueID = Attendance.AESourceUniqueID


	select @RowsInserted = @@rowcount

	select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	select @Stats = 
		'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

	exec WriteAuditLogEvent 'ExtractSymphonyAEAssault', @Stats, @StartTime
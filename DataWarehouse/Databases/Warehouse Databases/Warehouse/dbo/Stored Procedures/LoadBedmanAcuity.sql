﻿
CREATE PROCEDURE [dbo].[LoadBedmanAcuity]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime = NULL
declare @to smalldatetime = NULL

select @StartTime = getdate()

TRUNCATE TABLE Bedman.Acuity

INSERT INTO Bedman.Acuity

SELECT 
	   [DailyScoreID]
      ,[WardCode]
      ,[ScoreTime]
      
      ,[TotalPatientsLevel0]
      ,[TotalPatientsLevel0Score]
      ,[AcuityMutiplierLevel0]
      
      ,[TotalPatientsLevel1a]
      ,[AcuityMutiplierLevel1a]
      ,[TotalPatientsLevel1aScore]
      
      ,[TotalPatientsLevel1b]
      ,[AcuityMutiplierLevel1b]
      ,[TotalPatientsLevel1bScore]
      
      ,[TotalPatientsLevel2]
      ,[AcuityMutiplierLevel2]
      ,[TotalPatientsLevel2Score]
      
      ,[TotalPatientsLevel3]
      ,[AcuityMutiplierLevel3]
      ,[TotalPatientsLevel3Score]
      
      ,[AvailableBeds]
      
FROM 
	dbo.TLoadBedmanAcuity
	

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadBedmanAcuity', @Stats, @StartTime
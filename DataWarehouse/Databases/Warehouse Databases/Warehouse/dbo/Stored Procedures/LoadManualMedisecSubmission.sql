﻿
create proc dbo.LoadManualMedisecSubmission

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table Medisec.ManualSubmission

insert into Medisec.ManualSubmission

select [Ward]
      ,[DischargeDate]
      ,[SubmittedBy]
      ,[Total]
      ,[<24Hours]
      ,[24-48Hours]
      ,[>48Hours]
from dbo.TLoadManualMedisecSubmission

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


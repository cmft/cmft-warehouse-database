﻿
CREATE procedure [dbo].[LoadTheatreTimetableDetail]
as

/******************************************************************************************************
Stored procedure : [dbo].[LoadTheatreTimetableDetail]
Description		 : As proc name

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
18/11/2014	Paul Egan       Corrected WriteAuditLogEvent process name.
*******************************************************************************************************/

/**************************************************************************/
/* General Declarations                                                   */
/**************************************************************************/

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()


select @RowsDeleted = 
	(
	select
		count(*)
	from
		Theatre.TimetableDetail
	)

truncate table Theatre.TimetableDetail


declare @FetchStatus int

declare @SessionStartTime1_1 datetime
declare @SessionStartTime1_2 datetime
declare @SessionStartTime1_3 datetime
declare @SessionStartTime1_4 datetime
declare @SessionStartTime1_5 datetime
declare @SessionStartTime1_6 datetime
declare @SessionStartTime1_7 datetime
declare @SessionEndTime1_1 datetime
declare @SessionEndTime1_2 datetime
declare @SessionEndTime1_3 datetime
declare @SessionEndTime1_4 datetime
declare @SessionEndTime1_5 datetime
declare @SessionEndTime1_6 datetime
declare @SessionEndTime1_7 datetime
declare @SessionStartTime2_1 datetime
declare @SessionStartTime2_2 datetime
declare @SessionStartTime2_3 datetime
declare @SessionStartTime2_4 datetime
declare @SessionStartTime2_5 datetime
declare @SessionStartTime2_6 datetime
declare @SessionStartTime2_7 datetime
declare @SessionEndTime2_1 datetime
declare @SessionEndTime2_2 datetime
declare @SessionEndTime2_3 datetime
declare @SessionEndTime2_4 datetime
declare @SessionEndTime2_5 datetime
declare @SessionEndTime2_6 datetime
declare @SessionEndTime2_7 datetime
declare @SessionStartTime3_1 datetime
declare @SessionStartTime3_2 datetime
declare @SessionStartTime3_3 datetime
declare @SessionStartTime3_4 datetime
declare @SessionStartTime3_5 datetime
declare @SessionStartTime3_6 datetime
declare @SessionStartTime3_7 datetime
declare @SessionEndTime3_1 datetime
declare @SessionEndTime3_2 datetime
declare @SessionEndTime3_3 datetime
declare @SessionEndTime3_4 datetime
declare @SessionEndTime3_5 datetime
declare @SessionEndTime3_6 datetime
declare @SessionEndTime3_7 datetime
declare @SessionStartTime4_1 datetime
declare @SessionStartTime4_2 datetime
declare @SessionStartTime4_3 datetime
declare @SessionStartTime4_4 datetime
declare @SessionStartTime4_5 datetime
declare @SessionStartTime4_6 datetime
declare @SessionStartTime4_7 datetime
declare @SessionEndTime4_1 datetime
declare @SessionEndTime4_2 datetime
declare @SessionEndTime4_3 datetime
declare @SessionEndTime4_4 datetime
declare @SessionEndTime4_5 datetime
declare @SessionEndTime4_6 datetime
declare @SessionEndTime4_7 datetime
declare @AnaesthetistCode1_1 varchar(10)
declare @AnaesthetistCode1_2 varchar(10)
declare @AnaesthetistCode1_3 varchar(10)
declare @AnaesthetistCode1_4 varchar(10)
declare @AnaesthetistCode1_5 varchar(10)
declare @AnaesthetistCode1_6 varchar(10)
declare @AnaesthetistCode1_7 varchar(10)
declare @AnaesthetistCode2_1 varchar(10)
declare @AnaesthetistCode2_2 varchar(10)
declare @AnaesthetistCode2_3 varchar(10)
declare @AnaesthetistCode2_4 varchar(10)
declare @AnaesthetistCode2_5 varchar(10)
declare @AnaesthetistCode2_6 varchar(10)
declare @AnaesthetistCode2_7 varchar(10)
declare @AnaesthetistCode3_1 varchar(10)
declare @AnaesthetistCode3_2 varchar(10)
declare @AnaesthetistCode3_3 varchar(10)
declare @AnaesthetistCode3_4 varchar(10)
declare @AnaesthetistCode3_5 varchar(10)
declare @AnaesthetistCode3_6 varchar(10)
declare @AnaesthetistCode3_7 varchar(10)
declare @AnaesthetistCode4_1 varchar(10)
declare @AnaesthetistCode4_2 varchar(10)
declare @AnaesthetistCode4_3 varchar(10)
declare @AnaesthetistCode4_4 varchar(10)
declare @AnaesthetistCode4_5 varchar(10)
declare @AnaesthetistCode4_6 varchar(10)
declare @AnaesthetistCode4_7 varchar(10)
declare @ConsultantCode1_1 varchar(10)
declare @ConsultantCode1_2 varchar(10)
declare @ConsultantCode1_3 varchar(10)
declare @ConsultantCode1_4 varchar(10)
declare @ConsultantCode1_5 varchar(10)
declare @ConsultantCode1_6 varchar(10)
declare @ConsultantCode1_7 varchar(10)
declare @ConsultantCode2_1 varchar(10)
declare @ConsultantCode2_2 varchar(10)
declare @ConsultantCode2_3 varchar(10)
declare @ConsultantCode2_4 varchar(10)
declare @ConsultantCode2_5 varchar(10)
declare @ConsultantCode2_6 varchar(10)
declare @ConsultantCode2_7 varchar(10)
declare @ConsultantCode3_1 varchar(10)
declare @ConsultantCode3_2 varchar(10)
declare @ConsultantCode3_3 varchar(10)
declare @ConsultantCode3_4 varchar(10)
declare @ConsultantCode3_5 varchar(10)
declare @ConsultantCode3_6 varchar(10)
declare @ConsultantCode3_7 varchar(10)
declare @ConsultantCode4_1 varchar(10)
declare @ConsultantCode4_2 varchar(10)
declare @ConsultantCode4_3 varchar(10)
declare @ConsultantCode4_4 varchar(10)
declare @ConsultantCode4_5 varchar(10)
declare @ConsultantCode4_6 varchar(10)
declare @ConsultantCode4_7 varchar(10)
declare @SpecialtyCode1_1 varchar(8)
declare @SpecialtyCode1_2 varchar(8)
declare @SpecialtyCode1_3 varchar(8)
declare @SpecialtyCode1_4 varchar(8)
declare @SpecialtyCode1_5 varchar(8)
declare @SpecialtyCode1_6 varchar(8)
declare @SpecialtyCode1_7 varchar(8)
declare @SpecialtyCode2_1 varchar(8)
declare @SpecialtyCode2_2 varchar(8)
declare @SpecialtyCode2_3 varchar(8)
declare @SpecialtyCode2_4 varchar(8)
declare @SpecialtyCode2_5 varchar(8)
declare @SpecialtyCode2_6 varchar(8)
declare @SpecialtyCode2_7 varchar(8)
declare @SpecialtyCode3_1 varchar(8)
declare @SpecialtyCode3_2 varchar(8)
declare @SpecialtyCode3_3 varchar(8)
declare @SpecialtyCode3_4 varchar(8)
declare @SpecialtyCode3_5 varchar(8)
declare @SpecialtyCode3_6 varchar(8)
declare @SpecialtyCode3_7 varchar(8)
declare @SpecialtyCode4_1 varchar(8)
declare @SpecialtyCode4_2 varchar(8)
declare @SpecialtyCode4_3 varchar(8)
declare @SpecialtyCode4_4 varchar(8)
declare @SpecialtyCode4_5 varchar(8)
declare @SpecialtyCode4_6 varchar(8)
declare @SpecialtyCode4_7 varchar(8)
declare @StartDate datetime
declare @EndDate datetime
declare @SessionMinutes_1 numeric(9, 2)
declare @SessionMinutes_2 numeric(9, 2)
declare @SessionMinutes_3 numeric(9, 2)
declare @SessionMinutes_4 numeric(9, 2)
declare @SessionMinutes_5 numeric(9, 2)
declare @SessionMinutes_6 numeric(9, 2)
declare @SessionMinutes_7 numeric(9, 2)
declare @TimetableTemplateCode numeric(9, 0)
declare @LogLastUpdated datetime
declare @RecordLogDetails varchar(255)
declare @TimetableDetailCode numeric(9, 0)
declare @TheatreCode numeric(9, 0)


/**************************************************************************/
/* Declaration for trigger cursor                                         */
/**************************************************************************/

declare TableCursor CURSOR fast_forward FOR

select
	 SessionStartTime1_1 = TI_1WK1S
	,SessionStartTime1_2 = TI_1WK2S
	,SessionStartTime1_3 = TI_1WK3S
	,SessionStartTime1_4 = TI_1WK4S
	,SessionStartTime1_5 = TI_1WK5S
	,SessionStartTime1_6 = TI_1WK6S
	,SessionStartTime1_7 = TI_1WK7S

	,SessionEndTime1_1 = TI_1WK1F
	,SessionEndTime1_2 = TI_1WK2F
	,SessionEndTime1_3 = TI_1WK3F
	,SessionEndTime1_4 = TI_1WK4F
	,SessionEndTime1_5 = TI_1WK5F
	,SessionEndTime1_6 = TI_1WK6F
	,SessionEndTime1_7 = TI_1WK7F

	,SessionStartTime2_1 = TI_2WK1S
	,SessionStartTime2_2 = TI_2WK2S
	,SessionStartTime2_3 = TI_2WK3S
	,SessionStartTime2_4 = TI_2WK4S
	,SessionStartTime2_5 = TI_2WK5S
	,SessionStartTime2_6 = TI_2WK6S
	,SessionStartTime2_7 = TI_2WK7S

	,SessionEndTime2_1 = TI_2WK1F
	,SessionEndTime2_2 = TI_2WK2F
	,SessionEndTime2_3 = TI_2WK3F
	,SessionEndTime2_4 = TI_2WK4F
	,SessionEndTime2_5 = TI_2WK5F
	,SessionEndTime2_6 = TI_2WK6F
	,SessionEndTime2_7 = TI_2WK7F

	,SessionStartTime3_1 = TI_3WK1S
	,SessionStartTime3_2 = TI_3WK2S
	,SessionStartTime3_3 = TI_3WK3S
	,SessionStartTime3_4 = TI_3WK4S
	,SessionStartTime3_5 = TI_3WK5S
	,SessionStartTime3_6 = TI_3WK6S
	,SessionStartTime3_7 = TI_3WK7S

	,SessionEndTime3_1 = TI_3WK1F
	,SessionEndTime3_2 = TI_3WK2F
	,SessionEndTime3_3 = TI_3WK3F
	,SessionEndTime3_4 = TI_3WK4F
	,SessionEndTime3_5 = TI_3WK5F
	,SessionEndTime3_6 = TI_3WK6F
	,SessionEndTime3_7 = TI_3WK7F

	,SessionStartTime4_1 = TI_4WK1S
	,SessionStartTime4_2 = TI_4WK2S
	,SessionStartTime4_3 = TI_4WK3S
	,SessionStartTime4_4 = TI_4WK4S
	,SessionStartTime4_5 = TI_4WK5S
	,SessionStartTime4_6 = TI_4WK6S
	,SessionStartTime4_7 = TI_4WK7S

	,SessionEndTime4_1 = TI_4WK1F
	,SessionEndTime4_2 = TI_4WK2F
	,SessionEndTime4_3 = TI_4WK3F
	,SessionEndTime4_4 = TI_4WK4F
	,SessionEndTime4_5 = TI_4WK5F
	,SessionEndTime4_6 = TI_4WK6F
	,SessionEndTime4_7 = TI_4WK7F

	,AnaesthetistCode1_1 = case when TI_AN_D1S1 = '' then null else TI_AN_D1S1 end
	,AnaesthetistCode1_2 = case when TI_AN_D2S1 = '' then null else TI_AN_D2S1 end
	,AnaesthetistCode1_3 = case when TI_AN_D3S1 = '' then null else TI_AN_D3S1 end
	,AnaesthetistCode1_4 = case when TI_AN_D4S1 = '' then null else TI_AN_D4S1 end
	,AnaesthetistCode1_5 = case when TI_AN_D5S1 = '' then null else TI_AN_D5S1 end
	,AnaesthetistCode1_6 = case when TI_AN_D6S1 = '' then null else TI_AN_D6S1 end
	,AnaesthetistCode1_7 = case when TI_AN_D7S1 = '' then null else TI_AN_D7S1 end

	,AnaesthetistCode2_1 = case when TI_AN_D1S2 = '' then null else TI_AN_D1S2 end
	,AnaesthetistCode2_2 = case when TI_AN_D2S2 = '' then null else TI_AN_D2S2 end
	,AnaesthetistCode2_3 = case when TI_AN_D3S2 = '' then null else TI_AN_D3S2 end
	,AnaesthetistCode2_4 = case when TI_AN_D4S2 = '' then null else TI_AN_D4S2 end
	,AnaesthetistCode2_5 = case when TI_AN_D5S2 = '' then null else TI_AN_D5S2 end
	,AnaesthetistCode2_6 = case when TI_AN_D6S2 = '' then null else TI_AN_D6S2 end
	,AnaesthetistCode2_7 = case when TI_AN_D7S2 = '' then null else TI_AN_D7S2 end

	,AnaesthetistCode3_1 = case when TI_AN_D1S3 = '' then null else TI_AN_D1S3 end
	,AnaesthetistCode3_2 = case when TI_AN_D2S3 = '' then null else TI_AN_D2S3 end
	,AnaesthetistCode3_3 = case when TI_AN_D3S3 = '' then null else TI_AN_D3S3 end
	,AnaesthetistCode3_4 = case when TI_AN_D4S3 = '' then null else TI_AN_D4S3 end
	,AnaesthetistCode3_5 = case when TI_AN_D5S3 = '' then null else TI_AN_D5S3 end
	,AnaesthetistCode3_6 = case when TI_AN_D6S3 = '' then null else TI_AN_D6S3 end
	,AnaesthetistCode3_7 = case when TI_AN_D7S3 = '' then null else TI_AN_D7S3 end

	,AnaesthetistCode4_1 = case when TI_AN_D1S4 = '' then null else TI_AN_D1S4 end
	,AnaesthetistCode4_2 = case when TI_AN_D2S4 = '' then null else TI_AN_D2S4 end
	,AnaesthetistCode4_3 = case when TI_AN_D3S4 = '' then null else TI_AN_D3S4 end
	,AnaesthetistCode4_4 = case when TI_AN_D4S4 = '' then null else TI_AN_D4S4 end
	,AnaesthetistCode4_5 = case when TI_AN_D5S4 = '' then null else TI_AN_D5S4 end
	,AnaesthetistCode4_6 = case when TI_AN_D6S4 = '' then null else TI_AN_D6S4 end
	,AnaesthetistCode4_7 = case when TI_AN_D7S4 = '' then null else TI_AN_D7S4 end

	,ConsultantCode1_1 = case when TI_CO_D1S1 = '' then null else TI_CO_D1S1 end
	,ConsultantCode1_2 = case when TI_CO_D2S1 = '' then null else TI_CO_D2S1 end
	,ConsultantCode1_3 = case when TI_CO_D3S1 = '' then null else TI_CO_D3S1 end
	,ConsultantCode1_4 = case when TI_CO_D4S1 = '' then null else TI_CO_D4S1 end
	,ConsultantCode1_5 = case when TI_CO_D5S1 = '' then null else TI_CO_D5S1 end
	,ConsultantCode1_6 = case when TI_CO_D6S1 = '' then null else TI_CO_D6S1 end
	,ConsultantCode1_7 = case when TI_CO_D7S1 = '' then null else TI_CO_D7S1 end

	,ConsultantCode2_1 = case when TI_CO_D1S2 = '' then null else TI_CO_D1S2 end
	,ConsultantCode2_2 = case when TI_CO_D2S2 = '' then null else TI_CO_D2S2 end
	,ConsultantCode2_3 = case when TI_CO_D3S2 = '' then null else TI_CO_D3S2 end
	,ConsultantCode2_4 = case when TI_CO_D4S2 = '' then null else TI_CO_D4S2 end
	,ConsultantCode2_5 = case when TI_CO_D5S2 = '' then null else TI_CO_D5S2 end
	,ConsultantCode2_6 = case when TI_CO_D6S2 = '' then null else TI_CO_D6S2 end
	,ConsultantCode2_7 = case when TI_CO_D7S2 = '' then null else TI_CO_D7S2 end

	,ConsultantCode3_1 = case when TI_CO_D1S3 = '' then null else TI_CO_D1S3 end
	,ConsultantCode3_2 = case when TI_CO_D2S3 = '' then null else TI_CO_D2S3 end
	,ConsultantCode3_3 = case when TI_CO_D3S3 = '' then null else TI_CO_D3S3 end
	,ConsultantCode3_4 = case when TI_CO_D4S3 = '' then null else TI_CO_D4S3 end
	,ConsultantCode3_5 = case when TI_CO_D5S3 = '' then null else TI_CO_D5S3 end
	,ConsultantCode3_6 = case when TI_CO_D6S3 = '' then null else TI_CO_D6S3 end
	,ConsultantCode3_7 = case when TI_CO_D7S3 = '' then null else TI_CO_D7S3 end

	,ConsultantCode4_1 = case when TI_CO_D1S4 = '' then null else TI_CO_D1S4 end
	,ConsultantCode4_2 = case when TI_CO_D2S4 = '' then null else TI_CO_D2S4 end
	,ConsultantCode4_3 = case when TI_CO_D3S4 = '' then null else TI_CO_D3S4 end
	,ConsultantCode4_4 = case when TI_CO_D4S4 = '' then null else TI_CO_D4S4 end
	,ConsultantCode4_5 = case when TI_CO_D5S4 = '' then null else TI_CO_D5S4 end
	,ConsultantCode4_6 = case when TI_CO_D6S4 = '' then null else TI_CO_D6S4 end
	,ConsultantCode4_7 = case when TI_CO_D7S4 = '' then null else TI_CO_D7S4 end


	,SpecialtyCode1_1 = case when TI_SP_D1S1 = '' then null else TI_SP_D1S1 end
	,SpecialtyCode1_2 = case when TI_SP_D2S1 = '' then null else TI_SP_D2S1 end
	,SpecialtyCode1_3 = case when TI_SP_D3S1 = '' then null else TI_SP_D3S1 end
	,SpecialtyCode1_4 = case when TI_SP_D4S1 = '' then null else TI_SP_D4S1 end
	,SpecialtyCode1_5 = case when TI_SP_D5S1 = '' then null else TI_SP_D5S1 end
	,SpecialtyCode1_6 = case when TI_SP_D6S1 = '' then null else TI_SP_D6S1 end
	,SpecialtyCode1_7 = case when TI_SP_D7S1 = '' then null else TI_SP_D7S1 end

	,SpecialtyCode2_1 = case when TI_SP_D1S2 = '' then null else TI_SP_D1S2 end
	,SpecialtyCode2_2 = case when TI_SP_D2S2 = '' then null else TI_SP_D2S2 end
	,SpecialtyCode2_3 = case when TI_SP_D3S2 = '' then null else TI_SP_D3S2 end
	,SpecialtyCode2_4 = case when TI_SP_D4S2 = '' then null else TI_SP_D4S2 end
	,SpecialtyCode2_5 = case when TI_SP_D5S2 = '' then null else TI_SP_D5S2 end
	,SpecialtyCode2_6 = case when TI_SP_D6S2 = '' then null else TI_SP_D6S2 end
	,SpecialtyCode2_7 = case when TI_SP_D7S2 = '' then null else TI_SP_D7S2 end

	,SpecialtyCode3_1 = case when TI_SP_D1S3 = '' then null else TI_SP_D1S3 end
	,SpecialtyCode3_2 = case when TI_SP_D2S3 = '' then null else TI_SP_D2S3 end
	,SpecialtyCode3_3 = case when TI_SP_D3S3 = '' then null else TI_SP_D3S3 end
	,SpecialtyCode3_4 = case when TI_SP_D4S3 = '' then null else TI_SP_D4S3 end
	,SpecialtyCode3_5 = case when TI_SP_D5S3 = '' then null else TI_SP_D5S3 end
	,SpecialtyCode3_6 = case when TI_SP_D6S3 = '' then null else TI_SP_D6S3 end
	,SpecialtyCode3_7 = case when TI_SP_D7S3 = '' then null else TI_SP_D7S3 end

	,SpecialtyCode4_1 = case when TI_SP_D1S4 = '' then null else TI_SP_D1S4 end
	,SpecialtyCode4_2 = case when TI_SP_D2S4 = '' then null else TI_SP_D2S4 end
	,SpecialtyCode4_3 = case when TI_SP_D3S4 = '' then null else TI_SP_D3S4 end
	,SpecialtyCode4_4 = case when TI_SP_D4S4 = '' then null else TI_SP_D4S4 end
	,SpecialtyCode4_5 = case when TI_SP_D5S4 = '' then null else TI_SP_D5S4 end
	,SpecialtyCode4_6 = case when TI_SP_D6S4 = '' then null else TI_SP_D6S4 end
	,SpecialtyCode4_7 = case when TI_SP_D7S4 = '' then null else TI_SP_D7S4 end

	,StartDate = TI_DATS
	,EndDate = TI_DATF

	,SessionMinutes_1 = TI_HRS1
	,SessionMinutes_2 = TI_HRS2
	,SessionMinutes_3 = TI_HRS3
	,SessionMinutes_4 = TI_HRS4
	,SessionMinutes_5 = TI_HRS5
	,SessionMinutes_6 = TI_HRS6
	,SessionMinutes_7 = TI_HRS7

	,TimetableTemplateCode = TI_IT_SEQU
	,LogLastUpdated = TI_LOG_DATE
	,RecordLogDetails = case when TI_LOG_DETAILS_FILLER = '' then null else TI_LOG_DETAILS_FILLER end
	,TimetableDetailCode = TI_SEQU
	,TheatreCode = TI_TH_SEQU
from
	----------otrpd_TEST.dbo.FTIMETB	-- DEV only. Paul Egan 18/11/2014
	[$(otprd)].dbo.FTIMETB

/**************************************************************************/
/* Initialisation                                                         */
/**************************************************************************/

OPEN TableCursor

FETCH NEXT FROM TableCursor INTO
	 @SessionStartTime1_1
	,@SessionStartTime1_2
	,@SessionStartTime1_3
	,@SessionStartTime1_4
	,@SessionStartTime1_5
	,@SessionStartTime1_6
	,@SessionStartTime1_7
	,@SessionEndTime1_1
	,@SessionEndTime1_2
	,@SessionEndTime1_3
	,@SessionEndTime1_4
	,@SessionEndTime1_5
	,@SessionEndTime1_6
	,@SessionEndTime1_7
	,@SessionStartTime2_1
	,@SessionStartTime2_2
	,@SessionStartTime2_3
	,@SessionStartTime2_4
	,@SessionStartTime2_5
	,@SessionStartTime2_6
	,@SessionStartTime2_7
	,@SessionEndTime2_1
	,@SessionEndTime2_2
	,@SessionEndTime2_3
	,@SessionEndTime2_4
	,@SessionEndTime2_5
	,@SessionEndTime2_6
	,@SessionEndTime2_7
	,@SessionStartTime3_1
	,@SessionStartTime3_2
	,@SessionStartTime3_3
	,@SessionStartTime3_4
	,@SessionStartTime3_5
	,@SessionStartTime3_6
	,@SessionStartTime3_7
	,@SessionEndTime3_1
	,@SessionEndTime3_2
	,@SessionEndTime3_3
	,@SessionEndTime3_4
	,@SessionEndTime3_5
	,@SessionEndTime3_6
	,@SessionEndTime3_7
	,@SessionStartTime4_1
	,@SessionStartTime4_2
	,@SessionStartTime4_3
	,@SessionStartTime4_4
	,@SessionStartTime4_5
	,@SessionStartTime4_6
	,@SessionStartTime4_7
	,@SessionEndTime4_1
	,@SessionEndTime4_2
	,@SessionEndTime4_3
	,@SessionEndTime4_4
	,@SessionEndTime4_5
	,@SessionEndTime4_6
	,@SessionEndTime4_7
	,@AnaesthetistCode1_1
	,@AnaesthetistCode1_2
	,@AnaesthetistCode1_3
	,@AnaesthetistCode1_4
	,@AnaesthetistCode1_5
	,@AnaesthetistCode1_6
	,@AnaesthetistCode1_7
	,@AnaesthetistCode2_1
	,@AnaesthetistCode2_2
	,@AnaesthetistCode2_3
	,@AnaesthetistCode2_4
	,@AnaesthetistCode2_5
	,@AnaesthetistCode2_6
	,@AnaesthetistCode2_7
	,@AnaesthetistCode3_1
	,@AnaesthetistCode3_2
	,@AnaesthetistCode3_3
	,@AnaesthetistCode3_4
	,@AnaesthetistCode3_5
	,@AnaesthetistCode3_6
	,@AnaesthetistCode3_7
	,@AnaesthetistCode4_1
	,@AnaesthetistCode4_2
	,@AnaesthetistCode4_3
	,@AnaesthetistCode4_4
	,@AnaesthetistCode4_5
	,@AnaesthetistCode4_6
	,@AnaesthetistCode4_7
	,@ConsultantCode1_1
	,@ConsultantCode1_2
	,@ConsultantCode1_3
	,@ConsultantCode1_4
	,@ConsultantCode1_5
	,@ConsultantCode1_6
	,@ConsultantCode1_7
	,@ConsultantCode2_1
	,@ConsultantCode2_2
	,@ConsultantCode2_3
	,@ConsultantCode2_4
	,@ConsultantCode2_5
	,@ConsultantCode2_6
	,@ConsultantCode2_7
	,@ConsultantCode3_1
	,@ConsultantCode3_2
	,@ConsultantCode3_3
	,@ConsultantCode3_4
	,@ConsultantCode3_5
	,@ConsultantCode3_6
	,@ConsultantCode3_7
	,@ConsultantCode4_1
	,@ConsultantCode4_2
	,@ConsultantCode4_3
	,@ConsultantCode4_4
	,@ConsultantCode4_5
	,@ConsultantCode4_6
	,@ConsultantCode4_7
	,@SpecialtyCode1_1
	,@SpecialtyCode1_2
	,@SpecialtyCode1_3
	,@SpecialtyCode1_4
	,@SpecialtyCode1_5
	,@SpecialtyCode1_6
	,@SpecialtyCode1_7
	,@SpecialtyCode2_1
	,@SpecialtyCode2_2
	,@SpecialtyCode2_3
	,@SpecialtyCode2_4
	,@SpecialtyCode2_5
	,@SpecialtyCode2_6
	,@SpecialtyCode2_7
	,@SpecialtyCode3_1
	,@SpecialtyCode3_2
	,@SpecialtyCode3_3
	,@SpecialtyCode3_4
	,@SpecialtyCode3_5
	,@SpecialtyCode3_6
	,@SpecialtyCode3_7
	,@SpecialtyCode4_1
	,@SpecialtyCode4_2
	,@SpecialtyCode4_3
	,@SpecialtyCode4_4
	,@SpecialtyCode4_5
	,@SpecialtyCode4_6
	,@SpecialtyCode4_7
	,@StartDate
	,@EndDate
	,@SessionMinutes_1
	,@SessionMinutes_2
	,@SessionMinutes_3
	,@SessionMinutes_4
	,@SessionMinutes_5
	,@SessionMinutes_6
	,@SessionMinutes_7
	,@TimetableTemplateCode
	,@LogLastUpdated
	,@RecordLogDetails
	,@TimetableDetailCode
	,@TheatreCode

Select @FetchStatus = @@FETCH_STATUS


/**************************************************************************/
/* Main Processing Loop                                                   */
/**************************************************************************/
WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

--DayNumber = 1 is Sunday.
begin
	insert into Theatre.TimetableDetail
	select
		 @TimetableDetailCode
		,SessionNumber = 1
		,DayNumber = 2
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime1_1
		,@SessionEndTime1_1
		,@ConsultantCode1_1
		,@AnaesthetistCode1_1
		,@SpecialtyCode1_1
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_1

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 1
		,DayNumber = 3
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime1_2
		,@SessionEndTime1_2
		,@ConsultantCode1_2
		,@AnaesthetistCode1_2
		,@SpecialtyCode1_2
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_2

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 1
		,DayNumber = 4
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime1_3
		,@SessionEndTime1_3
		,@ConsultantCode1_3
		,@AnaesthetistCode1_3
		,@SpecialtyCode1_3
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_3

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 1
		,DayNumber = 5
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime1_4
		,@SessionEndTime1_4
		,@ConsultantCode1_4
		,@AnaesthetistCode1_4
		,@SpecialtyCode1_4
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_4

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 1
		,DayNumber = 6
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime1_5
		,@SessionEndTime1_5
		,@ConsultantCode1_5
		,@AnaesthetistCode1_5
		,@SpecialtyCode1_5
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_5

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 1
		,DayNumber = 7
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime1_6
		,@SessionEndTime1_6
		,@ConsultantCode1_6
		,@AnaesthetistCode1_6
		,@SpecialtyCode1_6
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_6

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 1
		,DayNumber = 1
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime1_7
		,@SessionEndTime1_7
		,@ConsultantCode1_7
		,@AnaesthetistCode1_7
		,@SpecialtyCode1_7
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_7

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 2
		,DayNumber = 2
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime2_1
		,@SessionEndTime2_1
		,@ConsultantCode2_1
		,@AnaesthetistCode2_1
		,@SpecialtyCode2_1
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_1

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 2
		,DayNumber = 3
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime2_2
		,@SessionEndTime2_2
		,@ConsultantCode2_2
		,@AnaesthetistCode2_2
		,@SpecialtyCode2_2
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_2

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 2
		,DayNumber = 4
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime2_3
		,@SessionEndTime2_3
		,@ConsultantCode2_3
		,@AnaesthetistCode2_3
		,@SpecialtyCode2_3
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_3

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 2
		,DayNumber = 5
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime2_4
		,@SessionEndTime2_4
		,@ConsultantCode2_4
		,@AnaesthetistCode2_4
		,@SpecialtyCode2_4
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_4

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 2
		,DayNumber = 6
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime2_5
		,@SessionEndTime2_5
		,@ConsultantCode2_5
		,@AnaesthetistCode2_5
		,@SpecialtyCode2_5
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_5

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 2
		,DayNumber = 7
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime2_6
		,@SessionEndTime2_6
		,@ConsultantCode2_6
		,@AnaesthetistCode2_6
		,@SpecialtyCode2_6
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_6

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 2
		,DayNumber = 1
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime2_7
		,@SessionEndTime2_7
		,@ConsultantCode2_7
		,@AnaesthetistCode2_7
		,@SpecialtyCode2_7
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_7

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 3
		,DayNumber = 2
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime3_1
		,@SessionEndTime3_1
		,@ConsultantCode3_1
		,@AnaesthetistCode3_1
		,@SpecialtyCode3_1
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_1

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 3
		,DayNumber = 3
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime3_2
		,@SessionEndTime3_2
		,@ConsultantCode3_2
		,@AnaesthetistCode3_2
		,@SpecialtyCode3_2
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_2

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 3
		,DayNumber = 4
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime3_3
		,@SessionEndTime3_3
		,@ConsultantCode3_3
		,@AnaesthetistCode3_3
		,@SpecialtyCode3_3
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_3

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 3
		,DayNumber = 5
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime3_4
		,@SessionEndTime3_4
		,@ConsultantCode3_4
		,@AnaesthetistCode3_4
		,@SpecialtyCode3_4
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_4

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 3
		,DayNumber = 6
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime3_5
		,@SessionEndTime3_5
		,@ConsultantCode3_5
		,@AnaesthetistCode3_5
		,@SpecialtyCode3_5
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_5

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 3
		,DayNumber = 7
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime3_6
		,@SessionEndTime3_6
		,@ConsultantCode3_6
		,@AnaesthetistCode3_6
		,@SpecialtyCode3_6
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_6

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 3
		,DayNumber = 1
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime3_7
		,@SessionEndTime3_7
		,@ConsultantCode3_7
		,@AnaesthetistCode3_7
		,@SpecialtyCode3_7
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_7


	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 4
		,DayNumber = 2
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime4_1
		,@SessionEndTime4_1
		,@ConsultantCode4_1
		,@AnaesthetistCode4_1
		,@SpecialtyCode4_1
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_1

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 4
		,DayNumber = 3
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime4_2
		,@SessionEndTime4_2
		,@ConsultantCode4_2
		,@AnaesthetistCode4_2
		,@SpecialtyCode4_2
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_2

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 4
		,DayNumber = 4
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime4_3
		,@SessionEndTime4_3
		,@ConsultantCode4_3
		,@AnaesthetistCode4_3
		,@SpecialtyCode4_3
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_3

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 4
		,DayNumber = 5
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime4_4
		,@SessionEndTime4_4
		,@ConsultantCode4_4
		,@AnaesthetistCode4_4
		,@SpecialtyCode4_4
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_4

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 4
		,DayNumber = 6
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime4_5
		,@SessionEndTime4_5
		,@ConsultantCode4_5
		,@AnaesthetistCode4_5
		,@SpecialtyCode4_5
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_5

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 4
		,DayNumber = 7
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime4_6
		,@SessionEndTime4_6
		,@ConsultantCode4_6
		,@AnaesthetistCode4_6
		,@SpecialtyCode4_6
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_6

	union all

	select
		 @TimetableDetailCode
		,SessionNumber = 4
		,DayNumber = 1
		,@StartDate
		,@EndDate
		,@TheatreCode
		,@SessionStartTime4_7
		,@SessionEndTime4_7
		,@ConsultantCode4_7
		,@AnaesthetistCode4_7
		,@SpecialtyCode4_7
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@SessionMinutes_7

	FETCH NEXT FROM TableCursor INTO
		 @SessionStartTime1_1
		,@SessionStartTime1_2
		,@SessionStartTime1_3
		,@SessionStartTime1_4
		,@SessionStartTime1_5
		,@SessionStartTime1_6
		,@SessionStartTime1_7
		,@SessionEndTime1_1
		,@SessionEndTime1_2
		,@SessionEndTime1_3
		,@SessionEndTime1_4
		,@SessionEndTime1_5
		,@SessionEndTime1_6
		,@SessionEndTime1_7
		,@SessionStartTime2_1
		,@SessionStartTime2_2
		,@SessionStartTime2_3
		,@SessionStartTime2_4
		,@SessionStartTime2_5
		,@SessionStartTime2_6
		,@SessionStartTime2_7
		,@SessionEndTime2_1
		,@SessionEndTime2_2
		,@SessionEndTime2_3
		,@SessionEndTime2_4
		,@SessionEndTime2_5
		,@SessionEndTime2_6
		,@SessionEndTime2_7
		,@SessionStartTime3_1
		,@SessionStartTime3_2
		,@SessionStartTime3_3
		,@SessionStartTime3_4
		,@SessionStartTime3_5
		,@SessionStartTime3_6
		,@SessionStartTime3_7
		,@SessionEndTime3_1
		,@SessionEndTime3_2
		,@SessionEndTime3_3
		,@SessionEndTime3_4
		,@SessionEndTime3_5
		,@SessionEndTime3_6
		,@SessionEndTime3_7
		,@SessionStartTime4_1
		,@SessionStartTime4_2
		,@SessionStartTime4_3
		,@SessionStartTime4_4
		,@SessionStartTime4_5
		,@SessionStartTime4_6
		,@SessionStartTime4_7
		,@SessionEndTime4_1
		,@SessionEndTime4_2
		,@SessionEndTime4_3
		,@SessionEndTime4_4
		,@SessionEndTime4_5
		,@SessionEndTime4_6
		,@SessionEndTime4_7
		,@AnaesthetistCode1_1
		,@AnaesthetistCode1_2
		,@AnaesthetistCode1_3
		,@AnaesthetistCode1_4
		,@AnaesthetistCode1_5
		,@AnaesthetistCode1_6
		,@AnaesthetistCode1_7
		,@AnaesthetistCode2_1
		,@AnaesthetistCode2_2
		,@AnaesthetistCode2_3
		,@AnaesthetistCode2_4
		,@AnaesthetistCode2_5
		,@AnaesthetistCode2_6
		,@AnaesthetistCode2_7
		,@AnaesthetistCode3_1
		,@AnaesthetistCode3_2
		,@AnaesthetistCode3_3
		,@AnaesthetistCode3_4
		,@AnaesthetistCode3_5
		,@AnaesthetistCode3_6
		,@AnaesthetistCode3_7
		,@AnaesthetistCode4_1
		,@AnaesthetistCode4_2
		,@AnaesthetistCode4_3
		,@AnaesthetistCode4_4
		,@AnaesthetistCode4_5
		,@AnaesthetistCode4_6
		,@AnaesthetistCode4_7
		,@ConsultantCode1_1
		,@ConsultantCode1_2
		,@ConsultantCode1_3
		,@ConsultantCode1_4
		,@ConsultantCode1_5
		,@ConsultantCode1_6
		,@ConsultantCode1_7
		,@ConsultantCode2_1
		,@ConsultantCode2_2
		,@ConsultantCode2_3
		,@ConsultantCode2_4
		,@ConsultantCode2_5
		,@ConsultantCode2_6
		,@ConsultantCode2_7
		,@ConsultantCode3_1
		,@ConsultantCode3_2
		,@ConsultantCode3_3
		,@ConsultantCode3_4
		,@ConsultantCode3_5
		,@ConsultantCode3_6
		,@ConsultantCode3_7
		,@ConsultantCode4_1
		,@ConsultantCode4_2
		,@ConsultantCode4_3
		,@ConsultantCode4_4
		,@ConsultantCode4_5
		,@ConsultantCode4_6
		,@ConsultantCode4_7
		,@SpecialtyCode1_1
		,@SpecialtyCode1_2
		,@SpecialtyCode1_3
		,@SpecialtyCode1_4
		,@SpecialtyCode1_5
		,@SpecialtyCode1_6
		,@SpecialtyCode1_7
		,@SpecialtyCode2_1
		,@SpecialtyCode2_2
		,@SpecialtyCode2_3
		,@SpecialtyCode2_4
		,@SpecialtyCode2_5
		,@SpecialtyCode2_6
		,@SpecialtyCode2_7
		,@SpecialtyCode3_1
		,@SpecialtyCode3_2
		,@SpecialtyCode3_3
		,@SpecialtyCode3_4
		,@SpecialtyCode3_5
		,@SpecialtyCode3_6
		,@SpecialtyCode3_7
		,@SpecialtyCode4_1
		,@SpecialtyCode4_2
		,@SpecialtyCode4_3
		,@SpecialtyCode4_4
		,@SpecialtyCode4_5
		,@SpecialtyCode4_6
		,@SpecialtyCode4_7
		,@StartDate
		,@EndDate
		,@SessionMinutes_1
		,@SessionMinutes_2
		,@SessionMinutes_3
		,@SessionMinutes_4
		,@SessionMinutes_5
		,@SessionMinutes_6
		,@SessionMinutes_7
		,@TimetableTemplateCode
		,@LogLastUpdated
		,@RecordLogDetails
		,@TimetableDetailCode
		,@TheatreCode

	Select @FetchStatus = @@FETCH_STATUS

END

/**************************************************************************/
/* Termination                                                            */
/**************************************************************************/
CLOSE TableCursor
DEALLOCATE TableCursor

delete from Theatre.TimetableDetail
where
	SessionStartTime is null


select @RowsInserted = 
	(
	select
		count(*)
	from
		Theatre.TimetableDetail
	)


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatreTimetableDetail', @Stats, @StartTime  -- Corrected from 'LoadTheatreOperationDetail' 18/11/2014 Paul Egan

print @Stats


﻿
CREATE procedure [dbo].[AssignAPCWLDirectorateCode] as

update
	APC.WaitingList
set
	DirectorateCode = Activity.DirectorateCode
from
	(
	select
		 Encounter.SourceUniqueID
		,DivisionRuleBase.DirectorateCode
	from
		APC.WaitingList Encounter

	inner join PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

	inner join PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

	inner join WH.DivisionRuleBase
	on	DivisionRuleBase.SiteCode = Encounter.SiteCode
	and	DivisionRuleBase.SpecialtyCode = Encounter.SpecialtyCode

	and	
		(
		
		 dbo.f_get_patient_category(
								 AdmissionMethod.InternalCode
								,ManagementIntention.InternalCode
								,Encounter.AddedToWaitingListTime
								,NULL
								)
			= DivisionRuleBase.PatientCategoryCode
		or	DivisionRuleBase.PatientCategoryCode is null
		)

	and	(
			Encounter.ConsultantCode = DivisionRuleBase.ConsultantCode
		or	DivisionRuleBase.ConsultantCode is null
		)

	and	(
			Encounter.WardCode = DivisionRuleBase.WardCode
		or	DivisionRuleBase.WardCode is null
		)

	and	not exists
		(
		select
			1
		from
			WH.DivisionRuleBase Previous
		where
			Previous.SiteCode = Encounter.SiteCode
		and	Previous.SpecialtyCode = Encounter.SpecialtyCode

		and	
			(
				 dbo.f_get_patient_category(
								 AdmissionMethod.InternalCode
								,ManagementIntention.InternalCode
								,Encounter.AddedToWaitingListTime
								,NULL
								)
			= Previous.PatientCategoryCode
			or	Previous.PatientCategoryCode is null
			)

		and	(
				Encounter.ConsultantCode = Previous.ConsultantCode
			or	Previous.ConsultantCode is null
			)

		and	(
				Encounter.WardCode = Previous.WardCode
			or	Previous.WardCode is null
			)

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when Previous.WardCode is not null
				then 2
				else 0
				end +

				case
				when Previous.PatientCategoryCode is not null
				then 1
				else 0
				end

				>

				case
				when DivisionRuleBase.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when DivisionRuleBase.WardCode is not null
				then 2
				else 0
				end +

				case
				when DivisionRuleBase.PatientCategoryCode is not null
				then 1
				else 0
				end

			or
				(
					case
					when Previous.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when Previous.WardCode is not null
					then 2
					else 0
					end +

					case
					when Previous.PatientCategoryCode is not null
					then 1
					else 0
					end

					=

					case
					when DivisionRuleBase.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when DivisionRuleBase.WardCode is not null
					then 2
					else 0
					end +

					case
					when DivisionRuleBase.PatientCategoryCode is not null
					then 1
					else 0
					end

					and	Previous.DivisionRuleBaseRecno > DivisionRuleBase.DivisionRuleBaseRecno
				)
			)
		)
	) Activity

where
	Activity.SourceUniqueID = APC.WaitingList.SourceUniqueID




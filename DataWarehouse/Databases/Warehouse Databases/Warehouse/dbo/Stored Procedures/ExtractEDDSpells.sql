﻿

CREATE procedure [dbo].[ExtractEDDSpells]
	-- @from smalldatetime
	--,@to smalldatetime
	--,@debug bit = 0
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

--EDD spells
delete from dbo.TImportEDDSpells

insert into dbo.TImportEDDSpells
(
	 SourceSpellNo 
	,SourcePatientID
	,SourceEncounterNo
	,SourceAEEncounterNo
	,CaseNoteNo
	,AENumber
	,AttendanceDt
	,AdmitDate
	,AdmitDatetime
	,DischargeDate
	,DischargeDatetime
	,CurrentWard
	,FacilityID
	,AEDepartureTime
	,CurrentSpecialty
	,CurrentConsulant
	,CurrentLocation
	,FirstEdd
	,FirstEddtime
	,FirstEddCreated
	,FirstReasonforChange
	,FirstReasonGroupforChange
	,LastEdd
	,LastEddCreated
	,LastReasonforChange
	,LastReasonGroupforChange
	,SourceAPCPatientNo
	,SourceAPCSpellNo
	,SourceAPCManagementIntentionCode
	,SourceAPCExpLOS
	,SourceAPCEdd
	,SourceAPCSiteCode
	,sourcepatientcategorycode
	,SourceRTTPathwayCondition
	,SourceAdmissionMethodCode

)

SELECT DISTINCT 
	 SourceSpellNo = tbl_hospspell.SpellID
	,SourcePatientID = tbl_hospspell.Patient
	,SourceEncounterNo = REPLACE(LTRIM(REPLACE(tbl_hospspell.IPEpisode, '0', ' ')), ' ', '0') 
	,SourceAEEncounterNo = tbl_hospspell.AEEpisode
	,CaseNoteNo = tbl_hospspell.HospitalNo
	,tbl_hospspell.AENumber
	,tbl_hospspell.AttendanceDt
	,AdmitDate = dateadd(day, datediff(day, 0, tbl_hospspell.AdmitDate), 0)
	,AdmitDatetime = tbl_hospspell.AdmitDate
	,DischargeDate =  dateadd(day, datediff(day, 0, tbl_hospspell.DischTime), 0)
	,DischargeDatetime = tbl_hospspell.DischTime
	,CurrentWard = tbl_apc.StartWardTypeCode
	,tbl_hospspell.FacilityID
	,AEDepartureTime = tbl_hospspell.DepartTime
	,CurrentSpecialty = tbl_apc.SpecialtyCode
	,CurrentConsulant = tbl_apc.ConsultantCode
	,CurrentLocation = tbl_hospspell.CurrLoc
	,FirstEdd = dateadd(day, datediff(day, 0, tbl_firsteddhis.PDD), 0)
	,FirstEddtime = tbl_firsteddhis.PDD
	,FirstEddCreated = tbl_firsteddhis.PDDEntered
	,FirstReasonforChange = CASE 
								tbl_firsteddhis.PDDReason 
								WHEN '-' THEN tbl_firsteddhis.PDDGroup
								ELSE tbl_firsteddhis.PDDReason 
							END
	,FirstReasonGroupforChange = tbl_firsteddhis.PDDGroup
	,LastEdd = tbl_hospspell.PDD
	,LastEddCreated = tbl_hospspell.PDDEntered	
	,LastReasonforChange = CASE  tbl_LastReason.PDDReason 
								WHEN '-' THEN tbl_LastReason.PDDGroup
								ELSE tbl_LastReason.PDDReason 
							END
	,LastReasonGroupforChange = tbl_LastReason.PDDGroup
	,SourceAPCPatientNo = tbl_apc.SourcePatientNo
	,SourceAPCSpellNo = tbl_apc.SourceSpellNo
	,SourceAPCManagementIntentionCode = tbl_apc.ManagementIntentionCode
	,SourceAPCExpLOS = tbl_apc.ExpectedLOS
	,SourceAPCEdd = dateadd(day, tbl_apc.ExpectedLOS,tbl_hospspell.AdmitDate)
	,SourceAPCSiteCode = tbl_apc.StartSiteCode
	,Sourcepatientcategorycode = PatientCategoryCode
	,SourceRTTPathwayCondition = RTTPathwayCondition
	,SourceAdmissionMethodCode = AdmissionMethodCode

FROM
	[$(BedmanTest)].[dbo].[HospSpell] tbl_hospspell

INNER JOIN 
	(
	SELECT
		 max(SpellID) AS spellID
		,Patient
		,REPLACE(LTRIM(REPLACE(IPEpisode, '0', ' ')), ' ', '0') IPEpisode
	FROM 
		[$(BedmanTest)].[dbo].[HospSpell]
	GROUP BY 	 
		 Patient
		,REPLACE(LTRIM(REPLACE(IPEpisode, '0', ' ')), ' ', '0')
	) LatestSpellID
ON tbl_hospspell.SpellID = LatestSpellID.spellID

LEFT OUTER JOIN
	(
	SELECT
		 PDDHistory.SpellID
		,PDDHistory.PDD
		,PDDHistory.PDDEntered
		,tbl_FirstReason.PDDReason
		,tbl_FirstReason.PDDGroup 
	FROM 
		[$(BedmanTest)].dbo.PDDHistory PDDHistory

	LEFT OUTER JOIN [$(BedmanTest)].[dbo].[vwPDDReasons] tbl_FirstReason 
	ON	PDDHistory.PDDreason =  tbl_FirstReason.PDDReasonID

	WHERE 
		NOT EXISTS
			(
			SELECT	
				1
			FROM 
				[$(BedmanTest)].dbo.PDDHistory FirstPDDHistory
			WHERE 
				FirstPDDHistory.SpellID = PDDHistory.SpellID
			AND FirstPDDHistory.PDDHistoryID < PDDHistory.PDDHistoryID
			)
	) AS tbl_firsteddhis 
ON tbl_hospspell.SpellID = tbl_firsteddhis.SpellID

LEFT OUTER JOIN [$(BedmanTest)].[dbo].[vwPDDReasons] tbl_LastReason 
ON	tbl_hospspell.PDDReason =  tbl_LastReason.PDDReasonID

LEFT OUTER JOIN 
	(
	SELECT DISTINCT 
			SourcePatientNo
		,SourceSpellNo 
		,ManagementIntentionCode
		,ExpectedLOS
		,SpecialtyCode
		,ConsultantCode
		,PatientCategoryCode
		,StartWardTypeCode
		,StartSiteCode
		,RTTPathwayCondition
		,AdmissionMethodCode
	FROM 
		APC.Encounter 
	WHERE 
		LastEpisodeInSpellIndicator = 'Y'  
	AND EpisodeEndTime IS NOT NULL
	) tbl_apc 
ON	tbl_hospspell.Patient = tbl_apc.SourcePatientNo  COLLATE Latin1_General_CI_AS 
AND REPLACE(LTRIM(REPLACE(tbl_hospspell.IPEpisode, '0', ' ')), ' ', '0')  = tbl_apc.SourceSpellNo   COLLATE Latin1_General_CI_AS 

WHERE 
	tbl_hospspell.DischTime IS NOT NULL



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'ExtractEDDSpells', @Stats, @StartTime



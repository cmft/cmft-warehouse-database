﻿
create proc dbo.ExtractMedisecInpatientSpell

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table dbo.TImportMedisecInpatientSpell

insert into dbo.TImportMedisecInpatientSpell

select [inppn]
      ,[inpdoa]
      ,[inpfb]
      ,[inpstatus]
      ,[inptoa]
      ,[inplspec]
      ,[inplcons]
      ,[inplward]
      ,[inplhosp]
      ,[inplpatcat]
      ,[inpmoa]
      ,[inpsoa]
      ,[inprefcode]
      ,[inpreforg]
      ,[inpdischdate]
      ,[inpdischtime]
      ,[inpdischmeth]
      ,[inpdischdest]
      ,[inpadmhosp]
      ,[inpcn]
      ,[inpeps]
from [$(CMFT_MEDISEC)].dbo.InpSpell

select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


﻿CREATE procedure [dbo].[BuildCOMWaitPTLBase] 
	@CensusDate smalldatetime
as

-- delete any for this census date
delete from COM.WaitPTLBase
where
	exists
(
	select
		1
	from
		COM.Wait
	where
		Wait.EncounterRecno = WaitPTLBase.EncounterRecno
	and	Wait.CensusDate = @CensusDate
)

-- delete orphaned records
delete from COM.WaitPTLBase
where
	not exists
(
	select
		1
	from
		COM.Wait
	where
		Wait.EncounterRecno = COM.WaitPTLBase.EncounterRecno
)

insert into COM.WaitPTLBase
(
	EncounterRecno
)
	select
		WL.EncounterRecno
	from
		COM.Wait WL
	where
		WL.CensusDate = @CensusDate
and
	(

		(
			LastAppointmentFlag = 1

		or
		
			ScheduleStartTime is null
		)


		--( --First attendance not recorded and not cancelled
		--	WL.DerivedFirstAttendanceFlag = 1
		--and	(WL.ScheduleOutcomeCode is null or WL.ScheduleOutcomeCode = 'NSPD')
		--and	WL.ScheduleOutcomeCode NOT IN ('CHOS','COTH','CPAT')
		--)
		--or	
		--( --First attendance recorded and not cancelled and is the last appointment in the referral
		--	WL.DerivedFirstAttendanceFlag = 1
		--and WL.ScheduleOutcomeCode is not null
		--and	WL.ScheduleOutcomeCode NOT IN ('CHOS','COTH','CPAT','NSPD')
		--and	WL.LastAppointmentFlag = 1
		--)
		--or -- where not recorded or not specified
		--(
		--	WL.ScheduleStartTime is null
		--)
		--or
		--( --last appointment
		--	convert(Date, WL.StartWaitDate) = convert(Date,WL.ScheduleStartTime)
		--and	WL.ScheduleOutcomeCode is not null
		--and WL.ScheduleOutcomeCode <> 'CPAT'
		--and	convert(Date,WL.ScheduleStartTime) <= WL.CensusDate
		--and	WL.LastAppointmentFlag = 1
		--)
		--or
		--(
		--	convert(Date, WL.StartWaitDate) = convert(Date,WL.ScheduleStartTime)
		--and	(WL.ScheduleOutcomeCode is null or WL.ScheduleOutcomeCode = 'NSPD')
		--and	convert(Date,WL.ScheduleStartTime) <= WL.CensusDate
		--and	WL.LastAppointmentFlag = 1
		--)
	)


--select
--	WL1.EncounterRecno
--into
--	#List
--from
--	COM.Wait WL1

--inner join COM.WaitPTLBase
--on	WaitPTLBase.EncounterRecno = WL1.EncounterRecno

--where
--	WL1.EncounterRecno in 
--	(
--	select
--		WL2.EncounterRecno
--	from
--		COM.Wait WL2
	
--	inner join COM.WaitPTLBase
--	on	WaitPTLBase.EncounterRecno = WL2.EncounterRecno
	
--	where
--		WL2.BookedTime < 
--			(
--			select
--				max(BookedTime)
--			from
--				COM.Wait WL3
	
--			inner join COM.WaitPTLBase
--			on	WaitPTLBase.EncounterRecno = WL3.EncounterRecno
		
--			where
--				WL3.SourceUniqueID = WL2.SourceUniqueID

--			and	WL3.CensusDate = WL2.CensusDate
--			)
	
--	and	WL2.SourceUniqueID = WL1.SourceUniqueID
--	and	WL2.CensusDate = WL1.CensusDate
--	)

--and	WL1.CensusDate = @CensusDate


--delete from COM.WaitPTLBase

--from
--	COM.WaitPTLBase

--inner join #List
--on	#List.EncounterRecno = WaitPTLBase.EncounterRecno

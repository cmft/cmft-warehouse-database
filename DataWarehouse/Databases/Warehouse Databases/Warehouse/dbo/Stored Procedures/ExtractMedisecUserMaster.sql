﻿
create proc dbo.ExtractMedisecUserMaster

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table dbo.TImportMedisecUserMaster

insert into dbo.TImportMedisecUserMaster

select 
	usercode
	,username
	,usertype
	,userdept

from [$(CMFT_MEDISEC)].dbo.UserMaster

select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


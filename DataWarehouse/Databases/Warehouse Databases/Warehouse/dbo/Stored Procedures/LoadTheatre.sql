﻿CREATE procedure [dbo].[LoadTheatre] 
as

/*-----------------------------------------------------------------------------
20150109	RR	Commented out the exec ETL.BuildAPCEncounterProcedureDetail
				Moved to WOMV2, once Cen and Trafford data have been merged
-----------------------------------------------------------------------------*/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

exec dbo.LoadTheatreSession
exec dbo.LoadTheatrePatientBooking
exec dbo.LoadTheatreOperationDetail
exec dbo.LoadTheatreProcedureDetail
exec dbo.LoadTheatreTimetableDetail
exec dbo.LoadTheatreTimetableTemplate
exec dbo.LoadTheatreReferenceData

--link encounters to ORMIS procedure detail
--20150109 RR think this is in here from a time before merged.  Commented out and will run script in WOMV2
-- exec ETL.BuildAPCEncounterProcedureDetail 



----update theatre details on APC.WaitingList
--update
--	APC.WaitingList
--set
--	 WaitingList.TheatrePatientBookingKey = TheatreSlot.TheatrePatientBookingKey
--	,WaitingList.ProcedureTime = TheatreSlot.ProcedureTime
--	,WaitingList.TheatreCode = TheatreSlot.TheatreCode
--from
--	APC.WaitingList WL
--
--inner join
--	(
--	select distinct
--		 WL.EncounterRecno
--		,TheatrePatientBookingKey = PatientBooking.SourceUniqueID
--		,ProcedureTime = coalesce(PatientBooking.EstimatedStartTime, PatientBooking.IntendedProcedureDate)
--		,TheatreCode = PatientBooking.TheatreCode
--	from
--		Theatre.PatientBooking PatientBooking
--
--	inner join APC.WaitingList WL
--	on	WL.DistrictNo = PatientBooking.DistrictNo
--	and	WL.CensusDate < PatientBooking.IntendedProcedureDate
--	and	WL.CensusDate = dateadd(day, datediff(day, 0, getdate()), 0)
----	and	PatientBooking.CancelReasonCode is null
--	and	not exists
--		(
--		select
--			1
--		from
--			APC.WaitingList WL1
--		where
--			WL1.CensusDate = WL.CensusDate
--		and	WL1.DistrictNo = WL.DistrictNo 
--		and	WL1.DistrictNo in 
--			(
--			select
--				DistrictNo
--			from
--				(
--				select
--					WLCount.DistrictNo,
--					count(*) Value
--				from
--					APC.WaitingList WLCount
--				where
--					WLCount.CensusDate = WL1.CensusDate
--				group by
--					WLCount.DistrictNo
--				having
--					count(*) > 1
--				) DistrictNumbers
--			)
--		)
--	) TheatreSlot
--on	TheatreSlot.EncounterRecno = WL.EncounterRecno

----reset all references
--update
--	APC.Encounter
--set
--	 Encounter.TheatrePatientBookingKey = null
--	,Encounter.ProcedureTime = null
--	,Encounter.TheatreCode = null

----update all references
--update
--	APC.Encounter
--set
--	 Encounter.TheatrePatientBookingKey = OperationDetail.SourceUniqueID
--	,Encounter.ProcedureTime = coalesce(OperationDetail.OperationStartDate, OperationDetail.OperationDate)
--	,Encounter.TheatreCode = OperationDetail.TheatreCode
--from
--	APC.Encounter Encounter
--
--inner join Theatre.OperationDetail OperationDetail
--on	OperationDetail.DistrictNo = Encounter.DistrictNo
--and	coalesce(OperationDetail.OperationStartDate, OperationDetail.OperationDate) between Encounter.EpisodeStartTime and	Encounter.EpisodeEndTime


----remove duplicate assignments where Encounter.PrimaryOperationCode is null
--update
--	APC.Encounter
--set
--	 Encounter.TheatrePatientBookingKey = null
--	,Encounter.ProcedureTime = null
--	,Encounter.TheatreCode = null
--from
--	APC.Encounter Encounter
--where
--	Encounter.PrimaryOperationCode is null
--and	Encounter.TheatrePatientBookingKey in
--	(
--	select
--		Duplicate.TheatrePatientBookingKey
--	from
--		APC.Encounter Duplicate
--	group by
--		Duplicate.TheatrePatientBookingKey
--	having
--		count(*) > 1
--	)
--
----remove other duplicate assignments
--update
--	APC.Encounter
--set
--	 Encounter.TheatrePatientBookingKey = null
--	,Encounter.ProcedureTime = null
--	,Encounter.TheatreCode = null
--from
--	APC.Encounter Encounter
--where
--	Encounter.TheatrePatientBookingKey in
--	(
--	select
--		Duplicate.TheatrePatientBookingKey
--	from
--		APC.Encounter Duplicate
--	group by
--		Duplicate.TheatrePatientBookingKey
--	having
--		count(*) > 1
--	)



update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADTHEATREDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADTHEATREDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadTheatre', @Stats, @StartTime

﻿
CREATE procedure [dbo].[ExtractCOMProfessionalCarer] as

declare @StartTime datetime
declare @to datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

--DECLARE @from smalldatetime = '04/01/2011'
--DECLARE @to smalldatetime = '04/02/2011'

truncate table dbo.TImportCOMProfessionalCarer

insert into dbo.TImportCOMProfessionalCarer
(
	 ProfessionalCarerID  --[PROCA_REFNO]
	,ProfessionalCarerCode --[PROCA_REFNO_MAIN_IDENT]
	,ProfessionalCarerTypeID --[PRTYP_REFNO]
	,Title --[TITLE_REFNO_DESCRIPTION]
	,Forename --[FORENAME]
	,Surname --[SURNAME]
	,StartTime --[START_DTTM]
	,EndTime --[END_DTTM]
	,PrimaryStaffTeamID --[PRIMARY_STEAM_REFNO]
	,ArchiveFlag --[ARCHV_FLAG]
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
) 

select
	 ProfessionalCarerID  = PROCA_REFNO
	,ProfessionalCarerCode = PROCA_REFNO_MAIN_IDENT
	,ProfessionalCarerTypeID = PRTYP_REFNO
	,Title = TITLE_REFNO_DESCRIPTION
	,Forename = FORENAME
	,Surname = SURNAME
	,StartTime = START_DTTM
	,EndTime = END_DTTM
	,PrimaryStaffTeamID = PRIMARY_STEAM_REFNO
	,ArchiveFlag = ARCHV_FLAG
	,HealthOrgOwner = OWNER_HEORG_REFNO
from
	[$(ipm)].dbo.ProfessionalCarer
where 
	OWNER_HEORG_REFNO IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
						  ,2031409 -- 5NT MANCHESTER PCT
						  ,2001932) -- NWWM Cluster

union
-- grab all exceptions from referrals table
select
	 ProfessionalCarerID  = PROCA_REFNO
	,ProfessionalCarerCode = PROCA_REFNO_MAIN_IDENT
	,ProfessionalCarerTypeID = PRTYP_REFNO
	,Title = TITLE_REFNO_DESCRIPTION
	,Forename = FORENAME
	,Surname = SURNAME
	,StartTime = START_DTTM
	,EndTime = END_DTTM
	,PrimaryStaffTeamID = PRIMARY_STEAM_REFNO
	,ArchiveFlag = ARCHV_FLAG
	,HealthOrgOwner = OWNER_HEORG_REFNO

from 
	[$(ipm)].dbo.ProfessionalCarer
where
	exists
		(
		select
			1
		from
			[$(ipm)].dbo.Schedule
		where
			ProfessionalCarer.PROCA_REFNO = Schedule.PROCA_REFNO
		) 

union

-- grab all exceptions from referrals table referred to
select
	 ProfessionalCarerID  = PROCA_REFNO
	,ProfessionalCarerCode = PROCA_REFNO_MAIN_IDENT
	,ProfessionalCarerTypeID = PRTYP_REFNO
	,Title = TITLE_REFNO_DESCRIPTION
	,Forename = FORENAME
	,Surname = SURNAME
	,StartTime = START_DTTM
	,EndTime = END_DTTM
	,PrimaryStaffTeamID = PRIMARY_STEAM_REFNO
	,ArchiveFlag = ARCHV_FLAG
	,HealthOrgOwner = OWNER_HEORG_REFNO

from 
	[$(ipm)].dbo.ProfessionalCarer
where
	exists
		(
		select
			1
		from
			[$(ipm)].dbo.Referral
		where
			ProfessionalCarer.PROCA_REFNO = Referral.REFTO_PROCA_REFNO
		) 

union

-- grab all exceptions from referrals table referred by
select
	 ProfessionalCarerID  = PROCA_REFNO
	,ProfessionalCarerCode = PROCA_REFNO_MAIN_IDENT
	,ProfessionalCarerTypeID = PRTYP_REFNO
	,Title = TITLE_REFNO_DESCRIPTION
	,Forename = FORENAME
	,Surname = SURNAME
	,StartTime = START_DTTM
	,EndTime = END_DTTM
	,PrimaryStaffTeamID = PRIMARY_STEAM_REFNO
	,ArchiveFlag = ARCHV_FLAG
	,HealthOrgOwner = OWNER_HEORG_REFNO

from 
	[$(ipm)].dbo.ProfessionalCarer
where
	exists
		(
		select
			1
		from
			[$(ipm)].dbo.Referral
		where
			ProfessionalCarer.PROCA_REFNO = Referral.REFBY_PROCA_REFNO
		) 


select @to = getdate()

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@StartTime, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractCommunityProfessionalCarer', @Stats, @StartTime



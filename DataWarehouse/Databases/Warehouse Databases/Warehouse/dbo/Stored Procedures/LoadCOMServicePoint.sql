﻿
CREATE procedure [dbo].[LoadCOMServicePoint] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the TImport table

TRUNCATE TABLE COM.ServicePoint

-- Insert new rows into encounter destination table
INSERT INTO COM.ServicePoint
(
		 
	 ServicePointID--[SPONT_REFNO]
	
	,ServicePointCode--[CODE]
	
	,ServicePoint--[NAME]
	
	,ServicePointDescription--[DESCRIPTION]
	
	,ServicePointSpecialty--[SPECT_REFNO]
	
	,ServicePointHealthOrgID -- [HEORG_REFNO]

	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]

	,ServicePointTypeID--[SPTYP_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,HealthOrgOwner--[OWNER_HEORG_REFNO]
	
) 
SELECT
	 	 
	 ServicePointID--[SPONT_REFNO]
	
	,ServicePointCode--[CODE]
	
	,ServicePoint--[NAME]
	
	,ServicePointDescription--[DESCRIPTION]
	
	,ServicePointSpecialty--[SPECT_REFNO]
	
	,ServicePointHealthOrgID -- [HEORG_REFNO]

	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]

	,ServicePointTypeID--[SPTYP_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,HealthOrgOwner--[OWNER_HEORG_REFNO]

FROM
	dbo.TLoadCOMServicePoint
	



/*
select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadCOMEncounter', @Stats, @StartTime
*/
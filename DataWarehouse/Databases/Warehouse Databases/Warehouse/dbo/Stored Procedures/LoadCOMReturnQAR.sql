﻿

CREATE PROCEDURE [dbo].[LoadCOMReturnQAR]
	-- Add the parameters for the stored procedure here
	
AS

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

    -- Insert statements for procedure here
--DECLARE @FromDate Date = convert(Date,(SELECT DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-1,0)) ),103)
--DECLARE @ToDate DATE =  convert(Date,(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0))),103)
--DECLARE @ReturnDate varchar(9) = CASE 
--									WHEN MONTH(@FromDate) IN (4,5,6) THEN CAST(YEAR(@FromDate)AS char(4)) + ' - Q1'
--									WHEN MONTH(@FromDate) IN (7,8,9) THEN CAST(YEAR(@FromDate)AS char(4)) + ' - Q2'
--									WHEN MONTH(@FromDate) IN (10,11,12) THEN CAST(YEAR(@FromDate)AS char(4)) + ' - Q3'
--									WHEN MONTH(@FromDate) IN (1,2,3) THEN CAST(YEAR(@FromDate)AS char(4)) + ' - Q4'
--								 END



declare @TheDate date = getdate()
declare @FromDate Date = 
							
							case
								when month(@TheDate) in (4, 5, 6)
								then '1 Jan' + cast(year(@TheDate) as char)
								when month(@TheDate) in (7, 8, 9)
								then '1 Apr' + cast(year(@TheDate) as char)
								when month(@TheDate) in (10, 11, 12)
								then '1 Jul' + cast(year(@TheDate) as char)
								when month(@TheDate) in (1, 2, 3)
								then '1 Oct' + cast(year(dateadd(year, -1, @TheDate)) as char)
							end
	
declare @ToDate Date = 
							
							case
								when month(@TheDate) in (4, 5, 6)
								then '31 mar' + cast(year(@TheDate) as char)
								when month(@TheDate) in (7, 8, 9)
								then '30 jun' + cast(year(@TheDate) as char)
								when month(@TheDate) in (10, 11, 12)
								then '30 sep' + cast(year(@TheDate) as char)
								when month(@TheDate) in (1, 2, 3)
								then '31 dec' + cast(year(dateadd(year, -1, @TheDate)) as char)
							end
								
DECLARE @ReturnDate varchar(9) = CASE 
									WHEN MONTH(@FromDate) IN (4,5,6) THEN CAST(YEAR(@FromDate)AS char(4)) + ' - Q1'
									WHEN MONTH(@FromDate) IN (7,8,9) THEN CAST(YEAR(@FromDate)AS char(4)) + ' - Q2'
									WHEN MONTH(@FromDate) IN (10,11,12) THEN CAST(YEAR(@FromDate)AS char(4)) + ' - Q3'
									WHEN MONTH(@FromDate) IN (1,2,3) THEN CAST(YEAR(dateadd(year, -1, @FromDate))AS char(4)) + ' - Q4' 
								 END



IF MONTH(@TheDate)IN(1,4,7,10)

BEGIN
	DELETE
	FROM COM.ReturnQAR
	WHERE ReturnPeriod = @ReturnDate
		AND LoadDate = Convert(Date,CURRENT_TIMESTAMP,103)
		AND [Return] = 'QAR'
		AND ReturnItem IN('GP Written Referrals Made'
						 ,'Other Referrals Made'
						 ,'Other Referrals Seen'
						 ,'GP Referrals Seen'
						 ,'FirstAttendance'
						 ,'SubsequentAttendance'
						 ,'FirstDNA'
						 ,'SubsequentDNA'
						 )


	-- Insert MARGPReferralsMade
	INSERT INTO COM.ReturnQAR
	(
		ReturnPeriod,	
		
		LoadDate,
		
		[Return],
		
		ReturnItem,
		
		InterfaceCode,
		
		SourceUniqueNo,
												
		ItemDate,
		
		ItemSpecialty,
			
		NHSNumber,
		
		ProfessionalCarer,
		
		CommissionerCode,
				 
		Commissioner
	)
	SELECT
		ReturnPeriod = @ReturnDate,	
		
		LoadDate,
		
		[Return]='QAR',
		
		ReturnItem,
		
		InterfaceCode,
		
		SourceUniqueNo,
												
		ItemDate,
		
		ItemSpecialty,
			
		NHSNumber,
		
		ProfessionalCarer,
		
		CommissionerCode,
				 
		Commissioner

	FROM
		dbo.TLoadCOMMARGPReferralsMade QARGPReferralsMade

	WHERE ItemDate >= @FromDate
		AND ItemDate <= @ToDate
		AND ReturnItem IN('GP Written Referrals Made'
						 ,'Other Referrals Made'
						 ,'Other Referrals Seen'
						 ,'GP Referrals Seen'
						 );


	-- Insert MARGPReferralsSeen

	--DECLARE @FromDate  datetime = '20110401'
	--DECLARE @ToDate datetime = '20110701'


	-- Find all frist attendance schedules
	DECLARE @SchedulesAttended TABLE 
			(
				 SCHDL_REFNO	INT				NULL
				,REFRL_REFNO	INT				NULL
				,PATNT_REFNO	INT				NULL
				,START_DTTM		DATETIME		NULL
			)

	INSERT INTO @SchedulesAttended

	SELECT	 MIN(SCHDL_REFNO) SCHDL_REFNO
			,REFRL_REFNO
			,PATNT_REFNO
			,START_DTTM
			
	FROM
		(
		SELECT   SCHDL_REFNO
				,REFRL_REFNO
				,PATNT_REFNO
				,START_DTTM 
		FROM 
				dbo.TLoadCOMQARSchedules 
		WHERE 
				ItemType = 'ATTD'

		) Encounter
		
	WHERE  	
		NOT EXISTS
				(
				SELECT
					1
				
				FROM
					dbo.TLoadCOMQARSchedules PreviousSchedule
				
				WHERE
					PreviousSchedule.REFRL_REFNO = Encounter.REFRL_REFNO
				AND PreviousSchedule.START_DTTM < Encounter.START_DTTM
				AND PreviousSchedule.ItemType = 'ATTD'
				)
	GROUP BY REFRL_REFNO
			,PATNT_REFNO
			,START_DTTM

	INSERT INTO COM.ReturnQAR
	(
		ReturnPeriod,		
		
		LoadDate,
		
		[Return],
		
		ReturnItem,
		
		InterfaceCode,
		
		SourceUniqueNo,
												
		ItemDate,
		
		ItemSpecialty,
			
		NHSNumber,
		
		ProfessionalCarer,
		
		CommissionerCode,
				 
		Commissioner
	)
	SELECT
		ReturnPeriod = @ReturnDate
		
		,LoadDate = CONVERT(Date,(CURRENT_TIMESTAMP),103)
		
		,[Return] = 'QAR'
		
		,ReturnItem = ReturnItem
		
		,InterfaceCode = 'ipm'
		
		,SourceUniqueNo = Scheduletypes.SCHDL_REFNO
		
		,ItemDate = CONVERT(date, Scheduletypes.START_DTTM,103)	
		
		,ItemSpecialty = specialty.[DESCRIPTION]
		
		,NHSNumber = patient.NHS_IDENTIFIER
		
		,ProfessionalCarer = ISNULL(profcarer.SURNAME, 'null') + ',' + ISNULL(profcarer.FORENAME, 'null')
							
		,CommissionerCode = COALESCE(
									ProfessionalCarerBase.ParentHealthOrgCode	
									,'5NT'
									)
		,Commissioner = COALESCE(
								ProfessionalCarerBase.ParentHealthOrgDescription
								,'MANCHESTER PCT'
								) 

	FROM
			--Schedules First Attended
			(
			SELECT
					 START_DTTM
					,SCHDL_REFNO
					,PATNT_REFNO
					,REFRL_REFNO
					,ReturnItem = 'First Attendance'
			FROM 
					@SchedulesAttended SchedulesAttended
			WHERE
					SchedulesAttended.START_DTTM >=@FromDate
				AND SchedulesAttended.START_DTTM <=@ToDate
				
			UNION ALL
			
			SELECT	
					 encounter.START_DTTM
					,encounter.SCHDL_REFNO
					,encounter.PATNT_REFNO
					,encounter.REFRL_REFNO
					,ReturnItem = 'Subsequent Attendance'
			
			FROM
				dbo.TLoadCOMQARSchedules encounter
				LEFT OUTER JOIN @SchedulesAttended SchedulesAttended
					ON encounter.REFRL_REFNO = SchedulesAttended.REFRL_REFNO
					AND encounter.START_DTTM >= SchedulesAttended.START_DTTM
			WHERE  	
					encounter.ItemType = 'ATTD'
				AND encounter.START_DTTM >=@FromDate
				AND encounter.START_DTTM <=@ToDate
				AND encounter.SCHDL_REFNO <> SchedulesAttended.SCHDL_REFNO
			UNION ALL
			
			SELECT	
					 encounter.START_DTTM
					,encounter.SCHDL_REFNO
					,encounter.PATNT_REFNO
					,encounter.REFRL_REFNO
					,ReturnItem = 'First DNA'
			
			FROM
				dbo.TLoadCOMQARSchedules encounter
				LEFT OUTER JOIN @SchedulesAttended SchedulesAttended
					ON encounter.REFRL_REFNO = SchedulesAttended.REFRL_REFNO

			WHERE  	
					encounter.ItemType = 'DNAT'
				AND (
						encounter.START_DTTM < SchedulesAttended.START_DTTM
					OR
						SchedulesAttended.START_DTTM IS NULL
					)
				AND encounter.START_DTTM >=@FromDate
				AND encounter.START_DTTM <=@ToDate
				
			UNION ALL
			
			SELECT	
					 encounter.START_DTTM
					,encounter.SCHDL_REFNO
					,encounter.PATNT_REFNO
					,encounter.REFRL_REFNO
					,ReturnItem = 'Subsequent DNA'
			
			FROM
				dbo.TLoadCOMQARSchedules encounter
				INNER JOIN @SchedulesAttended SchedulesAttended
					ON encounter.REFRL_REFNO = SchedulesAttended.REFRL_REFNO
					AND encounter.START_DTTM > SchedulesAttended.START_DTTM
			WHERE  	
				
				encounter.ItemType = 'DNAT'
			AND encounter.START_DTTM >=@FromDate
			AND encounter.START_DTTM <=@ToDate
			
			
			) Scheduletypes

	INNER JOIN (SELECT
						PATNT_REFNO
						,FORENAME
						,SURNAME
						,NHS_IDENTIFIER
						
				  FROM 
						[$(ipm)].dbo.PATIENTS_VE01 patient

				  WHERE  
						patient.ARCHV_FLAG = 'N'  
				   AND  NOT EXISTS
						(
						SELECT
							1
						FROM
							[$(ipm)].dbo.PATIENTS_VE01 Laterpatient
						WHERE
								Laterpatient.PATNT_REFNO = patient.PATNT_REFNO
							AND	Laterpatient.MODIF_DTTM > patient.MODIF_DTTM
							AND patient.ARCHV_FLAG = 'N'  
						  )
				  )  patient
						
	ON Scheduletypes.PATNT_REFNO = patient.PATNT_REFNO

	INNER JOIN
	(
		SELECT 
				SCHDL_REFNO
				,SPECT_REFNO
				,PATNT_REFNO
				,PROCA_REFNO
				,REFRL_REFNO
		FROM 
			(SELECT * 
			
			FROM [$(ipm)].dbo.SCHEDULES_VE02 
			
			WHERE 	 NOT EXISTS
				(
				SELECT
					1
				
				FROM
					[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
				
				WHERE
					PreviousEncounter.SCHDL_REFNO = SCHEDULES_VE02.SCHDL_REFNO
				
				AND PreviousEncounter.MODIF_DTTM > SCHEDULES_VE02.MODIF_DTTM
				)
			) LatestSCHEDULES
		WHERE 
				ARCHV_FLAG = 'N'

				AND	CONTY_REFNO <> '2004177' -- Other Client Linked Activity	
				AND												
					((SCTYP_REFNO = '1470' -- Outpatient Schedules	
						AND
					SPECT_REFNO IN ('2000820','10000054')) --Comm Paeds and Audiology

					OR

					(SCTYP_REFNO = '1468' -- Community Contacts
						AND
					SPECT_REFNO = '10000054'))-- community Paeds
				AND START_DTTM >=@FromDate
				AND START_DTTM < @ToDate
		)encounter
	ON Scheduletypes.SCHDL_REFNO = encounter.SCHDL_REFNO

	LEFT JOIN (SELECT
					SPECT_REFNO
					,[DESCRIPTION]
			  FROM 
					[$(ipm)].dbo.SPECIALTIES_V01 Specialty

			  WHERE   NOT EXISTS
					(
					SELECT
						1
					FROM
						[$(ipm)].dbo.SPECIALTIES_V01 LaterSpecialty
					WHERE
							LaterSpecialty.SPECT_REFNO = Specialty.SPECT_REFNO
						AND	LaterSpecialty.MODIF_DTTM > Specialty.MODIF_DTTM
					  )
			  ) specialty											
		ON encounter.SPECT_REFNO = specialty.SPECT_REFNO	


	LEFT JOIN (SELECT
					 PROCA_REFNO
					,PROCA_REFNO_MAIN_IDENT
					,FORENAME
					,SURNAME
			  FROM 
					[$(ipm)].dbo.PROF_CARERS_V01 ProfCarer

			  WHERE   NOT EXISTS
					(
					SELECT
						1
					FROM
						[$(ipm)].dbo.PROF_CARERS_V01 LaterProfCarer
					WHERE
							LaterProfCarer.PROCA_REFNO = ProfCarer.PROCA_REFNO
						AND	LaterProfCarer.MODIF_DTTM > ProfCarer.MODIF_DTTM
					  )
			  )profcarer
	 ON
				encounter.PROCA_REFNO = profcarer.PROCA_REFNO	



	LEFT OUTER JOIN
	(
		SELECT DISTINCT
				 EncounterPatientProfessionalCarer.SCHDL_REFNO
				,EncounterPatientProfessionalCarer.PATPC_REFNO
				,ProfessionalCarerCode
				,HealthOrgCode
				,ParentHealthOrgCode
				,ParentHealthOrgDescription


		FROM
				(
				SELECT 
					SCHDL_REFNO
					,MAX(PATPC_REFNO) AS PATPC_REFNO

				FROM 
					(
					SELECT 
							 SCHDL_REFNO
							,START_DTTM
							,PATNT_REFNO
					 FROM 
							[$(ipm)].dbo.SCHEDULES_VE02 Encounter
					 WHERE  NOT EXISTS
								(
								SELECT
									1
								FROM
									[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
								WHERE
									PreviousEncounter.SCHDL_REFNO = Encounter.SCHDL_REFNO
								AND PreviousEncounter.MODIF_DTTM > Encounter.MODIF_DTTM
								)
					)Encounter

				LEFT OUTER JOIN

					(
					SELECT
							 PATIENT_PROF_CARERS_V01.PATPC_REFNO
							,PATIENT_PROF_CARERS_V01.START_DTTM
							,PATIENT_PROF_CARERS_V01.END_DTTM
							,PATIENT_PROF_CARERS_V01.PATNT_REFNO
							,PATIENT_PROF_CARERS_V01.HEORG_REFNO

					FROM 
							[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 
							
					WHERE   ARCHV_FLAG = 'N'
						AND PRTYP_REFNO = '1132'-- General Practitioner
						AND NOT EXISTS (
										SELECT
											*
										FROM
											[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
										WHERE
											PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
										AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
										AND ARCHV_FLAG = 'N'
										AND PRTYP_REFNO = '1132' -- General Practitioner
										)
					)PatientProfCarer

				ON Encounter.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
				AND PatientProfCarer.START_DTTM <= Encounter.START_DTTM 
				AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > Encounter.START_DTTM)

				GROUP BY 
					SCHDL_REFNO

		)EncounterPatientProfessionalCarer
			

		LEFT OUTER JOIN (SELECT
							 PATIENT_PROF_CARERS_V01.PATPC_REFNO
							,PATIENT_PROF_CARERS_V01.START_DTTM
							,PATIENT_PROF_CARERS_V01.END_DTTM
							,PATIENT_PROF_CARERS_V01.PATNT_REFNO
							,PATIENT_PROF_CARERS_V01.HEORG_REFNO
							,PATIENT_PROF_CARERS_V01.PROCA_MAIN_IDENT AS ProfessionalCarerCode

					FROM 
							[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 
							
					WHERE   ARCHV_FLAG = 'N'
						AND PRTYP_REFNO = '1132'-- General Practitioner
						AND NOT EXISTS (
										SELECT
											*
										FROM
											[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
										WHERE
											PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
										AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
										AND ARCHV_FLAG = 'N'
										AND PRTYP_REFNO = '1132' -- General Practitioner
										)
					)PatientProfCarerDetail
		ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO


		LEFT OUTER JOIN (	
							SELECT DISTINCT
								 HEORG_REFNO
								 ,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS HealthOrgDescription
								 ,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS HealthOrgCode
								 ,HEALTH_ORGANISATIONS_V02.PARNT_REFNO
							 FROM [$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
							 WHERE ARCHV_FLAG = 'N'
							 
							 AND NOT EXISTS
										(
										SELECT
											1
										FROM
											[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
										WHERE
											previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
										AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
										AND ARCHV_FLAG = 'N'
										)
							) PatientProfCarerHealthOrg
								 
		ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
		
			LEFT OUTER JOIN 
			(	
			SELECT DISTINCT
				  HEORG_REFNO ParentHEORG_REFNO
				 ,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS ParentHealthOrgDescription
				 ,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS ParentHealthOrgCode
				 ,HEALTH_ORGANISATIONS_V02.HOTYP_REFNO AS ParentHealthOrgCodeType
			 FROM [$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
			 WHERE ARCHV_FLAG = 'N'
			   AND HOTYP_REFNO = 629
			 
			 AND NOT EXISTS
						(
						SELECT
							1
						FROM
							[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
						WHERE
							previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
						AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
						AND ARCHV_FLAG = 'N'
						AND HOTYP_REFNO = 629
						)
			) ParentPatientProfCarerHealthOrg

		ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
		
		
		
	)ProfessionalCarerBase
	ON encounter.SCHDL_REFNO = ProfessionalCarerBase.SCHDL_REFNO

	LEFT OUTER JOIN
	(
	SELECT	
		REFRL_REFNO
		,SORRF_REFNO
		
	FROM
		[$(ipm)].dbo.REFERRALS_VE02 referrals
		
	WHERE  
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.REFERRALS_VE02 Previousreferral
			WHERE
				Previousreferral.REFRL_REFNO = referrals.REFRL_REFNO
			AND Previousreferral.MODIF_DTTM > referrals.MODIF_DTTM
			)
	) Referral

		ON encounter.REFRL_REFNO = Referral.REFRL_REFNO

	WHERE 
			Scheduletypes.START_DTTM >= @FromDate
		
		AND Scheduletypes.START_DTTM < @ToDate
		
		AND profcarer.PROCA_REFNO_MAIN_IDENT <> 'pseu057'	--exclude t2 ENT referrals
		
		AND NOT (
						LEFT(patient.NHS_IDENTIFIER, 3) = '999'
					OR
						patient.FORENAME  IN ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')
					OR
						patient.SURNAME  IN ('xxtestpatientaavp','xxtestpatientaafe')
				)
	
END

﻿
create proc dbo.ExtractMedisecDocumentSummary

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table dbo.TImportMedisecDocumentSummary

insert into dbo.TImportMedisecDocumentSummary

select [dsdocname]
      ,[dspn]
      ,[dspatname]
      ,[dstype]
      ,[dsauthor]
      ,[dsdescription]
      ,[dstyped]
      ,[dscons]
      ,[dsdictated]
      ,[dsref]
      ,[dscn]
      ,[dshead]
      ,[dsfarm]
      ,[dsissued]
      ,[dsissuedby]
      ,[dssigned]
      ,[dssignedby]
      ,[dsward]
      ,[dsdictatedby]
      ,[dsgroup]
      ,[dssignstatus]
      ,[dsgp]
      ,[dspr]
      ,[dsinbox]
      ,[dsdept]
      ,[dsid]
      ,[dsprstatus]
      ,[dsprviewedby]
      ,[dsprvieweddate]
      ,[dsspec]
      ,[dseps]
      ,[dszipid]
from [$(CMFT_MEDISEC)].dbo.DocSumm

select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


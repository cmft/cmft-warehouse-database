﻿

CREATE PROCEDURE [dbo].[ExtractCOMReferral]
	-- @from smallTime
	--,@to smallTime
AS


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

DECLARE @from smalldatetime = NULL
DECLARE @to smalldatetime = NULL
--set @from = '04/01/2011'
--set @to = '04/02/2011'


SELECT
	@lastmodifiedTime =
		coalesce(MAX(CONVERT(datetime, ModifiedTime)) ,'01/01/1900'	)
FROM
	COM.Referral


TRUNCATE TABLE dbo.TImportCOMReferral

INSERT INTO dbo.TImportCOMReferral
(
	 SourceUniqueID --[REFRL_REFNO]
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]	
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]	
	,SourceofReferralID --[SORRF_REFNO]	
	,ReferralReceivedTime --[RECVD_DTTM]	
	,ReasonforReferralID --[REASN_REFNO]	
	,CancelledTime --[CANCD_DTTM]	
	,CancelledReasonID --[CANRS_REFNO]	
	,ReferralSentTime --[SENT_DTTM]	
	,Urgency --[URGNC_REFNO]	
	,Priority --[PRITY_REFNO]	
	,AuthoriedTime --[AUTHR_DTTM]	
	,ReferralClosedTime --[CLOSR_DATE]	
	,ClosureReasonID --CLORS_REFNO	
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]	
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]	
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]	
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]	
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]	
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]	
	,OutcomeOfReferral --[RFOCM_REFNO]	
	,ReferralStatus --[RSTAT_REFNO]	
	,RejectionID --RejectionID--[RJECT_REFNO]	
	,TypeofReferralID --[RETYP_REFNO]	
	,PatientSourceID 	
	,PatientSourceSystemUniqueID	
	,PatientNHSNumber	
	,PatientNHSNumberStatusIndicator
	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier	
	,PatientTitleID 	
	,PatientForename 	
	,PatientSurname 	
	,PatientAddress1	
	,PatientAddress2	
	,PatientAddress3	
	,PatientAddress4	
	,PatientPostcode	
	,PatientDateOfBirth	
	,PatientDateOfDeath	
	,PatientSexID	
	,PatientEthnicGroupID
	,ProfessionalCarerCurrentID		
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID	
	,ProfessionalCarerReferralID 		
	,ProfessionalCarerHealthOrgReferralID 
	,ProfessionalCarerParentHealthOrgReferralID 	
	,RTTStatusDate --[STATUS_DTTM]	
	,RTTPatientPathwayID --[PATNT_PATHWAY_ID]	
	,RTTStartTime --[RTT_START_DATE]	
	,RTTStartFlag --[RTT_STARTED]	
	,RTTStatusID --[RTTST_REFNO]	
	,CreatedTime --[CREATE_DTTM]	
	,ModifiedTime --[MODIF_DTTM]	
	,CreatedByID --[USER_CREATE]	
	,ModifiedByID --[USER_MODIF]	
	,ArchiveFlag --[ARCHV_FLAG]	
	,ParentID  --[PARNT_REFNO]	
	,HealthOrgOwner --[OWNER_HEORG_REFNO]	
	,RequestedServiceID --[ACTYP_REFNO]
) 

SELECT
	 SourceUniqueID = Referral.REFRL_REFNO	 
	,ReferredBySpecialtyID = Referral.REFBY_SPECT_REFNO	
	,ReferredToSpecialtyID = Referral.REFTO_SPECT_REFNO	
	,SourceofReferralID = Referral.SORRF_REFNO	
	,ReferralReceivedTime = Referral.RECVD_DTTM	
	,ReasonforReferralID = Referral.REASN_REFNO	
	,CancelledTime = Referral.CANCD_DTTM	
	,CancelledReasonID = Referral.CANRS_REFNO	
	,ReferralSentTime = Referral.SENT_DTTM	
	,Urgency = Referral.URGNC_REFNO	
	,Priority = Referral.PRITY_REFNO	
	,AuthoriedTime = Referral.AUTHR_DTTM	
	,ReferralClosedTime = Referral.CLOSR_DATE	
	,ClosureReasonID = Referral.CLORS_REFNO	
	,ReferredByProfessionalCarerID = Referral.REFBY_PROCA_REFNO	
	,ReferredByStaffTeamID = Referral.REFBY_STEAM_REFNO	
	,ReferredByHealthOrgID = Referral.REFBY_HEORG_REFNO	
	,ReferredToProfessionalCarerID = Referral.REFTO_PROCA_REFNO	
	,ReferredToHealthOrgID = Referral.REFTO_HEORG_REFNO	
	,ReferredToStaffTeamID = Referral.REFTO_STEAM_REFNO	
	,OutcomeOfReferral = Referral.RFOCM_REFNO	
	,ReferralStatus = Referral.RSTAT_REFNO	
	,RejectionID = Referral.RJECT_REFNO	
	,TypeofReferralID = Referral.RETYP_REFNO		
	,PatientSourceID = Referral.PATNT_REFNO	
	,PatientSourceSystemUniqueID = Patient.PASID	
	,PatientNHSNumber = Patient.NHS_IDENTIFIER	
	,PatientNHSNumberStatusIndicator = 1
	,PatientLocalIdentifier = PatientIDLocalIdentifier.IDENTIFIER
	,PatientPASIdentifier = PatientIDPASNumber.IDENTIFIER
	,PatientIdentifier = PatientIDPatientIdentifier.IDENTIFIER
	,PatientFacilityIdentifier = PatientIDPatientFacilityID.IDENTIFIER	
	,PatientTitleID = Patient.TITLE_REFNO	
	,PatientForename = Patient.FORENAME	
	,PatientSurname = Patient.SURNAME	
	,PatientAddress1 = ReferralPatientAddress.LINE1	
	,PatientAddress2 = ReferralPatientAddress.LINE2	
	,PatientAddress3 = ReferralPatientAddress.LINE3	
	,PatientAddress4 = ReferralPatientAddress.LINE4	
	,PatientPostcode = ReferralPatientAddress.PCODE	
	,PatientDateOfBirth = Patient.DATE_OF_BIRTH	
	,PatientDateOfDeath = Patient.DATE_OF_DEATH	
	,PatientSexID = Patient.SEXXX_REFNO	
	,PatientEthnicGroupID = Patient.ETHGR_REFNO	
	,ProfessionalCarerCurrentID = ProfessionalCarerCurrentBase.PatientProfCarerHealthOrgID		
	,ProfessionalCarerHealthOrgCurrentID = ProfessionalCarerCurrentBase.PatientProfCarerHealthOrgID
	,ProfessionalCarerParentHealthOrgCurrentID = ProfessionalCarerCurrentBase.PatientProfCarerParentHealthOrgID	
	,ProfessionalCarerReferralID = ProfessionalCarerReferralBase.PatientProfCarerHealthOrgID		
	,ProfessionalCarerHealthOrgReferralID = ProfessionalCarerReferralBase.PatientProfCarerHealthOrgID
	,ProfessionalCarerParentHealthOrgReferralID = ProfessionalCarerReferralBase.PatientProfCarerParentHealthOrgID	
	,RTTStatusDate = Referral.STATUS_DTTM	
	,RTTPatientPathwayID = Referral.PATNT_PATHWAY_ID	
	,RTTStartTime = Referral.RTT_START_DATE	
	,RTTStartFlag = Referral.RTT_STARTED	
	,RTTStatusID = Referral.RTTST_REFNO	
	,CreatedTime = Referral.CREATE_DTTM	
	,ModifiedTime = Referral.MODIF_DTTM	
	,CreatedByID = Referral.USER_CREATE	
	,ModifiedByID = Referral.USER_MODIF	
	,ArchiveFlag = Referral.ARCHV_FLAG	
	,ParentID  = Referral.PARNT_REFNO	
	,HealthOrgOwner = Referral.OWNER_HEORG_REFNO	
	,RequestedServiceID = ACTYP_REFNO
FROM 
	[$(ipm)].dbo.Referral Referral

INNER JOIN [$(ipm)].dbo.Patient Patient
ON	Referral.PATNT_REFNO = Patient.PATNT_REFNO
and Patient.ARCHV_FLAG = 'N'

left outer join
(
	select
		 ReferralPatientAddress.REFRL_REFNO
		,ReferralPatientAddress.ADDSS_REFNO
		,PatientAddressDetails.LINE1
		,PatientAddressDetails.LINE2
		,PatientAddressDetails.LINE3
		,PatientAddressDetails.LINE4
		,PatientAddressDetails.PCODE
	from
		(
		select 
			 REFRL_REFNO
			,ADDSS_REFNO = max(ADDSS_REFNO)
		from 
			[$(ipm)].dbo.Referral

		left outer join
			(
			SELECT
				 AddressRole.ADDSS_REFNO
				,AddressRole.START_DTTM
				,AddressRole.END_DTTM
				,AddressRole.PATNT_REFNO
			from 
				[$(ipm)].dbo.AddressRole  
			where   
				ROTYP_CODE = 'HOME'
			and ARCHV_FLAG = 'N'
			) PatientAddress
		on	Referral.PATNT_REFNO = PatientAddress.PATNT_REFNO
		and PatientAddress.START_DTTM <= Referral.RECVD_DTTM 
		and (
				PatientAddress.END_DTTM IS NULL 
			OR PatientAddress.END_DTTM > Referral.RECVD_DTTM
			)

		group by 
			REFRL_REFNO
	) ReferralPatientAddress
	
	left outer join 
		(	
		select distinct
			 ADDSS_REFNO
			,LINE1
			,LINE2
			,LINE3
			,LINE4
			,PCODE
		from 
			[$(ipm)].dbo.Address
		where 
			ADTYP_CODE = 'POSTL'
		and ARCHV_FLAG = 'N'
		) PatientAddressDetails
	on ReferralPatientAddress.ADDSS_REFNO = PatientAddressDetails.ADDSS_REFNO
) ReferralPatientAddress
on	Referral.REFRL_REFNO = ReferralPatientAddress.REFRL_REFNO

outer apply
	(
	select top 1
		 PatientProfessionalCarerID = PatientProfessionalCarer.PATPC_REFNO
		,PatientProfCarerHealthOrgID = PatientProfessionalCarer.HEORG_REFNO
		,PatientProfCarerParentHealthOrgID = PatientProfCarerHealthOrg.ParentHEORG_REFNO 
	from
		[$(ipm)].dbo.PatientProfessionalCarer

	left outer join 
		(	
		select
			 HealthOrganisationID = HealthOrganisation.HEORG_REFNO
			,ParentHEORG_REFNO = ParentHealthOrganisation.HEORG_REFNO 
		from 
			[$(ipm)].dbo.HealthOrganisation 	
		inner join	 
			(	
			select 
				 HEORG_REFNO 
			from 
				[$(ipm)].dbo.HealthOrganisation 
			where 
				ARCHV_FLAG = 'N'
			AND HOTYP_REFNO = 629
			) ParentHealthOrganisation
		on	ParentHealthOrganisation.HEORG_REFNO = HealthOrganisation.PARNT_REFNO

		where 
			ARCHV_FLAG = 'N'
		) PatientProfCarerHealthOrg
	 on PatientProfessionalCarer.HEORG_REFNO = PatientProfCarerHealthOrg.HealthOrganisationID

	where
		ARCHV_FLAG = 'N'
	and PRTYP_REFNO = '1132'-- General Practitioner
	and PatientProfessionalCarer.PATNT_REFNO = Referral.PATNT_REFNO
	and PatientProfessionalCarer.START_DTTM <= Referral.RECVD_DTTM 
	and (
			PatientProfessionalCarer.END_DTTM IS NULL 
		or	PatientProfessionalCarer.END_DTTM > Referral.RECVD_DTTM 
		)

	order by 
		 CREATE_DTTM desc
		,MODIF_DTTM desc
	) ProfessionalCarerReferralBase

outer apply
	(
	select top 1
		 PatientProfessionalCarerID = PatientProfessionalCarer.PATPC_REFNO
		,PatientProfCarerHealthOrgID = PatientProfessionalCarer.HEORG_REFNO
		,PatientProfCarerParentHealthOrgID = PatientProfCarerHealthOrg.ParentHEORG_REFNO 
	from
		[$(ipm)].dbo.PatientProfessionalCarer

	left outer join 
		(	
		select
			 HealthOrganisationID = HealthOrganisation.HEORG_REFNO
			,ParentHEORG_REFNO = ParentHealthOrganisation.HEORG_REFNO 
		from 
			[$(ipm)].dbo.HealthOrganisation 	

		inner join	 
			(	
			select 
				 HEORG_REFNO 
			from 
				[$(ipm)].dbo.HealthOrganisation 
			where 
				ARCHV_FLAG = 'N'
			AND HOTYP_REFNO = 629
			) ParentHealthOrganisation
		on	ParentHealthOrganisation.HEORG_REFNO = HealthOrganisation.PARNT_REFNO

		where 
			ARCHV_FLAG = 'N'
		) PatientProfCarerHealthOrg
	 on PatientProfessionalCarer.HEORG_REFNO = PatientProfCarerHealthOrg.HealthOrganisationID

	where
		ARCHV_FLAG = 'N'
	and PRTYP_REFNO = '1132'-- General Practitioner
	and PatientProfessionalCarer.PATNT_REFNO = Referral.PATNT_REFNO
	and PatientProfessionalCarer.END_DTTM IS NULL 

	order by 
		 CREATE_DTTM desc
		,MODIF_DTTM desc
	) ProfessionalCarerCurrentBase


outer apply
	(
	select top 1
		 PatientID.PATNT_REFNO
		,PatientID.IDENTIFIER
	from
		[$(ipm)].dbo.PatientID
	where
		PatientID.PITYP_REFNO =  1059		-- local identifier
	and PatientID.ARCHV_FLAG = 'n'
	and PatientID.CURNT_FLAG = 'y'
	and PatientID.END_DTTM is null
	and Referral.PATNT_REFNO = PatientID.PATNT_REFNO

	order by 
		 CREATE_DTTM desc
		,MODIF_DTTM desc
	) PatientIDLocalIdentifier 

outer apply
	(
	select top 1
		 PatientID.PATNT_REFNO
		,PatientID.IDENTIFIER
	from
		[$(ipm)].dbo.PatientID 
	where
		PatientID.PITYP_REFNO =  1063		-- pas number
	and PatientID.ARCHV_FLAG = 'n'
	and PatientID.CURNT_FLAG = 'y'
	and PatientID.END_DTTM is null
	and Referral.PATNT_REFNO = PatientID.PATNT_REFNO

	order by 
		 CREATE_DTTM desc
		,MODIF_DTTM desc
	) PatientIDPASNumber 

outer apply
	(
	select top 1
		 PatientID.PATNT_REFNO
		,PatientID.IDENTIFIER
	from
		[$(ipm)].dbo.PatientID
	where
		PatientID.PITYP_REFNO = 2001233		-- patient identifier
	and PatientID.ARCHV_FLAG = 'n'
	and PatientID.CURNT_FLAG = 'y'
	and PatientID.END_DTTM is null
	and Referral.PATNT_REFNO = PatientID.PATNT_REFNO

	order by 
		 CREATE_DTTM desc
		,MODIF_DTTM desc
	) PatientIDPatientIdentifier 

outer apply
	(
	select top 1
		 PatientID.PATNT_REFNO
		,PatientID.IDENTIFIER
	from
		[$(ipm)].dbo.PatientID
	where
		PatientID.PITYP_REFNO = 2001232		-- patient id (facility)
	and PatientID.ARCHV_FLAG = 'n'
	and PatientID.CURNT_FLAG = 'y'
	and PatientID.END_DTTM is null
	and Referral.PATNT_REFNO = PatientID.PATNT_REFNO

	order by 
		 CREATE_DTTM desc
		,MODIF_DTTM desc
	) PatientIDPatientFacilityID 

where
	Referral.MODIF_DTTM > @lastmodifiedTime;
	
select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractCommunityReferral', @Stats, @StartTime;

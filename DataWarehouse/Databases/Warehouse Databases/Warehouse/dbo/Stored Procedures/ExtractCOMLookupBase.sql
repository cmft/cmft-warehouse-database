﻿
CREATE procedure [dbo].[ExtractCOMLookupBase]
	-- @from smallTime
	--,@to smallTime
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

TRUNCATE TABLE dbo.TImportCOMLookupBase

INSERT INTO dbo.TImportCOMLookupBase
(
	 LookupID --[RFVAL_REFNO]
	,LookupTypeID --[RFVDM_CODE]
	,LookupDescription --[DESCRIPTION]
	,LookupCode --[MAIN_CODE]
	,LookupNHSCode
	,LookupCDSCode
	,LookupNatCode
	,ArchiveFlag --[ARCHV_FLAG]
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
) 
SELECT
	 LookupID = LookupBase.RFVAL_REFNO
	,LookupTypeID = LookupBase.RFVDM_CODE
	,LookupDescription = LookupBase.[DESCRIPTION]
	,LookupCode = LookupBase.MAIN_CODE
	,LookupNHSCode = LookupNHSBase.IDENTIFIER
	,LookupCDSCode = LookupCDSBase.IDENTIFIER
	,LookupNatCode = LookupNatBase.IDENTIFIER
	,ArchiveFlag = LookupBase.ARCHV_FLAG
	,HealthOrgOwner = LookupBase.OWNER_HEORG_REFNO
FROM 
	[$(ipm)].dbo.ReferenceValues LookupBase

LEFT OUTER JOIN [$(ipm)].dbo.ReferenceValueIDs LookupNHSBase
ON	LookupBase.RFVAL_REFNO = LookupNHSBase.RFVAL_REFNO
AND LookupNHSBase.[RITYP_CODE] = 'NHS'

LEFT OUTER JOIN [$(ipm)].dbo.ReferenceValueIDs LookupCDSBase
ON	LookupBase.RFVAL_REFNO = LookupCDSBase.RFVAL_REFNO
AND LookupCDSBase.[RITYP_CODE] = 'CDS'

LEFT OUTER JOIN [$(ipm)].dbo.ReferenceValueIDs LookupNatBase
ON	LookupBase.RFVAL_REFNO = LookupNatBase.RFVAL_REFNO
AND LookupNatBase.[RITYP_CODE] = 'NATNL'

WHERE 
	LookupBase.RFVDM_CODE <> 'COUNT'



select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' 

exec WriteAuditLogEvent 'ExtractCOMLookupBase', @Stats, @StartTime
﻿
CREATE proc [dbo].[ExtractBedmanDailyAcuityScore]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime = NULL
declare @to smalldatetime = NULL

select @StartTime = getdate()

truncate table dbo.TImportBedmanDailyAcuityScore

insert into dbo.TImportBedmanDailyAcuityScore

select 
	   [DailyScoreID]
      ,[WardCode]
      ,[ScoreTime]
      ,[TotalPatientsLevel0]
      ,[TotalPatientsLevel1a]
      ,[TotalPatientsLevel1b]
      ,[TotalPatientsLevel2]
      ,[TotalPatientsLevel3]
      ,[AvailableBeds]

FROM
(
SELECT 
	   [DailyScoreID] = [DailyScoreID]
      ,[WardCode] = [wardCode]
      ,[ScoreTime] = [ScoresDate]
      ,[TotalPatientsLevel0] = [Total_0]
      ,[TotalPatientsLevel1a] = [Total_1a]
      ,[TotalPatientsLevel1b] = [Total_1b]
      ,[TotalPatientsLevel2] = [Total_2]
      ,[TotalPatientsLevel3] = [Total_3]
      ,[AvailableBeds] = [OpenBeds]

FROM 
	[$(BedmanTest)].dbo.DailyAcuityScore

) AS tbl_DailyAcuityScore

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractBedmanDailyAcuityScore', @Stats, @StartTime
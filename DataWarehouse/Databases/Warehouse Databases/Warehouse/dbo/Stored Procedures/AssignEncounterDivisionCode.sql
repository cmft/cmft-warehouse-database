﻿CREATE procedure AssignEncounterDivisionCode as

update
	APC.Encounter
set
	AdmissionDivisionCode = Activity.DivisionCode
from
	(
	select
		 Encounter.ProviderSpellNo
		,DivisionRuleBase.DivisionCode
	from
		APC.Encounter Encounter

	inner join PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

	inner join PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

	inner join WH.DivisionRuleBase
	on	DivisionRuleBase.SiteCode = Encounter.SiteCode
	and	DivisionRuleBase.SpecialtyCode = Encounter.SpecialtyCode

	and	
		(
			case

			when AdmissionMethod.InternalCode in 
				(
				 '1'
				,'2'
				,'3'
				)
			and ManagementIntention.InternalCode in 
				(
				 '1'
				,'3'
				,'6'
				)
			then 'EL'

			when AdmissionMethod.InternalCode in 
				(
				 '1'
				,'2'
				,'3'
				)
			and ManagementIntention.InternalCode = '2'
			and	Encounter.AdmissionDate < Encounter.DischargeDate
			then 'EL'

			when AdmissionMethod.InternalCode in 
				(
				 '1'
				,'2'
				,'3'
				)
			and ManagementIntention.InternalCode = '2'
			and	Encounter.AdmissionDate = Encounter.DischargeDate
			then 'DC'
			else 'NE'
			end
			= DivisionRuleBase.PatientCategoryCode

		or	DivisionRuleBase.PatientCategoryCode is null
		)

	and	(
			Encounter.ConsultantCode = DivisionRuleBase.ConsultantCode
		or	DivisionRuleBase.ConsultantCode is null
		)

	and	(
			Encounter.StartWardTypeCode = DivisionRuleBase.WardCode
		or	DivisionRuleBase.WardCode is null
		)

	and	not exists
		(
		select
			1
		from
			WH.DivisionRuleBase Previous
		where
			Previous.SiteCode = Encounter.SiteCode
		and	Previous.SpecialtyCode = Encounter.SpecialtyCode

		and	
			(
				case

				when AdmissionMethod.InternalCode in 
					(
					 '1'
					,'2'
					,'3'
					)
				and ManagementIntention.InternalCode in 
					(
					 '1'
					,'3'
					,'6'
					)
				then 'EL'

				when AdmissionMethod.InternalCode in 
					(
					 '1'
					,'2'
					,'3'
					)
				and ManagementIntention.InternalCode = '2'
				and	Encounter.AdmissionDate < Encounter.DischargeDate
				then 'EL'

				when AdmissionMethod.InternalCode in 
					(
					 '1'
					,'2'
					,'3'
					)
				and ManagementIntention.InternalCode = '2'
				and	Encounter.AdmissionDate = Encounter.DischargeDate
				then 'DC'
				else 'NE'
				end
				= Previous.PatientCategoryCode

			or	Previous.PatientCategoryCode is null
			)

		and	(
				Encounter.ConsultantCode = Previous.ConsultantCode
			or	Previous.ConsultantCode is null
			)

		and	(
				Encounter.StartWardTypeCode = Previous.WardCode
			or	Previous.WardCode is null
			)

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when Previous.WardCode is not null
				then 2
				else 0
				end +

				case
				when Previous.PatientCategoryCode is not null
				then 1
				else 0
				end

				>

				case
				when DivisionRuleBase.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when DivisionRuleBase.WardCode is not null
				then 2
				else 0
				end +

				case
				when DivisionRuleBase.PatientCategoryCode is not null
				then 1
				else 0
				end

			or
				(
					case
					when Previous.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when Previous.WardCode is not null
					then 2
					else 0
					end +

					case
					when Previous.PatientCategoryCode is not null
					then 1
					else 0
					end

					=

					case
					when DivisionRuleBase.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when DivisionRuleBase.WardCode is not null
					then 2
					else 0
					end +

					case
					when DivisionRuleBase.PatientCategoryCode is not null
					then 1
					else 0
					end

					and	Previous.DivisionRuleBaseRecno > DivisionRuleBase.DivisionRuleBaseRecno
				)
			)
		)

	where
		Encounter.AdmissionTime = Encounter.EpisodeStartTime
	) Activity

where
	Activity.ProviderSpellNo = Encounter.ProviderSpellNo



update
	APC.Encounter
set
	DischargeDivisionCode = Activity.DivisionCode
from
	(
	select
		 Encounter.ProviderSpellNo
		,DivisionRuleBase.DivisionCode
	from
		APC.Encounter Encounter

	inner join PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

	inner join PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

	inner join WH.DivisionRuleBase
	on	DivisionRuleBase.SiteCode = Encounter.SiteCode
	and	DivisionRuleBase.SpecialtyCode = Encounter.SpecialtyCode

	and	
		(
			case

			when AdmissionMethod.InternalCode in 
				(
				 '1'
				,'2'
				,'3'
				)
			and ManagementIntention.InternalCode in 
				(
				 '1'
				,'3'
				,'6'
				)
			then 'EL'

			when AdmissionMethod.InternalCode in 
				(
				 '1'
				,'2'
				,'3'
				)
			and ManagementIntention.InternalCode = '2'
			and	Encounter.AdmissionDate < Encounter.DischargeDate
			then 'EL'

			when AdmissionMethod.InternalCode in 
				(
				 '1'
				,'2'
				,'3'
				)
			and ManagementIntention.InternalCode = '2'
			and	Encounter.AdmissionDate = Encounter.DischargeDate
			then 'DC'
			else 'NE'
			end
			= DivisionRuleBase.PatientCategoryCode

		or	DivisionRuleBase.PatientCategoryCode is null
		)

	and	(
			Encounter.ConsultantCode = DivisionRuleBase.ConsultantCode
		or	DivisionRuleBase.ConsultantCode is null
		)

	and	(
			Encounter.StartWardTypeCode = DivisionRuleBase.WardCode
		or	DivisionRuleBase.WardCode is null
		)

	and	not exists
		(
		select
			1
		from
			WH.DivisionRuleBase Previous
		where
			Previous.SiteCode = Encounter.SiteCode
		and	Previous.SpecialtyCode = Encounter.SpecialtyCode

		and	
			(
				case

				when AdmissionMethod.InternalCode in 
					(
					 '1'
					,'2'
					,'3'
					)
				and ManagementIntention.InternalCode in 
					(
					 '1'
					,'3'
					,'6'
					)
				then 'EL'

				when AdmissionMethod.InternalCode in 
					(
					 '1'
					,'2'
					,'3'
					)
				and ManagementIntention.InternalCode = '2'
				and	Encounter.AdmissionDate < Encounter.DischargeDate
				then 'EL'

				when AdmissionMethod.InternalCode in 
					(
					 '1'
					,'2'
					,'3'
					)
				and ManagementIntention.InternalCode = '2'
				and	Encounter.AdmissionDate = Encounter.DischargeDate
				then 'DC'
				else 'NE'
				end
				= Previous.PatientCategoryCode

			or	Previous.PatientCategoryCode is null
			)

		and	(
				Encounter.ConsultantCode = Previous.ConsultantCode
			or	Previous.ConsultantCode is null
			)

		and	(
				Encounter.StartWardTypeCode = Previous.WardCode
			or	Previous.WardCode is null
			)

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when Previous.WardCode is not null
				then 2
				else 0
				end +

				case
				when Previous.PatientCategoryCode is not null
				then 1
				else 0
				end

				>

				case
				when DivisionRuleBase.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when DivisionRuleBase.WardCode is not null
				then 2
				else 0
				end +

				case
				when DivisionRuleBase.PatientCategoryCode is not null
				then 1
				else 0
				end

			or
				(
					case
					when Previous.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when Previous.WardCode is not null
					then 2
					else 0
					end +

					case
					when Previous.PatientCategoryCode is not null
					then 1
					else 0
					end

					=

					case
					when DivisionRuleBase.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when DivisionRuleBase.WardCode is not null
					then 2
					else 0
					end +

					case
					when DivisionRuleBase.PatientCategoryCode is not null
					then 1
					else 0
					end

					and	Previous.DivisionRuleBaseRecno > DivisionRuleBase.DivisionRuleBaseRecno
				)
			)
		)

	where
		Encounter.DischargeTime = Encounter.EpisodeEndTime
	) Activity

where
	Activity.ProviderSpellNo = Encounter.ProviderSpellNo

﻿
CREATE PROCEDURE [dbo].[LoadCOM] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()



--Load Community Lookup Base
EXEC dbo.ExtractCOMLookupBase
EXEC dbo.LoadCOMLookupBase


--Load Community Specialty
EXEC dbo.ExtractCOMSpecialty
EXEC dbo.LoadCOMSpecialty


--Load Community Staff Team
EXEC dbo.ExtractCOMStaffTeam
EXEC dbo.LoadCOMStaffTeam


--Load Community Professional Carer
EXEC dbo.ExtractCOMProfessionalCarer
EXEC dbo.LoadCOMProfessionalCarer


--Load Community Health Organisation
EXEC dbo.ExtractCOMHealthOrg
EXEC dbo.LoadCOMHealthOrg


--Load Community ServicePoint and Locations
EXEC dbo.ExtractCOMServicePoint
EXEC dbo.LoadCOMServicePoint


--Load Community Referral
EXEC dbo.ExtractCOMReferral
EXEC dbo.LoadCOMReferral
--EXEC warehouseOLAP.dbo.BuildBaseCOMReferral
--EXEC warehouseOLAP.dbo.BuildFactCOMReferral

--Load Community encounter details
EXEC dbo.LoadCOMLocation

--Diagnosis Event
exec dbo.ExtractCOMDiagnosisProcedureEvent
exec dbo.LoadCOMDiagnosisProcedureEvent

exec dbo.LoadCOMDiagnosisProcedure
exec ETL.LoadCOMSystemUser


--Load Community Caseload details
TRUNCATE TABLE COM.CaseLoad
EXEC dbo.ExtractCOMCaseLoad
EXEC dbo.LoadCOMCaseLoad
--EXEC WarehouseOLAP.dbo.BuildBaseCOMCaseload
--EXEC WarehouseOLAP.dbo.BuildFactCOMCaseload



--Load Community encounter details
EXEC dbo.ExtractCOMEncounter
EXEC dbo.LoadCOMEncounter
EXEC dbo.AssignCOMEncounterDerivedFirstAttendanceFlag
--EXEC WarehouseOLAP.dbo.BuildBaseCOMEncounter
--EXEC WarehouseOLAP.dbo.BuildFactCOMEncounter

--Load Community Ward Stays
EXEC dbo.ExtractCOMWardStay
EXEC dbo.LoadCOMWardStay

exec  dbo.LoadCOMWait

update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADCOMDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADCOMDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadCOM', @Stats, @StartTime

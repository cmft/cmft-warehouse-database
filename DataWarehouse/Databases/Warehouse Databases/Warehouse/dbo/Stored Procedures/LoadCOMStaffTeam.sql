﻿


CREATE procedure [dbo].[LoadCOMStaffTeam] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the TImport table

TRUNCATE TABLE COM.StaffTeam

-- Insert new rows into encounter destination table
INSERT INTO COM.StaffTeam
(
	 StaffTeamID--[STEAM_REFNO]
	
	,StaffTeamCode--[STEAM_REFNO_CODE]
	
	,StaffTeam--[STEAM_REFNO_NAME]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]
	
	,StaffTeamDescription--[DESCRIPTION]

	,StaffTeamSpecialtyID--[SPECT_REFNO]

	,TeamLeaderID--[LEADR_PROCA_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,StaffTeamHealthOrgOwner--[OWNER_HEORG_REFNO]
	
) 
SELECT
	 StaffTeamID--[STEAM_REFNO]
	
	,StaffTeamCode--[STEAM_REFNO_CODE]
	
	,StaffTeam--[STEAM_REFNO_NAME]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]
	
	,StaffTeamDescription--[DESCRIPTION]

	,StaffTeamSpecialtyID--[SPECT_REFNO]

	,TeamLeaderID--[LEADR_PROCA_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,StaffTeamHealthOrgOwner--[OWNER_HEORG_REFNO]

FROM
	dbo.TLoadCOMStaffTeam
	
INSERT INTO COM.StaffTeam
(

	 StaffTeamID--[STEAM_REFNO]
	
	,StaffTeamCode--[STEAM_REFNO_CODE]
	
	,StaffTeam--[STEAM_REFNO_NAME]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]
	
	,StaffTeamDescription--[DESCRIPTION]

	,StaffTeamSpecialtyID--[SPECT_REFNO]

	,TeamLeaderID--[LEADR_PROCA_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,StaffTeamHealthOrgOwner--[OWNER_HEORG_REFNO]
)
VALUES
(
 -1
,-1
,'Not Recorded'
,'01/01/1900'
,NULL
,'Not Recorded'
,NULL
,NULL
,'N'
,NULL
)

/*
select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadCOMEncounter', @Stats, @StartTime
*/
﻿CREATE proc dbo.LoadCancelledOperation

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, CancellationDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, CancellationDate)) 
from
	dbo.TLoadCancelledOperation

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Theatre.CancelledOperation target
using
	(
	select
		 SourceUniqueID
		,CasenoteNumber = cast(CasenoteNumber as varchar(100))
		,DirectorateCode = cast(DirectorateCode as varchar(2))
		,NationalSpecialtyCode = cast(NationalSpecialtyCode as varchar(30))
		,ConsultantCode = cast(ConsultantCode as varchar(100))
		,AdmissionDate = cast(AdmissionDate as date)
		,ProcedureDate = cast(ProcedureDate as date)
		,WardCode = cast(WardCode as varchar(100))
		,Anaesthetist = cast(Anaesthetist as varchar(100))
		,Surgeon = cast(Surgeon as varchar(100))
		,ProcedureCode = cast(ProcedureCode as varchar(100))
		,CancellationDate = cast(CancellationDate as date)
		,CancellationReason = cast(CancellationReason as varchar(100))
		,TCIDate = cast(TCIDate as date)
		,Comments = cast(Comments as varchar(max))
		,ProcedureCompleted = cast(ProcedureCompleted as bit)
		,BreachwatchComments = cast(BreachwatchComments as varchar(max))
		,Removed = cast(Removed as bit)
		,RemovedComments = cast(RemovedComments as varchar(max))
		,ReportableBreach = cast(ReportableBreach as bit)
		,ContextCode = cast(ContextCode as varchar(5))
	from
		dbo.TLoadCancelledOperation
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.CancellationDate between @LoadStartDate and @LoadEndDate
	and target.SourceUniqueID is not null

	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CasenoteNumber
			,DirectorateCode
			,NationalSpecialtyCode
			,ConsultantCode
			,AdmissionDate
			,ProcedureDate
			,WardCode
			,Anaesthetist
			,Surgeon
			,ProcedureCode
			,CancellationDate
			,CancellationReason
			,TCIDate
			,Comments
			,ProcedureCompleted
			,BreachwatchComments
			,Removed
			,RemovedComments
			,ReportableBreach
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)

		values
			(
			 source.SourceUniqueID
			,source.CasenoteNumber
			,source.DirectorateCode
			,source.NationalSpecialtyCode
			,source.ConsultantCode
			,source.AdmissionDate
			,source.ProcedureDate
			,source.WardCode
			,source.Anaesthetist
			,source.Surgeon
			,source.ProcedureCode
			,source.CancellationDate
			,source.CancellationReason
			,source.TCIDate
			,source.Comments
			,source.ProcedureCompleted
			,source.BreachwatchComments
			,source.Removed
			,source.RemovedComments
			,source.ReportableBreach
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.DirectorateCode, '') = isnull(source.DirectorateCode,'')
		and isnull(target.NationalSpecialtyCode, '') = isnull(source.NationalSpecialtyCode, '')
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.AdmissionDate, '1 jan 1900') = isnull(source.AdmissionDate, '1 jan 1900')
		and isnull(target.ProcedureDate, '1 jan 1900') = isnull(source.ProcedureDate, '1 jan 1900')
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.Anaesthetist, '') = isnull(source.Anaesthetist, '')
		and isnull(target.Surgeon, '') = isnull(source.Surgeon, '')
		and isnull(target.ProcedureCode, '') = isnull(source.ProcedureCode, '')
		and isnull(target.CancellationDate, '1 jan 1900') = isnull(source.CancellationDate, '1 jan 1900')
		and isnull(target.CancellationReason, '') = isnull(source.CancellationReason, '')
		and isnull(target.TCIDate, '1 jan 1900') = isnull(source.TCIDate, '1 jan 1900')
		and isnull(target.Comments, '') = isnull(source.Comments, '')
		and isnull(target.ProcedureCompleted, 0) = isnull(source.ProcedureCompleted, 0)
		and isnull(target.BreachwatchComments, '') = isnull(source.BreachwatchComments, '')
		and isnull(target.Removed, 0) = isnull(source.Removed, 0)
		and isnull(target.RemovedComments, '') = isnull(source.RemovedComments, '')
		and isnull(target.ReportableBreach, 0) = isnull(source.ReportableBreach, 0)
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')

		)
	then
		update
		set
			target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DirectorateCode = source.DirectorateCode
			,target.NationalSpecialtyCode = source.NationalSpecialtyCode
			,target.ConsultantCode = source.ConsultantCode
			,target.AdmissionDate = source.AdmissionDate
			,target.ProcedureDate = source.ProcedureDate
			,target.WardCode = source.WardCode
			,target.Anaesthetist = source.Anaesthetist
			,target.Surgeon = source.Surgeon
			,target.ProcedureCode = source.ProcedureCode
			,target.CancellationDate = source.CancellationDate
			,target.CancellationReason = source.CancellationReason
			,target.TCIDate = source.TCIDate
			,target.Comments = source.Comments
			,target.ProcedureCompleted = source.ProcedureCompleted
			,target.BreachwatchComments = source.BreachwatchComments
			,target.Removed = source.Removed
			,target.RemovedComments = source.RemovedComments
			,target.ReportableBreach = source.ReportableBreach
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
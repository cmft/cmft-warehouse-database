﻿


CREATE procedure [dbo].[ExtractCOMSpecialty]
	-- @from smallTime
	--,@to smallTime
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

--DECLARE @from smalldatetime = NULL
--DECLARE @to smalldatetime = NULL
--set @from = '04/01/2011'
--set @to = '04/02/2011'


TRUNCATE TABLE dbo.TImportCOMSpecialty

INSERT INTO dbo.TImportCOMSpecialty
(
	 SpecialtyID  --[SPECT_REFNO]
	,SpecialtyCode --[MAIN_IDENT]
	,Specialty  --[DESCRIPTION]
	,SpecialtyParentID --[PARNT_REFNO]
	,SpecialtyHealthOrgOwner --[OWNER_HEORG_REFNO]
	
) 
SELECT
	 SpecialtyID  = SPECT_REFNO
	,SpecialtyCode = MAIN_IDENT
	,Specialty  = [DESCRIPTION]
	,SpecialtyParentID = PARNT_REFNO
	,SpecialtyHealthOrgOwner = OWNER_HEORG_REFNO
FROM 
	[$(ipm)].dbo.SPECIALTIES_V01 Specialty

WHERE 
	Specialty.[OWNER_HEORG_REFNO] IN
		(
		 2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
		,2031409 -- 5NT MANCHESTER PCT
		,2001932 -- NWWM Cluster
		)

AND NOT EXISTS
	(
	SELECT
		1
	FROM
		[$(ipm)].dbo.SPECIALTIES_V01 LaterSpecialty
	WHERE
		[OWNER_HEORG_REFNO] IN
			(
			 2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
			,2031409 -- 5NT MANCHESTER PCT
			,2001932 -- NWWM Cluster
			)
		AND	LaterSpecialty.SPECT_REFNO = Specialty.SPECT_REFNO
		AND	LaterSpecialty.MODIF_DTTM > Specialty.MODIF_DTTM
	)


/*

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractCommunityEncounter', @Stats, @StartTime
*/
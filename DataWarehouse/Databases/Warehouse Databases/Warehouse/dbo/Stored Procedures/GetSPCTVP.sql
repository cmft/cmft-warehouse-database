﻿
/* SP to populate TVP */

create proc GetSPCTVP
(
   @datasource dbo.SPCTableType readonly
   ,@group varchar(50) = null
)

as

/* Create cartesian for dates and group combinations with no values */

select 
	[group].[Group]
	,[date].[Date]
	,Cases = coalesce(data.Cases, 0)
	--,StepChangeStartID
	
from 
(
	(
	select distinct 
		[Date]
	from 
		@datasource
	) [date]
	
	cross join 
	(
	select distinct 
		[Group]
	from 
		@datasource
	) [group]
) 

left outer join @datasource data
on data.[Date] = [date].[Date]
and data.[Group] = [group].[Group]
where
	[group].[Group] = @group 
order by
	[Group]
	,[Date]

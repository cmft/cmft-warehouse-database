﻿

CREATE PROCEDURE [dbo].[LoadCOMLocation] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()



TRUNCATE TABLE COM.Location

INSERT INTO COM.Location

SELECT LocationType = 'Clinic'
	,Location = Healthorg.[DESCRIPTION]
	,LocationClinic =  ServicePoint.NAME
	,LocationClinicCode = ServicePoint.[CODE]
	,LocationCode = ServicePoint.SPONT_REFNO

FROM [$(ipm)].dbo.ServicePoint ServicePoint
	LEFT OUTER JOIN [$(ipm)].dbo.HealthOrganisation Healthorg
	ON ServicePoint.HEORG_REFNO = Healthorg.HEORG_REFNO


INSERT INTO COM.Location

SELECT 
	 LocationType = [DESCRIPTION]
	,Location = [DESCRIPTION] 
	,LocationClinic = [DESCRIPTION]
	,LocationClinicCode = 'Not Applicable as Contact'
	,LocationCode = RFVAL_REFNO

FROM        
	(
	SELECT
		*
	FROM
		[$(ipm)].dbo.ReferenceValue
	WHERE
		RFVDM_CODE <> 'COUNT'
		)REFERENCE_VALUES

WHERE     
	(RFVDM_CODE = 'LOTYP')


INSERT INTO COM.Location

SELECT DISTINCT 
	 LocationType = tbl_location_type_lkp.DESCRIPTION 
	,Location= tbl_hlth_org.[DESCRIPTION] 
	,LocationClinic =  tbl_hlth_org.[DESCRIPTION] 
	,LocationClinicCode =  'Not Applicable as Contact'
	,LocationCode = CAST(
							CAST(LOTYP_HEORG_REFNO as varchar)+CAST(LOTYP_REFNO as varchar)  
							AS numeric(20,0)
						)

FROM       
	(SELECT DISTINCT 
		 LOTYP_REFNO
		,LOTYP_HEORG_REFNO  
	FROM 
		[$(ipm)].dbo.Schedule
	WHERE 
		SCTYP_REFNO = 1468
	) con
	INNER JOIN (SELECT 
						* 
				FROM  
							(
							SELECT
								*
							FROM
								[$(ipm)].dbo.ReferenceValue
							WHERE
								RFVDM_CODE <> 'COUNT'
								)REFERENCE_VALUES
				WHERE   
						RFVDM_CODE = 'LOTYP'
				)  tbl_location_type_lkp
	
	ON  tbl_location_type_lkp.RFVAL_REFNO = con.LOTYP_REFNO
	
	INNER JOIN (
					SELECT 
							*
					FROM 
							[$(ipm)].dbo.HealthOrganisation
					
					WHERE  
							[OWNER_HEORG_REFNO] IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
												  ,2031409 -- 5NT MANCHESTER PCT
												  ,2001932) -- NWWM Cluster
				) tbl_hlth_org
	ON tbl_hlth_org.HEORG_REFNO = con.LOTYP_HEORG_REFNO

	
INSERT INTO COM.Location

SELECT    LocationType = 'Not Applicable'
		, Location = 'Not Applicable'
		, LocationClinic = 'Not Applicable as Waiting List or Referral'
		, LocationClinicCode ='Not Applicable'
		, LocationCode = '-1'	

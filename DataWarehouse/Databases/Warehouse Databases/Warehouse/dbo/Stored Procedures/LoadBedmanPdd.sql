﻿


CREATE procedure [dbo].[LoadBedmanPdd] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

--select @from = (select min(convert(smalldatetime, DischargeDatetime)) from TLoadBedmanSpells)
--select @to = (select max(convert(smalldatetime, DischargeDatetime)) from TLoadBedmanSpells)

--delete from Bedman.Spells
--where	coalesce(DischargeDatetime, @from) between @from and @to


select @RowsDeleted = @@rowcount

insert into Bedman.PddHistory
(
		  PddHistoryID
		,SourceSpellNo
		,PddSequence
		,prev_PddSequence
		,next_PddSequence
		,FirstPdd
		,LastPdd
		,Pdd
		,prev_Pdd
		,next_Pdd
		,PddCreated
		,PddPAS
		,PddReasonCode
)

SELECT 	  PddHistoryID
		,SourceSpellNo
		,PddSequence
		,prev_PddSequence
		,next_PddSequence
		,FirstPdd
		,LastPdd
		,Pdd
		,prev_Pdd
		,next_Pdd
		,PddCreated
		,PddPAS
		,PddReasonCode

FROM dbo.TLoadBedmanPdd
	



select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadBedmanPdd', @Stats, @StartTime




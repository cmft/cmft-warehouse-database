﻿
CREATE PROCEDURE [dbo].[ExtractCOMEncounter]

AS

--	20150130	RR	added in called time
-- 17/09/2015 GC added check for missing updated records

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

SELECT  @lastmodifiedTime = coalesce(
										MAX(CONVERT(datetime, ModifiedTime)) 
										,'01/01/1900'
									)
FROM COM.Encounter

DECLARE @from smalldatetime = @lastmodifiedTime
DECLARE @to smalldatetime =CURRENT_TIMESTAMP

--create records for loading based on missing NHS number
select 

	 PATNT_REFNO
	,SourceUniqueID

into #PatientUpdated


from COM.Encounter Encounter 

inner join [$(ipm)].[dbo].[Patient] Patient
on Patient.PATNT_REFNO = Encounter.PatientSourceID

where 
	(
		PatientNHSNumber is null
		and 
		PatientSourceID is not null
		and 
		NHS_IDENTIFIER is not NULL
	)
	OR PatientNHSNumber <> NHS_IDENTIFIER

--Create list of missing encounters

select 

	SCHDL_REFNO
	
	into #MissingEncounter

FROM 
		[$(ipm)].dbo.SCHEDULES_VE02 SourceEncounter
		
where

not exists
	
	(
		Select
			1
			from COM.Encounter Encounter
			
			where Encounter.SourceUniqueID = SourceEncounter.SCHDL_REFNO
			
	)
and 
--SourceEncounter.MODIF_DTTM >= '01 april 2014'	
--and
SourceEncounter.MODIF_DTTM <= @lastmodifiedTime


--create records that have not been updated

select 
 SCHDL_REFNO
,Encounter.MODIF_DTTM

into #NeedUpdate

from          [$(ipm)].dbo.SCHEDULES_VE02 Encounter
where 

Encounter.MODIF_DTTM > '01 april 2011'

and


exists

(

select 1 from COM.Encounter WarehouseEncounter 

where WarehouseEncounter.SourceUniqueID = Encounter.SCHDL_REFNO
and cast(Encounter.MODIF_DTTM as date) > cast(WarehouseEncounter.ModifiedTime as date)


and not exists

(
Select 1 from COM.Encounter c

where c.SourceUniqueID =  WarehouseEncounter.SourceUniqueID
and c.ModifiedTime > WarehouseEncounter.ModifiedTime

)

)



--delete
TRUNCATE TABLE dbo.TImportCOMEncounter


INSERT INTO dbo.TImportCOMEncounter
(
	 SourceEncounterID
	,SourceUniqueID
	,SpecialtyID
	,StaffTeamID
	,ProfessionalCarerID
	,SeenByProfessionalCarerID 
	,StartTime 
	,EndTime
	,ArrivedTime 
	,SeenTime 
	,DepartedTime  
	,AttendedID
	,OutcomeID 
	,ReferralID 
	,ProfessionalCarerEpisodeID
	,PatientSourceID 
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber 
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
	,PatientTitleID 
	,PatientForename 
	,PatientSurname 
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,PatientCurrentCommissionerCode
	,PatientRegisteredCommissionerCode
	,VisitID
	,ContactTypeID
	,ScheduleTypeID
	,ScheduleReasonID
	,CanceledReasonID
	,CanceledTime
	,CancelledByID
	,MoveReasonID
	,MoveTime
	,ServicePointID
	,ServicePointSessionID
	,MoveCount
	,LocationTypeID
	,LocationDescription
	,LocationTypeHealthOrgID
	,ServicePointStaysID
	,WaitingListID
	,PlannedAttendees
	,ActualAttendees
	,ProviderSpellID
	,RTTStatusID
	,EarliestReasinableOfferTime
	,EncounterInterventionsCount
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,ParentID
	,HealthOrgOwnerID
	,Comment
	,ReferralReceivedTime
	,ReferralClosedTime
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID

	,PatientEncounterRegisteredPracticeID
	,DNAReasonID
	,CalledTime
) 

Select Distinct
	 SourceEncounterID = encounter.SourceEncounterID
	,SourceUniqueID = Encounter.SCHDL_REFNO
	,SpecialtyID = Encounter.SPECT_REFNO
	,StaffTeamID = Encounter.STEAM_REFNO
	,ProfessionalCarerID = Encounter.PROCA_REFNO
	,SeenByProfessionalCarerID = Encounter.SEENBY_PROCA_REFNO
	,StartTime = Encounter.START_DTTM
	,EndTime = Encounter.END_DTTM
	,ArrivedTime = Encounter.ARRIVED_DTTM
	,SeenTime  = Encounter.SEEN_DTTM
	,DepartedTime  = Encounter.DEPARTED_DTTM
	,AttendedID = Encounter.ATTND_REFNO 
	,OutcomeID  = Encounter.SCOCM_REFNO 
	,ReferralID  = Encounter.REFRL_REFNO
	,ProfessionalCarerEpisodeID = Encounter.PRCAE_REFNO
	,PatientSourceID = Encounter.PATNT_REFNO
	,PatientSourceSystemUniqueID = Patient.PASID
	,PatientNHSNumber = Patient.NHS_IDENTIFIER
	,PatientNHSNumberStatusIndicator = 1
	,PatientIDLocalIdentifier = PatientIDLocalIdentifier.IDENTIFIER
	,PatientIDPASNumber = PatientIDPASNumber.IDENTIFIER
	,PatientIDPatientIdentifier = PatientIDPatientIdentifier.IDENTIFIER
	,PatientIDPatientFacilityID = PatientIDPatientFacilityID.IDENTIFIER
	,PatientTitleID = Patient.TITLE_REFNO
	,PatientForename = Patient.FORENAME
	,PatientSurname = Patient.SURNAME
	,PatientAddress1 = EncounterPatientAddress.LINE1
	,PatientAddress2 = EncounterPatientAddress.LINE2
	,PatientAddress3 = EncounterPatientAddress.LINE3
	,PatientAddress4 = EncounterPatientAddress.LINE4
	,PatientPostcode = EncounterPatientAddress.PCODE
	,PatientDateOfBirth = Patient.DATE_OF_BIRTH
	,PatientDateOfDeath = Patient.DATE_OF_DEATH
	,PatientSexID = Patient.SEXXX_REFNO
	,PatientEthnicGroupID = Patient.ETHGR_REFNO
	,PatientSpokenLanguageID = SPOKL_REFNO
	,PatientReligionID = RELIG_REFNO
	,PatientMaritalStatusID = MARRY_REFNO
	,PatientCurrentRegisteredPracticeCode = ProfessionalCarerBase.HealthOrgCode
	,PatientEncounterRegisteredPracticeCode = ProfessionalCarerBase.HealthOrgCode
	,PatientCurrentRegisteredGPCode = ProfessionalCarerBase.ProfessionalCarerCode
	,PatientEncounterRegisteredGPCode = ProfessionalCarerBase.ProfessionalCarerCode
	,PatientCurrentCommissionerCode = ProfessionalCarerBase.ParentHealthOrgCode
	,PatientRegisteredCommissionerCode = ProfessionalCarerBase.ParentHealthOrgCode
	,VisitID  = Encounter.VISIT_REFNO --Type of appointment
	,ContactTypeID = Encounter.CONTY_REFNO --Contact Type
	,ScheduleTypeID = Encounter.SCTYP_REFNO -- appointments 1470 Contacts 1468 
	,ScheduleReasonID = Encounter.REASN_REFNO
	,CanceledReasonID = Encounter.CANCR_REFNO
	,CanceledTime = Encounter.CANCR_DTTM
	,CancelledByID = Encounter.CANCB_REFNO
	,MoveReasonID  = Encounter.MOVRN_REFNO
	,Movetime = Encounter.MOVE_DTTM
	,ServicePointID  = Encounter.SPONT_REFNO
	,ServicePointSessionID = Encounter.SPSSN_REFNO
	,MoveCount = Encounter.MOVE_COUNT
	,LocationTypeID = Encounter.LOTYP_REFNO
	,LocationDescription = Encounter.LOCATION
	,LocationTypeHealthOrgID = Encounter.LOTYP_HEORG_REFNO
	,ServicePointStaysID = Encounter.SSTAY_REFNO
	,WaitingListID = Encounter.WLIST_REFNO
	,PlannedAttendees = Encounter.PLANNED_ATTENDEES
	,ActualAttendees = Encounter.ACTUAL_ATTENDEES
	,ProviderSpellID = Encounter.PRVSP_REFNO
	,RTTStatusID = Encounter.RTTST_REFNO
	,EarliestReasinableOffertime = Encounter.ERO_DTTM
	,EncounterInterventionsCount = CountInterventions
	,Createdtime = Encounter.CREATE_DTTM
	,Modifiedtime = Encounter.MODIF_DTTM
	,CreatedByID = Encounter.USER_CREATE
	,ModifiedByID = Encounter.USER_MODIF
	,ArchiveFlag = Encounter.ARCHV_FLAG
	,ParentID = Encounter.PARNT_REFNO
	,HealthOrgOwnerID = Encounter.OWNER_HEORG_REFNO
	,Comment = Encounter.COMMENTS
	,ReferralReceivedTime = Referral.RECVD_DTTM  --Received
	,ReferralClosedTime = Referral.CLOSR_DATE --referral closed
	,ReferralReasonID = Referral.REASN_REFNO --Reason for referral
	,ReferredToSpecialtyID = Referral.REFTO_SPECT_REFNO --referred to specialty
	,ReferredToStaffTeamID = Referral.REFTO_STEAM_REFNO --referred to staff team
	,ReferredToProfCarerID = Referral.REFTO_PROCA_REFNO --referred to professional carer
	,ReferralSourceID = Referral.SORRF_REFNO -- source of referral
	,ReferralTypeID = Referral.RETYP_REFNO --referral type

	,PatientEncounterRegisteredPracticeID = ProfessionalCarerBase.HEORG_REFNO
	,DNAReasonID = Encounter.DNARS_REFNO
	,CalledTime = Encounter.CALLED_DTTM

from 
(
	 SELECT CAST(YEAR(Encounter.MODIF_DTTM) AS VARCHAR) 
				    + CAST(MONTH(Encounter.MODIF_DTTM) AS VARCHAR) 
					+ CASE 
						WHEN LEN(CAST(DAY(Encounter.MODIF_DTTM) AS VARCHAR))=2 
						THEN CAST(DAY(Encounter.MODIF_DTTM) AS VARCHAR)
						ELSE '0' + CAST(DAY(Encounter.MODIF_DTTM) AS VARCHAR)
					  END  
					+ CAST(Encounter.SCHDL_REFNO AS VARCHAR)
					+ CAST(Encounter.MOVE_COUNT AS VARCHAR) AS SourceEncounterID
			,*
	FROM 
			[$(ipm)].dbo.SCHEDULES_VE02 Encounter
	 WHERE 
		Encounter.MODIF_DTTM > @lastmodifiedTime
		or
		exists
			
			(
		select 1 from #PatientUpdated 
		where #PatientUpdated.PATNT_REFNO = Encounter.PATNT_REFNO
		and #PatientUpdated.SourceUniqueID =  Encounter.SCHDL_REFNO
			)
		or
		exists
			
		(
			select 
			
				1 
			from #MissingEncounter 
			
			where 
			#MissingEncounter.SCHDL_REFNO =  Encounter.SCHDL_REFNO
		)
		or
		exists

		(
		Select 1 from #NeedUpdate 
		where #NeedUpdate.SCHDL_REFNO =  Encounter.SCHDL_REFNO
		and #NeedUpdate.MODIF_DTTM = Encounter.MODIF_DTTM
		)		
		
			
 ) Encounter


LEFT OUTER JOIN 
(
	 SELECT 
		 REFRL_REFNO
		,RECVD_DTTM  --RECEIVED
		,CLOSR_DATE --REFERRAL CLOSED
		,REASN_REFNO --REASON FOR REFERRAL
		,REFTO_SPECT_REFNO --REFERRED TO SPECIALTY
		,REFTO_STEAM_REFNO --REFERRED TO SPECIALTY
		,REFTO_PROCA_REFNO --REFERRED TO PROFESSIONAL CARER
		,SORRF_REFNO -- SOURCE OF REFERRAL
		,RETYP_REFNO --referral type
	 FROM
		[$(ipm)].dbo.Referral
)Referral

ON 
	--case 
	--when Encounter.REFRL_REFNO is null then 
	--(
	--	select	
	--		ParentEncounter.REFRL_REFNO
	--	from
	--		[$(ipm)].dbo.SCHEDULES_VE02 ParentEncounter	
	--	where 
	--		ParentEncounter.SCHDL_REFNO = Encounter.PARNT_REFNO
	--	and not exists
	--		(
	--		select
	--		1
	--		from
	--			[$(ipm)].dbo.SCHEDULES_VE02 PreviousParentEncounter	
	--		where 
	--			PreviousParentEncounter.SCHDL_REFNO = ParentEncounter.SCHDL_REFNO
	--		and PreviousParentEncounter.MODIF_DTTM < ParentEncounter.MODIF_DTTM
	--		)
	--)	
	--else 
	Encounter.REFRL_REFNO
	--end 
	= Referral.REFRL_REFNO


LEFT OUTER JOIN 
(
	 SELECT 
		 PATNT_REFNO
		,PASID
		,NHS_IDENTIFIER
		,TITLE_REFNO
		,FORENAME
		,SURNAME
		,DATE_OF_BIRTH
		,DATE_OF_DEATH
		,SEXXX_REFNO
		,ETHGR_REFNO
		,SPOKL_REFNO
		,RELIG_REFNO
		,MARRY_REFNO
	 FROM
		[$(ipm)].dbo.Patient
)Patient

ON Encounter.PATNT_REFNO = Patient.PATNT_REFNO

LEFT OUTER JOIN
(
	SELECT 
		 PATIENTID.PATNT_REFNO
		,PATIENTID.IDENTIFIER
	FROM 
		[$(ipm)].dbo.PatientID PATIENTID
	WHERE 
		PATIENTID.PITYP_REFNO = 1059		-- Local Identifier
	AND PATIENTID.ARCHV_FLAG = 'N'
	AND PATIENTID.CURNT_FLAG = 'Y'
	AND PATIENTID.END_DTTM IS NULL
	and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID previousPATIENTID
				where
					previousPATIENTID.PITYP_REFNO = 1059		-- Local Identifier
				AND previousPATIENTID.ARCHV_FLAG = 'N'
				AND previousPATIENTID.CURNT_FLAG = 'Y'
				AND previousPATIENTID.END_DTTM IS NULL
				AND previousPATIENTID.PATNT_REFNO = PATIENTID.PATNT_REFNO
				AND previousPATIENTID.MODIF_DTTM > PATIENTID.MODIF_DTTM
			)
) PatientIDLocalIdentifier
ON Encounter.PATNT_REFNO = PatientIDLocalIdentifier.PATNT_REFNO

LEFT OUTER JOIN
(
	SELECT 
		 PATIENTID.PATNT_REFNO
		,PATIENTID.IDENTIFIER
	FROM 
		[$(ipm)].dbo.PatientID PATIENTID
	WHERE 
		PATIENTID.PITYP_REFNO = 1063		-- PAS Number
	AND PATIENTID.ARCHV_FLAG = 'N'
	AND PATIENTID.CURNT_FLAG = 'Y'
	AND PATIENTID.END_DTTM IS NULL
	and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID previousPATIENTID
				where
					previousPATIENTID.PITYP_REFNO = 1063		-- PAS Number
				AND previousPATIENTID.ARCHV_FLAG = 'N'
				AND previousPATIENTID.CURNT_FLAG = 'Y'
				AND previousPATIENTID.END_DTTM IS NULL
				AND previousPATIENTID.PATNT_REFNO = PATIENTID.PATNT_REFNO
				AND previousPATIENTID.MODIF_DTTM > PATIENTID.MODIF_DTTM
			)
) PatientIDPASNumber
ON Encounter.PATNT_REFNO = PatientIDPASNumber.PATNT_REFNO

LEFT OUTER JOIN
(
	SELECT
		 PATIENTID.PATNT_REFNO
		,IDENTIFIER = max(PATIENTID.IDENTIFIER)
	FROM 
		[$(ipm)].dbo.PatientID PATIENTID
	WHERE 
		PATIENTID.PITYP_REFNO = 2001233		-- Patient Identifier
		AND PATIENTID.ARCHV_FLAG = 'N'
		AND PATIENTID.CURNT_FLAG = 'Y'
		AND PATIENTID.END_DTTM IS NULL
		and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID previousPATIENTID
				where
					previousPATIENTID.PITYP_REFNO = 2001233		-- Patient Identifier
				AND previousPATIENTID.ARCHV_FLAG = 'N'
				AND previousPATIENTID.CURNT_FLAG = 'Y'
				AND previousPATIENTID.END_DTTM IS NULL
				AND previousPATIENTID.PATNT_REFNO = PATIENTID.PATNT_REFNO
				AND previousPATIENTID.MODIF_DTTM > PATIENTID.MODIF_DTTM
			)
		Group by  PATIENTID.PATNT_REFNO

) PatientIDPatientIdentifier
ON Encounter.PATNT_REFNO = PatientIDPatientIdentifier.PATNT_REFNO

LEFT OUTER JOIN
(
	SELECT 
		 PATIENTID.PATNT_REFNO
		,PATIENTID.IDENTIFIER
	FROM 
		[$(ipm)].dbo.PatientID PATIENTID
	WHERE 
		PATIENTID.PITYP_REFNO = 2001232		-- Patient ID (Facility)
	AND PATIENTID.ARCHV_FLAG = 'N'
	AND PATIENTID.CURNT_FLAG = 'Y'
	AND PATIENTID.END_DTTM IS NULL
	and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID previousPATIENTID
				where
					previousPATIENTID.PITYP_REFNO =  2001232		-- Patient ID (Facility)
				AND previousPATIENTID.ARCHV_FLAG = 'N'
				AND previousPATIENTID.CURNT_FLAG = 'Y'
				AND previousPATIENTID.END_DTTM IS NULL
				AND previousPATIENTID.PATNT_REFNO = PATIENTID.PATNT_REFNO
				AND previousPATIENTID.MODIF_DTTM > PATIENTID.MODIF_DTTM
			)
) PatientIDPatientFacilityID
ON Encounter.PATNT_REFNO = PatientIDPatientFacilityID.PATNT_REFNO

LEFT OUTER JOIN
(
	SELECT DISTINCT
		 EncounterPatientAddress.SCHDL_REFNO
		,EncounterPatientAddress.ADDSS_REFNO
		,PatientAddressDetails.LINE1
		,PatientAddressDetails.LINE2
		,PatientAddressDetails.LINE3
		,PatientAddressDetails.LINE4
		,PatientAddressDetails.PCODE
	FROM
		(
		SELECT 
			SCHDL_REFNO,
			MAX(ADDSS_REFNO) AS ADDSS_REFNO
		FROM 
	(
	SELECT 
			 SCHDL_REFNO
			,START_DTTM
			,PATNT_REFNO
		 FROM 
				[$(ipm)].dbo.Schedule Encounter
		 WHERE   
				Encounter.MODIF_DTTM > @lastmodifiedTime
		or
		exists
	
			(
		select 1 from #PatientUpdated 
		where #PatientUpdated.PATNT_REFNO = Encounter.PATNT_REFNO
	
	)
	or
		exists
			
		(
			select 
			
				1 
			from #MissingEncounter 
			
			where 
			#MissingEncounter.SCHDL_REFNO =  Encounter.SCHDL_REFNO
		)						
			)Encounter

		LEFT OUTER JOIN

			(
			SELECT
					 AddressRole.ADDSS_REFNO
					,AddressRole.START_DTTM
					,AddressRole.END_DTTM
					,AddressRole.PATNT_REFNO
			FROM 
					[$(ipm)].dbo.AddressRole AddressRole
			WHERE   
					ROTYP_CODE = 'HOME'
				AND ARCHV_FLAG = 'N'
			)PatientAddress
		ON Encounter.PATNT_REFNO = PatientAddress.PATNT_REFNO
		AND PatientAddress.START_DTTM <= Encounter.START_DTTM 
		AND (PatientAddress.END_DTTM IS NULL OR PatientAddress.END_DTTM > Encounter.START_DTTM)
		GROUP BY 
				SCHDL_REFNO
	)EncounterPatientAddress
	
	LEFT OUTER JOIN (	
					SELECT DISTINCT
						 ADDSS_REFNO
						,LINE1
						,LINE2
						,LINE3
						,LINE4
						,PCODE
					 FROM 
						[$(ipm)].dbo.[Address]
					 WHERE ADTYP_CODE = 'POSTL'
					   AND ARCHV_FLAG = 'N'
					) PatientAddressDetails
	ON EncounterPatientAddress.ADDSS_REFNO = PatientAddressDetails.ADDSS_REFNO
)EncounterPatientAddress

ON Encounter.SCHDL_REFNO = EncounterPatientAddress.SCHDL_REFNO

LEFT OUTER JOIN
(
	SELECT DISTINCT
			 EncounterPatientProfessionalCarer.SCHDL_REFNO
			,EncounterPatientProfessionalCarer.PATPC_REFNO
			,ProfessionalCarerCode
			,HealthOrgCode
			,ParentHealthOrgCode
			,PatientProfCarerHealthOrg.HEORG_REFNO
	FROM
			(
			SELECT 
				SCHDL_REFNO
				,MAX(PATPC_REFNO) AS PATPC_REFNO
			FROM 
				(
					SELECT 
							 SCHDL_REFNO
							,START_DTTM
							,PATNT_REFNO
					 FROM 
							[$(ipm)].dbo.Schedule Encounter
 WHERE  
		Encounter.MODIF_DTTM > @lastmodifiedTime
	or
	exists

	(
		select 1 from #PatientUpdated 
		where #PatientUpdated.PATNT_REFNO = Encounter.PATNT_REFNO

	)
	or
	exists
			
	(
		select 
		
			1 
		from #MissingEncounter 
		
		where 
		#MissingEncounter.SCHDL_REFNO =  Encounter.SCHDL_REFNO
	)	
							
				)Encounter
			LEFT OUTER JOIN
				(
					SELECT
							 PatientProfessionalCarer.PATPC_REFNO
							,PatientProfessionalCarer.START_DTTM
							,PatientProfessionalCarer.END_DTTM
							,PatientProfessionalCarer.PATNT_REFNO
							,PatientProfessionalCarer.HEORG_REFNO
					FROM 
							[$(ipm)].dbo.PatientProfessionalCarer 
							
					WHERE   ARCHV_FLAG = 'N'
						AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarer

				ON Encounter.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
				AND PatientProfCarer.START_DTTM <= Encounter.START_DTTM 
				AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > Encounter.START_DTTM)
			GROUP BY 
				SCHDL_REFNO

	)EncounterPatientProfessionalCarer

	LEFT OUTER JOIN (
						SELECT
							 PatientProfessionalCarer.PATPC_REFNO
							,PatientProfessionalCarer.START_DTTM
							,PatientProfessionalCarer.END_DTTM
							,PatientProfessionalCarer.PATNT_REFNO
							,PatientProfessionalCarer.HEORG_REFNO
							,PatientProfessionalCarer.PROCA_MAIN_IDENT AS ProfessionalCarerCode
						FROM 
								[$(ipm)].dbo.PatientProfessionalCarer 
						WHERE   
								ARCHV_FLAG = 'N'
							AND PRTYP_REFNO = '1132'-- General Practitioner
					)PatientProfCarerDetail
	ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO

	LEFT OUTER JOIN (	
						SELECT DISTINCT
								 HEORG_REFNO
								,HealthOrganisation.[DESCRIPTION] AS HealthOrgDescription
								,HealthOrganisation.MAIN_IDENT AS HealthOrgCode
								,HealthOrganisation.PARNT_REFNO
						 FROM 
								[$(ipm)].dbo.HealthOrganisation 
						 WHERE 
								ARCHV_FLAG = 'N'
						) PatientProfCarerHealthOrg
							 
	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
	
	LEFT OUTER JOIN 
		(	
			SELECT DISTINCT
					  HealthOrganisation.HEORG_REFNO ParentHEORG_REFNO
					 ,HealthOrganisation.[DESCRIPTION] AS ParentHealthOrgDescription
					 ,HealthOrganisation.MAIN_IDENT AS ParentHealthOrgCode
					 ,HealthOrganisation.HOTYP_REFNO AS ParentHealthOrgCodeType
			 FROM 
					[$(ipm)].dbo.HealthOrganisation 
			 WHERE 
					ARCHV_FLAG = 'N'
			   AND HOTYP_REFNO = 629
		) ParentPatientProfCarerHealthOrg

	ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
	
)ProfessionalCarerBase
ON encounter.SCHDL_REFNO = ProfessionalCarerBase.SCHDL_REFNO

LEFT OUTER JOIN
(
SELECT 
		 SORCE_REFNO
		,COUNT(DISTINCT DGPRO_REFNO) CountInterventions
FROM 
		[$(ipm)].dbo.DIAGNOSIS_PROCEDURES_V03
WHERE  
		SORCE_CODE NOT IN ('WLIST', 'REFRL','PATNT')
GROUP BY 
		SORCE_REFNO

)Interventions
ON Encounter.SCHDL_REFNO = Interventions.SORCE_REFNO


select @RowsInserted = @@rowcount
;

-- Non patient events here

with GroupContact

(
	SourceEncounterID 
	,SourceUniqueID
	,SpecialtyID 
	,StaffTeamID
	,ProfessionalCarerID 
	,SeenByProfessionalCarerID
	,StartTime 
	,EndTime
	,ArrivedTime 
	,SeenTime  
	,DepartedTime 
	,AttendedID 
	,OutcomeID 
	,ReferralID 
	,ProfessionalCarerEpisodeID
	,PatientSourceID
	,PatientSourceSystemUniqueID 
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID 
	,PatientTitleID 
	,PatientForename 
	,PatientSurname 
	,PatientAddress1 
	,PatientAddress2
	,PatientAddress3 
	,PatientAddress4 
	,PatientPostcode 
	,PatientDateOfBirth 
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID 
	,PatientReligionID 
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode 
	,PatientEncounterRegisteredPracticeCode 
	,PatientCurrentRegisteredGPCode 
	,PatientEncounterRegisteredGPCode 
	,PatientCurrentCommissionerCode 
	,PatientRegisteredCommissionerCode
	,VisitID
	,ContactTypeID 
	,ScheduleTypeID
	,ScheduleReasonID 
	,CancelledReasonID 
	,CancelledTime 
	,CancelledByID
	,MoveReasonID  
	,Movetime 
	,ServicePointID  
	,ServicePointSessionID 
	,MoveCount
	,LocationTypeID
	,LocationDescription 
	,LocationTypeHealthOrgID 
	,ServicePointStaysID 
	,WaitingListID 
	,PlannedAttendees 
	,ActualAttendees 
	,ProviderSpellID
	,RTTStatusID
	,EarliestReasinableOffertime
	,EncounterInterventionsCount 
	,Createdtime 
	,Modifiedtime 
	,CreatedByID 
	,ModifiedByID 
	,ArchiveFlag 
	,ParentID 
	,HealthOrgOwnerID
	,Comment 
	,ReferralReceivedTime
	,ReferralClosedTime 
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID 
	,ReferredToProfCarerID 
	,ReferralSourceID
	,ReferralTypeID 
	,PatientEncounterRegisteredPracticeID 
	,DNAReasonID	
	,CalledTime
)

as

	(
	select
		SourceEncounterID = 	
						cast(year(GroupContact.MODIF_DTTM) as varchar) 
						+ cast(month(GroupContact.MODIF_DTTM) as varchar) 
						+ case 
							when len(cast(day(GroupContact.MODIF_DTTM) as varchar)) = 2 
							then cast(day(GroupContact.MODIF_DTTM) as varchar)
							else '0' + cast(day(GroupContact.MODIF_DTTM) as varchar)
						  end  
						+ cast(GroupContact.SCHDL_REFNO as varchar)
						+ cast(GroupContact.MOVE_COUNT as varchar)
		,SourceUniqueID = GroupContact.SCHDL_REFNO
		,SpecialtyID = GroupContact.SPECT_REFNO
		,StaffTeamID = GroupContact.STEAM_REFNO
		,ProfessionalCarerID = GroupContact.PROCA_REFNO
		,SeenByProfessionalCarerID = null
		,StartTime = GroupContact.START_DTTM
		,EndTime = GroupContact.END_DTTM
		,ArrivedTime = GroupContact.START_DTTM
		,SeenTime  = GroupContact.START_DTTM
		,DepartedTime  = GroupContact.END_DTTM
		,AttendedID = 45 -- Attended -- Encounter.ATTND_REFNO 
		,OutcomeID  = 3844 -- Open Appointment Encounter.SCOCM_REFNO 
		,ReferralID = -1 -- Default to -1 to let the rows into WOMV2 via TLoad view null -- Encounter.REFRL_REFNO
		,ProfessionalCarerEpisodeID = null -- Encounter.PRCAE_REFNO
		,PatientSourceID = null -- Encounter.PATNT_REFNO
		,PatientSourceSystemUniqueID = null -- Patient.PASID
		,PatientNHSNumber = null -- Patient.NHS_IDENTIFIER
		,PatientNHSNumberStatusIndicator = null -- 1
		,PatientIDLocalIdentifier = null -- PatientIDLocalIdentifier.IDENTIFIER
		,PatientIDPASNumber = null -- PatientIDPASNumber.IDENTIFIER
		,PatientIDPatientIdentifier = null -- PatientIDPatientIdentifier.IDENTIFIER
		,PatientIDPatientFacilityID = null -- PatientIDPatientFacilityID.IDENTIFIER
		,PatientTitleID = null -- Patient.TITLE_REFNO
		,PatientForename = null -- Patient.FORENAME
		,PatientSurname = null -- Patient.SURNAME
		,PatientAddress1 = null -- EncounterPatientAddress.LINE1
		,PatientAddress2 = null -- EncounterPatientAddress.LINE2
		,PatientAddress3 = null -- EncounterPatientAddress.LINE3
		,PatientAddress4 = null -- EncounterPatientAddress.LINE4
		,PatientPostcode = null -- EncounterPatientAddress.PCODE
		,PatientDateOfBirth = null -- Patient.DATE_OF_BIRTH
		,PatientDateOfDeath = null -- Patient.DATE_OF_DEATH
		,PatientSexID = null -- Patient.SEXXX_REFNO
		,PatientEthnicGroupID = null -- Patient.ETHGR_REFNO
		,PatientSpokenLanguageID = null -- SPOKL_REFNO
		,PatientReligionID = null -- RELIG_REFNO
		,PatientMaritalStatusID = null -- MARRY_REFNO
		,PatientCurrentRegisteredPracticeCode = null -- ProfessionalCarerBase.HealthOrgCode
		,PatientEncounterRegisteredPracticeCode = null -- ProfessionalCarerBase.HealthOrgCode
		,PatientCurrentRegisteredGPCode = null -- ProfessionalCarerBase.ProfessionalCarerCode
		,PatientEncounterRegisteredGPCode = null -- ProfessionalCarerBase.ProfessionalCarerCode
		,PatientCurrentCommissionerCode = null -- ProfessionalCarerBase.ParentHealthOrgCode
		,PatientRegisteredCommissionerCode = null -- ProfessionalCarerBase.ParentHealthOrgCode
		,VisitID = GroupContact.VISIT_REFNO --Type of appointment
		,ContactTypeID = 1452 -- Contact.CONTY_REFNO --Contact Type -- Group Session 1452 Group with Client 2004178  Face to Face with Group 3307376
		,ScheduleTypeID = 1468 -- GroupContact.SCTYP_REFNO -- appointments 1470 Contacts 1468 
		,ScheduleReasonID = GroupContact.REASN_REFNO
		,CancelledReasonID = GroupContact.CANCR_REFNO
		,CancelledTime = GroupContact.CANCR_DTTM
		,CancelledByID = GroupContact.CANCB_REFNO
		,MoveReasonID  = GroupContact.MOVRN_REFNO
		,Movetime = GroupContact.MOVE_DTTM
		,ServicePointID  = GroupContact.SPONT_REFNO
		,ServicePointSessionID = GroupContact.SPSSN_REFNO
		,MoveCount = GroupContact.MOVE_COUNT
		,LocationTypeID = GroupContact.LOTYP_REFNO
		,LocationDescription = GroupContact.LOCATION
		,LocationTypeHealthOrgID = GroupContact.LOTYP_HEORG_REFNO
		,ServicePointStaysID = GroupContact.SSTAY_REFNO
		,WaitingListID = GroupContact.WLIST_REFNO
		,PlannedAttendees = GroupContact.PLANNED_ATTENDEES
		,ActualAttendees = GroupContact.ACTUAL_ATTENDEES
		,ProviderSpellID = GroupContact.PRVSP_REFNO
		,RTTStatusID = GroupContact.RTTST_REFNO
		,EarliestReasinableOffertime = GroupContact.ERO_DTTM
		,EncounterInterventionsCount = 1 -- CountInterventions
		,Createdtime = GroupContact.CREATE_DTTM
		,Modifiedtime = GroupContact.MODIF_DTTM
		,CreatedByID = GroupContact.USER_CREATE
		,ModifiedByID = GroupContact.USER_MODIF
		,ArchiveFlag = GroupContact.ARCHV_FLAG
		,ParentID = GroupContact.PARNT_REFNO
		,HealthOrgOwnerID = GroupContact.OWNER_HEORG_REFNO
		,Comment = GroupContact.COMMENTS
		,ReferralReceivedTime = null -- Referral.RECVD_DTTM  --Received
		,ReferralClosedTime = null --Referral.CLOSR_DATE --referral closed
		,ReferralReasonID = null -- Referral.REASN_REFNO --Reason for referral
		,ReferredToSpecialtyID = null -- Referral.REFTO_SPECT_REFNO --referred to specialty
		,ReferredToStaffTeamID = null -- Referral.REFTO_STEAM_REFNO --referred to staff team
		,ReferredToProfCarerID = null -- Referral.REFTO_PROCA_REFNO --referred to professional carer
		,ReferralSourceID = null -- Referral.SORRF_REFNO -- source of referral
		,ReferralTypeID = null -- Referral.RETYP_REFNO --referral type
		,PatientEncounterRegisteredPracticeID = null -- ProfessionalCarerBase.HEORG_REFNO
		,DNAReasonID = GroupContact.DNARS_REFNO
		,CalledTime = GroupContact.CALLED_DTTM
	from
		(
		select
			NonPatientEventChild.SCHDL_REFNO 
			,NonPatientEventChild.PARNT_REFNO 
			,NonPatientEventChild.PROCA_REFNO
			,NonPatientEventChild.MODIF_DTTM
			,NonPatientEventParent.MOVE_COUNT
			,NonPatientEventChild.SCTYP_REFNO
			
			/* 
				Get team associated with clinician at time of encounter. 
				As non patient event we don't have the team on the record so have to derive
			*/
			
			,STEAM_REFNO = 				
				(
				select top 1 -- much quicker!
					 STEAM_REFNO
				from
					[$(ipm)].dbo.ProfessionalCarerStaffTeam
				where
					NonPatientEventChild.PROCA_REFNO = ProfessionalCarerStaffTeam.PROCA_REFNO 
				and NonPatientEventParent.START_DTTM between ProfessionalCarerStaffTeam.START_DTTM and coalesce(ProfessionalCarerStaffTeam.END_DTTM, getdate())
	
				order by
					ProfessionalCarerStaffTeam.START_DTTM desc
					,ProfessionalCarerStaffTeam.TISID desc
					
				)
				
			,SPECT_REFNO = 				
				(
				select top 1 -- much quicker!
					 SPECT_REFNO
				from
					[$(ipm)].dbo.ProfessionalCarerSpecialty
				where
					NonPatientEventChild.PROCA_REFNO = ProfessionalCarerSpecialty.PROCA_REFNO 
				and NonPatientEventParent.START_DTTM between ProfessionalCarerSpecialty.START_DTTM and coalesce(ProfessionalCarerSpecialty.END_DTTM, getdate())
				
				order by
					ProfessionalCarerSpecialty.START_DTTM desc
					,ProfessionalCarerSpecialty.TISID desc
				)	
					
			,NonPatientEventParent.START_DTTM	
			,NonPatientEventParent.END_DTTM	
			,NonPatientEventParent.VISIT_REFNO
			,NonPatientEventParent.CONTY_REFNO
			,NonPatientEventParent.REASN_REFNO
			,NonPatientEventParent.CANCR_REFNO
			,NonPatientEventParent.CANCR_DTTM
			,NonPatientEventParent.CANCB_REFNO
			,NonPatientEventParent.MOVRN_REFNO
			,NonPatientEventParent.MOVE_DTTM
			,NonPatientEventParent.SPONT_REFNO
			,NonPatientEventParent.SPSSN_REFNO
			--,PARENT_MOVE_COUNT = NonPatientEventParent.MOVE_COUNT
			,NonPatientEventParent.LOTYP_REFNO
			,NonPatientEventParent.LOCATION
			,NonPatientEventParent.LOTYP_HEORG_REFNO		
			,NonPatientEventParent.SSTAY_REFNO
			,NonPatientEventParent.WLIST_REFNO
			,NonPatientEventParent.PLANNED_ATTENDEES
			,NonPatientEventParent.ACTUAL_ATTENDEES
			,NonPatientEventParent.PRVSP_REFNO
			,NonPatientEventParent.RTTST_REFNO
			,NonPatientEventParent.ERO_DTTM
			,NonPatientEventChild.CREATE_DTTM
			--,PARENT_MODIF_DTTM = NonPatientEventParent.MODIF_DTTM
			,NonPatientEventChild.USER_CREATE
			,NonPatientEventChild.USER_MODIF
			,NonPatientEventParent.ARCHV_FLAG
			,NonPatientEventParent.OWNER_HEORG_REFNO
			,NonPatientEventParent.COMMENTS
			,NonPatientEventParent.DNARS_REFNO
			,NonPatientEventParent.CALLED_DTTM
			
			
		from
			(
			select
				NonPatientEvent.SCHDL_REFNO
				,NonPatientEvent.START_DTTM
				,NonPatientEvent.END_DTTM
				,NonPatientEvent.VISIT_REFNO --Type of appointment
				,NonPatientEvent.CONTY_REFNO --Contact Type
				,NonPatientEvent.REASN_REFNO
				,NonPatientEvent.CANCR_REFNO
				,NonPatientEvent.CANCR_DTTM
				,NonPatientEvent.CANCB_REFNO
				,NonPatientEvent.MOVRN_REFNO
				,NonPatientEvent.MOVE_DTTM
				,NonPatientEvent.SPONT_REFNO
				,NonPatientEvent.SPSSN_REFNO
				,NonPatientEvent.MOVE_COUNT
				,NonPatientEvent.LOTYP_REFNO
				,NonPatientEvent.LOCATION
				,NonPatientEvent.LOTYP_HEORG_REFNO
				,NonPatientEvent.SSTAY_REFNO
				,NonPatientEvent.WLIST_REFNO
				,NonPatientEvent.PLANNED_ATTENDEES
				,NonPatientEvent.ACTUAL_ATTENDEES
				,NonPatientEvent.PRVSP_REFNO
				,NonPatientEvent.RTTST_REFNO
				,NonPatientEvent.ERO_DTTM
				--,NonPatientEvent.CREATE_DTTM
				,NonPatientEvent.MODIF_DTTM
				--,NonPatientEvent.USER_CREATE
				--,NonPatientEvent.USER_MODIF
				,NonPatientEvent.ARCHV_FLAG
				,NonPatientEvent.OWNER_HEORG_REFNO
				,NonPatientEvent.COMMENTS
				,NonPatientEvent.DNARS_REFNO
				,NonPatientEvent.CALLED_DTTM
			from
				(
					select
						NPE_PROCA_REFNO
						,SCHDL_REFNO
						,START_DTTM
						,END_DTTM
						,VISIT_REFNO --Type of appointment
						,CONTY_REFNO --Contact Type
						,REASN_REFNO
						,CANCR_REFNO
						,CANCR_DTTM
						,CANCB_REFNO
						,MOVRN_REFNO
						,MOVE_DTTM
						,SPONT_REFNO
						,SPSSN_REFNO
						,MOVE_COUNT
						,LOTYP_REFNO
						,LOCATION
						,LOTYP_HEORG_REFNO
						,SSTAY_REFNO
						,WLIST_REFNO
						,PLANNED_ATTENDEES
						,ACTUAL_ATTENDEES
						,PRVSP_REFNO
						,RTTST_REFNO
						,ERO_DTTM
						,CREATE_DTTM
						,MODIF_DTTM
						,USER_CREATE
						,USER_MODIF
						,ARCHV_FLAG
						,OWNER_HEORG_REFNO
						,COMMENTS
						,DNARS_REFNO
						
						/*	
							NPE_PROCA_REFNO added to SCHEDULES_VE06_NON_PATIENT_EVENTS by CBS. 
							Represents first clinician associated with clinic. 
							Get specialty associated with clinician at time of encounter
						*/
									
						,SPECT_REFNO = 				
									(
									select top 1 -- much quicker!
										 SPECT_REFNO
									from
										[$(ipm)].dbo.ProfessionalCarerSpecialty
									where
										SCHEDULES_VE06_NON_PATIENT_EVENTS.NPE_PROCA_REFNO = ProfessionalCarerSpecialty.PROCA_REFNO 
									and SCHEDULES_VE06_NON_PATIENT_EVENTS.START_DTTM between ProfessionalCarerSpecialty.START_DTTM and coalesce(ProfessionalCarerSpecialty.END_DTTM, getdate())
									
									order by
										ProfessionalCarerSpecialty.START_DTTM desc
										,ProfessionalCarerSpecialty.TISID desc
									)
						,CALLED_DTTM	
					from
						[$(ipm)].dbo.SCHEDULES_VE06_NON_PATIENT_EVENTS
						
				) NonPatientEvent -- Contains derived SPECT_REFNO column so can perform join below				
			
			
			inner join COM.Service CMFTService
			on	CMFTService.SPECT_REFNO = NonPatientEvent.SPECT_REFNO
			and	TCSDestinationID = 2 -- only bring back records where the specialty related to the Prof Carer is defined as CMFT
			
			where
				(
					(
						COMMENTS like '%PFACE%'
					or 	COMMENTS like '%PTEL%'
					)
				and coalesce(ARCHV_FLAG, 'N') = 'N'
				)
				
			)	NonPatientEventParent -- once CMFT parent records identfied can link to children
			
			
			inner join [$(ipm)].dbo.SCHEDULES_VE06_NON_PATIENT_EVENTS_CARERS NonPatientEventChild
			on	NonPatientEventParent.SCHDL_REFNO = NonPatientEventChild.PARNT_REFNO		
			
			where
				NonPatientEventParent.MODIF_DTTM between @from and @to
			or	NonPatientEventChild.MODIF_DTTM between @from and @to
			and	coalesce(NonPatientEventChild.ARCHV_FLAG, 'N') = 'N'
			
		) GroupContact
		
		

	
	)



INSERT INTO dbo.TImportCOMEncounter
(
	 SourceEncounterID
	,SourceUniqueID
	,SpecialtyID
	,StaffTeamID
	,ProfessionalCarerID
	,SeenByProfessionalCarerID 
	,StartTime 
	,EndTime
	,ArrivedTime 
	,SeenTime 
	,DepartedTime  
	,AttendedID
	,OutcomeID 
	,ReferralID 
	,ProfessionalCarerEpisodeID
	,PatientSourceID 
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber 
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
	,PatientTitleID 
	,PatientForename 
	,PatientSurname 
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,PatientCurrentCommissionerCode
	,PatientRegisteredCommissionerCode
	,VisitID
	,ContactTypeID
	,ScheduleTypeID
	,ScheduleReasonID
	,CanceledReasonID
	,CanceledTime
	,CancelledByID
	,MoveReasonID
	,MoveTime
	,ServicePointID
	,ServicePointSessionID
	,MoveCount
	,LocationTypeID
	,LocationDescription
	,LocationTypeHealthOrgID
	,ServicePointStaysID
	,WaitingListID
	,PlannedAttendees
	,ActualAttendees
	,ProviderSpellID
	,RTTStatusID
	,EarliestReasinableOfferTime
	,EncounterInterventionsCount
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,ParentID
	,HealthOrgOwnerID
	,Comment
	,ReferralReceivedTime
	,ReferralClosedTime
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID

	,PatientEncounterRegisteredPracticeID
	,DNAReasonID
	,JointActivityFlag
	,CalledTime
) 


select
	SourceEncounterID 
	,SourceUniqueID
	,GroupContact.SpecialtyID 
	--,Specialty.Specialty
	,GroupContact.StaffTeamID
	--,StaffTeam.StaffTeam
	,GroupContact.ProfessionalCarerID 
	--,ProfessionalCarerName = ProfessionalCarer.Surname + ', ' + ProfessionalCarer.Forename 
	--,ProfessionalCarerCode
	,SeenByProfessionalCarerID
	,GroupContact.StartTime 
	,GroupContact.EndTime
	,ArrivedTime 
	,SeenTime  
	,DepartedTime 
	,AttendedID 
	,OutcomeID 
	,ReferralID 
	,ProfessionalCarerEpisodeID
	,PatientSourceID
	,PatientSourceSystemUniqueID 
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID 
	,PatientTitleID 
	,PatientForename 
	,PatientSurname 
	,PatientAddress1 
	,PatientAddress2
	,PatientAddress3 
	,PatientAddress4 
	,PatientPostcode 
	,PatientDateOfBirth 
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID 
	,PatientReligionID 
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode 
	,PatientEncounterRegisteredPracticeCode 
	,PatientCurrentRegisteredGPCode 
	,PatientEncounterRegisteredGPCode 
	,PatientCurrentCommissionerCode 
	,PatientRegisteredCommissionerCode
	,VisitID
	,ContactTypeID 
	,ScheduleTypeID
	,ScheduleReasonID 
	,CancelledReasonID 
	,CancelledTime 
	,CancelledByID
	,MoveReasonID  
	,Movetime 
	,ServicePointID  
	,ServicePointSessionID 
	,MoveCount
	,LocationTypeID
	,LocationDescription 
	,LocationTypeHealthOrgID 
	,ServicePointStaysID 
	,WaitingListID 
	,PlannedAttendees 
	,ActualAttendees 
	,ProviderSpellID
	,RTTStatusID
	,EarliestReasinableOffertime
	,EncounterInterventionsCount 
	,CreatedTime = Createdtime
	,ModifiedTime = Modifiedtime
	,CreatedByID 
	,ModifiedByID 
	,GroupContact.ArchiveFlag 
	,ParentID 
	,HealthOrgOwnerID
	,Comment 
	,ReferralReceivedTime
	,ReferralClosedTime 
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID 
	,ReferredToProfCarerID 
	,ReferralSourceID
	,ReferralTypeID 
	,PatientEncounterRegisteredPracticeID 
	,DNAReasonID
	,JointActivityFlag = 
				case
					when exists
								(
									select
										1
									from
										GroupContact GroupContactEarlier
									where
										GroupContactEarlier.ParentID = GroupContact.ParentID
									and
										GroupContactEarlier.SourceUniqueID < GroupContact.SourceUniqueID		
								)
					then 1
				end		
	,CalledTime
from
	GroupContact

select @RowsInserted = @RowsInserted + @@rowcount

drop table #PatientUpdated

SELECT @RowsDeleted = @@rowcount

SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + 
	', Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

EXEC WriteAuditLogEvent 'ExtractCommunityEncounter', @Stats, @StartTime

﻿
create proc dbo.ExtractMedisecOutpatient

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table dbo.TImportMedisecOutpatient

insert into dbo.TImportMedisecOutpatient

select [outpn]
      ,[outepno]
      ,[outappdate]
      ,[outapptime]
      ,[outdoctor]
      ,[outhosp]
      ,[outspec]
      ,[outdiary]
      ,[outstream]
      ,[outcons]
      ,[outappno]
      ,[outattno]
      ,[outattendmarker]
      ,[outoutcome]
      ,[outtype]
      ,[outcontract]
      ,[outapptcodes]
      ,[outproc1]
      ,[outproc2]
      ,[outproc3]
      ,[outproc4]
      ,[outepdireq]
      ,[outepclosed]
      ,[outpatcat]
      ,[outrefcode]
      ,[outreforg]
      ,[outrefsrce]
      ,[outpurchref]
      ,[outletdate]
      ,[outletcomment]
      ,[outletstatus]
      ,[outcank]
      ,[outcn]
      ,[outeps]
from [$(CMFT_MEDISEC)].dbo.OutPatient
where cast(outappdate as datetime) >= '01 april 2008'

select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


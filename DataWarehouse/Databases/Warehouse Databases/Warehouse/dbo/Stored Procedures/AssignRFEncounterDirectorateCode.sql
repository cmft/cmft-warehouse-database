﻿
create procedure [dbo].[AssignRFEncounterDirectorateCode] as


-- main mapping
update RF.Encounter
set
	Encounter.DirectorateCode = DivisionMap.DirectorateCode
from
	RF.Encounter

inner join WH.DivisionMap
on	DivisionMap.SiteCode = Encounter.SiteCode
and	DivisionMap.SpecialtyCode = Encounter.SpecialtyCode



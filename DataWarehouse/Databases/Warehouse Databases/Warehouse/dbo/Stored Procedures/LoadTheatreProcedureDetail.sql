﻿CREATE procedure [dbo].[LoadTheatreProcedureDetail]
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table Theatre.ProcedureDetail


insert into Theatre.ProcedureDetail
	(
	 SequenceNo
	,SourceUniqueID
	,SupervisingSurgeon1Code
	,SupervisingSurgeon2Code
	,SupervisingSurgeon3Code
	,SupervisingAnaesthetistNurseCode
	,SupervisingScoutNurseCode
	,SupervisingInstrumentNurseCode
	,SupervisingTechnician1Code
	,SupervisingTechnician2Code
	,ProcedureConfirmedFlag
	,CMBSItemCode
	,Unused2
	,SupervisingAnaesthetist2Code
	,Unused3
	,SupervisingAnaesthetist3Code
	,ProcedureStartTime
	,ProcedureEndTime
	,ProcedureDescription
	,WoundType1Code
	,WoundType2Code
	,WoundType3Code
	,ProcedureCode
	,OperationDetailSourceUniqueID
	,SupervisingAnaesthetist1Code
	,Unused4
	,Unused5
	,Unused6
	,LastUpdated
	,LogDetail
	,Surgeon1Code
	,Surgeon2Code
	,Surgeon3Code
	,Anaesthetist1Code
	,Anaesthetist2Code
	,Anaesthetist3Code
	,ScoutNurseCode
	,InstrumentNurseCode
	,AnaesthetistNurseCode
	,Technician1Code
	,Technician2Code
	,ChangeOverAnaesthetistNurseCode
	,ChangeOverScoutNurseCode
	,ChangeOverInstrumentNurseCode
	,InventoryRecordCreatedFlag
	,PrincipleDiagnosisFlag
	,WoundType4Code
	,PrimaryProcedureFlag
	,WoundType5Code
	,WoundType6Code
	,PANCode
	,PADCode
	,RecordTimesCode
	,RecoverySourceUniqueID
	,ProcedureItemSourceUniqueID
)
select
	 SequenceNo = OI_SEQN_FILLER
	,SourceUniqueID = OI_SEQU
	,SupervisingSurgeon1Code = OI_SURG1_SSU_SEQU
	,SupervisingSurgeon2Code = OI_SURG2_SSU_SEQU
	,SupervisingSurgeon3Code = OI_SURG3_SSU_SEQU
	,SupervisingAnaesthetistNurseCode = OI_ANAES_NUR_SSU_SEQU
	,SupervisingScoutNurseCode = OI_SCOUT_NUR_SSU_SEQU
	,SupervisingInstrumentNurseCode = OI_INS_NUR_SSU_SEQU
	,SupervisingTechnician1Code = OI_TECH1_SSU_SEQU
	,SupervisingTechnician2Code = OI_TECH2_SSU_SEQU
	,ProcedureConfirmedFlag = OI_CONFIRMED
	,CMBSItemCode = OI_MBSNO
	,Unused2 = OI_UNUSED_2
	,SupervisingAnaesthetist2Code = OI_ANAES2_SSU_SEQU
	,Unused3 = OI_UNUSED_3
	,SupervisingAnaesthetist3Code = OI_ANAES3_SSU_SEQU
	,ProcedureStartTime = OI_START
	,ProcedureEndTime = OI_FINISH
	,ProcedureDescription = OI_DESC
	,WoundType1Code = OI_WOUNDTYPE1
	,WoundType2Code = OI_WOUNDTYPE2
	,WoundType3Code = OI_WOUNDTYPE3
	,ProcedureCode = OI_CD_SEQU
	,OperationDetailSourceUniqueID = OI_OP_SEQU
	,SupervisingAnaesthetist1Code = OI_ANAES1_SSU_SEQU
	,Unused4 = OI_UNUSED_4
	,Unused5 = OI_UNUSED_5
	,Unused6 = OI_UNUSED_6
	,LastUpdated = OI_LOG_DATE
	,LogDetail = OI_LOG_DETAILS_FILLER
	,Surgeon1Code = OI_SURG1_SEQU
	,Surgeon2Code = OI_SURG2_SEQU
	,Surgeon3Code = OI_SURG3_SEQU
	,Anaesthetist1Code = OI_ANAES1_SEQU
	,Anaesthetist2Code = OI_ANAES2_SEQU
	,Anaesthetist3Code = OI_ANAES3_SEQU
	,ScoutNurseCode = OI_SCOUT_NU_SEQU
	,InstrumentNurseCode = OI_INST_NU_SEQU
	,AnaesthetistNurseCode = OI_ANAES_NU_SEQU
	,Technician1Code = OI_TECH1_SEQU
	,Technician2Code = OI_TECH2_SEQU
	,ChangeOverAnaesthetistNurseCode = OI_CO_ANAES_SEQU
	,ChangeOverScoutNurseCode = OI_CO_SCOUT_SEQU
	,ChangeOverInstrumentNurseCode = OI_CO_INST_SEQU
	,InventoryRecordCreatedFlag = OI_INV
	,PrincipleDiagnosisFlag = OI_PRINCIPLE_DIAGNOSIS
	,WoundType4Code = OI_WOUNDTYPE4
	,PrimaryProcedureFlag = OI_PRIMARY_CD
	,WoundType5Code = OI_WOUNDTYPE5
	,WoundType6Code = OI_WOUNDTYPE6
	,PANCode = OI_PAN_SEQU
	,PADCode = OI_PAD_SEQU
	,RecordTimesCode = OI_RECORD_TIMES
	,RecoverySourceUniqueID = OI_REC_SEQU
	,ProcedureItemSourceUniqueID = OI_PI_SEQU
from
	[$(otprd)].dbo.FOITEM ProcedureDetail


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatreProcedureDetail', @Stats, @StartTime

print @Stats

﻿CREATE procedure [dbo].[ExtractCOMDiagnosisProcedureEvent]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int

	,@lastmodifiedTime datetime =
	(
	select
		coalesce(
			(
			select
				MAX(
					CONVERT(
						 datetime
						,ModifiedTime
					)
				)
			from
				COM.DiagnosisProcedureEvent
			)
			,'01/01/1900'
		)
	)

truncate table dbo.TImportCOMDiagnosisProcedureEvent

insert
into
	dbo.TImportCOMDiagnosisProcedureEvent
(
	 SourceUniqueID
	,DiagnosisProcedureClassificationID
	,SourcePatientNo
	,MedicalProblemLevelID
	,DiagnosisProcedureSubTypeID
	,DiagnosisProcedureID
	,NHSIdentifier
	,Comment
	,DiagnosisProcedure
	,EncounterSourceUniqueID
	,SourceEntityCode
	,DiagnosisProcedureTime
	,DiagnosisProcedureTypeCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,DiagnosisProcedureCode
	,DiagnosisProcedureInternalCode
	,SupplementaryCode
	,SupplementaryInternalCode
	,ArchiveFlag
	,StartTime
	,EndTime
	,UpdateTime
	,StaffTeamID
	,ProfessionalCarerID
	,Duration
	,ConfidentialFlag
	,Dosage
	,DosageUnitID
	,LocationOnBodyID
	,DosageFrequency
	,FrequencyDosageUnitID
	,CancelledTime
	,RulesID
	,Value
	,ProblemFlag
	,LateralityID
	,SourceEpisodeID
	,PeriodAdministeredID
	,ReasonAdministeredID
	,AnaestheticCategoryID
	,CauseOfDeathFlag
	,EventSequenceNo
	,CodingAuthorisationFlag
	,LatchedCodingAuthorisationFlag
	,ActionToTake
	,AlertSeverityID
	,ContractedProcedureFlag
	,SpecialtyID
	,HostitalServiceFlag
	,DiagnosisProcedureEndTime
	,ProcedureLocationID
	,TheatreBodyRegionID
	,IndividualCodingStatusID
	,AnaestheticTypeID
	,CodingOnsetTypeID
	,VisitPurposeID
	,HealthOrganisationOwnerID
)
select
	 --TISID
	--,[FILE_NAME]
	--,DATA_START_DATE
	--,DATA_END_DATE
	--,TOTAL_ROW_COUNT
	--,ROW_IDENTIFIER
	 SourceUniqueID = DGPRO_REFNO
	,DiagnosisProcedureClassificationID = DPCLA_REFNO
	,SourcePatientNo = PATNT_REFNO
	,MedicalProblemLevelID = MPLEV_REFNO
	,DiagnosisProcedureSubTypeID = CPTYP_REFNO
	,DiagnosisProcedureID = ODPCD_REFNO
	,NHSIdentifier = LINK_DGPRO_REFNO
	,Comment = COMMENTS
	,DiagnosisProcedure = [DESCRIPTION]
	,EncounterSourceUniqueID = SORCE_REFNO
	,SourceEntityCode = SORCE_CODE
	,DiagnosisProcedureTime = DGPRO_DTTM
	,DiagnosisProcedureTypeCode = DPTYP_CODE
	,CreatedTime = CREATE_DTTM
	,ModifiedTime = MODIF_DTTM
	,CreatedByID = USER_CREATE
	,ModifiedByID = USER_MODIF
	,DiagnosisProcedureCode = CODE
	,DiagnosisProcedureInternalCode = CCSXT_CODE
	,SupplementaryCode = SUPL_CODE
	,SupplementaryInternalCode = SUPL_CCSXT_CODE
	,ArchiveFlag = ARCHV_FLAG
	--,STRAN_REFNO
	--,PRIOR_POINTER
	--,EXTERNAL_KEY
	,StartTime = START_DTTM
	,EndTime = END_DTTM
	,UpdateTime = UPDATE_DTTM
	,StaffTeamID = STEAM_REFNO
	,ProfessionalCarerID = PROCA_REFNO
	,Duration = DURATION
	,ConfidentialFlag = CONFIDENTIAL
	,Dosage = DOSAGE
	,DosageUnitID = DOUNT_REFNO
	,LocationOnBodyID = DPLOC_REFNO
	,DosageFrequency = FREQUENCY
	,FrequencyDosageUnitID = FRUNT_REFNO
	,CancelledTime = CANCEL_DTTM
	--,PARNT_REFNO
	,RulesID = RULES_REFNO
	,Value = VALUE
	,ProblemFlag = PROBLEM
	,LateralityID = LATRL_REFNO
	,SourceEpisodeID = PRCAE_REFNO
	,PeriodAdministeredID = PERAD_REFNO
	,ReasonAdministeredID = RSADM_REFNO
	,AnaestheticCategoryID = ANALC_REFNO
	,CauseOfDeathFlag = CAUSE_OF_DEATH
	,EventSequenceNo = SORT_ORDER
	,CodingAuthorisationFlag = COD_AUTH_FLAG
	,LatchedCodingAuthorisationFlag = LATCH_AUTH_FLAG
	,ActionToTake = ACTION
	,AlertSeverityID = ALSEV_REFNO
	,ContractedProcedureFlag = CONTRACTED_PROCEDURE
	,SpecialtyID = SPECT_REFNO
	,HostitalServiceFlag = HOSP_SERV_FLAG
	--,MIN_PRICE
	--,MAX_PRICE
	,DiagnosisProcedureEndTime = DGPRO_END_DTTM
	--,BILL_ODPCD_REFNO
	,ProcedureLocationID = PLCTN_REFNO
	,TheatreBodyRegionID = THEBR_REFNO
	,IndividualCodingStatusID = ICDST_REFNO
	--,SYN_CODE
	,AnaestheticTypeID = ANTYP_REFNO
	,CodingOnsetTypeID = CODON_REFNO
	,VisitPurposeID = VISTP_REFNO
	,HealthOrganisationOwnerID = OWNER_HEORG_REFNO
	--,TIS_FLAG
	--,load_datetime
from
	[$(ipm)].dbo.DIAGNOSIS_PROCEDURES_V03
where
	DIAGNOSIS_PROCEDURES_V03.SORCE_CODE NOT IN ('WLIST', 'REFRL','PATNT')
and	DIAGNOSIS_PROCEDURES_V03.MODIF_DTTM > @lastmodifiedTime
and	not exists
	(
	select
		1
	from
		[$(ipm)].dbo.DIAGNOSIS_PROCEDURES_V03 Previous
	where
		Previous.SORCE_CODE NOT IN ('WLIST', 'REFRL','PATNT')
	and	Previous.MODIF_DTTM > @lastmodifiedTime
	and	Previous.DGPRO_REFNO = DIAGNOSIS_PROCEDURES_V03.DGPRO_REFNO
	and	Previous.MODIF_DTTM > DIAGNOSIS_PROCEDURES_V03.MODIF_DTTM
	)

select
	 @inserted = @@ROWCOUNT
	,@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Records Inserted: ' + cast(@inserted as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


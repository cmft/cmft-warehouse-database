﻿CREATE procedure [dbo].[ExtractSymphonyAEDiagnosis] 
	 @from smalldatetime
	,@to smalldatetime
	,@debug bit = 0
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @localfrom smalldatetime
declare @localto smalldatetime

select @StartTime = getdate()


--select
--	 @localfrom = @from
--	,@localto = dateadd(second, 24 * 60 * 60 -1, @to)

--ACCIDENT AND EMERGENCY DIAGNOSIS is a six character code, comprising:
--
--Diagnosis Condition n2 
--Sub-Analysis n1 
--Accident And Emergency Attendance - ANATOMICAL AREA  n2 
--Accident And Emergency Attendance - ANATOMICAL SIDE  an1 

--Diagnosis Condition is in res_result
--Sub-Analysis is not held in Symphony so ignore
--Accident And Emergency Attendance - ANATOMICAL AREA  is in res_field1
--Accident And Emergency Attendance - ANATOMICAL SIDE  is in res_field2


insert into dbo.TImportAEDiagnosis
(
	 SourceUniqueID
	,SequenceNo
	,DiagnosisCode
	,AESourceUniqueID
	,SourceDiagnosisCode
	,SourceDiagnosisSiteCode
	,SourceDiagnosisSideCode
	,DiagnosisSiteCode
	,DiagnosisSideCode
)
select
	 SourceUniqueID = Result.res_resid

	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY Result.res_atdid ORDER BY Result.res_resid)

	,DiagnosisCode = left(Diagnosis.not_text, 6)
	,AESourceUniqueID = Result.res_atdid
	,SourceDiagnosisCode = Result.res_result
	,SourceDiagnosisSiteCode = Result.res_field1
	,SourceDiagnosisSideCode = Result.res_field2
	,DiagnosisSiteCode = NoteDiagnosisSite.not_text
	,DiagnosisSideCode = NoteDiagnosisSide.not_text
from
	[$(Symphony)].dbo.Result_details Result

inner join [$(Symphony)].dbo.LookupMappings Mapping
on	Mapping.flm_lkpid = Result.res_result

inner join [$(Symphony)].dbo.Notes Diagnosis
on	Diagnosis.not_noteid = Mapping.flm_value

left join [$(Symphony)].dbo.LookupMappings MappingDiagnosisSite
on	MappingDiagnosisSite.flm_lkpid = Result.res_field1
and	MappingDiagnosisSite.flm_mtid =
	(
	select
		MappingTypes.mt_id
	from
		[$(Symphony)].dbo.MappingTypes 
	where
		MappingTypes.mt_name = 'CDS'
	)
and	MappingDiagnosisSite.flm_lkpid <> 0

left join [$(Symphony)].dbo.Notes NoteDiagnosisSite
on	NoteDiagnosisSite.not_noteid = MappingDiagnosisSite.flm_value

left join [$(Symphony)].dbo.LookupMappings MappingDiagnosisSide
on	MappingDiagnosisSide.flm_lkpid = Result.res_field2
and	MappingDiagnosisSide.flm_mtid =
	(
	select
		MappingTypes.mt_id
	from
		[$(Symphony)].dbo.MappingTypes 
	where
		MappingTypes.mt_name = 'CDS'
	)
and	MappingDiagnosisSide.flm_lkpid <> 0

left join [$(Symphony)].dbo.Notes NoteDiagnosisSide
on	NoteDiagnosisSide.not_noteid = MappingDiagnosisSide.flm_value

inner join [$(Symphony)].dbo.Attendance_Details Encounter
on	Encounter.atd_id = Result.res_atdid

inner join [$(Symphony)].dbo.Episodes Episode
on	Episode.epd_id = Encounter.atd_epdid

inner join 
	(
	select
		 Department.dpt_id
		,CommissioningSerialNo = '5NT00A'

		,PCTCode = '5NT'

		,SiteCode =
		case
		when Department.dpt_name = 'MRI ED' then 'RW3MR'
		when Department.dpt_name = 'PED' then 'RW3RC'
		when Department.dpt_name = 'EEC' then 'RW3RE'
		when Department.dpt_name = 'St Mary''s' then 'RW3SM'
		when Department.dpt_name = 'Dental' then 'RW3DH'
		end
	from
		[$(Symphony)].dbo.CFG_Dept Department
	where
		Department.dpt_name in (
			 'MRI ED'
			,'PED'
			,'EEC'
			,'St Mary''s'
			,'Dental'
			)
	) Department
on	Department.dpt_id = Episode.epd_deptid

where
--	Encounter.atd_arrivaldate between @localfrom and @localto
	Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
and	Encounter.atd_deleted = 0
and	Episode.epd_deleted = 0
and	Result.res_inactive <> 1

--and	Result.res_depid in
--		(
--		select
--			rps_value
--		from
--			[$(Symphony)].dbo.cfg_reportsettings
--		where
--			rps_name like 'Diagnosis'
--		)

-- incorporate alternative DEP in SMH
and	Result.res_depid in
		(
		select
			dep_id
		from
			[$(Symphony)].dbo.CFG_DEProcedures
		where
			CFG_DEProcedures.dep_Caption like 'Diagnosis'
		)

-- only the attendances from after the switchover to Symphony
and (
		(
			Department.SiteCode = 'RW3SM'
		and	Encounter.atd_arrivaldate >= '11 Apr 2011 11:42'
		)
	or	(
			Department.SiteCode = 'RW3RC'
		and	Encounter.atd_arrivaldate >= '20 Oct 2009 09:10'
		)
	or	(
			Department.SiteCode = 'RW3MR'
		and	Encounter.atd_arrivaldate >= '20 Oct 2009 10:56'
		)

	or	(
			Department.SiteCode = 'RW3RE'
		and	Encounter.atd_arrivaldate >= '11 Sep 2010 00:00'
		)
	or	(
			Department.SiteCode = 'RW3DH'
		and	Encounter.atd_arrivaldate >= '11 Oct 2011 00:00'
		)
	)


select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractSymphonyAEDiagnosis', @Stats, @StartTime

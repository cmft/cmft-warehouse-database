﻿create procedure ReportCasenoteMergeFutureEncounter
(
	 @FromDate date
	,@ToDate date
)
as

select
	 Encounter.EncounterTypeCode
	,Encounter.SourcePatientNo
	,Encounter.EncounterDate
	,Encounter.EncounterTime
	,Encounter.DistrictNo
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Casenote.CasenoteNumber
	,Casenote.AllocatedDate
	,Casenote.CasenoteLocationCode
	,CasenoteLocation.CasenoteLocation
	,Casenote.CasenoteLocationDate
	,Casenote.CasenoteStatus
	--,Casenote.WithdrawnDate
	,Casenote.Comment
from
	(
	select
		 EncounterTypeCode = 'OPA'
		,Encounter.SourcePatientNo
		,EncounterDate = Encounter.AppointmentDate
		,EncounterTime = Encounter.AppointmentTime
		,Encounter.DistrictNo
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
	from
		OP.Encounter

	left join PAS.Clinic
	on	Clinic.ClinicCode = Encounter.ClinicCode

	where
		Encounter.AppointmentDate between @FromDate and @ToDate
	and	coalesce(DNACode, '9') not in ('1', '2', '3', '4', '5', '6', '7')

	union

	select
		 EncounterTypeCode = 'TCI'
		,Encounter.SourcePatientNo
		,EncounterDate = cast(Encounter.TCIDate as date)
		,EncounterTime = Encounter.TCIDate
		,Encounter.DistrictNo
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.DateOfBirth
	from
		APC.WaitingList Encounter
	where
		Encounter.CensusDate = (select max(CensusDate) from APC.WaitingList)
	and cast(Encounter.TCIDate as date) between @FromDate and @ToDate

	) Encounter

inner join
	(
	select
		SourcePatientNo
	from
		PAS.Casenote
	where
		coalesce(Casenote.CasenoteStatus , '') != 'WITHDRAWN'		
	group by
		SourcePatientNo
	having
		count(*) > 1
	) MultipleCasenote
on	Encounter.SourcePatientNo = MultipleCasenote.SourcePatientNo

left join PAS.Casenote
on	Casenote.SourcePatientNo = Encounter.SourcePatientNo

left join PAS.CasenoteLocation
on	CasenoteLocation.CasenoteLocationCode = Casenote.CasenoteLocationCode

where
	coalesce(Casenote.CasenoteStatus , '') != 'WITHDRAWN'	

order by
	 Encounter.EncounterTime
	,PatientSurname
	,PatientForename


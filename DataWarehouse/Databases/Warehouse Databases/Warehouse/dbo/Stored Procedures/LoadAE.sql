﻿


CREATE       procedure [dbo].[LoadAE] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from dbo.Parameter where Parameter = 'AEFREEZEDATE')
		,dateadd(month, -3, @to)
	)

EXEC ExtractSymphonyReferenceData

truncate table TImportAEEncounter
exec ExtractSymphonyAEEncounter @from, @to
exec LoadAEEncounter

truncate table TImportAEDiagnosis
exec ExtractSymphonyAEDiagnosis @from, @to
exec LoadAEDiagnosis @from, @to

truncate table TImportAEProcedure
exec ExtractSymphonyAEProcedure @from, @to
exec LoadAEProcedure @from, @to

truncate table TImportAEInvestigation
exec ExtractSymphonyAEInvestigation @from, @to
exec LoadAEInvestigation @from, @to

truncate table TImportAEAssault
exec ExtractSymphonyAEAssault
exec LoadAEAssault

truncate table TImportAEDischargeDrug
exec ExtractSymphonyAEDischargeDrug @from, @to
exec LoadAEDischargeDrug




--exec ExtractInfocomAE
--exec LoadPASAEEncounter

--exec LoadPCECAEEncounter
exec ETL.ExtractAdastraAEEncounterPCEC
exec ETL.LoadAEEncounterPCECCen -- 20141229 RR split the PCEC into Cen and Tra
exec ETL.LoadAEEncounterPCECTra


exec dbo.AssignAEReportableExcludePCEC @from, @to

--observation

exec ETL.BuildAEEncounterObservationSet


update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADAEDATE'

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADAEDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadAE', @Stats, @StartTime




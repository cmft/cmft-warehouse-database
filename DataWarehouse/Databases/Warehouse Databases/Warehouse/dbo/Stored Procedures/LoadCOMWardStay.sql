﻿
CREATE PROCEDURE [dbo].[LoadCOMWardStay] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

TRUNCATE TABLE  COM.WardStay

-- Insert new rows into encounter destination table
INSERT INTO COM.WardStay
(
	 SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,AdmissionDate 
	,DischargeDate
	,StartTime
	,EndTime
	,StartDate
	,EndDate
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID 
	,SourceAdmissionMethodID 
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID 
	,ExpectedDischargeDate 
	,SourceDischargeDestinationID 
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode

	,RegisteredPracticeID
	,Created
	,Updated
	,ByWhom
)

SELECT
	 SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,AdmissionDate 
	,DischargeDate
	,StartTime
	,EndTime
	,StartDate
	,EndDate
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID 
	,SourceAdmissionMethodID 
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID 
	,ExpectedDischargeDate 
	,SourceDischargeDestinationID 
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode

	,RegisteredPracticeID
	,Created = GETDATE()
	,Updated = GETDATE()
	,ByWhom = SUSER_NAME()


FROM
	dbo.TLoadCOMWardStay
	
select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

exec WriteAuditLogEvent 'LoadCOMWardStay', @Stats, @StartTime

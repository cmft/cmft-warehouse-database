﻿
create proc dbo.LoadMedisec

as

exec dbo.ExtractMedisecDocumentContent;
exec dbo.LoadMedisecDocumentContent;

exec dbo.ExtractMedisecDocumentSummary;
exec dbo.LoadMedisecDocumentSummary;

exec dbo.ExtractMedisecInpatientRequest;
exec dbo.LoadMedisecInpatientRequest;

exec dbo.ExtractMedisecConsultant;
exec dbo.LoadMedisecConsultant;

exec dbo.LoadManualMedisecSubmission;

exec dbo.ExtractMedisecUserMaster;
exec dbo.LoadMedisecUserMaster;

exec dbo.ExtractMedisecOutpatient;
exec dbo.LoadMedisecOutpatient;

exec dbo.LoadMedisecEncounter


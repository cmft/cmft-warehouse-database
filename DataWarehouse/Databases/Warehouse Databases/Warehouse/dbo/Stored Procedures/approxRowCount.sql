﻿create procedure dbo.approxRowCount(@db sysname, @schema sysname, @table sysname, @rowcount int output)
as begin

	declare @cnt table (cnt int);
	declare @ret int;

	declare @sql varchar(255) = 'use ' + @db + ';
	declare @cnt int; 
	select @cnt = sum(rows) 
	from sys.partitions 
	where index_id IN (0, 1) 
	  and object_id = OBJECT_ID(''' + @db + '.' + @schema + '.' + @table + '''); 
	select @cnt;';

	insert @cnt 
	exec (@sql)

	select top 1 @rowcount = cnt from @cnt
	  
end;

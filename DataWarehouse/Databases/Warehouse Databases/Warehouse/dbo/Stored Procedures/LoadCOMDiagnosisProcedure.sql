﻿CREATE procedure [dbo].[LoadCOMDiagnosisProcedure] as

truncate table COM.DiagnosisProcedure

insert
into
	COM.DiagnosisProcedure
(
	 DiagnosisProcedureID
	,DiagnosisProcedureCode
	,DiagnosisProcedureTypeCode
	,DiagnosisProcedure
	,ArchiveFlag
)

select
	 DiagnosisProcedureID = ODPCD_CODES_V01.ODPCD_REFNO
	,DiagnosisProcedureCode = ODPCD_CODES_V01.CODE
	,DiagnosisProcedureTypeCode = ODPCD_CODES_V01.CCSXT_CODE
	,DiagnosisProcedure = ODPCD_CODES_V01.DESCRIPTION
	,ArchiveFlag = ODPCD_CODES_V01.ARCHV_FLAG
from
	[$(ipm)].dbo.ODPCD_CODES_V01
where
	not exists
	(
	select
		1
	from
		[$(ipm)].dbo.ODPCD_CODES_V01 Previous
	where
		Previous.ODPCD_REFNO = ODPCD_CODES_V01.ODPCD_REFNO
	and	Previous.MODIF_DTTM > ODPCD_CODES_V01.MODIF_DTTM
	)

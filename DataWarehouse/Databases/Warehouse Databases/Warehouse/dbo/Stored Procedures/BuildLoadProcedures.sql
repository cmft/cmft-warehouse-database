﻿CREATE procedure [dbo].[BuildLoadProcedures]
	 @schema varchar(50) = 'CMIS'
	,@linkedServer varchar(50) = 'CMIS'
	,@linkedServerSchema varchar(50) = 'SMMIS'
as

BEGIN

	/**************************************************************************/
	/* General Declarations                                                   */
	/**************************************************************************/
	declare @FetchStatus int
	declare @sqlSelectColumns varchar(8000)
	declare @sqlInsertColumns varchar(8000)
	declare @sqlInsert varchar(8000)
	declare @sqlSelect varchar(8000)
	declare @sqlOpenquery varchar(8000)
	declare @sqlpart1 varchar(8000)
	declare @sqlpart2 varchar(8000)
	declare @crlf char(2)
	declare @tab char(1)

	declare @ProcedureName varchar(255)
	declare @TableName varchar(255)
	declare @ExtractFilePrefix varchar(255)

	declare @ColumnName varchar(255)
	declare @DataType varchar(255)
	declare @ColumnOrder int
	declare @sourceTable varchar(255)
	declare @columnOffset int


	/**************************************************************************/
	/* Declaration for trigger cursor                                         */
	/**************************************************************************/

	declare TableCursor CURSOR fast_forward FOR
		select
			ProcedureName =
				'Load' + Extract.TABLE_NAME

			 ,TableName = Extract.TABLE_NAME
		from
			INFORMATION_SCHEMA.TABLES Extract
		where
			Extract.TABLE_SCHEMA = @schema

	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	select
		 @sqlSelectColumns = ''
		,@sqlInsertColumns = ''
		,@crlf = char(13) + char(10)
		,@tab = char(9)


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	OPEN TableCursor

	FETCH NEXT FROM TableCursor INTO
		 @ProcedureName
		,@TableName

	Select @FetchStatus = @@FETCH_STATUS


    /**************************************************************************/
    /* Main Processing Loop                                                   */
    /**************************************************************************/
    WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

	begin

		declare ColumnCursor CURSOR fast_forward FOR
		select
			ColumnName = Attribute.COLUMN_NAME
			,DataType = Attribute.DATA_TYPE
		from
			INFORMATION_SCHEMA.COLUMNS Attribute
		where
			Attribute.TABLE_SCHEMA = @schema
		and	Attribute.TABLE_NAME = @TableName
		
		order by
			 Attribute.TABLE_NAME
			,Attribute.ORDINAL_POSITION

		OPEN ColumnCursor

		FETCH NEXT FROM ColumnCursor INTO
			  @ColumnName
			 ,@DataType

		Select @FetchStatus = @@FETCH_STATUS

		/**************************************************************************/
		/* Column Processing Loop                                                 */
		/**************************************************************************/
		WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

		begin
		if @ColumnName <> 'UniqueRecno' --system column
			select
				@sqlSelectColumns =
					@sqlSelectColumns + 
					case
					when @DataType like '%datetime'
					then ',to_date(' + @ColumnName + ') as ' + @ColumnName
					else ',' + @ColumnName
					end + @crlf + @tab

				,@sqlInsertColumns =
					@sqlInsertColumns + 
					',[' + @ColumnName + ']' + @crlf + @tab


			FETCH NEXT FROM ColumnCursor INTO
			  @ColumnName
			 ,@DataType

			Select @FetchStatus = @@FETCH_STATUS

		end
		
		select
			 @sqlSelectColumns = @tab + ' ' + right(@sqlSelectColumns, datalength(@sqlSelectColumns) - 1)
			,@sqlInsertColumns = @tab + ' ' + right(@sqlInsertColumns, datalength(@sqlInsertColumns) - 1)
		
		select
			 @sqlSelectColumns = left(@sqlSelectColumns, datalength(@sqlSelectColumns) - 1)
			,@sqlInsertColumns = left(@sqlInsertColumns, datalength(@sqlInsertColumns) - 1)

		select
			 @sqlpart1 = 
				case
				when
					exists
						(
						select
							1
						from
							INFORMATION_SCHEMA.ROUTINES
						where
							SPECIFIC_SCHEMA + '.' + SPECIFIC_NAME = @schema + '.' + @ProcedureName
						)
				then 'alter'
				else 'create'
				end +
				' procedure ' + 
				@schema + '.[' +
				@ProcedureName + '] as' + @crlf + @crlf +
				
				
				'declare @StartTime datetime' + @crlf + 
				'declare @Elapsed int' + @crlf + 
				'declare @RowsInserted Int' + @crlf + 
				'declare @Stats varchar(255)' + @crlf + 
				'declare @from datetime' + @crlf + @crlf + 

				'select @StartTime = getdate()' + @crlf + @crlf + 

				'truncate table ' + @schema + '.[' + @TableName + ']' + @crlf + @crlf
				
			,@sqlInsert = 
				'insert into ' + @crlf + @tab + @schema + '.[' + @TableName + ']' + @crlf + 
				'(' + @crlf +
				+ @sqlInsertColumns + ')' + @crlf
				
			,@sqlSelect =
				'select' + @crlf + 
				@sqlInsertColumns + 
--				@tab + '*' + @crlf +
				'from'
			
			,@sqlOpenquery =
				@crlf + @tab +
				'openquery(' + @linkedServer + ', ''' + @crlf +
				'select' + @crlf + 
				@sqlSelectColumns +
				'from' + @crlf + @tab + @linkedServerSchema + '.' + @TableName + @crlf +
				''')' + @crlf + @crlf

			,@sqlpart2 =
				'select @RowsInserted = @@rowcount' + @crlf + @crlf +

				'select @Elapsed = DATEDIFF(minute, @StartTime, getdate())' + @crlf + @crlf +

				'select @Stats = ' + @crlf +
				'	''Rows extracted '' + CONVERT(varchar(10), @RowsInserted) + '', Time Elapsed '' + ' + @crlf +
				'	CONVERT(varchar(6), @Elapsed) + '' Mins''' + @crlf + @crlf +

				'exec WriteAuditLogEvent ''' + @schema + '.' + @ProcedureName + ' + '', @Stats, @StartTime' + @crlf +


				@crlf + 'GO' + @crlf + @crlf

--		exec (@sqlpart1 + @sqlpart2)
		print @sqlpart1
		print @sqlInsert
		print @sqlSelect
		print @sqlOpenquery
		print @sqlpart2

		CLOSE ColumnCursor
		DEALLOCATE ColumnCursor

		select
			 @sqlSelectColumns = ''
			,@sqlInsertColumns = ''

		FETCH NEXT FROM TableCursor INTO
			 @ProcedureName
			,@TableName

		Select @FetchStatus = @@FETCH_STATUS

    END

	/**************************************************************************/
	/* Termination                                                            */
	/**************************************************************************/
	CLOSE TableCursor
	DEALLOCATE TableCursor

END

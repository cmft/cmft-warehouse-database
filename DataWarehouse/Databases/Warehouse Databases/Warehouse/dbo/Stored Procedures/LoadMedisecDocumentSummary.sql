﻿
create proc dbo.LoadMedisecDocumentSummary

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table Medisec.DocumentSummary

insert into Medisec.DocumentSummary
(
	   [DocumentName]
      ,[PatientNumber] 
      ,[PatientName]
      ,[DocumentTypeCode]
      ,[AuthorCode]
      ,[Description]
      ,[TypedTime]
      ,[ConsultantCode]
      ,[DictatedTime]
      ,[Reference]
      ,[HospitalNumber]
      ,[Head]
      ,[Farm]
      ,[IssuedTime]
      ,[IssuedByCode]
      ,[SignedTime]
      ,[SignedByCode]
      ,[WardCode]
      ,[DictatedByCode]
      ,[Group]
      ,[DocumentProgressCode]
      ,[GPCode]
      ,[GPPracticeCode]
      ,[Inbox]
      ,[Department]
      ,[SourceUniqueID]
      ,[GPPracticeCodeStatusCode]
      ,[GPPracticeCodeViewedByCode]
      ,[GPPracticeCodeViewedTime]
      ,[SpecialtyCode]
      ,[EpisodeSecurityFlag]
      ,[ZipID]
)

select 
	   [DocumentName]
      ,[PatientNumber]
      ,[PatientName]
      ,[DocumentTypeCode]
      ,[AuthorCode]
      ,[Description]
      ,[TypedTime]
      ,[ConsultantCode]
      ,[DictatedTime]
      ,[Reference]
      ,[HospitalNumber]
      ,[Head]
      ,[Farm]
      ,[IssuedTime]
      ,[IssuedByCode]
      ,[SignedTime]
      ,[SignedByCode]
      ,[WardCode]
      ,[DictatedByCode]
      ,[Group]
      ,[DocumentProgressCode]
      ,[GPCode]
      ,[GPPracticeCode]
      ,[Inbox]
      ,[Department]
      ,[SourceUniqueID]
      ,[GPPracticeCodeStatusCode]
      ,[GPPracticeCodeViewedByCode]
      ,[GPPracticeCodeViewedTime]
      ,[SpecialtyCode]
      ,[EpisodeSecurityFlag]
      ,[ZipID]
from dbo.TLoadMedisecDocumentSummary

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


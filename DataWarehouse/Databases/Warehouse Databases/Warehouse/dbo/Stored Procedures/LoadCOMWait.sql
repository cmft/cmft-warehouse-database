﻿
CREATE procedure [dbo].[LoadCOMWait] as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

declare @CensusDate smalldatetime

select @CensusDate = dateadd(day, datediff(day, 0, getdate()), -1) -- adjustment for extract running on the following day


exec dbo.ExtractCOMReferralWait @CensusDate

exec dbo.LoadCOMWaitingList

exec dbo.BuildCOMWaitPTLBase @CensusDate



-- reset snapshot table
delete from COM.Snapshot

insert into COM.Snapshot 
	(
	CensusDate
	)
select distinct 
	CensusDate
from 
	COM.Wait

-- insert the last date loaded - this is used by the load job to determine whether or not to run the load process
delete from Utility.Parameter where Parameter = 'LASTLOADEDCOMWait'

insert into Utility.Parameter (Parameter, DateValue) values ('LASTLOADEDCOMWait', @StartTime /*getdate()*/ )


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@CensusDate, ''))

exec Utility.WriteAuditLogEvent 'LoadCOMWait', @Stats, @StartTime



﻿

CREATE PROCEDURE dbo.LoadServiceDeskEncounter

AS

DECLARE @StartTime datetime
DECLARE @Elapsed int
DECLARE @RowsDeleted Int
DECLARE @RowsInserted Int
DECLARE @Stats varchar(255)
DECLARE @lastmodifiedTime datetime

SELECT @StartTime = getdate()

TRUNCATE TABLE ServiceDesk.Encounter

INSERT INTO ServiceDesk.Encounter

SELECT 
	 EncounterRecNo
	,SourceUniqueID
	,Request
	,StartTime
	,DeadlineTime
	,ResolvedTime
	,EndTime
	,RequestStatus
	,RequestStatusGroup
	,ResolvedFlag
	,BreachStatus 
	,PredictedBreachStatus
	,ResolvedDurationMinutes
	,RequestDurationMinutes
	,TargetDurationMinutes
	,BreachValue
	,Category
	,Workgroup
	,CreatedBy
	,AssignedTo
	,CreatedTime
	,Priority
	,Template
	,Classification
	,ClosureDescription
	,AssignmentCount
	,CallerFullName
	,CallerEmail
	,CallerOrganization
	,SolutionNotes
	,History
	,Impact
	,PriorityCode
FROM 	
	dbo.TLoadServiceDeskEncounter Encounter
	
SELECT @RowsInserted = @@rowcount

SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 

EXEC WriteAuditLogEvent 'LoadServiceDeskEncounter', @Stats, @StartTime
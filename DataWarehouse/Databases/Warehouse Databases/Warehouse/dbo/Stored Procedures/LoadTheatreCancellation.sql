﻿CREATE procedure [dbo].[LoadTheatreCancellation]
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0



truncate table Theatre.Cancellation


insert into Theatre.Cancellation
(
	 SourceUniqueID
	,CancellationDate
	,CancelReasonCode
	,CancelReasonCode1
	,CancellationComment
	,OperationDetailSourceUniqueID
	,ConsultantCode
	,SpecialtyCode
	,SurgeonCode
	,SurgeonSpecialtyCode
	,ProposedOperationDate
	,DistrictNo
	,Forename
	,Surname
	,WardCode
	,WardCode1
	,PreMedGivenFlag
	,FastedFlag
	,LastUpdated
	,PatientClassificationCode
	,PatientSourceUniqueID
	,CampusCode
	,TheatreCode
	,TheatreCode1
	,CancellationSurgeonCode
	,InitiatorCode
	,PriorityCode
	,AdmissionDate
)
select
	 SourceUniqueID = CC_SEQU
	,CancellationDate = CC_DATE_TIME_CANCELLED
	,CancelReasonCode = CC_CN_SEQU
	,CancelReasonCode1 = case when CC_CODE = '' then null else CC_CODE end
	,CancellationComment = CC_DETAILS
	,OperationDetailSourceUniqueID = CC_OP_SEQU
	,ConsultantCode = case when CC_CONSULTANT = '' then null else CC_CONSULTANT end
	,SpecialtyCode = CC_S1_SEQU
	,SurgeonCode = CC_SU_SEQU
	,SurgeonSpecialtyCode = CC_SURG_SPECL
	,ProposedOperationDate = CC_DATE
	,DistrictNo = case when CC_MRN = '' then null else CC_MRN end
	,Forename = case when CC_FNAME = '' then null else CC_FNAME end
	,Surname = case when CC_LNAME = '' then null else CC_LNAME end
	,WardCode = CC_WA_SEQU
	,WardCode1 = case when CC_WARD = '' then null else CC_WARD end
	,PreMedGivenFlag = CC_PREMED
	,FastedFlag = CC_FASTED
	,LastUpdated = CC_LOG_DATE
	,PatientClassificationCode = CC_CL_SEQU
	,PatientSourceUniqueID = CC_PA_SEQU
	,CampusCode = CC_CA_SEQU
	,TheatreCode = CC_TH_SEQU
	,TheatreCode1 = case when CC_TH_CODE = '' then null else CC_TH_CODE end
	,CancellationSurgeonCode = CC_CANCEL_SU_SEQU
	,InitiatorCode = CC_GLI_SEQU
	,PriorityCode = CC_CLP_SEQU
	,AdmissionDate = CC_ADMIT_DATE
from
	[$(otprd)].dbo.FCCITEMS Cancellation


select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadTheatreCancellation', @Stats, @StartTime

print @Stats

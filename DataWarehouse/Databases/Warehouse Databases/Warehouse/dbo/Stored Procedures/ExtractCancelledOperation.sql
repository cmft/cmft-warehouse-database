﻿--this process is a Staging operation and should be migrated to the ETL server
CREATE proc [dbo].[ExtractCancelledOperation]

as

declare @StartTime datetime
declare @Elapsed int
declare @Sproc varchar(255)
declare @Stats varchar(255)
declare @RowsInserted int;

select @StartTime = getdate();

truncate table dbo.TImportCancelledOperation

insert into dbo.TImportCancelledOperation
(
	SourceUniqueID
	,CasenoteNumber
	,Directorate
	,NationalSpecialty
	,ConsultantCode
	,AdmissionDate
	,ProcedureDate
	,WardCode
	,Anaesthetist
	,Surgeon
	,ProcedureCode
	,CancellationDate
	,CancellationReason
	,TCIDate
	,Comments
	,ProcedureCompleted
	,BreachwatchComments
	,Removed
	,RemovedComments
	,ReportableBreach
	,ContextCode
)
select
	ID
	,[Casenote Number]
	,Directorate
	,Specialty
	,Consultant
	,[Admission Date]
	,[Procedure Date]
	,Ward
	,Anaesthetist
	,Surgeon
	,[Procedure Code]
	,[Cancellation Date]
	,[Cancellation Reason]
	,[TCI Date]
	,Comments
	,[Procedure Completed]
	,[Breachwatch Comments]
	,Removed
	,[Removed Comments]
	,[Reportable Breach]
	,[Context Code]
from
	SHAREPOINT...CancelledOperations;


select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;
	


﻿

CREATE PROCEDURE [dbo].[LoadCOMReturnMAR]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

DECLARE @FromDate Date = convert(Date,(SELECT DATEADD(s,0,DATEADD(mm, DATEDIFF(m,0,getdate())-1,0)) ),103)
DECLARE @ToDate DATE =  convert(Date,(SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0))),103)

DELETE
FROM COM.ReturnMAR
WHERE ReturnPeriod = (YEAR(ItemDate) * 100) + MONTH (ItemDate)
	AND LoadDate = Convert(Date,dateadd(d,-1,CURRENT_TIMESTAMP),103)
	AND [Return] = 'MAR'
	AND ReturnItem IN('GP Written Referrals Made'
					 ,'Other Referrals Made'
					 ,'Other Referrals Seen'
					 ,'GP Referrals Seen'
					 )


-- Insert MARGPReferralsMade
INSERT INTO COM.ReturnMAR
(
	ReturnPeriod,	
	
	LoadDate,
	
	[Return],
	
	ReturnItem,
	
	InterfaceCode,
	
	SourceUniqueNo,
											
	ItemDate,
	
	ItemSpecialty,
		
	NHSNumber,
	
	ProfessionalCarer,
	
	CommissionerCode,
			 
	Commissioner
)
SELECT
	ReturnPeriod,	
	
	LoadDate,
	
	[Return],
	
	ReturnItem,
	
	InterfaceCode,
	
	SourceUniqueNo,
											
	ItemDate,
	
	ItemSpecialty,
		
	NHSNumber,
	
	ProfessionalCarer,
	
	CommissionerCode,
			 
	Commissioner

FROM
	dbo.TLoadCOMMARGPReferralsMade MARGPReferralsMade

WHERE ItemDate >= @FromDate
	AND ItemDate <= @ToDate;


-- Insert MARGPReferralsSeen
INSERT INTO COM.ReturnMAR
(
	ReturnPeriod,	
	
	LoadDate,
	
	[Return],
	
	ReturnItem,
	
	InterfaceCode,
	
	SourceUniqueNo,
											
	ItemDate,
	
	ItemSpecialty,
		
	NHSNumber,
	
	ProfessionalCarer,
	
	CommissionerCode,
			 
	Commissioner
)
SELECT
	ReturnPeriod,	
	
	LoadDate,
	
	[Return],
	
	ReturnItem,
	
	InterfaceCode,
	
	SourceUniqueNo,
											
	ItemDate,
	
	ItemSpecialty,
		
	NHSNumber,
	
	ProfessionalCarer,
	
	CommissionerCode,
			 
	Commissioner

FROM
	dbo.TLoadCOMMARGPReferralsSeen MARGPReferralsSeen

WHERE ItemDate >= @FromDate
	AND ItemDate <= @ToDate;
	
END

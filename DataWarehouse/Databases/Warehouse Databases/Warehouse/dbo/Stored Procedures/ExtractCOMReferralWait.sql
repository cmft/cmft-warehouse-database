﻿
CREATE procedure [dbo].[ExtractCOMReferralWait]
	@CensusDate smalldatetime
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate();

select @RowsInserted = 0;


truncate table dbo.TImportCOMReferralWait;

insert into dbo.TImportCOMReferralWait
(
	 SourceUniqueID
	,SourceEncounterNo 
	,CensusDate
	,ReferredBySpecialtyID
	,ReferredToSpecialtyID
	,SourceofReferralID
	,ReferralReceivedTime
	,ReasonforReferralID
	,CancelledTime
	,CancelledReasonID
	,ReferralSentTime
	,UrgencyID
	,ReferralPriorityID
	,AuthoriedTime
	,ReferralClosedTime
	,ClosureReasonID
	,ReferredByProfessionalCarerID
	,ReferredByStaffTeamID
	,ReferredByHealthOrgID
	,ReferredToProfessionalCarerID
	,ReferredToHealthOrgID
	,ReferredToStaffTeamID
	,OutcomeOfReferralID
	,ReferralStatusID
	,RejectionID
	,TypeofReferralID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,ProfessionalCarerCurrentID
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID
	,ProfessionalCarerScheduleID
	,ProfessionalCarerHealthOrgScheduleID
	,ProfessionalCarerParentHealthOrgScheduleID
	,ProfessionalCarerReferralID
	,ProfessionalCarerHealthOrgReferralID
	,ProfessionalCarerParentHealthOrgReferralID
	,RTTStatusDate
	,RTTPatientPathwayID
	,RTTStartTime
	,RTTStartFlag
	,RTTStatusID
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,ParentID
	,HealthOrgOwner
	,RequestedServiceID
	,ScheduleID
	,ScheduleSpecialtyID
	,ScheduleStaffTeamID
	,ScheduleProfessionalCarerID
	,ScheduleSeenByProfessionalCarerID
	,ScheduleStartTime
	,ScheduleEndTime
	,ScheduleArrivedTime
	,ScheduleSeenTime
	,ScheduleDepartedTime
	,ScheduleAttendedID
	,ScheduleOutcomeID
	,ScheduleVisitID
	,ScheduleContactTypeID
	,ScheduleTypeID
	,ScheduleReasonID
	,ScheduleCanceledReasonID
	,ScheduleCanceledTime
	,ScheduleCancelledByID
	,ScheduleMoveReasonID
	,ScheduleMovetime
	,ScheduleServicePointID
	,ScheduleServicePointSessionID
	,ScheduleMoveCount
	,ScheduleLocationTypeID
	,ScheduleLocationDescription
	,ScheduleLocationTypeHealthOrgID
	,ScheduleWaitingListID
	,ScheduleOutcomeCode
	,ScheduleBookedTime
) 

select
	 SourceUniqueID = Referral.REFRL_REFNO	
	,SourceEncounterNo  = Referral.SourceEncounterNo
	,CensusDate = @CensusDate
	,ReferredBySpecialtyID = Referral.REFBY_SPECT_REFNO	
	,ReferredToSpecialtyID = Referral.REFTO_SPECT_REFNO	
	,SourceofReferralID = Referral.SORRF_REFNO	
	,ReferralReceivedTime = Referral.RECVD_DTTM	
	,ReasonforReferralID = Referral.REASN_REFNO	
	,CancelledTime = Referral.CANCD_DTTM	
	,CancelledReasonID = Referral.CANRS_REFNO	
	,ReferralSentTime = Referral.SENT_DTTM	
	,UrgencyID = Referral.URGNC_REFNO	
	,ReferralPriorityID = Referral.PRITY_REFNO	
	,AuthoriedTime = Referral.AUTHR_DTTM	
	,ReferralClosedTime = Referral.CLOSR_DATE	
	,ClosureReasonID = Referral.CLORS_REFNO	
	,ReferredByProfessionalCarerID = Referral.REFBY_PROCA_REFNO	
	,ReferredByStaffTeamID = Referral.REFBY_STEAM_REFNO	
	,ReferredByHealthOrgID = Referral.REFBY_HEORG_REFNO	
	,ReferredToProfessionalCarerID = Referral.REFTO_PROCA_REFNO	
	,ReferredToHealthOrgID = Referral.REFTO_HEORG_REFNO	
	,ReferredToStaffTeamID = Referral.REFTO_STEAM_REFNO	
	,OutcomeOfReferralID = Referral.RFOCM_REFNO	
	,ReferralStatusID = Referral.RSTAT_REFNO	
	,RejectionID = Referral.RJECT_REFNO	
	,TypeofReferralID = Referral.RETYP_REFNO		
	,PatientSourceID = Referral.PATNT_REFNO
	,PatientSourceSystemUniqueID = Patient.PASID
	,PatientNHSNumber = Patient.NHS_IDENTIFIER
	,PatientNHSNumberStatusIndicator = 1
	,PatientLocalIdentifier = PatientIDLocalIdentifier.IDENTIFIER
	,PatientPASIdentifier = PatientIDPASNumber.IDENTIFIER
	,PatientIdentifier = PatientIDPatientIdentifier.Identifier
	,PatientFacilityIdentifier = PatientIDPatientFacilityID.IDENTIFIER	
	,PatientTitleID = Patient.TITLE_REFNO	
	,PatientForename = Patient.FORENAME	
	,PatientSurname = Patient.SURNAME	
	,PatientAddress1 = ReferralPatientAddress.LINE1	
	,PatientAddress2 = ReferralPatientAddress.LINE2	
	,PatientAddress3 = ReferralPatientAddress.LINE3	
	,PatientAddress4 = ReferralPatientAddress.LINE4	
	,PatientPostcode = ReferralPatientAddress.PCODE	
	,PatientDateOfBirth = Patient.DATE_OF_BIRTH	
	,PatientDateOfDeath = Patient.DATE_OF_DEATH	
	,PatientSexID = Patient.SEXXX_REFNO	
	,PatientEthnicGroupID = Patient.ETHGR_REFNO
	,PatientSpokenLanguageID = Patient.SPOKL_REFNO
	,PatientReligionID = Patient.RELIG_REFNO
	,PatientMaritalStatusID = Patient.MARRY_REFNO	
	,ProfessionalCarerCurrentID = ProfessionalCarerCurrentBase.PatientProfCarerHealthOrgID		
	,ProfessionalCarerHealthOrgCurrentID = ProfessionalCarerCurrentBase.PatientProfCarerHealthOrgID
	,ProfessionalCarerParentHealthOrgCurrentID = ProfessionalCarerCurrentBase.PatientProfCarerParentHealthOrgID	
	,ProfessionalCarerScheduleID = ProfessionalCarerScheduleBase.PatientProfCarerHealthOrgID		
	,ProfessionalCarerHealthOrgScheduleID = ProfessionalCarerScheduleBase.PatientProfCarerHealthOrgID
	,ProfessionalCarerParentHealthOrgScheduleID = ProfessionalCarerScheduleBase.PatientProfCarerParentHealthOrgID	
	,ProfessionalCarerReferralID = ProfessionalCarerReferralBase.PatientProfCarerHealthOrgID		
	,ProfessionalCarerHealthOrgReferralID = ProfessionalCarerReferralBase.PatientProfCarerHealthOrgID
	,ProfessionalCarerParentHealthOrgReferralID = ProfessionalCarerReferralBase.PatientProfCarerParentHealthOrgID
	,RTTStatusDate = Referral.STATUS_DTTM	
	,RTTPatientPathwayID = Referral.PATNT_PATHWAY_ID	
	,RTTStartTime = Referral.RTT_START_DATE	
	,RTTStartFlag = Referral.RTT_STARTED	
	,RTTStatusID = Referral.RTTST_REFNO	
	,CreatedTime = Referral.CREATE_DTTM	
	,ModifiedTime = Referral.MODIF_DTTM	
	,CreatedByID = Referral.USER_CREATE	
	,ModifiedByID = Referral.USER_MODIF	
	,ArchiveFlag = Referral.ARCHV_FLAG	
	,ParentID  = Referral.PARNT_REFNO	
	,HealthOrgOwner = Referral.OWNER_HEORG_REFNO	
	,RequestedServiceID = Referral.ACTYP_REFNO
	,ScheduleID = Schedule.SCHDL_REFNO	
	,ScheduleSpecialtyID = Schedule.SPECT_REFNO
	,ScheduleStaffTeamID = Schedule.STEAM_REFNO
	,ScheduleProfessionalCarerID = Schedule.PROCA_REFNO
	,ScheduleSeenByProfessionalCarerID = Schedule.SEENBY_PROCA_REFNO
	,ScheduleStartTime = Schedule.START_DTTM
	,ScheduleEndTime = Schedule.END_DTTM
	,ScheduleArrivedTime = Schedule.ARRIVED_DTTM
	,ScheduleSeenTime  = Schedule.SEEN_DTTM
	,ScheduleDepartedTime  = Schedule.DEPARTED_DTTM
	,ScheduleAttendedID = Schedule.ATTND_REFNO 
	,ScheduleOutcomeID  = Schedule.SCOCM_REFNO 
	,ScheduleVisitID  = Schedule.VISIT_REFNO --Type of appointment
	,ScheduleContactTypeID = Schedule.CONTY_REFNO --Contact Type
	,ScheduleTypeID = Schedule.SCTYP_REFNO -- appointments 1470 Contacts 1468 
	,ScheduleReasonID = Schedule.REASN_REFNO
	,ScheduleCanceledReasonID = Schedule.CANCR_REFNO
	,ScheduleCanceledTime = Schedule.CANCR_DTTM
	,ScheduleCancelledByID = Schedule.CANCB_REFNO
	,ScheduleMoveReasonID  = Schedule.MOVRN_REFNO
	,ScheduleMovetime = Schedule.MOVE_DTTM
	,ScheduleServicePointID  = Schedule.SPONT_REFNO
	,ScheduleServicePointSessionID = Schedule.SPSSN_REFNO
	,ScheduleMoveCount = Schedule.MOVE_COUNT
	,ScheduleLocationTypeID = Schedule.LOTYP_REFNO
	,ScheduleLocationDescription = Schedule.LOCATION
	,ScheduleLocationTypeHealthOrgID = Schedule.LOTYP_HEORG_REFNO
	,ScheduleWaitingListID = Schedule.WLIST_REFNO

	,ScheduleOutcomeCode = 
		case Schedule.SCTYP_REFNO
		WHEN 1468 -- Community Contact
		THEN
			CASE
			--DNA
			WHEN     Schedule.SCOCM_REFNO = 1457
			AND Schedule.CANCR_DTTM IS NULL
			AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'DNAT'
					 
			--Patient Died
			WHEN     Schedule.SCOCM_REFNO = 1459
			AND Schedule.CANCR_DTTM IS NULL
			AND convert(date,Schedule.START_DTTM)   <= @CensusDate THEN 'PDCD'
					 
			--Cancelled by hospital
			WHEN   
			(   
				Schedule.SCOCM_REFNO IN(2001651
							,2003640
							)
			OR 
				Schedule.CANCR_DTTM IS NOT NULL 
			)
			AND Schedule.CANCR_REFNO in (3249
									,2003537
									,2003538
									,2003546
									,2004149
									,2005554
									,2006152
									,2006153
									,2006154
									,2006155
									,2006156
									,2006157
									,2006158
									,2006159
									,2006160
									,2006161
									,2006162
									,2006163
									,2006164
									,2006165
									,2006166
									,2006167
									,2006168
									,2006169)
			AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'CHOS'
					
			--cancelled by patient
			WHEN   
			(   
				Schedule.SCOCM_REFNO IN(2001651
							,2003640
							)
			OR 
				Schedule.CANCR_DTTM IS NOT NULL 
			)
			AND Schedule.CANCR_REFNO in (2003548
									,2004148
									,3248
									,2003539
									,2003540
									,2003541
									,2003542
									,2003544
									,2005621
									,2006151)
			AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'CPAT'
					
			--cancelled by Other
			WHEN   
				(   
					Schedule.SCOCM_REFNO IN(2001651
								,2003640
								)
				OR 
					Schedule.CANCR_DTTM IS NOT NULL 
				)
			AND Schedule.CANCR_REFNO NOT IN (3249
									,2003537
									,2003538
									,2003546
									,2004149
									,2005554
									,2006152
									,2006153
									,2006154
									,2006155
									,2006156
									,2006157
									,2006158
									,2006159
									,2006160
									,2006161
									,2006162
									,2006163
									,2006164
									,2006165
									,2006166
									,2006167
									,2006168
									,2006169
									,2003548
									,2004148
									,3248
									,2003539
									,2003540
									,2003541
									,2003542
									,2003544
									,2005621
									,2006151)
			AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'COTH'
					
			--Attended
			WHEN
				Schedule.ARRIVED_DTTM IS NOT NULL
			AND	Schedule.DEPARTED_DTTM IS NOT NULL 
			AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'ATTD'
					
			--Unactualised
			WHEN 
				Schedule.SCOCM_REFNO NOT IN 
								(  1210
								,1457
								,1459
								,2001651
								,2003640
								)
			AND
				(
						Schedule.ARRIVED_DTTM IS NULL
					OR Schedule.DEPARTED_DTTM IS NULL 
				)
			AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'UACT'
					
			--Planned
			WHEN
				Schedule.SCOCM_REFNO = 1210
			AND convert(date,Schedule.START_DTTM) > @CensusDate THEN 'PLND'
					
			--Not Specified
			ELSE 'NSPD'
			END

		WHEN 1470 -- Clinic Contact
		THEN	
			CASE
			-- Attended
			WHEN Schedule.ATTND_REFNO IN (
							357,
							2868,
							2004151                 
						) 
			AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'ATTD'
										
			--Cancelled by Hospital
			WHEN Schedule.ATTND_REFNO IN (
									2004300
								) 
				AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'CHOS'				
								
			--Cancelled by Patient		
			WHEN Schedule.ATTND_REFNO IN (
									2004301
								) 
				AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'CPAT'	
					
			--Cancelled by Other				
			WHEN Schedule.ATTND_REFNO IN (
									2870,
									2003532,
									2004528,
									3006508
								) 
				AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'COTH'	
					
			--DNA
			WHEN Schedule.ATTND_REFNO IN (
									358,
									2000724,
									2003494,
									2003495,
									2003534
								)
				AND convert(date,Schedule.START_DTTM) <= @CensusDate THEN 'DNAT'
					

					
			--Planned
			WHEN Schedule.ATTND_REFNO NOT IN(357,
									2868,
									2004151,
									2870,
									2003532,
									2004528,
									2004300,
									2004301,
									3006508,
									358,
									2000724,
									2003494,
									2003495,
									2003534
									)
				AND cast(Schedule.START_DTTM as date) > @CensusDate  THEN 'PLND'

			--Not Specified	
			WHEN Schedule.ATTND_REFNO = '45' THEN 'NSPD'

			--Not Specified
			ELSE 'NSPD'
			END 
		end

	,ScheduleBookedTime = Schedule.CREATE_DTTM
from 
	(
	select 
		SourceEncounterNo = 
			ROW_NUMBER() OVER(PARTITION BY Referral.PATNT_REFNO ORDER BY Referral.RECVD_DTTM)

		,REFRL_REFNO
		,REFBY_SPECT_REFNO
		,REFTO_SPECT_REFNO
		,SORRF_REFNO
		,RECVD_DTTM
		,REASN_REFNO
		,CANCD_DTTM
		,CANRS_REFNO
		,SENT_DTTM
		,URGNC_REFNO
		,PRITY_REFNO
		,AUTHR_DTTM
		,CLOSR_DATE
		,CLORS_REFNO
		,REFBY_PROCA_REFNO
		,REFBY_STEAM_REFNO
		,REFBY_HEORG_REFNO
		,REFTO_PROCA_REFNO
		,REFTO_HEORG_REFNO
		,REFTO_STEAM_REFNO
		,RFOCM_REFNO
		,RSTAT_REFNO
		,RJECT_REFNO
		,RETYP_REFNO
		,PATNT_REFNO
		,STATUS_DTTM
		,PATNT_PATHWAY_ID
		,RTT_START_DATE
		,RTT_STARTED
		,RTTST_REFNO
		,CREATE_DTTM
		,MODIF_DTTM
		,USER_CREATE
		,USER_MODIF
		,ARCHV_FLAG
		,PARNT_REFNO
		,OWNER_HEORG_REFNO
		,ACTYP_REFNO
	from 
		[$(ipm)].dbo.Referral
	) Referral

inner join [$(ipm)].dbo.Patient Patient
on	Patient.PATNT_REFNO = Referral.PATNT_REFNO
and Patient.ARCHV_FLAG = 'N'

inner join
	(
	select 
		SPECT_REFNO
	from 
		COM.[Service]
	where 
		TCSDestinationID = 2 --CMFT services only
	and SPECT_REFNO is not null
	) Service
on	Service.SPECT_REFNO = Referral.REFTO_SPECT_REFNO

left outer join [$(ipm)].dbo.Schedule Schedule
on	Schedule.REFRL_REFNO = Referral.REFRL_REFNO
and Schedule.ARCHV_FLAG = 'N'
and Schedule.PARNT_REFNO is null

left outer join
	(
	select 
		 PatientID.PATNT_REFNO
		,PatientID.IDENTIFIER
	from 
		[$(ipm)].dbo.PatientID
	where 
		PatientID.PITYP_REFNO = 1059		-- local identifier
	and PatientID.ARCHV_FLAG = 'n'
	and PatientID.CURNT_FLAG = 'y'
	and PatientID.END_DTTM is null
	and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID PreviousPatientID
				where
					PreviousPatientID.PITYP_REFNO = 1059		-- local identifier
				and PreviousPatientID.ARCHV_FLAG = 'n'
				and PreviousPatientID.CURNT_FLAG = 'y'
				and PreviousPatientID.END_DTTM is null
				and PreviousPatientID.PATNT_REFNO = PatientID.PATNT_REFNO
				and PreviousPatientID.MODIF_DTTM > PatientID.MODIF_DTTM
			)
	) PatientIDLocalIdentifier
on Referral.PATNT_REFNO = PatientIDLocalIdentifier.PATNT_REFNO

left outer join
(
	select 
		 PatientID.PATNT_REFNO
		,PatientID.IDENTIFIER
	from 
		[$(ipm)].dbo.PatientID
	where 
		PatientID.PITYP_REFNO = 1063		-- pas number
	and PatientID.ARCHV_FLAG = 'n'
	and PatientID.CURNT_FLAG = 'y'
	and PatientID.END_DTTM is null
	and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID PreviousPatientID
				where
					PreviousPatientID.PITYP_REFNO = 1063		-- pas number
				and PreviousPatientID.ARCHV_FLAG = 'n'
				and PreviousPatientID.CURNT_FLAG = 'y'
				and PreviousPatientID.END_DTTM is null
				and PreviousPatientID.PATNT_REFNO = PatientID.PATNT_REFNO
				and PreviousPatientID.MODIF_DTTM > PatientID.MODIF_DTTM
			)
) PatientIDPASNumber
on Referral.PATNT_REFNO = PatientIDPASNumber.PATNT_REFNO

left outer join
(
	select
		 PatientID.PATNT_REFNO
		,Identifier = max(PatientID.IDENTIFIER)
	from 
		[$(ipm)].dbo.PatientID
	where 
		PatientID.PITYP_REFNO = 2001233		-- patient identifier
		and PatientID.ARCHV_FLAG = 'n'
		and PatientID.CURNT_FLAG = 'y'
		and PatientID.END_DTTM is null
		and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID PreviousPatientID
				where
					PreviousPatientID.PITYP_REFNO = 2001233		-- patient identifier
				and PreviousPatientID.ARCHV_FLAG = 'n'
				and PreviousPatientID.CURNT_FLAG = 'y'
				and PreviousPatientID.END_DTTM is null
				and PreviousPatientID.PATNT_REFNO = PatientID.PATNT_REFNO
				and PreviousPatientID.MODIF_DTTM > PatientID.MODIF_DTTM
			)
		group by  PatientID.PATNT_REFNO

) PatientIDPatientIdentifier
on Referral.PATNT_REFNO = PatientIDPatientIdentifier.PATNT_REFNO

left outer join
(
	select 
		 PatientID.PATNT_REFNO
		,PatientID.IDENTIFIER
	from 
		[$(ipm)].dbo.PatientID
	where 
		PatientID.PITYP_REFNO = 2001232		-- patient id (facility)
	and PatientID.ARCHV_FLAG = 'n'
	and PatientID.CURNT_FLAG = 'y'
	and PatientID.END_DTTM is null
			and not exists	
			(
				select
					1
				from
					[$(ipm)].dbo.PatientID PreviousPatientID
				where
					PreviousPatientID.PITYP_REFNO =  2001232		-- patient id (facility)
				and PreviousPatientID.ARCHV_FLAG = 'n'
				and PreviousPatientID.CURNT_FLAG = 'y'
				and PreviousPatientID.END_DTTM is null
				and PreviousPatientID.PATNT_REFNO = PatientID.PATNT_REFNO
				and PreviousPatientID.MODIF_DTTM > PatientID.MODIF_DTTM
			)
) PatientIDPatientFacilityID
ON Referral.PATNT_REFNO = PatientIDPatientFacilityID.PATNT_REFNO


left outer join
(
	select
		 ReferralPatientAddress.REFRL_REFNO
		,ReferralPatientAddress.ADDSS_REFNO
		,PatientAddressDetails.LINE1
		,PatientAddressDetails.LINE2
		,PatientAddressDetails.LINE3
		,PatientAddressDetails.LINE4
		,PatientAddressDetails.PCODE
	from
		(
			select 
				REFRL_REFNO,
				ADDSS_REFNO = 
					max(ADDSS_REFNO)
			from 
				[$(ipm)].dbo.Referral Referral
			left outer join
				(
				SELECT
					 AddressRole.ADDSS_REFNO
					,AddressRole.START_DTTM
					,AddressRole.END_DTTM
					,AddressRole.PATNT_REFNO
				from 
					[$(ipm)].dbo.AddressRole  
				where   
					ROTYP_CODE = 'HOME'
				and ARCHV_FLAG = 'N'
				)PatientAddress
			on Referral.PATNT_REFNO = PatientAddress.PATNT_REFNO
			and PatientAddress.START_DTTM <= Referral.RECVD_DTTM 
			and (PatientAddress.END_DTTM IS NULL OR PatientAddress.END_DTTM > Referral.RECVD_DTTM)
			group by 
				REFRL_REFNO
		) ReferralPatientAddress
	
	left outer join 
		(	
		select distinct
			 ADDSS_REFNO
			,LINE1
			,LINE2
			,LINE3
			,LINE4
			,PCODE
		 from 
			[$(ipm)].dbo.Address
		 where 
			ADTYP_CODE = 'POSTL'
		and ARCHV_FLAG = 'N'
		) PatientAddressDetails
	on ReferralPatientAddress.ADDSS_REFNO = PatientAddressDetails.ADDSS_REFNO
) ReferralPatientAddress

on Referral.REFRL_REFNO = ReferralPatientAddress.REFRL_REFNO


outer apply
(
select top 1
	 PatientProfessionalCarerID = 
		PatientProfessionalCarer.PATPC_REFNO
	,PatientProfCarerHealthOrgID = 
		PatientProfessionalCarer.HEORG_REFNO
	,PatientProfCarerParentHealthOrgID = 
		PatientProfCarerHealthOrg.ParentHeorg_Refno 

from
	[$(ipm)].dbo.PatientProfessionalCarer
left outer join 
	(	
	select
		 HealthOrganisationID = 
			HealthOrganisation.HEORG_REFNO
		,ParentHeorg_Refno = 
			ParentHealthOrganisation.HEORG_REFNO 
	from 
		[$(ipm)].dbo.HealthOrganisation 	
	inner join	 
	(	
		select 
			 HEORG_REFNO 
		from 
			[$(ipm)].dbo.HealthOrganisation 
		where 
			ARCHV_FLAG = 'N'
		AND HOTYP_REFNO = 629
		) ParentHealthOrganisation
	on ParentHealthOrganisation.HEORG_REFNO= HealthOrganisation.PARNT_REFNO
	where 
		ARCHV_FLAG = 'N'
	) PatientProfCarerHealthOrg
 on PatientProfessionalCarer.HEORG_REFNO = PatientProfCarerHealthOrg.HealthOrganisationID

where
	ARCHV_FLAG = 'N'
and PRTYP_REFNO = '1132'-- General Practitioner
and PatientProfessionalCarer.PATNT_REFNO = Schedule.PATNT_REFNO
and PatientProfessionalCarer.START_DTTM <= Schedule.START_DTTM 
and 
	(
		PatientProfessionalCarer.END_DTTM IS NULL 
	or
		PatientProfessionalCarer.END_DTTM > Schedule.START_DTTM
	)
order by 
	 CREATE_DTTM desc
	,MODIF_DTTM desc
)ProfessionalCarerScheduleBase

outer apply
(
select top 1
	 PatientProfessionalCarerID = 
		PatientProfessionalCarer.PATPC_REFNO
	,PatientProfCarerHealthOrgID = 
		PatientProfessionalCarer.HEORG_REFNO
	,PatientProfCarerParentHealthOrgID = 
		PatientProfCarerHealthOrg.ParentHEORG_REFNO 

from
	[$(ipm)].dbo.PatientProfessionalCarer
left outer join 
	(	
	select
		 HealthOrganisationID = HealthOrganisation.HEORG_REFNO
		,ParentHEORG_REFNO = ParentHealthOrganisation.HEORG_REFNO 
	from 
		[$(ipm)].dbo.HealthOrganisation 	
	inner join	 
	(	
		select 
			 HEORG_REFNO 
		from 
			[$(ipm)].dbo.HealthOrganisation 
		where 
			ARCHV_FLAG = 'N'
		AND HOTYP_REFNO = 629
		) ParentHealthOrganisation
	on ParentHealthOrganisation.HEORG_REFNO= HealthOrganisation.PARNT_REFNO
	where 
		ARCHV_FLAG = 'N'
	) PatientProfCarerHealthOrg
 on PatientProfessionalCarer.HEORG_REFNO = PatientProfCarerHealthOrg.HealthOrganisationID

where
	ARCHV_FLAG = 'N'
and PRTYP_REFNO = '1132'-- General Practitioner
and PatientProfessionalCarer.PATNT_REFNO = Referral.PATNT_REFNO
and PatientProfessionalCarer.START_DTTM <= Referral.RECVD_DTTM 
and 
	(
		PatientProfessionalCarer.END_DTTM IS NULL 
	or
		PatientProfessionalCarer.END_DTTM > Referral.RECVD_DTTM 
	)
order by 
	 CREATE_DTTM desc
	,MODIF_DTTM desc
)ProfessionalCarerReferralBase

outer apply
(
select top 1
	 PatientProfessionalCarerID = 
		PatientProfessionalCarer.PATPC_REFNO
	,PatientProfCarerHealthOrgID = 
		PatientProfessionalCarer.HEORG_REFNO
	,PatientProfCarerParentHealthOrgID = 
		PatientProfCarerHealthOrg.ParentHEORG_REFNO 

from
	[$(ipm)].dbo.PatientProfessionalCarer
left outer join 
	(	
	select
		 HealthOrganisationID = HealthOrganisation.HEORG_REFNO
		,ParentHEORG_REFNO = ParentHealthOrganisation.HEORG_REFNO 
	from 
		[$(ipm)].dbo.HealthOrganisation 	
	inner join	 
	(	
		select 
			 HEORG_REFNO 
		from 
			[$(ipm)].dbo.HealthOrganisation 
		where 
			ARCHV_FLAG = 'N'
		AND HOTYP_REFNO = 629
		) ParentHealthOrganisation
	on ParentHealthOrganisation.HEORG_REFNO= HealthOrganisation.PARNT_REFNO
	where 
		ARCHV_FLAG = 'N'
	) PatientProfCarerHealthOrg
 on PatientProfessionalCarer.HEORG_REFNO = PatientProfCarerHealthOrg.HealthOrganisationID

where
	ARCHV_FLAG = 'N'
and PRTYP_REFNO = '1132'-- General Practitioner
and PatientProfessionalCarer.PATNT_REFNO = Referral.PATNT_REFNO
and PatientProfessionalCarer.END_DTTM IS NULL 

order by 
	 CREATE_DTTM desc
	,MODIF_DTTM desc
)ProfessionalCarerCurrentBase

where 
	Referral.CLOSR_DATE is null
and Referral.ARCHV_FLAG = 'N'
and Referral.RECVD_DTTM > = '1 Apr 2011'
;


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT;


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate());

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Census Date ' + CONVERT(varchar(11), @CensusDate) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins';

EXEC Utility.WriteAuditLogEvent 'ExtractCOMReferralWait', @Stats, @StartTime;

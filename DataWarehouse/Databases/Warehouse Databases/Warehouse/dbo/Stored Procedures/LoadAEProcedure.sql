﻿CREATE procedure [dbo].[LoadAEProcedure] @from smalldatetime, @to smalldatetime as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

delete from AE.[Procedure]
where
	exists
	(
	select
		1
	from
		AE.Encounter AEEncounter
	where
		coalesce(AEEncounter.ArrivalDate, @from) between @from and @to
	and	AEEncounter.SourceUniqueID = [Procedure].AESourceUniqueID
	)

select @RowsDeleted = @@rowcount

delete from AE.[Procedure]
where
	not exists
	(
	select
		1
	from
		AE.Encounter
	where
		Encounter.SourceUniqueID = [Procedure].AESourceUniqueID
	)

select @RowsDeleted = @RowsDeleted + @@rowcount

insert into AE.[Procedure]
(
	 SourceUniqueID
	,ProcedureDate
	,SequenceNo
	,ProcedureCode
	,AESourceUniqueID
	,SourceProcedureCode
)
select
	 SourceUniqueID
	,ProcedureDate
	,SequenceNo
	,ProcedureCode
	,AESourceUniqueID
	,SourceProcedureCode
from
	dbo.TLoadAEProcedure

select @RowsInserted = @@rowcount


--now resequence procedures
update
	AE.[Procedure]
set
	SequenceNo = Resequenced.SequenceNo
from
	(
	select
		 ProcedureRecno
		,SequenceNo = 
			ROW_NUMBER() OVER(PARTITION BY AESourceUniqueID ORDER BY ProcedureDate, SourceUniqueID, SequenceNo, ProcedureRecno)
	from
		AE.[Procedure]
	) Resequenced
where
	Resequenced.ProcedureRecno = AE.[Procedure].ProcedureRecno


--populate the AEEncounter Procedure codes
update
	AE.Encounter
set
	 TreatmentCodeFirst = AEProcedure.ProcedureCode
	,TreatmentDateFirst = AEProcedure.ProcedureDate
from
	dbo.TLoadAEProcedure AEProcedure
where
	AEProcedure.AESourceUniqueID = AE.Encounter.SourceUniqueID
and	AEProcedure.SequenceNo = 1

update
	AE.Encounter
set
	 TreatmentCodeSecond = AEProcedure.ProcedureCode
	,TreatmentDateSecond = AEProcedure.ProcedureDate
from
	dbo.TLoadAEProcedure AEProcedure
where
	AEProcedure.AESourceUniqueID = AE.Encounter.SourceUniqueID
and	AEProcedure.SequenceNo = 2



select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadAEProcedure', @Stats, @StartTime

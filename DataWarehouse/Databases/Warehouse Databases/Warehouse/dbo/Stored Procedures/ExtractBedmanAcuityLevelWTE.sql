﻿
CREATE proc [dbo].[ExtractBedmanAcuityLevelWTE]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @from smalldatetime = NULL
declare @to smalldatetime = NULL

select @StartTime = getdate()

truncate table dbo.TImportBedmanAcuityLevelWTE

insert into dbo.TImportBedmanAcuityLevelWTE

select 
	[LevelID] ,
	[WardTypeID] ,
	[AcuityMutiplierLevel0] ,
	[AcuityMutiplierLevel1a] ,
	[AcuityMutiplierLevel1b],
	[AcuityMutiplierLevel2] ,
	[AcuityMutiplierLevel3]

FROM
(
SELECT [LevelID] = [LevelID]
      ,[WardTypeID] = [WardTypeID]
      ,[AcuityMutiplierLevel0] = [WTE_0]
      ,[AcuityMutiplierLevel1a] = [WTE_1a]
      ,[AcuityMutiplierLevel1b] = [WTE_1b]
      ,[AcuityMutiplierLevel2] = [WTE_2]
      ,[AcuityMutiplierLevel3] = [WTE_3]
FROM 
	[$(BedmanTest)].dbo.AcuityLevelWTE
) AS tbl_AcuityLevelWTE

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractBedmanAcuityLevelWTE', @Stats, @StartTime
﻿
create procedure [dbo].[BuildAPCEncounterProcedureDetail] 

as

--Links theatre procedure detail records to the provider spell

--Should be run following a refresh of either APC or Theatre data

truncate table APC.EncounterProcedureDetail

insert
into
	APC.EncounterProcedureDetail
(
	 EncounterRecno
	,ProcedureDetailSourceUniqueID
)
select distinct
	 EncounterRecno
	,ProcedureDetailSourceUniqueID
from
	(
	select
		 Encounter.EncounterRecno
		,ProcedureDetailSourceUniqueID = ProcedureDetail.SourceUniqueID
	from
		APC.Encounter Encounter

	inner join Theatre.PatientBooking PatientBooking
	on	PatientBooking.NHSNumber = replace(Encounter.NHSNumber, ' ', '')

	inner join Theatre.OperationDetail OperationDetail
	on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
	and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

	inner join Theatre.ProcedureDetail ProcedureDetail
	on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

	where
	--null OperationEndTime indicates cancelled procedure
		OperationDetail.OperationEndDate is not null

	union all

	select
		 Encounter.EncounterRecno
		,ProcedureDetail.SourceUniqueID
	from
		APC.Encounter Encounter

	inner join Theatre.PatientBooking PatientBooking
	on	replace(PatientBooking.CasenoteNumber, 'Y', '/') = Encounter.CasenoteNumber

	inner join Theatre.OperationDetail OperationDetail
	on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
	and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

	inner join Theatre.ProcedureDetail ProcedureDetail
	on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

	where
	--null OperationEndTime indicates cancelled procedure
		OperationDetail.OperationEndDate is not null

	union all

	select
		 Encounter.EncounterRecno
		,ProcedureDetail.SourceUniqueID
	from
		APC.Encounter Encounter

	inner join Theatre.PatientBooking PatientBooking
	on
		(
			PatientBooking.Surname = Encounter.PatientSurname
		and	PatientBooking.Forename = Encounter.PatientForename
		and	PatientBooking.SexCode = Encounter.SexCode
		and	PatientBooking.DateOfBirth = Encounter.DateOfBirth
		)

	inner join Theatre.OperationDetail OperationDetail
	on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
	and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

	inner join Theatre.ProcedureDetail ProcedureDetail
	on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

	where
	--null OperationEndTime indicates cancelled procedure
		OperationDetail.OperationEndDate is not null
	) Encounter



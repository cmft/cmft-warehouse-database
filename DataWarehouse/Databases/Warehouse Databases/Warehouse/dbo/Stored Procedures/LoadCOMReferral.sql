﻿
CREATE PROCEDURE [dbo].[LoadCOMReferral] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the Referral table
DELETE FROM COM.Referral
WHERE EXISTS
  (SELECT SourceUniqueID
    FROM dbo.TImportCOMReferral
    WHERE COM.Referral.SourceUniqueID = dbo.TImportCOMReferral.SourceUniqueID
	--		AND COM.Referral.ModifiedTime = dbo.TImportCOMReferral.ModifiedTime 
	);
			
select @RowsDeleted = @@rowcount

-- Insert new rows into encounter destination table
INSERT INTO COM.Referral
(
	 SourceUniqueID --[REFRL_REFNO]
	 
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]
	
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]
	
	,SourceofReferralID --[SORRF_REFNO]
	
	,ReferralReceivedTime --[RECVD_DTTM]
	
	,ReferralReceivedDate --[RECVD_DTTM]
	
	,ReasonforReferralID --[REASN_REFNO]
	
	,CancelledTime --[CANCD_DTTM]
	
	,CancelledDate --[CANCD_DTTM]
	
	,CancelledReasonID --[CANRS_REFNO]
	
	,ReferralSentTime --[SENT_DTTM]

	,ReferralSentDate --[SENT_DTTM]
	
	,Urgency --[URGNC_REFNO]
	
	,Priority --[PRITY_REFNO]
	
	,AuthoriedTime --[AUTHR_DTTM]

	,AuthoriedDate --[AUTHR_DTTM]
	
	,ReferralClosedTime --[CLOSR_DATE]
	
	,ReferralClosedDate --[CLOSR_DATE]
	
	,ClosureReasonID --CLORS_REFNO
	
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]
	
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]
	
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]
	
	,OutcomeOfReferral --[RFOCM_REFNO]
	
	,ReferralStatus --[RSTAT_REFNO]
	
	,RejectionID --RejectionID--[RJECT_REFNO]
	
	,TypeofReferralID --[RETYP_REFNO]
	
	,PatientSourceID 
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator

	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier
	
	,PatientTitleID 
	
	,PatientForename 
	
	,PatientSurname 
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,ProfessionalCarerCurrentID
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID
	,ProfessionalCarerReferralID 
	,ProfessionalCarerHealthOrgReferralID 
	,ProfessionalCarerParentHealthOrgReferralID 
	
	,RTTStatusDate --[STATUS_DTTM]
	
	,RTTPatientPathwayID --[PATNT_PATHWAY_ID]
	
	,RTTStartTime --[RTT_START_DATE]
	
	,RTTStartFlag --[RTT_STARTED]
	
	,RTTStatusID --[RTTST_REFNO]
	
	,CreatedTime --[CREATE_DTTM]
	
	,ModifiedTime --[MODIF_DTTM]
	
	,CreatedByID --[USER_CREATE]
	
	,ModifiedByID --[USER_MODIF]
	
	,ArchiveFlag --[ARCHV_FLAG]
	
	,ParentID  --[PARNT_REFNO]
	
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	
	,RecordStatus
	
	,RequestedServiceID

	,Created
	,Updated
	,ByWhom
)

SELECT
	 SourceUniqueID --[REFRL_REFNO]
	 
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]
	
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]
	
	,SourceofReferralID --[SORRF_REFNO]
	
	,ReferralReceivedTime --[RECVD_DTTM]
	
	,ReferralReceivedDate --[RECVD_DTTM]
	
	,ReasonforReferralID --[REASN_REFNO]
	
	,CancelledTime --[CANCD_DTTM]
	
	,CancelledDate --[CANCD_DTTM]
	
	,CancelledReasonID --[CANRS_REFNO]
	
	,ReferralSentTime --[SENT_DTTM]

	,ReferralSentDate --[SENT_DTTM]
	
	,Urgency --[URGNC_REFNO]
	
	,Priority --[PRITY_REFNO]
	
	,AuthoriedTime --[AUTHR_DTTM]

	,AuthoriedDate --[AUTHR_DTTM]
	
	,ReferralClosedTime --[CLOSR_DATE]
	
	,ReferralClosedDate --[CLOSR_DATE]
	
	,ClosureReasonID --CLORS_REFNO
	
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]
	
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]
	
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]
	
	,OutcomeOfReferral --[RFOCM_REFNO]
	
	,ReferralStatus --[RSTAT_REFNO]
	
	,RejectionID --RejectionID--[RJECT_REFNO]
	
	,TypeofReferralID --[RETYP_REFNO]
	
	,PatientSourceID 
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator

	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier
	
	,PatientTitleID 
	
	,PatientForename 
	
	,PatientSurname 
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,ProfessionalCarerCurrentID
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID
	,ProfessionalCarerReferralID 
	,ProfessionalCarerHealthOrgReferralID 
	,ProfessionalCarerParentHealthOrgReferralID 
	
	,RTTStatusDate --[STATUS_DTTM]
	
	,RTTPatientPathwayID --[PATNT_PATHWAY_ID]
	
	,RTTStartTime --[RTT_START_DATE]
	
	,RTTStartFlag --[RTT_STARTED]
	
	,RTTStatusID --[RTTST_REFNO]
	
	,CreatedTime --[CREATE_DTTM]
	
	,ModifiedTime --[MODIF_DTTM]
	
	,CreatedByID --[USER_CREATE]
	
	,ModifiedByID --[USER_MODIF]
	
	,ArchiveFlag --[ARCHV_FLAG]
	
	,ParentID  --[PARNT_REFNO]
	
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	
	,RecordStatus ='C' -- Default as 'C' - current
	
	,RequestedServiceID

	,Created = GETDATE()
	,Updated = GETDATE()
	,ByWhom = SUSER_NAME()
FROM
	dbo.TLoadCOMReferral
	
--Update where test patient
UPDATE COM.Referral
SET RecordStatus = 'T'
WHERE	LEFT(PatientNHSNumber, 3) = '999'
	OR
		PatientForename  IN ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')
	OR
		PatientSurname  IN ('xxtestpatientaavp','xxtestpatientaafe')
				

select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) 

exec WriteAuditLogEvent 'LoadCOMReferral', @Stats, @StartTime

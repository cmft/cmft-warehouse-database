﻿

CREATE procedure [dbo].[LoadTheatreReferenceData] as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

--truncate table Theatre.Theatre

insert into Theatre.Theatre
(
	 TheatreCode
	,Theatre
	,OperatingSuiteCode
	,TheatreCode1
	,InactiveFlag
	,Colour
)
select
	 TheatreCode = TH_SEQU
	,Theatre = TH_DESC
	,OperatingSuiteCode = TH_UN_SEQU
	,TheatreCode1 = TH_CODE
	,InactiveFlag = TH_INACTIVE
	,Colour = 'Gray'
from
	[$(otprd)].dbo.FTHEAT NewTheatre
where
	not exists
		(
		select
			1
		from
			Theatre.Theatre
		where
			Theatre.TheatreCode = NewTheatre.TH_SEQU
		)


truncate table Theatre.OperatingSuite

insert into Theatre.OperatingSuite
(
	 OperatingSuiteCode
	,OperatingSuite
	,OperatingSuiteCode1
	,InactiveFlag
)
select
	 OperatingSuiteCode = UN_SEQU
	,OperatingSuite = UN_DESC
	,OperatingSuiteCode1 = UN_CODE
	,InactiveFlag = UN_INACTIVE
from
	[$(otprd)].dbo.FUNIT OperatingSuite



truncate table Theatre.Staff

insert into Theatre.Staff
(
	 StaffCode
	,Surname
	,Forename
	,Initial
	,StaffCode1
	,StaffCategoryCode
	,SpecialtyCode
	,NationalStaffCode
      
)
select
	 StaffCode = SU_SEQU
	,Surname = SU_LNAME
	,Forename = SU_FNAME
	,Initial = SU_INITIAL
	,StaffCode1 = SU_CODE
	,StaffCategoryCode = SU_SC_SEQU
	,SpecialtyCode = SU_S1_SEQU
	,NationalStaffCode = nullif(SU_MRB_NUM,'')
from
	[$(otprd)].dbo.FSURGN Staff



truncate table Theatre.StaffCategory

insert into Theatre.StaffCategory
(
	 StaffCategoryCode
	,StaffCategoryCode1
	,StaffCategory
	,SurgeonFlag
	,InactiveFlag
	,ConsultantFlag
	,SupervisionFlag
	,AnaesthetistFlag
	,OtherFlag
	,NurseFlag
	,PorterFlag
	,Technician1Flag
	,Technician2Flag
	,ClerkFlag
)
select
	 StaffCategoryCode = SC_SEQU
	,StaffCategoryCode1 = SC_CODE
	,StaffCategory = SC_DESC
	,SurgeonFlag = SC_SURGEON
	,InactiveFlag = SC_INACTIVE
	,ConsultantFlag = SC_IS_CONSULT
	,SupervisionFlag = SC_SUPERVISION
	,AnaesthetistFlag = SC_ANAESTHETIST
	,OtherFlag = SC_OTHER
	,NurseFlag = SC_NURSE
	,PorterFlag = SC_PORTER
	,Technician1Flag = SC_TECHNICIAN_1
	,Technician2Flag = SC_TECHNICIAN_2
	,ClerkFlag = SC_CLERK
from
	[$(otprd)].dbo.FSCAT StaffCategory



truncate table Theatre.Specialty

insert into Theatre.Specialty
(
	 SpecialtyCode
	,Specialty
	,SpecialtyCode1
	,InactiveFlag
)
select
	 SpecialtyCode = S1_SEQU
	,Specialty = S1_DESC
	,SpecialtyCode1 = S1_CODE
	,InactiveFlag = S1_INACTIVE
from
	[$(otprd)].dbo.FS1SPEC Specialty



truncate table Theatre.Anaesthetic

insert into Theatre.Anaesthetic
(
	 AnaestheticCode
	,Anaesthetic
	,AnaestheticCode1
	,InactiveFlag
)
select
	 AnaestheticCode = ANA_SEQU
	,Anaesthetic = ANA_DESCRIPTION
	,AnaestheticCode1 = ANA_CODE
	,InactiveFlag = ANA_INACTIVE
from
	[$(otprd)].dbo.F_Anaesthetic Anaesthetic



truncate table Theatre.AdmissionType

insert into Theatre.AdmissionType
(
	 AdmissionTypeCode
	,AdmissionType
	,AdmissionTypeCode1
	,InactiveFlag
)
select
       AdmissionTypeCode = TA_SEQU
      ,AdmissionType = TA_DESCRIPTION
      ,AdmissionTypeCode1 = TA_CODE
      ,InactiveFlag = TA_INACTIVE
from
	[$(otprd)].dbo.F_Type_of_Admission AdmissionType


truncate table Theatre.ASAScore

insert into Theatre.ASAScore
(
	 ASAScoreCode
	,ASAScore
	,ASAScoreCode1
	,InactiveFlag
)
select
       ASAScoreCode = ASA_SEQU
      ,ASAScore = ASA_DESCRIPTION
      ,ASAScoreCode1 = ASA_CODE
      ,InactiveFlag = ASA_INACTIVE
from
	[$(otprd)].dbo.F_Asa ASAScore


truncate table Theatre.Ward

insert into Theatre.Ward
(
	 WardCode
	,Ward
	,WardCode1
	,InactiveFlag
)
select
	 WardCode = WA_SEQU
	,Ward = WA_DESC
	,WardCode1 = WA_CODE
	,InactiveFlag = WA_INACTIVE
from
	[$(otprd)].dbo.FWARD Ward



truncate table Theatre.Operation

insert into Theatre.Operation
(
	 OperationCode
	,Operation
	,OperationCode1
	,InactiveFlag
)
select
	 OperationCode = CD_SEQU
	,Operation = CD_DESCRIPTION
	,OperationCode1 = CD_CODE
	,InactiveFlag = CD_INACTIVE
from
	[$(otprd)].dbo.FCDOPER Operation


truncate table Theatre.CancelReason

insert into Theatre.CancelReason
(
	 CancelReasonCode
	,CancelReason
	,CancelReasonCode1
	,CancelReasonGroupCode
	,InactiveFlag
)
select
	 CancelReasonCode = CN_SEQU
	,CancelReason = CN_DESC
	,CancelReasonCode1 = CN_CODE
	,CancelReasonGroupCode = CN_CG_SEQU
	,InactiveFlag = CN_INACTIVE
from
	[$(otprd)].dbo.FCANCEL CancelReason



truncate table Theatre.CancelReasonGroup

insert into Theatre.CancelReasonGroup
(
	 CancelReasonGroupCode
	,CancelReasonGroup
	,CancelReasonGroupCode1
	,InactiveFlag
)
select
	 CancelReasonGroupCode = CG_SEQU
	,CancelReasonGroup = CG_DESC
	,CancelReasonGroupCode1 = CG_CODE
	,InactiveFlag = CG_INACTIVE
from
	[$(otprd)].dbo.F_Cancel_Group CancelReasonGroup



truncate table Theatre.UnplannedReturnReason

insert into Theatre.UnplannedReturnReason
(
	 UnplannedReturnReasonCode
	,UnplannedReturnReason
	,InactiveFlag

)
select
	 UnplannedReturnReasonCode = UP_CODE
	,UnplannedReturnReason = UP_DESC
	,InactiveFlag = UP_INACTIVE
from
	[$(otprd)].dbo.F_Unplanned


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadTheatreReferenceData', @Stats, @StartTime



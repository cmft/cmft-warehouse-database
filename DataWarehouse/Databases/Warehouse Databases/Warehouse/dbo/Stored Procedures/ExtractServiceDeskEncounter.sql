﻿

CREATE PROCEDURE [dbo].[ExtractServiceDeskEncounter]

AS

DECLARE @StartTime datetime
DECLARE @Elapsed int
DECLARE @RowsDeleted Int
DECLARE @RowsInserted Int
DECLARE @Stats varchar(255)
DECLARE @lastmodifiedTime datetime

SELECT @StartTime = getdate()

TRUNCATE TABLE dbo.TImportServiceDeskEncounter

INSERT INTO dbo.TImportServiceDeskEncounter

SELECT 
	EncounterRecNo = calls.SER_ID,
	SourceUniqueID = calls.SER_OID,
	Request = calls.SER_DESCRIPTION , 
	StartTime = calls.SER_ACTUALSTART, 
	EndTime = calls.SER_ACTUALFINISH, 
	DeadlineTime = calls.SER_DEADLINE, 
	Category = repcodetxt.RCT_NAME, 
	Workgroup = workgroup.WOG_NAME,
	CreatedBy = person.PER_NAME, 
	AssignedTo = person2.PER_NAME, 
	CreatedTime = calls.REG_CREATED, 
	Prioroty = codeslocale.CDL_NAME,
	RequestStatus = repcodetxt2.RCT_NAME, 
	ResolvedTime = customfield.SCF_SCDATE3, 
	Template = template.TEM_NAME, 
	ExceededDeadlineFlag = customfield.SCF_BOOLEAN2,
	Classification = codeslocale2.CDL_NAME,
	ClosureDescription = codeslocale3.CDL_NAME, 
	AssignmentCount = customfield.SCF_SCNUMBER1,
	CallerFullName = person3.PER_NAME, 
	CallerEmail = person3.PER_EMAIL, 
	CallerOrganization = org.ORG_NAME1,
	SolutionNotes = sol.SCS_SOLUTION,
	History = customfield.SCF_TEXT64KB,
	Impact = codeslocale4.CDL_NAME

FROM 	
	[$(ServiceDeskDB)].dbo.ITSM_SERVICECALLS as calls 

INNER JOIN	[$(ServiceDeskDB)].dbo.REP_CODES as repcodes
ON	calls.SER_CAT_OID = repcodes.RCD_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.REP_CODES_TEXT as repcodetxt
ON	repcodes.RCD_OID = repcodetxt.RCT_RCD_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.ITSM_WORKGROUPS as workgroup
ON	calls.SER_ASS_WOG_OID = workgroup.WOG_OID

LEFT OUTER JOIN	[$(ServiceDeskDB)].dbo.ITSM_PERSONS as person
ON	calls.REG_CREATED_BY_OID = person.PER_ACC_OID

LEFT OUTER JOIN	[$(ServiceDeskDB)].dbo.ITSM_PERSONS as person2
ON	calls.SER_ASS_PER_TO_OID = person2.PER_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.ITSM_CODES as codes
ON	calls.SER_PRI_OID = codes.COD_OID

INNER JOIN 	[$(ServiceDeskDB)].dbo.ITSM_CODES_LOCALE as codeslocale
ON	codes.COD_OID = codeslocale.CDL_COD_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.REP_CODES as repcodes2
ON	calls.SER_STA_OID = repcodes2.RCD_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.REP_CODES_TEXT as repcodetxt2
ON	repcodes2.RCD_OID = repcodetxt2.RCT_RCD_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.ITSM_SER_CUSTOM_FIELDS as customfield
ON	calls.SER_OID = customfield.SCF_SER_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.REP_TEMPLATES as template
ON	calls.SER_TEM_OID = template.TEM_OID

INNER JOIN	[$(ServiceDeskDB)].dbo.ITSM_CODES as codes2
ON	calls.SER_CLA_OID = codes2.COD_OID

INNER JOIN 	[$(ServiceDeskDB)].dbo.ITSM_CODES_LOCALE as codeslocale2
ON	codes2.COD_OID = codeslocale2.CDL_COD_OID

LEFT OUTER JOIN	[$(ServiceDeskDB)].dbo.ITSM_PERSONS as person3
ON	calls.SER_CALLER_PER = person3.PER_OID

LEFT OUTER JOIN	[$(ServiceDeskDB)].dbo.ITSM_CODES as codes3
ON	calls.SER_CLO_OID = codes3.COD_OID

LEFT OUTER JOIN	 	[$(ServiceDeskDB)].dbo.ITSM_CODES_LOCALE as codeslocale3
ON	codes3.COD_OID = codeslocale3.CDL_COD_OID

LEFT OUTER JOIN	[$(ServiceDeskDB)].dbo.ITSM_SER_SOLUTION as sol
ON		calls.SER_OID = sol.SCS_SER_OID

LEFT OUTER JOIN	[$(ServiceDeskDB)].dbo.ITSM_ORGANIZATIONS as org
ON	calls.SER_CALLER_ORG = org.ORG_OID

LEFT OUTER JOIN	[$(ServiceDeskDB)].dbo.ITSM_CODES as codes4
ON	calls.SER_IMP_OID = codes4.COD_OID

LEFT OUTER JOIN	 	[$(ServiceDeskDB)].dbo.ITSM_CODES_LOCALE as codeslocale4
ON	codes4.COD_OID = codeslocale4.CDL_COD_OID

WHERE
	calls.SER_ACTUALSTART >='01/April/2011'
AND calls.SER_ACTUALSTART < dateadd(day, datediff(day, 0, getdate()), 0)
	
SELECT @RowsInserted = @@rowcount

SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 

EXEC WriteAuditLogEvent 'ExtractServiceDeskEncounter', @Stats, @StartTime
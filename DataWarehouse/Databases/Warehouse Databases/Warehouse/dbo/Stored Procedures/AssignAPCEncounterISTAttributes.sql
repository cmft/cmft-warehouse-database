﻿CREATE procedure [dbo].[AssignAPCEncounterISTAttributes] as

update APC.Encounter
set
	 ISTAdmissionSpecialtyCode =
		case
		when AdmissionSpecialty.SpecialtyCode like 'AE%'
		then
			coalesce(
				(
				select top 1
					SpecialtyCode
				from
					APC.Encounter
				where
					Encounter.ProviderSpellNo = APCEncounter.ProviderSpellNo
				and	left(Encounter.SpecialtyCode , 2) not in ( 'AE' , 'IH' )

				order by
					SourceEncounterNo
				)
				,AdmissionSpecialty.SpecialtyCode
			)
		else AdmissionSpecialty.SpecialtyCode
		end

	,ISTAdmissionDemandTime =
		coalesce(AEArrival.ArrivalTimeAdjusted , OPAppointment.AppointmentTime , APCEncounter.AdmissionTime)

	,ISTDischargeTime =
		coalesce(TransferToDischargeLounge.TransferToDischargeLoungeTime , APCEncounter.DischargeTime)


from
	APC.Encounter APCEncounter

-- get the admission FCE specialty
left join
	(
	select
		 ProviderSpellNo
		,SpecialtyCode
	from
		APC.Encounter Episode
	where
		not exists
			(
			select
				1
			from
				APC.Encounter EarlierEpisode
			where
				EarlierEpisode.ProviderSpellNo = Episode.ProviderSpellNo
			and	EarlierEpisode.EpisodeStartTime < Episode.EpisodeStartTime
			)
	) AdmissionSpecialty
on	AdmissionSpecialty.ProviderSpellNo = APCEncounter.ProviderSpellNo

-- get the A&E attendance that resulted in the admission
left join
	(
	select
		 APCEncounter.ProviderSpellNo
		,AEEncounter.ArrivalTimeAdjusted
	from
		APC.Encounter APCEncounter

	inner join AE.Encounter AEEncounter
	on	AEEncounter.DistrictNo = APCEncounter.DistrictNo
	and	AEEncounter.DepartureTime between dateadd(hour , -6 , APCEncounter.AdmissionTime) and dateadd(hour , 6 , APCEncounter.AdmissionTime)
	and	AEEncounter.ArrivalTime < APCEncounter.AdmissionTime
	and	not exists
			(
			select
				1
			from
				AE.Encounter LaterAEEncounter
			where
				LaterAEEncounter.DistrictNo = APCEncounter.DistrictNo
			and	LaterAEEncounter.DepartureTime between dateadd(hour , -6 , APCEncounter.AdmissionTime) and dateadd(hour , 6 , APCEncounter.AdmissionTime)
			and	LaterAEEncounter.ArrivalTime < APCEncounter.AdmissionTime
			and	LaterAEEncounter.AttendanceDisposalCode = '01'
			and LaterAEEncounter.SiteCode = 'RW3MR'
			and	LaterAEEncounter.ArrivalTime > AEEncounter.ArrivalTime
			)		

	where
		APCEncounter.AdmissionMethodCode = 'AE'
	and	APCEncounter.AdmissionTime = EpisodeStartTime
	and	APCEncounter.SiteCode = 'MRI'
	and	AEEncounter.AttendanceDisposalCode = '01'
	and AEEncounter.SiteCode = 'RW3MR'
	) AEArrival
on	AEArrival.ProviderSpellNo = APCEncounter.ProviderSpellNo

-- get the OP Appointment that resulted in the admission
left join
	(
	select
		 APCEncounter.ProviderSpellNo
		,OPEncounter.AppointmentTime
	from
		APC.Encounter APCEncounter

	inner join OP.Encounter OPEncounter
	on	OPEncounter.DistrictNo = APCEncounter.DistrictNo
	and OPEncounter.SiteCode = APCEncounter.SiteCode
	and	OPEncounter.AppointmentTime between dateadd(hour , -6 , APCEncounter.AdmissionTime) and APCEncounter.AdmissionTime
	and	not exists
			(
			select
				1
			from
				OP.Encounter LaterOPEncounter 
			where
				LaterOPEncounter.DistrictNo = APCEncounter.DistrictNo
			and	LaterOPEncounter.AppointmentTime between dateadd(hour , -6 , APCEncounter.AdmissionTime) and APCEncounter.AdmissionTime
			and LaterOPEncounter.SiteCode = APCEncounter.SiteCode
			and	LaterOPEncounter.AppointmentTime > OPEncounter.AppointmentTime
			)		

	where
		APCEncounter.AdmissionMethodCode = 'OP'
	and	APCEncounter.AdmissionTime = EpisodeStartTime
	and	APCEncounter.SiteCode = 'MRI'
	) OPAppointment
on	OPAppointment.ProviderSpellNo = APCEncounter.ProviderSpellNo

-- get the time the patient was transferred to the Discharge Lounge, if immediately prior to discharge
left join
	(
	select
		 ProviderSpellNo
		,TransferToDischargeLoungeTime = StartTime
	from
		APC.WardStay
	where
		WardCode = 'DL'
	and	not exists
			(
			select
				1
			from
				APC.WardStay LaterWardStay
			where
				LaterWardStay.ProviderSpellNo = WardStay.ProviderSpellNo
			and	LaterWardStay.StartTime > WardStay.StartTime
			)
	) TransferToDischargeLounge
on	TransferToDischargeLounge.ProviderSpellNo = APCEncounter.ProviderSpellNo


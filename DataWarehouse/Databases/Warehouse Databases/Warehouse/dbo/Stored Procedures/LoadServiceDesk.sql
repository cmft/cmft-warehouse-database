﻿

CREATE PROCEDURE [dbo].[LoadServiceDesk]

AS

DECLARE @StartTime datetime
DECLARE @Elapsed int
DECLARE @Stats varchar(255)

SELECT @StartTime = getdate()

EXEC dbo.ExtractServiceDeskEncounter
EXEC dbo.LoadServiceDeskEncounter

SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	 ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 

EXEC WriteAuditLogEvent 'LoadServiceDesk', @Stats, @StartTime
﻿
create proc dbo.ExtractMedisecInpatientRequest

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table dbo.TImportMedisecInpatientRequest

insert into dbo.TImportMedisecInpatientRequest

select [ireqpn]
      ,[ireqdoa]
      ,[ireqfb]
      ,[ireqcons]
      ,[ireqtoa]
      ,[ireqdischdate]
      ,[ireqsurn]
      ,[ireqinits]
      ,[ireqhosp]
      ,[ireqward]
      ,[ireqspec]
      ,[ireqestatus]
      ,[ireqdatol]
      ,[ireqcomment]
      ,[ireqlstatus]
      ,[ireqmailflag]
      ,[ireqmaildate]
      ,[ireqemailflag]
      ,[ireqemaildate]
      ,[ireqfaxflag]
      ,[ireqfaxdate]
      ,[ireqediflag]
      ,[ireqedidate]
      ,[ireqcn]
      ,[ireqeps]
from [$(CMFT_MEDISEC)].dbo.InpRequest

select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


﻿
CREATE PROCEDURE [dbo].[ExtractCOMHealthOrg]
	-- @from smallTime
	--,@to smallTime
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

--DECLARE @from smalldatetime = NULL
--DECLARE @to smalldatetime = NULL
--set @from = '04/01/2011'
--set @to = '04/02/2011'


TRUNCATE TABLE dbo.TImportCOMHealthOrg

INSERT INTO dbo.TImportCOMHealthOrg
(
	 HealthOrgID--[HEORG_REFNO]	
	,HealthOrgTypeID--[HEORG_REFNO]--[HEORG_REFNO]
	,HealthOrgCode--[MAIN_IDENT]	
	,HealthOrg--[DESCRIPTION]	
	,HealthOrgParentID--[PARNT_REFNO]	
	,StartTime--[START_DTTM]	
	,EndTime--[END_DTTM]
	,ArchiveFlag--[ARCHV_FLAG]
	,HealthOrgOwnerID--[OWNER_HEORG_REFNO]	 
) 

SELECT
	 HealthOrgID = HEORG_REFNO	
	,HealthOrgTypeID = HOTYP_REFNO
	,HealthOrgCode = MAIN_IDENT	
	,HealthOrg = [DESCRIPTION]	
	,HealthOrgParentID = PARNT_REFNO	
	,StartTime = START_DTTM	
	,EndTime = END_DTTM
	,ArchiveFlag = ARCHV_FLAG
	,HealthOrgOwnerID = OWNER_HEORG_REFNO
FROM 
	--ipm.dbo.HEALTH_ORGANISATIONS_V02 HealthOrg
	[$(ipm)].dbo.HealthOrganisation HealthOrg

WHERE 
--	[OWNER_HEORG_REFNO] IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
--						  ,2031409 -- 5NT MANCHESTER PCT
--						  ,2001932) -- NWWM Cluster

	--AND 
	NOT EXISTS
		(
		SELECT
			1
		FROM
			--ipm.dbo.HEALTH_ORGANISATIONS_V02 LaterHealthOrg
			[$(ipm)].dbo.HealthOrganisation LaterHealthOrg
		WHERE
			--[OWNER_HEORG_REFNO] IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
			--				      ,2031409 -- 5NT MANCHESTER PCT
			--				      ,2001932) -- NWWM Cluster
			--AND
				LaterHealthOrg.HEORG_REFNO = HealthOrg.HEORG_REFNO
			AND	LaterHealthOrg.MODIF_DTTM > HealthOrg.MODIF_DTTM
		)


/*

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractCommunityEncounter', @Stats, @StartTime
*/

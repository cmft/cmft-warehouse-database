﻿


CREATE PROCEDURE [dbo].[LoadCOMReturnDM01Activity] 
(
	@Month INT = NULL,
	@UserId VARCHAR(255) = NULL
)

AS


SET XACT_ABORT OFF
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFORMAT dmy
SET NOCOUNT ON


DECLARE

	@MonthInternal INT
	

IF OBJECT_ID('Tempdb.dbo.#MDR_First_Activity') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_First_Activity
END

IF OBJECT_ID('Warehouse.COM.ReturnDM01Activity') IS NOT NULL BEGIN
	DROP TABLE COM.ReturnDM01Activity
END


SET DATEFORMAT dmy

SELECT
	fs.REFRL_REFNO,
	MIN(fs.START_DTTM) AS FirstAppt

INTO #MDR_First_Activity

FROM
	[$(ipm)].dbo.Schedule fs
LEFT JOIN [$(ipm)].dbo.TestPatient tp
	ON fs.PATNT_REFNO = tp.PATNT_REFNO
	
WHERE
		tp.PATNT_REFNO IS NULL -- exclude test patients
	AND
		fs.ARCHV_FLAG = 'N'
	AND
		(
				fs.SCTYP_REFNO = 1468
			OR
				fs.SCTYP_REFNO = 1470
		)
	AND
		( --extracting attendances only
				(
						fs.SCTYP_REFNO = '1470'					-- Clinic Contact
					AND
						fs.ATTND_REFNO IN (
							357,								-- Attended On Time
							2868								-- Patient Late / Seen
						)
				)
			OR
				(
						fs.SCTYP_REFNO = '1468'					-- Community Contact
					AND
						fs.SCOCM_REFNO NOT IN (
							1459,								-- Patient Died
							2001651,							-- Cancelled
							1457								-- Did Not Attend
						)
					AND
						fs.ARRIVED_DTTM IS NOT NULL
					AND
						fs.DEPARTED_DTTM IS NOT NULL
				)
		)
	AND
		fs.CONTY_REFNO <> 2004177
		
	--restrict to the subset of data we are interested in
	AND 							
		fs.SPECT_REFNO in ('2000820','150000117') -- Audiology, New Born Hearing Screening
	
GROUP BY
	fs.REFRL_REFNO


--*******************************************************************************
--Pre production - Evaluate First / New activity for the set of referrals - END
--*******************************************************************************

SELECT            								
	
	ReturnPeriod = (YEAR(s.START_DTTM) * 100) + MONTH (s.START_DTTM)
	
	,LoadDate = CONVERT(Date,(CURRENT_TIMESTAMP),103)
	
	,[Return] = 'DM01'
	
	,ReturnItem = 'DM01Activity'
	
	,InterfaceCode = 'ipm'
	
	,SourceUniqueNo = s.SCHDL_REFNO
	
	,ItemDate = CONVERT(date, s.[start_dttm],103)
	
	,ItemTime = CONVERT(smalldatetime, s.[start_dttm],103)
				
	,ItemSpecialty = spec.[description]
	
	,NHSNumber = p.nhs_identifier
	
	,ReferredByProfessionalCarer = ISNULL(prof1.forename, '') + ' ' + ISNULL(prof1.surname,'')						
	
	,ScheduleType =  CASE							
						WHEN s.SCTYP_REFNO = '1468' THEN 'Community Contact'						
						WHEN s.SCTYP_REFNO = '1470' THEN 'Clinic Contact'						
						ELSE 'Other'						
					END							
		
	,StaffTeam  = t.[description]	
						
	,SeenByProfessionalCarer = ISNULL(prof.forename, '') + ' ' + ISNULL(prof.surname, '')
	
	,PatientAge =    CASE					
						WHEN (MONTH(p.DTTM_OF_BIRTH) * 100 ) + DAY(p.DTTM_OF_BIRTH) <= (MONTH(r.RECVD_DTTM) * 100 ) + DAY(r.RECVD_DTTM)				
							THEN DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM)			
						ELSE DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM) - 1				
					END			
								
	,Clinic = c.description
							
	,ReferralSource = ISNULL(rfval1.[description], 'Unknown')

	,AppointmentOutcome = CASE s.SCTYP_REFNO							
								WHEN 1468 -- Community Contact						
									THEN	CASE				
												WHEN s.SCOCM_REFNO = 1457 THEN 'DNA'		
												WHEN s.SCOCM_REFNO = 1459 THEN 'Patient deceased'		
												WHEN s.SCOCM_REFNO = 2001651 THEN 'Cancelled'		
												WHEN		
														MONTH(s.ARRIVED_DTTM) IS NOT NULL
													AND	
														MONTH(s.DEPARTED_DTTM) IS NOT NULL THEN 'Attended'
												WHEN		
														MONTH(s.ARRIVED_DTTM) IS NULL
													OR	
														MONTH(s.DEPARTED_DTTM) IS NULL THEN 'Not Recorded'
												ELSE 'Other'		
											END			
								WHEN 1470 -- Clinic Contact						
									THEN	CASE				
												WHEN s.ATTND_Refno IN (		
													357,	--Attended On Time
													2868	--Patient Late / Seen
												) THEN 'Attended'		
												WHEN s.ATTND_Refno IN (		
													2870,	
													2003532,	
													2004528,	
													2004300,	
													2004301,	
													3006508	
												) THEN 'Cancelled'		
												WHEN s.ATTND_Refno IN (		
													358,	
													2000724,	
													2003494,	
													2003495	
												) THEN 'DNA'		
												WHEN s.ATTND_Refno = '45' THEN 'Not Recorded'		
												ELSE 'Other'		
											END			
							END

	,ActivityType =  CASE
					--	WHEN s.START_DTTM = fa.FirstAppt THEN 'New'
						WHEN s.START_DTTM > fa.FirstAppt THEN 'Follow-Up'
						ELSE 'New'
					END 

	,GPPracticeCode = ISNULL(ProfessionalCarerBase.HealthOrgCode, 'V81999')
	,GPPractice = ISNULL(ProfessionalCarerBase.HealthOrgDescription, 'GP Practice Code not known') 

	,CommissionerCode = ISNULL(ProfessionalCarerBase.ParentHealthOrgCode, 'V81999')
	,Commissioner = ISNULL(ProfessionalCarerBase.ParentHealthOrgDescription, 'GP Practice Code not known')  
  

INTO warehouse.COM.ReturnDM01Activity							
								
FROM								
	[$(ipm)].dbo.Schedule s							
left join [$(ipm)].dbo.Patient p								
on s.PATNT_REFNO = p.PATNT_REFNO								
								
left join [$(ipm)].dbo.Referral r								
on s.REFRL_REFNO = r.REFRL_REFNO								
								
left join [$(ipm)].dbo.ReferenceValue rfval1								
on r.SORRF_REFNO = rfval1.RFVAL_REFNO													
								
LEFT JOIN [$(ipm)].dbo.Specialty spec								
	ON s.SPECT_REFNO = spec.SPECT_REFNO							
								
left join [$(ipm)].dbo.StaffTeam t								
on s.STEAM_REFNO = t.STEAM_REFNO														
					
left join [$(ipm)].dbo.ServicePoint	c							
	on s.SPONT_REFNO = c.SPONT_REFNO	

LEFT JOIN [$(ipm)].dbo.ProfessionalCarer prof1
 on r.REFBY_PROCA_REFNO = prof1.PROCA_REFNO							
								
LEFT JOIN [$(ipm)].dbo.ProfessionalCarer prof								
      ON								
            CASE								
                  WHEN s.SCTYP_REFNO = 1468 THEN s.PROCA_REFNO								
                  WHEN s.SCTYP_REFNO = 1470 THEN ISNULL(s.SEENBY_PROCA_REFNO, s.PROCA_REFNO)								
            END = prof.PROCA_REFNO								
LEFT JOIN (								
	SELECT 							
		s.REFRL_REFNO						
	FROM							
		[$(ipm)].dbo.Schedule s						
	INNER JOIN [$(ipm)].dbo.Patient p							
		ON						
				s.PATNT_REFNO = p.PATNT_REFNO				
			AND					
				p.ARCHV_FLAG = 'N'   				
	WHERE							
			s.REFRL_REFNO IS NOT NULL					
		AND						
			(					
					LEFT(p.NHS_IDENTIFIER, 3) = '999'			
				OR				
					p.FORENAME  in ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')			
				OR				
					p.SURNAME  in ('xxtestpatientaavp','xxtestpatientaafe')			
			)					
	GROUP BY							
		s.REFRL_REFNO						
) ex -- list of schedules for test patients								
	ON s.REFRL_REFNO = ex.REFRL_REFNO	
	
LEFT JOIN #MDR_First_Activity fa
	ON s.REFRL_REFNO = fa.REFRL_REFNO
	
--P.McNulty 1.0.3. see below. Add join from Referrals direct to Staff Teams table 		
left join [$(ipm)].dbo.StaffTeam rst
on r.REFTO_STEAM_REFNO = rst.STEAM_REFNO

--P.McNulty 1.0.3. join to GP Practice and PCT

LEFT OUTER JOIN
(
	SELECT DISTINCT
			 EncounterPatientProfessionalCarer.SCHDL_REFNO
			,EncounterPatientProfessionalCarer.PATPC_REFNO
			,ProfessionalCarerCode
			,HealthOrgCode
			,HealthOrgDescription
			,ParentHealthOrgCode
			,ParentHealthOrgDescription


	FROM
			(
			SELECT 
				 SCHDL_REFNO
				,MAX(PATPC_REFNO) AS PATPC_REFNO

			FROM 
				(
				SELECT 
						 SCHDL_REFNO
						,START_DTTM
						,PATNT_REFNO
				 FROM 
						[$(ipm)].dbo.Schedule Encounter
				)Encounter

			LEFT OUTER JOIN

				(
				SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO

				FROM 
						[$(ipm)].dbo.PatientProfessionalCarer 
						
				WHERE   
						ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarer

			ON Encounter.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
			AND PatientProfCarer.START_DTTM <= Encounter.START_DTTM 
			AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > Encounter.START_DTTM)

			GROUP BY 
				SCHDL_REFNO

	)EncounterPatientProfessionalCarer
		

	LEFT OUTER JOIN (SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO
						,PatientProfessionalCarer.PROCA_MAIN_IDENT AS ProfessionalCarerCode

					FROM 
						[$(ipm)].dbo.PatientProfessionalCarer
						
					WHERE   
						 ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarerDetail
	ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO


	LEFT OUTER JOIN (	
						SELECT DISTINCT
							 HealthOrganisation.HEORG_REFNO
							 ,HealthOrganisation.[DESCRIPTION] AS HealthOrgDescription
							 ,HealthOrganisation.MAIN_IDENT AS HealthOrgCode
							 ,HealthOrganisation.PARNT_REFNO
						 FROM [$(ipm)].dbo.HealthOrganisation 
						 WHERE ARCHV_FLAG = 'N'
		
					) PatientProfCarerHealthOrg
							 
	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
	
		LEFT OUTER JOIN 
		(	
		SELECT DISTINCT
			  HealthOrganisation.HEORG_REFNO ParentHEORG_REFNO
			 ,HealthOrganisation.[DESCRIPTION] AS ParentHealthOrgDescription
			 ,HealthOrganisation.MAIN_IDENT AS ParentHealthOrgCode
			 ,HealthOrganisation.HOTYP_REFNO AS ParentHealthOrgCodeType
		 FROM [$(ipm)].dbo.HealthOrganisation 
		 WHERE ARCHV_FLAG = 'N'
		   AND HOTYP_REFNO = 629
		) ParentPatientProfCarerHealthOrg

	ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
	
)ProfessionalCarerBase
ON s.SCHDL_REFNO = ProfessionalCarerBase.SCHDL_REFNO
							
WHERE								
		ex.REFRL_REFNO IS NULL -- exclude test patients		
	AND			
		CONVERT(DATE,s.START_DTTM, 103) >= '1 Mar 2012' AND CONVERT(DATE,s.START_DTTM,103) <= '31 Mar 2012'					
		--CONVERT(DATETIME,s.START_DTTM, 103) > dbo.fn_FirstOfPreviousMonth(getdate()) AND s.START_DTTM <= dbo.fn_FirstofMonth(getdate())			
	AND							
		s.ARCHV_FLAG = 'N'						
	AND							
		s.SCTYP_REFNO in  ('1470', '1468')
	AND
		s.spect_refno = '2000820'
	AND 
		s.CONTY_REFNO <> '2004177'

	--1.0.3 replace spont_refno criteria with reference to Service Points table
	AND (
			c.[DESCRIPTION] not like 'NHSP%'
		OR
			c.[DESCRIPTION] is null
		)
	
	--replace r.REFTO_STEAM_REFNO criteria to dbo.[STAFF_TEAMS_V01]
	
	AND (
			(
				r.REFTO_STEAM_REFNO not in 
											(
											 '10003232', 
											 '10003233', 
											 '150000125', 
											 '150000126'
											 )
			AND
				rst.archv_flag = 'N'
			)
		OR
			r.REFTO_STEAM_REFNO is null
		)

	--remove Tier 2 ENT records

	AND ( 
			s.spont_Refno NOT in ('10009801',
						'10009803',
						'10009800',
						'10009802')
			OR 					
			s.spont_Refno is null
		)	

--delete cancellation / DNA
delete from COM.ReturnDM01Activity
where AppointmentOutcome in ('Cancelled','DNA')



﻿CREATE procedure [dbo].[LoadAEDiagnosis] @from smalldatetime, @to smalldatetime as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

delete from AE.Diagnosis
where
	exists
	(
	select
		1
	from
		AE.Encounter
	where
		coalesce(Encounter.ArrivalDate, @from) between @from and @to
	and	Encounter.SourceUniqueID = Diagnosis.AESourceUniqueID
	)

select @RowsDeleted = @@rowcount

delete from AE.Diagnosis
where
	not exists
	(
	select
		1
	from
		AE.Encounter
	where
		Encounter.SourceUniqueID = Diagnosis.AESourceUniqueID
	)

select @RowsDeleted = @RowsDeleted + @@rowcount

insert into AE.Diagnosis
(
	 SourceUniqueID
	,SequenceNo
	,DiagnosticSchemeInUse
	,DiagnosisCode
	,AESourceUniqueID
	,SourceDiagnosisCode
	,SourceSequenceNo
	,DiagnosisSiteCode
	,DiagnosisSideCode
)
select
	 SourceUniqueID
	,SequenceNo
	,DiagnosticSchemeInUse
	,DiagnosisCode
	,AESourceUniqueID
	,SourceDiagnosisCode
	,SourceSequenceNo
	,DiagnosisSiteCode
	,DiagnosisSideCode
from
	dbo.TLoadAEDiagnosis

select @RowsInserted = @@rowcount

--populate the AEEncounter diagnosis codes
update
	AE.Encounter
set
	DiagnosisCodeFirst = 
	(
	select
		AEDiagnosis.DiagnosisCode
	from
		dbo.TLoadAEDiagnosis AEDiagnosis
	where
		AEDiagnosis.AESourceUniqueID = Encounter.SourceUniqueID
	and	AEDiagnosis.SequenceNo = 1
	)

	,DiagnosisCodeSecond = 
	(
	select
		AEDiagnosis.DiagnosisCode
	from
		dbo.TLoadAEDiagnosis AEDiagnosis
	where
		AEDiagnosis.AESourceUniqueID = Encounter.SourceUniqueID
	and	AEDiagnosis.SequenceNo = 2
	)
from
	dbo.TLoadAEDiagnosis AEDiagnosis
where
	AEDiagnosis.AESourceUniqueID = Encounter.SourceUniqueID


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadAEDiagnosis', @Stats, @StartTime

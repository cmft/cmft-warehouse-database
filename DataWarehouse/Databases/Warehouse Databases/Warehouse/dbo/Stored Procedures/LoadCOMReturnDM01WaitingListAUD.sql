﻿

CREATE PROCEDURE [dbo].[LoadCOMReturnDM01WaitingListAUD] 
AS


SET XACT_ABORT OFF
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFORMAT dmy
SET NOCOUNT ON

--DECLARE @censusdate date = '01/September/2011'
--P.McNulty 25/05/2012. Script add more data to existing ReturnDM01WaitingList
--table. This script will be scheduled to run daily as an interim solution
--until it is superceded by later development.
/****** Script for latest waitinglist position  ******/

--P.McNulty 07/06/2012 - sort order in pivot to be by Weeks To TCI
--this is formatted as varchar...add leading zeroes to make those sort correctly
--i.e. pad with '0' when the length is less than 2...see below

--P.McNulty 27/07/2012 - more adjustments after meeting with Audiology.
--delete 'TR2 ENT' records

--P.McNulty 27/07/2012 - more adjustments after meeting with Audiology.
--sort order in new pivot worksheet to be by Weeks Waiting
--this is formatted as varchar...add leading zeroes to make those sort correctly
--i.e. pad with '0' when the length is less than 2...see below

--P.McNulty 20/08/2012 - In the Pivot, Service Point has been replaced by Clinic
--for the Row Labels definition as per spec. However, Clinic is only populated 
--where there is a schedule present. Add conditional logic so that if no Clinic
--is available we will use the field service point. (We do not want to alter those
--records where both fields are present and Clinic is a different value from Service
--Point... 


declare @EndDate date =  dateadd(d, -1,getdate())

IF OBJECT_ID (N'[COM].ReturnDM01WaitingListAud', N'U') IS NOT NULL DROP TABLE [COM].ReturnDM01WaitingListAud

CREATE TABLE [COM].[ReturnDM01WaitingListAud](
	[SourceUniqueNo] [numeric](10, 0) NULL,
	[ItemDate] [date] NULL,
	[DaysWaiting] int NULL,
	[WeeksWaiting] [varchar](50) NULL,
	[ItemTime] [smalldatetime] NULL,
	[ItemSpecialty] [varchar](50) NULL,
	[NHSNumber] [varchar](20) NULL,
	[ReferredByProfessionalCarer] [varchar](255) NULL,
	[ScheduleType] [varchar](255) NULL,
	[StaffTeam] [varchar](50) NULL,
	[PatientAge] int NULL,
	[Clinic] [varchar](100) NULL,
	[ReferralSource] [varchar](80) NULL,
	[AppointmentOutcome] [varchar](50) NULL,
	[ReferralClosedDate] [datetime] NULL,
	[FirstContact] [datetime] NULL,
	[AppointmentSpecialty] [varchar](50) NULL,
	[RecentActivity] [varchar](1)  NULL,
	[StatusType] [varchar](50) NULL,
	[StartDate] [datetime] NULL,
	[DaysWaiting2] int NULL,
	[Weeks To TCI] [varchar](255) NULL,
	[EncounterOutcome] [varchar](25) NULL ,
	--[AttendedID] [numeric](10, 0) NULL,
	--[OutcomeID] [varchar](30) NULL,
	[ServicePoint] [varchar](80) NULL, 
	[Comment] [varchar](255) NULL
) ON [PRIMARY]


exec dbo.LoadCOMReturnDM01WaitingList @EndDate

INSERT INTO [COM].[ReturnDM01WaitingListAud]

SELECT DISTINCT
        WL.SourceUniqueNo
       ,WL.ItemDate
       ,DaysWaiting = datediff(Day,ItemDate,@EndDate)
       --,WeeksWaiting = cast(ceiling(datediff(Day,ItemDate,@EndDate)/7) as varchar) + ' - ' + cast((ceiling(datediff(Day,ItemDate,@EndDate)/7)+1) as varchar)
       
       ,
       case 
       when datediff(Day,ItemDate,@EndDate) < 70 --week band < 10
       then
			  Right('0' + cast(ceiling(datediff(Day,ItemDate,@EndDate)/7) as varchar), 2)
			+ ' - ' 
			+ Right('0' + cast((ceiling(datediff(Day,ItemDate,@EndDate)/7)+1) as varchar), 2)
	   else
			cast(ceiling(datediff(Day,ItemDate,@EndDate)/7) as varchar) 
			+ ' - ' 
			+ cast((ceiling(datediff(Day,ItemDate,@EndDate)/7)+1) as varchar)
	   end		
		as 'WeeksWaiting'
       
       ,WL.ItemTime
       ,WL.ItemSpecialty
       ,WL.NHSNumber
       ,WL.ReferredByProfessionalCarer
       ,WL.ScheduleType
       ,WL.StaffTeam
       ,WL.PatientAge
       ,WL.Clinic
       ,WL.ReferralSource
       ,WL.AppointmentOutcome
       ,WL.ReferralClosedDate
       ,WL.FirstContact
       ,WL.AppointmentSpecialty
       ,WL.RecentActivity
       ,WL.StatusType
       ,Encounter.StartDate
       ,DaysWaitingEncounter = datediff(Day,ItemDate,Encounter.StartDate)
       
       ,
       case 
       when datediff(Day,ItemDate,Encounter.StartDate) < 70 --week band < 10
       then
			  Right('0' + cast(ceiling(datediff(Day,ItemDate,Encounter.StartDate)/7) as varchar), 2)
			+ ' - ' 
			+ Right('0' + cast((ceiling(datediff(Day,ItemDate,Encounter.StartDate)/7)+1) as varchar), 2)
	   else
			cast(ceiling(datediff(Day,ItemDate,Encounter.StartDate)/7) as varchar) 
			+ ' - ' 
			+ cast((ceiling(datediff(Day,ItemDate,Encounter.StartDate)/7)+1) as varchar)
	   end		
		as 'WeeksToTCI'
       
       ,EncounterOutcomeCode
       --,AttendedID
       --,OutcomeID
       ,ServicePoint.ServicePoint
       ,Encounter.Comment
       
FROM 
       COM.ReturnDM01WaitingList WL
INNER JOIN COM.Referral Ref
       ON WL.SourceUniqueNo = Ref.SourceUniqueID
AND RecordStatus = 'C'
LEFT OUTER JOIN COM.Encounter Encounter
       ON Ref.SourceUniqueID = Encounter.ReferralID
       AND Encounter.RecordStatus = 'C'
       AND Encounter.StartDate > @EndDate
LEFT OUTER JOIN COM.ServicePoint
       ON Encounter.ServicePointID = ServicePoint.ServicePointID
WHERE
       WL.LoadDate = (Select max(LoadDate) from COM.ReturnDM01WaitingList)
       --P.McNulty 25/05/2012 add comment field 
ORDER BY 
       -- WL.SourceUniqueNo,
       --datediff(Day,ItemDate,Encounter.StartDate) 
	   datediff(Day,ItemDate,Encounter.StartDate) desc
	   
	  
	  
--P.McNulty 27/07/2012. Delete spurious records
delete from COM.ReturnDM01WaitingListAud
where AppointmentSpecialty like '%TR2%'
--2000820	Audiological Medicine
--150000111	TR2 ENT

--P.McNulty 20/08/2012 - In the Pivot, Service Point has been replaced by Clinic
--for the Row Labels definition as per spec. However, Clinic is only populated 
--where there is a schedule present. Conditionally set Clinic as Service Point
--where the former is not set - because there is no schedule attached to the referral
UPDATE COM.ReturnDM01WaitingListAud 
SET Clinic = left(Clinic, 50)

UPDATE COM.ReturnDM01WaitingListAud 
SET Clinic = left(ServicePoint, 50)
WHERE 
(
	Clinic is null
	or
	len(Clinic) = 0
)
 


﻿
CREATE procedure [dbo].[AssignEDDSpellsDirectorateCode] as

update
	EDD.Spells
set
	DirectorateCode = Activity.DirectorateCode
from
	(
	select
		  Encounter.SourceEncounterNo
		 ,Encounter.SourceSpellNo
		 ,DivisionRuleBase.DirectorateCode
	from
		EDD.Spells Encounter

	left outer join PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Encounter.SourceAdmissionMethodCode

	left outer join PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = Encounter.SourceAPCManagementIntentionCode

	inner join WH.DivisionRuleBase
	on	DivisionRuleBase.SiteCode = Encounter.SourceAPCSiteCode
	and	DivisionRuleBase.SpecialtyCode = Encounter.CurrentSpecialty

	and	
		(
			case
			
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'3' --INTERVAL ADMISSION
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective


			--Elective Inpatients where intended management was daycase but patient stayed overnight
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode IN ('2','4','5') -- Regular day and night '2' -- DAY CASE
				and	Encounter.AdmitDate < Encounter.DischargeDate
				then 'EL'--Elective


			--Elective Daycase
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode = '2'-- DAY CASE
				and	Encounter.AdmitDate = Encounter.DischargeDate
				then 'DC'--Daycase
				
			
			--Elective Inaptient where intended management was daycase or regular but not discharged
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode IN ('2','4','5')-- DAY CASE, Regular day and night
				and	Encounter.DischargeDate IS NULL
				then 'EL'--Elective
	
	
			--Regular Day Case
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode IN ('4','5')-- Regular day and night
				and	Encounter.AdmitDate = Encounter.DischargeDate
				then 'RD'--Regular
				
			--Non Elective
				else 'NE' --Non Elective
			
			end
			= DivisionRuleBase.PatientCategoryCode

		or	DivisionRuleBase.PatientCategoryCode is null
		)

	and	(
			Encounter.CurrentConsulant = DivisionRuleBase.ConsultantCode
		or	DivisionRuleBase.ConsultantCode is null
		)

	and	(
			Encounter.CurrentWard = DivisionRuleBase.WardCode
		or	DivisionRuleBase.WardCode is null
		)

	and	not exists
		(
		select
			1
		from
			WH.DivisionRuleBase Previous
		where
			Previous.SiteCode = Encounter.SourceAPCSiteCode
		and	Previous.SpecialtyCode = Encounter.CurrentSpecialty

		and	
			(
				case
			
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'3' --INTERVAL ADMISSION
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective


			--Elective Inpatients where intended management was daycase but patient stayed overnight
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode IN ('2','4','5') -- Regular day and night '2' -- DAY CASE
				and	Encounter.AdmitDate < Encounter.DischargeDate
				then 'EL'--Elective


			--Elective Daycase
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode = '2'-- DAY CASE
				and	Encounter.AdmitDate = Encounter.DischargeDate
				then 'DC'--Daycase
				
			
			--Elective Inaptient where intended management was daycase or regular but not discharged
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode IN ('2','4','5')-- DAY CASE, Regular day and night
				and	Encounter.DischargeDate IS NULL
				then 'EL'--Elective
	
	
			--Regular Day Case
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode IN ('4','5')-- Regular day and night
				and	Encounter.AdmitDate = Encounter.DischargeDate
				then 'RD'--Regular
				
			--Non Elective
				else 'NE' --Non Elective
			
			end
				= Previous.PatientCategoryCode

			or	Previous.PatientCategoryCode is null
			)

		and	(
				Encounter.CurrentConsulant = Previous.ConsultantCode
			or	Previous.ConsultantCode is null
			)

		and	(
				Encounter.CurrentWard = Previous.WardCode
			or	Previous.WardCode is null
			)

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when Previous.WardCode is not null
				then 2
				else 0
				end +

				case
				when Previous.PatientCategoryCode is not null
				then 1
				else 0
				end

				>

				case
				when DivisionRuleBase.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when DivisionRuleBase.WardCode is not null
				then 2
				else 0
				end +

				case
				when DivisionRuleBase.PatientCategoryCode is not null
				then 1
				else 0
				end

			or
				(
					case
					when Previous.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when Previous.WardCode is not null
					then 2
					else 0
					end +

					case
					when Previous.PatientCategoryCode is not null
					then 1
					else 0
					end

					=

					case
					when DivisionRuleBase.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when DivisionRuleBase.WardCode is not null
					then 2
					else 0
					end +

					case
					when DivisionRuleBase.PatientCategoryCode is not null
					then 1
					else 0
					end

					and	Previous.DivisionRuleBaseRecno > DivisionRuleBase.DivisionRuleBaseRecno
				)
			)
		)
	) Activity

where
		Activity.SourceEncounterNo = EDD.Spells.SourceEncounterNo
	AND Activity.SourceSpellNo = EDD.Spells.SourceSpellNo

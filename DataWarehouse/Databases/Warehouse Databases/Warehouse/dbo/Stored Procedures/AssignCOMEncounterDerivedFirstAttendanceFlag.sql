﻿
CREATE procedure [dbo].[AssignCOMEncounterDerivedFirstAttendanceFlag] as


update COM.Encounter
set	
	DerivedFirstAttendanceFlag =
	
		case
		when ReferralID = -1 -- added to handle group contacts from non-patient events
		then '1'
		when exists --there has been a previous attendance
			(
			select
				1
			from
				COM.Encounter PreviousAttendance
			where
				EncounterOutcomeCode = 'ATTD'
			and RecordStatus = 'C'
			and ReferralID is not null
			and PreviousAttendance.PatientSourceID = Encounter.PatientSourceID
			and	PreviousAttendance.ReferralID = Encounter.ReferralID
			and	PreviousAttendance.StartTime < Encounter.StartTime
			)
		then '2'
		else '1'
		end
from
	COM.Encounter
where
	ReferralID is not null


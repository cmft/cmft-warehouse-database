﻿
create proc dbo.LoadMedisecUserMaster

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table Medisec.UserMaster

insert into Medisec.UserMaster

select [SourceUserCode]
      ,[UserName]
      ,[UserType]
      ,[UserDepartment] 
from dbo.TLoadMedisecUserMaster

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


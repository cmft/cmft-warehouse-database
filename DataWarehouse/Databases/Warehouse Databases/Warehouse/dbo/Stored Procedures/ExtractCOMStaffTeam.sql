﻿
CREATE PROCEDURE [dbo].[ExtractCOMStaffTeam]
	
AS


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()


TRUNCATE TABLE dbo.TImportCOMStaffTeam

INSERT INTO dbo.TImportCOMStaffTeam
(
	 StaffTeamID--[STEAM_REFNO]
	
	,StaffTeamCode--[STEAM_REFNO_CODE]
	
	,StaffTeam--[STEAM_REFNO_NAME]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]
	
	,StaffTeamDescription--[DESCRIPTION]

	,StaffTeamSpecialtyID--[SPECT_REFNO]

	,TeamLeaderID--[LEADR_PROCA_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,StaffTeamHealthOrgOwner--[OWNER_HEORG_REFNO]
	
) 
SELECT
	StaffTeamID = [STEAM_REFNO]	
	,StaffTeamCode = [STEAM_REFNO_CODE]	
	,StaffTeam = [STEAM_REFNO_NAME]	
	,StartTime = [START_DTTM]	
	,EndTime = [END_DTTM]	
	,StaffTeamDescription = [DESCRIPTION]
	,StaffTeamSpecialtyID = [SPECT_REFNO]
	,TeamLeaderID = [LEADR_PROCA_REFNO]	
	,ArchiveFlag = [ARCHV_FLAG]	
	,StaffTeamHealthOrgOwner = [OWNER_HEORG_REFNO]
FROM 
	[$(ipm)].dbo.STAFF_TEAMS_V01 StaffTeam

WHERE 
	[OWNER_HEORG_REFNO] IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
						  ,2031409) -- 5NT MANCHESTER PCT

	AND NOT EXISTS
		(
		SELECT
			1
		FROM
			[$(ipm)].dbo.STAFF_TEAMS_V01 LaterStaffTeam
		WHERE
			[OWNER_HEORG_REFNO] IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
							      ,2031409) -- 5NT MANCHESTER PCT
			AND	LaterStaffTeam.STEAM_REFNO = StaffTeam.STEAM_REFNO
			AND	LaterStaffTeam.MODIF_DTTM > StaffTeam.MODIF_DTTM
		)


/*

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractCommunityEncounter', @Stats, @StartTime
*/
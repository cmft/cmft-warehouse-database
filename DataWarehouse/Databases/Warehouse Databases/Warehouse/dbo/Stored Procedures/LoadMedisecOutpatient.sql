﻿
create proc dbo.LoadMedisecOutpatient

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table Medisec.Outpatient

insert into Medisec.Outpatient

select [PatientNumber]
      ,[EpisodeNumber]
      ,[AppointmentDate]
      ,[AppointmentTime]
      ,[DoctorCode]
      ,[HospitalCode]
      ,[SpecialtyCode]
      ,[Diary]
      ,[Stream]
      ,[ConsultantCode]
      ,[AppointmentNumber]
      ,[AttendanceNumber]
      ,[AttendanceMarker]
      ,[Outcome]
      ,[Type]
      ,[Contract]
      ,[AppointmentCode]
      ,[ProcedureCode1]
      ,[ProcedureCode2]
      ,[ProcedureCode3]
      ,[ProcedureCode4]
      ,[outepdireq]
      ,[EpisodeClosedDate]
      ,[PatientCategory]
      ,[ReferenceCode]
      ,[outreforg]
      ,[outrefsrce]
      ,[outpurchref]
      ,[LetterDate]
      ,[LetterComment]
      ,[LetterStatus]
      ,[outcank]
      ,[HospitalNumber]
      ,[EpisodeSecurityFlag]
from dbo.TLoadMedisecOutpatient

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


﻿create PROCEDURE [dbo].[ExtractCOMWardStay]
	-- @from smallTime
	--,@to smallTime
AS


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

DECLARE @from smalldatetime = NULL
DECLARE @to smalldatetime = NULL
--set @from = '04/01/2011'
--set @to = '04/02/2011'


--SELECT  @lastmodifiedTime = coalesce(MAX(CONVERT(datetime, ModifiedTime)) 
--,'01/01/1900'
--				)
--FROM COM.WardStay

TRUNCATE TABLE dbo.TImportCOMWardStay

INSERT INTO dbo.TImportCOMWardStay
(
	 SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,StartTime
	,EndTime
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID 
	,SourceAdmissionMethodID 
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID 
	,ExpectedDischargeDate 
	,SourceDischargeDestinationID 
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode

	,RegisteredPracticeID
)
SELECT 
	 WardStay.SourceUniqueID
	,WardStay.SourceSpellNo
	,WardStay.SourceEncounterNo
	,WardStay.ProviderSpellNo
	,WardStay.AdmissionTime
	,WardStay.DischargeTime
	,WardStay.StartTime
	,WardStay.EndTime
	,WardStay.PatientSourceUniqueID
	,Patient.PatientSourceSystemUniqueID
	,Patient.PatientNHSNumber
	,Patient.PatientTitleID
	,Patient.PatientForename
	,Patient.PatientSurname
	,WardStayPatientAddress.PatientAddress1
	,WardStayPatientAddress.PatientAddress2
	,WardStayPatientAddress.PatientAddress3
	,WardStayPatientAddress.PatientAddress4
	,WardStayPatientAddress.PatientPostCode
	,Patient.PatientDateOfBirth
	,Patient.PatientDateOfDeath
	,Patient.PatientSexID
	,Patient.PatientEthnicGroupID
	,WardStay.SourceServicePointID 
	,WardStay.SourceAdmissionMethodID 
	,WardStay.SourceAdmissionSourceID
	,WardStay.SourcePatientClassificationID
	,WardStay.SourceAdminCategoryID
	,WardStay.SourceIntendedManagementID
	,WardStay.SourceReasonForAdmissionID 
	,WardStay.ExpectedDischargeDate 
	,WardStay.SourceDischargeDestinationID 
	,WardStay.SourceDischargeMethodID
	,WardStay.SpellProfessionalCarer
	,WardStay.SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode

	,WardStayPCTCode = 
		ProfessionalCarerParentBase.MAIN_IDENT

	,WardStay.CreatedTime
	,WardStay.ModifiedTime
	,WardStay.CreatedByID
	,WardStay.ModifiedByID
	,WardStay.ArchiveFlag
	,WardStay.HealthOrgOwner
	,InterfaceCode = 'IPM'

	,RegisteredPracticeID = 
		ProfessionalCarerWaitBase.PatientProfCarerHealthOrgID
FROM
	(
	SELECT 
		 SourceUniqueID = Stay.SSTAY_REFNO
		,SourceSpellNo = Spell.PRVSP_REFNO
		,SourceEncounterNo =
			ROW_NUMBER() OVER(PARTITION BY Spell.PRVSP_REFNO, Spell.PATNT_REFNO ORDER BY Stay.START_DTTM)
		,ProviderSpellNo = CONVERT(varchar(20),Spell.PATNT_REFNO) + '/' + CONVERT(varchar(20),Spell.PRVSP_REFNO)
		,PatientSourceUniqueID = Spell.PATNT_REFNO
		,AdmissionTime = Spell.ADMIT_DTTM
		,DischargeTime = Spell.DISCH_DTTM
		,StartTime = Stay.START_DTTM
		,EndTime = Stay.END_DTTM
		,SourceServicePointID = Stay.SPONT_REFNO
		,SourceAdmissionMethodID = Spell.ADMET_REFNO
		,SourceAdmissionSourceID = Spell.ADSOR_REFNO
		,SourcePatientClassificationID = Spell.PATCL_REFNO
		,SourceAdminCategoryID = Spell.ADCAT_REFNO
		,SourceIntendedManagementID = Spell.INMGT_REFNO
		,SourceReasonForAdmissionID = READM_REFNO
		,ExpectedDischargeDate = EXPDS_DTTM
		,SourceDischargeDestinationID = Spell.DISDE_REFNO
		,SourceDischargeMethodID = Spell.DISMT_REFNO
		,SpellProfessionalCarer = Spell.PROCA_REFNO
		,SpellSpecialty = Spell.SPECT_REFNO
		,CreatedTime = Stay.CREATE_DTTM
		,ModifiedTime = Stay.MODIF_DTTM
		,CreatedByID = Stay.USER_CREATE
		,ModifiedByID = Stay.USER_MODIF
		,ArchiveFlag = Stay.ARCHV_FLAG
		,HealthOrgOwner = Spell.OWNER_HEORG_REFNO
	FROM 
		[$(ipm)].dbo.ProviderSpell Spell

	INNER JOIN [$(ipm)].dbo.ServicePointStay Stay
	ON	Stay.PRVSP_REFNO = Spell.PRVSP_REFNO
	) WardStay

LEFT OUTER JOIN 
	(
	SELECT 
		 SourceUniqueID = PATNT_REFNO
		,PatientSourceSystemUniqueID = PASID
		,PatientNHSNumber = NHS_IDENTIFIER
		,PatientTitleID = TITLE_REFNO
		,PatientForename = FORENAME
		,PatientSurname = SURNAME
		,PatientDateOfBirth = DATE_OF_BIRTH
		,PatientDateOfDeath = DATE_OF_DEATH
		,PatientSexID = SEXXX_REFNO
		,PatientEthnicGroupID = ETHGR_REFNO
	FROM
		[$(ipm)].dbo.Patient
	) Patient

ON	Patient.SourceUniqueID = WardStay.PatientSourceUniqueID

LEFT OUTER JOIN
	(
	SELECT DISTINCT
		 ServicePointStayPatientAddress.SSTAY_REFNO
		,ServicePointStayPatientAddress.ADDSS_REFNO
		,PatientAddress1 = PatientAddressDetails.LINE1
		,PatientAddress2 = PatientAddressDetails.LINE2
		,PatientAddress3 = PatientAddressDetails.LINE3
		,PatientAddress4 = PatientAddressDetails.LINE4
		,PatientPostCode = PatientAddressDetails.PCODE
	FROM
		(
		SELECT 
			 ServicePointStay.SSTAY_REFNO
			,MAX(PatientAddress.ADDSS_REFNO) AS ADDSS_REFNO
		FROM 
			(
			SELECT 
				 SSTAY_REFNO
				,START_DTTM
				,PATNT_REFNO
			FROM 
				[$(ipm)].dbo.ServicePointStay
			--WHERE   
			--	MODIF_DTTM > @lastmodifiedTime
			) ServicePointStay

		LEFT OUTER JOIN
			(
			SELECT
				 AddressRole.ADDSS_REFNO
				,AddressRole.START_DTTM
				,AddressRole.END_DTTM
				,AddressRole.PATNT_REFNO
			FROM 
				[$(ipm)].dbo.AddressRole AddressRole
			WHERE   
				ROTYP_CODE = 'HOME'
			AND ARCHV_FLAG = 'N'
			) PatientAddress
		ON	ServicePointStay.PATNT_REFNO = PatientAddress.PATNT_REFNO
		AND PatientAddress.START_DTTM <= ServicePointStay.START_DTTM 
		AND (
				PatientAddress.END_DTTM IS NULL
			OR	PatientAddress.END_DTTM > ServicePointStay.START_DTTM
			)

		GROUP BY 
			ServicePointStay.SSTAY_REFNO

		) ServicePointStayPatientAddress
	
	LEFT OUTER JOIN
		(	
		SELECT DISTINCT
			 ADDSS_REFNO
			,LINE1
			,LINE2
			,LINE3
			,LINE4
			,PCODE
		FROM 
			[$(ipm)].dbo.[Address]
		WHERE
			ADTYP_CODE = 'POSTL'
		AND ARCHV_FLAG = 'N'
		) PatientAddressDetails
	ON	ServicePointStayPatientAddress.ADDSS_REFNO = PatientAddressDetails.ADDSS_REFNO

	) WardStayPatientAddress
ON	WardStayPatientAddress.SSTAY_REFNO = WardStay.SourceUniqueID

--LEFT OUTER JOIN
--(
--	SELECT DISTINCT
--			 EncounterPatientProfessionalCarer.SSTAY_REFNO
--			,EncounterPatientProfessionalCarer.PATPC_REFNO
--			,WardStayPracticeCode = ProfessionalCarerCode
--			,WardStayGPCode = HealthOrgCode
--			,WardStayPCTCode = ParentHealthOrgCode

--			,PatientProfCarerDetail.HEORG_REFNO
--	FROM
--			(
--			SELECT 
--				SSTAY_REFNO
--				,MAX(PATPC_REFNO) AS PATPC_REFNO
--			FROM 
--				(
--					SELECT 
--							 SSTAY_REFNO
--							,START_DTTM
--							,PATNT_REFNO
--					 FROM 
--							[$(ipm)].dbo.ServicePointStay
--					 --WHERE  
--						--	MODIF_DTTM > @lastmodifiedTime
--				)ServicePointStay
--			LEFT OUTER JOIN
--				(
--					SELECT
--							 PatientProfessionalCarer.PATPC_REFNO
--							,PatientProfessionalCarer.START_DTTM
--							,PatientProfessionalCarer.END_DTTM
--							,PatientProfessionalCarer.PATNT_REFNO
--							,PatientProfessionalCarer.HEORG_REFNO
--					FROM 
--							[$(ipm)].dbo.PatientProfessionalCarer 
							
--					WHERE   ARCHV_FLAG = 'N'
--						AND PRTYP_REFNO = '1132'-- General Practitioner
--				)PatientProfCarer

--				ON ServicePointStay.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
--				AND PatientProfCarer.START_DTTM <= ServicePointStay.START_DTTM 
--				AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > ServicePointStay.START_DTTM)
--			GROUP BY 
--				SSTAY_REFNO

--	)EncounterPatientProfessionalCarer

--	LEFT OUTER JOIN (
--						SELECT
--							 PatientProfessionalCarer.PATPC_REFNO
--							,PatientProfessionalCarer.START_DTTM
--							,PatientProfessionalCarer.END_DTTM
--							,PatientProfessionalCarer.PATNT_REFNO
--							,PatientProfessionalCarer.HEORG_REFNO
--							,PatientProfessionalCarer.PROCA_MAIN_IDENT AS ProfessionalCarerCode
--						FROM 
--								[$(ipm)].dbo.PatientProfessionalCarer 
--						WHERE   
--								ARCHV_FLAG = 'N'
--							AND PRTYP_REFNO = '1132'-- General Practitioner
--					)PatientProfCarerDetail
--	ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO

--	LEFT OUTER JOIN (	
--						SELECT DISTINCT
--								 HEORG_REFNO
--								,HealthOrganisation.[DESCRIPTION] AS HealthOrgDescription
--								,HealthOrganisation.MAIN_IDENT AS HealthOrgCode
--								,HealthOrganisation.PARNT_REFNO
--						 FROM 
--								[$(ipm)].dbo.HealthOrganisation 
--						 WHERE 
--								ARCHV_FLAG = 'N'
--						) PatientProfCarerHealthOrg
							 
--	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
	
--	LEFT OUTER JOIN 
--		(	
--			SELECT DISTINCT
--					  HealthOrganisation.HEORG_REFNO ParentHEORG_REFNO
--					 ,HealthOrganisation.[DESCRIPTION] AS ParentHealthOrgDescription
--					 ,HealthOrganisation.MAIN_IDENT AS ParentHealthOrgCode
--					 ,HealthOrganisation.HOTYP_REFNO AS ParentHealthOrgCodeType
--			 FROM 
--					[$(ipm)].dbo.HealthOrganisation 
--			 WHERE 
--					ARCHV_FLAG = 'N'
--			   AND HOTYP_REFNO = 629
--		) ParentPatientProfCarerHealthOrg

--	ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
	
--)ProfessionalCarerBase
--ON WardStay.SourceUniqueID = ProfessionalCarerBase.SSTAY_REFNO


outer apply
	(
	select top 1
		 PatientProfessionalCarerID = PatientProfessionalCarer.PATPC_REFNO
		,PatientProfCarerHealthOrgID = PatientProfessionalCarer.HEORG_REFNO
		,PatientProfCarerParentHealthOrgID = PatientProfCarerHealthOrg.ParentHeorg_Refno 
		,WardStayPracticeCode = PatientProfessionalCarer.HEORG_MAIN_IDENT
		,WardStayGPCode = PatientProfessionalCarer.PROCA_MAIN_IDENT
	from
		[$(ipm)].dbo.PatientProfessionalCarer

	left outer join 
		(	
		select
			 HealthOrganisationID = HealthOrganisation.HEORG_REFNO
			,ParentHeorg_Refno = ParentHealthOrganisation.HEORG_REFNO 
		from 
			[$(ipm)].dbo.HealthOrganisation 	

		inner join	 
			(	
			select 
				 HEORG_REFNO 
			from 
				[$(ipm)].dbo.HealthOrganisation 
			where 
				ARCHV_FLAG = 'N'
			AND HOTYP_REFNO = 629
			) ParentHealthOrganisation
		on	ParentHealthOrganisation.HEORG_REFNO = HealthOrganisation.PARNT_REFNO

		where 
			ARCHV_FLAG = 'N'
		) PatientProfCarerHealthOrg
	 on	PatientProfCarerHealthOrg.HealthOrganisationID = PatientProfessionalCarer.HEORG_REFNO

	where
		ARCHV_FLAG = 'N'
	and PRTYP_REFNO = '1132'-- General Practitioner
	and PatientProfessionalCarer.PATNT_REFNO = WardStay.PatientSourceUniqueID
	and PatientProfessionalCarer.START_DTTM <= WardStay.StartTime 
	and (
			PatientProfessionalCarer.END_DTTM IS NULL 
		or	PatientProfessionalCarer.END_DTTM > WardStay.StartTime
		)
	order by 
		 CREATE_DTTM desc
		,MODIF_DTTM desc

	) ProfessionalCarerWaitBase

left outer join
	(
	select
		*
	from
		[$(ipm)].dbo.HealthOrganisation
	) ProfessionalCarerParentBase
on ProfessionalCarerParentBase.HEORG_REFNO = ProfessionalCarerWaitBase.PatientProfCarerParentHealthOrgID


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' 

exec WriteAuditLogEvent 'ExtractCOMWardStay', @Stats, @StartTime

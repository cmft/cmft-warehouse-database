﻿CREATE procedure [dbo].[AssignOPWLDirectorateCode] as


-- main mapping
update OP.WaitingList
set

	 WaitingList.DirectorateCode = DivisionMap.DirectorateCode
	,WaitingList.SpecialtyTypeCode = CASE 
										WHEN DivisionMap.Diagnostic = 'Y' THEN 'D'
										WHEN DivisionMap.Diagnostic = 'N' THEN 'I'
										ELSE DivisionMap.Diagnostic
									 END	
from
	OP.WaitingList 

inner join WH.DivisionMap
on	DivisionMap.SiteCode = WaitingList.SiteCode
and	DivisionMap.SpecialtyCode = WaitingList.PASSpecialtyCode


-- specific mapping rules

update OP.WaitingList
set
	DirectorateCode = 70
WHERE 
	PASSpecialtyCode in ('PT' , 'DIET' , 'DTCO') --Like '110 P%' Or	Specialty Like '654%'
AND DateOnWaitingList < '9 Jan 2009'



update OP.WaitingList
set
	DirectorateCode = 0
where
	PASSpecialtyCode in ( 'PAED' , 'PNEP' )
AND DateOnWaitingList >= '7 Jan 2009'
AND SiteCode = 'MRI'


update OP.WaitingList
set
	DirectorateCode = 30
where
	PASSpecialtyCode = 'PMET'
AND ClinicCode in ('CAAMMETT' , 'CJEWMET' , 'CJHWMET' , 'RJEWMET')


update OP.WaitingList
set
	DirectorateCode = 0
WHERE 
	PASSpecialtyCode = 'OPHC'
AND ClinicCode in ( 'CORTHOP' , 'CPREAD' )




﻿
CREATE PROCEDURE [dbo].[LoadCOMReturnDM01WaitingList] 
(
	@CensusDate Datetime = NULL
)
AS

SET XACT_ABORT OFF
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET DATEFORMAT dmy
SET NOCOUNT ON

--DECLARE @censusdate date = '01/September/2011'
/*
***********************************************************************
		START OF USER SPECIFIC STATETMENTS
***********************************************************************
*/
IF OBJECT_ID('Tempdb.dbo.#MDR_Referrals_With_Schedules_Pre_Snapshot') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_Referrals_With_Schedules_Pre_Snapshot
END

IF OBJECT_ID('Tempdb.dbo.#MDR_Referrals_With_Attended_Schedules_Pre_Snapshot') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_Referrals_With_Attended_Schedules_Pre_Snapshot
END

IF OBJECT_ID('Tempdb.dbo.#MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot
END

IF OBJECT_ID('Tempdb.dbo.#MDR_Schedules_Pre_Snapshot') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_Schedules_Pre_Snapshot
END

IF OBJECT_ID('Tempdb.dbo.#MDR_Referrals_With_Schedules_Post_Snapshot') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_Referrals_With_Schedules_Post_Snapshot
END

IF OBJECT_ID('Tempdb.dbo.#MDR_Referrals_With_Attended_Schedules_Post_Snapshot') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_Referrals_With_Attended_Schedules_Post_Snapshot
END

IF OBJECT_ID('Tempdb.dbo.#MDR_First_Activity') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_First_Activity
END

IF OBJECT_ID('COM.MDR_Waiting_Referrals_Not_Attended') IS NOT NULL BEGIN
	DROP TABLE COM.MDR_Waiting_Referrals_Not_Attended
END

IF OBJECT_ID('COM.MDR_Waiting_Referrals_No_Activity') IS NOT NULL BEGIN
	DROP TABLE COM.MDR_Waiting_Referrals_No_Activity
END

IF OBJECT_ID('COM.MDR_Waiting_Recent_Attendances') IS NOT NULL BEGIN
	DROP TABLE COM.MDR_Waiting_Recent_Attendances
END

IF OBJECT_ID('Tempdb.dbo.#MDR_Waiting_List') IS NOT NULL BEGIN
	DROP TABLE dbo.#MDR_Waiting_List
END


DELETE
--DECLARE @CensusDate date = '31/July/2011'
--SELECT Convert(Date,CURRENT_TIMESTAMP,103),*
FROM COM.ReturnDM01WaitingList
WHERE 
ReturnPeriod = (YEAR(@CensusDate) * 100) + MONTH (@CensusDate)
	AND LoadDate = Convert(Date,CURRENT_TIMESTAMP,103)
	AND [Return] = 'DM01'
	AND ReturnItem IN('DM01WaitingList' )


SET @CensusDate = COALESCE(@CensusDate
							, Convert(Date,dateadd(d,-1,CURRENT_TIMESTAMP),103)
							)

--IF OBJECT_ID('COM.ReturnDM01WaitingList') IS NOT NULL BEGIN
--	DROP TABLE COM.ReturnDM01WaitingList
--END

--*******************************************************************************
--Pre production - Evaluate First / New activity for the set of referrals - START
--*******************************************************************************



SELECT
	fs.REFRL_REFNO,
	MIN(fs.START_DTTM) AS FirstAppt
INTO 
	#MDR_First_Activity
FROM
	[$(ipm)].dbo.Schedule fs
LEFT JOIN [$(ipm)].dbo.TestPatient tp
	ON fs.PATNT_REFNO = tp.PATNT_REFNO
WHERE
		tp.PATNT_REFNO IS NULL -- exclude test patients
	AND
		fs.ARCHV_FLAG = 'N'
	AND
		(
				fs.SCTYP_REFNO = 1468
			OR
				fs.SCTYP_REFNO = 1470
		)
	AND
		( --extracting attendances only
				(
						fs.SCTYP_REFNO = '1470'					-- Clinic Contact
					AND
						fs.ATTND_REFNO IN (
							357,								-- Attended On Time
							2868								-- Patient Late / Seen
						)
				)
			OR
				(
						fs.SCTYP_REFNO = '1468'					-- Community Contact
					AND
						fs.SCOCM_REFNO NOT IN (
							1459,								-- Patient Died
							2001651,							-- Cancelled
							1457								-- Did Not Attend
						)
					AND
						fs.ARRIVED_DTTM IS NOT NULL
					AND
						fs.DEPARTED_DTTM IS NOT NULL
				)
		)
	AND
		fs.CONTY_REFNO <> 2004177
	AND 
		fs.SPECT_REFNO = '2000820'

GROUP BY
	fs.REFRL_REFNO
--SELECT COUNT(*),'#MDR_First_Activity'
--FROM #MDR_First_Activity
--*******************************************************************************
--Pre production - Evaluate First / New activity for the set of referrals - END
--*******************************************************************************	

--********************************************************************
--MDR_Waiting_Referrals_Not_Attended	- Referrals not attended START
--********************************************************************
SET DATEFORMAT dmy

--********************************************************************
--Referrals_Not_Attended Step 1. evaluate referrals with schedules
--********************************************************************

SELECT  
    --reference fields
    s.SCHDL_REFNO
    ,s.PATNT_REFNO
    ,s.sctyp_RefNo,
    r.REFRL_REFNO, 
    s.REFRL_REFNO as [schedule ref no],
    r.RECVD_DTTM as 'Referral Date',
    convert(decimal(10,1), 
		datediff(hour, r.RECVD_DTTM, @CensusDate) / 168.0)
		as 'weeks wait'
	,p.SURNAME
    ,p.FORENAME
    ,p.NHS_IDENTIFIER,
    r.CLOSR_DATE as [Referral Closed Date],
	s.[start_dttm],							
		ISNULL(prof.surname, 'null') + ',' + ISNULL(prof.forename, 'null') as 'Professional Carer',						
	CASE							
		WHEN s.SCTYP_REFNO = '1468' THEN 'Community Contact'						
		WHEN s.SCTYP_REFNO = '1470' THEN 'Clinic Contact'						
		ELSE 'Other'						
	END AS 'Schedule Type',							
	CASE s.SCTYP_REFNO							
		WHEN 1468 -- Community Contact						
			THEN	CASE				
						WHEN s.SCOCM_REFNO = 1457 THEN 'DNA'		
						WHEN s.SCOCM_REFNO = 1459 THEN 'Patient deceased'		
						WHEN s.SCOCM_REFNO = 2001651 THEN 'Cancelled'		
						WHEN		
								MONTH(s.ARRIVED_DTTM) IS NOT NULL
							AND	
								MONTH(s.DEPARTED_DTTM) IS NOT NULL THEN 'Attended'
						WHEN		
								MONTH(s.ARRIVED_DTTM) IS NULL
							OR	
								MONTH(s.DEPARTED_DTTM) IS NULL THEN 'Not Recorded'
						ELSE 'Other'		
					END			
		WHEN 1470 -- Clinic Contact						
			THEN	CASE				
						WHEN s.ATTND_Refno IN (		
							357,	--Attended On Time
							2868	--Patient Late / Seen
						) THEN 'Attended'		
						WHEN s.ATTND_Refno IN (		
							2870,	
							2003532,	
							2004528,	
							2004300,	
							2004301,	
							3006508	
						) THEN 'Cancelled'		
						WHEN s.ATTND_Refno IN (		
							358,	
							2000724,	
							2003494,	
							2003495	
						) THEN 'DNA'		
						WHEN s.ATTND_Refno = '45' THEN 'Not Recorded'		
						ELSE 'Other'		
					END			
	END AS Outcome,							
	--1.0.4. make unmatched explicit
	ISNULL(t.Description, 'Unknown') as 'Team',
	--P.McNulty 1.0.4 - replace age bands with age at referral
	CASE					
		WHEN (MONTH(p.DTTM_OF_BIRTH) * 100 ) + DAY(p.DTTM_OF_BIRTH) <= (MONTH(r.RECVD_DTTM) * 100 ) + DAY(r.RECVD_DTTM)				
			THEN DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM)			
		ELSE DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM) - 1				
	END as 'Age',					
	c.description as 'clinic description',	
	rvRefSource.description as 'referral Source',
	refspec.description as 'referral Specialty',								
	spec.description as 'Appt Specialty',
	CASE
		WHEN s.START_DTTM = fa.FirstAppt THEN 0
		ELSE  1
	END AS FirstAppt
	,GPPracticeCode = ISNULL(ProfessionalCarerBase.HealthOrgCode, 'V81999')
	,GPPractice = ISNULL(ProfessionalCarerBase.HealthOrgDescription, 'GP Practice Code not known') 
	,CommissionerCode = ISNULL(ProfessionalCarerBase.ParentHealthOrgCode, 'V81999')
	,Commissioner = ISNULL(ProfessionalCarerBase.ParentHealthOrgDescription, 'GP Practice Code not known')  
	,s.SPECT_REFNO

INTO #MDR_Referrals_With_Schedules_Pre_Snapshot								
								
FROM								
	[$(ipm)].dbo.Schedule s							
left join [$(ipm)].dbo.Patient p								
on s.PATNT_REFNO = p.PATNT_REFNO								
								
left join [$(ipm)].dbo.Referral r								
on s.REFRL_REFNO = r.REFRL_REFNO								
								
LEFT JOIN [$(ipm)].dbo.Specialty refspec								
	ON r.REFTO_SPECT_REFNO = refspec.SPECT_REFNO								
								
LEFT JOIN [$(ipm)].dbo.Specialty spec								
	ON s.SPECT_REFNO = spec.SPECT_REFNO
	
LEFT JOIN [$(ipm)].dbo.ReferenceValue rvRefSource
	ON
			r.SORRF_REFNO = rvRefSource.RFVAL_REFNO
		AND
			rvRefSource.RFVDM_CODE = 'SORRF'
		AND
			rvRefSource.ARCHV_FLAG = 'N'
	
left join [$(ipm)].dbo.ProfessionalCarer prof1								
on r.REFBY_PROCA_REFNO = prof1.PROCA_REFNO								
								
left join [$(ipm)].dbo.StaffTeam t								
on r.REFTO_STEAM_REFNO = t.STEAM_REFNO	
								
LEFT Join [$(ipm)].dbo.ReferenceValue rfval								
	ON r.SORRF_REFNO = rfval.RFVAL_REFNO							
								
left join [$(ipm)].dbo.ServicePoint	c							
	on s.SPONT_REFNO = c.SPONT_REFNO							
								
LEFT JOIN [$(ipm)].dbo.ProfessionalCarer prof								
      ON ISNULL(r.REFTO_PROCA_REFNO, 0) = prof.PROCA_REFNO																

LEFT JOIN (								
	SELECT 							
		s.REFRL_REFNO						
	FROM							
		[$(ipm)].dbo.Schedule s						
	INNER JOIN [$(ipm)].dbo.Patient p							
		ON						
				s.PATNT_REFNO = p.PATNT_REFNO				
			AND					
				p.ARCHV_FLAG = 'N'   				
	WHERE							
			s.REFRL_REFNO IS NOT NULL					
		AND						
			(					
					LEFT(p.NHS_IDENTIFIER, 3) = '999'			
				OR				
					p.FORENAME  in ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')			
				OR				
					p.SURNAME  in ('xxtestpatientaavp','xxtestpatientaafe')			
			)
	GROUP BY							
		s.REFRL_REFNO						
) ex -- list of schedules for test patients								
	ON s.REFRL_REFNO = ex.REFRL_REFNO
	
LEFT JOIN #MDR_First_Activity fa
	ON s.REFRL_REFNO = fa.REFRL_REFNO	
	
--Join to GP Practice and PCT
LEFT OUTER JOIN
(
	SELECT DISTINCT
			 EncounterPatientProfessionalCarer.SCHDL_REFNO
			,EncounterPatientProfessionalCarer.PATPC_REFNO
			,ProfessionalCarerCode
			,HealthOrgCode
			,HealthOrgDescription
			,ParentHealthOrgCode
			,ParentHealthOrgDescription
	FROM
			(
			SELECT 
				 SCHDL_REFNO
				,MAX(PATPC_REFNO) AS PATPC_REFNO
			FROM 
				(
				SELECT 
						 SCHDL_REFNO
						,START_DTTM
						,PATNT_REFNO
				 FROM 
						[$(ipm)].dbo.Schedule Encounter
				)Encounter

			LEFT OUTER JOIN
				(
				SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO
				FROM 
						[$(ipm)].dbo.PatientProfessionalCarer 
				WHERE   
						ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarer

			ON Encounter.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
			AND PatientProfCarer.START_DTTM <= Encounter.START_DTTM 
			AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > Encounter.START_DTTM)

			GROUP BY 
				SCHDL_REFNO
	)EncounterPatientProfessionalCarer

	LEFT OUTER JOIN (
					SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO
						,PatientProfessionalCarer.PROCA_MAIN_IDENT AS ProfessionalCarerCode
					FROM 
						[$(ipm)].dbo.PatientProfessionalCarer
					WHERE   
						 ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarerDetail
	ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO

	LEFT OUTER JOIN (	
						SELECT DISTINCT
							 HealthOrganisation.HEORG_REFNO
							 ,HealthOrganisation.[DESCRIPTION] AS HealthOrgDescription
							 ,HealthOrganisation.MAIN_IDENT AS HealthOrgCode
							 ,HealthOrganisation.PARNT_REFNO
						 FROM [$(ipm)].dbo.HealthOrganisation 
						 WHERE ARCHV_FLAG = 'N'
		
					) PatientProfCarerHealthOrg
	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
	
		LEFT OUTER JOIN 
		(	
		SELECT DISTINCT
			  HealthOrganisation.HEORG_REFNO ParentHEORG_REFNO
			 ,HealthOrganisation.[DESCRIPTION] AS ParentHealthOrgDescription
			 ,HealthOrganisation.MAIN_IDENT AS ParentHealthOrgCode
			 ,HealthOrganisation.HOTYP_REFNO AS ParentHealthOrgCodeType
		 FROM [$(ipm)].dbo.HealthOrganisation 
		 WHERE ARCHV_FLAG = 'N'
		   AND HOTYP_REFNO = 629
		) ParentPatientProfCarerHealthOrg

	ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
	
)ProfessionalCarerBase
ON s.SCHDL_REFNO = ProfessionalCarerBase.SCHDL_REFNO
	
WHERE								
		ex.REFRL_REFNO IS NULL -- exclude test patients						
	--return records where referral is either still open or was closed after end of last calendar month
	AND (
			r.CLOSR_DATE is null
		OR
			r.CLOSR_DATE > @CensusDate
		)
	AND							
		r.ARCHV_FLAG = 'N'			
	AND							
		s.ARCHV_FLAG = 'N'						
	AND							
		s.SCTYP_REFNO in ('1470', '1468') 
	AND 
		s.CONTY_REFNO <> 2004177-- Community Contacts and Outpatient Schedules						
	--1.0.4. cohort incorrectly based on schedules limiting criteria
	--rewrite as referral limiting criteria
	AND
		r.refto_spect_refno = '2000820' -- Audiology
	--Rererral received before end of last calendar month...	
	AND 	r.RECVD_DTTM <= @CensusDate
	
	--...and only consider those contacts that occured before end of last calendar month	
	AND s.START_DTTM <= @CensusDate                         	
	
	--P.McNulty 1.0.1 Exclusion criteria
	AND NOT r.REFTO_PROCA_REFNO	in ('10540965', '10000199')
	AND NOT r.REFTO_PROCA_REFNO	in ('10540966', '10538046')
	
	--P.McNulty 1.0.4 replace r.REFTO_STEAM_REFNO criteria to [$(ipm)].dbo.[STAFF_TEAMS_V01]
	AND (
			(
				r.REFTO_STEAM_REFNO not in ('10003232', '10003233', '150000125', '150000126', '10003234') 
			AND
				t.archv_flag = 'N'
			)
		OR
			r.REFTO_STEAM_REFNO is null
		)

	--P.McNulty 1.0.4 replace spont_refno criteria with reference to Service Points table
	AND (
			c.[DESCRIPTION] not like 'NHSP%'
		OR
			c.[DESCRIPTION] is null
		)
		
	--P.McNulty 1.0.7 remove Tier 2 ENT records
	AND (
			c.[DESCRIPTION] not like '%Central Manchester Ear and Nose Clinic%'
		OR
			c.[DESCRIPTION] is null
		)
	    								
--ORDER by r.REFRL_REFNO, s.start_dttm  GS20110914

--*************************************************************************
--Referrals Not Attended Step 2. evaluate referrals with attended schedules
--*************************************************************************
--SELECT COUNT(*),'#MDR_Referrals_With_Schedules_Pre_Snapshot'
--FROM #MDR_Referrals_With_Schedules_Pre_Snapshot	
			
						
SELECT 
	REFRL_REFNO as 'refrl_refno'
INTO 
	#MDR_Referrals_With_Attended_Schedules_Pre_Snapshot
FROM 
	#MDR_Referrals_With_Schedules_Pre_Snapshot
WHERE
	outcome in ('attended')
GROUP BY 
	REFRL_REFNO

--SELECT Count(*),'#MDR_Referrals_With_Attended_Schedules_Pre_Snapshot'
--FROM #MDR_Referrals_With_Attended_Schedules_Pre_Snapshot

--****************************************************************************
--Referrals Not Attended Step 3. evaluate referrals with no attended schedules
--****************************************************************************

--note: we are actually evaulating data at the schedule level because as well as
--returning referral level information we are returning information about the
--first (non attended) schedule that is attached to each referral...

--...also, non essential reference fields are removed here. To debug replace 
-- specific fields with
-- select 'sch.* into...' and then select the contents of  
-- #MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot

select 
	sch.REFRL_REFNO,
	sch.[Referral Date],
	sch.[Weeks Wait],
	sch.[NHS_Identifier] as [NHS Identifier],      
	sch.[Referral closed Date],  
	sch.[Start_Dttm] as [First Contact], 
	sch.[Professional Carer],
	sch.[Schedule Type],
	sch.[Outcome],
	sch.[Team],
	sch.[Age],
	sch.[Clinic Description],
	sch.[Referral Source],
	sch.[Referral Specialty],
	sch.[Appt Specialty]
	,sch.GPPracticeCode
	,sch.GPPractice
	,sch.CommissionerCode
	,sch.Commissioner  					          
INTO 
	#MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot
FROM 
	#MDR_Referrals_With_Schedules_Pre_Snapshot sch
LEFT OUTER JOIN #MDR_Referrals_With_Attended_Schedules_Pre_Snapshot ref
	ON sch.REFRL_REFNO = ref.REFRL_REFNO
WHERE
	ref.REFRL_REFNO is null

--SELECT COUNT(*), '#MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot'
--FROM #MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot


--****************************************************************************
--Referrals Not Attended Step 4. remove duplicate schedules (keep only first)
--****************************************************************************

DELETE #MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot

WHERE EXISTS (SELECT 1
			  FROM #MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot as selfjoin
			  WHERE selfjoin.REFRL_REFNO = 
			        #MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot.REFRL_REFNO
			  GROUP BY selfjoin.REFRL_REFNO
			  HAVING MIN(selfjoin.[First Contact]) <>       
				     #MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot.[First Contact] )
				     
--SELECT COUNT(*),'#MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot'
--FROM #MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot
--***********************************************************
--Referrals Not Attended Step 5. Write results to perm table
--***********************************************************

SELECT 
	sch.REFRL_REFNO,
	sch.[Referral Date],
	sch.[Weeks Wait],
	sch.[NHS Identifier],      
	sch.[Referral closed Date],  
	sch.[First Contact], 
	sch.[Professional Carer],
	sch.[Schedule Type],
	sch.[Outcome],
	sch.[Team],
	sch.[Age],
	sch.[clinic Description],
	sch.[Referral Source],
	sch.[Referral Specialty],
	sch.[Appt Specialty],
	'' as [Recent Activity],
	'1' as [Status Type Code],
	'Referrals Not Attended' as [Status Type Description]
	,sch.GPPracticeCode
	,sch.GPPractice
	,sch.CommissionerCode
	,sch.Commissioner   					
       
INTO 
	COM.MDR_Waiting_Referrals_Not_Attended
FROM
	#MDR_Referrals_With_No_Attended_Schedules_Pre_Snapshot sch

--SELECT COUNT(*),'COM.MDR_Waiting_Referrals_Not_Attended'
--FROM COM.MDR_Waiting_Referrals_Not_Attended

--***********************************************************
--Referrals Not Attended Step 6. Conditional Clock
--***********************************************************
--...Hell's bells! Now we have to consider the clock restarting
--on all occrances of either a Patient Cancellation or a DNA...

--at this stage it's probably easier to do an update directly 
--onto the table if we need to...

--We'll try calling the function that returns the latest activity
--and add the type of 'clock reset' to that function...we'll inner join
--the set of referrals that might need updating to the resultset returned
--from the function (referral number and time of latest event) and update
--the set of referrals where necessary...
 
--debug
--SELECT fn.* FROM [$(ipm)].dbo.fn_MostRecentActivity(null,
--									       [Common].f_FirstOfMonth(getdate()), 
--									       4) --4 = Clock Reset event 
--									       fn
--inner join [$(ipm)].dbo.MDR_Waiting_Referrals_Not_Attended refset
--on fn.REFRL_REFNO = refset.REFRL_REFNO

UPDATE refset

SET
	refset.[Weeks Wait] = 
		--1.0.6 see above
		--DATEDIFF(week, fn.[MostRecentActivity], [Common].f_FirstOfMonth(getdate()))
		convert(decimal(10,1), DATEDIFF(hour, fn.MostRecentActivity, @CensusDate) / 168.0)
FROM  
	[$(ipm)].dbo.fn_MostRecentActivity
		(
		 null
		,@CensusDate
		,4 --4 = Clock Reset event 
		) fn

INNER JOIN COM.MDR_Waiting_Referrals_Not_Attended refset
on fn.REFRL_REFNO = refset.REFRL_REFNO

--select [$(ipm)].dbo.MDR_Waiting_Referrals_Not_Attended

--1.0.4
--update [$(ipm)].dbo.MDR_Waiting_Referrals_Not_Attended
--set [Activity Type] = ''
--where outcome <> 'attended'

--********************************************************************
--MDR_Waiting_Referrals_Not_Attended	- Referrals not attended END
--********************************************************************

--********************************************************************
--MDR_Waiting_Referrals_No_Activity	-     Referrals no activity START
--********************************************************************

--***********************************************************
--Referrals no activity Step 1. Create temp table with activity
--reference prior to cutoff..this will prevent us picking up
-- post cut off activity in the Step 2 join
--***********************************************************

SELECT 
	    sch.REFRL_REFNO
	   ,sch.SCHDL_REFNO
	   ,sch.SPONT_REFNO
INTO 
	#MDR_Schedules_Pre_Snapshot
FROM 
	[$(ipm)].dbo.Schedule sch
WHERE 
		sch.SCTYP_REFNO in ('1470', '1468') -- Community Contacts and Outpatient Schedules		
	AND 
		sch.CONTY_REFNO <> 2004177				
	AND 							
		sch.SPECT_REFNO in ('2000820',    -- Audiology
							'3000993',    -- (Alternate) Audiology
							'2000722',    -- Audiology (non-consultant)   
							'2000889' )   -- Audiology (non-consultant)                
	AND
		sch.START_DTTM <= @CensusDate

--***********************************************************
--Referrals no activity Step 2. Write results to perm table
--***********************************************************

SELECT DISTINCT
	r.REFRL_REFNO,
	r.RECVD_DTTM as 'Referral Date',
    convert(decimal(10,1), datediff(hour, r.RECVD_DTTM, @CensusDate) / 168.0) as 'weeks wait',
    NHS_IDENTIFIER as [NHS IDENTIFIER],	
	r.CLOSR_DATE as [Referral Closed Date],
	null as [First Contact],
	ISNULL(prof.surname, 'null') + ',' + ISNULL(prof.forename, 'null') as 'Professional Carer',						
	'' as [Schedule Type],
	'' as [Outcome],
	ISNULL(t.Description, 'Unknown') as 'Team',
	CASE					
		WHEN (MONTH(p.DTTM_OF_BIRTH) * 100 ) + DAY(p.DTTM_OF_BIRTH) <= (MONTH(r.RECVD_DTTM) * 100 ) + DAY(r.RECVD_DTTM)				
			THEN DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM)			
		ELSE DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM) - 1				
	END as 'Age',					
	'' as [clinic description],
	rvRefSource.description as 'referral Source',
	refspec.description as 'referral specialty',								
	'' as 'Appt Specialty',
	'' as [Recent Activity],
    '2' as [Status Type Code],
    'Referrals No Activity' as [Status Type Description]
	,GPPracticeCode = ISNULL(ProfessionalCarerBase.HealthOrgCode, 'V81999')
	,GPPractice = ISNULL(ProfessionalCarerBase.HealthOrgDescription, 'GP Practice Code not known') 
	,CommissionerCode = ISNULL(ProfessionalCarerBase.ParentHealthOrgCode, 'V81999')
	,Commissioner = ISNULL(ProfessionalCarerBase.ParentHealthOrgDescription, 'GP Practice Code not known')  

into COM.MDR_Waiting_Referrals_No_Activity				
								
FROM [$(ipm)].dbo.Referral r	

left join #MDR_Schedules_Pre_Snapshot s
on s.refrl_refno = r.refrl_refno
							
left join [$(ipm)].dbo.Patient p								
on r.patnt_refno = p.patnt_refno	

LEFT JOIN [$(ipm)].dbo.ReferenceValue rvRefSource
	ON
			r.SORRF_REFNO = rvRefSource.RFVAL_REFNO
		AND
			rvRefSource.RFVDM_CODE = 'SORRF'
		AND
			rvRefSource.ARCHV_FLAG = 'N'							
										
LEFT JOIN [$(ipm)].dbo.Specialty refspec								
	ON r.refto_spect_refno = refspec.SPECT_REFNO							

left join [$(ipm)].dbo.ProfessionalCarer prof1								
on r.refby_proca_refno = prof1.proca_refno								
								
left join [$(ipm)].dbo.StaffTeam t								
on r.REFTO_STEAM_REFNO = t.steam_refno								
								
LEFT Join [$(ipm)].dbo.ReferenceValue rfval								
	ON r.SORRF_REFNO = rfval.RFVAL_REFNO							
								
left join [$(ipm)].dbo.ServicePoint	c							
	on s.spont_refno = c.spont_refno
	
LEFT JOIN [$(ipm)].dbo.ProfessionalCarer prof								
      ON ISNULL(r.refto_proca_refno, 0) = prof.proca_REFNO
		
--Join to GP Practice and PCT
LEFT OUTER JOIN
(
	SELECT DISTINCT
			 EncounterPatientProfessionalCarer.SCHDL_REFNO
			,EncounterPatientProfessionalCarer.PATPC_REFNO
			,ProfessionalCarerCode
			,HealthOrgCode
			,HealthOrgDescription
			,ParentHealthOrgCode
			,ParentHealthOrgDescription
	FROM
			(
			SELECT 
				 SCHDL_REFNO
				,MAX(PATPC_REFNO) AS PATPC_REFNO
			FROM 
				(
				SELECT 
						 SCHDL_REFNO
						,START_DTTM
						,PATNT_REFNO
				 FROM 
						[$(ipm)].dbo.Schedule Encounter
				)Encounter

			LEFT OUTER JOIN
				(
				SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO
				FROM 
						[$(ipm)].dbo.PatientProfessionalCarer 
				WHERE   
						ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarer

			ON Encounter.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
			AND PatientProfCarer.START_DTTM <= Encounter.START_DTTM 
			AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > Encounter.START_DTTM)

			GROUP BY 
				SCHDL_REFNO

	)EncounterPatientProfessionalCarer

	LEFT OUTER JOIN (SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO
						,PatientProfessionalCarer.PROCA_MAIN_IDENT AS ProfessionalCarerCode
					FROM 
						[$(ipm)].dbo.PatientProfessionalCarer
					WHERE   
						 ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarerDetail
	ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO

	LEFT OUTER JOIN (	
						SELECT DISTINCT
							 HealthOrganisation.HEORG_REFNO
							 ,HealthOrganisation.[DESCRIPTION] AS HealthOrgDescription
							 ,HealthOrganisation.MAIN_IDENT AS HealthOrgCode
							 ,HealthOrganisation.PARNT_REFNO
						 FROM [$(ipm)].dbo.HealthOrganisation 
						 WHERE ARCHV_FLAG = 'N'
		
					) PatientProfCarerHealthOrg
							 
	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
	
		LEFT OUTER JOIN 
		(	
		SELECT DISTINCT
			  HealthOrganisation.HEORG_REFNO ParentHEORG_REFNO
			 ,HealthOrganisation.[DESCRIPTION] AS ParentHealthOrgDescription
			 ,HealthOrganisation.MAIN_IDENT AS ParentHealthOrgCode
			 ,HealthOrganisation.HOTYP_REFNO AS ParentHealthOrgCodeType
		 FROM [$(ipm)].dbo.HealthOrganisation 
		 WHERE ARCHV_FLAG = 'N'
		   AND HOTYP_REFNO = 629
		) ParentPatientProfCarerHealthOrg
	ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
)ProfessionalCarerBase
ON s.SCHDL_REFNO = ProfessionalCarerBase.SCHDL_REFNO
								
WHERE								
	--return records where referral is either still open or was closed after end of last calendar month
	 (
			r.CLOSR_DATE is null
		OR
			r.CLOSR_DATE > @CensusDate		
		)
	AND							
		r.ARCHV_FLAG = 'N'
	AND
		r.RECVD_DTTM <= @CensusDate
	AND 
		--nothing from the temp schedules table means no activity prior to cutoff...
		s.REFRL_REFNO is null
	AND 							
		--1.0.4. Exclude New Born Hearing Screening
		r.refto_spect_refno in ('2000820') -- Audiology
	
	--remove test patients at source from patients table
	AND
	(                                                           
              LEFT(p.NHS_IDENTIFIER, 3) <> '999'                                             
        AND                                                    
              p.Forename  not in ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')                                                
        AND                                                     
              p.Surname  not in ('xxtestpatientaavp','xxtestpatientaafe')  
      )    
                  
  	--P.McNulty 1.0.1 Exclusion criteria 
	AND NOT r.REFTO_PROCA_REFNO	in ('10540956', '10000199')
	AND NOT r.REFTO_PROCA_REFNO	in ('10540966', '10538046')
	
	AND (
			(
			r.REFTO_STEAM_REFNO not in 
			('10003232', '10003233', '150000125', '150000126', '10003234') 			
			AND
			t.archv_flag = 'N'
			)
		OR
		r.REFTO_STEAM_REFNO is null
		)
	AND (
		c.[DESCRIPTION] not like 'NHSP%'
		OR
		c.[DESCRIPTION] is null
		)
	--P.McNulty 1.0.7 remove Tier 2 ENT records
	AND (
		c.[DESCRIPTION] not like '%Central Manchester Ear and Nose Clinic%'
		OR
		c.[DESCRIPTION] is null
		)	
  		          
  --  ORDER by r.RECVD_DTTM

--********************************************************************
--MDR_Waiting_Referrals_No_Activity	-     Referrals no activity END
--********************************************************************
  
--********************************************************************
--MDR_Waiting_Recent_Attendances	-     Recent Attendances START
--******************************************************************** 

--**********************************************************************************
--Recent Attendances Step 1. evaluate referrals with attended activity post snapshot
--**********************************************************************************

SELECT  
    s.SCHDL_REFNO, s.PATNT_REFNO, s.sctyp_RefNo,
    r.REFRL_REFNO, 
    s.REFRL_REFNO as [schedule ref no],
    r.RECVD_DTTM as 'Referral Date',
    convert(decimal(10,1), datediff(hour, r.RECVD_DTTM, @CensusDate) / 168.0) as 'weeks wait',
    p.SURNAME, p.FORENAME, p.NHS_IDENTIFIER,
    r.CLOSR_DATE as [Referral Closed Date],
	s.[start_dttm],							
	ISNULL(prof.surname, 'null') + ',' + ISNULL(prof.forename, 'null') as 'Professional Carer',						
	CASE							
		WHEN s.SCTYP_REFNO = '1468' THEN 'Community Contact'						
		WHEN s.SCTYP_REFNO = '1470' THEN 'Clinic Contact'						
		ELSE 'Other'						
	END AS 'Schedule Type',							
	CASE s.SCTYP_REFNO							
		WHEN 1468 -- Community Contact						
			THEN	CASE				
						WHEN s.SCOCM_REFNO = 1457 THEN 'DNA'		
						WHEN s.SCOCM_REFNO = 1459 THEN 'Patient deceased'		
						WHEN s.SCOCM_REFNO = 2001651 THEN 'Cancelled'		
						WHEN		
								MONTH(s.ARRIVED_DTTM) IS NOT NULL
							AND	
								MONTH(s.DEPARTED_DTTM) IS NOT NULL THEN 'Attended'
						WHEN		
								MONTH(s.ARRIVED_DTTM) IS NULL
							OR	
								MONTH(s.DEPARTED_DTTM) IS NULL THEN 'Not Recorded'
						ELSE 'Other'		
					END			
		WHEN 1470 -- Clinic Contact						
			THEN	CASE				
						WHEN s.ATTND_Refno IN (		
							357,	--Attended On Time
							2868	--Patient Late / Seen
						) THEN 'Attended'		
						WHEN s.ATTND_Refno IN (		
							2870,	
							2003532,	
							2004528,	
							2004300,	
							2004301,	
							3006508	
						) THEN 'Cancelled'		
						WHEN s.ATTND_Refno IN (		
							358,	
							2000724,	
							2003494,	
							2003495	
						) THEN 'DNA'		
						WHEN s.ATTND_Refno = '45' THEN 'Not Recorded'		
						ELSE 'Other'		
					END			
	END AS Outcome,							
    --1.0.4. make unmatched explicit
	ISNULL(t.Description, 'Unknown') as 'Team',
	--P.McNulty 1.0.4 - replace age bands with age at referral
	CASE					
		WHEN (MONTH(p.DTTM_OF_BIRTH) * 100 ) + DAY(p.DTTM_OF_BIRTH) <= (MONTH(r.RECVD_DTTM) * 100 ) + DAY(r.RECVD_DTTM)				
			THEN DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM)			
		ELSE DATEDIFF(year, p.DTTM_OF_BIRTH, r.RECVD_DTTM) - 1				
	END as 'Age',					

c.description as 'clinic description',

--1.0.4. add Referral Source
rvRefSource.description as 'referral Source',
								
refspec.description as 'referral specialty',								
spec.description as 'Appt Specialty',
CASE
	WHEN s.START_DTTM = fa.FirstAppt THEN 0
	WHEN s.START_DTTM > fa.FirstAppt THEN 1
END AS FirstAppt

	,GPPracticeCode = ISNULL(ProfessionalCarerBase.HealthOrgCode, 'V81999')
	,GPPractice = ISNULL(ProfessionalCarerBase.HealthOrgDescription, 'GP Practice Code not known') 

	,CommissionerCode = ISNULL(ProfessionalCarerBase.ParentHealthOrgCode, 'V81999')
	,Commissioner = ISNULL(ProfessionalCarerBase.ParentHealthOrgDescription, 'GP Practice Code not known')  

INTO #MDR_Referrals_With_Schedules_Post_Snapshot								
								
FROM								
	[$(ipm)].dbo.Schedule s							
left join [$(ipm)].dbo.Patient p								
on s.PATNT_REFNO = p.PATNT_REFNO								
								
left join [$(ipm)].dbo.Referral r								
on s.REFRL_REFNO = r.REFRL_REFNO

--1.0.4 Referral Source Description
LEFT JOIN [$(ipm)].dbo.ReferenceValue rvRefSource
	ON
			r.SORRF_REFNO = rvRefSource.RFVAL_REFNO
		AND
			rvRefSource.RFVDM_CODE = 'SORRF'
		AND
			rvRefSource.ARCHV_FLAG = 'N'

LEFT JOIN [$(ipm)].dbo.Specialty refspec								
	ON r.REFTO_SPECT_REFNO = refspec.SPECT_REFNO	

LEFT JOIN [$(ipm)].dbo.Specialty spec								
	ON s.SPECT_REFNO = spec.SPECT_REFNO								
								
left join [$(ipm)].dbo.ProfessionalCarer prof1								
on r.REFBY_PROCA_REFNO = prof1.PROCA_REFNO								

left join [$(ipm)].dbo.StaffTeam t								
on r.REFTO_STEAM_REFNO = t.STEAM_REFNO							
								
LEFT Join [$(ipm)].dbo.ReferenceValue rfval								
	ON r.SORRF_REFNO = rfval.RFVAL_REFNO							
								
left join [$(ipm)].dbo.ServicePoint	c							
	on s.SPONT_REFNO = c.SPONT_REFNO							
								
LEFT JOIN [$(ipm)].dbo.ProfessionalCarer prof								
      ON								
	ISNULL(r.REFTO_PROCA_REFNO, 0) = prof.PROCA_REFNO
            								
LEFT JOIN (								
	SELECT 							
		s.REFRL_REFNO						
	FROM							
		[$(ipm)].dbo.Schedule s						
	INNER JOIN [$(ipm)].dbo.Patient p							
		ON						
				s.PATNT_REFNO = p.PATNT_REFNO				
			AND					
				p.ARCHV_FLAG = 'N'   				
	WHERE							
			s.REFRL_REFNO IS NOT NULL					
		AND						
			(					
					LEFT(p.NHS_IDENTIFIER, 3) = '999'			
				OR				
					p.FORENAME  in ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')			
				OR				
					p.SURNAME  in ('xxtestpatientaavp','xxtestpatientaafe')			
			)
	GROUP BY							
		s.REFRL_REFNO						
) ex -- list of schedules for test patients								
	ON s.REFRL_REFNO = ex.REFRL_REFNO
	
LEFT JOIN #MDR_First_Activity fa
	ON s.REFRL_REFNO = fa.REFRL_REFNO
	
--Join to GP Practice and PCT
LEFT OUTER JOIN
(
	SELECT DISTINCT
			 EncounterPatientProfessionalCarer.SCHDL_REFNO
			,EncounterPatientProfessionalCarer.PATPC_REFNO
			,ProfessionalCarerCode
			,HealthOrgCode
			,HealthOrgDescription
			,ParentHealthOrgCode
			,ParentHealthOrgDescription
	FROM
			(
			SELECT 
				 SCHDL_REFNO
				,MAX(PATPC_REFNO) AS PATPC_REFNO
			FROM 
				(
				SELECT 
						 SCHDL_REFNO
						,START_DTTM
						,PATNT_REFNO
				 FROM 
						[$(ipm)].dbo.Schedule Encounter
				)Encounter
			LEFT OUTER JOIN
				(
				SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO
				FROM 
						[$(ipm)].dbo.PatientProfessionalCarer 
				WHERE   
						ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarer

			ON Encounter.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
			AND PatientProfCarer.START_DTTM <= Encounter.START_DTTM 
			AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > Encounter.START_DTTM)

			GROUP BY 
				SCHDL_REFNO

	)EncounterPatientProfessionalCarer

	LEFT OUTER JOIN (SELECT
						 PatientProfessionalCarer.PATPC_REFNO
						,PatientProfessionalCarer.START_DTTM
						,PatientProfessionalCarer.END_DTTM
						,PatientProfessionalCarer.PATNT_REFNO
						,PatientProfessionalCarer.HEORG_REFNO
						,PatientProfessionalCarer.PROCA_MAIN_IDENT AS ProfessionalCarerCode
					FROM 
						[$(ipm)].dbo.PatientProfessionalCarer
					WHERE   
						 ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
				)PatientProfCarerDetail
	ON EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO

	LEFT OUTER JOIN (	
						SELECT DISTINCT
							  HealthOrganisation.HEORG_REFNO
							 ,HealthOrganisation.[DESCRIPTION] AS HealthOrgDescription
							 ,HealthOrganisation.MAIN_IDENT AS HealthOrgCode
							 ,HealthOrganisation.PARNT_REFNO
						 FROM 
							  [$(ipm)].dbo.HealthOrganisation 
						 WHERE 
							  ARCHV_FLAG = 'N'
					) PatientProfCarerHealthOrg
							 
	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
		LEFT OUTER JOIN 
		(	
		SELECT DISTINCT
			  HealthOrganisation.HEORG_REFNO ParentHEORG_REFNO
			 ,HealthOrganisation.[DESCRIPTION] AS ParentHealthOrgDescription
			 ,HealthOrganisation.MAIN_IDENT AS ParentHealthOrgCode
			 ,HealthOrganisation.HOTYP_REFNO AS ParentHealthOrgCodeType
		 FROM [$(ipm)].dbo.HealthOrganisation 
		 WHERE ARCHV_FLAG = 'N'
		   AND HOTYP_REFNO = 629
		) ParentPatientProfCarerHealthOrg
	ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
)ProfessionalCarerBase
ON s.SCHDL_REFNO = ProfessionalCarerBase.SCHDL_REFNO
      								
WHERE								
		ex.REFRL_REFNO IS NULL -- exclude test patients						
	--return records where referral is either still open or was closed after end of last calendar month
	AND (
			r.CLOSR_DATE is null
		OR
			r.CLOSR_DATE > @CensusDate
		)
	AND							
		r.ARCHV_FLAG = 'N'			
	AND							
		s.ARCHV_FLAG = 'N'						
	AND							
		s.SCTYP_REFNO in ('1470', '1468') -- Community Contacts and Outpatient Schedules	
	AND  s.CONTY_REFNO <> '2004177'		
	AND
		r.refto_spect_refno = '2000820' -- Audiology
	AND 
		r.RECVD_DTTM <= @CensusDate
	
	--...and only consider those contacts that occured before end of last calendar month	
	AND s.START_DTTM >= @CensusDate                          	

	--P.McNulty 1.0.1 Exclusion criteria
	AND NOT r.REFTO_PROCA_REFNO	in ('10540956', '10000199')
	AND NOT r.REFTO_PROCA_REFNO	in ('10540966', '10538046')
	
	--P.McNulty 1.0.4 replace r.REFTO_STEAM_REFNO criteria to [$(ipm)].dbo.[STAFF_TEAMS_V01]
	AND (
			(
				r.REFTO_STEAM_REFNO not in ('10003232', '10003233', '150000125', '150000126', '10003234') 
			AND
				t.archv_flag = 'N'
			)
		OR
			r.REFTO_STEAM_REFNO is null
		)	
	--P.McNulty 1.0.4 replace spont_refno criteria with reference to Service Points table
	AND (
		c.[DESCRIPTION] not like 'NHSP%'
		OR
		c.[DESCRIPTION] is null
		)
	--P.McNulty 1.0.7 remove Tier 2 ENT records
	AND (
		c.[DESCRIPTION] not like '%Central Manchester Ear and Nose Clinic%'
		OR
		c.[DESCRIPTION] is null
		)
--ORDER by r.REFRL_REFNO, s.start_dttm

--*************************************************************************
--Recent Attendances Step 2. restrict to attended / DNA schedules
--*************************************************************************

select 
	sch.REFRL_REFNO,
	sch.[Referral Date],
	sch.[Weeks Wait],
	sch.[NHS_Identifier] as [NHS Identifier],      
	sch.[Referral closed Date],  
	sch.[Start_Dttm] as [First Contact], 
	sch.[Professional Carer],
	sch.[Schedule Type],
	sch.[Outcome],
	sch.[Team],
	sch.[Age],
	sch.[clinic Description],
	sch.[Referral Source],
	sch.[Referral specialty],	
	sch.[Appt Specialty],
	'Y' as [Recent Activity],
	'3' as [Status Type Code],
	'Recent Activity' as [Status Type Description]
	,sch.GPPracticeCode
	,sch.GPPractice
	,sch.CommissionerCode
	,sch.Commissioner  					
              
into #MDR_Referrals_With_Attended_Schedules_Post_Snapshot
from #MDR_Referrals_With_Schedules_Post_Snapshot sch

--P.McNulty 1.0.3. reinstate DNA to waiting list
where outcome in ('attended')

--****************************************************************************
--Recent Attendances Step 3. remove duplicate schedules (keep only first)
--****************************************************************************

delete #MDR_Referrals_With_Attended_Schedules_Post_Snapshot
WHERE EXISTS (SELECT 
					1
			  FROM 
					#MDR_Referrals_With_Attended_Schedules_Post_Snapshot as selfjoin
			  WHERE 
					selfjoin.REFRL_REFNO = 
			        #MDR_Referrals_With_Attended_Schedules_Post_Snapshot.REFRL_REFNO
			  GROUP BY 
						selfjoin.REFRL_REFNO
			  HAVING 
					  MIN(selfjoin.[First Contact]) <>       
					  #MDR_Referrals_With_Attended_Schedules_Post_Snapshot.[First Contact] )
	
--***********************************************************
--Recent Attendances Step 4. Write results to perm table
--***********************************************************

select 
	sch.REFRL_REFNO,
	sch.[Referral Date],
	sch.[Weeks Wait],
	sch.[NHS Identifier],      
	sch.[Referral closed Date],  
	sch.[First Contact], 
	sch.[Professional Carer],
	sch.[Schedule Type],
	sch.[Outcome],
	sch.[Team],
	sch.[Age],
	sch.[clinic Description],
	sch.[Referral Source],
	sch.[Referral specialty],	
	sch.[Appt Specialty],
	sch.[Recent Activity],	
	sch.[Status Type Code],	
	sch.[Status Type Description]
	,sch.GPPracticeCode
	,sch.GPPractice
	,sch.CommissionerCode
	,sch.Commissioner  
INTO
	COM.MDR_Waiting_Recent_Attendances
FROM 
	#MDR_Referrals_With_Attended_Schedules_Post_Snapshot sch

--1.0.4.
--update [$(ipm)].dbo.MDR_Waiting_Recent_Attendances
--set [Activity Type] = ''
--where outcome <> 'attended'

--****************************************************************
--Recent Attendances Step 5. Update waiting times with clock reset 
--****************************************************************

--Note: As of now this interim table is not used as part of the
--final waiting list resultset [$(ipm)].dbo.MDR_Waiting_List but for
--consistency we will add the update of the waiting times...

UPDATE refset
	SET refset.[Weeks Wait] = convert(decimal(10,1),DATEDIFF(hour, fn.MostRecentActivity, @CensusDate) / 168.0)
	FROM  
			[$(ipm)].dbo.fn_MostRecentActivity(null,
									       @CensusDate, 
									       4) --4 = Clock Reset event 
									       fn
	INNER JOIN COM.MDR_Waiting_Recent_Attendances refset
	on fn.REFRL_REFNO = refset.REFRL_REFNO

--********************************************************************
--MDR_Waiting_Recent_Attendances	-     Recent Attendances END
--******************************************************************** 
 
--********************************************************************
--Update tables for cross checking	-     START
--******************************************************************** 

UPDATE na
Set [Recent Activity] = 'Y'
FROM COM.MDR_Waiting_Referrals_Not_Attended na
INNER JOIN COM.MDR_Waiting_Recent_Attendances ra
on na.[NHS Identifier] = ra.[NHS Identifier]  

UPDATE noa
Set [Recent Activity] = 'Y'
FROM COM.MDR_Waiting_Referrals_No_Activity noa
INNER JOIN COM.MDR_Waiting_Recent_Attendances ra
on noa.[NHS IDENTIFIER] = ra.[NHS Identifier]  

--deliberately join on the patient not the referral itself
--since we would want to set the flag for ALL referrals for
--that patient

--********************************************************************
--Update tables for cross checking	-     END
--******************************************************************** 

--********************************************************************
--1.0.4. Union all 	-     START
--******************************************************************** 	
--P.McNulty 1.0.4 25/07/2011. Do not explicitly write the detailed
--information about recent activity - in effect attendances - since
--the end of month snapshot, since this would result in multiple records
--per referral (one with the position pre-snapshot, one with activity
--snapshot. The recent activity flag is written into a matching 
--referral number as a 'prompt' so that the user can interrogate
--source data / iPM for more detailed information. This is why the
--union statement below does not include the third table.



SELECT 
	 ReturnPeriod = (YEAR(@CensusDate) * 100) + MONTH (@CensusDate)
	,LoadDate = CONVERT(Date,(CURRENT_TIMESTAMP),103)
	,[Return] = 'DM01'
	,ReturnItem = 'DM01WaitingList'
	,InterfaceCode = 'ipm'
	,SourceUniqueNo = WL.REFRL_REFNO
	,ItemDate = CONVERT(date, WL.[Referral Date],103)
	,ItemTime = CONVERT(smalldatetime, WL.[Referral Date],103)

	--,ItemSpecialty = WL.[Referral specialty] -- CCB 2015-11-11 column does not exist, replaced with [Appt Specialty]
	,ItemSpecialty = WL.[Appt Specialty]

	,NHSNumber = WL.[NHS Identifier]
	,ReferredByProfessionalCarer = WL.[Professional Carer]
	,ScheduleType = WL.[Schedule Type]
	,StaffTeam = [Team]
	,SeenByProfessionalCarer = ''
	,PatientAge = [Age]
	,Clinic = [clinic Description]
	,ReferralSource = [Referral Source]
	,AppointmentOutcome = [Outcome]
	,ReferralClosedDate = WL.[Referral closed Date]
	,FirstContact = WL.[First Contact]
	,AppointmentSpecialty = WL.[Appt Specialty]
	,RecentActivity = WL.[Recent Activity]
	,StatusTypeCode = WL.[Status Type Code]
	,StatusType = WL.[Status Type Description]
	,WL.[GPPracticeCode]
	,WL.[GPPractice]
	,WL.[CommissionerCode]
	,WL.[Commissioner]
	,Exclusion = NULL
	,ExclusionReason = NULL
	,ExclusionComments = NULL
INTO 
	#MDR_Waiting_List
FROM 
	COM.MDR_Waiting_Referrals_Not_Attended WL
	
UNION ALL
	 
SELECT
	 ReturnPeriod = (YEAR(@CensusDate) * 100) + MONTH (@CensusDate)
	,LoadDate = CONVERT(Date,(CURRENT_TIMESTAMP),103)
	,[Return] = 'DM01'
	,ReturnItem = 'DM01WaitingList'
	,InterfaceCode = 'ipm'
	,SourceUniqueNo = WL.REFRL_REFNO
	,ItemDate = CONVERT(date, WL.[Referral Date],103)
	,ItemTime = CONVERT(smalldatetime, WL.[Referral Date],103)
	,ItemSpecialty = WL.[referral specialty]
	,NHSNumber = WL.[NHS IDENTIFIER]
	,ReferredByProfessionalCarer = WL.[Professional Carer]
	,ScheduleType = WL.[Schedule Type]
	,StaffTeam = [Team]
	,SeenByProfessionalCarer = ''
	,PatientAge = [Age]
	,Clinic = [clinic description]
	,ReferralSource = [referral Source]
	,AppointmentOutcome = [Outcome]
	,ReferralClosedDate = WL.[Referral Closed Date]
	,FirstContact = WL.[First Contact]
	,AppointmentSpecialty = WL.[Appt Specialty]
	,RecentActivity = WL.[Recent Activity]
	,StatusTypeCode = WL.[Status Type Code]
	,StatusType = WL.[Status Type Description]
	,WL.[GPPracticeCode]
	,WL.[GPPractice]
	,WL.[CommissionerCode]
	,WL.[Commissioner]
	,Exclusion = NULL
	,ExclusionReason = NULL
	,ExclusionComments = NULL
FROM
	COM.MDR_Waiting_Referrals_No_Activity WL

--********************************************************************
--1.0.4. Union all 	-     END
--******************************************************************** 	

--1.0.6 If non attended activity has taken place exclusively outside
--of audiology, then it will not have been picked up by the match to
--#MDR_First_Activity and so there will be cases of a referral 
--appearing as both Status Type 1 and Status Type 2. In this case we
--will always choose the former. So, delete duplicates...

DELETE main

FROM 
	COM.ReturnDM01WaitingList main
INNER JOIN (
			  select SourceUniqueNo
			  from COM.ReturnDM01WaitingList
			  group by SourceUniqueNo
			  having count(ItemDate) > 1
           ) selfjoin on
main.SourceUniqueNo = selfjoin.SourceUniqueNo 

WHERE 
	main.StatusTypeCode = '2'			  
 

INSERT INTO COM.ReturnDM01WaitingList

SELECT
	   wl.ReturnPeriod
      ,wl.LoadDate
      ,wl.[Return]
      ,wl.ReturnItem
      ,wl.InterfaceCode
      ,wl.SourceUniqueNo
      ,wl.ItemDate
      ,wl.ItemTime
      ,wl.ItemSpecialty
      ,wl.NHSNumber
      ,wl.ReferredByProfessionalCarer
      ,wl.ScheduleType
      ,wl.StaffTeam
      ,wl.SeenByProfessionalCarer
      ,wl.PatientAge
      ,wl.Clinic
      ,wl.ReferralSource
      ,wl.AppointmentOutcome
      ,wl.ReferralClosedDate
      ,wl.FirstContact
      ,wl.AppointmentSpecialty
      ,wl.RecentActivity
      ,wl.StatusTypeCode
      ,wl.StatusType
      ,wl.GPPracticeCode
      ,wl.GPPractice
      ,wl.CommissionerCode
      ,wl.Commissioner
      ,Exclusion = CASE 
						WHEN Exclusions.SourceUniqueNo IS NULL THEN NULL
						ELSE 1
					END 
      ,wl.ExclusionReason
      ,wl.ExclusionComments
FROM
	#MDR_Waiting_List wl

LEFT JOIN COM.ReturnDM01WaitingListExclusions Exclusions
ON wl.SourceUniqueNo = Exclusions.SourceUniqueNo

WHERE
	Exclusions.SourceUniqueNo IS NULL
				
					
				
	
﻿
CREATE PROCEDURE [dbo].[LoadCOMSpecialty] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

-- Delete duplicates from the TImport table

TRUNCATE TABLE COM.Specialty

-- Insert new rows into encounter destination table
INSERT INTO COM.Specialty
(
	 SpecialtyID  --[SPECT_REFNO]
	 
	,SpecialtyCode --[MAIN_IDENT]
	
	,Specialty  --[DESCRIPTION]
	
	,SpecialtyParentID --[PARNT_REFNO]
	
	,SpecialtyHealthOrgOwner --[OWNER_HEORG_REFNO]
	
) 
SELECT
	 SpecialtyID  = SpecialtyID
		 
	,SpecialtyCode = SpecialtyCode
		
	,Specialty  = Specialty
	
	,SpecialtyParentID = SpecialtyParentID
	
	,SpecialtyHealthOrgOwner = SpecialtyHealthOrgOwner

FROM
	dbo.TLoadCOMSpecialty
	



/*
select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'LoadCOMEncounter', @Stats, @StartTime
*/
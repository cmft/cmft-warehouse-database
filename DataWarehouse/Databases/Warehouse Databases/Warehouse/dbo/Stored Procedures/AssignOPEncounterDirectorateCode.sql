﻿CREATE procedure [dbo].[AssignOPEncounterDirectorateCode] as


-- main mapping
update OP.Encounter
set
	 Encounter.DirectorateCode = DivisionMap.DirectorateCode
	,Encounter.ReferringSpecialtyTypeCode =
		CASE 
		WHEN DivisionMap.Diagnostic = 'Y' THEN 'D'
		WHEN DivisionMap.Diagnostic = 'N' THEN 'I'
		ELSE DivisionMap.Diagnostic
		END
from
	OP.Encounter

inner join WH.DivisionMap
on	DivisionMap.SiteCode = Encounter.SiteCode
and	DivisionMap.SpecialtyCode = Encounter.ReferringSpecialtyCode


-- specific mapping rules

update OP.Encounter
set
	DirectorateCode = 70
WHERE 
	ReferringSpecialtyCode in ('PT' , 'DIET' , 'DTCO') --Like '110 P%' Or	Specialty Like '654%'
AND AppointmentDate < '9 Jan 2009'



update OP.Encounter
set
	DirectorateCode = 0
where
	ReferringSpecialtyCode in ( 'PAED' , 'PNEP' )
AND AppointmentDate >= '7 Jan 2009'
AND SiteCode = 'MRI'


update OP.Encounter
set
	DirectorateCode = 30
where
	ReferringSpecialtyCode = 'PMET'
AND ClinicCode in ('CAAMMETT' , 'CJEWMET' , 'CJHWMET' , 'RJEWMET')


update OP.Encounter
set
	DirectorateCode = 0
WHERE 
	ReferringSpecialtyCode = 'OPHC'
AND ClinicCode in ( 'CORTHOP' , 'CPREAD' )




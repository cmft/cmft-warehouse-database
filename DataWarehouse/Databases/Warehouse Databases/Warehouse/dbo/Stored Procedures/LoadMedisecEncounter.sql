﻿

CREATE proc [dbo].[LoadMedisecEncounter]

as

set dateformat dmy

declare @StartTime datetime
		,@Elapsed int
		,@Sproc varchar(255)
		,@Stats varchar(255)
			
select @StartTime = getdate();
			
print 'Build Medisec.Encounter'

truncate table Medisec.Encounter

insert into Medisec.Encounter

(
	[SourcePatientNo]
	,[ProviderSpellNo]
	,[CaseNoteNumber]
	,EpisodicGpPracticeCode
	,EpisodicGpPracticeIsMedisecUser
	,PCTCode
	,[AdmissionDate]
	,[AdmissionTime]
	,[DischargeDate]
	,[DischargeTime]
	,[EpisodeStartDate]
	,[EpisodeEndDate]
	,[PatientCategoryCode]
	,[EndDirectorateCode]
	,[SiteCode]
	,[ManagementIntentionCode]
	,[SpecialtyCode]
	,[NationalSpecialtyCode]
	,[ConsultantCode]
	,[NeonatalLevelOfCare]
	,[PrimaryOperationCode]
	,[PrimaryDiagnosisCode]
	,[WardCode]
	,[DocumentTime]
	,[StatusCode]
	,[Comment]
	,[DocumentName]
	,[Description]
	,[DocumentTypeCode]
	,[AuthorCode]
	,[IssuedTime]
	,[IssuedByCode]
	,[SignedTime]
	,[SignedByCode]
	,[DocumentProgressCode]
	,[RequestOrderForDischarge]
	,[IsFirstDocumentForDischarge]
	,[DocumentSentGPForDischarge]
	,[TemplateCode]
	,[AdmissionReason]
	,[Diagnosis]
	,[Investigations]
	,[RecommendationsOnFutureManagement]
	,[InformationToPatientOrRelatives]
	,[ChangesToMedication]
	,[Cases]
)

select
		encounter.SourcePatientNo
		,ProviderSpellNo
		,encounter.CasenoteNumber
		,encounter.EpisodicGpPracticeCode
		,EpisodicGpPracticeIsMedisecUser = 
									case
										when gp.[PracticeCode] is not null
										then 1
										else 0
									end
		,encounter.PCTCode
		,encounter.AdmissionDate
		,encounter.AdmissionTime
		,encounter.DischargeDate
		,encounter.DischargeTime
		,encounter.EpisodeStartDate
		,encounter.EpisodeEndDate
		,encounter.PatientCategoryCode
		,encounter.EndDirectorateCode
		,encounter.EndSiteCode
		,encounter.ManagementIntentionCode
		,encounter.SpecialtyCode
		,specialty.NationalSpecialtyCode
		,encounter.ConsultantCode
		,encounter.NeonatalLevelOfCare
		,encounter.PrimaryOperationCode
		,encounter.PrimaryDiagnosisCode
		,encounter.EndWardTypeCode
		,request.DocumentTime
		,request.StatusCode
		,request.Comment
		,documentsummary.DocumentName
		,documentsummary.[Description]
		,documentsummary.DocumentTypeCode
		,documentsummary.AuthorCode
		,documentsummary.IssuedTime
		,documentsummary.IssuedByCode
		,documentsummary.SignedTime
		,documentsummary.SignedByCode
		,documentsummary.DocumentProgressCode
		,RequestOrderForDischarge = row_number() over (partition by encounter.SourcePatientNo, encounter.AdmissionTime order by request.DocumentTime) 
		,IsFirstDocumentForDischarge = 0
		,DocumentSentGPForDischarge = 0 
		,documentcontent.[TemplateCode]						
		,documentcontent.[AdmissionReason]
		,documentcontent.[Diagnosis]
		,documentcontent.[Investigations]
		,documentcontent.[RecommendationsOnFutureManagement]
		,documentcontent.InformationToPatientOrRelatives
		,documentcontent.[ChangesToMedication]
		,[Cases] = 1				
											
from 
	APC.Encounter encounter
	
--inner join
--	Medisec.InpatientRequest request 
--	on encounter.SourcePatientNo = coalesce(request.MergePatientNumber, request.PatientNumber)
--	and encounter.AdmissionTime = request.AdmissionTime
	
inner join
	Medisec.InpatientRequest request 
	on encounter.SourcePatientNo = request.PatientNumber --coalesce(request.MergePatientNumber, request.PatientNumber)
	and encounter.AdmissionTime = request.AdmissionTime

inner join 
	PAS.Specialty specialty
	on encounter.SpecialtyCode = specialty.SpecialtyCode
		
left outer join 
	Medisec.DocumentSummary documentsummary 
	on documentsummary.PatientNumber = request.PatientNumber
	and request.Comment = documentsummary.DocumentName

left outer join 
	Medisec.DocumentContent documentcontent
	on request.PatientNumber = documentcontent.PatientNumber
	and request.AdmissionDate = documentcontent.AdmissionDate
	and request.ForcebackNumber = documentcontent.ForcebackNumber
	
left outer join 
	Medisec.GPPracticeUsers gp
	on encounter.RegisteredGpPracticeCode = gp.[PracticeCode]


/* Spell only (Episode in PAS) */ 
											
where encounter.LastEpisodeInSpellIndicator = 'Y'

/* Exclude Private patients */

and isnull(encounter.LocalAdminCategoryCode,'') not in ('', 'PAY')

/* Exclude stillbirths and deaths */

and encounter.DischargeMethodCode not in ('DN', 'DP', 'SP', 'ST')

--and request.StatusCode is not null -- check inner join

/* Exclude regular day cases where LOS = 0 */

and not exists
			(
				select 
					1
				from 
					APC.Encounter e1 
				where 
					e1.ManagementIntentionCode = 'R'
					and datediff(day, e1.AdmissionDate, e1.DischargeDate) = 0
					and encounter.SourceUniqueID = e1.SourceUniqueID
			) 
		
/* Exclude Child Paeds */

and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					PAS.Specialty s1
					on e1.SpecialtyCode = s1.SpecialtyCode
				inner join 
					WH.Directorate directorate
					on e1.EndDirectorateCode = directorate.DirectorateCode

				where 
					directorate.DivisionCode = 'SMH'
					and s1.NationalSpecialtyCode = '420'
					and (
						e1.EndWardTypeCode like 'SM4%'
						or e1.EndWardTypeCode like 'SM5%'
						or e1.EndWardTypeCode like 'SM6%'
						or e1.EndWardTypeCode like 'CDU%'
						)
					and e1.DischargeDate < '01/07/2009'
					and encounter.SourceUniqueID = e1.SourceUniqueID
			)  

/* Exclude Z30.3 */

/* Removed at Ian Daniels request */	

--and not exists
--			(
--				select 
--					1
--				from 
--					APC.Encounter e1
--				inner join 
--					WH.Directorate directorate
--					on e1.EndDirectorateCode = directorate.DirectorateCode
--				where 
--					e1.PrimaryDiagnosisCode = 'Z30.3'
--					and directorate.DivisionCode = 'SMH'
--					and encounter.SourceUniqueID = e1.SourceUniqueID
--			) 
				
/* Exclude Obstetrics */

and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					WH.Directorate directorate
					on e1.EndDirectorateCode = directorate.DirectorateCode
				where 
					e1.SpecialtyCode in ('OBIC', 'OBPP', 'OBS')
					and directorate.DivisionCode = 'SMH'
					and encounter.SourceUniqueID = e1.SourceUniqueID
			)
				
				
/* Exclude Well Babies */

and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					WH.Directorate directorate
					on e1.EndDirectorateCode = directorate.DirectorateCode
				where 
					e1.SpecialtyCode in ('PWB') 
					and directorate.DivisionCode = 'SMH' 
					and coalesce(e1.NeonatalLevelOfCare,'') in ('', 'N', '0') 
					and encounter.SourceUniqueID = e1.SourceUniqueID 

			)
					
and not exists
			(
			select 
					1
				from 
					APC.Encounter e1
				inner join 
					WH.Directorate directorate
					on e1.EndDirectorateCode = directorate.DirectorateCode
				where 
					e1.SpecialtyCode in ('PWB') 
					and directorate.DivisionCode = 'SMH' 
					and encounter.SourceUniqueID = e1.SourceUniqueID 
					and
						not exists				
								(
									select
										1
									from
										APC.WardStay
									where
										e1.ProviderSpellNo = WardStay.ProviderSpellNo
									and
										WardStay.WardCode = '68'
								)
			)

/* Exclude GYN1 SEND SEND */
				
and encounter.SpecialtyCode not in ('GYN1', 'GEND', 'SEND') 


/* Exclude PDEN DENTAL */

and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					WH.Directorate directorate
					on e1.EndDirectorateCode = directorate.DirectorateCode
				where 
					directorate.DivisionCode = 'DENT' 
					and e1.EndWardTypeCode = 'PDEN'
					and encounter.SourceUniqueID = e1.SourceUniqueID
			)
				
/* Exclude GALAXY HOUSE */

and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				where 
					e1.EndWardTypeCode = '87'
				and encounter.SourceUniqueID = e1.SourceUniqueID
			)			
				
/* Births for obstetrics */	
	
/* Excluded following meeting with Ian D and Dusia 21/11/11 */
	
/*	
and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					PAS.Specialty s1
					on e1.SpecialtyCode = s1.SpecialtyCode
					inner join InfocomStaging.dbo.Births b
					on b.InternalNo = e1.SourcePatientNo
					and b.EpisodeNo = e1.SourceSpellNo
				where 
					s1.NationalSpecialtyCode = '501'
				--and encounter.SourcePatientNo = e1.SourcePatientNo
					and encounter.SourceUniqueID = e1.SourceUniqueID
			)
*/

/* Exclude M45/M77 in Surgery */	
				
and not exists	
			(	
				
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					PAS.Specialty s1
					on e1.SpecialtyCode = s1.SpecialtyCode
				inner join 
					WH.Directorate directorate
					on e1.EndDirectorateCode = directorate.DirectorateCode
				where 
					s1.NationalSpecialtyCode = '101'
					and (e1.PrimaryOperationCode like 'M45%' 
					or e1.PrimaryOperationCode like 'M77%')
					and e1.PatientCategoryCode <> 'NE'
					and directorate.DivisionCode = 'SURG'
					and encounter.SourceUniqueID = e1.SourceUniqueID

				
			)
					
/* Exclude Q11.1 at SMH */	

/* Removed at Ian Daniels request */		
			
--and not exists	
--			(	
				
--				select 
--					1
--				from 
--					APC.Encounter e1
--				inner join 
--					WH.Directorate directorate
--					on e1.EndDirectorateCode = directorate.DirectorateCode
--				where 
--						e1.PrimaryOperationCode = 'Q11.1'
--					and directorate.DivisionCode = 'SMH'
--					and encounter.SourceUniqueID = e1.SourceUniqueID
--			)
				
			
				
/* New requirement from Dusia */			
			
and not exists	
			(	
				
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					PAS.Specialty s1
					on e1.SpecialtyCode = s1.SpecialtyCode
				inner join 
					WH.Directorate directorate
					on e1.EndDirectorateCode = directorate.DirectorateCode
				where 
					s1.NationalSpecialtyCode < '252'
					and directorate.DivisionCode = 'CHILD'
					and e1.EndWardTypeCode in
											(
											'SUBA'
											,'SUBB'
											,'SUBD'
											,'SUBE'
											,'SUBM'
											,'SUBP'
											,'SUBS'
											,'SUBT'
											)
					and encounter.SourceUniqueID = e1.SourceUniqueID
			)
			


/* Exclusion from Phil Huitson - plasma patients */

and not exists	
			(	
				select 
					1
				from 
					APC.Encounter e1
					where e1.PrimaryOperationCode like 'X32.2%'
					and e1.PatientCategoryCode = 'DC'
					and encounter.SourceUniqueID = e1.SourceUniqueID
			)

and not exists	
			(	
				select 
					1
				from 
					APC.Encounter e1
				where
					 e1.SpecialtyCode = 'PLEX'
				and	e1.PatientCategoryCode = 'DC'
				and	encounter.SourceUniqueID = e1.SourceUniqueID

			)


/* Requested by Phil H 25/06/12 */

and not exists	
			(	
				select 
					1
				from 
					APC.Encounter e1
					inner join PAS.Specialty s1
					on e1.SpecialtyCode = s1.SpecialtyCode
					where e1.PrimaryOperationCode like 'X32.2%'
					and s1.NationalSpecialtyCode = '361'
					and encounter.SourceUniqueID = e1.SourceUniqueID

			)
			
			
/* Exclude Termination of Pregnancies - Requested by Ian Daniels 21/11/11 */
			
and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				where 
					(
					e1.PrimaryOperationCode like 'Q11%'
					or e1.PrimaryOperationCode like 'Q14%'
					or e1.PrimaryDiagnosisCode like 'O04%'
					or e1.PrimaryDiagnosisCode like 'O07%'
					or e1.PrimaryDiagnosisCode = 'M30.3'
					)
					and encounter.SourceUniqueID = e1.SourceUniqueID
			) 


/* ENDO location  - Requested by Phil Huitson 17/08/2012 */
			
and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				where 
					e1.EndWardTypeCode = 'ENDO'
					and encounter.SourceUniqueID = e1.SourceUniqueID
			) 

and not exists
			(
				select 
					1
				from 
					APC.Encounter e1
				inner join 
					PAS.Specialty s1
					on e1.SpecialtyCode = s1.SpecialtyCode
				where 
					s1.NationalSpecialtyCode =  '303'
					and e1.PatientCategoryCode = 'DC'
					and datediff(day, e1.DateOfBirth, e1.AdmissionDate)/365.25 >=19
					and encounter.SourceUniqueID = e1.SourceUniqueID
			) 



/* Flag IsFirstDocumentForDischarge column - handles scenario where 2 documents created and sent to GP for the same discharge */ 


update e1
set IsFirstDocumentForDischarge = 1
from Medisec.Encounter e1
where e1.RequestOrderForDischarge = 
									(
									select 
										min(RequestOrderForDischarge)
									from 
										Medisec.Encounter e2
									where 
										e1.SourcePatientNo = e2.SourcePatientNo
										and e1.AdmissionTime = e2.AdmissionTime
										and e2.DocumentProgressCode in ('G','P')
									)
							

/* Flag DocumentSentGPForDischarge column - handles scenario where 2 requests created and only one sent to GP for the same discharge */ 						
							
update e1
set DocumentSentGPForDischarge = 1
from Medisec.Encounter e1
where exists
			(
			select 
				1
			from 
				Medisec.Encounter e2
			where 
				e1.SourcePatientNo = e2.SourcePatientNo
				and e1.AdmissionTime = e2.AdmissionTime
				and e2.DocumentProgressCode in ('G','P')
			)

/* Manually collated data from SMH */

insert into Medisec.Encounter

(
	[SourcePatientNo]
	,[ProviderSpellNo]
	,[CaseNoteNumber]
	,EpisodicGpPracticeCode
	,EpisodicGpPracticeIsMedisecUser
	,PCTCode
	,[AdmissionDate]
	,[AdmissionTime]
	,[DischargeDate]
	,[DischargeTime]
	,[EpisodeStartDate]
	,[EpisodeEndDate]
	,[PatientCategoryCode]
	,[EndDirectorateCode]
	,[SiteCode]
	,[ManagementIntentionCode]
	,[SpecialtyCode]
	,[NationalSpecialtyCode]
	,[ConsultantCode]
	,[NeonatalLevelOfCare]
	,[PrimaryOperationCode]
	,[PrimaryDiagnosisCode]
	,[WardCode]
	,[DocumentTime]
	,[StatusCode]
	,[Comment]
	,[DocumentName]
	,[Description]
	,[DocumentTypeCode]
	,[AuthorCode]
	,[IssuedTime]
	,[IssuedByCode]
	,[SignedTime]
	,[SignedByCode]
	,[DocumentProgressCode]
	,[RequestOrderForDischarge]
	,[IsFirstDocumentForDischarge]
	,[DocumentSentGPForDischarge]
	,[TemplateCode]
	,[AdmissionReason]
	,[Diagnosis]
	,[Investigations]
	,[RecommendationsOnFutureManagement]
	,[InformationToPatientOrRelatives]
	,[ChangesToMedication]
	,[Cases]
)

select 
	[SourcePatientNo] = null
	,[ProviderSpellNo] = null
	--,SourceEncounterNo = 0
	--,SourceSpellNo = 0
	,CaseNoteNumber = 'AggregateDataOnly'
	,EpisodicGpPracticeCode = null
	,EpisodicGpPracticeIsMedisecUser = 0
	,PCTCode = null
	,AdmissionDate = null
	,AdmissionTime = null
	,DischargeDate
	,DischargeTime = DischargeDate
	,EpisodeStartDate = null
	,EpisodeEndDate = null
	,PatientCategoryCode = null
	,[EndDirectorateCode] = '30'
	,SiteCode = null
	,ManagementIntentionCode = null
	,SpecialtyCode = null
	,NationalSpecialtyCode = null
	,ConsultantCode = null
	,NeonatalLevelOfCare = null
	,PrimaryOperationCode = null
	,PrimaryDiagnosisCode = null
	,WardCode = left(Ward, 4)
	,DocumentTime = null
	,StatusCode = '02'
	,Comment = null
	,DocumentName = null
	,[Description] = null
	,DocumentTypeCode = 'I'
	,AuthorCode = null
	,IssuedTime = null
	,IssuedByCode = null
	,SignedTime = DischargeDate
	,SignedByCode = null
	,DocumentProgressCode = 'G'
	,RequestOrderForDischarge = 1
	,IsFirstDocumentForDischarge = 1
	,DocumentSentGPForDischarge = 1
	,TemplateCode = null
	,AdmissionReason = null
	,Diagnosis = null
	,Investigations = null
	,RecommendationsOnFutureManagement = null
	,InformationToPatientOrRelatives = null
	,ChangesToMedication = null
	,Cases = [<24Hours]
	
from
	Medisec.ManualSubmission

where
	[<24Hours] > 0

union all

select 
	[SourcePatientNo] = null
	,[ProviderSpellNo] = null
	--,SourceEncounterNo = 0
	--,SourceSpellNo = 0
	,CaseNoteNumber = 'AggregateDataOnly'
	,EpisodicGpPracticeCode = null
	,EpisodicGpPracticeIsMedisecUser = 0
	,PCTCode = null
	,AdmissionDate = null
	,AdmissionTime = null
	,DischargeDate
	,DischargeTime = DischargeDate
	,EpisodeStartDate = null
	,EpisodeEndDate = null
	,PatientCategoryCode = null
	,[EndDirectorateCode] = '30'
	,SiteCode = null
	,ManagementIntentionCode = null
	,SpecialtyCode = null
	,NationalSpecialtyCode = null
	,ConsultantCode = null
	,NeonatalLevelOfCare = null
	,PrimaryOperationCode = null
	,PrimaryDiagnosisCode = null
	,WardCode = left(Ward, 4)
	,DocumentTime = null
	,StatusCode = '02'
	,Comment = null
	,DocumentName = null
	,[Description] = null
	,DocumentTypeCode = 'I'
	,AuthorCode = null
	,IssuedTime = null
	,IssuedByCode = null
	,SignedTime = dateadd(hour, 24, DischargeDate)
	,SignedByCode = null
	,DocumentProgressCode = 'G'
	,RequestOrderForDischarge = 1
	,IsFirstDocumentForDischarge = 1
	,DocumentSentGPForDischarge = 1
	,TemplateCode = null
	,AdmissionReason = null
	,Diagnosis = null
	,Investigations = null
	,RecommendationsOnFutureManagement = null
	,InformationToPatientOrRelatives = null
	,ChangesToMedication = null
	,Cases = [24-48Hours]
	
from
	Medisec.ManualSubmission
where
	[24-48Hours] > 0
	
union all

select 
	[SourcePatientNo] = null
	,[ProviderSpellNo] = null
	--,SourceEncounterNo = 0
	--,SourceSpellNo = 0
	,CaseNoteNumber = 'AggregateDataOnly'
	,EpisodicGpPracticeCode = null
	,EpisodicGpPracticeIsMedisecUser = 0
	,PCTCode = null
	,AdmissionDate = null
	,AdmissionTime = null
	,DischargeDate
	,DischargeTime = DischargeDate
	,EpisodeStartDate = null
	,EpisodeEndDate = null
	,PatientCategoryCode = null
	,[EndDirectorateCode] = '30'
	,SiteCode = null
	,ManagementIntentionCode = null
	,SpecialtyCode = null
	,NationalSpecialtyCode = null
	,ConsultantCode = null
	,NeonatalLevelOfCare = null
	,PrimaryOperationCode = null
	,PrimaryDiagnosisCode = null
	,WardCode = left(Ward, 4)
	,DocumentTime = null
	,StatusCode = '02'
	,Comment = null
	,DocumentName = null
	,[Description] = null
	,DocumentTypeCode = 'I'
	,AuthorCode = null
	,IssuedTime = null
	,IssuedByCode = null
	,SignedTime = dateadd(hour, 49, DischargeDate)
	,SignedByCode = null
	,DocumentProgressCode = 'G'
	,RequestOrderForDischarge = 1
	,IsFirstDocumentForDischarge = 1
	,DocumentSentGPForDischarge = 1
	,TemplateCode = null
	,AdmissionReason = null
	,Diagnosis = null
	,Investigations = null
	,RecommendationsOnFutureManagement = null
	,InformationToPatientOrRelatives = null
	,ChangesToMedication = null
	,Cases = [>48Hours]
	
from
	Medisec.ManualSubmission
where
	[>48Hours] > 0	


select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;



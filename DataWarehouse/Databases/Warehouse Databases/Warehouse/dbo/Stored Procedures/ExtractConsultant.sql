﻿
create proc dbo.ExtractConsultant

as

declare @StartTime datetime
declare @Elapsed int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table dbo.TImportMedisecConsultant

insert into dbo.TImportMedisecConsultant

select [conscode]
      ,[constitle]
      ,[consinits]
      ,[conssurn]
      ,[consgmc]
      ,[consfn1]
      ,[consqual]
      ,[consjobtitle]
from [$(CMFT_MEDISEC)].[dbo].[XCons]

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


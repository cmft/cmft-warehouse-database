﻿CREATE PROCEDURE [dbo].[ExtractSymphonyAEInvestigation]
	 @from smalldatetime
	,@to smalldatetime
	,@debug bit = 0
as

-- 2010-02-12 CCB
-- This script is for release 2.28 of Symphony, which introduced some changes to the Request/Result tables


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @localfrom smalldatetime
declare @localto smalldatetime

select @StartTime = getdate()


--select
--	 @localfrom = @from
--	,@localto = dateadd(second, 24 * 60 * 60 -1, @to)

insert into dbo.TImportAEInvestigation
(
	 SourceUniqueID
	,SequenceNo
	,InvestigationCode
	,InvestigationDate
	,AESourceUniqueID
	,SourceInvestigationCode
	,ResultDate
)
select
	 Investigation.SourceUniqueID
	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY Investigation.AESourceUniqueID ORDER BY Investigation.SourceUniqueID)
	,InvestigationCode = not_text
	,Investigation.InvestigationDate
	,Investigation.AESourceUniqueID
	,Investigation.InvestigationID
	,Investigation.ResultDate
from
	(
	select
		 SourceUniqueID = convert(varchar, Request.req_reqid)
		,AESourceUniqueID = Request.req_atdid
		,InvestigationDate = Request.req_date
		,InvestigationID = Request.req_request
		,ResultDate = Result.res_date
	from
		[$(Symphony)].dbo.Request_Details Request

	left join [$(Symphony)].dbo.Result_details Result
	on	Result.res_reqid = Request.req_reqid
	and	Result.res_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'TreatmentOutcome'
			)

	where
		Request.req_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'Treatment'
			)
	and	Request.req_inactive = 0

	and	exists
	(
	select
		1
	from
		AE.InvestigationList
	where
		InvestigationList.InvestigationCode = Request.req_request
	)

	union all

	select
		 SourceUniqueID = convert(varchar, Request.req_reqid)
		,AESourceUniqueID = Request.req_atdid
		,InvestigationDate = Request.req_date
		,InvestigationID = Request.req_request
		,ResultDate = Result.res_date
	from
		[$(Symphony)].dbo.Request_Details Request

	left join [$(Symphony)].dbo.Result_details Result
	on	Result.res_reqid = Request.req_reqid
	and	Result.res_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'PathologyOutcome'
			)
	where
		Request.req_depid in 
			(
			select
				rps_value
			from
				[$(Symphony)].dbo.CFG_ReportSettings
			where
				rps_name = 'Pathology'
			)
	and	Request.req_inactive = 0
	and	Request.req_request <> 0

	union all

	select
		 SourceUniqueID = convert(varchar, Request.req_reqid)
		,AESourceUniqueID = Request.req_atdid
		,InvestigationDate = Request.req_date
		,InvestigationID = Request.req_request
		,ResultDate = Result.res_date
	from
		[$(Symphony)].dbo.Request_Details Request

	left join [$(Symphony)].dbo.Result_details Result
	on	Result.res_reqid = Request.req_reqid
	and	Result.res_depid = 237 --ICE Requests Outcome

	where
		Request.req_depid = 236 --ICE Requests
	and	Request.req_inactive = 0
	and	Request.req_request <> 0

	) Investigation

inner join [$(Symphony)].dbo.LookupMappings lookupmappings
on	Investigation.InvestigationID = lookupmappings.flm_lkpid

inner join [$(Symphony)].dbo.MappingTypes mappingtypes
on	mappingtypes.mt_id = lookupmappings.flm_mtid
and	mappingtypes.mt_name = 'CDS'

left join [$(Symphony)].dbo.Notes notes
on	notes.not_noteid = lookupmappings.flm_value

inner join [$(Symphony)].dbo.Attendance_Details Encounter
on	Encounter.atd_id = Investigation.AESourceUniqueID

inner join [$(Symphony)].dbo.Episodes Episode
on	Episode.epd_id = Encounter.atd_epdid

inner join 
	(
	select
		 Department.dpt_id
		,CommissioningSerialNo = '5NT00A'

		,PCTCode = '5NT'

		,SiteCode =
		case
		when Department.dpt_name = 'MRI ED' then 'RW3MR'
		when Department.dpt_name = 'PED' then 'RW3RC'
		when Department.dpt_name = 'EEC' then 'RW3RE'
		when Department.dpt_name = 'St Mary''s' then 'RW3SM'
		when Department.dpt_name = 'Dental' then 'RW3DH'
		end
	from
		[$(Symphony)].dbo.CFG_Dept Department
	where
		Department.dpt_name in (
			 'MRI ED'
			,'PED'
			,'EEC'
			,'St Mary''s'
			,'Dental'
			)
	) Department
on	Department.dpt_id = Episode.epd_deptid

where
--	Encounter.atd_arrivaldate between @localfrom and @localto
	Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
and	Encounter.atd_deleted = 0
and	Episode.epd_deleted = 0
and	Investigation.InvestigationDate < '1 Jan 2200'

-- only the attendances from after the switchover to Symphony
and (
		(
			Department.SiteCode = 'RW3SM'
		and	Encounter.atd_arrivaldate >= '11 Apr 2011 11:42'
		)
	or	(
			Department.SiteCode = 'RW3RC'
		and	Encounter.atd_arrivaldate >= '20 Oct 2009 09:10'
		)
	or	(
			Department.SiteCode = 'RW3MR'
		and	Encounter.atd_arrivaldate >= '20 Oct 2009 10:56'
		)

	or	(
			Department.SiteCode = 'RW3RE'
		and	Encounter.atd_arrivaldate >= '11 Sep 2010 00:00'
		)
	or	(
			Department.SiteCode = 'RW3DH' 
		and	Encounter.atd_arrivaldate >= '11 Oct 2011 00:00'
		)
	)


select @RowsInserted = @@rowcount



--now extract and unbundle the composite field

BEGIN

	/**************************************************************************/
	/* General Declarations                                                   */
	/**************************************************************************/
	declare @FetchStatus int

	declare @SourceUniqueID int
	declare @InvestigationCode varchar(255)
	declare @InvestigationDate smalldatetime
	declare @AESourceUniqueID int
	declare @ProcedureList varchar(8000)
	declare @ResultCode int
	declare @ResultDate smalldatetime
	declare @SequenceNo int


	/**************************************************************************/
	/* Declaration for trigger cursor                                         */
	/**************************************************************************/

	declare TableCursor CURSOR fast_forward FOR

		select
			 SourceUniqueID = Result.res_resid
			,InvestigationDate = Result.res_date
			,AESourceUniqueID = Result.res_atdid
			,ProcedureList = Result.res_field6
			,ResultDate = Result.res_date
		from
			[$(Symphony)].dbo.Result_details Result

		inner join [$(Symphony)].dbo.CFG_DEProcedures
		on	Result.res_depid = CFG_DEProcedures.dep_id
		and	CFG_DEProcedures.dep_name IN('DEP225','Treatment','DEP194') --GS20110920 new code added DEP225 (investigations)

		inner join [$(Symphony)].dbo.Attendance_Details Encounter
		on	Encounter.atd_id = Result.res_atdid

		inner join [$(Symphony)].dbo.Episodes Episode
		on	Episode.epd_id = Encounter.atd_epdid

		inner join 
			(
			select
				 Department.dpt_id
				,CommissioningSerialNo = '5NT00A'

				,PCTCode = '5NT'

				,SiteCode =
				case
				when Department.dpt_name = 'MRI ED' then 'RW3MR'
				when Department.dpt_name = 'PED' then 'RW3RC'
				when Department.dpt_name = 'EEC' then 'RW3RE'
				when Department.dpt_name = 'St Mary''s' then 'RW3SM'
				when Department.dpt_name = 'Dental' then 'RW3DH'
				end
			from
				[$(Symphony)].dbo.CFG_Dept Department
			where
				Department.dpt_name in (
					 'MRI ED'
					,'PED'
					,'EEC'
					,'St Mary''s'
					,'Dental'
					)
			) Department
		on	Department.dpt_id = Episode.epd_deptid

		where
			Encounter.atd_arrivaldate between @from and dateadd(second, 24 * 60 * 60 -1, @to)
		--	Encounter.atd_arrivaldate between '1 apr 2011' and dateadd(second, 24 * 60 * 60 -1, '30 apr 2011')
		and	Encounter.atd_deleted = 0
		and	Episode.epd_deleted = 0

		-- only the attendances from after the switchover to Symphony
		and (
				(
					Department.SiteCode = 'RW3SM'
				and	Encounter.atd_arrivaldate >= '11 Apr 2011 11:42'
				and CFG_DEProcedures.dep_name = 'DEP194'
				)
			or	(
					Department.SiteCode = 'RW3RC'
				and	Encounter.atd_arrivaldate >= '20 Oct 2009 09:10'
				)
			or	(
					Department.SiteCode = 'RW3MR'
				and	Encounter.atd_arrivaldate >= '20 Oct 2009 10:56'
				)

			or	(
					Department.SiteCode = 'RW3RE'
				and	Encounter.atd_arrivaldate >= '11 Sep 2010 00:00'
				)
			or	(
					Department.SiteCode = 'RW3DH' 
				and	Encounter.atd_arrivaldate >= '11 Oct 2011 00:00'
				)
			)

		and	Result.res_field6 <> ''


	/**************************************************************************/
	/* Initialisation                                                         */
	/**************************************************************************/
	OPEN TableCursor

	FETCH NEXT FROM TableCursor INTO
		 @SourceUniqueID
		,@InvestigationDate
		,@AESourceUniqueID
		,@ProcedureList
		,@ResultDate

	Select @FetchStatus = @@FETCH_STATUS


    /**************************************************************************/
    /* Main Processing Loop                                                   */
    /**************************************************************************/
    WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

	begin

		select
			 @SequenceNo = 0

		declare DetailCursor CURSOR fast_forward FOR

		select
			 ResultCode = convert(int, ItemCode)
			,InvestigationCode = notes.not_text
		from
			dbo.ParamParserFn(@ProcedureList, '||')

		inner join [$(Symphony)].dbo.LookupMappings lookupmappings
		on	lookupmappings.flm_lkpid = convert(int, ItemCode)

		inner join [$(Symphony)].dbo.MappingTypes mappingtypes
		on	mappingtypes.mt_id = lookupmappings.flm_mtid
		and	mappingtypes.mt_name = 'CDS'

		left join [$(Symphony)].dbo.Notes notes
		on	notes.not_noteid = lookupmappings.flm_value


		where
			(exists
				(
				select
					1
				from
					AE.InvestigationList
				where
					InvestigationList.InvestigationCode = convert(int, ItemCode)
				)
			)


		OPEN DetailCursor

		FETCH NEXT FROM DetailCursor INTO
			 @ResultCode
			,@InvestigationCode

		Select @FetchStatus = @@FETCH_STATUS

		/**************************************************************************/
		/* Column Processing Loop                                                 */
		/**************************************************************************/
		WHILE (@FetchStatus  <> -1) and (@FetchStatus  <> -2)

		begin

			select
				 @SequenceNo = @SequenceNo + 1

			insert into dbo.TImportAEInvestigation
			(
				 SourceUniqueID
				,SequenceNo
				,InvestigationCode
				,InvestigationDate
				,AESourceUniqueID
				,SourceInvestigationCode
				,ResultDate
			)
			select
				 @SourceUniqueID
				,@SequenceNo
				,@InvestigationCode
				,@InvestigationDate
				,@AESourceUniqueID
				,@ResultCode
				,@ResultDate

			select @RowsInserted = @RowsInserted + 1


			FETCH NEXT FROM DetailCursor INTO
				 @ResultCode
				,@InvestigationCode

			Select @FetchStatus = @@FETCH_STATUS

		end
		

		CLOSE DetailCursor
		DEALLOCATE DetailCursor


		FETCH NEXT FROM TableCursor INTO
			 @SourceUniqueID
			,@InvestigationDate
			,@AESourceUniqueID
			,@ProcedureList
			,@ResultDate

		Select @FetchStatus = @@FETCH_STATUS

    END

	/**************************************************************************/
	/* Termination                                                            */
	/**************************************************************************/
	CLOSE TableCursor
	DEALLOCATE TableCursor

END

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec Utility.WriteAuditLogEvent 'ExtractSymphonyAEInvestigation', @Stats, @StartTime

﻿
CREATE PROCEDURE [dbo].[LoadAEDischargeDrug]

as 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 @LoadStartDate = min(DischargeTime)
	,@LoadEndDate = max(DischargeTime) 
from
	dbo.TLoadAEDischargeDrug

merge
	AE.DischargeDrug target
using
	(
	select
		 SourceUniqueID
		,AESourceUniqueID
		,DischargeDate
		,DischargeTime
		,DrugCode
		,SequenceNo
		,InterfaceCode
	from
		dbo.TLoadAEDischargeDrug

	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	and	source.SequenceNo = target.SequenceNo

	when not matched by source
	and	target.DischargeTime between @LoadStartDate and @LoadEndDate

	then delete	

	when not matched
	then
		insert
			(
			SourceUniqueID
			,AESourceUniqueID
			,DischargeDate
			,DischargeTime
			,DrugCode
			,SequenceNo
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.AESourceUniqueID
			,source.DischargeDate
			,source.DischargeTime
			,source.DrugCode
			,source.SequenceNo
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not 
			(
				isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
			and isnull(target.AESourceUniqueID, 0) = isnull(source.AESourceUniqueID, 0)
			and isnull(target.DischargeDate, getdate()) = isnull(source.DischargeDate, getdate())
			and isnull(target.DischargeTime, getdate()) = isnull(source.DischargeTime, getdate())
			and isnull(target.DrugCode, 0) = isnull(source.DrugCode, 0)
			and isnull(target.SequenceNo, 0) = isnull(source.SequenceNo, 0)
			and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
			)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.AESourceUniqueID = source.AESourceUniqueID
			,target.DischargeDate = source.DischargeDate
			,target.DischargeTime = source.DischargeTime
			,target.DrugCode = source.DrugCode
			,target.SequenceNo = source.SequenceNo
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




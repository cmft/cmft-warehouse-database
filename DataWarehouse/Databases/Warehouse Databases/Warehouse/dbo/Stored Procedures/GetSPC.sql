﻿
create proc dbo.GetSPC
(@datasource SPCTableType readonly)

as

declare @data table  -- needed as cannot update TVP
(
	[ID] int identity (1,1)
	,[Group] varchar(50)
	,[Date] datetime
	,[Cases] int
	,[StepChangeStartID] int
)


/* Variables for SPC */

declare @meanstart int = 1
		,@meanend int = 1
		,@mean float 
		,@counter int = 1
		,@currentrow int = 1
		,@stepchange int = 1
		,@currentminus1 int = 0
		,@current float
		,@current1 float
		,@current2 float
		,@current3 float
		,@current4 float
		,@current5 float
		,@current6 float

		
declare @spc table 
(
	ID int identity (1,1)
	,[Group] varchar(50)
	,[Date] datetime
	--,StepChangeStartID int
	,[Cases] float
	,[MeanStart] float
	,[MeanEnd] float
	,[Mean] float
	,[MovingRange] float

)

/* Cursor to iterate across groups */

declare @group varchar(50)

if object_id('cur_group') is not null
begin
	close cur_group
	deallocate cur_group
end

declare cur_group cursor fast_forward
for 
select distinct
		[Group]
	from 
		@datasource
	order by
			[Group]
		
open cur_group
fetch next from cur_group into @group


while (@@fetch_status = 0)
begin

	insert into @data
	(
		[Group]
		,[Date]
		,[Cases]
	)

	/* Create cartesian for dates and group combinations with no values */

	select 
		[group].[Group]
		,[date].[Date]
		,Cases = coalesce(data.Cases, 0)
		
	from 
	(
		(
		select distinct 
			[Date]
		from 
			@datasource
		) [date]
		
		cross join 
		(
		select distinct 
			[Group]
		from 
			@datasource
		) [group]
	) 

	left outer join @datasource data
	on data.[Date] = [date].[Date]
	and data.[Group] = [group].[Group]
	
	where
		[group].[Group] = @group 
	order by
		[Group]
		,[Date]
	
	print @group + ' ' + cast(@@rowcount as varchar) + ' rows'
	
	while @counter <= 
				(
					select
						count(*) 
					from 
						@data
				)
						
	begin

		print 'new loop' + ' for ' + @group
		
		print 'Counter' + ' ' + cast(@counter as varchar)
		
		set @current =	
					(
					select Cases
					from @data
					where ID = @currentrow
					)
					
		print 'Current row' + ' ' + cast(@currentrow as char)			
		print 'Current cases' + ' ' + cast(@current as char)

		-- scan current and next 6 values for deviation from mean

		set @currentminus1 =	
					(
					select Cases
					from @data l2
					where l2.ID = @currentrow - 1
					and [Group] = @group -- prevents picking up data for previous group
					)
				
		set @current1 =	
					(
					select Cases
					from @data
					where ID = @currentrow + 1
					)
					
		set @current2 =	
					(
					select Cases
					from @data
					where ID = @currentrow + 2
					)
					
		set @current3 =	
					(
					select Cases
					from @data
					where ID = @currentrow + 3
					)

		set @current4 =	
					(
					select Cases
					from @data l2
					where l2.ID = @currentrow + 4
					)
					
		set @current5 =	
					(
					select Cases
					from @data
					where ID = @currentrow + 5
					)
					
		set @current6 =	
					(
					select Cases
					from @data
					where ID = @currentrow + 6
					)
					
		set @stepchange	= 
			case
				when 
					(
					(
					(
					@current > @mean
					and @current1 > @mean
					and @current2 > @mean
					and @current3 > @mean
					and @current4 > @mean
					and @current5 > @mean
					and @current6 > @mean
					)
					or 
					(
					@current < @mean
					and @current1 < @mean
					and @current2 < @mean
					and @current3 < @mean
					and @current4 < @mean
					and @current5 < @mean
					and @current6 < @mean
					)
					)
				and not exists
				(
					select
						1
					from 
						@data 
					where 
						StepChangeStartID = ID
						and ID between @currentrow - 13 and @currentrow
				)
				-- allow only after 14 week lag after previous step change
				)
				or
				(
				(
					select
						[Group]
					from 
						@data
					where ID = @currentrow
				)	
				!=
				(
					select
						[Group]
					from 
						@data
					where ID = @currentrow  - 1
				)
				)
				-- step change resets following change in group
				
				then @currentrow
				else @stepchange
			end	

		-- update data table to hold step change values
		
		update @data
		set StepChangeStartID = @stepchange
		where ID >= @currentrow
		
		print 'update step change' + ' ' + cast(@@rowcount as char)
			
		set @meanstart =	
						(
						select max(ID)
						from @data
						where ID <= @counter
						and StepChangeStartID = ID
						and [Group] = @group
						)
							
						
					
		print 'mean start' + ' ' + cast(@meanstart as char)

		set @meanend =	
					(
					select max(ID)
					from @data 
					where ID <= @counter
					)	
					
		print 'mean end' + ' ' + cast(@meanend as char)
									
		set @mean = 
					(
					select avg(Cases)
					from @data
					where ID between @meanstart
					and @meanend
					)
			
		print 'Mean' + ' ' + cast(@mean as char) 

		print 'insert row into @spc'		
		
		insert into @spc

		select 
			[Group]
			,[Date] 
			,[Cases]
			,[MeanStart] = @meanstart
			,[MeanEnd] = @meanend
			,[Mean] = @mean
			,[MovingRange] = coalesce(abs(@current - @currentminus1), 0)			
		from 
			@data l1
		where 
			ID = @counter

		set @counter = @counter + 1
		set @currentrow = @counter	

		end
	
	fetch next from cur_group into @group
	print 'next group' + ' ' + @group

	
	
end

close cur_group
deallocate cur_group

--select * from @data
--select * from @spc
--select * from @datasource

/* Result set */

select 
	[Group]
	,[Date]
	,[Cases]
	,[Mean] = (
				select 
					Mean
				from 
					@spc l2
				where 
					l2.ID = 
							(
							select 
								max(ID) 
							from 
								@spc l3
							where 
								l2.MeanStart = l3.MeanStart
								and l2.[Group] = l3.[Group]
							)
				and l2.MeanStart = l1.MeanStart
				and l2.[Group] = l1.[Group]
				)		
	,[AverageMovingRange] = (
							select 
								avg(MovingRange)
							from 
								@spc l2
							where 
								l2.ID between l1.MeanStart and (
																select 
																	max(ID) 
																from @spc l3
																where 
																	l2.MeanStart = l3.MeanStart
																	and l2.[Group] = l3.[Group]
																)
							and l2.[Group] = l1.[Group]
							)
	,LCL = (
				select 
					Mean
				from 
					@spc l2
				where 
					l2.ID = 
							(
							select 
								max(ID) 
							from 
								@spc l3
							where 
								l2.MeanStart = l3.MeanStart
								and l2.[Group] = l3.[Group]
							)
				and l2.MeanStart = l1.MeanStart
				and l2.[Group] = l1.[Group]
				) 
				-
				(
				3*
				(
				select 
					avg(MovingRange)
				from 
					@spc l2
				where 
					l2.ID between l1.MeanStart and (
													select 
														max(ID) 
													from 
														@spc l3
													where
														l2.MeanStart = l3.MeanStart
														and l2.[Group] = l3.[Group]
													)
				and l2.[Group] = l1.[Group]
				)/1.128
				)
							
	,UCL = (
				select 
					Mean
				from 
					@spc l2
				where 
					l2.ID = 
							(
							select 
								max(ID) 
							from 
								@spc l3
							where 
								l2.MeanStart = l3.MeanStart
								and l2.[Group] = l3.[Group]
							)
				and l2.MeanStart = l1.MeanStart
				and l2.[Group] = l1.[Group]
				) 
				+
				(
				3*
				(
				select 
					avg(MovingRange)
				from 
					@spc l2
				where 
					l2.ID between l1.MeanStart and (
													select 
														max(ID) 
													from 
														@spc l3
													where 
														l2.MeanStart = l3.MeanStart
														and l2.[Group] = l3.[Group]
													)
				and l2.[Group] = l1.[Group]
				)/1.128
				)			
from @spc l1


﻿
CREATE procedure [dbo].[ExtractCOMServicePoint]
	-- @from smallTime
	--,@to smallTime
as


declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @lastmodifiedTime datetime

select @StartTime = getdate()

--DECLARE @from smalldatetime = NULL
--DECLARE @to smalldatetime = NULL
--set @from = '04/01/2011'
--set @to = '04/02/2011'


TRUNCATE TABLE dbo.TImportCOMServicePoint

INSERT INTO dbo.TImportCOMServicePoint
(
	 
	 ServicePointID--[SPONT_REFNO]
	
	,ServicePointCode--[CODE]
	
	,ServicePoint--[NAME]
	
	,ServicePointDescription--[DESCRIPTION]
	
	,ServicePointSpecialty--[SPECT_REFNO]
	
	,ServicePointHealthOrgID --[HEORG_REFNO]

	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]

	,ServicePointTypeID--[SPTYP_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,HealthOrgOwner--[OWNER_HEORG_REFNO]
	 
) 
SELECT
	 ServicePointID = SPONT_REFNO
	
	,ServicePointCode = CODE
	
	,ServicePoint = [NAME]
	
	,ServicePointDescription = [DESCRIPTION]
	
	,ServicePointSpecialty = SPECT_REFNO
	
	,ServicePointHealthOrgID = HEORG_REFNO

	,StartTime = START_DTTM
	
	,EndTime = END_DTTM

	,ServicePointTypeID = SPTYP_REFNO

	,ArchiveFlag = ARCHV_FLAG
	
	,HealthOrgOwner = OWNER_HEORG_REFNO
	

FROM 
	[$(ipm)].dbo.SERVICE_POINTS_V01 ServicePoint

WHERE 
	[OWNER_HEORG_REFNO] IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
						  ,2031409) -- 5NT MANCHESTER PCT

	AND NOT EXISTS
		(
		SELECT
			1
		FROM
			[$(ipm)].dbo.SERVICE_POINTS_V01 LaterServicePoint
		WHERE
			[OWNER_HEORG_REFNO] IN(2031366 -- Q31 NORTH WEST STRATEGIC HEALTH AUTHORITY
							      ,2031409) -- 5NT MANCHESTER PCT
			AND	LaterServicePoint.SPONT_REFNO = ServicePoint.SPONT_REFNO
			AND	LaterServicePoint.MODIF_DTTM > ServicePoint.MODIF_DTTM
		)


/*

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec WriteAuditLogEvent 'ExtractCommunityEncounter', @Stats, @StartTime
*/
﻿
create proc dbo.LoadMedisecConsultant

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table Medisec.Consultant

insert into Medisec.Consultant

select [ConsultantCode]
      ,[Title]
      ,[Initials]
      ,[Surname]
      ,[GMCCode]
      ,[Forename]
      ,[Qualifications]
      ,[JobTitle]
from dbo.TLoadMedisecConsultant

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


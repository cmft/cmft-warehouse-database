﻿
CREATE procedure [dbo].[AssignAEReportableExcludePCEC]
(
	 @from smalldatetime
	,@to smalldatetime
)

as

--declare @from smalldatetime = '1 apr 2012'
--declare @to smalldatetime = '1 Sep 2012'

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


--Step 1. Set all encounters as reportable
update 
	AE.Encounter
set 
	Reportable = 1
where
	ArrivalDate between @from and @to


--Step 2. Set as excluded PCEC records where patient was transferred to WIC and 4 out of 5 matching criteria was met. 


--declare #TableVar table 
create table #TableVarPcecExclusions
( 
	 AdastraEncounterRecno int
	,Adastrasourceuniqueid varchar(50)
	,SymAttendanceNumber [varchar](max)  
	,SymEncounterRecno int  
	,SymArrivalTime [smalldatetime] NULL
	,AdastraAttendanceNumber [varchar](max) 
	,AdastraArrivaltime [smalldatetime] NULL
	,AdastraAttendanceConclusionTime [smalldatetime] NULL
	,AdastraDepartureTime [smalldatetime] NULL

)

Insert into #TableVarPcecExclusions
(
	 AdastraEncounterRecno
	,Adastrasourceuniqueid
	,SymAttendanceNumber
	,SymEncounterRecno
	,SymArrivalTime
	,AdastraAttendanceNumber
	,AdastraArrivaltime
	,AdastraAttendanceConclusionTime 
	,AdastraDepartureTime
)
Select
	 exclusions.AdastraEncounterRecno
	,exclusions.Adastrasourceuniqueid
	,exclusions.SymAttendanceNumber
	,exclusions.SymEncounterRecno
	,exclusions.SymArrivalTime

	,exclusions.AdastraAttendanceNumber
	,exclusions.AdastraArrivaltime
	,exclusions.AdastraAttendanceConclusionTime 
	,exclusions.AdastraDepartureTime
from
	AE.Encounter 

left outer join
	(
	select
		 SymAttendanceNumber = SymEncounter.AttendanceNumber
		,SymEncounterRecno = SymEncounter.EncounterRecno
		,AdastraAttendanceNumber = AdastraExclusions.AttendanceNumber
		,AdastraArrivaltime = AdastraExclusions.ArrivalTime
		,SymArrivalTime = SymEncounter.ArrivalTime
		,Adastrasourceuniqueid = AdastraExclusions.SourceUniqueID
		,AdastraEncounterRecno =AdastraExclusions.EncounterRecno
		,AdastraAttendanceConclusionTime = AdastraExclusions.AttendanceConclusionTime
		,AdastraDepartureTime = AdastraExclusions.DepartureTime
		
	from 
		(
		select
			 EncounterRecno
			,SourceUniqueID
			,AttendanceNumber
			,InterfaceCode
			,ArrivalTime
			,DepartureTime
			,AttendanceConclusionTime
			,NHSNumber
			,Postcode
			,DateOfBirth
			,PatientSurname
			,PatientForename
			,SexCode = Case
						when SexCode ='M' then '01'
						when SexCode = 'F' then '02'
					end
			,Reportable
		from
			AE.Encounter 
		where
			InterfaceCode = 'Adas'
		and SiteCode = 'RW3MR'
		and ArrivalDate between @from and @to
		) AdastraExclusions

	inner join
		(
		select
			*
		from
			AE.Encounter  
		where 
			InterfaceCode = 'SYM'
		and SiteCode = 'RW3MR'
		and SourceAttendanceDisposalCode IN ('17756' , '15140') -- 'transfer' and 'Transfered to Other health care provider'
		and	DischargeDestinationCode = '15141' -- WIC1
		and DepartureTime is not null

		) SymEncounter
	on 
		(
			AdastraExclusions.ArrivalTime between SymEncounter.DepartureTime and dateadd(hour,5,SymEncounter.DepartureTime)
		or
			(
				AdastraExclusions.ArrivalTime between SymEncounter.ArrivalTime and SymEncounter.DepartureTime
			and
				AdastraExclusions.DepartureTime > SymEncounter.DepartureTime
			)
		)
	and 
		(
			(
					SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
				and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
				and SymEncounter.SexCode = AdastraExclusions.SexCode
				and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
			)
			or
			(
					SymEncounter.Postcode = AdastraExclusions.Postcode
				and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
				and SymEncounter.SexCode = AdastraExclusions.SexCode				
				and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
			)
			or
			(
					SymEncounter.Postcode = AdastraExclusions.Postcode
				and	SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
				and SymEncounter.SexCode = AdastraExclusions.SexCode
				and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
			)
			or
			(
					SymEncounter.Postcode = AdastraExclusions.Postcode
				and	SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
				and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
				and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
			)
			or
			(
					SymEncounter.Postcode = AdastraExclusions.Postcode
				and	SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
				and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
				and SymEncounter.SexCode = AdastraExclusions.SexCode
			)
		)
	) exclusions
on	exclusions.Adastrasourceuniqueid = Encounter.SourceUniqueID

where 
	exclusions.Adastrasourceuniqueid is not null

--Exclude WIC records where transfer between MRI and WIC was found
Update
	AE.Encounter
set 
	Reportable = 0
where
	Encounter.EncounterRecno in 
(
select
	AdastraEncounterRecno
from
	#TableVarPcecExclusions PcecExclusions 
where
	not exists
	(
	select
		1
	from
		#TableVarPcecExclusions previousTableVarPcecExclusions
	where
		previousTableVarPcecExclusions.SymAttendanceNumber = PcecExclusions.SymAttendanceNumber
	and previousTableVarPcecExclusions.AdastraArrivaltime < PcecExclusions.AdastraArrivaltime
	)
)
select @updated = @@rowcount

merge into AE.Encounter

using 	
   (	
	select
		*
	from
		#TableVarPcecExclusions PcecExclusions 
	where
		not exists
			(
			Select
				1
			From
				#TableVarPcecExclusions previousTableVarPcecExclusions
			Where
				previousTableVarPcecExclusions.SymAttendanceNumber = PcecExclusions.SymAttendanceNumber
			and previousTableVarPcecExclusions.AdastraArrivaltime < PcecExclusions.AdastraArrivaltime
			) 
	) PcecExclusions 

on
	Encounter.EncounterRecno = PcecExclusions.SymEncounterRecno

when MATCHED then

update 

set
	 CarePathwayAttendanceConclusionTime = AdastraAttendanceConclusionTime
	,CarePathwayDepartureTime = AdastraDepartureTime
;

select @updated = @updated + @@rowcount




--Step 3. Set as excluded Adastra records where PCEC session was between a MRI session and 4 out of 5 matching criteria was met. 
Update
	AE.Encounter
set 
	Reportable = 0
where
	Encounter.EncounterRecno in (
		select 
			EncounterRecno 
		from 
			(
			select
				 EncounterRecno
				,AttendanceNumber
				,InterfaceCode
				,ArrivalTime
				,DepartureTime
				,NHSNumber
				,Postcode
				,DateOfBirth
				,PatientSurname
				,PatientForename
				,SexCode = Case
							when SexCode ='M' then '01'
							when SexCode = 'F' then '02'
						end
				,Reportable
			from 
				AE.Encounter
			where
				InterfaceCode = 'Adas'
			and SiteCode = 'RW3MR'
			and ArrivalDate between @from and @to
			and DepartureTime is not null
			and Reportable = 1
		)  AdasEncounter
		where 
			exists
			(
			select 
				1
			from
				AE.Encounter MRIEncounter
			where 
				MRIEncounter.InterfaceCode = 'SYM'
			--and MRIEncounter.arrivaldate >= '1 apr 2011'
			and MRIEncounter.ArrivalDate between dateadd(day, -1, @from) and dateadd(day, 1, @to)
			and AdasEncounter.ArrivalTime between MRIEncounter.ArrivalTime and MRIEncounter.DepartureTime
			and AdasEncounter.DepartureTime between MRIEncounter.ArrivalTime and MRIEncounter.DepartureTime
			and 
				(
					(
						AdasEncounter.DateOfBirth = MRIEncounter.DateOfBirth
					and AdasEncounter.PatientSurname = MRIEncounter.PatientSurname
					and AdasEncounter.SexCode= MRIEncounter.SexCode
					and left(AdasEncounter.PatientForename,1) = left(MRIEncounter.PatientForename,1)
					)
					or
					(
						AdasEncounter.Postcode = MRIEncounter.Postcode
					and AdasEncounter.PatientSurname = MRIEncounter.PatientSurname
					and AdasEncounter.SexCode= MRIEncounter.SexCode
					and left(AdasEncounter.PatientForename,1) = left(MRIEncounter.PatientForename,1)
					)
					or
					(
						AdasEncounter.Postcode = MRIEncounter.Postcode
					and	AdasEncounter.DateOfBirth = MRIEncounter.DateOfBirth
					and AdasEncounter.SexCode= MRIEncounter.SexCode
					and left(AdasEncounter.PatientForename,1) = left(MRIEncounter.PatientForename,1)
					)
					or
					(
						AdasEncounter.Postcode = MRIEncounter.Postcode
					and	AdasEncounter.DateOfBirth = MRIEncounter.DateOfBirth
					and AdasEncounter.PatientSurname = MRIEncounter.PatientSurname
					and left(AdasEncounter.PatientForename,1) = left(MRIEncounter.PatientForename,1)
					)
					or
					(
						AdasEncounter.Postcode = MRIEncounter.Postcode
					and	AdasEncounter.DateOfBirth = MRIEncounter.DateOfBirth
					and AdasEncounter.PatientSurname = MRIEncounter.PatientSurname
					and AdasEncounter.SexCode= MRIEncounter.SexCode
					)	
				)
			)
	)

select @updated = @updated + @@rowcount

--Update WIC record to excluded where a corresponding MRI record is set to 'Transfer from WIC'

--declare @TableVarPcectoMRIExclusions table 
create table #TableVarPcectoMRIExclusions
( 
	 AdastraEncounterRecno int
	,Adastrasourceuniqueid varchar(50)
	,SymAttendanceNumber [varchar](max)  
	,SymArrivalTime [smalldatetime] NULL
	,AdastraAttendanceNumber [varchar](max) 
	,AdastraArrivaltime [smalldatetime] NULL
	,AdastraDeparturetime [smalldatetime] NULL
)

Insert into #TableVarPcectoMRIExclusions
(
	 AdastraEncounterRecno
	,Adastrasourceuniqueid
	,SymAttendanceNumber
	,SymArrivalTime
	,AdastraAttendanceNumber
	,AdastraArrivaltime
	,AdastraDeparturetime
)
select
	 AdastraEncounterRecno =AdastraExclusions.EncounterRecno 
	,Adastrasourceuniqueid = AdastraExclusions.SourceUniqueID
	,SymAttendanceNumber = SymEncounter.AttendanceNumber
	,SymArrivalTime = SymEncounter.ArrivalTime
	,AdastraAttendanceNumber = AdastraExclusions.AttendanceNumber
	,AdastraArrivaltime = AdastraExclusions.ArrivalTime
	,AdastraDeparturetime = AdastraExclusions.DepartureTime
		
from 
(
	select
		 EncounterRecno
		,SourceUniqueID
		,AttendanceNumber
		,InterfaceCode
		,ArrivalTime
		,DepartureTime
		,NHSNumber
		,Postcode
		,DateOfBirth
		,PatientSurname
		,PatientForename
		,SexCode
		,Reportable
	from
		AE.Encounter  

	where 
		InterfaceCode = 'SYM'
	and SiteCode = 'RW3MR'
	and ArrivalDate between @from and @to
	and Reportable = 1
	and AttendanceCategoryCode = 1
	and DepartureTime is not null
	and ArrivalModeCode = 17727
)  SymEncounter
inner join
	(
	select
		 EncounterRecno
		,AttendanceNumber
		,SourceUniqueID
		,InterfaceCode
		,ArrivalTime
		,DepartureTime
		,NHSNumber
		,Postcode
		,DateOfBirth
		,PatientSurname
		,PatientForename
		,SexCode = Case
					when SexCode ='M' then '01'
					when SexCode = 'F' then '02'
				end
		,Reportable
	from 
		AE.Encounter
	where
		InterfaceCode = 'Adas'
	and SiteCode = 'RW3MR'
	and ArrivalDate between @from and @to
	and DepartureTime is not null
	and Reportable = 1
	) AdastraExclusions
on  
	(
		SymEncounter.ArrivalTime between AdastraExclusions.DepartureTime and dateadd(hour,5,AdastraExclusions.DepartureTime)
	or
		(
			SymEncounter.ArrivalTime between AdastraExclusions.ArrivalTime and AdastraExclusions.DepartureTime
		and
			SymEncounter.DepartureTime > AdastraExclusions.DepartureTime
		)
	)
and 
	(
		(
				SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
			and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
			and SymEncounter.SexCode = AdastraExclusions.SexCode
			and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
		)
		or
		(
				SymEncounter.Postcode = AdastraExclusions.Postcode
			and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
			and SymEncounter.SexCode = AdastraExclusions.SexCode				
			and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
		)
		or
		(
				SymEncounter.Postcode = AdastraExclusions.Postcode
			and	SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
			and SymEncounter.SexCode = AdastraExclusions.SexCode
			and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
		)
		or
		(
				SymEncounter.Postcode = AdastraExclusions.Postcode
			and	SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
			and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
			and left(SymEncounter.PatientForename,1) = left(AdastraExclusions.PatientForename,1)
		)
		or
		(
				SymEncounter.Postcode = AdastraExclusions.Postcode
			and	SymEncounter.DateOfBirth = AdastraExclusions.DateOfBirth
			and SymEncounter.PatientSurname = AdastraExclusions.PatientSurname
			and SymEncounter.SexCode = AdastraExclusions.SexCode
		)
	)



Update
	AE.Encounter
set 
	Reportable = 0
where
	Encounter.EncounterRecno in 
(
select
	AdastraEncounterRecno
from
	#TableVarPcectoMRIExclusions PcecExclusions 
where
	not exists
	(
	select
		1
	from
		#TableVarPcectoMRIExclusions previousTableVarPcecExclusions
	where
		previousTableVarPcecExclusions.SymAttendanceNumber = PcecExclusions.SymAttendanceNumber
	and previousTableVarPcecExclusions.AdastraArrivaltime > PcecExclusions.AdastraArrivaltime
	)
)

--Update Reportable Flag based on  Central ADAS ID
--Gareth Cunnah
--15 11 2013

update AE.Encounter

set
	Reportable = 0 
where
	exists
		(
		select
			1
		from 
			dbo.EntityLookup ADASCENNONREPORTABLE
		where
			ADASCENNONREPORTABLE.EntityCode = Encounter.SourceUniqueID
		and ADASCENNONREPORTABLE.EntityTypeCode = 'ADASCENNONREPORTABLE'
		)

select @updated = @updated + @@rowcount


update AE.Encounter
set
	 CarePathwayAttendanceConclusionTime = '2014-06-03 13:27:00'
	,CarePathwayDepartureTime = '2014-06-03 13:27:00'
where
	AttendanceNumber = 'MAE-14-043542-1'


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes, Period ' +
		CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
		CONVERT(varchar(11), coalesce(@to, ''))


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


﻿CREATE procedure [dbo].[ReportClinicInformation]
	 @AppointmentDate smalldatetime = null
	,@SiteCode varchar(10) = null
	,@ConsultantCode varchar(10) = null
	,@SpecialtyCode varchar(10) = null
	,@NewReview varchar(50) = null
as


select
	 Encounter.EncounterRecno
	,Encounter.DistrictNo
	,Encounter.PatientSurname
	,Encounter.PatientForename
	,Encounter.PatientTitle
	,Encounter.DateOfBirth
	,Encounter.HomePhone
	,Encounter.WorkPhone
	,Encounter.ConsultantCode

	,Consultant =
	case 
		when Encounter.ConsultantCode is null then 'No Consultant'
		when Encounter.ConsultantCode = '' then 'No Consultant'
		else
			case 
				when Consultant.Surname is null  then Encounter.ConsultantCode + ' - No Description'
				else Consultant.Surname + ', ' + coalesce(Consultant.Forename, '') + ' ' + coalesce(Consultant.Title, '')
			end
	end 


	,Encounter.SpecialtyCode
	,Specialty.Specialty

	,Encounter.AppointmentTime

	,Encounter.SiteCode

	,Site =
	case 
	when Encounter.SiteCode is null then 'Unknown Site' 
	when Site.Site is null then Encounter.SiteCode + ' - No Description' 
	else Site.Site
	end

	,NewReview =
	case when Encounter.FirstAttendanceFlag = '2' then 'Review' else 'New' end

	,Encounter.ClinicCode
	,Clinic =
		coalesce(Clinic.Comment, Clinic.Clinic)

	,Age =
	 case
		when datediff(day,DateOfBirth,AppointmentDate) =0 then '0 Days'
		when datediff(day,DateOfBirth,AppointmentDate) =1 then '1 Day'
		when datediff(day,DateOfBirth,AppointmentDate) > 1 and
			datediff(day,DateOfBirth,AppointmentDate) <= 28 then
				Convert(Varchar,datediff(day,DateOfBirth,AppointmentDate)) + ' Days'

		when datediff(day,DateOfBirth,AppointmentDate) > 28 and
			datediff(month,DateOfBirth,AppointmentDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,AppointmentDate) then 1 else 0 end
				 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month,DateOfBirth,AppointmentDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,AppointmentDate) then 1 else 0 end = 1
				then '1 Month'
		when datediff(month,DateOfBirth,AppointmentDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,AppointmentDate) then 1 else 0 end > 1 and
			 datediff(month,DateOfBirth,AppointmentDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,AppointmentDate) then 1 else 0 end <= 23
			then
			Convert(varchar,datediff(month,DateOfBirth,AppointmentDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,AppointmentDate) then 1 else 0 end) + ' Months'
		when datediff(month,DateOfBirth,AppointmentDate) -
			case  when datepart(day,DateOfBirth) > datepart(day,AppointmentDate) then 1 else 0 end > 23
			and datediff(year,DateOfBirth,AppointmentDate) -

			case  when datepart(dy,DateOfBirth) > datepart(dy,AppointmentDate) then 1 else 0 end <=100
			then
			Convert(varchar,datediff(year,DateOfBirth,AppointmentDate) -
			case  when datepart(dy,DateOfBirth) > datepart(dy,AppointmentDate) then 1 else 0 end) + '' --' Years'
		when datediff(year,DateOfBirth,AppointmentDate) -
			case  when datepart(dy,DateOfBirth) > datepart(dy,AppointmentDate) then 1 else 0 end > 100 then

			'> 100 Years'
		else 'Age Unknown'

		end

from
	OP.Encounter Encounter

left join PAS.Consultant PASConsultant 
on	PASConsultant.ConsultantCode = Encounter.ConsultantCode

left join WH.Consultant Consultant 
on	PASConsultant.NationalConsultantCode = Consultant.ConsultantCode

left join PAS.Site PASSite
on	PASSite.SiteCode = Encounter.SiteCode

left join WH.Site Site
on	Site.SiteCode = PASSite.MappedSiteCode

left join WH.SpecialtyPennine Specialty
on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

left join PAS.Clinic
on	Clinic.ClinicCode = Encounter.ClinicCode

where
	Encounter.AppointmentDate = coalesce(@AppointmentDate, dateadd(day, datediff(day, 0, getdate()), 0))

and
	(
		Site.SiteCode = @SiteCode
	or	@SiteCode is null
	)

and
	(
		Consultant.ConsultantCode = @ConsultantCode
	or	@ConsultantCode is null
	)

and
	(
		Encounter.SpecialtyCode = @SpecialtyCode
	or	@SpecialtyCode is null
	)

and
	(
		case when Encounter.FirstAttendanceFlag = '2' then 'Review' else 'New' end = @NewReview
	or	@NewReview is null
	)

and	Encounter.CancelledByCode is null


order by
	Encounter.DateOfBirth

﻿-- Lists Space Used for all user tables

CREATE PROCEDURE GetAllTableSizes

AS

DECLARE @TableName VARCHAR(200)

DECLARE tableCursor CURSOR FORWARD_ONLY

FOR 

--< TableName with SchemaName >

SELECT 

sys.schemas.name + '.' + sys.tables.name

FROM

sys.tables INNER JOIN sys.schemas

ON sys.tables.schema_id = sys.schemas.schema_id

WHERE 

sys.tables.schema_id > 1

ORDER BY

sys.schemas.schema_id

FOR READ ONLY

CREATE TABLE #TempTable

(

tableName varchar(200),

numberofRows varchar(100),

reservedSize varchar(50),

dataSize varchar(50),

indexSize varchar(50),

unusedSize varchar(50)

)

OPEN tableCursor

WHILE (1=1)

BEGIN

FETCH NEXT FROM tableCursor INTO @TableName

IF(@@FETCH_STATUS<>0)

BREAK;

INSERT #TempTable

EXEC sp_spaceused @TableName

END

CLOSE tableCursor

DEALLOCATE tableCursor

UPDATE #TempTable

SET reservedSize = REPLACE(reservedSize, ' KB', '')

SELECT 

tableName 'Table Name',

numberofRows 'Total Rows',

reservedSize 'Reserved KB',

dataSize 'Data Size',

indexSize 'Index Size',

unusedSize 'Unused Size'

FROM #TempTable

ORDER BY CONVERT(bigint,reservedSize) DESC

DROP TABLE #TempTable


﻿CREATE TYPE [dbo].[SPCTableType] AS TABLE (
    [ID]    INT          IDENTITY (1, 1) NOT NULL,
    [Group] VARCHAR (50) NULL,
    [Date]  DATETIME     NULL,
    [Cases] INT          NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC));


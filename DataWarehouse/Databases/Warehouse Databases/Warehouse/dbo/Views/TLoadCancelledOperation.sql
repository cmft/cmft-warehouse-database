﻿




CREATE view [dbo].[TLoadCancelledOperation]

as 

select 
	SourceUniqueID
	,CasenoteNumber = ltrim(CasenoteNumber)
	,DirectorateCode =
				case
				when Directorate = 'Acute & Rehab' then '26' 
				when Directorate = 'Acute Surgery' then '14'  
				when Directorate = 'Childrens' then '0'
				when Directorate = 'Childrens - CAMHS' then '62'
				when Directorate = 'Clin & Scient' then '92'
				when Directorate = 'Clinical Research Facility' then '99'
				when Directorate = 'Dental' then '50'
				when Directorate = 'Emergency Serv' then '90'
				when Directorate = 'Heart Centre' then '12'
				when Directorate = 'Laboratory Medicine' then '75'
				when Directorate = 'MH TRUST' then '60'
				when Directorate = 'N/A' then '9'
				when Directorate = 'Ophthalmology' then '40'
				when Directorate = 'Renal Medicine' then '24'		
				when Directorate = 'Spec Medicine' then '20'										
				when Directorate = 'Spec Surgery' then '10'															
				when Directorate = 'Women & Children' then '30'
				when Directorate = 'xClin & Scient' then '70'
				when Directorate = 'Medical' then '20'
				when Directorate = 'Trafford' then '80'
				end
	,NationalSpecialtyCode = cast(substring(NationalSpecialty, (charindex('(', NationalSpecialty) + 1), 3) as varchar)
	,ConsultantCode
	,AdmissionDate
	,ProcedureDate
	,WardCode
	,Anaesthetist
	,Surgeon
	,ProcedureCode
	,CancellationDate
	,CancellationReason
	,TCIDate
	,Comments
	,ProcedureCompleted = 
						case
						when ProcedureCompleted = 'Yes'
						then 1
						end
	,BreachwatchComments
	,Removed =  
			case
			when Removed = 'Yes'
			then 1
			end
	,RemovedComments
	,ReportableBreach = case
						when ReportableBreach = 'Yes'
						then 1
						end
	,ContextCode
from
	dbo.TImportCancelledOperation
	





﻿
		CREATE view [dbo].[TLoadAEAssault] as

		SELECT
			null as [SourceUniqueID]
			,AESourceUniqueID
			,AssaultDate = convert(datetime,AssaultDate,103)
			,AssaultTime = convert(datetime,AssaultDate,103) + convert(datetime,AssaultTime,103)
			,AssaultWeapon
			,AssaultWeaponDetails
			,AssaultLocation
			,AssaultLocationDetails
			,AlcoholConsumed3Hour
			,AssaultRelationship
			,AssaultRelationshipDetails
		FROM
			TImportAEAssault Assault
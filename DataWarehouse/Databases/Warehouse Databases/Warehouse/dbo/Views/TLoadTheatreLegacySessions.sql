﻿CREATE view [dbo].[TLoadTheatreLegacySessions]
as

select
	 ID

	,AmendTime =
		cast(cast(AmendDate as varchar(11)) + ' ' + right(convert(varchar, AmendTime, 113), 12) as smalldatetime)

	,Comment
	,ConsultantCode
	,Description
	,PlannedAnaesthetist

	,PrintTime =
		cast(cast(PrintDate as varchar(11)) + ' ' + right(convert(varchar, PrintTime, 113), 12) as smalldatetime)

	,ProcedureDate
	,Registrar
	,SessionStatus
	,SessionType

	,StartTime =
	case
	when datalength(StartAndEnd) < 9 then dateadd(minute, 9 * 60, ProcedureDate)
	else dateadd(minute, convert(int, left(StartAndEnd, 2) * 60) + convert(int, substring(StartAndEnd, 3, 2)), ProcedureDate)
	end

	,EndTime =
	case
	when datalength(StartAndEnd) < 9 then dateadd(minute, 17 * 60, ProcedureDate)
	else dateadd(minute, convert(int, left(right(StartAndEnd, 4), 2) * 60) + convert(int, right(StartAndEnd, 2)), ProcedureDate)
	end

	,SubTheatre
	,TheatreNumber
	,InterfaceCode

from
	dbo.TImportTheatreLegacySessions

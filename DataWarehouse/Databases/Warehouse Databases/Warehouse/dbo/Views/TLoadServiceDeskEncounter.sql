﻿

CREATE VIEW [dbo].[TLoadServiceDeskEncounter]

AS

SELECT
	 EncounterRecNo
	,SourceUniqueID
	,Request
	,StartTime
	,DeadlineTime
	,ResolvedTime
	,EndTime
	,RequestStatus
	,RequestStatusGroup
	,ResolvedFlag
	,BreachStatus 
	,PredictedBreachStatus = 
				CASE
					WHEN BreachStatus = -1 
						AND dateadd(minute, -1, dateadd(day, datediff(day, 0, getdate()), 0)) > DeadlineTime
						THEN 1
					WHEN BreachStatus = -1 
						AND dateadd(minute, -1, dateadd(day, datediff(day, 0, getdate()), 0)) <= DeadlineTime
						THEN 0
					ELSE BreachStatus
				END
	,ResolvedDurationMinutes
	,RequestDurationMinutes
	,TargetDurationMinutes
	,BreachValue =
				CASE 
					WHEN BreachStatus = 1
						THEN  Datediff(Minute,DeadlineTime,COALESCE (
																		ResolvedTime
																		,EndTime
																		,0
																	))
					WHEN BreachStatus = 0 THEN 0
				END
	,Category
	,Workgroup
	,CreatedBy
	,AssignedTo
	,CreatedTime
	,Priority
	,Template
	,Classification
	,ClosureDescription
	,AssignmentCount
	,CallerFullName
	,CallerEmail
	,CallerOrganization
	,SolutionNotes
	,History
	,Impact
	,PriorityCode
FROM
(
	SELECT  
		 EncounterRecNo
		,SourceUniqueID
		,Request
		,StartTime
		,DeadlineTime
		,ResolvedTime
		,EndTime = 
				COALESCE(EndTime
						,ResolvedTime
						)
		,RequestStatus
		,RequestStatusGroup = 
					CASE
						WHEN RequestStatus = 'Closed' THEN 'Closed'
						WHEN RequestStatus = 'Resolved' THEN 'Closed'
						WHEN RequestStatus = 'Informed' THEN 'Closed'
						ELSE 'Open'
					END
		,ResolvedFlag = 
					CASE
						WHEN ResolvedTime IS NOT NULL THEN 1
						ELSE 0
					END
		,BreachStatus = 
					CASE
						WHEN ClosureDescription IS NULL OR COALESCE (ResolvedTime,EndTime) IS NULL  THEN -1
						ELSE
							ExceededDeadlineFlag
					END	
		,ResolvedDurationMinutes = 
					CASE
						WHEN ResolvedTime IS NOT NULL	THEN Datediff(Minute,StartTime,ResolvedTime)
					END
		,RequestDurationMinutes = 
					CASE
						WHEN EndTime IS NOT NULL  THEN Datediff(Minute,StartTime,EndTime)
					END
		,TargetDurationMinutes = Datediff(Minute,StartTime, DeadlineTime)
			
		,Category
		,Workgroup
		,CreatedBy
		,AssignedTo
		,CreatedTime
		,Priority
		,Template
		,Classification
		,ClosureDescription
		,AssignmentCount
		,CallerFullName
		,CallerEmail
		,CallerOrganization
		,SolutionNotes
		,History
		,Impact
		,PriorityCode=
				CASE
					WHEN Priority = '4 Hours' THEN 'P1'
					WHEN Priority = '8 Hours' THEN 'P2'
					WHEN Priority = '5 Days' THEN 'P3'
					WHEN Priority = '2 Weeks' THEN 'P4'
					WHEN Priority = '6 weeks' THEN 'P5'
				END
	FROM 
		dbo.TImportServiceDeskEncounter
)Encounter;


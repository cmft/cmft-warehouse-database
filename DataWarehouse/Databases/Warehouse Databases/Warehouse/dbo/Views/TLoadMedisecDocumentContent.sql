﻿
create view dbo.TLoadMedisecDocumentContent

as

select 
		 cast(cast([PatientNumber] as int) as varchar(10)) as [PatientNumber]
		,[AdmissionDate]
		,cast(ForcebackNumber as int) as ForcebackNumber
		,ltrim(rtrim([TemplateCode])) as [TemplateCode]
		,[Content]
		,[Content].value('data((//template/admission/admitreason)[1])', 'varchar(max)') as AdmissionReason
		,case 
			when [TemplateCode] = 'Stroke'
			then [Content].value('data((template/diagnoses/diag/diagm/diagnosis)[1])', 'varchar(max)')
			else [Content].value('data((template/diagnoses/diagnosis)[1])', 'varchar(max)')  
		end as Diagnosis
		,case
			when [TemplateCode] = 'Stroke'
			then [Content].value('data((template/invplans,invplan)[1])', 'varchar(max)')
			when [TemplateCode] = 'Childrens'
			then 'N/A' -- cannot find appropriate match in Childrens DNF template
			when [TemplateCode] = 'Urology'
			then [Content].value('data((template/investigations/invfind)[1])', 'varchar(max)')
			else [Content].value('data((template/investigations/invitem)[1])', 'varchar(max)')
		end as Investigations
		,case
			when [TemplateCode] = 'Stroke'
			then [Content].value('data((template/mgt/mgtnote)[1])', 'varchar(max)')
			when [TemplateCode] = 'Childrens'
			then coalesce([Content].value('data((template/fmaninfo/Verbal)[1])', 'varchar(max)'),[Content].value('data((template/fmaninfo/Written)[1])', 'varchar(max)'))
			else [Content].value('data((template/fmaninfo/fman)[1])', 'varchar(max)')
		end as [RecommendationsOnFutureManagement]
		,case
			when [TemplateCode] = 'Stroke'
			then [Content].value('data((template/mgt/infotopat)[1])', 'varchar(max)')
			when [TemplateCode] = 'Childrens'
			then 'N/A' -- cannot find appropriate match in Childrens DNF template
			else [Content].value('data((template/fmaninfo/info)[1])', 'varchar(max)')
		end	as [InformationToPatientOrRelatives]
   		,[Content].value('data((template/doctor/PescchangesToMed)[1])', 'varchar(max)') as ChangesToMedication		         
 from [dbo].[TImportMedisecDocumentContent]


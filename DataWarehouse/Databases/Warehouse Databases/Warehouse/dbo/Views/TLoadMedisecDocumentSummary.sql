﻿
create view dbo.[TLoadMedisecDocumentSummary]

as

select 
	   ltrim(rtrim([DocumentName])) as [DocumentName]
      ,cast(cast([PatientNumber] as int) as varchar(10)) as [PatientNumber]
      ,ltrim(rtrim([PatientName])) as [PatientName]
      ,ltrim(rtrim([DocumentTypeCode])) as [DocumentTypeCode]
      ,ltrim(rtrim([AuthorCode])) as [AuthorCode]
      ,ltrim(rtrim([Description])) as [Description]
      ,ltrim(rtrim([TypedTime])) as [TypedTime]
      ,ltrim(rtrim([ConsultantCode])) as [ConsultantCode]
      ,[DictatedTime]
      ,ltrim(rtrim([Reference])) as [Reference]
      ,ltrim(rtrim([HospitalNumber])) as [HospitalNumber]
      ,ltrim(rtrim([Head])) as [Head]
      ,ltrim(rtrim([Farm])) as [Farm]
      ,[IssuedTime]
      ,ltrim(rtrim([IssuedByCode])) as [IssuedByCode]
      ,[SignedTime]
      ,ltrim(rtrim([SignedByCode])) as [SignedByCode]
      ,ltrim(rtrim([WardCode])) as [WardCode]
      ,ltrim(rtrim([DictatedByCode])) as [DictatedByCode]
      ,ltrim(rtrim([Group])) as [Group]
      ,ltrim(rtrim([DocumentProgressCode])) as [DocumentProgressCode]
      ,ltrim(rtrim([GPCode])) as [GPCode]
      ,ltrim(rtrim([GPPracticeCode])) as [GPPracticeCode]
      ,ltrim(rtrim([Inbox])) as [Inbox]
      ,ltrim(rtrim([Department])) as [Department]
      ,[SourceUniqueID]
      ,ltrim(rtrim([GPPracticeCodeStatusCode])) as [GPPracticeCodeStatusCode]
      ,ltrim(rtrim([GPPracticeCodeViewedByCode])) as [GPPracticeCodeViewedByCode]
      ,[GPPracticeCodeViewedTime]
      ,ltrim(rtrim([SpecialtyCode])) as [SpecialtyCode]
      ,ltrim(rtrim([EpisodeSecurityFlag])) as [EpisodeSecurityFlag]
      ,ltrim(rtrim([ZipID])) as [ZipID]
      
from [dbo].TImportMedisecDocumentSummary
--where isnumeric(PatientNumber) = 1


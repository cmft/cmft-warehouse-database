﻿

CREATE VIEW [dbo].[TLoadCOMMARGPReferralsMade]

AS

SELECT 
	
	 ReturnPeriod = (YEAR(r.[RECVD_DTTM]) * 100) + MONTH (r.[RECVD_DTTM])
	
	,LoadDate = CONVERT(Date,(CURRENT_TIMESTAMP),103)
	
	,[Return] = 'MAR'
	
	,ReturnItem =
		CASE 
		WHEN r.SORRF_REFNO in ('5300','2002324','6428','2003641') THEN 'GP Written Referrals Made' 
		ELSE 'Other Referrals Made'
		END

	,InterfaceCode = 'ipm'
	
	,SourceUniqueNo = r.REFRL_REFNO
											
	,ItemDate = CONVERT(date, r.RECVD_DTTM,103)
	
	,ItemSpecialty = spec.[DESCRIPTION]
		
	,NHSNumber = pat.NHS_IDENTIFIER
	
	,ProfessionalCarer = ISNULL(prof.SURNAME, 'null') + ',' + ISNULL(prof.FORENAME, 'null')
	
	,CommissionerCode =
		COALESCE(
			 ParentPatientProfCarerHealthOrg.ParentHealthOrgCode
			,ProfessionalCarerBase.ParentHealthOrgCode
			,'5NT' 
		)
			 
	,Commissioner =
		COALESCE(
			 ParentPatientProfCarerHealthOrg.ParentHealthOrgDescription
			,ProfessionalCarerBase.ParentHealthOrgDescription
			,'MANCHESTER PCT' 
		)	 
FROM 
	(
	SELECT	
		*
	FROM
		[$(ipm)].dbo.REFERRALS_VE02 referrals
	WHERE  NOT EXISTS
						(
						SELECT
							1
						FROM
							[$(ipm)].dbo.REFERRALS_VE02 Previousreferral
						WHERE
							Previousreferral.REFRL_REFNO = referrals.REFRL_REFNO
						AND Previousreferral.MODIF_DTTM > referrals.MODIF_DTTM
						)
	)  r

LEFT OUTER JOIN 
		(	
		SELECT DISTINCT
			 HEORG_REFNO
			 ,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS HealthOrgDescription
			 ,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS HealthOrgCode
			 ,HEALTH_ORGANISATIONS_V02.PARNT_REFNO
			 ,HEALTH_ORGANISATIONS_V02.HOTYP_REFNO
		FROM
			[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
		WHERE
			ARCHV_FLAG = 'N'
		AND HOTYP_REFNO = 626
		 
		AND NOT EXISTS
					(
					SELECT
						1
					FROM
						[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
					WHERE
						previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
					AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
					AND ARCHV_FLAG = 'N'
					AND HOTYP_REFNO = 626
					)
		) HealthOrg
			 
				
ON r.[REFBY_HEORG_REFNO] = HealthOrg.HEORG_REFNO
	
LEFT OUTER JOIN 
	(	
	SELECT DISTINCT
		  HEORG_REFNO ParentHEORG_REFNO
		 ,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS ParentHealthOrgDescription
		 ,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS ParentHealthOrgCode
		 ,HEALTH_ORGANISATIONS_V02.HOTYP_REFNO AS ParentHealthOrgCodeType
	 FROM [$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
	 WHERE ARCHV_FLAG = 'N'
	   AND HOTYP_REFNO = 629
	 
	 AND NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
				WHERE
					previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
				AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
				AND ARCHV_FLAG = 'N'
				AND HOTYP_REFNO = 629
				)
	) ParentPatientProfCarerHealthOrg
				
	ON HealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO



LEFT JOIN (SELECT
				*
		  FROM 
				[$(ipm)].dbo.REFERENCE_VALUES_V01 refvalues

		  WHERE   NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.REFERENCE_VALUES_V01 Laterrefvalues
				WHERE
					    Laterrefvalues.RFVAL_REFNO = refvalues.RFVAL_REFNO
					AND	Laterrefvalues.MODIF_DTTM > refvalues.MODIF_DTTM
				  )
		  )  val
	ON r.SORRF_REFNO = val.RFVAL_REFNO
	
LEFT JOIN (SELECT
				*
		  FROM 
				[$(ipm)].dbo.SPECIALTIES_V01 Specialty

		  WHERE   NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.SPECIALTIES_V01 LaterSpecialty
				WHERE
					    LaterSpecialty.SPECT_REFNO = Specialty.SPECT_REFNO
					AND	LaterSpecialty.MODIF_DTTM > Specialty.MODIF_DTTM
				  )
		  ) spec													
	ON r.REFTO_SPECT_REFNO = spec.SPECT_REFNO
	
LEFT JOIN (SELECT
				*
		  FROM 
				[$(ipm)].dbo.PROF_CARERS_V01 ProfCarer

		  WHERE   NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.PROF_CARERS_V01 LaterProfCarer
				WHERE
					    LaterProfCarer.PROCA_REFNO = ProfCarer.PROCA_REFNO
					AND	LaterProfCarer.MODIF_DTTM > ProfCarer.MODIF_DTTM
				  )
		  ) prof
	ON r.REFTO_PROCA_REFNO =prof.PROCA_REFNO
	
LEFT JOIN (SELECT
				*
		  FROM 
				[$(ipm)].dbo.PATIENTS_VE01 patient

		  WHERE   NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.PATIENTS_VE01 Laterpatient
				WHERE
					    Laterpatient.PATNT_REFNO = patient.PATNT_REFNO
					AND	Laterpatient.MODIF_DTTM > patient.MODIF_DTTM
				  )
		  ) pat
	ON r.PATNT_REFNO = pat.PATNT_REFNO

LEFT JOIN (
	    SELECT 
			r.REFRL_REFNO
		FROM
			(
				SELECT	
					*
				FROM
					[$(ipm)].dbo.REFERRALS_VE02 referrals
				WHERE  NOT EXISTS
						(
						SELECT
							1
						FROM
							[$(ipm)].dbo.REFERRALS_VE02 Previousreferral
						WHERE
							Previousreferral.REFRL_REFNO = referrals.REFRL_REFNO
						AND Previousreferral.MODIF_DTTM > referrals.MODIF_DTTM
						)
				) r
		INNER JOIN (SELECT
									*
							  FROM 
									[$(ipm)].dbo.PATIENTS_VE01 patient

							  WHERE   NOT EXISTS
									(
									SELECT
										1
									FROM
										[$(ipm)].dbo.PATIENTS_VE01 Laterpatient
									WHERE
											Laterpatient.PATNT_REFNO = patient.PATNT_REFNO
										AND	Laterpatient.MODIF_DTTM > patient.MODIF_DTTM
									  )
							  )p
			ON
					r.PATNT_REFNO = p.PATNT_REFNO
				AND
					p.ARCHV_FLAG = 'N'   
		WHERE
				r.REFRL_REFNO IS NOT NULL
			AND
				(
						LEFT(p.NHS_IDENTIFIER, 3) = '999'
					OR
						p.FORENAME  IN ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')
					OR
						p.SURNAME  IN ('xxtestpatientaavp','xxtestpatientaafe')
				)
		GROUP BY
			r.REFRL_REFNO
	) ex -- list of schedules for test patients	

	ON
		r.REFRL_REFNO = ex.REFRL_REFNO


LEFT OUTER JOIN
(
	SELECT DISTINCT
			 ReferralPatientProfessionalCarer.[REFRL_REFNO]
			,ReferralPatientProfessionalCarer.PATPC_REFNO
			,ProfessionalCarerCode
			,HealthOrgCode
			,ParentPatientProfCarerHealthOrg.ParentHealthOrgCode
			,ParentPatientProfCarerHealthOrg.ParentHealthOrgDescription


	FROM
			(
			SELECT 
				[REFRL_REFNO]
				,MAX(PATPC_REFNO) AS PATPC_REFNO

			FROM 
					(
					SELECT 
							 [REFRL_REFNO]
							,PATNT_REFNO
							,RECVD_DTTM
					 FROM 
							[$(ipm)].dbo.REFERRALS_VE02 referral
					 WHERE  NOT EXISTS
								(
								SELECT
									1
								FROM
									[$(ipm)].dbo.REFERRALS_VE02 Previousreferral
								WHERE
									Previousreferral.REFRL_REFNO = referral.REFRL_REFNO
								AND Previousreferral.MODIF_DTTM > referral.MODIF_DTTM
								)
					)referral

			LEFT OUTER JOIN

				(
				SELECT
						 PATIENT_PROF_CARERS_V01.PATPC_REFNO
						,PATIENT_PROF_CARERS_V01.START_DTTM
						,PATIENT_PROF_CARERS_V01.END_DTTM
						,PATIENT_PROF_CARERS_V01.PATNT_REFNO
						,PATIENT_PROF_CARERS_V01.HEORG_REFNO

				FROM 
						[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 
						
				WHERE   ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
					AND NOT EXISTS (
									SELECT
										*
									FROM
										[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
									WHERE
										PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
									AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
									AND ARCHV_FLAG = 'N'
									AND PRTYP_REFNO = '1132' -- General Practitioner
									)
				)PatientProfCarer

			ON referral.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
			AND PatientProfCarer.START_DTTM <= referral.RECVD_DTTM 
			AND (PatientProfCarer.END_DTTM IS NULL OR PatientProfCarer.END_DTTM > referral.RECVD_DTTM)

			GROUP BY 
				REFRL_REFNO

	)ReferralPatientProfessionalCarer
		

	LEFT OUTER JOIN (SELECT
						 PATIENT_PROF_CARERS_V01.PATPC_REFNO
						,PATIENT_PROF_CARERS_V01.START_DTTM
						,PATIENT_PROF_CARERS_V01.END_DTTM
						,PATIENT_PROF_CARERS_V01.PATNT_REFNO
						,PATIENT_PROF_CARERS_V01.HEORG_REFNO
						,PATIENT_PROF_CARERS_V01.PROCA_MAIN_IDENT AS ProfessionalCarerCode

				FROM 
						[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 
						
				WHERE   ARCHV_FLAG = 'N'
					AND PRTYP_REFNO = '1132'-- General Practitioner
					AND NOT EXISTS (
									SELECT
										*
									FROM
										[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
									WHERE
										PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
									AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
									AND ARCHV_FLAG = 'N'
									AND PRTYP_REFNO = '1132' -- General Practitioner
									)
				)PatientProfCarerDetail
	ON ReferralPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO


	LEFT OUTER JOIN (	
						SELECT DISTINCT
							 HEORG_REFNO
							 ,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS HealthOrgDescription
							 ,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS HealthOrgCode
							 ,HEALTH_ORGANISATIONS_V02.PARNT_REFNO
						 FROM [$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
						 WHERE ARCHV_FLAG = 'N'
						 
						 AND NOT EXISTS
									(
									SELECT
										1
									FROM
										[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
									WHERE
										previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
									AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
									AND ARCHV_FLAG = 'N'
									)
						) PatientProfCarerHealthOrg
							 
	ON PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
	
	
LEFT OUTER JOIN 
	(	
	SELECT DISTINCT
		  HEORG_REFNO ParentHEORG_REFNO
		 ,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS ParentHealthOrgDescription
		 ,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS ParentHealthOrgCode
		 ,HEALTH_ORGANISATIONS_V02.HOTYP_REFNO AS ParentHealthOrgCodeType
	 FROM [$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
	 WHERE ARCHV_FLAG = 'N'
	   AND HOTYP_REFNO = 629
	 
	 AND NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
				WHERE
					previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
				AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
				AND ARCHV_FLAG = 'N'
				AND HOTYP_REFNO = 629
				)
	) ParentPatientProfCarerHealthOrg

	ON PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
	
	
	
)ProfessionalCarerBase
ON r.REFRL_REFNO = ProfessionalCarerBase.REFRL_REFNO

WHERE 
		ex.REFRL_REFNO IS NULL -- exclude test patients
	--AND
	--	r.RECVD_DTTM BETWEEN CONVERT(DATETIME,'01/06/2011 00:00:00.000',103) AND CONVERT(DATETIME,'30/06/2011 23:59:59.997',103)
	AND
		r.ARCHV_FLAG = 'N'			
	AND
		r.SORRF_REFNO not in ('3007115','1509') --  self refs and friends/family
	AND
		prof.PROCA_REFNO_MAIN_IDENT <> 'pseu057'	--exclude t2 ENT referrals
	AND
		(r.REFTO_SPECT_REFNO = '10000054' --  Community Paeds
		
		OR
	
		(r.REFTO_SPECT_REFNO = '2000820' -- Audiology
			AND
				r.REFTO_PROCA_REFNO in ('10000011','10000014'))) -- Gillian Painter and Doreen Roberts


		

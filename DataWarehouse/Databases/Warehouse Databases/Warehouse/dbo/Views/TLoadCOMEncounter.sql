﻿








CREATE VIEW [dbo].[TLoadCOMEncounter] AS

SELECT
	 SourceEncounterID
	,SourceUniqueID --[SCHDL_REFNO]
	,SpecialtyID --[SPECT_REFNO]
	,StaffTeamID--[STEAM_REFNO]
	,ProfessionalCarerID--[PROCA_REFNO]
	,SeenByProfessionalCarerID --[SEENBY_PROCA_REFNO]
	,EncounterProfessionalCarerID
	,StartDate
	,StartTime
	,EndTime
	,ArrivedTime  --[ARRIVED_DTTM]
	,SeenTime  --[SEEN_DTTM]
	,DepartedTime --[DEPARTED_DTTM]
	,AttendedID --[ATTND_REFNO] for use in clinic activity
	,OutcomeID  --[SCOCM_REFNO] for use in contact activity
	,EncounterOutcomeCode --Derived calculated field
	,EncounterDuration --Derived calculated field
	,ReferralID  --[REFRL_REFNO]
	,ProfessionalCarerEpisodeID --[PRCAE_REFNO]
	,PatientSourceID --[PATNT_REFNO]
	,PatientSourceSystemUniqueID
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier
	,PatientIDPASNumber
	,PatientIDPatientIdentifier
	,PatientIDPatientFacilityID
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,VisitID  --[VISIT_REFNO]
	,ContactTypeID --[CONTY_REFNO]
	,EncounterTypeID
	,ScheduleTypeID --[SCTYP_REFNO]  appointments 1470 Contacts 1468 
	,ScheduleReasonID --[REASN_REFNO]
	,CanceledReasonID --[CANCR_REFNO]
	,CanceledTime --[CANCR_DTTM]
	,CancelledByID --[CANCB_REFNO]
	,MoveReasonID  --[MOVRN_REFNO]
	,MoveTime --[MOVE_DTTM]
	,ServicePointID  --[SPONT_REFNO]
	,ServicePointSessionID --[SPSSN_REFNO]
	,MoveCount --[MOVE_COUNT]
	,LocationTypeID --[LOTYP_REFNO]
	,LocationDescription --[LOCATION]
	,[LocationTypeHealthOrgID]
	,EncounterLocationID
	,ServicePointStaysID --[SSTAY_REFNO]
	,WaitingListID --[WLIST_REFNO]
	,PlannedAttendees --[PLANNED_ATTENDEES]
	,ActualAttendees --[ACTUAL_ATTENDEES]
	,ProviderSpellID --[PRVSP_REFNO]
	,RTTStatusID --[RTTST_REFNO]
	,EarliestReasinableOfferTime --[ERO_DTTM]
	,EncounterInterventionsCount
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedByID --[USER_CREATE]
	,ModifiedByID --[USER_MODIF]
	,ArchiveFlag --[ARCHV_FLAG]
	,ParentID --[PARNT_REFNO]
	,HealthOrgOwnerID --[OWNER_HEORG_REFNO]
	,Comment
	,ReferralReceivedTime
	,ReferralReceivedDate
	,ReferralClosedTime
	,ReferralClosedDate
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,CommissionerCode 
	,PctOfResidence  
	,LocalAuthorityCode 
	,DistrictOfResidence 
	,PatientEncounterRegisteredPracticeID
	,DNAReasonID
	,JointActivityFlag
	,CalledTime
FROM 
(
SELECT 
	 SourceEncounterID
	,SourceUniqueID --[SCHDL_REFNO]
	,SpecialtyID =ISNULL(SpecialtyID,-1)--[SPECT_REFNO]
	,StaffTeamID =  CASE StaffTeamID
						WHEN 1 THEN -1
						ELSE StaffTeamID
					END --[STEAM_REFNO]
	,ProfessionalCarerID--[PROCA_REFNO]
	,SeenByProfessionalCarerID --[SEENBY_PROCA_REFNO]
	,EncounterProfessionalCarerID = coalesce(SeenByProfessionalCarerID
											,ProfessionalCarerID
											,-1)
	,StartDate = CONVERT(date,StartTime,103)
	,StartTime
	,EndTime
	,ArrivedTime  --[ARRIVED_DTTM]
	,SeenTime  --[SEEN_DTTM]
	,DepartedTime --[DEPARTED_DTTM]
	,AttendedID --[ATTND_REFNO] for use in clinic activity
	,OutcomeID  --[SCOCM_REFNO] for use in contact activity
	,EncounterOutcomeCode =
		CASE ScheduleTypeID
				WHEN 1468 -- Community Contact
				THEN CASE
					--DNA
					WHEN     OutcomeID = 1457
					AND CanceledTime IS NULL
					AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) THEN 'DNAT'
					 
					--Patient Died
					WHEN     OutcomeID = 1459
					AND CanceledTime IS NULL
					--AND convert(date,StartTime)   <= convert(date,DATEADD(dd,-1,getdate())) 
					THEN 'PDCD'
					 
					--Cancelled by hospital
					WHEN   
					(   
					   OutcomeID IN(2001651
					    		   ,2003640
						    	   )
					OR 
						CanceledTime IS NOT NULL 
				    )
					AND CanceledReasonID in (3249
											,2003537
											,2003538
											,2003546
											,2004149
											,2005554
											,2006152
											,2006153
											,2006154
											,2006155
											,2006156
											,2006157
											,2006158
											,2006159
											,2006160
											,2006161
											,2006162
											,2006163
											,2006164
											,2006165
											,2006166
											,2006167
											,2006168
											,2006169)
					--AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) 
					THEN 'CHOS'
					
					--cancelled by patient
					WHEN   
					(   
					   OutcomeID IN(2001651
					    		   ,2003640
						    	   )
					OR 
						CanceledTime IS NOT NULL 
				    )
					AND CanceledReasonID in (2003548
											,2004148
											,3248
											,2003539
											,2003540
											,2003541
											,2003542
											,2003544
											,2005621
											,2006151)
					--AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) 
					THEN 'CPAT'
					
					--cancelled by Other
					WHEN   
						(   
						   OutcomeID IN(2001651
						    		   ,2003640
							    	   )
						OR 
							CanceledTime IS NOT NULL 
					    )
					AND CanceledReasonID NOT IN (3249
											,2003537
											,2003538
											,2003546
											,2004149
											,2005554
											,2006152
											,2006153
											,2006154
											,2006155
											,2006156
											,2006157
											,2006158
											,2006159
											,2006160
											,2006161
											,2006162
											,2006163
											,2006164
											,2006165
											,2006166
											,2006167
											,2006168
											,2006169
											,2003548
											,2004148
											,3248
											,2003539
											,2003540
											,2003541
											,2003542
											,2003544
											,2005621
											,2006151)
					--AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate()))
					 THEN 'COTH'
					
					--Attended
					WHEN
					    ArrivedTime IS NOT NULL
					AND	DepartedTime IS NOT NULL 
					AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) THEN 'ATTD'
					
					--Unactualised
					WHEN 
						OutcomeID NOT IN 
									  (  1210
										,1457
										,1459
										,2001651
										,2003640
									  )
					AND
						(
							 ArrivedTime IS NULL
						  OR DepartedTime IS NULL 
						)
					AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) THEN 'UACT'
					
					--Planned
					WHEN
						OutcomeID = 1210
					AND StartTime > ModifiedTime THEN 'PLND'
					
					--Not Specified
					ELSE 'NSPD'
					END

				WHEN 1470 -- Clinic Contact
				THEN	
					CASE
					-- Attended
					WHEN AttendedID IN (
									357,
									2868,
									2004151                 
								) 
					AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) THEN 'ATTD'
										
					--Cancelled by Hospital
					WHEN AttendedID IN (
											2004300
										) 
						--AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) 
						THEN 'CHOS'				
								
					--Cancelled by Patient		
					WHEN AttendedID IN (
											2004301
										) 
						--AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) 
						THEN 'CPAT'	
					
					--Cancelled by Other				
					WHEN AttendedID IN (
											2870,
											2003532,
											2004528,
											3006508
										) 
						--AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) 
						THEN 'COTH'	
					
					--DNA
					WHEN AttendedID IN (
											358,
											2000724,
											2003494,
											2003495,
											2003534
										)
						AND convert(date,StartTime) <= convert(date,DATEADD(dd,-1,getdate())) THEN 'DNAT'
					

					
					--Planned
					WHEN AttendedID NOT IN(357,
											2868,
											2004151,
											2870,
											2003532,
											2004528,
											2004300,
											2004301,
											3006508,
											358,
											2000724,
											2003494,
											2003495,
											2003534
											)
						AND StartTime > ModifiedTime  THEN 'PLND'

					--Not Specified	
					WHEN AttendedID = '45' THEN 'NSPD'

					--Not Specified
					ELSE 'NSPD'
					END
			END 
	,EncounterDuration = 
			CASE ScheduleTypeID
				WHEN 1468 -- Community Contact
				THEN	CASE
							WHEN
							ArrivedTime IS NOT NULL
										AND
							DepartedTime IS NOT NULL THEN DATEDIFF(MINUTE,ArrivedTime,DepartedTime)
							ELSE 0
						END

				WHEN 1470 -- Clinic Contact
				THEN	CASE
							WHEN AttendedID IN (
									357,
									2868                  
								) THEN DATEDIFF(MINUTE,ArrivedTime,DepartedTime)
								
								ELSE 0
							END
			END 
	,ReferralID  --[REFRL_REFNO]
	,ProfessionalCarerEpisodeID --[PRCAE_REFNO]
	,PatientSourceID --[PATNT_REFNO]
	,PatientSourceSystemUniqueID
	,PatientNHSNumber 
	,PatientNHSNumberStatusIndicator 
	,PatientIDLocalIdentifier 
	,PatientIDPASNumber 
	,PatientIDPatientIdentifier 
	,PatientIDPatientFacilityID
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath 
	,PatientSexID 
	,PatientEthnicGroupID 
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,PatientCurrentRegisteredPracticeCode
	,PatientEncounterRegisteredPracticeCode
	,PatientCurrentRegisteredGPCode
	,PatientEncounterRegisteredGPCode
	,VisitID  --[VISIT_REFNO]
	,ContactTypeID --[CONTY_REFNO]
	,EncounterTypeID = CASE ScheduleTypeID
							WHEN 1468 -- Community Contact
							THEN ContactTypeID

							WHEN 1470 -- Clinic Contact
							THEN VisitID
						END 
						
	,ScheduleTypeID --[SCTYP_REFNO]  appointments 1470 Contacts 1468 
	,ScheduleReasonID --[REASN_REFNO]
	,CanceledReasonID --[CANCR_REFNO]
	,CanceledTime --[CANCR_DTTM]
	,CancelledByID --[CANCB_REFNO]
	,MoveReasonID  --[MOVRN_REFNO]
	,MoveTime --[MOVE_DTTM]
	,ServicePointID  --[SPONT_REFNO]
	,ServicePointSessionID --[SPSSN_REFNO]
	,MoveCount --[MOVE_COUNT]
	,LocationTypeID --[LOTYP_REFNO]
	,LocationDescription --[LOCATION]
	,[LocationTypeHealthOrgID]
	,EncounterLocationID = CASE ScheduleTypeID
							WHEN 1468 -- Community Contact
							THEN CASE 
									WHEN LocationTypeHealthOrgID IS NULL 
										THEN LocationTypeID  
									ELSE CAST(
											  CAST(CASE LocationTypeHealthOrgID
														WHEN -1 THEN NULL
														ELSE LocationTypeHealthOrgID
													END
														 AS varchar(50))
											  + CAST(LocationTypeID as varchar(50))
										      AS numeric(20,0)
											  ) 
									END 

							WHEN 1470 -- Clinic Contact
							THEN ServicePointID
						END 
						
	,ServicePointStaysID --[SSTAY_REFNO]
	,WaitingListID --[WLIST_REFNO]
	,PlannedAttendees --[PLANNED_ATTENDEES]
	,ActualAttendees --[ACTUAL_ATTENDEES]
	,ProviderSpellID --[PRVSP_REFNO]
	,RTTStatusID --[RTTST_REFNO]
	,EarliestReasinableOfferTime --[ERO_DTTM]
	,EncounterInterventionsCount = ISNULL(EncounterInterventionsCount,0)
	,CreatedTime --[CREATE_DTTM]
	,ModifiedTime --[MODIF_DTTM]
	,CreatedByID --[USER_CREATE]
	,ModifiedByID --[USER_MODIF]
	,ArchiveFlag --[ARCHV_FLAG]
	,ParentID --[PARNT_REFNO]
	,HealthOrgOwnerID --[OWNER_HEORG_REFNO]
	,Comment
	,ReferralReceivedTime
	,ReferralReceivedDate = 
		CONVERT(date,ReferralReceivedTime,103)
	,ReferralClosedTime
	,ReferralClosedDate = 
		CONVERT(date,ReferralClosedTime,103)
	,ReferralReasonID
	,ReferredToSpecialtyID
	,ReferredToStaffTeamID
	,ReferredToProfCarerID
	,ReferralSourceID
	,ReferralTypeID
	,CommissionerCode = 
		Coalesce (
				 PatientRegisteredCommissionerCode
				,'5NT'
		)
	,PctOfResidence  = 
		Coalesce (
				  Postcode.PCTCode
				 ,'5NT'
		)
	,LocalAuthorityCode = 
		Coalesce (
				 Postcode.LocalAuthorityCode
				,'00BN'
		)
	,DistrictOfResidence  = 
		Coalesce (
				 Postcode.DistrictOfResidence
				,'Q31'
		)

	,PatientEncounterRegisteredPracticeID
	,DNAReasonID
	,JointActivityFlag
	,CalledTime  --[CALLED_DTTM]
	
FROM 
	dbo.TImportCOMEncounter
left outer join [$(Organisation)].dbo.Postcode
on case
		when datalength(rtrim(ltrim(TImportCOMEncounter.PatientPostcode))) = 6 then left(TImportCOMEncounter.PatientPostcode, 2) + '   ' + right(TImportCOMEncounter.PatientPostcode, 3)
		when datalength(rtrim(ltrim(TImportCOMEncounter.PatientPostcode))) = 7 then left(TImportCOMEncounter.PatientPostcode, 3) + '  ' + right(TImportCOMEncounter.PatientPostcode, 3)
		else TImportCOMEncounter.PatientPostcode
		end = Postcode.Postcode
) Encounter










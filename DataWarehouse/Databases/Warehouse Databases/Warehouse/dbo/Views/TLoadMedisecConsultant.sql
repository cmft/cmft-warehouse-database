﻿

create view dbo.TLoadMedisecConsultant

as

select 
	ConsultantCode = ltrim(rtrim(ConsultantCode))
	,Title = ltrim(rtrim(Title))
	,Initials = ltrim(rtrim(Initials))
	,Surname = ltrim(rtrim(Surname))
	,GMCCode = ltrim(rtrim(GMCCode))
	,Forename = ltrim(rtrim(Forename))
	,Qualifications = ltrim(rtrim(Qualifications))
	,JobTitle  = ltrim(rtrim(JobTitle))
from 
	dbo.TImportMedisecConsultant


﻿create view [dbo].[OlapTimeBand] as

select
	 TimeBandCode
	,MinuteBand
	,FifteenMinuteBand
	,ThirtyMinuteBand
	,HourBand
from
	WH.TimeBand

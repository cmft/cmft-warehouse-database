﻿
CREATE VIEW [dbo].[TLoadCOMLookupBase] 

AS

SELECT 
	 LookupID --[RFVAL_REFNO]
	,LookupTypeID --[RFVDM_CODE]
	,LookupDescription --[DESCRIPTION]
	,LookupCode --[MAIN_CODE]
	,LookupNHSCode
	,LookupCDSCode
	,LookupNatCode
	,ArchiveFlag --[ARCHV_FLAG]
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
FROM 
	dbo.TImportCOMLookupBase LookupBase


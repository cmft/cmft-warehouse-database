﻿



CREATE view [dbo].[TLoadCOMReferralWait] as

select
	 WaitingList.SourceUniqueID
	,WaitingList.SourceEncounterNo
	,CensusDate
	,ReferredBySpecialtyID
	,ReferredToSpecialtyID
	,SourceofReferralID
	,ReferralReceivedTime = 
			case when ReferralReceivedTime <= FirstScheduleStartTime.CreatedTime
				then ReferralReceivedTime
			else FirstScheduleStartTime.CreatedTime
			end
	,ReasonforReferralID
	,CancelledTime
	,CancelledReasonID
	,ReferralSentTime
	,UrgencyID
	,ReferralPriorityID
	,AuthoriedTime
	,ReferralClosedTime
	,ClosureReasonID
	,ReferredByProfessionalCarerID
	,ReferredByStaffTeamID
	,ReferredByHealthOrgID
	,ReferredToProfessionalCarerID
	,ReferredToHealthOrgID
	,ReferredToStaffTeamID
	,OutcomeOfReferralID
	,ReferralStatusID
	,RejectionID
	,TypeofReferralID
	,WaitingList.PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,ProfessionalCarerCurrentID
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID
	,ProfessionalCarerScheduleID
	,ProfessionalCarerHealthOrgScheduleID
	,ProfessionalCarerParentHealthOrgScheduleID
	,ProfessionalCarerReferralID
	,ProfessionalCarerHealthOrgReferralID
	,ProfessionalCarerParentHealthOrgReferralID
	,RTTStatusDate
	,RTTPatientPathwayID
	,RTTStartTime
	,RTTStartFlag
	,RTTStatusID
	,WaitingList.CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,ParentID
	,HealthOrgOwner
	,RequestedServiceID
	,WaitingList.ScheduleID
	,ScheduleSpecialtyID
	,ScheduleStaffTeamID
	,ScheduleProfessionalCarerID
	,ScheduleSeenByProfessionalCarerID
	,WaitingList.ScheduleStartTime
	,WaitingList.ScheduleEndTime
	,ScheduleArrivedTime
	,ScheduleSeenTime
	,ScheduleDepartedTime
	,ScheduleAttendedID
	,ScheduleOutcomeID
	,ScheduleVisitID
	,ScheduleContactTypeID
	,ScheduleTypeID
	,ScheduleReasonID
	,ScheduleCanceledReasonID
	,ScheduleCanceledTime
	,ScheduleCancelledByID
	,ScheduleMoveReasonID
	,ScheduleMovetime
	,ScheduleServicePointID
	,ScheduleServicePointSessionID
	,ScheduleMoveCount
	,ScheduleLocationTypeID
	,ScheduleLocationDescription
	,ScheduleLocationTypeHealthOrgID
	,ScheduleWaitingListID
	,KornerWait =
		case
				when
					datediff(
						 day
						,coalesce( 
								 WaitingList.ScheduleStartTime
								,WaitingList.ReferralReceivedTime
						)
						,WaitingList.CensusDate
					) < 0
				then datediff(day, 
							coalesce(
								(
								select
									max(MaxWL.ScheduleStartTime) 
								from
									COM.WaitPTL MaxWL
								where
									MaxWL.SourceUniqueID = WaitingList.SourceUniqueID
								and	MaxWL.ScheduleStartTime < MaxWL.CensusDate
								and	MaxWL.ScheduleOutcomeCode in ('DNAT', 'CPAT')

								)
								, WaitingList.ReferralReceivedTime
							)
						,WaitingList.CensusDate
					)
				else datediff(
							 day
							,coalesce( 
								 WaitingList.ScheduleStartTime
								,WaitingList.ReferralReceivedTime
							)
							,WaitingList.CensusDate
					)
				end  


	,BreachDate = 
		convert(smalldatetime, left(convert(varchar, 
			dateadd(day,(select NumericValue from Utility.Parameter where Parameter = 'OPDEFAULTBREACHDAYS')
			-
			case
			when WaitingList.RTTStartTime is not null then datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate)
			when datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate) < 0
			then datediff(day, 
				coalesce(
					(
					select
						max( MaxWL.ScheduleStartTime) 
					from
						dbo.TImportCOMReferralWait MaxWL
					where
						MaxWL.SourceUniqueID = WaitingList.SourceUniqueID
					and	MaxWL.ScheduleStartTime < MaxWL.CensusDate
					and	MaxWL.ScheduleOutcomeCode in ('DNAT', 'CPAT')

					)
					,WaitingList.RTTStartTime
					,WaitingList.ReferralReceivedTime)
				, WaitingList.CensusDate)
			else datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate)
			end
			, WaitingList.CensusDate)
		, 113), 11))

	,NationalBreachDate = 
		convert(smalldatetime, left(convert(varchar, 
			dateadd(day,(select NumericValue from Utility.Parameter where Parameter = 'NATIONALOPDEFAULTBREACHDAYS')
			-
			case
			when WaitingList.RTTStartTime is not null then datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate)
			when datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate) < 0
			then datediff(day, 
				coalesce(
					(
					select
						max(MaxWL.ScheduleStartTime) 
					from
						dbo.TImportCOMReferralWait MaxWL
					where
						MaxWL.SourceUniqueID = WaitingList.SourceUniqueID
					and	MaxWL.ScheduleStartTime < MaxWL.CensusDate
					and	MaxWL.ScheduleOutcomeCode in ('DNAT', 'CPAT')

					)
					,WaitingList.RTTStartTime
					, WaitingList.ReferralReceivedTime)
				, WaitingList.CensusDate)
			else datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate)
			end
			, WaitingList.CensusDate)
		, 113), 11))

	,BreachDays =
		(select NumericValue from Utility.Parameter where Parameter = 'OPDEFAULTBREACHDAYS')

	,NationalBreachDays =
		(select NumericValue from Utility.Parameter where Parameter = 'NATIONALOPDEFAULTBREACHDAYS')

	,RTTBreachDate = 
		convert(
			smalldatetime
			,left(
				convert(
					varchar
					,dateadd(
						day
						,(select NumericValue from Utility.Parameter where Parameter = 'RTTBREACHDAYS')
						-
						case
						when WaitingList.RTTStartTime is not null 
						then datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate)
						when datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate) < 0
						then datediff(
								day
								,coalesce(
									(
									select
										max(MaxWL.ScheduleStartTime) 
									from
										dbo.TImportCOMReferralWait MaxWL
									where
										MaxWL.SourceUniqueID = WaitingList.SourceUniqueID
									and	MaxWL.ScheduleStartTime < MaxWL.CensusDate
									and	MaxWL.ScheduleOutcomeCode in ('DNAT', 'CPAT')
									)
									,WaitingList.RTTStartTime
									,WaitingList.ReferralReceivedTime
								)
								,WaitingList.CensusDate
							)
						else datediff(day, WaitingList.StartWaitDate, WaitingList.CensusDate)
						end
						,WaitingList.CensusDate
						)
					,113
				)
			,11
			)
		)

	,ClockStartDate = 
		convert(
			smalldatetime
		,coalesce(
				(
					select	
						max(convert(smalldatetime, MaxWL.ScheduleStartTime)) 
					from	
						dbo.TImportCOMReferralWait MaxWL
					where
						MaxWL.SourceUniqueID = WaitingList.SourceUniqueID
					and	MaxWL.ScheduleStartTime < MaxWL.CensusDate
					and	MaxWL.ScheduleOutcomeCode in ('CPAT')
				)
				,WaitingList.RTTStartTime
				,WaitingList.StartWaitDate
				,WaitingList.ReferralReceivedTime
			) 
		)
	,ScheduleOutcomeCode
	,PatientDeathIndicator
	,CountOfDNAs 
	,CountOfHospitalCancels 
	,CountOfPatientCancels 
	,LastAppointmentFlag
	,AdditionFlag
	,FuturePatientCancelDate
	,PCTCode
	,StartWaitDate
	,EndWaitDate
	,BookedTime
	,DerivedFirstAttendanceFlag =
		case 
		when
			FirstAttendance.ScheduleID is not null then
			case 
			when FirstAttendance.ScheduleID is null then 1 
			when FirstAttendance.ScheduleID = WaitingList.ScheduleID then 1 
			when WaitingList.ScheduleStartTime < FirstAttendance.ScheduleStartTime  then 1
			else 2 
			end
		when
			FirstPlanned.ScheduleID is not null then
			case 
			when FirstPlanned.ScheduleID is null then 1 
			when FirstPlanned.ScheduleID = WaitingList.ScheduleID then 1 
			when WaitingList.ScheduleStartTime < FirstPlanned.ScheduleStartTime  then 1
			else 2 
			end
		else 1
		end
from
(
	select
		 WL.SourceUniqueID
		,WL.SourceEncounterNo
		,WL.CensusDate
		,ReferredBySpecialtyID
		,ReferredToSpecialtyID
		,SourceofReferralID
		,ReferralReceivedTime
		,ReasonforReferralID
		,CancelledTime
		,CancelledReasonID
		,ReferralSentTime
		,UrgencyID
		,ReferralPriorityID
		,AuthoriedTime
		,ReferralClosedTime
		,ClosureReasonID
		,ReferredByProfessionalCarerID
		,ReferredByStaffTeamID
		,ReferredByHealthOrgID
		,ReferredToProfessionalCarerID
		,ReferredToHealthOrgID
		,ReferredToStaffTeamID
		,OutcomeOfReferralID
		,ReferralStatusID
		,RejectionID
		,TypeofReferralID
		,PatientSourceID
		,PatientSourceSystemUniqueID
		,PatientNHSNumber
		,PatientNHSNumberStatusIndicator
		,PatientLocalIdentifier
		,PatientPASIdentifier
		,PatientIdentifier
		,PatientFacilityIdentifier
		,PatientTitleID
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,PatientPostcode
		,PatientDateOfBirth
		,PatientDateOfDeath
		,PatientSexID
		,PatientEthnicGroupID
		,PatientSpokenLanguageID
		,PatientReligionID
		,PatientMaritalStatusID
		,ProfessionalCarerCurrentID
		,ProfessionalCarerHealthOrgCurrentID
		,ProfessionalCarerParentHealthOrgCurrentID
		,ProfessionalCarerScheduleID
		,ProfessionalCarerHealthOrgScheduleID
		,ProfessionalCarerParentHealthOrgScheduleID
		,ProfessionalCarerReferralID
		,ProfessionalCarerHealthOrgReferralID
		,ProfessionalCarerParentHealthOrgReferralID
		,RTTStatusDate
		,RTTPatientPathwayID
		,RTTStartTime
		,RTTStartFlag
		,RTTStatusID
		,CreatedTime 
		,ModifiedTime
		,CreatedByID
		,ModifiedByID
		,ArchiveFlag
		,ParentID
		,HealthOrgOwner
		,RequestedServiceID
		,ScheduleID
		,ScheduleSpecialtyID
		,ScheduleStaffTeamID
		,ScheduleProfessionalCarerID
		,ScheduleSeenByProfessionalCarerID
		,ScheduleStartTime
		,ScheduleEndTime
		,ScheduleArrivedTime
		,ScheduleSeenTime
		,ScheduleDepartedTime
		,ScheduleAttendedID
		,ScheduleOutcomeID
		,ScheduleVisitID
		,ScheduleContactTypeID
		,ScheduleTypeID
		,ScheduleReasonID
		,ScheduleCanceledReasonID
		,ScheduleCanceledTime
		,ScheduleCancelledByID
		,ScheduleMoveReasonID
		,ScheduleMovetime
		,ScheduleServicePointID
		,ScheduleServicePointSessionID
		,ScheduleMoveCount
		,ScheduleLocationTypeID
		,ScheduleLocationDescription
		,ScheduleLocationTypeHealthOrgID
		,ScheduleWaitingListID
		,ScheduleOutcomeCode
		,FirstAttendanceFlag = 	null
		,PatientDeathIndicator =
			case
			when PatientDateOfDeath is null then 0
			else 1
			end

		,CountOfDNAs = 
			Counts.DNAs
		
		,CountOfHospitalCancels = 
			Counts.HospitalCancels
		
		,CountOfPatientCancels = 
			Counts.PatientCancels

		,LastAppointmentFlag = 
			case
				when WL.ScheduleStartTime is null	then 0
				when
					WL.ScheduleStartTime =
					(	
					select
						max(LastWL.ScheduleStartTime)
					from
						dbo.TImportCOMReferralWait LastWL
					where
						LastWL.SourceUniqueID = WL.SourceUniqueID
					and	LastWL.CensusDate = WL.CensusDate
					and	LastWL.ScheduleOutcomeCode = 'ATTD'
					) then 1

				when exists
					(
					select
						1
					from
						dbo.TImportCOMReferralWait LastWL
					where
						LastWL.SourceUniqueID = WL.SourceUniqueID
					and	LastWL.CensusDate = WL.CensusDate
					and	LastWL.ScheduleOutcomeCode = 'ATTD'
					) then 0

				when not exists
					(	
					select
						1
					from
						dbo.TImportCOMReferralWait LastWL
					where
						LastWL.SourceUniqueID = WL.SourceUniqueID
					and	LastWL.CensusDate = WL.CensusDate
					and	
						(
							LastWL.ScheduleStartTime > WL.ScheduleStartTime
						or	(
								LastWL.ScheduleStartTime = WL.ScheduleStartTime
							and	LastWL.CreatedTime >  WL.CreatedTime
							)
						)
					)	then 1
				else 0
				end
		
		,AdditionFlag = null
			--case
			--when not exists
			--	(
			--		select
			--			1
			--		from
			--			COM.Wait LastWL
			--		where
			--			LastWL.CensusDate = dateadd(day, -7, convert(smalldatetime, left(convert(varchar, WL.CensusDate, 113), 11)))
			--		and	LastWL.PatientSourceID = WL.PatientSourceID
			--		and	LastWL.SpecialtyID = WL.SpecialtyID
			--	) then 1
			--else 0 
			--end 

	-- if the next appointment has been cancelled by the patient and the appointment
	-- date is after the census date then use it
		,FuturePatientCancelDate = 
			(
				select	
					--2014-02-07 CH Added aggregate function to guarantee returning one row
					 min(MaxWL.ScheduleStartTime)
				from	
					dbo.TImportCOMReferralWait MaxWL
				where
					MaxWL.SourceUniqueID = WL.SourceUniqueID
				and	MaxWL.ScheduleStartTime >= MaxWL.CensusDate
				and	MaxWL.ScheduleOutcomeCode in ('CPAT')
			)

		,PCTCode = null
			--coalesce(
			--	 ProfessionalCarerParentHealthOrgScheduleID
			--	,'5NT'
			--)

		-- if the next appointment has been cancelled by the patient and the appointment
		-- date is prior to the census date then use it otherwise use the referral date
		,StartWaitDate = 
			coalesce(
				(
				select	
					max(convert(smalldatetime, MaxWL.ScheduleStartTime)) 
				from	
					dbo.TImportCOMReferralWait MaxWL
				where
					MaxWL.SourceUniqueID = WL.SourceUniqueID
				and	MaxWL.ScheduleStartTime < MaxWL.CensusDate
				and	(
						(
							MaxWL.ScheduleOutcomeCode in ('CPAT','ATTD')
						)
						or	MaxWL.ScheduleOutcomeCode is null
					)
				)
				,	case 
					when WL.ReferralReceivedTime <
						(
						select
							min(MinWL.ScheduleStartTime)
						from	
							dbo.TImportCOMReferralWait MinWL
						where
							MinWL.SourceUniqueID = WL.SourceUniqueID
						and	MinWL.ScheduleStartTime < MinWL.CensusDate
						) then WL.ReferralReceivedTime
					else
						(
						select
							min(MinWL.ScheduleStartTime)
						from	
							dbo.TImportCOMReferralWait MinWL
						where
							MinWL.SourceUniqueID = WL.SourceUniqueID
						and	MinWL.ScheduleStartTime < MinWL.CensusDate
						) 
					end
				) 
		,EndWaitDate = ScheduleEndTime
		,BookedTime = ScheduleBookedTime
	from
		dbo.TImportCOMReferralWait WL
	left join 
		(
		select
			 SourceUniqueID
			,DNAs = 
				sum(case when ScheduleOutcomeCode = 'DNAT' then 1 else 0 end) 
			,HospitalCancels =
				sum(case when ScheduleOutcomeCode IN ('CHOS') then 1 else 0 end)
			,PatientCancels = 
				sum(case when ScheduleOutcomeCode IN ('CPAT','COTH') then 1 else 0 end) 
		from
			dbo.TImportCOMReferralWait
		where
			ScheduleOutcomeCode in ('DNAT', 'CHOS','CPAT','COTH')
		group by
			SourceUniqueID
		) Counts
	on	Counts.SourceUniqueID = WL.SourceUniqueID
)WaitingList

outer apply
(
select top 1
	CreatedTime
from 
	dbo.TImportCOMReferralWait WL
where 
	SourceUniqueID = WaitingList.SourceUniqueID
order by 
	CreatedTime
)FirstScheduleStartTime

outer apply
(
select top 1
	 ScheduleID
	,SourceUniqueID
	,ScheduleStartTime
	,ScheduleEndTime
from
	dbo.TImportCOMReferralWait WL 
where	
	SourceUniqueID = WaitingList.SourceUniqueID
and ScheduleOutcomeCode =  'ATTD'
order by 
	 ScheduleStartTime
	,ScheduleEndTime
	,ScheduleID

) FirstAttendance

outer apply
(
select top 1
	 ScheduleID
	,SourceUniqueID
	,ScheduleStartTime
	,ScheduleEndTime
from
	dbo.TImportCOMReferralWait WL
where	
	SourceUniqueID = WaitingList.SourceUniqueID
and ScheduleOutcomeCode =  'PLND'
order by 
	 ScheduleStartTime
	,ScheduleEndTime
	,ScheduleID
) FirstPlanned


--where WaitingList.sourceUniqueID =154488544 --153571605 --13339624 --  153644650 --10180710 -- 
--order by waitinglist.sourceUniqueID, waitinglist.ScheduleStartTime ,waitinglist.ScheduleEndTime, waitinglist.ScheduleID




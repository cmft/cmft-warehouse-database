﻿
create view dbo.[TLoadMedisecOutpatient]

as

select
	 [PatientNumber] = ltrim(rtrim([PatientNumber]))
	,[EpisodeNumber] = ltrim(rtrim([EpisodeNumber]))
	,[AppointmentDate]
	,[AppointmentTime] = ltrim(rtrim([AppointmentTime]))
	,[DoctorCode] = ltrim(rtrim([DoctorCode]))
	,[HospitalCode] = ltrim(rtrim([HospitalCode]))
	,[SpecialtyCode] = ltrim(rtrim([SpecialtyCode]))
	,[Diary] = ltrim(rtrim([Diary]))
	,[Stream] = ltrim(rtrim([Stream]))
	,[ConsultantCode] = ltrim(rtrim([ConsultantCode]))
	,[AppointmentNumber] = ltrim(rtrim([AppointmentNumber]))
	,[AttendanceNumber] = ltrim(rtrim([AttendanceNumber]))
	,[AttendanceMarker] = ltrim(rtrim([AttendanceMarker]))
	,[Outcome] = ltrim(rtrim([Outcome]))
	,[Type] = ltrim(rtrim([Type]))
	,[Contract] = ltrim(rtrim([Contract]))
	,[AppointmentCode] = ltrim(rtrim([AppointmentCode]))
	,[ProcedureCode1] = ltrim(rtrim([ProcedureCode1]))
	,[ProcedureCode2] = ltrim(rtrim([ProcedureCode2]))
	,[ProcedureCode3] = ltrim(rtrim([ProcedureCode3]))
	,[ProcedureCode4] = ltrim(rtrim([ProcedureCode4]))
	,[outepdireq] = ltrim(rtrim([outepdireq]))
	,[EpisodeClosedDate] = ltrim(rtrim([EpisodeClosedDate]))
	,[PatientCategory] = ltrim(rtrim([PatientCategory]))
	,[ReferenceCode] = ltrim(rtrim([ReferenceCode]))
	,[outreforg] = ltrim(rtrim([outreforg]))
	,[outrefsrce] = ltrim(rtrim([outrefsrce]))
	,[outpurchref] = ltrim(rtrim([outpurchref]))
	,[LetterDate]
	,[LetterComment] = ltrim(rtrim([LetterComment]))
	,[LetterStatus] = ltrim(rtrim([LetterStatus]))
	,[outcank] = ltrim(rtrim([outcank]))
	,[HospitalNumber] = ltrim(rtrim([HospitalNumber]))
	,[EpisodeSecurityFlag] = ltrim(rtrim([EpisodeSecurityFlag]))
from 
	[dbo].[TImportMedisecOutpatient]


/* ============================ */
/* TLoadManualMedisecSubmission */
/* ============================ */


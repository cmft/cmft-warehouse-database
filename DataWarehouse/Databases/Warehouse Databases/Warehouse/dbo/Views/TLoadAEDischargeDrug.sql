﻿


CREATE view [dbo].[TLoadAEDischargeDrug]

as

select
	SourceUniqueID
	,AESourceUniqueID
	,DischargeDate
	,DischargeTime
	,DrugCode
	,SequenceNo
	,InterfaceCode = 'SYM'
from
	dbo.TImportAEDischargeDrug


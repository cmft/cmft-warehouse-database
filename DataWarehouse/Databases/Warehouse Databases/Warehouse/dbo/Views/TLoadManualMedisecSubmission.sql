﻿

create view dbo.TLoadManualMedisecSubmission

as

select [Ward]
      ,[DischargeDate]
      ,[SubmittedBy]
      ,[Total]
      ,[<24Hours]
      ,[24-48Hours]
      ,[>48Hours]
from dbo.TImportManualMedisecSubmission


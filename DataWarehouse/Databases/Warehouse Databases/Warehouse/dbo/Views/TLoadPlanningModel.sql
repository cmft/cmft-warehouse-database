﻿
CREATE VIEW [dbo].[TLoadPlanningModel]

AS


SELECT 
	 CensusDate
	,Region
	,Purchaser
	,Tariff
	,[Site]
	,Category
	,Specialty
	,Hrg
	,Age
	,Los
	,Scenario
	,Step
	,StepSeqno
	,Period
	,Cases
	,BedDays
	,BedsAt90PctOccupancy
	,Price
	,TheatreMins
	,OperationDuration
	,AnaestheticDuration
FROM 
	dbo.TimportPlanningModel

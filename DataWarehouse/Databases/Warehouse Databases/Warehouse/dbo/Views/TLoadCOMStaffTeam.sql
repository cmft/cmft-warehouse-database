﻿
CREATE VIEW [dbo].[TLoadCOMStaffTeam] 

AS

SELECT
	 StaffTeamID--[STEAM_REFNO]
	
	,StaffTeamCode--[STEAM_REFNO_CODE]
	
	,StaffTeam--[STEAM_REFNO_NAME]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]
	
	,StaffTeamDescription--[DESCRIPTION]

	,StaffTeamSpecialtyID--[SPECT_REFNO]

	,TeamLeaderID--[LEADR_PROCA_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,StaffTeamHealthOrgOwner--[OWNER_HEORG_REFNO]

FROM (

SELECT 
	 StaffTeamID--[STEAM_REFNO]
	
	,StaffTeamCode--[STEAM_REFNO_CODE]
	
	,StaffTeam--[STEAM_REFNO_NAME]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]
	
	,StaffTeamDescription--[DESCRIPTION]

	,StaffTeamSpecialtyID--[SPECT_REFNO]

	,TeamLeaderID--[LEADR_PROCA_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,StaffTeamHealthOrgOwner--[OWNER_HEORG_REFNO]

FROM dbo.TImportCOMStaffTeam

) Encounter

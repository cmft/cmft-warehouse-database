﻿





CREATE view [dbo].[TLoadAEEncounterPCECTHT] as

/******************************************************************************************************
View		: [dbo].[TLoadAEEncounterPCECTHT]
Description	: 

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
14/11/2014	Paul Egan       Added AttendanceNumber, Age, Postcode
							Added Tiebreaker to SourceUniqueID OVER clause (otherwise non-repeatable results)
*******************************************************************************************************/

select
	 SourceUniqueID = 'SystemGenerated_' + right('00000000' + 
						convert(varchar, row_number() over (order by FirstCons.EntryTime, FirstCons.CaseNo)) , 8)
		--CaseExport.SourceUniqueID

	,UniqueBookingReferenceNo = null
	,PathwayId = null
	,PathwayIdIssuerCode = null
	,RTTStatusCode = null
	,RTTStartDate = null
	,RTTEndDate = null
	,DistrictNo = null
	,TrustNo = null
	,CasenoteNo = null
	,DistrictNoOrganisationCode = null
	,NHSNumber = null
	,NHSNumberStatusId = null
	,PatientTitle = null
	,PatientForename = null
		--Encounter.PatientForename

	,PatientSurname = null
		--Encounter.PatientSurname

	,PatientAddress1 = null
		--left(Encounter.PatientAddress1 , 50)

	,PatientAddress2 = null
		--case
		--when len(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '')) > 50
		--	then substring(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '') , 51 , len(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '')) - 50)
		--else null
		--end


	,PatientAddress3 = null
		--Encounter.PatientAddress3

	,PatientAddress4 =  null
	,Postcode = InfOutcomes.Postcode		-- Added Paul Egan 14/11/2014
		--Encounter.Postcode

	,DateOfBirth = null
		--cast(Encounter.DateOfBirth as date)

	,DateOfDeath = null
	,SexCode = null
		--Encounter.SexCode

	,CarerSupportIndicator = null
	,RegisteredGpCode = 'G9999998'
	,RegisteredGpPracticeCode = 'V81999'
	,AttendanceNumber = FirstCons.CaseNo	-- Added Paul Egan 14/11/2014
		--Encounter.CaseNo

	,ArrivalModeCode = null
		--case ArrivalMode.Answer
		--when 'Ambulance' then 6477 -- Ambulance
		--when 'Bus' then 6665 -- Public Transport
		--when 'Car' then 6476 -- Own Transport
		--when 'Cycle' then 6476 -- Own Transport
		--when 'Taxi' then 6480 -- Other Arrival Mode
		--when 'Walked' then 6476 -- Own Transport
		--end

	,AttendanceCategoryCode = 1
		--case
		--when Encounter.CaseType like '%repeat%' then 2
		--else 1
		--end

	,AttendanceDisposalCode = '02' --NULL
	,SourceAttendanceDisposalCode = '-2' --NULL
	,IncidentLocationTypeCode = null
	,PatientGroupCode = 80
	,SourceOfReferralCode = null

	,ArrivalDate =
		cast(FirstCons.EntryTime as date)

	,ArrivalTime = FirstCons.EntryTime

	,AgeOnArrival = --null
		case
		when InfOutcomes.Age in( 'Unknown' , '') then null
		when InfOutcomes.Age like '%month%' then 
			case
			when left(InfOutcomes.Age , 2) > 23 then 2
			when left(InfOutcomes.Age , 2) between 12 and 23 then 1
			else 0
			end
		when InfOutcomes.Age like '%week%' then 0		-- Added Paul Egan 14/11/2014
		when InfOutcomes.Age like '%day%' then 0		-- Added Paul Egan 14/11/2014
		else cast(left(InfOutcomes.Age , charindex(' ' , InfOutcomes.Age) - 1) as int)
		end

	,InitialAssessmentTime = null
	,SeenForTreatmentTime = FirstCons.ConsultationStartTime
	,AttendanceConclusionTime = FirstCons.ConsultationEndTime
	,DepartureTime = FirstCons.ConsultationEndTime

	,CommissioningSerialNo = --'X98'	-- Altered Paul Egan 14/11/2014
		rtrim(
			coalesce(	
				 --Practice.ParentOrganisationCode
				 Postcode.PCTCode
				--,Encounter.ResidencePCTCode
				,'X98'
			) 
		) + '00A'

	,NHSServiceAgreementLineNo = null
	,ProviderReferenceNo = null
	,CommissionerReferenceNo = null
	,ProviderCode = 'RW3'

	,CommissionerCode = --'X98'		-- Altered Paul Egan 14/11/2014
		coalesce(	
			 --Practice.ParentOrganisationCode
			 Postcode.PCTCode
			--,Encounter.ResidencePCTCode
			,'X98'
		) 

	,StaffMemberCode = null
	,InvestigationCodeFirst = null
	,InvestigationCodeSecond = null
	,DiagnosisCodeFirst = null
	,DiagnosisCodeSecond = null
	,TreatmentCodeFirst = null
	,TreatmentCodeSecond = null
	,PASHRGCode = null
	,HRGVersionCode = null
	,PASDGVPCode = null
	,SiteCode = 'RW3TR'
	,Created = null
	,Updated = GETDATE()
	,ByWhom = null
	,InterfaceCode = 'ADAS'

	,PCTCode = -- 'X98'		-- Altered Paul Egan 14/11/2014
		coalesce(	
			 --Practice.ParentOrganisationCode
			 Postcode.PCTCode
			--,Encounter.ResidencePCTCode
			,'X98'
		) 

	,ResidencePCTCode = coalesce(Postcode.PCTCode , 'X98')	-- Altered Paul Egan 14/11/2014

	,InvestigationCodeList = null
	,TriageCategoryCode = null
	,PresentingProblem = null
	,ToXrayTime = null
	,FromXrayTime = null
	,ToSpecialtyTime = null
	,SeenBySpecialtyTime = null
	,EthnicCategoryCode = null
	,ReferredToSpecialtyCode = null
	,DecisionToAdmitTime = null
	,DischargeDestinationCode = null
	,RegisteredTime = null
	,TransportRequestTime = null
	,TransportAvailableTime = null
	,AscribeLeftDeptTime = null
	,CDULeftDepartmentTime = null
	,PCDULeftDepartmentTime = null

	,AmbulanceArrivalTime = null
	,AmbulanceCrewPRF = null
	,ArrivalTimeAdjusted = FirstCons.EntryTime
	,UnplannedReattend7Day = 0
	,ClinicalAssessmentTime = null

from
	(
	select		
		 CaseNo
		,EntryTime = cast(EntryTime as datetime)
		,ConsultationStartTime = cast(ConsultationStartTime as datetime)
		,ConsultationEndTime = cast(ConsultationEndTime as datetime)
	from
		dbo.TImportPCECAEEntryToFirstConsTHT		
	) FirstCons

	left join 
	(
	select		
		 CaseNo
		,Postcode
		,Age
		,CallDate
	from
		dbo.TImportPCECAECodesInfOutcomesTHT a
	) InfOutcomes
	on InfOutcomes.CaseNo = FirstCons.CaseNo
	----------and cast(InfOutcomes.CallDate as date) = cast(FirstCons.EntryTime as date)  -- Duplicate rows in TImport table!
	
	/* Added Paul Egan 14/11/2014 */
	left join [$(Organisation)].dbo.Postcode Postcode on
		Postcode.Postcode = 
			case
			when len(InfOutcomes.Postcode) = 8 then InfOutcomes.Postcode
			else left(InfOutcomes.Postcode, 3) + ' ' + right(InfOutcomes.Postcode, 4) 
			end collate Latin1_General_CI_AS






﻿
create view dbo.TLoadCMISProviderSpell

as

select  
	[HospitalNumber] = ltrim(rtrim([HospitalNumber]))
	,[Occurrence]
	,[Spell] = ltrim(rtrim([Spell]))
	,[IsMotherOrInfant]
	,[SpellStatus]
	,[SpellStartDate]
	,[SpellStartTime] = cast(left(SpellStartDate, 11) + left(SpellStartTime, 2) + ':' + right(SpellStartTime, 2) as datetime)
	,[SpellEndDate]
	,[SpellEndTime] = cast(left(SpellEndDate, 11) + left(SpellEndTime, 2) + ':' + right(SpellEndTime, 2) as datetime)
	,[SpellLengthInDays]
	,[SpellLengthInHours]
	,[SpellLengthInMinutes]
	,[BuyerCode] = ltrim(rtrim([BuyerCode]))
	,[ContractNumber] = ltrim(rtrim([ContractNumber]))
	,[DateStamp]
	,[UserID]
	,[Location]

from dbo.TImportCMISProviderSpell;


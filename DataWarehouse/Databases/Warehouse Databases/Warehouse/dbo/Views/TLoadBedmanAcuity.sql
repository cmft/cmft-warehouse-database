﻿


CREATE VIEW [dbo].[TLoadBedmanAcuity] 

AS

SELECT
	   [DailyScoreID]
      ,[WardCode]
      ,[ScoreTime]
      
      ,[TotalPatientsLevel0]
      ,[TotalPatientsLevel0Score]
      ,[AcuityMutiplierLevel0]
      
      ,[TotalPatientsLevel1a]
      ,[AcuityMutiplierLevel1a]
      ,[TotalPatientsLevel1aScore]
      
      ,[TotalPatientsLevel1b]
      ,[AcuityMutiplierLevel1b]
      ,[TotalPatientsLevel1bScore]
      
      ,[TotalPatientsLevel2]
      ,[AcuityMutiplierLevel2]
      ,[TotalPatientsLevel2Score]
      
      ,[TotalPatientsLevel3]
      ,[AcuityMutiplierLevel3]
      ,[TotalPatientsLevel3Score]
      
      ,[AvailableBeds]

FROM (

SELECT 
	   [DailyScoreID]
      ,[WardCode]
      ,[ScoreTime]
      
      ,[TotalPatientsLevel0]
      ,[AcuityMutiplierLevel0]
      ,[TotalPatientsLevel0Score]= [TotalPatientsLevel0] * [AcuityMutiplierLevel0]
      
      ,[TotalPatientsLevel1a]
      ,[AcuityMutiplierLevel1a]
      ,[TotalPatientsLevel1aScore]= [TotalPatientsLevel1a] * [AcuityMutiplierLevel1a]
      
      ,[TotalPatientsLevel1b]
      ,[AcuityMutiplierLevel1b]
      ,[TotalPatientsLevel1bScore]= [TotalPatientsLevel1b] * [AcuityMutiplierLevel1b]
      
      ,[TotalPatientsLevel2]
      ,[AcuityMutiplierLevel2]
      ,[TotalPatientsLevel2Score]= [TotalPatientsLevel2] * [AcuityMutiplierLevel2]
      
      ,[TotalPatientsLevel3]
      ,[AcuityMutiplierLevel3]
      ,[TotalPatientsLevel3Score]= [TotalPatientsLevel3] * [AcuityMutiplierLevel3]
      
      ,[AvailableBeds]

FROM 
	[TImportBedmanDailyAcuityScore] tbl_DailyAcuityScore

CROSS JOIN (
				SELECT 
						[AcuityMutiplierLevel0]
					   ,[AcuityMutiplierLevel1a]
					   ,[AcuityMutiplierLevel1b]
					   ,[AcuityMutiplierLevel2]
					   ,[AcuityMutiplierLevel3]
				FROM 
						dbo.TImportBedmanAcuityLevelWTE
				
				WHERE 
						WardTypeID = 0
			)tbl_AcuityLevelWTE

)tbl_Acuity


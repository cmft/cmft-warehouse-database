﻿
CREATE VIEW dbo.TLoadCOMQARSchedules

AS

SELECT	
	 SCHDL_REFNO 
	,REFRL_REFNO
	,PATNT_REFNO
	,START_DTTM START_DTTM
	,ItemType = CASE SCTYP_REFNO
					WHEN 1468 -- Community Contact
					THEN CASE
						WHEN     SCOCM_REFNO = 1457
							 AND CANCR_DTTM IS NULL
							 AND START_DTTM  < DATEADD(dd,-1,getdate()) THEN 'DNAT'
							 
						WHEN     SCOCM_REFNO = 1459
							 AND CANCR_DTTM IS NULL
							 AND START_DTTM  < DATEADD(dd,-1,getdate()) THEN 'PDCD'
							 
						WHEN   (   
								   SCOCM_REFNO IN(2001651
								    		   ,2003640
									    	   )
								OR CANCR_DTTM IS NOT NULL 
							    )
							AND START_DTTM  < DATEADD(dd,-1,getdate()) THEN 'CNLD'
							
						WHEN
							    ARRIVED_DTTM IS NOT NULL
							AND	DEPARTED_DTTM IS NOT NULL 
							AND CANCR_DTTM IS NULL
							AND START_DTTM  < DATEADD(dd,-1,getdate()) THEN 'ATTD'
							
						WHEN 
								SCOCM_REFNO NOT IN 
											  (  1210
												,1457
												,1459
												,2001651
												,2003640
											  )
							AND
								(
									 ARRIVED_DTTM IS NULL
								  OR DEPARTED_DTTM IS NULL 
								)
							AND START_DTTM < DATEADD(dd,-1,getdate()) THEN 'UACT'
							
						WHEN
							SCOCM_REFNO = 1210
							AND START_DTTM  >= DATEADD(dd,-1,getdate()) THEN 'PLND'
							
						ELSE 'NSPD'
						END

					WHEN 1470 -- Clinic Contact
					THEN	CASE
						WHEN ATTND_REFNO IN (
										357,
										2868,
										2004151                 
									) 
							AND START_DTTM  < DATEADD(dd,-1,getdate()) THEN 'ATTD'
									
						WHEN ATTND_REFNO IN (
												2870,
												2003532,
												2004528,
												2004300,
												2004301,
												3006508
											) 
							AND START_DTTM  < DATEADD(dd,-1,getdate()) THEN 'CNLD'
											
						WHEN ATTND_REFNO IN (
												358,
												2000724,
												2003494,
												2003495,
												2003534
											)
							AND START_DTTM  < DATEADD(dd,-1,getdate()) THEN 'DNAT'
											
						WHEN ATTND_REFNO = '45' THEN 'NSPD'
						
						WHEN ATTND_REFNO NOT IN(357,
												2868,
												2004151,
												2870,
												2003532,
												2004528,
												2004300,
												2004301,
												3006508,
												358,
												2000724,
												2003494,
												2003495,
												2003534
												)
							AND START_DTTM  >= DATEADD(dd,-1,getdate())  THEN 'PLND'
						
						ELSE 'NSPD'
						END
				END 
	
FROM
	(SELECT 
			*
	FROM 
		[$(ipm)].dbo.SCHEDULES_VE02 
	
	WHERE 
		NOT EXISTS
			(
			SELECT
				1
			
			FROM
				[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
			
			WHERE
				PreviousEncounter.SCHDL_REFNO = SCHEDULES_VE02.SCHDL_REFNO
			
			AND PreviousEncounter.MODIF_DTTM > SCHEDULES_VE02.MODIF_DTTM
			)
	)encounter

WHERE  	

	ARCHV_FLAG = 'N'

	AND CANCR_DTTM IS NULL
	
	AND	encounter.CONTY_REFNO <> '2004177' -- Other Client Linked Activity	

	AND	encounter.PROCA_REFNO NOT IN ('10000199'
									 ,'10540964'
									 ,'10538047'
									 ,'10538046'
									 ) --Harling, Lawton, Butterworth (PAT Audiology), Gulati (T2 ENT)

	AND												
		((encounter.SCTYP_REFNO = '1470' -- Outpatient Schedules	
			AND
		encounter.SPECT_REFNO IN ('2000820','10000054')) --Comm Paeds and Audiology

		OR

		(encounter.SCTYP_REFNO = '1468' -- Community Contacts
			AND
		encounter.SPECT_REFNO = '10000054'))-- community Paeds





;


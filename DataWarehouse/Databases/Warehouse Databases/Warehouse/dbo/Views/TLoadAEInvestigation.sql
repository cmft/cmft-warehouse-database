﻿
CREATE VIEW [dbo].[TLoadAEInvestigation] as

SELECT
	 SourceUniqueID
	,InvestigationDate
	--,SequenceNo GS20110629 sequence number reset to take into account appended records from cursor
	,SequenceNo = ROW_NUMBER() OVER(PARTITION BY AESourceUniqueID ORDER BY convert(int,SourceUniqueID))
	,InvestigationCode
	,AESourceUniqueID
	,SourceInvestigationCode
	,ResultDate
	
FROM
	dbo.TImportAEInvestigation


﻿--2015-11-02 CCB Refactored
--2015-11-02 CCB This code requires optimisation due to extensive use of select * etc.

CREATE VIEW [dbo].[TLoadCOMMARGPReferralsSeen]

AS													
													
SELECT            													
	 ReturnPeriod = (YEAR(s.START_DTTM) * 100) + MONTH (s.START_DTTM)
	
	,LoadDate = CONVERT(Date,(CURRENT_TIMESTAMP),103)
	
	,[Return] = 'MAR'
	
	,ReturnItem =
		CASE 
		WHEN r.SORRF_REFNO in ('5300','2002324','6428','2003641') THEN 'GP Referrals Seen' 
		ELSE 'Other Referrals Seen'
		END 
	
	,InterfaceCode = 'ipm'
	
	,SourceUniqueNo = s.SCHDL_REFNO 
	
	,ItemDate =
		CONVERT(date, ref.FirstAppt,103)	
	
	,ItemSpecialty = spec.[DESCRIPTION]
	
	,NHSNumber = pat.NHS_IDENTIFIER
	
	,ProfessionalCarer =
		ISNULL(prof.SURNAME , 'null') + ',' + ISNULL(prof.FORENAME , 'null')
						
	,CommissionerCode =
		COALESCE(
			 ProfessionalCarerBase.ParentHealthOrgCode	
			,'5NT'
		)

	,Commissioner =
		COALESCE(
			ProfessionalCarerBase.ParentHealthOrgDescription
			,'MANCHESTER PCT'
		) 
																				
FROM													
	(
	SELECT	
		*
	FROM
		[$(ipm)].dbo.SCHEDULES_VE02 Encounter
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
			WHERE
				PreviousEncounter.SCHDL_REFNO = Encounter.SCHDL_REFNO
			AND PreviousEncounter.MODIF_DTTM > Encounter.MODIF_DTTM
			)
	) s		

 INNER JOIN
	(							
    SELECT							
         fs.REFRL_REFNO
		,FirstAppt = MIN(fs.START_DTTM)
    FROM							
        (
		SELECT	
			*
		FROM
			[$(ipm)].dbo.SCHEDULES_VE02 Encounter
		WHERE
			NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
				WHERE
					PreviousEncounter.SCHDL_REFNO = Encounter.SCHDL_REFNO
				AND PreviousEncounter.MODIF_DTTM > Encounter.MODIF_DTTM
				)
		) fs							

    LEFT JOIN 
		(
		SELECT DISTINCT
			p.PATNT_REFNO
		FROM
			(
			SELECT	
				*
			FROM
				[$(ipm)].dbo.SCHEDULES_VE02 Encounter
			WHERE
				NOT EXISTS
					(
					SELECT
						1
					FROM
						[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
					WHERE
						PreviousEncounter.SCHDL_REFNO = Encounter.SCHDL_REFNO
					AND PreviousEncounter.MODIF_DTTM > Encounter.MODIF_DTTM
					)
			) s

		INNER JOIN
			(
			SELECT
				*
			FROM 
				[$(ipm)].dbo.PATIENTS_VE01 patient

			WHERE
				NOT EXISTS
					(
					SELECT
						1
					FROM
						[$(ipm)].dbo.PATIENTS_VE01 Laterpatient
					WHERE
							Laterpatient.PATNT_REFNO = patient.PATNT_REFNO
						AND	Laterpatient.MODIF_DTTM > patient.MODIF_DTTM
					)
			) p

		ON	s.PATNT_REFNO = p.PATNT_REFNO
		AND	p.ARCHV_FLAG = 'N'   

		WHERE
			s.REFRL_REFNO IS NOT NULL
		AND
			(
				LEFT(p.NHS_IDENTIFIER, 3) = '999'
			OR	p.FORENAME  IN ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')
			OR	p.SURNAME  IN ('xxtestpatientaavp','xxtestpatientaafe')
			)
		--GROUP BY
		--	s.REFRL_REFNO	
		) tp -- list of schedules for test patients
 					
	ON fs.PATNT_REFNO = tp.PATNT_REFNO							
            
    WHERE							
		tp.PATNT_REFNO IS NULL -- exclude test patients							
	AND	fs.ARCHV_FLAG = 'N'							
	AND	(							
			fs.SCTYP_REFNO = 1468							
		OR	fs.SCTYP_REFNO = 1470							
		)
	AND	( --extracting attendances only							
			(							
				fs.SCTYP_REFNO = '1470' -- Clinic Contact							
			AND	fs.ATTND_REFNO IN
					(
					 357 -- Attended On Time							
					,2868 -- Patient Late / Seen							
					--P.McNulty 1.4.1 11/05/2011							
					,45 -- Not specified 							
					)							
			)							
		OR	(							
				fs.SCTYP_REFNO = '1468' -- Community Contact							
			AND	fs.SCOCM_REFNO NOT IN
					(							
					 1459 -- Patient Died	
					,2001651 -- Cancelled					
					,1457 -- Did Not Attend		
					)							
				--P.McNulty 1.4.1 11/05/2011							
			AND	fs.ARRIVED_DTTM IS NOT NULL							
			AND	fs.DEPARTED_DTTM IS NOT NULL							
			)							
		)							
	AND	fs.CONTY_REFNO <> 2004177

	GROUP BY							
		fs.REFRL_REFNO							
	) ref

ON	s.REFRL_REFNO = ref.REFRL_REFNO										

LEFT JOIN 
	(
	SELECT	
		*
	FROM
		[$(ipm)].dbo.REFERRALS_VE02 referrals
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.REFERRALS_VE02 Previousreferral
			WHERE
				Previousreferral.REFRL_REFNO = referrals.REFRL_REFNO
			AND Previousreferral.MODIF_DTTM > referrals.MODIF_DTTM
			)
	) r
ON	s.REFRL_REFNO = r.REFRL_REFNO
									
LEFT JOIN
	(													
	SELECT 												
		s.REFRL_REFNO											
	FROM												
		(
		SELECT	
			*
		FROM
			[$(ipm)].dbo.SCHEDULES_VE02 Encounter
		WHERE
			NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
				WHERE
						PreviousEncounter.SCHDL_REFNO = Encounter.SCHDL_REFNO
					AND PreviousEncounter.MODIF_DTTM > Encounter.MODIF_DTTM
				)
		) s	

	INNER JOIN
		(
		SELECT
			*
		FROM 
			[$(ipm)].dbo.PATIENTS_VE01 patient
		WHERE
			NOT EXISTS
				(
				SELECT
					1
				FROM
					[$(ipm)].dbo.PATIENTS_VE01 Laterpatient
				WHERE
					Laterpatient.PATNT_REFNO = patient.PATNT_REFNO
				AND	Laterpatient.MODIF_DTTM > patient.MODIF_DTTM
				)
		)  p												
	ON	s.PATNT_REFNO = p.PATNT_REFNO									
	AND	p.ARCHV_FLAG = 'N'   									

	WHERE												
		s.REFRL_REFNO IS NOT NULL										
	AND	(
			LEFT(p.NHS_IDENTIFIER, 3) = '999'								
		OR	p.FORENAME  in ('Ebs-Donotuse','Test','Patient','Mimium','xxtest','CSC-Donotuse')								
		OR	p.SURNAME  in ('xxtestpatientaavp','xxtestpatientaafe')								
		)

	GROUP BY												
		s.REFRL_REFNO											
	) ex -- list of schedules for test patients

ON	s.REFRL_REFNO = ex.REFRL_REFNO

LEFT JOIN
	(
	SELECT
		*
	FROM 
		[$(ipm)].dbo.SPECIALTIES_V01 Specialty
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.SPECIALTIES_V01 LaterSpecialty
			WHERE
				LaterSpecialty.SPECT_REFNO = Specialty.SPECT_REFNO
			AND	LaterSpecialty.MODIF_DTTM > Specialty.MODIF_DTTM
			)
	) spec

ON	s.SPECT_REFNO = spec.SPECT_REFNO	
	
LEFT JOIN
	(
	SELECT
		*
	FROM 
		[$(ipm)].dbo.PATIENTS_VE01 patient
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.PATIENTS_VE01 Laterpatient
			WHERE
				Laterpatient.PATNT_REFNO = patient.PATNT_REFNO
			AND	Laterpatient.MODIF_DTTM > patient.MODIF_DTTM
			)
	) pat
ON	s.PATNT_REFNO = pat.PATNT_REFNO
	
LEFT JOIN
	(
	SELECT
		*
	FROM 
		[$(ipm)].dbo.REFERENCE_VALUES_V01 refvalues
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.REFERENCE_VALUES_V01 Laterrefvalues
			WHERE
				Laterrefvalues.RFVAL_REFNO = refvalues.RFVAL_REFNO
			AND	Laterrefvalues.MODIF_DTTM > refvalues.MODIF_DTTM
			)
	)  val
ON	r.SORRF_REFNO = val.RFVAL_REFNO
	
LEFT JOIN
	(
	SELECT
		*
	FROM 
		[$(ipm)].dbo.SPECIALTIES_V01 Specialty
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.SPECIALTIES_V01 LaterSpecialty
			WHERE
				LaterSpecialty.SPECT_REFNO = Specialty.SPECT_REFNO
			AND	LaterSpecialty.MODIF_DTTM > Specialty.MODIF_DTTM
			)
	) spec1
ON	r.REFTO_SPECT_REFNO = spec1.SPECT_REFNO	
	
LEFT JOIN
	(
	SELECT
		*
	FROM 
		[$(ipm)].dbo.PROF_CARERS_V01 ProfCarer
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.PROF_CARERS_V01 LaterProfCarer
			WHERE
				LaterProfCarer.PROCA_REFNO = ProfCarer.PROCA_REFNO
			AND	LaterProfCarer.MODIF_DTTM > ProfCarer.MODIF_DTTM
			)
	) prof
ON
	CASE
	WHEN s.SCTYP_REFNO = 1468 THEN s.PROCA_REFNO
	WHEN s.SCTYP_REFNO = 1470 THEN ISNULL(s.SEENBY_PROCA_REFNO, s.PROCA_REFNO)
	END = prof.PROCA_REFNO	


--LEFT JOIN [5NTc].PatientsCurrentGPPractice g				
--	ON r.PATNT_REFNO = g.PATNT_REFNO			
--LEFT JOIN Common.[General Medical Practice] gmp				
--	ON g.HEORG_MAIN_IDENT = gmp.[Organisation Code]			
--LEFT JOIN Common.[Primary Care Organisation] pct				
--	ON gmp.[parent organisation code] = pct.[organisation code]	


LEFT JOIN
	(
	SELECT DISTINCT
		 EncounterPatientProfessionalCarer.SCHDL_REFNO
		,EncounterPatientProfessionalCarer.PATPC_REFNO
		,ProfessionalCarerCode
		,HealthOrgCode
		,ParentHealthOrgCode
		,ParentHealthOrgDescription
	FROM
		(
		SELECT 
			 SCHDL_REFNO
			,MAX(PATPC_REFNO) AS PATPC_REFNO
		FROM 
			(
			SELECT 
				 SCHDL_REFNO
				,START_DTTM
				,PATNT_REFNO
			FROM 
				[$(ipm)].dbo.SCHEDULES_VE02 Encounter
			WHERE
				NOT EXISTS
					(
					SELECT
						1
					FROM
						[$(ipm)].dbo.SCHEDULES_VE02 PreviousEncounter
					WHERE
						PreviousEncounter.SCHDL_REFNO = Encounter.SCHDL_REFNO
					AND PreviousEncounter.MODIF_DTTM > Encounter.MODIF_DTTM
					--AND Encounter.MODIF_DTTM > '01/April/2011' --@lastmodifiedTime
					)
			) Encounter

		LEFT JOIN
			(
			SELECT
				 PATIENT_PROF_CARERS_V01.PATPC_REFNO
				,PATIENT_PROF_CARERS_V01.START_DTTM
				,PATIENT_PROF_CARERS_V01.END_DTTM
				,PATIENT_PROF_CARERS_V01.PATNT_REFNO
				,PATIENT_PROF_CARERS_V01.HEORG_REFNO
			FROM 
				[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 
			WHERE
				ARCHV_FLAG = 'N'
			AND PRTYP_REFNO = '1132'-- General Practitioner
			AND NOT EXISTS 
				(
				SELECT
					*
				FROM
					[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
				WHERE
					PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
				AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
				AND ARCHV_FLAG = 'N'
				AND PRTYP_REFNO = '1132' -- General Practitioner
				)
			) PatientProfCarer

		ON	Encounter.PATNT_REFNO = PatientProfCarer.PATNT_REFNO
		AND PatientProfCarer.START_DTTM <= Encounter.START_DTTM 
		AND (
				PatientProfCarer.END_DTTM IS NULL 
			OR PatientProfCarer.END_DTTM > Encounter.START_DTTM
			)

		GROUP BY 
			SCHDL_REFNO
		) EncounterPatientProfessionalCarer
		

	LEFT JOIN
		(
		SELECT
			 PATIENT_PROF_CARERS_V01.PATPC_REFNO
			,PATIENT_PROF_CARERS_V01.START_DTTM
			,PATIENT_PROF_CARERS_V01.END_DTTM
			,PATIENT_PROF_CARERS_V01.PATNT_REFNO
			,PATIENT_PROF_CARERS_V01.HEORG_REFNO
			,PATIENT_PROF_CARERS_V01.PROCA_MAIN_IDENT AS ProfessionalCarerCode
		FROM 
			[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 
		WHERE
			ARCHV_FLAG = 'N'
		AND PRTYP_REFNO = '1132'-- General Practitioner
		AND NOT EXISTS
			(
			SELECT
				*
			FROM
				[$(ipm)].dbo.PATIENT_PROF_CARERS_V01 PreviousPatientProfCarer
			WHERE
				PreviousPatientProfCarer.PATPC_REFNO = PATIENT_PROF_CARERS_V01.PATPC_REFNO
			AND PreviousPatientProfCarer.MODIF_DTTM > PATIENT_PROF_CARERS_V01.MODIF_DTTM
			AND ARCHV_FLAG = 'N'
			AND PRTYP_REFNO = '1132' -- General Practitioner
			)
		)PatientProfCarerDetail
	ON	EncounterPatientProfessionalCarer.PATPC_REFNO = PatientProfCarerDetail.PATPC_REFNO


	LEFT JOIN
		(	
		SELECT DISTINCT
			 HEORG_REFNO
			,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS HealthOrgDescription
			,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS HealthOrgCode
			,HEALTH_ORGANISATIONS_V02.PARNT_REFNO
		FROM
			[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
		WHERE
			ARCHV_FLAG = 'N'
		AND NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
			WHERE
				previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
			AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
			AND ARCHV_FLAG = 'N'
			)
		) PatientProfCarerHealthOrg
	ON	PatientProfCarerDetail.HEORG_REFNO = PatientProfCarerHealthOrg.HEORG_REFNO
	
	LEFT JOIN 
		(	
		SELECT DISTINCT
			 HEORG_REFNO ParentHEORG_REFNO
			,HEALTH_ORGANISATIONS_V02.[DESCRIPTION] AS ParentHealthOrgDescription
			,HEALTH_ORGANISATIONS_V02.MAIN_IDENT AS ParentHealthOrgCode
			,HEALTH_ORGANISATIONS_V02.HOTYP_REFNO AS ParentHealthOrgCodeType
		FROM
			[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 
		WHERE
			ARCHV_FLAG = 'N'
		AND HOTYP_REFNO = 629
		AND NOT EXISTS
			(
			SELECT
				1
			FROM
				[$(ipm)].dbo.HEALTH_ORGANISATIONS_V02 previousHealthOrg
			WHERE
				previousHealthOrg.HEORG_REFNO = HEALTH_ORGANISATIONS_V02.HEORG_REFNO
			AND previousHealthOrg.MODIF_DTTM > HEALTH_ORGANISATIONS_V02.MODIF_DTTM
			AND ARCHV_FLAG = 'N'
			AND HOTYP_REFNO = 629
			)
		) ParentPatientProfCarerHealthOrg
	ON	PatientProfCarerHealthOrg.PARNT_REFNO = ParentPatientProfCarerHealthOrg.ParentHEORG_REFNO
	) ProfessionalCarerBase
ON	s.SCHDL_REFNO = ProfessionalCarerBase.SCHDL_REFNO

WHERE													
	ex.REFRL_REFNO IS NULL -- exclude test patients											
	--AND s.START_DTTM BETWEEN CONVERT(DATETIME,'01/04/2011 00:00:00.000',103) AND CONVERT(DATETIME,'10/04/2011 23:59:59.997',103)											
AND	s.ARCHV_FLAG = 'N'
AND	r.SORRF_REFNO not in ('3007115','1509') --  self refs and friends/family
AND	s.CONTY_REFNO <>'2004177' -- Other Client Linked Activity	
AND	prof.PROCA_REFNO_MAIN_IDENT <> 'pseu057'	--exclude t2 ENT referrals
AND	s.PROCA_REFNO not in ('10000199','10540964','10538047','10538046') --Harling, Lawton, Butterworth (PAT Audiology), Gulati (T2 ENT)
AND	s.CANCR_DTTM is null -- cancelled appts	
AND	s.START_DTTM = ref.FirstAppt	 -- only extract 1st appts
AND	(
		(
			s.SCTYP_REFNO ='1470'
		AND	s.ATTND_REFNO IN
				(
				 357
				,2868
				)
		)
	OR	(
			s.SCTYP_REFNO ='1468'
		AND	(
				s.ARRIVED_DTTM IS NOT NULL					
			AND	s.DEPARTED_DTTM IS NOT NULL
			)
		)
	)			-- attended contacts and appts only
AND	(
		(
			s.SCTYP_REFNO = '1470' -- Outpatient Schedules	
		AND	s.SPECT_REFNO IN  --Comm Paeds and Audiology
			(
			 '2000820'
			,'10000054'
			)
		)
	OR	(
			s.SCTYP_REFNO = '1468' -- Community Contacts
		AND	s.SPECT_REFNO = '10000054' -- community Paeds
		)
	)
			


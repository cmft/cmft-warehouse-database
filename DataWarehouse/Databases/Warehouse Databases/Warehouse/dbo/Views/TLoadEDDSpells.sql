﻿


CREATE VIEW [dbo].[TLoadEDDSpells] as


/****** Script for SelectTopNRows command from SSMS  ******/

SELECT SourceSpellNo
	,SourcePatientID
	,SourceEncounterNo
	,SourceAEEncounterNo
	,CaseNoteNo
	,AENumber
	,AttendanceDt
	,AdmitDate
	,AdmitDatetime
	,DischargeDate
	,DischargeDatetime
	,CurrentWard
	,FacilityID
	,AEDepartureTime 
	,CurrentSpecialty
	,CurrentConsulant
	,CurrentLocation
	,FirstEdd
	,FirstEddtime
	,FirstEddCreated
	,FirstReasonforChange
	,FirstReasonGroupforChange
	,LastEdd
	,LastEddCreated
	,LastReasonforChange
	,LastReasonGroupforChange
	,SourceAPCPatientNo
	,SourceAPCSpellNo
	,SourceAPCManagementIntentionCode
	,SourceAPCExpLOS
	,SourceAPCEdd
	,SourceAPCSiteCode
	,sourcepatientcategorycode
	,SourceRTTPathwayCondition
	,SourceAdmissionMethodCode
	,DirectorateCode = NULL
	
FROM dbo.TImportEDDSpells
	













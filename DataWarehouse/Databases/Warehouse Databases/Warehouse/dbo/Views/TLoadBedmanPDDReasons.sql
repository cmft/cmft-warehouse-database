﻿

CREATE VIEW dbo.TLoadBedmanPDDReasons

AS

SELECT 
	 PDDReasonID
	,PDDGroup
	,PDDReason
	,Active
FROM 
	dbo.TImportBedmanPDDReasons
﻿

CREATE view [dbo].[TLoadMedisecInpatientRequest]

as

select
      cast(cast([PatientNumber] as int) as varchar(10)) as [PatientNumber]
	  ,[AdmissionDate] = case
							when left([AdmissionTime] ,2) = 24
							then dateadd(day, 1, [AdmissionDate])
							else [AdmissionDate]
						end
	  ,ForcebackNumber = cast(ForcebackNumber as int) 
      ,[ConsultantCode] = ltrim(rtrim([ConsultantCode])) 
      --,[AdmissionTime] = left([AdmissionDate],11) + ' ' + 
						--									case
						--										when left([AdmissionTime] ,2) = 24
						--										then '00:' + right([AdmissionTime] ,2) + ':00'
						--										when coalesce([AdmissionTime], '') = ''
						--										then '00:00:00'
						--										else left([AdmissionTime],2) + ':' + right([AdmissionTime],2) + ':00'
						--									end
						
      ,[AdmissionTime] = case
							when left([AdmissionTime] ,2) = 24
							then dateadd(day, 1, [AdmissionDate])
							else [AdmissionDate]
						end + ' ' + 
														case
															when left([AdmissionTime] ,2) = 24
															then '00:' + right([AdmissionTime] ,2) + ':00'
															when coalesce([AdmissionTime], '') = ''
															then '00:00:00'
															else left([AdmissionTime],2) + ':' + right([AdmissionTime],2) + ':00'
														end					
	
      ,[DischargeDate]
      ,[PatientSurname] = ltrim(rtrim([PatientSurname])) 
      ,[PatientInitials] = ltrim(rtrim([PatientInitials])) 
      ,[HospitalCode] = ltrim(rtrim([HospitalCode])) 
      ,[WardCode] = ltrim(rtrim([WardCode])) 
      ,[SpecialtyCode] = ltrim(rtrim([SpecialtyCode])) 
      ,[EpisodeStatusCode] = ltrim(rtrim([EpisodeStatusCode])) 
      ,[DocumentTime]
      ,[Comment] = ltrim(rtrim([Comment])) 
      ,[StatusCode] = ltrim(rtrim([StatusCode])) 
      ,[MailFlag] = ltrim(rtrim([MailFlag])) 
      ,[MailTime]
      ,[EmailFlag] = ltrim(rtrim([EmailFlag])) 
      ,[EmailTime]
      ,[FaxFlag] = ltrim(rtrim([FaxFlag])) 
      ,[FaxDate]
      ,[EDIFlag] = ltrim(rtrim([EDIFlag])) 
      ,[EDIDate]
      ,[HospitalNumber] = ltrim(rtrim([HospitalNumber])) 
      ,[EpisodeSecurityFlag] = ltrim(rtrim([EpisodeSecurityFlag])) 
      ,MergedFlag = 
					case
						when MergeInternalNo is not null
						then 1
						else 0
					end
	,[MergePatientNumber] = m1.MergeInternalNo
	,[MergeEpisodeNumber] = m1.[MergeEpisodeNo]
from [dbo].TImportMedisecInpatientRequest t1
left outer join (
				select 
					distinct 
							InternalNo
							,EpisodeNo
							,MergeInternalNo
							,MergeEpisodeNo
					from 
						PAS.[Merge]
				) m1 
on cast([PatientNumber] as int) = InternalNo
and cast(ForcebackNumber as int) = EpisodeNo

--where not exists (
--				select 1
--				from warehouse.PAS.[Merge] m2
--				where cast(t1.[PatientNumber] as int) = m2.MergeInternalNo
--				and cast(t1.ForcebackNumber as int) = m2.MergeEpisodeNo
--				)

--where cast([PatientNumber] as int) = 3053769
--where PatientNumber in (1151068,3186100)


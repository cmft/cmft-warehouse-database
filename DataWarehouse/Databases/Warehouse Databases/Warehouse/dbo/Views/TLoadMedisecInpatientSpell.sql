﻿

create view dbo.[TLoadMedisecInpatientSpell]

as

select
      cast(cast([PatientNumber] as int) as varchar(10)) as [PatientNumber]
      ,[AdmissionDate]
	  ,cast(ForcebackNumber as int) ForcebackNumber
      ,ltrim(rtrim([InpatientStatus])) as [InpatientStatus]
      ,[AdmissionTime] = case
							when left([AdmissionTime] ,2) = 24
							then '00' + right([AdmissionTime] ,2)
							when isnull([AdmissionTime],'') = ''
							then '0000'
							else [AdmissionTime]
						end
      ,ltrim(rtrim([SpecialtyCode])) as [SpecialtyCode]
      ,ltrim(rtrim([ConsultantCode])) as [ConsultantCode]
      ,ltrim(rtrim([WardCode])) as [WardCode]
      ,ltrim(rtrim([HospitalCode])) as [HospitalCode]
      ,ltrim(rtrim([PatientCategoryCode])) as [PatientCategoryCode]
      ,ltrim(rtrim([AdmissionMethodCode])) as [AdmissionMethodCode]
      ,ltrim(rtrim([AdmissionSourceCode])) as [AdmissionSourceCode]
      ,ltrim(rtrim([ReferenceCode])) as [ReferenceCode]
      ,ltrim(rtrim([ReferenceOrganisationCode])) as [ReferenceOrganisationCode]
      ,[DischargeDate]
      ,[DischargeTime] = case
							when left(DischargeTime ,2) = 24
							then '00' + right(DischargeTime ,2)
							else DischargeTime  
						end
      ,ltrim(rtrim([DischargeMethodCode])) as [DischargeMethodCode]
      ,ltrim(rtrim([DischargeDestinationCode])) as [DischargeDestinationCode]
      ,ltrim(rtrim([AdmittingHospitalCode])) as [AdmittingHospitalCode]
      ,ltrim(rtrim([HospitalNumber])) as [HospitalNumber]
      ,ltrim(rtrim([EpisodeSecurityFlag])) as [EpisodeSecurityFlag]

from [dbo].TImportMedisecInpatientSpell
where isnumeric(PatientNumber) = 1
--and [PatientNumber] in (901690,3058810,3185324)


﻿


CREATE VIEW [dbo].[TLoadCOMProfessionalCarer] 

AS

SELECT
	 ProfessionalCarerID  --[PROCA_REFNO]
	
	,ProfessionalCarerCode --[PROCA_REFNO_MAIN_IDENT]
	
	,ProfessionalCarerTypeID --[PRTYP_REFNO]

	,Title --[TITLE_REFNO_DESCRIPTION]
	
	,Forename --[FORENAME]

	,Surname --[SURNAME]
	
	,FullName

	,StartTime --[START_DTTM]
	
	,EndTime --[END_DTTM]

	,PrimaryStaffTeamID --[PRIMARY_STEAM_REFNO]
	
	,ArchiveFlag --[ARCHV_FLAG]

	,HealthOrgOwner --[OWNER_HEORG_REFNO]

FROM (

SELECT 
	 ProfessionalCarerID  --[PROCA_REFNO]
	
	,ProfessionalCarerCode --[PROCA_REFNO_MAIN_IDENT]
	
	,ProfessionalCarerTypeID --[PRTYP_REFNO]

	,Title --[TITLE_REFNO_DESCRIPTION]
	
	,Forename --[FORENAME]

	,Surname --[SURNAME]
	
	,FullName = CASE ISNULL(Title,'') 
					WHEN 'Not Known' THEN '' 
					ELSE  ISNULL(Title,'')
				END
				+ ' ' 
				+ ISNULL(Forename,'')
				+ ' ' 
				+ ISNULL(Surname,'')

	,StartTime --[START_DTTM]
	
	,EndTime --[END_DTTM]

	,PrimaryStaffTeamID --[PRIMARY_STEAM_REFNO]
	
	,ArchiveFlag --[ARCHV_FLAG]

	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	
FROM dbo.TImportCOMProfessionalCarer

) ProfessionalCarer


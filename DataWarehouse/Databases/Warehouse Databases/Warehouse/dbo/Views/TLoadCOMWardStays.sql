﻿

CREATE VIEW [dbo].[TLoadCOMWardStays]

AS

SELECT
	 SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,AdmissionDate 
	,DischargeDate
	,StartTime
	,EndTime
	,StartDate
	,EndDate
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID 
	,SourceAdmissionMethodID 
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID 
	,ExpectedDischargeDate 
	,SourceDischargeDestinationID 
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode
	,RegisteredPracticeID
FROM
(
SELECT
	 SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,AdmissionDate = CONVERT(DATE,AdmissionTime)
	,DischargeDate = CONVERT(DATE,DischargeTime)
	,StartTime
	,EndTime
	,StartDate = CONVERT(Date,StartTime)
	,EndDate = CONVERT(DATE,EndTime)
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator = 1
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID 
	,SourceAdmissionMethodID 
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID 
	,ExpectedDischargeDate 
	,SourceDischargeDestinationID 
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode
	,RegisteredPracticeID
FROM
	dbo.TImportCOMWardStay
) WardStay

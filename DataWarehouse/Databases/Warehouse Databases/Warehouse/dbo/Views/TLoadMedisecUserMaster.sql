﻿

create view dbo.TLoadMedisecUserMaster

as

select
	SourceUserCode = ltrim(rtrim(SourceUserCode))
	,UserName = ltrim(rtrim(UserName))
	,UserType = ltrim(rtrim(UserType))
	,UserDepartment = ltrim(rtrim(UserDepartment))

from 
	dbo.TImportMedisecUserMaster

﻿
CREATE VIEW [dbo].[TLoadCOMServicePoint] 

AS

SELECT
	 ServicePointID--[SPONT_REFNO]
	
	,ServicePointCode--[CODE]
	
	,ServicePoint--[NAME]
	
	,ServicePointDescription--[DESCRIPTION]
	
	,ServicePointSpecialty--[SPECT_REFNO]
	
	,ServicePointHealthOrgID --[HEORG_REFNO]

	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]

	,ServicePointTypeID--[SPTYP_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,HealthOrgOwner--[OWNER_HEORG_REFNO]

FROM (

SELECT 
	 ServicePointID--[SPONT_REFNO]
	
	,ServicePointCode--[CODE]
	
	,ServicePoint--[NAME]
	
	,ServicePointDescription--[DESCRIPTION]
	
	,ServicePointSpecialty--[SPECT_REFNO]
	
	,ServicePointHealthOrgID --[HEORG_REFNO]

	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]

	,ServicePointTypeID--[SPTYP_REFNO]
	
	,ArchiveFlag--[ARCHV_FLAG]
	
	,HealthOrgOwner--[OWNER_HEORG_REFNO]
	
FROM dbo.TImportCOMServicePoint

) ServicePoint

﻿
CREATE view [dbo].[TLoadAEDiagnosis] as

select
	 SourceUniqueID
	,SequenceNo
	,DiagnosticSchemeInUse = '704'
	,DiagnosisCode
	,AESourceUniqueID
	,SourceDiagnosisCode
	,SourceDiagnosisSiteCode
	,SourceDiagnosisSideCode
	,SourceSequenceNo = null
	,DiagnosisSiteCode
	,DiagnosisSideCode
from
	dbo.TImportAEDiagnosis


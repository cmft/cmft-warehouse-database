﻿
CREATE VIEW [dbo].[TLoadCOMSpecialty] 

AS

SELECT
	 SpecialtyID  --[SPECT_REFNO]	 
	,SpecialtyCode --[MAIN_IDENT]	
	,Specialty  --[DESCRIPTION]	
	,SpecialtyParentID --[PARNT_REFNO]	
	,SpecialtyHealthOrgOwner --[OWNER_HEORG_REFNO]
FROM
	(
	SELECT 
		 SpecialtyID  --[SPECT_REFNO]		 
		,SpecialtyCode --[MAIN_IDENT]	
		,Specialty  --[DESCRIPTION]		
		,SpecialtyParentID --[PARNT_REFNO]		
		,SpecialtyHealthOrgOwner --[OWNER_HEORG_REFNO]
	FROM
		dbo.TImportCOMSpecialty
	) Specialty


﻿


CREATE VIEW [dbo].[TLoadCOMReferral] 

AS

SELECT
	 SourceUniqueID --[REFRL_REFNO]
	 
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]
	
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]
	
	,SourceofReferralID --[SORRF_REFNO]
	
	,ReferralReceivedTime --[RECVD_DTTM]
	
	,ReferralReceivedDate --[RECVD_DTTM]
	
	,ReasonforReferralID --[REASN_REFNO]
	
	,CancelledTime --[CANCD_DTTM]
	
	,CancelledDate --[CANCD_DTTM]
	
	,CancelledReasonID --[CANRS_REFNO]
	
	,ReferralSentTime --[SENT_DTTM]

	,ReferralSentDate --[SENT_DTTM]
	
	,Urgency --[URGNC_REFNO]
	
	,Priority --[PRITY_REFNO]
	
	,AuthoriedTime --[AUTHR_DTTM]

	,AuthoriedDate --[AUTHR_DTTM]
	
	,ReferralClosedTime --[CLOSR_DATE]
	
	,ReferralClosedDate --[CLOSR_DATE]
	
	,ClosureReasonID --CLORS_REFNO
	
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]
	
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]
	
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]
	
	,OutcomeOfReferral --[RFOCM_REFNO]
	
	,ReferralStatus --[RSTAT_REFNO]
	
	,RejectionID --RejectionID--[RJECT_REFNO]
	
	,TypeofReferralID --[RETYP_REFNO]
	
	,PatientSourceID 
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator

	,PatientLocalIdentifier

	,PatientPASIdentifier

	,PatientIdentifier

	,PatientFacilityIdentifier
	
	,PatientTitleID 
	
	,PatientForename 
	
	,PatientSurname 
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,ProfessionalCarerCurrentID
		
	,ProfessionalCarerHealthOrgCurrentID

	,ProfessionalCarerParentHealthOrgCurrentID
	
	,ProfessionalCarerReferralID 
		
	,ProfessionalCarerHealthOrgReferralID 

	,ProfessionalCarerParentHealthOrgReferralID 
	
	,RTTStatusDate --[STATUS_DTTM]
	
	,RTTPatientPathwayID --[PATNT_PATHWAY_ID]
	
	,RTTStartTime --[RTT_START_DATE]
	
	,RTTStartFlag --[RTT_STARTED]
	
	,RTTStatusID --[RTTST_REFNO]
	
	,CreatedTime --[CREATE_DTTM]
	
	,ModifiedTime --[MODIF_DTTM]
	
	,CreatedByID --[USER_CREATE]
	
	,ModifiedByID --[USER_MODIF]
	
	,ArchiveFlag --[ARCHV_FLAG]
	
	,ParentID  --[PARNT_REFNO]
	
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	
	,RequestedServiceID

FROM (

SELECT 
	 SourceUniqueID --[REFRL_REFNO]
	 
	,ReferredBySpecialtyID --[REFBY_SPECT_REFNO]
	
	,ReferredToSpecialtyID --[REFTO_SPECT_REFNO]
	
	,SourceofReferralID --[SORRF_REFNO]
	
	,ReferralReceivedTime --[RECVD_DTTM]
	
	,ReferralReceivedDate = CONVERT(DATE,ReferralReceivedTime,103) --[RECVD_DTTM]	
	
	,ReasonforReferralID --[REASN_REFNO]
	
	,CancelledTime --[CANCD_DTTM]
	
	,CancelledDate  = CONVERT(DATE,CancelledTime,103)--[CANCD_DTTM]
	
	,CancelledReasonID --[CANRS_REFNO]
	
	,ReferralSentTime --[SENT_DTTM]
	
	,ReferralSentDate  = CONVERT(DATE,ReferralSentTime,103)--[CANCD_DTTM]	
	
	,Urgency --[URGNC_REFNO]
	
	,Priority --[PRITY_REFNO]
	
	,AuthoriedTime --[AUTHR_DTTM]
		
	,AuthoriedDate  = CONVERT(DATE,AuthoriedTime,103)--[CANCD_DTTM]	
	
	,ReferralClosedTime --[CLOSR_DATE]
		
	,ReferralClosedDate  = CONVERT(DATE,ReferralClosedTime,103)--[CANCD_DTTM]	
	
	,ClosureReasonID --CLORS_REFNO
	
	,ReferredByProfessionalCarerID --[REFBY_PROCA_REFNO]
	
	,ReferredByStaffTeamID --[REFBY_STEAM_REFNO]
	
	,ReferredByHealthOrgID --[REFBY_HEORG_REFNO]
	
	,ReferredToProfessionalCarerID --[REFTO_PROCA_REFNO]
	
	,ReferredToHealthOrgID --[REFTO_HEORG_REFNO]
	
	,ReferredToStaffTeamID --[REFTO_STEAM_REFNO]
	
	,OutcomeOfReferral --[RFOCM_REFNO]
	
	,ReferralStatus --[RSTAT_REFNO]
	
	,RejectionID --RejectionID--[RJECT_REFNO]
	
	,TypeofReferralID --[RETYP_REFNO]
	
	,PatientSourceID 
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator

	,PatientLocalIdentifier

	,PatientPASIdentifier

	,PatientIdentifier

	,PatientFacilityIdentifier
	
	,PatientTitleID 
	
	,PatientForename 
	
	,PatientSurname 
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,ProfessionalCarerCurrentID
		
	,ProfessionalCarerHealthOrgCurrentID

	,ProfessionalCarerParentHealthOrgCurrentID
	
	,ProfessionalCarerReferralID 
		
	,ProfessionalCarerHealthOrgReferralID 

	,ProfessionalCarerParentHealthOrgReferralID 
	
	,RTTStatusDate --[STATUS_DTTM]
	
	,RTTPatientPathwayID --[PATNT_PATHWAY_ID]
	
	,RTTStartTime --[RTT_START_DATE]
	
	,RTTStartFlag --[RTT_STARTED]
	
	,RTTStatusID --[RTTST_REFNO]
	
	,CreatedTime --[CREATE_DTTM]
	
	,ModifiedTime --[MODIF_DTTM]
	
	,CreatedByID --[USER_CREATE]
	
	,ModifiedByID --[USER_MODIF]
	
	,ArchiveFlag --[ARCHV_FLAG]
	
	,ParentID  --[PARNT_REFNO]
	
	,HealthOrgOwner --[OWNER_HEORG_REFNO]
	
	,RequestedServiceID

FROM dbo.TImportCOMReferral

) Referral;


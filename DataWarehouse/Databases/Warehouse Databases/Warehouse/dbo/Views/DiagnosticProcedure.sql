﻿create view [dbo].[DiagnosticProcedure] as

select
	 EntityCode ProcedureCode
from
	dbo.EntityLookup
where
	EntityTypeCode = 'DIAGNOSTICPROCEDURE'

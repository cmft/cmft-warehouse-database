﻿






CREATE view [dbo].[TLoadAEEncounterPCEC] as

/******************************************************************************************************
View		: [dbo].[TLoadAEEncounterPCEC]
Description	: 

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
14/11/2014	Paul Egan       Corrected AgeOnArrival case statement.
08/12/2014	Rachel Royston	Brought through NHS Number from Adastra, previously set to = Null
*******************************************************************************************************/
	
	
	
select
	 SourceUniqueID = left(CaseExport.SourceUniqueID , 50)
	,UniqueBookingReferenceNo = null
	,PathwayId = null
	,PathwayIdIssuerCode = null
	,RTTStatusCode = null
	,RTTStartDate = null
	,RTTEndDate = null
	,DistrictNo = null
	,TrustNo = null
	,CasenoteNo = null
	,DistrictNoOrganisationCode = null
	,NHSNumber = replace(NHSNumber,' ','') 
	,NHSNumberStatusId = null
	,PatientTitle = null
	,Encounter.PatientForename
	,Encounter.PatientSurname

	,PatientAddress1 =
		left(Encounter.PatientAddress1 , 50)

	,PatientAddress2 =
		left(
		case
			when len(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '')) > 50
				then substring(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '') , 51 , len(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '')) - 50)
			else null
			end
		,50)


	,Encounter.PatientAddress3
	,PatientAddress4 =  null
	,Encounter.Postcode
	,DateOfBirth = cast(Encounter.DateOfBirth as date)
	,DateOfDeath = null
	,Encounter.SexCode
	,CarerSupportIndicator = null
	,RegisteredGpCode = null
	,RegisteredGpPracticeCode = null
	,AttendanceNumber = Encounter.CaseNo

	,ArrivalModeCode =
		case ArrivalMode.Answer
		when 'Ambulance' then 6477 -- Ambulance
		when 'Bus' then 6665 -- Public Transport
		when 'Car' then 6476 -- Own Transport
		when 'Cycle' then 6476 -- Own Transport
		when 'Taxi' then 6480 -- Other Arrival Mode
		when 'Walked' then 6476 -- Own Transport
		end

	,AttendanceCategoryCode =
		case
		when Encounter.CaseType like '%repeat%' then 2
		else 1
		end

	,AttendanceDisposalCode = '02' --NULL
	,SourceAttendanceDisposalCode = '-2' --NULL
	,IncidentLocationTypeCode = null
	,PatientGroupCode = 80
	,SourceOfReferralCode = null
	,ArrivalDate = cast(CaseExport.EntryTime as date) 
	,ArrivalTime =  CaseExport.EntryTime
	,AgeOnArrival = 
		case
		when Encounter.Age in( 'Unknown' , '') then null
		when Encounter.Age like '%month%' then 
			case
			when left(Encounter.Age , 2) > 23 then 2
			when left(Encounter.Age , 2) between 12 and 23 then 1
			else 0
			end
		when Encounter.Age like '%week%' then 0			-- Added Paul Egan 14/11/2014
		when Encounter.Age like '%day%' then 0			-- Added Paul Egan 14/11/2014
		else cast(left(Encounter.Age , charindex(' ' , Encounter.Age) - 1) as int)
		end

	,InitialAssessmentTime = null
	,SeenForTreatmentTime = FirstCons.ConsultationStartTime
	,AttendanceConclusionTime = FirstCons.ConsultationEndTime
	,DepartureTime = FirstCons.ConsultationEndTime
	,CommissioningSerialNo = 
		rtrim(
			coalesce(	
				 --Practice.ParentOrganisationCode
				 Postcode.PCTCode
				--,Encounter.ResidencePCTCode
				,'X98'
			) 
		) + '00A'

	,NHSServiceAgreementLineNo = null
	,ProviderReferenceNo = null
	,CommissionerReferenceNo = null
	,ProviderCode = 'RW3'

	,CommissionerCode = 
		coalesce(	
			 --Practice.ParentOrganisationCode
			 Postcode.PCTCode
			--,Encounter.ResidencePCTCode
			,'X98'
		) 

	,StaffMemberCode = null
	,InvestigationCodeFirst = null
	,InvestigationCodeSecond = null
	,DiagnosisCodeFirst = null
	,DiagnosisCodeSecond = null
	,TreatmentCodeFirst = null
	,TreatmentCodeSecond = null
	,PASHRGCode = null
	,HRGVersionCode = null
	,PASDGVPCode = null
	,SiteCode = 'RW3MR'
	,Created = null
	,Updated = null
	,ByWhom = null
	,InterfaceCode = 'ADAS'

	,PCTCode = 
		coalesce(	
			 --Practice.ParentOrganisationCode
			 Postcode.PCTCode
			--,Encounter.ResidencePCTCode
			,'X98'
		) 

	,ResidencePCTCode = coalesce(Postcode.PCTCode , 'X98') 

	,InvestigationCodeList = null
	,TriageCategoryCode = null
	,PresentingProblem = null
	,ToXrayTime = null
	,FromXrayTime = null
	,ToSpecialtyTime = null
	,SeenBySpecialtyTime = null
	,EthnicCategoryCode = null
	,ReferredToSpecialtyCode = null
	,DecisionToAdmitTime = null
	,DischargeDestinationCode = null
	,RegisteredTime = null
	,TransportRequestTime = null
	,TransportAvailableTime = null
	,AscribeLeftDeptTime = null
	,CDULeftDepartmentTime = null
	,PCDULeftDepartmentTime = null

	,AmbulanceArrivalTime = null
	,AmbulanceCrewPRF = null
	,ArrivalTimeAdjusted = CaseExport.EntryTime
	,UnplannedReattend7Day = 0
	,ClinicalAssessmentTime = null

from
	TImportPCECAEFullCase Encounter

left join (
		select distinct
			 SourceUniqueID
			,CaseNo
			,EntryTime = cast(EntryTime as datetime)
		from
			TImportPCECAECaseExport
) CaseExport
on	Encounter.CaseNo = CaseExport.CaseNo

left join (
		select distinct
			 CaseNo
			,ConsultationStartTime = cast(ConsultationStartTime as datetime)
			,ConsultationEndTime = cast(ConsultationEndTime as datetime)
		from
			 TImportPCECAEEntryToFirstCons
) FirstCons
on	Encounter.CaseNo = FirstCons.CaseNo


left join (
		select distinct
			 CaseNo
			,Answer
		from
			 TImportPCECAECaseQuestions a
		where
			Question = 'Travel arrangements to the WIC'
		and	not exists (
				select
					1
				from
					TImportPCECAECaseQuestions b
				where
					Question = 'Travel arrangements to the WIC'
				and	a.CaseNo = b.CaseNo
				and	a.Answer > b.Answer
			)
) ArrivalMode
on	Encounter.CaseNo = ArrivalMode.CaseNo



--left join Organisation.dbo.Practice Practice
--on	Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode

left join [$(Organisation)].dbo.Postcode Postcode on
	Postcode.Postcode = 
		case
		when len(Encounter.Postcode) = 8 then Encounter.Postcode
		else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
		end collate Latin1_General_CI_AS
where
	Encounter.CaseNo is not null
and	rtrim(Encounter.CaseNo) != ''
and	Encounter.CaseNo not in ('79815' , '79823')


















﻿

CREATE VIEW [dbo].[TLoadCOMHealthOrg] 

AS

SELECT
	 HealthOrgID--[HEORG_REFNO]
	
	,HealthOrgTypeID--[HEORG_REFNO]--[HEORG_REFNO]

	,HealthOrgCode--[MAIN_IDENT]
	
	,HealthOrg--[DESCRIPTION]
	
	,HealthOrgParentID--[PARNT_REFNO]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]

	,ArchiveFlag--[ARCHV_FLAG]

	,HealthOrgOwnerID--[OWNER_HEORG_REFNO]

FROM (

SELECT 
	 HealthOrgID--[HEORG_REFNO]
	
	,HealthOrgTypeID--[HEORG_REFNO]--[HEORG_REFNO]

	,HealthOrgCode--[MAIN_IDENT]
	
	,HealthOrg--[DESCRIPTION]
	
	,HealthOrgParentID--[PARNT_REFNO]
	
	,StartTime--[START_DTTM]
	
	,EndTime--[END_DTTM]

	,ArchiveFlag--[ARCHV_FLAG]

	,HealthOrgOwnerID--[OWNER_HEORG_REFNO]
	
FROM dbo.TImportCOMHealthOrg

) HealthOrg

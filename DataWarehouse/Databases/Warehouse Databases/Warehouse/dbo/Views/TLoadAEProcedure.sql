﻿create view [dbo].[TLoadAEProcedure] as

select
	 SourceUniqueID
	,ProcedureDate
	,SequenceNo
	,ProcedureCode
	,AESourceUniqueID
	,SourceProcedureCode
from
	dbo.TImportAEProcedure

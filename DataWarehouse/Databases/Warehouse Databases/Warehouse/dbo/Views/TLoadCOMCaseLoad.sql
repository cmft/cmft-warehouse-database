﻿
CREATE VIEW [dbo].[TLoadCOMCaseLoad] 

AS

SELECT
	 [CaseLoadID]
	
	,[ReferralID]
	
	,[RoleTypeID]
	
	,[StatusID]
	
	,AllocationTime

	,AllocationDate
	
	,AllocationReasonID
	
	,InterventionlevelID
	
	,DischargeTime

	,DischargeDate
	
	,Outcome
	
	,AllocationProfessionalCarerID
	
	,AllocationSpecialtyID
	
	,AllocationStaffTeamID
	
	,ResponsibleHealthOrganisation
	
	,DischargeReasonID
	
	,PatientSourceID
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator
	
	,PatientTitleID
	
	,PatientForename
	
	,PatientSurname
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,PatientCurrentRegisteredPracticeCode
		
	,PatientEncounterRegisteredPracticeCode
	
	,PatientCurrentRegisteredGPCode
		
	,PatientEncounterRegisteredGPCode
	
	,[CreatedTime]
	
	,[ModifiedTime]
	
	,[CreatedByID]
	
	,[ModifiedByID]
	
	,[ArchiveFlag]
	
	,[HealthOrgOwnerID]

FROM (

SELECT 
	 [CaseLoadID]
	
	,[ReferralID]
	
	,[RoleTypeID]
	
	,[StatusID]
	
	,AllocationTime
	
	,AllocationDate = CONVERT(Date,AllocationTime,103)
	
	,AllocationReasonID
	
	,InterventionlevelID
	
	,DischargeTime
	
	,DischargeDate = CONVERT(Date,DischargeTime,103)
	
	,Outcome
	
	,AllocationProfessionalCarerID
	
	,AllocationSpecialtyID
	
	,AllocationStaffTeamID
	
	,ResponsibleHealthOrganisation
	
	,DischargeReasonID
	
	,PatientSourceID
	
	,PatientSourceSystemUniqueID
	
	,PatientNHSNumber
	
	,PatientNHSNumberStatusIndicator
	
	,PatientTitleID
	
	,PatientForename
	
	,PatientSurname
	
	,PatientAddress1
	
	,PatientAddress2
	
	,PatientAddress3
	
	,PatientAddress4
	
	,PatientPostcode
	
	,PatientDateOfBirth
	
	,PatientDateOfDeath
	
	,PatientSexID
	
	,PatientEthnicGroupID
	
	,PatientCurrentRegisteredPracticeCode
		
	,PatientEncounterRegisteredPracticeCode
	
	,PatientCurrentRegisteredGPCode
		
	,PatientEncounterRegisteredGPCode
	
	,[CreatedTime]
	
	,[ModifiedTime]
	
	,[CreatedByID]
	
	,[ModifiedByID]
	
	,[ArchiveFlag]
	
	,[HealthOrgOwnerID]
	
FROM dbo.TImportCOMCaseLoad

) CaseLoad

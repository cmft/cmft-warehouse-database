﻿










CREATE VIEW [dbo].[TLoadAEEncounter] as

select
 	 SourceUniqueID
	,UniqueBookingReferenceNo
	,PathwayId
	,PathwayIdIssuerCode
	,RTTStatusCode
	,RTTStartDate
	,RTTEndDate
	,DistrictNo
	,TrustNo
	,CasenoteNo
	,DistrictNoOrganisationCode
	,NHSNumber
	,NHSNumberStatusId
	,PatientTitle
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Encounter.Postcode
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,CarerSupportIndicator
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,AttendanceNumber
	,ArrivalModeCode
	,AttendanceCategoryCode
	,AttendanceDisposalCode
	,SourceAttendanceDisposalCode
	,IncidentLocationTypeCode
	,PatientGroupCode
	,SourceOfReferralCode
	,ArrivalDate
	,ArrivalTime
	,AgeOnArrival
	,InitialAssessmentTime
	,SeenForTreatmentTime
	,AttendanceConclusionTime
	,DepartureTime
	,CommissioningSerialNo
	,NHSServiceAgreementLineNo
	,ProviderReferenceNo
	,CommissionerReferenceNo
	,ProviderCode
	,CommissionerCode
	,StaffMemberCode
	,InvestigationCodeFirst
	,InvestigationCodeSecond
	,DiagnosisCodeFirst
	,DiagnosisCodeSecond
	,TreatmentCodeFirst
	,TreatmentCodeSecond
	,PASHRGCode
	,HRGVersionCode
	,PASDGVPCode
	,SiteCode
	,Created
	,Updated
	,ByWhom
	,InterfaceCode
	,PCTCode
	,ResidencePCTCode
	,InvestigationCodeList
	,TriageCategoryCode
	,PresentingProblem
	,PresentingProblemCode
	,ToXrayTime
	,FromXrayTime
	,ToSpecialtyTime
	,SeenBySpecialtyTime
	,EthnicCategoryCode
	,ReligionCode
	,ReferredToSpecialtyCode
	,DecisionToAdmitTime
	,DischargeDestinationCode
	,RegisteredTime
	,TransportRequestTime
	,TransportAvailableTime
	,AscribeLeftDeptTime
	,CDULeftDepartmentTime
	,PCDULeftDepartmentTime

	,AmbulanceArrivalTime
	,AmbulanceCrewPRF
	,UnplannedReattend7Day

	,ArrivalTimeAdjusted =
		--ISNULL(
		--	CASE 
		--	WHEN dateadd(MINUTE , 15 , AE.AE_Format_Ambulancedatetime(AmbulanceArrivalTime , ArrivalTime)) > convert(smalldatetime , ArrivalTime , 103)	THEN ArrivalTime
		--	ELSE dateadd(MINUTE , 15 , AE.AE_Format_Ambulancedatetime(AmbulanceArrivalTime , ArrivalTime))
		--	END
		--	,ArrivalTime
		--)--GS 20110406  New column added. Takes the adjusted ambulance time or time arrived - whichever is lower
		case
		when datediff(minute , AmbulanceArrivalTime , ArrivalTime) > 15 then dateadd(minute , 15 , AmbulanceArrivalTime)
		else ArrivalTime
		end

	,ClinicalAssessmentTime
	,Reportable
	,SourceCareGroupCode
	,AlcoholLocation
	,AlcoholInPast12Hours
	,RapidAssessmentTime
	,TriageComment
	,CCGCode

	,GpCodeAtAttendance
	,GpPracticeCodeAtAttendance
	,PostcodeAtAttendance
from
	(
	select distinct  -- 2010-07-26 CCB Added to remove duplicate record created at Extract-time; needs debugging
		 SourceUniqueID
		,UniqueBookingReferenceNo
		,PathwayId
		,PathwayIdIssuerCode
		,RTTStatusCode
		,RTTStartDate
		,RTTEndDate
		,Encounter.DistrictNo
		,TrustNo
		,CasenoteNo
		,DistrictNoOrganisationCode
		,NHSNumber
		,NHSNumberStatusId
		,PatientTitle
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4

		,Postcode =
			case
			when Encounter.Postcode = '' then null
			else Encounter.Postcode
			end

		,DateOfBirth
		,DateOfDeath
		,SexCode
		,CarerSupportIndicator
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,AttendanceNumber

		,ArrivalModeCode =
			case SiteCode
			when 'RW3DH' then 6480 -- All Dental patients are coded 6480 (national code of 2), users do not enter this data
			else ArrivalModeCode
			end
		
		,AttendanceCategoryCode
		,AttendanceDisposalCode
		,SourceAttendanceDisposalCode
		,IncidentLocationTypeCode

		,PatientGroupCode =
			case SiteCode
			when 'RW3SM' then 80 -- All St Mary's patients are type 80, but users do not enter this data
			when 'RW3DH' then 80 -- All Dental patients are type 80, but users do not enter this data
			else PatientGroupCode
			end

		,SourceOfReferralCode
		,ArrivalDate
		,ArrivalTime
		,AgeOnArrival
		,InitialAssessmentTime
		,SeenForTreatmentTime

		,AttendanceConclusionTime = 
			case
			when AttendanceConclusionTime > getdate() then null
			else AttendanceConclusionTime
			end

		,DepartureTime

		,CommissioningSerialNo = 
			CASE 
			WHEN InterfaceCode = 'ADAS' THEN '5NT00A'
			ELSE
				rtrim(
					coalesce(	
						 Practice.ParentOrganisationCode
						,Postcode.PCTCode
						,Encounter.ResidencePCTCode
						,'X98'
					) 
				) + '00A'
			END

		,NHSServiceAgreementLineNo
		,ProviderReferenceNo
		,CommissionerReferenceNo
		,ProviderCode

		,CommissionerCode = 
			coalesce(	
				 Practice.ParentOrganisationCode
				,Postcode.PCTCode
				,Encounter.ResidencePCTCode
				,'X98'
			) 

		,StaffMemberCode
		,InvestigationCodeFirst
		,InvestigationCodeSecond
		,DiagnosisCodeFirst
		,DiagnosisCodeSecond
		,TreatmentCodeFirst
		,TreatmentCodeSecond
		,PASHRGCode
		,HRGVersionCode
		,PASDGVPCode
		,SiteCode
		,Created
		,Updated
		,ByWhom
		,InterfaceCode

		,PCTCode = 
			coalesce(	
				 Practice.ParentOrganisationCode
				,Postcode.PCTCode
				,Encounter.ResidencePCTCode
				,'X98'
			) 

		,ResidencePCTCode =
			case
			when Encounter.Postcode like 'ZZ99%' then 'X98'
			else coalesce(
					 Postcode.PCTCode
					,Encounter.ResidencePCTCode
				)
			end

		,InvestigationCodeList
		,TriageCategoryCode
		,PresentingProblem
		,PresentingProblemCode
		,ToXrayTime
		,FromXrayTime
		,ToSpecialtyTime
		,SeenBySpecialtyTime
		,EthnicCategoryCode
		,ReligionCode 
		,ReferredToSpecialtyCode
		,DecisionToAdmitTime
		,DischargeDestinationCode

		,RegisteredTime =
			case
			when RegisteredTime > getdate()	then null
			else RegisteredTime
			end

		,TransportRequestTime
		,TransportAvailableTime
		,AscribeLeftDeptTime
		,CDULeftDepartmentTime
		,PCDULeftDepartmentTime

		,AmbulanceArrivalTime =
			case
			when datediff(hour
				,dateadd(minute , cast(right(AmbulanceArrivalTimeInt, 2) as int) ,
					dateadd(hour , cast(left(REPLACE(STR(AmbulanceArrivalTimeInt, 4), SPACE(1), '0')  , 2) as int) ,
						dateadd(day , 0 , datediff(day , 0 , Encounter.ArrivalDate)
						)
					)
				)
				,ArrivalTime
				) > 12
			then
				dateadd(minute , cast(right(AmbulanceArrivalTimeInt, 2) as int) ,
					dateadd(hour , cast(left(REPLACE(STR(AmbulanceArrivalTimeInt, 4), SPACE(1), '0')  , 2) as int) ,
						dateadd(day , -1 , Encounter.ArrivalDate)
					)
				)
			else
				dateadd(minute , cast(right(AmbulanceArrivalTimeInt, 2) as int) ,
					dateadd(hour , cast(left(REPLACE(STR(AmbulanceArrivalTimeInt, 4), SPACE(1), '0')  , 2) as int) ,
						Encounter.ArrivalDate
					)
				)
			end

		,AmbulanceCrewPRF --GS 20110406  New column added
		,UnplannedReattend7Day

		,ClinicalAssessmentTime

		,Reportable =
			case
			when
					SourceAttendanceDisposalCode IN ('17756' , '15140') -- 'transfer' and 'Transfered to Other health care provider'
				and	Encounter.DischargeDestinationCode = '15141' -- WIC1
				and	ArrivalDate >= '1 Apr 2011' -- PCEC data begins
			then 0
			else 1
			end

		,SourceCareGroupCode
		,AlcoholLocation
		,AlcoholInPast12Hours
		,RapidAssessmentTime
		,TriageComment



		,CCGCode =
				coalesce(
					 CCGPractice.ParentOrganisationCode
					,CCGPostcode.CCGCode
					,'00W'
				)
		,GpCodeAtAttendance = GpAtAttendance.GpCode
		,GpPracticeCodeAtAttendance = GpAtAttendance.GpPracticeCode
		,PostcodeAtAttendance  = 

		
			case
				when datalength(rtrim(ltrim(AddressAtAttendance.Postcode))) = 6 then left(AddressAtAttendance.Postcode, 2) + '   ' + right(AddressAtAttendance.Postcode, 3)
				when datalength(rtrim(ltrim(AddressAtAttendance.Postcode))) = 7 then left(AddressAtAttendance.Postcode, 3) + '  ' + right(AddressAtAttendance.Postcode, 3)
				else AddressAtAttendance.Postcode
			end

	from
		dbo.TImportAEEncounter Encounter

	left join [$(Organisation)].dbo.Practice Practice
	on	Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode

	left join [$(Organisation)].dbo.Postcode Postcode on
		Postcode.Postcode = 
			case
			when len(Encounter.Postcode) = 8 then Encounter.Postcode
			else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
			end

	left join [PAS].[PatientGp] GpAtAttendance
	on GpAtAttendance.DistrictNo = Encounter.DistrictNo
	and Encounter.ArrivalDate between GpAtAttendance.EffectiveFromDate and coalesce(GpAtAttendance.EffectiveToDate, getdate())

	left join PAS.PatientAddress AddressAtAttendance
	on AddressAtAttendance.DistrictNo = Encounter.DistrictNo
	and Encounter.ArrivalDate between AddressAtAttendance.EffectiveFromDate and coalesce(AddressAtAttendance.EffectiveToDate, getdate())

	left join [$(Organisation)].dbo.CCGPractice CCGPractice
	on CCGPractice.OrganisationCode = GpAtAttendance.GpPracticeCode
	
	--EpisodicGp.GpPracticeCode

	--left join Organisation.dbo.CCGPostcode CCGPostcode
	--on	CCGPostcode.Postcode =
	--		case
	--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
	--		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
	--		else Encounter.Postcode
	--		end

	left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
	on	CCGPostcode.Postcode =
			case
			when datalength(rtrim(ltrim(AddressAtAttendance.Postcode))) = 6 then left(AddressAtAttendance.Postcode, 2) + '   ' + right(AddressAtAttendance.Postcode, 3)
			when datalength(rtrim(ltrim(AddressAtAttendance.Postcode))) = 7 then left(AddressAtAttendance.Postcode, 3) + '  ' + right(AddressAtAttendance.Postcode, 3)
			else Encounter.Postcode
			end

	) Encounter

where
	Encounter.AttendanceNumber not in (
		 'MAE-11-055562-1'
		,'MAE-11-053787-1'
		,'MAE-11-024719-1'
		,'MAE-11-033069-1'
		,'MAE-11-048468'
		,'MAE-11-035556-1'
		)
	






















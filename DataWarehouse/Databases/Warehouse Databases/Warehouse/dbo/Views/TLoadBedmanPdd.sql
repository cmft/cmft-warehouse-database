﻿








CREATE VIEW [dbo].[TLoadBedmanPdd] as


/****** Script for SelectTopNRows command from SSMS  ******/

SELECT
	 tbl_pdd.PddHistoryID
	,tbl_pdd.SourceSpellNo
	,tbl_pdd.PddSequence
	,tbl_prev_pdd.PddSequence AS prev_PddSequence
	,tbl_next_pdd.PddSequence AS next_PddSequence
	,tbl_firsteddhis.Pdd AS FirstPdd
	,tbl_Lasteddhis.Pdd As LastPdd
	,tbl_pdd.Pdd
	,tbl_prev_pdd.Pdd AS prev_Pdd
	,tbl_next_pdd.Pdd AS next_Pdd
	,tbl_pdd.PddCreated
	,tbl_pdd.PddPAS
	,tbl_pdd.PddReasonCode
FROM
	dbo.TImportBedmanPdd tbl_pdd

LEFT OUTER JOIN dbo.TImportBedmanPdd tbl_prev_pdd 
ON	tbl_pdd.SourceSpellNo = tbl_prev_pdd.SourceSpellNo
AND tbl_pdd.PddSequence = tbl_prev_pdd.PddSequence + 1

LEFT OUTER JOIN dbo.TImportBedmanPdd tbl_next_pdd 
ON	tbl_pdd.SourceSpellNo = tbl_next_pdd.SourceSpellNo
AND tbl_pdd.PddSequence = tbl_next_pdd.PddSequence - 1

LEFT OUTER JOIN
	(
	SELECT
		 PDDHistory.SourceSpellNo
		,PDDHistory.Pdd
		,PDDHistory.PddCreated
	FROM 
		dbo.TImportBedmanPdd PDDHistory
	WHERE
		NOT EXISTS
			(
			SELECT
				1
			FROM 
				dbo.TImportBedmanPdd  FirstPDDHistory
			WHERE 
				FirstPDDHistory.SourceSpellNo = PDDHistory.SourceSpellNo
			AND FirstPDDHistory.PddHistoryID < PDDHistory.PddHistoryID
			)
	) AS tbl_firsteddhis
ON tbl_pdd.SourceSpellNo = tbl_firsteddhis.SourceSpellNo

LEFT OUTER JOIN
	(
	SELECT
		 PDDHistory.SourceSpellNo
		,PDDHistory.Pdd
		,PDDHistory.PddCreated
	FROM 
		dbo.TImportBedmanPdd PDDHistory
	WHERE 
		NOT EXISTS	
			(
			SELECT	
				1
			FROM 
				dbo.TImportBedmanPdd  FirstPDDHistory
			WHERE 
				FirstPDDHistory.SourceSpellNo = PDDHistory.SourceSpellNo
			AND FirstPDDHistory.PddHistoryID > PDDHistory.PddHistoryID
			)
	) AS tbl_Lasteddhis
ON tbl_pdd.SourceSpellNo = tbl_Lasteddhis.SourceSpellNo


	











﻿
create view dbo.TLoadCMISDocumentsPrinted

as

select  
	[HospitalNumber]
	,[Occurrence]
	,[Spell]
	,[Episode] = ltrim(rtrim([Episode]))
	,[UserID]
	,[DateStamp]
	,[Location]
	,[DocumentReference]
	,[DocumentName]
	,[DocumentDescription]
	,[NumberOfCopies]

from dbo.TImportCMISDocumentsPrinted
﻿CREATE TABLE [dbo].[TImportOrderCommsBasicObservation] (
    [SiteID]               SMALLINT      NULL,
    [ReplicationFlags]     TINYINT       NULL,
    [ModifiedBy]           VARCHAR (50)  NULL,
    [ModifiedTime]         DATETIME      NULL,
    [CreatedBy]            VARCHAR (50)  NULL,
    [CreatedTime]          DATETIME      NULL,
    [Active]               TINYINT       NULL,
    [SourceUniqueID]       NUMERIC (18)  NOT NULL,
    [EnteredTime]          DATETIME      NULL,
    [Status]               VARCHAR (10)  NULL,
    [ToBeVerified]         TINYINT       NULL,
    [ToBeSigned]           TINYINT       NULL,
    [EnterRole]            VARCHAR (30)  NULL,
    [UserID]               NUMERIC (18)  NULL,
    [ClientVisitID]        NUMERIC (18)  NULL,
    [ClientID]             NUMERIC (18)  NULL,
    [ChartID]              NUMERIC (18)  NULL,
    [Item]                 VARCHAR (160) NULL,
    [RestrictedAccessFlag] TINYINT       NULL,
    [AuthorisedTime]       DATETIME      NULL,
    [ClusterID]            VARCHAR (20)  NULL,
    [Value]                VARCHAR (60)  NULL,
    [TypeCode]             VARCHAR (2)   NULL,
    [IsHistoryFlag]        TINYINT       NULL,
    [IsTextualFlag]        TINYINT       NULL,
    [HasHistoryFlag]       TINYINT       NULL,
    [AbnormalityCode]      VARCHAR (5)   NULL,
    [ReferenceUpperLimit]  VARCHAR (10)  NULL,
    [ReferenceLowerLimit]  VARCHAR (10)  NULL,
    [UnitOfMeasure]        VARCHAR (30)  NULL,
    [OrderID]              NUMERIC (18)  NULL,
    [ResultItemID]         NUMERIC (18)  NULL,
    [MasterID]             NUMERIC (18)  NULL,
    [ArrivalTime]          DATETIME      NULL,
    [HasMediaLinkFlag]     TINYINT       NULL,
    [Build]                INT           NULL
);


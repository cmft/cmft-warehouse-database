﻿CREATE TABLE [dbo].[TImportRADRoomBase] (
    [ImportDateTimeStamp] DATETIME       NULL,
    [Code]                VARCHAR (4000) NULL,
    [Dept]                VARCHAR (4000) NULL,
    [Dosage typ]          VARCHAR (4000) NULL,
    [End date]            VARCHAR (4000) NULL,
    [Hospital]            VARCHAR (4000) NULL,
    [Modality]            VARCHAR (4000) NULL,
    [Name]                VARCHAR (4000) NULL
);


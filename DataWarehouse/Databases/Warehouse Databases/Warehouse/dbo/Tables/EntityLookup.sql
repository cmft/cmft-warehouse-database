﻿CREATE TABLE [dbo].[EntityLookup] (
    [EntityTypeCode] VARCHAR (50)  NOT NULL,
    [EntityCode]     VARCHAR (50)  NOT NULL,
    [Description]    VARCHAR (MAX) NULL,
    [Created]        DATETIME      CONSTRAINT [df_EntityLookupCreated] DEFAULT (getdate()) NULL,
    [Updated]        DATETIME      CONSTRAINT [df_EntityLookupUpdated] DEFAULT (getdate()) NULL,
    [ByWhom]         VARCHAR (50)  CONSTRAINT [df_EntityLookupUpdatedBy] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_EntityLookup] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC, [EntityCode] ASC)
);




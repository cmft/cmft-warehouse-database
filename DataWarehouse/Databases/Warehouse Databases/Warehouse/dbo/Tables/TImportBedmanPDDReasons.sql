﻿CREATE TABLE [dbo].[TImportBedmanPDDReasons] (
    [PDDReasonID] INT          NOT NULL,
    [PDDGroup]    VARCHAR (10) NOT NULL,
    [PDDReason]   VARCHAR (30) NOT NULL,
    [Active]      TINYINT      NOT NULL
);


﻿CREATE TABLE [dbo].[lospts] (
    [Date]                DATETIME       NULL,
    [LOSGroup]            NVARCHAR (255) NULL,
    [Division]            NVARCHAR (255) NULL,
    [Act Type]            NVARCHAR (255) NULL,
    [Ward]                NVARCHAR (255) NULL,
    [Ward Description]    NVARCHAR (255) NULL,
    [LoS]                 FLOAT (53)     NULL,
    [Spec]                FLOAT (53)     NULL,
    [Specialty]           NVARCHAR (255) NULL,
    [Admit Date]          DATETIME       NULL,
    [Ward Start]          DATETIME       NULL,
    [CaseNoteNo]          NVARCHAR (255) NULL,
    [Patient First Names] NVARCHAR (255) NULL,
    [Patient Surnames]    NVARCHAR (255) NULL,
    [Age]                 FLOAT (53)     NULL,
    [Consultant]          NVARCHAR (255) NULL,
    [IntMan]              NVARCHAR (255) NULL,
    [ExpLOS]              FLOAT (53)     NULL
);


﻿CREATE TABLE [dbo].[TImportAEDiagnosis] (
    [SourceUniqueID]          NUMERIC (9)  NOT NULL,
    [SequenceNo]              SMALLINT     NULL,
    [DiagnosisCode]           VARCHAR (6)  NULL,
    [AESourceUniqueID]        VARCHAR (50) NOT NULL,
    [SourceDiagnosisCode]     VARCHAR (50) NULL,
    [SourceDiagnosisSiteCode] VARCHAR (50) NULL,
    [SourceDiagnosisSideCode] NCHAR (10)   NULL,
    [DiagnosisSiteCode]       VARCHAR (10) NULL,
    [DiagnosisSideCode]       VARCHAR (10) NULL
);


﻿CREATE TABLE [dbo].[EntityType] (
    [EntityTypeCode] VARCHAR (50) NOT NULL,
    [EntityType]     VARCHAR (50) NULL,
    [Created]        DATETIME     CONSTRAINT [df_EntityTypeCreated] DEFAULT (getdate()) NULL,
    [Updated]        DATETIME     CONSTRAINT [df_EntityTypeUpdated] DEFAULT (getdate()) NULL,
    [ByWhom]         VARCHAR (50) CONSTRAINT [df_EntityTypeUpdatedBy] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_EntityType] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC)
);




﻿CREATE TABLE [dbo].[TImportRADWardBase] (
    [ImportDateTimeStamp] DATETIME     NULL,
    [Code]                VARCHAR (50) NULL,
    [Cost centre]         VARCHAR (50) NULL,
    [End date]            VARCHAR (50) NULL,
    [Group]               VARCHAR (50) NULL,
    [HIS code]            VARCHAR (50) NULL,
    [Hospital]            VARCHAR (50) NULL,
    [Local copy]          VARCHAR (50) NULL,
    [Location i hrs]      VARCHAR (50) NULL,
    [Location o hrs]      VARCHAR (50) NULL,
    [Location rep]        VARCHAR (50) NULL,
    [Ward Name]           VARCHAR (50) NULL,
    [Patient type]        VARCHAR (50) NULL,
    [Printer]             VARCHAR (50) NULL,
    [Request cat]         VARCHAR (50) NULL,
    [Source]              VARCHAR (50) NULL
);


﻿CREATE TABLE [dbo].[Exclusions] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [IdentifierCode]   VARCHAR (50)  NULL,
    [IdentifierType]   VARCHAR (50)  NULL,
    [IdentifierSource] VARCHAR (50)  NULL,
    [RequestedBy]      VARCHAR (50)  NULL,
    [Comments]         VARCHAR (MAX) NULL,
    [EffectiveFrom]    DATETIME      DEFAULT ('01/01/1900') NULL,
    [EffectiveTo]      DATETIME      NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


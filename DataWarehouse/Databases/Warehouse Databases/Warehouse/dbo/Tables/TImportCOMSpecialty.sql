﻿CREATE TABLE [dbo].[TImportCOMSpecialty] (
    [SpecialtyID]             NUMERIC (10)  NULL,
    [SpecialtyCode]           VARCHAR (20)  NULL,
    [Specialty]               VARCHAR (255) NULL,
    [SpecialtyParentID]       NUMERIC (10)  NULL,
    [SpecialtyHealthOrgOwner] NUMERIC (10)  NULL,
    [ArchiveFlag]             CHAR (1)      NULL
);


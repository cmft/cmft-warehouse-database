﻿CREATE TABLE [dbo].[TImportPCECAERelationshipToCallerTHT] (
    [Location]             VARCHAR (MAX) NULL,
    [CaseNo]               VARCHAR (MAX) NULL,
    [ActiveTime]           VARCHAR (MAX) NULL,
    [CaseType]             VARCHAR (MAX) NULL,
    [RelationshipToCaller] VARCHAR (MAX) NULL,
    [Created]              DATETIME2 (7) CONSTRAINT [DF_dboTImportPCECAERelationshipToCallerTHTCreated] DEFAULT (getdate()) NULL
);


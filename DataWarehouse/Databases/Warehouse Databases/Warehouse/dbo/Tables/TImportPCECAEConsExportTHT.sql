﻿CREATE TABLE [dbo].[TImportPCECAEConsExportTHT] (
    [Location]                 VARCHAR (MAX) NULL,
    [SourceUniqueID]           VARCHAR (MAX) NULL,
    [CaseNo]                   VARCHAR (MAX) NULL,
    [ActiveTime]               VARCHAR (MAX) NULL,
    [CaseType]                 VARCHAR (MAX) NULL,
    [Organisation]             VARCHAR (MAX) NULL,
    [ConsultationStartTime]    VARCHAR (MAX) NULL,
    [ConsultationEndTime]      VARCHAR (MAX) NULL,
    [ConsultationStartType]    VARCHAR (MAX) NULL,
    [ConsultationEndType]      VARCHAR (MAX) NULL,
    [ConsultingClinician]      VARCHAR (MAX) NULL,
    [ConsultationHistory]      VARCHAR (MAX) NULL,
    [ConsultationExamination]  VARCHAR (MAX) NULL,
    [ConsultationDiagnosis]    VARCHAR (MAX) NULL,
    [ConsultationExamination2] VARCHAR (MAX) NULL,
    [ConsultationObsolete]     VARCHAR (MAX) NULL,
    [Cancelled]                VARCHAR (MAX) NULL,
    [TestCall]                 VARCHAR (MAX) NULL,
    [Created]                  DATETIME2 (7) CONSTRAINT [DF_dboTImportPCECAEConsExportTHTCreated] DEFAULT (getdate()) NULL
);


﻿CREATE TABLE [dbo].[TImportBedmanPdd] (
    [SourceSpellNo] INT      NOT NULL,
    [PddHistoryID]  INT      NOT NULL,
    [PddSequence]   BIGINT   NULL,
    [Pdd]           DATETIME NULL,
    [PddCreated]    DATETIME NULL,
    [PddPAS]        INT      NULL,
    [PddReasonCode] INT      NULL
);


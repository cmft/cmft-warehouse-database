﻿CREATE TABLE [dbo].[PathwayDiagnosisData] (
    [ProviderSpell] VARCHAR (50) NULL,
    [PathwayNo]     INT          NULL,
    [PathwayDesc]   VARCHAR (10) NULL
);


﻿CREATE TABLE [dbo].[TImportRADRadiologistBase] (
    [ImportDateTimeStamp] DATETIME     NULL,
    [Code]                VARCHAR (50) NULL,
    [End date]            VARCHAR (50) NULL,
    [Name]                VARCHAR (50) NULL,
    [Suspended]           VARCHAR (50) NULL,
    [Type]                VARCHAR (50) NULL,
    [Unprinted]           VARCHAR (50) NULL,
    [Unverified]          VARCHAR (50) NULL,
    [User ID]             VARCHAR (50) NULL
);


﻿CREATE TABLE [dbo].[TImportServiceDeskEncounter] (
    [EncounterRecNo]       DECIMAL (18)    NULL,
    [SourceUniqueID]       DECIMAL (18)    NOT NULL,
    [Request]              NVARCHAR (80)   NULL,
    [StartTime]            DATETIME        NULL,
    [EndTime]              DATETIME        NULL,
    [DeadlineTime]         DATETIME        NULL,
    [Category]             NVARCHAR (255)  NULL,
    [Workgroup]            NVARCHAR (50)   NULL,
    [CreatedBy]            NVARCHAR (80)   NULL,
    [AssignedTo]           NVARCHAR (80)   NULL,
    [CreatedTime]          DATETIME        NULL,
    [Priority]             NVARCHAR (255)  NULL,
    [RequestStatus]        NVARCHAR (255)  NULL,
    [ResolvedTime]         DATETIME        NULL,
    [Template]             NVARCHAR (50)   NULL,
    [ExceededDeadlineFlag] DECIMAL (1)     NULL,
    [Classification]       NVARCHAR (255)  NULL,
    [ClosureDescription]   NVARCHAR (255)  NULL,
    [AssignmentCount]      DECIMAL (10)    NULL,
    [CallerFullName]       NVARCHAR (80)   NULL,
    [CallerEmail]          NVARCHAR (255)  NULL,
    [CallerOrganization]   NVARCHAR (50)   NULL,
    [SolutionNotes]        NVARCHAR (4000) NULL,
    [History]              NTEXT           NULL,
    [Impact]               NVARCHAR (255)  NULL
);


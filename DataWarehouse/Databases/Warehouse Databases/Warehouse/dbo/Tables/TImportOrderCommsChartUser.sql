﻿CREATE TABLE [dbo].[TImportOrderCommsChartUser] (
    [SiteID]                SMALLINT     NULL,
    [ReplicationFlags]      TINYINT      NULL,
    [ModifiedBy]            VARCHAR (50) NULL,
    [ModifiedTime]          DATETIME     NULL,
    [CreatedBy]             VARCHAR (50) NULL,
    [CreatedTime]           DATETIME     NULL,
    [Active]                TINYINT      NULL,
    [SourceUniqueID]        NUMERIC (18) NOT NULL,
    [ChartID]               NUMERIC (18) NULL,
    [UserID]                NUMERIC (18) NULL,
    [LastSeenResultTime]    DATETIME     NULL,
    [LastSeenOrderTime]     DATETIME     NULL,
    [TrackNewFlag]          TINYINT      NULL,
    [LastSeenStatOrderTime] DATETIME     NULL,
    [SinceTimeResult]       VARCHAR (15) NULL,
    [TimeScaleResult]       VARCHAR (10) NULL,
    [SinceTimeDocument]     VARCHAR (15) NULL,
    [TimeScaleDocument]     VARCHAR (10) NULL,
    [LastSeenAlertTime]     DATETIME     NULL,
    [LastSeenDocumentTime]  DATETIME     NULL,
    [Build]                 INT          NULL
);


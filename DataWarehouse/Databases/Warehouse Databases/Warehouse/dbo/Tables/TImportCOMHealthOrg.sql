﻿CREATE TABLE [dbo].[TImportCOMHealthOrg] (
    [HealthOrgID]       NUMERIC (10)  NULL,
    [HealthOrgTypeID]   NUMERIC (10)  NULL,
    [HealthOrgCode]     VARCHAR (20)  NULL,
    [HealthOrg]         VARCHAR (255) NULL,
    [HealthOrgParentID] NUMERIC (10)  NULL,
    [StartTime]         DATETIME      NULL,
    [EndTime]           DATETIME      NULL,
    [ArchiveFlag]       CHAR (1)      NULL,
    [HealthOrgOwnerID]  NUMERIC (10)  NULL
);


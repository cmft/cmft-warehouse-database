﻿CREATE TABLE [dbo].[AuditProcess] (
    [ProcessCode]       VARCHAR (255) NOT NULL,
    [Process]           VARCHAR (255) NULL,
    [ParentProcessCode] VARCHAR (255) NULL,
    [ProcessSourceCode] VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Process] PRIMARY KEY CLUSTERED ([ProcessCode] ASC),
    CONSTRAINT [FK_AuditProcess_AuditProcess] FOREIGN KEY ([ProcessSourceCode]) REFERENCES [dbo].[AuditProcessSource] ([ProcessSourceCode])
);


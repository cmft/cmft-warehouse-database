﻿CREATE TABLE [dbo].[TImportBedmanDailyAcuityScore] (
    [DailyScoreID]         INT      NOT NULL,
    [WardCode]             CHAR (4) NOT NULL,
    [ScoreTime]            DATETIME NOT NULL,
    [TotalPatientsLevel0]  INT      NOT NULL,
    [TotalPatientsLevel1a] INT      NOT NULL,
    [TotalPatientsLevel1b] INT      NOT NULL,
    [TotalPatientsLevel2]  INT      NOT NULL,
    [TotalPatientsLevel3]  INT      NOT NULL,
    [AvailableBeds]        INT      NOT NULL,
    CONSTRAINT [PK_DAS] PRIMARY KEY CLUSTERED ([DailyScoreID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [IDX_uniq_WardCode_ScoresTime] UNIQUE NONCLUSTERED ([WardCode] ASC, [ScoreTime] ASC) WITH (FILLFACTOR = 90)
);


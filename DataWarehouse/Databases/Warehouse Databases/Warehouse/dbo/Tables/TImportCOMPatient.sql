﻿CREATE TABLE [dbo].[TImportCOMPatient] (
    [SourceUniqueID]       NUMERIC (10)  NULL,
    [SourceSystemUniqueID] VARCHAR (20)  NULL,
    [NHSNumber]            VARCHAR (20)  NULL,
    [SexID]                NUMERIC (10)  NULL,
    [Forename]             VARCHAR (30)  NULL,
    [TitleID]              NUMERIC (10)  NULL,
    [Surname]              VARCHAR (30)  NULL,
    [EthnicGroupID]        NUMERIC (10)  NULL,
    [MaritalStatusID]      NUMERIC (10)  NULL,
    [DateofBirth]          DATETIME      NULL,
    [BirthLocation]        VARCHAR (80)  NULL,
    [OccupationID]         NUMERIC (10)  NULL,
    [SpokenLanguageID]     NUMERIC (10)  NULL,
    [RegisteredDisabled]   VARCHAR (1)   NULL,
    [Deceased]             VARCHAR (1)   NULL,
    [DeceasedTime]         DATETIME      NULL,
    [DeceasedLocation]     VARCHAR (80)  NULL,
    [ReligionID]           NUMERIC (10)  NULL,
    [PDSUpdateNeeded]      VARCHAR (25)  NULL,
    [PDSReason]            VARCHAR (500) NULL,
    [PDSStopNoted]         VARCHAR (1)   NULL,
    [CreatedTime]          DATETIME      NULL,
    [ModifiedTime]         DATETIME      NULL,
    [CreatedByID]          VARCHAR (30)  NULL,
    [ModifiedByID]         VARCHAR (30)  NULL,
    [ArchiveFlag]          VARCHAR (1)   NULL,
    [HealthOrgOwnerID]     NUMERIC (10)  NULL
);


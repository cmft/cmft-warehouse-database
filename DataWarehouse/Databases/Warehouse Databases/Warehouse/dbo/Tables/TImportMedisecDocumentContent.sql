﻿CREATE TABLE [dbo].[TImportMedisecDocumentContent] (
    [PatientNumber]   VARCHAR (8)  NOT NULL,
    [AdmissionDate]   DATETIME     NOT NULL,
    [ForcebackNumber] VARCHAR (2)  NOT NULL,
    [TemplateCode]    VARCHAR (20) NOT NULL,
    [Content]         XML          NULL
);


﻿CREATE TABLE [dbo].[TImportCMISDocumentsPrinted] (
    [HospitalNumber]      VARCHAR (20)  NOT NULL,
    [Occurrence]          INT           NULL,
    [Spell]               VARCHAR (20)  NULL,
    [Episode]             VARCHAR (2)   NULL,
    [UserID]              VARCHAR (10)  NULL,
    [DateStamp]           DATETIME2 (7) NOT NULL,
    [Location]            VARCHAR (35)  NULL,
    [DocumentReference]   VARCHAR (30)  NULL,
    [DocumentName]        VARCHAR (30)  NULL,
    [DocumentDescription] VARCHAR (50)  NULL,
    [NumberOfCopies]      INT           NULL
);


﻿CREATE TABLE [dbo].[TImportCOMServicePoint] (
    [ServicePointID]          NUMERIC (10)  NULL,
    [ServicePointCode]        VARCHAR (20)  NULL,
    [ServicePoint]            VARCHAR (80)  NULL,
    [ServicePointDescription] VARCHAR (255) NULL,
    [ServicePointSpecialty]   NUMERIC (10)  NULL,
    [ServicePointHealthOrgID] NUMERIC (10)  NULL,
    [StartTime]               DATETIME      NULL,
    [EndTime]                 DATETIME      NULL,
    [ServicePointTypeID]      NUMERIC (10)  NULL,
    [ArchiveFlag]             CHAR (1)      NULL,
    [HealthOrgOwner]          NUMERIC (10)  NULL
);


﻿CREATE TABLE [dbo].[TImportCOMProfessionalCarer] (
    [ProfessionalCarerID]     NUMERIC (10) NULL,
    [ProfessionalCarerCode]   VARCHAR (25) NULL,
    [ProfessionalCarerTypeID] NUMERIC (10) NULL,
    [Title]                   VARCHAR (80) NULL,
    [Forename]                VARCHAR (30) NULL,
    [Surname]                 VARCHAR (30) NULL,
    [StartTime]               DATETIME     NULL,
    [EndTime]                 DATETIME     NULL,
    [PrimaryStaffTeamID]      NUMERIC (10) NULL,
    [ArchiveFlag]             CHAR (1)     NULL,
    [HealthOrgOwner]          NUMERIC (10) NULL
);


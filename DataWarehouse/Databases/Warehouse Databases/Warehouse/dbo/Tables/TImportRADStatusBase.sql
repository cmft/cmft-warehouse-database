﻿CREATE TABLE [dbo].[TImportRADStatusBase] (
    [ImportDateTimeStamp] DATETIME     NULL,
    [Category]            VARCHAR (60) NULL,
    [Code]                VARCHAR (60) NULL,
    [Default]             VARCHAR (60) NULL,
    [End date]            VARCHAR (60) NULL,
    [Long desc]           VARCHAR (60) NULL,
    [Order]               VARCHAR (60) NULL,
    [Short desc]          VARCHAR (60) NULL,
    [Type]                VARCHAR (60) NULL
);


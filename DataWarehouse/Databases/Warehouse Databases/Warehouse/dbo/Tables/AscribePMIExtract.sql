﻿CREATE TABLE [dbo].[AscribePMIExtract] (
    [PatientTelephone]      VARCHAR (23)    NULL,
    [PatientWorkTelephone]  VARCHAR (23)    NULL,
    [PatientTitle]          NVARCHAR (5)    NULL,
    [PatientForename]       NVARCHAR (20)   NULL,
    [PatientSurname]        NVARCHAR (24)   NULL,
    [PatientGender]         NVARCHAR (1)    NULL,
    [PatientDateOfBirth]    DATE            NULL,
    [PatientNhsNumber]      NVARCHAR (4000) NULL,
    [PatientCaseNumber]     NVARCHAR (14)   NULL,
    [PatientDoorNumber]     VARCHAR (1)     NOT NULL,
    [PatientBuilding]       VARCHAR (1)     NOT NULL,
    [PatientStreet]         NVARCHAR (20)   NULL,
    [PatientTown]           NVARCHAR (20)   NULL,
    [PatientDistrict]       NVARCHAR (20)   NULL,
    [PatientLocalAuthority] VARCHAR (20)    NULL,
    [PatientPostCode]       NVARCHAR (10)   NULL,
    [GP]                    VARCHAR (8)     NULL,
    [GPPractice]            VARCHAR (6)     NULL,
    [CurrentCaseNumber]     VARCHAR (20)    NULL
);


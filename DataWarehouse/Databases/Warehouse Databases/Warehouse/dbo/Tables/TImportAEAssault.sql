﻿CREATE TABLE [dbo].[TImportAEAssault] (
    [SourceUniqueID]             VARCHAR (50)   NULL,
    [AESourceUniqueID]           VARCHAR (50)   NOT NULL,
    [AssaultDate]                VARCHAR (255)  NULL,
    [AssaultTime]                VARCHAR (255)  NULL,
    [AssaultWeapon]              VARCHAR (80)   NULL,
    [AssaultWeaponDetails]       VARCHAR (4000) NULL,
    [AssaultLocation]            VARCHAR (80)   NULL,
    [AssaultLocationDetails]     VARCHAR (4000) NULL,
    [AlcoholConsumed3Hour]       VARCHAR (80)   NULL,
    [AssaultRelationship]        VARCHAR (80)   NULL,
    [AssaultRelationshipDetails] VARCHAR (4000) NULL
);


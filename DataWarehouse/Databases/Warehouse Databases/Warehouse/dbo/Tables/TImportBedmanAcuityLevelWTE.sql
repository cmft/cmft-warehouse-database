﻿CREATE TABLE [dbo].[TImportBedmanAcuityLevelWTE] (
    [LevelID]                INT            NOT NULL,
    [WardTypeID]             INT            NOT NULL,
    [AcuityMutiplierLevel0]  DECIMAL (8, 2) NOT NULL,
    [AcuityMutiplierLevel1a] DECIMAL (8, 2) NOT NULL,
    [AcuityMutiplierLevel1b] DECIMAL (8, 2) NOT NULL,
    [AcuityMutiplierLevel2]  DECIMAL (8, 2) NOT NULL,
    [AcuityMutiplierLevel3]  DECIMAL (8, 2) NOT NULL,
    PRIMARY KEY CLUSTERED ([LevelID] ASC) WITH (FILLFACTOR = 90)
);


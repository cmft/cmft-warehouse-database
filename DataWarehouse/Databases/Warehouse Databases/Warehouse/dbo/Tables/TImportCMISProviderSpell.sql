﻿CREATE TABLE [dbo].[TImportCMISProviderSpell] (
    [HospitalNumber]       VARCHAR (20)  NOT NULL,
    [Occurrence]           INT           NOT NULL,
    [Spell]                VARCHAR (20)  NOT NULL,
    [IsMotherOrInfant]     CHAR (1)      NULL,
    [SpellStatus]          CHAR (1)      NULL,
    [SpellStartDate]       DATETIME2 (7) NULL,
    [SpellStartTime]       VARCHAR (4)   NULL,
    [SpellEndDate]         DATETIME2 (7) NULL,
    [SpellEndTime]         VARCHAR (4)   NULL,
    [SpellLengthInDays]    INT           NULL,
    [SpellLengthInHours]   INT           NULL,
    [SpellLengthInMinutes] INT           NULL,
    [BuyerCode]            VARCHAR (5)   NULL,
    [ContractNumber]       VARCHAR (6)   NULL,
    [DateStamp]            DATETIME2 (7) NULL,
    [UserID]               VARCHAR (10)  NULL,
    [Location]             VARCHAR (35)  NULL
);


﻿CREATE TABLE [dbo].[TImportRadiologyStatusBase] (
    [Category]   VARCHAR (8)  NULL,
    [Code]       VARCHAR (6)  NULL,
    [Default]    VARCHAR (7)  NULL,
    [End date]   VARCHAR (10) NULL,
    [Long desc]  VARCHAR (60) NULL,
    [Order]      VARCHAR (5)  NULL,
    [Short desc] VARCHAR (30) NULL,
    [Type]       VARCHAR (6)  NULL
);


﻿CREATE TABLE [dbo].[TImportAEInvestigation] (
    [SourceUniqueID]          VARCHAR (50)  NOT NULL,
    [InvestigationDate]       SMALLDATETIME NULL,
    [SequenceNo]              SMALLINT      NULL,
    [InvestigationCode]       VARCHAR (6)   NULL,
    [AESourceUniqueID]        VARCHAR (50)  NOT NULL,
    [SourceInvestigationCode] VARCHAR (6)   NULL,
    [ResultDate]              SMALLDATETIME NULL
);


GO
CREATE NONCLUSTERED INDEX [Idx_TII_SourceIdx]
    ON [dbo].[TImportAEInvestigation]([SourceUniqueID] ASC, [AESourceUniqueID] ASC)
    INCLUDE([InvestigationCode], [SourceInvestigationCode]);


GO
CREATE NONCLUSTERED INDEX [Idx_TII_SourceIdx2]
    ON [dbo].[TImportAEInvestigation]([SourceUniqueID] ASC, [AESourceUniqueID] ASC, [InvestigationCode] ASC, [SourceInvestigationCode] ASC);


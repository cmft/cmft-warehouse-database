﻿CREATE TABLE [dbo].[TImportCancelledOperation] (
    [SourceUniqueID]      INT            NULL,
    [CasenoteNumber]      VARCHAR (100)  NULL,
    [Directorate]         VARCHAR (100)  NULL,
    [AdmissionDate]       DATE           NULL,
    [ProcedureDate]       DATE           NULL,
    [Anaesthetist]        VARCHAR (100)  NULL,
    [Surgeon]             VARCHAR (100)  NULL,
    [CancellationDate]    DATE           NULL,
    [CancellationReason]  VARCHAR (100)  NULL,
    [TCIDate]             DATE           NULL,
    [Comments]            NVARCHAR (MAX) NULL,
    [ProcedureCompleted]  VARCHAR (100)  NULL,
    [BreachwatchComments] VARCHAR (MAX)  NULL,
    [Removed]             VARCHAR (100)  NULL,
    [RemovedComments]     VARCHAR (MAX)  NULL,
    [ReportableBreach]    VARCHAR (100)  NULL,
    [NationalSpecialty]   VARCHAR (100)  NULL,
    [ConsultantCode]      VARCHAR (100)  NULL,
    [WardCode]            VARCHAR (100)  NULL,
    [ProcedureCode]       VARCHAR (100)  NULL,
    [ContextCode]         VARCHAR (5)    NULL
);


﻿CREATE TABLE [dbo].[TImportOrderCommsOrderStatus] (
    [SiteID]           SMALLINT     NULL,
    [ReplicationFlags] TINYINT      NULL,
    [ModifiedBy]       VARCHAR (50) NULL,
    [ModifiedTime]     DATETIME     NULL,
    [CreatedBy]        VARCHAR (50) NULL,
    [CreatedTime]      DATETIME     NULL,
    [Active]           TINYINT      NULL,
    [SourceUniqueID]   NUMERIC (18) NOT NULL,
    [OrderStatusCode]  VARCHAR (30) NULL,
    [OrderStatus]      VARCHAR (60) NULL,
    [LevelNumber]      INT          NULL,
    [Build]            INT          NULL
);


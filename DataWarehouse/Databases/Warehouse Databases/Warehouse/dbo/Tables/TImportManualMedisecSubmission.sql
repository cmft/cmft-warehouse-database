﻿CREATE TABLE [dbo].[TImportManualMedisecSubmission] (
    [Ward]          NVARCHAR (50) NULL,
    [DischargeDate] DATETIME      NULL,
    [SubmittedBy]   NVARCHAR (50) NULL,
    [Total]         INT           NULL,
    [<24Hours]      INT           NULL,
    [24-48Hours]    INT           NULL,
    [>48Hours]      INT           NULL
);


﻿CREATE TABLE [dbo].[TImportOrderCommsTextualObservationLine] (
    [SiteID]                   SMALLINT      NULL,
    [ReplicationFlags]         TINYINT       NULL,
    [ModifiedBy]               VARCHAR (50)  NULL,
    [ModifiedTime]             DATETIME      NULL,
    [CreatedBy]                VARCHAR (50)  NULL,
    [CreatedTime]              DATETIME      NULL,
    [Active]                   TINYINT       NULL,
    [SourceUniqueID]           NUMERIC (18)  NOT NULL,
    [TextualObservationLine]   VARCHAR (255) NULL,
    [TextualObservationLineNo] INT           NULL,
    [ObservationID]            NUMERIC (18)  NULL,
    [ClientID]                 NUMERIC (18)  NULL,
    [Build]                    INT           NULL
);


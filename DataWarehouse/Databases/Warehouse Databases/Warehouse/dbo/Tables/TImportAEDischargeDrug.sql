﻿CREATE TABLE [dbo].[TImportAEDischargeDrug] (
    [SourceUniqueID]   INT      NOT NULL,
    [AESourceUniqueID] INT      NOT NULL,
    [DischargeDate]    DATE     NULL,
    [DischargeTime]    DATETIME NULL,
    [DrugCode]         INT      NOT NULL,
    [SequenceNo]       INT      NOT NULL
);


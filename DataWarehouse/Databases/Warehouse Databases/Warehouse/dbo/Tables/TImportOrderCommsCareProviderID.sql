﻿CREATE TABLE [dbo].[TImportOrderCommsCareProviderID] (
    [SiteID]             SMALLINT     NULL,
    [ReplicationFlags]   TINYINT      NULL,
    [ModifiedBy]         VARCHAR (50) NULL,
    [ModifiedTime]       DATETIME     NULL,
    [CreatedBy]          VARCHAR (50) NULL,
    [CreatedTime]        DATETIME     NULL,
    [Active]             TINYINT      NULL,
    [SourceUniqueID]     NUMERIC (18) NOT NULL,
    [ProviderIDCode]     VARCHAR (20) NULL,
    [ProviderIDTypeCode] VARCHAR (30) NULL,
    [ProviderID]         NUMERIC (18) NULL,
    [Build]              INT          NULL
);


﻿CREATE TABLE [dbo].[TImportOrderCommsPhone] (
    [SiteID]            SMALLINT     NULL,
    [ReplicationFlags]  TINYINT      NULL,
    [ModifiedBy]        VARCHAR (50) NULL,
    [ModifiedTime]      DATETIME     NULL,
    [CreatedBy]         VARCHAR (50) NULL,
    [CreatedTime]       DATETIME     NULL,
    [Active]            TINYINT      NULL,
    [SourceUniqueID]    NUMERIC (18) NOT NULL,
    [PhoneNumber]       VARCHAR (40) NULL,
    [PhoneNote]         VARCHAR (60) NULL,
    [PhoneType]         VARCHAR (15) NULL,
    [PhoneAreaCode]     VARCHAR (10) NULL,
    [PersonID]          NUMERIC (18) NULL,
    [ClientVisitID]     NUMERIC (18) NULL,
    [ApplicationSource] CHAR (5)     NULL,
    [ScopeLevel]        CHAR (1)     NULL,
    [Build]             INT          NULL
);


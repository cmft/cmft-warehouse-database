﻿CREATE TABLE [dbo].[TImportRADSpecialtyBase] (
    [ImportDateTimeStamp] DATETIME       NULL,
    [Code]                VARCHAR (4000) NULL,
    [End date]            VARCHAR (4000) NULL,
    [Group]               VARCHAR (4000) NULL,
    [Name]                VARCHAR (4000) NULL
);


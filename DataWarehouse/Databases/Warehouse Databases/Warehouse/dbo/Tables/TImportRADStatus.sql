﻿CREATE TABLE [dbo].[TImportRADStatus] (
    [ImportDateTimeStamp] DATETIME       NULL,
    [Cat at End Date]     VARCHAR (4000) NULL,
    [Cat]                 VARCHAR (4000) NULL,
    [Code At End Date]    VARCHAR (4000) NULL,
    [CRIS No ]            VARCHAR (4000) NULL,
    [Cur]                 VARCHAR (4000) NULL,
    [Date]                VARCHAR (4000) NULL,
    [Deleted]             VARCHAR (4000) NULL,
    [Description]         VARCHAR (4000) NULL,
    [End date]            VARCHAR (4000) NULL,
    [End time]            VARCHAR (4000) NULL,
    [Event Key]           VARCHAR (4000) NULL,
    [Exam Key]            VARCHAR (4000) NULL,
    [Exam]                VARCHAR (4000) NULL,
    [Other code]          VARCHAR (4000) NULL,
    [Site]                VARCHAR (4000) NULL,
    [Start date]          VARCHAR (4000) NULL,
    [Start time]          VARCHAR (4000) NULL,
    [Status Cde]          VARCHAR (4000) NULL,
    [Status Key]          VARCHAR (4000) NULL,
    [Time]                VARCHAR (4000) NULL,
    [Type]                VARCHAR (4000) NULL,
    [Type at End Date]    VARCHAR (4000) NULL,
    [User ID]             VARCHAR (4000) NULL
);


﻿CREATE TABLE [dbo].[TImportRADRadiographerBase] (
    [ImportDateTimeStamp] DATETIME     NULL,
    [Code]                VARCHAR (50) NULL,
    [End date]            VARCHAR (50) NULL,
    [Name]                VARCHAR (50) NULL,
    [Student grading]     VARCHAR (50) NULL,
    [User ID]             VARCHAR (50) NULL
);


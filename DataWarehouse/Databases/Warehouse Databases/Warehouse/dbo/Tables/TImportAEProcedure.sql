﻿CREATE TABLE [dbo].[TImportAEProcedure] (
    [SourceUniqueID]      NUMERIC (9)  NOT NULL,
    [ProcedureDate]       DATETIME     NULL,
    [SequenceNo]          BIGINT       NULL,
    [ProcedureCode]       VARCHAR (6)  NULL,
    [AESourceUniqueID]    VARCHAR (50) NOT NULL,
    [SourceProcedureCode] VARCHAR (6)  NULL
);


﻿CREATE TABLE [dbo].[TImportMedisecInpatientSpell] (
    [PatientNumber]             VARCHAR (8)  NOT NULL,
    [AdmissionDate]             DATETIME     NOT NULL,
    [ForcebackNumber]           VARCHAR (2)  NOT NULL,
    [InpatientStatus]           VARCHAR (1)  NULL,
    [AdmissionTime]             VARCHAR (4)  NULL,
    [SpecialtyCode]             VARCHAR (5)  NULL,
    [ConsultantCode]            VARCHAR (6)  NULL,
    [WardCode]                  VARCHAR (4)  NULL,
    [HospitalCode]              VARCHAR (4)  NULL,
    [PatientCategoryCode]       VARCHAR (2)  NULL,
    [AdmissionMethodCode]       VARCHAR (2)  NULL,
    [AdmissionSourceCode]       VARCHAR (2)  NULL,
    [ReferenceCode]             VARCHAR (8)  NULL,
    [ReferenceOrganisationCode] VARCHAR (6)  NULL,
    [DischargeDate]             DATETIME     NULL,
    [DischargeTime]             VARCHAR (4)  NULL,
    [DischargeMethodCode]       VARCHAR (2)  NULL,
    [DischargeDestinationCode]  VARCHAR (2)  NULL,
    [AdmittingHospitalCode]     VARCHAR (4)  NULL,
    [HospitalNumber]            VARCHAR (14) NULL,
    [EpisodeSecurityFlag]       VARCHAR (1)  NULL
);


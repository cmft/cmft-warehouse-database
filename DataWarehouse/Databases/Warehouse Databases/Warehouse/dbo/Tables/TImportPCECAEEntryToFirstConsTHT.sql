﻿CREATE TABLE [dbo].[TImportPCECAEEntryToFirstConsTHT] (
    [Location]              VARCHAR (MAX) NULL,
    [Provider]              VARCHAR (MAX) NULL,
    [CaseNo]                VARCHAR (MAX) NULL,
    [EntryTime]             VARCHAR (MAX) NULL,
    [ConsultationStartTime] VARCHAR (MAX) NULL,
    [ConsultationEndTime]   VARCHAR (MAX) NULL,
    [TimeTaken]             VARCHAR (MAX) NULL,
    [ConsultationStartType] VARCHAR (MAX) NULL,
    [ConsultationEndType]   VARCHAR (MAX) NULL,
    [Created]               DATETIME2 (7) CONSTRAINT [DF_dboTImportPCECAEEntryToFirstConsTHTCreated] DEFAULT (getdate()) NULL
);


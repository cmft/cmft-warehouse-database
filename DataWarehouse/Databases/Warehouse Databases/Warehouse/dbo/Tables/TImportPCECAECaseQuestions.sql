﻿CREATE TABLE [dbo].[TImportPCECAECaseQuestions] (
    [Location]    VARCHAR (MAX) NULL,
    [CaseNo]      VARCHAR (MAX) NULL,
    [ActiveTime]  VARCHAR (MAX) NULL,
    [CaseType]    VARCHAR (MAX) NULL,
    [QuestionSet] VARCHAR (MAX) NULL,
    [SequenceNo]  VARCHAR (MAX) NULL,
    [Question]    VARCHAR (MAX) NULL,
    [Answer]      VARCHAR (MAX) NULL,
    [Created]     DATETIME2 (7) CONSTRAINT [DF_dboTImportPCECAECaseQuestionsCreated] DEFAULT (getdate()) NULL
);


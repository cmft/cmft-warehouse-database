﻿CREATE TABLE [dbo].[TImportTheatreHistory] (
    [ID]             INT           NOT NULL,
    [AmendDate]      SMALLDATETIME NULL,
    [AmendTime]      SMALLDATETIME NULL,
    [AmendUser]      INT           NULL,
    [Anaesthetist]   VARCHAR (50)  NULL,
    [CancelReason]   INT           NULL,
    [Consultant]     VARCHAR (50)  NULL,
    [EndTime]        SMALLDATETIME NULL,
    [ReAssignReason] INT           NULL,
    [SessionDate]    SMALLDATETIME NULL,
    [SessionType]    VARCHAR (50)  NULL,
    [StartTime]      VARCHAR (50)  NULL,
    [Theatre]        VARCHAR (50)  NULL,
    [InterfaceCode]  VARCHAR (10)  NULL
);


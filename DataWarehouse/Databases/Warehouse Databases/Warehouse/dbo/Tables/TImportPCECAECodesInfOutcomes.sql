﻿CREATE TABLE [dbo].[TImportPCECAECodesInfOutcomes] (
    [Location]              VARCHAR (MAX) NULL,
    [SourceUniqueID]        VARCHAR (MAX) NULL,
    [CaseNo]                VARCHAR (MAX) NULL,
    [Age]                   VARCHAR (MAX) NULL,
    [DateOfBirth]           VARCHAR (MAX) NULL,
    [CaseType]              VARCHAR (MAX) NULL,
    [CallDate]              VARCHAR (MAX) NULL,
    [DayOfWeek]             VARCHAR (MAX) NULL,
    [CallTime]              VARCHAR (MAX) NULL,
    [Postcode]              VARCHAR (MAX) NULL,
    [Codes]                 VARCHAR (MAX) NULL,
    [InformationalOutcomes] VARCHAR (MAX) NULL,
    [Created]               DATETIME2 (7) CONSTRAINT [DF_dboTImportPCECAECodesInfOutcomesCreated] DEFAULT (getdate()) NULL
);


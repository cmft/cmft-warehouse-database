﻿CREATE TABLE [dbo].[TImportRADReferrerBase] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [ImportDateTimeStamp] DATETIME       NULL,
    [Code]                VARCHAR (100)  NULL,
    [Email]               VARCHAR (100)  NULL,
    [End date]            VARCHAR (100)  NULL,
    [Gp link code]        VARCHAR (100)  NULL,
    [Group]               VARCHAR (100)  NULL,
    [HIS code]            VARCHAR (100)  NULL,
    [Initials]            VARCHAR (100)  NULL,
    [Lead clinician]      VARCHAR (100)  NULL,
    [Name]                VARCHAR (100)  NULL,
    [National code]       VARCHAR (100)  NULL,
    [Send EDI]            VARCHAR (4000) NULL,
    [Sort code]           VARCHAR (100)  NULL,
    [Specialities]        VARCHAR (100)  NULL,
    [Start date]          VARCHAR (100)  NULL,
    [Type]                VARCHAR (100)  NULL
);


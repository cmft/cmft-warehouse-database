﻿CREATE TABLE [dbo].[TImportAEPCECBreaches] (
    [AttendanceNumber]                  VARCHAR (20)  NOT NULL,
    [PatientSurname]                    VARCHAR (100) NULL,
    [ArrivalDate]                       DATE          NULL,
    [ArrivalTime]                       DATETIME      NULL,
    [SeenForTreatmentTime]              DATETIME      NULL,
    [AttendanceConclusionTime]          DATETIME      NULL,
    [DepartureTime]                     DATETIME      NULL,
    [MinutesInDepartment]               INT           NULL,
    [ValidatedArrivalTime]              DATETIME      NULL,
    [ValidatedSeenForTreatmentTime]     DATETIME      NULL,
    [ValidatedAttendanceConclusionTime] DATETIME      NULL,
    [ValidatedDepartureTime]            DATETIME      NULL,
    [SiteCode]                          VARCHAR (10)  NULL,
    [Created]                           DATETIME2 (7) CONSTRAINT [DF_dboTImportAEPCECBreachesCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_AttendanceNumber] PRIMARY KEY CLUSTERED ([AttendanceNumber] ASC)
);




GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Sec - Warehouse BI Developers]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [cmmc\tom.smith]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Sec - Warehouse BI Developers]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Helen.Shackleton]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Diane.Thomas]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [cmmc\tom.smith]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Sec - Warehouse BI Developers]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Helen.Shackleton]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Diane.Thomas]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [cmmc\tom.smith]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Sec - Warehouse BI Developers]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Diane.Thomas]
    AS [dbo];


GO
GRANT DELETE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [cmmc\tom.smith]
    AS [dbo];


GO
GRANT DELETE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Sec - Warehouse BI Developers]
    AS [dbo];


GO
GRANT DELETE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Helen.Shackleton]
    AS [dbo];


GO
GRANT DELETE
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Diane.Thomas]
    AS [dbo];


GO
GRANT ALTER
    ON OBJECT::[dbo].[TImportAEPCECBreaches] TO [CMMC\Sec - Warehouse BI Developers]
    AS [dbo];


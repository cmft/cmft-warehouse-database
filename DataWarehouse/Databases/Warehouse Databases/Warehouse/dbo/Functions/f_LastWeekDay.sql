﻿
create function dbo.f_LastWeekDay
(
    @date datetime,
    @day tinyint
)

returns smalldatetime

as

begin

    if @date is null or @day not between 1 and 7 return null;
    
    declare @lastday datetime;
    
    set @lastday = dateadd(month, datediff(month, 0, @date), 30);
    
    return dateadd(dd, -(datepart(dw, @lastday) - @day + 7) % 7, @lastday);
    
end

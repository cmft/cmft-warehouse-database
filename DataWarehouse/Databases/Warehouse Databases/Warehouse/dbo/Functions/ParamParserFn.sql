﻿





CREATE    FUNCTION [dbo].[ParamParserFn]( @delimString varchar(8000), @delimiter varchar(10) = ',' ) 
RETURNS @paramtable 
TABLE ( ItemCode varchar(8000)) 
AS BEGIN

DECLARE
		 @len int
        ,@index int
        ,@nextindex int
		,@delimiterLength int

SET @len = DATALENGTH(@delimString)
SET @index = 0
SET @nextindex = 0
SET @delimiterLength = DATALENGTH(@delimiter)


WHILE (@len >= @index )
BEGIN

SET @nextindex = CHARINDEX(@delimiter, @delimString, @index)

if (@nextindex = 0 ) SET @nextindex = @len + @delimiterLength + 1

 INSERT @paramtable
 SELECT rtrim(ltrim(SUBSTRING( @delimString, @index, @nextindex - @index )))


SET @index = @nextindex +  + @delimiterLength

END
 RETURN
END

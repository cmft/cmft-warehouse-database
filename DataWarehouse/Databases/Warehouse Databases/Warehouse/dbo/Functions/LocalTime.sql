﻿create function [dbo].[LocalTime]
(@Date datetime)

RETURNS datetime
AS
BEGIN

	declare @WorkingYear varchar(4)
	declare @StartDay varchar(2)
	declare @EndDay varchar(2)
	declare @SummertimeStart datetime
	declare @SummertimeEnd datetime
	declare @LocalDate datetime

	set @StartDay = convert(varchar(2),(31 - (5 * Year(@Date)/4 + 4) % 7))
	set @EndDay = convert(varchar(2),(31 - (5 * Year(@Date)/4 + 1) % 7))
	set @WorkingYear = convert(varchar(4),Year(@Date))
	set @SummertimeStart = convert(datetime,@WorkingYear + '-03-' + @StartDay + ' 01:00:00',20)
	set @SummertimeEnd = convert(datetime,@WorkingYear + '-10-' + @EndDay + ' 00:00:00',20)
		
		if DateDiff(hh,@SummertimeStart,@Date) >= 0 AND DateDiff(hh,@SummertimeEnd,@Date) <= 0
		set @LocalDate = DATEADD(hh,1,@Date)
		else set @LocalDate = DATEADD(hh,0,@Date)
		
	return @LocalDate    -- Retun Value

END


﻿CREATE function [dbo].[StringToAscii]
(@input varchar(450))
returns varchar(450)
as
begin
	declare @s varchar(450) = @input
	declare @output varchar(450) = ''
	
	while len(@s) > 0
	begin
		set @output = @output + cast(ascii(substring(@s,1,1)) as varchar(450))
		set @s = substring(@s,2,len(@s)-1)	
	end
	return @output
end

﻿
create function dbo.f_Last_of_Last_Month (
	@date datetime
)
returns datetime
as
begin
	if @date is null return null;
	
	declare @end datetime
	set @end = convert(datetime, convert(char(10), dateadd(day, -datepart(day, @date), @date), 103), 103)
	return(@end)
end

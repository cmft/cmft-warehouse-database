﻿
CREATE FUNCTION [dbo].[Split]
(
	@List nvarchar(2000),
	@SplitOn nvarchar(5)
)  

returns @Value table 

(
	ID int identity(1,1),
	Value nvarchar(100)
) 

as 

begin
	while (Charindex(@SplitOn, @List) >0)
		begin
			insert into @Value (Value)
			select 
				Value = ltrim(rtrim(substring(@List, 1, charindex(@SplitOn, @List) -1)))

		set @List = substring(@List, charindex(@SplitOn, @List) + len(@SplitOn), len(@List))
		end

	insert into @Value (Value)
	select
		Value = ltrim(rtrim(@List))
return
end
﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[formatDateDDMMYYYY]
(
	-- Add the parameters for the function here
	@sourceDate  datetime
)
RETURNS varchar(8)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @reformatedDate varchar(12)

	-- Add the T-SQL statements to compute the return value here
	select @reformatedDate = 

		  substring(convert(varchar,@sourceDate,112),7,2)
		+ substring(convert(varchar,@sourceDate,112),5,2)
		+ substring(convert(varchar,@sourceDate,112),1,4)

	-- Return the result of the function
	RETURN @reformatedDate

END

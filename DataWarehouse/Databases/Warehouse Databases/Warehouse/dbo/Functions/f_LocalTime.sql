﻿
create function dbo.f_LocalTime
(@date datetime)

returns datetime

as

begin

    if @date is null return null;
    
    return case
				when @date between dateadd(hh, 1, dbo.f_LastWeekDay(cast('01/03' + '/' + cast(datepart(yy, @date) as char(4)) as datetime),1)) and dateadd(hh, 1, dbo.f_LastWeekDay(cast('01/10' + '/' + cast(datepart(yy, @date) as char(4)) as datetime), 1))
				then dateadd(hh, datediff(hh, getutcdate(),getdate()),@date)
				else @date
			end
    
end

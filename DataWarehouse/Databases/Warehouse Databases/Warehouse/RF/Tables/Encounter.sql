﻿CREATE TABLE [RF].[Encounter] (
    [EncounterRecno]               INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]               VARCHAR (50)  NOT NULL,
    [SourcePatientNo]              VARCHAR (20)  NOT NULL,
    [SourceEncounterNo]            VARCHAR (20)  NOT NULL,
    [PatientTitle]                 VARCHAR (10)  NULL,
    [PatientForename]              VARCHAR (20)  NULL,
    [PatientSurname]               VARCHAR (30)  NULL,
    [DateOfBirth]                  DATETIME      NULL,
    [DateOfDeath]                  SMALLDATETIME NULL,
    [SexCode]                      VARCHAR (1)   NULL,
    [NHSNumber]                    VARCHAR (17)  NULL,
    [DistrictNo]                   VARCHAR (14)  NULL,
    [Postcode]                     VARCHAR (10)  NULL,
    [PatientAddress1]              VARCHAR (25)  NULL,
    [PatientAddress2]              VARCHAR (25)  NULL,
    [PatientAddress3]              VARCHAR (25)  NULL,
    [PatientAddress4]              VARCHAR (25)  NULL,
    [DHACode]                      VARCHAR (3)   NULL,
    [EthnicOriginCode]             VARCHAR (4)   NULL,
    [MaritalStatusCode]            VARCHAR (1)   NULL,
    [ReligionCode]                 VARCHAR (4)   NULL,
    [RegisteredGpCode]             VARCHAR (8)   NULL,
    [RegisteredGpPracticeCode]     VARCHAR (8)   NULL,
    [EpisodicGpCode]               VARCHAR (8)   NULL,
    [EpisodicGpPracticeCode]       VARCHAR (6)   NULL,
    [EpisodicGdpCode]              VARCHAR (8)   NULL,
    [SiteCode]                     VARCHAR (5)   NULL,
    [ConsultantCode]               VARCHAR (10)  NULL,
    [SpecialtyCode]                VARCHAR (5)   NULL,
    [SourceOfReferralCode]         VARCHAR (5)   NULL,
    [PriorityCode]                 VARCHAR (5)   NULL,
    [ReferralDate]                 DATE          NULL,
    [DischargeDate]                DATE          NULL,
    [DischargeTime]                DATETIME      NULL,
    [DischargeReasonCode]          VARCHAR (4)   NULL,
    [DischargeReason]              VARCHAR (30)  NULL,
    [AdminCategoryCode]            VARCHAR (3)   NULL,
    [ContractSerialNo]             VARCHAR (6)   NULL,
    [RTTPathwayID]                 VARCHAR (25)  NULL,
    [RTTPathwayCondition]          VARCHAR (20)  NULL,
    [RTTStartDate]                 SMALLDATETIME NULL,
    [RTTEndDate]                   SMALLDATETIME NULL,
    [RTTSpecialtyCode]             VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]       VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]         VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]         SMALLDATETIME NULL,
    [RTTCurrentPrivatePatientFlag] BIT           NULL,
    [RTTOverseasStatusFlag]        BIT           NULL,
    [NextFutureAppointmentDate]    SMALLDATETIME NULL,
    [ReferralComment]              VARCHAR (25)  NULL,
    [InterfaceCode]                VARCHAR (5)   NULL,
    [Created]                      DATETIME      NULL,
    [Updated]                      DATETIME      NULL,
    [ByWhom]                       VARCHAR (128) NULL,
    [ReasonForReferralCode]        VARCHAR (10)  NULL,
    [SourceOfReferralGroupCode]    VARCHAR (10)  NULL,
    [DirectorateCode]              VARCHAR (5)   NULL,
    [CasenoteNo]                   VARCHAR (16)  NULL,
    [PCTCode]                      NVARCHAR (6)  NULL,
    CONSTRAINT [PK_Encounter_2] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC),
    CONSTRAINT [IX_Encounter_1] UNIQUE NONCLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_RTT_CurrentAccessViewIndex]
    ON [RF].[Encounter]([SourcePatientNo] ASC, [SourceEncounterNo] ASC)
    INCLUDE([SiteCode], [ReferralDate], [PriorityCode], [DischargeDate], [SourceOfReferralCode], [EpisodicGpPracticeCode], [EpisodicGpCode], [AdminCategoryCode], [ReasonForReferralCode], [ConsultantCode], [SpecialtyCode], [Postcode], [PCTCode], [SourceOfReferralGroupCode], [CasenoteNo]) WHERE ([ReferralDate]>=CONVERT([date],'2007-01-01',0) AND [SourceOfReferralCode] IS NOT NULL);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_3]
    ON [RF].[Encounter]([SourcePatientNo] ASC, [DischargeDate] ASC, [SourceEncounterNo] ASC, [SourceOfReferralCode] ASC)
    INCLUDE([CasenoteNo]);


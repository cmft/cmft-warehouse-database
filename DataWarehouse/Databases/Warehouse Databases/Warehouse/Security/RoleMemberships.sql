﻿EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\peter.graham';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datawriter', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'Dusia';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\James.Watson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\ian.howarth';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Ian.Connolly';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\joanna.ashberry';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Samaira.Akram';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Infounit members';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Delwyn.Jones';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\sec - sp information';


GO
--EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'tgh.information';


--GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Thomas.Drury';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Helen.Shackleton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Diane.Thomas';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Wendy.Collier';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\gordon.fenton';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Mohamed.Athman';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'THT\WCollier';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'THT\LSlatcher';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\paul.westhead';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'THT\KBurns';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\gareth.jones';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Malcom.hodson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Colin.Hunter';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Phil.Orrell';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Alison.Horsley';


GO
--EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmftslamlink';


--GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Suman.Sidda';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\ben.robinson';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse BI Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\prinu.merrythomas';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sandeep.Solipuram';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Central Infopath Developers';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Rachel.Royston';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\SQL.Service.BA';


GO
--EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'scc';


--GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Sec - Warehouse SQL Divisional Analysts';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'cmmc\tom.smith';


GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\Mark.Williams2';


GO
--EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'adamblack';


--GO
EXECUTE sp_addrolemember @rolename = N'db_datareader', @membername = N'CMMC\paul.miles';


GO



GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\Data.Warehouse';


GO
EXECUTE sp_addrolemember @rolename = N'db_owner', @membername = N'CMMC\Sec - Warehouse BI Developers';


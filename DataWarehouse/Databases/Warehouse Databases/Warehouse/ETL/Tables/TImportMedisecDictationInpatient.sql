﻿CREATE TABLE [ETL].[TImportMedisecDictationInpatient] (
    [SourcePatientNo] VARCHAR (20) NULL,
    [AdmissionTime]   DATETIME     NULL,
    [SpellSequenceNo] INT          NULL,
    [RequestTypeCode] VARCHAR (2)  NULL
);


﻿CREATE TABLE [ETL].[TImportAPCDementia] (
    [ProviderSpellNo]               VARCHAR (14) NULL,
    [SpellID]                       INT          NOT NULL,
    [SourceUniqueID]                INT          NOT NULL,
    [SourcePatientNo]               VARCHAR (8)  NULL,
    [SourceSpellNo]                 VARCHAR (5)  NULL,
    [AdmissionWardCode]             VARCHAR (4)  NOT NULL,
    [RecordedDate]                  DATE         NULL,
    [RecordedTime]                  DATETIME     NOT NULL,
    [KnownToHaveDementia]           INT          NULL,
    [ForgetfulnessQuestionAnswered] INT          NULL,
    [AssessmentScore]               VARCHAR (60) NULL,
    [Investigation]                 INT          NULL,
    [AssessmentNotPerformedReason]  VARCHAR (60) NULL,
    [ReferralSentDate]              VARCHAR (10) NULL,
    [InterfaceCode]                 VARCHAR (6)  NOT NULL
);


﻿CREATE TABLE [ETL].[TImportResultCWSTextResult] (
    [SourcePatientNo]     INT           NULL,
    [SourceEpisodeNo]     INT           NULL,
    [TransactionNo]       INT           NOT NULL,
    [SessionNo]           INT           NOT NULL,
    [OrderSourceUniqueID] VARCHAR (50)  NULL,
    [ConsultantCode]      VARCHAR (10)  NULL,
    [SpecialtyCode]       VARCHAR (8)   NULL,
    [OrderPriorityCode]   VARCHAR (20)  NULL,
    [OrderStatusCode]     TINYINT       NULL,
    [OrderEnteredByCode]  VARCHAR (4)   NULL,
    [OrderedByCode]       VARCHAR (20)  NULL,
    [OrderTime]           DATETIME      NULL,
    [OrderComment]        VARCHAR (MAX) NULL,
    [SampleReferenceCode] VARCHAR (50)  NULL,
    [EffectiveTime]       DATETIME      NULL,
    [LocationCode]        VARCHAR (4)   NULL,
    [ServiceCode]         VARCHAR (20)  NULL,
    [ResultCode]          CHAR (4)      NOT NULL,
    [Result]              VARCHAR (MAX) NULL
);


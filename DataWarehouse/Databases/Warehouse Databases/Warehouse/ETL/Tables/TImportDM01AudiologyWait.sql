﻿CREATE TABLE [ETL].[TImportDM01AudiologyWait] (
    [HOSPITAL_NO]         VARCHAR (MAX) NULL,
    [LASTNAME]            VARCHAR (MAX) NULL,
    [FIRSTNAME]           VARCHAR (MAX) NULL,
    [APPT_DATE]           VARCHAR (MAX) NULL,
    [APPOINTMENT_TYPE]    VARCHAR (MAX) NULL,
    [REFDATE]             VARCHAR (MAX) NULL,
    [Census]              VARCHAR (MAX) NULL,
    [TBSB]                VARCHAR (MAX) NULL,
    [Status]              VARCHAR (MAX) NULL,
    [Wait Time @ Census]  VARCHAR (MAX) NULL,
    [Wait Time @ Appt]    VARCHAR (MAX) NULL,
    [PTL wks for 6 weeks] VARCHAR (MAX) NULL
);


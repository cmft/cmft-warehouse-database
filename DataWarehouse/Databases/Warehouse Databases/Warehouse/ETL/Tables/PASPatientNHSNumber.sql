﻿CREATE TABLE [ETL].[PASPatientNHSNumber] (
    [Postcode]       VARCHAR (10) NULL,
    [DateOfBirth]    DATETIME     NULL,
    [Surname]        VARCHAR (30) NULL,
    [SexCode]        VARCHAR (1)  NULL,
    [PatientInitial] VARCHAR (1)  NULL,
    [NHSNumber]      VARCHAR (20) NULL
);


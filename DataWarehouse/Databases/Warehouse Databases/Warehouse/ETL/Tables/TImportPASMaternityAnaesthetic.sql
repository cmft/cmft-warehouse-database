﻿CREATE TABLE [ETL].[TImportPASMaternityAnaesthetic] (
    [ANANINITID]            VARCHAR (50) NULL,
    [AnaesanalgDuring]      VARCHAR (2)  NULL,
    [Aneadurlab]            VARCHAR (2)  NULL,
    [Aneadurlabreas]        VARCHAR (2)  NULL,
    [Aneapostlab]           VARCHAR (1)  NULL,
    [Aneapostlabreas]       VARCHAR (1)  NULL,
    [EpisodeNumber]         VARCHAR (12) NULL,
    [InternalPatientNumber] VARCHAR (12) NOT NULL,
    [KeMatPerAnaesAdminInt] VARCHAR (1)  NULL
);


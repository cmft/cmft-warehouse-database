﻿CREATE TABLE [ETL].[TImportCHAMPAccount] (
    [dateCreated]        VARCHAR (255) NULL,
    [AccountID]          VARCHAR (255) NULL,
    [AccountName]        VARCHAR (255) NULL,
    [appl_ContactIdName] VARCHAR (255) NULL,
    [ContactId]          VARCHAR (255) NULL,
    [FirstName]          VARCHAR (255) NULL,
    [LastName]           VARCHAR (255) NULL,
    [Created]            DATETIME2 (3) NULL
);


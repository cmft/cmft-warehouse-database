﻿CREATE TABLE [ETL].[TImportDM01URDActivity] (
    [Date listed]  VARCHAR (MAX) NULL,
    [First Name]   VARCHAR (MAX) NULL,
    [Last Name]    VARCHAR (MAX) NULL,
    [Hospital No]  VARCHAR (MAX) NULL,
    [DOB]          VARCHAR (MAX) NULL,
    [Date of test] VARCHAR (MAX) NULL,
    [Sent for]     VARCHAR (MAX) NULL,
    [Confirmed]    VARCHAR (MAX) NULL,
    [Done]         VARCHAR (MAX) NULL,
    [Comments]     VARCHAR (MAX) NULL
);


﻿CREATE TABLE [ETL].[TImportPASNeonate] (
    [NEONATEID]             VARCHAR (50)  NULL,
    [ApgarScoreAt1Min]      VARCHAR (2)   NULL,
    [ApgarScoreAt5Mins]     VARCHAR (2)   NULL,
    [BirthOrder]            VARCHAR (1)   NOT NULL,
    [DiagnosisCode1]        VARCHAR (8)   NULL,
    [DiagnosisCode2]        VARCHAR (6)   NULL,
    [EpisodeNumber]         VARCHAR (255) NULL,
    [HeadCircumference]     VARCHAR (2)   NULL,
    [InternalPatientNumber] VARCHAR (255) NULL,
    [KeMatBcgAdminInt]      VARCHAR (1)   NULL,
    [KeMatFeedingInt]       VARCHAR (1)   NULL,
    [KeMatFollUpCareInt]    VARCHAR (1)   NULL,
    [KeMatHipExamInt]       VARCHAR (1)   NULL,
    [KeMatJaundiceInt]      VARCHAR (1)   NULL,
    [KeMatMetabolicScInt]   VARCHAR (1)   NULL,
    [Length]                VARCHAR (2)   NULL,
    [PaedLenGestation]      VARCHAR (2)   NULL
);


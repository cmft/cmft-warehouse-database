﻿CREATE TABLE [ETL].[TImportObservationAlert] (
    [SourceUniqueID]                          INT           NULL,
    [CasenoteNumber]                          VARCHAR (20)  NULL,
    [DateOfBirth]                             DATE          NULL,
    [SpecialtyID]                             INT           NULL,
    [LocationID]                              INT           NULL,
    [AdmissionSourceUniqueID]                 INT           NULL,
    [TypeID]                                  INT           NULL,
    [ReasonID]                                INT           NULL,
    [ObservationSetSourceUniqueID]            INT           NULL,
    [CreatedTime]                             DATETIME      NULL,
    [BedsideDueTime]                          DATETIME      NULL,
    [EscalationTime]                          DATETIME      NULL,
    [AcceptedTime]                            DATETIME      NULL,
    [AttendedByUserID]                        INT           NULL,
    [AttendanceTypeID]                        INT           NULL,
    [Comment]                                 VARCHAR (MAX) NULL,
    [SeverityID]                              INT           NULL,
    [ClinicianSeniorityID]                    INT           NULL,
    [AcceptanceRemindersRemaining]            INT           NULL,
    [ChainSequenceNumber]                     INT           NULL,
    [NextReminderTime]                        DATETIME      NULL,
    [ClosedTime]                              DATETIME      NULL,
    [ClosureReasonID]                         INT           NULL,
    [ClosedByUserID]                          INT           NULL,
    [CancelledByObservationSetSourceUniqueID] INT           NULL,
    [LastModifiedTime]                        DATETIME      NULL
);




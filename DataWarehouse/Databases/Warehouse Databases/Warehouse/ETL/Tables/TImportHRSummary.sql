﻿CREATE TABLE [ETL].[TImportHRSummary] (
    [OrganisationCode]                    INT        NULL,
    [CensusDate]                          DATE       NULL,
    [AppraisalRequired]                   FLOAT (53) NULL,
    [AppraisalCompleted]                  FLOAT (53) NULL,
    [ClinicalMandatoryTrainingRequired]   FLOAT (53) NULL,
    [ClinicalMandatoryTrainingCompleted]  FLOAT (53) NULL,
    [CorporateMandatoryTrainingRequired]  FLOAT (53) NULL,
    [CorporateMandatoryTrainingCompleted] FLOAT (53) NULL,
    [RecentAppointment]                   FLOAT (53) NULL,
    [LongTermAppointment]                 FLOAT (53) NULL,
    [SicknessAbsenceFTE]                  FLOAT (53) NULL,
    [SicknessEstablishmentFTE]            FLOAT (53) NULL,
    [Headcount3mthRolling]                FLOAT (53) NULL,
    [FTE3mthRolling]                      FLOAT (53) NULL,
    [StartersHeadcount3mthRolling]        FLOAT (53) NULL,
    [StartersFTE3mthRolling]              FLOAT (53) NULL,
    [LeaversHeadcount3mthRolling]         FLOAT (53) NULL,
    [LeaversFTE3mthRolling]               FLOAT (53) NULL,
    [BudgetWTE]                           FLOAT (53) NULL,
    [ContractedWTE]                       FLOAT (53) NULL,
    [ClinicalBudgetWTE]                   FLOAT (53) NULL,
    [ClinicalContractedWTE]               FLOAT (53) NULL,
    [AverageWorkingDaysTimeToFill]        FLOAT (53) NULL,
    [ACAgencySpend]                       FLOAT (53) NULL,
    [HeadcountMonthly]                    FLOAT (53) NULL,
    [FTEMonthly]                          FLOAT (53) NULL,
    [StartersHeadcountMonthly]            FLOAT (53) NULL,
    [StartersFTEMonthly]                  FLOAT (53) NULL,
    [LeaversHeadcountMonthly]             FLOAT (53) NULL,
    [LeaversFTEMonthly]                   FLOAT (53) NULL,
    [BMERecentAppointment]                FLOAT (53) NULL,
    [BMELongTermAppointment]              FLOAT (53) NULL,
    [ESRSIP]                              FLOAT (53) NULL,
    [GeneralLedgerEst]                    FLOAT (53) NULL,
    [ESRSIPBand5]                         FLOAT (53) NULL,
    [GeneralLedgerEstBand5]               FLOAT (53) NULL
);






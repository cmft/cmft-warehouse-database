﻿CREATE TABLE [ETL].[TImportSIDMedicalReview] (
    [CandidateDataID]                                                     INT           NOT NULL,
    [CandidateID]                                                         INT           NOT NULL,
    [SectionID]                                                           INT           NOT NULL,
    [ActionID]                                                            INT           NOT NULL,
    [MDTDateTimeOfMDT]                                                    DATETIME      NULL,
    [MDTChairedBy]                                                        VARCHAR (MAX) NULL,
    [MDTMembersPresentDTODoctor]                                          VARCHAR (MAX) NULL,
    [MDTMembersPresentDTOANP]                                             VARCHAR (MAX) NULL,
    [MDTMembersPresentDTONurse]                                           VARCHAR (MAX) NULL,
    [MDTMembersPresentDTOPhysio]                                          VARCHAR (MAX) NULL,
    [MDTMembersPresentDTOSLT]                                             VARCHAR (MAX) NULL,
    [MDTMembersPresentDTOOT]                                              VARCHAR (MAX) NULL,
    [MDTMembersPresentDTOSpecialistNurse]                                 VARCHAR (MAX) NULL,
    [MDTMembersPresentDTOOther]                                           VARCHAR (MAX) NULL,
    [MDTMembersPresentDTOOthersText]                                      VARCHAR (MAX) NULL,
    [MDTRehabStream]                                                      VARCHAR (MAX) NULL,
    [MDTMedicalUpdateAndActionPlan]                                       VARCHAR (MAX) NULL,
    [MDTHasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia]     VARCHAR (MAX) NULL,
    [MDTHasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection] VARCHAR (MAX) NULL,
    [MDTBarthelScores]                                                    VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTFeeding]                                          VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTBathing]                                          VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTGrooming]                                         VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTDressing]                                         VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTBowels]                                           VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTBladder]                                          VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTToiletUse]                                        VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTTransfers]                                        VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTMobility]                                         VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTStairs]                                           VARCHAR (MAX) NULL,
    [MDTBarthelScoresMDTTotalScore]                                       VARCHAR (MAX) NULL,
    [MDTWaterlowScore]                                                    VARCHAR (MAX) NULL,
    [MDTMUST]                                                             VARCHAR (MAX) NULL,
    [MDTFallsRiskScore]                                                   VARCHAR (MAX) NULL,
    [MDTModifiedRankingScore]                                             VARCHAR (MAX) NULL,
    [MDTTOMsAphasia]                                                      VARCHAR (MAX) NULL,
    [MDTTOMsDysphagia]                                                    VARCHAR (MAX) NULL,
    [MDTMoodAlgorithmaReviewed]                                           VARCHAR (MAX) NULL,
    [MDTFormalMoodAssessmentRequired]                                     VARCHAR (MAX) NULL,
    [MDTMoodToolUsed]                                                     VARCHAR (MAX) NULL,
    [MDTMoodAssessmentScore]                                              VARCHAR (MAX) NULL,
    [MDTDateTimeMoodAssessment]                                           DATETIME      NULL,
    [MDTMoCAAssessmentScore]                                              VARCHAR (MAX) NULL,
    [MDTDateTimeMoCAAssessmentScore]                                      DATETIME      NULL,
    [MDTNursing]                                                          VARCHAR (MAX) NULL,
    [MDTPatientImpairments]                                               VARCHAR (MAX) NULL,
    [MDTFunctionalStatus]                                                 VARCHAR (MAX) NULL,
    [MDTEligibleForESD]                                                   VARCHAR (MAX) NULL,
    [MDTEligibleForESDNo]                                                 VARCHAR (MAX) NULL,
    [MDTESD]                                                              VARCHAR (MAX) NULL,
    [MDTComplexDischarge]                                                 VARCHAR (MAX) NULL,
    [MDTSocialServices]                                                   VARCHAR (MAX) NULL,
    [MDTICT]                                                              VARCHAR (MAX) NULL,
    [MDTHomepathway]                                                      VARCHAR (MAX) NULL,
    [MDTReablement]                                                       VARCHAR (MAX) NULL,
    [MDTDischargePlanningNotes]                                           VARCHAR (MAX) NULL,
    [MDTNotes]                                                            VARCHAR (MAX) NULL,
    [MDTUsername]                                                         VARCHAR (MAX) NULL,
    [MDTModifiedDate]                                                     VARCHAR (MAX) NULL,
    [PrincipleDiagnosisAndMedicalUpdate]                                  VARCHAR (MAX) NULL,
    [CTMRScanResults]                                                     VARCHAR (MAX) NULL,
    [CoMorbiditiesCongestiveHeartFailure]                                 VARCHAR (MAX) NULL,
    [CoMorbiditiesHypertension]                                           VARCHAR (MAX) NULL,
    [CoMorbiditiesAtrialFibrillation]                                     VARCHAR (MAX) NULL,
    [CoMorbiditiesDiabeties]                                              VARCHAR (MAX) NULL,
    [CoMorbiditiesStroke]                                                 VARCHAR (MAX) NULL,
    [CoMorbiditiesTIA]                                                    VARCHAR (MAX) NULL,
    [CoMorbiditiesOtherMorbidities]                                       VARCHAR (MAX) NULL,
    [CoMorbiditiesNoMorbidities]                                          VARCHAR (MAX) NULL,
    [WasThePatientAlreadyOnAntiplateletMedication]                        VARCHAR (MAX) NULL,
    [WasThePatientAlreadyOnAnticoagulant]                                 VARCHAR (MAX) NULL,
    [HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia]        VARCHAR (MAX) NULL,
    [HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection]    VARCHAR (MAX) NULL,
    [FunctionalBaseline]                                                  VARCHAR (MAX) NULL,
    [MoCAAssessmentScore]                                                 VARCHAR (MAX) NULL,
    [DateTimeMoCAAssessmentScore]                                         DATETIME      NULL,
    [ActionPlanGoals]                                                     VARCHAR (MAX) NULL,
    [AdviceOnSmoking]                                                     VARCHAR (MAX) NULL,
    [AdviceOnDriving]                                                     VARCHAR (MAX) NULL,
    [AdviceOnAlcoholConsumption]                                          VARCHAR (MAX) NULL,
    [AdviceOnExercise]                                                    VARCHAR (MAX) NULL,
    [Username]                                                            VARCHAR (MAX) NULL,
    [ModifiedDate]                                                        DATETIME      NULL,
    [IsSectionComplete]                                                   VARCHAR (MAX) NULL,
    [SectionCompleteTime]                                                 DATETIME      NULL
);


﻿CREATE TABLE [ETL].[TImportResultGPICEOrder] (
    [SourceUniqueID]      INT          NOT NULL,
    [PatientID]           INT          NULL,
    [PatientIdentifier]   VARCHAR (50) NULL,
    [DistrictNo]          VARCHAR (8)  NULL,
    [CasenoteNumber]      VARCHAR (20) NULL,
    [NHSNumber]           VARCHAR (10) NULL,
    [ClinicianID]         INT          NULL,
    [MainSpecialtyCode]   INT          NULL,
    [ProviderID]          INT          NOT NULL,
    [LocationID]          INT          NOT NULL,
    [OrderPriority]       VARCHAR (20) NOT NULL,
    [OrderStatusCode]     VARCHAR (3)  NULL,
    [OrderRequestTime]    DATETIME     NULL,
    [OrderReceivedTime]   DATETIME     NULL,
    [OrderCollectionTime] DATETIME     NULL
);




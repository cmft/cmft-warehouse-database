﻿CREATE TABLE [ETL].[TImportNeonatalBeddayDrug] (
    [EpisodeID]    UNIQUEIDENTIFIER NOT NULL,
    [CareDate]     DATE             NULL,
    [DrugCode]     INT              NOT NULL,
    [ModifiedTime] DATETIME         NOT NULL
);


﻿CREATE TABLE [ETL].[TImportBedComplementWardBed] (
    [WardID]         INT           NOT NULL,
    [InpatientBeds]  INT           NULL,
    [DaycaseBeds]    INT           NULL,
    [SingleRoomBeds] INT           NULL,
    [Comment]        VARCHAR (255) NULL,
    [StartDate]      DATE          NOT NULL
);


﻿CREATE TABLE [ETL].[TImportCHAMPContactUsRequest] (
    [id]                     VARCHAR (255) NULL,
    [timeOfContactUsRequest] VARCHAR (255) NULL,
    [email]                  VARCHAR (255) NULL,
    [content]                VARCHAR (MAX) NULL,
    [Created]                DATETIME      NULL
);


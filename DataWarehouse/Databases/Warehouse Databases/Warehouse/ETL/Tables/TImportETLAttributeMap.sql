﻿CREATE TABLE [ETL].[TImportETLAttributeMap] (
    [AttributeCode]      VARCHAR (50)  NULL,
    [ContextCode]        VARCHAR (50)  NULL,
    [Context]            VARCHAR (100) NULL,
    [AttributeValueCode] VARCHAR (50)  NULL,
    [AttributeValue]     VARCHAR (100) NULL,
    [LocalValueCode]     VARCHAR (50)  NULL,
    [LocalValue]         VARCHAR (100) NULL,
    [NationalValueCode]  VARCHAR (50)  NULL
);


﻿CREATE TABLE [ETL].[TImportObservationObservationSet] (
    [SourceUniqueID]                       INT          NOT NULL,
    [CasenoteNumber]                       VARCHAR (20) NULL,
    [DateOfBirth]                          DATE         NULL,
    [SpecialtyID]                          INT          NULL,
    [LocationID]                           INT          NULL,
    [LocationStartTime]                    DATETIME     NULL,
    [ReplacedSourceUniqueID]               INT          NULL,
    [CurrentObservationSetFlag]            BIT          NULL,
    [AdditionalObservationSetFlag]         BIT          NULL,
    [AdmissionSourceUniqueID]              INT          NULL,
    [EarlyWarningScoreRegimeApplicationID] INT          NULL,
    [ObservationProfileApplicationID]      INT          NULL,
    [ObservationNotTakenReasonID]          INT          NULL,
    [ClinicianPresentSeniorityID]          INT          NULL,
    [StartTime]                            DATETIME     NULL,
    [TakenTime]                            DATETIME     NULL,
    [OverallRiskIndexCode]                 INT          NULL,
    [OverallAssessedStatusID]              INT          NULL,
    [AlertSeverityID]                      INT          NULL,
    [DueTime]                              DATETIME     NULL,
    [DueTimeStatusID]                      INT          NULL,
    [DueTimeCreatedBySourceUniqueID]       INT          NULL,
    [LastModifiedTime]                     DATETIME     NULL
);




﻿CREATE TABLE [ETL].[TImportNeonatalBeddayDiagnosis] (
    [EpisodeID]     UNIQUEIDENTIFIER NOT NULL,
    [CareDate]      DATE             NULL,
    [DiagnosisCode] INT              NOT NULL,
    [ModifiedTime]  DATETIME         NOT NULL
);


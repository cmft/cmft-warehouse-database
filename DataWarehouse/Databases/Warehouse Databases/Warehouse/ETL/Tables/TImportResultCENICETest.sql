﻿CREATE TABLE [ETL].[TImportResultCENICETest] (
    [SourceUniqueID]      INT          NOT NULL,
    [OrderSourceUniqueID] INT          NOT NULL,
    [OrderRequestTime]    DATETIME     NULL,
    [OrderStatusCode]     VARCHAR (10) NULL,
    [PatientID]           INT          NULL,
    [PatientIdentifier]   VARCHAR (50) NULL,
    [DistrictNo]          VARCHAR (8)  NULL,
    [CasenoteNumber]      VARCHAR (20) NULL,
    [NHSNumber]           VARCHAR (10) NULL,
    [ClinicianID]         INT          NULL,
    [MainSpecialtyCode]   INT          NULL,
    [ProviderID]          INT          NOT NULL,
    [LocationID]          INT          NOT NULL,
    [TestStatus]          VARCHAR (32) NOT NULL,
    [TestID]              INT          NOT NULL
);




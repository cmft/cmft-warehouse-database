﻿CREATE TABLE [ETL].[TImportPASPatientSpecialRegister] (
    [SourcePatientNo]     INT           NOT NULL,
    [SpecialRegisterCode] VARCHAR (4)   NOT NULL,
    [DistrictNo]          VARCHAR (20)  NULL,
    [NHSNumber]           VARCHAR (17)  NULL,
    [EnteredDate]         DATETIME      NULL,
    [ActivityTime]        DATETIME      NULL,
    [SourceEpisodeNo]     VARCHAR (20)  NULL,
    [Created]             DATETIME      CONSTRAINT [df_ETL_TImportPASPatientSpecialRegister_CreatedTime] DEFAULT (getdate()) NULL,
    [ByWhom]              VARCHAR (255) CONSTRAINT [df_ETL_TImportPASPatientSpecialRegister_CreatedByWhom] DEFAULT (suser_name()) NULL
);


﻿CREATE TABLE [ETL].[TImportResultCENICEResult] (
    [SourceUniqueID]           INT            NOT NULL,
    [PatientID]                INT            NULL,
    [PatientIdentifier]        VARCHAR (50)   NULL,
    [DistrictNo]               VARCHAR (8)    NULL,
    [CasenoteNumber]           VARCHAR (20)   NULL,
    [NHSNumber]                VARCHAR (10)   NULL,
    [DateOfBirth]              DATE           NULL,
    [SexCode]                  INT            NULL,
    [ClinicianID]              INT            NULL,
    [MainSpecialtyCode]        INT            NULL,
    [SpecialtyCode]            INT            NULL,
    [LocationID]               INT            NOT NULL,
    [ReportSourceUniqueID]     VARCHAR (35)   NOT NULL,
    [ReportStatusCode]         VARCHAR (3)    NULL,
    [ReportTime]               DATETIME       NULL,
    [ReportComment]            VARCHAR (MAX)  NULL,
    [SampleReferenceCode]      VARCHAR (50)   NULL,
    [SampleTypeCode]           VARCHAR (8)    NULL,
    [SampleType]               VARCHAR (MAX)  NULL,
    [SampleCollectionTime]     DATETIME       NULL,
    [InvestigationCode]        VARCHAR (10)   NULL,
    [InvestigationName]        VARCHAR (100)  NULL,
    [InvestigationComment]     VARCHAR (MAX)  NULL,
    [ResultCode]               VARCHAR (10)   NULL,
    [ResultName]               VARCHAR (100)  NULL,
    [Result]                   VARCHAR (70)   NULL,
    [UnitOfMeasurement]        VARCHAR (35)   NULL,
    [Abnormal]                 BIT            NOT NULL,
    [LowerReferenceRangeValue] VARCHAR (35)   NULL,
    [UpperReferenceRangeValue] VARCHAR (35)   NULL,
    [ResultComment]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__TImportR__613D576F3BB86251] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);




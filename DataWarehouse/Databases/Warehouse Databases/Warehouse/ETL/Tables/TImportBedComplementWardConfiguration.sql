﻿CREATE TABLE [ETL].[TImportBedComplementWardConfiguration] (
    [WardID]         INT           NOT NULL,
    [WardType]       VARCHAR (255) NULL,
    [Schedule]       VARCHAR (255) NULL,
    [Division]       VARCHAR (255) NULL,
    [SpecialtyGroup] VARCHAR (255) NULL,
    [Comment]        VARCHAR (255) NULL,
    [StartDate]      DATE          NOT NULL,
    [EndDate]        DATE          NULL
);


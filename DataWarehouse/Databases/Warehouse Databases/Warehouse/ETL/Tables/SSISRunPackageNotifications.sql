﻿CREATE TABLE [ETL].[SSISRunPackageNotifications] (
    [SSISRunPackageNotificationId] INT           IDENTITY (1, 1) NOT NULL,
    [CreatedDate]                  DATETIME      NULL,
    [PackageName]                  VARCHAR (30)  NULL,
    [NotificationType]             VARCHAR (50)  NULL,
    [NotificationMessage]          VARCHAR (MAX) NULL,
    [ImportFileName]               VARCHAR (100) NULL,
    CONSTRAINT [PK_SSISRunPackageNotifications] PRIMARY KEY CLUSTERED ([SSISRunPackageNotificationId] ASC)
);


﻿CREATE TABLE [ETL].[TImportDM01SleepActivity] (
    [Surname]                           VARCHAR (MAX) NULL,
    [First Name]                        VARCHAR (MAX) NULL,
    [Hospital No.]                      VARCHAR (MAX) NULL,
    [Consultant]                        VARCHAR (MAX) NULL,
    [Attended/dna]                      VARCHAR (MAX) NULL,
    [Date rec.]                         VARCHAR (MAX) NULL,
    [Appointment]                       VARCHAR (MAX) NULL,
    [SS completed]                      VARCHAR (MAX) NULL,
    [SS Normal]                         VARCHAR (MAX) NULL,
    [CPAP/NIV]                          VARCHAR (MAX) NULL,
    [further invests]                   VARCHAR (MAX) NULL,
    [Date results taken to ENT by hand] VARCHAR (MAX) NULL
);


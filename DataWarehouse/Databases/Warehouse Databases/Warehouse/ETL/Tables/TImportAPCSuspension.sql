﻿CREATE TABLE [ETL].[TImportAPCSuspension] (
    [SourcePatientNo]      VARCHAR (9)  NOT NULL,
    [SourceEncounterNo]    VARCHAR (9)  NOT NULL,
    [SuspensionStartDate]  VARCHAR (12) NULL,
    [SuspensionEndDate]    VARCHAR (12) NULL,
    [SuspensionReasonCode] VARCHAR (4)  NULL,
    [SuspensionReason]     VARCHAR (30) NULL
);


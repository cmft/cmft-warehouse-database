﻿CREATE TABLE [ETL].[TImportPASCasenote] (
    [Allocated]             VARCHAR (12) NULL,
    [Comment]               VARCHAR (50) NULL,
    [InternalPatientNumber] VARCHAR (9)  NOT NULL,
    [Location]              VARCHAR (4)  NULL,
    [PtCasenoteIdPhyskey]   VARCHAR (14) NOT NULL,
    [PtCsNtLocDat]          VARCHAR (12) NULL,
    [Status]                VARCHAR (10) NULL,
    [Withdrawn]             VARCHAR (20) NULL
);


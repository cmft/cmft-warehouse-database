﻿CREATE TABLE [ETL].[TImportPEXSMSResponse] (
    [ResponseID]       BIGINT        NOT NULL,
    [DischargeID]      INT           NOT NULL,
    [DischargeDate]    DATE          NULL,
    [DischargeTime]    DATETIME      NULL,
    [LocationID]       INT           NULL,
    [SurveyDate]       DATE          NULL,
    [SurveyTime]       DATETIME      NULL,
    [SurveyID]         INT           NOT NULL,
    [LastRating]       INT           NULL,
    [LastResponseTime] DATETIME      NULL,
    [Status]           VARCHAR (255) NOT NULL,
    [SentTime]         DATETIME      NULL,
    [QuestionID]       INT           NULL,
    [AnswerID]         BIGINT        NULL,
    [Answer]           VARCHAR (MAX) NULL,
    [ResponseDate]     DATE          NULL,
    [ResponseTime]     DATETIME      NULL,
    [QuestionTypeID]   INT           NULL,
    [ChannelID]        INT           NULL,
    [LocationTypeID]   INT           NULL
);


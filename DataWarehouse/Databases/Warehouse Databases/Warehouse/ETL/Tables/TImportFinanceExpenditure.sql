﻿CREATE TABLE [ETL].[TImportFinanceExpenditure] (
    [CensusDate]   DATE          NULL,
    [Division]     VARCHAR (100) NULL,
    [Budget]       FLOAT (53)    NULL,
    [AnnualBudget] FLOAT (53)    NULL,
    [Actual]       FLOAT (53)    NULL
);


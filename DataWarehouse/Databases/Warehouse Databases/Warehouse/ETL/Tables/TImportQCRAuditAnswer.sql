﻿CREATE TABLE [ETL].[TImportQCRAuditAnswer] (
    [SourceUniqueID] INT           NULL,
    [AuditTime]      SMALLDATETIME NOT NULL,
    [LocationCode]   INT           NULL,
    [AuditTypeCode]  INT           NULL,
    [QuestionCode]   INT           NULL,
    [Answer]         INT           NULL
);


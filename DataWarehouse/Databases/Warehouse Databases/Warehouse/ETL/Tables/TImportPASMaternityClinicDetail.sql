﻿CREATE TABLE [ETL].[TImportPASMaternityClinicDetail] (
    [MATCLINICDETSID]               VARCHAR (50)  NULL,
    [EpisodeNumber]                 VARCHAR (255) NULL,
    [InternalPatientNumber]         VARCHAR (255) NULL,
    [IsThePatientKnownToBeImmune]   VARCHAR (3)   NULL,
    [KeMatRubellaImmInt]            VARCHAR (1)   NULL,
    [LeadProfessionalType]          VARCHAR (12)  NULL,
    [MaternalHeight]                VARCHAR (3)   NULL,
    [NoOfNeonatalDeaths]            VARCHAR (2)   NULL,
    [NoOfNoninducedAbortions]       VARCHAR (2)   NULL,
    [NoOfPreviousCaesareanSections] VARCHAR (2)   NULL,
    [NoOfPreviousInducedAbortions]  VARCHAR (2)   NULL,
    [NoOfRegistrableLiveBirths]     VARCHAR (2)   NULL,
    [NoOfRegistrableStillBirths]    VARCHAR (2)   NULL,
    [PatientImmunised]              VARCHAR (3)   NULL,
    [PreviousTransfusion]           VARCHAR (3)   NULL,
    [WasAntibodyStatusTested]       VARCHAR (3)   NULL,
    [WasTheResultPositive]          VARCHAR (3)   NULL
);


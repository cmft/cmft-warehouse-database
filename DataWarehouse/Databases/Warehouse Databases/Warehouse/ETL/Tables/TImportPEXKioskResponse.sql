﻿CREATE TABLE [ETL].[TImportPEXKioskResponse] (
    [SurveyTakenID]  INT           NOT NULL,
    [SurveyID]       INT           NOT NULL,
    [QuestionID]     INT           NOT NULL,
    [AnswerID]       BIGINT        NOT NULL,
    [Answer]         VARCHAR (MAX) NULL,
    [SurveyDate]     DATE          NOT NULL,
    [SurveyTime]     DATETIME      NOT NULL,
    [LocationID]     BIGINT        NULL,
    [DeviceNumber]   INT           NULL,
    [ChannelID]      INT           NULL,
    [QuestionTypeID] INT           NULL,
    [LocationTypeID] INT           NULL
);


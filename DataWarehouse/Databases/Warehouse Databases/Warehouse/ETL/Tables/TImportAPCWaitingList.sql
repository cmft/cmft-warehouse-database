﻿CREATE TABLE [ETL].[TImportAPCWaitingList] (
    [SourcePatientNo]              VARCHAR (9)    NOT NULL,
    [SourceEncounterNo]            VARCHAR (13)   NOT NULL,
    [CensusDate]                   SMALLDATETIME  NOT NULL,
    [PatientSurname]               VARCHAR (30)   NULL,
    [PatientForename]              VARCHAR (20)   NULL,
    [PatientTitle]                 VARCHAR (10)   NULL,
    [SexCode]                      CHAR (1)       NULL,
    [DateOfBirth]                  DATETIME2 (7)  NULL,
    [DateOfDeath]                  DATETIME2 (7)  NULL,
    [NHSNumber]                    VARCHAR (8000) NULL,
    [DistrictNo]                   VARCHAR (16)   NULL,
    [MaritalStatusCode]            CHAR (1)       NULL,
    [ReligionCode]                 VARCHAR (4)    NULL,
    [Postcode]                     VARCHAR (8)    NULL,
    [PatientsAddress1]             VARCHAR (25)   NULL,
    [PatientsAddress2]             VARCHAR (25)   NULL,
    [PatientsAddress3]             VARCHAR (25)   NULL,
    [PatientsAddress4]             VARCHAR (25)   NULL,
    [DHACode]                      VARCHAR (3)    NULL,
    [HomePhone]                    VARCHAR (23)   NULL,
    [WorkPhone]                    VARCHAR (23)   NULL,
    [EthnicOriginCode]             VARCHAR (4)    NULL,
    [ConsultantCode]               VARCHAR (6)    NULL,
    [SpecialtyCode]                VARCHAR (6)    NULL,
    [PASSpecialtyCode]             VARCHAR (6)    NULL,
    [EpisodeSpecialtyCode]         VARCHAR (6)    NULL,
    [ManagementIntentionCode]      CHAR (1)       NULL,
    [AdmissionMethodCode]          VARCHAR (2)    NULL,
    [PriorityCode]                 VARCHAR (2)    NULL,
    [WaitingListCode]              VARCHAR (6)    NOT NULL,
    [CommentClinical]              INT            NULL,
    [CommentNonClinical]           INT            NULL,
    [IntendedPrimaryOperationCode] VARCHAR (8)    NULL,
    [Operation]                    VARCHAR (121)  NULL,
    [SiteCode]                     VARCHAR (4)    NOT NULL,
    [WardCode]                     VARCHAR (8)    NULL,
    [WLStatus]                     VARCHAR (20)   NOT NULL,
    [ProviderCode]                 VARCHAR (8)    NULL,
    [PurchaserCode]                VARCHAR (8)    NULL,
    [ContractSerialNumber]         VARCHAR (6)    NULL,
    [CancelledBy]                  VARCHAR (1)    NULL,
    [BookingTypeCode]              VARCHAR (4)    NULL,
    [CasenoteNumber]               INT            NULL,
    [OriginalDateOnWaitingList]    DATETIME2 (7)  NULL,
    [DateOnWaitingList]            DATETIME2 (7)  NULL,
    [TCIDate]                      DATETIME2 (7)  NULL,
    [KornerWait]                   INT            NULL,
    [CountOfDaysSuspended]         INT            NULL,
    [SuspensionStartDate]          DATETIME2 (7)  NULL,
    [SuspensionEndDate]            DATETIME2 (7)  NULL,
    [SuspensionReasonCode]         INT            NULL,
    [SuspensionReason]             VARCHAR (30)   NULL,
    [InterfaceCode]                VARCHAR (4)    NOT NULL,
    [EpisodicGpCode]               VARCHAR (8)    NULL,
    [EpisodicGpPracticeCode]       VARCHAR (8)    NULL,
    [RegisteredGpCode]             VARCHAR (8)    NULL,
    [RegisteredGpPracticeCode]     VARCHAR (8)    NULL,
    [SourceTreatmentFunctionCode]  INT            NULL,
    [TreatmentFunctionCode]        INT            NULL,
    [NationalSpecialtyCode]        INT            NULL,
    [PCTCode]                      INT            NULL,
    [BreachDate]                   INT            NULL,
    [ExpectedAdmissionDate]        DATETIME2 (7)  NULL,
    [ReferralDate]                 INT            NULL,
    [FuturePatientCancelDate]      DATETIME2 (7)  NULL,
    [AdditionFlag]                 INT            NULL,
    [LocalRegisteredGpCode]        VARCHAR (8)    NULL,
    [LocalEpisodicGpCode]          VARCHAR (8)    NULL,
    [NextOfKinName]                VARCHAR (40)   NULL,
    [NextOfKinRelationship]        INT            NULL,
    [NextOfKinHomePhone]           INT            NULL,
    [NextOfKinWorkPhone]           INT            NULL,
    [ExpectedLOS]                  INT            NULL,
    [MRSA]                         INT            NULL,
    [RTTPathwayID]                 VARCHAR (25)   NULL,
    [RTTPathwayCondition]          VARCHAR (20)   NULL,
    [RTTStartDate]                 DATETIME2 (7)  NULL,
    [RTTEndDate]                   DATETIME2 (7)  NULL,
    [RTTSpecialtyCode]             VARCHAR (8)    NULL,
    [RTTCurrentProviderCode]       VARCHAR (10)   NULL,
    [RTTCurrentStatusCode]         VARCHAR (4)    NULL,
    [RTTCurrentStatusDate]         DATETIME2 (7)  NULL,
    [RTTCurrentPrivatePatientFlag] VARCHAR (1)    NULL,
    [RTTOverseasStatusFlag]        VARCHAR (1)    NULL,
    [AddedToWaitingListTime]       DATETIME2 (7)  NULL,
    [SourceUniqueID]               VARCHAR (24)   NOT NULL,
    [AdminCategoryCode]            VARCHAR (3)    NULL,
    [PatientDeathIndicator]        INT            NOT NULL,
    [CurrentAdmissionDays]         INT            NULL,
    [LastReviewDate]               DATETIME2 (7)  NULL,
    [KornerCharterWaitMonths]      INT            NULL,
    [KornerWaitMonths]             INT            NULL,
    [DiagnosticGroupCode]          VARCHAR (8)    NULL,
    [IntendedProcedureDate]        DATETIME2 (7)  NULL,
    [ShortNotice]                  INT            NOT NULL,
    [CEAEpisode]                   VARCHAR (9)    NULL
);


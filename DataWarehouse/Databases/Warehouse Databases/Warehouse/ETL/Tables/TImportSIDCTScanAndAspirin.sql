﻿CREATE TABLE [ETL].[TImportSIDCTScanAndAspirin] (
    [CandidateDataID]                                        INT           NOT NULL,
    [CandidateID]                                            INT           NOT NULL,
    [SectionID]                                              INT           NOT NULL,
    [ActionID]                                               INT           NOT NULL,
    [DidCTScanShowAHaemorrhage]                              VARCHAR (MAX) NULL,
    [PatientHaveASuspectedHeadInjury]                        VARCHAR (MAX) NULL,
    [oPatientHaveASuspectedSubarachnoidHaemorrhage]          VARCHAR (MAX) NULL,
    [PatientHaveAnAllergyToAspirin]                          VARCHAR (MAX) NULL,
    [HasAspirinAlreadyBeenGiven]                             VARCHAR (MAX) NULL,
    [DateTimeHasAspirinAlreadyBeenGiven]                     DATETIME      NULL,
    [NameOfAdministering]                                    VARCHAR (MAX) NULL,
    [DateTimeAspirinAdministered]                            DATETIME      NULL,
    [PrescriberContactedToWriteUpAsRegularDailyDose]         VARCHAR (MAX) NULL,
    [DateTimePrescriberContactedToWriteUpAsRegularDailyDose] DATETIME      NULL,
    [Username]                                               VARCHAR (MAX) NULL,
    [ModifiedDate]                                           DATETIME      NULL,
    [IsSectionComplete]                                      VARCHAR (MAX) NULL,
    [SectionCompleteTime]                                    DATETIME      NULL
);


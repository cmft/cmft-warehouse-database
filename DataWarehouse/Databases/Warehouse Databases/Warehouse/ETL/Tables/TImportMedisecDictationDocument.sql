﻿CREATE TABLE [ETL].[TImportMedisecDictationDocument] (
    [SourceUniqueID]              INT           NOT NULL,
    [SourcePatientNo]             VARCHAR (20)  NOT NULL,
    [DocumentID]                  VARCHAR (50)  NULL,
    [DocumentDescription]         VARCHAR (MAX) NULL,
    [DocumentTypeCode]            VARCHAR (5)   NULL,
    [AuthorCode]                  VARCHAR (50)  NULL,
    [TypedTime]                   DATETIME      NULL,
    [DictatedTime]                DATETIME      NULL,
    [DictatedByCode]              VARCHAR (50)  NULL,
    [IssuedTime]                  DATETIME      NULL,
    [IssuedByCode]                VARCHAR (50)  NULL,
    [SignedTime]                  DATETIME      NULL,
    [SignedByCode]                VARCHAR (255) NULL,
    [SignedStatusCode]            VARCHAR (5)   NULL,
    [DepartmentCode]              VARCHAR (50)  NULL,
    [DocumentXMLTemplateCode]     VARCHAR (50)  NULL,
    [DocumentXML]                 XML           NULL,
    [TransmissionTime]            DATETIME      NULL,
    [TransmissionDestinationCode] VARCHAR (10)  NULL
);


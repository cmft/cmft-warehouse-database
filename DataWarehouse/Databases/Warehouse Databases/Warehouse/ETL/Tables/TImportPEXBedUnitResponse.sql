﻿CREATE TABLE [ETL].[TImportPEXBedUnitResponse] (
    [SurveyTakenID]  BIGINT        NOT NULL,
    [SurveyDate]     DATE          NOT NULL,
    [SurveyTime]     DATETIME      NOT NULL,
    [SurveyID]       INT           NOT NULL,
    [LocationID]     INT           NULL,
    [QuestionID]     INT           NOT NULL,
    [QuestionNumber] INT           NOT NULL,
    [AnswerID]       BIGINT        NOT NULL,
    [Answer]         VARCHAR (MAX) NULL,
    [OptionNo]       INT           NOT NULL,
    [ResponseDate]   DATE          NOT NULL,
    [ResponseTime]   DATETIME      NULL,
    [ChannelID]      INT           NULL,
    [QuestionTypeID] INT           NULL,
    [LocationTypeID] INT           NULL
);


﻿CREATE TABLE [ETL].[TImportAPCWardStay] (
    [SourceUniqueID]    VARCHAR (50) NOT NULL,
    [SourceSpellNo]     VARCHAR (20) NOT NULL,
    [SourcePatientNo]   VARCHAR (20) NOT NULL,
    [StartDate]         VARCHAR (19) NULL,
    [EndDate]           VARCHAR (19) NULL,
    [StartTime]         VARCHAR (19) NULL,
    [EndTime]           VARCHAR (19) NULL,
    [SiteCode]          VARCHAR (5)  NULL,
    [WardCode]          VARCHAR (10) NULL,
    [StartActivityCode] VARCHAR (10) NULL,
    [EndActivityCode]   VARCHAR (10) NULL,
    [InterfaceCode]     VARCHAR (20) NULL
);




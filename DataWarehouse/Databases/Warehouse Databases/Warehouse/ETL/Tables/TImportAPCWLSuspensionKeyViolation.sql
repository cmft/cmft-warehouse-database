﻿CREATE TABLE [ETL].[TImportAPCWLSuspensionKeyViolation] (
    [EncounterRecno]       INT           IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]      VARCHAR (9)   NOT NULL,
    [SourceEncounterNo]    VARCHAR (9)   NOT NULL,
    [CensusDate]           SMALLDATETIME NULL,
    [SuspensionStartDate]  VARCHAR (12)  NULL,
    [SuspensionEndDate]    VARCHAR (10)  NULL,
    [SuspensionReasonCode] VARCHAR (4)   NULL,
    [SuspensionReason]     VARCHAR (30)  NULL,
    [SourceUniqueID]       VARCHAR (255) NULL
);


﻿CREATE TABLE [ETL].[TImportNeonatalBeddayProcedure] (
    [EpisodeID]     UNIQUEIDENTIFIER NOT NULL,
    [CareDate]      DATE             NULL,
    [ProcedureCode] INT              NOT NULL,
    [ModifiedTime]  DATETIME         NOT NULL
);


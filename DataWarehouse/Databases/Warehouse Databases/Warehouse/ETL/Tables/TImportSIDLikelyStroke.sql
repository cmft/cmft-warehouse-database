﻿CREATE TABLE [ETL].[TImportSIDLikelyStroke] (
    [CandidateDataID]                                                     INT           NOT NULL,
    [CandidateID]                                                         INT           NOT NULL,
    [SectionID]                                                           INT           NOT NULL,
    [ActionID]                                                            INT           NOT NULL,
    [ClinicalDiagnosis]                                                   VARCHAR (MAX) NULL,
    [Plan]                                                                VARCHAR (MAX) NULL,
    [SuitableForThrombolysis]                                             VARCHAR (MAX) NULL,
    [SuitableForThrombolysisReasonsWhyNo]                                 VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOHaemorrhagicStroke]                   VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOAge]                                  VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOArrivedOutsideThrombolysisTimeWindow] VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOSymptomsImproving]                    VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOCoMorbidity]                          VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOStrokeTooMildOrTooSevere]             VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOContraindicatedMedication]            VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOSymptomOnsetTimeUnknownWakeUpStroke]  VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOPatientOrRelativeRefusal]             VARCHAR (MAX) NULL,
    [SuitableForThrombolysisNoButDTOOtherMedicalReason]                   VARCHAR (MAX) NULL,
    [RequestForUrgentCTScan]                                              VARCHAR (MAX) NULL,
    [DateTimeRequestForUrgentCTScan]                                      DATETIME      NULL,
    [DateTimeCTScanPerformed]                                             DATETIME      NULL,
    [ResultsOfScan]                                                       VARCHAR (MAX) NULL,
    [CTScanNAReasonyWhy]                                                  VARCHAR (MAX) NULL,
    [Aspirin300mgAdministered]                                            VARCHAR (MAX) NULL,
    [DateTimeAspirin300mgAdministered]                                    DATETIME      NULL,
    [DateTimeAspirin300mgAssessed]                                        DATETIME      NULL,
    [AspirinNotes]                                                        VARCHAR (MAX) NULL,
    [SuitableForDirectAdmission]                                          VARCHAR (MAX) NULL,
    [ObservationsReviewed]                                                VARCHAR (MAX) NULL,
    [DateTimeObservationsReviewed]                                        DATETIME      NULL,
    [BloodGlucoseRecorded]                                                VARCHAR (MAX) NULL,
    [SwallowAssessmentCompleted]                                          VARCHAR (MAX) NULL,
    [IfNotWithin4HoursWhyNot]                                             VARCHAR (MAX) NULL,
    [DateTimeSwallowAssessmentCompleted]                                  DATETIME      NULL,
    [SwallowScreenResults]                                                VARCHAR (MAX) NULL,
    [SwallowAssessmentCompletedNAReasonyWhy]                              VARCHAR (MAX) NULL,
    [DutyTherapistNotified]                                               VARCHAR (MAX) NULL,
    [DateTimeDutyTherapistNotified]                                       DATETIME      NULL,
    [StrokePhysicianNotified]                                             VARCHAR (MAX) NULL,
    [DateTimeStrokePhysicianNotified]                                     DATETIME      NULL,
    [WillAspirinNeedAdminsteringOnStrokeUnit]                             VARCHAR (MAX) NULL,
    [Username]                                                            VARCHAR (MAX) NULL,
    [ModifiedDate]                                                        DATETIME      NULL,
    [IsSectionComplete]                                                   VARCHAR (MAX) NULL,
    [SectionCompleteTime]                                                 DATETIME      NULL
);


﻿CREATE TABLE [ETL].[TImportNeonatalBeddayManagement] (
    [EpisodeID]      UNIQUEIDENTIFIER NOT NULL,
    [CareDate]       DATE             NULL,
    [ManagementCode] INT              NOT NULL,
    [ModifiedTime]   DATETIME         NOT NULL
);


﻿CREATE TABLE [ETL].[TImportBedManEvent] (
    [SourceUniqueID]  INT           NOT NULL,
    [SourceSpellNo]   VARCHAR (100) NULL,
    [EventDate]       DATETIME      NULL,
    [EventTypeCode]   VARCHAR (20)  NULL,
    [SourcePatientNo] VARCHAR (8)   NULL,
    [AdmissionTime]   DATETIME      NULL,
    [DischargeTime]   DATETIME      NULL,
    [EventDetails]    XML           NULL
);


﻿CREATE TABLE [ETL].[TImportResultCENICEOrderResult] (
    [OrderSourceUniqueID]  INT      NOT NULL,
    [OrderRequestTime]     DATETIME NULL,
    [ResultSourceUniqueID] INT      NOT NULL
);


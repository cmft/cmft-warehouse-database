﻿CREATE TABLE [ETL].[TImportResultCWSNumericResult] (
    [SourceUniqueID]      VARCHAR (50)  NULL,
    [SourcePatientNo]     INT           NULL,
    [SourceEpisodeNo]     INT           NULL,
    [TransactionNo]       INT           NOT NULL,
    [SessionNo]           INT           NOT NULL,
    [OrderSourceUniqueID] VARCHAR (50)  NULL,
    [ConsultantCode]      VARCHAR (10)  NULL,
    [SpecialtyCode]       VARCHAR (8)   NULL,
    [OrderPriorityCode]   VARCHAR (20)  NULL,
    [OrderStatusCode]     TINYINT       NULL,
    [OrderEnteredByCode]  VARCHAR (4)   NULL,
    [OrderedByCode]       VARCHAR (20)  NULL,
    [OrderTime]           VARCHAR (39)  NULL,
    [OrderComment]        VARCHAR (MAX) NULL,
    [SampleReferenceCode] VARCHAR (50)  NULL,
    [EffectiveTime]       DATETIME      NULL,
    [LocationCode]        VARCHAR (4)   NULL,
    [ServiceCode]         VARCHAR (20)  NULL,
    [ResultCode]          VARCHAR (4)   NULL,
    [Result]              VARCHAR (9)   NULL,
    [ResultTime]          DATETIME      NULL,
    [RangeIndicator]      SMALLINT      NULL,
    [ResultEnteredByCode] VARCHAR (4)   NULL,
    [ResultStatusCode]    SMALLINT      NULL,
    [ResultComment]       VARCHAR (MAX) NULL
);


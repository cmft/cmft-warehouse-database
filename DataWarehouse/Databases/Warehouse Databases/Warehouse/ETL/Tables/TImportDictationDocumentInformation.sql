﻿CREATE TABLE [ETL].[TImportDictationDocumentInformation] (
    [SourceUniqueID]           INT        NOT NULL,
    [Running]                  DATETIME   NULL,
    [DictationTime]            FLOAT (53) NULL,
    [CorrectionTime]           FLOAT (53) NULL,
    [PassThroughTime]          FLOAT (53) NULL,
    [SmSoundQualityAcceptable] BIT        NULL,
    [SmClippingRatio]          FLOAT (53) NULL,
    [SmSignalLevel]            INT        NULL,
    [SmSignalToNoiseRatio]     INT        NULL
);


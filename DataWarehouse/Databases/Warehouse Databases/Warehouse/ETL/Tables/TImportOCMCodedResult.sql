﻿CREATE TABLE [ETL].[TImportOCMCodedResult] (
    [SourceUniqueID]  VARCHAR (50)  NOT NULL,
    [SourcePatientNo] INT           NOT NULL,
    [SourceEpisodeNo] INT           NOT NULL,
    [SequenceNo]      INT           NOT NULL,
    [TransactionNo]   INT           NOT NULL,
    [SessionNo]       INT           NOT NULL,
    [ResultCode]      VARCHAR (4)   NOT NULL,
    [Result]          VARCHAR (9)   NULL,
    [ResultDate]      DATE          NULL,
    [ResultTime]      SMALLDATETIME NULL,
    [RangeIndicator]  SMALLINT      NULL,
    [EnteredBy]       VARCHAR (4)   NULL,
    [StatusCode]      SMALLINT      NULL,
    [Comment1]        VARCHAR (60)  NULL,
    [Comment2]        VARCHAR (60)  NULL,
    [Comment3]        VARCHAR (60)  NULL,
    [Comment4]        VARCHAR (60)  NULL,
    [Comment5]        VARCHAR (60)  NULL,
    [Comment6]        VARCHAR (60)  NULL
);


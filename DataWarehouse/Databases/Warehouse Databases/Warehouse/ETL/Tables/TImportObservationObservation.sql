﻿CREATE TABLE [ETL].[TImportObservationObservation] (
    [SourceUniqueID]               INT        NOT NULL,
    [ObservationSetSourceUniqueID] INT        NOT NULL,
    [MeasureID]                    INT        NOT NULL,
    [NumericResult]                FLOAT (53) NULL,
    [ListID]                       INT        NULL,
    [FlagCode]                     BIT        NULL,
    [CreatedByUserID]              INT        NOT NULL,
    [CreatedTime]                  DATETIME   NOT NULL,
    [ModifiedByUserID]             INT        NOT NULL,
    [LastModifiedTime]             DATETIME   NOT NULL,
    CONSTRAINT [PK_TImportObservationObservation] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


﻿CREATE TABLE [ETL].[TImportDM01RadiologyActivity] (
    [Best Hosp No.]    VARCHAR (MAX) NULL,
    [Event Date]       VARCHAR (MAX) NULL,
    [Site]             VARCHAR (MAX) NULL,
    [Patient Type Des] VARCHAR (MAX) NULL,
    [Exam Name]        VARCHAR (MAX) NULL,
    [Referrer Name]    VARCHAR (MAX) NULL,
    [Name]             VARCHAR (MAX) NULL,
    [PCT]              VARCHAR (MAX) NULL,
    [Practice]         VARCHAR (MAX) NULL,
    [Modality]         VARCHAR (MAX) NULL,
    [NHS number]       VARCHAR (MAX) NULL,
    [No of Pr]         VARCHAR (MAX) NULL,
    [Planned]          VARCHAR (MAX) NULL,
    [Korner band]      VARCHAR (MAX) NULL,
    [Interventional]   VARCHAR (MAX) NULL,
    [Pstcde1]          VARCHAR (MAX) NULL
);


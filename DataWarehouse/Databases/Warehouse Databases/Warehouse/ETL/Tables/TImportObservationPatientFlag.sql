﻿CREATE TABLE [ETL].[TImportObservationPatientFlag] (
    [SourceUniqueID]          INT           NOT NULL,
    [CasenoteNumber]          VARCHAR (20)  NULL,
    [DateOfBirth]             DATE          NULL,
    [AdmissionSourceUniqueID] INT           NOT NULL,
    [PatientFlagID]           INT           NOT NULL,
    [StartTime]               DATETIME      NOT NULL,
    [EndTime]                 DATETIME      NULL,
    [Comment]                 VARCHAR (MAX) NULL,
    [CreatedByUserID]         INT           NOT NULL,
    [CreatedTime]             DATETIME      NOT NULL,
    [LastModifiedByUserID]    INT           NOT NULL,
    [LastModifiedTime]        DATETIME      NOT NULL,
    [EWSDisabled]             BIT           NOT NULL
);


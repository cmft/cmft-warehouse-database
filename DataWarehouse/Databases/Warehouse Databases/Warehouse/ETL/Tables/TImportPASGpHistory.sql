﻿CREATE TABLE [ETL].[TImportPASGpHistory] (
    [GPHISTORYID]                       VARCHAR (255) NULL,
    [DistrictNumber]                    VARCHAR (14)  NULL,
    [FromDate]                          VARCHAR (10)  NULL,
    [FromDtTmInt]                       VARCHAR (255) NULL,
    [GpAddrLine1]                       VARCHAR (255) NULL,
    [GpAddrLine2]                       VARCHAR (255) NULL,
    [GpAddrLine3]                       VARCHAR (255) NULL,
    [GpAddrLine4]                       VARCHAR (255) NULL,
    [GpCode]                            VARCHAR (8)   NULL,
    [GpfhCode]                          VARCHAR (5)   NULL,
    [Initials]                          VARCHAR (4)   NULL,
    [InternalPatientNumber]             VARCHAR (9)   NULL,
    [NationalCode]                      VARCHAR (7)   NULL,
    [Phone]                             VARCHAR (23)  NULL,
    [PmiPatientGpHistoryToDateIndexKey] VARCHAR (8)   NULL,
    [Postcode]                          VARCHAR (10)  NULL,
    [Practice]                          VARCHAR (6)   NULL,
    [SeqNo]                             VARCHAR (3)   NULL,
    [Surname]                           VARCHAR (24)  NULL,
    [Title]                             VARCHAR (4)   NULL,
    [ToDate]                            VARCHAR (12)  NULL
);


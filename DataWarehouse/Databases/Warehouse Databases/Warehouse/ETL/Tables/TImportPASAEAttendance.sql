﻿CREATE TABLE [ETL].[TImportPASAEAttendance] (
    [AEATTENDANCEID]              VARCHAR (255) NULL,
    [AeAttendanceDatetimeneutral] VARCHAR (12)  NULL,
    [DepartmentCode]              VARCHAR (8)   NULL,
    [InternalPatientNumber]       VARCHAR (9)   NULL,
    [EpisodeNumber]               VARCHAR (9)   NULL,
    [HospitalCode]                VARCHAR (4)   NULL,
    [Created]                     DATETIME2 (0) CONSTRAINT [DF_ETL_TImportPASAEAttendance_Created] DEFAULT (getdate()) NULL
);


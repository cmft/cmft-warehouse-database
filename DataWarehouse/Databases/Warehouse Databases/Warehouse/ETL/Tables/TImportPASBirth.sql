﻿CREATE TABLE [ETL].[TImportPASBirth] (
    [BIRTHID]               VARCHAR (50) NULL,
    [BabyPatientNumber]     VARCHAR (9)  NULL,
    [BirthOrder]            VARCHAR (1)  NOT NULL,
    [BirthWeight]           VARCHAR (4)  NULL,
    [CaseNoteNumber]        VARCHAR (14) NULL,
    [Comments]              VARCHAR (30) NULL,
    [DeliveryDate]          VARCHAR (8)  NULL,
    [DeliveryTime]          VARCHAR (5)  NULL,
    [DrugsUsed]             VARCHAR (30) NULL,
    [EpisodeNumber]         VARCHAR (12) NOT NULL,
    [InternalPatientNumber] VARCHAR (12) NOT NULL,
    [KeMatDrugsUsed2]       VARCHAR (30) NULL,
    [KeMatDtimeDelivNeut]   VARCHAR (12) NULL,
    [KeMatLiveStillIndInt]  VARCHAR (1)  NULL,
    [KeMatMethDelivInt]     VARCHAR (2)  NULL,
    [KeMatMorDrugsInt]      VARCHAR (1)  NULL,
    [KeMatMorPpInt]         VARCHAR (1)  NULL,
    [KeMatPresFetusInt]     VARCHAR (1)  NULL,
    [SuspCongAnomaly]       VARCHAR (7)  NULL
);


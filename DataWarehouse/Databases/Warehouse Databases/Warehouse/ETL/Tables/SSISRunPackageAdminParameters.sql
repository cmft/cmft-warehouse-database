﻿CREATE TABLE [ETL].[SSISRunPackageAdminParameters] (
    [SSISRunPackageAdminParameterId] INT           NOT NULL,
    [SSISRunPackageAdminId]          INT           NULL,
    [ParameterVariableName]          VARCHAR (50)  NULL,
    [ParameterValue]                 VARCHAR (200) NULL
);




GO
GRANT VIEW DEFINITION
    ON OBJECT::[ETL].[SSISRunPackageAdminParameters] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[ETL].[SSISRunPackageAdminParameters] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[ETL].[SSISRunPackageAdminParameters] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[ETL].[SSISRunPackageAdminParameters] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT CONTROL
    ON OBJECT::[ETL].[SSISRunPackageAdminParameters] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT ALTER
    ON OBJECT::[ETL].[SSISRunPackageAdminParameters] TO [CMMC\malcom.hodson]
    AS [dbo];


﻿CREATE TABLE [ETL].[TImportResultGPICEOrderResult] (
    [OrderSourceUniqueID]  INT      NOT NULL,
    [OrderRequestTime]     DATETIME NULL,
    [ResultSourceUniqueID] INT      NOT NULL
);


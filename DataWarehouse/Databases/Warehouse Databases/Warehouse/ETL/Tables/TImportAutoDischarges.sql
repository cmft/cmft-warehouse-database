﻿CREATE TABLE [ETL].[TImportAutoDischarges] (
    [OpRegDtimeInt]         VARCHAR (12) NOT NULL,
    [DistrictNumber]        VARCHAR (14) NULL,
    [HospitalCode]          VARCHAR (4)  NULL,
    [Specialty]             VARCHAR (4)  NULL,
    [ConsCode]              VARCHAR (6)  NULL,
    [EpisodeNumber]         VARCHAR (9)  NOT NULL,
    [InternalPatientNumber] VARCHAR (9)  NOT NULL,
    [DischargeDatetime]     VARCHAR (16) NULL,
    [Reason]                VARCHAR (30) NULL,
    [ReasonCode]            VARCHAR (4)  NULL
);


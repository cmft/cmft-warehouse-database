﻿CREATE TABLE [ETL].[TImportPASPreviousAddress] (
    [PREVADDRESSID]         NVARCHAR (254) NULL,
    [DistrictNumber]        NVARCHAR (14)  NULL,
    [EffectiveFromDate]     NVARCHAR (10)  NULL,
    [EffectiveFromDateInt]  NVARCHAR (10)  NULL,
    [EffectiveToDate]       NVARCHAR (10)  NULL,
    [EffectiveToDateInt]    NVARCHAR (8)   NULL,
    [HaCode]                NVARCHAR (3)   NULL,
    [InternalPatientNumber] NVARCHAR (9)   NULL,
    [Postcode]              NVARCHAR (8)   NULL,
    [PseudoPostCode]        NVARCHAR (8)   NULL,
    [PtAddLn1]              NVARCHAR (20)  NULL,
    [PtAddLn2]              NVARCHAR (20)  NULL,
    [PtAddLn3]              NVARCHAR (20)  NULL,
    [PtAddLn4]              NVARCHAR (20)  NULL,
    [SeqNo]                 NVARCHAR (3)   NULL
);


﻿CREATE TABLE [ETL].[TImportMedisecDictationOutpatient] (
    [SourcePatientNo] VARCHAR (20) NOT NULL,
    [AppointmentTime] DATETIME     NULL,
    [DoctorCode]      VARCHAR (50) NULL
);


﻿CREATE TABLE [ETL].[TImportSIDQualityIndicators] (
    [CandidateDataID]                                                 INT           NOT NULL,
    [CandidateID]                                                     INT           NOT NULL,
    [SectionID]                                                       INT           NOT NULL,
    [ActionID]                                                        INT           NOT NULL,
    [DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival]       VARCHAR (MAX) NULL,
    [DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalReason] VARCHAR (MAX) NULL,
    [DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalNotes]  VARCHAR (MAX) NULL,
    [ScreenedForSwallowingDisordersWithin24HoursOfAttendance]         VARCHAR (MAX) NULL,
    [ScreenedForSwallowingDisordersWithin24HoursOfAttendanceReason]   VARCHAR (MAX) NULL,
    [ScreenedForSwallowingDisordersWithin24HoursOfAttendanceNotes]    VARCHAR (MAX) NULL,
    [BrainScanWithin24HoursOfAttendance]                              VARCHAR (MAX) NULL,
    [BrainScanWithin24HoursOfAttendanceReason]                        VARCHAR (MAX) NULL,
    [BrainScanWithin24HoursOfAttendanceNotes]                         VARCHAR (MAX) NULL,
    [AspirinWithin24HoursOfAttendance]                                VARCHAR (MAX) NULL,
    [AspirinWithin24HoursOfAttendanceReason]                          VARCHAR (MAX) NULL,
    [AspirinWithin24HoursOfAttendanceNotes]                           VARCHAR (MAX) NULL,
    [PhysiotherapyAssessmentWithinFirst24Hours]                       VARCHAR (MAX) NULL,
    [PhysiotherapyAssessmentWithinFirst24HoursReason]                 VARCHAR (MAX) NULL,
    [PhysiotherapyAssessmentWithinFirst24HoursNotes]                  VARCHAR (MAX) NULL,
    [AssessmentByAnOccupationalTherapistWithinFirst24Hours]           VARCHAR (MAX) NULL,
    [AssessmentByAnOccupationalTherapistWithinFirst24HoursReason]     VARCHAR (MAX) NULL,
    [AssessmentByAnOccupationalTherapistWithinFirst24HoursNotes]      VARCHAR (MAX) NULL,
    [WeighedAtLeastOnceDuringAttendance]                              VARCHAR (MAX) NULL,
    [WeighedAtLeastOnceDuringAttendanceReason]                        VARCHAR (MAX) NULL,
    [WeighedAtLeastOnceDuringAttendanceNotes]                         VARCHAR (MAX) NULL,
    [SeenByStrokePhysicianWithin24HoursOfAttendance]                  VARCHAR (MAX) NULL,
    [SeenByStrokePhysicianWithin24HoursOfAttendanceReason]            VARCHAR (MAX) NULL,
    [SeenByStrokePhysicianWithin24HoursOfAttendanceNotes]             VARCHAR (MAX) NULL,
    [CurrentLocation]                                                 VARCHAR (MAX) NULL,
    [Username]                                                        VARCHAR (MAX) NULL,
    [ModifiedDate]                                                    DATETIME      NULL
);


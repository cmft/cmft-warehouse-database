﻿CREATE TABLE [ETL].[SSISRunPackageAdmin] (
    [SSISRunPackageAdminId]      INT            NOT NULL,
    [PackageName]                NVARCHAR (50)  NOT NULL,
    [PackageLocation]            NVARCHAR (300) NOT NULL,
    [ImportFolderLocation]       NVARCHAR (300) NOT NULL,
    [ArchiveFolderLocation]      NVARCHAR (300) NOT NULL,
    [IsActive]                   BIT            NOT NULL,
    [NotificationMailAddresses]  NVARCHAR (500) NOT NULL,
    [ImportToServer]             VARCHAR (50)   NULL,
    [ImportToDatabase]           VARCHAR (50)   NULL,
    [ConnectionExtendedProperty] VARCHAR (100)  NULL
);




GO
GRANT VIEW DEFINITION
    ON OBJECT::[ETL].[SSISRunPackageAdmin] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[ETL].[SSISRunPackageAdmin] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[ETL].[SSISRunPackageAdmin] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[ETL].[SSISRunPackageAdmin] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT CONTROL
    ON OBJECT::[ETL].[SSISRunPackageAdmin] TO [CMMC\malcom.hodson]
    AS [dbo];


GO
GRANT ALTER
    ON OBJECT::[ETL].[SSISRunPackageAdmin] TO [CMMC\malcom.hodson]
    AS [dbo];


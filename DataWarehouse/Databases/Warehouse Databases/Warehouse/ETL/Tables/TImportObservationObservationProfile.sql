﻿CREATE TABLE [ETL].[TImportObservationObservationProfile] (
    [SourceUniqueID]          INT          NOT NULL,
    [CasenoteNumber]          VARCHAR (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateOfBirth]             DATETIME     NOT NULL,
    [SpecialtyID]             INT          NOT NULL,
    [LocationID]              INT          NOT NULL,
    [AdmissionSourceUniqueID] INT          NOT NULL,
    [ProfileID]               INT          NOT NULL,
    [TemplatePriorityID]      INT          NULL,
    [DrivingTableCode]        VARCHAR (5)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [DrivingTableID]          INT          NULL,
    [ProfileReasonID]         INT          NOT NULL,
    [StepNumber]              INT          NOT NULL,
    [IterationNumber]         INT          NOT NULL,
    [StartTime]               DATETIME     NOT NULL,
    [EndTime]                 DATETIME     NULL,
    [CreatedBy]               INT          NOT NULL,
    [CreatedTime]             DATETIME     NOT NULL,
    [LastModifiedBy]          INT          NOT NULL,
    [LastModifiedTime]        DATETIME     NOT NULL,
    [Active]                  BIT          NOT NULL
);




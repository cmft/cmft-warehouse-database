﻿CREATE TABLE [ETL].[TImportOPDiagnosis] (
    [SourceUniqueID]    VARCHAR (50) NULL,
    [SourceSpellNo]     VARCHAR (20) NULL,
    [SourcePatientNo]   VARCHAR (20) NULL,
    [SourceEncounterNo] VARCHAR (20) NULL,
    [SequenceNo]        VARCHAR (10) NULL,
    [DiagnosisCode]     VARCHAR (10) NULL,
    [OPSourceUniqueID]  VARCHAR (50) NULL
);


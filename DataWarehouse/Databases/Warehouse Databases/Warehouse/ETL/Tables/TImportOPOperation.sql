﻿CREATE TABLE [ETL].[TImportOPOperation] (
    [SourceUniqueID]    VARCHAR (50) NULL,
    [SourcePatientNo]   VARCHAR (20) NULL,
    [SourceEncounterNo] VARCHAR (20) NULL,
    [SequenceNo]        VARCHAR (10) NULL,
    [OperationCode]     VARCHAR (10) NULL,
    [OperationDate]     VARCHAR (19) NULL,
    [DoctorCode]        VARCHAR (10) NULL,
    [OPSourceUniqueID]  VARCHAR (50) NULL
);


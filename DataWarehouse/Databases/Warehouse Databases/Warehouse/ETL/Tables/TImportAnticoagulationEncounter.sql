﻿CREATE TABLE [ETL].[TImportAnticoagulationEncounter] (
    [SourceUniqueID]     INT          NOT NULL,
    [NHSNumber]          VARCHAR (20) NULL,
    [PatientSurname]     VARCHAR (50) NOT NULL,
    [PatientForename]    VARCHAR (50) NULL,
    [DateOfBirth]        DATE         NULL,
    [SexCode]            CHAR (1)     NULL,
    [Postcode]           VARCHAR (15) NULL,
    [ClinicID]           INT          NULL,
    [TestDate]           DATE         NULL,
    [Result]             FLOAT (53)   NOT NULL,
    [Dose]               FLOAT (53)   NOT NULL,
    [DiagnosisID]        INT          NOT NULL,
    [DiagnosisDate]      DATE         NULL,
    [CommissionerID]     INT          NULL,
    [GpPracticeCode]     VARCHAR (20) NULL,
    [TreatmentStartDate] DATE         NULL,
    CONSTRAINT [PkTImportDawnAnticoagulantEncounter] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


﻿CREATE TABLE [ETL].[TImportObservationAdmission] (
    [SourceUniqueID]         INT           NOT NULL,
    [CasenoteNumber]         VARCHAR (20)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [DateOfBirth]            DATE          NULL,
    [SexID]                  INT           NOT NULL,
    [Surname]                VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    [Forename]               VARCHAR (50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AdmissionTime]          DATETIME      NOT NULL,
    [StartSpecialtyID]       INT           NOT NULL,
    [StartLocationID]        INT           NOT NULL,
    [DischargeTime]          DATETIME      NULL,
    [EndSpecialtyID]         INT           NULL,
    [EndLocationID]          INT           NULL,
    [TreatmentOutcomeID]     INT           NOT NULL,
    [DischargeDestinationID] INT           NOT NULL,
    [Comment]                VARCHAR (MAX) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [CreatedByUserID]        INT           NOT NULL,
    [CreatedTime]            DATETIME      NOT NULL,
    [LastModifiedByUserID]   INT           NOT NULL,
    [LastModifiedTime]       DATETIME      NOT NULL
);






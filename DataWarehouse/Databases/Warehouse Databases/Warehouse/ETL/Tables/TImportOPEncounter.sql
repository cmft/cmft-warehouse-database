﻿CREATE TABLE [ETL].[TImportOPEncounter] (
    [SourceUniqueID]                VARCHAR (50)  NOT NULL,
    [SourcePatientNo]               VARCHAR (20)  NOT NULL,
    [SourceEncounterNo]             VARCHAR (20)  NOT NULL,
    [PatientTitle]                  VARCHAR (10)  NULL,
    [PatientForename]               VARCHAR (20)  NULL,
    [PatientSurname]                VARCHAR (30)  NULL,
    [DateOfBirth]                   VARCHAR (19)  NULL,
    [DateOfDeath]                   VARCHAR (19)  NULL,
    [SexCode]                       VARCHAR (1)   NULL,
    [NHSNumber]                     VARCHAR (17)  NULL,
    [DistrictNo]                    VARCHAR (12)  NULL,
    [Postcode]                      VARCHAR (8)   NULL,
    [PatientAddress1]               VARCHAR (25)  NULL,
    [PatientAddress2]               VARCHAR (25)  NULL,
    [PatientAddress3]               VARCHAR (25)  NULL,
    [PatientAddress4]               VARCHAR (25)  NULL,
    [DHACode]                       VARCHAR (3)   NULL,
    [EthnicOriginCode]              VARCHAR (4)   NULL,
    [MaritalStatusCode]             VARCHAR (1)   NULL,
    [ReligionCode]                  VARCHAR (4)   NULL,
    [RegisteredGpCode]              VARCHAR (8)   NULL,
    [RegisteredGpPracticeCode]      VARCHAR (8)   NULL,
    [SiteCode]                      VARCHAR (5)   NULL,
    [AppointmentDate]               VARCHAR (19)  NULL,
    [AppointmentTime]               VARCHAR (19)  NULL,
    [ClinicCode]                    VARCHAR (8)   NULL,
    [AdminCategoryCode]             VARCHAR (3)   NULL,
    [SourceOfReferralCode]          VARCHAR (5)   NULL,
    [ReasonForReferralCode]         VARCHAR (5)   NULL,
    [PriorityCode]                  VARCHAR (5)   NULL,
    [FirstAttendanceFlag]           VARCHAR (1)   NULL,
    [AppointmentStatusCode]         VARCHAR (10)  NULL,
    [CancelledByCode]               VARCHAR (5)   NULL,
    [TransportRequiredFlag]         VARCHAR (3)   NULL,
    [AppointmentTypeCode]           VARCHAR (5)   NULL,
    [DisposalCode]                  VARCHAR (8)   NULL,
    [ConsultantCode]                VARCHAR (10)  NULL,
    [SpecialtyCode]                 VARCHAR (5)   NULL,
    [ReferringConsultantCode]       VARCHAR (10)  NULL,
    [ReferringSpecialtyCode]        VARCHAR (8)   NULL,
    [BookingTypeCode]               VARCHAR (3)   NULL,
    [CasenoteNo]                    VARCHAR (16)  NULL,
    [AppointmentCreateDate]         VARCHAR (19)  NULL,
    [EpisodicGpCode]                VARCHAR (8)   NULL,
    [EpisodicGpPracticeCode]        VARCHAR (6)   NULL,
    [DoctorCode]                    VARCHAR (20)  NULL,
    [PrimaryDiagnosisCode]          VARCHAR (10)  NULL,
    [SubsidiaryDiagnosisCode]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode1]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode2]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode3]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode4]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode5]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode6]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode7]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode8]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode9]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode10]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode11]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode12]      VARCHAR (10)  NULL,
    [PrimaryOperationCode]          VARCHAR (10)  NULL,
    [PrimaryOperationDate]          VARCHAR (19)  NULL,
    [SecondaryOperationCode1]       VARCHAR (10)  NULL,
    [SecondaryOperationDate1]       VARCHAR (19)  NULL,
    [SecondaryOperationCode2]       VARCHAR (10)  NULL,
    [SecondaryOperationDate2]       VARCHAR (19)  NULL,
    [SecondaryOperationCode3]       VARCHAR (10)  NULL,
    [SecondaryOperationDate3]       VARCHAR (19)  NULL,
    [SecondaryOperationCode4]       VARCHAR (10)  NULL,
    [SecondaryOperationDate4]       VARCHAR (19)  NULL,
    [SecondaryOperationCode5]       VARCHAR (10)  NULL,
    [SecondaryOperationDate5]       VARCHAR (19)  NULL,
    [SecondaryOperationCode6]       VARCHAR (10)  NULL,
    [SecondaryOperationDate6]       VARCHAR (19)  NULL,
    [SecondaryOperationCode7]       VARCHAR (10)  NULL,
    [SecondaryOperationDate7]       VARCHAR (19)  NULL,
    [SecondaryOperationCode8]       VARCHAR (10)  NULL,
    [SecondaryOperationDate8]       VARCHAR (19)  NULL,
    [SecondaryOperationCode9]       VARCHAR (10)  NULL,
    [SecondaryOperationDate9]       VARCHAR (19)  NULL,
    [SecondaryOperationCode10]      VARCHAR (10)  NULL,
    [SecondaryOperationDate10]      VARCHAR (19)  NULL,
    [SecondaryOperationCode11]      VARCHAR (10)  NULL,
    [SecondaryOperationDate11]      VARCHAR (19)  NULL,
    [PurchaserCode]                 VARCHAR (10)  NULL,
    [ProviderCode]                  VARCHAR (5)   NULL,
    [ContractSerialNo]              VARCHAR (6)   NULL,
    [ReferralDate]                  VARCHAR (19)  NULL,
    [RTTPathwayID]                  VARCHAR (25)  NULL,
    [RTTPathwayCondition]           VARCHAR (20)  NULL,
    [RTTStartDate]                  VARCHAR (19)  NULL,
    [RTTEndDate]                    VARCHAR (19)  NULL,
    [RTTSpecialtyCode]              VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]        VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]          VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]          VARCHAR (19)  NULL,
    [RTTCurrentPrivatePatientFlag]  BIT           NULL,
    [RTTOverseasStatusFlag]         BIT           NULL,
    [RTTPeriodStatusCode]           VARCHAR (10)  NULL,
    [AppointmentCategoryCode]       VARCHAR (10)  NULL,
    [AppointmentCreatedBy]          VARCHAR (3)   NULL,
    [AppointmentCancelDate]         VARCHAR (19)  NULL,
    [LastRevisedDate]               VARCHAR (19)  NULL,
    [LastRevisedBy]                 VARCHAR (3)   NULL,
    [OverseasStatusFlag]            VARCHAR (1)   NULL,
    [PatientChoiceCode]             VARCHAR (3)   NULL,
    [ScheduledCancelReasonCode]     VARCHAR (4)   NULL,
    [PatientCancelReason]           VARCHAR (50)  NULL,
    [DischargeDate]                 VARCHAR (19)  NULL,
    [QM08StartWaitDate]             VARCHAR (19)  NULL,
    [QM08EndWaitDate]               VARCHAR (19)  NULL,
    [DestinationSiteCode]           VARCHAR (10)  NULL,
    [EBookingReferenceNo]           VARCHAR (50)  NULL,
    [InterfaceCode]                 VARCHAR (5)   NULL,
    [IsWardAttender]                BIT           NULL,
    [HomePhone]                     VARCHAR (50)  NULL,
    [WorkPhone]                     VARCHAR (50)  NULL,
    [SourceOfReferralGroupCode]     VARCHAR (10)  NULL,
    [WardCode]                      VARCHAR (10)  NULL,
    [ReferredByCode]                VARCHAR (10)  NULL,
    [ReferrerCode]                  VARCHAR (10)  NULL,
    [LastDNAorPatientCancelledDate] SMALLDATETIME NULL,
    [StaffGroupCode]                VARCHAR (10)  NULL,
    [InterpreterIndicator]          VARCHAR (3)   NULL,
    [EpisodicSiteCode]              VARCHAR (5)   NULL,
    [PseudoPostcode]                VARCHAR (10)  NULL,
    [DirectorateCode]               VARCHAR (5)   NULL,
    [ReferringSpecialtyTypeCode]    VARCHAR (1)   NULL,
    [EpisodicPostcode]              VARCHAR (8)   NULL
) ON [OP]
WITH (DATA_COMPRESSION = PAGE);


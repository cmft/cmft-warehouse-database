﻿CREATE TABLE [ETL].[TImportStaffingLevelAPC] (
    [DivisionCode]             VARCHAR (10)  NULL,
    [WardCode]                 VARCHAR (20)  NULL,
    [Shift]                    VARCHAR (10)  NULL,
    [Comments]                 VARCHAR (MAX) NULL,
    [SeniorComments]           VARCHAR (MAX) NULL,
    [CensusDate]               DATETIME      NULL,
    [RegisteredNursePlan]      INT           NULL,
    [RegisteredNurseActual]    INT           NULL,
    [NonRegisteredNursePlan]   INT           NULL,
    [NonRegisteredNurseActual] INT           NULL
);


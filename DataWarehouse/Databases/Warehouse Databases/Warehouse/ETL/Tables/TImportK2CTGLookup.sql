﻿CREATE TABLE [ETL].[TImportK2CTGLookup] (
    [Category]    VARCHAR (50)   NULL,
    [ID]          INT            NULL,
    [DisplayText] NVARCHAR (100) NULL,
    [OrderID]     INT            NULL
);


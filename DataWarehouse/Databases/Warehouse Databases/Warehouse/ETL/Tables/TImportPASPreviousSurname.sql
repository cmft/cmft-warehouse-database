﻿CREATE TABLE [ETL].[TImportPASPreviousSurname] (
    [DistrictNumber]        VARCHAR (14) NULL,
    [Forenames]             VARCHAR (20) NULL,
    [InternalDateOfBirth]   VARCHAR (10) NULL,
    [InternalPatientNumber] VARCHAR (10) NOT NULL,
    [PtPrevSexInt]          VARCHAR (1)  NULL,
    [SeqNo]                 VARCHAR (30) NOT NULL,
    [Surname]               VARCHAR (24) NULL
);


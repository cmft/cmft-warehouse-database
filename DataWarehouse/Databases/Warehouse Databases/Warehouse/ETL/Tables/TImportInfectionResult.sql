﻿CREATE TABLE [ETL].[TImportInfectionResult] (
    [AlertTypeCode]          VARCHAR (17)  NULL,
    [TestCategoryID]         INT           NULL,
    [Positive]               BIT           NULL,
    [SpecimenNumber]         VARCHAR (18)  NULL,
    [PatientName]            VARCHAR (50)  NULL,
    [DistrictNo]             VARCHAR (14)  NULL,
    [NHSNumber]              VARCHAR (15)  NULL,
    [SexCode]                VARCHAR (1)   NULL,
    [DateOfBirth]            DATE          NULL,
    [SpecimenCollectionDate] DATE          NULL,
    [SpecimenCollectionTime] DATETIME      NULL,
    [PreviousPositiveDate]   VARCHAR (50)  NULL,
    [OrganismCode]           VARCHAR (13)  NULL,
    [SampleTypeCode]         VARCHAR (100) NULL,
    [SampleSubTypeCode]      VARCHAR (15)  NULL,
    [SampleQualifierCode]    VARCHAR (100) NULL,
    [AuthorisationDate]      DATE          NULL,
    [ExtractFileName]        VARCHAR (30)  NULL
);


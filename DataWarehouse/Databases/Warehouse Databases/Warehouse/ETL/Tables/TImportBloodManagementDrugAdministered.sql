﻿CREATE TABLE [ETL].[TImportBloodManagementDrugAdministered] (
    [SourceUniqueID]       INT           NOT NULL,
    [EpisodeKey]           INT           NOT NULL,
    [HospitalNumber]       NVARCHAR (30) NULL,
    [LastName]             VARCHAR (100) NULL,
    [FirstName]            VARCHAR (100) NULL,
    [DateOfBirth]          DATETIME      NULL,
    [GenderCode]           INT           NOT NULL,
    [SpecialtyCode]        INT           NULL,
    [ConsultantCode]       INT           NULL,
    [IntendedDestination]  VARCHAR (100) NULL,
    [ActualDestination]    VARCHAR (100) NULL,
    [DrugCode]             INT           NULL,
    [Drug]                 VARCHAR (100) NULL,
    [Category]             VARCHAR (100) NULL,
    [Dose]                 REAL          NULL,
    [UnitsCode]            INT           NULL,
    [Units]                VARCHAR (100) NULL,
    [DrugAdministeredTime] DATETIME      NULL,
    [SessionType]          VARCHAR (100) NULL,
    [SessionLocationCode]  INT           NULL,
    [InterfaceCode]        VARCHAR (10)  NOT NULL
);


﻿CREATE TABLE [ETL].[TImportWAOperation] (
    [SourceUniqueID]    VARCHAR (50) NULL,
    [SourcePatientNo]   VARCHAR (20) NULL,
    [SourceEncounterNo] VARCHAR (20) NULL,
    [SequenceNo]        VARCHAR (10) NULL,
    [OperationCode]     VARCHAR (10) NULL,
    [OperationDate]     VARCHAR (19) NULL,
    [ConsultantCode]    VARCHAR (10) NULL,
    [WASourceUniqueID]  VARCHAR (50) NULL
);


﻿


CREATE view [ETL].[TLoadRenalPatientVascularAccess] as

select
	SourceUniqueID = cast(ObjectID as varchar(100))
	,PatientObjectID
	,PatientFullName = PatientFullName collate Latin1_General_CI_AS
	,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
	,StartDate = StartDate_Local
	,StopDate = StopDate_Local
	,DaysElapsed
	,AgeAtEvent
	,TimelineEvent = TimelineEvent collate Latin1_General_CI_AS
	,TimelineEventDetailCode = TimelineEventDetailCode collate Latin1_General_CI_AS
	,InformationSource = InformationSource collate Latin1_General_CI_AS
	,Notes = Notes collate Latin1_General_CI_AS
	,TerminationReason = TerminationReason collate Latin1_General_CI_AS
	,TerminationMethod = TerminationMethod collate Latin1_General_CI_AS
	,DateMatured = DateMatured_Local
	,IsPrimary
	,AccessType = AccessType collate Latin1_General_CI_AS
	,AccessLocation = AccessLocation collate Latin1_General_CI_AS
	,PermanentStatus = PermanentStatus collate Latin1_General_CI_AS
	,Brand = Brand collate Latin1_General_CI_AS
	,CatheterType = CatheterType collate Latin1_General_CI_AS
	,Configuration = Configuration collate Latin1_General_CI_AS
	,Elasticity = Elasticity collate Latin1_General_CI_AS
	,ExternalStatus = ExternalStatus collate Latin1_General_CI_AS
	,NeedleGuage = NeedleGuage collate Latin1_General_CI_AS
	,NeedleLength = NeedleLength collate Latin1_General_CI_AS
	,NeedleType = NeedleType collate Latin1_General_CI_AS
	,NeedleGuage2 = NeedleGuage2 collate Latin1_General_CI_AS
	,NeedleLength2 = NeedleLength2 collate Latin1_General_CI_AS
	,NeedleType2 = NeedleType2 collate Latin1_General_CI_AS
	,VenousLumen = VenousLumen collate Latin1_General_CI_AS
	,ArterialLumen = ArterialLumen collate Latin1_General_CI_AS
	,Preparation = Preparation collate Latin1_General_CI_AS
	,PrimingProcedures = PrimingProcedures collate Latin1_General_CI_AS
	,Surgeon = Surgeon collate Latin1_General_CI_AS
	,TerminationSurgeon = TerminationSurgeon collate Latin1_General_CI_AS
	,Summary = Summary collate Latin1_General_CI_AS
	,SurgicalHistoryObjectID
from ETL.TLoadRenalPatientVascularAccessBase






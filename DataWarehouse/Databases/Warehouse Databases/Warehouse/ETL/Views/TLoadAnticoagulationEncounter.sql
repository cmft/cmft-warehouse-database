﻿



CREATE view [ETL].[TLoadAnticoagulationEncounter]

as

select
	SourceUniqueID
	,NHSNumber
	,PatientSurname
	,PatientForename
	,DateOfBirth
	,SexCode
	,Postcode
	,ClinicID
	,TreatmentStartDate
	,TestDate
	,Result
	,Dose
	,DiagnosisID
	,DiagnosisDate
	,CommissionerID
	,GpPracticeCode
	,InterfaceCode = 'DAWN'
from
	ETL.TImportAnticoagulationEncounter



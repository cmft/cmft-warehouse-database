﻿


CREATE view [ETL].[TLoadNeonatalBeddayDiagnosis]

as

select
	[EpisodeID]
	,[CareDate]
	,[DiagnosisCode]
	,ModifiedTime
	,InterfaceCode = 'BDGR'
from
	[ETL].[TImportNeonatalBeddayDiagnosis]


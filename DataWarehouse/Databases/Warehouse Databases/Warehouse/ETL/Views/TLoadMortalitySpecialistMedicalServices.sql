﻿


CREATE view [ETL].[TLoadMortalitySpecialistMedicalServices] as

WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' AS ns)

select
	 SourceUniqueID = ReceivedForm.ID
	,FormTypeID = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Header/ns:FormID)[1]', 'INT')
	,ReviewStatus
	,Forename = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:FirstName)[1]', 'VARCHAR(50)')       
	,Surname = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:LastName)[1]', 'VARCHAR(50)')  
	,CasenoteNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:CaseNote)[1]', 'VARCHAR(50)')  
	,Age = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:age)[1]', 'INT')  
	,AdmissionTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DateTimeOfAdmission)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DateTimeOfAdmission)[1]', 'datetime')
			else null
		end
	,DateOfDeath = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:DateTimeOfDeath)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:DateTimeOfDeath)[1]', 'datetime')
			else null
		end
	,PlaceOfDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:PlaceOfDeath)[1]', 'VARCHAR(50)')  
	,LeadConsultant = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:LeadConsultant)[1]', 'VARCHAR(50)')  
	,OtherConsultants = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:OtherConsultants)[1]', 'VARCHAR(255)')  
	,ReviewedDate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DateOfReview)[1]', 'DATE') 
	,ReviewedBy = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:Reviewer)[1]', 'VARCHAR(50)')     
	,PrimaryDiagnosisOnAdmission = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:PrimaryDiagnosisOnAdmissionYN)[1]', 'BIT')  
	,PrimaryDiagnosisOnAdmissionComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:PrimaryDiagnosisOnAdmissionC)[1]', 'VARCHAR(max)')  
	,PrimaryDiagnosisAfterTestsConfirmed = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:ConfirmedPrimaryDiagnosisAfterTestsYN)[1]', 'BIT')  
	,PrimaryDiagnosisAfterTestsConfirmedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:ConfirmedPrimaryDiagnosisAfterTestsC)[1]', 'VARCHAR(max)')
	,CauseOfDeath1a= ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeath1a)[1]', 'bit')  
	,CauseOfDeath1b= ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeath1b)[1]', 'bit')  
	,CauseOfDeath1c= ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeath1c)[1]', 'bit')  
	,CauseOfDeath2= ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeathII)[1]', 'bit')  
	,CauseOfDeathComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeathComments)[1]', 'VARCHAR(max)')  
	,CodedDiagnosis = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CodedDiagnosisYN)[1]', 'bit')  
	,CodedDiagnosisComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CodedDiagnosisC)[1]', 'VARCHAR(max)')  
	,AgreeWithCauseOfDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:AgreeWithCauseOfDeathYN)[1]', 'bit')  
	,AgreeWithCauseOfDeathComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:AgreeWithCauseOfDeathC)[1]', 'VARCHAR(max)')  
	,HospitalPostmortem = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:HospitalPostmortemYN)[1]', 'bit')  
	,HospitalPostmortemComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:HospitalPostmortemC)[1]', 'VARCHAR(max)')  
	,CoronerInformedOrConsulted = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoronerInformedOrConsultedYN)[1]', 'bit')  
	,CoronerInformedOrConsultedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoronerInformedOrConsultedC)[1]', 'VARCHAR(max)')  
	,CoronerPostmortemPerformed = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoronersPostmortemPerformedYN)[1]', 'bit')  
	,CoronerPostmortemPerformedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoronersPostmortemPerformedC)[1]', 'VARCHAR(max)')  
	,MalignancyPresent = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:MalignancyPresentYN)[1]', 'bit')  
	,MalignancyPresentComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:MalignancyPresentC)[1]', 'VARCHAR(max)')  
	,CoExistingFactors = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoExistingFactorsYN)[1]', 'bit')  
	,CoExistingFactorsComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoExistingFactorsC)[1]', 'VARCHAR(max)')  
	,ConsultantReview = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:ConsultantReviewYN)[1]', 'bit')  
	,ConsultantReviewComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:ConsultantReviewC)[1]', 'VARCHAR(max)')  
	,EvidenceOfClearManagementPlan = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:EvidenceOfClearManagementPlanYN)[1]', 'bit')  
	,EvidenceOfClearManagementPlanComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:EvidenceOfClearManagementPlanC)[1]', 'VARCHAR(max)')  
	,EssentialInvestigationsObtainedWithoutDelay = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:EssentialInvestigationsObtainedWithoutDelayYN)[1]', 'bit')  
	,EssentialInvestigationsObtainedWithoutDelayComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:EssentialInvestigationsObtainedWithoutDelayC)[1]', 'VARCHAR(max)')  
	,InitialManagementStepsAppropriateAndAdequate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:InitialManagementStepsAppropriateAndAdequateYN)[1]', 'bit')  
	,InitialManagementStepsAppropriateAndAdequateComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:InitialManagementStepsAppropriateAndAdequateC)[1]', 'VARCHAR(max)') 
	,OmissionsInInitialManagement = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:OmissionsInInitialManagementYN)[1]', 'bit')  	
	,OmissionsInInitialManagementComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:First24Hours/ns:OmissionsInInitialManagementC)[1]', 'VARCHAR(max)') 
	,AdmittedToAppropriateWard = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:AdmittedToAppropriateWardYN)[1]', 'bit')  
	,AdmittedToAppropriateWardComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:AdmittedToAppropriateWardC)[1]', 'VARCHAR(max)')  
	,HowManyWardsPatientOn = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:HowManyWardsPatientOn)[1]', 'int')  
	,HowManyWardsPatientOnComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:HowManyWardsPatientOnC)[1]', 'VARCHAR(max)')  
	,MedicalStaffWriteInNotesEveryWeekday = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:MedicalStaffWriteInNotesEveryWeekdayYN)[1]', 'bit')  
	,MedicalStaffWriteInNotesEveryWeekdayComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:MedicalStaffWriteInNotesEveryWeekdayC)[1]', 'VARCHAR(max)')  
	,ConsultantReviewAtLeastTwiceAWeek = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:ConsultantReviewAtLeastTwiceAWeekYN)[1]', 'bit')  
	,ConsultantReviewAtLeastTwiceAWeekComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:DuringAdmission/ns:ConsultantReviewAtLeastTwiceAWeekC)[1]', 'VARCHAR(max)')  
	,ICUOpinionGiven = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:CareEscalation/ns:ICUOpinionGivenYN)[1]', 'bit') 
	,ICUOpinionGivenComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:CareEscalation/ns:ICUOpinionGivenC)[1]', 'VARCHAR(max)') 
	,TransferFromGeneralToICUorHDU = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:CareEscalation/ns:TransferFromGeneralToICUorHDUYN)[1]', 'bit') 
	,TransferFromGeneralToICUorHDUComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:CareEscalation/ns:TransferFromGeneralToICUorHDUC)[1]', 'VARCHAR(max)') 
	,ReadmissionToICUorHDU = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:CareEscalation/ns:ReadmissionToICUorHDUYN)[1]', 'bit') 
	,ReadmissionToICUorHDUComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:CareEscalation/ns:ReadmissionToICUorHDUC)[1]', 'VARCHAR(max)') 
	,AnyRecognisableMedicationErrors = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Medication/ns:AnyRecognisableMedicationErrorsYN)[1]', 'bit')  
	,AnyRecognisableMedicationErrorsComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Medication/ns:AnyRecognisableMedicationErrorsC)[1]', 'VARCHAR(max)') 
	,ChemotherapyOrImmunosuppressantsWithin30Days = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Medication/ns:ChemotherapyOrImmunosuppressantsWithin30DaysYN)[1]', 'bit') 
	,ChemotherapyOrImmunosuppressantsWithin30DaysComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Medication/ns:ChemotherapyOrImmunosuppressantsWithin30DaysC)[1]', 'VARCHAR(max)') 
	,TimingOfDiagnosis = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:TimingOfDiagnosisYN)[1]', 'bit')  
	,TimingOfDiagnosisComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:TimingOfDiagnosisC)[1]', 'VARCHAR(max)')  
	,TimingOfDeliveringCare = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:TimingOfDeliveringCareYN)[1]', 'bit')  
	,TimingOfDeliveringCareComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:TimingOfDeliveringCareC)[1]', 'VARCHAR(max)')  
	,CommunicationIssues = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:CommunicationIssuesYN)[1]', 'bit')  
	,CommunicationIssuesComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:CommunicationIssuesC)[1]', 'VARCHAR(max)')  
	,AdverseEvents = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:AdverseEventsYN)[1]', 'bit')  
	,AdverseEventsComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:AdverseEventsC)[1]', 'VARCHAR(max)')  
	,IncidentReport = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:IncidentReportYN)[1]', 'bit')  
	,IncidentReportComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:IncidentReportC)[1]', 'VARCHAR(max)')  
	,AnythingCouldBeDoneDifferently = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:AnythingCouldBeDoneDifferentlyYN)[1]', 'bit')  
	,AnythingCouldBeDoneDifferentlyComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:AnythingCouldBeDoneDifferentlyC)[1]', 'VARCHAR(max)')  
	,DocumentationIssues = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:DocumentationIssuesYN)[1]', 'bit')  
	,DocumentationIssuesComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:DocumentationIssuesC)[1]', 'VARCHAR(max)')  
	,NotableGoodQualityCare = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:IssuesForConsideration/ns:NotableGoodQualityCare)[1]', 'VARCHAR(max)')  
	,LCPInPatientsNotes = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:EndOfLifeCare/ns:LCPInPatientsNotesYN)[1]', 'bit')  
	,LCPInPatientsNotesComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:EndOfLifeCare/ns:LCPInPatientsNotesC)[1]', 'VARCHAR(max)') 
	,PreferredPlaceOfDeathRecorded = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:EndOfLifeCare/ns:PreferredPlaceOfDeathRecordedYN)[1]', 'bit') 
	,PreferredPlaceOfDeathRecordedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:EndOfLifeCare/ns:PreferredPlaceOfDeathRecordedC)[1]', 'VARCHAR(max)') 
	,ExpressedWithToDieOutsideOfHospital = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:EndOfLifeCare/ns:ExpressedWithToDieOutsideOfHospitalYN)[1]', 'bit') 
	,ExpressedWithToDieOutsideOfHospitalComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:EndOfLifeCare/ns:ExpressedWithToDieOutsideOfHospitalC)[1]', 'VARCHAR(max)') 
	,LessonsLearned = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:FinalBlock/ns:LessonsLearned)[1]', 'VARCHAR(max)')
	,ActionPlan = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:FinalBlock/ns:ActionPlan)[1]', 'VARCHAR(max)')
	,ActionPlanReviewDate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:FinalBlock/ns:ReviewDateOfActionPlan)[1]', 'date')
	,SourcePatientNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:sPN)[1]', 'varchar(20)')  
	,EpisodeStartTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]', 'datetime')
			else null
		end
	,ContextCode

from
	[$(InfoPathRepository)].InfoPath.ReceivedForms ReceivedForm

inner join
       [$(Information)].Information.MortalityReviewForms
on     MortalityReviewForms.MortalityReviewFormID = ReceivedForm.ID

where
	--ReceivedForm.FormTypeID = 301 --Division of Specialist Medical Services - Mortality Review --20151009 RR on validation, this was bringing through other forms, (8 300) so changed the logic)
	ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Header/ns:FormID)[1]', 'INT') = 301








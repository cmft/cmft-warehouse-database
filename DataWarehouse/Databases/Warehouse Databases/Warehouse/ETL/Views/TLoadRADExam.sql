﻿CREATE view [ETL].[TLoadRADExam] 
AS

	/******************************************************************************
	**  Name: TLoadRADExam
	**  Purpose: 
	**
	**  Import procedure view for Radiology RAD.Exam table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 21/02/2012  Paul Egan Added ROW_NUMBER window function to FROM clause, to only
	** select distinct exam keys from the TImportRADExam table when an exam 
	** stat has been run in CRIS that joined with tables that result in duplicate exam
	** keys in the SAME stat. (Note that duplicate exam keys in 
	** DIFFERENT CRIS stats work ok, due to separate parsing of each stat file based on
	** ImportDateTimeStamp).
	** Cannot use SELECT DISTINCT as this can still result in a small number of duplicate 
	** exam keys where patient details have changed, resulting in non-unique rows for the 
	** same exam key.
	** 26.06.12    MH         Altered for CMFT environment
	******************************************************************************/

SELECT
	 ImportDateTimeStamp
	,SourceUniqueID = Utility.udfEmptyStringToNull(RADExam.[Exam Key])
	,EventSourceUniqueID = Utility.udfEmptyStringToNull(RADExam.[Event key])
	,AccessionNo = Utility.udfEmptyStringToNull(RADExam.[Accession])
	,ExamCode = Utility.udfEmptyStringToNull(RADExam.[Examination])
	,InterventionalFlag = Utility.udfEmptyStringToNull(RADExam.[Interventional])
	,AllocatedTimeMins = convert(int , RADExam.[Appt mins])

	,ExamDate = Utility.udfConvertDate(RADExam.[Exam Date])
	,ExamStartTime = Utility.udfConvertDateTime(RADExam.[Exam Date] , RADExam.[Start tim])
	,ExamEndTime =
		case
		when RADExam.[End time] < RADExam.[Start tim]
		then dateadd(dd , 1 , Utility.udfConvertDateTime(RADExam.[Exam Date] , RADExam.[End time]))
		else Utility.udfConvertDateTime(RADExam.[Exam Date] , RADExam.[End time])
		end
	,RoomCode = Utility.udfEmptyStringToNull(RADExam.[Room])

	,Radiographer1Code = Utility.udfEmptyStringToNull(RADExam.[Radiographer 1])
	,Radiographer1CompetenceLevel = Utility.udfEmptyStringToNull(RADExam.[Competance 1])
	,Radiographer1Difficulty = Utility.udfEmptyStringToNull(RADExam.[Difficulty 1])
	,Radiographer2Code = Utility.udfEmptyStringToNull(RADExam.[Radiographer 2])
	,Radiographer2CompetenceLevel = Utility.udfEmptyStringToNull(RADExam.[Competance 2])
	,Radiographer2Difficulty = Utility.udfEmptyStringToNull(RADExam.[Difficulty 2])
	,Radiographer3Code = Utility.udfEmptyStringToNull(RADExam.[Radiographer 3])
	,Radiographer3CompetenceLevel = Utility.udfEmptyStringToNull(RADExam.[Competance 3])
	,Radiographer3Difficulty = Utility.udfEmptyStringToNull(RADExam.[Difficulty 3])
	,RadiologistCode = Utility.udfEmptyStringToNull(RADExam.[Radiologist])

	,ContrastBatchNo = Utility.udfEmptyStringToNull(RADExam.[Batch number])
	,BoneDensitometryValue = Utility.udfEmptyStringToNull(RADExam.[Bone dense])
	,Concentration = Utility.udfEmptyStringToNull(RADExam.[Concentration])
	,ContractCode = Utility.udfEmptyStringToNull(RADExam.[Contract code])
	,ContrastCode = Utility.udfEmptyStringToNull(RADExam.[Contrast code])
	,ImageStoreDisk = Utility.udfEmptyStringToNull(RADExam.[Disk])
	,ExamQuality = Utility.udfEmptyStringToNull(RADExam.[Exam Qual])
	,FlexibleForm = Utility.udfEmptyStringToNull(RADExam.[Flexi form])
	,IgnoreAppt = Utility.udfEmptyStringToNull(RADExam.[Ignore appt])
	,InjectedBy = Utility.udfEmptyStringToNull(RADExam.[Injected By])
	,MuseumCode = Utility.udfEmptyStringToNull(RADExam.[Museum code])

	,Reaction = Utility.udfEmptyStringToNull(RADExam.[Reaction])
	,HasRedDot = Utility.udfEmptyStringToNull(RADExam.[Red Dot])
	,RestrainDose = Utility.udfEmptyStringToNull(RADExam.[Restrain dose])
	,RestrainName = Utility.udfEmptyStringToNull(RADExam.[Restrain name])
	,RestrainType = Utility.udfEmptyStringToNull(RADExam.[Restrain type])
	,ScanSlices = Utility.udfEmptyStringToNull(RADExam.[Scan slices])
	,ExamScreeningTime = Utility.udfEmptyStringToNull(RADExam.[Screening time])
	,ExamStatus = Utility.udfEmptyStringToNull(RADExam.[Status])

	--,ExamReportedTime = RADExam.[Time Reported]
	--,FirstVerifiedDate = RADExam.[Date First Ver]
	--,LastVerifiedDate = RADExam.[Date Last Ver]
	--,FirstAddendumDate = RADExam.[Date First Add]
	--,LastAddendumDate = RADExam.[Date Last Add]
	--,FirstAddendumBy = RADExam.[First Add By]
	--,FirstVerifiedBy = RADExam.[First Ver By]
	--,LastAddendumBy = RADExam.[Last Add By]
	--,LastVerifiedBy = RADExam.[Last Ver By]
	--,ExamTypedTime = RADExam.[Time Typed]
	--,ExamTypedBy = RADExam.[Typed By]

	,ExamBookedDate = Utility.udfConvertDate(RADExam.[Booked Date])
	,ExamBookedTime = Utility.udfConvertDateTime(RADExam.[Booked Date] , RADExam.[Booked Time])
	,ApptBookMode = Utility.udfEmptyStringToNull(RADExam.[Appt book mode])
	,ApptCommitFlag = Utility.udfEmptyStringToNull(RADExam.[Appt commit])
	,ApptPreviousDate = Utility.udfConvertDate(RADExam.[Appt prevdate])
	,ApptPreviousTime = Utility.udfConvertDateTime(RADExam.[Appt prevdate] , RADExam.[Appt prevtime])
	,ApptLetterPrintedDate = Utility.udfEmptyStringToNull(RADExam.[Appt printed])
	,SiteCodeAtTimeOfBooking = Utility.udfEmptyStringToNull(RADExam.[Appt site])
	,OrderCommsUniqueID = Utility.udfEmptyStringToNull(RADExam.[Order key])
	,DaysWaiting = Utility.udfEmptyStringToNull([Days Waiting])

	,ExamLogicalDeleteFlag = Utility.udfEmptyStringToNull(RADExam.[Deleted])
	,ExamCreatedDate = Utility.udfConvertDate(RADExam.[Creation date])
	,ExamCreatedTime = DATEADD(ss , convert(int , RADExam.[Creation time]) , Utility.udfConvertDate(RADExam.[Creation date]))
	,ExamModifiedDate = Utility.udfConvertDate(RADExam.[Mutation date])
	,ExamModifiedTime = DATEADD(ss , convert(int , RADExam.[Mutation time]) , Utility.udfConvertDate(RADExam.[Mutation date])) --into RAD.Encounter
	,ReportCheckedBy = Utility.udfEmptyStringToNull(RADExam.[Checked By])
	
	-- MH 05.05.11
	-- New fields included for CRIS update		
	,DateReported = Utility.udfConvertDate(RADExam.[Date Reported])
	,DateTyped = Utility.udfConvertDate(RADExam.[Date Typed])
	,DefaultLength = Utility.udfEmptyStringToNull(RADExam.[Default Length])
	,IDChecked = Utility.udfEmptyStringToNull(RADExam.[ID Checked])
	,IDCheckedBy = Utility.udfEmptyStringToNull(RADExam.[ID checked By])
	,IsTyped = Utility.udfEmptyStringToNull(RADExam.[Is Typed])
	,IsVerified = Utility.udfEmptyStringToNull(RADExam.[Is Verified])
	,MPPSID = Utility.udfEmptyStringToNull(RADExam.[MPPS ID])
	,MPPSStatus = Utility.udfEmptyStringToNull(RADExam.[MPPS Status])
	,OrderID = Utility.udfEmptyStringToNull(RADExam.[Order ID])
	,PregnancyChecked = Utility.udfEmptyStringToNull(RADExam.[Preg checked])
	,PregnancyCheckedBy = Utility.udfEmptyStringToNull(RADExam.[Preg checked By])
	,QuantityUsed = Utility.udfEmptyStringToNull(RADExam.[Quantity used])
	--,Red spot conf
	,StudyID = Utility.udfEmptyStringToNull(RADExam.[Study ID])
	,TimeFirstVerified = Utility.udfConvertDateTime(RADExam.[Exam Date] , RADExam.[Time First Verified])
	,TimeLastVerified = Utility.udfConvertDateTime(RADExam.[Exam Date] , RADExam.[Time Last Verified])
	,WaitBreachDate = Utility.udfConvertDate(RADExam.[Wait Breach Date])
	,WaitNPland = Utility.udfEmptyStringToNull(RADExam.[Wait N. Pland])
	,WasPlanned = Utility.udfEmptyStringToNull(RADExam.[Was Planned])
	,WasScheduled = Utility.udfEmptyStringToNull(RADExam.[Was Scheduled])
	,WasWaiting = Utility.udfEmptyStringToNull(RADExam.[Was Waiting])
	,WeeksWaiting = REPLACE(Utility.udfEmptyStringToNull(RADExam.[Weeks Waiting]),CHAR(10),'') -- Bodge to removes any LF character left over from SSIS import

from
	--Paul Egan 21/02/2012
	--Window function added (see changelog above)
	(
	SELECT
		 rowNumber =
			ROW_NUMBER() OVER (PARTITION BY [Exam Key] ORDER BY [ImportDateTimeStamp])
		,* -- 2015-11-03 CCB Replace with column names
	FROM
		[$(CRIS_Import)].dbo.TImportRADExam
	) RADExam
WHERE
	RADExam.rowNumber = 1

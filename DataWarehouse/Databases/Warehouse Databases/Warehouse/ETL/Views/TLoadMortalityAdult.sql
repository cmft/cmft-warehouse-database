﻿


CREATE View [ETL].[TLoadMortalityAdult]

as

/*==============================================================================================
Description: Mortality Review application XML shredding - Adult

When		Who				What
01/04/2015	Paul Egan		Initial Coding
08/04/2015	Rachel Royston	Extract Proc

===============================================================================================*/


Select
	SourceUniqueID
	,SourcePatientNo
	,EpisodeStartTime
	,FormTypeCode
	,MortalityReviewAdded
	,ReviewStatusCode
	,DelayInDiagnosis 
	,DelayInDiagnosisDetail  
	,DelayInDeliveringCare 
	,DelayInDeliveringCareDetail 
	,PoorCommunication 
	,PoorCommunicationDetail 
	,OrganisationalFailure 
	,OrganisationalFailureDetail
	,AreasOfConcernNoDiff 
	,AreasOfConcernNoDiffDetail 
	,AreasOfConcernContributed 
	,AreasOfConcernContributedDetail 
	,DoneDifferently 
	,DoneDifferentlyDetail 
	,DocStandard 
	,FurtherComment
	,ClassificationScore 
	,PrimaryDiagnosis 
	,ConfirmedPrimaryDiagnosis 
	,DeathExpected
	,RaisedEWS
	,EWSNotApplicable 
	,PolicyMet
	,EOLPlan 
	,CauseOfDeath1 
	,CauseOfDeath2 
	,CauseOfDeath3 
	,CauseOfDeath4 
	,PostMortem
	,CoronerInformed 
	,MalignancyPresent
	,AcuteMyocardialInfarction 
	,CerebralVA 
	,CongestiveHeartFailure 
	,ConnectiveTissue 
	,Dementia 
	,Diabetes 
	,LiverDisease 
	,PepticUlcer 
	,PeripheralVascularDisease 
	,PulmonaryDisease
	,Cancer 
	,DiabetesComplications 
	,Paraplegia
	,RenalDisease
	,MetastaticCancer 
	,SevereLiverDisease
	,HIV
	,ClinicalReviewTime 
	,ConsultantReviewTime
	,SeenWithinTwelveHours
	,AppropriateWard 
	,AppropriateWardDetail 
	,HowManyWards 
	,HowManyWardsDetail 
	,WriteInNotes 
	,WriteInNotesDetail 
	,NotReviewed48Hour 
	,NotReviewed48HourDetail 
	,FluidBalance 
	,FluidBalanceDetail 
	,Surgery 
	,SurgeryDetail
	,Sepsis 
	,SepsisDetail 
	,SepsisRecognised 
	,SepsisRecognisedDetail 
	,SepsisTreated 
	,SepsisTreatedDetail 
	,DNACPR
	,DNACPRDetail 
	,DNACPRDiscussion 
	,DNACPRDiscussionDetail 
	,ManagementPlan
	,ManagementPlanDetail 
	,Investigations 
	,InvestigationsDetail 
	,ManagementSteps 
	,ManagementStepsDetail 
	,Omissions 
	,OmissionsDetail 
	,CriticalCareReview 
	,CriticalCareReviewDetail 
	,CriticalAppropriateAdmitted 
	,CriticalAppropriateAdmittedDetail 
	,CriticalLessThan4Hours 
	,CriticalLessThan4HoursDetail
	,CriticalAppropriateNotAdmitted
	,CriticalAppropriateNotAdmittedDetail 
	,MedicationAnyErrors 
	,MedicationAnyErrorsDetail 
	,PrimaryReviewerCode
	,PrimaryReviewerAssignedTime 
	,PrimaryReviewerCompletedTime
	,SecondaryReviewerCode
	,SecondaryReviewerComments 
	,SecondaryReviewerAssignedTime
	,SecondaryReviewerCompletedTime
	,ContextCode
from
	(
	select
		SourceUniqueID = MortalityReviewBase.ID
		,SourcePatientNo
		,EpisodeStartTime
		,FormTypeCode
		,MortalityReviewAdded = cast(dateMortalityReviewAdded as date)
		,ReviewStatusCode = coalesce(ReviewStatusCode,0)
			
	/* ====================================== Form Section: Review Case ===========================================================*/
		
		,DelayInDiagnosis = cast(case FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDiagnosis)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,DelayInDiagnosisDetail = FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDiagnosisDets)[1]', 'varchar(4000)')
		,DelayInDeliveringCare = cast(case FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDelivering)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,DelayInDeliveringCareDetail = FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDeliveringDets)[1]', 'varchar(4000)')
		,PoorCommunication = cast(case FormData.XMLData.value('(/FormData/Sections/ReviewCase/PoorComms)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,PoorCommunicationDetail = FormData.XMLData.value('(/FormData/Sections/ReviewCase/PoorCommsDets)[1]', 'varchar(4000)')
		,OrganisationalFailure = cast(case FormData.XMLData.value('(/FormData/Sections/ReviewCase/OrganisationalFailure)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,OrganisationalFailureDetail = FormData.XMLData.value('(/FormData/Sections/ReviewCase/OrganisationalFailureDets)[1]', 'varchar(4000)')
		,AreasOfConcernNoDiff = cast(case FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernND)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,AreasOfConcernNoDiffDetail = FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernNDDets)[1]', 'varchar(4000)')
		,AreasOfConcernContributed = cast(case FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernContributed)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,AreasOfConcernContributedDetail = FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernContributedDets)[1]', 'varchar(4000)')
		,DoneDifferently = cast(case FormData.XMLData.value('(/FormData/Sections/ReviewCase/DoneDifferently)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,DoneDifferentlyDetail = FormData.XMLData.value('(/FormData/Sections/ReviewCase/DoneDifferentlyDets)[1]', 'varchar(4000)')
		,DocStandard = FormData.XMLData.value('(/FormData/Sections/ReviewCase/DocStandards)[1]', 'int')
		,FurtherComment = FormData.XMLData.value('(/FormData/Sections/ReviewCase/FurtherComments)[1]', 'varchar(4000)')
		
	/* ====================================== Form Section: Classification ===========================================================*/

		,ClassificationScore = FormData.XMLData.value('(/FormData/Sections/Classification/ClassificationScore)[1]', 'int')

	/* ====================================== Form Section: AdultPatientInfo ===========================================================*/

		,PrimaryDiagnosis = FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/PrimaryDiagnosis)[1]', 'varchar(4000)')
		,ConfirmedPrimaryDiagnosis = FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/ConfirmedPrimaryDiagnosis)[1]', 'varchar(4000)')
		,DeathExpected = cast(case FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/DeathExpected)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,RaisedEWS = cast(case FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/RaisedEWS)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit) -- N/As will appear as NULL
		,EWSNotApplicable = FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/EWSNotApplicable)[1]', 'varchar(4000)')
		,PolicyMet = cast(case FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/PolicyMet)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,EOLPlan = cast(case FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/EOLPlan)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,CauseOfDeath1 = FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/CauseOfDeath1)[1]', 'varchar(4000)')
		,CauseOfDeath2 = FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/CauseOfDeath2)[1]', 'varchar(4000)')
		,CauseOfDeath3 = FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/CauseOfDeath3)[1]', 'varchar(4000)')
		,CauseOfDeath4 = FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/CauseOfDeath4)[1]', 'varchar(4000)')
		,PostMortem = cast(case FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/PostMortem)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,CoronerInformed = cast(case FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/CoronerInformed)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,MalignancyPresent = cast(case FormData.XMLData.value('(/FormData/Sections/AdultPatientInfo/MalignancyPresent)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)

	/* ====================================== Form Section: CEFactors ===========================================================*/

		,AcuteMyocardialInfarction = FormData.XMLData.value('(/FormData/Sections/CEFactors/AcuteMyocardialInfarction)[1]', 'bit')
		,CerebralVA = FormData.XMLData.value('(/FormData/Sections/CEFactors/CerebralVA)[1]', 'bit')
		,CongestiveHeartFailure = FormData.XMLData.value('(/FormData/Sections/CEFactors/CongestiveHeartFailure)[1]', 'bit')
		,ConnectiveTissue = FormData.XMLData.value('(/FormData/Sections/CEFactors/ConnectiveTissue)[1]', 'bit')
		,Dementia = FormData.XMLData.value('(/FormData/Sections/CEFactors/Dementia)[1]', 'bit')
		,Diabetes = FormData.XMLData.value('(/FormData/Sections/CEFactors/Diabetes)[1]', 'bit')
		,LiverDisease = FormData.XMLData.value('(/FormData/Sections/CEFactors/LiverDisease)[1]', 'bit')
		,PepticUlcer = FormData.XMLData.value('(/FormData/Sections/CEFactors/PepticUlcer)[1]', 'bit')
		,PeripheralVascularDisease = FormData.XMLData.value('(/FormData/Sections/CEFactors/PeripheralVascularDisease)[1]', 'bit')
		,PulmonaryDisease = FormData.XMLData.value('(/FormData/Sections/CEFactors/PulmonaryDisease)[1]', 'bit')
		,Cancer = FormData.XMLData.value('(/FormData/Sections/CEFactors/Cancer)[1]', 'bit')
		,DiabetesComplications = FormData.XMLData.value('(/FormData/Sections/CEFactors/DiabetesComplications)[1]', 'bit')
		,Paraplegia = FormData.XMLData.value('(/FormData/Sections/CEFactors/Paraplegia)[1]', 'bit')
		,RenalDisease = FormData.XMLData.value('(/FormData/Sections/CEFactors/RenalDisease)[1]', 'bit')
		,MetastaticCancer = FormData.XMLData.value('(/FormData/Sections/CEFactors/MetastaticCancer)[1]', 'bit')
		,SevereLiverDisease = FormData.XMLData.value('(/FormData/Sections/CEFactors/SevereLiverDisease)[1]', 'bit')
		,HIV = FormData.XMLData.value('(/FormData/Sections/CEFactors/HIV)[1]', 'bit')

	/* ====================================== Form Section: AdmissionStart ===========================================================*/

		,ClinicalReviewTime = FormData.XMLData.value('(/FormData/Sections/AdmissionStart/ClinicalReviewTime)[1]', 'time(0)')
		,ConsultantReviewTime = FormData.XMLData.value('(/FormData/Sections/AdmissionStart/ConsultantReviewTime)[1]', 'time(0)')
		,SeenWithinTwelveHours = cast(case FormData.XMLData.value('(/FormData/Sections/AdmissionStart/SeenWithinTwelveHours)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)

	/* ====================================== Form Section: DuringAdmission ===========================================================*/

		,AppropriateWard = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/AppropriateWard)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,AppropriateWardDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/AppropriateWardDets)[1]', 'varchar(4000)')
		,HowManyWards = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/HowManyWards)[1]', 'varchar(4000)')
		,HowManyWardsDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/HowManyWardsDets)[1]', 'varchar(4000)')
		,WriteInNotes = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/Notes)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,WriteInNotesDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/NotesDets)[1]', 'varchar(4000)')
		,NotReviewed48Hour = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/NotReviewed48)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,NotReviewed48HourDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/NotReviewed48Dets)[1]', 'varchar(4000)')
		,FluidBalance = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/FluidBalance)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,FluidBalanceDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/FluidBalanceDets)[1]', 'varchar(4000)')
		,Surgery = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/Surgery)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,SurgeryDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/SurgeryDets)[1]', 'varchar(4000)')
		,Sepsis = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/Sepsis)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,SepsisDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/SepsisDets)[1]', 'varchar(4000)')
		,SepsisRecognised = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/SepsisRecognised)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,SepsisRecognisedDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/SepsisRecognisedDets)[1]', 'varchar(4000)')
		,SepsisTreated = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/SepsisTreated)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,SepsisTreatedDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/SepsisTreatedDets)[1]', 'varchar(4000)')
		,DNACPR = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/DNACPR)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,DNACPRDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/DNACPRDets)[1]', 'varchar(4000)')
		,DNACPRDiscussion = cast(case FormData.XMLData.value('(/FormData/Sections/DuringAdmission/DNACPRDiscussion)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,DNACPRDiscussionDetail = FormData.XMLData.value('(/FormData/Sections/DuringAdmission/DNACPRDiscussionDets)[1]', 'varchar(4000)')

	/* ====================================== Form Section: FirstHours (first 24 hours) ===========================================================*/

		,ManagementPlan = cast(case FormData.XMLData.value('(/FormData/Sections/FirstHours/ManagementPlan)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,ManagementPlanDetail = FormData.XMLData.value('(/FormData/Sections/FirstHours/ManagementPlanDets)[1]', 'varchar(4000)')
		,Investigations = cast(case FormData.XMLData.value('(/FormData/Sections/FirstHours/Investigations)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,InvestigationsDetail = FormData.XMLData.value('(/FormData/Sections/FirstHours/InvestigationsDets)[1]', 'varchar(4000)')
		,ManagementSteps = cast(case FormData.XMLData.value('(/FormData/Sections/FirstHours/ManagementSteps)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,ManagementStepsDetail = FormData.XMLData.value('(/FormData/Sections/FirstHours/ManagementStepsDets)[1]', 'varchar(4000)')
		,Omissions = cast(case FormData.XMLData.value('(/FormData/Sections/FirstHours/Omissions)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,OmissionsDetail = FormData.XMLData.value('(/FormData/Sections/FirstHours/OmissionsDets)[1]', 'varchar(4000)')

	/* ====================================== Form Section: EscalationOfCare ===========================================================*/

		,CriticalCareReview = cast(case FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/Critical)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,CriticalCareReviewDetail = FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/CriticalDets)[1]', 'varchar(4000)')
		,CriticalAppropriateAdmitted = cast(case FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/CriticalAppropriate)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit) --N/As will appear as NULL
		,CriticalAppropriateAdmittedDetail = FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/CriticalAppropriateDets)[1]', 'varchar(4000)')
		,CriticalLessThan4Hours = cast(case FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/Critical4)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,CriticalLessThan4HoursDetail = FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/Critical4Dets)[1]', 'varchar(4000)')
		,CriticalAppropriateNotAdmitted = cast(case FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/NotCriticalAppropriate)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,CriticalAppropriateNotAdmittedDetail = FormData.XMLData.value('(/FormData/Sections/EscalationOfCare/NotCriticalAppropriateDets)[1]', 'varchar(4000)')

	/* ====================================== Form Section: Medication ===========================================================*/

		,MedicationAnyErrors = cast(case FormData.XMLData.value('(/FormData/Sections/Medication/AnyErrors)[1]', 'varchar(10)') when 'Yes' then 1 when 'No' then 0 end as bit)
		,MedicationAnyErrorsDetail = FormData.XMLData.value('(/FormData/Sections/Medication/AnyErrorsDets)[1]', 'varchar(4000)')

	/* ====================================== Form Section: Reviews ===========================================================*/

		,PrimaryReviewerCode = FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/@ID)[1]', 'int') 
		,PrimaryReviewerAssignedTime = 
				case
					when FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRAssignedDate)[1]', 'varchar(4000)')
						= '' then null
					else cast(left(FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRAssignedDate)[1]', 'varchar(4000)'), 19) as datetime2(0))
				end
		,PrimaryReviewerCompletedTime = 
				case
					when FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRCompletedDate)[1]', 'varchar(4000)')
						= '' then null
					else cast(left(FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRCompletedDate)[1]', 'varchar(4000)'),19) as datetime2(0))
				end
		,SecondaryReviewerCode = FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/@ID)[1]', 'varchar(4000)')
		,SecondaryReviewerComments = FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SecondaryReviewerComments)[1]', 'varchar(4000)')
		,SecondaryReviewerAssignedTime = 
				case
					when FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRAssignedDate)[1]', 'varchar(4000)')
						= '' then null
					else CAST(left(FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRAssignedDate)[1]', 'varchar(4000)'),19) as datetime2(0))
				end
		,SecondaryReviewerCompletedTime = 
				case
					when FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRCompletedDate)[1]', 'varchar(4000)')
						= '' then null
					else CAST(left(FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRCompletedDate)[1]', 'varchar(4000)'),19) as datetime2(0))
				end
		,ContextCode
	from
		[$(MortalityReview)].dbo.MortalityReviewBase

	inner join Mortality.FormType
	on FormType.FormTypeCode = MortalityReviewBase.FormTypeID
	
	left join [$(MortalityReview)].dbo.FormData 
	on FormData.MortalityReviewBaseID = MortalityReviewBase.ID
			
	left join Mortality.ReviewStatus	
	on ReviewStatus = FormData.Status

	where
		FormType.Category = 'Adult'
	and cast(dateMortalityReviewAdded as date) between 
				coalesce(FormType.FromDate,getdate())
			and Coalesce(FormType.ToDate,getdate())
	) MortalityReviewAdult



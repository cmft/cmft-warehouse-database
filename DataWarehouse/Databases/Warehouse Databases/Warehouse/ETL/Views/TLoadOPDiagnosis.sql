﻿CREATE view [ETL].[TLoadOPDiagnosis] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID
from
	ETL.TImportOPDiagnosis

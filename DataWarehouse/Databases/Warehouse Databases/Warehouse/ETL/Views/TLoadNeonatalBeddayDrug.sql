﻿

CREATE view [ETL].[TLoadNeonatalBeddayDrug]

as

select
	[EpisodeID]
	,[CareDate]
	,[DrugCode]
	,ModifiedTime
	,InterfaceCode = 'BDGR'
from
	[ETL].[TImportNeonatalBeddayDrug]

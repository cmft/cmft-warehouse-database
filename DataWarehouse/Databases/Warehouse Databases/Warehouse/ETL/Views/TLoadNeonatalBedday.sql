﻿









CREATE view [ETL].[TLoadNeonatalBedday]

as

select
	EpisodeID
	,CareDate
	,CareLocationID
	,WardCode
	,Weight
	,WeightToday
	,HeadCircumference
	,Length
	,OneToOneNursing
	,CarerStatusCode
	,NursingStatusCode
	,ObservationsMonitoringCode
	,SurgeryToday
	,NeonatalLevelOfCare2001Code
	,NeonatalLevelOfCare2011Code
	/*
	Translate system HRG Code to standard code ...
	XA01Z	Neonatal Critical Care Intensive Care (1)
	XA02Z   Neonatal Critical Care High Dependency (2)
	XA03Z   Neonatal Critical Care Special Care without external carer (3)
	XA04Z   Neonatal Critical Care Special Care with external carer (4)
	XA05Z   Neonatal Critical Care Normal Care (5)
	*/
	,HRGCode =
		case NeonatalLevelOfCare2011Code
		--when '1' then 'XA01Z'
		--when '2' then 'XA02Z'
		--when '3' then 'XA03Z'
		--when '4' then 'XA04Z'
		--when '5' then 'XA05Z'
		--else cast(HRGCode as varchar(5))
		-- as per Ben Robinsons instruction 10.10.14
		when 1 then 'XA01Z'
		when 2 then 'XA02Z'
		when 3 then 'XA03Z'
		when 4 then 'XA05Z'
		else null
		end
	,GlasgowScore
	,RespiratorySupportCode
	,OxygenCode
	,VentilationModeCode
	,CPAPModeCode
	,NPAirway
	,NitricOxide
	,ECMO
	,Apnoea
	,Pneumothorax
	,ChestDrain
	,TracheostomyCode
	,TracheostomyCarerCode
	,ReplogleTube
	,Surfactant
	,SurfactantDoses
	,SurfactantDoseTotal
	,NecrotizingEntercolitisTreatmentCode
	,InotropesToday
	,ProstinToday
	,PulmonaryVasodilator
	,GastroschisisSilo
	,Stoma
	,RectalWashout
	,Peritonealdialysis
	,Haemofiltration
	,TAT
	,Tone
	,ConsciousnessCode
	,Convulsions
	,NASTreatment
	,NASDrugTreatment
	,NASScore
	,VPShuntSurgery
	,EEG
	,Cooling
	,EpiduralCatheter
	,VentricularDrain
	,VentricularTap
	,ROPScreen
	,ROPGrade
	,ROPSurgery
	,MaxBilirubin
	,Phototherapy
	,Immunoglbulins
	,UrinaryCtaheter
	,TPN
	,GlucoseElectrolyte
	,EnteralFeedCode
	,VolumeMilk
	,FormulaCode
	,FeedingMethodCode
	,AdditiveCode
	,LinesInSitu
	,ExchangeFull
	,ExchangePartial
	,BloodProductCode
	,DiagnosesDay
	,DrugsDay
	,IVDrugsToday
	,SkinToSkinToday
	,PDAToday
	,TransportedToday
	,CVSTreatmentToday
	,InfectionDiagnosis
	,JaundiceDiagnosed
	,Bilirubin
	,HIEScore
	,Probiotics
	,MRSASwab
	,ModifiedTime
	,InterfaceCode = 'BDGR'
from
	ETL.TImportNeonatalBedday









﻿

--Use Warehouse

CREATE View [ETL].[TLoadBloodManagementCellSalvage]

as

select 
	SourceUniqueID
	,EpisodeKey
	,HospitalNumber
	,LastName
	,FirstName
	,DateOfBirth
	,GenderCode
	,SpecialtyCode
	,ConsultantCode
	,IntendedDestination
	,ActualDestination
	,DrugCode
	,Drug
	,Category
	,Dose
	,Units
	,StartTime
	,EndTime
	,Duration
	,SessionType
	,SessionLocationCode
	,InterfaceCode 
from 
	ETL.TImportBloodManagementCellSalvage




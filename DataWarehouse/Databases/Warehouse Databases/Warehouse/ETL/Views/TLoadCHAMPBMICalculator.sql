﻿








CREATE view [ETL].[TLoadCHAMPBMICalculator] as

/* 
==============================================================================================
Description: 

When		Who			What
23/02/2015	Paul Egan	Initial coding
===============================================================================================
*/
select 
	BMICalculatorTime = 
		cast(cast(TImportCHAMPBMICalculator.[date] as date) as datetime) + 
			cast(TImportCHAMPBMICalculator.[time] as time) 
	,Username = 
		case [cs-username]
			when '-' then null
			else [cs-username]
		end
from 
	ETL.TImportCHAMPBMICalculator
;




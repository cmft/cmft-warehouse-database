﻿





/****** Object:  View [ETL].[TLoadAPCWaitingList]    Script Date: 10/17/2014 11:12:32 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

CREATE view [ETL].[TLoadAPCWaitingList] as

/******************************************************************************************************
View		: [ETL].[TLoadAPCWaitingList]
Description	: As view name

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
17/10/2014	Paul Egan       Added TCITime for day case and inpatient admission reminders.
*******************************************************************************************************/

SELECT
	 WL.SourcePatientNo
	,WL.SourceEncounterNo
	,WL.CensusDate
	,WL.PatientSurname
	,WL.PatientForename
	,WL.PatientTitle
	,WL.SexCode

	,DateOfBirth =
		case
		when isdate(WL.DateOfBirth) = 1 then WL.DateOfBirth 
		else null
		end

	,DateOfDeath =
		case
		when isdate(WL.DateOfDeath) = 1 then WL.DateOfDeath 
		else null
		end 

	,WL.NHSNumber
	,WL.DistrictNo
	,WL.MaritalStatusCode
	,WL.ReligionCode
	,WL.Postcode
	,WL.PatientsAddress1
	,WL.PatientsAddress2
	,WL.PatientsAddress3
	,WL.PatientsAddress4
	,WL.DHACode
	,WL.HomePhone
	,WL.WorkPhone
	,EthnicOriginCode =
		case
		when WL.EthnicOriginCode is null then 'Z' 
		else WL.EthnicOriginCode 
		end
	,WL.ConsultantCode

	,SpecialtyCode =
		coalesce(ConsultantToSpecialtyMap.XrefEntityCode, SpecialtyToSpecialtyMap.XrefEntityCode, WL.SpecialtyCode)
	
	,WL.SpecialtyCode PASSpecialtyCode

	,WL.ManagementIntentionCode
	,WL.AdmissionMethodCode
	,WL.PriorityCode
	,WL.WaitingListCode
	,WL.CommentClinical
	,WL.CommentNonClinical
	,WL.IntendedPrimaryOperationCode
	,WL.AdmissionReason
	,WL.Operation Operation

	,SiteCode = 
		coalesce(WL.SiteCode , WaitingList.SiteCode , 'MRI')

	,WL.WardCode
	,WL.WLStatus
	,WL.ProviderCode
	,WL.PurchaserCode
	,WL.ContractSerialNumber
	,WL.CancelledBy
	,WL.BookingTypeCode
	,WL.CasenoteNumber

	,OriginalDateOnWaitingList =
		case
		when isdate(WL.OriginalDateOnWaitingList) = 1 then WL.OriginalDateOnWaitingList 
		else null
		end

-- catch dodgy KornerWaits...
-- this is the KeyDate
	,DateOnWaitingList =
		case
		when dateadd(day, WL.KornerWait * -1, convert(datetime, WL.CensusDate)) < '1 Jan 1900' then null
		else dateadd(day, WL.KornerWait * -1, WL.CensusDate)
		end

--	WL.DateOnWaitingList,

	,TCIDate =
		case
		when isdate(WL.TCIDate) = 1 then WL.TCIDate 
		else null
		end

	,WL.KornerWait
	,WL.CountOfDaysSuspended

	,SuspensionStartDate =
		case
		when isdate(SuspensionWL.SuspensionStartDate) = 1 then SuspensionWL.SuspensionStartDate
		else null
		end

	,SuspensionEndDate =
		case
		when isdate(SuspensionWL.SuspensionEndDate) = 1 then SuspensionWL.SuspensionEndDate
		else null
		end

	,SuspensionWL.SuspensionReasonCode
	,SuspensionWL.SuspensionReason

	,InterfaceCode = 'INQ'

	,LocalEpisodicGpCode = WL.EpisodicGpCode 
	,LocalRegisteredGpCode = WL.RegisteredGpCode

	,EpisodicGpCode = EpisodicPractice.NationalCode 
	,WL.EpisodicGpPracticeCode
	,RegisteredGpCode = RegisteredPractice.NationalCode 
	,RegisteredPracticeCode = RegisteredPractice.PracticeCode 
	,SourceTreatmentFunctionCode = null 
	,TreatmentFunctionCode = null 
	,Specialty.NationalSpecialtyCode

	,PCTCode =
		rtrim(
			left(
				coalesce(
					Practice.ParentOrganisationCode
					,Postcode.PCTCode
					,WL.PurchaserCode
					,'OAT  '
				) + '00000'
				, 5
			)
		)

-- BreachDate
-- Set the BreachDate to CensusDate (or suspend end date if populated) + (breach days - Days waiting)
	,BreachDate =
		case
		when WL.AdmissionMethodCode = 'PL' then null
		else
			case
			when SuspensionWL.SuspensionEndDate is null then
				dateadd(day,
					coalesce(
						convert(int, DiagnosticBreachDays.Description), 
						convert(int, ProcedureBreachDays.Description), 
						convert(int, ConsultantBreachDays.Description), 
						convert(int, SpecialtyBreachDays.Description), 
						(select NumericValue from Utility.Parameter where Parameter = 'IPDEFAULTBREACHDAYS')
					),
				case
				when dateadd(day, WL.KornerWait * -1, convert(datetime, WL.CensusDate)) < '1 Jan 1900' then null
				else dateadd(day, WL.KornerWait * -1, WL.CensusDate)
				end)
			else
				case
				when coalesce(
					convert(int, DiagnosticBreachDays.Description), 
					convert(int, ProcedureBreachDays.Description), 
					convert(int, ConsultantBreachDays.Description), 
					convert(int, SpecialtyBreachDays.Description), 
					(select NumericValue from Utility.Parameter where Parameter = 'IPDEFAULTBREACHDAYS')
					) < WL.KornerWait then SuspensionWL.SuspensionEndDate
				else
					dateadd(day,
					coalesce(
						convert(int, DiagnosticBreachDays.Description), 
						convert(int, ProcedureBreachDays.Description), 
						convert(int, ConsultantBreachDays.Description), 
						convert(int, SpecialtyBreachDays.Description), 
						(select NumericValue from Utility.Parameter where Parameter = 'IPDEFAULTBREACHDAYS')
					) - WL.KornerWait,
					SuspensionWL.SuspensionEndDate)
				end
			end
		end

	,AdditionFlag =
		case
		when not exists
			(
				select
					1
				from
					APC.WaitingList LastWL
				where
					LastWL.CensusDate = 
					(
					select
						max(LatestWL.CensusDate)
					from
						APC.WaitingList LatestWL --get the last populated census over a week old
					where
				-- go back to previous Sunday
						LatestWL.CensusDate <= dateadd(day, -1 * (datepart(dw, WL.CensusDate) - 1), WL.CensusDate)
					and	LastWL.SourcePatientNo = LatestWL.SourcePatientNo
					and	LastWL.SourceEncounterNo = LatestWL.SourceEncounterNo
					)
				and	LastWL.SourcePatientNo = WL.SourcePatientNo
				and	LastWL.SourceEncounterNo = WL.SourceEncounterNo
				--and	LastWL.WLStatus = WL.WLStatus -- removed 4 Feb 2005
			)
		then 1
		else 0
		end

	,WL.ExpectedAdmissionDate
	,WL.NextOfKinName
	,WL.NextOfKinRelationship
	,WL.NextOfKinHomePhone
	,WL.NextOfKinWorkPhone
	,WL.ExpectedLOS

	,BreachDays =
		coalesce(
			 convert(int, DiagnosticBreachDays.Description)
			,convert(int, ProcedureBreachDays.Description)
			,convert(int, ConsultantBreachDays.Description)
			,convert(int, SpecialtyBreachDays.Description)
			,(select NumericValue from Utility.Parameter where Parameter = 'IPDEFAULTBREACHDAYS')
		)

	,WL.MRSA

	,WL.RTTPathwayID
	,WL.RTTPathwayCondition
	,WL.RTTStartDate
	,WL.RTTEndDate
	,WL.RTTSpecialtyCode
	,WL.RTTCurrentProviderCode
	,WL.RTTCurrentStatusCode
	,WL.RTTCurrentStatusDate
	,WL.RTTCurrentPrivatePatientFlag
	,WL.RTTOverseasStatusFlag

	,AddedToWaitingListTime =
		case
		when right(WL.AddedToWaitingListTime , 4) = '2400'
			then dateadd(minute , 1 , convert(smalldatetime , left(WL.AddedToWaitingListTime , 8) + ' 23:59'))
		else
			convert(smalldatetime ,
				left(WL.AddedToWaitingListTime , 8) + ' ' + left(right(WL.AddedToWaitingListTime, 4), 2) + ':' +
				right(WL.AddedToWaitingListTime, 2)
			)
		end

	,WL.SourceUniqueID
	,WL.AdminCategoryCode

	,GuaranteedAdmissionDate =
		case
		when isdate(WL.GuaranteedAdmissionDate) = 1 then WL.GuaranteedAdmissionDate
		else null
		end 
	,WithRTTOpenPathway = 0

	,IsShortNotice =
		case
		when WL.ShortNotice = 'YES' then 1
		when WL.ShortNotice = 'NO' then 0
		else null
		end
		
	,TCITime =			-- Added 17/10/2014 Paul Egan
		case
		when WL.TCITime = '24:00'
			then convert(smalldatetime , WL.TCIDate + ' 00:00')
		else convert(smalldatetime , WL.TCIDate + ' ' + WL.TCITime)
		end
FROM

	(
	SELECT
		 CensusDate
		,SourcePatientNo
		,SourceEncounterNo
		,PatientSurname
		,PatientForename
		,PatientTitle
		,SexCode
		,DateOfBirth
		,DateOfDeath
		,NHSNumber
		,DistrictNo
		,MaritalStatusCode
		,ReligionCode

		,Postcode =
			case
			when datalength(rtrim(ltrim(Postcode))) = 6 then left(Postcode, 2) + '   ' + right(Postcode, 3)
			when datalength(rtrim(ltrim(Postcode))) = 7 then left(Postcode, 3) + '  ' + right(Postcode, 3)
			else Postcode
			end

		,RegisteredGpCode
		,PatientsAddress1
		,PatientsAddress2
		,PatientsAddress3
		,PatientsAddress4
		,DHACode
		,HomePhone
		,WorkPhone
		,EthnicOriginCode
		,ConsultantCode
		,SpecialtyCode
		,ManagementIntentionCode
		,AdmissionMethodCode
		,PriorityCode
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,WaitingListCode
		,CommentClinical
		,CommentNonClinical
		,IntendedPrimaryOperationCode
		,AdmissionReason
		,Operation

		,SiteCode

		,WardCode
		,WLStatus
		,ProviderCode
		,PurchaserCode
		,ContractSerialNumber
		,CancelledBy
		,BookingTypeCode
		,CasenoteNumber
		,OriginalDateOnWaitingList
		,DateOnWaitingList
		,TCIDate = null 
		,KornerWait = convert(int, KornerWait) 
		,CountOfDaysSuspended
		,SourceTreatmentFunctionCode = null 
		,TreatmentFunctionCode = null 
		,ExpectedAdmissionDate
		,NextOfKinName
		,NextOfKinRelationship
		,NextOfKinHomePhone
		,NextOfKinWorkPhone
		,ExpectedLOS
		,MRSA

		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,AddedToWaitingListTime
		,SourceUniqueID
		,AdminCategoryCode
		,GuaranteedAdmissionDate
		,ShortNotice
		,TCITime = null		-- Added 17/10/2014 Paul Egan
	FROM
		ETL.TImportAPCWLCurrent CurrentWL

	-- arbitrarily exclude duplicate records from the extract table

	where
		not exists
			(
			select
				1
			from
				ETL.TImportAPCWLCurrent PreviousWLCurrent
			where
				PreviousWLCurrent.SourcePatientNo = CurrentWL.SourcePatientNo
			and	PreviousWLCurrent.SourceEncounterNo = CurrentWL.SourceEncounterNo
			and	PreviousWLCurrent.SourceUniqueID > CurrentWL.SourceUniqueID
			)

	union all

	SELECT
		 CensusDate
		,SourcePatientNo
		,SourceEncounterNo
		,PatientSurname
		,PatientForename
		,PatientTitle
		,SexCode
		,DateOfBirth
		,DateOfDeath
		,NHSNumber
		,DistrictNo
		,MaritalStatusCode
		,ReligionCode

		,Postcode =
			case
			when datalength(rtrim(ltrim(Postcode))) = 6 then left(Postcode, 2) + '   ' + right(Postcode, 3)
			when datalength(rtrim(ltrim(Postcode))) = 7 then left(Postcode, 3) + '  ' + right(Postcode, 3)
			else Postcode
			end

		,RegisteredGpCode
		,PatientsAddress1
		,PatientsAddress2
		,PatientsAddress3
		,PatientsAddress4
		,DHACode
		,HomePhone
		,WorkPhone
		,EthnicOriginCode
		,ConsultantCode
		,SpecialtyCode
		,ManagementIntentionCode
		,AdmissionMethodCode
		,PriorityCode
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,WaitingListCode
		,CommentClinical
		,CommentNonClinical
		,IntendedPrimaryOperationCode
		,AdmissionReason
		,Operation

		,SiteCode

		,WardCode
		,WLStatus
		,ProviderCode
		,PurchaserCode
		,ContractSerialNumber
		,CancelledBy = null 
		,BookingTypeCode
		,CasenoteNumber
		,OriginalDateOnWaitingList
		,DateOnWaitingList
		,TCIDate
		,KornerWait = convert(int, KornerWait) 
		,CountOfDaysSuspended
		,SourceTreatmentFunctionCode = null 
		,TreatmentFunctionCode = null 
		,ExpectedAdmissionDate = TCIDate 
		,NextOfKinName
		,NextOfKinRelationship
		,NextOfKinHomePhone
		,NextOfKinWorkPhone
		,ExpectedLOS
		,MRSA

		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,AddedToWaitingListTime
		,SourceUniqueID
		,AdminCategoryCode
		,GuaranteedAdmissionDate
		,ShortNotice
		,TCITime		-- Added 17/10/2014 Paul Egan
	FROM
		ETL.TImportAPCWLPreadmission PreadmissionWL
	where
		not exists
		(
		select
			1
		from
			dbo.APCWaitingListExclusion
		where
			APCWaitingListExclusion.SourcePatientNo = PreadmissionWL.SourcePatientNo
		and	APCWaitingListExclusion.SourceEncounterNo = PreadmissionWL.SourceEncounterNo
		)

	) WL


LEFT JOIN 
	(
	select
		*
	from
		ETL.TImportAPCWLSuspension
	where
		not exists
		(
		select
			1
		from
			ETL.TImportAPCWLSuspension PreviousSuspension
		where
			PreviousSuspension.SourcePatientNo = TImportAPCWLSuspension.SourcePatientNo
		and	PreviousSuspension.SourceEncounterNo = TImportAPCWLSuspension.SourceEncounterNo
		and	(
				convert(smalldatetime, PreviousSuspension.SuspensionStartDate) > convert(smalldatetime, TImportAPCWLSuspension.SuspensionStartDate)
			or	(
					convert(smalldatetime, PreviousSuspension.SuspensionStartDate) = convert(smalldatetime, TImportAPCWLSuspension.SuspensionStartDate)
				and	PreviousSuspension.SourceUniqueID > TImportAPCWLSuspension.SourceUniqueID
				)
			or	(
					convert(smalldatetime, PreviousSuspension.SuspensionStartDate) = convert(smalldatetime, TImportAPCWLSuspension.SuspensionStartDate)
				and	PreviousSuspension.SourceUniqueID = TImportAPCWLSuspension.SourceUniqueID
				and	PreviousSuspension.SuspensionEndDate < TImportAPCWLSuspension.SuspensionEndDate
				)
			)
		)
	) SuspensionWL 
ON	WL.SourceEncounterNo = SuspensionWL.SourceEncounterNo
AND	WL.SourcePatientNo = SuspensionWL.SourcePatientNo
AND	WL.CensusDate between convert(smalldatetime, SuspensionWL.SuspensionStartDate) and convert(smalldatetime, SuspensionWL.SuspensionEndDate)

LEFT JOIN PAS.Gp AS EpisodicPractice 
ON	WL.EpisodicGpCode = EpisodicPractice.GpCode

LEFT JOIN PAS.Gp AS RegisteredPractice 
ON	WL.RegisteredGpCode = RegisteredPractice.GpCode

left join [$(Organisation)].dbo.Practice Practice
on	Practice.OrganisationCode = coalesce(EpisodicPractice.PracticeCode, RegisteredPractice.PracticeCode)

left join [$(Organisation)].dbo.Postcode Postcode
on	Postcode.Postcode = WL.Postcode

left join EntityXref ConsultantToSpecialtyMap
on	ConsultantToSpecialtyMap.EntityTypeCode = 'CONSULTANT'
and	ConsultantToSpecialtyMap.XrefEntityTypeCode = 'SPECIALTY'
and	ConsultantToSpecialtyMap.EntityCode = WL.ConsultantCode

left join EntityXref SpecialtyToSpecialtyMap
on	SpecialtyToSpecialtyMap.EntityTypeCode = 'SPECIALTY'

and	SpecialtyToSpecialtyMap.XrefEntityTypeCode = 'SPECIALTY'
and	SpecialtyToSpecialtyMap.EntityCode = WL.SpecialtyCode

LEFT JOIN PAS.Specialty
ON	WL.SpecialtyCode = Specialty.SpecialtyCode

left join dbo.EntityLookup ConsultantBreachDays
on	ConsultantBreachDays.EntityTypeCode = 'IPCONSULTANTBREACHDAYS'
and	ConsultantBreachDays.EntityCode = WL.ConsultantCode

left join dbo.EntityLookup SpecialtyBreachDays
on	SpecialtyBreachDays.EntityTypeCode = 'IPSPECIALTYBREACHDAYS'
--changed 17 Feb 2006 PDO - Used Mapped Specialty
--and	SpecialtyBreachDays.EntityCode = WL.SpecialtyCode
and	SpecialtyBreachDays.EntityCode = 
		coalesce(
			 ConsultantToSpecialtyMap.XrefEntityCode
			,SpecialtyToSpecialtyMap.XrefEntityCode
			,WL.SpecialtyCode
		)

left join dbo.EntityLookup ProcedureBreachDays
on	ProcedureBreachDays.EntityTypeCode = 'IPPROCEDUREBREACHDAYS'
and	WL.IntendedPrimaryOperationCode like ProcedureBreachDays.EntityCode + '%'

left join dbo.EntityLookup DiagnosticBreachDays
on	DiagnosticBreachDays.EntityTypeCode = 'IPDIAGNOSTICBREACHDAYS'
and	left(WL.IntendedPrimaryOperationCode, 3) = DiagnosticBreachDays.EntityCode
and	upper(WL.Operation) not like '%$T%'

left join PAS.WaitingList
on	WaitingList.WaitingListCode = WL.WaitingListCode



















﻿




-- USE [Warehouse]
-- drop view [ETL].[TLoadAPCDementia]


CREATE view [ETL].[TLoadAPCDementia]

as

/***************************************************************************
**	20140626	RR	Create Dementia CMFT using GC script and DG Bedman logic
**
****************************************************************************/


select 
	ProviderSpellNo
	,SpellID
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,AdmissionWardCode
	,RecordedDate
	,RecordedTime	
	,KnownToHaveDementia	
	,ForgetfulnessQuestionAnswered	
	,AssessmentScore	
	,Investigation	
	,AssessmentNotPerformedReason	
	,ReferralSentDate	
	,InterfaceCode
from 
	ETL.TImportAPCDementia










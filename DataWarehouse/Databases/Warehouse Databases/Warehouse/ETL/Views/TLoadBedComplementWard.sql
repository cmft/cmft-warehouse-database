﻿CREATE view [ETL].[TLoadBedComplementWard]

as

select 
	 WardID
	,WardCode
	,Ward
from 
	ETL.TImportBedComplementWard

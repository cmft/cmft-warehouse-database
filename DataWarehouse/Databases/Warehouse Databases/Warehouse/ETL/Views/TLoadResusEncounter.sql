﻿




CREATE view [ETL].[TLoadResusEncounter]

as

select
	 SourceUniqueID
	,NHSNumber
	,CasenoteNumber 
	,PatientForename 
	,PatientSurname
	,DateOfBirth 
	,Sex
	,ConsultantCode 
	,Ethnicity
	,PatientGroup 
	,AdmissionReason 
	,AdmissionDate 
	,EventDate 
	,LocationCode 
	,Witnessed 
	,Monitored 
	,ALSInterventionNone
	,IVAccess 
	,IVMedication
	,ECGMonitor 
	,TrachealTube
	,OtherAirwayDevice 
	,MechanicalVentilation 
	,ImplantDefibCardioverter 
	,IntraArterialCatheter
	,ImmediateCause
	,ImmediateCauseOther
	,ResuscitationAttempt
	,Conscious
	,Breathing
	,Pulse
	,PresentingECG 
	,CPRStoppedTime 
	,CPRStoppedReason 
	,SpontaneousCirculation
	,ROSCTime 
	,TimeOfDeath 
	,UnsustainedROSC
	,CollapseTime 
	,CPRCalloutTime 
	,CPRArriveTime 
	,ArrestConfirmTime 
	,CPRStartTime 
	,AwakeningTime
	,AwakeningDate
	,DischargeOrDeath
	,DischargeDate
	,DischargeDestination
	,DateOfDeath 
	,CauseOfDeath
	,CallOutType 
	,Comment
	,ebmB 
	,ebmB2 
	,ebmB3 
	,ebmB4 
	,ebmB5 
	,ebmPAI 
	,InterfaceCode
from
	(
	select
		 SourceUniqueID = UUID 
		,NHSNumber
		,CasenoteNumber = PatientURN
		,PatientForename = forename
		,PatientSurname = surname
		,DateOfBirth = cast(dob as date)
		,Sex
		,ConsultantCode = Consultant
		,Ethnicity
		,PatientGroup = patientgroup
		,AdmissionReason = ReasonforAdmission
		,AdmissionDate = cast(DateofAdmit as date)
		,EventDate = cast(DateofEvent as date)
		,LocationCode = Location
		,Witnessed = 				
					case Witnessed
					when 'No'	then 0
					when 'Yes'	then 1
					end
		,Monitored = 
					case Monitored
					when 'No'	then 0
					when 'Yes'	then 1
					end
		,ALSInterventionNone = ALSinterventionnone
		,IVAccess = IVaccess
		,IVMedication = IVmedication
		,ECGMonitor = ECGmonitor
		,TrachealTube
		,OtherAirwayDevice = Otherairwaydevice
		,MechanicalVentilation = Mechanicalventilation
		,ImplantDefibCardioverter = Implantdefibcardioverter
		,IntraArterialCatheter
		,ImmediateCause
		,ImmediateCauseOther
		,ResuscitationAttempt = 
					case ResuscitationAttemptText
					when 'No'	then 0
					when 'Yes'	then 1
					end
		,Conscious
		,Breathing
		,Pulse
		,PresentingECG = Presentingecgtext
		,CPRStoppedTime = cast(left(DateofEvent, 10) + ' ' + left(cast(TimeCPRstopped as time), 8) as datetime)
		,CPRStoppedReason = WhyCPRstoppedtext
		,SpontaneousCirculation
		,ROSCTime = cast(left(DateofEvent, 10) + ' ' + left(cast(ROSCtime as time), 8) as datetime)
		,TimeOfDeath = cast(left(DateofEvent, 10) + ' ' + left(cast(TimeofDeath as time), 8) as datetime)
		,UnsustainedROSC
		,CollapseTime = cast(left(DateofEvent, 10) + ' ' + left(cast(CollapseTime as time), 8) as datetime)
		,CPRCalloutTime = cast(left(DateofEvent, 10) + ' ' + left(cast(CPRcalloutTime as time), 8) as datetime)
		,CPRArriveTime = cast(left(DateofEvent, 10) + ' ' + left(cast(CPRarriveTime as time), 8) as datetime)
		,ArrestConfirmTime = cast(left(DateofEvent, 10) + ' ' + left(cast(ArrestConfirmTime as time), 8) as datetime)
		,CPRStartTime = cast(left(DateofEvent, 10) + ' ' + left(cast(CPRstartTime as time), 8) as datetime)
		,AwakeningTime = cast(left(AwakeningDate, 10) + ' ' + left(cast(AwakeningTime as time), 8) as datetime)
		,AwakeningDate = cast(AwakeningDate as date)
		,DischargeOrDeath = nullif([Discharge/Death], '')
		,DischargeDate = cast(DateofDischarge as date)
		,DischargeDestination
		,DateOfDeath = cast(DateofDeath as date)
		,CauseOfDeath = CauseofDeath
		,CallOutType = TypeofCallouttext
		,Comment = comments
		,ebmB = 
				case ebmB
				when 'No'	then 0
				when 'Yes'	then 1
				end
		,ebmB2 =
				case ebmB2
				when 'No'	then 0
				when 'Yes'	then 1
				end
		,ebmB3 = 
				case ebmB3
				when 'No'	then 0
				when 'Yes'	then 1
				end
		,ebmB4 = 
				case ebmB4
				when 'No'	then 0
				when 'Yes'	then 1
				end
		,ebmB5 = 
				case ebmB5
				when 'No'	then 0
				when 'Yes'	then 1
				end
		,ebmPAI = 
				case ebmPAI
				when 'No'	then 0
				when 'Yes'	then 1
				end
		,InterfaceCode = 'CARDIAC'
	from
		[$(CardiacArrest)].dbo.DWresus
	) ResuscitationEvent






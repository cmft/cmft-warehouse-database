﻿
create view [ETL].TLoadPASGpHistory 

as

select
	GpHistoryID = [GPHISTORYID]
	,[DistrictNo] = [DistrictNumber]
	,EffectiveFromDate = case
					when isdate([FromDate]) = 0 
					then null
					else cast([FromDate] as datetime)
				end
	,FromDateTimeInteger = [FromDtTmInt]
	,GpAddressLine1 = [GpAddrLine1]
	,GpAddressLine2 = [GpAddrLine2]
	,GpAddressLine3 = [GpAddrLine3]
	,GpAddressLine4 = [GpAddrLine4]
	,[GpCode]
	,[GpfhCode]
	,[Initials]
	,SourcePatientNo = [InternalPatientNumber]
	,[NationalCode]
	,[Phone]
	,[PmiPatientGpHistoryToDateIndexKey]
	,[Postcode]
	,[Practice]
	,[SequenceNo] = [SeqNo]
	,[Surname]
	,[Title]
	,EffectiveToDate = case
					when isdate([ToDate]) = 0 
					then null
					else cast([ToDate] as datetime)
				end
from
	[ETL].[TImportPASGpHistory]

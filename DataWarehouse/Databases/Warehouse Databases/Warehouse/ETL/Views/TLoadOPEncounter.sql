﻿











CREATE  view [ETL].[TLoadOPEncounter] as

select 
	 SourceUniqueID
	,Encounter.SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Encounter.DistrictNo

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode = RegisteredGp.PracticeCode
	,SiteCode

	,AppointmentDate =
		case
		when Encounter.IsWardAttender = 1
		then Encounter.AppointmentDate
		else
			cast(
				case
				when Encounter.AppointmentTime = '24:00' then dateadd(minute , 1 , convert(smalldatetime , Encounter.AppointmentDate + ' 23:59'))
				else Encounter.AppointmentDate
				end
				as date
			)
		end

	,AppointmentTime =
		case
		when Encounter.IsWardAttender = 1
		then Encounter.AppointmentDate
		else
			case
			when Encounter.AppointmentTime = '24:00' then dateadd(minute , 1 , convert(smalldatetime , Encounter.AppointmentDate + ' 23:59'))
			else Encounter.AppointmentDate + ' ' + Encounter.AppointmentTime
			end
		end

	,ClinicCode

	,AdminCategoryCode =
		case
		when Encounter.AdminCategoryCode = 'NHS' then '01'
		when Encounter.AdminCategoryCode = 'PAY' then '02'
		when Encounter.AdminCategoryCode = 'IND' then '02'
		when Encounter.AdminCategoryCode = 'OV' then '02'
		when Encounter.AdminCategoryCode = 'AME' then '03'
		when Encounter.AdminCategoryCode = 'CAT' then '04'
		else '01'
		end

	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode

	,FirstAttendanceFlag = 
		case
		when IsWardAttender = 1 and FirstAttendanceFlag = 0 then 2
		else FirstAttendanceFlag
		end

	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode = EpisodicGp.PracticeCode
	,DoctorCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode

-- aef updated 7/5/2010

	,AppointmentCategoryCode = 
		case 
		when IsWardAttender = 1 and FirstAttendanceFlag = 1 then '1ST'
		when IsWardAttender = 1 and FirstAttendanceFlag = 2 then 'REA'
		else AppointmentCategoryCode end

	,AppointmentCreatedBy
	,AppointmentCancelDate

	,LastRevisedDate =
		case
		when right(rtrim(Encounter.LastRevisedDate) , 4) = '2400' then
			dateadd(minute , 1 ,
				convert(smalldatetime ,
					left(Encounter.LastRevisedDate , 8) + ' 23:59'
				)
			)
		else convert(smalldatetime ,
				left(Encounter.LastRevisedDate , 8) + ' ' +
				left(right(rtrim(Encounter.LastRevisedDate) , 4) , 2) + ':' + 
				right(right(rtrim(Encounter.LastRevisedDate) , 4) , 2)
			)
		end

	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode = 'INQ'

	,DNACode =
		case
		when Encounter.IsWardAttender = 0
		then
			case
			when Encounter.AppointmentStatusCode = 'CND' then '2'
			when Encounter.AppointmentStatusCode = 'DNA' then '3'
			when Encounter.AppointmentStatusCode like 'ATT%' then '5'
			when Encounter.AppointmentStatusCode = 'SATT' then '6'
			when Encounter.AppointmentStatusCode = 'NATT' then '7'
			when Encounter.AppointmentStatusCode = 'WLK' then '5'
			when Encounter.CancelledByCode = 'P' then '2'
			when Encounter.CancelledByCode = 'H' then '4'
			else '9'
			end
		else
--			08/01/2010 CCB added 2, 7 & 9; removed 3
			case Encounter.AppointmentStatusCode
			when '1' then '5'
			when '2' then '3'
			when '3' then '5'
			when '4' then '2'
			when '6' then '7'
			else '9'
--			else '3'
			end
		end

	,AttendanceOutcomeCode =

--	case
--	when Encounter.AppointmentStatusCode = 'NR'
--	then
--		case
--		when Encounter.CancelledByCode is null
--		then Encounter.AppointmentStatusCode
--		else Encounter.CancelledByCode
--		end
--	else Encounter.AppointmentStatusCode
--	end

--	08/01/2010 CCB map the OP outcome codes onto WA events 
	case
	when Encounter.IsWardAttender = 0 then
		case
		when Encounter.AppointmentStatusCode = 'NR' then coalesce(Encounter.CancelledByCode, Encounter.AppointmentStatusCode)
		else Encounter.AppointmentStatusCode
		end
	else
		case Encounter.AppointmentStatusCode
		when '1' then 'ATT'
		when '2' then 'DNA'
		when '3' then 'WLK'
		when '4' then 'CND'
		when '6' then 'NATT'
		else 'NR'
		end
	end

	,LocalAdminCategoryCode = Encounter.AdminCategoryCode

	,PCTCode =
		coalesce(
			 Practice.ParentOrganisationCode
			,Postcode.PCTCode
			,'X98'
		)

	,LocalityCode = LocalAuthority.LocalAuthorityCode

	,Encounter.IsWardAttender

	,RTTBreachDate = null --set later

	,ClockStartDate = null --set later

	,HomePhone
	,WorkPhone

	,SourceOfReferralGroupCode
	,WardCode
	,ReferredByCode
	,ReferrerCode
	,LastDNAorPatientCancelledDate
	,StaffGroupCode
	,InterpreterIndicator
	,EpisodicSiteCode
	,DirectorateCode
	,PseudoPostcode

	,PostcodeAtAppointment = 

		case
		when datalength(rtrim(ltrim(AddressAtAppointment.Postcode))) = 6 then left(AddressAtAppointment.Postcode, 2) + '   ' + right(AddressAtAppointment.Postcode, 3)
		when datalength(rtrim(ltrim(AddressAtAppointment.Postcode))) = 7 then left(AddressAtAppointment.Postcode, 3) + '  ' + right(AddressAtAppointment.Postcode, 3)
		else AddressAtAppointment.Postcode
		end
	
	,GpPracticeCodeAtAppointment =

		GpAtAppointment.GpPracticeCode

	,GpCodeAtAppointment = 

		GpAtAppointment.GpCode

	,CCGCode =
		coalesce(
			 CCGPractice.[Parent Organisation Code]
			,CCGPostcode.CCGCode
			,'00W'
		)
from
	ETL.TImportOPEncounter Encounter

-- CCB 2011-02-02
-- Correctly derives Practice Codes

left join PAS.Gp RegisteredGp
on	RegisteredGp.GpCode = Encounter.RegisteredGpCode

left join PAS.Gp EpisodicGp
on	EpisodicGp.GpCode = Encounter.EpisodicGpCode

--left join [PAS].[PatientGp] EpisodicGpAtAppointment
--on EpisodicGpAtAppointment.SourcePatientNo = Encounter.SourcePatientNo
--and  
--convert(datetime , Encounter.AppointmentDate )
--between convert(datetime , EpisodicGpAtAppointment.EffectiveFromDate )  and Coalesce(convert(datetime , EpisodicGpAtAppointment.EffectiveToDate ) ,getdate ()+999)

left join PAS.PatientGp GpAtAppointment
on GpAtAppointment.SourcePatientNo = Encounter.SourcePatientNo
and cast(Encounter.AppointmentDate as date) between GpAtAppointment.EffectiveFromDate and coalesce(GpAtAppointment.EffectiveToDate,'31 dec 9999')

--left join PAS.PatientAddress EpisodicAddress
--on EpisodicAddress.SourcePatientNo = Encounter.SourcePatientNo
--and  
--convert(datetime , Encounter.AppointmentDate)
--between convert(datetime , EpisodicAddress.EffectiveFromDate )  and Coalesce(convert(datetime , EpisodicAddress.EffectiveToDate ) ,getdate ()+999)

left join PAS.PatientAddress AddressAtAppointment
on AddressAtAppointment.SourcePatientNo = Encounter.SourcePatientNo
and cast(Encounter.AppointmentDate as date) between AddressAtAppointment.EffectiveFromDate and coalesce(AddressAtAppointment.EffectiveToDate,'31 dec 9999')

left join [$(Organisation)].dbo.Practice Practice
on	Practice.OrganisationCode = coalesce(EpisodicGp.PracticeCode, RegisteredGp.PracticeCode)


--left join Organisation.dbo.Practice Practice
--on	Practice.OrganisationCode = coalesce(EpisodicGpPracticeCode, RegisteredGpPracticeCode)

left join [$(Organisation)].dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join [$(Organisation)].dbo.LocalAuthority LocalAuthority
on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode

left join [$(Organisation)].[ODS].[General Medical Practice]  CCGPractice
on CCGPractice.[Organisation Code] = GpAtAppointment.GpPracticeCode


left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
on	CCGPostcode.Postcode =
		case
		when datalength(rtrim(ltrim(AddressAtAppointment.Postcode))) = 6 then left(AddressAtAppointment.Postcode, 2) + '   ' + right(AddressAtAppointment.Postcode, 3)
		when datalength(rtrim(ltrim(AddressAtAppointment.Postcode))) = 7 then left(AddressAtAppointment.Postcode, 3) + '  ' + right(AddressAtAppointment.Postcode, 3)
		else AddressAtAppointment.Postcode
		end

















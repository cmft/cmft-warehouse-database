﻿CREATE view [ETL].[TLoadRADEvent] 
AS

	/******************************************************************************
	**  Name: TLoadRADEvent
	**  Purpose: 
	**
	**  Import procedure view for Radiology RAD.Event table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 21/02/2012  Paul Egan Added ROW_NUMBER window function to FROM clause, to only
	** select distinct event keys from the TImportRADEvent table when an event 
	** stat has been run in CRIS that joined with tables that result in duplicate event
	** keys in the SAME stat. (Note that duplicate event keys in 
	** DIFFERENT CRIS stats work ok, due to separate parsing of each stat file based on
	** ImportDateTimeStamp).
	** Cannot use SELECT DISTINCT as this can still result in a small number of duplicate 
	** event keys where patient details have changed, resulting in non-unique rows for the 
	** same event key.
	** 26.06.12   MH         Altered for CMFT environment
	******************************************************************************/

SELECT 
	 ImportDateTimeStamp
	,SourceUniqueID = Utility.udfEmptyStringToNull(RADEvent.[Event key])
	,RTTPathwayID = Utility.udfEmptyStringToNull(RADEvent.[Pathway ID])
	,SourcePatientNo = Utility.udfEmptyStringToNull(RADEvent.[CRIS number] )
	,OriginalSourcePatientNo = Utility.udfEmptyStringToNull(RADEvent.[1st CRIS No])
	,AENumber = Utility.udfEmptyStringToNull(RADEvent.[A & E number])
	,AttendanceNo = Utility.udfEmptyStringToNull(RADEvent.[Attend no])
	,AttendedFlag = Utility.udfEmptyStringToNull(RADEvent.[Att])
	,BillReferrerFlag = Utility.udfEmptyStringToNull(RADEvent.[Bill Refer])
	,BookingModeCode = Utility.udfEmptyStringToNull(RADEvent.[Mode])
	,BookedByCode = Utility.udfEmptyStringToNull(RADEvent.[Booked by])

	,RequestDate = Utility.udfConvertDate(RADEvent.[Request date])
	,BookedDate = Utility.udfConvertDate(RADEvent.[Date Booked])
	,EventDate = Utility.udfConvertDate(RADEvent.[Event Date])
	,EventTime = Utility.udfConvertDateTime(RADEvent.[Event Date] , RADEvent.[Time])
	,DateOnWL = Utility.udfConvertDate(RADEvent.[Date on WL])
	,EventLogicalDeleteFlag = Utility.udfEmptyStringToNull(RADEvent.[Deleted])
	,IntendedRadiologistCode = Utility.udfEmptyStringToNull(RADEvent.[Intended radiol])
	,PatientTypeCode = Utility.udfEmptyStringToNull(RADEvent.[PT])
	,KeyDate = Utility.udfConvertDate([Date For Wait])
	,NumberOfCancellations = Utility.udfEmptyStringToNull([No of Cancels])

	,PacsVolumeKey = Utility.udfEmptyStringToNull(RADEvent.[Volume key])
	,ImageNo = Utility.udfEmptyStringToNull(RADEvent.[Image no])

	,LeadClinician = Utility.udfEmptyStringToNull(RADEvent.[Lead clinician])
	,LmpDate = Utility.udfConvertDate(RADEvent.[Lmp date])
	,MobilityCode = Utility.udfEmptyStringToNull(RADEvent.[Mobility])
	,DuringOnCallHoursFlag = Utility.udfEmptyStringToNull(RADEvent.[On call])
	,PatientHeight = Utility.udfEmptyStringToNull(RADEvent.[Pat height])
	,PatientWeight = Utility.udfEmptyStringToNull(RADEvent.[Pat weight])
	,Practitioner = Utility.udfEmptyStringToNull(RADEvent.[Practitioner])
	,PregnancyEventKey = Utility.udfEmptyStringToNull(RADEvent.[Preg key])
	,PregnancyFlag = Utility.udfEmptyStringToNull(RADEvent.[Pregnant])
	,ReferralSourceCode = Utility.udfEmptyStringToNull(RADEvent.[Ref Src])
	,ReferrerCode = Utility.udfEmptyStringToNull(RADEvent.[Referrer])
	,ReferralLocationCode = Utility.udfEmptyStringToNull(RADEvent.[Referring location])
	,ReferringPCTCode = Utility.udfEmptyStringToNull(RADEvent.[Referring PCT])
	,RequestCategoryCode = Utility.udfEmptyStringToNull(RADEvent.[RC])
	,ClinicalHistory = Utility.udfEmptyStringToNull(RADEvent.[Clinical history])
	,Comment = Utility.udfEmptyStringToNull(RADEvent.[Comment])

	,EpisodicPCTCode = Utility.udfEmptyStringToNull(RADEvent.[Patient PCT])
	,EpisodicPCT = rtrim(PCT.[Organisation Code]) + ' - ' + PCT.[Organisation Name]

	,SiteCode = Utility.udfEmptyStringToNull(RADEvent.[Site])
	,AttendancesAtSite = Utility.udfEmptyStringToNull(RADEvent.[Site att no])
	,SpecialtyCode = Utility.udfEmptyStringToNull(RADEvent.[Speciality])
	,UnreportedFlag = Utility.udfEmptyStringToNull(RADEvent.[Unrep]) -- careful with updates..
	,UrgencyCode = Utility.udfEmptyStringToNull(RADEvent.[Urgency])
	,DictatedDate = Utility.udfConvertDate(RADEvent.[Date Dictated])
	,DictatedTime = Utility.udfConvertDateTime(RADEvent.[Date Dictated] , RADEvent.[Dictated Time])
	,DictatedByCode = Utility.udfEmptyStringToNull(RADEvent.[Dictated By])
	--,DictatedBy = Utility.udfEmptyStringToNull(RADEvent.[Dictated By Name])
	,DictatedBy2Code = Utility.udfEmptyStringToNull(RADEvent.[Dictated By 2])
	--,DictatedBy2 = Utility.udfEmptyStringToNull(RADEvent.[Dictated By 2 Name])

	,ConsentFlag = Utility.udfEmptyStringToNull(RADEvent.[Consent])
	,ContrastReactionFlag = Utility.udfEmptyStringToNull(RADEvent.[Contrast react])
	,EpisodicReferrerCode = Utility.udfEmptyStringToNull(RADEvent.[Current referre])
	,EpisodicLocationCode = Utility.udfEmptyStringToNull(RADEvent.[Current ward])

	,Surname = Utility.udfEmptyStringToNull(RADEvent.[Surname])
	,Forenames = Utility.udfEmptyStringToNull(RADEvent.[Forenames])
	,Title = Utility.udfEmptyStringToNull(RADEvent.[Title])
	,DateOfBirth = Utility.udfConvertDate(RADEvent.[DOB])
	,DateOfDeath = Utility.udfConvertDate(RADEvent.[DOD])
	,DistrictNo = right(Utility.udfEmptyStringToNull(RADEvent.[Best Hosp No ]) , 8)
	,NHSNumber = Utility.udfEmptyStringToNull(RADEvent.[NHS number])
	,NHSNumberStatus = Utility.udfEmptyStringToNull(RADEvent.[NHS status])
	,NHSTraceStatus = Utility.udfEmptyStringToNull(RADEvent.[Traced status])
	,RegisteredGpCode = Utility.udfEmptyStringToNull(RADEvent.[GP])
	,RegisteredPracticeCode = Utility.udfEmptyStringToNull(RADEvent.[Practice])
	,EthnicOriginCode = Utility.udfEmptyStringToNull(RADEvent.[Ethnic origin])
	,MRSAIndicator = Utility.udfEmptyStringToNull(RADEvent.[MRSA])
	,SexCode = Utility.udfEmptyStringToNull(RADEvent.[Sex])
	,Address1 = Utility.udfEmptyStringToNull(RADEvent.[Address 1])
	,Address2 = Utility.udfEmptyStringToNull(RADEvent.[Address 2])
	,Address3 = Utility.udfEmptyStringToNull(RADEvent.[Address 3])
	,Address4 = Utility.udfEmptyStringToNull(RADEvent.[Address 4])
	,Postcode = Utility.udfEmptyStringToNull(RADEvent.[Postcode])
	,PostcodePart1 = Utility.udfEmptyStringToNull(RADEvent.[Pstcde1])
	,PostcodePart2 = Utility.udfEmptyStringToNull(RADEvent.[Pstcde2])
	,ResidentialStatusCode = Utility.udfEmptyStringToNull(RADEvent.[Resident])
	,PhoneMobile = Utility.udfEmptyStringToNull(RADEvent.[Tel mobile])
	,Phone1 = Utility.udfEmptyStringToNull(RADEvent.[Telephone])
	,Phone2 = Utility.udfEmptyStringToNull(RADEvent.[Telephone2])
	,OriginatingPasInternalPatientNo1 = Utility.udfEmptyStringToNull(RADEvent.[HIS ID])
	,OriginatingPasInternalPatientNo2 = Utility.udfEmptyStringToNull(RADEvent.[HIS ID Number])
	,RestrictedDetailsFlag = Utility.udfEmptyStringToNull(RADEvent.[Restricted])

	,DiabetesDrugFlag = Utility.udfEmptyStringToNull(RADEvent.[Diabetes drug])
	,ExamsInRange = Utility.udfEmptyStringToNull(RADEvent.[Exams])
	,UsesInhalerFlag = Utility.udfEmptyStringToNull(RADEvent.[Inhaler])
	,PreviousEventDate = Utility.udfConvertDate(RADEvent.[Last Event Date])
	,NumberOfAttendances = Utility.udfEmptyStringToNull(RADEvent.[No of attend])
	,NumberOfEvents = Utility.udfEmptyStringToNull(RADEvent.[No of events])
	,PdsScn = Utility.udfEmptyStringToNull(RADEvent.[PDS SCN])
	,PatientDetailsStatus = Utility.udfEmptyStringToNull(RADEvent.[Status])
	,HeartTransplantFlag = Utility.udfEmptyStringToNull(RADEvent.[Transplant])

	,CurrentLocationArrivalDate = Utility.udfConvertDate(RADEvent.[Ward date])

	,EventCreatedDate = Utility.udfConvertDate(RADEvent.[Creation date])
	,EventCreatedTime = Utility.udfConvertDateTime(RADEvent.[Creation date] , RADEvent.[Creation Time])
	,EventModifiedDate = Utility.udfConvertDate(RADEvent.[Mutation date])
	,EventModifiedTime = Utility.udfConvertDateTime(RADEvent.[Mutation date] , RADEvent.[Mutation Time])

	-- MH 05.05.11
	-- New fields included for CRIS update	
	,Abnormal = Utility.udfEmptyStringToNull(RADEvent.Abnormal)
	,AbnormalActioned =Utility.udfEmptyStringToNull(RADEvent.[Abnormal actioned])
	,AntiD = Utility.udfEmptyStringToNull(RADEvent.[Anti-D])
	,AwaitingAcknowledgement = Utility.udfEmptyStringToNull(RADEvent.[Awaiting acknowledgement])
	,DateReported = Utility.udfConvertDate(RADEvent.[Date Reported])
	,DayOfWeek = Utility.udfEmptyStringToNull(RADEvent.[Day of Week])
	,HasPathway = Utility.udfEmptyStringToNull(RADEvent.[Has Pathway])
	,IsDictated = Utility.udfEmptyStringToNull(RADEvent.Dictated)
	,IsProcessed = Utility.udfEmptyStringToNull(RADEvent.[Is Processed])
	,JustifiedBy = Utility.udfEmptyStringToNull(RADEvent.[Justified by])
	,NumberOfProcs = Utility.udfEmptyStringToNull(RADEvent.[No. Of Procs])
	,NumberOfScannedDocs = Utility.udfEmptyStringToNull(RADEvent.[No. Scanned Docs])
	,Questions = Utility.udfEmptyStringToNull(RADEvent.Questions)
	,Reason = Utility.udfEmptyStringToNull(RADEvent.Reason)
	,RequiredClinician = Utility.udfEmptyStringToNull(RADEvent.[Required clinician])
	,VettingStatus = Utility.udfEmptyStringToNull(RADEvent.[Vetting status])
	,XDSFolderID = Utility.udfEmptyStringToNull(RADEvent.[XDS folder ID])
	,XDSFolderUID = Utility.udfEmptyStringToNull(RADEvent.[XDS folder UID])
	,AgeAtEvent = Utility.udfEmptyStringToNull(RADEvent.[Age At Event])
	,ConsentComment = Utility.udfEmptyStringToNull(RADEvent.[Consent comment])
	,PatientCreationDate = Utility.udfConvertDate(RADEvent.[PatientCreationDate])
	--,PatientCreationTime = Utility.udfConvertDateTime(RADEvent.[PatientCreationDate] , RADEvent.[PatientCreationTime])
	,PatientCreationTime = null
	,Deleted = Utility.udfEmptyStringToNull(RADEvent.Deleted)
	,EmailConsent = Utility.udfEmptyStringToNull(RADEvent.[Email consent])
	,Language = Utility.udfEmptyStringToNull(RADEvent.Language)
	,PatientModifiedDate = Utility.udfConvertDate(RADEvent.[PatientMutationDate])
	--,PatientModifiedTime = Utility.udfConvertDateTime(RADEvent.[PatientMutationDate] , RADEvent.[PatientMutationTime])
	,PatientModifiedTime = null
	,TelMobileConsent = Utility.udfEmptyStringToNull(RADEvent.[Tel mobile consent])
FROM
	(
	--Paul Egan 21/02/2012
	--Window function added (see changelog above)
	SELECT ROW_NUMBER() OVER (PARTITION BY [Event key] ORDER BY ImportDateTimeStamp DESC) as rowNumber, *
	FROM [$(CRIS_Import)].dbo.TImportRADEvent
	) RADEvent
	LEFT OUTER JOIN [$(Organisation)].dbo.[Primary Care Organisation] PCT ON RADEvent.[Patient PCT] = PCT.[Organisation Code]
	
WHERE RADEvent.rowNumber = 1

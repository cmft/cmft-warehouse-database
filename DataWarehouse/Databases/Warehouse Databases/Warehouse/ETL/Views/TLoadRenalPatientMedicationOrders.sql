﻿


CREATE view [ETL].[TLoadRenalPatientMedicationOrders] as

select
SourceUniqueID = cast(ObjectID as varchar(100))
,PatientObjectID
,PatientFullName = PatientFullName collate Latin1_General_CI_AS
,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
,ServiceMasterName = ServiceMasterName collate Latin1_General_CI_AS
,ServiceType = ServiceType collate Latin1_General_CI_AS
,OrderDate = OrderDate_Local
,StartDate = StartDate_Local
,OrderedByMedicalStaffObjectID
,OrderedByInternalStaffObjectID
,OrderedBy = OrderedBy collate Latin1_General_CI_AS
,SignatureOnFile
,Indication = Indication collate Latin1_General_CI_AS
,StopDate = StopDate_Local
,ReasonStopped = ReasonStopped collate Latin1_General_CI_AS
,StoppedByMedicalStaffObjectID
,StoppedByInternalStaffObjectID
,StoppedBy = StoppedBy collate Latin1_General_CI_AS
,Location = Location collate Latin1_General_CI_AS
,Provider = Provider collate Latin1_General_CI_AS
,DurationInMinutes
,OrderedTreatments
,MaximumTreatments
,Notifications = Notifications collate Latin1_General_CI_AS
,Notes = Notes collate Latin1_General_CI_AS
,IsOpen
,IsSigned
,IsRefused
,IsMaster
,IndicationCode = IndicationCode collate Latin1_General_CI_AS
,DefaultScheduleObjectID
,ServiceMasterObjectID
,ProviderObjectID
,LocationObjectID
,NextScheduledDate = NextScheduledDate_Local
,LastServiceDate = LastServiceDate_Local
,ParentOrderObjectID
,DrugName = DrugName collate Latin1_General_CI_AS
,DrugType = DrugType collate Latin1_General_CI_AS
,MedicationGroup = MedicationGroup collate Latin1_General_CI_AS
,NonESRD
,Dose
,DoseUnits = DoseUnits collate Latin1_General_CI_AS
,Dosage = Dosage collate Latin1_General_CI_AS
,Frequency = Frequency collate Latin1_General_CI_AS
,AllowSubstitutions
,Route = Route collate Latin1_General_CI_AS
,DoseRangeMin
,DoseRangeMax
,SupplySource = SupplySource collate Latin1_General_CI_AS
,Method = Method collate Latin1_General_CI_AS
,Rate = Rate collate Latin1_General_CI_AS
,Device = Device collate Latin1_General_CI_AS
,Site = Site collate Latin1_General_CI_AS
,Summary = Summary collate Latin1_General_CI_AS
,ServiceDetails = ServiceDetails collate Latin1_General_CI_AS
from ETL.[TLoadRenalPatientMedicationOrdersBase]



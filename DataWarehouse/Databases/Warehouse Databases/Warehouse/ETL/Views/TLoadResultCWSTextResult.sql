﻿












CREATE view [ETL].[TLoadResultCWSTextResult] as

select --top 10
	SourcePatientNo
	,SourceEpisodeNo
	,TransactionNo
	,SessionNo
	,OrderSourceUniqueID
	,ConsultantCode
	,SpecialtyCode
	,OrderPriorityCode
	,OrderStatusCode
	,OrderEnteredByCode
	,OrderedByCode
	,OrderTime
	,OrderComment
	,SampleReferenceCode
	,EffectiveTime
	,LocationCode
	,ServiceCode
	,ResultCode
	,Result = 
		replace(
			Result
			, '||', '' + char(13)
			)
	,InterfaceCode = 'INQ' 
from
	ETL.TImportResultCWSTextResult












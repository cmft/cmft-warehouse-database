﻿






CREATE view [ETL].[TLoadCHAMPEncounter] as

/*
When		Who			What
=========================================================================================
16/01/2015	Paul Egan	Initial Coding
20/02/2015	Paul Egan	Added YearGroup
03/03/2015	Paul Egan	Added IsAccountRegistered
=========================================================================================
*/

select 
	 CRMChildID			-- Not unique in this dataset
	,CRMAssessmentID	-- Comes from a left join in staging so not guaranteed to be not null
	,SchoolCode
	,YearGroup
	,SchoolYear
	,AssessmentTime
	,MckessonID
	,NHSNumber = case when isnumeric(NHSNumber) = 1 then NHSNumber else null end	-- Some strange data entry in this field
	,Forename
	,Surname
	,DateOfBirth
	,SexCode
	,OptedOut
	,IsVisited
	,IsMeasured = case when BMI is null then 0 else 1 end
	,IsMailed
	,Centile91 = cast(Centile91 as numeric(11, 8))
	,Centile98 = cast(Centile98 as numeric(11, 8))
	,Centile99 = cast(Centile99 as numeric(11, 8))
	,Height = cast(Height as numeric(6, 2))
	,[Weight] = cast([Weight] as numeric(6, 2))
	,BMI = cast(BMI as numeric(12, 8))
	,Classification = 
		case
			when Classification is null then 'Unknown Centile'
			else Classification
		end
	,ClassificationOrder = 
		case
			when Classification is null then (select max(ClassificationOrder) + 1 from ETL.TImportCHAMPEncounter)
			else ClassificationOrder
		end
	,idx	-- Comes from a row_number function in staging, filtered on '1'
	,AgeMonths
	,AgeInMonthsAtMeasurementDate
	,ActivityCode
	,ReasonOtherDescription
	,AlternativeCode
	,CodeDescription
	,LastUpdated
	,CreatedByUserID
	,InterfaceCode = 'CHAMP'
	,IsAccountRegistered
from 
	ETL.TImportCHAMPEncounter






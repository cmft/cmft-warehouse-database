﻿create view [ETL].[TLoadDictationDocument]

as

select
	--DocumentCode 
	SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length]
	,[DocumentTypeCode]
	,[Priority]
	,[StatusCode] = null
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate] = dateadd(dd, datediff(dd, 0, [CreationDate]), 0)
	,[CreationTime] = [CreationDate]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate] = dateadd(dd, datediff(dd, 0, [CorrectionDate]), 0)
	,[CorrectionTime] = [CorrectionDate]
	,[CorrectionTimeTaken] = [CorrectionTime]
	,[WaitingTimeAuthorization]
	,[AuthorisationDate] = dateadd(dd, datediff(dd, 0, [AuthorisationDate]), 0)
	,[AuthorisationTime] = [AuthorisationDate]
	,[PassThroughTimeTaken] = [PassThroughTime]
	,[ContextCode]
	,[DictationTimeTaken] = [DictationTime]
	,[CorrectionRate]
	,WarehouseInterfaceCode = 'G2Archive'
from
	ETL.TImportDictationArchiveDocument
	
union all

select
	--document.[DocumentCode]
	document.SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length] = cast([Length] / 1000 as float) 
	,[DocumentTypeCode]
	,[Priority]
	,[StatusCode]
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate] = dateadd(dd, datediff(dd, 0, [CreationDate]), 0)
	,[CreationTime] = [CreationDate]
	,[RecognitionRate] = null
	,[WaitingTimeCorrection] = null
	,[CorrectionDate] = dateadd(dd, datediff(dd, 0, [CorrectionDate]), 0)
	,[CorrectionTime] = [CorrectionDate]
	,[CorrectionTimeTaken] = documentinformation.[CorrectionTime]
	,[WaitingTimeAuthorization] = null
	,[AuthorisationDate] = dateadd(dd, datediff(dd, 0, [AuthorisationDate]), 0)
	,[AuthorisationTime] = [AuthorisationDate]
	,[PassThroughTimeTaken] = documentinformation.[PassThroughTime]
	,[ContextCode]
	,[DictationTimeTaken] = documentinformation.[DictationTime]
	,[CorrectionRate] = null
	,WarehouseInterfaceCode = 'G2Speech'
from
	ETL.TImportDictationSpeechDocument document
	left outer join ETL.TImportDictationDocumentInformation documentinformation
	on document.SourceUniqueID = documentinformation.SourceUniqueID

﻿CREATE view [ETL].[TLoadPASMaternityClinicDetail] as

select
	 MaternityClinicDetailID = MATCLINICDETSID
	,SourceSpellNo = EpisodeNumber
	,SourcePatientNo = InternalPatientNumber
	,PatientKnownToBeImmune = IsThePatientKnownToBeImmune
	,RubellaAntibodyImmune = KeMatRubellaImmInt
	,StatusOfPersonConductingDeliveryCode = LeadProfessionalType
	,MotherHeight = MaternalHeight
	,NumberOfPreviousNeonatalDeaths = NoOfNeonatalDeaths
	,NumberOfPreviousNonInducedAbortions = NoOfNoninducedAbortions
	,NumberOfPreviousCaesarians = NoOfPreviousCaesareanSections
	,NumberOfPreviousInducedAbortions = NoOfPreviousInducedAbortions
	,NumberOfPreviousRegisteredLiveBirths = NoOfRegistrableLiveBirths
	,NumberOfPreviousRegisteredStillBirths = NoOfRegistrableStillBirths
	--,PatientImmunised
	,NumberOfPreviousTransfusions = PreviousTransfusion
	,AntibodyStatusTested = WasAntibodyStatusTested
	,AntibodyStatusTestedPositive = WasTheResultPositive
from
	ETL.TImportPASMaternityClinicDetail

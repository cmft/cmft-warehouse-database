﻿




CREATE view [ETL].[TLoadObservationAdmission] as

select
	SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SexID
	,Surname
	,Forename
	,AdmissionTime
	,StartSpecialtyID
	,StartLocationID
	,DischargeTime
	,EndSpecialtyID
	,EndLocationID
	,TreatmentOutcomeID
	,DischargeDestinationID
	,Comment
	,CreatedByUserID
	,CreatedTime
	,LastModifiedByUserID
	,LastModifiedTime
	,InterfaceCode = 'PTRACK'
from
	ETL.TImportObservationAdmission

﻿






CREATE view [ETL].[TLoadPEXSMSResponse]

as

SELECT 
	 ResponseID
	,SurveyTakenID = DischargeID
	,DischargeDate
	,DischargeTime
	,LocationID
	,WardID
	,SurveyDate
	,SurveyTime
	,SurveyID
	,LastRating
	,LastResponseTime
	,Status
	,SentTime
	,QuestionID
    ,AnswerID = SMSAnswer.AnswerID
    ,Answer = TImportPEXSMSResponse.Answer
	,ResponseDate
	,ResponseTime
	,QuestionTypeID
	,ChannelID
	,LocationTypeID     
 FROM
	ETL.TImportPEXSMSResponse

left join PEX.SMSWard
on	TImportPEXSMSResponse.LocationID = SMSWard.WardID	


left join PEX.SMSAnswer
on	TImportPEXSMSResponse.AnswerID = SMSAnswer.AnswerID

where
	[Status] = 'Survey Sent'
and TImportPEXSMSResponse.AnswerID is not null
and TImportPEXSMSResponse.Answer <> 'No Response Available'










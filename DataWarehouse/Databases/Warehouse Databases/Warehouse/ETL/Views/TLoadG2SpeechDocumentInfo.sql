﻿
create view ETL.TLoadG2SpeechDocumentInfo

as

select
	SourceUniqueID
	,[Running]
	,[DictationTime]
	,[CorrectionTime]
	,[PassThroughTime]
	,[SmSoundQualityAcceptable]
	,[SmClippingRatio]
	,[SmSignalLevel]
	,[SmSignalToNoiseRatio]
from
	[ETL].[TImportG2SpeechDocumentInfo]

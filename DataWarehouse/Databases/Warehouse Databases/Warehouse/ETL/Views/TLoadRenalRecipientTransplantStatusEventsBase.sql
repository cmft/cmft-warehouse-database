﻿
CREATE VIEW [ETL].[TLoadRenalRecipientTransplantStatusEventsBase] ([ObjectID], [PatientObjectID], [PatientFullName], 
[PatientMedicalRecordNo], [StartDate_Local], [StopDate_Local], [DaysElapsed], [AgeAtEvent], [TimelineEvent], 
TimelineEventDetailCode, [TimelineEventDetail], [InformationSource], [Notes], [RecipientUKTNumber], [ReferralObjectID], [ReferralStartDate_Local], [ReferralStopDate_Local], [ConsultantObjectID], [Consultant], [Provider], [DonorType], [TransplantPriority], [StatusDetail], [StatusReason])
as select
/* ObjectID ObjectID */ recipients0_.[oid] as col_0_0_,
/* PatientObjectID Patient.ObjectID */ patient1_.[oid] as col_1_0_,
/* PatientFullName Patient.Person.Name */ person2_.[Name] as col_2_0_,
/* PatientMedicalRecordNo Patient.MedicalRecordNo */ patient1_.[MedicalRecordNo] as col_3_0_,
/* StartDate_Local PatientDateRange.PatientStartDate */ patientdat3_.[PatientStartDate] as col_4_0_,
/* StopDate_Local PatientDateRange.PatientStopDate */ patientdat3_.[PatientStopDate] as col_5_0_,
/* DaysElapsed PatientDateRange.DaysElapsed */ case when cast(case when patientdat3_.[PatientStopDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStopDate] as float)) as datetime) end as int)-cast(case when patientdat3_.[PatientStartDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) end as int)>0 then cast(case when patientdat3_.[PatientStopDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStopDate] as float)) as datetime) end as int)-cast(case when patientdat3_.[PatientStartDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) end as int) else null end as col_6_0_,
/* AgeAtEvent AgeAtEvent */ case when (person2_.[DateOfBirth] is not null) and (patientdat3_.[PatientStartDate] is not null) then floor((cast(cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) as int)-cast(cast(floor(cast(person2_.[DateOfBirth] as float)) as datetime) as int))/365.25) else null end as col_7_0_,
/* TimelineEvent TimelineEvent */ code0.ClinicalVisionCore_UKEnglish as col_8_0_,
/* TimelineEventDetailCode */ code1.LocalCode as col_9_1_,
/* TimelineEventDetail TimelineEventDetail */ code1.ClinicalVisionCore_UKEnglish as col_9_0_,
/* InformationSource InformationSource */ code2.ClinicalVisionCore_UKEnglish as col_10_0_,
/* Notes Notes */ recipients0_4_.[Notes] as col_11_0_,
/* RecipientUKTNumber Patient.TransplantPatient.Recipient.UKTRecipientNumber */ recipient5_.[UKTRecipientNumber] as col_12_0_,
/* ReferralObjectID RecipientReferral.ObjectID */ recipients0_4_.[ReferralOID] as col_13_0_,
/* ReferralStartDate_Local RecipientReferral.PatientDateRange.PatientStartDate */ patientdat7_.[PatientStartDate] as col_14_0_,
/* ReferralStopDate_Local RecipientReferral.PatientDateRange.PatientStopDate */ patientdat7_.[PatientStopDate] as col_15_0_,
/* ConsultantObjectID MedicalStaffOfRecord.ObjectID */ medicalsta8_.[oid] as col_16_0_,
/* Consultant MedicalStaffOfRecord.LookUp */ case when medicalsta8_.[MedicalStaffOID] is not null then person11_.[Name] when otherphysi12_.[Name] is not null then otherphysi12_.[Name] else N'' end as col_17_0_,
/* Provider ProviderOfRecord.LookUp */ case when provider14_.[oid] is not null then provider14_.[Name] when otherprovi15_.[Name] is not null then otherprovi15_.[Name] else N'' end as col_18_0_,
/* DonorType RecipientReferral.DonorType */ code3.ClinicalVisionCore_UKEnglish as col_19_0_,
/* TransplantPriority TransplantPriority */ code4.ClinicalVisionCore_UKEnglish as col_20_0_,
/* StatusDetail RecipientStatusDetail */ code5.ClinicalVisionCore_UKEnglish as col_21_0_,
/* StatusReason RecipientStatusReason */ code6.ClinicalVisionCore_UKEnglish as col_22_0_ 
from [$(ClinicalVision)].[Transplant].[RecipientStatus] recipients0_  
inner join [$(ClinicalVision)].[Core].[TransplantStatus] recipients0_2_ on recipients0_.[oid]=recipients0_2_.[oid] 
inner join [$(ClinicalVision)].[Core].[ReferralEvent] recipients0_3_ on recipients0_.[oid]=recipients0_3_.[oid] 
inner join [$(ClinicalVision)].[Core].[Timeline] recipients0_4_ on recipients0_.[oid]=recipients0_4_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Patient] patient1_ on recipients0_4_.[PatientOID]=patient1_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Person] person2_ on patient1_.[oid]=person2_.[oid]  
left outer join [$(ClinicalVision)].[Transplant].[Recipient] recipient5_ on patient1_.[oid]=recipient5_.[oid] 
left outer join [$(ClinicalVision)].[Core].[PatientDateRange] patientdat3_ on recipients0_4_.[PatientDateRangeOID]=patientdat3_.[oid]  
left outer join [$(ClinicalVision)].[Transplant].[TransplantReferral] recipientr6_1_ on recipients0_4_.[ReferralOID]=recipientr6_1_.[oid]  
left outer join [$(ClinicalVision)].[Core].[Timeline] recipientr6_3_ on recipients0_4_.[ReferralOID]=recipientr6_3_.[oid] 
left outer join [$(ClinicalVision)].[Core].[PatientDateRange] patientdat7_ on recipientr6_3_.[PatientDateRangeOID]=patientdat7_.[oid] 
left outer join [$(ClinicalVision)].[Core].[MedicalStaffOfRecord] medicalsta8_ on recipients0_3_.[MedicalStaffOfRecordOID]=medicalsta8_.[oid]   
left outer join [$(ClinicalVision)].[Core].[Person] person11_ on medicalsta8_.[MedicalStaffOID]=person11_.[oid] 
left outer join [$(ClinicalVision)].[Core].[OtherPhysician] otherphysi12_ on medicalsta8_.[OtherPhysicianOID]=otherphysi12_.[oid] 
left outer join [$(ClinicalVision)].[Core].[ProviderOfRecord] providerof13_ on recipients0_3_.[ProviderOfRecordOID]=providerof13_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Provider] provider14_ on providerof13_.[ProviderOID]=provider14_.[oid] 
left outer join [$(ClinicalVision)].[Core].[OtherProvider] otherprovi15_ on providerof13_.[OtherProviderOID]=otherprovi15_.[oid]
left outer join [$(ClinicalVision)].[Core].[Code] code0 on code0.oid=recipients0_4_.[TimelineEvent]
left outer join [$(ClinicalVision)].[Core].[Code] code1 on code1.oid=recipients0_4_.[TimelineEventDetail]
left outer join [$(ClinicalVision)].[Core].[Code] code2 on code2.oid=recipients0_4_.[InformationSource]
left outer join [$(ClinicalVision)].[Core].[Code] code3 on code3.oid=recipientr6_1_.[DonorType]
left outer join [$(ClinicalVision)].[Core].[Code] code4 on code4.oid=recipients0_2_.[TransplantPriority]
left outer join [$(ClinicalVision)].[Core].[Code] code5 on code5.oid=recipients0_.[RecipientStatusDetail]
left outer join [$(ClinicalVision)].[Core].[Code] code6 on code6.oid=recipients0_.[RecipientStatusReason]


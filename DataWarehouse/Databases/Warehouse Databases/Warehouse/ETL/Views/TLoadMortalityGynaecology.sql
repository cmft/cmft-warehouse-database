﻿




CREATE view [ETL].[TLoadMortalityGynaecology] as

WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' AS ns)

select
	 SourceUniqueID = ReceivedForm.ID
	,FormTypeID = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Header/ns:FormID)[1]', 'INT')
	,ReviewStatus
	,ReviewedDate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfReview)[1]', 'date') 
	,ReviewedBy = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:Reviewer)[1]', 'varchar(50)') 
	,CasenoteNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:HospitalNumber)[1]', 'varchar(50)')
	,DateOfDeath = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfDeath)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfDeath)[1]', 'datetime')
			else null
		end
	,UnitNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:UnitNo)[1]', 'varchar(50)')
	,ClinicalObs = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinicalObsYN)[1]', 'bit')
	,ClinicalObsDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinicalObsD)[1]', 'varchar(max)')
	,AdmissionTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfAdmission)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfAdmission)[1]', 'datetime')
			else null
		end
	,CaseDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:SummaryOfDetailsOfCase)[1]', 'varchar(max)')
	,PatientName = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:PatientName)[1]', 'varchar(255)') 
	,DNR = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DNRYN)[1]', 'bit')
	,DNRDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DNRD)[1]', 'varchar(max)')
	,DeathPreventable = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DeathAvoidableOrPreventableYN)[1]', 'bit')
	,DeathPreventableComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DeathAvoidableOrPreventableD)[1]', 'varchar(max)')
	,NursingConcernsClinicalObs = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:NursingConcerns/ns:NC_ClinicalObsYN)[1]', 'bit')
	,NursingConcernsClinicalObsDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:NursingConcerns/ns:NC_ClinicalObsD)[1]', 'varchar(max)')
	,InitialManagementStepsAppropriateAndAdequate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:AppropriateDecisionsYN)[1]', 'bit')
	,InitialManagementStepsAppropriateAndAdequateComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:AppropriateDecisionsD)[1]', 'varchar(max)')
	,Palliative = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PalliativeYN)[1]', 'bit')
	,PalliativeDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PalliativeD)[1]', 'varchar(max)')
	,NursingConcernsRoleInDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:NursingConcerns/ns:NC_RoleInDeathYN)[1]', 'bit')
	,NursingConcernsRoleInDeathDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:NursingConcerns/ns:NC_RoleInDeathD)[1]', 'varchar(max)')
	,TransferICU = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Transfers/ns:T_ICUYN)[1]', 'bit')
	,TransferICUDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Transfers/ns:T_ICUD)[1]', 'varchar(max)')
	,TransferHDU = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Transfers/ns:T_HDUYN)[1]', 'bit')
	,TransferHDUDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Transfers/ns:T_HDUD)[1]', 'varchar(max)')
	,Miscomm = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:MiscommYN)[1]', 'bit')
	,MiscommDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:MiscommD)[1]', 'varchar(max)')
	,PoorCommPrimaryandSecondary = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PoorCommPrimaryandSecondaryYN)[1]', 'bit')
	,PoorCommPrimaryandSecondaryDDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PoorCommPrimaryandSecondaryD)[1]', 'varchar(max)')
	,NotableGoodQualityCare = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:GoodPractice)[1]', 'varchar(max)')
	,EndOfLifeCareLCPInPatientNotes = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:LCP/ns:LCPYN)[1]', 'bit')
	,EndOfLifeCareLCPApproTime = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:LCP/ns:LCPApproTimeYN)[1]', 'bit')
	,EndOfLifeCareLCPApproTimeComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:LCP/ns:LCPApproTimeD)[1]', 'varchar(max)')
	,EndOfLifeCarePreferredPlaceOfDeathRecorded = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:HospSet/ns:AdvanceDirectiveYN)[1]', 'bit')
	,EndOfLifeCarePreferredPlaceOfDeathRecordedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:HospSet/ns:PreferredPlaceOfDeath)[1]', 'varchar(max)')
	,LessonsLearned = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:MainConcerns)[1]', 'varchar(max)')
	,ActionPlan = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ActionsToBeTaken)[1]', 'varchar(max)')
	,DateOfBirth = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfBirth)[1]', 'datetime') 
	,ConsultantDiscussionB4WDofIC = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:TreatmentLimitYN)[1]', 'bit')
	,ConsultantDiscussionB4WDofICComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:TreatmentLimitD)[1]', 'varchar(max)')
	,EWSgteq3 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:EWSgteq3YN)[1]', 'bit')
	,EWSgteq3Detail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:EWSgteq3D)[1]', 'varchar(max)')
	,ManageOutOfHospital = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:HospSet/ns:ManageOutOfHospYN)[1]', 'bit')
	,WhereOutOfHospital = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:HospSet/ns:WhereOutOfHosp)[1]', 'varchar(max)')
	,PalliativeCareTeamInvolvement = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PallCareTeamInvolvementYN)[1]', 'bit')
	,PalliativeCareTeamInvolvementNoDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PallCareTeamInvolvementNoD)[1]', 'varchar(max)')
	,AdmittedOutOfHours = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:AdmittedOutOfHoursYN)[1]', 'bit')
	,TransferredOtherHospitalSiteInUnstableCondition = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:TransfrerredOtheHospSiteInUnstableCondYN)[1]', 'bit')
	,TransferredOtherHospitalSiteInUnstableConditionDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:TransfrerredOtheHospSiteInUnstableCondD)[1]', 'varchar(max)')
	,ClinicianNotListeningToPatient = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinNotListeningtoPatientYN)[1]', 'bit')
	,ClinicianNotListeningtoPatientDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinNotListeningtoPatientD)[1]', 'varchar(max)')
	,PreviousFindingsEBMorHLI = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PreviousFindingsEBMorHLI)[1]', 'varchar(max)')

	,SourcePatientNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:sPN)[1]', 'varchar(20)')
	,EpisodeStartTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]', 'datetime')
			else null
		end
	,ContextCode
from
	[$(InfoPathRepository)].InfoPath.ReceivedForms ReceivedForm

inner join
	[$(Information)].Information.MortalityReviewForms
on	MortalityReviewForms.MortalityReviewFormID = ReceivedForm.ID

where
	ReceivedForm.FormTypeID = 304 --Gynaecology Mortality Review







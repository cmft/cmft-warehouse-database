﻿

CREATE view [ETL].[TLoadRenalPatientLabPanelBoneAndMinerals] as

select
SourceUniqueID = cast(ObjectID as varchar(100))
,PatientObjectID
,PatientFullName = PatientFullName collate Latin1_General_CI_AS
,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
,LabTestObjectID
,LabTestDate = LabTestDate_Local
,LabTestQualifier = LabTestQualifier collate Latin1_General_CI_AS
,LabTestStatus = LabTestStatus collate Latin1_General_CI_AS
,AluminumNum
,AluminumStr = AluminumStr collate Latin1_General_CI_AS
,AluminumFlg = AluminumFlg collate Latin1_General_CI_AS
,AluminumUnstimulatedNum
,AluminumUnstimulatedStr = AluminumUnstimulatedStr collate Latin1_General_CI_AS
,AluminumUnstimulatedFlg = AluminumUnstimulatedFlg collate Latin1_General_CI_AS
,AluminumStimulatedNum
,AluminumStimulatedStr = AluminumStimulatedStr collate Latin1_General_CI_AS
,AluminumStimulatedFlg = AluminumStimulatedFlg collate Latin1_General_CI_AS
,AlbuminNum
,AlbuminStr = AlbuminStr collate Latin1_General_CI_AS
,AlbuminFlg = AlbuminFlg collate Latin1_General_CI_AS
,AlkPhosNum
,AlkPhosStr = AlkPhosStr collate Latin1_General_CI_AS
,AlkPhosFlg = AlkPhosFlg collate Latin1_General_CI_AS
,CalciumNum
,CalciumStr = CalciumStr collate Latin1_General_CI_AS
,CalciumFlg = CalciumFlg collate Latin1_General_CI_AS
,CaxPhosNum
,CaxPhosStr = CaxPhosStr collate Latin1_General_CI_AS
,CaxPhosFlg = CaxPhosFlg collate Latin1_General_CI_AS
,IonizedCaNum
,IonizedCaStr = IonizedCaStr collate Latin1_General_CI_AS
,IonizedCaFlg = IonizedCaFlg collate Latin1_General_CI_AS
,PhosNum
,PhosStr = PhosStr collate Latin1_General_CI_AS
,PhosFlg = PhosFlg collate Latin1_General_CI_AS
,PTHIntactNum
,PTHIntactStr = PTHIntactStr collate Latin1_General_CI_AS
,PTHIntactFlg = PTHIntactFlg collate Latin1_General_CI_AS
,CAadjNum
,CAadjStr = CAadjStr collate Latin1_General_CI_AS
,CAadjFlg = CAadjFlg collate Latin1_General_CI_AS
,CaxPhosadjNum
,CaxPhosadjStr = CaxPhosadjStr collate Latin1_General_CI_AS
,CaxPhosadjFlg = CaxPhosadjFlg collate Latin1_General_CI_AS
,BIPTHNum
,BIPTHStr = BIPTHStr collate Latin1_General_CI_AS
,BIPTHFlg = BIPTHFlg collate Latin1_General_CI_AS
from ETL.TLoadRenalPatientLabPanelBoneAndMineralsBase


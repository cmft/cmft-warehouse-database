﻿
CREATE VIEW [ETL].[TLoadRenalPatientVascularAccessBase] ([ObjectID], [PatientObjectID], [PatientFullName], [PatientMedicalRecordNo], [StartDate_Local], [StopDate_Local], [DaysElapsed], [AgeAtEvent], [TimelineEvent], TimelineEventDetailCode, [TimelineEventDetail], [InformationSource], [Notes], [TerminationReason], [TerminationMethod], [DateMatured_Local], [IsPrimary], [AccessType], [AccessLocation], [PermanentStatus], [Brand], [CatheterType], [Configuration], [Elasticity], [ExternalStatus], [NeedleGuage], [NeedleLength], [NeedleType], [NeedleGuage2], [NeedleLength2], [NeedleType2], [VenousLumen], [ArterialLumen], [Preparation], [PrimingProcedures], [Surgeon], [TerminationSurgeon], [Summary], [SurgicalHistoryObjectID])
as select
/* ObjectID ObjectID */ vascularac0_.[oid] as col_0_0_,
/* PatientObjectID Patient.ObjectID */ patient1_.[oid] as col_1_0_,
/* PatientFullName Patient.Person.Name */ person2_.[Name] as col_2_0_,
/* PatientMedicalRecordNo Patient.MedicalRecordNo */ patient1_.[MedicalRecordNo] as col_3_0_,
/* StartDate_Local PatientDateRange.PatientStartDate */ patientdat3_.[PatientStartDate] as col_4_0_,
/* StopDate_Local PatientDateRange.PatientStopDate */ patientdat3_.[PatientStopDate] as col_5_0_,
/* DaysElapsed PatientDateRange.DaysElapsed */ case when cast(case when patientdat3_.[PatientStopDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStopDate] as float)) as datetime) end as int)-cast(case when patientdat3_.[PatientStartDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) end as int)>0 then cast(case when patientdat3_.[PatientStopDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStopDate] as float)) as datetime) end as int)-cast(case when patientdat3_.[PatientStartDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) end as int) else null end as col_6_0_,
/* AgeAtEvent AgeAtEvent */ case when (person2_.[DateOfBirth] is not null) and (patientdat3_.[PatientStartDate] is not null) then floor((cast(cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) as int)-cast(cast(floor(cast(person2_.[DateOfBirth] as float)) as datetime) as int))/365.25) else null end as col_7_0_,
/* TimelineEvent TimelineEvent */ code0.ClinicalVisionCore_UKEnglish as col_8_0_,
/* TimelineEventDetailCode */ code1.LocalCode as col_9_1_,
/* TimelineEventDetail TimelineEventDetail */ code1.ClinicalVisionCore_UKEnglish as col_9_0_,
/* InformationSource InformationSource */ code2.ClinicalVisionCore_UKEnglish as col_10_0_,
/* Notes Notes */ vascularac0_1_.[Notes] as col_11_0_,
/* TerminationReason VascularAccessTerminationReason */ code3.ClinicalVisionCore_UKEnglish as col_12_0_,
/* TerminationMethod VascularAccessTerminationMethod */ code4.ClinicalVisionCore_UKEnglish as col_13_0_,
/* DateMatured_Local DateMatured */ vascularac0_.[DateMatured] as col_14_0_,
/* IsPrimary IsPrimary */ case when (patient1_.[PrimaryVascularAccessOID] is not null) and patient1_.[PrimaryVascularAccessOID]=vascularac0_.[oid] then 1 else 0 end as col_15_0_,
/* AccessType AccessDefinition.VascularAccessType */ code5.ClinicalVisionCore_UKEnglish as col_16_0_,
/* AccessLocation AccessDefinition.VascularAccessLocation */ code6.ClinicalVisionCore_UKEnglish as col_17_0_,
/* PermanentStatus AccessDefinition.VascularAccessPermanentStatus */ code7.ClinicalVisionCore_UKEnglish as col_18_0_,
/* Brand AccessDefinition.VascularAccessBrand */ code8.ClinicalVisionCore_UKEnglish as col_19_0_,
/* CatheterType AccessDefinition.VascularAccessCatheterType */ code9.ClinicalVisionCore_UKEnglish as col_20_0_,
/* Configuration AccessDefinition.VascularAccessConfiguration */ code10.ClinicalVisionCore_UKEnglish as col_21_0_,
/* Elasticity AccessDefinition.VascularAccessElasticity */ code11.ClinicalVisionCore_UKEnglish as col_22_0_,
/* ExternalStatus AccessDefinition.VascularAccessExternalStatus */ code12.ClinicalVisionCore_UKEnglish as col_23_0_,
/* NeedleGuage AccessDefinition.VascularAccessNeedleGauge */ code13.ClinicalVisionCore_UKEnglish as col_24_0_,
/* NeedleLength AccessDefinition.VascularAccessNeedleLength */ code14.ClinicalVisionCore_UKEnglish as col_25_0_,
/* NeedleType AccessDefinition.VascularAccessNeedleType */ code15.ClinicalVisionCore_UKEnglish as col_26_0_,
/* NeedleGuage2 AccessDefinition.VascularAccessNeedleGauge2 */ code16.ClinicalVisionCore_UKEnglish as col_27_0_,
/* NeedleLength2 AccessDefinition.VascularAccessNeedleLength2 */ code17.ClinicalVisionCore_UKEnglish as col_28_0_,
/* NeedleType2 AccessDefinition.VascularAccessNeedleType2 */ code18.ClinicalVisionCore_UKEnglish as col_29_0_,
/* VenousLumen AccessDefinition.VenousLumen */ accessdefi5_.[VenousLumen] as col_30_0_,
/* ArterialLumen AccessDefinition.ArterialLumen */ accessdefi5_.[ArterialLumen] as col_31_0_,
/* Preparation AccessDefinition.Preparation */ accessdefi5_.[Preparation] as col_32_0_,
/* PrimingProcedures AccessDefinition.PrimingProcedures */ accessdefi5_.[PrimingProcedures] as col_33_0_,
/* Surgeon Surgeon.LookUp */ case when medicalsta6_.[MedicalStaffOID] is not null then person9_.[Name] when otherphysi10_.[Name] is not null then otherphysi10_.[Name] else N'' end as col_34_0_,
/* TerminationSurgeon Surgeon2.LookUp */ case when medicalsta11_.[MedicalStaffOID] is not null then person14_.[Name] when otherphysi15_.[Name] is not null then otherphysi15_.[Name] else N'' end as col_35_0_,
/* Summary InstanceName */ case when accessdefi5_.[Type] is not null then case when accessdefi5_.[Location] is not null then code5.ClinicalVisionCore_UKEnglish+N' '+N'{'+code6.ClinicalVisionCore_UKEnglish+N'}' else code5.ClinicalVisionCore_UKEnglish end else N'' end as col_36_0_,
/* SurgicalHistoryObjectID SurgicalHistory.ObjectID */ vascularac0_.[SurgicalHistoryOID] as col_37_0_ 
from [$(ClinicalVision)].[Core].[VascularAccess] vascularac0_ 
inner join [$(ClinicalVision)].[Core].[Timeline] vascularac0_1_ on vascularac0_.[oid]=vascularac0_1_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Patient] patient1_ on vascularac0_1_.[PatientOID]=patient1_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Person] person2_ on patient1_.[oid]=person2_.[oid]   
left outer join [$(ClinicalVision)].[Core].[PatientDateRange] patientdat3_ on vascularac0_1_.[PatientDateRangeOID]=patientdat3_.[oid] 
left outer join [$(ClinicalVision)].[Core].[AccessDefinition] accessdefi5_ on vascularac0_.[AccessDefinitionOID]=accessdefi5_.[oid] 
left outer join [$(ClinicalVision)].[Core].[MedicalStaffOfRecord] medicalsta6_ on vascularac0_.[SurgeonOID]=medicalsta6_.[oid]   
left outer join [$(ClinicalVision)].[Core].[Person] person9_ on medicalsta6_.[MedicalStaffOID]=person9_.[oid] 
left outer join [$(ClinicalVision)].[Core].[OtherPhysician] otherphysi10_ on medicalsta6_.[OtherPhysicianOID]=otherphysi10_.[oid] 
left outer join [$(ClinicalVision)].[Core].[MedicalStaffOfRecord] medicalsta11_ on vascularac0_.[Surgeon2OID]=medicalsta11_.[oid]   
left outer join [$(ClinicalVision)].[Core].[Person] person14_ on medicalsta11_.[MedicalStaffOID]=person14_.[oid] 
left outer join [$(ClinicalVision)].[Core].[OtherPhysician] otherphysi15_ on medicalsta11_.[OtherPhysicianOID]=otherphysi15_.[oid]
left outer join [$(ClinicalVision)].[Core].[Code] code0 on code0.oid=vascularac0_1_.[TimelineEvent]
left outer join [$(ClinicalVision)].[Core].[Code] code1 on code1.oid=vascularac0_1_.[TimelineEventDetail]
left outer join [$(ClinicalVision)].[Core].[Code] code2 on code2.oid=vascularac0_1_.[InformationSource]
left outer join [$(ClinicalVision)].[Core].[Code] code3 on code3.oid=vascularac0_.[TerminationReason]
left outer join [$(ClinicalVision)].[Core].[Code] code4 on code4.oid=vascularac0_.[TerminationMethod]
left outer join [$(ClinicalVision)].[Core].[Code] code5 on code5.oid=accessdefi5_.[Type]
left outer join [$(ClinicalVision)].[Core].[Code] code6 on code6.oid=accessdefi5_.[Location]
left outer join [$(ClinicalVision)].[Core].[Code] code7 on code7.oid=accessdefi5_.[PermanentStatus]
left outer join [$(ClinicalVision)].[Core].[Code] code8 on code8.oid=accessdefi5_.[Brand]
left outer join [$(ClinicalVision)].[Core].[Code] code9 on code9.oid=accessdefi5_.[CatheterType]
left outer join [$(ClinicalVision)].[Core].[Code] code10 on code10.oid=accessdefi5_.[Configuration]
left outer join [$(ClinicalVision)].[Core].[Code] code11 on code11.oid=accessdefi5_.[Elasticity]
left outer join [$(ClinicalVision)].[Core].[Code] code12 on code12.oid=accessdefi5_.[ExternalStatus]
left outer join [$(ClinicalVision)].[Core].[Code] code13 on code13.oid=accessdefi5_.[NeedleGauge]
left outer join [$(ClinicalVision)].[Core].[Code] code14 on code14.oid=accessdefi5_.[NeedleLength]
left outer join [$(ClinicalVision)].[Core].[Code] code15 on code15.oid=accessdefi5_.[NeedleType]
left outer join [$(ClinicalVision)].[Core].[Code] code16 on code16.oid=accessdefi5_.[NeedleGauge2]
left outer join [$(ClinicalVision)].[Core].[Code] code17 on code17.oid=accessdefi5_.[NeedleLength2]
left outer join [$(ClinicalVision)].[Core].[Code] code18 on code18.oid=accessdefi5_.[NeedleType2]


﻿
CREATE VIEW [ETL].[TLoadDM01AudiologyActivity] 
AS

	/******************************************************************************
	**  Name: ETL.TLoadDM01AudiologyActivity
	**  Purpose: 
	**
	**  Import procedure view for DM01 Audiology Activity table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 05.11.12    MH         Created
	******************************************************************************/

SELECT 
		 EncounterDate			= TImportDM01AudiologyActivity.CALENDAR_CREATE_DATE
		,SourceUniqueId			= CAST(CAST(TImportDM01AudiologyActivity.CALENDAR_CREATE_DATE AS DATE) AS CHAR(10)) + '||' + TImportDM01AudiologyActivity.PATIENT_PRIMARYCODE
		,SourcePatientNo 
		,CaseNoteNumber			= COALESCE(PATIENTNO.CaseNoteNumber, TImportDM01AudiologyActivity.PATIENT_PRIMARYCODE)
		,PCTCode				= COALESCE(PATIENTDETAILS.PCTCode,'5NT')

FROM
	ETL.TImportDM01AudiologyActivity 
	OUTER APPLY [Utility].[fn_GetPatientFromCaseNote](TImportDM01AudiologyActivity.PATIENT_PRIMARYCODE) PATIENTNO
	OUTER APPLY [Utility].[fn_GetPatientDetails](PATIENTNO.SourcePatientNo) PATIENTDETAILS


﻿




create view [ETL].[TLoadCOMService]

as

select
	ServiceID = ID 
	,ServiceCode = cast(Code as int)
	,Service = Name 
	,SpecialtyID = SPECT_REFNO 
	,IPMDescription
	,Destination
	,TCSDestinationID
	,District
	--,LeadName
	--,TreatmentFunctionCode
	--,[18Weeks]
	--,RefCosts
	,Directorate
	,StartDate
	,EndDate
	,ArchiveFlag
	,ModifiedDate
	,ModifiedUser
	--,ArchiveDate
	--,ArchiveUser
from
	[$(ipm)].dbo.Services



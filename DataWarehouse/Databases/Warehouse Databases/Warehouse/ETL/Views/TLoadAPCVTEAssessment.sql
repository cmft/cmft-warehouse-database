﻿



CREATE view [ETL].[TLoadAPCVTEAssessment]

as

--VTEAssessment

with VTEAssessmentCTE
(
	SourceUniqueID 
	,SourcePatientNo 
	,SourceSpellNo 
	,NoAssessmentReasonID
	,AssessmentDate 
	,AssessmentTime 
	,AtRisk
	,PreventativeMedicationStartDate 
	,PreventativeMedicationStartTime	
	,PreventativeNursingActionStartDate 
	,PreventativeNursingActionStartTime
	,WardCode 
)

as
	(
	select
		SourceUniqueID = VTEAssessment.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,NoAssessmentReasonID = Details.value('data(VTEAssessment/NoAssessmentReasonID)[1]', 'int')
		,AssessmentDate = nullif(Details.value('data(VTEAssessment/AssessmentCompleted_TS)[1]', 'date'), '1 jan 1900')
		,AssessmentTime = nullif(Details.value('data(VTEAssessment/AssessmentCompleted_TS)[1]', 'datetime'), '1 jan 1900')
		,AtRisk = 
				case
				when Details.value('data(VTEAssessment/ConsideredAtRisk)[1]', 'varchar(10)') = 'true'
				then 1
				else 0
				end
		,PreventativeMedicationStartDate = nullif(Details.value('data(VTEAssessment/VTEProphylaxis/MedicationStarted_TS)[1]', 'date'), '1 jan 1900')
		,PreventativeMedicationStartTime = nullif(Details.value('data(VTEAssessment/VTEProphylaxis/MedicationStarted_TS)[1]', 'datetime'), '1 jan 1900')		
		,PreventativeNursingActionStartDate = nullif(Details.value('data(VTEAssessment/VTEProphylaxis/NursingActionsStarted_TS)[1]', 'date'), '1 jan 1900')
		,PreventativeNursingActionStartTime = nullif(Details.value('data(VTEAssessment/VTEProphylaxis/NursingActionsStarted_TS)[1]', 'datetime'), '1 jan 1900')
		,WardCode = Locations.PASCode
		--,Details
	from
		[$(BedmanTest)].dbo.Event VTEAssessment

	inner join [$(BedmanTest)].dbo.HospSpell Spell
	on	Spell.SpellID = VTEAssessment.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on VTEAssessment.LocationID = Locations.LocationID

	where
		EventTypeID = 4
	and not exists --If we have the same event type occuring at the same time, get the latest occurence
			(
			select	
				1
			from
				[$(BedmanTest)].dbo.Event VTEAssessmentLater
			where
				EventTypeID = 4
			and VTEAssessmentLater.HospSpellID = VTEAssessment.HospSpellID
			and	VTEAssessmentLater.Details.value('data(VTEAssessment/AssessmentCompleted_TS)[1]', 'datetime')  = VTEAssessment.Details.value('data(VTEAssessment/AssessmentCompleted_TS)[1]', 'datetime')
			and	VTEAssessmentLater.ID > VTEAssessment.ID
			)
	)

select
	SourceUniqueID 
	,SourcePatientNo 
	,SourceSpellNo 
	,NoAssessmentReasonID
	,AssessmentDate 
	,AssessmentTime 
	,AtRisk
	,PreventativeMedicationStartDate 
	,PreventativeMedicationStartTime	
	,PreventativeNursingActionStartDate 
	,PreventativeNursingActionStartTime
	,WardCode 
	,InterfaceCode = 'BEDMAN'
from
	VTEAssessmentCTE




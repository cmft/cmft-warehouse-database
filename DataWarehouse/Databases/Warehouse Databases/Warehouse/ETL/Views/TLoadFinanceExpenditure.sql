﻿

CREATE view [ETL].[TLoadFinanceExpenditure]

as

select
	CensusDate
	,Division
	,Budget
	,AnnualBudget
	,Actual
	,InterfaceCode = 'ORACLE'
from
	ETL.TImportFinanceExpenditure



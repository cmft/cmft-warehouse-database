﻿

Create view [ETL].[TLoadMortalityPaediatrics] as


WITH XMLNAMESPACES (N'http://schemas.microsoft.com/office/infopath/2003/myXSD/2012-07-30T07:31:29' AS ns)

select 
	 SourceUniqueID = ReceivedForm.ID
	,ReceivedForm.FormTypeID
	
	,ReviewStatus
	,ReviewedDate = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:Header/ns:LastUpdated)[1]', 'date') 
	,ReviewedBy = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PersonCompletingThisProforma_Name)[1]', 'varchar(50)')  = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PersonCompletingThisProforma_Name)[1]', 'varchar(50)') 
		end
	,Designation = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PersonCompletingThisProforma_Designation)[1]', 'varchar(50)')  = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PersonCompletingThisProforma_Designation)[1]', 'varchar(50)') 
		end
	,Comments = 
		case 
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:Comments)[1]', 'varchar(50)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:Comments)[1]', 'varchar(50)')
		end
		
	,CasenoteNo = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:HospitalNumber)[1]', 'varchar(50)')
	,DateOfDeath = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DateOfDeath)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DateOfDeath)[1]', 'datetime')
			else null
		end	
	,AdmissionTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DateOfAdmission)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DateOfAdmission)[1]', 'datetime')
			else null
		end	
	,ClinicalSummary = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:BriefClinicalSummary)[1]', 'varchar(max)')
	
	,PlaceOfDeath = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PlaceOfDeath)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PlaceOfDeath)[1]', 'varchar(max)')
		end
	,AgeAtDeath = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AgeAtDeath)[1]', 'varchar(max)')
	
	,TransferredFromOtherHospital = 
			cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TransferredFromOtherHospital_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TransferredFromOtherHospital_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,TransferredFrom = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TransferredFromOtherHospital_YN__Y_Name)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TransferredFromOtherHospital_YN__Y_Name)[1]', 'varchar(max)')
		end
	,TransferredFromOther = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TransferredFromOtherHospital_YN__Y_Name_Other)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TransferredFromOtherHospital_YN__Y_Name_Other)[1]', 'varchar(max)')
		end
	,ClinicalDiagnosis = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ClinicalDiagnosis)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ClinicalDiagnosis)[1]', 'varchar(max)')
		end
	
	,AdmissionForTerminalCare = 
			cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmissionForTerminalCare_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmissionForTerminalCare_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
 
	,EndOfLifeCarePlan = 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmissionForTerminalCare_Y/ns:AdmissionForTerminalCare_Y__EndOfLifeCarePlan_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmissionForTerminalCare_Y/ns:AdmissionForTerminalCare_Y__EndOfLifeCarePlan_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	
	
	,AdmittedToICU = 
			cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
 
	,ICUSource = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y__Source)[1]', 'varchar(max)')
	,ICUWardEWS = 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Ward/ns:AdmittedToICU_Y_Ward__EWSYN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Ward/ns:AdmittedToICU_Y_Ward__EWSYN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,ICUWardComments = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Ward/ns:AdmittedToICU_Y_Ward__EWSComment)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Ward/ns:AdmittedToICU_Y_Ward__EWSComment)[1]', 'varchar(max)')
		end
	
	,TheatrePlanned = 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Theatre/ns:AdmittedToICU_Y_Theatre_PlannedYN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Theatre/ns:AdmittedToICU_Y_Theatre_PlannedYN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,TheatrePlannedComment = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Theatre/ns:AdmittedToICU_Y_Theatre_PlannedComment)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_Theatre/ns:AdmittedToICU_Y_Theatre_PlannedComment)[1]', 'varchar(max)')
		end
	,AEComments = 
		case 
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_AE/ns:AdmittedToICU_Y_AE_Comments)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AdmittedToICU_Y/ns:AdmittedToICU_Y_AE/ns:AdmittedToICU_Y_AE_Comments)[1]', 'varchar(max)')
		end
    ,DrugErrorIdentified = 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DrugErrorIdentified_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DrugErrorIdentified_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,DrugErrorIdentifiedComments = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DrugErrorIdentified_Comments)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DrugErrorIdentified_Comments)[1]', 'varchar(max)')
		end
	,MRSA = 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:MRSA_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:MRSA_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,CDifficile = 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CDifficile_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CDifficile_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
 	,CriticalIncidentBeforeMortalityReview = 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CriticalIncidentBeforeMortalityReview_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CriticalIncidentBeforeMortalityReview_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,CriticalIncidentBeforeMortalityReviewComments = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CriticalIncidentBeforeMortalityReview_Comments)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CriticalIncidentBeforeMortalityReview_Comments)[1]', 'varchar(max)')
		end
	
	,WithdrawalOfCare= 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit) 
	,Indication = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_Y/ns:WithdrawalOfCare_Y__Indication)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_Y/ns:WithdrawalOfCare_Y__Indication)[1]', 'varchar(max)')
		end
	,DocumentationType = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_Y/ns:WithdrawalOfCare_Y__DocumentationType)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_Y/ns:WithdrawalOfCare_Y__DocumentationType)[1]', 'varchar(max)')
		end 
	,WithdrawlOfCareByWhom = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_Y/ns:WithdrawalOfCare_Y__ByWhom)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:WithdrawalOfCare_Y/ns:WithdrawalOfCare_Y__ByWhom)[1]', 'varchar(max)')
		end
	,LeadConsultant = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:LeadConsultant_Name)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:LeadConsultant_Name)[1]', 'varchar(max)')
		end
	,LeadConsultantEasilyIdentified= 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:LeadConsultantClearlyIdentified_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:LeadConsultantClearlyIdentified_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,OtherConsultantInvolved = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:OtherConsultantsInvolved/ns:OtherConsultantInvolved)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:OtherConsultantsInvolved/ns:OtherConsultantInvolved)[1]', 'varchar(max)')
		end
	,GradeOfMostSeniorClinicianInvolved = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:GradeOfMostSeniorClinicianInvolved)[1]', 'varchar(max)')
	
	,PreviousAdmission = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PreviousAdmission_YNNN)[1]', 'varchar(max)')
	,ChronicIllness = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ChronicIllness_YNNN)[1]', 'varchar(max)')
	,ChronicIllnessSpecify = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ChronicIllness_YNNN__Y/ns:ChronicIllness_YNNN_Y__PleaseSpecify)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ChronicIllness_YNNN__Y/ns:ChronicIllness_YNNN_Y__PleaseSpecify)[1]', 'varchar(max)')
		end
	,RelevantPreviousAdmissionsAttendances = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RelevantPreviousAdmissionsAttendances)[1]', 'varchar(max)')
	,OperationsProceduresPerformed = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:OperationsProceduresPerformed)[1]', 'varchar(max)')
	,CaseReferredToCoroner= 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseReferredToCoroner_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseReferredToCoroner_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,ConsultantReferringCase = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ConsultantReferringCase_Name)[1]', 'varchar(max)')
	,CaseDiscussedWithCoroner= 
		cast(
				case 
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN)[1]', 'varchar(max)') = 'true'
					then 1
					when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN)[1]', 'varchar(max)') = 'false'
					then 0
					else null
				end
			as bit)
	,CaseDiscussedWithCoronerConsultant = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__ConsultantName)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__ConsultantName)[1]', 'varchar(max)')
		end
	,CaseDiscussedWithCoronerDocumentedInMedicalRecords = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__DocumentedInMedicalRecords_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__DocumentedInMedicalRecords_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,CaseDiscussedWithCoronerPostMortemPerformed = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__PostMortemPerformedAsOutcome_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__PostMortemPerformedAsOutcome_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,CaseDiscussedWithCoronerOutcomeAppropriate = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__OutcomeOfDiscussionConsideredAppropriate_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__OutcomeOfDiscussionConsideredAppropriate_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,CaseDiscussedWithCoronerOutcomeNotAppropriateComments = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__OutcomeOfDiscussionConsideredAppropriate_YN_N/ns:CaseDiscussedWithCoroner_YN_Y__OutcomeOfDiscussionConsideredAppropriate_YN_N_Reason)[1]', 'varchar(max)')   = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CaseDiscussedWithCoroner_YN_Y/ns:CaseDiscussedWithCoroner_YN_Y__OutcomeOfDiscussionConsideredAppropriate_YN_N/ns:CaseDiscussedWithCoroner_YN_Y__OutcomeOfDiscussionConsideredAppropriate_YN_N_Reason)[1]', 'varchar(max)')  
		end
	,DeathCertificateIssued = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DeathCertificateIssued_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DeathCertificateIssued_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,RequestForHospitalPostMortem = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RequestForHospitalPostMortem_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RequestForHospitalPostMortem_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,RequestForHospitalPostMortemMadeToParentsByWhom = 
		 case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RequestForHospitalPostMortemMadeToParentsByConsultant_YNO)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RequestForHospitalPostMortemMadeToParentsByConsultant_YNO)[1]', 'varchar(max)')
		end 
	,RequestForHospitalPostMortemMadeToParentsByOther = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RequestForHospitalPostMortemMadeToParentsByConsultant_YNO_O/ns:RequestForHospitalPostMortemMadeToParentsByConsultant_YNO_O__Other)[1]', 'varchar(max)')  = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RequestForHospitalPostMortemMadeToParentsByConsultant_YNO_O/ns:RequestForHospitalPostMortemMadeToParentsByConsultant_YNO_O__Other)[1]', 'varchar(max)') 
		end
	,PermissionForHospitalPostMortemGiven = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PermissionForHospitalPostMortemGiven_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PermissionForHospitalPostMortemGiven_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,FullPermissionForHospitalPostMortemGiven = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PermissionForHospitalPostMortemGiven_YN_Y/ns:PermissionForHospitalPostMortemGiven_YN_Y__Full_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PermissionForHospitalPostMortemGiven_YN_Y/ns:PermissionForHospitalPostMortemGiven_YN_Y__Full_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,LimitedPermissionForHospitalPostMortemGiven = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PermissionForHospitalPostMortemGiven_YN_Y/ns:PermissionForHospitalPostMortemGiven_YN_Y__Limited_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:PermissionForHospitalPostMortemGiven_YN_Y/ns:PermissionForHospitalPostMortemGiven_YN_Y__Limited_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
  ,CauseOfDeath1a = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_1a)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_1a)[1]', 'varchar(max)')
		end
	,CauseOfDeath1b = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_b)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_b)[1]', 'varchar(max)')
		end
	,CauseOfDeath1c = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_c)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_c)[1]', 'varchar(max)')
		end
	,CauseOfDeath2 = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_2)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_2)[1]', 'varchar(max)')
		end
	,CauseOfDeathAgreed = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_CauseOfDeathAgreed_YN)[1]', 'varchar(max)') = 'Yes'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_CauseOfDeathAgreed_YN)[1]', 'varchar(max)') = 'No'
				then 0
				else null
			end
		as bit)	
	,CauseOfDeathNotAgreedReason = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_CauseOfDeathAgreed_YN_N/ns:CauseOfDeath_CauseOfDeathAgreed_YN_N__Reason)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CauseOfDeath/ns:CauseOfDeath_CauseOfDeathAgreed_YN_N/ns:CauseOfDeath_CauseOfDeathAgreed_YN_N__Reason)[1]', 'varchar(max)')
		end
	,AnyFurtherPointsOfDiscussion= 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AnyFurtherPointsForDiscussion_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:AnyFurtherPointsForDiscussion_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)	
	,SignificantQuestionsWhichRemainUnanswered= 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SignificantQuestionsThatRemainUnanswered)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SignificantQuestionsThatRemainUnanswered)[1]', 'varchar(max)')
		end
	,IssuesForDiscussion= 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:IssuesForDiscussion)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:IssuesForDiscussion)[1]', 'varchar(max)')
		end
	
  	,OrganDonationDiscussed= 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:OrganDonationDiscussed_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:OrganDonationDiscussed_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)	
	,TissueDonationDiscussed= 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TissueDonationDiscussed_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TissueDonationDiscussed_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)	
	,OrgansDonated= 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:OrgansDonated_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:OrgansDonated_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)	
	,TissuesDonated= 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TissuesDonated_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:TissuesDonated_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)	
  
	,SecondaryReviewReason = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_Reason)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_Reason)[1]', 'varchar(max)')
		end
	,SecondaryReviewOutcome = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_Outcomes)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_Outcomes)[1]', 'varchar(max)')
		end
	,SecondaryReviewActions = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_Actions)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_Actions)[1]', 'varchar(max)')
		end
	,SecondaryReviewPatientCareConsultant = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_PatientCareConsultants/ns:SecondaryReview_PatientCareConsultants_Consultant)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_PatientCareConsultants/ns:SecondaryReview_PatientCareConsultants_Consultant)[1]', 'varchar(max)')
		end
	,SecondaryReviewDiscussedConsultants = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_CaseBeenDiscussedConsultants/ns:SecondaryReview_CaseBeenDiscussedConsultants_Consultant)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_CaseBeenDiscussedConsultants/ns:SecondaryReview_CaseBeenDiscussedConsultants_Consultant)[1]', 'varchar(max)')
		end
	,SecondaryReviewer = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_PersonCompletingThisProforma_Name)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_PersonCompletingThisProforma_Name)[1]', 'varchar(max)')
		end
	,SecondaryReviewerDesignation = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_PersonCompletingThisProforma_Designation)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_PersonCompletingThisProforma_Designation)[1]', 'varchar(max)')
		end
	,SecondaryReviewFuturePresentation = 
		cast(
			case
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_FuturePresentation_YN)[1]', 'varchar(max)') = 'true'
				then 1
				when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_FuturePresentation_YN)[1]', 'varchar(max)') = 'false'
				then 0
				else null
			end
		as bit)
	,SecondaryReviewFuturePresentationPersonNominated = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_FuturePresentation_YN_Y/ns:SecondaryReview_FuturePresentation_YN_Y__PersonNominated)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondaryReview/ns:SecondaryReview_FuturePresentation_YN_Y/ns:SecondaryReview_FuturePresentation_YN_Y__PersonNominated)[1]', 'varchar(max)')
		end
	
	,FirstReviewerUser = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:FirstReviewerUser)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:FirstReviewerUser)[1]', 'varchar(max)')
		end
	,SecondReviewerUser = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondReviewerUser)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:SecondReviewerUser)[1]', 'varchar(max)')
		end
	,SecondaryReviewRequired = 
		case 
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ReviewBoard1/ns:SecondaryReviewRequired_YN)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ReviewBoard1/ns:SecondaryReviewRequired_YN)[1]', 'varchar(max)')
		end
	,PersonNominatedForSecondReview = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ReviewBoard1/ns:PersonNominatedForSecondReview)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:ReviewBoard1/ns:PersonNominatedForSecondReview)[1]', 'varchar(max)')
		end 
	,DiagDiseaseGroup = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DiagDiseaseGroups/ns:DiagDiseaseGroup )[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:DiagDiseaseGroups/ns:DiagDiseaseGroup )[1]', 'varchar(max)')
		end
	,RecurringThemes = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RecurringThemes/ns:RecurringTheme)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:RecurringThemes/ns:RecurringTheme)[1]', 'varchar(max)')
		end
	,CodingCategory = 
		case
			when ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CodingCategory)[1]', 'varchar(max)') = '' then null
			else ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:CodingCategory)[1]', 'varchar(max)')
		end
	,SourcePatientNo = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:Runtime/ns:sPN)[1]', 'varchar(20)')
	,EpisodeStartTime = cast(ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:Runtime/ns:eST)[1]', 'varchar(50)') as datetime)
	,ContextCode = ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:Runtime/ns:cTX)[1]', 'varchar(20)')

from
	[$(InfoPathRepository)].InfoPath.ReceivedForms ReceivedForm

inner join [$(Information)].Information.MortalityReviewForms
on	MortalityReviewForms.MortalityReviewFormID = ReceivedForm.ID

where
	--ReceivedForm.FormTypeID = 308
	ReceivedForm.FormXML.value('(ns:InfoPathForm/ns:Header/ns:FormID)[1]', 'INT') = 308
and not exists
		(
		Select
			1
		from 
			[$(Information)].Information.MortalityReviewForms Latest
		where
			Latest.MortalityReviewFormID = MortalityReviewForms.MortalityReviewFormID
		and Latest.ID > MortalityReviewForms.ID
		)
﻿

CREATE view [ETL].[TLoadK2CTGStaffGroups]

as

/***************************************************************************
**	20140708	RR	A view containing 1 row per staffID, with hierarchy, Cons - Dr - Midwife - Student Midwife
**
****************************************************************************/

select
	 StaffID 
	,Initials
	,StaffName
	,StaffGroup
from
	(
	select
		StaffID = StaffId
		,Initials
		,StaffName
		,StaffGroup
		,SeqNo = row_number ( ) over (partition by StaffId order by TimeEntered desc)
	from 

	(
		select  
			StaffId
			,Initials 
			,StaffName = (FirstName + ' ' + SurName)
			,TimeEntered
			,StaffGroup = 'Consultant'
		from 
			[$(K2)].dbo.ListStaffMemberHistory Cons
		where 
			StaffType = 3
		--30
		union
		select  
			StaffId
			,Initials
			,StaffName = (FirstName + ' ' + SurName)
			,TimeEntered
			,StaffGroup = 'Doctor'
		from 
			[$(K2)].dbo.ListStaffMemberHistory Doctor
		where 
			StaffType = 6
		and not exists
			(select 1
			from [$(K2)].dbo.ListStaffMemberHistory Dr
			where Dr.StaffId = Doctor.StaffId
			and StaffType = 3
			)
		--142
		union
		select  
			StaffId
			,Initials
			,StaffName = (FirstName + ' ' + SurName)
			,TimeEntered
			,StaffGroup = 'Midwife'
		from 
			[$(K2)].dbo.ListStaffMemberHistory Midwife
		where 
			StaffType in (4,7,8,14)
		and not exists
			(select 1
			from [$(K2)].dbo.ListStaffMemberHistory MW
			where Midwife.StaffId = MW.StaffId
			and StaffType in (3,6)
			)
		--400
		union
		select  
			StaffId
			,Initials
			,StaffName = (FirstName + ' ' + SurName)
			,TimeEntered
			,StaffGroup = 'StudentMidwife'
		from 
			[$(K2)].dbo.ListStaffMemberHistory StuMid
		where 
			StaffType = 9
		and not exists
			(select 1
			from [$(K2)].dbo.ListStaffMemberHistory SMW
			where StuMid.StaffId = SMW.StaffId
			and StaffType in (3,6,4,7,8,14)
			)
	--107	
		)
		Staff
	)
	StaffGroup
where SeqNo = 1


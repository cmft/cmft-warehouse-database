﻿CREATE view [ETL].[TLoadBedComplementWardBed]

as 

select
	 WardID
	,InpatientBeds
	,DaycaseBeds
	,SingleRoomBeds
	,Comment
	,StartDate
from 
	ETL.TImportBedComplementWardBed

﻿



CREATE View [ETL].[TLoadMortalityNeonatology]

as

/*==============================================================================================
Description: Mortality Review application XML shredding - Neonatology

When		Who				What
23/09/2015	Rachel Royston	Initial Coding

===============================================================================================*/


Select
	SourceUniqueID
	,SourcePatientNo
	,EpisodeStartTime
	,FormTypeCode
	,MortalityReviewAdded
	,ReviewStatusCode
	,MRSA 
	,MedicationErrorsReported 
	,CriticalIncident 
	,CauseOfDeath1 
	,CauseOfDeath2 
	,IntensiveCareWithdrawn 
	,LOTADNR 
	,ConsultantDiscussion 
	,DeathExpected 
	,DeathPreventable
	,PalliativeCare 
	,PostMortem 
	,BereavementAppointmentDate 
	,AppointmentAttended 
	,FollowUp 
	,AdditionalInformation 
	,SignificantIssues 
	,AreasOfNotablePractice 
	,AreasForFurtherDevelopment 
	,AdditionalInstructions 
	,SourceOfAdmission 
	,Gestation 
	,BirthWeight
	,DelayInDiagnosis 
	,DelayInDelivering
	,PoorComms 
	,OrganisationalFailure 
	,AreasofConcernND 
	,AreasofConcernContributed 
	,DoneDifferently 
	,GoodQualityDetails
	,DocStandards 
	,FurtherComments 
	,ClassificationScore 
	,CEFactors 
	,PrimaryReviewerID 
	,PrimaryReviewerName
	,PRAssignedTime 
	,PRCompletedTime
	,SecondaryReviewerID 
	,SecondaryReviewConsultantList 			
	,SecondaryReviewConsultantDiscussion 
	,SRAssignedTime 
	,SRCompletedTime 
	,SubmittedDiagnostics 
	,SubmittedRecurringThemes
	,ContextCode
from
	(
	select
		 SourceUniqueID = MortalityReviewBase.ID
		,SourcePatientNo
		,EpisodeStartTime
		,FormTypeCode
		,MortalityReviewAdded = cast(dateMortalityReviewAdded as date)
		,ReviewStatusCode = coalesce(ReviewStatusCode,0)
			
		,MRSA = 
			CAST(
				case
					when FormData.XMLData.value('(/FormData/Sections/ClinicalRisk/MRSA)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ClinicalRisk/MRSA)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,MedicationErrorsReported = 
			cast(
				 case 
					when FormData.XMLData.value('(/FormData/Sections/ClinicalRisk/MedicationErrorsReported)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ClinicalRisk/MedicationErrorsReported)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,CriticalIncident = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ClinicalRisk/CriticalIncident)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ClinicalRisk/CriticalIncident)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,CauseOfDeath1 = FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/CauseOfDeath1)[1]', 'varchar(10)') 
		,CauseOfDeath2 = FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/CauseOfDeath2)[1]', 'varchar(10)') 

		,IntensiveCareWithdrawn = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/IntensiveCareWithdrawn)[1]', 'varchar(10)')  = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/IntensiveCareWithdrawn)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,LOTADNR = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/LOTADNR)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/LOTADNR)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,ConsultantDiscussion = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/ConsultantDiscussion)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/ConsultantDiscussion)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,DeathExpected = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/DeathExpected)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/DeathExpected)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,DeathPreventable = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/DeathPreventable)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/DeathPreventable)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,PalliativeCare = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/PalliativeCare)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/PalliativeCare)[1]', 'varchar(10)') = 'Yes' then 1
					else null 
				end
			as bit)

		,PostMortem = 
			CAST(	
				case 
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/PostMortem)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/PostMortem)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,BereavementAppointmentDate = 
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/BereavementAppointmentDate)[1]', 'varchar(4000)') 
						is null then null
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/BereavementAppointmentDate)[1]', 'varchar(4000)') 
						= '' then null
					else left(FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/BereavementAppointmentDate)[1]', 'varchar(4000)'),10)
				end

		,AppointmentAttended = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/AppointmentAttended)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/AppointmentAttended)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,FollowUp = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/FollowUp)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/PrimaryReviewDetails/FollowUp)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,AdditionalInformation = FormData.XMLData.value('(/FormData/Sections/ExtraInfo/ClinSummary)[1]', 'varchar(max)') 
		,SignificantIssues = FormData.XMLData.value('(/FormData/Sections/ExtraInfo/SignificantIssues)[1]', 'varchar(100)') 
		,AreasOfNotablePractice = FormData.XMLData.value('(/FormData/Sections/ExtraInfo/AreasOfNotablePractice)[1]', 'varchar(max)') 
		,AreasForFurtherDevelopment = FormData.XMLData.value('(/FormData/Sections/ExtraInfo/AreasForFurtherDevelopment)[1]', 'varchar(max)') 
		,AdditionalInstructions = FormData.XMLData.value('(/FormData/Sections/ExtraInfo/AdditionalInstructions)[1]', 'varchar(max)') 
		,SourceOfAdmission = FormData.XMLData.value('(/FormData/Sections/NeonatalPatientInfo/SourceOfAdmission)[1]', 'varchar(max)') 
		,Gestation = FormData.XMLData.value('(/FormData/Sections/NeonatalPatientInfo/Gestation)[1]', 'varchar(max)') 
		,BirthWeight = FormData.XMLData.value('(/FormData/Sections/NeonatalPatientInfo/BirthWeight)[1]', 'varchar(max)') 

		,DelayInDiagnosis = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDiagnosis)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDiagnosis)[1]', 'varchar(10)')  = 'Yes' then 1
					else null
				end
			as bit)

		,DelayInDelivering = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDelivering)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/DelayInDelivering)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,PoorComms = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/PoorComms)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/PoorComms)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,OrganisationalFailure = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/OrganisationalFailure)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/OrganisationalFailure)[1]', 'varchar(10)')  = 'Yes' then 1
					else null
				end
			as bit)

		,AreasofConcernND = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernND)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernND)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,AreasofConcernContributed = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernContributed)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/AreasofConcernContributed)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,DoneDifferently = 
			CAST(	
				case
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/DoneDifferently)[1]', 'varchar(10)') = 'No' then 0
					when FormData.XMLData.value('(/FormData/Sections/ReviewCase/DoneDifferently)[1]', 'varchar(10)') = 'Yes' then 1
					else null
				end
			as bit)

		,GoodQualityDetails = FormData.XMLData.value('(/FormData/Sections/ReviewCase/GoodQualityDetails)[1]', 'varchar(max)') 
		,DocStandards = FormData.XMLData.value('(/FormData/Sections/ReviewCase/DocStandards)[1]', 'int') 
		,FurtherComments = FormData.XMLData.value('(/FormData/Sections/ReviewCase/FurtherComments)[1]', 'varchar(max)') 
		,ClassificationScore = FormData.XMLData.value('(/FormData/Sections/Classification/ClassificationScore)[1]', 'int') 

		,CEFactors = 
			case
			when FormData.XMLData.value('(/FormData/Sections/CEFactors)[1]', 'varchar(10)') = '' then null
			else FormData.XMLData.value('(/FormData/Sections/CEFactors)[1]', 'varchar(10)') 
			end

		,PrimaryReviewerID = FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/@ID)[1]', 'int') 
		,PrimaryReviewerName = FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PrimaryReviewerName)[1]', 'varchar(50)') 

		,PRAssignedTime =
				case
				when FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRAssignedDate)[1]', 'varchar(4000)')  
					= '' then null
				else cast(left(FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRAssignedDate)[1]', 'varchar(4000)')  , 19) as datetime2(0))
				end

		,PRCompletedTime = 
				case
				when FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRCompletedDate)[1]', 'varchar(4000)') 
					= '' then null
				else cast(left(FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRCompletedDate)[1]', 'varchar(4000)') , 19) as datetime2(0))
				end

		,SecondaryReviewerID = FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/@ID)[1]', 'int') 

		,SecondaryReviewConsultantList = 
			case
				when FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/ListConsultantsInvolved)[1]', 'varchar(50)')  = '' then null
				else FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/ListConsultantsInvolved)[1]', 'varchar(50)') 
			end
				
		,SecondaryReviewConsultantDiscussion = 
			case
				when FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/ListConsultantsDiscussed)[1]', 'varchar(50)')  = '' then null
				else FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/ListConsultantsDiscussed)[1]', 'varchar(50)') 
			end

		,SRAssignedTime =
				case
					when FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRAssignedDate)[1]', 'varchar(4000)')  
						= '' then null
					else cast(left(FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRAssignedDate)[1]', 'varchar(4000)')  , 19) as datetime2(0))
				end

		,SRCompletedTime = 
				case
					when FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRCompletedDate)[1]', 'varchar(4000)') 
						= '' then null
					else cast(left(FormData.XMLData.value('(/FormData/Sections/Reviews/SecondaryReview/SRCompletedDate)[1]', 'varchar(4000)') , 19) as datetime2(0))
				end

		,SubmittedDiagnostics = 
			case 
				when FormData.XMLData.value('(/FormData/ReviewBoard/SubmittedDiagnostics)[1]', 'varchar(50)')  = '' then null
				else FormData.XMLData.value('(/FormData/ReviewBoard/SubmittedDiagnostics)[1]', 'varchar(50)') 
			end

		,SubmittedRecurringThemes = 
			case
				when FormData.XMLData.value('(/FormData/ReviewBoard/SubmittedRecurringThemes)[1]', 'varchar(50)') = '' then null
				else FormData.XMLData.value('(/FormData/ReviewBoard/SubmittedRecurringThemes)[1]', 'varchar(50)') 		
			end

		,ContextCode
	from
		[$(MortalityReview)].dbo.MortalityReviewBase

	inner join Mortality.FormType
	on FormType.FormTypeCode = MortalityReviewBase.FormTypeID
	
	left join [$(MortalityReview)].dbo.FormData 
	on FormData.MortalityReviewBaseID = MortalityReviewBase.ID
			
	left join Mortality.ReviewStatus	
	on ReviewStatus = FormData.Status

	where
		FormType.FormType = 'Neonatal'
	and cast(left(FormData.XMLData.value('(/FormData/Sections/Reviews/PrimaryReview/PRAssignedDate)[1]', 'varchar(4000)')  , 19) as datetime2(0)) >= '1 Aug 2015'
	and cast(dateMortalityReviewAdded as date) between 
				coalesce(FormType.FromDate,getdate())
			and Coalesce(FormType.ToDate,getdate())
	) MortalityReviewNeonatal
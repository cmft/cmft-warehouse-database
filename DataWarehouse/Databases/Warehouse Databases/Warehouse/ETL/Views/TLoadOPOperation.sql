﻿CREATE view [ETL].[TLoadOPOperation] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,DoctorCode
	,OPSourceUniqueID
from
	ETL.TImportOPOperation

﻿
create view ETL.TLoadG2SpeechDocument

as

select
	--DocumentCode int, 
	SourceUniqueID
	,[ExternIdentification]
	,[ConnectionInfoID]
	,[CreationDate]
	,[Document]
	,[Length]
	,[InterfaceCode]
	,[DocumentTypeCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[ExporterCode]
	,[AuthoriserCode]
	,[ExternTextCode]
	,[WorktypeCode]
	,[BatchNumber]
	,[StatusCode]
	,[CheckOutStatus]
	,[Priority]
	,[WorkStationCode]
	,[RecRate]
	,[MatterNumber]
	,[Reserved02]
	,[InputDevice]
	,[Supervisor]
	,[Reserved05]
	,[UserDefined01]
	,[UserDefined02]
	,[UserDefined03]
	,[UserDefined04]
	,[UserDefined05]
	,[ContextCode]
	,[NextStatusCode]
	,[SoundPos]
	,[SrIdentification]
	,[Reserved01]
	,[Reserved03]
	,[Reserved04]
	,[SupervisorCode]
	,[CurrentUserCode]
	,[CurrentWorkstationCode]
	,[CurrentApplicationCode]
	,[CorrectionDate]
	,[Reserved06]
	,[Reserved07]
	,[Reserved08]
	,[Reserved09]
	,[Reserved10]
	,[AuthorisationDate]
	,[CorRate]
from
	[ETL].[TImportG2SpeechDocument] 
	
--select * from ETL.TLoadG2SpeechDocument


﻿







CREATE view [ETL].[TLoadMortalityCriticalCare] as

WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' AS ns)

select
	 SourceUniqueID = ReceivedForm.ID
	,FormTypeID = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Header/ns:FormID)[1]', 'INT')
	,ReviewStatus
	,ReviewedDate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfReview)[1]', 'date') 
	,ReviewedBy = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:Reviewer)[1]', 'varchar(50)')     
	,CasenoteNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:HospitalNumber)[1]', 'varchar(50)')
	,NHSNumber = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:NHSNumber)[1]', 'varchar(50)')

	,Age =
		case
		when isnumeric(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:AgeAtDeath)[1]', 'varchar(20)')) = 1
		then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:AgeAtDeath)[1]', 'int')
		else null
		end

	,AdmissionTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateTimeOfAdmission)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateTimeOfAdmission)[1]', 'datetime')
			else null
		end
	,DateOfDeath= 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateTimeOfDeath)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateTimeOfDeath)[1]', 'datetime')
			else null
		end
	,PlaceOfDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:PlaceOfDeath)[1]', 'varchar(50)')  
	,FirstConsultantReview = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:FirstConsultantReview)[1]', 'varchar(50)')  
	,SpecialtyGroup = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:SpecialtyGroup)[1]', 'varchar(50)')  
	,PatientName = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:PatientName)[1]', 'varchar(255)') 
	,PrimaryDiagnosis = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PrimaryDiagnosis)[1]', 'varchar(255)')  
	,ConfirmedPrimaryDiagnosis = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ConfirmedPrimaryDiagnosis)[1]', 'varchar(255)')  
	,CauseOfDeath1a = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CauseOfDeath1a)[1]', 'varchar(50)')  
	,CauseOfDeath1b = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CauseOfDeath1b)[1]', 'varchar(50)')  
	,CauseOfDeath1c = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CauseOfDeath1c)[1]', 'varchar(50)')  
	,CauseOfDeath2 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CauseOfDeathII)[1]', 'varchar(50)')  
	,CauseOfDeathComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CauseOfDeathComments)[1]', 'varchar(max)')  
	,CodedDiagnosis = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CodedDiagnosis)[1]', 'varchar(50)')  
	,AgreeWithStatedCauseOfDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:AgreeWithStatedCauseOfDeath)[1]', 'bit')  
	,HospitalPM = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:HospitalPostmortemYN)[1]', 'bit')  
	,CoronerInformed = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CoronerInformed)[1]', 'bit')  
	,CoronerPM = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CoronerPM)[1]', 'bit')  
	,CoexistingFactor = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CoexistingFactor)[1]', 'varchar(50)')    
	,TimeClinicalDecisionToAdmitMade = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:StartOfAdmission/ns:TimeClinicalDecisionToAdmitMade)[1]', 'varchar(50)')
	,TimePatientAdmittedToCriticalCare = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:StartOfAdmission/ns:TimePatientAdmittedToCriticalCare)[1]', 'varchar(50)')  
	,AdmissionStickerUsed = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:StartOfAdmission/ns:AdmissionStickerUsed)[1]', 'bit')  
	,TimeToConsultantReviewComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:TimeToConsultantReviewC)[1]', 'varchar(max)')  
	,EvidenceOfClearManagementPlan = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:EvidenceOfClearManagementPlanYN)[1]', 'bit')  
	,EvidenceOfClearManagementPlanComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:EvidenceOfClearManagementPlanC)[1]', 'varchar(50)')  
	,EssentialInvestigationsObtainedWithoutDelay = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:EssentialInvestigationsObtainedWithoutDelayYN)[1]', 'bit')  
	,EssentialInvestigationsObtainedWithoutDelayComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:EssentialInvestigationsObtainedWithoutDelayC)[1]', 'varchar(max)')  
	,InitialManagementStepsAppropriateAndAdequate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:InitialManagementStepsAppropriateAndAdequateYN)[1]', 'bit')  
	,InitialManagementStepsAppropriateAndAdequateComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:InitialManagementStepsAppropriateAndAdequateC)[1]', 'varchar(max)')  
	,OmissionsInInitialManagement = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:OmissionsInInitialManagementYN)[1]', 'bit')  
	,OmissionsInInitialManagementComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:First24Hrs/ns:OmissionsInInitialManagementC)[1]', 'varchar(max)')  
	,PeriodAtrialFibrilation = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:PeriodAtrialFibrilationYN)[1]', 'bit')  
	,PeriodAtrialFibrilationComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:PeriodAtrialFibrilationC)[1]', 'varchar(max)')  
	,AtrialFibrilationAppropriatelyManaged = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:AtrialFibrilationAppropriatelyManagedYN)[1]', 'bit')  
	,AtrialFibrilationAppropriatelyManagedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:AtrialFibrilationAppropriatelyManagedC)[1]', 'varchar(max)')  
	,EvidenceOfIschemiaInGut = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:EvidenceOfIschemiaInGutYN)[1]', 'bit')  
	,EvidenceOfIschemiaInGutComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:EvidenceOfIschemiaInGutC)[1]', 'varchar(max)')  
	,TherapeuticAntiCoagulation = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:TherapeuticAntiCoagulationYN)[1]', 'bit')  
	,TherapeuticAntiCoagulationComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:TherapeuticAntiCoagulationC)[1]', 'varchar(max)')  
	,CriticalCareRoundsDocumentedEveryDay = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:CriticalCareRoundsDocumentedEveryDayYN)[1]', 'bit')  
	,CriticalCareRoundsDocumentedEveryDayComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DuringAdmission/ns:CriticalCareRoundsDocumentedEveryDayC)[1]', 'varchar(max)')  
	,AnyRecognisableMedicationErrors = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Medication/ns:AnyRecognisableMedicationErrorsYN)[1]', 'bit')  
	,AnyRecognisableMedicationErrorsComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Medication/ns:AnyRecognisableMedicationErrorsC)[1]', 'varchar(max)')  
	,DelayInDiagnosis = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:DelayInDiagnosisYN)[1]', 'bit')  
	,DelayInDiagnosisComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:DelayInDiagnosisC)[1]', 'varchar(max)')  
	,DelayInDeliveringCare = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:DelayInDeliveringCareYN)[1]', 'bit')  
	,DelayInOfDeliveringCareComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:DelayInOfDeliveringCareC)[1]', 'varchar(max)')  
	,PoorCommunication = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:PoorCommunicationYN)[1]', 'bit')  
	,PoorCommunicationComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:PoorCommunicationC)[1]', 'varchar(max)')  
	,AdverseEvents = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:AdverseEventsYN)[1]', 'bit')  
	,AdverseEventsComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:AdverseEventsC)[1]', 'varchar(max)')  
	,AnythingCouldBeDoneDifferently = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:AnythingCouldBeDoneDifferentlyYN)[1]', 'bit')  
	,AnythingCouldBeDoneDifferentlyComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:AnythingCouldBeDoneDifferentlyC)[1]', 'varchar(max)')  
	,NotableGoodQualityCare = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:NotableGoodQualityCare)[1]', 'varchar(max)')  
	,DocumentationIssues = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:DocumentationIssuesYN)[1]', 'bit')  
	,DocumentationIssuesComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:DocumentationIssuesC)[1]', 'varchar(max)')  
	,EndOfLifeCareLCPInPatientNotes = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:EOLC_LCPInPatientNotesYN)[1]', 'bit')  
	,EndOfLifeCareLCPInPatientNotesComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:EOLC_LCPInPatientNotesC)[1]', 'varchar(max)')  
	,EndOfLifeCarePreferredPlaceOfDeathRecorded = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:EOLC_PreferredPlaceOfDeathRecordedYN)[1]', 'bit')  
	,EndOfLifeCarePreferredPlaceOfDeathRecordedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:EOLC_PreferredPlaceOfDeathRecordedC)[1]', 'varchar(max)')  
	,EndOfLifeCareWishToDieOutsideHospitalExpressed = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:EOLC_WishToDieOutsideHospitalExpressedYN)[1]', 'bit')  
	,EndOfLifeCareWishToDieOutsideHospitalExpressedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Summary/ns:EOLC_WishToDieOutsideHospitalExpressedC)[1]', 'varchar(max)')  
	,LessonsLearned = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:LessonsLearned)[1]', 'varchar(max)')  
	,ActionPlan = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ActionPlan)[1]', 'varchar(max)')  

	,DateOfActionPlanReview =
		case
		when ISDATE(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DateOfActionPlanReview)[1]', 'varchar(50)')) = 1
		then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DateOfActionPlanReview)[1]', 'datetime')
		else null
		end

	,ClassificationGrade = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClassificationGrade)[1]', 'int')  
	,SourcePatientNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:sPN)[1]', 'varchar(20)')  
	,EpisodeStartTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]', 'datetime')
			else null
		end
	,ContextCode
from
	[$(InfoPathRepository)].InfoPath.ReceivedForms ReceivedForm

inner join
	[$(Information)].Information.MortalityReviewForms
on	MortalityReviewForms.MortalityReviewFormID = ReceivedForm.ID

where
	ReceivedForm.FormTypeID = 307 --Critical Care Mortality Review










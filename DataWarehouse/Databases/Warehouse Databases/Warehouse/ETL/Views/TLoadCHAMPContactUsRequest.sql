﻿






CREATE view [ETL].[TLoadCHAMPContactUsRequest] as

/* 
==============================================================================================
Description: Parents contact the CHAMP team by filling in a form on CHAMP web page.

Notes:	Added extra email columns to split [EmailFullContent] into separate fields. No time to
		do that just now so set fields to NULL.

When		Who			What
23/02/2015	Paul Egan	Initial coding
===============================================================================================
*/
select 
	 CHAMPContactUsRequestID = id
	,ContactUsRequestTime = cast(timeOfContactUsRequest as datetime2(0))
	,EmailAddressTo = email
	,EmailAddressFrom = null
	,EmailFromName = null
	,EmailChildName = null
	,EmailRelationshipToChild = null
	,EmailMessage = null
	,EmailFullContent = content
from 
	ETL.TImportCHAMPContactUsRequest




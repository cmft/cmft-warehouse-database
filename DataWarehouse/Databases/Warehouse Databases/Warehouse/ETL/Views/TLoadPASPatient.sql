﻿




CREATE view [ETL].[TLoadPASPatient] 

as

select
	SourcePatientNo
	,DateOfBirth
	,DeathIndicator
	,DateOfDeath
	,SexCode
	,MaritalStatusCode
	,Surname
	,Forenames
	,Title
	,NextOfKin
	,EthnicOriginCode
	,ReligionCode
	,PreviousSurname
	,NHSNumber
	,NHSNumberStatusId
	,DOR
	,DistrictNo
	,MilitaryNo
	,AddressLine1
	,AddressLine2
	,AddressLine3
	,AddressLine4
	,Postcode
	,RegisteredGpCode
	,RegisteredGdpCode
	,MRSAFlag
	,MRSADate
	,Modified
	,HomePhone
	,MobilePhone
	,WorkPhone
	,AEAttendanceToday
from
	ETL.TImportPASPatient






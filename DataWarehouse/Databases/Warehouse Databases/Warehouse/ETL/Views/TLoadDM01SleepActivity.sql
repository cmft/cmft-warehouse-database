﻿
CREATE VIEW [ETL].[TLoadDM01SleepActivity] 
AS

	/******************************************************************************
	**  Name: ETL.TLoadDM01SleepActivity
	**  Purpose: 
	**
	**  Import procedure view for DM01 Sleep Activity table rows 
	**  This view is used for Waiting List and Encounter table population
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**  IMPORTANT NOTE - Ensure you use a SET DATEFORMAT dmy in the calling session, otherwise the ISDATE
	**                   function will not interpret a valid correctly
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 06.11.12    MH         Created
	** 08.01.13    MH         Adjusted the Exclusion from WaitingList derivation to remove Cancelled
	**                        appts from [SS Completed] column
	******************************************************************************/

	SELECT 
		 SourceUniqueId			= TImportDM01SleepActivity.[Surname] + '||' + TImportDM01SleepActivity.[First Name] + '||' + TImportDM01SleepActivity.[Appointment] + '||' + TImportDM01SleepActivity.[Date rec.]
		,SourcePatientNo		= PATIENTNO.SourcePatientNo
		,CaseNoteNumber			= PATIENTNO.CaseNoteNumber
		,ConsultantCode			= TImportDM01SleepActivity.Consultant
		,ReceivedDate			= CASE
									WHEN ISDATE(TImportDM01SleepActivity.[Date rec.]) = 1 THEN CAST(TImportDM01SleepActivity.[Date rec.] AS DATETIME)
									ELSE NULL
								  END
		,AppointmentDate 		= CASE
									WHEN ISDATE(TImportDM01SleepActivity.[Appointment]) = 1 THEN CAST(TImportDM01SleepActivity.[Appointment] AS DATETIME)
									ELSE CAST('1900-01-01' AS DATE)
								  END
		,Completed				= TImportDM01SleepActivity.[SS completed]
		,Attendance				= TImportDM01SleepActivity.[Attended/dna]
		,IsExcludedWaitingList	= CASE
									WHEN	TImportDM01SleepActivity.[SS completed] LIKE ('%COMP%')
										OR	(
													LEFT(LTRIM(TImportDM01SleepActivity.[SS completed]),4) = 'CANC'
												AND TImportDM01SleepActivity.[SS completed] NOT LIKE ('%CANC BY PT%')
												AND TImportDM01SleepActivity.[SS completed] NOT LIKE ('%CANC BY PATIENT%')
											)
										OR	TImportDM01SleepActivity.[SS completed] LIKE ('%DISCH%')
										OR	TImportDM01SleepActivity.[SS completed] LIKE ('%NOT REQ%')
										OR	TImportDM01SleepActivity.[Attended/dna] LIKE ('%PL%')
										OR	TImportDM01SleepActivity.[Attended/dna] LIKE ('%YES%')
									THEN 'Y'
									ELSE 'N'
								  END
		,IsCancelledByPatient	= CASE
									WHEN	TImportDM01SleepActivity.[Attended/dna] LIKE '%PT CANC%'
										OR	TImportDM01SleepActivity.[Attended/dna] LIKE '%CANC BY PT%'
										OR	TImportDM01SleepActivity.[Attended/dna] LIKE '%CANC BY PATIENT%'
										OR	TImportDM01SleepActivity.[Attended/dna] LIKE '%CANC UNWELL%'
										OR	TImportDM01SleepActivity.[Attended/dna] LIKE '%CANC ILL%'
										OR	TImportDM01SleepActivity.[SS completed] LIKE ('%CANC BY PT%')
										OR	TImportDM01SleepActivity.[SS completed] LIKE ('%CANC BY PATIENT%')
									THEN 'Y'
									ELSE 'N'
								  END
		,IsCancelledAndRebooked	= CASE
									WHEN LEFT(LTRIM(TImportDM01SleepActivity.[Attended/dna]), 3) IN ('CRB','DNA','DNS')
									THEN 'Y'
									ELSE 'N'
								  END
		,IsExcludedActivity		= CASE
									WHEN	LEFT(LTRIM(TImportDM01SleepActivity.[Attended/dna]),1) <> 'Y'
										OR	TImportDM01SleepActivity.[Attended/dna] LIKE '%CR%'
										OR	TImportDM01SleepActivity.[SS completed] LIKE ('%CANC%')
									THEN 'Y'
									ELSE 'N'
								  END
		,PCTCode				= PATIENTDETAILS.PCTCode

	FROM
		TImportDM01SleepActivity
		OUTER APPLY [Utility].[fn_GetPatientFromCaseNote](TImportDM01SleepActivity.[Hospital No.]) PATIENTNO
		OUTER APPLY [Utility].[fn_GetPatientDetails](PATIENTNO.SourcePatientNo) PATIENTDETAILS

	WHERE
			TImportDM01SleepActivity.[Surname] <> 'Surname'
		AND	TImportDM01SleepActivity.[Hospital No.] IS NOT NULL


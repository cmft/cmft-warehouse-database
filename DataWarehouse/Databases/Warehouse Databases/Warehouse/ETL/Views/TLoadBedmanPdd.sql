﻿

CREATE VIEW [ETL].[TLoadBedmanPdd] as


/****** Script for SelectTopNRows command from SSMS  ******/

SELECT   
  	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,CasenoteNumber
	,AdmissionDate
	,AdmissionTime
	,WardTypeCode
	,DischargeDate
	,DischargeTime
	,SpecialtyCode
	,ConsultantCode
	,InterfaceCode
	,PddSequence
	,Pdd
	,PddComment
	,PddEntered
	,PddReason
	,PddPasFlag
	,PddEnteredWithinTarget
	,ExpectedLOS
	,Created
	,Updated
	,ByWhom
FROM 
	ETL.TImportBedmanPdd 



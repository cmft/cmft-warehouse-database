﻿










CREATE view [ETL].[TLoadPEXOnlineResponse]

as

select
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID = case
					when AnswerID = 0
					then null
					else AnswerID
				end	
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,WardID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
from
	ETL.TImportPEXOnlineResponse


left join
	PEX.KioskOnlinePostcardTabletWard
on	TImportPEXOnlineResponse.LocationID = KioskOnlinePostcardTabletWard.WardID	











﻿

CREATE view [ETL].[TLoadRenalPatientLabPanelIronStudies] as

select
	SourceUniqueID = cast(ObjectID as varchar(100))
	,PatientObjectID
	,PatientFullName = PatientFullName collate Latin1_General_CI_AS
	,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
	,LabTestObjectID
	,LabTestDate = LabTestDate_Local
	,LabTestQualifier = LabTestQualifier collate Latin1_General_CI_AS
	,LabTestStatus = LabTestStatus collate Latin1_General_CI_AS
	,percentHypoNum
	,percentHypoStr = percentHypoStr collate Latin1_General_CI_AS
	,percentHypoFlg = percentHypoFlg collate Latin1_General_CI_AS
	,TransferrinSaturationNum
	,TransferrinSaturationStr = TransferrinSaturationStr collate Latin1_General_CI_AS
	,TransferrinSaturationFlg = TransferrinSaturationFlg collate Latin1_General_CI_AS
	,CHrNum
	,CHrStr = CHrStr collate Latin1_General_CI_AS
	,CHrFlg = CHrFlg collate Latin1_General_CI_AS
	,FerritinNum
	,FerritinStr = FerritinStr collate Latin1_General_CI_AS
	,FerritinFlg = FerritinFlg collate Latin1_General_CI_AS
	,FolicAcidNum
	,FolicAcidStr = FolicAcidStr collate Latin1_General_CI_AS
	,FolicAcidFlg = FolicAcidFlg collate Latin1_General_CI_AS
	,HctNum
	,HctStr = HctStr collate Latin1_General_CI_AS
	,HctFlg = HctFlg collate Latin1_General_CI_AS
	,HgbNum
	,HgbStr = HgbStr collate Latin1_General_CI_AS
	,HgbFlg = HgbFlg collate Latin1_General_CI_AS
	,IronNum
	,IronStr = IronStr collate Latin1_General_CI_AS
	,IronFlg = IronFlg collate Latin1_General_CI_AS
	,TIBCNum
	,TIBCStr = TIBCStr collate Latin1_General_CI_AS
	,TIBCFlg = TIBCFlg collate Latin1_General_CI_AS
	,TransferrinNum
	,TransferrinStr = TransferrinStr collate Latin1_General_CI_AS
	,TransferrinFlg = TransferrinFlg collate Latin1_General_CI_AS
	,UIBCNum
	,UIBCStr = UIBCStr collate Latin1_General_CI_AS
	,UIBCFlg = UIBCFlg collate Latin1_General_CI_AS
	,VitB12Num
	,VitB12Str = VitB12Str collate Latin1_General_CI_AS
	,VitB12Flg = VitB12Flg collate Latin1_General_CI_AS
	,HgbX3Num
	,HgbX3Str = HgbX3Str collate Latin1_General_CI_AS
	,HgbX3Flg = HgbX3Flg collate Latin1_General_CI_AS
	,RBCFolateNum
	,RBCFolateStr = RBCFolateStr collate Latin1_General_CI_AS
	,RBCFolateFlg = RBCFolateFlg collate Latin1_General_CI_AS
from ETL.[TLoadRenalPatientLabPanelIronStudiesBase]


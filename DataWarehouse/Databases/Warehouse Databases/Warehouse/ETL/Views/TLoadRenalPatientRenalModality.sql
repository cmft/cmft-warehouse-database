﻿
CREATE view [ETL].[TLoadRenalPatientRenalModality] as

select

	SourceUniqueID = cast(ObjectID as varchar(100))
	,PatientObjectID
	,PatientFullName = PatientFullName collate Latin1_General_CI_AS
	,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
	,StartDate = StartDate_Local
	,StopDate = StopDate_Local
	,ModalityCode = ModalityCode collate Latin1_General_CI_AS
	,ModalitySettingCode = ModalitySettingCode collate Latin1_General_CI_AS
	,DialysisProviderCode = cast(DialysisProviderObjectID as varchar(100))
	,EventDetailCode = EventDetailCode collate Latin1_General_CI_AS
	,InformationSourceCode = InformationSourceCode collate Latin1_General_CI_AS
	,ReasonForChangeCode = ReasonForChangeCode collate Latin1_General_CI_AS
	,Notes = Notes collate Latin1_General_CI_AS
	,IsPrimary
	,AgeAtEvent

from ETL.[TLoadRenalPatientRenalModalityBase] B




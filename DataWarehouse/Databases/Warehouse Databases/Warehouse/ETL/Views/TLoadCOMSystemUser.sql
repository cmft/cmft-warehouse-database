﻿CREATE view ETL.TLoadCOMSystemUser as

select
	 SystemUserID = [TISID]
	--,[FILE_NAME]
	--,[TOTAL_ROW_COUNT]
	--,[ROW_IDENTIFIER]
	,SystemUserRefno = [USERS_REFNO]
	--,[PROCA_REFNO]
	,SystemUserCode = [CODE]
	,SystemUser = [USER_NAME]
	,Department = [DEPARTMENT]
	,Email = [EMAIL]
	--,[EVENTS_EXIST_FLAG]
	--,[USER_CREATE]
	--,[USER_MODIF]
	--,[CREATE_DTTM]
	--,[MODIF_DTTM]
	--,[EXTERNAL_KEY]
	,ArchiveCode = [ARCHV_FLAG]
	--,[STRAN_REFNO]
	--,[ALTER_DTTM]
	--,[LAST_LOGON_DTTM]
	--,[LOGGED_ON]
	--,[LOCKED_FLAG]
	--,[LOCKED_COMMENTS]
	--,[CHANGE_PASSWORD_FLAG]
	--,[ACLEV_REFNO]
	--,[NHS_USER_FLAG]
	--,[OWNER_HEORG_REFNO]
	--,[TIS_FLAG]
	--,[load_datetime]
from
	[$(ipm)].dbo.USERS_V01
where
	not exists
	(
	select
		1
	from
		[$(ipm)].dbo.USERS_V01 Previous
	where
		Previous.CODE = USERS_V01.CODE
	and	(
			Previous.DATA_START_DATE > USERS_V01.DATA_START_DATE
		or	(
				Previous.DATA_START_DATE = USERS_V01.DATA_START_DATE
			and	Previous.TISID > USERS_V01.TISID
			)
		)
	)

﻿




CREATE view [ETL].[TLoadIUIEncounter]

as

select
	 DateOfBirth = cast(DOB as date)
	,NHSNumber = NHSNumber 
	,CasenoteNumber = Hospnum 
	,PatientName = Name 
	--,FollStart
	,AttemptNumber = Attempt 
	,CommissionerCode = Source 
	,TreatmentDate = cast(TrDate as date)

	,OutcomeID = 
		(
		select
			OutcomeID
		from
			IUI.Outcome
		where
			Outcome.Outcome = baseData.Outcome
		)

	,TreatmentID = 
		(
		select
			TreatmentID
		from
			IUI.Treatment
		where
			Treatment.Treatment = baseData.TreatType
		)

	,InterfaceCode = 'IUI'
from
	[$(SmallDatasets)].IUI.baseData




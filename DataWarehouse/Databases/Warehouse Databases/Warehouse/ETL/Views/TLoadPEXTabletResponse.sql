﻿









CREATE view [ETL].[TLoadPEXTabletResponse]

as

select
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID = case
					when AnswerID = 0
					then null
					else AnswerID
				end	
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,WardID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
from
	ETL.TImportPEXTabletResponse


left join
	PEX.KioskOnlinePostcardTabletWard
on	TImportPEXTabletResponse.LocationID = KioskOnlinePostcardTabletWard.WardID	









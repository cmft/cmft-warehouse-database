﻿CREATE  view [ETL].[TLoadRFEncounter] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode

	,RegisteredGpPracticeCode =
		RegisteredGp.PracticeCode

	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate

	,DischargeDate =
		left(DischargeDate, 10)

	,DischargeTime =
		case
		when right(rtrim(DischargeDate) , 5) = '24:00' then 
			dateadd(minute , 1 , convert(smalldatetime , replace(DischargeDate, '24:00', '23:59')))
		else DischargeDate
		end

	,DischargeReasonCode
	,DischargeReason

	,AdminCategoryCode =
		case
		when Encounter.AdminCategoryCode = 'NHS' then '01'
		when Encounter.AdminCategoryCode = 'PAY' then '02'
		when Encounter.AdminCategoryCode = 'IND' then '02'
		when Encounter.AdminCategoryCode = 'OV' then '02'
		when Encounter.AdminCategoryCode = 'AME' then '03'
		when Encounter.AdminCategoryCode = 'CAT' then '04'
		else '01'
		end

	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag

	,NextFutureAppointmentDate =
		convert(
			 smalldatetime
			,left(
				convert(
					 varchar
					,convert(
						 smalldatetime
						,left(Encounter.NextFutureAppointmentDate, 8)
						,112
					)
				)
				,11
			) + ' ' +
			substring(Encounter.NextFutureAppointmentDate, 9, 2) + ':' + substring(Encounter.NextFutureAppointmentDate, 11, 2)
		)

	,InterfaceCode

	,LocalAdminCategoryCode = Encounter.AdminCategoryCode

	,PCTCode =
		coalesce(
			 Practice.ParentOrganisationCode
			,Postcode.PCTCode
			,'X98'
		)

	,LocalityCode = LocalAuthority.LocalAuthorityCode
	,ReferralComment

from
	ETL.TImportRFEncounter Encounter

left join PAS.Gp RegisteredGp 
ON	Encounter.RegisteredGpCode = RegisteredGp.GpCode

left join [$(Organisation)].dbo.Practice Practice
on	Practice.OrganisationCode = RegisteredGp.PracticeCode

left join [$(Organisation)].dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join [$(Organisation)].dbo.LocalAuthority LocalAuthority
on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode

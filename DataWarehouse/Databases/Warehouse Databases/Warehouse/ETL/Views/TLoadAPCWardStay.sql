﻿CREATE view [ETL].[TLoadAPCWardStay] as

select distinct --this required due to issue when extracting recent data based on midnight bed stay table
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo

	,ProviderSpellNo =
		SourcePatientNo + '/' + SourceSpellNo

	,StartDate =
		case
		when StartTime = ':'
		then StartDate
		else
			cast(
				case
				when StartTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
				else convert(smalldatetime , StartDate)
				end
				as date
			)
		end

	,EndDate =
		case
		when EndTime = ':'
		then EndDate
		else
			cast(
				case
				when EndTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
				else convert(smalldatetime , EndDate)
				end
				as date
			)
		end


	,StartTime =
		case
		when StartTime = ':'
		then null
		else
			case
			when StartTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
			else convert(smalldatetime , StartDate + ' ' + StartTime)
			end
		end

	,EndTime =
		case
		when EndTime = ':'
		then null
		else
			case
			when EndTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
			else convert(smalldatetime , EndDate + ' ' + EndTime)
			end
		end

	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
from
	ETL.TImportAPCWardStay

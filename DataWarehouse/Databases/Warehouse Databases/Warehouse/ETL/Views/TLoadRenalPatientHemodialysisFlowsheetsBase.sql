﻿CREATE VIEW [ETL].[TLoadRenalPatientHemodialysisFlowsheetsBase] ([ObjectID], [HemodialysisServicesObjectID], [PatientObjectID], [PatientFullName], [PatientMedicalRecordNo], [Date_Local], [TimingQualifier], [StaffObjectID], [Staff], [AccessTransonicValue], [Weight], [WeightUnits], [WeightInKg], [Temperature], [TemperatureUnits], [TemperatureInCelsius], [SittingBloodPressureSystolic], [SittingBloodPressureDiastolic], [SittingHeartRate], [StandingBloodPressureSystolic], [StandingBloodPressureDiastolic], [StandingHeartRate], [Height], [HeightUnits], [HeightInCm], [UltrafiltrationGoal], [UltrafiltrationRate], [UFVRemoved], [BloodVolumeProcessed], [BloodFlowRate], [DialysateFlowRate], [ArtP], [VenP], [TransmembranePressure], [Conductivity], [AlarmState], [MachineMode], [MachineTemperature], [Notes])
as select
/* ObjectID ObjectID */ hdflowshee0_.[oid] as col_0_0_,
/* HemodialysisServicesObjectID HemodialysisService.ObjectID */ hdflowshee0_2_.[ObservationOID] as col_1_0_,
/* PatientObjectID Patient.ObjectID */ patient2_.[oid] as col_2_0_,
/* PatientFullName Patient.Person.Name */ person3_.[Name] as col_3_0_,
/* PatientMedicalRecordNo Patient.MedicalRecordNo */ patient2_.[MedicalRecordNo] as col_4_0_,
/* Date_Local PatientDateTime */ hdflowshee0_2_.[PatientDateTime] as col_5_0_,
/* TimingQualifier TimingQualifier */ code0.ClinicalVisionCore_UKEnglish as col_6_0_,
/* StaffObjectID InternalStaffOfRecord.ObjectID */ internalst4_.[oid] as col_7_0_,
/* Staff InternalStaffOfRecord.LookUp */ case when internalst4_.[InternalOID] is not null then person7_.[Name] when otherstaff8_.[Name] is not null then otherstaff8_.[Name] else N'' end as col_8_0_,
/* AccessTransonicValue Transonic */ hdflowshee0_.[Transonic] as col_9_0_,
/* Weight Weight.Weight */ weight9_.[Weight] as col_10_0_,
/* WeightUnits Weight.WeightUnits */ code1.ClinicalVisionCore_UKEnglish as col_11_0_,
/* WeightInKg Weight.WeightInKg */ case when weight9_.[Weight] is not null then case when weight9_.[WeightUnits]=1258 then weight9_.[Weight]*0.001 when weight9_.[WeightUnits]=1259 then weight9_.[Weight]*0.028 when weight9_.[WeightUnits]=1260 then weight9_.[Weight]*0.454 when weight9_.[WeightUnits]=1261 then weight9_.[Weight] else null end else null end as col_12_0_,
/* Temperature Temperature.Temperature */ temperatur10_.[Temperature] as col_13_0_,
/* TemperatureUnits Temperature.TemperatureUnits */ code2.ClinicalVisionCore_UKEnglish as col_14_0_,
/* TemperatureInCelsius Temperature.TemperatureInCelsius */ case when temperatur10_.[Temperature] is not null then case when temperatur10_.[TemperatureUnits]=10807 then temperatur10_.[Temperature]*1.0 when temperatur10_.[TemperatureUnits]=10808 then (temperatur10_.[Temperature]-32.0)*5.0/9.0 else null end else null end as col_15_0_,
/* SittingBloodPressureSystolic SittingBPandHR.Systolic */ sittingblo11_1_.[Systolic] as col_16_0_,
/* SittingBloodPressureDiastolic SittingBPandHR.Diastolic */ sittingblo11_1_.[Diastolic] as col_17_0_,
/* SittingHeartRate SittingBPandHR.HeartRate */ sittingblo11_1_.[HeartRate] as col_18_0_,
/* StandingBloodPressureSystolic StandingBPandHR.Systolic */ standingbl12_1_.[Systolic] as col_19_0_,
/* StandingBloodPressureDiastolic StandingBPandHR.Diastolic */ standingbl12_1_.[Diastolic] as col_20_0_,
/* StandingHeartRate StandingBPandHR.HeartRate */ standingbl12_1_.[HeartRate] as col_21_0_,
/* Height Height.Height */ height13_.[Height] as col_22_0_,
/* HeightUnits Height.HeightUnits */ code3.ClinicalVisionCore_UKEnglish as col_23_0_,
/* HeightInCm Height.HeightInCm */ case when height13_.[Height] is not null then case when height13_.[HeightUnits]=9797 then height13_.[Height] when height13_.[HeightUnits]=9798 then height13_.[Height]*2.54 else null end else null end as col_24_0_,
/* UltrafiltrationGoal UltrafiltrationGoal */ hdflowshee0_.[UltrafiltrationGoal] as col_25_0_,
/* UltrafiltrationRate UltrafiltrationRate */ hdflowshee0_.[UltrafiltrationRate] as col_26_0_,
/* UFVRemoved UFVRemoved */ hdflowshee0_.[UFVRemoved] as col_27_0_,
/* BloodVolumeProcessed BloodVolumeProcessed */ hdflowshee0_.[BloodVolumeProcessed] as col_28_0_,
/* BloodFlowRate BloodFlow */ hdflowshee0_.[BloodFlow] as col_29_0_,
/* DialysateFlowRate DialysateFlow */ hdflowshee0_.[DialysateFlow] as col_30_0_,
/* ArtP ArterialPressure */ hdflowshee0_.[ArterialPressure] as col_31_0_,
/* VenP VenousPressure */ hdflowshee0_.[VenousPressure] as col_32_0_,
/* TransmembranePressure TransmembranePressure */ hdflowshee0_.[TransmembranePressure] as col_33_0_,
/* Conductivity Conductivity */ hdflowshee0_.[Conductivity] as col_34_0_,
/* AlarmState AlarmState */ hdflowshee0_.[AlarmState] as col_35_0_,
/* MachineMode MachineMode */ hdflowshee0_.[MachineMode] as col_36_0_,
/* MachineTemperature MachineTemperature */ hdflowshee0_.[MachineTemperature] as col_37_0_,
/* Notes Notes */ hdflowshee0_2_.[Notes] as col_38_0_ 
from
	[$(ClinicalVision)].[Core].[HDFlowsheet] hdflowshee0_ 

inner join [$(ClinicalVision)].[Core].[Vitals] hdflowshee0_1_ on hdflowshee0_.[oid]=hdflowshee0_1_.[oid] 
inner join [$(ClinicalVision)].[Core].[Observation] hdflowshee0_2_ on hdflowshee0_.[oid]=hdflowshee0_2_.[oid]    
left join [$(ClinicalVision)].[Core].[Patient] patient2_ on hdflowshee0_2_.[PatientOID]=patient2_.[oid] 
left join [$(ClinicalVision)].[Core].[Person] person3_ on patient2_.[oid]=person3_.[oid] 
left join [$(ClinicalVision)].[Core].[InternalStaffOfRecord] internalst4_ on hdflowshee0_2_.[InternalStaffOfRecordOID]=internalst4_.[oid]   
left join [$(ClinicalVision)].[Core].[Person] person7_ on internalst4_.[InternalOID]=person7_.[oid] 
left join [$(ClinicalVision)].[Core].[OtherStaff] otherstaff8_ on internalst4_.[OtherStaffOID]=otherstaff8_.[oid] 
left join [$(ClinicalVision)].[Core].[Weight] weight9_ on hdflowshee0_1_.[WeightOID]=weight9_.[oid] 
left join [$(ClinicalVision)].[Core].[Temperature] temperatur10_ on hdflowshee0_1_.[TemperatureOID]=temperatur10_.[oid]  
left join [$(ClinicalVision)].[Core].[BloodPressure] sittingblo11_1_ on hdflowshee0_1_.[SittingBPandHROID]=sittingblo11_1_.[oid]  
left join [$(ClinicalVision)].[Core].[BloodPressure] standingbl12_1_ on hdflowshee0_1_.[StandingBPandHROID]=standingbl12_1_.[oid] 
left join [$(ClinicalVision)].[Core].[Height] height13_ on hdflowshee0_1_.[HeightOID]=height13_.[oid]
left join [$(ClinicalVision)].[Core].[Code] code0 on code0.oid=hdflowshee0_2_.[TimingQualifier]
left join [$(ClinicalVision)].[Core].[Code] code1 on code1.oid=weight9_.[WeightUnits]
left join [$(ClinicalVision)].[Core].[Code] code2 on code2.oid=temperatur10_.[TemperatureUnits]
left join [$(ClinicalVision)].[Core].[Code] code3 on code3.oid=height13_.[HeightUnits]

﻿CREATE view [ETL].[TLoadPASMaternityAnaesthetic] as

select
	 MaternityAnaestheticID = ANANINITID
	,AnalgesicDuringLabour = AnaesanalgDuring
	,DuringLabourAnaestheticCode = Aneadurlab
	,DuringLabourAnaestheticReasonCode = Aneadurlabreas
	,PostLabourAnaestheticCode = Aneapostlab
	,PostLabourAnaestheticReasonCode = Aneapostlabreas
	,SourceSpellNo = EpisodeNumber
	,SourcePatientNo = InternalPatientNumber
	--,KeMatPerAnaesAdminInt
from
	ETL.TImportPASMaternityAnaesthetic

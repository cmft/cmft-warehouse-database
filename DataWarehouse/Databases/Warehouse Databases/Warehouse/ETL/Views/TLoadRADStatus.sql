﻿CREATE view [ETL].[TLoadRADStatus] 

AS
	/******************************************************************************
	**  Name: TLoadRADStatus
	**  Purpose: 
	**
	**  Import procedure view for Radiology RAD.Status table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 06.05.11     MH        Removed Comment column - no longer used after CRIS update
	** 26.06.12     MH        Altered for CMFT environment
	******************************************************************************/


SELECT
	 ImportDateTimeStamp
	,SourceUniqueID = Utility.udfEmptyStringToNull([Status Key])
	,ExamSourceUniqueID = Utility.udfEmptyStringToNull([Exam Key])
	,EventSourceUniqueID = Utility.udfEmptyStringToNull([Event Key])
	,SourcePatientNo = Utility.udfEmptyStringToNull([CRIS No ])
	,SiteCode = Utility.udfEmptyStringToNull([Site])
	,ExamCode = Utility.udfEmptyStringToNull([Exam])
	,CurrentStatusFlag = Utility.udfEmptyStringToNull([Cur])
	,StatusCode = Utility.udfEmptyStringToNull([Status Cde])
	,StatusCategoryCode = Utility.udfEmptyStringToNull([Cat])
	,StatusTypeCode = Utility.udfEmptyStringToNull([Type])
	,StatusDate = Utility.udfConvertDate([Date])
	,StatusTime = Utility.udfConvertDateTime([Date] , [Time])
	,StatusCodeAtEndDate = Utility.udfEmptyStringToNull([Code At End Date])
	,StatusCategoryCodeAtEndDate = Utility.udfEmptyStringToNull([Cat at End Date])
	,StatysTypeCodeAtEndDate = Utility.udfEmptyStringToNull([Type at End Date])
	,StartDate = Utility.udfConvertDate([Start date])
	,StartTime = Utility.udfConvertDateTime([Start date] , [Start time])
	,EndDate = Utility.udfConvertDate([End date])
	,EndTime = Utility.udfConvertDateTime([End date] , [End time])
	,LogicalDeleteFlag = Utility.udfEmptyStringToNull([Deleted])
	,Description = Utility.udfEmptyStringToNull([Description])
	,OtherCode = Utility.udfEmptyStringToNull([Other code])
	,UserID = Utility.udfEmptyStringToNull([User ID])
FROM
	[$(CRIS_Import)].dbo.TImportRADStatus RADStatus
where
	Utility.udfEmptyStringToNull([Status Key]) is not null

﻿









CREATE view [ETL].[TLoadResultGPICEOrder] as

select
	SourceUniqueID
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber 
	,ClinicianID
	,MainSpecialtyCode
	,ProviderID
	,LocationID
	,OrderPriority
	,OrderStatusCode
	,OrderRequestTime
	,OrderReceivedTime
	,OrderCollectionTime
	,InterfaceCode = 'GPICE'
from
	ETL.TImportResultGPICEOrder









﻿





CREATE view [ETL].[TLoadDictationMedisecDocument]

as

select
	SourceUniqueID
	,SourcePatientNo
	,DocumentID
	,DocumentDescription
	,DocumentTypeCode
	--,DocumentStatusCode
	,AuthorCode
	,TypedTime
	,DictatedTime
	,DictatedByCode
	,IssuedTime
	,IssuedByCode
	,SignedTime
	,SignedByCode
	,SignedStatusCode
	,DepartmentCode
	,DocumentXMLTemplateCode
	,DocumentXML
	,TransmissionTime
	,TransmissionDestinationCode
from
	ETL.TImportMedisecDictationDocument







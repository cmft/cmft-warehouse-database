﻿










CREATE view [ETL].[TLoadResultCENICETest] as

select
	SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber 
	,ClinicianID
	,MainSpecialtyCode
	,ProviderID
	,LocationID
	,TestStatus
	,TestID
	,InterfaceCode = 'CENICE'
from
	ETL.TImportResultCENICETest










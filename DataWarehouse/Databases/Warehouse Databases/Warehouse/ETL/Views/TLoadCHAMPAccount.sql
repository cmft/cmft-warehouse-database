﻿




CREATE view [ETL].[TLoadCHAMPAccount] as

/* 
==============================================================================================
Description: 

When		Who			What
24/02/2015	Paul Egan	Initial coding
===============================================================================================
*/
with AllAccountsCTE as
(
select 
	 AccountID
	,AccountIDOccurance = row_number() over(partition by AccountID order by AccountName)
	,ContactId
	,AccountCreatedTime = cast(dateCreated as datetime2(0))
	,Account = AccountName
	,ContactFullName = appl_ContactIdName
	,ContactFirstName = FirstName
	,ContactLastName = LastName
	,Created = getdate()
	,Updated = getdate()
	,ByWhom = suser_name()
from 
	ETL.TImportCHAMPAccount
)


select
	 AccountID
	,ContactId
	,AccountCreatedTime
	,Account
	,ContactFullName
	,ContactFirstName
	,ContactLastName
	,Created
	,Updated
	,ByWhom
from 
	AllAccountsCTE
where
	AccountIDOccurance = 1		-- Removes the account duplicates ('Not in Use' in the name)
;





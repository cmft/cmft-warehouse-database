﻿CREATE  view [ETL].[TLoadWaitingListEntry] as

Select
	 SourceUniqueID

	,ActivityDate = 
		cast(
			case
			when substring(ActivityTime, 9, 2) = '24'
				then dateadd(minute , 1 , convert(smalldatetime , left(ActivityTime , 8) + ' 23:59'))
			else convert(smalldatetime , left(ActivityTime , 8) + ' ' + substring(ActivityTime, 9, 2) + ':' + substring(ActivityTime, 11, 2))
			end
			as date
		)	

	,ActivityTime = 
		case
		when substring(ActivityTime, 9, 2) = '24'
			then dateadd(minute , 1 , convert(smalldatetime , left(ActivityTime , 8) + ' 23:59'))
		else convert(smalldatetime , left(ActivityTime , 8) + ' ' + substring(ActivityTime, 9, 2) + ':' + substring(ActivityTime, 11, 2))
		end

	,CasenoteNumber
	,ConsultantCode
	,DiagnosticGroupCode
	,DistrictNo
	,SourceEntityRecno
	,SourcePatientNo
	,IntendedPrimaryOperationCode
	,ManagementIntentionCode
	,SiteCode
	,AdmissionMethodCode
	,Operation
	,SpecialtyCode
	,WaitingListCode
	,ContractSerialNumber
	,PurchaserCode
	,EpisodicGpCode
	,InterfaceCode
from
	ETL.TImportWaitingListEntry

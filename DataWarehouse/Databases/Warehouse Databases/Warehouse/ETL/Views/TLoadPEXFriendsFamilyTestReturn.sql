﻿





CREATE view [ETL].[TLoadPEXFriendsFamilyTestReturn]

as


select
	  [Return]
      ,CensusDate = [ReturnMonth]
      ,[HospitalSiteCode]
      ,[HospitalSite]
      ,[WardCode]
      ,[Ward]
      ,[1ExtremelyLikely]
      ,[2Likely]
      ,[3NeitherLikelyNorUnlikely]
      ,[4Unlikely]
      ,[5ExtremelyUnlikely]
      ,[6DontKnow]
      ,Responses = [1ExtremelyLikely] + [2Likely] + [3NeitherLikelyNorUnlikely] + [4Unlikely] + [5ExtremelyUnlikely] + [6DontKnow]
      ,[EligibleResponders]
	  ,InterfaceCode = 'FFTRTN'
from
	(
	select
		  [Return]
		  ,[ReturnMonth]
		  ,[HospitalSiteCode]
		  ,[HospitalSite]
		  ,[WardCode] = 
				case
					when HospitalSiteCode = 'RW3TR' then ('TRA:' + WardCode)
					else ('CEN:' + WardCode)
				end
		  ,[Ward] = 
				case
					when HospitalSiteCode = 'RW3TR' then ('TRA:' + Ward)
					else ('CEN:' + Ward)
				end				
		  ,[1ExtremelyLikely]
		  ,[2Likely]
		  ,[3NeitherLikelyNorUnlikely]
		  ,[4Unlikely]
		  ,[5ExtremelyUnlikely]
		  ,[6DontKnow]
		  ,[EligibleResponders]
	from
		[$(SmallDatasets)].[CQUIN].[APCFriendsFamilyTestReturn]

	union all

	select
		  [Return]
		  ,[ReturnMonth]
		  ,[HospitalSiteCode]
		  ,[HospitalSite]
		  ,WardCode = null
		  ,Ward = null
		  ,[1ExtremelyLikely]
		  ,[2Likely]
		  ,[3NeitherLikelyNorUnlikely]
		  ,[4Unlikely]
		  ,[5ExtremelyUnlikely]
		  ,[6DontKnow]
		  ,[EligibleResponders]
	from
		[$(SmallDatasets)].[CQUIN].[AEFriendsFamilyTestReturn]
	
	)FFT







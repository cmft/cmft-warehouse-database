﻿






CREATE view [ETL].[TLoadDictationMedisecInpatient]

as

select --top 1000
	SourcePatientNo
    ,AdmissionTime
	,SpellSequenceNo
    ,RequestTypeCode
	,ContextCode = 'CEN||MEDI'
	,InterfaceCode = 'MEDI'
from
	ETL.TImportMedisecDictationInpatient 




/* 20141008 RR/DG
Doesn't need to link to APC.Encounter at this stage
Moved to WOMV2 and used the Allocation process for the Exclusions


select --top 1000
	APCEncounterRecno = Encounter.EncounterRecno 
	,MedisecInpatient.SourcePatientNo
    ,MedisecInpatient.AdmissionTime
    ,RequestTypeCode
	,MedisecInpatient.DocumentID
	,SequenceNo = row_number() over 
									(
									partition by 
												MedisecInpatient.SourcePatientNo
												,MedisecInpatient.AdmissionTime
												,MedisecInpatient.RequestTypeCode 
									order by
										MedisecInpatient.DocumentID
									) 
	,ExcludeFlag = 
				case
					when 
							/* Exclude Private patients */
							Encounter.LocalAdminCategoryCode = 'PAY' 
						or
							
							/* Exclude stillbirths and deaths */
							Encounter.DischargeMethodCode in ('DN', 'DP', 'SP', 'ST')
						or 
						
							/* Exclude regular day cases where LOS = 0 */
						(
							Encounter.ManagementIntentionCode = 'R'
						and
							datediff(day, Encounter.AdmissionDate, Encounter.DischargeDate) = 0
						)
						
						or
						
							/* Exclude Child Paeds */
						(
							Directorate.DivisionCode = 'SMH'
						and
							Specialty.NationalSpecialtyCode = '420'
						and
							Encounter.DischargeDate < '1 july 2009'
						and 
							(
									Encounter.EndWardTypeCode like 'SM4%'
								or
									Encounter.EndWardTypeCode like 'SM5%'
								or
									Encounter.EndWardTypeCode like 'SM6%'
								or
									Encounter.EndWardTypeCode like 'CDU%'
							)
						)
						
						or
						
							/* Exclude Obstetrics */
						(
								Encounter.SpecialtyCode in ('OBIC', 'OBPP', 'OBS')
							and
								Directorate.DivisionCode = 'SMH'
						)
						
						or
						
							/* Exclude Well Babies */
						(
								(
									Encounter.SpecialtyCode = 'PWB'
								and
									Directorate.DivisionCode = 'SMH'
								and
									coalesce(
											Encounter.NeonatalLevelOfCare
											,''
											) in (
													''
													,'N'
													,'0'
													)
								)		
							
						)	
						
						or 
						
						(
							Encounter.SpecialtyCode = 'PWB'
						and
							Directorate.DivisionCode = 'SMH'
						and
							not exists				
									(
										select
											1
										from
											APC.WardStay
										where
											Encounter.ProviderSpellNo = WardStay.ProviderSpellNo
										and
											WardStay.WardCode = '68'
									)
						)				

						or
						
							/* Exclude GYN1 SEND SEND */
							Encounter.SpecialtyCode in (
														'GYN1'
														,'GEND'
														,'SEND'
														) 
						or
						
							/* Exclude PDEN DENTAL */
						(
							Directorate.DivisionCode = 'DENT' 
						and
							Encounter.EndWardTypeCode = 'PDEN'
						)
						
						or
						
							/* Exclude GALAXY HOUSE */
							Encounter.EndWardTypeCode = '87'
						or
						
							/* Exclude M45/M77 in Surgery */	
						(
							Specialty.NationalSpecialtyCode = '101'
						and
							(
								Encounter.PrimaryOperationCode like 'M45%' 
							or
								Encounter.PrimaryOperationCode like 'M77%'
							)
						and
							Encounter.PatientCategoryCode <> 'NE'
						and
							Directorate.DivisionCode = 'SURG'
						)
						
						or
						
							/* New requirement from Dusia NEED TO CHECK LOGIC! */	
						(
							Specialty.NationalSpecialtyCode < '252'
						and
							Directorate.DivisionCode = 'CHILD'
						and
							Encounter.EndWardTypeCode in
														(
														'SUBA'
														,'SUBB'
														,'SUBD'
														,'SUBE'
														,'SUBM'
														,'SUBP'
														,'SUBS'
														,'SUBT'
														)
						)
						
						or
						
							/* Exclusion from Phil Huitson - plasma patients */
						(
							Encounter.PrimaryOperationCode like 'X32.2%'
						and
							Encounter.PatientCategoryCode = 'DC'
						)
						
						or
							
						(
							Encounter.SpecialtyCode = 'PLEX'		
						and
							Encounter.PatientCategoryCode = 'DC'	
						)
						
						or
						
							/* Requested by Phil H 25/06/12 */
						(
							Encounter.PrimaryOperationCode like 'X32.2%'
						and
							Specialty.NationalSpecialtyCode = '361'
						)
						
						or
						
							/* Exclude Termination of Pregnancies - Requested by Ian Daniels 21/11/11 */
						(
							Encounter.PrimaryOperationCode like 'Q11%'
						or
							Encounter.PrimaryOperationCode like 'Q14%'
						or
							Encounter.PrimaryDiagnosisCode like 'O04%'
						or
							Encounter.PrimaryDiagnosisCode like 'O07%'
						or
							Encounter.PrimaryDiagnosisCode = 'M30.3'
						)
						or
							/* ENDO location  - Requested by Phil Huitson 17/08/2012 */
						(
							Encounter.EndWardTypeCode = 'ENDO'
						)
							or
							(
									Specialty.NationalSpecialtyCode =  '303'
								and
									Encounter.PatientCategoryCode = 'DC'
								and
									--datediff(day, Encounter.DateOfBirth, Encounter.AdmissionDate)/365.25 >= 19
									CMFT.Dates.GetAge(Encounter.DateOfBirth, Encounter.AdmissionDate)  >= 19
							)
						or
							/* Exclude 'DNRs' */
							RequestTypeCode = '09'
						or
							/* Exclude where not required or Signed off not for GP */
							MedisecDocument.SignedStatusCode in
																(
																'N'
																,'X'
																)
					then 1
					else null
				end
		,ContextCode = 'CEN||MEDI'
		,InterfaceCode = 'MEDI'
		,TransmissionTime
		,TransmissionDestinationCode
from
	ETL.TImportMedisecDictationInpatient MedisecInpatient

inner join APC.Encounter Encounter
on	MedisecInpatient.SourcePatientNo = Encounter.SourcePatientNo
and MedisecInpatient.AdmissionTime = Encounter.AdmissionTime
and LastEpisodeInSpellIndicator = 'Y'

inner join PAS.Specialty Specialty
on	Encounter.SpecialtyCode = Specialty.SpecialtyCode

inner join WH.Directorate Directorate
on	coalesce(Encounter.EndDirectorateCode, '9') = Directorate.DirectorateCode

left outer join Dictation.MedisecDocument MedisecDocument
on	MedisecInpatient.DocumentID = MedisecDocument.DocumentID

*/
	



	
	


























﻿
CREATE View [ETL].[TLoadK2CTGAssessments]

As


select 
	PatientID
	,HospitalPatientID
	,CaseNumber
	,Surname
	,FirstName
	,DoB
	,NHSNumber
	,StaffID
	,SessionNumber
	,FetusNumber
	,OrderID
	,DateEntered
	,TimeEntered
	,BuddySigWithin10mins
	,BuddyDescription
	,BuddyStaffGroup
	,ReviewMaternalPulse
	,ReviewDilatation
	,Dilatation
	,MaternalPulseRecorded
	,DilatationRecorded
	,ContractionRate
	,CTGEvaluation
	,CTGDescriptionID
	,CTGDescription
	,BaselineRate
	,Variability
	,DecelerationDuration
	,DecelerationType
	,DecelerationDescription
	,EFMPlan
	,CTGReviewComment
	,CTGReviewAction
	,CTGReviewActionOther
	,ReviewLiquorColour
	,StaffGroup
	,StaffInitials
from
	ETL.TImportK2CTGAssessments 


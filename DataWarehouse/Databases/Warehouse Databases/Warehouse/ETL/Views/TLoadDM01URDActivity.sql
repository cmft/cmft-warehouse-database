﻿
CREATE VIEW [ETL].[TLoadDM01URDActivity] 
AS

	/******************************************************************************
	**  Name: ETL.TLoadDM01URDActivity
	**  Purpose: 
	**
	**  Import procedure view for DM01 Urethal Dynamics Activity table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 07.11.12    MH         Created
	******************************************************************************/

	SELECT 
			 EncounterDate			= TImportDM01URDActivity.[Date of test]
			,SourceUniqueId			= TImportDM01URDActivity.[Last Name] + '||' + TImportDM01URDActivity.[First Name] + '||' + TImportDM01URDActivity.[Date listed] + '||' + TImportDM01URDActivity.[Date of test]
			,SourcePatientNo		= PATIENTNO.SourcePatientNo
			,ListDate				= TImportDM01URDActivity.[Date listed]
			,SentFor				= UPPER(LEFT(TImportDM01URDActivity.[Sent for],1))
			,Confirmed				= UPPER(LEFT(TImportDM01URDActivity.[Confirmed],1))
			,Completed				= UPPER(LEFT(TImportDM01URDActivity.[Done],1))
			,Comments				= TImportDM01URDActivity.[Comments]
			,Planned				= CASE
										WHEN LEFT(TImportDM01URDActivity.[Comments],1) = 'P'
										THEN 'Y'
										ELSE 'N'
									  END
			,CaseNoteNumber			= COALESCE(PATIENTNO.CaseNoteNumber, TImportDM01URDActivity.[Hospital No])
			,PCTCode				= COALESCE(PATIENTDETAILS.PCTCode,'5NT')

	FROM
		ETL.TImportDM01URDActivity 
		OUTER APPLY [Utility].[fn_GetPatientFromCaseNote](TImportDM01URDActivity.[Hospital No]) PATIENTNO
		OUTER APPLY [Utility].[fn_GetPatientDetails](PATIENTNO.SourcePatientNo) PATIENTDETAILS


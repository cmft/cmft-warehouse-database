﻿


CREATE view [ETL].[TLoadAPCLearningDisability]

as

/* 
==============================================================================================
Description:	Created for learning disability tracking report. 

When		Who			What
20150602	Paul Egan	Initial Coding
20150805	RR			Discussed with JR, using Bedman in Dev, may want to change to BedmanTest when deployed to Live (in Live Bedman is updated twice daily, where BedmanTest is updated every 15mins)
===============================================================================================
*/

with LearningDisabilityCTE as
	(
	select
		SourceUniqueID = LearningDisability.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,AssessmentDate = cast(LearningDisability.LastUpdated_TS as date)
		,AssessmentTime = cast(LearningDisability.LastUpdated_TS as datetime2(0))
		,KnownLearningDisability =
			case Details.value('data(LearningDisability/KnownLearningDisability)[1]', 'char(1)')
				when 'Y' then 1
				when 'N' then 0
				else null
			end
		,ReasonableAdjustmentsCarePlan = 
			case Details.value('data(LearningDisability/ReasonableAdjustmentsCarePlan)[1]', 'char(1)')
				when 'Y' then 1
				when 'N' then 0
				else null
			end
		,LDPassport =
			case Details.value('data(LearningDisability/LDPassport)[1]', 'char(1)')
				when 'Y' then 1
				when 'N' then 0
				else null
			end
		,WardCode = Locations.PASCode
		--,LenDetails = len(cast(details as varchar(max)))
		--,Details
	from
		[$(BedmanTest)].dbo.Event LearningDisability

	inner join [$(BedmanTest)].dbo.HospSpell Spell -- need to change to bedmantest snapshot once deployed
	on	Spell.SpellID = LearningDisability.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on LearningDisability.LocationID = Locations.LocationID

	where
		EventTypeID = 19
	and not exists --If we have the same event type occuring at the same time, get the latest occurence
			(
			select	
				1
			from
				[$(BedmanTest)].dbo.Event LearningDisabilityLater
			where
				EventTypeID = 19
			and LearningDisabilityLater.HospSpellID = LearningDisability.HospSpellID
			and LearningDisabilityLater.LastUpdated_TS = LearningDisability.LastUpdated_TS
			and	LearningDisabilityLater.ID > LearningDisability.ID
			)
	)

select
	SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,AssessmentDate
	,AssessmentTime
	,KnownLearningDisability
	,ReasonableAdjustmentsCarePlan
	,LDPassport
	,WardCode
	--,LenDetails
	--,Details
	,InterfaceCode = 'BEDMAN'	
from
	LearningDisabilityCTE
;
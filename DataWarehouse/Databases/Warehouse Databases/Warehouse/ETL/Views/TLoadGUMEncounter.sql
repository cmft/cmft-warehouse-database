﻿







CREATE view [ETL].[TLoadGUMEncounter]

as

select
	AppointmentDate = cast([Appointment Date] as date)
	,AppointmentTime = [Appointment Date]
	,PatientIdentifier = [PatientNumber]
	,Postcode = [PostCode]
	,ClinicID = 
				(
				select
					ClinicID
				from
					GUM.Clinic
				where
					Clinic.Clinic = baseData.Clinic
				)
	,AppointmentTypeID = 
				(
				select
					AppointmentTypeID
				from
					GUM.AppointmentType
				where
					AppointmentType.AppointmentType = baseData.[Appointment Type]
				)			
	,AttendanceStatusID = 
			(
			select
				AttendanceStatusID
			from
				GUM.AttendanceStatus
			where
				AttendanceStatus.AttendanceStatus = baseData.[Attended or Did Not Attend]
			)
	,InterfaceCode = 'MCSH'
from
	[$(SmallDatasets)].GUM.baseData






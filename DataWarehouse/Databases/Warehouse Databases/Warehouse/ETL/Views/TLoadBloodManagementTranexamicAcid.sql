﻿--Use Warehouse

CREATE View [ETL].[TLoadBloodManagementTranexamicAcid]

as

select 
	TranexamicAcidPrimaryKey	
	,EpisodeKey	
	,HospitalNumber	
	,LastName	
	,FirstName	
	,DateOfBirth	
	,Gender	
	,SpecialtyKey	
	,ConsultantKey
	,IntendedDestination	
	,ActualDestination	
	,DrugKey	
	,DrugDescription	
	,Category
	,Dose	
	,UnitsKey	
	,UnitsDescription	
	,DrugAdministeredDate
	,DrugAdministeredTime	
	,SessionTypeDescription 
	,SessionLocationKey 
	,InterfaceCode
from 
	ETL.TImportBloodManagementTranexamicAcid


﻿

CREATE view [ETL].[TLoadRenalPatientLabPanelElectrolytes] as

select
SourceUniqueID = cast(ObjectID as varchar(100))
,PatientObjectID
,PatientFullName = PatientFullName collate Latin1_General_CI_AS
,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
,LabTestObjectID
,LabTestDate = LabTestDate_Local
,LabTestQualifier = LabTestQualifier collate Latin1_General_CI_AS
,LabTestStatus = LabTestStatus collate Latin1_General_CI_AS
,AnionGapNum
,AnionGapStr = AnionGapStr collate Latin1_General_CI_AS
,AnionGapFlg = AnionGapFlg collate Latin1_General_CI_AS
,ChlorideNum
,ChlorideStr = ChlorideStr collate Latin1_General_CI_AS
,ChlorideFlg = ChlorideFlg collate Latin1_General_CI_AS
,CO2Num
,CO2Str = CO2Str collate Latin1_General_CI_AS
,CO2Flg = CO2Flg collate Latin1_General_CI_AS
,CreatinineNum
,CreatinineStr = CreatinineStr collate Latin1_General_CI_AS
,CreatinineFlg = CreatinineFlg collate Latin1_General_CI_AS
,GlucoseNum
,GlucoseStr = GlucoseStr collate Latin1_General_CI_AS
,GlucoseFlg = GlucoseFlg collate Latin1_General_CI_AS
,HCO3Num
,HCO3Str = HCO3Str collate Latin1_General_CI_AS
,HCO3Flg = HCO3Flg collate Latin1_General_CI_AS
,PotassiumNum
,PotassiumStr = PotassiumStr collate Latin1_General_CI_AS
,PotassiumFlg = PotassiumFlg collate Latin1_General_CI_AS
,SodiumNum
,SodiumStr = SodiumStr collate Latin1_General_CI_AS
,SodiumFlg = SodiumFlg collate Latin1_General_CI_AS
,UrateNum
,UrateStr = UrateStr collate Latin1_General_CI_AS
,UrateFlg = UrateFlg collate Latin1_General_CI_AS
,BloodUreaNitrogenNum
,BloodUreaNitrogenStr = BloodUreaNitrogenStr collate Latin1_General_CI_AS
,BloodUreaNitrogenFlg = BloodUreaNitrogenFlg collate Latin1_General_CI_AS
,UricAcidNum
,UricAcidStr = UricAcidStr collate Latin1_General_CI_AS
,UricAcidFlg = UricAcidFlg collate Latin1_General_CI_AS
from ETL.[TLoadRenalPatientLabPanelElectrolytesBase]

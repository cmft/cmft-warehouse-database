﻿





CREATE view [ETL].[TLoadSIDEncounter]

as

select
	 *
	,AQKPIPassed = 
		case
		when AQKPIDenominator >0
		then (case when AQKPINumberofMeasuresPassed = AQKPIDenominator then 1 else 0 end)
		else null
		end

from
(

select
	 SourcePatientNo
	,DateCreated
	,StatusID
	,CaseNoteNumber
	,AENumber
	,NHSNumber
	,DistrictNumber
	,Surname
	,Forenames
	,DOB
	,Gender
	,DateSubmitted
	,SpellID
	,CandidateDateCreated
	,CandidateStatusID
	,CandidateTIE
	,DateDeparture
	,Address1
	,Address2
	,Address3
	,Postcode
	,GPName
	,GPAddress
	,DateHospitalArrival
	,HoursInHospitalVal
	,TIE
	,DateTimeOfAttendedED
	,DateTimeOfOnsetStrokeSymptoms
	,RosierLossOfConciousnessOrSyncope
	,RosierSeizureActivity
	,RosierAsymmetricFacialWeakness
	,RosierAsymmetricArmWeakness
	,RosierAsymmetricLegWeakness
	,RosierSpeechDisturbance
	,RosierVisualFieldDefect
	,RosierTotalScore
	,SwallowAssessmentCompleted
	,IfNotWithin4HoursWhyNot
	,DateTimeSwallowAssessmentCompleted
	,SwallowScreenResults
	,SwallowAssessmentCompletedNAReasonyWhy
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendance
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceReason
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceNotes
	,RequestForUrgentCTScan
	,DateTimeRequestForUrgentCTScan
	,DateTimeCTScanPerformed
	,BrainScanWithin24HoursOfAttendance
	,BrainScanWithin24HoursOfAttendanceReason
	,BrainScanWithin24HoursOfAttendanceNotes
	,Aspirin300mgAdministered
	,DateTimeAspirin300mgAdministered
	,DateTimeAspirin300mgAssessed
	,AspirinNotes
	,AspirinWithin24HoursOfAttendance
	,AspirinWithin24HoursOfAttendanceReason
	,AspirinWithin24HoursOfAttendanceNotes
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalReason
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalNotes
	,DateTimeFirstAssessedByPhysiotherapy
	,PhysiotherapyAssessmentWithinFirst24Hours
	,PhysiotherapyAssessmentWithinFirst24HoursReason
	,PhysiotherapyAssessmentWithinFirst24HoursNotes
	,BarthelAssessmentPreAdmissionDetailsTotalScore
	,BarthelAssessmentOnAdmissionDetailsTotalScore
	,DateTimeFirstAssessedByOccupationalTherapy
	,AssessmentByAnOccupationalTherapistWithinFirst24Hours
	,AssessmentByAnOccupationalTherapistWithinFirst24HoursReason
	,AssessmentByAnOccupationalTherapistWithinFirst24HoursNotes
	,DateTimeFirstAssessedSpeechAndLanguageTherapy
	,DateTimeNurseWithStrokeExperienceReview
	,DateTimeFirstAssessedByStrokePhysician
	,SeenByStrokePhysicianWithin24HoursOfAttendance
	,SeenByStrokePhysicianWithin24HoursOfAttendanceReason
	,SeenByStrokePhysicianWithin24HoursOfAttendanceNotes
	,HasContinenceAssessmentBeenDone
	,DateTimeContinenceAssessmentDone
	,Weighed
	,WeighedAtLeastOnceDuringAttendance
	,WeighedAtLeastOnceDuringAttendanceReason
	,WeighedAtLeastOnceDuringAttendanceNotes
	,InitialAssessmentSectionComplete
	,InitialAssessmentSectionCompleteTime
	,LikelyStrokeSectionComplete
	,ILikelyStrokeSectionCompleteTime
	,AcuteCareBundleSectionComplete
	,AcuteCareBundleSectionCompleteTime
	,AQExcluded
	,AQSwallowAssessmentWithin24hrsOfEDAttendance
	,AQCTScanPerformedWithin24hrsOfEDAttendance
	,AQAspirinGivenWithin24hrsOfEDAttendance
	,AQDirectAdmissionToAStrokeUnitWithin4HoursOfEDAttendance
	,AQPhysioAssessmentWithin72hrsOfEDAttendance
	,AQOccupationalTherapyAssessmentWithin72hrsOfEDAttendance
	,AQWeighed
	,AQKPIDenominator = 
			case when AQSwallowAssessmentWithin24hrsOfEDAttendance is null then 0 else 1 end
		+
			case when AQCTScanPerformedWithin24hrsOfEDAttendance is null then 0 else 1 end
		+
			case when AQAspirinGivenWithin24hrsOfEDAttendance is null then 0 else 1 end
		+
			case when AQDirectAdmissionToAStrokeUnitWithin4HoursOfEDAttendance is null then 0 else 1 end
		+
			case when AQPhysioAssessmentWithin72hrsOfEDAttendance is null then 0 else 1 end
		+
			case when AQOccupationalTherapyAssessmentWithin72hrsOfEDAttendance is null then 0 else 1 end
		+
			case when AQWeighed is null then 0 else 1 end
	,AQKPINumberofMeasuresPassed = 
	
			isnull(AQSwallowAssessmentWithin24hrsOfEDAttendance,0)
		+
			isnull(AQCTScanPerformedWithin24hrsOfEDAttendance ,0)
		+
			isnull(AQAspirinGivenWithin24hrsOfEDAttendance ,0)
		+
			isnull(AQDirectAdmissionToAStrokeUnitWithin4HoursOfEDAttendance ,0)
		+
			isnull(AQPhysioAssessmentWithin72hrsOfEDAttendance ,0)
		+
			isnull(AQOccupationalTherapyAssessmentWithin72hrsOfEDAttendance ,0)
		+
			isnull(AQWeighed ,0)
	,StrokePhysicianAssessmentWithin72hrsOfEDAttendance 

from
(

select
	 SourcePatientNo = 
		encounter.CandidateID
	,encounter.DateCreated
	,encounter.StatusID
	,encounter.CaseNoteNumber
	,encounter.AENumber
	,encounter.NHSNumber
	,encounter.DistrictNumber
	,encounter.Surname
	,encounter.Forenames
	,encounter.DOB
	,encounter.Gender
	,encounter.DateSubmitted
	,encounter.SpellID
	,encounter.CandidateDateCreated
	,encounter.CandidateStatusID
	,encounter.CandidateTIE
	,encounter.DateDeparture
	,encounter.Address1
	,encounter.Address2
	,encounter.Address3
	,encounter.Postcode
	,encounter.GPName
	,encounter.GPAddress
	,encounter.DateHospitalArrival
	,encounter.HoursInHospitalVal
	,encounter.TIE
	,DateTimeOfAttendedED = 
		InitialAssessment.DateTimeOfAttendedED
	,DateTimeOfOnsetStrokeSymptoms = 
		InitialAssessment.DateTimeOfOnsetStrokeSymptoms
	,TypeOfPresentation = 
		InitialAssessment.TypeOfPresentation
	,RosierLossOfConciousnessOrSyncope = 
		InitialAssessment.ROSIERDTOLossOfConciousnessOrSyncope
	,RosierSeizureActivity = 
		InitialAssessment.ROSIERDTOSeizureActivity
	,RosierAsymmetricFacialWeakness = 
		InitialAssessment.ROSIERDTOAsymmetricFacialWeakness
	,RosierAsymmetricArmWeakness = 
		InitialAssessment.ROSIERDTOAsymmetricArmWeakness
	,RosierAsymmetricLegWeakness = 
		InitialAssessment.ROSIERDTOAsymmetricLegWeakness
	,RosierSpeechDisturbance = 
		InitialAssessment.ROSIERDTOSpeechDisturbance
	,RosierVisualFieldDefect = 
		InitialAssessment.ROSIERDTOVisualFieldDefect
	,RosierTotalScore = 
		InitialAssessment.ROSIERDTOTotalScore
	,SwallowAssessmentCompleted = 
		LikelyStroke.SwallowAssessmentCompleted
	,IfNotWithin4HoursWhyNot = 
		replace(replace(LikelyStroke.IfNotWithin4HoursWhyNot,char(13),''),char(10),'')
	,DateTimeSwallowAssessmentCompleted = 
		LikelyStroke.DateTimeSwallowAssessmentCompleted
	,SwallowScreenResults = 
		replace(replace(LikelyStroke.SwallowScreenResults,char(13),''),char(10),'')
	,SwallowAssessmentCompletedNAReasonyWhy = 
		replace(replace(LikelyStroke.SwallowAssessmentCompletedNAReasonyWhy,char(13),''),char(10),'')
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendance = 
		QualityIndicators.ScreenedForSwallowingDisordersWithin24HoursOfAttendance
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceReason = 
		replace(replace(QualityIndicators.ScreenedForSwallowingDisordersWithin24HoursOfAttendanceReason,char(13),''),char(10),'')
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceNotes = 
		replace(replace(QualityIndicators.ScreenedForSwallowingDisordersWithin24HoursOfAttendanceNotes,char(13),''),char(10),'')
	,RequestForUrgentCTScan = 
		LikelyStroke.RequestForUrgentCTScan
	,DateTimeRequestForUrgentCTScan = 
		LikelyStroke.DateTimeRequestForUrgentCTScan
	,DateTimeCTScanPerformed = 
		LikelyStroke.DateTimeCTScanPerformed
	,BrainScanWithin24HoursOfAttendance = 
		QualityIndicators.BrainScanWithin24HoursOfAttendance
	,BrainScanWithin24HoursOfAttendanceReason = 
		replace(replace(QualityIndicators.BrainScanWithin24HoursOfAttendanceReason,char(13),''),char(10),'')
	,BrainScanWithin24HoursOfAttendanceNotes = 
		replace(replace(QualityIndicators.BrainScanWithin24HoursOfAttendanceNotes,char(13),''),char(10),'')
	,Aspirin300mgAdministered = 
		LikelyStroke.Aspirin300mgAdministered
    ,DateTimeAspirin300mgAdministered = 
		LikelyStroke.DateTimeAspirin300mgAdministered
    ,DateTimeAspirin300mgAssessed = 
		LikelyStroke.DateTimeAspirin300mgAssessed
    ,AspirinNotes = 
		replace(replace(LikelyStroke.AspirinNotes,char(13),''),char(10),'')
	,AspirinWithin24HoursOfAttendance = 
		QualityIndicators.AspirinWithin24HoursOfAttendance
    ,AspirinWithin24HoursOfAttendanceReason = 
		replace(replace(QualityIndicators.AspirinWithin24HoursOfAttendanceReason,char(13),''),char(10),'')
    ,AspirinWithin24HoursOfAttendanceNotes = 
		replace(replace(QualityIndicators.AspirinWithin24HoursOfAttendanceNotes,char(13),''),char(10),'')
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival = 
		QualityIndicators.DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival
    ,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalReason = 
		replace(replace(QualityIndicators.DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalReason,char(13),''),char(10),'')
    ,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalNotes = 
		replace(replace(QualityIndicators.DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalNotes,char(13),''),char(10),'')
    ,DateTimeFirstAssessedByPhysiotherapy = 
		AcuteCareBundle.DateTimeFirstAssessedByPhysiotherapy
	,PhysiotherapyAssessmentWithinFirst24Hours = 
		QualityIndicators.PhysiotherapyAssessmentWithinFirst24Hours
    ,PhysiotherapyAssessmentWithinFirst24HoursReason = 
		replace(replace(QualityIndicators.PhysiotherapyAssessmentWithinFirst24HoursReason,char(13),''),char(10),'')
    ,PhysiotherapyAssessmentWithinFirst24HoursNotes = 
		replace(replace(QualityIndicators.PhysiotherapyAssessmentWithinFirst24HoursNotes,char(13),''),char(10),'')
	,AcuteCareBundle.BarthelAssessmentPreAdmissionDetailsTotalScore
	,AcuteCareBundle.BarthelAssessmentOnAdmissionDetailsTotalScore
    ,DateTimeFirstAssessedByOccupationalTherapy = 
		AcuteCareBundle.DateTimeFirstAssessedByOccupationalTherapy
	,AssessmentByAnOccupationalTherapistWithinFirst24Hours =
		QualityIndicators.AssessmentByAnOccupationalTherapistWithinFirst24Hours
    ,AssessmentByAnOccupationalTherapistWithinFirst24HoursReason =
		replace(replace(QualityIndicators.AssessmentByAnOccupationalTherapistWithinFirst24HoursReason,char(13),''),char(10),'')
    ,AssessmentByAnOccupationalTherapistWithinFirst24HoursNotes =
		replace(replace(QualityIndicators.AssessmentByAnOccupationalTherapistWithinFirst24HoursNotes,char(13),''),char(10),'')
    ,DateTimeFirstAssessedSpeechAndLanguageTherapy = 
		AcuteCareBundle.DateTimeFirstAssessedSpeechAndLanguageTherapy
	,DateTimeNurseWithStrokeExperienceReview = 
		AcuteCareBundle.DateTimeNurseWithStrokeExperienceReview
	,DateTimeFirstAssessedByStrokePhysician = 	
		AcuteCareBundle.DateTimeFirstAssessedByStrokePhysician
	,SeenByStrokePhysicianWithin24HoursOfAttendance =
		QualityIndicators.SeenByStrokePhysicianWithin24HoursOfAttendance
    ,SeenByStrokePhysicianWithin24HoursOfAttendanceReason =
		replace(replace(QualityIndicators.SeenByStrokePhysicianWithin24HoursOfAttendanceReason,char(13),''),char(10),'')
    ,SeenByStrokePhysicianWithin24HoursOfAttendanceNotes =
		replace(replace(QualityIndicators.SeenByStrokePhysicianWithin24HoursOfAttendanceNotes,char(13),''),char(10),'')
	,HasContinenceAssessmentBeenDone = 	
		AcuteCareBundle.HasContinenceAssessmentBeenDone
    ,DateTimeContinenceAssessmentDone = 	
		AcuteCareBundle.DateTimeContinenceAssessmentDone
	,Weighed =
		AcuteCareBundle.Weighed
	,WeighedAtLeastOnceDuringAttendance =
		QualityIndicators.WeighedAtLeastOnceDuringAttendance
    ,WeighedAtLeastOnceDuringAttendanceReason =
		replace(replace(QualityIndicators.WeighedAtLeastOnceDuringAttendanceReason,char(13),''),char(10),'')
    ,WeighedAtLeastOnceDuringAttendanceNotes =
		replace(replace(QualityIndicators.WeighedAtLeastOnceDuringAttendanceNotes,char(13),''),char(10),'')
	,InitialAssessmentSectionComplete =
		InitialAssessment.IsSectionComplete
	,InitialAssessmentSectionCompleteTime =
		InitialAssessment.SectionCompleteTime
	,LikelyStrokeSectionComplete =
		LikelyStroke.IsSectionComplete
	,ILikelyStrokeSectionCompleteTime =
		LikelyStroke.SectionCompleteTime
	,AcuteCareBundleSectionComplete =
		AcuteCareBundle.IsSectionComplete
	,AcuteCareBundleSectionCompleteTime =
		AcuteCareBundle.SectionCompleteTime
	,AQExcluded = 
		case
		when StatusID = 8 then 1
		when StatusID = 10 then 1
		when InitialAssessment.DateTimeOfAttendedED is null then 1
		else 0
		end
	,AQSwallowAssessmentWithin24hrsOfEDAttendance = 
		case
		when LikelyStroke.SwallowAssessmentCompleted = 'NA' then NULL
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke','Inpatient Stroke') then 
			case
			when datediff(MINUTE,coalesce(InitialAssessment.DateTimeOfAttendedED,InitialAssessment.DateTimeOfOnsetStrokeSymptoms),LikelyStroke.DateTimeSwallowAssessmentCompleted) <= 1440 then 1 
			else 0
			end
		else null
		end

	,AQCTScanPerformedWithin24hrsOfEDAttendance = 
		case
		when LikelyStroke.RequestForUrgentCTScan = 'NA' then null
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke','Inpatient Stroke') then
			case
			when datediff(MINUTE,coalesce(InitialAssessment.DateTimeOfAttendedED,InitialAssessment.DateTimeOfOnsetStrokeSymptoms),LikelyStroke.DateTimeCTScanPerformed) <= 1440 then 1 
			else 0
			end
		else null
		end
	,AQAspirinGivenWithin24hrsOfEDAttendance = 
		case		
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke','Inpatient Stroke') then 
			case
			when QualityIndicators.AspirinWithin24HoursOfAttendance = 'NA' then null 
			when QualityIndicators.AspirinWithin24HoursOfAttendance = 'Y' then 1 
			when QualityIndicators.AspirinWithin24HoursOfAttendance = 'N' then 0 
			end
		else null
		end
	,AQDirectAdmissionToAStrokeUnitWithin4HoursOfEDAttendance = 
		case
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke') then 
			case
			when QualityIndicators.DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival = 'NA' then null 
			when QualityIndicators.DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival = 'Y' then 1 
			when QualityIndicators.DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival = 'N' then 0 
			end
		else null
		end
	,AQPhysioAssessmentWithin72hrsOfEDAttendance = 
		case
		when QualityIndicators.PhysiotherapyAssessmentWithinFirst24Hours = 'NA' then NULL
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke','Inpatient Stroke') then 
			case
			when datediff(MINUTE,coalesce(InitialAssessment.DateTimeOfAttendedED,InitialAssessment.DateTimeOfOnsetStrokeSymptoms),AcuteCareBundle.DateTimeFirstAssessedByPhysiotherapy) <= 4320 then 1 
			else 0
			end
		else null
		end
	,AQOccupationalTherapyAssessmentWithin72hrsOfEDAttendance = 
		case
		when QualityIndicators.AssessmentByAnOccupationalTherapistWithinFirst24Hours = 'NA' then NULL
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke','Inpatient Stroke') then 
			case
			when datediff(MINUTE,coalesce(InitialAssessment.DateTimeOfAttendedED,InitialAssessment.DateTimeOfOnsetStrokeSymptoms),AcuteCareBundle.DateTimeFirstAssessedByOccupationalTherapy) <= 4320 then 1 
			else 0
			end
		else null
		end
	
	,AQWeighed = 
		case
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke','Inpatient Stroke') then 
			case 
			when PATINDEX('%[0-9]%', AcuteCareBundle.Weighed) != 0 then 1
			else 0
			end
		else null
		end
	,StrokePhysicianAssessmentWithin72hrsOfEDAttendance = 
		case
		when QualityIndicators.WeighedAtLeastOnceDuringAttendance = 'NA' then NULL
		when InitialAssessment.TypeOfPresentation in('Suspect Stroke','Inpatient Stroke') then 
			case
			when datediff(MINUTE,coalesce(InitialAssessment.DateTimeOfAttendedED,InitialAssessment.DateTimeOfOnsetStrokeSymptoms),AcuteCareBundle.DateTimeFirstAssessedByStrokePhysician) <= 4320 then 1
			else 0
			end
		else null
		end
from 
	ETL.TImportSIDEncounter encounter
left outer join ETL.TImportSIDInitialAssessment InitialAssessment
on InitialAssessment.CandidateID = encounter.CandidateID

left outer join ETL.TImportSIDLikelyStroke LikelyStroke
on LikelyStroke.CandidateID = encounter.CandidateID

left outer join ETL.TImportSIDQualityIndicators QualityIndicators
on QualityIndicators.CandidateID = encounter.CandidateID

left outer join ETL.TImportSIDAcuteCareBundle AcuteCareBundle
on AcuteCareBundle.CandidateID = encounter.CandidateID
) encounter
)AQEncounter





﻿


CREATE VIEW [ETL].[TLoadAPCUpdateEncounter] AS


SELECT
	 Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.SourceEncounterNo
	,Encounter.ProviderSpellNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.NHSNumber

	,Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.DateOnWaitingList

	,AdmissionDate =
		cast(
			case
			when Encounter.AdmissionTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , Encounter.AdmissionDate + ' 23:59'))
			else Encounter.AdmissionDate
			end
			as date
		)

	,DischargeDate =
		cast(
			case
			when Encounter.DischargeTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , Encounter.DischargeDate + ' 23:59'))
			else Encounter.DischargeDate
			end
			as date
		)
	,EpisodeStartDate =
		cast(
			case
			when Encounter.EpisodeStartTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , Encounter.EpisodeStartDate + ' 23:59'))
			else Encounter.EpisodeStartDate
			end
			as date
		)

	,EpisodeEndDate =
		cast(
			case
			when Encounter.EpisodeEndTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , Encounter.EpisodeEndDate + ' 23:59'))
			else Encounter.EpisodeEndDate
			end
			as date
		)

	,Encounter.StartSiteCode
	,Encounter.StartWardTypeCode
	,Encounter.EndSiteCode
	,Encounter.EndWardTypeCode
	,Encounter.RegisteredGpCode

	,RegisteredGpPracticeCode = RegisteredGp.PracticeCode

	,Encounter.SiteCode
	,Encounter.AdmissionMethodCode
	,Encounter.AdmissionSourceCode
	,Encounter.PatientClassificationCode
	,Encounter.ManagementIntentionCode
	,Encounter.DischargeMethodCode
	,Encounter.DischargeDestinationCode

	,AdminCategoryCode =
	case
	when Encounter.AdminCategoryCode = 'NHS' then '01'
	when Encounter.AdminCategoryCode = 'PAY' then '02'
	when Encounter.AdminCategoryCode = 'IND' then '02'
	when Encounter.AdminCategoryCode = 'OV' then '02'
	when Encounter.AdminCategoryCode = 'AME' then '03'
	when Encounter.AdminCategoryCode = 'CAT' then '04'
	else '01'
	end

	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.LastEpisodeInSpellIndicator
	,Encounter.FirstRegDayOrNightAdmit
	,Encounter.NeonatalLevelOfCare

	,IsWellBabyFlag =
		case
		when NeonatalLevelOfCare = 0 and PrimaryDiagnosisCode like 'Z38%' then 1
		else 0
		end

	,Encounter.PASHRGCode

	,ClinicalCodingStatus = 
							CASE Encounter.ClinicalCodingStatus
								WHEN 'YES' THEN 'C'
								WHEN 'NO' THEN 'I'
							ELSE Encounter.ClinicalCodingStatus
							END

	,ClinicalCodingCompleteDate =
		cast(
			case
			when Encounter.ClinicalCodingCompleteTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , Encounter.ClinicalCodingCompleteDate + ' 23:59'))
			else Encounter.ClinicalCodingCompleteDate
			end
			as date
		)

	,ClinicalCodingCompleteTime =
		case
		when Encounter.ClinicalCodingCompleteTime = '24:00'
			then dateadd(minute , 1 , convert(smalldatetime , Encounter.ClinicalCodingCompleteDate + ' 23:59'))
		else convert(smalldatetime , Encounter.ClinicalCodingCompleteDate + ' ' + Encounter.ClinicalCodingCompleteTime)
		end

	,Encounter.PrimaryDiagnosisCode

	,PrimaryDiagnosisDate =
		substring(Encounter.PrimaryDiagnosisDate , 7 , 2) + '/' +
		substring(Encounter.PrimaryDiagnosisDate , 5 , 2) + '/' +
		substring(Encounter.PrimaryDiagnosisDate , 1 , 4)	

	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,Encounter.PrimaryOperationDate
	,Encounter.SecondaryOperationCode1
	,Encounter.SecondaryOperationDate1
	,Encounter.SecondaryOperationCode2
	,Encounter.SecondaryOperationDate2
	,Encounter.SecondaryOperationCode3
	,Encounter.SecondaryOperationDate3
	,Encounter.SecondaryOperationCode4
	,Encounter.SecondaryOperationDate4
	,Encounter.SecondaryOperationCode5
	,Encounter.SecondaryOperationDate5
	,Encounter.SecondaryOperationCode6
	,Encounter.SecondaryOperationDate6
	,Encounter.SecondaryOperationCode7
	,Encounter.SecondaryOperationDate7
	,Encounter.SecondaryOperationCode8
	,Encounter.SecondaryOperationDate8
	,Encounter.SecondaryOperationCode9
	,Encounter.SecondaryOperationDate9
	,Encounter.SecondaryOperationCode10
	,Encounter.SecondaryOperationDate10
	,Encounter.SecondaryOperationCode11
	,Encounter.SecondaryOperationDate11
	,Encounter.OperationStatusCode
	,Encounter.ContractSerialNo
	,Encounter.CodingCompleteDate
	,Encounter.PurchaserCode
	,Encounter.ProviderCode

	,EpisodeStartTime =
		case
		when Encounter.EpisodeStartTime = '24:00'
			then dateadd(minute , 1 , convert(smalldatetime , Encounter.EpisodeStartDate + ' 23:59'))
		else convert(smalldatetime , Encounter.EpisodeStartDate + ' ' + Encounter.EpisodeStartTime)
		end

	,EpisodeEndTime =
		case
		when Encounter.EpisodeEndTime = '24:00'
			then dateadd(minute , 1 , convert(smalldatetime , Encounter.EpisodeEndDate + ' 23:59'))
		else convert(smalldatetime , Encounter.EpisodeEndDate + ' ' + Encounter.EpisodeEndTime)
		end

	,Encounter.RegisteredGdpCode
	,Encounter.EpisodicGpCode
	,Encounter.InterfaceCode
	,Encounter.CasenoteNumber
	,Encounter.NHSNumberStatusCode

	,AdmissionTime =
		case
		when Encounter.AdmissionTime = '24:00'
			then dateadd(minute , 1 , convert(smalldatetime , Encounter.AdmissionDate + ' 23:59'))
		else convert(smalldatetime , Encounter.AdmissionDate + ' ' + Encounter.AdmissionTime)
		end

	,DischargeTime =
		case
		when Encounter.DischargeTime = '24:00'
			then dateadd(minute , 1 , convert(smalldatetime , Encounter.DischargeDate + ' 23:59'))
		else convert(smalldatetime , Encounter.DischargeDate + ' ' + Encounter.DischargeTime)
		end

	,Encounter.TransferFrom
	,Encounter.DistrictNo
	,Encounter.SourceUniqueID
	,Encounter.ExpectedLOS
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.IntendedPrimaryOperationCode
	,Encounter.Operation
	,Encounter.Research1
	,Encounter.Research2


	,CancelledElectiveAdmissionFlag =
	coalesce(
		Encounter.CancelledElectiveAdmissionFlag
		,0
	)

	,LocalAdminCategoryCode = Encounter.AdminCategoryCode
	,EpisodicGpPracticeCode = EpisodicGp.PracticeCode

	,PCTCode =
		coalesce(
			 Practice.ParentOrganisationCode
			,Postcode.PCTCode
			,'X98'
		)

	,LocalityCode = LocalAuthority.LocalAuthorityCode

	,FirstEpisodeInSpellIndicator =
		case
		when AdmissionTime = EpisodeStartTime and AdmissionDate = EpisodeStartDate then 'Y'
		else 'N'
		end

	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode

	,DecidedToAdmitDate = null
	,ResidencePCTCode = null

	,PatientCategoryCode =
	/*	case

		when AdmissionMethod.InternalCode in 
			(
			 '1'
			,'2'
			,'3'
			)
		and ManagementIntention.InternalCode in 
			(
			 '1'
			,'3'
			,'6'
			)
		then 'EL'

		when AdmissionMethod.InternalCode in 
			(
			 '1'
			,'2'
			,'3'
			)
		and ManagementIntention.InternalCode in
			(
			 '2'
			,'3'
			)
		and	Encounter.AdmissionDate < Encounter.DischargeDate
		then 'EL'

		when AdmissionMethod.InternalCode in 
			(
			 '1'
			,'2'
			,'3'
			)
		and ManagementIntention.InternalCode = '2'
		and	Encounter.AdmissionDate = Encounter.DischargeDate
		then 'DC'
		else 'NE'
		end
*/
case
			
			--Elective Inpatient
				when AdmissionMethod.InternalCode in 
					(
					 '1' --Waiting List
					,'2' --Booked
					,'3' --Planned
					)
				and ManagementIntention.InternalCode in 
					(
					 '1' --INPATIENT
					,'3' --INTERVAL ADMISSION
					,'6' --BORN IN HOSP/ON WAY
					)
				then 'EL' --Elective


			--Elective Inpatients where intended management was daycase but patient stayed overnight
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode = '2' -- DAY CASE
				and	Encounter.AdmissionDate < Encounter.DischargeDate
				then 'EL'--Elective


			--Elective Daycase
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode = '2'-- DAY CASE
				and	Encounter.AdmissionDate = Encounter.DischargeDate
				then 'DC'--Daycase
				
			
			--Elective Inaptient where intended management was daycase but not discharged
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3'--Planned
					)
				and ManagementIntention.InternalCode = '2'-- DAY CASE
				and	Encounter.DischargeDate IS NULL
				then 'EL'--Elective
			
			
			--Regular Day Case where intended management was Regular but patient stayed overnight
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
				    ,'3' --Regular
					)
				and ManagementIntention.InternalCode IN ('4','5')-- Regular day and night
				and	Encounter.AdmissionDate < Encounter.DischargeDate
				then 'EL'--Elective


			--Regular Day Case
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3' --Regular
					)
				and ManagementIntention.InternalCode IN ('4','5')-- Regular day and night
				and	Encounter.AdmissionDate = Encounter.DischargeDate
				then 'RD'--Regular
				
			
			--Regular Day Case where intended management was Regular but not discharged
				when AdmissionMethod.InternalCode in 
					(
					 '1'--Waiting List
					,'2'--Booked
					,'3' --Regular
					)
				and ManagementIntention.InternalCode IN ('4','5')-- Regular day and night
				and	Encounter.DischargeDate IS NULL
				then 'EL'--Elective	
			
						
			--Non Elective
				else 'NE' --Non Elective
			
			end

from
	ETL.TImportAPCUpdateEncounter Encounter

LEFT JOIN PAS.Gp EpisodicGp 
ON	Encounter.EpisodicGpCode = EpisodicGp.GpCode

LEFT JOIN PAS.Gp RegisteredGp 
ON	Encounter.RegisteredGpCode = RegisteredGp.GpCode

left join [$(Organisation)].dbo.Practice Practice
on	Practice.OrganisationCode = coalesce(EpisodicGp.PracticeCode, RegisteredGp.PracticeCode)

left join [$(Organisation)].dbo.Postcode Postcode
on	Postcode.Postcode =
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

left join [$(Organisation)].dbo.LocalAuthority LocalAuthority
on	LocalAuthority.LocalAuthorityCode = Postcode.LocalAuthorityCode

left join PAS.AdmissionMethod
on	Encounter.AdmissionMethodCode = AdmissionMethod.AdmissionMethodCode

left join PAS.ManagementIntention ManagementIntention
on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode






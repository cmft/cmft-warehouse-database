﻿CREATE view [ETL].[TLoadAPCSuspension] as

select distinct
	 SourcePatientNo
	,SourceEncounterNo
	,SuspensionStartDate = convert(smalldatetime, SuspensionStartDate)
	,SuspensionEndDate = convert(smalldatetime, SuspensionEndDate)
	,SuspensionReasonCode
	,SuspensionReason
from
	ETL.TImportAPCSuspension

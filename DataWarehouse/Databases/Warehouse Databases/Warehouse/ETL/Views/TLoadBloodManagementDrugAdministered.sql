﻿

--Use Warehouse

CREATE View [ETL].[TLoadBloodManagementDrugAdministered]

as

select 
	SourceUniqueID	
	,EpisodeKey	
	,HospitalNumber	
	,LastName	
	,FirstName	
	,DateOfBirth	
	,GenderCode
	,SpecialtyCode	
	,ConsultantCode
	,IntendedDestination	
	,ActualDestination	
	,DrugCode	
	,Drug
	,Category
	,Dose	
	,UnitsCode
	,Units
	,DrugAdministeredTime	
	,SessionType
	,SessionLocationCode
	,InterfaceCode
from 
	ETL.TImportBloodManagementDrugAdministered



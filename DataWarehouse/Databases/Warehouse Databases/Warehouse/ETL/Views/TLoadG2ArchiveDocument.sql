﻿
create view ETL.TLoadG2ArchiveDocument 

as

select 
	--DocumentCode int, 
	SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length]
	,[DocumentTypeCode]
	,[Priority]
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate]
	,[CorrectionTime]
	,[WaitingTimeAuthorization]
	,[AuthorisationDate]
	,[PassThroughTime]
	,[Reserved01]
	,[Reserved02]
	,[Reserved03]
	,[Reserved04]
	,[Reserved05]
	,[Reserved06]
	,[Reserved07]
	,[Reserved08]
	,[Reserved09]
	,[Reserved10]
	,[ContextCode]
	,[DictationTime]
	,[CorrectionRate]
from
	ETL.TImportG2ArchiveDocument
	

﻿CREATE view [ETL].[TLoadPASDelivery] as

select
	 DeliveryID = DELIVERYID
	,Stage1LabourLength = A1stStage
	,Stage2LabourLength = A2ndStage
	--,AdtMaternityProfessionalPriorInvolvementInt
	,AnaestheticGivenDuringCode = AnaesanalgDuring

	,EpisodeEndTime = 
		cast(
			case
			when right(ConsEpiEndDtTmInt , 4) = '2400' then 
				dateadd(minute , 1 , cast(left(ConsEpiEndDtTmInt , 8) + ' 23:59' as smalldatetime))
			else left(ConsEpiEndDtTmInt , 8) + ' ' + substring(ConsEpiEndDtTmInt , 9 , 2) + ':' + substring(ConsEpiEndDtTmInt , 11 , 2)
			end
			as smalldatetime
		)

	--,ConsEpiEndDtTmInt
	,EpisodeStartTime = 
		cast(
			case
			when right(ConsEpiStartDtTmInt , 4) = '2400' then 
				dateadd(minute , 1 , cast(left(ConsEpiStartDtTmInt , 8) + ' 23:59' as smalldatetime))
			else left(ConsEpiStartDtTmInt , 8) + ' ' + substring(ConsEpiStartDtTmInt , 9 , 2) + ':' + substring(ConsEpiStartDtTmInt , 11 , 2)
			end
			as smalldatetime
		)

	--,ConsEpiStartDtTmInt
	,SourceSpellNo = EpisodeNumber
	,EpisodeNo = FCENumber

	,FirstAnteNatalAssessmentDate = 
		cast(
			case
			when isdate(FirstAnteNatalAssessmentDate) = 0
			then null
			else FirstAnteNatalAssessmentDate
			end
			as smalldatetime
		)

	,GestationLength = cast(Gestationonset as int)
	,SourcePatientNo = InternalPatientNumber
	,AnaestheticGivenReasonCode = KeMatAnaesReas
	,AnaestheticTypeCode = KeMatAnaesType
	,AnaestheticGivenDate = KeMatDate1stAnaNeut
	,LabourOnsetMethodCode = KeMatMethOnsetInt
	,IntendedDeliveryPlaceTypeCode = KeMatOrigIntInt
	,ActualDeliveryPlaceTypeCode = KeMatPlaceOfDelivInt
	,DeliveryPlaceChangeReasonCode = KeMatReasChgInt
	,StatusOfPersonConductingDeliveryCode = KeMatStatPersCondDelInt
	,Parity
	,PreviousPregnancies = PregnancyNumber
	--,SmokedPrior
	--,SmokedPriorInt
	--,SmokedTimeOfBooking
	--,SmokedTimeOfBookInt
	--,SmokedTimeOfDelivery
	--,SmokedTimeOfDelivInt
from
	ETL.TImportPASDelivery

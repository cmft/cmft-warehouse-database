﻿
CREATE VIEW [ETL].[TLoadDM01AudiologyWait] 
AS

	/******************************************************************************
	**  Name: ETL.TLoadDM01AudiologyWait
	**  Purpose: 
	**
	**  Import procedure view for DM01 Audiology Waiting List table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 06.11.12    MH         Created
	** 18.01.12    MH         Included District Number, Surname and Forenames
	******************************************************************************/

SELECT 	 
	 	 CensusDate				= CONVERT(DATETIME, TImportDM01AudiologyWait.Census, 103)
		,SourcePatientNo		= PATIENTNO.SourcePatientNo
		,DistrictNo				= PATIENTDETAILS.DistrictNo
		,Surname				= PATIENTDETAILS.Surname
		,Forenames				= PATIENTDETAILS.Forenames
		,AppointmentDate		= CONVERT(DATETIME, TImportDM01AudiologyWait.APPT_DATE, 103)
		,AppointmentType		= TImportDM01AudiologyWait.APPOINTMENT_TYPE
		,ReferralDate			= CONVERT(DATETIME, TImportDM01AudiologyWait.REFDATE, 103)
		,TCIDate				= CONVERT(DATETIME, TImportDM01AudiologyWait.TBSB, 103)
		,WLStatus				= TImportDM01AudiologyWait.Status
		,WaitTimeAtCensusDate	= CAST(TImportDM01AudiologyWait.[Wait Time @ Census] AS INT)
		,WaitTimeAtAppointment	= CAST(TImportDM01AudiologyWait.[Wait Time @ Appt] AS INT)
		,PTLTreatByMonth		= CAST(TImportDM01AudiologyWait.[PTL wks for 6 weeks] AS INT)
		,CaseNoteNumber			= COALESCE(PATIENTNO.CaseNoteNumber, TImportDM01AudiologyWait.HOSPITAL_NO)
		,PCTCode				= COALESCE(PATIENTDETAILS.PCTCode,'5NT')

FROM
	ETL.TImportDM01AudiologyWait 
	OUTER APPLY [Utility].[fn_GetPatientFromCaseNote](TImportDM01AudiologyWait.HOSPITAL_NO) PATIENTNO
	OUTER APPLY [Utility].[fn_GetPatientDetails](PATIENTNO.SourcePatientNo) PATIENTDETAILS


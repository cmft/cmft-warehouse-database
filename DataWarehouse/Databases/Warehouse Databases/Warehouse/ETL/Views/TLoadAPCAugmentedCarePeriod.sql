﻿
create view ETL.TLoadAPCAugmentedCarePeriod

as

select
	SourceUniqueID 
	,SourcePatientNo 
	,SourceSpellNo 
	,ProviderSpellNo = SourcePatientNo + '/' + SourceSpellNo
	,ConsultantCode 
	,AcpDisposalCode 
	,LocalIdentifier 
	,LocationCode 
	,OutcomeIndicator 
	,Source 
	,SpecialtyCode 
	,Status
	,StartDate =
				cast(
					case
						when StartTime = '24:00'
						then dateadd(minute , 1 , convert(smalldatetime, StartDate + ' 23:59'))
						else left(StartDate, 10)
					end
					as date
					)
	,StartTime = 
				cast(
					case
						when StartTime = '24:00'
						then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
						else convert(smalldatetime ,StartDate + ' ' + StartTime)
						end
					as datetime
					)
	,EndDate = 
			cast(
				case
					when EndTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime, EndDate) + ' 23:59')
					else replace(EndDate, '0000','')
				end
				as date
				)
	,EndTime =
			cast(
				case
					when EndTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime, EndDate + ' 23:59'))
					else convert(smalldatetime ,EndDate + ' ' + EndTime)
					end
				as datetime
				)
	,AdvancedRespiratorySystemIndicator 
	,BasicRespiratorySystemIndicator 
	,CirculatorySystemIndicator 
	,NeurologicalSystemIndicator 
	,RenalSystemIndicator 
	,NoOfSupportSystemsUsed 
	,HighDependencyCareLevelDays 
	,IntensiveCareLevelDays 
	,PlannedAcpPeriodIndicator 
	,ReviseByUserId
	,ReviseTime	
from
	ETL.TImportAPCAugmentedCarePeriod

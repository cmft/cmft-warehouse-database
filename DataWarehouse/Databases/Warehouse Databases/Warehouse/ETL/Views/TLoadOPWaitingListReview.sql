﻿



CREATE view [ETL].[TLoadOPWaitingListReview] as

select
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,ReferralDate
	,BookedDate
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,CasenoteNumber

-- changed 31 Mar 2005 - PDO
-- add check for DNAs as clock restarts following a DNA
	,KornerWait =
		--case
		--when
		--	datediff(
		--		 day
		--		,convert(smalldatetime, WL.QM08StartWaitDate)
		--		,WL.CensusDate
		--	) < 0
		--then datediff(day, 
		--	coalesce(
		--		(
		--		select
		--			max(convert(smalldatetime, MaxWL.AppointmentDate)) 
		--		from
		--			ETL.TImportOPWaitingListReview MaxWL
		--		where
		--			MaxWL.SourcePatientNo = WL.SourcePatientNo
		--		and	MaxWL.SourceEncounterNo = WL.SourceEncounterNo
		--		and MaxWL.WaitingListCode = WL.WaitingListCode -- advised by Dusia 2 Oct 2015
		--		and	convert(smalldatetime, MaxWL.AppointmentDate) < MaxWL.CensusDate
		--		and	(
		--				MaxWL.CancelledBy != 'H'
		--			or	MaxWL.AppointmentStatusCode in ('DNA', 'CND', 'NATT')
		--			)
		--		)
		--		, convert(smalldatetime, WL.ReferralDate))
		--	, WL.CensusDate)
		--else datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate)
		--end  
	datediff(day, convert(date, WL.QM08StartWaitDate), WL.CensusDate)
	
	--datediff(day,WL.QM08StartWaitDate, WL.CensusDate)

	,InterfaceCode = 'INQ'
	,LocalEpisodicGpCode
	,LocalRegisteredGpCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode

-- BreachDate
-- Set the BreachDate to CensusDate + (breach days - Days waiting)
	,BreachDate =
		convert(smalldatetime, left(convert(varchar, 
			dateadd(day,
				coalesce(
					convert(int, ConsultantBreachDays.Description), 
					convert(int, SpecialtyBreachDays.Description), 
					(select NumericValue from Utility.Parameter where Parameter = 'OPDEFAULTBREACHDAYS')
				)
			-	
			--case
			--when WL.RTTStartDate is not null then datediff(day, WL.QM08StartWaitDate, WL.CensusDate)
			--when datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate) < 0
			--then datediff(day, 
			--	coalesce(
			--		(
			--		select
			--			max(convert(smalldatetime, MaxWL.AppointmentDate)) 
			--		from
			--			ETL.TImportOPWaitingListReview MaxWL
			--		where
			--			MaxWL.SourcePatientNo = WL.SourcePatientNo
			--		and	MaxWL.SourceEncounterNo = WL.SourceEncounterNo
			--		and	convert(smalldatetime, MaxWL.AppointmentDate) < MaxWL.CensusDate
			--		and	(
			--				MaxWL.CancelledBy != 'H'
			--			or	MaxWL.AppointmentStatusCode in ('DNA', 'CND', 'NATT')
			--			)
			--		)
			--		,convert(smalldatetime, WL.RTTStartDate)
			--		, convert(smalldatetime, WL.ReferralDate))
			--	, WL.CensusDate)
			--else datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate)
			--end
			datediff(day, convert(date, WL.QM08StartWaitDate), WL.CensusDate)
			, WL.CensusDate)
		, 113), 11))


-- NationalBreachDate
-- Set the BreachDate to CensusDate + (breach days - Days waiting)
	,NationalBreachDate =
		convert(smalldatetime, left(convert(varchar, 
			dateadd(day,
				coalesce(
					convert(int, NationalConsultantBreachDays.Description), 
					convert(int, NationalSpecialtyBreachDays.Description), 
					(select NumericValue from Utility.Parameter where Parameter = 'NATIONALOPDEFAULTBREACHDAYS')
				)
			-	
			--case
			--when datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate) < 0
			--then datediff(day, 
			--	coalesce(
			--		(
			--		select
			--			max(convert(smalldatetime, MaxWL.AppointmentDate)) 
			--		from
			--			ETL.TImportOPWaitingListReview MaxWL
			--		where
			--			MaxWL.SourcePatientNo = WL.SourcePatientNo
			--		and	MaxWL.SourceEncounterNo = WL.SourceEncounterNo
			--		and	convert(smalldatetime, MaxWL.AppointmentDate) < MaxWL.CensusDate
			--		and	(
			--				MaxWL.CancelledBy != 'H'
			--			or	MaxWL.AppointmentStatusCode in ('DNA', 'CND', 'NATT')
			--			)
			--		)
			--		,convert(smalldatetime, WL.RTTStartDate)
			--		, convert(smalldatetime, WL.ReferralDate))
			--	, WL.CensusDate)
			--else datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate)
			--end
			datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate)
			, WL.CensusDate)
		, 113), 11)
		)

	,FuturePatientCancelDate
	,AdditionFlag
	,LastAppointmentFlag

	,BreachDays =
		coalesce(
		convert(int, ConsultantBreachDays.Description), 
		convert(int, SpecialtyBreachDays.Description), 
		(select NumericValue from Utility.Parameter where Parameter = 'OPDEFAULTBREACHDAYS')
		)

	,NationalBreachDays = 
		coalesce(
		convert(int, ConsultantBreachDays.Description), 
		convert(int, SpecialtyBreachDays.Description), 
		(select NumericValue from Utility.Parameter where Parameter = 'NATIONALOPDEFAULTBREACHDAYS')
		)

	,MRSA

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag

	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode


-- Set the BreachDate to CensusDate + (breach days - Days waiting)
	,RTTBreachDate =
		convert(
			smalldatetime
			,left(
				convert(
					varchar
					,dateadd(
						day
						,(select NumericValue from Utility.Parameter where Parameter = 'RTTBREACHDAYS')
						-
						--case
						--when WL.RTTStartDate is not null 
						--then datediff(day, WL.QM08StartWaitDate, WL.CensusDate)
						--when datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate) < 0
						--then datediff(
						--		day
						--		,coalesce(
						--			(
						--			select
						--				max(convert(smalldatetime, MaxWL.AppointmentDate)) 
						--			from
						--				ETL.TImportOPWaitingListReview MaxWL
						--			where
						--				MaxWL.SourcePatientNo = WL.SourcePatientNo
						--			and	MaxWL.SourceEncounterNo = WL.SourceEncounterNo
						--			and	convert(smalldatetime, MaxWL.AppointmentDate) < MaxWL.CensusDate
						--			and	(
						--					MaxWL.CancelledBy != 'H'
						--				or	MaxWL.AppointmentStatusCode in ('DNA', 'CND', 'NATT')
						--				)
						--			)
						--			,convert(smalldatetime, WL.RTTStartDate)
						--			,convert(smalldatetime, WL.ReferralDate)
						--		)
						--		,WL.CensusDate
						--	)
						--else datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate)
						--end
						datediff(day, convert(smalldatetime, WL.QM08StartWaitDate), WL.CensusDate)
						,WL.CensusDate
						)
					,113
				)
			,11
			)
		)

	,ClockStartDate = WL.QM08StartWaitDate
		--convert(
		--	smalldatetime
		--	,coalesce(
		--		(
		--		select
		--			max(convert(smalldatetime, MaxWL.AppointmentDate)) 
		--		from
		--			ETL.TImportOPWaitingListReview MaxWL
		--		where
		--			MaxWL.SourcePatientNo = WL.SourcePatientNo
		--		and	MaxWL.SourceEncounterNo = WL.SourceEncounterNo
		--		and	convert(smalldatetime, MaxWL.AppointmentDate) < MaxWL.CensusDate
		--		and	(
		--				MaxWL.CancelledBy != 'H'
		--			or	MaxWL.AppointmentStatusCode in ('DNA', 'CND', 'NATT')
		--			)
		--		)
		--		,WL.RTTStartDate
		--		,WL.QM08StartWaitDate
		--		,WL.ReferralDate
		--	)
		--)

	,WL.WLAppointmentTypeCode
	,WL.PatientDeathIndicator
	,WL.ScheduledCancelReasonCode
	,WL.FirstAttendanceFlag
from
	(
	select
		 WL.SourcePatientNo 
		,WL.SourceEncounterNo

--		,SourceEncounterNo =
--		WL.SourceEncounterNo + 
--		case
--		when WL.AppointmentID is null then
--			case
--			when coalesce(WL.ClinicCode, '') = '' then ''
--			else '~' + WL.ClinicCode
--			end + 
--			case
--			when coalesce(WL.AppointmentTime, '') = '' then ''
--			else '~' + WL.AppointmentTime
--			end + 
--			case
--			when coalesce(WL.BookedDate, '') = '' then ''
--			else '~' + WL.BookedDate
--			end  + 
--			case
--			when coalesce(WL.BookedTime, '') = '' then ''
--			else '~' + WL.BookedTime
--			end
--		else '~' + WL.AppointmentID
--		end, 

		,WL.CensusDate
		,WL.PatientSurname
		,WL.PatientForename
		,WL.PatientTitle
		,WL.SexCode
		,WL.DateOfBirth

		,WL.DateOfDeath
		,WL.NHSNumber
		,WL.DistrictNo
		,WL.ReferralDate

		,BookedDate = 
			case
			when WL.BookedTime = '24:00' then
				dateadd(minute , 1 , convert(smalldatetime , WL.BookedDate + ' 23:59'))
			else WL.BookedDate + ' ' + WL.BookedTime
			end

		,WL.AppointmentDate
		,WL.AppointmentTypeCode
		,WL.AppointmentStatusCode
		,WL.AppointmentCategoryCode


	-- if the next appointment has been cancelled by the patient and the appointment
	-- date is prior to the census date then use it otherwise use the referral date
		,QM08StartWaitDate = convert(date,QM08StartWaitDate)
		--	coalesce(
		----	WL.RTTStartDate --turned off until Peter has explained to Roman et al
		--	null
		--	,(
		--	select	max(convert(smalldatetime, MaxWL.AppointmentDate)) 
		--	from	ETL.TImportOPWaitingListReview MaxWL
		--	where
		--		MaxWL.SourcePatientNo = WL.SourcePatientNo
		--	and	MaxWL.SourceEncounterNo = WL.SourceEncounterNo
		--	and	MaxWL.AppointmentDate < MaxWL.CensusDate
		--	and	(
		--			(
		--				MaxWL.CancelledBy is not null
		--			and	MaxWL.CancelledBy != 'H'
		--			)
		--			or	coalesce(MaxWL.AppointmentStatusCode, 'X') != 'NR'
		--		)
		--	), WL.ReferralDate) 

	--	QM08StartWaitDate QM08StartWaitDateOrig,

		,WL.QM08EndWaitDate, 

		AppointmentTime =
			case
			when substring(WL.AppointmentTime, 9,2) = '24'
				then dateadd(minute , 1 , convert(smalldatetime , left(WL.AppointmentTime , 8) + ' 23:59'))
			else
				left(WL.AppointmentTime , 8) + ' ' +
				substring(WL.AppointmentTime, 9,2) + ':' +
				substring(WL.AppointmentTime,11,2)
			end

		,WL.ClinicCode
		,WL.SourceOfReferralCode
		,WL.MaritalStatusCode
		,WL.ReligionCode

		,Postcode =
			case
			when datalength(rtrim(ltrim(WL.Postcode))) = 6 then left(WL.Postcode, 2) + '   ' + right(WL.Postcode, 3)
			when datalength(rtrim(ltrim(WL.Postcode))) = 7 then left(WL.Postcode, 3) + '  ' + right(WL.Postcode, 3)
			else WL.Postcode
			end

		,WL.PatientsAddress1
		,WL.PatientsAddress2
		,WL.PatientsAddress3
		,WL.PatientsAddress4
		,WL.DHACode
		,WL.HomePhone
		,WL.WorkPhone
		,WL.EthnicOriginCode
		,WL.ConsultantCode

		,SpecialtyCode =
			coalesce(
				 ConsultantToSpecialtyMap.XrefEntityCode
				,SpecialtyToSpecialtyMap.XrefEntityCode
				,WL.SpecialtyCode
			)

		,PASSpecialtyCode = WL.SpecialtyCode 
		,WL.PriorityCode
		,WL.WaitingListCode
		,WL.CommentClinical
		,WL.CommentNonClinical
		,WL.SiteCode
		,WL.ProviderCode
		,WL.PurchaserCode
		,WL.ContractSerialNumber
		,WL.CancelledBy
		,WL.CasenoteNumber

		,LocalEpisodicGpCode = WL.EpisodicGpCode 
		,LocalRegisteredGpCode = WL.RegisteredGpCode 

		,EpisodicGpCode = EpisodicPractice.NationalCode 
		,WL.EpisodicGpPracticeCode
		,RegisteredGpCode = RegisteredPractice.NationalCode 
		,RegisteredPracticeCode = RegisteredPractice.PracticeCode 
		,SourceTreatmentFunctionCode = null 
		,TreatmentFunctionCode = null 
		,Specialty.NationalSpecialtyCode

		,PCTCode =
			rtrim(
				left(
					coalesce(
						Practice.ParentOrganisationCode,
						Postcode.PCTCode,
						WL.PurchaserCode,
						'OAT  '
					) + '00000'
				, 5
				)
			)
		

	-- if the next appointment has been cancelled by the patient and the appointment
	-- date is after the census date then use it
		,FuturePatientCancelDate =
			(
			select	min(convert(smalldatetime, MaxWL.AppointmentDate)) 
			from	ETL.TImportOPWaitingListReview MaxWL
			where
				MaxWL.SourcePatientNo = WL.SourcePatientNo
			and	MaxWL.SourceEncounterNo = WL.SourceEncounterNo
			and MaxWL.WaitingListCode = WL.WaitingListCode
			and	MaxWL.AppointmentDate >= MaxWL.CensusDate
			and	(
					(
						MaxWL.CancelledBy is not null
					and	MaxWL.CancelledBy != 'H'
					)
					or	coalesce(MaxWL.AppointmentStatusCode, 'X') != 'NR'
				)
			)

		,AdditionFlag =
			case
			when not exists
				(
					select
						1
					from
						OP.WaitingListReview LastWL
					where
						LastWL.CensusDate = dateadd(day, -7, convert(smalldatetime, left(convert(varchar, WL.CensusDate, 113), 11)))
					and	LastWL.SourcePatientNo = WL.SourcePatientNo
					and	LastWL.ConsultantCode = WL.ConsultantCode
				) then 1
			else 0 
			end 

	-- flag if this is the final appointment date for this patient episode
		,LastAppointmentFlag =
			case

			when WL.AppointmentTime is null
			then 0

			when
				WL.AppointmentTime =
				(	
				select
					max(LastWL.AppointmentTime)
				from
					ETL.TImportOPWaitingListReview LastWL
				where
					LastWL.SourcePatientNo = WL.SourcePatientNo
				and	LastWL.CensusDate = WL.CensusDate
				and	LastWL.SourceEncounterNo = WL.SourceEncounterNo
				and	LastWL.WaitingListCode = WL.WaitingListCode
				and	LastWL.CancelledBy is null
				and	LastWL.AppointmentStatusCode not in ('DNA', 'CND', 'NATT')
				) then 1

			when exists
				(
				select
					1
				from
					ETL.TImportOPWaitingListReview LastWL
				where
					LastWL.SourcePatientNo = WL.SourcePatientNo
				and	LastWL.CensusDate = WL.CensusDate
				and	LastWL.SourceEncounterNo = WL.SourceEncounterNo
				and	LastWL.WaitingListCode = WL.WaitingListCode
				and	LastWL.CancelledBy is null
				and	LastWL.AppointmentStatusCode not in ('DNA', 'CND', 'NATT')
				) then 0

			when not exists
				(	
				select
					1
				from
					ETL.TImportOPWaitingListReview LastWL
				where
					LastWL.SourcePatientNo = WL.SourcePatientNo
				and	LastWL.CensusDate = WL.CensusDate
				and	LastWL.SourceEncounterNo = WL.SourceEncounterNo
				and	LastWL.WaitingListCode = WL.WaitingListCode
				and	
					(
						LastWL.AppointmentTime > WL.AppointmentTime
					or	(
							LastWL.AppointmentTime = WL.AppointmentTime
						--and	convert(smalldatetime, LastWL.BookedDate + ' ' + LastWL.BookedTime) > 
						--	convert(smalldatetime, WL.BookedDate + ' ' + WL.BookedTime)
						and	convert(smalldatetime, 
								case
								when LastWL.BookedTime = '24:00' then
									dateadd(minute , 1 , convert(smalldatetime , LastWL.BookedDate + ' 23:59'))
								else LastWL.BookedDate + ' ' + LastWL.BookedTime
								end
							) > 
							convert(smalldatetime,
								case
								when WL.BookedTime = '24:00' then
									dateadd(minute , 1 , convert(smalldatetime , WL.BookedDate + ' 23:59'))
								else WL.BookedDate + ' ' + WL.BookedTime
								end
							)
						)
					)
				)
			then 1

			else 0
			end

		,WL.MRSA

		,WL.RTTPathwayID
		,WL.RTTPathwayCondition
		,WL.RTTStartDate
		,WL.RTTEndDate
		,WL.RTTSpecialtyCode
		,WL.RTTCurrentProviderCode
		,WL.RTTCurrentStatusCode
		,WL.RTTCurrentStatusDate
		,WL.RTTCurrentPrivatePatientFlag
		,WL.RTTOverseasStatusFlag

		,CountOfDNAs = Counts.DNAs
		,CountOfHospitalCancels = Counts.HospitalCancels
		,CountOfPatientCancels = Counts.PatientCancels
		,WL.AppointmentID
		,WL.SourceUniqueID
		,WL.AdminCategoryCode
		,WL.WLAppointmentTypeCode
		,PatientDeathIndicator =
			coalesce(WL.PatientDeathIndicator , 0)
		,WL.ScheduledCancelReasonCode

		--this is a review (based on the where clause of the ETL.ExtractInquireOPWaitingListReview procedure
		,FirstAttendanceFlag = '2'
	FROM
		ETL.TImportOPWaitingListReview WL

	INNER JOIN PAS.SourceOfReferral 
	ON	WL.SourceOfReferralCode = SourceOfReferral.SourceOfReferralCode

	INNER JOIN PAS.Site Site
	ON	WL.SiteCode = Site.SiteCode

	LEFT JOIN PAS.Gp AS EpisodicPractice 
	ON	WL.EpisodicGpCode = EpisodicPractice.GpCode

	LEFT JOIN PAS.Gp AS RegisteredPractice 
	ON	WL.RegisteredGpCode = RegisteredPractice.GpCode

	left join [$(Organisation)].dbo.Practice Practice
	on	Practice.OrganisationCode = coalesce(EpisodicPractice.PracticeCode, RegisteredPractice.PracticeCode)

	left join [$(Organisation)].dbo.Postcode Postcode
	on	Postcode.Postcode = WL.Postcode

	left join EntityXref ConsultantToSpecialtyMap
	on	ConsultantToSpecialtyMap.EntityTypeCode = 'CONSULTANT'
	and	ConsultantToSpecialtyMap.XrefEntityTypeCode = 'SPECIALTY'
	and	ConsultantToSpecialtyMap.EntityCode = WL.ConsultantCode

	left join EntityXref SpecialtyToSpecialtyMap
	on	SpecialtyToSpecialtyMap.EntityTypeCode = 'SPECIALTY'
	and	SpecialtyToSpecialtyMap.XrefEntityTypeCode = 'SPECIALTY'
	and	SpecialtyToSpecialtyMap.EntityCode = WL.SpecialtyCode

	LEFT JOIN PAS.Specialty
	ON	WL.SpecialtyCode = Specialty.SpecialtyCode

	left join 
		(
		select
			SourcePatientNo,
			SourceEncounterNo,
			sum(case when AppointmentStatusCode in ('DNA', 'CND') then 1 else 0 end) DNAs,
			sum(case when CancelledBy = 'H' then 1 else 0 end) HospitalCancels,
			sum(case when CancelledBy = 'P' then 1 else 0 end) PatientCancels
		from
			ETL.TImportOPWaitingListReview
		where
			AppointmentStatusCode in ('DNA', 'CND')
		or	CancelledBy is not null
		
		group by
			SourcePatientNo,
			SourceEncounterNo

		) Counts
	on	Counts.SourcePatientNo = WL.SourcePatientNo
	and	Counts.SourceEncounterNo = WL.SourceEncounterNo

	) WL

left join dbo.EntityLookup ConsultantBreachDays
on	ConsultantBreachDays.EntityTypeCode = 'OPCONSULTANTBREACHDAYS'
and	ConsultantBreachDays.EntityCode = WL.ConsultantCode

left join dbo.EntityLookup SpecialtyBreachDays
on	SpecialtyBreachDays.EntityTypeCode = 'OPSPECIALTYBREACHDAYS'
and	SpecialtyBreachDays.EntityCode = WL.SpecialtyCode

left join dbo.EntityLookup NationalConsultantBreachDays
on	NationalConsultantBreachDays.EntityTypeCode = 'NATIONALOPCONSULTANTBREACHDAYS'
and	NationalConsultantBreachDays.EntityCode = WL.ConsultantCode

left join dbo.EntityLookup NationalSpecialtyBreachDays
on	NationalSpecialtyBreachDays.EntityTypeCode = 'NATIONALOPSPECIALTYBREACHDAYS'
and	NationalSpecialtyBreachDays.EntityCode = WL.SpecialtyCode
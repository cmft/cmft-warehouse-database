﻿CREATE VIEW [ETL].[TLoadRenalPatientVascularAccessEventsBase] ([ObjectID], [PatientObjectID], [PatientFullName], [PatientMedicalRecordNo], [StartDate_Local], [StopDate_Local], [DaysElapsed], [AgeAtEvent], [TimelineEvent], [TimelineEventDetail], [InformationSource], [Notes], [Etiology], [AccessType], [AccessLocation], [PermanentStatus], [Brand], [CatheterType], [Configuration], [Elasticity], [ExternalStatus], [NeedleGuage], [NeedleLength], [NeedleType], [NeedleGuage2], [NeedleLength2], [NeedleType2], [VenousLumen], [ArterialLumen], [Preparation], [PrimingProcedures], [Surgeon], [VascularAccessObjectID])
as select
/* ObjectID ObjectID */ vascularac0_.[oid] as col_0_0_,
/* PatientObjectID Patient.ObjectID */ patient1_.[oid] as col_1_0_,
/* PatientFullName Patient.Person.Name */ person2_.[Name] as col_2_0_,
/* PatientMedicalRecordNo Patient.MedicalRecordNo */ patient1_.[MedicalRecordNo] as col_3_0_,
/* StartDate_Local PatientDateRange.PatientStartDate */ patientdat3_.[PatientStartDate] as col_4_0_,
/* StopDate_Local PatientDateRange.PatientStopDate */ patientdat3_.[PatientStopDate] as col_5_0_,
/* DaysElapsed PatientDateRange.DaysElapsed */ case when cast(case when patientdat3_.[PatientStopDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStopDate] as float)) as datetime) end as int)-cast(case when patientdat3_.[PatientStartDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) end as int)>0 then cast(case when patientdat3_.[PatientStopDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStopDate] as float)) as datetime) end as int)-cast(case when patientdat3_.[PatientStartDate] is null then cast(floor(cast(getdate() as float)) as datetime) else cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) end as int) else null end as col_6_0_,
/* AgeAtEvent AgeAtEvent */ case when (person2_.[DateOfBirth] is not null) and (patientdat3_.[PatientStartDate] is not null) then floor((cast(cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) as int)-cast(cast(floor(cast(person2_.[DateOfBirth] as float)) as datetime) as int))/365.25) else null end as col_7_0_,
/* TimelineEvent TimelineEvent */ code0.ClinicalVisionCore_UKEnglish as col_8_0_,
/* TimelineEventDetail TimelineEventDetail */ code1.ClinicalVisionCore_UKEnglish as col_9_0_,
/* InformationSource InformationSource */ code2.ClinicalVisionCore_UKEnglish as col_10_0_,
/* Notes Notes */ vascularac0_1_.[Notes] as col_11_0_,
/* Etiology VascularEventEtiology */ code3.ClinicalVisionCore_UKEnglish as col_12_0_,
/* AccessType AccessDefinition.VascularAccessType */ code4.ClinicalVisionCore_UKEnglish as col_13_0_,
/* AccessLocation AccessDefinition.VascularAccessLocation */ code5.ClinicalVisionCore_UKEnglish as col_14_0_,
/* PermanentStatus AccessDefinition.VascularAccessPermanentStatus */ code6.ClinicalVisionCore_UKEnglish as col_15_0_,
/* Brand AccessDefinition.VascularAccessBrand */ code7.ClinicalVisionCore_UKEnglish as col_16_0_,
/* CatheterType AccessDefinition.VascularAccessCatheterType */ code8.ClinicalVisionCore_UKEnglish as col_17_0_,
/* Configuration AccessDefinition.VascularAccessConfiguration */ code9.ClinicalVisionCore_UKEnglish as col_18_0_,
/* Elasticity AccessDefinition.VascularAccessElasticity */ code10.ClinicalVisionCore_UKEnglish as col_19_0_,
/* ExternalStatus AccessDefinition.VascularAccessExternalStatus */ code11.ClinicalVisionCore_UKEnglish as col_20_0_,
/* NeedleGuage AccessDefinition.VascularAccessNeedleGauge */ code12.ClinicalVisionCore_UKEnglish as col_21_0_,
/* NeedleLength AccessDefinition.VascularAccessNeedleLength */ code13.ClinicalVisionCore_UKEnglish as col_22_0_,
/* NeedleType AccessDefinition.VascularAccessNeedleType */ code14.ClinicalVisionCore_UKEnglish as col_23_0_,
/* NeedleGuage2 AccessDefinition.VascularAccessNeedleGauge2 */ code15.ClinicalVisionCore_UKEnglish as col_24_0_,
/* NeedleLength2 AccessDefinition.VascularAccessNeedleLength2 */ code16.ClinicalVisionCore_UKEnglish as col_25_0_,
/* NeedleType2 AccessDefinition.VascularAccessNeedleType2 */ code17.ClinicalVisionCore_UKEnglish as col_26_0_,
/* VenousLumen AccessDefinition.VenousLumen */ accessdefi4_.[VenousLumen] as col_27_0_,
/* ArterialLumen AccessDefinition.ArterialLumen */ accessdefi4_.[ArterialLumen] as col_28_0_,
/* Preparation AccessDefinition.Preparation */ accessdefi4_.[Preparation] as col_29_0_,
/* PrimingProcedures AccessDefinition.PrimingProcedures */ accessdefi4_.[PrimingProcedures] as col_30_0_,
/* Surgeon Surgeon.LookUp */ case when medicalsta5_.[MedicalStaffOID] is not null then person8_.[Name] when otherphysi9_.[Name] is not null then otherphysi9_.[Name] else N'' end as col_31_0_,
/* VascularAccessObjectID VascularAccess.ObjectID */ vascularac0_.[VascularAccessOID] as col_32_0_ 
from [$(ClinicalVision)].[Core].[VascularAccessEvent] vascularac0_ 
inner join [$(ClinicalVision)].[Core].[Timeline] vascularac0_1_ on vascularac0_.[oid]=vascularac0_1_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Patient] patient1_ on vascularac0_1_.[PatientOID]=patient1_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Person] person2_ on patient1_.[oid]=person2_.[oid] 
left outer join [$(ClinicalVision)].[Core].[PatientDateRange] patientdat3_ on vascularac0_1_.[PatientDateRangeOID]=patientdat3_.[oid] 
left outer join [$(ClinicalVision)].[Core].[AccessDefinition] accessdefi4_ on vascularac0_.[AccessDefinitionOID]=accessdefi4_.[oid] 
left outer join [$(ClinicalVision)].[Core].[MedicalStaffOfRecord] medicalsta5_ on vascularac0_.[SurgeonOID]=medicalsta5_.[oid]   
left outer join [$(ClinicalVision)].[Core].[Person] person8_ on medicalsta5_.[MedicalStaffOID]=person8_.[oid] 
left outer join [$(ClinicalVision)].[Core].[OtherPhysician] otherphysi9_ on medicalsta5_.[OtherPhysicianOID]=otherphysi9_.[oid]
left outer join [$(ClinicalVision)].[Core].[Code] code0 on code0.oid=vascularac0_1_.[TimelineEvent]
left outer join [$(ClinicalVision)].[Core].[Code] code1 on code1.oid=vascularac0_1_.[TimelineEventDetail]
left outer join [$(ClinicalVision)].[Core].[Code] code2 on code2.oid=vascularac0_1_.[InformationSource]
left outer join [$(ClinicalVision)].[Core].[Code] code3 on code3.oid=vascularac0_.[VascularEventEtiology]
left outer join [$(ClinicalVision)].[Core].[Code] code4 on code4.oid=accessdefi4_.[Type]
left outer join [$(ClinicalVision)].[Core].[Code] code5 on code5.oid=accessdefi4_.[Location]
left outer join [$(ClinicalVision)].[Core].[Code] code6 on code6.oid=accessdefi4_.[PermanentStatus]
left outer join [$(ClinicalVision)].[Core].[Code] code7 on code7.oid=accessdefi4_.[Brand]
left outer join [$(ClinicalVision)].[Core].[Code] code8 on code8.oid=accessdefi4_.[CatheterType]
left outer join [$(ClinicalVision)].[Core].[Code] code9 on code9.oid=accessdefi4_.[Configuration]
left outer join [$(ClinicalVision)].[Core].[Code] code10 on code10.oid=accessdefi4_.[Elasticity]
left outer join [$(ClinicalVision)].[Core].[Code] code11 on code11.oid=accessdefi4_.[ExternalStatus]
left outer join [$(ClinicalVision)].[Core].[Code] code12 on code12.oid=accessdefi4_.[NeedleGauge]
left outer join [$(ClinicalVision)].[Core].[Code] code13 on code13.oid=accessdefi4_.[NeedleLength]
left outer join [$(ClinicalVision)].[Core].[Code] code14 on code14.oid=accessdefi4_.[NeedleType]
left outer join [$(ClinicalVision)].[Core].[Code] code15 on code15.oid=accessdefi4_.[NeedleGauge2]
left outer join [$(ClinicalVision)].[Core].[Code] code16 on code16.oid=accessdefi4_.[NeedleLength2]
left outer join [$(ClinicalVision)].[Core].[Code] code17 on code17.oid=accessdefi4_.[NeedleType2]

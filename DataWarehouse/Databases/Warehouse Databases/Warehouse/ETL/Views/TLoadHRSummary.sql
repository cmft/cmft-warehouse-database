﻿



CREATE view [ETL].[TLoadHRSummary]

as

select
	OrganisationCode
	,WardCode = Ward.WardCode
	,CensusDate
	,AppraisalRequired
	,AppraisalCompleted
	,ClinicalMandatoryTrainingRequired
	,ClinicalMandatoryTrainingCompleted
	,CorporateMandatoryTrainingRequired
	,CorporateMandatoryTrainingCompleted
	,RecentAppointment
	,LongTermAppointment
	,SicknessAbsenceFTE
	,SicknessEstablishmentFTE
	,Headcount3mthRolling
	,FTE3mthRolling
	,StartersHeadcount3mthRolling
	,StartersFTE3mthRolling
	,LeaversHeadcount3mthRolling
	,LeaversFTE3mthRolling
	,BudgetWTE
	,ContractedWTE
	,ClinicalBudgetWTE
	,ClinicalContractedWTE
	,ACAgencySpend
	,InterfaceCode = 'ESREXTRACT'
	
	,HeadcountMonthly
	,FTEMonthly
	,StartersHeadcountMonthly
	,StartersFTEMonthly
	,LeaversHeadcountMonthly
	,LeaversFTEMonthly
	,BMERecentAppointment
	,BMELongTermAppointment
	
from
	ETL.TImportHRSummary

left outer join HR.Ward
on	TImportHRSummary.OrganisationCode = Ward.WardCode







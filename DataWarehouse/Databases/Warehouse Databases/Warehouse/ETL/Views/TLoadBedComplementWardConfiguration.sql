﻿CREATE view [ETL].[TLoadBedComplementWardConfiguration]

as 

select 
	 WardID
	,WardType
	,Schedule
	,DivisionCode = left(Division, charindex('-', Division) - 2)
	,SpecialtyGroup
	,Comment
	,StartDate
	,EndDate

from 
	ETL.TImportBedComplementWardConfiguration

﻿




CREATE view [ETL].[TLoadIncidentIncident]

as

select
	SourceIncidentCode
	,IncidentCode
	,IncidentNumber
	,Description
	,IncidentDate = 
					case
					when isdate(IncidentDate) = 0
					then null
					else IncidentDate
					end
	,IncidentTime = 
					case
					when isdate(IncidentTime) = 0
					then null
					else IncidentTime
					end
	,ReportedDate = 					
					case
					when isdate(ReportedDate) = 0
					then null
					else ReportedDate
					end
	,ReportedTime = 					
					case
					when isdate(ReportedTime) = 0
					then null
					else ReportedTime
					end
	
	,ReceiptDate
	,CauseCode1 = convert(varchar(50),cast(CauseCode1 as varbinary(50)), 1) 
	,CauseCode2 = convert(varchar(50),cast(CauseCode2 as varbinary(50)), 1) 
	,IncidentTypeCode = convert(varchar(50),cast(IncidentTypeCode as varbinary (50)), 1) 
	,ContributingFactorCode
	,SignedByCode
	,IncidentGradeCode = convert(varchar(50),cast(IncidentGradeCode as varbinary(50)), 1) 
	,CompletedByCode
	,SeverityCode  = convert(varchar(50),cast(SeverityCode as varbinary(50)), 1) 
	,LikelihoodCode
	,RiskRatingCode
	,RiskScore
	,SpecialtyCode
	,PatientSafety
	,CauseGroupCode = convert(varchar(50),cast(CauseGroupCode as varbinary(50)), 1) 
	,SeriousUntowardIncident
	,ResearchOrTrialProject
	,InitialSeverityCode
	,InitialLikelihoodCode
	,InitialRiskRatingCode
	,InitialRiskScore
	,SecurityIncidentReportingSystemReportable
	,StatusTypeCode
	,NeverEvent
	,OrganisationCode
	,SiteTypeCode
	,SiteCode = convert(varchar(50),cast(SiteCode as varbinary(50)), 1) 
	,DivisionCode = convert(varchar(50),cast(DivisionCode as varbinary(50)), 1) 
	,DirectorateCode = convert(varchar(50),cast(DirectorateCode as varbinary(50)), 1) 
	,LocationCode
	,DepartmentCode = convert(varchar(50),cast(DepartmentCode as varbinary(50)), 1)  
	,WardCode = Ward.WardCode
	,SequenceNo
	,PersonCode
	,PersonTypeCode
	,EntityTypeCode
	,DateOfBirth
	,SexCode
	,CasenoteNumber
	,NHSNumber
	,InterfaceCode = 'ULYSS'
from
	ETL.TImportIncidentIncident

left outer join	Incident.Ward -- subset of deparments to help with conforming wards in reference map
on	convert(varchar(50),cast(DepartmentCode as varbinary(50)), 1) = Ward.WardCode










﻿CREATE view [ETL].[TLoadWAOperation] as

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,WASourceUniqueID
from
	ETL.TImportWAOperation

﻿CREATE view [ETL].[TLoadComplaintComplaint]

as

with CTECalendar (RowNumber,TheDate,WorkingDay)

as 

( 
select 
     RowNumber = 1
     ,TheDate = cast('1 jan 2000' as date)
     ,WorkingDay = null

union all 

select 
     RowNumber = RowNumber + 1
     ,dateadd(day, 1, TheDate)    
     ,WorkingDay = case 
						when dateadd(day, 1, TheDate) in	
									(
									select
										HolidayDate
									from
										WH.Holiday
									)
							then null 	
						when datename(dw,dateadd(day, 1, TheDate)) in ('Saturday','Sunday') 
							then null 									
						else 1 
						end     
from 
     CTECalendar
where 
     dateadd(day, 1, TheDate) < dateadd(year,1,getdate())
) 

select
	SourceComplaintCode
	,ComplaintCode 
	,ClaimantCode
	,ClaimTypeCode
	,BehalfOfCode
	,BehalfOfTypeCode
	,CaseNumber
	,ReceivedFromCode
	,ReceiptDate
	,AcknowledgementDate
	,AcknowledgementMethodCode
	,ComplaintDetail = replace(replace(ComplaintDetail, char(13), ' '), char(10), ' ')
	,ResolutionDate
	,OutcomeDetail = replace(replace(OutcomeDetail, char(13), ' '), char(10), ' ')
	,Satisfied
	,ResponseDate
	,DelayReasonCode
	,SeverityCode
	,ResponseDueDate
	,LetterDate
	,GradeCode = convert(varchar(50),cast(GradeCode as varbinary(50)), 1) 
	,LikelihoodCode
	,RiskRatingCode
	,RiskScore
	,HandlerCode
	,StatusTypeCode  = convert(varchar(50),cast(StatusTypeCode as varbinary(50)), 1) 
	,CaseTypeCode = convert(varchar(50),cast(CaseTypeCode as varbinary(50)), 1) 
	,ConsentRequired
	,ConsentDate
	,EventDate
	,InitialContactDate
	,InitialDueDate
	,InitialSeverityCode
	,InitalScore
	,SourceTargetDate = TargetDate
	,SequenceNo
	,CategoryCode = convert(varchar(50),cast(CategoryCode as varbinary(50)), 1) 
	,CategoryTypeCode = convert(varchar(50),cast(CategoryTypeCode as varbinary(50)), 1) 
	,PrimaryOrSecondary
	,OrganisationCode
	,SiteCode = convert(varchar(50),cast(SiteCode as varbinary(50)), 1) 
	,SiteTypeCode
	,DivisionCode = convert(varchar(50),cast(DivisionCode as varbinary(50)), 1)
	,DirectorateCode = convert(varchar(50),cast(DirectorateCode as varbinary(50)), 1)
	,DepartmentCode = convert(varchar(50),cast(DepartmentCode as varbinary(50)), 1)
	,WardCode = Ward.WardCode 
	,ServiceCode
	,ProfessionCode
	,InterfaceCode = 'ULYSS'
	,Reopened
	,ReopenedDetail = replace(replace(ReopenedDetail, char(13), ' '), char(10), ' ')
	,[ConsentTargetDate] =           
			(
			select
				TheDate
			from
				(
				select
					TheDate
					,WorkingDays = row_number() over (order by TheDate)
				from
					CTECalendar
	                  
				where
					TheDate >= ReceiptDate
				and	WorkingDay = 1
				) FutureWorkingDay
			where
				WorkingDays = 4 --Received date is not a working day
				  )  
	,[TargetDate7Day] =           
			(
			select
				TheDate
			from
				(
				select
					TheDate
					,WorkingDays = row_number() over (order by TheDate)
				from
					CTECalendar
	                  
				where
					TheDate >= coalesce(ConsentDate, ReceiptDate)
				and WorkingDay = 1
				) FutureWorkingDay
			where
				WorkingDays = 8 --Received date is not a working day
				  )  
	,[TargetDate14Day] =   
			(
			select
				TheDate
			from
				(
				select
					TheDate
					,WorkingDays = row_number() over (order by TheDate)
				from
					CTECalendar
				where
					TheDate >= coalesce(ConsentDate, ReceiptDate)
				and WorkingDay = 1
				) FutureWorkingDay
			where
				WorkingDays = 15 --Received date is not a working day
			) 
	,[TargetDate25Day] =           
			(
			select
				TheDate
			from
				(
				select
					TheDate
					,WorkingDays = row_number() over (order by TheDate)
				from
					CTECalendar
	                  
				where
					TheDate >= coalesce(ConsentDate, ReceiptDate)
				and WorkingDay = 1
				) FutureWorkingDay
			where
				WorkingDays = 26 --Received date is not a working day
				  )  
	,[TargetDate40Day] =   
			(
			select
				TheDate
			from
				(
				select
					TheDate
					,WorkingDays = row_number() over (order by TheDate)
				from
					CTECalendar
				where
					TheDate >= coalesce(ConsentDate, ReceiptDate)
				and WorkingDay = 1
				) FutureWorkingDay
			where
				WorkingDays = 41 --Received date is not a working day
			)     
 	,Duration =  
				(
				select
					count(1)
				from
					CTECalendar
				where
					TheDate between ReceiptDate and coalesce(ResponseDate, cast(getdate() as date))
				and WorkingDay = 1
				)
				      
from
	ETL.TImportComplaintComplaint

left outer join	Complaint.Ward -- subset of deparments to help with conforming wards in reference map
on	convert(varchar(50),cast(DepartmentCode as varbinary(50)), 1) = Ward.WardCode


﻿CREATE  view [ETL].[TLoadWaitingListActivity] as

select
	 SourceUniqueID

	,ActivityTypeCode

	,ActivityDate = 
		cast(
			case
			when substring(ActivityTime, 9, 2) = '24'
				then dateadd(minute , 1 , convert(smalldatetime , left(ActivityTime , 8) + ' 23:59'))
			else convert(smalldatetime , left(ActivityTime , 8))
			end
			as date
		)	

	,ActivityTime = 
		case
		when substring(ActivityTime, 9, 2) = '24'
			then dateadd(minute , 1 , convert(smalldatetime , left(ActivityTime , 8) + ' 23:59'))
		else convert(smalldatetime , left(ActivityTime , 8) + ' ' + substring(ActivityTime, 9, 2) + ':' + substring(ActivityTime, 11, 2))
		end

	,AdminCategoryCode =
		case
		when AdminCategoryCode = 'NHS' then '01'
		when AdminCategoryCode = 'PAY' then '02'
		when AdminCategoryCode = 'IND' then '02'
		when AdminCategoryCode = 'OV' then '02'
		when AdminCategoryCode = 'AME' then '03'
		when AdminCategoryCode = 'CAT' then '04'
		else '01'
		end

	,BookingTypeCode
	,CancelledBy
	,ConsultantCode
	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno
	,SiteCode
	,SourcePatientNo

	,LastRevisionTime =
		case
		when substring(LastRevisionTime, 9, 2) = '24'
			then dateadd(minute , 1 , convert(smalldatetime , left(LastRevisionTime , 8) + ' 23:59'))
		else convert(smalldatetime , left(LastRevisionTime , 8) + ' ' + substring(LastRevisionTime, 9, 2) + ':' + substring(LastRevisionTime, 11, 2))
		end

	,LastRevisionUser
	,PreviousActivityID
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode
	,SpecialtyCode
	,SuspensionReasonCode

	,TCIAcceptDate =
		substring(TCIAcceptDate, 7, 2) + '/' +
		substring(TCIAcceptDate, 5, 2) + '/' +
		substring(TCIAcceptDate, 1, 4)

	,TCITime =
		case
		when substring(TCITime, 9, 2) = '24'
			then dateadd(minute , 1 , convert(smalldatetime , left(TCITime , 8) + ' 23:59'))
		else convert(smalldatetime , left(TCITime , 8) + ' ' + substring(TCITime, 9, 2) + ':' + substring(TCITime, 11, 2))
		end

	,TCIOfferDate =
		substring(TCIOfferDate, 7, 2) + '/' +
		substring(TCIOfferDate, 5, 2) + '/' +
		substring(TCIOfferDate, 1, 4) + ' '

	,PriorityCode
	,WaitingListCode
	,WardCode
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator
from
	ETL.TImportWaitingListActivity
where
	not exists
		(
		select
			1
		from
			ETL.TImportWaitingListActivity Previous
		where
			Previous.SourceUniqueID = TImportWaitingListActivity.SourceUniqueID
		and	Previous.LastRevisionTime > TImportWaitingListActivity.LastRevisionTime
		)

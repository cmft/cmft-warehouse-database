﻿





--falls

CREATE view [ETL].[TLoadAPCFall]

as

with FallCTE

(
	SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,FallDate
	,FallTime
	,InitialSeverityID
	,Validated
	,OccuredOnAnotherWard
	,WardCode
	--,Details
)
as
	(
	select
		SourceUniqueID = Fall.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,FallDate = nullif(Details.value('data(FallIncident/Fall_TS)[1]', 'date'), '1 jan 1900')
		,FallTime = nullif(Details.value('data(FallIncident/Fall_TS)[1]', 'datetime'), '1 jan 1900')
		,InitialSeverityID = Details.value('data(FallIncident/InitialSeverity)[1]', 'int')
		
		,Validated = 
					case
					when Details.value('data(FallIncident/Validated)[1]', 'varchar(10)') = 'true'
					then 1
					end
		,OccuredOnAnotherWard = 
							case
							when Details.value('data(FallIncident/OccuredOnAnotherWard)[1]', 'varchar(10)') = 'true'
							then 1
							end
		,WardCode = Locations.PASCode
		--,Details
	from
		[$(BedmanTest)].dbo.Event Fall

	inner join [$(BedmanTest)].dbo.HospSpell Spell -- need to change to bedmantest snapshot once deployed
	on	Spell.SpellID = Fall.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on Fall.LocationID = Locations.LocationID

	where
		EventTypeID = 1
	and not exists --If we have the same event type occuring at the same time, get the latest occurence
			(
			select	
				1
			from
				[$(BedmanTest)].dbo.Event FallLater
			where
				EventTypeID = 1
			and FallLater.HospSpellID = Fall.HospSpellID
			and	FallLater.Details.value('data(FallIncident/Fall_TS)[1]', 'datetime') = Fall.Details.value('data(FallIncident/Fall_TS)[1]', 'datetime')
			and	FallLater.ID > Fall.ID
			)
	)

select
	SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,FallDate
	,FallTime
	,InitialSeverityID
	,Validated
	,OccuredOnAnotherWard
	,WardCode
	--,Details
	,InterfaceCode = 'BEDMAN'	
from
	FallCTE






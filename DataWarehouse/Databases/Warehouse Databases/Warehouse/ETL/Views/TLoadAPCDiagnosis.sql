﻿CREATE view [ETL].[TLoadAPCDiagnosis] as

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo

	,ProviderSpellNo =
		SourcePatientNo + '/' + SourceSpellNo

	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
from
	ETL.TImportAPCDiagnosis

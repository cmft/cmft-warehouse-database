﻿CREATE view [ETL].[TLoadRADReport] 
AS

	/******************************************************************************
	**  Name: TLoadRADReport
	**  Purpose: 
	**
	**  Import procedure view for Radiology RAD.Report table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 06.05.11     MH        New columns added due to CRIS update
	** 25.07.11     MH        Included Report text
	** 26.06.12     MH        Altered for CMFT Environment
	******************************************************************************/


SELECT
	 ImportDateTimeStamp
	,SourceUniqueID = Utility.udfEmptyStringToNull(RADReport.[Report key])
	,ExamSourceUniqueID = Utility.udfEmptyStringToNull(RADReport.[Exam Key])
	,EventSourceUniqueID = Utility.udfEmptyStringToNull(RADReport.[Event key])
	,EventDate = RADEvent.EventDate
	,EventTime = RADEvent.EventTime
	,ExamCode = Utility.udfEmptyStringToNull(RADReport.[Examination])
	,ReportTypeCode = Utility.udfEmptyStringToNull(RADReport.[Rep type])
	,ReportedDate = Utility.udfConvertDate(RADReport.[Date reported])
	,ReportedTime = Utility.udfConvertDateTime(RADReport.[Date reported] , RADReport.[Time reported])
	,ReportedBy = Utility.udfEmptyStringToNull(RADReport.[Reported by])
	,TypedDate = Utility.udfConvertDate(RADReport.[Date typed])
	,TypedTime = Utility.udfConvertDateTime(RADReport.[Date typed] , RADReport.[Time typed])
	,TypedBy = Utility.udfEmptyStringToNull(RADReport.[Typed by])
	,VerifiedDate = Utility.udfConvertDate(RADReport.[Verify Date])
	,VerifiedTime = Utility.udfConvertDateTime(RADReport.[Verify Date] , RADReport.[Time verified])
	,VerifiedBy = Utility.udfEmptyStringToNull(RADReport.[Verif By])
	,LastVerifiedDate = Utility.udfConvertDate(RADReport.[Date last verif])
	,LastVerifiedTime = Utility.udfConvertDateTime(RADReport.[Date last verif] , RADReport.[Time last verif])
	,LastVerifiedBy = Utility.udfEmptyStringToNull(RADReport.[Last verify by])
	
	-- MH 06.05.11 New columns
	
	,AddendumBy = Utility.udfEmptyStringToNull(RADReport.[Addendum by])
	,AddendumDate = Utility.udfConvertDate(RADReport.[Addendum date])
	,AddendumTime = Utility.udfConvertDateTime(RADReport.[Addendum date] , RADReport.[Addendum time])
	,ChangedBy = Utility.udfEmptyStringToNull(RADReport.[Changed by])
	,Confirmed = Utility.udfEmptyStringToNull(RADReport.[Confirmed])
	,ConfirmedBy = Utility.udfEmptyStringToNull(RADReport.[Confirmed by])
	,ConfirmedDesc = Utility.udfEmptyStringToNull(RADReport.[Confirm desc])
	,CreationDate = Utility.udfConvertDate(RADReport.[Creation date])
	--,CreationTime = Utility.udfConvertDateTime(RADReport.[Creation date] , RADReport.[Creation time])
	,CreationTime = null
	,DateAcknowledged = Utility.udfConvertDate(RADReport.[Date ack])
	,DateChanged = Utility.udfConvertDate(RADReport.[Date changed])
	,DatePrinted = Utility.udfConvertDate(RADReport.[Date printed])
	,DateSent = Utility.udfConvertDate(RADReport.[Date sent])
	,VerifyCheckDate = Utility.udfConvertDate(RADReport.[Date verify chk])
	,VerifyCheckTime = Utility.udfConvertDateTime(RADReport.[Date verify chk] , RADReport.[Time verify chk])
	,Deleted = Utility.udfEmptyStringToNull(RADReport.[Deleted])
	,ExamKey = Utility.udfEmptyStringToNull(RADReport.[Exam Key])
	,FoetusNumber = Utility.udfEmptyStringToNull(RADReport.[Foetus number])
	,LastAddendumBy = Utility.udfEmptyStringToNull(RADReport.[Last add by])
	,LastAddendumDate = Utility.udfConvertDate(RADReport.[Last addendum])
	,LastAddendumTime = Utility.udfConvertDateTime(RADReport.[Last addendum] , RADReport.[Last add time])
	,ModifiedDate = Utility.udfConvertDate(RADReport.[Mutation date])
	--,ModifiedTime = Utility.udfConvertDateTime(RADReport.[Mutation date] , RADReport.[Mutation time])
	,ModifiedTime = null
	,Printed = Utility.udfEmptyStringToNull(RADReport.[Printed])
	,Priority = Utility.udfEmptyStringToNull(RADReport.[Priority])
	,RejectReason = Utility.udfEmptyStringToNull(RADReport.[Reject reason])
	,ReportedBy2 = Utility.udfEmptyStringToNull(RADReport.[Reported by 2])
	,Restricted = Utility.udfEmptyStringToNull(RADReport.[Restricted])
	,Sent = Utility.udfEmptyStringToNull(RADReport.[Sent])
	,Status = Utility.udfEmptyStringToNull(RADReport.[Status])
	,Verified = Utility.udfEmptyStringToNull(RADReport.[Verified])
	,VerifyCheckBy = Utility.udfEmptyStringToNull(RADReport.[Verify check by])
	,ReportText

FROM
	[$(CRIS_Import)].dbo.TImportRADReport RADReport	
	LEFT OUTER JOIN RAD.Event RADEvent ON RADReport.[Event key] = RADEvent.SourceUniqueID

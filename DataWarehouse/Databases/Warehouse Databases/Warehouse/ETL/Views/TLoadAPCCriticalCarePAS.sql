﻿
create view [ETL].[TLoadAPCCriticalCarePAS] as

select
	 SourceUniqueID
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,CreatedByUser
	,CreatedByTime
	,DermatologicalSupportDays
	,EndDate =
		cast(
			case
			when right(EndTimeInt, 4) = '2400'
				then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
			else convert(smalldatetime , EndDate + ' ' + left(right(EndTimeInt, 4), 2) + ':' + right(EndTimeInt, 2))
			end
			as date
		)

	,EndTime =
		case
		when right(EndTimeInt, 4) = '2400'
			then dateadd(minute , 1 , convert(smalldatetime , EndDate + ' 23:59'))
		else convert(smalldatetime , EndDate + ' ' + left(right(EndTimeInt, 4), 2) + ':' + right(EndTimeInt, 2))
		end

	,LiverSupportDays
	,LocalIdentifier
	,LocationCode
	,NeurologicalSupportDays
	,RenalSupportDays
	,StartDate =
		case
		when StartTime = ':'
		then StartDate
		else 
			cast(
				case
				when StartTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
				else StartDate
				end
				as date
			)
		end

	,StartTime =
		case
		when StartTime = ':'
		then null
		else 
			case
			when StartTime = '24:00'
				then dateadd(minute , 1 , convert(smalldatetime , StartDate + ' 23:59'))
			else convert(smalldatetime , StartDate + ' ' + StartTime)
			end
		end

	,StatusCode
	,TreatmentFunctionCode
	,SourceSpellNo
	,SourcePatientNo
	,PlannedAcpPeriod
	,InterfaceCode = 'INQ'
	,CasenoteNumber
	,WardCode
	,AdmissionDate
from
	ETL.TImportAPCCriticalCarePAS


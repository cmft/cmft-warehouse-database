﻿





CREATE view [ETL].[TLoadStaffingLevelAPC]

as

select
	DivisionCode
	,WardCode
	,CensusDate
	,ShiftID = ShiftBase.ID
	,RegisteredNursePlan 
	,RegisteredNurseActual 
	,NonRegisteredNursePlan 
	,NonRegisteredNurseActual 
	,Comments
	,SeniorComments

from
	ETL.TImportStaffingLevelAPC
	
inner join IPR.ShiftBase
on	TImportStaffingLevelAPC.Shift = ShiftBase.Shift






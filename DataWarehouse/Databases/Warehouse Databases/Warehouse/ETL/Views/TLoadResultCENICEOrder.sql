﻿








CREATE view [ETL].[TLoadResultCENICEOrder] as

select
	SourceUniqueID
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber 
	,ClinicianID
	,MainSpecialtyCode
	,ProviderID
	,LocationID
	,OrderPriority
	,OrderStatusCode
	,OrderRequestTime
	,OrderReceivedTime
	,OrderCollectionTime
	,InterfaceCode = 'CENICE'
from
	ETL.TImportResultCENICEOrder








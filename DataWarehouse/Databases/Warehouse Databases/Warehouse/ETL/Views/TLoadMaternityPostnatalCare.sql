﻿


CREATE view [ETL].[TLoadMaternityPostnatalCare]

as

select
	SourceUniqueID
	,CasenoteNumber
	,NHSNumber
	,DateOfBirth
	,AgeAtBooking
	,GPPracticeCode
	,DischargedToHealthVisitorDate
	,HIV
	,RenalDisease
	,Renal
	,RHIsoimmunisationCode
	,DiabeticCode
	,DiabetesCode
	,EndocrineCode
	,AntenatalEndocrineCode
	,DiabetesEndocrine
	,HeartDisease
	,CardiacCode
	,CardiacComplications
	,NumberOfFetuses
	,InheritedDiseases
	,PsychiatricProblemCode
	,BMIAtBooking
	,Interpreter
	,PatientTypeCode
	,SpecialistMidwifeReferral
	,AccomodationTypeCode
	,DrugCode
	,HypertensionComplications
	,Thromboembolism
	,DVT
	,HELLP
	,Preeclampsia
	,HELLPPreeclampsia
	,Eclampsia
	,NeonatalDeathStillBirth
	,HRGCode = 
		case
		when 
			HIV = 'P' 
		or	RenalDisease = 'Y'
		or	Renal = 'Y' 
		then 'INTENSIVE'  
		when 
			coalesce(RHIsoimmunisationCode,'N') not in ('N','P')
		or	coalesce(DiabeticCode,'N') <> 'N'
		or	coalesce(DiabetesCode,'N') <> 'N'
		or	coalesce(EndocrineCode,'N') not in ('N','Y')
		or	coalesce(AntenatalEndocrineCode,'N') not in ('N','Y')
		or	DiabetesEndocrine is not null
		or	coalesce(HeartDisease,'N') in ('C','A')
		or	coalesce(CardiacCode,'N') in ('C','A')
		or	CardiacComplications is not null
		or	NumberOfFetuses > 1
		or 	InheritedDiseases = 'Y' 
		or	coalesce(PsychiatricProblemCode,'N') not in ('N','P')
		or	BMIAtBooking >= '35'
		or	Interpreter = 'Y'
		or	PatientTypeCode in ('X','Y')
		or	SpecialistMidwifeReferral = 'Y'
		or 	AgeAtBooking < '20'
		or 	AccomodationTypeCode = '6'
		or 	DrugCode = 'D'
		or	HypertensionComplications is not null
		or 	Thromboembolism = 'Y' 
		or	DVT is not null
		or	HELLP = 'Y'
		or	Preeclampsia = 'Y'
		or	Eclampsia = 'Y' 
		or 	HELLPPreeclampsia is not null
		or 	NeonatalDeathStillBirth is not null 
		then 'INTERMEDIATE'  
		else 'STANDARD'
		end
	,InterfaceCode = 'SMMIS'
	,ModifiedTime
from
	ETL.MaternityPostnatalCare


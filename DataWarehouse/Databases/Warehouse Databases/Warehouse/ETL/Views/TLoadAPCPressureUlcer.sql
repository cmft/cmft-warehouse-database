﻿






--PressureUlcer

CREATE view [ETL].[TLoadAPCPressureUlcer]

as

with PressureUlcerCTE

(
	SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,LocationID
	,IdentifiedDate
	,IdentifiedTime
	,HealedDate
	,HealedTime
	,CategoryID
	,Validated
	,OccuredOnAnotherWard
	,WardCode
	--,Details
)
as
	(
	select
		SourceUniqueID = PressureUlcer.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,LocationID = Details.value('data(PressureUlcerCondition/PressureUlcerLocationID)[1]', 'int')
		,IdentifiedDate = nullif(Details.value('data(PressureUlcerCondition/Identified_TS)[1]', 'date'), '1 jan 1900')
		,IdentifiedTime = nullif(Details.value('data(PressureUlcerCondition/Identified_TS)[1]', 'datetime'), '1 jan 1900')
		,HealedDate = nullif(Details.value('data(PressureUlcerCondition/Healed_TS)[1]', 'date'), '1 jan 1900')
		,HealedTime = nullif(Details.value('data(PressureUlcerCondition/Healed_TS)[1]', 'datetime'), '1 jan 1900')
		,CategoryID = Details.value('data(PressureUlcerCondition/Category)[1]', 'int')
		,Validated = 
					case
					when Details.value('data(PressureUlcerCondition/Validated)[1]', 'varchar(10)') = 'true'
					then 1
					end
		,OccuredOnAnotherWard = 
							case
							when Details.value('data(PressureUlcerCondition/OccuredOnAnotherWard)[1]', 'varchar(10)') = 'true'
							then 1
							end
		,WardCode = Locations.PASCode
		--,Details
	from
		[$(BedmanTest)].dbo.Event PressureUlcer

	inner join [$(BedmanTest)].dbo.HospSpell Spell
	on	Spell.SpellID = PressureUlcer.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on PressureUlcer.LocationID = Locations.LocationID

	where
		EventTypeID = 8
	)

select
	SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,LocationID
	,IdentifiedDate
	,IdentifiedTime
	,HealedDate
	,HealedTime
	,CategoryID
	,Validated
	,OccuredOnAnotherWard
	,WardCode
	,InterfaceCode = 'BEDMAN'
	--,Details
from
	PressureUlcerCTE;







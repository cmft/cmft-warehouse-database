﻿



CREATE view [ETL].[TLoadPASPatientSpecialRegister]
as

/****************************************************************************************
	View		: ETL.TLoadPASPatientSpecialRegister
	Description	: Standard ETL view

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	14/08/2014	Paul Egan       Initial Coding
*****************************************************************************************/

select
	SourcePatientNo
	,SpecialRegisterCode
	,DistrictNo
	,NHSNumber
	,EnteredDate
	,ActivityTime
	,SourceEpisodeNo
from
	ETL.TImportPASPatientSpecialRegister



﻿










CREATE view [ETL].[TLoadPEXKioskResponse]

as

select
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID = case
					when AnswerID = 0
					then null
					else AnswerID
				end	
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,WardID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
from
	ETL.TImportPEXKioskResponse


left join
	PEX.KioskOnlinePostcardTabletWard
on	TImportPEXKioskResponse.LocationID = KioskOnlinePostcardTabletWard.WardID	











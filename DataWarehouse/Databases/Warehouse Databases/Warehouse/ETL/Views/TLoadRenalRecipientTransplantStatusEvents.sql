﻿



CREATE view [ETL].[TLoadRenalRecipientTransplantStatusEvents] as

select
	SourceUniqueID = cast(ObjectID as varchar(100))
	,PatientObjectID
	,PatientFullName = PatientFullName collate Latin1_General_CI_AS
	,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
	,StartDate = StartDate_Local
	,StopDate = StopDate_Local
	,DaysElapsed
	,AgeAtEvent
	,TimelineEvent = TimelineEvent collate Latin1_General_CI_AS
	,TimelineEventDetailCode = TimelineEventDetailCode collate Latin1_General_CI_AS
	,InformationSource = InformationSource collate Latin1_General_CI_AS
	,Notes = Notes collate Latin1_General_CI_AS
	,RecipientUKTNumber = RecipientUKTNumber collate Latin1_General_CI_AS
	,ReferralObjectID
	,ReferralStartDate = ReferralStartDate_Local
	,ReferralStopDate = ReferralStopDate_Local
	,ConsultantObjectID
	,Consultant = Consultant collate Latin1_General_CI_AS
	,Provider = Provider collate Latin1_General_CI_AS
	,DonorType = DonorType collate Latin1_General_CI_AS
	,TransplantPriority = TransplantPriority collate Latin1_General_CI_AS
	,StatusDetail = StatusDetail collate Latin1_General_CI_AS
	,StatusReason = StatusReason collate Latin1_General_CI_AS
from ETL.[TLoadRenalRecipientTransplantStatusEventsBase]




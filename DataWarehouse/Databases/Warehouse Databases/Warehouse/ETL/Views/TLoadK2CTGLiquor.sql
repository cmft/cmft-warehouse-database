﻿
CREATE View [ETL].[TLoadK2CTGLiquor]

As


-- 20140506 RR	The following was taken from google to split csv columns into rows by the comma http://stackoverflow.com/questions/4250475/split-one-column-into-multiple-rows 
-- During June 2012 there were some DQ issues, 17 records have come through with ASCII values rather than integer values, and 2 with 43f/85f.  
-- 19 / 59,347 v.small %.  Excluded these.  Old issues.  If job starts to fail, it may be worth seeing if this has started to happen again.

With
	L0 as(Select 1 as c Union All Select 1),
	L1 as(Select 1 as c From L0 as A, L0 as B),
	L2 as(Select 1 as c From L1 as A, L1 as B),
	L3 as(Select 1 as c From L2 as A, L2 as B),
	Numbers as(Select ROW_NUMBER() over(order by c) as n From L3)


Select 
	LiqSplit.*
	,[Liquor] = [DisplayText]

from
	(SELECT  
		SessionNumber
		,FetusNumber
		,DateEntered
		,TimeEntered
		,CTGDescriptionID
		,CTGDescription
		,[Value] = 
					LTRIM
						(RTRIM
							(SUBSTRING
								(CTG.ReviewLiquorColour, Nums.n, 
									charindex
										(N',', CTG.ReviewLiquorColour + N',', Nums.n
										) 
									- Nums.n
								)
							)
						)
	FROM   
		Numbers AS Nums 
	
	inner join ETL.TImportK2CTGAssessments CTG 
	ON Nums.n <= CONVERT(int, LEN(CTG.ReviewLiquorColour)) 
	AND SUBSTRING(N',' + CTG.ReviewLiquorColour, n, 1) = N','
	
	where 
		left(ReviewLiquorColour,1) in ('0','1','2','3','4','5','6','7','8','9') 
	and ReviewLiquorColour not like '%á%'
	
	) as LiqSplit

inner join ETL.TImportK2CTGLookup
on [Value] = [ID]
and [Category] = 'Liquor'





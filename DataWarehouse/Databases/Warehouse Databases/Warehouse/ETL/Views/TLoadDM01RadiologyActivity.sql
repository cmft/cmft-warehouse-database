﻿
CREATE VIEW [ETL].[TLoadDM01RadiologyActivity] 
AS

	/******************************************************************************
	**  Name: ETL.TLoadDM01RadiologyActivity
	**  Purpose: 
	**
	**  Import procedure view for DM01 Radiology Activity table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 09.11.12    MH         Created
	******************************************************************************/

	SELECT 
		DistrictNo						= SUBSTRING(T.[Best Hosp No.], 4, LEN(T.[Best Hosp No.]) - 3),
		EventDate						= T.[Event Date],
		SiteCode						= T.[Site],
		PatientTypeCode					= COALESCE(PTB.PatientTypeCode, LEFT(T.[Patient Type Des],50)),
		ExamCode						= COALESCE(EB.ExamCode, LEFT(T.[Exam Name],50)),
		Referrer						= T.[Referrer Name],
		SpecialtyCode					= COALESCE(S.SpecialtyCode, LEFT(T.[Name],50)),
		EpisodicPCTCode					= COALESCE(PCTORG.[Organisation Code],'5NT'),
		RegisteredPracticeCode			= T.[Practice],
		ModalityCode					= T.[Modality],
		NHSNUmber						= T.[NHS number],
		NumberofExams					= T.[No of Pr],
		Planned							= T.[Planned],
		KornerBand						= T.[Korner band],
		Interventional					= T.[Interventional],
		PostcodeAbbr					= T.[Pstcde1]

	FROM
		ETL.TImportDM01RadiologyActivity T

		LEFT JOIN RAD.ExamBase EB				-- To remove duplicate exam descriptions
			ON		EB.Exam = T.[Exam Name] 
				AND EB.ExamEndDate IS NULL 
				AND	EB.ExamRealFlag = 'Y'

		LEFT JOIN RAD.SpecialtyBase S
			ON		T.[Name] = S.Specialty
				AND S.EndDate IS NULL

		LEFT JOIN RAD.PatientTypeBase PTB
			ON T.[Patient Type Des] = PTB.PatientType

		LEFT JOIN [$(Organisation)].[dbo].[Primary Care Organisation] PCTORG
			ON T.[PCT] = PCTORG.[Organisation Code]


﻿




CREATE view [ETL].[TLoadObservationSignificantEvent] as

-- tiny dataset hence no TImport table and extract proc...

select
	SourceUniqueID 
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID 
	,LocationID 
	,AdmissionSourceUniqueID 
	,SignificantEventTypeID
	,StartTime 
	,EndTime 
	,Comment 
	,CreatedByUserID 
	,CreatedTime
	,LastModifiedByUserID 
	,LastModifiedTime 
	,InterfaceCode 
from
	(
	select
		SourceUniqueID = SignificantEvent.SIGEV_PK
		,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
		,DateOfBirth = Admission.BIRTH_DATE
		,SpecialtyID = AdmissionUnit.UNITT_RFVAL
		,LocationID = WardStay.WARDD_RFVAL
		,AdmissionSourceUniqueID = SignificantEvent.ADMSN_PK
		,SignificantEventTypeID = SIEVT_RFVAL 
		,StartTime = START_DTTM
		,EndTime = END_DTTM
		,Comment = nullif(cast(SignificantEvent.NOTE as varchar(max)),'')
		,CreatedByUserID = SignificantEvent.CREATED_BY_USERR_PK
		,CreatedTime = SignificantEvent.CREATED_DTTM
		,LastModifiedByUserID = SignificantEvent.LAST_MODIFIED_BY_USERR_PK
		,LastModifiedTime = SignificantEvent.LAST_MODIFIED_DTTM
		,InterfaceCode = 'PTRACK'
	from
		[$(Patientrack)].dbo.SIGNIFICANT_EVENT SignificantEvent


	inner join [$(Patientrack)].dbo.ADMISSION Admission
	on	Admission.ADMSN_PK = SignificantEvent.ADMSN_PK
	and	coalesce(Admission.DELETED_FLAG, 0) = 0

	inner join [$(Patientrack)].dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
	on	PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
	and	PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
	and	coalesce(PatientIdentifier.DELETED_FLAG, 0) = 0	

	inner join
		(
		select
			SignificantEvent.SIGEV_PK 
			,AdmissionUnit.UNITT_RFVAL
		from
			[$(Patientrack)].dbo.ADMISSION_UNIT AdmissionUnit
		inner join [$(Patientrack)].dbo.SIGNIFICANT_EVENT SignificantEvent
		on	SignificantEvent.ADMSN_PK = AdmissionUnit.ADMSN_PK
		and SignificantEvent.CREATED_DTTM between AdmissionUnit.START_DTTM and coalesce(AdmissionUnit.END_DTTM, getdate())
		
		where
			coalesce(AdmissionUnit.DELETED_FLAG, 0) = 0
		and	not exists
					(
					select
						1
					from
						[$(Patientrack)].dbo.ADMISSION_UNIT AdmissionUnitNext
					where
						SignificantEvent.ADMSN_PK = AdmissionUnitNext.ADMSN_PK
					and coalesce(AdmissionUnitNext.DELETED_FLAG, 0) = 0
					and SignificantEvent.CREATED_DTTM between AdmissionUnitNext.START_DTTM and coalesce(AdmissionUnitNext.END_DTTM, getdate())
					and AdmissionUnitNext.ADUNT_PK > AdmissionUnit.ADUNT_PK
					)

		) AdmissionUnit

		on	SignificantEvent.SIGEV_PK = AdmissionUnit.SIGEV_PK
				
	inner join
		(
		select
			SignificantEvent.SIGEV_PK 
			,WardStay.WARDD_RFVAL
		from
			[$(Patientrack)].dbo.WARD_STAY WardStay
		inner join [$(Patientrack)].dbo.SIGNIFICANT_EVENT SignificantEvent
		on	SignificantEvent.ADMSN_PK = WardStay.ADMSN_PK
		and SignificantEvent.CREATED_DTTM between WardStay.START_DTTM and coalesce(WardStay.END_DTTM, getdate())
		
		where
			coalesce(WardStay.DELETED_FLAG, 0) = 0
		and	not exists
					(
					select
						1
					from
						[$(Patientrack)].dbo.WARD_STAY WardStayNext
					where
						SignificantEvent.ADMSN_PK = WardStayNext.ADMSN_PK
					and coalesce(WardStayNext.DELETED_FLAG, 0) = 0
					and SignificantEvent.CREATED_DTTM between WardStayNext.START_DTTM and coalesce(WardStayNext.END_DTTM, getdate())
					and WardStayNext.WRDST_PK > WardStay.WRDST_PK
					)
		) WardStay

		on	SignificantEvent.SIGEV_PK = WardStay.SIGEV_PK

	) SignificantEvent



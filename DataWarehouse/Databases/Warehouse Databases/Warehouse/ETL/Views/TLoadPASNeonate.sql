﻿CREATE view [ETL].[TLoadPASNeonate] as

select
	 NeonateID = NEONATEID
	,ApgarScoreAt1Minute = ApgarScoreAt1Min
	,ApgarScoreAt5Minutes = ApgarScoreAt5Mins
	,BirthOrder
	,DiagnosisCode1
	,DiagnosisCode2
	,SourceSpellNo = EpisodeNumber
	,HeadCircumference
	,SourcePatientNo = InternalPatientNumber
	,BCGAdministered = KeMatBcgAdminInt
	,Feeding = KeMatFeedingInt
	,FollowUpCare = KeMatFollUpCareInt
	,HipsExamination = KeMatHipExamInt
	,Jaundice = KeMatJaundiceInt
	,MetabolicScreening = KeMatMetabolicScInt
	,LengthAtBirth = Length
	,GestationLength = PaedLenGestation
from
	ETL.TImportPASNeonate

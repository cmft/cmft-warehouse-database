﻿CREATE view [ETL].[TLoadPASBirth] as

select
	 BirthID = BIRTHID
	,BabyPatientNo = BabyPatientNumber
	,BirthOrder
	,BirthWeight
	,CasenoteNumber = CaseNoteNumber
	,Comments

	,DeliveryDate = 
		cast(
			case
			when isdate(DeliveryDate) = 0
			then null
			else
				case
				when DeliveryTime = '24:00'
					then dateadd(minute , 1 , convert(smalldatetime , DeliveryDate + ' 23:59'))
				when DeliveryTime = ':'	then DeliveryDate + ' 00:00'
				else convert(smalldatetime , DeliveryDate + ' ' + DeliveryTime)
				end
			end
			as date
		) 

	,DeliveryTime =
		cast(
			case
			when isdate(DeliveryDate) = 0
			then null
			else
				case
				when DeliveryTime = '24:00'	then dateadd(minute , 1 , convert(smalldatetime , DeliveryDate + ' 23:59'))
				when DeliveryTime = ':'	then DeliveryDate + ' 00:00'
				else convert(smalldatetime , DeliveryDate + ' ' + DeliveryTime)
				end
			end
			as smalldatetime
		) 


	,DrugsUsed
	,SourceSpellNo = EpisodeNumber
	,SourcePatientNo = InternalPatientNumber
	--,KeMatDrugsUsed2
	--,KeMatDtimeDelivNeut
	,LiveStillBirth = KeMatLiveStillIndInt
	,DeliveryMethodCode = KeMatMethDelivInt
	--,KeMatMorDrugsInt
	--,KeMatMorPpInt
	--,KeMatPresFetusInt
	--,SuspCongAnomaly
from
	ETL.TImportPASBirth

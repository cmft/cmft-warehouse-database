﻿
CREATE view [ETL].[TLoadAPCWaitingListSuspension]
as

select
	 SourcePatientNo 
	,SourceEncounterNo
	,CensusDate
	,SuspensionStartDate 
	,SuspensionEndDate 
	,SuspensionReasonCode 
	,SuspensionReason
	,SourceUniqueID
from
	ETL.TImportAPCWLSuspension


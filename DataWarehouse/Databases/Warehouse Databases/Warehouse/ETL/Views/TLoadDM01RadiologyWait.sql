﻿
CREATE VIEW [ETL].[TLoadDM01RadiologyWait] 
AS

	/******************************************************************************
	**  Name: ETL.TLoadDM01RadiologyWait
	**  Purpose: 
	**
	**  Import procedure view for DM01 Radiology Wait table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** 14.11.12    MH         Created
	** 28.01.13    MH         Added ClockResetDate column
	** 29.01.13    MH         Added ExamKey column
	******************************************************************************/

	SELECT 
		EventKey					= [Event key],
		DistrictNo					= CASE
										WHEN LEN([Best Hosp No.]) > 3 
										  THEN RIGHT([Best Hosp No.], LEN([Best Hosp No.]) - 3)
										  ELSE [Best Hosp No.]
									  END,
		SourcePatientNo				= [CRIS No.],
		Surname,
		Forenames,
		DOB,
		PatientTypeDescription		= [Patient Type Des],
		ExamCode					= [Examination],
		SiteCode					= Site,
		StatusCode					= [Status Cde],
		RequestDate					= CAST([Request date] AS DATE),
		EventDate					= [Event Date],
		WeeksWaiting				= [Weeks Waiting],
		PCTCode						= CASE
										WHEN [PCT] = '' 
										  THEN '5NT'
										  ELSE [PCT]
									  END,
		Planned,
		Scheduled,
		ModalityCode				= Modality,
		Group1						= [Group 1],
		Group2						= [Group 2],
		RequestToApptDays			= [ReqToA],
		[Days],
		NHSNumber					= NHSNumber,
		ClockResetDate				= ClockResetDate,
		ExamKey						= ExamKey

	FROM
		ETL.TImportDM01RadiologyWait T


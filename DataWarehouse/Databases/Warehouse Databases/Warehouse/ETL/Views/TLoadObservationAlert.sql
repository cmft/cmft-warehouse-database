﻿


CREATE view [ETL].[TLoadObservationAlert]

as

select
	SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,LocationID
	,AdmissionSourceUniqueID
	,TypeID
	,ReasonID
	,ObservationSetSourceUniqueID
	,CreatedDate = cast(CreatedTime as date)
	,CreatedTime
	,BedsideDueDate = cast(BedsideDueTime as date)
	,BedsideDueTime
	,EscalationDate = cast(EscalationTime as date)
	,EscalationTime
	,AcceptedDate = cast(AcceptedTime as date)
	,AcceptedTime
	,AttendedByUserID
	,AttendanceTypeID
	,Comment
	,SeverityID
	,ClinicianSeniorityID
	,AcceptanceRemindersRemaining
	,ChainSequenceNumber
	,NextReminderTime
	,ClosedDate = cast(ClosedTime as date)
	,ClosedTime
	,ClosureReasonID
	,ClosedByUserID
	,CancelledByObservationSetSourceUniqueID
	,LastModifiedTime
	,InterfaceCode = 'PTRACK'
from
	ETL.TImportObservationAlert TImport

inner join Observation.AlertType AlertType
on	TImport.TypeID = AlertType.AlertTypeID

where
	AlertType = 'Alert'




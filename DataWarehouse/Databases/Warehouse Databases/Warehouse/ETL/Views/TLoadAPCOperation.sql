﻿CREATE view [ETL].[TLoadAPCOperation] as

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo

	,ProviderSpellNo =
		SourcePatientNo + '/' + SourceSpellNo

	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,APCSourceUniqueID
from
	ETL.TImportAPCOperation

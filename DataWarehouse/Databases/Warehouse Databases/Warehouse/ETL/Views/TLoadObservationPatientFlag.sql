﻿




CREATE view [ETL].[TLoadObservationPatientFlag] as

select
	SourceUniqueID
	,CasenoteNumber 
	,DateOfBirth
	,AdmissionSourceUniqueID
	,PatientFlagID
	,StartTime
	,EndTime
	,Comment
	,CreatedByUserID
	,CreatedTime
	,LastModifiedByUserID
	,LastModifiedTime
	,EWSDisabled
	,InterfaceCode = 'PTRACK'
from
	ETL.TImportObservationPatientFlag

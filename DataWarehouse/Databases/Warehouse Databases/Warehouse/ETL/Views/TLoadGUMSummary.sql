﻿
CREATE VIEW [ETL].[TLoadGUMSummary] 
AS

	/******************************************************************************
	**  Name: TLoadGUMSummary
	**  Purpose: 
	**
	**  Import procedure view for GUM AMM Summary table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	******************************************************************************/

SELECT 
	 StartDate								= TImportGUMSummary.[StartDate]
	,EndDate								= TImportGUMSummary.[EndDate]
	,CensusDate								= TImportGUMSummary.[EndDate]
	,ClinicCode								= TImportGUMSummary.[Clinic Code]
	,PCTCode								= TImportGUMSummary.[PCT Code]
	,AllAtt									= TImportGUMSummary.[GU ALL ATTENDANCES TOTAL]
	,FirstAtt								= TImportGUMSummary.[GU FIRST ATTENDANCES TOTAL]
	,FirstAttSeenWithin2Days				= TImportGUMSummary.[GU FIRST ATTENDANCES SEEN WITHIN 2 DAYS TOTAL]
	,FirstAttSeenAfter10Days				= TImportGUMSummary.[GU FIRST ATTENDANCES SEEN AFTER 10 DAYS TOTAL]
	,FirstAttUnscheduled					= TImportGUMSummary.[GU FIRST ATTENDANCES - UNSCHEDULED TOTAL]
	,FirstAttPatientPreferredClinic			= TImportGUMSummary.[GU FIRST ATTENDANCES - PATIENT PREFERRED CLINIC TOTAL]
	,FirstApptMissed						= TImportGUMSummary.[GU FIRST APPOINTMENTS MISSED TOTAL]
	,FirstApptMissedWithin2Days				= TImportGUMSummary.[GU FIRST APPOINTMENTS MISSED WITHIN 2 DAYS TOTAL]
	,FirstApptOfferedWithin2Days			= TImportGUMSummary.[GU FIRST APPOINTMENTS OFFERED WITHIN 2 DAYS TOTAL]
	,PatientsReportingSymptoms				= TImportGUMSummary.[PATIENTS REPORTING SYMPTOMS TOTAL]
	,FirstApptSeenAfter2DaysPatientChoice	= TImportGUMSummary.[GU FIRST APPOINTMENTS SEEN AFTER 2 DAYS - PATIENT CHOICE TOTAL]
	,FirstApptSeenAfter2DaysClinicalReason	= TImportGUMSummary.[GU FIRST APPOINTMENTS SEEN AFTER 2 DAYS - CLINICAL REASON TOTAL]
	,FirstApptSeenAfter2DaysSpecialistClinic= TImportGUMSummary.[GU FIRST APPOINTMENTS SEEN AFTER 2 DAYS - SPECIALIST CLINIC TOTAL]
	,PatientPersUnScheduledAttWithin2days	= TImportGUMSummary.[PATIENT PERSPECTIVE ON WAITING TIMES - UNSCHEDULED ATTENDANCES WITHIN 2 DAYS TOTAL]
	,PatientPersScheduledAttWithin2days		= TImportGUMSummary.[PATIENT PERSPECTIVE ON WAITING TIMES - SCHEDULED ATTENDANCES WITHIN 2 DAYS TOTAL]
	,UnScheduledAttResponseToPatientWaitQ	= TImportGUMSummary.[UNSCHEDULED ATTENDANCES - RESPONSES TO PATIENT WAIT QUESTION TOTAL]
	,ScheduledAttResponseToPatientWaitQ		= TImportGUMSummary.[SCHEDULED ATTENDANCES - RESPONSES TO PATIENT WAIT QUESTION TOTAL]
	,PatientRegNotOfferedAppt				= TImportGUMSummary.[PATIENTS REGISTERED BUT NOT OFFERED AN APPOINTMENT TOTAL]	

FROM
	ETL.TImportGUMSummary

	

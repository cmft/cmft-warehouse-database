﻿



--VTECondition

CREATE view [ETL].[TLoadAPCVTECondition]

as

with VTEConditionCTE
(
	SourceUniqueID
	,SourcePatientNo 
	,SourceSpellNo 
	,DiagnosisDate
	,DiagnosisTime 
	,VTETypeID 
	,MedicationRequired
	,OccuredOnAnotherWard 					
	,MedicationStartDate 
	,MedicationStartTime 
	,WardCode 
)

as
	(
	select
		SourceUniqueID = VTECondition.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,DiagnosisDate = nullif(Details.value('data(VTECondition/Diagnosis_TS)[1]', 'date'), '1 jan 1900')
		,DiagnosisTime = nullif(Details.value('data(VTECondition/Diagnosis_TS)[1]', 'datetime'), '1 jan 1900')
		,VTETypeID = Details.value('data(VTECondition/VTETypeID)[1]', 'int')
		,MedicationRequired = 
							case
							when Details.value('data(VTECondition/MedicationRequired)[1]', 'varchar(10)') = 'true'
							then 1
							end
		,OccuredOnAnotherWard = 
						case
						when Details.value('data(VTECondition/OccuredOnAnotherWard)[1]', 'varchar(10)') = 'true'
						then 1
						end
		,MedicationStartDate = nullif(Details.value('data(VTECondition/VTETreatment/MedicationStarted_TS)[1]', 'date'), '1 jan 1900')
		,MedicationStartTime = nullif(Details.value('data(VTECondition/VTETreatment/MedicationStarted_TS)[1]', 'datetime'), '1 jan 1900')
		,WardCode = Locations.PASCode
		--,Details
	from
		[$(BedmanTest)].dbo.Event VTECondition

	inner join [$(BedmanTest)].dbo.HospSpell Spell
	on	Spell.SpellID = VTECondition.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on VTECondition.LocationID = Locations.LocationID

	where
		EventTypeID = 5
	and not exists --If we have the same event type occuring at the same time, get the latest occurence
			(
			select	
				1
			from
				[$(BedmanTest)].dbo.Event VTEConditionLater
			where
				VTEConditionLater.EventTypeID = 5
			and VTEConditionLater.HospSpellID = VTECondition.HospSpellID
			and	VTEConditionLater.Details.value('data(VTECondition/Diagnosis_TS)[1]', 'datetime') = VTECondition.Details.value('data(VTECondition/Diagnosis_TS)[1]', 'datetime')
			and	VTEConditionLater.ID > VTECondition.ID
			)
	)

select
	VTEConditionCTE.SourceUniqueID
	,VTEConditionCTE.SourcePatientNo 
	,VTEConditionCTE.SourceSpellNo
	,VTEConditionCTE.DiagnosisDate
	,VTEConditionCTE.DiagnosisTime 
	,VTEConditionCTE.VTETypeID 
	,VTEConditionCTE.OccuredOnAnotherWard
	,VTEConditionCTE.MedicationRequired			
	,VTEConditionCTE.MedicationStartDate 
	,VTEConditionCTE.MedicationStartTime 
	,VTEConditionCTE.WardCode 
	,InterfaceCode = 'BEDMAN'
from
	VTEConditionCTE






﻿CREATE view [ETL].[TLoadRADUserBase] 
AS

	/******************************************************************************
	**  Name: TLoadRADUserBase
	**  Purpose: 
	**
	**  Import procedure view for Radiology RAD.UserBase table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 06.05.11     MH        New columns added due to CRIS update
	** 26.06.12     MH        Altered for CMFT environment
	******************************************************************************/


SELECT
	 ImportDateTimeStamp
	,UserID = Utility.udfEmptyStringToNull(RADUserBase.[User ID])
	,UserName = Utility.udfEmptyStringToNull(RADUserBase.[Name])
	,LoginID = Utility.udfEmptyStringToNull(RADUserBase.[Login ID])
	,CreationTime = Utility.udfConvertDateTime(RADUserBase.[Creation date] , RADUserBase.[Creation time])
	,LogicallyDeletedFlag = Utility.udfEmptyStringToNull(RADUserBase.[Deleted])
	,Level = Utility.udfEmptyStringToNull(RADUserBase.[Level])
	,ModifiedTime = Utility.udfConvertDateTime(RADUserBase.[Mutation date] , RADUserBase.[Mutation time])
	,UserAccessLevel = Utility.udfEmptyStringToNull(RADUserBase.[User access])
	,ValidFrom = Utility.udfConvertDate(RADUserBase.[Valid From])
	,ValidTo = Utility.udfConvertDate(RADUserBase.[Valid To])
	,SiteCode = Utility.udfEmptyStringToNull(RADUserBase.[Site code])
	,SpineRolesDate = Utility.udfConvertDate(RADUserBase.[Spine roles date])
FROM
	[$(CRIS_Import)].dbo.TImportRADUserBase RADUserBase

﻿









CREATE view [ETL].[TLoadObservationObservation]

as

select
	SourceUniqueID
	,ObservationSetSourceUniqueID
	,MeasureID
	,NumericResult
	,ListID 
	,FlagCode
	,CreatedByUserID
	,CreatedTime
	,ModifiedByUserID
	,LastModifiedTime
	,InterfaceCode = 'PTRACK'
from
	ETL.TImportObservationObservation
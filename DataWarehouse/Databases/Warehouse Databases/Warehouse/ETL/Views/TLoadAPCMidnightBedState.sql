﻿CREATE  view [ETL].[TLoadAPCMidnightBedState] as

select
	 SourceUniqueID
    ,SourcePatientNo
    ,SourceSpellNo
    ,ProviderSpellNo
    ,SiteCode
    ,WardCode
    ,ConsultantCode
    ,SpecialtyCode
    ,SourceAdminCategoryCode
    ,ActivityInCode

    ,AdmissionTime = 
		case
		when right(rtrim(AdmissionDateTimeInt) , 4) = '2400' then
			dateadd(minute , 1 , convert(smalldatetime , left(AdmissionDateTimeInt , 8) + ' 23:59' ))
		else convert(smalldatetime ,
				left(AdmissionDateTimeInt , 8) + ' ' +
				substring(AdmissionDateTimeInt, 9, 2) + ':' + substring(AdmissionDateTimeInt, 11, 2)
			)
		end

	,AdmissionDate =  
		cast(
			case
			when right(rtrim(AdmissionDateTimeInt) , 4) = '2400' then
				dateadd(minute , 1 , convert(smalldatetime , left(AdmissionDateTimeInt , 8) + ' 23:59' ))
			else convert(smalldatetime , left(AdmissionDateTimeInt , 8))
			end
			as date
		)

    ,CensusDate = 
			convert(smalldatetime , left(MidnightBedStateDate, 8) , 112)

    ,InterfaceCode
from
	ETL.TImportAPCMidnightBedState

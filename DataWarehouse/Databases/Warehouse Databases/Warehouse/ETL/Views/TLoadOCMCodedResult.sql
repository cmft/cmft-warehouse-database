﻿



CREATE view [ETL].[TLoadOCMCodedResult]

as

select
	[SourceUniqueID]
	,[SourcePatientNo]
	,[SourceEpisodeNo]
	,[SequenceNo]
	,[TransactionNo]
	,[SessionNo]
	,[ResultCode]
	,[Result]
	,[ResultDate]
	,[ResultTime]
	,[RangeIndicator]
	,[EnteredBy]
	,[StatusCode]
	,[Comment1]
	,[Comment2]
	,[Comment3]
	,[Comment4]
	,[Comment5]
	,[Comment6]
from
	[ETL].[TImportOCMCodedResult]
where
	SourceUniqueID not in -- DQ records to be excluded for now - Failed merges
						(
						'24604252||27576050||CA  '
						)


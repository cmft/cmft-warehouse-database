﻿



CREATE VIEW [ETL].[TLoadQCRAuditAnswer] AS

SELECT
	 SourceUniqueID
	,AuditTime
	,AuditDate = CONVERT(DATE, AuditTime)
	,LocationCode
	,WardCode = WardCode
	,AuditTypeCode
	,QuestionCode
	,Answer
FROM
	ETL.TImportQCRAuditAnswer

left outer join QCR.Ward
on	Ward.WardCode = TImportQCRAuditAnswer.LocationCode




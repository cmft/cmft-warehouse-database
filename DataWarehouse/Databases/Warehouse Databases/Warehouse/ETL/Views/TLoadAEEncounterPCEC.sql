﻿



CREATE view [ETL].[TLoadAEEncounterPCEC] as

/******************************************************************************************************
View        : [dbo].[TLoadAEEncounterPCEC]
Description : 

Modification History
====================

Date        Person                  Description
=======================================================================================================
?                 ?                       Initial coding.
14/11/2014  Paul Egan       Corrected AgeOnArrival case statement.
08/12/2014  Rachel Royston    Brought through NHS Number from Adastra, previously set to = Null
                                          Added SequenceNo to account for duplicate rows
*******************************************************************************************************/
      
      
      
select
      SourceUniqueID = left(Encounter.SourceUniqueID , 50)
      ,SequenceNo = row_number () over (partition by left(Encounter.SourceUniqueID , 50)
                  order by Encounter.EntryTime
                  )
      ,UniqueBookingReferenceNo = null
      ,PathwayId = null
      ,PathwayIdIssuerCode = null
      ,RTTStatusCode = null
      ,RTTStartDate = null
      ,RTTEndDate = null
      ,DistrictNo = null
      ,TrustNo = null
      ,CasenoteNo = null
      ,DistrictNoOrganisationCode = null
      ,NHSNumber = 
                  case 
                        when NHSNumber = '' then Null
                        else replace(NHSNumber,' ','') 
                  end
      ,NHSNumberStatusId = null
      ,PatientTitle = null
      ,Encounter.PatientForename
      ,Encounter.PatientSurname

      ,PatientAddress1 =
            left(Encounter.PatientAddress1 , 50)

      ,PatientAddress2 =
            left(
            case
                  when len(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '')) > 50
                        then substring(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '') , 51 , len(Encounter.PatientAddress1 + coalesce(Encounter.PatientAddress2 , '')) - 50)
                  else null
                  end
            ,50)


      ,Encounter.PatientAddress3
      ,PatientAddress4 =  null
      ,Encounter.Postcode
      ,DateOfBirth = cast(Encounter.DateOfBirth as date)
      ,DateOfDeath = null
      ,Encounter.SexCode
      ,CarerSupportIndicator = null
      ,RegisteredGpCode = null
      ,RegisteredGpPracticeCode = null
      ,AttendanceNumber = Encounter.CaseNo

      ,ArrivalModeCode =
            case Encounter.Answer
            when 'Ambulance' then 6477 -- Ambulance
            when 'Bus' then 6665 -- Public Transport
            when 'Car' then 6476 -- Own Transport
            when 'Cycle' then 6476 -- Own Transport
            when 'Taxi' then 6480 -- Other Arrival Mode
            when 'Walked' then 6476 -- Own Transport
            end

      ,AttendanceCategoryCode =
            case
            when Encounter.CaseType like '%repeat%' then 2
            else 1
            end

      ,AttendanceDisposalCode = '02' --NULL
      ,SourceAttendanceDisposalCode = '-2' --NULL
      ,IncidentLocationTypeCode = null
      ,PatientGroupCode = 80
      ,SourceOfReferralCode = null
      ,ArrivalDate = cast(Encounter.EntryTime as date) 
      ,ArrivalTime =  Encounter.EntryTime
      ,AgeOnArrival = 
            case
            when Encounter.Age in( 'Unknown' , '') then null
            when Encounter.Age like '%month%' then 
                  case
                  when left(Encounter.Age , 2) > 23 then 2
                  when left(Encounter.Age , 2) between 12 and 23 then 1
                  else 0
                  end
            when Encounter.Age like '%week%' then 0               -- Added Paul Egan 14/11/2014
            when Encounter.Age like '%day%' then 0                -- Added Paul Egan 14/11/2014
            else cast(left(Encounter.Age , charindex(' ' , Encounter.Age) - 1) as int)
            end

      ,InitialAssessmentTime = null
      ,SeenForTreatmentTime = Encounter.ConsultationStartTime
      ,AttendanceConclusionTime = Encounter.ConsultationEndTime
      ,DepartureTime = Encounter.ConsultationEndTime
      ,CommissioningSerialNo = 
            rtrim(
                  coalesce(   
                        --Practice.ParentOrganisationCode
                        Postcode.PCTCode
                        --,Encounter.ResidencePCTCode
                        ,'X98'
                  ) 
            ) + '00A'

      ,NHSServiceAgreementLineNo = null
      ,ProviderReferenceNo = null
      ,CommissionerReferenceNo = null
      ,ProviderCode = 'RW3'

      ,CommissionerCode = 
            coalesce(   
                  --Practice.ParentOrganisationCode
                  Postcode.PCTCode
                  --,Encounter.ResidencePCTCode
                  ,'X98'
            ) 

      ,StaffMemberCode = null
      ,InvestigationCodeFirst = null
      ,InvestigationCodeSecond = null
      ,DiagnosisCodeFirst = null
      ,DiagnosisCodeSecond = null
      ,TreatmentCodeFirst = null
      ,TreatmentCodeSecond = null
      ,PASHRGCode = null
      ,HRGVersionCode = null
      ,PASDGVPCode = null
      ,SiteCode
      ,Created = null
      ,Updated = null
      ,ByWhom = null
      ,InterfaceCode 

      ,PCTCode = 
            coalesce(   
                  --Practice.ParentOrganisationCode
                  Postcode.PCTCode
                  --,Encounter.ResidencePCTCode
                  ,'X98'
            ) 

      ,ResidencePCTCode = coalesce(Postcode.PCTCode , 'X98') 

      ,InvestigationCodeList = null
      ,TriageCategoryCode = null
      ,PresentingProblem = null
      ,ToXrayTime = null
      ,FromXrayTime = null
      ,ToSpecialtyTime = null
      ,SeenBySpecialtyTime = null
      ,EthnicCategoryCode = null
      ,ReferredToSpecialtyCode = null
      ,DecisionToAdmitTime = null
      ,DischargeDestinationCode = null
      ,RegisteredTime = null
      ,TransportRequestTime = null
      ,TransportAvailableTime = null
      ,AscribeLeftDeptTime = null
      ,CDULeftDepartmentTime = null
      ,PCDULeftDepartmentTime = null

      ,AmbulanceArrivalTime = null
      ,AmbulanceCrewPRF = null
      ,ArrivalTimeAdjusted = Encounter.EntryTime
      ,UnplannedReattend7Day = 0
      ,ClinicalAssessmentTime = null

from
     ETL.[TImportAEEncounterPCEC] Encounter


--left join Organisation.dbo.Practice Practice
--on  Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode

left join [$(Organisation)].dbo.Postcode Postcode on
      Postcode.Postcode = 
            case
            when len(Encounter.Postcode) = 8 then Encounter.Postcode
            else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
            end collate Latin1_General_CI_AS
where
      Encounter.CaseNo is not null
and   rtrim(Encounter.CaseNo) != ''
and   Encounter.CaseNo not in ('79815' , '79823')







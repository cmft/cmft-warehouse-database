﻿



CREATE view [ETL].[TLoadNeonatalBeddayProcedure]

as

select
	[EpisodeID]
	,[CareDate]
	,[ProcedureCode]
	,ModifiedTime
	,InterfaceCode = 'BDGR'
from
	ETL.TImportNeonatalBeddayProcedure



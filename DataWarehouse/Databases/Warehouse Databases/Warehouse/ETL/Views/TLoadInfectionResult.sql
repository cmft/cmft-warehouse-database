﻿





CREATE view [ETL].[TLoadInfectionResult]

as

select
	AlertTypeCode
	,TestCategoryID
	,Positive
	,SpecimenNumber
	,PatientName
	,DistrictNo
	,NHSNumber
	,SexCode
	,DateOfBirth
	,SpecimenCollectionDate
	,SpecimenCollectionTime
	,PreviousPositiveDate
	,OrganismCode
	,SampleTypeCode
	,SampleSubTypeCode
	,SampleQualifierCode
	,AuthorisationDate
	,ExtractFileName
	,InterfaceCode = 'TPATH'
from
	ETL.TImportInfectionResult





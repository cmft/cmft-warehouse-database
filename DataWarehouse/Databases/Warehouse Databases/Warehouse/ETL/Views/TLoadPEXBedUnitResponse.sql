﻿



CREATE view [ETL].[TLoadPEXBedUnitResponse]

as

select
[SurveyTakenID]
      ,[SurveyDate]
      ,[SurveyTime]
      ,[SurveyID]
      ,[LocationID]
      ,WardID
      ,[QuestionID]
      ,[QuestionNumber]
      ,[AnswerID] = BedUnitAnswer.AnswerID
      ,Answer = TImportPEXBedUnitResponse.Answer
      ,[OptionNo]
      ,[ResponseDate]
      ,[ResponseTime]
      ,[ChannelID]
      ,[QuestionTypeID]
      ,[LocationTypeID]

from
	ETL.TImportPEXBedUnitResponse

left join PEX.BedUnitWard
on	TImportPEXBedUnitResponse.LocationID = BedUnitWard.WardID	

left join PEX.BedUnitAnswer
on	TImportPEXBedUnitResponse.AnswerID = BedUnitAnswer.AnswerID





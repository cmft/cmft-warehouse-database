﻿








CREATE view [ETL].[TLoadObservationObservationSet]

as

select
	SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,LocationID
	,LocationStartTime
	,ReplacedSourceUniqueID
	,CurrentObservationSetFlag
	,AdditionalObservationSetFlag
	,AdmissionSourceUniqueID
	,EarlyWarningScoreRegimeApplicationID
	,ObservationProfileApplicationID
	,ObservationNotTakenReasonID
	,ClinicianPresentSeniorityID
	,StartDate = dateadd(day, datediff(day, 0, StartTime), 0)
	,StartTime
	,TakenDate = dateadd(day, datediff(day, 0, TakenTime), 0)
	,TakenTime
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,AlertSeverityID
	,DueTime
	,DueTimeStatusID
	,DueTimeCreatedBySourceUniqueID
	,LastModifiedTime
	,InterfaceCode = 'PTRACK'
from
	ETL.TImportObservationObservationSet
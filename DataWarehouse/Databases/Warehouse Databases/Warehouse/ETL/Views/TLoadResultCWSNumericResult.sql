﻿













CREATE view [ETL].[TLoadResultCWSNumericResult] as

select --top 10
	SourceUniqueID
	,SourcePatientNo
	,SourceEpisodeNo
	,TransactionNo
	,SessionNo
	,OrderSourceUniqueID
	,ConsultantCode
	,SpecialtyCode
	,OrderPriorityCode
	,OrderStatusCode
	,OrderEnteredByCode
	,OrderedByCode
	,OrderTime
	,OrderComment
	,SampleReferenceCode
	,EffectiveTime
	,LocationCode
	,ServiceCode
	,ResultCode
	,Result
	,ResultTime
	,RangeIndicator
	,ResultEnteredByCode
	,ResultStatusCode
	,ResultComment
	,InterfaceCode = 'INQ' 
from
	ETL.TImportResultCWSNumericResult













﻿










CREATE view [ETL].[TLoadPEXPostcardResponse]

as

select
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID = case
					when AnswerID = 0
					then null
					else AnswerID
				end	
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,WardID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
from
	ETL.TImportPEXPostcardResponse


left join
	PEX.KioskOnlinePostcardTabletWard
on	TImportPEXPostcardResponse.LocationID = KioskOnlinePostcardTabletWard.WardID	








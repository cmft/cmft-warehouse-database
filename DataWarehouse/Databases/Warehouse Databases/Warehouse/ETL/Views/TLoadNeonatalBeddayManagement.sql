﻿


CREATE view [ETL].[TLoadNeonatalBeddayManagement]

as

select
	[EpisodeID]
	,[CareDate]
	,[ManagementCode]
	,ModifiedTime
	,InterfaceCode = 'BDGR'
from
	ETL.TImportNeonatalBeddayManagement


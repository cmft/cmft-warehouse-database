﻿




CREATE view [ETL].[TLoadRenalPatientHemodialysisFlowsheets] as
 
SELECT 
	SourceUniqueID = cast([ObjectID] as varchar(100))
	,HemodialysisServicesObjectID
	,PatientObjectID
	,PatientFullName = PatientFullName collate Latin1_General_CI_AS
	,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
	,DatePoint = Date_Local
	,TimingQualifier = TimingQualifier collate Latin1_General_CI_AS
	,StaffObjectID
	,Staff = Staff collate Latin1_General_CI_AS
	,AccessTransonicValue
	,Weight
	,WeightUnits = WeightUnits collate Latin1_General_CI_AS
	,WeightInKg
	,Temperature
	,TemperatureUnits = TemperatureUnits collate Latin1_General_CI_AS
	,TemperatureInCelsius
	,SittingBloodPressureSystolic
	,SittingBloodPressureDiastolic
	,SittingHeartRate
	,StandingBloodPressureSystolic
	,StandingBloodPressureDiastolic
	,StandingHeartRate
	,Height
	,HeightUnits = HeightUnits collate Latin1_General_CI_AS
	,HeightInCm
	,UltrafiltrationGoal
	,UltrafiltrationRate
	,UFVRemoved
	,BloodVolumeProcessed
	,BloodFlowRate
	,DialysateFlowRate
	,ArtP
	,VenP
	,TransmembranePressure
	,Conductivity
	,AlarmState = AlarmState collate Latin1_General_CI_AS
	,MachineMode = MachineMode collate Latin1_General_CI_AS
	,MachineTemperature = MachineTemperature collate Latin1_General_CI_AS
	,Notes = Notes collate Latin1_General_CI_AS

FROM ETL.TLoadRenalPatientHemodialysisFlowsheetsBase






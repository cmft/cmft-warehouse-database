﻿

CREATE view [ETL].[TLoadAPCLegalStatus] as

select
	 SourceSpellNo = LegalStatus.EpisodeNumber 
	,SourcePatientNo = LegalStatus.InternalPatientNumber 
	,HospitalCode = WardStay.HospitalCode

	,StartTime = 
		case
		when right(rtrim(LegalStatus.KeLegalStatChangeDtimeInt) , 4) = '2400' then
			dateadd(minute , 1 , convert(smalldatetime , left(LegalStatus.KeLegalStatChangeDtimeInt , 8) + ' 23:59'))
		else convert(smalldatetime ,
				left(LegalStatus.KeLegalStatChangeDtimeInt , 8) + ' ' +
				left(right(rtrim(LegalStatus.KeLegalStatChangeDtimeInt) , 4) , 2) + ':' + 
				right(right(rtrim(LegalStatus.KeLegalStatChangeDtimeInt) , 4) , 2)
			)
		end

	,PsychiatricPatientStatusCode = LegalStatus.PsycPtSts 
	,MentalCategoryCode = LegalStatus.MentalCategory 
	,LegalStatusCode = LegalStatus.LegalStatus
	,LegalStatusStartDateTime1 = LegalStatus.KeLegalStatChangeDtimeInt
from
	[$(PAS)].Inquire.LEGALSTATUSDETS LegalStatus

left join [$(PAS)].Inquire.WARDSTAY WardStay
on	WardStay.InternalPatientNumber = LegalStatus.InternalPatientNumber
and	WardStay.EpisodeNumber = LegalStatus.EpisodeNumber
and	WardStay.EpsActvDtimeInt <= LegalStatus.KeLegalStatChangeDtimeInt
and	not exists
		(
		select
			1
		from
			[$(PAS)].Inquire.WARDSTAY LaterStay
		where
			LaterStay.InternalPatientNumber = WardStay.InternalPatientNumber
		and	LaterStay.EpisodeNumber = WardStay.EpisodeNumber
		and	LaterStay.EpsActvDtimeInt <= LegalStatus.KeLegalStatChangeDtimeInt
		and	LaterStay.EpsActvDtimeInt > WardStay.EpsActvDtimeInt
		)



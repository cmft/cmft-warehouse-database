﻿










CREATE view [ETL].[TLoadMaternityAntenatalCare]

as

select
	SourceUniqueID
	,CasenoteNumber
	,DistrictNo 
	,NHSNumber
	,DateOfBirth
	,AgeAtBooking
	,GPPracticeCode
	,VisitDate
	,VisitTime
	,VisitTypeCode
	,FetalMedicineCases
	,Thrombophilia
	,SickleCellCode
	,ThalassemiaCode
	,Thromboembolism
	,AntiTNFTreatment
	,RHIsoimmunisationCode
	,RenalDisease
	,Renal
	,EndocrineCode
	,AntenatalEndocrineCode
	,MalignantDisease
	,HIV
	,HeartDiseaseCode
	,CardiacCode
	,NumberOfFetuses
	,DiabeticCode
	,DiabetesCode
	,RespiratoryProblemCode
	,EarlyPreTerm
	,Miscarriage
	,NeonatalDeath
	,SecondThirdTrimesterLoss
	,GrowthRestrictionCode
	,PreviousBirthWeight
	,PsychiatricProblemCode
	,HELLPEclampsia
	,HypertensionMedication
	,PreviousOperations
	,EpilepsyMedication
	,InheritedDiseases
	,HepatitisB
	,HepatitisC
	,BMIAtBooking
	,PhysicalDisability
	,Interpreter
	,PatientTypeCode
	,SpecialistMidwifeReferral
	,AccomodationTypeCode
	,DrugCode
	,SafeguardingChildrenCode
	,SafeguardingWomenCode
	,HRGCode = 
			case																			
			when
				FetalMedicineCases is not null
			or  Thrombophilia = 'Y'
			or	coalesce(SickleCellCode,'N') not in ('N','P')
			or  coalesce(ThalassemiaCode,'N') in ('M','J') 
			or	Thromboembolism = 'Y'   
			or  AntiTNFTreatment = 'Y' 
			or	coalesce(RHIsoimmunisationCode,'N') not in ('N','P')
			or	RenalDisease = 'Y' 
			or 	Renal = 'Y'    
			or	coalesce(EndocrineCode,'N') <> 'N'
			or	coalesce(AntenatalEndocrineCode,'N') <> 'N'
			or	MalignantDisease = 'Y' 
			or 	HIV = 'P' 
			or	coalesce(HeartDiseaseCode,'N') in ('C','A')
			or	coalesce(CardiacCode,'N') in ('C','A')
			or	NumberOfFetuses > 1 
			or	coalesce(DiabeticCode,'N') <> 'N'
			or	coalesce(DiabeticCode,'N') <> 'N'
			then 'INTENSIVE'
			when
				coalesce(RespiratoryProblemCode,'N') <> 'N' 
			--or 	FETALCONGENITALABNORMALITY is not null -- see extract proc notes
			or	EarlyPreTerm is not null
			or	Miscarriage >= 3
			or	NeonatalDeath is not null
			or	SecondThirdTrimesterLoss is not null
			--or 	PLACENTAACCRETA is not null  -- see extract proc notes
			or	GrowthRestrictionCode is not null
			or	PreviousBirthWeight is not null
			or	PsychiatricProblemCode = 'P'
			or	HELLPEclampsia is not null
			or	HypertensionMedication = 'Y'
			or	PreviousOperations is not null
			or	EpilepsyMedication = 'Y'
			or	InheritedDiseases = 'Y'
			or	HepatitisB = 'P' 
			or	HepatitisC = 'P'
			or	coalesce(PsychiatricProblemCode,'N') not in ('N','P')
			or	BMIAtBooking >= '35' 
			or	BMIAtBooking < '18' 
			or	PhysicalDisability = 'Y' 
			or	Interpreter = 'Y' 
			or	PatientTypeCode in ('X','Y')
			or	SpecialistMidwifeReferral = 'Y'
			or	(
					AgeAtBooking < '20' 
				and AgeAtBooking is not Null
				) 
			or	AccomodationTypeCode = '6' 
			or	DrugCode = 'D'
			or	SafeguardingChildrenCode = 'C' 
			or	SafeguardingWomenCode = 'C'
			then 'INTERMEDIATE' 
			else 'STANDARD'
			end
	,HypertensionCode
    ,HaematologicalDisorderCode
    ,MedicalHistoryComments
    ,AutoimmuneDiseaseCode
    ,EstimatedDeliveryDate
    ,FirstContactDate
    ,FirstContactProfessionalCarerTypeCode
    ,FirstContactOther
    ,LastMenstrualPeriodDate
    ,FirstLanguageEnglishCode
    ,EmploymentStatusMotherCode
    ,EmploymentStatusPartnerCode
    ,SmokingStatusCode 
    ,CigarettesPerDay 
    ,AlcoholUnitsPerWeek
    ,WeightMother 
    ,HeightMother 
	,RubellaOfferDate
	,RubellaOfferStatus 
	,RubellaBloodSampleDate 
	,RubellaResult
	,HepatitisBOfferDate 
	,HepatitisBOfferStatus 
	,HepatitisBBloodSampleDate 
	,HepatitisBResult 
	,HaemoglobinopathyOfferDate 
	,HaemoglobinopathyOfferStatus 
	,HaemoglobinopathyBloodSampleDate 
	,HaemoglobinopathyResult 
	,DownsSyndromeOfferDate 
	,DownsSyndromeOfferStatus
	,DownsSyndromeBloodSampleDate 
	,DownsSyndromeInvestigationRiskRatio 
	,PreviousCaesareanSections
	,PreviousLiveBirths
	,PreviousStillBirths
	,PreviousLossesLessThan24Weeks
	,ModifiedTime
	,InterfaceCode = 'SMMIS'

from
	ETL.MaternityAntenatalCare




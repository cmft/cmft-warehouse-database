﻿










CREATE view [ETL].[TLoadResultGPICETest] as

select
	SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber 
	,ClinicianID
	,MainSpecialtyCode
	,ProviderID
	,LocationID
	,TestStatus
	,TestID
	,InterfaceCode = 'GPICE'
from
	ETL.TImportResultGPICETest









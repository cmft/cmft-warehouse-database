﻿




CREATE view [ETL].[TLoadMortalityNICU] as

WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' AS ns)

select
	 SourceUniqueID = ReceivedForm.ID
	,FormTypeID = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Header/ns:FormID)[1]', 'INT')
	,ReviewStatus
	,ReviewedDate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfReview)[1]', 'date') 
	,ReviewedBy = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:Reviewer)[1]', 'varchar(50)')     
	,CasenoteNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:HospitalNumber)[1]', 'varchar(50)')

	,Age =
		case
		when isnumeric(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:AgeAtDeath)[1]', 'varchar(20)')) = 1
		then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:AgeAtDeath)[1]', 'int')
		else null
		end

	,DateOfDeath = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfDeath)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfDeath)[1]', 'datetime')
			else null
		end
	,NamedConsultant= ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:NamedConsultant)[1]', 'varchar(50)')  
	,PatientName = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:InfantName)[1]', 'varchar(50)') 
	,TerminalCareDeathExpected = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DeathExpected)[1]', 'bit')  
	,TerminalCareDeathExpectedComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DeathExpectedDetails)[1]', 'varchar(max)')  
	,DeathPreventable = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DeathPreventableOrAvoidable)[1]', 'bit')  
	,TransferComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:Transfer)[1]', 'varchar(max)')
	,Postmortem = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Postmortem)[1]', 'varchar(max)')  
	,PostmortemComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:PMFindings)[1]', 'varchar(max)')  
	,MedicationErrors = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DrugErrors)[1]', 'bit')  
	,MedicationErrorsComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:DrugErrorsDetail)[1]', 'varchar(max)')  
	,CommunicationComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Discussion/ns:D_Communication)[1]', 'varchar(max)')  
	,AnythingCouldBeDoneDifferentlyComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:FurtherDevelopment)[1]', 'varchar(max)')
	,NotableGoodQualityCare = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:NotablePractice)[1]', 'varchar(max)')  
	,LessonsLearned = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Conclusion)[1]', 'varchar(max)')  
	,ReferenceNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:RefNo)[1]', 'varchar(50)')  
	,Gestation = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:Gestation)[1]', 'varchar(50)') 
	,BirthWeight = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:BirthWeight)[1]', 'varchar(50)')  
	,DateOfBirth = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:DateOfBirth)[1]', 'datetime') 
	,Sex = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:Sex)[1]', 'varchar(50)')  
	,Ethnicity = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnHeader/ns:Ethnicity)[1]', 'varchar(50)') 
	,MRSA = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:MRSA)[1]', 'bit')  
	,CriticalIncident = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CriticalIncident)[1]', 'bit') 
	,CriticalIncidentDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:CriticalIncidentDetails)[1]', 'varchar(max)') 
	,WithdrawalOfIC = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:WithdrawalOfIC)[1]', 'bit')  
	,WithdrawalOfICDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:WithdrawalOfICDetails)[1]', 'varchar(max)')  
	,WithdrawalOfICLOTADNRFormsUsed = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:WithdrawalOfIC_LOTADNRFormsUsed)[1]', 'bit')  
	,SeverelyAbnormalCUSS = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:SeverelyAbnormalCUSS)[1]', 'bit')  
	,ConsultantDiscussionB4WDofIC = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ConsultantDiscussionB4WDofIC)[1]', 'bit')  
	,BereavementAppointmentSent = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:BereavementAppointmentSent)[1]', 'bit')  
	,BereavementAppointmentSentDetail = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:BereavementAppointmentDetails)[1]', 'varchar(max)')  

	,ClinicalSummaryBackground = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinicalSummary/ns:CS_Background)[1]', 'varchar(max)')  
	,ClinicalSummaryProgress = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinicalSummary/ns:CS_Progress)[1]', 'varchar(max)')  
	,ClinicalSummaryManagement = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinicalSummary/ns:CS_Management)[1]', 'varchar(max)')  
	,ClinicalSummarySurgical = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:ClinicalSummary/ns:CS_Surgical)[1]', 'varchar(max)')
	,DiscussionObstetricIssues = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Discussion/ns:D_ObstetricIssues)[1]', 'varchar(max)')  
	,DiscussionResus = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Discussion/ns:D_Resus)[1]', 'varchar(max)')  
	,DiscussionFirst48Hours = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Discussion/ns:D_First48Hrs)[1]', 'varchar(max)')  
	,DiscussionClinicalManagement = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Discussion/ns:D_ClinicalMgmt)[1]', 'varchar(max)')  
	,DiscussionCommunication = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Discussion/ns:D_Communication)[1]', 'varchar(max)')  
	,DiscussionDocumentation = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:ReturnDetail/ns:Discussion/ns:D_Documentation)[1]', 'varchar(max)')  

	,SourcePatientNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:sPN)[1]', 'varchar(20)')  
	,EpisodeStartTime = 
		case
			when IsDate(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]','varchar(20)') ) = 1
			then ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]', 'datetime')
			else null
		end
	,ContextCode
from
	[$(InfoPathRepository)].InfoPath.ReceivedForms ReceivedForm

inner join
       [$(Information)].Information.MortalityReviewForms
on     MortalityReviewForms.MortalityReviewFormID = ReceivedForm.ID

where
	ReceivedForm.FormTypeID = 303 --NICU Mortality Review







﻿CREATE VIEW [ETL].[TLoadRenalPatientRenalModalityBase] 
(
[ObjectID], [PatientObjectID], [PatientFullName], [PatientMedicalRecordNo], [StartDate_Local], [StopDate_Local]
, ModalityCode
, [Modality]
, ModalitySettingCode
, [ModalitySetting]
, [DialysisProvider]
, EventDetailCode
, [EventDetail]
, InformationSourceCode
, [InformationSource]
, ReasonForChangeCode
, [ReasonForChange]
, [Notes], [IsPrimary], [AgeAtEvent]
, [DialysisProviderObjectID])
as select

/* ObjectID ObjectID */ renalmodal0_.[oid] as col_0_0_,
/* PatientObjectID Patient.ObjectID */ patient1_.[oid] as col_1_0_,
/* PatientFullName Patient.Person.Name */ person2_.[Name] as col_2_0_,
/* PatientMedicalRecordNo Patient.MedicalRecordNo */ patient1_.[MedicalRecordNo] as col_3_0_,
/* StartDate_Local PatientDateRange.PatientStartDate */ patientdat3_.[PatientStartDate] as col_4_0_,
/* StopDate_Local PatientDateRange.PatientStopDate */ patientdat3_.[PatientStopDate] as col_5_0_,
/* ModalityCode */ code0.LocalCode as col_6_1_,
/* Modality RenalModality */ code0.ClinicalVisionCore_UKEnglish as col_6_0_,
/* ModalitySettingCode */ code1.LocalCode as col_7_1_,
/* ModalitySetting RenalModalitySetting */ code1.ClinicalVisionCore_UKEnglish as col_7_0_,
/* DialysisProvider DialysisProvider.Provider.Name */ provider5_.[Name] as col_8_0_,
/* EventDetailCode */ code2.LocalCode as col_9_1_,
/* EventDetail TimelineEventDetail */ code2.ClinicalVisionCore_UKEnglish as col_9_0_,
/* InformationSourceCode */ code3.LocalCode as col_10_1_,
/* InformationSource InformationSource */ code3.ClinicalVisionCore_UKEnglish as col_10_0_,
/* ReasonForChangeCode */ code4.LocalCode as col_11_0_,
/* ReasonForChange ReasonModalityChange */ code4.ClinicalVisionCore_UKEnglish as col_11_0_,
/* Notes Notes */ renalmodal0_1_.[Notes] as col_12_0_,
/* IsPrimary IsPrimary */ case when (patient1_.[PrimaryRenalModalityOID] is not null) and patient1_.[PrimaryRenalModalityOID]=renalmodal0_.[oid] then 1 else 0 end as col_13_0_,
/* AgeAtEvent AgeAtEvent */ case when (person2_.[DateOfBirth] is not null) and (patientdat3_.[PatientStartDate] is not null) then floor((cast(cast(floor(cast(patientdat3_.[PatientStartDate] as float)) as datetime) as int)-cast(cast(floor(cast(person2_.[DateOfBirth] as float)) as datetime) as int))/365.25) else null end as col_14_0_,
/* DialysisProviderObjectID DialysisProvider.Provider.ObjectID */ provider5_.[oid] as col_15_0_ 
from [$(ClinicalVision)].[Core].[RenalModality] renalmodal0_ 
inner join [$(ClinicalVision)].[Core].[Timeline] renalmodal0_1_ on renalmodal0_.[oid]=renalmodal0_1_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Patient] patient1_ on renalmodal0_1_.[PatientOID]=patient1_.[oid] 
left outer join [$(ClinicalVision)].[Core].[Person] person2_ on patient1_.[oid]=person2_.[oid]   
left outer join [$(ClinicalVision)].[Core].[PatientDateRange] patientdat3_ on renalmodal0_1_.[PatientDateRangeOID]=patientdat3_.[oid]  
left outer join [$(ClinicalVision)].[Core].[Provider] provider5_ on renalmodal0_.[DialysisProviderOID]=provider5_.[oid]
left outer join [$(ClinicalVision)].[Core].[Code] code0 on code0.oid=renalmodal0_.[RenalModality]
left outer join [$(ClinicalVision)].[Core].[Code] code1 on code1.oid=renalmodal0_.[RenalModalitySetting]
left outer join [$(ClinicalVision)].[Core].[Code] code2 on code2.oid=renalmodal0_1_.[TimelineEventDetail]
left outer join [$(ClinicalVision)].[Core].[Code] code3 on code3.oid=renalmodal0_1_.[InformationSource]
left outer join [$(ClinicalVision)].[Core].[Code] code4 on code4.oid=renalmodal0_.[ReasonModalityChange]


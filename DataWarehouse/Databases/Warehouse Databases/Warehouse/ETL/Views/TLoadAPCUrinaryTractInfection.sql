﻿

CREATE view [ETL].[TLoadAPCUrinaryTractInfection]

as


--get individual datasets for UTI

--catheter
with CatheterCTE
(
	SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,InsertedDate
	,InsertedTime
	,RemovedDate
	,RemovedTime
	,CatheterTypeID
	,WardCode
	,Details
)
as
	(
	select
		SourceUniqueID = Catheter.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,InsertedDate = nullif(Details.value('data(CatheterIntervention/Inserted_TS)[1]', 'date'), '1 jan 1900')			
		,InsertedTime = nullif(Details.value('data(CatheterIntervention/Inserted_TS)[1]', 'datetime'), '1 jan 1900')			
		,RemovedDate = nullif(Details.value('data(CatheterIntervention/Removed_TS)[1]', 'date'), '1 jan 1900')
		,RemovedTime = nullif(Details.value('data(CatheterIntervention/Removed_TS)[1]', 'datetime'), '1 jan 1900')
		,CatheterTypeID = Details.value('data(CatheterIntervention/CatheterType)[1]', 'int')
		,WardCode = Locations.PASCode
		,Catheter.Details
	from
		[$(BedmanTest)].dbo.Event Catheter

	inner join [$(BedmanTest)].dbo.HospSpell Spell
	on	Spell.SpellID = Catheter.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on Catheter.LocationID = Locations.LocationID

	where
		EventTypeID = 2	
	and not exists --If we have the same event type occuring at the same time, get the latest occurence
				(
				select	
					1
				from
					[$(BedmanTest)].dbo.Event CatheterLater
				where
					CatheterLater.EventTypeID = 2
				and CatheterLater.HospSpellID = Catheter.HospSpellID
				and	CatheterLater.Details.value('data(CatheterIntervention/Inserted_TS)[1]', 'datetime') = Catheter.Details.value('data(CatheterIntervention/Inserted_TS)[1]', 'datetime')
				and	CatheterLater.ID > Catheter.ID
				)
	)
,
--UTISymptom
UTISymptomCTE
(
	SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SymptomStartDate
	,SymptomStartTime
	,SymptomEndDate
	,SymptomEndTime
	,WardCode
	,Details
)
as
	(
	select
		SourceUniqueID = UTISymptom.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,SymptomStartDate = nullif(Details.value('data(UTISymptom/UTISymptonsObserved_TS)[1]', 'date'), '1 jan 1900')
		,SymptomStartTime = nullif(Details.value('data(UTISymptom/UTISymptonsObserved_TS)[1]', 'datetime'), '1 jan 1900')
		,SymptomEndDate = nullif(Details.value('data(UTISymptom/UTISymptonsClosed_TS)[1]', 'date'), '1 jan 1900')
		,SymptomEndTime = nullif(Details.value('data(UTISymptom/UTISymptonsClosed_TS)[1]', 'datetime'), '1 jan 1900')
		--,SequenceNo = row_number() over (
		--						partition by
		--								Spell.Patient, Spell.IPEpisode 
		--						order by 
		--								Details.value('data(UTISymptom/UTISymptonsObserved_TS)[1]', 'datetime')
		--								,UTISymptom.ID
		--						)				
		,WardCode = Locations.PASCode
		,UTISymptom.Details
	from
		[$(BedmanTest)].dbo.Event UTISymptom

	inner join [$(BedmanTest)].dbo.HospSpell Spell
	on	Spell.SpellID = UTISymptom.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on UTISymptom.LocationID = Locations.LocationID

	where
		EventTypeID = 3
	and not exists --If we have the same event type occuring at the same time, get the latest occurence
				(
				select	
					1
				from
					[$(BedmanTest)].dbo.Event UTISymptomLater
				where
					UTISymptomLater.EventTypeID = 3
				and UTISymptomLater.HospSpellID = UTISymptom.HospSpellID
				and	UTISymptomLater.Details.value('data(UTISymptom/UTISymptonsObserved_TS)[1]', 'datetime') = UTISymptom.Details.value('data(UTISymptom/UTISymptonsObserved_TS)[1]', 'datetime')
				and	UTISymptomLater.ID > UTISymptom.ID
				)
	)
,
--UTITest
UTITestCTE
(
	SourceUniqueID 
	,SourcePatientNo 
	,SourceSpellNo 
	,UrinaryTestRequestedDate 
	,UrinaryTestRequestedTime
	,UrinaryTestReceivedDate 
	,UrinaryTestReceivedTime 										
	,UrinaryTestPositive 
	,WardCode
	,Details
)

as
	(
	select
		SourceUniqueID = UTITest.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,UrinaryTestRequestedDate = nullif(Details.value('data(UTITest/UrinaryTestResultsSent_TS)[1]', 'date'), '1 jan 1900')
		,UrinaryTestRequestedTime = nullif(Details.value('data(UTITest/UrinaryTestResultsSent_TS)[1]', 'datetime'), '1 jan 1900')
		,UrinaryTestReceivedDate = nullif(Details.value('data(UTITest/UrinaryTestResultsReceived_TS)[1]', 'date'), '1 jan 1900')	
		,UrinaryTestReceivedTime = nullif(Details.value('data(UTITest/UrinaryTestResultsReceived_TS)[1]', 'datetime'), '1 jan 1900')											
		,UrinaryTestPositive = Details.value('data(UTITest/TestResultsPositive)[1]', 'bit')
		,WardCode = Locations.PASCode
		,UTITest.Details
	from
		[$(BedmanTest)].dbo.Event UTITest

	inner join [$(BedmanTest)].dbo.HospSpell Spell
	on	Spell.SpellID = UTITest.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on UTITest.LocationID = Locations.LocationID

	where
		EventTypeID = 9
	and not exists --If we have the same event type occuring at the same time, get the latest occurence
				(
				select	
					1
				from
					[$(BedmanTest)].dbo.Event UTITestLater
				where
					UTITestLater.EventTypeID = 9
				and UTITestLater.HospSpellID = UTITest.HospSpellID
				and	UTITestLater.Details.value('data(UTITest/UrinaryTestResultsSent_TS)[1]', 'datetime') = UTITest.Details.value('data(UTITest/UrinaryTestResultsSent_TS)[1]', 'datetime')
				and	UTITestLater.ID > UTITest.ID
				)
)
,
--UTIMedication
UTIMedicationCTE
(
	SourceUniqueID
	,SourcePatientNo 
	,SourceSpellNo 
	,TreatmentStartDate 
	,TreatmentStartTime 
	,TreatmentEndDate  
	,TreatmentEndTime 
	,WardCode
	,Details
)
as
	(
	select
		SourceUniqueID = UTIMedication.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,TreatmentStartDate = nullif(Details.value('data(UTIMedication/TreatmentStarted_TS)[1]', 'date'), '1 jan 1900')
		,TreatmentStartTime = nullif(Details.value('data(UTIMedication/TreatmentStarted_TS)[1]', 'datetime'), '1 jan 1900')
		,TreatmentEndDate = nullif(Details.value('data(UTIMedication/TreatmentFinished_TS)[1]', 'date'), '1 jan 1900')	
		,TreatmentEndTime = nullif(Details.value('data(UTIMedication/TreatmentFinished_TS)[1]', 'datetime'), '1 jan 1900')					
		,WardCode = Locations.PASCode
		,UTIMedication.Details
	from
		[$(BedmanTest)].dbo.Event UTIMedication

	inner join [$(BedmanTest)].dbo.HospSpell Spell
	on	Spell.SpellID = UTIMedication.HospSpellID

	inner join [$(BedmanTest)].dbo.Locations
	on UTIMedication.LocationID = Locations.LocationID

	where
		EventTypeID = 10
	)

--	now bring the four datasets together to form a consolidated UTI dataset

select
	UTISymptomCTE.SourceUniqueID
	,UTISymptomCTE.SourcePatientNo
	,UTISymptomCTE.SourceSpellNo
	,UTISymptomCTE.SymptomStartDate
	,UTISymptomCTE.SymptomStartTime
	,UTISymptomCTE.SymptomEndDate
	,UTISymptomCTE.SymptomEndTime
	,UTITestCTE.UrinaryTestRequestedDate 
	,UTITestCTE.UrinaryTestRequestedTime
	,UTITestCTE.UrinaryTestReceivedDate 
	,UTITestCTE.UrinaryTestReceivedTime 										
	,UTITestCTE.UrinaryTestPositive 									
	,TreatmentStartDate 
	,TreatmentStartTime 
	,TreatmentEndDate  
	,TreatmentEndTime 
	,CatheterInsertedDate = CatheterCTE.InsertedDate
	,CatheterInsertedTime = CatheterCTE.InsertedTime
	,CatheterRemovedDate = CatheterCTE.RemovedDate
	,CatheterRemovedTime = CatheterCTE.RemovedTime
	,CatheterTypeID
	,UTISymptomCTE.WardCode
	,InterfaceCode = 'BEDMAN'

from
	UTISymptomCTE

--Only link the first positive test recorded on or after the symptom date
left outer join UTITestCTE 
on	UTISymptomCTE.SourcePatientNo = UTITestCTE.SourcePatientNo
and	UTISymptomCTE.SourceSpellNo = UTITestCTE.SourceSpellNo
and UTITestCTE.UrinaryTestPositive = 1
and	UTITestCTE.UrinaryTestRequestedDate >= UTISymptomCTE.SymptomStartDate 
and not exists
			(
			select
				1
			from
				UTITestCTE UTITestCTEEarlier
			where
				UTITestCTEEarlier.SourcePatientNo = UTITestCTE.SourcePatientNo
			and	UTITestCTEEarlier.SourceSpellNo = UTITestCTE.SourceSpellNo
			and UTITestCTEEarlier.UrinaryTestPositive = 1
			and	UTITestCTEEarlier.UrinaryTestRequestedTime < UTITestCTE.UrinaryTestRequestedTime
			and	UTITestCTEEarlier.UrinaryTestRequestedDate >= UTISymptomCTE.SymptomStartDate
			
			) 

--Only link the first treatment recorded on or after the symptom date
left outer join UTIMedicationCTE	
on	UTISymptomCTE.SourcePatientNo = UTIMedicationCTE.SourcePatientNo
and	UTISymptomCTE.SourceSpellNo = UTIMedicationCTE.SourceSpellNo	
and UTIMedicationCTE.TreatmentStartDate > = UTISymptomCTE.SymptomStartDate
and not exists
			(
			select
				1
			from
				UTIMedicationCTE UTIMedicationCTEEarlier
			where
				UTIMedicationCTEEarlier.SourcePatientNo = UTIMedicationCTE.SourcePatientNo
			and	UTIMedicationCTEEarlier.SourceSpellNo = UTIMedicationCTE.SourceSpellNo
			and	UTIMedicationCTEEarlier.TreatmentStartTime < UTIMedicationCTE.TreatmentStartTime
			and	UTIMedicationCTEEarlier.TreatmentStartDate >= UTISymptomCTE.SymptomStartDate
			) 	

--Only link the latest catheter recorded on or before the symptom date
left outer join CatheterCTE	
on	UTISymptomCTE.SourcePatientNo = CatheterCTE.SourcePatientNo
and	UTISymptomCTE.SourceSpellNo = CatheterCTE.SourceSpellNo	
and UTISymptomCTE.SymptomStartDate >= CatheterCTE.InsertedDate 
and not exists
			(
			select
				1
			from
				CatheterCTE CatheterCTELater
			where
				CatheterCTELater.SourcePatientNo = UTISymptomCTE.SourcePatientNo
			and	CatheterCTELater.SourceSpellNo = UTISymptomCTE.SourceSpellNo
			and	CatheterCTELater.InsertedTime > CatheterCTE.InsertedTime
			and	CatheterCTELater.InsertedDate <= UTISymptomCTE.SymptomStartDate
			) 	





















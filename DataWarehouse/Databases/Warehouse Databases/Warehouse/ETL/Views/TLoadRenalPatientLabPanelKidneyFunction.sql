﻿

CREATE view [ETL].[TLoadRenalPatientLabPanelKidneyFunction] as

select
SourceUniqueID = cast(ObjectID as varchar(100))
,PatientObjectID
,PatientFullName = PatientFullName collate Latin1_General_CI_AS
,PatientMedicalRecordNo = PatientMedicalRecordNo collate Latin1_General_CI_AS
,LabTestObjectID
,LabTestDate = LabTestDate_Local
,LabTestQualifier = LabTestQualifier collate Latin1_General_CI_AS
,LabTestStatus = LabTestStatus collate Latin1_General_CI_AS
,[24hrCreatinineNum]
,[24hrCreatinineStr] = [24hrCreatinineStr] collate Latin1_General_CI_AS
,[24hrCreatinineFlg] = [24hrCreatinineFlg] collate Latin1_General_CI_AS
,[24hProteinNum]
,[24hProteinStr] = [24hProteinStr] collate Latin1_General_CI_AS
,[24hProteinFlg] = [24hProteinFlg] collate Latin1_General_CI_AS
,[24hUreaNum]
,[24hUreaStr] = [24hUreaStr] collate Latin1_General_CI_AS
,[24hUreaFlg] = [24hUreaFlg] collate Latin1_General_CI_AS
,CreatinineNum
,CreatinineStr = CreatinineStr collate Latin1_General_CI_AS
,CreatinineFlg = CreatinineFlg collate Latin1_General_CI_AS
,CreatinineUrineNum
,CreatinineUrineStr = CreatinineUrineStr collate Latin1_General_CI_AS
,CreatinineUrineFlg = CreatinineUrineFlg collate Latin1_General_CI_AS
,CreatinineClearanceNum
,CreatinineClearanceStr = CreatinineClearanceStr collate Latin1_General_CI_AS
,CreatinineClearanceFlg = CreatinineClearanceFlg collate Latin1_General_CI_AS
,KtVNum
,KtVStr = KtVStr collate Latin1_General_CI_AS
,KtVFlg = KtVFlg collate Latin1_General_CI_AS
,ProteinNum
,ProteinStr = ProteinStr collate Latin1_General_CI_AS
,ProteinFlg = ProteinFlg collate Latin1_General_CI_AS
,ProteinUrineNum
,ProteinUrineStr = ProteinUrineStr collate Latin1_General_CI_AS
,ProteinUrineFlg = ProteinUrineFlg collate Latin1_General_CI_AS
,RecirculationRatioNum
,RecirculationRatioStr = RecirculationRatioStr collate Latin1_General_CI_AS
,RecirculationRatioFlg = RecirculationRatioFlg collate Latin1_General_CI_AS
,BUNNum
,BUNStr = BUNStr collate Latin1_General_CI_AS
,BUNFlg = BUNFlg collate Latin1_General_CI_AS
,UreaUrineNum
,UreaUrineStr = UreaUrineStr collate Latin1_General_CI_AS
,UreaUrineFlg = UreaUrineFlg collate Latin1_General_CI_AS
,BloodUreaNitrogenArterialNum
,BloodUreaNitrogenArterialStr = BloodUreaNitrogenArterialStr collate Latin1_General_CI_AS
,BloodUreaNitrogenArterialFlg = BloodUreaNitrogenArterialFlg collate Latin1_General_CI_AS
,UreaClearanceNum
,UreaClearanceStr = UreaClearanceStr collate Latin1_General_CI_AS
,UreaClearanceFlg = UreaClearanceFlg collate Latin1_General_CI_AS
,UreaCreaRatioNum
,UreaCreaRatioStr = UreaCreaRatioStr collate Latin1_General_CI_AS
,UreaCreaRatioFlg = UreaCreaRatioFlg collate Latin1_General_CI_AS
,BloodUreaNitrogenPeripheralNum
,BloodUreaNitrogenPeripheralStr = BloodUreaNitrogenPeripheralStr collate Latin1_General_CI_AS
,BloodUreaNitrogenPeripheralFlg = BloodUreaNitrogenPeripheralFlg collate Latin1_General_CI_AS
,URRNum
,URRStr = URRStr collate Latin1_General_CI_AS
,URRFlg = URRFlg collate Latin1_General_CI_AS
,BloodUreaNitrogenVenousNum
,BloodUreaNitrogenVenousStr = BloodUreaNitrogenVenousStr collate Latin1_General_CI_AS
,BloodUreaNitrogenVenousFlg = BloodUreaNitrogenVenousFlg collate Latin1_General_CI_AS
,VolumeNum
,VolumeStr = VolumeStr collate Latin1_General_CI_AS
,VolumeFlg = VolumeFlg collate Latin1_General_CI_AS
,CollTimeNum
,CollTimeStr = CollTimeStr collate Latin1_General_CI_AS
,CollTimeFlg = CollTimeFlg collate Latin1_General_CI_AS
from ETL.[TLoadRenalPatientLabPanelKidneyFunctionBase]

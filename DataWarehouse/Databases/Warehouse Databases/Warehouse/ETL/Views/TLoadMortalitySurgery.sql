﻿




CREATE view [ETL].[TLoadMortalitySurgery] as

WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' AS ns)

select
	 SourceUniqueID = ReceivedForm.ID
	,FormTypeID = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Header/ns:FormID)[1]', 'INT')
	,ReviewStatus
	,Forename = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:FirstName)[1]', 'VARCHAR(50)')       
	,Surname = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:LastName)[1]', 'VARCHAR(50)')  
	,CasenoteNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:CaseNote)[1]', 'VARCHAR(50)')  
	,DateOfDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Patient/ns:DateTimeOfDeath)[1]', 'DATETIME')  
	,ReviewedDate = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DateOfReview)[1]', 'DATE') 
	,ReviewedBy = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:Reviewer)[1]', 'VARCHAR(50)')     
	,RecordedAppropriatelyToYou = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:RecordedAppropriatelyToYouYN)[1]', 'bit')  
	,AlternateNamedConsultant= ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:AlternateNamedConsultant)[1]', 'VARCHAR(50)')  
	,TerminalCareDeathExpected = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:TerminalCareDeathExpectedYN)[1]', 'bit')  
	,AgreeWithCauseOfDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:AgreeWithCauseOfDeathYN)[1]', 'bit')  
	,DidSurgeryDuringLastAdmission = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DidSurgeryDuringLastAdmYN)[1]', 'bit')  
	,HowManyDaysBeforeDeathApprox = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:HowManyDaysBeforeDeathApprox)[1]', 'int')  
	,CoronerInformed = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoronerInformedYN)[1]', 'bit')  
	,HighLevelInvestigation = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:HighLevelInvestigationYN)[1]', 'bit')  
	,CaseReferredToBleepReview = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CaseReferredToBleepReviewYN)[1]', 'bit')  
	,SurgicalComplicationContributedToDeath = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:SurgicalComplicationContribToDeathYN)[1]', 'bit')  
	,SurgeryHigherThanNormalAnticipatedMortality = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:SurgeryHigherThanNormalAnticipatedMortalityYN)[1]', 'bit')  
	,StandardRiskPercentage = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:StandardRiskPercentage)[1]', 'int')  
	,MortalityQuotedPercentage = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:MortalityQuotedPercentage)[1]', 'int')  
	,DeathPreventable = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DeathPreventableYN)[1]', 'bit')  
	,DeathPreventableComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DeathPreventableC)[1]', 'VARCHAR(max)')  
	,TerminalCareManagedWell = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:TerminalCareManagedWellYN)[1]', 'bit')  
	,TerminalCareManagedWellComment = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:TerminalCareManagedWellC)[1]', 'VARCHAR(max)')  
	,CauseOfDeath1 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeath)[1]', 'VARCHAR(max)')  
	,CauseOfDeath2 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeath)[2]', 'VARCHAR(max)')
	,CauseOfDeath3 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeath)[3]', 'VARCHAR(max)')
	,CauseOfDeath4 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CauseOfDeath)[4]', 'VARCHAR(max)')
	,CoMorbidity1 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoMorbidity)[1]', 'VARCHAR(max)')  
	,CoMorbidity2 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoMorbidity)[2]', 'VARCHAR(max)')
	,CoMorbidity3 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoMorbidity)[3]', 'VARCHAR(max)')
	,CoMorbidity4 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoMorbidity)[4]', 'VARCHAR(max)')
	,CoMorbidity5 = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:CoMorbidity)[5]', 'VARCHAR(max)')
	,DeathDiscussedWithAnaesthetist = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:DeathDiscussedWithAnaesthetistYN)[1]', 'bit')  
	,Anaesthetist = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:AnaesthetistName)[1]', 'VARCHAR(50)')  
	,ClassificationGrade = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:ClassificationGrade)[1]', 'VARCHAR(50)') 
	,SourcePatientNo = ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:sPN)[1]', 'varchar(20)')  
	,EpisodeStartTime = cast(ReceivedForm.FormXML.value('(ns:InfoPathSubmission/ns:Review/ns:eST)[1]', 'varchar(50)') as datetime)
	,ContextCode
from
	[$(InfoPathRepository)].InfoPath.ReceivedForms ReceivedForm

inner join
       [$(Information)].Information.MortalityReviewForms
on     MortalityReviewForms.MortalityReviewFormID = ReceivedForm.ID

where
	ReceivedForm.FormTypeID = 302 --Division of Surgery - Mortality Review








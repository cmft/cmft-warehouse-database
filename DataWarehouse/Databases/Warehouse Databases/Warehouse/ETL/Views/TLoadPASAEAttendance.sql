﻿






CREATE view [ETL].[TLoadPASAEAttendance] as

/* 
==============================================================================================
Description:	

When		Who			What
20150508	Paul Egan	Initial Coding
===============================================================================================
*/

select 
	 SourceUniqueID = AEATTENDANCEID
	,SourcePatientNo = cast(InternalPatientNumber as int)
	,SourceEncounterNo = cast(EpisodeNumber as int)
	,ArrivalTime = 
		case 
			when right(AeAttendanceDatetimeneutral, 4) = '2400'
				then cast(dateadd(day, 1, cast(left(AeAttendanceDatetimeneutral, 8) as date)) as datetime2(0))
			else cast(stuff(stuff(AeAttendanceDatetimeneutral, 11, 0, ':'), 9, 0, ' ') as datetime2(0))
		end
	,DepartmentCode
	,HospitalCode
from
	ETL.TImportPASAEAttendance 
--order by 
--	AeAttendanceDatetimeneutral
;






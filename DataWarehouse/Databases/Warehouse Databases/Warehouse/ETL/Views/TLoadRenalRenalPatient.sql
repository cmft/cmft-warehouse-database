﻿


CREATE view [ETL].[TLoadRenalRenalPatient] as
select
	SourceUniqueID = cast(ObjectID as varchar(100))
	,FirstName = FirstName collate Latin1_General_CI_AS
	,MiddleName = MiddleName collate Latin1_General_CI_AS
	,LastName = LastName collate Latin1_General_CI_AS
	,Title = Title collate Latin1_General_CI_AS
	,FullName = FullName collate Latin1_General_CI_AS
	,ExtendedName = ExtendedName collate Latin1_General_CI_AS
	,ShortName = ShortName collate Latin1_General_CI_AS
	,Initials = Initials collate Latin1_General_CI_AS
	,Signature = Signature collate Latin1_General_CI_AS
	,MedicalRecordNo = MedicalRecordNo collate Latin1_General_CI_AS
	,NationalHealthServiceNo = NationalHealthServiceNo collate Latin1_General_CI_AS
	,DateOfBirth = DateOfBirth_Local
	,BirthPlace = BirthPlace collate Latin1_General_CI_AS
	,DateOfDeath = DateOfDeath_Local
	,CauseOfDeath = CauseOfDeath collate Latin1_General_CI_AS
	,EMailAddress = EMailAddress collate Latin1_General_CI_AS
	,MaritalStatus = MaritalStatus collate Latin1_General_CI_AS
	,Age
	,Sex = Sex collate Latin1_General_CI_AS
	,EducationLevel = EducationLevel collate Latin1_General_CI_AS
	,PrimaryPhone = PrimaryPhone collate Latin1_General_CI_AS
	,PrimaryPhoneObjectID
	,MailingAddress = MailingAddress collate Latin1_General_CI_AS
	,MailingAddressObjectID
	,Race = Race collate Latin1_General_CI_AS
	,Ethnicity = Ethnicity collate Latin1_General_CI_AS
	,Religion = Religion collate Latin1_General_CI_AS
	,BloodGroup = BloodGroup collate Latin1_General_CI_AS
	,BloodRhesus = BloodRhesus collate Latin1_General_CI_AS
	,DiabeticStatus = DiabeticStatus collate Latin1_General_CI_AS
	,KidneyFunction = KidneyFunction collate Latin1_General_CI_AS
	,PrimaryCaregiver = PrimaryCaregiver collate Latin1_General_CI_AS
	,MedicalPractice = MedicalPractice collate Latin1_General_CI_AS
	,MedicalPracticeObjectID
	,MedicalPracticeAddress = MedicalPracticeAddress collate Latin1_General_CI_AS
	,GeneralPractitioner = GeneralPractitioner collate Latin1_General_CI_AS
	,GeneralPractitionerFirstName = GeneralPractitionerFirstName collate Latin1_General_CI_AS
	,GeneralPractitionerLastName = GeneralPractitionerLastName collate Latin1_General_CI_AS
	,HearingImpairment = HearingImpairment collate Latin1_General_CI_AS
	,SightImpairment = SightImpairment collate Latin1_General_CI_AS
	,SpeechImpairment = SpeechImpairment collate Latin1_General_CI_AS
	,InterpreterRequired = InterpreterRequired collate Latin1_General_CI_AS
	,IsAmbulatory = IsAmbulatory collate Latin1_General_CI_AS
	,AmbulatoryStatus = AmbulatoryStatus collate Latin1_General_CI_AS
	,WheelchairStatus = WheelchairStatus collate Latin1_General_CI_AS
	,OxygenRequired = OxygenRequired collate Latin1_General_CI_AS
	,PrimaryCareGiverObjectID
	,GeneralPractitionerObjectID
	,PrimaryPharmacyObjectID
	,PrimaryTransportObjectID
	,VeteranStatus = VeteranStatus collate Latin1_General_CI_AS
	,AdminCategory = AdminCategory collate Latin1_General_CI_AS
	,PrimaryContactPersonName = PrimaryContactPersonName collate Latin1_General_CI_AS
	,PrimaryContactPersonObjectID
	,PrimaryRenalModalityStartDate = PrimaryRenalModalityStartDate_Local
	,PrimaryRenalModalityStopDate = PrimaryRenalModalityStopDate_Local
	,PrimaryRenalModality = PrimaryRenalModality collate Latin1_General_CI_AS
	,PrimaryRenalModalitySetting = PrimaryRenalModalitySetting collate Latin1_General_CI_AS
	,PrimaryRenalModalityDialysisProvider = PrimaryRenalModalityDialysisProvider collate Latin1_General_CI_AS
	,PrimaryRenalModalityObjectID
	,PrimaryRenalModalityDialysisProviderObjectID
	,PrimaryKidneyTransplantStatusObjectID
	,IsolationStatus = IsolationStatus collate Latin1_General_CI_AS
	,IsActive
	,RegistrationDate = RegistrationDate_Local
	,InactiveDate = InactiveDate_Local
	,ReasonInactive = ReasonInactive collate Latin1_General_CI_AS
	,PrimaryNephrologist = PrimaryNephrologist collate Latin1_General_CI_AS
	,PrimaryNephrologistObjectID
	,WeeklyCalendar = WeeklyCalendar collate Latin1_General_CI_AS
	,PrimaryRenalDiagnosis = PrimaryRenalDiagnosis collate Latin1_General_CI_AS
	,RenalDiagnosisDateOfOnset = RenalDiagnosisDateOfOnset_Local
	,OtherRenalDiagnosis = OtherRenalDiagnosis collate Latin1_General_CI_AS
	,RenalDiagnosisCode = RenalDiagnosisCode collate Latin1_General_CI_AS
	,RenalDiagnosisDescription = RenalDiagnosisDescription collate Latin1_General_CI_AS
	,RegistrationHeight
	,RegistrationHeightUnits = RegistrationHeightUnits collate Latin1_General_CI_AS
	,RegistrationHeightInCm
	,RegistrationWeight
	,RegistrationWeightUnits = RegistrationWeightUnits collate Latin1_General_CI_AS
	,RegistrationWeightInKg
	,Remarks = Remarks collate Latin1_General_CI_AS
	,DateFirstSeenByPhysician
	,PreDialysisFollowUp = PreDialysisFollowUp collate Latin1_General_CI_AS
	,PreDialysisErythropoietin = PreDialysisErythropoietin collate Latin1_General_CI_AS
	,UKRROptOut
	,PediatricPatient
	,GeneticRelationshipBetweenParents
	,SingleAdultResponsible
	,OtherFamilyMembersAlsoESRF
	,OtherFamilyMembersSameRenalDisease
	,DateReferredToAdultRenalServices = DateReferredToAdultRenalServices_Local
	,DateRegisteredWithBAPN = DateRegisteredWithBAPN_Local
	,PrimaryHDScheduleObjectID
	,PrimaryVascularAccessObjectID
from ETL.TLoadRenalRenalPatientBase




﻿





CREATE view [ETL].[TLoadObservationObservationProfile] as

select
	SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,LocationID
	,AdmissionSourceUniqueID
	,ProfileID
	,TemplatePriorityID
	,DrivingTableCode
	,DrivingTableID
	,ProfileReasonID
	,StepNumber
	,IterationNumber
	,StartTime
	,EndTime
	,CreatedBy
	,CreatedTime
	,LastModifiedBy
	,LastModifiedTime
	,Active
	,InterfaceCode = 'PTRACK'
from
	ETL.TImportObservationObservationProfile
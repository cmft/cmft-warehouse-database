﻿












CREATE view [ETL].[TLoadResultGPICEResult] as

select 
	SourceUniqueID
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,DateOfBirth
	,SexCode
	,ClinicianID
	,MainSpecialtyCode
	,SpecialtyCode
	,LocationID
	,ReportSourceUniqueID
	,ReportStatusCode
	,ReportTime
	,ReportComment = 
				replace(
					ReportComment
					, '||', '' + char(13)
					)
	,SampleReferenceCode
	,SampleTypeCode
	,SampleType
	,SampleCollectionTime
	,InvestigationCode
	,InvestigationName
	,InvestigationComment =
				replace(
					InvestigationComment
					, '||', '' + char(13)
					)
	,ResultCode
	,ResultName
	,Result
	,UnitOfMeasurement
	,Abnormal
	,LowerReferenceRangeValue
	,UpperReferenceRangeValue
	,ResultComment = 
				replace(
					ResultComment
					, '||', '' + char(13)
					)
	,InterfaceCode = 
					case
					when SpecialtyCode in
										(
										'820'
										,'821'
										,'822'
										,'823'
										)
					then 'APEX'
					when SpecialtyCode = '831'
					then 'TPATH'
					when SpecialtyCode = '824'
					then 'MLAB'
					when SpecialtyCode = '810'
					then 'CRIS'
					end
from
	ETL.TImportResultGPICEResult

















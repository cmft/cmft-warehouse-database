﻿
CREATE view [ETL].[TLoadRADWaitingList] as

	/******************************************************************************
	**  Name: TLoadRADWaitingList
	**  Purpose: 
	**
	**  Import procedure view for Radiology RAD.WaitingList table rows 
	**
	**	Any transformations to columns should be made here between the import table
	**  and the data warehouse table.
	**
	**	NOTE - Please maintain this view via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:	Author:      Description:
	** --------	------------ ---------------------------------------------------- 
	** 20120626     MH		Altered for CMFT environment
	** 20121003     MH		Added CensusDate column
	** 20121101     MH		Added StatusType
	** 20150924	Paul Egan	Added clock-reset columns
	******************************************************************************/

SELECT
	 ImportDateTimeStamp
	,CensusDate = WL.CensusDate
	,SourceUniqueID = WL.[Event key]
	,SourcePatientNo = WL.[CRIS No ]
	,DistrictNo = RIGHT(Utility.udfEmptyStringToNull(WL.[Best Hosp No ]) , 8)
	,PatientSurname = Utility.udfEmptyStringToNull(WL.[Surname])
	,PatientForenames = Utility.udfEmptyStringToNull(WL.[Forenames])
	,PatientTitle = Utility.udfEmptyStringToNull(Patient.Title)
	,DateOfBirth = Utility.udfConvertDate(WL.[DOB])
	,SexCode = Utility.udfEmptyStringToNull(WL.[Sex])
	,NHSNumber = Utility.udfEmptyStringToNull(WL.[NHS number])
	,PatientAddress1 = Utility.udfEmptyStringToNull(Patient.AddressLine1)
	,PatientAddress2 = Utility.udfEmptyStringToNull(Patient.AddressLine2)
	,PatientAddress3 = Utility.udfEmptyStringToNull(Patient.AddressLine3)
	,PatientAddress4 = Utility.udfEmptyStringToNull(Patient.AddressLine4)
	,Postcode = Utility.udfEmptyStringToNull(Patient.Postcode)
	,ExamCode = Utility.udfEmptyStringToNull(WL.[Examination])
	,SiteCode = Utility.udfEmptyStringToNull(WL.[Site])
	,StatusCode = Utility.udfEmptyStringToNull(WL.[Status Cde])
	,RequestDate = Utility.udfConvertDate(WL.[Request date])
	,EventDate =
		CASE 
			WHEN Utility.udfEmptyStringToNull(WL.[Event Date]) = '01/01/2099' THEN NULL
			ELSE Utility.udfConvertDate(rtrim(WL.[Event Date]))
		END
	,DaysWaiting = WL.[ReqToA]	-- Total days Request to Attended. Removed 'Adjusted' from name Paul Egan 20150924
	,WeeksWaiting = WL.[Weeks Waiting]
	,ModalityCode = Utility.udfEmptyStringToNull(WL.[Modality])
	,RegisteredGpCode = Utility.udfEmptyStringToNull(WL.[GP])
	,RegisteredPracticeCode = Utility.udfEmptyStringToNull(WL.[Practice])
	,RegisteredPCTCode = Utility.udfEmptyStringToNull(WL.[PCT])
	,SpecialtyCode = Utility.udfEmptyStringToNull(WL.[Speciality])
	,PatientTypeCode = Utility.udfEmptyStringToNull(WL.[PT])
	--,PatientType = WL.[Patient Type Des]
	,UrgencyCode = Utility.udfEmptyStringToNull(WL.[Urg])
	,Comment = Utility.udfEmptyStringToNull(WL.[Comment])
	,ExamGroupCode1 = Utility.udfEmptyStringToNull(WL.[Group 1])
	,ExamGroupCode2 = Utility.udfEmptyStringToNull(WL.[Group 2])
	,ReferralSourceCode = Utility.udfEmptyStringToNull(WL.[Ref Src])
	,ReferrerCode = Utility.udfEmptyStringToNull(WL.[Referrer])
	--,Referrer = WL.[Referrer Name]
	,ReferralLocationCode = Utility.udfEmptyStringToNull(WL.[Referring Locat])
	,WaitingButNotPlanned = Utility.udfEmptyStringToNull(WL.[Wait N  Pl])
	,Planned = Utility.udfEmptyStringToNull(WL.[Planned])
	,Scheduled = Utility.udfEmptyStringToNull(WL.[Scheduled])
	,Waiting = Utility.udfEmptyStringToNull(WL.[Was Waiting])
	,StatusType = Utility.udfEmptyStringToNull(WL.[StatusType])
	
	/* Added Paul Egan 20150924 */
	,ClockResetDate = Utility.udfConvertDate(WL.ClockResetDate)
	,DaysWaitingAdjusted = WL.DaysWaiting					-- Takes into account clock resets (e.g. patient cancellations) on status history
	,BreachDate = Utility.udfConvertDate(WL.BreachDate)		-- Takes into account clock resets (e.g. patient cancellations) on status history
	,ExamSourceUniqueID = WL.ExamKey
FROM
	[$(CRIS_Import)].dbo.TImportRADWaitingList WL

left join PAS.Patient
on	right(rtrim(WL.[Best Hosp No ]) , 8) = Patient.DistrictNo

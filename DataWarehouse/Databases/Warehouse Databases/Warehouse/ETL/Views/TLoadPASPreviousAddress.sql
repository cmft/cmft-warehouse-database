﻿
create view [ETL].TLoadPASPreviousAddress

as

select
	--top 1000 
	[PREVADDRESSID]
	,DistrictNo = [DistrictNumber]
	,[EffectiveFromDate] = cast([EffectiveFromDate] as datetime)
	,[EffectiveFromDateInt]
	,[EffectiveToDate] = cast([EffectiveToDate] as datetime)
	,[EffectiveToDateInt]
	,[HaCode]
	,[InternalPatientNumber]
	,[Postcode]
	,[PseudoPostCode]
	,PatientAddressLine1 = [PtAddLn1]
	,PatientAddressLine2 = [PtAddLn2]
	,PatientAddressLine3 = [PtAddLn3]
	,PatientAddressLine4 = [PtAddLn4]
	,[SequenceNo] = [SeqNo]
from
	[ETL].[TImportPASPreviousAddress]
	

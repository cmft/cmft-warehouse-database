﻿

--Use Warehouse

CREATE View [ETL].[TLoadBloodManagementTransaction]

as

select 
	TransactionID
	,TransactionTypeID
	,TransactionTime
	,ConditionID
	,BloodUniqueID 
	,DonorNumber 
	,BloodProductID
	,BloodProductVolume 
	,BloodGroupID
	,ReservationTime
	,ExpiryTime
	,FateID 
	,SiteID 
	,LocationID
	,PatientID
	,HospitalNumber
	,NHSNumber 
	,LastName
	,FirstName
	,DateOfBirth 
	,GenderCode
	,SystemUserID
	,InterfaceCode 
from 
	ETL.TImportBloodManagementTransaction



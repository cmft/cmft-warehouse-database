﻿
CREATE view ETL.TLoadACUOPFertility 
as
 
 
select 
	SourceID = DateKey
	,HospitalNumber = hospnum
	,PatientForename = right(PatientName,(len(PatientName)-charindex(',',PatientName)-1))
	,PatientSurname = 
		case
			when PatientName like '%,%' then left(PatientName,charindex(',',PatientName)-1)
			else PatientName
		end
	,EncounterDate = 
		case 
			when GonadStartDate in ('0200-06-22','0200-04-03','0112-07-19','0014-09-08')
			then null
			else GonadStartDate
		end
	,TreatmentType
	,DateOfBirth = DOB
	,Age
	,ReferralSource = [source]
	,InterfaceCode = 'ACU' -- Assisted Conception Unit
from 
	[$(ACU)].ACUBASE.IVF
where
	hospnum is not null
and PatientName is not null
	
	
--Go
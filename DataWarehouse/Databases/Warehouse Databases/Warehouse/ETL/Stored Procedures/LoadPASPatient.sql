﻿
CREATE PROCEDURE [ETL].[LoadPASPatient]
(
@Update bit
)
 
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, Modified)) 
	,@LoadEndDate = MAX(CONVERT(datetime, Modified)) 
from
	ETL.TLoadPASPatient

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	PAS.Patient target
using
	(
	select
		 SourcePatientNo
		,DateOfBirth
		,DeathIndicator
		,DateOfDeath
		,SexCode
		,MaritalStatusCode
		,Surname
		,Forenames
		,Title
		,NextOfKin
		,EthnicOriginCode
		,ReligionCode
		,PreviousSurname
		,NHSNumber
		,NHSNumberStatusId
		,DOR
		,DistrictNo
		,MilitaryNo
		,AddressLine1
		,AddressLine2
		,AddressLine3
		,AddressLine4
		,Postcode
		,RegisteredGpCode
		,RegisteredGdpCode
		,MRSAFlag
		,MRSADate
		,Modified
		,HomePhone
		,MobilePhone
		,WorkPhone
		,AEAttendanceToday
		,Deleted = 0
	from
		ETL.TLoadPASPatient
	) source
	on	source.SourcePatientNo = target.SourcePatientNo
	
	when not matched by source
	and target.Deleted != 1 -- not already marked as deleted
	and @Update = 0 -- only if executing main ETL process

	then
		update
		set
			target.Deleted = 1 -- retain record but denote as deleted from PAS. 
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

	when not matched by target
	then
		insert
			(
			 SourcePatientNo
			,DateOfBirth
			,DeathIndicator
			,DateOfDeath
			,SexCode
			,MaritalStatusCode
			,Surname
			,Forenames
			,Title
			,NextOfKin
			,EthnicOriginCode
			,ReligionCode
			,PreviousSurname
			,NHSNumber
			,NHSNumberStatusId
			,DOR
			,DistrictNo
			,MilitaryNo
			,AddressLine1
			,AddressLine2
			,AddressLine3
			,AddressLine4
			,Postcode
			,RegisteredGpCode
			,RegisteredGdpCode
			,MRSAFlag
			,MRSADate
			,Modified
			,HomePhone
			,MobilePhone
			,WorkPhone
			,AEAttendanceToday
			,Deleted
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 SourcePatientNo
			,DateOfBirth
			,DeathIndicator
			,DateOfDeath
			,SexCode
			,MaritalStatusCode
			,Surname
			,Forenames
			,Title
			,NextOfKin
			,EthnicOriginCode
			,ReligionCode
			,PreviousSurname
			,NHSNumber
			,NHSNumberStatusId
			,DOR
			,DistrictNo
			,MilitaryNo
			,AddressLine1
			,AddressLine2
			,AddressLine3
			,AddressLine4
			,Postcode
			,RegisteredGpCode
			,RegisteredGdpCode
			,MRSAFlag
			,MRSADate
			,Modified
			,HomePhone
			,MobilePhone
			,WorkPhone
			,AEAttendanceToday
			,Deleted
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DeathIndicator, '') = isnull(source.DeathIndicator, '')
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.MaritalStatusCode, '') = isnull(source.MaritalStatusCode, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.Forenames, '') = isnull(source.Forenames, '')
		and isnull(target.Title, '') = isnull(source.Title, '')
		and isnull(target.NextOfKin, '') = isnull(source.NextOfKin, '')
		and isnull(target.EthnicOriginCode, '') = isnull(source.EthnicOriginCode, '')
		and isnull(target.ReligionCode, '') = isnull(source.ReligionCode, '')
		and isnull(target.PreviousSurname, '') = isnull(source.PreviousSurname, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.NHSNumberStatusId, '') = isnull(source.NHSNumberStatusId, '')
		and isnull(target.DOR, '') = isnull(source.DOR, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.MilitaryNo, 0) = isnull(source.MilitaryNo, 0)
		and isnull(target.AddressLine1, '') = isnull(source.AddressLine1, '')
		and isnull(target.AddressLine2, '') = isnull(source.AddressLine2, '')
		and isnull(target.AddressLine3, '') = isnull(source.AddressLine3, '')
		and isnull(target.AddressLine4, '') = isnull(source.AddressLine4, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredGdpCode, '') = isnull(source.RegisteredGdpCode, '')
		and isnull(target.MRSAFlag, '') = isnull(source.MRSAFlag, '')
		and isnull(target.MRSADate, '') = isnull(source.MRSADate, '')
		and isnull(target.Modified, getdate()) = isnull(source.Modified, getdate())
		and isnull(target.HomePhone, '') = isnull(source.HomePhone, '')
		and isnull(target.MobilePhone, '') = isnull(source.MobilePhone, '')
		and isnull(target.WorkPhone, '') = isnull(source.WorkPhone, '')
		and isnull(target.AEAttendanceToday, 0) = isnull(source.AEAttendanceToday, 0)
		and isnull(target.Deleted, 0) = isnull(source.Deleted, 0)

		)
	
	then
		update
		set
			 target.SourcePatientNo = source.SourcePatientNo
			,target.DateOfBirth = source.DateOfBirth
			,target.DeathIndicator = source.DeathIndicator
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.Surname = source.Surname
			,target.Forenames = source.Forenames
			,target.Title = source.Title
			,target.NextOfKin = source.NextOfKin
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.ReligionCode = source.ReligionCode
			,target.PreviousSurname = source.PreviousSurname
			,target.NHSNumber = source.NHSNumber
			,target.NHSNumberStatusId = source.NHSNumberStatusId
			,target.DOR = source.DOR
			,target.DistrictNo = source.DistrictNo
			,target.MilitaryNo = source.MilitaryNo
			,target.AddressLine1 = source.AddressLine1
			,target.AddressLine2 = source.AddressLine2
			,target.AddressLine3 = source.AddressLine3
			,target.AddressLine4 = source.AddressLine4
			,target.Postcode = source.Postcode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGdpCode = source.RegisteredGdpCode
			,target.MRSAFlag = source.MRSAFlag
			,target.MRSADate = source.MRSADate
			,target.Modified = source.Modified
			,target.HomePhone = source.HomePhone
			,target.MobilePhone = source.MobilePhone
			,target.WorkPhone = source.WorkPhone
			,target.AEAttendanceToday = source.AEAttendanceToday
			,target.Deleted = source.Deleted
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;

select
	@updated = @updated + @@rowcount
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
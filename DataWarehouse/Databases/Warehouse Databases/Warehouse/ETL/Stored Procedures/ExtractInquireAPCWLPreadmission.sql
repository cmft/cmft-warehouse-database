﻿

CREATE    procedure [ETL].[ExtractInquireAPCWLPreadmission]
	@CensusDate smalldatetime
as

/******************************************************************************************************
Stored procedure : [ETL].[ExtractInquireAPCWLPreadmission]
Description		 : As proc name

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
17/10/2014	Paul Egan       Added TCITime for day case and inpatient appointment reminders.
*******************************************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()
select @RowsInserted = 0


insert into ETL.TImportAPCWLPreadmission 
(
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,RegisteredGpCode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,AdmissionReason
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,ExpectedAdmissionDate
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode
	,GuaranteedAdmissionDate
	,ShortNotice
	,TCITime	-- Added 17/10/2014 Paul Egan
)
select
	 SourcePatientNo
	,SourceEncounterNo
	,@CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,RegisteredGpCode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,AdmissionReason
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,ExpectedAdmissionDate
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode
	,GuaranteedAdmissionDate
	,ShortNotice
	,TCITime	-- Added 17/10/2014 Paul Egan
from
	(
	SELECT 
		 SourcePatientNo = Preadmission.InternalPatientNumber 
		,SourceEncounterNo = Preadmission.EpisodeNumber 
		,PatientSurname = Patient.Surname 
		,PatientForename = Patient.Forenames 
		,PatientTitle = Patient.Title 
		,SexCode = Patient.Sex 
		,DateOfBirth = Patient.PtDoB 
		,DateOfDeath = left(Patient.PtDateOfDeath, 10) 
		,Patient.NHSNumber
		,DistrictNo = Patient.DistrictNumber 
		,MaritalStatusCode = Patient.MaritalStatus 
		,ReligionCode = Patient.Religion 
		,Postcode = Patient.PtAddrPostCode 
		,RegisteredGpCode = Patient.GpCode 
		,PatientsAddress1 = Patient.PtAddrLine1 
		,PatientsAddress2 = Patient.PtAddrLine2 
		,PatientsAddress3 = Patient.PtAddrLine3 
		,PatientsAddress4 = Patient.PtAddrLine4 
		,DHACode = Patient.DistrictOfResidenceCode 
		,HomePhone = Patient.PtHomePhone 
		,WorkPhone = Patient.PtWorkPhone 
		,EthnicOriginCode = Patient.EthnicType 
		,ConsultantCode = Preadmission.Consultant 
		,SpecialtyCode = Preadmission.Specialty 
		,ManagementIntentionCode = Preadmission.IntdMgmt 
		,AdmissionMethodCode = Preadmission.MethodOfAdm 
		,PriorityCode = Preadmission.Urgency 
		,EpisodicGpCode = Preadmission.EpiGPCode 
		,EpisodicGpPracticeCode = Preadmission.EpiGPPractice 
		,WaitingListCode = Preadmission.WlCode 
		,CommentClinical = Preadmission.WLCommentCl 
		,CommentNonClinical = Preadmission.WLCommentNonClin 
		,IntendedPrimaryOperationCode = Preadmission.IntendedProc 
		,Preadmission.Operation
		,AdmissionReason= Preadmission.AdmReason 
		,SiteCode = Preadmission.PreadmitHospital 
		,WardCode = Preadmission.ExpectedWard 
		,WLStatus = 'Preadmission' 
		,ProviderCode = Preadmission.Provider 
		,PurchaserCode = Preadmission.Purchaser 
		,ContractSerialNumber = Preadmission.ContractID 
		,BookingTypeCode = Preadmission.BookingType 
		,CasenoteNumber = Preadmission.CaseNoteNumber 
		,OriginalDateOnWaitingList = Preadmission.OriginalDateOnList 
		,DateOnWaitingList = Preadmission.KH07StartWait 
		,Preadmission.TCIDate
		,KornerWait = Preadmission.KH07Wait 
		,CountOfDaysSuspended = Preadmission.KH07SusDays 
		,ExpectedAdmissionDate = null 
		,NextOfKinName = Patient.NoKName 
		,NextOfKinRelationship = Patient.NoKRelationship 
		,NextOfKinHomePhone = Patient.NoKHomePhone 
		,NextOfKinWorkPhone = Patient.NoKWorkPhone 
		,ExpectedLOS = Preadmission.ExpectedLos 
		,Patient.MRSA

		,RTTPathwayID = left(PW.PathwayNumber , 25) 
		,RTTPathwayCondition = PW.PathwayCondition 
		,RTTStartDate = PWP.RttStartDate 
		,RTTEndDate = PWP.RttEndDate 
		,RTTSpecialtyCode = PW.RttSpeciality 
		,RTTCurrentProviderCode = PW.RttCurProv 
		,RTTCurrentStatusCode = PW.RttCurrentStatus 
		,RTTCurrentStatusDate = PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
		,RTTOverseasStatusFlag = PW.RttOsvStatus 

		,AddedToWaitingListTime =
			(
			select
				min(Latest.EpsActvDtimeInt) 
			from
				[$(PAS)].Inquire.WLACTIVITY Latest
			where
				Latest.EpisodeNumber = Preadmission.EpisodeNumber
			and	Latest.InternalPatientNumber = Preadmission.InternalPatientNumber
			) 

		,SourceUniqueID = Preadmission.CURRENTPREADMISSIONSID 
		,AdminCategoryCode = Preadmission.Category 
		,GuaranteedAdmissionDate = Preadmission.GuaranteedAdmDate
		,WLEntry.ShortNotice
		,Preadmission.TCITime	-- Added 17/10/2014 Paul Egan
	FROM
		[$(PAS)].Inquire.PATDATA Patient

	INNER JOIN [$(PAS)].Inquire.CURRENTPREADMISSIONS Preadmission 
	ON	Patient.InternalPatientNumber = Preadmission.InternalPatientNumber

	left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
	on	EPW.InternalPatientNumber = Preadmission.InternalPatientNumber
	and	EPW.EpisodeNumber = Preadmission.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTPATHWAY PW
	on	PW.InternalPatientNumber = EPW.InternalPatientNumber
	and	PW.PathwayNumber = EPW.PathwayNumber

	left join [$(PAS)].Inquire.RTTPERIOD PWP
	on	PWP.InternalPatientNumber = PW.InternalPatientNumber
	and	PWP.PathwayNumber = PW.PathwayNumber
	and	PWP.PPeriodId = '0'
	
	left join [$(PAS)].Inquire.WLENTRY WLEntry
	on	WLEntry.InternalPatientNumber = Preadmission.InternalPatientNumber
	and	WLEntry.EpisodeNumber = Preadmission.EpisodeNumber

	WHERE
		Preadmission.IntdMgmt In ('I','D')
	AND	Preadmission.MethodOfAdm In ('WL','BA','BL','PL','BP','EL','PA')

	) Preadmission


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT

-- if the WLCurrent snapshot contains duplicate records keep this snapshot so that the entire Waiting List dataset can be loaded later

-- this block is redundant now that the WLCurrent load removes duplicates
-- also, there is an untraced bug preventing the load of duplicates into the holding table!
-- delete this bit when the load process is tested satisfactorily

--if
--	(
--	select
--		count(*)
--	from
--		TImportWLCurrentKeyViolation
--	where
--		CensusDate = (select top 1 CensusDate from TImportWLPreadmission)
--	) != 0
--
--begin
--	delete from TImportWLPreadmissionKeyViolation
--	where
--		exists
--			(
--			select
--				1
--			from
--				TImportWLPreadmission
--			where
--				TImportWLPreadmissionKeyViolation.CensusDate = TImportWLPreadmission.CensusDate
--			)
--
--	insert into TImportWLPreadmissionKeyViolation
--
--	select * from TImportWLPreadmission
--end




SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Census Date ' + CONVERT(varchar(11), @CensusDate) +
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireAPCWLPreadmission', @Stats, @StartTime



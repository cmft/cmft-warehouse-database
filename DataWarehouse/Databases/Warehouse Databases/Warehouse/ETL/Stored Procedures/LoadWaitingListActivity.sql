﻿CREATE       procedure [ETL].[LoadWaitingListActivity] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from Utility.Parameter where Parameter = 'WLACTIVITYFREEZEDATE')
		,dateadd(month, -24, @to)
	)

truncate table ETL.TImportWaitingListActivity
exec ETL.ExtractInquireWaitingListActivity @from, @to
exec ETL.LoadWaitingListActivityEncounter

truncate table ETL.TImportWaitingListEntry
exec ETL.ExtractInquireWaitingListEntry @from, @to
exec ETL.LoadWaitingListEntry


exec ETL.AssignAPCWaitingListCode


update Utility.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADWLACTIVITYDATE'

if @@rowcount = 0

insert into Utility.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADWLACTIVITYDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

EXEC Utility.WriteAuditLogEvent 'LoadWaitingListActivity', @Stats, @StartTime

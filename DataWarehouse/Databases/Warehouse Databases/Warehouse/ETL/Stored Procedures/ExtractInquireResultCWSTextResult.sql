﻿
CREATE proc [ETL].[ExtractInquireResultCWSTextResult] 
(
	@FromDate date
	,@ToDate date
) as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0


insert into ETL.TImportResultCWSTextResult
(
	SourcePatientNo
	,SourceEpisodeNo
	,TransactionNo
	,SessionNo
	,OrderSourceUniqueID
	,ConsultantCode
	,SpecialtyCode
	,OrderPriorityCode
	,OrderStatusCode
	,OrderEnteredByCode
	,OrderedByCode
	,OrderTime
	,OrderComment
	,SampleReferenceCode
	,EffectiveTime
	,LocationCode
	,ServiceCode
	,ResultCode
	,Result
)

select 
	SourcePatientNo 
	,SourceEpisodeNo 
	,TransactionNo 
	,SessionNo
	,OrderSourceUniqueID
	,ConsultantCode 
	,SpecialtyCode 
	,OrderPriorityCode
	,OrderStatusCode
	,OrderEnteredByCode 
	,OrderedByCode 
	,OrderTime  
	,OrderComment
	,SampleReferenceCode
	,EffectiveTime 
	,LocationCode 
	,ServiceCode
	,ResultCode
	,Result 
from
	(
	select
		SourcePatientNo = OCORDER.PatientNumber
		,SourceEpisodeNo = OCORDER.EpisodeNumber
		,TransactionNo = OCORDER.OcmTransKey
		,SessionNo = TextResult.SessionNumber
		,OrderSourceUniqueID = OCORDER.OCORDERID
		,ConsultantCode = OCORDER.Consultant
		,SpecialtyCode = OCORDER.Specialty
		,OrderPriorityCode = OCORDER.Priority
		,OrderStatusCode = OCORDER.Status
		,OrderEnteredByCode = OCORDER.EnteredBy
		,OrderedByCode = OCORDER.OrderedBy
		,OrderTime =
				 coalesce(
					cast(OrderDate as varchar)
					,'1 jan 1900'
					) + ' ' + 
				coalesce(
					left(OrderTime, 8)
					,'00:00:00'
					)
		,OrderComment = 
					nullif(
						coalesce(nullif(OCORDER.OrderComment1 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment2 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment3 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment4 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment5 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment6 + char(13), '   '), '')	
					, '')
		,SampleReferenceCode = OCORDER.SampleRef 
		,EffectiveTime = cast(OCORDER.StartDateTime as datetime)
		,LocationCode = OCORDER.PerfLocation
		,ServiceCode = OCORDER.ServiceCode
		,ResultCode = TextResult.ResultCode
		,Result = TextResult.Result
	from
		[$(PAS)].Inquire.OCORDER
	inner join
		(
		select 
			TextResult.OcmTransKey
			,TextResult.SessionNumber
			,TextResult.ResultCode
			,Result = 		
					replace(			
						replace(
							replace(		
								(
								select
									stuff((
										select
											'||' + ltrim(rtrim(TextResultComment.Comment)) 
										from
											[$(PAS)].Inquire.OCMTEXTRESULTS TextResultComment
										where
											TextResultComment.OcmTransKey = TextResult.OcmTransKey
										and	TextResultComment.SessionNumber = TextResult.SessionNumber
										order by
											TextResultComment.AgpSeqNo
										for xml path('') 
										), 1, 2, '')
								) 
								,'&amp;', '&'
							)
							,'&lt;', '<'
						)
						,'&gt;', '>'
					)
		from
			[$(PAS)].Inquire.OCMTEXTRESULTS TextResult
		where
			not exists
					(
					select
						1
					from
						[$(PAS)].Inquire.OCMTEXTRESULTS EarlierTextResult
					where
						EarlierTextResult.OcmTransKey = TextResult.OcmTransKey
					and	EarlierTextResult.SessionNumber < TextResult.SessionNumber
					)
		group by
			TextResult.OcmTransKey
			,TextResult.SessionNumber
			,TextResult.ResultCode
		) TextResult
	on	TextResult.OcmTransKey = OCORDER.OcmTransKey

	where
		cast(OCORDER.StartDateTime as date) between @FromDate and @ToDate

	) TextResult


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿
CREATE procedure [ETL].[LoadStaffingLevelAPCReferenceData]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = object_schema_name(@@procid) + '.' + object_name(@@procid)

select @StartTime = getdate();

select @RowsInserted = 0;


--Load IPR.WardBase
truncate table IPR.WardBase;

insert into IPR.WardBase
(
	WardID
	,WardCode
	,Ward
	,DivisionCode
)	

select 
	WardID = ID
	,WardCode = Value
	,Ward = DisplayValue
	,DivisionCode = LookupFilterValue
	
from
	[$(InfoPathRepository)].InfoPath.LookupMembers
where
	[LookupID] = 410 

select
	@RowsInserted = @RowsInserted + @@rowcount;
	
	
--Load IPR.DivisionBase
truncate table IPR.DivisionBase;

insert into IPR.DivisionBase
(
	DivisionID
	,DivisionCode
	,Division
)		
select 
	DivisionID = ID
	,DivisionCode = Value
	,Division = DisplayValue
 
from
	[$(InfoPathRepository)].InfoPath.LookupMembers
where
	[LookupID] = 400
	
select
	@RowsInserted = @RowsInserted + @@rowcount;
	

--Load IPR.StaffingShiftBase
--truncate table IPR.ShiftBase

insert into IPR.ShiftBase
(
	Shift
	,ShiftType
)		
select distinct
	Shift = FormXML.value ('(//*:Shift/text())[1]','varchar(10)')
	,ShiftType = case when FormXML.value ('(//*:Shift/text())[1]','varchar(10)') in ('Early','Late','Night') then 0 else 1 end
from
	[$(InfoPathRepository)].InfoPath.ReceivedForms
where 
	FormTypeID = 25
and not exists
		(
		select
			1
		from
			IPR.ShiftBase
		where
			Shift = FormXML.value ('(//*:Shift/text())[1]','varchar(10)')
		)

select
	@RowsInserted = @RowsInserted + @@rowcount;
	

select	
	@Elapsed = datediff(minute,@StartTime,getdate());

select @Stats = 
	'Rows Inserted ' + convert(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;


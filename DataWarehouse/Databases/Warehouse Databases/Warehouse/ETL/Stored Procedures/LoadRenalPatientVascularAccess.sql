﻿
CREATE procedure [ETL].[LoadRenalPatientVascularAccess] (@from date = '19000101', @to date = '21000101') as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

declare @MergeSummary TABLE(Action nvarchar(10));

merge Renal.PatientVascularAccess target
using ETL.TLoadRenalPatientVascularAccess source
on source.SourceUniqueID = target.SourceUniqueID
when matched and source.StartDate between @from and @to
and not
(
	isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[StartDate], getdate()) = isnull(source.[StartDate], getdate())
	and isnull(target.[StopDate], getdate()) = isnull(source.[StopDate], getdate())
	and isnull(target.[DaysElapsed], 0) = isnull(source.[DaysElapsed], 0)
	and isnull(target.[AgeAtEvent], 0) = isnull(source.[AgeAtEvent], 0)
	and isnull(target.[TimelineEvent], '') = isnull(source.[TimelineEvent], '')
	and isnull(target.[TimelineEventDetailCode], '') = isnull(source.[TimelineEventDetailCode], '')
	and isnull(target.[InformationSource], '') = isnull(source.[InformationSource], '')
	and isnull(target.[Notes], '') = isnull(source.[Notes], '')
	and isnull(target.[TerminationReason], '') = isnull(source.[TerminationReason], '')
	and isnull(target.[TerminationMethod], '') = isnull(source.[TerminationMethod], '')
	and isnull(target.[DateMatured], getdate()) = isnull(source.[DateMatured], getdate())
	and isnull(target.[IsPrimary], 0) = isnull(source.[IsPrimary], 0)
	and isnull(target.[AccessType], '') = isnull(source.[AccessType], '')
	and isnull(target.[AccessLocation], '') = isnull(source.[AccessLocation], '')
	and isnull(target.[PermanentStatus], '') = isnull(source.[PermanentStatus], '')
	and isnull(target.[Brand], '') = isnull(source.[Brand], '')
	and isnull(target.[CatheterType], '') = isnull(source.[CatheterType], '')
	and isnull(target.[Configuration], '') = isnull(source.[Configuration], '')
	and isnull(target.[Elasticity], '') = isnull(source.[Elasticity], '')
	and isnull(target.[ExternalStatus], '') = isnull(source.[ExternalStatus], '')
	and isnull(target.[NeedleGuage], '') = isnull(source.[NeedleGuage], '')
	and isnull(target.[NeedleLength], '') = isnull(source.[NeedleLength], '')
	and isnull(target.[NeedleType], '') = isnull(source.[NeedleType], '')
	and isnull(target.[NeedleGuage2], '') = isnull(source.[NeedleGuage2], '')
	and isnull(target.[NeedleLength2], '') = isnull(source.[NeedleLength2], '')
	and isnull(target.[NeedleType2], '') = isnull(source.[NeedleType2], '')
	and isnull(target.[VenousLumen], '') = isnull(source.[VenousLumen], '')
	and isnull(target.[ArterialLumen], '') = isnull(source.[ArterialLumen], '')
	and isnull(target.[Preparation], '') = isnull(source.[Preparation], '')
	and isnull(target.[PrimingProcedures], '') = isnull(source.[PrimingProcedures], '')
	and isnull(target.[Surgeon], '') = isnull(source.[Surgeon], '')
	and isnull(target.[TerminationSurgeon], '') = isnull(source.[TerminationSurgeon], '')
	and isnull(target.[Summary], '') = isnull(source.[Summary], '')
	and isnull(target.[SurgicalHistoryObjectID], 0) = isnull(source.[SurgicalHistoryObjectID], 0))
then update set
	target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[StartDate] = source.[StartDate]
	,target.[StopDate] = source.[StopDate]
	,target.[DaysElapsed] = source.[DaysElapsed]
	,target.[AgeAtEvent] = source.[AgeAtEvent]
	,target.[TimelineEvent] = source.[TimelineEvent]
	,target.[TimelineEventDetailCode] = source.[TimelineEventDetailCode]
	,target.[InformationSource] = source.[InformationSource]
	,target.[Notes] = source.[Notes]
	,target.[TerminationReason] = source.[TerminationReason]
	,target.[TerminationMethod] = source.[TerminationMethod]
	,target.[DateMatured] = source.[DateMatured]
	,target.[IsPrimary] = source.[IsPrimary]
	,target.[AccessType] = source.[AccessType]
	,target.[AccessLocation] = source.[AccessLocation]
	,target.[PermanentStatus] = source.[PermanentStatus]
	,target.[Brand] = source.[Brand]
	,target.[CatheterType] = source.[CatheterType]
	,target.[Configuration] = source.[Configuration]
	,target.[Elasticity] = source.[Elasticity]
	,target.[ExternalStatus] = source.[ExternalStatus]
	,target.[NeedleGuage] = source.[NeedleGuage]
	,target.[NeedleLength] = source.[NeedleLength]
	,target.[NeedleType] = source.[NeedleType]
	,target.[NeedleGuage2] = source.[NeedleGuage2]
	,target.[NeedleLength2] = source.[NeedleLength2]
	,target.[NeedleType2] = source.[NeedleType2]
	,target.[VenousLumen] = source.[VenousLumen]
	,target.[ArterialLumen] = source.[ArterialLumen]
	,target.[Preparation] = source.[Preparation]
	,target.[PrimingProcedures] = source.[PrimingProcedures]
	,target.[Surgeon] = source.[Surgeon]
	,target.[TerminationSurgeon] = source.[TerminationSurgeon]
	,target.[Summary] = source.[Summary]
	,target.[SurgicalHistoryObjectID] = source.[SurgicalHistoryObjectID]
	,target.Updated = getdate()
	,target.ByWhom = system_user

when not matched by source then delete

when not matched by target then insert
(
	[SourceUniqueID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[StartDate]
	,[StopDate]
	,[DaysElapsed]
	,[AgeAtEvent]
	,[TimelineEvent]
	,[TimelineEventDetailCode]
	,[InformationSource]
	,[Notes]
	,[TerminationReason]
	,[TerminationMethod]
	,[DateMatured]
	,[IsPrimary]
	,[AccessType]
	,[AccessLocation]
	,[PermanentStatus]
	,[Brand]
	,[CatheterType]
	,[Configuration]
	,[Elasticity]
	,[ExternalStatus]
	,[NeedleGuage]
	,[NeedleLength]
	,[NeedleType]
	,[NeedleGuage2]
	,[NeedleLength2]
	,[NeedleType2]
	,[VenousLumen]
	,[ArterialLumen]
	,[Preparation]
	,[PrimingProcedures]
	,[Surgeon]
	,[TerminationSurgeon]
	,[Summary]
	,[SurgicalHistoryObjectID]	
	,Created
	,Updated
	,ByWhom
)
values
(
	source.[SourceUniqueID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[StartDate]
	,source.[StopDate]
	,source.[DaysElapsed]
	,source.[AgeAtEvent]
	,source.[TimelineEvent]
	,source.[TimelineEventDetailCode]
	,source.[InformationSource]
	,source.[Notes]
	,source.[TerminationReason]
	,source.[TerminationMethod]
	,source.[DateMatured]
	,source.[IsPrimary]
	,source.[AccessType]
	,source.[AccessLocation]
	,source.[PermanentStatus]
	,source.[Brand]
	,source.[CatheterType]
	,source.[Configuration]
	,source.[Elasticity]
	,source.[ExternalStatus]
	,source.[NeedleGuage]
	,source.[NeedleLength]
	,source.[NeedleType]
	,source.[NeedleGuage2]
	,source.[NeedleLength2]
	,source.[NeedleType2]
	,source.[VenousLumen]
	,source.[ArterialLumen]
	,source.[Preparation]
	,source.[PrimingProcedures]
	,source.[Surgeon]
	,source.[TerminationSurgeon]
	,source.[Summary]
	,source.[SurgicalHistoryObjectID]	
	,getdate()
	,getdate()
	,system_user
)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats


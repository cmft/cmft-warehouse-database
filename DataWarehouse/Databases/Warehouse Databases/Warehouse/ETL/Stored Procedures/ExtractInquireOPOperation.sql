﻿CREATE procedure [ETL].[ExtractInquireOPOperation]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'


insert into ETL.TImportOPOperation
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,DoctorCode
	,OPSourceUniqueID
)
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,DoctorCode
	,OPSourceUniqueID
from
	(
	SELECT 
		 OPPROCEDURE.OPPROCEDUREID SourceUniqueID
		,OPPROCEDURE.InternalPatientNumber SourcePatientNo
		,OPPROCEDURE.EpisodeNumber SourceEncounterNo
		,left(OPPROCEDURE.OpAppointmentProcedureSequenceNumber, 2) SequenceNo
		,OPPROCEDURE.Operation OperationCode
		,OPPROCEDURE.AppointmentStartDate OperationDate
		,OPPROCEDURE.ResCode DoctorCode
		,OPA.OPAID OPSourceUniqueID
	FROM
		[$(PAS)].Inquire.OPPROCEDURE

	INNER JOIN [$(PAS)].Inquire.OPA 
	ON	OPA.EpisodeNumber = OPPROCEDURE.EpisodeNumber
	AND	OPA.InternalPatientNumber = OPPROCEDURE.InternalPatientNumber
	AND	OPA.PtApptStartDtimeInt = OPPROCEDURE.PtApptStartDtimeInt
	AND	OPA.ResCode = OPPROCEDURE.ResCode
	and	OPPROCEDURE.OpAppointmentProcedureSequenceNumber > 0

	WHERE
		OPA.PtApptStartDtimeInt between @from and @to
	and	OPA.ApptStatus = 'ATT'
	) OPOperation


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireOPOperation', @Stats, @StartTime

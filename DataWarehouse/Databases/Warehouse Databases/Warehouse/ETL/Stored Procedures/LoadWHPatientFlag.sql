﻿

CREATE procedure ETL.LoadWHPatientFlag

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	WH.PatientFlag target
using
	(
	select	
		DistrictNo
		,FlagCode = 'COPD'
	from
		PAS.Patient
	where
		exists
		(
		select
			1
		from
			(
			select
				DistrictNo
			from
				OP.Encounter
			where 
				ClinicCode in
							(
							 'COPDC'
							,'COPDCH'
							,'COPDCR'
							,'COPDFOL'
							,'COPDGF'
							,'COPDGT'
							,'COPDL'
							,'COPDLS'
							,'COPDMS'
							,'COPDR'
							,'COPDRH'
							,'COPDVC'
							,'COPDVR'
							,'HOMEOX'
							,'HOMEV'
							,'OXYFOL'
							,'OXYLS'
							,'OXYMON'
							,'OXYREV'
							,'PREHAB'
							,'PULASS'
							,'AMBCL'
							)
			union all
					
			select
				DistrictNo
			from 
				AE.Encounter

			inner join AE.Diagnosis
			ON Encounter.SourceUniqueID = Diagnosis.AESourceUniqueID

			where
				SourceDiagnosisCode in 
										(
										 9992
										,9793
										,9642
										,9554
										,8610
										,11785
										,11786
										,11486
										,11487
										,16508
										,8262
										,16929
										)
			union all

			select
				DistrictNo
			from
				APC.Encounter

			inner join APC.Diagnosis
			on Encounter.SourceUniqueID = Diagnosis.APCSourceUniqueID

			where 
				Diagnosis.DiagnosisCode in 
										(
										 'J44.0' 
										,'J44.1' 
										,'J44.8' 
										,'J44.9' 
										,'J43.9'
										)
			) COPD
		where
			COPD.DistrictNo = Patient.DistrictNo
		)
	) source
	on	source.DistrictNo = target.DistrictNo
	and	source.FlagCode = target.FlagCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			DistrictNo
			,FlagCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.DistrictNo
			,source.FlagCode
			,getdate()
			,getdate()
			,system_user
			)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime



﻿
CREATE proc [ETL].[LoadGPICEReferenceData] as

-- Discipline

insert Result.GPICEDisciplineBase
(
DisciplineID
,DisciplineCode
,Discipline
,NationalSpecialtyCode
)

select
	DisciplineID = Discipline_Index
	,DisciplineCode = Discipline_Text
	,Discipline = Discipline_Expansion
	,NationalSpecialtyCode = Specialty_Code
	--,Display
	--,Is_Numeric
	--,Specialty_Text
	--,Report_Header_Text
	--,Use_Report_Filing_Message
	--,Report_Filing_Message
	--,Auto_File_Popup
from
	[$(ICE_GP)].dbo.Service_Discipline_Map
where
	not exists
		(
		select
			1
		from
			Result.GPICEDisciplineBase
		where
			GPICEDisciplineBase.DisciplineID = Service_Discipline_Map.Discipline_Index
		)

-- Provider

insert Result.GPICEProviderBase

(
ProviderID
,ProviderCode
,Provider
,ProviderNationalCode
,DisciplineID
,OrganisationCode
)

select
	ProviderID = Provider_ID
	,ProviderCode = Provider_Code
	,Provider = Provider_Name
	,ProviderNationalCode = nullif(Provider_Nat_Code,'')
	,DisciplineID = Discipline_Index
	,OrganisationCode = organisation
	--,Request_Header_1
	--,Request_Header_2
	--,Request_Copies
	--,Requests_To_Outtray
	--,Requests_To_Barcode
	--,HDR_Request_Logo
	--,HDR_Request_Logo_Text
	--,FTR_Request_Logo
	--,FTR_Request_Logo_Text
	--,Orientation
	--,Label_Labnos
	--,Provider_Printer
	--,Allow_AddOns
	--,Print_2_Up
	--,Print_Lab_Panel
	--,Sample_Panel_ID
	--,Discipline_Index
	--,Queue_Requests
	--,Colour
	--,Specialty
	--,System_Provider_Index
	--,organisation
	--,Show_Telephone_Results
	--,Show_CopyTo_Clinician
	--,Show_CopyTo_Location
	--,Show_Postpone
	--,MaxTests
	--,Print_Option
	--,Show_ICEMail
	--,Queue_RequestsAcceptReject
	--,Queue_RequestsAcceptRejectPrint
	--,Write_Multiple_Reports
from
	[$(ICE_GP)].dbo.Service_Providers
where
	not exists
		(
		select
			1
		from
			Result.GPICEProviderBase
		where
			GPICEProviderBase.ProviderID = Service_Providers.Provider_ID
		)

-- Clinician

insert Result.GPICEClinicianBase
(
ClinicianID
,NationalCode
,Surname
,Forename
,Title
,Active
,OrganisationCode
,NationalSpecialtyCode
,GP
,Created
)

select
	ClinicianID = Clinician_Index 
	,NationalCode = National_Code
	,Surname
	,Forename = nullif(Forename, '')
	--,MiddleNames = nullif(MiddleNames, '')
	,Title = nullif(Title, '')
	,Active
	,OrganisationCode = Org_Code
	,NationalSpecialtyCode = Specialty_Code
	--,Specialty_Description
	--,Date_Added
	,GP = GP_Indicator
	--,Master_Clinician_Index
	--,Master_Record
	,Created = Date_Added
from
	[$(ICE_GP)].dbo.ICEView_ConsultantSummary

where
	not exists
		(
		select
			1
		from
			Result.GPICEClinicianBase
		where
			GPICEClinicianBase.ClinicianID = ICEView_ConsultantSummary.Clinician_Index
		) 


-- Specialty

insert Result.GPICESpecialtyBase
(
SpecialtyCode
,Specialty
)

select
	SpecialtyCode = Specialty_Code
	,Specialty = Specialty
from
	[$(ICE_GP)].dbo.Specialty
where
	not exists
		(
		select
			1
		from
			Result.GPICESpecialtyBase
		where
			GPICESpecialtyBase.SpecialtyCode = Specialty.Specialty_Code
		) 
	

-- Status

insert Result.GPICEOrderStatusBase
(
StatusID
,StatusCode
,Status
)
select
	StatusID = Status_Index
	,StatusCode = Status
	,Status = Description
from
	[$(ICE_GP)].dbo.Service_Requests_Status_Levels
where
	not exists
		(
		select
			1
		from
			Result.GPICEOrderStatusBase
		where
			GPICEOrderStatusBase.StatusID = Service_Requests_Status_Levels.Status_Index
		) 
	
	
-- Priority

insert Result.GPICEPriorityBase
(
PriorityID
,Priority
)

select
	PriorityID = Priority_ID
	,Priority = Priority_Desc
	--,Priority_Urgent
	--,Priority_Default
	--,Priority_Active
	--,Colour_Index
	--,Priority_Value
	--,Organisation
from
	[$(ICE_GP)].dbo.Request_Priority
where
	not exists
		(
		select
			1
		from
			Result.GPICEPriorityBase
		where
			GPICEPriorityBase.PriorityID = Request_Priority.Priority_ID
		) 
	
insert Result.GPICETestBase
 (
TestID
,TestCode
,Test
)

select
	Test_Index
	,Test_Code
	,Screen_Caption
from
	[$(ICE_GP)].dbo.Request_Tests
where
	not exists
		(
		select
			1
		from
			Result.GPICETestBase
		where
			GPICETestBase.TestID = Request_Tests.Test_Index
		);
	
insert Result.GPICEInvestigationBase

(
InvestigationCode
,InvestigationName
,SpecialtyCode
)

select distinct
	Investigation_Code
	,Investigation_Name
	,Specialty
from
	[$(ICE_GP)].dbo.ICEView_ReportInvestigations ReportInvestigations

inner join [$(ICE_GP)].dbo.ICEView_ReportSummary ReportSummary 
on ReportInvestigations.Service_Report_Index = ReportSummary.Service_Report_Index

where
	not exists
		(
		select
			*
		from
			Result.GPICEInvestigationBase
		where
			GPICEInvestigationBase.InvestigationCode = ReportInvestigations.Investigation_Code
		and	GPICEInvestigationBase.InvestigationName = ReportInvestigations.Investigation_Name
		and	GPICEInvestigationBase.SpecialtyCode = ReportSummary.Specialty
		);



insert Result.GPICEResultBase

(
ResultCode
,ResultName
,SpecialtyCode
)

select distinct
	Result_Code
	,Result_Name
	,Specialty
from
	[$(ICE_GP)].dbo.ICEView_ReportInvestigations ReportInvestigations

inner join [$(ICE_GP)].dbo.ICEView_ReportSummary ReportSummary 
on ReportInvestigations.Service_Report_Index = ReportSummary.Service_Report_Index

inner join [$(ICE_GP)].dbo.ICEView_ReportResults ReportResults 
on	ReportResults.Investigation_Index = ReportInvestigations.Investigation_Index 
and ReportResults.Sample_Index = ReportInvestigations.Sample_Index

where
	not exists
		(
		select
			1
		from
			Result.GPICEResultBase
		where
			GPICEResultBase.ResultCode = ReportResults.Result_Code
		and	GPICEResultBase.ResultName = ReportResults.Result_Name
		and	GPICEResultBase.SpecialtyCode = ReportSummary.Specialty
		)

insert Result.GPICELocationBase
(
LocationID
,Location 
,Active
)

select
	LocationID = Location_Index
	,Location = Name
	--,[Type]
	,Active
	--,[Date_Added]
	--,[National_Code]
	--,[Org_Code]
	--,[Specialty_Code]
	--,[Specialty_Description]
	--,[Address_Line1]
	--,[Address_Line2]
	--,[Address_Line3]
	--,[Address_Line4]
	--,[Postcode]
	--,[Telephone]
	--,[Master_Location_Index]
	--,[Master_Record]
	--,[GP_Practice_Ind]
	--,[Site_Code]
from
	[$(ICE_GP)].dbo.ICEView_LocationSummary
where
	not exists
		(
		select
			1
		from
			Result.GPICELocationBase
		where
			GPICELocationBase.LocationID = ICEView_LocationSummary.Location_Index
		)
	
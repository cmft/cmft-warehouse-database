﻿CREATE procedure [ETL].[LoadRenalPatientRenalModality] (@from date = '19000101', @to date = '21000101') as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

declare @MergeSummary TABLE(Action nvarchar(10));

merge
	Renal.PatientRenalModality target

using
	ETL.TLoadRenalPatientRenalModality source

on
	source.SourceUniqueID = target.SourceUniqueID

when
	matched
and source.StartDate between @from and @to
and not
	(
		isnull(target.PatientObjectID, 0) = isnull(source.PatientObjectID, 0)
		and isnull(target.PatientFullName, '') = isnull(source.PatientFullName, '')
		and isnull(target.PatientMedicalRecordNo, '') = isnull(source.PatientMedicalRecordNo, '')
		and isnull(target.StartDate, getdate()) = isnull(source.StartDate, getdate())
		and isnull(target.StopDate, getdate()) = isnull(source.StopDate, getdate())
		and isnull(target.ModalityCode, '') = isnull(source.ModalityCode, '')
		and isnull(target.ModalitySettingCode, '') = isnull(source.ModalitySettingCode, '')
		and isnull(target.DialysisProviderCode, '') = isnull(source.DialysisProviderCode, '')
		and isnull(target.EventDetailCode, '') = isnull(source.EventDetailCode, '')
		and isnull(target.InformationSourceCode, '') = isnull(source.InformationSourceCode, '')
		and isnull(target.ReasonForChangeCode, '') = isnull(source.ReasonForChangeCode, '')
		and isnull(target.Notes, '') = isnull(source.Notes, '')
		and isnull(target.IsPrimary, 0) = isnull(source.IsPrimary, 0)
		and isnull(target.AgeAtEvent, 0) = isnull(source.AgeAtEvent, 0)
	)
then update

	set
		 target.PatientObjectID = source.PatientObjectID
		,target.PatientFullName = source.PatientFullName
		,target.PatientMedicalRecordNo = source.PatientMedicalRecordNo
		,target.StartDate = source.StartDate
		,target.StopDate = source.StopDate
		,target.ModalityCode = source.ModalityCode
		,target.ModalitySettingCode = source.ModalitySettingCode
		,target.DialysisProviderCode = source.DialysisProviderCode
		,target.EventDetailCode = source.EventDetailCode
		,target.InformationSourceCode = source.InformationSourceCode
		,target.ReasonForChangeCode = source.ReasonForChangeCode
		,target.Notes = source.Notes
		,target.IsPrimary = source.IsPrimary
		,target.AgeAtEvent = source.AgeAtEvent
		,target.Updated = getdate()
		,target.ByWhom = system_user

when
	not matched by source

then delete

when
	not matched by target
then
	insert
		(
			 SourceUniqueID
			,PatientObjectID
			,PatientFullName
			,PatientMedicalRecordNo
			,StartDate
			,StopDate
			,ModalityCode
			,ModalitySettingCode
			,DialysisProviderCode
			,EventDetailCode
			,InformationSourceCode
			,ReasonForChangeCode
			,Notes
			,IsPrimary
			,AgeAtEvent
			,Created
			,Updated
			,ByWhom
		)

		values
		(
			 source.SourceUniqueID
			,source.PatientObjectID
			,source.PatientFullName
			,source.PatientMedicalRecordNo
			,source.StartDate
			,source.StopDate
			,source.ModalityCode
			,source.ModalitySettingCode
			,source.DialysisProviderCode
			,source.EventDetailCode
			,source.InformationSourceCode
			,source.ReasonForChangeCode
			,source.Notes
			,source.IsPrimary
			,source.AgeAtEvent
			,getdate()
			,getdate()
			,system_user
		)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats


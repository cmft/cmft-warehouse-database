﻿
CREATE  Procedure [ETL].[LoadGUMSummary] 
AS

	/******************************************************************************
	**  Name: ETL.LoadGUMSummary
	**  Purpose: 
	**
	**  Loading proc for GUM summary data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 17.10.12    MH         Initial coding
	** 08.11.12    MH         Changes to bring Trafford data into the import
	******************************************************************************/

	DECLARE @CensusDate DATETIME
	DECLARE @ClinicCode VARCHAR(10)

	SELECT DISTINCT
		 @CensusDate = CensusDate
		,@ClinicCode	= ClinicCode
	FROM
		ETL.TLoadGUMSummary

	/*
		Delete existing EncounterDate rows
	*/
	DELETE 
		FROM GUM.Summary 
	WHERE 
			CensusDate = @CensusDate
		AND ClinicCode = @ClinicCode

	--Insert into Table
	INSERT INTO GUM.Summary
	(
		CensusDate,
		ClinicCode,
		PCTCode,
		AllAtt,
		FirstAtt,
		FirstAttSeenWithin2Days,
		FirstAttSeenAfter10Days,
		FirstAttUnscheduled,
		FirstAttPatientPreferredClinic,
		FirstApptMissed,
		FirstApptMissedWithin2Days,
		FirstApptOfferedWithin2Days,
		PatientsReportingSymptoms,
		FirstApptSeenAfter2DaysPatientChoice,
		FirstApptSeenAfter2DaysClinicalReason,
		FirstApptSeenAfter2DaysSpecialistClinic,
		PatientPersUnScheduledAttWithin2days,
		PatientPersScheduledAttWithin2days,
		UnScheduledAttResponseToPatientWaitQ,
		ScheduledAttResponseToPatientWaitQ,
		PatientRegNotOfferedAppt
	)
	SELECT
		CensusDate,
		ClinicCode,
		PCTCode,
		AllAtt,
		FirstAtt,
		FirstAttSeenWithin2Days,
		FirstAttSeenAfter10Days,
		FirstAttUnscheduled,
		FirstAttPatientPreferredClinic,
		FirstApptMissed,
		FirstApptMissedWithin2Days,
		FirstApptOfferedWithin2Days,
		PatientsReportingSymptoms,
		FirstApptSeenAfter2DaysPatientChoice,
		FirstApptSeenAfter2DaysClinicalReason,
		FirstApptSeenAfter2DaysSpecialistClinic,
		PatientPersUnScheduledAttWithin2days,
		PatientPersScheduledAttWithin2days,
		UnScheduledAttResponseToPatientWaitQ,
		ScheduledAttResponseToPatientWaitQ,
		PatientRegNotOfferedAppt

	FROM
		ETL.TLoadGUMSummary

	
﻿

CREATE proc [ETL].[LoadInfection]


(
	@FromDate datetime = null
	,@ToDate datetime = null
)

as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime = getdate()
declare @Elapsed int
declare @Stats varchar(255)

select @FromDate = coalesce(@FromDate, (select cast(dateadd(year, -1, getdate()) as date)))
select @ToDate = coalesce(@ToDate,getdate())


exec ETL.LoadInfectionReferenceData

truncate table ETL.TImportInfectionResult
exec ETL.ExtractTelepathInfectionResult @FromDate, @ToDate
exec ETL.LoadInfectionResult


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @ProcedureName
print @Stats
print @StartTime
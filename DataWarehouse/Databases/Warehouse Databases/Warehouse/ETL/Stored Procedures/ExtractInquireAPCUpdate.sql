﻿
CREATE procedure [ETL].[ExtractInquireAPCUpdate]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)


TRUNCATE TABLE ETL.TImportAPCUpdateEncounter;

insert into ETL.TImportAPCUpdateEncounter
(
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime	
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CancelledElectiveAdmissionFlag
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
)

select
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo = SourcePatientNo + '/' + SourceSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,PostCode
	,Address1
	,Address2
	,Address3
	,DhaCode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate =
		case
		when DischargeDate = '//'
		then null
		else DischargeDate
		end
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardType
	,EndSiteCode
	,EndWardType
	,RegisteredGpCode
	,HospitalCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode = rtrim(PatientClassificationCode)
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,SourceAdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit = left(FirstRegDayOrNightAdmit, 1)
	,NeonatalLevelOfCare
	,SourceHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate

	,ClinicalCodingCompleteTime =
		case
		when ClinicalCodingCompleteTime = ':' then null
		--else ClinicalCodingCompleteDate + ' ' + ClinicalCodingCompleteTime
		else ClinicalCodingCompleteTime
		end

	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SourceContractSerialNo
	,CodingCompleteDate
	,SourcePurchaserCode
	,SourceProviderCode

	,EpisodeStartTime =
		case
		when EpisodeStartTime = ':' then null
		--else EpisodeStartDate + ' ' + EpisodeStartTime
		else EpisodeStartTime
		end 

	,EpisodeEndTime =
		case
		when EpisodeEndTime = ':' then null
		--else EpisodeEndDate + ' ' + EpisodeEndTime
		else EpisodeEndTime
		end

	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode = 'INQ' 
	,CasenoteNumber
	,NHSNumberStatusCode

	,AdmissionTime =
		case
		when AdmissionTime = ':' then null
		--else AdmissionDate + ' ' + AdmissionTime
		else AdmissionTime
		end

	,DischargeTime =
		case
		when DischargeTime = ':' then null
		--else DischargeDate + ' ' + DischargeTime
		else DischargeTime
		end

	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLos
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CEA
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode

from
	(
	SELECT 
		 SourceUniqueID = Episode.CONSEPISODEID
		,DistrictNo = P.DistrictNumber 
		,SourcePatientNo = A.InternalPatientNumber 
		,SourceSpellNo = A.EpisodeNumber 
		,SourceEncounterNo = Episode.FCENumber --FCE.FceSequenceNo 
		,PatientTitle = P.Title 
		,PatientForename = P.Forenames 
		,PatientSurname = P.Surname 
		,DateOfBirth = P.PtDoB 
		,DateOfDeath = P.PtDateOfDeath 
		,SexCode = P.Sex 
		,NHSNumber = P.NHSNumber
		,PostCode = P.PtAddrPostCode 
		,Address1 = P.PtAddrLine1 
		,Address2 = P.PtAddrLine2 
		,Address3 = P.PtAddrLine3 
		,DhaCode = P.DistrictOfResidenceCode 
		,EthnicOriginCode = P.EthnicType 
		,MaritalStatusCode = P.MaritalStatus 
		,ReligionCode = P.Religion 
		,DateOnWaitingList = A.WlDateCcyy 
		,AdmissionDate = A.AdmissionDate 
		,DischargeDate = A.DischargeDate 
		,EpisodeEndDate = Episode.EpisodeEndDt --FCE.FCEEndDate 
		,EpisodeEndTime = Episode.EpisodeEndTm --FCE.FCEEndTime
		,EpisodeStartDate = Episode.EpisodeStartDate --FCE.FCEStartDate
		,EpisodeStartTime = Episode.EpisodeStartTime --FCE.FCEStartTime
		,RegisteredGpCode = A.EpiGPCode --FCE.GpCode 
		,HospitalCode = A.HospCode 
		,AdmissionMethodCode = A.MethodOfAdmission 
		,AdmissionSourceCode = A.SourceOfAdm 
		,ManagementIntentionCode = A.IntdMgmt
		,DischargeMethodCode = A.MethodOfDischarge 
		,DischargeDestinationCode = A.DestinationOnDischarge 
		,ConsultantCode = Episode.ConsultantCode --FCE.Consultant 
		,SpecialtyCode = Episode.SpecialtyCode --FCE.Specialty 

		,ClinicalCodingStatus = NULL --left(FCE.ClinicalCodingStatus , 3) 
		,ClinicalCodingCompleteDate = Episode.CodingCompleteDt
		,ClinicalCodingCompleteTime = Episode.CodingCompleteTm
		,PrimaryDiagnosisCode = Episode.KornerEpisodePrimaryDiagnosisCode --FCE.KornerEpisodePrimaryDiagnosisCode 

		,PrimaryDiagnosisDate = Episode.PrimDiagCodeDt 
		
		,SubsidiaryDiagnosisCode = Episode.KornerEpisodeSubsidiaryDiagnosisCode --FCE.Subsid 
		,PrimaryOperationCode = Episode.KornerEpisodePrimaryProcedureCode --FCE.KornerEpisodePrimaryProcedureCode 
		,PrimaryOperationDate = KornerEpisodeProcedureDateInternal --FCE.KornerEpisodePrimaryProcedureDateExternal 
		,TransferFrom = A.TransFrom 
		,RegisteredGdpCode = P.GdpCode 

		,PatientClassificationCode =
			case
			when A.IntdMgmt= 'I' then '1'
			when A.IntdMgmt= 'B' then '1'
			when A.IntdMgmt= 'D' AND A.MethodOfAdmission Not In 
				(
				'WL',
				'EL',
				'BA',
				'BL',
				'BP',
				'PA',
				'PL'
				) then '1'
			when A.IntdMgmt= 'D' AND A.DischargeDate Is Null then '1'
			when A.IntdMgmt= 'D' AND A.AdmissionDate <> A.DischargeDate then '1'
			when A.IntdMgmt= 'D' AND A.AdmissionDate = A.DischargeDate then '2'
			when A.IntdMgmt= 'V' then '1'
			when A.IntdMgmt= 'R' then '3'
			when A.IntdMgmt= 'N' then '4'
			end 

		,EpisodicGpCode = A.EpiGPCode --FCE.EpisodicGP 
		,CasenoteNumber = A.CaseNoteNumber 
		,NHSNumberStatusCode = P.NHSNumberStatus 
		,SourceAdminCategoryCode = A.Category 
		,FirstRegDayOrNightAdmit = A.A1stRegDaynightAdm 

		,AdmissionTime = A.AdmissionTime
		,DischargeTime = A.DischargeTime

		,NationalSpecialtyCode = left(SPEC.DohCode, 4) 
		,NeonatalLevelOfCare = Episode.AdtNeonatalLevelOfCareInt 
		,SourceHRGCode = Episode.HRGCode --FCE.HrgCode 

		,LastEpisodeInSpellIndicator =
			case 
			when Episode.EpsActvDtimeInt = A.IpDschDtimeInt then 'Y' --FCE.EpsActvDtimeInt = A.IpDschDtimeInt then 'Y' 
			else 'N' 
			end 

		,SourceProviderCode = NULL --FCE.ProviderCode 
		,SourcePurchaserCode = NULL --FCE.PurchaserCode 
		,SourceContractSerialNo = NULL --FCE.ContractId 
		,StartWardType = (
							select
								WardStay.WardCode
							from
								[$(PAS)].InquireUpdate.WARDSTAY WardStay
							where
								WardStay.InternalPatientNumber = Episode.InternalPatientNumber
							and	WardStay.EpisodeNumber = Episode.EpisodeNumber
							and WardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
							and	not exists
								(
								select
									1
								from
									[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
								where
								 	WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
								and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
								and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
								and LastWardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
								)
						)  -- FCE.WardCdadmit 
		,EndWardType = 
						(
							select
								WardStay.WardCode
							from
								[$(PAS)].InquireUpdate.WARDSTAY WardStay
							where
								WardStay.InternalPatientNumber = Episode.InternalPatientNumber
							and	WardStay.EpisodeNumber = Episode.EpisodeNumber
							and WardStay.EpsActvDtimeInt < Episode.ConsultantEpisodeEndDttm
							and	not exists
								(
								select
									1
								from
									[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
								where
								 	WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
								and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
								and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
								and LastWardStay.EpsActvDtimeInt < Episode.ConsultantEpisodeEndDttm
								)
						)   --FCE.WardCdend 
						
		,ExpectedLos = A.ExpectedLos
		,MRSAFlag = P.MRSA 

		,RTTPathwayID = NULL --left(PW.PathwayNumber , 25) 
		,RTTPathwayCondition = NULL --PW.PathwayCondition 
		,RTTStartDate = NULL --PWP.RTTStartDate
		,RTTEndDate = NULL --PWP.RTTEndDate
		,RTTSpecialtyCode = NULL --PW.RTTSpeciality 
		,RTTCurrentProviderCode = NULL --PW.RTTCurProv 
		,RTTCurrentStatusCode = NULL --PW.RTTCurrentStatus 
		,RTTCurrentStatusDate = NULL --PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = NULL --PW.RTTPrivatePat 
		,RTTOverseasStatusFlag = NULL --PW.RTTOSVStatus 

		,RTTPeriodStatusCode = A.RttPeriodStatus 
		,IntendedPrimaryOperationCode = NULL --WL.IntendedProc 
		,Operation = NULL --WL.Operation
		,StartSiteCode = 
						(
							select
								WardStay.HospitalCode
							from
								[$(PAS)].InquireUpdate.WARDSTAY WardStay
							where
								WardStay.InternalPatientNumber = Episode.InternalPatientNumber
							and	WardStay.EpisodeNumber = Episode.EpisodeNumber
							and WardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
							and	not exists
								(
								select
									1
								from
									[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
								where
								 	WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
								and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
								and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
								and LastWardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
								)
						)
		,EndSiteCode = 
						(
							select
								WardStay.HospitalCode
							from
								[$(PAS)].InquireUpdate.WARDSTAY WardStay
							where
								WardStay.InternalPatientNumber = Episode.InternalPatientNumber
							and	WardStay.EpisodeNumber = Episode.EpisodeNumber
							and WardStay.EpsActvDtimeInt < Episode.ConsultantEpisodeEndDttm
							and	not exists
								(
								select
									1
								from
									[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
								where
								 	WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
								and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
								and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
								and LastWardStay.EpsActvDtimeInt < Episode.ConsultantEpisodeEndDttm
								)
						)
		,CEA = NULL --WL.CEA
		,CodingCompleteDate = Episode.CodingCompleteDt 
		,Research1 = Episode.Research1
		,Research2 =  Episode.Research2

		,DischargeReadyDate = null
		,DischargeWaitReasonCode = null

		,ReferredByCode = null
		,ReferrerCode = null

	FROM
		[$(PAS)].InquireUpdate.CONSEPISODE Episode

	INNER JOIN [$(PAS)].InquireUpdate.ADMITDISCH A 
	ON	Episode.EpisodeNumber = A.EpisodeNumber
	AND	Episode.InternalPatientNumber = A.InternalPatientNumber

	INNER JOIN [$(PAS)].InquireUpdate.PATDATA P 
	ON	Episode.InternalPatientNumber = P.InternalPatientNumber

	INNER JOIN [$(PAS)].Inquire.SPEC 
	ON	Episode.SpecialtyCode = SPEC.SPECID

	WHERE
		Episode.ConsultantEpisodeEndDttm Is NOT Null
	) Encounter


-- open episodes

union all

select
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo = SourcePatientNo + '/' + SourceSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,PostCode
	,Address1
	,Address2
	,Address3
	,DhaCode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate = null 
	,EpisodeStartDate
	,EpisodeEndDate = null 
	,StartSiteCode
	,StartWardType
	,EndSiteCode
	,EndWardType
	,RegisteredGpCode
	,HospitalCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode = rtrim(PatientClassificationCode) 
	,ManagementIntentionCode
	,DischargeMethodCode = null 
	,DischargeDestinationCode = null 
	,SourceAdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator = null 
	,FirstRegDayOrNightAdmit = left(FirstRegDayOrNightAdmit, 1) 
	,NeonatalLevelOfCare
	,SourceHRGCode
	,ClinicalCodingStatus = null 
	,ClinicalCodingCompleteDate

	,ClinicalCodingCompleteTime =
		case
		when ClinicalCodingCompleteTime = ':' then null
		--else ClinicalCodingCompleteDate + ' ' + ClinicalCodingCompleteTime
		else ClinicalCodingCompleteTime
		end 
	
	,PrimaryDiagnosisCode = null 
	,PrimaryDiagnosisDate = null 
	,SubsidiaryDiagnosisCode = null 
	,PrimaryOperationCode = null 
	,PrimaryOperationDate = null 
	,SourceContractSerialNo = null 
	,CodingCompleteDate
	,SourcePurchaserCode = null 
	,SourceProviderCode = null 

	,EpisodeStartTime = 
		case
		when EpisodeStartTime = ':' then null
		--else EpisodeStartDate + ' ' + EpisodeStartTime
		else EpisodeStartTime
		end 

	,EpisodeEndTime = null 
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode = 'INQ' 
	,CasenoteNumber
	,NHSNumberStatusCode

	,AdmissionTime = 
		case
		when AdmissionTime = ':' then null
		--else AdmissionDate + ' ' + AdmissionTime
		else AdmissionTime
		end 

	,DischargeTime = null 
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS = ExpectedLos
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CEA
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
from
	(
	SELECT  
		 SourceUniqueID = Episode.CONSEPISODEID 
		,DistrictNo = P.DistrictNumber 
		,SourcePatientNo = A.InternalPatientNumber 
		,SourceSpellNo = A.EpisodeNumber 
		,SourceEncounterNo = '999' 
		,PatientTitle = P.Title 
		,PatientForename = P.Forenames 
		,PatientSurname = P.Surname 
		,DateOfBirth = P.PtDoB  
		,DateOfDeath = P.PtDateOfDeath 
		,SexCode = P.Sex 
		,P.NHSNumber
		,PostCode = P.PtAddrPostCode 
		,Address1 = P.PtAddrLine1 
		,Address2 = P.PtAddrLine2 
		,Address3 = P.PtAddrLine3 
		,DhaCode = P.HealthAuthorityCode 
		,EthnicOriginCode = P.EthnicType 
		,MaritalStatusCode = P.MaritalStatus 
		,ReligionCode = P.Religion 
		,DateOnWaitingList = A.WlDateCcyy 
		,AdmissionDate = A.AdmissionDate 
		,Episode.EpisodeStartDate

		,Episode.EpisodeStartTime

		,RegisteredGpCode = P.GpCode 
		,HospitalCode = A.HospCode 
		,AdmissionMethodCode = A.MethodOfAdmission 
		,AdmissionSourceCode = A.SourceOfAdm 
		,ManagementIntentionCode = A.IntdMgmt 
		,DischargeMethodCode = A.MethodOfDischarge 
		,DischargeDestinationCode = A.DestinationOnDischarge 
		,ConsultantCode = Episode.ConsultantCode 
		,SpecialtyCode = Episode.SpecialtyCode
		,TransferFrom = A.TransFrom 
		,RegisteredGdpCode = P.GdpCode 
		,PatientClassificationCode = '1' 
		,CasenoteNumber = A.CaseNoteNumber 
		,NHSNumberStatusCode = P.NHSNumberStatus 
		,SourceAdminCategoryCode = A.Category 
		,FirstRegDayOrNightAdmit = A.A1stRegDaynightAdm 

		,A.AdmissionTime

		,NationalSpecialtyCode = left(SPEC.DohCode, 4) 
		,NeonatalLevelOfCare = Episode.AdtNeonatalLevelOfCareInt 
		,SourceHRGCode = Episode.HRGCode 
		,StartWardType = --A.AdmWard 
						(
							select
								WardStay.WardCode
							from
								[$(PAS)].InquireUpdate.WARDSTAY WardStay
							where
								WardStay.InternalPatientNumber = Episode.InternalPatientNumber
							and	WardStay.EpisodeNumber = Episode.EpisodeNumber
							and WardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
							and	not exists
								(
								select
									1
								from
									[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
								where
								 	WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
								and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
								and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
								and LastWardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
								)
						)  -- FCE.WardCdadmit

		,EndWardType =
			(
			select
				WardStay.WardCode
			from
				[$(PAS)].InquireUpdate.WARDSTAY WardStay
			where
				WardStay.InternalPatientNumber = A.InternalPatientNumber
			and	WardStay.EpisodeNumber = A.EpisodeNumber
			and	not exists
				(
				select
					1
				from
					[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
				where
					WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
				and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
				and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
				)
			) 

		,ExpectedLos = A.ExpectedLos
		,MRSAFlag = P.MRSA 

		,RTTPathwayID = NULL --left(PW.PathwayNumber , 20) 
		,RTTPathwayCondition = NULL --PW.PathwayCondition 
		,RTTStartDate = NULL --PWP.RTTStartDate
		,RTTEndDate = NULL --PWP.RTTEndDate
		,RTTSpecialtyCode = NULL -- PW.RTTSpeciality 
		,RTTCurrentProviderCode = NULL --PW.RTTCurProv 
		,RTTCurrentStatusCode = NULL -- PW.RTTCurrentStatus 
		,RTTCurrentStatusDate = NULL -- PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = NULL -- PW.RTTPrivatePat 
		,RTTOverseasStatusFlag = NULL -- PW.RTTOSVStatus 

		,RTTPeriodStatusCode = A.RttPeriodStatus 
		,IntendedPrimaryOperationCode = NULL -- WL.IntendedProc 
		,Operation = A.Operation --WL.Operation
		,StartSiteCode = --A.HospCode 
						(
							select
								WardStay.HospitalCode
							from
								[$(PAS)].InquireUpdate.WARDSTAY WardStay
							where
								WardStay.InternalPatientNumber = Episode.InternalPatientNumber
							and	WardStay.EpisodeNumber = Episode.EpisodeNumber
							and WardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
							and	not exists
								(
								select
									1
								from
									[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
								where
								 	WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
								and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
								and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
								and LastWardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
								)
						)
		,EndSiteCode = 
					(
						select
							WardStay.HospitalCode
						from
							[$(PAS)].InquireUpdate.WARDSTAY WardStay
						where
							WardStay.InternalPatientNumber = A.InternalPatientNumber
						and	WardStay.EpisodeNumber = A.EpisodeNumber
						and	not exists
							(
							select
								1
							from
								[$(PAS)].InquireUpdate.WARDSTAY LastWardStay
							where
								WardStay.InternalPatientNumber = LastWardStay.InternalPatientNumber
							and	WardStay.EpisodeNumber = LastWardStay.EpisodeNumber
							and	WardStay.EpsActvDtimeInt < LastWardStay.EpsActvDtimeInt
							)
						) 
		,CEA = NULL --WL.CEA
		,CodingCompleteDate = Episode.CodingCompleteDt 
		,ClinicalCodingCompleteDate = Episode.CodingCompleteDt 

		,ClinicalCodingCompleteTime = Episode.CodingCompleteTm

		,Research1 = Episode.Research1
		,Research2 = Episode.Research2

		,DischargeReadyDate = null
		,DischargeWaitReasonCode = null

		,ReferredByCode = null
		,ReferrerCode = null
		,EpisodicGpCode = A.EpiGPCode 

	FROM
		[$(PAS)].InquireUpdate.ADMITDISCH A 

	INNER JOIN [$(PAS)].InquireUpdate.PATDATA P 
	ON	A.InternalPatientNumber = P.InternalPatientNumber

	INNER JOIN [$(PAS)].InquireUpdate.CONSEPISODE  Episode 
	ON	A.EpisodeNumber = Episode.EpisodeNumber
	AND	A.InternalPatientNumber = Episode.InternalPatientNumber

	LEFT JOIN [$(PAS)].Inquire.SPEC 
	ON	A.Specialty = SPEC.SPECID

	WHERE
		Episode.EpsActvDtimeInt <= convert(varchar, dateadd(day, datediff(day, 0, coalesce(getdate(), dateadd(day, -1, getdate()))), 0), 112) + '2400'
	AND	(
			Episode.ConsultantEpisodeEndDttm > convert(varchar, dateadd(day, datediff(day, 0, coalesce(getdate(), dateadd(day, -1, getdate()))), 0), 112) + '2400'
		or	Episode.ConsultantEpisodeEndDttm Is Null
		)
	) Encounter

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireUpdateAPC', @Stats, @StartTime


﻿


CREATE proc [ETL].[LoadPEXKioskOnlinePostcardTabletReferenceData]

as

truncate table PEX.KioskOnlinePostcardTabletLocation

insert into PEX.KioskOnlinePostcardTabletLocation

select
	LocationID = EntityXref.EntityXrefRecno --Recno used because location reference will be a combination of locations and selected location
	,Location = EntityLookup.Description
	,LocationTypeID = EntityXref.XrefEntityCode					
from
	dbo.EntityXref
	
inner join dbo.EntityLookup
on	EntityLookup.EntityTypeCode = EntityXref.EntityTypeCode
and	EntityLookup.EntityCode = EntityXref.EntityCode
and	EntityLookup.EntityTypeCode in ('PEXLOCATIONSELECTED','PEXLOCATION')




truncate table PEX.KioskOnlinePostcardTabletAnswer

insert into PEX.KioskOnlinePostcardTabletAnswer

select
	AnswerID = ID
	,Answer = max(AnswerText) --Required as duplicate -1
from
	[$(PatientExperience)].PEX.Answers
	
where
	ID <> 0
	
group by
	ID





truncate table PEX.KioskOnlinePostcardTabletQuestion

insert into PEX.KioskOnlinePostcardTabletQuestion

select
	QuestionID = Questions.ID
	,Questions.Question
	,QuestionTypeID = coalesce(XrefEntityCode, 0)
from
	[$(PatientExperience)].PEX.Questions

left join dbo.EntityXref QuestionType
on	Questions.ID = QuestionType.EntityCode
and QuestionType.EntityTypeCode in ('PEXQUESTION')
	
left join dbo.EntityLookup
on	EntityLookup.EntityTypeCode = QuestionType.EntityTypeCode
and	EntityLookup.EntityCode = QuestionType.EntityCode
and EntityLookup.EntityTypeCode in ('PEXQUESTION')



truncate table PEX.KioskOnlinePostcardTabletSurvey

insert into PEX.KioskOnlinePostcardTabletSurvey

select
	SurveyID = Surveys.ID
	,Survey = SurveyName
	,ChannelID = EntityXref.XrefEntityCode					
from
	[$(PatientExperience)].PEX.Surveys

left join dbo.EntityXref 
on	Surveys.ID = EntityXref.EntityCode
and EntityXref.EntityTypeCode in ('PEXSURVEY')
	
left join dbo.EntityLookup
on	EntityLookup.EntityTypeCode = EntityXref.EntityTypeCode
and	EntityLookup.EntityCode = EntityXref.EntityCode
and EntityLookup.EntityTypeCode in ('PEXSURVEY')




﻿

CREATE PROCEDURE [ETL].[LoadResultCENICETest]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, OrderRequestTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, OrderRequestTime)) 
from
	ETL.TLoadResultCENICETest

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Result.CENICETest target
using
	(
	select
		 SourceUniqueID
		,OrderSourceUniqueID
		,OrderRequestTime
		,OrderStatusCode
		,PatientID
		,PatientIdentifier
		,DistrictNo
		,CasenoteNumber
		,NHSNumber 
		,ClinicianID
		,ProviderID
		,LocationID
		,TestStatus
		,TestID
		,InterfaceCode
		,TestChecksum = 
		checksum(
			 SourceUniqueID
			,OrderSourceUniqueID
			,OrderRequestTime
			,OrderStatusCode
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber 
			,ClinicianID
			,ProviderID
			,LocationID
			,TestStatus
			,TestID
			,InterfaceCode
		)				
	from
		ETL.TLoadResultCENICETest
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.OrderRequestTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,OrderSourceUniqueID
			,OrderRequestTime
			,OrderStatusCode
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber 
			,ClinicianID
			,ProviderID
			,LocationID
			,TestStatus
			,TestID
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,TestChecksum
			)
		values
			(
			 source.SourceUniqueID
			,source.OrderSourceUniqueID
			,source.OrderRequestTime
			,source.OrderStatusCode
			,source.PatientID
			,source.PatientIdentifier
			,source.DistrictNo
			,source.CasenoteNumber
			,source.NHSNumber 
			,source.ClinicianID
			,source.ProviderID
			,source.LocationID
			,source.TestStatus
			,source.TestID
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.TestChecksum
			)

	when matched
	and	target.TestChecksum <> source.TestChecksum
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.OrderSourceUniqueID = source.OrderSourceUniqueID
			,target.OrderRequestTime = source.OrderRequestTime
			,target.OrderStatusCode = source.OrderStatusCode
			,target.PatientID = source.PatientID
			,target.PatientIdentifier = source.PatientIdentifier
			,target.DistrictNo = source.DistrictNo
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.ClinicianID = source.ClinicianID
			,target.ProviderID = source.ProviderID
			,target.LocationID = source.LocationID
			,target.TestStatus = source.TestStatus
			,target.TestID = source.TestID
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.TestChecksum = source.TestChecksum

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

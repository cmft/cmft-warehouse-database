﻿
Create proc [ETL].[LoadACUReferenceData]

as




Insert into ACU.TreatmentTypeBase
(
	TreatmentTypeCode
	,TreatmentType
)
select distinct
	TreatmentTypeCode = IVF.TreatmentType
	,TreatmentType =
		case
			when IVF.TreatmentType = 'ICSI' then 'Intracytoplasmic sperm injection'
			when IVF.TreatmentType = 'IVF' then 'In vitro fertilisation'
			when IVF.TreatmentType = 'GIFT' then 'Gamete intra fallopian transfer'
			when IVF.TreatmentType = 'FET' then 'Frozen embryo transfer'
			when IVF.TreatmentType = 'Egg Storage' then 'Egg storage'
			when IVF.TreatmentType = 'OI' then 'Ovulation induction'
			when IVF.TreatmentType = 'IUI' then 'Intrauterine insemenation'
			when IVF.TreatmentType = 'SIUI' then 'Stimulated intrauterine insemenation'
			else IVF.TreatmentType
		end	
from
	[$(ACU)].ACUBASE.IVF
where 
	TreatmentType is not null
and not exists
	(
	Select 
		1
	from 
		ACU.TreatmentTypeBase Present
	where
		Present.TreatmentTypeCode = IVF.TreatmentType
	)
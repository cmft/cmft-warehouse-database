﻿






CREATE procedure [ETL].[ExtractInquireAPC]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

--select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)



update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSAPCENCOUNTER'


insert into ETL.TImportAPCEncounter
(
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,PASHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime	
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CancelledElectiveAdmissionFlag
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
	,CarerSupportIndicator
	,PseudoPostCode
	,EpisodicPostcode
)

select
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo = SourcePatientNo + '/' + SourceSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,PostCode
	,Address1
	,Address2
	,Address3
	,DhaCode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate =
		case
		when DischargeDate = '//'
		then null
		else DischargeDate
		end
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardType
	,EndSiteCode
	,EndWardType
	,RegisteredGpCode
	,HospitalCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode = rtrim(PatientClassificationCode)
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,SourceAdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit = left(FirstRegDayOrNightAdmit, 1)
	,NeonatalLevelOfCare
	,SourceHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate

	,ClinicalCodingCompleteTime =
		case
		when ClinicalCodingCompleteTime = ':' then null
		--else ClinicalCodingCompleteDate + ' ' + ClinicalCodingCompleteTime
		else ClinicalCodingCompleteTime
		end

	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SourceContractSerialNo
	,CodingCompleteDate
	,SourcePurchaserCode
	,SourceProviderCode

	,EpisodeStartTime =
		case
		when EpisodeStartTime = ':' then null
		--else EpisodeStartDate + ' ' + EpisodeStartTime
		else EpisodeStartTime
		end 

	,EpisodeEndTime =
		case
		when EpisodeEndTime = ':' then null
		--else EpisodeEndDate + ' ' + EpisodeEndTime
		else EpisodeEndTime
		end

	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode = 'INQ' 
	,CasenoteNumber
	,NHSNumberStatusCode

	,AdmissionTime =
		case
		when AdmissionTime = ':' then null
		--else AdmissionDate + ' ' + AdmissionTime
		else AdmissionTime
		end

	,DischargeTime =
		case
		when DischargeTime = ':' then null
		--else DischargeDate + ' ' + DischargeTime
		else DischargeTime
		end

	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CEA
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
	,CarerSupportIndicator
	,PseudoPostcode
	,EpisodicPostcode 

from
	(
	SELECT 
		 SourceUniqueID = FCE.FCEEXTID
		,DistrictNo = P.DistrictNumber 
		,SourcePatientNo = FCE.InternalPatientNumber 
		,SourceSpellNo = FCE.EpisodeNumber 
		,SourceEncounterNo = FCE.FceSequenceNo 
		,PatientTitle = P.Title 
		,PatientForename = P.Forenames 
		,PatientSurname = P.Surname 
		,DateOfBirth = P.PtDoB 
		,DateOfDeath = P.PtDateOfDeath 
		,SexCode = P.Sex 
		,P.NHSNumber
		,PostCode = P.PtAddrPostCode 
		,Address1 = P.PtAddrLine1 
		,Address2 = P.PtAddrLine2 
		,Address3 = P.PtAddrLine3 
		,DhaCode = P.DistrictOfResidenceCode 
		,EthnicOriginCode = P.EthnicType 
		,MaritalStatusCode = P.MaritalStatus 
		,ReligionCode = P.Religion 
		,DateOnWaitingList = A.WlDateCcyy 
		,AdmissionDate = A.AdmissionDate 
		,DischargeDate = A.DischargeDate 
		,EpisodeEndDate = FCE.FCEEndDate 
		,EpisodeEndTime = FCE.FCEEndTime
		,EpisodeStartDate = FCE.FCEStartDate
		,EpisodeStartTime = FCE.FCEStartTime
		,RegisteredGpCode = FCE.GpCode 
		,HospitalCode = A.HospCode 
		,AdmissionMethodCode = A.MethodOfAdmission 
		,AdmissionSourceCode = A.SourceOfAdm 
		,ManagementIntentionCode = A.IntdMgmt
		,DischargeMethodCode = A.MethodOfDischarge 
		,DischargeDestinationCode = A.DestinationOnDischarge 
		,ConsultantCode = FCE.Consultant 
		,SpecialtyCode = FCE.Specialty 

		,ClinicalCodingStatus = left(FCE.ClinicalCodingStatus , 3) 
		,ClinicalCodingCompleteDate = Episode.CodingCompleteDt 
		,ClinicalCodingCompleteTime = Episode.CodingCompleteTm
		,PrimaryDiagnosisCode = FCE.KornerEpisodePrimaryDiagnosisCode 

		,PrimaryDiagnosisDate = Episode.PrimDiagCodeDt 
		
		,SubsidiaryDiagnosisCode = FCE.Subsid 
		,PrimaryOperationCode = FCE.KornerEpisodePrimaryProcedureCode 
		,PrimaryOperationDate = FCE.KornerEpisodePrimaryProcedureDateExternal 
		,TransferFrom = A.TransFrom 
		,RegisteredGdpCode = P.GdpCode 

		,PatientClassificationCode =
			case
			when FCE.IntdMgmt= 'I' then '1'
			when FCE.IntdMgmt= 'B' then '1'
			when FCE.IntdMgmt= 'D' AND FCE.MethodOfAdmission Not In 
				(
				'WL',
				'EL',
				'BA',
				'BL',
				'BP',
				'PA',
				'PL'
				) then '1'
			when FCE.IntdMgmt= 'D' AND A.DischargeDate Is Null then '1'
			when FCE.IntdMgmt= 'D' AND A.AdmissionDate <> A.DischargeDate then '1'
			when FCE.IntdMgmt= 'D' AND A.AdmissionDate = A.DischargeDate then '2'
			when FCE.IntdMgmt= 'V' then '1'
			--when FCE.IntdMgmt= 'R' then '3' -- DG 02/04/2012 - Now handles LoS > 24 hours conversion to ordinary admission
			when FCE.IntdMgmt = 'R' AND A.DischargeDate Is Null then '1'
			when FCE.IntdMgmt = 'R' AND A.AdmissionDate <> A.DischargeDate then '1'
			when FCE.IntdMgmt = 'R' AND A.AdmissionDate = A.DischargeDate then '3'
			--when FCE.IntdMgmt= 'N' then '4' -- DG 02/04/2012 - Now handles LoS > 24 hours conversion to ordinary admission - have to use hours as admission date and discharge date may differ
			when FCE.IntdMgmt = 'N' then
				case									
				when datediff(
						 hour
						,cast(
							case
                            when A.AdmissionTime = '24:00'
                                      then dateadd(minute , 1 , convert(smalldatetime , A.AdmissionDate + ' 23:59'))
							
							when A.AdmissionTime = ':' then null
							else A.AdmissionDate + ' ' + A.AdmissionTime
							end 
							as datetime
						)
						,cast(
							case
                            when A.DischargeTime = '24:00'
                                      then dateadd(minute , 1 , convert(smalldatetime , A.DischargeDate + ' 23:59'))
							
							when A.DischargeTime = ':' then null
							else A.DischargeDate + ' ' + A.DischargeTime
							end
							as datetime
						)
					) >= 24	then '1'
				else '4'
				end
			end 

		,EpisodicGpCode = FCE.EpisodicGP 
		,CasenoteNumber = A.CaseNoteNumber 
		,NHSNumberStatusCode = P.NHSNumberStatus 
		,SourceAdminCategoryCode = A.Category 
		,FirstRegDayOrNightAdmit = A.A1stRegDaynightAdm 

		,A.AdmissionTime
		,A.DischargeTime

		,NationalSpecialtyCode = left(SPEC.DohCode, 4) 
		,NeonatalLevelOfCare = Episode.AdtNeonatalLevelOfCareInt 
		,SourceHRGCode = FCE.HrgCode 

		,LastEpisodeInSpellIndicator =
			case 
			when FCE.EpsActvDtimeInt = A.IpDschDtimeInt then 'Y' 
			else 'N' 
			end 

		,SourceProviderCode = FCE.ProviderCode 
		,SourcePurchaserCode = FCE.PurchaserCode 
		,SourceContractSerialNo = FCE.ContractId 
		,StartWardType = EpisodeStartWardStay.WardCode
		,EndWardType = FCE.WardCdend 
		,ExpectedLOS = A.ExpectedLos
		,MRSAFlag = P.MRSA 

		,RTTPathwayID = left(PW.PathwayNumber , 25) 
		,RTTPathwayCondition = PW.PathwayCondition 
		,RTTStartDate = PWP.RttStartDate
		,RTTEndDate = PWP.RttEndDate
		,RTTSpecialtyCode = PW.RttSpeciality 
		,RTTCurrentProviderCode = PW.RttCurProv 
		,RTTCurrentStatusCode = PW.RttCurrentStatus 
		,RTTCurrentStatusDate = PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
		,RTTOverseasStatusFlag = PW.RttOsvStatus 

		,RTTPeriodStatusCode = A.RttPeriodStatus 
		,IntendedPrimaryOperationCode = WL.IntendedProc 
		,WL.Operation
		,StartSiteCode = EpisodeStartWardStay.HospitalCode -- A.HospCode
		,EndSiteCode = FCE.HospCdend -- Ward.HospitalCode

		,WL.CEA
		,CodingCompleteDate = Episode.CodingCompleteDt 
		,Episode.Research1
		,Episode.Research2

		,DischargeReadyDate = null
		,DischargeWaitReasonCode = null

		,ReferredByCode = EpiReg.ReferredBy
		,ReferrerCode = 
			case
			when EpiReg.ReferredBy = 'GP' then EpiReg.EpiGP
			when EpiReg.ReferredBy = 'GDP' then EpiReg.EpiGDP
			when EpiReg.ReferredBy = 'CON' then EpiReg.Consultant
			else null
			end
		,CarerSupportIndicator = P.CarerSupport
		,PseudoPostcode = P.PtPseudoPostCode
		,EpisodicPostcode = EpiReg.Epipc

	FROM
		[$(PAS)].Inquire.FCEEXT FCE

	INNER JOIN [$(PAS)].Inquire.ADMITDISCH A 
	ON	FCE.EpisodeNumber = A.EpisodeNumber
	AND	FCE.InternalPatientNumber = A.InternalPatientNumber

	INNER JOIN [$(PAS)].Inquire.PATDATA P 
	ON	FCE.InternalPatientNumber = P.InternalPatientNumber

	INNER JOIN [$(PAS)].Inquire.CONSEPISODE Episode 
	ON	FCE.FCEStartDTimeInt = Episode.EpsActvDtimeInt
	AND	FCE.EpisodeNumber = Episode.EpisodeNumber
	AND	FCE.InternalPatientNumber = Episode.InternalPatientNumber

	INNER JOIN [$(PAS)].Inquire.SPEC 
	ON	FCE.Specialty = SPEC.SPECID

	left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
	on	EPW.InternalPatientNumber = A.InternalPatientNumber
	and	EPW.EpisodeNumber = A.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTPATHWAY PW
	on	PW.InternalPatientNumber = EPW.InternalPatientNumber
	and	PW.PathwayNumber = EPW.PathwayNumber

	left join [$(PAS)].Inquire.RTTPERIOD PWP
	on	PWP.InternalPatientNumber = PW.InternalPatientNumber
	and	PWP.PathwayNumber = PW.PathwayNumber
	and	PWP.PPeriodId = '0'

	left join [$(PAS)].Inquire.WLENTRY WL
	on	WL.InternalPatientNumber = FCE.InternalPatientNumber
	and	WL.EpisodeNumber = FCE.EpisodeNumber
	
	LEFT JOIN [$(PAS)].Inquire.EPIREGISTRATION EpiReg
	ON	FCE.EpisodeNumber = EpiReg.EpisodeNumber
	AND	FCE.InternalPatientNumber =  EpiReg.InternalPatientNumber

	left join [$(PAS)].Inquire.WARD Ward
	on	Ward.WARDID = A.DischWard

	left join [$(PAS)].Inquire.WARDSTAY EpisodeStartWardStay
	on	EpisodeStartWardStay.InternalPatientNumber = FCE.InternalPatientNumber
	and	EpisodeStartWardStay.EpisodeNumber = FCE.EpisodeNumber
	and	EpisodeStartWardStay.EpsActvDtimeInt <= FCE.FCEStartDTimeInt
	and	not exists
		(
		select
			1
		from
			[$(PAS)].Inquire.WARDSTAY Previous
		where
			Previous.InternalPatientNumber = FCE.InternalPatientNumber
		and	Previous.EpisodeNumber = FCE.EpisodeNumber
		and	Previous.EpsActvDtimeInt <= FCE.FCEStartDTimeInt
		and	(
				Previous.EpsActvDtimeInt > EpisodeStartWardStay.EpsActvDtimeInt
			or	(
					Previous.EpsActvDtimeInt = EpisodeStartWardStay.EpsActvDtimeInt
				and	Previous.WARDSTAYID > EpisodeStartWardStay.WARDSTAYID
				)
			)
		)

	WHERE
		FCE.EpsActvDtimeInt between @from and @to
	) Encounter


-- open episodes

union all

select
	 SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo = SourcePatientNo + '/' + SourceSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,PostCode
	,Address1
	,Address2
	,Address3
	,DhaCode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate = null 
	,EpisodeStartDate
	,EpisodeEndDate = null 
	,StartSiteCode
	,StartWardType
	,EndSiteCode
	,EndWardType
	,RegisteredGpCode
	,HospitalCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode = rtrim(PatientClassificationCode) 
	,ManagementIntentionCode
	,DischargeMethodCode = null 
	,DischargeDestinationCode = null 
	,SourceAdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator = null 
	,FirstRegDayOrNightAdmit = left(FirstRegDayOrNightAdmit, 1) 
	,NeonatalLevelOfCare
	,SourceHRGCode
	,ClinicalCodingStatus = null 
	,ClinicalCodingCompleteDate

	,ClinicalCodingCompleteTime =
		case
		when ClinicalCodingCompleteTime = ':' then null
		--else ClinicalCodingCompleteDate + ' ' + ClinicalCodingCompleteTime
		else ClinicalCodingCompleteTime
		end 
	
	,PrimaryDiagnosisCode = null 
	,PrimaryDiagnosisDate = null 
	,SubsidiaryDiagnosisCode = null 
	,PrimaryOperationCode = null 
	,PrimaryOperationDate = null 
	,SourceContractSerialNo = null 
	,CodingCompleteDate
	,SourcePurchaserCode = null 
	,SourceProviderCode = null 

	,EpisodeStartTime = 
		case
		when EpisodeStartTime = ':' then null
		--else EpisodeStartDate + ' ' + EpisodeStartTime
		else EpisodeStartTime
		end 

	,EpisodeEndTime = null 
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode = 'INQ' 
	,CasenoteNumber
	,NHSNumberStatusCode

	,AdmissionTime = 
		case
		when AdmissionTime = ':' then null
		--else AdmissionDate + ' ' + AdmissionTime
		else AdmissionTime
		end 

	,DischargeTime = null 
	,TransferFrom
	,DistrictNo
	,SourceUniqueID
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CEA
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
	,CarerSupportIndicator
	,PseudoPostcode
	,EpisodicPostcode 
from
	(
	SELECT
		 SourceUniqueID = Episode.CONSEPISODEID 
		,DistrictNo = P.DistrictNumber 
		,SourcePatientNo = A.InternalPatientNumber 
		,SourceSpellNo = A.EpisodeNumber 
		,SourceEncounterNo = '999' 
		,PatientTitle = P.Title 
		,PatientForename = P.Forenames 
		,PatientSurname = P.Surname 
		,DateOfBirth = P.PtDoB  
		,DateOfDeath = P.PtDateOfDeath 
		,SexCode = P.Sex 
		,P.NHSNumber
		,PostCode = P.PtAddrPostCode 
		,Address1 = P.PtAddrLine1 
		,Address2 = P.PtAddrLine2 
		,Address3 = P.PtAddrLine3 
		,DhaCode = P.HealthAuthorityCode 
		,EthnicOriginCode = P.EthnicType 
		,MaritalStatusCode = P.MaritalStatus 
		,ReligionCode = P.Religion 
		,DateOnWaitingList = A.WlDateCcyy 
		,AdmissionDate = A.AdmissionDate 
		,Episode.EpisodeStartDate

		,Episode.EpisodeStartTime

		,RegisteredGpCode = P.GpCode 
		,HospitalCode = A.HospCode 
		,AdmissionMethodCode = A.MethodOfAdmission 
		,AdmissionSourceCode = A.SourceOfAdm 
		,ManagementIntentionCode = A.IntdMgmt 
		,DischargeMethodCode = A.MethodOfDischarge 
		,DischargeDestinationCode = A.DestinationOnDischarge 
		,ConsultantCode = Episode.ConsultantCode
		,SpecialtyCode = Episode.SpecialtyCode
		,TransferFrom = A.TransFrom 
		,RegisteredGdpCode = P.GdpCode 
		,PatientClassificationCode = '1' 
		,CasenoteNumber = A.CaseNoteNumber 
		,NHSNumberStatusCode = P.NHSNumberStatus 
		,SourceAdminCategoryCode = A.Category 
		,FirstRegDayOrNightAdmit = A.A1stRegDaynightAdm 

		,A.AdmissionTime

		,NationalSpecialtyCode = left(SPEC.DohCode, 4) 
		,NeonatalLevelOfCare = Episode.AdtNeonatalLevelOfCareInt 
		,SourceHRGCode = Episode.HRGCode 
		,StartWardType = EpisodeStartWardStay.WardCode 

		,EndWardType = null --open spell so no discharge ward

		,ExpectedLOS = A.ExpectedLos
		,MRSAFlag = P.MRSA 

		,RTTPathwayID = left(PW.PathwayNumber , 20) 
		,RTTPathwayCondition = PW.PathwayCondition 
		,RTTStartDate = PWP.RttStartDate
		,RTTEndDate = PWP.RttEndDate
		,RTTSpecialtyCode = PW.RttSpeciality 
		,RTTCurrentProviderCode = PW.RttCurProv 
		,RTTCurrentStatusCode = PW.RttCurrentStatus 
		,RTTCurrentStatusDate = PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
		,RTTOverseasStatusFlag = PW.RttOsvStatus 

		,RTTPeriodStatusCode = A.RttPeriodStatus 
		,IntendedPrimaryOperationCode = WL.IntendedProc 
		,WL.Operation
		,StartSiteCode = EpisodeStartWardStay.HospitalCode -- A.HospCode 
		,EndSiteCode = null --open spell so no discharge site
		,WL.CEA
		,CodingCompleteDate = Episode.CodingCompleteDt 
		,ClinicalCodingCompleteDate = Episode.CodingCompleteDt 

		,ClinicalCodingCompleteTime = Episode.CodingCompleteTm

		,Episode.Research1
		,Episode.Research2

		,DischargeReadyDate = null
		,DischargeWaitReasonCode = null

		,ReferredByCode = EpiReg.ReferredBy
		,ReferrerCode = 
			case
			when EpiReg.ReferredBy = 'GP' then EpiReg.EpiGP
			when EpiReg.ReferredBy = 'GDP' then EpiReg.EpiGDP
			when EpiReg.ReferredBy = 'CON' then EpiReg.Consultant
			else null
			end
		,EpisodicGpCode = A.EpiGPCode 
		,CarerSupportIndicator = P.CarerSupport
		,PseudoPostcode = P.PtPseudoPostCode
		,EpisodicPostcode = EpiReg.Epipc

	FROM
		[$(PAS)].Inquire.CONSEPISODE  Episode

	INNER JOIN [$(PAS)].Inquire.PATDATA P 
	ON	Episode.InternalPatientNumber = P.InternalPatientNumber

	INNER JOIN [$(PAS)].Inquire.ADMITDISCH A 
	ON	Episode.EpisodeNumber = A.EpisodeNumber
	AND	Episode.InternalPatientNumber =  A.InternalPatientNumber

	LEFT JOIN [$(PAS)].Inquire.SPEC 
	ON	Episode.SpecialtyCode = SPEC.SPECID

	left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
	on	EPW.InternalPatientNumber = A.InternalPatientNumber
	and	EPW.EpisodeNumber = A.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTPATHWAY PW
	on	PW.InternalPatientNumber = EPW.InternalPatientNumber
	and	PW.PathwayNumber = EPW.PathwayNumber

	left join [$(PAS)].Inquire.RTTPERIOD PWP
	on	PWP.InternalPatientNumber = PW.InternalPatientNumber
	and	PWP.PathwayNumber = PW.PathwayNumber
	and	PWP.PPeriodId = '0'

	left join [$(PAS)].Inquire.WLENTRY WL
	on	WL.InternalPatientNumber = Episode.InternalPatientNumber
	and	WL.EpisodeNumber = Episode.EpisodeNumber
	
	LEFT JOIN [$(PAS)].Inquire.EPIREGISTRATION EpiReg
	ON	Episode.EpisodeNumber = EpiReg.EpisodeNumber
	AND	Episode.InternalPatientNumber =  EpiReg.InternalPatientNumber

	left join [$(PAS)].Inquire.WARDSTAY EpisodeStartWardStay
	on	EpisodeStartWardStay.InternalPatientNumber = Episode.InternalPatientNumber
	and	EpisodeStartWardStay.EpisodeNumber = Episode.EpisodeNumber
	and	EpisodeStartWardStay.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
	and	not exists
		(
		select
			1
		from
			[$(PAS)].Inquire.WARDSTAY Previous
		where
			Previous.InternalPatientNumber = Episode.InternalPatientNumber
		and	Previous.EpisodeNumber = Episode.EpisodeNumber
		and	Previous.EpsActvDtimeInt <= Episode.EpsActvDtimeInt
		and	(
				Previous.EpsActvDtimeInt > EpisodeStartWardStay.EpsActvDtimeInt
			or	(
					Previous.EpsActvDtimeInt = EpisodeStartWardStay.EpsActvDtimeInt
				and	Previous.WARDSTAYID > EpisodeStartWardStay.WARDSTAYID
				)
			)
		)


	where
		Episode.EpsActvDtimeInt <= @to
	AND	(
			Episode.ConsultantEpisodeEndDttm > @to
		or	Episode.ConsultantEpisodeEndDttm Is Null
		)

--PDO 7 Nov 2012
--Long open episode - Tim informed
	and	Episode.CONSEPISODEID <> '1091701||17||199902250900'

	) Encounter

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

if @RowsInserted != 0
begin
	update Utility.Parameter set NumericValue = 1 where Parameter = 'EXTRACTSTATUSAPCENCOUNTER'
end

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireAPC', @Stats, @StartTime








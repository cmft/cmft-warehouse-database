﻿



CREATE PROCEDURE [ETL].[ExtractBedmanPdd]

as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

TRUNCATE TABLE ETL.TImportBedmanPdd

INSERT INTO ETL.TImportBedmanPdd
(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,CasenoteNumber
	,AdmissionDate
	,AdmissionTime
	,WardTypeCode
	,DischargeDate
	,DischargeTime
	,SpecialtyCode
	,ConsultantCode
	,InterfaceCode
	,PddSequence
	,Pdd
	,PddComment
	,PddEntered
	,PddReason
	,PddPasFlag
	,PddEnteredWithinTarget
	,ExpectedLOS
	,Created
	,Updated
	,ByWhom
)
SELECT
	 EncounterRecno = PDDHistory.PDDHistoryID
	,SourceUniqueID = convert(varchar(max),Spell.SpellID) + '||' + convert(varchar(max),PDDHistory.PDDHistoryID)
	,SourcePatientNo = Spell.Patient
	,SourceSpellNo =SUBSTRING(Spell.IPEpisode, PATINDEX('%[^0]%', Spell.IPEpisode+'.'), LEN(Spell.IPEpisode))
	,ProviderSpellNo = Spell.Patient + '/' + REPLACE(LTRIM(REPLACE(Spell.IPEpisode, '0', ' ')), ' ', '0') 
	,CasenoteNumber = Spell.HospitalNo
	,AdmissionDate = CONVERT(DATE,Spell.AdmitDate)
	,AdmissionTime = Spell.AdmitDate
	,WardTypeCode = Spell.CurrWard
	,DischargeDate = CONVERT(DATE,Spell.DischTime)
	,DischargeTime = Spell.DischTime
	,SpecialtyCode = Spell.CurrSpec
	,ConsultantCode = Spell.CurrCon
	,InterfaceCode = 'Bedman'
	,PddSequence =	ROW_NUMBER() OVER(PARTITION BY PDDHistory.SpellID ORDER BY PDDHistory.PDDEntered)
	,Pdd = PDDHistory.PDD
	,PddComment = PDDHistory.PDDComment
	,PddEntered = PDDHistory.PDDEntered
	,PddReason = PDDHistory.PDDreason
	,PddPasFlag =	CASE PDDHistory.PDDComment 
					WHEN 'Calculated from PAS length of stay' THEN 1 
					ELSE 0
				END
	,PddEnteredWithinTarget = CASE 
							WHEN DATEDIFF(Minute,Spell.AdmitDate, PDDHistory.PDDEntered) <1440 THEN 1 
							ELSE 0 
						  END
	,ExpectedLOS = datediff(dd,Spell.AdmitDate, PDDHistory.PDD)
	,Created = getdate()
	,Updated = getdate()
	,ByWhom = System_user
FROM         
	[$(BedmanTest)].dbo.HospSpell Spell

INNER JOIN [$(BedmanTest)].dbo.PDDHistory PDDHistory
ON	PDDHistory.SpellID = Spell.SpellID

LEFT OUTER JOIN [$(BedmanTest)].dbo.vwPDDReasons PDDReasons 
ON	PDDReasons.PDDReasonID = PDDHistory.PDDreason 

where
	Spell.AdmitDate IS NOT NULL



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'ExtractBedmanPdd', @Stats, @StartTime


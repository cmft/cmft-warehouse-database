﻿



CREATE proc [ETL].[LoadCHAMPBMICalculator] 
	(
	@from date = null
	,@to date = null
	)
as

/* 
==============================================================================================
Description:

Notes:	No unique identifier, so base on dates instead of merge.

When		Who			What
23/02/2015	Paul Egan	Initial Coding
===============================================================================================
*/




declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@fromDate date
	,@toDate date
;

declare
	 @deleted int
	,@inserted int
	,@updated int
;

set @fromDate = coalesce(@from, dateadd(month, -36, getdate()));
set @toDate = coalesce(@to, getdate());

----select @fromDate, @toDate;

delete from COM.CHAMPBMICalculator
where BMICalculatorTime between @fromDate and @toDate
;

select @deleted = @@rowcount;

insert into COM.CHAMPBMICalculator
	(
	BMICalculatorTime
	,Username
	,Created
	,Updated
	,ByWhom
	)
select
	BMICalculatorTime
	,Username
	,Created = getdate()
	,Updated = getdate()
	,ByWhom = suser_name()
from
	ETL.TLoadCHAMPBMICalculator	
	

select @inserted = @@rowcount;
select @updated = 0;

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime








﻿CREATE procedure [ETL].[ExtractMortalityCriticalCare]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.CriticalCare target
using
	(
	select
		 SourceUniqueID
		,FormTypeID
		,ReviewStatus
		,ReviewedDate
		,ReviewedBy
		,CasenoteNo
		,NHSNumber
		,Age
		,AdmissionTime
		,DateOfDeath
		,PlaceOfDeath
		,FirstConsultantReview
		,SpecialtyGroup
		,PatientName
		,PrimaryDiagnosis
		,ConfirmedPrimaryDiagnosis
		,CauseOfDeath1a
		,CauseOfDeath1b
		,CauseOfDeath1c
		,CauseOfDeath2
		,CauseOfDeathComment
		,CodedDiagnosis
		,AgreeWithStatedCauseOfDeath
		,HospitalPM
		,CoronerInformed
		,CoronerPM
		,CoexistingFactor
		,TimeClinicalDecisionToAdmitMade
		,TimePatientAdmittedToCriticalCare
		,AdmissionStickerUsed
		,TimeToConsultantReviewComment
		,EvidenceOfClearManagementPlan
		,EvidenceOfClearManagementPlanComment
		,EssentialInvestigationsObtainedWithoutDelay
		,EssentialInvestigationsObtainedWithoutDelayComment
		,InitialManagementStepsAppropriateAndAdequate
		,InitialManagementStepsAppropriateAndAdequateComment
		,OmissionsInInitialManagement
		,OmissionsInInitialManagementComment
		,PeriodAtrialFibrilation
		,PeriodAtrialFibrilationComment
		,AtrialFibrilationAppropriatelyManaged
		,AtrialFibrilationAppropriatelyManagedComment
		,EvidenceOfIschemiaInGut
		,EvidenceOfIschemiaInGutComment
		,TherapeuticAntiCoagulation
		,TherapeuticAntiCoagulationComment
		,CriticalCareRoundsDocumentedEveryDay
		,CriticalCareRoundsDocumentedEveryDayComment
		,AnyRecognisableMedicationErrors
		,AnyRecognisableMedicationErrorsComment
		,DelayInDiagnosis
		,DelayInDiagnosisComment
		,DelayInDeliveringCare
		,DelayInOfDeliveringCareComment
		,PoorCommunication
		,PoorCommunicationComment
		,AdverseEvents
		,AdverseEventsComment
		,AnythingCouldBeDoneDifferently
		,AnythingCouldBeDoneDifferentlyComment
		,NotableGoodQualityCare
		,DocumentationIssues
		,DocumentationIssuesComment
		,EndOfLifeCareLCPInPatientNotes
		,EndOfLifeCareLCPInPatientNotesComment
		,EndOfLifeCarePreferredPlaceOfDeathRecorded
		,EndOfLifeCarePreferredPlaceOfDeathRecordedComment
		,EndOfLifeCareWishToDieOutsideHospitalExpressed
		,EndOfLifeCareWishToDieOutsideHospitalExpressedComment
		,LessonsLearned
		,ActionPlan
		,DateOfActionPlanReview
		,ClassificationGrade
		,SourcePatientNo
		,EpisodeStartTime
		,ContextCode
	from
		ETL.TLoadMortalityCriticalCare
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalityCriticalCare Latest
			where
				Latest.SourcePatientNo = TLoadMortalityCriticalCare.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalityCriticalCare.SourceUniqueID
			)
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,FormTypeID
			,ReviewStatus
			,ReviewedDate
			,ReviewedBy
			,CasenoteNo
			,NHSNumber
			,Age
			,AdmissionTime
			,DateOfDeath
			,PlaceOfDeath
			,FirstConsultantReview
			,SpecialtyGroup
			,PatientName
			,PrimaryDiagnosis
			,ConfirmedPrimaryDiagnosis
			,CauseOfDeath1a
			,CauseOfDeath1b
			,CauseOfDeath1c
			,CauseOfDeath2
			,CauseOfDeathComment
			,CodedDiagnosis
			,AgreeWithStatedCauseOfDeath
			,HospitalPM
			,CoronerInformed
			,CoronerPM
			,CoexistingFactor
			,TimeClinicalDecisionToAdmitMade
			,TimePatientAdmittedToCriticalCare
			,AdmissionStickerUsed
			,TimeToConsultantReviewComment
			,EvidenceOfClearManagementPlan
			,EvidenceOfClearManagementPlanComment
			,EssentialInvestigationsObtainedWithoutDelay
			,EssentialInvestigationsObtainedWithoutDelayComment
			,InitialManagementStepsAppropriateAndAdequate
			,InitialManagementStepsAppropriateAndAdequateComment
			,OmissionsInInitialManagement
			,OmissionsInInitialManagementComment
			,PeriodAtrialFibrilation
			,PeriodAtrialFibrilationComment
			,AtrialFibrilationAppropriatelyManaged
			,AtrialFibrilationAppropriatelyManagedComment
			,EvidenceOfIschemiaInGut
			,EvidenceOfIschemiaInGutComment
			,TherapeuticAntiCoagulation
			,TherapeuticAntiCoagulationComment
			,CriticalCareRoundsDocumentedEveryDay
			,CriticalCareRoundsDocumentedEveryDayComment
			,AnyRecognisableMedicationErrors
			,AnyRecognisableMedicationErrorsComment
			,DelayInDiagnosis
			,DelayInDiagnosisComment
			,DelayInDeliveringCare
			,DelayInOfDeliveringCareComment
			,PoorCommunication
			,PoorCommunicationComment
			,AdverseEvents
			,AdverseEventsComment
			,AnythingCouldBeDoneDifferently
			,AnythingCouldBeDoneDifferentlyComment
			,NotableGoodQualityCare
			,DocumentationIssues
			,DocumentationIssuesComment
			,EndOfLifeCareLCPInPatientNotes
			,EndOfLifeCareLCPInPatientNotesComment
			,EndOfLifeCarePreferredPlaceOfDeathRecorded
			,EndOfLifeCarePreferredPlaceOfDeathRecordedComment
			,EndOfLifeCareWishToDieOutsideHospitalExpressed
			,EndOfLifeCareWishToDieOutsideHospitalExpressedComment
			,LessonsLearned
			,ActionPlan
			,DateOfActionPlanReview
			,ClassificationGrade
			,SourcePatientNo
			,EpisodeStartTime
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.FormTypeID
			,source.ReviewStatus
			,source.ReviewedDate
			,source.ReviewedBy
			,source.CasenoteNo
			,source.NHSNumber
			,source.Age
			,source.AdmissionTime
			,source.DateOfDeath
			,source.PlaceOfDeath
			,source.FirstConsultantReview
			,source.SpecialtyGroup
			,source.PatientName
			,source.PrimaryDiagnosis
			,source.ConfirmedPrimaryDiagnosis
			,source.CauseOfDeath1a
			,source.CauseOfDeath1b
			,source.CauseOfDeath1c
			,source.CauseOfDeath2
			,source.CauseOfDeathComment
			,source.CodedDiagnosis
			,source.AgreeWithStatedCauseOfDeath
			,source.HospitalPM
			,source.CoronerInformed
			,source.CoronerPM
			,source.CoexistingFactor
			,source.TimeClinicalDecisionToAdmitMade
			,source.TimePatientAdmittedToCriticalCare
			,source.AdmissionStickerUsed
			,source.TimeToConsultantReviewComment
			,source.EvidenceOfClearManagementPlan
			,source.EvidenceOfClearManagementPlanComment
			,source.EssentialInvestigationsObtainedWithoutDelay
			,source.EssentialInvestigationsObtainedWithoutDelayComment
			,source.InitialManagementStepsAppropriateAndAdequate
			,source.InitialManagementStepsAppropriateAndAdequateComment
			,source.OmissionsInInitialManagement
			,source.OmissionsInInitialManagementComment
			,source.PeriodAtrialFibrilation
			,source.PeriodAtrialFibrilationComment
			,source.AtrialFibrilationAppropriatelyManaged
			,source.AtrialFibrilationAppropriatelyManagedComment
			,source.EvidenceOfIschemiaInGut
			,source.EvidenceOfIschemiaInGutComment
			,source.TherapeuticAntiCoagulation
			,source.TherapeuticAntiCoagulationComment
			,source.CriticalCareRoundsDocumentedEveryDay
			,source.CriticalCareRoundsDocumentedEveryDayComment
			,source.AnyRecognisableMedicationErrors
			,source.AnyRecognisableMedicationErrorsComment
			,source.DelayInDiagnosis
			,source.DelayInDiagnosisComment
			,source.DelayInDeliveringCare
			,source.DelayInOfDeliveringCareComment
			,source.PoorCommunication
			,source.PoorCommunicationComment
			,source.AdverseEvents
			,source.AdverseEventsComment
			,source.AnythingCouldBeDoneDifferently
			,source.AnythingCouldBeDoneDifferentlyComment
			,source.NotableGoodQualityCare
			,source.DocumentationIssues
			,source.DocumentationIssuesComment
			,source.EndOfLifeCareLCPInPatientNotes
			,source.EndOfLifeCareLCPInPatientNotesComment
			,source.EndOfLifeCarePreferredPlaceOfDeathRecorded
			,source.EndOfLifeCarePreferredPlaceOfDeathRecordedComment
			,source.EndOfLifeCareWishToDieOutsideHospitalExpressed
			,source.EndOfLifeCareWishToDieOutsideHospitalExpressedComment
			,source.LessonsLearned
			,source.ActionPlan
			,source.DateOfActionPlanReview
			,source.ClassificationGrade
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.ContextCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			isnull(target.ReviewedDate, getdate()) = isnull(source.ReviewedDate, getdate())
		and isnull(target.ReviewedBy, '') = isnull(source.ReviewedBy, '') 
		and isnull(target.ReviewStatus, 0) = isnull(source.ReviewStatus, 0) 
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.Age, 0) = isnull(source.Age, 0)
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.PlaceOfDeath, '') = isnull(source.PlaceOfDeath, '')
		and isnull(target.FirstConsultantReview, '') = isnull(source.FirstConsultantReview, '')
		and isnull(target.SpecialtyGroup, '') = isnull(source.SpecialtyGroup, '')
		and isnull(target.PatientName, '') = isnull(source.PatientName, '')
		and isnull(target.PrimaryDiagnosis, '') = isnull(source.PrimaryDiagnosis, '')
		and isnull(target.ConfirmedPrimaryDiagnosis, '') = isnull(source.ConfirmedPrimaryDiagnosis, '')
		and isnull(target.CauseOfDeath1a, '') = isnull(source.CauseOfDeath1a, '')
		and isnull(target.CauseOfDeath1b, '') = isnull(source.CauseOfDeath1b, '')
		and isnull(target.CauseOfDeath1c, '') = isnull(source.CauseOfDeath1c, '')
		and isnull(target.CauseOfDeath2, '') = isnull(source.CauseOfDeath2, '')
		and isnull(target.CauseOfDeathComment, '') = isnull(source.CauseOfDeathComment, '')
		and isnull(target.CodedDiagnosis, '') = isnull(source.CodedDiagnosis, '')
		and isnull(target.AgreeWithStatedCauseOfDeath, 0) = isnull(source.AgreeWithStatedCauseOfDeath, 0)
		and isnull(target.HospitalPM, 0) = isnull(source.HospitalPM, 0)
		and isnull(target.CoronerInformed, 0) = isnull(source.CoronerInformed, 0)
		and isnull(target.CoronerPM, 0) = isnull(source.CoronerPM, 0)
		and isnull(target.CoexistingFactor, '') = isnull(source.CoexistingFactor, '')
		and isnull(target.TimeClinicalDecisionToAdmitMade, '') = isnull(source.TimeClinicalDecisionToAdmitMade, '')
		and isnull(target.TimePatientAdmittedToCriticalCare, '') = isnull(source.TimePatientAdmittedToCriticalCare, '')
		and isnull(target.AdmissionStickerUsed, 0) = isnull(source.AdmissionStickerUsed, 0)
		and isnull(target.TimeToConsultantReviewComment, '') = isnull(source.TimeToConsultantReviewComment, '')
		and isnull(target.EvidenceOfClearManagementPlan, 0) = isnull(source.EvidenceOfClearManagementPlan, 0)
		and isnull(target.EvidenceOfClearManagementPlanComment, '') = isnull(source.EvidenceOfClearManagementPlanComment, '')
		and isnull(target.EssentialInvestigationsObtainedWithoutDelay, 0) = isnull(source.EssentialInvestigationsObtainedWithoutDelay, 0)
		and isnull(target.EssentialInvestigationsObtainedWithoutDelayComment, '') = isnull(source.EssentialInvestigationsObtainedWithoutDelayComment, '')
		and isnull(target.InitialManagementStepsAppropriateAndAdequate, 0) = isnull(source.InitialManagementStepsAppropriateAndAdequate, 0)
		and isnull(target.InitialManagementStepsAppropriateAndAdequateComment, '') = isnull(source.InitialManagementStepsAppropriateAndAdequateComment, '')
		and isnull(target.OmissionsInInitialManagement, 0) = isnull(source.OmissionsInInitialManagement, 0)
		and isnull(target.OmissionsInInitialManagementComment, '') = isnull(source.OmissionsInInitialManagementComment, '')
		and isnull(target.PeriodAtrialFibrilation, 0) = isnull(source.PeriodAtrialFibrilation, 0)
		and isnull(target.PeriodAtrialFibrilationComment, '') = isnull(source.PeriodAtrialFibrilationComment, '')
		and isnull(target.AtrialFibrilationAppropriatelyManaged, 0) = isnull(source.AtrialFibrilationAppropriatelyManaged, 0)
		and isnull(target.AtrialFibrilationAppropriatelyManagedComment, '') = isnull(source.AtrialFibrilationAppropriatelyManagedComment, '')
		and isnull(target.EvidenceOfIschemiaInGut, 0) = isnull(source.EvidenceOfIschemiaInGut, 0)
		and isnull(target.EvidenceOfIschemiaInGutComment, '') = isnull(source.EvidenceOfIschemiaInGutComment, '')
		and isnull(target.TherapeuticAntiCoagulation, 0) = isnull(source.TherapeuticAntiCoagulation, 0)
		and isnull(target.TherapeuticAntiCoagulationComment, '') = isnull(source.TherapeuticAntiCoagulationComment, '')
		and isnull(target.CriticalCareRoundsDocumentedEveryDay, 0) = isnull(source.CriticalCareRoundsDocumentedEveryDay, 0)
		and isnull(target.CriticalCareRoundsDocumentedEveryDayComment, '') = isnull(source.CriticalCareRoundsDocumentedEveryDayComment, '')
		and isnull(target.AnyRecognisableMedicationErrors, 0) = isnull(source.AnyRecognisableMedicationErrors, 0)
		and isnull(target.AnyRecognisableMedicationErrorsComment, '') = isnull(source.AnyRecognisableMedicationErrorsComment, '')
		and isnull(target.DelayInDiagnosis, 0) = isnull(source.DelayInDiagnosis, 0)
		and isnull(target.DelayInDiagnosisComment, '') = isnull(source.DelayInDiagnosisComment, '')
		and isnull(target.DelayInDeliveringCare, 0) = isnull(source.DelayInDeliveringCare, 0)
		and isnull(target.DelayInOfDeliveringCareComment, '') = isnull(source.DelayInOfDeliveringCareComment, '')
		and isnull(target.PoorCommunication, 0) = isnull(source.PoorCommunication, 0)
		and isnull(target.PoorCommunicationComment, '') = isnull(source.PoorCommunicationComment, '')
		and isnull(target.AdverseEvents, 0) = isnull(source.AdverseEvents, 0)
		and isnull(target.AdverseEventsComment, '') = isnull(source.AdverseEventsComment, '')
		and isnull(target.AnythingCouldBeDoneDifferently, 0) = isnull(source.AnythingCouldBeDoneDifferently, 0)
		and isnull(target.AnythingCouldBeDoneDifferentlyComment, '') = isnull(source.AnythingCouldBeDoneDifferentlyComment, '')
		and isnull(target.NotableGoodQualityCare, '') = isnull(source.NotableGoodQualityCare, '')
		and isnull(target.DocumentationIssues, 0) = isnull(source.DocumentationIssues, 0)
		and isnull(target.DocumentationIssuesComment, '') = isnull(source.DocumentationIssuesComment, '')
		and isnull(target.EndOfLifeCareLCPInPatientNotes, 0) = isnull(source.EndOfLifeCareLCPInPatientNotes, 0)
		and isnull(target.EndOfLifeCareLCPInPatientNotesComment, '') = isnull(source.EndOfLifeCareLCPInPatientNotesComment, '')
		and isnull(target.EndOfLifeCarePreferredPlaceOfDeathRecorded, 0) = isnull(source.EndOfLifeCarePreferredPlaceOfDeathRecorded, 0)
		and isnull(target.EndOfLifeCarePreferredPlaceOfDeathRecordedComment, '') = isnull(source.EndOfLifeCarePreferredPlaceOfDeathRecordedComment, '')
		and isnull(target.EndOfLifeCareWishToDieOutsideHospitalExpressed, 0) = isnull(source.EndOfLifeCareWishToDieOutsideHospitalExpressed, 0)
		and isnull(target.EndOfLifeCareWishToDieOutsideHospitalExpressedComment, '') = isnull(source.EndOfLifeCareWishToDieOutsideHospitalExpressedComment, '')
		and isnull(target.LessonsLearned, '') = isnull(source.LessonsLearned, '')
		and isnull(target.ActionPlan, '') = isnull(source.ActionPlan, '')
		and isnull(target.DateOfActionPlanReview, getdate()) = isnull(source.DateOfActionPlanReview, getdate())
		and isnull(target.ClassificationGrade, 0) = isnull(source.ClassificationGrade, 0)
		and isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		
		)
	then
		update
		set
			 target.FormTypeID = source.FormTypeID 
			,target.ReviewStatus = source.ReviewStatus 
			,target.ReviewedDate = source.ReviewedDate
			,target.ReviewedBy = source.ReviewedBy
			,target.CasenoteNo = source.CasenoteNo
			,target.NHSNumber = source.NHSNumber
			,target.Age = source.Age
			,target.AdmissionTime = source.AdmissionTime
			,target.DateOfDeath = source.DateOfDeath
			,target.PlaceOfDeath = source.PlaceOfDeath
			,target.FirstConsultantReview = source.FirstConsultantReview
			,target.SpecialtyGroup = source.SpecialtyGroup
			,target.PatientName = source.PatientName
			,target.PrimaryDiagnosis = source.PrimaryDiagnosis
			,target.ConfirmedPrimaryDiagnosis = source.ConfirmedPrimaryDiagnosis
			,target.CauseOfDeath1a = source.CauseOfDeath1a
			,target.CauseOfDeath1b = source.CauseOfDeath1b
			,target.CauseOfDeath1c = source.CauseOfDeath1c
			,target.CauseOfDeath2 = source.CauseOfDeath2
			,target.CauseOfDeathComment = source.CauseOfDeathComment
			,target.CodedDiagnosis = source.CodedDiagnosis
			,target.AgreeWithStatedCauseOfDeath = source.AgreeWithStatedCauseOfDeath
			,target.HospitalPM = source.HospitalPM
			,target.CoronerInformed = source.CoronerInformed
			,target.CoronerPM = source.CoronerPM
			,target.CoexistingFactor = source.CoexistingFactor
			,target.TimeClinicalDecisionToAdmitMade = source.TimeClinicalDecisionToAdmitMade
			,target.TimePatientAdmittedToCriticalCare = source.TimePatientAdmittedToCriticalCare
			,target.AdmissionStickerUsed = source.AdmissionStickerUsed
			,target.TimeToConsultantReviewComment = source.TimeToConsultantReviewComment
			,target.EvidenceOfClearManagementPlan = source.EvidenceOfClearManagementPlan
			,target.EvidenceOfClearManagementPlanComment = source.EvidenceOfClearManagementPlanComment
			,target.EssentialInvestigationsObtainedWithoutDelay = source.EssentialInvestigationsObtainedWithoutDelay
			,target.EssentialInvestigationsObtainedWithoutDelayComment = source.EssentialInvestigationsObtainedWithoutDelayComment
			,target.InitialManagementStepsAppropriateAndAdequate = source.InitialManagementStepsAppropriateAndAdequate
			,target.InitialManagementStepsAppropriateAndAdequateComment = source.InitialManagementStepsAppropriateAndAdequateComment
			,target.OmissionsInInitialManagement = source.OmissionsInInitialManagement
			,target.OmissionsInInitialManagementComment = source.OmissionsInInitialManagementComment
			,target.PeriodAtrialFibrilation = source.PeriodAtrialFibrilation
			,target.PeriodAtrialFibrilationComment = source.PeriodAtrialFibrilationComment
			,target.AtrialFibrilationAppropriatelyManaged = source.AtrialFibrilationAppropriatelyManaged
			,target.AtrialFibrilationAppropriatelyManagedComment = source.AtrialFibrilationAppropriatelyManagedComment
			,target.EvidenceOfIschemiaInGut = source.EvidenceOfIschemiaInGut
			,target.EvidenceOfIschemiaInGutComment = source.EvidenceOfIschemiaInGutComment
			,target.TherapeuticAntiCoagulation = source.TherapeuticAntiCoagulation
			,target.TherapeuticAntiCoagulationComment = source.TherapeuticAntiCoagulationComment
			,target.CriticalCareRoundsDocumentedEveryDay = source.CriticalCareRoundsDocumentedEveryDay
			,target.CriticalCareRoundsDocumentedEveryDayComment = source.CriticalCareRoundsDocumentedEveryDayComment
			,target.AnyRecognisableMedicationErrors = source.AnyRecognisableMedicationErrors
			,target.AnyRecognisableMedicationErrorsComment = source.AnyRecognisableMedicationErrorsComment
			,target.DelayInDiagnosis = source.DelayInDiagnosis
			,target.DelayInDiagnosisComment = source.DelayInDiagnosisComment
			,target.DelayInDeliveringCare = source.DelayInDeliveringCare
			,target.DelayInOfDeliveringCareComment = source.DelayInOfDeliveringCareComment
			,target.PoorCommunication = source.PoorCommunication
			,target.PoorCommunicationComment = source.PoorCommunicationComment
			,target.AdverseEvents = source.AdverseEvents
			,target.AdverseEventsComment = source.AdverseEventsComment
			,target.AnythingCouldBeDoneDifferently = source.AnythingCouldBeDoneDifferently
			,target.AnythingCouldBeDoneDifferentlyComment = source.AnythingCouldBeDoneDifferentlyComment
			,target.NotableGoodQualityCare = source.NotableGoodQualityCare
			,target.DocumentationIssues = source.DocumentationIssues
			,target.DocumentationIssuesComment = source.DocumentationIssuesComment
			,target.EndOfLifeCareLCPInPatientNotes = source.EndOfLifeCareLCPInPatientNotes
			,target.EndOfLifeCareLCPInPatientNotesComment = source.EndOfLifeCareLCPInPatientNotesComment
			,target.EndOfLifeCarePreferredPlaceOfDeathRecorded = source.EndOfLifeCarePreferredPlaceOfDeathRecorded
			,target.EndOfLifeCarePreferredPlaceOfDeathRecordedComment = source.EndOfLifeCarePreferredPlaceOfDeathRecordedComment
			,target.EndOfLifeCareWishToDieOutsideHospitalExpressed = source.EndOfLifeCareWishToDieOutsideHospitalExpressed
			,target.EndOfLifeCareWishToDieOutsideHospitalExpressedComment = source.EndOfLifeCareWishToDieOutsideHospitalExpressedComment
			,target.LessonsLearned = source.LessonsLearned
			,target.ActionPlan = source.ActionPlan
			,target.DateOfActionPlanReview = source.DateOfActionPlanReview
			,target.ClassificationGrade = source.ClassificationGrade
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



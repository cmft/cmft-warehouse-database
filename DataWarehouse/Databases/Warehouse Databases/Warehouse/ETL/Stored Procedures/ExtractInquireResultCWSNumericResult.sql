﻿

CREATE proc [ETL].[ExtractInquireResultCWSNumericResult] 
(
	@FromDate date
	,@ToDate date
) as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

insert into ETL.TImportResultCWSNumericResult
	
(
	SourceUniqueID
	,SourcePatientNo
	,SourceEpisodeNo
	,TransactionNo
	,SessionNo
	,OrderSourceUniqueID
	,ConsultantCode
	,SpecialtyCode
	,OrderPriorityCode
	,OrderStatusCode
	,OrderEnteredByCode
	,OrderedByCode
	,OrderTime
	,OrderComment
	,SampleReferenceCode
	,EffectiveTime
	,LocationCode
	,ServiceCode
	,ResultCode
	,Result
	,ResultTime
	,RangeIndicator
	,ResultEnteredByCode
	,ResultStatusCode
	,ResultComment
)

select
	SourceUniqueID
	,SourcePatientNo 
	,SourceEpisodeNo 
	,TransactionNo 
	,SessionNo
	,OrderSourceUniqueID
	,ConsultantCode 
	,SpecialtyCode 
	,OrderPriorityCode
	,OrderStatusCode 
	,OrderEnteredByCode 
	,OrderedByCode 
	,OrderTime 
	,OrderComment
	,SampleReferenceCode
	,EffectiveTime 
	,LocationCode 
	,ServiceCode 
	,ResultCode 
	,Result
	,ResultTime
	,RangeIndicator
	,ResultEnteredByCode 
	,ResultStatusCode
	,ResultComment
from
	(
	select --top 100
		SourceUniqueID = rtrim(Result.OCRESULTID)
		,SourcePatientNo = OCORDER.PatientNumber
		,SourceEpisodeNo = OCORDER.EpisodeNumber
		,TransactionNo = OCORDER.OcmTransKey
		,SessionNo = Result.SessionNumber 
		,OrderSourceUniqueID = OCORDER.OCORDERID 
		,ConsultantCode = OCORDER.Consultant
		,SpecialtyCode = OCORDER.Specialty
		,OrderPriorityCode = OCORDER.Priority
		,OrderStatusCode = OCORDER.Status
		,OrderEnteredByCode = OCORDER.EnteredBy
		,OrderedByCode = OCORDER.OrderedBy
		,OrderTime =
				 coalesce(
					cast(OrderDate as varchar)
					,'1 jan 1900'
					) + ' ' + 
				coalesce(
					left(OrderTime, 8)
					,'00:00:00'
					)
		,OrderComment = 
					nullif(
						coalesce(nullif(OCORDER.OrderComment1 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment2 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment3 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment4 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment5 + char(13), '   '), '')
						+ coalesce(nullif(OCORDER.OrderComment6 + char(13), '   '), '')	
					, '')
		,SampleReferenceCode = OCORDER.SampleRef 
		,EffectiveTime = cast(OCORDER.StartDateTime as datetime)
		,LocationCode = OCORDER.PerfLocation
		,ServiceCode = OCORDER.ServiceCode
		,ResultCode = ltrim(rtrim(Result.ResultCode))
		,Result.Result
		,ResultTime =
				case
				when Result.ResultTime = '24:00'
				then convert(smalldatetime , dateadd(day , 1 , Result.ResultDate))
				else convert(smalldatetime , cast(Result.ResultDate as varchar(20)) + ' '  + Result.ResultTime)
				end
		,Result.RangeIndicator
		,ResultEnteredByCode = Result.EnteredBy
		,ResultStatusCode = Result.Status
		,ResultComment = 
					nullif(
						coalesce(nullif(Result.Comment1 + char(13), '   '), '')
						+ coalesce(nullif(Result.Comment2 + char(13), '   '), '')
						+ coalesce(nullif(Result.Comment3 + char(13), '   '), '')
						+ coalesce(nullif(Result.Comment4 + char(13), '   '), '')
						+ coalesce(nullif(Result.Comment5 + char(13), '   '), '')
						+ coalesce(nullif(Result.Comment6 + char(13), '   '), '')	
					, '')
	from
		[$(PAS)].Inquire.OCORDER

	inner join [$(PAS)].Inquire.OCRESULT Result
	on	Result.OcmTransKey = OCORDER.OcmTransKey

	inner join [$(PAS)].Inquire.OCMFRESULTS ResultReference
	on	ResultReference.ResultCd = Result.ResultCode
	and	ResultReference.ResultType = 'NUMERIC'
	and not exists
				(
				select
					1
				from
					[$(PAS)].Inquire.OCMFRESULTS ResultReferenceDeleted
				where
					ResultReferenceDeleted.ResultCd = ResultReference.ResultCd
				and	ResultReferenceDeleted.Deleted < ResultReference.Deleted
				)

	--Within each OCMTransKey there may be multiple order sessions and result sessions.
	--These are essentially storing a version history.
	--The session numbers decrement by one for each version created (this is for Cache optimisation)
	--E.g. 2 hours after ordering a test the first few results are sent back (creating the first result session); 4 hours later more results are available and the entire result set is sent back (creating a second result session)
	--Therefore the only result session that you need to worry about is the one with the lowest result session number
	
	where
		cast(OCORDER.StartDateTime as date) between @FromDate and @ToDate
	--and ResultReference.ResultType = 'NUMERIC'
	and Result.Status = '85' -- Confirmed as required by D.Forster 24.9.14
	and not exists
				(
				select
					1
				from
					[$(PAS)].Inquire.OCRESULT EarlierSession
				where
					EarlierSession.OcmTransKey = Result.OcmTransKey
				and	EarlierSession.SessionNumber < Result.SessionNumber
				and EarlierSession.Status = '85' 
				)	

	) NumericResult


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿
create proc ETL.ExtractDictationArchiveDocument

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table ETL.TImportDictationArchiveDocument

insert into ETL.TImportDictationArchiveDocument

select
	[ID]
	,[Title]
	,[BatchNr]
	,[Length]
	,[DocumentType]
	,[Priority]
	,[WorktypeID]
	,[WorkPlace]
	,[InterfaceID]
	,[GroupID]
	,[UserIDAuthor]
	,[UserIDTranscriptionist]
	,[UserIDAuthorizer]
	,[CreationDate]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate]
	,[CorrectionTime]
	,[WaitingTimeAuthorization]
	,[AuthorizationDate]
	,[PassThroughTime]
	,[Reserved01]
	,[Reserved02]
	,[Reserved03]
	,[Reserved04]
	,[Reserved05]
	,[Reserved06]
	,[Reserved07]
	,[Reserved08]
	,[Reserved09]
	,[Reserved10]
	,[ContextID]
	,[DictationTime]
	,[CorrectionRate]
from
	[$(G2_G2Archive5)].[dbo].[Archived_G2Document]
	
select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


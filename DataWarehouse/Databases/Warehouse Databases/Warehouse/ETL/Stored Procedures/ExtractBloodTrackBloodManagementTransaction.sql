﻿

CREATE Procedure [ETL].[ExtractBloodTrackBloodManagementTransaction]  --'20140101','20140930'
	@fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @RowsInserted = 0

select @from = coalesce(@fromDate, cast(cast(dateadd(month, -3, getdate())as date) as datetime))

select @to = coalesce(@toDate, cast(cast(dateadd(day, -1, getdate())as date)as datetime)+'23:59')


--BloodTransaction.TransactionTypeID in (3,5,6,9,12,16) -- Discussed with DG who advised to bring through every transaction, this criteria can be added at the RPT stage

Truncate Table ETL.TImportBloodManagementTransaction

Insert into ETL.TImportBloodManagementTransaction
	(
	TransactionID
	,TransactionTypeID
	,TransactionTime 
	,ConditionID
	,BloodUniqueID 
	,DonorNumber 
	,BloodProductID
	,BloodProductVolume 
	,BloodGroupID
	,ReservationTime
	,ExpiryTime
	,FateID 
	,SiteID 
	,LocationID
	,PatientID
	,HospitalNumber
	,NHSNumber 
	,LastName
	,FirstName
	,DateOfBirth 
	,GenderCode
	,SystemUserID
	,InterfaceCode 
	)
select 
	BloodTransaction.TransactionID
	,BloodTransaction.TransactionTypeID
	,TransactionTime = BloodTransaction.TransactionDate
	,BloodTransaction.ConditionID
	,BloodUniqueID = (UnitNumber + Unit.ProductCode)
	,DonorNumber = Unit.UnitNumber
	,Product.BloodProductID
	,BloodProductVolume = Product.DefaultVolume
	,BloodTransaction.BloodGroupID
	,BloodTransaction.ReservationDate
	,BloodTransaction.ExpiryDate
	,FateID = BloodTransaction.BloodUnitFateID
	,BloodTransaction.SiteID
	,LocationID = BloodTransaction.DeviceLocationID
	,BloodTransaction.PatientID
	,Patient.HospitalNumber
	,NHSNumber = Patient.HealthNumber
	,Patient.LastName
	,Patient.FirstName
	,DateOfBirth = Patient.BirthDate
	,GenderCode = 
			case 
				when Patient.GenderID = 1 then 2
				when Patient.GenderID = 0 then 1
				when Patient.GenderID = 2 then 9
				else 0
			end
	
	,SystemUserID = BloodTransaction.UserID
	,InterfaceCode = 'BLOODTRACK'
from 
	[$(Bloodtrack_Neoteric)].dbo.tbl_BloodUnitTransaction BloodTransaction

inner join [$(Bloodtrack_Neoteric)].dbo.tbl_BloodUnit Unit
on BloodTransaction.BloodUnitID = Unit.BloodUnitID

inner join [$(Bloodtrack_Neoteric)].dbo.tbl_BloodProduct Product
on Unit.BloodProductID = Product.BloodProductID

left join [$(Bloodtrack_Neoteric)].dbo.tbl_Patient Patient
on BloodTransaction.PatientID = Patient.PatientID

where 
	TransactionDate between @from and @to
and not exists
	(select
		1
	from 
		[$(Bloodtrack_Neoteric)].dbo.tbl_Patient TestPatient
	where 
		Patient.PatientID = TestPatient.PatientID
	and (
		left(HospitalNumber,2) = 'IH'
		or
		(LastName like '%Neg%' and FirstName like '%Test%')
		or
		(LastName like '%Pos%' and FirstName like '%Test%')
		or
		(LastName like '%Test%' and FirstName like '%Test%')
		or
		(LastName like '%Unknown%' and FirstName like '%Test%')
		)

	)




select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractBloodTrackTransaction', @Stats, @StartTime




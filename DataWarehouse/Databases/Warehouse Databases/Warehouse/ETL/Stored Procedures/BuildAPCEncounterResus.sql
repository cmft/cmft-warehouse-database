﻿
CREATE proc [ETL].[BuildAPCEncounterResus]

as


truncate table APC.EncounterResus

insert into APC.EncounterResus
(
	ResusEncounterRecno
	,APCEncounterRecno
)


select
	ResusEncounterRecno = Resus.EncounterRecno
	,APCEncounterRecno = APC.EncounterRecno  
from
	Resus.Encounter Resus

inner join APC.Encounter APC
on	Resus.CasenoteNumber = APC.CasenoteNumber
and Resus.EventDate between APC.EpisodeStartDate and APC.EpisodeEndDate
and not exists
			(
			select
				1
			from
				APC.Encounter APCLater
			where
				Resus.CasenoteNumber = APCLater.CasenoteNumber
			and Resus.EventDate between APCLater.EpisodeStartDate and APCLater.EpisodeEndDate
			and APCLater.EpisodeStartTime > APC.EpisodeStartTime
			)
where
	Resus.EventDate is not null
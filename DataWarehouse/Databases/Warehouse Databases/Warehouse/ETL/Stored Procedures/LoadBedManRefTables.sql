﻿CREATE procedure [ETL].[LoadBedManRefTables] as

-- Copy BedMan reference tables into the current database
truncate table Bedman.EventType

insert into Bedman.EventType

select
	EventTypeCode = cast([ID] as varchar(20))
	,EventType = Description
from
	[$(BedmanTest)].dbo.EventTypes

truncate table Bedman.Severity

insert into Bedman.Severity

select
	SeverityCode = cast([ID] as varchar(20))
	,Severity = Description
from
	[$(BedmanTest)].dbo.FallSeverity





﻿CREATE proc [ETL].[LoadPEXSMSReferenceData]

as

truncate table PEX.SMSLocation

insert into PEX.SMSLocation

select
	LocationID = cast([locationIdentifier] as int)
	,[Location]
	,LocationTypeID = 1
from
	[$(SmallDatasets)].[Envoy].[locations]




truncate table PEX.SMSQuestion

insert into PEX.SMSQuestion


select
	QuestionID = cast([questionIdentifier] as int)
	,Question = [Survey Question]
	,QuestionTypeID = coalesce(XrefEntityCode, 0)
from 
	[$(SmallDatasets)].[Envoy].[SurveyQuestions]

left join dbo.EntityXref QuestionType
on	SurveyQuestions.questionIdentifier = QuestionType.EntityCode
and QuestionType.EntityTypeCode in ('PEXQUESTION')
	
left join dbo.EntityLookup
on	EntityLookup.EntityTypeCode = QuestionType.EntityTypeCode
and	EntityLookup.EntityCode = QuestionType.EntityCode
and EntityLookup.EntityTypeCode in ('PEXQUESTION')


	
	
	

truncate table PEX.SMSAnswer

insert into PEX.SMSAnswer

select
	AnswerID = cast(answerIdentifier as bigint)
	,Answer = response
from 
	[$(SmallDatasets)].Envoy.SurveyAnswers
	
where 
	response in ('1','2','3','4','5','6')
	
union
	select 1,'1'
union
	select 2,'2'		
union
	select 3,'3'	
union
	select 4,'4'
union
	select 5,'5'
union
	select 6,'6'  --brought in beacuse LatsRating replaces the Answer Value in teh extract proc CH 12012015

		
truncate table PEX.SMSSurvey

insert into PEX.SMSSurvey
	(
	SurveyID
	,Survey
	,ChannelID
	)
values(
	1
	,'Short Message Service'
	,2
	)


	


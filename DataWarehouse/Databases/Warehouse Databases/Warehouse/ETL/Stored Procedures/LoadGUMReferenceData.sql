﻿
CREATE procedure [ETL].[LoadGUMReferenceData] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


insert GUM.AppointmentType
(
	AppointmentType
)

select distinct
	[Appointment Type]
from
	[$(SmallDatasets)].GUM.baseData
where
	not exists
		(
		select
			1
		from
			GUM.AppointmentType
		where
			AppointmentType.AppointmentType = baseData.[Appointment Type]
		)

insert GUM.AttendanceStatus
(
	AttendanceStatus
)

select distinct
	[Attended or Did Not Attend]
from
	[$(SmallDatasets)].GUM.baseData
where
	not exists
		(
		select
			1
		from
			GUM.AttendanceStatus
		where
			AttendanceStatus.AttendanceStatus = baseData.[Attended or Did Not Attend]
		)



insert GUM.Clinic
(
Clinic
)
select distinct
	Clinic
from
	[$(SmallDatasets)].GUM.baseData
where
	not exists
		(
		select
			1
		from
			GUM.Clinic
		where
			Clinic.Clinic = baseData.[Clinic]
		)

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


﻿CREATE Procedure [ETL].[LoadRADUserBase] 
(
	@ImportDateTimeStamp datetime
)

AS

	/******************************************************************************
	**  Name: LoadRADUserBase
	**  Purpose: 
	**
	**  Import procedure for Radiology RAD.UserBase table rows 
	**
	**	Called by LoadRAD
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 26.06.12     MH        Altered for CMFT environment
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CensusDate smalldatetime
declare @InterfaceCode char(5)
declare @Process varchar(255)


select @Process = 'LoadRADUserBase'

select @StartTime = getdate()

	
delete from RAD.UserBase 
where	
	exists (
		select
			1
		from
			ETL.TLoadRADUserBase
		where
			    UserBase.UserID = TLoadRADUserBase.UserID
			AND TLoadRADUserBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)

select @RowsDeleted = @@ROWCOUNT


INSERT INTO RAD.UserBase
(
	 UserID 
	,UserName 
	,LoginID 
	,CreationTime 
	,LogicallyDeletedFlag 
	,Level 
	,ModifiedTime 
	,UserAccessLevel 
	,ValidFrom 
	,ValidTo 
	,SiteCode 
	,SpineRolesDate 
)

SELECT
	 UserID 
	,UserName 
	,LoginID 
	,CreationTime 
	,LogicallyDeletedFlag 
	,Level 
	,ModifiedTime 
	,UserAccessLevel 
	,ValidFrom 
	,ValidTo 
	,SiteCode 
	,SpineRolesDate 
FROM
	ETL.TLoadRADUserBase
where
	ImportDateTimeStamp = @ImportDateTimeStamp


select @RowsInserted = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats =
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Net change '  + CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins '



exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

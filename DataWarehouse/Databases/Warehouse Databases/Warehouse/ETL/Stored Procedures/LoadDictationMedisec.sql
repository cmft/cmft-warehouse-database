﻿
CREATE Procedure [ETL].[LoadDictationMedisec]

	@fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime = getdate()
declare @Elapsed int
declare @Stats varchar(255)
declare @From smalldatetime = coalesce(@fromDate, dateadd(year, -1, cast(getdate() as date))) 
declare @To smalldatetime = coalesce(@toDate, getdate())


exec ETL.LoadDictationMedisecReferenceData

truncate table ETL.TImportMedisecDictationDocument
exec ETL.ExtractMedisecDictationDocument @From ,@To
exec ETL.LoadDictationMedisecDocument

truncate table ETL.TImportMedisecDictationInpatient
exec ETL.ExtractMedisecDictationInpatient @From ,@To
exec ETL.LoadDictationMedisecInpatient

truncate table ETL.TImportMedisecDictationOutpatient
exec ETL.ExtractMedisecDictationOutpatient @From ,@To
exec ETL.LoadDictationMedisecOutpatient

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@From, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@To, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats
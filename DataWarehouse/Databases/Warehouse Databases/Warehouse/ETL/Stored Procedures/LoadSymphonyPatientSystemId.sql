﻿
CREATE PROCEDURE [ETL].[LoadSymphonyPatientSystemId]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony PatientSystemId table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 PatientSystemIdID
			,PatientID
			,SystemID
			,PatientSystemId
			,StatusID
			,CreatedByID
			,UpdatedTime
			,PatientSystemIdReversed
			)
into
	#TLoadSymphonyPatientSystemId
from
	(
	select
		 PatientSystemIdID = cast(PatientSystemIdID as int)
		,PatientID = cast(PatientID as int)
		,SystemID = cast(SystemID as int)
		,PatientSystemId = cast(PatientSystemId as varchar(30))
		,StatusID = cast(StatusID as int)
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,PatientSystemIdReversed = cast(PatientSystemIdReversed as varchar(30))
	from
		(
		select
			 PatientSystemIdID = Patient_system_ids.psi_id
			,PatientID = Patient_system_ids.psi_pid
			,SystemID = Patient_system_ids.psi_system_name
			,PatientSystemId = Patient_system_ids.psi_system_id
			,StatusID = Patient_system_ids.psi_status
			,CreatedByID = Patient_system_ids.psi_createdby
			,UpdatedTime = Patient_system_ids.psi_update
			,PatientSystemIdReversed = Patient_system_ids.psi_system_idrev
		from
			[$(Symphony)].dbo.Patient_system_ids

		) Encounter
	) Encounter
order by
	Encounter.PatientSystemIdID

create unique clustered index #IX_TLoadSymphonyPatientSystemId on #TLoadSymphonyPatientSystemId
	(
	PatientSystemIdID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.PatientSystemId target
using
	(
	select
		 PatientSystemIdID
		,PatientID
		,SystemID
		,PatientSystemId
		,StatusID
		,CreatedByID
		,UpdatedTime
		,PatientSystemIdReversed

		,EncounterChecksum
	from
		#TLoadSymphonyPatientSystemId
	
	) source
	on	source.PatientSystemIdID = target.PatientSystemIdID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 PatientSystemIdID
			,PatientID
			,SystemID
			,PatientSystemId
			,StatusID
			,CreatedByID
			,UpdatedTime
			,PatientSystemIdReversed

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.PatientSystemIdID
			,source.PatientID
			,source.SystemID
			,source.PatientSystemId
			,source.StatusID
			,source.CreatedByID
			,source.UpdatedTime
			,source.PatientSystemIdReversed
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.PatientSystemIdID = source.PatientSystemIdID
			,target.PatientID = source.PatientID
			,target.SystemID = source.SystemID
			,target.PatientSystemId = source.PatientSystemId
			,target.StatusID = source.StatusID
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.PatientSystemIdReversed = source.PatientSystemIdReversed
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿
CREATE PROCEDURE [ETL].[LoadSymphonyNote]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Note table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 NoteID
			,Note
			,RecordedByID
			,CreatedByID
			,UpdatedTime
			)
into
	#TLoadSymphonyNote
from
	(
	select
		 NoteID = cast(NoteID as int)
		,Note = cast(Note as varchar(4000))
		,RecordedByID = cast(RecordedByID as int)
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
	from
		(
		select
			 NoteID = Notes.not_noteid
			,Note = Notes.not_text
			,RecordedByID = Notes.not_recordedby
			,CreatedByID = Notes.not_createdby
			,UpdatedTime = Notes.not_update
		from
			[$(Symphony)].dbo.Notes

		) Encounter
	) Encounter
order by
	Encounter.NoteID

create unique clustered index #IX_TLoadSymphonyNote on #TLoadSymphonyNote
	(
	NoteID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Note target
using
	(
	select
		 NoteID
		,Note
		,RecordedByID
		,CreatedByID
		,UpdatedTime
		,EncounterChecksum
	from
		#TLoadSymphonyNote
	
	) source
	on	source.NoteID = target.NoteID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 NoteID
			,Note
			,RecordedByID
			,CreatedByID
			,UpdatedTime

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.NoteID
			,source.Note
			,source.RecordedByID
			,source.CreatedByID
			,source.UpdatedTime
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.NoteID = source.NoteID
			,target.Note = source.Note
			,target.RecordedByID = source.RecordedByID
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
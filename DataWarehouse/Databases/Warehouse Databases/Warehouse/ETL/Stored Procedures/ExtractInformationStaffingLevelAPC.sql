﻿
CREATE procedure [ETL].[ExtractInformationStaffingLevelAPC]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = object_schema_name(@@procid) + '.' + object_name(@@procid)


select @StartTime = getdate();

select @RowsInserted = 0;

truncate table ETL.TImportStaffingLevelAPC;

insert into ETL.TImportStaffingLevelAPC
(
	DivisionCode
	,WardCode
	,CensusDate
	,Shift
	,RegisteredNursePlan
	,RegisteredNurseActual
	,NonRegisteredNursePlan    
	,NonRegisteredNurseActual
	,Comments
	,SeniorComments
)	

	select  
		DivisionCode = Division
		,WardCode = Ward
		,CensusDate = DateFor
		,Shift = WorkShift
		,RegisteredNursePlan = RN_Planned
		,RegisteredNurseActual = RN_Actual
		,NonRegisteredNursePlan = AP_CSW_Planned 
		,NonRegisteredNurseActual = AP_CSW_Actual
		,Comments = Comments
		,SeniorComments = CommentsSenior
	from
		[$(Information)].NursingWorkforce.PlannedActuals
	
select
	@RowsInserted = @RowsInserted + @@rowcount;

select	
	@Elapsed = datediff(minute,@StartTime,getdate());

select @Stats = 
	'Rows Inserted ' + convert(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;

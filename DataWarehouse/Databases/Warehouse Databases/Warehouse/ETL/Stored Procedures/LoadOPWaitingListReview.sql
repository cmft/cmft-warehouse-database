﻿CREATE procedure [ETL].[LoadOPWaitingListReview] as

set dateformat dmy

Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsDeleted Int
Declare @RowsInserted Int
Declare @Stats varchar(255)

Select @StartTime = getdate()

declare @CensusDate smalldatetime

select
	@CensusDate =
	(
	select top 1
		CensusDate
	from
		ETL.TImportOPWaitingListReview
	)


-- Clear down old snapshots
delete
from
	OP.WaitingListReview
where
	CensusDate = @CensusDate


SELECT @RowsDeleted = @@ROWCOUNT

declare @CensusCutOffDate smalldatetime

select @CensusCutOffDate =
	dateadd(
		 day
		,(
		select
			NumericValue * -1
		from
			Utility.Parameter
		where
			Parameter = 'KEEPSNAPSHOTDAYS'
		)
		,@CensusDate
	)

--last snapshot in month
select
	CensusDate = max(CensusDate) 
into
	#OldSnapshot
from
	OP.WaitingListReview
where
	CensusDate < @CensusCutOffDate
group by
	 datepart(year, CensusDate)
	,datepart(month, CensusDate)
;
--sunday snapshot
set datefirst 1

insert into #OldSnapshot

select distinct
	CensusDate 
from
	OP.WaitingListReview

where
	datepart(dw, WaitingListReview.CensusDate) = 7
and	WaitingListReview.CensusDate < @CensusCutOffDate
and	not exists
		(
		select
			1
		from
			#OldSnapshot OldSnapshot
		where
			OldSnapshot.CensusDate = WaitingListReview.CensusDate
		)

----sunday snapshot (old version)
--insert into #OldSnapshot

--select distinct
--	CensusDate 
--from
--	OP.WaitingListReview

--inner join WarehouseOLAPMergedV2.WH.Calendar OlapCalendar
--on	WaitingListReview.CensusDate = OlapCalendar.TheDate
--and	WaitingListReview.CensusDate = OlapCalendar.LastDayOfWeek

--where
--	CensusDate < @CensusCutOffDate
--and	not exists
--		(
--		select
--			1
--		from
--			#OldSnapshot OldSnapshot
--		where
--			OldSnapshot.CensusDate = WaitingListReview.CensusDate
--		)

--2011-12-12 CCB Keep all snapshots for now
----delete unwanted snapshots
--delete from OP.WaitingList
--where
--	not exists
--	(
--	select
--		1
--	from
--		#OldSnapshot OldSnapshot
--	where
--		OldSnapshot.CensusDate = WaitingList.CensusDate
--	)
--and	CensusDate < @CensusCutOffDate


--select @RowsDeleted = @RowsDeleted + @@ROWCOUNT
select @RowsDeleted = 0


INSERT INTO OP.WaitingListReview
(
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,ReferralDate
	,BookedDate
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,SiteCode
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,CasenoteNumber
	,KornerWait
	,InterfaceCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,FuturePatientCancelDate
	,AdditionFlag
	,LastAppointmentFlag
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode
	,RTTBreachDate
	,ClockStartDate
	,WLAppointmentTypeCode
	,PatientDeathIndicator
	,ScheduledCancelReasonCode
	,FirstAttendanceFlag
)
select
	 TLoadOPWaitingList.SourcePatientNo
	,TLoadOPWaitingList.SourceEncounterNo
	,TLoadOPWaitingList.CensusDate
	,TLoadOPWaitingList.PatientSurname
	,TLoadOPWaitingList.PatientForename
	,TLoadOPWaitingList.PatientTitle
	,TLoadOPWaitingList.SexCode
	,TLoadOPWaitingList.DateOfBirth
	,TLoadOPWaitingList.DateOfDeath
	,TLoadOPWaitingList.NHSNumber
	,TLoadOPWaitingList.DistrictNo
	,TLoadOPWaitingList.ReferralDate
	,TLoadOPWaitingList.BookedDate
	,TLoadOPWaitingList.AppointmentDate
	,TLoadOPWaitingList.AppointmentTypeCode
	,TLoadOPWaitingList.AppointmentStatusCode
	,TLoadOPWaitingList.AppointmentCategoryCode
	,TLoadOPWaitingList.QM08StartWaitDate
	,TLoadOPWaitingList.QM08EndWaitDate
	,TLoadOPWaitingList.AppointmentTime
	,TLoadOPWaitingList.ClinicCode
	,TLoadOPWaitingList.SourceOfReferralCode
	,TLoadOPWaitingList.MaritalStatusCode
	,TLoadOPWaitingList.ReligionCode
	,TLoadOPWaitingList.Postcode
	,TLoadOPWaitingList.PatientsAddress1
	,TLoadOPWaitingList.PatientsAddress2
	,TLoadOPWaitingList.PatientsAddress3
	,TLoadOPWaitingList.PatientsAddress4
	,TLoadOPWaitingList.DHACode
	,TLoadOPWaitingList.HomePhone
	,TLoadOPWaitingList.WorkPhone
	,TLoadOPWaitingList.EthnicOriginCode
	,TLoadOPWaitingList.ConsultantCode
	,TLoadOPWaitingList.SpecialtyCode
	,TLoadOPWaitingList.PASSpecialtyCode
	,TLoadOPWaitingList.PriorityCode
	,TLoadOPWaitingList.WaitingListCode
	,TLoadOPWaitingList.CommentClinical
	,TLoadOPWaitingList.CommentNonClinical
	,TLoadOPWaitingList.SiteCode
	,TLoadOPWaitingList.ProviderCode
	,TLoadOPWaitingList.PurchaserCode
	,TLoadOPWaitingList.ContractSerialNumber
	,TLoadOPWaitingList.CancelledBy
	,TLoadOPWaitingList.CasenoteNumber
	,TLoadOPWaitingList.KornerWait
	,TLoadOPWaitingList.InterfaceCode
	,TLoadOPWaitingList.EpisodicGpCode
	,TLoadOPWaitingList.EpisodicGpPracticeCode
	,TLoadOPWaitingList.RegisteredGpCode
	,TLoadOPWaitingList.RegisteredPracticeCode
	,TLoadOPWaitingList.SourceTreatmentFunctionCode
	,TLoadOPWaitingList.TreatmentFunctionCode
	,TLoadOPWaitingList.NationalSpecialtyCode
	,TLoadOPWaitingList.PCTCode
	,TLoadOPWaitingList.BreachDate
	,TLoadOPWaitingList.FuturePatientCancelDate
	,TLoadOPWaitingList.AdditionFlag
	,TLoadOPWaitingList.LastAppointmentFlag
	,TLoadOPWaitingList.BreachDays
	,TLoadOPWaitingList.MRSA
	,TLoadOPWaitingList.RTTPathwayID
	,TLoadOPWaitingList.RTTPathwayCondition
	,TLoadOPWaitingList.RTTStartDate
	,TLoadOPWaitingList.RTTEndDate
	,TLoadOPWaitingList.RTTSpecialtyCode
	,TLoadOPWaitingList.RTTCurrentProviderCode
	,TLoadOPWaitingList.RTTCurrentStatusCode
	,TLoadOPWaitingList.RTTCurrentStatusDate
	,TLoadOPWaitingList.RTTCurrentPrivatePatientFlag
	,TLoadOPWaitingList.RTTOverseasStatusFlag
	,TLoadOPWaitingList.CountOfDNAs
	,TLoadOPWaitingList.CountOfHospitalCancels
	,TLoadOPWaitingList.CountOfPatientCancels
	,TLoadOPWaitingList.AppointmentID
	,TLoadOPWaitingList.SourceUniqueID
	,TLoadOPWaitingList.AdminCategoryCode
	,TLoadOPWaitingList.RTTBreachDate
	,TLoadOPWaitingList.ClockStartDate
	,TLoadOPWaitingList.WLAppointmentTypeCode
	,TLoadOPWaitingList.PatientDeathIndicator
	,TLoadOPWaitingList.ScheduledCancelReasonCode
	,TLoadOPWaitingList.FirstAttendanceFlag
FROM
	ETL.TLoadOPWaitingListReview TLoadOPWaitingList


SELECT @RowsInserted = @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())
SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@CensusDate, ''))

EXEC Utility.WriteAuditLogEvent 'LoadOPWaitingListReview', @Stats, @StartTime
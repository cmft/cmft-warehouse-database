﻿
create proc ETL.LoadPASPreviousAddress

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select
	@StartTime = getdate()

truncate table PAS.PreviousAddress

insert into PAS.PreviousAddress
(
	[PREVADDRESSID]
	,[DistrictNo]
	,[EffectiveFromDate]
	,[EffectiveToDate]
	,[HaCode]
	,[SourcePatientNo]
	,[Postcode]
	,[PseudoPostCode]
	,[PatientAddressLine1]
	,[PatientAddressLine2]
	,[PatientAddressLine3]
	,[PatientAddressLine4]
	,[SequenceNo]
)

select
	[PREVADDRESSID]
	,[DistrictNo]
	,[EffectiveFromDate]
	,[EffectiveToDate]
	,[HaCode]
	,[InternalPatientNumber]
	,[Postcode]
	,[PseudoPostCode]
	,[PatientAddressLine1]
	,[PatientAddressLine2]
	,[PatientAddressLine3]
	,[PatientAddressLine4]
	,[SequenceNo]
from
	[ETL].TLoadPASPreviousAddress

select @RowsInserted = @@Rowcount


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadPASPreviousAddress', @Stats, @StartTime


﻿CREATE PROCEDURE [ETL].[LoadAPCMidnightBedState]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select
	@StartTime = getdate()


select
	 @LoadStartDate = MIN(CONVERT(datetime, CensusDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, CensusDate)) 
from
	ETL.TLoadAPCMidnightBedState

delete from APC.MidnightBedState
where	
	CensusDate between @LoadStartDate and @LoadEndDate

SELECT @RowsDeleted = @@Rowcount



INSERT INTO APC.MidnightBedState
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SiteCode
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,SourceAdminCategoryCode
	,ActivityInCode
	,AdmissionTime
	,AdmissionDate
	,CensusDate
	,InterfaceCode
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SiteCode
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,SourceAdminCategoryCode
	,ActivityInCode
	,AdmissionTime
	,AdmissionDate
	,CensusDate
	,InterfaceCode
	,Created = getdate()
	,ByWhom = system_user
from
	ETL.TLoadAPCMidnightBedState


SELECT @RowsInserted = @@Rowcount

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

EXEC Utility.WriteAuditLogEvent 'LoadAPCMidnightBedState', @Stats, @StartTime

print @Stats

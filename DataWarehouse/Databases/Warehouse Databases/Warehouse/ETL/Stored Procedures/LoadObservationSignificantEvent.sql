﻿

CREATE PROCEDURE [ETL].[LoadObservationSignificantEvent]

as

set dateformat dmy



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, LastModifiedTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, LastModifiedTime)) 
from
	ETL.TLoadObservationSignificantEvent

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Observation.SignificantEvent target
using
	(
	select
		 SourceUniqueID
		,CasenoteNumber = cast(CasenoteNumber as varchar(20))
		,DateOfBirth = cast(DateOfBirth as date)
		,SpecialtyID = cast(SpecialtyID as int)
		,LocationID = cast(LocationID as int)
		,AdmissionSourceUniqueID = cast(AdmissionSourceUniqueID as int)
		,SignificantEventTypeID = cast(SignificantEventTypeID as int)
		,StartTime = cast(StartTime as datetime)
		,EndTime = cast(EndTime as datetime)
		,Comment = cast(Comment as varchar(max))
		,CreatedByUserID = cast(CreatedByUserID as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,LastModifiedByUserID = cast(LastModifiedByUserID as int)
		,LastModifiedTime = cast(LastModifiedTime as datetime)
		,InterfaceCode = cast(InterfaceCode as varchar(11))
	from
		ETL.TLoadObservationSignificantEvent

	) source
	on	source.SourceUniqueID = target.SourceUniqueID 

	when not matched by source
	and	target.LastModifiedTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SpecialtyID
			,LocationID
			,AdmissionSourceUniqueID
			,SignificantEventTypeID
			,StartTime
			,EndTime
			,Comment
			,CreatedByUserID
			,CreatedTime
			,LastModifiedByUserID
			,LastModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SpecialtyID
			,source.LocationID
			,source.AdmissionSourceUniqueID
			,source.SignificantEventTypeID
			,source.StartTime
			,source.EndTime
			,source.Comment
			,source.CreatedByUserID
			,source.CreatedTime
			,source.LastModifiedByUserID
			,source.LastModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not 
			isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.AdmissionSourceUniqueID = source.AdmissionSourceUniqueID
			,target.SignificantEventTypeID = source.SignificantEventTypeID
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.Comment = source.Comment
			,target.CreatedByUserID = source.CreatedByUserID
			,target.CreatedTime = source.CreatedTime
			,target.LastModifiedByUserID = source.LastModifiedByUserID
			,target.LastModifiedTime = source.LastModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

﻿
CREATE PROCEDURE [ETL].[LoadResusEncounter]

as

set dateformat ymd

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, coalesce(EventDate, '1 jan 1900'))) 
	,@LoadEndDate = MAX(CONVERT(datetime, coalesce(EventDate, '1 jan 1900'))) 
from
	ETL.TLoadResusEncounter

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action varchar(10));

merge
	Resus.Encounter target
using
	(
	select
		 SourceUniqueID
		,NHSNumber = cast(NHSNumber as varchar(100))
		,CasenoteNumber = cast(CasenoteNumber as varchar(510))
		,PatientForename = cast(PatientForename as varchar(100))
		,PatientSurname = cast(PatientSurname as varchar(100))
		,DateOfBirth = cast(DateOfBirth as date)
		,Sex = cast(Sex as varchar(100))
		,ConsultantCode = cast(ConsultantCode as int)
		,Ethnicity = cast(Ethnicity as varchar(100))
		,PatientGroup = cast(PatientGroup as varchar(100))
		,AdmissionReason = cast(AdmissionReason as varchar(100))
		,AdmissionDate = cast(AdmissionDate as date)
		,EventDate = cast(EventDate as date)
		,LocationCode = cast(LocationCode as varchar(100))
		,Witnessed = cast(Witnessed as int)
		,Monitored = cast(Monitored as int)
		,ALSInterventionNone = cast(ALSInterventionNone as bit)
		,IVAccess = cast(IVAccess as bit)
		,IVMedication = cast(IVMedication as bit)
		,ECGMonitor = cast(ECGMonitor as bit)
		,TrachealTube = cast(TrachealTube as bit)
		,OtherAirwayDevice = cast(OtherAirwayDevice as bit)
		,MechanicalVentilation = cast(MechanicalVentilation as bit)
		,ImplantDefibCardioverter = cast(ImplantDefibCardioverter as bit)
		,IntraArterialCatheter = cast(IntraArterialCatheter as bit)
		,ImmediateCause = cast(ImmediateCause as varchar(100))
		,ImmediateCauseOther = cast(ImmediateCauseOther as varchar(100))
		,ResuscitationAttempt = cast(ResuscitationAttempt as int)
		,Conscious = cast(Conscious as bit)
		,Breathing = cast(Breathing as bit)
		,Pulse = cast(Pulse as bit)
		,PresentingECG = cast(PresentingECG as varchar(100))
		,CPRStoppedTime = cast(CPRStoppedTime as datetime)
		,CPRStoppedReason = cast(CPRStoppedReason as varchar(100))
		,SpontaneousCirculation = cast(SpontaneousCirculation as varchar(100))
		,ROSCTime = cast(ROSCTime as datetime)
		,TimeOfDeath = cast(TimeOfDeath as datetime)
		,UnsustainedROSC = cast(UnsustainedROSC as varchar(100))
		,CollapseTime = cast(CollapseTime as datetime)
		,CPRCalloutTime = cast(CPRCalloutTime as datetime)
		,CPRArriveTime = cast(CPRArriveTime as datetime)
		,ArrestConfirmTime = cast(ArrestConfirmTime as datetime)
		,CPRStartTime = cast(CPRStartTime as datetime)
		,AwakeningTime = cast(AwakeningTime as datetime)
		,AwakeningDate = cast(AwakeningDate as date)
		,DischargeOrDeath = cast(DischargeOrDeath as varchar(100))
		,DischargeDate = cast(DischargeDate as date)
		,DischargeDestination = cast(DischargeDestination as varchar(100))
		,DateOfDeath = cast(DateOfDeath as date)
		,CauseOfDeath = cast(CauseOfDeath as varchar(100))
		,CallOutType = cast(CallOutType as varchar(100))
		,Comment = cast(Comment as varchar(max))
		,ebmB = cast(ebmB as int)
		,ebmB2 = cast(ebmB2 as int)
		,ebmB3 = cast(ebmB3 as int)
		,ebmB4 = cast(ebmB4 as int)
		,ebmB5 = cast(ebmB5 as int)
		,ebmPAI = cast(ebmPAI as int)
		,InterfaceCode = cast(InterfaceCode as varchar(7))
	from
		ETL.TLoadResusEncounter
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	when not matched by source
	and	coalesce(target.EventDate, '1 jan 1900') between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,NHSNumber
			,CasenoteNumber
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,Sex
			,ConsultantCode
			,Ethnicity
			,PatientGroup
			,AdmissionReason
			,AdmissionDate
			,EventDate
			,LocationCode
			,Witnessed
			,Monitored
			,ALSInterventionNone
			,IVAccess
			,IVMedication
			,ECGMonitor
			,TrachealTube
			,OtherAirwayDevice
			,MechanicalVentilation
			,ImplantDefibCardioverter
			,IntraArterialCatheter
			,ImmediateCause
			,ImmediateCauseOther
			,ResuscitationAttempt
			,Conscious
			,Breathing
			,Pulse
			,PresentingECG
			,CPRStoppedTime
			,CPRStoppedReason
			,SpontaneousCirculation
			,ROSCTime
			,TimeOfDeath
			,UnsustainedROSC
			,CollapseTime
			,CPRCalloutTime
			,CPRArriveTime
			,ArrestConfirmTime
			,CPRStartTime
			,AwakeningTime
			,AwakeningDate
			,DischargeOrDeath
			,DischargeDate
			,DischargeDestination
			,DateOfDeath
			,CauseOfDeath
			,CallOutType
			,Comment
			,ebmB
			,ebmB2
			,ebmB3
			,ebmB4
			,ebmB5
			,ebmPAI
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.NHSNumber
			,source.CasenoteNumber
			,source.PatientForename
			,source.PatientSurname
			,source.DateOfBirth
			,source.Sex
			,source.ConsultantCode
			,source.Ethnicity
			,source.PatientGroup
			,source.AdmissionReason
			,source.AdmissionDate
			,source.EventDate
			,source.LocationCode
			,source.Witnessed
			,source.Monitored
			,source.ALSInterventionNone
			,source.IVAccess
			,source.IVMedication
			,source.ECGMonitor
			,source.TrachealTube
			,source.OtherAirwayDevice
			,source.MechanicalVentilation
			,source.ImplantDefibCardioverter
			,source.IntraArterialCatheter
			,source.ImmediateCause
			,source.ImmediateCauseOther
			,source.ResuscitationAttempt
			,source.Conscious
			,source.Breathing
			,source.Pulse
			,source.PresentingECG
			,source.CPRStoppedTime
			,source.CPRStoppedReason
			,source.SpontaneousCirculation
			,source.ROSCTime
			,source.TimeOfDeath
			,source.UnsustainedROSC
			,source.CollapseTime
			,source.CPRCalloutTime
			,source.CPRArriveTime
			,source.ArrestConfirmTime
			,source.CPRStartTime
			,source.AwakeningTime
			,source.AwakeningDate
			,source.DischargeOrDeath
			,source.DischargeDate
			,source.DischargeDestination
			,source.DateOfDeath
			,source.CauseOfDeath
			,source.CallOutType
			,source.Comment
			,source.ebmB
			,source.ebmB2
			,source.ebmB3
			,source.ebmB4
			,source.ebmB5
			,source.ebmPAI
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.Sex, '') = isnull(source.Sex, '')
		and isnull(target.ConsultantCode, 0) = isnull(source.ConsultantCode, 0)
		and isnull(target.Ethnicity, '') = isnull(source.Ethnicity, '')
		and isnull(target.PatientGroup, '') = isnull(source.PatientGroup, '')
		and isnull(target.AdmissionReason, '') = isnull(source.AdmissionReason, '')
		and isnull(target.AdmissionDate, getdate()) = isnull(source.AdmissionDate, getdate())
		and isnull(target.EventDate, getdate()) = isnull(source.EventDate, getdate())
		and isnull(target.LocationCode, '') = isnull(source.LocationCode, '')
		and isnull(target.Witnessed, 0) = isnull(source.Witnessed, 0)
		and isnull(target.Monitored, 0) = isnull(source.Monitored, 0)
		and isnull(target.ALSInterventionNone, 0) = isnull(source.ALSInterventionNone, 0)
		and isnull(target.IVAccess, 0) = isnull(source.IVAccess, 0)
		and isnull(target.IVMedication, 0) = isnull(source.IVMedication, 0)
		and isnull(target.ECGMonitor, 0) = isnull(source.ECGMonitor, 0)
		and isnull(target.TrachealTube, 0) = isnull(source.TrachealTube, 0)
		and isnull(target.OtherAirwayDevice, 0) = isnull(source.OtherAirwayDevice, 0)
		and isnull(target.MechanicalVentilation, 0) = isnull(source.MechanicalVentilation, 0)
		and isnull(target.ImplantDefibCardioverter, 0) = isnull(source.ImplantDefibCardioverter, 0)
		and isnull(target.IntraArterialCatheter, 0) = isnull(source.IntraArterialCatheter, 0)
		and isnull(target.ImmediateCause, '') = isnull(source.ImmediateCause, '')
		and isnull(target.ImmediateCauseOther, '') = isnull(source.ImmediateCauseOther, '')
		and isnull(target.ResuscitationAttempt, 0) = isnull(source.ResuscitationAttempt, 0)
		and isnull(target.Conscious, 0) = isnull(source.Conscious, 0)
		and isnull(target.Breathing, 0) = isnull(source.Breathing, 0)
		and isnull(target.Pulse, 0) = isnull(source.Pulse, 0)
		and isnull(target.PresentingECG, '') = isnull(source.PresentingECG, '')
		and isnull(target.CPRStoppedTime, getdate()) = isnull(source.CPRStoppedTime, getdate())
		and isnull(target.CPRStoppedReason, '') = isnull(source.CPRStoppedReason, '')
		and isnull(target.SpontaneousCirculation, '') = isnull(source.SpontaneousCirculation, '')
		and isnull(target.ROSCTime, getdate()) = isnull(source.ROSCTime, getdate())
		and isnull(target.TimeOfDeath, getdate()) = isnull(source.TimeOfDeath, getdate())
		and isnull(target.UnsustainedROSC, '') = isnull(source.UnsustainedROSC, '')
		and isnull(target.CollapseTime, getdate()) = isnull(source.CollapseTime, getdate())
		and isnull(target.CPRCalloutTime, getdate()) = isnull(source.CPRCalloutTime, getdate())
		and isnull(target.CPRArriveTime, getdate()) = isnull(source.CPRArriveTime, getdate())
		and isnull(target.ArrestConfirmTime, getdate()) = isnull(source.ArrestConfirmTime, getdate())
		and isnull(target.CPRStartTime, getdate()) = isnull(source.CPRStartTime, getdate())
		and isnull(target.AwakeningTime, getdate()) = isnull(source.AwakeningTime, getdate())
		and isnull(target.AwakeningDate, getdate()) = isnull(source.AwakeningDate, getdate())
		and isnull(target.DischargeOrDeath, '') = isnull(source.DischargeOrDeath, '')
		and isnull(target.DischargeDate, getdate()) = isnull(source.DischargeDate, getdate())
		and isnull(target.DischargeDestination, '') = isnull(source.DischargeDestination, '')
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.CauseOfDeath, '') = isnull(source.CauseOfDeath, '')
		and isnull(target.CallOutType, '') = isnull(source.CallOutType, '')
		and isnull(target.Comment, '') = isnull(source.Comment, '')
		and isnull(target.ebmB, 0) = isnull(source.ebmB, 0)
		and isnull(target.ebmB2, 0) = isnull(source.ebmB2, 0)
		and isnull(target.ebmB3, 0) = isnull(source.ebmB3, 0)
		and isnull(target.ebmB4, 0) = isnull(source.ebmB4, 0)
		and isnull(target.ebmB5, 0) = isnull(source.ebmB5, 0)
		and isnull(target.ebmPAI, 0) = isnull(source.ebmPAI, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')


		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.NHSNumber = source.NHSNumber
			,target.CasenoteNumber = source.CasenoteNumber
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.DateOfBirth = source.DateOfBirth
			,target.Sex = source.Sex
			,target.ConsultantCode = source.ConsultantCode
			,target.Ethnicity = source.Ethnicity
			,target.PatientGroup = source.PatientGroup
			,target.AdmissionReason = source.AdmissionReason
			,target.AdmissionDate = source.AdmissionDate
			,target.EventDate = source.EventDate
			,target.LocationCode = source.LocationCode
			,target.Witnessed = source.Witnessed
			,target.Monitored = source.Monitored
			,target.ALSInterventionNone = source.ALSInterventionNone
			,target.IVAccess = source.IVAccess
			,target.IVMedication = source.IVMedication
			,target.ECGMonitor = source.ECGMonitor
			,target.TrachealTube = source.TrachealTube
			,target.OtherAirwayDevice = source.OtherAirwayDevice
			,target.MechanicalVentilation = source.MechanicalVentilation
			,target.ImplantDefibCardioverter = source.ImplantDefibCardioverter
			,target.IntraArterialCatheter = source.IntraArterialCatheter
			,target.ImmediateCause = source.ImmediateCause
			,target.ImmediateCauseOther = source.ImmediateCauseOther
			,target.ResuscitationAttempt = source.ResuscitationAttempt
			,target.Conscious = source.Conscious
			,target.Breathing = source.Breathing
			,target.Pulse = source.Pulse
			,target.PresentingECG = source.PresentingECG
			,target.CPRStoppedTime = source.CPRStoppedTime
			,target.CPRStoppedReason = source.CPRStoppedReason
			,target.SpontaneousCirculation = source.SpontaneousCirculation
			,target.ROSCTime = source.ROSCTime
			,target.TimeOfDeath = source.TimeOfDeath
			,target.UnsustainedROSC = source.UnsustainedROSC
			,target.CollapseTime = source.CollapseTime
			,target.CPRCalloutTime = source.CPRCalloutTime
			,target.CPRArriveTime = source.CPRArriveTime
			,target.ArrestConfirmTime = source.ArrestConfirmTime
			,target.CPRStartTime = source.CPRStartTime
			,target.AwakeningTime = source.AwakeningTime
			,target.AwakeningDate = source.AwakeningDate
			,target.DischargeOrDeath = source.DischargeOrDeath
			,target.DischargeDate = source.DischargeDate
			,target.DischargeDestination = source.DischargeDestination
			,target.DateOfDeath = source.DateOfDeath
			,target.CauseOfDeath = source.CauseOfDeath
			,target.CallOutType = source.CallOutType
			,target.Comment = source.Comment
			,target.ebmB = source.ebmB
			,target.ebmB2 = source.ebmB2
			,target.ebmB3 = source.ebmB3
			,target.ebmB4 = source.ebmB4
			,target.ebmB5 = source.ebmB5
			,target.ebmPAI = source.ebmPAI
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
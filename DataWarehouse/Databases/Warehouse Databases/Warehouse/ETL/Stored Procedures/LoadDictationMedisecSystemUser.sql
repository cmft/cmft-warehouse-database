﻿
CREATE proc ETL.LoadDictationMedisecSystemUser

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()


insert into Dictation.MedisecSystemUser
(
UserCode
,UserName
,UserType
,UserDepartment
)

select
	UserCode = usercode
	,UserName = username
	,UserType = usertype
	,UserDepartment = userdept
from
	[$(CMFT_MEDISEC)].dbo.UserMaster
where
	not exists
	(
	select
		1
	from
		 Dictation.MedisecSystemUser
	where
		MedisecSystemUser.UserCode = UserMaster.usercode collate database_default
	)

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 
			'Rows inserted ' + convert(varchar(10), @RowsInserted) + 
			', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
			
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


﻿
CREATE procedure [ETL].[LoadIncidentIncident] 

as


declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int
declare @FromDate datetime
declare @ToDate datetime

declare
	 @deleted int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

-- Update status to 0
update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSULYSSESINCIDENT'

if(@@rowcount = 0)
insert into Utility.Parameter(Parameter, NumericValue) values('EXTRACTSTATUSULYSSESINCIDENT',0)

--Use date range to delete existing target recs, as no viable key fields on source table


select @FromDate = (select min(IncidentDate)from ETL.TLoadIncidentIncident)
select @ToDate = (select max(IncidentDate)from ETL.TLoadIncidentIncident)

delete from Incident.Incident
where
	IncidentDate between @FromDate and @ToDate

set @deleted = @@rowcount


insert into Incident.Incident
(
	SourceIncidentCode
	,IncidentCode
	,IncidentNumber
	,Description
	,IncidentDate
	,IncidentTime
	,ReportedDate
	,ReportedTime
	,ReceiptDate
	,CauseCode1
	,CauseCode2
	,IncidentTypeCode
	,ContributingFactorCode
	,SignedByCode
	,IncidentGradeCode
	,CompletedByCode
	,SeverityCode
	,LikelihoodCode
	,RiskRatingCode
	,RiskScore
	,SpecialtyCode
	,PatientSafety
	,CauseGroupCode
	,SeriousUntowardIncident
	,ResearchOrTrialProject
	,InitialSeverityCode
	,InitialLikelihoodCode
	,InitialRiskRatingCode
	,InitialRiskScore
	,SecurityIncidentReportingSystemReportable
	,StatusTypeCode
	,NeverEvent
	,OrganisationCode
	,SiteTypeCode
	,SiteCode
	,DivisionCode
	,DirectorateCode
	,LocationCode
	,DepartmentCode
	,WardCode
	,SequenceNo
	,PersonCode
	,PersonTypeCode
	,EntityTypeCode
	,DateOfBirth
	,SexCode
	,CasenoteNumber
	,NHSNumber
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
)
     

select
	SourceIncidentCode
	,IncidentCode
	,IncidentNumber
	,Description
	,IncidentDate
	,IncidentTime
	,ReportedDate
	,ReportedTime
	,ReceiptDate
	,CauseCode1
	,CauseCode2
	,IncidentTypeCode
	,ContributingFactorCode
	,SignedByCode
	,IncidentGradeCode
	,CompletedByCode
	,SeverityCode
	,LikelihoodCode
	,RiskRatingCode
	,RiskScore
	,SpecialtyCode
	,PatientSafety
	,CauseGroupCode
	,SeriousUntowardIncident
	,ResearchOrTrialProject
	,InitialSeverityCode
	,InitialLikelihoodCode
	,InitialRiskRatingCode
	,InitialRiskScore
	,SecurityIncidentReportingSystemReportable
	,StatusTypeCode
	,NeverEvent
	,OrganisationCode
	,SiteTypeCode
	,SiteCode
	,DivisionCode
	,DirectorateCode
	,LocationCode
	,DepartmentCode
	,WardCode
	,SequenceNo
	,PersonCode
	,PersonTypeCode
	,EntityTypeCode
	,DateOfBirth
	,SexCode
	,CasenoteNumber
	,NHSNumber
	,InterfaceCode
	,getdate()
	,getdate()
	,system_user
from
	ETL.TLoadIncidentIncident

set @inserted = @@rowcount


-- Log run date
update Utility.Parameter set DateValue = getdate() where Parameter = 'LOADULYSSESINCIDENTDATE'
if(@@rowcount = 0)
insert into Utility.Parameter(Parameter, DateValue) values('LOADULYSSESINCIDENTDATE',getdate())


-- Calc elapsed time
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


-- Set stats and log them
select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
		', Period from ' + CONVERT(varchar(11), @FromDate) + ' to ' + CONVERT(varchar(11), @ToDate)

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime





﻿
create proc ETL.ExtractDictationDocumentInformation

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table ETL.TImportDictationDocumentInformation

insert into ETL.TImportDictationDocumentInformation

select
	[DocumentID]
	,[Running]
	,[DictationTime]
	,[CorrectionTime]
	,[PassThroughTime]
	,[SmSoundQualityAcceptable]
	,[SmClippingRatio]
	,[SmSignalLevel]
	,[SmSignalToNoiseRatio]
from
	[$(G2_G2Speech5)].[dbo].[G2DocumentInfo]
	
select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


﻿
CREATE proc [ETL].[LoadUlyssesReferenceData]

as

--incident

truncate table Ulysses.IncidentCauseBase

insert into Ulysses.IncidentCauseBase

(
	CODE
	,BinaryCode
	,DESCRIPT
	,VIOLENCE
	,HSE_NUMBER
	,CAUSE_GRP
	,SUMMARY
	,INACTIVE
	,ABBREVIATE
	,NPSA1
	,CORP_CAT
	,MED_ERROR
	,MISS_PERS
	,PSI
	,SUI
	,ABBREVIAT2
	,ABBREVIAT3
	,NEVER_EVT
	,CLIN_ASSLT
	,SIRS7
	,SIRS8
	,NPSA2
	,DAYS_RCA
)

select
	CODE
	,cast(CODE as varbinary (100))
	,DESCRIPT
	,VIOLENCE
	,HSE_NUMBER
	,CAUSE_GRP
	,SUMMARY
	,INACTIVE
	,ABBREVIATE
	,NPSA1
	,CORP_CAT
	,MED_ERROR
	,MISS_PERS
	,PSI
	,SUI
	,ABBREVIAT2
	,ABBREVIAT3
	,NEVER_EVT
	,CLIN_ASSLT
	,SIRS7
	,SIRS8
	,NPSA2
	,DAYS_RCA
from
	[$(Ulysses)].Incident.CAUSE


truncate table Ulysses.IncidentCauseGroupBase

insert into Ulysses.IncidentCauseGroupBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[INC_TYPE]
)


select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DESCRIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[INC_TYPE]
from
	[$(Ulysses)].[Incident].[CAUSEGRP]



truncate table Ulysses.IncidentGradeBase

insert into Ulysses.IncidentGradeBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[ABBREVIATE]
	,[INACTIVE]
	,[COLOUR]
	,[NPSA2]
	,[NPSA3]
	,[NPSA4]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[INFORM_REL]
)

select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DESCRIPT]
	,[ABBREVIATE]
	,[INACTIVE]
	,[COLOUR]
	,[NPSA2]
	,[NPSA3]
	,[NPSA4]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[INFORM_REL]
from
	[$(Ulysses)].[Incident].[GRADETYP]


truncate table Ulysses.IncidentTypeBase

insert into Ulysses.IncidentTypeBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
)

select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DESCRIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
from
	[$(Ulysses)].[Incident].[INCTYPE]


truncate table Ulysses.IncidentSeverityBase

insert into Ulysses.IncidentSeverityBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[SCORE]
	,[ABBREVIATE]
	,[INACTIVE]
	,[MATRIX]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
)
select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DESCRIPT]
	,[SCORE]
	,[ABBREVIATE]
	,[INACTIVE]
	,[MATRIX]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
from
	[$(Ulysses)].[Summary].[Severity]



--Complaint



truncate table Ulysses.ComplaintGradeBase

insert into Ulysses.ComplaintGradeBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[EXCLD_KO41]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
)

select	
	[CODE]
	,BinaryCode = cast([CODE] as varbinary(100))
	,[DESCRIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[EXCLD_KO41]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
from
	[$(Ulysses)].[Complain].[SEVERTYP]



truncate table Ulysses.ComplaintCaseTypeBase

insert into Ulysses.ComplaintCaseTypeBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[DAYS_RSP]
	,[INACTIVE]
	,[ABBREVIATE]
	,[KO41_TYPE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[DAYS_RSP2]
	,[DAYS_ROPN1]
	,[DAYS_ROPN2]
	,[DAYS_RSP3]
	,[PREFIX]
)

select 
	[CODE]
	,BinaryCode = cast([CODE] as varbinary(100))
	,[DESCRIPT]
	,[DAYS_RSP]
	,[INACTIVE]
	,[ABBREVIATE]
	,[KO41_TYPE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[DAYS_RSP2]
	,[DAYS_ROPN1]
	,[DAYS_ROPN2]
	,[DAYS_RSP3]
	,[PREFIX]
from
	[$(Ulysses)].[Complain].[CASETYPE]


truncate table Ulysses.ComplaintCategoryBase

insert into Ulysses.ComplaintCategoryBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[TYPE]
	,[KO41]
	,[INACTIVE]
	,[ABBREVIATE]
	,[KO41B]
	,[CORP_CAT]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
)

select 
	[CODE]
	,BinaryCode = cast([CODE] as varbinary(100))
	,[DESCRIPT]
	,[TYPE]
	,[KO41]
	,[INACTIVE]
	,[ABBREVIATE]
	,[KO41B]
	,[CORP_CAT]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
from
	[$(Ulysses)].[Complain].[COMPCAT]


truncate table  Ulysses.ComplaintCategoryTypeBase

insert into Ulysses.ComplaintCategoryTypeBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[KO41]
	,[INACTIVE]
	,[ABBREVIATE]
	,[KO41B]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
)


select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DESCRIPT]
	,[KO41]
	,[INACTIVE]
	,[ABBREVIATE]
	,[KO41B]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
from
	[$(Ulysses)].[Complain].[CATTYPE]



--summary

truncate table Ulysses.DepartmentBase

insert into Ulysses.DepartmentBase

(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[SITE]
	,[DIRECTORAT]
	,[FUNCTION]
	,[NUMBER_ST]
	,[INACTIVE]
	,[ABBREVIATE]
	,[DIVISION]
	,[NPSA1]
	,[NPSA2]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[LOCALITY]
	,[LOCALITY2]
	,[LOCALITY3]
)


select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DESCRIPT]
	,[SITE]
	,[DIRECTORAT]
	,[FUNCTION]
	,[NUMBER_ST]
	,[INACTIVE]
	,[ABBREVIATE]
	,[DIVISION]
	,[NPSA1]
	,[NPSA2]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[LOCALITY]
	,[LOCALITY2]
	,[LOCALITY3]
from
	[$(Ulysses)].[Summary].[DEPARTS]

truncate table Ulysses.DirectorateBase

insert into Ulysses.DirectorateBase

(
	[CODE]
	,[BinaryCode]
	,[DIRECTORAT]
	,[NUMBER_ST]
	,[GROUP]
	,[INACTIVE]
	,[ABBREVIATE]
	,[EXCLD_KO41]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[SITE_LOC]
)

select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DIRECTORAT]
	,[NUMBER_ST]
	,[GROUP]
	,[INACTIVE]
	,[ABBREVIATE]
	,[EXCLD_KO41]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
	,[SITE_LOC]
from
	[$(Ulysses)].[Summary].[DIRECTOR]


truncate table Ulysses.DivisionBase

insert into Ulysses.DivisionBase
(
	[CODE]
	,[BinaryCode]
	,[DESCRIPT]
	,[ABBREVIATE]
	,[INACTIVE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
)

select
	[CODE]
	,cast(CODE as varbinary (100))
	,[DESCRIPT]
	,[ABBREVIATE]
	,[INACTIVE]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
from
	[$(Ulysses)].[Summary].[DEPTDIV]


truncate table Ulysses.SiteBase

insert into Ulysses.SiteBase
(
	[CODE]
	,[BinaryCode]
	,[SITE]
	,[AUTHORITY]
	,[SITE_TYPE]
	,[NUMBER_ST]
	,[RECEIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[SITE_LOC]
	,[ADDRESS]
	,[EXCLD_KO41]
	,[ORGANISE]
	,[NPSA1]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
)

select 
	[CODE]
	,BinaryCode = cast([CODE] as varbinary(100))
	,[SITE]
	,[AUTHORITY]
	,[SITE_TYPE]
	,[NUMBER_ST]
	,[RECEIPT]
	,[INACTIVE]
	,[ABBREVIATE]
	,[SITE_LOC]
	,[ADDRESS]
	,[EXCLD_KO41]
	,[ORGANISE]
	,[NPSA1]
	,[ABBREVIAT2]
	,[ABBREVIAT3]
from
	[$(Ulysses)].[Summary].[SITE]

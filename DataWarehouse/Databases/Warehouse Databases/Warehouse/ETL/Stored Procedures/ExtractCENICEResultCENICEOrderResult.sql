﻿

CREATE proc [ETL].[ExtractCENICEResultCENICEOrderResult] 

(
@StartDate date
,@EndDate date
) 

as

set dateformat ymd


declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

insert into ETL.TImportResultCENICEOrderResult
(
OrderSourceUniqueID
,OrderRequestTime
,ResultSourceUniqueID
)

select --top 10 
	OrderSourceUniqueID
	,OrderRequestTime
	,ResultSourceUniqueID
from
	(
	select
		OrderSourceUniqueID = OrderSummary.Service_Request_Index
		,OrderRequestTime = OrderSummary.Date_Last_Edited 
		,ResultSourceUniqueID = ReportResults.Result_Index
		
	from
		[$(ICE_Central)].dbo.ICEView_ReportSummary ReportSummary 

	inner join [$(ICE_Central)].dbo.ICEView_ReportInvestigations ReportInvestigations 
	on ReportInvestigations.Service_Report_Index = ReportSummary.Service_Report_Index

	inner join [$(ICE_Central)].dbo.ICEView_ReportResults ReportResults 
	on	ReportResults.Investigation_Index = ReportInvestigations.Investigation_Index 
	and ReportResults.Sample_Index = ReportInvestigations.Sample_Index

	inner join [$(ICE_Central)].dbo.ICEView_OrderSummary OrderSummary
	on OrderSummary.Service_Request_Index = ReportSummary.Service_Request_Index

	where
		cast(OrderSummary.Date_Last_Edited as date) between @StartDate and @EndDate

	) OrderResult

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


	


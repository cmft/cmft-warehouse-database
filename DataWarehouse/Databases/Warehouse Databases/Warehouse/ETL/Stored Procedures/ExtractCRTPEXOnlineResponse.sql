﻿


CREATE proc [ETL].[ExtractCRTPEXOnlineResponse]

 @FromDate smalldatetime = null
, @ToDate smalldatetime = null


as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
select @StartTime = getdate()

select @RowsInserted = 0

--select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -12, getdate()))), 0), 112) 

--select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

truncate table ETL.TImportPEXOnlineResponse

insert into	ETL.TImportPEXOnlineResponse

(
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
)

select
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
from
	(
	select
		SurveyTakenID = SurveyTaken.ID
		,SurveyID = SurveyTaken.SurveyID
		,QuestionID = SurveyQuestions.QuestionID
		,AnswerID = SurveyAnswers.SurveyAnswerID
		,Answer = SurveyAnswers.AnswerText
		,SurveyDate = cast(SurveyTaken.DateTaken as date)
		,SurveyTime = SurveyTaken.DateTaken
		,LocationID = ResponseLocationID
		,DeviceNumber = SurveyTaken.DeviceNum
		,ChannelID
		,QuestionTypeID = QuestionType.XrefEntityCode
		,LocationTypeID
	from
		(
		select 
			 ID = SurveyTaken.id
			,SurveyTaken.SurveyID
			,SurveyTaken.DateTaken
			,SurveyTaken.DeviceNum
			,SurveyTaken.LocationID
			,ChannelID = cast(Channel.XrefEntityCode as int)
		from 
			[$(PatientExperience)].PEX.SurveyTaken
			
		inner join [dbo].[EntityXref] Channel
		on	Channel.EntityCode = SurveyTaken.SurveyID
		and Channel.EntityTypeCode = 'PEXSURVEY'	
		and Channel.XrefEntityCode = '4'	--Online
		
		) SurveyTaken
      
    inner join [$(PatientExperience)].PEX.SurveyQuestions
	on	SurveyTaken.SurveyID = SurveyQuestions.SurveyID 

	inner join [$(PatientExperience)].PEX.SurveyAnswers
	on	SurveyAnswers.SurveyTakenID = SurveyTaken.ID
	and	SurveyAnswers.SurveyQuestionID = SurveyQuestions.ID
	and SurveyAnswers.SurveyAnswerID <> '-1' --No Response (not actually a response)
					                   
	inner join 
		(
			select
				ResponseLocationID = LocationType.EntityXrefRecno --Recno used because location reference will be a combination of locations and selected location
				,SurveyAnswers.SurveyTakenID
				,LocationTypeID = LocationType.XrefEntityCode
			from	
				[$(PatientExperience)].PEX.SurveyAnswers	
			
			inner join [$(PatientExperience)].PEX.SurveyQuestions
			on	SurveyAnswers.SurveyQuestionID = SurveyQuestions.ID
				
			inner join dbo.EntityXref ResponseLocation
			on	ResponseLocation.EntityCode = SurveyQuestions.QuestionID
			and ResponseLocation.EntityTypeCode = 'PEXQUESTION'	
			and ResponseLocation.XrefEntityCode = '1' --Location Selector
			
			inner join EntityXref LocationType
			on	LocationType.EntityCode = SurveyAnswers.SurveyAnswerID
			and LocationType.EntityTypeCode = 'PEXLOCATIONSELECTED'
		
		) ResponseLocation
	
	on	SurveyTaken.ID = ResponseLocation.SurveyTakenID

	left join dbo.EntityXref QuestionType
	on	QuestionType.EntityCode = SurveyQuestions.QuestionID
	and QuestionType.EntityTypeCode = 'PEXQUESTION'
		
	)Online

	where
		SurveyDate between @FromDate and @ToDate
	
select @RowsInserted = @@rowcount

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime







﻿




CREATE proc [ETL].[LoadCHAMPContactUsRequest] as

/*
When		Who			What
=========================================================================================
23/02/2015	Paul Egan	Initial Coding
=========================================================================================
*/


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
;

declare
	 @deleted int
	,@inserted int
	,@updated int
;

declare
	@MergeSummary TABLE(Action nvarchar(10))
;


merge
	COM.CHAMPContactUsRequest target
using
	(
	select 
		CHAMPContactUsRequestID
		,ContactUsRequestTime
		,EmailAddressTo
		,EmailAddressFrom
		,EmailFromName
		,EmailChildName
		,EmailRelationshipToChild
		,EmailMessage
		,EmailFullContent
	from 
		ETL.TLoadCHAMPContactUsRequest
	) source
	on	source.CHAMPContactUsRequestID = target.CHAMPContactUsRequestID

	when not matched by source
	
	then delete
	
	when not matched
	then
		insert
			(
			 CHAMPContactUsRequestID
			,ContactUsRequestTime
			,EmailAddressTo
			,EmailAddressFrom
			,EmailFromName
			,EmailChildName
			,EmailRelationshipToChild
			,EmailMessage
			,EmailFullContent
			,Created
			,Updated
			,ByWhom
           )
		values
			(
			source.CHAMPContactUsRequestID
			,source.ContactUsRequestTime
			,source.EmailAddressTo
			,source.EmailAddressFrom
			,source.EmailFromName
			,source.EmailChildName
			,source.EmailRelationshipToChild
			,source.EmailMessage
			,source.EmailFullContent
			,getdate()
			,getdate()
			,suser_name()
		)
		
	when matched
	and not
		(
			isnull(target.ContactUsRequestTime, '19000101') = isnull(source.ContactUsRequestTime, '19000101')
		and isnull(target.EmailAddressTo, '') = isnull(source.EmailAddressTo, '')
		and isnull(target.EmailAddressFrom, '') = isnull(source.EmailAddressFrom, '')
		and isnull(target.EmailFromName, '') = isnull(source.EmailFromName, '')
		and isnull(target.EmailChildName, '') = isnull(source.EmailChildName, '')
		and isnull(target.EmailRelationshipToChild, '') = isnull(source.EmailRelationshipToChild, '')
		and isnull(target.EmailMessage, '') = isnull(source.EmailMessage, '')
		and isnull(target.EmailFullContent, '') = isnull(source.EmailFullContent, '')

		)
	then
		update
		set
			target.ContactUsRequestTime = source.ContactUsRequestTime
			,target.EmailAddressTo = source.EmailAddressTo
			,target.EmailAddressFrom = source.EmailAddressFrom
			,target.EmailFromName = source.EmailFromName
			,target.EmailChildName = source.EmailChildName
			,target.EmailRelationshipToChild = source.EmailRelationshipToChild
			,target.EmailMessage = source.EmailMessage
			,target.EmailFullContent = source.EmailFullContent
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime






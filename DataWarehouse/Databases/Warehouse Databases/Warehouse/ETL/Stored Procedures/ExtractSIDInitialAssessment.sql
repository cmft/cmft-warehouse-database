﻿CREATE procedure [ETL].[ExtractSIDInitialAssessment]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDInitialAssessment;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDInitialAssessment
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,TypeOfPresentation
	,PreviousHistoryOfStroke
	,ReferralForAssessmentSent
	,DateTimeReferralForAssessmentSent
	,ReasonWhyNotAssessedWithin72Hours
	,DateTimeOfAssessment
	,LocationAtPointOfAssessment
	,DateTimeOfOnsetStrokeSymptoms
	,DateOfOnsetStrokeSymptomsIs
	,TimeOfOnsetStrokeSymptomsIs
	,ArrivedByAmbulance
	,TransferFrom
	,DateTimeOfAttendedED
	,InpatientStroke
	,[FAST]
	,SymptomsFullyResolved
	,ROSIERDTOLossOfConciousnessOrSyncope
	,ROSIERDTOSeizureActivity
	,ROSIERDTOAsymmetricFacialWeakness
	,ROSIERDTOAsymmetricArmWeakness
	,ROSIERDTOAsymmetricLegWeakness
	,ROSIERDTOSpeechDisturbance
	,ROSIERDTOVisualFieldDefect
	,ROSIERDTOTotalScore
	,NIHSSScore
	,WasNIHSSToolCompleted
	,NIHSSDTOLevelOfConsciousness
	,NIHSSDTOLOCQuestions
	,NIHSSDTOLOCCommands
	,NIHSSDTOBestGaze
	,NIHSSDTOVisual
	,NIHSSDTOFacialPalsy
	,NIHSSDTOMotorArmLeft
	,NIHSSDTOMotorArmRight
	,NIHSSDTOMotorLegLeft
	,NIHSSDTOMotorLegRight
	,NIHSSDTOLimbAtaxia
	,NIHSSDTOSensory
	,NIHSSDTOBestLanguage
	,NIHSSDTODysarthria
	,NIHSSDTOExtinctionAndInattention
	,NIHSSDTOTotalScore
	,IANotes
	,Username
	,ModifiedDate
	,IsSectionComplete
	,SectionCompleteTime
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		CandidateData.SectionID
	,ActionID = 
		CandidateData.ActionID
	,TypeOfPresentation = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/TypeOfPresentation)[1]', 'varchar(max)'),'')
	,PreviousHistoryOfStroke = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/PreviousHistoryOfStroke)[1]', 'varchar(max)'),'')
	,ReferralForAssessmentSent = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ReferralForAssessmentSent)[1]', 'varchar(max)'),'')
	,DateTimeReferralForAssessmentSent = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/InitialAssessmentDTO/DateTimeReferralForAssessmentSent)[1]', 'varchar(max)'),'T',' ' )),'')
	,ReasonWhyNotAssessedWithin72Hours = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ReasonWhyNotAssessedWithin72Hours)[1]', 'varchar(max)'),'')
	,DateTimeOfAssessment = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/InitialAssessmentDTO/DateTimeOfAssessment)[1]', 'varchar(max)'),'T',' ' )),'')
	,LocationAtPointOfAssessment = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/LocationAtPointOfAssessment)[1]', 'varchar(max)'),'')
	,DateTimeOfOnsetStrokeSymptoms = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/InitialAssessmentDTO/DateTimeOfOnsetStrokeSymptoms)[1]', 'varchar(max)'),'T',' ' )),'')
	,DateOfOnsetStrokeSymptomsIs = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/DateOfOnsetStrokeSymptomsIs)[1]', 'varchar(max)'),'')
	,TimeOfOnsetStrokeSymptomsIs = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/TimeOfOnsetStrokeSymptomsIs)[1]', 'varchar(max)'),'')
	,ArrivedByAmbulance = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ArrivedByAmbulance)[1]', 'varchar(max)'),'')
	,TransferFrom = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/TransferFrom)[1]', 'varchar(max)'),'')
	,DateTimeOfAttendedED = 
		case 
		when convert(datetime,replace(CandidateData.Details.value('(/InitialAssessmentDTO/DateTimeOfAttendedED)[1]', 'varchar(max)'),'T',' ' )) = '1900-01-01 00:00:00.000' 
			then null
		else convert(datetime,replace(CandidateData.Details.value('(/InitialAssessmentDTO/DateTimeOfAttendedED)[1]', 'varchar(max)'),'T',' ' )) 
		end
	,InpatientStroke = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/InpatientStroke)[1]', 'varchar(max)'),'')
	,[FAST] = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/FAST)[1]', 'varchar(max)'),'')
	,SymptomsFullyResolved = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/SymptomsFullyResolved)[1]', 'varchar(max)'),'')
	,ROSIERDTOLossOfConciousnessOrSyncope = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/LossOfConciousnessOrSyncope)[1]', 'varchar(max)'),'')
	,ROSIERDTOSeizureActivity = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/SeizureActivity)[1]', 'varchar(max)'),'')
	,ROSIERDTOAsymmetricFacialWeakness = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/AsymmetricFacialWeakness)[1]', 'varchar(max)'),'')
	,ROSIERDTOAsymmetricArmWeakness = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/AsymmetricArmWeakness)[1]', 'varchar(max)'),'')
	,ROSIERDTOAsymmetricLegWeakness = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/AsymmetricLegWeakness)[1]', 'varchar(max)'),'')
	,ROSIERDTOSpeechDisturbance = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/SpeechDisturbance)[1]', 'varchar(max)'),'')
	,ROSIERDTOVisualFieldDefect = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/VisualFieldDefect)[1]', 'varchar(max)'),'')
	,ROSIERDTOTotalScore = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/ROSIER/TotalScore)[1]', 'varchar(max)'),'')
	,NIHSSScore = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSSScore)[1]', 'varchar(max)'),'')
	,WasNIHSSToolCompleted = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/WasNIHSSToolCompleted)[1]', 'varchar(max)'),'')
	,NIHSSDTOLevelOfConsciousness = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/LevelOfConsciousness)[1]', 'varchar(max)'),'')
	,NIHSSDTOLOCQuestions = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/LOCQuestions)[1]', 'varchar(max)'),'')
	,NIHSSDTOLOCCommands = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/LOCCommands)[1]', 'varchar(max)'),'')
	,NIHSSDTOBestGaze = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/BestGaze)[1]', 'varchar(max)'),'')
	,NIHSSDTOVisual = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/Visual)[1]', 'varchar(max)'),'')
	,NIHSSDTOFacialPalsy = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/FacialPalsy)[1]', 'varchar(max)'),'')
	,NIHSSDTOMotorArmLeft = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/MotorArmLeft)[1]', 'varchar(max)'),'')
	,NIHSSDTOMotorArmRight = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/MotorArmRight)[1]', 'varchar(max)'),'')
	,NIHSSDTOMotorLegLeft = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/MotorLegLeft)[1]', 'varchar(max)'),'')
	,NIHSSDTOMotorLegRight = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/MotorLegRight)[1]', 'varchar(max)'),'')
	,NIHSSDTOLimbAtaxia = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/LimbAtaxia)[1]', 'varchar(max)'),'')
	,NIHSSDTOSensory = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/Sensory)[1]', 'varchar(max)'),'')
	,NIHSSDTOBestLanguage = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/BestLanguage)[1]', 'varchar(max)'),'')
	,NIHSSDTODysarthria = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/Dysarthria)[1]', 'varchar(max)'),'')
	,NIHSSDTOExtinctionAndInattention = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/ExtinctionAndInattention)[1]', 'varchar(max)'),'')
	,NIHSSDTOTotalScore = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/NIHSS/TotalScore)[1]', 'varchar(max)'),'')
	,IANotes = 
		nullif(CandidateData.Details.value('(/InitialAssessmentDTO/IANotes)[1]', 'varchar(max)'),'')
	,Username = 
		CandidateData.Details.value('(/InitialAssessmentDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/InitialAssessmentDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,IsSectionComplete = 
		SectionComplete.Details.value('(/SectionCompletedDTO/CurrentSelection)[1]', 'varchar(max)')
	,SectionCompleteTime = 
		SectionComplete.DateTimeCreated
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
left outer join
(
	select
		*
	from
		[$(SID)].dbo.TARNCandidateData 
	where
		SectionID = 14
	and ActionID = 50
) SectionComplete
on SectionComplete.CandidateID = CandidateData.CandidateID
where
	CandidateData.SectionID = 14
and CandidateData.ActionID =99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDInitialAssessment', @Stats, @StartTime





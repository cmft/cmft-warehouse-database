﻿

CREATE proc [ETL].[ExtractInquireAPCAugmentedCarePeriod]

	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @Sproc varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

truncate table ETL.TImportAPCAugmentedCarePeriod

insert into ETL.TImportAPCAugmentedCarePeriod
(
	SourceUniqueID 
	,SourcePatientNo 
	,SourceSpellNo 
	,ConsultantCode 
	,AcpDisposalCode 
	,LocalIdentifier 
	,LocationCode 
	,OutcomeIndicator 
	,Source 
	,SpecialtyCode 
	,Status
	,StartDate 
	,StartTime 
	,EndDate 
	,EndTime 
	,AdvancedRespiratorySystemIndicator 
	,BasicRespiratorySystemIndicator 
	,CirculatorySystemIndicator 
	,NeurologicalSystemIndicator 
	,RenalSystemIndicator 
	,NoOfSupportSystemsUsed 
	,HighDependencyCareLevelDays 
	,IntensiveCareLevelDays 
	,PlannedAcpPeriodIndicator 
	,ReviseByUserId
	,ReviseTime
)
select
	AUGCAREPATDETID
	,InternalPatientNumber
	,EpisodeNumber
	,AcpConsultantCode
	,AcpDisposal
	,AcpLocalIdentifier
	,AcpLocation
	,AcpOutcomeIndicator
	,AcpSource
	,AcpSpecialtyCode
	,AcpStatus
	,AugmentedCarePeriodStartDate
	,AugmentedCarePeriodStartTime
	,AugmentedCarePeriodEndDate

	,AugmentedCarePeriodEndTime
	,AdvancedRespiratorySystem
	,BasicRespiratorySystem
	,CirculatorySystem
	,NeurologicalSystem
	,RenalSystem
	,NoOfSupportSystemsUsed
	,HighDependencyCareLevelDays
	,IntensiveCareLevelDays
	,PlannedAcpPeriod
	,ApcAugmentedCarePeriodCreatereviseByUserId
	,ApcAugmentedCarePeriodCreatereviseDateExt
	
from
	[$(PAS)].Inquire.AUGCAREPATDET
--where
--	ApcAugmentedCarePeriodStartDatetimeInt between @from and @to


select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec Utility.WriteAuditLogEvent 'ExtractInquireAPCAugmentedCarePeriod', @Stats, @StartTime



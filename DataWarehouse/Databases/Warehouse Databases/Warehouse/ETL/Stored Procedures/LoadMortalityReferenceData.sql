﻿
CREATE Procedure [ETL].[LoadMortalityReferenceData]

as 

Insert into	Mortality.SystemUserBase
	(
	SystemUserCode
	,SystemUser
	,DomainLogin
	,Email
	,Active
	)
select
	SystemUserCode = UserID
	,SystemUser = Name
	,DomainLogin
	,Email
	,Active
from 
	[$(MortalityReview)].dbo.Users
where
	not exists
		(
		Select
			1
		from
			Mortality.SystemUserBase Present
		where
			Present.SystemUserCode = Users.UserID
		)



Insert into	Mortality.ReviewStatusBase
	(
	ReviewStatusCode
	,ReviewStatus
	)
Select distinct
	ReviewStatusCode =
		case 
			when FormData.[Status] = 'Primary Assigned' then 1
			when FormData.[Status] = 'Primary Completed' then 2
			when FormData.[Status] = 'Form Completed' then 3
			when FormData.[Status] = 'Review Not Needed' then 4
			else 99
		end
	,ReviewStatus = FormData.[Status]
from
	[$(MortalityReview)].dbo.FormData
where
	not exists
		(
		Select
			1
		from
			Mortality.ReviewStatusBase Present
		where
			Present.ReviewStatusCode = case 
											when FormData.[Status] = 'Primary Assigned' then 1
											when FormData.[Status] = 'Primary Completed' then 2
											when FormData.[Status] = 'Form Completed' then 3
											when FormData.[Status] = 'Review Not Needed' then 4
											else 99
										end
		)
		
		
Insert into Mortality.FormTypeBase
	(
	FormTypeCode
	,FormType
	,Category
	)
select
	FormTypeCode = FormTypeID
	,FormType = Name
	,Category = 
		case FormTypeID
			when 1 then 'Adult'			-- Acute Medicine
			when 2 then 'Neonatal'		-- Neonatal (I just used 'Other' for now - may need changing later)
			when 3 then 'Adult'			-- Specialist Medicine
			when 4 then 'Adult'			-- Trafford
			when 5 then 'Adult'			-- Gynaecology 
			when 6 then 'Adult'			-- Clinical and Scientific Support
			when 7 then 'Other'			-- Childrens (I just used 'Other' for now - may need changing later)
			when 8 then 'Adult'			-- Surgery
			when 9 then 'Other'			-- Still Birth (I just used 'Other' for now - may need changing later)
		else 'Unknown'
	end
from 
	[$(MortalityReview)].dbo.FormType
where
	not exists
		(
		select
			1
		from 
			Mortality.FormTypeBase Present
		where
			Present.FormTypeCode = FormType.FormTypeID
		)

﻿CREATE procedure [ETL].[LoadBedmanEvent] 
	 @from datetime = '19000101'
	,@to datetime = '21000101'
as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()


-- Update status to 0
update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSBEDMANEVENT'
if(@@rowcount = 0)
	insert into Utility.Parameter(Parameter, NumericValue) values('EXTRACTSTATUSBEDMANEVENT',0)

-- Load up staging table
exec ETL.ExtractBedManEvent @from, @to

-- If status now = 1 ...
if (
	select NumericValue from Utility.Parameter
	where Parameter = 'EXTRACTSTATUSBEDMANEVENT'
	) = 1
begin


-- Log run date
update Utility.Parameter set DateValue = getdate() where Parameter = 'LOADBEDMANEVENTDATE'
if(@@rowcount = 0)
	insert into Utility.Parameter(Parameter, DateValue) values('LOADBEDMANEVENTDATE',getdate())


declare @MergeSummary TABLE(Action nvarchar(10));

merge
	Bedman.Event target

using
	ETL.TLoadBedManEvent source

on	source.SourceUniqueID = target.SourceUniqueID

when
	matched and not
		(
			isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.EventTypeCode, '') = isnull(source.EventTypeCode, '')
		and isnull(target.EventDate, getdate()) = isnull(source.EventDate, getdate())
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.DischargeTime, getdate()) = isnull(source.DischargeTime, getdate())
		and cast(isnull(target.EventDetails, '') as varchar(max)) = cast(isnull(source.EventDetails, '') as varchar(max))
		)
then
	update
		set
			 target.SourceSpellNo = source.SourceSpellNo
			,target.EventTypeCode = source.EventTypeCode
			,target.EventDate = source.EventDate
			,target.SourcePatientNo = source.SourcePatientNo
			,target.AdmissionTime = source.AdmissionTime
			,target.DischargeTime = source.DischargeTime
			,target.EventDetails = source.EventDetails
			,target.Updated = getdate()
			,target.ByWhom = system_user

when not matched by source
then
	delete

when not matched by target
then
	insert
		(
		 SourceUniqueID
		,SourceSpellNo
		,EventTypeCode
		,EventDate
		,EventDetails
		,SourcePatientNo
		,AdmissionTime
		,DischargeTime
		,Created
		,Updated
		,ByWhom
		)
	values
		(
		 source.SourceUniqueID
		,source.SourceSpellNo
		,source.EventTypeCode
		,source.EventDate
		,source.EventDetails
		,source.SourcePatientNo
		,source.AdmissionTime
		,source.DischargeTime
		,getdate()
		,getdate()
		,system_user
		)

OUTPUT $Action
INTO
	@MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats

end

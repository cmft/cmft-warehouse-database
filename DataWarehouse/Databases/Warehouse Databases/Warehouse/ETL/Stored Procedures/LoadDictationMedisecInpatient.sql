﻿

CREATE PROCEDURE [ETL].[LoadDictationMedisecInpatient]

as

set dateformat dmy



declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, AdmissionTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, AdmissionTime)) 
from
	ETL.TLoadDictationMedisecInpatient

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Dictation.MedisecInpatient target
using
	(
	select
		SourcePatientNo =  cast(SourcePatientNo as varchar(max))
		,AdmissionTime = cast(AdmissionTime as datetime)
		,RequestTypeCode = cast(RequestTypeCode as varchar(max))
		,SpellSequenceNo 
		,InterfaceCode = cast(InterfaceCode as varchar(max))
		,ContextCode = cast(ContextCode as varchar(max))
	from
		ETL.TLoadDictationMedisecInpatient
	) source
	on	source.SourcePatientNo = target.SourcePatientNo
	and source.AdmissionTime = target.AdmissionTime
	and source.SpellSequenceNo = target.SpellSequenceNo	

	when not matched by source
	and	target.AdmissionTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourcePatientNo
			,AdmissionTime
			,SpellSequenceNo
			,RequestTypeCode
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.SourcePatientNo
			,source.AdmissionTime
			,source.SpellSequenceNo
			,source.RequestTypeCode
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.SpellSequenceNo, '') = isnull(source.SpellSequenceNo, '')
		and isnull(target.RequestTypeCode, '') = isnull(source.RequestTypeCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.SourcePatientNo = source.SourcePatientNo
			,target.AdmissionTime = source.AdmissionTime
			,target.SpellSequenceNo = source.SpellSequenceNo
			,target.RequestTypeCode = source.RequestTypeCode
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

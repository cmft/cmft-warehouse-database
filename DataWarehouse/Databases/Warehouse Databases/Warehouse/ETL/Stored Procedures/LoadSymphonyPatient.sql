﻿
CREATE PROCEDURE [ETL].[LoadSymphonyPatient]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Attenance table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 PatientID
			,Surname
			,TitleID
			,Forename
			,MiddleNames
			,SexID
			,DateOfBirth
			,IsTemporaryRegistration
			--,LastAttendanceNumber
			,CreatedByID
			--,UpdatedTime
			,IsDead

			)
into
	#TLoadSymphonyPatient
from
	(
	select
		 PatientID
		,Surname = cast(Surname as varchar(35))
		,TitleID = cast(TitleID as int)
		,Forename = cast(Forename as varchar(35))
		,MiddleNames = cast(MiddleNames as varchar(35))
		,SexID = cast(SexID as int)
		,DateOfBirth = cast(DateOfBirth as datetime)
		,IsTemporaryRegistration = cast(IsTemporaryRegistration as tinyint)
		,LastAttendanceNumber = cast(LastAttendanceNumber as varchar(20))
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,IsDead = cast(IsDead as bit)
	from
		(
		select
			 PatientID = Patient.pat_pid
			,Surname = Patient.pat_surname
			,TitleID = Patient.pat_title
			,Forename = Patient.pat_forename
			,MiddleNames = Patient.pat_midnames
			,SexID = Patient.pat_sex
			,DateOfBirth = Patient.pat_dob
			,IsTemporaryRegistration = Patient.pat_tempreg
			,LastAttendanceNumber = Patient.pat_lastattno
			,CreatedByID = Patient.pat_createdby
			,UpdatedTime = Patient.pat_update
			,IsDead = Patient.pat_IsDead
		from
			[$(Symphony)].dbo.Patient
		where
			Patient.pat_deleted = 0

		) Encounter
	) Encounter
order by
	Encounter.PatientID

create unique clustered index #IX_TLoadSymphonyPatient on #TLoadSymphonyPatient
	(
	PatientID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Patient target
using
	(
	select
		 PatientID
		,Surname
		,TitleID
		,Forename
		,MiddleNames
		,SexID
		,DateOfBirth
		,IsTemporaryRegistration
		,LastAttendanceNumber
		,CreatedByID
		,UpdatedTime
		,IsDead

		,EncounterChecksum
	from
		#TLoadSymphonyPatient
	
	) source
	on	source.PatientID = target.PatientID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 PatientID
			,Surname
			,TitleID
			,Forename
			,MiddleNames
			,SexID
			,DateOfBirth
			,IsTemporaryRegistration
			,LastAttendanceNumber
			,CreatedByID
			,UpdatedTime
			,IsDead

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.PatientID
			,source.Surname
			,source.TitleID
			,source.Forename
			,source.MiddleNames
			,source.SexID
			,source.DateOfBirth
			,source.IsTemporaryRegistration
			,source.LastAttendanceNumber
			,source.CreatedByID
			,source.UpdatedTime
			,source.IsDead

			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.PatientID = source.PatientID
			,target.Surname = source.Surname
			,target.TitleID = source.TitleID
			,target.Forename = source.Forename
			,target.MiddleNames = source.MiddleNames
			,target.SexID = source.SexID
			,target.DateOfBirth = source.DateOfBirth
			,target.IsTemporaryRegistration = source.IsTemporaryRegistration
			,target.LastAttendanceNumber = source.LastAttendanceNumber
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.IsDead = source.IsDead

			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
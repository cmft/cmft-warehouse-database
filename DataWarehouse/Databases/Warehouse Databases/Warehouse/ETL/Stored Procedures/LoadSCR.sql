﻿CREATE   procedure [ETL].[LoadSCR] 
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

exec SCR.LoadReferral
exec SCR.LoadDefinitiveTreatment
exec SCR.LoadDemographic
exec SCR.LoadCarePlan
exec SCR.LoadInitialAssessment
exec SCR.LoadChemotherapy
exec SCR.LoadSurgery
exec SCR.LoadReferenceData
exec SCR.LoadTrackingComment
exec SCR.LoadImaging
exec SCR.LoadPathology
exec SCR.LoadReferralGynaecology
exec SCR.LoadGynaecologyMDT
exec SCR.LoadTNMVersionHistory
exec SCR.LoadMetastases


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


﻿create procedure [ETL].[ExtractSIDQualityIndicators]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDQualityIndicators;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDQualityIndicators
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalReason
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalNotes
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendance
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceReason
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceNotes
	,BrainScanWithin24HoursOfAttendance
	,BrainScanWithin24HoursOfAttendanceReason
	,BrainScanWithin24HoursOfAttendanceNotes
	,AspirinWithin24HoursOfAttendance
	,AspirinWithin24HoursOfAttendanceReason
	,AspirinWithin24HoursOfAttendanceNotes
	,PhysiotherapyAssessmentWithinFirst24Hours
	,PhysiotherapyAssessmentWithinFirst24HoursReason
	,PhysiotherapyAssessmentWithinFirst24HoursNotes
	,AssessmentByAnOccupationalTherapistWithinFirst24Hours
	,AssessmentByAnOccupationalTherapistWithinFirst24HoursReason
	,AssessmentByAnOccupationalTherapistWithinFirst24HoursNotes
	,WeighedAtLeastOnceDuringAttendance
	,WeighedAtLeastOnceDuringAttendanceReason
	,WeighedAtLeastOnceDuringAttendanceNotes
	,SeenByStrokePhysicianWithin24HoursOfAttendance
	,SeenByStrokePhysicianWithin24HoursOfAttendanceReason
	,SeenByStrokePhysicianWithin24HoursOfAttendanceNotes
	,CurrentLocation
	,Username
	,ModifiedDate
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		SectionID
	,ActionID = 
		ActionID
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrival)[1]', 'varchar(max)'),'')
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalReason)[1]', 'varchar(max)'),'')
	,DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/DirectAdmissionToAStrokeUnitWithin4HoursOfHospitalArrivalNotes)[1]', 'varchar(max)'),'')
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendance = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/ScreenedForSwallowingDisordersWithin24HoursOfAttendance)[1]', 'varchar(max)'),'')
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/ScreenedForSwallowingDisordersWithin24HoursOfAttendanceReason)[1]', 'varchar(max)'),'')
	,ScreenedForSwallowingDisordersWithin24HoursOfAttendanceNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/ScreenedForSwallowingDisordersWithin24HoursOfAttendanceNotes)[1]', 'varchar(max)'),'')
	,BrainScanWithin24HoursOfAttendance = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/BrainScanWithin24HoursOfAttendance)[1]', 'varchar(max)'),'')
	,BrainScanWithin24HoursOfAttendanceReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/BrainScanWithin24HoursOfAttendanceReason)[1]', 'varchar(max)'),'')
	,BrainScanWithin24HoursOfAttendanceNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/BrainScanWithin24HoursOfAttendanceNotes)[1]', 'varchar(max)'),'')
	,AspirinWithin24HoursOfAttendance = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/AspirinWithin24HoursOfAttendance)[1]', 'varchar(max)'),'')
	,AspirinWithin24HoursOfAttendanceReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/AspirinWithin24HoursOfAttendanceReason)[1]', 'varchar(max)'),'')
	,AspirinWithin24HoursOfAttendanceNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/AspirinWithin24HoursOfAttendanceNotes)[1]', 'varchar(max)'),'')
	,PhysiotherapyAssessmentWithinFirst24Hours = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/PhysiotherapyAssessmentWithinFirst24Hours)[1]', 'varchar(max)'),'')
	,PhysiotherapyAssessmentWithinFirst24HoursReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/PhysiotherapyAssessmentWithinFirst24HoursReason)[1]', 'varchar(max)'),'')
	,PhysiotherapyAssessmentWithinFirst24HoursNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/PhysiotherapyAssessmentWithinFirst24HoursNotes)[1]', 'varchar(max)'),'')
	,AssessmentByAnOccupationalTherapistWithinFirst24Hours = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/AssessmentByAnOccupationalTherapistWithinFirst24Hours)[1]', 'varchar(max)'),'')
	,AssessmentByAnOccupationalTherapistWithinFirst24HoursReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/AssessmentByAnOccupationalTherapistWithinFirst24HoursReason)[1]', 'varchar(max)'),'')
	,AssessmentByAnOccupationalTherapistWithinFirst24HoursNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/AssessmentByAnOccupationalTherapistWithinFirst24HoursNotes)[1]', 'varchar(max)'),'')
	,WeighedAtLeastOnceDuringAttendance = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/WeighedAtLeastOnceDuringAttendance)[1]', 'varchar(max)'),'')
	,WeighedAtLeastOnceDuringAttendanceReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/WeighedAtLeastOnceDuringAttendanceReason)[1]', 'varchar(max)'),'')
	,WeighedAtLeastOnceDuringAttendanceNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/WeighedAtLeastOnceDuringAttendanceNotes)[1]', 'varchar(max)'),'')
	,SeenByStrokePhysicianWithin24HoursOfAttendance = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/SeenByStrokePhysicianWithin24HoursOfAttendance)[1]', 'varchar(max)'),'')
	,SeenByStrokePhysicianWithin24HoursOfAttendanceReason = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/SeenByStrokePhysicianWithin24HoursOfAttendanceReason)[1]', 'varchar(max)'),'')
	,SeenByStrokePhysicianWithin24HoursOfAttendanceNotes = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/SeenByStrokePhysicianWithin24HoursOfAttendanceNotes)[1]', 'varchar(max)'),'')
	,CurrentLocation = 
		nullif(CandidateData.Details.value('(/QualityIndicatorsDTO/CurrentLocation)[1]', 'varchar(max)'),'')
	,Username = 
		CandidateData.Details.value('(/QualityIndicatorsDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/QualityIndicatorsDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
where
	CandidateData.SectionID = 22
and CandidateData.ActionID = 99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDQualityIndicators', @Stats, @StartTime





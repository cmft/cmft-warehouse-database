﻿CREATE PROCEDURE [ETL].[LoadQCRReferenceData]

AS

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

SELECT @StartTime = getdate();

SELECT @RowsInserted = 0;

--Load Question Data
TRUNCATE TABLE QCR.Question;

INSERT INTO QCR.Question
(
	 QuestionCode
	,AuditTypeCode 
	,Question 
	,Guidance
	,AdditionalText 
	,QuestionOrder
)
SELECT
	 QuestionCode = QuestionID
	,AuditTypeCode = AuditTypeID
	,Question = QuestionText
	,Guidance = Guidance
	,AdditionalText = add_text
	,[Order] = QuestionsTemplate.QuestionOrder
FROM 
	[$(QualityOfCare)].dbo.QuestionsTemplate

SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;


-- Load Ward Data
TRUNCATE TABLE QCR.Location;

INSERT INTO QCR.Location
(
	LocationCode,
	Location,
	CategoryTypeCode,
	CategoryGroupCode,
	DivisionCode,
	Active
)
SELECT
	LocationCode = WardID ,
	Location = WardName,
	CategoryTypeCode = WardCategoryTypeID,
	CategoryGroupCode = WardCategoryGroupID,
	DivisionCode = WardDivisionID,
	Active =Active
FROM [$(QualityOfCare)].dbo.Ward;

SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;


-- Load Custom List
TRUNCATE TABLE QCR.CustomList;

INSERT INTO QCR.CustomList
(
	CustomListCode,
	CustomList
)
SELECT
	CustomListCode = CustomListID,
	CustomList = CustomListDescription
FROM 
	[$(QualityOfCare)].dbo.CustomList;

SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;

-- Load Custom List Question
TRUNCATE TABLE QCR.CustomListQuestion

INSERT INTO QCR.CustomListQuestion
(
	CustomListCode,
	QuestionCode
)
SELECT
	CustomListID = CustomListID,
	QuestionID = QuestionID

FROM 
	[$(QualityOfCare)].dbo.CustomListQuestions;

SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;

-- Load Category Group
TRUNCATE TABLE QCR.CategoryGroup

INSERT INTO QCR.CategoryGroup
(
	CategoryGroupCode,
	CategoryGroup
)
SELECT
	CategoryGroupCode = WardCategoryGroupID,
	CategoryGroup = GroupDescription
FROM 
	[$(QualityOfCare)].dbo.WardCategoryGroup;

SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;


-- Load Category Type
TRUNCATE TABLE QCR.CategoryType

INSERT INTO QCR.CategoryType
(
	CategoryTypeCode,
	CategoryType
)
SELECT
	CategoryTypeCode = [WardCategoryTypeID],
	CategoryType = [WardDescription]
FROM 
	[$(QualityOfCare)].dbo.WardCategoryType;

SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;


-- Load Division
TRUNCATE TABLE QCR.Division

INSERT INTO QCR.Division
(
	DivisionCode,
	Division
)
SELECT 
	DivisionCode = WardDivisionID,
	Division = WardDivisionDescription
FROM 
	[$(QualityOfCare)].dbo.WardDivisions;

	SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;


-- Load Audit Type
TRUNCATE TABLE QCR.AuditType;

INSERT INTO QCR.AuditType
(
	AuditTypeCode,
	AuditType,
	AuditOrder
)
SELECT
	AuditTypeCode = AuditTypeID,
	AuditType = AuditType.AuditTypeDescription,
	AuditOrder = AuditOrder
FROM 
	[$(QualityOfCare)].dbo.AuditType;

SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;

SELECT	
	@Elapsed = DATEDIFF(minute,@StartTime,getdate());

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins';

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;

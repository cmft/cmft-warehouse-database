﻿
CREATE PROCEDURE [ETL].[LoadSymphonyClinicAppointment]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony ClinicAppointment table

*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	*
	,EncounterChecksum =
		CHECKSUM(
			ClinicInstanceID
			,CurrentLocationID
			,SeenByID
			,AssociatedDepartmentData
			,IsCancelled
		)
into
	#TLoadSymphonyClinicAppointment
from
	(
	select
		AttendanceID = cast(AttendanceID as int)
		,ClinicInstanceID
		,CurrentLocationID = cast(CurrentLocationID as smallint)
		,SeenByID = cast(SeenByID as varchar(100))
		,AssociatedDepartmentData = cast(AssociatedDepartmentData as varchar(1000))
		,IsCancelled = cast(IsCancelled as bit)
	from
		(
		select
			AttendanceID = ClinicAppointment.cla_atdid
			,ClinicInstanceID = ClinicAppointment.cla_clininst
			,CurrentLocationID = cla_currentlocationid
			,SeenByID = case when ClinicAppointment.cla_seenby = '' then null else ClinicAppointment.cla_seenby end
			,AssociatedDepartmentData = case when ClinicAppointment.cla_assocdepdata = '' then null else ClinicAppointment.cla_assocdepdata end
			,IsCancelled = ClinicAppointment.cla_cancelled
		from
			[$(Symphony)].dbo.Clinic_Appointments ClinicAppointment
		) Encounter
	) Encounter

order by
	Encounter.AttendanceID


create unique clustered index #IX_TLoadSymphony#TLoadSymphonyClinicAppointment on #TLoadSymphonyClinicAppointment
	(
	AttendanceID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.ClinicAppointment target
using
	(
	select
		AttendanceID
		,ClinicInstanceID
		,CurrentLocationID
		,SeenByID
		,AssociatedDepartmentData
		,IsCancelled
		,EncounterChecksum
	from
		#TLoadSymphonyClinicAppointment
	
	) source
	on	source.AttendanceID = target.AttendanceID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			AttendanceID
			,ClinicInstanceID
			,CurrentLocationID
			,SeenByID
			,AssociatedDepartmentData
			,IsCancelled
			,Created
			,ByWhom
			)
		values
			(
			source.AttendanceID
			,source.ClinicInstanceID
			,source.CurrentLocationID
			,source.SeenByID
			,source.AssociatedDepartmentData
			,source.IsCancelled
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> 
		CHECKSUM(
			target.ClinicInstanceID
			,target.CurrentLocationID
			,target.SeenByID
			,target.AssociatedDepartmentData
			,target.IsCancelled
		)
	then
		update
		set
			target.ClinicInstanceID = source.ClinicInstanceID
			,target.CurrentLocationID = source.CurrentLocationID
			,target.SeenByID = source.SeenByID
			,target.AssociatedDepartmentData = source.AssociatedDepartmentData
			,target.IsCancelled = source.IsCancelled

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
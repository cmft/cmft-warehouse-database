﻿CREATE procedure [ETL].[ExtractSMMISBirth]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)



update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSSMMISBIRTH'

truncate table [ETL].[TImportSMMISBirth]

INSERT INTO [ETL].[TImportSMMISBirth]

	([MotherNo]
	,[MotherNhsNumber]
	,[MotherNhsNoValid]
	,[CurrentOccurrence]
	,[MotherMaidenName]
	,[MotherCurrentSurname]
	,[MotherFForenames]
	,[MotherTitle]
	,[MotherDateOfBirth]
	,[MotherPlaceOfBirth]
	,[UkEntryDate]
	,[MotherDateApprox]
	,[MotherEthnicGroupPrefix]
	,[MotherEthnicGroupBody]
	,[MotherEthnicGroupOpcs]
	,[MotherEthnicGroupPas]
	,[MotherGpSurname]
	,[MotherGpSequence]
	,[MotherTraceStatus]
	,[MotherLocation]
	,[MotherDateOfDeath]
	,[MotherTimeOfDeath]
	,[MotherDeathIndicator]
	,[PregnancyPortable]
	,[PregnancyDiskCreateDate]
	,[PregnancyStatus]
	,[PregnancyType]
	,[PregnancyUnbooked]
	,[PregnancyCardNumber]
	,[PregnancyCreateDate]
	,[PregnancyCreateTime]
	,[MotherReligion]
	,[OtherReligion]
	,[Marital]
	,[MSurname]
	,[PrefName]
	,[MotherOccupation]
	,[MainIncome]
	,[PrevEmployment]
	,[MotherTelephoneWork]
	,[OneParentFam]
	,[SocialClass]
	,[BkScreen1Ind]
	,[BkScreen1aInd]
	,[BkScreen2Ind]
	,[InvScreen1Ind]
	,[InvScreen2Ind]
	,[AnScreen1Ind]
	,[AnScreen2Ind]
	,[AnReason]
	,[PregnancyAnDate]
	,[AnOperation]
	,[PregnancyGpSurname]
	,[PregnancyGpSequence]
	,[TypeOfPatient]
	,[OverseasVisitor]
	,[PregnancyConCode]
	,[PregnancyBookingMidwife]
	,[PregnancyMidwifeTeam]
	,[ReasonCompletion]
	,[SpecOtherPlace]
	,[ConfirmedDob]
	,[ReferredBy]
	,[ArchiveDate]
	,[AnSex]
	,[PregnancyBookingData]
	,[Erdip]
	,[HospReferredTo]
	,[ReferralDate]
	,[ReferralNumber]
	,[PregnancyLocation]
	,[ScunEcno]
	,[AnWardType]
	,[ReferredByOther]
	,[ReferredByHosp]
	,[PregnancyCommunityType]
	,[ReferralGestWks]
	,[ReferralGestDays]
	,[LmpAtReferral]
	,[EddAtReferral]
	,[BnScreen1Ind]
	,[NumberInfants]
	,[OneParent]
	,[LabourOnset]
	,[Augmentation]
	,[OnsetFirstDate]
	,[OnsetFirstTime]
	,[OnsetSecondDate]
	,[OnsetSecondTime]
	,[MethodIndAug]
	,[BnScreen2Ind]
	,[Pyrexia]
	,[BloodLoss]
	,[Perineum]
	,[NonPerinealTears]
	,[Suture]
	,[ManualRemoval]
	,[PlacentaMembranes]
	,[Membranes]
	,[LabCompl]
	,[LabComplText]
	,[IntendedFeeding]
	,[LabResearch]
	,[Support]
	,[DScreen1Ind]
	,[Eclampsia]
	,[Thromboembolism]
	,[Erpc]
	,[PuerperalPsychosis]
	,[Infection]
	,[DischHbTest]
	,[DischHaemoglobin]
	,[AntiD]
	,[RubellaVac]
	,[BloodTrans]
	,[Sterilization]
	,[Contraception]
	,[DMatCompl]
	,[DMatComplText]
	,[DScreen2Ind]
	,[Appointment]
	,[ResearchDisch]
	,[AgeAtDel]
	,[LabourDuration1stStage]
	,[LabourDuration2ndStage]
	,[LabourDurationLabour]
	,[PnLenStay]
	,[AnComplicationsIcd]
	,[DelComplicationsIcd]
	,[PuerpComplicationsIcd]
	,[MatOperationsIcd]
	,[TenSmokingStage]
	,[TenCigsPerDay]
	,[TenPartnerSmoke]
	,[DischSmokingStage]
	,[DischCigsPerDay]
	,[DischPartnerSmoke]
	,[RiskDelivery]
	,[DischMedication]
	,[DischMedicationText]
	,[DischPartnerCigsPerDay]
	,[TenPartnerCigsPerDay]
	,[SkinToSkin]
	,[SkinToSkinText]
	,[AnalMucosa]
	,[TempMaxLabour]
	,[EpisiotomySutured]
	,[LabourLocation]
	,[BreastFeedInitiated]
	,[Date1stBreastFeed]
	,[Time1stBreastFeed]
	,[PnLenStayHours]
	,[PnLenStayMinutes]
	,[TransBpSystolic]
	,[TransBpDiastolic]
	,[TransToCommunityDate]
	,[TransferringHospNumber]
	,[CmwTeam]
	,[CmwDefaultMidwife]
	,[CmwBackupMidwife]
	,[CmwSource]
	,[CmwHospitalName]
	,[CmwHospitalChar]
	,[CmwHospitalOrgCode]
	,[TransComments]
	,[CmwMDischDate]
	,[CmwMDischCondition]
	,[CpNormalLabour]
	,[CpReasonNotUsed]
	,[CpOtherReasonNotUsed]
	,[PnCare]
	,[PnCareHosp]
	,[PnCareOther]
	,[NonPerinealSuture]
	,[TimeSutureStarted]
	,[ReasonSutureDelayed]
	,[SterilisationText]
	,[OneToOneCare]
	,[FgmReversal]
	,[FgmReversalStage]
	,[FgmReferralToCpMw]
	,[DelGbsCarrier]
	,[DischargedToHvDate]
	,[DischargedToHvTime]
	,[PlannedDischargeDate]
	,[PlannedDischargeTime]
	,[PnComplications]
	,[PnComplicationsText]
	,[Occurrence]
	,[BirthOrder]
	,[InfantNo]
	,[BnScreen3Ind]
	,[RuptureMethod]
	,[RuptureDate]
	,[RuptureTime]
	,[CordProlapse]
	,[PresentationLabour]
	,[ElectronicFh]
	,[FetalPhTest]
	,[FetalPh]
	,[Meconium]
	,[CordBloodGas]
	,[TimeOfBirth]
	,[BirthWeight]
	,[BirthWeightCentile]
	,[Gestation]
	,[GestationByDate]
	,[GestationByUs]
	,[GestUsCertain]
	,[CongenitalAbnorm]
	,[CongenitalDesc]
	,[BnScreen4Ind]
	,[PresentationDeliv]
	,[BreechDiagnosis]
	,[BirthingPool]
	,[MethodDelivery]
	,[IndicCaesarian]
	,[Outcome]
	,[PlaceDelivery]
	,[ReasonForChange]
	,[ConductDel]
	,[DeliveryTeam]
	,[DeliveryMidwife]
	,[SpecPlaceDelivery]
	,[BnScreen5Ind]
	,[ApgarOne]
	,[ApgarFive]
	,[ApgarTen]
	,[OnsetRespiration]
	,[ResusPressure]
	,[ResusDrugs]
	,[ChildDischAddress]
	,[ConsPaed]
	,[InfCompl]
	,[InfComplText]
	,[InfResearch]
	,[MidwifeDelName]
	,[MidwifeDelInitials]
	,[MidwifeDelStatus]
	,[ChildNumber]
	,[IndexPage]
	,[HealthDistrict]
	,[TransferType]
	,[HealthVisitor]
	,[Registration]
	,[BirthPlace]
	,[BirthHA]
	,[OPCSBirthArea]
	,[OPCSResidArea]
	,[BornAt]
	,[LAWard]
	,[AssessGest]
	,[AgreedGestFrom]
	,[AgreedGestTo]
	,[HeadCircum]
	,[HeadCircumCentile]
	,[Length]
	,[LengthCentile]
	,[DScreenInd]
	,[HIP]
	,[Convulsions]
	,[OtherAbnorm]
	,[Jaundice]
	,[Bilirubin]
	,[DInfCompl]
	,[DInfComplText]
	,[CongAbsDisch]
	,[DScreen3AInd]
	,[SCBU]
	,[TransCare]
	,[Cord]
	,[Guthrie]
	,[GuthrieDate]
	,[Haemoglobinopathy]
	,[HaemoglobinopathyDate]
	,[Vitamink]
	,[VitaminkDate]
	,[VitaminkText]
	,[InfHbTest]
	,[InfHaemaglobin]
	,[InfBloodGroup]
	,[InfCoombs]
	,[DischargeExamination]
	,[Condition]
	,[DischExamComments]
	,[HighestLoc]
	,[Condition28Day]
	,[Resarch28Day]
	,[Comments]
	,[Feeding28Day]
	,[NnCompl]
	,[NnComplText]
	,[DatePlacenta]
	,[TimePlacenta]
	,[BirthDurationLabour]
	,[BirthDuration2ndStage]
	,[BirthDuration3rdStage]
	,[HearingTest]
	,[dateHearing]
	,[HearingResult]
	,[ActualHospital]
	,[BirthDuration1stStage]
	,[Tab0]
	,[Tab1]
	,[Tab2]
	,[Tab3]
	,[tab4]
	,[DelHospCode]
	,[DelHospName]
	,[ChildHealthCode]
	,[CordVenousPH]
	,[CordVenBaseDeficit]
	,[CordArterialPH]
	,[CordArtBaseDeficit]
	,[ThirdStageManagement]
	,[Inf1Complications]
	,[Inf2Complications]
	,[CreateBaby]
	,[BnBoxCode]
	,[EcvLabour]
	,[HighestLoc2002]
	,[BirthLocation]
	,[DateCsDecided]
	,[TimeCsDecided]
	,[TimeToCaesarian]
	,[MidwifeKnown]
	,[GestationDays]
	,[GestByDateDays]
	,[GestByUsDays]
	,[AssessGestDays]
	,[AgreedGestFromDays]
	,[FollowUpDay]
	,[DilationAtCs]
	,[TransWeight]
	,[TransFeeding]
	,[CmwIDischWeight]
	,[CmwIDischCondition]
	,[PnSex]
	,[PnDateOfBirth]
	,[PnAgeAtDischarge]
	,[BirthAnDate]
	,[IndicInstrumental]
	,[ShoulderDystocia]
	,[ShoulderInjury]
	,[ShoulderInjuryText]
	,[CsReasonForDelay]
	,[Palpable5ths]
	,[FetalPosition]
	,[Station]
	,[Moulding]
	,[Caput]
	,[LiquorColour]
	,[DeliveryOfBaby]
	,[CordEntanglement]
	,[Position]
	,[NewbornExamination]
	,[NewbornExaminationDate]
	,[NewbornExaminationTime]
	,[ExaminationCompletedBy]
	,[ExaminationMidwifePin]
	,[ExaminationText]
	,[Heart]
	,[Eyes]
	,[Testes]
	,[BirthMarksBruises]
	,[BirthMarksBruisesText]
	,[NewbornReferral]
	,[CongenitalList]
	,[RedBookCompleted]
	,[InfantNhsNumber]
	,[InfantNhsNoValid]
	,[InfantSex]
	,[InfantMaidenName]
	,[InfantCurrentSurname]
	,[InfantForenames]
	,[InfantDateOfBirth]
	,[InfantPlaceOfBirth]
	,[InfantEthnicGroupPrefix]
	,[InfantEthnicGroupBody]
	,[InfantEthnicGroupOpcs]
	,[InfantEthnicGroupPas]
	,[InfantGpSurname]
	,[InfantGpSequence]
	,[InfantTraceStatus]
	,[InfantLocation])

select

	MotherNo = Birth.[M_NUMBER]	
	,MotherNhsNumber = Mother.M_NHS_NUMBER
	,MotherNhsNoValid = Mother.M_NHS_NO_VALID
	,CurrentOccurrence = Mother.CURRENT_OCCURRENCE
	,MotherMaidenName = Mother.MAIDEN_NAME
	,MotherCurrentSurname = Mother.CURRENT_SURNAME
	,MotherFForenames = Mother.F_FORENAMES
	,MotherTitle = Mother.M_TITLE
	,MotherDateOfBirth = Mother.M_DOB
	,MotherPlaceOfBirth = Mother.PLACE_OF_BIRTH
	,UkEntryDate = Mother.UK_ENTRY_DATE
	,MotherDateApprox = Mother.DATE_APPROX
	,MotherEthnicGroupPrefix = Mother.ETHNIC_GROUP_PREFIX
	,MotherEthnicGroupBody = Mother.ETHNIC_GROUP_BODY
	,MotherEthnicGroupOpcs = Mother.ETHNIC_GROUP_OPCS
	,MotherEthnicGroupPas = Mother.ETHNIC_GROUP_PAS
	,MotherGpSurname = Mother.GP_SURNAME
	,MotherGpSequence = Mother.GP_SEQUENCE
	,MotherTraceStatus = Mother.TRACE_STATUS
	,MotherLocation = Mother.LOCATION
	,MotherDateOfDeath = Mother.DATE_OF_DEATH
	,MotherTimeOfDeath = Mother.TIME_OF_DEATH
	,MotherDeathIndicator = Mother.DEATH_INDICATOR

	,PregnancyPortable = Pregnancy.PORTABLE
	,PregnancyDiskCreateDate = Pregnancy.DISK_CREATE_DATE
	,PregnancyStatus = Pregnancy.STATUS
	,PregnancyType = Pregnancy.TYPE
	,PregnancyUnbooked = Pregnancy.UNBOOKED
	,PregnancyCardNumber = Pregnancy.CARD_NUMBER
	,PregnancyCreateDate = Pregnancy.CREATE_DATE
	,PregnancyCreateTime = Pregnancy.CREATE_TIME
	,MotherReligion = Pregnancy.M_RELIGION
	,OtherReligion = Pregnancy.OTHER_RELIGION
	,Marital = Pregnancy.MARITAL
	,MSurname = Pregnancy.M_SURNAME
	,PrefName = Pregnancy.PREF_NAME
	,MotherOccupation = Pregnancy.M_OCCUPATION
	,MainIncome = Pregnancy.MAIN_INCOME
	,PrevEmployment = Pregnancy.PREV_EMPLOYMENT
	,MotherTelephoneWork = Pregnancy.M_TELEPHONE_WORK
	,OneParentFam = Pregnancy.ONE_PARENT_FAM
	,SocialClass = Pregnancy.SOCIAL_CLASS
	,BkScreen1Ind = Pregnancy.BK_SCREEN1_IND
	,BkScreen1aInd = Pregnancy.BK_SCREEN1A_IND
	,BkScreen2Ind = Pregnancy.BK_SCREEN2_IND
	,InvScreen1Ind = Pregnancy.INV_SCREEN1_IND
	,InvScreen2Ind = Pregnancy.INV_SCREEN2_IND
	,AnScreen1Ind = Pregnancy.AN_SCREEN1_IND
	,AnScreen2Ind = Pregnancy.AN_SCREEN2_IND
	,AnReason = Pregnancy.AN_REASON
	,PregnancyAnDate = Pregnancy.AN_DATE
	,AnOperation = Pregnancy.AN_OPERATION
	,PregnancyGpSurname = Pregnancy.GP_SURNAME
	,PregnancyGpSequence = Pregnancy.GP_SEQUENCE
	,TypeOfPatient = Pregnancy.TYPE_OF_PATIENT
	,OverseasVisitor = Pregnancy.OVERSEAS_VISITOR
	,PregnancyConCode = Pregnancy.CON_CODE
	,PregnancyBookingMidwife = Pregnancy.BOOKING_MIDWIFE
	,PregnancyMidwifeTeam = Pregnancy.MIDWIFE_TEAM
	,ReasonCompletion = Pregnancy.REASON_COMPLETION
	,SpecOtherPlace = Pregnancy.SPEC_OTHER_PLACE
	,ConfirmedDob = Pregnancy.CONFIRMED_DOB
	,ReferredBy = Pregnancy.REFERRED_BY
	,ArchiveDate = Pregnancy.ARCHIVE_DATE
	,AnSex = Pregnancy.AN_SEX
	,PregnancyBookingData = Pregnancy.BOOKING_DATA
	,Erdip = Pregnancy.ERDIP
	,HospReferredTo = Pregnancy.HOSP_REFERRED_TO
	,ReferralDate = Pregnancy.REFERRAL_DATE
	,ReferralNumber = Pregnancy.REFERRAL_NUMBER
	,PregnancyLocation = Pregnancy.LOCATION
	,ScunEcno = Pregnancy.SCUN_ECNO
	,AnWardType = Pregnancy.AN_WARD_TYPE
	,ReferredByOther = Pregnancy.REFERRED_BY_OTHER
	,ReferredByHosp = Pregnancy.REFERRED_BY_HOSP
	,PregnancyCommunityType = Pregnancy.COMMUNITY_TYPE
	,ReferralGestWks = Pregnancy.REFERRAL_GEST_WKS
	,ReferralGestDays = Pregnancy.REFERRAL_GEST_DAYS
	,LmpAtReferral = Pregnancy.LMP_AT_REFERRAL
	,EddAtReferral = Pregnancy.EDD_AT_REFERRAL

	,BnScreen1Ind = Labour.BN_SCREEN1_IND
	,NumberInfants = Labour.NUMBER_INFANTS
	,OneParent = Labour.ONE_PARENT
	,LabourOnset = Labour.LABOUR_ONSET
	,Augmentation = Labour.AUGMENTATION
	,OnsetFirstDate = Labour.ONSET_FIRST_DATE
	,OnsetFirstTime = Labour.ONSET_FIRST_TIME
	,OnsetSecondDate = Labour.ONSET_SECOND_DATE
	,OnsetSecondTime = Labour.ONSET_SECOND_TIME
	,MethodIndAug = rtrim(Labour.METHOD_IND_AUG)
	,BnScreen2Ind = Labour.BN_SCREEN2_IND
	,Pyrexia = Labour.PYREXIA
	,BloodLoss = Labour.BLOOD_LOSS
	,Perineum = Labour.PERINEUM
	,NonPerinealTears = Labour.NON_PERINEAL_TEARS
	,Suture = Labour.SUTURE
	,ManualRemoval = Labour.MANUAL_REMOVAL
	,PlacentaMembranes = Labour.PLACENTA_MEMBRANES
	,Membranes = Labour.MEMBRANES
	,LabCompl = Labour.LAB_COMPL
	,LabComplText = Labour.LAB_COMPL_TEXT
	,IntendedFeeding = Labour.INTENDED_FEEDING
	,LabResearch = Labour.LAB_RESEARCH
	,Support = Labour.SUPPORT
	,DScreen1Ind = Labour.D_SCREEN1_IND
	,Eclampsia = Labour.ECLAMPSIA
	,Thromboembolism = Labour.THROMBOEMBOLISM
	,Erpc = Labour.ERPC
	,PuerperalPsychosis = Labour.PUERPERAL_PSYCHOSIS
	,Infection = Labour.INFECTION
	,DischHbTest = Labour.DISCH_HB_TEST
	,DischHaemoglobin = Labour.DISCH_HAEMOGLOBIN
	,AntiD = Labour.ANTI_D
	,RubellaVac = Labour.RUBELLA_VAC
	,BloodTrans = Labour.BLOOD_TRANS
	,Sterilization = Labour.STERILIZATION
	,Contraception = Labour.CONTRACEPTION
	,DMatCompl = Labour.D_MAT_COMPL
	,DMatComplText = Labour.D_MAT_COMPL_TEXT
	,DScreen2Ind = Labour.D_SCREEN2_IND
	,Appointment = Labour.APPOINTMENT
	,ResearchDisch = Labour.RESEARCH_DISCH
	,AgeAtDel = Labour.AGE_AT_DEL
	,LabourDuration1stStage = Labour.DURATION_1ST_STAGE
	,LabourDuration2ndStage = Labour.DURATION_2ND_STAGE
	,LabourDurationLabour = Labour.DURATION_LABOUR
	,PnLenStay = Labour.PN_LEN_STAY
	,AnComplicationsIcd = Labour.AN_COMPLICATIONS_ICD
	,DelComplicationsIcd = Labour.DEL_COMPLICATIONS_ICD
	,PuerpComplicationsIcd = Labour.PUERP_COMPLICATIONS_ICD
	,MatOperationsIcd = Labour.MAT_OPERATIONS_ICD
	,TenSmokingStage = Labour.TEN_SMOKING_STAGE
	,TenCigsPerDay = Labour.TEN_CIGS_PER_DAY
	,TenPartnerSmoke = Labour.TEN_PARTNER_SMOKE
	,DischSmokingStage = Labour.DISCH_SMOKING_STAGE
	,DischCigsPerDay = Labour.DISCH_CIGS_PER_DAY
	,DischPartnerSmoke = Labour.DISCH_PARTNER_SMOKE
	,RiskDelivery = Labour.RISK_DELIVERY
	,DischMedication = Labour.DISCH_MEDICATION
	,DischMedicationText = Labour.DISCH_MEDICATION_TEXT
	,DischPartnerCigsPerDay = Labour.DISCH_PARTNER_CIGS_PER_DAY
	,TenPartnerCigsPerDay = Labour.TEN_PARTNER_CIGS_PER_DAY
	,SkinToSkin = Labour.SKIN_TO_SKIN
	,SkinToSkinText = Labour.SKIN_TO_SKIN_TEXT
	,AnalMucosa = Labour.ANAL_MUCOSA
	,TempMaxLabour = Labour.TEMP_MAX_LABOUR
	,EpisiotomySutured = Labour.EPISIOTOMY_SUTURED
	,LabourLocation = Labour.LOCATION
	,BreastFeedInitiated = Labour.BREAST_FEED_INITIATED
	,Date1stBreastFeed = Labour.DATE_1ST_BREAST_FEED
	,Time1stBreastFeed = Labour.TIME_1ST_BREAST_FEED
	,PnLenStayHours = Labour.PN_LEN_STAY_HOURS
	,PnLenStayMinutes = Labour.PN_LEN_STAY_MINUTES
	,TransBpSystolic = Labour.TRANS_BP_SYSTOLIC
	,TransBpDiastolic = Labour.TRANS_BP_DIASTOLIC
	,TransToCommunityDate = Labour.TRANS_TO_COMMUNITY_DATE
	,TransferringHospNumber = Labour.TRANSFERRING_HOSP_NUMBER
	,CmwTeam = Labour.CMW_TEAM
	,CmwDefaultMidwife = Labour.CMW_DEFAULT_MIDWIFE
	,CmwBackupMidwife = Labour.CMW_BACKUP_MIDWIFE
	,CmwSource = Labour.CMW_SOURCE
	,CmwHospitalName = Labour.CMW_HOSPITAL_NAME
	,CmwHospitalChar = Labour.CMW_HOSPITAL_CHAR
	,CmwHospitalOrgCode = Labour.CMW_HOSPITAL_ORG_CODE
	,TransComments = Labour.TRANS_COMMENTS
	,CmwMDischDate = Labour.CMW_M_DISCH_DATE
	,CmwMDischCondition = Labour.CMW_M_DISCH_CONDITION
	,CpNormalLabour = Labour.CP_NORMAL_LABOUR
	,CpReasonNotUsed = Labour.CP_REASON_NOT_USED
	,CpOtherReasonNotUsed = Labour.CP_OTHER_REASON_NOT_USED
	,PnCare = Labour.PN_CARE
	,PnCareHosp = Labour.PN_CARE_HOSP
	,PnCareOther = Labour.PN_CARE_OTHER
	,NonPerinealSuture = Labour.NON_PERINEAL_SUTURE
	,TimeSutureStarted = Labour.TIME_SUTURE_STARTED
	,ReasonSutureDelayed = Labour.REASON_SUTURE_DELAYED
	,SterilisationText = Labour.STERILISATION_TEXT
	,OneToOneCare = Labour.ONE_TO_ONE_CARE
	,FgmReversal = Labour.FGM_REVERSAL
	,FgmReversalStage = Labour.FGM_REVERSAL_STAGE
	,FgmReferralToCpMw = Labour.FGM_REFERRAL_TO_CP_MW
	,DelGbsCarrier = Labour.DEL_GBS_CARRIER
	,DischargedToHvDate = Labour.DISCHARGED_TO_HV_DATE
	,DischargedToHvTime = Labour.DISCHARGED_TO_HV_TIME
	,PlannedDischargeDate = Labour.PLANNED_DISCHARGE_DATE
	,PlannedDischargeTime = Labour.PLANNED_DISCHARGE_TIME
	,PnComplications = Labour.PN_COMPLICATIONS
	,PnComplicationsText = Labour.PN_COMPLICATIONS_TEXT

	,Occurrence = Birth.[OCCURRENCE]
	,BirthOrder = Birth.[BIRTH_ORDER]
	,InfantNo = Birth.[I_NUMBER]
	,BnScreen3Ind = Birth.[BN_SCREEN3_IND]
	,RuptureMethod = Birth.[RUPTURE_METHOD]
	,RuptureDate = Birth.[RUPTURE_DATE]
	,RuptureTime = Birth.[RUPTURE_TIME]
	,CordProlapse = Birth.[CORD_PROLAPSE]
	,PresentationLabour = Birth.[PRESENTATION_LABOUR]
	,ElectronicFh = Birth.[ELECTRONIC_FH]
	,FetalPhTest = Birth.[FETAL_PH_TEST]
	,FetalPh = Birth.[FETAL_PH]
	,Meconium = Birth.[MECONIUM]
	,CordBloodGas = Birth.[CORD_BLOOD_GAS]
	,TimeOfBirth = Birth.[TIME_OF_BIRTH]
	,BirthWeight = Birth.[BIRTH_WEIGHT]
	,BirthWeightCentile = Birth.[BIRTH_WT_CENTILE]
	,Gestation = Birth.[GESTATION]
	,GestationByDate = Birth.[GEST_BY_DATE]
	,GestationByUs = Birth.[GEST_BY_US]
	,GestUsCertain = Birth.[GEST_US_CERTAIN]
	,CongenitalAbnorm = Birth.[CONGENITAL_ABNORM]
	,CongenitalDesc = Birth.[CONGENITAL_DESC]
	,BnScreen4Ind = Birth.[BN_SCREEN4_IND]
	,PresentationDeliv = Birth.[PRESENTATION_DELIV]
	,BreechDiagnosis = Birth.[BREECH_DIAGNOSIS]
	,BirthingPool = Birth.[BIRTHING_POOL]
	,MethodDelivery = Birth.[METHOD_DELIVERY]
	,IndicCaesarian = Birth.[INDIC_CAESAREAN]
	,Outcome = Birth.[OUTCOME]
	,PlaceDelivery = Birth.[PLACE_DELIVERY]
	,ReasonForChange = Birth.[REASON_FOR_CHANGE]
	,ConductDel = Birth.[CONDUCT_DEL]
	,DeliveryTeam = Birth.[DELIVERY_TEAM]
	,DeliveryMidwife = Birth.[DELIVERY_MIDWIFE]
	,SpecPlaceDelivery = Birth.[SPEC_PLACE_DELIVERY]
	,BnScreen5Ind = Birth.[BN_SCREEN5_IND]
	,ApgarOne = Birth.[APGAR_ONE]
	,ApgarFive = Birth.[APGAR_FIVE]
	,ApgarTen = Birth.[APGAR_TEN]
	,OnsetRespiration = Birth.[ONSET_RESPIRATION]
	,ResusPressure = Birth.[RESUS_PRESSURE]
	,ResusDrugs = Birth.[RESUS_DRUGS]
	,ChildDischAddress = Birth.[CHILD_DISCH_ADDRESS]
	,ConsPaed = Birth.[CONS_PAED]
	,InfCompl = Birth.[INF_COMPL]
	,InfComplText = Birth.[INF_COMPL_TEXT]
	,InfResearch = Birth.[INF_RESEARCH]
	,MidwifeDelName = Birth.[MIDWIFE_DEL_NAME]
	,MidwifeDelInitials = Birth.[MIDWIFE_DEL_INITIALS]
	,MidwifeDelStatus = Birth.[MIDWIFE_DEL_STATUS]
	,ChildNumber = Birth.[CHILDS_NUMBER]
	,IndexPage = Birth.[INDEX_PAGE]
	,HealthDistrict = Birth.[HEALTH_DISTRICT]
	,TransferType = Birth.[TRANSFER_TYPE]
	,HealthVisitor = Birth.[HEALTH_VISITOR]
	,Registration = Birth.[REGISTRATION]
	,BirthPlace = Birth.[BIRTH_PLACE]
	,BirthHA = Birth.[BIRTH_HA]
	,OPCSBirthArea = Birth.[OPCS_BIRTH_AREA]
	,OPCSResidArea = Birth.[OPCS_RESID_AREA]
	,BornAt = Birth.[BORN_AT]
	,LAWard = Birth.[LA_WARD]
	,AssessGest = Birth.[ASSESS_GEST]
	,AgreedGestFrom = Birth.[AGREED_GEST_FROM]
	,AgreedGestTo = Birth.[AGREED_GEST_TO]
	,HeadCircum = Birth.[HEAD_CIRCUM]
	,HeadCircumCentile = Birth.[HEAD_CIRCUM_CENTILE]
	,[Length] = Birth.[LENGTH]
	,LengthCentile = Birth.[LENGTH_CENTILE]
	,DScreenInd = Birth.[D_SCREEN3_IND]
	,HIP = Birth.[HIP]
	,Convulsions = Birth.[CONVULSIONS]
	,OtherAbnorm = Birth.[OTHER_ABNORM]
	,Jaundice = Birth.[JAUNDICE]
	,Bilirubin = Birth.[BILIRUBIN]
	,DInfCompl = Birth.[D_INF_COMPL]
	,DInfComplText = Birth.[D_INF_COMPL_TEXT]
	,CongAbsDisch = Birth.[CONG_ABS_DISCH]
	,DScreen3AInd = Birth.[D_SCREEN3A_IND]
	,SCBU = Birth.[SCBU]
	,TransCare = Birth.[TRANS_CARE]
	,Cord = Birth.[CORD]
	,Guthrie = Birth.[GUTHRIE]
	,GuthrieDate = Birth.[GUTHRIE_DATE]
	,Haemoglobinopathy = Birth.[HAEMOGLOBINOPATHY]
	,HaemoglobinopathyDate = Birth.[HAEMOGLOBINOPATHY_DATE]
	,Vitamink = Birth.[VITAMINK]
	,VitaminkDate = Birth.[VITAMINK_DATE]
	,VitaminkText = Birth.[VITAMINK_TEXT]
	,InfHbTest = Birth.[INF_HB_TEST]
	,InfHaemaglobin = Birth.[INF_HAEMOGLOBIN]
	,InfBloodGroup = Birth.[INF_BLOOD_GROUP]
	,InfCoombs = Birth.[INF_COOMBS]
	,DischargeExamination = Birth.[DISCHARGE_EXAMINATION]
	,Condition = Birth.[CONDITION]
	,DischExamComments = Birth.[DISCH_EXAM_COMMENTS]
	,HighestLoc = Birth.[HIGHEST_LOC]
	,Condition28Day = Birth.[CONDITION_28_DAY]
	,Resarch28Day = Birth.[RESEARCH_28_DAY]
	,Comments = Birth.[COMMENTS]
	,Feeding28Day = Birth.[FEEDING_28_DAY]
	,NnCompl = Birth.[NN_COMPL]
	,NnComplText = Birth.[NN_COMPL_TEXT]
	,DatePlacenta = Birth.[DATE_PLACENTA]
	,TimePlacenta = Birth.[TIME_PLACENTA]
	,BirthDurationLabour = Birth.[DURATION_LABOUR]
	,BirthDuration2ndStage = Birth.[DURATION_2ND_STAGE]
	,BirthDuration3rdStage = Birth.[DURATION_3RD_STAGE]
	,HearingTest = Birth.[HEARING_TEST]
	,dateHearing = Birth.[DATE_HEARING]
	,HearingResult = Birth.[HEARING_RESULT]
	,ActualHospital = Birth.[ACTUAL_HOSPITAL]
	,BirthDuration1stStage = Birth.[DURATION_1ST_STAGE]
	,Tab0 = Birth.[TAB0]
	,Tab1 = Birth.[TAB1]
	,Tab2 = Birth.[TAB2]
	,Tab3 = Birth.[TAB3]
	,tab4 = Birth.[TAB4]
	,DelHospCode = Birth.[DEL_HOSP_CODE]
	,DelHospName = Birth.[DEL_HOSP_NAME]
	,ChildHealthCode = Birth.[CHILD_HEALTH_CODE]
	,CordVenousPH = Birth.[CORD_VENOUS_PH]
	,CordVenBaseDeficit = Birth.[CORD_VEN_BASE_DEFICIT]
	,CordArterialPH = Birth.[CORD_ARTERIAL_PH]
	,CordArtBaseDeficit = Birth.[CORD_ART_BASE_DEFICIT]
	,ThirdStageManagement = Birth.[THIRD_STAGE_MANAGEMENT]
	,Inf1Complications = Birth.[INF1_COMPLICATIONS]
	,Inf2Complications = Birth.[INF2_COMPLICATIONS]
	,CreateBaby = Birth.[CREATE_BABY]
	,BnBoxCode = Birth.[BN_BOX_CODE]
	,EcvLabour = Birth.[ECV_LABOUR]
	,HighestLoc2002 = Birth.[HIGHEST_LOC_2002]
	,BirthLocation = Birth.[LOCATION]
	,DateCsDecided = Birth.[DATE_CS_DECIDED]
	,TimeCsDecided = Birth.[TIME_CS_DECIDED]
	,TimeToCaesarian = Birth.[TIME_TO_CAESAREAN]
	,MidwifeKnown = Birth.[MIDWIFE_KNOWN]
	,GestationDays = Birth.[GESTATION_DAYS]
	,GestByDateDays = Birth.[GEST_BY_DATE_DAYS]
	,GestByUsDays = Birth.[GEST_BY_US_DAYS]
	,AssessGestDays = Birth.[ASSESS_GEST_DAYS]
	,AgreedGestFromDays = Birth.[AGREED_GEST_FROM_DAYS]
	,FollowUpDay = Birth.[FOLLOW_UP_DAY]
	,DilationAtCs = Birth.[DILATION_AT_CS]
	,TransWeight = Birth.[TRANS_WEIGHT]
	,TransFeeding = Birth.[TRANS_FEEDING]
	,CmwIDischWeight = Birth.[CMW_I_DISCH_WEIGHT]
	,CmwIDischCondition = Birth.[CMW_I_DISCH_CONDITION]
	,PnSex = Birth.[PN_SEX]
	,PnDateOfBirth = Birth.[PN_DATE_OF_BIRTH]
	,PnAgeAtDischarge = Birth.[PN_AGE_AT_DISCHARGE]
	,BirthAnDate = Birth.[AN_DATE]
	,IndicInstrumental = Birth.[INDIC_INSTRUMENTAL]
	,ShoulderDystocia = Birth.[SHOULDER_DYSTOCIA]
	,ShoulderInjury = Birth.[SHOULDER_INJURY]
	,ShoulderInjuryText = Birth.[SHOULDER_INJURY_TEXT]
	,CsReasonForDelay = Birth.[CS_REASON_FOR_DELAY]
	,Palpable5ths = Birth.[PALPABLE_5THS]
	,FetalPosition = Birth.[FETAL_POSITION]
	,Station = Birth.[STATION]
	,Moulding = Birth.[MOULDING]
	,Caput = Birth.[CAPUT]
	,LiquorColour = Birth.[LIQUOR_COLOUR]
	,DeliveryOfBaby = Birth.[DELIVERY_OF_BABY]
	,CordEntanglement = Birth.[CORD_ENTANGLEMENT]
	,Position = Birth.[POSITION]
	,NewbornExamination = Birth.[NEWBORN_EXAMINATION]
	,NewbornExaminationDate = Birth.[NEWBORN_EXAMINATION_DATE]
	,NewbornExaminationTime = Birth.[NEWBORN_EXAMINATION_TIME]
	,ExaminationCompletedBy = Birth.[EXAMINATION_COMPLETED_BY]
	,ExaminationMidwifePin = Birth.[EXAMINATION_MIDWIFE_PIN]
	,ExaminationText = Birth.[EXAMINATION_TEXT]
	,Heart = Birth.[HEART]
	,Eyes = Birth.[EYES]
	,Testes = Birth.[TESTES]
	,BirthMarksBruises = Birth.[BIRTH_MARKS_BRUISES]
	,BirthMarksBruisesText = Birth.[BIRTH_MARKS_BRUISES_TEXT]
	,NewbornReferral = Birth.[NEWBORN_REFERRAL]
	,CongenitalList = Birth.[CONGENITAL_LIST]
	,RedBookCompleted = Birth.[RED_BOOK_COMPLETED]

	--,InfantNhsNumber = Infant.I_NHS_NUMBER --CH 23/12/2014 removed as always blank
	
	,InfantNhsNumber = NHSIndex.NHS_NUMBER
	,InfantNhsNoValid = Infant.I_NHS_NO_VALID
	,InfantSex = Infant.SEX
	,InfantMaidenName = Infant.MAIDEN_NAME
	,InfantCurrentSurname = Infant.CURRENT_SURNAME
	,InfantForenames = Infant.FORENAMES
	,InfantDateOfBirth = Infant.DATE_OF_BIRTH
	,InfantPlaceOfBirth = Infant.PLACE_OF_BIRTH
	,InfantEthnicGroupPrefix = Infant.ETHNIC_GROUP_PREFIX
	,InfantEthnicGroupBody = Infant.ETHNIC_GROUP_BODY
	,InfantEthnicGroupOpcs = Infant.ETHNIC_GROUP_OPCS
	,InfantEthnicGroupPas = Infant.ETHNIC_GROUP_PAS
	,InfantGpSurname = Infant.GP_SURNAME
	,InfantGpSequence = Infant.GP_SEQUENCE
	,InfantTraceStatus = Infant.TRACE_STATUS
	,InfantLocation = Infant.LOCATION

from [$(CMISStaging)].[SMMIS].[BIRTH_REG] Birth

left join [$(CMISStaging)].[SMMIS].[MOTHER_REG] Mother 
on Mother.M_NUMBER = Birth.M_NUMBER

left join [$(CMISStaging)].[SMMIS].[INFANT_REG] Infant 
on Infant.I_NUMBER = Birth.I_NUMBER

left join [$(CMISStaging)].[SMMIS].[LABOUR] Labour 
on Labour.M_NUMBER = Birth.M_NUMBER
and Labour.OCCURRENCE = Birth.OCCURRENCE

left join [$(CMISStaging)].[SMMIS].[PREGNANCY] Pregnancy 
on Pregnancy.M_NUMBER = Birth.M_NUMBER
and Pregnancy.OCCURRENCE = Birth.OCCURRENCE

left join [$(CMISStaging)].[SMMIS].[NUMBER_INDEX] NHSIndex 
on NHSIndex.HOSP_NUMBER = Infant.I_NUMBER

where Infant.DATE_OF_BIRTH between @fromDate and @toDate

-- AG 30/8/13 - Two Mother keys exist twice in the MOTHER_REG table, and have duplicate PREGNANCY records. Ommitted here.  
and Mother.M_NUMBER not in ('S03/6096','M09/077635')

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

if @RowsInserted != 0
begin
	update Utility.Parameter set NumericValue = 1 where Parameter = 'EXTRACTSTATUSSMMISBIRTH'
end

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSMMISBirth', @Stats, @StartTime








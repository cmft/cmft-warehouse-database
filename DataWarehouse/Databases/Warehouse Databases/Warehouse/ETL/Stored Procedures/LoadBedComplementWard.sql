﻿CREATE proc [ETL].[LoadBedComplementWard]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table BedComplement.Ward

insert into BedComplement.Ward
(
	 WardID
	,WardCode
	,Ward
)

select 	
	 WardID
	,WardCode
	,Ward
from 
	ETL.TLoadBedComplementWard;

select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime

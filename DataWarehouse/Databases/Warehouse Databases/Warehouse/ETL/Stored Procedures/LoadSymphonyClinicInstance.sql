﻿
CREATE PROCEDURE [ETL].[LoadSymphonyClinicInstance]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony ClinicInstance table

*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	*
	,EncounterChecksum =
		CHECKSUM(
			ClinicID
			,ClinicTypeID
			,Clinic
			,InstanceDate
			,ClinicianID
			,FirstAppointmentTime
			,NumberOfAppointments
			,AppointmentLength
			,MaximumPatientsPerAppointment
			,MaximumPatientsPerClinic
			,AsFirstAttendanceID
			,ClinicStatusID
			,IsDisabled
			,Comments
			,CreatedByID
			,CreatedTime
			,DepartmentID
		)

into
	#TLoadSymphonyClinicInstance
from
	(
	select
		ClinicInstanceID
		,ClinicID = cast(ClinicID as int)
		,ClinicTypeID = cast(ClinicTypeID as int)
		,Clinic = cast(Clinic as varchar(50))
		,InstanceDate = cast(InstanceDate as date)
		,ClinicianID = cast(ClinicianID as int)
		,FirstAppointmentTime = cast(FirstAppointmentTime as time)
		,NumberOfAppointments = cast(NumberOfAppointments as smallint)
		,AppointmentLength = cast(AppointmentLength as smallint)
		,MaximumPatientsPerAppointment = cast(MaximumPatientsPerAppointment as smallint)
		,MaximumPatientsPerClinic = cast(MaximumPatientsPerClinic as smallint)
		,AsFirstAttendanceID = cast(AsFirstAttendanceID as tinyint)
		,ClinicStatusID = cast(ClinicStatusID as tinyint)
		,IsDisabled = cast(IsDisabled as bit)
		,Comments = cast(Comments as varchar(2000))
		,CreatedByID = cast(CreatedByID as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,DepartmentID = cast(DepartmentID as int)

	from
		(
		select
			ClinicInstanceID = ClinicInstance.cln_clinicinst
			,ClinicID = ClinicInstance.cln_clinicid
			,ClinicTypeID = ClinicInstance.cln_type
			,Clinic = ClinicInstance.cln_usfname
			,InstanceDate = cast(ClinicInstance.cln_instancedate as date)
			,ClinicianID = ClinicInstance.cln_clinician
			,FirstAppointmentTime = cast(ClinicInstance.cln_firstapptime as time)
			,NumberOfAppointments = ClinicInstance.cln_numofappts
			,AppointmentLength = ClinicInstance.cln_apptlength
			,MaximumPatientsPerAppointment = ClinicInstance.cln_maxpatsperappt
			,MaximumPatientsPerClinic = ClinicInstance.cln_maxpatsperclinic
			,AsFirstAttendanceID = ClinicInstance.cln_asfirstattendance
			,ClinicStatusID = ClinicInstance.cln_status
			,IsDisabled = ClinicInstance.cln_disabled
			,Comments = case when ClinicInstance.cln_comments = '' then null else ClinicInstance.cln_comments end
			,CreatedByID = ClinicInstance.cln_createdby
			,CreatedTime = ClinicInstance.cln_created
			,DepartmentID = ClinicInstance.cln_deptid
		from
			[$(Symphony)].dbo.ClinicInstance
		) Encounter
	) Encounter

order by
	Encounter.ClinicInstanceID


create unique clustered index #IX_TLoadSymphony#TLoadSymphonyClinicInstance on #TLoadSymphonyClinicInstance
	(
	ClinicInstanceID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.ClinicInstance target
using
	(
	select
		ClinicInstanceID
		,ClinicID
		,ClinicTypeID
		,Clinic
		,InstanceDate
		,ClinicianID
		,FirstAppointmentTime
		,NumberOfAppointments
		,AppointmentLength
		,MaximumPatientsPerAppointment
		,MaximumPatientsPerClinic
		,AsFirstAttendanceID
		,ClinicStatusID
		,IsDisabled
		,Comments
		,CreatedByID
		,CreatedTime
		,DepartmentID
		,EncounterChecksum
	from
		#TLoadSymphonyClinicInstance
	
	) source
	on	source.ClinicInstanceID = target.ClinicInstanceID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			ClinicInstanceID
			,ClinicID
			,ClinicTypeID
			,Clinic
			,InstanceDate
			,ClinicianID
			,FirstAppointmentTime
			,NumberOfAppointments
			,AppointmentLength
			,MaximumPatientsPerAppointment
			,MaximumPatientsPerClinic
			,AsFirstAttendanceID
			,ClinicStatusID
			,IsDisabled
			,Comments
			,CreatedByID
			,CreatedTime
			,DepartmentID
			,Created
			,ByWhom
			)
		values
			(
			source.ClinicInstanceID
			,source.ClinicID
			,source.ClinicTypeID
			,source.Clinic
			,source.InstanceDate
			,source.ClinicianID
			,source.FirstAppointmentTime
			,source.NumberOfAppointments
			,source.AppointmentLength
			,source.MaximumPatientsPerAppointment
			,source.MaximumPatientsPerClinic
			,source.AsFirstAttendanceID
			,source.ClinicStatusID
			,source.IsDisabled
			,source.Comments
			,source.CreatedByID
			,source.CreatedTime
			,source.DepartmentID

			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> 
		CHECKSUM(
			target.ClinicID
			,target.ClinicTypeID
			,target.Clinic
			,target.InstanceDate
			,target.ClinicianID
			,target.FirstAppointmentTime
			,target.NumberOfAppointments
			,target.AppointmentLength
			,target.MaximumPatientsPerAppointment
			,target.MaximumPatientsPerClinic
			,target.AsFirstAttendanceID
			,target.ClinicStatusID
			,target.IsDisabled
			,target.Comments
			,target.CreatedByID
			,target.CreatedTime
			,target.DepartmentID
		)
	then
		update
		set
			target.ClinicID = source.ClinicID
			,target.ClinicTypeID = source.ClinicTypeID
			,target.Clinic = source.Clinic
			,target.InstanceDate = source.InstanceDate
			,target.ClinicianID = source.ClinicianID
			,target.FirstAppointmentTime = source.FirstAppointmentTime
			,target.NumberOfAppointments = source.NumberOfAppointments
			,target.AppointmentLength = source.AppointmentLength
			,target.MaximumPatientsPerAppointment = source.MaximumPatientsPerAppointment
			,target.MaximumPatientsPerClinic = source.MaximumPatientsPerClinic
			,target.AsFirstAttendanceID = source.AsFirstAttendanceID
			,target.ClinicStatusID = source.ClinicStatusID
			,target.IsDisabled = source.IsDisabled
			,target.Comments = source.Comments
			,target.CreatedByID = source.CreatedByID
			,target.CreatedTime = source.CreatedTime
			,target.DepartmentID = source.DepartmentID

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿CREATE procedure [ETL].[LoadRADReferenceData] 

AS

	/******************************************************************************
	**  Name: LoadRADReferenceData
	**  Purpose: 
	**
	**  Import procedure for Radiology Reference tables rows 
	**
	**	Called by LoadRAD
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 26.06.12     MH        Altered for CMFT environment
	** 21.09.12     MH        RoomBase table required a complex primary key of Code and Hospital
	** 25.09.12     MH        More detailed information in comments
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ImportDateTimeStamp datetime


select @StartTime = getdate()

-- ExamBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADExamBase)

DELETE FROM RAD.ExamBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADExamBase 
		WHERE ExamBase.ExamCode = [$(CRIS_Import)].dbo.TImportRADExamBase.Code
		AND  [$(CRIS_Import)].dbo.TImportRADExamBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)

INSERT INTO RAD.ExamBase
	(
	 ExamCode 
	,Exam 
	,AreaCode 
	,Area 
	,ARSACLimit 
	,DiagnosticWaitingTimesReportableFlag 
	,ExamEndDate 
	,InterventionalProcedureFlag 
	,KornerBand 
	,KornerCode 
	,KornerValue 
	,KornerWeight 
	,QueryLMPFlag 
	,ModalityCode 
	,NumberOfExams 
	,ExamRealFlag 
	,IntendedExamDuration 
	,NuclearWasteCreated 
	)

SELECT
	 ExamCode = [Code]
	,Exam = [Exam Name]
	,AreaCode = [Area]
	,Area = [Area Desc]
	,ARSACLimit = [Arsac limit]
	,DiagnosticWaitingTimesReportableFlag = [DWT Exam]
	,ExamEndDate = Utility.udfConvertDate([End date])
	,InterventionalProcedureFlag = [Interventional]
	,KornerBand = [Korner band]
	,KornerCode = [Korner code]
	,KornerValue = [Korner value]
	,KornerWeight = [Korner we]
	,QueryLMPFlag = [LMP]
	,ModalityCode = [Modality]
	,NumberOfExams = [No exams]
	,ExamRealFlag = [Real exam]
	,IntendedExamDuration = [Real time]
	,NuclearWasteCreated = [Waste]
FROM
	[$(CRIS_Import)].dbo.TImportRADExamBase 
WHERE
	[$(CRIS_Import)].dbo.TImportRADExamBase.ImportDateTimeStamp = @ImportDateTimeStamp
	AND NOT EXISTS 
		(
			SELECT 1
			FROM RAD.ExamBase
			WHERE ExamBase.ExamCode = [$(CRIS_Import)].dbo.TImportRADExamBase.[Code]
		)


-- LocationBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADWardBase)

DELETE FROM RAD.LocationBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADWardBase 
		WHERE LocationBase.LocationCode = [$(CRIS_Import)].dbo.TImportRADWardBase.Code
		AND  [$(CRIS_Import)].dbo.TImportRADWardBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)
	
INSERT INTO RAD.LocationBase
(
	 LocationCode
	,Location
	,EndDate
	,SiteCode
	,PatientTypeCode
	,RequestCategoryCode
	,ReferralSourceCode
)
SELECT
	 LocationCode = [Code]
	,Location = [Ward Name]
	,EndDate = Utility.udfConvertDate([End date])
	,SiteCode = [Hospital]
	,PatientTypeCode = [Patient type]
	,RequestCategoryCode = [Request cat]
	,ReferralSourceCode = [Source]
FROM
	[$(CRIS_Import)].dbo.TImportRADWardBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.LocationBase
		WHERE LocationBase.LocationCode = [$(CRIS_Import)].dbo.TImportRADWardBase.[Code]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADWardBase.ImportDateTimeStamp = @ImportDateTimeStamp

-- RadiographerBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADRadiographerBase)

DELETE FROM RAD.RadiographerBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADRadiographerBase 
		WHERE RadiographerBase.RadiographerCode = [$(CRIS_Import)].dbo.TImportRADRadiographerBase.Code
		AND  [$(CRIS_Import)].dbo.TImportRADRadiographerBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)
	
INSERT INTO RAD.RadiographerBase
(
	 RadiographerCode
	,Radiographer
	,EndDate
	,UserID
)
SELECT
	 RadiographerCode = [Code]
	,Radiographer = [Name]
	,EndDate = Utility.udfConvertDate([End date])
	,UserID = [User ID]
FROM
	[$(CRIS_Import)].dbo.TImportRADRadiographerBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.RadiographerBase
		WHERE RadiographerBase.RadiographerCode = [$(CRIS_Import)].dbo.TImportRADRadiographerBase.[Code]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADRadiographerBase.ImportDateTimeStamp = @ImportDateTimeStamp
	AND	 [$(CRIS_Import)].dbo.TImportRADRadiographerBase.Code != ''


-- RadiologistBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADRadiologistBase)

DELETE FROM RAD.RadiologistBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADRadiologistBase 
		WHERE RadiologistBase.RadiologistCode = [$(CRIS_Import)].dbo.TImportRADRadiologistBase.Code
		AND  [$(CRIS_Import)].dbo.TImportRADRadiologistBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)
	
INSERT INTO RAD.RadiologistBase
(
	 RadiologistCode
	,Radiologist
	,EndDate
	,UserID
	,RadiologistTypeCode
)
SELECT	DISTINCT
	 RadiologistCode = [Code]
	,Radiologist = [Name]
	,EndDate =Utility.udfConvertDate([End date])
	,UserID = [User ID]
	,RadiologistTypeCode = '3'
FROM
	[$(CRIS_Import)].dbo.TImportRADRadiologistBase 
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.RadiologistBase
		WHERE RadiologistBase.RadiologistCode = [$(CRIS_Import)].dbo.TImportRADRadiologistBase.[Code]
	)
	AND [$(CRIS_Import)].dbo.TImportRADRadiologistBase.ImportDateTimeStamp = @ImportDateTimeStamp
	AND	[$(CRIS_Import)].dbo.TImportRADRadiologistBase.[Code] != ''
	AND	[$(CRIS_Import)].dbo.TImportRADRadiologistBase.[Code] is not null


-- ReferralSourceBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADReferralSourceBase)

DELETE FROM RAD.ReferralSourceBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADReferralSourceBase 
		WHERE ReferralSourceBase.ReferralSourceCode = [$(CRIS_Import)].dbo.TImportRADReferralSourceBase.Code
		AND  [$(CRIS_Import)].dbo.TImportRADReferralSourceBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)
	
INSERT INTO RAD.ReferralSourceBase
(
	 ReferralSourceCode
	,ReferralSource
	,EndDate
	,Address1
	,Address2
	,Address3
	,Address4
	,Address5 
	,ClusterCode 
	,[Courier]
	,NationalCode 
	,PCTCode
	,PostCode1
	,PostCode2
	,SHACode
	,Phone
	,ReferralSourceTypeCode
)

SELECT
	 ReferralSourceCode = [Code]
	,ReferralSource = [Name]
	,EndDate =Utility.udfConvertDate([End date])
	,Address1 = [Address 1]
	,Address2 = [Address 2]
	,Address3 = [Address 3]
	,Address4 = [Address 4]
	,Address5 = [Address 5]
	,ClusterCode = [Cluster code]
	,[Courier]
	,NationalCode = [National code]
	,PCTCode = [PCT]
	,PostCode1 = [Postcode 1]
	,PostCode2 = [Postcode 2]
	,SHACode = [SHA]
	,Phone = [Telephone]
	,ReferralSourceTypeCode = [Type]
FROM
	[$(CRIS_Import)].dbo.TImportRADReferralSourceBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.ReferralSourceBase
		WHERE ReferralSourceBase.ReferralSourceCode = [$(CRIS_Import)].dbo.TImportRADReferralSourceBase.[Code]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADReferralSourceBase.ImportDateTimeStamp = @ImportDateTimeStamp

-- ReferrerBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADReferrerBase)

DELETE FROM RAD.ReferrerBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADReferrerBase 
		WHERE ReferrerBase.ReferrerCode = [$(CRIS_Import)].dbo.TImportRADReferrerBase.Code
		AND  [$(CRIS_Import)].dbo.TImportRADReferrerBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)

--	Build temp table of ID identity column values from [$(CRIS_Import)].dbo.TImportRADReferrerBase without any duplicate Code values

SELECT		MAX(ID) AS LatestID
INTO		#TReferrerBase
FROM		[$(CRIS_Import)].dbo.TImportRADReferrerBase
WHERE		[$(CRIS_Import)].dbo.TImportRADReferrerBase.ImportDateTimeStamp = @ImportDateTimeStamp
GROUP BY	Code

INSERT INTO RAD.ReferrerBase
(
	 ReferrerCode
	,Referrer
	,[Initials]
	,StartDate
	,EndDate
	,LeadClinician
	,NationalCode
	,Specialties
	,ReferrerTypeCode
	,SendEDI
)
SELECT
	 ReferrerCode = [Code]
	,Referrer = [Name]
	,[Initials]
	,StartDate = Utility.udfConvertDate([Start date])
	,EndDate = Utility.udfConvertDate([End date])
	,LeadClinician = [Lead clinician]
	,NationalCode = [National code]
	,Specialties = [Specialities]
	,ReferrerTypeCode = [Type]
	,SendEDI = [Send EDI]
FROM
	[$(CRIS_Import)].dbo.TImportRADReferrerBase
	INNER JOIN #TReferrerBase ON [$(CRIS_Import)].dbo.TImportRADReferrerBase.ID = #TReferrerBase.LatestID -- Ensures only one row per Referrer Code
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.ReferrerBase
		WHERE ReferrerBase.ReferrerCode = [$(CRIS_Import)].dbo.TImportRADReferrerBase.[Code]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADReferrerBase.ImportDateTimeStamp = @ImportDateTimeStamp

-- RoomBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADRoomBase)

DELETE FROM RAD.RoomBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADRoomBase 
		WHERE 
				RoomBase.RoomCode	= [$(CRIS_Import)].dbo.TImportRADRoomBase.Code
			AND RoomBase.SiteCode	= [$(CRIS_Import)].dbo.TImportRADRoomBase.Hospital
		AND  [$(CRIS_Import)].dbo.TImportRADRoomBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)
	
INSERT INTO RAD.RoomBase
(
	 RoomCode
	,Room
	,DepartmentCode
	,SiteCode
	,DosageTypeCode
	,EndDate
	,ModalityCode
)
SELECT
	 RoomCode = [Code]
	,Room = [Name]
	,DepartmentCode = [Dept]
	,SiteCode = [Hospital]
	,DosageTypeCode = [Dosage typ]
	,EndDate = Utility.udfConvertDate([End date])
	,ModalityCode = [Modality] 
FROM
	[$(CRIS_Import)].dbo.TImportRADRoomBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.RoomBase
		WHERE		RoomBase.RoomCode	= [$(CRIS_Import)].dbo.TImportRADRoomBase.[Code]
				AND RoomBase.SiteCode	= [$(CRIS_Import)].dbo.TImportRADRoomBase.Hospital
	)
	AND  [$(CRIS_Import)].dbo.TImportRADRoomBase.ImportDateTimeStamp = @ImportDateTimeStamp

-- SpecialtyBase

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADSpecialtyBase)

DELETE FROM RAD.SpecialtyBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADSpecialtyBase 
		WHERE 
				 SpecialtyBase.SpecialtyCode = [$(CRIS_Import)].dbo.TImportRADSpecialtyBase.Code
			AND  [$(CRIS_Import)].dbo.TImportRADSpecialtyBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)
	
INSERT INTO RAD.SpecialtyBase
(
	 SpecialtyCode
	,Specialty
	,EndDate
	,SpecialtyGroup
)
SELECT
	 SpecialtyCode = [Code]
	,Specialty = [Name]
	,EndDate = Utility.udfConvertDate([End date])
	,SpecialtyGroup = [Group]
FROM
	[$(CRIS_Import)].dbo.TImportRADSpecialtyBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.SpecialtyBase
		WHERE SpecialtyBase.SpecialtyCode = [$(CRIS_Import)].dbo.TImportRADSpecialtyBase.[Code]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADSpecialtyBase.ImportDateTimeStamp = @ImportDateTimeStamp

-- StatusBase Table

SET @ImportDateTimeStamp = (SELECT MAX(ImportDateTimeStamp) FROM [$(CRIS_Import)].dbo.TImportRADStatusBase)

DELETE FROM RAD.StatusBase
WHERE EXISTS
	(
		SELECT 1 
		FROM [$(CRIS_Import)].dbo.TImportRADStatusBase 
		WHERE StatusBase.StatusCode = [$(CRIS_Import)].dbo.TImportRADStatusBase.Code
		AND  [$(CRIS_Import)].dbo.TImportRADStatusBase.ImportDateTimeStamp = @ImportDateTimeStamp
	)
	
INSERT INTO RAD.StatusBase
(
	 StatusCode
	,Status 
	,StatusLong
	,StatusTypeCode
	,StatusCategoryCode
	,DefaultFlag 
	,EndDate
)
SELECT DISTINCT
	 StatusCode = [Code]
	,Status = [Short desc]
	,StatusLong = [Long desc]
	,StatusTypeCode = [Type]
	,StatusCategoryCode = [Category]
	,DefaultFlag = [Default]
	,EndDate = Utility.udfConvertDate([End date])
FROM
	[$(CRIS_Import)].dbo.TImportRADStatusBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.StatusBase
		WHERE StatusBase.StatusCode = [$(CRIS_Import)].dbo.TImportRADStatusBase.[Code]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADStatusBase.ImportDateTimeStamp = @ImportDateTimeStamp


-- StatusCategory Table - Only missing codes driven from StatusBase are added

INSERT INTO RAD.StatusCategory
(
	 StatusCategoryCode
	,StatusCategory
)
SELECT DISTINCT
	 StatusCategoryCode = [Category]
	,StatusCategory = [Category] + ' - No Description'
FROM
	[$(CRIS_Import)].dbo.TImportRADStatusBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.StatusCategory
		WHERE StatusCategory.StatusCategoryCode = [$(CRIS_Import)].dbo.TImportRADStatusBase.[Category]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADStatusBase.ImportDateTimeStamp = @ImportDateTimeStamp

-- StatusType Table - Only missing codes driven from StatusBase are added

INSERT INTO RAD.StatusType
(
	 StatusTypeCode
	,StatusType
)
SELECT DISTINCT
	 StatusTypeCode = [Type]
	,StatusType = [Type] + ' - No Description'
FROM
	[$(CRIS_Import)].dbo.TImportRADStatusBase
WHERE
	NOT EXISTS 
	(
		SELECT 1
		FROM RAD.StatusType
		WHERE StatusType.StatusTypeCode = [$(CRIS_Import)].dbo.TImportRADStatusBase.[Type]
	)
	AND  [$(CRIS_Import)].dbo.TImportRADStatusBase.ImportDateTimeStamp = @ImportDateTimeStamp


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadRADReferenceData', @Stats, @StartTime

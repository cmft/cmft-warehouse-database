﻿
CREATE PROCEDURE [ETL].[LoadSymphonyAddress]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Address table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 AddressID
			,Line1
			,Line2
			,Line3
			,Line4
			,Line5
			,Postcode
			,DHACode
			,PCGCode
			,AddressTypeID
			,LinkTypeID
			,LinkID
			,CreatedByID
			,UpdatedTime
			,ExtraDate1
			,ExtraDate2
			,MoveID
			)
into
	#TLoadSymphonyAddress
from
	(
	select
		 AddressID = cast(AddressID as int)
		,Line1 = cast(Line1 as varchar(255))
		,Line2 = cast(Line2 as varchar(255))
		,Line3 = cast(Line3 as varchar(255))
		,Line4 = cast(Line4 as varchar(255))
		,Line5 = cast(Line5 as varchar(255))
		,Postcode = cast(Postcode as varchar(10))
		,DHACode = cast(DHACode as varchar(3))
		,PCGCode = cast(PCGCode as varchar(5))
		,AddressTypeID = cast(AddressTypeID as int)
		,LinkTypeID = cast(LinkTypeID as tinyint)
		,LinkID = cast(LinkID as int)
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,ExtraDate1 = cast(ExtraDate1 as datetime)
		,ExtraDate2 = cast(ExtraDate2 as datetime)
		,MoveID = cast(MoveID as int)
	from
		(
		select
			 AddressID = Address.add_addid
			,Line1 = Address.add_line1
			,Line2 = Address.add_line2
			,Line3 = Address.add_line3
			,Line4 = Address.add_line4
			,Line5 = Address.add_line5
			,Postcode = Address.add_postcode
			,DHACode = Address.add_dha
			,PCGCode = Address.add_pcg
			,AddressTypeID = Address.add_type
			,LinkTypeID = Address.add_linktype
			,LinkID = Address.add_linkid
			,CreatedByID = Address.add_createdby
			,UpdatedTime = Address.add_update
			,ExtraDate1 = Address.add_extradate1
			,ExtraDate2 = Address.add_extradate2
			,MoveID = Address.add_move
		from
			[$(Symphony)].dbo.Address

		) Encounter
	) Encounter
order by
	Encounter.AddressID

create unique clustered index #IX_TLoadSymphonyAddress on #TLoadSymphonyAddress
	(
	AddressID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Address target
using
	(
	select
		 AddressID
		,Line1
		,Line2
		,Line3
		,Line4
		,Line5
		,Postcode
		,DHACode
		,PCGCode
		,AddressTypeID
		,LinkTypeID
		,LinkID
		,CreatedByID
		,UpdatedTime
		,ExtraDate1
		,ExtraDate2
		,MoveID
		,EncounterChecksum
	from
		#TLoadSymphonyAddress
	
	) source
	on	source.AddressID = target.AddressID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 AddressID
			,Line1
			,Line2
			,Line3
			,Line4
			,Line5
			,Postcode
			,DHACode
			,PCGCode
			,AddressTypeID
			,LinkTypeID
			,LinkID
			,CreatedByID
			,UpdatedTime
			,ExtraDate1
			,ExtraDate2
			,MoveID
			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.AddressID
			,source.Line1
			,source.Line2
			,source.Line3
			,source.Line4
			,source.Line5
			,source.Postcode
			,source.DHACode
			,source.PCGCode
			,source.AddressTypeID
			,source.LinkTypeID
			,source.LinkID
			,source.CreatedByID
			,source.UpdatedTime
			,source.ExtraDate1
			,source.ExtraDate2
			,source.MoveID
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.AddressID = source.AddressID
			,target.Line1 = source.Line1
			,target.Line2 = source.Line2
			,target.Line3 = source.Line3
			,target.Line4 = source.Line4
			,target.Line5 = source.Line5
			,target.Postcode = source.Postcode
			,target.DHACode = source.DHACode
			,target.PCGCode = source.PCGCode
			,target.AddressTypeID = source.AddressTypeID
			,target.LinkTypeID = source.LinkTypeID
			,target.LinkID = source.LinkID
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.ExtraDate1 = source.ExtraDate1
			,target.ExtraDate2 = source.ExtraDate2
			,target.MoveID = source.MoveID
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿CREATE procedure [ETL].[ExtractSIDMDT]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDMDT;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDMDT
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,DateTimeOfMDT
	,ChairedBy
	,MDTMemebersPresentDTODoctor
	,MDTMemebersPresentDTOANP
	,MDTMemebersPresentDTONurse
	,MDTMemebersPresentDTOPhysio
	,MDTMemebersPresentDTOSLT
	,MDTMemebersPresentDTOOT
	,MDTMemebersPresentDTOSpecialistNurse
	,MDTMemebersPresentDTOOther
	,MDTMemebersPresentDTOOthersText
	,RehabStream
	,MedicalUpdateAndActionPlan
	,HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia
	,HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection
	,BarthelScores
	,BarthelScoresMDTFeeding
	,BarthelScoresMDTBathing
	,BarthelScoresMDTGrooming
	,BarthelScoresMDTDressing
	,BarthelScoresMDTBowels
	,BarthelScoresMDTBladder
	,BarthelScoresMDTToiletUse
	,BarthelScoresMDTTransfers
	,BarthelScoresMDTMobility
	,BarthelScoresMDTStairs
	,BarthelScoresMDTTotalScore
	,WaterlowScore
	,MUST
	,FallsRiskScore
	,ModifiedRankingScore
	,TOMsAphasia
	,TOMsDysphagia
	,MoodAlgorithmaReviewed
	,FormalMoodAssessmentRequired
	,MoodToolUsed
	,MoodAssessmentScore
	,DateTimeMoodAssessment
	,MoCAAssessmentScore
	,DateTimeMoCAAssessmentScore
	,Nursing
	,PatientImpairments
	,FunctionalStatus
	,EligibleForESD
	,EligibleForESDNo
	,ESD
	,ComplexDischarge
	,SocialServices
	,ICT
	,Homepathway
	,Reablement
	,DischargePlanningNotes
	,MDTNotes
	,Username
	,ModifiedDate
	,IsSectionComplete
	,SectionCompleteTime
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		CandidateData.SectionID
	,ActionID = 
		CandidateData.ActionID
	,DateTimeOfMDT = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/MDTDTO/DateTimeOfMDT)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ChairedBy = 
		nullif(CandidateData.Details.value('(/MDTDTO/ChairedBy)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTODoctor = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/Doctor)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOANP = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/ANP)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTONurse = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/Nurse)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOPhysio = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/Physio)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOSLT = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/SLT)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOOT = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/OT)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOSpecialistNurse = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/SpecialistNurse)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOOther = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/Other)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOOthersText = 
		nullif(CandidateData.Details.value('(/MDTDTO/MembersPresent/OthersText)[1]', 'varchar(max)'),'')
	,RehabStream = 
		nullif(CandidateData.Details.value('(/MDTDTO/RehabStream)[1]', 'varchar(max)'),'')
	,MedicalUpdateAndActionPlan = 
		nullif(CandidateData.Details.value('(/MDTDTO/MedicalUpdateAndActionPlan)[1]', 'varchar(max)'),'')
	,HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia = 
		nullif(CandidateData.Details.value('(/MDTDTO/HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia)[1]', 'varchar(max)'),'')
	,HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection = 
		nullif(CandidateData.Details.value('(/MDTDTO/HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection)[1]', 'varchar(max)'),'')
	,BarthelScores = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScores)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTFeeding = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Feeding)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTBathing = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Bathing)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTGrooming = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Grooming)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTDressing = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Dressing)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTBowels = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Bowels)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTBladder = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Bladder)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTToiletUse = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/ToiletUse)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTTransfers = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Transfers)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTMobility = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Mobility)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTStairs = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/Stairs)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTTotalScore = 
		nullif(CandidateData.Details.value('(/MDTDTO/BarthelScoresMDT/TotalScore)[1]', 'varchar(max)'),'')
	,WaterlowScore = 
		nullif(CandidateData.Details.value('(/MDTDTO/WaterlowScore)[1]', 'varchar(max)'),'')
	,MUST = 
		nullif(CandidateData.Details.value('(/MDTDTO/MUST)[1]', 'varchar(max)'),'')
	,FallsRiskScore = 
		nullif(CandidateData.Details.value('(/MDTDTO/FallsRiskScore)[1]', 'varchar(max)'),'')
	,ModifiedRankingScore = 
		nullif(CandidateData.Details.value('(/MDTDTO/ModifiedRankingScore)[1]', 'varchar(max)'),'')
	,TOMsAphasia = 
		nullif(CandidateData.Details.value('(/MDTDTO/TOMsAphasia)[1]', 'varchar(max)'),'')
	,TOMsDysphagia = 
		nullif(CandidateData.Details.value('(/MDTDTO/TOMsDysphagia)[1]', 'varchar(max)'),'')
	,MoodAlgorithmaReviewed = 
		nullif(CandidateData.Details.value('(/MDTDTO/MoodAlgorithmaReviewed)[1]', 'varchar(max)'),'')
	,FormalMoodAssessmentRequired = 
		nullif(CandidateData.Details.value('(/MDTDTO/FormalMoodAssessmentRequired)[1]', 'varchar(max)'),'')
	,MoodToolUsed = 
		nullif(CandidateData.Details.value('(/MDTDTO/MoodToolUsed)[1]', 'varchar(max)'),'')
	,MoodAssessmentScore = 
		nullif(CandidateData.Details.value('(/MDTDTO/MoodAssessmentScore)[1]', 'varchar(max)'),'')
	,DateTimeMoodAssessment = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/MDTDTO/DateTimeMoodAssessment)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,MoCAAssessmentScore = 
		nullif(CandidateData.Details.value('(/MDTDTO/MoCAAssessmentScore)[1]', 'varchar(max)'),'')
	,DateTimeMoCAAssessmentScore = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/MDTDTO/DateTimeMoCAAssessmentScore)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,Nursing = 
		nullif(CandidateData.Details.value('(/MDTDTO/Nursing)[1]', 'varchar(max)'),'')
	,PatientImpairments = 
		nullif(CandidateData.Details.value('(/MDTDTO/PatientImpairments)[1]', 'varchar(max)'),'')
	,FunctionalStatus = 
		nullif(CandidateData.Details.value('(/MDTDTO/FunctionalStatus)[1]', 'varchar(max)'),'')
	,EligibleForESD = 
		nullif(CandidateData.Details.value('(/MDTDTO/EligibleForESD)[1]', 'varchar(max)'),'')
	,EligibleForESDNo = 
		nullif(CandidateData.Details.value('(/MDTDTO/EligibleForESDNo)[1]', 'varchar(max)'),'')
	,ESD = 
		nullif(CandidateData.Details.value('(/MDTDTO/ESD)[1]', 'varchar(max)'),'')
	,ComplexDischarge = 
		nullif(CandidateData.Details.value('(/MDTDTO/ComplexDischarge)[1]', 'varchar(max)'),'')
	,SocialServices = 
		nullif(CandidateData.Details.value('(/MDTDTO/SocialServices)[1]', 'varchar(max)'),'')
	,ICT = 
		nullif(CandidateData.Details.value('(/MDTDTO/ICT)[1]', 'varchar(max)'),'')
	,Homepathway = 
		nullif(CandidateData.Details.value('(/MDTDTO/Homepathway)[1]', 'varchar(max)'),'')
	,Reablement = 
		nullif(CandidateData.Details.value('(/MDTDTO/Reablement)[1]', 'varchar(max)'),'')
	,DischargePlanningNotes = 
		nullif(CandidateData.Details.value('(/MDTDTO/DischargePlanningNotes)[1]', 'varchar(max)'),'')
	,MDTNotes = 
		nullif(CandidateData.Details.value('(/MDTDTO/MDTNotes)[1]', 'varchar(max)'),'')
	,Username = 
		CandidateData.Details.value('(/MDTDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/MDTDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,IsSectionComplete = 
		SectionComplete.Details.value('(/SectionCompletedDTO/CurrentSelection)[1]', 'varchar(max)')
	,SectionCompleteTime = 
		SectionComplete.DateTimeCreated
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
left outer join 
(
	select
		*
	from
		[$(SID)].dbo.TARNCandidateData 
	where
		SectionID = 20
and ActionID = 50
) SectionComplete
on SectionComplete.CandidateID = CandidateData.CandidateID
where
	CandidateData.SectionID = 20
and CandidateData.ActionID = 10


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDMDT', @Stats, @StartTime





﻿


CREATE proc [ETL].[ExtractMedisecDictationInpatient]

 @fromDate smalldatetime = null
, @toDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -12, getdate()))), 0), 112) 

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, 0, getdate()))), 0), 112)




insert into ETL.TImportMedisecDictationInpatient

(
	SourcePatientNo
	,AdmissionTime
	,SpellSequenceNo
	,RequestTypeCode
)

select 
	SourcePatientNo = cast(ireqpn as int)
	,AdmissionTime = 
					case
					when left(InpatientRequest.ireqtoa ,2) = 24
					then dateadd(day, 1, cast(InpatientRequest.ireqdoa as datetime))
					else cast(InpatientRequest.ireqdoa as datetime)
					end
					+ ' ' + 
					case
					when left(InpatientRequest.ireqtoa ,2) = 24
					then '00:00:00'
					when coalesce(InpatientRequest.ireqtoa, '') = ''
					then '00:00:00'
					else left(InpatientRequest.ireqtoa,2) + ':' + right(InpatientRequest.ireqtoa,2) + ':00'
					end 
	,SpellSequenceNo = cast(ireqfb as int)
	,RequestTypeCode = ireqlstatus

	
from
	[$(CMFT_MEDISEC)].dbo.InpRequest InpatientRequest


where
	case
	when left(InpatientRequest.ireqtoa ,2) = 24
	then dateadd(day, 1, cast(InpatientRequest.ireqdoa as datetime))
	else cast(InpatientRequest.ireqdoa as datetime)
	end between @from and @to
and coalesce(InpatientRequest.ireqlstatus, '') not in ('00','09') -- 00 = Letter Not Created; 09 = DNR - these are not letters, so can be excluded (DG)
and	not exists
			-- takes last ireqfb in event of duplicates - confirmed with Liam Orrell							
			(
			select
				1
			from
				[$(CMFT_MEDISEC)].dbo.InpRequest LaterInpatientRequest
			where
				LaterInpatientRequest.ireqpn = InpatientRequest.ireqpn
			and	LaterInpatientRequest.ireqdoa = InpatientRequest.ireqdoa
			and	LaterInpatientRequest.ireqtoa = InpatientRequest.ireqtoa
			and coalesce(LaterInpatientRequest.ireqlstatus, '') in ('00','09')
			and	cast(LaterInpatientRequest.ireqfb as int) > cast(InpatientRequest.ireqfb as int)
			)

select @RowsInserted = @@ROWCOUNT

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
      'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
      'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractMedisecDictationInpatient', @Stats, @StartTime





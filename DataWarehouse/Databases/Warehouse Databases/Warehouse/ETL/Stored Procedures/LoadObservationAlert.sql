﻿
CREATE PROCEDURE [ETL].[LoadObservationAlert]

as 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 @LoadStartDate = min(LastModifiedTime)
	,@LoadEndDate = max(LastModifiedTime) 
from
	ETL.TLoadObservationAlert

merge
	Observation.Alert target
using
	(
	select
		 SourceUniqueID
		,CasenoteNumber
		,DateOfBirth
		,SpecialtyID
		,LocationID
		,AdmissionSourceUniqueID
		,TypeID
		,ReasonID
		,ObservationSetSourceUniqueID
		,CreatedDate
		,CreatedTime
		,BedsideDueDate
		,BedsideDueTime
		,EscalationDate
		,EscalationTime
		,AcceptedDate
		,AcceptedTime
		,AttendedByUserID
		,AttendanceTypeID
		,Comment
		,SeverityID
		,ClinicianSeniorityID
		,AcceptanceRemindersRemaining
		,ChainSequenceNumber
		,NextReminderTime
		,ClosedDate
		,ClosedTime
		,ClosureReasonID
		,ClosedByUserID
		,CancelledByObservationSetSourceUniqueID
		,LastModifiedTime
		,InterfaceCode
	from
		ETL.TLoadObservationAlert

	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.LastModifiedTime between @LoadStartDate and @LoadEndDate

	then delete	

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SpecialtyID
			,LocationID
			,AdmissionSourceUniqueID
			,TypeID
			,ReasonID
			,ObservationSetSourceUniqueID
			,CreatedDate
			,CreatedTime
			,BedsideDueDate
			,BedsideDueTime
			,EscalationDate
			,EscalationTime
			,AcceptedDate
			,AcceptedTime
			,AttendedByUserID
			,AttendanceTypeID
			,Comment
			,SeverityID
			,ClinicianSeniorityID
			,AcceptanceRemindersRemaining
			,ChainSequenceNumber
			,NextReminderTime
			,ClosedDate
			,ClosedTime
			,ClosureReasonID
			,ClosedByUserID
			,CancelledByObservationSetSourceUniqueID
			,LastModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SpecialtyID
			,source.LocationID
			,source.AdmissionSourceUniqueID
			,source.TypeID
			,source.ReasonID
			,source.ObservationSetSourceUniqueID
			,source.CreatedDate
			,source.CreatedTime
			,source.BedsideDueDate
			,source.BedsideDueTime
			,source.EscalationDate
			,source.EscalationTime
			,source.AcceptedDate
			,source.AcceptedTime
			,source.AttendedByUserID
			,source.AttendanceTypeID
			,source.Comment
			,source.SeverityID
			,source.ClinicianSeniorityID
			,source.AcceptanceRemindersRemaining
			,source.ChainSequenceNumber
			,source.NextReminderTime
			,source.ClosedDate
			,source.ClosedTime
			,source.ClosureReasonID
			,source.ClosedByUserID
			,source.CancelledByObservationSetSourceUniqueID
			,source.LastModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not 
			isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.AdmissionSourceUniqueID = source.AdmissionSourceUniqueID
			,target.TypeID = source.TypeID
			,target.ReasonID = source.ReasonID
			,target.ObservationSetSourceUniqueID = source.ObservationSetSourceUniqueID
			,target.CreatedDate = source.CreatedDate
			,target.CreatedTime = source.CreatedTime
			,target.BedsideDueDate = source.BedsideDueDate
			,target.BedsideDueTime = source.BedsideDueTime
			,target.EscalationDate = source.EscalationDate
			,target.EscalationTime = source.EscalationTime
			,target.AcceptedDate = source.AcceptedDate
			,target.AcceptedTime = source.AcceptedTime
			,target.AttendedByUserID = source.AttendedByUserID
			,target.AttendanceTypeID = source.AttendanceTypeID
			,target.Comment = source.Comment
			,target.SeverityID = source.SeverityID
			,target.ClinicianSeniorityID = source.ClinicianSeniorityID
			,target.AcceptanceRemindersRemaining = source.AcceptanceRemindersRemaining
			,target.ChainSequenceNumber = source.ChainSequenceNumber
			,target.NextReminderTime = source.NextReminderTime
			,target.ClosedDate = source.ClosedDate
			,target.ClosedTime = source.ClosedTime
			,target.ClosureReasonID = source.ClosureReasonID
			,target.ClosedByUserID = source.ClosedByUserID
			,target.CancelledByObservationSetSourceUniqueID = source.CancelledByObservationSetSourceUniqueID
			,target.LastModifiedTime = source.LastModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

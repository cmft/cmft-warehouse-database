﻿
CREATE procedure [ETL].[BuildCWSProcessListArchive] as

insert
into
	Result.CWSNumericProcessListArchive
(
	 ResultRecno
	,Action
	,ArchiveTime
)
select
	 ResultRecno
	,Action
	,ArchiveTime = getdate()
from
	Result.CWSNumericProcessList;

truncate table Result.CWSNumericProcessList


insert
into
	Result.CWSTextProcessListArchive
(
	 ResultRecno
	,Action
	,ArchiveTime
)
select
	 ResultRecno
	,Action
	,ArchiveTime = getdate()
from
	Result.CWSTextProcessList;

truncate table Result.CWSTextProcessList



﻿CREATE proc [ETL].[LoadBedComplementWardConfiguration]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

--populate the reference tables
insert
into
	BedComplement.WardType
(
	WardType
)
select distinct
	WardType
from
	ETL.TLoadBedComplementWardConfiguration
where
	not exists
	(
	select
		1
	from
		BedComplement.WardType
	where
		WardType.WardType = TLoadBedComplementWardConfiguration.WardType
	)


insert
into
	BedComplement.SpecialtyGroup
(
	SpecialtyGroup
)
select distinct
	SpecialtyGroup
from
	ETL.TLoadBedComplementWardConfiguration
where
	not exists
	(
	select
		1
	from
		BedComplement.SpecialtyGroup
	where
		SpecialtyGroup.SpecialtyGroup = TLoadBedComplementWardConfiguration.SpecialtyGroup
	)


truncate table BedComplement.WardConfiguration

insert into BedComplement.WardConfiguration
(
	 WardID
	,WardTypeID
	,Schedule
	,DivisionCode
	,SpecialtyGroupID
	,Comment
	,StartDate
	,EndDate
)
select 	
	 WardID
	,WardType.WardTypeID
	,Schedule
	,DivisionCode
	,SpecialtyGroup.SpecialtyGroupID
	,Comment
	,StartDate
	,EndDate
from 
	ETL.TLoadBedComplementWardConfiguration

inner join BedComplement.WardType
on	WardType.WardType = TLoadBedComplementWardConfiguration.WardType

inner join BedComplement.SpecialtyGroup
on	SpecialtyGroup.SpecialtyGroup = TLoadBedComplementWardConfiguration.SpecialtyGroup


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime

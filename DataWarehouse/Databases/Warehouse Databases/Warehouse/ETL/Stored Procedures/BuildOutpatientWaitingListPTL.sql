﻿
CREATE     procedure [ETL].[BuildOutpatientWaitingListPTL]
	@CensusDate smalldatetime
as



declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @RowsDeleted Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()
select @RowsInserted = 0
select @RowsDeleted = 0


-- delete any for this census date
delete from OP.WaitingListPTL
where
	exists
(
	select
		1
	from
		OP.WaitingList
	where
		WaitingList.EncounterRecno = OP.WaitingListPTL.EncounterRecno
	and	WaitingList.CensusDate = @CensusDate
)

select
	@RowsDeleted = @RowsDeleted + @@ROWCOUNT

-- delete orphaned records
delete from OP.WaitingListPTL
where
	not exists
(
	select
		1
	from
		OP.WaitingList
	where
		WaitingList.EncounterRecno = OP.WaitingListPTL.EncounterRecno
)

select
	@RowsDeleted = @RowsDeleted + @@ROWCOUNT


insert into OP.WaitingListPTL
(
	EncounterRecno
)
select
	WL.EncounterRecno
from
	OP.WaitingList WL

----new SoR only
--inner join PAS.SourceOfReferral
--on	WL.SourceOfReferralCode = SourceOfReferral.SourceOfReferralCode
--and	SourceOfReferral.NewFlag = 1

where
	WL.CensusDate = @CensusDate
and
	(
		(
			WL.FirstAttendanceFlag = 1
		)
		or
		(
			WL.AppointmentDate is null
		)
--		or
--		(
--			WL.AppointmentCategoryCode = '1ST'
--		and	WL.AppointmentStatusCode = 'NR'
--		and	WL.CancelledBy is null
--		)
--		or	(
--			WL.AppointmentCategoryCode = '1ST'
--		and	WL.AppointmentStatusCode = 'NR'
--		and	WL.CancelledBy is not null
--		and	WL.LastAppointmentFlag = 1
--		)
--		or
--		(
--			WL.QM08StartWaitDate = WL.AppointmentDate
--		and	WL.AppointmentStatusCode is not null
--		and	coalesce(WL.CancelledBy, 'X') != 'P'
--		and	WL.AppointmentDate <= WL.CensusDate
--		and	WL.LastAppointmentFlag = 1
--		)
--		or
--		(
--			WL.QM08StartWaitDate = WL.AppointmentDate
--		and	WL.AppointmentStatusCode = 'NR'
--		and	WL.AppointmentDate <= WL.CensusDate
--		and	WL.LastAppointmentFlag = 1
--		)
	)

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

select
	WL1.EncounterRecno
into
	#List
from
	OP.WaitingList WL1

inner join OP.WaitingListPTL
on	OP.WaitingListPTL.EncounterRecno = WL1.EncounterRecno

where
	WL1.EncounterRecno in 
	(
	select
		WL2.EncounterRecno
	from
		OP.WaitingList WL2
	
	inner join OP.WaitingListPTL
	on	OP.WaitingListPTL.EncounterRecno = WL2.EncounterRecno
	
	where
		WL2.BookedDate < 
		(
		select
			max(BookedDate)
		from
			OP.WaitingList WL3
	
		inner join OP.WaitingListPTL
		on	OP.WaitingListPTL.EncounterRecno = WL3.EncounterRecno
		
		where
			WL3.SourcePatientNo = WL2.SourcePatientNo
		and	WL3.SourceEncounterNo = WL2.SourceEncounterNo
		and	WL3.CensusDate = WL2.CensusDate
		)
	
	and	WL2.SourcePatientNo = WL1.SourcePatientNo
	and	WL2.SourceEncounterNo = WL1.SourceEncounterNo
	and	WL2.CensusDate = WL1.CensusDate
	)

and	WL1.CensusDate = @CensusDate


delete from OP.WaitingListPTL

from
	OP.WaitingListPTL

inner join #List
on	#List.EncounterRecno = OP.WaitingListPTL.EncounterRecno


select
	@RowsInserted = @RowsInserted - @@ROWCOUNT

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted) +
	', Inserted ' + CONVERT(varchar(10), @RowsInserted) + 
	', Net Change '  + CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + 
	', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'BuildOutpatientWaitingListPTL', @Stats, @StartTime


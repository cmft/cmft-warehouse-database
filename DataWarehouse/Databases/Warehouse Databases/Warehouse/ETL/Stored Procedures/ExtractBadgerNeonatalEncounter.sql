﻿
CREATE proc [ETL].[ExtractBadgerNeonatalEncounter]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

insert into ETL.TImportNeonatalEncounter
(
EpisodeID
,NHSNumber
,CasenoteNumber
,SourceUniqueID
,CareLocationID
,EpisodeTypeCode
,Surname
,Forename
,SexCode
,BirthTime
,GestationWeeks
,GestationDays
,BirthWeight
,BirthLength
,BirthHeadCircumference
,BirthOrder
,FetusNumber
,BirthSummary
,EpisodeNumber
,AdmissionDate
,AdmissionTime
,AdmissionFromSiteCode
,AdmissionSourceCode
,Readmission
,AdmissionTypeCode
,SourceOfReferralCode
,AdmissionReasonCode
,ProviderCode
,NetworkID
,ConsultationWithParents
,AdmissionTemperatureMeasured
,AdmissionTemperature
,AdmissionTemperatureTime
,AdmissionBloodPressure
,AdmissionHeartRate
,AdmissionRespiratoryRate
,AdmissionSaO2
,AdmissionBloodGlucose
,AdmissionWeight
,AdmissionHeadCircumference
,TemperatureNotRecordable
,DischargeDate
,DischargeTime
,DischargeDestinationCode
,DischargeHospitalCode
,DischargeDestinationWardCode
,DischargeWeight
,DischargeHeadCircumference
,DischargeMilkCode
,DischargeFeedingCode
,HomeTubeFeeding
,DischargeOxygen
,TransferReasonCode
,TransferNetworkCode
,CauseOfDeathCode
,PostMortemConsent
,PostMortemCode
,PostMortemConfirmedNECCode
,EpisodeSummary
,VentilationDays
,CPAPDays
,OxygenDays
,OxygenDaysNoCPAPVentilation
,OxygenLastGestation
,OxygenLastTime
,IntensiveCareDays2001
,HighDependancyCareDays2001
,SpecialCareDays2001
,IntensiveCareDays2011
,HighDependancyCareDays2011
,SpecialCareDays2011
,NormalCareDays2011
,IntensiveCareHRGDays
,HighDependancyHRGDays
,SpecialCareNoCarerResidentHRGDays
,SpecialCareWithCarerResidentHRGDays
,NormalCareHRGDays
,NeonatalUnitDays
,TransitionalCareDays
,PostnatalWardDays
,OtherObstetricAreaDays
,LocnNNUPortion
,LocnTCPoriton
,LocnPNWPortion
,DrugsDuringStay
,DiagnosisDuringStay
,MotherNHSNumber
,MotherCasenoteNumber
,MotherSurname
,MotherForename
,MotherReligionCode
,MotherMaritalStatusCode
,MotherOccupation
,MotherBloodGroup
,MotherDateOfBirth
,MotherPostCode
,MotherEthnicCategoryCode
,MotherHaemoglobinoapthyCode
,MotherMedicalProblemCode
,MotherDiabetesCode
,MotherHepatitisBCode
,MotherHepatitisBHighRiskCode
,MotherHIVCode
,MotherRubellaScreen
,MotherSyphilisCode
,MotherHepatitisCCode
,MotherMotherHepatitisCScreen
,MotherSyphilisVDRLScreenCode
,MotherSyphilisTPHAScreenCode
,MaternalPyrexiaInLabour
,MotherIntrapartumAntibiotics
,MeconiumStainedLiquor
,MembraneRuptureTime
,MotherAntenatalScanCode
,ParentsConsanguinous
,MotherDrugAbuseCode
,MotherSmoker
,MotherCigarettesPerDay
,MotherAlcoholUseCode
,MotherPregnancyProblemCode
,LastMenstrualPeriodDate
,ExpectedDateOfDelivery
,PreviousPregnancies
,FatherOccupation
,FatherAge
,FatherEthnicCategoryCode
,CalculatedGestationWeeks
,CalculatedGestationDays
,BookingLocationCode
,AntenatalSteroidCode
,AntenatalSteroid
,AntenatalSteroidCourseCode
,LabourOnsetCode
,LabourPresentationCode
,LabourDeliveryCode
,PlaceOfBirthCode
,LabourDrugCode
,StaffPresentAtResuscitation
,AgparScoreOneMinute
,AgparScoreFiveMinutes
,AgparScoreTenMinutes
,BloodTypeCode
,CRIBIIScore
,Outcome
,VitaminKCode
,VitaminKRouteCode
,CordArterialpH
,CordVenouspH
,CordPcO2Arterial
,CordPcO2Venous
,CordArterialBE
,CordVenousBE
,CordClampingCode
,CordClampingTimeMinute
,CordClampingTimeSecond
,CordStripping
,ResuscitationCode
,ResuscitationSurfactantCode
,FirstGaspCode
,SpontaneousRespirationCode
,OffensiveLiquor
,GPCode
,GPPracticeCode
,SeizureCode
,HIEGradeCode
,AnticonvulsantDrug
,Pneumothorax
,NecrotisingEnterocolitis
,NeonatalAbstinence
,ROPScreenDate
,ROPSurgeryDate
,Dexamethasone
,PDAIndomethacin
,PDAIbuprofen
,PDASurgery
,PDADischarge
,UACDate
,UVCDate
,LongLineTime
,PeripheralArterialLineDate
,SurgicalLineDate
,ParenteralNutritionDays
,FirstHeadScanDate
,FirstHeadScanResult
,LastHeadScanDate
,LastHeadScanResult
,CongenitalAnomaly
,VPShuntDate
,FirstBloodCultureDate
,FirstBloodCultureResult
,FirstCSFCultureDate
,FirstCSFCultureResult
,FirstUrineCultureDate
,FirstUrineCultureResult
,ExchangeTransfusion
,Tracheostomy
,PulmonaryVasodilatorDate
,PulmonaryVasodilatorDrug
,Inotrope
,FirstInotropeDate
,PeritonealDialysis
,DischargeApnoeaCardioSat
,Gastroschisis
,Cooled
,Consultant
,LastUpdatedTime
,ModifiedTime
)

select
	EpisodeID 
	,NHSNumber 
	,CasenoteNumber
	,SourceUniqueID 
	,CareLocationID
	,EpisodeTypeCode 
	,Surname 
	,Forename 
	,SexCode 
	,BirthTime 
	,GestationWeeks
	,GestationDays
	,BirthWeight
	,BirthLength
	,BirthHeadCircumference
	,BirthOrder
	,FetusNumber
	,BirthSummary
	,EpisodeNumber
	,AdmissionDate 
	,AdmissionTime 
	,AdmissionFromSiteCode
	,AdmissionSourceCode 
	,Readmission
	,AdmissionTypeCode
	,SourceOfReferralCode 
	,AdmissionReasonCode 
	,ProviderCode
	,NetworkID
	,ConsultationWithParents
	,AdmissionTemperatureMeasured 
	,AdmissionTemperature 
	,AdmissionTemperatureTime
	,AdmissionBloodPressure
	,AdmissionHeartRate 
	,AdmissionRespiratoryRate 
	,AdmissionSaO2 
	,AdmissionBloodGlucose
	,AdmissionWeight 
	,AdmissionHeadCircumference 
	,TemperatureNotRecordable
	,DischargeDate
	,DischargeTime 
	,DischargeDestinationCode 
	,DischargeHospitalCode
	,DischargeDestinationWardCode 
	,DischargeWeight
	,DischargeHeadCircumference
	,DischargeMilkCode 
	,DischargeFeedingCode 
	,HomeTubeFeeding
	,DischargeOxygen
	,TransferReasonCode 
	,TransferNetworkCode 
	,CauseOfDeathCode 
	,PostMortemConsent
	,PostMortemCode 
	,PostMortemConfirmedNECCode 
	,EpisodeSummary
	,VentilationDays
	,CPAPDays
	,OxygenDays
	,OxygenDaysNoCPAPVentilation
	,OxygenLastGestation
	,OxygenLastTime
	,IntensiveCareDays2001 
	,HighDependancyCareDays2001 
	,SpecialCareDays2001
	,IntensiveCareDays2011 
	,HighDependancyCareDays2011 
	,SpecialCareDays2011
	,NormalCareDays2011
	,IntensiveCareHRGDays
	,HighDependancyHRGDays 
	,SpecialCareNoCarerResidentHRGDays
	,SpecialCareWithCarerResidentHRGDays 
	,NormalCareHRGDays
	,NeonatalUnitDays 
	,TransitionalCareDays 
	,PostnatalWardDays 
	,OtherObstetricAreaDays
	,LocnNNUPortion
	,LocnTCPoriton
	,LocnPNWPortion
	,DrugsDuringStay
	,DiagnosisDuringStay
	,MotherNHSNumber 
	,MotherCasenoteNumber
	,MotherSurname 
	,MotherForename 
	,MotherReligionCode 
	,MotherMaritalStatusCode 
	,MotherOccupation 
	,MotherBloodGroup 
	,MotherDateOfBirth
	,MotherPostCode
	,MotherEthnicCategoryCode
	,MotherHaemoglobinoapthyCode
	,MotherMedicalProblemCode 
	,MotherDiabetesCode
	,MotherHepatitisBCode 
	,MotherHepatitisBHighRiskCode
	,MotherHIVCode 
	,MotherRubellaScreen 
	,MotherSyphilisCode 
	,MotherHepatitisCCode
	,MotherMotherHepatitisCScreen
	,MotherSyphilisVDRLScreenCode 
	,MotherSyphilisTPHAScreenCode 
	,MaternalPyrexiaInLabour 
	,MotherIntrapartumAntibiotics 
	,MeconiumStainedLiquor
	,MembraneRuptureTime
	,MotherAntenatalScanCode 
	,ParentsConsanguinous
	,MotherDrugAbuseCode 
	,MotherSmoker
	,MotherCigarettesPerDay 
	,MotherAlcoholUseCode
	,MotherPregnancyProblemCode
	,LastMenstrualPeriodDate
	,ExpectedDateOfDelivery
	,PreviousPregnancies 
	,FatherOccupation
	,FatherAge 
	,FatherEthnicCategoryCode 
	,CalculatedGestationWeeks
	,CalculatedGestationDays 
	,BookingLocationCode
	,AntenatalSteroidCode 
	,AntenatalSteroid 
	,AntenatalSteroidCourseCode 
	,LabourOnsetCode 
	,LabourPresentationCode 
	,LabourDeliveryCode 
	,PlaceOfBirthCode
	,LabourDrugCode 
	,StaffPresentAtResuscitation 
	,AgparScoreOneMinute
	,AgparScoreFiveMinutes
	,AgparScoreTenMinutes 
	,BloodTypeCode 
	,CRIBIIScore
	,Outcome 
	,VitaminKCode 
	,VitaminKRouteCode 
	,CordArterialpH
	,CordVenouspH
	,CordPcO2Arterial
	,CordPcO2Venous
	,CordArterialBE
	,CordVenousBE
	,CordClampingCode 
	,CordClampingTimeMinute
	,CordClampingTimeSecond
	,CordStripping
	,ResuscitationCode 
	,ResuscitationSurfactantCode 
	,FirstGaspCode
	,SpontaneousRespirationCode
	,OffensiveLiquor
	,GPCode
	,GPPracticeCode
	,SeizureCode 
	,HIEGradeCode
	,AnticonvulsantDrug
	,Pneumothorax 
	,NecrotisingEnterocolitis
	,NeonatalAbstinence
	,ROPScreenDate
	,ROPSurgeryDate 
	,Dexamethasone
	,PDAIndomethacin
	,PDAIbuprofen
	,PDASurgery
	,PDADischarge
	,UACDate 
	,UVCDate 
	,LongLineTime
	,PeripheralArterialLineDate
	,SurgicalLineDate 
	,ParenteralNutritionDays
	,FirstHeadScanDate 
	,FirstHeadScanResult 
	,LastHeadScanDate 
	,LastHeadScanResult 
	,CongenitalAnomaly  
	,VPShuntDate 
	,FirstBloodCultureDate 
	,FirstBloodCultureResult
	,FirstCSFCultureDate 
	,FirstCSFCultureResult 
	,FirstUrineCultureDate
	,FirstUrineCultureResult
	,ExchangeTransfusion
	,Tracheostomy
	,PulmonaryVasodilatorDate 
	,PulmonaryVasodilatorDrug 
	,Inotrope 
	,FirstInotropeDate
	,PeritonealDialysis
	,DischargeApnoeaCardioSat
	,Gastroschisis
	,Cooled
	,Consultant
	,LastUpdatedTime
	,ModifiedTime 
from
	(
	select --top 100
		EpisodeID = EntityID
		,NHSNumber = NationalIDBaby 
		--,NationalIDBabyAnon
		,CasenoteNumber = HospitalIDBaby
		,SourceUniqueID = BadgerUniqueID
		,CareLocationID
		--,CareLocationName
		,EpisodeTypeCode = EpisodeType
		,Surname = SurnameBaby
		,Forename = ForenameBaby
		,SexCode = Sex
		,BirthTime = BirthTimeBaby
		,GestationWeeks
		,GestationDays
		,BirthWeight = Birthweight
		,BirthLength
		,BirthHeadCircumference
		,BirthOrder
		,FetusNumber
		,BirthSummary
		,EpisodeNumber
		,AdmissionDate = cast(AdmitTime as date)
		,AdmissionTime = AdmitTime
		--,AdmitFromName
		,AdmissionFromSiteCode = AdmitFromNHSCode
		,AdmissionSourceCode = AdmissionSource
		,Readmission
		,AdmissionTypeCode = AdmitType
		,SourceOfReferralCode = Referral
		,AdmissionReasonCode = AdmitPrincipalReason
		--,ProviderName
		,ProviderCode = ProviderNHSCode
		--,NetworkName
		,NetworkID = NetworkB3ID
		,ConsultationWithParents
		,AdmissionTemperatureMeasured = AdmitTemperatureMeasured
		,AdmissionTemperature = AdmitTemperature
		,AdmissionTemperatureTime = AdmitTemperatureTime
		,AdmissionBloodPressure = AdmitBloodPressure
		,AdmissionHeartRate = AdmitHeartRate
		,AdmissionRespiratoryRate = AdmitRespiratoryRate
		,AdmissionSaO2 = AdmitSaO2
		,AdmissionBloodGlucose = AdmitBloodGlucose
		,AdmissionWeight = AdmitWeight
		,AdmissionHeadCircumference = AdmitHeadCircumference
		,TemperatureNotRecordable = TempNotRecordable
		,DischargeDate = cast(DischTime as date)
		,DischargeTime = DischTime
		,DischargeDestinationCode = DischargeDestination
		--,DischargeHospitalName
		,DischargeHospitalCode
		,DischargeDestinationWardCode = DischargeDestinationWard
		,DischargeWeight
		,DischargeHeadCircumference
		,DischargeMilkCode = DischargeMilk
		,DischargeFeedingCode = DischargeFeeding
		,HomeTubeFeeding
		,DischargeOxygen
		,TransferReasonCode = TransferReason
		,TransferNetworkCode = TransferNetwork
		,CauseOfDeathCode = DiedCause
		,PostMortemConsent = DiedPMConsent
		,PostMortemCode = DiedPMDone
		,PostMortemConfirmedNECCode = DiedPostmortemConfirmationNEC
		,EpisodeSummary
		,VentilationDays
		,CPAPDays
		,OxygenDays
		,OxygenDaysNoCPAPVentilation = OxygenDaysNoVent
		,OxygenLastGestation
		,OxygenLastTime
		,IntensiveCareDays2001 = ICCareDays
		,HighDependancyCareDays2001 = HDCareDays
		,SpecialCareDays2001 = SCCareDays
		,IntensiveCareDays2011 = ICCareDays2011
		,HighDependancyCareDays2011 = HDCareDays2011
		,SpecialCareDays2011 = SCCareDays2011
		,NormalCareDays2011
		,IntensiveCareHRGDays = HRG1
		,HighDependancyHRGDays = HRG2
		,SpecialCareNoCarerResidentHRGDays = HRG3
		,SpecialCareWithCarerResidentHRGDays = HRG4
		,NormalCareHRGDays = HRG5
		,NeonatalUnitDays = LocnNNUDays
		,TransitionalCareDays = LocnTCDays
		,PostnatalWardDays = LocnPNWDays
		,OtherObstetricAreaDays = LocnOBSDays
		,LocnNNUPortion
		,LocnTCPoriton
		,LocnPNWPortion
		,DrugsDuringStay
		,DiagnosisDuringStay
		,MotherNHSNumber = NationalIDMother
		,MotherCasenoteNumber = HospitalIDMother
		,MotherSurname = SurnameMother
		,MotherForename = ForenameMother
		,MotherReligionCode = ReligionMother
		,MotherMaritalStatusCode = MaritalStatusMother
		,MotherOccupation = OccupationMother
		,MotherBloodGroup = BloodGroupMother
		,MotherDateOfBirth = cast(BirthDateMother as date)
		--,AgeMother
		--,AddressMother
		,MotherPostCode = PostCodeMother
		,MotherEthnicCategoryCode = EthnicityMother
		,MotherHaemoglobinoapthyCode = HaemoglobinopathyMother
		,MotherMedicalProblemCode = ProblemsMedicalMother
		,MotherDiabetesCode = DiabetesMother
		,MotherHepatitisBCode = HepBMother
		,MotherHepatitisBHighRiskCode = HepBMotherHighRisk
		,MotherHIVCode = HivMother
		,MotherRubellaScreen = RubellaScreenMother
		,MotherSyphilisCode = SyphilisScreenMother
		,MotherHepatitisCCode = MumHCV
		,MotherMotherHepatitisCScreen = HepCPCRMother
		,MotherSyphilisVDRLScreenCode = MumVDRL
		,MotherSyphilisTPHAScreenCode = MumTPHA
		,MaternalPyrexiaInLabour = MaternalPyrexiaInLabour38c
		,MotherIntrapartumAntibiotics = IntrapartumAntibioticsGiven
		,MeconiumStainedLiquor
		,MembraneRuptureTime = MembraneRuptureDate
		----,MembranerupturedDuration
		,MotherAntenatalScanCode = AntenatalScanMother
		,ParentsConsanguinous
		,MotherDrugAbuseCode = DrugsAbusedMother
		,MotherSmoker = SmokingMother
		,MotherCigarettesPerDay = CigarettesMother
		,MotherAlcoholUseCode = AlcoholMother
		,MotherPregnancyProblemCode = ProblemsPregnancyMother
		,LastMenstrualPeriodDate = LMP
		,ExpectedDateOfDelivery = EDDMother
		,PreviousPregnancies = PreviousPregnanciesNumber
		,FatherOccupation = OccupationFather
		,FatherAge = 
				nullif(
						AgeFather
						,'Age'
						)
		,FatherEthnicCategoryCode = EthnicityFather
		,CalculatedGestationWeeks = GestationWeeksCalculated
		,CalculatedGestationDays = GestationDaysCalculated
		--,BookingLocation = BookingName
		,BookingLocationCode = BookingNHSCode
		,AntenatalSteroidCode = SteroidsAntenatalGiven
		,AntenatalSteroid = SteroidsName
		,AntenatalSteroidCourseCode = SteroidsAntenatalCourses
		,LabourOnsetCode = LabourOnset
		,LabourPresentationCode = LabourPresentation
		,LabourDeliveryCode = LabourDelivery
		--,PlaceOfBirthName
		,PlaceOfBirthCode = PlaceOfBirthNHSCode
		,LabourDrugCode = DrugsInLabour
		,StaffPresentAtResuscitation = StaffAtResus
		,AgparScoreOneMinute = Apgar1
		,AgparScoreFiveMinutes = Apgar5
		,AgparScoreTenMinutes = Apgar10
		,BloodTypeCode = BabyBloodType
		,CRIBIIScore = Crib2Score
		,Outcome = FinalNNUOutcome
		,VitaminKCode = VitaminKGiven
		,VitaminKRouteCode = VitaminKRoute
		,CordArterialpH
		,CordVenouspH
		,CordPcO2Arterial
		,CordPcO2Venous
		,CordArterialBE
		,CordVenousBE
		,CordClampingCode = CordClamping
		,CordClampingTimeMinute
		,CordClampingTimeSecond
		,CordStripping
		,ResuscitationCode = Resuscitation
		,ResuscitationSurfactantCode = ResusSurfactant
		,FirstGaspCode = FirstGasp
		,SpontaneousRespirationCode = SpontaneousRespiration
		,OffensiveLiquor
		,GPCode
		--,GPName
		--,GPAddress
		--,GPPostCode
		,GPPracticeCode
		--,GPPCTCode
		--,GPPCTName
		--,GPCCGCode
		--,GPCCGName
		,SeizureCode = Seizures
		,HIEGradeCode = HIEGrade
		,AnticonvulsantDrug = Anticonvulsants
		,Pneumothorax = Pneumothorax
		,NecrotisingEnterocolitis
		,NeonatalAbstinence
		,ROPScreenDate = cast(ROPScreenDate as date)
		,ROPSurgeryDate  = cast(ROPSurgeryDate as date)
		,Dexamethasone
		,PDAIndomethacin
		,PDAIbuprofen
		,PDASurgery
		,PDADischarge
		,UACDate = cast(UACTime as date)
		,UVCDate = cast(UVCTime as date)
		,LongLineTime = cast(LongLineTime as date)
		,PeripheralArterialLineDate = cast(PeripheralArterialLineTime as date)
		,SurgicalLineDate = cast(SurgicalLineTime as date)
		,ParenteralNutritionDays
		,FirstHeadScanDate = cast(HeadScanFirstTime as date)
		,FirstHeadScanResult = HeadScanFirstResult
		,LastHeadScanDate = cast(HeadScanLastTime as date)
		,LastHeadScanResult = HeadScanLastResult
		,CongenitalAnomaly  = CongenitalAnomalies
		,VPShuntDate = cast(VPShuntTime as date)
		,FirstBloodCultureDate = cast(BloodCultureFirstTime as date)
		,FirstBloodCultureResult = BloodCultureFirstResult
		,FirstCSFCultureDate = cast(CSFCultureFirstTime as date)
		,FirstCSFCultureResult = CSFCultureFirstResult
		,FirstUrineCultureDate = cast(UrineCultureFirstTime as date)
		,FirstUrineCultureResult = UrineCultureFirstResult
		,ExchangeTransfusion
		,Tracheostomy
		,PulmonaryVasodilatorDate = cast(PulmonaryVasodilatorTime as date)
		,PulmonaryVasodilatorDrug = PulmonaryVasodilatorDrugs
		,Inotrope = Inotropes
		,FirstInotropeDate = cast(InotropesFirstTime as date)
		,PeritonealDialysis
		,DischargeApnoeaCardioSat
		,Gastroschisis = gastroschisis
		,Cooled
		,Consultant
		/* Following columns not in data dictionary? */
		--,FirstConsultationWithParents
		--,ReceivedMothersMilkDuringAdmission
		--,DischargeLength
		--,PrincipalDiagnosisAtDischarge
		--,ActiveProblemsAtDischarge
		--,PrincipleProceduresDuringStay
		--,RespiratoryDiagnoses
		--,CardiovascularDiagnoses
		--,GastrointestinalDiagnoses
		--,NeurologyDiagnoses
		--,ROPDiagnosis
		--,HaemDiagnoses
		--,RenalDiagnoses
		--,SkinDiagnoses
		--,MetabolicDiagnoses
		--,InfectionsDiagnoses
		--,SocialIssues
		--,ResearchStudy
		--,ResearchStudyDrugs
		--,DayOneLocationOfCare
		--,BirthCareLocationName
		--,UnitResponsibleFor2YearFollowUp
		--,CordLactate
		--,Badger3UniqueID
		--,ROPScreenFirstDateDueStart
		--,ROPScreenFirstDateDueEnd
		--,ROPFirstScreenStart
		--,ROPFirstScreenEnd
		--,LSOA
		--,DateOfFirstExamination
		--,DateOfRoutineNeonatalExamination
		,LastUpdatedTime = LastUpdate 
		,ModifiedTime = RecordTimestamp
	from
		[$(Bnet_DSS_business)].bnf_dbsync.NNUEpisodes
	--where
	--	cast(RecordTimestamp as date) between @FromDate and @ToDate

	) Encounter

select
	@RowsInserted = @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

--EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

	
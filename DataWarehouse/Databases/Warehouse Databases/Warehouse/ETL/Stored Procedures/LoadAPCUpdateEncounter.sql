﻿
CREATE PROCEDURE [ETL].[LoadAPCUpdateEncounter]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select
	@StartTime = getdate()

TRUNCATE TABLE APCUpdate.Encounter;

INSERT INTO APCUpdate.Encounter
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,IsWellBabyFlag
	,PASHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode
	,EpisodicGpPracticeCode
	,PCTCode
	,LocalityCode
	,FirstEpisodeInSpellIndicator
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
	,DecidedToAdmitDate
	,ResidencePCTCode
	,PatientCategoryCode
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,IsWellBabyFlag
	,PASHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode
	,EpisodicGpPracticeCode
	,PCTCode
	,LocalityCode
	,FirstEpisodeInSpellIndicator
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
	,DecidedToAdmitDate
	,ResidencePCTCode
	,PatientCategoryCode

	,Created = getdate()
	,ByWhom = system_user
from
	ETL.TLoadAPCUpdateEncounter


SELECT @RowsInserted = @@Rowcount

--finally update discharge details on other fces in this spell
--we would normally extract from PAS based on DischargeDate but as there is no useful index
--on INQUIRE we have to use EpisodeEndDate
update
	APCUpdate.Encounter
set
	 DateOnWaitingList = TLoadAPCUpdateEncounter.DateOnWaitingList
	,AdmissionDate = TLoadAPCUpdateEncounter.AdmissionDate
	,DischargeDate = TLoadAPCUpdateEncounter.DischargeDate
	,SiteCode = TLoadAPCUpdateEncounter.SiteCode
	,AdmissionMethodCode = TLoadAPCUpdateEncounter.AdmissionMethodCode
	,AdmissionSourceCode = TLoadAPCUpdateEncounter.AdmissionSourceCode
	,ManagementIntentionCode = TLoadAPCUpdateEncounter.ManagementIntentionCode
	,DischargeMethodCode = TLoadAPCUpdateEncounter.DischargeMethodCode
	,DischargeDestinationCode = TLoadAPCUpdateEncounter.DischargeDestinationCode
	,TransferFrom = TLoadAPCUpdateEncounter.TransferFrom
	,AdminCategoryCode = TLoadAPCUpdateEncounter.AdminCategoryCode
	,LocalAdminCategoryCode = TLoadAPCUpdateEncounter.LocalAdminCategoryCode
	,FirstRegDayOrNightAdmit = TLoadAPCUpdateEncounter.FirstRegDayOrNightAdmit
	,AdmissionTime = TLoadAPCUpdateEncounter.AdmissionTime
	,DischargeTime = TLoadAPCUpdateEncounter.DischargeTime
	,ExpectedLOS = TLoadAPCUpdateEncounter.ExpectedLOS
	,CancelledElectiveAdmissionFlag = TLoadAPCUpdateEncounter.CancelledElectiveAdmissionFlag
from
	ETL.TLoadAPCUpdateEncounter
where
	TLoadAPCUpdateEncounter.SourcePatientNo = Encounter.SourcePatientNo
and	TLoadAPCUpdateEncounter.SourceSpellNo = Encounter.SourceSpellNo

--now copy RTT Current Status back to historical fces
update APCUpdate.Encounter
set
	 RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
from
	ETL.TLoadAPCUpdateEncounter TEncounter
where
	TEncounter.RTTPathwayID = Encounter.RTTPathwayID


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins, Period from ' 

EXEC Utility.WriteAuditLogEvent 'LoadAPCUpdateEncounter', @Stats, @StartTime


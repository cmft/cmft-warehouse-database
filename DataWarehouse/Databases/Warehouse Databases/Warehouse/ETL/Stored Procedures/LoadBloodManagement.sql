﻿




CREATE Procedure [ETL].[LoadBloodManagement] --'20120101','20140930'
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as


declare
	@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@StartTime datetime
	,@Elapsed int
	,@Stats varchar(255)
	,@from smalldatetime
	,@to smalldatetime
	
select @StartTime = getdate()

select @from = coalesce(@fromDate, cast(cast(dateadd(month, -3, getdate())as date) as datetime))
select @to = coalesce(@toDate, cast(cast(dateadd(day, -1, getdate())as date)as datetime)+'23:59')


exec ETL.ExtractBloodTrackBloodManagementTransaction @from, @to
exec ETL.LoadBloodManagementTransaction

exec ETL.ExtractRecallBloodManagementCellSalvage @from, @to
exec ETL.LoadBloodManagementCellSalvage

exec ETL.ExtractRecallBloodManagementDrugAdministered @from, @to
exec ETL.LoadBloodManagementDrugAdministered



exec ETL.LoadBloodManagementReferenceData


select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




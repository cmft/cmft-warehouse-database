﻿
CREATE procedure [ETL].[LoadRenalRecipientTransplantStatusEvents] (@from date = '19000101', @to date = '21000101') as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

declare @MergeSummary TABLE(Action nvarchar(10));

merge Renal.RecipientTransplantStatusEvents target
using ETL.TLoadRenalRecipientTransplantStatusEvents source
on source.SourceUniqueID = target.SourceUniqueID
when matched and source.StartDate between @from and @to
and not
(
	isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[StartDate], getdate()) = isnull(source.[StartDate], getdate())
	and isnull(target.[StopDate], getdate()) = isnull(source.[StopDate], getdate())
	and isnull(target.[DaysElapsed], 0) = isnull(source.[DaysElapsed], 0)
	and isnull(target.[AgeAtEvent], 0) = isnull(source.[AgeAtEvent], 0)
	and isnull(target.[TimelineEvent], '') = isnull(source.[TimelineEvent], '')
	and isnull(target.[TimelineEventDetailCode], '') = isnull(source.[TimelineEventDetailCode], '')
	and isnull(target.[InformationSource], '') = isnull(source.[InformationSource], '')
	and isnull(target.[Notes], '') = isnull(source.[Notes], '')
	and isnull(target.[RecipientUKTNumber], '') = isnull(source.[RecipientUKTNumber], '')
	and isnull(target.[ReferralObjectID], 0) = isnull(source.[ReferralObjectID], 0)
	and isnull(target.[ReferralStartDate], getdate()) = isnull(source.[ReferralStartDate], getdate())
	and isnull(target.[ReferralStopDate], getdate()) = isnull(source.[ReferralStopDate], getdate())
	and isnull(target.[ConsultantObjectID], 0) = isnull(source.[ConsultantObjectID], 0)
	and isnull(target.[Consultant], '') = isnull(source.[Consultant], '')
	and isnull(target.[Provider], '') = isnull(source.[Provider], '')
	and isnull(target.[DonorType], '') = isnull(source.[DonorType], '')
	and isnull(target.[TransplantPriority], '') = isnull(source.[TransplantPriority], '')
	and isnull(target.[StatusDetail], '') = isnull(source.[StatusDetail], '')
	and isnull(target.[StatusReason], '') = isnull(source.[StatusReason], '')
)
then update set
	target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[StartDate] = source.[StartDate]
	,target.[StopDate] = source.[StopDate]
	,target.[DaysElapsed] = source.[DaysElapsed]
	,target.[AgeAtEvent] = source.[AgeAtEvent]
	,target.[TimelineEvent] = source.[TimelineEvent]
	,target.[TimelineEventDetailCode] = source.[TimelineEventDetailCode]
	,target.[InformationSource] = source.[InformationSource]
	,target.[Notes] = source.[Notes]
	,target.[RecipientUKTNumber] = source.[RecipientUKTNumber]
	,target.[ReferralObjectID] = source.[ReferralObjectID]
	,target.[ReferralStartDate] = source.[ReferralStartDate]
	,target.[ReferralStopDate] = source.[ReferralStopDate]
	,target.[ConsultantObjectID] = source.[ConsultantObjectID]
	,target.[Consultant] = source.[Consultant]
	,target.[Provider] = source.[Provider]
	,target.[DonorType] = source.[DonorType]
	,target.[TransplantPriority] = source.[TransplantPriority]
	,target.[StatusDetail] = source.[StatusDetail]
	,target.[StatusReason] = source.[StatusReason]
	,target.Updated = getdate()
	,target.ByWhom = system_user

when not matched by source then delete

when not matched by target then insert
(
	SourceUniqueID
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[StartDate]
	,[StopDate]
	,[DaysElapsed]
	,[AgeAtEvent]
	,[TimelineEvent]
	,[TimelineEventDetailCode]
	,[InformationSource]
	,[Notes]
	,[RecipientUKTNumber]
	,[ReferralObjectID]
	,[ReferralStartDate]
	,[ReferralStopDate]
	,[ConsultantObjectID]
	,[Consultant]
	,[Provider]
	,[DonorType]
	,[TransplantPriority]
	,[StatusDetail]
	,[StatusReason]
	,Created
	,Updated
	,ByWhom
)
values
(
	 source.SourceUniqueID
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[StartDate]
	,source.[StopDate]
	,source.[DaysElapsed]
	,source.[AgeAtEvent]
	,source.[TimelineEvent]
	,source.[TimelineEventDetailCode]
	,source.[InformationSource]
	,source.[Notes]
	,source.[RecipientUKTNumber]
	,source.[ReferralObjectID]
	,source.[ReferralStartDate]
	,source.[ReferralStopDate]
	,source.[ConsultantObjectID]
	,source.[Consultant]
	,source.[Provider]
	,source.[DonorType]
	,source.[TransplantPriority]
	,source.[StatusDetail]
	,source.[StatusReason]
	,getdate()
	,getdate()
	,system_user
)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats


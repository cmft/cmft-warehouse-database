﻿CREATE PROCEDURE [ETL].[LoadAPCWardStay]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @from smalldatetime
declare @to smalldatetime

select
	@StartTime = getdate()

select
	 @from = min(convert(smalldatetime, EndDate))
	,@to = max(convert(smalldatetime, EndDate))
from
	ETL.TLoadAPCWardStay
where
	EndDate is not null

--delete for all WardStays in the date range to be loaded
delete from APC.WardStay
where
	WardStay.EndDate between @from and @to

SELECT @RowsDeleted = @@Rowcount

--delete the open ward stays
delete from APC.WardStay
where
	WardStay.EndDate is null

SELECT @RowsDeleted = @RowsDeleted + @@Rowcount

--belt and braces delete of data about to be loaded. this should have been already hit by above deletes.
delete from APC.WardStay
where
	exists
	(
	select
		1
	from
		ETL.TLoadAPCWardStay
	where
		TLoadAPCWardStay.SourceUniqueID = WardStay.SourceUniqueID
	)

SELECT @RowsDeleted = @RowsDeleted + @@Rowcount


INSERT INTO APC.WardStay
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,StartDate
	,EndDate
	,StartTime
	,EndTime
	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID = TEncounter.SourceUniqueID
	,SourcePatientNo = TEncounter.SourcePatientNo
	,SourceSpellNo = TEncounter.SourceSpellNo
	,ProviderSpellNo = TEncounter.ProviderSpellNo
	,StartDate = TEncounter.StartDate
	,EndDate = TEncounter.EndDate
	,StartTime = TEncounter.StartTime
	,EndTime = TEncounter.EndTime
	,SiteCode = TEncounter.SiteCode
	,WardCode = TEncounter.WardCode
	,StartActivityCode = TEncounter.StartActivityCode
	,EndActivityCode = TEncounter.EndActivityCode

	,Created = getdate()
	,ByWhom = system_user
from
	ETL.TLoadAPCWardStay TEncounter


SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadAPCWardStay', @Stats, @StartTime

print @Stats

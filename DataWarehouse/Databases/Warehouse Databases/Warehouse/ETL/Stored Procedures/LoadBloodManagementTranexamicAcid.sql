﻿

CREATE PROCEDURE [ETL].[LoadBloodManagementTranexamicAcid]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartTime datetime
	,@LoadEndTime datetime

select
	  @LoadStartTime = MIN(DrugAdministeredDate)
	,@LoadEndTime = (MAX(cast(DrugAdministeredDate as datetime))+'23:59')
from
	ETL.TLoadBloodManagementTranexamicAcid
	

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	BloodManagement.TranexamicAcid target
using
	(
	select
		TranexamicAcidPrimaryKey	
		,EpisodeKey	
		,HospitalNumber	
		,LastName	
		,FirstName	
		,DateOfBirth	
		,Gender	
		,SpecialtyKey	
		,ConsultantKey	
		,IntendedDestination	
		,ActualDestination	
		,DrugKey	
		,DrugDescription	
		,Category	
		,Dose	
		,UnitsKey	
		,UnitsDescription
		,DrugAdministeredDate	
		,DrugAdministeredTime
		,SessionTypeDescription 
		,SessionLocationKey 
		,InterfaceCode
	from
		ETL.TLoadBloodManagementTranexamicAcid
	) source
	on	source.TranexamicAcidPrimaryKey = target.TranexamicAcidPrimaryKey
	when not matched by source
	and	target.DrugAdministeredTime between @LoadStartTime and @LoadEndTime

	then delete

	when not matched
	then
		insert
			(
			TranexamicAcidPrimaryKey	
			,EpisodeKey	
			,HospitalNumber	
			,LastName	
			,FirstName	
			,DateOfBirth	
			,Gender	
			,SpecialtyKey	
			,ConsultantKey
			,IntendedDestination	
			,ActualDestination	
			,DrugKey	
			,DrugDescription	
			,Category	
			,Dose	
			,UnitsKey	
			,UnitsDescription	
			,DrugAdministeredDate
			,DrugAdministeredTime	
			,SessionTypeDescription 
			,SessionLocationKey 
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.TranexamicAcidPrimaryKey	
			,source.EpisodeKey	
			,source.HospitalNumber	
			,source.LastName	
			,source.FirstName	
			,source.DateOfBirth	
			,source.Gender	
			,source.SpecialtyKey	
			,source.ConsultantKey
			,source.IntendedDestination	
			,source.ActualDestination	
			,source.DrugKey	
			,source.DrugDescription	
			,source.Category	
			,source.Dose	
			,source.UnitsKey	
			,source.UnitsDescription	
			,source.DrugAdministeredDate
			,source.DrugAdministeredTime	
			,source.SessionTypeDescription 
			,source.SessionLocationKey 
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.TranexamicAcidPrimaryKey, 0) = isnull(source.TranexamicAcidPrimaryKey, 0)
		and isnull(target.EpisodeKey, 0) = isnull(source.EpisodeKey,0)
		and isnull(target.HospitalNumber, '')  = isnull(source.HospitalNumber, '')  
		and isnull(target.LastName, '')  = isnull(source.LastName, '')  
		and isnull(target.FirstName, '') = isnull(source.FirstName, '')  
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.Gender, '') = isnull(source.Gender, '')
		and isnull(target.SpecialtyKey, 0) = isnull(source.SpecialtyKey, 0)
		and isnull(target.ConsultantKey, 0) = isnull(source.ConsultantKey, 0)
		and isnull(target.IntendedDestination, '')  = isnull(source.IntendedDestination, '')  
		and isnull(target.ActualDestination, '') = isnull(source.ActualDestination, '')  
		and isnull(target.DrugKey, '') = isnull(source.DrugKey, '')
		and isnull(target.DrugDescription, '')  = isnull(source.DrugDescription, '')  
		and isnull(target.Category, '') = isnull(source.Category, '')
		and isnull(target.Dose, '') = isnull(source.Dose, '')
		and isnull(target.UnitsKey, 0) = isnull(source.UnitsKey, 0)
		and isnull(target.UnitsDescription, '') = isnull(source.UnitsDescription, '')
		and isnull(target.DrugAdministeredDate, getdate()) = isnull(source.DrugAdministeredDate, getdate())
		and isnull(target.DrugAdministeredTime, getdate()) = isnull(source.DrugAdministeredTime, getdate())
		and isnull(target.SessionTypeDescription, '') = isnull(source.SessionTypeDescription, '')
		and isnull(target.SessionLocationKey, 0)  = isnull(source.SessionLocationKey,0) 
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			target.TranexamicAcidPrimaryKey = source.TranexamicAcidPrimaryKey
			,target.EpisodeKey = source.EpisodeKey
			,target.HospitalNumber = source.HospitalNumber
			,target.LastName = source.LastName
			,target.FirstName = source.FirstName
			,target.DateOfBirth = source.DateOfBirth
			,target.Gender = source.Gender
			,target.SpecialtyKey = source.SpecialtyKey
			,target.ConsultantKey = source.ConsultantKey
			,target.IntendedDestination = source.IntendedDestination
			,target.ActualDestination = source.ActualDestination
			,target.DrugKey = source.DrugKey
			,target.DrugDescription = source.DrugDescription
			,target.Category = source.Category
			,target.Dose = source.Dose
			,target.UnitsKey = source.UnitsKey
			,target.UnitsDescription = source.UnitsDescription
			,target.DrugAdministeredDate = source.DrugAdministeredDate
			,target.DrugAdministeredTime = source.DrugAdministeredTime
			,target.SessionTypeDescription = source.SessionTypeDescription
			,target.SessionLocationKey = source.SessionLocationKey
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


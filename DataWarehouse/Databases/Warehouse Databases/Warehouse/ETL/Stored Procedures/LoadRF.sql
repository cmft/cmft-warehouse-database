﻿CREATE       procedure [ETL].[LoadRF] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @toRTT smalldatetime

select @StartTime = getdate()

select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from Utility.Parameter where Parameter = 'RFENCOUNTERFREEZEDATE')
		,dateadd(month, -60, @to)
	)


truncate table ETL.TImportRFEncounter
exec ETL.ExtractInquireRF @from, @to
exec ETL.LoadRFEncounter


--re-run the matching process whenever the referrals are updated. This improves the assignment of breach details for activity.

--CCB 2011-12-06 check this is compatible with latest version of RTT before enabling

--select @toRTT = (select max(CensusDate) from RTT.dbo.MatchMaster)
--exec RTT.dbo.BuildMatch null , @toRTT

exec ETL.AssignRFEncounterDirectorateCode

update Utility.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADRFDATE'

if @@rowcount = 0

insert into Utility.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADRFDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec Utility.WriteAuditLogEvent 'LoadRF', @Stats, @StartTime

﻿
CREATE PROCEDURE [ETL].[LoadAPCUpdateExpectedLOS]

AS

BEGIN

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

--Insert new admit records from PAS
INSERT INTO APC.ExpectedLOS

SELECT 
	 SourceUniqueID
	,LoadTime = GETDATE()
	,LoadModifiedTime = NULL
	,SourceCreatedTime = Created
	,EDDCreatedTime = CASE 
							WHEN ExpectedLOS IS NULL THEN NULL 
							ELSE Created
					   END
	,SourceSystem = 'PAS'
	,ModifiedFromSystem = NULL
	,AdmissionTime
	,DischargeTime = DischargeDate
	,SourcePatientNo = SourcePatientNo
	,SourceSpellNo = SourceSpellNo
	,SiteCode = StartSiteCode
	,Ward = StartWardTypeCode
	,Specialty = SpecialtyCode
	,Consultant = ConsultantCode
	,ManagementIntentionCode = ManagementIntentionCode
	,CasenoteNumber = CasenoteNumber
	,ExpectedLOS
	,EnteredWithin48hrs = CASE 
							WHEN ExpectedLOS IS NOT NULL 
							AND DATEDIFF(Minute,AdmissionTime, Created) <1440 THEN 1 
							ELSE 0 
						  END
	,CreateEddDaysDuration = CASE 
								WHEN ExpectedLOS IS NOT NULL THEN DATEDIFF(dd,AdmissionTime, Created) 
								ELSE NULL 
							 END
	,DirectorateCode = StartDirectorateCode
	,AdmissionMethodCode = AdmissionMethodCode
	,RTTPathwayCondition = Encounter.RTTPathwayCondition
	,ArchiveFlag = 'N'
FROM 
	APCUpdate.Encounter
WHERE  
	NOT EXISTS
		(
		SELECT
			1
		FROM
			APC.ExpectedLOS
		WHERE
			ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
		AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo
		)
		AND Encounter.AdmissionTime >='01/April/2011'
		AND Encounter.AdmissionTime = Encounter.EpisodeStartTime

SELECT @RowsInserted = @@rowcount

SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows extracted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 

EXEC WriteAuditLogEvent 'LoadAPCUpdateExpectedLOS', @Stats, @StartTime


END




﻿

CREATE proc [ETL].[ExtractUlyssesComplaint]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

--select @FromDate = coalesce(@FromDate, (select cast(dateadd(year, -1, getdate()) as date)))
--select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))


truncate table ETL.TImportComplaintComplaint

insert into ETL.TImportComplaintComplaint
(
SourceComplaintCode
,ComplaintCode
,ClaimantCode
,ClaimTypeCode
,BehalfOfCode
,BehalfOfTypeCode
,CaseNumber
,ReceivedFromCode
,ReceiptDate
,AcknowledgementDate
,AcknowledgementMethodCode
,ComplaintDetail
,ResolutionDate
,OutcomeDetail
,Satisfied
,ResponseDate
,DelayReasonCode
,SeverityCode
,ResponseDueDate
,LetterDate
,GradeCode
,LikelihoodCode
,RiskRatingCode
,RiskScore
,HandlerCode
,StatusTypeCode
,CaseTypeCode
,ConsentRequired
,ConsentDate
,EventDate
,InitialContactDate
,InitialDueDate
,InitialSeverityCode
,InitalScore
,TargetDate
,SequenceNo
,CategoryCode
,CategoryTypeCode
,PrimaryOrSecondary
,OrganisationCode
,SiteCode
,SiteTypeCode
,DivisionCode
,DirectorateCode
,DepartmentCode
,ServiceCode
,ProfessionCode
,Reopened
,ReopenedDetail
)

select
	SourceComplaintCode
	,ComplaintCode
	,ClaimantCode 
	,ClaimTypeCode
	,BehalfOfCode
	,BehalfOfTypeCode
	,CaseNumber 
	,ReceivedFromCode 
	,ReceiptDate
	,AcknowledgementDate
	,AcknowledgementMethodCode 
	,ComplaintDetail 
	,ResolutionDate 
	,OutcomeDetail 
	,Satisfied 
	,ResponseDate 
	,DelayReasonCode 
	,SeverityCode 
	,ResponseDueDate 
	,LetterDate 
	,GradeCode 
	,LikelihoodCode 
	,RiskRatingCode 
	,RiskScore
	,HandlerCode
	,StatusTypeCode
	,CaseTypeCode
	,ConsentRequired 
	,ConsentDate 
	,EventDate
	,InitialContactDate 
	,InitialDueDate 
	,InitialSeverityCode 
	,InitalScore 
	,TargetDate 
	,SequenceNo = row_number() over (partition by ComplaintCode,Reopened,ReceiptDate order by CategoryCode)
	,CategoryCode
	,CategoryTypeCode 
	,PrimaryOrSecondary
	,OrganisationCode 
	,SiteCode 
	,SiteTypeCode
	,DivisionCode
	,DirectorateCode 
	,DepartmentCode 
	,ServiceCode
	,ProfessionCode 
	,Reopened
	,ReopenedDetail

from
	(
		select
			SourceComplaintCode = Complaint.COMP_CODE
			,ComplaintCode = cast(Complaint.COMP_CODE as varbinary(50))			
			,ClaimantCode = Complaint.CLAIMANT
			,ClaimTypeCode = Complaint.CLAIM_TYPE
			,BehalfOfCode = nullif(Complaint.BEHALF_OF, '    ')
			,BehalfOfTypeCode = nullif(Complaint.BEHALF_TYP, ' ')
			,CaseNumber = Complaint.CASE_NO
			,ReceivedFromCode = Complaint.REC_FROM
			,ReceiptDate = Complaint.RECEIVE_DT
			,AcknowledgementDate = Complaint.ACKNOW_DT
			,AcknowledgementMethodCode = Complaint.VERB_WRIT
			,ComplaintDetail = nullif(cast(Complaint.DETAILS as varchar(max)), '')
			,ResolutionDate = Complaint.RESOLVE_DT
			,OutcomeDetail = nullif(cast(Complaint.OUT_DETAIL as varchar(max)), '')
			,Satisfied = nullif(Complaint.SATISFIED, ' ')
			,ResponseDate = Complaint.RESPON_DT
			,DelayReasonCode = nullif(DELAY_REAS, '  ')
			,SeverityCode = nullif(Complaint.SEVERITY, '  ')
			,ResponseDueDate = Complaint.RESPON_DUE
			,LetterDate = Complaint.LETTER_DT
			,GradeCode = nullif(Complaint.GRADE, '  ')
			,LikelihoodCode = nullif(Complaint.LIKELIHOOD, '  ')
			,RiskRatingCode = nullif(Complaint.RISK_RATE, '  ')
			,RiskScore = Complaint.RISK_SCORE
			,HandlerCode = Complaint.HANDLER
			,StatusTypeCode = Complaint.STATUS_TY
			,CaseTypeCode = nullif(Complaint.CASE_TYPE,'  ')
			,ConsentRequired = Complaint.CONSENT_RQ
			,ConsentDate = Complaint.CONSENT_DT
			,EventDate = EVENT_DT
			,InitialContactDate = Complaint.INIT_CONT	
			,InitialDueDate =  Complaint.INIT_DUE
			,InitialSeverityCode = nullif(Complaint.INIT_SEVER, '  ')
			,InitalScore = Complaint.INIT_SCORE
			,TargetDate = Complaint.TARGET_DT

			,CategoryCode = nullif(SubComplaint.CATEGORY,'  ')
			,SiteCode = SubComplaint.SITE
			,DepartmentCode = nullif(SubComplaint.DEPARTMENT, '  ')
			,CategoryTypeCode = nullif(SubComplaint.CAT_TYPE,'  ')
			,DirectorateCode = nullif(SubComplaint.DIRECTORAT, '  ')
			,ServiceCode = nullif(SubComplaint.SERVICE, '  ')
			,ProfessionCode = nullif(SubComplaint.PROFESSION, '  ')
			,SiteTypeCode = SubComplaint.SITE_TYPE
			,OrganisationCode = SubComplaint.ORGANISE
			,PrimaryOrSecondary = SubComplaint.TYPE -- no idea what this is...
			,DivisionCode = nullif(SubComplaint.DIVISION, '  ')
			
			,Reopened = null
			,ReopenedDetail = null
			
			--,GRADE
			--        ,GRADE_DESCRIPT = SEVERTYPE.DESCRIPT     
			--,STATUS_TY
			--        ,STATUSTYPE_DESCRIPT = STATUSTYPE.DESCRIPT              
			--,PRIMARY_SECONDARY = SUBCOMP.TYPE
			--,CASE_TYPE
			--        ,CASETYPE_DESCRIPT = CASETYPE.DESCRIPT                                         
			--,CORPORATETHEMETYPE_CODE = COMPCAT.CORP_CAT      
			--        ,CORPORATETHEMETYPE_DESCRIPT = CORPORATETHEMETYPE.DESCRIPT
			--,SUBCOMP.CAT_TYPE
			--        ,CATTYPE_DESCRIPT = CATTYPE.DESCRIPT                    
			--,SUBCOMP.CATEGORY
			--        ,CATEGORY_DESCRIPT = COMPCAT.DESCRIPT

			--,SUBCOMP.ORGANISE
			--        ,ORGANISE_DESCRIPT = ORGANISE.DESCRIPT                  
			--,SUBCOMP.SITE_TYPE
			--        ,SITETYPE_DESCRIPT = SITETYPE.DESCRIPT
			--,SUBCOMP.SITE
			--        ,SITE_DESCRIPT = SITE.SITE        
			--,SUBCOMP.SERVICE
			--        ,SERVICE_DESCRIPT = SERVICE.DESCRIPT
			--,SUBCOMP.PROFESSION
			--        ,PROFESSION_DESCRIPT = PROFESS.DESCRIPT
			--,SUBCOMP.DIVISION
			--        ,DIVISION_DESCRIPT = DIVISION.DESCRIPT                  
			--,SUBCOMP.DIRECTORAT
			--        ,DIRECTORAT_DESCRIPT = DIRECTORATE.DIRECTORAT                                                         
			--,SUBCOMP.DEPARTMENT
			--        ,DEPARTMENT_DESCRIPT = DEPARTMENT.DESCRIPT
			--,SUBCOMP.DEPTFUNC
			--        ,DEPTFUNC_DESCRIPT = DEPTFUNCTION.DESCRIPT              

		from
			[$(Ulysses)].Complain.COMPLAIN Complaint

		left join  [$(Ulysses)].Complain.SUBCOMP SubComplaint
		ON cast(SubComplaint.COMP_CODE as varbinary(50)) = cast(Complaint.COMP_CODE as varbinary(50))

		union all --Reopened Complaints
		   
		select
			SourceComplaintCode = Complaint.COMP_CODE
			,ComplaintCode = cast(Complaint.COMP_CODE as varbinary(50))			
			,ClaimantCode = Complaint.CLAIMANT
			,ClaimTypeCode = Complaint.CLAIM_TYPE
			,BehalfOfCode = nullif(Complaint.BEHALF_OF, '    ')
			,BehalfOfTypeCode = nullif(Complaint.BEHALF_TYP, ' ')
			,CaseNumber = Complaint.CASE_NO
			,ReceivedFromCode = Complaint.REC_FROM 
			,ReceiptDate = ComplaintReopen.RECEIVE_DT --Reopened Complaints
			,AcknowledgementDate = ComplaintReopen.ACKNOW_DT --Reopened Complaints
			,AcknowledgementMethodCode = null --Reopened Complaints
			,ComplaintDetail = nullif(cast(Complaint.DETAILS as varchar(max)), '')
			,ResolutionDate = ComplaintReopen.RESPON_DT  --Reopened Complaints
			,OutcomeDetail = nullif(cast(Complaint.OUT_DETAIL as varchar(max)), '')
			,Satisfied = nullif(Complaint.SATISFIED, ' ')
			,ResponseDate = ComplaintReopen.RESPON_DT --Reopened Complaints
			,DelayReasonCode = nullif(DELAY_REAS, '  ')
			,SeverityCode = nullif(Complaint.SEVERITY, '  ')
			,ResponseDueDate = ComplaintReopen.RESPON_DUE --Reopened Complaints
			,LetterDate = Complaint.LETTER_DT
			,GradeCode = nullif(Complaint.GRADE, '  ')
			,LikelihoodCode = nullif(Complaint.LIKELIHOOD, '  ')
			,RiskRatingCode = nullif(Complaint.RISK_RATE, '  ')
			,RiskScore = Complaint.RISK_SCORE
			,HandlerCode = Complaint.HANDLER
			,StatusTypeCode = Complaint.STATUS_TY
			,CaseTypeCode = nullif(Complaint.CASE_TYPE, '  ')
			,ConsentRequired = Complaint.CONSENT_RQ
			,ConsentDate = null --Reopened Complaints
			,EventDate = EVENT_DT
			,InitialContactDate = ComplaintReopen.INIT_CONT --Reopened Complaints	
			,InitialDueDate =  ComplaintReopen.INIT_DUE --Reopened Complaints
			,InitialSeverityCode = nullif(Complaint.INIT_SEVER, '  ')
			,InitalScore = Complaint.INIT_SCORE
			,TargetDate = null --Reopened Complaints

			,CategoryCode = nullif(SubComplaint.CATEGORY,'  ')
			,SiteCode = SubComplaint.SITE
			,DepartmentCode = nullif(SubComplaint.DEPARTMENT, '  ')
			,CategoryTypeCode = nullif(SubComplaint.CAT_TYPE,'  ')
			,DirectorateCode = nullif(SubComplaint.DIRECTORAT, '  ')
			,ServiceCode = nullif(SubComplaint.SERVICE, '  ')
			,ProfessionCode = nullif(SubComplaint.PROFESSION, '  ')
			,SiteTypeCode = SubComplaint.SITE_TYPE
			,OrganisationCode = SubComplaint.ORGANISE
			,PrimaryOrSecondary = SubComplaint.TYPE
			,DivisionCode = nullif(SubComplaint.DIVISION, '  ')
			
			,Reopened = 1
			,ReopenedDetail = nullif(cast(ComplaintReopen.DETAILS as varchar(max)), '') --Reopened Complaints  

		from
			[$(Ulysses)].Complain.COMPLAIN Complaint

		left join  [$(Ulysses)].Complain.SUBCOMP SubComplaint
		ON cast(SubComplaint.COMP_CODE as varbinary(50)) = cast(Complaint.COMP_CODE as varbinary(50))

		inner join  [$(Ulysses)].Complain.COMPREOPENED ComplaintReopen
		ON cast(ComplaintReopen.COMP_CODE as varbinary(50)) = cast(Complaint.COMP_CODE as varbinary(50))   


	) Complaint
	
	where
		ReceiptDate between @FromDate and @ToDate



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

-- Update extract status
if @RowsInserted != 0
	update Utility.Parameter set NumericValue = 1 where Parameter = 'EXTRACTSTATUSULYSSESCOMPLAINT'

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

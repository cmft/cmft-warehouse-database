﻿CREATE procedure [ETL].[LoadAPCCriticalCareMidas] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)


select
	@StartTime = getdate()

--we receive a financial YTD extract so delete everything from the 1st day of the extract (usually expect this to be 1 April)

delete
from
	APC.CriticalCarePeriod
where
	CriticalCarePeriod.StartDate >=
		(
		select
			min(convert(date, cast([CRITICAL CARE START DATE] as varchar), 112))
		from
			[$(CriticalCare)].dbo.[tbSys_Inpatient CDS Export]
		)
AND CriticalCarePeriod.InterfaceCode = 'MID'

SELECT @RowsDeleted = @@Rowcount


insert into APC.CriticalCarePeriod
(
	   SourceUniqueID
      ,SourceSpellNo
      ,SourcePatientNo
      ,StartDate
      ,StartTime
      ,EndDate
      ,EndTime
      ,AdvancedCardiovascularSupportDays
      ,AdvancedRespiratorySupportDays
      ,BasicCardiovascularSupportDays
      ,BasicRespiratorySupportDays
      ,CriticalCareLevel2Days
      ,CriticalCareLevel3Days
      ,DermatologicalSupportDays
      ,LiverSupportDays
      ,NeurologicalSupportDays
      ,RenalSupportDays
      ,CreatedByUser
      ,CreatedByTime
      ,LocalIdentifier
      ,LocationCode
      ,StatusCode
      ,TreatmentFunctionCode
      ,PlannedAcpPeriod
      ,Created
      ,ByWhom
      ,InterfaceCode
      ,CasenoteNumber
      ,WardCode
      ,AdmissionDate
      ,SiteCode
      ,NHSNumber
      ,UnitFunctionCode
	  
)
select
	 SourceUniqueID = [Midas CareEpisodeID]
	,SourceSpellNo = NULL
    ,SourcePatientNo = NULL
    ,StartDate = convert(date, cast([CRITICAL CARE START DATE] as varchar), 112)
    ,StartTime = 
			convert(varchar, convert(datetime, cast([CRITICAL CARE START DATE] as varchar)), 111)
			+ ' ' + substring([Birth Date (Baby)], 1, 2)
			+ ':' + substring([Birth Date (Baby)], 4, 2)
			+ ':' + substring([Birth Date (Baby)], 7, 2)
    
    
    ,EndDate = convert(date, cast([CRITICAL CARE DISCAHRGE DATE] as varchar), 112)
    ,EndTime =  
			convert(varchar, convert(datetime, cast([CRITICAL CARE DISCAHRGE DATE] as varchar)), 111)
			+ ' ' + substring([Birth Date (Mother)], 1, 2)
			+ ':' + substring([Birth Date (Mother)], 4, 2)
			+ ':' + substring([Birth Date (Mother)], 7, 2)
    
	,AdvancedCardiovascularSupportDays =  [ADVANCED RESPIRATORY SUPPORT DAYS]
	,AdvancedRespiratorySupportDays = [ADVANCED CARDIOVASCULAR SUPPORT DAYS]
	,BasicCardiovascularSupportDays =  [BASIC CARDIOVASCULAR SUPPORT DAYS]
	,BasicRespiratorySupportDays = [BASIC RESPIRATORY SUPPORT DAYS]
	,CriticalCareLevel2Days = [CRITCAL CARE LEVEL 2 DAYS]
	,CriticalCareLevel3Days = [CRITICAL CARE LEVEL 3 DAYS]
	,DermatologicalSupportDays = [DERMATOLOGICAL SUPPORT DAYS]
	,LiverSupportDays = [LIVER SUPPORT DAYS]
	,NeurologicalSupportDays = [NEUROLOGICAL SUPPORT DAYS]
	,RenalSupportDay =[RENAL SUPPORT DAYS]
	,CreatedByUser = NULL
    ,CreatedByTime = NULL
	,LocalIdentifier = [CRITICAL CARE LOCAL IDENTIFIER]
	,LocationCode = NULL
	,StatusCode = NULL
	,TreatmentFunctionCode = [TREATMENT FUNCTION CODE]
	,PlannedAcpPeriod = NULL
	,Created = getdate()
	,ByWhom = system_user
	,InterfaceCode = 'MID'
	,CasenoteNumber = [Casenote Number]
	,WardCode = [Ward Code (General Details)]
	,AdmissionDate= convert(date, cast([Start Date (Hospital Provider Spell)] as varchar), 112)
	,SiteCode = [Provider Code]
	,NHSNumber = 
		CASE 
			WHEN [NHS Number] = 'NONHSNUMBER' THEN NULL
			WHEN ISNUMERIC([NHS Number]) <> 1 THEN NULL
			WHEN LEN([NHS Number]) <>10 THEN NULL
			ELSE [NHS Number]
		END
,UnitFunctionCode = [CRITCAL CARE UNIT FUNCTION]

     
from
	[$(CriticalCare)].dbo.[tbSys_Inpatient CDS Export]

SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'LoadAPCCriticalCareMidas', @Stats, @StartTime

print @Stats

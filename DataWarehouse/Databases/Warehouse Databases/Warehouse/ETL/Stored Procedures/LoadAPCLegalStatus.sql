﻿CREATE procedure [ETL].[LoadAPCLegalStatus]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table APC.LegalStatus

insert into APC.LegalStatus
(
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,HospitalCode
	,StartTime
	,PsychiatricPatientStatusCode 
	,MentalCategoryCode
	,LegalStatusCode
)
select
	 SourceUniqueID =
		SourcePatientNo + '||' +
		SourceSpellNo + '||' +
		LegalStatusStartDateTime1

	,SourceSpellNo
	,SourcePatientNo
	,HospitalCode
	,StartTime
	,PsychiatricPatientStatusCode 
	,MentalCategoryCode
	,LegalStatusCode
from
	ETL.TLoadAPCLegalStatus


select
	@RowsInserted = @@ROWCOUNT



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadAPCLegalStatus', @Stats, @StartTime

﻿
CREATE PROCEDURE ETL.LoadInfectionResult

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, SpecimenCollectionTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, SpecimenCollectionTime)) 
from
	ETL.TLoadInfectionResult

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Infection.Result target
using
	(
	select
		 AlertTypeCode
		,TestCategoryID
		,Positive
		,SpecimenNumber
		,PatientName
		,DistrictNo
		,NHSNumber
		,SexCode
		,DateOfBirth
		,SpecimenCollectionDate
		,SpecimenCollectionTime
		,PreviousPositiveDate
		,OrganismCode
		,SampleTypeCode
		,SampleSubTypeCode
		,SampleQualifierCode
		,AuthorisationDate
		,ExtractFileName
		,InterfaceCode
	from
		ETL.TLoadInfectionResult
	) source
	on	source.AlertTypeCode = target.AlertTypeCode
	and	source.TestCategoryID = target.TestCategoryID
	and	source.SpecimenNumber = target.SpecimenNumber
	and source.OrganismCode = target.OrganismCode
	and source.AuthorisationDate = target.AuthorisationDate


	when not matched by source
	and	target.SpecimenCollectionTime between @LoadStartDate and @LoadEndDate
	then delete

	when not matched
	then
		insert
			(
			 AlertTypeCode
			,TestCategoryID
			,Positive
			,SpecimenNumber
			,PatientName
			,DistrictNo
			,NHSNumber
			,SexCode
			,DateOfBirth
			,SpecimenCollectionDate
			,SpecimenCollectionTime
			,PreviousPositiveDate
			,OrganismCode
			,SampleTypeCode
			,SampleSubTypeCode
			,SampleQualifierCode
			,AuthorisationDate
			,ExtractFileName
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.AlertTypeCode
			,source.TestCategoryID
			,source.Positive
			,source.SpecimenNumber
			,source.PatientName
			,source.DistrictNo
			,source.NHSNumber
			,source.SexCode
			,source.DateOfBirth
			,source.SpecimenCollectionDate
			,source.SpecimenCollectionTime
			,source.PreviousPositiveDate
			,source.OrganismCode
			,source.SampleTypeCode
			,source.SampleSubTypeCode
			,source.SampleQualifierCode
			,source.AuthorisationDate
			,source.ExtractFileName
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.AlertTypeCode, '') = isnull(source.AlertTypeCode, '')
		and isnull(target.TestCategoryID, 0) = isnull(source.TestCategoryID, 0)
		and isnull(target.Positive, 0) = isnull(source.Positive, 0)
		and isnull(target.SpecimenNumber, '') = isnull(source.SpecimenNumber, '')
		and isnull(target.PatientName, '') = isnull(source.PatientName, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.SpecimenCollectionDate, getdate()) = isnull(source.SpecimenCollectionDate, getdate())
		and isnull(target.SpecimenCollectionTime, getdate()) = isnull(source.SpecimenCollectionTime, getdate())
		and isnull(target.PreviousPositiveDate, '') = isnull(source.PreviousPositiveDate, '')
		and isnull(target.OrganismCode, '') = isnull(source.OrganismCode, '')
		and isnull(target.SampleTypeCode, '') = isnull(source.SampleTypeCode, '')
		and isnull(target.SampleSubTypeCode, '') = isnull(source.SampleSubTypeCode, '')
		and isnull(target.SampleQualifierCode, '') = isnull(source.SampleQualifierCode, '')
		and isnull(target.AuthorisationDate, getdate()) = isnull(source.AuthorisationDate, getdate())
		and isnull(target.ExtractFileName, '') = isnull(source.ExtractFileName, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			target.AlertTypeCode = source.AlertTypeCode
			,target.TestCategoryID = source.TestCategoryID
			,target.Positive = source.Positive
			,target.SpecimenNumber = source.SpecimenNumber
			,target.PatientName = source.PatientName
			,target.DistrictNo = source.DistrictNo
			,target.NHSNumber = source.NHSNumber
			,target.SexCode = source.SexCode
			,target.DateOfBirth = source.DateOfBirth
			,target.SpecimenCollectionDate = source.SpecimenCollectionDate
			,target.SpecimenCollectionTime = source.SpecimenCollectionTime
			,target.PreviousPositiveDate = source.PreviousPositiveDate
			,target.OrganismCode = source.OrganismCode
			,target.SampleTypeCode = source.SampleTypeCode
			,target.SampleSubTypeCode = source.SampleSubTypeCode
			,target.SampleQualifierCode = source.SampleQualifierCode
			,target.AuthorisationDate = source.AuthorisationDate
			,target.ExtractFileName = source.ExtractFileName
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
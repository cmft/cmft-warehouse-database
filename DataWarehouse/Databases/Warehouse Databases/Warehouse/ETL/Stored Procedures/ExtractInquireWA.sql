﻿CREATE procedure [ETL].[ExtractInquireWA]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(8)
declare @to varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112)

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)


insert into ETL.TImportWAEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,SeenByCode
	,InterfaceCode
)


select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,SeenByCode
	,InterfaceCode = 'INQ'
from
	(
	SELECT
		 SourceUniqueID = WARDATTENDER.WARDATTENDERID 
		,SourcePatientNo = WARDATTENDER.InternalPatientNumber 
		,SourceEncounterNo = WARDATTENDER.EpisodeNumber 
		,PatientTitle = Patient.Title 
		,PatientForename = Patient.Forenames 
		,PatientSurname = Patient.Surname 
		,DateOfBirth = Patient.PtDoB 
		,DateOfDeath = Patient.PtDateOfDeath 
		,SexCode = Patient.Sex 
		,NHSNumber = Patient.NHSNumber 
		,DistrictNo = WARDATTENDER.DistrictNumber 
		,Postcode = Patient.PtAddrPostCode 
		,PatientAddress1 = Patient.PtAddrLine1 
		,PatientAddress2 = Patient.PtAddrLine2 
		,PatientAddress3 = Patient.PtAddrLine3 
		,PatientAddress4 = Patient.PtAddrLine4 
		,DHACode = Patient.DistrictOfResidenceCode 
		,EthnicOriginCode = Patient.EthnicType 
		,MaritalStatusCode = Patient.MaritalStatus 
		,ReligionCode = Patient.Religion 
		,RegisteredGpCode = Patient.GpCode 
		,RegisteredGpPracticeCode = null 
		,SiteCode = WARD.HospitalCode 
		,AppointmentDate = WARDATTENDER.AttendanceDate 
		,AppointmentTime = WARDATTENDER.AttendanceDate 
		,ClinicCode = WARDATTENDER.Ward 
		,AdminCategoryCode = WARDATTENDER.Category 
		,SourceOfReferralCode = WARDATTENDER.RefBy 
		,ReasonForReferralCode = WARDATTENDER.ReasonForReferralCode 
		,PriorityCode = WARDATTENDER.PriorityType 
		,FirstAttendanceFlag = WARDATTENDER.FirstAttendance 
		,AppointmentStatusCode = WARDATTENDER.WaWardAttendersAttendanceStatusInternal 
		,CancelledByCode = null 
		,TransportRequiredFlag = null 
		,AppointmentTypeCode = 'WA' 
		,DisposalCode = WARDATTENDER.Disposal 
		,ConsultantCode = WARDATTENDER.Consultant 
		,SpecialtyCode = WARDATTENDER.Specialty 
		,ReferringConsultantCode = null 
		,ReferringSpecialtyCode = null 
		,BookingTypeCode = null 
		,CasenoteNo = WARDATTENDER.CaseNoteNumber 
		,AppointmentCreateDate = null 
		,EpisodicGpCode = Patient.GpCode 
		,EpisodicGpPracticeCode = null 
		,DoctorCode = WARDATTENDER.WaWardAttendersDoctorCodePhysical 
		,PrimaryDiagnosisCode = null 
		,PrimaryOperationCode = WARDATTENDER.PrimaryProcedureCode 
		,PurchaserCode = WARDATTENDER.PurchaserCode 
		,ProviderCode = null 
		,ContractSerialNo = WARDATTENDER.ContractId 
		,ReferralDate = WARDATTENDER.ReferralDate 
		,RTTPathwayID = PW.PathwayNumber 
		,RTTPathwayCondition = PW.PathwayCondition 
		,RTTStartDate = PWP.RttStartDate 
		,RTTEndDate = PWP.RttEndDate
		,RTTSpecialtyCode = PW.RttSpeciality 
		,RTTCurrentProviderCode = PW.RttCurProv 
		,RTTCurrentStatusCode = PW.RttCurrentStatus 
		,RTTCurrentStatusDate = PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
		,RTTOverseasStatusFlag = PW.RttOsvStatus 
		,RTTPeriodStatusCode = null 
		,AppointmentCategoryCode = null 
		,AppointmentCreatedBy = null 
		,AppointmentCancelDate = null 
		,LastRevisedDate = null 
		,LastRevisedBy = null 
		,OverseasStatusFlag = null 
		,PatientChoiceCode = null 
		,ScheduledCancelReasonCode = null 
		,PatientCancelReason = null 
		,DischargeDate = null 
		,QM08StartWaitDate = null 
		,QM08EndWaitDate = null 
		,DestinationSiteCode = null 
		,EBookingReferenceNo = null 
		,SeenByCode = WARDATTENDER.SeenBy 
	FROM
		[$(PAS)].Inquire.WARDATTENDER

	INNER JOIN [$(PAS)].Inquire.PATDATA Patient
	ON	WARDATTENDER.InternalPatientNumber = Patient.InternalPatientNumber

	INNER JOIN [$(PAS)].Inquire.WARD
	ON	WARDATTENDER.Ward = WARD.WARDID

	left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
	on	EPW.InternalPatientNumber = WARDATTENDER.InternalPatientNumber
	and	EPW.EpisodeNumber = WARDATTENDER.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTPATHWAY PW
	on	PW.InternalPatientNumber = EPW.InternalPatientNumber
	and	PW.PathwayNumber = EPW.PathwayNumber

	left join [$(PAS)].Inquire.RTTPERIOD PWP
	on	PWP.InternalPatientNumber = PW.InternalPatientNumber
	and	PWP.PathwayNumber = PW.PathwayNumber
	and	PWP.PPeriodId = '0'

	WHERE
		WARDATTENDER.AttendanceDateInternal between @from and @to
	) WardAttender




select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireWA', @Stats, @StartTime

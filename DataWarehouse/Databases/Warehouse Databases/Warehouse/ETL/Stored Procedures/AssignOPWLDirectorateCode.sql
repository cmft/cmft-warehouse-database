﻿

CREATE procedure [ETL].[AssignOPWLDirectorateCode] as


declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()
select @RowsUpdated = 0

-- main mapping
update OP.WaitingList
set

	 WaitingList.DirectorateCode = DivisionMap.DirectorateCode
	,WaitingList.SpecialtyTypeCode =
		CASE 
		WHEN DivisionMap.Diagnostic = 'Y' THEN 'D'
		WHEN DivisionMap.Diagnostic = 'N' THEN 'I'
		ELSE DivisionMap.Diagnostic
		END	
from
	OP.WaitingList WaitingList

inner join WH.DivisionMap DivisionMap 
on	DivisionMap.SiteCode = WaitingList.SiteCode
and	DivisionMap.SpecialtyCode = WaitingList.PASSpecialtyCode
and 
	(
		DivisionMap.ConsultantCode = WaitingList.ConsultantCode
	or
		DivisionMap.ConsultantCode is null
	)
		
and WaitingList.CensusDate between FromDate and coalesce(ToDate, '31 dec 9999')


where
	not exists
		(
		select
			1
		from
			WH.DivisionMap Previous
		where
			Previous.SiteCode = WaitingList.SiteCode
		and	Previous.SpecialtyCode = WaitingList.PASSpecialtyCode


		and 
			(
				Previous.ConsultantCode = WaitingList.ConsultantCode
			or
				Previous.ConsultantCode is null
			)

		and
			WaitingList.CensusDate between Previous.FromDate and coalesce(Previous.ToDate, '31 dec 9999')

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end

				>

				case
				when DivisionMap.ConsultantCode is not null
				then 4
				else 0
				end
	
			--or
			--	(
			--		case
			--		when Previous.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end

			--		=

			--		case
			--		when DivisionRuleBase.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end 

				)
			)

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

-- specific mapping rules

update OP.WaitingList
set
	DirectorateCode = 70
WHERE 
	PASSpecialtyCode in ('PT' , 'DIET' , 'DTCO') --Like '110 P%' Or	Specialty Like '654%'
AND DateOnWaitingList < '9 Jan 2009'


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update OP.WaitingList
set
	DirectorateCode = 0
where
	PASSpecialtyCode in ( 'PAED' , 'PNEP' )
AND DateOnWaitingList >= '7 Jan 2009'
AND SiteCode = 'MRI'

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update OP.WaitingList
set
	DirectorateCode = 30
where
	PASSpecialtyCode = 'PMET'
AND ClinicCode in ('CAAMMETT' , 'CJEWMET' , 'CJHWMET' , 'RJEWMET')

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update OP.WaitingList
set
	DirectorateCode = 0
WHERE 
	PASSpecialtyCode = 'OPHC'
AND ClinicCode in ( 'CORTHOP' , 'CPREAD' )


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'AssignOPWLDirectorateCode', @Stats, @StartTime




﻿CREATE proc [ETL].[ExtractSharepointWardConfiguration]

as

declare @StartTime datetime
declare @Elapsed int
declare @Sproc varchar(255)
declare @Stats varchar(255)
declare @RowsInserted int;

select @StartTime = getdate();

truncate table ETL.TImportBedComplementWardConfiguration

--Awaiting Jabran Sharepoint extract solution...

--insert into ETL.TImportBedComplementWardConfiguration
--(
--	 WardID
--	,WardType
--	,Schedule
--	,Division
--	,SpecialtyGroup
--	,Comment
--	,StartDate
--	,EndDate
--)
--select
--	 [WardCode]
--	,[WardType]
--	,[Days/Nights]
--	,[Division]
--	,[Specialty]
--	,[Comments]
--	,[StartDate]
--	,[EndDate]
--from [SHAREPOINT]...[qry_WardMapping];


select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;

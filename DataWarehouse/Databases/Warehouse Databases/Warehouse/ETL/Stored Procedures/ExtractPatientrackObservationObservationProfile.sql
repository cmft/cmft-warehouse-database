﻿
CREATE proc [ETL].[ExtractPatientrackObservationObservationProfile]


	@FromDate datetime = null
	,@ToDate datetime = null


as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @LocalFromDate datetime 
declare @LocalToDate datetime 

select @LocalFromDate = @FromDate
select @LocalToDate = @ToDate

select @StartTime = getdate()

select @RowsInserted = 0

insert ETL.TImportObservationObservationProfile
(
SourceUniqueID
,CasenoteNumber
,DateOfBirth
,SpecialtyID
,LocationID
,AdmissionSourceUniqueID
,ProfileID
,TemplatePriorityID
,DrivingTableCode
,DrivingTableID
,ProfileReasonID
,StepNumber
,IterationNumber
,StartTime
,EndTime
,CreatedBy
,CreatedTime
,LastModifiedBy
,LastModifiedTime
,Active
)

select
	SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,LocationID
	,AdmissionSourceUniqueID
	,ProfileID
	,TemplatePriorityID
	,DrivingTableCode
	,DrivingTableID
	,ProfileReasonID
	,StepNumber
	,IterationNumber
	,StartTime
	,EndTime
	,CreatedBy
	,CreatedTime
	,LastModifiedBy
	,LastModifiedTime
	,Active
from
	(
	select --top 1000
		SourceUniqueID = ProfileApplication.OBPAP_PK
		,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
		,DateOfBirth = Admission.BIRTH_DATE
		,SpecialtyID = AdmissionUnit.UNITT_RFVAL
		,LocationID = WardStay.WARDD_RFVAL
		,AdmissionSourceUniqueID = ProfileApplication.ADMSN_PK
		,ProfileID = ProfileApplication.OBPRF_PK
		,TemplatePriorityID = ProfileApplication.DTMPR_PK 
		,DrivingTableCode = ProfileApplication.DRIVING_TABLE_CODE
		,DrivingTableID = ProfileApplication.DRIVING_TABLE_PK
		,ProfileReasonID = ProfileApplication.OARSN_RFVAL
		,StepNumber = ProfileApplication.STEP_NUMBER
		,IterationNumber = ProfileApplication.ITERATION_NUMBER
		,StartTime = ProfileApplication.START_DTTM
		,EndTime = ProfileApplication.END_DTTM
		,CreatedBy = ProfileApplication.CREATED_BY_USERR_PK
		,CreatedTime = ProfileApplication.CREATED_DTTM
		,LastModifiedBy = ProfileApplication.LAST_MODIFIED_BY_USERR_PK
		,LastModifiedTime = ProfileApplication.LAST_MODIFIED_DTTM
		,Active = ProfileApplication.ACTIVE_FLAG
	from
		[$(PatientrackSS)].dbo.OBSERVATION_PROFILE_APPLICATION ProfileApplication

	inner join [$(PatientrackSS)].dbo.ADMISSION Admission
	on	Admission.ADMSN_PK = ProfileApplication.ADMSN_PK
	and	coalesce(Admission.DELETED_FLAG, 0) = 0

	inner join [$(PatientrackSS)].dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
	on    PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
	and   PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
	and   coalesce(PatientIdentifier.DELETED_FLAG, 0) = 0 

	inner join
			(
			select
				ProfileApplication.OBPAP_PK
				,AdmissionUnit.UNITT_RFVAL
			from
				[$(PatientrackSS)].dbo.ADMISSION_UNIT AdmissionUnit
			inner join [$(PatientrackSS)].dbo.OBSERVATION_PROFILE_APPLICATION ProfileApplication
			on	ProfileApplication.ADMSN_PK = AdmissionUnit.ADMSN_PK
			and ProfileApplication.START_DTTM between AdmissionUnit.START_DTTM and coalesce(AdmissionUnit.END_DTTM, getdate())
			
			where
				coalesce(AdmissionUnit.DELETED_FLAG, 0) = 0
			and	not exists
						(
						select
							1
						from
							[$(PatientrackSS)].dbo.ADMISSION_UNIT AdmissionUnitNext
						where
							ProfileApplication.ADMSN_PK = AdmissionUnitNext.ADMSN_PK
						and coalesce(AdmissionUnitNext.DELETED_FLAG, 0) = 0
						and ProfileApplication.START_DTTM between AdmissionUnitNext.START_DTTM and coalesce(AdmissionUnitNext.END_DTTM, getdate())
						and AdmissionUnitNext.ADUNT_PK > AdmissionUnit.ADUNT_PK
						)

			) AdmissionUnit

	on	ProfileApplication.OBPAP_PK = AdmissionUnit.OBPAP_PK
					
	inner join
			(
			select
				ProfileApplication.OBPAP_PK 
				,WardStay.WARDD_RFVAL
			from
				[$(PatientrackSS)].dbo.WARD_STAY WardStay
			inner join [$(PatientrackSS)].dbo.OBSERVATION_PROFILE_APPLICATION ProfileApplication
			on	ProfileApplication.ADMSN_PK = WardStay.ADMSN_PK
			and ProfileApplication.START_DTTM between WardStay.START_DTTM and coalesce(WardStay.END_DTTM, getdate())
			
			where
				coalesce(WardStay.DELETED_FLAG, 0) = 0
			and	not exists
						(
						select
							1
						from
							[$(PatientrackSS)].dbo.WARD_STAY WardStayNext
						where
							ProfileApplication.ADMSN_PK = WardStayNext.ADMSN_PK
						and coalesce(WardStayNext.DELETED_FLAG, 0) = 0
						and ProfileApplication.START_DTTM between WardStayNext.START_DTTM and coalesce(WardStayNext.END_DTTM, getdate())
						and WardStayNext.WRDST_PK > WardStay.WRDST_PK
						)

			) WardStay

	on	ProfileApplication.OBPAP_PK = WardStay.OBPAP_PK

	where
		coalesce(ProfileApplication.DELETED_FLAG, 0) = 0
	and	ProfileApplication.LAST_MODIFIED_DTTM between @LocalFromDate and @LocalToDate
	) Profile



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
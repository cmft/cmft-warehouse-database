﻿
CREATE procedure [ETL].[LoadPASPatient1] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @RowsArchived Int
declare @Stats varchar(255)


select @RowsDeleted = 0
select @RowsInserted = 0
select @RowsUpdated = 0
select @RowsArchived = 0

select
	@StartTime = getdate()

declare @rc int;
exec dbo.approxRowCount @db = 'PAS', @schema = 'Inquire',@table = 'PATDATA', @rowcount = @rc output;
select @rc

if @rc > 2500000
begin
	truncate table PAS.PatientBase
	-- insert new records
	insert into PAS.PatientBase
	(
		 PATDATAID
		,Allergies
		,Allergies_1
		,BirthName
		,BloodGroup
		,CarerAddress
		,CarerName
		,CarerSupport
		,Comments
		,DateTimeModified
		,DateTimeModifiedInt
		,DistrictNumber
		,DistrictOfResidenceCode
		,EthnicType
		,ExtAddressAddLine1
		,ExtAddressLine1
		,ExtAddressLine2
		,ExtAddressLine3
		,ExtAddressLine4
		,ExtAddressLine5
		,ExtAddressPostcd
		,Forenames
		,GdpCode
		,GeneralComments
		,GpCode
		,HealthAuthorityCode
		,HomePhone
		,InternalDateOfBirth
		,InternalPatientNumber
		,MRSA
		,MRSASetDt
		,MRSASetDtInt
		,MaritalStatus
		,MedicalProfileComments
		,NHSNumber
		,NHSNumberStatus
		,NoKAddrLine1
		,NoKAddrLine2
		,NoKAddrLine3
		,NoKAddrLine4
		,NoKComments
		,NoKHomePhone
		,NoKName
		,NoKPostCode
		,NoKRelationship
		,NoKWorkPhone
		,Occnspouse
		,Occupation
		,OldNHSNumber
		,PlaceOfBirth
		,PmiAlternatePatientNumberinternal
		,PostAddrExpiryDate
		,PostAddrExpiryDateInt
		,PostalAddressDistrictOfResidence
		,PreferredName
		,PreviousSurname1
		,PreviousSurname2
		,PreviousSurname3
		,PseudoDisrtrictOfResidence
		,PtAddrComments
		,PtAddrLine1
		,PtAddrLine2
		,PtAddrLine3
		,PtAddrLine4
		,PtAddrPostCode
		,PtDateOfDeath
		,PtDateOfDeathInt
		,PtDeathComment1
		,PtDeathComment2
		,PtDeathIndInt
		,PtDeathInformedBy
		,PtDeathRecUserId
		,PtDeathRecordedDt
		,PtDeathRecordedDtInt
		,PtDoB
		,PtDobDayInt
		,PtDobMonthInt
		,PtDobYearInt
		,PtEmail
		,PtHomePhone
		,PtIdentificationComments
		,PtMobilePhone
		,PtPostAddrLine1
		,PtPostAddrLine2
		,PtPostAddrLine3
		,PtPostAddrLine4
		,PtPostAddrPostCode
		,PtPseudoPostCode
		,PtWorkPhone
		,Relationship
		,RelationshipInt
		,Religion
		,RiskFactorCode
		,RiskFactorCode_1
		,School
		,Sex
		,Sex_1
		,Surname
		,Title
		,UserField1
		,UserField2
		,WorkPhone
	)

	SELECT
		 PATDATAID
		,Allergies
		,Allergies_1
		,BirthName
		,BloodGroup
		,CarerAddress
		,CarerName
		,CarerSupport
		,Comments
		,DateTimeModified
		,DateTimeModifiedInt
		,DistrictNumber
		,DistrictOfResidenceCode
		,EthnicType
		,ExtAddressAddLine1
		,ExtAddressLine1
		,ExtAddressLine2
		,ExtAddressLine3
		,ExtAddressLine4
		,ExtAddressLine5
		,ExtAddressPostcd
		,Forenames
		,GdpCode
		,GeneralComments
		,GpCode
		,HealthAuthorityCode
		,HomePhone
		,InternalDateOfBirth
		,InternalPatientNumber
		,MRSA
		,MRSASetDt
		,MRSASetDtInt
		,MaritalStatus
		,MedicalProfileComments
		,NHSNumber
		,NHSNumberStatus
		,NoKAddrLine1
		,NoKAddrLine2
		,NoKAddrLine3
		,NoKAddrLine4
		,NoKComments
		,NoKHomePhone
		,NoKName
		,NoKPostCode
		,NoKRelationship
		,NoKWorkPhone
		,Occnspouse
		,Occupation
		,OldNHSNumber
		,PlaceOfBirth
		,PmiAlternatePatientNumberinternal
		,PostAddrExpiryDate
		,PostAddrExpiryDateInt
		,PostalAddressDistrictOfResidence
		,PreferredName
		,PreviousSurname1
		,PreviousSurname2
		,PreviousSurname3
		,PseudoDisrtrictOfResidence
		,PtAddrComments
		,PtAddrLine1
		,PtAddrLine2
		,PtAddrLine3
		,PtAddrLine4
		,PtAddrPostCode
		,PtDateOfDeath
		,PtDateOfDeathInt
		,PtDeathComment1
		,PtDeathComment2
		,PtDeathIndInt
		,PtDeathInformedBy
		,PtDeathRecUserId
		,PtDeathRecordedDt
		,PtDeathRecordedDtInt
		,PtDoB
		,PtDobDayInt
		,PtDobMonthInt
		,PtDobYearInt
		,PtEmail
		,PtHomePhone
		,PtIdentificationComments
		,PtMobilePhone
		,PtPostAddrLine1
		,PtPostAddrLine2
		,PtPostAddrLine3
		,PtPostAddrLine4
		,PtPostAddrPostCode
		,PtPseudoPostCode
		,PtWorkPhone
		,Relationship
		,RelationshipInt
		,Religion
		,RiskFactorCode
		,RiskFactorCode_1
		,School
		,Sex
		,Sex_1
		,Surname
		,Title
		,UserField1
		,UserField2
		,WorkPhone
	FROM
		[$(PAS)].Inquire.PATDATA


	SELECT @RowsInserted = @RowsInserted + @@Rowcount

end



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) +
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadPASPatient', @Stats, @StartTime


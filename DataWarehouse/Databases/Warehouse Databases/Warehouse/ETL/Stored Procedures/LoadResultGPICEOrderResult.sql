﻿


CREATE PROCEDURE [ETL].[LoadResultGPICEOrderResult]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, OrderRequestTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, OrderRequestTime)) 
from
	ETL.TLoadResultGPICEOrderResult

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Result.GPICEOrderResult target
using
	(
	select
		OrderSourceUniqueID
		,OrderRequestTime
		,ResultSourceUniqueID
		,InterfaceCode				
	from
		ETL.TLoadResultGPICEOrderResult
	) source
	on	source.OrderSourceUniqueID = target.OrderSourceUniqueID
	and	source.ResultSourceUniqueID = target.ResultSourceUniqueID

	when not matched by source
	and	target.OrderRequestTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			OrderSourceUniqueID
			,OrderRequestTime
			,ResultSourceUniqueID
			,InterfaceCode	
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.OrderSourceUniqueID
			,source.OrderRequestTime
			,source.ResultSourceUniqueID
			,source.InterfaceCode	
			,getdate()
			,getdate()
			,suser_name()
			)

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


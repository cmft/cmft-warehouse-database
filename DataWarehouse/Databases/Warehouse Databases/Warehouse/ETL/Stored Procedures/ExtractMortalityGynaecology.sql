﻿
CREATE procedure [ETL].[ExtractMortalityGynaecology]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.Gynaecology target
using
	(
	select
		 SourceUniqueID
		,FormTypeID
		,ReviewStatus
		,ReviewedDate
		,ReviewedBy
		,CasenoteNo
		,DateOfDeath
		,UnitNo
		,ClinicalObs
		,ClinicalObsDetail
		,AdmissionTime
		,CaseDetail
		,PatientName
		,DNR
		,DNRDetail
		,DeathPreventable
		,DeathPreventableComment
		,NursingConcernsClinicalObs
		,NursingConcernsClinicalObsDetail
		,InitialManagementStepsAppropriateAndAdequate
		,InitialManagementStepsAppropriateAndAdequateComment
		,Palliative
		,PalliativeDetail
		,NursingConcernsRoleInDeath
		,NursingConcernsRoleInDeathDetail
		,TransferICU
		,TransferICUDetail
		,TransferHDU
		,TransferHDUDetail
		,Miscomm
		,MiscommDetail
		,PoorCommPrimaryandSecondary
		,PoorCommPrimaryandSecondaryDDetail
		,NotableGoodQualityCare
		,EndOfLifeCareLCPInPatientNotes
		,EndOfLifeCareLCPApproTime
		,EndOfLifeCareLCPApproTimeComment
		,EndOfLifeCarePreferredPlaceOfDeathRecorded
		,EndOfLifeCarePreferredPlaceOfDeathRecordedComment
		,LessonsLearned
		,ActionPlan
		,DateOfBirth
		,ConsultantDiscussionB4WDofIC
		,ConsultantDiscussionB4WDofICComment
		,EWSgteq3
		,EWSgteq3Detail
		,ManageOutOfHospital
		,WhereOutOfHospital
		,PalliativeCareTeamInvolvement
		,PalliativeCareTeamInvolvementNoDetail
		,AdmittedOutOfHours
		,TransferredOtherHospitalSiteInUnstableCondition
		,TransferredOtherHospitalSiteInUnstableConditionDetail
		,ClinicianNotListeningToPatient
		,ClinicianNotListeningtoPatientDetail
		,PreviousFindingsEBMorHLI
		,SourcePatientNo
		,EpisodeStartTime
		,ContextCode
	from
		ETL.TLoadMortalityGynaecology
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalityGynaecology Latest
			where
				Latest.SourcePatientNo = TLoadMortalityGynaecology.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalityGynaecology.SourceUniqueID
			)
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,FormTypeID
			,ReviewStatus
			,ReviewedDate
			,ReviewedBy
			,CasenoteNo
			,DateOfDeath
			,UnitNo
			,ClinicalObs
			,ClinicalObsDetail
			,AdmissionTime
			,CaseDetail
			,PatientName
			,DNR
			,DNRDetail
			,DeathPreventable
			,DeathPreventableComment
			,NursingConcernsClinicalObs
			,NursingConcernsClinicalObsDetail
			,InitialManagementStepsAppropriateAndAdequate
			,InitialManagementStepsAppropriateAndAdequateComment
			,Palliative
			,PalliativeDetail
			,NursingConcernsRoleInDeath
			,NursingConcernsRoleInDeathDetail
			,TransferICU
			,TransferICUDetail
			,TransferHDU
			,TransferHDUDetail
			,Miscomm
			,MiscommDetail
			,PoorCommPrimaryandSecondary
			,PoorCommPrimaryandSecondaryDDetail
			,NotableGoodQualityCare
			,EndOfLifeCareLCPInPatientNotes
			,EndOfLifeCareLCPApproTime
			,EndOfLifeCareLCPApproTimeComment
			,EndOfLifeCarePreferredPlaceOfDeathRecorded
			,EndOfLifeCarePreferredPlaceOfDeathRecordedComment
			,LessonsLearned
			,ActionPlan
			,DateOfBirth
			,ConsultantDiscussionB4WDofIC
			,ConsultantDiscussionB4WDofICComment
			,EWSgteq3
			,EWSgteq3Detail
			,ManageOutOfHospital
			,WhereOutOfHospital
			,PalliativeCareTeamInvolvement
			,PalliativeCareTeamInvolvementNoDetail
			,AdmittedOutOfHours
			,TransferredOtherHospitalSiteInUnstableCondition
			,TransferredOtherHospitalSiteInUnstableConditionDetail
			,ClinicianNotListeningToPatient
			,ClinicianNotListeningtoPatientDetail
			,PreviousFindingsEBMorHLI
			,SourcePatientNo
			,EpisodeStartTime
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.FormTypeID
			,source.ReviewStatus
			,source.ReviewedDate
			,source.ReviewedBy
			,source.CasenoteNo
			,source.DateOfDeath
			,source.UnitNo
			,source.ClinicalObs
			,source.ClinicalObsDetail
			,source.AdmissionTime
			,source.CaseDetail
			,source.PatientName
			,source.DNR
			,source.DNRDetail
			,source.DeathPreventable
			,source.DeathPreventableComment
			,source.NursingConcernsClinicalObs
			,source.NursingConcernsClinicalObsDetail
			,source.InitialManagementStepsAppropriateAndAdequate
			,source.InitialManagementStepsAppropriateAndAdequateComment
			,source.Palliative
			,source.PalliativeDetail
			,source.NursingConcernsRoleInDeath
			,source.NursingConcernsRoleInDeathDetail
			,source.TransferICU
			,source.TransferICUDetail
			,source.TransferHDU
			,source.TransferHDUDetail
			,source.Miscomm
			,source.MiscommDetail
			,source.PoorCommPrimaryandSecondary
			,source.PoorCommPrimaryandSecondaryDDetail
			,source.NotableGoodQualityCare
			,source.EndOfLifeCareLCPInPatientNotes
			,source.EndOfLifeCareLCPApproTime
			,source.EndOfLifeCareLCPApproTimeComment
			,source.EndOfLifeCarePreferredPlaceOfDeathRecorded
			,source.EndOfLifeCarePreferredPlaceOfDeathRecordedComment
			,source.LessonsLearned
			,source.ActionPlan
			,source.DateOfBirth
			,source.ConsultantDiscussionB4WDofIC
			,source.ConsultantDiscussionB4WDofICComment
			,source.EWSgteq3
			,source.EWSgteq3Detail
			,source.ManageOutOfHospital
			,source.WhereOutOfHospital
			,source.PalliativeCareTeamInvolvement
			,source.PalliativeCareTeamInvolvementNoDetail
			,source.AdmittedOutOfHours
			,source.TransferredOtherHospitalSiteInUnstableCondition
			,source.TransferredOtherHospitalSiteInUnstableConditionDetail
			,source.ClinicianNotListeningToPatient
			,source.ClinicianNotListeningtoPatientDetail
			,source.PreviousFindingsEBMorHLI
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.ContextCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			isnull(target.FormTypeID, 0) = isnull(source.FormTypeID, 0)
		and isnull(target.ReviewStatus, 0) = isnull(source.ReviewStatus, 0)
		and isnull(target.ReviewedDate, getdate()) = isnull(source.ReviewedDate, getdate())
		and isnull(target.ReviewedBy, '') = isnull(source.ReviewedBy, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.UnitNo, '') = isnull(source.UnitNo, '')
		and isnull(target.ClinicalObs, 0) = isnull(source.ClinicalObs, 0)
		and isnull(target.ClinicalObsDetail, '') = isnull(source.ClinicalObsDetail, '')
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.CaseDetail, '') = isnull(source.CaseDetail, '')
		and isnull(target.PatientName, '') = isnull(source.PatientName, '')
		and isnull(target.DNR, 0) = isnull(source.DNR, 0)
		and isnull(target.DNRDetail, '') = isnull(source.DNRDetail, '')
		and isnull(target.DeathPreventable, 0) = isnull(source.DeathPreventable, 0)
		and isnull(target.DeathPreventableComment, '') = isnull(source.DeathPreventableComment, '')
		and isnull(target.NursingConcernsClinicalObs, 0) = isnull(source.NursingConcernsClinicalObs, 0)
		and isnull(target.NursingConcernsClinicalObsDetail, '') = isnull(source.NursingConcernsClinicalObsDetail, '')
		and isnull(target.InitialManagementStepsAppropriateAndAdequate, 0) = isnull(source.InitialManagementStepsAppropriateAndAdequate, 0)
		and isnull(target.InitialManagementStepsAppropriateAndAdequateComment, '') = isnull(source.InitialManagementStepsAppropriateAndAdequateComment, '')
		and isnull(target.Palliative, 0) = isnull(source.Palliative, 0)
		and isnull(target.PalliativeDetail, '') = isnull(source.PalliativeDetail, '')
		and isnull(target.NursingConcernsRoleInDeath, 0) = isnull(source.NursingConcernsRoleInDeath, 0)
		and isnull(target.NursingConcernsRoleInDeathDetail, '') = isnull(source.NursingConcernsRoleInDeathDetail, '')
		and isnull(target.TransferICU, 0) = isnull(source.TransferICU, 0)
		and isnull(target.TransferICUDetail, '') = isnull(source.TransferICUDetail, '')
		and isnull(target.TransferHDU, 0) = isnull(source.TransferHDU, 0)
		and isnull(target.TransferHDUDetail, '') = isnull(source.TransferHDUDetail, '')
		and isnull(target.Miscomm, 0) = isnull(source.Miscomm, 0)
		and isnull(target.MiscommDetail, '') = isnull(source.MiscommDetail, '')
		and isnull(target.PoorCommPrimaryandSecondary, 0) = isnull(source.PoorCommPrimaryandSecondary, 0)
		and isnull(target.PoorCommPrimaryandSecondaryDDetail, '') = isnull(source.PoorCommPrimaryandSecondaryDDetail, '')
		and isnull(target.NotableGoodQualityCare, '') = isnull(source.NotableGoodQualityCare, '')
		and isnull(target.EndOfLifeCareLCPInPatientNotes, 0) = isnull(source.EndOfLifeCareLCPInPatientNotes, 0)
		and isnull(target.EndOfLifeCareLCPApproTime, 0) = isnull(source.EndOfLifeCareLCPApproTime, 0)
		and isnull(target.EndOfLifeCareLCPApproTimeComment, '') = isnull(source.EndOfLifeCareLCPApproTimeComment, '')
		and isnull(target.EndOfLifeCarePreferredPlaceOfDeathRecorded, 0) = isnull(source.EndOfLifeCarePreferredPlaceOfDeathRecorded, 0)
		and isnull(target.EndOfLifeCarePreferredPlaceOfDeathRecordedComment, '') = isnull(source.EndOfLifeCarePreferredPlaceOfDeathRecordedComment, '')
		and isnull(target.LessonsLearned, '') = isnull(source.LessonsLearned, '')
		and isnull(target.ActionPlan, '') = isnull(source.ActionPlan, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.ConsultantDiscussionB4WDofIC, 0) = isnull(source.ConsultantDiscussionB4WDofIC, 0)
		and isnull(target.ConsultantDiscussionB4WDofICComment, '') = isnull(source.ConsultantDiscussionB4WDofICComment, '')
		and isnull(target.EWSgteq3, 0) = isnull(source.EWSgteq3, 0)
		and isnull(target.EWSgteq3Detail, '') = isnull(source.EWSgteq3Detail, '')
		and isnull(target.ManageOutOfHospital, 0) = isnull(source.ManageOutOfHospital, 0)
		and isnull(target.WhereOutOfHospital, '') = isnull(source.WhereOutOfHospital, '')
		and isnull(target.PalliativeCareTeamInvolvement, 0) = isnull(source.PalliativeCareTeamInvolvement, 0)
		and isnull(target.PalliativeCareTeamInvolvementNoDetail, '') = isnull(source.PalliativeCareTeamInvolvementNoDetail, '')
		and isnull(target.AdmittedOutOfHours, 0) = isnull(source.AdmittedOutOfHours, 0)
		and isnull(target.TransferredOtherHospitalSiteInUnstableCondition, 0) = isnull(source.TransferredOtherHospitalSiteInUnstableCondition, 0)
		and isnull(target.TransferredOtherHospitalSiteInUnstableConditionDetail, '') = isnull(source.TransferredOtherHospitalSiteInUnstableConditionDetail, '')
		and isnull(target.ClinicianNotListeningToPatient, 0) = isnull(source.ClinicianNotListeningToPatient, 0)
		and isnull(target.ClinicianNotListeningtoPatientDetail, '') = isnull(source.ClinicianNotListeningtoPatientDetail, '')
		and isnull(target.PreviousFindingsEBMorHLI, '') = isnull(source.PreviousFindingsEBMorHLI, '')
		and isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.FormTypeID = source.FormTypeID
			,target.ReviewStatus = source.ReviewStatus
			,target.ReviewedDate = source.ReviewedDate
			,target.ReviewedBy = source.ReviewedBy
			,target.CasenoteNo = source.CasenoteNo
			,target.DateOfDeath = source.DateOfDeath
			,target.UnitNo = source.UnitNo
			,target.ClinicalObs = source.ClinicalObs
			,target.ClinicalObsDetail = source.ClinicalObsDetail
			,target.AdmissionTime = source.AdmissionTime
			,target.CaseDetail = source.CaseDetail
			,target.PatientName = source.PatientName
			,target.DNR = source.DNR
			,target.DNRDetail = source.DNRDetail
			,target.DeathPreventable = source.DeathPreventable
			,target.DeathPreventableComment = source.DeathPreventableComment
			,target.NursingConcernsClinicalObs = source.NursingConcernsClinicalObs
			,target.NursingConcernsClinicalObsDetail = source.NursingConcernsClinicalObsDetail
			,target.InitialManagementStepsAppropriateAndAdequate = source.InitialManagementStepsAppropriateAndAdequate
			,target.InitialManagementStepsAppropriateAndAdequateComment = source.InitialManagementStepsAppropriateAndAdequateComment
			,target.Palliative = source.Palliative
			,target.PalliativeDetail = source.PalliativeDetail
			,target.NursingConcernsRoleInDeath = source.NursingConcernsRoleInDeath
			,target.NursingConcernsRoleInDeathDetail = source.NursingConcernsRoleInDeathDetail
			,target.TransferICU = source.TransferICU
			,target.TransferICUDetail = source.TransferICUDetail
			,target.TransferHDU = source.TransferHDU
			,target.TransferHDUDetail = source.TransferHDUDetail
			,target.Miscomm = source.Miscomm
			,target.MiscommDetail = source.MiscommDetail
			,target.PoorCommPrimaryandSecondary = source.PoorCommPrimaryandSecondary
			,target.PoorCommPrimaryandSecondaryDDetail = source.PoorCommPrimaryandSecondaryDDetail
			,target.NotableGoodQualityCare = source.NotableGoodQualityCare
			,target.EndOfLifeCareLCPInPatientNotes = source.EndOfLifeCareLCPInPatientNotes
			,target.EndOfLifeCareLCPApproTime = source.EndOfLifeCareLCPApproTime
			,target.EndOfLifeCareLCPApproTimeComment = source.EndOfLifeCareLCPApproTimeComment
			,target.EndOfLifeCarePreferredPlaceOfDeathRecorded = source.EndOfLifeCarePreferredPlaceOfDeathRecorded
			,target.EndOfLifeCarePreferredPlaceOfDeathRecordedComment = source.EndOfLifeCarePreferredPlaceOfDeathRecordedComment
			,target.LessonsLearned = source.LessonsLearned
			,target.ActionPlan = source.ActionPlan
			,target.DateOfBirth = source.DateOfBirth
			,target.ConsultantDiscussionB4WDofIC = source.ConsultantDiscussionB4WDofIC
			,target.ConsultantDiscussionB4WDofICComment = source.ConsultantDiscussionB4WDofICComment
			,target.EWSgteq3 = source.EWSgteq3
			,target.EWSgteq3Detail = source.EWSgteq3Detail
			,target.ManageOutOfHospital = source.ManageOutOfHospital
			,target.WhereOutOfHospital = source.WhereOutOfHospital
			,target.PalliativeCareTeamInvolvement = source.PalliativeCareTeamInvolvement
			,target.PalliativeCareTeamInvolvementNoDetail = source.PalliativeCareTeamInvolvementNoDetail
			,target.AdmittedOutOfHours = source.AdmittedOutOfHours
			,target.TransferredOtherHospitalSiteInUnstableCondition = source.TransferredOtherHospitalSiteInUnstableCondition
			,target.TransferredOtherHospitalSiteInUnstableConditionDetail = source.TransferredOtherHospitalSiteInUnstableConditionDetail
			,target.ClinicianNotListeningToPatient = source.ClinicianNotListeningToPatient
			,target.ClinicianNotListeningtoPatientDetail = source.ClinicianNotListeningtoPatientDetail
			,target.PreviousFindingsEBMorHLI = source.PreviousFindingsEBMorHLI
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




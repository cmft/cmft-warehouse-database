﻿CREATE procedure [ETL].[ExtractSIDLikelyStroke]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDLikelyStroke;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDLikelyStroke
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,ClinicalDiagnosis
	,[Plan]
	,SuitableForThrombolysis
	,SuitableForThrombolysisReasonsWhyNo
	,SuitableForThrombolysisNoButDTOHaemorrhagicStroke
	,SuitableForThrombolysisNoButDTOAge
	,SuitableForThrombolysisNoButDTOArrivedOutsideThrombolysisTimeWindow
	,SuitableForThrombolysisNoButDTOSymptomsImproving
	,SuitableForThrombolysisNoButDTOCoMorbidity
	,SuitableForThrombolysisNoButDTOStrokeTooMildOrTooSevere
	,SuitableForThrombolysisNoButDTOContraindicatedMedication
	,SuitableForThrombolysisNoButDTOSymptomOnsetTimeUnknownWakeUpStroke
	,SuitableForThrombolysisNoButDTOPatientOrRelativeRefusal
	,SuitableForThrombolysisNoButDTOOtherMedicalReason
	,RequestForUrgentCTScan
	,DateTimeRequestForUrgentCTScan
	,DateTimeCTScanPerformed
	,ResultsOfScan
	,CTScanNAReasonyWhy
	,Aspirin300mgAdministered
	,DateTimeAspirin300mgAdministered
	,DateTimeAspirin300mgAssessed
	,AspirinNotes
	,SuitableForDirectAdmission
	,ObservationsReviewed
	,DateTimeObservationsReviewed
	,BloodGlucoseRecorded
	,SwallowAssessmentCompleted
	,IfNotWithin4HoursWhyNot
	,DateTimeSwallowAssessmentCompleted
	,SwallowScreenResults
	,SwallowAssessmentCompletedNAReasonyWhy
	,DutyTherapistNotified
	,DateTimeDutyTherapistNotified
	,StrokePhysicianNotified
	,DateTimeStrokePhysicianNotified
	,WillAspirinNeedAdminsteringOnStrokeUnit
	,Username
	,ModifiedDate
	,IsSectionComplete
	,SectionCompleteTime
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		CandidateData.SectionID
	,ActionID = 
		CandidateData.ActionID
	,ClinicalDiagnosis = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/ClinicalDiagnosis)[1]', 'varchar(max)'),'')
	,[Plan] = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/Plan)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysis = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysis)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisReasonsWhyNo = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisReasonsWhyNo)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOHaemorrhagicStroke = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/HaemorrhagicStroke)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOAge = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/Age)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOArrivedOutsideThrombolysisTimeWindow = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/ArrivedOutsideThrombolysisTimeWindow)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOSymptomsImproving = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/SymptomsImproving)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOCoMorbidity = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/CoMorbidity)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOStrokeTooMildOrTooSevere = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/StrokeTooMildOrTooSevere)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOContraindicatedMedication = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/ContraindicatedMedication)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOSymptomOnsetTimeUnknownWakeUpStroke = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/ymptomOnsetTimeUnknownWakeUpStroke)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOPatientOrRelativeRefusal = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/PatientOrRelativeRefusal)[1]', 'varchar(max)'),'')
	,SuitableForThrombolysisNoButDTOOtherMedicalReason = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForThrombolysisNoBut/OtherMedicalReason)[1]', 'varchar(max)'),'')
	,RequestForUrgentCTScan = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/RequestForUrgentCTScan)[1]', 'varchar(max)'),'')
	,DateTimeRequestForUrgentCTScan = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeRequestForUrgentCTScan)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeCTScanPerformed = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeCTScanPerformed)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ResultsOfScan = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/ResultsOfScan)[1]', 'varchar(max)'),'')
	,CTScanNAReasonyWhy = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/CTScanNAReasonyWhy)[1]', 'varchar(max)'),'')
	,Aspirin300mgAdministered = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/Aspirin300mgAdministered)[1]', 'varchar(max)'),'')
	,DateTimeAspirin300mgAdministered = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeAspirin300mgAdministered)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeAspirin300mgAssessed = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeAspirin300mgAssessed)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,AspirinNotes = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/AspirinNotes)[1]', 'varchar(max)'),'')
	,SuitableForDirectAdmission = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SuitableForDirectAdmission)[1]', 'varchar(max)'),'')
	,ObservationsReviewed = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/ObservationsReviewed)[1]', 'varchar(max)'),'')
	,DateTimeObservationsReviewed = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeObservationsReviewed)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,BloodGlucoseRecorded = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/BloodGlucoseRecorded)[1]', 'varchar(max)'),'')
	,SwallowAssessmentCompleted = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SwallowAssessmentCompleted)[1]', 'varchar(max)'),'')
	,IfNotWithin4HoursWhyNot = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/IfNotWithin4HoursWhyNot)[1]', 'varchar(max)'),'')
	,DateTimeSwallowAssessmentCompleted = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeSwallowAssessmentCompleted)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,SwallowScreenResults = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SwallowScreenResults)[1]', 'varchar(max)'),'')
	,SwallowAssessmentCompletedNAReasonyWhy = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/SwallowAssessmentCompletedNAReasonyWhy)[1]', 'varchar(max)'),'')
	,DutyTherapistNotified = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/DutyTherapistNotified)[1]', 'varchar(max)'),'')
	,DateTimeDutyTherapistNotified = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeDutyTherapistNotified)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,StrokePhysicianNotified = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/StrokePhysicianNotified)[1]', 'varchar(max)'),'')
	,DateTimeStrokePhysicianNotified = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeStrokePhysicianNotified)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,WillAspirinNeedAdminsteringOnStrokeUnit = 
		nullif(CandidateData.Details.value('(/LikelyStrokeDTO/WillAspirinNeedAdminsteringOnStrokeUnit)[1]', 'varchar(max)'),'')
	,Username = 
		CandidateData.Details.value('(/LikelyStrokeDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/LikelyStrokeDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,IsSectionComplete = 
		SectionComplete.Details.value('(/SectionCompletedDTO/CurrentSelection)[1]', 'varchar(max)')
	,SectionCompleteTime = 
		SectionComplete.DateTimeCreated
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
left outer join 
(
	select
		*
	from
		[$(SID)].dbo.TARNCandidateData 
	where
		SectionID = 16
	and ActionID = 50
) SectionComplete
on SectionComplete.CandidateID = CandidateData.CandidateID
where
	CandidateData.SectionID = 16
and CandidateData.ActionID =99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDLikelyStroke', @Stats, @StartTime





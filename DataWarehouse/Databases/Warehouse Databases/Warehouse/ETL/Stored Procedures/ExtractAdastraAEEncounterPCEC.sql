﻿-- use Warehouse

CREATE Procedure [ETL].[ExtractAdastraAEEncounterPCEC]
as

Truncate table ETL.[TImportAEEncounterPCEC]

Insert into ETL.[TImportAEEncounterPCEC]

Select
	[SourceUniqueID] = CaseExport.CaseRef
           ,[CaseNo] = Encounter.CaseNo
           ,[ActiveTime] = CaseExport.ActiveDate
           ,[EntryTime] = cast(convert(nvarchar,CaseExport.EntryDate ,100) as datetime)
           ,[ProviderGroupAdditionalText] = CaseExport.ProviderGroupAdditionalText
           ,[Provider] = CaseExport.Provider
           ,[ProviderAdditionalText] = CaseExport.ProviderAdditionalText
           ,[OrganisationGroup] = CaseExport.OrganisationGroup
           ,[CallerName] = CaseExport.CallersName
           ,[CaseSummary] = CaseExport.CaseSummary
           ,[RegistrationType] = CaseExport.RegistrationType
           ,[Cancelled] = CaseExport.Cancelled
           ,[TestCall] = CaseExport.TestCall
           ,[Answer] = ArrivalMode.AnswerText
           ,[ExtractTime] = SYSDATETIME()
           ,[DayOfWeek] = CodesAndInfOutcomes.[DayOfWeek]
           ,[ReceivingOperator] = Encounter.OperatorWhoReceivedCase
           ,[ReportedCondition] = Encounter.ReportedCondition
           ,[ReceiveTime] = Encounter.ReceiveTime
           ,[CaseStartTime] = Encounter.CaseStartDate
           ,[PatientForename] = Encounter.Forename
           ,[PatientSurname] = Encounter.Surname
           ,[SexCode] = Encounter.Sex
           ,[DateOfBirth] = Encounter.Dob
           ,[Age] = Encounter.Age
           ,[PatientAddress1] = Encounter.AddressLine1
           ,[PatientAddress2] = Encounter.AddressLine2
           ,[PatientAddress3] = Encounter.AddressLine3
           ,[Postcode] = Encounter.Postcode
           ,[PatientHomePhone] = Encounter.HomePhone
           ,[PatientCurrentAddress1] = Encounter.CurrentAddressLine1
           ,[PatientCurrentAddress2] = Encounter.CurrentAddressLine2
           ,[PatientCurrentAddress3] = Encounter.CurrentAddressLine3
           ,[CurrentPostcode] = Encounter.CurrentPostcode
           ,[PatientReturnPhone] = Encounter.ReturnPhoneNo
           ,[Clinician] = Encounter.CliniciansName
           ,[ProviderGroup] = Encounter.ProviderGroup
           ,[CallOrigin] = Encounter.CallOrigin
           ,[Caller] = Encounter.CallerName
           ,[CallerPhone] = Encounter.CallerPhone
           ,[CaseType] = Encounter.CaseType
           ,[LatestPriority] = Encounter.LatestPriority
           ,[Location] = Encounter.LocationName
           ,[BookedDate] = Encounter.BookedDate
           ,[PCCArrival] = Encounter.PCCArrival
           ,[FollowUp] = Encounter.Followup
           ,[NHSDReportedCondition] = Encounter.NHSDReportedCondition
           ,[ClinicalSum] = Encounter.ClinicalSum
           ,[DispositionText] = Encounter.DispositionText
           ,[OutcomeCode] = Encounter.OutcomeCode
           ,[TriageBy] = Encounter.TriageBy
           ,[TriageStartTime] = Encounter.triageStart
           ,[TriageEndTime] = Encounter.TriageFinish
           ,[ReceivedBy] = Encounter.ReceiveBy
           ,[ReceivedTimeNHSD] = Encounter.ReceiveTimeNHSD
           ,[Allergy] = Encounter.Allergy
           ,[Medications] = Encounter.Medications
           ,[PreviousMedicalHistory] = Encounter.PreviousMedicalHistory
           ,[AdviceSummary] = Encounter.AdviceSummary
           ,[AdviceHistory] = Encounter.AdviceHistory
           ,[AdviceExamination] = Encounter.AdviceExamination
           ,[AdviceDiagnosis] = Encounter.AdvDiagnosis
           ,[AdviceTreatment] = Encounter.AdviceTreatment
           ,[AdviceStartTime] = Encounter.AdviceBeganTime
           ,[AdviceEndTime] = Encounter.AdviceEndTime
           ,[AdviceConsultationClinician] = AdviceConsCliniciansName
           ,[ConsultationSummary] = Encounter.ConsSummary
           ,[ConsultationHistory] = Encounter.ConsHistory
           ,[ConsultationExamination] = Encounter.ConsExamination
           ,[ConsultationDiagnosis] = Encounter.ConsDiagnosis
           ,[ConsultationTreatment] = Encounter.ConsTreatment
           ,[ConsultationClinician] = Encounter.ConsCliniciansName
           ,[ClinicalCodes] = Encounter.ClinicalCodes
           ,[ExaminationDetails] = Encounter.ExaminationDetails
           ,[Prescriptions] = Encounter.Prescriptions
           ,[FollowUpWithComments] = Encounter.FollowupWithComments
           ,NHSNumber = Encounter.nationalCode
           ,[CallDate] = CodesAndInfOutcomes.DateOfCall
           ,[CallTime] = CodesAndInfOutcomes.TImeOfCall
           ,[Codes] = CodesAndInfOutcomes.Codes
           ,[InformationalOutcomes] = CodesAndInfOutcomes.InformationalOutcomes
           ,[ConsultationStartTime] = cast(convert(nvarchar,EntryToFirstConsult.ConsultStart ,100) as datetime)
           ,[ConsultationEndTime] = cast(convert(nvarchar,EntryToFirstConsult.ConsultEnd ,100) as datetime)
           ,[TimeTaken] = EntryToFirstConsult.TimeTaken
           ,[ConsultationStartType] = EntryToFirstConsult.StartConsultType
           ,[ConsultationEndType] = EntryToFirstConsult.EndConsultType
           ,[RelationshipToCaller] = RelationshipToCallerCases.RTC
           ,SiteCode = 'RW3MR'
           ,InterfaceCode = 'ADAS'
from
	[$(PCECStaging)].Staging.FullCaseReportAllCasesLocationRO Encounter

left join [$(PCECStaging)].Staging.CaseExport 
on CaseExport.CaseNo = Encounter.CaseNo 

left join 
	(
	select 
		 CaseNo
		,AnswerText
	from
		 [$(PCECStaging)].Staging.CaseQuestionSet
	where
		QuestionText = 'Travel arrangements to the WIC'
	and	not exists (
			select
				1
			from
				[$(PCECStaging)].Staging.CaseQuestionSet Earliest
			where
				QuestionText = 'Travel arrangements to the WIC'
			and	CaseQuestionSet.CaseNo = Earliest.CaseNo
			and	CaseQuestionSet.AnswerText > Earliest.AnswerText
			)
		)ArrivalMode

on ArrivalMode.CaseNo = Encounter.CaseNo 

left join [$(PCECStaging)].Staging.CodesAndInfOutcomes 
on CodesAndInfOutcomes.CaseNo = Encounter.CaseNo 

left join [$(PCECStaging)].Staging.EntryToFirstConsult     
on EntryToFirstConsult.CaseNo = Encounter.CaseNo 

left join [$(PCECStaging)].Staging.RelationshipToCallerCases 
on RelationshipToCallerCases.caseNo = Encounter.CaseNo 



﻿create proc [ETL].[LoadDictationDocument]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table Dictation.Document

insert into Dictation.Document
(
	--DocumentCode 
	SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length]
	,[DocumentTypeCode]
	,[Priority]
	,[StatusCode]
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate]
	,[CreationTime]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate]
	,[CorrectionTime]
	,[CorrectionTimeTaken]
	,[WaitingTimeAuthorization]
	,[AuthorisationDate]
	,[AuthorisationTime]
	,[PassThroughTimeTaken]
	,[ContextCode]
	,[DictationTimeTaken]
	,[CorrectionRate]
	,WarehouseInterfaceCode
)

select
	--DocumentCode 
	SourceUniqueID
	,[Document]
	,[BatchNumber]
	,[Length]
	,[DocumentTypeCode]
	,[Priority]
	,[StatusCode]
	,[WorktypeCode]
	,[WorkStationCode]
	,[InterfaceCode]
	,[GroupCode]
	,[AuthorCode]
	,[TranscriptionistCode]
	,[AuthoriserCode]
	,[CreationDate]
	,[CreationTime]
	,[RecognitionRate]
	,[WaitingTimeCorrection]
	,[CorrectionDate]
	,[CorrectionTime]
	,[CorrectionTimeTaken]
	,[WaitingTimeAuthorization]
	,[AuthorisationDate]
	,[AuthorisationTime]
	,[PassThroughTimeTaken]
	,[ContextCode]
	,[DictationTimeTaken]
	,[CorrectionRate]
	,WarehouseInterfaceCode
from
	[ETL].[TLoadDictationDocument]

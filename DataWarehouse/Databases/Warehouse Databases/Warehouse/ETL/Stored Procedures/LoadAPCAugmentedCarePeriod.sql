﻿


CREATE proc [ETL].[LoadAPCAugmentedCarePeriod]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()

truncate table APC.AugmentedCarePeriod

insert into APC.AugmentedCarePeriod

(
	SourceUniqueID 
	,SourcePatientNo 
	,SourceSpellNo 
	,ProviderSpellNo
	,ConsultantCode 
	,AcpDisposalCode 
	,LocalIdentifier 
	,LocationCode 
	,OutcomeIndicator 
	,Source 
	,SpecialtyCode 
	,Status
	,StartDate 
	,StartTime 
	,EndDate 
	,EndTime 
	,AdvancedRespiratorySystemIndicator 
	,BasicRespiratorySystemIndicator 
	,CirculatorySystemIndicator 
	,NeurologicalSystemIndicator 
	,RenalSystemIndicator 
	,NoOfSupportSystemsUsed 
	,HighDependencyCareLevelDays 
	,IntensiveCareLevelDays 
	,PlannedAcpPeriodIndicator 
	,ReviseByUserId
	,ReviseTime
)

select
	SourceUniqueID 
	,SourcePatientNo 
	,SourceSpellNo 
	,ProviderSpellNo
	,ConsultantCode 
	,AcpDisposalCode 
	,LocalIdentifier 
	,LocationCode 
	,OutcomeIndicator 
	,Source 
	,SpecialtyCode 
	,Status
	,StartDate 
	,StartTime 
	,EndDate 
	,EndTime 
	,AdvancedRespiratorySystemIndicator 
	,BasicRespiratorySystemIndicator 
	,CirculatorySystemIndicator 
	,NeurologicalSystemIndicator 
	,RenalSystemIndicator 
	,NoOfSupportSystemsUsed 
	,HighDependencyCareLevelDays 
	,IntensiveCareLevelDays 
	,PlannedAcpPeriodIndicator 
	,ReviseByUserId
	,ReviseTime
from 
	ETL.TLoadAPCAugmentedCarePeriod


select @RowsInserted = @@rowcount
select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Rows inserted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec Utility.WriteAuditLogEvent @Sproc, @Stats, @StartTime




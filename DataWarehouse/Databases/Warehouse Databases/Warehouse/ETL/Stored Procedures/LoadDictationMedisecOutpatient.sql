﻿
CREATE PROCEDURE [ETL].[LoadDictationMedisecOutpatient]

as

set dateformat dmy


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, AppointmentTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, AppointmentTime)) 
from
	ETL.TLoadDictationMedisecOutpatient

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));
	
	
merge
	Dictation.MedisecOutpatient target
using
	(
	select
		SourcePatientNo =  cast(SourcePatientNo as varchar(max))
		,AppointmentTime = cast(AppointmentTime as datetime)
		,DoctorCode = cast(DoctorCode as varchar(max))
		,InterfaceCode = cast(InterfaceCode as varchar(max))

	from
		ETL.TLoadDictationMedisecOutpatient
	) source
	on	source.SourcePatientNo = target.SourcePatientNo
	and source.AppointmentTime = target.AppointmentTime	

	when not matched by source
	and	target.AppointmentTime between @LoadStartDate and @LoadEndDate

	then delete
	
	
	when not matched
	then
		insert
			(
			SourcePatientNo
			,AppointmentTime
			,DoctorCode
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			SourcePatientNo
			,AppointmentTime
			,DoctorCode
			,InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)


	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.AppointmentTime, getdate()) = isnull(source.AppointmentTime, getdate())
		and isnull(target.DoctorCode, '') = isnull(source.DoctorCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
		
	then
		update
		set
			target.SourcePatientNo = source.SourcePatientNo
			,target.AppointmentTime = source.AppointmentTime
			,target.DoctorCode = source.DoctorCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
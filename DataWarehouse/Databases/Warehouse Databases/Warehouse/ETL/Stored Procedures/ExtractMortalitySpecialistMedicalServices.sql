﻿CREATE procedure [ETL].[ExtractMortalitySpecialistMedicalServices]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.SpecialistMedicalServices target
using
	(
	select
		 SourceUniqueID
		,FormTypeID
		,ReviewStatus
		,Forename
		,Surname
		,CasenoteNo
		,Age
		,AdmissionTime
		,DateOfDeath
		,PlaceOfDeath
		,LeadConsultant
		,OtherConsultants
		,ReviewedDate
		,ReviewedBy
		,PrimaryDiagnosisOnAdmission
		,PrimaryDiagnosisOnAdmissionComment
		,PrimaryDiagnosisAfterTestsConfirmed
		,PrimaryDiagnosisAfterTestsConfirmedComment
		,CauseOfDeath1a
		,CauseOfDeath1b
		,CauseOfDeath1c
		,CauseOfDeath2
		,CauseOfDeathComment
		,CodedDiagnosis
		,CodedDiagnosisComment
		,AgreeWithCauseOfDeath
		,AgreeWithCauseOfDeathComment
		,HospitalPostmortem
		,HospitalPostmortemComment
		,CoronerInformedOrConsulted
		,CoronerInformedOrConsultedComment
		,CoronerPostmortemPerformed
		,CoronerPostmortemPerformedComment
		,MalignancyPresent
		,MalignancyPresentComment
		,CoExistingFactors
		,CoExistingFactorsComment
		,ConsultantReview
		,ConsultantReviewComment
		,EvidenceOfClearManagementPlan
		,EvidenceOfClearManagementPlanComment
		,EssentialInvestigationsObtainedWithoutDelay
		,EssentialInvestigationsObtainedWithoutDelayComment
		,InitialManagementStepsAppropriateAndAdequate
		,InitialManagementStepsAppropriateAndAdequateComment
		,OmissionsInInitialManagement
		,OmissionsInInitialManagementComment
		,AdmittedToAppropriateWard
		,AdmittedToAppropriateWardComment
		,HowManyWardsPatientOn
		,HowManyWardsPatientOnComment
		,MedicalStaffWriteInNotesEveryWeekday
		,MedicalStaffWriteInNotesEveryWeekdayComment
		,ConsultantReviewAtLeastTwiceAWeek
		,ConsultantReviewAtLeastTwiceAWeekComment
		,ICUOpinionGiven
		,ICUOpinionGivenComment
		,TransferFromGeneralToICUorHDU
		,TransferFromGeneralToICUorHDUComment
		,ReadmissionToICUorHDU
		,ReadmissionToICUorHDUComment
		,AnyRecognisableMedicationErrors
		,AnyRecognisableMedicationErrorsComment
		,ChemotherapyOrImmunosuppressantsWithin30Days
		,ChemotherapyOrImmunosuppressantsWithin30DaysComment
		,TimingOfDiagnosis
		,TimingOfDiagnosisComment
		,TimingOfDeliveringCare
		,TimingOfDeliveringCareComment
		,CommunicationIssues
		,CommunicationIssuesComment
		,AdverseEvents
		,AdverseEventsComment
		,IncidentReport
		,IncidentReportComment
		,AnythingCouldBeDoneDifferently
		,AnythingCouldBeDoneDifferentlyComment
		,DocumentationIssues
		,DocumentationIssuesComment
		,NotableGoodQualityCare
		,LCPInPatientsNotes
		,LCPInPatientsNotesComment
		,PreferredPlaceOfDeathRecorded
		,PreferredPlaceOfDeathRecordedComment
		,ExpressedWithToDieOutsideOfHospital
		,ExpressedWithToDieOutsideOfHospitalComment
		,LessonsLearned
		,ActionPlan
		,ActionPlanReviewDate
		,SourcePatientNo
		,EpisodeStartTime
		,ContextCode
	from
		ETL.TLoadMortalitySpecialistMedicalServices
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalitySpecialistMedicalServices Latest
			where
				Latest.SourcePatientNo = TLoadMortalitySpecialistMedicalServices.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalitySpecialistMedicalServices.SourceUniqueID
			)
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,FormTypeID
			,ReviewStatus
			,Forename
			,Surname
			,CasenoteNo
			,Age
			,AdmissionTime
			,DateOfDeath
			,PlaceOfDeath
			,LeadConsultant
			,OtherConsultants
			,ReviewedDate
			,ReviewedBy
			,PrimaryDiagnosisOnAdmission
			,PrimaryDiagnosisOnAdmissionComment
			,PrimaryDiagnosisAfterTestsConfirmed
			,PrimaryDiagnosisAfterTestsConfirmedComment
			,CauseOfDeath1a
			,CauseOfDeath1b
			,CauseOfDeath1c
			,CauseOfDeath2
			,CauseOfDeathComment
			,CodedDiagnosis
			,CodedDiagnosisComment
			,AgreeWithCauseOfDeath
			,AgreeWithCauseOfDeathComment
			,HospitalPostmortem
			,HospitalPostmortemComment
			,CoronerInformedOrConsulted
			,CoronerInformedOrConsultedComment
			,CoronerPostmortemPerformed
			,CoronerPostmortemPerformedComment
			,MalignancyPresent
			,MalignancyPresentComment
			,CoExistingFactors
			,CoExistingFactorsComment
			,ConsultantReview
			,ConsultantReviewComment
			,EvidenceOfClearManagementPlan
			,EvidenceOfClearManagementPlanComment
			,EssentialInvestigationsObtainedWithoutDelay
			,EssentialInvestigationsObtainedWithoutDelayComment
			,InitialManagementStepsAppropriateAndAdequate
			,InitialManagementStepsAppropriateAndAdequateComment
			,OmissionsInInitialManagement
			,OmissionsInInitialManagementComment
			,AdmittedToAppropriateWard
			,AdmittedToAppropriateWardComment
			,HowManyWardsPatientOn
			,HowManyWardsPatientOnComment
			,MedicalStaffWriteInNotesEveryWeekday
			,MedicalStaffWriteInNotesEveryWeekdayComment
			,ConsultantReviewAtLeastTwiceAWeek
			,ConsultantReviewAtLeastTwiceAWeekComment
			,ICUOpinionGiven
			,ICUOpinionGivenComment
			,TransferFromGeneralToICUorHDU
			,TransferFromGeneralToICUorHDUComment
			,ReadmissionToICUorHDU
			,ReadmissionToICUorHDUComment
			,AnyRecognisableMedicationErrors
			,AnyRecognisableMedicationErrorsComment
			,ChemotherapyOrImmunosuppressantsWithin30Days
			,ChemotherapyOrImmunosuppressantsWithin30DaysComment
			,TimingOfDiagnosis
			,TimingOfDiagnosisComment
			,TimingOfDeliveringCare
			,TimingOfDeliveringCareComment
			,CommunicationIssues
			,CommunicationIssuesComment
			,AdverseEvents
			,AdverseEventsComment
			,IncidentReport
			,IncidentReportComment
			,AnythingCouldBeDoneDifferently
			,AnythingCouldBeDoneDifferentlyComment
			,DocumentationIssues
			,DocumentationIssuesComment
			,NotableGoodQualityCare
			,LCPInPatientsNotes
			,LCPInPatientsNotesComment
			,PreferredPlaceOfDeathRecorded
			,PreferredPlaceOfDeathRecordedComment
			,ExpressedWithToDieOutsideOfHospital
			,ExpressedWithToDieOutsideOfHospitalComment
			,LessonsLearned
			,ActionPlan
			,ActionPlanReviewDate
			,SourcePatientNo
			,EpisodeStartTime
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.FormTypeID
			,source.ReviewStatus
			,source.Forename
			,source.Surname
			,source.CasenoteNo
			,source.Age
			,source.AdmissionTime
			,source.DateOfDeath
			,source.PlaceOfDeath
			,source.LeadConsultant
			,source.OtherConsultants
			,source.ReviewedDate
			,source.ReviewedBy
			,source.PrimaryDiagnosisOnAdmission
			,source.PrimaryDiagnosisOnAdmissionComment
			,source.PrimaryDiagnosisAfterTestsConfirmed
			,source.PrimaryDiagnosisAfterTestsConfirmedComment
			,source.CauseOfDeath1a
			,source.CauseOfDeath1b
			,source.CauseOfDeath1c
			,source.CauseOfDeath2
			,source.CauseOfDeathComment
			,source.CodedDiagnosis
			,source.CodedDiagnosisComment
			,source.AgreeWithCauseOfDeath
			,source.AgreeWithCauseOfDeathComment
			,source.HospitalPostmortem
			,source.HospitalPostmortemComment
			,source.CoronerInformedOrConsulted
			,source.CoronerInformedOrConsultedComment
			,source.CoronerPostmortemPerformed
			,source.CoronerPostmortemPerformedComment
			,source.MalignancyPresent
			,source.MalignancyPresentComment
			,source.CoExistingFactors
			,source.CoExistingFactorsComment
			,source.ConsultantReview
			,source.ConsultantReviewComment
			,source.EvidenceOfClearManagementPlan
			,source.EvidenceOfClearManagementPlanComment
			,source.EssentialInvestigationsObtainedWithoutDelay
			,source.EssentialInvestigationsObtainedWithoutDelayComment
			,source.InitialManagementStepsAppropriateAndAdequate
			,source.InitialManagementStepsAppropriateAndAdequateComment
			,source.OmissionsInInitialManagement
			,source.OmissionsInInitialManagementComment
			,source.AdmittedToAppropriateWard
			,source.AdmittedToAppropriateWardComment
			,source.HowManyWardsPatientOn
			,source.HowManyWardsPatientOnComment
			,source.MedicalStaffWriteInNotesEveryWeekday
			,source.MedicalStaffWriteInNotesEveryWeekdayComment
			,source.ConsultantReviewAtLeastTwiceAWeek
			,source.ConsultantReviewAtLeastTwiceAWeekComment
			,source.ICUOpinionGiven
			,source.ICUOpinionGivenComment
			,source.TransferFromGeneralToICUorHDU
			,source.TransferFromGeneralToICUorHDUComment
			,source.ReadmissionToICUorHDU
			,source.ReadmissionToICUorHDUComment
			,source.AnyRecognisableMedicationErrors
			,source.AnyRecognisableMedicationErrorsComment
			,source.ChemotherapyOrImmunosuppressantsWithin30Days
			,source.ChemotherapyOrImmunosuppressantsWithin30DaysComment
			,source.TimingOfDiagnosis
			,source.TimingOfDiagnosisComment
			,source.TimingOfDeliveringCare
			,source.TimingOfDeliveringCareComment
			,source.CommunicationIssues
			,source.CommunicationIssuesComment
			,source.AdverseEvents
			,source.AdverseEventsComment
			,source.IncidentReport
			,source.IncidentReportComment
			,source.AnythingCouldBeDoneDifferently
			,source.AnythingCouldBeDoneDifferentlyComment
			,source.DocumentationIssues
			,source.DocumentationIssuesComment
			,source.NotableGoodQualityCare
			,source.LCPInPatientsNotes
			,source.LCPInPatientsNotesComment
			,source.PreferredPlaceOfDeathRecorded
			,source.PreferredPlaceOfDeathRecordedComment
			,source.ExpressedWithToDieOutsideOfHospital
			,source.ExpressedWithToDieOutsideOfHospitalComment
			,source.LessonsLearned
			,source.ActionPlan
			,source.ActionPlanReviewDate
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.ContextCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			isnull(target.FormTypeID, 0) = isnull(source.FormTypeID, 0)
		and isnull(target.ReviewStatus, 0) = isnull(source.ReviewStatus, 0)
		and	isnull(target.Forename, '') = isnull(source.Forename, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.Age, 0) = isnull(source.Age, 0)
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.PlaceOfDeath, '') = isnull(source.PlaceOfDeath, '')
		and isnull(target.LeadConsultant, '') = isnull(source.LeadConsultant, '')
		and isnull(target.OtherConsultants, '') = isnull(source.OtherConsultants, '')
		and isnull(target.ReviewedDate, getdate()) = isnull(source.ReviewedDate, getdate())
		and isnull(target.ReviewedBy, '') = isnull(source.ReviewedBy, '')
		and isnull(target.PrimaryDiagnosisOnAdmission, 0) = isnull(source.PrimaryDiagnosisOnAdmission, 0)
		and isnull(target.PrimaryDiagnosisOnAdmissionComment, '') = isnull(source.PrimaryDiagnosisOnAdmissionComment, '')
		and isnull(target.PrimaryDiagnosisAfterTestsConfirmed, 0) = isnull(source.PrimaryDiagnosisAfterTestsConfirmed, 0)
		and isnull(target.PrimaryDiagnosisAfterTestsConfirmedComment, '') = isnull(source.PrimaryDiagnosisAfterTestsConfirmedComment, '')
		and isnull(target.CauseOfDeath1a, 0) = isnull(source.CauseOfDeath1a, 0)
		and isnull(target.CauseOfDeath1b, 0) = isnull(source.CauseOfDeath1b, 0)
		and isnull(target.CauseOfDeath1c, 0) = isnull(source.CauseOfDeath1c, 0)
		and isnull(target.CauseOfDeath2, 0) = isnull(source.CauseOfDeath2, 0)
		and isnull(target.CauseOfDeathComment, '') = isnull(source.CauseOfDeathComment, '')
		and isnull(target.CodedDiagnosis, 0) = isnull(source.CodedDiagnosis, 0)
		and isnull(target.CodedDiagnosisComment, '') = isnull(source.CodedDiagnosisComment, '')
		and isnull(target.AgreeWithCauseOfDeath, 0) = isnull(source.AgreeWithCauseOfDeath, 0)
		and isnull(target.AgreeWithCauseOfDeathComment, '') = isnull(source.AgreeWithCauseOfDeathComment, '')
		and isnull(target.HospitalPostmortem, 0) = isnull(source.HospitalPostmortem, 0)
		and isnull(target.HospitalPostmortemComment, '') = isnull(source.HospitalPostmortemComment, '')
		and isnull(target.CoronerInformedOrConsulted, 0) = isnull(source.CoronerInformedOrConsulted, 0)
		and isnull(target.CoronerInformedOrConsultedComment, '') = isnull(source.CoronerInformedOrConsultedComment, '')
		and isnull(target.CoronerPostmortemPerformed, 0) = isnull(source.CoronerPostmortemPerformed, 0)
		and isnull(target.CoronerPostmortemPerformedComment, '') = isnull(source.CoronerPostmortemPerformedComment, '')
		and isnull(target.MalignancyPresent, 0) = isnull(source.MalignancyPresent, 0)
		and isnull(target.MalignancyPresentComment, '') = isnull(source.MalignancyPresentComment, '')
		and isnull(target.CoExistingFactors, 0) = isnull(source.CoExistingFactors, 0)
		and isnull(target.CoExistingFactorsComment, '') = isnull(source.CoExistingFactorsComment, '')
		and isnull(target.ConsultantReview, 0) = isnull(source.ConsultantReview, 0)
		and isnull(target.ConsultantReviewComment, '') = isnull(source.ConsultantReviewComment, '')
		and isnull(target.EvidenceOfClearManagementPlan, 0) = isnull(source.EvidenceOfClearManagementPlan, 0)
		and isnull(target.EvidenceOfClearManagementPlanComment, '') = isnull(source.EvidenceOfClearManagementPlanComment, '')
		and isnull(target.EssentialInvestigationsObtainedWithoutDelay, 0) = isnull(source.EssentialInvestigationsObtainedWithoutDelay, 0)
		and isnull(target.EssentialInvestigationsObtainedWithoutDelayComment, '') = isnull(source.EssentialInvestigationsObtainedWithoutDelayComment, '')
		and isnull(target.InitialManagementStepsAppropriateAndAdequate, 0) = isnull(source.InitialManagementStepsAppropriateAndAdequate, 0)
		and isnull(target.InitialManagementStepsAppropriateAndAdequateComment, '') = isnull(source.InitialManagementStepsAppropriateAndAdequateComment, '')
		and isnull(target.OmissionsInInitialManagement, 0) = isnull(source.OmissionsInInitialManagement, 0)
		and isnull(target.OmissionsInInitialManagementComment, '') = isnull(source.OmissionsInInitialManagementComment, '')
		and isnull(target.AdmittedToAppropriateWard, 0) = isnull(source.AdmittedToAppropriateWard, 0)
		and isnull(target.AdmittedToAppropriateWardComment, '') = isnull(source.AdmittedToAppropriateWardComment, '')
		and isnull(target.HowManyWardsPatientOn, 0) = isnull(source.HowManyWardsPatientOn, 0)
		and isnull(target.HowManyWardsPatientOnComment, '') = isnull(source.HowManyWardsPatientOnComment, '')
		and isnull(target.MedicalStaffWriteInNotesEveryWeekday, 0) = isnull(source.MedicalStaffWriteInNotesEveryWeekday, 0)
		and isnull(target.MedicalStaffWriteInNotesEveryWeekdayComment, '') = isnull(source.MedicalStaffWriteInNotesEveryWeekdayComment, '')
		and isnull(target.ConsultantReviewAtLeastTwiceAWeek, 0) = isnull(source.ConsultantReviewAtLeastTwiceAWeek, 0)
		and isnull(target.ConsultantReviewAtLeastTwiceAWeekComment, '') = isnull(source.ConsultantReviewAtLeastTwiceAWeekComment, '')
		and isnull(target.ICUOpinionGiven, 0) = isnull(source.ICUOpinionGiven, 0)
		and isnull(target.ICUOpinionGivenComment, '') = isnull(source.ICUOpinionGivenComment, '')
		and isnull(target.TransferFromGeneralToICUorHDU, 0) = isnull(source.TransferFromGeneralToICUorHDU, 0)
		and isnull(target.TransferFromGeneralToICUorHDUComment, '') = isnull(source.TransferFromGeneralToICUorHDUComment, '')
		and isnull(target.ReadmissionToICUorHDU, 0) = isnull(source.ReadmissionToICUorHDU, 0)
		and isnull(target.ReadmissionToICUorHDUComment, '') = isnull(source.ReadmissionToICUorHDUComment, '')
		and isnull(target.AnyRecognisableMedicationErrors, 0) = isnull(source.AnyRecognisableMedicationErrors, 0)
		and isnull(target.AnyRecognisableMedicationErrorsComment, '') = isnull(source.AnyRecognisableMedicationErrorsComment, '')
		and isnull(target.ChemotherapyOrImmunosuppressantsWithin30Days, 0) = isnull(source.ChemotherapyOrImmunosuppressantsWithin30Days, 0)
		and isnull(target.ChemotherapyOrImmunosuppressantsWithin30DaysComment, '') = isnull(source.ChemotherapyOrImmunosuppressantsWithin30DaysComment, '')
		and isnull(target.TimingOfDiagnosis, 0) = isnull(source.TimingOfDiagnosis, 0)
		and isnull(target.TimingOfDiagnosisComment, '') = isnull(source.TimingOfDiagnosisComment, '')
		and isnull(target.TimingOfDeliveringCare, 0) = isnull(source.TimingOfDeliveringCare, 0)
		and isnull(target.TimingOfDeliveringCareComment, '') = isnull(source.TimingOfDeliveringCareComment, '')
		and isnull(target.CommunicationIssues, 0) = isnull(source.CommunicationIssues, 0)
		and isnull(target.CommunicationIssuesComment, '') = isnull(source.CommunicationIssuesComment, '')
		and isnull(target.AdverseEvents, 0) = isnull(source.AdverseEvents, 0)
		and isnull(target.AdverseEventsComment, '') = isnull(source.AdverseEventsComment, '')
		and isnull(target.IncidentReport, 0) = isnull(source.IncidentReport, 0)
		and isnull(target.IncidentReportComment, '') = isnull(source.IncidentReportComment, '')
		and isnull(target.AnythingCouldBeDoneDifferently, 0) = isnull(source.AnythingCouldBeDoneDifferently, 0)
		and isnull(target.AnythingCouldBeDoneDifferentlyComment, '') = isnull(source.AnythingCouldBeDoneDifferentlyComment, '')
		and isnull(target.DocumentationIssues, 0) = isnull(source.DocumentationIssues, 0)
		and isnull(target.DocumentationIssuesComment, '') = isnull(source.DocumentationIssuesComment, '')
		and isnull(target.NotableGoodQualityCare, '') = isnull(source.NotableGoodQualityCare, '')
		and isnull(target.LCPInPatientsNotes, 0) = isnull(source.LCPInPatientsNotes, 0)
		and isnull(target.LCPInPatientsNotesComment, '') = isnull(source.LCPInPatientsNotesComment, '')
		and isnull(target.PreferredPlaceOfDeathRecorded, 0) = isnull(source.PreferredPlaceOfDeathRecorded, 0)
		and isnull(target.PreferredPlaceOfDeathRecordedComment, '') = isnull(source.PreferredPlaceOfDeathRecordedComment, '')
		and isnull(target.ExpressedWithToDieOutsideOfHospital, 0) = isnull(source.ExpressedWithToDieOutsideOfHospital, 0)
		and isnull(target.ExpressedWithToDieOutsideOfHospitalComment, '') = isnull(source.ExpressedWithToDieOutsideOfHospitalComment, '')
		and isnull(target.LessonsLearned, '') = isnull(source.LessonsLearned, '')
		and isnull(target.ActionPlan, '') = isnull(source.ActionPlan, '')
		and isnull(target.ActionPlanReviewDate, getdate()) = isnull(source.ActionPlanReviewDate, getdate())
		and isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.FormTypeID = source.FormTypeID
			,target.ReviewStatus = source.ReviewStatus
			,target.Forename = source.Forename
			,target.Surname = source.Surname
			,target.CasenoteNo = source.CasenoteNo
			,target.Age = source.Age
			,target.AdmissionTime = source.AdmissionTime
			,target.DateOfDeath = source.DateOfDeath
			,target.PlaceOfDeath = source.PlaceOfDeath
			,target.LeadConsultant = source.LeadConsultant
			,target.OtherConsultants = source.OtherConsultants
			,target.ReviewedDate = source.ReviewedDate
			,target.ReviewedBy = source.ReviewedBy
			,target.PrimaryDiagnosisOnAdmission = source.PrimaryDiagnosisOnAdmission
			,target.PrimaryDiagnosisOnAdmissionComment = source.PrimaryDiagnosisOnAdmissionComment
			,target.PrimaryDiagnosisAfterTestsConfirmed = source.PrimaryDiagnosisAfterTestsConfirmed
			,target.PrimaryDiagnosisAfterTestsConfirmedComment = source.PrimaryDiagnosisAfterTestsConfirmedComment
			,target.CauseOfDeath1a = source.CauseOfDeath1a
			,target.CauseOfDeath1b = source.CauseOfDeath1b
			,target.CauseOfDeath1c = source.CauseOfDeath1c
			,target.CauseOfDeath2 = source.CauseOfDeath2
			,target.CauseOfDeathComment = source.CauseOfDeathComment
			,target.CodedDiagnosis = source.CodedDiagnosis
			,target.CodedDiagnosisComment = source.CodedDiagnosisComment
			,target.AgreeWithCauseOfDeath = source.AgreeWithCauseOfDeath
			,target.AgreeWithCauseOfDeathComment = source.AgreeWithCauseOfDeathComment
			,target.HospitalPostmortem = source.HospitalPostmortem
			,target.HospitalPostmortemComment = source.HospitalPostmortemComment
			,target.CoronerInformedOrConsulted = source.CoronerInformedOrConsulted
			,target.CoronerInformedOrConsultedComment = source.CoronerInformedOrConsultedComment
			,target.CoronerPostmortemPerformed = source.CoronerPostmortemPerformed
			,target.CoronerPostmortemPerformedComment = source.CoronerPostmortemPerformedComment
			,target.MalignancyPresent = source.MalignancyPresent
			,target.MalignancyPresentComment = source.MalignancyPresentComment
			,target.CoExistingFactors = source.CoExistingFactors
			,target.CoExistingFactorsComment = source.CoExistingFactorsComment
			,target.ConsultantReview = source.ConsultantReview
			,target.ConsultantReviewComment = source.ConsultantReviewComment
			,target.EvidenceOfClearManagementPlan = source.EvidenceOfClearManagementPlan
			,target.EvidenceOfClearManagementPlanComment = source.EvidenceOfClearManagementPlanComment
			,target.EssentialInvestigationsObtainedWithoutDelay = source.EssentialInvestigationsObtainedWithoutDelay
			,target.EssentialInvestigationsObtainedWithoutDelayComment = source.EssentialInvestigationsObtainedWithoutDelayComment
			,target.InitialManagementStepsAppropriateAndAdequate = source.InitialManagementStepsAppropriateAndAdequate
			,target.InitialManagementStepsAppropriateAndAdequateComment = source.InitialManagementStepsAppropriateAndAdequateComment
			,target.OmissionsInInitialManagement = source.OmissionsInInitialManagement
			,target.OmissionsInInitialManagementComment = source.OmissionsInInitialManagementComment
			,target.AdmittedToAppropriateWard = source.AdmittedToAppropriateWard
			,target.AdmittedToAppropriateWardComment = source.AdmittedToAppropriateWardComment
			,target.HowManyWardsPatientOn = source.HowManyWardsPatientOn
			,target.HowManyWardsPatientOnComment = source.HowManyWardsPatientOnComment
			,target.MedicalStaffWriteInNotesEveryWeekday = source.MedicalStaffWriteInNotesEveryWeekday
			,target.MedicalStaffWriteInNotesEveryWeekdayComment = source.MedicalStaffWriteInNotesEveryWeekdayComment
			,target.ConsultantReviewAtLeastTwiceAWeek = source.ConsultantReviewAtLeastTwiceAWeek
			,target.ConsultantReviewAtLeastTwiceAWeekComment = source.ConsultantReviewAtLeastTwiceAWeekComment
			,target.ICUOpinionGiven = source.ICUOpinionGiven
			,target.ICUOpinionGivenComment = source.ICUOpinionGivenComment
			,target.TransferFromGeneralToICUorHDU = source.TransferFromGeneralToICUorHDU
			,target.TransferFromGeneralToICUorHDUComment = source.TransferFromGeneralToICUorHDUComment
			,target.ReadmissionToICUorHDU = source.ReadmissionToICUorHDU
			,target.ReadmissionToICUorHDUComment = source.ReadmissionToICUorHDUComment
			,target.AnyRecognisableMedicationErrors = source.AnyRecognisableMedicationErrors
			,target.AnyRecognisableMedicationErrorsComment = source.AnyRecognisableMedicationErrorsComment
			,target.ChemotherapyOrImmunosuppressantsWithin30Days = source.ChemotherapyOrImmunosuppressantsWithin30Days
			,target.ChemotherapyOrImmunosuppressantsWithin30DaysComment = source.ChemotherapyOrImmunosuppressantsWithin30DaysComment
			,target.TimingOfDiagnosis = source.TimingOfDiagnosis
			,target.TimingOfDiagnosisComment = source.TimingOfDiagnosisComment
			,target.TimingOfDeliveringCare = source.TimingOfDeliveringCare
			,target.TimingOfDeliveringCareComment = source.TimingOfDeliveringCareComment
			,target.CommunicationIssues = source.CommunicationIssues
			,target.CommunicationIssuesComment = source.CommunicationIssuesComment
			,target.AdverseEvents = source.AdverseEvents
			,target.AdverseEventsComment = source.AdverseEventsComment
			,target.IncidentReport = source.IncidentReport
			,target.IncidentReportComment = source.IncidentReportComment
			,target.AnythingCouldBeDoneDifferently = source.AnythingCouldBeDoneDifferently
			,target.AnythingCouldBeDoneDifferentlyComment = source.AnythingCouldBeDoneDifferentlyComment
			,target.DocumentationIssues = source.DocumentationIssues
			,target.DocumentationIssuesComment = source.DocumentationIssuesComment
			,target.NotableGoodQualityCare = source.NotableGoodQualityCare
			,target.LCPInPatientsNotes = source.LCPInPatientsNotes
			,target.LCPInPatientsNotesComment = source.LCPInPatientsNotesComment
			,target.PreferredPlaceOfDeathRecorded = source.PreferredPlaceOfDeathRecorded
			,target.PreferredPlaceOfDeathRecordedComment = source.PreferredPlaceOfDeathRecordedComment
			,target.ExpressedWithToDieOutsideOfHospital = source.ExpressedWithToDieOutsideOfHospital
			,target.ExpressedWithToDieOutsideOfHospitalComment = source.ExpressedWithToDieOutsideOfHospitalComment
			,target.LessonsLearned = source.LessonsLearned
			,target.ActionPlan = source.ActionPlan
			,target.ActionPlanReviewDate = source.ActionPlanReviewDate
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



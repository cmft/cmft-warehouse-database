﻿


CREATE proc [ETL].[BuildAPCEncounterAlert]

as

truncate table APC.EncounterAlert

insert into APC.EncounterAlert

select 
	Encounter.EncounterRecno
	,Alert.AlertRecno
from
	Observation.Alert

inner join WH.DayLightSaving
on	Alert.CreatedTime between DayLightSaving.StartTime and DayLightSaving.EndTime

inner join APC.Encounter 
on	Alert.CasenoteNumber = Encounter.CasenoteNumber
and dateadd(hour, DayLightSaving.Offset, Alert.CreatedTime) between Encounter.EpisodeStartTime and Encounter.EpisodeEndTime



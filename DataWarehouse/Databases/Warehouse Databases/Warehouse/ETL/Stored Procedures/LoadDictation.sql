﻿
create proc [ETL].[LoadDictation]

as

exec ETL.ExtractDictationDocumentInformation
--exec ETL.LoadDictationDocumentInformation
exec ETL.ExtractDictationArchiveDocument
exec ETL.ExtractDictationSpeechDocument
exec ETL.ExtractDictationReferenceData
exec ETL.LoadDictationDocument



﻿CREATE procedure [ETL].[LoadAPCWaitingListSuspension]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()


declare @CensusDate smalldatetime

select
	@CensusDate =
	(
	select top 1
		CensusDate
	from
		ETL.TImportAPCWLSuspension
	)

-- Clear down old snapshots
delete from APC.WaitingListSuspension
where
	CensusDate = @CensusDate


select @RowsDeleted = @@ROWCOUNT

declare @CensusCutOffDate smalldatetime

select @CensusCutOffDate =
	dateadd(
		 day
		,(
		select
			NumericValue * -1
		from
			Utility.Parameter
		where
			Parameter = 'KEEPSNAPSHOTDAYS'
		)
		,@CensusDate
	)

select
	CensusDate = max(CensusDate) 
into
	#OldSnapshot
from
	APC.WaitingListSuspension 
where
	CensusDate < @CensusCutOffDate
group by
	datepart(year, CensusDate), datepart(month, CensusDate)


delete from APC.WaitingListSuspension
where
	not exists
	(
	select
		1
	from
		#OldSnapshot OldSnapshot
	where
		OldSnapshot.CensusDate = WaitingListSuspension.CensusDate
	)
and	CensusDate < @CensusCutOffDate


select @RowsDeleted = @RowsDeleted + @@ROWCOUNT


insert into APC.WaitingListSuspension
(
	 SourcePatientNo 
	,SourceEncounterNo 
	,SuspensionStartDate 
	,SuspensionEndDate 
	,SuspensionReasonCode 
	,SuspensionReason
	,CensusDate
	,SourceUniqueID
)
select
	 SourcePatientNo 
	,SourceEncounterNo 
	,SuspensionStartDate 
	,SuspensionEndDate 
	,SuspensionReasonCode 
	,SuspensionReason
	,CensusDate
	,SourceUniqueID
from
	ETL.TLoadAPCWaitingListSuspension


select @RowsInserted = @@ROWCOUNT

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadAPCWaitingListSuspension', @Stats, @StartTime

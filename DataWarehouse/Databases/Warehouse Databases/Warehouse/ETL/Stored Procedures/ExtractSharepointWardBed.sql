﻿CREATE proc [ETL].[ExtractSharepointWardBed]

as

declare @StartTime datetime
declare @Elapsed int
declare @Sproc varchar(255)
declare @Stats varchar(255)
declare @RowsInserted int;

select @StartTime = getdate();

truncate table ETL.TImportBedComplementWardBed

--Awaiting Jabran Sharepoint extract solution...

--insert into ETL.TImportBedComplementWardBed
--(
--	 WardID
--	,InpatientBeds
--	,DaycaseBeds
--	,SingleRoomBeds
--	,Comment
--	,StartDate
--)
--select
--	 [WardCode]
--	,[InpatientBeds]
--	,[DaycaseBeds]
--	,[SingleRooms]
--	,[Comments]
--	,[EffectiveDate]
--from [SHAREPOINT]...[qry_WardBedComplement];


select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;

﻿CREATE procedure [ETL].[LoadSMMISBirth]
	 @from smalldatetime = null
	,@to smalldatetime = null
as

if @from is null set @from = '19000101'
if @to is null set @to = getdate()

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()


-- Update status to 0
update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSSMMISBIRTH'
if(@@rowcount = 0)
	insert into Utility.Parameter(Parameter, NumericValue) values('EXTRACTSTATUSSMMISBIRTH',0)

-- Load up staging table
exec ETL.ExtractSMMISBirth @from, @to

-- If status now = 1 ...
if (
	select NumericValue from Utility.Parameter
	where Parameter = 'EXTRACTSTATUSSMMISBIRTH'
	) = 1
begin


-- Log run date
update Utility.Parameter set DateValue = getdate() where Parameter = 'LOADSMMISBIRTHDATE'
if(@@rowcount = 0)
	insert into Utility.Parameter(Parameter, DateValue) values('LOADSMMISBIRTHDATE',getdate())


declare @MergeSummary TABLE(Action nvarchar(10));

merge
	Maternity.Birth target

using
	ETL.TLoadSMMISBirth source
on	source.MotherNo = target.MotherNo
and source.InfantNo = target.InfantNo

when
	matched
and not
	(
		isnull(target.MotherNo, '') = isnull(source.MotherNo, '')
	and isnull(target.MotherNhsNumber, '') = isnull(source.MotherNhsNumber, '')
	and isnull(target.MotherNhsNoValid, '') = isnull(source.MotherNhsNoValid, '')
	and isnull(target.CurrentOccurrence, 0) = isnull(source.CurrentOccurrence, 0)
	and isnull(target.MotherMaidenName, '') = isnull(source.MotherMaidenName, '')
	and isnull(target.MotherCurrentSurname, '') = isnull(source.MotherCurrentSurname, '')
	and isnull(target.MotherFForenames, '') = isnull(source.MotherFForenames, '')
	and isnull(target.MotherTitle, '') = isnull(source.MotherTitle, '')
	and isnull(target.MotherDateOfBirth, getdate()) = isnull(source.MotherDateOfBirth, getdate())
	and isnull(target.MotherPlaceOfBirth, '') = isnull(source.MotherPlaceOfBirth, '')
	and isnull(target.UkEntryDate, getdate()) = isnull(source.UkEntryDate, getdate())
	and isnull(target.MotherDateApprox, '') = isnull(source.MotherDateApprox, '')
	and isnull(target.MotherEthnicGroupPrefix, '') = isnull(source.MotherEthnicGroupPrefix, '')
	and isnull(target.MotherEthnicGroupBody, '') = isnull(source.MotherEthnicGroupBody, '')
	and isnull(target.MotherEthnicGroupOpcs, '') = isnull(source.MotherEthnicGroupOpcs, '')
	and isnull(target.MotherEthnicGroupPas, '') = isnull(source.MotherEthnicGroupPas, '')
	and isnull(target.MotherGpSurname, '') = isnull(source.MotherGpSurname, '')
	and isnull(target.MotherGpSequence, 0) = isnull(source.MotherGpSequence, 0)
	and isnull(target.MotherTraceStatus, '') = isnull(source.MotherTraceStatus, '')
	and isnull(target.MotherLocation, '') = isnull(source.MotherLocation, '')
	and isnull(target.MotherDateOfDeath, getdate()) = isnull(source.MotherDateOfDeath, getdate())
	and isnull(target.MotherTimeOfDeath, '') = isnull(source.MotherTimeOfDeath, '')
	and isnull(target.MotherDeathIndicator, '') = isnull(source.MotherDeathIndicator, '')
	and isnull(target.PregnancyPortable, '') = isnull(source.PregnancyPortable, '')
	and isnull(target.PregnancyDiskCreateDate, getdate()) = isnull(source.PregnancyDiskCreateDate, getdate())
	and isnull(target.PregnancyStatus, '') = isnull(source.PregnancyStatus, '')
	and isnull(target.PregnancyType, '') = isnull(source.PregnancyType, '')
	and isnull(target.PregnancyUnbooked, '') = isnull(source.PregnancyUnbooked, '')
	and isnull(target.PregnancyCardNumber, 0) = isnull(source.PregnancyCardNumber, 0)
	and isnull(target.PregnancyCreateDate, getdate()) = isnull(source.PregnancyCreateDate, getdate())
	and isnull(target.PregnancyCreateTime, '') = isnull(source.PregnancyCreateTime, '')
	and isnull(target.MotherReligion, '') = isnull(source.MotherReligion, '')
	and isnull(target.OtherReligion, '') = isnull(source.OtherReligion, '')
	and isnull(target.Marital, '') = isnull(source.Marital, '')
	and isnull(target.MSurname, '') = isnull(source.MSurname, '')
	and isnull(target.PrefName, '') = isnull(source.PrefName, '')
	and isnull(target.MotherOccupation, '') = isnull(source.MotherOccupation, '')
	and isnull(target.MainIncome, '') = isnull(source.MainIncome, '')
	and isnull(target.PrevEmployment, '') = isnull(source.PrevEmployment, '')
	and isnull(target.MotherTelephoneWork, '') = isnull(source.MotherTelephoneWork, '')
	and isnull(target.OneParentFam, '') = isnull(source.OneParentFam, '')
	and isnull(target.SocialClass, '') = isnull(source.SocialClass, '')
	and isnull(target.BkScreen1Ind, '') = isnull(source.BkScreen1Ind, '')
	and isnull(target.BkScreen1aInd, '') = isnull(source.BkScreen1aInd, '')
	and isnull(target.BkScreen2Ind, '') = isnull(source.BkScreen2Ind, '')
	and isnull(target.InvScreen1Ind, '') = isnull(source.InvScreen1Ind, '')
	and isnull(target.InvScreen2Ind, '') = isnull(source.InvScreen2Ind, '')
	and isnull(target.AnScreen1Ind, '') = isnull(source.AnScreen1Ind, '')
	and isnull(target.AnScreen2Ind, '') = isnull(source.AnScreen2Ind, '')
	and isnull(target.AnReason, '') = isnull(source.AnReason, '')
	and isnull(target.PregnancyAnDate, getdate()) = isnull(source.PregnancyAnDate, getdate())
	and isnull(target.AnOperation, '') = isnull(source.AnOperation, '')
	and isnull(target.PregnancyGpSurname, '') = isnull(source.PregnancyGpSurname, '')
	and isnull(target.PregnancyGpSequence, 0) = isnull(source.PregnancyGpSequence, 0)
	and isnull(target.TypeOfPatient, '') = isnull(source.TypeOfPatient, '')
	and isnull(target.OverseasVisitor, '') = isnull(source.OverseasVisitor, '')
	and isnull(target.PregnancyConCode, '') = isnull(source.PregnancyConCode, '')
	and isnull(target.PregnancyBookingMidwife, '') = isnull(source.PregnancyBookingMidwife, '')
	and isnull(target.PregnancyMidwifeTeam, '') = isnull(source.PregnancyMidwifeTeam, '')
	and isnull(target.ReasonCompletion, '') = isnull(source.ReasonCompletion, '')
	and isnull(target.SpecOtherPlace, '') = isnull(source.SpecOtherPlace, '')
	and isnull(target.ConfirmedDob, '') = isnull(source.ConfirmedDob, '')
	and isnull(target.ReferredBy, '') = isnull(source.ReferredBy, '')
	and isnull(target.ArchiveDate, getdate()) = isnull(source.ArchiveDate, getdate())
	and isnull(target.AnSex, '') = isnull(source.AnSex, '')
	and isnull(target.PregnancyBookingData, '') = isnull(source.PregnancyBookingData, '')
	and isnull(target.Erdip, '') = isnull(source.Erdip, '')
	and isnull(target.HospReferredTo, '') = isnull(source.HospReferredTo, '')
	and isnull(target.ReferralDate, getdate()) = isnull(source.ReferralDate, getdate())
	and isnull(target.ReferralNumber, '') = isnull(source.ReferralNumber, '')
	and isnull(target.PregnancyLocation, '') = isnull(source.PregnancyLocation, '')
	and isnull(target.ScunEcno, '') = isnull(source.ScunEcno, '')
	and isnull(target.AnWardType, '') = isnull(source.AnWardType, '')
	and isnull(target.ReferredByOther, '') = isnull(source.ReferredByOther, '')
	and isnull(target.ReferredByHosp, '') = isnull(source.ReferredByHosp, '')
	and isnull(target.PregnancyCommunityType, '') = isnull(source.PregnancyCommunityType, '')
	and isnull(target.ReferralGestWks, 0) = isnull(source.ReferralGestWks, 0)
	and isnull(target.ReferralGestDays, 0) = isnull(source.ReferralGestDays, 0)
	and isnull(target.LmpAtReferral, getdate()) = isnull(source.LmpAtReferral, getdate())
	and isnull(target.EddAtReferral, getdate()) = isnull(source.EddAtReferral, getdate())
	and isnull(target.BnScreen1Ind, '') = isnull(source.BnScreen1Ind, '')
	and isnull(target.NumberInfants, '') = isnull(source.NumberInfants, '')
	and isnull(target.OneParent, '') = isnull(source.OneParent, '')
	and isnull(target.LabourOnset, '') = isnull(source.LabourOnset, '')
	and isnull(target.Augmentation, '') = isnull(source.Augmentation, '')
	and isnull(target.OnsetFirstDate, getdate()) = isnull(source.OnsetFirstDate, getdate())
	and isnull(target.OnsetFirstTime, '') = isnull(source.OnsetFirstTime, '')
	and isnull(target.OnsetSecondDate, getdate()) = isnull(source.OnsetSecondDate, getdate())
	and isnull(target.OnsetSecondTime, '') = isnull(source.OnsetSecondTime, '')
	and isnull(target.MethodIndAug, '') = isnull(source.MethodIndAug, '')
	and isnull(target.BnScreen2Ind, '') = isnull(source.BnScreen2Ind, '')
	and isnull(target.Pyrexia, '') = isnull(source.Pyrexia, '')
	and isnull(target.BloodLoss, '') = isnull(source.BloodLoss, '')
	and isnull(target.Perineum, '') = isnull(source.Perineum, '')
	and isnull(target.NonPerinealTears, '') = isnull(source.NonPerinealTears, '')
	and isnull(target.Suture, '') = isnull(source.Suture, '')
	and isnull(target.ManualRemoval, '') = isnull(source.ManualRemoval, '')
	and isnull(target.PlacentaMembranes, '') = isnull(source.PlacentaMembranes, '')
	and isnull(target.Membranes, '') = isnull(source.Membranes, '')
	and isnull(target.LabCompl, '') = isnull(source.LabCompl, '')
	and isnull(target.LabComplText, '') = isnull(source.LabComplText, '')
	and isnull(target.IntendedFeeding, '') = isnull(source.IntendedFeeding, '')
	and isnull(target.LabResearch, '') = isnull(source.LabResearch, '')
	and isnull(target.Support, '') = isnull(source.Support, '')
	and isnull(target.DScreen1Ind, '') = isnull(source.DScreen1Ind, '')
	and isnull(target.Eclampsia, '') = isnull(source.Eclampsia, '')
	and isnull(target.Thromboembolism, '') = isnull(source.Thromboembolism, '')
	and isnull(target.Erpc, '') = isnull(source.Erpc, '')
	and isnull(target.PuerperalPsychosis, '') = isnull(source.PuerperalPsychosis, '')
	and isnull(target.Infection, '') = isnull(source.Infection, '')
	and isnull(target.DischHbTest, '') = isnull(source.DischHbTest, '')
	and isnull(target.DischHaemoglobin, '') = isnull(source.DischHaemoglobin, '')
	and isnull(target.AntiD, '') = isnull(source.AntiD, '')
	and isnull(target.RubellaVac, '') = isnull(source.RubellaVac, '')
	and isnull(target.BloodTrans, '') = isnull(source.BloodTrans, '')
	and isnull(target.Sterilization, '') = isnull(source.Sterilization, '')
	and isnull(target.Contraception, '') = isnull(source.Contraception, '')
	and isnull(target.DMatCompl, '') = isnull(source.DMatCompl, '')
	and isnull(target.DMatComplText, '') = isnull(source.DMatComplText, '')
	and isnull(target.DScreen2Ind, '') = isnull(source.DScreen2Ind, '')
	and isnull(target.Appointment, '') = isnull(source.Appointment, '')
	and isnull(target.ResearchDisch, '') = isnull(source.ResearchDisch, '')
	and isnull(target.AgeAtDel, '') = isnull(source.AgeAtDel, '')
	and isnull(target.LabourDuration1stStage, '') = isnull(source.LabourDuration1stStage, '')
	and isnull(target.LabourDuration2ndStage, '') = isnull(source.LabourDuration2ndStage, '')
	and isnull(target.LabourDurationLabour, '') = isnull(source.LabourDurationLabour, '')
	and isnull(target.PnLenStay, '') = isnull(source.PnLenStay, '')
	and isnull(target.AnComplicationsIcd, '') = isnull(source.AnComplicationsIcd, '')
	and isnull(target.DelComplicationsIcd, '') = isnull(source.DelComplicationsIcd, '')
	and isnull(target.PuerpComplicationsIcd, '') = isnull(source.PuerpComplicationsIcd, '')
	and isnull(target.MatOperationsIcd, '') = isnull(source.MatOperationsIcd, '')
	and isnull(target.TenSmokingStage, '') = isnull(source.TenSmokingStage, '')
	and isnull(target.TenCigsPerDay, 0) = isnull(source.TenCigsPerDay, 0)
	and isnull(target.TenPartnerSmoke, '') = isnull(source.TenPartnerSmoke, '')
	and isnull(target.DischSmokingStage, '') = isnull(source.DischSmokingStage, '')
	and isnull(target.DischCigsPerDay, 0) = isnull(source.DischCigsPerDay, 0)
	and isnull(target.DischPartnerSmoke, '') = isnull(source.DischPartnerSmoke, '')
	and isnull(target.RiskDelivery, '') = isnull(source.RiskDelivery, '')
	and isnull(target.DischMedication, '') = isnull(source.DischMedication, '')
	and isnull(target.DischMedicationText, '') = isnull(source.DischMedicationText, '')
	and isnull(target.DischPartnerCigsPerDay, 0) = isnull(source.DischPartnerCigsPerDay, 0)
	and isnull(target.TenPartnerCigsPerDay, 0) = isnull(source.TenPartnerCigsPerDay, 0)
	and isnull(target.SkinToSkin, '') = isnull(source.SkinToSkin, '')
	and isnull(target.SkinToSkinText, '') = isnull(source.SkinToSkinText, '')
	and isnull(target.AnalMucosa, '') = isnull(source.AnalMucosa, '')
	and isnull(target.TempMaxLabour, 0) = isnull(source.TempMaxLabour, 0)
	and isnull(target.EpisiotomySutured, '') = isnull(source.EpisiotomySutured, '')
	and isnull(target.LabourLocation, '') = isnull(source.LabourLocation, '')
	and isnull(target.BreastFeedInitiated, '') = isnull(source.BreastFeedInitiated, '')
	and isnull(target.Date1stBreastFeed, getdate()) = isnull(source.Date1stBreastFeed, getdate())
	and isnull(target.Time1stBreastFeed, '') = isnull(source.Time1stBreastFeed, '')
	and isnull(target.PnLenStayHours, 0) = isnull(source.PnLenStayHours, 0)
	and isnull(target.PnLenStayMinutes, 0) = isnull(source.PnLenStayMinutes, 0)
	and isnull(target.TransBpSystolic, 0) = isnull(source.TransBpSystolic, 0)
	and isnull(target.TransBpDiastolic, 0) = isnull(source.TransBpDiastolic, 0)
	and isnull(target.TransToCommunityDate, getdate()) = isnull(source.TransToCommunityDate, getdate())
	and isnull(target.TransferringHospNumber, '') = isnull(source.TransferringHospNumber, '')
	and isnull(target.CmwTeam, '') = isnull(source.CmwTeam, '')
	and isnull(target.CmwDefaultMidwife, '') = isnull(source.CmwDefaultMidwife, '')
	and isnull(target.CmwBackupMidwife, '') = isnull(source.CmwBackupMidwife, '')
	and isnull(target.CmwSource, '') = isnull(source.CmwSource, '')
	and isnull(target.CmwHospitalName, '') = isnull(source.CmwHospitalName, '')
	and isnull(target.CmwHospitalChar, '') = isnull(source.CmwHospitalChar, '')
	and isnull(target.CmwHospitalOrgCode, '') = isnull(source.CmwHospitalOrgCode, '')
	and isnull(target.TransComments, '') = isnull(source.TransComments, '')
	and isnull(target.CmwMDischDate, getdate()) = isnull(source.CmwMDischDate, getdate())
	and isnull(target.CmwMDischCondition, '') = isnull(source.CmwMDischCondition, '')
	and isnull(target.CpNormalLabour, '') = isnull(source.CpNormalLabour, '')
	and isnull(target.CpReasonNotUsed, '') = isnull(source.CpReasonNotUsed, '')
	and isnull(target.CpOtherReasonNotUsed, '') = isnull(source.CpOtherReasonNotUsed, '')
	and isnull(target.PnCare, '') = isnull(source.PnCare, '')
	and isnull(target.PnCareHosp, '') = isnull(source.PnCareHosp, '')
	and isnull(target.PnCareOther, '') = isnull(source.PnCareOther, '')
	and isnull(target.NonPerinealSuture, '') = isnull(source.NonPerinealSuture, '')
	and isnull(target.TimeSutureStarted, '') = isnull(source.TimeSutureStarted, '')
	and isnull(target.ReasonSutureDelayed, '') = isnull(source.ReasonSutureDelayed, '')
	and isnull(target.SterilisationText, '') = isnull(source.SterilisationText, '')
	and isnull(target.OneToOneCare, '') = isnull(source.OneToOneCare, '')
	and isnull(target.FgmReversal, '') = isnull(source.FgmReversal, '')
	and isnull(target.FgmReversalStage, '') = isnull(source.FgmReversalStage, '')
	and isnull(target.FgmReferralToCpMw, '') = isnull(source.FgmReferralToCpMw, '')
	and isnull(target.DelGbsCarrier, '') = isnull(source.DelGbsCarrier, '')
	and isnull(target.DischargedToHvDate, getdate()) = isnull(source.DischargedToHvDate, getdate())
	and isnull(target.DischargedToHvTime, '') = isnull(source.DischargedToHvTime, '')
	and isnull(target.PlannedDischargeDate, getdate()) = isnull(source.PlannedDischargeDate, getdate())
	and isnull(target.PlannedDischargeTime, '') = isnull(source.PlannedDischargeTime, '')
	and isnull(target.PnComplications, '') = isnull(source.PnComplications, '')
	and isnull(target.PnComplicationsText, '') = isnull(source.PnComplicationsText, '')
	and isnull(target.Occurrence, 0) = isnull(source.Occurrence, 0)
	and isnull(target.BirthOrder, '') = isnull(source.BirthOrder, '')
	and isnull(target.InfantNo, '') = isnull(source.InfantNo, '')
	and isnull(target.BnScreen3Ind, '') = isnull(source.BnScreen3Ind, '')
	and isnull(target.RuptureMethod, '') = isnull(source.RuptureMethod, '')
	and isnull(target.RuptureDate, getdate()) = isnull(source.RuptureDate, getdate())
	and isnull(target.RuptureTime, '') = isnull(source.RuptureTime, '')
	and isnull(target.CordProlapse, '') = isnull(source.CordProlapse, '')
	and isnull(target.PresentationLabour, '') = isnull(source.PresentationLabour, '')
	and isnull(target.ElectronicFh, '') = isnull(source.ElectronicFh, '')
	and isnull(target.FetalPhTest, '') = isnull(source.FetalPhTest, '')
	and isnull(target.FetalPh, '') = isnull(source.FetalPh, '')
	and isnull(target.Meconium, '') = isnull(source.Meconium, '')
	and isnull(target.CordBloodGas, '') = isnull(source.CordBloodGas, '')
	and isnull(target.TimeOfBirth, '') = isnull(source.TimeOfBirth, '')
	and isnull(target.BirthWeight, '') = isnull(source.BirthWeight, '')
	and isnull(target.BirthWeightCentile, '') = isnull(source.BirthWeightCentile, '')
	and isnull(target.Gestation, '') = isnull(source.Gestation, '')
	and isnull(target.GestationByDate, '') = isnull(source.GestationByDate, '')
	and isnull(target.GestationByUs, '') = isnull(source.GestationByUs, '')
	and isnull(target.GestUsCertain, '') = isnull(source.GestUsCertain, '')
	and isnull(target.CongenitalAbnorm, '') = isnull(source.CongenitalAbnorm, '')
	and isnull(target.CongenitalDesc, '') = isnull(source.CongenitalDesc, '')
	and isnull(target.BnScreen4Ind, '') = isnull(source.BnScreen4Ind, '')
	and isnull(target.PresentationDeliv, '') = isnull(source.PresentationDeliv, '')
	and isnull(target.BreechDiagnosis, '') = isnull(source.BreechDiagnosis, '')
	and isnull(target.BirthingPool, '') = isnull(source.BirthingPool, '')
	and isnull(target.MethodDelivery, '') = isnull(source.MethodDelivery, '')
	and isnull(target.IndicCaesarian, '') = isnull(source.IndicCaesarian, '')
	and isnull(target.Outcome, '') = isnull(source.Outcome, '')
	and isnull(target.PlaceDelivery, '') = isnull(source.PlaceDelivery, '')
	and isnull(target.ReasonForChange, '') = isnull(source.ReasonForChange, '')
	and isnull(target.ConductDel, '') = isnull(source.ConductDel, '')
	and isnull(target.DeliveryTeam, '') = isnull(source.DeliveryTeam, '')
	and isnull(target.DeliveryMidwife, '') = isnull(source.DeliveryMidwife, '')
	and isnull(target.SpecPlaceDelivery, '') = isnull(source.SpecPlaceDelivery, '')
	and isnull(target.BnScreen5Ind, '') = isnull(source.BnScreen5Ind, '')
	and isnull(target.ApgarOne, '') = isnull(source.ApgarOne, '')
	and isnull(target.ApgarFive, '') = isnull(source.ApgarFive, '')
	and isnull(target.ApgarTen, '') = isnull(source.ApgarTen, '')
	and isnull(target.OnsetRespiration, '') = isnull(source.OnsetRespiration, '')
	and isnull(target.ResusPressure, '') = isnull(source.ResusPressure, '')
	and isnull(target.ResusDrugs, '') = isnull(source.ResusDrugs, '')
	and isnull(target.ChildDischAddress, '') = isnull(source.ChildDischAddress, '')
	and isnull(target.ConsPaed, '') = isnull(source.ConsPaed, '')
	and isnull(target.InfCompl, '') = isnull(source.InfCompl, '')
	and isnull(target.InfComplText, '') = isnull(source.InfComplText, '')
	and isnull(target.InfResearch, '') = isnull(source.InfResearch, '')
	and isnull(target.MidwifeDelName, '') = isnull(source.MidwifeDelName, '')
	and isnull(target.MidwifeDelInitials, '') = isnull(source.MidwifeDelInitials, '')
	and isnull(target.MidwifeDelStatus, '') = isnull(source.MidwifeDelStatus, '')
	and isnull(target.ChildNumber, '') = isnull(source.ChildNumber, '')
	and isnull(target.IndexPage, '') = isnull(source.IndexPage, '')
	and isnull(target.HealthDistrict, '') = isnull(source.HealthDistrict, '')
	and isnull(target.TransferType, '') = isnull(source.TransferType, '')
	and isnull(target.HealthVisitor, '') = isnull(source.HealthVisitor, '')
	and isnull(target.Registration, '') = isnull(source.Registration, '')
	and isnull(target.BirthPlace, '') = isnull(source.BirthPlace, '')
	and isnull(target.BirthHA, '') = isnull(source.BirthHA, '')
	and isnull(target.OPCSBirthArea, '') = isnull(source.OPCSBirthArea, '')
	and isnull(target.OPCSResidArea, '') = isnull(source.OPCSResidArea, '')
	and isnull(target.BornAt, '') = isnull(source.BornAt, '')
	and isnull(target.LAWard, '') = isnull(source.LAWard, '')
	and isnull(target.AssessGest, '') = isnull(source.AssessGest, '')
	and isnull(target.AgreedGestFrom, '') = isnull(source.AgreedGestFrom, '')
	and isnull(target.AgreedGestTo, '') = isnull(source.AgreedGestTo, '')
	and isnull(target.HeadCircum, '') = isnull(source.HeadCircum, '')
	and isnull(target.HeadCircumCentile, '') = isnull(source.HeadCircumCentile, '')
	and isnull(target.Length, '') = isnull(source.Length, '')
	and isnull(target.LengthCentile, '') = isnull(source.LengthCentile, '')
	and isnull(target.DScreenInd, '') = isnull(source.DScreenInd, '')
	and isnull(target.HIP, '') = isnull(source.HIP, '')
	and isnull(target.Convulsions, '') = isnull(source.Convulsions, '')
	and isnull(target.OtherAbnorm, '') = isnull(source.OtherAbnorm, '')
	and isnull(target.Jaundice, '') = isnull(source.Jaundice, '')
	and isnull(target.Bilirubin, '') = isnull(source.Bilirubin, '')
	and isnull(target.DInfCompl, '') = isnull(source.DInfCompl, '')
	and isnull(target.DInfComplText, '') = isnull(source.DInfComplText, '')
	and isnull(target.CongAbsDisch, '') = isnull(source.CongAbsDisch, '')
	and isnull(target.DScreen3AInd, '') = isnull(source.DScreen3AInd, '')
	and isnull(target.SCBU, '') = isnull(source.SCBU, '')
	and isnull(target.TransCare, '') = isnull(source.TransCare, '')
	and isnull(target.Cord, '') = isnull(source.Cord, '')
	and isnull(target.Guthrie, '') = isnull(source.Guthrie, '')
	and isnull(target.GuthrieDate, getdate()) = isnull(source.GuthrieDate, getdate())
	and isnull(target.Haemoglobinopathy, '') = isnull(source.Haemoglobinopathy, '')
	and isnull(target.HaemoglobinopathyDate, getdate()) = isnull(source.HaemoglobinopathyDate, getdate())
	and isnull(target.Vitamink, '') = isnull(source.Vitamink, '')
	and isnull(target.VitaminkDate, getdate()) = isnull(source.VitaminkDate, getdate())
	and isnull(target.VitaminkText, '') = isnull(source.VitaminkText, '')
	and isnull(target.InfHbTest, '') = isnull(source.InfHbTest, '')
	and isnull(target.InfHaemaglobin, '') = isnull(source.InfHaemaglobin, '')
	and isnull(target.InfBloodGroup, '') = isnull(source.InfBloodGroup, '')
	and isnull(target.InfCoombs, '') = isnull(source.InfCoombs, '')
	and isnull(target.DischargeExamination, '') = isnull(source.DischargeExamination, '')
	and isnull(target.Condition, '') = isnull(source.Condition, '')
	and isnull(target.DischExamComments, '') = isnull(source.DischExamComments, '')
	and isnull(target.HighestLoc, '') = isnull(source.HighestLoc, '')
	and isnull(target.Condition28Day, '') = isnull(source.Condition28Day, '')
	and isnull(target.Resarch28Day, '') = isnull(source.Resarch28Day, '')
	and isnull(target.Comments, '') = isnull(source.Comments, '')
	and isnull(target.Feeding28Day, '') = isnull(source.Feeding28Day, '')
	and isnull(target.NnCompl, '') = isnull(source.NnCompl, '')
	and isnull(target.NnComplText, '') = isnull(source.NnComplText, '')
	and isnull(target.DatePlacenta, getdate()) = isnull(source.DatePlacenta, getdate())
	and isnull(target.TimePlacenta, '') = isnull(source.TimePlacenta, '')
	and isnull(target.BirthDurationLabour, '') = isnull(source.BirthDurationLabour, '')
	and isnull(target.BirthDuration2ndStage, '') = isnull(source.BirthDuration2ndStage, '')
	and isnull(target.BirthDuration3rdStage, '') = isnull(source.BirthDuration3rdStage, '')
	and isnull(target.HearingTest, '') = isnull(source.HearingTest, '')
	and isnull(target.dateHearing, getdate()) = isnull(source.dateHearing, getdate())
	and isnull(target.HearingResult, '') = isnull(source.HearingResult, '')
	and isnull(target.ActualHospital, '') = isnull(source.ActualHospital, '')
	and isnull(target.BirthDuration1stStage, '') = isnull(source.BirthDuration1stStage, '')
	and isnull(target.Tab0, '') = isnull(source.Tab0, '')
	and isnull(target.Tab1, '') = isnull(source.Tab1, '')
	and isnull(target.Tab2, '') = isnull(source.Tab2, '')
	and isnull(target.Tab3, '') = isnull(source.Tab3, '')
	and isnull(target.tab4, '') = isnull(source.tab4, '')
	and isnull(target.DelHospCode, '') = isnull(source.DelHospCode, '')
	and isnull(target.DelHospName, '') = isnull(source.DelHospName, '')
	and isnull(target.ChildHealthCode, '') = isnull(source.ChildHealthCode, '')
	and isnull(target.CordVenousPH, 0) = isnull(source.CordVenousPH, 0)
	and isnull(target.CordVenBaseDeficit, 0) = isnull(source.CordVenBaseDeficit, 0)
	and isnull(target.CordArterialPH, 0) = isnull(source.CordArterialPH, 0)
	and isnull(target.CordArtBaseDeficit, 0) = isnull(source.CordArtBaseDeficit, 0)
	and isnull(target.ThirdStageManagement, '') = isnull(source.ThirdStageManagement, '')
	and isnull(target.Inf1Complications, '') = isnull(source.Inf1Complications, '')
	and isnull(target.Inf2Complications, '') = isnull(source.Inf2Complications, '')
	and isnull(target.CreateBaby, '') = isnull(source.CreateBaby, '')
	and isnull(target.BnBoxCode, '') = isnull(source.BnBoxCode, '')
	and isnull(target.EcvLabour, '') = isnull(source.EcvLabour, '')
	and isnull(target.HighestLoc2002, '') = isnull(source.HighestLoc2002, '')
	and isnull(target.BirthLocation, '') = isnull(source.BirthLocation, '')
	and isnull(target.DateCsDecided, getdate()) = isnull(source.DateCsDecided, getdate())
	and isnull(target.TimeCsDecided, '') = isnull(source.TimeCsDecided, '')
	and isnull(target.TimeToCaesarian, '') = isnull(source.TimeToCaesarian, '')
	and isnull(target.MidwifeKnown, '') = isnull(source.MidwifeKnown, '')
	and isnull(target.GestationDays, 0) = isnull(source.GestationDays, 0)
	and isnull(target.GestByDateDays, 0) = isnull(source.GestByDateDays, 0)
	and isnull(target.GestByUsDays, 0) = isnull(source.GestByUsDays, 0)
	and isnull(target.AssessGestDays, 0) = isnull(source.AssessGestDays, 0)
	and isnull(target.AgreedGestFromDays, 0) = isnull(source.AgreedGestFromDays, 0)
	and isnull(target.FollowUpDay, 0) = isnull(source.FollowUpDay, 0)
	and isnull(target.DilationAtCs, 0) = isnull(source.DilationAtCs, 0)
	and isnull(target.TransWeight, 0) = isnull(source.TransWeight, 0)
	and isnull(target.TransFeeding, '') = isnull(source.TransFeeding, '')
	and isnull(target.CmwIDischWeight, 0) = isnull(source.CmwIDischWeight, 0)
	and isnull(target.CmwIDischCondition, '') = isnull(source.CmwIDischCondition, '')
	and isnull(target.PnSex, '') = isnull(source.PnSex, '')
	and isnull(target.PnDateOfBirth, getdate()) = isnull(source.PnDateOfBirth, getdate())
	and isnull(target.PnAgeAtDischarge, 0) = isnull(source.PnAgeAtDischarge, 0)
	and isnull(target.BirthAnDate, getdate()) = isnull(source.BirthAnDate, getdate())
	and isnull(target.IndicInstrumental, '') = isnull(source.IndicInstrumental, '')
	and isnull(target.ShoulderDystocia, '') = isnull(source.ShoulderDystocia, '')
	and isnull(target.ShoulderInjury, '') = isnull(source.ShoulderInjury, '')
	and isnull(target.ShoulderInjuryText, '') = isnull(source.ShoulderInjuryText, '')
	and isnull(target.CsReasonForDelay, '') = isnull(source.CsReasonForDelay, '')
	and isnull(target.Palpable5ths, '') = isnull(source.Palpable5ths, '')
	and isnull(target.FetalPosition, '') = isnull(source.FetalPosition, '')
	and isnull(target.Station, '') = isnull(source.Station, '')
	and isnull(target.Moulding, '') = isnull(source.Moulding, '')
	and isnull(target.Caput, '') = isnull(source.Caput, '')
	and isnull(target.LiquorColour, '') = isnull(source.LiquorColour, '')
	and isnull(target.DeliveryOfBaby, '') = isnull(source.DeliveryOfBaby, '')
	and isnull(target.CordEntanglement, '') = isnull(source.CordEntanglement, '')
	and isnull(target.Position, '') = isnull(source.Position, '')
	and isnull(target.NewbornExamination, '') = isnull(source.NewbornExamination, '')
	and isnull(target.NewbornExaminationDate, getdate()) = isnull(source.NewbornExaminationDate, getdate())
	and isnull(target.NewbornExaminationTime, '') = isnull(source.NewbornExaminationTime, '')
	and isnull(target.ExaminationCompletedBy, '') = isnull(source.ExaminationCompletedBy, '')
	and isnull(target.ExaminationMidwifePin, '') = isnull(source.ExaminationMidwifePin, '')
	and isnull(target.ExaminationText, '') = isnull(source.ExaminationText, '')
	and isnull(target.Heart, '') = isnull(source.Heart, '')
	and isnull(target.Eyes, '') = isnull(source.Eyes, '')
	and isnull(target.Testes, '') = isnull(source.Testes, '')
	and isnull(target.BirthMarksBruises, '') = isnull(source.BirthMarksBruises, '')
	and isnull(target.BirthMarksBruisesText, '') = isnull(source.BirthMarksBruisesText, '')
	and isnull(target.NewbornReferral, '') = isnull(source.NewbornReferral, '')
	and isnull(target.CongenitalList, '') = isnull(source.CongenitalList, '')
	and isnull(target.RedBookCompleted, '') = isnull(source.RedBookCompleted, '')
	and isnull(target.InfantNhsNumber, '') = isnull(source.InfantNhsNumber, '')
	and isnull(target.InfantNhsNoValid, '') = isnull(source.InfantNhsNoValid, '')
	and isnull(target.InfantSex, '') = isnull(source.InfantSex, '')
	and isnull(target.InfantMaidenName, '') = isnull(source.InfantMaidenName, '')
	and isnull(target.InfantCurrentSurname, '') = isnull(source.InfantCurrentSurname, '')
	and isnull(target.InfantForenames, '') = isnull(source.InfantForenames, '')
	and isnull(target.InfantDateOfBirth, getdate()) = isnull(source.InfantDateOfBirth, getdate())
	and isnull(target.InfantPlaceOfBirth, '') = isnull(source.InfantPlaceOfBirth, '')
	and isnull(target.InfantEthnicGroupPrefix, '') = isnull(source.InfantEthnicGroupPrefix, '')
	and isnull(target.InfantEthnicGroupBody, '') = isnull(source.InfantEthnicGroupBody, '')
	and isnull(target.InfantEthnicGroupOpcs, '') = isnull(source.InfantEthnicGroupOpcs, '')
	and isnull(target.InfantEthnicGroupPas, '') = isnull(source.InfantEthnicGroupPas, '')
	and isnull(target.InfantGpSurname, '') = isnull(source.InfantGpSurname, '')
	and isnull(target.InfantGpSequence, 0) = isnull(source.InfantGpSequence, 0)
	and isnull(target.InfantTraceStatus, '') = isnull(source.InfantTraceStatus, '')
	and isnull(target.InfantLocation, '') = isnull(source.InfantLocation, '')
	)

then
update
	set
		 target.MotherNo = source.MotherNo
		,target.MotherNhsNumber = source.MotherNhsNumber
		,target.MotherNhsNoValid = source.MotherNhsNoValid
		,target.CurrentOccurrence = source.CurrentOccurrence
		,target.MotherMaidenName = source.MotherMaidenName
		,target.MotherCurrentSurname = source.MotherCurrentSurname
		,target.MotherFForenames = source.MotherFForenames
		,target.MotherTitle = source.MotherTitle
		,target.MotherDateOfBirth = source.MotherDateOfBirth
		,target.MotherPlaceOfBirth = source.MotherPlaceOfBirth
		,target.UkEntryDate = source.UkEntryDate
		,target.MotherDateApprox = source.MotherDateApprox
		,target.MotherEthnicGroupPrefix = source.MotherEthnicGroupPrefix
		,target.MotherEthnicGroupBody = source.MotherEthnicGroupBody
		,target.MotherEthnicGroupOpcs = source.MotherEthnicGroupOpcs
		,target.MotherEthnicGroupPas = source.MotherEthnicGroupPas
		,target.MotherGpSurname = source.MotherGpSurname
		,target.MotherGpSequence = source.MotherGpSequence
		,target.MotherTraceStatus = source.MotherTraceStatus
		,target.MotherLocation = source.MotherLocation
		,target.MotherDateOfDeath = source.MotherDateOfDeath
		,target.MotherTimeOfDeath = source.MotherTimeOfDeath
		,target.MotherDeathIndicator = source.MotherDeathIndicator
		,target.PregnancyPortable = source.PregnancyPortable
		,target.PregnancyDiskCreateDate = source.PregnancyDiskCreateDate
		,target.PregnancyStatus = source.PregnancyStatus
		,target.PregnancyType = source.PregnancyType
		,target.PregnancyUnbooked = source.PregnancyUnbooked
		,target.PregnancyCardNumber = source.PregnancyCardNumber
		,target.PregnancyCreateDate = source.PregnancyCreateDate
		,target.PregnancyCreateTime = source.PregnancyCreateTime
		,target.MotherReligion = source.MotherReligion
		,target.OtherReligion = source.OtherReligion
		,target.Marital = source.Marital
		,target.MSurname = source.MSurname
		,target.PrefName = source.PrefName
		,target.MotherOccupation = source.MotherOccupation
		,target.MainIncome = source.MainIncome
		,target.PrevEmployment = source.PrevEmployment
		,target.MotherTelephoneWork = source.MotherTelephoneWork
		,target.OneParentFam = source.OneParentFam
		,target.SocialClass = source.SocialClass
		,target.BkScreen1Ind = source.BkScreen1Ind
		,target.BkScreen1aInd = source.BkScreen1aInd
		,target.BkScreen2Ind = source.BkScreen2Ind
		,target.InvScreen1Ind = source.InvScreen1Ind
		,target.InvScreen2Ind = source.InvScreen2Ind
		,target.AnScreen1Ind = source.AnScreen1Ind
		,target.AnScreen2Ind = source.AnScreen2Ind
		,target.AnReason = source.AnReason
		,target.PregnancyAnDate = source.PregnancyAnDate
		,target.AnOperation = source.AnOperation
		,target.PregnancyGpSurname = source.PregnancyGpSurname
		,target.PregnancyGpSequence = source.PregnancyGpSequence
		,target.TypeOfPatient = source.TypeOfPatient
		,target.OverseasVisitor = source.OverseasVisitor
		,target.PregnancyConCode = source.PregnancyConCode
		,target.PregnancyBookingMidwife = source.PregnancyBookingMidwife
		,target.PregnancyMidwifeTeam = source.PregnancyMidwifeTeam
		,target.ReasonCompletion = source.ReasonCompletion
		,target.SpecOtherPlace = source.SpecOtherPlace
		,target.ConfirmedDob = source.ConfirmedDob
		,target.ReferredBy = source.ReferredBy
		,target.ArchiveDate = source.ArchiveDate
		,target.AnSex = source.AnSex
		,target.PregnancyBookingData = source.PregnancyBookingData
		,target.Erdip = source.Erdip
		,target.HospReferredTo = source.HospReferredTo
		,target.ReferralDate = source.ReferralDate
		,target.ReferralNumber = source.ReferralNumber
		,target.PregnancyLocation = source.PregnancyLocation
		,target.ScunEcno = source.ScunEcno
		,target.AnWardType = source.AnWardType
		,target.ReferredByOther = source.ReferredByOther
		,target.ReferredByHosp = source.ReferredByHosp
		,target.PregnancyCommunityType = source.PregnancyCommunityType
		,target.ReferralGestWks = source.ReferralGestWks
		,target.ReferralGestDays = source.ReferralGestDays
		,target.LmpAtReferral = source.LmpAtReferral
		,target.EddAtReferral = source.EddAtReferral
		,target.BnScreen1Ind = source.BnScreen1Ind
		,target.NumberInfants = source.NumberInfants
		,target.OneParent = source.OneParent
		,target.LabourOnset = source.LabourOnset
		,target.Augmentation = source.Augmentation
		,target.OnsetFirstDate = source.OnsetFirstDate
		,target.OnsetFirstTime = source.OnsetFirstTime
		,target.OnsetSecondDate = source.OnsetSecondDate
		,target.OnsetSecondTime = source.OnsetSecondTime
		,target.MethodIndAug = source.MethodIndAug
		,target.BnScreen2Ind = source.BnScreen2Ind
		,target.Pyrexia = source.Pyrexia
		,target.BloodLoss = source.BloodLoss
		,target.Perineum = source.Perineum
		,target.NonPerinealTears = source.NonPerinealTears
		,target.Suture = source.Suture
		,target.ManualRemoval = source.ManualRemoval
		,target.PlacentaMembranes = source.PlacentaMembranes
		,target.Membranes = source.Membranes
		,target.LabCompl = source.LabCompl
		,target.LabComplText = source.LabComplText
		,target.IntendedFeeding = source.IntendedFeeding
		,target.LabResearch = source.LabResearch
		,target.Support = source.Support
		,target.DScreen1Ind = source.DScreen1Ind
		,target.Eclampsia = source.Eclampsia
		,target.Thromboembolism = source.Thromboembolism
		,target.Erpc = source.Erpc
		,target.PuerperalPsychosis = source.PuerperalPsychosis
		,target.Infection = source.Infection
		,target.DischHbTest = source.DischHbTest
		,target.DischHaemoglobin = source.DischHaemoglobin
		,target.AntiD = source.AntiD
		,target.RubellaVac = source.RubellaVac
		,target.BloodTrans = source.BloodTrans
		,target.Sterilization = source.Sterilization
		,target.Contraception = source.Contraception
		,target.DMatCompl = source.DMatCompl
		,target.DMatComplText = source.DMatComplText
		,target.DScreen2Ind = source.DScreen2Ind
		,target.Appointment = source.Appointment
		,target.ResearchDisch = source.ResearchDisch
		,target.AgeAtDel = source.AgeAtDel
		,target.LabourDuration1stStage = source.LabourDuration1stStage
		,target.LabourDuration2ndStage = source.LabourDuration2ndStage
		,target.LabourDurationLabour = source.LabourDurationLabour
		,target.PnLenStay = source.PnLenStay
		,target.AnComplicationsIcd = source.AnComplicationsIcd
		,target.DelComplicationsIcd = source.DelComplicationsIcd
		,target.PuerpComplicationsIcd = source.PuerpComplicationsIcd
		,target.MatOperationsIcd = source.MatOperationsIcd
		,target.TenSmokingStage = source.TenSmokingStage
		,target.TenCigsPerDay = source.TenCigsPerDay
		,target.TenPartnerSmoke = source.TenPartnerSmoke
		,target.DischSmokingStage = source.DischSmokingStage
		,target.DischCigsPerDay = source.DischCigsPerDay
		,target.DischPartnerSmoke = source.DischPartnerSmoke
		,target.RiskDelivery = source.RiskDelivery
		,target.DischMedication = source.DischMedication
		,target.DischMedicationText = source.DischMedicationText
		,target.DischPartnerCigsPerDay = source.DischPartnerCigsPerDay
		,target.TenPartnerCigsPerDay = source.TenPartnerCigsPerDay
		,target.SkinToSkin = source.SkinToSkin
		,target.SkinToSkinText = source.SkinToSkinText
		,target.AnalMucosa = source.AnalMucosa
		,target.TempMaxLabour = source.TempMaxLabour
		,target.EpisiotomySutured = source.EpisiotomySutured
		,target.LabourLocation = source.LabourLocation
		,target.BreastFeedInitiated = source.BreastFeedInitiated
		,target.Date1stBreastFeed = source.Date1stBreastFeed
		,target.Time1stBreastFeed = source.Time1stBreastFeed
		,target.PnLenStayHours = source.PnLenStayHours
		,target.PnLenStayMinutes = source.PnLenStayMinutes
		,target.TransBpSystolic = source.TransBpSystolic
		,target.TransBpDiastolic = source.TransBpDiastolic
		,target.TransToCommunityDate = source.TransToCommunityDate
		,target.TransferringHospNumber = source.TransferringHospNumber
		,target.CmwTeam = source.CmwTeam
		,target.CmwDefaultMidwife = source.CmwDefaultMidwife
		,target.CmwBackupMidwife = source.CmwBackupMidwife
		,target.CmwSource = source.CmwSource
		,target.CmwHospitalName = source.CmwHospitalName
		,target.CmwHospitalChar = source.CmwHospitalChar
		,target.CmwHospitalOrgCode = source.CmwHospitalOrgCode
		,target.TransComments = source.TransComments
		,target.CmwMDischDate = source.CmwMDischDate
		,target.CmwMDischCondition = source.CmwMDischCondition
		,target.CpNormalLabour = source.CpNormalLabour
		,target.CpReasonNotUsed = source.CpReasonNotUsed
		,target.CpOtherReasonNotUsed = source.CpOtherReasonNotUsed
		,target.PnCare = source.PnCare
		,target.PnCareHosp = source.PnCareHosp
		,target.PnCareOther = source.PnCareOther
		,target.NonPerinealSuture = source.NonPerinealSuture
		,target.TimeSutureStarted = source.TimeSutureStarted
		,target.ReasonSutureDelayed = source.ReasonSutureDelayed
		,target.SterilisationText = source.SterilisationText
		,target.OneToOneCare = source.OneToOneCare
		,target.FgmReversal = source.FgmReversal
		,target.FgmReversalStage = source.FgmReversalStage
		,target.FgmReferralToCpMw = source.FgmReferralToCpMw
		,target.DelGbsCarrier = source.DelGbsCarrier
		,target.DischargedToHvDate = source.DischargedToHvDate
		,target.DischargedToHvTime = source.DischargedToHvTime
		,target.PlannedDischargeDate = source.PlannedDischargeDate
		,target.PlannedDischargeTime = source.PlannedDischargeTime
		,target.PnComplications = source.PnComplications
		,target.PnComplicationsText = source.PnComplicationsText
		,target.Occurrence = source.Occurrence
		,target.BirthOrder = source.BirthOrder
		,target.InfantNo = source.InfantNo
		,target.BnScreen3Ind = source.BnScreen3Ind
		,target.RuptureMethod = source.RuptureMethod
		,target.RuptureDate = source.RuptureDate
		,target.RuptureTime = source.RuptureTime
		,target.CordProlapse = source.CordProlapse
		,target.PresentationLabour = source.PresentationLabour
		,target.ElectronicFh = source.ElectronicFh
		,target.FetalPhTest = source.FetalPhTest
		,target.FetalPh = source.FetalPh
		,target.Meconium = source.Meconium
		,target.CordBloodGas = source.CordBloodGas
		,target.TimeOfBirth = source.TimeOfBirth
		,target.BirthWeight = source.BirthWeight
		,target.BirthWeightCentile = source.BirthWeightCentile
		,target.Gestation = source.Gestation
		,target.GestationByDate = source.GestationByDate
		,target.GestationByUs = source.GestationByUs
		,target.GestUsCertain = source.GestUsCertain
		,target.CongenitalAbnorm = source.CongenitalAbnorm
		,target.CongenitalDesc = source.CongenitalDesc
		,target.BnScreen4Ind = source.BnScreen4Ind
		,target.PresentationDeliv = source.PresentationDeliv
		,target.BreechDiagnosis = source.BreechDiagnosis
		,target.BirthingPool = source.BirthingPool
		,target.MethodDelivery = source.MethodDelivery
		,target.IndicCaesarian = source.IndicCaesarian
		,target.Outcome = source.Outcome
		,target.PlaceDelivery = source.PlaceDelivery
		,target.ReasonForChange = source.ReasonForChange
		,target.ConductDel = source.ConductDel
		,target.DeliveryTeam = source.DeliveryTeam
		,target.DeliveryMidwife = source.DeliveryMidwife
		,target.SpecPlaceDelivery = source.SpecPlaceDelivery
		,target.BnScreen5Ind = source.BnScreen5Ind
		,target.ApgarOne = source.ApgarOne
		,target.ApgarFive = source.ApgarFive
		,target.ApgarTen = source.ApgarTen
		,target.OnsetRespiration = source.OnsetRespiration
		,target.ResusPressure = source.ResusPressure
		,target.ResusDrugs = source.ResusDrugs
		,target.ChildDischAddress = source.ChildDischAddress
		,target.ConsPaed = source.ConsPaed
		,target.InfCompl = source.InfCompl
		,target.InfComplText = source.InfComplText
		,target.InfResearch = source.InfResearch
		,target.MidwifeDelName = source.MidwifeDelName
		,target.MidwifeDelInitials = source.MidwifeDelInitials
		,target.MidwifeDelStatus = source.MidwifeDelStatus
		,target.ChildNumber = source.ChildNumber
		,target.IndexPage = source.IndexPage
		,target.HealthDistrict = source.HealthDistrict
		,target.TransferType = source.TransferType
		,target.HealthVisitor = source.HealthVisitor
		,target.Registration = source.Registration
		,target.BirthPlace = source.BirthPlace
		,target.BirthHA = source.BirthHA
		,target.OPCSBirthArea = source.OPCSBirthArea
		,target.OPCSResidArea = source.OPCSResidArea
		,target.BornAt = source.BornAt
		,target.LAWard = source.LAWard
		,target.AssessGest = source.AssessGest
		,target.AgreedGestFrom = source.AgreedGestFrom
		,target.AgreedGestTo = source.AgreedGestTo
		,target.HeadCircum = source.HeadCircum
		,target.HeadCircumCentile = source.HeadCircumCentile
		,target.Length = source.Length
		,target.LengthCentile = source.LengthCentile
		,target.DScreenInd = source.DScreenInd
		,target.HIP = source.HIP
		,target.Convulsions = source.Convulsions
		,target.OtherAbnorm = source.OtherAbnorm
		,target.Jaundice = source.Jaundice
		,target.Bilirubin = source.Bilirubin
		,target.DInfCompl = source.DInfCompl
		,target.DInfComplText = source.DInfComplText
		,target.CongAbsDisch = source.CongAbsDisch
		,target.DScreen3AInd = source.DScreen3AInd
		,target.SCBU = source.SCBU
		,target.TransCare = source.TransCare
		,target.Cord = source.Cord
		,target.Guthrie = source.Guthrie
		,target.GuthrieDate = source.GuthrieDate
		,target.Haemoglobinopathy = source.Haemoglobinopathy
		,target.HaemoglobinopathyDate = source.HaemoglobinopathyDate
		,target.Vitamink = source.Vitamink
		,target.VitaminkDate = source.VitaminkDate
		,target.VitaminkText = source.VitaminkText
		,target.InfHbTest = source.InfHbTest
		,target.InfHaemaglobin = source.InfHaemaglobin
		,target.InfBloodGroup = source.InfBloodGroup
		,target.InfCoombs = source.InfCoombs
		,target.DischargeExamination = source.DischargeExamination
		,target.Condition = source.Condition
		,target.DischExamComments = source.DischExamComments
		,target.HighestLoc = source.HighestLoc
		,target.Condition28Day = source.Condition28Day
		,target.Resarch28Day = source.Resarch28Day
		,target.Comments = source.Comments
		,target.Feeding28Day = source.Feeding28Day
		,target.NnCompl = source.NnCompl
		,target.NnComplText = source.NnComplText
		,target.DatePlacenta = source.DatePlacenta
		,target.TimePlacenta = source.TimePlacenta
		,target.BirthDurationLabour = source.BirthDurationLabour
		,target.BirthDuration2ndStage = source.BirthDuration2ndStage
		,target.BirthDuration3rdStage = source.BirthDuration3rdStage
		,target.HearingTest = source.HearingTest
		,target.dateHearing = source.dateHearing
		,target.HearingResult = source.HearingResult
		,target.ActualHospital = source.ActualHospital
		,target.BirthDuration1stStage = source.BirthDuration1stStage
		,target.Tab0 = source.Tab0
		,target.Tab1 = source.Tab1
		,target.Tab2 = source.Tab2
		,target.Tab3 = source.Tab3
		,target.tab4 = source.tab4
		,target.DelHospCode = source.DelHospCode
		,target.DelHospName = source.DelHospName
		,target.ChildHealthCode = source.ChildHealthCode
		,target.CordVenousPH = source.CordVenousPH
		,target.CordVenBaseDeficit = source.CordVenBaseDeficit
		,target.CordArterialPH = source.CordArterialPH
		,target.CordArtBaseDeficit = source.CordArtBaseDeficit
		,target.ThirdStageManagement = source.ThirdStageManagement
		,target.Inf1Complications = source.Inf1Complications
		,target.Inf2Complications = source.Inf2Complications
		,target.CreateBaby = source.CreateBaby
		,target.BnBoxCode = source.BnBoxCode
		,target.EcvLabour = source.EcvLabour
		,target.HighestLoc2002 = source.HighestLoc2002
		,target.BirthLocation = source.BirthLocation
		,target.DateCsDecided = source.DateCsDecided
		,target.TimeCsDecided = source.TimeCsDecided
		,target.TimeToCaesarian = source.TimeToCaesarian
		,target.MidwifeKnown = source.MidwifeKnown
		,target.GestationDays = source.GestationDays
		,target.GestByDateDays = source.GestByDateDays
		,target.GestByUsDays = source.GestByUsDays
		,target.AssessGestDays = source.AssessGestDays
		,target.AgreedGestFromDays = source.AgreedGestFromDays
		,target.FollowUpDay = source.FollowUpDay
		,target.DilationAtCs = source.DilationAtCs
		,target.TransWeight = source.TransWeight
		,target.TransFeeding = source.TransFeeding
		,target.CmwIDischWeight = source.CmwIDischWeight
		,target.CmwIDischCondition = source.CmwIDischCondition
		,target.PnSex = source.PnSex
		,target.PnDateOfBirth = source.PnDateOfBirth
		,target.PnAgeAtDischarge = source.PnAgeAtDischarge
		,target.BirthAnDate = source.BirthAnDate
		,target.IndicInstrumental = source.IndicInstrumental
		,target.ShoulderDystocia = source.ShoulderDystocia
		,target.ShoulderInjury = source.ShoulderInjury
		,target.ShoulderInjuryText = source.ShoulderInjuryText
		,target.CsReasonForDelay = source.CsReasonForDelay
		,target.Palpable5ths = source.Palpable5ths
		,target.FetalPosition = source.FetalPosition
		,target.Station = source.Station
		,target.Moulding = source.Moulding
		,target.Caput = source.Caput
		,target.LiquorColour = source.LiquorColour
		,target.DeliveryOfBaby = source.DeliveryOfBaby
		,target.CordEntanglement = source.CordEntanglement
		,target.Position = source.Position
		,target.NewbornExamination = source.NewbornExamination
		,target.NewbornExaminationDate = source.NewbornExaminationDate
		,target.NewbornExaminationTime = source.NewbornExaminationTime
		,target.ExaminationCompletedBy = source.ExaminationCompletedBy
		,target.ExaminationMidwifePin = source.ExaminationMidwifePin
		,target.ExaminationText = source.ExaminationText
		,target.Heart = source.Heart
		,target.Eyes = source.Eyes
		,target.Testes = source.Testes
		,target.BirthMarksBruises = source.BirthMarksBruises
		,target.BirthMarksBruisesText = source.BirthMarksBruisesText
		,target.NewbornReferral = source.NewbornReferral
		,target.CongenitalList = source.CongenitalList
		,target.RedBookCompleted = source.RedBookCompleted
		,target.InfantNhsNumber = source.InfantNhsNumber
		,target.InfantNhsNoValid = source.InfantNhsNoValid
		,target.InfantSex = source.InfantSex
		,target.InfantMaidenName = source.InfantMaidenName
		,target.InfantCurrentSurname = source.InfantCurrentSurname
		,target.InfantForenames = source.InfantForenames
		,target.InfantDateOfBirth = source.InfantDateOfBirth
		,target.InfantPlaceOfBirth = source.InfantPlaceOfBirth
		,target.InfantEthnicGroupPrefix = source.InfantEthnicGroupPrefix
		,target.InfantEthnicGroupBody = source.InfantEthnicGroupBody
		,target.InfantEthnicGroupOpcs = source.InfantEthnicGroupOpcs
		,target.InfantEthnicGroupPas = source.InfantEthnicGroupPas
		,target.InfantGpSurname = source.InfantGpSurname
		,target.InfantGpSequence = source.InfantGpSequence
		,target.InfantTraceStatus = source.InfantTraceStatus
		,target.InfantLocation = source.InfantLocation	
		,target.Updated = getdate()
		,target.ByWhom = system_user 

when
	not matched by source
then
	delete

when
	not matched by target
then
	insert
		(
		 MotherNo
		,MotherNhsNumber
		,MotherNhsNoValid
		,CurrentOccurrence
		,MotherMaidenName
		,MotherCurrentSurname
		,MotherFForenames
		,MotherTitle
		,MotherDateOfBirth
		,MotherPlaceOfBirth
		,UkEntryDate
		,MotherDateApprox
		,MotherEthnicGroupPrefix
		,MotherEthnicGroupBody
		,MotherEthnicGroupOpcs
		,MotherEthnicGroupPas
		,MotherGpSurname
		,MotherGpSequence
		,MotherTraceStatus
		,MotherLocation
		,MotherDateOfDeath
		,MotherTimeOfDeath
		,MotherDeathIndicator
		,PregnancyPortable
		,PregnancyDiskCreateDate
		,PregnancyStatus
		,PregnancyType
		,PregnancyUnbooked
		,PregnancyCardNumber
		,PregnancyCreateDate
		,PregnancyCreateTime
		,MotherReligion
		,OtherReligion
		,Marital
		,MSurname
		,PrefName
		,MotherOccupation
		,MainIncome
		,PrevEmployment
		,MotherTelephoneWork
		,OneParentFam
		,SocialClass
		,BkScreen1Ind
		,BkScreen1aInd
		,BkScreen2Ind
		,InvScreen1Ind
		,InvScreen2Ind
		,AnScreen1Ind
		,AnScreen2Ind
		,AnReason
		,PregnancyAnDate
		,AnOperation
		,PregnancyGpSurname
		,PregnancyGpSequence
		,TypeOfPatient
		,OverseasVisitor
		,PregnancyConCode
		,PregnancyBookingMidwife
		,PregnancyMidwifeTeam
		,ReasonCompletion
		,SpecOtherPlace
		,ConfirmedDob
		,ReferredBy
		,ArchiveDate
		,AnSex
		,PregnancyBookingData
		,Erdip
		,HospReferredTo
		,ReferralDate
		,ReferralNumber
		,PregnancyLocation
		,ScunEcno
		,AnWardType
		,ReferredByOther
		,ReferredByHosp
		,PregnancyCommunityType
		,ReferralGestWks
		,ReferralGestDays
		,LmpAtReferral
		,EddAtReferral
		,BnScreen1Ind
		,NumberInfants
		,OneParent
		,LabourOnset
		,Augmentation
		,OnsetFirstDate
		,OnsetFirstTime
		,OnsetSecondDate
		,OnsetSecondTime
		,MethodIndAug
		,BnScreen2Ind
		,Pyrexia
		,BloodLoss
		,Perineum
		,NonPerinealTears
		,Suture
		,ManualRemoval
		,PlacentaMembranes
		,Membranes
		,LabCompl
		,LabComplText
		,IntendedFeeding
		,LabResearch
		,Support
		,DScreen1Ind
		,Eclampsia
		,Thromboembolism
		,Erpc
		,PuerperalPsychosis
		,Infection
		,DischHbTest
		,DischHaemoglobin
		,AntiD
		,RubellaVac
		,BloodTrans
		,Sterilization
		,Contraception
		,DMatCompl
		,DMatComplText
		,DScreen2Ind
		,Appointment
		,ResearchDisch
		,AgeAtDel
		,LabourDuration1stStage
		,LabourDuration2ndStage
		,LabourDurationLabour
		,PnLenStay
		,AnComplicationsIcd
		,DelComplicationsIcd
		,PuerpComplicationsIcd
		,MatOperationsIcd
		,TenSmokingStage
		,TenCigsPerDay
		,TenPartnerSmoke
		,DischSmokingStage
		,DischCigsPerDay
		,DischPartnerSmoke
		,RiskDelivery
		,DischMedication
		,DischMedicationText
		,DischPartnerCigsPerDay
		,TenPartnerCigsPerDay
		,SkinToSkin
		,SkinToSkinText
		,AnalMucosa
		,TempMaxLabour
		,EpisiotomySutured
		,LabourLocation
		,BreastFeedInitiated
		,Date1stBreastFeed
		,Time1stBreastFeed
		,PnLenStayHours
		,PnLenStayMinutes
		,TransBpSystolic
		,TransBpDiastolic
		,TransToCommunityDate
		,TransferringHospNumber
		,CmwTeam
		,CmwDefaultMidwife
		,CmwBackupMidwife
		,CmwSource
		,CmwHospitalName
		,CmwHospitalChar
		,CmwHospitalOrgCode
		,TransComments
		,CmwMDischDate
		,CmwMDischCondition
		,CpNormalLabour
		,CpReasonNotUsed
		,CpOtherReasonNotUsed
		,PnCare
		,PnCareHosp
		,PnCareOther
		,NonPerinealSuture
		,TimeSutureStarted
		,ReasonSutureDelayed
		,SterilisationText
		,OneToOneCare
		,FgmReversal
		,FgmReversalStage
		,FgmReferralToCpMw
		,DelGbsCarrier
		,DischargedToHvDate
		,DischargedToHvTime
		,PlannedDischargeDate
		,PlannedDischargeTime
		,PnComplications
		,PnComplicationsText
		,Occurrence
		,BirthOrder
		,InfantNo
		,BnScreen3Ind
		,RuptureMethod
		,RuptureDate
		,RuptureTime
		,CordProlapse
		,PresentationLabour
		,ElectronicFh
		,FetalPhTest
		,FetalPh
		,Meconium
		,CordBloodGas
		,TimeOfBirth
		,BirthWeight
		,BirthWeightCentile
		,Gestation
		,GestationByDate
		,GestationByUs
		,GestUsCertain
		,CongenitalAbnorm
		,CongenitalDesc
		,BnScreen4Ind
		,PresentationDeliv
		,BreechDiagnosis
		,BirthingPool
		,MethodDelivery
		,IndicCaesarian
		,Outcome
		,PlaceDelivery
		,ReasonForChange
		,ConductDel
		,DeliveryTeam
		,DeliveryMidwife
		,SpecPlaceDelivery
		,BnScreen5Ind
		,ApgarOne
		,ApgarFive
		,ApgarTen
		,OnsetRespiration
		,ResusPressure
		,ResusDrugs
		,ChildDischAddress
		,ConsPaed
		,InfCompl
		,InfComplText
		,InfResearch
		,MidwifeDelName
		,MidwifeDelInitials
		,MidwifeDelStatus
		,ChildNumber
		,IndexPage
		,HealthDistrict
		,TransferType
		,HealthVisitor
		,Registration
		,BirthPlace
		,BirthHA
		,OPCSBirthArea
		,OPCSResidArea
		,BornAt
		,LAWard
		,AssessGest
		,AgreedGestFrom
		,AgreedGestTo
		,HeadCircum
		,HeadCircumCentile
		,[Length]
		,LengthCentile
		,DScreenInd
		,HIP
		,Convulsions
		,OtherAbnorm
		,Jaundice
		,Bilirubin
		,DInfCompl
		,DInfComplText
		,CongAbsDisch
		,DScreen3AInd
		,SCBU
		,TransCare
		,Cord
		,Guthrie
		,GuthrieDate
		,Haemoglobinopathy
		,HaemoglobinopathyDate
		,Vitamink
		,VitaminkDate
		,VitaminkText
		,InfHbTest
		,InfHaemaglobin
		,InfBloodGroup
		,InfCoombs
		,DischargeExamination
		,Condition
		,DischExamComments
		,HighestLoc
		,Condition28Day
		,Resarch28Day
		,Comments
		,Feeding28Day
		,NnCompl
		,NnComplText
		,DatePlacenta
		,TimePlacenta
		,BirthDurationLabour
		,BirthDuration2ndStage
		,BirthDuration3rdStage
		,HearingTest
		,dateHearing
		,HearingResult
		,ActualHospital
		,BirthDuration1stStage
		,Tab0
		,Tab1
		,Tab2
		,Tab3
		,tab4
		,DelHospCode
		,DelHospName
		,ChildHealthCode
		,CordVenousPH
		,CordVenBaseDeficit
		,CordArterialPH
		,CordArtBaseDeficit
		,ThirdStageManagement
		,Inf1Complications
		,Inf2Complications
		,CreateBaby
		,BnBoxCode
		,EcvLabour
		,HighestLoc2002
		,BirthLocation
		,DateCsDecided
		,TimeCsDecided
		,TimeToCaesarian
		,MidwifeKnown
		,GestationDays
		,GestByDateDays
		,GestByUsDays
		,AssessGestDays
		,AgreedGestFromDays
		,FollowUpDay
		,DilationAtCs
		,TransWeight
		,TransFeeding
		,CmwIDischWeight
		,CmwIDischCondition
		,PnSex
		,PnDateOfBirth
		,PnAgeAtDischarge
		,BirthAnDate
		,IndicInstrumental
		,ShoulderDystocia
		,ShoulderInjury
		,ShoulderInjuryText
		,CsReasonForDelay
		,Palpable5ths
		,FetalPosition
		,Station
		,Moulding
		,Caput
		,LiquorColour
		,DeliveryOfBaby
		,CordEntanglement
		,Position
		,NewbornExamination
		,NewbornExaminationDate
		,NewbornExaminationTime
		,ExaminationCompletedBy
		,ExaminationMidwifePin
		,ExaminationText
		,Heart
		,Eyes
		,Testes
		,BirthMarksBruises
		,BirthMarksBruisesText
		,NewbornReferral
		,CongenitalList
		,RedBookCompleted
		,InfantNhsNumber
		,InfantNhsNoValid
		,InfantSex
		,InfantMaidenName
		,InfantCurrentSurname
		,InfantForenames
		,InfantDateOfBirth
		,InfantPlaceOfBirth
		,InfantEthnicGroupPrefix
		,InfantEthnicGroupBody
		,InfantEthnicGroupOpcs
		,InfantEthnicGroupPas
		,InfantGpSurname
		,InfantGpSequence
		,InfantTraceStatus
		,InfantLocation
		,Created
		,Updated
		,ByWhom
		)

	values
		(
		 source.MotherNo
		,source.MotherNhsNumber
		,source.MotherNhsNoValid
		,source.CurrentOccurrence
		,source.MotherMaidenName
		,source.MotherCurrentSurname
		,source.MotherFForenames
		,source.MotherTitle
		,source.MotherDateOfBirth
		,source.MotherPlaceOfBirth
		,source.UkEntryDate
		,source.MotherDateApprox
		,source.MotherEthnicGroupPrefix
		,source.MotherEthnicGroupBody
		,source.MotherEthnicGroupOpcs
		,source.MotherEthnicGroupPas
		,source.MotherGpSurname
		,source.MotherGpSequence
		,source.MotherTraceStatus
		,source.MotherLocation
		,source.MotherDateOfDeath
		,source.MotherTimeOfDeath
		,source.MotherDeathIndicator
		,source.PregnancyPortable
		,source.PregnancyDiskCreateDate
		,source.PregnancyStatus
		,source.PregnancyType
		,source.PregnancyUnbooked
		,source.PregnancyCardNumber
		,source.PregnancyCreateDate
		,source.PregnancyCreateTime
		,source.MotherReligion
		,source.OtherReligion
		,source.Marital
		,source.MSurname
		,source.PrefName
		,source.MotherOccupation
		,source.MainIncome
		,source.PrevEmployment
		,source.MotherTelephoneWork
		,source.OneParentFam
		,source.SocialClass
		,source.BkScreen1Ind
		,source.BkScreen1aInd
		,source.BkScreen2Ind
		,source.InvScreen1Ind
		,source.InvScreen2Ind
		,source.AnScreen1Ind
		,source.AnScreen2Ind
		,source.AnReason
		,source.PregnancyAnDate
		,source.AnOperation
		,source.PregnancyGpSurname
		,source.PregnancyGpSequence
		,source.TypeOfPatient
		,source.OverseasVisitor
		,source.PregnancyConCode
		,source.PregnancyBookingMidwife
		,source.PregnancyMidwifeTeam
		,source.ReasonCompletion
		,source.SpecOtherPlace
		,source.ConfirmedDob
		,source.ReferredBy
		,source.ArchiveDate
		,source.AnSex
		,source.PregnancyBookingData
		,source.Erdip
		,source.HospReferredTo
		,source.ReferralDate
		,source.ReferralNumber
		,source.PregnancyLocation
		,source.ScunEcno
		,source.AnWardType
		,source.ReferredByOther
		,source.ReferredByHosp
		,source.PregnancyCommunityType
		,source.ReferralGestWks
		,source.ReferralGestDays
		,source.LmpAtReferral
		,source.EddAtReferral
		,source.BnScreen1Ind
		,source.NumberInfants
		,source.OneParent
		,source.LabourOnset
		,source.Augmentation
		,source.OnsetFirstDate
		,source.OnsetFirstTime
		,source.OnsetSecondDate
		,source.OnsetSecondTime
		,source.MethodIndAug
		,source.BnScreen2Ind
		,source.Pyrexia
		,source.BloodLoss
		,source.Perineum
		,source.NonPerinealTears
		,source.Suture
		,source.ManualRemoval
		,source.PlacentaMembranes
		,source.Membranes
		,source.LabCompl
		,source.LabComplText
		,source.IntendedFeeding
		,source.LabResearch
		,source.Support
		,source.DScreen1Ind
		,source.Eclampsia
		,source.Thromboembolism
		,source.Erpc
		,source.PuerperalPsychosis
		,source.Infection
		,source.DischHbTest
		,source.DischHaemoglobin
		,source.AntiD
		,source.RubellaVac
		,source.BloodTrans
		,source.Sterilization
		,source.Contraception
		,source.DMatCompl
		,source.DMatComplText
		,source.DScreen2Ind
		,source.Appointment
		,source.ResearchDisch
		,source.AgeAtDel
		,source.LabourDuration1stStage
		,source.LabourDuration2ndStage
		,source.LabourDurationLabour
		,source.PnLenStay
		,source.AnComplicationsIcd
		,source.DelComplicationsIcd
		,source.PuerpComplicationsIcd
		,source.MatOperationsIcd
		,source.TenSmokingStage
		,source.TenCigsPerDay
		,source.TenPartnerSmoke
		,source.DischSmokingStage
		,source.DischCigsPerDay
		,source.DischPartnerSmoke
		,source.RiskDelivery
		,source.DischMedication
		,source.DischMedicationText
		,source.DischPartnerCigsPerDay
		,source.TenPartnerCigsPerDay
		,source.SkinToSkin
		,source.SkinToSkinText
		,source.AnalMucosa
		,source.TempMaxLabour
		,source.EpisiotomySutured
		,source.LabourLocation
		,source.BreastFeedInitiated
		,source.Date1stBreastFeed
		,source.Time1stBreastFeed
		,source.PnLenStayHours
		,source.PnLenStayMinutes
		,source.TransBpSystolic
		,source.TransBpDiastolic
		,source.TransToCommunityDate
		,source.TransferringHospNumber
		,source.CmwTeam
		,source.CmwDefaultMidwife
		,source.CmwBackupMidwife
		,source.CmwSource
		,source.CmwHospitalName
		,source.CmwHospitalChar
		,source.CmwHospitalOrgCode
		,source.TransComments
		,source.CmwMDischDate
		,source.CmwMDischCondition
		,source.CpNormalLabour
		,source.CpReasonNotUsed
		,source.CpOtherReasonNotUsed
		,source.PnCare
		,source.PnCareHosp
		,source.PnCareOther
		,source.NonPerinealSuture
		,source.TimeSutureStarted
		,source.ReasonSutureDelayed
		,source.SterilisationText
		,source.OneToOneCare
		,source.FgmReversal
		,source.FgmReversalStage
		,source.FgmReferralToCpMw
		,source.DelGbsCarrier
		,source.DischargedToHvDate
		,source.DischargedToHvTime
		,source.PlannedDischargeDate
		,source.PlannedDischargeTime
		,source.PnComplications
		,source.PnComplicationsText
		,source.Occurrence
		,source.BirthOrder
		,source.InfantNo
		,source.BnScreen3Ind
		,source.RuptureMethod
		,source.RuptureDate
		,source.RuptureTime
		,source.CordProlapse
		,source.PresentationLabour
		,source.ElectronicFh
		,source.FetalPhTest
		,source.FetalPh
		,source.Meconium
		,source.CordBloodGas
		,source.TimeOfBirth
		,source.BirthWeight
		,source.BirthWeightCentile
		,source.Gestation
		,source.GestationByDate
		,source.GestationByUs
		,source.GestUsCertain
		,source.CongenitalAbnorm
		,source.CongenitalDesc
		,source.BnScreen4Ind
		,source.PresentationDeliv
		,source.BreechDiagnosis
		,source.BirthingPool
		,source.MethodDelivery
		,source.IndicCaesarian
		,source.Outcome
		,source.PlaceDelivery
		,source.ReasonForChange
		,source.ConductDel
		,source.DeliveryTeam
		,source.DeliveryMidwife
		,source.SpecPlaceDelivery
		,source.BnScreen5Ind
		,source.ApgarOne
		,source.ApgarFive
		,source.ApgarTen
		,source.OnsetRespiration
		,source.ResusPressure
		,source.ResusDrugs
		,source.ChildDischAddress
		,source.ConsPaed
		,source.InfCompl
		,source.InfComplText
		,source.InfResearch
		,source.MidwifeDelName
		,source.MidwifeDelInitials
		,source.MidwifeDelStatus
		,source.ChildNumber
		,source.IndexPage
		,source.HealthDistrict
		,source.TransferType
		,source.HealthVisitor
		,source.Registration
		,source.BirthPlace
		,source.BirthHA
		,source.OPCSBirthArea
		,source.OPCSResidArea
		,source.BornAt
		,source.LAWard
		,source.AssessGest
		,source.AgreedGestFrom
		,source.AgreedGestTo
		,source.HeadCircum
		,source.HeadCircumCentile
		,source.Length
		,source.LengthCentile
		,source.DScreenInd
		,source.HIP
		,source.Convulsions
		,source.OtherAbnorm
		,source.Jaundice
		,source.Bilirubin
		,source.DInfCompl
		,source.DInfComplText
		,source.CongAbsDisch
		,source.DScreen3AInd
		,source.SCBU
		,source.TransCare
		,source.Cord
		,source.Guthrie
		,source.GuthrieDate
		,source.Haemoglobinopathy
		,source.HaemoglobinopathyDate
		,source.Vitamink
		,source.VitaminkDate
		,source.VitaminkText
		,source.InfHbTest
		,source.InfHaemaglobin
		,source.InfBloodGroup
		,source.InfCoombs
		,source.DischargeExamination
		,source.Condition
		,source.DischExamComments
		,source.HighestLoc
		,source.Condition28Day
		,source.Resarch28Day
		,source.Comments
		,source.Feeding28Day
		,source.NnCompl
		,source.NnComplText
		,source.DatePlacenta
		,source.TimePlacenta
		,source.BirthDurationLabour
		,source.BirthDuration2ndStage
		,source.BirthDuration3rdStage
		,source.HearingTest
		,source.dateHearing
		,source.HearingResult
		,source.ActualHospital
		,source.BirthDuration1stStage
		,source.Tab0
		,source.Tab1
		,source.Tab2
		,source.Tab3
		,source.tab4
		,source.DelHospCode
		,source.DelHospName
		,source.ChildHealthCode
		,source.CordVenousPH
		,source.CordVenBaseDeficit
		,source.CordArterialPH
		,source.CordArtBaseDeficit
		,source.ThirdStageManagement
		,source.Inf1Complications
		,source.Inf2Complications
		,source.CreateBaby
		,source.BnBoxCode
		,source.EcvLabour
		,source.HighestLoc2002
		,source.BirthLocation
		,source.DateCsDecided
		,source.TimeCsDecided
		,source.TimeToCaesarian
		,source.MidwifeKnown
		,source.GestationDays
		,source.GestByDateDays
		,source.GestByUsDays
		,source.AssessGestDays
		,source.AgreedGestFromDays
		,source.FollowUpDay
		,source.DilationAtCs
		,source.TransWeight
		,source.TransFeeding
		,source.CmwIDischWeight
		,source.CmwIDischCondition
		,source.PnSex
		,source.PnDateOfBirth
		,source.PnAgeAtDischarge
		,source.BirthAnDate
		,source.IndicInstrumental
		,source.ShoulderDystocia
		,source.ShoulderInjury
		,source.ShoulderInjuryText
		,source.CsReasonForDelay
		,source.Palpable5ths
		,source.FetalPosition
		,source.Station
		,source.Moulding
		,source.Caput
		,source.LiquorColour
		,source.DeliveryOfBaby
		,source.CordEntanglement
		,source.Position
		,source.NewbornExamination
		,source.NewbornExaminationDate
		,source.NewbornExaminationTime
		,source.ExaminationCompletedBy
		,source.ExaminationMidwifePin
		,source.ExaminationText
		,source.Heart
		,source.Eyes
		,source.Testes
		,source.BirthMarksBruises
		,source.BirthMarksBruisesText
		,source.NewbornReferral
		,source.CongenitalList
		,source.RedBookCompleted
		,source.InfantNhsNumber
		,source.InfantNhsNoValid
		,source.InfantSex
		,source.InfantMaidenName
		,source.InfantCurrentSurname
		,source.InfantForenames
		,source.InfantDateOfBirth
		,source.InfantPlaceOfBirth
		,source.InfantEthnicGroupPrefix
		,source.InfantEthnicGroupBody
		,source.InfantEthnicGroupOpcs
		,source.InfantEthnicGroupPas
		,source.InfantGpSurname
		,source.InfantGpSequence
		,source.InfantTraceStatus
		,source.InfantLocation
		,getdate()
		,getdate()
		,system_user
		)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats

end
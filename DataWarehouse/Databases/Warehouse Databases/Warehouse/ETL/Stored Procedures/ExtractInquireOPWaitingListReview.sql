﻿CREATE procedure [ETL].[ExtractInquireOPWaitingListReview] 
	@CensusDate smalldatetime
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)


select @StartTime = getdate()

select @RowsInserted = 0


insert into ETL.TImportOPWaitingListReview
(
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,NHSNumber
	,DistrictNo
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,ResCode
	,HomePhone
	,WorkPhone
	,ConsultantCode
	,SpecialtyCode
	,PriorityCode
	,CommentClinical
	,SiteCode
	,PurchaserCode
	,CancelledBy
	,CasenoteNumber
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode
	,ContractSerialNumber
	,PatientDeathIndicator
	,ScheduledCancelReasonCode
	,WLAppointmentTypeCode
	,WaitingListCode
	,EthnicOriginCode	
	,DateOnList
)
select
	 SourcePatientNo
	,SourceEncounterNo
	,@CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,NHSNumber
	,DistrictNo
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,ResCode
	,HomePhone
	,WorkPhone
	,ConsultantCode
	,SpecialtyCode
	,PriorityCode
	,CommentClinical
	,SiteCode
	,PurchaserCode
	,CancelledBy
	,CasenoteNumber
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode
	,ContractSerialNo
	,PatientDeathIndicator
	,ScheduledCancelReasonCode
	,WLAppointmentTypeCode
	,WaitingListCode
	,EthnicOriginCode
	,DateOnList
from
	(
	SELECT
		 SourcePatientNo = R.InternalPatientNumber 
		,SourceEncounterNo = R.EpisodeNumber  
		,PatientSurname = P.Surname  
		,PatientForename = P.Forenames  
		,PatientTitle = P.Title  
		,SexCode = P.Sex  
		,DateOfBirth = P.PtDoB  
		,NHSNumber = P.NHSNumber 
		,DistrictNo = R.DistrictNumber  
		,R.ReferralDate
		,BookedDate = OPA.ApptBookedDate  
		,BookedTime = OPA.ApptBookedTime  
		,AppointmentDate = ApptDate  
		,AppointmentTypeCode = OPA.ApptType  
		,AppointmentStatusCode = OPA.ApptStatus  
		,AppointmentCategoryCode = OPA.ApptCategory  
		--,R.QM08StartWaitDate
		,QM08StartWaitDate = 
			convert(date,
				coalesce(
					(
					select
						max(convert(date, LatestNonAttendance.ApptDate)) 
					from
						[$(PAS)].Inquire.OPA LatestNonAttendance
					where
						LatestNonAttendance.InternalPatientNumber = OPWLENTRY.InternalPatientNumber
					and	LatestNonAttendance.EpisodeNumber = OPWLENTRY.EpisodeNumber
					and LatestNonAttendance.BookFromWL = OPWLENTRY.TreatmentCode -- advised by Dusia 2 Oct 2015
					and	convert(smalldatetime, LatestNonAttendance.ApptDate) > OPWLENTRY.DateOnList and convert(smalldatetime, LatestNonAttendance.ApptDate) < @CensusDate
					and	(
							LatestNonAttendance.CancelBy != 'H'
						or	LatestNonAttendance.ApptStatus in ('DNA', 'CND', 'NATT')
						)
					)
				,OPWLENTRY.DateOnList
				)
			)
		,QM08EndWaitDate = null -- R.QM08EndWtDate -- advised by Dusia 2 Oct 2015
		,AppointmentTime = OPA.PtApptStartDtimeInt  
		,OPA.ClinicCode
		,SourceOfReferralCode = R.RefBy  
		,MaritalStatusCode = P.MaritalStatus 
		,ReligionCode = P.Religion 
		,Postcode = P.PtAddrPostCode  
		,PatientsAddress1 = P.PtAddrLine1 
		,PatientsAddress2 = P.PtAddrLine2 
		,PatientsAddress3 = P.PtAddrLine3 
		,ResCode = OPA.ResCode  
		,HomePhone = P.PtHomePhone 
		,WorkPhone = P.PtWorkPhone 
		,ConsultantCode = R.ConsCode  
		,SpecialtyCode = R.Specialty  
		,PriorityCode = R.PriorityType  
		,CommentClinical = OPA.ApptComment 
		,SiteCode = R.HospitalCode  
		,PurchaserCode = OPA.ApptPurchaser  
		,CancelledBy = OPA.CancelBy  
		,CasenoteNumber = R.CaseNoteNumber  
		,EpisodicGpCode = R.EpiGPCode  
		,EpisodicGpPracticeCode = R.EpiGPPracticeCode  

		,RTTPathwayID = left(PW.PathwayNumber , 25) 
		,RTTPathwayCondition = PW.PathwayCondition 
		,RTTStartDate = PWP.RttStartDate 
		,RTTEndDate = PWP.RttEndDate 
		,RTTSpecialtyCode = PW.RttSpeciality 
		,RTTCurrentProviderCode = PW.RttCurProv 
		,RTTCurrentStatusCode = PW.RttCurrentStatus 
		,RTTCurrentStatusDate = PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
		,RTTOverseasStatusFlag = PW.RttOsvStatus 
		,AppointmentID = OPA.OPAID 
		,SourceUniqueID = coalesce(OPA.OPAID, R.OPREFERRALID) 
		,AdminCategoryCode = R.Category 
		,ContractSerialNo = ALLOCATEDCONTRACT.ContractId 
		,PatientDeathIndicator = P.PtDeathIndInt
		,ScheduledCancelReasonCode = Attend.SchReasonForCancellationCodeAppointment
		,WLAppointmentTypeCode = OPWLENTRY.AppointmentType 
		,WaitingListCode = OPWLENTRY.TreatmentCode
		,EthnicOriginCode = P.EthnicType  
		,OPWLENTRY.DateOnList
	FROM
		[$(PAS)].Inquire.OPWLENTRY
	
	inner join [$(PAS)].Inquire.OPREFERRAL R
	on	R.InternalPatientNumber = OPWLENTRY.InternalPatientNumber
	AND	R.EpisodeNumber = OPWLENTRY.EpisodeNumber
		
	LEFT JOIN [$(PAS)].Inquire.OPA --appointment after census date
	ON	R.EpisodeNumber = OPA.EpisodeNumber
	AND	R.InternalPatientNumber = OPA.InternalPatientNumber
    AND R.ReferralDate = OPA.ReferralDate
	and	OPA.ClinicCode != 'CASREH'
	and	OPA.BookFromWL = OPWLENTRY.TreatmentCode
	and	OPA.ApptTime > 
			convert(
				varchar
				,@CensusDate
				,112
			) + '2400'

	and	not exists --get the earliest appointment
		(
		select
			1
		from
			[$(PAS)].Inquire.OPA Previous
		where
			Previous.EpisodeNumber = OPA.EpisodeNumber
		AND	Previous.InternalPatientNumber = OPA.InternalPatientNumber
		AND Previous.ReferralDate = OPA.ReferralDate
		and	Previous.ClinicCode != 'CASREH'
		and	Previous.BookFromWL = OPWLENTRY.TreatmentCode
		and	Previous.ApptTime > 
				convert(
					varchar
					,@CensusDate
					,112
				) + '2400'
				
		and	(
				Previous.ApptTime < OPA.ApptTime
			or	(
					Previous.ApptTime = OPA.ApptTime
				and	Previous.OPAID < OPA.OPAID
				)
			)
			
		)

	left JOIN [$(PAS)].Inquire.OPAPPOINTATTEND Attend 
	ON	OPA.InternalPatientNumber = Attend.InternalPatientNumber
	AND	OPA.EpisodeNumber = Attend.EpisodeNumber
	AND	OPA.PtApptStartDtimeInt = Attend.PtApptStartDtimeInt
	AND	OPA.ResCode = Attend.ResCode

	INNER JOIN [$(PAS)].Inquire.PATDATA P
	ON	R.InternalPatientNumber = P.InternalPatientNumber

	left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
	on	EPW.InternalPatientNumber = R.InternalPatientNumber
	and	EPW.EpisodeNumber = R.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTPATHWAY PW
	on	PW.InternalPatientNumber = EPW.InternalPatientNumber
	and	PW.PathwayNumber = EPW.PathwayNumber

	left join [$(PAS)].Inquire.RTTPERIOD PWP
	on	PWP.InternalPatientNumber = PW.InternalPatientNumber
	and	PWP.PathwayNumber = PW.PathwayNumber
	and	PWP.PPeriodId = '0'

	LEFT JOIN [$(PAS)].Inquire.ALLOCATEDCONTRACT 
	ON	ALLOCATEDCONTRACT.InternalPatientNumber = R.InternalPatientNumber
	AND	ALLOCATEDCONTRACT.EpisodeNumber = R.EpisodeNumber
	AND	ALLOCATEDCONTRACT.CmReverseDttime = '999999999999'
	
	WHERE
		R.QM08EndWtDate Is not Null --has already been seen (as a first)
	AND	R.DischargeDt Is Null --still waiting

	and	not exists
		(
		select
			1
		from
			[$(PAS)].Inquire.OPWLENTRY Previous
		where
			Previous.InternalPatientNumber = R.InternalPatientNumber
		AND	Previous.EpisodeNumber = R.EpisodeNumber
		and	(
				Previous.DateOnList < OPWLENTRY.DateOnList
			or	(
					Previous.DateOnList = OPWLENTRY.DateOnList
				and	Previous.OPWLENTRYID < OPWLENTRY.OPWLENTRYID
				)
			)
		)

	) WLOutpatient


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Census Date ' + CONVERT(varchar(11), @CensusDate) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireOPWaitingListReview', @Stats, @StartTime
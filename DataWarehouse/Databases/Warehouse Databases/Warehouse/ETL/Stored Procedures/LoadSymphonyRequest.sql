﻿
CREATE PROCEDURE [ETL].[LoadSymphonyRequest]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Request table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 RequestID
			,AttendanceID
			,DepartmentID
			,RequestTime
			,RequestID1
			,Field1ID
			,Field2ID
			,Field3ID
			,Field4ID
			,Field5ID
			,Field6ID
			,Field7ID
			,HasExtraFields
			,NotesID
			,IncludingPatientLetter
			,CreatedByID
			,CreatedTime
			,UpdatedByID
			,UpdatedTime
			,CautionID
			,RepeatID
			,TPTransID
			)
into
	#TLoadSymphonyRequest
from
	(
	select
		 RequestID
		,AttendanceID = cast(AttendanceID as int)
		,DepartmentID = cast(DepartmentID as smallint)
		,RequestTime = cast(RequestTime as datetime)
		,RequestID1 = cast(RequestID1 as int)
		,Field1ID = cast(Field1ID as int)
		,Field2ID = cast(Field2ID as int)
		,Field3ID = cast(Field3ID as int)
		,Field4ID = cast(Field4ID as int)
		,Field5ID = cast(Field5ID as int)
		,Field6ID = cast(Field6ID as varchar(255))
		,Field7ID = cast(Field7ID as varchar(255))
		,HasExtraFields = cast(HasExtraFields as bit)
		,NotesID = cast(NotesID as int)
		,IncludingPatientLetter = cast(IncludingPatientLetter as bit)
		,CreatedByID = cast(CreatedByID as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,UpdatedByID = cast(UpdatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,CautionID = cast(CautionID as int)
		,RepeatID = cast(RepeatID as int)
		,TPTransID = cast(TPTransID as varchar(30))
	from
		(
		select
			 RequestID = Request.req_reqid
			,AttendanceID = Request.req_atdid
			,DepartmentID = Request.req_depid
			,RequestTime = Request.req_date
			,RequestID1 = Request.req_request
			,Field1ID = Request.req_field1
			,Field2ID = Request.req_field2
			,Field3ID = Request.req_field3
			,Field4ID = Request.req_field4
			,Field5ID = Request.req_field5
			,Field6ID = Request.req_field6
			,Field7ID = Request.req_field7
			,HasExtraFields = Request.req_hasefds
			,NotesID = Request.req_notes
			,IncludingPatientLetter = Request.req_includeingpletter
			,CreatedByID = Request.req_createdby
			,CreatedTime = Request.req_created
			,UpdatedByID = Request.req_updatedby
			,UpdatedTime = Request.req_update
			,CautionID = Request.req_caution
			,RepeatID = Request.req_repeat
			,TPTransID = Request.Req_TPTransID
		from
			[$(Symphony)].dbo.Request_Details Request
		where
			Request.req_inactive <> 1

		) Encounter
	) Encounter
order by
	Encounter.RequestID


create unique clustered index #IX_TLoadSymphonyRequest on #TLoadSymphonyRequest
	(
	RequestID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Request target
using
	(
	select
		 RequestID
		,AttendanceID
		,DepartmentID
		,RequestTime
		,RequestID1
		,Field1ID
		,Field2ID
		,Field3ID
		,Field4ID
		,Field5ID
		,Field6ID
		,Field7ID
		,HasExtraFields
		,NotesID
		,IncludingPatientLetter
		,CreatedByID
		,CreatedTime
		,UpdatedByID
		,UpdatedTime
		,CautionID
		,RepeatID
		,TPTransID
		,EncounterChecksum
	from
		#TLoadSymphonyRequest
	
	) source
	on	source.RequestID = target.RequestID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 RequestID
			,AttendanceID
			,DataEntryProcedureID
			,RequestTime
			,RequestID1
			,Field1ID
			,Field2ID
			,Field3ID
			,Field4ID
			,Field5ID
			,Field6ID
			,Field7ID
			,HasExtraFields
			,NotesID
			,IncludingPatientLetter
			,CreatedByID
			,CreatedTime
			,UpdatedByID
			,UpdatedTime
			,CautionID
			,RepeatID
			,TPTransID
			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.RequestID
			,source.AttendanceID
			,source.DepartmentID
			,source.RequestTime
			,source.RequestID1
			,source.Field1ID
			,source.Field2ID
			,source.Field3ID
			,source.Field4ID
			,source.Field5ID
			,source.Field6ID
			,source.Field7ID
			,source.HasExtraFields
			,source.NotesID
			,source.IncludingPatientLetter
			,source.CreatedByID
			,source.CreatedTime
			,source.UpdatedByID
			,source.UpdatedTime
			,source.CautionID
			,source.RepeatID
			,source.TPTransID

			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.RequestID = source.RequestID
			,target.AttendanceID = source.AttendanceID
			,target.DataEntryProcedureID = source.DepartmentID
			,target.RequestTime = source.RequestTime
			,target.RequestID1 = source.RequestID1
			,target.Field1ID = source.Field1ID
			,target.Field2ID = source.Field2ID
			,target.Field3ID = source.Field3ID
			,target.Field4ID = source.Field4ID
			,target.Field5ID = source.Field5ID
			,target.Field6ID = source.Field6ID
			,target.Field7ID = source.Field7ID
			,target.HasExtraFields = source.HasExtraFields
			,target.NotesID = source.NotesID
			,target.IncludingPatientLetter = source.IncludingPatientLetter
			,target.CreatedByID = source.CreatedByID
			,target.CreatedTime = source.CreatedTime
			,target.UpdatedByID = source.UpdatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.CautionID = source.CautionID
			,target.RepeatID = source.RepeatID
			,target.TPTransID = source.TPTransID
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


--Update IsFirst and IsLast to improve subsequent performance
update
	Symphony.Request
set
	IsFirst =
		case
		when
			Request.RequestID =
			(
			select top 1
				PreviousRequest.RequestID
			from
				Symphony.Request PreviousRequest
			where
				PreviousRequest.DataEntryProcedureID = Request.DataEntryProcedureID
			and	PreviousRequest.AttendanceID = Request.AttendanceID
			
			order by
				PreviousRequest.RequestTime asc
				,Request.RequestID asc
			)
		then 1
		else 0
		end

	,IsLast =
		case
		when
			Request.RequestID =
			(
			select top 1
				PreviousRequest.RequestID
			from
				Symphony.Request PreviousRequest
			where
				PreviousRequest.DataEntryProcedureID = Request.DataEntryProcedureID
			and	PreviousRequest.AttendanceID = Request.AttendanceID
			
			order by
				PreviousRequest.RequestTime desc
				,Request.RequestID desc
			)
		then 1
		else 0
		end


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
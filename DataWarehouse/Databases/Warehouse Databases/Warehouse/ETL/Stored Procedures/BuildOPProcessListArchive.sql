﻿create procedure [ETL].[BuildOPProcessListArchive] as

insert
into
	OP.ProcessListArchive
(
	 EncounterRecno
	,Action
	,ArchiveTime
)
select
	 EncounterRecno
	,Action
	,ArchiveTime = getdate()
from
	OP.ProcessList;


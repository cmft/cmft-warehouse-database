﻿
CREATE proc [ETL].[LoadNeonatalBedday]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime


select @FromDate = (select min(ModifiedTime) from ETL.TLoadNeonatalBedday)
select @ToDate = (select max(ModifiedTime) from ETL.TLoadNeonatalBedday)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Neonatal.Bedday target
using
	(
	select
		 EpisodeID
		,CareDate
		,CareLocationID
		,WardCode
		,Weight
		,WeightToday
		,HeadCircumference
		,Length
		,OneToOneNursing
		,CarerStatusCode
		,NursingStatusCode
		,ObservationsMonitoringCode
		,SurgeryToday
		,NeonatalLevelOfCare2001Code
		,NeonatalLevelOfCare2011Code
		,HRGCode
		,GlasgowScore
		,RespiratorySupportCode
		,OxygenCode
		,VentilationModeCode
		,CPAPModeCode
		,NPAirway
		,NitricOxide
		,ECMO
		,Apnoea
		,Pneumothorax
		,ChestDrain
		,TracheostomyCode
		,TracheostomyCarerCode
		,ReplogleTube
		,Surfactant
		,SurfactantDoses
		,SurfactantDoseTotal
		,NecrotizingEntercolitisTreatmentCode
		,InotropesToday
		,ProstinToday
		,PulmonaryVasodilator
		,GastroschisisSilo
		,Stoma
		,RectalWashout
		,Peritonealdialysis
		,Haemofiltration
		,TAT
		,Tone
		,ConsciousnessCode
		,Convulsions
		,NASTreatment
		,NASDrugTreatment
		,NASScore
		,VPShuntSurgery
		,EEG
		,Cooling
		,EpiduralCatheter
		,VentricularDrain
		,VentricularTap
		,ROPScreen
		,ROPGrade
		,ROPSurgery
		,MaxBilirubin
		,Phototherapy
		,Immunoglbulins
		,UrinaryCtaheter
		,TPN
		,GlucoseElectrolyte
		,EnteralFeedCode
		,VolumeMilk
		,FormulaCode
		,FeedingMethodCode
		,AdditiveCode
		,LinesInSitu
		,ExchangeFull
		,ExchangePartial
		,BloodProductCode
		,DiagnosesDay
		,DrugsDay
		,IVDrugsToday
		,SkinToSkinToday
		,PDAToday
		,TransportedToday
		,CVSTreatmentToday
		,InfectionDiagnosis
		,JaundiceDiagnosed
		,Bilirubin
		,HIEScore
		,Probiotics
		,MRSASwab
		,ModifiedTime
		,InterfaceCode
	from
		ETL.TLoadNeonatalBedday

	) source
	on	source.EpisodeID = target.EpisodeID
	and	source.CareDate = target.CareDate
	
	when not matched by source
	and target.ModifiedTime between @FromDate and @ToDate

	then delete

	when not matched
	then
		insert
			(
			 EpisodeID
			,CareDate
			,CareLocationID
			,WardCode
			,Weight
			,WeightToday
			,HeadCircumference
			,Length
			,OneToOneNursing
			,CarerStatusCode
			,NursingStatusCode
			,ObservationsMonitoringCode
			,SurgeryToday
			,NeonatalLevelOfCare2001Code
			,NeonatalLevelOfCare2011Code
			,HRGCode
			,GlasgowScore
			,RespiratorySupportCode
			,OxygenCode
			,VentilationModeCode
			,CPAPModeCode
			,NPAirway
			,NitricOxide
			,ECMO
			,Apnoea
			,Pneumothorax
			,ChestDrain
			,TracheostomyCode
			,TracheostomyCarerCode
			,ReplogleTube
			,Surfactant
			,SurfactantDoses
			,SurfactantDoseTotal
			,NecrotizingEntercolitisTreatmentCode
			,InotropesToday
			,ProstinToday
			,PulmonaryVasodilator
			,GastroschisisSilo
			,Stoma
			,RectalWashout
			,Peritonealdialysis
			,Haemofiltration
			,TAT
			,Tone
			,ConsciousnessCode
			,Convulsions
			,NASTreatment
			,NASDrugTreatment
			,NASScore
			,VPShuntSurgery
			,EEG
			,Cooling
			,EpiduralCatheter
			,VentricularDrain
			,VentricularTap
			,ROPScreen
			,ROPGrade
			,ROPSurgery
			,MaxBilirubin
			,Phototherapy
			,Immunoglbulins
			,UrinaryCtaheter
			,TPN
			,GlucoseElectrolyte
			,EnteralFeedCode
			,VolumeMilk
			,FormulaCode
			,FeedingMethodCode
			,AdditiveCode
			,LinesInSitu
			,ExchangeFull
			,ExchangePartial
			,BloodProductCode
			,DiagnosesDay
			,DrugsDay
			,IVDrugsToday
			,SkinToSkinToday
			,PDAToday
			,TransportedToday
			,CVSTreatmentToday
			,InfectionDiagnosis
			,JaundiceDiagnosed
			,Bilirubin
			,HIEScore
			,Probiotics
			,MRSASwab
			,ModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.EpisodeID
			,source.CareDate
			,source.CareLocationID
			,source.WardCode
			,source.Weight
			,source.WeightToday
			,source.HeadCircumference
			,source.Length
			,source.OneToOneNursing
			,source.CarerStatusCode
			,source.NursingStatusCode
			,source.ObservationsMonitoringCode
			,source.SurgeryToday
			,source.NeonatalLevelOfCare2001Code
			,source.NeonatalLevelOfCare2011Code
			,source.HRGCode
			,source.GlasgowScore
			,source.RespiratorySupportCode
			,source.OxygenCode
			,source.VentilationModeCode
			,source.CPAPModeCode
			,source.NPAirway
			,source.NitricOxide
			,source.ECMO
			,source.Apnoea
			,source.Pneumothorax
			,source.ChestDrain
			,source.TracheostomyCode
			,source.TracheostomyCarerCode
			,source.ReplogleTube
			,source.Surfactant
			,source.SurfactantDoses
			,source.SurfactantDoseTotal
			,source.NecrotizingEntercolitisTreatmentCode
			,source.InotropesToday
			,source.ProstinToday
			,source.PulmonaryVasodilator
			,source.GastroschisisSilo
			,source.Stoma
			,source.RectalWashout
			,source.Peritonealdialysis
			,source.Haemofiltration
			,source.TAT
			,source.Tone
			,source.ConsciousnessCode
			,source.Convulsions
			,source.NASTreatment
			,source.NASDrugTreatment
			,source.NASScore
			,source.VPShuntSurgery
			,source.EEG
			,source.Cooling
			,source.EpiduralCatheter
			,source.VentricularDrain
			,source.VentricularTap
			,source.ROPScreen
			,source.ROPGrade
			,source.ROPSurgery
			,source.MaxBilirubin
			,source.Phototherapy
			,source.Immunoglbulins
			,source.UrinaryCtaheter
			,source.TPN
			,source.GlucoseElectrolyte
			,source.EnteralFeedCode
			,source.VolumeMilk
			,source.FormulaCode
			,source.FeedingMethodCode
			,source.AdditiveCode
			,source.LinesInSitu
			,source.ExchangeFull
			,source.ExchangePartial
			,source.BloodProductCode
			,source.DiagnosesDay
			,source.DrugsDay
			,source.IVDrugsToday
			,source.SkinToSkinToday
			,source.PDAToday
			,source.TransportedToday
			,source.CVSTreatmentToday
			,source.InfectionDiagnosis
			,source.JaundiceDiagnosed
			,source.Bilirubin
			,source.HIEScore
			,source.Probiotics
			,source.MRSASwab
			,source.ModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	
			isnull(cast(target.EpisodeID as varchar(100)), '') = isnull(cast(source.EpisodeID as varchar(100)), '')
		and isnull(target.CareDate, getdate()) = isnull(source.CareDate, getdate())
		and isnull(cast(target.CareLocationID as varchar(100)),'') = isnull(cast(source.CareLocationID as varchar(100)),'')
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.Weight, 0) = isnull(source.Weight, 0)
		and isnull(target.WeightToday, 0) = isnull(source.WeightToday, 0)
		and isnull(target.HeadCircumference, 0) = isnull(source.HeadCircumference, 0)
		and isnull(target.Length, 0) = isnull(source.Length, 0)
		and isnull(target.OneToOneNursing, 0) = isnull(source.OneToOneNursing, 0)
		and isnull(target.CarerStatusCode, 0) = isnull(source.CarerStatusCode, 0)
		and isnull(target.NursingStatusCode, '') = isnull(source.NursingStatusCode, '')
		and isnull(target.ObservationsMonitoringCode, '') = isnull(source.ObservationsMonitoringCode, '')
		and isnull(target.SurgeryToday, 0) = isnull(source.SurgeryToday, 0)
		and isnull(target.NeonatalLevelOfCare2001Code, 0) = isnull(source.NeonatalLevelOfCare2001Code, 0)
		and isnull(target.NeonatalLevelOfCare2011Code, 0) = isnull(source.NeonatalLevelOfCare2011Code, 0)
		and isnull(target.HRGCode, '') = isnull(source.HRGCode, '')
		and isnull(target.GlasgowScore, '') = isnull(source.GlasgowScore, '')
		and isnull(target.RespiratorySupportCode, '') = isnull(source.RespiratorySupportCode, '')
		and isnull(target.OxygenCode, '') = isnull(source.OxygenCode, '')
		and isnull(target.VentilationModeCode, '') = isnull(source.VentilationModeCode, '')
		and isnull(target.CPAPModeCode, '') = isnull(source.CPAPModeCode, '')
		and isnull(target.NPAirway, 0) = isnull(source.NPAirway, 0)
		and isnull(target.NitricOxide, 0) = isnull(source.NitricOxide, 0)
		and isnull(target.ECMO, 0) = isnull(source.ECMO, 0)
		and isnull(target.Apnoea, 0) = isnull(source.Apnoea, 0)
		and isnull(target.Pneumothorax, 0) = isnull(source.Pneumothorax, 0)
		and isnull(target.ChestDrain, 0) = isnull(source.ChestDrain, 0)
		and isnull(target.TracheostomyCode, 0) = isnull(source.TracheostomyCode, 0)
		and isnull(target.TracheostomyCarerCode, 0) = isnull(source.TracheostomyCarerCode, 0)
		and isnull(target.ReplogleTube, 0) = isnull(source.ReplogleTube, 0)
		and isnull(target.Surfactant, 0) = isnull(source.Surfactant, 0)
		and isnull(target.SurfactantDoses, 0) = isnull(source.SurfactantDoses, 0)
		and isnull(target.SurfactantDoseTotal, 0) = isnull(source.SurfactantDoseTotal, 0)
		and isnull(target.NecrotizingEntercolitisTreatmentCode, 0) = isnull(source.NecrotizingEntercolitisTreatmentCode, 0)
		and isnull(target.InotropesToday, 0) = isnull(source.InotropesToday, 0)
		and isnull(target.ProstinToday, 0) = isnull(source.ProstinToday, 0)
		and isnull(target.PulmonaryVasodilator, 0) = isnull(source.PulmonaryVasodilator, 0)
		and isnull(target.GastroschisisSilo, 0) = isnull(source.GastroschisisSilo, 0)
		and isnull(target.Stoma, 0) = isnull(source.Stoma, 0)
		and isnull(target.RectalWashout, 0) = isnull(source.RectalWashout, 0)
		and isnull(target.Peritonealdialysis, 0) = isnull(source.Peritonealdialysis, 0)
		and isnull(target.Haemofiltration, 0) = isnull(source.Haemofiltration, 0)
		and isnull(target.TAT, 0) = isnull(source.TAT, 0)
		and isnull(target.Tone, 0) = isnull(source.Tone, 0)
		and isnull(target.ConsciousnessCode, 0) = isnull(source.ConsciousnessCode, 0)
		and isnull(target.Convulsions, 0) = isnull(source.Convulsions, 0)
		and isnull(target.NASTreatment, 0) = isnull(source.NASTreatment, 0)
		and isnull(target.NASDrugTreatment, 0) = isnull(source.NASDrugTreatment, 0)
		and isnull(target.NASScore, '') = isnull(source.NASScore, '')
		and isnull(target.VPShuntSurgery, 0) = isnull(source.VPShuntSurgery, 0)
		and isnull(target.EEG, 0) = isnull(source.EEG, 0)
		and isnull(target.Cooling, 0) = isnull(source.Cooling, 0)
		and isnull(target.EpiduralCatheter, 0) = isnull(source.EpiduralCatheter, 0)
		and isnull(target.VentricularDrain, 0) = isnull(source.VentricularDrain, 0)
		and isnull(target.VentricularTap, 0) = isnull(source.VentricularTap, 0)
		and isnull(target.ROPScreen, '') = isnull(source.ROPScreen, '')
		and isnull(target.ROPGrade, 0) = isnull(source.ROPGrade, 0)
		and isnull(target.ROPSurgery, 0) = isnull(source.ROPSurgery, 0)
		and isnull(target.MaxBilirubin, 0) = isnull(source.MaxBilirubin, 0)
		and isnull(target.Phototherapy, 0) = isnull(source.Phototherapy, 0)
		and isnull(target.Immunoglbulins, 0) = isnull(source.Immunoglbulins, 0)
		and isnull(target.UrinaryCtaheter, 0) = isnull(source.UrinaryCtaheter, 0)
		and isnull(target.TPN, 0) = isnull(source.TPN, 0)
		and isnull(target.GlucoseElectrolyte, 0) = isnull(source.GlucoseElectrolyte, 0)
		and isnull(target.EnteralFeedCode, '') = isnull(source.EnteralFeedCode, '')
		and isnull(target.VolumeMilk, 0) = isnull(source.VolumeMilk, 0)
		and isnull(target.FormulaCode, '') = isnull(source.FormulaCode, '')
		and isnull(target.FeedingMethodCode, '') = isnull(source.FeedingMethodCode, '')
		and isnull(target.AdditiveCode, '') = isnull(source.AdditiveCode, '')
		and isnull(target.LinesInSitu, '') = isnull(source.LinesInSitu, '')
		and isnull(target.ExchangeFull, 0) = isnull(source.ExchangeFull, 0)
		and isnull(target.ExchangePartial, 0) = isnull(source.ExchangePartial, 0)
		and isnull(target.BloodProductCode, '') = isnull(source.BloodProductCode, '')
		and isnull(target.DiagnosesDay, '') = isnull(source.DiagnosesDay, '')
		and isnull(target.DrugsDay, '') = isnull(source.DrugsDay, '')
		and isnull(target.IVDrugsToday, 0) = isnull(source.IVDrugsToday, 0)
		and isnull(target.SkinToSkinToday, 0) = isnull(source.SkinToSkinToday, 0)
		and isnull(target.PDAToday, '') = isnull(source.PDAToday, '')
		and isnull(target.TransportedToday, 0) = isnull(source.TransportedToday, 0)
		and isnull(target.CVSTreatmentToday, '') = isnull(source.CVSTreatmentToday, '')
		and isnull(target.InfectionDiagnosis, '') = isnull(source.InfectionDiagnosis, '')
		and isnull(target.JaundiceDiagnosed, '') = isnull(source.JaundiceDiagnosed, '')
		and isnull(target.Bilirubin, 0) = isnull(source.Bilirubin, 0)
		and isnull(target.HIEScore, 0) = isnull(source.HIEScore, 0)
		and isnull(target.Probiotics, 0) = isnull(source.Probiotics, 0)
		and isnull(target.MRSASwab, 0) = isnull(source.MRSASwab, 0)
		and isnull(target.ModifiedTime, getdate()) = isnull(source.ModifiedTime, getdate())
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			 target.EpisodeID = source.EpisodeID
			,target.CareDate = source.CareDate
			,target.CareLocationID = source.CareLocationID
			,target.WardCode = source.WardCode
			,target.Weight = source.Weight
			,target.WeightToday = source.WeightToday
			,target.HeadCircumference = source.HeadCircumference
			,target.Length = source.Length
			,target.OneToOneNursing = source.OneToOneNursing
			,target.CarerStatusCode = source.CarerStatusCode
			,target.NursingStatusCode = source.NursingStatusCode
			,target.ObservationsMonitoringCode = source.ObservationsMonitoringCode
			,target.SurgeryToday = source.SurgeryToday
			,target.NeonatalLevelOfCare2001Code = source.NeonatalLevelOfCare2001Code
			,target.NeonatalLevelOfCare2011Code = source.NeonatalLevelOfCare2011Code
			,target.HRGCode = source.HRGCode
			,target.GlasgowScore = source.GlasgowScore
			,target.RespiratorySupportCode = source.RespiratorySupportCode
			,target.OxygenCode = source.OxygenCode
			,target.VentilationModeCode = source.VentilationModeCode
			,target.CPAPModeCode = source.CPAPModeCode
			,target.NPAirway = source.NPAirway
			,target.NitricOxide = source.NitricOxide
			,target.ECMO = source.ECMO
			,target.Apnoea = source.Apnoea
			,target.Pneumothorax = source.Pneumothorax
			,target.ChestDrain = source.ChestDrain
			,target.TracheostomyCode = source.TracheostomyCode
			,target.TracheostomyCarerCode = source.TracheostomyCarerCode
			,target.ReplogleTube = source.ReplogleTube
			,target.Surfactant = source.Surfactant
			,target.SurfactantDoses = source.SurfactantDoses
			,target.SurfactantDoseTotal = source.SurfactantDoseTotal
			,target.NecrotizingEntercolitisTreatmentCode = source.NecrotizingEntercolitisTreatmentCode
			,target.InotropesToday = source.InotropesToday
			,target.ProstinToday = source.ProstinToday
			,target.PulmonaryVasodilator = source.PulmonaryVasodilator
			,target.GastroschisisSilo = source.GastroschisisSilo
			,target.Stoma = source.Stoma
			,target.RectalWashout = source.RectalWashout
			,target.Peritonealdialysis = source.Peritonealdialysis
			,target.Haemofiltration = source.Haemofiltration
			,target.TAT = source.TAT
			,target.Tone = source.Tone
			,target.ConsciousnessCode = source.ConsciousnessCode
			,target.Convulsions = source.Convulsions
			,target.NASTreatment = source.NASTreatment
			,target.NASDrugTreatment = source.NASDrugTreatment
			,target.NASScore = source.NASScore
			,target.VPShuntSurgery = source.VPShuntSurgery
			,target.EEG = source.EEG
			,target.Cooling = source.Cooling
			,target.EpiduralCatheter = source.EpiduralCatheter
			,target.VentricularDrain = source.VentricularDrain
			,target.VentricularTap = source.VentricularTap
			,target.ROPScreen = source.ROPScreen
			,target.ROPGrade = source.ROPGrade
			,target.ROPSurgery = source.ROPSurgery
			,target.MaxBilirubin = source.MaxBilirubin
			,target.Phototherapy = source.Phototherapy
			,target.Immunoglbulins = source.Immunoglbulins
			,target.UrinaryCtaheter = source.UrinaryCtaheter
			,target.TPN = source.TPN
			,target.GlucoseElectrolyte = source.GlucoseElectrolyte
			,target.EnteralFeedCode = source.EnteralFeedCode
			,target.VolumeMilk = source.VolumeMilk
			,target.FormulaCode = source.FormulaCode
			,target.FeedingMethodCode = source.FeedingMethodCode
			,target.AdditiveCode = source.AdditiveCode
			,target.LinesInSitu = source.LinesInSitu
			,target.ExchangeFull = source.ExchangeFull
			,target.ExchangePartial = source.ExchangePartial
			,target.BloodProductCode = source.BloodProductCode
			,target.DiagnosesDay = source.DiagnosesDay
			,target.DrugsDay = source.DrugsDay
			,target.IVDrugsToday = source.IVDrugsToday
			,target.SkinToSkinToday = source.SkinToSkinToday
			,target.PDAToday = source.PDAToday
			,target.TransportedToday = source.TransportedToday
			,target.CVSTreatmentToday = source.CVSTreatmentToday
			,target.InfectionDiagnosis = source.InfectionDiagnosis
			,target.JaundiceDiagnosed = source.JaundiceDiagnosed
			,target.Bilirubin = source.Bilirubin
			,target.HIEScore = source.HIEScore
			,target.Probiotics = source.Probiotics
			,target.MRSASwab = source.MRSASwab
			,target.ModifiedTime = source.ModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime










﻿

/******************************************************************************
**  Name: ETL.p_InsertSSISRunPackageNotification
**  Purpose: Insert a row to ETL.SSISRunPackageNotifications table
**
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
** 19.10.12   MH          Brought into source control
******************************************************************************/

CREATE PROCEDURE [ETL].[p_InsertSSISRunPackageNotification]
	         @ImportFileName		VARCHAR(100)
            ,@PackageName			VARCHAR(50)
            ,@NotificationType		VARCHAR(20)
            ,@NotificationMessage	VARCHAR(MAX)

AS
BEGIN

	INSERT INTO ETL.SSISRunPackageNotifications
	(
		ImportFileName
	   ,CreatedDate	
       ,PackageName
       ,NotificationType
       ,NotificationMessage
	)
	VALUES
	(
		@ImportFileName	
	   ,GETDATE()
       ,@PackageName
       ,@NotificationType
       ,@NotificationMessage	
	)
END


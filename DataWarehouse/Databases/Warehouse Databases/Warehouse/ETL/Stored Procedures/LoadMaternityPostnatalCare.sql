﻿



CREATE proc [ETL].[LoadMaternityPostnatalCare]

as

set dateformat dmy


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, DischargedToHealthVisitorDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, DischargedToHealthVisitorDate)) 
from
	ETL.TLoadMaternityPostnatalCare

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Maternity.PostnatalCare target
using
	(
	select
		 SourceUniqueID
		,CasenoteNumber
		,NHSNumber
		,DateOfBirth
		,AgeAtBooking
		,GPPracticeCode
		,DischargedToHealthVisitorDate
		,HIV
		,RenalDisease
		,Renal
		,RHIsoimmunisationCode
		,DiabeticCode
		,DiabetesCode
		,EndocrineCode
		,AntenatalEndocrineCode
		,DiabetesEndocrine
		,HeartDisease
		,CardiacCode
		,CardiacComplications
		,NumberOfFetuses
		,InheritedDiseases
		,PsychiatricProblemCode
		,BMIAtBooking
		,Interpreter
		,PatientTypeCode
		,SpecialistMidwifeReferral
		,AccomodationTypeCode
		,DrugCode
		,HypertensionComplications
		,Thromboembolism
		,DVT
		,HELLP
		,Preeclampsia
		,HELLPPreeclampsia
		,Eclampsia
		,NeonatalDeathStillBirth
		,HRGCode
		,ModifiedTime
		,InterfaceCode
	from
		ETL.TLoadMaternityPostnatalCare
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	

	when not matched by source
	and	target.DischargedToHealthVisitorDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CasenoteNumber
			,NHSNumber
			,DateOfBirth
			,AgeAtBooking
			,GPPracticeCode
			,DischargedToHealthVisitorDate
			,HIV
			,RenalDisease
			,Renal
			,RHIsoimmunisationCode
			,DiabeticCode
			,DiabetesCode
			,EndocrineCode
			,AntenatalEndocrineCode
			,DiabetesEndocrine
			,HeartDisease
			,CardiacCode
			,CardiacComplications
			,NumberOfFetuses
			,InheritedDiseases
			,PsychiatricProblemCode
			,BMIAtBooking
			,Interpreter
			,PatientTypeCode
			,SpecialistMidwifeReferral
			,AccomodationTypeCode
			,DrugCode
			,HypertensionComplications
			,Thromboembolism
			,DVT
			,HELLP
			,Preeclampsia
			,HELLPPreeclampsia
			,Eclampsia
			,NeonatalDeathStillBirth
			,HRGCode
			,ModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
		  )
		values
			(
			 source.SourceUniqueID
			,source.CasenoteNumber
			,source.NHSNumber
			,source.DateOfBirth
			,source.AgeAtBooking
			,source.GPPracticeCode
			,source.DischargedToHealthVisitorDate
			,source.HIV
			,source.RenalDisease
			,source.Renal
			,source.RHIsoimmunisationCode
			,source.DiabeticCode
			,source.DiabetesCode
			,source.EndocrineCode
			,source.AntenatalEndocrineCode
			,source.DiabetesEndocrine
			,source.HeartDisease
			,source.CardiacCode
			,source.CardiacComplications
			,source.NumberOfFetuses
			,source.InheritedDiseases
			,source.PsychiatricProblemCode
			,source.BMIAtBooking
			,source.Interpreter
			,source.PatientTypeCode
			,source.SpecialistMidwifeReferral
			,source.AccomodationTypeCode
			,source.DrugCode
			,source.HypertensionComplications
			,source.Thromboembolism
			,source.DVT
			,source.HELLP
			,source.Preeclampsia
			,source.HELLPPreeclampsia
			,source.Eclampsia
			,source.NeonatalDeathStillBirth
			,source.HRGCode
			,source.ModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.AgeAtBooking, '') = isnull(source.AgeAtBooking, '')
		and isnull(target.GPPracticeCode, '') = isnull(source.GPPracticeCode, '')
		and isnull(target.DischargedToHealthVisitorDate, getdate()) = isnull(source.DischargedToHealthVisitorDate, getdate())
		and isnull(target.HIV, '') = isnull(source.HIV, '')
		and isnull(target.RenalDisease, '') = isnull(source.RenalDisease, '')
		and isnull(target.Renal, '') = isnull(source.Renal, '')
		and isnull(target.RHIsoimmunisationCode, '') = isnull(source.RHIsoimmunisationCode, '')
		and isnull(target.DiabeticCode, '') = isnull(source.DiabeticCode, '')
		and isnull(target.DiabetesCode, '') = isnull(source.DiabetesCode, '')
		and isnull(target.EndocrineCode, '') = isnull(source.EndocrineCode, '')
		and isnull(target.AntenatalEndocrineCode, '') = isnull(source.AntenatalEndocrineCode, '')
		and isnull(target.DiabetesEndocrine, 0) = isnull(source.DiabetesEndocrine, 0)
		and isnull(target.HeartDisease, '') = isnull(source.HeartDisease, '')
		and isnull(target.CardiacCode, '') = isnull(source.CardiacCode, '')
		and isnull(target.CardiacComplications, 0) = isnull(source.CardiacComplications, 0)
		and isnull(target.NumberOfFetuses, '') = isnull(source.NumberOfFetuses, '')
		and isnull(target.InheritedDiseases, '') = isnull(source.InheritedDiseases, '')
		and isnull(target.PsychiatricProblemCode, '') = isnull(source.PsychiatricProblemCode, '')
		and isnull(target.BMIAtBooking, '') = isnull(source.BMIAtBooking, '')
		and isnull(target.Interpreter, '') = isnull(source.Interpreter, '')
		and isnull(target.PatientTypeCode, '') = isnull(source.PatientTypeCode, '')
		and isnull(target.SpecialistMidwifeReferral, '') = isnull(source.SpecialistMidwifeReferral, '')
		and isnull(target.AccomodationTypeCode, '') = isnull(source.AccomodationTypeCode, '')
		and isnull(target.DrugCode, '') = isnull(source.DrugCode, '')
		and isnull(target.HypertensionComplications, 0) = isnull(source.HypertensionComplications, 0)
		and isnull(target.Thromboembolism, '') = isnull(source.Thromboembolism, '')
		and isnull(target.DVT, 0) = isnull(source.DVT, 0)
		and isnull(target.HELLP, '') = isnull(source.HELLP, '')
		and isnull(target.Preeclampsia, '') = isnull(source.Preeclampsia, '')
		and isnull(target.HELLPPreeclampsia, 0) = isnull(source.HELLPPreeclampsia, 0)
		and isnull(target.Eclampsia, '') = isnull(source.Eclampsia, '')
		and isnull(target.NeonatalDeathStillBirth, 0) = isnull(source.NeonatalDeathStillBirth, 0)
		and isnull(target.HRGCode, '') = isnull(source.HRGCode, '')
		and isnull(target.ModifiedTime, '') = isnull(source.ModifiedTime, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.AgeAtBooking = source.AgeAtBooking
			,target.GPPracticeCode = source.GPPracticeCode
			,target.DischargedToHealthVisitorDate = source.DischargedToHealthVisitorDate
			,target.HIV = source.HIV
			,target.RenalDisease = source.RenalDisease
			,target.Renal = source.Renal
			,target.RHIsoimmunisationCode = source.RHIsoimmunisationCode
			,target.DiabeticCode = source.DiabeticCode
			,target.DiabetesCode = source.DiabetesCode
			,target.EndocrineCode = source.EndocrineCode
			,target.AntenatalEndocrineCode = source.AntenatalEndocrineCode
			,target.DiabetesEndocrine = source.DiabetesEndocrine
			,target.HeartDisease = source.HeartDisease
			,target.CardiacCode = source.CardiacCode
			,target.CardiacComplications = source.CardiacComplications
			,target.NumberOfFetuses = source.NumberOfFetuses
			,target.InheritedDiseases = source.InheritedDiseases
			,target.PsychiatricProblemCode = source.PsychiatricProblemCode
			,target.BMIAtBooking = source.BMIAtBooking
			,target.Interpreter = source.Interpreter
			,target.PatientTypeCode = source.PatientTypeCode
			,target.SpecialistMidwifeReferral = source.SpecialistMidwifeReferral
			,target.AccomodationTypeCode = source.AccomodationTypeCode
			,target.DrugCode = source.DrugCode
			,target.HypertensionComplications = source.HypertensionComplications
			,target.Thromboembolism = source.Thromboembolism
			,target.DVT = source.DVT
			,target.HELLP = source.HELLP
			,target.Preeclampsia = source.Preeclampsia
			,target.HELLPPreeclampsia = source.HELLPPreeclampsia
			,target.Eclampsia = source.Eclampsia
			,target.NeonatalDeathStillBirth = source.NeonatalDeathStillBirth
			,target.HRGCode = source.HRGCode
			,target.ModifiedTime = source.ModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary
;
select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




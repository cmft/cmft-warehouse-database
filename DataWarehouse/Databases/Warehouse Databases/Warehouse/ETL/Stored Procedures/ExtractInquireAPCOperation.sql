﻿CREATE procedure [ETL].[ExtractInquireAPCOperation]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

--select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)
select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)


update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSAPCOPERATION'


insert into ETL.TImportAPCOperation
(
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,APCSourceUniqueID
)


SELECT 
	 SourceUniqueID = CONSEPISPROC.CONSEPISPROCID 
	,SourceSpellNo = CONSEPISPROC.EpisodeNumber 
	,SourcePatientNo = CONSEPISPROC.InternalPatientNumber 
	,SourceEncounterNo = CONSEPISPROC.FCENumber 
	,SequenceNo = CONSEPISPROC.SecondaryProcSeqNo 
	,OperationCode = CONSEPISPROC.OPCS 
	,OperationDate = CONSEPISPROC.ProcedureDate 
	,ConsultantCode = CONSEPISPROC.DoctorPerfOp 
	,APCSourceUniqueID = FCE.FCEEXTID 
FROM
	[$(PAS)].Inquire.FCEEXT FCE

INNER JOIN [$(PAS)].Inquire.CONSEPISPROC
ON	FCE.EpisodeNumber = CONSEPISPROC.EpisodeNumber
AND	FCE.InternalPatientNumber = CONSEPISPROC.InternalPatientNumber
AND	FCE.FceSequenceNo = CONSEPISPROC.FCENumber
AND	FCE.EpsActvDtimeInt = CONSEPISPROC.FCEEndDateTimeInt

WHERE
	FCE.EpsActvDtimeInt between @from and @to


union all


SELECT 
	 SourceUniqueID = CONSEPISPROC.CONSEPISPROCID 
	,SourceSpellNo = CONSEPISPROC.EpisodeNumber 
	,SourcePatientNo = CONSEPISPROC.InternalPatientNumber 
	,SourceEncounterNo = CONSEPISPROC.FCENumber 
	,SequenceNo = CONSEPISPROC.SecondaryProcSeqNo 
	,OperationCode = CONSEPISPROC.OPCS 
	,OperationDate = CONSEPISPROC.ProcedureDate 
	,ConsultantCode = CONSEPISPROC.DoctorPerfOp 
	,APCSourceUniqueID = Episode.CONSEPISODEID 
FROM
	[$(PAS)].Inquire.CONSEPISPROC

INNER JOIN [$(PAS)].Inquire.CONSEPISODE Episode 
ON	Episode.EpisodeNumber = CONSEPISPROC.EpisodeNumber
AND	Episode.InternalPatientNumber = CONSEPISPROC.InternalPatientNumber
AND	Episode.FCENumber = CONSEPISPROC.FCENumber

INNER JOIN [$(PAS)].Inquire.ADMITDISCH AdmitDisch 
ON	AdmitDisch.EpisodeNumber = Episode.EpisodeNumber
AND	AdmitDisch.InternalPatientNumber = Episode.InternalPatientNumber

WHERE
	AdmitDisch.IpAdmDtimeInt <= @to
AND	(
		AdmitDisch.IpDschDtimeInt > @to
	or	AdmitDisch.IpDschDtimeInt Is Null
	)
and	not exists
	(
	select
		1
	from
		[$(PAS)].Inquire.FCEEXT FCE
	where
		FCE.EpisodeNumber = CONSEPISPROC.EpisodeNumber
	AND	FCE.InternalPatientNumber = CONSEPISPROC.InternalPatientNumber
	AND	FCE.FceSequenceNo = CONSEPISPROC.FCENumber
	AND	FCE.EpsActvDtimeInt = CONSEPISPROC.FCEEndDateTimeInt
	)


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

if @RowsInserted != 0
begin
	update Utility.Parameter set NumericValue = 1 where Parameter = 'EXTRACTSTATUSAPCOPERATION'
end

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireAPCOperation', @Stats, @StartTime

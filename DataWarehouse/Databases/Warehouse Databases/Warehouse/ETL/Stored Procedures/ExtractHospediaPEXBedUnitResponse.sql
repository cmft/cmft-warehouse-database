﻿

CREATE proc [ETL].[ExtractHospediaPEXBedUnitResponse]

 @FromDate smalldatetime = null
, @ToDate smalldatetime = null

as

/* 
==============================================================================================
Description:

When		Who			What
20150506	Paul Egan	Excluded problem record until Colin Hunter returns from leave.
						The record has two answers for the same question, causing job failure.
						Hospedia confirmed this is genuine data entry and not a duplicate error.
===============================================================================================
*/




set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
select @StartTime = getdate()

select @RowsInserted = 0

truncate table 
	ETL.TImportPEXBedUnitResponse

insert into	
	ETL.TImportPEXBedUnitResponse

(
	SurveyTakenID
	,SurveyDate
	,SurveyTime
	,LocationID
	,SurveyID 
	,QuestionID
	,QuestionNumber
	,AnswerID
	,Answer
	,OptionNo
	,ResponseDate
	,ResponseTime
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
)

select 

	SurveyTakenID = cast(SurveyTakenID as bigint)
	,SurveyDate
	,SurveyTime
	,LocationID
	,SurveyID 
	,QuestionID
	,QuestionNumber
	,AnswerID = cast(AnswerID as bigint)
	,Answer
	,OptionNo
	,ResponseDate
	,ResponseTime
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
from
	(
	select
		SurveyTakenID = replace(SurveySession.SessionID,'-','')
		,SurveyDate = cast([SurveyDate] as date)
		,SurveyTime = SurveyDate
		,LocationID = wardIdentifier
		,SurveyID = surveyIdentifier
		,QuestionID = SurveyQuestions.questionIdentifier
		,QuestionNumber = QuestionNo
		--,Question
		,AnswerID = answerIdentifier
		,OptionNo
		,Answer
		,ResponseDate = cast(ResponseDate as date)
		,ResponseTime = ResponseDate	
		,ChannelID = 1
		,QuestionTypeID = QuestionType.XrefEntityCode
		,LocationTypeID = 2
		
	from
		[$(SmallDatasets)].Hospedia.SurveySession

	inner join
		[$(SmallDatasets)].Hospedia.SurveyQuestions
	on	SurveyQuestions.SessionID = SurveySession.SessionID 

	inner join
		[$(SmallDatasets)].Hospedia.SurveyResponse
	on	SurveyResponse.SessionID = SurveyQuestions.SessionID 
	and	SurveyResponse.questionIdentifier = SurveyQuestions.questionIdentifier

	left join 
		dbo.EntityXref QuestionType
	on	QuestionType.EntityCode = SurveyQuestions.questionIdentifier
	and QuestionType.EntityTypeCode = 'PEXQUESTION'
	  
	where
		[ResponseDate] between @FromDate and @ToDate

	) Response
	
	where
		SurveyTakenID not in
			(
			 '26201402519130x'
			 ,'262903371430561451'  -- Added Paul Egan 20150506 temp fix
			 )


select @RowsInserted = @@rowcount

-- Calculate stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	


﻿CREATE PROCEDURE [ETL].[LoadPASDelivery]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


truncate table PAS.Delivery

INSERT INTO PAS.Delivery
(
	 DeliveryID
	,Stage1LabourLength
	,Stage2LabourLength
	,AnaestheticGivenDuringCode
	,EpisodeEndTime
	,EpisodeStartTime
	,SourceSpellNo
	,EpisodeNo
	,FirstAnteNatalAssessmentDate
	,GestationLength
	,SourcePatientNo
	,AnaestheticGivenReasonCode
	,AnaestheticTypeCode
	,AnaestheticGivenDate
	,LabourOnsetMethodCode
	,IntendedDeliveryPlaceTypeCode
	,ActualDeliveryPlaceTypeCode
	,DeliveryPlaceChangeReasonCode
	,StatusOfPersonConductingDeliveryCode
	,Parity
	,PreviousPregnancies
) 
select
	 DeliveryID
	,Stage1LabourLength
	,Stage2LabourLength
	,AnaestheticGivenDuringCode
	,EpisodeEndTime
	,EpisodeStartTime
	,SourceSpellNo
	,EpisodeNo
	,FirstAnteNatalAssessmentDate
	,GestationLength
	,SourcePatientNo
	,AnaestheticGivenReasonCode
	,AnaestheticTypeCode
	,AnaestheticGivenDate
	,LabourOnsetMethodCode
	,IntendedDeliveryPlaceTypeCode
	,ActualDeliveryPlaceTypeCode
	,DeliveryPlaceChangeReasonCode
	,StatusOfPersonConductingDeliveryCode
	,Parity
	,PreviousPregnancies
from
	ETL.TLoadPASDelivery

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadPASDelivery', @Stats, @StartTime

print @Stats

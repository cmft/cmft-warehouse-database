﻿





CREATE proc [ETL].[LoadCHAMPAccount] as

/*
When		Who			What
=========================================================================================
24/02/2015	Paul Egan	Initial Coding
=========================================================================================
*/


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
;

declare
	 @deleted int
	,@inserted int
	,@updated int
;

declare
	@MergeSummary TABLE(Action nvarchar(10))
;


merge
	COM.CHAMPAccount target
using
	(
	select 
		 AccountID
		,ContactId
		,AccountCreatedTime
		,Account
		,ContactFullName
		,ContactFirstName
		,ContactLastName
	from 
		ETL.TLoadCHAMPAccount
	) source
	on	source.AccountID = target.AccountID

	when not matched by source
	
	then delete
	
	when not matched
	then
		insert
			(
			 AccountID
			,ContactID
			,AccountCreatedTime
			,Account
			,ContactFullName
			,ContactFirstName
			,ContactLastName
			,Created
			,Updated
			,ByWhom
           )
		values
			(
			 source.AccountID
			,source.ContactId
			,source.AccountCreatedTime
			,source.Account
			,source.ContactFullName
			,source.ContactFirstName
			,source.ContactLastName
			,getdate()
			,getdate()
			,suser_name()
		)
		
	when matched
	and not
		(
			isnull(target.ContactID, '') = isnull(source.ContactId, '')
		and isnull(target.AccountCreatedTime, '19000101') = isnull(source.AccountCreatedTime, '19000101')
		and isnull(target.Account, '') = isnull(source.Account, '')
		and isnull(target.ContactFullName, '') = isnull(source.ContactFullName, '')
		and isnull(target.ContactFirstName, '') = isnull(source.ContactFirstName, '')
		and isnull(target.ContactLastName, '') = isnull(source.ContactLastName, '')
		)
	then
		update
		set
			target.ContactID = source.ContactId
			,target.AccountCreatedTime = source.AccountCreatedTime
			,target.Account = source.Account
			,target.ContactFullName = source.ContactFullName
			,target.ContactFirstName = source.ContactFirstName
			,target.ContactLastName = source.ContactLastName
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime







﻿CREATE   procedure [ETL].[LoadRADWL] as

	/******************************************************************************
	**  Name: LoadRADWL
	**  Purpose: 
	**
	**  Calling stored procedure for importing Radiology PTL data
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 26.06.12     MH        Altered for CMFT environment
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

declare @ImportDateTimeStamp datetime

select @StartTime = getdate()


DECLARE ImportCursor CURSOR FOR
	SELECT DISTINCT
		ImportDateTimeStamp
	FROM
		ETL.TLoadRADWaitingList
	ORDER BY
		ImportDateTimeStamp


OPEN ImportCursor  
FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp

WHILE @@FETCH_STATUS = 0
BEGIN
	exec ETL.LoadRADWaitingList @ImportDateTimeStamp
	FETCH NEXT FROM ImportCursor INTO @ImportDateTimeStamp
END
  
CLOSE ImportCursor
DEALLOCATE ImportCursor


	-- reset snapshot table
	delete from RAD.Snapshot

	insert into RAD.Snapshot 
		(
		CensusDate
		)
	select distinct 
		CensusDate
	from 
		RAD.WaitingList


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadRADWL', @Stats, @StartTime

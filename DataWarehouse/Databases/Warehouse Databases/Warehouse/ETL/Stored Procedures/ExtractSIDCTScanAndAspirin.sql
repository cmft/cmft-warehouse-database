﻿CREATE procedure [ETL].[ExtractSIDCTScanAndAspirin]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDCTScanAndAspirin;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDCTScanAndAspirin
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,DidCTScanShowAHaemorrhage
	,PatientHaveASuspectedHeadInjury
	,oPatientHaveASuspectedSubarachnoidHaemorrhage
	,PatientHaveAnAllergyToAspirin
	,HasAspirinAlreadyBeenGiven
	,DateTimeHasAspirinAlreadyBeenGiven
	,NameOfAdministering
	,DateTimeAspirinAdministered
	,PrescriberContactedToWriteUpAsRegularDailyDose
	,DateTimePrescriberContactedToWriteUpAsRegularDailyDose
	,Username
	,ModifiedDate
	,IsSectionComplete
	,SectionCompleteTime
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		CandidateData.SectionID
	,ActionID = 
		CandidateData.ActionID
	,DidCTScanShowAHaemorrhage = 
		nullif(CandidateData.Details.value('(/CTScanAndAspirinDTO/DidCTScanShowAHaemorrhage)[1]', 'varchar(max)'),'')
	,PatientHaveASuspectedHeadInjury = 
		nullif(CandidateData.Details.value('(/CTScanAndAspirinDTO/PatientHaveASuspectedHeadInjury)[1]', 'varchar(max)'),'')
	,oPatientHaveASuspectedSubarachnoidHaemorrhage = 
		nullif(CandidateData.Details.value('(/CTScanAndAspirinDTO/oPatientHaveASuspectedSubarachnoidHaemorrhage)[1]', 'varchar(max)'),'')
	,PatientHaveAnAllergyToAspirin = 
		nullif(CandidateData.Details.value('(/CTScanAndAspirinDTO/PatientHaveAnAllergyToAspirin)[1]', 'varchar(max)'),'')
	,HasAspirinAlreadyBeenGiven = 
		nullif(CandidateData.Details.value('(/CTScanAndAspirinDTO/HasAspirinAlreadyBeenGiven)[1]', 'varchar(max)'),'')
	,DateTimeHasAspirinAlreadyBeenGiven = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeHasAspirinAlreadyBeenGiven)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,NameOfAdministering = 
		nullif(CandidateData.Details.value('(/CTScanAndAspirinDTO/NameOfAdministering)[1]', 'varchar(max)'),'')
	,DateTimeAspirinAdministered = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimeAspirinAdministered)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,PrescriberContactedToWriteUpAsRegularDailyDose = 
		nullif(CandidateData.Details.value('(/CTScanAndAspirinDTO/PrescriberContactedToWriteUpAsRegularDailyDose)[1]', 'varchar(max)'),'')
	,DateTimePrescriberContactedToWriteUpAsRegularDailyDose = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/LikelyStrokeDTO/DateTimePrescriberContactedToWriteUpAsRegularDailyDose)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,Username = 
		CandidateData.Details.value('(/CTScanAndAspirinDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/CTScanAndAspirinDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,IsSectionComplete = 
		SectionComplete.Details.value('(/SectionCompletedDTO/CurrentSelection)[1]', 'varchar(max)')
	,SectionCompleteTime = 
		SectionComplete.DateTimeCreated
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
left outer join 
(
	select
		*
	from
		[$(SID)].dbo.TARNCandidateData 
	where
		SectionID = 17
	and ActionID = 50
) SectionComplete
on SectionComplete.CandidateID = CandidateData.CandidateID
where
	CandidateData.SectionID = 17
and CandidateData.ActionID =99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDCTScanAndAspirin', @Stats, @StartTime





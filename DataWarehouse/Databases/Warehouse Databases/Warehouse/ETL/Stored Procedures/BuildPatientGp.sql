﻿



CREATE proc [ETL].[BuildPatientGp]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@RowsInserted int
	,@Stats varchar(max)

truncate table PAS.PatientGp

insert into PAS.PatientGp
(
	 SourcePatientNo
	,DistrictNo
	,GpPracticeCode
	,GpCode
	,GpHistorySourceUniqueID
	,EffectiveFromDate 
	,EffectiveToDate 
	,IsCurrent
)

select
	 GpHistory.SourcePatientNo
	,GpHistory.DistrictNo
	,GpHistory.Practice
	,GpCode = coalesce(
					Gp.NationalCode
					,'G9999998'
				)
	,GpHistorySourceUniqueID = GpHistory.GpHistoryID
	,EffectiveFromDate = coalesce(
								GpHistory.EffectiveFromDate
								,'01/01/1900'
							)
	,EffectiveToDate = GpHistory.EffectiveToDate
	,IsCurrent = 0
from
	PAS.GpHistory

inner join PAS.Gp
on	 Gp.GpCode = GpHistory.GpCode

where
	GpHistory.Practice is not null
and GpHistory.GpCode is not null
and GpHistory.EffectiveToDate is not null -- remove records where we have more than one gp for date range

SELECT @RowsInserted = @@Rowcount

insert into PAS.PatientGp
(
	 SourcePatientNo
	,DistrictNo
	,GpPracticeCode 
	,GpCode
	,GpSourceUniqueID
	,EffectiveFromDate 
	,EffectiveToDate 
	,IsCurrent
)

select
	 Patient.SourcePatientNo
	,DistrictNo
	,GpPracticeCode = Gp.PracticeCode
	,GpCode = coalesce(
					Gp.NationalCode
					,'G9999998'
				)
	,GpSourceUniqueID = Gp.GpCode
	,EffectiveFromDate =
		coalesce(
			 dateadd(
					 day
					,1 -- add one day to ensure no overlap with previous EffectiveToDate
					,GpHistory.PreviousEffectiveToDate
				)
				,'01/01/1900'
			)
	,EffectiveToDate = null
	,IsCurrent = 1
from
	PAS.Patient 

inner join PAS.Gp
on	Gp.GpCode = Patient.RegisteredGpCode

left join
    (
	select
		 SourcePatientNo
		,PreviousEffectiveToDate = max(EffectiveToDate)
	from
		PAS.PatientGp GpHistory
	group by
		SourcePatientNo
	) GpHistory
on	GpHistory.SourcePatientNo = Patient.SourcePatientNo
	
where
	Gp.PracticeCode is not null
and Patient.RegisteredGpCode is not null

	
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


SELECT @RowsInserted = @RowsInserted + @@Rowcount

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) +
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent @ProcedureName, @Stats, @StartTime






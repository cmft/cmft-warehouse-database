﻿CREATE procedure [ETL].[LoadMaternity] 

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null

as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @FromDate = coalesce(@FromDate, (select cast(dateadd(month, -12, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

truncate table ETL.TImportPASBirth
exec ETL.ExtractInquireBirth
exec ETL.LoadPASBirth

truncate table ETL.TImportPASDelivery
exec ETL.ExtractInquireDelivery
exec ETL.LoadPASDelivery

truncate table ETL.TImportPASMaternityAnaesthetic
exec ETL.ExtractInquireMaternityAnaesthetic
exec ETL.LoadPASMaternityAnaesthetic

truncate table ETL.TImportPASMaternityClinicDetail
exec ETL.ExtractInquireMaternityClinicDetail
exec ETL.LoadPASMaternityClinicDetail

truncate table ETL.TImportPASNeonate
exec ETL.ExtractInquireNeonate
exec ETL.LoadPASNeonate

truncate table ETL.MaternityAntenatalCare
exec ETL.ExtractMaternityAntenatalCare @FromDate, @ToDate
exec ETL.LoadMaternityAntenatalCare


truncate table ETL.MaternityPostnatalCare
exec ETL.ExtractMaternityPostnatalCare  @FromDate, @ToDate
exec ETL.LoadMaternityPostnatalCare

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

print @Stats

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

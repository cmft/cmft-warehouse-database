﻿
CREATE procedure [ETL].[LoadIUIReferenceData] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


insert IUI.Outcome
(
Outcome
)

select distinct
	Outcome
from
	[$(SmallDatasets)].IUI.baseData
where
	not exists
			(
			select
				1
			from
				 IUI.Outcome
			where
				Outcome.Outcome = baseData.Outcome
			)
and	Outcome is not null


insert IUI.Treatment
(
Treatment
,Type
)
select distinct
	TreatType
	,[$(CMFT)].String.ProperCase(Type)
from
	[$(SmallDatasets)].IUI.baseData
where
	not exists
			(
			select
				1
			from
				IUI.Treatment
			where
				Treatment.Treatment = baseData.TreatType
			)
select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


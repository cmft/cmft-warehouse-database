﻿
CREATE procedure [ETL].[LoadStaffingLevel]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = object_schema_name(@@procid) + '.' + object_name(@@procid)

select @StartTime = getdate()

exec ETL.ExtractInformationStaffingLevelAPC;
exec ETL.LoadStaffingLevelAPCReferenceData;
exec ETL.LoadStaffingLevelAPC;

SELECT @Elapsed = datediff(minute,@StartTime,getdate())

SELECT @Stats = 
	'Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

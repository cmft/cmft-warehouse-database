﻿




CREATE proc [ETL].[ExtractMedisecDictationDocument]

	@fromDate smalldatetime = null
	,@toDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(day, -30, getdate()))), 0), 112) 

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, 0, getdate()))), 0), 112)




insert into ETL.TImportMedisecDictationDocument

(
	SourceUniqueID
	,SourcePatientNo
	,DocumentID
	,DocumentDescription
	,DocumentTypeCode
	,AuthorCode
	,TypedTime
	,DictatedTime
	,DictatedByCode
	,IssuedTime
	,IssuedByCode
	,SignedTime
	,SignedByCode
	,SignedStatusCode
	,DepartmentCode
	,DocumentXMLTemplateCode
	,DocumentXML
	,TransmissionTime
	,TransmissionDestinationCode
)

select --top 10000
	SourceUniqueID
	,SourcePatientNo
	,Document.DocumentID
	,DocumentDescription
	,DocumentTypeCode
	,AuthorCode
	,TypedTime
	,DictatedTime 
	,DictatedByCode 
	,IssuedTime 
	,IssuedByCode 
	,SignedTime 
	,SignedByCode 
	,SignedStatusCode 
	,DepartmentCode
	,DocumentXMLTemplateCode 
	,DocumentXML 
	,TransmissionTime = Hub.DateTimeSentToHub
	,TransmissionDestinationCode = 
								case 
								when Hub.DocumentID is not null then 'HUB'
								else null
								end
from
	(
	select
		SourceUniqueID = dsid
		,SourcePatientNo = cast(dspn as int)
		,DocumentID = rtrim(dsdocname)
		,DocumentDescription = rtrim(dsdescription)
		,DocumentTypeCode = dstype
		,AuthorCode = dsauthor
		,TypedTime = dstyped
		,DictatedTime = dsdictated
		,DictatedByCode = 
					case
					when len(dsdictatedby) = 0 then null
					else dsdictatedby
					end
		,IssuedTime = dsissued
		,IssuedByCode = dsissuedby
		,SignedTime = dssigned
		,SignedByCode = dssignedby
		,SignedStatusCode = dssignstatus
		,DepartmentCode = rtrim(dsdept)
		,DocumentXMLTemplateCode = gdsxdscode
		,DocumentXML = cast(gdsxxml as xml)
	from
		[$(CMFT_MEDISEC)].dbo.DocSumm DocumentSummary

	/* this join needed to bring in XML for DNFs. Only way to link DNF XML is via InpRequest table  
	10.2.15 perhaps not - testing code from Dan - produces multiple records for some documents */
	
	--left join
	--	(
	--	select
	--		ireqpn
	--		,ireqdoa
	--		,ireqfb
	--		,ireqcomment
	--	from
	--		[CMFT-MEDISEC].dbo.InpRequest InpatientRequest
	--	where
	--		not exists
	--			-- takes last ireqfb in event of duplicates - confirmed with Liam Orrell					
	--			(
	--			select
	--				1
	--			from
	--				[CMFT-MEDISEC].dbo.InpRequest EarlierInpatientRequest
	--			where
	--				EarlierInpatientRequest.ireqpn = InpatientRequest.ireqpn
	--			and	EarlierInpatientRequest.ireqdoa = InpatientRequest.ireqdoa
	--			and	EarlierInpatientRequest.ireqcomment = InpatientRequest.ireqcomment
	--			and	cast(EarlierInpatientRequest.ireqfb as int) > cast(InpatientRequest.ireqfb as int)
	--			)
				
	--	and	not exists
	--		-- takes last ireqdoa in event of duplicates - confirmed with Liam Orrell
	--			(
	--			select
	--				1
	--			from
	--				[CMFT-MEDISEC].dbo.InpRequest EarlierInpatientRequest
	--			where
	--				EarlierInpatientRequest.ireqpn = InpatientRequest.ireqpn
	--			and	EarlierInpatientRequest.ireqdoa > InpatientRequest.ireqdoa
	--			and	EarlierInpatientRequest.ireqcomment = InpatientRequest.ireqcomment
	--			)      
	--	) InpatientRequest
		
	--on	DocumentSummary.dsdocname = InpatientRequest.ireqcomment
	
	left join
		(
		select
			gdsxpn
			,gdsxdoa -- = cast(gdsxdoa as datetime)
			,gdsxfb
			,gdsxdscode
			,gdsxxml
		from
			[$(CMFT_MEDISEC)].dbo.GDSXML DocumentXML
		where
			-- handful of records which have one record in docsum but link 
			-- to more than one in GDSXML. Liam Orrell has checked and only one 
			-- document available to user in front end. After some spot checking 
			-- it appears to be presenting the first record based on the ordering brought
			-- about by the primary key. The block below replicates that (based on template!?)
			not exists
				(
				select
					1
				from
					[$(CMFT_MEDISEC)].dbo.GDSXML DocumentXMLEarlier
				where
					DocumentXMLEarlier.gdsxpn = DocumentXML.gdsxpn
				and	DocumentXMLEarlier.gdsxdoa = DocumentXML.gdsxdoa
				and	DocumentXMLEarlier.gdsxfb = DocumentXML.gdsxfb
				and	DocumentXMLEarlier.gdsxdscode < DocumentXML.gdsxdscode
				)
		) DocumentXML

	--on	DocumentXML.gdsxpn = InpatientRequest.ireqpn
	--and DocumentXML.gdsxdoa = InpatientRequest.ireqdoa	
	--and DocumentXML.gdsxfb = InpatientRequest.ireqfb
	--and DocumentXML.gdsxfb = right(ltrim(rtrim(DocumentSummary.dsdocname)),2)

	on	DocumentXML.gdsxpn = DocumentSummary.dspn
	and DocumentXML.gdsxdoa = substring(DocumentSummary.dsdocname,10, 8)
	and DocumentXML.gdsxfb = right(ltrim(rtrim(DocumentSummary.dsdocname)),2)
	and DocumentSummary.dstype = 'F'
						
	where
		DocumentSummary.dstyped between @from and @to

	) Document

left join [$(SmallDatasets)].[EDT].[hubData] Hub
on Document.DocumentID collate database_default = Hub.DocumentID collate database_default
and Hub.DocumentID is not null
and Hub.ContextCode = 'CEN||MEDI'
and not exists
-- Agreed with GC/DG to use the first time letter is sent to the Hub
	(
	select
		1
	from 
		[$(SmallDatasets)].EDT.hubData Earliest
	where 
		Earliest.DocumentID = Hub.DocumentID
	and Earliest.DateTimeSentToHub < Hub.DateTimeSentToHub
	)


select @RowsInserted = @@ROWCOUNT

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
      'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
      'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractMedisecDictationDocument', @Stats, @StartTime












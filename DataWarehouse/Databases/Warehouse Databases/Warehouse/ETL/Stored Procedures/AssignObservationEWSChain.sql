﻿

CREATE proc [ETL].[AssignObservationEWSChain]

as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


if object_id('tempdb.#EWSChain') is not null
drop table #EWSChain

create table #EWSChain
(
	ID int identity (1,1) primary key
	,AdmissionSourceUniqueID int
	,SourceUniqueID int
	,TakenTime datetime
	,OverallRiskIndexCode int
	,ChainID int
)


insert into 
	#EWSChain

select 
	 AdmissionSourceUniqueID 
	,SourceUniqueID
	,TakenTime
	,OverallRiskIndexCode
	,ChainID = null 
from 
	Observation.ObservationSet
	
order by 
	AdmissionSourceUniqueID
	,SourceUniqueID asc
		
/* chain start - EWS score of 3 or more only if no EWS score of 3 or more immediately prior */

update 
	CurrentRecord
set ChainID = CurrentRecord.ID
from 
	#EWSChain CurrentRecord
where 
	CurrentRecord.OverallRiskIndexCode >= 3
and exists
		(
		select
			1
		from
			#EWSChain PreviousRecord
		where
			coalesce(previousrecord.OverallRiskIndexCode, 0) < 3 
		and PreviousRecord.ID = CurrentRecord.ID - 1 
		and PreviousRecord.AdmissionSourceUniqueID = CurrentRecord.AdmissionSourceUniqueID
		)	

/* subsequent EWS score of 3 or more */

update 
	CurrentRecord
set 
	ChainID = 
			(
			select 
				max(PreviousRecord.ChainID)
			from 
				#EWSChain PreviousRecord 
			where 
				PreviousRecord.AdmissionSourceUniqueID = CurrentRecord.AdmissionSourceUniqueID
			and PreviousRecord.SourceUniqueID < CurrentRecord.SourceUniqueID
			)					
from 
	#EWSChain CurrentRecord
where 
	CurrentRecord.OverallRiskIndexCode >= 3
	and CurrentRecord.ChainID is null
	
	
	
/* chain end - allocates next observation to chain following EWS score of 3 or more immediately prior */

update 
	CurrentRecord
set 
	ChainID = 
			(
			select 
				max(PreviousRecord.ChainID)
			from 
				#EWSChain PreviousRecord 
			where 
				PreviousRecord.AdmissionSourceUniqueID = CurrentRecord.AdmissionSourceUniqueID
				and PreviousRecord.SourceUniqueID < CurrentRecord.SourceUniqueID
			)			
from 
	#EWSChain CurrentRecord
where 
	coalesce(CurrentRecord.OverallRiskIndexCode, 0) < 3
	and CurrentRecord.ChainID is null

and exists
		(
		select
			1
		from
			#EWSChain PreviousRecord
		where
			coalesce(PreviousRecord.OverallRiskIndexCode, 0) >= 3 
			and PreviousRecord.ID = CurrentRecord.ID - 1 
			and PreviousRecord.AdmissionSourceUniqueID = CurrentRecord.AdmissionSourceUniqueID
		)		
	
--select top 1000 *
--from #chains
--order by ID desc


/* reset */

update 
	Observation.ObservationSet
set
	AlertChainID = null
	,FirstInAlertChain = null
	,LastInAlertChain = null
	,AlertChainMinutes = null

update ObservationSet

set
	ObservationSet.AlertChainID = Chain.ChainID
from 
	Observation.ObservationSet

inner join #EWSChain Chain
on	Chain.AdmissionSourceUniqueID = ObservationSet.AdmissionSourceUniqueID
and Chain.SourceUniqueID = ObservationSet.SourceUniqueID


update ObservationSet

set
	FirstInAlertChain = 1
from
	Observation.ObservationSet
where
	ObservationSet.SourceUniqueID = 
							(
							select 
								SourceUniqueID = min(SourceUniqueID)
							from 
								Observation.ObservationSet FirstInChain
							where
								FirstInChain.AlertChainID = ObservationSet.AlertChainID
							)

update ObservationSet

set
	LastInAlertChain = 1
from
	Observation.ObservationSet
where
	ObservationSet.SourceUniqueID = 
							(
							select 
								SourceUniqueID = max(SourceUniqueID)
							from 
								Observation.ObservationSet LastInChain
							where
								LastInChain.AlertChainID = ObservationSet.AlertChainID
							)


update ObservationSet

set
	AlertChainMinutes =
		datediff(
			 minute
			,
				(
				select
					min(StartTime)
				from
					Observation.ObservationSet FirstInChain
				where
					FirstInChain.AlertChainID = ObservationSet.AlertChainID
				)
			,
			(								
			StartTime
			)
		)
from
	Observation.ObservationSet
--where
--	IsLastInChain = 1
	


--if object_id('ETL.TAlertChain') is not null
--drop table ETL.TAlertChain
	

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 
exec Utility.WriteAuditLogEvent 'AssignObservationEWSChain', @Stats, @StartTime
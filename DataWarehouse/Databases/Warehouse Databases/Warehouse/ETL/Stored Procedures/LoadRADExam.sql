﻿CREATE    procedure [ETL].[LoadRADExam] (
	@ImportDateTimeStamp datetime
)

AS

	/******************************************************************************
	**  Name: LoadRADExam
	**  Purpose: 
	**
	**  Import procedure for Radiology RAD.Exam table rows 
	**
	**	Called by LoadRAD
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 16.05.11    MH         Was deleting rows from RAD.Exam based on TLoadRADExam rows that  
	**                        were not part of the current process time stamp
	** 01.06.11    MH         Exclude an ExamDate of 2099-1-1 when checking for a min and max date
	**                        to base deleting rows on.
	** 08.06.11    MH         Place a check on the start and end dates range, to prevent
	**                        a corrupt date from deleting rows from the main table
	** 13.03.12    AP		  Quick fudge to stop procedure failing if no unique exam keys are found
	**						  for a specific ImportDateTimeStamp value
	** 26.06.12    MH         Altered for CMFT environment
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CensusDate smalldatetime
declare @InterfaceCode char(5)
declare @Process varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime
declare @LoadStartDateString varchar(20)
declare @LoadEndDateString varchar(20)



select @Process = 'LoadRADExam'

select @StartTime = getdate()

-- 2010-03-11 CCB
-- Should use EventDate but this is currently missing from the CRIS Stat
-- Ask Paul Egan to add this column to the end of the Stat

select
	 @LoadStartDate = MIN(ExamDate) 
	,@LoadEndDate = MAX(ExamDate) 
from
	ETL.TLoadRADExam
where
	ImportDateTimeStamp = @ImportDateTimeStamp
AND ExamDate <> '2099-1-1'								-- Exclude this date, which shouldn't occur


delete from RAD.Exam 
where	
--	Exam.ExamDate between @LoadStartDate and @LoadEndDate
--or
	exists (
		select
			1
		from
			TLoadRADExam
		where
			Exam.SourceUniqueID = TLoadRADExam.SourceUniqueID
		AND TLoadRADExam.ImportDateTimeStamp = @ImportDateTimeStamp
	)


select @RowsDeleted = @@ROWCOUNT


INSERT INTO RAD.Exam
(
	 SourceUniqueID
	,EventSourceUniqueID
	,AccessionNo
	,ExamCode
	,InterventionalFlag
	,AllocatedTimeMins
	,ExamDate
	,ExamStartTime
	,ExamEndTime
	,RoomCode
	,Radiographer1Code
	,Radiographer1CompetenceLevel
	,Radiographer1Difficulty
	,Radiographer2Code
	,Radiographer2CompetenceLevel
	,Radiographer2Difficulty
	,Radiographer3Code
	,Radiographer3CompetenceLevel
	,Radiographer3Difficulty
	,RadiologistCode
	,ContrastBatchNo
	,BoneDensitometryValue
	,Concentration
	,ContractCode
	,ContrastCode
	,ImageStoreDisk
	,ExamQuality
	,FlexibleForm
	,IgnoreAppt
	,InjectedBy
	,MuseumCode
	,Reaction
	,HasRedDot
	,RestrainDose
	,RestrainName
	,RestrainType
	,ScanSlices
	,ExamScreeningTime
	,ExamStatus
	,ExamBookedDate
	,ExamBookedTime
	,ApptBookMode
	,ApptCommitFlag
	,ApptPreviousDate
	,ApptPreviousTime
	,ApptLetterPrintedDate
	,SiteCodeAtTimeOfBooking
	,OrderCommsUniqueID
	,DaysWaiting
	,ExamLogicalDeleteFlag
	,ExamCreatedDate
	,ExamCreatedTime
	,ExamModifiedDate
	,ExamModifiedTime
	,ReportCheckedBy
	,Created
	,ByWhom
	
	-- MH 05.05.11
	-- New fields included for CRIS update		
	,DateReported
	,DateTyped
	,DefaultLength
	,IDChecked
	,IDCheckedBy
	,IsTyped
	,IsVerified
	,MPPSID
	,MPPSStatus
	,OrderID
	,PregnancyChecked
	,PregnancyCheckedBy
	,QuantityUsed
	,StudyID
	,TimeFirstVerified
	,TimeLastVerified
	,WaitBreachDate
	,WaitNPland
	,WasPlanned
	,WasScheduled
	,WasWaiting
	,WeeksWaiting
)

SELECT
	 SourceUniqueID
	,EventSourceUniqueID
	,AccessionNo
	,ExamCode
	,InterventionalFlag
	,AllocatedTimeMins
	,ExamDate
	,ExamStartTime
	,ExamEndTime
	,RoomCode
	,Radiographer1Code
	,Radiographer1CompetenceLevel
	,Radiographer1Difficulty
	,Radiographer2Code
	,Radiographer2CompetenceLevel
	,Radiographer2Difficulty
	,Radiographer3Code
	,Radiographer3CompetenceLevel
	,Radiographer3Difficulty
	,RadiologistCode
	,ContrastBatchNo
	,BoneDensitometryValue
	,Concentration
	,ContractCode
	,ContrastCode
	,ImageStoreDisk
	,ExamQuality
	,FlexibleForm
	,IgnoreAppt
	,InjectedBy
	,MuseumCode
	,Reaction
	,HasRedDot
	,RestrainDose
	,RestrainName
	,RestrainType
	,ScanSlices
	,ExamScreeningTime
	,ExamStatus
	,ExamBookedDate
	,ExamBookedTime
	,ApptBookMode
	,ApptCommitFlag
	,ApptPreviousDate
	,ApptPreviousTime
	,ApptLetterPrintedDate
	,SiteCodeAtTimeOfBooking
	,OrderCommsUniqueID
	,DaysWaiting
	,ExamLogicalDeleteFlag
	,ExamCreatedDate
	,ExamCreatedTime
	,ExamModifiedDate
	,ExamModifiedTime
	,ReportCheckedBy

	,Created = getdate()
	,ByWhom = system_user
	
	-- MH 05.05.11
	-- New fields included for CRIS update		
	,DateReported
	,DateTyped
	,DefaultLength
	,IDChecked
	,IDCheckedBy
	,IsTyped
	,IsVerified
	,MPPSID
	,MPPSStatus
	,OrderID
	,PregnancyChecked
	,PregnancyCheckedBy
	,QuantityUsed
	,StudyID
	,TimeFirstVerified
	,TimeLastVerified
	,WaitBreachDate
	,WaitNPland
	,WasPlanned
	,WasScheduled
	,WasWaiting
	,WeeksWaiting
FROM
	ETL.TLoadRADExam
where
	ImportDateTimeStamp = @ImportDateTimeStamp


select @RowsInserted = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

if (@RowsDeleted is null or @RowsInserted is null or @LoadStartDate is null or @LoadEndDate is null)
	BEGIN
		--Declare these as 0 so the load doesnt fail if no records are present for a specific timestamp
		set @RowsDeleted = 0
		set @RowsInserted = 0
		set @LoadStartDate = '1900-01-01'
		set @LoadEndDate = '1900-01-01'
		set @Stats = 'no valid dates for ' + CONVERT(varchar(11), @ImportDateTimeStamp)
END
ELSE
	BEGIN
		SELECT @Stats =
			'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
			', Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
			', Net change '  + CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + 
			', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
			CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)
			
	END

exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

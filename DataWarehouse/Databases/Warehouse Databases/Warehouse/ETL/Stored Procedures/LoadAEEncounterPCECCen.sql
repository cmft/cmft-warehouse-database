﻿



CREATE procedure [ETL].[LoadAEEncounterPCECCen] 
as


/****************************************************************************************
      View              : [dbo].[LoadPCECAEEncounter]
      Description       : 

      Modification History
      ====================
      
      Date        Person                  Description
      ====================================================================================
      ?                 ?                       Initial Coding
      20140612    RR                      Create table of breaches from Adastra for validation.
      20140730    Paul Egan       Corrected position of 'select @RowsInsertedTGH = @@rowcount;'
      20140829    RR                      CaseNo 082790135 to breach table. Following issue with this record raised by HS and cannot be amended on front end.
      20141110    Paul Egan         CaseNo 110596934 to breach table. Patient returned to PCEC later in the day for a new prescription, 2nd attendance showing as breach on CI report.
      20141111    Paul Egan         Can disable Central using parameters, for Trafford backload.
                                                Trafford now populates breach table.
      20141119    Paul Egan         Added Trafford extra fields (AttendanceNumber, AgeOnArrival, Postcode)
      
      20141229    RR                      Created new version of proc based on new TImport process from PCEC staging.  Updated to a merge.  Original is saved in G:\,,,,\RR\SQL\Adastra
      
*****************************************************************************************/

set dateformat dmy

declare @StartTime datetime = getdate()
      ,@Elapsed int
      --,@RowsDeleted int
      --,@RowsDeletedTGH int
      --,@RowsInserted int
      --,@RowsInsertedTGH int
      --,@RowsUpdatedValidation int       -- Added Paul Egan 11/11/2014
      ,@Stats varchar(255)    
      ,@from datetime  
      ,@to datetime
      ,@deleted int
      ,@inserted int
      ,@updated int
      ,@InterfaceCode varchar(10)
      ,@SiteCode varchar(10)
      ,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
      
set @from = 
            (-- 20150511 RR on a couple of occassions, 2 patients have arrived at the same time, therfore potentially has duplicates, changed to a min to get 1 value returned.
            Select
                  min(ArrivalTime)
            from
                  ETL.TLoadAEEncounterPCEC
            where 
                  SequenceNo = 1
            --and   not exists
            --      (
            --      Select      
            --            1
            --      from 
            --            ETL.TLoadAEEncounterPCEC Earliest
            --      where
            --            TLoadAEEncounterPCEC.ArrivalTime > Earliest.ArrivalTime
            --      )
            )
set @to = 
            (
            Select
                  max(ArrivalTime)
            from
                  ETL.TLoadAEEncounterPCEC
            where 
                  SequenceNo = 1
            --and   not exists
            --      (
            --      Select      
            --            1
            --      from 
            --            ETL.TLoadAEEncounterPCEC Latest
            --      where
            --            TLoadAEEncounterPCEC.ArrivalTime < Latest.ArrivalTime
            --      )
            )

set @InterfaceCode =
            (
            select 
                  InterfaceCode
            from
                  ETL.TLoadAEEncounterPCEC
            where 
                  SequenceNo = 1
            and   not exists
                  (
                  Select      
                        1
                  from 
                        ETL.TLoadAEEncounterPCEC Latest
                  where
                        TLoadAEEncounterPCEC.ArrivalTime < Latest.ArrivalTime
                  )
            )

set @SiteCode =
            (
            select 
                  SiteCode
            from
                  ETL.TLoadAEEncounterPCEC
            where 
                  SequenceNo = 1
            and   not exists
                  (
                  Select      
                        1
                  from 
                        ETL.TLoadAEEncounterPCEC Latest
                  where
                        TLoadAEEncounterPCEC.ArrivalTime < Latest.ArrivalTime
                  )
            )

-- 20140612 RR Creates table of Breaches from Adastra since 01/04/2014 for validation
-- Validated Times are used in the view which is referenced below

insert into dbo.TImportAEPCECBreaches 
      (
      AttendanceNumber
      ,SiteCode
      ,PatientSurname
      ,ArrivalDate
      ,ArrivalTime
      ,SeenForTreatmentTime 
      ,AttendanceConclusionTime 
      ,DepartureTime
      ,MinutesInDepartment 
      )
select
      AttendanceNumber
      ,SiteCode         -- Added Paul Egan 11/11/2014
      ,Encounter.PatientSurname
      ,ArrivalDate
      ,ArrivalTime
      ,SeenForTreatmentTime 
      ,AttendanceConclusionTime 
      ,DepartureTime 
      ,MinutesInDepartment = datediff(mi,ArrivalTime,DepartureTime)
from
      ETL.TLoadAEEncounterPCEC Encounter

where
      AttendanceNumber is not null
and   rtrim(AttendanceNumber) != ''
and SequenceNo = 1
and   AttendanceNumber not in ('79815' , '79823')
and ArrivalDate >='20140401'
and
      (datediff(mi,ArrivalTime,DepartureTime)>240
      or AttendanceNumber in
            (
            '082790135' -- 20140829 RR, following issue with this record raised by HS and cannot be amended on front end.
            ,'110596934' -- 20141110 Paul Egan, patient returned to PCEC later in the day for a new prescription, showing as breach.
            )
      )
and not exists
      (select 1
      from dbo.TImportAEPCECBreaches
      where TImportAEPCECBreaches.AttendanceNumber = Encounter.AttendanceNumber
      )


-- Insert new attendances into AE.Encounter     

-- Query was taking a while using view, discussed with JB, used CTE still took a while (stopped after 20mins), changed to temp table, took 9 secs.
declare
      @MergeSummary TABLE(Action nvarchar(10));

select
      SourceUniqueID
      ,UniqueBookingReferenceNo
      ,PathwayId
      ,PathwayIdIssuerCode
      ,RTTStatusCode
      ,RTTStartDate
      ,RTTEndDate
      ,DistrictNo
      ,TrustNo
      ,CasenoteNo
      ,DistrictNoOrganisationCode
      ,NHSNumber
      ,NHSNumberStatusId
      ,PatientTitle
      ,PatientForename
      ,PatientSurname
      ,PatientAddress1
      ,PatientAddress2
      ,PatientAddress3
      ,PatientAddress4
      ,Postcode
      ,DateOfBirth
      ,DateOfDeath
      ,SexCode
      ,CarerSupportIndicator
      ,RegisteredGpCode
      ,RegisteredGpPracticeCode
      ,AttendanceNumber
      ,ArrivalModeCode
      ,AttendanceCategoryCode
      ,AttendanceDisposalCode
      ,SourceAttendanceDisposalCode
      ,IncidentLocationTypeCode
      ,PatientGroupCode
      ,SourceOfReferralCode
      ,ArrivalDate
      ,ArrivalTime
      ,AgeOnArrival
      ,InitialAssessmentTime
      ,SeenForTreatmentTime
      ,AttendanceConclusionTime
      ,DepartureTime
      ,CommissioningSerialNo
      ,NHSServiceAgreementLineNo
      ,ProviderReferenceNo
      ,CommissionerReferenceNo
      ,ProviderCode
      ,CommissionerCode
      ,StaffMemberCode
      ,InvestigationCodeFirst
      ,InvestigationCodeSecond
      ,DiagnosisCodeFirst
      ,DiagnosisCodeSecond
      ,TreatmentCodeFirst
      ,TreatmentCodeSecond
      ,PASHRGCode
      ,HRGVersionCode
      ,PASDGVPCode
      ,SiteCode
      ,Created
      ,Updated
      ,ByWhom
      ,InterfaceCode
      ,PCTCode
      ,ResidencePCTCode
      ,InvestigationCodeList
      ,TriageCategoryCode
      ,PresentingProblem
      ,ToXrayTime
      ,FromXrayTime
      ,ToSpecialtyTime
      ,SeenBySpecialtyTime
      ,EthnicCategoryCode
      ,ReferredToSpecialtyCode
      ,DecisionToAdmitTime
      ,DischargeDestinationCode
      ,RegisteredTime
      ,TransportRequestTime
      ,TransportAvailableTime
      ,AscribeLeftDeptTime
      ,CDULeftDepartmentTime
      ,PCDULeftDepartmentTime
      ,AmbulanceArrivalTime
      ,AmbulanceCrewPRF
      ,ArrivalTimeAdjusted
      ,UnplannedReattend7Day
      ,ClinicalAssessmentTime
      ,Reportable = 1
into
      #AEEncounterPCEC
from
      ETL.TLoadAEEncounterPCEC
where
      SequenceNo = 1



merge AE.Encounter target

using
      (
      select
            SourceUniqueID
            ,UniqueBookingReferenceNo
            ,PathwayId
            ,PathwayIdIssuerCode
            ,RTTStatusCode
            ,RTTStartDate
            ,RTTEndDate
            ,DistrictNo
            ,TrustNo
            ,CasenoteNo
            ,DistrictNoOrganisationCode
            ,NHSNumber
            ,NHSNumberStatusId
            ,PatientTitle
            ,PatientForename
            ,PatientSurname
            ,PatientAddress1
            ,PatientAddress2
            ,PatientAddress3
            ,PatientAddress4
            ,Postcode
            ,DateOfBirth
            ,DateOfDeath
            ,SexCode
            ,CarerSupportIndicator
            ,RegisteredGpCode
            ,RegisteredGpPracticeCode
            ,AttendanceNumber
            ,ArrivalModeCode
            ,AttendanceCategoryCode
            ,AttendanceDisposalCode
            ,SourceAttendanceDisposalCode
            ,IncidentLocationTypeCode
            ,PatientGroupCode
            ,SourceOfReferralCode
            ,ArrivalDate
            ,ArrivalTime
            ,AgeOnArrival
            ,InitialAssessmentTime
            ,SeenForTreatmentTime
            ,AttendanceConclusionTime
            ,DepartureTime
            ,CommissioningSerialNo
            ,NHSServiceAgreementLineNo
            ,ProviderReferenceNo
            ,CommissionerReferenceNo
            ,ProviderCode
            ,CommissionerCode
            ,StaffMemberCode
            ,InvestigationCodeFirst
            ,InvestigationCodeSecond
            ,DiagnosisCodeFirst
            ,DiagnosisCodeSecond
            ,TreatmentCodeFirst
            ,TreatmentCodeSecond
            ,PASHRGCode
            ,HRGVersionCode
            ,PASDGVPCode
            ,SiteCode
            ,Created
            ,Updated
            ,ByWhom
            ,InterfaceCode
            ,PCTCode
            ,ResidencePCTCode
            ,InvestigationCodeList
            ,TriageCategoryCode
            ,PresentingProblem
            ,ToXrayTime
            ,FromXrayTime
            ,ToSpecialtyTime
            ,SeenBySpecialtyTime
            ,EthnicCategoryCode
            ,ReferredToSpecialtyCode
            ,DecisionToAdmitTime
            ,DischargeDestinationCode
            ,RegisteredTime
            ,TransportRequestTime
            ,TransportAvailableTime
            ,AscribeLeftDeptTime
            ,CDULeftDepartmentTime
            ,PCDULeftDepartmentTime
            ,AmbulanceArrivalTime
            ,AmbulanceCrewPRF
            ,ArrivalTimeAdjusted
            ,UnplannedReattend7Day
            ,ClinicalAssessmentTime
            ,Reportable = 1
      from
            #AEEncounterPCEC
      ) source
      on    source.SourceUniqueID = target.SourceUniqueID
            
      when not matched by source
      and target.ArrivalTime between @from and @to
      and target.InterfaceCode = @InterfaceCode
      and target.SiteCode = @SiteCode
            
      then delete
            
      when not matched
      then
            insert
                  (
                  SourceUniqueID
                  ,UniqueBookingReferenceNo
                  ,PathwayId
                  ,PathwayIdIssuerCode
                  ,RTTStatusCode
                  ,RTTStartDate
                  ,RTTEndDate
                  ,DistrictNo
                  ,TrustNo
                  ,CasenoteNo
                  ,DistrictNoOrganisationCode
                  ,NHSNumber
                  ,NHSNumberStatusId
                  ,PatientTitle
                  ,PatientForename
                  ,PatientSurname
                  ,PatientAddress1
                  ,PatientAddress2
                  ,PatientAddress3
                  ,PatientAddress4
                  ,Postcode
                  ,DateOfBirth
                  ,DateOfDeath
                  ,SexCode
                  ,CarerSupportIndicator
                  ,RegisteredGpCode
                  ,RegisteredGpPracticeCode
                  ,AttendanceNumber
                  ,ArrivalModeCode
                  ,AttendanceCategoryCode
                  ,AttendanceDisposalCode
                  ,SourceAttendanceDisposalCode
                  ,IncidentLocationTypeCode
                  ,PatientGroupCode
                  ,SourceOfReferralCode
                  ,ArrivalDate
                  ,ArrivalTime
                  ,AgeOnArrival
                  ,InitialAssessmentTime
                  ,SeenForTreatmentTime
                  ,AttendanceConclusionTime
                  ,DepartureTime
                  ,CommissioningSerialNo
                  ,NHSServiceAgreementLineNo
                  ,ProviderReferenceNo
                  ,CommissionerReferenceNo
                  ,ProviderCode
                  ,CommissionerCode
                  ,StaffMemberCode
                  ,InvestigationCodeFirst
                  ,InvestigationCodeSecond
                  ,DiagnosisCodeFirst
                  ,DiagnosisCodeSecond
                  ,TreatmentCodeFirst
                  ,TreatmentCodeSecond
                  ,PASHRGCode
                  ,HRGVersionCode
                  ,PASDGVPCode
                  ,SiteCode
                  ,InterfaceCode
                  ,PCTCode
                  ,ResidencePCTCode
                  ,InvestigationCodeList
                  ,TriageCategoryCode
                  ,PresentingProblem
                  ,ToXrayTime
                  ,FromXrayTime
                  ,ToSpecialtyTime
                  ,SeenBySpecialtyTime
                  ,EthnicCategoryCode
                  ,ReferredToSpecialtyCode
                  ,DecisionToAdmitTime
                  ,DischargeDestinationCode
                  ,RegisteredTime
                  ,TransportRequestTime
                  ,TransportAvailableTime
                  ,AscribeLeftDeptTime
                  ,CDULeftDepartmentTime
                  ,PCDULeftDepartmentTime
                  ,AmbulanceArrivalTime
                  ,AmbulanceCrewPRF
                  ,ArrivalTimeAdjusted
                  ,UnplannedReattend7Day
                  ,ClinicalAssessmentTime
                  ,Reportable
                  ,Created
                  ,Updated
                  ,ByWhom
                  )
            values
                  (
                   source.SourceUniqueID
                  ,source.UniqueBookingReferenceNo
                  ,source.PathwayId
                  ,source.PathwayIdIssuerCode
                  ,source.RTTStatusCode
                  ,source.RTTStartDate
                  ,source.RTTEndDate
                  ,source.DistrictNo
                  ,source.TrustNo
                  ,source.CasenoteNo
                  ,source.DistrictNoOrganisationCode
                  ,source.NHSNumber
                  ,source.NHSNumberStatusId
                  ,source.PatientTitle
                  ,source.PatientForename
                  ,source.PatientSurname
                  ,source.PatientAddress1
                  ,source.PatientAddress2
                  ,source.PatientAddress3
                  ,source.PatientAddress4
                  ,source.Postcode
                  ,source.DateOfBirth
                  ,source.DateOfDeath
                  ,source.SexCode
                  ,source.CarerSupportIndicator
                  ,source.RegisteredGpCode
                  ,source.RegisteredGpPracticeCode
                  ,source.AttendanceNumber
                  ,source.ArrivalModeCode
                  ,source.AttendanceCategoryCode
                  ,source.AttendanceDisposalCode
                  ,source.SourceAttendanceDisposalCode
                  ,source.IncidentLocationTypeCode
                  ,source.PatientGroupCode
                  ,source.SourceOfReferralCode
                  ,source.ArrivalDate
                  ,source.ArrivalTime
                  ,source.AgeOnArrival
                  ,source.InitialAssessmentTime
                  ,source.SeenForTreatmentTime
                  ,source.AttendanceConclusionTime
                  ,source.DepartureTime
                  ,source.CommissioningSerialNo
                  ,source.NHSServiceAgreementLineNo
                  ,source.ProviderReferenceNo
                  ,source.CommissionerReferenceNo
                  ,source.ProviderCode
                  ,source.CommissionerCode
                  ,source.StaffMemberCode
                  ,source.InvestigationCodeFirst
                  ,source.InvestigationCodeSecond
                  ,source.DiagnosisCodeFirst
                  ,source.DiagnosisCodeSecond
                  ,source.TreatmentCodeFirst
                  ,source.TreatmentCodeSecond
                  ,source.PASHRGCode
                  ,source.HRGVersionCode
                  ,source.PASDGVPCode
                  ,source.SiteCode
                  ,source.InterfaceCode
                  ,source.PCTCode
                  ,source.ResidencePCTCode
                  ,source.InvestigationCodeList
                  ,source.TriageCategoryCode
                  ,source.PresentingProblem
                  ,source.ToXrayTime
                  ,source.FromXrayTime
                  ,source.ToSpecialtyTime
                  ,source.SeenBySpecialtyTime
                  ,source.EthnicCategoryCode
                  ,source.ReferredToSpecialtyCode
                  ,source.DecisionToAdmitTime
                  ,source.DischargeDestinationCode
                  ,source.RegisteredTime
                  ,source.TransportRequestTime
                  ,source.TransportAvailableTime
                  ,source.AscribeLeftDeptTime
                  ,source.CDULeftDepartmentTime
                  ,source.PCDULeftDepartmentTime
                  ,source.AmbulanceArrivalTime
                  ,source.AmbulanceCrewPRF
                  ,source.ArrivalTimeAdjusted
                  ,source.UnplannedReattend7Day
                  ,source.ClinicalAssessmentTime
                  ,source.Reportable 
                  ,getdate()
                  ,getdate()
                  ,suser_name()
                  )
      when matched
      and not
            (
                  isnull(target.UniqueBookingReferenceNo, 0) = isnull(source.UniqueBookingReferenceNo, 0)
            and isnull(target.PathwayId, 0) = isnull(source.PathwayId, 0)
            and isnull(target.PathwayIdIssuerCode, 0) = isnull(source.PathwayIdIssuerCode, 0)
            and isnull(target.RTTStatusCode, 0) = isnull(source.RTTStatusCode, 0)
            and isnull(target.RTTStartDate, 0) = isnull(source.RTTStartDate, 0)
            and isnull(target.RTTEndDate, 0) = isnull(source.RTTEndDate, 0)
            and isnull(target.DistrictNo, 0) = isnull(source.DistrictNo, 0)
            and isnull(target.TrustNo, 0) = isnull(source.TrustNo, 0)
            and isnull(target.CasenoteNo, 0) = isnull(source.CasenoteNo, 0)
            and isnull(target.DistrictNoOrganisationCode, 0) = isnull(source.DistrictNoOrganisationCode, 0)
            and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
            and isnull(target.NHSNumberStatusId, 0) = isnull(source.NHSNumberStatusId, 0)
            and isnull(target.PatientTitle, 0) = isnull(source.PatientTitle, 0)
            and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
            and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
            and isnull(target.PatientAddress1, '') = isnull(source.PatientAddress1, '')
            and isnull(target.PatientAddress2, '') = isnull(source.PatientAddress2, '')
            and isnull(target.PatientAddress3, '') = isnull(source.PatientAddress3, '')
            and isnull(target.PatientAddress4, 0) = isnull(source.PatientAddress4, 0)
            and isnull(target.Postcode, '') = isnull(source.Postcode, '')
            and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
            and isnull(target.DateOfDeath, 0) = isnull(source.DateOfDeath, 0)
            and isnull(target.SexCode, '') = isnull(source.SexCode, '')
            and isnull(target.CarerSupportIndicator, 0) = isnull(source.CarerSupportIndicator, 0)
            and isnull(target.RegisteredGpCode, 0) = isnull(source.RegisteredGpCode, 0)
            and isnull(target.RegisteredGpPracticeCode, 0) = isnull(source.RegisteredGpPracticeCode, 0)
            and isnull(target.AttendanceNumber, '') = isnull(source.AttendanceNumber, '')
            and isnull(target.ArrivalModeCode, 0) = isnull(source.ArrivalModeCode, 0)
            and isnull(target.AttendanceCategoryCode, 0) = isnull(source.AttendanceCategoryCode, 0)
            and isnull(target.AttendanceDisposalCode, '') = isnull(source.AttendanceDisposalCode, '')
            and isnull(target.SourceAttendanceDisposalCode, '') = isnull(source.SourceAttendanceDisposalCode, '')
            and isnull(target.IncidentLocationTypeCode, 0) = isnull(source.IncidentLocationTypeCode, 0)
            and isnull(target.PatientGroupCode, 0) = isnull(source.PatientGroupCode, 0)
            and isnull(target.SourceOfReferralCode, 0) = isnull(source.SourceOfReferralCode, 0)
            and isnull(target.ArrivalDate, getdate()) = isnull(source.ArrivalDate, getdate())
            and isnull(target.ArrivalTime, getdate()) = isnull(source.ArrivalTime, getdate())
            and isnull(target.AgeOnArrival, 0) = isnull(source.AgeOnArrival, 0)
            and isnull(target.InitialAssessmentTime, 0) = isnull(source.InitialAssessmentTime, 0)
            and isnull(target.SeenForTreatmentTime, getdate()) = isnull(source.SeenForTreatmentTime, getdate())
            and isnull(target.AttendanceConclusionTime, getdate()) = isnull(source.AttendanceConclusionTime, getdate())
            and isnull(target.DepartureTime, getdate()) = isnull(source.DepartureTime, getdate())
            and isnull(target.CommissioningSerialNo, '') = isnull(source.CommissioningSerialNo, '')
            and isnull(target.NHSServiceAgreementLineNo, 0) = isnull(source.NHSServiceAgreementLineNo, 0)
            and isnull(target.ProviderReferenceNo, 0) = isnull(source.ProviderReferenceNo, 0)
            and isnull(target.CommissionerReferenceNo, 0) = isnull(source.CommissionerReferenceNo, 0)
            and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
            and isnull(target.CommissionerCode, '') = isnull(source.CommissionerCode, '')
            and isnull(target.StaffMemberCode, 0) = isnull(source.StaffMemberCode, 0)
            and isnull(target.InvestigationCodeFirst, 0) = isnull(source.InvestigationCodeFirst, 0)
            and isnull(target.InvestigationCodeSecond, 0) = isnull(source.InvestigationCodeSecond, 0)
            and isnull(target.DiagnosisCodeFirst, 0) = isnull(source.DiagnosisCodeFirst, 0)
            and isnull(target.DiagnosisCodeSecond, 0) = isnull(source.DiagnosisCodeSecond, 0)
            and isnull(target.TreatmentCodeFirst, 0) = isnull(source.TreatmentCodeFirst, 0)
            and isnull(target.TreatmentCodeSecond, 0) = isnull(source.TreatmentCodeSecond, 0)
            and isnull(target.PASHRGCode, 0) = isnull(source.PASHRGCode, 0)
            and isnull(target.HRGVersionCode, 0) = isnull(source.HRGVersionCode, 0)
            and isnull(target.PASDGVPCode, 0) = isnull(source.PASDGVPCode, 0)
            and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
            and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
            and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
            and isnull(target.ResidencePCTCode, '') = isnull(source.ResidencePCTCode, '')
            and isnull(target.InvestigationCodeList, 0) = isnull(source.InvestigationCodeList, 0)
            and isnull(target.TriageCategoryCode, 0) = isnull(source.TriageCategoryCode, 0)
            and isnull(target.PresentingProblem, 0) = isnull(source.PresentingProblem, 0)
            and isnull(target.ToXrayTime, 0) = isnull(source.ToXrayTime, 0)
            and isnull(target.FromXrayTime, 0) = isnull(source.FromXrayTime, 0)
            and isnull(target.ToSpecialtyTime, 0) = isnull(source.ToSpecialtyTime, 0)
            and isnull(target.SeenBySpecialtyTime, 0) = isnull(source.SeenBySpecialtyTime, 0)
            and isnull(target.EthnicCategoryCode, 0) = isnull(source.EthnicCategoryCode, 0)
            and isnull(target.ReferredToSpecialtyCode, 0) = isnull(source.ReferredToSpecialtyCode, 0)
            and isnull(target.DecisionToAdmitTime, 0) = isnull(source.DecisionToAdmitTime, 0)
            and isnull(target.DischargeDestinationCode, 0) = isnull(source.DischargeDestinationCode, 0)
            and isnull(target.RegisteredTime, 0) = isnull(source.RegisteredTime, 0)
            and isnull(target.TransportRequestTime, 0) = isnull(source.TransportRequestTime, 0)
            and isnull(target.TransportAvailableTime, 0) = isnull(source.TransportAvailableTime, 0)
            and isnull(target.AscribeLeftDeptTime, 0) = isnull(source.AscribeLeftDeptTime, 0)
            and isnull(target.CDULeftDepartmentTime, 0) = isnull(source.CDULeftDepartmentTime, 0)
            and isnull(target.PCDULeftDepartmentTime, 0) = isnull(source.PCDULeftDepartmentTime, 0)
            and isnull(target.AmbulanceArrivalTime, 0) = isnull(source.AmbulanceArrivalTime, 0)
            and isnull(target.AmbulanceCrewPRF, 0) = isnull(source.AmbulanceCrewPRF, 0)
            and isnull(target.ArrivalTimeAdjusted, getdate()) = isnull(source.ArrivalTimeAdjusted, getdate())
            and isnull(target.UnplannedReattend7Day, 0) = isnull(source.UnplannedReattend7Day, 0)
            and isnull(target.ClinicalAssessmentTime, 0) = isnull(source.ClinicalAssessmentTime, 0)
            and isnull(target.Reportable,0) = isnull(source.Reportable,0)
            and isnull(target.Created, 0) = isnull(source.Created, 0)
            and isnull(target.Updated, 0) = isnull(source.Updated, 0)
            and isnull(target.ByWhom, 0) = isnull(source.ByWhom, 0)
            )
      then
            update
            set
                  target.UniqueBookingReferenceNo = source.UniqueBookingReferenceNo
                  ,target.PathwayId = source.PathwayId
                  ,target.PathwayIdIssuerCode = source.PathwayIdIssuerCode
                  ,target.RTTStatusCode = source.RTTStatusCode
                  ,target.RTTStartDate = source.RTTStartDate
                  ,target.RTTEndDate = source.RTTEndDate
                  ,target.DistrictNo = source.DistrictNo
                  ,target.TrustNo = source.TrustNo
                  ,target.CasenoteNo = source.CasenoteNo
                  ,target.DistrictNoOrganisationCode = source.DistrictNoOrganisationCode
                  ,target.NHSNumber = source.NHSNumber
                  ,target.NHSNumberStatusId = source.NHSNumberStatusId
                  ,target.PatientTitle = source.PatientTitle
                  ,target.PatientForename = source.PatientForename
                  ,target.PatientSurname = source.PatientSurname
                  ,target.PatientAddress1 = source.PatientAddress1
                  ,target.PatientAddress2 = source.PatientAddress2
                  ,target.PatientAddress3 = source.PatientAddress3
                  ,target.PatientAddress4 = source.PatientAddress4
                  ,target.Postcode = source.Postcode
                  ,target.DateOfBirth = source.DateOfBirth
                  ,target.DateOfDeath = source.DateOfDeath
                  ,target.SexCode = source.SexCode
                  ,target.CarerSupportIndicator = source.CarerSupportIndicator
                  ,target.RegisteredGpCode = source.RegisteredGpCode
                  ,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
                  ,target.AttendanceNumber = source.AttendanceNumber
                  ,target.ArrivalModeCode = source.ArrivalModeCode
                  ,target.AttendanceCategoryCode = source.AttendanceCategoryCode
                  ,target.AttendanceDisposalCode = source.AttendanceDisposalCode
                  ,target.SourceAttendanceDisposalCode = source.SourceAttendanceDisposalCode
                  ,target.IncidentLocationTypeCode = source.IncidentLocationTypeCode
                  ,target.PatientGroupCode = source.PatientGroupCode
                  ,target.SourceOfReferralCode = source.SourceOfReferralCode
                  ,target.ArrivalDate = source.ArrivalDate
                  ,target.ArrivalTime = source.ArrivalTime
                  ,target.AgeOnArrival = source.AgeOnArrival
                  ,target.InitialAssessmentTime = source.InitialAssessmentTime
                  ,target.SeenForTreatmentTime = source.SeenForTreatmentTime
                  ,target.AttendanceConclusionTime = source.AttendanceConclusionTime
                  ,target.DepartureTime = source.DepartureTime
                  ,target.CommissioningSerialNo = source.CommissioningSerialNo
                  ,target.NHSServiceAgreementLineNo = source.NHSServiceAgreementLineNo
                  ,target.ProviderReferenceNo = source.ProviderReferenceNo
                  ,target.CommissionerReferenceNo = source.CommissionerReferenceNo
                  ,target.ProviderCode = source.ProviderCode
                  ,target.CommissionerCode = source.CommissionerCode
                  ,target.StaffMemberCode = source.StaffMemberCode
                  ,target.InvestigationCodeFirst = source.InvestigationCodeFirst
                  ,target.InvestigationCodeSecond = source.InvestigationCodeSecond
                  ,target.DiagnosisCodeFirst = source.DiagnosisCodeFirst
                  ,target.DiagnosisCodeSecond = source.DiagnosisCodeSecond
                  ,target.TreatmentCodeFirst = source.TreatmentCodeFirst
                  ,target.TreatmentCodeSecond = source.TreatmentCodeSecond
                  ,target.PASHRGCode = source.PASHRGCode
                  ,target.HRGVersionCode = source.HRGVersionCode
                  ,target.PASDGVPCode = source.PASDGVPCode
                  ,target.SiteCode = source.SiteCode
                  ,target.InterfaceCode = source.InterfaceCode
                  ,target.PCTCode = source.PCTCode
                  ,target.ResidencePCTCode = source.ResidencePCTCode
                  ,target.InvestigationCodeList = source.InvestigationCodeList
                  ,target.TriageCategoryCode = source.TriageCategoryCode
                  ,target.PresentingProblem = source.PresentingProblem
                  ,target.ToXrayTime = source.ToXrayTime
                  ,target.FromXrayTime = source.FromXrayTime
                  ,target.ToSpecialtyTime = source.ToSpecialtyTime
                  ,target.SeenBySpecialtyTime = source.SeenBySpecialtyTime
                  ,target.EthnicCategoryCode = source.EthnicCategoryCode
                  ,target.ReferredToSpecialtyCode = source.ReferredToSpecialtyCode
                  ,target.DecisionToAdmitTime = source.DecisionToAdmitTime
                  ,target.DischargeDestinationCode = source.DischargeDestinationCode
                  ,target.RegisteredTime = source.RegisteredTime
                  ,target.TransportRequestTime = source.TransportRequestTime
                  ,target.TransportAvailableTime = source.TransportAvailableTime
                  ,target.AscribeLeftDeptTime = source.AscribeLeftDeptTime
                  ,target.CDULeftDepartmentTime = source.CDULeftDepartmentTime
                  ,target.PCDULeftDepartmentTime = source.PCDULeftDepartmentTime
                  ,target.AmbulanceArrivalTime = source.AmbulanceArrivalTime
                  ,target.AmbulanceCrewPRF = source.AmbulanceCrewPRF
                  ,target.ArrivalTimeAdjusted = source.ArrivalTimeAdjusted
                  ,target.UnplannedReattend7Day = source.UnplannedReattend7Day
                  ,target.ClinicalAssessmentTime = source.ClinicalAssessmentTime
                  ,target.Reportable = source.Reportable
                  ,target.Created = getdate()
                  ,target.Updated = getdate()
                  ,target.ByWhom = suser_name()

                  
      output
      $action into @MergeSummary
;


select
      @inserted = sum(Inserted)
      ,@updated = sum(Updated)
      ,@deleted = sum(Deleted)
from
      (
      select
            Inserted = case when Action = 'INSERT' then 1 else 0 end
            ,Updated = case when Action = 'UPDATE' then 1 else 0 end
            ,Deleted = case when Action = 'DELETE' then 1 else 0 end
      from
            @MergeSummary
      ) MergeSummary

select
      @Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
      @Stats =
            'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
            ', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
            ', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
            ', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
      @ProcedureName
      ,@Stats
      ,@StartTime       
                        




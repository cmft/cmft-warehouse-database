﻿CREATE proc [ETL].[ExtractSharepointWard] 

as

declare @StartTime datetime
declare @Elapsed int
declare @Sproc varchar(255)
declare @Stats varchar(255)
declare @RowsInserted int;

select @StartTime = getdate();

truncate table ETL.TImportBedComplementWard

--Awaiting Jabran Sharepoint extract solution...

--insert into ETL.TImportBedComplementWard
--(
--	 WardID
--	,WardCode
--	,Ward
--)
--select
--	 WardCode
--	,WardCode1
--	,Ward
--from [SHAREPOINT]...[qry_Ward];


select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins';
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;

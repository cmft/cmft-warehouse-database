﻿CREATE Procedure [ETL].[LoadRADReport] 
(
	@ImportDateTimeStamp datetime
)

AS

	/******************************************************************************
	**  Name: LoadRADReport
	**  Purpose: 
	**
	**  Import procedure for Radiology RAD.Report table rows 
	**
	**	Called by LoadRAD
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 16.05.11    MH         Was deleting rows from RAD.Report based on TLoadRADReport rows that  
	**                        were not part of the current process time stamp
	** 08.06.11    MH         Place a check on the start and end dates range, to prevent
	**                        a corrupt date from deleting rows from the main table
	** 25.07.11    MH         Include ReportText
	** 26.06.12    MH         Altered for CMFT environment
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CensusDate smalldatetime
declare @InterfaceCode char(5)
declare @Process varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime
declare @LoadStartDateString varchar(20)
declare @LoadEndDateString varchar(20)


select @Process = 'LoadRADReport'

select @StartTime = getdate()


select
	 @LoadStartDate = MIN(ReportedDate) 
	,@LoadEndDate = MAX(ReportedDate) 
from
	ETL.TLoadRADReport
where
	ImportDateTimeStamp = @ImportDateTimeStamp


delete from RAD.Report 
where	
--	Report.ReportedDate between @LoadStartDate and @LoadEndDate
--or
	exists (
		select
			1
		from
			ETL.TLoadRADReport
		where
			Report.SourceUniqueID = TLoadRADReport.SourceUniqueID
		AND TLoadRADReport.ImportDateTimeStamp = @ImportDateTimeStamp
	)

select @RowsDeleted = @@ROWCOUNT


INSERT INTO RAD.Report
(
	 SourceUniqueID
	,ExamSourceUniqueID
	,EventSourceUniqueID
	,EventDate
	,EventTime
	,ExamCode
	,ReportTypeCode
	,ReportedDate
	,ReportedTime
	,ReportedBy
	,TypedDate
	,TypedTime
	,TypedBy
	,VerifiedDate
	,VerifiedTime
	,VerifiedBy
	,LastVerifiedDate
	,LastVerifiedTime
	,LastVerifiedBy
	,Created
	,ByWhom
	
	-- MH 06.05.11 New columns
	,AddendumBy 
	,AddendumDate 
	,AddendumTime 
	,ChangedBy 
	,Confirmed 
	,ConfirmedBy 
	,ConfirmedDesc 
	,CreationDate 
	,CreationTime 
	,DateAcknowledged 
	,DateChanged 
	,DatePrinted 
	,DateSent 
	,VerifyCheckDate 
	,VerifyCheckTime 
	,Deleted 
	,ExamKey 
	,FoetusNumber 
	,LastAddendumBy 
	,LastAddendumDate 
	,LastAddendumTime 
	,ModifiedDate 
	,ModifiedTime 
	,Printed 
	,Priority 
	,RejectReason 
	,ReportedBy2 
	,Restricted 
	,Sent 
	,Status 
	,Verified 
	,VerifyCheckBy 
	,ReportText
)


SELECT
	 SourceUniqueID
	,ExamSourceUniqueID
	,EventSourceUniqueID
	,EventDate
	,EventTime
	,ExamCode
	,ReportTypeCode
	,ReportedDate
	,ReportedTime
	,ReportedBy
	,TypedDate
	,TypedTime
	,TypedBy
	,VerifiedDate
	,VerifiedTime
	,VerifiedBy
	,LastVerifiedDate
	,LastVerifiedTime
	,LastVerifiedBy
	,Created = getdate()
	,ByWhom = system_user
	
	-- MH 06.05.11 New columns
	,AddendumBy 
	,AddendumDate 
	,AddendumTime 
	,ChangedBy 
	,Confirmed 
	,ConfirmedBy 
	,ConfirmedDesc 
	,CreationDate 
	,CreationTime 
	,DateAcknowledged 
	,DateChanged 
	,DatePrinted 
	,DateSent 
	,VerifyCheckDate 
	,VerifyCheckTime 
	,Deleted 
	,ExamKey 
	,FoetusNumber 
	,LastAddendumBy 
	,LastAddendumDate 
	,LastAddendumTime 
	,ModifiedDate 
	,ModifiedTime 
	,Printed 
	,Priority 
	,RejectReason 
	,ReportedBy2 
	,Restricted 
	,Sent 
	,Status 
	,Verified 
	,VerifyCheckBy 
	,ReportText
FROM
	ETL.TLoadRADReport
where
	ImportDateTimeStamp = @ImportDateTimeStamp


select @RowsInserted = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats =
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Net change '  + CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

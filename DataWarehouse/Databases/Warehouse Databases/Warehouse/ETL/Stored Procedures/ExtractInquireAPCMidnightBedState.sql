﻿CREATE procedure [ETL].[ExtractInquireAPCMidnightBedState] 
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112)

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)



insert into ETL.TImportAPCMidnightBedState
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SiteCode
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,SourceAdminCategoryCode
	,ActivityInCode
	,AdmissionDateTimeInt
	,MidnightBedStateDate
	,InterfaceCode
)

select
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo = SourcePatientNo + '/' + SourceSpellNo
	,SiteCode
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,SourceAdminCategoryCode
	,ActivityInCode
	,AdmissionDateTimeInt
	,MidnightBedStateDate
	,InterfaceCode = 'INQ'
from
	(
	SELECT  
		 SourceUniqueID = MIDNIGHTBEDSTATEID 
		,SourcePatientNo = InternalPatientNumber 
		,SourceSpellNo = EpisodeNumber 
		,ConsultantCode = Consultant 
		,SpecialtyCode = Specialty 
		,SourceAdminCategoryCode = Category 
		,SiteCode = HospitalCode 
		,ActivityInCode = ActivityIn 
		,AdmissionDateTimeInt = IpAdmDtimeInt 
		,MidnightBedStateDate = StatisticsDate 
		,WardCode = Ward 
	FROM
		[$(PAS)].Inquire.MIDNIGHTBEDSTATE
	WHERE
		StatisticsDate between @from and @to
	) MidnightBedState



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireMidnightBedState', @Stats, @StartTime

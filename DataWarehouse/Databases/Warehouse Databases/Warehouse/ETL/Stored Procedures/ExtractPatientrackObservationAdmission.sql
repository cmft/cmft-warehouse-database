﻿
CREATE proc [ETL].[ExtractPatientrackObservationAdmission] 

	@FromDate datetime = null
	,@ToDate datetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @RowsInserted = 0

insert ETL.TImportObservationAdmission

(
SourceUniqueID
,CasenoteNumber
,DateOfBirth
,SexID
,Surname
,Forename
,AdmissionTime
,StartSpecialtyID
,StartLocationID
,DischargeTime
,EndSpecialtyID
,EndLocationID
,TreatmentOutcomeID
,DischargeDestinationID
,Comment
,CreatedByUserID
,CreatedTime
,LastModifiedByUserID
,LastModifiedTime
)

select
	SourceUniqueID 
	,CasenoteNumber 
	,DateOfBirth
	,SexID 
	,Surname 
	,Forename 
	,AdmissionTime 
	,StartSpecialtyID
	,StartLocationID
	,DischargeTime 
	,EndSpecialtyID
	,EndLocationID
	,TreatmentOutcomeID 
	,DischargeDestinationID 
	,Comment 
	,CreatedByUserID 
	,CreatedTime 
	,LastModifiedByUserID
	,LastModifiedTime 

from	
	(
	select --top 1000
		SourceUniqueID = Admission.ADMSN_PK
		,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
		,DateOfBirth = cast(Admission.BIRTH_DATE as date)
		,SexID = SEXXX_RFVAL
		,Surname = SURNAME
		,Forename = FORENAME1
		,AdmissionTime = ADMISSION_DTTM
 		,StartSpecialtyID = StartAdmissionUnit.UNITT_RFVAL
 		,StartLocationID = StartWardStay.WARDD_RFVAL
		,DischargeTime = DISCHARGED_DTTM
		,EndSpecialtyID = EndAdmissionUnit.UNITT_RFVAL
		,EndLocationID = EndWardStay.WARDD_RFVAL
		,TreatmentOutcomeID = TROCM_RFVAL 
		,DischargeDestinationID = DISDE_RFVAL
		,Comment = nullif(cast(NOTE as varchar(max)), '')
		,CreatedByUserID = Admission.CREATED_BY_USERR_PK
		,CreatedTime = Admission.CREATED_DTTM
		,LastModifiedByUserID = Admission.LAST_MODIFIED_BY_USERR_PK
		,LastModifiedTime = Admission.LAST_MODIFIED_DTTM
	from
		[$(PatientrackSS)].dbo.ADMISSION Admission

	inner join [$(PatientrackSS)].dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
	on	PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
	and	PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
	and	coalesce(PatientIdentifier.DELETED_FLAG, 0) = 0	

	inner join
			(
			select
				AdmissionUnit.ADMSN_PK 
				,AdmissionUnit.UNITT_RFVAL
			from
				[$(PatientrackSS)].dbo.ADMISSION_UNIT AdmissionUnit
			where
				coalesce(AdmissionUnit.DELETED_FLAG, 0) = 0
			and	not exists
					(
					select
						1
					from
						[$(PatientrackSS)].dbo.ADMISSION_UNIT AdmissionUnitEarlier
					where
						AdmissionUnitEarlier.ADMSN_PK = AdmissionUnit.ADMSN_PK
					and coalesce(AdmissionUnitEarlier.DELETED_FLAG, 0) = 0
					and AdmissionUnitEarlier.ADUNT_PK < AdmissionUnit.ADUNT_PK
					)
			) StartAdmissionUnit
	on	StartAdmissionUnit.ADMSN_PK = Admission.ADMSN_PK

	left join
			(
			select
				AdmissionUnit.ADMSN_PK 
				,AdmissionUnit.UNITT_RFVAL
			from
				[$(PatientrackSS)].dbo.ADMISSION_UNIT AdmissionUnit
			where
				coalesce(AdmissionUnit.DELETED_FLAG, 0) = 0
			and	not exists
					(
					select
						1
					from
						[$(PatientrackSS)].dbo.ADMISSION_UNIT AdmissionUnitNext
					where
						AdmissionUnitNext.ADMSN_PK = AdmissionUnit.ADMSN_PK
					and coalesce(AdmissionUnitNext.DELETED_FLAG, 0) = 0
					and AdmissionUnitNext.ADUNT_PK > AdmissionUnit.ADUNT_PK
					)
			) EndAdmissionUnit
	on	EndAdmissionUnit.ADMSN_PK = Admission.ADMSN_PK

	inner join
		(
		select
			WardStay.ADMSN_PK
			,WardStay.WARDD_RFVAL
		from
			[$(PatientrackSS)].dbo.WARD_STAY WardStay		
		where
			coalesce(WardStay.DELETED_FLAG, 0) = 0
		and	not exists
				(
				select
					1
				from
					[$(PatientrackSS)].dbo.WARD_STAY WardStayEarlier
				where
					WardStayEarlier.ADMSN_PK = WardStay.ADMSN_PK
				and	coalesce(WardStayEarlier.DELETED_FLAG, 0) = 0
				and	WardStayEarlier.WRDST_PK < WardStay.WRDST_PK
				)

		) StartWardStay
	on	StartWardStay.ADMSN_PK = Admission.ADMSN_PK


	left join
		(
		select
			WardStay.ADMSN_PK
			,WardStay.WARDD_RFVAL
		from
			[$(PatientrackSS)].dbo.WARD_STAY WardStay		
		where
			coalesce(WardStay.DELETED_FLAG, 0) = 0
		and	not exists
				(
				select
					1
				from
					[$(PatientrackSS)].dbo.WARD_STAY WardStayNext
				where
					WardStayNext.ADMSN_PK = WardStay.ADMSN_PK
				and	coalesce(WardStayNext.DELETED_FLAG, 0) = 0
				and	WardStayNext.WRDST_PK > WardStay.WRDST_PK
				)

		) EndWardStay
	on	EndWardStay.ADMSN_PK = Admission.ADMSN_PK

	where
		Admission.LAST_MODIFIED_DTTM between @FromDate and @ToDate
	and coalesce(Admission.DELETED_FLAG, 0) = 0	

	) Admission




select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	
﻿
CREATE proc [ETL].[LoadNeonatalReferenceData]

as

--Diagnosis

insert into Neonatal.DiagnosisBase
(
DiagnosisCode
,Diagnosis
)

select
	Code
	,min(Name) -- multiple names for each code, get the first
from
	[$(Bnet_DSS_business)].bnf_dbsync.NNUCodedItems
where
	CodeType = 'Diagnosis'
and not exists
			(
			select
				1
			from
				Neonatal.DiagnosisBase
			where
				NNUCodedItems.Code = DiagnosisBase.DiagnosisCode
			)
group by
	Code

--Drug

insert into Neonatal.DrugBase
(
DrugCode
,Drug
)

select
	Code
	,min(Name) -- multiple names for each code, get the first
from
	[$(Bnet_DSS_business)].[bnf_dbsync].[NNUCodedItems]
where
	CodeType = 'Drug'
and not exists
			(
			select
				1
			from
				Neonatal.DrugBase
			where
				NNUCodedItems.Code = DrugBase.DrugCode
			)
group by
	Code

--Procedure

insert into Neonatal.ProcedureBase
(
ProcedureCode
,[Procedure]
)

select
	Code
	,min(Name) -- multiple names for each code, get the first
from
	[$(Bnet_DSS_business)].[bnf_dbsync].[NNUCodedItems]
where
	CodeType = 'Procedure'
and not exists
			(
			select
				1
			from
				Neonatal.ProcedureBase
			where
				NNUCodedItems.Code = ProcedureBase.ProcedureCode
			)
group by
	Code

--Management

insert into Neonatal.ManagementBase
(
ManagementCode
,Management
)

select
	Code
	,min(Name) -- multiple names for each code, get the first
from
	[$(Bnet_DSS_business)].[bnf_dbsync].[NNUCodedItems]
where
	CodeType = 'Management'
and not exists
			(
			select
				1
			from
				Neonatal.ManagementBase
			where
				NNUCodedItems.Code = ManagementBase.ManagementCode
			)
group by
	Code

--Oxygen

insert into Neonatal.OxygenBase
(
OxygenCode
,Oxygen
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'AddedO2'
and not exists
			(
			select
				1
			from
				Neonatal.OxygenBase
			where
				DaySum.[Lookup] = OxygenBase.OxygenCode
			)

--Additive

insert into Neonatal.AdditiveBase
(
AdditiveCode
,Additive
)


select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'Additives'
and not exists
			(
			select
				1
			from
				Neonatal.AdditiveBase
			where
				DaySum.[Lookup] = AdditiveBase.AdditiveCode
			)



--LOC

insert into Neonatal.NeonatalLevelOfCareBase
(
NeonatalLevelOfCareCode
,NeonatalLevelOfCare
,Year
)


select
	[Lookup]
	,ltrim([Description])
	,'2001'
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'BAPM2001'
and not exists
			(
			select
				1
			from
				Neonatal.NeonatalLevelOfCareBase
			where
				DaySum.[Lookup] = NeonatalLevelOfCareBase.NeonatalLevelOfCareCode
			and	NeonatalLevelOfCareBase.Year = '2001' 
			)

insert into Neonatal.NeonatalLevelOfCareBase
(
NeonatalLevelOfCareCode
,NeonatalLevelOfCare
,Year
)


select
	[Lookup]
	,ltrim([Description])
	,'2011'
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'BAPM2011'
and not exists
			(
			select
				1
			from
				Neonatal.NeonatalLevelOfCareBase
			where
				DaySum.[Lookup] = NeonatalLevelOfCareBase.NeonatalLevelOfCareCode
			and	NeonatalLevelOfCareBase.Year = '2011' 
			)

--BloodProduct

insert into Neonatal.BloodProductBase
(
BloodProductCode
,BloodProduct
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'BloodProducts'
and not exists
			(
			select
				1
			from
				Neonatal.BloodProductBase
			where
				DaySum.[Lookup] = BloodProductBase.BloodProductCode
			)



insert into Neonatal.CarerStatusBase
(
CarerStatusCode
,CarerStatus
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'CarerStatus'
and not exists
			(
			select
				1
			from
				Neonatal.CarerStatusBase
			where
				DaySum.[Lookup] = CarerStatusBase.CarerStatusCode
			)

--Consciousness

insert into Neonatal.ConsciousnessBase
(
ConsciousnessCode
,Consciousness
)


select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'Consciousness'
and not exists
			(
			select
				1
			from
				Neonatal.ConsciousnessBase
			where
				DaySum.[Lookup] = ConsciousnessBase.ConsciousnessCode
			)

--CPAPMode

insert into Neonatal.CPAPModeBase
(
CPAPModeCode
,CPAPMode
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'CPAPMode'
and not exists
			(
			select
				1
			from
				Neonatal.CPAPModeBase
			where
				DaySum.[Lookup] = CPAPModeBase.CPAPModeCode
			)

--EnteralFeeds

insert into Neonatal.EnteralFeedBase
(
EnteralFeedCode
,EnteralFeed
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'EnteralFeeds'
and not exists
			(
			select
				1
			from
				Neonatal.EnteralFeedBase
			where
				DaySum.[Lookup] = EnteralFeedBase.EnteralFeedCode
			)

--FeedingMethod

insert into Neonatal.FeedingMethodBase
(
FeedingMethodCode
,FeedingMethod
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'FeedingMethod'
and not exists
			(
			select
				1
			from
				Neonatal.FeedingMethodBase
			where
				DaySum.[Lookup] = FeedingMethodBase.FeedingMethodCode
			)

--FormulaName

insert into Neonatal.FormulaBase
(
FormulaCode
,Formula
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'FormulaName'
and not exists
			(
			select
				1
			from
				Neonatal.FormulaBase
			where
				DaySum.[Lookup] = FormulaBase.FormulaCode
			)

--HRG

insert into Neonatal.HRGBase
(
HRGCode
,HRG
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'HRG'
and not exists
			(
			select
				1
			from
				Neonatal.HRGBase
			where
				DaySum.[Lookup] = HRGBase.HRGCode
			)

--NECTreatment

insert into Neonatal.NecrotizingEntercolitisTreatmentBase
(
NecrotizingEntercolitisTreatmentCode
,NecrotizingEntercolitisTreatment
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'NECTreatment'
and not exists
			(
			select
				1
			from
				Neonatal.NecrotizingEntercolitisTreatmentBase
			where
				DaySum.[Lookup] = NecrotizingEntercolitisTreatmentBase.NecrotizingEntercolitisTreatmentCode
			)


--NursingStatus

insert into Neonatal.NursingStatusBase
(
NursingStatusCode
,NursingStatus
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'NursingStatus'
and not exists
			(
			select
				1
			from
				Neonatal.NursingStatusBase
			where
				DaySum.[Lookup] = NursingStatusBase.NursingStatusCode
			)

--ObservationsMonitoring

insert into Neonatal.ObservationMonitoringBase
(
ObservationMonitoringCode
,ObservationMonitoring
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'ObservationsMonitoring'
and not exists
			(
			select
				1
			from
				Neonatal.ObservationMonitoringBase
			where
				DaySum.[Lookup] = ObservationMonitoringBase.ObservationMonitoringCode
			)

--RespSupport

insert into Neonatal.RespiratorySupportBase
(
RespiratorySupportCode
,RespiratorySupport
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'RespSupport'
and not exists
			(
			select
				1
			from
				Neonatal.RespiratorySupportBase
			where
				DaySum.[Lookup] = RespiratorySupportBase.RespiratorySupportCode
			)

--Tracheostomy

insert into Neonatal.TracheostomyBase
(
TracheostomyCode
,Tracheostomy
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'Tracheostomy'
and not exists
			(
			select
				1
			from
				Neonatal.TracheostomyBase
			where
				DaySum.[Lookup] = TracheostomyBase.TracheostomyCode
			)

--TracheostomyCarer

insert into Neonatal.TracheostomyCarerBase
(
TracheostomyCarerCode
,TracheostomyCare
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'TracheostomyCarer'
and not exists
			(
			select
				1
			from
				Neonatal.TracheostomyCarerBase
			where
				DaySum.[Lookup] = TracheostomyCarerBase.TracheostomyCarerCode
			)

--VentilationMode


insert into Neonatal.VentilationModeBase
(
VentilationModeCode
,VentilationMode
)

select
	[Lookup]
	,ltrim([Description])
from
	[$(DataDictionary)].Badger.DaySum
where
	Field = 'VentilationMode'
and not exists
			(
			select
				1
			from
				Neonatal.VentilationModeBase
			where
				DaySum.[Lookup] = VentilationModeBase.VentilationModeCode
			)





﻿CREATE procedure [ETL].[ExtractSIDEncounter]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDEncounter;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDEncounter
(
	 CandidateID
	,DateCreated
	,StatusID
	,CaseNoteNumber
	,AENumber
	,NHSNumber
	,DistrictNumber
	,Surname
	,Forenames
	,DOB
	,Gender
	,DateSubmitted
	,SpellID
	,CandidateDateCreated
	,CandidateStatusID
	,CandidateTIE
	,DateDeparture
	,Address1
	,Address2
	,Address3
	,Postcode
	,GPName
	,GPAddress
	,DateHospitalArrival
	,HoursInHospitalVal
	,TIE
)

select 
	 CandidateID
	,DateCreated
	,StatusID
	,CaseNoteNumber
	,AENumber
	,NHSNumber
	,DistrictNumber
	,Surname
	,Forenames
	,DOB
	,Gender
	,DateSubmitted
	,SpellID = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/SpellID)[1]', 'varchar(max)'),'')
	,CandidateDateCreated = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/CandidateDateCreated)[1]', 'varchar(max)'),'')
	,CandidateStatusID = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/CandidateStatusID)[1]', 'varchar(max)'),'')
	,CandidateTIE = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/CandidateTIE)[1]', 'varchar(max)'),'')
	,DateDeparture = 
		case 
		when Candidate.TIEXMLData.value('(/CandidateGridDataDTO/DateDeparture)[1]', 'varchar(max)') = '0001-01-01T00:00:00' then null 
		else nullif(convert(datetime,replace(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/DateDeparture)[1]', 'varchar(max)'),'T',' ' )),'')
		end
	,Address1 = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/Address1)[1]', 'varchar(max)'),'')
	,Address2 = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/Address2)[1]', 'varchar(max)'),'')
	,Address3 = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/Address3)[1]', 'varchar(max)'),'')
	,Postcode = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/Postcode)[1]', 'varchar(max)'),'')
	,GPName = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/GPName)[1]', 'varchar(max)'),'')
	,GPAddress = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/GPAddress)[1]', 'varchar(max)'),'')
	,DateHospitalArrival = 
		case 
		when Candidate.TIEXMLData.value('(/CandidateGridDataDTO/DateHospitalArrival)[1]', 'varchar(max)') = '0001-01-01T00:00:00' then null 
		else nullif(convert(datetime,replace(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/DateHospitalArrival)[1]', 'varchar(max)'),'T',' ' )),'')
		end
	,HoursInHospitalVal = 
		nullif(Candidate.TIEXMLData.value('(/CandidateGridDataDTO/HoursInHospitalVa)[1]', 'varchar(max)'),'')
	,TIE 

from 
	[$(SID)].dbo.TARNCandidate Candidate


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDEncounter', @Stats, @StartTime





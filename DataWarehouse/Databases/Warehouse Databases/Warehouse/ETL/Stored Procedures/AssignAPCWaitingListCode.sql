﻿CREATE procedure [ETL].[AssignAPCWaitingListCode] as

update APC.Encounter
set
	WaitingListCode = WLA.WaitingListCode
from
	APC.Encounter

left join APC.WaitingListActivity WLA
on    Encounter.SourcePatientNo = WLA.SourcePatientNo
and   Encounter.SourceSpellNo = WLA.SourceEntityRecno

where 
	not exists
        (
        select
              1
        from
              APC.WaitingListActivity Later
        where
              Later.SourcePatientNo = WLA.SourcePatientNo
        and   Later.SourceEntityRecno = WLA.SourceEntityRecno
        and   Later.ActivityTime > WLA.ActivityTime
        )

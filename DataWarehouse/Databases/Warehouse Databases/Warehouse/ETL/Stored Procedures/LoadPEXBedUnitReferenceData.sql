﻿CREATE proc [ETL].[LoadPEXBedUnitReferenceData]

as

truncate table PEX.BedUnitLocation

insert into PEX.BedUnitLocation

select 
		LocationID = cast([WardIdentifier] as bigint)
		,Location = [WardName]
		,LocationTypeID = 2
		,SiteID = [HospitalIdentifier]
		,Site = [HospitalName]		
FROM
  [$(SmallDatasets)].[Hospedia].[HospitalWards]



truncate table PEX.BedUnitQuestion

insert into PEX.BedUnitQuestion

select distinct
	QuestionID = cast([questionIdentifier] as int)
	,Question = [question]
	,QuestionType = coalesce(XrefEntityCode, 0)
from 
	[$(SmallDatasets)].[Hospedia].[Questions]

left join dbo.EntityXref QuestionType
on	Questions.questionIdentifier = QuestionType.EntityCode
and QuestionType.EntityTypeCode in ('PEXQUESTION')
	
left join dbo.EntityLookup
on	EntityLookup.EntityTypeCode = QuestionType.EntityTypeCode
and	EntityLookup.EntityCode = QuestionType.EntityCode
and EntityLookup.EntityTypeCode in ('PEXQUESTION')	
	
	
	
	
truncate table PEX.BedUnitSurvey

insert into PEX.BedUnitSurvey

select
	SurveyID = cast([SurveyIdentifier] as int)
	,Survey = [SurveyName]
	,ChannelID = 1
from 
	[$(SmallDatasets)].Hospedia.Surveys	



truncate table PEX.BedUnitAnswer

insert into PEX.BedUnitAnswer

select
	AnswerID = cast([answerIdentifier] as int)
	,Answer = [answer]
from 
	[$(SmallDatasets)].[Hospedia].Answers
	
where
	[answer] is not null
	

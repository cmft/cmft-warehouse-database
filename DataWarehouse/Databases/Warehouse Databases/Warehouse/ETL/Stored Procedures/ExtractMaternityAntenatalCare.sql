﻿

CREATE proc [ETL].[ExtractMaternityAntenatalCare]

	@FromDate datetime = null
	,@ToDate datetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @LocalFromDate datetime = @FromDate
declare @LocalToDate datetime = @ToDate

select @StartTime = getdate()

select @RowsInserted = 0

set dateformat dmy	
	
--insert into ETL.MaternityAntenatalCare
--(
--	SourceUniqueID
--	,CasenoteNumber
--	,DistrictNo
--	,NHSNumber
--	,DateOfBirth
--	,AgeAtBooking
--	,GPPracticeCode
--	,VisitDate
--	,VisitTime
--	,VisitTypeCode
--	,FetalMedicineCases
--	,Thrombophilia
--	,SickleCellCode
--	,ThalassemiaCode
--	,Thromboembolism
--	,AntiTNFTreatment
--	,RHIsoimmunisationCode
--	,RenalDisease
--	,Renal
--	,EndocrineCode
--	,AntenatalEndocrineCode
--	,MalignantDisease
--	,HIV
--	,HeartDiseaseCode
--	,CardiacCode
--	,NumberOfFetuses
--	,DiabeticCode
--	,DiabetesCode
--	,RespiratoryProblemCode
--	,EarlyPreTerm
--	,Miscarriage
--	,NeonatalDeath
--	,SecondThirdTrimesterLoss
--	,GrowthRestrictionCode
--	,PreviousBirthWeight
--	,PsychiatricProblemCode
--	,HELLPEclampsia
--	,HypertensionMedication
--	,PreviousOperations
--	,EpilepsyMedication
--	,InheritedDiseases
--	,HepatitisB
--	,HepatitisC
--	,BMIAtBooking
--	,PhysicalDisability
--	,Interpreter
--	,PatientTypeCode
--	,SpecialistMidwifeReferral
--	,AccomodationTypeCode
--	,DrugCode
--	,SafeguardingChildrenCode
--	,SafeguardingWomenCode
--	,HypertensionCode
--    ,HaematologicalDisorderCode
--    ,MedicalHistoryComments
--    ,AutoimmuneDiseaseCode
--    ,EstimatedDeliveryDate
--    ,FirstContactDate
--    ,FirstContactProfessionalCarerTypeCode
--    ,FirstContactOther
--    ,LastMenstrualPeriodDate
--    ,FirstLanguageEnglishCode
--    ,EmploymentStatusMotherCode
--    ,EmploymentStatusPartnerCode
--    ,SmokingStatusCode
--    ,CigarettesPerDay
--    ,AlcoholUnitsPerWeek
--    ,WeightMother
--    ,HeightMother
--	,RubellaOfferDate 
--	,RubellaOfferStatus 
--	,RubellaBloodSampleDate 
--	,RubellaResult 

--	,HepatitisBOfferDate 
--	,HepatitisBOfferStatus 
--	,HepatitisBBloodSampleDate 
--	,HepatitisBResult
	
--	,HaemoglobinopathyOfferDate 
--	,HaemoglobinopathyOfferStatus 
--	,HaemoglobinopathyBloodSampleDate 
--	,HaemoglobinopathyResult
	
--	,DownsSyndromeOfferDate 
--	,DownsSyndromeOfferStatus 
--	,DownsSyndromeBloodSampleDate 
--	,DownsSyndromeInvestigationRiskRatio 
	
--	,PreviousCaesareanSections
--	,PreviousLiveBirths
--	,PreviousStillBirths
--	,PreviousLossesLessThan24Weeks	 	    
    
--	,ModifiedTime
--)
	

SELECT [SourceUniqueID]
      ,[CasenoteNumber]
      ,[DistrictNo]
      ,[NHSNumber]
      ,[DateOfBirth]
      ,[AgeAtBooking]
      ,[GPPracticeCode]
      ,[VisitDate]
      ,[VisitTime]
      ,[VisitTypeCode]
      ,[FetalMedicineCases]
      ,[Thrombophilia]
      ,[SickleCellCode]
      ,[ThalassemiaCode]
      ,[Thromboembolism]
      ,[AntiTNFTreatment]
      ,[RHIsoimmunisationCode]
      ,[RenalDisease]
      ,[Renal]
      ,[EndocrineCode]
      ,[AntenatalEndocrineCode]
      ,[MalignantDisease]
      ,[HIV]
      ,[HeartDiseaseCode]
      ,[CardiacCode]
      ,[NumberOfFetuses]
      ,[DiabeticCode]
      ,[DiabetesCode]
      ,[RespiratoryProblemCode]
      ,[EarlyPreTerm]
      ,[Miscarriage]
      ,[NeonatalDeath]
      ,[SecondThirdTrimesterLoss]
      ,[GrowthRestrictionCode]
      ,[PreviousBirthWeight]
      ,[PsychiatricProblemCode]
      ,[HELLPEclampsia]
      ,[HypertensionMedication]
      ,[PreviousOperations]
      ,[EpilepsyMedication]
      ,[InheritedDiseases]
      ,[HepatitisB]
      ,[HepatitisC]
      ,[BMIAtBooking]
      ,[PhysicalDisability]
      ,[Interpreter]
      ,[PatientTypeCode]
      ,[SpecialistMidwifeReferral]
      ,[AccomodationTypeCode]
      ,[DrugCode]
      ,[SafeguardingChildrenCode]
      ,[SafeguardingWomenCode]
      ,[ModifiedTime]
      ,[HypertensionCode]
      ,[HaematologicalDisorderCode]
      ,[MedicalHistoryComments]
      ,[AutoimmuneDiseaseCode]
      ,[EstimatedDeliveryDate]
      ,[FirstContactDate]
      ,[FirstContactProfessionalCarerTypeCode]
      ,[FirstContactOther]
      ,[LastMenstrualPeriodDate]
      ,[FirstLanguageEnglishCode]
      ,[EmploymentStatusMotherCode]
      ,[EmploymentStatusPartnerCode]
      ,[SmokingStatusCode]
      ,[CigarettesPerDay]
      ,[AlcoholUnitsPerWeek]
      ,[WeightMother]
      ,[HeightMother]
      ,[RubellaOfferDate]
      ,[RubellaOfferStatus]
      ,[RubellaBloodSampleDate]
      ,[RubellaResult]
      ,[HepatitisBOfferDate]
      ,[HepatitisBOfferStatus]
      ,[HepatitisBBloodSampleDate]
      ,[HepatitisBResult]
      ,[HaemoglobinopathyOfferDate]
      ,[HaemoglobinopathyOfferStatus]
      ,[HaemoglobinopathyBloodSampleDate]
      ,[HaemoglobinopathyResult]
      ,[DownsSyndromeOfferDate]
      ,[DownsSyndromeOfferStatus]
      ,[DownsSyndromeBloodSampleDate]
      ,[DownsSyndromeInvestigationRiskRatio]
      ,[PreviousCaesareanSections]
      ,[PreviousLiveBirths]
      ,[PreviousStillBirths]
      ,[PreviousLossesLessThan24Weeks]
into
	#Activity
from
	(						
	select
		SourceUniqueID = rtrim(VISIT.M_NUMBER) + '||' + cast(VISIT.OCCURRENCE as varchar(50)) --+ '||' + convert(varchar, VISIT.VISIT_DATE, 112) + coalesce(VISIT_TIME, '0000')                                                                                      										
		,CasenoteNumber = rtrim(VISIT.M_NUMBER)		
		,DistrictNo = NUMBER_INDEX.F_SYS_NUMBER
		,NHSNumber = NUMBER_INDEX.NHS_NUMBER  								
		,DateOfBirth = cast(MOTHER_REG.M_DOB as date)
		,AgeAtBooking = BK_VISIT_DATA.AGE_AT_BOOKING       
		,GPPracticeCode = GP.GP_PRACTICE	                 										
		,VisitDate = cast(VISIT.VISIT_DATE as date)		
		,VisitTime = 
				cast(
					case 
					when VISIT_TIME is null 
					then convert(varchar, VISIT.VISIT_DATE, 103) + ' ' + '00:00'
					else convert(varchar, VISIT.VISIT_DATE, 103) + ' ' + left(VISIT_TIME, 2) + ':' + right(VISIT_TIME, 2)
					end
				as smalldatetime)	
		,VisitTypeCode = VISIT.VISIT_TYPE		
		,FetalMedicineCases = FETALMEDICINE                 										
		,Thrombophilia = BK_VISIT_DATA.THROMBOPHILIA								
		,SickleCellCode = SICK.RESULT_MENU                       										
		,ThalassemiaCode = THAL.RESULT_MENU                    								
		,Thromboembolism = BK_VISIT_DATA.THROMBOEMBOLISM									
		,AntiTNFTreatment = BK_VISIT_DATA.ANTI_TNF_TREATMENT									
		,RHIsoimmunisationCode = BK_VISIT_DATA.RH_ISOIMMUNISATION							
		,RenalDisease = BK_VISIT_DATA.RENAL_DISEASE -- difference to below?		                      										
		,Renal = AN_SUMMARY.RENAL -- difference to above?										
		,EndocrineCode = BK_VISIT_DATA.OTHER_ENDOCRINE_PROB							
		,AntenatalEndocrineCode = AN_SUMMARY.OTHER_ENDOCRINE_PROB				
		,MalignantDisease = BK_VISIT_DATA.MALIGNANT_DISEASE										
		,HIV = BK_VISIT_DATA.HIV_RESULT										
		,HeartDiseaseCode = BK_VISIT_DATA.HEART_DISEASE 										
		,CardiacCode = AN_SUMMARY.CARDIAC										
		,NumberOfFetuses = AN_SUMMARY.NUMBER_OF_FETUSES    										
		,DiabeticCode = BK_VISIT_DATA.DIABETIC 	-- difference to below?								
		,DiabetesCode = AN_SUMMARY.DIABETES -- difference to above?								
		,RespiratoryProblemCode = BK_VISIT_DATA.RESPIRATORY_PROBLEMS            										
		--,FetalCongenitalAbnormality = ABNORMALITY.FETALCONGENITALABNORMALITY -- derived table brings nothing back									
		,EarlyPreTerm = EARLYPRETERM.EARLYPRETERM 										
		,Miscarriage = MISCARRIAGE.MISCARRIAGE										
		,NeonatalDeath = NEONATALDEATH.NEONATALDEATHSTILLBIRTH										
		,SecondThirdTrimesterLoss = LOSS.SECONDTHIRDTRILOSS										
		--,PlacentaAccreta = PLACENTAACCRETA.PLACENTAACCRETA -- derived table brings nothing back	   										
		,GrowthRestrictionCode = GROWTHRES.GROWTHRESTRICTION										
		,PreviousBirthWeight = LOWWEIGHT.PREVIOUSBIRTHWEIGHT										
		,PsychiatricProblemCode = BK_VISIT_DATA.PSYCHIATRIC_PROBLEMS                                      										
		,HELLPEclampsia = HELLP.HELLPECLAMPSIA 										
		,HypertensionMedication = BK_VISIT_DATA.HYPERTENSION_MEDICATION                										
		,PreviousOperations = PASTOPS.PREVIOUS_OPERATIONS
		,EpilepsyMedication = BK_VISIT_DATA.EPILEPSY_MEDICATION
		,InheritedDiseases = BK_VISIT_DATA.FH_INHERITED_DISEASES
		,HepatitisB = HEPB.RESULT_MENU
		,HepatitisC = HEPC.RESULT_MENU                        								
		,BMIAtBooking = BK_VISIT_DATA.BMI_AT_BOOKING										
		,PhysicalDisability = BK_VISIT_DATA.PHYS_DISABILITY                                                                           										
		,Interpreter = BK_VISIT_DATA.INTERPRETER 										
		,PatientTypeCode = PREGNANCY.TYPE_OF_PATIENT										
		,SpecialistMidwifeReferral = BK_VISIT_DATA.SPECIALIST_MW_REFERRAL										
		,AccomodationTypeCode = BK_VISIT_DATA.ACCOMMODATION_TYPE										
		,DrugCode = left(BK_VISIT_DATA.DRUGS,1) --needs re working as now can hold Multiple values										
		,SafeguardingChildrenCode = BK_VISIT_DATA.SAFEGUARDING_CHILDREN										
		,SafeguardingWomenCode = BK_VISIT_DATA.SAFEGUARDING_WOMEN	
		,HypertensionCode = BK_VISIT_DATA.HYPERTENSION	
		,HaematologicalDisorderCode = BK_VISIT_DATA.HAEMOGLOBINOPATHY
		,MedicalHistoryComments = BK_VISIT_DATA.MEDICAL_HISTORY_COMMENTS
		,AutoimmuneDiseaseCode = BK_VISIT_DATA.AUTO_IMMUNE_DISEASE
		,EstimatedDeliveryDate = DATE_EDD_LNMP
		,FirstContactDate = PREGNANCY.FIRST_CONTACT_DATE
		,FirstContactProfessionalCarerTypeCode = PREGNANCY.FIRST_CONTACT 
		,FirstContactOther = PREGNANCY.FIRST_CONTACT_OTHER
		,LastMenstrualPeriodDate = PREGNANCY.LMP_AT_REFERRAL
		,FirstLanguageEnglishCode = 
				Case
					when BK_VISIT_DATA.FIRST_LANGUAGE_ENGLISH = 'Y' then 1
					when BK_VISIT_DATA.FIRST_LANGUAGE_ENGLISH = 'N' then 0
				else null
				end
			
		,EmploymentStatusMotherCode = PREGNANCY.EMPLOYMENT_STATUS
		,EmploymentStatusPartnerCode = BK_VISIT_DATA.P_EMPLOYMENT_STATUS

		,SmokingStatusCode = BK_VISIT_DATA.BK_SMOKING_STAGE
		,CigarettesPerDay = BK_VISIT_DATA.CIGARETTES_PER_DAY
		,AlcoholUnitsPerWeek = BK_VISIT_DATA.ALCOHOL_UNITS
		,WeightMother = convert(float,BK_VISIT_DATA.WEIGHT)
		,HeightMother = convert(float,BK_VISIT_DATA.HEIGHT) 
		
		
		,RubellaOfferDate = case when INVESTIGATIONS.RUBELLA_STATUS is not null then INVESTIGATIONS.TEST_DATE else null end
		,RubellaOfferStatus = INVESTIGATIONS.RUBELLA_STATUS
		,RubellaBloodSampleDate = case when INVESTIGATIONS.RUBELLA_STATUS = 'D' then INVESTIGATIONS.TEST_DATE else null end
		,RubellaResult = INVESTIGATIONS.RUBELLA_RESULT
	
		,HepatitisBOfferDate = HEPB.TEST_DATE
		,HepatitisBOfferStatus = HEPB.TEST_STATUS
		,HepatitisBBloodSampleDate = case when HEPB.TEST_STATUS = 'D' then HEPB.TEST_DATE else null end
		,HepatitisBResult = HEPB.RESULT_MENU
		
		,HaemoglobinopathyOfferDate = case when INVESTIGATIONS.HB_STATUS is not null then INVESTIGATIONS.TEST_DATE else null end
		,HaemoglobinopathyOfferStatus = INVESTIGATIONS.HB_STATUS
		,HaemoglobinopathyBloodSampleDate = case when INVESTIGATIONS.HB_STATUS = 'D' then INVESTIGATIONS.TEST_DATE else null end
		,HaemoglobinopathyResult =	HB_RESULT	
		
		,DownsSyndromeOfferDate = case when AN_SUMMARY.AN_SCREENING is not null then  VISIT.VISIT_DATE else null end
		,DownsSyndromeOfferStatus = AN_SUMMARY.AN_SCREENING
		,DownsSyndromeBloodSampleDate =	case when AN_SUMMARY.AN_SCREENING = 'A' then  VISIT.VISIT_DATE else null end
		,DownsSyndromeInvestigationRiskRatio = BK_VISIT_DATA.DOWNS_RISK
		
		,PreviousCaesareanSections =
		
				(										
				select     										
														
					COUNT(*) 								
				from          										
					[$(CMISStaging)].SMMIS.PREV_BIRTHS									
				where      										
					MODE_OF_DELIVERY in ('C','E','U','D')									
					and PREV_BIRTHS.M_NUMBER = VISIT.M_NUMBER	
					and PREV_BIRTHS.DATE_OF_BIRTH < VISIT.VISIT_DATE						
				)
		 
		,PreviousLiveBirths =
				(										
				select										
														
					count(*) 									
				from										
					[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
				where 										
					OUTCOME	= 'L'	
					and PREV_PREGNANCY.M_NUMBER = VISIT.M_NUMBER
					and PREV_PREGNANCY.PREG_END_DATE < VISIT.VISIT_DATE				
												
				)
		
		
		,PreviousStillBirths =
		
					(										
					select										
															
						count(*) 									
					from										
						[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
					where 										
						OUTCOME	= 'S'	
						and PREV_PREGNANCY.M_NUMBER = VISIT.M_NUMBER
						and PREV_PREGNANCY.PREG_END_DATE < VISIT.VISIT_DATE				
					)	
		,PreviousLossesLessThan24Weeks =
		
					(										
					select										
															
						count(*) 									
					from										
						[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
					where 										
						OUTCOME	not in ('L','U')	
						and PREV_PREGNANCY.M_NUMBER = VISIT.M_NUMBER
						and PREV_PREGNANCY.PREG_END_DATE < VISIT.VISIT_DATE	
						and GESTATION < 24			
					)
						
		
		,ModifiedTime = VISIT.DATE_STAMP
											
	from											
		[$(CMISStaging)].SMMIS.VISIT										
												
	inner join [$(CMISStaging)].SMMIS.PREGNANCY										
	on	PREGNANCY.M_NUMBER = VISIT.M_NUMBER	collate Latin1_General_CS_AS															
	and PREGNANCY.OCCURRENCE = VISIT.OCCURRENCE										
											
	inner join [$(CMISStaging)].SMMIS.MOTHER_REG										
	on MOTHER_REG.M_NUMBER = VISIT.M_NUMBER	collate Latin1_General_CS_AS														
											
	inner join [$(CMISStaging)].SMMIS.BK_VISIT_DATA										
	on	BK_VISIT_DATA.M_NUMBER = VISIT.M_NUMBER	collate Latin1_General_CS_AS														
	and	BK_VISIT_DATA.OCCURRENCE = VISIT.OCCURRENCE     										
											
	inner join [$(CMISStaging)].SMMIS.AN_SUMMARY 										
	ON	AN_SUMMARY.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS															
	and AN_SUMMARY.OCCURRENCE = VISIT.OCCURRENCE    										
											
	left join [$(CMISStaging)].SMMIS.NUMBER_INDEX										
	ON	NUMBER_INDEX.HOSP_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS												
	and NUMBER_INDEX.MOTHER_INFANT = 'M' 										
											
	left join [$(CMISStaging)].SMMIS.GP 										
	ON	GP.GP_SEQUENCE = MOTHER_REG.GP_SEQUENCE 										
	and GP.GP_SURNAME = MOTHER_REG.GP_SURNAME 										
	and GP.ACTIVE = 'Y'										
											
	left join [$(CMISStaging)].SMMIS.BK_INVESTIGATIONS HEPB 										
	ON	HEPB.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS										
	and HEPB.OCCURRENCE = VISIT.OCCURRENCE 										
	and HEPB.TYPE = '10' 										
											
	left join [$(CMISStaging)].SMMIS.BK_INVESTIGATIONS HEPC 										
	on	HEPC.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS									
	and HEPC.OCCURRENCE = VISIT.OCCURRENCE 										
	and HEPC.TYPE = '12' 										
											
	left join [$(CMISStaging)].SMMIS.BK_INVESTIGATIONS SICK 										
	on	SICK.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS									
	and SICK.OCCURRENCE = VISIT.OCCURRENCE 										
	and SICK.TYPE = '5' 										
											
	left join [$(CMISStaging)].SMMIS.BK_INVESTIGATIONS THAL 										
	on	THAL.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS									
	and THAL.OCCURRENCE = VISIT.OCCURRENCE 										
	and THAL.TYPE = '6'	
	
	left join [$(CMISStaging)].SMMIS.BK_INVESTIGATIONS_VW INVESTIGATIONS
	on INVESTIGATIONS.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS									
	and INVESTIGATIONS.OCCURRENCE = VISIT.OCCURRENCE 	
												
	left join 										
		(										
		select     										
			M_NUMBER									
			,COUNT(*) AS FETALMEDICINE									
		from          										
			[$(CMISStaging)].SMMIS.PREV_BIRTHS									
		where      										
			FETAL_MEDICINE_REQD = 'Y'									
		group by										
			M_NUMBER									
		) FETALMED 										
	on	FETALMED.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS								
											
	left join										
		(										
		select										
			M_NUMBER									
			,count(*) AS PREVIOUS_OPERATIONS									
		from										
			[$(CMISStaging)].SMMIS.PAST_OPS									
		where 										
			OPERATION_CHAR <> 'O'									
		group by										
			M_NUMBER									
		) PASTOPS 										
	ON	PASTOPS.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS											
											
	left join										
		(										
		select										
			M_NUMBER 									
			,HELLPECLAMPSIA = count(*) 									
		from										
			[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
		where 										
			(									
				HELLP is not null 									
			or	PRE_ECLAMPSIA is not null 									
			or	ECLAMPSIA is not null									
			)									
		and									
			(									
				HELLP > 0 									
			or	PRE_ECLAMPSIA > 0 									
			or	ECLAMPSIA > 0									
			)									
		group by 										
			M_NUMBER									
		) HELLP										
	on	HELLP.M_NUMBER = VISIT.M_NUMBER	collate Latin1_General_CS_AS									
											
	--left join										
	--	(										
	--	select										
	--		 PREV_BIRTHS.M_NUMBER 									
	--		,FETALCONGENITALABNORMALITY = count(*) 									
	--	from										
	--		[$(CMISStaging)].SMMIS.PREV_BIRTHS									
	--	where 										
	--		FETAL_CONGENITAL_ABNORMALITY = 'Y'									
	--	group by 										
	--		PREV_BIRTHS.M_NUMBER									
	--	) ABNORMALITY										
	--on	ABNORMALITY.M_NUMBER = VISIT.M_NUMBER										
											
	left join										
		(										
		select										
			 M_NUMBER 									
			,EARLYPRETERM = COUNT(*) 									
		from										
			[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
		where 										
			OUTCOME = 'L'									
		and GESTATION < '34'									
		group by 										
			M_NUMBER									
		) as EARLYPRETERM										
	on	EARLYPRETERM.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS											
											
	left join										
		(										
		select										
			M_NUMBER 									
			,MISCARRIAGE = COUNT(*)  									
		from										
			[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
		where 										
			OUTCOME = 'M'									
		group by 										
			M_NUMBER									
		) as MISCARRIAGE										
	on	MISCARRIAGE.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS										
											
	left join										
		(										
		select										
			M_NUMBER 									
			,NEONATALDEATHSTILLBIRTH = COUNT(*) 									
		from										
			[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
		where 										
			OUTCOME In ('N','S')									
		group by 										
			M_NUMBER									
		) as NEONATALDEATH										
	on	NEONATALDEATH.M_NUMBER = VISIT.M_NUMBER	collate Latin1_General_CS_AS										
											
	left join										
		(										
		select									
			M_NUMBER 								
			,SECONDTHIRDTRILOSS = COUNT(*)  								
		from									
			[$(CMISStaging)].SMMIS.PREV_PREGNANCY								
		where 									
			COALESCE(OUTCOME,'N') not in ('S','N','L','T')								
		and GESTATION > '12'								
		group by 									
			M_NUMBER								
		) as LOSS										
	on	LOSS.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS											
											
	--left join										
	--	(										
	--	select										
	--		M_Number 									
	--		,PLACENTAACCRETA = COUNT(*) 									
	--	from										
	--		[$(CMISStaging)].SMMIS.PREV_PREGNANCY									
	--	where 										
	--		PLACENTA_ACCRETA is not null 									
	--	and PLACENTA_ACCRETA >0									
	--	group by 										
	--		M_Number									
	--	) as PLACENTAACCRETA										
	--on	PLACENTAACCRETA.M_NUMBER = VISIT.M_NUMBER									
																					
	left join										
		(										
		select										
			 PREV_BIRTHS.M_NUMBER 									
			,GROWTHRESTRICTION = COUNT(*)									
		from										
			[$(CMISStaging)].SMMIS.PREV_BIRTHS									
		where 										
			CENTILE is not null									
			and CENTILE <= '10'									
		group by 										
			PREV_BIRTHS.M_NUMBER									
		) as GROWTHRES										
	on	GROWTHRES.M_NUMBER = VISIT.M_NUMBER collate Latin1_General_CS_AS									
											
	left join										
		(										
		select										
			 PREV_PREGNANCY.M_NUMBER 									
			,PREVIOUSBIRTHWEIGHT = count(*)							
		from										
			[$(CMISStaging)].SMMIS.PREV_BIRTHS		
																	
		inner join [$(CMISStaging)].SMMIS.PREV_PREGNANCY									
		on PREV_BIRTHS.M_NUMBER	= PREV_PREGNANCY.M_NUMBER								
		and PREV_BIRTHS.DATE_OF_BIRTH =	PREV_PREGNANCY.PREG_END_DATE	
								
		where 										
			WEIGHT_GMS is not null									
			and (									
					WEIGHT_GMS < 2500								
				or	WEIGHT_GMS > 4500								
				) 								
			and GESTATION >= 37 									
		group by 										
			PREV_PREGNANCY.M_NUMBER							
		) as LOWWEIGHT										
	on	LOWWEIGHT.M_NUMBER = VISIT.M_NUMBER	collate Latin1_General_CS_AS									
												
	where											
		VISIT.VISIT_TYPE = 'B'								
	and PREGNANCY.UNBOOKED in ('B','C','3','4')
	and VISIT.DATE_STAMP between @LocalFromDate and @LocalToDate	
	--and	VISIT.M_NUMBER = 'M09/077635'
	--and	VISIT.VISIT_DATE =  '26 nov 2009'
	
	

	) Antenatal                                                           										

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


insert into ETL.MaternityAntenatalCare
select
	*
from
	#Activity


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	

﻿





CREATE proc [ETL].[LoadCHAMPEncounter] as

/*
When		Who			What
=========================================================================================
16/01/2015	Paul Egan	Initial Coding
03/03/2015	Paul Egan	Added IsAccountRegistered
=========================================================================================
*/


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
;

declare
	 @deleted int
	,@inserted int
	,@updated int
;

declare
	@MergeSummary TABLE(Action nvarchar(10))
;


merge
	COM.CHAMPEncounter target
using
	(
	select 
		 CRMChildID		-- Not unique in this dataset
		,CRMAssessmentID
		,SchoolCode
		,YearGroup
		,SchoolYear
		,AssessmentTime
		,MckessonID
		,NHSNumber
		,Forename
		,Surname
		,DateOfBirth
		,SexCode
		,OptedOut
		,IsVisited
		,IsMeasured
		,IsMailed
		,IsAccountRegistered
		,Centile91
		,Centile98
		,Centile99
		,Height
		,[Weight]
		,BMI
		,Classification
		,ClassificationOrder
		,idx
		,AgeMonths
		,AgeInMonthsAtMeasurementDate
		,ActivityCode
		,ReasonOtherDescription
		,AlternativeCode
		,CodeDescription
		,LastUpdated
		,CreatedByUserID
		,InterfaceCode
	from 
		ETL.TLoadCHAMPEncounter
	) source
	on	source.CRMChildID = target.CRMChildID
	and source.CRMAssessmentID = target.CRMAssessmentID

	when not matched by source
	
	then delete
	
	when not matched
	then
		insert
			(
		   CRMChildID
           ,CRMAssessmentID
           ,SchoolCode
           ,YearGroup
           ,SchoolYear
           ,AssessmentTime
           ,MckessonID
           ,NHSNumber
           ,Forename
           ,Surname
           ,DateOfBirth
           ,SexCode
           ,OptedOut
           ,IsVisited
           ,IsMeasured
           ,IsMailed
           ,IsAccountRegistered
           ,Centile91
           ,Centile98
           ,Centile99
           ,Height
           ,Weight
           ,BMI
           ,Classification
           ,ClassificationOrder
           ,idx
           ,AgeMonths
           ,AgeInMonthsAtMeasurementDate
           ,ActivityCode
           ,ReasonOtherDescription
           ,AlternativeCode
           ,CodeDescription
           ,LastUpdated
           ,CreatedByUserID
           ,InterfaceCode
           ,Created
           ,Updated
           ,ByWhom
           )
		values
			(
			source.CRMChildID		-- Not unique in this dataset
			,source.CRMAssessmentID
			,source.SchoolCode
			,source.YearGroup
			,source.SchoolYear
			,source.AssessmentTime
			,source.MckessonID
			,source.NHSNumber
			,source.Forename
			,source.Surname
			,source.DateOfBirth
			,source.SexCode
			,source.OptedOut
			,source.IsVisited
			,source.IsMeasured
			,source.IsMailed
			,source.IsAccountRegistered
			,source.Centile91
			,source.Centile98
			,source.Centile99
			,source.Height
			,source.[Weight]
			,source.BMI
			,source.Classification
			,source.ClassificationOrder
			,source.idx
			,source.AgeMonths
			,source.AgeInMonthsAtMeasurementDate
			,source.ActivityCode
			,source.ReasonOtherDescription
			,source.AlternativeCode
			,source.CodeDescription
			,source.LastUpdated
			,source.CreatedByUserID
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
		)
		
	when matched
	and not
		(
			isnull(target.SchoolCode, '') = isnull(source.SchoolCode, '')
		and isnull(target.YearGroup, '') = isnull(source.YearGroup, '')
		and isnull(target.SchoolYear, '') = isnull(source.SchoolYear, '')
		and isnull(target.AssessmentTime, '19000101') = isnull(source.AssessmentTime, '19000101')
		and isnull(target.MckessonID, '') = isnull(source.MckessonID, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.Forename, '') = isnull(source.Forename, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.DateOfBirth, '19000101') = isnull(source.DateOfBirth, '19000101')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.OptedOut, '') = isnull(source.OptedOut, '')
		and isnull(target.IsVisited, 0) = isnull(source.IsVisited, 0)
		and isnull(target.IsMeasured, 0) = isnull(source.IsMeasured, 0)
		and isnull(target.IsMailed, 0) = isnull(source.IsMailed, 0)
		and isnull(target.IsAccountRegistered, 0) = isnull(source.IsAccountRegistered, 0)
		and isnull(target.Centile91, 0) = isnull(source.Centile91, 0)
		and isnull(target.Centile98, 0) = isnull(source.Centile98, 0)
		and isnull(target.Centile99, 0) = isnull(source.Centile99, 0)
		and isnull(target.Height, 0) = isnull(source.Height, 0)
		and isnull(target.[Weight], 0) = isnull(source.[Weight], 0)
		and isnull(target.BMI, 0) = isnull(source.BMI, 0)
		and isnull(target.Classification, '') = isnull(source.Classification, '')
		and isnull(target.ClassificationOrder, 0) = isnull(source.ClassificationOrder, 0)
		and isnull(target.idx, 0) = isnull(source.idx, 0)
		and isnull(target.AgeMonths, '') = isnull(source.AgeMonths, '')
		and isnull(target.AgeInMonthsAtMeasurementDate, '') = isnull(source.AgeInMonthsAtMeasurementDate, '')
		and isnull(target.ActivityCode, '') = isnull(source.ActivityCode, '')
		and isnull(target.ReasonOtherDescription, '') = isnull(source.ReasonOtherDescription, '')
		and isnull(target.AlternativeCode, '') = isnull(source.AlternativeCode, '')
		and isnull(target.CodeDescription, '') = isnull(source.CodeDescription, '')
		and isnull(target.LastUpdated, '19000101') = isnull(source.LastUpdated, '19000101')
		and isnull(target.CreatedByUserID, '') = isnull(source.CreatedByUserID, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			target.SchoolCode = source.SchoolCode
			,target.YearGroup = source.YearGroup
			,target.SchoolYear = source.SchoolYear
			,target.AssessmentTime = source.AssessmentTime
			,target.MckessonID = source.MckessonID
			,target.NHSNumber = source.NHSNumber
			,target.Forename = source.Forename
			,target.Surname = source.Surname
			,target.DateOfBirth = source.DateOfBirth
			,target.SexCode = source.SexCode
			,target.OptedOut = source.OptedOut
			,target.IsVisited = source.IsVisited
			,target.IsMeasured = source.IsMeasured
			,target.IsMailed = source.IsMailed
			,target.IsAccountRegistered = source.IsAccountRegistered
			,target.Centile91 = source.Centile91
			,target.Centile98 = source.Centile98
			,target.Centile99 = source.Centile99
			,target.Height = source.Height
			,target.[Weight] = source.[Weight]
			,target.BMI = source.BMI
			,target.Classification = source.Classification
			,target.ClassificationOrder = source.ClassificationOrder
			,target.idx = source.idx
			,target.AgeMonths = source.AgeMonths
			,target.AgeInMonthsAtMeasurementDate = source.AgeInMonthsAtMeasurementDate
			,target.ActivityCode = source.ActivityCode
			,target.ReasonOtherDescription = source.ReasonOtherDescription
			,target.AlternativeCode = source.AlternativeCode
			,target.CodeDescription = source.CodeDescription
			,target.LastUpdated = source.LastUpdated
			,target.CreatedByUserID = source.CreatedByUserID
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime







﻿
--use Warehouse
CREATE Procedure [ETL].[ExtractK2CTGAssessments] 

As

Truncate table ETL.TImportK2CTGAssessments ;

With CTGAnnotationCTE
	(SessionNumber
	,FetusNumber
	,CTGSummaryTime
	,CTGBuddyTime
	,[Description]
	,SignedBy
	,StaffGroup
	,SequenceNo
	)
as

	(
	select 
		EFM.SessionNumber
		,FetusNumber = EFM.Fetus_Number
		,CTGSummaryTime = EFM.TimeEntered
		,CTGBuddyTime = CTGAnn.TimeEntered
		,[Description]
		,SignedBy
		,StaffGroup = case
						when StaffGroup is null then 'Unable to Group'
						else StaffGroup
					end
		,SequenceNo = row_number ( ) over (partition by EFM.SessionNumber,EFM.Fetus_Number,EFM.TimeEntered order by CTGAnn.TimeEntered,Description asc)
	from 
		[$(K2)].dbo.EFMSummary EFM
	inner join
		(select 
			SessionNumber
			,TimeEntered
			,[Description]
			,SignedBy =
				 case 
					when Description like 'Trace was reviewed by%' then substring(Description,23,len(Description))
					when Description like 'Signed by%' then substring(Description,11,len(Description))
					else null 
				end
		from 
			[$(K2)].dbo.[CTGAnnotations] 
		where Annotation = 'Signed'
		) CTGAnn
	on EFM.SessionNumber = CTGAnn.SessionNumber 
	and datediff(mi,EFM.TimeEntered,CTGAnn.TimeEntered) between -10 and 10	
	
	left join ETL.[TLoadK2CTGStaffGroups] Staff
	on CTGAnn.SignedBy = Staff.StaffName

	) 
		
--66455

Insert into ETL.TImportK2CTGAssessments 

select 
   [PatientID] = Pt.PatientID
      ,[HospitalPatientID] = Pt.HospitalPatientID
      ,[CaseNumber] = Pt.CaseNumber
      ,[Surname] = Pt.Surname
      ,[FirstName] = Pt.FirstName
      ,[DoB] = Pt.DOB
      ,[NHSNumber] = Pt.NHSNumber
      ,[StaffID] = Pt.StaffID
      ,[SessionNumber] = EFM.SessionNumber
      ,[FetusNumber] = EFM.Fetus_Number
      ,[OrderID] = row_number() over 
								(partition by EFM.SessionNumber,EFM.Fetus_Number order by EFM.TimeEntered)
      ,[DateEntered] = cast(EFM.TimeEntered as date)
      ,[TimeEntered] = EFM.TimeEntered
      ,[BuddySigWithin10mins] = case 
									when CTGAnnotationCTE.SessionNumber is not null 
									then 'Y' 
									else null 
								end
      ,[BuddyDescription] = CTGAnnotationCTE.SignedBy
      ,[BuddyStaffGroup] = CTGAnnotationCTE.StaffGroup
      ,[ReviewMaternalPulse] = EFM.[Review_Maternal_Pulse]
      ,[ReviewDilatation] = EFM.[Review_Dilatation]
      ,[Dilatation] = case 
									when EFM.Review_Dilatation is null or EFM.Review_Dilatation <0 or EFM.Review_Dilatation>20
									then 0 
									else 1 
								end
      ,[MaternalPulseRecorded] = case 
									when EFM.Review_Maternal_Pulse is null or EFM.Review_Maternal_Pulse <0 or EFM.Review_Maternal_Pulse >1000
									then 0 
									else 1 
									end
      ,[DilatationRecorded] = case 
									when EFM.Review_Dilatation is null or EFM.Review_Dilatation <0 or EFM.Review_Dilatation>20
									then 0 
									else 1 
								end
      ,[ContractionRate] = EFM.ContractionRate
      ,[CTGEvaluation] = CTGEvaluation
      ,[CTGDescriptionID] = EFM.CTGEvaluation
      ,[CTGDescription] = CTGEv.DisplayText
      ,[BaselineRate] = EFM.BaselineRate
      ,[Variability] = EFM.Variability
      ,[DecelerationDuration] = EFM.Deceleration_Duration
      ,[DecelerationType] = DecelerationType
      ,[DecelerationDescription] = [Dec].DisplayText
      ,[EFMPlan] = EFM_plan
      ,[CTGReviewComment] = CTG_Review_Comment
      ,[CTGReviewAction] = CTG_Review_Action
      ,[CTGReviewActionOther] = CTG_Review_Action_Other
      ,[ReviewLiquorColour] = case when Review_Liquor_Colour is null or Review_Liquor_Colour = '' 
									then Liquor_Condition
									else Review_Liquor_Colour 
								end
      ,[StaffGroup] = Staff.StaffGroup
      ,[StaffInitials] = Initials


from 
	[$(K2)].dbo.EFMSummary EFM 

inner join 
	[$(K2)].[dbo].[List_EFMSummary_CTGEvaluation] CTGEv
on EFM.CTGEvaluation = CTGEv.ID

left join
	(select distinct 
		PatPreg.CaseNumber
		,PatPreg.StaffID
		,Ses.SessionNumber
		,Pat.PatientID
		,Pat.HospitalPatientID
		,Pat.Surname
		,Pat.FirstName
		,Pat.DOB
		,NHSNumber = Pat.NHS_Number
		,Pat.Ethnicity
		,MaritalStatus = Pat.Marital_Status
		,AddressID = Pat.Link_Address_ID
	from 
		[$(K2)].[dbo].[SessionAssignment] Ses
	
	inner join 
		[$(K2)].dbo.PatientPregnancy PatPreg
	on Ses.CaseNumber = PatPreg.CaseNumber
	
	inner join 
		[$(K2)].[dbo].[Patient] Pat
	on PatPreg.PatientID = Pat.PatientID
	
	) Pt
on EFM.SessionNumber = Pt.SessionNumber

left join
	[ETL].[TLoadK2CTGStaffGroups] Staff
on Pt.StaffID = Staff.StaffID

left join 
	[$(K2)].[dbo].[List_EFMSummary_DecelerationType] [Dec]
on EFM.DecelerationType = [Dec].ID

left join
	(select 
		SessionNumber
		,TimeEntered
		,Liquor_Condition = Liquor.Liquor_condition
		,ListPrimaryStatus
		,ListSecondaryStatus
	from 
		[$(K2)].dbo.Liquor
	where Liquor_condition is not null
	) Liq
on EFM.SessionNumber = Liq.SessionNumber
and EFM.TimeEntered = Liq.TimeEntered


left join CTGAnnotationCTE
on EFM.SessionNumber = CTGAnnotationCTE.SessionNumber 
and EFM.TimeEntered = CTGAnnotationCTE.CTGSummaryTime
and CTGAnnotationCTE.SequenceNo = 1	

where 
	EFM.TimeEntered >=dateadd(year,-3,getdate())
	and EFM.Fetus_Number>=0
	and (Surname <> 'Test' or Surname is null)




Truncate table ETL.TImportK2CTGLookup
Insert into ETL.TImportK2CTGLookup

select 
	[Category] = 'Action'
	,ID
	,DisplayText
	,OrderID
from 
	[$(K2)].[dbo].[List_EFMSummary_CTG_Review_Action]

union

select 
	[Category] = 'Liquor'
	,ID
	,DisplayText
	,OrderID
from
	[$(K2)].[dbo].[List_EFMSummary_Review_Liquor_Colour]
	
	


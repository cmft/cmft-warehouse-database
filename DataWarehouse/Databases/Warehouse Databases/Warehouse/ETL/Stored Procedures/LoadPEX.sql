﻿

CREATE proc [ETL].[LoadPEX] 
(
	@FromDate date = null
	,@ToDate date = null
)

--with execute as owner
as

 

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime = getdate()
declare @Elapsed int
declare @Stats varchar(255)

select @FromDate = coalesce(@FromDate, (select cast(dateadd(year, -3, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

exec ETL.LoadPEXBedUnit @FromDate, @ToDate
exec ETL.LoadPEXKioskOnlinePostcardTablet @FromDate, @ToDate
exec ETL.LoadPEXSMS @FromDate, @ToDate


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

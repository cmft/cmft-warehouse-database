﻿
CREATE proc [ETL].[ExtractORACLEFinanceExpenditure]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0


Truncate table ETL.TImportFinanceExpenditure

insert into ETL.TImportFinanceExpenditure
(
	CensusDate
	,Division
	,Budget
	,AnnualBudget
	,Actual
)

select
	CensusDate = Actual.[Date]
	,Division = Actual.Division
	,Budget = cast(Budget.Value as float)
	,AnnualBudget = cast(AnnualBudget.Value as float)
	,Actual = cast(Actual.Value as float)
from 
	[$(SmallDatasets)].Finance.MonthlyBudget Actual

inner join [$(SmallDatasets)].Finance.MonthlyBudget AnnualBudget
on	Actual.Date = AnnualBudget.Date
and Actual.Division = AnnualBudget.Division
and Actual.Type = 'Actual'
and replace(AnnualBudget.Type,' ','') = 'AnnualBudget'

inner join [$(SmallDatasets)].Finance.MonthlyBudget Budget
on	Actual.Date = Budget.Date
and Actual.Division = Budget.Division
and Actual.Type = 'Actual'
and Budget.Type = 'Budget'

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime






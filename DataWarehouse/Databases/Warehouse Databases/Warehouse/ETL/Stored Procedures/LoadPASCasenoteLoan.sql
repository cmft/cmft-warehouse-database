﻿CREATE procedure [ETL].[LoadPASCasenoteLoan] as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

select
	 SourceUniqueID = CASENOTELOANID
	,SourcePatientNo = InternalPatientNumber
	,CasenoteNumber = PtCasenoteIdPhyskey
	,BorrowerCode = Borrower
			
	,SequenceNo =
		right(rtrim(CntPatientFileLoanDatesequenceNoKey) , len(CntPatientFileLoanDatesequenceNoKey) - 8)

	,TransactionTime =
		case
		when right(CntPcr4860DatetimeOfTransactionUpdateNeut , 4) = '2400' then
			dateadd(
				minute
				,1
				,cast(
					cast(
						left(CntPcr4860DatetimeOfTransactionUpdateNeut , 8 )
						as varchar
					) + ' 23:59'
					as smalldatetime
				)
			)
		else			
			cast(
				cast(
					left(CntPcr4860DatetimeOfTransactionUpdateNeut , 8 )
					as varchar
				)
				+ ' '
				+ cast(substring(CntPcr4860DatetimeOfTransactionUpdateNeut , 9 , 2) as varchar) + ':'
				+ cast(substring(CntPcr4860DatetimeOfTransactionUpdateNeut , 11 , 2) as varchar)
			as smalldatetime
			)
		end

	,LoanTime = 
		case
		when CntLoanTimeHhmm = '2400' then
			dateadd(
				minute
				,1
				,cast(
					cast(
						left(CntPatientFileLoanDatesequenceNoKey , 8 )
					as varchar
					)
					+ ' 23:59'
					as smalldatetime
				)
			)
		else			
			cast(
				cast(left(CntPatientFileLoanDatesequenceNoKey , 8 ) as varchar ) + ' ' +

				coalesce(
					cast(substring(CntLoanTimeHhmm , 1 , 2) as varchar) + ':' +
					cast(substring(CntLoanTimeHhmm , 3 , 2) as varchar)
					,'00:00'
				)
			as smalldatetime
			)
		end

	,ExpectedReturnDate = convert(date , CntExpectedReturnDateNeutral)

	,ReturnTime =
		case
		when CntReturnTimeHhmm = '2400' then
			dateadd(
				minute
				,1
				,cast(
					cast(left(CntActualReturnDateNeutral , 8 ) as varchar) + ' 23:59'
					as smalldatetime
				)
			)
		else			
			cast(
				cast(left(CntActualReturnDateNeutral , 8 ) as varchar ) + ' ' +

				coalesce(
					cast(substring(CntReturnTimeHhmm , 1 , 2) as varchar) + ':' +
					cast(substring(CntReturnTimeHhmm , 3 , 2) as varchar)
					,'00:00'
				)
			as smalldatetime
			)
		end

	,Comment = Comments
	,LoanReason = ReasonForLoan
	,UserId
into
	#CasenoteLoan
FROM
	[$(PAS)].Inquire.CASENOTELOAN


CREATE unique CLUSTERED INDEX T_IX_CasenoteLoan ON #CasenoteLoan
(
	SourcePatientNo ASC,
	CasenoteNumber ASC,
	SequenceNo ASC,
	LoanTime ASC
)

insert
into
	PAS.CasenoteLoan
(
	 SourceUniqueID
	,SourcePatientNo
	,CasenoteNumber
	,BorrowerCode
	,SequenceNo
	,TransactionTime
	,LoanTime
	,ExpectedReturnDate
	,ReturnTime
	,Comment
	,LoanReason
	,UserId
)
select
	 SourceUniqueID
	,SourcePatientNo
	,CasenoteNumber
	,BorrowerCode
	,SequenceNo
	,TransactionTime
	,LoanTime
	,ExpectedReturnDate
	,ReturnTime
	,Comment
	,LoanReason
	,UserId
from
	#CasenoteLoan
where
	not exists
	(
	select
		1
	from
		PAS.CasenoteLoan
	where
		CasenoteLoan.SourcePatientNo = #CasenoteLoan.SourcePatientNo
	and	CasenoteLoan.CasenoteNumber = #CasenoteLoan.CasenoteNumber
	and	CasenoteLoan.SequenceNo = #CasenoteLoan.SequenceNo
	and	CasenoteLoan.LoanTime = #CasenoteLoan.LoanTime
	)


declare
	 @inserted int = @@ROWCOUNT

update PAS.CasenoteLoan
set
	 CasenoteLoan.SourceUniqueID = #CasenoteLoan.SourceUniqueID
	,CasenoteLoan.BorrowerCode = #CasenoteLoan.BorrowerCode
	,CasenoteLoan.TransactionTime = #CasenoteLoan.TransactionTime
	,CasenoteLoan.ExpectedReturnDate = #CasenoteLoan.ExpectedReturnDate
	,CasenoteLoan.ReturnTime = #CasenoteLoan.ReturnTime
	,CasenoteLoan.Comment = #CasenoteLoan.Comment
	,CasenoteLoan.LoanReason = #CasenoteLoan.LoanReason
	,CasenoteLoan.UserId = #CasenoteLoan.UserId
from
	PAS.CasenoteLoan

inner join #CasenoteLoan
on	CasenoteLoan.SourcePatientNo = #CasenoteLoan.SourcePatientNo
and	CasenoteLoan.CasenoteNumber = #CasenoteLoan.CasenoteNumber
and	CasenoteLoan.SequenceNo = #CasenoteLoan.SequenceNo
and	CasenoteLoan.LoanTime = #CasenoteLoan.LoanTime

where
	not
		(
			isnull(CasenoteLoan.BorrowerCode, '') = isnull(#CasenoteLoan.BorrowerCode, '')
		and isnull(CasenoteLoan.TransactionTime, getdate()) = isnull(#CasenoteLoan.TransactionTime, getdate())
		and isnull(CasenoteLoan.ExpectedReturnDate, getdate()) = isnull(#CasenoteLoan.ExpectedReturnDate, getdate())
		and isnull(CasenoteLoan.ReturnTime, getdate()) = isnull(#CasenoteLoan.ReturnTime, getdate())
		and isnull(CasenoteLoan.Comment, '') = isnull(#CasenoteLoan.Comment, '')
		and isnull(CasenoteLoan.LoanReason, '') = isnull(#CasenoteLoan.LoanReason, '')
		and isnull(CasenoteLoan.UserId, '') = isnull(#CasenoteLoan.UserId, '')
		)
	
declare
	 @updated int = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		' Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


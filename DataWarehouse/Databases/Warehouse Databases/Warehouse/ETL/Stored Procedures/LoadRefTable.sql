﻿create procedure [ETL].[LoadRefTable]

	@server varchar(450),
	@database varchar(450),
	@schema varchar(450),
	@table varchar(450),
	@keycol varchar(450),
	@outputSchema varchar(450),
	@outputTable varchar(450),
	@asciiKey bit

as

/*
Merge a source table with an identically structured target table, with or without a generated Ascii key

E.g. ...
	
	exec ETL.LoadRefTable '[WAREHOUSE2\DEV]', 'Ulysses', 'Summary', 'DEPTDIV', 'CODE', 'Incident', 'DivisionBase', 1
	
	will update Incident.DivisionBase from [WAREHOUSE2\DEV].Ulysses.Summary.DEPTDIV using the Ascii value of the CODE column
	
*/

declare @tablepath varchar(450) = @server + '.' + @database + '.' + @schema + '.' + @table
declare @databasepath varchar(450) = @server + '.' + @database

declare @sql nvarchar(max)

if(@asciiKey = 1) -- Target table uses generated Ascii primary key field
begin
	set @sql =
	'
	insert into #t (sql)
	select
			''merge ' + @outputSchema + '.' + @outputTable + ' t 
			using ' + @tablepath + ' s 
			on dbo.StringToAscii(t.' + @keycol + ') = dbo.StringToAscii(s.' + @keycol + ') 
			when matched then update set '' + stuff(u.list,1,2,'''') + '' '' +
			''when not matched then insert (Ascii' + @keycol + ','' + stuff(i.list,1,2,'''') + '')
			values (dbo.StringToAscii(s.' + @keycol + '),'' + stuff(i.list,1,2,'''') + '');''
		from ' + @databasepath + '.INFORMATION_SCHEMA.TABLES t
		cross apply
			(select 
				'', t.['' + COLUMN_NAME + ''] = s.['' + COLUMN_NAME + '']'' 
				from ' + @databasepath + '.INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = t.TABLE_SCHEMA and TABLE_NAME = t.TABLE_NAME
				order by ORDINAL_POSITION
				for xml path('''')) u (list)
		cross apply
			(select 
				'', ['' + COLUMN_NAME + '']'' 
				from ' + @databasepath + '.INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = t.TABLE_SCHEMA and TABLE_NAME = t.TABLE_NAME
				order by ORDINAL_POSITION
				for xml path('''')) i (list)
		where t.TABLE_SCHEMA = ''' + @schema + ''' AND t.TABLE_NAME = ''' + @table + '''
	'

end
else -- Target table does not use generated Ascii primary key field
begin
	set @sql =
	'
	insert into #t (sql)
	select
			''merge ' + @outputSchema + '.' + @outputTable + ' t 
			using ' + @tablepath + ' s 
			on t.' + @keycol + ' = s.' + @keycol + ' 
			when matched then update set '' + stuff(u.list,1,2,'''') + '' '' +
			''when not matched then insert ('' + stuff(i.list,1,2,'''') + '')
			values ('' + stuff(i.list,1,2,'''') + '');''
		from ' + @databasepath + '.INFORMATION_SCHEMA.TABLES t
		cross apply
			(select 
				'', t.['' + COLUMN_NAME + ''] = s.['' + COLUMN_NAME + '']'' 
				from ' + @databasepath + '.INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = t.TABLE_SCHEMA and TABLE_NAME = t.TABLE_NAME
				order by ORDINAL_POSITION
				for xml path('''')) u (list)
		cross apply
			(select 
				'', ['' + COLUMN_NAME + '']'' 
				from ' + @databasepath + '.INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = t.TABLE_SCHEMA and TABLE_NAME = t.TABLE_NAME
				order by ORDINAL_POSITION
				for xml path('''')) i (list)
		where t.TABLE_SCHEMA = ''' + @schema + ''' AND t.TABLE_NAME = ''' + @table + '''
	'
end


-- Create temp table, and execute @sql to insert the sql string into the temp table
create table #t (sql nvarchar(max))
exec sp_executesql @sql

-- Read sql string from the temp table, and execute it
set @sql = (select sql from #t)

drop table #t

exec sp_executesql @sql


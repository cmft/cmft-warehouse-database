﻿
CREATE PROCEDURE [ETL].[LoadSymphonyReferenceLookup]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony ReferenceLookup table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ReferenceLookupID
			,ReferenceLookupParentID
			,ReferenceLookup
			,DisplayOrder
			,InAscOrder
			,Used
			,CreatedTime
			,ReferenceLookupSystem
			,FlatLookupID
			,CreatedByID
			,UpdatedTime
			,DepartmentID
			,RefreshedTime
			,IsDeleted
			,IsActive
			,IsSubAnalysed
			,TableID
			)
into
	#TLoadSymphonyReferenceLookup
from
	(
	select
		 ReferenceLookupID = cast(ReferenceLookupID as int)
		,ReferenceLookupParentID = cast(ReferenceLookupParentID as int)
		,ReferenceLookup = cast(ReferenceLookup as varchar(80))
		,DisplayOrder = cast(DisplayOrder as smallint)
		,InAscOrder = cast(InAscOrder as bit)
		,Used = cast(Used as bit)
		,CreatedTime = cast(CreatedTime as datetime)
		,ReferenceLookupSystem = cast(ReferenceLookupSystem as varchar(50))
		,FlatLookupID = cast(FlatLookupID as int)
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,DepartmentID = cast(DepartmentID as int)
		,RefreshedTime = cast(RefreshedTime as datetime)
		,IsDeleted = cast(IsDeleted as bit)
		,IsActive = cast(IsActive as bit)
		,IsSubAnalysed = cast(IsSubAnalysed as bit)
		,TableID = cast(TableID as int)
	from
		(
		select
			 ReferenceLookupID = Lookups.Lkp_ID
			,ReferenceLookupParentID = Lookups.Lkp_ParentID
			,ReferenceLookup = Lookups.Lkp_Name
			,DisplayOrder = Lookups.Lkp_DisplayOrder
			,InAscOrder = Lookups.Lkp_InAscOrder
			,Used = Lookups.Lkp_Used
			,CreatedTime = Lookups.Lkp_Datecreated
			,ReferenceLookupSystem = Lookups.Lkp_System
			,FlatLookupID = Lookups.Lkp_FlatLookupID
			,CreatedByID = Lookups.Lkp_createdby
			,UpdatedTime = Lookups.Lkp_update
			,DepartmentID = Lookups.lkp_deptid
			,RefreshedTime = Lookups.lkp_refreshed
			,IsDeleted = Lookups.lkp_deleted
			,IsActive = Lookups.lkp_active
			,IsSubAnalysed = Lookups.lkp_IsSubAnalysed
			,TableID = Lookups.lkp_TableID
		from
			[$(Symphony)].dbo.Lookups

		) Encounter
	) Encounter
order by
	Encounter.ReferenceLookupID


create unique clustered index #IX_TLoadSymphonyReferenceLookup on #TLoadSymphonyReferenceLookup
	(
	ReferenceLookupID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.ReferenceLookup target
using
	(
	select
		 ReferenceLookupID
		,ReferenceLookupParentID
		,ReferenceLookup
		,DisplayOrder
		,InAscOrder
		,Used
		,CreatedTime
		,ReferenceLookupSystem
		,FlatLookupID
		,CreatedByID
		,UpdatedTime
		,DepartmentID
		,RefreshedTime
		,IsDeleted
		,IsActive
		,IsSubAnalysed
		,TableID
		,EncounterChecksum
	from
		#TLoadSymphonyReferenceLookup
	
	) source
	on	source.ReferenceLookupID = target.ReferenceLookupID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 ReferenceLookupID
			,ReferenceLookupParentID
			,ReferenceLookup
			,DisplayOrder
			,InAscOrder
			,Used
			,CreatedTime
			,ReferenceLookupSystem
			,FlatLookupID
			,CreatedByID
			,UpdatedTime
			,DepartmentID
			,RefreshedTime
			,IsDeleted
			,IsActive
			,IsSubAnalysed
			,TableID
			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.ReferenceLookupID
			,source.ReferenceLookupParentID
			,source.ReferenceLookup
			,source.DisplayOrder
			,source.InAscOrder
			,source.Used
			,source.CreatedTime
			,source.ReferenceLookupSystem
			,source.FlatLookupID
			,source.CreatedByID
			,source.UpdatedTime
			,source.DepartmentID
			,source.RefreshedTime
			,source.IsDeleted
			,source.IsActive
			,source.IsSubAnalysed
			,source.TableID
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.ReferenceLookupID = source.ReferenceLookupID
			,target.ReferenceLookupParentID = source.ReferenceLookupParentID
			,target.ReferenceLookup = source.ReferenceLookup
			,target.DisplayOrder = source.DisplayOrder
			,target.InAscOrder = source.InAscOrder
			,target.Used = source.Used
			,target.CreatedTime = source.CreatedTime
			,target.ReferenceLookupSystem = source.ReferenceLookupSystem
			,target.FlatLookupID = source.FlatLookupID
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.DepartmentID = source.DepartmentID
			,target.RefreshedTime = source.RefreshedTime
			,target.IsDeleted = source.IsDeleted
			,target.IsActive = source.IsActive
			,target.IsSubAnalysed = source.IsSubAnalysed
			,target.TableID = source.TableID
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
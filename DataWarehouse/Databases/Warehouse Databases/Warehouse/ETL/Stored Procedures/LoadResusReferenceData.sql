﻿
CREATE proc [ETL].[LoadResusReferenceData] as


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()


insert Resus.CallOutReason

select
	CallOutReasonID = ItemID
	,CallOutReason = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 6
and not exists
	(
	select
		1
	from
		 Resus.CallOutReason
	where
		ItemID = CallOutReasonID
	)


insert Resus.Outcome

select
	OutcomeID = ItemID
	,Outcome = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 15
and not exists
	(
	select
		1
	from
		Resus.Outcome
	where
		ItemID = OutcomeID
	)


insert Resus.FinalOutcome

select
	FinalOutcomeID = ItemID
	,FinalOutcome = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 18
and not exists
	(
	select
		1
	from
		Resus.FinalOutcome
	where
		ItemID = FinalOutcomeID
	)


insert Resus.Circulation

select
	CirculationID = ItemID
	,Circulation = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 11
and not exists
	(
	select
		1
	from
		Resus.Circulation
	where
		ItemID = CirculationID
	)


insert Resus.Airway

select
	AirwayID = ItemID
	,Airway = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 9
and not exists
	(
	select
		1
	from
		Resus.Airway
	where
		ItemID = AirwayID
	)


insert Resus.Breathing

select
	BreathingID = ItemID
	,Breathing = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 10
and not exists
	(
	select
		1
	from
		Resus.Breathing
	where
		ItemID = BreathingID
	)


insert Resus.Drug

select
	DrugID = ItemID
	,Drug = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 12
and not exists
	(
	select
		1
	from
		Resus.Drug
	where
		ItemID = DrugID
	)


insert Resus.PresentingECG

select
	PresentingECGID = ItemID
	,PresentingECG = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 19
and not exists
	(
	select
		1
	from
		Resus.PresentingECG
	where
		ItemID = PresentingECGID
	)


insert Resus.Team

select
	TeamID = ItemID
	,Team = ItemName
from
	[$(CardiacArrest)].multisite.tblitem
where
	ItemTypeID = 2
and not exists
	(
	select
		1
	from
		Resus.Team
	where
		ItemID = TeamID
	)


insert Resus.Location

select
	LocationID = DepartmentID
	,Location = DepartmentName
from
	[$(CardiacArrest)].multisite.tbldepartment location
where
	not exists
	(
	select
		1
	from
		Resus.Location
	where
		DepartmentID = LocationID
	)



select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = name from sysobjects where id = @@procid

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


﻿CREATE procedure [ETL].[ExtractInquireAPCWardStay]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @sql2 varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
declare @localFromDate smalldatetime

select @StartTime = getdate()

select @RowsInserted = 0

select @localFromDate = dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0)

select @from = convert(varchar, @localFromDate, 112) + '0000'

--select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)


insert into ETL.TImportAPCWardStay
(
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,EndDate
	,StartTime
	,EndTime
	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
)
select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,EndDate
	,StartTime
	,EndTime
	,SiteCode
	,WardCode
	,StartActivityCode
	,EndActivityCode
from
	(
	SELECT 
		 SourceUniqueID = WARDSTAY.WARDSTAYID 
		,SourceSpellNo = WARDSTAY.EpisodeNumber 
		,SourcePatientNo = WARDSTAY.InternalPatientNumber 
		,WARDSTAY.StartDate
		,WARDSTAY.EndDate
		,WARDSTAY.StartTime
		,WARDSTAY.EndTime
		,SiteCode = WARDSTAY.HospitalCode 
		,WARDSTAY.WardCode
		,StartActivityCode = WARDSTAY.StartActivity 
		,EndActivityCode = WARDSTAY.EndActivity 
	FROM
		[$(PAS)].Inquire.WARDSTAY

	INNER JOIN [$(PAS)].Inquire.ADMITDISCH AdmitDisch 
	ON	AdmitDisch.EpisodeNumber = WARDSTAY.EpisodeNumber
	AND	AdmitDisch.InternalPatientNumber = WARDSTAY.InternalPatientNumber

	WHERE
		AdmitDisch.IpDschDtimeInt >= @from
	or	AdmitDisch.IpDschDtimeInt is null
	) WardStay
where
	convert(smalldatetime, WardStay.EndDate) >= convert(varchar, @localFromDate)
or	WardStay.EndDate is null

--union all

--select
--	 SourceUniqueID
--	,SourceSpellNo
--	,SourcePatientNo
--	,StartDate
--	,EndDate
--	,StartTime
--	,EndTime
--	,SiteCode
--	,WardCode
--	,StartActivityCode
--	,EndActivityCode
--from
--	(
--	SELECT 
--		 SourceUniqueID = WARDSTAY.WARDSTAYID 
--		,SourceSpellNo = WARDSTAY.EpisodeNumber 
--		,SourcePatientNo = WARDSTAY.InternalPatientNumber 
--		,WARDSTAY.StartDate
--		,WARDSTAY.EndDate
--		,WARDSTAY.StartTime
--		,WARDSTAY.EndTime
--		,SiteCode = WARDSTAY.HospitalCode 
--		,WARDSTAY.WardCode
--		,StartActivityCode = WARDSTAY.StartActivity 
--		,EndActivityCode = WARDSTAY.EndActivity 
--	FROM
--		[$(PAS)].Inquire.WARDSTAY

--	INNER JOIN [$(PAS)].Inquire.MIDNIGHTBEDSTATE M 
--	ON	M.EpisodeNumber = WARDSTAY.EpisodeNumber
--	AND	M.InternalPatientNumber =  WARDSTAY.InternalPatientNumber

--	WHERE
--		M.StatisticsDate = @census
--	) WardStay
--where
--	convert(smalldatetime, WardStay.EndDate) >= convert(varchar, @localFromDate)
--or	WardStay.EndDate is null



select
	@RowsInserted = @RowsInserted + isnull(@@ROWCOUNT , 0)

--occasionally, when a ward stay is closed mid-query, duplicates may occur. This retains the last closed ward stay and removes the open one.
delete
from
	ETL.TImportAPCWardStay
where
	exists
	(
	select
		1
	from
		ETL.TImportAPCWardStay Previous
	where
		Previous.SourceUniqueID = TImportAPCWardStay.SourceUniqueID
	and	Previous.EndDate is not null
	and	TImportAPCWardStay.EndDate is null
	)

select
	@RowsDeleted = isnull(@@ROWCOUNT , 0)

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'From ' + CONVERT(varchar, @localFromDate) + ', '  + 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireWardStay', @Stats, @StartTime

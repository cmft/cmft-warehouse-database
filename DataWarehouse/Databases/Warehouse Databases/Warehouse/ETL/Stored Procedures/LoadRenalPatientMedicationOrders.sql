﻿
CREATE procedure [ETL].[LoadRenalPatientMedicationOrders] (@from date = '19000101', @to date = '21000101') as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

declare @MergeSummary TABLE(Action nvarchar(10));

merge Renal.PatientMedicationOrders target
using ETL.TLoadRenalPatientMedicationOrders source
on source.SourceUniqueID = target.SourceUniqueID
when matched and source.StartDate between @from and @to
and not
(
	isnull(target.PatientObjectID, 0) = isnull(source.PatientObjectID, 0)
	and isnull(target.PatientFullName, '') = isnull(source.PatientFullName, '')
	and isnull(target.PatientMedicalRecordNo, '') = isnull(source.PatientMedicalRecordNo, '')
	and isnull(target.ServiceMasterName, '') = isnull(source.ServiceMasterName, '')
	and isnull(target.ServiceType, '') = isnull(source.ServiceType, '')
	and isnull(target.OrderDate, getdate()) = isnull(source.OrderDate, getdate())
	and isnull(target.StartDate, getdate()) = isnull(source.StartDate, getdate())
	and isnull(target.OrderedByMedicalStaffObjectID, 0) = isnull(source.OrderedByMedicalStaffObjectID, 0)
	and isnull(target.OrderedByInternalStaffObjectID, 0) = isnull(source.OrderedByInternalStaffObjectID, 0)
	and isnull(target.OrderedBy, '') = isnull(source.OrderedBy, '')
	and isnull(target.SignatureOnFile, 0) = isnull(source.SignatureOnFile, 0)
	and isnull(target.Indication, '') = isnull(source.Indication, '')
	and isnull(target.StopDate, getdate()) = isnull(source.StopDate, getdate())
	and isnull(target.ReasonStopped, '') = isnull(source.ReasonStopped, '')
	and isnull(target.StoppedByMedicalStaffObjectID, 0) = isnull(source.StoppedByMedicalStaffObjectID, 0)
	and isnull(target.StoppedByInternalStaffObjectID, 0) = isnull(source.StoppedByInternalStaffObjectID, 0)
	and isnull(target.StoppedBy, '') = isnull(source.StoppedBy, '')
	and isnull(target.Location, '') = isnull(source.Location, '')
	and isnull(target.Provider, '') = isnull(source.Provider, '')
	and isnull(target.DurationInMinutes, 0) = isnull(source.DurationInMinutes, 0)
	and isnull(target.OrderedTreatments, 0) = isnull(source.OrderedTreatments, 0)
	and isnull(target.MaximumTreatments, 0) = isnull(source.MaximumTreatments, 0)
	and isnull(target.Notifications, '') = isnull(source.Notifications, '')
	and isnull(target.Notes, '') = isnull(source.Notes, '')
	and isnull(target.IsOpen, 0) = isnull(source.IsOpen, 0)
	and isnull(target.IsSigned, 0) = isnull(source.IsSigned, 0)
	and isnull(target.IsRefused, 0) = isnull(source.IsRefused, 0)
	and isnull(target.IsMaster, 0) = isnull(source.IsMaster, 0)
	and isnull(target.IndicationCode, '') = isnull(source.IndicationCode, '')
	and isnull(target.DefaultScheduleObjectID, 0) = isnull(source.DefaultScheduleObjectID, 0)
	and isnull(target.ServiceMasterObjectID, 0) = isnull(source.ServiceMasterObjectID, 0)
	and isnull(target.ProviderObjectID, 0) = isnull(source.ProviderObjectID, 0)
	and isnull(target.LocationObjectID, 0) = isnull(source.LocationObjectID, 0)
	and isnull(target.NextScheduledDate, getdate()) = isnull(source.NextScheduledDate, getdate())
	and isnull(target.LastServiceDate, getdate()) = isnull(source.LastServiceDate, getdate())
	and isnull(target.ParentOrderObjectID, 0) = isnull(source.ParentOrderObjectID, 0)
	and isnull(target.DrugName, '') = isnull(source.DrugName, '')
	and isnull(target.DrugType, '') = isnull(source.DrugType, '')
	and isnull(target.MedicationGroup, '') = isnull(source.MedicationGroup, '')
	and isnull(target.NonESRD, 0) = isnull(source.NonESRD, 0)
	and isnull(target.Dose, 0) = isnull(source.Dose, 0)
	and isnull(target.DoseUnits, '') = isnull(source.DoseUnits, '')
	and isnull(target.Dosage, '') = isnull(source.Dosage, '')
	and isnull(target.Frequency, '') = isnull(source.Frequency, '')
	and isnull(target.AllowSubstitutions, 0) = isnull(source.AllowSubstitutions, 0)
	and isnull(target.Route, '') = isnull(source.Route, '')
	and isnull(target.DoseRangeMin, 0) = isnull(source.DoseRangeMin, 0)
	and isnull(target.DoseRangeMax, 0) = isnull(source.DoseRangeMax, 0)
	and isnull(target.SupplySource, '') = isnull(source.SupplySource, '')
	and isnull(target.Method, '') = isnull(source.Method, '')
	and isnull(target.Rate, '') = isnull(source.Rate, '')
	and isnull(target.Device, '') = isnull(source.Device, '')
	and isnull(target.Site, '') = isnull(source.Site, '')
	and isnull(target.Summary, '') = isnull(source.Summary, '')
	and isnull(target.ServiceDetails, '') = isnull(source.ServiceDetails, '')
)
	
then update set
	
	target.PatientObjectID = source.PatientObjectID
	,target.PatientFullName = source.PatientFullName
	,target.PatientMedicalRecordNo = source.PatientMedicalRecordNo
	,target.ServiceMasterName = source.ServiceMasterName
	,target.ServiceType = source.ServiceType
	,target.OrderDate = source.OrderDate
	,target.StartDate = source.StartDate
	,target.OrderedByMedicalStaffObjectID = source.OrderedByMedicalStaffObjectID
	,target.OrderedByInternalStaffObjectID = source.OrderedByInternalStaffObjectID
	,target.OrderedBy = source.OrderedBy
	,target.SignatureOnFile = source.SignatureOnFile
	,target.Indication = source.Indication
	,target.StopDate = source.StopDate
	,target.ReasonStopped = source.ReasonStopped
	,target.StoppedByMedicalStaffObjectID = source.StoppedByMedicalStaffObjectID
	,target.StoppedByInternalStaffObjectID = source.StoppedByInternalStaffObjectID
	,target.StoppedBy = source.StoppedBy
	,target.Location = source.Location
	,target.Provider = source.Provider
	,target.DurationInMinutes = source.DurationInMinutes
	,target.OrderedTreatments = source.OrderedTreatments
	,target.MaximumTreatments = source.MaximumTreatments
	,target.Notifications = source.Notifications
	,target.Notes = source.Notes
	,target.IsOpen = source.IsOpen
	,target.IsSigned = source.IsSigned
	,target.IsRefused = source.IsRefused
	,target.IsMaster = source.IsMaster
	,target.IndicationCode = source.IndicationCode
	,target.DefaultScheduleObjectID = source.DefaultScheduleObjectID
	,target.ServiceMasterObjectID = source.ServiceMasterObjectID
	,target.ProviderObjectID = source.ProviderObjectID
	,target.LocationObjectID = source.LocationObjectID
	,target.NextScheduledDate = source.NextScheduledDate
	,target.LastServiceDate = source.LastServiceDate
	,target.ParentOrderObjectID = source.ParentOrderObjectID
	,target.DrugName = source.DrugName
	,target.DrugType = source.DrugType
	,target.MedicationGroup = source.MedicationGroup
	,target.NonESRD = source.NonESRD
	,target.Dose = source.Dose
	,target.DoseUnits = source.DoseUnits
	,target.Dosage = source.Dosage
	,target.Frequency = source.Frequency
	,target.AllowSubstitutions = source.AllowSubstitutions
	,target.Route = source.Route
	,target.DoseRangeMin = source.DoseRangeMin
	,target.DoseRangeMax = source.DoseRangeMax
	,target.SupplySource = source.SupplySource
	,target.Method = source.Method
	,target.Rate = source.Rate
	,target.Device = source.Device
	,target.Site = source.Site
	,target.Summary = source.Summary
	,target.ServiceDetails = source.ServiceDetails	
	,target.Updated = getdate()
	,target.ByWhom = system_user

when not matched by source then delete

when not matched by target then insert
(
	SourceUniqueID
	,PatientObjectID
	,PatientFullName
	,PatientMedicalRecordNo
	,ServiceMasterName
	,ServiceType
	,OrderDate
	,StartDate
	,OrderedByMedicalStaffObjectID
	,OrderedByInternalStaffObjectID
	,OrderedBy
	,SignatureOnFile
	,Indication
	,StopDate
	,ReasonStopped
	,StoppedByMedicalStaffObjectID
	,StoppedByInternalStaffObjectID
	,StoppedBy
	,Location
	,Provider
	,DurationInMinutes
	,OrderedTreatments
	,MaximumTreatments
	,Notifications
	,Notes
	,IsOpen
	,IsSigned
	,IsRefused
	,IsMaster
	,IndicationCode
	,DefaultScheduleObjectID
	,ServiceMasterObjectID
	,ProviderObjectID
	,LocationObjectID
	,NextScheduledDate
	,LastServiceDate
	,ParentOrderObjectID
	,DrugName
	,DrugType
	,MedicationGroup
	,NonESRD
	,Dose
	,DoseUnits
	,Dosage
	,Frequency
	,AllowSubstitutions
	,Route
	,DoseRangeMin
	,DoseRangeMax
	,SupplySource
	,Method
	,Rate
	,Device
	,Site
	,Summary
	,ServiceDetails
	,Created
	,Updated
	,ByWhom
)
values
(
	 source.SourceUniqueID
	,source.PatientObjectID
	,source.PatientFullName
	,source.PatientMedicalRecordNo
	,source.ServiceMasterName
	,source.ServiceType
	,source.OrderDate
	,source.StartDate
	,source.OrderedByMedicalStaffObjectID
	,source.OrderedByInternalStaffObjectID
	,source.OrderedBy
	,source.SignatureOnFile
	,source.Indication
	,source.StopDate
	,source.ReasonStopped
	,source.StoppedByMedicalStaffObjectID
	,source.StoppedByInternalStaffObjectID
	,source.StoppedBy
	,source.Location
	,source.Provider
	,source.DurationInMinutes
	,source.OrderedTreatments
	,source.MaximumTreatments
	,source.Notifications
	,source.Notes
	,source.IsOpen
	,source.IsSigned
	,source.IsRefused
	,source.IsMaster
	,source.IndicationCode
	,source.DefaultScheduleObjectID
	,source.ServiceMasterObjectID
	,source.ProviderObjectID
	,source.LocationObjectID
	,source.NextScheduledDate
	,source.LastServiceDate
	,source.ParentOrderObjectID
	,source.DrugName
	,source.DrugType
	,source.MedicationGroup
	,source.NonESRD
	,source.Dose
	,source.DoseUnits
	,source.Dosage
	,source.Frequency
	,source.AllowSubstitutions
	,source.Route
	,source.DoseRangeMin
	,source.DoseRangeMax
	,source.SupplySource
	,source.Method
	,source.Rate
	,source.Device
	,source.Site
	,source.Summary
	,source.ServiceDetails
	,getdate()
	,getdate()
	,system_user
)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats


﻿CREATE procedure [ETL].[ExtractSIDSuspectedTIA]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDSuspectedTIA;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDSuspectedTIA
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,DateTimeOfAssessment
	,LocationAtPointOfAssessment
	,DateTimeOfAttendedED
	,ABCD2DTOAgeIs60YearsOrOlder
	,ABCD2DTOBloodPressureOver140_90mmHg
	,ABCD2DTOClinicalFeatures
	,ABCD2DTODuration
	,ABCD2DTODiabetes
	,ABCD2DTOTotalScore
	,ABCD2DTOClinicalOpinion
	,ABCD2DTOBedRequestForInpatientInvestigations
	,RequestForUrgentBrainScan
	,DateTimeRequestForUrgentBrainScan
	,DidThePatientHaveSignificantAndTreatableCarotidStenosis
	,CoMorbiditiesDTOCongestiveHeartFailure
	,CoMorbiditiesDTOHypertension
	,CoMorbiditiesDTOAtrialFibrillation
	,CoMorbiditiesDTODiabeties
	,CoMorbiditiesDTOStroke
	,CoMorbiditiesDTOTIA
	,CoMorbiditiesDTOOtherMorbidities
	,CoMorbiditiesDTONoMorbidities
	,RequestForUrgentCarotidDopplers
	,DateTimeUrgentCarotidDopplers
	,DateTimeAdviceGivenIfHappensAgainAnE
	,Aspirin300mgAdministered
	,DateTimeAspirin300mgAdministered
	,EnsureStatinPrescribed
	,DateTimeEnsureStatinPrescribed
	,BloodPressureRecordedAndAdviceGiven
	,DateTimeBloodPressureRecordedAndAdviceGiven
	,ManualHeartRateRecorded
	,DateTimeManualHeartRateRecorded
	,ECGRequested
	,DateTimeECGRequested
	,AFNoted
	,AdviceGiven
	,AntiplateletMedicationPriorToAdmission
	,AnticoagulantMedicationPriorToAdmission
	,DietaryAdviceGiven
	,ExerciseAdviceGiven
	,BloodGlucoseRecorded
	,SmokingAdviceGiven
	,InformedNotToDriveForAMonth
	,DateTimeInvestigationsReviewed
	,Username
	,ModifiedDate
	,IsSectionComplete
	,SectionCompleteTime
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		CandidateData.SectionID
	,ActionID = 
		CandidateData.ActionID

	,DateTimeOfAssessment = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeOfAssessment)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,LocationAtPointOfAssessment = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/LocationAtPointOfAssessment)[1]', 'varchar(max)'),'')
	,DateTimeOfAttendedED = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeOfAttendedED)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ABCD2DTOAgeIs60YearsOrOlder = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ABCD2/AgeIs60YearsOrOlde)[1]', 'varchar(max)'),'')
	,ABCD2DTOBloodPressureOver140_90mmHg = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ABCD2/BloodPressureOver140_90mmHg)[1]', 'varchar(max)'),'')
	,ABCD2DTOClinicalFeatures = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ABCD2/ClinicalFeatures)[1]', 'varchar(max)'),'')
	,ABCD2DTODuration = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ABCD2/Duratio)[1]', 'varchar(max)'),'')
	,ABCD2DTODiabetes = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ABCD2/Diabete)[1]', 'varchar(max)'),'')
	,ABCD2DTOTotalScore = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ABCD2/TotalScore)[1]', 'varchar(max)'),'')
	,ABCD2DTOClinicalOpinion = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ABCD2/ClinicalOpinion)[1]', 'varchar(max)'),'')
	,ABCD2DTOBedRequestForInpatientInvestigations = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/BedRequestForInpatientInvestigations)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,RequestForUrgentBrainScan = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/RequestForUrgentBrainScan)[1]', 'varchar(max)'),'')
	,DateTimeRequestForUrgentBrainScan = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeRequestForUrgentBrainScan)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DidThePatientHaveSignificantAndTreatableCarotidStenosis = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/DidThePatientHaveSignificantAndTreatableCarotidStenosis)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTOCongestiveHeartFailure = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/CongestiveHeartFailure)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTOHypertension = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/Hypertension)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTOAtrialFibrillation = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/AtrialFibrillation)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTODiabeties = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/Diabetie)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTOStroke = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/Stroke)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTOTIA = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/TIA)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTOOtherMorbidities = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/OOtherMorbiditie)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDTONoMorbidities = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/CoMorbidities/NoMorbidities)[1]', 'varchar(max)'),'')
	,RequestForUrgentCarotidDopplers = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/RequestForUrgentCarotidDopplers)[1]', 'varchar(max)'),'')
	,DateTimeUrgentCarotidDopplers = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeUrgentCarotidDopplers)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeAdviceGivenIfHappensAgainAnE = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeAdviceGivenIfHappensAgainAnE)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,Aspirin300mgAdministered = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/Aspirin300mgAdministered)[1]', 'varchar(max)'),'')
	,DateTimeAspirin300mgAdministered = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeAspirin300mgAdministered)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,EnsureStatinPrescribed = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/EnsureStatinPrescribed)[1]', 'varchar(max)'),'')
	,DateTimeEnsureStatinPrescribed = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeEnsureStatinPrescribed)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,BloodPressureRecordedAndAdviceGiven = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/BloodPressureRecordedAndAdviceGiven)[1]', 'varchar(max)'),'')
	,DateTimeBloodPressureRecordedAndAdviceGiven = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeBloodPressureRecordedAndAdviceGiven)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ManualHeartRateRecorded = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ManualHeartRateRecorded)[1]', 'varchar(max)'),'')
	,DateTimeManualHeartRateRecorded = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeManualHeartRateRecorded)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ECGRequested = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ECGRequested)[1]', 'varchar(max)'),'')
	,DateTimeECGRequested = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeECGRequested)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,AFNoted = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/AFNoted)[1]', 'varchar(max)'),'')
	,AdviceGiven = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/AdviceGiven)[1]', 'varchar(max)'),'')
	,AntiplateletMedicationPriorToAdmission = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/AntiplateletMedicationPriorToAdmission)[1]', 'varchar(max)'),'')
	,AnticoagulantMedicationPriorToAdmission = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/AnticoagulantMedicationPriorToAdmission)[1]', 'varchar(max)'),'')
	,DietaryAdviceGiven = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/DietaryAdviceGiven)[1]', 'varchar(max)'),'')
	,ExerciseAdviceGiven = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/ExerciseAdviceGiven)[1]', 'varchar(max)'),'')
	,BloodGlucoseRecorded = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/BloodGlucoseRecorded)[1]', 'varchar(max)'),'')
	,SmokingAdviceGiven = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/SmokingAdviceGiven)[1]', 'varchar(max)'),'')
	,InformedNotToDriveForAMonth = 
		nullif(CandidateData.Details.value('(/SuspectedTIADTO/InformedNotToDriveForAMonth)[1]', 'varchar(max)'),'')
	,DateTimeInvestigationsReviewed = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/SuspectedTIADTO/DateTimeInvestigationsReviewed)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,Username = 
		CandidateData.Details.value('(/SuspectedTIADTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/SuspectedTIADTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,IsSectionComplete = 
		SectionComplete.Details.value('(/SectionCompletedDTO/CurrentSelection)[1]', 'varchar(max)')
	,SectionCompleteTime = 
		SectionComplete.DateTimeCreated
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
left outer join 
(
	select
		*
	from
		[$(SID)].dbo.TARNCandidateData 
	where
		SectionID = 15
	and ActionID = 50
) SectionComplete
on SectionComplete.CandidateID = CandidateData.CandidateID
where
	CandidateData.SectionID = 15
and CandidateData.ActionID =99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDSuspectedTIA', @Stats, @StartTime





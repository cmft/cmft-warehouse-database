﻿CREATE       procedure [ETL].[LoadOP] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @toOP varchar(12)
declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

-- 999 day limit for future OPA
select @toOP = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, 999, getdate())), 0), 112)

--select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @to = coalesce(@to, @toOP)

select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from Utility.Parameter where Parameter = 'OPENCOUNTERFREEZEDATE')
		,dateadd(month, -12, cast(sysdatetime() as date))
	)

truncate table ETL.TImportOPEncounter
exec ETL.ExtractInquireOP @from , @to

--exec ETL.AssignTImportOPEncounterDirectorateCode


exec ETL.LoadOPEncounter

--Comment out when not required
--exec Utility.BuildOPProcessList 

--exec ETL.BuildOPProcessListArchive

--barely used on PAS
--truncate table ETL.TImportOPDiagnosis
--exec ETL.ExtractInquireOPDiagnosis @from, @to
--exec ETL.LoadOPDiagnosis

truncate table ETL.TImportOPOperation
exec ETL.ExtractInquireOPOperation @from, @to
exec ETL.LoadOPOperation

truncate table ETL.TImportOPDiary
exec ETL.ExtractInquireOPDiary
exec ETL.LoadOPDiary

--disabled CCB 2013-01-14
----work out derived breach dates etc.
--exec ETL.AssignOPBreachDate

exec ETL.AssignOPEncounterDirectorateCode

--exec ETL.AssignOPEncounterFirstAttendanceFlag
exec ETL.AssignOPEncounterDerivedFirstAttendanceFlag

update Utility.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADOPDATE'

if @@rowcount = 0

insert into Utility.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADOPDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec Utility.WriteAuditLogEvent 'LoadOP', @Stats, @StartTime

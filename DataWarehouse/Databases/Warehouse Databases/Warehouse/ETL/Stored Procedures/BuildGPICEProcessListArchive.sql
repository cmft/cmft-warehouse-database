﻿
CREATE procedure [ETL].[BuildGPICEProcessListArchive] as

insert
into
	Result.GPICEProcessListArchive
(
	 ResultRecno
	,Action
	,ArchiveTime
)
select
	 ResultRecno
	,Action
	,ArchiveTime = getdate()
from
	Result.GPICEProcessList;

truncate table Result.GPICEProcessList


﻿CREATE procedure [ETL].[ExtractInquireMaternityClinicDetail]

as

insert into ETL.TImportPASMaternityClinicDetail
select
	 *
from
	(
	select
		 MATCLINICDETSID
		,left(EpisodeNumber, 12) EpisodeNumber
		,left(InternalPatientNumber, 12) InternalPatientNumber
		,IsThePatientKnownToBeImmune
		,KeMatRubellaImmInt
		,LeadProfessionalType
		,MaternalHeight
		,NoOfNeonatalDeaths
		,NoOfNoninducedAbortions
		,NoOfPreviousCaesareanSections
		,NoOfPreviousInducedAbortions
		,NoOfRegistrableLiveBirths
		,NoOfRegistrableStillBirths
		,PatientImmunised
		,PreviousTransfusion
		,WasAntibodyStatusTested
		,WasTheResultPositive
	from
		[$(PAS)].Inquire.MATCLINICDETS
) MatClinicDets

﻿
CREATE procedure [ETL].[LoadMortality] as

exec ETL.ExtractMortalityCriticalCare

exec ETL.ExtractMortalityGynaecology

exec ETL.ExtractMortalityNICU

exec ETL.ExtractMortalitySpecialistMedicalServices

exec ETL.ExtractMortalitySurgery

exec ETL.ExtractMortalityPaediatrics

-- New Process

exec ETL.ExtractMortalityAdult

exec ETL.ExtractMortalityNeonatology

exec ETL.LoadMortalityReferenceData



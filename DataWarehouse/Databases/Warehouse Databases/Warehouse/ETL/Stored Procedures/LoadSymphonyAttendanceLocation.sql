﻿
CREATE PROCEDURE [ETL].[LoadSymphonyAttendanceLocation]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony AttendanceLocation table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 AttendanceLocationID
			,AttendanceID
			,LocationID
			,LocationTime
			,CreatedByID
			,UpdatedTime
			)
into
	#TLoadSymphonyAttendanceLocation
from
	(
	select
		 AttendanceLocationID = cast(AttendanceLocationID as int)
		,AttendanceID = cast(AttendanceID as int)
		,LocationID = cast(LocationID as smallint)
		,LocationTime = cast(LocationTime as datetime)
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
	from
		(
		select
			 AttendanceLocationID = Current_Locations.cul_culid
			,AttendanceID = Current_Locations.cul_atdid
			,LocationID = Current_Locations.cul_locationid
			,LocationTime = Current_Locations.cul_locationdate
			,CreatedByID = Current_Locations.cul_createdby
			,UpdatedTime = Current_Locations.cul_update
		from
			[$(Symphony)].dbo.Current_Locations

		) Encounter
	) Encounter
order by
	Encounter.AttendanceLocationID


create unique clustered index #IX_TLoadSymphonyAttendanceLocation on #TLoadSymphonyAttendanceLocation
	(
	AttendanceLocationID ASC
	)



declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.AttendanceLocation target
using
	(
	select
		 AttendanceLocationID
		,AttendanceID
		,LocationID
		,LocationTime
		,CreatedByID
		,UpdatedTime
		,EncounterChecksum
	from
		#TLoadSymphonyAttendanceLocation
	
	) source
	on	source.AttendanceLocationID = target.AttendanceLocationID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 AttendanceLocationID
			,AttendanceID
			,LocationID
			,LocationTime
			,CreatedByID
			,UpdatedTime

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.AttendanceLocationID
			,source.AttendanceID
			,source.LocationID
			,source.LocationTime
			,source.CreatedByID
			,source.UpdatedTime
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.AttendanceLocationID = source.AttendanceLocationID
			,target.AttendanceID = source.AttendanceID
			,target.LocationID = source.LocationID
			,target.LocationTime = source.LocationTime
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
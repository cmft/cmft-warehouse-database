﻿

CREATE PROCEDURE [ETL].[LoadResultCENICEResult]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, ReportTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, ReportTime)) 
from
	ETL.TLoadResultCENICEResult

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Result.CENICEResult target
using
	(
	select
		SourceUniqueID
		,PatientID
		,PatientIdentifier
		,DistrictNo
		,CasenoteNumber
		,NHSNumber
		,DateOfBirth
		,SexCode
		,ClinicianID
		,MainSpecialtyCode
		,SpecialtyCode
		,LocationID
		,ReportSourceUniqueID
		,ReportStatusCode
		,ReportTime
		,ReportComment
		,SampleReferenceCode
		,SampleTypeCode
		,SampleType
		,SampleCollectionTime
		,InvestigationCode
		,InvestigationName
		,InvestigationComment
		,ResultCode
		,ResultName
		,Result
		,UnitOfMeasurement
		,Abnormal
		,LowerReferenceRangeValue
		,UpperReferenceRangeValue
		,ResultComment
		,InterfaceCode
		,ResultChecksum = 
		checksum(
				SourceUniqueID
				,PatientID
				,PatientIdentifier
				,DistrictNo
				,CasenoteNumber
				,NHSNumber
				,DateOfBirth
				,SexCode
				,ClinicianID
				,MainSpecialtyCode
				,SpecialtyCode
				,LocationID
				,ReportSourceUniqueID
				,ReportStatusCode
				,ReportTime
				,ReportComment
				,SampleReferenceCode
				,SampleTypeCode
				,SampleType
				,SampleCollectionTime
				,InvestigationCode
				,InvestigationName
				,InvestigationComment
				,ResultCode
				,ResultName
				,Result
				,UnitOfMeasurement
				,Abnormal
				,LowerReferenceRangeValue
				,UpperReferenceRangeValue
				,ResultComment
				,InterfaceCode
		)				
	from
		ETL.TLoadResultCENICEResult
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ReportTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber
			,DateOfBirth
			,SexCode
			,ClinicianID
			,MainSpecialtyCode
			,SpecialtyCode
			,LocationID
			,ReportSourceUniqueID
			,ReportStatusCode
			,ReportTime
			,ReportComment
			,SampleReferenceCode
			,SampleTypeCode
			,SampleType
			,SampleCollectionTime
			,InvestigationCode
			,InvestigationName
			,InvestigationComment
			,ResultCode
			,ResultName
			,Result
			,UnitOfMeasurement
			,Abnormal
			,LowerReferenceRangeValue
			,UpperReferenceRangeValue
			,ResultComment
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,ResultChecksum
			)
		values
			(
			 source.SourceUniqueID
			,source.PatientID
			,source.PatientIdentifier
			,source.DistrictNo
			,source.CasenoteNumber
			,source.NHSNumber
			,source.DateOfBirth
			,source.SexCode
			,source.ClinicianID
			,source.MainSpecialtyCode
			,source.SpecialtyCode
			,source.LocationID
			,source.ReportSourceUniqueID
			,source.ReportStatusCode
			,source.ReportTime
			,source.ReportComment
			,source.SampleReferenceCode
			,source.SampleTypeCode
			,source.SampleType
			,source.SampleCollectionTime
			,source.InvestigationCode
			,source.InvestigationName
			,source.InvestigationComment
			,source.ResultCode
			,source.ResultName
			,source.Result
			,source.UnitOfMeasurement
			,source.Abnormal
			,source.LowerReferenceRangeValue
			,source.UpperReferenceRangeValue
			,source.ResultComment
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,ResultChecksum
			)

	when matched
	and	target.ResultChecksum <> source.ResultChecksum
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.PatientID = source.PatientID
			,target.PatientIdentifier = source.PatientIdentifier
			,target.DistrictNo = source.DistrictNo
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SexCode = source.SexCode
			,target.ClinicianID = source.ClinicianID
			,target.MainSpecialtyCode = source.MainSpecialtyCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.LocationID = source.LocationID
			,target.ReportSourceUniqueID = source.ReportSourceUniqueID
			,target.ReportStatusCode = source.ReportStatusCode
			,target.ReportTime = source.ReportTime
			,target.ReportComment = source.ReportComment
			,target.SampleReferenceCode = source.SampleReferenceCode
			,target.SampleTypeCode = source.SampleTypeCode
			,target.SampleType = source.SampleType
			,target.SampleCollectionTime = source.SampleCollectionTime
			,target.InvestigationCode = source.InvestigationCode
			,target.InvestigationName = source.InvestigationName
			,target.InvestigationComment = source.InvestigationComment
			,target.ResultCode = source.ResultCode
			,target.ResultName = source.ResultName
			,target.Result = source.Result
			,target.UnitOfMeasurement = source.UnitOfMeasurement
			,target.Abnormal = source.Abnormal
			,target.LowerReferenceRangeValue = source.LowerReferenceRangeValue
			,target.UpperReferenceRangeValue = source.UpperReferenceRangeValue
			,target.ResultComment = source.ResultComment
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.ResultChecksum = source.ResultChecksum

output
	 coalesce(inserted.ResultRecno, deleted.ResultRecno)
	,$action
	into
		Result.CENICEProcessList
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			Result.CENICEProcessList
		) MergeSummary
;

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

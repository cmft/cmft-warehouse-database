﻿CREATE PROCEDURE [ETL].[LoadWAEncounter]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select
	@StartTime = getdate()

/* If no Load Start and End Date parameters supplied, use the earliest Episode End Date
   and the latest Episode Start Date */
select
	 @LoadStartDate = MIN(CONVERT(datetime, AppointmentDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, AppointmentDate)) 
from
	ETL.TLoadWAEncounter

delete from WA.Encounter 
where	
	Encounter.AppointmentDate between @LoadStartDate and @LoadEndDate
and	not exists
	(
	select
		1
	from
		ETL.TLoadWAEncounter
	where
		TLoadWAEncounter.SourceUniqueID = Encounter.SourceUniqueID
	)


SELECT @RowsDeleted = @@Rowcount

update WA.Encounter
set
	 SourceUniqueID = TEncounter.SourceUniqueID
	,SourcePatientNo = TEncounter.SourcePatientNo
	,SourceEncounterNo = TEncounter.SourceEncounterNo
	,PatientTitle = TEncounter.PatientTitle
	,PatientForename = TEncounter.PatientForename
	,PatientSurname = TEncounter.PatientSurname
	,DateOfBirth = TEncounter.DateOfBirth
	,DateOfDeath = TEncounter.DateOfDeath
	,SexCode = TEncounter.SexCode
	,NHSNumber = TEncounter.NHSNumber
	,DistrictNo = TEncounter.DistrictNo
	,Postcode = TEncounter.Postcode
	,PatientAddress1 = TEncounter.PatientAddress1
	,PatientAddress2 = TEncounter.PatientAddress2
	,PatientAddress3 = TEncounter.PatientAddress3
	,PatientAddress4 = TEncounter.PatientAddress4
	,DHACode = TEncounter.DHACode
	,EthnicOriginCode = TEncounter.EthnicOriginCode
	,MaritalStatusCode = TEncounter.MaritalStatusCode
	,ReligionCode = TEncounter.ReligionCode
	,RegisteredGpCode = TEncounter.RegisteredGpCode
	,RegisteredGpPracticeCode = TEncounter.RegisteredGpPracticeCode
	,SiteCode = TEncounter.SiteCode
	,AppointmentDate = TEncounter.AppointmentDate
	,AppointmentTime = TEncounter.AppointmentTime
	,ClinicCode = TEncounter.ClinicCode
	,AdminCategoryCode = TEncounter.AdminCategoryCode
	,SourceOfReferralCode = TEncounter.SourceOfReferralCode
	,ReasonForReferralCode = TEncounter.ReasonForReferralCode
	,PriorityCode = TEncounter.PriorityCode
	,FirstAttendanceFlag = TEncounter.FirstAttendanceFlag
	,DNACode = TEncounter.DNACode
	,AppointmentStatusCode = TEncounter.AppointmentStatusCode
	,CancelledByCode = TEncounter.CancelledByCode
	,TransportRequiredFlag = TEncounter.TransportRequiredFlag
	,AttendanceOutcomeCode = TEncounter.AttendanceOutcomeCode
	,AppointmentTypeCode = TEncounter.AppointmentTypeCode
	,DisposalCode = TEncounter.DisposalCode
	,ConsultantCode = TEncounter.ConsultantCode
	,SpecialtyCode = TEncounter.SpecialtyCode
	,ReferringConsultantCode = TEncounter.ReferringConsultantCode
	,ReferringSpecialtyCode = TEncounter.ReferringSpecialtyCode
	,BookingTypeCode = TEncounter.BookingTypeCode
	,CasenoteNo = TEncounter.CasenoteNo
	,AppointmentCreateDate = TEncounter.AppointmentCreateDate
	,EpisodicGpCode = TEncounter.EpisodicGpCode
	,EpisodicGpPracticeCode = TEncounter.EpisodicGpPracticeCode
	,DoctorCode = TEncounter.DoctorCode
	,PrimaryDiagnosisCode = TEncounter.PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode = TEncounter.SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1 = TEncounter.SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2 = TEncounter.SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3 = TEncounter.SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4 = TEncounter.SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5 = TEncounter.SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6 = TEncounter.SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7 = TEncounter.SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8 = TEncounter.SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9 = TEncounter.SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10 = TEncounter.SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11 = TEncounter.SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12 = TEncounter.SecondaryDiagnosisCode12
	,PrimaryOperationCode = TEncounter.PrimaryOperationCode
	,PrimaryOperationDate = TEncounter.PrimaryOperationDate
	,SecondaryOperationCode1 = TEncounter.SecondaryOperationCode1
	,SecondaryOperationDate1 = TEncounter.SecondaryOperationDate1
	,SecondaryOperationCode2 = TEncounter.SecondaryOperationCode2
	,SecondaryOperationDate2 = TEncounter.SecondaryOperationDate2
	,SecondaryOperationCode3 = TEncounter.SecondaryOperationCode3
	,SecondaryOperationDate3 = TEncounter.SecondaryOperationDate3
	,SecondaryOperationCode4 = TEncounter.SecondaryOperationCode4
	,SecondaryOperationDate4 = TEncounter.SecondaryOperationDate4
	,SecondaryOperationCode5 = TEncounter.SecondaryOperationCode5
	,SecondaryOperationDate5 = TEncounter.SecondaryOperationDate5
	,SecondaryOperationCode6 = TEncounter.SecondaryOperationCode6
	,SecondaryOperationDate6 = TEncounter.SecondaryOperationDate6
	,SecondaryOperationCode7 = TEncounter.SecondaryOperationCode7
	,SecondaryOperationDate7 = TEncounter.SecondaryOperationDate7
	,SecondaryOperationCode8 = TEncounter.SecondaryOperationCode8
	,SecondaryOperationDate8 = TEncounter.SecondaryOperationDate8
	,SecondaryOperationCode9 = TEncounter.SecondaryOperationCode9
	,SecondaryOperationDate9 = TEncounter.SecondaryOperationDate9
	,SecondaryOperationCode10 = TEncounter.SecondaryOperationCode10
	,SecondaryOperationDate10 = TEncounter.SecondaryOperationDate10
	,SecondaryOperationCode11 = TEncounter.SecondaryOperationCode11
	,SecondaryOperationDate11 = TEncounter.SecondaryOperationDate11
	,PurchaserCode = TEncounter.PurchaserCode
	,ProviderCode = TEncounter.ProviderCode
	,ContractSerialNo = TEncounter.ContractSerialNo
	,ReferralDate = TEncounter.ReferralDate
	,RTTPathwayID = TEncounter.RTTPathwayID
	,RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	,RTTPeriodStatusCode = TEncounter.RTTPeriodStatusCode
	,AppointmentCategoryCode = TEncounter.AppointmentCategoryCode
	,AppointmentCreatedBy = TEncounter.AppointmentCreatedBy
	,AppointmentCancelDate = TEncounter.AppointmentCancelDate
	,LastRevisedDate = TEncounter.LastRevisedDate
	,LastRevisedBy = TEncounter.LastRevisedBy
	,OverseasStatusFlag = TEncounter.OverseasStatusFlag
	,PatientChoiceCode = TEncounter.PatientChoiceCode
	,ScheduledCancelReasonCode = TEncounter.ScheduledCancelReasonCode
	,PatientCancelReason = TEncounter.PatientCancelReason
	,DischargeDate = TEncounter.DischargeDate
	,QM08StartWaitDate = TEncounter.QM08StartWaitDate
	,QM08EndWaitDate = TEncounter.QM08EndWaitDate
	,DestinationSiteCode = TEncounter.DestinationSiteCode
	,EBookingReferenceNo = TEncounter.EBookingReferenceNo
	,InterfaceCode = TEncounter.InterfaceCode
	,LocalAdminCategoryCode = TEncounter.LocalAdminCategoryCode
	,PCTCode = TEncounter.PCTCode
	,LocalityCode = TEncounter.LocalityCode
	,SeenByCode = TEncounter.SeenByCode

	,Updated = getdate()
	,ByWhom = system_user
from
	ETL.TLoadWAEncounter TEncounter
where
	TEncounter.SourceUniqueID = WA.Encounter.SourceUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO WA.Encounter
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode
	,SeenByCode
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,DNACode
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AttendanceOutcomeCode
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,InterfaceCode
	,LocalAdminCategoryCode
	,PCTCode
	,LocalityCode
	,SeenByCode

	,Created = getdate()
	,ByWhom = system_user
from
	ETL.TLoadWAEncounter
where
	not exists
	(
	select
		1
	from
		WA.Encounter
	where
		Encounter.SourceUniqueID = TLoadWAEncounter.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

EXEC Utility.WriteAuditLogEvent 'LoadWAEncounter', @Stats, @StartTime

print @Stats

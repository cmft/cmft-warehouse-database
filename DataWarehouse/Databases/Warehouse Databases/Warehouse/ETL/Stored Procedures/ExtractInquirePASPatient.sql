﻿--/****** Object:  StoredProcedure [ETL].[LoadPASPatient]    Script Date: 10/16/2014 11:28:10 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO

CREATE procedure [ETL].[ExtractInquirePASPatient]
(
@Update bit
)

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)


select @RowsInserted = 0

select
	@StartTime = getdate()

--declare @rc int;
--exec dbo.approxRowCount @db = 'PAS', @schema = 'Inquire',@table = 'PATDATA', @rowcount = @rc output;
--select @rc

--if @rc > 2500000
--begin
--	truncate table PAS.Patient
--	-- insert new records

if @Update = 0

begin
	insert into ETL.TImportPASPatient
	(
		 SourcePatientNo
		,DateOfBirth
		,DeathIndicator
		,DateOfDeath
		,SexCode
		,MaritalStatusCode
		,Surname
		,Forenames
		,Title
		,NextOfKin
		,EthnicOriginCode
		,ReligionCode
		,PreviousSurname
		,NHSNumber
		,NHSNumberStatusId
		,DOR
		,DistrictNo
		,MilitaryNo
		,AddressLine1
		,AddressLine2
		,AddressLine3
		,AddressLine4
		,Postcode
		,RegisteredGpCode
		,RegisteredGdpCode
		,MRSAFlag
		,MRSADate
		,Modified
		,HomePhone
		,MobilePhone
		,WorkPhone
		,AEAttendanceToday
	)

	select
		SourcePatientNo  		
		,DateOfBirth
		,DeathIndicator
		,DateOfDeath
		,SexCode
		,MaritalStatusCode
		,Surname
		,Forenames
		,Title
		,NextOfKin
		,EthnicOriginCode
		,ReligionCode
		,PreviousSurname
		,NHSNumber
		,NHSNumberStatusId
		,DOR 
		,DistrictNo 
		,MilitaryNo
		,AddressLine1
		,AddressLine2
		,AddressLine3
		,AddressLine4 
		,Postcode
		,RegisteredGpCode
		,RegisteredGdpCode 
		,MRSAFlag 
		,MRSADate 
		,Modified
		,HomePhone 
		,MobilePhone 
		,WorkPhone 
		,AEAttendanceToday
	from
		(
		select
			 SourcePatientNo = 
				convert(int , PATDATA.InternalPatientNumber)
			,DateOfBirth =
				convert(date , InternalDateOfBirth , 103)
			,DeathIndicator =
				case 
				when PtDeathIndInt = 0 then 'No'
				when PtDeathIndInt = 1 then 'Yes'
				end
			,DateOfDeath = 
				convert(date , PtDateOfDeath , 103)
			,SexCode = Sex
			,MaritalStatusCode = MaritalStatus
			,Surname
			,Forenames
			,Title
			,NextOfKin = NoKName
			,EthnicOriginCode = EthnicType
			,ReligionCode = Religion
			,PreviousSurname = PreviousSurname1
			,NHSNumber
			,NHSNumberStatusId = NHSNumberStatus
			,DOR = DistrictOfResidenceCode
			,DistrictNo = DistrictNumber
			,MilitaryNo = NULL
			,AddressLine1 = [PtAddrLine1]
			,AddressLine2 = [PtAddrLine2]
			,AddressLine3 = [PtAddrLine3]
			,AddressLine4 = [PtAddrLine4]
			,Postcode = PtAddrPostCode
			,RegisteredGpCode = GpCode
			,RegisteredGdpCode = GdpCode
			,MRSAFlag = MRSA
			,MRSADate = MRSASetDt	
			,Modified =
				case
				when right(DateTimeModifiedInt , 4) = '2400'
					then
						dateadd(
							 day
							,1 
							,cast(
								substring(DateTimeModifiedInt , 7 , 2) + '/' +
								substring(DateTimeModifiedInt , 5 , 2) + '/' +
								left(DateTimeModifiedInt , 4)
							as smalldatetime
							)
						)
				else
					cast(
						substring(DateTimeModifiedInt , 7 , 2) + '/' +
						substring(DateTimeModifiedInt , 5 , 2) + '/' +
						left(DateTimeModifiedInt , 4) +
						' ' + left(right(DateTimeModifiedInt , 4) , 2) + ':' + (right(DateTimeModifiedInt , 2))
					as smalldatetime
					)
				end
			,HomePhone = PATDATA.PtHomePhone
			,MobilePhone = PATDATA.PtMobilePhone
			,WorkPhone = PATDATA.PtWorkPhone
			,AEAttendanceToday = 
						case
						when exists
									(
									select
										1
									from
										[$(PAS)].InquireUpdate.AEATTENDANCE
									where
										PATDATA.InternalPatientNumber = AEATTENDANCE.InternalPatientNumber
									and AEATTENDANCE.AeAttendanceDatetimeneutral >= convert(varchar, cast(getdate() as date), 112) + '0000'
									) 
						then 1
						end
		from
			[$(PAS)].Inquire.PATDATA
		) Patient
end

else if @Update = 1

begin
	insert into ETL.TImportPASPatient
	(
		 SourcePatientNo
		,DateOfBirth
		,DeathIndicator
		,DateOfDeath
		,SexCode
		,MaritalStatusCode
		,Surname
		,Forenames
		,Title
		,NextOfKin
		,EthnicOriginCode
		,ReligionCode
		,PreviousSurname
		,NHSNumber
		,NHSNumberStatusId
		,DOR
		,DistrictNo
		,MilitaryNo
		,AddressLine1
		,AddressLine2
		,AddressLine3
		,AddressLine4
		,Postcode
		,RegisteredGpCode
		,RegisteredGdpCode
		,MRSAFlag
		,MRSADate
		,Modified
		,HomePhone
		,MobilePhone
		,WorkPhone
		,AEAttendanceToday
	)

	select
		SourcePatientNo  		
		,DateOfBirth
		,DeathIndicator
		,DateOfDeath
		,SexCode
		,MaritalStatusCode
		,Surname
		,Forenames
		,Title
		,NextOfKin
		,EthnicOriginCode
		,ReligionCode
		,PreviousSurname
		,NHSNumber
		,NHSNumberStatusId
		,DOR 
		,DistrictNo 
		,MilitaryNo
		,AddressLine1
		,AddressLine2
		,AddressLine3
		,AddressLine4 
		,Postcode
		,RegisteredGpCode
		,RegisteredGdpCode 
		,MRSAFlag 
		,MRSADate 
		,Modified
		,HomePhone 
		,MobilePhone 
		,WorkPhone 
		,AEAttendanceToday
	from
		(
		select
			 SourcePatientNo = 
				convert(int , PATDATA.InternalPatientNumber)
			,DateOfBirth =
				convert(date , InternalDateOfBirth , 103)
			,DeathIndicator =
				case 
				when PtDeathIndInt = 0 then 'No'
				when PtDeathIndInt = 1 then 'Yes'
				end
			,DateOfDeath = 
				convert(date , PtDateOfDeath , 103)
			,SexCode = Sex
			,MaritalStatusCode = MaritalStatus
			,Surname
			,Forenames
			,Title
			,NextOfKin = NoKName
			,EthnicOriginCode = EthnicType
			,ReligionCode = Religion
			,PreviousSurname = PreviousSurname1
			,NHSNumber
			,NHSNumberStatusId = NHSNumberStatus
			,DOR = DistrictOfResidenceCode
			,DistrictNo = DistrictNumber
			,MilitaryNo = NULL
			,AddressLine1 = [PtAddrLine1]
			,AddressLine2 = [PtAddrLine2]
			,AddressLine3 = [PtAddrLine3]
			,AddressLine4 = [PtAddrLine4]
			,Postcode = PtAddrPostCode
			,RegisteredGpCode = GpCode
			,RegisteredGdpCode = GdpCode
			,MRSAFlag = MRSA
			,MRSADate = MRSASetDt	
			,Modified =
				case
				when right(DateTimeModifiedInt , 4) = '2400'
					then
						dateadd(
							 day
							,1 
							,cast(
								substring(DateTimeModifiedInt , 7 , 2) + '/' +
								substring(DateTimeModifiedInt , 5 , 2) + '/' +
								left(DateTimeModifiedInt , 4)
							as smalldatetime
							)
						)
				else
					cast(
						substring(DateTimeModifiedInt , 7 , 2) + '/' +
						substring(DateTimeModifiedInt , 5 , 2) + '/' +
						left(DateTimeModifiedInt , 4) +
						' ' + left(right(DateTimeModifiedInt , 4) , 2) + ':' + (right(DateTimeModifiedInt , 2))
					as smalldatetime
					)
				end
			,HomePhone = PATDATA.PtHomePhone
			,MobilePhone = PATDATA.PtMobilePhone
			,WorkPhone = PATDATA.PtWorkPhone
			,AEAttendanceToday = 
						case
						when exists
									(
									select
										1
									from
										[$(PAS)].InquireUpdate.AEATTENDANCE
									where
										PATDATA.InternalPatientNumber = AEATTENDANCE.InternalPatientNumber
									and AEATTENDANCE.AeAttendanceDatetimeneutral >= convert(varchar, cast(getdate() as date), 112) + '0000'
									) 
						then 1
						end
		from
			[$(PAS)].InquireUpdate.PATDATA
		) Patient
end



SELECT @RowsInserted = @@Rowcount

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) +
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

print @Stats

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


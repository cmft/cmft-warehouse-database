﻿CREATE procedure [ETL].[ExtractInquireAPCDiagnosis]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'

--select @census = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)


update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSAPCDIAGNOSIS'


insert into ETL.TImportAPCDiagnosis
(
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
)

SELECT 
	 SourceUniqueID = CONSEPISDIAG.CONSEPISDIAGID 
	,SourceSpellNo = CONSEPISDIAG.EpisodeNumber 
	,SourcePatientNo = CONSEPISDIAG.InternalPatientNumber 
	,SourceEncounterNo = CONSEPISDIAG.FCENumber 
	,SequenceNo = CONSEPISDIAG.SecondaryDiagSeqNo 
	,DiagnosisCode = CONSEPISDIAG.SecondaryDiagnosis 
	,APCSourceUniqueID = FCE.FCEEXTID 
FROM
	[$(PAS)].Inquire.FCEEXT FCE

INNER JOIN [$(PAS)].Inquire.CONSEPISDIAG
ON	FCE.EpisodeNumber = CONSEPISDIAG.EpisodeNumber
AND	FCE.InternalPatientNumber = CONSEPISDIAG.InternalPatientNumber
AND	FCE.FceSequenceNo = CONSEPISDIAG.FCENumber
AND	FCE.EpsActvDtimeInt = CONSEPISDIAG.FCEEndDateTime

WHERE
	FCE.EpsActvDtimeInt between @from and @to

union all

SELECT 
	 SourceUniqueID = CONSEPISDIAG.CONSEPISDIAGID 
	,SourceSpellNo = CONSEPISDIAG.EpisodeNumber 
	,SourcePatientNo = CONSEPISDIAG.InternalPatientNumber 
	,SourceEncounterNo = CONSEPISDIAG.FCENumber 
	,SequenceNo = CONSEPISDIAG.SecondaryDiagSeqNo 
	,DiagnosisCode = CONSEPISDIAG.SecondaryDiagnosis 
	,APCSourceUniqueID = Episode.CONSEPISODEID 
FROM
	[$(PAS)].Inquire.CONSEPISDIAG

INNER JOIN [$(PAS)].Inquire.CONSEPISODE Episode 
ON	Episode.EpisodeNumber = CONSEPISDIAG.EpisodeNumber
AND	Episode.InternalPatientNumber = CONSEPISDIAG.InternalPatientNumber
AND	Episode.FCENumber = CONSEPISDIAG.FCENumber

INNER JOIN [$(PAS)].Inquire.ADMITDISCH AdmitDisch 
ON	AdmitDisch.EpisodeNumber = Episode.EpisodeNumber
AND	AdmitDisch.InternalPatientNumber = Episode.InternalPatientNumber

WHERE
	AdmitDisch.IpAdmDtimeInt <= @to
AND	(
		AdmitDisch.IpDschDtimeInt > @to
	or	AdmitDisch.IpDschDtimeInt Is Null
	)
and	not exists
	(
	select
		1
	from
		[$(PAS)].Inquire.FCEEXT FCE
	where
		FCE.EpisodeNumber = CONSEPISDIAG.EpisodeNumber
	AND	FCE.InternalPatientNumber = CONSEPISDIAG.InternalPatientNumber
	AND	FCE.FceSequenceNo = CONSEPISDIAG.FCENumber
	AND	FCE.EpsActvDtimeInt = CONSEPISDIAG.FCEEndDateTime	
	)



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

if @RowsInserted != 0
begin
	update Utility.Parameter set NumericValue = 1 where Parameter = 'EXTRACTSTATUSAPCDIAGNOSIS'
end


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireAPCDiagnosis', @Stats, @StartTime

﻿
CREATE procedure [ETL].[LoadCENICE]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null
as


set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @FromDate = coalesce(@FromDate, (select cast(dateadd(month, -3, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

exec ETL.LoadCENICEReferenceData

truncate table ETL.TImportResultCENICEOrder
exec ETL.ExtractCENICEResultCENICEOrder @FromDate, @ToDate
exec ETL.LoadResultCENICEOrder

truncate table ETL.TImportResultCENICEResult
exec ETL.ExtractCENICEResultCENICEResult @FromDate, @ToDate
exec ETL.LoadResultCENICEResult

truncate table ETL.TImportResultCENICETest
exec ETL.ExtractCENICEResultCENICETest @FromDate, @ToDate
exec ETL.LoadResultCENICETest

truncate table ETL.TImportResultCENICEOrderResult
exec ETL.ExtractCENICEResultCENICEOrderResult @FromDate, @ToDate
exec ETL.LoadResultCENICEOrderResult



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	convert(varchar(6), @Elapsed) + ' Mins, Period ' + 
	convert(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	convert(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿
CREATE procedure [ETL].[BuildCENICEProcessListArchive] as

insert
into
	Result.CENICEProcessListArchive
(
	 ResultRecno
	,Action
	,ArchiveTime
)
select
	 ResultRecno
	,Action
	,ArchiveTime = getdate()
from
	Result.CENICEProcessList;


truncate table Result.CENICEProcessList



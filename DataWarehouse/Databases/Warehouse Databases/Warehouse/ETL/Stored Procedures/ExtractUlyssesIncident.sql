﻿

CREATE proc [ETL].[ExtractUlyssesIncident]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)


--improver performance of proc (query plan usage)
declare @FromDatelocal datetime = @FromDate
declare @ToDateLocal datetime = @ToDate

select @StartTime = getdate()

select @RowsInserted = 0

--select @fromDate = coalesce(@fromDate, (select cast(dateadd(year, -1, getdate()) as date)))
--select @toDate = coalesce(@toDate, (select cast(getdate() as date)))

truncate table ETL.TImportIncidentIncident

insert into ETL.TImportIncidentIncident
(
	SourceIncidentCode
	,IncidentCode
	,IncidentNumber
	,Description
	,IncidentDate
	,IncidentTime
	,ReportedDate
	,ReportedTime
	,ReceiptDate
	,CauseCode1
	,CauseCode2
	,IncidentTypeCode
	,ContributingFactorCode
	,SignedByCode
	,IncidentGradeCode
	,CompletedByCode
	,SeverityCode
	,LikelihoodCode
	,RiskRatingCode
	,RiskScore
	,SpecialtyCode
	,PatientSafety
	,CauseGroupCode
	,SeriousUntowardIncident
	,ResearchOrTrialProject
	,InitialSeverityCode
	,InitialLikelihoodCode
	,InitialRiskRatingCode
	,InitialRiskScore
	,SecurityIncidentReportingSystemReportable
	,StatusTypeCode
	,NeverEvent
	,OrganisationCode
	,SiteTypeCode
	,SiteCode
	,DivisionCode
	,DirectorateCode
	,LocationCode
	,DepartmentCode
	,SequenceNo
	,PersonCode
	,PersonTypeCode
	,EntityTypeCode
	,DateOfBirth
	,SexCode
	,CasenoteNumber
	,NHSNumber
)

select
	SourceIncidentCode
	,IncidentCode
	,IncidentNumber
	,Description
	,IncidentDate
	,IncidentTime
	,ReportedDate
	,ReportedTime
	,ReceiptDate 
	,CauseCode1 
	,CauseCode2 
	,IncidentTypeCode 
	,ContributingFactorCode
	,SignedByCode
	,IncidentGrade 
	,CompletedByCode
	,SeverityCode 
	,LikelihoodCode 
	,RiskRatingCode
	,RiskScore 
	,SpecialtyCode 
	,PatientSafety 
	,CauseGroupCode 
	,SeriousUntowardIncident 
	,ResearchOrTrialProject
	,InitialSeverityCode 
	,InitialLikelihoodCode 
	,InitialRiskRatingCode 
	,InitialRiskScore 
	,SecurityIncidentReportingSystemReportable 
	,StatusTypeCode 
	,NeverEvent 
	,OrganisationCode 
	,SiteTypeCode 
	,SiteCode 
	,DivisionCode 
	,DirectorateCode 
	,LocationCode 
	,DepartmentCode 
	,SequenceNo = row_number() over(partition by IncidentCode order by PersonCode)
	,PersonCode 
	,PersonTypeCode 
	,EntityTypeCode 
	,DateOfBirth
	,SexCode
	,CasenoteNumber
	,NHSNumber


from
	(
	select --top 100
		SourceIncidentCode = Incident.INC_CODE
		,IncidentCode = cast(Incident.INC_CODE as varbinary(50))
		,Description = Incident.DESCRIPT
		,SiteCode = Incident.SITE
		,DepartmentCode = Incident.DEPARTMENT
		,LocationCode = nullif(Incident.LOCATION, '  ')
		,IncidentDate = Incident.INC_DATE
						--case
						--	when isdate(cast(Incident.INC_DATE as varchar)) = 0 
						--	then null
						--	else Incident.INC_DATE		
						--end
		,IncidentTime = cast(Incident.INC_DATE as varchar(10)) + ' ' + coalesce(
																				replace(
																						Incident.TIME
																						,'     '																																																					
																						,'00:00'
																						)
																				,'00:00'
																				)
						--case
						--	when isdate(cast(Incident.INC_DATE as varchar)) = 0 
						--	then null
						--	else cast(Incident.INC_DATE as varchar(10)) + ' ' + coalesce(
						--														replace(
						--																Incident.TIME
						--																,'     '																																																					
						--																,'00:00'
						--																)
						--														,'00:00'
						--														)
						--end
		,ReportedDate = Incident.REPORTED
		,ReportedTime = 
						cast(Incident.REPORTED as varchar(10)) + ' ' + coalesce(
																				replace(
																						Incident.REPORT_TM
																						,'     '																																																					
																						,'00:00'
																						)
																				,'00:00'
																				)
						--case
						--	when isdate(cast(Incident.REPORTED as varchar)) = 0 
						--	then null
						--	else cast(Incident.REPORTED as varchar(10)) + ' ' + coalesce(
						--														replace(
						--																Incident.REPORT_TM
						--																,'     '																																																					
						--																,'00:00'
						--																)
						--														,'00:00'
						--														)
						--	end
		,CauseCode1 = nullif(Incident.INC_CAUSE1, '  ')
		,CauseCode2 = nullif(Incident.INC_CAUSE2, '  ')
		,IncidentTypeCode = nullif(INC_TYPE, '  ')
		,ContributingFactorCode = nullif(Incident.CONTRIB_FC, '  ')
		,SignedByCode = nullif(Incident.DR_NAME, '                         ')
		,IncidentNumber = Incident.NUMBER
		,DirectorateCode = nullif(Incident.DIRECTORAT, '  ')
		,IncidentGrade = nullif(Incident.GRADE, '  ') 
		,CompletedByCode = nullif(Incident.COMPLET_BY, '                         ')
		,SeverityCode = nullif(Incident.SEVERITY, '  ')
		,LikelihoodCode = nullif(Incident.LIKELIHOOD, '  ')
		,RiskRatingCode = nullif(Incident.RISK_RATE, '  ')
		,RiskScore = Incident.RISK_SCORE
		,SpecialtyCode = nullif(Incident.SPECIALTY, '  ')
		,SiteTypeCode = nullif(Incident.SITE_TYPE, '  ')
		,OrganisationCode = Incident.ORGANISE
		,PatientSafety = nullif(Incident.PSI, ' ')
		,CauseGroupCode = nullif(Incident.CAUSE_GRP, '  ')
		,ReceiptDate = Incident.RECEIVE_DT
		,SeriousUntowardIncident = nullif(Incident.SUI, ' ')
		,ResearchOrTrialProject = nullif(Incident.RESEARCH, ' ')
		,InitialSeverityCode = nullif(Incident.INIT_SEVER, '  ')
		,InitialLikelihoodCode = nullif(Incident.INIT_LIKE, '  ')   
		,InitialRiskRatingCode = nullif(Incident.INIT_RATE, '  ')
		,InitialRiskScore = Incident.INIT_SCORE
		,SecurityIncidentReportingSystemReportable = nullif(Incident.CLIN_ASSLT, ' ')
		,StatusTypeCode = nullif(Incident.STATUS_TYP, '  ')
		,NeverEvent = Incident.NEVER_EVT
		,DivisionCode = nullif(Incident.DIVISION, '  ')

		,PersonCode = nullif(IncidentPerson.PERSON_COD, '    ')
		,PersonTypeCode = nullif(IncidentPerson.PERSON_TY, '  ')
		,EntityTypeCode = IncidentPerson.TYPE -- not sure what this but no reference data and not dictionary?
		,DateOfBirth = Person.DATE_BIRTH
		,SexCode = nullif(Person.SEX, ' ')
		,CasenoteNumber = nullif([ID1], '               ')
		,NHSNumber = nullif(NHS_NO, '          ')
		
	from
		[$(Ulysses)].Incident.INCIDENT Incident

	left join [$(Ulysses)].Incident.INCPERSO IncidentPerson
	on	cast(Incident.INC_CODE as varbinary(50)) = cast(IncidentPerson.INC_CODE as varbinary(50)) 

	left join [$(Ulysses)].Incident.PERSON Person
	on	cast(Person.PERSON_COD as varbinary(50)) = cast(IncidentPerson.PERSON_COD as varbinary(50))

	where
		Incident.INC_DATE between @FromDatelocal and @ToDateLocal
	) Incident

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

-- Update extract status
if @RowsInserted != 0
	update Utility.Parameter set NumericValue = 1 where Parameter = 'EXTRACTSTATUSULYSSESINCIDENT'

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime






﻿
CREATE PROCEDURE [ETL].[LoadGUMEncounter]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, AppointmentDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, AppointmentDate)) 
from
	ETL.TLoadGUMEncounter

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	GUM.Encounter target
using
	(
	select
		AppointmentDate
		,AppointmentTime
		,PatientIdentifier
		,Postcode
		,ClinicID
		,AppointmentTypeID
		,AttendanceStatusID
		,InterfaceCode
	from
		ETL.TLoadGUMEncounter
	) source
	on	source.AppointmentTime = target.AppointmentTime
	and	source.PatientIdentifier = target.PatientIdentifier
	when not matched by source
	and	target.AppointmentDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 AppointmentDate
			,AppointmentTime
			,PatientIdentifier
			,Postcode
			,ClinicID
			,AppointmentTypeID
			,AttendanceStatusID
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.AppointmentDate
			,source.AppointmentTime
			,source.PatientIdentifier
			,source.Postcode
			,source.ClinicID
			,source.AppointmentTypeID
			,source.AttendanceStatusID
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.AppointmentDate, getdate()) = isnull(source.AppointmentDate, getdate())
		and isnull(target.AppointmentTime, getdate()) = isnull(source.AppointmentTime, getdate())
		and isnull(target.PatientIdentifier, '') = isnull(source.PatientIdentifier, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.ClinicID, 0) = isnull(source.ClinicID, 0)
		and isnull(target.AppointmentTypeID, 0) = isnull(source.AppointmentTypeID, 0)
		and isnull(target.AttendanceStatusID, 0) = isnull(source.AttendanceStatusID, 0)
		and isnull(target.InterfaceCode, 0) = isnull(source.InterfaceCode, 0)
		)
	then
		update
		set
			 target.AppointmentDate = source.AppointmentDate
			,target.AppointmentTime = source.AppointmentTime
			,target.PatientIdentifier = source.PatientIdentifier
			,target.Postcode = source.Postcode
			,target.ClinicID = source.ClinicID
			,target.AppointmentTypeID = source.AppointmentTypeID
			,target.AttendanceStatusID = source.AttendanceStatusID
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
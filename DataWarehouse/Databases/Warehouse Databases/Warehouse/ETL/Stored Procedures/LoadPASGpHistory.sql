﻿
create proc ETL.LoadPASGpHistory

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select
	@StartTime = getdate()

truncate table [PAS].[GpHistory]

insert into [PAS].[GpHistory]
(
	[GpHistoryID]
	,[DistrictNo]
	,EffectiveFromDate
	--,[FromDateTimeInteger]
	,[GpAddressLine1]
	,[GpAddressLine2]
	,[GpAddressLine3]
	,[GpAddressLine4]
	,[GpCode]
	,[GpfhCode]
	,[Initials]
	,[SourcePatientNo]
	,[NationalCode]
	,[Phone]
	--,[PmiPatientGpHistoryToDateIndexKey]
	,[Postcode]
	,[Practice]
	,[SequenceNo]
	,[Surname]
	,[Title]
	,EffectiveToDate
)

select
	[GpHistoryID]
	,[DistrictNo]
	,EffectiveFromDate
	--,[FromDateTimeInteger]
	,[GpAddressLine1]
	,[GpAddressLine2]
	,[GpAddressLine3]
	,[GpAddressLine4]
	,[GpCode]
	,[GpfhCode]
	,[Initials]
	,[SourcePatientNo]
	,[NationalCode]
	,[Phone]
	--,[PmiPatientGpHistoryToDateIndexKey]
	,[Postcode]
	,[Practice]
	,[SequenceNo]
	,[Surname]
	,[Title]
	,EffectiveToDate
from
	[ETL].[TLoadPASGpHistory]

select @RowsInserted = @@Rowcount


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'LoadPASGpHistory', @Stats, @StartTime


﻿CREATE procedure [ETL].[LoadPASCasenote] as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)



--need to copy data to a temporary table to allow removal of duplicate record

CREATE TABLE #Casenote(
	 CasenoteRecno int identity
	,SourcePatientNo varchar(9) NOT NULL
	,CasenoteNumber varchar(14) NOT NULL
	,AllocatedDate date NULL
	,CasenoteLocationCode varchar(4) NULL
	,CasenoteLocationDate date NULL
	,CasenoteStatus varchar(10) NULL
	,WithdrawnDate date NULL
	,Comment varchar(50) NULL
	,CurrentBorrowerCode nvarchar(6) NULL
	,LoanTime smalldatetime NULL
	,ExpectedReturnDate date NULL
)

insert
into
	#Casenote
(
	 SourcePatientNo
	,CasenoteNumber
	,AllocatedDate
	,CasenoteLocationCode
	,CasenoteLocationDate
	,CasenoteStatus
	,WithdrawnDate
	,Comment
	,CurrentBorrowerCode
	,LoanTime
	,ExpectedReturnDate
)
select
	 SourcePatientNo = Casenote.InternalPatientNumber
	,CasenoteNumber = Casenote.PtCasenoteIdPhyskey

	,AllocatedDate =
		case
		when isdate(Allocated) = 1 then convert(date , Allocated)
		else
			convert(
				 date 
				,left(Allocated , 4) + '-' + substring(Allocated , 5 , 2) + '-' + substring(Allocated , 7 , 2)
			)
		end

	,CasenoteLocationCode = Location

	,CasenoteLocationDate =
		case
		when isdate(PtCsNtLocDat) = 1 then convert(date , PtCsNtLocDat)
		else
			convert(
				 date 
				,left(PtCsNtLocDat , 4) + '-' + substring(PtCsNtLocDat , 5 , 2) + '-' + substring(PtCsNtLocDat , 7 , 2)
			)
		end

	,CasenoteStatus = Status

	,WithdrawnDate = 
		case
		when isdate(Withdrawn) = 1 then convert(date , left(Withdrawn , 10 ))
		else null
		end

	,Comment
	,CurrentBorrowerCode = CasenoteLoan.Borrower

	,LoanTime = 
		case
		when CntLoanTimeHhmm = '2400' then
			dateadd(
				minute
				,1
				,cast(
					cast(
						left(CntPatientFileLoanDatesequenceNoKey , 8 )
					as varchar
					)
					+ ' 23:59'
					as smalldatetime
				)
			)
		else			
			cast(
				cast(left(CntPatientFileLoanDatesequenceNoKey , 8 ) as varchar ) + ' ' +

				coalesce(
					cast(substring(CntLoanTimeHhmm , 1 , 2) as varchar) + ':' +
					cast(substring(CntLoanTimeHhmm , 3 , 2) as varchar)
					,'00:00'
				)
			as smalldatetime
			)
		end

	,ExpectedReturnDate = convert(date , CntExpectedReturnDateNeutral)
from
	[$(PAS)].Inquire.CASENOTEDETS Casenote

left join [$(PAS)].Inquire.CASENOTELOAN CasenoteLoan
on	CasenoteLoan.InternalPatientNumber = Casenote.InternalPatientNumber
and	CasenoteLoan.PtCasenoteIdPhyskey = Casenote.PtCasenoteIdPhyskey
and	CasenoteLoan.CntReturnTimeHhmm is null
and	not exists
	(
	select
		1
	from
		[$(PAS)].Inquire.CASENOTELOAN Previous
	where
		Previous.InternalPatientNumber = Casenote.InternalPatientNumber
	and	Previous.PtCasenoteIdPhyskey = Casenote.PtCasenoteIdPhyskey
	and	Previous.CntReturnTimeHhmm is null
	and	Previous.CASENOTELOANID > CasenoteLoan.CASENOTELOANID
	)

CREATE INDEX T_IX_Casenote ON #Casenote
(
	SourcePatientNo ASC,
	CasenoteNumber ASC
)

--remove duplicates
delete
from
	#Casenote

from
	#Casenote

inner join
	(
	select
		 SourcePatientNo
		,CasenoteNumber
		,CasenoteRecno = min(CasenoteRecno)
	from
		#Casenote
	group by
		 SourcePatientNo
		,CasenoteNumber
	having
		COUNT(*) > 1
	) Duplicate
on	Duplicate.SourcePatientNo = #Casenote.SourcePatientNo
and	Duplicate.CasenoteNumber = #Casenote.CasenoteNumber
and	Duplicate.CasenoteRecno < #Casenote.CasenoteRecno


CREATE unique CLUSTERED INDEX T_IX_Casenote_Unique ON #Casenote
(
	SourcePatientNo ASC,
	CasenoteNumber ASC
)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	PAS.Casenote target
using
	(
	select
		 SourcePatientNo
		,CasenoteNumber
		,AllocatedDate
		,CasenoteLocationCode
		,CasenoteLocationDate
		,CasenoteStatus
		,WithdrawnDate
		,Comment
		,CurrentBorrowerCode
		,LoanTime
		,ExpectedReturnDate
	from
		#Casenote
	) source
	on	source.SourcePatientNo = target.SourcePatientNo
	and	source.CasenoteNumber = target.CasenoteNumber

	when not matched
	then
		insert
			(
			 SourcePatientNo
			,CasenoteNumber
			,AllocatedDate
			,CasenoteLocationCode
			,CasenoteLocationDate
			,CasenoteStatus
			,WithdrawnDate
			,Comment
			,CurrentBorrowerCode
			,LoanTime
			,ExpectedReturnDate
			)
		values
			(
			 source.SourcePatientNo
			,source.CasenoteNumber
			,source.AllocatedDate
			,source.CasenoteLocationCode
			,source.CasenoteLocationDate
			,source.CasenoteStatus
			,source.WithdrawnDate
			,source.Comment
			,source.CurrentBorrowerCode
			,source.LoanTime
			,source.ExpectedReturnDate
			)

	when matched
	and not
		(
			isnull(target.AllocatedDate, getdate()) = isnull(source.AllocatedDate, getdate())
		and isnull(target.CasenoteLocationCode, '') = isnull(source.CasenoteLocationCode, '')
		and isnull(target.CasenoteLocationDate, getdate()) = isnull(source.CasenoteLocationDate, getdate())
		and isnull(target.CasenoteStatus, '') = isnull(source.CasenoteStatus, '')
		and isnull(target.WithdrawnDate, getdate()) = isnull(source.WithdrawnDate, getdate())
		and isnull(target.Comment, '') = isnull(source.Comment, '')
		and isnull(target.CurrentBorrowerCode, '') = isnull(source.CurrentBorrowerCode, '')
		and isnull(target.LoanTime, getdate()) = isnull(source.LoanTime, getdate())
		and isnull(target.ExpectedReturnDate, getdate()) = isnull(source.ExpectedReturnDate, getdate())

		)
	then
		update
		set
			 target.AllocatedDate = source.AllocatedDate
			,target.CasenoteLocationCode = source.CasenoteLocationCode
			,target.CasenoteLocationDate = source.CasenoteLocationDate
			,target.CasenoteStatus = source.CasenoteStatus
			,target.WithdrawnDate = source.WithdrawnDate
			,target.Comment = source.Comment
			,target.CurrentBorrowerCode = source.CurrentBorrowerCode
			,target.LoanTime = source.LoanTime
			,target.ExpectedReturnDate = source.ExpectedReturnDate

output
	$action into @MergeSummary
;


if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary



select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




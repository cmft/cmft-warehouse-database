﻿


CREATE procedure [ETL].[ExtractMortalityNeonatology]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.Neonatology target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,EpisodeStartTime
		,FormTypeCode
		,MortalityReviewAdded 
		,ReviewStatusCode
		,MRSA 
		,MedicationErrorsReported 
		,CriticalIncident 
		,CauseOfDeath1 
		,CauseOfDeath2 
		,IntensiveCareWithdrawn 
		,LOTADNR 
		,ConsultantDiscussion 
		,DeathExpected 
		,DeathPreventable
		,PalliativeCare 
		,PostMortem 
		,BereavementAppointmentDate 
		,AppointmentAttended 
		,FollowUp 
		,AdditionalInformation 
		,SignificantIssues 
		,AreasOfNotablePractice 
		,AreasForFurtherDevelopment 
		,AdditionalInstructions 
		,SourceOfAdmission 
		,Gestation 
		,BirthWeight
		,DelayInDiagnosis 
		,DelayInDelivering
		,PoorComms 
		,OrganisationalFailure 
		,AreasofConcernND 
		,AreasofConcernContributed 
		,DoneDifferently 
		,GoodQualityDetails
		,DocStandards 
		,FurtherComments 
		,ClassificationScore 
		,CEFactors 
		,PrimaryReviewerID 
		,PrimaryReviewerName
		,PRAssignedTime 
		,PRCompletedTime
		,SecondaryReviewerID 
		,SecondaryReviewConsultantList 			
		,SecondaryReviewConsultantDiscussion 
		,SRAssignedTime 
		,SRCompletedTime 
		,SubmittedDiagnostics 
		,SubmittedRecurringThemes
		,ContextCode
	from
		ETL.TLoadMortalityNeonatology
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalityNeonatology Latest
			where
				Latest.SourcePatientNo = TLoadMortalityNeonatology.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalityNeonatology.SourceUniqueID
			)
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourcePatientNo
			,EpisodeStartTime
			,FormTypeCode
			,MortalityReviewAdded 
			,ReviewStatusCode
			,MRSA 
			,MedicationErrorsReported 
			,CriticalIncident 
			,CauseOfDeath1 
			,CauseOfDeath2 
			,IntensiveCareWithdrawn 
			,LOTADNR 
			,ConsultantDiscussion 
			,DeathExpected 
			,DeathPreventable
			,PalliativeCare 
			,PostMortem 
			,BereavementAppointmentDate 
			,AppointmentAttended 
			,FollowUp 
			,AdditionalInformation 
			,SignificantIssues 
			,AreasOfNotablePractice 
			,AreasForFurtherDevelopment 
			,AdditionalInstructions 
			,SourceOfAdmission 
			,Gestation 
			,BirthWeight
			,DelayInDiagnosis 
			,DelayInDelivering
			,PoorComms 
			,OrganisationalFailure 
			,AreasofConcernND 
			,AreasofConcernContributed 
			,DoneDifferently 
			,GoodQualityDetails
			,DocStandards 
			,FurtherComments 
			,ClassificationScore 
			,CEFactors 
			,PrimaryReviewerID 
			,PrimaryReviewerName
			,PRAssignedTime 
			,PRCompletedTime
			,SecondaryReviewerID 
			,SecondaryReviewConsultantList 			
			,SecondaryReviewConsultantDiscussion 
			,SRAssignedTime 
			,SRCompletedTime 
			,SubmittedDiagnostics 
			,SubmittedRecurringThemes
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.FormTypeCode
			,source.MortalityReviewAdded 
			,source.ReviewStatusCode
			,source.MRSA 
			,source.MedicationErrorsReported 
			,source.CriticalIncident 
			,source.CauseOfDeath1 
			,source.CauseOfDeath2 
			,source.IntensiveCareWithdrawn 
			,source.LOTADNR 
			,source.ConsultantDiscussion 
			,source.DeathExpected 
			,source.DeathPreventable
			,source.PalliativeCare 
			,source.PostMortem 
			,source.BereavementAppointmentDate 
			,source.AppointmentAttended 
			,source.FollowUp 
			,source.AdditionalInformation 
			,source.SignificantIssues 
			,source.AreasOfNotablePractice 
			,source.AreasForFurtherDevelopment 
			,source.AdditionalInstructions 
			,source.SourceOfAdmission 
			,source.Gestation 
			,source.BirthWeight
			,source.DelayInDiagnosis 
			,source.DelayInDelivering
			,source.PoorComms 
			,source.OrganisationalFailure 
			,source.AreasofConcernND 
			,source.AreasofConcernContributed 
			,source.DoneDifferently 
			,source.GoodQualityDetails
			,source.DocStandards 
			,source.FurtherComments 
			,source.ClassificationScore 
			,source.CEFactors 
			,source.PrimaryReviewerID 
			,source.PrimaryReviewerName
			,source.PRAssignedTime 
			,source.PRCompletedTime
			,source.SecondaryReviewerID 
			,source.SecondaryReviewConsultantList 			
			,source.SecondaryReviewConsultantDiscussion 
			,source.SRAssignedTime 
			,source.SRCompletedTime 
			,source.SubmittedDiagnostics 
			,source.SubmittedRecurringThemes
			,source.ContextCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.FormTypeCode, 0) = isnull(source.FormTypeCode, 0)
		and isnull(target.MortalityReviewAdded, getdate()) = isnull(source.MortalityReviewAdded, getdate())
		and isnull(target.ReviewStatusCode, 0) = isnull(source.ReviewStatusCode, 0)
		and isnull(target.MRSA, 0) = isnull(source.MRSA, 0)
		and isnull(target.MedicationErrorsReported, 0) = isnull(source.MedicationErrorsReported, 0)
		and isnull(target.CriticalIncident, 0) = isnull(source.CriticalIncident, 0)
		and isnull(target.CauseOfDeath1, '') = isnull(source.CauseOfDeath1, '')
		and isnull(target.CauseOfDeath2, '') = isnull(source.CauseOfDeath2, '')
		and isnull(target.IntensiveCareWithdrawn, 0) = isnull(source.IntensiveCareWithdrawn, 0)
		and isnull(target.LOTADNR, 0) = isnull(source.LOTADNR, 0)
		and isnull(target.ConsultantDiscussion, 0) = isnull(source.ConsultantDiscussion, 0)
		and isnull(target.DeathExpected, 0) = isnull(source.DeathExpected, 0)
		and isnull(target.DeathPreventable, 0) = isnull(source.DeathPreventable, 0)
		and isnull(target.PalliativeCare, 0) = isnull(source.PalliativeCare, 0)
		and isnull(target.PostMortem, 0) = isnull(source.PostMortem, 0)
		and isnull(target.BereavementAppointmentDate, '') = isnull(source.BereavementAppointmentDate, '')
		and isnull(target.AppointmentAttended, 0) = isnull(source.AppointmentAttended, 0)
		and isnull(target.FollowUp, 0) = isnull(source.FollowUp, 0)
		and isnull(target.AdditionalInformation, '') = isnull(source.AdditionalInformation, '')
		and isnull(target.SignificantIssues, '') = isnull(source.SignificantIssues, '')
		and isnull(target.AreasOfNotablePractice, '') = isnull(source.AreasOfNotablePractice, '')
		and isnull(target.AreasForFurtherDevelopment, '') = isnull(source.AreasForFurtherDevelopment, '')
		and isnull(target.AdditionalInstructions, '') = isnull(source.AdditionalInstructions, '')
		and isnull(target.SourceOfAdmission, '') = isnull(source.SourceOfAdmission, '')
		and isnull(target.Gestation, '') = isnull(source.Gestation, '')
		and isnull(target.BirthWeight, '') = isnull(source.BirthWeight, '')
		and isnull(target.DelayInDiagnosis, 0) = isnull(source.DelayInDiagnosis, 0)
		and isnull(target.DelayInDelivering, 0) = isnull(source.DelayInDelivering, 0)
		and isnull(target.PoorComms, 0) = isnull(source.PoorComms, 0)
		and isnull(target.OrganisationalFailure, 0) = isnull(source.OrganisationalFailure, 0)
		and isnull(target.AreasofConcernND, 0) = isnull(source.AreasofConcernND, 0)
		and isnull(target.AreasofConcernContributed, 0) = isnull(source.AreasofConcernContributed, 0)
		and isnull(target.DoneDifferently, 0) = isnull(source.DoneDifferently, 0)
		and isnull(target.GoodQualityDetails, '') = isnull(source.GoodQualityDetails, '')
		and isnull(target.DocStandards, 0) = isnull(source.DocStandards, 0)
		and isnull(target.FurtherComments, '') = isnull(source.FurtherComments, '')
		and isnull(target.ClassificationScore, 0) = isnull(source.ClassificationScore, 0)
		and isnull(target.CEFactors, '') = isnull(source.CEFactors, '')
		and isnull(target.PrimaryReviewerID, 0) = isnull(source.PrimaryReviewerID, 0)
		and isnull(target.PrimaryReviewerName, '') = isnull(source.PrimaryReviewerName, '')
		and isnull(target.PRAssignedTime, getdate()) = isnull(source.PRAssignedTime, getdate())
		and isnull(target.PRCompletedTime, getdate()) = isnull(source.PRCompletedTime, getdate())
		and isnull(target.SecondaryReviewerID, 0) = isnull(source.SecondaryReviewerID, 0)
		and isnull(target.SecondaryReviewConsultantList, '') = isnull(source.SecondaryReviewConsultantList, '')
		and isnull(target.SecondaryReviewConsultantDiscussion, '') = isnull(source.SecondaryReviewConsultantDiscussion, '')
		and isnull(target.SRAssignedTime, getdate()) = isnull(source.SRAssignedTime, getdate())
		and isnull(target.SRCompletedTime, getdate()) = isnull(source.SRCompletedTime, getdate())
		and isnull(target.SubmittedDiagnostics, '') = isnull(source.SubmittedDiagnostics, '')
		and isnull(target.SubmittedRecurringThemes, '') = isnull(source.SubmittedRecurringThemes, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.FormTypeCode = source.FormTypeCode
			,target.MortalityReviewAdded  = source.MortalityReviewAdded 
			,target.ReviewStatusCode = source.ReviewStatusCode
			,target.MRSA = source.MRSA
			,target.MedicationErrorsReported = source.MedicationErrorsReported
			,target.CriticalIncident = source.CriticalIncident
			,target.CauseOfDeath1 = source.CauseOfDeath1
			,target.CauseOfDeath2 = source.CauseOfDeath2
			,target.IntensiveCareWithdrawn = source.IntensiveCareWithdrawn
			,target.LOTADNR = source.LOTADNR
			,target.ConsultantDiscussion = source.ConsultantDiscussion
			,target.DeathExpected = source.DeathExpected
			,target.DeathPreventable = source.DeathPreventable
			,target.PalliativeCare = source.PalliativeCare
			,target.PostMortem = source.PostMortem
			,target.BereavementAppointmentDate = source.BereavementAppointmentDate
			,target.AppointmentAttended = source.AppointmentAttended
			,target.FollowUp = source.FollowUp
			,target.AdditionalInformation = source.AdditionalInformation
			,target.SignificantIssues = source.SignificantIssues
			,target.AreasOfNotablePractice = source.AreasOfNotablePractice
			,target.AreasForFurtherDevelopment = source.AreasForFurtherDevelopment
			,target.AdditionalInstructions = source.AdditionalInstructions
			,target.SourceOfAdmission = source.SourceOfAdmission
			,target.Gestation = source.Gestation
			,target.BirthWeight = source.BirthWeight
			,target.DelayInDiagnosis = source.DelayInDiagnosis
			,target.DelayInDelivering = source.DelayInDelivering
			,target.PoorComms = source.PoorComms
			,target.OrganisationalFailure = source.OrganisationalFailure
			,target.AreasofConcernND = source.AreasofConcernND
			,target.AreasofConcernContributed = source.AreasofConcernContributed
			,target.DoneDifferently = source.DoneDifferently
			,target.GoodQualityDetails = source.GoodQualityDetails
			,target.DocStandards = source.DocStandards
			,target.FurtherComments = source.FurtherComments
			,target.ClassificationScore = source.ClassificationScore
			,target.CEFactors = source.CEFactors
			,target.PrimaryReviewerID = source.PrimaryReviewerID
			,target.PrimaryReviewerName = source.PrimaryReviewerName
			,target.PRAssignedTime = source.PRAssignedTime
			,target.PRCompletedTime = source.PRCompletedTime
			,target.SecondaryReviewerID = source.SecondaryReviewerID
			,target.SecondaryReviewConsultantList = source.SecondaryReviewConsultantList
			,target.SecondaryReviewConsultantDiscussion = source.SecondaryReviewConsultantDiscussion
			,target.SRAssignedTime = source.SRAssignedTime
			,target.SRCompletedTime = source.SRCompletedTime
			,target.SubmittedDiagnostics = source.SubmittedDiagnostics
			,target.SubmittedRecurringThemes = source.SubmittedRecurringThemes
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
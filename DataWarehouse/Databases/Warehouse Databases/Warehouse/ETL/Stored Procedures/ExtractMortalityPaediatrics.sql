﻿
CREATE procedure [ETL].[ExtractMortalityPaediatrics]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.Paediatrics target
using
	(
	select
		 SourceUniqueID
		,FormTypeID
		,ReviewStatus
		,ReviewedDate
		,ReviewedBy
		,Designation
		,Comments
		,CasenoteNo
		,DateOfDeath
		,AdmissionTime
		,ClinicalSummary
		,PlaceOfDeath
		,AgeAtDeath
		,TransferredFromOtherHospital
		,TransferredFrom
		,TransferredFromOther
		,ClinicalDiagnosis
		,AdmissionForTerminalCare
		,EndOfLifeCarePlan
		,AdmittedToICU
		,ICUSource
		,ICUWardEWS
		,ICUWardComments
		,TheatrePlanned
		,TheatrePlannedComment
		,AEComments
		,DrugErrorIdentified
		,DrugErrorIdentifiedComments
		,MRSA
		,CDifficile
		,CriticalIncidentBeforeMortalityReview
		,CriticalIncidentBeforeMortalityReviewComments
		,WithdrawalOfCare
		,Indication
		,DocumentationType
		,WithdrawlOfCareByWhom
		,LeadConsultant
		,LeadConsultantEasilyIdentified
		,OtherConsultantInvolved
		,GradeOfMostSeniorClinicianInvolved
		,PreviousAdmission
		,ChronicIllness
		,ChronicIllnessSpecify
		,RelevantPreviousAdmissionsAttendances
		,OperationsProceduresPerformed
		,CaseReferredToCoroner
		,ConsultantReferringCase
		,CaseDiscussedWithCoroner
		,CaseDiscussedWithCoronerConsultant
		,CaseDiscussedWithCoronerDocumentedInMedicalRecords
		,CaseDiscussedWithCoronerPostMortemPerformed
		,CaseDiscussedWithCoronerOutcomeAppropriate
		,CaseDiscussedWithCoronerOutcomeNotAppropriateComments
		,DeathCertificateIssued
		,RequestForHospitalPostMortem
		,RequestForHospitalPostMortemMadeToParentsByWhom
		,RequestForHospitalPostMortemMadeToParentsByOther
		,PermissionForHospitalPostMortemGiven
		,FullPermissionForHospitalPostMortemGiven
		,LimitedPermissionForHospitalPostMortemGiven
		,CauseOfDeath1a
		,CauseOfDeath1b
		,CauseOfDeath1c
		,CauseOfDeath2
		,CauseOfDeathAgreed
		,CauseOfDeathNotAgreedReason
		,AnyFurtherPointsOfDiscussion
		,SignificantQuestionsWhichRemainUnanswered
		,IssuesForDiscussion
		,OrganDonationDiscussed
		,TissueDonationDiscussed
		,OrgansDonated
		,TissuesDonated
		,SecondaryReviewReason
		,SecondaryReviewOutcome
		,SecondaryReviewActions
		,SecondaryReviewPatientCareConsultant
		,SecondaryReviewDiscussedConsultants
		,SecondaryReviewer
		,SecondaryReviewerDesignation
		,SecondaryReviewFuturePresentation
		,SecondaryReviewFuturePresentationPersonNominated
		,FirstReviewerUser
		,SecondReviewerUser
		,SecondaryReviewRequired
		,PersonNominatedForSecondReview
		,DiagDiseaseGroup
		,RecurringThemes
		,CodingCategory
		,SourcePatientNo
		,EpisodeStartTime
		,ContextCode

	from
		ETL.TLoadMortalityPaediatrics
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalityPaediatrics Latest
			where
				Latest.SourcePatientNo = TLoadMortalityPaediatrics.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalityPaediatrics.SourceUniqueID
			)
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,FormTypeID
			,ReviewStatus
			,ReviewedDate
			,ReviewedBy
			,Designation
			,Comments
			,CasenoteNo
			,DateOfDeath
			,AdmissionTime
			,ClinicalSummary
			,PlaceOfDeath
			,AgeAtDeath
			,TransferredFromOtherHospital
			,TransferredFrom
			,TransferredFromOther
			,ClinicalDiagnosis
			,AdmissionForTerminalCare
			,EndOfLifeCarePlan
			,AdmittedToICU
			,ICUSource
			,ICUWardEWS
			,ICUWardComments
			,TheatrePlanned
			,TheatrePlannedComment
			,AEComments
			,DrugErrorIdentified
			,DrugErrorIdentifiedComments
			,MRSA
			,CDifficile
			,CriticalIncidentBeforeMortalityReview
			,CriticalIncidentBeforeMortalityReviewComments
			,WithdrawalOfCare
			,Indication
			,DocumentationType
			,WithdrawlOfCareByWhom
			,LeadConsultant
			,LeadConsultantEasilyIdentified
			,OtherConsultantInvolved
			,GradeOfMostSeniorClinicianInvolved
			,PreviousAdmission
			,ChronicIllness
			,ChronicIllnessSpecify
			,RelevantPreviousAdmissionsAttendances
			,OperationsProceduresPerformed
			,CaseReferredToCoroner
			,ConsultantReferringCase
			,CaseDiscussedWithCoroner
			,CaseDiscussedWithCoronerConsultant
			,CaseDiscussedWithCoronerDocumentedInMedicalRecords
			,CaseDiscussedWithCoronerPostMortemPerformed
			,CaseDiscussedWithCoronerOutcomeAppropriate
			,CaseDiscussedWithCoronerOutcomeNotAppropriateComments
			,DeathCertificateIssued
			,RequestForHospitalPostMortem
			,RequestForHospitalPostMortemMadeToParentsByWhom
			,RequestForHospitalPostMortemMadeToParentsByOther
			,PermissionForHospitalPostMortemGiven
			,FullPermissionForHospitalPostMortemGiven
			,LimitedPermissionForHospitalPostMortemGiven
			,CauseOfDeath1a
			,CauseOfDeath1b
			,CauseOfDeath1c
			,CauseOfDeath2
			,CauseOfDeathAgreed
			,CauseOfDeathNotAgreedReason
			,AnyFurtherPointsOfDiscussion
			,SignificantQuestionsWhichRemainUnanswered
			,IssuesForDiscussion
			,OrganDonationDiscussed
			,TissueDonationDiscussed
			,OrgansDonated
			,TissuesDonated
			,SecondaryReviewReason
			,SecondaryReviewOutcome
			,SecondaryReviewActions
			,SecondaryReviewPatientCareConsultant
			,SecondaryReviewDiscussedConsultants
			,SecondaryReviewer
			,SecondaryReviewerDesignation
			,SecondaryReviewFuturePresentation
			,SecondaryReviewFuturePresentationPersonNominated
			,FirstReviewerUser
			,SecondReviewerUser
			,SecondaryReviewRequired
			,PersonNominatedForSecondReview
			,DiagDiseaseGroup
			,RecurringThemes
			,CodingCategory
			,SourcePatientNo
			,EpisodeStartTime
			,ContextCode

			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.FormTypeID
			,source.ReviewStatus
			,source.ReviewedDate
			,source.ReviewedBy
			,source.Designation
			,source.Comments
			,source.CasenoteNo
			,source.DateOfDeath
			,source.AdmissionTime
			,source.ClinicalSummary
			,source.PlaceOfDeath
			,source.AgeAtDeath
			,source.TransferredFromOtherHospital
			,source.TransferredFrom
			,source.TransferredFromOther
			,source.ClinicalDiagnosis
			,source.AdmissionForTerminalCare
			,source.EndOfLifeCarePlan
			,source.AdmittedToICU
			,source.ICUSource
			,source.ICUWardEWS
			,source.ICUWardComments
			,source.TheatrePlanned
			,source.TheatrePlannedComment
			,source.AEComments
			,source.DrugErrorIdentified
			,source.DrugErrorIdentifiedComments
			,source.MRSA
			,source.CDifficile
			,source.CriticalIncidentBeforeMortalityReview
			,source.CriticalIncidentBeforeMortalityReviewComments
			,source.WithdrawalOfCare
			,source.Indication
			,source.DocumentationType
			,source.WithdrawlOfCareByWhom
			,source.LeadConsultant
			,source.LeadConsultantEasilyIdentified
			,source.OtherConsultantInvolved
			,source.GradeOfMostSeniorClinicianInvolved
			,source.PreviousAdmission
			,source.ChronicIllness
			,source.ChronicIllnessSpecify
			,source.RelevantPreviousAdmissionsAttendances
			,source.OperationsProceduresPerformed
			,source.CaseReferredToCoroner
			,source.ConsultantReferringCase
			,source.CaseDiscussedWithCoroner
			,source.CaseDiscussedWithCoronerConsultant
			,source.CaseDiscussedWithCoronerDocumentedInMedicalRecords
			,source.CaseDiscussedWithCoronerPostMortemPerformed
			,source.CaseDiscussedWithCoronerOutcomeAppropriate
			,source.CaseDiscussedWithCoronerOutcomeNotAppropriateComments
			,source.DeathCertificateIssued
			,source.RequestForHospitalPostMortem
			,source.RequestForHospitalPostMortemMadeToParentsByWhom
			,source.RequestForHospitalPostMortemMadeToParentsByOther
			,source.PermissionForHospitalPostMortemGiven
			,source.FullPermissionForHospitalPostMortemGiven
			,source.LimitedPermissionForHospitalPostMortemGiven
			,source.CauseOfDeath1a
			,source.CauseOfDeath1b
			,source.CauseOfDeath1c
			,source.CauseOfDeath2
			,source.CauseOfDeathAgreed
			,source.CauseOfDeathNotAgreedReason
			,source.AnyFurtherPointsOfDiscussion
			,source.SignificantQuestionsWhichRemainUnanswered
			,source.IssuesForDiscussion
			,source.OrganDonationDiscussed
			,source.TissueDonationDiscussed
			,source.OrgansDonated
			,source.TissuesDonated
			,source.SecondaryReviewReason
			,source.SecondaryReviewOutcome
			,source.SecondaryReviewActions
			,source.SecondaryReviewPatientCareConsultant
			,source.SecondaryReviewDiscussedConsultants
			,source.SecondaryReviewer
			,source.SecondaryReviewerDesignation
			,source.SecondaryReviewFuturePresentation
			,source.SecondaryReviewFuturePresentationPersonNominated
			,source.FirstReviewerUser
			,source.SecondReviewerUser
			,source.SecondaryReviewRequired
			,source.PersonNominatedForSecondReview
			,source.DiagDiseaseGroup
			,source.RecurringThemes
			,source.CodingCategory
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.ContextCode
			,getdate()
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			  isnull(target.FormTypeID, 0) = isnull(source.FormTypeID, 0)
		and isnull(target.ReviewStatus, 0) = isnull(source.ReviewStatus, 0)
		and isnull(target.ReviewedDate, getdate()) = isnull(source.ReviewedDate, getdate())
		and isnull(target.ReviewedBy, '') = isnull(source.ReviewedBy, '')
		and isnull(target.Designation, '') = isnull(source.Designation, '')
		and isnull(target.Comments, '') = isnull(source.Comments, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.ClinicalSummary, '') = isnull(source.ClinicalSummary, '')
		and isnull(target.PlaceOfDeath, '') = isnull(source.PlaceOfDeath, '')
		and isnull(target.AgeAtDeath, '') = isnull(source.AgeAtDeath, '')
		and isnull(target.TransferredFromOtherHospital, 0) = isnull(source.TransferredFromOtherHospital, 0)
		and isnull(target.TransferredFrom, '') = isnull(source.TransferredFrom, '')
		and isnull(target.TransferredFromOther, '') = isnull(source.TransferredFromOther, '')
		and isnull(target.ClinicalDiagnosis, '') = isnull(source.ClinicalDiagnosis, '')
		and isnull(target.AdmissionForTerminalCare, 0) = isnull(source.AdmissionForTerminalCare, 0)
		and isnull(target.EndOfLifeCarePlan, 0) = isnull(source.EndOfLifeCarePlan, 0)
		and isnull(target.AdmittedToICU, 0) = isnull(source.AdmittedToICU, 0)
		and isnull(target.ICUSource, '') = isnull(source.ICUSource, '')
		and isnull(target.ICUWardEWS, 0) = isnull(source.ICUWardEWS, 0)
		and isnull(target.ICUWardComments, '') = isnull(source.ICUWardComments, '')
		and isnull(target.TheatrePlanned, 0) = isnull(source.TheatrePlanned, 0)
		and isnull(target.TheatrePlannedComment, '') = isnull(source.TheatrePlannedComment, '')
		and isnull(target.AEComments, '') = isnull(source.AEComments, '')
		and isnull(target.DrugErrorIdentified, 0) = isnull(source.DrugErrorIdentified, 0)
		and isnull(target.DrugErrorIdentifiedComments, '') = isnull(source.DrugErrorIdentifiedComments, '')
		and isnull(target.MRSA, 0) = isnull(source.MRSA, 0)
		and isnull(target.CDifficile, 0) = isnull(source.CDifficile, 0)
		and isnull(target.CriticalIncidentBeforeMortalityReview, 0) = isnull(source.CriticalIncidentBeforeMortalityReview, 0)
		and isnull(target.CriticalIncidentBeforeMortalityReviewComments, '') = isnull(source.CriticalIncidentBeforeMortalityReviewComments, '')
		and isnull(target.WithdrawalOfCare, 0) = isnull(source.WithdrawalOfCare, 0)
		and isnull(target.Indication, '') = isnull(source.Indication, '')
		and isnull(target.DocumentationType, '') = isnull(source.DocumentationType, '')
		and isnull(target.WithdrawlOfCareByWhom, '') = isnull(source.WithdrawlOfCareByWhom, '')
		and isnull(target.LeadConsultant, '') = isnull(source.LeadConsultant, '')
		and isnull(target.LeadConsultantEasilyIdentified, 0) = isnull(source.LeadConsultantEasilyIdentified, 0)
		and isnull(target.OtherConsultantInvolved, '') = isnull(source.OtherConsultantInvolved, '')
		and isnull(target.GradeOfMostSeniorClinicianInvolved, '') = isnull(source.GradeOfMostSeniorClinicianInvolved, '')
		and isnull(target.PreviousAdmission, '') = isnull(source.PreviousAdmission, '')
		and isnull(target.ChronicIllness, '') = isnull(source.ChronicIllness, '')
		and isnull(target.ChronicIllnessSpecify, '') = isnull(source.ChronicIllnessSpecify, '')
		and isnull(target.RelevantPreviousAdmissionsAttendances, '') = isnull(source.RelevantPreviousAdmissionsAttendances, '')
		and isnull(target.OperationsProceduresPerformed, '') = isnull(source.OperationsProceduresPerformed, '')
		and isnull(target.CaseReferredToCoroner, 0) = isnull(source.CaseReferredToCoroner, 0)
		and isnull(target.ConsultantReferringCase, '') = isnull(source.ConsultantReferringCase, '')
		and isnull(target.CaseDiscussedWithCoroner, 0) = isnull(source.CaseDiscussedWithCoroner, 0)
		and isnull(target.CaseDiscussedWithCoronerConsultant, '') = isnull(source.CaseDiscussedWithCoronerConsultant, '')
		and isnull(target.CaseDiscussedWithCoronerDocumentedInMedicalRecords, 0) = isnull(source.CaseDiscussedWithCoronerDocumentedInMedicalRecords, 0)
		and isnull(target.CaseDiscussedWithCoronerPostMortemPerformed, 0) = isnull(source.CaseDiscussedWithCoronerPostMortemPerformed, 0)
		and isnull(target.CaseDiscussedWithCoronerOutcomeAppropriate, 0) = isnull(source.CaseDiscussedWithCoronerOutcomeAppropriate, 0)
		and isnull(target.CaseDiscussedWithCoronerOutcomeNotAppropriateComments, '') = isnull(source.CaseDiscussedWithCoronerOutcomeNotAppropriateComments, '')
		and isnull(target.DeathCertificateIssued, 0) = isnull(source.DeathCertificateIssued, 0)
		and isnull(target.RequestForHospitalPostMortem, 0) = isnull(source.RequestForHospitalPostMortem, 0)
		and isnull(target.RequestForHospitalPostMortemMadeToParentsByWhom, '') = isnull(source.RequestForHospitalPostMortemMadeToParentsByWhom, '')
		and isnull(target.RequestForHospitalPostMortemMadeToParentsByOther, '') = isnull(source.RequestForHospitalPostMortemMadeToParentsByOther, '')
		and isnull(target.PermissionForHospitalPostMortemGiven, 0) = isnull(source.PermissionForHospitalPostMortemGiven, 0)
		and isnull(target.FullPermissionForHospitalPostMortemGiven, 0) = isnull(source.FullPermissionForHospitalPostMortemGiven, 0)
		and isnull(target.LimitedPermissionForHospitalPostMortemGiven, 0) = isnull(source.LimitedPermissionForHospitalPostMortemGiven, 0)
		and isnull(target.CauseOfDeath1a, '') = isnull(source.CauseOfDeath1a, '')
		and isnull(target.CauseOfDeath1b, '') = isnull(source.CauseOfDeath1b, '')
		and isnull(target.CauseOfDeath1c, '') = isnull(source.CauseOfDeath1c, '')
		and isnull(target.CauseOfDeath2, '') = isnull(source.CauseOfDeath2, '')
		and isnull(target.CauseOfDeathAgreed, 0) = isnull(source.CauseOfDeathAgreed, 0)
		and isnull(target.CauseOfDeathNotAgreedReason, '') = isnull(source.CauseOfDeathNotAgreedReason, '')
		and isnull(target.AnyFurtherPointsOfDiscussion, 0) = isnull(source.AnyFurtherPointsOfDiscussion, 0)
		and isnull(target.SignificantQuestionsWhichRemainUnanswered, '') = isnull(source.SignificantQuestionsWhichRemainUnanswered, '')
		and isnull(target.IssuesForDiscussion, '') = isnull(source.IssuesForDiscussion, '')
		and isnull(target.OrganDonationDiscussed, 0) = isnull(source.OrganDonationDiscussed, 0)
		and isnull(target.TissueDonationDiscussed, 0) = isnull(source.TissueDonationDiscussed, 0)
		and isnull(target.OrgansDonated, 0) = isnull(source.OrgansDonated, 0)
		and isnull(target.TissuesDonated, 0) = isnull(source.TissuesDonated, 0)
		and isnull(target.SecondaryReviewReason, '') = isnull(source.SecondaryReviewReason, '')
		and isnull(target.SecondaryReviewOutcome, '') = isnull(source.SecondaryReviewOutcome, '')
		and isnull(target.SecondaryReviewActions, '') = isnull(source.SecondaryReviewActions, '')
		and isnull(target.SecondaryReviewPatientCareConsultant, '') = isnull(source.SecondaryReviewPatientCareConsultant, '')
		and isnull(target.SecondaryReviewDiscussedConsultants, '') = isnull(source.SecondaryReviewDiscussedConsultants, '')
		and isnull(target.SecondaryReviewer, '') = isnull(source.SecondaryReviewer, '')
		and isnull(target.SecondaryReviewerDesignation, '') = isnull(source.SecondaryReviewerDesignation, '')
		and isnull(target.SecondaryReviewFuturePresentation, 0) = isnull(source.SecondaryReviewFuturePresentation, 0)
		and isnull(target.SecondaryReviewFuturePresentationPersonNominated, '') = isnull(source.SecondaryReviewFuturePresentationPersonNominated, '')
		and isnull(target.FirstReviewerUser, '') = isnull(source.FirstReviewerUser, '')
		and isnull(target.SecondReviewerUser, '') = isnull(source.SecondReviewerUser, '')
		and isnull(target.SecondaryReviewRequired, '') = isnull(source.SecondaryReviewRequired, '')
		and isnull(target.PersonNominatedForSecondReview, '') = isnull(source.PersonNominatedForSecondReview, '')
		and isnull(target.DiagDiseaseGroup, '') = isnull(source.DiagDiseaseGroup, '')
		and isnull(target.RecurringThemes, '') = isnull(source.RecurringThemes, '')
		and isnull(target.CodingCategory, '') = isnull(source.CodingCategory, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			  target.SourceUniqueID = source.SourceUniqueID
			,target.FormTypeID = source.FormTypeID
			,target.ReviewStatus = source.ReviewStatus
			,target.ReviewedDate = source.ReviewedDate
			,target.ReviewedBy = source.ReviewedBy
			,target.Designation = source.Designation
			,target.Comments = source.Comments
			,target.CasenoteNo = source.CasenoteNo
			,target.DateOfDeath = source.DateOfDeath
			,target.AdmissionTime = source.AdmissionTime
			,target.ClinicalSummary = source.ClinicalSummary
			,target.PlaceOfDeath = source.PlaceOfDeath
			,target.AgeAtDeath = source.AgeAtDeath
			,target.TransferredFromOtherHospital = source.TransferredFromOtherHospital
			,target.TransferredFrom = source.TransferredFrom
			,target.TransferredFromOther = source.TransferredFromOther
			,target.ClinicalDiagnosis = source.ClinicalDiagnosis
			,target.AdmissionForTerminalCare = source.AdmissionForTerminalCare
			,target.EndOfLifeCarePlan = source.EndOfLifeCarePlan
			,target.AdmittedToICU = source.AdmittedToICU
			,target.ICUSource = source.ICUSource
			,target.ICUWardEWS = source.ICUWardEWS
			,target.ICUWardComments = source.ICUWardComments
			,target.TheatrePlanned = source.TheatrePlanned
			,target.TheatrePlannedComment = source.TheatrePlannedComment
			,target.AEComments = source.AEComments
			,target.DrugErrorIdentified = source.DrugErrorIdentified
			,target.DrugErrorIdentifiedComments = source.DrugErrorIdentifiedComments
			,target.MRSA = source.MRSA
			,target.CDifficile = source.CDifficile
			,target.CriticalIncidentBeforeMortalityReview = source.CriticalIncidentBeforeMortalityReview
			,target.CriticalIncidentBeforeMortalityReviewComments = source.CriticalIncidentBeforeMortalityReviewComments
			,target.WithdrawalOfCare = source.WithdrawalOfCare
			,target.Indication = source.Indication
			,target.DocumentationType = source.DocumentationType
			,target.WithdrawlOfCareByWhom = source.WithdrawlOfCareByWhom
			,target.LeadConsultant = source.LeadConsultant
			,target.LeadConsultantEasilyIdentified = source.LeadConsultantEasilyIdentified
			,target.OtherConsultantInvolved = source.OtherConsultantInvolved
			,target.GradeOfMostSeniorClinicianInvolved = source.GradeOfMostSeniorClinicianInvolved
			,target.PreviousAdmission = source.PreviousAdmission
			,target.ChronicIllness = source.ChronicIllness
			,target.ChronicIllnessSpecify = source.ChronicIllnessSpecify
			,target.RelevantPreviousAdmissionsAttendances = source.RelevantPreviousAdmissionsAttendances
			,target.OperationsProceduresPerformed = source.OperationsProceduresPerformed
			,target.CaseReferredToCoroner = source.CaseReferredToCoroner
			,target.ConsultantReferringCase = source.ConsultantReferringCase
			,target.CaseDiscussedWithCoroner = source.CaseDiscussedWithCoroner
			,target.CaseDiscussedWithCoronerConsultant = source.CaseDiscussedWithCoronerConsultant
			,target.CaseDiscussedWithCoronerDocumentedInMedicalRecords = source.CaseDiscussedWithCoronerDocumentedInMedicalRecords
			,target.CaseDiscussedWithCoronerPostMortemPerformed = source.CaseDiscussedWithCoronerPostMortemPerformed
			,target.CaseDiscussedWithCoronerOutcomeAppropriate = source.CaseDiscussedWithCoronerOutcomeAppropriate
			,target.CaseDiscussedWithCoronerOutcomeNotAppropriateComments = source.CaseDiscussedWithCoronerOutcomeNotAppropriateComments
			,target.DeathCertificateIssued = source.DeathCertificateIssued
			,target.RequestForHospitalPostMortem = source.RequestForHospitalPostMortem
			,target.RequestForHospitalPostMortemMadeToParentsByWhom = source.RequestForHospitalPostMortemMadeToParentsByWhom
			,target.RequestForHospitalPostMortemMadeToParentsByOther = source.RequestForHospitalPostMortemMadeToParentsByOther
			,target.PermissionForHospitalPostMortemGiven = source.PermissionForHospitalPostMortemGiven
			,target.FullPermissionForHospitalPostMortemGiven = source.FullPermissionForHospitalPostMortemGiven
			,target.LimitedPermissionForHospitalPostMortemGiven = source.LimitedPermissionForHospitalPostMortemGiven
			,target.CauseOfDeath1a = source.CauseOfDeath1a
			,target.CauseOfDeath1b = source.CauseOfDeath1b
			,target.CauseOfDeath1c = source.CauseOfDeath1c
			,target.CauseOfDeath2 = source.CauseOfDeath2
			,target.CauseOfDeathAgreed = source.CauseOfDeathAgreed
			,target.CauseOfDeathNotAgreedReason = source.CauseOfDeathNotAgreedReason
			,target.AnyFurtherPointsOfDiscussion = source.AnyFurtherPointsOfDiscussion
			,target.SignificantQuestionsWhichRemainUnanswered = source.SignificantQuestionsWhichRemainUnanswered
			,target.IssuesForDiscussion = source.IssuesForDiscussion
			,target.OrganDonationDiscussed = source.OrganDonationDiscussed
			,target.TissueDonationDiscussed = source.TissueDonationDiscussed
			,target.OrgansDonated = source.OrgansDonated
			,target.TissuesDonated = source.TissuesDonated
			,target.SecondaryReviewReason = source.SecondaryReviewReason
			,target.SecondaryReviewOutcome = source.SecondaryReviewOutcome
			,target.SecondaryReviewActions = source.SecondaryReviewActions
			,target.SecondaryReviewPatientCareConsultant = source.SecondaryReviewPatientCareConsultant
			,target.SecondaryReviewDiscussedConsultants = source.SecondaryReviewDiscussedConsultants
			,target.SecondaryReviewer = source.SecondaryReviewer
			,target.SecondaryReviewerDesignation = source.SecondaryReviewerDesignation
			,target.SecondaryReviewFuturePresentation = source.SecondaryReviewFuturePresentation
			,target.SecondaryReviewFuturePresentationPersonNominated = source.SecondaryReviewFuturePresentationPersonNominated
			,target.FirstReviewerUser = source.FirstReviewerUser
			,target.SecondReviewerUser = source.SecondReviewerUser
			,target.SecondaryReviewRequired = source.SecondaryReviewRequired
			,target.PersonNominatedForSecondReview = source.PersonNominatedForSecondReview
			,target.DiagDiseaseGroup = source.DiagDiseaseGroup
			,target.RecurringThemes = source.RecurringThemes
			,target.CodingCategory = source.CodingCategory
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
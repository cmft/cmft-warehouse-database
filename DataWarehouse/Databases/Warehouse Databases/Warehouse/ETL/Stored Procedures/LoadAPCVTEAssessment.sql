﻿
CREATE PROCEDURE ETL.LoadAPCVTEAssessment

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, AssessmentDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, AssessmentDate)) 
from
	ETL.TLoadAPCVTEAssessment

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.VTEAssessment target
using
	(
	select
		SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,NoAssessmentReasonID
		,AssessmentDate
		,AssessmentTime
		,AtRisk
		,PreventativeMedicationStartDate
		,PreventativeMedicationStartTime
		,PreventativeNursingActionStartDate
		,PreventativeNursingActionStartTime
		,WardCode
		,InterfaceCode
	from
		ETL.TLoadAPCVTEAssessment
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	when not matched by source
	and	target.AssessmentDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,NoAssessmentReasonID
			,AssessmentDate
			,AssessmentTime
			,AtRisk
			,PreventativeMedicationStartDate
			,PreventativeMedicationStartTime
			,PreventativeNursingActionStartDate
			,PreventativeNursingActionStartTime
			,WardCode
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.NoAssessmentReasonID
			,source.AssessmentDate
			,source.AssessmentTime
			,source.AtRisk
			,source.PreventativeMedicationStartDate
			,source.PreventativeMedicationStartTime
			,source.PreventativeNursingActionStartDate
			,source.PreventativeNursingActionStartTime
			,source.WardCode
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.NoAssessmentReasonID, '') = isnull(source.NoAssessmentReasonID, '')
		and isnull(target.AssessmentDate, getdate()) = isnull(source.AssessmentDate, getdate())
		and isnull(target.AssessmentTime, getdate()) = isnull(source.AssessmentTime, getdate())
		and isnull(target.AtRisk, 0) = isnull(source.AtRisk, 0)
		and isnull(target.PreventativeMedicationStartDate, getdate()) = isnull(source.PreventativeMedicationStartDate, getdate())
		and isnull(target.PreventativeMedicationStartTime, getdate()) = isnull(source.PreventativeMedicationStartTime, getdate())
		and isnull(target.PreventativeNursingActionStartDate, getdate()) = isnull(source.PreventativeNursingActionStartDate, getdate())
		and isnull(target.PreventativeNursingActionStartTime, getdate()) = isnull(source.PreventativeNursingActionStartTime, getdate())
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')


		)
	then
		update
		set
			target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.AssessmentDate = source.AssessmentDate
			,target.AssessmentTime = source.AssessmentTime
			,target.AtRisk = source.AtRisk
			,target.PreventativeMedicationStartDate = source.PreventativeMedicationStartDate
			,target.PreventativeMedicationStartTime = source.PreventativeMedicationStartTime
			,target.PreventativeNursingActionStartDate = source.PreventativeNursingActionStartDate
			,target.PreventativeNursingActionStartTime = source.PreventativeNursingActionStartTime
			,target.WardCode = source.WardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
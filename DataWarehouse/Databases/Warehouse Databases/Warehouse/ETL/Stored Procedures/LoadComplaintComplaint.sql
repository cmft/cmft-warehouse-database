﻿CREATE procedure [ETL].[LoadComplaintComplaint] 

	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null

as

set dateformat dmy

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime = getdate()
declare @Elapsed int

declare
	 @deleted int
	,@inserted int
	,@updated int

declare @Stats varchar(255)


-- Update status to 0
update Utility.Parameter set NumericValue = 0 where Parameter = 'EXTRACTSTATUSULYSSESCOMPLAINT'
if(@@rowcount = 0)
insert into Utility.Parameter(Parameter, NumericValue) values('EXTRACTSTATUSULYSSESCOMPLAINT',1)


select @fromDate = (select min(ReceiptDate)from ETL.TLoadComplaintComplaint)
select @toDate = (select max(ReceiptDate)from ETL.TLoadComplaintComplaint)

-- No apparent key field on SubComplaint table so cannot merge the normal way 
-- delete records in in the target table where the receipt date falls within the date range used for extraction

delete from Complaint.Complaint
where
	ReceiptDate between @fromDate and @toDate

set @deleted = @@rowcount

insert into Complaint.Complaint
(
	SourceComplaintCode
	,ComplaintCode
	,ClaimantCode
	,ClaimTypeCode
	,BehalfOfCode
	,BehalfOfTypeCode
	,CaseNumber
	,ReceivedFromCode
	,ReceiptDate
	,AcknowledgementDate
	,AcknowledgementMethodCode
	,ComplaintDetail
	,ResolutionDate
	,OutcomeDetail
	,Satisfied
	,ResponseDate
	,DelayReasonCode
	,SeverityCode
	,ResponseDueDate
	,LetterDate
	,GradeCode
	,LikelihoodCode
	,RiskRatingCode
	,RiskScore
	,HandlerCode
	,StatusTypeCode
	,CaseTypeCode
	,ConsentRequired
	,ConsentDate
	,EventDate
	,InitialContactDate
	,InitialDueDate
	,InitialSeverityCode
	,InitalScore
	,SourceTargetDate
	,SequenceNo
	,CategoryCode
	,CategoryTypeCode
	,PrimaryOrSecondary
	,OrganisationCode
	,SiteCode
	,SiteTypeCode
	,DivisionCode
	,DirectorateCode
	,DepartmentCode
	,WardCode
	,ServiceCode
	,ProfessionCode
	,Reopened
	,ReopenedDetail
	,ConsentTargetDate
	,TargetDate7Day
	,TargetDate14Day
	,TargetDate25Day
	,TargetDate40Day
	,Duration
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
)

select
	SourceComplaintCode
	,ComplaintCode
	,ClaimantCode
	,ClaimTypeCode
	,BehalfOfCode
	,BehalfOfTypeCode
	,CaseNumber
	,ReceivedFromCode
	,ReceiptDate
	,AcknowledgementDate
	,AcknowledgementMethodCode
	,ComplaintDetail
	,ResolutionDate
	,OutcomeDetail
	,Satisfied
	,ResponseDate
	,DelayReasonCode
	,SeverityCode
	,ResponseDueDate
	,LetterDate
	,GradeCode
	,LikelihoodCode
	,RiskRatingCode
	,RiskScore
	,HandlerCode
	,StatusTypeCode
	,CaseTypeCode
	,ConsentRequired
	,ConsentDate
	,EventDate
	,InitialContactDate
	,InitialDueDate
	,InitialSeverityCode
	,InitalScore
	,SourceTargetDate
	,SequenceNo
	,CategoryCode
	,CategoryTypeCode
	,PrimaryOrSecondary
	,OrganisationCode
	,SiteCode
	,SiteTypeCode
	,DivisionCode
	,DirectorateCode
	,DepartmentCode
	,WardCode
	,ServiceCode
	,ProfessionCode
	,Reopened
	,ReopenedDetail
	,ConsentTargetDate
	,TargetDate7Day
	,TargetDate14Day
	,TargetDate25Day
	,TargetDate40Day
	,Duration
	,InterfaceCode
	,getdate()
	,getdate()
	,suser_name()
from
	ETL.TLoadComplaintComplaint

option (maxrecursion 0)

set @inserted = @@rowcount

set @updated = 0

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
		', Period from ' + CONVERT(varchar(11), @fromDate) + ' to ' + CONVERT(varchar(11), @toDate)


exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats

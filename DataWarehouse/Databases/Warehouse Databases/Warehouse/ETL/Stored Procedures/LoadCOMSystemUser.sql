﻿CREATE procedure ETL.[LoadCOMSystemUser] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()


truncate table COM.SystemUser

insert
into
	COM.SystemUser
(
	 SystemUserID
	,SystemUserRefno
	,SystemUserCode
	,SystemUser
	,Department
	,Email
	,ArchiveCode
) 
SELECT
	 SystemUserID
	,SystemUserRefno
	,SystemUserCode
	,SystemUser
	,Department
	,Email
	,ArchiveCode
FROM
	ETL.TLoadCOMSystemUser


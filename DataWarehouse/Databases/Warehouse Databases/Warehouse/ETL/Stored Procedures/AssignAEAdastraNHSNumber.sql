﻿
create proc ETL.[AssignAEAdastraNHSNumber]

as

with PCECNHSNumberMatch 
(
EncounterRecno
,NHSNumber
)

as 

	(

	select 
		 Adastra.EncounterRecno
		,Patient.NHSNumber
	from 
	(
		select
			 EncounterRecno
			,Postcode
			,DateOfBirth
			,PatientSurname
			,PatientForename
			,SexCode = 
				Case
					when SexCode ='M' then '01'
					when SexCode = 'F' then '02'
				end
		from 
			AE.Encounter
		where
			InterfaceCode = 'Adas'
		) Adastra
	inner join PAS.Patient
	on
		(
				Patient.DateOfBirth = Adastra.DateOfBirth
			and Patient.Surname = Adastra.PatientSurname
			and Patient.SexCode = Adastra.SexCode
			and left(Patient.Forenames,1) = left(Adastra.PatientForename,1)
		)
	where
		NHSNumber is not null

	union

	select 
		 Adastra.EncounterRecno
		,Patient.NHSNumber
	from 
	(
		select
			 EncounterRecno
			,Postcode
			,DateOfBirth
			,PatientSurname
			,PatientForename
			,SexCode = 
				Case
					when SexCode ='M' then '01'
					when SexCode = 'F' then '02'
				end
		from 
			AE.Encounter
		where
			InterfaceCode = 'Adas'
		--and arrivaldate between '1 apr 2011' and '30 Apr 2012' --@from and @to
		) Adastra
	inner join PAS.Patient
	on
		(
				Patient.Postcode = Adastra.Postcode
			and Patient.Surname = Adastra.PatientSurname
			and Patient.SexCode = Adastra.SexCode				
			and left(Patient.Forenames,1) = left(Adastra.PatientForename,1)
		)
	where
		NHSNumber is not null

	union 

	select 
		 Adastra.EncounterRecno
		,Patient.NHSNumber
	from 
	(
		select
			 EncounterRecno
			,Postcode
			,DateOfBirth
			,PatientSurname
			,PatientForename
			,SexCode = 
				Case
					when SexCode ='M' then '01'
					when SexCode = 'F' then '02'
				end
		from 
			AE.Encounter
		where
			InterfaceCode = 'Adas'
		) Adastra
	inner join PAS.Patient
	on

		(
				Patient.Postcode = Adastra.Postcode
			and	Patient.DateOfBirth = Adastra.DateOfBirth
			and Patient.SexCode = Adastra.SexCode
			and left(Patient.Forenames,1) = left(Adastra.PatientForename,1)
		)
	
	where
		NHSNumber is not null

	union

	select 
		 Adastra.EncounterRecno
		,Patient.NHSNumber
	from 
	(
		select
			 EncounterRecno
			,Postcode
			,DateOfBirth
			,PatientSurname
			,PatientForename
			,SexCode = 
				Case
					when SexCode ='M' then '01'
					when SexCode = 'F' then '02'
				end
		from 
			AE.Encounter
		where
			InterfaceCode = 'Adas'

		) Adastra
	inner join PAS.Patient
	on
		
		(
				Patient.Postcode = Adastra.Postcode
			and Patient.Surname = Adastra.PatientSurname
			and	Patient.DateOfBirth = Adastra.DateOfBirth
			and left(Patient.Forenames,1) = left(Adastra.PatientForename,1)
		)

	where
		NHSNumber is not null

	union 

	select 
		 Adastra.EncounterRecno
		,Patient.NHSNumber
	from 
	(
		select
			 EncounterRecno
			,Postcode
			,DateOfBirth
			,PatientSurname
			,PatientForename
			,SexCode = 
				Case
					when SexCode ='M' then '01'
					when SexCode = 'F' then '02'
				end
		from 
			AE.Encounter
		where
			InterfaceCode = 'Adas'
	
		) Adastra
	inner join PAS.Patient
	on
		(
				Patient.Postcode = Adastra.Postcode
			and	Patient.DateOfBirth = Adastra.DateOfBirth
			and Patient.Surname = Adastra.PatientSurname
			and Patient.SexCode = Adastra.SexCode
		)

	where
		NHSNumber is not null
	)

--select
--	*
--from 
--	PCECNHSNumberMatch
--where
--	EncounterRecno in 
--						(
--						select
--							EncounterRecno
--						from
--							PCECNHSNumberMatch
--						group by
--							EncounterRecno
--						having
--							count (distinct NHSNumber) = 1
--						)

--select 
--	PCECNHSNumberMatch.EncounterRecno						
--from 
--	AE.Encounter Encounter
--inner join 
--		(
--		select
--			EncounterRecno
--			,NHSNumber
--		from 
--			PCECNHSNumberMatch
--		where
--			EncounterRecno in 
--								(
--								select
--									EncounterRecno
--								from
--									PCECNHSNumberMatch
--								group by
--									EncounterRecno
--								having
--									count(NHSNumber) = 1
--								) 
--		) PCECNHSNumberMatch

--		on PCECNHSNumberMatch.EncounterRecno = Encounter.EncounterRecno



----select
----	EncounterRecno
----from
----	PCECNHSNumberMatch
----group by
----	EncounterRecno
----having
----	count (NHSNumber) > 1


--select
--*
--from PCECNHSNumberMatch
--where EncounterRecno = 80909140


update Encounter
set NHSNumber = PCECNHSNumberMatch.NHSNumber
from AE.Encounter Encounter

inner join 
		(
		select
			EncounterRecno
			,NHSNumber
		from 
			PCECNHSNumberMatch
		where
			EncounterRecno in 
								(
								select
									EncounterRecno
								from
									PCECNHSNumberMatch
								group by
									EncounterRecno
								having
									count(NHSNumber) = 1
								) 
		) PCECNHSNumberMatch

		on PCECNHSNumberMatch.EncounterRecno = Encounter.EncounterRecno

--select NHSNumber
--from AE.Encounter Encounter
--where NHSNumber is not null
--and InterfaceCode = 'Adas'
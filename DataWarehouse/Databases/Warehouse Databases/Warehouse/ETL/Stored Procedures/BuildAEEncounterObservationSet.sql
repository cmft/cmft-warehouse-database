﻿

CREATE proc [ETL].[BuildAEEncounterObservationSet]

as


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

truncate table AE.EncounterObservationSet

insert into AE.EncounterObservationSet

select 
	Encounter.EncounterRecno
	,ObservationSetRecno
from
	Observation.ObservationSet

inner join	WH.DayLightSaving
on	ObservationSet.StartTime between DayLightSaving.StartTime and DayLightSaving.EndTime

inner join 	AE.Encounter 
on	ObservationSet.CasenoteNumber = left(
										AttendanceNumber
										,charindex('-', AttendanceNumber, charindex('-', AttendanceNumber, charindex('-', AttendanceNumber) + 1) + 1) - 1
										) -- AttendanceNumbers sent from PAS to Patientrack have the -1, -2, etc stripped out
and dateadd(hour, DayLightSaving.Offset, ObservationSet.StartTime) between Encounter.ArrivalTime and coalesce(Encounter.DepartureTime, getdate())

where
	Encounter.InterfaceCode = 'SYM' -- only attempt to link Patientrack records with Symphony records


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime



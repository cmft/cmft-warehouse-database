﻿--Use Warehouse

-- 20140929	RR	Create Date
--				Pulls Tranexamic Acid data from the Recall system.  

CREATE Procedure [ETL].[ExtractRecallBloodManagementTranexamicAcid]  --'20120101','20141130'
	@fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @RowsInserted = 0

select @from = coalesce(@fromDate, cast(cast(dateadd(month, -3, getdate())as date) as datetime))

select @to = coalesce(@toDate, cast(cast(dateadd(day, -1, getdate())as date)as datetime)+'23:59')


Truncate Table ETL.TImportBloodManagementTranexamicAcid
insert into ETL.TImportBloodManagementTranexamicAcid
	( 
	TranexamicAcidPrimaryKey 
	,EpisodeKey
	,HospitalNumber
	,LastName
	,FirstName
	,DateOfBirth
	,Gender
	,SpecialtyKey
	,ConsultantKey
	,IntendedDestination 
	,ActualDestination
	,DrugKey
	,DrugDescription
	,Category
	,Dose
	,UnitsKey
	,UnitsDescription
	,DrugAdministeredDate
	,DrugAdministeredTime 
	,SessionTypeDescription 
	,SessionLocationKey 
	,InterfaceCode 
	)
select 
	TranexamicAcidPrimaryKey = EpisodeDrug.PrimaryKey
	,EpisodeDrug.EpisodeKey
	,EpisodePatientDetails.HospitalNumber
	,EpisodePatientDetails.LastName
	,EpisodePatientDetails.FirstName
	,EpisodePatientDetails.DateOfBirth
	,Gender = 
		case 
			when left (EpisodePatientDetails.SexDescription,1)='F' then 2
			when left (EpisodePatientDetails.SexDescription,1)='M' then 1
			when left (EpisodePatientDetails.SexDescription,1)='U' then 9
			else 0
		end
	,EpisodeSpecialty.SpecialtyKey
	,EpisodeAdmission.ConsultantKey
	,IntendedDestination = EpisodeAdmission.IntendedDestLocationDesc
	,ActualDestination = EpisodeAdmission.ActualDestLocationDesc
	,EpisodeDrug.DrugKey
	,EpisodeDrug.DrugDescription
	,EpisodeDrug.CategoryDescription
	,EpisodeDrug.Dose
	,EpisodeDrug.UnitsKey
	,EpisodeDrug.UnitsDescription
	,DrugAdministeredDate = cast(EpisodeDrug.StartTime as date)
	,DrugAdministeredTime = EpisodeDrug.StartTime
	,SessionTypeDescription = Session.SessionTypeDescription
	,SessionLocationKey = Session.LocationKey
	,InterfaceCode = 'RECALL' 
from
	[$(RecallReports)].dbo.EpisodeDrug

inner join [$(RecallReports)].dbo.EpisodePatientDetails
on EpisodeDrug.EpisodeKey = EpisodePatientDetails.EpisodeKey

left join [$(RecallReports)].dbo.EpisodeAdmission
on EpisodeAdmission.EpisodeKey = EpisodeDrug.EpisodeKey

left join [$(RecallReports)].dbo.EpisodeSpecialty 
on EpisodeDrug.EpisodeKey = EpisodeSpecialty.EpisodeKey
and SpecialtyKey <> 0 
and not exists
	(select
		1
	from 
		[$(RecallReports)].dbo.EpisodeSpecialty Latest
	where 
		Latest.EpisodeKey = EpisodeSpecialty.EpisodeKey
	and SpecialtyKey <> 0 
	and Latest.PrimaryKey > EpisodeSpecialty.PrimaryKey
	)

left join [$(RecallReports)].dbo.EpisodeSchedule
on EpisodeDrug.EpisodeKey = EpisodeSchedule.EpisodeKey
and not exists
	(Select
		1
	from 
		[$(RecallReports)].dbo.EpisodeSchedule Latest
	where
		Latest.EpisodeKey = EpisodeSchedule.EpisodeKey
	and Latest.PrimaryKey > EpisodeSchedule.PrimaryKey
	)

left join [$(RecallReports)].dbo.SessionListLink
on EpisodeSchedule.ListKey = SessionListLink.ListKey

left join [$(RecallReports)].dbo.[Session]
on SessionListLink.SessionKey = [Session].SessionKey

where 
	EpisodeDrug.DrugKey = 2100006190	 --TRANEXAMIC ACID
and EpisodeDrug.StartTime between @from and @to 
and not exists
	(select
		1
	from
		[$(RecallReports)].dbo.EpisodePatientDetails TestPatients
	where
		TestPatients.PrimaryKey = EpisodePatientDetails.PrimaryKey
	and left(TestPatients.LastName,4) = 'Test'
	and (
		left(FirstName,4) = 'Test'
		or
		FirstName is null
		)	
	)

--1687

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractRecallTranexamicAcid', @Stats, @StartTime




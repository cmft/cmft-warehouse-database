﻿CREATE PROCEDURE [ETL].[LoadAPCCriticalCarePAS]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)


select
	@StartTime = getdate()


delete from APC.CriticalCarePeriod
where
	StartDate between 
	(
	select
		min(convert(smalldatetime, StartDate))
	from
			ETL.TLoadAPCCriticalCarePAS
	where InterfaceCode = 'INQ'
	)
and
	(
	select
		max(convert(smalldatetime,StartDate))
	from
			ETL.TLoadAPCCriticalCarePAS
	where InterfaceCode = 'INQ'
	)


SELECT @RowsDeleted = @@Rowcount


INSERT INTO APC.CriticalCarePeriod
	(
	 SourceUniqueID
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,CreatedByUser
	,CreatedByTime
	,DermatologicalSupportDays
	,EndDate
	,EndTime
	,LiverSupportDays
	,LocalIdentifier
	,LocationCode
	,NeurologicalSupportDays
	,RenalSupportDays
	,StartDate
	,StartTime
	,StatusCode
	,TreatmentFunctionCode
	,SourceSpellNo
	,SourcePatientNo
	,PlannedAcpPeriod
	,Created
	,ByWhom
	,InterfaceCode
	,CasenoteNumber
	,WardCode
	,AdmissionDate
	) 
select
	 SourceUniqueID
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,CreatedByUser
	,CreatedByTime
	,DermatologicalSupportDays
	,EndDate
	,EndTime
	,LiverSupportDays
	,LocalIdentifier
	,LocationCode
	,NeurologicalSupportDays
	,RenalSupportDays
	,StartDate
	,StartTime
	,StatusCode
	,TreatmentFunctionCode
	,SourceSpellNo
	,SourcePatientNo
	,PlannedAcpPeriod
	,Created = getdate()
	,ByWhom = system_user
	,InterfaceCode
	,CasenoteNumber
	,WardCode
	,AdmissionDate
from
	ETL.TLoadAPCCriticalCarePAS TEncounter

SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadAPCCriticalCarePAS', @Stats, @StartTime

print @Stats

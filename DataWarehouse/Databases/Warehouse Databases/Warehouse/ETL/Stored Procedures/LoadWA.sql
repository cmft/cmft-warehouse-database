﻿CREATE       procedure [ETL].[LoadWA] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select @from = 
	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from Utility.Parameter where Parameter = 'WAENCOUNTERFREEZEDATE')
		,dateadd(month, -12, @to)
	)

truncate table ETL.TImportWAEncounter
exec ETL.ExtractInquireWA @from, @to
exec ETL.LoadWAEncounter

--not used at CMFT
--truncate table TImportWAOperation
--exec ExtractInquireWAOperation @from, @to
--exec LoadWAOperation


update Utility.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADWADATE'

if @@rowcount = 0

insert into Utility.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADWADATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec Utility.WriteAuditLogEvent 'LoadWA', @Stats, @StartTime

﻿
CREATE PROCEDURE [ETL].[LoadAPCPressureUlcer]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, IdentifiedDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, IdentifiedDate)) 
from
	ETL.TLoadAPCPressureUlcer

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.PressureUlcer target
using
	(
	select
		SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,LocationID
		,IdentifiedDate
		,IdentifiedTime
		,HealedDate
		,HealedTime
		,CategoryID
		,Validated
		,OccuredOnAnotherWard
		,WardCode
		,InterfaceCode
	from
		ETL.TLoadAPCPressureUlcer
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	when not matched by source
	and	target.IdentifiedDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,LocationID
			,IdentifiedDate
			,IdentifiedTime
			,HealedDate
			,HealedTime
			,CategoryID
			,Validated
			,OccuredOnAnotherWard
			,WardCode
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.LocationID
			,source.IdentifiedDate
			,source.IdentifiedTime
			,source.HealedDate
			,source.HealedTime
			,source.CategoryID
			,source.Validated
			,source.OccuredOnAnotherWard
			,source.WardCode
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.LocationID, 0) = isnull(source.LocationID, 0)
		and isnull(target.IdentifiedDate, getdate()) = isnull(source.IdentifiedDate, getdate())
		and isnull(target.IdentifiedTime, getdate()) = isnull(source.IdentifiedTime, getdate())
		and isnull(target.HealedDate, getdate()) = isnull(source.HealedDate, getdate())
		and isnull(target.HealedTime, getdate()) = isnull(source.HealedTime, getdate())
		and isnull(target.CategoryID, 0) = isnull(source.CategoryID, 0)
		and isnull(target.Validated, 0) = isnull(source.Validated, 0)
		and isnull(target.OccuredOnAnotherWard, 0) = isnull(source.OccuredOnAnotherWard, 0)
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.LocationID = source.LocationID
			,target.IdentifiedDate = source.IdentifiedDate
			,target.IdentifiedTime = source.IdentifiedTime
			,target.HealedDate = source.HealedDate
			,target.HealedTime = source.HealedTime	
			,target.CategoryID = source.CategoryID
			,target.Validated = source.Validated
			,target.OccuredOnAnotherWard = source.OccuredOnAnotherWard
			,target.WardCode = source.WardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
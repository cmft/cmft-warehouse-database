﻿

CREATE proc [ETL].[ExtractBadgerNeonatalBeddayDrug]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

declare @StartDate date = (select min(CareDate) from [$(Bnet_DSS_business)].bnf_dbsync.NNUDaySum)
declare @EndDate date = (select max(CareDate) from [$(Bnet_DSS_business)].bnf_dbsync.NNUDaySum);

--Create calendar table so can generate record for each care date

with Calendar 
(
TheDate
)

as 
	( 
	select 
		 TheDate = @StartDate

	union all 

	select 
		 dateadd(day, 1, TheDate) 
	from 
		 Calendar
	where 
		 dateadd(day, 1, TheDate) < @EndDate
	) 
	
insert into ETL.TImportNeonatalBeddayDrug
(
EpisodeID
,CareDate
,DrugCode
,ModifiedTime
)

select
	EpisodeID 
	,CareDate 
	,DrugCode 
	,ModifiedTime 
from
	(	
	select
		EpisodeID = EntityID
		,CareDate = cast(TheDate as date)
		,DrugCode = Code
		,ModifiedTime = NNUCodedItems.RecordTimestamp
	from
		[$(Bnet_DSS_business)].bnf_dbsync.NNUCodedItems
	inner join Calendar 
	on Calendar.TheDate >= NNUCodedItems.FirstDate 
	and Calendar.TheDate <= NNUCodedItems.LastDate
	where
		NNUCodedItems.CodeType = 'Drug'
	and	NNUCodedItems.EpisodePoint = 'DuringStay'
	and	NNUCodedItems.RecordTimestamp between @FromDate and @ToDate
	
	) NeontatalBeddayDrug


option (maxrecursion 0)

select
	@RowsInserted = @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime





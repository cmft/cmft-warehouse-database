﻿CREATE procedure [ETL].[ExtractInquireMaternityAnaesthetic]

as

insert into ETL.TImportPASMaternityAnaesthetic
select
	 *
from
	(
	select
		 ANANINITID
		,AnaesanalgDuring
		,Aneadurlab
		,Aneadurlabreas
		,Aneapostlab
		,Aneapostlabreas
		,left(EpisodeNumber, 12) EpisodeNumber
		,left(InternalPatientNumber, 12) InternalPatientNumber
		,KeMatPerAnaesAdminInt
	from
		[$(PAS)].Inquire.ANANINIT
) Anaes

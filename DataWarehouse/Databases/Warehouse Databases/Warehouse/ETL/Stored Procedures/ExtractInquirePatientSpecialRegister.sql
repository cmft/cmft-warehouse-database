﻿

CREATE proc [ETL].[ExtractInquirePatientSpecialRegister]
	 @FromDate date = null
	,@ToDate date = null
as

/****************************************************************************************
	Stored procedure : ETL.ExtractInquirePatientSpecialRegister
	Description		 : Get special register from Inquire into import table

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	14/08/2014	Paul Egan       Initial Coding
	19/10/2015	Rachel Royston	Updated
*****************************************************************************************/

/*========== Debug Only ==========*/
--declare @fromDate date = null
--declare @toDate date = null
/*================================*/

declare @StartTime datetime;
declare @Elapsed int;
declare @RowsInserted Int;
declare @Stats varchar(255);
declare @from date;
declare @to date;

declare @ProcedureName varchar(255);

select @StartTime = getdate();
select @RowsInserted = 0;

set @from = 
	coalesce(
		@FromDate
		,
		case
			when datename(dw,getdate()) = 'Saturday' 
			then dateadd(year,-3,getdate())
			else dateadd(day,-90,getdate())
		end
		)
set @to = coalesce(@ToDate,getdate())

select @ProcedureName = object_schema_name(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);


Truncate table ETL.TImportPASPatientSpecialRegister

Insert into ETL.TImportPASPatientSpecialRegister
	(
	SourcePatientNo
	,SpecialRegisterCode
	,DistrictNo
	,NHSNumber
	,EnteredDate
	,ActivityTime
	,SourceEpisodeNo
	)
select
	SourcePatientNo = PMISPECIALREG.InternalPatientNumber
	,SpecialRegisterCode = PMISPECIALREG.SpecialRegister
	,DistrictNo = PATDATA.DistrictNumber
	,NHSNumber = PATDATA.NHSNumber
	,EnteredDate = PMISPECIALREG.DateEntered
	,ActivityTime = PMISPECIALREG.ActivityDateTime
	,SourceEpisodeNo = PMISPECIALREG.EpisodeNumber
from
	[$(PAS)].Inquire.PMISPECIALREG
	
	inner join [$(PAS)].Inquire.PATDATA
	on PATDATA.InternalPatientNumber = PMISPECIALREG.InternalPatientNumber
where
	DateEntered between @from and @to

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT
;

select @Elapsed = datediff(minute,@StartTime,getdate());

select @Stats = 
	'Rows Inserted ' + convert(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + convert(varchar(6), @Elapsed) + ' Mins'
;

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;







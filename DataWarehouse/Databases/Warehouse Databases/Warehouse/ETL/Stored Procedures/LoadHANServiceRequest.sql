﻿

CREATE procedure [ETL].[LoadHANServiceRequest]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


set dateformat dmy

truncate table HAN.ServiceRequest

insert into HAN.ServiceRequest
(
	 SourceUniqueID
	,RaisedBy
	,ContactTelephone
	,ContactBleep
	,EmailAddress
	,StatusID
	,CallTypeID
	,SituationID
	,SituationNotes
	,SituationHistory
	,WorkGroupID
	,PersonID
	,ActionRequestID
	,UrgencyID
	,SpecialistTypeID
	,RaisedTime
	,RaisedDate
	,ClinicalRoleRequestedID
	,ClinicalRoleAllocatedID
	,ClinicalRoleAllocatedTime
	,ClinicalRoleAllocatedDate
	,OutcomeID
	,ClosedByClinicalRoleID
	,ClosedNotes
	,CasenoteNumber
	,NHSNumber
	,DistrictNo
	,InpatientEpisode
	,PatientName
	,DateOfBirth
	,SexCode
	,PASWardCode
	,BedManSourceUniqueID
)

select
	 SourceUniqueID = ID
	,RaisedBy
	,ContactTelephone
	,ContactBleep = replace(ContactBleep , '' , null)
	,EmailAddress = replace(EmailAddress , '' , null)
	,StatusID
	,CallTypeID
	,SituationID
	,SituationNotes = replace(SituationNotes , '' , null)
	,SituationHistory
	,WorkGroupID
	,PersonID
	,ActionRequestID
	,UrgencyID
	,SpecialistTypeID
	,RaisedTime = RaisedTimeStamp
	
	,RaisedDate = convert(date , RaisedTimeStamp)

	--,NurseHandOver

	,ClinicalRoleRequestedID =
			ServiceRequest.RoleRequested.value('(StaffRoles/ListRoles/Role/ID)[1]', 'INT')

	,ClinicalRoleAllocatedID =
		ServiceRequest.RoleAllocated.value('(StaffRoles/ListRoles/Role/ID)[1]', 'INT')

	,ClinicalRoleAllocatedTime =
		case
		when ServiceRequest.RoleAllocated.value('(StaffRoles/ListRoles/Role/AllocatedDate)[1]', 'DATETIME2') = '0001-01-01 00:00:00.0000000' then null
		else ServiceRequest.RoleAllocated.value('(StaffRoles/ListRoles/Role/AllocatedDate)[1]', 'DATETIME2')
		end

	,ClinicalRoleAllocatedDate =
		cast(
			case
			when ServiceRequest.RoleAllocated.value('(StaffRoles/ListRoles/Role/AllocatedDate)[1]', 'DATETIME2') = '0001-01-01 00:00:00.0000000' then null
			else ServiceRequest.RoleAllocated.value('(StaffRoles/ListRoles/Role/AllocatedDate)[1]', 'DATETIME2')
			end
			as date
		)

	,OutcomeID
	,ClosedByClinicalRoleID = ClosedByRoleID
	,ClosedNotes = replace(ClosedNotes , '' , null)

	--,PatientDetails
	,CasenoteNumber =
		replace(
			ServiceRequest.PatientDetails.value('(PatientInformation/CaseNote)[1]', 'VARCHAR(50)')
			,''
			,null
		)

	,NHSNumber =
		replace(
			ServiceRequest.PatientDetails.value('(PatientInformation/NHSNumber)[1]', 'VARCHAR(50)')
			,''
			,null
		)

	,DistrictNo =
		replace(
			 ServiceRequest.PatientDetails.value('(PatientInformation/DistrictNumber)[1]', 'VARCHAR(50)')
			,''
			,null
		)

	,InpatientEpisode =
		replace(
			 ServiceRequest.PatientDetails.value('(PatientInformation/IPEpisode)[1]', 'VARCHAR(50)')
			,''
			,null
		)

	,PatientName =
		ServiceRequest.PatientDetails.value('(PatientInformation/Name)[1]', 'VARCHAR(100)')

	,DateOfBirth =
		ServiceRequest.PatientDetails.value('(PatientInformation/DOB)[1]', 'DATETIME2')

	,SexCode =
		left(
			ServiceRequest.PatientDetails.value('(PatientInformation/Gender)[1]', 'VARCHAR(50)')
			,1
		)

	,PASWardCode =
		coalesce(
			replace(
				ServiceRequest.PatientDetails.value('(PatientInformation/PASCode)[1]', 'VARCHAR(50)')
				,''
				,null
			)
			,ServiceRequest.PASCode
		)

	,BedManSourceUniqueID =
		ServiceRequest.PatientDetails.value('(PatientInformation/SpellID)[1]', 'VARCHAR(50)')

	--,Recommendations --multi-value xml
from
	[$(HospitalAtNight)].dbo.ServiceRequest

select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		--', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		--', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


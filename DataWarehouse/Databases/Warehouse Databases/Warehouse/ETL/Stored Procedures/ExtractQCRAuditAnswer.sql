﻿CREATE PROCEDURE [ETL].[ExtractQCRAuditAnswer]

AS

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)


SELECT @StartTime = getdate();

SELECT @RowsInserted = 0;

TRUNCATE TABLE ETL.TImportQCRAuditAnswer;


INSERT INTO ETL.TImportQCRAuditAnswer
(
	 SourceUniqueID
	,AuditTime
	,LocationCode
	,AuditTypeCode
	,QuestionCode
	,Answer
)

--SELECT     
--	  SourceUniqueID = Answers.audit_answersID
--	 ,AuditTime = audit_head_answer.audit_date
--	 ,AgeBandCode = audit_head_answer.adult_childID 
--	 ,CategoryID = Head.categoryID
--	 ,WardID = audit_head_answer.WardID
--	 ,AuditID = QuestionsTemplate.AuditTypeID
--	 ,QuestionID = Answers.questionID
--	 ,Answer = 
--				CASE
--					WHEN Answers.answer = 'x' THEN NULL
--					ELSE Answers.answer
--				END
					
--FROM        
--	QualityOfCare.dbo.audit_answers Answers
--INNER JOIN QualityOfCare.dbo.audit_head_answer audit_head_answer
--	ON audit_head_answer.head_answerID = Answers.head_answerID 
--INNER JOIN QualityOfCare.dbo.questionsTemplate QuestionsTemplate
--	ON Answers.questionID = QuestionsTemplate.questionID
--INNER JOIN QualityOfCare.dbo.audit_head Head
--	ON audit_head_answer.AuditID = Head.audit_headID
SELECT  
        SourceUniqueID = Answers.[AuditAnswersID]
       ,AuditTime = audit_head_answer.[ActualAuditDate]
       ,LocationCode = audit_head_answer.WardID
       ,AuditCode = QuestionsTemplate.AuditTypeID
       ,QuestionCode = Answers.QuestionID
       ,Answer = 
                           CASE
                                  WHEN Answers.Answer = 'x' THEN NULL
                                  ELSE Answers.Answer
                           END

FROM        
       [$(QualityOfCare)].[dbo].[AuditAnswers] Answers
INNER JOIN [$(QualityOfCare)].[dbo].[AuditHeadAnswers] audit_head_answer
       ON audit_head_answer.[AuditHeadAnswersID] = Answers.[AuditHeadAnswersID] 
       
INNER JOIN [$(QualityOfCare)].dbo.QuestionsTemplate QuestionsTemplate
       ON Answers.QuestionID = QuestionsTemplate.QuestionID

	
SELECT
	@RowsInserted = @RowsInserted + @@ROWCOUNT;

SELECT	
	@Elapsed = DATEDIFF(minute,@StartTime,getdate());

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins';

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;

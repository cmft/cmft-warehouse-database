﻿




CREATE       procedure [ETL].[LoadAPC] 
	 @from smalldatetime = null
	,@to smalldatetime = null
as

/* 
==============================================================================================
Description:

When		Who			What
20150420	Paul Egan	Added exec APC.BuildEncounterAEEncounter to update AE/APC bridging table.
20150724	RR			Added LoadLearningDisability
===============================================================================================
*/




declare @censusDate smalldatetime
declare @continue int

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select @to = coalesce(@to, dateadd(day, datediff(day, 0, getdate()), 0))
select

 @from = 
 
	


	coalesce(
		 @from
		,(select dateadd(day, 1, DateValue) from dbo.Parameter where Parameter = 'APCENCOUNTERFREEZEDATE')
		,case 
		 when datename(dw, @to) = 'Sunday' 	then dateadd(month, -36, @to) -- final logic to be implemented
		-- when datename(dw, @to) = 'Thursday' 	then dateadd(month, -60, @to) -- temp line to intitially update 5 years of data
		 else dateadd(month, -12, @to)
		 end
	)

update Utility.Parameter
set NumericValue = 0
where
	Parameter in ('EXTRACTSTATUSAPCDIAGNOSIS' , 'EXTRACTSTATUSAPCENCOUNTER' , 'EXTRACTSTATUSAPCOPERATION')

truncate table ETL.TImportAPCEncounter
exec ETL.ExtractInquireAPC @from, @to

truncate table ETL.TImportAPCDiagnosis
exec ETL.ExtractInquireAPCDiagnosis @from, @to

truncate table ETL.TImportAPCOperation
exec ETL.ExtractInquireAPCOperation @from, @to

if (
	select sum(NumericValue) from Utility.Parameter
	where Parameter in ('EXTRACTSTATUSAPCDIAGNOSIS' , 'EXTRACTSTATUSAPCENCOUNTER' , 'EXTRACTSTATUSAPCOPERATION')
	) = 3
begin

	exec ETL.LoadAPCEncounter
	exec ETL.LoadAPCDiagnosis @from, @to
	exec ETL.LoadAPCOperation @from, @to

--link encounters to ORMIS procedure detail
	exec ETL.BuildAPCEncounterProcedureDetail

end

truncate table ETL.TImportAPCSuspension
exec ETL.ExtractInquireAPCSuspension @from
exec ETL.LoadAPCSuspension

truncate table ETL.TImportAPCWardStay
exec ETL.ExtractInquireAPCWardStay @from
exec ETL.LoadAPCWardStay

--GS20120420 to be reviewed as part of Critical Care Paed CDS requirements
--truncate table ETL.TImportAPCCriticalCarePAS
--exec ETL.ExtractInquireAPCCriticalCare @from, @to
--exec ETL.LoadAPCCriticalCarePAS

exec [ETL].[ExtractInquireAPCAugmentedCarePeriod]
exec [ETL].[LoadAPCAugmentedCarePeriod]


truncate table ETL.TImportAPCMidnightBedState
exec ETL.ExtractInquireAPCMidnightBedState @from, @to
exec ETL.LoadAPCMidnightBedState

exec ETL.LoadAPCLegalStatus

--work out derived attributes
--exec ETL.AssignAPCBreachDate

--turned off as redundant - 6 Jun 2013 - PDO
--exec ETL.AssignAPCEncounterDivisionCode
exec ETL.AssignAPCEncounterStartDirectorateCode
exec ETL.AssignAPCEncounterEndDirectorateCode

exec ETL.AssignAPCEncounterISTAttributes

--VTE is now evaluated using the Allocation System - turned off 2013-10-04 - CCB
--exec ETL.BuildAPCVTESpell

--Check for any additions in APC encounter and load into APC expected LOS table.  
--Also update details where appropriate
EXEC ETL.LoadAPCExpectedLOS
exec ETL.AssignAPCExpectedLOSDirectorateCode


--from bedman 

exec ETL.LoadBedmanReferenceData

exec ETL.LoadAPCFall
exec ETL.LoadAPCPressureUlcer
exec ETL.LoadAPCUrinaryTractInfection
exec ETL.LoadAPCVTEAssessment
exec ETL.LoadAPCVTECondition
exec ETL.LoadAPCLearningDisability

--observation
exec ETL.BuildAPCEncounterObservationSet
exec ETL.BuildAPCEncounterAlert

/* Added Paul Egan 20150420 Update APC/AE bridging table */
--exec APC.BuildEncounterAEEncounter -- This is a step in the job


update Utility.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADAPCDATE'

if @@rowcount = 0

insert into Utility.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADAPCDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec Utility.WriteAuditLogEvent 'LoadAPC', @Stats, @StartTime





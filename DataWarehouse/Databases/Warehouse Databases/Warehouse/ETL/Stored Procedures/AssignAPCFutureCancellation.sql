﻿

CREATE     procedure [ETL].[AssignAPCFutureCancellation] as

/*
When		Who			What
===================================================================================================
?			?			Initial Coding
20150316	Paul Egan	Added FutureCancellationTime for day case and inpatient admission reminders
===================================================================================================
*/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsProcessed int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()

-- hospital cancellations
select
	EpisodeNumber,
	InternalPatientNumber,
	CancelledBy,
	CancellationDate = convert(smalldatetime, TCIDate),
	CancellationTime = TCITime
into
	#Cancellation
from
	(
	select
		B.EpisodeNumber,
		B.InternalPatientNumber,
		A.CancelledBy,
		B.TCIDate,
		B.TCITime
	from
		[$(PAS)].Inquire.CURRENTPREADMISSIONS P

	inner join [$(PAS)].Inquire.WLACTIVITY A
	on	A.WLACTIVITYID = 
		(
		select
			max(B.WLACTIVITYID)
		from
			[$(PAS)].Inquire.WLACTIVITY B
		where
			B.EpisodeNumber = P.EpisodeNumber
		and	B.InternalPatientNumber = P.InternalPatientNumber
		and	B.CancelledBy != 'P'
		and	B.Activity like 'Preadmit Cance%'
		)

	inner join [$(PAS)].Inquire.WLACTIVITY B
	on	B.EpisodeNumber = A.EpisodeNumber
	and	B.InternalPatientNumber = A.InternalPatientNumber
	and	B.Activity like 'Preadmission%'
	and	B.EpsActvDtimeInt = A.PrevActivityDTimeInt


	union all


	select
		B.EpisodeNumber,
		B.InternalPatientNumber,
		A.CancelledBy,
		B.TCIDate,
		B.TCITime
	from
		[$(PAS)].Inquire.WLCURRENT P

	inner join [$(PAS)].Inquire.WLACTIVITY A
	on	A.WLACTIVITYID = 
		(
		select
			max(B.WLACTIVITYID)
		from
			[$(PAS)].Inquire.WLACTIVITY B
		where
			B.EpisodeNumber = P.EpisodeNumber
		and	B.InternalPatientNumber = P.InternalPatientNumber
		and	B.CancelledBy != 'P'
		and	B.Activity like 'Preadmit Cance%'
		)

	inner join [$(PAS)].Inquire.WLACTIVITY B
	on	B.EpisodeNumber = A.EpisodeNumber
	and	B.InternalPatientNumber = A.InternalPatientNumber
	and	B.Activity like 'Preadmission%'
	and	B.EpsActvDtimeInt = A.PrevActivityDTimeInt
		
	) Cancellation

where
	convert(smalldatetime, TCIDate) >= dateadd(day, datediff(day, 0, getdate()), 0)

select @RowsProcessed = @@ROWCOUNT


update
	APC.WaitingList
set
	FutureCancellationDate = #Cancellation.CancellationDate,
	CancelledBy = #Cancellation.CancelledBy,
	FutureCancellationTime =
		case
		when #Cancellation.CancellationTime = '24:00'
			then convert(smalldatetime , #Cancellation.CancellationDate + ' 00:00')
		else convert(smalldatetime , #Cancellation.CancellationDate + ' ' + #Cancellation.CancellationTime)
		end	
from
	APC.WaitingList

inner join #Cancellation
on	#Cancellation.EpisodeNumber = WaitingList.SourceEncounterNo
and	#Cancellation.InternalPatientNumber = WaitingList.SourcePatientNo

where
	WaitingList.CensusDate = dateadd(day, datediff(day, 0, getdate()), 0)


SELECT @RowsUpdated = @@ROWCOUNT

-- patient cancellations

select
	EpisodeNumber,
	InternalPatientNumber,
	CancelledBy,
	CancellationDate = convert(smalldatetime, TCIDate),
	CancellationTime = TCITime
into
	#PatientCancellation
from
	(
	select
		B.EpisodeNumber,
		B.InternalPatientNumber,
		A.CancelledBy,
		B.TCIDate,
		B.TCITime
	from
		[$(PAS)].Inquire.CURRENTPREADMISSIONS P

	inner join [$(PAS)].Inquire.WLACTIVITY A
	on	A.WLACTIVITYID = 
		(
		select
			max(B.WLACTIVITYID)
		from
			[$(PAS)].Inquire.WLACTIVITY B
		where
			B.EpisodeNumber = P.EpisodeNumber
		and	B.InternalPatientNumber = P.InternalPatientNumber
		and	B.CancelledBy = 'P'
		and	B.Activity like 'Preadmit Cance%'
		)

	inner join [$(PAS)].Inquire.WLACTIVITY B
	on	B.EpisodeNumber = A.EpisodeNumber
	and	B.InternalPatientNumber = A.InternalPatientNumber
	and	B.Activity like 'Preadmission%'
	and	B.EpsActvDtimeInt = A.PrevActivityDTimeInt

	union all

	select
		B.EpisodeNumber,
		B.InternalPatientNumber,
		A.CancelledBy,
		B.TCIDate,
		B.TCITime
	from
		[$(PAS)].Inquire.WLCURRENT P

	inner join [$(PAS)].Inquire.WLACTIVITY A
	on	A.WLACTIVITYID = 
		(
		select
			max(B.WLACTIVITYID)
		from
			[$(PAS)].Inquire.WLACTIVITY B
		where
			B.EpisodeNumber = P.EpisodeNumber
		and	B.InternalPatientNumber = P.InternalPatientNumber
		and	B.CancelledBy = 'P'
		and	B.Activity like 'Preadmit Cance%'
		)

	inner join [$(PAS)].Inquire.WLACTIVITY B
	on	B.EpisodeNumber = A.EpisodeNumber
	and	B.InternalPatientNumber = A.InternalPatientNumber
	and	B.Activity like 'Preadmission%'
	and	B.EpsActvDtimeInt = A.PrevActivityDTimeInt

	) Cancellation

where
	convert(smalldatetime, TCIDate) >= dateadd(day, datediff(day, 0, getdate()), 0)

select @RowsProcessed = @RowsProcessed + @@ROWCOUNT


update
	APC.WaitingList
set
	FutureCancellationDate = 
	case
	when WaitingList.FutureCancellationDate is null then FuturePatient.CancellationDate
	when FuturePatient.CancellationDate < FutureCancellationDate then WaitingList.FutureCancellationDate
	else FuturePatient.CancellationDate
	end,

	CancelledBy = 
	case
	when WaitingList.CancelledBy is null then FuturePatient.CancelledBy
	when FuturePatient.CancellationDate < WaitingList.FutureCancellationDate then WaitingList.CancelledBy
	else FuturePatient.CancelledBy
	end,
	FuturePatientCancelDate = FuturePatient.CancellationDate,
	
	FutureCancellationTime =	-- Added Paul Egan 16/03/2015
	case
	when WaitingList.FutureCancellationTime is null then 
		case
		when FuturePatient.CancellationTime = '24:00'
			then convert(smalldatetime , FuturePatient.CancellationDate + ' 00:00')
		else convert(smalldatetime , FuturePatient.CancellationDate + ' ' + FuturePatient.CancellationTime)
		end	
	when FuturePatient.CancellationTime < FutureCancellationTime then WaitingList.FutureCancellationTime
	else
		case
		when FuturePatient.CancellationTime = '24:00'
			then convert(smalldatetime , FuturePatient.CancellationDate + ' 00:00')
		else convert(smalldatetime , FuturePatient.CancellationDate + ' ' + FuturePatient.CancellationTime)
		end	
	end

from
	APC.WaitingList

inner join #PatientCancellation FuturePatient
on	FuturePatient.EpisodeNumber = WaitingList.SourceEncounterNo
and	FuturePatient.InternalPatientNumber = WaitingList.SourcePatientNo

where
	WaitingList.CensusDate = dateadd(day, datediff(day, 0, getdate()), 0)


SELECT @RowsUpdated = @RowsUpdated + @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())
SELECT @Stats = 
	'Rows Updated ' + CONVERT(varchar(10), @RowsUpdated) + 
	', Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'AssignAPCFutureCancellation', @Stats, @StartTime



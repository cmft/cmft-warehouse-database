﻿
CREATE proc [ETL].[LoadInfectionReferenceData]

as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

--Organism

insert into Infection.Organism
(
OrganismCode
,Organism
)

select
     Code
	,Description
from
	(
	select 
		 Code
		,Description
	from
		[$(Telepath)].HCAI.Organisms
	union 
	select
		'NONE'
		,'No Organism Isolated'
	) Organisms
where
	not exists
			(
			select
				1
			from
				Infection.Organism
			where
				Organisms.Code = Organism.OrganismCode
			)



--SampleQualifier

insert into Infection.SampleQualifier
(
SampleQualifierCode
,SampleQualifier
)

select 
     Code
	,Description
from
	[$(Telepath)].HCAI.SampleQualifier NewSampleQualifier
where
	not exists
			(
			select
				1
			from
				Infection.SampleQualifier
			where
				NewSampleQualifier.Code = SampleQualifier.SampleQualifierCode
			)

--SampleSubType

insert into Infection.SampleSubType
(
SampleSubTypeCode
,SampleSubType
)

select 
     Code
	,Description
from
	[$(Telepath)].HCAI.SampleSubType NewSampleSubType
where
	not exists
			(
			select
				1
			from
				Infection.SampleSubType
			where
				NewSampleSubType.Code = SampleSubType.SampleSubTypeCode
			)

--SampleSubType

insert into Infection.SampleType
(
SampleTypeCode
,SampleType
)

select 
     Code
	,Description
from
	[$(Telepath)].HCAI.SampleType NewSampleType
where
	not exists
			(
			select
				1
			from
				Infection.SampleType
			where
				NewSampleType.Code = SampleType.SampleTypeCode
			)

--TestCategory

insert into Infection.TestCategory
(
TestCategory
)

select distinct
      TestCategory
from
	[$(Telepath)].HCAI.TelepathBase
where
	not exists
			(
			select
				1
			from
				Infection.TestCategory
			where
				TelepathBase.TestCategory = TestCategory.TestCategory
			)




select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

print @Stats

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
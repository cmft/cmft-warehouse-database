﻿
CREATE proc [ETL].[BuildAPCEncounterObservationSet]

as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

truncate table APC.EncounterObservationSet

insert into APC.EncounterObservationSet

select 
	Encounter.EncounterRecno
	,ObservationSet.ObservationSetRecno
from
	Observation.ObservationSet

inner join WH.DayLightSaving
on	ObservationSet.StartTime between DayLightSaving.StartTime and DayLightSaving.EndTime

inner join  APC.Encounter
on	ObservationSet.CasenoteNumber = Encounter.CasenoteNumber
and dateadd(hour, DayLightSaving.Offset, ObservationSet.StartTime) between Encounter.EpisodeStartTime and coalesce(Encounter.EpisodeEndTime, getdate())

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime






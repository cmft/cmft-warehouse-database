﻿






CREATE proc [ETL].[LoadCHAMP] as

/* 
==============================================================================================
Description:

When		Who			What
24/12/2014	Paul Egan	Initial Coding
23/02/2014	Paul Egan	Added CHAMPContactUsRequest, CHAMPBMICalculator, CHAMPAccount
===============================================================================================
*/

truncate table ETL.TImportCHAMPEncounter;
exec ETL.ExtractCHAMPEncounter;
exec ETL.LoadCHAMPEncounter;

truncate table ETL.TImportCHAMPContactUsRequest;
exec ETL.ExtractCHAMPContactUsRequest;
exec ETL.LoadCHAMPContactUsRequest;

truncate table ETL.TImportCHAMPBMICalculator;
exec ETL.ExtractCHAMPBMICalculator;
exec ETL.LoadCHAMPBMICalculator;

truncate table ETL.TImportCHAMPAccount;
exec ETL.ExtractCHAMPAccount;
exec ETL.LoadCHAMPAccount;




﻿

create procedure [ETL].[LoadAEEncounterPCECTra]  
	@PCECfrom smalldatetime = null
	,@PCECto smalldatetime = null
as


/****************************************************************************************
	View			: [dbo].[LoadPCECAEEncounter]
	Description		: 

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Initial Coding
	20140612	RR				Create table of breaches from Adastra for validation.
	20140730	Paul Egan       Corrected position of 'select @RowsInsertedTGH = @@rowcount;'
	20140829	RR				CaseNo 082790135 to breach table. Following issue with this record raised by HS and cannot be amended on front end.
	20141110	Paul Egan		CaseNo 110596934 to breach table. Patient returned to PCEC later in the day for a new prescription, 2nd attendance showing as breach on CI report.
	20141111	Paul Egan		Can disable Central using parameters, for Trafford backload.
								Trafford now populates breach table.
	20141119	Paul Egan		Added Trafford extra fields (AttendanceNumber, AgeOnArrival, Postcode)
	
	20141229	RR				Created new version of proc based on new TImport process from PCEC staging (Central).  
								Updated Central to a merge.  Original is saved in G:\,,,,\RR\SQL\Adastra
								Split out Trafford and Central, made no changes to Trafford
	
*****************************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted int
declare @RowsDeletedTGH int
declare @RowsInserted int
declare @RowsInsertedTGH int
declare @RowsUpdatedValidation int		-- Added Paul Egan 11/11/2014
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @to = coalesce(@PCECto, dateadd(day, datediff(day, 0, getdate()), 0));
select @from =		
	coalesce(
		 @PCECfrom
		,(select dateadd(day, 1, DateValue) from dbo.Parameter where Parameter = 'AEFREEZEDATE')
		,dateadd(day, -6, @to)
	)



/* ==================================== Trafford ==================================== */

	delete from AE.Encounter
	where
		InterfaceCode = 'ADAS'
		and SiteCode = 'RW3TR'		-- Added Paul Egan 11/11/2014

		and  coalesce(ArrivalDate, @from) between @from and @to

	select @RowsDeletedTGH = @@rowcount


	/*	20140612 RR Creates table of Breaches from Adastra for validation.
		Validated Times are used in the view which is referenced below */
	/*	20141111 Paul Egan added and altered for Trafford */

	insert into dbo.TImportAEPCECBreaches 
		(
		 AttendanceNumber
		,SiteCode
		,PatientSurname
		,ArrivalDate
		,ArrivalTime
		,SeenForTreatmentTime 
		,AttendanceConclusionTime 
		,DepartureTime
		,MinutesInDepartment 
		)
	select
		 AttendanceNumber = FirstCons.CaseNo 
		,SiteCode = 'RW3TR'
		,PatientSurname = '#N/A'	-- Not available in Trafford data
		,ArrivalDate = cast(FirstCons.EntryTime as date)
		,ArrivalTime = FirstCons.EntryTime
		,SeenForTreatmentTime = FirstCons.ConsultationStartTime
		,AttendanceConclusionTime = FirstCons.ConsultationEndTime
		,DepartureTime = FirstCons.ConsultationEndTime
		,MinutesInDepartment = datediff(mi,FirstCons.EntryTime,FirstCons.ConsultationEndTime)
	from
		dbo.TImportPCECAEEntryToFirstConsTHT FirstCons

	where
		cast(FirstCons.EntryTime as date) >= '20130401'
		and (datediff(mi,FirstCons.EntryTime,FirstCons.ConsultationEndTime) > 240)
		and not exists
			(
			select 1
			from dbo.TImportAEPCECBreaches
			where TImportAEPCECBreaches.AttendanceNumber = FirstCons.CaseNo
			)


	/* Trafford Attendances */
	INSERT INTO AE.Encounter
	(
		 SourceUniqueID
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,AttendanceCategoryCode
		,AttendanceDisposalCode
		,SourceAttendanceDisposalCode
		,PatientGroupCode
		,ArrivalDate
		,ArrivalTime
		,SeenForTreatmentTime
		,AttendanceConclusionTime
		,DepartureTime
		,CommissioningSerialNo
		,ProviderCode
		,CommissionerCode
		,SiteCode
		,InterfaceCode
		,PCTCode
		,ResidencePCTCode
		,ArrivalTimeAdjusted
		,UnplannedReattend7Day
		,Reportable
		
		/* Added Paul Egan 19/11/2014 */
		,Postcode
		,AttendanceNumber
		,AgeOnArrival
	)


	select
		 SourceUniqueID
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,AttendanceCategoryCode
		,AttendanceDisposalCode
		,SourceAttendanceDisposalCode
		,PatientGroupCode
		,ArrivalDate
		,ArrivalTime
		,SeenForTreatmentTime
		,AttendanceConclusionTime
		,DepartureTime
		,CommissioningSerialNo
		,ProviderCode
		,CommissionerCode
		,SiteCode
		,InterfaceCode
		,PCTCode
		,ResidencePCTCode
		,ArrivalTimeAdjusted
		,UnplannedReattend7Day
		,Reportable = 1
		
		/* Added Paul Egan 19/11/2014 */
		,Postcode
		,AttendanceNumber
		,AgeOnArrival
	FROM
		dbo.TLoadAEEncounterPCECTHT

		where

		coalesce(ArrivalDate, @from) between @from and @to
	


select @RowsInsertedTGH = @@rowcount;		-- Moved above the following update statement Paul Egan 30/07/2014


-- 20140612 RR added in the following to pick up validations which cannot be amended on the front end system
Update Encounter
set ArrivalDate = coalesce(cast(Br.ValidatedArrivalTime as date),Encounter.ArrivalDate)
	,ArrivalTime = coalesce(Br.ValidatedArrivalTime,Encounter.ArrivalTime)
	,ArrivalTimeAdjusted = coalesce(Br.ValidatedArrivalTime,Encounter.ArrivalTime)
	,SeenForTreatmentTime = coalesce(Br.ValidatedSeenForTreatmentTime,Encounter.SeenForTreatmentTime)
	,AttendanceConclusionTime = coalesce(Br.ValidatedAttendanceConclusionTime,Encounter.AttendanceConclusionTime)
	,DepartureTime = coalesce(Br.ValidatedDepartureTime,Encounter.DepartureTime)
from AE.Encounter Encounter
inner join TImportAEPCECBreaches Br
on Encounter.AttendanceNumber = Br.AttendanceNumber


select @RowsUpdatedValidation = @@rowcount;		-- Added Paul Egan 11/11/2014

select @Elapsed = DATEDIFF(minute, @StartTime, getdate());

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows deletedTGH ' + CONVERT(varchar(10), @RowsDeletedTGH) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', ' + 
	'Rows insertedTGH ' + CONVERT(varchar(10), @RowsInsertedTGH) + ', ' + 
	'Rows updated from validation ' + CONVERT(varchar(10), @RowsUpdatedValidation) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))


exec WriteAuditLogEvent 'LoadPCECAEEncounter', @Stats, @StartTime


	print @to
	print @from





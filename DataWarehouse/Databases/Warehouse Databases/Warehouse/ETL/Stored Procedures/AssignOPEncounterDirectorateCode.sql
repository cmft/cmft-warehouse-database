﻿




CREATE procedure [ETL].[AssignOPEncounterDirectorateCode] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()
select @RowsUpdated = 0

--if exists (select name from sys.tables where name = 'WHDivisionMap' and schema_name(schema_id) = 'OP')
if object_id('OP.WHDivisionMap') is not null 
  drop table OP.WHDivisionMap;

select * into OP.WHDivisionMap from WH.DivisionMap with (nolock);

-- main mapping
update OP.Encounter
set
	 Encounter.DirectorateCode = DivisionMap.DirectorateCode

	,Encounter.ReferringSpecialtyTypeCode =
		CASE 
		WHEN DivisionMap.Diagnostic = 'Y' THEN 'D'
		WHEN DivisionMap.Diagnostic = 'N' THEN 'I'
		ELSE DivisionMap.Diagnostic
		END
from
	OP.Encounter

inner join ETL.TImportOPEncounter --added 2013-07-24 CCB
on	TImportOPEncounter.SourceUniqueID = Encounter.SourceUniqueID

inner join OP.WHDivisionMap DivisionMap 
on	DivisionMap.SiteCode = Encounter.EpisodicSiteCode
and	DivisionMap.SpecialtyCode = Encounter.ReferringSpecialtyCode
and 
	(
		DivisionMap.ConsultantCode = Encounter.ConsultantCode
	or
		DivisionMap.ConsultantCode is null
	)
		
and Encounter.AppointmentDate between FromDate and coalesce(ToDate, '31 dec 9999')


where
	not exists
		(
		select
			1
		from
			OP.WHDivisionMap Previous
		where
			Previous.SiteCode = Encounter.EpisodicSiteCode
		and	Previous.SpecialtyCode = Encounter.ReferringSpecialtyCode


		and 
			(
				Previous.ConsultantCode = Encounter.ConsultantCode
			or
				Previous.ConsultantCode is null
			)

		and
			Encounter.AppointmentDate between Previous.FromDate and coalesce(Previous.ToDate, '31 dec 9999')

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end

				>

				case
				when DivisionMap.ConsultantCode is not null
				then 4
				else 0
				end
	
			--or
			--	(
			--		case
			--		when Previous.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end

			--		=

			--		case
			--		when DivisionRuleBase.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end 

				)
			)
			

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

-- specific mapping rules

update OP.Encounter
set
	DirectorateCode = 70
from
	OP.Encounter

inner join ETL.TImportOPEncounter --added 2013-07-24 CCB
on	TImportOPEncounter.SourceUniqueID = Encounter.SourceUniqueID

WHERE 
	Encounter.ReferringSpecialtyCode in ('PT' , 'DIET' , 'DTCO') --Like '110 P%' Or	Specialty Like '654%'
AND Encounter.AppointmentDate < '9 Jan 2009'


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


update OP.Encounter
set
	DirectorateCode = 0
from
	OP.Encounter

inner join ETL.TImportOPEncounter --added 2013-07-24 CCB
on	TImportOPEncounter.SourceUniqueID = Encounter.SourceUniqueID

where
	Encounter.ReferringSpecialtyCode in ( 'PAED' , 'PNEP' )
AND Encounter.AppointmentDate >= '7 Jan 2009'
AND Encounter.SiteCode = 'MRI'


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


update OP.Encounter
set
	DirectorateCode = 30
from
	OP.Encounter

inner join ETL.TImportOPEncounter --added 2013-07-24 CCB
on	TImportOPEncounter.SourceUniqueID = Encounter.SourceUniqueID

where
	Encounter.ReferringSpecialtyCode = 'PMET'
AND Encounter.ClinicCode in ('CAAMMETT' , 'CJEWMET' , 'CJHWMET' , 'RJEWMET')


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


update OP.Encounter
set
	DirectorateCode = 0
from
	OP.Encounter

inner join ETL.TImportOPEncounter --added 2013-07-24 CCB
on	TImportOPEncounter.SourceUniqueID = Encounter.SourceUniqueID

WHERE 
	Encounter.ReferringSpecialtyCode = 'OPHC'
AND Encounter.ClinicCode in ( 'CORTHOP' , 'CPREAD' );

--if exists (select name from sys.tables where name = 'WHDivisionMap1' and schema_name(schema_id) = 'OP')
if object_id('OP.WHDivisionMap') is not null 
  drop table OP.WHDivisionMap;



select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'AssignOPEncounterDirectorateCode', @Stats, @StartTime







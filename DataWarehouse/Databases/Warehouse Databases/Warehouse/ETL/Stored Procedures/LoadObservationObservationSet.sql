﻿
CREATE PROCEDURE [ETL].[LoadObservationObservationSet]

as 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 @LoadStartDate = min(LastModifiedTime)
	,@LoadEndDate = max(LastModifiedTime) 
from
	ETL.TLoadObservationObservationSet

merge
	Observation.ObservationSet target
using
	(
	select
		 SourceUniqueID
		,CasenoteNumber
		,DateOfBirth
		,SpecialtyID
		,LocationID
		,LocationStartTime
		,ReplacedSourceUniqueID
		,CurrentObservationSetFlag
		,AdditionalObservationSetFlag
		,AdmissionSourceUniqueID
		,EarlyWarningScoreRegimeApplicationID
		,ObservationProfileApplicationID
		,ObservationNotTakenReasonID
		,ClinicianPresentSeniorityID
		,StartDate
		,StartTime
		,TakenDate
		,TakenTime
		,OverallRiskIndexCode
		,OverallAssessedStatusID
		,AlertSeverityID
		,DueTime
		,DueTimeStatusID
		,DueTimeCreatedBySourceUniqueID
		,LastModifiedTime
		,InterfaceCode
	from
		ETL.TLoadObservationObservationSet

	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.LastModifiedTime between @LoadStartDate and @LoadEndDate

	then delete	

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SpecialtyID
			,LocationID
			,LocationStartTime
			,ReplacedSourceUniqueID
			,CurrentObservationSetFlag
			,AdditionalObservationSetFlag
			,AdmissionSourceUniqueID
			,EarlyWarningScoreRegimeApplicationID
			,ObservationProfileApplicationID
			,ObservationNotTakenReasonID
			,ClinicianPresentSeniorityID
			,StartDate
			,StartTime
			,TakenDate
			,TakenTime
			,OverallRiskIndexCode
			,OverallAssessedStatusID
			,AlertSeverityID
			,DueTime
			,DueTimeStatusID
			,DueTimeCreatedBySourceUniqueID
			,LastModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SpecialtyID
			,source.LocationID
			,source.LocationStartTime
			,source.ReplacedSourceUniqueID
			,source.CurrentObservationSetFlag
			,source.AdditionalObservationSetFlag
			,source.AdmissionSourceUniqueID
			,source.EarlyWarningScoreRegimeApplicationID
			,source.ObservationProfileApplicationID
			,source.ObservationNotTakenReasonID
			,source.ClinicianPresentSeniorityID
			,source.StartDate
			,source.StartTime
			,source.TakenDate
			,source.TakenTime
			,source.OverallRiskIndexCode
			,source.OverallAssessedStatusID
			,source.AlertSeverityID
			,source.DueTime
			,source.DueTimeStatusID
			,source.DueTimeCreatedBySourceUniqueID
			,source.LastModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not 
			isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.LocationStartTime = source.LocationStartTime
			,target.ReplacedSourceUniqueID = source.ReplacedSourceUniqueID
			,target.CurrentObservationSetFlag = source.CurrentObservationSetFlag
			,target.AdditionalObservationSetFlag = source.AdditionalObservationSetFlag
			,target.AdmissionSourceUniqueID = source.AdmissionSourceUniqueID
			,target.EarlyWarningScoreRegimeApplicationID = source.EarlyWarningScoreRegimeApplicationID
			,target.ObservationProfileApplicationID = source.ObservationProfileApplicationID
			,target.ObservationNotTakenReasonID = source.ObservationNotTakenReasonID
			,target.ClinicianPresentSeniorityID = source.ClinicianPresentSeniorityID
			,target.StartDate = source.StartDate
			,target.StartTime = source.StartTime
			,target.TakenDate = source.TakenDate
			,target.TakenTime = source.TakenTime
			,target.OverallRiskIndexCode = source.OverallRiskIndexCode
			,target.OverallAssessedStatusID = source.OverallAssessedStatusID
			,target.AlertSeverityID = source.AlertSeverityID
			,target.DueTime = source.DueTime
			,target.DueTimeStatusID = source.DueTimeStatusID
			,target.DueTimeCreatedBySourceUniqueID = source.DueTimeCreatedBySourceUniqueID
			,target.LastModifiedTime = source.LastModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿

create proc ETL.LoadHRReferenceData

as

insert into HR.Organisation
(
	OrganisationCode
	,Organisation
)

select distinct
	OrganisationCode
	,Organisation
from
	[$(SmallDatasets)].HR.Organisation Staging
where not exists
				(
				select
					1
				from
					HR.Organisation
				where
					Staging.OrganisationCode = Organisation.OrganisationCode
				)


insert into [HR].[Ward]
(
	[WardCode]
	,[Ward]
)

select distinct
	WardCode
	,Ward
from
	[$(SmallDatasets)].HR.Ward Staging
where not exists
				(
				select
					1
				from
					HR.Ward
				where
					Staging.WardCode = Ward.WardCode
				)
﻿CREATE procedure [ETL].[ExtractMortalitySurgery]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.Surgery target
using
	(
	select
		 SourceUniqueID
		,FormTypeID
		,ReviewStatus
		,Forename
		,Surname
		,CasenoteNo
		,DateOfDeath
		,ReviewedDate
		,ReviewedBy
		,RecordedAppropriatelyToYou
		,AlternateNamedConsultant
		,TerminalCareDeathExpected
		,AgreeWithCauseOfDeath
		,DidSurgeryDuringLastAdmission
		,HowManyDaysBeforeDeathApprox
		,CoronerInformed
		,HighLevelInvestigation
		,CaseReferredToBleepReview
		,SurgicalComplicationContributedToDeath
		,SurgeryHigherThanNormalAnticipatedMortality
		,StandardRiskPercentage
		,MortalityQuotedPercentage
		,DeathPreventable
		,DeathPreventableComment
		,TerminalCareManagedWell
		,TerminalCareManagedWellComment
		,CauseOfDeath1
		,CauseOfDeath2
		,CauseOfDeath3
		,CauseOfDeath4
		,CoMorbidity1
		,CoMorbidity2
		,CoMorbidity3
		,CoMorbidity4
		,CoMorbidity5
		,DeathDiscussedWithAnaesthetist
		,Anaesthetist
		,ClassificationGrade
		,SourcePatientNo
		,EpisodeStartTime
		,ContextCode
	from
		ETL.TLoadMortalitySurgery
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalitySurgery Latest
			where
				Latest.SourcePatientNo = TLoadMortalitySurgery.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalitySurgery.SourceUniqueID
			)
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,FormTypeID
			,ReviewStatus
			,Forename
			,Surname
			,CasenoteNo
			,DateOfDeath
			,ReviewedDate
			,ReviewedBy
			,RecordedAppropriatelyToYou
			,AlternateNamedConsultant
			,TerminalCareDeathExpected
			,AgreeWithCauseOfDeath
			,DidSurgeryDuringLastAdmission
			,HowManyDaysBeforeDeathApprox
			,CoronerInformed
			,HighLevelInvestigation
			,CaseReferredToBleepReview
			,SurgicalComplicationContributedToDeath
			,SurgeryHigherThanNormalAnticipatedMortality
			,StandardRiskPercentage
			,MortalityQuotedPercentage
			,DeathPreventable
			,DeathPreventableComment
			,TerminalCareManagedWell
			,TerminalCareManagedWellComment
			,CauseOfDeath1
			,CauseOfDeath2
			,CauseOfDeath3
			,CauseOfDeath4
			,CoMorbidity1
			,CoMorbidity2
			,CoMorbidity3
			,CoMorbidity4
			,CoMorbidity5
			,DeathDiscussedWithAnaesthetist
			,Anaesthetist
			,ClassificationGrade
			,SourcePatientNo
			,EpisodeStartTime
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.FormTypeID
			,source.ReviewStatus
			,source.Forename
			,source.Surname
			,source.CasenoteNo
			,source.DateOfDeath
			,source.ReviewedDate
			,source.ReviewedBy
			,source.RecordedAppropriatelyToYou
			,source.AlternateNamedConsultant
			,source.TerminalCareDeathExpected
			,source.AgreeWithCauseOfDeath
			,source.DidSurgeryDuringLastAdmission
			,source.HowManyDaysBeforeDeathApprox
			,source.CoronerInformed
			,source.HighLevelInvestigation
			,source.CaseReferredToBleepReview
			,source.SurgicalComplicationContributedToDeath
			,source.SurgeryHigherThanNormalAnticipatedMortality
			,source.StandardRiskPercentage
			,source.MortalityQuotedPercentage
			,source.DeathPreventable
			,source.DeathPreventableComment
			,source.TerminalCareManagedWell
			,source.TerminalCareManagedWellComment
			,source.CauseOfDeath1
			,source.CauseOfDeath2
			,source.CauseOfDeath3
			,source.CauseOfDeath4
			,source.CoMorbidity1
			,source.CoMorbidity2
			,source.CoMorbidity3
			,source.CoMorbidity4
			,source.CoMorbidity5
			,source.DeathDiscussedWithAnaesthetist
			,source.Anaesthetist
			,source.ClassificationGrade
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.ContextCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			isnull(target.FormTypeID, 0) = isnull(source.FormTypeID, 0)
		and isnull(target.ReviewStatus, 0) = isnull(source.ReviewStatus, 0)
		and isnull(target.Forename, '') = isnull(source.Forename, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.ReviewedDate, getdate()) = isnull(source.ReviewedDate, getdate())
		and isnull(target.ReviewedBy, '') = isnull(source.ReviewedBy, '')
		and isnull(target.RecordedAppropriatelyToYou, 0) = isnull(source.RecordedAppropriatelyToYou, 0)
		and isnull(target.AlternateNamedConsultant, '') = isnull(source.AlternateNamedConsultant, '')
		and isnull(target.TerminalCareDeathExpected, 0) = isnull(source.TerminalCareDeathExpected, 0)
		and isnull(target.AgreeWithCauseOfDeath, 0) = isnull(source.AgreeWithCauseOfDeath, 0)
		and isnull(target.DidSurgeryDuringLastAdmission, 0) = isnull(source.DidSurgeryDuringLastAdmission, 0)
		and isnull(target.HowManyDaysBeforeDeathApprox, 0) = isnull(source.HowManyDaysBeforeDeathApprox, 0)
		and isnull(target.CoronerInformed, 0) = isnull(source.CoronerInformed, 0)
		and isnull(target.HighLevelInvestigation, 0) = isnull(source.HighLevelInvestigation, 0)
		and isnull(target.CaseReferredToBleepReview, 0) = isnull(source.CaseReferredToBleepReview, 0)
		and isnull(target.SurgicalComplicationContributedToDeath, 0) = isnull(source.SurgicalComplicationContributedToDeath, 0)
		and isnull(target.SurgeryHigherThanNormalAnticipatedMortality, 0) = isnull(source.SurgeryHigherThanNormalAnticipatedMortality, 0)
		and isnull(target.StandardRiskPercentage, 0) = isnull(source.StandardRiskPercentage, 0)
		and isnull(target.MortalityQuotedPercentage, 0) = isnull(source.MortalityQuotedPercentage, 0)
		and isnull(target.DeathPreventable, 0) = isnull(source.DeathPreventable, 0)
		and isnull(target.DeathPreventableComment, '') = isnull(source.DeathPreventableComment, '')
		and isnull(target.TerminalCareManagedWell, 0) = isnull(source.TerminalCareManagedWell, 0)
		and isnull(target.TerminalCareManagedWellComment, '') = isnull(source.TerminalCareManagedWellComment, '')
		and isnull(target.CauseOfDeath1, '') = isnull(source.CauseOfDeath1, '')
		and isnull(target.CauseOfDeath2, '') = isnull(source.CauseOfDeath2, '')
		and isnull(target.CauseOfDeath3, '') = isnull(source.CauseOfDeath3, '')
		and isnull(target.CauseOfDeath4, '') = isnull(source.CauseOfDeath4, '')
		and isnull(target.CoMorbidity1, '') = isnull(source.CoMorbidity1, '')
		and isnull(target.CoMorbidity2, '') = isnull(source.CoMorbidity2, '')
		and isnull(target.CoMorbidity3, '') = isnull(source.CoMorbidity3, '')
		and isnull(target.CoMorbidity4, '') = isnull(source.CoMorbidity4, '')
		and isnull(target.CoMorbidity5, '') = isnull(source.CoMorbidity5, '')
		and isnull(target.DeathDiscussedWithAnaesthetist, 0) = isnull(source.DeathDiscussedWithAnaesthetist, 0)
		and isnull(target.Anaesthetist, '') = isnull(source.Anaesthetist, '')
		and isnull(target.ClassificationGrade, '') = isnull(source.ClassificationGrade, '')
		and isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.FormTypeID = source.FormTypeID
			,target.ReviewStatus = source.ReviewStatus
			,target.Forename = source.Forename
			,target.Surname = source.Surname
			,target.CasenoteNo = source.CasenoteNo
			,target.DateOfDeath = source.DateOfDeath
			,target.ReviewedDate = source.ReviewedDate
			,target.ReviewedBy = source.ReviewedBy
			,target.RecordedAppropriatelyToYou = source.RecordedAppropriatelyToYou
			,target.AlternateNamedConsultant = source.AlternateNamedConsultant
			,target.TerminalCareDeathExpected = source.TerminalCareDeathExpected
			,target.AgreeWithCauseOfDeath = source.AgreeWithCauseOfDeath
			,target.DidSurgeryDuringLastAdmission = source.DidSurgeryDuringLastAdmission
			,target.HowManyDaysBeforeDeathApprox = source.HowManyDaysBeforeDeathApprox
			,target.CoronerInformed = source.CoronerInformed
			,target.HighLevelInvestigation = source.HighLevelInvestigation
			,target.CaseReferredToBleepReview = source.CaseReferredToBleepReview
			,target.SurgicalComplicationContributedToDeath = source.SurgicalComplicationContributedToDeath
			,target.SurgeryHigherThanNormalAnticipatedMortality = source.SurgeryHigherThanNormalAnticipatedMortality
			,target.StandardRiskPercentage = source.StandardRiskPercentage
			,target.MortalityQuotedPercentage = source.MortalityQuotedPercentage
			,target.DeathPreventable = source.DeathPreventable
			,target.DeathPreventableComment = source.DeathPreventableComment
			,target.TerminalCareManagedWell = source.TerminalCareManagedWell
			,target.TerminalCareManagedWellComment = source.TerminalCareManagedWellComment
			,target.CauseOfDeath1 = source.CauseOfDeath1
			,target.CauseOfDeath2 = source.CauseOfDeath2
			,target.CauseOfDeath3 = source.CauseOfDeath3
			,target.CauseOfDeath4 = source.CauseOfDeath4
			,target.CoMorbidity1 = source.CoMorbidity1
			,target.CoMorbidity2 = source.CoMorbidity2
			,target.CoMorbidity3 = source.CoMorbidity3
			,target.CoMorbidity4 = source.CoMorbidity4
			,target.CoMorbidity5 = source.CoMorbidity5
			,target.DeathDiscussedWithAnaesthetist = source.DeathDiscussedWithAnaesthetist
			,target.Anaesthetist = source.Anaesthetist
			,target.ClassificationGrade = source.ClassificationGrade
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



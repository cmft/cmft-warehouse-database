﻿


CREATE PROCEDURE [ETL].[LoadResultCENICEOrder]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, OrderRequestTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, OrderRequestTime)) 
from
	ETL.TLoadResultCENICEOrder

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Result.CENICEOrder target
using
	(
	select
		 SourceUniqueID
		,PatientID
		,PatientIdentifier
		,DistrictNo
		,CasenoteNumber
		,NHSNumber 
		,ClinicianID
		,ProviderID
		,LocationID
		,OrderPriority
		,OrderStatusCode
		,OrderRequestTime
		,OrderReceivedTime
		,OrderCollectionTime
		,InterfaceCode
		,OrderChecksum = 
		checksum(
			 SourceUniqueID
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber 
			,ClinicianID
			,ProviderID
			,LocationID
			,OrderPriority
			,OrderStatusCode
			,OrderRequestTime
			,OrderReceivedTime
			,OrderCollectionTime
			,InterfaceCode
		)				
	from
		ETL.TLoadResultCENICEOrder
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.OrderRequestTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber 
			,ClinicianID
			,ProviderID
			,LocationID
			,OrderPriority
			,OrderStatusCode
			,OrderRequestTime
			,OrderReceivedTime
			,OrderCollectionTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,OrderChecksum
			)
		values
			(
			 source.SourceUniqueID
			,source.PatientID
			,source.PatientIdentifier
			,source.DistrictNo
			,source.CasenoteNumber
			,source.NHSNumber 
			,source.ClinicianID
			,source.ProviderID
			,source.LocationID
			,source.OrderPriority
			,source.OrderStatusCode
			,source.OrderRequestTime
			,source.OrderReceivedTime
			,source.OrderCollectionTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,OrderChecksum
			)

	when matched
	and	target.OrderChecksum <> source.OrderChecksum
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.PatientID = source.PatientID
			,target.PatientIdentifier = source.PatientIdentifier
			,target.DistrictNo = source.DistrictNo
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.ClinicianID = source.ClinicianID
			,target.ProviderID = source.ProviderID
			,target.LocationID = source.LocationID
			,target.OrderPriority = source.OrderPriority
			,target.OrderStatusCode = source.OrderStatusCode
			,target.OrderRequestTime = source.OrderRequestTime
			,target.OrderReceivedTime = source.OrderReceivedTime
			,target.OrderCollectionTime = source.OrderCollectionTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.OrderChecksum = source.OrderChecksum

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


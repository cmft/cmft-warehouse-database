﻿CREATE procedure [ETL].[ExtractInquireDelivery]

as

insert into ETL.TImportPASDelivery
select
	 *
from
	(
	select
		 DELIVERYID
		,A1stStage
		,A2ndStage
		,AdtMaternityProfessionalPriorInvolvementInt
		,AnaesanalgDuring
		,ConsEpiEndDtTm
		,ConsEpiEndDtTmInt
		,ConsEpiStartDtTm
		,ConsEpiStartDtTmInt
		,left(EpisodeNumber, 12) EpisodeNumber
		,FCENumber
		,left(FirstAnteNatalAssessmentDate, 12) FirstAnteNatalAssessmentDate
		,Gestationonset
		,left(InternalPatientNumber, 12) InternalPatientNumber
		,KeMatAnaesReas
		,KeMatAnaesType
		,KeMatDate1stAnaNeut
		,KeMatMethOnsetInt
		,KeMatOrigIntInt
		,KeMatPlaceOfDelivInt
		,KeMatReasChgInt
		,KeMatStatPersCondDelInt
		,Parity
		,PregnancyNumber
		,SmokedPrior
		,SmokedPriorInt
		,SmokedTimeOfBooking
		,SmokedTimeOfBookInt
		,SmokedTimeOfDelivery
		,SmokedTimeOfDelivInt
	from
		[$(PAS)].Inquire.DELIVERY
	where
		DELIVERYID != '199609231459||199609151223'
) Delivery

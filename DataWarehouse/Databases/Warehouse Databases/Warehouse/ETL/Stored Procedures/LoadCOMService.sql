﻿

CREATE procedure [ETL].[LoadCOMService] as


insert into COM.Service
(
	Code
	,Name
	,SPECT_REFNO
	,IPMDescription
	,Destination
	,TCSDestinationID
	,District
	,Directorate
	,StartDate
	,EndDate
	,ArchiveFlag
	,ModifiedDate
	,ModifiedUser
)
           
select
	ServiceCode
	,Service
	,SpecialtyID
	,IPMDescription
	,Destination
	,TCSDestinationID
	,District
	,Directorate
	,StartDate
	,EndDate
	,ArchiveFlag
	,ModifiedDate
	,ModifiedUser
from
	ETL.TLoadCOMService 
	
where
	not exists
			(
				select
					1
				from
					COM.Service
				where
					TLoadCOMService.ServiceCode = Service.Code
			) 
			


﻿

CREATE procedure [ETL].[ExtractInquireAEAttendance]
(
@FromDate datetime = null
,@ToDate datetime = null
)

as

/* 
==============================================================================================
Description:	Created for linking OCM results with AE attendances 

When		Who			What
20150501	Paul Egan	Initial Coding
===============================================================================================
*/


declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @from varchar(12)
declare @to varchar(12)

/* ======== Debug only ========*/
 ----------Declare
	---------- @FromDate datetime = null
	----------,@ToDate datetime = null
/* ============================*/

/* Default to last 3 months */
select @FromDate = coalesce(@FromDate, dateadd(month, -3, getdate()))
select @ToDate = coalesce(@ToDate, dateadd(day, 0, getdate()))

/* Convert datetime to integer format */
select @from = cast(year(@FromDate) as varchar(4)) + right('0' + cast(month(@FromDate) as varchar(2)), 2) + right('0' + cast(day(@FromDate) as varchar(2)), 2) + '0000'
select @to = cast(year(@ToDate) as varchar(4)) + right('0' + cast(month(@ToDate) as varchar(2)), 2) + right('0' + cast(day(@ToDate) as varchar(2)), 2) + '2400'

select @RowsInserted = 0
select @StartTime = getdate()

insert into ETL.TImportPASAEAttendance
	(
	 AEATTENDANCEID
	,AeAttendanceDatetimeneutral
	,DepartmentCode
	,InternalPatientNumber
	,EpisodeNumber
	,HospitalCode
	)

select
	 AEATTENDANCEID
	,AeAttendanceDatetimeneutral
	,DepartmentCode
	,InternalPatientNumber
	,EpisodeNumber
	,HospitalCode
from
	(
	select
		 AEATTENDANCEID
		,AeAttendanceDatetimeneutral
		,DepartmentCode
		,InternalPatientNumber
		,EpisodeNumber
		,HospitalCode
	from
		[$(PAS)].Inquire.AEATTENDANCE
	) AEAttendance

where
	AeAttendanceDatetimeneutral between @from and @to
;



select @RowsInserted = @@Rowcount

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) +
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

print @Stats

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime





﻿
CREATE procedure [ETL].[LoadChildHealth_v2] as

--Entity
truncate table ChildHealth.Entity

insert
into
	ChildHealth.Entity
(
	 SourceUniqueID
	,ExternalID
	,TypeCode
	,CategoryCode
	,NHSNumber
	,FullName
	,Surname
	,Forename
	,MiddleName
	,Prefix
	,Suffix
	,OrganisationCode
	,Active
	,Created
	,ByWhom
	,PseudonomisedPerson
	,SecondaryLanguageSpokenCode
	,PrimaryLanguageSpokenCode
	,SocialSecurityNo
	,DateOfBirth
	,SexCode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,OccupationCode
	,EmploymentCode
	,InterpreterRequiredCode
	,DateOfBirthEstimated
	,GPCode
	,RegistrationStatusCode
	,RegistrationStatusStartDate
)
select
	 EntID.EntSys
	,EntID.ExtID
	,EntID.Typ 
	,EntID.Cat 
	,EntID.ID 
	,EntID.NamStr 
	,EntID.LstNam
	,EntID.FstNam 
	,EntID.MidNam 
	,EntID.Pre 
	,EntID.Suf 
	,EntID.OrgSys 
	,EntID.ActSts 
	,EntID.EtrDat 
	,EntID.EntPrsNum 
	,EntID.PsuPrsInd 
	,EntDem.SecLng 
	,EntDem.PrmLng 
	,EntDem.Ssn 
	,chsREG_CMF_BASIC_DETAILS.DATE_OF_BIRTH 
	,EntDem.Sex 
	,coalesce(EntDem.Eth,'99')
	,EntDem.MarSts 
	,EntDem.Rlg 
	,EntDem.Edu 
	,EntDem.Inc 
	,EntDem.MomMdnNam 
	,EntDem.DobEst 
	,GPPrac.GPCode 
	,chsREG_CURRENT_STATUS.REG_STATUS_CODE 
	,chsREG_CURRENT_STATUS.STATUS_START_DATE 
from
	[$(ChildHealth_CarePlus)].SQLUser.EntID

left join [$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BASIC_DETAILS
on	chsREG_CMF_BASIC_DETAILS.EntSys = EntID.EntSys

left join [$(ChildHealth_CarePlus)].SQLUser.EntDem
on	EntDem.EntSys = EntID.EntSys

--Duplicates unless we take the last distinct one!
left join 
	[$(ChildHealth_CarePlus)].SQLUser.GPPrac_view GPPrac
on	GPPrac.EntSys = EntID.EntSys

left join [$(ChildHealth_CarePlus)].SQLUser.chsREG_CURRENT_STATUS
on	chsREG_CURRENT_STATUS.EntSys = EntID.EntSys


--AntigenDetail
truncate table ChildHealth.AntigenDetail

insert
into
	ChildHealth.AntigenDetail
(
	 EntitySourceUniqueID
	,EventDate
	,SequenceNo
	,AntigenValue
	,ReasonNotGivenCode
	,SupplementaryDose
	,DoseIdentifier
	,AntigenCode
)
select
	 chsVI_CVP_ANTIGEN_DETAILS.EntSys 
	,chsVI_CVP_ANTIGEN_DETAILS.EVENT_DATE 
	,chsVI_CVP_ANTIGEN_DETAILS.EVENT_SEQ_NUMBER 
	,chsVI_CVP_ANTIGEN_DETAILS.ANTIGEN_VALUE 
	,chsVI_CVP_ANTIGEN_DETAILS.REASON_ANTIGEN_NOT_GIVEN_CODE 
	,chsVI_CVP_ANTIGEN_DETAILS.SUPPLEMENTARY_DOSE 
	,chsVI_CVP_ANTIGEN_DETAILS.DOSE_IDENTIFIER 
	,chsVI_CVP_ANTIGEN_DETAILS.ANTIGEN_CODE 
from
	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_ANTIGEN_DETAILS


--BCG
truncate table ChildHealth.BCG

insert
into
	ChildHealth.BCG
(
	 EntitySourceUniqueID
	,EventDate
	,ConsentDate
	,Refusal
	,Course
	,HeafSessionDate
	,AbsentForHeafCode
	,DeferHeafCode
	,BCGContraIndCode
	,HeafAdministeredByCode
	,HeafTestLocationCode
	,HeafReadingDate
	,AbsentForReadingCode
	,HeafResult
	,ContraIndToBCGCode
	,AppointmentNextTime
	,HeafReadByTypeCode
	,HeafReadByCode
	,ReadingLocationCode
	,SessionDate
	,PreviousScar
	,RefusedVaccination
	,SiteCode
	,BatchNumber
	,GivenByCode
	,LocationCode
	,XRayReferralDate
	,HeafAdministeredByTypeCode
	,PlannedSessionCode
	,GivenByTypeCode
	,Comment
	,HeafSiteCode
	,HeafBatchNumber
	,SkinTestAdminVenueTypeCode
	,SkinTestReadingVenueTypeCode
	,GivenVenueTypeCode
	,SkinTestTypeCode
)
select
	 chsVI_CVP_BCG_DETAILS.EntSys
	,chsVI_CVP_BCG_DETAILS.EVENT_DATE 
	,chsVI_CVP_BCG_DETAILS.CONSENT_DATE 
	,chsVI_CVP_BCG_DETAILS.REFUSAL 
	,chsVI_CVP_BCG_DETAILS.BCG_COURSE 
	,chsVI_CVP_BCG_DETAILS.HEAF_SESSION_DATE 
	,chsVI_CVP_BCG_DETAILS.ABSENT_FOR_HEAF 
	,chsVI_CVP_BCG_DETAILS.DEFER_HEAF_CODE 
	,chsVI_CVP_BCG_DETAILS.PERM_CONTRA_IND_CODE 
	,chsVI_CVP_BCG_DETAILS.HEAF_ADMINISTERED_BY_CODE 
	,chsVI_CVP_BCG_DETAILS.HEAF_TEST_LOCATION 
	,chsVI_CVP_BCG_DETAILS.HEAF_READING_DATE 
	,chsVI_CVP_BCG_DETAILS.ABSENT_FOR_READING 
	,chsVI_CVP_BCG_DETAILS.HEAF_RESULT 
	,chsVI_CVP_BCG_DETAILS.CONTRA_IND_TO_BCG_CODE 
	,chsVI_CVP_BCG_DETAILS.APPT_NEXT_TIME 
	,chsVI_CVP_BCG_DETAILS.HEAF_READ_BY_TYPE 
	,chsVI_CVP_BCG_DETAILS.HEAF_READ_BY_CODE 
	,chsVI_CVP_BCG_DETAILS.READING_LOCATION 
	,chsVI_CVP_BCG_DETAILS.BCG_SESSION_DATE 
	,chsVI_CVP_BCG_DETAILS.PREVIOUS_SCAR 
	,chsVI_CVP_BCG_DETAILS.REFUSED_VACCINATION 
	,chsVI_CVP_BCG_DETAILS.BCG_SITE 
	,chsVI_CVP_BCG_DETAILS.BCG_BATCH_NUMBER 
	,chsVI_CVP_BCG_DETAILS.BCG_GIVEN_BY_CODE 
	,chsVI_CVP_BCG_DETAILS.BCG_LOCATION 
	,chsVI_CVP_BCG_DETAILS.X_RAY_REFERRAL_DATE 
	,chsVI_CVP_BCG_DETAILS.HEAF_ADMINISTERED_BY_TYPE 
	,chsVI_CVP_BCG_DETAILS.PLANNED_SESSION 
	,chsVI_CVP_BCG_DETAILS.BCG_GIVEN_BY_TYPE 
	,chsVI_CVP_BCG_DETAILS.BCG_COMMENT 
	,chsVI_CVP_BCG_DETAILS.HEAF_SITE 
	,chsVI_CVP_BCG_DETAILS.HEAF_BATCH_NUMBER 
	,chsVI_CVP_BCG_DETAILS.SKIN_TEST_ADMIN_VENUE_TYPE 
	,chsVI_CVP_BCG_DETAILS.SKIN_TEST_READ_VENUE_TYPE 
	,chsVI_CVP_BCG_DETAILS.BCG_GIVEN_VENUE_TYPE 
	,chsVI_CVP_BCG_DETAILS.SKIN_TEST_TYPE 
from
	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_BCG_DETAILS

--2015-11-18 CCB Don't write back to Staging data, handle in the script instead (which it now does).
----Birth
----20140729 - bad non-numeric data in birth_weight
----
--update [$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BIRTH_DETAILS
--set Birth_weight = null 
--where isnumeric(birth_weight) = 0;
--update [$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BIRTH_DETAILS
--set Birth_weight = null 
--where rtrim(ltrim(birth_weight)) = '.';

truncate table ChildHealth.Birth

insert
into
	ChildHealth.Birth
(
	 EntitySourceUniqueID
	,MotherSystemNumber
	,MotherName
	,MotherDateOfBirth
	,MotherNHSNumber
	,RegistrationStatusCode
	,Pregnancy
	,NumberBorn
	,BirthOrder
	,BirthStateCode
	,BirthPlaceCode
	,BirthWeight
	,BirthTime
	,PreviousLiveBirths
	,PreviousStillBirths
	,PreviousAbortions
	,AbortionInduced
	,AbortionsSpontaneous
	,PreviousMiscarriages
	,PreviousChildDeaths
	,GestationCalculatedEstimate
	,GestationPeriod
	,GestationByExam
	,GestationByDate
	,GestationCalculated
	,FirstAssessmentDate
	,LabourDeliveryComplications
	,DurationOfFirstStage
	,DurationOfSecondStage
	,MotherHEPBStatusCode
	,PlaceOfBirthTypeCode
	,RegistrarCode
	,ApgarScoreCode1
	,ApgarScoreCode2
	,ApgarScoreCode3
	,CongenitalMalformation
	,DischargeAddress
	,Ultrasound
	,OnsetOfRespiration
	,Length
	,SCBUHospitalCode
	,ResusMethodCode
	,OnsetLabourCode
	,DeliveryModeCode
	,FeedIntentionCode
	,NotifyEducationCode
	,DelivererStatusCode
	,AnaesthesiaDuringLabourCode
	,AnaesthesiaDuringDeliveryCode
	,AnaesthesiaPostDeliveryCode
	,OneParentFamily
	,MotherOccupation
	,FatherOccupation
	,HeadCircumference
	,HealthVisitorCode
	,SchoolCode
	,Smoking
	,BreastFeedingAtBirth
	,BreastFeedingAtDischarge
	,MotherAgeAtConception
	,YearOfBirth
	,PCTCode
	,InitialStatusCode
)
select
	 EntitySourceUniqueID = chsREG_CMF_BIRTH_DETAILS.Entsys
	,MotherSystemNumber = chsREG_CMF_BIRTH_DETAILS.MOTHERS_SYSTEM_NUMBER 
	,MotherName = chsREG_CMF_BIRTH_DETAILS.MOTHERS_NAME 
	,MotherDateOfBirth = chsREG_CMF_BIRTH_DETAILS.MOTHERS_DOB 
	,MotherNHSNumber = chsREG_CMF_BIRTH_DETAILS.MOTHERS_NHS 
	,RegistrationStatusCode =chsREG_CMF_BIRTH_DETAILS.REG_STAT_CODE 
	,Pregnancy = chsREG_CMF_BIRTH_DETAILS.PREGNANCY 
	,NumberBorn = chsREG_CMF_BIRTH_DETAILS.NUM_BORN 
	,BirthOrder = chsREG_CMF_BIRTH_DETAILS.BIRTH_ORDER 

	,BirthStateCode =
		case
		when chsREG_CMF_BIRTH_DETAILS.BIRTH_STATE_CODE = ''
		then null
		else chsREG_CMF_BIRTH_DETAILS.BIRTH_STATE_CODE 
		end

	,BirthPlaceCode = chsREG_CMF_BIRTH_DETAILS.BIRTH_PLACE_CODE 

	--,BirthWeight = chsREG_CMF_BIRTH_DETAILS.BIRTH_WEIGHT 
	,BirthWeight = 
		case
		when isnumeric(chsREG_CMF_BIRTH_DETAILS.BIRTH_WEIGHT) = 0 then null
		when rtrim(ltrim(chsREG_CMF_BIRTH_DETAILS.BIRTH_WEIGHT)) = '.' then null
		else chsREG_CMF_BIRTH_DETAILS.BIRTH_WEIGHT
		end

--	,chsREG_CMF_BIRTH_DETAILS.BTIME --not sure what this is... nothing in database documentation so see what it contains when table is populated
	,BirthTime = chsREG_CMF_BIRTH_DETAILS.TIME_OF_BIRTH 
	,PreviousLiveBirths = chsREG_CMF_BIRTH_DETAILS.PREV_LIVE_BIRTHS 
	,PreviousStillBirths = chsREG_CMF_BIRTH_DETAILS.PREV_STILL_BIRTHS 
	,PreviousAbortions = chsREG_CMF_BIRTH_DETAILS.PREV_ABORT 
	,AbortionInduced = chsREG_CMF_BIRTH_DETAILS.ABORT_INDUCED 
	,AbortionsSpontaneous = chsREG_CMF_BIRTH_DETAILS.ABORT_SPONTANEOUS 
	,PreviousMiscarriages = chsREG_CMF_BIRTH_DETAILS.PREV_MISCARRIAGES 
	,PreviousChildDeaths = chsREG_CMF_BIRTH_DETAILS.PREV_CHILD_DEATHS 
	,GestationCalculatedEstimate = chsREG_CMF_BIRTH_DETAILS.GESTATION_CALC_EST 
	,GestationPeriod = chsREG_CMF_BIRTH_DETAILS.GESTATION_PERIOD 
	,GestationByExam = chsREG_CMF_BIRTH_DETAILS.GESTATION_BY_EXAM 
	,GestationByDate = chsREG_CMF_BIRTH_DETAILS.GESTATION_BY_DATE 
	,GestationCalculated = chsREG_CMF_BIRTH_DETAILS.GESTATION_CALCULATED 
	,FirstAssessmentDate = chsREG_CMF_BIRTH_DETAILS.FIRST_ASSESSMENT_DATE 
	,LabourDeliveryComplications = chsREG_CMF_BIRTH_DETAILS.LABOUR_DEL_COMPLICATIONS 
	,DurationOfFirstStage = chsREG_CMF_BIRTH_DETAILS.DURATION_OF_1ST_STAGE 
	,DurationOfSecondStage = chsREG_CMF_BIRTH_DETAILS.DURATION_OF_2ND_STAGE 
	,MotherHEPBStatusCode = chsREG_CMF_BIRTH_DETAILS.MOTHERS_HEP_B_STAT_CODE 
	,PlaceOfBirthTypeCode = chsREG_CMF_BIRTH_DETAILS.PLACE_OF_BIRTH_CODE_TYPE 
	,RegistrarCode = chsREG_CMF_BIRTH_DETAILS.REGISTRAR_CODE 
	,ApgarScoreCode1 = chsREG_CMF_BIRTH_DETAILS.APGAR_SCORE_1 
	,ApgarScoreCode2 = chsREG_CMF_BIRTH_DETAILS.APGAR_SCORE_2 
	,ApgarScoreCode3 = chsREG_CMF_BIRTH_DETAILS.APGAR_SCORE_3 
	,CongenitalMalformation = chsREG_CMF_BIRTH_DETAILS.CONG_MAL 
	,DischargeAddress = chsREG_CMF_BIRTH_DETAILS.DISCHARGE_ADDRESS 
	,Ultrasound = chsREG_CMF_BIRTH_DETAILS.ULTRA_SOUND 
	,OnsetOfRespiration = chsREG_CMF_BIRTH_DETAILS.ONSET_OF_RESP 
	,Length = chsREG_CMF_BIRTH_DETAILS.LENGTH 
	,SCBUHospitalCode = chsREG_CMF_BIRTH_DETAILS.SCBU_HOSP_CODE 
	,ResusMethodCode = chsREG_CMF_BIRTH_DETAILS.RESUS_METHOD_CODE 
	,OnsetLabourCode = chsREG_CMF_BIRTH_DETAILS.ONSET_LABOUR_CODE 
	,DeliveryModeCode = chsREG_CMF_BIRTH_DETAILS.MODE_DELIVERY_CODE 
	,FeedIntentionCode = chsREG_CMF_BIRTH_DETAILS.FEED_INTENT_CODE 
	,NotifyEducationCode = chsREG_CMF_BIRTH_DETAILS.NOTIFY_EDUCATION_CODE 
	,DelivererStatusCode = chsREG_CMF_BIRTH_DETAILS.STATUS_OF_DELIVERER_CODE 
	,AnaesthesiaDuringLabourCode = chsREG_CMF_BIRTH_DETAILS.ANAESTHESIA_DURING_LABOUR 
	,AnaesthesiaDuringDeliveryCode = chsREG_CMF_BIRTH_DETAILS.ANAESTHESIA_DURING_DELIVERY 
	,AnaesthesiaPostDeliveryCode = chsREG_CMF_BIRTH_DETAILS.ANAESTHESIA_POST_DELIVERY 
	,OneParentFamily = chsREG_CMF_BIRTH_DETAILS.ONE_PARENT_FAMILY 
	,MotherOccupation = chsREG_CMF_BIRTH_DETAILS.MOTHER_OCCUPATION 
	,FatherOccupation = chsREG_CMF_BIRTH_DETAILS.FATHER_OCCUPATION 
	,HeadCircumference = chsREG_CMF_BIRTH_DETAILS.HEAD_CIRCUMFERENCE 
	,HealthVisitorCode = chsREG_CMF_BIRTH_DETAILS.HEALTH_VISITOR_CODE 
	,SchoolCode = chsREG_CMF_BIRTH_DETAILS.SCHOOL_CODE 
	,Smoking = chsREG_CMF_BIRTH_DETAILS.SMOKING 
	,BreastFeedingAtBirth = chsREG_CMF_BIRTH_DETAILS.BREAST_FEEDING_AT_BIRTH 
	,BreastFeedingAtDischarge = chsREG_CMF_BIRTH_DETAILS.FEEDING_AT_DISCHARGE 
	,MotherAgeAtConception = chsREG_CMF_BIRTH_DETAILS.MOTHERS_AGE_CONCEPTION 
	,DateOfBirth = chsREG_CMF_BIRTH_DETAILS.DATE_OF_BIRTH 
	,PCTCode = chsREG_CMF_BIRTH_DETAILS.INITIAL_PCT 
	,InitialStatusCode = chsREG_CMF_BIRTH_DETAILS.INITIAL_STATUS 
from
	[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BIRTH_DETAILS



--BreastFeeding
truncate table ChildHealth.BreastFeeding

insert
into
	ChildHealth.BreastFeeding
(
	 SourceUniqueID
	,EntitySourceUniqueID
	,StageNo
	,BreastFeedingResultCode
	,StageDate
)
select
	 REG_BREAST_FEEDING.ID 
	,REG_BREAST_FEEDING.SYSTEM_NUMBER 
	,REG_BREAST_FEEDING.STAGENUMBER 
	,REG_BREAST_FEEDING.BREASTFEEDINGRESULT 
	,REG_BREAST_FEEDING.BREASTFEEDINGSTAGEDATE 

from
	[$(ChildHealth_CarePlus)].SQLUser.REG_BREAST_FEEDING


--ChildStatusHistory
truncate table ChildHealth.ChildStatusHistory

insert
into
	ChildHealth.ChildStatusHistory
(
	 EntitySourceUniqueID
	,ChangedDate
	,RegistrationStatusCode
	,VIStatusCode
	,PreSchoolStatusCode
	,SchoolStatusCode
	,LostContactReasonCode
	,LostContactComment
	,LostContactDate
	,ByWhom
	,StartDate
)
select
	 chsREG_CH_STATUS_HIST.EntSys 
	,chsREG_CH_STATUS_HIST.DATE_CHANGED 
	,chsREG_CH_STATUS_HIST.REG_STATUS_CODE 
	,chsREG_CH_STATUS_HIST.VI_STATUS_CODE 
	,chsREG_CH_STATUS_HIST.PRE_SCHOOL_STATUS 
	,chsREG_CH_STATUS_HIST.SCHOOL_STATUS 
	,chsREG_CH_STATUS_HIST.LOST_CONTACT_REASON 
	,chsREG_CH_STATUS_HIST.LOST_CONTACT_COMMENTS 
	,chsREG_CH_STATUS_HIST.LOST_CONTACT_DATE 
	,chsREG_CH_STATUS_HIST.[USER_ID] 
	,chsREG_CH_STATUS_HIST.STATUS_START_DATE 

from
	[$(ChildHealth_CarePlus)].SQLUser.chsREG_CH_STATUS_HIST


--EntityAddress
truncate table ChildHealth.EntityAddress

insert
into
	ChildHealth.EntityAddress
(
	 EntitySourceUniqueID
	,AddressSeqno
	,AddressTypeCode
	,AddressStartDate
	,AddressEndDate
	,Address1
	,Address2
	,City
	,Street
	,Postcode
	,Country
	,Location
	,Directions
	,TimeZone
	,ObsDst
	,CouID
	,SHACode
	,WardOfResidenceCode
	,PCTOfResidenceCode
)
select
	 EntAdd.EntSys 
	,EntAdd.AddSeq 
	,EntAdd.Lbl 
	,EntAdd.EffBegDat 
	,EntAdd.EffEndDat 
	,EntAdd.Add1 
	,EntAdd.Add2 
	,EntAdd.Cit 
	,EntAdd.Stt 
	,EntAdd.Zip 
	,EntAdd.Cty 
	,EntAdd.Loc 
	,EntAdd.Dir 
	,EntAdd.TimZon 
	,EntAdd.ObsDst 
	,EntAdd.CouID 
	,EntAdd.SHA 
	,EntAdd.WardOfRes 
	,EntAdd.PCTOfRes 

from
	[$(ChildHealth_CarePlus)].SQLUser.EntAdd

--EntityGP
truncate table ChildHealth.EntityGP

insert
into
	ChildHealth.EntityGP
(
	 EntitySourceUniqueID
	,GPCode
	,LocalID
	,PCTStartDate
	,PCTCode
	,PracticeCode
	,SurgeryCode

)
select distinct
	 EntitySourceUniqueID = EntSys 
	,GPCode = coalesce(GPCode, 2) --GP CODE UNKNOWN
	,LocalID = LocalID 
	,PCTStartDate = PCTBegDat 
	,PCTCode = PCTCode 
	,PracticeCode = PracCode 
	,SurgeryCode = SurgCode 

from
	[$(ChildHealth_CarePlus)].SQLUser.GPPrac_view GPPrac



--EntityXRef
truncate table ChildHealth.EntityXRef

insert
into
	ChildHealth.EntityXRef
(
	 EntitySourceUniqueID
	,SequenceNo
	,XrefTypeCode
	,XRefValue
	,StartDate
	,EndDate
)
select
	 EntitySourceUniqueID = EntSys 
	,SequenceNo = IdSeq 
	,XrefTypeCode = IDTyp 
	,XRefValue = IdVal 
	,StartDate = EffBegDat 
	,EndDate = EffEndDat 

from
	[$(ChildHealth_CarePlus)].SQLUser.EntXID


--Event
truncate table ChildHealth.Event

insert
into
	ChildHealth.Event
(
	 EntitySourceUniqueID
	,EventDate
	,SequenceNo
	,ClinicCode
	,SessionCode
	,AppointmentTime
	,ScheduledCode
	,Attended
	,AnyVaccineGiven
	,ReasonNoVaccineGivenCode
	,ScheduledAntiGensCode
	,ReactionCode
	,DateFlag
	,ModeOfEntryCode
	,Comment
	,AwaitingResults
	,OnPCV38PrintCode
	,SeenByTypeCode
	,SeenByCode
	,VenueTypeCode
	,DateOfBirth
	,BCGTypeCode
	,HPVExtractedDate
	,SuspensionEndDate
	,EventDateFormat
	,PCTCode
	,RegistrationStatusCode
	,ResultEnteredDate
)
select
	 chsVI_CVP_EVENT_DETAILS.EntSys 
	,chsVI_CVP_EVENT_DETAILS.EVENT_DATE 
	,chsVI_CVP_EVENT_DETAILS.EVENT_SEQUENCE_NUMBER 
	,chsVI_CVP_EVENT_DETAILS.CLINIC_CODE 
	,chsVI_CVP_EVENT_DETAILS.SESSIONx 
	,chsVI_CVP_EVENT_DETAILS.APPOINTMENT_TIME 
	,chsVI_CVP_EVENT_DETAILS.SCHEDULED 
	,chsVI_CVP_EVENT_DETAILS.ATTENDED_YNC 
	,chsVI_CVP_EVENT_DETAILS.ANY_VACCINE_GIVEN_YN 
	,chsVI_CVP_EVENT_DETAILS.REASON_NONE_GIVEN_CODE 
	,chsVI_CVP_EVENT_DETAILS.SCHEDULED_ANTIGENS 
	,chsVI_CVP_EVENT_DETAILS.REACTION_CODE 
	,chsVI_CVP_EVENT_DETAILS.DATE_FLAG 
	,chsVI_CVP_EVENT_DETAILS.MODE_OF_ENTRY 
	,chsVI_CVP_EVENT_DETAILS.COMMENT_TEXT 
	,chsVI_CVP_EVENT_DETAILS.AWAITING_RESULTS_YN 
	,chsVI_CVP_EVENT_DETAILS.ON_PCV38_PRINT 
	,chsVI_CVP_EVENT_DETAILS.SEEN_BY_TYPE 
	,chsVI_CVP_EVENT_DETAILS.SEEN_BY 
	,chsVI_CVP_EVENT_DETAILS.VENUE_TYPE_CODE ed
	,chsVI_CVP_EVENT_DETAILS.DOB 
	,chsVI_CVP_EVENT_DETAILS.BCG_TYPE 
	,chsVI_CVP_EVENT_DETAILS.DATE_HPV_EXTRACTED 
	,chsVI_CVP_EVENT_DETAILS.SUSPENSION_END_DATE 
	,chsVI_CVP_EVENT_DETAILS.EVENT_DATE_FORMAT 
	,chsVI_CVP_EVENT_DETAILS.PCT_CODE_EV 
	,chsVI_CVP_EVENT_DETAILS.REG_STATUS_CODE_EV 
	,chsVI_CVP_EVENT_DETAILS.RESULT_ENTERED_DATE 

from
	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS

--ExamHistory

--this is required to convert the RecallDate field from varchar to date
set dateformat dmy

truncate table ChildHealth.ExamHistory

insert
into
	ChildHealth.ExamHistory
(
	 EntitySourceUniqueID
	,ExamDate
	,ExamCode
	,SessionCode
	,AppointmentTime
	,IsScheduled
	,AttendanceCode
	,ReasonForDNACode
	,Examined
	,ReasonNotExaminedCode
	,SeenByCode
	,Referred
	,ClinicCode
	,SeenByTypeCode
	,Comment1
	,Comment2
	,Comment3
	,Comment4
	,Comment5
	,TimeTaken
	,ParentCarerPresent
	,ChoiceLetterIndicator
	,RecallDate
	,DiscussedWithMo
	,FurtherAction
	,RestartImms
	,VisionTestTypeCode
	,ColourTestTypeCode
	,IsExamRecall
	,ReferralSourceCode
	,RecallExamCode
	,RecallTimeslotCode
	,AppointmentDate
	,FollowUpDetails
	,ReviewTypeCode
	,DiagnosisCodesRecorded
	,VenueTypeCode
	,SequenceNo
	,RecallVenueTypeCode
	,RecallVenueCode
	,RetestHeight
	,HeightReferredToCode
	,RetestWeight
	,WeightReferredToCode
	,RetestHearing
	,HearingReferredToCode
	,RetestVision
	,VisionReferredToCode
	,RetestColourVision
	,ColourVisionReferredToCode
	,SourceOfReferralSchoolHealthCode
	,MST
	,Y9
	,Y10
	,ReviewDate
	,HearingRetestTime
	,VisionRetestTime
	,AudiometricReferredToCode
	,AudiometricReferredToDate
	,OrthopticReferredToCode
	,OrthopticReferredToDate
	,ReferredToCode
	,ReferredFromCode
	,CurrentTreatmentCode
	,HearingReferralSequenceNo
	,VisionReferralSequenceNo
	,HearingReferralClinicCode
	,VisionReferralClinicCode
	,SMOAudioReferredToCode
	,Attended
	,ExamModuleCode
	,PCTCode
	,PreSchoolScheduleCode
	,ReferedToOpthalmicOptician
	,RegistrationStatusCode
	,ResultEnteredDate
)
select
	 EntitySourceUniqueID = EntSys 
	,ExamDate = EXAM_DATE 
	,ExamCode = EXAM_CODE 
	,SessionCode = SESSIONx 
	,AppointmentTime = APPOINTMENT_TIME 
	,IsScheduled = SCHEDULED_YN 
	,AttendanceCode = ATTENDED_YNC 
	,ReasonForDNACode = REASON_FOR_DNA 
	,Examined = EXAMINED_YN 
	,ReasonNotExaminedCode = REASON_NOT_EXAMINED 
	,SeenByCode = SEEN_BY 
	,Referred = REFERRED_YN 
	,ClinicCode = CLINIC_CODE 
	,SeenByTypeCode = SEEN_BY_TYPE 
	,Comment1 = EXAM_COMMENT 
	,Comment2 = EXAM_COMMENT_1 
	,Comment3 = EXAM_COMMENT_2 
	,Comment4 = EXAM_COMMENT_3 
	,Comment5 = EXAM_COMMENT_4 
	,TimeTaken = TIME_TAKEN 
	,ParentCarerPresent = PARENT_CARER_PRESENT 
	,ChoiceLetterIndicator = CHOICE_LETTER_IND_YN 
	,RecallDate = RECALL_DATE 
	,DiscussedWithMo = DISCUSSED_WITH_MO_YN 
	,FurtherAction = FURTHER_ACTION 
	,RestartImms = RESTART_IMMS_YN 
	,VisionTestTypeCode = VISION_TEST_TYPE_CODE 
	,ColourTestTypeCode = COLOUR_TEST_TYPE_CODE 
	,IsExamRecall = IS_EXAM_A_RECALL_YN 
	,ReferralSourceCode = REFERRAL_SOURCE_CODE 
	,RecallExamCode = RECALL_EXAM 
	,RecallTimeslotCode = RECALL_TIMESLOT 
	,AppointmentDate = APPOINTMENT_DATE 
	,FollowUpDetails = FOLLOW_UP_DETAILS_YN 
	,ReviewTypeCode = REVIEW_TYPE 
	,DiagnosisCodesRecorded = ICD_CODES_RECORDED 
	,VenueTypeCode = VENUE_TYPE_CODE 
	,SequenceNo = SEQ_NO 
	,RecallVenueTypeCode = RECALL_VENUE_TYPE_CODE 
	,RecallVenueCode = RECALL_VENUE_CODE 
	,RetestHeight = RESTEST_HEIGHT 
	,HeightReferredToCode = HEIGHT_REFERRED_TO_CODE 
	,RetestWeight = RETEST_WEIGHT 
	,WeightReferredToCode = WEIGHT_REFERRED_TO_CODE 
	,RetestHearing = RETEST_HEARING 
	,HearingReferredToCode = HEARING_REFERRED_TO_CODE 
	,RetestVision = RETEST_VISION 
	,VisionReferredToCode = VISION_REFERRED_TO_CODE 
	,RetestColourVision = RETEST_COLOUR_VISION 
	,ColourVisionReferredToCode = COLOUR_VISION_REFERRED_TO_CODE 
	,SourceOfReferralSchoolHealthCode = SOURCE_OF_REFERRAL_SCH_HEALTH_CODE 
	,MST = MST_YN 
	,Y9 = Y9_YN 
	,Y10 = Y10_YN 
	,ReviewDate = REVIEW_DATE 
	,HearingRetestTime = HEARING_RETEST_TIME 
	,VisionRetestTime = VISION_RETEST_TIME 
	,AudiometricReferredToCode = AUDIOMETRIC_REF_TO_CODE 
	,AudiometricReferredToDate = AUDIOMETRIC_REF_DATE 
	,OrthopticReferredToCode = ORTHOPTIC_REF_TO_CODE 
	,OrthopticReferredToDate = ORTHOPTIC_REF_DATE 
	,ReferredToCode = REFERRED_TO_CODE 
	,ReferredFromCode = REFERRED_FROM_CODE 
	,CurrentTreatmentCode = CURRENT_TREATMENT 
	,HearingReferralSequenceNo = HEARING_REFERRAL_SEQUENCE_NUMBER 
	,VisionReferralSequenceNo = VISION_REFERRAL_SEQUENCE_NUMBER 
	,HearingReferralClinicCode = HEARING_REFERRAL_CLINIC 
	,VisionReferralClinicCode = VISION_REFERRAL_CLINIC 
	,SMOAudioReferredToCode = SMO_AUDIO_REF_TO_CODE 
	,Attended = ATTENDED 
	,ExamModuleCode = EXAM_MODULE 
	,PCTCode = PCT_CODE 
	,PreSchoolScheduleCode = PRE_SCHOOL_SCHEDULE_CODE 
	,ReferedToOpthalmicOptician = REF_TO_OPTHALMIC_OPT_YN 
	,RegistrationStatusCode = REG_STATUS_CODE_EX 
	,ResultEnteredDate = RESULT_ENTERED_DATE 
from
	[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS

--ExamHistoryTestResult

truncate table ChildHealth.ExamHistoryTestResult

insert
into
	ChildHealth.ExamHistoryTestResult
(
	 EntitySourceUniqueID
	,ExamDate
	,ExamCode
	,TestCode
	,Result
	,ReferredToCode
	,ClinicCode
)
select
	 EntitySourceUniqueID = EntSys 
	,ExamDate = EXAM_DATE 
	,ExamCode = EXAM_CODE 
	,TestCode = TEST_CODE 
	,Result = coalesce( RESULT_DESC, RESULT )
	,ReferredToCode = REFERRED_TO_CODE 
	,ClinicCode = CLINIC_CODE 
from
	[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_TEST_RESULTS


--ExamHistoryReferral

truncate table ChildHealth.ExamHistoryReferral

insert
into
	ChildHealth.ExamHistoryReferral
(
	 EntitySourceUniqueID
	,ExamDate
	,ExamCode
	,ReferredToCode
	,ClinicCode
	,ReferralDate
	,SequenceNo
)
select
	 EntitySourceUniqueID = EntSys 
	,ExamDate = EXAM_DATE 
	,ExamCode = EXAM_CODE 
	,ReferredToCode = REFERRED_TO_CODE 
	,ClinicCode = CLINIC_CODE 
	,ReferralDate = REFERRAL_DATE 
	,SequenceNo = REFERRAL_SEQUENCE_NUMBER 

from
	[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_REFERRALS


--GP
truncate table ChildHealth.GP

insert
into
	ChildHealth.GP
(
	 GPCode
	,Initials
	,Surname
	,Address1
	,Address2
	,Address3
	,Address4
	,Postcode
	,TelephoneNo
	,PracticeCode
	,FPCCode
	,FPCContractedCode
	,PooledGP
	,Active
	,LocalID
	,NationalCode
	,PASGPCode
)
select
	 MAST_GP.GP_NUMBER_INT 
	,MAST_GP.GP_INITALS 
	,MAST_GP.GP_SURNAME 
	,MAST_GP.GP_PRIV_ADDR1 
	,MAST_GP.GP_PRIV_ADDR2 
	,MAST_GP.GP_PRIV_ADDR3 
	,MAST_GP.GP_PRIV_ADDR4 
	,MAST_GP.GP_PRIV_PC 
	,MAST_GP.GP_PRIV_TEL_NUM 
	,MAST_GP.GP_PRACTICE_CODE 
	,MAST_GP.GP_FPC_CODE 
	,MAST_GP.GP_FPC_CONTRACTED_CODE 
	,MAST_GP.POOLEDGP 
	,MAST_GP.GP_INUSE_YN 
	,MAST_GP.GP_NUMBER 
	,MAST_GP.NATIONAL_CODE 
	,MAST_GP.PAS_GP_CODE 

from
	[$(ChildHealth_CarePlus)].SQLUser.MAST_GP



--Practice
truncate table ChildHealth.Practice

insert
into
	ChildHealth.Practice
(
	 PracticeCode
	,Practice
	,Address1
	,Address2
	,Address3
	,Address4
	,Postcode
	,TelephoneNo
	,SeniorPartnerCode
	,NationalCode
	,PCTCode
	,Email
	,FaxNo
	,MainFacilityTypeCode
	,MainFacilityCode
	,LocalID
	,Active
	,PooledGP
)
select
	 MAST_PRAC.GP_PRAC_CODE 
	,MAST_PRAC.GP_PRAC_NAME 
	,MAST_PRAC.GP_PRAC_ADDR1 
	,MAST_PRAC.GP_PRAC_ADDR2 
	,MAST_PRAC.GP_PRAC_ADDR3 
	,MAST_PRAC.GP_PRAC_ADDR4 
	,MAST_PRAC.GP_PRAC_PC 
	,MAST_PRAC.GP_PRAC_TEL_NUM 
	,MAST_PRAC.GP_PRAC_SENIOR_PARTNER 
	,MAST_PRAC.GP_PRAC_NAT_CODE 
	,MAST_PRAC.GP_PRAC_PCG_CODE 
	,MAST_PRAC.PRACTICE_E_MAIL_ADDRESS 
	,MAST_PRAC.PRACTICE_FAX_NUMBER 
	,MAST_PRAC.PRACTICE_MAIN_FACILITY_TYPE 
	,MAST_PRAC.PRACTICE_MAIN_FACILITY_CODE 
	,MAST_PRAC.LOCAL_ID 
	,MAST_PRAC.GP_PRAC_INUSE_YN 
	,MAST_PRAC.POOLED_GP_CODE 

from
	[$(ChildHealth_CarePlus)].SQLUser.MAST_PRAC


--ChildFlag
truncate table ChildHealth.ChildFlag

insert
into
	ChildHealth.ChildFlag
(
	 FlagCode
	,EntitySourceUniqueID
	,AddedDate
	,RemovedDate
	,FlagReasonForRemovalCode
)
select
	 chsREG_CLIENT_FLAGS.FLAG 
	,chsREG_CLIENT_FLAGS.EntSys 
	,chsREG_CLIENT_FLAGS.DATA_FLAG_ADDED 
	,chsREG_CLIENT_FLAGS.DATE_FLAG_REMOVED 
	,chsREG_CLIENT_FLAGS.REASON_FOR_REMOVAL_CODE 

from
	[$(ChildHealth_CarePlus)].SQLUser.chsREG_CLIENT_FLAGS


--School
truncate table ChildHealth.School

insert
into
	ChildHealth.School
(
	 SchoolCode
	,School
	,Address1
	,Address2
	,Address3
	,Address4
	,Postcode
	,TelephoneNo
	,FaxNo
	,MaximumAge
	,MinimmumAge
	,ClerksName
	,ClerksTelephoneNo
	,SchoolCategoryCode
	,SchoolTypeCode
	,Active
	,PCTCode
	,LocalID
	,NationalSchoolCode
	,EmailAddress
	,DepartmentOfEducationCode
	,EmailAppointment
)
select
	 chsSH_SCHOOL_MASTER.SCHOOL_CODE 
	,chsSH_SCHOOL_MASTER.SCHOOL_NAME 
	,chsSH_SCHOOL_MASTER.ADDR_LINE1 
	,chsSH_SCHOOL_MASTER.ADDR_LINE2 
	,chsSH_SCHOOL_MASTER.ADDR_LINE3 
	,chsSH_SCHOOL_MASTER.ADDR_LINE4 
	,chsSH_SCHOOL_MASTER.POST_CODE 
	,chsSH_SCHOOL_MASTER.TELEPHONE_NO 
	,chsSH_SCHOOL_MASTER.FAX 
	,chsSH_SCHOOL_MASTER.NORMAL_MAX_AGE 
	,chsSH_SCHOOL_MASTER.NORMAL_MIN_AGE 
	,chsSH_SCHOOL_MASTER.CLERKS_NAME 
	,chsSH_SCHOOL_MASTER.CLERKS_TEL_EXT
	,chsSH_SCHOOL_MASTER.CATEGORY_CODE 
	,chsSH_SCHOOL_MASTER.SCHOOL_TYPE_CODE 
	,chsSH_SCHOOL_MASTER.IN_USE_FLAG_YN 
	,chsSH_SCHOOL_MASTER.PCT_CODE 
	,chsSH_SCHOOL_MASTER.LOCAL_ID 
	,chsSH_SCHOOL_MASTER.NATIONALSCHOOLID 
	,chsSH_SCHOOL_MASTER.SCHOOLEMAILADDRESS 
	,chsSH_SCHOOL_MASTER.DEPARTMENT_OF_EDUCATION_CODE 
	,chsSH_SCHOOL_MASTER.EMAIL_APPT_YN 

from
	[$(ChildHealth_CarePlus)].SQLUser.chsSH_SCHOOL_MASTER

--Relationship
truncate table ChildHealth.Relationship

insert
into
	ChildHealth.Relationship
(
	 EntitySourceUniqueID
	,RelationshipEntitySourceUniqueID
	,RelationshipCode
	,RoleTypeCode
	,StartDate
	,EndDate
)
select
	 EntitySourceUniqueID = EntSys 
	,RelationshipEntitySourceUniqueID = RelEntSys 
	,RelationshipCode = Rel 
	,RoleTypeCode = Typ 
	,StartDate = StartDate 
	,EndDate = EndDate 

from
	[$(ChildHealth_CarePlus)].SQLUser.RelEnt


--Reference
truncate table ChildHealth.Reference

insert
into
	ChildHealth.Reference
(
	 ReferenceDomainCode
	,ReferenceCode
	,Reference
	,ReservedCode
	,StartDate
	,EndDate
	,Switch
	,Alert
	,EpisodeGroup
)
select
	 ReferenceDomainCode = McrTyp 
	,ReferenceCode = Mcr 
	,Reference = [Des] 
	,ReservedCode = RsvCod 
	,StartDate = StartDate 
	,EndDate = EndDate 
	,Switch = Switch 
	,Alert = Alert 
	,EpisodeGroup = EpisodeGrp 

from
	[$(ChildHealth_CarePlus)].SQLUser.MCR

--ChildHealthEncounter
truncate table ChildHealth.ChildHealthEncounter

insert
into
	ChildHealth.ChildHealthEncounter
(
	 SourceUniqueID
	,EntitySourceUniqueID
	,StartDate
)
select
	 SourceUniqueID = ChildHealthEncounter.AdmSys
	,EntitySourceUniqueID = ChildHealthEncounter.PatSys
	,StartDate = ChildHealthEncounter.BegDat
from
	[$(ChildHealth_CarePlus)].SQLUser.Enc ChildHealthEncounter

--Staff
truncate table ChildHealth.Staff

insert
into
	ChildHealth.Staff
(
	 ChildHealthEncounterSourceUniqueID
	,SequenceNo
	,TEMSYS
	,PersonTypeCode
	,StaffCode
	,Staff
	,StartDate
	,EndDate
	,PersonOrder
	,StaffTypeCode
)
select
	 ChildHealthEncounterSourceUniqueID = AdmSys 
	,SequenceNo = PrsSeq 
	,TEMSYS = TEMSYS 
	,PersonTypeCode = PrsTyp 
	,StaffCode = PrsSys 
	,Staff = Dsc 
	,StartDate = BegDat 
	,EndDate = EndDat 
	,PersonOrder = PrsOrd 
	,StaffTypeCode = StaffTyp 

from
	[$(ChildHealth_CarePlus)].SQLUser.AdmPrs


--ChildRegister

truncate table ChildHealth.ChildRegister

insert
into
	ChildHealth.ChildRegister
(
	 RegisterCode
	,EntitySourceUniqueID
	,CurrentRegisterDate
	,OnRegisterDate
	,OffRegisterDate
	,ReasonForRemovalCode
	,ClientNumber
	,RegisterStaffCode
	,RegisterStaffTypeCode
)
select
	 RegisterCode = REGISTER_CODE
	,EntitySourceUniqueID = EntSys
	,CurrentRegisterDate = CURRENT_DATE_REGISTER
	,OnRegisterDate = DATE_ON_REGISTER
	,OffRegisterDate = DATE_OFF_REGISTER
	,ReasonForRemovalCode = REASON_FOR_REMOVAL_CODE
	,ClientNumber = CLIENT_NUMBER
	,RegisterStaffCode = REG_STAFF_CODE
	,RegisterStaffTypeCode = REG_STAFF_TYPE
from
	[$(ChildHealth_CarePlus)].SQLUser.chsREG_CHILD_REGISTER


--AEAttendance

truncate table ChildHealth.AEAttendance

insert
into
	ChildHealth.AEAttendance
(
	 EntitySourceUniqueID
	,AttendanceDate
	,SequenceNo
	,VenueTypeCode
	,VenueCode
	,Comment
	,AEDiagnosisCode
	,ReasonForAttending
	,ReasonForDischargeCode
	,WhereDischarged
	,FollowUp
	,FollowUpByWhomCode
	,FollowUpByWhomTypeCode
)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,AttendanceDate = ATTENDANCE_DATE
	,SequenceNo = EVENT_SEQUENCE
	,VenueTypeCode = VENUE_TYPE
	,VenueCode = VENUE_CODE
	,Comment = COMMENT
	,AEDiagnosisCode = AE_DIAGNOSIS_CODE
	,ReasonForAttending = REASON_FOR_ATTENDING_TEXT
	,ReasonForDischargeCode = REASON_FOR_DISCHARGE_CODE
	,WhereDischarged = WHERE_DISCHARGED
	,FollowUp = FOLLOW_UP
	,FollowUpByWhomCode = FOLLOW_UP_BY_WHOM_CODE
	,FollowUpByWhomTypeCode = FOLLOW_UP_BY_WHOM_TYPE
from
	[$(ChildHealth_CarePlus)].SQLUser.AE_ATTENDANCES


--NeonatalInitialExam

truncate table ChildHealth.NeonatalInitialExam

insert
into
	ChildHealth.NeonatalInitialExam
(
	 EntitySourceUniqueID
	,ExamDate
	,SequenceNo
	,ExamCentreCode
	,ExamCategoryCode
	,ExaminerCode
	,TestNumber
	,RiskFactorPresent
	,RiskFactorCode
	,TestMethodCode
	,EquipmentCode
	,RightTestID
	,RightResultCode
	,LeftTestID
	,LeftTestResultCode
	,OutcomeCode
	,ReferralCode
	,Comments1
	,Comments2
	,VenueTypeCode
)
select
	 EntitySourceUniqueID = EntSys
	,ExamDate = EXAM_DATE
	,SequenceNo = SEQ_NUMBER
	,ExamCentreCode = EXAM_CENTRE_CODE
	,ExamCategoryCode = EXAMINER_CATEGORY
	,ExaminerCode = EXAMINER_CODE
	,TestNumber = TEST_NUMBER
	,RiskFactorPresent = RISK_FACTOR_PRESENT
	,RiskFactorCode = RISK_FACTOR_CODE
	,TestMethodCode = TEST_METHOD_CODE
	,EquipmentCode = EQUIPMENT_CODE
	,RightTestID = RIGHT_TEST_ID
	,RightResultCode = RIGHT_RESULT_CODE
	,LeftTestID = LEFT_TEST_ID
	,LeftTestResultCode = LEFT_RESULT_CODE
	,OutcomeCode = OUTCOME_CODE
	,ReferralCode = REFERRAL_CODE
	,Comments1 = COMMENTS1
	,Comments2 = COMMENTS2
	,VenueTypeCode = VENUE_TYPE_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.chsREG_NEO_NATAL_INITIAL_EXAM


--ClientSummary

truncate table ChildHealth.ClientSummary

insert
into
	ChildHealth.ClientSummary
(
	 EntitySourceUniqueID
	,ScheduleCode
	,ClinicCode
	,OutstandingAppointmentDate
	,OutstandingAppointmentSequenceNo
	,OnMMRList
	,ImmunisationsUpToDate
)
select
	 EntitySourceUniqueID = EntSys
	,ScheduleCode = SCHEDULE_CODE
	,ClinicCode = CLINIC_CODE
	,OutstandingAppointmentDate = OUTSTANDING_APPT_DATE
	,OutstandingAppointmentSequenceNo = OUTSTANDING_APPT_SEQ
	,OnMMRList = ON_MMR_LIST
	,ImmunisationsUpToDate = IMMS_UPTO_DATE
from
	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_CLIENT_SUMMARY


--EventConsent

truncate table ChildHealth.EventConsent

insert
into
	ChildHealth.EventConsent
(
	 EntitySourceUniqueID
	,AntigenCode
	,ConsentCode
	,ConsentDate
	,ReasonForNegativeConsentCode
)
select
	 EntitySourceUniqueID = EntSys
	,AntigenCode = ANTIGEN_CODE
	,ConsentCode = INTENT
	,ConsentDate = DATE_OF_INTENT
	,ReasonForNegativeConsentCode =
		case
		when left(chsVI_CVP_INTENT.REASON_FOR_NEG_CODE, 1) collate latin1_general_bin like '[a-z]'
		then chsVI_CVP_INTENT.REASON_FOR_NEG_CODE + 'Old'
		else chsVI_CVP_INTENT.REASON_FOR_NEG_CODE
		end
from
	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_INTENT


--ExamConsent

truncate table ChildHealth.ExamConsent

insert
into
	ChildHealth.ExamConsent
(
	 EntitySourceUniqueID
	,ExamCode
	,ExamTypeCode
	,ConsentCode
	,ConsentDate
	,ReasonForNegativeConsentCode
)
select
	 EntitySourceUniqueID = EntSys
	,ExamCode = EXAM_CODE
	,ExamTypeCode = TYPE
	,ConsentCode = CONSENT
	,ConsentDate = DATE_OF_CONSENT
	,ReasonForNegativeConsentCode =
		case
		when left(chsSH_EXAM_CONSENTS.RESFUSAL_CODE, 1) collate latin1_general_bin like '[a-z]'
		then chsSH_EXAM_CONSENTS.RESFUSAL_CODE + 'Old'
		else chsSH_EXAM_CONSENTS.RESFUSAL_CODE
		end
from
	[$(ChildHealth_CarePlus)].SQLUser.chsSH_EXAM_CONSENTS


--ExamScheduled

truncate table ChildHealth.ExamScheduled

insert
into
	ChildHealth.ExamScheduled
(
	 EntitySourceUniqueID
	,ExamCode
	,DueDate
	,VenueCode
)
select
	 EntitySourceUniqueID = chsPS_NEXT_PS_EXAM_INDEX.EntSys
	,ExamCode = chsPS_NEXT_PS_EXAM_INDEX.NEXT_PS_EXAM1_CODE
	,DueDate = chsPS_NEXT_PS_EXAM_INDEX.NEXT_PS_EXAM1_DUE_DATE
	,VenueCode = chsPS_NEXT_PS_EXAM_INDEX.NEXT_PS_EXAM1_VENUE
from
	[$(ChildHealth_CarePlus)].SQLUser.chsPS_NEXT_PS_EXAM_INDEX


--AdviceLog

truncate table ChildHealth.AdviceLog

insert
into
	ChildHealth.AdviceLog
(
	 EntitySourceUniqueID
	,AdviceDate
	,AdviceIndex
	,AdviceGiven
	,OnBehalfOfCode
)
select 
	 EntitySourceUniqueID = EntSys
	,AdviceDate = ADVICE_DATE
	,row_number() over (partition by EntSys,ADVICE_DATE order by EntSys,ADVICE_DATE)
	,AdviceGiven = ADVICE_GIVEN
	,OnBehalfOfCode = ON_BEHALF_OF_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.chs_ADVICE_LOG


--LookedAfterCarerDetail

truncate table ChildHealth.LookedAfterCarerDetail

insert
into
	ChildHealth.LookedAfterCarerDetail
(
	 EntitySourceUniqueID
	,LookedAfterDate
	,StartDate
	,EndDate
	,CarerDetail
)
select distinct
	 EntitySourceUniqueID = EntSys
	,LookedAfterDate = DATE_CHILD_BECAME_LAC
	,StartDate = START_DATE
	,EndDate = END_DATE
	,CarerDetail = CARER_DETAILS
from
	[$(ChildHealth_CarePlus)].SQLUser.chs_LAC_CARER_DETAILS


--LookedAfterLegalStatus

truncate table ChildHealth.LookedAfterLegalStatus

insert
into
	ChildHealth.LookedAfterLegalStatus
(
	 EntitySourceUniqueID
	,LookedAfterDate
	,StartDate
	,EndDate
	,LegalStatusCode
)
select
	 EntitySourceUniqueID = EntSys
	,LookedAfterDate = DATE_CHILD_BECAME_LAC
	,StartDate = DATE_IDENTIFIED
	,EndDate = END_DATE
	,LegalStatusCode = LEGAL_STATUS_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.chs_LAC_LEGAL_STATUS


--LookedAfterAssessmentSummary

truncate table ChildHealth.LookedAfterAssessmentSummary

insert
into
	ChildHealth.LookedAfterAssessmentSummary
(
	 EntitySourceUniqueID
	,AssessmentTypeCode
	,RequestDate
	,ChildSeen
	,FirstAppointmentDate
	,SecondAppointmentDate
	,ThirdAppointmentDate
	,EscLetterSentDate
	,KeyInformationRequestDate
	,PeperworkCompleteDate
	,ReportReceivedDate
	,TeamInformedDate
	,KeyInformationReceived
	,PaperworkProcessed
	,PaperworkScanned
	,DelayReason
	,RequestSentTo
	,TeamInformed
)
select distinct -- added distinct 2/4/2013 due to duplicate records in the source dataset
	 EntitySourceUniqueID = EntSys
	,AssessmentTypeCode = ASSESSMENT_TYPE_CODE
	,RequestDate = REQUEST_DATE
	,ChildSeen = CHILD_SEEN
	,FirstAppointmentDate = DATE_FIRST_APPOINTMENT
	,SecondAppointmentDate = DATE_SECOND_APPOINTMENT
	,ThirdAppointmentDate = DATE_THIRD_APPOINTMENT
	,EscLetterSentDate = DATE_ESC_LETTER_SENT
	,KeyInformationRequestDate = DATE_KEY_INFO_REQUESTED
	,PeperworkCompleteDate = DATE_PAPERWORK_COMPLETE
	,ReportReceivedDate = DATE_REPORT_RECEIVED
	,TeamInformedDate = DATE_TEAM_INFORMED
	,KeyInformationReceived = KEY_INFORMATION_RECEIVED
	,PaperworkProcessed = PAPERWORK_PROCESSED
	,PaperworkScanned = PAPERWORK_SCANNED
	,DelayReason = REASON_FOR_DELAY
	,RequestSentTo = REQUEST_SENT_TO
	,TeamInformed = TEAM_INFORMED
from
	[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_ASSESSMENT_SUMMARY

--LookedAfter

truncate table ChildHealth.LookedAfter

insert
into
	ChildHealth.LookedAfter
(
	 EntitySourceUniqueID
	,LookedAfterDate
	,LookedAfterRemovalReasonCode
	,ConfirmedDate
	,DeliveryDate
	,HealthNotifiedDate
	,RemovedDate
	,ImmsUptoDateDecisionDate
	,ImmsUptoDateClinician
	,ImmsUptoDateFlag
	,LookedAfterStatusCode
	,LastDentalExamDate
	,OriginatingPCTCode
	,PlacementTypeCode
	,PayingPCTCode
	,PregnancyOutcomeCode
	,ResponsibleForPregnancyCode
	,ResidingPCTCode
	,SocialWorker
	,SocialServicesDepartmentCode
	,Supervision
	,SupervisionStartDate
	,SupervisionEndDate
	,UnaccompaniedAsylumSeeker
	,LeadClinicianCode
	,LeadClinicianTypeCode
)
select
	 EntitySourceUniqueID = EntSys
	,LookedAfterDate = DATE_CHILD_BECAME_LAC
	,LookedAfterRemovalReasonCode = REASON_REMOVED_FROM_LAC_CODE
	,ConfirmedDate = DATE_CONFIRMED
	,DeliveryDate = DELIVERY_DATE
	,HealthNotifiedDate = DATE_HEALTH_NOTIFIED_CHILD_IS_LAC
	,RemovedDate = DATE_REMOVED_FROM_LAC
	,ImmsUptoDateDecisionDate = IMMS_UPTO_DATE_DECISION_DATE
	,ImmsUptoDateClinician = IMMS_UPTO_DATE_CLINICIAN
	,ImmsUptoDateFlag = IMMS_UPTO_DATE
	,LookedAfterStatusCode = LAC_STATUS_CODE
	,LastDentalExamDate = LAST_DENTAL_EXAM_DATE
	,OriginatingPCTCode = ORIGINATING_PCT_CODE
	,PlacementTypeCode = PLACEMENT_TYPE_CODE
	,PayingPCTCode = PAYING_PCT_CODE
	,PregnancyOutcomeCode = PREGNANCY_OUTCOME_CODE
	,ResponsibleForPregnancyCode = RESPONSIBLE_FOR_PREGNANCY
	,ResidingPCTCode = RESIDING_PCT_CODE 
	,SocialWorker = SOCIAL_WORKER
	,SocialServicesDepartmentCode = SOCIAL_SERVICES_DEPARTMENT_CODE
	,Supervision = SUPERVISION
	,SupervisionStartDate = SUPERVISION_START_DATE
	,SupervisionEndDate = SUPERVISION_END_DATE
	,UnaccompaniedAsylumSeeker = UNACCOMPANIED_ASYLUM_SEEKER
	,LeadClinicianCode = LEAD_CLINICIAN_CODE
	,LeadClinicianTypeCode = LEAD_CLINICIAN_TYPE_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_STATUS


--Meeting

truncate table ChildHealth.Meeting

insert
into
	ChildHealth.Meeting
(
	 EntitySourceUniqueID
	,MeetingTypeCode
	,MeetingDate
	,MeetingTime
	,LocationCode
)
select
	 EntitySourceUniqueID = EntSys
	,MeetingTypeCode = MEETING_TYPE_CODE
	,MeetingDate = MEETING_DATE
	,MeetingTime = MEETING_TIME
	,LocationCode = MEETING_LOCATION_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.chs_MEETING_DETAILS


--ChildProtectionPlan

truncate table ChildHealth.ChildProtectionPlan

insert
into
	ChildHealth.ChildProtectionPlan
(
	 EntitySourceUniqueID
	,PlacedOnPlanDate
	,ReasonRemovedFromPlanCode
	,ResponsibleLocalAuthorityCode
	,SocialWorker
	,RemovedFromPlanDate
	,SocialServicesDepartmentCode
	,SupervisionEndDate
	,SupervisionStartDate
	,Supervision
	,LeadClinicianCode
	,LeadClinicianTypeCode
	,VulnerabilityFactorCode
)
select
	 EntitySourceUniqueID = EntSys
	,PlacedOnPlanDate = DATE_PLACED_ON_PLAN
	,ReasonRemovedFromPlanCode = REASON_REMOVED_FROM_PLAN_CODE
	,ResponsibleLocalAuthorityCode = RESPONSIBLE_LOCAL_AUTHORITY
	,SocialWorker = SOCIAL_WORKER
	,RemovedFromPlanDate = DATE_REMOVED_FROM_PLAN
	,SocialServicesDepartmentCode = SOCIAL_SERVICES_DEPARTMENT_CODE
	,SupervisionEndDate = SUPERVISION_END_DATE
	,SupervisionStartDate = SUPERVISION_START_DATE
	,Supervision = SUPERVISION
	,LeadClinicianCode = LEAD_CLINICIAN_CODE
	,LeadClinicianTypeCode = LEAD_CLINICIAN_TYPE_CODE
	,VulnerabilityFactorCode = VULNERABILITY_FACTOR
from
	[$(ChildHealth_CarePlus)].SQLUser.chsCP_CHILD_PROTECTION_DETAILS


--ChildProtectionCauseForConcern

truncate table ChildHealth.ChildProtectionCauseForConcern

insert
into
	ChildHealth.ChildProtectionCauseForConcern
(
	 EntitySourceUniqueID
	,IdentifiedDate
	,SequenceNo
	,CauseForConcernCode
	,EndDate

)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,IdentifiedDate = DATE_IDENTIFIED
	,SequenceNo = SEQUENCE_NUMBER
	,CauseForConcernCode = CAUSE_FOR_CONCERN_CODE
	,EndDate = END_DATE
from
	[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_CAUSE_FOR_CONCERN



--ChildProtectionDomesticAbuse

truncate table ChildHealth.ChildProtectionDomesticAbuse

insert
into
	ChildHealth.ChildProtectionDomesticAbuse
(
	 EntitySourceUniqueID
	,IncidentDate
	,SequenceNo
	,CAFDeclined
	,CAFRecommended
	,CrimeNumber
	,LevelCode
	,OutcomeCode
	,ReasonDeclined
	,HealthProfessionalCode
	,HealthProfessionalTypeCode
)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,IncidentDate = DATE_OF_INCIDENT
	,SequenceNo = SEQUENCE_NUMBER
	,CAFDeclined = CAF_DECLINED
	,CAFRecommended = CAF_RECOMMENDED
	,CrimeNumber = CRIME_NUMBER
	,LevelCode = LEVEL_CODE
	,OutcomeCode = OUTCOME_CODE
	,ReasonDeclined = REASON_DECLINED
	,HealthProfessionalCode = HEALTH_PROFESSIONAL_CODE
	,HealthProfessionalTypeCode = HEALTH_PROFESSIONAL_TYPE
from
	[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_DOMESTIC_ABUSE


--ChildProtectionMappa

truncate table ChildHealth.ChildProtectionMappa

insert
into
	ChildHealth.ChildProtectionMappa
(
	 EntitySourceUniqueID
	,IncidentDate
	,SequenceNo
	,EndDate
	,MappaCategoryCode
)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,IncidentDate = DATE_REFERRED
	,SequenceNo = SEQUENCE_NUMBER
	,EndDate = END_DATE
	,MappaCategoryCode = MAPPA_CATEGORY_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_MAPPA



--ChildProtectionMarac

truncate table ChildHealth.ChildProtectionMarac

insert
into
	ChildHealth.ChildProtectionMarac
(
	 EntitySourceUniqueID
	,ReferredDate
	,SequenceNo
	,EndDate
	,MaracReasonForReferralCode
)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,ReferredDate = DATE_REFERRED
	,SequenceNo = SEQUENCE_NUMBER
	,EndDate = END_DATE
	,MaracReasonForReferralCode = REASON_FOR_REFERRAL_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_MARAC



--ChildProtectionNeeds

truncate table ChildHealth.ChildProtectionNeeds

insert
into
	ChildHealth.ChildProtectionNeeds
(
	 EntitySourceUniqueID
	,IdentifiedDate
	,SequenceNo
	,EndDate
	,NeedCode
)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,IdentifiedDate = DATE_IDENTIFIED
	,SequenceNo = SEQUENCE_NUMBER
	,EndDate = END_DATE
	,NeedCode = NEED_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_NEEDS


--ChildProtectionStatus

truncate table ChildHealth.ChildProtectionStatus

insert
into
	ChildHealth.ChildProtectionStatus
(
	 EntitySourceUniqueID
	,PlacedOnPlanDate
	,RegistrationDate
	,SequenceNo
	,EndDate
	,StatusCode
)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,PlacedOnPlanDate = DATE_PLACED_ON_PLAN
	,RegistrationDate = REGISTRATION_DATE
	,SequenceNo = SEQUENCE_NUMBER
	,EndDate = END_DATE
	,StatusCode = STATUS_CODE
from
	[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_STATUS



--Bloodspot

truncate table ChildHealth.Bloodspot

insert
into
	ChildHealth.Bloodspot
(
	 EntitySourceUniqueID
	,SpecimenDate
	,SequenceNo
	,TestCode
	,TestResultCode
	,ResultLetterDate
	,SpecimenResultDate
	,ResultReceivedDate
	,ResultEnteredDate
	,NotKnownDateFlag
	,AcknowledgedDate
	,SentDate
	,TestGivenCode
	,BadSpotIndicator
	,ReasonForRepeatCode
	,LabNumber
	,SpecimenComment
	,TestComment
)
select
	 EntitySourceUniqueID = EntSys
	,SpecimenDate = SPECIMEN_DATE
	,SequenceNo = SEQ_NO
	,TestCode = TEST_CODE
	,TestResultCode = TEST_RES_CODE
	,ResultLetterDate = ON_PCMMTP_PRINT
	,SpecimenResultDate = SPEC_RESULT_DATE
	,ResultReceivedDate = RESULT_REC_DATE
	,ResultEnteredDate = DATE_RESULT_ENTERED
	,NotKnownDateFlag = NK_DATE_FLAG
	,AcknowledgedDate = DATE_ACKNOWLEDGED
	,SentDate = DATE_SENT
	,TestGivenCode = TEST_GIVEN
	,BadSpotIndicator = BAD_SPOT_IND
	,ReasonForRepeatCode = REASON_CODE
	,LabNumber = LAB_NUMBER
	,SpecimenComment = SPECIMEN_COMMENT
	,TestComment = TEST_FREE_TEXT_COMMENT
from
	[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_ROUTINE_TESTS

--ChildConsent

truncate table ChildHealth.ChildConsent

insert
into
	ChildHealth.ChildConsent
(
	 EntitySourceUniqueID
	,SystemOrDepartmentCode
	,ConsentDate
	,ConsentSequenceNo
	,ConsentCode
	,PersonGivingConsentCode
	,PersonGivingConsentTypeCode
	,DissentionCode
)
select
	 EntitySourceUniqueID = SYSTEM_NUMBER
	,SystemOrDepartmentCode = SYSTEM_DEPARTMENT_CODE
	,ConsentDate = CONSENT_DATE
	,ConsentSequenceNo = CONSENT_SEQUENCE
	,ConsentCode = CONSENT_CODE
	,PersonGivingConsentCode = PERSON_GIVING_CONSENT
	,PersonGivingConsentTypeCode = TYPE_OF_PERSON_GIVING_CONSENT
	,DissentionCode = DISSENTION_INDICATOR
from
	[$(ChildHealth_CarePlus)].SQLUser.DATA_CONSENT


--Death

truncate table ChildHealth.Death

insert
into
	ChildHealth.Death
(
	 EntitySourceUniqueID
	,DateOfDeath
	,TimeOfDeath
	,DeathPlaceCode
	,CauseOfDeath
	,CauseOfDeathCode1
	,CauseOfDeathCode2
	,CauseOfDeathCode3
	,CauseOfDeathCode4
	,CauseOfDeathCode5
	,CauseOfDeathCode6
	,CauseOfDeathCode7
	,CauseOfDeathCode8
	,DeathNotifiedByCode
	,DeathNotifiedDate
	,RapidResponseClinicianCode
	,RapidResponseTeamInvolvedCode
	,TypeOfDeathCode
)
select
	 EntitySourceUniqueID = EntSys
	,DateOfDeath = DATE_OF_DEATH
	,TimeOfDeath = TIME_OF_DEATH
	,DeathPlaceCode = PLACE_OF_DEATH
	,CauseOfDeath = CAUSE_OF_DEATH_TEXT
	,CauseOfDeathCode1 = CAUSE_OF_DEATH_CODE1
	,CauseOfDeathCode2 = CAUSE_OF_DEATH_CODE2
	,CauseOfDeathCode3 = CAUSE_OF_DEATH_CODE3
	,CauseOfDeathCode4 = CAUSE_OF_DEATH_CODE4
	,CauseOfDeathCode5 = CAUSE_OF_DEATH_CODE5
	,CauseOfDeathCode6 = CAUSE_OF_DEATH_CODE6
	,CauseOfDeathCode7 = CAUSE_OF_DEATH_CODE7
	,CauseOfDeathCode8 = CAUSE_OF_DEATH_CODE8
	,DeathNotifiedByCode = NOTIFIED_BY_CODE
	,DeathNotifiedDate = NOTIFIED_DATE
	,RapidResponseClinicianCode = RAPID_RESPONSE_CLINICIAN
	,RapidResponseTeamInvolvedCode = RAPID_RESPONSE_TEAM_INVOLVED
	,TypeOfDeathCode = TYPD
from
	[$(ChildHealth_CarePlus)].SQLUser.chsREG_DEATH_DETAILS



--Derived Reference tables

--HealthVisitor
MERGE
	ChildHealth.HealthVisitor target

USING
	(
	select distinct
		 chsREG_CMF_BIRTH_DETAILS.HEALTH_VISITOR_CODE
		,chsREG_CMF_BIRTH_DETAILS.HEALTH_VISITOR_NAME
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BIRTH_DETAILS
	where
		chsREG_CMF_BIRTH_DETAILS.HEALTH_VISITOR_CODE is not null
	) source
	(
	 HealthVisitorCode
	,HealthVisitor
	)
ON	target.HealthVisitorCode = source.HealthVisitorCode

WHEN MATCHED THEN 
    UPDATE SET HealthVisitor = source.HealthVisitor

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 HealthVisitorCode
		,HealthVisitor
		)
    VALUES
		(
		 source.HealthVisitorCode
		,source.HealthVisitor
		)
;


--BirthPlace
MERGE
	ChildHealth.BirthPlace target

USING
	(
	select distinct
		 BirthPlaceCode =chsREG_CMF_BIRTH_DETAILS.BIRTH_PLACE_CODE
		,BirthPlace = chsREG_CMF_BIRTH_DETAILS.BIRTH_PLACE_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BIRTH_DETAILS
	where
		chsREG_CMF_BIRTH_DETAILS.BIRTH_PLACE_CODE is not null
	) source
	(
	 BirthPlaceCode
	,BirthPlace
	)
ON	target.BirthPlaceCode = source.BirthPlaceCode

WHEN MATCHED THEN 
    UPDATE SET BirthPlace = source.BirthPlace

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 BirthPlaceCode
		,BirthPlace
		)
    VALUES
		(
		 source.BirthPlaceCode
		,source.BirthPlace
		)
;


--HEPBStatus
MERGE
	ChildHealth.HEPBStatus target

USING
	(
	select distinct
		 HEPBStatusCode = chsREG_CMF_BIRTH_DETAILS.MOTHERS_HEP_B_STAT_CODE
		,HEPBStatus = chsREG_CMF_BIRTH_DETAILS.MOTHERS_HEP_B_STATUS
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BIRTH_DETAILS
	where
		chsREG_CMF_BIRTH_DETAILS.MOTHERS_HEP_B_STAT_CODE is not null
	) source
	(
	 HEPBStatusCode
	,HEPBStatus
	)
ON	target.HEPBStatusCode = source.HEPBStatusCode

WHEN MATCHED THEN 
    UPDATE SET HEPBStatus = source.HEPBStatus

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 HEPBStatusCode
		,HEPBStatus
		)
    VALUES
		(
		 source.HEPBStatusCode
		,source.HEPBStatus
		)
;


--BreastFeedingResult
MERGE
	ChildHealth.BreastFeedingResult target

USING
	(
	select distinct
		 REG_BREAST_FEEDING.BREASTFEEDINGRESULT
		,REG_BREAST_FEEDING.BREASTFEEDINGRESULTDESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.REG_BREAST_FEEDING
	where
		REG_BREAST_FEEDING.BREASTFEEDINGRESULT <> ''
	) source
	(
	 BreastFeedingResultCode
	,BreastFeedingResult
	)
ON	target.BreastFeedingResultCode = source.BreastFeedingResultCode

WHEN MATCHED THEN 
    UPDATE SET BreastFeedingResult = source.BreastFeedingResult

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 BreastFeedingResultCode
		,BreastFeedingResult
		)
    VALUES
		(
		 source.BreastFeedingResultCode
		,source.BreastFeedingResult
		)
;


--BreastFeedingStage
MERGE
	ChildHealth.BreastFeedingStage target

USING
	(
	select distinct
		 BreastFeedingStageNo = REG_BREAST_FEEDING.STAGENUMBER
		,BreastFeedingStage = REG_BREAST_FEEDING.STAGE_NUMBER_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.REG_BREAST_FEEDING
	where
		REG_BREAST_FEEDING.STAGENUMBER <> ''
	) source
	(
	 BreastFeedingStageNo
	,BreastFeedingStage
	)
ON	target.BreastFeedingStageNo = source.BreastFeedingStageNo

WHEN MATCHED THEN 
    UPDATE SET BreastFeedingStage = source.BreastFeedingStage

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 BreastFeedingStageNo
		,BreastFeedingStage
		)
    VALUES
		(
		 source.BreastFeedingStageNo
		,source.BreastFeedingStage
		)
;


--Flag
MERGE
	ChildHealth.Flag target

USING
	(
	select distinct
		 chsREG_CLIENT_FLAGS.FLAG
		,chsREG_CLIENT_FLAGS.FLAG_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CLIENT_FLAGS
	) source
	(
	 FlagCode
	,Flag
	)
ON	target.FlagCode = source.FlagCode

WHEN MATCHED THEN 
    UPDATE SET Flag = source.Flag

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 FlagCode
		,Flag
		)
    VALUES
		(
		 source.FlagCode
		,source.Flag
		)
;


--FlagReasonForRemoval
MERGE
	ChildHealth.FlagReasonForRemoval target

USING
	(
	select distinct
		 chsREG_CLIENT_FLAGS.REASON_FOR_REMOVAL_CODE
		,chsREG_CLIENT_FLAGS.REASON_FOR_REMOVAL_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CLIENT_FLAGS
	where
		chsREG_CLIENT_FLAGS.REASON_FOR_REMOVAL_CODE is not null
	) source
	(
	 FlagReasonForRemovalCode
	,FlagReasonForRemoval
	)
ON	target.FlagReasonForRemovalCode = source.FlagReasonForRemovalCode

WHEN MATCHED THEN 
    UPDATE SET FlagReasonForRemoval = source.FlagReasonForRemoval

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 FlagReasonForRemovalCode
		,FlagReasonForRemoval
		)
    VALUES
		(
		 source.FlagReasonForRemovalCode
		,source.FlagReasonForRemoval
		)
;



--RegistrationStatus
MERGE
	ChildHealth.RegistrationStatus target

USING
	(
	select distinct
		 chsREG_CH_STATUS_HIST.REG_STATUS_CODE
		,chsREG_CH_STATUS_HIST.REG_STATUS_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CH_STATUS_HIST
	where
		chsREG_CH_STATUS_HIST.REG_STATUS_CODE is not null
	) source
	(
	 RegistrationStatusCode
	,RegistrationStatus
	)
ON	target.RegistrationStatusCode = source.RegistrationStatusCode

WHEN MATCHED THEN 
    UPDATE SET RegistrationStatus = source.RegistrationStatus

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 RegistrationStatusCode
		,RegistrationStatus
		)
    VALUES
		(
		 source.RegistrationStatusCode
		,source.RegistrationStatus
		)
;


--ReferredTo
MERGE
	ChildHealth.ReferredTo target

USING
	(
	select distinct
		 chsPS_EXAM_HIST_REFERRALS.REFERRED_TO_CODE
		,chsPS_EXAM_HIST_REFERRALS.REFERRED_TO_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_REFERRALS
	) source
	(
	 ReferredToCode
	,ReferredTo
	)
ON	target.ReferredToCode = source.ReferredToCode

WHEN MATCHED THEN 
    UPDATE SET ReferredTo = source.ReferredTo

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReferredToCode
		,ReferredTo
		)
    VALUES
		(
		 source.ReferredToCode
		,source.ReferredTo
		)
;


--Exam
MERGE
	ChildHealth.Exam target

USING
	(
	select distinct
		 ExamCode
		,Exam
	from
		(
			select distinct
				 ExamCode = chsPS_EXAM_HIST_REFERRALS.EXAM_CODE
				,Exam = chsPS_EXAM_HIST_REFERRALS.EXAM_DESC
			from
				[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_REFERRALS

			union all

			select distinct
				 chsPS_EXAM_HIST_DETS.EXAM_CODE
				,chsPS_EXAM_HIST_DETS.EXAM_DESC
			from
				[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS

			union all

			select distinct
				 chsPS_EXAM_HIST_DETS.RECALL_EXAM
				,chsPS_EXAM_HIST_DETS.RECALL_EXAM_DESC
			from
				[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS
			where
				chsPS_EXAM_HIST_DETS.RECALL_EXAM is not null
		) Exam
	) source
	(
	 ExamCode
	,Exam
	)
ON	target.ExamCode = source.ExamCode

WHEN MATCHED THEN 
    UPDATE SET Exam = source.Exam

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ExamCode
		,Exam
		)
    VALUES
		(
		 source.ExamCode
		,source.Exam
		)
;


--Test
MERGE
	ChildHealth.Test target

USING
	(
	select distinct
		 chsPS_EXAM_HIST_TEST_RESULTS.TEST_CODE
		,chsPS_EXAM_HIST_TEST_RESULTS.TEST_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_TEST_RESULTS
	) source
	(
	 TestCode
	,Test
	)
ON	target.TestCode = source.TestCode

WHEN MATCHED THEN 
    UPDATE SET Test = source.Test

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 TestCode
		,Test
		)
    VALUES
		(
		 source.TestCode
		,source.Test
		)
;

--BirthState
MERGE
	ChildHealth.BirthState target

USING
	(
	select distinct
		 chsREG_CMF_BIRTH_DETAILS.BIRTH_STATE_CODE
		,chsREG_CMF_BIRTH_DETAILS.BIRTH_STATE_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_BIRTH_DETAILS
	where
		chsREG_CMF_BIRTH_DETAILS.BIRTH_STATE_CODE is not null
	and	chsREG_CMF_BIRTH_DETAILS.BIRTH_STATE_CODE <> ''
	) source
	(
	 BirthStateCode
	,BirthState
	)
ON	target.BirthStateCode = source.BirthStateCode

WHEN MATCHED THEN 
    UPDATE SET BirthState = source.BirthState

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 BirthStateCode
		,BirthState
		)
    VALUES
		(
		 source.BirthStateCode
		,source.BirthState
		)
;


--Clinic
MERGE
	ChildHealth.Clinic target

USING
	(
	select distinct
		 ClinicCode = chsPS_EXAM_HIST_DETS.CLINIC_CODE
		,Clinic = chsPS_EXAM_HIST_DETS.CLINIC_NAME
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS
	where
		chsPS_EXAM_HIST_DETS.CLINIC_CODE is not null

	--union

	--select distinct
	--	 VenueTypeCode = chsVI_CVP_EVENT_DETAILS.CLINIC_CODE
	--	,VenueType = chsVI_CVP_EVENT_DETAILS.CLINIC_NAME
	--from
	--	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	--where
	--	chsVI_CVP_EVENT_DETAILS.CLINIC_CODE is not null
	) source
	(
	 ClinicCode
	,Clinic
	)
ON	target.ClinicCode = source.ClinicCode

WHEN MATCHED THEN 
    UPDATE SET Clinic = source.Clinic

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ClinicCode
		,Clinic
		)
    VALUES
		(
		 source.ClinicCode
		,source.Clinic
		)
;

--ReasonForDNA
MERGE
	ChildHealth.ReasonForDNA target

USING
	(
	select distinct
		 ReasonForDNACode = chsPS_EXAM_HIST_DETS.REASON_FOR_DNA
		,ReasonForDNA = chsPS_EXAM_HIST_DETS.REASON_FOR_DNA_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS
	where
		chsPS_EXAM_HIST_DETS.REASON_FOR_DNA is not null
	) source
	(
	 ReasonForDNACode
	,ReasonForDNA
	)
ON	target.ReasonForDNACode = source.ReasonForDNACode

WHEN MATCHED THEN 
    UPDATE SET ReasonForDNA = source.ReasonForDNA

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonForDNACode
		,ReasonForDNA
		)
    VALUES
		(
		 source.ReasonForDNACode
		,source.ReasonForDNA
		)
;



--ReasonNotExamined
MERGE
	ChildHealth.ReasonNotExamined target

USING
	(
	select distinct
		 ReasonNotExaminedCode = chsPS_EXAM_HIST_DETS.REASON_NOT_EXAMINED
		,ReasonNotExamined = chsPS_EXAM_HIST_DETS.REASON_NOT_EXAMINED_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS
	where
		chsPS_EXAM_HIST_DETS.REASON_NOT_EXAMINED is not null
	) source
	(
	 ReasonNotExaminedCode
	,ReasonNotExamined
	)
ON	target.ReasonNotExaminedCode = source.ReasonNotExaminedCode

WHEN MATCHED THEN 
    UPDATE SET ReasonNotExamined = source.ReasonNotExamined

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonNotExaminedCode
		,ReasonNotExamined
		)
    VALUES
		(
		 source.ReasonNotExaminedCode
		,source.ReasonNotExamined
		)
;


--VenueType
MERGE
	ChildHealth.VenueType target

USING
	(
	select distinct
		 VenueTypeCode = chsPS_EXAM_HIST_DETS.VENUE_TYPE_CODE
		,VenueType = chsPS_EXAM_HIST_DETS.VENUE_TYPE_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS
	where
		chsPS_EXAM_HIST_DETS.VENUE_TYPE_CODE is not null

	union

	select distinct
		 VenueTypeCode = chsVI_CVP_EVENT_DETAILS.VENUE_TYPE_CODE
		,VenueType = chsVI_CVP_EVENT_DETAILS.VENUE_TYPE_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	where
		chsVI_CVP_EVENT_DETAILS.VENUE_TYPE_CODE is not null

	union

	select distinct
		 VenueTypeCode = AE_ATTENDANCES.VENUE_TYPE
		,VenueType = AE_ATTENDANCES.VENUE_TYPE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.AE_ATTENDANCES
	where
		AE_ATTENDANCES.VENUE_TYPE is not null

	) source
	(
	 VenueTypeCode
	,VenueType
	)
ON	target.VenueTypeCode = source.VenueTypeCode

WHEN MATCHED THEN 
    UPDATE SET VenueType = source.VenueType

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 VenueTypeCode
		,VenueType
		)
    VALUES
		(
		 source.VenueTypeCode
		,source.VenueType
		)
;


--ModeOfEntry
MERGE
	ChildHealth.ModeOfEntry target

USING
	(
	select distinct
		 ModeOfEntryCode = chsVI_CVP_EVENT_DETAILS.MODE_OF_ENTRY
		,ModeOfEntry = chsVI_CVP_EVENT_DETAILS.MODE_OF_ENTRY_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	where
		chsVI_CVP_EVENT_DETAILS.MODE_OF_ENTRY is not null
	) source
	(
	 ModeOfEntryCode
	,ModeOfEntry
	)
ON	target.ModeOfEntryCode = source.ModeOfEntryCode

WHEN MATCHED THEN 
    UPDATE SET ModeOfEntry = source.ModeOfEntry

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ModeOfEntryCode
		,ModeOfEntry
		)
    VALUES
		(
		 source.ModeOfEntryCode
		,source.ModeOfEntry
		)
;


--Scheduled
MERGE
	ChildHealth.Scheduled target

USING
	(
	select distinct
		 ScheduledCode = chsVI_CVP_EVENT_DETAILS.SCHEDULED
		,Scheduled = chsVI_CVP_EVENT_DETAILS.SCHEDULED_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	where
		chsVI_CVP_EVENT_DETAILS.SCHEDULED is not null
	) source
	(
	 ScheduledCode
	,Scheduled
	)
ON	target.ScheduledCode = source.ScheduledCode

WHEN MATCHED THEN 
    UPDATE SET Scheduled = source.Scheduled

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ScheduledCode
		,Scheduled
		)
    VALUES
		(
		 source.ScheduledCode
		,source.Scheduled
		)
;


--ReasonNoVaccineGiven
MERGE
	ChildHealth.ReasonNoVaccineGiven target

USING
	(
	select distinct
		 ReasonNoVaccineGivenCode = chsVI_CVP_EVENT_DETAILS.REASON_NONE_GIVEN_CODE
		,ReasonNoVaccineGiven = chsVI_CVP_EVENT_DETAILS.REASON_NONE_GIVEN_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	where
		chsVI_CVP_EVENT_DETAILS.REASON_NONE_GIVEN_CODE is not null
	) source
	(
	 ReasonNoVaccineGivenCode
	,ReasonNoVaccineGiven
	)
ON	target.ReasonNoVaccineGivenCode = source.ReasonNoVaccineGivenCode

WHEN MATCHED THEN 
    UPDATE SET ReasonNoVaccineGiven = source.ReasonNoVaccineGiven

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonNoVaccineGivenCode
		,ReasonNoVaccineGiven
		)
    VALUES
		(
		 source.ReasonNoVaccineGivenCode
		,source.ReasonNoVaccineGiven
		)
;

--Antigen
MERGE
	ChildHealth.Antigen target

USING
	(
	select distinct
		 AntigenCode = chsVI_CVP_ANTIGEN_DETAILS.ANTIGEN_VALUE
		,Antigen = chsVI_CVP_ANTIGEN_DETAILS.ANTIGEN_NAME
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_ANTIGEN_DETAILS
	where
		chsVI_CVP_ANTIGEN_DETAILS.ANTIGEN_VALUE is not null

	UNION

	select distinct
		 AntigenCode = chsVI_CVP_EVENT_DETAILS.SCHEDULED_ANTIGENS
		,Antigen = chsVI_CVP_EVENT_DETAILS.SCHEDULED_ANTIGENS_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	where
		chsVI_CVP_EVENT_DETAILS.SCHEDULED_ANTIGENS is not null
	) source
	(
	 AntigenCode
	,Antigen
	)
ON	target.AntigenCode = source.AntigenCode

WHEN MATCHED THEN 
    UPDATE SET Antigen = source.Antigen

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 AntigenCode
		,Antigen
		)
    VALUES
		(
		 source.AntigenCode
		,source.Antigen
		)
;

--BCGType
MERGE
	ChildHealth.BCGType target

USING
	(
	select distinct
		 BCGTypeCode = chsVI_CVP_EVENT_DETAILS.BCG_TYPE
		,BCGType = chsVI_CVP_EVENT_DETAILS.BCG_TYPE
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	where
		chsVI_CVP_EVENT_DETAILS.BCG_TYPE is not null
	) source
	(
	 BCGTypeCode
	,BCGType
	)
ON	target.BCGTypeCode = source.BCGTypeCode

WHEN MATCHED THEN 
    UPDATE SET BCGType = source.BCGType

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 BCGTypeCode
		,BCGType
		)
    VALUES
		(
		 source.BCGTypeCode
		,source.BCGType
		)
;


--ReasonForNegativeConsent
MERGE
	ChildHealth.ReasonForNegativeConsent target

USING
	(
	select distinct
		 ReasonForNegativeConsentCode =
			case
			when left(chsVI_CVP_INTENT.REASON_FOR_NEG_CODE, 1) collate latin1_general_bin like '[a-z]'
			then chsVI_CVP_INTENT.REASON_FOR_NEG_CODE + 'Old'
			else chsVI_CVP_INTENT.REASON_FOR_NEG_CODE
			end

		,ReasonForNegativeConsent = chsVI_CVP_INTENT.REASON_FOR_NEGATIVE_INTENT
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_INTENT
	where
		chsVI_CVP_INTENT.REASON_FOR_NEG_CODE is not null
	) source
	(
	 ReasonForNegativeConsentCode
	,ReasonForNegativeConsent
	)
ON	target.ReasonForNegativeConsentCode = source.ReasonForNegativeConsentCode

WHEN MATCHED THEN 
    UPDATE SET ReasonForNegativeConsent = source.ReasonForNegativeConsent

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonForNegativeConsentCode
		,ReasonForNegativeConsent
		)
    VALUES
		(
		 source.ReasonForNegativeConsentCode
		,source.ReasonForNegativeConsent
		)
;


--Outcome
MERGE
	ChildHealth.Outcome target

USING
	(
	select distinct
		 OutcomeCode = chsREG_NEO_NATAL_INITIAL_EXAM.OUTCOME_CODE
		,Outcome = chsREG_NEO_NATAL_INITIAL_EXAM.OUTCOME_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_NEO_NATAL_INITIAL_EXAM
	where
		chsREG_NEO_NATAL_INITIAL_EXAM.OUTCOME_CODE is not null
	) source
	(
	 OutcomeCode
	,Outcome
	)
ON	target.OutcomeCode = source.OutcomeCode

WHEN MATCHED THEN 
    UPDATE SET Outcome = source.Outcome

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 OutcomeCode
		,Outcome
		)
    VALUES
		(
		 source.OutcomeCode
		,source.Outcome
		)
;


--SchoolCategory
MERGE
	ChildHealth.SchoolCategory target

USING
	(
	select distinct
		 SchoolCategoryCode = chsSH_SCHOOL_MASTER.CATEGORY_CODE
		,SchoolCategory = chsSH_SCHOOL_MASTER.CATEGORY_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsSH_SCHOOL_MASTER
	where
		chsSH_SCHOOL_MASTER.CATEGORY_CODE is not null
	) source
	(
	 SchoolCategoryCode
	,SchoolCategory
	)
ON	target.SchoolCategoryCode = source.SchoolCategoryCode

WHEN MATCHED THEN 
    UPDATE SET SchoolCategory = source.SchoolCategory

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 SchoolCategoryCode
		,SchoolCategory
		)
    VALUES
		(
		 source.SchoolCategoryCode
		,source.SchoolCategory
		)
;


--Venue
MERGE
	ChildHealth.Venue target

USING
	(
	select distinct
		 VenueCode = chsPS_EXAM_HIST_DETS.CLINIC_CODE
		,Venue = chsPS_EXAM_HIST_DETS.CLINIC_NAME
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS
	where
		chsPS_EXAM_HIST_DETS.CLINIC_CODE is not null

	union

	select distinct
		 VenueCode = chsPS_EXAM_HIST_DETS.RECALL_VENUE_CODE
		,Venue = coalesce(chsPS_EXAM_HIST_DETS.RECALL_VENUE_DESC, chsPS_EXAM_HIST_DETS.RECALL_VENUE_CODE)
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsPS_EXAM_HIST_DETS
	where
		chsPS_EXAM_HIST_DETS.RECALL_VENUE_CODE is not null

	--union

	--select distinct
	--	 VenueCode = chsVI_CVP_EVENT_DETAILS.CLINIC_CODE
	--	,Venue = chsVI_CVP_EVENT_DETAILS.CLINIC_NAME
	--from
	--	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
	--where
	--	chsVI_CVP_EVENT_DETAILS.CLINIC_CODE is not null

	--union

	--select distinct
	--	 VenueCode = AE_ATTENDANCES.VENUE_CODE
	--	,Venue = AE_ATTENDANCES.VENUE_DESCRIPTION
	--from
	--	[$(ChildHealth_CarePlus)].SQLUser.AE_ATTENDANCES
	--where
	--	AE_ATTENDANCES.VENUE_CODE is not null
	) source
	(
	 VenueCode
	,Venue
	)
ON	target.VenueCode = source.VenueCode

WHEN MATCHED THEN 
    UPDATE SET Venue = source.Venue

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 VenueCode
		,Venue
		)
    VALUES
		(
		 source.VenueCode
		,source.Venue
		)
;

--insert the dodgy event based venues
insert
into
	ChildHealth.Venue
(
	 VenueCode
	,Venue
)
select distinct
	 VenueCode = chsVI_CVP_EVENT_DETAILS.CLINIC_CODE
	,Venue =
		coalesce(
			 chsVI_CVP_EVENT_DETAILS.CLINIC_NAME
			,chsVI_CVP_EVENT_DETAILS.CLINIC_CODE
		)
from
	[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
where
	chsVI_CVP_EVENT_DETAILS.CLINIC_CODE is not null
and	not exists
	(
	select
		1
	from
		(
		select distinct
			 chsVI_CVP_EVENT_DETAILS.CLINIC_CODE
			,chsVI_CVP_EVENT_DETAILS.CLINIC_NAME
		from
			[$(ChildHealth_CarePlus)].SQLUser.chsVI_CVP_EVENT_DETAILS
		where
			chsVI_CVP_EVENT_DETAILS.CLINIC_CODE is not null
		) Previous
	where
		Previous.CLINIC_CODE is not null
	and	Previous.CLINIC_CODE = chsVI_CVP_EVENT_DETAILS.CLINIC_CODE
	and	coalesce(Previous.CLINIC_NAME, '?') > coalesce(chsVI_CVP_EVENT_DETAILS.CLINIC_NAME, '?')
	)

and	not exists
	(
	select
		1
	from
		ChildHealth.Venue
	where
		Venue.VenueCode = chsVI_CVP_EVENT_DETAILS.CLINIC_CODE
	)



--AEDiagnosis
MERGE
	ChildHealth.AEDiagnosis target

USING
	(
	select distinct
		 AEDiagnosisCode = AE_ATTENDANCES.AE_DIAGNOSIS_CODE
		,AEDiagnosis = AE_ATTENDANCES.AE_DIAGNOSIS_CODE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.AE_ATTENDANCES
	where
		AE_ATTENDANCES.AE_DIAGNOSIS_CODE is not null
	) source
	(
	 AEDiagnosisCode
	,AEDiagnosis
	)
ON	target.AEDiagnosisCode = source.AEDiagnosisCode

WHEN MATCHED THEN 
    UPDATE SET AEDiagnosis = source.AEDiagnosis

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 AEDiagnosisCode
		,AEDiagnosis
		)
    VALUES
		(
		 source.AEDiagnosisCode
		,source.AEDiagnosis
		)
;


--ReasonForDischarge
MERGE
	ChildHealth.ReasonForDischarge target

USING
	(
	select distinct
		 ReasonForDischargeCode = AE_ATTENDANCES.REASON_FOR_DISCHARGE_CODE
		,ReasonForDischarge = AE_ATTENDANCES.REASON_FOR_DISCHARGE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.AE_ATTENDANCES
	where
		AE_ATTENDANCES.REASON_FOR_DISCHARGE_CODE is not null
	) source
	(
	 ReasonForDischargeCode
	,ReasonForDischarge
	)
ON	target.ReasonForDischargeCode = source.ReasonForDischargeCode

WHEN MATCHED THEN 
    UPDATE SET ReasonForDischarge = source.ReasonForDischarge

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonForDischargeCode
		,ReasonForDischarge
		)
    VALUES
		(
		 source.ReasonForDischargeCode
		,source.ReasonForDischarge
		)
;


--FollowUpByWhom
MERGE
	ChildHealth.FollowUpByWhom target

USING
	(
	select distinct
		 FollowUpByWhomCode = AE_ATTENDANCES.FOLLOW_UP_BY_WHOM_CODE
		,FollowUpByWhom = AE_ATTENDANCES.FOLLOW_UP_BY_WHOM_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.AE_ATTENDANCES
	where
		AE_ATTENDANCES.FOLLOW_UP_BY_WHOM_CODE is not null
	) source
	(
	 FollowUpByWhomCode
	,FollowUpByWhom
	)
ON	target.FollowUpByWhomCode = source.FollowUpByWhomCode

WHEN MATCHED THEN 
    UPDATE SET FollowUpByWhom = source.FollowUpByWhom

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 FollowUpByWhomCode
		,FollowUpByWhom
		)
    VALUES
		(
		 source.FollowUpByWhomCode
		,source.FollowUpByWhom
		)
;


--FollowUpByWhomType
MERGE
	ChildHealth.FollowUpByWhomType target

USING
	(
	select distinct
		 FollowUpByWhomTypeCode = AE_ATTENDANCES.FOLLOW_UP_BY_WHOM_TYPE
		,FollowUpByWhomType = AE_ATTENDANCES.FOLLOW_UP_BY_WHOM_TYPE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.AE_ATTENDANCES
	where
		AE_ATTENDANCES.FOLLOW_UP_BY_WHOM_TYPE is not null
	) source
	(
	 FollowUpByWhomTypeCode
	,FollowUpByWhomType
	)
ON	target.FollowUpByWhomTypeCode = source.FollowUpByWhomTypeCode

WHEN MATCHED THEN 
    UPDATE SET FollowUpByWhomType = source.FollowUpByWhomType

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 FollowUpByWhomTypeCode
		,FollowUpByWhomType
		)
    VALUES
		(
		 source.FollowUpByWhomTypeCode
		,source.FollowUpByWhomType
		)
;


--RegisterStaff
MERGE
	ChildHealth.RegisterStaff target

USING
	(
	select distinct
		 RegisterStaffCode = chsREG_CHILD_REGISTER.REG_STAFF_CODE
		,RegisterStaff = chsREG_CHILD_REGISTER.REG_STAFF_NAME
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CHILD_REGISTER
	where
		chsREG_CHILD_REGISTER.REG_STAFF_CODE is not null
	) source
	(
	 RegisterStaffCode
	,RegisterStaff
	)
ON	target.RegisterStaffCode = source.RegisterStaffCode

WHEN MATCHED THEN 
    UPDATE SET RegisterStaff = source.RegisterStaff

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 RegisterStaffCode
		,RegisterStaff
		)
    VALUES
		(
		 source.RegisterStaffCode
		,source.RegisterStaff
		)
;


--ReasonForRemoval
MERGE
	ChildHealth.ReasonForRemoval target

USING
	(
	select distinct
		 ReasonForRemovalCode = chsREG_CHILD_REGISTER.REASON_FOR_REMOVAL_CODE
		,ReasonForRemoval = chsREG_CHILD_REGISTER.REASON_FOR_REMOVAL
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CHILD_REGISTER
	where
		chsREG_CHILD_REGISTER.REASON_FOR_REMOVAL_CODE is not null
		
	union
	
	select distinct
		 LookedAfterRemovalReasonCode = REASON_REMOVED_FROM_LAC_CODE
		,LookedAfterRemovalReason = REASON_REMOVED_FROM_LAC_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_STATUS
	where
		REASON_REMOVED_FROM_LAC_CODE is not null

	) source
	(
	 ReasonForRemovalCode
	,ReasonForRemoval
	)
ON	target.ReasonForRemovalCode = source.ReasonForRemovalCode

WHEN MATCHED THEN 
    UPDATE SET ReasonForRemoval = source.ReasonForRemoval

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonForRemovalCode
		,ReasonForRemoval
		)
    VALUES
		(
		 source.ReasonForRemovalCode
		,source.ReasonForRemoval
		)
;


--Register
MERGE
	ChildHealth.Register target

USING
	(
	select distinct
		 RegisterCode = chsREG_CHILD_REGISTER.REGISTER_CODE
		,Register = chsREG_CHILD_REGISTER.REGISTER_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CHILD_REGISTER
	where
		chsREG_CHILD_REGISTER.REGISTER_CODE is not null
	) source
	(
	 RegisterCode
	,Register
	)
ON	target.RegisterCode = source.RegisterCode

WHEN MATCHED THEN 
    UPDATE SET Register = source.Register

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 RegisterCode
		,Register
		)
    VALUES
		(
		 source.RegisterCode
		,source.Register
		)
;


--RegisterStaffType
MERGE
	ChildHealth.RegisterStaffType target

USING
	(
	select distinct
		 RegisterStaffTypeCode = chsREG_CHILD_REGISTER.REG_STAFF_TYPE
		,RegisterStaffType = chsREG_CHILD_REGISTER.REG_STAFF_TYPE_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CHILD_REGISTER
	where
		chsREG_CHILD_REGISTER.REG_STAFF_TYPE is not null
	) source
	(
	 RegisterStaffTypeCode
	,RegisterStaffType
	)
ON	target.RegisterStaffTypeCode = source.RegisterStaffTypeCode

WHEN MATCHED THEN 
    UPDATE SET RegisterStaffType = source.RegisterStaffType

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 RegisterStaffTypeCode
		,RegisterStaffType
		)
    VALUES
		(
		 source.RegisterStaffTypeCode
		,source.RegisterStaffType
		)
;


--ReasonRemovedFromPlan
MERGE
	ChildHealth.ReasonRemovedFromPlan target

USING
	(
	select distinct
		 ReasonRemovedFromPlanCode = chsCP_CHILD_PROTECTION_DETAILS.REASON_REMOVED_FROM_PLAN_CODE
		,ReasonRemovedFromPlan = left(chsCP_CHILD_PROTECTION_DETAILS.REASON_REMOVED_FROM_PLAN_DESCRIPTION,50)
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsCP_CHILD_PROTECTION_DETAILS
	where
		chsCP_CHILD_PROTECTION_DETAILS.REASON_REMOVED_FROM_PLAN_CODE is not null
	) source
	(
	 ReasonRemovedFromPlanCode
	,ReasonRemovedFromPlan
	)
ON	target.ReasonRemovedFromPlanCode = source.ReasonRemovedFromPlanCode

WHEN MATCHED THEN 
    UPDATE SET ReasonRemovedFromPlan = source.ReasonRemovedFromPlan

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonRemovedFromPlanCode
		,ReasonRemovedFromPlan
		)
    VALUES
		(
		 source.ReasonRemovedFromPlanCode
		,source.ReasonRemovedFromPlan
		)
;


--SocialServicesDepartment
MERGE
	ChildHealth.SocialServicesDepartment target

USING
	(
	select distinct
		 SocialServicesDepartmentCode = chsCP_CHILD_PROTECTION_DETAILS.SOCIAL_SERVICES_DEPARTMENT_CODE
		,SocialServicesDepartment = chsCP_CHILD_PROTECTION_DETAILS.SOCIAL_SERVICES_DEPARTMENT_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsCP_CHILD_PROTECTION_DETAILS
	where
		chsCP_CHILD_PROTECTION_DETAILS.SOCIAL_SERVICES_DEPARTMENT_CODE is not null

	union

	select distinct
		 SocialServicesDepartmentCode = chs_LOOKED_AFTER_STATUS.SOCIAL_SERVICES_DEPARTMENT_CODE
		,SocialServicesDepartment = chs_LOOKED_AFTER_STATUS.SOCIAL_SERVICES_DEPARTMENT_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_STATUS
	where
		chs_LOOKED_AFTER_STATUS.SOCIAL_SERVICES_DEPARTMENT_CODE is not null
	) source
	(
	 SocialServicesDepartmentCode
	,SocialServicesDepartment
	)
ON	target.SocialServicesDepartmentCode = source.SocialServicesDepartmentCode

WHEN MATCHED THEN 
    UPDATE SET SocialServicesDepartment = source.SocialServicesDepartment

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 SocialServicesDepartmentCode
		,SocialServicesDepartment
		)
    VALUES
		(
		 source.SocialServicesDepartmentCode
		,source.SocialServicesDepartment
		)
;


--LeadClinician
MERGE
	ChildHealth.LeadClinician target

USING
	(
	select distinct
		 LeadClinicianCode = chsCP_CHILD_PROTECTION_DETAILS.LEAD_CLINICIAN_CODE
		,LeadClinician = chsCP_CHILD_PROTECTION_DETAILS.LEAD_CLINICIAN_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsCP_CHILD_PROTECTION_DETAILS
	where
		chsCP_CHILD_PROTECTION_DETAILS.LEAD_CLINICIAN_CODE is not null

	union

	select distinct
		 LeadClinicianCode = chs_LOOKED_AFTER_STATUS.LEAD_CLINICIAN_CODE
		,LeadClinician = chs_LOOKED_AFTER_STATUS.LEAD_CLINICIAN_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_STATUS
	where
		chs_LOOKED_AFTER_STATUS.LEAD_CLINICIAN_CODE is not null

	) source
	(
	 LeadClinicianCode
	,LeadClinician
	)
ON	target.LeadClinicianCode = source.LeadClinicianCode

WHEN MATCHED THEN 
    UPDATE SET LeadClinician = source.LeadClinician

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 LeadClinicianCode
		,LeadClinician
		)
    VALUES
		(
		 source.LeadClinicianCode
		,source.LeadClinician
		)
;


--LeadClinicianType
MERGE
	ChildHealth.LeadClinicianType target

USING
	(
	select distinct
		 LeadClinicianTypeCode = chsCP_CHILD_PROTECTION_DETAILS.LEAD_CLINICIAN_TYPE_CODE
		,LeadClinicianType = chsCP_CHILD_PROTECTION_DETAILS.LEAD_CLINICIAN_TYPE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsCP_CHILD_PROTECTION_DETAILS
	where
		chsCP_CHILD_PROTECTION_DETAILS.LEAD_CLINICIAN_TYPE_CODE is not null

	union

	select distinct
		 LeadClinicianTypeCode = chs_LOOKED_AFTER_STATUS.LEAD_CLINICIAN_TYPE_CODE
		,LeadClinicianType = chs_LOOKED_AFTER_STATUS.LEAD_CLINICIAN_TYPE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_STATUS
	where
		chs_LOOKED_AFTER_STATUS.LEAD_CLINICIAN_TYPE_CODE is not null

	) source
	(
	 LeadClinicianTypeCode
	,LeadClinicianType
	)
ON	target.LeadClinicianTypeCode = source.LeadClinicianTypeCode

WHEN MATCHED THEN 
    UPDATE SET LeadClinicianType = source.LeadClinicianType

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 LeadClinicianTypeCode
		,LeadClinicianType
		)
    VALUES
		(
		 source.LeadClinicianTypeCode
		,source.LeadClinicianType
		)
;


--VulnerabilityFactor
MERGE
	ChildHealth.VulnerabilityFactor target

USING
	(
	select distinct
		 VulnerabilityFactorCode = chsCP_CHILD_PROTECTION_DETAILS.VULNERABILITY_FACTOR
		,VulnerabilityFactor = chsCP_CHILD_PROTECTION_DETAILS.VULNERABILITY_FACTOR_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsCP_CHILD_PROTECTION_DETAILS
	where
		chsCP_CHILD_PROTECTION_DETAILS.VULNERABILITY_FACTOR is not null
	) source
	(
	 VulnerabilityFactorCode
	,VulnerabilityFactor
	)
ON	target.VulnerabilityFactorCode = source.VulnerabilityFactorCode

WHEN MATCHED THEN 
    UPDATE SET VulnerabilityFactor = source.VulnerabilityFactor

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 VulnerabilityFactorCode
		,VulnerabilityFactor
		)
    VALUES
		(
		 source.VulnerabilityFactorCode
		,source.VulnerabilityFactor
		)
;


--CauseForConcern
MERGE
	ChildHealth.CauseForConcern target

USING
	(
	select distinct
		 CauseForConcernCode = CP_CHILD_PROTECTION_CAUSE_FOR_CONCERN.CAUSE_FOR_CONCERN_CODE
		,CauseForConcern = CP_CHILD_PROTECTION_CAUSE_FOR_CONCERN.CAUSE_FOR_CONCERN_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_CAUSE_FOR_CONCERN
	where
		CP_CHILD_PROTECTION_CAUSE_FOR_CONCERN.CAUSE_FOR_CONCERN_CODE is not null
	) source
	(
	 CauseForConcernCode
	,CauseForConcern
	)
ON	target.CauseForConcernCode = source.CauseForConcernCode

WHEN MATCHED THEN 
    UPDATE SET CauseForConcern = source.CauseForConcern

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 CauseForConcernCode
		,CauseForConcern
		)
    VALUES
		(
		 source.CauseForConcernCode
		,source.CauseForConcern
		)
;


--HealthProfessional
MERGE
	ChildHealth.HealthProfessional target

USING
	(
	select distinct
		 HealthProfessionalCode = CP_CHILD_PROTECTION_DOMESTIC_ABUSE.HEALTH_PROFESSIONAL_CODE
		,HealthProfessional = CP_CHILD_PROTECTION_DOMESTIC_ABUSE.HEALTH_PROFESSIONAL_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_DOMESTIC_ABUSE
	where
		CP_CHILD_PROTECTION_DOMESTIC_ABUSE.HEALTH_PROFESSIONAL_CODE is not null
	) source
	(
	 HealthProfessionalCode
	,HealthProfessional
	)
ON	target.HealthProfessionalCode = source.HealthProfessionalCode

WHEN MATCHED THEN 
    UPDATE SET HealthProfessional = source.HealthProfessional

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 HealthProfessionalCode
		,HealthProfessional
		)
    VALUES
		(
		 source.HealthProfessionalCode
		,source.HealthProfessional
		)
;


--HealthProfessionalType
MERGE
	ChildHealth.HealthProfessionalType target

USING
	(
	select distinct
		 HealthProfessionalTypeCode = CP_CHILD_PROTECTION_DOMESTIC_ABUSE.HEALTH_PROFESSIONAL_TYPE
		,HealthProfessionalType = CP_CHILD_PROTECTION_DOMESTIC_ABUSE.HEALTH_PROFESSIONAL_TYPE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_DOMESTIC_ABUSE
	where
		CP_CHILD_PROTECTION_DOMESTIC_ABUSE.HEALTH_PROFESSIONAL_TYPE is not null
	) source
	(
	 HealthProfessionalTypeCode
	,HealthProfessionalType
	)
ON	target.HealthProfessionalTypeCode = source.HealthProfessionalTypeCode

WHEN MATCHED THEN 
    UPDATE SET HealthProfessionalType = source.HealthProfessionalType

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 HealthProfessionalTypeCode
		,HealthProfessionalType
		)
    VALUES
		(
		 source.HealthProfessionalTypeCode
		,source.HealthProfessionalType
		)
;


--MappaCategory
MERGE
	ChildHealth.MappaCategory target

USING
	(
	select distinct
		 MappaCategoryCode = CP_CHILD_PROTECTION_MAPPA.MAPPA_CATEGORY_CODE
		,MappaCategory = CP_CHILD_PROTECTION_MAPPA.MAPPA_CATEGORY_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_MAPPA
	where
		CP_CHILD_PROTECTION_MAPPA.MAPPA_CATEGORY_CODE is not null
	) source
	(
	 MappaCategoryCode
	,MappaCategory
	)
ON	target.MappaCategoryCode = source.MappaCategoryCode

WHEN MATCHED THEN 
    UPDATE SET MappaCategory = source.MappaCategory

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 MappaCategoryCode
		,MappaCategory
		)
    VALUES
		(
		 source.MappaCategoryCode
		,source.MappaCategory
		)
;


--MaracReasonForReferral
MERGE
	ChildHealth.MaracReasonForReferral target

USING
	(
	select distinct
		 MaracReasonForReferralCode = CP_CHILD_PROTECTION_MARAC.REASON_FOR_REFERRAL_CODE
		,MaracReasonForReferral = CP_CHILD_PROTECTION_MARAC.REASON_FOR_REFERRAL_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_MARAC
	where
		CP_CHILD_PROTECTION_MARAC.REASON_FOR_REFERRAL_CODE is not null
	) source
	(
	 MaracReasonForReferralCode
	,MaracReasonForReferral
	)
ON	target.MaracReasonForReferralCode = source.MaracReasonForReferralCode

WHEN MATCHED THEN 
    UPDATE SET MaracReasonForReferral = source.MaracReasonForReferral

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 MaracReasonForReferralCode
		,MaracReasonForReferral
		)
    VALUES
		(
		 source.MaracReasonForReferralCode
		,source.MaracReasonForReferral
		)
;


--Need
MERGE
	ChildHealth.Need target

USING
	(
	select distinct
		 NeedCode = CP_CHILD_PROTECTION_NEEDS.NEED_CODE
		,Need = CP_CHILD_PROTECTION_NEEDS.NEED_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.CP_CHILD_PROTECTION_NEEDS
	where
		CP_CHILD_PROTECTION_NEEDS.NEED_CODE is not null
	) source
	(
	 NeedCode
	,Need
	)
ON	target.NeedCode = source.NeedCode

WHEN MATCHED THEN 
    UPDATE SET Need = source.Need

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 NeedCode
		,Need
		)
    VALUES
		(
		 source.NeedCode
		,source.Need
		)
;


--PlacementType
MERGE
	ChildHealth.PlacementType target

USING
	(
	select distinct
		 PlacementTypeCode = PLACEMENT_TYPE_CODE
		,PlacementType = PLACEMENT_TYPE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_STATUS
	where
		chs_LOOKED_AFTER_STATUS.PLACEMENT_TYPE_CODE is not null
	) source
	(
	 PlacementTypeCode
	,PlacementType
	)
ON	target.PlacementTypeCode = source.PlacementTypeCode

WHEN MATCHED THEN 
    UPDATE SET PlacementType = source.PlacementType

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 PlacementTypeCode
		,PlacementType
		)
    VALUES
		(
		 source.PlacementTypeCode
		,source.PlacementType
		)
;



--LookedAfterStatus
MERGE
	ChildHealth.LookedAfterStatus target

USING
	(
	select distinct
		 LookedAfterStatusCode = LAC_STATUS_CODE
		,LookedAfterStatus = LAC_STATUS_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chs_LOOKED_AFTER_STATUS
	where
		chs_LOOKED_AFTER_STATUS.LAC_STATUS_CODE is not null
	) source
	(
	 LookedAfterStatusCode
	,LookedAfterStatus
	)
ON	target.LookedAfterStatusCode = source.LookedAfterStatusCode

WHEN MATCHED THEN 
    UPDATE SET LookedAfterStatus = source.LookedAfterStatus

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 LookedAfterStatusCode
		,LookedAfterStatus
		)
    VALUES
		(
		 source.LookedAfterStatusCode
		,source.LookedAfterStatus
		)
;


--BloodspotTest
MERGE
	ChildHealth.BloodspotTest target

USING
	(
	select distinct
		 BloodspotTestCode = TEST_CODE
		,BloodspotTest = TEST_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_ROUTINE_TESTS
	where
		chsREG_CMF_ROUTINE_TESTS.TEST_CODE is not null
	) source
	(
	 BloodspotTestCode
	,BloodspotTest
	)
ON	target.BloodspotTestCode = source.BloodspotTestCode

WHEN MATCHED THEN 
    UPDATE SET BloodspotTest = source.BloodspotTest

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 BloodspotTestCode
		,BloodspotTest
		)
    VALUES
		(
		 source.BloodspotTestCode
		,source.BloodspotTest
		)
;


--BloodspotTestResult
MERGE
	ChildHealth.BloodspotTestResult target

USING
	(
	select distinct
		 BloodspotTestResultCode = TEST_RES_CODE
		,BloodspotTestResult = TEST_RESULT_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_ROUTINE_TESTS
	where
		chsREG_CMF_ROUTINE_TESTS.TEST_RES_CODE is not null
	and	chsREG_CMF_ROUTINE_TESTS.TEST_RES_CODE <> ''
	) source
	(
	 BloodspotTestResultCode
	,BloodspotTestResult
	)
ON	target.BloodspotTestResultCode = source.BloodspotTestResultCode

WHEN MATCHED THEN 
    UPDATE SET BloodspotTestResult = source.BloodspotTestResult

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 BloodspotTestResultCode
		,BloodspotTestResult
		)
    VALUES
		(
		 source.BloodspotTestResultCode
		,source.BloodspotTestResult
		)
;


--ReasonForRepeat
MERGE
	ChildHealth.ReasonForRepeat target

USING
	(
	select distinct
		 ReasonForRepeatCode = REASON_CODE
		,ReasonForRepeat = REASON_CODE_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_CMF_ROUTINE_TESTS
	where
		chsREG_CMF_ROUTINE_TESTS.REASON_CODE is not null

	) source
	(
	 ReasonForRepeatCode
	,ReasonForRepeat
	)
ON	target.ReasonForRepeatCode = source.ReasonForRepeatCode

WHEN MATCHED THEN 
    UPDATE SET ReasonForRepeat = source.ReasonForRepeat

WHEN NOT MATCHED BY SOURCE THEN	
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ReasonForRepeatCode
		,ReasonForRepeat
		)
    VALUES
		(
		 source.ReasonForRepeatCode
		,source.ReasonForRepeat
		)
;

--Consent
MERGE
	ChildHealth.Consent target

USING
	(
	select distinct
		 ConsentCode = CONSENT_CODE
		,Consent = DATA_CONSENT_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.DATA_CONSENT
	where
		CONSENT_CODE is not null
	) source
	(
	 ConsentCode
	,Consent
	)
ON	target.ConsentCode = source.ConsentCode

WHEN MATCHED THEN 
    UPDATE SET Consent = source.Consent

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 ConsentCode
		,Consent
		)
    VALUES
		(
		 source.ConsentCode
		,source.Consent
		)
;

--SystemOrDepartment
MERGE
	ChildHealth.SystemOrDepartment target

USING
	(
	select distinct
		 SystemOrDepartmentCode = SYSTEM_DEPARTMENT_CODE
		,SystemOrDepartment = SYSTEM_DEPARTMENT_NAME
	from
		[$(ChildHealth_CarePlus)].SQLUser.DATA_CONSENT
	where
		SYSTEM_DEPARTMENT_CODE is not null
	) source
	(
	 SystemOrDepartmentCode
	,SystemOrDepartment
	)
ON	target.SystemOrDepartmentCode = source.SystemOrDepartmentCode

WHEN MATCHED THEN 
    UPDATE SET SystemOrDepartment = source.SystemOrDepartment

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 SystemOrDepartmentCode
		,SystemOrDepartment
		)
    VALUES
		(
		 source.SystemOrDepartmentCode
		,source.SystemOrDepartment
		)
;



--PersonGivingConsentType
MERGE
	ChildHealth.PersonGivingConsentType target

USING
	(
	select distinct
		 PersonGivingConsentTypeCode = TYPE_OF_PERSON_GIVING_CONSENT
		,PersonGivingConsentType = TYPE_OF_PERSON_GIVING_CONSENT_DESCRIPTION
	from
		[$(ChildHealth_CarePlus)].SQLUser.DATA_CONSENT
	where
		TYPE_OF_PERSON_GIVING_CONSENT is not null
	) source
	(
	 PersonGivingConsentTypeCode
	,PersonGivingConsentType
	)
ON	target.PersonGivingConsentTypeCode = source.PersonGivingConsentTypeCode

WHEN MATCHED THEN 
    UPDATE SET PersonGivingConsentType = source.PersonGivingConsentType

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 PersonGivingConsentTypeCode
		,PersonGivingConsentType
		)
    VALUES
		(
		 source.PersonGivingConsentTypeCode
		,source.PersonGivingConsentType
		)
;



--PersonGivingConsent
MERGE
	ChildHealth.PersonGivingConsent target

USING
	(
	select distinct
		 PersonGivingConsentCode = PERSON_GIVING_CONSENT
		,PersonGivingConsent = PERSON_GIVING_CONSENT
	from
		[$(ChildHealth_CarePlus)].SQLUser.DATA_CONSENT
	where
		PERSON_GIVING_CONSENT is not null
	) source
	(
	 PersonGivingConsentCode
	,PersonGivingConsent
	)
ON	target.PersonGivingConsentCode = source.PersonGivingConsentCode

WHEN MATCHED THEN 
    UPDATE SET PersonGivingConsent = source.PersonGivingConsent

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 PersonGivingConsentCode
		,PersonGivingConsent
		)
    VALUES
		(
		 source.PersonGivingConsentCode
		,source.PersonGivingConsent
		)
;



--DeathPlace
MERGE
	ChildHealth.DeathPlace target

USING
	(
	select distinct
		 DeathPlaceCode = PLACE_OF_DEATH
		,DeathPlace = PLACE_OF_DEATH
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_DEATH_DETAILS
	where
		PLACE_OF_DEATH is not null
	) source
	(
	 DeathPlaceCode
	,DeathPlace
	)
ON	target.DeathPlaceCode = source.DeathPlaceCode

WHEN MATCHED THEN 
    UPDATE SET DeathPlace = source.DeathPlace

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 DeathPlaceCode
		,DeathPlace
		)
    VALUES
		(
		 source.DeathPlaceCode
		,source.DeathPlace
		)
;



--CauseOfDeath
MERGE
	ChildHealth.CauseOfDeath target

USING
	(
	select distinct
		 CauseOfDeathCode = CAUSE_OF_DEATH_CODE1
		,CauseOfDeath = CAUSE_OF_DEATH_DESC1
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_DEATH_DETAILS
	where
		CAUSE_OF_DEATH_CODE1 is not null
	) source
	(
	 CauseOfDeathCode
	,CauseOfDeath
	)
ON	target.CauseOfDeathCode = source.CauseOfDeathCode

WHEN MATCHED THEN 
    UPDATE SET CauseOfDeath = source.CauseOfDeath

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 CauseOfDeathCode
		,CauseOfDeath
		)
    VALUES
		(
		 source.CauseOfDeathCode
		,source.CauseOfDeath
		)
;



--DeathNotifiedBy
MERGE
	ChildHealth.DeathNotifiedBy target

USING
	(
	select distinct
		 DeathNotifiedByCode = NOTIFIED_BY_CODE
		,DeathNotifiedBy = NOTIFIED_BY_DESC
	from
		[$(ChildHealth_CarePlus)].SQLUser.chsREG_DEATH_DETAILS
	where
		NOTIFIED_BY_CODE is not null
	) source
	(
	 DeathNotifiedByCode
	,DeathNotifiedBy
	)
ON	target.DeathNotifiedByCode = source.DeathNotifiedByCode

WHEN MATCHED THEN 
    UPDATE SET DeathNotifiedBy = source.DeathNotifiedBy

WHEN NOT MATCHED BY SOURCE THEN
	DELETE

WHEN NOT MATCHED THEN	
    INSERT
		(
		 DeathNotifiedByCode
		,DeathNotifiedBy
		)
    VALUES
		(
		 source.DeathNotifiedByCode
		,source.DeathNotifiedBy
		)
;




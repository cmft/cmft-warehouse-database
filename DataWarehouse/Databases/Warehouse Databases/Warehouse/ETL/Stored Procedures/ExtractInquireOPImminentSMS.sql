﻿

CREATE procedure [ETL].[ExtractInquireOPImminentSMS]
	 --@AppointmentDate smalldatetime = null
	@debug bit = 0
as

/****************************************************************************************
	Stored procedure : [ETL].[ExtractInquireOPImminentSMS]
	Description		 : Data for envoy
	
	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
	12/06/2014	Paul Egan		Added 3 extra fields for Endoscopy exclusions
*****************************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql1 varchar(8000)
declare @fromTime varchar(12)
declare @fromTimePreadmitCancel varchar(12)
declare @toTime varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

--select @fromTime = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@AppointmentDate, getdate())), 0), 112) + '0000'

--select @toTime = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@AppointmentDate, getdate())), 0), 112) + '2400'

select @fromTime = convert(varchar, dateadd(day, datediff(day, 0, getdate()) , 0), 112) + '0000'

select @fromTimePreadmitCancel = convert(varchar, dateadd(day, datediff(day, 0, getdate()) , -3), 112) + '0000'

select @toTime = convert(varchar, dateadd(day, datediff(day, 0, getdate()), 7), 112) + '2400'

----debug
--select @fromTime = '201205040000'

--select @fromTimePreadmitCancel = '201205010000'

--select @toTime = '201205112400'


INSERT INTO ETL.TImportOPImminentSMS
(
	 SourceUniqueID
	,DistrictNo
	,AppointmentDate
	,AppointmentTime
	,AppointmentStatusCode
	,CancelledByCode
	,ClinicCode
	,SpecialtyCode
	,HomePhone
	,MobilePhone
	,Forenames
	,Surname
	,DateOfBirth
	,DateOfDeath
	,CancellationDate
	,EncounterTypeCode
	,AppointmentTypeCode
	,Postcode
	,WardCode
	,ConsultantCode
	
	/* Added Paul Egan 12/06/2014 for Endoscopy exclusions and clinic codes appended with sex code */
	,DiagGroup
	,SexCode
	,Comment
)

select
	 SourceUniqueID
	,DistrictNo
	,AppointmentDate
	,AppointmentTime
	,AppointmentStatusCode
	,CancelledByCode
	,ClinicCode
	,SpecialtyCode
	,HomePhone = replace(HomePhone , ' ' , '')
	,MobilePhone = replace(MobilePhone , ' ' , '')
	,Forenames
	,Surname
	,DateOfBirth
	,DateOfDeath
	,CancellationDate
	,EncounterTypeCode
	,AppointmentTypeCode
	,Postcode
	,WardCode
	,ConsultantCode
	
	/* Added Paul Egan 12/06/2014 for Endoscopy exclusions and clinic codes appended with sex code */
	,DiagGroup
	,SexCode
	,Comment
from
	(
	select
		 OPA.OPAID SourceUniqueID
		,PATDATA.DistrictNumber DistrictNo
		,OPA.ApptDate AppointmentDate
		,OPA.ApptTime AppointmentTime
		,OPA.ApptStatus AppointmentStatusCode
		,left(OPA.CancelBy , 3) CancelledByCode
		,OPA.ClinicCode
		,OPA.ClinicSpecialty SpecialtyCode
		,PATDATA.PtHomePhone HomePhone
		,PATDATA.PtMobilePhone MobilePhone
		,PATDATA.Forenames
		,PATDATA.Surname
		,PATDATA.PtDoB DateOfBirth
		,PATDATA.PtDateOfDeath DateOfDeath
		,OPA.ApptCancDate CancellationDate
		,'OP' EncounterTypeCode
		,OPA.ApptType AppointmentTypeCode
		,PATDATA.PtAddrPostCode Postcode
		,WardCode = null
		,ConsultantCode = OPA.ClinicConsultant
		
		/* Added Paul Egan 12/06/2014 for Endoscopy exclusions and clinic codes appended with sex code */
		,DiagGroup = null	-- Does not exist in OPA table
		,SexCode = PATDATA.Sex
		,Comment = OPA.ApptComment
	from
		[$(PAS)].Inquire.OPA

	inner join [$(PAS)].Inquire.PATDATA
	on	PATDATA.InternalPatientNumber = OPA.InternalPatientNumber

	where
		PtApptStartDtimeInt between @fromTime and @toTime

	union all

	select
		 Preadmission.CURRENTPREADMISSIONSID SourceUniqueID
		,PATDATA.DistrictNumber DistrictNo
		,Preadmission.TCIDate AppointmentDate
		,Preadmission.TCITime AppointmentTime
		,null AppointmentStatusCode
		,null CancelledByCode
		,Preadmission.WlCode ClinicCode
		,Preadmission.Specialty SpecialtyCode
		,PATDATA.PtHomePhone HomePhone
		,PATDATA.PtMobilePhone MobilePhone
		,PATDATA.Forenames
		,PATDATA.Surname
		,PATDATA.PtDoB DateOfBirth
		,PATDATA.PtDateOfDeath DateOfDeath
		,null CancellationDate
		,'APC' EncounterTypeCode
		,Preadmission.IntdMgmt AppointmentTypeCode
		,PATDATA.PtAddrPostCode Postcode
		,WardCode = Preadmission.ExpectedWard
		,ConsultantCode = Preadmission.Consultant
		
		/* Added Paul Egan 12/06/2014 for Endoscopy exclusions and clinic codes appended with sex code */
		,DiagGroup = Preadmission.DiagGroup
		,SexCode = PATDATA.Sex
		,Comment = Preadmission.WLCommentNonClin	-- This was the field that contained '%BCSP%' for exclusion.
		
	from
		[$(PAS)].Inquire.CURRENTPREADMISSIONS Preadmission

	inner join [$(PAS)].Inquire.PATDATA
	on	PATDATA.InternalPatientNumber = Preadmission.InternalPatientNumber

	where
		EpsCurrExpAdmDtimeInt between @fromTime and @toTime

	union all

	select
		 PreadmitCancel.WLACTIVITYID SourceUniqueID
		,PATDATA.DistrictNumber DistrictNo
		,Preadmission.TCIDate AppointmentDate
		,Preadmission.TCITime AppointmentTime
		,'CND' AppointmentStatusCode
		,left(PreadmitCancel.CancelledBy , 3) CancelledByCode
		,PreadmitCancel.WaitingList ClinicCode
		,Preadmission.Specialty SpecialtyCode
		,PATDATA.PtHomePhone HomePhone
		,PATDATA.PtMobilePhone MobilePhone
		,PATDATA.Forenames
		,PATDATA.Surname
		,PATDATA.PtDoB DateOfBirth
		,PATDATA.PtDateOfDeath DateOfDeath
		,PreadmitCancel.ActivityDate CancellationDate
		,'APC' EncounterTypeCode
		,WLEntry.IntdMgmt AppointmentTypeCode
		,PATDATA.PtAddrPostCode Postcode
		,WardCode = Preadmission.Ward
		,ConsultantCode = Preadmission.Consultant
		
		/* Added Paul Egan 12/06/2014 for Endoscopy exclusions and clinic codes appended with sex code */
		,DiagGroup = Preadmission.DiagGroup
		,SexCode = PATDATA.Sex
		,Comment = WLEntry.CommentNonClin	-- This was the field that contained '%BCSP%' for exclusion.

	from
		[$(PAS)].Inquire.WLACTIVITY PreadmitCancel

	inner join [$(PAS)].Inquire.WLACTIVITY Preadmission
	on	Preadmission.EpsActvTypeInt = PreadmitCancel.PrevActivityInt
	and	Preadmission.EpsActvDtimeInt = PreadmitCancel.PrevActivityDTimeInt
	and	Preadmission.InternalPatientNumber = PreadmitCancel.InternalPatientNumber
	and	Preadmission.EpisodeNumber = PreadmitCancel.EpisodeNumber

	inner join [$(PAS)].Inquire.WLENTRY WLEntry
	on	WLEntry.InternalPatientNumber = PreadmitCancel.InternalPatientNumber
	and	WLEntry.EpisodeNumber = PreadmitCancel.EpisodeNumber

	inner join [$(PAS)].Inquire.PATDATA
	on	PATDATA.InternalPatientNumber = PreadmitCancel.InternalPatientNumber

	where
		PreadmitCancel.EpsActvDtimeInt >= @fromTimePreadmitCancel
	and	PreadmitCancel.EpsActvTypeInt = '6'
	) Encounter



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'ExtractInquireOPImminentSMS', @Stats, @StartTime




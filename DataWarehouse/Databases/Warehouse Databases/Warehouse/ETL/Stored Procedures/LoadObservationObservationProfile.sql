﻿

CREATE procedure [ETL].[LoadObservationObservationProfile]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 @LoadStartDate = min(LastModifiedTime)
	,@LoadEndDate = max(LastModifiedTime) 
from
	ETL.TLoadObservationObservationProfile

merge
	Observation.ObservationProfile target
using
	(
	select
		 SourceUniqueID
		,CasenoteNumber = cast(CasenoteNumber as varchar(20))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,SpecialtyID = cast(SpecialtyID as int)
		,LocationID = cast(LocationID as int)
		,AdmissionSourceUniqueID = cast(AdmissionSourceUniqueID as int)
		,ProfileID = cast(ProfileID as int)
		,TemplatePriorityID = cast(TemplatePriorityID as int)
		,DrivingTableCode = cast(DrivingTableCode as varchar(5))
		,DrivingTableID = cast(DrivingTableID as int)
		,ProfileReasonID = cast(ProfileReasonID as int)
		,StepNumber = cast(StepNumber as int)
		,IterationNumber = cast(IterationNumber as int)
		,StartTime = cast(StartTime as datetime)
		,EndTime = cast(EndTime as datetime)
		,CreatedBy = cast(CreatedBy as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,LastModifiedBy = cast(LastModifiedBy as int)
		,LastModifiedTime = cast(LastModifiedTime as datetime)
		,Active = cast(Active as bit)
		,InterfaceCode = cast(InterfaceCode as varchar(11))
	from
		ETL.TLoadObservationObservationProfile

	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.LastModifiedTime between @LoadStartDate and @LoadEndDate

	then delete	

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SpecialtyID
			,LocationID
			,AdmissionSourceUniqueID
			,ProfileID
			,TemplatePriorityID
			,DrivingTableCode
			,DrivingTableID
			,ProfileReasonID
			,StepNumber
			,IterationNumber
			,StartTime
			,EndTime
			,CreatedBy
			,CreatedTime
			,LastModifiedBy
			,LastModifiedTime
			,Active
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SpecialtyID
			,source.LocationID
			,source.AdmissionSourceUniqueID
			,source.ProfileID
			,source.TemplatePriorityID
			,source.DrivingTableCode
			,source.DrivingTableID
			,source.ProfileReasonID
			,source.StepNumber
			,source.IterationNumber
			,source.StartTime
			,source.EndTime
			,source.CreatedBy
			,source.CreatedTime
			,source.LastModifiedBy
			,source.LastModifiedTime
			,source.Active
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
			isnull(target.LastModifiedTime, '1 jan 1900') = isnull(source.LastModifiedTime, '1 jan 1900')
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.AdmissionSourceUniqueID = source.AdmissionSourceUniqueID
			,target.ProfileID = source.ProfileID
			,target.TemplatePriorityID = source.TemplatePriorityID
			,target.DrivingTableCode = source.DrivingTableCode
			,target.DrivingTableID = source.DrivingTableID
			,target.ProfileReasonID = source.ProfileReasonID
			,target.StepNumber = source.StepNumber
			,target.IterationNumber = source.IterationNumber
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.CreatedBy = source.CreatedBy
			,target.CreatedTime = source.CreatedTime
			,target.LastModifiedBy = source.LastModifiedBy
			,target.LastModifiedTime = source.LastModifiedTime
			,target.Active = source.Active
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿

CREATE procedure [ETL].[LoadGPICE]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null
as


set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @FromDate = coalesce(@FromDate, (select cast(dateadd(month, -3, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

exec ETL.LoadGPICEReferenceData

truncate table ETL.TImportResultGPICEOrder
exec ETL.ExtractGPICEResultGPICEOrder @FromDate, @ToDate
exec ETL.LoadResultGPICEOrder

truncate table ETL.TImportResultGPICEResult
exec ETL.ExtractGPICEResultGPICEResult @FromDate, @ToDate
exec ETL.LoadResultGPICEResult

truncate table ETL.TImportResultGPICETest
exec ETL.ExtractGPICEResultGPICETest @FromDate, @ToDate
exec ETL.LoadResultGPICETest

truncate table ETL.TImportResultGPICEOrderResult
exec ETL.ExtractGPICEResultGPICEOrderResult @FromDate, @ToDate
exec ETL.LoadResultGPICEOrderResult



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	convert(varchar(6), @Elapsed) + ' Mins, Period ' + 
	convert(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	convert(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


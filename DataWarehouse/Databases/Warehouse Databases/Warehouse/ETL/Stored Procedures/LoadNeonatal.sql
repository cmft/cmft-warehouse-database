﻿


CREATE procedure [ETL].[LoadNeonatal]
	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null
as

/* Under source control under Neonatal */

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @FromDate = coalesce(@FromDate, (select cast(dateadd(month, -12, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

--update Utility.Parameter
--set NumericValue = 0
--where
--	Parameter in ('EXTRACTSTATUSBADGERPATIENT', 'EXTRACTSTATUSBADGERENCOUNTER', 'EXTRACTSTATUSBADGERASSESSMENT') 

exec ETL.LoadNeonatalReferenceData

truncate table ETL.TImportNeonatalBedday
exec ETL.ExtractBadgerNeonatalBedday @FromDate, @ToDate
exec ETL.LoadNeonatalBedday

truncate table ETL.TImportNeonatalBeddayDiagnosis
exec ETL.ExtractBadgerNeonatalBeddayDiagnosis @FromDate, @ToDate
exec ETL.LoadNeonatalBeddayDiagnosis

truncate table ETL.TImportNeonatalBeddayDrug
exec ETL.ExtractBadgerNeonatalBeddayDrug @FromDate, @ToDate
exec ETL.LoadNeonatalBeddayDrug

truncate table ETL.TImportNeonatalBeddayManagement
exec ETL.ExtractBadgerNeonatalBeddayManagement @FromDate, @ToDate
exec ETL.LoadNeonatalBeddayManagement

truncate table ETL.TImportNeonatalBeddayProcedure
exec ETL.ExtractBadgerNeonatalBeddayProcedure @FromDate, @ToDate
exec ETL.LoadNeonatalBeddayProcedure


truncate table ETL.TImportNeonatalEncounter 
exec ETL.ExtractBadgerNeonatalEncounter @FromDate, @ToDate
exec ETL.LoadNeonatalEncounter

--exec ETL.ExtractBadgerNeonatalPatient 
--exec ETL.ExtractBadgerNeonatalEncounter 
--exec [ETL].[ExtractBadgerNeonatalAssessment]

--exec [ETL].[LoadNeonatalEncounter]
--exec [ETL].[LoadNeonatalAssessment]
--exec [ETL].[LoadNeonatalPatient]

--update Utility.Parameter
--set
--	DateValue = getdate()
--where
--	Parameter = 'LOADBADGER'

--if @@rowcount = 0

--insert into Utility.Parameter
--	(
--	 Parameter
--	,DateValue
--	)

--select
--	 Parameter = 'LOADBADGER'
--	,DateValue = getdate()

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	convert(varchar(6), @Elapsed) + ' Mins, Period ' + 
	convert(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	convert(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


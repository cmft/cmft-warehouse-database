﻿CREATE procedure [ETL].[LoadRenal] (@from date = '19000101', @to date = '21000101')  as

exec ETL.LoadRenalRenalPatient
exec ETL.LoadRenalPatientRenalModality @from, @to
exec ETL.LoadRenalPatientVascularAccessEvents @from, @to
exec ETL.LoadRenalPatientLabPanelIronStudies @from, @to
exec ETL.LoadRenalPatientMedicationOrders @from, @to
exec ETL.LoadRenalPatientLabPanelBoneAndMinerals @from, @to
exec ETL.LoadRenalPatientLabPanelElectrolytes @from, @to
exec ETL.LoadRenalPatientLabPanelKidneyFunction @from, @to
exec ETL.LoadRenalPatientHemodialysisServices @from, @to
exec ETL.LoadRenalPatientHemodialysisFlowsheets @from, @to
exec ETL.LoadRenalRecipientTransplantStatusEvents @from, @to

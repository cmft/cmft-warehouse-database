﻿
CREATE proc [ETL].[ExtractGPICEResultGPICEResult]

(
@StartDate date
,@EndDate date
) 

as

set dateformat ymd


declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

declare @LocalStartDate date = @StartDate
declare @LocalEndDate date = @EndDate

select @StartTime = getdate()

select @RowsInserted = 0

insert ETL.TImportResultGPICEResult
(
SourceUniqueID
,PatientID
,PatientIdentifier
,DistrictNo
,CasenoteNumber
,NHSNumber
,DateOfBirth
,SexCode
,ClinicianID
,MainSpecialtyCode
,SpecialtyCode
,LocationID
,ReportSourceUniqueID
,ReportStatusCode
,ReportTime
,ReportComment
,SampleReferenceCode
,SampleTypeCode 
,SampleType 
,SampleCollectionTime 
,InvestigationCode
,InvestigationName
,InvestigationComment
,ResultCode
,ResultName
,Result
,UnitOfMeasurement
,Abnormal
,LowerReferenceRangeValue
,UpperReferenceRangeValue
,ResultComment
)


select 
	SourceUniqueID 
	,PatientID 
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,DateOfBirth
	,SexCode
	,ClinicianID 
	,MainSpecialtyCode
	,SpecialtyCode 
	,LocationID 
	,ReportSourceUniqueID 
	,ReportStatusCode 
	,ReportTime
	,ReportComment
	,SampleReferenceCode
	,SampleTypeCode 
	,SampleType 
	,SampleCollectionTime 
	,InvestigationCode
	,InvestigationName
	,InvestigationComment
	,ResultCode
	,ResultName
	,Result 
	,UnitOfMeasurement 
	,Abnormal 
	,LowerReferenceRangeValue 
	,UpperReferenceRangeValue 
	,ResultComment
from
	(
	select --top 20 
		SourceUniqueID = ReportResults.Result_Index
		,PatientID = ReportSummary.Patient_Id_Key
		,PatientIdentifier = ReportSummary.HospitalNumber
		,DistrictNo = 
					case
					when 
						isnumeric(ReportSummary.HospitalNumber) = 1
					and len(ReportSummary.HospitalNumber) = 8
					then ReportSummary.HospitalNumber
					else DistrictNo.HospitalNumber
					end
		,CasenoteNumber = CasenoteNumber.HospitalNumber
		,NHSNumber = nullif(PatientSummary.New_NHS_No,'')			
		,DateOfBirth = 	PatientSummary.Date_Of_Birth
		,SexCode = PatientSummary.Sex
		,ClinicianID = ReportSummary.Clinician_Index
		,MainSpecialtyCode = ConsultantSummary.Specialty_Code
		,SpecialtyCode = ReportSummary.Specialty
		,LocationID = ReportSummary.Location_Index
		,ReportSourceUniqueID = ReportSummary.Report_ID
		,ReportStatusCode = ReportSummary.Report_Status 
		,ReportTime = ReportSummary.DateTime_Of_Report
		,SampleReferenceCode = ReportSamples.Sample_Number 
		,SampleTypeCode = ReportSamples.Sample_Code
		,SampleType = ReportSamples.Sample_Text
		,SampleCollectionTime = ReportSamples.Collection_DateTime
		,InvestigationCode = ReportInvestigations.Investigation_Code
		,InvestigationName = ReportInvestigations.Investigation_Name
		,InvestigationComment = InvestigationComments.Comment
		,ResultCode = ReportResults.Result_Code 
		,ResultName = ReportResults.Result_Name
		,Result = nullif(ReportResults.Result,'')
		,UnitOfMeasurement = nullif(ReportResults.Unit_Of_Measure,'')
		--,UnitOfMeasurement = coalesce(
		--					nullif(UnitOfMeasurement.UOM_Text, '')
		--					,nullif(UnitOfMeasurement.UOM_Code, '')
		--					)							
		,Abnormal = ReportResults.Abnormal_Flag
		,LowerReferenceRangeValue = nullif(ReferenceRanges.Lower_Range,'')
		,UpperReferenceRangeValue = nullif(ReferenceRanges.Upper_Range,'')
		,ResultComment = ResultComments.Comment
		,ReportComment = ReportComments.Comment
	from
		[$(ICE_GP)].dbo.ICEView_ReportSummary ReportSummary 

	inner join [$(ICE_GP)].dbo.ICEView_ConsultantSummary ConsultantSummary 
	on ConsultantSummary.Clinician_Index = ReportSummary.Clinician_Index 

	inner join [$(ICE_GP)].dbo.ICEView_PatientSummary PatientSummary
	on ReportSummary.Patient_Id_Key = PatientSummary.Patient_Id_Key

	inner join [$(ICE_GP)].dbo.ICEView_ReportInvestigations ReportInvestigations 
	on ReportInvestigations.Service_Report_Index = ReportSummary.Service_Report_Index

	left join 
			(
			select 
				InvestigationComments.Investigation_Index
				,Comment = 				
					replace(			
					replace(
					replace(
						(
						select
							stuff((
								select
									'||' + ltrim(rtrim(InvestigationCommentsText.Comment)) 
								from
									[$(ICE_GP)].dbo.ICEView_InvestigationComments InvestigationCommentsText
								where
									InvestigationCommentsText.Investigation_Index = InvestigationComments.Investigation_Index
								for xml path('') 
								), 1, 2, '')
						) 
					,'&amp;', '&'
					)
					,'&lt;', '<'
					)
					,'&gt;', '>'
					)
					
			from
				[$(ICE_GP)].dbo.ICEView_InvestigationComments InvestigationComments 

			group by
				InvestigationComments.Investigation_Index
			) InvestigationComments
	on	InvestigationComments.Investigation_Index = ReportInvestigations.Investigation_Index

	inner join [$(ICE_GP)].dbo.ICEView_ReportSamples ReportSamples 
	on	ReportSamples.Sample_Index = ReportInvestigations.Sample_Index 
	and ReportSamples.Service_Report_Index = ReportInvestigations.Service_Report_Index

	inner join [$(ICE_GP)].dbo.ICEView_ReportResults ReportResults 
	on	ReportResults.Investigation_Index = ReportInvestigations.Investigation_Index 
	and ReportResults.Sample_Index = ReportInvestigations.Sample_Index

	inner join [$(ICE_GP)].dbo.Service_Results UnitOfMeasurement
	on	UnitOfMeasurement.Result_Index = ReportResults.Result_Index 

	left join 
			(
			select 
				ReportComments.Service_Report_Index
				,Comment = 				
					replace(			
					replace(
					replace(
						(
						select
							stuff((
								select
									'||' + ltrim(rtrim(ReportCommentsText.Comment)) 
								from
									[$(ICE_GP)].dbo.ICEView_ReportComments ReportCommentsText
								where
									ReportCommentsText.Service_Report_Index = ReportComments.Service_Report_Index
								for xml path('') 
								), 1, 2, '')
						) 
					,'&amp;', '&'
					)
					,'&lt;', '<'
					)
					,'&gt;', '>'
					)
			from
				[$(ICE_GP)].dbo.ICEView_ReportComments ReportComments

			group by
				ReportComments.Service_Report_Index
			) ReportComments
	on	ReportComments.Service_Report_Index = ReportSummary.Service_Report_Index

	left join 
			(
			select 
				ResultComments.Result_Index
				,Comment = 				
					replace(			
					replace(
					replace(
						(
						select
							stuff((
								select
									'||' + ltrim(rtrim(ResultCommentsText.Comment)) 
								from
									[$(ICE_GP)].dbo.ICEView_ResultComments ResultCommentsText
								where
									ResultCommentsText.Result_Index = ResultComments.Result_Index
								for xml path('') 
								), 1, 2, '')
						) 
					,'&amp;', '&'
					)
					,'&lt;', '<'
					)
					,'&gt;', '>'
					)
			from
				[$(ICE_GP)].dbo.ICEView_ResultComments ResultComments

			group by
				ResultComments.Result_Index
			) ResultComments
	on	ReportResults.Result_Index = ResultComments.Result_Index

	left join [$(ICE_GP)].dbo.Service_Ranges ReferenceRanges
	on ReferenceRanges.Result_Index = ReportResults.Result_Index	

	left join
		(
		select
			Patient_Id_Key
			,HospitalNumber = max(HospitalNumber)
		from
			[$(ICE_GP)].dbo.ICEView_PatientHospitalNumbers DistrictNo
		where
			Main_Identifier = 1
		and	isnumeric(HospitalNumber) = 1
		and len(HospitalNumber) = 8
		and	Retired = 0
		group by
			Patient_Id_Key
		) DistrictNo
	on	ReportSummary.Patient_Id_Key = DistrictNo.Patient_Id_Key

	left join
		(
		select
			Patient_Id_Key
			,HospitalNumber = max(HospitalNumber)
		from
			[$(ICE_GP)].dbo.ICEView_PatientHospitalNumbers
		where
			charindex('/', HospitalNumber) > 0
		and	Retired = 0
		group by
			Patient_Id_Key
		) CasenoteNumber
	on	ReportSummary.Patient_Id_Key = CasenoteNumber.Patient_Id_Key

	where
		cast(ReportSummary.DateTime_Of_Report as date) between @LocalStartDate and @LocalEndDate

	) Result



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


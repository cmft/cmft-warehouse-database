﻿
CREATE PROCEDURE [ETL].[LoadSymphonyPatientDetail]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony PatientDetail table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			PatientID
			,GpID
			,PracticeID
			,HonoursID
			,PracticeAddressID
			,NoteID
			,DateOfDeath
			,MaritalStatusID
			,ReligionID
			,EthnicCategoryID
			,Occupation
			,EmploymentStatusID
			,OverseasVisitorStatusID
			,NationalityID
			,PUPID
			,PEPID
			,CreatedByID
			,UpdatedTime
			,GpMoveID
			,SpecialCase
			,IsSpecialCase
			)
into
	#TLoadSymphonyPatient
from
	(
	select
		PatientID = cast(PatientID as int)
		,GpID = cast(GpID as int)
		,PracticeID = cast(PracticeID as int)
		,HonoursID = cast(HonoursID as int)
		,PracticeAddressID = cast(PracticeAddressID as int)
		,NoteID = cast(NoteID as int)
		,DateOfDeath = cast(DateOfDeath as datetime)
		,MaritalStatusID = cast(MaritalStatusID as int)
		,ReligionID = cast(ReligionID as int)
		,EthnicCategoryID = cast(EthnicCategoryID as int)
		,Occupation = cast(Occupation as varchar(50))
		,EmploymentStatusID = cast(EmploymentStatusID as int)
		,OverseasVisitorStatusID = cast(OverseasVisitorStatusID as int)
		,NationalityID = cast(NationalityID as int)
		,PUPID = cast(PUPID as int)
		,PEPID = cast(PEPID as int)
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,GpMoveID = cast(GpMoveID as int)
		,SpecialCase = cast(SpecialCase as varchar(255))
		,IsSpecialCase = cast(IsSpecialCase as bit)
	from
		(
		select
			PatientID = Patient_details.pdt_pid
			,GpID = Patient_details.pdt_gpid
			,PracticeID = Patient_details.pdt_practise
			,HonoursID = Patient_details.pdt_honours
			,PracticeAddressID = Patient_details.pdt_praddid
			,NoteID = Patient_details.pdt_noteid
			,DateOfDeath = Patient_details.pdt_dod
			,MaritalStatusID = Patient_details.pdt_maritalstatus
			,ReligionID = Patient_details.pdt_religion
			,EthnicCategoryID = Patient_details.pdt_ethnic
			,Occupation = Patient_details.pdt_occupation
			,EmploymentStatusID = Patient_details.pdt_employmentstatus
			,OverseasVisitorStatusID = Patient_details.pdt_overseas
			,NationalityID = Patient_details.pdt_nationality
			,PUPID = Patient_details.pdt_PUP
			,PEPID = Patient_details.pdt_PEP
			,CreatedByID = Patient_details.pdt_createdby
			,UpdatedTime = Patient_details.pdt_update
			,GpMoveID = Patient_details.pdt_gpmove
			,SpecialCase = Patient_details.pdt_SpecialCaseUwm
			,IsSpecialCase = Patient_details.pdt_IsSpecialCase
		from
			[$(Symphony)].dbo.Patient_details

		) Encounter
	) Encounter
order by
	Encounter.PatientID


create unique clustered index #IX_TLoadSymphonyPatient on #TLoadSymphonyPatient
	(
	PatientID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.PatientDetail target
using
	(
	select
		PatientID
		,GpID
		,PracticeID
		,HonoursID
		,PracticeAddressID
		,NoteID
		,DateOfDeath
		,MaritalStatusID
		,ReligionID
		,EthnicCategoryID
		,Occupation
		,EmploymentStatusID
		,OverseasVisitorStatusID
		,NationalityID
		,PUPID
		,PEPID
		,CreatedByID
		,UpdatedTime
		,GpMoveID
		,SpecialCase
		,IsSpecialCase
		,EncounterChecksum
	from
		#TLoadSymphonyPatient
	
	) source
	on	source.PatientID = target.PatientID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			PatientID
			,GpID
			,PracticeID
			,HonoursID
			,PracticeAddressID
			,NoteID
			,DateOfDeath
			,MaritalStatusID
			,ReligionID
			,EthnicCategoryID
			,Occupation
			,EmploymentStatusID
			,OverseasVisitorStatusID
			,NationalityID
			,PUPID
			,PEPID
			,CreatedByID
			,UpdatedTime
			,GpMoveID
			,SpecialCase
			,IsSpecialCase

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			source.PatientID
			,source.GpID
			,source.PracticeID
			,source.HonoursID
			,source.PracticeAddressID
			,source.NoteID
			,source.DateOfDeath
			,source.MaritalStatusID
			,source.ReligionID
			,source.EthnicCategoryID
			,source.Occupation
			,source.EmploymentStatusID
			,source.OverseasVisitorStatusID
			,source.NationalityID
			,source.PUPID
			,source.PEPID
			,source.CreatedByID
			,source.UpdatedTime
			,source.GpMoveID
			,source.SpecialCase
			,source.IsSpecialCase

			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			target.GpID = source.GpID
			,target.PracticeID = source.PracticeID
			,target.HonoursID = source.HonoursID
			,target.PracticeAddressID = source.PracticeAddressID
			,target.NoteID = source.NoteID
			,target.DateOfDeath = source.DateOfDeath
			,target.MaritalStatusID = source.MaritalStatusID
			,target.ReligionID = source.ReligionID
			,target.EthnicCategoryID = source.EthnicCategoryID
			,target.Occupation = source.Occupation
			,target.EmploymentStatusID = source.EmploymentStatusID
			,target.OverseasVisitorStatusID = source.OverseasVisitorStatusID
			,target.NationalityID = source.NationalityID
			,target.PUPID = source.PUPID
			,target.PEPID = source.PEPID
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.GpMoveID = source.GpMoveID
			,target.SpecialCase = source.SpecialCase
			,target.IsSpecialCase = source.IsSpecialCase

			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
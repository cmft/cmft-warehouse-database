﻿
CREATE procedure [ETL].[ExtractInquireGpHistory]

as

truncate table ETL.TImportPASGpHistory

insert into ETL.TImportPASGpHistory

select
	[GPHISTORYID]
	,[DistrictNumber]
	,[FromDate]
	,[FromDtTmInt]
	,[GpAddrLine1]
	,[GpAddrLine2]
	,[GpAddrLine3]
	,[GpAddrLine4]
	,[GpCode]
	,[GpfhCode]
	,[Initials]
	,[InternalPatientNumber]
	,[NationalCode]
	,[Phone]
	,[PmiPatientGpHistoryToDateIndexKey]
	,[Postcode]
	,[Practice]
	,[SeqNo]
	,[Surname]
	,[Title]
	,[ToDate]
from
	[$(PAS)].[Inquire].[GPHISTORY]


﻿create procedure [ETL].[LoadIUI]
	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null
as


set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @FromDate = coalesce(@FromDate, (select cast(dateadd(month, -12, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

exec ETL.LoadIUIReferenceData
exec ETL.LoadIUIEncounter


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	convert(varchar(6), @Elapsed) + ' Mins, Period ' + 
	convert(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	convert(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
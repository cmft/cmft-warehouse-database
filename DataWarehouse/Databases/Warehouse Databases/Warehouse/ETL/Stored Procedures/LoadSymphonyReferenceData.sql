﻿
CREATE PROCEDURE [ETL].[LoadSymphonyReferenceData]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony simple reference tables

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


	
--GpPractice
merge
	Symphony.GpPractice target
using
	(
	select
		*
		,RowChecksum =
			checksum(
				GpPracticeID
				,GpPracticeCode
				,TelephoneID
				,GPFHCode
				,HealthVisitor
				,LocalCode
				,StatusID
				,LocalGpID
				,PracticeGroupCareCode
				,CreatedByID
				,UpdatedTime
				,LetterPreference
				,CreatedTime
				,UpdatedByID
				,RecordVersion
			)
	from
		(
		select
			GpPracticeID
			,GpPracticeCode = cast(GpPracticeCode as varchar(200))
			,TelephoneID = cast(TelephoneID as int)
			,GPFHCode = cast(GPFHCode as varchar(3))
			,HealthVisitor = cast(HealthVisitor as varchar(56))
			,LocalCode = cast(LocalCode as varchar(56))
			,StatusID = cast(StatusID as int)
			,LocalGpID = cast(LocalGpID as int)
			,PracticeGroupCareCode = cast(PracticeGroupCareCode as varchar(35))
			,CreatedByID = cast(CreatedByID as int)
			,UpdatedTime = cast(UpdatedTime as datetime)
			,LetterPreference = cast(LetterPreference as varchar(35))
			,CreatedTime = cast(CreatedTime as datetime)
			,UpdatedByID = cast(UpdatedByID as int)
			,RecordVersion = cast(RecordVersion as int)
		from
			(

			select
				GpPracticeID = Gp_Practise.Pr_id
				,GpPracticeCode = Gp_Practise.pr_praccode
				,TelephoneID = Gp_Practise.pr_telid
				,GPFHCode = Gp_Practise.pr_gpfhcode
				,HealthVisitor = Gp_Practise.pr_healthvisitor
				,LocalCode = Gp_Practise.pr_localcode
				,StatusID = Gp_Practise.pr_status
				,LocalGpID = Gp_Practise.pr_localgp
				,PracticeGroupCareCode = Gp_Practise.pr_pgrpcarecode
				,CreatedByID = Gp_Practise.pr_createdby
				,UpdatedTime = Gp_Practise.pr_update
				,LetterPreference = Gp_Practise.pr_letterpref
				,CreatedTime = null --Gp_Practise.pr_created
				,UpdatedByID = null --Gp_Practise.pr_updatedby
				,RecordVersion = null --Gp_Practise.pr_RecVersion
			from
				[$(Symphony)].dbo.Gp_Practise

			) ReferenceData
		) ReferenceData
	
	) source
	on	source.GpPracticeID = target.GpPracticeID

	--when not matched by source
	--then delete

	when not matched
	then
		insert
			(
			GpPracticeID
			,GpPracticeCode
			,TelephoneID
			,GPFHCode
			,HealthVisitor
			,LocalCode
			,StatusID
			,LocalGpID
			,PracticeGroupCareCode
			,CreatedByID
			,UpdatedTime
			,LetterPreference
			,CreatedTime
			,UpdatedByID
			,RecordVersion

			,Created
			,ByWhom
			)
		values
			(
			source.GpPracticeID
			,source.GpPracticeCode
			,source.TelephoneID
			,source.GPFHCode
			,source.HealthVisitor
			,source.LocalCode
			,source.StatusID
			,source.LocalGpID
			,source.PracticeGroupCareCode
			,source.CreatedByID
			,source.UpdatedTime
			,source.LetterPreference
			,source.CreatedTime
			,source.UpdatedByID
			,source.RecordVersion

			,getdate()
			,suser_name()
			)

	when matched
	and source.RowChecksum <> 
			checksum(
				target.GpPracticeID
				,target.GpPracticeCode
				,target.TelephoneID
				,target.GPFHCode
				,target.HealthVisitor
				,target.LocalCode
				,target.StatusID
				,target.LocalGpID
				,target.PracticeGroupCareCode
				,target.CreatedByID
				,target.UpdatedTime
				,target.LetterPreference
				,target.CreatedTime
				,target.UpdatedByID
				,target.RecordVersion
			)

	then
		update
		set
			target.GpPracticeID = source.GpPracticeID
			,target.GpPracticeCode = source.GpPracticeCode
			,target.TelephoneID = source.TelephoneID
			,target.GPFHCode = source.GPFHCode
			,target.HealthVisitor = source.HealthVisitor
			,target.LocalCode = source.LocalCode
			,target.StatusID = source.StatusID
			,target.LocalGpID = source.LocalGpID
			,target.PracticeGroupCareCode = source.PracticeGroupCareCode
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.LetterPreference = source.LetterPreference
			,target.CreatedTime = source.CreatedTime
			,target.UpdatedByID = source.UpdatedByID
			,target.RecordVersion = source.RecordVersion

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

;

	
--Gp
merge
	Symphony.Gp target
using
	(
	select
		*
		,RowChecksum =
			checksum(
				GpID
				,GpCode
				,StartDate
				,EndDate
				,Surname
				,Initials
				,TitleID
				,QualificationsID
				,LetterPreference
				,CreatedByID
				,UpdatedTime
				,CreatedTime
				,UpdatedByID
				,RecordVersion
			)
	from
		(
		select
			GpID
			,GpCode = cast(GpCode as varchar(200))
			,StartDate = cast(StartDate as date)
			,EndDate = cast(EndDate as date)
			,Surname = cast(Surname as varchar(35))
			,Initials = cast(Initials as varchar(30))
			,TitleID = cast(TitleID as int)
			,QualificationsID = cast(QualificationsID as int)
			,LetterPreference = cast(LetterPreference as varchar(35))
			,CreatedByID = cast(CreatedByID as int)
			,UpdatedTime = cast(UpdatedTime as datetime)
			,CreatedTime = cast(CreatedTime as datetime)
			,UpdatedByID = cast(UpdatedByID as int)
			,RecordVersion = cast(RecordVersion as int)
		from
			(

			select
				GpID = Gp.gp_id
				,GpCode = Gp.gp_code
				,StartDate = Gp.gp_startdate
				,EndDate = Gp.gp_enddate
				,Surname = Gp.gp_surname
				,Initials = Gp.gp_initials
				,TitleID = Gp.gp_title
				,QualificationsID = Gp.gp_qualifications
				,LetterPreference = Gp.gp_letterpref
				,CreatedByID = Gp.gp_createdby
				,UpdatedTime = Gp.gp_update
				,CreatedTime = null --Gp.gp_created
				,UpdatedByID = null --Gp.gp_updatedby
				,RecordVersion = null --Gp.gp_RecVersion
			from
				[$(Symphony)].dbo.Gp

			) ReferenceData
		) ReferenceData
	
	) source
	on	source.GpID = target.GpID

	--when not matched by source
	--then delete

	when not matched
	then
		insert
			(
			GpID
			,GpCode
			,StartDate
			,EndDate
			,Surname
			,Initials
			,TitleID
			,QualificationsID
			,LetterPreference
			,CreatedByID
			,UpdatedTime
			,CreatedTime
			,UpdatedByID
			,RecordVersion

			,Created
			,ByWhom
			)
		values
			(
			source.GpID
			,source.GpCode
			,source.StartDate
			,source.EndDate
			,source.Surname
			,source.Initials
			,source.TitleID
			,source.QualificationsID
			,source.LetterPreference
			,source.CreatedByID
			,source.UpdatedTime
			,source.CreatedTime
			,source.UpdatedByID
			,source.RecordVersion

			,getdate()
			,suser_name()
			)

	when matched
	and source.RowChecksum <> 
			checksum(
				target.GpID
				,target.GpCode
				,target.StartDate
				,target.EndDate
				,target.Surname
				,target.Initials
				,target.TitleID
				,target.QualificationsID
				,target.LetterPreference
				,target.CreatedByID
				,target.UpdatedTime
				,target.CreatedTime
				,target.UpdatedByID
				,target.RecordVersion
			)

	then
		update
		set
			target.GpID = source.GpID
			,target.GpCode = source.GpCode
			,target.StartDate = source.StartDate
			,target.EndDate = source.EndDate
			,target.Surname = source.Surname
			,target.Initials = source.Initials
			,target.TitleID = source.TitleID
			,target.QualificationsID = source.QualificationsID
			,target.LetterPreference = source.LetterPreference
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.CreatedTime = source.CreatedTime
			,target.UpdatedByID = source.UpdatedByID
			,target.RecordVersion = source.RecordVersion

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

;
	
--Department
merge
	Symphony.Department target
using
	(
	select
		*
		,RowChecksum =
			checksum(
				 DepartmentID
				,Department
				,DepartmentShortName
				,HasBeenUsed
				,CreatedTime
				,CreatedByID
				,UpdatedTime
				,UpdatedByID
				,IsServiceDepartment
			)
	from
		(
		select
			 DepartmentID
			,Department = cast(Department as varchar(20))
			,DepartmentShortName = cast(DepartmentShortName as varchar(4))
			,HasBeenUsed = cast(HasBeenUsed as bit)
			,CreatedTime = cast(CreatedTime as datetime)
			,CreatedByID = cast(CreatedByID as int)
			,UpdatedTime = cast(UpdatedTime as datetime)
			,UpdatedByID = cast(UpdatedByID as int)
			,IsServiceDepartment = cast(IsServiceDepartment as bit)
		from
			(
			select
				 DepartmentID = CFG_Dept.dpt_id
				,Department = CFG_Dept.dpt_name
				,DepartmentShortName = CFG_Dept.dpt_shortname
				,HasBeenUsed = CFG_Dept.dpt_hasbeenused
				,CreatedTime = CFG_Dept.dpt_created
				,CreatedByID = CFG_Dept.dpt_createdby
				,UpdatedTime = CFG_Dept.dpt_updated
				,UpdatedByID = CFG_Dept.dpt_updatedby
				,IsServiceDepartment = CFG_Dept.dpt_IsServiceDept
			from
				[$(Symphony)].dbo.CFG_Dept

			) ReferenceData
		) ReferenceData
	
	) source
	on	source.DepartmentID = target.DepartmentID

	--when not matched by source
	--then delete

	when not matched
	then
		insert
			(
			 DepartmentID
			,Department
			,DepartmentShortName
			,HasBeenUsed
			,CreatedTime
			,CreatedByID
			,UpdatedTime
			,UpdatedByID
			,IsServiceDepartment

			,Created
			,ByWhom
			)
		values
			(
			 source.DepartmentID
			,source.Department
			,source.DepartmentShortName
			,source.HasBeenUsed
			,source.CreatedTime
			,source.CreatedByID
			,source.UpdatedTime
			,source.UpdatedByID
			,source.IsServiceDepartment
			,getdate()
			,suser_name()
			)

	when matched
	and source.RowChecksum <> 
			checksum(
				 target.DepartmentID
				,target.Department
				,target.DepartmentShortName
				,target.HasBeenUsed
				,target.CreatedTime
				,target.CreatedByID
				,target.UpdatedTime
				,target.UpdatedByID
				,target.IsServiceDepartment
			)

	then
		update
		set
			 target.DepartmentID = source.DepartmentID
			,target.Department = source.Department
			,target.DepartmentShortName = source.DepartmentShortName
			,target.HasBeenUsed = source.HasBeenUsed
			,target.CreatedTime = source.CreatedTime
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.UpdatedByID = source.UpdatedByID
			,target.IsServiceDepartment = source.IsServiceDepartment

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

;


--Location
merge
	Symphony.Location target
using
	(
	select
		*
		,RowChecksum =
			checksum(
				 LocationID
				,LocationParentID
				,Location
				,LocationIndex
				,IsActive
				,LocationDescription
				,CreatedByID
				,UpdatedTime
				,ReasonForSuspension
				,SuspendedTime
				,SuspendedByID
				,DepartmentID
				,IsUsed
			)
	from
		(
		select
			 LocationID
			,LocationParentID = cast(LocationParentID as smallint)
			,Location = cast(Location as varchar(100))
			,LocationIndex = cast(LocationIndex as smallint)
			,IsActive = cast(IsActive as tinyint)
			,LocationDescription = cast(LocationDescription as varchar(255))
			,CreatedByID = cast(CreatedByID as int)
			,UpdatedTime = cast(UpdatedTime as datetime)
			,ReasonForSuspension = cast(ReasonForSuspension as varchar(255))
			,SuspendedTime = cast(SuspendedTime as datetime)
			,SuspendedByID = cast(SuspendedByID as int)
			,DepartmentID = cast(DepartmentID as int)
			,IsUsed = cast(IsUsed as bit)
		from
			(
			select
				 LocationID = CFG_Locations.loc_id
				,LocationParentID = CFG_Locations.loc_parentid
				,Location = CFG_Locations.loc_name
				,LocationIndex = CFG_Locations.loc_index
				,IsActive = CFG_Locations.loc_active
				,LocationDescription = CFG_Locations.loc_description
				,CreatedByID = CFG_Locations.loc_createdby
				,UpdatedTime = CFG_Locations.loc_update
				,ReasonForSuspension = CFG_Locations.loc_reasonforsuspension
				,SuspendedTime = CFG_Locations.loc_suspendedon
				,SuspendedByID = CFG_Locations.loc_suspendedby
				,DepartmentID = CFG_Locations.loc_deptid
				,IsUsed = CFG_Locations.loc_used
			from
				[$(Symphony)].dbo.CFG_Locations

			) ReferenceData
		) ReferenceData
	
	) source
	on	source.LocationID = target.LocationID

	--when not matched by source
	--then delete

	when not matched
	then
		insert
			(
			 LocationID
			,LocationParentID
			,Location
			,LocationIndex
			,IsActive
			,LocationDescription
			,CreatedByID
			,UpdatedTime
			,ReasonForSuspension
			,SuspendedTime
			,SuspendedByID
			,DepartmentID
			,IsUsed

			,Created
			,ByWhom
			)
		values
			(
			 source.LocationID
			,source.LocationParentID
			,source.Location
			,source.LocationIndex
			,source.IsActive
			,source.LocationDescription
			,source.CreatedByID
			,source.UpdatedTime
			,source.ReasonForSuspension
			,source.SuspendedTime
			,source.SuspendedByID
			,source.DepartmentID
			,source.IsUsed
			,getdate()
			,suser_name()
			)

	when matched
	and source.RowChecksum <> 
			checksum(
				 target.LocationID
				,target.LocationParentID
				,target.Location
				,target.LocationIndex
				,target.IsActive
				,target.LocationDescription
				,target.CreatedByID
				,target.UpdatedTime
				,target.ReasonForSuspension
				,target.SuspendedTime
				,target.SuspendedByID
				,target.DepartmentID
				,target.IsUsed
			)

	then
		update
		set
			 target.LocationID = source.LocationID
			,target.LocationParentID = source.LocationParentID
			,target.Location = source.Location
			,target.LocationIndex = source.LocationIndex
			,target.IsActive = source.IsActive
			,target.LocationDescription = source.LocationDescription
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.ReasonForSuspension = source.ReasonForSuspension
			,target.SuspendedTime = source.SuspendedTime
			,target.SuspendedByID = source.SuspendedByID
			,target.DepartmentID = source.DepartmentID
			,target.IsUsed = source.IsUsed

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

;

--MappingType
merge
	Symphony.MappingType target
using
	(
	select
		*
		,RowChecksum =
			checksum(
				 MappingTypeID
				,MappingType
				,MappingTypeDescription
				,IsUsed
				,CreatedByID
				,CreatedTime
			)
	from
		(
		select
			 MappingTypeID = cast(MappingTypeID as int)
			,MappingType = cast(MappingType as varchar(200))
			,MappingTypeDescription = cast(MappingTypeDescription as varchar(200))
			,IsUsed = cast(IsUsed as bit)
			,CreatedByID = cast(CreatedByID as int)
			,CreatedTime = cast(CreatedTime as datetime)
		from
			(
			select
				 MappingTypeID = MappingTypes.mt_id
				,MappingType = MappingTypes.mt_name
				,MappingTypeDescription = MappingTypes.mt_description
				,IsUsed = MappingTypes.mt_used
				,CreatedByID = MappingTypes.mt_createdby
				,CreatedTime = MappingTypes.mt_datecreated
			from
				[$(Symphony)].dbo.MappingTypes
			) ReferenceData
		) ReferenceData
	
	) source
	on	source.MappingTypeID = target.MappingTypeID

	--when not matched by source
	--then delete

	when not matched
	then
		insert
			(
			 MappingTypeID
			,MappingType
			,MappingTypeDescription
			,IsUsed
			,CreatedByID
			,CreatedTime

			,Created
			,ByWhom
			)
		values
			(
			 source.MappingTypeID
			,source.MappingType
			,source.MappingTypeDescription
			,source.IsUsed
			,source.CreatedByID
			,source.CreatedTime
			,getdate()
			,suser_name()
			)

	when matched
	and source.RowChecksum <> 
			checksum(
				 target.MappingTypeID
				,target.MappingType
				,target.MappingTypeDescription
				,target.IsUsed
				,target.CreatedByID
				,target.CreatedTime
			)

	then
		update
		set
			 target.MappingTypeID = source.MappingTypeID
			,target.MappingType = source.MappingType
			,target.MappingTypeDescription = source.MappingTypeDescription
			,target.IsUsed = source.IsUsed
			,target.CreatedByID = source.CreatedByID
			,target.CreatedTime = source.CreatedTime

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

;


--ReportSetting
merge
	Symphony.ReportSetting target
using
	(
	select
		*
		,RowChecksum =
			checksum(
				 ReportSettingID
				,ReportSetting
				,ValueID
				,DepartmentID
			)
	from
		(
		select
			 ReportSettingID
			,ReportSetting = cast(ReportSetting as varchar(50))
			,ValueID = cast(ValueID as int)
			,DepartmentID = cast(DepartmentID as int)
		from
			(
			select
				 ReportSettingID = CFG_ReportSettings.rps_id
				,ReportSetting = CFG_ReportSettings.rps_name
				,ValueID = CFG_ReportSettings.rps_value
				,DepartmentID = CFG_ReportSettings.rps_deptid
			from
				[$(Symphony)].dbo.CFG_ReportSettings

			) ReferenceData
		) ReferenceData
	
	) source
	on	source.ReportSettingID = target.ReportSettingID

	--when not matched by source
	--then delete

	when not matched
	then
		insert
			(
			 ReportSettingID
			,ReportSetting
			,ValueID
			,DepartmentID

			,Created
			,ByWhom
			)
		values
			(
			 source.ReportSettingID
			,source.ReportSetting
			,source.ValueID
			,source.DepartmentID
			,getdate()
			,suser_name()
			)

	when matched
	and source.RowChecksum <> 
			checksum(
				 target.ReportSettingID
				,target.ReportSetting
				,target.ValueID
				,target.DepartmentID
			)

	then
		update
		set
			 target.ReportSettingID = source.ReportSettingID
			,target.ReportSetting = source.ReportSetting
			,target.ValueID = source.ValueID
			,target.DepartmentID = source.DepartmentID

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

;

--Staff
merge
	Symphony.Staff target
using
	(
	select
		*
		,RowChecksum =
			checksum(
				 StaffID
				,Surname
				,TitleID
				,Forename
				,MiddleNames
				,HonorsID
				,JobTypeID
				,StartTime
				,EndTime
				,SpecialtyID
				,LogonId
				,JobTitle
				,PasswordUpdatedTime
				,FailedLogons
				,IsSuspended
				,CreatedByID
				,IsDeleted
				,CreatedTime
				,UpdatedTime
				,HmeDepartment
				,IsDutyClinician
			)
	from
		(
		select
			 StaffID
			,Surname = cast(Surname as varchar(35))
			,TitleID = cast(TitleID as int)
			,Forename = cast(Forename as varchar(35))
			,MiddleNames = cast(MiddleNames as varchar(35))
			,HonorsID = cast(HonorsID as int)
			,JobTypeID = cast(JobTypeID as int)
			,StartTime = cast(StartTime as datetime)
			,EndTime = cast(EndTime as datetime)
			,SpecialtyID = cast(SpecialtyID as int)
			,LogonId = cast(LogonId as varchar(30))
			,JobTitle = cast(JobTitle as varchar(30))
			,PasswordUpdatedTime = cast(PasswordUpdatedTime as datetime)
			,FailedLogons = cast(FailedLogons as tinyint)
			,IsSuspended = cast(IsSuspended as bit)
			,CreatedByID = cast(CreatedByID as int)
			,IsDeleted = cast(IsDeleted as bit)
			,CreatedTime = cast(CreatedTime as datetime)
			,UpdatedTime = cast(UpdatedTime as datetime)
			,HmeDepartment = cast(HmeDepartment as int)
			,IsDutyClinician = cast(IsDutyClinician as bit)
		from
			(
			select
				 StaffID = Staff.stf_staffid
				,Surname = Staff.stf_surname
				,TitleID = Staff.stf_title
				,Forename = Staff.stf_forename
				,MiddleNames = Staff.stf_midnames
				,HonorsID = Staff.stf_honors
				,JobTypeID = Staff.stf_jobtype
				,StartTime = Staff.stf_startdate
				,EndTime = Staff.stf_enddate
				,SpecialtyID = Staff.stf_specialty
				,LogonId = Staff.stf_logonid
				,JobTitle = Staff.stf_jobtitle
				,PasswordUpdatedTime = Staff.stf_passwordupdated
				,FailedLogons = Staff.stf_failedlogons
				,IsSuspended = Staff.stf_suspended
				,CreatedByID = Staff.stf_createdby
				,IsDeleted = Staff.stf_deleted
				,CreatedTime = Staff.stf_created
				,UpdatedTime = Staff.stf_update
				,HmeDepartment = Staff.stf_hmedept
				,IsDutyClinician = Staff.stf_dutyclinician
			from
				[$(Symphony)].dbo.Staff

			) ReferenceData

		) ReferenceData
	
	) source
	on	source.StaffID = target.StaffID

	--when not matched by source
	--then delete

	when not matched
	then
		insert
			(
			 StaffID
			,Surname
			,TitleID
			,Forename
			,MiddleNames
			,HonorsID
			,JobTypeID
			,StartTime
			,EndTime
			,SpecialtyID
			,LogonId
			,JobTitle
			,PasswordUpdatedTime
			,FailedLogons
			,IsSuspended
			,CreatedByID
			,IsDeleted
			,CreatedTime
			,UpdatedTime
			,HmeDepartment
			,IsDutyClinician

			,Created
			,ByWhom
			)
		values
			(
			 source.StaffID
			,source.Surname
			,source.TitleID
			,source.Forename
			,source.MiddleNames
			,source.HonorsID
			,source.JobTypeID
			,source.StartTime
			,source.EndTime
			,source.SpecialtyID
			,source.LogonId
			,source.JobTitle
			,source.PasswordUpdatedTime
			,source.FailedLogons
			,source.IsSuspended
			,source.CreatedByID
			,source.IsDeleted
			,source.CreatedTime
			,source.UpdatedTime
			,source.HmeDepartment
			,source.IsDutyClinician
			,getdate()
			,suser_name()
			)

	when matched
	and source.RowChecksum <> 
			checksum(
				 target.StaffID
				,target.Surname
				,target.TitleID
				,target.Forename
				,target.MiddleNames
				,target.HonorsID
				,target.JobTypeID
				,target.StartTime
				,target.EndTime
				,target.SpecialtyID
				,target.LogonId
				,target.JobTitle
				,target.PasswordUpdatedTime
				,target.FailedLogons
				,target.IsSuspended
				,target.CreatedByID
				,target.IsDeleted
				,target.CreatedTime
				,target.UpdatedTime
				,target.HmeDepartment
				,target.IsDutyClinician
			)

	then
		update
		set
			 target.StaffID = source.StaffID
			,target.Surname = source.Surname
			,target.TitleID = source.TitleID
			,target.Forename = source.Forename
			,target.MiddleNames = source.MiddleNames
			,target.HonorsID = source.HonorsID
			,target.JobTypeID = source.JobTypeID
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.SpecialtyID = source.SpecialtyID
			,target.LogonId = source.LogonId
			,target.JobTitle = source.JobTitle
			,target.PasswordUpdatedTime = source.PasswordUpdatedTime
			,target.FailedLogons = source.FailedLogons
			,target.IsSuspended = source.IsSuspended
			,target.CreatedByID = source.CreatedByID
			,target.IsDeleted = source.IsDeleted
			,target.CreatedTime = source.CreatedTime
			,target.UpdatedTime = source.UpdatedTime
			,target.HmeDepartment = source.HmeDepartment
			,target.IsDutyClinician = source.IsDutyClinician

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

;


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
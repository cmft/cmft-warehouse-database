﻿


CREATE proc [ETL].[ExtractCRTPEXTabletResponse]

 @FromDate smalldatetime = null
, @ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
select @StartTime = getdate()

select @RowsInserted = 0

--select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -12, getdate()))), 0), 112) 

--select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)

truncate table ETL.TImportPEXTabletResponse

insert into	ETL.TImportPEXTabletResponse

(
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
)

select
	SurveyTakenID
	,SurveyID
	,QuestionID
	,AnswerID
	,Answer
	,SurveyDate
	,SurveyTime
	,LocationID
	,DeviceNumber
	,ChannelID
	,QuestionTypeID
	,LocationTypeID
from
	(
	select
		SurveyTakenID = SurveyTaken.ID
		,SurveyID = SurveyTaken.SurveyID
		,QuestionID = SurveyQuestions.QuestionID
		,AnswerID = SurveyAnswers.SurveyAnswerID
		,Answer = SurveyAnswers.AnswerText
		,SurveyDate = cast(SurveyTaken.DateTaken as date)
		,SurveyTime = SurveyTaken.DateTaken
		,LocationID = LocationType.EntityXrefRecno --Recno used because location reference will be a combination of locations and selected location   Locations.ID
		,OriginalLocationID = Locations.ID
		,DeviceNumber = SurveyTaken.DeviceNum
		,ChannelID
		,QuestionTypeID = QuestionType.XrefEntityCode
		,LocationTypeID = LocationType.XrefEntityCode
	from
		(
		select 
			ID = SurveyTaken.id
			,SurveyTaken.SurveyID
			,SurveyTaken.DateTaken
			,SurveyTaken.DeviceNum
			,SurveyTaken.LocationID
			,ChannelID = 6
		from 
			[$(PatientExperience)].PEX.SurveyTaken
			
		where 
			not exists	(
						select 
							SurveyID = EntityCode 
						from 
							dbo.EntityXref 
						where 
							EntityXref.EntityCode = SurveyTaken.SurveyID 
						and EntityTypeCode = 'PEXSURVEY'
						)
		) SurveyTaken
		
    inner join [$(PatientExperience)].PEX.Locations
    on SurveyTaken.LocationID = Locations.ID
      
    inner join [$(PatientExperience)].PEX.SurveyQuestions
	on	SurveyTaken.SurveyID = SurveyQuestions.SurveyID 

	inner join [$(PatientExperience)].PEX.SurveyAnswers
	on	SurveyAnswers.SurveyTakenID = SurveyTaken.ID
	and	SurveyAnswers.SurveyQuestionID = SurveyQuestions.ID
	and SurveyAnswers.SurveyAnswerID <> '-1' --No Response (not actually a response)
					                   		
	inner join dbo.EntityXref LocationType --brings through all locations despite Location ID not being in the EntityXref CH 16/10/2014
	on	LocationType.EntityCode = Locations.ID
	and LocationType.EntityTypeCode = 'PEXLOCATION'

	left join dbo.EntityXref QuestionType
	on	QuestionType.EntityCode = SurveyQuestions.QuestionID
	and QuestionType.EntityTypeCode = 'PEXQUESTION'
		
	) Tablet
	
	where 
		SurveyDate between @FromDate and @ToDate
	
select @RowsInserted = @@rowcount

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime







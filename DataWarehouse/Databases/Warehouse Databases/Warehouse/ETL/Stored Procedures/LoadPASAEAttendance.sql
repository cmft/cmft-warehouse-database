﻿



CREATE PROCEDURE [ETL].[LoadPASAEAttendance]

as

/* 
==============================================================================================
Description: Created for linking OCM results with AE attendances

NB SourceUniqueID is composite string of all other fields in Inquire.

When		Who			What
20150508	Paul Egan	Initial Coding
===============================================================================================
*/

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime2(0)
	,@LoadEndDate datetime2(0)

select
	 @LoadStartDate = min(ArrivalTime)
	,@LoadEndDate = max(ArrivalTime)
from
	ETL.TLoadPASAEAttendance
;

declare
	 @deleted int
	,@inserted int
	,@updated int
	
declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	PAS.AEAttendance target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,SourceEncounterNo
		,ArrivalTime
		,DepartmentCode
		,HospitalCode
		,Deleted = 0
	from
		ETL.TLoadPASAEAttendance
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	
	when not matched by source
	and	target.ArrivalTime between @LoadStartDate and @LoadEndDate
	and target.Deleted <> 1 -- not already marked as deleted

	then
		update
		set
			target.Deleted = 1 -- retain record but denote as deleted from PAS. 
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

	when not matched by target
	then
		insert
			(
			SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,ArrivalTime
			,DepartmentCode
			,HospitalCode
			,Deleted
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,ArrivalTime
			,DepartmentCode
			,HospitalCode
			,Deleted
			,getdate()
			,getdate()
			,suser_name()
			)
			
	when matched
	and not
		(
			isnull(target.Deleted, 0) = isnull(source.Deleted, 0)
		)
	then
		update
		set
			target.Deleted = source.Deleted  -- This is the only thing that can change.

output
	$action into @MergeSummary
;

select
	@updated = @updated + @@rowcount
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime



﻿CREATE procedure [ETL].[ExtractInquireNeonate]

as

insert into ETL.TImportPASNeonate
select
	 *
from
	(
	select
		 NEONATEID
		,ApgarScoreAt1Min
		,ApgarScoreAt5Mins
		,BirthOrder
		,DiagnosisCode1
		,DiagnosisCode2
		,left(EpisodeNumber, 12) EpisodeNumber
		,HeadCircumference
		,left(InternalPatientNumber, 12) InternalPatientNumber
		,KeMatBcgAdminInt
		,KeMatFeedingInt
		,KeMatFollUpCareInt
		,KeMatHipExamInt
		,KeMatJaundiceInt
		,KeMatMetabolicScInt
		,Length
		,PaedLenGestation

	from
		[$(PAS)].Inquire.NEONATE
) Neonate

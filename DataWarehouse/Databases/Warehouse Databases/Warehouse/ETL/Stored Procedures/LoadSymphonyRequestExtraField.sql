﻿
CREATE PROCEDURE [ETL].[LoadSymphonyRequestExtraField]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Request Extra Field table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 RequestExtraFieldID
			,FieldTypeID
			,FieldID
			,AttendanceID
			,FieldValue
			,DataCategoryID
			,RecordID
			,FieldValueCode
			,ComputationStateID
			,DataEntryProcedureID
			,CPMessagesID
			)
into
	#TLoadSymphonyRequestExtraField
from
	(
	select
		 RequestExtraFieldID = cast(RequestExtraFieldID as int)
		,FieldTypeID = cast(FieldTypeID as smallint)
		,FieldID = cast(FieldID as int)
		,AttendanceID = cast(AttendanceID as int)
		,FieldValue = cast(FieldValue as varchar(4000))
		,DataCategoryID = cast(DataCategoryID as smallint)
		,RecordID = cast(RecordID as int)
		,FieldValueCode = cast(FieldValueCode as varchar(200))
		,ComputationStateID = cast(ComputationStateID as tinyint)
		,DataEntryProcedureID = cast(DataEntryProcedureID as tinyint)
		,CPMessagesID = cast(CPMessagesID as varchar(200))
	from
		(
		select
			 RequestExtraFieldID = RequestExtraField.req_EF_extrafieldID
			,FieldTypeID = RequestExtraField.req_EF_FieldType
			,FieldID = RequestExtraField.req_EF_FieldID
			,AttendanceID = RequestExtraField.req_EF_atdid
			,FieldValue = RequestExtraField.req_EF_Value
			,DataCategoryID = RequestExtraField.req_EF_DataCategory
			,RecordID = RequestExtraField.req_EF_RecordID
			,FieldValueCode = RequestExtraField.req_EF_FieldCodeValues
			,ComputationStateID = RequestExtraField.Req_EF_ComputationState
			,DataEntryProcedureID = RequestExtraField.Req_EF_depId
			,CPMessagesID = RequestExtraField.Req_EF_CPMessagesIDs
		from
			[$(Symphony)].dbo.Request_Details_ExtraFields RequestExtraField

		) Encounter
	) Encounter
order by
	Encounter.RequestExtraFieldID


create unique clustered index #IX_TLoadSymphonyRequestExtraField on #TLoadSymphonyRequestExtraField
	(
	RequestExtraFieldID ASC
	)



declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.RequestExtraField target
using
	(
	select
		 RequestExtraFieldID
		,FieldTypeID
		,FieldID
		,AttendanceID
		,FieldValue
		,DataCategoryID
		,RecordID
		,FieldValueCode
		,ComputationStateID
		,DataEntryProcedureID
		,CPMessagesID
		,EncounterChecksum
	from
		#TLoadSymphonyRequestExtraField
	
	) source
	on	source.RequestExtraFieldID = target.RequestExtraFieldID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 RequestExtraFieldID
			,FieldTypeID
			,FieldID
			,AttendanceID
			,FieldValue
			,DataCategoryID
			,RecordID
			,FieldValueCode
			,ComputationStateID
			,DataEntryProcedureID
			,CPMessagesID
			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.RequestExtraFieldID
			,source.FieldTypeID
			,source.FieldID
			,source.AttendanceID
			,source.FieldValue
			,source.DataCategoryID
			,source.RecordID
			,source.FieldValueCode
			,source.ComputationStateID
			,source.DataEntryProcedureID
			,source.CPMessagesID

			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.RequestExtraFieldID = source.RequestExtraFieldID
			,target.FieldTypeID = source.FieldTypeID
			,target.FieldID = source.FieldID
			,target.AttendanceID = source.AttendanceID
			,target.FieldValue = source.FieldValue
			,target.DataCategoryID = source.DataCategoryID
			,target.RecordID = source.RecordID
			,target.FieldValueCode = source.FieldValueCode
			,target.ComputationStateID = source.ComputationStateID
			,target.DataEntryProcedureID = source.DataEntryProcedureID
			,target.CPMessagesID = source.CPMessagesID
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
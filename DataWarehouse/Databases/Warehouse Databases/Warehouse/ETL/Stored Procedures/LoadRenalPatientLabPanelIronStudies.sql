﻿CREATE procedure [ETL].[LoadRenalPatientLabPanelIronStudies] (@from date = '19000101', @to date = '21000101') as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

declare @MergeSummary TABLE(Action nvarchar(10));

merge Renal.PatientLabPanelIronStudies target
using ETL.TLoadRenalPatientLabPanelIronStudies source
on source.SourceUniqueID = target.SourceUniqueID
when matched and source.LabTestDate between @from and @to
and not
(
	isnull(target.PatientObjectID, 0) = isnull(source.PatientObjectID, 0)
	and isnull(target.PatientFullName, '') = isnull(source.PatientFullName, '')
	and isnull(target.PatientMedicalRecordNo, '') = isnull(source.PatientMedicalRecordNo, '')
	and isnull(target.LabTestObjectID, 0) = isnull(source.LabTestObjectID, 0)
	and isnull(target.LabTestDate, getdate()) = isnull(source.LabTestDate, getdate())
	and isnull(target.LabTestQualifier, '') = isnull(source.LabTestQualifier, '')
	and isnull(target.LabTestStatus, '') = isnull(source.LabTestStatus, '')
	and isnull(target.percentHypoNum, 0) = isnull(source.percentHypoNum, 0)
	and isnull(target.percentHypoStr, '') = isnull(source.percentHypoStr, '')
	and isnull(target.percentHypoFlg, '') = isnull(source.percentHypoFlg, '')
	and isnull(target.TransferrinSaturationNum, 0) = isnull(source.TransferrinSaturationNum, 0)
	and isnull(target.TransferrinSaturationStr, '') = isnull(source.TransferrinSaturationStr, '')
	and isnull(target.TransferrinSaturationFlg, '') = isnull(source.TransferrinSaturationFlg, '')
	and isnull(target.CHrNum, 0) = isnull(source.CHrNum, 0)
	and isnull(target.CHrStr, '') = isnull(source.CHrStr, '')
	and isnull(target.CHrFlg, '') = isnull(source.CHrFlg, '')
	and isnull(target.FerritinNum, 0) = isnull(source.FerritinNum, 0)
	and isnull(target.FerritinStr, '') = isnull(source.FerritinStr, '')
	and isnull(target.FerritinFlg, '') = isnull(source.FerritinFlg, '')
	and isnull(target.FolicAcidNum, 0) = isnull(source.FolicAcidNum, 0)
	and isnull(target.FolicAcidStr, '') = isnull(source.FolicAcidStr, '')
	and isnull(target.FolicAcidFlg, '') = isnull(source.FolicAcidFlg, '')
	and isnull(target.HctNum, 0) = isnull(source.HctNum, 0)
	and isnull(target.HctStr, '') = isnull(source.HctStr, '')
	and isnull(target.HctFlg, '') = isnull(source.HctFlg, '')
	and isnull(target.HgbNum, 0) = isnull(source.HgbNum, 0)
	and isnull(target.HgbStr, '') = isnull(source.HgbStr, '')
	and isnull(target.HgbFlg, '') = isnull(source.HgbFlg, '')
	and isnull(target.IronNum, 0) = isnull(source.IronNum, 0)
	and isnull(target.IronStr, '') = isnull(source.IronStr, '')
	and isnull(target.IronFlg, '') = isnull(source.IronFlg, '')
	and isnull(target.TIBCNum, 0) = isnull(source.TIBCNum, 0)
	and isnull(target.TIBCStr, '') = isnull(source.TIBCStr, '')
	and isnull(target.TIBCFlg, '') = isnull(source.TIBCFlg, '')
	and isnull(target.TransferrinNum, 0) = isnull(source.TransferrinNum, 0)
	and isnull(target.TransferrinStr, '') = isnull(source.TransferrinStr, '')
	and isnull(target.TransferrinFlg, '') = isnull(source.TransferrinFlg, '')
	and isnull(target.UIBCNum, 0) = isnull(source.UIBCNum, 0)
	and isnull(target.UIBCStr, '') = isnull(source.UIBCStr, '')
	and isnull(target.UIBCFlg, '') = isnull(source.UIBCFlg, '')
	and isnull(target.VitB12Num, 0) = isnull(source.VitB12Num, 0)
	and isnull(target.VitB12Str, '') = isnull(source.VitB12Str, '')
	and isnull(target.VitB12Flg, '') = isnull(source.VitB12Flg, '')
	and isnull(target.HgbX3Num, 0) = isnull(source.HgbX3Num, 0)
	and isnull(target.HgbX3Str, '') = isnull(source.HgbX3Str, '')
	and isnull(target.HgbX3Flg, '') = isnull(source.HgbX3Flg, '')
	and isnull(target.RBCFolateNum, 0) = isnull(source.RBCFolateNum, 0)
	and isnull(target.RBCFolateStr, '') = isnull(source.RBCFolateStr, '')
	and isnull(target.RBCFolateFlg, '') = isnull(source.RBCFolateFlg, '')
)
then update set
	target.PatientObjectID = source.PatientObjectID
	,target.PatientFullName = source.PatientFullName
	,target.PatientMedicalRecordNo = source.PatientMedicalRecordNo
	,target.LabTestObjectID = source.LabTestObjectID
	,target.LabTestDate = source.LabTestDate
	,target.LabTestQualifier = source.LabTestQualifier
	,target.LabTestStatus = source.LabTestStatus
	,target.percentHypoNum = source.percentHypoNum
	,target.percentHypoStr = source.percentHypoStr
	,target.percentHypoFlg = source.percentHypoFlg
	,target.TransferrinSaturationNum = source.TransferrinSaturationNum
	,target.TransferrinSaturationStr = source.TransferrinSaturationStr
	,target.TransferrinSaturationFlg = source.TransferrinSaturationFlg
	,target.CHrNum = source.CHrNum
	,target.CHrStr = source.CHrStr
	,target.CHrFlg = source.CHrFlg
	,target.FerritinNum = source.FerritinNum
	,target.FerritinStr = source.FerritinStr
	,target.FerritinFlg = source.FerritinFlg
	,target.FolicAcidNum = source.FolicAcidNum
	,target.FolicAcidStr = source.FolicAcidStr
	,target.FolicAcidFlg = source.FolicAcidFlg
	,target.HctNum = source.HctNum
	,target.HctStr = source.HctStr
	,target.HctFlg = source.HctFlg
	,target.HgbNum = source.HgbNum
	,target.HgbStr = source.HgbStr
	,target.HgbFlg = source.HgbFlg
	,target.IronNum = source.IronNum
	,target.IronStr = source.IronStr
	,target.IronFlg = source.IronFlg
	,target.TIBCNum = source.TIBCNum
	,target.TIBCStr = source.TIBCStr
	,target.TIBCFlg = source.TIBCFlg
	,target.TransferrinNum = source.TransferrinNum
	,target.TransferrinStr = source.TransferrinStr
	,target.TransferrinFlg = source.TransferrinFlg
	,target.UIBCNum = source.UIBCNum
	,target.UIBCStr = source.UIBCStr
	,target.UIBCFlg = source.UIBCFlg
	,target.VitB12Num = source.VitB12Num
	,target.VitB12Str = source.VitB12Str
	,target.VitB12Flg = source.VitB12Flg
	,target.HgbX3Num = source.HgbX3Num
	,target.HgbX3Str = source.HgbX3Str
	,target.HgbX3Flg = source.HgbX3Flg
	,target.RBCFolateNum = source.RBCFolateNum
	,target.RBCFolateStr = source.RBCFolateStr
	,target.RBCFolateFlg = source.RBCFolateFlg	,target.Updated = getdate()
	,target.ByWhom = system_user

when not matched by source then delete

when not matched by target then insert
(
	SourceUniqueID
	,PatientObjectID
	,PatientFullName
	,PatientMedicalRecordNo
	,LabTestObjectID
	,LabTestDate
	,LabTestQualifier
	,LabTestStatus
	,percentHypoNum
	,percentHypoStr
	,percentHypoFlg
	,TransferrinSaturationNum
	,TransferrinSaturationStr
	,TransferrinSaturationFlg
	,CHrNum
	,CHrStr
	,CHrFlg
	,FerritinNum
	,FerritinStr
	,FerritinFlg
	,FolicAcidNum
	,FolicAcidStr
	,FolicAcidFlg
	,HctNum
	,HctStr
	,HctFlg
	,HgbNum
	,HgbStr
	,HgbFlg
	,IronNum
	,IronStr
	,IronFlg
	,TIBCNum
	,TIBCStr
	,TIBCFlg
	,TransferrinNum
	,TransferrinStr
	,TransferrinFlg
	,UIBCNum
	,UIBCStr
	,UIBCFlg
	,VitB12Num
	,VitB12Str
	,VitB12Flg
	,HgbX3Num
	,HgbX3Str
	,HgbX3Flg
	,RBCFolateNum
	,RBCFolateStr
	,RBCFolateFlg
	,Created
	,Updated
	,ByWhom
)
values
(
	 source.SourceUniqueID
	,source.PatientObjectID
	,source.PatientFullName
	,source.PatientMedicalRecordNo
	,source.LabTestObjectID
	,source.LabTestDate
	,source.LabTestQualifier
	,source.LabTestStatus
	,source.percentHypoNum
	,source.percentHypoStr
	,source.percentHypoFlg
	,source.TransferrinSaturationNum
	,source.TransferrinSaturationStr
	,source.TransferrinSaturationFlg
	,source.CHrNum
	,source.CHrStr
	,source.CHrFlg
	,source.FerritinNum
	,source.FerritinStr
	,source.FerritinFlg
	,source.FolicAcidNum
	,source.FolicAcidStr
	,source.FolicAcidFlg
	,source.HctNum
	,source.HctStr
	,source.HctFlg
	,source.HgbNum
	,source.HgbStr
	,source.HgbFlg
	,source.IronNum
	,source.IronStr
	,source.IronFlg
	,source.TIBCNum
	,source.TIBCStr
	,source.TIBCFlg
	,source.TransferrinNum
	,source.TransferrinStr
	,source.TransferrinFlg
	,source.UIBCNum
	,source.UIBCStr
	,source.UIBCFlg
	,source.VitB12Num
	,source.VitB12Str
	,source.VitB12Flg
	,source.HgbX3Num
	,source.HgbX3Str
	,source.HgbX3Flg
	,source.RBCFolateNum
	,source.RBCFolateStr
	,source.RBCFolateFlg
	,getdate()
	,getdate()
	,system_user
)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats


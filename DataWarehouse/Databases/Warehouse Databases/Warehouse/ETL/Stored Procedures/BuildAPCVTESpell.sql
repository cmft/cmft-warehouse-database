﻿


CREATE procedure [ETL].[BuildAPCVTESpell] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()
select @RowsUpdated = 0


--initialise table
truncate table APC.VTESpell

insert into APC.VTESpell
(
	ProviderSpellNo
)

select distinct
	ProviderSpellNo
from
	APC.Encounter
where
	AdmissionDate >= '1 Jan 2008' --Jan 2008 seems to be the earliest occurrence of VTE coding


--VTE Categorisation

update APC.VTESpell
set
	VTECategoryCode = 'C'
from
	APC.VTESpell

inner join	APC.Encounter
on	Encounter.ProviderSpellNo = VTESpell.ProviderSpellNo

where
	left(Encounter.Research2 , 1) = 'V'
and	charindex('C' , Encounter.Research2) > 1



select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT



update APC.VTESpell
set
	VTECategoryCode = 'I'
from
	APC.VTESpell

inner join	APC.Encounter
on	Encounter.ProviderSpellNo = VTESpell.ProviderSpellNo
and	VTESpell.VTECategoryCode is null

where
	left(Encounter.Research2 , 1) = 'V'
and	charindex('I' , Encounter.Research2) > 1




select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT



update APC.VTESpell
set
	VTECategoryCode = 'M'
from
	APC.VTESpell

inner join	APC.Encounter
on	Encounter.ProviderSpellNo = VTESpell.ProviderSpellNo
and	VTESpell.VTECategoryCode is null

where
	left(Encounter.Research2 , 1) = 'V'
and	charindex ('M' , Research2) > 1
and charindex ('C' , Research2) = 0
and charindex ('I' , Research2) = 0


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT




--Build work tables for 'any position' seeks

create table #Diagnosis
	(
	 ProviderSpellNo varchar(20) not null
	,SourceEncounterNo bigint not null
	,DiagnosisCode varchar(10) not null
	,SequenceNo int not null
	)

create table #Operation
	(
	 ProviderSpellNo varchar(20) not null
	,SourceEncounterNo bigint not null
	,OperationCode varchar(10) not null
	,SequenceNo int not null
	)


-- Diagnosis
insert #Diagnosis
select
	 Diagnosis.ProviderSpellNo
	,Diagnosis.SourceEncounterNo
	,Diagnosis.DiagnosisCode
	,Diagnosis.SequenceNo
from
	(
	select
		 Encounter.ProviderSpellNo
		,Encounter.SourceEncounterNo
		,DiagnosisCode = Encounter.PrimaryDiagnosisCode
		,SequenceNo = 0
	from
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

	union

	select
		 Diagnosis.ProviderSpellNo
		,Diagnosis.SourceEncounterNo
		,Diagnosis.DiagnosisCode
		,Diagnosis.SequenceNo
	from
		APC.Diagnosis

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Diagnosis.ProviderSpellNo
	) Diagnosis
where
	Diagnosis.DiagnosisCode is not null

CREATE NONCLUSTERED INDEX IX_wkDiagnosis ON #Diagnosis 
	(
	 ProviderSpellNo asc
	,SourceEncounterNo asc
	,SequenceNo asc
	)


-- Operation
insert #Operation
select
	 Operation.ProviderSpellNo
	,Operation.SourceEncounterNo
	,Operation.OperationCode
	,Operation.SequenceNo
from
	(
	select
		 Encounter.ProviderSpellNo
		,Encounter.SourceEncounterNo
		,OperationCode = Encounter.PrimaryOperationCode
		,SequenceNo = 0
	from
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo

	union

	select
		 Operation.ProviderSpellNo
		,Operation.SourceEncounterNo
		,Operation.OperationCode
		,Operation.SequenceNo
	from
		APC.Operation

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Operation.ProviderSpellNo
	) Operation
where
	Operation.OperationCode is not null

CREATE NONCLUSTERED INDEX IX_wkOperation ON #Operation 
	(
	 ProviderSpellNo asc
	,SourceEncounterNo asc
	,SequenceNo asc
	)

-- finish building work tables




--Exclusion Categorisation

--1 Exclude on the basis of Primary Procedure
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join
	(
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXPRIMPROCWITHOUTLACODE'
	and	Exclusion.EntityCode = Encounter.PrimaryOperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo
and	Exclusion.SequenceNo = 1


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


-- 2 Exclude on basis of Diagnosis Codes found in any position
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join
	(  
	select
		 #Diagnosis.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Diagnosis.ProviderSpellNo order by #Diagnosis.SourceEncounterNo , #Diagnosis.SequenceNo)
	FROM
		#Diagnosis

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = #Diagnosis.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXANYDIAGNOSISCODE'
	and	Exclusion.EntityCode = #Diagnosis.DiagnosisCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1



select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT




-- 3 Exclude on basis of Procedure Codes found in any position
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join
	(  
	select
		 #Operation.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Operation.ProviderSpellNo order by #Operation.SourceEncounterNo , #Operation.SequenceNo)
	FROM
		#Operation

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = #Operation.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXANYPROCEDURECODE'
	and	Exclusion.EntityCode = #Operation.OperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

-- 4 Exclude on basis of Procedure Codes found in any position, for Chemotherapy patients only

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join
	(
	select
		 #Operation.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Operation.ProviderSpellNo order by #Operation.SourceEncounterNo , #Operation.SequenceNo)
	FROM
		#Operation

	inner join 
		(
		select distinct
			#Diagnosis.ProviderSpellNo
		from
			#Diagnosis

		inner join APC.VTESpell
		on	VTESpell.ProviderSpellNo = #Diagnosis.ProviderSpellNo
		and	VTESpell.VTEExclusionReasonCode is null

		where
			#Diagnosis.DiagnosisCode in ( 'Z51.1' , 'Z51.2' )
		) Diagnosis	
	on	Diagnosis.ProviderSpellNo = #Operation.ProviderSpellNo

	inner join EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXCHEMOANYPROCEDURECODE'
	and	Exclusion.EntityCode = #Operation.OperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

-- 5 Exclude Day Cases on the basis of Procedures in any position
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join
	(  
	select
		 #Operation.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Operation.ProviderSpellNo order by #Operation.SourceEncounterNo , #Operation.SequenceNo)
	FROM
		#Operation

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = #Operation.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join APC.Encounter
	on	Encounter.ProviderSpellNo = #Operation.ProviderSpellNo
	and	Encounter.SourceEncounterNo = #Operation.SourceEncounterNo
	and	Encounter.PatientCategoryCode = 'DC'

	inner join EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXDAYCASEANYPROCEDURECODE'
	and	Exclusion.EntityCode = #Operation.OperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

--6 Exclude spells on the basis of TOPS categorisation and admission ward equals Ward 63
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join
	(  
	select
		 #Operation.ProviderSpellNo
		,VTEExclusionReasonCode = Exclusion.XrefEntityCode 
		,SequenceNo =
			row_number() over (partition by #Operation.ProviderSpellNo order by #Operation.SourceEncounterNo , #Operation.SequenceNo)
	FROM
		#Operation

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = #Operation.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join APC.Encounter
	on	Encounter.ProviderSpellNo = #Operation.ProviderSpellNo
	and	Encounter.SourceEncounterNo = #Operation.SourceEncounterNo
	and	Encounter.StartWardTypeCode = '63'

	inner join EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXTOPSWARD63CODE'
	and	Exclusion.EntityCode = #Operation.OperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'

	where
		Encounter.FirstEpisodeInSpellIndicator = 'Y'

	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


-- Dental Day Case Exclusions 1st Test   
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(  
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'DN' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartSiteCode = 'DENT'
	AND Encounter.EndSiteCode = 'DENT'
	AND (
			Encounter.PatientCategoryCode = 'DC'
		 OR datediff(minute , Encounter.EpisodeStartTime , Encounter.EpisodeEndTime) < 721
		)
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


-- Dental Day Case Exclusions 2nd Test  
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	( 

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'DENTDC' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		StartDirectorateCode = '50'
	AND Encounter.PatientCategoryCode = 'DC'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

-- ESTU Length of Stay Less Than 8 hours  Exclusions  
update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(   

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'ESTU' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartWardTypeCode IN ( 'ESTU' , 'MAU' , 'CLDU' , 'PIU' , 'OMU', 'AMU')
	and	datediff(minute , Encounter.EpisodeStartTime , Encounter.EpisodeEndTime) < 481   
	and	Encounter.FirstEpisodeInSpellIndicator = 'Y'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	-- Clinical Haematology zero LOS Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'HAEMZERO' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null


	inner join PAS.Specialty
	on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

	WHERE 
		datediff(day , Encounter.AdmissionDate , Encounter.DischargeDate) = 0
	AND left(Specialty.NationalSpecialtyCode , 3)  IN ( '303' , '253' )
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	-- St Mary's GYN1 Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'GYN1' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter 

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		StartDirectorateCode = '30'
	AND
		(
			(
				Encounter.SpecialtyCode = 'GYN1'
			and left(Encounter.PrimaryOperationCode , 3 ) != 'Q48'
			)
	   or  (
				Encounter.SpecialtyCode = 'GYN'
			and Encounter.PrimaryOperationCode = 'Q13.1'
			)
	  )
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(

	-- Endoscopy room Exclusions     
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'ENDOR' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartWardTypeCode = 'ENDO'
	AND Encounter.EndWardTypeCode = 'ENDO'
	and	Encounter.FirstEpisodeInSpellIndicator = 'Y'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	( 
	-- Ophthalmology Day Case Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'OPHTHDC' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join PAS.Specialty
	on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

	WHERE
		Encounter.StartDirectorateCode = '40'
	and left(Specialty.NationalSpecialtyCode , 3) IN ( '130' , '216' )
	and Encounter.PatientCategoryCode = 'DC'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	-- Ophthalmology Laser Day Case Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'EDC' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartDirectorateCode = '40'
	and Encounter.StartWardTypeCode IN ( 'EDCA' , 'EDCC' , 'EDCD' )
	and	Encounter.FirstEpisodeInSpellIndicator = 'Y'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	-- Admission Method AN Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'AN' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null


	inner join PAS.Specialty
	on	Specialty.SpecialtyCode = Encounter.SpecialtyCode

	WHERE
		Encounter.AdmissionMethodCode = 'AN' 
	AND LEFT(Specialty.NationalSpecialtyCode , 3) = '501'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	-- Check Anaesthetic in Theatre data
	-- CCB 2012-01-10 update Theatre/APC matching to include casenote and name matching
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode =
			case
			when 
				datediff(
					 minute
					,coalesce(AnaestheticInductionTime , InAnaestheticTime)
					,InRecoveryTime
				) < 90 then '90'
			when Anaesthetic.AnaestheticCode1 = 'LA' and Encounter.PatientCategoryCode = 'DC' then 'LA'

			--when ExclusionProcedure.LA = 'SE' and Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' ) then ExclusionProcedure.Type
			when Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' ) then Exclusion.XrefEntityCode

			end
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by ProcedureDetail.ProcedureStartTime)
	from
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	inner join APC.EncounterProcedureDetail
	on	EncounterProcedureDetail.EncounterRecno = Encounter.EncounterRecno

	inner join Theatre.ProcedureDetail
	on	ProcedureDetail.SourceUniqueID = EncounterProcedureDetail.ProcedureDetailSourceUniqueID

	inner join Theatre.OperationDetail
	on	OperationDetail.SourceUniqueID = ProcedureDetail.OperationDetailSourceUniqueID

	inner join Theatre.PatientBooking
	on	PatientBooking.SourceUniqueID = OperationDetail.PatientBookingSourceUniqueID

	left join Theatre.Anaesthetic
	on	Anaesthetic.AnaestheticCode = PatientBooking.AnaestheticCode

	inner join PAS.Specialty
	on	Encounter.SpecialtyCode = Specialty.SpecialtyCode

	--LEFT JOIN SandboxPeterHoyle.dbo.tblExcPrimProc ExclusionProcedure
	--ON ExclusionProcedure.OPCS = Encounter.PrimaryOperationCode

	inner join EntityXref Exclusion
	on	Exclusion.EntityTypeCode = 'VTEXPRIMPROCWITHSECODE'
	and	Exclusion.EntityCode = Encounter.PrimaryOperationCode
	and	Exclusion.XrefEntityTypeCode = 'VTEEXCLUSIONCODE'


	where
		(
			Anaesthetic.AnaestheticCode1 in ( 'LA' , 'SE' )
		and	Specialty.NationalSpecialtyCode in ( '160' , '140' , '120' )
		and	left(Encounter.PrimaryOperationCode , 1) not in ('C' , 'D')
		)
	or
		(
			Anaesthetic.AnaestheticCode1 = 'LA'
		and	(
				Specialty.NationalSpecialtyCode not in ( '160' , '140' , '120' )
			or	Encounter.PatientCategoryCode = 'DC'
			)
		)
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


--Process the REH hospital exclusions in the following pieces of code

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	--Process the REH Withington Exclusions     

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'WH' 
		,SequenceNo = 1
			--row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.StartWardTypeCode = 'WCH' 
	and	Encounter.FirstEpisodeInSpellIndicator = 'Y'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

----now done in the earlier generic procedure step

--left join
--	(
--	--Process The Opthalmology PRIMARY Procedure Exclusions 

--	select
--		 Encounter.ProviderSpellNo
--		,VTEExclusionReasonCode = ExclusionsLookup.ReturnValue 
--		,SequenceNo =
--			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
--	FROM
--		APC.Encounter
		
--	INNER JOIN SandboxPeterHoyle.dbo.ExclusionsLookup
--	ON ExclusionsLookup.Code = Encounter.PrimaryOperationCode
	     
--	WHERE
--		ExclusionsLookup.DiagProc = 'EYE'
--	) Exclusion15
--on	Exclusion15.ProviderSpellNo = Encounter.ProviderSpellNo
--and	Exclusion15.SequenceNo = 1


---- is this now redundant?

--update APC.VTESpell
--set
--	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
--from
--	APC.VTESpell
--inner join	
--	(
--	--Process The Opthalmology ANY Procedure Exclusions 

--	select
--		 Encounter.ProviderSpellNo
--		,VTEExclusionReasonCode = ExclusionsLookup.ReturnValue 
--		,SequenceNo =
--			row_number() over (partition by Encounter.ProviderSpellNo order by Operation.SourceEncounterNo , Operation.SequenceNo)
--	FROM
--		APC.Encounter

--	inner join APC.VTESpell
--	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
--	and	VTESpell.VTEExclusionReasonCode is null
		
--	INNER JOIN APC.Operation
--	ON	Operation.APCSourceUniqueID = Encounter.SourceUniqueID

--	INNER JOIN SandboxPeterHoyle.dbo.ExclusionsLookup
--	ON	ExclusionsLookup.Code = Operation.OperationCode
	     
--	WHERE
--		ExclusionsLookup.DiagProc = 'EYE'
--	AND Operation.OperationCode  IN ( 'C64.7' , 'X85.1' )
--	) Exclusion
--on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
--and	Exclusion.SequenceNo = 1


update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	--Process The Opthalmology DOUBLE Procedure Exclusions 
	 
	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 
			case
			when Encounter.PrimaryOperationCode = 'C75.1' and Operation.OperationCode = 'Y71.2'	then 'SECI'
			when Encounter.PrimaryOperationCode = 'C79.4' and Operation.OperationCode = 'X93.1' then 'LUCI'
			when Encounter.PrimaryOperationCode = 'C22.6' and Operation.OperationCode = 'S60.6' then 'ELEC'
			when Encounter.PrimaryOperationCode = 'C22.8' and Operation.OperationCode = 'S50.3' then 'IGW'
			when Encounter.PrimaryOperationCode = 'C10.8' and Operation.OperationCode = 'S43.3' then 'REMSUT'
			when Encounter.PrimaryOperationCode = 'C22.8' and Operation.OperationCode = 'S43.3' then 'REMSUT'
			when Encounter.PrimaryOperationCode = 'C12.5' and Operation.OperationCode = 'S10.3' then 'DSX'
			when Encounter.PrimaryOperationCode = 'L67.1' and Operation.OperationCode = 'O12.1' then 'TEMPAB'
			when Encounter.PrimaryOperationCode = 'C75.1' and Operation.OperationCode = 'C71.2' then 'PHACOI'
			when Encounter.PrimaryOperationCode = 'C27.3' and Operation.OperationCode = 'C27.5' then 'SYRP'
			end
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Operation.SourceEncounterNo , Operation.SequenceNo)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	INNER JOIN APC.Operation
	ON	Operation.APCSourceUniqueID = Encounter.SourceUniqueID
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1
and	Exclusion.VTEExclusionReasonCode is not null

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

update APC.VTESpell
set
	VTEExclusionReasonCode = Exclusion.VTEExclusionReasonCode
from
	APC.VTESpell
inner join	
	(
	--Process The Opthalmology TRIPLE Procedure Exclusions

	select
		 Encounter.ProviderSpellNo
		,VTEExclusionReasonCode = 'PHACOS' 
		,SequenceNo =
			row_number() over (partition by Encounter.ProviderSpellNo order by Encounter.EpisodeStartTime)
	FROM
		APC.Encounter

	inner join APC.VTESpell
	on	VTESpell.ProviderSpellNo = Encounter.ProviderSpellNo
	and	VTESpell.VTEExclusionReasonCode is null

	WHERE
		Encounter.PrimaryOperationCode    = 'C75.1'
	AND Encounter.SecondaryOperationCode1 = 'C71.2'
	AND Encounter.SecondaryOperationCode2 = 'C61.8'
	AND Encounter.SecondaryOperationCode3 = 'Y14.3'
	) Exclusion
on	Exclusion.ProviderSpellNo = VTESpell.ProviderSpellNo    
and	Exclusion.SequenceNo = 1

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

--delete uncategorised spells
delete
from
	APC.VTESpell
where
	VTECategoryCode is null
and	VTEExclusionReasonCode is null



--drop work tables

drop table #Diagnosis
drop table #Operation


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'BuildAPCVTESpell', @Stats, @StartTime


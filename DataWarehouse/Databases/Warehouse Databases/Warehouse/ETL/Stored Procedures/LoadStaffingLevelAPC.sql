﻿
CREATE procedure [ETL].[LoadStaffingLevelAPC]

as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, CensusDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, CensusDate)) 
from
	ETL.TLoadStaffingLevelAPC

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	APC.StaffingLevel target
using
	(
	select  
		DivisionCode
		,WardCode
		,CensusDate
		,ShiftID
		,RegisteredNursePlan 
		,RegisteredNurseActual 
		,NonRegisteredNursePlan 
		,NonRegisteredNurseActual 
		,Comments
		,SeniorComments
	from
		ETL.TLoadStaffingLevelAPC
	) source
	on	source.DivisionCode = target.DivisionCode
	and	source.WardCode = target.WardCode
	and source.CensusDate = target.CensusDate
	and	source.ShiftID = target.ShiftID
	
	when not matched by source
	and	target.CensusDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			DivisionCode
			,WardCode
			,CensusDate
			,ShiftID
			,RegisteredNursePlan 
			,RegisteredNurseActual 
			,NonRegisteredNursePlan 
			,NonRegisteredNurseActual 
			,Comments
			,SeniorComments
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.DivisionCode
			,source.WardCode
			,source.CensusDate
			,source.ShiftID
			,source.RegisteredNursePlan 
			,source.RegisteredNurseActual 
			,source.NonRegisteredNursePlan 
			,source.NonRegisteredNurseActual 
			,source.Comments
			,source.SeniorComments
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
		and not
			(
				isnull(target.DivisionCode, '') = isnull(source.DivisionCode, '')
			and isnull(target.WardCode, '') = isnull(source.WardCode, '')
			and isnull(target.CensusDate, '1 jan 1900') = isnull(source.CensusDate, '1 jan 1900')
			and isnull(target.ShiftID, 0) = isnull(source.ShiftID, 0)
			and isnull(target.RegisteredNursePlan, 0) = isnull(source.RegisteredNursePlan, 0)
			and isnull(target.RegisteredNurseActual, 0) = isnull(source.RegisteredNurseActual, 0)
			and isnull(target.NonRegisteredNursePlan, 0) = isnull(source.NonRegisteredNursePlan, 0)
			and isnull(target.NonRegisteredNurseActual, 0) = isnull(source.NonRegisteredNurseActual, 0)
			and isnull(target.Comments, '') = isnull(source.Comments, '')
			and isnull(target.SeniorComments, '') = isnull(source.SeniorComments, '')
			)
	then
		update
		set
			target.DivisionCode = source.DivisionCode
			,target.WardCode = source.WardCode
			,target.CensusDate = source.CensusDate
			,target.ShiftID = source.ShiftID
			,target.RegisteredNursePlan = source.RegisteredNursePlan
			,target.RegisteredNurseActual = source.RegisteredNurseActual
			,target.NonRegisteredNursePlan = source.NonRegisteredNursePlan
			,target.NonRegisteredNurseActual = source.NonRegisteredNurseActual
			,target.Comments = source.Comments
			,target.SeniorComments = source.SeniorComments
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


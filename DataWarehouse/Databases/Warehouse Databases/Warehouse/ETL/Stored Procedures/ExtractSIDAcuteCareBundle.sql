﻿CREATE procedure [ETL].[ExtractSIDAcuteCareBundle]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDAcuteCareBundle;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDAcuteCareBundle
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,ArrayOfLocationDetailsDTOLocation
	,ArrayOfLocationDetailsDTOStartDate
	,ArrayOfLocationDetailsDTOEndDate
	,DateTimeNurseWithStrokeExperienceReview
	,StrokeHandbookGivenToPatient
	,DateTimeStrokeHandbookGivenToPatient
	,BaselineNeuroObsPerformed
	,AVPU
	,DateTimeRandomBloodGlucoseLevelRecorded
	,IsThePatientSafeToEatAndDrink
	,RiskOfMalnutritionIdentified
	,DateTimeSeenByADietician
	,DateTimeCommenceEntralFeedingViaNG
	,HasContinenceAssessmentBeenDone
	,DateTimeContinenceAssessmentDone
	,ContinenceIssuesIdenitifed
	,CarePlanStarted
	,DateTimeCarePlanStarted
	,Weighed
	,MUST
	,DateTimeFirstAssessedByPhysiotherapy
	,DateTimeFirstAssessedByOccupationalTherapy
	,DateTimeFirstAssessedSpeechAndLanguageTherapy
	,DateTimeFormalSwallowAssessmentCompleted
	,PerformBarthelAssessmentPreAdmission
	,BarthelAssessmentPreAdmissionDetailsFeeding
	,BarthelAssessmentPreAdmissionDetailsBathing
	,BarthelAssessmentPreAdmissionDetailsGrooming
	,BarthelAssessmentPreAdmissionDetailsDressing
	,BarthelAssessmentPreAdmissionDetailsBowels
	,BarthelAssessmentPreAdmissionDetailsBladder
	,BarthelAssessmentPreAdmissionDetailsToiletUse
	,BarthelAssessmentPreAdmissionDetailsTransfers
	,BarthelAssessmentPreAdmissionDetailsMobility
	,BarthelAssessmentPreAdmissionDetailsStairs
	,BarthelAssessmentPreAdmissionDetailsTotalScore
	,PerformBarthelAssessmentOnAdmission
	,BarthelAssessmentOnAdmissionDetailsFeeding
	,BarthelAssessmentOnAdmissionDetailsBathing
	,BarthelAssessmentOnAdmissionDetailsGrooming
	,BarthelAssessmentOnAdmissionDetailsDressing
	,BarthelAssessmentOnAdmissionDetailsBowels
	,BarthelAssessmentOnAdmissionDetailsBladder
	,BarthelAssessmentOnAdmissionDetailsToiletUse
	,BarthelAssessmentOnAdmissionDetailsTransfers
	,BarthelAssessmentOnAdmissionDetailsMobility
	,BarthelAssessmentOnAdmissionDetailsStairs
	,BarthelAssessmentOnAdmissionDetailsTotalScore
	,ModifiedRankinScorePreAdmission
	,ModifiedRankinScoreOnAdmission
	,DateTimeFirstAssessedByStrokePhysician
	,DiagnosisExplainedToPatientFamily
	,DateTimeDiagnosisExplainedToPatientFamily
	,DiagnosisExplainedToPatientFamilyNo
	,Username
	,ModifiedDate
	,IsSectionComplete
	,SectionCompleteTime
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		CandidateData.SectionID
	,ActionID = 
		CandidateData.ActionID
	,ArrayOfLocationDetailsDTOLocation = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/LocationDetails/Location)[1]', 'varchar(max)'),'')
	,ArrayOfLocationDetailsDTOStartDate = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/LocationDetails/StartDate)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ArrayOfLocationDetailsDTOEndDate = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/LocationDetails/EndDate)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeNurseWithStrokeExperienceReview = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeNurseWithStrokeExperienceReview)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,StrokeHandbookGivenToPatient = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/StrokeHandbookGivenToPatient)[1]', 'varchar(max)'),'')
	,DateTimeStrokeHandbookGivenToPatient = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeStrokeHandbookGivenToPatient)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,BaselineNeuroObsPerformed = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BaselineNeuroObsPerformed)[1]', 'varchar(max)'),'')
	,AVPU = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/AVPU)[1]', 'varchar(max)'),'')
	,DateTimeRandomBloodGlucoseLevelRecorded = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeRandomBloodGlucoseLevelRecorded)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,IsThePatientSafeToEatAndDrink = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/IsThePatientSafeToEatAndDrink)[1]', 'varchar(max)'),'')
	,RiskOfMalnutritionIdentified = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/RiskOfMalnutritionIdentified)[1]', 'varchar(max)'),'')
	,DateTimeSeenByADietician = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeSeenByADietician)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeCommenceEntralFeedingViaNG = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeCommenceEntralFeedingViaNG)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,HasContinenceAssessmentBeenDone = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/HasContinenceAssessmentBeenDone)[1]', 'varchar(max)'),'')
	,DateTimeContinenceAssessmentDone = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeContinenceAssessmentDone)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ContinenceIssuesIdenitifed = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/ContinenceIssuesIdenitifed)[1]', 'varchar(max)'),'')
	,CarePlanStarted = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/CarePlanStarted)[1]', 'varchar(max)'),'')
	,DateTimeCarePlanStarted = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeCarePlanStarted)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,Weighed = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/Weighed)[1]', 'varchar(max)'),'')
	,MUST = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/MUST)[1]', 'varchar(max)'),'')
	,DateTimeFirstAssessedByPhysiotherapy = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeFirstAssessedByPhysiotherapy)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeFirstAssessedByOccupationalTherapy = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeFirstAssessedByOccupationalTherapy)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeFirstAssessedSpeechAndLanguageTherapy = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeFirstAssessedSpeechAndLanguageTherapy)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateTimeFormalSwallowAssessmentCompleted = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeFormalSwallowAssessmentCompleted)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,PerformBarthelAssessmentPreAdmission = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/PerformBarthelAssessmentPreAdmission)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsFeeding = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Feeding)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsBathing = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Bathing)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsGrooming = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Grooming)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsDressing = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Dressing)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsBowels = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Bowels)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsBladder = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Bladder)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsToiletUse = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/ToiletUse)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsTransfers = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Transfers)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsMobility = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Mobility)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsStairs = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/Stairs)[1]', 'varchar(max)'),'')
	,BarthelAssessmentPreAdmissionDetailsTotalScore = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentPreAdmissionDetails/TotalScore)[1]', 'varchar(max)'),'')
	,PerformBarthelAssessmentOnAdmission = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/PerformBarthelAssessmentOnAdmission)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsFeeding = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Feeding)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsBathing = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Bathing)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsGrooming = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Grooming)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsDressing = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Dressing)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsBowels = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Bowels)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsBladder = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Bladder)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsToiletUse = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/ToiletUse)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsTransfers = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Transfers)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsMobility = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Mobility)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsStairs = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/Stairs)[1]', 'varchar(max)'),'')
	,BarthelAssessmentOnAdmissionDetailsTotalScore = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/BarthelAssessmentOnAdmissionDetails/TotalScore)[1]', 'varchar(max)'),'')
	,ModifiedRankinScorePreAdmission = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/ModifiedRankinScorePreAdmission)[1]', 'varchar(max)'),'')
	,ModifiedRankinScoreOnAdmission = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/ModifiedRankinScoreOnAdmission)[1]', 'varchar(max)'),'')
	,DateTimeFirstAssessedByStrokePhysician = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeFirstAssessedByStrokePhysician)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DiagnosisExplainedToPatientFamily = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/DiagnosisExplainedToPatientFamily)[1]', 'varchar(max)'),'')
	,DateTimeDiagnosisExplainedToPatientFamily = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/AcuteCareBundleDTO/DateTimeDiagnosisExplainedToPatientFamily)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DiagnosisExplainedToPatientFamilyNo = 
		nullif(CandidateData.Details.value('(/AcuteCareBundleDTO/DiagnosisExplainedToPatientFamilyNo)[1]', 'varchar(max)'),'')
	,Username = 
		CandidateData.Details.value('(/AcuteCareBundleDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/AcuteCareBundleDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,IsSectionComplete = 
		SectionComplete.Details.value('(/SectionCompletedDTO/CurrentSelection)[1]', 'varchar(max)')
	,SectionCompleteTime = 
		SectionComplete.DateTimeCreated
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
left outer join 
(
	select
		*
	from
		[$(SID)].dbo.TARNCandidateData 
	where
		SectionID = 18
	and ActionID = 50
) SectionComplete
on SectionComplete.CandidateID = CandidateData.CandidateID
where
	CandidateData.SectionID = 18
and CandidateData.ActionID =99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDAcuteCareBundle', @Stats, @StartTime





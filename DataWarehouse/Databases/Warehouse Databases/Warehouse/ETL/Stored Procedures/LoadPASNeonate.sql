﻿CREATE PROCEDURE [ETL].[LoadPASNeonate]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


truncate table PAS.Neonate

INSERT INTO PAS.Neonate
(
	 NeonateID
	,ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes
	,BirthOrder
	,DiagnosisCode1
	,DiagnosisCode2
	,SourceSpellNo
	,HeadCircumference
	,SourcePatientNo
	,BCGAdministered
	,Feeding
	,FollowUpCare
	,HipsExamination
	,Jaundice
	,MetabolicScreening
	,LengthAtBirth
	,GestationLength
) 
select
	 NeonateID
	,ApgarScoreAt1Minute
	,ApgarScoreAt5Minutes
	,BirthOrder
	,DiagnosisCode1
	,DiagnosisCode2
	,SourceSpellNo
	,HeadCircumference
	,SourcePatientNo
	,BCGAdministered
	,Feeding
	,FollowUpCare
	,HipsExamination
	,Jaundice
	,MetabolicScreening
	,LengthAtBirth
	,GestationLength
from
	ETL.TLoadPASNeonate

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadPASNeonate', @Stats, @StartTime

print @Stats

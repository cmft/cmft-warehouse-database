﻿
CREATE PROCEDURE [ETL].[LoadResultGPICEResult]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, ReportTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, ReportTime)) 
from
	ETL.TLoadResultGPICEResult

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

--create table #GPICEResult
--(
--	SourceUniqueID int not null,
--	PatientID int null,
--	CasenoteNumber varchar(24) null,
--	NHSNumber varchar(10) null,
--	ClinicianID int null,
--	MainSpecialtyCode varchar(35) null,
--	SpecialtyCode varchar(35) null,
--	LocationID int not null,
--	ReportSourceUniqueID varchar(35) not null,
--	ReportStatusCode varchar(3) null,
--	ReportTime datetime null,
--	ReportComment varchar(max) null,
--	SampleTypeCode varchar(8) null,
--	SampleCollectionTime datetime null,
--	InvestigationCode varchar(10) null,
--	InvestigationComment varchar(max) null,
--	ResultCode varchar(10) null,
--	Result varchar(70) null,
--	UnitOfMeasure varchar(35) null,
--	Abnormal bit not null,
--	LowerReferenceRangeValue varchar(35) null,
--	UpperReferenceRangeValue varchar(35) null,
--	ResultComment varchar(max) null,
--	InterfaceCode varchar(5) not null,
--	ResultChecksum int null,
--)

--alter table #GPICEResult
--add constraint PK_#GPICEResult primary key (SourceUniqueID) 

--insert #GPICEResult
--(
--	SourceUniqueID
--	,PatientID
--	,CasenoteNumber
--	,NHSNumber
--	,ClinicianID
--	,MainSpecialtyCode
--	,SpecialtyCode
--	,LocationID
--	,ReportSourceUniqueID
--	,ReportStatusCode
--	,ReportTime
--	,ReportComment
--	,SampleTypeCode
--	,SampleCollectionTime
--	,InvestigationCode
--	,InvestigationComment
--	,ResultCode
--	,Result
--	,UnitOfMeasure
--	,Abnormal
--	,LowerReferenceRangeValue
--	,UpperReferenceRangeValue
--	,ResultComment
--	,InterfaceCode
--	,ResultChecksum
--)

--select
--	SourceUniqueID
--	,PatientID
--	,CasenoteNumber
--	,NHSNumber
--	,ClinicianID
--	,MainSpecialtyCode
--	,SpecialtyCode
--	,LocationID
--	,ReportSourceUniqueID
--	,ReportStatusCode
--	,ReportTime
--	,ReportComment
--	,SampleTypeCode
--	,SampleCollectionTime
--	,InvestigationCode
--	,InvestigationComment
--	,ResultCode
--	,Result
--	,UnitOfMeasure
--	,Abnormal
--	,LowerReferenceRangeValue
--	,UpperReferenceRangeValue
--	,ResultComment
--	,InterfaceCode
--	,ResultChecksum = 
--	checksum(
--		 SourceUniqueID
--		,PatientID
--		,CasenoteNumber
--		,NHSNumber
--		,ClinicianID
--		,MainSpecialtyCode
--		,SpecialtyCode
--		,LocationID
--		,ReportSourceUniqueID
--		,ReportStatusCode
--		,ReportTime
--		,ReportComment
--		,SampleTypeCode
--		,SampleCollectionTime
--		,InvestigationCode
--		,InvestigationComment
--		,ResultCode
--		,Result
--		,UnitOfMeasure
--		,Abnormal
--		,LowerReferenceRangeValue
--		,UpperReferenceRangeValue
--		,ResultComment
--		,InterfaceCode
--	)				
--from
--	ETL.TLoadResultGPICEResult

merge
	Result.GPICEResult target
using
	(
	select
		 SourceUniqueID
		,PatientID
		,PatientIdentifier
		,DistrictNo
		,CasenoteNumber
		,NHSNumber
		,DateOfBirth
		,SexCode
		,ClinicianID
		,MainSpecialtyCode
		,SpecialtyCode
		,LocationID
		,ReportSourceUniqueID
		,ReportStatusCode
		,ReportTime
		,ReportComment
		,SampleReferenceCode
		,SampleTypeCode
		,SampleType
		,SampleCollectionTime
		,InvestigationCode
		,InvestigationName
		,InvestigationComment
		,ResultCode
		,ResultName
		,Result
		,UnitOfMeasurement
		,Abnormal
		,LowerReferenceRangeValue
		,UpperReferenceRangeValue
		,ResultComment
		,InterfaceCode
		,ResultChecksum = 
			checksum(
				 SourceUniqueID
				,PatientID
				,PatientIdentifier
				,DistrictNo
				,CasenoteNumber
				,NHSNumber
				,DateOfBirth
				,SexCode
				,ClinicianID
				,MainSpecialtyCode
				,SpecialtyCode
				,LocationID
				,ReportSourceUniqueID
				,ReportStatusCode
				,ReportTime
				,ReportComment
				,SampleReferenceCode
				,SampleTypeCode
				,SampleType
				,SampleCollectionTime
				,InvestigationCode
				,InvestigationName
				,InvestigationComment
				,ResultCode
				,ResultName
				,Result
				,UnitOfMeasurement
				,Abnormal
				,LowerReferenceRangeValue
				,UpperReferenceRangeValue
				,ResultComment
				,InterfaceCode
			)			
	from
		ETL.TLoadResultGPICEResult
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ReportTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber
			,DateOfBirth
			,SexCode
			,ClinicianID
			,MainSpecialtyCode
			,SpecialtyCode
			,LocationID
			,ReportSourceUniqueID
			,ReportStatusCode
			,ReportTime
			,ReportComment
			,SampleReferenceCode
			,SampleTypeCode
			,SampleType
			,SampleCollectionTime
			,InvestigationCode
			,InvestigationName
			,InvestigationComment
			,ResultCode
			,ResultName
			,Result
			,UnitOfMeasurement
			,Abnormal
			,LowerReferenceRangeValue
			,UpperReferenceRangeValue
			,ResultComment
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,ResultChecksum
			)
		values
			(
			 source.SourceUniqueID
			,source.PatientID
			,source.PatientIdentifier
			,source.DistrictNo
			,source.CasenoteNumber
			,source.NHSNumber
			,source.DateOfBirth
			,source.SexCode
			,source.ClinicianID
			,source.MainSpecialtyCode
			,source.SpecialtyCode
			,source.LocationID
			,source.ReportSourceUniqueID
			,source.ReportStatusCode
			,source.ReportTime
			,source.ReportComment
			,source.SampleReferenceCode
			,source.SampleTypeCode
			,source.SampleType
			,source.SampleCollectionTime
			,source.InvestigationCode
			,source.InvestigationName
			,source.InvestigationComment
			,source.ResultCode
			,source.ResultName
			,source.Result
			,source.UnitOfMeasurement
			,source.Abnormal
			,source.LowerReferenceRangeValue
			,source.UpperReferenceRangeValue
			,source.ResultComment
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.ResultChecksum
			)

	when matched
	and	target.ResultChecksum <> source.ResultChecksum
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.PatientID = source.PatientID
			,target.PatientIdentifier = source.PatientIdentifier
			,target.DistrictNo = source.DistrictNo
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SexCode = source.SexCode
			,target.ClinicianID = source.ClinicianID
			,target.MainSpecialtyCode = source.MainSpecialtyCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.LocationID = source.LocationID
			,target.ReportSourceUniqueID = source.ReportSourceUniqueID
			,target.ReportStatusCode = source.ReportStatusCode
			,target.ReportTime = source.ReportTime
			,target.ReportComment = source.ReportComment
			,target.SampleReferenceCode = source.SampleReferenceCode
			,target.SampleTypeCode = source.SampleTypeCode
			,target.SampleType = source.SampleType
			,target.SampleCollectionTime = source.SampleCollectionTime
			,target.InvestigationCode = source.InvestigationCode
			,target.InvestigationName = source.InvestigationName
			,target.InvestigationComment = source.InvestigationComment
			,target.ResultCode = source.ResultCode
			,target.ResultName = source.ResultName
			,target.Result = source.Result
			,target.UnitOfMeasurement = source.UnitOfMeasurement
			,target.Abnormal = source.Abnormal
			,target.LowerReferenceRangeValue = source.LowerReferenceRangeValue
			,target.UpperReferenceRangeValue = source.UpperReferenceRangeValue
			,target.ResultComment = source.ResultComment
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.ResultChecksum = source.ResultChecksum

output
	 coalesce(inserted.ResultRecno, deleted.ResultRecno)
	,$action
	into
		Result.GPICEProcessList
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			Result.GPICEProcessList
		) MergeSummary
;

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
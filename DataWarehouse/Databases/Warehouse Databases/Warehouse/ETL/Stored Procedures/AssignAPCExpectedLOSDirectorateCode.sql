﻿




CREATE procedure [ETL].[AssignAPCExpectedLOSDirectorateCode] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @from varchar(19)
declare @to varchar(19)


select @StartTime = getdate()
select @RowsUpdated = 0

update
	APC.ExpectedLOS
set
	DirectorateCode = Activity.DirectorateCode
from
	(
	select
		 Encounter.SourceUniqueID
		,DivisionRuleBase.DirectorateCode
	from
		APC.ExpectedLOS Encounter

	left outer join PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

	left outer join PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

	inner join WH.DivisionRuleBase
	on	DivisionRuleBase.SiteCode = Encounter.SiteCode
	and	DivisionRuleBase.SpecialtyCode = Encounter.Specialty

		and	
		(
		
		 dbo.f_get_patient_category(
								 AdmissionMethod.InternalCode
								,ManagementIntention.InternalCode
								,CONVERT(Date,Encounter.AdmissionTime)
								,CONVERT(Date,Encounter.DischargeTime)
								)
			= DivisionRuleBase.PatientCategoryCode

		or	DivisionRuleBase.PatientCategoryCode is null
		)

	and	(
			Encounter.Consultant = DivisionRuleBase.ConsultantCode
		or	DivisionRuleBase.ConsultantCode is null
		)

	and	(
			Encounter.Ward = DivisionRuleBase.WardCode
		or	DivisionRuleBase.WardCode is null
		)

	and
		Encounter.AdmissionTime between DivisionRuleBase.FromDate and coalesce(DivisionRuleBase.FromDate, getdate())

	and	not exists
		(
		select
			1
		from
			WH.DivisionRuleBase Previous
		where
			Previous.SiteCode = Encounter.SiteCode
		and	Previous.SpecialtyCode = Encounter.Specialty

		and	
			(
					 dbo.f_get_patient_category(
								 AdmissionMethod.InternalCode
								,ManagementIntention.InternalCode
								,CONVERT(Date,Encounter.AdmissionTime)
								,CONVERT(Date,Encounter.DischargeTime)
								)
				= Previous.PatientCategoryCode

			or	Previous.PatientCategoryCode is null
			)

		and	(
				Encounter.Consultant = Previous.ConsultantCode
			or	Previous.ConsultantCode is null
			)

		and	(
				Encounter.Ward = Previous.WardCode
			or	Previous.WardCode is null
			)

		and

			Encounter.AdmissionTime between Previous.FromDate and coalesce(Previous.FromDate, getdate())
		
		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when Previous.WardCode is not null
				then 2
				else 0
				end +

				case
				when Previous.PatientCategoryCode is not null
				then 1
				else 0
				end

				>

				case
				when DivisionRuleBase.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when DivisionRuleBase.WardCode is not null
				then 2
				else 0
				end +

				case
				when DivisionRuleBase.PatientCategoryCode is not null
				then 1
				else 0
				end

			or
				(
					case
					when Previous.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when Previous.WardCode is not null
					then 2
					else 0
					end +

					case
					when Previous.PatientCategoryCode is not null
					then 1
					else 0
					end

					=

					case
					when DivisionRuleBase.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when DivisionRuleBase.WardCode is not null
					then 2
					else 0
					end +

					case
					when DivisionRuleBase.PatientCategoryCode is not null
					then 1
					else 0
					end

					and	Previous.DivisionRuleBaseRecno > DivisionRuleBase.DivisionRuleBaseRecno
				)
			)
		)
	) Activity

where
	Activity.SourceUniqueID = ExpectedLOS.SourceUniqueID



select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'AssignAPCExpectedLOSDirectorateCode', @Stats, @StartTime




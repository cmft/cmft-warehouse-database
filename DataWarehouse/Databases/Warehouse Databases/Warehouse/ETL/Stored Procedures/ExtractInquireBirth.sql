﻿CREATE procedure [ETL].[ExtractInquireBirth]

as

insert into ETL.TImportPASBirth
select
	 *
from
	(
	select
		 BIRTHID
		,BabyPatientNumber
		,BirthOrder
		,BirthWeight
		,CaseNoteNumber
		,Comments
		,DeliveryDate
		,DeliveryTime
		,DrugsUsed
		,left(EpisodeNumber, 12) EpisodeNumber
		,left(InternalPatientNumber, 12) InternalPatientNumber
		,KeMatDrugsUsed2
		,KeMatDtimeDelivNeut
		,KeMatLiveStillIndInt
		,KeMatMethDelivInt
		,KeMatMorDrugsInt
		,KeMatMorPpInt
		,KeMatPresFetusInt
		,SuspCongAnomaly
	from
		[$(PAS)].Inquire.BIRTH

) Birth

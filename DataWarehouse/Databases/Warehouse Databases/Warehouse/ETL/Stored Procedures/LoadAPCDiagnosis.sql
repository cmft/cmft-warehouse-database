﻿CREATE PROCEDURE [ETL].[LoadAPCDiagnosis]
	 @from smalldatetime
	,@to smalldatetime
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()



exec Utility.AlterTableIndex 'DISABLE', 'APC.Diagnosis'


delete from	APC.Diagnosis
where
	exists
	(
	select
		1
	from
		APC.Encounter
	where
		Diagnosis.APCSourceUniqueID = Encounter.SourceUniqueID
	and	Encounter.EpisodeEndDate between @from and @to
	)


SELECT @RowsDeleted = @@Rowcount

/* 
	Originally omitted from this (ETL) version. Proc will now handle orphaned records 
	brought about by changes in apcsourceuniqueids - DG 10.01.2014	
*/


delete from	APC.Diagnosis
where
	not exists
	(
		select
			1
		from
			APC.Encounter
		where
			Diagnosis.APCSourceUniqueID = Encounter.SourceUniqueID
	)

SELECT @RowsDeleted = @RowsDeleted + @@Rowcount

INSERT INTO APC.Diagnosis
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,APCSourceUniqueID

	,Created = getdate()
	,ByWhom = system_user
from
	ETL.TLoadAPCDiagnosis TEncounter
where
	not exists
	(
	select
		1
	from
		APC.Diagnosis
	where
		Diagnosis.SourceUniqueID = TEncounter.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount


exec Utility.AlterTableIndex 'REBUILD', 'APC.Diagnosis'

--update the appropriate columns on the APC.Encounter

update
	APC.Encounter
set
	 SecondaryDiagnosisCode1 = Diagnosis1.DiagnosisCode
	,SecondaryDiagnosisCode2 = Diagnosis2.DiagnosisCode
	,SecondaryDiagnosisCode3 = Diagnosis3.DiagnosisCode
	,SecondaryDiagnosisCode4 = Diagnosis4.DiagnosisCode
	,SecondaryDiagnosisCode5 = Diagnosis5.DiagnosisCode
	,SecondaryDiagnosisCode6 = Diagnosis6.DiagnosisCode
	,SecondaryDiagnosisCode7 = Diagnosis7.DiagnosisCode
	,SecondaryDiagnosisCode8 = Diagnosis8.DiagnosisCode
	,SecondaryDiagnosisCode9 = Diagnosis9.DiagnosisCode
	,SecondaryDiagnosisCode10 = Diagnosis10.DiagnosisCode
	,SecondaryDiagnosisCode11 = Diagnosis11.DiagnosisCode
	,SecondaryDiagnosisCode12 = Diagnosis12.DiagnosisCode
	,SecondaryDiagnosisCode13 = Diagnosis13.DiagnosisCode
from
	APC.Encounter

left join APC.Diagnosis Diagnosis1
on	Diagnosis1.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis1.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis1.SequenceNo = 1

left join APC.Diagnosis Diagnosis2
on	Diagnosis2.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis2.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis2.SequenceNo = 2

left join APC.Diagnosis Diagnosis3
on	Diagnosis3.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis3.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis3.SequenceNo = 3

left join APC.Diagnosis Diagnosis4
on	Diagnosis4.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis4.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis4.SequenceNo = 4

left join APC.Diagnosis Diagnosis5
on	Diagnosis5.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis5.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis5.SequenceNo = 5

left join APC.Diagnosis Diagnosis6
on	Diagnosis6.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis6.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis6.SequenceNo = 6

left join APC.Diagnosis Diagnosis7
on	Diagnosis7.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis7.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis7.SequenceNo = 7

left join APC.Diagnosis Diagnosis8
on	Diagnosis8.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis8.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis8.SequenceNo = 8

left join APC.Diagnosis Diagnosis9
on	Diagnosis9.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis9.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis9.SequenceNo = 9

left join APC.Diagnosis Diagnosis10
on	Diagnosis10.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis10.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis10.SequenceNo = 10

left join APC.Diagnosis Diagnosis11
on	Diagnosis11.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis11.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis11.SequenceNo = 11

left join APC.Diagnosis Diagnosis12
on	Diagnosis12.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis12.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis12.SequenceNo = 12

left join APC.Diagnosis Diagnosis13
on	Diagnosis13.ProviderSpellNo = Encounter.ProviderSpellNo
and	Diagnosis13.SourceEncounterNo = Encounter.SourceEncounterNo
and	Diagnosis13.SequenceNo = 13



SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadAPCDiagnosis', @Stats, @StartTime

print @Stats

﻿CREATE procedure [ETL].[AssignOPBreachDate] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsUpdated = 0

update
	OP.Encounter
set

--use this code when we switch on RTT

---- Set the BreachDate to ClockStartDate + RTT breach days

--	RTTBreachDate =
--		dateadd(
--			day
--			,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
--			,coalesce(
--	--manually entered
--				 RTTClockStartOP.ClockStartDate
--	--otherwise get the earliest date
--				,case
--				when Encounter.RTTStartDate < Encounter.ReferralDate
--				then Encounter.RTTStartDate
--				else Encounter.ReferralDate
--				end
--			)
--		)


--	,ClockStartDate =
--		coalesce(
----manually entered
--			 RTTClockStartOP.ClockStartDate
----otherwise get the earliest date
--			,case
--			when Encounter.RTTStartDate < Encounter.ReferralDate
--			then Encounter.RTTStartDate
--			else Encounter.ReferralDate
--			end
--		)


--from
--	OP.Encounter

--left join RTT.dbo.RTTOPClockStop RTTClockStartOP
--on    RTTClockStartOP.SourcePatientNo = Encounter.SourcePatientNo
--and   RTTClockStartOP.SourceEntityRecno = Encounter.SourceEncounterNo
--and	  RTTClockStartOP.ClockStartDate is not null



-- presumably this is the pre-RTT version
	RTTBreachDate =
		dateadd(
			day
			,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS')
			,case

			when Encounter.RTTStartDate is not null
			then Encounter.QM08StartWaitDate

			when datediff(day, Encounter.QM08StartWaitDate, Encounter.AppointmentDate) < 0
			then
			coalesce(
				(
				select
					max(MaxEncounter.AppointmentDate) 
				from
					OP.Encounter MaxEncounter
				where
					MaxEncounter.SourcePatientNo = Encounter.SourcePatientNo
				and	MaxEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
				and	MaxEncounter.DNACode in ('2', '3') --patient cancelled or DNA
				and	MaxEncounter.FirstAttendanceFlag = 1
				)
				,Encounter.ReferralDate
			)
			else Encounter.ReferralDate
			end
		)

	,ClockStartDate =
		case

		when Encounter.RTTStartDate is not null
		then Encounter.QM08StartWaitDate

		when datediff(day, Encounter.QM08StartWaitDate, Encounter.AppointmentDate) < 0
		then
		coalesce(
			(
			select
				max(MaxEncounter.AppointmentDate) 
			from
				OP.Encounter MaxEncounter
			where
				MaxEncounter.SourcePatientNo = Encounter.SourcePatientNo
			and	MaxEncounter.SourceEncounterNo = Encounter.SourceEncounterNo
			and	MaxEncounter.DNACode in ('2', '3') --patient cancelled or DNA
			and	MaxEncounter.FirstAttendanceFlag = 1
			)
			,Encounter.ReferralDate
		)
		else Encounter.ReferralDate
		end

select
	@RowsUpdated = @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute , @StartTime , getdate())

SELECT @Stats = 
	'Rows Updated ' + CONVERT(varchar(10), @RowsUpdated) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'AssignOPBreachDate', @Stats, @StartTime

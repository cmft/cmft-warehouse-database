﻿
CREATE Procedure [ETL].[LoadRADWaitingList] 
(
	@ImportDateTimeStamp datetime
)

as

	/******************************************************************************
	**  Name: LoadRADWaitingList
	**  Purpose: 
	**
	**  Import procedure for Radiology RAD.WaitingList table rows 
	**
	**	Called by LoadRADWL
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 26.06.12     MH		Altered for CMFT environment
	** 03.10.12     MH		Included CensusDate column in the import table
	** 01.11.12     MH		Added StatusType
	** 20150924	Paul Egan	Added clock-reset columns
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CensusDate smalldatetime
declare @InterfaceCode char(5)
declare @Process varchar(255)

select @StartTime = getdate()

select @Process = 'LoadRADWaitingList'

select @CensusDate = (select max(COALESCE(CensusDate,RequestDate)) from ETL.TLoadRADWaitingList where ImportDateTimeStamp = @ImportDateTimeStamp)

select @RowsDeleted = 0

-- Clear down old snapshots

--delete from RAD.WaitingList
--where 
--	CensusDate < dateadd(day, (select NumericValue * -1 from ETL.Parameter where Parameter = 'KEEPSNAPSHOTDAYS'), getdate())
--and	CensusDate not in (select max(CensusDate) from RAD.WaitingList group by datepart(year, CensusDate), datepart(month, CensusDate))
--select @RowsDeleted = @@ROWCOUNT

delete from RAD.WaitingList
where WaitingList.CensusDate = @CensusDate

select @RowsDeleted = @RowsDeleted + @@ROWCOUNT



INSERT INTO [RAD].[WaitingList]
	(
	 SourceUniqueID
	,CensusDate
	,DistrictNo
	,SourcePatientNo
	,PatientSurname
	,PatientForenames
	,PatientTitle
	,DateOfBirth
	,SexCode
	,NHSNumber
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,ExamCode
	,SiteCode
	,StatusCode
	,RequestDate
	,EventDate
	,DaysWaiting		-- Removed 'Adjusted' from name. Paul Egan 20150924
	,WeeksWaiting
	,ModalityCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,RegisteredPCTCode
	,SpecialtyCode
	,PatientTypeCode
	,UrgencyCode
	,Comment
	,ExamGroupCode1
	,ExamGroupCode2
	,ReferralSourceCode
	,ReferrerCode
	,ReferralLocationCode
	,WaitingButNotPlanned
	,Planned
	,Scheduled
	,Waiting
	,Created
	,ByWhom
	,StatusType
	
	/* Added Paul Egan 20150924 */
	,ClockResetDate
	,DaysWaitingAdjusted
	,BreachDate
	,ExamSourceUniqueID
	)

SELECT
	 SourceUniqueID
	,CensusDate = @CensusDate	
	,DistrictNo
	,SourcePatientNo
	,PatientSurname
	,PatientForenames
	,PatientTitle
	,DateOfBirth
	,SexCode
	,NHSNumber
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,Postcode
	,ExamCode
	,SiteCode
	,StatusCode
	,RequestDate
	,EventDate
	,DaysWaiting		-- Removed 'Adjusted' from name. Paul Egan 20150924
	,WeeksWaiting
	,ModalityCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,RegisteredPCTCode
	,SpecialtyCode
	,PatientTypeCode
	,UrgencyCode
	,Comment
	,ExamGroupCode1
	,ExamGroupCode2
	,ReferralSourceCode
	,ReferrerCode
	,ReferralLocationCode
	,WaitingButNotPlanned
	,Planned
	,Scheduled
	,Waiting

	,Created = getdate()
	,ByWhom = system_user
	,StatusType
	
	/* Added Paul Egan 20150924 */
	,ClockResetDate
	,DaysWaitingAdjusted
	,BreachDate
	,ExamSourceUniqueID
FROM
	ETL.TLoadRADWaitingList
where
	ImportDateTimeStamp = @ImportDateTimeStamp



select @RowsInserted = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@CensusDate, ''))


exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

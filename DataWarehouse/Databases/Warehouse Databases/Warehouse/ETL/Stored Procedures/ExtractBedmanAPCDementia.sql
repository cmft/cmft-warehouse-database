﻿




-- USE [Warehouse]
-- drop view [ETL].[TLoadAPCDementia]


CREATE Procedure [ETL].[ExtractBedmanAPCDementia]

as

/***************************************************************************
**	20140626	RR	Create Dementia CMFT using GC script and DG Bedman logic
**	20150128	RR	Process updated to account for cross site pathways
****************************************************************************/

Truncate table ETL.TImportAPCDementia
Insert into ETL.TImportAPCDementia
	(
	ProviderSpellNo
	,SpellID
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,AdmissionWardCode
	,RecordedDate
	,RecordedTime
	,KnownToHaveDementia
	,ForgetfulnessQuestionAnswered
	,AssessmentScore
	,Investigation 
	,AssessmentNotPerformedReason 
	,ReferralSentDate 
	,InterfaceCode
	)
select 
	ProviderSpellNo
	,SpellID
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,AdmissionWardCode
	,RecordedDate
	,RecordedTime
	,KnownToHaveDementia
	,ForgetfulnessQuestionAnswered
	,AssessmentScore = AMTScore
	,Investigation = CognitiveInvestigationStarted
	,AssessmentNotPerformedReason = 
			case
				when AMTTestNotPerformedReason = '' then Null
				else AMTTestNotPerformedReason 
			end
	,ReferralSentDate = DementiaReferralSentDate
	,InterfaceCode = 'BEDMAN' 
from
	(select
		ProviderSpellNo = Spell.Patient + '/' + Spell.IPEpisode
		,SpellID	 = Spell.SpellID
		,SourceUniqueID = Dementia.ID
		,SourcePatientNo = Spell.Patient
		,SourceSpellNo = Spell.IPEpisode
		,RecordedDate = cast(LastUpdated_TS as date)
		,RecordedTime = LastUpdated_TS
		,KnownToHaveDementia =
						case
							when Details.value('(DementiaAssessmentData/KnownToHaveDementia)[1]', 'VARCHAR(60)') = 'N'
							then 0 
							when Details.value('(DementiaAssessmentData/KnownToHaveDementia)[1]', 'VARCHAR(60)') = 'Y'
							then 1
							else null
						end
		,PatientDementiaFlag = 
						case
							when Details.value('(DementiaAssessmentData/KnownToHaveDementia)[1]', 'VARCHAR(60)') = 'Y'
							then 1
							when Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[1]', 'VARCHAR(60)') between 0 and 7 
							then 1
							when Details.value('(DementiaAssessmentData/PatientDementiaFlag)[1]', 'VARCHAR(60)') = 'Y'
							then 1
							when Details.value('(DementiaAssessmentData/PatientDementiaFlag)[1]', 'VARCHAR(60)') = 'N'
							then 0
							else null
						end
		,RememberMePlan = 
						case
							when Details.value('(DementiaAssessmentData/RememberMePlan)[1]', 'VARCHAR(60)') = 'False'
							then 0
							when Details.value('(DementiaAssessmentData/RememberMePlan)[1]', 'VARCHAR(60)') = 'True'
							then 1
							else null
						end
		,RememberMePlanCommenced = 
						cast(left(
							nullif(Details.value('(DementiaAssessmentData/RememberMePlanCommenced)[1]', 'VARCHAR(60)'),'')
							,10) 
						as date)
		,RememberMePlanDiscontinued = 
						cast(left(
							nullif(Details.value('(DementiaAssessmentData/RememberMePlanDiscontinued)[1]', 'VARCHAR(60)'),'')
							,10) 
						as date)
		,ForgetfulnessQuestionAnswered = 
						case
							when Details.value('(DementiaAssessmentData/ForgetfulnessQuestionAnswered)[1]', 'VARCHAR(60)') = 'N' 
							then 0
							when Details.value('(DementiaAssessmentData/ForgetfulnessQuestionAnswered)[1]', 'VARCHAR(60)') = 'Y' 
							then 1
							else null
						end
		,AMTTestPerformed = 
						case
							when Details.value('(DementiaAssessmentData/AMTTestPerformed)[1]', 'VARCHAR(60)') = 'N' 
							then 0
							when Details.value('(DementiaAssessmentData/AMTTestPerformed)[1]', 'VARCHAR(60)') = 'Y' 
							then 1
							else null
						end
		,ClinicalIndicationsPresent = 
						case 
							when Details.value('(DementiaAssessmentData/ClinicalIndicationsPresent)[1]', 'VARCHAR(60)') = 'N' 
							then 0
							when Details.value('(DementiaAssessmentData/ClinicalIndicationsPresent)[1]', 'VARCHAR(60)') = 'Y' 
							then 1
							else null
						end
		,AMTScore = coalesce -- pulling the latest score available
						(
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[10]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[9]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[8]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[7]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[6]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[5]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[4]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[3]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[2]', 'VARCHAR(60)'),
						Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[1]', 'VARCHAR(60)')
						) 
		,AMTTestNotPerformedReason = 
						case
							when Details.value('(DementiaAssessmentData/AMTTests/AMTTest/Score)[1]', 'VARCHAR(60)') >= 0 
							then null
							else Details.value('(DementiaAssessmentData/AMTTestNotPerformedReason)[1]', 'VARCHAR(60)')
						end
		,CognitiveInvestigationStarted = 
												case
							when Details.value('(DementiaAssessmentData/CognitiveInvestigationStarted)[1]', 'VARCHAR(60)') = 'N' 
							then 0
							when Details.value('(DementiaAssessmentData/CognitiveInvestigationStarted)[1]', 'VARCHAR(60)') = 'Y' 
							then 1
							else null
						end
		,ReferralNeeded = Null
		,DementiaReferralSentDate = 
						case
							when Details.value('(DementiaAssessmentData/DementiaReferralSentDate)[1]', 'VARCHAR(60)') <> ' '
							then left(Details.value('(DementiaAssessmentData/DementiaReferralSentDate)[1]', 'VARCHAR(60)'),10)
							else null
						end
		,SequenceNo = row_number ( ) over (partition by Dementia.HospSpellID order by LastUpdated_TS asc)
		,AdmissionWardCode = Locations.PASCode
	from
		[$(BedmanTest)].dbo.HospSpell Spell

	inner join [$(BedmanTest)].dbo.[Event] Dementia
	on Spell.SpellID = Dementia.HospSpellID
	and EventTypeID = 11

	inner join [$(BedmanTest)].dbo.Locations Locations
	on Dementia.LocationID = Locations.LocationID

	where 
		Spell.Patient is not null 
	and Spell.IPEpisode is not null
	
	) 
	DementiaEncounter




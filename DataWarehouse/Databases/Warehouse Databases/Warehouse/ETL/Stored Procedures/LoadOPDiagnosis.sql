﻿CREATE PROCEDURE [ETL].[LoadOPDiagnosis]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


--delete for all related FCEs
delete from OP.Diagnosis 
where
	exists
	(
	select
		1
	from
		ETL.TLoadOPDiagnosis
	where
		TLoadOPDiagnosis.SourcePatientNo = OP.Diagnosis.SourcePatientNo
	and	TLoadOPDiagnosis.SourceEncounterNo = OP.Diagnosis.SourceEncounterNo
	)


SELECT @RowsDeleted = @@Rowcount


INSERT INTO OP.Diagnosis
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID
	,Created
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID

	,Created = getdate()
	,ByWhom = system_user
from
	ETL.TLoadOPDiagnosis TEncounter
where
	not exists
	(
	select
		1
	from
		OP.Diagnosis
	where
		Diagnosis.SourceUniqueID = TEncounter.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount

--update the appropriate columns on the OP.Encounter
update
	OP.Encounter
set
	 SecondaryDiagnosisCode1 = Diagnosis1.DiagnosisCode
	,SecondaryDiagnosisCode2 = Diagnosis2.DiagnosisCode
	,SecondaryDiagnosisCode3 = Diagnosis3.DiagnosisCode
	,SecondaryDiagnosisCode4 = Diagnosis4.DiagnosisCode
	,SecondaryDiagnosisCode5 = Diagnosis5.DiagnosisCode
	,SecondaryDiagnosisCode6 = Diagnosis6.DiagnosisCode
	,SecondaryDiagnosisCode7 = Diagnosis7.DiagnosisCode
	,SecondaryDiagnosisCode8 = Diagnosis8.DiagnosisCode
	,SecondaryDiagnosisCode9 = Diagnosis9.DiagnosisCode
	,SecondaryDiagnosisCode10 = Diagnosis10.DiagnosisCode
	,SecondaryDiagnosisCode11 = Diagnosis11.DiagnosisCode
	,SecondaryDiagnosisCode12 = Diagnosis12.DiagnosisCode
from
	OP.Encounter

inner join OP.Diagnosis Diagnosis1
on	Diagnosis1.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis1.SequenceNo = 1

left join OP.Diagnosis Diagnosis2
on	Diagnosis2.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis2.SequenceNo = 2

left join OP.Diagnosis Diagnosis3
on	Diagnosis3.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis3.SequenceNo = 3

left join OP.Diagnosis Diagnosis4
on	Diagnosis4.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis4.SequenceNo = 4

left join OP.Diagnosis Diagnosis5
on	Diagnosis5.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis5.SequenceNo = 5

left join OP.Diagnosis Diagnosis6
on	Diagnosis6.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis6.SequenceNo = 6

left join OP.Diagnosis Diagnosis7
on	Diagnosis7.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis7.SequenceNo = 7

left join OP.Diagnosis Diagnosis8
on	Diagnosis8.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis8.SequenceNo = 8

left join OP.Diagnosis Diagnosis9
on	Diagnosis9.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis9.SequenceNo = 9

left join OP.Diagnosis Diagnosis10
on	Diagnosis10.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis10.SequenceNo = 10

left join OP.Diagnosis Diagnosis11
on	Diagnosis11.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis11.SequenceNo = 11

left join OP.Diagnosis Diagnosis12
on	Diagnosis12.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis12.SequenceNo = 12



SELECT @Elapsed = DATEDIFF(minute, @StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadOPDiagnosis', @Stats, @StartTime

print @Stats

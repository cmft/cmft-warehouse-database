﻿
CREATE proc [ETL].[LoadPASPatientSpecialRegister]
as

/****************************************************************************************
	Stored procedure : ETL.LoadPASPatientSpecialRegister
	Description		 : Merge new/updated Special Register data into production

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	14/08/2014	Paul Egan       Initial Coding
	19/10/2015	Rachel Royston	Updated
*****************************************************************************************/

declare @ProcedureName varchar(255) 
	= OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

declare @StartTime datetime = getdate();
declare @Elapsed int;
declare @Stats varchar(255);
declare @FromDate datetime;
declare @ToDate datetime;

declare
	 @deleted int
	,@updated int
	,@inserted int


declare @MergeSummary TABLE(Action nvarchar(10));

set @FromDate = (select min(EnteredDate) from ETL.TLoadPASPatientSpecialRegister)
set @ToDate = (select max(EnteredDate) from ETL.TLoadPASPatientSpecialRegister)


merge
	PAS.PatientSpecialRegister target
using
	ETL.TLoadPASPatientSpecialRegister source
on
	source.SourcePatientNo = target.SourcePatientNo
	and	source.SpecialRegisterCode = target.SpecialRegisterCode

	when not matched by source 
	and target.EnteredDate between @FromDate and @ToDate
	
	then delete
	
	when not matched then insert
	(
	SourcePatientNo
	,SpecialRegisterCode
	,DistrictNo
	,NHSNumber
	,EnteredDate
	,ActivityTime
	,SourceEpisodeNo
	,Created
	,CreatedByWhom
	,Updated
	,UpdatedByWhom
	)
	values
	(
	 source.SourcePatientNo
	,source.SpecialRegisterCode
	,source.DistrictNo
	,source.NHSNumber
	,source.EnteredDate
	,source.ActivityTime
	,source.SourceEpisodeNo
	,getdate()
	,suser_name()
	,getdate()
	,suser_name()
	)
	
	when matched 
	and not
	(
		isnull(target.DistrictNo, 0) = isnull(source.DistrictNo, 0)
	and isnull(target.NHSNumber, 0) = isnull(source.NHSNumber, 0)
	and isnull(target.EnteredDate,  getdate()) = isnull(source.EnteredDate,  getdate())
	and isnull(target.ActivityTime,  getdate()) = isnull(source.ActivityTime,  getdate())
	and isnull(target.SourceEpisodeNo, 0) = isnull(source.SourceEpisodeNo, 0)
	)
then update set
	target.DistrictNo = source.DistrictNo
	,target.NHSNumber = source.NHSNumber
	,target.EnteredDate = source.EnteredDate
	,target.ActivityTime = source.ActivityTime
	,target.SourceEpisodeNo = source.SourceEpisodeNo
	,target.Updated = getdate()
	,target.UpdatedByWhom = suser_name()
	
output $Action into @MergeSummary	
;

/* Stats */
select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary
;

select @Elapsed = datediff(minute,@StartTime,getdate());

select @Stats = 

	'Deleted ' + convert(varchar(10), @deleted)  + 
	', Updated '  + convert(varchar(10), @updated) +  
	', Inserted '  + convert(varchar(10), @inserted) + 
	', Elapsed ' + convert(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	convert(varchar(11), @FromDate) + ' to ' + convert(varchar(11), @ToDate)
;

print @Stats;

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime;

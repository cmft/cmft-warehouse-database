﻿
CREATE PROCEDURE [ETL].[LoadResultCWSNumericResult]

as

set dateformat ymd

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, EffectiveTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, EffectiveTime)) 
from
	ETL.TLoadResultCWSNumericResult

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Result.CWSNumericResult target
using
	(
	select
		SourceUniqueID
		,SourcePatientNo = cast(SourcePatientNo as int)
		,SourceEpisodeNo = cast(SourceEpisodeNo as int)
		,TransactionNo = cast(TransactionNo as int)
		,SessionNo = cast(SessionNo as int)
		,OrderSourceUniqueID =  cast(OrderSourceUniqueID as varchar(50))
		,ConsultantCode = cast(ConsultantCode as varchar(10))
		,SpecialtyCode = cast(SpecialtyCode as varchar(8))
		,OrderPriorityCode = cast(OrderPriorityCode as varchar(20))
		,OrderStatusCode = cast(OrderStatusCode as tinyint)
		,OrderEnteredByCode = cast(OrderEnteredByCode as varchar(4))
		,OrderedByCode = cast(OrderedByCode as varchar(20))
		,OrderTime = cast(OrderTime as varchar(39))
		,OrderComment = cast(OrderComment as varchar(max))
		,SampleReferenceCode = cast(SampleReferenceCode as varchar(50))
		,EffectiveTime = cast(EffectiveTime as datetime)
		,LocationCode = cast(LocationCode as varchar(4))
		,ServiceCode = cast(ServiceCode as varchar(20))
		,ResultCode = cast(ResultCode as varchar(4))
		,Result = cast(Result as varchar(9))
		,ResultTime = cast(ResultTime as datetime)
		,RangeIndicator = cast(RangeIndicator as smallint)
		,ResultEnteredByCode = cast(ResultEnteredByCode as varchar(4))
		,ResultStatusCode = cast(ResultStatusCode as smallint)
		,ResultComment = cast(ResultComment as varchar(max))
		,InterfaceCode = cast(InterfaceCode as varchar(3))
		,ResultChecksum = 
		checksum(
			SourceUniqueID
			,SourcePatientNo
			,SourceEpisodeNo
			,TransactionNo
			,SessionNo
			,OrderSourceUniqueID
			,ConsultantCode
			,SpecialtyCode
			,OrderPriorityCode
			,OrderStatusCode
			,OrderEnteredByCode
			,OrderedByCode
			,OrderTime
			,OrderComment
			,SampleReferenceCode
			,EffectiveTime
			,LocationCode
			,ServiceCode
			,ResultCode
			,Result
			,ResultTime
			,RangeIndicator
			,ResultEnteredByCode
			,ResultStatusCode
			,ResultComment
			,InterfaceCode
		)				
	from
		ETL.TLoadResultCWSNumericResult
	) source
	on	source.TransactionNo = target.TransactionNo
	and	source.SessionNo = target.SessionNo
	and	source.ResultCode = target.ResultCode

	when not matched by source
	and	target.EffectiveTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,SourcePatientNo
			,SourceEpisodeNo
			,TransactionNo
			,SessionNo
			,OrderSourceUniqueID
			,ConsultantCode
			,SpecialtyCode
			,OrderPriorityCode
			,OrderStatusCode
			,OrderEnteredByCode
			,OrderedByCode
			,OrderTime
			,OrderComment
			,SampleReferenceCode
			,EffectiveTime
			,LocationCode
			,ServiceCode
			,ResultCode
			,Result
			,ResultTime
			,RangeIndicator
			,ResultEnteredByCode
			,ResultStatusCode
			,ResultComment
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,ResultChecksum
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceEpisodeNo
			,source.TransactionNo
			,source.SessionNo
			,source.OrderSourceUniqueID
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.OrderPriorityCode
			,source.OrderStatusCode
			,source.OrderEnteredByCode
			,source.OrderedByCode
			,source.OrderTime
			,source.OrderComment
			,source.SampleReferenceCode
			,source.EffectiveTime
			,source.LocationCode
			,source.ServiceCode
			,source.ResultCode
			,source.Result
			,source.ResultTime
			,source.RangeIndicator
			,source.ResultEnteredByCode
			,source.ResultStatusCode
			,source.ResultComment
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.ResultChecksum
			)

	when matched
	and	target.ResultChecksum <> source.ResultChecksum
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEpisodeNo = source.SourceEpisodeNo
			,target.TransactionNo = source.TransactionNo
			,target.SessionNo = source.SessionNo
			,target.OrderSourceUniqueID = source.OrderSourceUniqueID
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.OrderPriorityCode = source.OrderPriorityCode
			,target.OrderStatusCode = source.OrderStatusCode
			,target.OrderEnteredByCode = source.OrderEnteredByCode
			,target.OrderedByCode = source.OrderedByCode
			,target.OrderTime = source.OrderTime
			,target.OrderComment = source.OrderComment
			,target.SampleReferenceCode = source.SampleReferenceCode
			,target.EffectiveTime = source.EffectiveTime
			,target.LocationCode = source.LocationCode
			,target.ServiceCode = source.ServiceCode
			,target.ResultCode = source.ResultCode
			,target.Result = source.Result
			,target.ResultTime = source.ResultTime
			,target.RangeIndicator = source.RangeIndicator
			,target.ResultEnteredByCode = source.ResultEnteredByCode
			,target.ResultStatusCode = source.ResultStatusCode
			,target.ResultComment = source.ResultComment
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.ResultChecksum = source.ResultChecksum

output
	 coalesce(inserted.ResultRecno, deleted.ResultRecno)
	,$action
	into
		Result.CWSNumericProcessList
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			Result.CWSNumericProcessList
		) MergeSummary
;


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
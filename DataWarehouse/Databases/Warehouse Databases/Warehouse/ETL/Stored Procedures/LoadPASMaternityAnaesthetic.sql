﻿CREATE PROCEDURE [ETL].[LoadPASMaternityAnaesthetic]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


truncate table PAS.MaternityAnaesthetic

INSERT INTO PAS.MaternityAnaesthetic
(
	 MaternityAnaestheticID
	,AnalgesicDuringLabour
	,DuringLabourAnaestheticCode
	,DuringLabourAnaestheticReasonCode
	,PostLabourAnaestheticCode
	,PostLabourAnaestheticReasonCode
	,SourceSpellNo
	,SourcePatientNo
) 
select
	 MaternityAnaestheticID
	,AnalgesicDuringLabour
	,DuringLabourAnaestheticCode
	,DuringLabourAnaestheticReasonCode
	,PostLabourAnaestheticCode
	,PostLabourAnaestheticReasonCode
	,SourceSpellNo
	,SourcePatientNo
from
	ETL.TLoadPASMaternityAnaesthetic

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadPASMaternityAnaesthetic', @Stats, @StartTime

print @Stats

﻿
CREATE PROCEDURE [ETL].[LoadSymphonyAttendanceExtraField]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Attendance Extra Field table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 AttendanceExtraFieldID
			,FieldTypeID
			,FieldID
			,AttendanceID
			,FieldValue
			,DataCategoryID
			,RecordID
			)
into
	#TLoadSymphonyAttendanceExtraField
from
	(
	select
		 AttendanceExtraFieldID = cast(AttendanceExtraFieldID as int)
		,FieldTypeID = cast(FieldTypeID as smallint)
		,FieldID = cast(FieldID as int)
		,AttendanceID = cast(AttendanceID as int)
		,FieldValue = cast(FieldValue as varchar(4000))
		,DataCategoryID = cast(DataCategoryID as smallint)
		,RecordID = cast(RecordID as int)
	from
		(
		select
			 AttendanceExtraFieldID = AttendanceExtraField.atd_EF_extrafieldID
			,FieldTypeID = AttendanceExtraField.atd_EF_FieldType
			,FieldID = AttendanceExtraField.atd_EF_FieldID
			,AttendanceID = AttendanceExtraField.atd_EF_atdid
			,FieldValue = AttendanceExtraField.atd_EF_Value
			,DataCategoryID = AttendanceExtraField.atd_EF_DataCategory
			,RecordID = AttendanceExtraField.atd_EF_RecordID
		from
			[$(Symphony)].dbo.Attendance_Details_ExtraFields AttendanceExtraField

		) Encounter
	) Encounter
order by
	Encounter.AttendanceExtraFieldID

create unique clustered index #IX_TLoadSymphonyAttendanceExtraField on #TLoadSymphonyAttendanceExtraField
	(
	AttendanceExtraFieldID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.AttendanceExtraField target
using
	(
	select
		 AttendanceExtraFieldID
		,FieldTypeID
		,FieldID
		,AttendanceID
		,FieldValue
		,DataCategoryID
		,RecordID
		,EncounterChecksum
	from
		#TLoadSymphonyAttendanceExtraField
	
	) source
	on	source.AttendanceExtraFieldID = target.AttendanceExtraFieldID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 AttendanceExtraFieldID
			,FieldTypeID
			,FieldID
			,AttendanceID
			,FieldValue
			,DataCategoryID
			,RecordID
			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.AttendanceExtraFieldID
			,source.FieldTypeID
			,source.FieldID
			,source.AttendanceID
			,source.FieldValue
			,source.DataCategoryID
			,source.RecordID

			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.AttendanceExtraFieldID = source.AttendanceExtraFieldID
			,target.FieldTypeID = source.FieldTypeID
			,target.FieldID = source.FieldID
			,target.AttendanceID = source.AttendanceID
			,target.FieldValue = source.FieldValue
			,target.DataCategoryID = source.DataCategoryID
			,target.RecordID = source.RecordID
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
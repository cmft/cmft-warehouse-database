﻿

CREATE proc [ETL].[ExtractEnvoyPEXSMSResponse]

 @FromDate smalldatetime = null
, @ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
select @StartTime = getdate()

select @RowsInserted = 0

truncate table 
	ETL.TImportPEXSMSResponse

insert into	
	ETL.TImportPEXSMSResponse

(
	ResponseID
	,DischargeID
	,DischargeDate
	,DischargeTime
	,LocationID
	,SurveyDate
	,SurveyTime
	,SurveyID
	,LastRating
	,LastResponseTime
	,[Status]
	,SentTime
	,QuestionID
	,AnswerID
	,Answer
	,ResponseDate
	,ResponseTime
	,QuestionTypeID
	,ChannelID
	,LocationTypeID
)

select 
	ResponseID
	,DischargeID
	,DischargeDate
	,DischargeTime
	,LocationID
	,SurveyDate = cast(SurveyTime as date)
	,SurveyTime
	,SurveyID
	,LastRating
	,LastResponseTime
	,[Status]
	,SentTime
	,QuestionID
	,AnswerID
	,Answer
	,ResponseDate	
	,ResponseTime
	,QuestionTypeID
	,ChannelID
	,LocationTypeID
from
	(

		select 
			 ResponseID =		cast(Response.ResponseID as int)
			,DischargeID =		cast(Response.[Discharge ID] as int)
			,DischargeDate =	cast(Response.[Discharge Date] as date)
			,DischargeTime =	cast(Response.[Discharge Date] as datetime)
			,LocationID =		cast(Location.locationIdentifier as int)
			,SurveyTime =		cast(Response.[Discharge Date] as datetime) --cast(case when Response.[Contact Initiated] = 'N/A' then null else Response.[Contact Initiated] end as datetime) --Report would alsways be by the teh discharge
			,SurveyID = 1
			,[Last Rating]
			,LastRating =		cast(case when Response.[Last Rating] = 'No Rating' then null else Response.[Last Rating] end as int)
			,LastResponseTime =	cast(case when Response.[Last Response Date] = 'N/A' then null  else Response.[Last Response Date] end as datetime)
			,[Status] =			Response.[Status]
			,SentTime =			cast(case when Response.[Survey Date] = 'N/A' then null else Response.[Survey Date] end  as datetime)
			,QuestionID =		cast(Question.questionIdentifier as int)
			,AnswerID =			cast(case when right(Response.rNo,1) = '1' then (case when Response.[Last Rating] = 'No Rating' then Answer.answerIdentifier else Response.[Last Rating] end) else Answer.answerIdentifier end as bigint)
			,Answer =			case when right(Response.rNo,1) = '1' then (case when Response.[Last Rating] = 'No Rating' then Answer.response else Response.[Last Rating] end) else Answer.response end
			,Response = Answer.response
			,ResponseDate =		cast(case when Response.[Response Date] = 'N/A' then null else Response.[Response Date] end as date)			
			,ResponseTime =		cast(case when Response.[Response Date] = 'N/A' then null else Response.[Response Date] end as datetime)
			,QuestionTypeID =	cast(case when Response.[Survey Date] = 'N/A' then 0 else right(Response.rNo,1)+1 end as int)
			,ChannelID = 2
			,LocationTypeID = 1
		from
			[$(SmallDatasets)].Envoy.SurveysAll Response 
		  
		inner join 
			[$(SmallDatasets)].Envoy.locations Location
		on	Location.Location = Response.Location

		left join 
			[$(SmallDatasets)].Envoy.SurveyQuestions Question
		on	Question.[Survey Question] = Response.[Survey Question]

		left join 
			[$(SmallDatasets)].Envoy.SurveyAnswers Answer
		on	Answer.questionIdentifier = Question.questionIdentifier
		and	Answer.response = Response.Response
		
		where
			Response.[Discharge Date] between @FromDate and @ToDate

	) Response  

select @RowsInserted = @@rowcount

-- Calculate stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	


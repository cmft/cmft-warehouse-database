﻿
-- Use Warehouse
-- execute [ETL].[LoadK2CTGAssessments]

CREATE Procedure [ETL].[LoadK2CTGAssessments]

As
-- Create table to calculate if assessment was carried out within 1hr of previous assessment

Truncate table Maternity.CTGAssessment

Insert into Maternity.CTGAssessment 

select 
	A.SessionNumber
	,A.FetusNumber
	,A.CaseNumber
	,NHSNumber
	,HospitalPatientID
	,Surname
	,OrderID
	,CTGStartDate = cast(CTGStartTime as date)
	,CTGStartTime
	,DateEntered
	,TimeEntered
	,MaternalPulseRecorded
	,DilatationRecorded
	,Dilatation
	,CTGDescriptionID
	,CTGDescription
	,CheckWithinAnHour = case 
							when OrderID = 1 
							then 'Start' 
							else 'x' 
						end
	,CheckWithin30mins = cast('z' as nvarchar(20))
	,BuddySigWithin10mins
	,BuddyDescription
	,BuddyStaffGroup
	,TimeBetweenAssessments = Null
	,TimeBetweenAssessments2ndStage = Null
	,LabourCount = cast(null as int)
	,StaffGroup
	,StaffInitials
from 
	[ETL].[TLoadK2CTGAssessments] A
inner join
	(select
		SessionNumber
		,FetusNumber
		,CTGStartTime = min(TimeEntered)
	from 
		[ETL].[TLoadK2CTGAssessments] 
	group by
		SessionNumber
		,FetusNumber
	) B
on A.SessionNumber = B.SessionNumber
and A.FetusNumber = B.FetusNumber


-- Update if assessment carried out within an hour

While (select distinct CheckWithinAnHour from Maternity.CTGAssessment where CheckWithinAnHour = 'x') = 'x'	
Begin	
	
	Update A	
	set 
		CheckWithinAnHour = case 
								when datediff(mi,C.TimeEntered,A.TimeEntered) between 10 and 70 
								then 'Yes' 
								else 'No' 
							end
		,TimeBetweenAssessments = datediff(mi,C.TimeEntered,A.TimeEntered)
	From 
		Maternity.CTGAssessment A 
	
	inner join 	
		(select 
			SessionNumber
			,FetusNumber
			,[Min] = min(TimeEntered)
		from 
			Maternity.CTGAssessment
		where 
			CheckWithinAnHour = 'x' 
		group by 
			SessionNumber
			,FetusNumber
		) B
	on A.SessionNumber = B.SessionNumber 
	and A.FetusNumber = B.FetusNumber 
	and A.TimeEntered = B.[Min]	
	
	inner join Maternity.CTGAssessment C
	on A.SessionNumber = C.SessionNumber 
	and A.FetusNumber = C.FetusNumber 
	and A.OrderID = (C.OrderID +1)	

End	


-- Update if assessment within 30mins, once patient reached Second Stage
Update A
set CheckWithin30mins = case 
							when B.SessionNumber is null 
							then 'NA'
							when TimeEntered = [Min] 
							then 'Start'
							when TimeEntered < [Min]
							then 'NA'
						else CheckWithin30mins
						end
from 
	Maternity.CTGAssessment A 
left join
	(select 
		SessionNumber
		,FetusNumber
		,[Min] = min(TimeEntered)
	from 
		Maternity.CTGAssessment
	where 
		Dilatation = '10cm+'
	group by 
		SessionNumber
		,FetusNumber
	) B
on A.SessionNumber = B.SessionNumber
and A.FetusNumber = B.FetusNumber


While (select distinct CheckWithin30mins from Maternity.CTGAssessment where CheckWithin30mins = 'z') = 'z'	
Begin	
	
	Update A	
	set 
		CheckWithin30mins = case 
								when datediff(mi,C.TimeEntered,A.TimeEntered) between 10 and 35 
								then 'Yes' 
								else 'No' 
							end
		,TimeBetweenAssessments2ndStage = datediff(mi,C.TimeEntered,A.TimeEntered)
	From 
		Maternity.CTGAssessment A 
	inner join 	
		(select 
			SessionNumber
			,FetusNumber
			,[Min] = min(TimeEntered)
		from 
			Maternity.CTGAssessment
		where 
			CheckWithin30mins = 'z' 
		group by 
			SessionNumber
			,FetusNumber
		) B
	on A.SessionNumber = B.SessionNumber 
		and A.FetusNumber = B.FetusNumber 
		and A.TimeEntered = B.[Min]	
	
	inner join 
		Maternity.CTGAssessment C
	on A.SessionNumber = C.SessionNumber 
		and A.FetusNumber = C.FetusNumber 
		and A.OrderID = (C.OrderID +1)	

End	



Truncate table Maternity.CTGLabour
Insert into Maternity.CTGLabour

Select
	SessionNumber
	,FetusNumber
	,TimeEntered
	,HospitalPatientID
	,OrderNo
	,LabourCount = Case 
						when OrderNo = 1 then 1 
						else 9 end
from
	(select 
		SessionNumber
		,FetusNumber
		,TimeEntered
		,HospitalPatientID
		,OrderNo = row_number() over 
						(partition by HospitalPatientID,Surname 
						order by TimeEntered)
	from 
		Maternity.CTGAssessment
	where 
		OrderID = 1
	and HospitalPatientID is not null
	) Labour


While (select distinct LabourCount from Maternity.CTGLabour where LabourCount = '9') = '9'	
Begin	
	
	Update C	
	set 
		LabourCount = case 
							when datediff(day,A.TimeEntered,C.TimeEntered) between -56 and 56
							then 0
							else 1
							end
	From 
		Maternity.CTGLabour A 
	inner join 	
		(select 
			HospitalPatientID
			,[Min] = min(OrderNo)
		from 
			Maternity.CTGLabour
		where 
			LabourCount = '9' 
		group by 
			HospitalPatientID
		) B
	on A.HospitalPatientID = B.HospitalPatientID 
	and (A.OrderNo+1) = B.[Min]
	
	inner join Maternity.CTGLabour C
	on B.HospitalPatientID = C.HospitalPatientID
	and B.[Min] = C.OrderNo

End	


Update A
set LabourCount = B.LabourCount
from 
	Maternity.CTGAssessment A
inner join Maternity.CTGLabour B
on A.SessionNumber = B.SessionNumber
and A.FetusNumber = B.FetusNumber
and A.OrderID = 1




﻿CREATE Procedure [ETL].[LoadRADEvent] 
(
	 @ImportDateTimeStamp datetime
)

AS

	/******************************************************************************
	**  Name: LoadRADEvent
	**  Purpose: 
	**
	**  Import procedure for Radiology RAD.Event table rows 
	**
	**	Called by LoadRAD
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 16.05.11    MH         Was deleting rows from RAD.Event based on TLoadRADEvent rows that  
	**                        were not part of the current process time stamp
	** 08.06.11    MH         Place a check on the start and end dates range, to prevent
	**                        a corrupt date from deleting rows from the main table
	** 20.03.12	   AP		  Amended writing to AuditLog to overcome situation where there are no unique
	**						  event keys in the last timestamp to be processed
	** 26.06.12    MH         Altered for CMFT environment
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CensusDate smalldatetime
declare @InterfaceCode char(5)
declare @Process varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime
declare @LoadStartDateString varchar(20)
declare @LoadEndDateString varchar(20)

select @Process = 'LoadRADEvent'

select @StartTime = getdate()


select
	 @LoadStartDate = MIN(EventDate) 
	,@LoadEndDate = MAX(EventDate) 
from ETL.TLoadRADEvent
where
	ImportDateTimeStamp = @ImportDateTimeStamp



delete from RAD.Event 
where	
--	Event.EventDate between @LoadStartDate and @LoadEndDate
--or	
	exists (
		select
			1
		from ETL.TLoadRADEvent
		where
			Event.SourceUniqueID = TLoadRADEvent.SourceUniqueID
		AND TLoadRADEvent.ImportDateTimeStamp = @ImportDateTimeStamp
	)

select @RowsDeleted = @@ROWCOUNT


INSERT INTO RAD.Event
(
	 SourceUniqueID
	,RTTPathwayID
	,SourcePatientNo
	,OriginalSourcePatientNo
	,AENumber
	,AttendanceNo
	,AttendedFlag
	,BillReferrerFlag
	,BookingModeCode
	,BookedByCode
	,RequestDate
	,BookedDate
	,EventDate
	,EventTime
	,DateOnWL
	,EventLogicalDeleteFlag
	,IntendedRadiologistCode
	,PatientTypeCode
	,KeyDate
	,NumberOfCancellations
	,PacsVolumeKey
	,ImageNo
	,LeadClinician
	,LmpDate
	,MobilityCode
	,DuringOnCallHoursFlag
	,PatientHeight
	,PatientWeight
	,Practitioner
	,PregnancyEventKey
	,PregnancyFlag
	,ReferralSourceCode
	,ReferrerCode
	,ReferralLocationCode
	,ReferringPCTCode
	,RequestCategoryCode
	,ClinicalHistory
	,Comment
	,EpisodicPCTCode
	,EpisodicPCT
	,SiteCode
	,AttendancesAtSite
	,SpecialtyCode
	,UnreportedFlag
	,UrgencyCode
	,DictatedDate
	,DictatedTime
	,DictatedByCode
	--,DictatedBy
	,DictatedBy2Code
	--,DictatedBy2
	,ConsentFlag
	,ContrastReactionFlag
	,EpisodicReferrerCode
	,EpisodicLocationCode
	,Surname
	,Forenames
	,Title
	,DateOfBirth
	,DateOfDeath
	,DistrictNo
	,NHSNumber
	,NHSNumberStatus
	,NHSTraceStatus
	,RegisteredGpCode
	,RegisteredPracticeCode
	,EthnicOriginCode
	,MRSAIndicator
	,SexCode
	,Address1
	,Address2
	,Address3
	,Address4
	,Postcode
	,PostcodePart1
	,PostcodePart2
	,ResidentialStatusCode
	,PhoneMobile
	,Phone1
	,Phone2
	,OriginatingPasInternalPatientNo1
	,OriginatingPasInternalPatientNo2
	,RestrictedDetailsFlag
	,DiabetesDrugFlag
	,ExamsInRange
	,UsesInhalerFlag
	,PreviousEventDate
	,NumberOfAttendances
	,NumberOfEvents
	,PdsScn
	,PatientDetailsStatus
	,HeartTransplantFlag
	,CurrentLocationArrivalDate
	,EventCreatedDate
	,EventCreatedTime
	,EventModifiedDate
	,EventModifiedTime
	,Created
	,ByWhom
	
	-- MH 05.05.11
	-- New fields included for CRIS update	
	,Abnormal
	,AbnormalActioned
	,AntiD
	,AwaitingAcknowledgement
	,DateReported
	,DayOfWeek
	,HasPathway
	,IsDictated
	,IsProcessed
	,JustifiedBy
	,NumberOfProcs
	,NumberOfScannedDocs
	,Questions
	,Reason
	,RequiredClinician
	,VettingStatus
	,XDSFolderID
	,XDSFolderUID
	,AgeAtEvent
	,ConsentComment
	,PatientCreationDate
	,PatientCreationTime
	,Deleted
	,EmailConsent
	,Language
	,PatientModifiedDate
	,PatientModifiedTime
	,TelMobileConsent
)



SELECT
	 SourceUniqueID
	,RTTPathwayID
	,SourcePatientNo
	,OriginalSourcePatientNo
	,AENumber
	,AttendanceNo
	,AttendedFlag
	,BillReferrerFlag
	,BookingModeCode
	,BookedByCode
	,RequestDate
	,BookedDate
	,EventDate
	,EventTime
	,DateOnWL
	,EventLogicalDeleteFlag
	,IntendedRadiologistCode
	,PatientTypeCode
	,KeyDate
	,NumberOfCancellations
	,PacsVolumeKey
	,ImageNo
	,LeadClinician
	,LmpDate
	,MobilityCode
	,DuringOnCallHoursFlag
	,PatientHeight
	,PatientWeight
	,Practitioner
	,PregnancyEventKey
	,PregnancyFlag
	,ReferralSourceCode
	,ReferrerCode
	,ReferralLocationCode
	,ReferringPCTCode
	,RequestCategoryCode
	,ClinicalHistory
	,Comment
	,EpisodicPCTCode
	,EpisodicPCT
	,SiteCode
	,AttendancesAtSite
	,SpecialtyCode
	,UnreportedFlag
	,UrgencyCode
	,DictatedDate
	,DictatedTime
	,DictatedByCode
	--,DictatedBy
	,DictatedBy2Code
	--,DictatedBy2
	,ConsentFlag
	,ContrastReactionFlag
	,EpisodicReferrerCode
	,EpisodicLocationCode
	,Surname
	,Forenames
	,Title
	,DateOfBirth
	,DateOfDeath
	,DistrictNo
	,NHSNumber
	,NHSNumberStatus
	,NHSTraceStatus
	,RegisteredGpCode
	,RegisteredPracticeCode
	,EthnicOriginCode
	,MRSAIndicator
	,SexCode
	,Address1
	,Address2
	,Address3
	,Address4
	,Postcode
	,PostcodePart1
	,PostcodePart2
	,ResidentialStatusCode
	,PhoneMobile
	,Phone1
	,Phone2
	,OriginatingPasInternalPatientNo1
	,OriginatingPasInternalPatientNo2
	,RestrictedDetailsFlag
	,DiabetesDrugFlag
	,ExamsInRange
	,UsesInhalerFlag
	,PreviousEventDate
	,NumberOfAttendances
	,NumberOfEvents
	,PdsScn
	,PatientDetailsStatus
	,HeartTransplantFlag
	,CurrentLocationArrivalDate
	,EventCreatedDate
	,EventCreatedTime
	,EventModifiedDate
	,EventModifiedTime

	,Created = getdate()
	,ByWhom = system_user
	
	-- MH 05.05.11
	-- New fields included for CRIS update
	,Abnormal
	,AbnormalActioned
	,AntiD
	,AwaitingAcknowledgement
	,DateReported
	,DayOfWeek
	,HasPathway
	,IsDictated
	,IsProcessed
	,JustifiedBy
	,NumberOfProcs
	,NumberOfScannedDocs
	,Questions
	,Reason
	,RequiredClinician
	,VettingStatus
	,XDSFolderID
	,XDSFolderUID
	,AgeAtEvent
	,ConsentComment
	,PatientCreationDate
	,PatientCreationTime
	,Deleted
	,EmailConsent
	,Language
	,PatientModifiedDate
	,PatientModifiedTime
	,TelMobileConsent

FROM
	ETL.TLoadRADEvent
where
	ImportDateTimeStamp = @ImportDateTimeStamp


select @RowsInserted = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

IF (@RowsDeleted is null or @RowsInserted is null or @LoadStartDate is null or @LoadEndDate is null)
	BEGIN
		set @RowsDeleted = 0
		set @RowsInserted = 0
		set @LoadStartDate = '1900-01-01'
		set @LoadEndDate = '1900-01-01'
		set @Stats = 'no valid dates for ' + CONVERT(varchar(11), @ImportDateTimeStamp)
	END
ELSE
	BEGIN
		SELECT @Stats =
			'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
			', Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
			', Net change '  + CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + 
			', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
			CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)
	END

exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

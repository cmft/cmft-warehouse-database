﻿
CREATE PROCEDURE [ETL].LoadAPCUpdate AS

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
SET NOCOUNT OFF;

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select
	@StartTime = getdate()
	
--Identify new and updated Spell data
EXEC ETL.ExtractInquireAPCUpdate
EXEC ETL.LoadAPCUpdateEncounter

--Update Directorates
EXEC ETL.AssignAPCUpdateEncounterStartDirectorateCode
EXEC ETL.AssignAPCUpdateEncounterEndDirectorateCode

--Update ExpectedLOS table with new admits
EXEC ETL.LoadAPCUpdateExpectedLOS


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 	 ', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins' 

EXEC Utility.WriteAuditLogEvent 'LoadAPCUpdate', @Stats, @StartTime


﻿
CREATE procedure [ETL].LoadRenalPatientHemodialysisFlowsheets (@from date = '19000101', @to date = '21000101') as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

declare @MergeSummary TABLE(Action nvarchar(10));

merge
	Renal.PatientHemodialysisFlowsheets target

using
	ETL.TLoadRenalPatientHemodialysisFlowsheets source
on source.SourceUniqueID = target.SourceUniqueID

when
	matched
and source.DatePoint between @from and @to
and not
	(
		isnull(target.[HemodialysisServicesObjectID], 0) = isnull(source.[HemodialysisServicesObjectID], 0)
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[DatePoint], getdate()) = isnull(source.[DatePoint], getdate())
	and isnull(target.[TimingQualifier], '') = isnull(source.[TimingQualifier], '')
	and isnull(target.[StaffObjectID], 0) = isnull(source.[StaffObjectID], 0)
	and isnull(target.[Staff], '') = isnull(source.[Staff], '')
	and isnull(target.[AccessTransonicValue], 0) = isnull(source.[AccessTransonicValue], 0)
	and isnull(target.[Weight], 0) = isnull(source.[Weight], 0)
	and isnull(target.[WeightUnits], '') = isnull(source.[WeightUnits], '')
	and isnull(target.[WeightInKg], 0) = isnull(source.[WeightInKg], 0)
	and isnull(target.[Temperature], 0) = isnull(source.[Temperature], 0)
	and isnull(target.[TemperatureUnits], '') = isnull(source.[TemperatureUnits], '')
	and isnull(target.[TemperatureInCelsius], 0) = isnull(source.[TemperatureInCelsius], 0)
	and isnull(target.[SittingBloodPressureSystolic], 0) = isnull(source.[SittingBloodPressureSystolic], 0)
	and isnull(target.[SittingBloodPressureDiastolic], 0) = isnull(source.[SittingBloodPressureDiastolic], 0)
	and isnull(target.[SittingHeartRate], 0) = isnull(source.[SittingHeartRate], 0)
	and isnull(target.[StandingBloodPressureSystolic], 0) = isnull(source.[StandingBloodPressureSystolic], 0)
	and isnull(target.[StandingBloodPressureDiastolic], 0) = isnull(source.[StandingBloodPressureDiastolic], 0)
	and isnull(target.[StandingHeartRate], 0) = isnull(source.[StandingHeartRate], 0)
	and isnull(target.[Height], 0) = isnull(source.[Height], 0)
	and isnull(target.[HeightUnits], '') = isnull(source.[HeightUnits], '')
	and isnull(target.[HeightInCm], 0) = isnull(source.[HeightInCm], 0)
	and isnull(target.[UltrafiltrationGoal], 0) = isnull(source.[UltrafiltrationGoal], 0)
	and isnull(target.[UltrafiltrationRate], 0) = isnull(source.[UltrafiltrationRate], 0)
	and isnull(target.[UFVRemoved], 0) = isnull(source.[UFVRemoved], 0)
	and isnull(target.[BloodVolumeProcessed], 0) = isnull(source.[BloodVolumeProcessed], 0)
	and isnull(target.[BloodFlowRate], 0) = isnull(source.[BloodFlowRate], 0)
	and isnull(target.[DialysateFlowRate], 0) = isnull(source.[DialysateFlowRate], 0)
	and isnull(target.[ArtP], 0) = isnull(source.[ArtP], 0)
	and isnull(target.[VenP], 0) = isnull(source.[VenP], 0)
	and isnull(target.[TransmembranePressure], 0) = isnull(source.[TransmembranePressure], 0)
	and isnull(target.[Conductivity], 0) = isnull(source.[Conductivity], 0)
	and isnull(target.[AlarmState], '') = isnull(source.[AlarmState], '')
	and isnull(target.[MachineMode], '') = isnull(source.[MachineMode], '')
	and isnull(target.[MachineTemperature], '') = isnull(source.[MachineTemperature], '')
	and isnull(target.[Notes], '') = isnull(source.[Notes], '')
	)

then
	update
	set
		 target.[HemodialysisServicesObjectID] = source.[HemodialysisServicesObjectID]
		,target.[PatientObjectID] = source.[PatientObjectID]
		,target.[PatientFullName] = source.[PatientFullName]
		,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
		,target.[DatePoint] = source.[DatePoint]
		,target.[TimingQualifier] = source.[TimingQualifier]
		,target.[StaffObjectID] = source.[StaffObjectID]
		,target.[Staff] = source.[Staff]
		,target.[AccessTransonicValue] = source.[AccessTransonicValue]
		,target.[Weight] = source.[Weight]
		,target.[WeightUnits] = source.[WeightUnits]
		,target.[WeightInKg] = source.[WeightInKg]
		,target.[Temperature] = source.[Temperature]
		,target.[TemperatureUnits] = source.[TemperatureUnits]
		,target.[TemperatureInCelsius] = source.[TemperatureInCelsius]
		,target.[SittingBloodPressureSystolic] = source.[SittingBloodPressureSystolic]
		,target.[SittingBloodPressureDiastolic] = source.[SittingBloodPressureDiastolic]
		,target.[SittingHeartRate] = source.[SittingHeartRate]
		,target.[StandingBloodPressureSystolic] = source.[StandingBloodPressureSystolic]
		,target.[StandingBloodPressureDiastolic] = source.[StandingBloodPressureDiastolic]
		,target.[StandingHeartRate] = source.[StandingHeartRate]
		,target.[Height] = source.[Height]
		,target.[HeightUnits] = source.[HeightUnits]
		,target.[HeightInCm] = source.[HeightInCm]
		,target.[UltrafiltrationGoal] = source.[UltrafiltrationGoal]
		,target.[UltrafiltrationRate] = source.[UltrafiltrationRate]
		,target.[UFVRemoved] = source.[UFVRemoved]
		,target.[BloodVolumeProcessed] = source.[BloodVolumeProcessed]
		,target.[BloodFlowRate] = source.[BloodFlowRate]
		,target.[DialysateFlowRate] = source.[DialysateFlowRate]
		,target.[ArtP] = source.[ArtP]
		,target.[VenP] = source.[VenP]
		,target.[TransmembranePressure] = source.[TransmembranePressure]
		,target.[Conductivity] = source.[Conductivity]
		,target.[AlarmState] = source.[AlarmState]
		,target.[MachineMode] = source.[MachineMode]
		,target.[MachineTemperature] = source.[MachineTemperature]
		,target.[Notes] = source.[Notes]
		,target.Updated = getdate()
		,target.ByWhom = system_user

when
	not matched by source
then
	delete

when
	not matched by target
then
	insert
		(
		 SourceUniqueID
		,[HemodialysisServicesObjectID]
		,[PatientObjectID]
		,[PatientFullName]
		,[PatientMedicalRecordNo]
		,[DatePoint]
		,[TimingQualifier]
		,[StaffObjectID]
		,[Staff]
		,[AccessTransonicValue]
		,[Weight]
		,[WeightUnits]
		,[WeightInKg]
		,[Temperature]
		,[TemperatureUnits]
		,[TemperatureInCelsius]
		,[SittingBloodPressureSystolic]
		,[SittingBloodPressureDiastolic]
		,[SittingHeartRate]
		,[StandingBloodPressureSystolic]
		,[StandingBloodPressureDiastolic]
		,[StandingHeartRate]
		,[Height]
		,[HeightUnits]
		,[HeightInCm]
		,[UltrafiltrationGoal]
		,[UltrafiltrationRate]
		,[UFVRemoved]
		,[BloodVolumeProcessed]
		,[BloodFlowRate]
		,[DialysateFlowRate]
		,[ArtP]
		,[VenP]
		,[TransmembranePressure]
		,[Conductivity]
		,[AlarmState]
		,[MachineMode]
		,[MachineTemperature]
		,[Notes]
		,Created
		,Updated
		,ByWhom
		)

	values
		(
		 source.SourceUniqueID
		,source.[HemodialysisServicesObjectID]
		,source.[PatientObjectID]
		,source.[PatientFullName]
		,source.[PatientMedicalRecordNo]
		,source.[DatePoint]
		,source.[TimingQualifier]
		,source.[StaffObjectID]
		,source.[Staff]
		,source.[AccessTransonicValue]
		,source.[Weight]
		,source.[WeightUnits]
		,source.[WeightInKg]
		,source.[Temperature]
		,source.[TemperatureUnits]
		,source.[TemperatureInCelsius]
		,source.[SittingBloodPressureSystolic]
		,source.[SittingBloodPressureDiastolic]
		,source.[SittingHeartRate]
		,source.[StandingBloodPressureSystolic]
		,source.[StandingBloodPressureDiastolic]
		,source.[StandingHeartRate]
		,source.[Height]
		,source.[HeightUnits]
		,source.[HeightInCm]
		,source.[UltrafiltrationGoal]
		,source.[UltrafiltrationRate]
		,source.[UFVRemoved]
		,source.[BloodVolumeProcessed]
		,source.[BloodFlowRate]
		,source.[DialysateFlowRate]
		,source.[ArtP]
		,source.[VenP]
		,source.[TransmembranePressure]
		,source.[Conductivity]
		,source.[AlarmState]
		,source.[MachineMode]
		,source.[MachineTemperature]
		,source.[Notes]
		,getdate()
		,getdate()
		,system_user
		)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats


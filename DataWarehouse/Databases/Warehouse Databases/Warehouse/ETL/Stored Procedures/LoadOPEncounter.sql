﻿

CREATE procedure [ETL].[LoadOPEncounter]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


select
	*
	,EncounterChecksum =
	checksum(
		 SourcePatientNo
		,SourceEncounterNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,NHSNumber
		,DistrictNo
		,Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,EthnicOriginCode
		,MaritalStatusCode
		,ReligionCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		--,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode
		,ReferringConsultantCode
		,ReferringSpecialtyCode
		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryOperationDate
		,SecondaryOperationCode1
		,SecondaryOperationDate1
		,SecondaryOperationCode2
		,SecondaryOperationDate2
		,SecondaryOperationCode3
		,SecondaryOperationDate3
		,SecondaryOperationCode4
		,SecondaryOperationDate4
		,SecondaryOperationCode5
		,SecondaryOperationDate5
		,SecondaryOperationCode6
		,SecondaryOperationDate6
		,SecondaryOperationCode7
		,SecondaryOperationDate7
		,SecondaryOperationCode8
		,SecondaryOperationDate8
		,SecondaryOperationCode9
		,SecondaryOperationDate9
		,SecondaryOperationCode10
		,SecondaryOperationDate10
		,SecondaryOperationCode11
		,SecondaryOperationDate11
		,PurchaserCode
		,ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,LocalAdminCategoryCode
		,PCTCode
		,LocalityCode
		,IsWardAttender
		--,ClockStartDate
		--,RTTBreachDate
		,HomePhone
		,WorkPhone
		--,SourceOfReferralGroupCode
		,WardCode
		,ReferredByCode
		,ReferrerCode
		--,LastDNAorPatientCancelledDate
		--,DirectorateCode
		,StaffGroupCode
		,InterpreterIndicator
		,EpisodicSiteCode
		,PseudoPostcode
		,CCGCode
		,PostcodeAtAppointment
		,GpPracticeCodeAtAppointment
		,GpCodeAtAppointment
	)
into
	#TLoadOPEncounter
from
	(
	select
		 SourceUniqueID = cast(SourceUniqueID as varchar(50))
		,SourcePatientNo = cast(SourcePatientNo as varchar(20))
		,SourceEncounterNo = cast(SourceEncounterNo as varchar(20))
		,PatientTitle = cast(PatientTitle as varchar(10))
		,PatientForename = cast(PatientForename as varchar(20))
		,PatientSurname = cast(PatientSurname as varchar(30))
		,DateOfBirth = cast(DateOfBirth as datetime)
		,DateOfDeath = cast(DateOfDeath as smalldatetime)
		,SexCode = cast(SexCode as varchar(1))
		,NHSNumber = cast(NHSNumber as varchar(17))
		,DistrictNo = cast(DistrictNo as varchar(12))
		,Postcode = cast(Postcode as varchar(8))
		,PatientAddress1 = cast(PatientAddress1 as varchar(25))
		,PatientAddress2 = cast(PatientAddress2 as varchar(25))
		,PatientAddress3 = cast(PatientAddress3 as varchar(25))
		,PatientAddress4 = cast(PatientAddress4 as varchar(25))
		,DHACode = cast(DHACode as varchar(3))
		,EthnicOriginCode = cast(EthnicOriginCode as varchar(4))
		,MaritalStatusCode = cast(MaritalStatusCode as varchar(1))
		,ReligionCode = cast(ReligionCode as varchar(4))
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(8))
		,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(8))
		,SiteCode = cast(SiteCode as varchar(5))
		,AppointmentDate = cast(AppointmentDate as smalldatetime)
		,AppointmentTime = cast(AppointmentTime as smalldatetime)
		,ClinicCode = cast(ClinicCode as varchar(8))
		,AdminCategoryCode = cast(AdminCategoryCode as varchar(3))
		,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(5))
		,ReasonForReferralCode = cast(ReasonForReferralCode as varchar(5))
		,PriorityCode = cast(PriorityCode as varchar(5))
		,FirstAttendanceFlag = cast(FirstAttendanceFlag as varchar(1))
		,DNACode = cast(DNACode as char(1))
		,AppointmentStatusCode = cast(AppointmentStatusCode as varchar(10))
		,CancelledByCode = cast(CancelledByCode as varchar(5))
		,TransportRequiredFlag = cast(TransportRequiredFlag as varchar(3))
		,AttendanceOutcomeCode = cast(AttendanceOutcomeCode as varchar(5))
		,AppointmentTypeCode = cast(AppointmentTypeCode as varchar(5))
		,DisposalCode = cast(DisposalCode as varchar(8))
		,ConsultantCode = cast(ConsultantCode as varchar(10))
		,SpecialtyCode = cast(SpecialtyCode as varchar(5))
		,ReferringConsultantCode = cast(ReferringConsultantCode as varchar(10))
		,ReferringSpecialtyCode = cast(ReferringSpecialtyCode as varchar(8))
		,BookingTypeCode = cast(BookingTypeCode as varchar(3))
		,CasenoteNo = cast(CasenoteNo as varchar(16))
		,AppointmentCreateDate = cast(AppointmentCreateDate as smalldatetime)
		,EpisodicGpCode = cast(EpisodicGpCode as varchar(8))
		,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar(6))
		,DoctorCode = cast(DoctorCode as varchar(20))
		,PrimaryDiagnosisCode = cast(PrimaryDiagnosisCode as varchar(10))
		,SubsidiaryDiagnosisCode = cast(SubsidiaryDiagnosisCode as varchar(10))
		,SecondaryDiagnosisCode1 = cast(SecondaryDiagnosisCode1 as varchar(10))
		,SecondaryDiagnosisCode2 = cast(SecondaryDiagnosisCode2 as varchar(10))
		,SecondaryDiagnosisCode3 = cast(SecondaryDiagnosisCode3 as varchar(10))
		,SecondaryDiagnosisCode4 = cast(SecondaryDiagnosisCode4 as varchar(10))
		,SecondaryDiagnosisCode5 = cast(SecondaryDiagnosisCode5 as varchar(10))
		,SecondaryDiagnosisCode6 = cast(SecondaryDiagnosisCode6 as varchar(10))
		,SecondaryDiagnosisCode7 = cast(SecondaryDiagnosisCode7 as varchar(10))
		,SecondaryDiagnosisCode8 = cast(SecondaryDiagnosisCode8 as varchar(10))
		,SecondaryDiagnosisCode9 = cast(SecondaryDiagnosisCode9 as varchar(10))
		,SecondaryDiagnosisCode10 = cast(SecondaryDiagnosisCode10 as varchar(10))
		,SecondaryDiagnosisCode11 = cast(SecondaryDiagnosisCode11 as varchar(10))
		,SecondaryDiagnosisCode12 = cast(SecondaryDiagnosisCode12 as varchar(10))
		,PrimaryOperationCode = cast(PrimaryOperationCode as varchar(10))
		,PrimaryOperationDate = cast(PrimaryOperationDate as smalldatetime)
		,SecondaryOperationCode1 = cast(SecondaryOperationCode1 as varchar(10))
		,SecondaryOperationDate1 = cast(SecondaryOperationDate1 as smalldatetime)
		,SecondaryOperationCode2 = cast(SecondaryOperationCode2 as varchar(10))
		,SecondaryOperationDate2 = cast(SecondaryOperationDate2 as smalldatetime)
		,SecondaryOperationCode3 = cast(SecondaryOperationCode3 as varchar(10))
		,SecondaryOperationDate3 = cast(SecondaryOperationDate3 as smalldatetime)
		,SecondaryOperationCode4 = cast(SecondaryOperationCode4 as varchar(10))
		,SecondaryOperationDate4 = cast(SecondaryOperationDate4 as smalldatetime)
		,SecondaryOperationCode5 = cast(SecondaryOperationCode5 as varchar(10))
		,SecondaryOperationDate5 = cast(SecondaryOperationDate5 as smalldatetime)
		,SecondaryOperationCode6 = cast(SecondaryOperationCode6 as varchar(10))
		,SecondaryOperationDate6 = cast(SecondaryOperationDate6 as smalldatetime)
		,SecondaryOperationCode7 = cast(SecondaryOperationCode7 as varchar(10))
		,SecondaryOperationDate7 = cast(SecondaryOperationDate7 as smalldatetime)
		,SecondaryOperationCode8 = cast(SecondaryOperationCode8 as varchar(10))
		,SecondaryOperationDate8 = cast(SecondaryOperationDate8 as smalldatetime)
		,SecondaryOperationCode9 = cast(SecondaryOperationCode9 as varchar(10))
		,SecondaryOperationDate9 = cast(SecondaryOperationDate9 as smalldatetime)
		,SecondaryOperationCode10 = cast(SecondaryOperationCode10 as varchar(10))
		,SecondaryOperationDate10 = cast(SecondaryOperationDate10 as smalldatetime)
		,SecondaryOperationCode11 = cast(SecondaryOperationCode11 as varchar(10))
		,SecondaryOperationDate11 = cast(SecondaryOperationDate11 as smalldatetime)
		,PurchaserCode = cast(PurchaserCode as varchar(10))
		,ProviderCode = cast(ProviderCode as varchar(5))
		,ContractSerialNo = cast(ContractSerialNo as varchar(6))
		,ReferralDate = cast(ReferralDate as smalldatetime)
		,RTTPathwayID = cast(RTTPathwayID as varchar(25))
		,RTTPathwayCondition = cast(RTTPathwayCondition as varchar(20))
		,RTTStartDate = cast(RTTStartDate as smalldatetime)
		,RTTEndDate = cast(RTTEndDate as smalldatetime)
		,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar(10))
		,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar(10))
		,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar(10))
		,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
		,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
		,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as bit)
		,RTTPeriodStatusCode = cast(RTTPeriodStatusCode as varchar(10))
		,AppointmentCategoryCode = cast(AppointmentCategoryCode as varchar(10))
		,AppointmentCreatedBy = cast(AppointmentCreatedBy as varchar(3))
		,AppointmentCancelDate = cast(AppointmentCancelDate as smalldatetime)
		,LastRevisedDate = cast(LastRevisedDate as smalldatetime)
		,LastRevisedBy = cast(LastRevisedBy as varchar(3))
		,OverseasStatusFlag = cast(OverseasStatusFlag as varchar(1))
		,PatientChoiceCode = cast(PatientChoiceCode as varchar(3))
		,ScheduledCancelReasonCode = cast(ScheduledCancelReasonCode as varchar(4))
		,PatientCancelReason = cast(PatientCancelReason as varchar(50))
		,DischargeDate = cast(DischargeDate as smalldatetime)
		,QM08StartWaitDate = cast(QM08StartWaitDate as smalldatetime)
		,QM08EndWaitDate = cast(QM08EndWaitDate as smalldatetime)
		,DestinationSiteCode = cast(DestinationSiteCode as varchar(10))
		,EBookingReferenceNo = cast(EBookingReferenceNo as varchar(50))
		,InterfaceCode = cast(InterfaceCode as varchar(5))
		,LocalAdminCategoryCode = cast(LocalAdminCategoryCode as varchar(10))
		,PCTCode = cast(PCTCode as varchar(10))
		,LocalityCode = cast(LocalityCode as varchar(10))
		,IsWardAttender = cast(IsWardAttender as bit)
		,ClockStartDate = cast(ClockStartDate as smalldatetime)
		,RTTBreachDate = cast(RTTBreachDate as smalldatetime)
		,HomePhone = cast(HomePhone as varchar(50))
		,WorkPhone = cast(WorkPhone as varchar(50))
		,SourceOfReferralGroupCode = cast(SourceOfReferralGroupCode as varchar(10))
		,WardCode = cast(WardCode as varchar(10))
		,ReferredByCode = cast(ReferredByCode as varchar(10))
		,ReferrerCode = cast(ReferrerCode as varchar(10))
		,LastDNAorPatientCancelledDate = cast(LastDNAorPatientCancelledDate as smalldatetime)
		,DirectorateCode = cast(DirectorateCode as varchar(5))
		,StaffGroupCode = cast(StaffGroupCode as varchar(10))
		,InterpreterIndicator = cast(InterpreterIndicator as varchar(3))
		,EpisodicSiteCode = cast(EpisodicSiteCode as varchar(5))
		,PseudoPostcode = cast(PseudoPostcode as varchar(8))
		,CCGCode = cast(CCGCode as varchar (10))
		,PostcodeAtAppointment = cast(PostcodeAtAppointment as varchar (8))
		,GpPracticeCodeAtAppointment = cast(GpPracticeCodeAtAppointment as varchar (6))
		,GpCodeAtAppointment = cast(GpCodeAtAppointment as varchar (8))
	from
		ETL.TLoadOPEncounter
	) Activity


CREATE UNIQUE CLUSTERED INDEX IX_TLoadOPEncounter ON #TLoadOPEncounter
	(
	SourceUniqueID
	)
	--include
	--(
	--EncounterChecksum
	--)
WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(AppointmentDate)
	,@LoadEndDate = MAX(AppointmentDate)
from
	#TLoadOPEncounter

declare
	 @deleted int
	,@inserted int
	,@updated int




merge
	OP.Encounter target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,SourceEncounterNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,NHSNumber
		,DistrictNo
		,Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,EthnicOriginCode
		,MaritalStatusCode
		,ReligionCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode
		,ReferringConsultantCode
		,ReferringSpecialtyCode
		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryOperationDate
		,SecondaryOperationCode1
		,SecondaryOperationDate1
		,SecondaryOperationCode2
		,SecondaryOperationDate2
		,SecondaryOperationCode3
		,SecondaryOperationDate3
		,SecondaryOperationCode4
		,SecondaryOperationDate4
		,SecondaryOperationCode5
		,SecondaryOperationDate5
		,SecondaryOperationCode6
		,SecondaryOperationDate6
		,SecondaryOperationCode7
		,SecondaryOperationDate7
		,SecondaryOperationCode8
		,SecondaryOperationDate8
		,SecondaryOperationCode9
		,SecondaryOperationDate9
		,SecondaryOperationCode10
		,SecondaryOperationDate10
		,SecondaryOperationCode11
		,SecondaryOperationDate11
		,PurchaserCode
		,ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,LocalAdminCategoryCode
		,PCTCode
		,LocalityCode
		,IsWardAttender
		,ClockStartDate
		,RTTBreachDate
		,HomePhone
		,WorkPhone
		,SourceOfReferralGroupCode
		,WardCode
		,ReferredByCode
		,ReferrerCode
		,LastDNAorPatientCancelledDate
		,DirectorateCode
		,StaffGroupCode
		,InterpreterIndicator
		,EpisodicSiteCode
		,PseudoPostcode
		,CCGCode
		,PostcodeAtAppointment
		,GpPracticeCodeAtAppointment
		,GpCodeAtAppointment
		,EncounterChecksum
	from
		#TLoadOPEncounter
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.AppointmentDate between @LoadStartDate and @LoadEndDate
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,PatientTitle
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,NHSNumber
			,DistrictNo
			,Postcode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,DHACode
			,EthnicOriginCode
			,MaritalStatusCode
			,ReligionCode
			,RegisteredGpCode
			,RegisteredGpPracticeCode
			,SiteCode
			,AppointmentDate
			,AppointmentTime
			,ClinicCode
			,AdminCategoryCode
			,SourceOfReferralCode
			,ReasonForReferralCode
			,PriorityCode
			,FirstAttendanceFlag
			,DNACode
			,AppointmentStatusCode
			,CancelledByCode
			,TransportRequiredFlag
			,AttendanceOutcomeCode
			,AppointmentTypeCode
			,DisposalCode
			,ConsultantCode
			,SpecialtyCode
			,ReferringConsultantCode
			,ReferringSpecialtyCode
			,BookingTypeCode
			,CasenoteNo
			,AppointmentCreateDate
			,EpisodicGpCode
			,EpisodicGpPracticeCode
			,DoctorCode
			,PrimaryDiagnosisCode
			,SubsidiaryDiagnosisCode
			,SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			,PrimaryOperationCode
			,PrimaryOperationDate
			,SecondaryOperationCode1
			,SecondaryOperationDate1
			,SecondaryOperationCode2
			,SecondaryOperationDate2
			,SecondaryOperationCode3
			,SecondaryOperationDate3
			,SecondaryOperationCode4
			,SecondaryOperationDate4
			,SecondaryOperationCode5
			,SecondaryOperationDate5
			,SecondaryOperationCode6
			,SecondaryOperationDate6
			,SecondaryOperationCode7
			,SecondaryOperationDate7
			,SecondaryOperationCode8
			,SecondaryOperationDate8
			,SecondaryOperationCode9
			,SecondaryOperationDate9
			,SecondaryOperationCode10
			,SecondaryOperationDate10
			,SecondaryOperationCode11
			,SecondaryOperationDate11
			,PurchaserCode
			,ProviderCode
			,ContractSerialNo
			,ReferralDate
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,RTTPeriodStatusCode
			,AppointmentCategoryCode
			,AppointmentCreatedBy
			,AppointmentCancelDate
			,LastRevisedDate
			,LastRevisedBy
			,OverseasStatusFlag
			,PatientChoiceCode
			,ScheduledCancelReasonCode
			,PatientCancelReason
			,DischargeDate
			,QM08StartWaitDate
			,QM08EndWaitDate
			,DestinationSiteCode
			,EBookingReferenceNo
			,InterfaceCode
			,LocalAdminCategoryCode
			,PCTCode
			,LocalityCode
			,IsWardAttender
			,ClockStartDate
			,RTTBreachDate
			,HomePhone
			,WorkPhone
			,SourceOfReferralGroupCode
			,WardCode
			,ReferredByCode
			,ReferrerCode
			,LastDNAorPatientCancelledDate
			,DirectorateCode
			,StaffGroupCode
			,InterpreterIndicator
			,EpisodicSiteCode
			,PseudoPostcode
			,CCGCode
			,PostcodeAtAppointment
			,GpPracticeCodeAtAppointment
			,GpCodeAtAppointment
			,EncounterChecksum
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceEncounterNo
			,source.PatientTitle
			,source.PatientForename
			,source.PatientSurname
			,source.DateOfBirth
			,source.DateOfDeath
			,source.SexCode
			,source.NHSNumber
			,source.DistrictNo
			,source.Postcode
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.DHACode
			,source.EthnicOriginCode
			,source.MaritalStatusCode
			,source.ReligionCode
			,source.RegisteredGpCode
			,source.RegisteredGpPracticeCode
			,source.SiteCode
			,source.AppointmentDate
			,source.AppointmentTime
			,source.ClinicCode
			,source.AdminCategoryCode
			,source.SourceOfReferralCode
			,source.ReasonForReferralCode
			,source.PriorityCode
			,source.FirstAttendanceFlag
			,source.DNACode
			,source.AppointmentStatusCode
			,source.CancelledByCode
			,source.TransportRequiredFlag
			,source.AttendanceOutcomeCode
			,source.AppointmentTypeCode
			,source.DisposalCode
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.ReferringConsultantCode
			,source.ReferringSpecialtyCode
			,source.BookingTypeCode
			,source.CasenoteNo
			,source.AppointmentCreateDate
			,source.EpisodicGpCode
			,source.EpisodicGpPracticeCode
			,source.DoctorCode
			,source.PrimaryDiagnosisCode
			,source.SubsidiaryDiagnosisCode
			,source.SecondaryDiagnosisCode1
			,source.SecondaryDiagnosisCode2
			,source.SecondaryDiagnosisCode3
			,source.SecondaryDiagnosisCode4
			,source.SecondaryDiagnosisCode5
			,source.SecondaryDiagnosisCode6
			,source.SecondaryDiagnosisCode7
			,source.SecondaryDiagnosisCode8
			,source.SecondaryDiagnosisCode9
			,source.SecondaryDiagnosisCode10
			,source.SecondaryDiagnosisCode11
			,source.SecondaryDiagnosisCode12
			,source.PrimaryOperationCode
			,source.PrimaryOperationDate
			,source.SecondaryOperationCode1
			,source.SecondaryOperationDate1
			,source.SecondaryOperationCode2
			,source.SecondaryOperationDate2
			,source.SecondaryOperationCode3
			,source.SecondaryOperationDate3
			,source.SecondaryOperationCode4
			,source.SecondaryOperationDate4
			,source.SecondaryOperationCode5
			,source.SecondaryOperationDate5
			,source.SecondaryOperationCode6
			,source.SecondaryOperationDate6
			,source.SecondaryOperationCode7
			,source.SecondaryOperationDate7
			,source.SecondaryOperationCode8
			,source.SecondaryOperationDate8
			,source.SecondaryOperationCode9
			,source.SecondaryOperationDate9
			,source.SecondaryOperationCode10
			,source.SecondaryOperationDate10
			,source.SecondaryOperationCode11
			,source.SecondaryOperationDate11
			,source.PurchaserCode
			,source.ProviderCode
			,source.ContractSerialNo
			,source.ReferralDate
			,source.RTTPathwayID
			,source.RTTPathwayCondition
			,source.RTTStartDate
			,source.RTTEndDate
			,source.RTTSpecialtyCode
			,source.RTTCurrentProviderCode
			,source.RTTCurrentStatusCode
			,source.RTTCurrentStatusDate
			,source.RTTCurrentPrivatePatientFlag
			,source.RTTOverseasStatusFlag
			,source.RTTPeriodStatusCode
			,source.AppointmentCategoryCode
			,source.AppointmentCreatedBy
			,source.AppointmentCancelDate
			,source.LastRevisedDate
			,source.LastRevisedBy
			,source.OverseasStatusFlag
			,source.PatientChoiceCode
			,source.ScheduledCancelReasonCode
			,source.PatientCancelReason
			,source.DischargeDate
			,source.QM08StartWaitDate
			,source.QM08EndWaitDate
			,source.DestinationSiteCode
			,source.EBookingReferenceNo
			,source.InterfaceCode
			,source.LocalAdminCategoryCode
			,source.PCTCode
			,source.LocalityCode
			,source.IsWardAttender
			,source.ClockStartDate
			,source.RTTBreachDate
			,source.HomePhone
			,source.WorkPhone
			,source.SourceOfReferralGroupCode
			,source.WardCode
			,source.ReferredByCode
			,source.ReferrerCode
			,source.LastDNAorPatientCancelledDate
			,source.DirectorateCode
			,source.StaffGroupCode
			,source.InterpreterIndicator
			,source.EpisodicSiteCode
			,source.PseudoPostcode
			,source.CCGCode 
			,source.PostcodeAtAppointment 
			,source.GpPracticeCodeAtAppointment
			,source.GpCodeAtAppointment
			,source.EncounterChecksum
			,getdate()
			,getdate()
			,system_user
			)

	when matched
	and	target.EncounterChecksum <> source.EncounterChecksum

	then
		update
		set
			 target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.NHSNumber = source.NHSNumber
			,target.DistrictNo = source.DistrictNo
			,target.Postcode = source.Postcode
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.DHACode = source.DHACode
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.ReligionCode = source.ReligionCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.SiteCode = source.SiteCode
			,target.AppointmentDate = source.AppointmentDate
			,target.AppointmentTime = source.AppointmentTime
			,target.ClinicCode = source.ClinicCode
			,target.AdminCategoryCode = source.AdminCategoryCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ReasonForReferralCode = source.ReasonForReferralCode
			,target.PriorityCode = source.PriorityCode
			,target.FirstAttendanceFlag = source.FirstAttendanceFlag
			,target.DNACode = source.DNACode
			,target.AppointmentStatusCode = source.AppointmentStatusCode
			,target.CancelledByCode = source.CancelledByCode
			,target.TransportRequiredFlag = source.TransportRequiredFlag
			,target.AttendanceOutcomeCode = source.AttendanceOutcomeCode
			,target.AppointmentTypeCode = source.AppointmentTypeCode
			,target.DisposalCode = source.DisposalCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ReferringConsultantCode = source.ReferringConsultantCode
			,target.ReferringSpecialtyCode = source.ReferringSpecialtyCode
			,target.BookingTypeCode = source.BookingTypeCode
			,target.CasenoteNo = source.CasenoteNo
			,target.AppointmentCreateDate = source.AppointmentCreateDate
			,target.EpisodicGpCode = source.EpisodicGpCode
			,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
			,target.DoctorCode = source.DoctorCode
			,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
			,target.SubsidiaryDiagnosisCode = source.SubsidiaryDiagnosisCode
			,target.SecondaryDiagnosisCode1 = source.SecondaryDiagnosisCode1
			,target.SecondaryDiagnosisCode2 = source.SecondaryDiagnosisCode2
			,target.SecondaryDiagnosisCode3 = source.SecondaryDiagnosisCode3
			,target.SecondaryDiagnosisCode4 = source.SecondaryDiagnosisCode4
			,target.SecondaryDiagnosisCode5 = source.SecondaryDiagnosisCode5
			,target.SecondaryDiagnosisCode6 = source.SecondaryDiagnosisCode6
			,target.SecondaryDiagnosisCode7 = source.SecondaryDiagnosisCode7
			,target.SecondaryDiagnosisCode8 = source.SecondaryDiagnosisCode8
			,target.SecondaryDiagnosisCode9 = source.SecondaryDiagnosisCode9
			,target.SecondaryDiagnosisCode10 = source.SecondaryDiagnosisCode10
			,target.SecondaryDiagnosisCode11 = source.SecondaryDiagnosisCode11
			,target.SecondaryDiagnosisCode12 = source.SecondaryDiagnosisCode12
			,target.PrimaryOperationCode = source.PrimaryOperationCode
			,target.PrimaryOperationDate = source.PrimaryOperationDate
			,target.SecondaryOperationCode1 = source.SecondaryOperationCode1
			,target.SecondaryOperationDate1 = source.SecondaryOperationDate1
			,target.SecondaryOperationCode2 = source.SecondaryOperationCode2
			,target.SecondaryOperationDate2 = source.SecondaryOperationDate2
			,target.SecondaryOperationCode3 = source.SecondaryOperationCode3
			,target.SecondaryOperationDate3 = source.SecondaryOperationDate3
			,target.SecondaryOperationCode4 = source.SecondaryOperationCode4
			,target.SecondaryOperationDate4 = source.SecondaryOperationDate4
			,target.SecondaryOperationCode5 = source.SecondaryOperationCode5
			,target.SecondaryOperationDate5 = source.SecondaryOperationDate5
			,target.SecondaryOperationCode6 = source.SecondaryOperationCode6
			,target.SecondaryOperationDate6 = source.SecondaryOperationDate6
			,target.SecondaryOperationCode7 = source.SecondaryOperationCode7
			,target.SecondaryOperationDate7 = source.SecondaryOperationDate7
			,target.SecondaryOperationCode8 = source.SecondaryOperationCode8
			,target.SecondaryOperationDate8 = source.SecondaryOperationDate8
			,target.SecondaryOperationCode9 = source.SecondaryOperationCode9
			,target.SecondaryOperationDate9 = source.SecondaryOperationDate9
			,target.SecondaryOperationCode10 = source.SecondaryOperationCode10
			,target.SecondaryOperationDate10 = source.SecondaryOperationDate10
			,target.SecondaryOperationCode11 = source.SecondaryOperationCode11
			,target.SecondaryOperationDate11 = source.SecondaryOperationDate11
			,target.PurchaserCode = source.PurchaserCode
			,target.ProviderCode = source.ProviderCode
			,target.ContractSerialNo = source.ContractSerialNo
			,target.ReferralDate = source.ReferralDate
			,target.RTTPathwayID = source.RTTPathwayID
			,target.RTTPathwayCondition = source.RTTPathwayCondition
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTSpecialtyCode = source.RTTSpecialtyCode
			,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
			,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
			,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
			,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
			,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
			,target.RTTPeriodStatusCode = source.RTTPeriodStatusCode
			,target.AppointmentCategoryCode = source.AppointmentCategoryCode
			,target.AppointmentCreatedBy = source.AppointmentCreatedBy
			,target.AppointmentCancelDate = source.AppointmentCancelDate
			,target.LastRevisedDate = source.LastRevisedDate
			,target.LastRevisedBy = source.LastRevisedBy
			,target.OverseasStatusFlag = source.OverseasStatusFlag
			,target.PatientChoiceCode = source.PatientChoiceCode
			,target.ScheduledCancelReasonCode = source.ScheduledCancelReasonCode
			,target.PatientCancelReason = source.PatientCancelReason
			,target.DischargeDate = source.DischargeDate
			,target.QM08StartWaitDate = source.QM08StartWaitDate
			,target.QM08EndWaitDate = source.QM08EndWaitDate
			,target.DestinationSiteCode = source.DestinationSiteCode
			,target.EBookingReferenceNo = source.EBookingReferenceNo
			,target.InterfaceCode = source.InterfaceCode
			,target.LocalAdminCategoryCode = source.LocalAdminCategoryCode
			,target.PCTCode = source.PCTCode
			,target.LocalityCode = source.LocalityCode
			,target.IsWardAttender = source.IsWardAttender
			,target.ClockStartDate = source.ClockStartDate
			,target.RTTBreachDate = source.RTTBreachDate
			,target.HomePhone = source.HomePhone
			,target.WorkPhone = source.WorkPhone
			,target.SourceOfReferralGroupCode = source.SourceOfReferralGroupCode
			,target.WardCode = source.WardCode
			,target.ReferredByCode = source.ReferredByCode
			,target.ReferrerCode = source.ReferrerCode
			,target.LastDNAorPatientCancelledDate = source.LastDNAorPatientCancelledDate
			,target.DirectorateCode = source.DirectorateCode
			,target.StaffGroupCode = source.StaffGroupCode
			,target.InterpreterIndicator = source.InterpreterIndicator
			,target.EpisodicSiteCode = source.EpisodicSiteCode
			,target.PseudoPostcode = source.PseudoPostcode
			,target.CCGCode = source.CCGCode
			,target.GpPracticeCodeAtAppointment= source.GpPracticeCodeAtAppointment
			,target.PostcodeAtAppointment= source.PostcodeAtAppointment
			,target.GpCodeAtAppointment= source.GpCodeAtAppointment
			,target.EncounterChecksum = source.EncounterChecksum
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
	into
		OP.ProcessList
;


if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			OP.ProcessList
		) MergeSummary


--11 Apr 2013 PDO
--removed until RTT details are required in reporting.
--should fix this to take the LATEST RTT details when updating

----now copy RTT Current Status back to historical appointments
--update OP.Encounter
--set
--	 RTTPathwayCondition = TEncounter.RTTPathwayCondition
--	,RTTStartDate = TEncounter.RTTStartDate
--	,RTTEndDate = TEncounter.RTTEndDate
--	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
--	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
--	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
--	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
--	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
--	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
--from
--	ETL.TImportOPEncounter TEncounter
--where
--	TEncounter.RTTPathwayID = Encounter.RTTPathwayID


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
		', Period from ' + CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




﻿
CREATE proc [ETL].[ExtractMaternityPostnatalCare]

	@FromDate datetime = null
	,@ToDate datetime = null
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @LocalFromDate datetime = @FromDate
declare @LocalToDate datetime = @ToDate

select @StartTime = getdate()

select @RowsInserted = 0

set dateformat dmy	

insert ETL.MaternityPostnatalCare
(
	SourceUniqueID
	,CasenoteNumber
	,NHSNumber
	,DateOfBirth
	,AgeAtBooking
	,GPPracticeCode
	,DischargedToHealthVisitorDate
	,HIV
	,RenalDisease
	,Renal
	,RHIsoimmunisationCode
	,DiabeticCode
	,DiabetesCode
	,EndocrineCode
	,AntenatalEndocrineCode
	,DiabetesEndocrine
	,HeartDisease
	,CardiacCode
	,CardiacComplications
	,NumberOfFetuses
	,InheritedDiseases
	,PsychiatricProblemCode
	,BMIAtBooking
	,Interpreter
	,PatientTypeCode
	,SpecialistMidwifeReferral
	,AccomodationTypeCode
	,DrugCode
	,HypertensionComplications
	,Thromboembolism
	,DVT
	,HELLP
	,Preeclampsia
	,HELLPPreeclampsia
	,Eclampsia
	,NeonatalDeathStillBirth
	,ModifiedTime
)


select
	SourceUniqueID
	,CasenoteNumber 				
	,NHSNumber 							
	,DateOfBirth 
	,AgeAtBooking                    								
	,GPPracticeCode 
	,DischargedToHealthVisitorDate 						
	,HIV 							
	,RenalDisease    								
	,Renal								
	,RHIsoimmunisationCode								
	,DiabeticCode 							
	,DiabetesCode 								
	,EndocrineCode			
	,AntenatalEndocrineCode 	
	,DiabetesEndocrine 								
	,HeartDisease 									
	,CardiacCode 								
	,CardiacComplications 							
	,NumberOfFetuses 								
	,InheritedDiseases 								
	,PsychiatricProblemCode 								
	,BMIAtBooking 									
	,Interpreter 									
	,PatientTypeCode									
	,SpecialistMidwifeReferral 								
	,AccomodationTypeCode 								
	,DrugCode 							
	,HypertensionComplications 							
	,Thromboembolism 							
	,DVT						
	,HELLP 								
	,Preeclampsia 								
	,HELLPPreeclampsia 								
	,Eclampsia								
	,NeonatalDeathStillBirth
	,ModifiedTime
from
	(									
	select									        									
		SourceUniqueID = rtrim(LABOUR.M_NUMBER) + '||' + cast(LABOUR.OCCURRENCE as varchar(50))
		,CasenoteNumber = rtrim(LABOUR.M_NUMBER)									
		,NHSNumber = NUMBER_INDEX.NHS_NUMBER									
		,DateOfBirth = cast(MOTHER_REG.M_DOB as date)	
		,AgeAtBooking = BK_VISIT_DATA.AGE_AT_BOOKING                         								
		,GPPracticeCode = GP.GP_PRACTICE	
		,DischargedToHealthVisitorDate = cast(LABOUR.DISCHARGED_TO_HV_DATE as date)									
		,HIV = BK_VISIT_DATA.HIV_RESULT									
		,RenalDisease = BK_VISIT_DATA.RENAL_DISEASE -- difference to below?		   								
		,Renal = AN_SUMMARY.RENAL -- difference to above?										
		,RHIsoimmunisationCode = BK_VISIT_DATA.RH_ISOIMMUNISATION 									
		,DiabeticCode = BK_VISIT_DATA.DIABETIC 	-- difference to below?								
		,DiabetesCode = AN_SUMMARY.DIABETES -- difference to above?										
		,EndocrineCode = BK_VISIT_DATA.OTHER_ENDOCRINE_PROB					
		,AntenatalEndocrineCode = AN_SUMMARY.OTHER_ENDOCRINE_PROB			
		,DiabetesEndocrine = DIABETESENDOCRINE.DIABETESENDOCRINE									
		,HeartDisease = BK_VISIT_DATA.HEART_DISEASE									
		,CardiacCode = AN_SUMMARY.CARDIAC 									
		,CardiacComplications = CARDIACCOMP.CARDIACCOMP 									
		,NumberOfFetuses = AN_SUMMARY.NUMBER_OF_FETUSES									
		,InheritedDiseases = BK_VISIT_DATA.FH_INHERITED_DISEASES									
		,PsychiatricProblemCode = BK_VISIT_DATA.PSYCHIATRIC_PROBLEMS									
		,BMIAtBooking = BK_VISIT_DATA.BMI_AT_BOOKING									
		,Interpreter = BK_VISIT_DATA.INTERPRETER									
		,PatientTypeCode = PREGNANCY.TYPE_OF_PATIENT									
		,SpecialistMidwifeReferral = BK_VISIT_DATA.SPECIALIST_MW_REFERRAL									
		,AccomodationTypeCode = BK_VISIT_DATA.ACCOMMODATION_TYPE									
		,DrugCode = Left(BK_VISIT_DATA.DRUGS,1)	--Needs to be reworked as can now hold Multiple values								
		,HypertensionComplications = HYPERTENSIONCOMP.HYPERTENSIONCOMP									
		,Thromboembolism = LABOUR.THROMBOEMBOLISM									
		,DVT = DVT.DVT									
		,HELLP = AN_SUMMARY.HELLP									
		,Preeclampsia = AN_SUMMARY.PRE_ECLAMPSIA									
		,HELLPPreeclampsia = HELLPPREECLAMP.HELLPPREECLAMP									
		,Eclampsia = LABOUR.ECLAMPSIA									
		,NeonatalDeathStillBirth = NEONATALDEATHSTILLBIRTH.NEONATALDEATHSTILLBIRTH		
		,ModifiedTime = LABOUR.DATE_STAMP																													
	from										
		[$(CMISStaging)].SMMIS.LABOUR									
											
	left join [$(CMISStaging)].SMMIS.MOTHER_REG									
	on MOTHER_REG.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
										
	left join [$(CMISStaging)].SMMIS.BK_VISIT_DATA									
	on	BK_VISIT_DATA.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and BK_VISIT_DATA.OCCURRENCE = LABOUR.OCCURRENCE		
										
	left join [$(CMISStaging)].SMMIS.PREGNANCY									
	on	PREGNANCY.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and PREGNANCY.OCCURRENCE = LABOUR.OCCURRENCE									
	  										
	left join [$(CMISStaging)].SMMIS.NUMBER_INDEX									
	on	NUMBER_INDEX.HOSP_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS					
	and NUMBER_INDEX.MOTHER_INFANT = 'M'									
										
	left join [$(CMISStaging)].SMMIS.AN_SUMMARY									
	on	AN_SUMMARY.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and AN_SUMMARY.OCCURRENCE = LABOUR.OCCURRENCE									
	    									
	left join [$(CMISStaging)].SMMIS.GP									
	on	GP.GP_SEQUENCE = MOTHER_REG.GP_SEQUENCE									
	and GP.GP_SURNAME = MOTHER_REG.GP_SURNAME									
	and GP.ACTIVE = 'Y'									
	  										
	left join									
		(								
		select							
			M_NUMBER					
			,OCCURRENCE					
			,HELLPPREECLAMP = count(*)											
		from							
			[$(CMISStaging)].SMMIS.LABOUR											
		where 							
			D_MAT_COMPL Like '%H%'					
		or	D_MAT_COMPL Like '%P%'											
		group by 							
			M_NUMBER					
			,OCCURRENCE					
		) HELLPPREECLAMP 									
	on	HELLPPREECLAMP.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and HELLPPREECLAMP.OCCURRENCE = LABOUR.OCCURRENCE									
	        										
	left join									
		(								
		select								
			M_NUMBER							
			,OCCURRENCE							
			,DVT = count(*)													
		from								
			[$(CMISStaging)].SMMIS.LABOUR																
		where 								
			D_MAT_COMPL Like '%V%'							
		or	D_MAT_COMPL Like '%B%'														
		group by 								
			M_NUMBER							
			,OCCURRENCE							
		) DVT								
	on	DVT.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and DVT.OCCURRENCE = LABOUR.OCCURRENCE    									
	              										
	left join										
		(								
		select								
			M_NUMBER							
			,OCCURRENCE							
			,DIABETESENDOCRINE = count(*)												
		from								
			[$(CMISStaging)].SMMIS.LABOUR															
		where 								
			D_MAT_COMPL Like '%D%'							
			or D_MAT_COMPL Like '%E%'															
		group by 								
			M_NUMBER							
			,OCCURRENCE							
		) DIABETESENDOCRINE								
	on	DIABETESENDOCRINE.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and DIABETESENDOCRINE.OCCURRENCE = LABOUR.OCCURRENCE									
	                    										
	left join									
		(								
		select								
			M_NUMBER							
			,OCCURRENCE							
			,CARDIACCOMP = count(*)												
		from								
			[$(CMISStaging)].SMMIS.LABOUR															
		where 								
			D_MAT_COMPL Like '%C%'															
		group by 								
			M_NUMBER							
			,OCCURRENCE							
		) CARDIACCOMP								
	on	CARDIACCOMP.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and CARDIACCOMP.OCCURRENCE = LABOUR.OCCURRENCE 									
										
	left join									
		(								
		select								
			M_NUMBER							
			,OCCURRENCE							
			,HYPERTENSIONCOMP = count(*)																				
		from								
			[$(CMISStaging)].SMMIS.LABOUR														
		where 								
			D_MAT_COMPL Like '%G%'															
		group by 								
			M_NUMBER							
			,OCCURRENCE							
		) HYPERTENSIONCOMP 								
	on	HYPERTENSIONCOMP.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and HYPERTENSIONCOMP.OCCURRENCE = LABOUR.OCCURRENCE									
										
	left join									
		(								
		select								
			 M_NUMBER							
			,OCCURRENCE							
			,NEONATALDEATHSTILLBIRTH = count(*)															
		from								
			[$(CMISStaging)].SMMIS.BIRTH_REG															
		where 								
			OUTCOME not in ('L','Y','Z')															
		group by 								
			 M_NUMBER							
			,OCCURRENCE							
		) NEONATALDEATHSTILLBIRTH								
	on	NEONATALDEATHSTILLBIRTH.M_NUMBER = LABOUR.M_NUMBER collate Latin1_General_CS_AS									
	and NEONATALDEATHSTILLBIRTH.OCCURRENCE = LABOUR.OCCURRENCE									
											
	where										
		LABOUR.DATE_STAMP between @FromDate and @ToDate		

	) PostNatal				

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	
	


	

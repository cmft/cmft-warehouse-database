﻿CREATE procedure [ETL].[ExtractSIDMedicalReview]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDMedicalReview;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDMedicalReview
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,MDTDateTimeOfMDT
	,MDTChairedBy
	,MDTMembersPresentDTODoctor
	,MDTMembersPresentDTOANP
	,MDTMembersPresentDTONurse
	,MDTMembersPresentDTOPhysio
	,MDTMembersPresentDTOSLT
	,MDTMembersPresentDTOOT
	,MDTMembersPresentDTOSpecialistNurse
	,MDTMembersPresentDTOOther
	,MDTMembersPresentDTOOthersText
	,MDTRehabStream
	,MDTMedicalUpdateAndActionPlan
	,MDTHasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia
	,MDTHasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection
	,MDTBarthelScores
	,MDTBarthelScoresMDTFeeding
	,MDTBarthelScoresMDTBathing
	,MDTBarthelScoresMDTGrooming
	,MDTBarthelScoresMDTDressing
	,MDTBarthelScoresMDTBowels
	,MDTBarthelScoresMDTBladder
	,MDTBarthelScoresMDTToiletUse
	,MDTBarthelScoresMDTTransfers
	,MDTBarthelScoresMDTMobility
	,MDTBarthelScoresMDTStairs
	,MDTBarthelScoresMDTTotalScore
	,MDTWaterlowScore
	,MDTMUST
	,MDTFallsRiskScore
	,MDTModifiedRankingScore
	,MDTTOMsAphasia
	,MDTTOMsDysphagia
	,MDTMoodAlgorithmaReviewed
	,MDTFormalMoodAssessmentRequired
	,MDTMoodToolUsed
	,MDTMoodAssessmentScore
	,MDTDateTimeMoodAssessment
	,MDTMoCAAssessmentScore
	,MDTDateTimeMoCAAssessmentScore
	,MDTNursing
	,MDTPatientImpairments
	,MDTFunctionalStatus
	,MDTEligibleForESD
	,MDTEligibleForESDNo
	,MDTESD
	,MDTComplexDischarge
	,MDTSocialServices
	,MDTICT
	,MDTHomepathway
	,MDTReablement
	,MDTDischargePlanningNotes
	,MDTNotes
	,MDTUsername
	,MDTModifiedDate
	,PrincipleDiagnosisAndMedicalUpdate
	,CTMRScanResults
	,CoMorbiditiesCongestiveHeartFailure
	,CoMorbiditiesHypertension
	,CoMorbiditiesAtrialFibrillation
	,CoMorbiditiesDiabeties
	,CoMorbiditiesStroke
	,CoMorbiditiesTIA
	,CoMorbiditiesOtherMorbidities
	,CoMorbiditiesNoMorbidities
	,WasThePatientAlreadyOnAntiplateletMedication
	,WasThePatientAlreadyOnAnticoagulant
	,HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia
	,HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection
	,FunctionalBaseline
	,MoCAAssessmentScore
	,DateTimeMoCAAssessmentScore
	,ActionPlanGoals
	,AdviceOnSmoking
	,AdviceOnDriving
	,AdviceOnAlcoholConsumption
	,AdviceOnExercise
	,Username
	,ModifiedDate
	,IsSectionComplete
	,SectionCompleteTime
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		CandidateData.SectionID
	,ActionID = 
		CandidateData.ActionID

	,MDTDateTimeOfMDT = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/DateTimeOfMDT)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,MDTChairedBy = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/ChairedBy)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTODoctor = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/Doctor)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOANP = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/ANP)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTONurse = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/Nurse)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOPhysio = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/Physio)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOSLT = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/SLT)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOOT = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/OT)[1]', 'varchar(max)'),'')
	,MDTMemebersPresentDTOSpecialistNurse = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/SpecialistNurse)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOOther = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/Other)[1]', 'varchar(max)'),'')
	,MDTMembersPresentDTOOthersText = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MembersPresent/OthersText)[1]', 'varchar(max)'),'')
	,MDTRehabStream = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/RehabStream)[1]', 'varchar(max)'),'')
	,MDTMedicalUpdateAndActionPlan = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MedicalUpdateAndActionPlan)[1]', 'varchar(max)'),'')
	,MDTHasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia)[1]', 'varchar(max)'),'')
	,MDTHasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection)[1]', 'varchar(max)'),'')
	,MDTBarthelScores = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScores)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTFeeding = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Feeding)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTBathing = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Bathing)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTGrooming = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Grooming)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTDressing = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Dressing)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTBowels = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Bowels)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTBladder = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Bladder)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTToiletUse = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/ToiletUse)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTTransfers = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Transfers)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTMobility = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Mobility)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTStairs = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/Stairs)[1]', 'varchar(max)'),'')
	,MDTBarthelScoresMDTTotalScore = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/BarthelScoresMDT/TotalScore)[1]', 'varchar(max)'),'')
	,MDTWaterlowScore = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/WaterlowScore)[1]', 'varchar(max)'),'')
	,MDTMUST = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MUST)[1]', 'varchar(max)'),'')
	,MDTFallsRiskScore = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/FallsRiskScore)[1]', 'varchar(max)'),'')
	,MDTModifiedRankingScore = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/ModifiedRankingScore)[1]', 'varchar(max)'),'')
	,MDTTOMsAphasia = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/TOMsAphasia)[1]', 'varchar(max)'),'')
	,MDTTOMsDysphagia = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/TOMsDysphagia)[1]', 'varchar(max)'),'')
	,MDTMoodAlgorithmaReviewed = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MoodAlgorithmaReviewed)[1]', 'varchar(max)'),'')
	,MDTFormalMoodAssessmentRequired = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/FormalMoodAssessmentRequired)[1]', 'varchar(max)'),'')
	,MDTMoodToolUsed = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MoodToolUsed)[1]', 'varchar(max)'),'')
	,MDTMoodAssessmentScore = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MoodAssessmentScore)[1]', 'varchar(max)'),'')
	,MDTDateTimeMoodAssessment = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/DateTimeMoodAssessment)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,MDTMoCAAssessmentScore = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/MoCAAssessmentScore)[1]', 'varchar(max)'),'')
	,MDTDateTimeMoCAAssessmentScore = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/DateTimeMoCAAssessmentScore)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,MDTNursing = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/Nursing)[1]', 'varchar(max)'),'')
	,MDTPatientImpairments = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/PatientImpairments)[1]', 'varchar(max)'),'')
	,MDTFunctionalStatus = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/FunctionalStatus)[1]', 'varchar(max)'),'')
	,MDTEligibleForESD = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/EligibleForESD)[1]', 'varchar(max)'),'')
	,MDTEligibleForESDNo = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/EligibleForESDNo)[1]', 'varchar(max)'),'')
	,MDTESD = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/ESD)[1]', 'varchar(max)'),'')
	,MDTComplexDischarge = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/ComplexDischarge)[1]', 'varchar(max)'),'')
	,MDTSocialServices = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/SocialServices)[1]', 'varchar(max)'),'')
	,MDTICT = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/ICT)[1]', 'varchar(max)'),'')
	,MDTHomepathway = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/Homepathway)[1]', 'varchar(max)'),'')
	,MDTReablement = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/Reablement)[1]', 'varchar(max)'),'')
	,MDTDischargePlanningNotes = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/DischargePlanningNotes)[1]', 'varchar(max)'),'')
	,MDTNotes = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/Notes)[1]', 'varchar(max)'),'')
	,MDTUsername = 
		CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/UserModified/Username)[1]', 'varchar(max)')
	,MDTModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/MedicalReviewMDTDTO/MDT/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,PrincipleDiagnosisAndMedicalUpdate = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/PrincipleDiagnosisAndMedicalUpdate)[1]', 'varchar(max)'),'')
	,CTMRScanResults = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CTMRScanResults)[1]', 'varchar(max)'),'')
	,CoMorbiditiesCongestiveHeartFailure = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/CongestiveHeartFailure)[1]', 'varchar(max)'),'')
	,CoMorbiditiesHypertension = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/Hypertension)[1]', 'varchar(max)'),'')
	,CoMorbiditiesAtrialFibrillation = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/AtrialFibrillation)[1]', 'varchar(max)'),'')
	,CoMorbiditiesDiabeties = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/Diabeties)[1]', 'varchar(max)'),'')
	,CoMorbiditiesStroke = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/Stroke)[1]', 'varchar(max)'),'')
	,CoMorbiditiesTIA = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/TIA)[1]', 'varchar(max)'),'')
	,CoMorbiditiesOtherMorbidities = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/OtherMorbidities)[1]', 'varchar(max)'),'')
	,CoMorbiditiesNoMorbidities = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/CoMorbidities/NoMorbidities)[1]', 'varchar(max)'),'')
	,WasThePatientAlreadyOnAntiplateletMedication = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/WasThePatientAlreadyOnAntiplateletMedication)[1]', 'varchar(max)'),'')
	,WasThePatientAlreadyOnAnticoagulant = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/WasThePatientAlreadyOnAnticoagulant)[1]', 'varchar(max)'),'')
	,HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/HasThePatientRequiredAntibioticsForHospitalAccquiredPnemonia)[1]', 'varchar(max)'),'')
	,HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/HasThePatientRequiredAntibioticsForHospitalUrinaryTractInfection)[1]', 'varchar(max)'),'')
	,FunctionalBaseline = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/FunctionalBaseline)[1]', 'varchar(max)'),'')
	,MoCAAssessmentScore = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/MoCAAssessmentScore)[1]', 'varchar(max)'),'')
	,DateTimeMoCAAssessmentScore = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/MedicalReviewMDTDTO/DateTimeMoCAAssessmentScore)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ActionPlanGoals = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/ActionPlanGoals)[1]', 'varchar(max)'),'')
	,AdviceOnSmoking = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/AdviceOnSmoking)[1]', 'varchar(max)'),'')
	,AdviceOnDriving = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/AdviceOnDriving)[1]', 'varchar(max)'),'')
	,AdviceOnAlcoholConsumption = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/AdviceOnAlcoholConsumption)[1]', 'varchar(max)'),'')
	,AdviceOnExercise = 
		nullif(CandidateData.Details.value('(/MedicalReviewMDTDTO/AdviceOnExercise)[1]', 'varchar(max)'),'')
	,Username = 
		CandidateData.Details.value('(/MedicalReviewMDTDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/MedicalReviewMDTDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))
	,IsSectionComplete = 
		SectionComplete.Details.value('(/SectionCompletedDTO/CurrentSelection)[1]', 'varchar(max)')
	,SectionCompleteTime = 
		SectionComplete.DateTimeCreated
from 
	[$(SID)].dbo.TARNCandidateData CandidateData
left outer join 
(
	select
		*
	from
		[$(SID)].dbo.TARNCandidateData 
	where
		SectionID = 19
	and ActionID = 50
) SectionComplete
on SectionComplete.CandidateID = CandidateData.CandidateID
where
	CandidateData.SectionID = 19
and CandidateData.ActionID =99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDMedicalReview', @Stats, @StartTime





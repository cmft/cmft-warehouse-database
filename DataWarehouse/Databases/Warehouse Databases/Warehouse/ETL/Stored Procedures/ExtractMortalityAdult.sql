﻿

CREATE procedure [ETL].[ExtractMortalityAdult]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.Adult target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,EpisodeStartTime
		,FormTypeCode
		,MortalityReviewAdded
		,ReviewStatusCode
		,DelayInDiagnosis 
		,DelayInDiagnosisDetail 
		,DelayInDeliveringCare 
		,DelayInDeliveringCareDetail 
		,PoorCommunication 
		,PoorCommunicationDetail 
		,OrganisationalFailure 
		,OrganisationalFailureDetail
		,AreasOfConcernNoDiff 
		,AreasOfConcernNoDiffDetail 
		,AreasOfConcernContributed 
		,AreasOfConcernContributedDetail 
		,DoneDifferently 
		,DoneDifferentlyDetail 
		,DocStandard 
		,FurtherComment
		,ClassificationScore 
		,PrimaryDiagnosis 
		,ConfirmedPrimaryDiagnosis 
		,DeathExpected
		,RaisedEWS
		,EWSNotApplicable 
		,PolicyMet
		,EOLPlan 
		,CauseOfDeath1 
		,CauseOfDeath2 
		,CauseOfDeath3 
		,CauseOfDeath4 
		,PostMortem
		,CoronerInformed 
		,MalignancyPresent
		,AcuteMyocardialInfarction 
		,CerebralVA 
		,CongestiveHeartFailure 
		,ConnectiveTissue 
		,Dementia 
		,Diabetes 
		,LiverDisease 
		,PepticUlcer 
		,PeripheralVascularDisease 
		,PulmonaryDisease
		,Cancer 
		,DiabetesComplications 
		,Paraplegia
		,RenalDisease
		,MetastaticCancer 
		,SevereLiverDisease
		,HIV
		,ClinicalReviewTime 
		,ConsultantReviewTime
		,SeenWithinTwelveHours
		,AppropriateWard 
		,AppropriateWardDetail 
		,HowManyWards 
		,HowManyWardsDetail 
		,WriteInNotes 
		,WriteInNotesDetail 
		,NotReviewed48Hour 
		,NotReviewed48HourDetail 
		,FluidBalance 
		,FluidBalanceDetail 
		,Surgery 
		,SurgeryDetail
		,Sepsis 
		,SepsisDetail 
		,SepsisRecognised 
		,SepsisRecognisedDetail 
		,SepsisTreated 
		,SepsisTreatedDetail 
		,DNACPR
		,DNACPRDetail 
		,DNACPRDiscussion 
		,DNACPRDiscussionDetail 
		,ManagementPlan
		,ManagementPlanDetail 
		,Investigations 
		,InvestigationsDetail 
		,ManagementSteps 
		,ManagementStepsDetail 
		,Omissions 
		,OmissionsDetail 
		,CriticalCareReview 
		,CriticalCareReviewDetail 
		,CriticalAppropriateAdmitted 
		,CriticalAppropriateAdmittedDetail 
		,CriticalLessThan4Hours 
		,CriticalLessThan4HoursDetail 
		,CriticalAppropriateNotAdmitted
		,CriticalAppropriateNotAdmittedDetail 
		,MedicationAnyErrors 
		,MedicationAnyErrorsDetail 
		,PrimaryReviewerCode
		,PrimaryReviewerAssignedTime 
		,PrimaryReviewerCompletedTime
		,SecondaryReviewerCode
		,SecondaryReviewerComments 
		,SecondaryReviewerAssignedTime
		,SecondaryReviewerCompletedTime
		,ContextCode
	from
		ETL.TLoadMortalityAdult
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalityAdult Latest
			where
				Latest.SourcePatientNo = TLoadMortalityAdult.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalityAdult.SourceUniqueID
			)
			
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourcePatientNo
			,EpisodeStartTime
			,FormTypeCode
			,MortalityReviewAdded
			,ReviewStatusCode
			,DelayInDiagnosis 
			,DelayInDiagnosisDetail 
			,DelayInDeliveringCare 
			,DelayInDeliveringCareDetail 
			,PoorCommunication 
			,PoorCommunicationDetail 
			,OrganisationalFailure 
			,OrganisationalFailureDetail
			,AreasOfConcernNoDiff 
			,AreasOfConcernNoDiffDetail 
			,AreasOfConcernContributed 
			,AreasOfConcernContributedDetail 
			,DoneDifferently 
			,DoneDifferentlyDetail 
			,DocStandard 
			,FurtherComment
			,ClassificationScore 
			,PrimaryDiagnosis 
			,ConfirmedPrimaryDiagnosis 
			,DeathExpected
			,RaisedEWS
			,EWSNotApplicable 
			,PolicyMet
			,EOLPlan 
			,CauseOfDeath1 
			,CauseOfDeath2 
			,CauseOfDeath3 
			,CauseOfDeath4 
			,PostMortem
			,CoronerInformed 
			,MalignancyPresent
			,AcuteMyocardialInfarction 
			,CerebralVA 
			,CongestiveHeartFailure 
			,ConnectiveTissue 
			,Dementia 
			,Diabetes 
			,LiverDisease 
			,PepticUlcer 
			,PeripheralVascularDisease 
			,PulmonaryDisease
			,Cancer 
			,DiabetesComplications 
			,Paraplegia
			,RenalDisease
			,MetastaticCancer 
			,SevereLiverDisease
			,HIV
			,ClinicalReviewTime 
			,ConsultantReviewTime
			,SeenWithinTwelveHours
			,AppropriateWard 
			,AppropriateWardDetail 
			,HowManyWards 
			,HowManyWardsDetail 
			,WriteInNotes 
			,WriteInNotesDetail 
			,NotReviewed48Hour 
			,NotReviewed48HourDetail 
			,FluidBalance 
			,FluidBalanceDetail 
			,Surgery 
			,SurgeryDetail
			,Sepsis 
			,SepsisDetail 
			,SepsisRecognised 
			,SepsisRecognisedDetail 
			,SepsisTreated 
			,SepsisTreatedDetail 
			,DNACPR
			,DNACPRDetail 
			,DNACPRDiscussion 
			,DNACPRDiscussionDetail 
			,ManagementPlan
			,ManagementPlanDetail 
			,Investigations 
			,InvestigationsDetail 
			,ManagementSteps 
			,ManagementStepsDetail 
			,Omissions 
			,OmissionsDetail 
			,CriticalCareReview 
			,CriticalCareReviewDetail 
			,CriticalAppropriateAdmitted 
			,CriticalAppropriateAdmittedDetail 
			,CriticalLessThan4Hours 
			,CriticalLessThan4HoursDetail 
			,CriticalAppropriateNotAdmitted
			,CriticalAppropriateNotAdmittedDetail 
			,MedicationAnyErrors 
			,MedicationAnyErrorsDetail 
			,PrimaryReviewerCode
			,PrimaryReviewerAssignedTime 
			,PrimaryReviewerCompletedTime
			,SecondaryReviewerCode
			,SecondaryReviewerComments 
			,SecondaryReviewerAssignedTime
			,SecondaryReviewerCompletedTime
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.FormTypeCode
			,source.MortalityReviewAdded
			,source.ReviewStatusCode
			,source.DelayInDiagnosis 
			,source.DelayInDiagnosisDetail 
			,source.DelayInDeliveringCare 
			,source.DelayInDeliveringCareDetail 
			,source.PoorCommunication 
			,source.PoorCommunicationDetail 
			,source.OrganisationalFailure 
			,source.OrganisationalFailureDetail
			,source.AreasOfConcernNoDiff 
			,source.AreasOfConcernNoDiffDetail 
			,source.AreasOfConcernContributed 
			,source.AreasOfConcernContributedDetail 
			,source.DoneDifferently 
			,source.DoneDifferentlyDetail 
			,source.DocStandard 
			,source.FurtherComment
			,source.ClassificationScore 
			,source.PrimaryDiagnosis 
			,source.ConfirmedPrimaryDiagnosis 
			,source.DeathExpected
			,source.RaisedEWS
			,source.EWSNotApplicable 
			,source.PolicyMet
			,source.EOLPlan 
			,source.CauseOfDeath1 
			,source.CauseOfDeath2 
			,source.CauseOfDeath3 
			,source.CauseOfDeath4 
			,source.PostMortem
			,source.CoronerInformed 
			,source.MalignancyPresent
			,source.AcuteMyocardialInfarction 
			,source.CerebralVA 
			,source.CongestiveHeartFailure 
			,source.ConnectiveTissue 
			,source.Dementia 
			,source.Diabetes 
			,source.LiverDisease 
			,source.PepticUlcer 
			,source.PeripheralVascularDisease 
			,source.PulmonaryDisease
			,source.Cancer 
			,source.DiabetesComplications 
			,source.Paraplegia
			,source.RenalDisease
			,source.MetastaticCancer 
			,source.SevereLiverDisease
			,source.HIV
			,source.ClinicalReviewTime 
			,source.ConsultantReviewTime
			,source.SeenWithinTwelveHours
			,source.AppropriateWard 
			,source.AppropriateWardDetail 
			,source.HowManyWards 
			,source.HowManyWardsDetail 
			,source.WriteInNotes 
			,source.WriteInNotesDetail 
			,source.NotReviewed48Hour 
			,source.NotReviewed48HourDetail 
			,source.FluidBalance 
			,source.FluidBalanceDetail 
			,source.Surgery 
			,source.SurgeryDetail
			,source.Sepsis 
			,source.SepsisDetail 
			,source.SepsisRecognised 
			,source.SepsisRecognisedDetail 
			,source.SepsisTreated 
			,source.SepsisTreatedDetail 
			,source.DNACPR
			,source.DNACPRDetail 
			,source.DNACPRDiscussion 
			,source.DNACPRDiscussionDetail 
			,source.ManagementPlan
			,source.ManagementPlanDetail 
			,source.Investigations 
			,source.InvestigationsDetail 
			,source.ManagementSteps 
			,source.ManagementStepsDetail 
			,source.Omissions 
			,source.OmissionsDetail 
			,source.CriticalCareReview 
			,source.CriticalCareReviewDetail 
			,source.CriticalAppropriateAdmitted 
			,source.CriticalAppropriateAdmittedDetail 
			,source.CriticalLessThan4Hours 
			,source.CriticalLessThan4HoursDetail 
			,source.CriticalAppropriateNotAdmitted
			,source.CriticalAppropriateNotAdmittedDetail 
			,source.MedicationAnyErrors 
			,source.MedicationAnyErrorsDetail 
			,source.PrimaryReviewerCode
			,source.PrimaryReviewerAssignedTime 
			,source.PrimaryReviewerCompletedTime
			,source.SecondaryReviewerCode
			,source.SecondaryReviewerComments 
			,source.SecondaryReviewerAssignedTime
			,source.SecondaryReviewerCompletedTime
			,source.ContextCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.FormTypeCode, 0) = isnull(source.FormTypeCode, 0)
		and isnull(target.MortalityReviewAdded, getdate()) = isnull(source.MortalityReviewAdded, getdate())
		and isnull(target.ReviewStatusCode, 0) = isnull(source.ReviewStatusCode, 0)
		and isnull(target.DelayInDiagnosis, 0) = isnull(source.DelayInDiagnosis, 0)
		and isnull(target.DelayInDiagnosisDetail, '') = isnull(source.DelayInDiagnosisDetail, '')
		and isnull(target.DelayInDeliveringCare, 0) = isnull(source.DelayInDeliveringCare, 0)
		and isnull(target.DelayInDeliveringCareDetail, '') = isnull(source.DelayInDeliveringCareDetail, '')
		and isnull(target.PoorCommunication, 0) = isnull(source.PoorCommunication, 0)
		and isnull(target.PoorCommunicationDetail, '') = isnull(source.PoorCommunicationDetail, '')
		and isnull(target.OrganisationalFailure, 0) = isnull(source.OrganisationalFailure, 0)
		and isnull(target.OrganisationalFailureDetail, '') = isnull(source.OrganisationalFailureDetail, '')
		and isnull(target.AreasOfConcernNoDiff, 0) = isnull(source.AreasOfConcernNoDiff, 0)
		and isnull(target.AreasOfConcernNoDiffDetail, '') = isnull(source.AreasOfConcernNoDiffDetail, '')
		and isnull(target.AreasOfConcernContributed, 0) = isnull(source.AreasOfConcernContributed, 0)
		and isnull(target.AreasOfConcernContributedDetail, '') = isnull(source.AreasOfConcernContributedDetail, '')
		and isnull(target.DoneDifferently, 0) = isnull(source.DoneDifferently, 0)
		and isnull(target.DoneDifferentlyDetail, '') = isnull(source.DoneDifferentlyDetail, '')
		and isnull(target.DocStandard, 0) = isnull(source.DocStandard, 0)
		and isnull(target.FurtherComment, '') = isnull(source.FurtherComment, '')
		and isnull(target.ClassificationScore, 0) = isnull(source.ClassificationScore, 0)
		and isnull(target.PrimaryDiagnosis, '') = isnull(source.PrimaryDiagnosis, '')
		and isnull(target.ConfirmedPrimaryDiagnosis, '') = isnull(source.ConfirmedPrimaryDiagnosis, '')
		and isnull(target.DeathExpected, 0) = isnull(source.DeathExpected, 0)
		and isnull(target.RaisedEWS, 0) = isnull(source.RaisedEWS, 0)
		and isnull(target.EWSNotApplicable, '') = isnull(source.EWSNotApplicable, '')
		and isnull(target.PolicyMet, 0) = isnull(source.PolicyMet, 0)
		and isnull(target.EOLPlan, 0) = isnull(source.EOLPlan, 0)
		and isnull(target.CauseOfDeath1, '') = isnull(source.CauseOfDeath1, '')
		and isnull(target.CauseOfDeath2, '') = isnull(source.CauseOfDeath2, '')
		and isnull(target.CauseOfDeath3, '') = isnull(source.CauseOfDeath3, '')
		and isnull(target.CauseOfDeath4, '') = isnull(source.CauseOfDeath4, '')
		and isnull(target.PostMortem, 0) = isnull(source.PostMortem, 0)
		and isnull(target.CoronerInformed, 0) = isnull(source.CoronerInformed, 0)
		and isnull(target.MalignancyPresent, 0) = isnull(source.MalignancyPresent, 0)
		and isnull(target.AcuteMyocardialInfarction, 0) = isnull(source.AcuteMyocardialInfarction, 0)
		and isnull(target.CerebralVA, 0) = isnull(source.CerebralVA, 0)
		and isnull(target.CongestiveHeartFailure, 0) = isnull(source.CongestiveHeartFailure, 0)
		and isnull(target.ConnectiveTissue, 0) = isnull(source.ConnectiveTissue, 0)
		and isnull(target.Dementia, 0) = isnull(source.Dementia, 0)
		and isnull(target.Diabetes, 0) = isnull(source.Diabetes, 0)
		and isnull(target.LiverDisease, 0) = isnull(source.LiverDisease, 0)
		and isnull(target.PepticUlcer, 0) = isnull(source.PepticUlcer, 0)
		and isnull(target.PeripheralVascularDisease, 0) = isnull(source.PeripheralVascularDisease, 0)
		and isnull(target.PulmonaryDisease, 0) = isnull(source.PulmonaryDisease, 0)
		and isnull(target.Cancer, 0) = isnull(source.Cancer, 0)
		and isnull(target.DiabetesComplications, 0) = isnull(source.DiabetesComplications, 0)
		and isnull(target.Paraplegia, 0) = isnull(source.Paraplegia, 0)
		and isnull(target.RenalDisease, 0) = isnull(source.RenalDisease, 0)
		and isnull(target.MetastaticCancer, 0) = isnull(source.MetastaticCancer, 0)
		and isnull(target.SevereLiverDisease, 0) = isnull(source.SevereLiverDisease, 0)
		and isnull(target.HIV, 0) = isnull(source.HIV, 0)
		and isnull(target.ClinicalReviewTime, getdate()) = isnull(source.ClinicalReviewTime, getdate())
		and isnull(target.ConsultantReviewTime, getdate()) = isnull(source.ConsultantReviewTime, getdate())
		and isnull(target.SeenWithinTwelveHours, 0) = isnull(source.SeenWithinTwelveHours, 0)
		and isnull(target.AppropriateWard, 0) = isnull(source.AppropriateWard, 0)
		and isnull(target.AppropriateWardDetail, '') = isnull(source.AppropriateWardDetail, '')
		and isnull(target.HowManyWards, '') = isnull(source.HowManyWards, '')
		and isnull(target.HowManyWardsDetail, '') = isnull(source.HowManyWardsDetail, '')
		and isnull(target.WriteInNotes, 0) = isnull(source.WriteInNotes, 0)
		and isnull(target.WriteInNotesDetail, '') = isnull(source.WriteInNotesDetail, '')
		and isnull(target.NotReviewed48Hour, 0) = isnull(source.NotReviewed48Hour, 0)
		and isnull(target.NotReviewed48HourDetail, '') = isnull(source.NotReviewed48HourDetail, '')
		and isnull(target.FluidBalance, 0) = isnull(source.FluidBalance, 0)
		and isnull(target.FluidBalanceDetail, '') = isnull(source.FluidBalanceDetail, '')
		and isnull(target.Surgery, 0) = isnull(source.Surgery, 0)
		and isnull(target.SurgeryDetail, '') = isnull(source.SurgeryDetail, '')
		and isnull(target.Sepsis, 0) = isnull(source.Sepsis, 0)
		and isnull(target.SepsisDetail, '') = isnull(source.SepsisDetail, '')
		and isnull(target.SepsisRecognised, 0) = isnull(source.SepsisRecognised, 0)
		and isnull(target.SepsisRecognisedDetail, '') = isnull(source.SepsisRecognisedDetail, '')
		and isnull(target.SepsisTreated, 0) = isnull(source.SepsisTreated, 0)
		and isnull(target.SepsisTreatedDetail, '') = isnull(source.SepsisTreatedDetail, '')
		and isnull(target.DNACPR, 0) = isnull(source.DNACPR, 0)
		and isnull(target.DNACPRDetail, '') = isnull(source.DNACPRDetail, '')
		and isnull(target.DNACPRDiscussion, 0) = isnull(source.DNACPRDiscussion, 0)
		and isnull(target.DNACPRDiscussionDetail, '') = isnull(source.DNACPRDiscussionDetail, '')
		and isnull(target.ManagementPlan, 0) = isnull(source.ManagementPlan, 0)
		and isnull(target.ManagementPlanDetail, '') = isnull(source.ManagementPlanDetail, '')
		and isnull(target.Investigations, 0) = isnull(source.Investigations, 0)
		and isnull(target.InvestigationsDetail, '') = isnull(source.InvestigationsDetail, '')
		and isnull(target.ManagementSteps, 0) = isnull(source.ManagementSteps, 0)
		and isnull(target.ManagementStepsDetail, '') = isnull(source.ManagementStepsDetail, '')
		and isnull(target.Omissions, 0) = isnull(source.Omissions, 0)
		and isnull(target.OmissionsDetail, '') = isnull(source.OmissionsDetail, '')
		and isnull(target.CriticalCareReview, 0) = isnull(source.CriticalCareReview, 0)
		and isnull(target.CriticalCareReviewDetail, '') = isnull(source.CriticalCareReviewDetail, '')
		and isnull(target.CriticalAppropriateAdmitted, 0) = isnull(source.CriticalAppropriateAdmitted, 0)
		and isnull(target.CriticalAppropriateAdmittedDetail, '') = isnull(source.CriticalAppropriateAdmittedDetail, '')
		and isnull(target.CriticalLessThan4Hours, 0) = isnull(source.CriticalLessThan4Hours, 0)
		and isnull(target.CriticalLessThan4HoursDetail, '') = isnull(source.CriticalLessThan4HoursDetail, '')
		and isnull(target.CriticalAppropriateNotAdmitted, 0) = isnull(source.CriticalAppropriateNotAdmitted, 0)
		and isnull(target.CriticalAppropriateNotAdmittedDetail, '') = isnull(source.CriticalAppropriateNotAdmittedDetail, '')
		and isnull(target.MedicationAnyErrors, 0) = isnull(source.MedicationAnyErrors, 0)
		and isnull(target.MedicationAnyErrorsDetail, '') = isnull(source.MedicationAnyErrorsDetail, '')
		and isnull(target.PrimaryReviewerCode, 0) = isnull(source.PrimaryReviewerCode, 0)
		and isnull(target.PrimaryReviewerAssignedTime, getdate()) = isnull(source.PrimaryReviewerAssignedTime, getdate())
		and isnull(target.PrimaryReviewerCompletedTime, getdate()) = isnull(source.PrimaryReviewerCompletedTime, getdate())
		and isnull(target.SecondaryReviewerCode, '') = isnull(source.SecondaryReviewerCode, '')
		and isnull(target.SecondaryReviewerComments, '') = isnull(source.SecondaryReviewerComments, '')
		and isnull(target.SecondaryReviewerAssignedTime, getdate()) = isnull(source.SecondaryReviewerAssignedTime, getdate())
		and isnull(target.SecondaryReviewerCompletedTime, getdate()) = isnull(source.SecondaryReviewerCompletedTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.FormTypeCode = source.FormTypeCode
			,target.MortalityReviewAdded = source.MortalityReviewAdded
			,target.ReviewStatusCode = source.ReviewStatusCode
			,target.DelayInDiagnosis = source.DelayInDiagnosis
			,target.DelayInDiagnosisDetail = source.DelayInDiagnosisDetail
			,target.DelayInDeliveringCare = source.DelayInDeliveringCare
			,target.DelayInDeliveringCareDetail = source.DelayInDeliveringCareDetail
			,target.PoorCommunication = source.PoorCommunication
			,target.PoorCommunicationDetail = source.PoorCommunicationDetail
			,target.OrganisationalFailure = source.OrganisationalFailure
			,target.OrganisationalFailureDetail = source.OrganisationalFailureDetail
			,target.AreasOfConcernNoDiff = source.AreasOfConcernNoDiff
			,target.AreasOfConcernNoDiffDetail = source.AreasOfConcernNoDiffDetail
			,target.AreasOfConcernContributed = source.AreasOfConcernContributed
			,target.AreasOfConcernContributedDetail = source.AreasOfConcernContributedDetail
			,target.DoneDifferently = source.DoneDifferently
			,target.DoneDifferentlyDetail = source.DoneDifferentlyDetail
			,target.DocStandard = source.DocStandard
			,target.FurtherComment = source.FurtherComment
			,target.ClassificationScore = source.ClassificationScore
			,target.PrimaryDiagnosis = source.PrimaryDiagnosis
			,target.ConfirmedPrimaryDiagnosis = source.ConfirmedPrimaryDiagnosis
			,target.DeathExpected = source.DeathExpected
			,target.RaisedEWS = source.RaisedEWS
			,target.EWSNotApplicable = source.EWSNotApplicable
			,target.PolicyMet = source.PolicyMet
			,target.EOLPlan = source.EOLPlan
			,target.CauseOfDeath1 = source.CauseOfDeath1
			,target.CauseOfDeath2 = source.CauseOfDeath2
			,target.CauseOfDeath3 = source.CauseOfDeath3
			,target.CauseOfDeath4 = source.CauseOfDeath4
			,target.PostMortem = source.PostMortem
			,target.CoronerInformed = source.CoronerInformed
			,target.MalignancyPresent = source.MalignancyPresent
			,target.AcuteMyocardialInfarction = source.AcuteMyocardialInfarction
			,target.CerebralVA = source.CerebralVA
			,target.CongestiveHeartFailure = source.CongestiveHeartFailure
			,target.ConnectiveTissue = source.ConnectiveTissue
			,target.Dementia = source.Dementia
			,target.Diabetes = source.Diabetes
			,target.LiverDisease = source.LiverDisease
			,target.PepticUlcer = source.PepticUlcer
			,target.PeripheralVascularDisease = source.PeripheralVascularDisease
			,target.PulmonaryDisease = source.PulmonaryDisease
			,target.Cancer = source.Cancer
			,target.DiabetesComplications = source.DiabetesComplications
			,target.Paraplegia = source.Paraplegia
			,target.RenalDisease = source.RenalDisease
			,target.MetastaticCancer = source.MetastaticCancer
			,target.SevereLiverDisease = source.SevereLiverDisease
			,target.HIV = source.HIV
			,target.ClinicalReviewTime = source.ClinicalReviewTime
			,target.ConsultantReviewTime = source.ConsultantReviewTime
			,target.SeenWithinTwelveHours = source.SeenWithinTwelveHours
			,target.AppropriateWard = source.AppropriateWard
			,target.AppropriateWardDetail = source.AppropriateWardDetail
			,target.HowManyWards = source.HowManyWards
			,target.HowManyWardsDetail = source.HowManyWardsDetail
			,target.WriteInNotes = source.WriteInNotes
			,target.WriteInNotesDetail = source.WriteInNotesDetail
			,target.NotReviewed48Hour = source.NotReviewed48Hour
			,target.NotReviewed48HourDetail = source.NotReviewed48HourDetail
			,target.FluidBalance = source.FluidBalance
			,target.FluidBalanceDetail = source.FluidBalanceDetail
			,target.Surgery = source.Surgery
			,target.SurgeryDetail = source.SurgeryDetail
			,target.Sepsis = source.Sepsis
			,target.SepsisDetail = source.SepsisDetail
			,target.SepsisRecognised = source.SepsisRecognised
			,target.SepsisRecognisedDetail = source.SepsisRecognisedDetail
			,target.SepsisTreated = source.SepsisTreated
			,target.SepsisTreatedDetail = source.SepsisTreatedDetail
			,target.DNACPR = source.DNACPR
			,target.DNACPRDetail = source.DNACPRDetail
			,target.DNACPRDiscussion = source.DNACPRDiscussion
			,target.DNACPRDiscussionDetail = source.DNACPRDiscussionDetail
			,target.ManagementPlan = source.ManagementPlan
			,target.ManagementPlanDetail = source.ManagementPlanDetail
			,target.Investigations = source.Investigations
			,target.InvestigationsDetail = source.InvestigationsDetail
			,target.ManagementSteps = source.ManagementSteps
			,target.ManagementStepsDetail = source.ManagementStepsDetail
			,target.Omissions = source.Omissions
			,target.OmissionsDetail = source.OmissionsDetail
			,target.CriticalCareReview = source.CriticalCareReview
			,target.CriticalCareReviewDetail = source.CriticalCareReviewDetail
			,target.CriticalAppropriateAdmitted = source.CriticalAppropriateAdmitted
			,target.CriticalAppropriateAdmittedDetail = source.CriticalAppropriateAdmittedDetail
			,target.CriticalLessThan4Hours = source.CriticalLessThan4Hours
			,target.CriticalLessThan4HoursDetail = source.CriticalLessThan4HoursDetail
			,target.CriticalAppropriateNotAdmitted = source.CriticalAppropriateNotAdmitted
			,target.CriticalAppropriateNotAdmittedDetail = source.CriticalAppropriateNotAdmittedDetail
			,target.MedicationAnyErrors = source.MedicationAnyErrors
			,target.MedicationAnyErrorsDetail = source.MedicationAnyErrorsDetail
			,target.PrimaryReviewerCode = source.PrimaryReviewerCode
			,target.PrimaryReviewerAssignedTime = source.PrimaryReviewerAssignedTime
			,target.PrimaryReviewerCompletedTime = source.PrimaryReviewerCompletedTime
			,target.SecondaryReviewerCode = source.SecondaryReviewerCode
			,target.SecondaryReviewerComments = source.SecondaryReviewerComments
			,target.SecondaryReviewerAssignedTime = source.SecondaryReviewerAssignedTime
			,target.SecondaryReviewerCompletedTime = source.SecondaryReviewerCompletedTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





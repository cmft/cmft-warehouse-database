﻿



CREATE proc [ETL].[BuildPatientAddress]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@RowsInserted int
	,@Stats varchar(max)

truncate table PAS.PatientAddress

insert into PAS.PatientAddress
(
	SourcePatientNo
	,DistrictNo
	,AddressLine1
	,AddressLine2
	,AddressLine3
	,AddressLine4
	,Postcode 
	,PreviousAddressSourceUniqueID
	,EffectiveFromDate 
	,EffectiveToDate 
	,IsCurrent
)

/* Get previous addresses */
	
select 
	SourcePatientNo
	,DistrictNo
	,PatientAddressLine1
	,PatientAddressLine2
	,PatientAddressLine3
	,PatientAddressLine4
	,Postcode
	,PREVADDRESSID
	,EffectiveFromDate = coalesce(
								EffectiveFromDate
								,'01/01/1900'
							) 
	,EffectiveToDate = dateadd(
							day
							,-1
							,EffectiveToDate
							) -- to ensure ranges mutually exclusive
	,0
from
	PAS.PreviousAddress
where
	Postcode is not null
and	not exists
				(
				select
					1
				from
					PAS.PreviousAddress LaterPreviousAddress
				where
					LaterPreviousAddress.SourcePatientNo = PreviousAddress.SourcePatientNo
				and	coalesce(
							LaterPreviousAddress.EffectiveFromDate
							,'01/01/1900'
						) =	coalesce(
									PreviousAddress.EffectiveFromDate
									,'01/01/1900'
								)
				and	LaterPreviousAddress.SequenceNo > PreviousAddress.SequenceNo
				and LaterPreviousAddress.Postcode is not null
				)

	
SELECT @RowsInserted = @@Rowcount
			
insert into PAS.PatientAddress
(
	SourcePatientNo
	,DistrictNo
	,AddressLine1
	,AddressLine2
	,AddressLine3
	,AddressLine4
	,Postcode 
	,PreviousAddressSourceUniqueID
	,EffectiveFromDate 
	,EffectiveToDate 
	,IsCurrent
)

/* Get current address from PAS.PatientBase and look for any previous addresses */
	
select
	Patient.SourcePatientNo
	,Patient.DistrictNo
	,AddressLine1 = Patient.AddressLine1
	,AddressLine2 = Patient.AddressLine2
	,AddressLine3 = Patient.AddressLine3
	,AddressLine4 = Patient.AddressLine4
	,PostCode = Patient.Postcode
	,null
	,EffectiveFromDate =
			coalesce(
				 dateadd(
						 day
						,1 -- add one day to ensure no overlap with previous EffectiveToDate
						,PreviousAddress.EffectiveToDate
					)
					,'01/01/1900'
				)
	,EffectiveToDate = null
	,1
from
	PAS.Patient
left outer join
	(
	select 
		SourcePatientNo
		,EffectiveToDate = max(EffectiveToDate)
	from
		PAS.PatientAddress
	group by
		SourcePatientNo
	) PreviousAddress
on	Patient.SourcePatientNo = PreviousAddress.SourcePatientNo
	
where
	Postcode is not null


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


SELECT @RowsInserted = @RowsInserted + @@Rowcount

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) +
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿
CREATE PROCEDURE [ETL].[LoadAPCFall]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, FallDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, FallDate)) 
from
	ETL.TLoadAPCFall

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.Fall target
using
	(
	select
		SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,FallDate
		,FallTime
		,InitialSeverityID
		,Validated
		,OccuredOnAnotherWard
		,WardCode
		,InterfaceCode
	from
		ETL.TLoadAPCFall
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	when not matched by source
	and	target.FallDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,FallDate
			,FallTime
			,InitialSeverityID
			,Validated
			,OccuredOnAnotherWard
			,WardCode
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.FallDate
			,source.FallTime
			,source.InitialSeverityID
			,source.Validated
			,source.OccuredOnAnotherWard
			,source.WardCode
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.FallDate, getdate()) = isnull(source.FallDate, getdate())
		and isnull(target.FallTime, getdate()) = isnull(source.FallTime, getdate())
		and isnull(target.InitialSeverityID, 0) = isnull(source.InitialSeverityID, 0)
		and isnull(target.Validated, 0) = isnull(source.Validated, 0)
		and isnull(target.OccuredOnAnotherWard, 0) = isnull(source.OccuredOnAnotherWard, 0)
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')


		)
	then
		update
		set
			target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.FallDate = source.FallDate
			,target.FallTime = source.FallTime
			,target.InitialSeverityID = source.InitialSeverityID
			,target.Validated = source.Validated
			,target.OccuredOnAnotherWard = source.OccuredOnAnotherWard
			,target.WardCode = source.WardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
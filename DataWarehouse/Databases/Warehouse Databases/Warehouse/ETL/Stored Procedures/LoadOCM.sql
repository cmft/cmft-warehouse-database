﻿CREATE procedure [ETL].[LoadOCM]


(
	@FromDate date = null
	,@ToDate date = null
	,@Update bit = null
	
)

as

select @FromDate = coalesce(@FromDate, (select cast(dateadd(month, -3, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

declare @StartTime datetime
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

select @Update = coalesce(@Update, 0)

-- for creatinine processes
--exec ETL.LoadOCMCodedResult 


truncate table ETL.TImportOCMCodedResult
exec ETL.ExtractInquireOCMCodedResult @FromDate, @ToDate, @Update
exec ETL.LoadOCMCodedResult @Update



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿
CREATE procedure [ETL].[ExtractInquirePreviousAddress]

as

truncate table ETL.TImportPASPreviousAddress

insert into ETL.TImportPASPreviousAddress

select
	[PREVADDRESSID]
	,[DistrictNumber]
	,[EffectiveFromDate]
	,[EffectiveFromDateInt]
	,[EffectiveToDate]
	,[EffectiveToDateInt]
	,[HaCode]
	,[InternalPatientNumber]
	,[Postcode]
	,[PseudoPostCode]
	,[PtAddLn1]
	,[PtAddLn2]
	,[PtAddLn3]
	,[PtAddLn4]
	,[SeqNo]
from
	[$(PAS)].Inquire.PREVADDRESS


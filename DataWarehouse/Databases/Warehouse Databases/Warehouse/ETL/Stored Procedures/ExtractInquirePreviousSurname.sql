﻿CREATE procedure [ETL].[ExtractInquirePreviousSurname] as

INSERT INTO ETL.[TImportPASPreviousSurname]
(
	 [DistrictNumber]
	,[Forenames]
	,[InternalDateOfBirth]
	,[InternalPatientNumber]
	,[PtPrevSexInt]
	,[SeqNo]
	,[Surname]
)

select
	 DistrictNumber
	,Forenames
	,InternalDateOfBirth
	,InternalPatientNumber
	,PtPrevSexInt
	,SeqNo
	,Surname
from
	[$(PAS)].Inquire.PREVSURNAME

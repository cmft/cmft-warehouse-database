﻿CREATE procedure [ETL].[ExtractInquireWaitingListEntry]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0


select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'



insert into		ETL.TImportWaitingListEntry

(
	 SourceUniqueID
	,ActivityTime
	,CasenoteNumber
	,ConsultantCode
	,DiagnosticGroupCode
	,DistrictNo
	,SourceEntityRecno
	,SourcePatientNo
	,IntendedPrimaryOperationCode
	,ManagementIntentionCode
	,SiteCode
	,AdmissionMethodCode
	,Operation
	,SpecialtyCode
	,WaitingListCode
	,ContractSerialNumber
	,PurchaserCode
	,EpisodicGpCode
	,InterfaceCode
)


Select
	 SourceUniqueID
	,ActivityTime
	,CasenoteNumber
	,ConsultantCode
	,DiagnosticGroupCode
	,DistrictNo
	,SourceEntityRecno
	,SourcePatientNo
	,IntendedPrimaryOperationCode
	,ManagementIntentionCode
	,SiteCode
	,AdmissionMethodCode
	,Operation
	,SpecialtyCode
	,WaitingListCode
	,ContractSerialNumber
	,PurchaserCode
	,EpisodicGpCode
	,InterfaceCode = 'INQ'

from
	(
	select
		 SourceUniqueID = WLENTRYID 
		,ActivityTime = EpsActvDtimeInt 
		,CasenoteNumber = CaseNoteNumber 
		,ConsultantCode = Consultant 
		,DiagnosticGroupCode = DiagGroup 
		,DistrictNo = DistrictNumber 
		,SourceEntityRecno = EpisodeNumber 
		,SourcePatientNo = InternalPatientNumber 
		,IntendedPrimaryOperationCode = IntendedProc 
		,ManagementIntentionCode = IntdMgmt 
		,SiteCode = Hospital 
		,AdmissionMethodCode = MethodOfAdm 
		,Operation 
		,SpecialtyCode = Specialty 
		,WaitingListCode = WlCode	
		,ContractSerialNumber = ContractID 
		,PurchaserCode = Purchaser 
		,EpisodicGpCode = EpiGPCode 
	from
		[$(PAS)].Inquire.WLENTRY
	where
		EpsActvDtimeInt between @from and @to
	) Activity



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireWaitingListEntry', @Stats, @StartTime

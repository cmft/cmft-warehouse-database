﻿CREATE    procedure [ETL].[ExtractInquireAPCWLCurrent]
	@CensusDate smalldatetime
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @KeyViolations Int


select @StartTime = getdate()

select @RowsInserted = 0


insert into ETL.TImportAPCWLCurrent
(
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,RegisteredGpCode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,AdmissionReason
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,KornerWait
	,CountOfDaysSuspended
	,ExpectedAdmissionDate
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,MRSA

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag

	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode
	
	,GuaranteedAdmissionDate

	,ShortNotice
)
select distinct
	 SourcePatientNo
	,SourceEncounterNo
	,@CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,RegisteredGpCode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,AdmissionReason
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,KornerWait
	,CountOfDaysSuspended
	,ExpectedAdmissionDate
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,MRSA

	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag

	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode

	,GuaranteedAdmissionDate

	,ShortNotice
from
	(
	SELECT
		WLCurrent.InternalPatientNumber SourcePatientNo, 
		WLCurrent.EpisodeNumber SourceEncounterNo, 
		Patient.Surname PatientSurname, 
		Patient.Forenames PatientForename, 
		Patient.Title PatientTitle, 
		Patient.Sex SexCode, 
		Patient.PtDoB DateOfBirth, 
		left(Patient.PtDateOfDeath, 10) DateOfDeath, 
		Patient.NHSNumber, 
		Patient.DistrictNumber DistrictNo, 
		Patient.MaritalStatus MaritalStatusCode, 
		Patient.Religion ReligionCode, 
		Patient.PtAddrPostCode Postcode, 
		Patient.GpCode RegisteredGpCode, 
		Patient.PtAddrLine1 PatientsAddress1, 
		Patient.PtAddrLine2 PatientsAddress2, 
		Patient.PtAddrLine3 PatientsAddress3, 
		Patient.PtAddrLine4 PatientsAddress4, 
		Patient.DistrictOfResidenceCode DHACode, 
		Patient.PtHomePhone HomePhone,
		Patient.PtWorkPhone WorkPhone,
		Patient.EthnicType EthnicOriginCode, 
		WLCurrent.Consultant ConsultantCode, 
		WLCurrent.Specialty SpecialtyCode, 
		WLCurrent.IntdMgmt ManagementIntentionCode, 
		WLCurrent.MethodOfAdm AdmissionMethodCode, 
		WLCurrent.Urgency PriorityCode, 
		WLCurrent.EpiGPCode EpisodicGpCode, 
		WLCurrent.EpiGPPractice EpisodicGpPracticeCode, 
		WLCurrent.WlCode WaitingListCode, 
		WLCurrent.CommentCl CommentClinical, 
		WLCurrent.CommentNonClin CommentNonClinical, 
		WLCurrent.IntendedProc IntendedPrimaryOperationCode, 
		WLCurrent.Operation, 
		WLCurrent.AdmReason AdmissionReason,
		WLCurrent.Hospital SiteCode, 
		WLCurrent.Ward WardCode, 
		case when WLActivity.Activity = 'WL Transfer' then WLActivity.PrevActivity else WLActivity.Activity end WLStatus, 
		WLCurrent.Provider ProviderCode, 
		WLCurrent.Purchaser PurchaserCode, 
		WLCurrent.ContractID ContractSerialNumber, 
		WLActivity.CancelledBy, 
		WLCurrent.BookingType BookingTypeCode, 
		WLCurrent.CaseNoteNumber CasenoteNumber, 
		WLCurrent.OriginalDateOnList OriginalDateOnWaitingList, 
		WLCurrent.KH07StartWait DateOnWaitingList, 
		WLCurrent.KH07Wait KornerWait, 
		WLCurrent.KH07SusDays CountOfDaysSuspended,
		WLCurrent.WLApproxAdmissionDate ExpectedAdmissionDate,
		Patient.NoKName NextOfKinName,
		Patient.NoKRelationship NextOfKinRelationship,
		Patient.NoKHomePhone NextOfKinHomePhone,
		Patient.NoKWorkPhone NextOfKinWorkPhone,
		WLCurrent.ExpectedLos ExpectedLOS,
		Patient.MRSA

		,left(PW.PathwayNumber , 20) RTTPathwayID
		,PW.PathwayCondition RTTPathwayCondition
		,PWP.RttStartDate RTTStartDate
		,PWP.RttEndDate RTTEndDate
		,PW.RttSpeciality RTTSpecialtyCode
		,PW.RttCurProv RTTCurrentProviderCode
		,PW.RttCurrentStatus RTTCurrentStatusCode
		,PWP.CurrentStatusDt RTTCurrentStatusDate
		,PW.RttPrivatePat RTTCurrentPrivatePatientFlag
		,PW.RttOsvStatus RTTOverseasStatusFlag

		,
		(
		select
			min(Latest.EpsActvDtimeInt) 
		from
			[$(PAS)].Inquire.WLACTIVITY Latest
		where
			Latest.EpisodeNumber = WLActivity.EpisodeNumber
		and	Latest.InternalPatientNumber = WLActivity.InternalPatientNumber
		) AddedToWaitingListTime

		,WLCurrent.WLCURRENTID SourceUniqueID
		,WLCurrent.Category AdminCategoryCode
		,WLCurrent.GuaranteedAdmissionDate

		,WLCurrent.ShortNotice

	FROM
		[$(PAS)].Inquire.PATDATA Patient
		
	inner join [$(PAS)].Inquire.WLCURRENT WLCurrent 
	on	Patient.InternalPatientNumber = WLCurrent.InternalPatientNumber

	inner join [$(PAS)].Inquire.WLACTIVITY WLActivity 
	on	WLCurrent.InternalPatientNumber = WLActivity.InternalPatientNumber
	and	WLCurrent.EpisodeNumber = WLActivity.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
	on	EPW.InternalPatientNumber = WLCurrent.InternalPatientNumber
	and	EPW.EpisodeNumber = WLCurrent.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTPATHWAY PW
	on	PW.InternalPatientNumber = EPW.InternalPatientNumber
	and	PW.PathwayNumber = EPW.PathwayNumber

	left join [$(PAS)].Inquire.RTTPERIOD PWP
	on	PWP.InternalPatientNumber = PW.InternalPatientNumber
	and	PWP.PathwayNumber = PW.PathwayNumber
	and	PWP.PPeriodId = '0'

	WHERE
		WLCurrent.EpsCurrActvStsInt In (1, 2)
	and	WLActivity.EpsActvDtimeInt = 
		(
		select
			max(Latest.EpsActvDtimeInt) 
		from
			[$(PAS)].Inquire.WLACTIVITY Latest
		where
			Latest.EpisodeNumber = WLActivity.EpisodeNumber
		and	Latest.InternalPatientNumber = WLActivity.InternalPatientNumber
		)

	) WLCurrent


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @KeyViolations =
(
	select count(*) from
		(
		select
			 SourcePatientNo
			,SourceEncounterNo
		from
			ETL.TImportAPCWLCurrent
		group by
			 SourcePatientNo
			,SourceEncounterNo
		having
			count(*) > 1
		) Records
)

-- if there are duplicates keep the snapshot in case the failed load cannot be dealt with before the next snapshot is generated
-- associated Preadmission and Suspension snapshots will also be kept (see their stored procedures)

-- this block is redundant now that the WLCurrent load removes duplicates
-- delete this bit when the load process is tested satisfactorily

--if @KeyViolations != 0
--begin
--	delete from TImportWLCurrentKeyViolation
--	where
--		exists
--			(
--			select
--				1
--			from
--				TImportWLCurrent
--			where
--				TImportWLCurrentKeyViolation.CensusDate = TImportWLCurrent.CensusDate
--			)
--	
--	insert into TImportWLCurrentKeyViolation
--
--	select * from TImportWLCurrent
--end

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Census Date ' + CONVERT(varchar(11), @CensusDate) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins' + ', ' +
	'Key Violations ' + CONVERT(varchar(6), @KeyViolations)

EXEC Utility.WriteAuditLogEvent 'ExtractInquireAPCWLCurrent', @Stats, @StartTime

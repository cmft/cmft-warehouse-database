﻿

CREATE PROCEDURE [ETL].[LoadPCCActivity]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@xmlData xml;
	

set @xmlData = 
	(
	select 
		xmlData 
	from 
		[$(SmallDatasets)].PICANet.BaseExtract 
	where 
		ExtractRecno =	
			(
			select 
				max(ExtractRecno) 
			from 
				[$(SmallDatasets)].PICANet.BaseExtract
			)	-- *** Does this need changing, maybe to a cursor in case more than one extract file needs processing? ***
	)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	@LoadStartDate = MIN(cast(T.Episode.value('(admissionDate)[1]', 'varchar(10)') + ' ' + T.Episode.value('(admissionTime)[1]', 'varchar(8)') as date))
	,@LoadEndDate = max(cast(T.Episode.value('(admissionDate)[1]', 'varchar(10)') + ' ' + T.Episode.value('(admissionTime)[1]', 'varchar(8)') as date))
from
	@xmlData.nodes('picanetPccmdsExport/episode') T(Episode)

select @LoadEndDate = @LoadEndDate + '23:59'


/* ============================================ Shred XML Activity into temp table ================================================ */

select
	 EventID = E.Episode.value('(EventID)[1]', 'int')
	 ,AdmissionDate = E.Episode.value('(admissionDate)[1]', 'varchar(10)')
	,ActivityDate = A.Activity.value('(activityDate)[1]', 'date')
	,ActivityCode = AC.ActivityCode.value('(.)[1]', 'varchar(20)')
	,CreatedTemp = getdate()
	,ByWhomTemp = suser_name()
into 
	#PICANetExtractETLActivityShreddedXML
from
	@xmlData.nodes('picanetPccmdsExport/episode') E(Episode)

	cross apply E.Episode.nodes('criticalCareActivities/criticalCareActivity') A(Activity)
	
	cross apply A.Activity.nodes('ActivityCode') AC(ActivityCode)

declare
	 @deleted int
	,@inserted int
	,@updated int
	
declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	PCC.Activity target
using
	(
	select
		 PeriodID = EventID
		,AdmissionDate = cast(AdmissionDate as date)
		,ActivityDate
		,ActivityCode
		,ActivitySequence = row_number() over(partition by EventID, ActivityDate order by ActivityCode)
		,Created = getdate()
		,Updated = getdate()
		,ByWhom = suser_name()
	from 
		#PICANetExtractETLActivityShreddedXML

	where
		isnumeric(ActivityCode) = 1	
	) source
	on	source.PeriodID = target.PeriodID
	and source.ActivityDate = target.ActivityDate
	and source.ActivityCode = target.ActivityCode
	
	when not matched by source
	and	target.AdmissionDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 PeriodID 
			,AdmissionDate
			,ActivityDate
			,ActivityCode
			,ActivitySequence 
			,Created 
			,Updated 
			,ByWhom 
			)
		values
			(
			 source.PeriodID 
			,source.AdmissionDate
			,source.ActivityDate
			,source.ActivityCode
			,source.ActivitySequence 
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.PeriodID, 0) = isnull(source.PeriodID, 0)
		and isnull(target.AdmissionDate, getdate()) = isnull(source.AdmissionDate, getdate())
		and isnull(target.ActivityDate, getdate()) = isnull(source.ActivityDate, getdate())
		and isnull(target.ActivityCode, 0) = isnull(source.ActivityCode, 0)
		and isnull(target.ActivitySequence, 0) = isnull(source.ActivitySequence, 0)
		)
	then
		update
		set
			target.PeriodID = source.PeriodID
			,target.AdmissionDate = source.AdmissionDate
			,target.ActivityDate = source.ActivityDate
			,target.ActivityCode = source.ActivityCode
			,target.ActivitySequence = source.ActivitySequence
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
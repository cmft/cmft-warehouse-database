﻿
CREATE PROCEDURE [ETL].[LoadSymphonyReferenceLookupMapping]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony ReferenceLookupMapping table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ReferenceLookupMappingID
			,ReferenceLookupID
			,ReferenceMappingTypeID
			,ValueID
			,ReferenceLookupMapping
			,CreatedByID
			,CreatedTime
			,MappedType
			)
into
	#TLoadSymphonyReferenceLookupMapping
from
	(
	select
		 ReferenceLookupMappingID = cast(ReferenceLookupMappingID as int)
		,ReferenceLookupID = cast(ReferenceLookupID as int)
		,ReferenceMappingTypeID = cast(ReferenceMappingTypeID as int)
		,ValueID = cast(ValueID as int)
		,ReferenceLookupMapping = cast(ReferenceLookupMapping as varchar(200))
		,CreatedByID = cast(CreatedByID as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,MappedType = cast(MappedType as tinyint)
	from
		(
		select
			 ReferenceLookupMappingID = LookupMappings.flm_id
			,ReferenceLookupID = LookupMappings.flm_lkpid
			,ReferenceMappingTypeID = LookupMappings.flm_mtid
			,ValueID = LookupMappings.flm_value
			,ReferenceLookupMapping = LookupMappings.flm_description
			,CreatedByID = LookupMappings.flm_createdby
			,CreatedTime = LookupMappings.flm_datecreated
			,MappedType = LookupMappings.flm_MappedType
		from
			[$(Symphony)].dbo.LookupMappings


		) Encounter
	) Encounter
order by
	Encounter.ReferenceLookupMappingID

create unique clustered index #IX_TLoadSymphonyReferenceLookupMapping on #TLoadSymphonyReferenceLookupMapping
	(
	ReferenceLookupMappingID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.ReferenceLookupMapping target
using
	(
	select
		 ReferenceLookupMappingID
		,ReferenceLookupID
		,ReferenceMappingTypeID
		,ValueID
		,ReferenceLookupMapping
		,CreatedByID
		,CreatedTime
		,MappedType
		,EncounterChecksum
	from
		#TLoadSymphonyReferenceLookupMapping
	
	) source
	on	source.ReferenceLookupMappingID = target.ReferenceLookupMappingID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 ReferenceLookupMappingID
			,ReferenceLookupID
			,ReferenceMappingTypeID
			,ValueID
			,ReferenceLookupMapping
			,CreatedByID
			,CreatedTime
			,MappedType

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.ReferenceLookupMappingID
			,source.ReferenceLookupID
			,source.ReferenceMappingTypeID
			,source.ValueID
			,source.ReferenceLookupMapping
			,source.CreatedByID
			,source.CreatedTime
			,source.MappedType
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.ReferenceLookupMappingID = source.ReferenceLookupMappingID
			,target.ReferenceLookupID = source.ReferenceLookupID
			,target.ReferenceMappingTypeID = source.ReferenceMappingTypeID
			,target.ValueID = source.ValueID
			,target.ReferenceLookupMapping = source.ReferenceLookupMapping
			,target.CreatedByID = source.CreatedByID
			,target.CreatedTime = source.CreatedTime
			,target.MappedType = source.MappedType
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
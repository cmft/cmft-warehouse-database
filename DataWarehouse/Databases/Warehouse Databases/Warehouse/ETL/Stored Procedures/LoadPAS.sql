﻿


CREATE procedure [ETL].[LoadPAS] 

(
	@FromDate datetime = null
	,@ToDate datetime = null
	,@Update bit = null
)

as

/****************************************************************************************
	Stored procedure : [ETL].[LoadPAS]
	Description		 : As proc name

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Initial Coding
	20140815	Paul Egan       Added Special Register ETL
	20150512	Paul Egan		Added AEAttendance ETL
*****************************************************************************************/


select @FromDate = coalesce(@FromDate, cast(dateadd(month, -12, getdate()) as date))
select @ToDate = coalesce(@ToDate, cast(getdate() as date))
select @Update = coalesce(@Update, 0)


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()
	
truncate table ETL.TImportPASPatient
exec ETL.ExtractInquirePASPatient @Update
exec ETL.LoadPASPatient @Update

--exec ETL.LoadPASReferenceData

exec ETL.LoadPASCasenote
exec ETL.LoadPASCasenoteLoan

exec ETL.LoadPASPatientDuplication


/*	Added Paul Egan 20140815 */
truncate table ETL.TImportPASPatientSpecialRegister;
exec ETL.ExtractInquirePatientSpecialRegister @FromDate, @ToDate;
exec ETL.LoadPASPatientSpecialRegister;


/* Added Paul Egan 20150512 */
truncate table ETL.TImportPASAEAttendance;
exec ETL.ExtractInquireAEAttendance @FromDate, @ToDate;
exec ETL.LoadPASAEAttendance;



update Utility.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADPASDATE'

if @@rowcount = 0

insert into Utility.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADPASDATE'
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'LoadPAS', @Stats, @StartTime




﻿
CREATE PROCEDURE ETL.LoadAPCVTECondition

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, DiagnosisDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, DiagnosisDate)) 
from
	ETL.TLoadAPCVTECondition

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.VTECondition target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,DiagnosisDate
		,DiagnosisTime
		,VTETypeID
		,OccuredOnAnotherWard
		,MedicationRequired
		,MedicationStartDate
		,MedicationStartTime
		,WardCode
		,InterfaceCode
	from
		ETL.TLoadAPCVTECondition
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	when not matched by source

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,DiagnosisDate
			,DiagnosisTime
			,VTETypeID
			,OccuredOnAnotherWard
			,MedicationRequired
			,MedicationStartDate
			,MedicationStartTime
			,WardCode
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.DiagnosisDate
			,source.DiagnosisTime
			,source.VTETypeID
			,source.OccuredOnAnotherWard
			,source.MedicationRequired
			,source.MedicationStartDate
			,source.MedicationStartTime
			,source.WardCode
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.DiagnosisDate, getdate()) = isnull(source.DiagnosisDate, getdate())
		and isnull(target.DiagnosisTime, getdate()) = isnull(source.DiagnosisTime, getdate())
		and isnull(target.VTETypeID, 0) = isnull(source.VTETypeID, 0)
		and isnull(target.OccuredOnAnotherWard, 0) = isnull(source.OccuredOnAnotherWard, 0)
		and isnull(target.MedicationRequired, 0) = isnull(source.MedicationRequired, 0)
		and isnull(target.MedicationStartDate, getdate()) = isnull(source.MedicationStartDate, getdate())
		and isnull(target.MedicationStartTime, getdate()) = isnull(source.MedicationStartTime, getdate())
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')

		)
	then
		update
		set
			target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.DiagnosisDate = source.DiagnosisDate
			,target.DiagnosisTime = source.DiagnosisTime
			,target.VTETypeID = source.VTETypeID
			,target.OccuredOnAnotherWard = source.OccuredOnAnotherWard
			,target.MedicationRequired = source.MedicationRequired
			,target.MedicationStartDate = source.MedicationStartDate
			,target.MedicationStartTime = source.MedicationStartTime
			,target.WardCode = source.WardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
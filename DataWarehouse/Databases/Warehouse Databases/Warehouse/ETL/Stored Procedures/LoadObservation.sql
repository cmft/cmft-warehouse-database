﻿
CREATE proc [ETL].[LoadObservation]

	 @FromDate datetime = null
	,@ToDate datetime = null
as


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()


select @FromDate = coalesce(@FromDate, dateadd(hour, -48, getdate()))
select @ToDate = coalesce(@ToDate, getdate())

exec ETL.LoadObservationReferenceData

truncate table ETL.TImportObservationAdmission
exec ETL.ExtractPatientrackObservationAdmission @FromDate, @ToDate
exec ETL.LoadObservationAdmission

truncate table ETL.TImportObservationObservation
exec ETL.ExtractPatientrackObservationObservation @FromDate, @ToDate
exec ETL.LoadObservationObservation

truncate table ETL.TImportObservationObservationSet
exec ETL.ExtractPatientrackObservationObservationSet  @FromDate, @ToDate
exec ETL.LoadObservationObservationSet

truncate table ETL.TImportObservationAlert
exec ETL.ExtractPatientrackObservationAlert  @FromDate, @ToDate
exec ETL.LoadObservationAlert

truncate table ETL.TImportObservationPatientFlag
exec ETL.ExtractPatientrackObservationPatientFlag  @FromDate, @ToDate
exec ETL.LoadObservationPatientFlag

exec ETL.LoadObservationSignificantEvent

truncate table ETL.TImportObservationObservationProfile
exec ETL.ExtractPatientrackObservationObservationProfile  @FromDate, @ToDate
exec ETL.LoadObservationObservationProfile

exec ETL.AssignObservationEWSChain

exec ETL.BuildAPCEncounterObservationSet
exec ETL.BuildAPCEncounterAlert
exec ETL.BuildAEEncounterObservationSet


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


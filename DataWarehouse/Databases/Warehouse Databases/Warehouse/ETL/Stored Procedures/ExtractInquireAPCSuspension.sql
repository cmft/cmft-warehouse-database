﻿CREATE procedure [ETL].[ExtractInquireAPCSuspension]
	 @fromDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @sql varchar(8000)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112) + '2400'

select @census = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, -1, getdate())), 0), 112)


insert into ETL.TImportAPCSuspension
(
	SourcePatientNo, 
	SourceEncounterNo, 
	SuspensionStartDate,
	SuspensionEndDate,
	SuspensionReasonCode,
	SuspensionReason
)


select
	 SourcePatientNo = Suspend.InternalPatientNumber 
	,SourceEncounterNo = Suspend.EpisodeNumber 
	,SuspensionStartDate = Suspend.SuspensionStartDate 
	,SuspensionEndDate = Suspend.SuspendedUntil_1 
	,SuspensionReasonCode = Suspend.SuspReason 
	,SuspensionReason = Suspend.Reason 
from
	[$(PAS)].Inquire.FCEEXT FCE

INNER JOIN [$(PAS)].Inquire.WLSUSPENSION Suspend
ON	FCE.EpisodeNumber = Suspend.EpisodeNumber
AND	FCE.InternalPatientNumber = Suspend.InternalPatientNumber

WHERE
	FCE.EpsActvDtimeInt between @from and @to


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT


-- open episodes

insert into ETL.TImportAPCSuspension
(
	SourcePatientNo, 
	SourceEncounterNo, 
	SuspensionStartDate,
	SuspensionEndDate,
	SuspensionReasonCode,
	SuspensionReason
)

select
	 SourcePatientNo = Suspend.InternalPatientNumber 
	,SourceEncounterNo = Suspend.EpisodeNumber 
	,SuspensionStartDate = Suspend.SuspensionStartDate 
	,SuspensionEndDate = Suspend.SuspendedUntil_1 
	,SuspensionReasonCode = Suspend.SuspReason 
	,SuspensionReason = Suspend.Reason 
from
	[$(PAS)].Inquire.MIDNIGHTBEDSTATE M

INNER JOIN [$(PAS)].Inquire.WLSUSPENSION Suspend
ON	M.EpisodeNumber = Suspend.EpisodeNumber
AND	M.InternalPatientNumber = Suspend.InternalPatientNumber

INNER JOIN [$(PAS)].Inquire.CONSEPISODE  Episode 
ON	M.EpisodeNumber = Episode.EpisodeNumber
AND	M.InternalPatientNumber = Episode.InternalPatientNumber

WHERE
	Episode.EpsActvDtimeInt <= @to 
AND	(
		Episode.ConsultantEpisodeEndDttm > @to
	or	Episode.ConsultantEpisodeEndDttm Is Null
	)
AND	M.StatisticsDate = @census


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireAPCSuspension', @Stats, @StartTime

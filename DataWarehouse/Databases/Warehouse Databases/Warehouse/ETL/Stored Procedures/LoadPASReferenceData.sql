﻿






CREATE procedure [ETL].[LoadPASReferenceData] as

/****************************************************************************************
	Stored procedure : ETL.LoadPASReferenceData
	Description		 : As proc name.

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Initial Coding
	14/08/2014	Paul Egan		Added Special Register master file for CPE reporting requirements.
	05/01/2015	Paul Egan		Populate LocationBase table. 
*****************************************************************************************/

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


set dateformat dmy


--Site
insert into PAS.Site
(
	 SiteCode
	,Site
	,MappedSiteCode
)
select
	 SiteCode
	,Site
	,MappedSiteCode = '#'
from
	(
	select
		 HOSPDETID SiteCode
		,HospitalName Site
	from
		[$(PAS)].Inquire.HOSPDET
	) NewSite
where
	not exists
	(
	select
		1
	from
		PAS.Site
	where
		Site.SiteCode = NewSite.SiteCode
	)


--Gp
delete from PAS.GpBase

insert into PAS.GpBase
(
	GPID, Address, DateJoined, DateJoinedPractice, DateLeft, DateLeftPractice, DohCode, Fax, GeneralPractitionerDateJoinedFhSchemeInt, GeneralPractitionerDateJoinedGpPracticeInter, GeneralPractitionerDateLeftFhSchemeInternal, GeneralPractitionerDateLeftGpPracticeInterna, GpAddrLine2, GpAddrLine3, GpAddrLine4, GpAddrOnOneLine, GpCode, GpName, GpName_1, GpName_2, GpfhCode, HaaCode, Initials, MfRecStsInd, NationalCode, PathologyGpCode, Phone, Postcode, PracticeCode, Surname, Title
)
select
	GPID, Address, DateJoined, DateJoinedPractice, DateLeft, DateLeftPractice, DohCode, Fax, GeneralPractitionerDateJoinedFhSchemeInt, GeneralPractitionerDateJoinedGpPracticeInter, GeneralPractitionerDateLeftFhSchemeInternal, GeneralPractitionerDateLeftGpPracticeInterna, GpAddrLine2, GpAddrLine3, GpAddrLine4, GpAddrOnOneLine, GpCode, GpName, GpName_1, GpName_2, GpfhCode, HaaCode, Initials, ltrim(rtrim(MfRecStsInd)), NationalCode, PathologyGpCode, Phone, Postcode, PracticeCode, Surname, Title
from
	(
	select
		GPID, Address, DateJoined, DateJoinedPractice, DateLeft, DateLeftPractice, DohCode, Fax, GeneralPractitionerDateJoinedFhSchemeInt, GeneralPractitionerDateJoinedGpPracticeInter, GeneralPractitionerDateLeftFhSchemeInternal, GeneralPractitionerDateLeftGpPracticeInterna, GpAddrLine2, GpAddrLine3, GpAddrLine4, GpAddrOnOneLine, GpCode, GpName, GpName_1, GpName_2, GpfhCode, HaaCode, Initials, left(MfRecStsInd, 2) MfRecStsInd, left(NationalCode , 8) NationalCode, PathologyGpCode, Phone, Postcode, PracticeCode, Surname, Title
	from
		[$(PAS)].Inquire.GP
	) Gp



--Gdp
TRUNCATE TABLE PAS.GdpBase

insert into PAS.GdpBase
(
	 GDPID
	,Address
	,DpbContractCode
	,GdpAddLine2
	,GdpAddrLine3
	,GdpAddrLine4
	,GdpAddrOnOneLine
	,GdpCode
	,Initials
	,MfRecStsInd
	,Name
	,Phone
	,Postcode
	,Surname
	,Title
)
select
	 GDPID
	,Address
	,DpbContractCode
	,GdpAddLine2
	,GdpAddrLine3
	,GdpAddrLine4
	,GdpAddrOnOneLine
	,GdpCode
	,Initials
	,MfRecStsInd
	,Name
	,Phone
	,Postcode
	,Surname
	,Title
from
(
select
	 GDPID
	,Address
	,DpbContractCode
	,GdpAddLine2
	,GdpAddrLine3
	,GdpAddrLine4
	,GdpAddrOnOneLine
	,GdpCode
	,Initials
	,MfRecStsInd
	,Name
	,Phone
	,Postcode
	,Surname
	,Title
FROM 
	[$(PAS)].Inquire.GDP
) Gdp



--Provider
TRUNCATE TABLE PAS.ProviderBase

insert into PAS.ProviderBase
(
	 PROVID
	,CmmCommissioningMainCommissionerDescription
	,CmmCommissioningMainCommissionerPcgDesc
	,CmmTypeOfProviderDescription
	,Description
	,DohCode
	,MainCommissionerHa
	,MainCommissionerPcg
	,MfRecStsInd
	,OrganisationCode
	,ProvType
	,ProviderCode
)
select
	 PROVID
	,CmmCommissioningMainCommissionerDescription
	,CmmCommissioningMainCommissionerPcgDesc
	,CmmTypeOfProviderDescription
	,Description
	,DohCode
	,MainCommissionerHa
	,MainCommissionerPcg
	,MfRecStsInd
	,OrganisationCode
	,ProvType
	,ProviderCode
from
(
select
	 PROVID
	,CmmCommissioningMainCommissionerDescription
	,CmmCommissioningMainCommissionerPcgDesc
	,CmmTypeOfProviderDescription
	,Description
	,DohCode
	,MainCommissionerHa
	,MainCommissionerPcg
	,MfRecStsInd
	,OrganisationCode
	,ProvType
	,ProviderCode
FROM 
	[$(PAS)].Inquire.PROV
) Prov


--Specialty
delete from PAS.SpecialtyBase

insert into PAS.SpecialtyBase
(
	SpecialtyCode, CmtGpfhExemptSpecialtyInt, Description, DohCode, GpfhExempt, HaaCode, KarsIdCode, MfRecStsInd, ScottishIntValue, ScottishLocalCode, SpecGroup, Specialty, SpecialtyType, SupraregCode, TreatmentFunction
)
select
	SpecialtyCode, CmtGpfhExemptSpecialtyInt, Description, DohCode, GpfhExempt, HaaCode, KarsIdCode, MfRecStsInd, ScottishIntValue, ScottishLocalCode, SpecGroup, Specialty, SpecialtyType, SupraregCode, TreatmentFunction
from
	(
	select
		SPECID SpecialtyCode, CmtGpfhExemptSpecialtyInt, Description, left(DohCode, 4) DohCode, GpfhExempt, HaaCode, KarsIdCode, left(MfRecStsInd, 2) MfRecStsInd, ScottishIntValue, ScottishLocalCode, SpecGroup, Specialty, SpecialtyType, SupraregCode, TreatmentFunction
	from
		[$(PAS)].Inquire.SPEC
	) Spec


----add any new specialties to the national Specialty reference table
--insert into WH.Specialty
--(
--	 SpecialtyCode
--	,Specialty
--	,DirectorateCode
--	,ALSDirectorateCode
--)
--select distinct
--	 PASPennine.NationalSpecialtyCode
--	,max(PASPennine.Specialty)
--	,DirectorateCode = '12' --other
--	,ALSDirectorateCode = '12' --other
--from
--	PAS.Specialty PASPennine
--where
--	not exists
--	(
--	select
--		1
--	from
--		WH.Specialty Specialty
--	where
--		Specialty.SpecialtyCode = PASPennine.NationalSpecialtyCode
--	)
--group by
--	PASPennine.NationalSpecialtyCode


----add any new specialties to the local SpecialtyPennine reference table
--insert into WH.SpecialtyPennine
--(
--	 SpecialtyCode
--	,Specialty
--	,NationalSpecialtyCode
--)
--select
--	 SpecialtyCode
--	,Specialty
--	,NationalSpecialtyCode
--from
--	PAS.Specialty
--where
--	not exists
--	(
--	select
--		1
--	from
--		WH.SpecialtyPennine
--	where
--		SpecialtyPennine.SpecialtyCode = Specialty.SpecialtyCode
--	)


--Consultant
--delete from PAS.ConsultantBase

--insert into PAS.ConsultantBase
--(
--	CONSMASTID, Consultant, ConsultantCode2, ContractedSpecialty, Deleted, ExternalCons, ForenamePfx, GMCCode, Inits, KornerCode, Midwife, Name, PrimarySpecialty, ProviderCode, Surname
--)
--select
--	CONSMASTID, Consultant, ConsultantCode2, ContractedSpecialty, ltrim(rtrim(Deleted)), ExternalCons, ForenamePfx, GMCCode, Inits, KornerCode, Midwife, Name, PrimarySpecialty, ProviderCode, Surname
--from
--	(
--	select
--		CONSMASTID, Consultant, ConsultantCode2, ContractedSpecialty, left(Deleted, 2) Deleted, ExternalCons, left(ForenamePfx, 4) ForenamePfx, GMCCode, Inits, KornerCode, Midwife, Name, PrimarySpecialty, ProviderCode, Surname
--	from
--		[$(PAS)].Inquire.CONSMAST
--	) ConsMast


merge
	PAS.ConsultantBase target
using
	(
	select
		CONSMASTID
		,Consultant
		,ConsultantCode2
		,ContractedSpecialty
		,Deleted
		,ExternalCons
		,ForenamePfx
		,GMCCode
		,Inits
		,KornerCode
		,Midwife
		,Name
		,PrimarySpecialty
		,ProviderCode
		,Surname
	from
		[$(PAS)].Inquire.CONSMAST
	) source
	on	source.CONSMASTID = target.CONSMASTID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			CONSMASTID
			,Consultant
			,ConsultantCode2
			,ContractedSpecialty
			,Deleted
			,ExternalCons
			,ForenamePfx
			,GMCCode
			,Inits
			,KornerCode
			,Midwife
			,Name
			,PrimarySpecialty
			,ProviderCode
			,Surname
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.CONSMASTID
			,source.Consultant
			,source.ConsultantCode2
			,source.ContractedSpecialty
			,source.Deleted
			,source.ExternalCons
			,source.ForenamePfx
			,source.GMCCode
			,source.Inits
			,source.KornerCode
			,source.Midwife
			,source.Name
			,source.PrimarySpecialty
			,source.ProviderCode
			,source.Surname
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.CONSMASTID, '') = isnull(source.CONSMASTID, '')
		and isnull(target.Consultant, '') = isnull(source.Consultant, '')
		and isnull(target.ConsultantCode2, '') = isnull(source.ConsultantCode2, '')
		and isnull(target.ContractedSpecialty, '') = isnull(source.ContractedSpecialty, '')
		and isnull(target.Deleted, 0) = isnull(source.Deleted, 0)
		and isnull(target.ExternalCons, '') = isnull(source.ExternalCons, '')
		and isnull(target.ForenamePfx, '') = isnull(source.ForenamePfx, '')
		and isnull(target.GMCCode, '') = isnull(source.GMCCode, '')
		and isnull(target.Inits, '') = isnull(source.Inits, '')
		and isnull(target.KornerCode, '') = isnull(source.KornerCode, '')
		and isnull(target.Midwife, '') = isnull(source.Midwife, '')
		and isnull(target.Name, '') = isnull(source.Name, '')
		and isnull(target.PrimarySpecialty, '') = isnull(source.PrimarySpecialty, '')
		and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')

		)
	then
		update
		set
			target.CONSMASTID = source.CONSMASTID
			,target.Consultant = source.Consultant
			,target.ConsultantCode2 = source.ConsultantCode2
			,target.ContractedSpecialty = source.ContractedSpecialty
			,target.Deleted = source.Deleted
			,target.ExternalCons = source.ExternalCons
			,target.ForenamePfx = source.ForenamePfx
			,target.GMCCode = source.GMCCode
			,target.Inits = source.Inits
			,target.KornerCode = source.KornerCode
			,target.Midwife = source.Midwife
			,target.Name = source.Name	
			,target.PrimarySpecialty = source.PrimarySpecialty
			,target.ProviderCode = source.ProviderCode
			,target.Surname = source.Surname
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
;

--add any new consultants to the national Consultant reference table
insert into WH.Consultant
(
	 ConsultantCode
	,Title
	,Forename
	,Surname
)
select
	 NewConsultant.NationalConsultantCode
	,NewConsultant.Title
	,NewConsultant.Initials
	,NewConsultant.Surname
from
	PAS.Consultant NewConsultant
where
	not exists
	(
	select
		1
	from
		WH.Consultant Consultant
	where
		Consultant.ConsultantCode = NewConsultant.NationalConsultantCode
	)
and	NewConsultant.NationalConsultantCode is not null
and	NewConsultant.Deleted = 0
and	NewConsultant.Surname not like '%DO NOT USE%'

and	not exists
	(
	select
		1
	from
		PAS.Consultant Consultant
	where
		Consultant.NationalConsultantCode = NewConsultant.NationalConsultantCode
	and	Consultant.NationalConsultantCode is not null
	and	Consultant.Deleted = 0
	and	Consultant.Surname not like '%DO NOT USE%'
	and	Consultant.ConsultantCode > NewConsultant.ConsultantCode
	)


--SourceOfReferral
delete from PAS.SourceOfReferralBase

insert into PAS.SourceOfReferralBase
(
	OPREFSOURCEID, Applications, CmdsValue, CollectGpRefStatsInternal, CollectGpReferralStatisticsWithThisCode, Description, InternalValue, MfRecStsInd, Modules, ReferredBy, ScottishIntValue
	,ReportableFlag
	,NewFlag
)
select
	OPREFSOURCEID, Applications, CmdsValue, CollectGpRefStatsInternal, CollectGpReferralStatisticsWithThisCode, SourceOfReferralBase.Description, InternalValue, ltrim(rtrim(MfRecStsInd)), Modules, ReferredBy, ScottishIntValue
	,case when ExcludedSourceOfReferral.Description is null then 1 else 0 end ReportableFlag
	,case when NewSourceOfReferral.Description is null then 0 else 1 end NewFlag
from
	(
	select
		 OPREFSOURCEID
		,Applications
		,CmdsValue
		,CollectGpRefStatsInternal
		,CollectGpReferralStatisticsWithThisCode
		,Description
		,InternalValue
		,left(MfRecStsInd, 2) MfRecStsInd
		,left(Modules, 3) Modules
		,ReferredBy
		,ScottishIntValue
	from
		[$(PAS)].Inquire.OPREFSOURCE
	) SourceOfReferralBase

left join dbo.EntityLookup ExcludedSourceOfReferral
on	ExcludedSourceOfReferral.EntityCode = SourceOfReferralBase.OPREFSOURCEID
and	ExcludedSourceOfReferral.EntityTypeCode = 'EXCLUDEDSOURCEOFREFERRAL'

left join dbo.EntityLookup NewSourceOfReferral
on	NewSourceOfReferral.EntityCode = SourceOfReferralBase.OPREFSOURCEID
and	NewSourceOfReferral.EntityTypeCode = 'NEWSOURCEOFREFERRAL'


--Admission Source
insert into
	PAS.AdmissionSourceBase
(
	 SOADID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,SourceOfAdm
)
select
	 SOADID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,SourceOfAdm
from
	(
	select
		 SOADID
		,Description
		,FullDescription
		,HaaCode
		,InternalValue
		,left(MfRecStsInd, 2) MfRecStsInd
		,ScottishIntValue
		,SourceOfAdm
	from
		[$(PAS)].Inquire.SOAD
	) NewAdmissionSourceBase
where
	not exists
		(
		select
			1
		from
			PAS.AdmissionSourceBase
		where
			AdmissionSourceBase.SOADID = NewAdmissionSourceBase.SOADID
		)



--Admission Method
insert into
	PAS.AdmissionMethodBase
(
	 MOAID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfAdmission
)
select
	 MOAID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfAdmission
from
	(
	select
		 MOAID
		,Description
		,FullDescription
		,HaaCode
		,InternalValue
		,left(MfRecStsInd, 2) MfRecStsInd
		,ScottishIntValue
		,MethodOfAdmission
	from
		[$(PAS)].Inquire.MOA
	) NewAdmissionMethodBase
where
	not exists
		(
		select
			1
		from
			PAS.AdmissionMethodBase
		where
			AdmissionMethodBase.MOAID = NewAdmissionMethodBase.MOAID
		)


--Discharge Destination
insert into
	PAS.DischargeDestinationBase
(
	 DODID
	,Description
	,FullDescription
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,DestinationCode
)
select
	 DODID
	,Description
	,FullDescription
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,DestinationCode
from
	(
	select
		 DODID
		,Description
		,FullDescription
		,InternalValue
		,left(MfRecStsInd, 2) MfRecStsInd
		,ScottishIntValue
		,DestinationCode
	from
		[$(PAS)].Inquire.DOD
	) NewDischargeDestinationBase
where
	not exists
		(
		select
			1
		from
			PAS.DischargeDestinationBase
		where
			DischargeDestinationBase.DODID = NewDischargeDestinationBase.DODID
		)


--Discharge Method
insert into
	PAS.DischargeMethodBase
(
	 MODID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfDischarge
)
select
	 MODID
	,Description
	,FullDescription
	,HaaCode = case when HaaCode = 7 then 4 else coalesce(HaaCode, InternalValue) end
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfDischarge
from
	(
	select
		 MODID
		,Description
		,FullDescription
		,HaaCode
		,InternalValue
		,left(MfRecStsInd, 2) MfRecStsInd
		,ScottishIntValue
		,MethodOfDischarge
	from
		[$(PAS)].Inquire.MOD
	) NewDischargeMethodBase
where
	not exists
		(
		select
			1
		from
			PAS.DischargeMethodBase
		where
			DischargeMethodBase.MODID = NewDischargeMethodBase.MODID
		)



--PAS.OperationBase
delete from PAS.OperationBase

insert into PAS.OperationBase
	(
	 OPCS4ID
	,Opcs4Dx3
	,Opcs4Dx4
	,Opcs4GpfhChargeable
	,Opcs4GpfhEffDate
	,Opcs4Groupcode
	,Opcs4Ldf
	,Opcs4Procpricegroup
	,RevisionFourOperationCode
	)
select
	 OPCS4ID
	,Opcs4Dx3
	,Opcs4Dx4
	,Opcs4GpfhChargeable
	,Opcs4GpfhEffDate
	,Opcs4Groupcode
	,Opcs4Ldf
	,Opcs4Procpricegroup
	,RevisionFourOperationCode
from
	(
	select
		 OPCS4ID
		,Opcs4Dx3
		,Opcs4Dx4
		,Opcs4GpfhChargeable
		,Opcs4GpfhEffDate
		,Opcs4Groupcode
		,left(Opcs4Ldf, 2) Opcs4Ldf
		,Opcs4Procpricegroup
		,RevisionFourOperationCode
	from
		[$(PAS)].Inquire.OPCS4
	) Opcs4


--PAS.DiagnosisBase
delete from PAS.DiagnosisBase

insert into PAS.DiagnosisBase
(
	 ICD10ID
	,A5thDigitLoMax
	,A5thDigitUpMin
	,AccidentFlag
	,AccidentPrompt
	,DaggerAsterisk
	,Deleted
	,DeliveryMethod
	,Description
	,EpisodeCompleteCheck
	,FourDigitName
	,Icd10Code
	,MaxAge
	,MaxBirthWeight
	,MaxDurationStay
	,MethodAdmissionFlag
	,MethodOfOnset
	,MinAge
	,MinBirthWeight
	,NumberOfBabies
	,Parity
	,PrimCondFlag
	,PrimCondPrompt
	,RareDeathFlag
	,RareDeathPrompt
	,RareDiagFlag
	,RareDiagPrompt
	,SexValidationFlag
	,SpecialtyFunction
	,StillBirthInd
	,StillBirthIndPrompt
	,ThreeDigitName
	,ValidForCancerReg
	,ValidForCancerRegExt
)

select
	 ICD10ID
	,A5thDigitLoMax
	,A5thDigitUpMin
	,AccidentFlag
	,AccidentPrompt
	,DaggerAsterisk
	,left(Deleted , 2) Deleted
	,DeliveryMethod
	,Description
	,EpisodeCompleteCheck
	,left(FourDigitName, 255) FourDigitName
	,Icd10Code
	,MaxAge
	,MaxBirthWeight
	,MaxDurationStay
	,MethodAdmissionFlag
	,MethodOfOnset
	,MinAge
	,MinBirthWeight
	,NumberOfBabies
	,Parity
	,PrimCondFlag
	,PrimCondPrompt
	,RareDeathFlag
	,RareDeathPrompt
	,RareDiagFlag
	,RareDiagPrompt
	,SexValidationFlag
	,SpecialtyFunction
	,StillBirthInd
	,StillBirthIndPrompt
	,ThreeDigitName
	,ValidForCancerReg
	,ValidForCancerRegExt
from
	[$(PAS)].Inquire.ICD10



--BookingTypeBase
delete from PAS.BookingTypeBase

insert into PAS.BookingTypeBase
(
	 OPBOOKTYPEMFID
	,Code
	,Description
	,InternalValue
	,InternalValueDescription
	,MfRecStsInd
	,SchedulingApplicationAreaExternal
)
select
	 OPBOOKTYPEMFID
	,Code
	,Description
	,InternalValue
	,InternalValueDescription
	,MfRecStsInd
	,SchedulingApplicationAreaExternal
from
	(
	select
		 OPBOOKTYPEMFID
		,Code
		,Description
		,InternalValue
		,left(InternalValueDescription, 41) InternalValueDescription
		,left(MfRecStsInd, 1) MfRecStsInd
		,SchedulingApplicationAreaExternal
	from
		[$(PAS)].Inquire.OPBOOKTYPEMF
	) OpBookTypeMf


--Clinic

truncate table PAS.ClinicBase

insert into PAS.ClinicBase
	(
	 OUTCLNCID
	,AppTypesAvlblGpBook
	,AppTypesAvlblGpInt
	,Comment
	,ConsCode
	,Description
	,DestForGpAppLetters
	,DestinationHospCd
	,FunctionCode
	,GpAduAppLtrDocument
	,GpAppConfirmPara
	,GpBroMaxNoDaysFuture
	,GpChiAppLtrDocument
	,GpLnkDftBookTypeDesc
	,GpReasonRefrlText
	,GpRefrlCriteriaPara
	,GplClinMfRtnAppTypes
	,GplClinMfUrgAppTypes
	,GplClinicMfUrgent
	,GplClinicMfUrgentInt
	,GplinkDftBookType
	,MaxAgeThatMeansChild
	,MfRecStsInd
	,ProviderCode
	,ResultLocationCode
	,ResultLocationDesc
	,RptToLocCode
	,SchedResGrpMfCd
	,ServiceGroup
	,Specialty
	)
select
	 OUTCLNCID
	,AppTypesAvlblGpBook
	,AppTypesAvlblGpInt
	,Comment
	,ConsCode
	,Description
	,DestForGpAppLetters
	,DestinationHospCd
	,FunctionCode
	,GpAduAppLtrDocument
	,GpAppConfirmPara
	,GpBroMaxNoDaysFuture
	,GpChiAppLtrDocument
	,GpLnkDftBookTypeDesc
	,GpReasonRefrlText
	,GpRefrlCriteriaPara
	,GplClinMfRtnAppTypes
	,GplClinMfUrgAppTypes
	,GplClinicMfUrgent
	,GplClinicMfUrgentInt
	,GplinkDftBookType
	,MaxAgeThatMeansChild
	,MfRecStsInd
	,ProviderCode
	,ResultLocationCode
	,ResultLocationDesc
	,RptToLocCode
	,SchedResGrpMfCd
	,ServiceGroup
	,Specialty
from
	(
	select 
		 OUTCLNCID
		,AppTypesAvlblGpBook
		,AppTypesAvlblGpInt
		,Comment
		,ConsCode
		,Description
		,DestForGpAppLetters
		,DestinationHospCd
		,FunctionCode
		,GpAduAppLtrDocument
		,GpAppConfirmPara
		,GpBroMaxNoDaysFuture
		,GpChiAppLtrDocument
		,GpLnkDftBookTypeDesc
		,GpReasonRefrlText
		,GpRefrlCriteriaPara
		,GplClinMfRtnAppTypes
		,GplClinMfUrgAppTypes
		,GplClinicMfUrgent
		,GplClinicMfUrgentInt
		,GplinkDftBookType
		,MaxAgeThatMeansChild
		,left(MfRecStsInd, 1) MfRecStsInd
		,ProviderCode
		,ResultLocationCode
		,ResultLocationDesc
		,RptToLocCode
		,SchedResGrpMfCd
		,ServiceGroup
		,Specialty
	from
		[$(PAS)].Inquire.OUTCLNC
	) Outclnc


--OPDoctor

truncate table PAS.OPDoctorBase

insert into PAS.OPDoctorBase
	(
	 OPDOCTORID
	,AllowOverbooking
	,ClinicHospCode
	,Comment
	,GmcGdcHcp
	,GradeOfStaff
	,LocCode
	,MfRecStsInd
	,Name
	,SchDays
	,SchedResMfCd
	,ServiceGroup
	,TypeOfDoctor
	,ValidApptTypes
	)
select
	 OPDOCTORID
	,AllowOverBooking = AllowOverbooking
	,ClinicHospCode
	,Comment
	,GmcGdcHcp
	,GradeOfStaff
	,LocCode
	,MfRecStsInd
	,Name
	,SchDays
	,SchedResMfCd
	,ServiceGroup
	,TypeOfDoctor
	,ValidApptTypes
from
	(
	select 
		 OPDOCTORID
		,AllowOverbooking
		,ClinicHospCode
		,Comment
		,GmcGdcHcp
		,GradeOfStaff
		,LocCode
		,left(MfRecStsInd, 2) MfRecStsInd
		,Name
		,SchDays
		,SchedResMfCd
		,ServiceGroup
		,TypeOfDoctor
		,ValidApptTypes
	from
		[$(PAS)].Inquire.OPDOCTOR
	where
		not -- arbitrarily remove duplicates!
			(
				(
					OPDOCTOR.OPDOCTORID = 'CONSHO'
				and	OPDOCTOR.Comment = 'CONS/SHO/TUES/PM'
				)
			or
				(
					OPDOCTOR.OPDOCTORID = 'NURSE'
				and	OPDOCTOR.MfRecStsInd = '-1'
				)
			or
				(
					OPDOCTOR.OPDOCTORID = 'OUG4FP '
				and	OPDOCTOR.AllowOverbooking = '0'
				)
			)
	) OpDoctor



--Ward

update
	PAS.WardBase
set
	  WARDID = Ward.WARDID
	 ,ActivityStatus = Ward.ActivityStatus
	 ,AgesexGroup = Ward.AgesexGroup
	 ,AgesexGroup_1 = Ward.AgesexGroup_1
	 ,ClosedBeds = Ward.ClosedBeds
	 ,CmdsAgesexMix = Ward.CmdsAgesexMix
	 ,CmdsIntClCare = Ward.CmdsIntClCare
	 ,CmdsPtGroup = Ward.CmdsPtGroup
	 ,CmmLocationTypeDescriptionTruncated = Ward.CmmLocationTypeDescriptionTruncated
	 ,CottedWard = Ward.CottedWard
	 ,DayBedUnit = Ward.DayBedUnit
	 ,Days = Ward.Days
	 ,HospitalCode = Ward.HospitalCode
	 ,IntensityOfCare = Ward.IntensityOfCare
	 ,KarsIdCode = Ward.KarsIdCode
	 ,KornerCode = Ward.KornerCode
	 ,LocationType = Ward.LocationType
	 ,MfRecStsInd = Ward.MfRecStsInd
	 ,Nights = Ward.Nights
	 ,OwningWard = Ward.OwningWard
	 ,PatientGroup = Ward.PatientGroup
	 ,PrintSequence = Ward.PrintSequence
	 ,ProviderCode = Ward.ProviderCode
	 ,PtGroupintOfCare = Ward.PtGroupintOfCare
	 ,TimeAvailable = Ward.TimeAvailable
	 ,Ward = Ward.Ward
	 ,WardClassification = Ward.WardClassification
	 ,WardName = Ward.WardName
	 ,WardType = Ward.WardType
	 ,WardType_1 = Ward.WardType_1
from
	PAS.WardBase WardBase

inner join
	(
	select
		 WARDID
		,ActivityStatus
		,AgesexGroup
		,AgesexGroup_1
		,ClosedBeds
		,CmdsAgesexMix
		,CmdsIntClCare
		,CmdsPtGroup
		,CmmLocationTypeDescriptionTruncated
		,CottedWard
		,DayBedUnit
		,Days
		,HospitalCode
		,IntensityOfCare
		,KarsIdCode
		,KornerCode
		,LocationType
		,MfRecStsInd
		,Nights
		,OwningWard
		,PatientGroup
		,PrintSequence
		,ProviderCode
		,PtGroupintOfCare
		,TimeAvailable
		,Ward
		,WardClassification
		,WardName
		,WardType
		,WardType_1
	from
		(
		select
			 WARDID
			,ActivityStatus
			,AgesexGroup
			,AgesexGroup_1
			,ClosedBeds
			,CmdsAgesexMix
			,CmdsIntClCare
			,CmdsPtGroup
			,left(CmmLocationTypeDescriptionTruncated, 50) CmmLocationTypeDescriptionTruncated
			,CottedWard
			,DayBedUnit
			,Days
			,HospitalCode
			,IntensityOfCare
			,KarsIdCode
			,KornerCode
			,LocationType
			,left(MfRecStsInd, 2) MfRecStsInd
			,Nights
			,OwningWard
			,PatientGroup
			,PrintSequence
			,ProviderCode
			,PtGroupintOfCare
			,TimeAvailable
			,Ward
			,WardClassification
			,WardName
			,WardType
			,left(WardType_1, 10) WardType_1
		from
			[$(PAS)].Inquire.WARD
		) NewWard
	) Ward
on	WardBase.WARDID = Ward.WARDID

where
	not
	(
		WardBase.ActivityStatus = Ward.ActivityStatus
	and WardBase.AgesexGroup = Ward.AgesexGroup
	and WardBase.AgesexGroup_1 = Ward.AgesexGroup_1
	and WardBase.ClosedBeds = Ward.ClosedBeds
	and WardBase.CmdsAgesexMix = Ward.CmdsAgesexMix
	and WardBase.CmdsIntClCare = Ward.CmdsIntClCare
	and WardBase.CmdsPtGroup = Ward.CmdsPtGroup
	and WardBase.CmmLocationTypeDescriptionTruncated = Ward.CmmLocationTypeDescriptionTruncated
	and WardBase.CottedWard = Ward.CottedWard
	and WardBase.DayBedUnit = Ward.DayBedUnit
	and WardBase.Days = Ward.Days
	and WardBase.HospitalCode = Ward.HospitalCode
	and WardBase.IntensityOfCare = Ward.IntensityOfCare
	and WardBase.KarsIdCode = Ward.KarsIdCode
	and WardBase.KornerCode = Ward.KornerCode
	and WardBase.LocationType = Ward.LocationType
	and WardBase.MfRecStsInd = Ward.MfRecStsInd
	and WardBase.Nights = Ward.Nights
	and WardBase.OwningWard = Ward.OwningWard
	and WardBase.PatientGroup = Ward.PatientGroup
	and WardBase.PrintSequence = Ward.PrintSequence
	and WardBase.ProviderCode = Ward.ProviderCode
	and WardBase.PtGroupintOfCare = Ward.PtGroupintOfCare
	and WardBase.TimeAvailable = Ward.TimeAvailable
	and WardBase.Ward = Ward.Ward
	and WardBase.WardClassification = Ward.WardClassification
	and WardBase.WardName = Ward.WardName
	and WardBase.WardType = Ward.WardType
	and WardBase.WardType_1 = Ward.WardType_1
	)


insert into PAS.WardBase
(
	 WARDID
	,ActivityStatus
	,AgesexGroup
	,AgesexGroup_1
	,ClosedBeds
	,CmdsAgesexMix
	,CmdsIntClCare
	,CmdsPtGroup
	,CmmLocationTypeDescriptionTruncated
	,CottedWard
	,DayBedUnit
	,Days
	,HospitalCode
	,IntensityOfCare
	,KarsIdCode
	,KornerCode
	,LocationType
	,MfRecStsInd
	,Nights
	,OwningWard
	,PatientGroup
	,PrintSequence
	,ProviderCode
	,PtGroupintOfCare
	,TimeAvailable
	,Ward
	,WardClassification
	,WardName
	,WardType
	,WardType_1
)
select
	 WARDID
	,ActivityStatus
	,AgesexGroup
	,AgesexGroup_1
	,ClosedBeds
	,CmdsAgesexMix
	,CmdsIntClCare
	,CmdsPtGroup
	,CmmLocationTypeDescriptionTruncated
	,CottedWard
	,DayBedUnit
	,Days
	,HospitalCode
	,IntensityOfCare
	,KarsIdCode
	,KornerCode
	,LocationType
	,MfRecStsInd
	,Nights
	,OwningWard
	,PatientGroup
	,PrintSequence
	,ProviderCode
	,PtGroupintOfCare
	,TimeAvailable
	,Ward
	,WardClassification
	,WardName
	,WardType
	,WardType_1
from
	(
	select
		 WARDID
		,ActivityStatus
		,AgesexGroup
		,AgesexGroup_1
		,ClosedBeds
		,CmdsAgesexMix
		,CmdsIntClCare
		,CmdsPtGroup
		,left(CmmLocationTypeDescriptionTruncated, 50) CmmLocationTypeDescriptionTruncated
		,CottedWard
		,DayBedUnit
		,Days
		,HospitalCode
		,IntensityOfCare
		,KarsIdCode
		,KornerCode
		,LocationType
		,left(MfRecStsInd, 2) MfRecStsInd
		,Nights
		,OwningWard
		,PatientGroup
		,PrintSequence
		,ProviderCode
		,PtGroupintOfCare
		,TimeAvailable
		,Ward
		,WardClassification
		,WardName
		,WardType
		,left(WardType_1, 10) WardType_1
	from
		[$(PAS)].Inquire.WARD
	) NewWard

where
	not exists
		(
		select
			1
		from
			PAS.WardBase Ward
		where
			Ward.WARDID = NewWard.WARDID
		)


-- Ethnic Origin

truncate table PAS.EthnicOriginBase

insert into PAS.EthnicOriginBase
	(
	 EthnicOriginCode
	,EthnicOrigin
	,EthnicGroupNationalCode
	,EthnicityNationalCode
	,EthnicityNational
	,MfRecStsInd
	)
    
select
	 EthnicOriginCode
	,EthnicOrigin
	,EthnicGroupNationalCode
	,EthnicityNationalCode
	,EthnicityNational
	,MfRecStsInd
from
	(
	select
		 EthnicGroupNatCode EthnicGroupNationalCode
		,EthnicityNatCode EthnicityNationalCode
		,EthnicityNatCodeDesc EthnicityNational
		,EthnicOriginCode
		,EthnicOriginDesc EthnicOrigin
		,left(MfRecStsInd , 2) MfRecStsInd
	from
		[$(PAS)].Inquire.ETHN
	) Ethn

-- Staff Group

truncate table PAS.StaffGroupBase

insert into PAS.StaffGroupBase
	(
	 GradeOfStaffCode
	,GradeOfStaff
	,MedicalStaffTypeCode
	,MedicalStaffType
	,MfRecStsInd
	)
    
select
	 GradeOfStaffCode
	,GradeOfStaff
	,MedicalStaffTypeCode
	,MedicalStaffType
	,MfRecStsInd
from
	(
	select
		 GradeOfStaffCode
		,GradeOfStaffDescription GradeOfStaff
		,MedicalStaffType MedicalStaffTypeCode
		,MedicalStaffTypeDescription MedicalStaffType
		,left(MfRecStsInd , 2) MfRecStsInd
	from
		[$(PAS)].Inquire.STAFFGP
	) StaffGp


-- Management Intention

truncate table PAS.ManagementIntentionBase

INSERT INTO PAS.ManagementIntentionBase
           (Description
           ,HaaCode
           ,IntdMgmt
           ,InternalValue
           ,IntMgmtCode
           ,MfRecStsInd
           ,ScottishIntValue)
select
	*
from
	(
	select
		 Description
		,HaaCode
		,IntdMgmt
		,InternalValue
		,IntMgmtCode
		,left(MfRecStsInd , 2) MfRecStsInd
		,ScottishIntValue
	from
		[$(PAS)].Inquire.INMAN
	) InMan


-- Contract

truncate table PAS.ContractBase

INSERT INTO PAS.ContractBase
           (AgreementCDS
           ,CidGrp
           ,CidType
           ,Cm2ContractMfSuitableToAutoAllocateInt
           ,ContractDesc
           ,ContractGroup
           ,ContractGroupCodeDescription
           ,ContractId
           ,ContractId_1
           ,ContractManagementEndDateInternal
           ,ContractManagementStartDateInternal
           ,Costsum
           ,Description
           ,Description_1
           ,EndDate
           ,InterimBilling
           ,IntrmBilling
           ,MfRecStsInd
           ,OutpatientPrice1st
           ,PaymentTerms
           ,PriceProfile
           ,PriceProfileDescription
           ,ProcedureProfileCode
           ,ProviderCode
           ,PurchaserCode
           ,Reatt
           ,StartDate
           ,SuitableToAutoallocate
           ,Thresholdceiling
           ,Type
           ,TypeOfCont
           ,WardAttPrice)

select
	* 
from
	(
	select
		 AgreementCDS
		,CidGrp
		,CidType
		,Cm2ContractMfSuitableToAutoAllocateInt
		,ContractDesc
		,ContractGroup
		,ContractGroupCodeDescription
		,ContractId
		,ContractId_1
		,ContractManagementEndDateInternal
		,ContractManagementStartDateInternal
		,Costsum
		,Description
		,Description_1
		,EndDate
		,InterimBilling
		,IntrmBilling
		,left(MfRecStsInd , 2) MfRecStsInd
		,OutpatientPrice1st
		,PaymentTerms
		,PriceProfile
		,PriceProfileDescription
		,ProcedureProfileCode
		,ProviderCode
		,PurchaserCode
		,Reatt
		,StartDate
		,SuitableToAutoallocate
		,Thresholdceiling
		,Type
		,TypeOfCont
		,WardAttPrice
	from
		[$(PAS)].Inquire.CONTR
	) contr

-- Purchaser

truncate table PAS.PurchaserBase

INSERT INTO PAS.PurchaserBase
           (PURCHASERID
           ,Address1
           ,Address2
           ,Address3
           ,Address4
           ,Aka
           ,AuthorisationAddress1
           ,AuthorisationAddress2
           ,AuthorisationAddress3
           ,AuthorisationAddress4
           ,AuthorisationName
           ,AuthorisationPhone
           ,AuthorisationPostcode
           ,CmmCommissioningConsoritaLeadCommDescription
           ,CmmOrgCodesPurchaserEndDateinternal
           ,CmmOrgCodesPurchaserStartDateinternal
           ,ContactAddress1
           ,ContactAddress2
           ,ContactAddress3
           ,ContactAddress4
           ,ContactName
           ,ContactPhone
           ,ContactPostcode
           ,Fax
           ,InvoiceAddress1
           ,InvoiceAddress2
           ,InvoiceAddress3
           ,InvoiceAddress4
           ,InvoiceName
           ,InvoicePhone
           ,InvoicePostcode
           ,Ldf
           ,LeadCommissioner
           ,Name
           ,Phone
           ,Postcode
           ,PurchaserCode
           ,Type)
select
	*
from
	(
	select
		 PURCHASERID
		,Address1
		,Address2
		,Address3
		,Address4
		,Aka
		,AuthorisationAddress1
		,AuthorisationAddress2
		,AuthorisationAddress3
		,AuthorisationAddress4
		,AuthorisationName
		,AuthorisationPhone
		,AuthorisationPostcode
		,CmmCommissioningConsoritaLeadCommDescription
		,CmmOrgCodesPurchaserEndDateinternal
		,CmmOrgCodesPurchaserStartDateinternal
		,ContactAddress1
		,ContactAddress2
		,ContactAddress3
		,ContactAddress4
		,ContactName
		,ContactPhone
		,ContactPostcode
		,Fax
		,InvoiceAddress1
		,InvoiceAddress2
		,InvoiceAddress3
		,InvoiceAddress4
		,InvoiceName
		,InvoicePhone
		,InvoicePostcode
		,left(Ldf , 2) Ldf
		,LeadCommissioner
		,Name
		,Phone
		,Postcode
		,PurchaserCode
		,Type
	from
		[$(PAS)].Inquire.PURCHASER
	) Purchaser


-- OP Cancellation Reason

truncate table PAS.OPCancellationReasonBase

INSERT INTO PAS.OPCancellationReasonBase
           (CancellationCode
           ,Description
           ,EbsInboundCancReason
           ,EbsInboundCancReasonInt
           ,EbsReasonCode
           ,EbsReasonDesc
           ,MfRecStsInd)
select
	*
from
	(
	select
		 CancellationCode
		,left(Description , 34) Description
		,EbsInboundCancReason
		,EbsInboundCancReasonInt
		,EbsReasonCode
		,EbsReasonDesc
		,left(MfRecStsInd , 2) MfRecStsInd
	from
		[$(PAS)].Inquire.OPCANCRSN
	) OpCancRsn


--  OP Appointment Type
truncate table PAS.OPAppointmentTypeBase


insert into PAS.OPAppointmentTypeBase

		(AppointmentTypeCode,
		AppointmentTypeDesc,
		ApptCategory,
		ApptClassification,
		EBooking,
		EBookingInt,
		MfRecStsInd,
		ReminderLetter,
		SchedulingAppointmentClassification,
		SchedulingApptClassificationDescription,
		SchRemLetterInt,
		Urgent,
		Urgent_1)

Select  * 

from
	(
	Select 
		AppointmentTypeCode,
		AppointmentTypeDesc,
		ApptCategory,
		ApptClassification,
		EBooking,
		EBookingInt,
		left(MfRecStsInd , 2) MfRecStsInd,
		ReminderLetter,
		SchedulingAppointmentClassification,
		SchedulingApptClassificationDescription,
		SchRemLetterInt,
		Urgent,
		Urgent_1
	from
		[$(PAS)].Inquire.APPTYP 
	) ApptTyp



----User
--insert into PAS.User
--(
--	 UserCode
--	,User
--	,UserGroupCode
--)
--select
--	*
--from
--	(
--	select
--		 UserCode = rtrim(UserId)
--		,USER = rtrim(UserIdName)
--		,UserGroupCode = '#'
--	from
--		(
--		select
--			 UserId
--			,left(UserIdName , 26) UserIdName
--		from
--			[$(PAS)].Inquire.AMSUID
--		) PasUser

--	union all

--	select
--		 UserCode = '***' 
--		,User = 'PAS' 
--		,UserGroupCode = '#'
--	) NewUser

--where
--	not exists
--		(
--		select
--			1
--		from
--			PAS.User
--		where
--			User.UserCode = NewUser.UserCode
--	)

--Inpatient Waiting List

truncate table PAS.WaitingListBase
insert into PAS.WaitingListBase

select
	*
from
	(
	select
		 CurrentlySequencedBy
		,ElAdmListNo
		,HospitalCode
		,MainConsultant
		,left(MfRecStsInd , 2) MfRecStsInd
		,ProviderCode
		,RepeatList
		,RepeatList_1
		,SequencedBy
		,SpecOfList
		,WaitingList
		,WlDescription
		,WlMaxAdmTimed
		,WlMaxAdmTimeo
		,WlMfCurrentSortInt
		,WlMfRequestedSortInt
	from
		[$(PAS)].Inquire.WAITCD
	) WaitCd



insert into PAS.UserBase

select
	*
from
	(
	select
		*
		,UserGroupCode = '#'
	from
		(
		select
			 left(GpUserIdAccountDeletionFlag, 2) GpUserIdAccountDeletionFlag
			,GpxgpUsername
			,UserId
			,left(UserIdName, 26) UserIdName
			,UsersDepartment
			,UsersHospital
		from
			[$(PAS)].Inquire.AMSUID
		where
			not exists
			(
			select
				1
			from
				[$(PAS)].Inquire.AMSUID Previous
			where
				Previous.UserId = AMSUID.UserId
			and	Previous.AMSUIDID < AMSUID.AMSUIDID
			)
		) AmsUid

	union all

	select
		 null
		,null
		,UserID = '***'
		,UserIdName = 'PAS'
		,null
		,null
		,UserGroupCode = '#'
	) NewUser
where
	not exists
	(
	select
		1
	from
		PAS.UserBase
	where
		UserBase.UserId = NewUser.UserId
	)


--Casenote Location
delete from PAS.CasenoteLocationBase

INSERT INTO PAS.CasenoteLocationBase
(
	Location
	,LocationName
	,MfRecStsInd
)

SELECT 
	 Location
	,LocationName
	,MfRecStsInd
FROM
	[$(PAS)].Inquire.CASENLOC


--Borrower

DELETE FROM PAS.BorrowerBase;

INSERT into PAS.BorrowerBase
(
	 BorrowerAddressLine1
	,BorrowerCode
	,BorrowerPostcode
	,BorrowerSurname
	,BorrowerTelephoneNumber
	,BorrowerTitleAndInitials
	,Line2
	,Line3
	,Line4
	,MfRecStsInd
)

SELECT 
	 BorrowerAddressLine1
	,BorrowerCode
	,BorrowerPostcode
	,BorrowerSurname
	,BorrowerTelephoneNumber
	,BorrowerTitleAndInitials
	,Line2
	,Line3
	,Line4
	,MfRecStsInd
FROM
	[$(PAS)].Inquire.BORROWER

--religion

DELETE FROM PAS.ReligionBase

INSERT into PAS.ReligionBase

(
Description
,HaaCode
,InternalValue
,MfRecStsInd
,RelgionGroupDescription
,Religion
,ReligionGroup
)

select
	Description
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,RelgionGroupDescription
	,Religion
	,ReligionGroup
from
	[$(PAS)].Inquire.RELIGION

/* augmented care location */

truncate table PAS.AugmentedCareLocationBase

insert into PAS.AugmentedCareLocationBase
(
	AcpLocationCode
	,AcpLocation
	,AcpLocNatCode
	--,ApcAugmentedCareLocationDefaultConsultantInt
	,ApcAugmentedCareLocationNationalCodeDescription
	,CcUnitCode
	,CcUnitFunctionDescription
	,DefaultConsultant
	,Description
	,SiteCode
	,MfRecStsInd
	,WardCode
)

select
	AUGPATCARELOCID
	,AcpLocation
	,AcpLocNatCode
	--,ApcAugmentedCareLocationDefaultConsultantInt
	,ApcAugmentedCareLocationNationalCodedesc
	,CcUnitCode
	,CcUnitFunctionDescr
	,DefaultConsultant
	,Description
	,HospitalCode
	,left(MfRecStsInd , 2) MfRecStsInd
	,WardCode
from
	[$(PAS)].Inquire.AUGPATCARELOC
	
	
/* Special Register master file. Added Paul Egan 14/08/2014 */
--truncate table PAS.PatientSpecialRegisterBase;

insert into PAS.SpecialRegisterBase
	(
	SpecialRegisterCode
	,SpecialRegister
	,FormOfWarningCode
	,FormOfWarning
	,MfRecStsInd
	)
select 
	SpecialRegisterCode = PMISPECIALREGMFID
	,SpecialRegister	= SpecialRegisterDescription
	,FormOfWarningCode	= PmiSpecialRegisterCodeFormOfWarningInt
	,FormOfWarning		= FormOfWarning
	,MfRecStsInd		= MfRecStsInd
from
	[$(PAS)].Inquire.PMISPECIALREGMF
where
	not exists
			(
			select
				1
			from
				 PAS.SpecialRegisterBase
			where
				PMISPECIALREGMF.PMISPECIALREGMFID = SpecialRegisterBase.SpecialRegisterCode
			)

/* Order comms result MF. Added 15.9.14 - DG */

insert PAS.ResultBase
(
ResultCd
,ResultDescription
,ResultName
,ResultType
,UnitOfMeasurement
,Deleted
,OCMFRESULTSID
)
select
	ResultCd
	,ResultDescription
	,ResultName
	,ResultType
	,UnitOfMeasurement
	,Deleted
	,OCMFRESULTSID
from
	(
	select
		ResultCd = ltrim(rtrim(ResultCd))
		,ResultDescription
		,ResultName
		,ResultType
		,UnitOfMeasurement
		,Deleted
		,OCMFRESULTSID
		,SequenceNo = row_number() over (partition by ResultCd order by cast(Deleted as int)) -- handles where same code gets added again and the older version is marked for deletion
	from
		[$(PAS)].Inquire.OCMFRESULTS
	where
		not exists
				(
				select
					1
				from
					PAS.ResultBase
				where
					ResultBase.ResultCd = OCMFRESULTS.ResultCd
				)
	) Result
where
	SequenceNo = 1
;


/* Location - Added Paul Egan 05/01/2015 */
truncate table PAS.LocationBase;

insert into PAS.LocationBase
	(
	 LocationCode
	,Location
	,Location1
	,LocationTypeCode
	,MfRecStsInd
	,SgLocnLink
	)
select
	 LocationCode
	,LocationDesc
	,LocationDesc_1
	,LocationType
	,MfRecStsInd
	,SgLocnLink
from
	[$(PAS)].Inquire.LOCN
;



insert PAS.ServiceBase
(
HospitalCode
,Service
,Description
,DescriptionLabels
,DescriptionShort
,CategoryOfService
,CategoryOfServiceInt
,ScreenType
,Mnemonic
,Deleted
,OCMSERVICEID
)

select
	HospitalCode
	,Service
	,Description
	,DescriptionLabels
	,DescriptionShort
	,CategoryOfService
	,CategoryOfServiceInt
	,ScreenType
	,Mnemonic
	,Deleted
	,OCMSERVICEID
from
	[$(PAS)].Inquire.OCMSERVICE
where
	not exists
		(
		select
			1
		from
			PAS.ServiceBase
		where
			OCMSERVICE.OCMSERVICEID = ServiceBase.OCMSERVICEID
		)
;


insert PAS.ScreenTypeBase
(
ScreenTypeCode
,ScreenType 
,Deleted
)

select
	ScreenTypeCode = ScreenType
	,ScreenType = ScreenType_Desc
	,Deleted
from
	[$(PAS)].Inquire.OCMSCREENTYPE
where
	not exists
		(
		select
			1
		from
			PAS.ScreenTypeBase
		where
			ScreenTypeBase.ScreenTypeCode = OCMSCREENTYPE.ScreenType
		)
;



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'LoadPASReferenceData', @Stats, @StartTime








﻿

CREATE PROCEDURE ETL.LoadPASPatientDuplication
AS
	SET NOCOUNT ON
	
	TRUNCATE TABLE PAS.PatientDuplicationTemp
	
	-- Temporary table used only by this procedure
	-- Contains patient numbers and a hashkey which is used to determine duplicates
	-- So rows with the same Hashkey value are duplicate patient numbers
	INSERT INTO PAS.PatientDuplicationTemp
	(
		 SourcePatientNo
		,[HashKey]
		,MasterScore
		,IsMaster
		,RegistrationDate
	)
	-- Note - Need the DISTINCT to compress rows where the SourcePatientNo and Hashkey are the same
	-- due to many rows in the PatientAddress table for the same SourcePatientNo
	SELECT DISTINCT				
		 PAT.SourcePatientNo
		,MatchPatientHashKey = 
			-- Define how duplicates are determined here
			HASHBYTES('MD5', 
						 COALESCE(CAST(PAT.DateOfBirth AS VARCHAR(20)), CAST(PAT.SourcePatientNo AS VARCHAR(20)))	
						+ '||' + PAT.Surname							
						+ '||' + PAT.Forenames	
						+ '||' + PAT.SexCode						
						+ '||' + COALESCE(REPLACE(ADDR.Postcode,' ',''), REPLACE(PAT.PostCode,' ',''), '')
					)
		-- Define how the Master patient number of a set of duplicates is determined here
		,MasterScore	=		CASE WHEN PAT.NHSNumber IS NOT NULL THEN 10 ELSE 0 END
							+	CASE WHEN PAT.PostCode IS NOT NULL THEN 10 ELSE 0 END
							+	CASE WHEN PAT.NextOfKin IS NOT NULL THEN 10 ELSE 0 END
							-- Give a higher score value if a Casenote or some activity exists for the patient no
							+	CASE WHEN CN.RegistrationDate IS NOT NULL THEN 50 ELSE 0 END
		,IsMaster = 0
		,RegistrationDate	= COALESCE(CN.RegistrationDate, CAST(PAT.ModifiedDate AS DATE))		-- Earliest Casenote allocated or Activity date
		
	FROM	
		(
			SELECT
				 SourcePatientNo	= PAT.SourcePatientNo
				,Surname			= PAT.Surname
				,Forenames			= PAT.Forenames
				,DateOfBirth		= PAT.DateOfBirth
				,PostCode			= PAT.Postcode
				,NextOfKin			= PAT.NextOfKin
				,NHSNumber			= PAT.NHSNumber
				,SexCode			= PAT.SexCode
				,ModifiedDate		= PAT.Modified
			FROM
				PAS.Patient  PAT
				
			UNION
			-- Look at previous surnames
			SELECT
				 SourcePatientNo	= PATP.SourcePatientNo
				,Surname			= PATP.PreviousSurname
				,Forenames			= PATP.Forenames
				,DateOfBirth		= PATP.DateOfBirth
				,PostCode			= PATP.Postcode
				,NextOfKin			= PATP.NextOfKin
				,NHSNumber			= PATP.NHSNumber
				,SexCode			= PATP.SexCode
				,ModifiedDate		= PATP.Modified
			FROM
				PAS.Patient  PATP
			WHERE
				PATP.PreviousSurname IS NOT NULL	
			
		) PAT
		LEFT JOIN 
		(
			-- This part is attemting to derive a patient registration date from a casenote or activity
			-- The Patient table Modified date is unrelable for this  
			SELECT
				 SourcePatientNo
				,RegistrationDate = MIN(EncounterDate)
			FROM
			(
				-- Casenotes
				SELECT 
					 SourcePatientNo
					,EncounterDate		= AllocatedDate
				FROM 
					PAS.Casenote
				WHERE
					AllocatedDate >= '1 Jan 1960'				-- Exclude a raft of dates in 1900
					
				UNION
				
				-- In-Patient Activity
				SELECT 
					 SourcePatientNo
					,CAST(AdmissionDate AS DATE)
				FROM 
					APC.Encounter

				UNION
				
				-- Out-patient activity
				SELECT 
					 SourcePatientNo
					,CAST(COALESCE(AppointmentCreateDate, AppointmentDate) AS DATE)
				FROM 
					OP.Encounter
					
				UNION
				
				-- A&E activity
				SELECT 
					 P.SourcePatientNo
					,CAST(AE.ArrivalDate AS DATE)
				FROM 
					AE.Encounter AE
					INNER JOIN PAS.Patient P
						ON AE.DistrictNo = P.DistrictNo
				
			) DATA
			GROUP BY
				SourcePatientNo
		) CN
			ON		CN.SourcePatientNo = PAT.SourcePatientNo
		-- Historic Post Codes		
		LEFT JOIN PAS.PatientAddress ADDR
			ON PAT.SourcePatientNo = ADDR.SourcePatientNo
			
-----------------------------------------------------------------		
-- Update the Patient table with the Registration Date
-----------------------------------------------------------------	

	UPDATE PAS.Patient
		SET RegistrationDate = PDT.RegistrationDate
	FROM
		PAS.Patient P
		INNER JOIN
		(
			SELECT DISTINCT
				 SourcePatientNo
				,RegistrationDate
			FROM
				PAS.PatientDuplicationTemp
		) PDT ON PDT.SourcePatientNo = P.SourcePatientNo
	
-----------------------------------------------------------------	
-- From all the rows in the PAS.PatientDuplicationTemp table (of which the majority will not be duplicates)
-- we want to find duplicates and from these assign one to be a Master Patient Number
--
-- Example of the PAS.PatientDuplicationTemp :
--
-- SourcePatientNo     HashKey     MasterScore
-- ===============     =======     ===========
--       1234          0xABC3213        20      --
--       2313          0xABC3213        30       | - All these are duplicate Patient Number as they have the same Hashkey value
--       7879          0xABC3213        80      --   This one is assigned as the Master patient, as it has the highest score of the three
--      
--       6876          0xCED3455        10      -- As these duplicate patient nos all have the same score, assign the Master
--       5765          0xCED3455        10      -- patient to the 6876 SourcePatientNo row, as it has a higher SourcePatientNo value 
--
--       9898          0x45A1241        20      -- No duplicate so ignored
--
-----------------------------------------------------------------	

	UPDATE PAS.PatientDuplicationTemp
		SET IsMaster = 1	
	FROM
		PAS.PatientDuplicationTemp PD
		INNER JOIN
		(
			SELECT
				 A.SourcePatientNo
				,A.[HashKey]
				-- For each hashkey, rank the duplicate patient nos by Score, the highest will be set as the Master Patient No
				,RK = RANK() OVER (PARTITION BY DUP.[HashKey] ORDER BY A.MasterScore DESC, A.SourcePatientNo DESC)
			FROM
			(
				-- Create a set of HashKeys with duplicate Patient Nos
				SELECT	
					 [HashKey]
					,DuplicationCount = COUNT(DISTINCT SourcePatientNo)
				FROM
					PAS.PatientDuplicationTemp	
				GROUP BY [HashKey]
					HAVING COUNT(DISTINCT SourcePatientNo) > 1
			) DUP

			-- Find other Patient nos that have the same HashKey value
			INNER JOIN PAS.PatientDuplicationTemp A
				ON		DUP.[HashKey] = A.[HashKey]
		) D
			ON		PD.SourcePatientNo	= D.SourcePatientNo
				AND	PD.[HashKey]		= D.[HashKey]
				AND D.RK				= 1						-- Pick the patient no with the highest Master score
		
		-- Exclude if the only activity is an AdmissionMethod denoting a birth
		INNER JOIN
		(
			SELECT DISTINCT
				SourcePatientNo
			FROM
				APC.Encounter
			WHERE
                AdmissionMethodCode NOT IN ('BH','BL','BT')
		) BIRTHS
			ON BIRTHS.SourcePatientNo = D.SourcePatientNo
				
--------------------------------
-- Insert to Duplication Table
--------------------------------
	TRUNCATE TABLE PAS.PatientDuplicationBase

-------------------------------------------------------------
-- Insert the duplicated patient rows for each Master patient
-------------------------------------------------------------

	INSERT INTO PAS.PatientDuplicationBase
	(
		 SourcePatientNo
		,MasterSourcePatientNo
		,RegistrationDate
		,IsEarliestRegistration
	)
	SELECT
		 PDTDUP.SourcePatientNo
		,PDTMAS.SourcePatientNo
		,PDTDUP.RegistrationDate
		,'N'
	FROM
		PAS.PatientDuplicationTemp PDTMAS
		-- Find other patient nos with the same hashkey value as the Master patient number
		INNER JOIN PAS.PatientDuplicationTemp PDTDUP
			ON		PDTMAS.[HashKey] = PDTDUP.[HashKey]
				AND	PDTMAS.PatientDuplicationTempId <> PDTDUP.PatientDuplicationTempId
	WHERE
		PDTMAS.IsMaster = 1	
	
-------------------------------------------------------------
-- Insert the Master patient
-------------------------------------------------------------
	INSERT INTO PAS.PatientDuplicationBase
	(
		 SourcePatientNo
		,MasterSourcePatientNo
		,RegistrationDate
		,IsEarliestRegistration
	)
	SELECT
		 PDT.SourcePatientNo
		,PDT.SourcePatientNo						-- Master Patient has the same value as the SourcePatientNo column
		,PDT.RegistrationDate
		,'N'										-- Set default
	FROM
		PAS.PatientDuplicationTemp PDT
	WHERE
		PDT.IsMaster = 1

-------------------------------------------------------------
-- Delete rows where there are more than one Master/Duplicate
-------------------------------------------------------------
	DELETE PAS.PatientDuplicationBase
	FROM 
		PAS.PatientDuplicationBase PD
		CROSS APPLY
		(
			SELECT 
				DupCount = COUNT(*)
			FROM
				PAS.PatientDuplicationBase A
			WHERE
					A.SourcePatientNo			= PD.SourcePatientNo
				AND A.MasterSourcePatientNo		= PD.MasterSourcePatientNo
				AND A.PatientDuplicationId		> PD.PatientDuplicationId
		) DUPS
	WHERE
		DUPS.DupCount >= 1
		
-------------------------------------------------------------
-- Set a flag if the duplicate is the earliest in its MasterSourcePatientNo group
-------------------------------------------------------------

	UPDATE PAS.PatientDuplicationBase
		SET IsEarliestRegistration = 'Y'
	FROM
		PAS.PatientDuplicationBase M
		CROSS APPLY
		(
			-- For each row processed from the M table alias, see if this has the earliest
			-- registration date for the row MasterSourcePatienNo group by returning the row
			-- If it isn't a row will not be returned and the M table alias row will not be updated
			SELECT 
				PDB_A.PatientDuplicationId
			FROM 
				PAS.PatientDuplicationBase PDB_A
				LEFT JOIN	PAS.PatientDuplicationBase PDB_B
					ON		PDB_B.MasterSourcePatientNo = PDB_A.MasterSourcePatientNo
						AND (
								PDB_B.RegistrationDate		< PDB_A.RegistrationDate
								OR 
								(		PDB_B.RegistrationDate		= PDB_A.RegistrationDate
									AND PDB_B.PatientDuplicationId < PDB_A.PatientDuplicationId
								)
							)
			WHERE
					PDB_A.PatientDuplicationId = M.PatientDuplicationId
				AND PDB_B.PatientDuplicationId IS NULL
		) D
		



﻿

CREATE procedure [ETL].[LoadObservationObservation]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 @LoadStartDate = min(LastModifiedTime)
	,@LoadEndDate = max(LastModifiedTime) 
from
	ETL.TLoadObservationObservation

merge
	Observation.Observation target
using
	(
	select
		SourceUniqueID
		,ObservationSetSourceUniqueID
		,MeasureID
		,NumericResult
		,ListID
		,FlagCode
		,CreatedByUserID
		,CreatedTime
		,ModifiedByUserID
		,LastModifiedTime
		,InterfaceCode 
	from
		ETL.TLoadObservationObservation

	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.LastModifiedTime between @LoadStartDate and @LoadEndDate

	then delete	

	when not matched
	then
		insert
			(
			SourceUniqueID
			,ObservationSetSourceUniqueID
			,MeasureID
			,NumericResult
			,ListID
			,FlagCode
			,CreatedByUserID
			,CreatedTime
			,ModifiedByUserID
			,LastModifiedTime
			,InterfaceCode 
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.ObservationSetSourceUniqueID
			,source.MeasureID
			,source.NumericResult
			,source.ListID
			,source.FlagCode
			,source.CreatedByUserID
			,source.CreatedTime
			,source.ModifiedByUserID
			,source.LastModifiedTime
			,source.InterfaceCode 
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
			isnull(target.LastModifiedTime, '1 jan 1900') = isnull(source.LastModifiedTime, '1 jan 1900')
	then
		update
		set
			target.MeasureID = source.MeasureID
			,target.NumericResult = source.NumericResult
			,target.ListID = source.ListID
			,target.FlagCode = source.FlagCode
			,target.CreatedByUserID = source.CreatedByUserID
			,target.CreatedTime = source.CreatedTime
			,target.ModifiedByUserID = source.ModifiedByUserID
			,target.LastModifiedTime = source.LastModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
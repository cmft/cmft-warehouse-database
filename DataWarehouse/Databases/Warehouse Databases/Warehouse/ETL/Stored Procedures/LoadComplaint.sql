﻿

CREATE proc [ETL].[LoadComplaint]
(
	@FromDate date = null
	,@ToDate date = null
)

as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime = getdate()
declare @Elapsed int
declare @Stats varchar(255)

select @FromDate = coalesce(@FromDate, (select cast(dateadd(year, -3, getdate()) as date)))
select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))

exec ETL.LoadUlyssesReferenceData
exec ETL.ExtractUlyssesComplaint @FromDate, @ToDate
exec ETL.LoadComplaintComplaint


select @Elapsed = datediff(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @ProcedureName
print @Stats
print @StartTime
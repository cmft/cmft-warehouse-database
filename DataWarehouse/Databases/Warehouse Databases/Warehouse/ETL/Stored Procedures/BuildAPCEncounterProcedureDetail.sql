﻿CREATE procedure [ETL].[BuildAPCEncounterProcedureDetail] as

--Links theatre procedure detail records to the provider spell

--Should be run following a refresh of either APC or Theatre data

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int

truncate table APC.EncounterProcedureDetail

insert
into
	APC.EncounterProcedureDetail
(
	 EncounterRecno
	,ProcedureDetailSourceUniqueID
)
select distinct
	 EncounterRecno
	,ProcedureDetailSourceUniqueID
from
	(
	select
		 Encounter.EncounterRecno
		,ProcedureDetailSourceUniqueID = ProcedureDetail.SourceUniqueID
	from
		APC.Encounter Encounter

	inner join Theatre.PatientBooking PatientBooking
	on	PatientBooking.NHSNumber = replace(Encounter.NHSNumber, ' ', '')

	inner join Theatre.OperationDetail OperationDetail
	on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
	and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

	inner join Theatre.ProcedureDetail ProcedureDetail
	on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

	where
	--null OperationEndTime indicates cancelled procedure
		OperationDetail.OperationEndDate is not null

	union all

	select
		 Encounter.EncounterRecno
		,ProcedureDetail.SourceUniqueID
	from
		APC.Encounter Encounter

	inner join Theatre.PatientBooking PatientBooking
	on	replace(PatientBooking.CasenoteNumber, 'Y', '/') = Encounter.CasenoteNumber

	inner join Theatre.OperationDetail OperationDetail
	on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
	and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

	inner join Theatre.ProcedureDetail ProcedureDetail
	on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

	where
	--null OperationEndTime indicates cancelled procedure
		OperationDetail.OperationEndDate is not null

	union all

	select
		 Encounter.EncounterRecno
		,ProcedureDetail.SourceUniqueID
	from
		APC.Encounter Encounter

	inner join Theatre.PatientBooking PatientBooking
	on
		(
			PatientBooking.Surname = Encounter.PatientSurname
		and	PatientBooking.Forename = Encounter.PatientForename
		and	PatientBooking.SexCode = Encounter.SexCode
		and	PatientBooking.DateOfBirth = Encounter.DateOfBirth
		)

	inner join Theatre.OperationDetail OperationDetail
	on	OperationDetail.PatientBookingSourceUniqueID = PatientBooking.SourceUniqueID
	and	dateadd(day, datediff(day, 0, OperationDetail.OperationStartDate), 0) between Encounter.EpisodeStartDate and coalesce(Encounter.EpisodeEndDate, getdate())

	inner join Theatre.ProcedureDetail ProcedureDetail
	on	ProcedureDetail.OperationDetailSourceUniqueID = OperationDetail.SourceUniqueID

	where
	--null OperationEndTime indicates cancelled procedure
		OperationDetail.OperationEndDate is not null
	) Encounter

select
	@inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())



select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

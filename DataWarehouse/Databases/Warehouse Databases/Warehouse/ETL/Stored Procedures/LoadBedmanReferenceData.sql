﻿
CREATE proc ETL.LoadBedmanReferenceData

as

--fall

truncate table Bedman.FallSeverityBase

insert into Bedman.FallSeverityBase
(
	ID
	,[Description]
	,Active
	,[Order]
)

select
	[ID]
	,[Description]
	,[Active]
	,[Order]
from
	[$(BedmanTest)].[dbo].[FallSeverity]

--pressure ulcer

truncate table Bedman.PressureUlcerCategoryBase

insert into Bedman.PressureUlcerCategoryBase
(
	ID
	,[Description]
	,Active
	,[Order]
)
select
	[ID]
	,[Description]
	,[Active]
	,[Order]
from
	[$(BedmanTest)].[dbo].[PressureUlcerCategory]


truncate table Bedman.PressureUlcerLocationBase

insert into Bedman.PressureUlcerLocationBase
(
	ID
	,[Description]
	,Active
	,[Order]
)

select
	[ID]
	,[Description]
	,[Active]
	,[Order]
from
	[$(BedmanTest)].[dbo].[PressureUlcerLocation]

--catheter

truncate table Bedman.CatheterTypeBase

insert into Bedman.CatheterTypeBase
(
	ID
	,[Description]
	,Active
	,[Order]
)

select
	[ID]
	,[Description]
	,[Active]
	,[Order]
from
	[$(BedmanTest)].[dbo].[CatheterType]


--vte

truncate table Bedman.VTEAssessmentNotRequiredReasonBase

insert into Bedman.VTEAssessmentNotRequiredReasonBase
(
	ID
	,[Description]
	,Active
	,[Order]
)

select
	[ID]
	,[Description]
	,[Active]
	,[Order]
from
	[$(BedmanTest)].[dbo].[VTEAssessNotRequiredReasons]


truncate table Bedman.VTETypeBase

insert into Bedman.VTETypeBase
(
	ID
	,[Description]
	,Active
	,[Order]
)

select
	[ID]
	,[Description]
	,[Active]
	,[Order]
from
	[$(BedmanTest)].[dbo].[VTEType]
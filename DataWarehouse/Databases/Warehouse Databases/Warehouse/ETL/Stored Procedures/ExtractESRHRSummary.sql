﻿

CREATE proc [ETL].[ExtractESRHRSummary]

	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null

as

/*
	The cost centre code is used as a code to describe level 5 organisations. Problem is that for District Nursing
	there are multiple level 5 organisations that share the same cost centre. There is only one other example of this
	so rather than create codes we will just remain at the cost centre level so we need to aggregate the data against the
	cost centre attribute

20150109	RR	Brought in AC Agency Spend
20150609	RR	Brought through additional Turnover fields for monthly data.  Renamed columns for current process to 3mth rolling
*/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

select @fromDate = coalesce(@fromDate, (select cast(dateadd(year, -1, getdate()) as date)))
select @toDate = coalesce(@toDate, (select cast(getdate() as date)))


truncate table ETL.TImportHRSummary


insert into ETL.TImportHRSummary

(
	OrganisationCode
	,CensusDate
	,AppraisalRequired
	,AppraisalCompleted
	,ClinicalMandatoryTrainingRequired
	,ClinicalMandatoryTrainingCompleted
	,CorporateMandatoryTrainingRequired
	,CorporateMandatoryTrainingCompleted
	,RecentAppointment
	,LongTermAppointment
	,SicknessAbsenceFTE
	,SicknessEstablishmentFTE
	,Headcount3mthRolling
	,FTE3mthRolling
	,StartersHeadcount3mthRolling
	,StartersFTE3mthRolling
	,LeaversHeadcount3mthRolling
	,LeaversFTE3mthRolling
	,BudgetWTE
	,ContractedWTE
	,ClinicalBudgetWTE 
	,ClinicalContractedWTE
	--20150109
	,ACAgencySpend
	--20150609
	,HeadcountMonthly
	,FTEMonthly
	,StartersHeadcountMonthly
	,StartersFTEMonthly
	,LeaversHeadcountMonthly
	,LeaversFTEMonthly
	,BMERecentAppointment
	,BMELongTermAppointment
)


select
	OrganisationCode = Organisation.OrganisationCode
	--,Organisation = nullif(Organisation.Organisation, '#N/A')
	,CensusDate = Organisation.CensusDate

	,AppraisalRequired = Appraisal.AppraisalRequired
	,AppraisalCompleted = Appraisal.AppraisalCompleted

	,ClinicalMandatoryTrainingRequired = ClinicalMandatoryTraining.ClinicalMandatoryTrainingRequired
	,ClinicalMandatoryTrainingCompleted = ClinicalMandatoryTraining.ClinicalMandatoryTrainingCompleted

	,CorporateMandatoryTrainingRequired = CorporateMandatoryTraining.CorporateMandatoryTrainingRequired
	,CorporateMandatoryTrainingCompleted = CorporateMandatoryTraining.CorporateMandatoryTrainingCompleted

	,RecentAppointment = RetentionStaging.RecentAppointment
	,LongTermAppointment = RetentionStaging.LongTermAppointment

	,SicknessAbsenceFTE = SicknessStaging.SicknessAbsenceFTE
	,SicknessEstablishmentFTE = SicknessStaging.EstablishmentFTE

	,Headcount3mthRolling = TurnoverRollingMonthStaging.Headcount3mthRolling
	,FTE3mthRolling = TurnoverRollingMonthStaging.FTE3mthRolling
	,StartersHeadcount3mthRolling = TurnoverRollingMonthStaging.StartersHeadcount3mthRolling
	,StartersFTE3mthRolling = TurnoverRollingMonthStaging.StartersFTE3mthRolling
	,LeaversHeadcount3mthRolling = TurnoverRollingMonthStaging.LeaversHeadcount3mthRolling
	,LeaversFTE3mthRolling = TurnoverRollingMonthStaging.LeaversFTE3mthRolling

	,BudgetWTE = WTEStaging.BudgetWTE
	,ContractedWTE = WTEStaging.ContractedWTE

	,ClinicalBudgetWTE = ClinicalWTEStaging.ClinicalBudgetWTE
	,ClinicalContractedWTE = ClinicalWTEStaging.ClinicalContractedWTE
	,ACAgencySpendStaging.ACAgencySpend
	
	,HeadcountMonthly = TurnoverStaging.HeadcountMonthly
	,FTEMonthly = TurnoverStaging.FTEMonthly
	,StartersHeadcountMonthly = TurnoverStaging.StartersHeadcountMonthly
	,StartersFTEMonthly = TurnoverStaging.StartersFTEMonthly
	,LeaversHeadcountMonthly = TurnoverStaging.LeaversHeadcountMonthly
	,LeaversFTEMonthly = TurnoverStaging.LeaversFTEMonthly
	
	,BMERecent = BMERetention.BMERecentAppointment
	,BMELongTerm = BMERetention.BMELongTermAppointment
	
from
	[$(SmallDatasets)].HR.Organisation

left outer join	
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,AppraisalRequired = sum([NUMBER OF STAFF WHO REQUIRE APPRAISAL])
		,AppraisalCompleted = sum([NUMBER OF STAFF THAT MEET REQUIREMENT])
	from
		[$(SmallDatasets)].HR.AppraisalStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) Appraisal
on	Organisation.OrganisationCode = Appraisal.OrganisationCode
and	Organisation.CensusDate = Appraisal.CensusDate


left outer join 
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,ClinicalMandatoryTrainingRequired = sum([NUMBER OF STAFF WHO REQUIRE CLINCAL MANDATORY COMPETENCY])
		,ClinicalMandatoryTrainingCompleted = sum([NUMBER OF STAFF THAT MEET REQUIREMENT])
	from
		[$(SmallDatasets)].HR.ClinicalMandatoryTrainingStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) ClinicalMandatoryTraining
on	Organisation.OrganisationCode = ClinicalMandatoryTraining.OrganisationCode
and	Organisation.CensusDate = ClinicalMandatoryTraining.CensusDate

left outer join
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,CorporateMandatoryTrainingRequired = sum([NUMBER OF STAFF WHO REQUIRE CORPORATE MANDATORY])
		,CorporateMandatoryTrainingCompleted = sum([NUMBER OF STAFF THAT MEET REQUIREMENT])
	from
		[$(SmallDatasets)].HR.CorporateMandatoryTrainingStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) CorporateMandatoryTraining
on	Organisation.OrganisationCode = CorporateMandatoryTraining.OrganisationCode
and	Organisation.CensusDate = CorporateMandatoryTraining.CensusDate

left outer join
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,RecentAppointment = sum(recent)
		,LongTermAppointment = sum([long term])

	from
		[$(SmallDatasets)].HR.RetentionStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) RetentionStaging
on	Organisation.OrganisationCode = RetentionStaging.OrganisationCode
and	Organisation.CensusDate = RetentionStaging.CensusDate

left outer join
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,SicknessAbsenceFTE = sum([Abs (FTE)])
		,EstablishmentFTE = sum([Avail (FTE)])

	from
		[$(SmallDatasets)].HR.SicknessStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) SicknessStaging
on	Organisation.OrganisationCode = SicknessStaging.OrganisationCode
and	Organisation.CensusDate = SicknessStaging.CensusDate


left outer join
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,HeadcountMonthly = sum([Headcount Monthly])
		,FTEMonthly = sum([FTE Monthly])
		,StartersHeadcountMonthly = sum([Starters Headcount Monthly])
		,StartersFTEMonthly = sum([Starters FTE Monthly])
		,LeaversHeadcountMonthly = sum([Leavers Headcount Monthly])
		,LeaversFTEMonthly = sum([Leavers FTE Monthly])
	from
		[$(SmallDatasets)].HR.TurnoverStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) TurnoverStaging
on	Organisation.OrganisationCode = coalesce(TurnoverStaging.OrganisationCode,-999)
and	Organisation.CensusDate = TurnoverStaging.CensusDate

left outer join
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,Headcount3mthRolling = sum([Headcount 3mthrolling])
		,FTE3mthRolling = sum([FTE 3mthrolling])
		,StartersHeadcount3mthRolling = sum([Starters Headcount 3mthrolling])
		,StartersFTE3mthRolling = sum([Starters FTE 3mthrolling])
		,LeaversHeadcount3mthRolling = sum([Leavers Headcount 3mthrolling])
		,LeaversFTE3mthRolling = sum([Leavers FTE 3mthrolling])
	from
		[$(SmallDatasets)].HR.TurnoverRollingMonthStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) TurnoverRollingMonthStaging
on	Organisation.OrganisationCode = coalesce(TurnoverRollingMonthStaging.OrganisationCode,-999)
and	Organisation.CensusDate = TurnoverRollingMonthStaging.CensusDate




left outer join
(

	select
		OrganisationCode = '349' + [Cost Centre] -- to match the format of the other HR datasets
		,CensusDate = [Census Date]
		,BudgetWTE = sum([Wte Budget])
		,ContractedWTE = sum([Wte Contracted])
	from
		[$(SmallDatasets)].HR.WTEStaging
	group by
		[Cost Centre]
		,[Census Date]

) WTEStaging
on	Organisation.OrganisationCode = WTEStaging.OrganisationCode
and	Organisation.CensusDate = WTEStaging.CensusDate


left outer join
(
	select
		OrganisationCode = '349' + [Cost Centre] -- to match the format of the other HR datasets
		,CensusDate = [Census Date]
		,ClinicalBudgetWTE = sum([Wte Budget])
		,ClinicalContractedWTE = sum([Wte Contracted])
	from
		[$(SmallDatasets)].HR.WTEStaging
	where
		[Staff Group] = 'Clinical'
	group by
		[Cost Centre]
		,[Census Date]

) ClinicalWTEStaging
on	Organisation.OrganisationCode = ClinicalWTEStaging.OrganisationCode
and	Organisation.CensusDate = ClinicalWTEStaging.CensusDate

left outer join
(
	select
		OrganisationCode = '349' + [Organisation Cost Centre] -- to match the format of the other HR datasets
		,CensusDate = [Census Date]
		,ACAgencySpend = sum([Current Month Actual SUM])
	from
		[$(SmallDatasets)].HR.AdminClericalAgencySpendStaging
	group by
		[Organisation Cost Centre]
		,[Census Date]

) ACAgencySpendStaging
on	Organisation.OrganisationCode = ACAgencySpendStaging.OrganisationCode
and	Organisation.CensusDate = ACAgencySpendStaging.CensusDate

left outer join	
(
	select
		OrganisationCode = [Organisation Cost Centre]
		,CensusDate
		,BMERecentAppointment = SUM(BMERecent)
		,BMELongTermAppointment = SUM(BMELongTerm)
	from
		[$(SmallDatasets)].HR.BMERetentionStaging
	group by
		[Organisation Cost Centre]
		,CensusDate

) BMERetention
on	Organisation.OrganisationCode = BMERetention.OrganisationCode
and	Organisation.CensusDate = BMERetention.CensusDate

where
	Organisation.CensusDate between @fromDate and @toDate



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime






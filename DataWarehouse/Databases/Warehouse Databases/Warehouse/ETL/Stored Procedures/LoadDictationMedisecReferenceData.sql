﻿
CREATE proc [ETL].[LoadDictationMedisecReferenceData]

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Stats varchar(255)
declare @Sproc varchar(255)

select @StartTime = getdate()


insert into Dictation.MedisecSystemUser
(
UserCode
,UserName
,UserType
,UserDepartment
)

select
	UserCode = rtrim(usercode)
	,UserName = rtrim(username)
	,UserType = rtrim(usertype)
	,UserDepartment = rtrim(userdept)
from
	[$(CMFT_MEDISEC)].dbo.UserMaster
where
	not exists
	(
	select
		1
	from
		 Dictation.MedisecSystemUser
	where
		MedisecSystemUser.UserCode = rtrim(UserMaster.usercode) collate database_default
	)


insert into Dictation.MedisecDepartment
(
DepartmentCode
,Department
)

select
	DepartmentCode = rtrim(zdeptcode)
	,Department = rtrim(zdeptcode)
from
	[$(CMFT_MEDISEC)].dbo.ZDept
where
	not exists
	(
	select
		1
	from
		 Dictation.MedisecDepartment
	where
		MedisecDepartment.DepartmentCode = rtrim(ZDept.zdeptcode) collate database_default
	)

select @Elapsed = datediff(minute, @StartTime, getdate())
select @Stats = 'Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
			
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime


﻿

CREATE PROCEDURE [ETL].[p_SSISRunPackageConfiguration]

AS

/******************************************************************************
**  Name: ETL.p_SSISRunPackageConfiguration
**  Purpose: Reads the SSISRunPackage process configuration parameters
**
**
**	NOTE - Please maintain this stored procedure via Visual Studio 
**         located in the source control database
**
*******************************************************************************
**  Modified: 
*******************************************************************************
** Date:     Author:      Description:
** --------  ------------ ---------------------------------------------------- 
******************************************************************************/

	SELECT 
		  ParameterName		= Parameter
		 ,ParameterValue	= TextValue

	FROM
		Utility.Parameter

	WHERE
			Parameter LIKE 'SSISRunPackage%' 
		OR	Parameter LIKE 'SSISNotification%'




﻿CREATE PROCEDURE [ETL].[LoadRFEncounter]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select
	@StartTime = getdate()

/* If no Load Start and End Date parameters supplied, use the earliest Episode End Date
   and the latest Episode Start Date */
select
	 @LoadStartDate = MIN(CONVERT(datetime, ReferralDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, ReferralDate)) 
from
	ETL.TLoadRFEncounter

delete from RF.Encounter 
where	
	Encounter.ReferralDate between @LoadStartDate and @LoadEndDate
and	not exists
	(
	select
		1
	from
		ETL.TLoadRFEncounter
	where
		TLoadRFEncounter.SourceUniqueID = Encounter.SourceUniqueID
	)


SELECT @RowsDeleted = @@Rowcount

update RF.Encounter
set
	 SourceUniqueID = TEncounter.SourceUniqueID
	,SourcePatientNo = TEncounter.SourcePatientNo
	,SourceEncounterNo = TEncounter.SourceEncounterNo
	,PatientTitle = TEncounter.PatientTitle
	,PatientForename = TEncounter.PatientForename
	,PatientSurname = TEncounter.PatientSurname
	,DateOfBirth = TEncounter.DateOfBirth
	,DateOfDeath = TEncounter.DateOfDeath
	,SexCode = TEncounter.SexCode
	,NHSNumber = TEncounter.NHSNumber
	,DistrictNo = TEncounter.DistrictNo
	,Postcode = TEncounter.Postcode
	,PatientAddress1 = TEncounter.PatientAddress1
	,PatientAddress2 = TEncounter.PatientAddress2
	,PatientAddress3 = TEncounter.PatientAddress3
	,PatientAddress4 = TEncounter.PatientAddress4
	,DHACode = TEncounter.DHACode
	,EthnicOriginCode = TEncounter.EthnicOriginCode
	,MaritalStatusCode = TEncounter.MaritalStatusCode
	,ReligionCode = TEncounter.ReligionCode
	,RegisteredGpCode = TEncounter.RegisteredGpCode
	,RegisteredGpPracticeCode = TEncounter.RegisteredGpPracticeCode
	,EpisodicGpCode = TEncounter.EpisodicGpCode
	,EpisodicGpPracticeCode = TEncounter.EpisodicGpPracticeCode
	,EpisodicGdpCode = TEncounter.EpisodicGdpCode
	,SiteCode = TEncounter.SiteCode
	,ConsultantCode = TEncounter.ConsultantCode
	,SpecialtyCode = TEncounter.SpecialtyCode
	,SourceOfReferralCode = TEncounter.SourceOfReferralCode
	,PriorityCode = TEncounter.PriorityCode
	,ReferralDate = TEncounter.ReferralDate
	,DischargeDate = TEncounter.DischargeDate
	,DischargeTime = TEncounter.DischargeTime
	,DischargeReasonCode = TEncounter.DischargeReasonCode
	,DischargeReason = TEncounter.DischargeReason
	,AdminCategoryCode = TEncounter.AdminCategoryCode
	,ContractSerialNo = TEncounter.ContractSerialNo
	,RTTPathwayID = TEncounter.RTTPathwayID
	,RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	,NextFutureAppointmentDate = TEncounter.NextFutureAppointmentDate
	,ReferralComment = TEncounter.ReferralComment
	,InterfaceCode = TEncounter.InterfaceCode

	,Updated = getdate()
	,ByWhom = system_user
	,PCTCode = TEncounter.PCTCode
from
	ETL.TLoadRFEncounter TEncounter
where
	TEncounter.SourceUniqueID = RF.Encounter.SourceUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO RF.Encounter
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,ReferralComment
	,InterfaceCode
	,Created
	,ByWhom
	,PCTCode
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,EpisodicGdpCode
	,SiteCode
	,ConsultantCode
	,SpecialtyCode
	,SourceOfReferralCode
	,PriorityCode
	,ReferralDate
	,DischargeDate
	,DischargeTime
	,DischargeReasonCode
	,DischargeReason
	,AdminCategoryCode
	,ContractSerialNo
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NextFutureAppointmentDate
	,ReferralComment
	,InterfaceCode

	,Created = getdate()
	,ByWhom = system_user
	,PCTCode
from
	ETL.TLoadRFEncounter
where
	not exists
	(
	select
		1
	from
		RF.Encounter
	where
		Encounter.SourceUniqueID = TLoadRFEncounter.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

EXEC Utility.WriteAuditLogEvent 'LoadRFEncounter', @Stats, @StartTime

print @Stats

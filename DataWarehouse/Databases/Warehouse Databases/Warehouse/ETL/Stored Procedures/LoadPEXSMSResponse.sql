﻿

CREATE proc [ETL].[LoadPEXSMSResponse]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime


select @FromDate = (select min(DischargeDate) from ETL.TLoadPEXSMSResponse)
select @ToDate = (select max(DischargeDate) from ETL.TLoadPEXSMSResponse)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	PEX.SMSResponse target
using
	(
	select
	 [ResponseID]
      ,[SurveyTakenID]
      ,[DischargeDate]
      ,[DischargeTime]
      ,[LocationID]
      ,WardID
      ,[SurveyDate]
      ,[SurveyTime]
      ,SurveyID
      ,[LastRating]
      ,[LastResponseTime]
      ,[Status]
      ,[SentTime]
      ,[QuestionID]
      ,[AnswerID]
      ,Answer
      ,[ResponseDate]      
      ,[ResponseTime]
      ,[QuestionTypeID]
      ,[ChannelID]
      ,[LocationTypeID]
	from
		ETL.TLoadPEXSMSResponse

	) source
	on	source.ResponseID = target.ResponseID
	and	source.SurveyTakenID = target.SurveyTakenID
	and	source.QuestionID = target.QuestionID
	and	source.AnswerID = target.AnswerID
	

	when not matched by source
	and target.DischargeDate between @FromDate and @ToDate

	then delete

	when not matched
	then
		insert
			(
			[ResponseID]
			,[SurveyTakenID]
			,[DischargeDate]
			,[DischargeTime]
			,[LocationID]
			,WardID
			,[SurveyDate]
			,[SurveyTime]
			,SurveyID
			,[LastRating]
			,[LastResponseTime]
			,[Status]
			,[SentTime]
			,[QuestionID]
			,[AnswerID]
			,Answer
			,[ResponseTime]
			,[ResponseDate]  
			,[QuestionTypeID]
			,[ChannelID]
			,[LocationTypeID]
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.[ResponseID]
			,source.[SurveyTakenID]
			,source.[DischargeDate]
			,source.[DischargeTime]
			,source.[LocationID]
			,source.WardID
			,source.[SurveyDate]
			,source.[SurveyTime]
			,source.SurveyID
			,source.[LastRating]
			,source.[LastResponseTime]
			,source.[Status]
			,source.[SentTime]
			,source.[QuestionID]
			,source.[AnswerID]
			,source.Answer
			,source.[ResponseTime]
			,source.[ResponseDate]  
			,source.[QuestionTypeID]
			,source.[ChannelID]
			,source.[LocationTypeID]
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	
			isnull(target.ResponseID, 0) = isnull(source.ResponseID, 0)
		and isnull(target.SurveyTakenID, 0) = isnull(source.SurveyTakenID, 0)
		and isnull(target.DischargeDate, getdate()) = isnull(source.DischargeDate, getdate())		
		and isnull(target.DischargeTime, getdate()) = isnull(source.DischargeTime, getdate())	
		and	isnull(target.LocationID, 0) = isnull(source.LocationID, 0)
		and	isnull(target.WardID, 0) = isnull(source.WardID, 0)
		and isnull(target.SurveyDate, getdate()) = isnull(source.SurveyDate, getdate())		
		and isnull(target.SurveyTime, getdate()) = isnull(source.SurveyTime, getdate())	
		and	isnull(target.SurveyID, 0) = isnull(source.SurveyID, 0)					
		and	isnull(target.LastRating, 0) = isnull(source.LastRating, 0)
		and isnull(target.LastResponseTime, getdate()) = isnull(source.LastResponseTime, getdate())					
		and isnull(target.[Status], 0) = isnull(source.[Status], 0)
		and isnull(target.SentTime, getdate()) = isnull(source.SentTime, getdate())
		and isnull(target.QuestionID, 0) = isnull(source.QuestionID, 0)
		and isnull(target.AnswerID, 0) = isnull(source.AnswerID, 0)
		and isnull(target.Answer, 0) = isnull(source.Answer, 0)
		and isnull(target.ResponseDate, getdate()) = isnull(source.ResponseDate, getdate())	
		and isnull(target.ResponseTime, getdate()) = isnull(source.ResponseTime, getdate())	
		and isnull(target.QuestionTypeID, 0) = isnull(source.QuestionTypeID, 0)
		and isnull(target.ChannelID, 0) = isnull(source.ChannelID, 0)		
		and isnull(target.LocationTypeID, 0) = isnull(source.LocationTypeID, 0)		
		)
	then
		update
		set
			target.ResponseID = source.ResponseID
			,target.SurveyTakenID = source.SurveyTakenID
			,target.DischargeDate = source.DischargeDate
			,target.DischargeTime = source.DischargeTime			
			,target.LocationID = source.LocationID
			,target.WardID = source.WardID
			,target.SurveyDate = source.SurveyDate		
			,target.SurveyTime = source.SurveyTime
			,target.SurveyID = source.SurveyID
			,target.LastRating = source.LastRating
			,target.LastResponseTime = source.LastResponseTime
			,target.[Status] = source.[Status]
			,target.SentTime = source.SentTime
			,target.QuestionID = source.QuestionID
			,target.AnswerID = source.AnswerID
			,target.Answer = source.Answer
			,target.ResponseDate = source.ResponseDate				
			,target.ResponseTime = source.ResponseTime	
			,target.QuestionTypeID = source.QuestionTypeID				
			,target.ChannelID = source.ChannelID
			,target.LocationTypeID = source.LocationTypeID			
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime









﻿CREATE procedure [ETL].[ExtractInquireWaitingListActivity]
	 @from smalldatetime
	,@to smalldatetime
as

set dateformat dmy
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @fromString varchar(12)
declare @toString varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

select
	 @fromString = convert(varchar, @from, 112) + '0000'
	,@toString = convert(varchar, @to, 112) + '2400'


insert into ETL.TImportWaitingListActivity
	(
	 SourceUniqueID
	,ActivityTypeCode
	,ActivityTime
	,AdminCategoryCode
	,BookingTypeCode
	,CancelledBy
	,ConsultantCode
	,DeferralEndDate
	,DeferralRevisedEndDate
	,DiagnosticGroupCode
	,SourceEntityRecno
	,SiteCode
	,SourcePatientNo
	,LastRevisionTime
	,LastRevisionUser
	,PreviousActivityID
	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode
	,SpecialtyCode
	,SuspensionReasonCode
	,TCIAcceptDate
	,TCITime
	,TCIOfferDate
	,PriorityCode
	,WaitingListCode
	,WardCode
	,CharterCancelCode
	,CharterCancelDeferFlag
	,CharterCancel
	,OpCancelledFlag
	,OpCancelled
	,PatientChoiceFlag
	,PatientChoice
	,WLSuspensionInitiatorCode
	,WLSuspensionInitiator

	)


select
	 SourceUniqueID =
		WLACTIVITYID + '||' + EpsActvTypeInt

	,ActivityTypeCode = EpsActvTypeInt
	,ActivityTime = EpsActvDtimeInt
	,AdminCategoryCode = Category
	,BookingTypeCode = BookingType
	,CancelledBy
	,ConsultantCode = Consultant
	,DeferralEndDate = DefEndDate
	,DeferralRevisedEndDate = DefRevisedEndDate
	,DiagnosticGroupCode = DiagGroup
	,SourceEntityRecno = EpisodeNumber
	,SiteCode = Hospital
	,SourcePatientNo = InternalPatientNumber
	,LastRevisionTime = LastRevisionDtime
	,LastRevisionUser = LastRevisionUID

	,PreviousActivityId = 
		convert(varchar, InternalPatientNumber) + '||' + 
		convert(varchar, EpisodeNumber) + '||' + 
		convert(varchar, PrevActivityDTimeInt) + '||' + 
		convert(varchar, PrevActivityInt)

	,Reason
	,Remark
	,RemovalComment
	,RemovalReasonCode = RemovalReason
	,SpecialtyCode = Specialty
	,SuspensionReasonCode = SuspReason
	,TCIAcceptDate = TCIAcceptDateInt
	,TCITime = TCIDTimeInt
	,TCIOfferDate = TCIOfferDateInt
	,PriorityCode = Urgency
	,WaitingListCode = WaitingList
	,WardCode = Ward
	,CharterCancelCode
	,CharterCancelDeferFlag = CharterCancelDeferInd
	,CharterCancel = CharterCancelDescription
	,OpCancelledFlag = OpCancelled
	,OpCancelled = OpCancelledDesc
	,PatientChoiceFlag = PatientChoice
	,PatientChoice = PatientChoiceDesc
	,WLSuspensionInitiatorCode = WlSuspensionInitiator
	,WLSuspensionInitiator = WLSuspensionInitiatorDesc

from
	(
	select
		 Activity.WLACTIVITYID
		,Activity.Activity
		,Activity.ActivityDate
		,Activity.ActivityTime
		,Activity.AdmEROD
		,Activity.AdmERODInt
		,Activity.BookingType
		,Activity.CancelledBy
		,Activity.Category
		,Activity.CharterCancelCode
		,Activity.CharterCancelDeferInd
		,Activity.CharterCancelDescription
		,Activity.Consultant
		,Activity.DefEndDate
		,Activity.DefEndDateInt
		,Activity.DefRevisedEndDate
		,Activity.DefRevisedEndDateInt
		,Activity.DiagGroup
		,Activity.EfgCode
		,Activity.EpisodeNumber
		,Activity.EpsActvDtimeInt
		,Activity.EpsActvTypeInt
		,Activity.FirstPbLetterSent
		,Activity.FirstPbReminderSent
		,Activity.Hospital
		,Activity.HrgRankedProcedureCode
		,Activity.HrgResourceGroup
		,Activity.InternalPatientNumber
		,Activity.LastRevisionDtime
		,Activity.LastRevisionUID
		,Activity.OpCancelled
		,Activity.OpCancelledDesc
		,Activity.PatSelfDeferral
		,Activity.PatSelfDeferralInt
		,Activity.PatientChoice
		,Activity.PatientChoiceDesc
		,Activity.PbResponseDate
		,Activity.PrevActivity
		,Activity.PrevActivityDTimeInt
		,Activity.PrevActivityDate
		,Activity.PrevActivityInt
		,Activity.PrevActivityTime
		,Activity.PrevConsultant
		,Activity.PrevDiagGroup
		,Activity.PrevHospital
		,Activity.PrevSpecialty
		,Activity.PrevWaitingList
		,Activity.Reason
		,Activity.Remark
		,RemovalComment = left(Activity.RemovalComment, 25) 
		,Activity.RemovalReason
		,Activity.SecondPbReminderSent
		,Activity.Specialty
		,Activity.SuspReason
		,Activity.TCIAcceptDate
		,Activity.TCIAcceptDateInt
		,Activity.TCIDTimeInt
		,Activity.TCIDate
		,Activity.TCILetterComment
		,Activity.TCILetterDate
		,Activity.TCIOfferDateInt
		,Activity.TCITime
		,Activity.Urgency
		,Activity.WLSuspensionInitiatorDesc
		,Activity.WaitingList
		,Activity.Ward
		,Activity.WlSuspensionInitiator
	from
		[$(PAS)].Inquire.WLACTIVITY Activity

	--inner join [$(PAS)].Inquire.HOSPDET
	--on	HOSPDET.HOSPDETID = Activity.Hospital

	where
		EpsActvDtimeInt between @fromString and @toString

	and	Activity.EpsActvTypeInt in (1, 2, 3, 4, 5, 6, 19)

	--and	Activity.EpsActvTypeInt between 1 and 19

	--and	Activity.Hospital in
	--	(
	--	select
	--		Hospital.HOSPDETID
	--	from
	--		HOSPDET Hospital
	--	)

	) Activity

--in the above where clause we need to use the EpsActvTypeInt and Hospital clauses to force the index to be used!

-- CCB 2010-12-09
-- Cache upgrade has caused some alteration to indexing...  added inner join to Hospital and changed the ActivityType filter

SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireWaitingListActivity', @Stats, @StartTime

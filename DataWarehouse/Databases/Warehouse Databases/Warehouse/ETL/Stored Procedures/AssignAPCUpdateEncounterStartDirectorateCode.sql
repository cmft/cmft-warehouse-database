﻿

CREATE procedure [ETL].[AssignAPCUpdateEncounterStartDirectorateCode] as

update
	APCUpdate.Encounter
set
	StartDirectorateCode = Activity.DirectorateCode
from
	(
	select
		 Encounter.SourceUniqueID
		,DivisionRuleBase.DirectorateCode
	from
		APCUpdate.Encounter Encounter

	inner join PAS.AdmissionMethod AdmissionMethod
	on	AdmissionMethod.AdmissionMethodCode = Encounter.AdmissionMethodCode

	inner join PAS.ManagementIntention ManagementIntention
	on	ManagementIntention.ManagementIntentionCode = Encounter.ManagementIntentionCode

	inner join WH.DivisionRuleBase
	on	DivisionRuleBase.SiteCode = Encounter.StartSiteCode
	and	DivisionRuleBase.SpecialtyCode = Encounter.SpecialtyCode

	and	
		(
		
		 dbo.f_get_patient_category(
								 AdmissionMethod.InternalCode
								,ManagementIntention.InternalCode
								,Encounter.AdmissionDate
								,Encounter.DischargeDate
								)
			= DivisionRuleBase.PatientCategoryCode
			--case
			
			----Elective Inpatient
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1' --Waiting List
			--		,'2' --Booked
			--		,'3' --Planned
			--		)
			--	and ManagementIntention.InternalCode in 
			--		(
			--		 '1' --INPATIENT
			--		,'3' --INTERVAL ADMISSION
			--		,'6' --BORN IN HOSP/ON WAY
			--		)
			--	then 'EL' --Elective


			----Elective Inpatients where intended management was daycase but patient stayed overnight
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode IN ('2','4','5') -- Regular day and night '2' -- DAY CASE
			--	and	Encounter.AdmissionDate < Encounter.DischargeDate
			--	then 'EL'--Elective


			----Elective Daycase
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode = '2'-- DAY CASE
			--	and	Encounter.AdmissionDate = Encounter.DischargeDate
			--	then 'DC'--Daycase
				
			
			----Elective Inaptient where intended management was daycase or regular but not discharged
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode IN ('2','4','5')-- DAY CASE, Regular day and night
			--	and	Encounter.DischargeDate IS NULL
			--	then 'EL'--Elective
	
	
			----Regular Day Case
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode IN ('4','5')-- Regular day and night
			--	and	Encounter.AdmissionDate = Encounter.DischargeDate
			--	then 'RD'--Regular
				
			----Non Elective
			--	else 'NE' --Non Elective
			
			--end
			--= DivisionRuleBase.PatientCategoryCode

		or	DivisionRuleBase.PatientCategoryCode is null
		)

	and	(
			Encounter.ConsultantCode = DivisionRuleBase.ConsultantCode
		or	DivisionRuleBase.ConsultantCode is null
		)

	and	(
			Encounter.StartWardTypeCode = DivisionRuleBase.WardCode
		or	DivisionRuleBase.WardCode is null
		)

	and	not exists
		(
		select
			1
		from
			WH.DivisionRuleBase Previous
		where
			Previous.SiteCode = Encounter.StartSiteCode
		and	Previous.SpecialtyCode = Encounter.SpecialtyCode

		and	
			(
				 dbo.f_get_patient_category(
								 AdmissionMethod.InternalCode
								,ManagementIntention.InternalCode
								,Encounter.AdmissionDate
								,Encounter.DischargeDate
								)
			= Previous.PatientCategoryCode
				
			--	case
			
			----Elective Inpatient
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1' --Waiting List
			--		,'2' --Booked
			--		,'3' --Planned
			--		)
			--	and ManagementIntention.InternalCode in 
			--		(
			--		 '1' --INPATIENT
			--		,'3' --INTERVAL ADMISSION
			--		,'6' --BORN IN HOSP/ON WAY
			--		)
			--	then 'EL' --Elective


			----Elective Inpatients where intended management was daycase but patient stayed overnight
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode IN ('2','4','5') -- Regular day and night '2' -- DAY CASE
			--	and	Encounter.AdmissionDate < Encounter.DischargeDate
			--	then 'EL'--Elective


			----Elective Daycase
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode = '2'-- DAY CASE
			--	and	Encounter.AdmissionDate = Encounter.DischargeDate
			--	then 'DC'--Daycase
				
			
			----Elective Inaptient where intended management was daycase or regular but not discharged
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode IN ('2','4','5')-- DAY CASE, Regular day and night
			--	and	Encounter.DischargeDate IS NULL
			--	then 'EL'--Elective
	
	
			----Regular Day Case
			--	when AdmissionMethod.InternalCode in 
			--		(
			--		 '1'--Waiting List
			--		,'2'--Booked
			--		,'3'--Planned
			--		)
			--	and ManagementIntention.InternalCode IN ('4','5')-- Regular day and night
			--	and	Encounter.AdmissionDate = Encounter.DischargeDate
			--	then 'RD'--Regular
				
			----Non Elective
			--	else 'NE' --Non Elective
			
			--end
			--	= Previous.PatientCategoryCode

			or	Previous.PatientCategoryCode is null
			)

		and	(
				Encounter.ConsultantCode = Previous.ConsultantCode
			or	Previous.ConsultantCode is null
			)

		and	(
				Encounter.StartWardTypeCode = Previous.WardCode
			or	Previous.WardCode is null
			)

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when Previous.WardCode is not null
				then 2
				else 0
				end +

				case
				when Previous.PatientCategoryCode is not null
				then 1
				else 0
				end

				>

				case
				when DivisionRuleBase.ConsultantCode is not null
				then 4
				else 0
				end +

				case
				when DivisionRuleBase.WardCode is not null
				then 2
				else 0
				end +

				case
				when DivisionRuleBase.PatientCategoryCode is not null
				then 1
				else 0
				end

			or
				(
					case
					when Previous.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when Previous.WardCode is not null
					then 2
					else 0
					end +

					case
					when Previous.PatientCategoryCode is not null
					then 1
					else 0
					end

					=

					case
					when DivisionRuleBase.ConsultantCode is not null
					then 4
					else 0
					end +

					case
					when DivisionRuleBase.WardCode is not null
					then 2
					else 0
					end +

					case
					when DivisionRuleBase.PatientCategoryCode is not null
					then 1
					else 0
					end

					and	Previous.DivisionRuleBaseRecno > DivisionRuleBase.DivisionRuleBaseRecno
				)
			)
		)
	) Activity

where
	Activity.SourceUniqueID = Encounter.SourceUniqueID





﻿
CREATE procedure [ETL].[LoadRenalPatientHemodialysisServices] (@from date = '19000101', @to date = '21000101') as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime
declare @Elapsed int

declare
	 @deleted int
	,@updated int
	,@inserted int

declare @Stats varchar(255)

select @StartTime = getdate()

declare @MergeSummary TABLE(Action nvarchar(10));

merge Renal.PatientHemodialysisServices target
using ETL.TLoadRenalPatientHemodialysisServices source
on source.SourceUniqueID = target.SourceUniqueID
when matched and source.ServiceStartTime between @from and @to
and not
(
	isnull(target.[OrderTableObjectID], 0) = isnull(source.[OrderTableObjectID], 0)
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[ServiceMasterName], '') = isnull(source.[ServiceMasterName], '')
	and isnull(target.[ServiceMasterObjectID], 0) = isnull(source.[ServiceMasterObjectID], 0)
	and isnull(target.[Notes], '') = isnull(source.[Notes], '')
	and isnull(target.[ServiceStartTime], getdate()) = isnull(source.[ServiceStartTime], getdate())
	and isnull(target.[DurationInMinutes], 0) = isnull(source.[DurationInMinutes], 0)
	and isnull(target.[Timing], '') = isnull(source.[Timing], '')
	and isnull(target.[AsOrdered], 0) = isnull(source.[AsOrdered], 0)
	and isnull(target.[Status], '') = isnull(source.[Status], '')
	and isnull(target.[IsCompleted], 0) = isnull(source.[IsCompleted], 0)
	and isnull(target.[ServiceType], '') = isnull(source.[ServiceType], '')
	and isnull(target.[ReasonNotDone], '') = isnull(source.[ReasonNotDone], '')
	and isnull(target.[NumberOfTreatments], 0) = isnull(source.[NumberOfTreatments], 0)
	and isnull(target.[MaximumTreatments], 0) = isnull(source.[MaximumTreatments], 0)
	and isnull(target.[EpisodeEventObjectID], 0) = isnull(source.[EpisodeEventObjectID], 0)
	and isnull(target.[EpisodeEvent], '') = isnull(source.[EpisodeEvent], '')
	and isnull(target.[Provider], '') = isnull(source.[Provider], '')
	and isnull(target.[Location], '') = isnull(source.[Location], '')
	and isnull(target.[StaffName], '') = isnull(source.[StaffName], '')
	and isnull(target.[StaffObjectID], 0) = isnull(source.[StaffObjectID], 0)
	and isnull(target.[ScheduledDate], getdate()) = isnull(source.[ScheduledDate], getdate())
	and isnull(target.[StaffSignature], '') = isnull(source.[StaffSignature], '')
	and isnull(target.[ProviderObjectID], 0) = isnull(source.[ProviderObjectID], 0)
	and isnull(target.[LocationObjectID], 0) = isnull(source.[LocationObjectID], 0)
	and isnull(target.[RequiredSignatureStaffObjectID], 0) = isnull(source.[RequiredSignatureStaffObjectID], 0)
	and isnull(target.[RequiredSignatureStaffName], '') = isnull(source.[RequiredSignatureStaffName], '')
	and isnull(target.[CoSignatureStaffObjectID], 0) = isnull(source.[CoSignatureStaffObjectID], 0)
	and isnull(target.[CoSignatureStaffName], '') = isnull(source.[CoSignatureStaffName], '')
	and isnull(target.[HemodialysisOrderObjectID], 0) = isnull(source.[HemodialysisOrderObjectID], 0)
	and isnull(target.[TargetWeight], 0) = isnull(source.[TargetWeight], 0)
	and isnull(target.[TargetWeightUnits], '') = isnull(source.[TargetWeightUnits], '')
	and isnull(target.[TargetWeightInKg], 0) = isnull(source.[TargetWeightInKg], 0)
	and isnull(target.[VascularAccessObjectID], 0) = isnull(source.[VascularAccessObjectID], 0)
	and isnull(target.[VascularAccessType], '') = isnull(source.[VascularAccessType], '')
	and isnull(target.[VascularAccessLocation], '') = isnull(source.[VascularAccessLocation], '')
	and isnull(target.[OtherAccessType], '') = isnull(source.[OtherAccessType], '')
	and isnull(target.[OtherAccessLocation], '') = isnull(source.[OtherAccessLocation], '')
	and isnull(target.[MachineSettingsBFR], 0) = isnull(source.[MachineSettingsBFR], 0)
	and isnull(target.[MachineSettingsDFR], 0) = isnull(source.[MachineSettingsDFR], 0)
	and isnull(target.[MachineSettingsTemperature], 0) = isnull(source.[MachineSettingsTemperature], 0)
	and isnull(target.[MachineSettingsTemperatureUnits], '') = isnull(source.[MachineSettingsTemperatureUnits], '')
	and isnull(target.[MachineSettingsTemperatureInCelsius], 0) = isnull(source.[MachineSettingsTemperatureInCelsius], 0)
	and isnull(target.[MachineSettingsDialysate], '') = isnull(source.[MachineSettingsDialysate], '')
	and isnull(target.[MachineSettingsPotassium], '') = isnull(source.[MachineSettingsPotassium], '')
	and isnull(target.[MachineSettingsCa], '') = isnull(source.[MachineSettingsCa], '')
	and isnull(target.[MachineSettingsNa], '') = isnull(source.[MachineSettingsNa], '')
	and isnull(target.[MachineSettingsGlucose], '') = isnull(source.[MachineSettingsGlucose], '')
	and isnull(target.[MachineSettingsBicarb], '') = isnull(source.[MachineSettingsBicarb], '')
	and isnull(target.[PreHemodialysisFlowsheetObjectID], 0) = isnull(source.[PreHemodialysisFlowsheetObjectID], 0)
	and isnull(target.[PreVitalsWeight], 0) = isnull(source.[PreVitalsWeight], 0)
	and isnull(target.[PreVitalsWeightUnits], '') = isnull(source.[PreVitalsWeightUnits], '')
	and isnull(target.[PreVitalsWeightInKg], 0) = isnull(source.[PreVitalsWeightInKg], 0)
	and isnull(target.[PreVitalsSittingBloodPressureSystolic], 0) = isnull(source.[PreVitalsSittingBloodPressureSystolic], 0)
	and isnull(target.[PreVitalsSittingBloodPressureDiastolic], 0) = isnull(source.[PreVitalsSittingBloodPressureDiastolic], 0)
	and isnull(target.[PreVitalsStandingBloodPressureSystolic], 0) = isnull(source.[PreVitalsStandingBloodPressureSystolic], 0)
	and isnull(target.[PreVitalsStandingBloodPressureDiastolic], 0) = isnull(source.[PreVitalsStandingBloodPressureDiastolic], 0)
	and isnull(target.[PreVitalsHeartRate], 0) = isnull(source.[PreVitalsHeartRate], 0)
	and isnull(target.[PreVitalsTemperature], 0) = isnull(source.[PreVitalsTemperature], 0)
	and isnull(target.[PreVitalsTemperatureUnits], '') = isnull(source.[PreVitalsTemperatureUnits], '')
	and isnull(target.[PreVitalsTemperatureInCelsius], 0) = isnull(source.[PreVitalsTemperatureInCelsius], 0)
	and isnull(target.[PostHemodialysisFlowsheetObjectID], 0) = isnull(source.[PostHemodialysisFlowsheetObjectID], 0)
	and isnull(target.[PostVitalsWeight], 0) = isnull(source.[PostVitalsWeight], 0)
	and isnull(target.[PostVitalsWeightUnits], '') = isnull(source.[PostVitalsWeightUnits], '')
	and isnull(target.[PostVitalsWeightInKg], 0) = isnull(source.[PostVitalsWeightInKg], 0)
	and isnull(target.[PostVitalsSittingBloodPressureSystolic], 0) = isnull(source.[PostVitalsSittingBloodPressureSystolic], 0)
	and isnull(target.[PostVitalsSittingBloodPressureDiastolic], 0) = isnull(source.[PostVitalsSittingBloodPressureDiastolic], 0)
	and isnull(target.[PostVitalsStandingBloodPressureSystolic], 0) = isnull(source.[PostVitalsStandingBloodPressureSystolic], 0)
	and isnull(target.[PostVitalsStandingBloodPressureDiastolic], 0) = isnull(source.[PostVitalsStandingBloodPressureDiastolic], 0)
	and isnull(target.[PostVitalsHeartRate], 0) = isnull(source.[PostVitalsHeartRate], 0)
	and isnull(target.[PostVitalsTemperature], 0) = isnull(source.[PostVitalsTemperature], 0)
	and isnull(target.[PostVitalsTemperatureUnits], '') = isnull(source.[PostVitalsTemperatureUnits], '')
	and isnull(target.[PostVitalsTemperatureInCelsius], 0) = isnull(source.[PostVitalsTemperatureInCelsius], 0)
	and isnull(target.[IDW], 0) = isnull(source.[IDW], 0)
	and isnull(target.[RunTimeGoal], 0) = isnull(source.[RunTimeGoal], 0)
	and isnull(target.[HDStartTime], getdate()) = isnull(source.[HDStartTime], getdate())
	and isnull(target.[HDEndTime], getdate()) = isnull(source.[HDEndTime], getdate())
	and isnull(target.[ActualRunTime], 0) = isnull(source.[ActualRunTime], 0)
	and isnull(target.[UltrafiltrationGoal], 0) = isnull(source.[UltrafiltrationGoal], 0)
	and isnull(target.[UltrafiltrationVolume], 0) = isnull(source.[UltrafiltrationVolume], 0)
	and isnull(target.[BloodVolumeProcessed], 0) = isnull(source.[BloodVolumeProcessed], 0)
	and isnull(target.[UltrafiltrationRate], 0) = isnull(source.[UltrafiltrationRate], 0)
	and isnull(target.[MeanDialysateFlow], 0) = isnull(source.[MeanDialysateFlow], 0)
	and isnull(target.[MeanBloodFlow], 0) = isnull(source.[MeanBloodFlow], 0)
	and isnull(target.[MeanVenousPressure], 0) = isnull(source.[MeanVenousPressure], 0)
	and isnull(target.[MeanArterialPressure], 0) = isnull(source.[MeanArterialPressure], 0)
	and isnull(target.[PreBun], 0) = isnull(source.[PreBun], 0)
	and isnull(target.[PostBun], 0) = isnull(source.[PostBun], 0)
	and isnull(target.[URR], 0) = isnull(source.[URR], 0)
	and isnull(target.[KtV], 0) = isnull(source.[KtV], 0)
	and isnull(target.[EKtV], 0) = isnull(source.[EKtV], 0)
	and isnull(target.[NPCR], 0) = isnull(source.[NPCR], 0)
	and isnull(target.[WatsonVolume], 0) = isnull(source.[WatsonVolume], 0)
	and isnull(target.[Kd], 0) = isnull(source.[Kd], 0)
	and isnull(target.[Urea], 0) = isnull(source.[Urea], 0)
	and isnull(target.[MachineCheckSummary], '') = isnull(source.[MachineCheckSummary], '')
	and isnull(target.[MachineCheckStaffName], '') = isnull(source.[MachineCheckStaffName], '')
	and isnull(target.[MachineCheckStaffInitials], '') = isnull(source.[MachineCheckStaffInitials], '')
	and isnull(target.[PreVitalsSittingHeartRate], 0) = isnull(source.[PreVitalsSittingHeartRate], 0)
	and isnull(target.[PreVitalsSittingBloodPressure], '') = isnull(source.[PreVitalsSittingBloodPressure], '')
	and isnull(target.[PreVitalsStandingBloodPressure], '') = isnull(source.[PreVitalsStandingBloodPressure], '')
	and isnull(target.[PreVitalsStandingHeartRate], 0) = isnull(source.[PreVitalsStandingHeartRate], 0)
	and isnull(target.[PostVitalsSittingHeartRate], 0) = isnull(source.[PostVitalsSittingHeartRate], 0)
	and isnull(target.[PostVitalsSittingBloodPressure], '') = isnull(source.[PostVitalsSittingBloodPressure], '')
	and isnull(target.[PostVitalsStandingBloodPressure], '') = isnull(source.[PostVitalsStandingBloodPressure], '')
	and isnull(target.[PostVitalsStandingHeartRate], 0) = isnull(source.[PostVitalsStandingHeartRate], 0)
	and isnull(target.[DialysisMachineObjectID], 0) = isnull(source.[DialysisMachineObjectID], 0)
	and isnull(target.[DialysisMachineName], '') = isnull(source.[DialysisMachineName], '')
	and isnull(target.[DialysisMachineType], '') = isnull(source.[DialysisMachineType], '')
	and isnull(target.[DialysisStationObjectID], 0) = isnull(source.[DialysisStationObjectID], 0)
	and isnull(target.[DialysisStationName], '') = isnull(source.[DialysisStationName], '')
	and isnull(target.[SpecialProcedures], '') = isnull(source.[SpecialProcedures], '')
	and isnull(target.[ServiceDetails], '') = isnull(source.[ServiceDetails], '')
)
then update set
	target.[OrderTableObjectID] = source.[OrderTableObjectID]
	,target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[ServiceMasterName] = source.[ServiceMasterName]
	,target.[ServiceMasterObjectID] = source.[ServiceMasterObjectID]
	,target.[Notes] = source.[Notes]
	,target.[ServiceStartTime] = source.[ServiceStartTime]
	,target.[DurationInMinutes] = source.[DurationInMinutes]
	,target.[Timing] = source.[Timing]
	,target.[AsOrdered] = source.[AsOrdered]
	,target.[Status] = source.[Status]
	,target.[IsCompleted] = source.[IsCompleted]
	,target.[ServiceType] = source.[ServiceType]
	,target.[ReasonNotDone] = source.[ReasonNotDone]
	,target.[NumberOfTreatments] = source.[NumberOfTreatments]
	,target.[MaximumTreatments] = source.[MaximumTreatments]
	,target.[EpisodeEventObjectID] = source.[EpisodeEventObjectID]
	,target.[EpisodeEvent] = source.[EpisodeEvent]
	,target.[Provider] = source.[Provider]
	,target.[Location] = source.[Location]
	,target.[StaffName] = source.[StaffName]
	,target.[StaffObjectID] = source.[StaffObjectID]
	,target.[ScheduledDate] = source.[ScheduledDate]
	,target.[StaffSignature] = source.[StaffSignature]
	,target.[ProviderObjectID] = source.[ProviderObjectID]
	,target.[LocationObjectID] = source.[LocationObjectID]
	,target.[RequiredSignatureStaffObjectID] = source.[RequiredSignatureStaffObjectID]
	,target.[RequiredSignatureStaffName] = source.[RequiredSignatureStaffName]
	,target.[CoSignatureStaffObjectID] = source.[CoSignatureStaffObjectID]
	,target.[CoSignatureStaffName] = source.[CoSignatureStaffName]
	,target.[HemodialysisOrderObjectID] = source.[HemodialysisOrderObjectID]
	,target.[TargetWeight] = source.[TargetWeight]
	,target.[TargetWeightUnits] = source.[TargetWeightUnits]
	,target.[TargetWeightInKg] = source.[TargetWeightInKg]
	,target.[VascularAccessObjectID] = source.[VascularAccessObjectID]
	,target.[VascularAccessType] = source.[VascularAccessType]
	,target.[VascularAccessLocation] = source.[VascularAccessLocation]
	,target.[OtherAccessType] = source.[OtherAccessType]
	,target.[OtherAccessLocation] = source.[OtherAccessLocation]
	,target.[MachineSettingsBFR] = source.[MachineSettingsBFR]
	,target.[MachineSettingsDFR] = source.[MachineSettingsDFR]
	,target.[MachineSettingsTemperature] = source.[MachineSettingsTemperature]
	,target.[MachineSettingsTemperatureUnits] = source.[MachineSettingsTemperatureUnits]
	,target.[MachineSettingsTemperatureInCelsius] = source.[MachineSettingsTemperatureInCelsius]
	,target.[MachineSettingsDialysate] = source.[MachineSettingsDialysate]
	,target.[MachineSettingsPotassium] = source.[MachineSettingsPotassium]
	,target.[MachineSettingsCa] = source.[MachineSettingsCa]
	,target.[MachineSettingsNa] = source.[MachineSettingsNa]
	,target.[MachineSettingsGlucose] = source.[MachineSettingsGlucose]
	,target.[MachineSettingsBicarb] = source.[MachineSettingsBicarb]
	,target.[PreHemodialysisFlowsheetObjectID] = source.[PreHemodialysisFlowsheetObjectID]
	,target.[PreVitalsWeight] = source.[PreVitalsWeight]
	,target.[PreVitalsWeightUnits] = source.[PreVitalsWeightUnits]
	,target.[PreVitalsWeightInKg] = source.[PreVitalsWeightInKg]
	,target.[PreVitalsSittingBloodPressureSystolic] = source.[PreVitalsSittingBloodPressureSystolic]
	,target.[PreVitalsSittingBloodPressureDiastolic] = source.[PreVitalsSittingBloodPressureDiastolic]
	,target.[PreVitalsStandingBloodPressureSystolic] = source.[PreVitalsStandingBloodPressureSystolic]
	,target.[PreVitalsStandingBloodPressureDiastolic] = source.[PreVitalsStandingBloodPressureDiastolic]
	,target.[PreVitalsHeartRate] = source.[PreVitalsHeartRate]
	,target.[PreVitalsTemperature] = source.[PreVitalsTemperature]
	,target.[PreVitalsTemperatureUnits] = source.[PreVitalsTemperatureUnits]
	,target.[PreVitalsTemperatureInCelsius] = source.[PreVitalsTemperatureInCelsius]
	,target.[PostHemodialysisFlowsheetObjectID] = source.[PostHemodialysisFlowsheetObjectID]
	,target.[PostVitalsWeight] = source.[PostVitalsWeight]
	,target.[PostVitalsWeightUnits] = source.[PostVitalsWeightUnits]
	,target.[PostVitalsWeightInKg] = source.[PostVitalsWeightInKg]
	,target.[PostVitalsSittingBloodPressureSystolic] = source.[PostVitalsSittingBloodPressureSystolic]
	,target.[PostVitalsSittingBloodPressureDiastolic] = source.[PostVitalsSittingBloodPressureDiastolic]
	,target.[PostVitalsStandingBloodPressureSystolic] = source.[PostVitalsStandingBloodPressureSystolic]
	,target.[PostVitalsStandingBloodPressureDiastolic] = source.[PostVitalsStandingBloodPressureDiastolic]
	,target.[PostVitalsHeartRate] = source.[PostVitalsHeartRate]
	,target.[PostVitalsTemperature] = source.[PostVitalsTemperature]
	,target.[PostVitalsTemperatureUnits] = source.[PostVitalsTemperatureUnits]
	,target.[PostVitalsTemperatureInCelsius] = source.[PostVitalsTemperatureInCelsius]
	,target.[IDW] = source.[IDW]
	,target.[RunTimeGoal] = source.[RunTimeGoal]
	,target.[HDStartTime] = source.[HDStartTime]
	,target.[HDEndTime] = source.[HDEndTime]
	,target.[ActualRunTime] = source.[ActualRunTime]
	,target.[UltrafiltrationGoal] = source.[UltrafiltrationGoal]
	,target.[UltrafiltrationVolume] = source.[UltrafiltrationVolume]
	,target.[BloodVolumeProcessed] = source.[BloodVolumeProcessed]
	,target.[UltrafiltrationRate] = source.[UltrafiltrationRate]
	,target.[MeanDialysateFlow] = source.[MeanDialysateFlow]
	,target.[MeanBloodFlow] = source.[MeanBloodFlow]
	,target.[MeanVenousPressure] = source.[MeanVenousPressure]
	,target.[MeanArterialPressure] = source.[MeanArterialPressure]
	,target.[PreBun] = source.[PreBun]
	,target.[PostBun] = source.[PostBun]
	,target.[URR] = source.[URR]
	,target.[KtV] = source.[KtV]
	,target.[EKtV] = source.[EKtV]
	,target.[NPCR] = source.[NPCR]
	,target.[WatsonVolume] = source.[WatsonVolume]
	,target.[Kd] = source.[Kd]
	,target.[Urea] = source.[Urea]
	,target.[MachineCheckSummary] = source.[MachineCheckSummary]
	,target.[MachineCheckStaffName] = source.[MachineCheckStaffName]
	,target.[MachineCheckStaffInitials] = source.[MachineCheckStaffInitials]
	,target.[PreVitalsSittingHeartRate] = source.[PreVitalsSittingHeartRate]
	,target.[PreVitalsSittingBloodPressure] = source.[PreVitalsSittingBloodPressure]
	,target.[PreVitalsStandingBloodPressure] = source.[PreVitalsStandingBloodPressure]
	,target.[PreVitalsStandingHeartRate] = source.[PreVitalsStandingHeartRate]
	,target.[PostVitalsSittingHeartRate] = source.[PostVitalsSittingHeartRate]
	,target.[PostVitalsSittingBloodPressure] = source.[PostVitalsSittingBloodPressure]
	,target.[PostVitalsStandingBloodPressure] = source.[PostVitalsStandingBloodPressure]
	,target.[PostVitalsStandingHeartRate] = source.[PostVitalsStandingHeartRate]
	,target.[DialysisMachineObjectID] = source.[DialysisMachineObjectID]
	,target.[DialysisMachineName] = source.[DialysisMachineName]
	,target.[DialysisMachineType] = source.[DialysisMachineType]
	,target.[DialysisStationObjectID] = source.[DialysisStationObjectID]
	,target.[DialysisStationName] = source.[DialysisStationName]
	,target.[SpecialProcedures] = source.[SpecialProcedures]
	,target.[ServiceDetails] = source.[ServiceDetails]	,target.Updated = getdate()
	,target.ByWhom = system_user

when not matched by source then delete

when not matched by target then insert
(
	SourceUniqueID
	,[OrderTableObjectID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[ServiceMasterName]
	,[ServiceMasterObjectID]
	,[Notes]
	,[ServiceStartTime]
	,[DurationInMinutes]
	,[Timing]
	,[AsOrdered]
	,[Status]
	,[IsCompleted]
	,[ServiceType]
	,[ReasonNotDone]
	,[NumberOfTreatments]
	,[MaximumTreatments]
	,[EpisodeEventObjectID]
	,[EpisodeEvent]
	,[Provider]
	,[Location]
	,[StaffName]
	,[StaffObjectID]
	,[ScheduledDate]
	,[StaffSignature]
	,[ProviderObjectID]
	,[LocationObjectID]
	,[RequiredSignatureStaffObjectID]
	,[RequiredSignatureStaffName]
	,[CoSignatureStaffObjectID]
	,[CoSignatureStaffName]
	,[HemodialysisOrderObjectID]
	,[TargetWeight]
	,[TargetWeightUnits]
	,[TargetWeightInKg]
	,[VascularAccessObjectID]
	,[VascularAccessType]
	,[VascularAccessLocation]
	,[OtherAccessType]
	,[OtherAccessLocation]
	,[MachineSettingsBFR]
	,[MachineSettingsDFR]
	,[MachineSettingsTemperature]
	,[MachineSettingsTemperatureUnits]
	,[MachineSettingsTemperatureInCelsius]
	,[MachineSettingsDialysate]
	,[MachineSettingsPotassium]
	,[MachineSettingsCa]
	,[MachineSettingsNa]
	,[MachineSettingsGlucose]
	,[MachineSettingsBicarb]
	,[PreHemodialysisFlowsheetObjectID]
	,[PreVitalsWeight]
	,[PreVitalsWeightUnits]
	,[PreVitalsWeightInKg]
	,[PreVitalsSittingBloodPressureSystolic]
	,[PreVitalsSittingBloodPressureDiastolic]
	,[PreVitalsStandingBloodPressureSystolic]
	,[PreVitalsStandingBloodPressureDiastolic]
	,[PreVitalsHeartRate]
	,[PreVitalsTemperature]
	,[PreVitalsTemperatureUnits]
	,[PreVitalsTemperatureInCelsius]
	,[PostHemodialysisFlowsheetObjectID]
	,[PostVitalsWeight]
	,[PostVitalsWeightUnits]
	,[PostVitalsWeightInKg]
	,[PostVitalsSittingBloodPressureSystolic]
	,[PostVitalsSittingBloodPressureDiastolic]
	,[PostVitalsStandingBloodPressureSystolic]
	,[PostVitalsStandingBloodPressureDiastolic]
	,[PostVitalsHeartRate]
	,[PostVitalsTemperature]
	,[PostVitalsTemperatureUnits]
	,[PostVitalsTemperatureInCelsius]
	,[IDW]
	,[RunTimeGoal]
	,[HDStartTime]
	,[HDEndTime]
	,[ActualRunTime]
	,[UltrafiltrationGoal]
	,[UltrafiltrationVolume]
	,[BloodVolumeProcessed]
	,[UltrafiltrationRate]
	,[MeanDialysateFlow]
	,[MeanBloodFlow]
	,[MeanVenousPressure]
	,[MeanArterialPressure]
	,[PreBun]
	,[PostBun]
	,[URR]
	,[KtV]
	,[EKtV]
	,[NPCR]
	,[WatsonVolume]
	,[Kd]
	,[Urea]
	,[MachineCheckSummary]
	,[MachineCheckStaffName]
	,[MachineCheckStaffInitials]
	,[PreVitalsSittingHeartRate]
	,[PreVitalsSittingBloodPressure]
	,[PreVitalsStandingBloodPressure]
	,[PreVitalsStandingHeartRate]
	,[PostVitalsSittingHeartRate]
	,[PostVitalsSittingBloodPressure]
	,[PostVitalsStandingBloodPressure]
	,[PostVitalsStandingHeartRate]
	,[DialysisMachineObjectID]
	,[DialysisMachineName]
	,[DialysisMachineType]
	,[DialysisStationObjectID]
	,[DialysisStationName]
	,[SpecialProcedures]
	,[ServiceDetails]	
	,Created
	,Updated
	,ByWhom
)
values
(
	source.SourceUniqueID
	,source.[OrderTableObjectID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[ServiceMasterName]
	,source.[ServiceMasterObjectID]
	,source.[Notes]
	,source.[ServiceStartTime]
	,source.[DurationInMinutes]
	,source.[Timing]
	,source.[AsOrdered]
	,source.[Status]
	,source.[IsCompleted]
	,source.[ServiceType]
	,source.[ReasonNotDone]
	,source.[NumberOfTreatments]
	,source.[MaximumTreatments]
	,source.[EpisodeEventObjectID]
	,source.[EpisodeEvent]
	,source.[Provider]
	,source.[Location]
	,source.[StaffName]
	,source.[StaffObjectID]
	,source.[ScheduledDate]
	,source.[StaffSignature]
	,source.[ProviderObjectID]
	,source.[LocationObjectID]
	,source.[RequiredSignatureStaffObjectID]
	,source.[RequiredSignatureStaffName]
	,source.[CoSignatureStaffObjectID]
	,source.[CoSignatureStaffName]
	,source.[HemodialysisOrderObjectID]
	,source.[TargetWeight]
	,source.[TargetWeightUnits]
	,source.[TargetWeightInKg]
	,source.[VascularAccessObjectID]
	,source.[VascularAccessType]
	,source.[VascularAccessLocation]
	,source.[OtherAccessType]
	,source.[OtherAccessLocation]
	,source.[MachineSettingsBFR]
	,source.[MachineSettingsDFR]
	,source.[MachineSettingsTemperature]
	,source.[MachineSettingsTemperatureUnits]
	,source.[MachineSettingsTemperatureInCelsius]
	,source.[MachineSettingsDialysate]
	,source.[MachineSettingsPotassium]
	,source.[MachineSettingsCa]
	,source.[MachineSettingsNa]
	,source.[MachineSettingsGlucose]
	,source.[MachineSettingsBicarb]
	,source.[PreHemodialysisFlowsheetObjectID]
	,source.[PreVitalsWeight]
	,source.[PreVitalsWeightUnits]
	,source.[PreVitalsWeightInKg]
	,source.[PreVitalsSittingBloodPressureSystolic]
	,source.[PreVitalsSittingBloodPressureDiastolic]
	,source.[PreVitalsStandingBloodPressureSystolic]
	,source.[PreVitalsStandingBloodPressureDiastolic]
	,source.[PreVitalsHeartRate]
	,source.[PreVitalsTemperature]
	,source.[PreVitalsTemperatureUnits]
	,source.[PreVitalsTemperatureInCelsius]
	,source.[PostHemodialysisFlowsheetObjectID]
	,source.[PostVitalsWeight]
	,source.[PostVitalsWeightUnits]
	,source.[PostVitalsWeightInKg]
	,source.[PostVitalsSittingBloodPressureSystolic]
	,source.[PostVitalsSittingBloodPressureDiastolic]
	,source.[PostVitalsStandingBloodPressureSystolic]
	,source.[PostVitalsStandingBloodPressureDiastolic]
	,source.[PostVitalsHeartRate]
	,source.[PostVitalsTemperature]
	,source.[PostVitalsTemperatureUnits]
	,source.[PostVitalsTemperatureInCelsius]
	,source.[IDW]
	,source.[RunTimeGoal]
	,source.[HDStartTime]
	,source.[HDEndTime]
	,source.[ActualRunTime]
	,source.[UltrafiltrationGoal]
	,source.[UltrafiltrationVolume]
	,source.[BloodVolumeProcessed]
	,source.[UltrafiltrationRate]
	,source.[MeanDialysateFlow]
	,source.[MeanBloodFlow]
	,source.[MeanVenousPressure]
	,source.[MeanArterialPressure]
	,source.[PreBun]
	,source.[PostBun]
	,source.[URR]
	,source.[KtV]
	,source.[EKtV]
	,source.[NPCR]
	,source.[WatsonVolume]
	,source.[Kd]
	,source.[Urea]
	,source.[MachineCheckSummary]
	,source.[MachineCheckStaffName]
	,source.[MachineCheckStaffInitials]
	,source.[PreVitalsSittingHeartRate]
	,source.[PreVitalsSittingBloodPressure]
	,source.[PreVitalsStandingBloodPressure]
	,source.[PreVitalsStandingHeartRate]
	,source.[PostVitalsSittingHeartRate]
	,source.[PostVitalsSittingBloodPressure]
	,source.[PostVitalsStandingBloodPressure]
	,source.[PostVitalsStandingHeartRate]
	,source.[DialysisMachineObjectID]
	,source.[DialysisMachineName]
	,source.[DialysisMachineType]
	,source.[DialysisStationObjectID]
	,source.[DialysisStationName]
	,source.[SpecialProcedures]
	,source.[ServiceDetails]	
	,getdate()
	,getdate()
	,system_user
)

OUTPUT $Action INTO @MergeSummary;

select
	 @inserted = coalesce(sum(Inserted),0)
	,@updated = coalesce(sum(Updated),0)
	,@deleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @deleted)  + 
	', Updated '  + CONVERT(varchar(10), @updated) +  
	', Inserted '  + CONVERT(varchar(10), @inserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @from) + ' to ' + CONVERT(varchar(11), @to)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats


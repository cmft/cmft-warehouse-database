﻿
CREATE procedure [ETL].[LoadRADCheckRadiologyExtract]
(
	@SourceFileName varchar(1000)
)
AS

	/******************************************************************************
	**  Name: LoadRADCheckRadiologyExtract
	**  Purpose: 
	**
	**  Used in Import Radiology Data package
	**
	**	Returns a count value if the input parameter name matches a Radiology extract
	**  file name template.
	**
	**  Ensures only Radiology extract files are copied to the staging import area
	**  from the FTP download area.
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:		Author:		Description:
	** --------		----------	---------------------------------------------------- 
	**	20120626    MH			Altered for CMFT environment
	**	20150817	Paul Egan	Changed LIKE clause to enable loading backload stats
								automatically, appended with YYYYMM from CRIS
	******************************************************************************/

select
	CopyFileFlag = COUNT(*)
from
	ETL.RADImportFiles
where
	@SourceFileName like '%CMFTDW%' + LTRIM(RTRIM(Extract)) + '%_output%.csv'
	
/* Paul Egan 20150817 this was the original clause:
		@SourceFileName like '%' + LTRIM(RTRIM(Extract)) + '_output%.csv'
*/


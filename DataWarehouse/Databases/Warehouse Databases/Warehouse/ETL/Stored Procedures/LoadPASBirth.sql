﻿CREATE PROCEDURE [ETL].[LoadPASBirth]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


truncate table PAS.Birth

INSERT INTO PAS.Birth
(
	 BirthID
	,BabyPatientNo
	,BirthOrder
	,BirthWeight
	,CasenoteNumber
	,Comments
	,DeliveryDate
	,DeliveryTime
	,DrugsUsed
	,SourceSpellNo
	,SourcePatientNo
	,LiveStillBirth
	,DeliveryMethodCode
) 
select
	 BirthID
	,BabyPatientNo
	,BirthOrder
	,BirthWeight
	,CasenoteNumber
	,Comments
	,DeliveryDate
	,DeliveryTime
	,DrugsUsed
	,SourceSpellNo
	,SourcePatientNo
	,LiveStillBirth
	,DeliveryMethodCode
from
	ETL.TLoadPASBirth

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadPASBirth', @Stats, @StartTime

print @Stats

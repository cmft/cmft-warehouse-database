﻿
CREATE  Procedure [ETL].[LoadGUM] 
AS

	/******************************************************************************
	**  Name: LoadGUM
	**  Purpose: 
	**
	**  Calling stored procedure for loading GUM data 
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 17.10.12    MH         Initial coding
	******************************************************************************/

	DECLARE @StartTime datetime
	DECLARE @Elapsed int
	DECLARE @Stats varchar(255)

	SELECT @StartTime = getdate()

	SET NOCOUNT ON

	 --Summary data
	EXEC ETL.LoadGUMSummary


	--GUM data from MCSH
	exec ETL.LoadGUMReferenceData
	exec ETL.LoadGUMEncounter

	-- Run times
	SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

	SELECT @Stats = 'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

	EXEC Utility.WriteAuditLogEvent 'LoadGUM', @Stats, @StartTime
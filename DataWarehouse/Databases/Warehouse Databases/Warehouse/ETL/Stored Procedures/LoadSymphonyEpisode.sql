﻿
CREATE PROCEDURE [ETL].[LoadSymphonyEpisode]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Episode table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 EpisodeID
			,PatientID
			,EpisodeNumber
			,RecordedByID
			,CreatedByID
			,UpdatedTime
			,DepartmentID
			)
into
	#TLoadSymphonyEpisode
from
	(
	select
		 EpisodeID = cast(EpisodeID as int)
		,PatientID = cast(PatientID as int)
		,EpisodeNumber = cast(EpisodeNumber as varchar(20))
		,RecordedByID = cast(RecordedByID as int)
		,CreatedByID = cast(CreatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,DepartmentID = cast(DepartmentID as int)
	from
		(
		select
			 EpisodeID = Episodes.epd_id
			,PatientID = Episodes.epd_pid
			,EpisodeNumber = Episodes.epd_num
			,RecordedByID = Episodes.epd_recordedby
			,CreatedByID = Episodes.epd_createdby
			,UpdatedTime = Episodes.epd_update
			,DepartmentID = Episodes.epd_deptid
		from
			[$(Symphony)].dbo.Episodes
		where
			Episodes.epd_deleted <> 1

		) Encounter
	) Encounter
order by
	Encounter.EpisodeID

create unique clustered index #IX_TLoadSymphonyEpisode on #TLoadSymphonyEpisode
	(
	EpisodeID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Episode target
using
	(
	select
		 EpisodeID
		,PatientID
		,EpisodeNumber
		,RecordedByID
		,CreatedByID
		,UpdatedTime
		,DepartmentID
		,EncounterChecksum
	from
		#TLoadSymphonyEpisode
	
	) source
	on	source.EpisodeID = target.EpisodeID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 EpisodeID
			,PatientID
			,EpisodeNumber
			,RecordedByID
			,CreatedByID
			,UpdatedTime
			,DepartmentID
			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.EpisodeID
			,source.PatientID
			,source.EpisodeNumber
			,source.RecordedByID
			,source.CreatedByID
			,source.UpdatedTime
			,source.DepartmentID
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.EpisodeID = source.EpisodeID
			,target.PatientID = source.PatientID
			,target.EpisodeNumber = source.EpisodeNumber
			,target.RecordedByID = source.RecordedByID
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.DepartmentID = source.DepartmentID
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
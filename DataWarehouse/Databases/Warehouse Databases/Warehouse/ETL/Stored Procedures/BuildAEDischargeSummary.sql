﻿CREATE PROCEDURE [ETL].[BuildAEDischargeSummary] 
	@to date = null
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


set @to = 
	coalesce(@to, getdate())

declare @from date =
	dateadd(day, -14, @to)

--prepare observation data
;
with

Calendar as
	(
	select
		TheDate = dateadd(day, -14, @to)

	union all

	select 
		 dateadd(day, 1, TheDate) 
	from 
		 Calendar
	where 
		 dateadd(day, 1, TheDate)  <= @to
	)
	
select
	ObservationSet.ObservationSetID
	,CasenoteNumber = ObservationSet.CasenoteNumber collate SQL_Latin1_General_CP850_CI_AI

	,StartTime =
		dateadd(
			hour
			,DayLightSaving.Offset
			,ObservationSet.StartTime
		)

	,ObservationMeasure.ObservationMeasure
	,ObservationSetDetail.Result
	,ObservationSetDetail.ObservationSetDetailID
	,ObservationSet.OverallRiskIndexCode
into
	#Observation
from	
	PatientrackStaging.ObservationSet

inner join PatientrackStaging.ObservationSetDetail
on	ObservationSetDetail.ObservationSetID = ObservationSet.ObservationSetID

inner join PatientrackStaging.ObservationMeasure
on	ObservationMeasure.ObservationMeasureID = ObservationSetDetail.ObservationMeasureID

inner join WH.DayLightSaving
on	ObservationSet.StartTime between DayLightSaving.StartTime and DayLightSaving.EndTime

inner join Calendar
on
	Calendar.TheDate =
	CAST(
		dateadd(
			hour
			,DayLightSaving.Offset
			,ObservationSet.StartTime
		) 
	as date
	)
;


create index #IX_Observation on #Observation ([CasenoteNumber],[ObservationMeasure],[StartTime]) INCLUDE ([Result],[ObservationSetDetailID]);

--prepare attendance data
with

Calendar as
	(
	select
		TheDate = dateadd(day, -14, @to)

	union all

	select 
		 dateadd(day, 1, TheDate) 
	from 
		 Calendar
	where 
		 dateadd(day, 1, TheDate)  <= @to
	)


select
	Attendance.*
into
	#Attendance
from
	SymphonyStaging.Attendance

inner join SymphonyStaging.Episode
on	Episode.EpisodeID = Attendance.EpisodeID
and	Episode.IsDeleted = 0

left join SymphonyStaging.AttendanceLocation Departure
on	Departure.AttendanceID = Attendance.AttendanceID
and	Departure.LocationID in
		(
		select
			Location.LocationID
		from
			SymphonyStaging.Location
		where
			Location.Location = 'Left Department'
		)
		
and	Departure.AttendanceLocationID =
	(
	select top 1
		PreviousLocation.AttendanceLocationID
	from
		SymphonyStaging.AttendanceLocation PreviousLocation
	where
		PreviousLocation.AttendanceID = Attendance.AttendanceID
	and	PreviousLocation.LocationID in 
		(
		select
			Location.LocationID
		from
			SymphonyStaging.Location
		where
			Location.Location = 'Left Department'
		)	
	order by
		PreviousLocation.UpdatedTime desc
		,PreviousLocation.AttendanceLocationID desc
	)

inner join Calendar
on	cast(Departure.LocationTime as date) = Calendar.TheDate

left join Symphony.Reference AttendanceDisposal
on	AttendanceDisposal.ReferenceID = Attendance.AttendanceDisposalID

where
	Attendance.IsDeleted = 0

--Reportable
and	not
	(
		Attendance.AttendanceDisposalID in ('17756' , '15140') -- 'transfer' and 'Transfered to Other health care provider'
	and	Attendance.DischargeDestinationID = '15141' -- WIC1
	)

and right('0' + AttendanceDisposal.NationalReferenceCode, 2) != '01' -- Exclude Admitted Attendance Disposal Code

/* Added 31/03/2015 Paul Egan *** TEMP FIX - TO BE REMOVED WHEN FIXED BY DAVID RODWAY'S TEAM *** */
and Attendance.AttendanceNumber not in
	(
	'EEC-15-005631-1',
	'MAE-15-023756-1',
	'MAE-15-023793-1'

	)

and	Episode.DepartmentID = 1 --RW3MR
;

create unique clustered index #IX_Attendance on #Attendance (AttendanceID);


with

Observation as
	(
	select
		Observation.ObservationSetID
		,Observation.CasenoteNumber
		,Observation.StartTime	
		,Observation.ObservationMeasure
		,Result = cast(Observation.Result as int)
		,Observation.ObservationSetDetailID
		,Observation.OverallRiskIndexCode
	from	
		#Observation Observation
	)

,Investigation as
	(
	select
		 AttendanceID = Result.AttendanceID
		,DepartmentalInvestigation = ReferenceLookup.ReferenceLookup
		,InvestigationID = Investigations.Value
		,InvestigationTime = Result.ResultTime
	from
		SymphonyStaging.Result Result

	cross apply dbo.Split(Result.Field6ID, '||') Investigations

	left join SymphonyStaging.ReferenceLookup
	on	ReferenceLookup.ReferenceLookupID = Investigations.Value

	where
		Result.DataEntryProcedureID = 225
	)

,Treatment as
	(
	select
		 AttendanceID = Result.AttendanceID
		,DepartmentalTreatment = ReferenceLookup.ReferenceLookup
	from
		SymphonyStaging.Result Result

	cross apply dbo.Split(Result.Field6ID, '||') Treatments

	left join SymphonyStaging.ReferenceLookup
	on	ReferenceLookup.ReferenceLookupID = Treatments.Value

	where
		Result.DataEntryProcedureID in (182, 204, 221)
	)

,Medication as
	(
	select
		 AttendanceID = AttendanceExtraField.AttendanceID
		,Medication = ReferenceLookup.ReferenceLookup
	from
		SymphonyStaging.AttendanceExtraField

	cross apply dbo.Split(AttendanceExtraField.FieldValue, '||') Medications

	left join SymphonyStaging.ReferenceLookup
	on	ReferenceLookup.ReferenceLookupID = Medications.Value

	where
		AttendanceExtraField.FieldID in (1472)
	)

select
	DischargedByStaffMember =
		ltrim(
			DischargedByTitle.ReferenceLookup + ' ' +
			DischargedBy.Forename + ' ' +
			DischargedBy.Surname 
		)

	,DepartureTime = Departure.LocationTime
	,AttendanceNumber = Attendance.AttendanceNumber
	,PatientName = upper(Patient.Surname + ', ' + Patient.Forename)

	,PatientAddressLine1 =
		coalesce(
			 PermanentAddress.Line1
			,TemporaryAddress.Line1
			,''
		)

	,PatientAddressLine2 =
		coalesce(
			 PermanentAddress.Line2
			,TemporaryAddress.Line2
			,''
		)

	,PatientAddressLine3 =
		coalesce(
			 PermanentAddress.Line3
			,TemporaryAddress.Line3
			,''
		)

	,PatientAddressLine4 =
		coalesce(
			 PermanentAddress.Line4
			,TemporaryAddress.Line4
			,''
		)

	,PatientPostcode =
		coalesce(
			 PermanentAddress.Postcode
			,TemporaryAddress.Postcode
			,''
		)

	,Patient.DateOfBirth
	,NHSNumber = replace(NHSNumber.NHSNumber, ' ', '')
	,HospitalNumber = DistrictNo.DistrictNo
	,ArrivalDate = cast(Attendance.ArrivalTime as date)
	,ArrivalTime = cast(Attendance.ArrivalTime as time)
	,ArrivalMode = ArrivalMode.ReferenceLookup
	,SourceOfArrival = null

	,AccompaniedBy = AccompaniedBy.AccompaniedBy

	,PreviousAttendances = 
		(
		select
			count(*)
		from
			SymphonyStaging.Attendance EarlierEncounter

		inner join SymphonyStaging.Episode EarlierEpisode
		on	EarlierEpisode.EpisodeID = EarlierEncounter.EpisodeID

		where
			EarlierEncounter.ArrivalTime < Attendance.ArrivalTime
		and	EarlierEpisode.PatientID = Episode.PatientID
		) 

	,ReferredBy = SourceOfReferral.SourceOfReferral

	,SchoolAttended =
		replace(
			School.School
			,' (free text)'
			,''
		) +
		isnull(': ' + OtherSchool.School , '')

	,RegisteredGpCode = isnull(Gp.GpCode, '')

	,RegisteredGp = 
		isnull(
			GpTitle.ReferenceLookup + ' ' +
			Gp.Initials + ' ' +
			Gp.Surname 
			,''
		)

	,RegisteredGpPracticeCode = isnull(Practice.GpPracticeCode, '')
	,RegisteredGpAddressLine1 = isnull(PracticeAddress.Line1, '')
	,RegisteredGpAddressLine2 = isnull(PracticeAddress.Line2, '')
	,RegisteredGpAddressLine3 = isnull(PracticeAddress.Line3, '')
	,RegisteredGpAddressLine4 = isnull(PracticeAddress.Line4, '')
	,RegisteredGpAddressPostcode = isnull(PracticeAddress.Postcode, '')

	,PresentingCondition = PresentingCondition.ReferenceLookup

	,TriagedBy =
		ltrim(
			TriagedByTitle.ReferenceLookup + ' ' +
			TriagedBy.Forename + ' ' +
			TriagedBy.Surname
		)

	,BriefHistory =
		isnull(
			Attendance.PatientComplaint
			,''
		)

	,TriageComments = TriageComment.Note
	,AllergyOrSensitivity = AllergyOrSensitivity.ReferenceLookup

	,InitialObservationsPulse = InitialObservation.Field11ID
	,InitialObservationsSystolicBP = InitialObservation.Field9ID
	,InitialObservationsDiastolicBP = InitialObservation.Field10ID
	,InitialObservationsO2Saturation = InitialObservation.Field7ID
	,InitialObservationsRespiratoryRate = InitialObservation.Field8ID
	,InitialObservationsGCS = InitialObservationGCS.FieldValue
	,InitialObservationsEWS = InitialObservationEWS.FieldValue
	,InitialObservationsTemperature = InitialObservation.Field12ID
	,InitialObservationsPainScoreAtRest = InitialObservationPainScoreRestValue.ReferenceLookup
	,InitialObservationsPainScoreOnMovement = InitialObservationPainScoreMovementValue.ReferenceLookup
	,InitialObservationsBloodSugar  = InitialObservationBloodsugar.FieldValue
	,InitialObservationsFractionOfInspiredOxygen = InitialObservationFractionOfInspiredOxygen.FieldValue
	,InitialObservationsPerformPeakFlow = isnull(PerformPeakFlow.ReferenceLookup, '')
	,InitialObservationsCapRefill = isnull(InitialObservation.Field6ID, '')

	,FinalObservationsPulse = FinalObservationPulse.Result
	,FinalObservationsSystolicBP = FinalObservationSystolicBP.Result
	,FinalObservationsDiastolicBP = FinalObservationDiastolicBP.Result
	,FinalObservationsO2Saturation = FinalObservationO2Saturation.Result
	,FinalObservationsRespiratoryRate = FinalObservationRespiratoryRate.Result
	,FinalObservationsGCS = FinalObservationGCS.Result
	,FinalObservationsEWS = FinalObservationEWS.OverallRiskIndexCode

	,DutyConsultant =
		ltrim(
			DutyConsultantTitle.ReferenceLookup + ' ' +
			DutyConsultant.Forename + ' ' +
			DutyConsultant.Surname
		)

	,ClinicianSeen =
		ltrim(
			SeenByTitle.ReferenceLookup + ' ' +
			SeenBy.Forename + ' ' +
			SeenBy.Surname
		)

	,Diagnosis01 = Diagnosis01Code.ReferenceLookup
	,Diagnosis02 = Diagnosis02Code.ReferenceLookup

	,DepartmentalInvestigation = DepartmentalInvestigation.Investigation

	,DepartmentalInvestigationDetails = 
		(
		select top 1
			 Note.Note
		from
			SymphonyStaging.Result Result

		inner join SymphonyStaging.Note Note
		on	Note.NoteID = Result.NoteID

		where
			Result.DataEntryProcedureID = 225
		and	Result.AttendanceID = Attendance.AttendanceID
		and	Note.Note is not null
		and	Note.Note <> ''

		order by
			Note.NoteID
		)


	,Investigation01 = Investigation01.Investigation
	,Investigation02 = Investigation02.Investigation

	,Treatment = DepartmentalTreatment.Treatment
	,TreatmentDetails =
		(
		select top 1
			 Note.Note
		from
			SymphonyStaging.Result Result

		inner join SymphonyStaging.Note Note
		on	Note.NoteID = Result.NoteID

		where
			Result.DataEntryProcedureID in (182, 204, 221)
		and	Result.AttendanceID = Attendance.AttendanceID
		and	Note.Note is not null
		and	Note.Note <> ''

		order by
			Note.NoteID
		)


	,MedicationGiven = DepartmentalMedication.Medication

	,InformationForGP = DischargeNote.Note
	,AuditCScore = AuditCScore.FieldValue

	,FollowUpAdvice = 'Not Collected at MRI'
	,FollowUpArrangement = null
	,FollowUpTime = null

	,DischargeOutcome = DisposalBase.Disposal
	,DischargeOutcomeDetails = null
	,DischargeDestination = DischargeDestination.ReferenceLookup
	,DischargeMedicationGiven = null

	,LetterSentBy = 'CMFT Data Warehouse'
	,PASSourcePatientNo = cast(PASSourcePatientNo.PatientSystemId as int)
	,PASSourceEpisodeNo = cast(Attendance.EpisodeID as int)
	,DistrictNo = DistrictNo.DistrictNo		
	,AESourceUniqueID = Attendance.AttendanceID

into
	#AEDischargeSummary 

from
	#Attendance Attendance

left join SymphonyStaging.StaffMember DischargedBy
on	DischargedBy.StaffID = Attendance.DischargeCreatedByID

left join SymphonyStaging.ReferenceLookup DischargedByTitle
on	DischargedByTitle.ReferenceLookupID = DischargedBy.TitleID

left join SymphonyStaging.AttendanceLocation Departure
on	Departure.AttendanceID = Attendance.AttendanceID
and	Departure.LocationID in
		(
		select
			Location.LocationID
		from
			SymphonyStaging.Location
		where
			Location.Location = 'Left Department'
		)
		
and	Departure.AttendanceLocationID =
	(
	select top 1
		PreviousLocation.AttendanceLocationID
	from
		SymphonyStaging.AttendanceLocation PreviousLocation
	where
		PreviousLocation.AttendanceID = Attendance.AttendanceID
	and	PreviousLocation.LocationID in 
		(
		select
			Location.LocationID
		from
			SymphonyStaging.Location
		where
			Location.Location = 'Left Department'
		)	
	order by
		PreviousLocation.UpdatedTime desc
		,PreviousLocation.AttendanceLocationID desc
	)

inner join SymphonyStaging.Episode
on	Episode.EpisodeID = Attendance.EpisodeID
and	Episode.IsDeleted = 0

inner join SymphonyStaging.Patient
on	Patient.PatientID = Episode.PatientID
and	Patient.IsDeleted = 0

left join
	(
	select distinct
		 DistrictNo = PatientSystemId.PatientSystemId
		,PatientSystemId.PatientID
	from
		SymphonyStaging.PatientSystemId
	where
		PatientSystemId.SystemID = 
			(
			select
				ReferenceLookup.ReferenceLookupID
			from
				SymphonyStaging.ReferenceLookup
			where
				ReferenceLookup.ReferenceLookup = 'District Number'
			)
	and	not exists
		(
		select
			1
		from
			SymphonyStaging.PatientSystemId PreviousDistrictNo
		where
			PreviousDistrictNo.SystemID = PatientSystemId.SystemID
		and	PreviousDistrictNo.PatientID = PatientSystemId.PatientID
		and	PreviousDistrictNo.PatientSystemIdID > PatientSystemId.PatientSystemIdID
		)
	) DistrictNo
on	DistrictNo.PatientID = Patient.PatientID

left join
	(
	select
		*
	from
		SymphonyStaging.Address PermanentAddress
	where
		PermanentAddress.LinkTypeID = 1
	and	PermanentAddress.AddressTypeID = 2673
	and not exists
		(
		select
			1
		from
			SymphonyStaging.Address PreviousPermanentAddress
		where
			PreviousPermanentAddress.LinkID = PermanentAddress.LinkID
		and	PreviousPermanentAddress.LinkTypeID = PermanentAddress.LinkTypeID
		and	PreviousPermanentAddress.AddressTypeID = PermanentAddress.AddressTypeID
		and	PreviousPermanentAddress.UpdatedTime > PermanentAddress.UpdatedTime
		)
	) PermanentAddress
on	PermanentAddress.LinkID = Patient.PatientID

left join
	(
	select
		*
	from
		SymphonyStaging.Address TemporaryAddress
	where
		TemporaryAddress.LinkTypeID = 1
	and	TemporaryAddress.AddressTypeID in (4993, 10522)
	and not exists
		(
		select
			1
		from
			SymphonyStaging.Address PreviousTemporaryAddress
		where
			PreviousTemporaryAddress.LinkID = TemporaryAddress.LinkID
		and	PreviousTemporaryAddress.LinkTypeID = TemporaryAddress.LinkTypeID
		and	PreviousTemporaryAddress.AddressTypeID = TemporaryAddress.AddressTypeID
		and	PreviousTemporaryAddress.UpdatedTime > TemporaryAddress.UpdatedTime
		)
	) TemporaryAddress
on	TemporaryAddress.LinkID = Patient.PatientID


left join
	(
	select distinct
		 NHSNumber = PatientSystemId.PatientSystemId
		,PatientSystemId.PatientID
	from
		SymphonyStaging.PatientSystemId
	where
		PatientSystemId.SystemID = 
			(
			select
				ReferenceLookup.ReferenceLookupID
			from
				SymphonyStaging.ReferenceLookup
			where
				ReferenceLookup.ReferenceLookup = 'NHS Number'
			)
	and	not exists
		(
		select
			1
		from
			SymphonyStaging.PatientSystemId PreviousNHSNumber
		where
			PreviousNHSNumber.SystemID = PatientSystemId.SystemID
		and	PreviousNHSNumber.PatientID = PatientSystemId.PatientID
		and	PreviousNHSNumber.PatientSystemIdID > PatientSystemId.PatientSystemIdID
		)
	) NHSNumber
on	NHSNumber.PatientID = Patient.PatientID

left join SymphonyStaging.ReferenceLookup ArrivalMode
on	ArrivalMode.ReferenceLookupID = Attendance.ArrivalModeID

outer apply 
	(
	select distinct
		AccompaniedBy =
			substring(
				(
				select
					', ' + ReferenceLookup.ReferenceLookup as [text()]
				from
					dbo.Split(Attendance.AccompaniedBy, '||') AccompaniedBy

				left join SymphonyStaging.ReferenceLookup
				on	ReferenceLookup.ReferenceLookupID = AccompaniedBy.Value

				for XML PATH ('')
				)

				,3
				,4000
			)
	) AccompaniedBy

left join
	(
	select distinct
		 SourceOfReferralID = ReferenceLookupMapping.ReferenceLookupID
		,SourceOfReferralCode = Note.Note
	from
		SymphonyStaging.Note

	inner join SymphonyStaging.ReferenceLookupMapping
	on	ReferenceLookupMapping.ValueID = Note.NoteID
	and	ReferenceLookupMapping.ReferenceMappingTypeID = 
		(
		select
			MappingType.MappingTypeID
		from
			SymphonyStaging.MappingType
		where
			MappingType.MappingType = 'CDS'
		)
	) AttendanceSourceOfReferral
on	AttendanceSourceOfReferral.SourceOfReferralID = Attendance.SourceOfReferralID

left join AE.SourceOfReferral
on	SourceOfReferral.SourceOfReferralCode = AttendanceSourceOfReferral.SourceOfReferralCode collate database_default

left join
	(
	select
		 PatientID = PatientDetailExtraField.PatientID
		,SchoolCode = PatientDetailExtraField.FieldValue
		,School = ReferenceLookup.ReferenceLookup
	from
		SymphonyStaging.PatientDetailExtraField

	inner join SymphonyStaging.ReferenceLookup
	on	ReferenceLookup.ReferenceLookupID = PatientDetailExtraField.FieldValue

	where
		PatientDetailExtraField.DataEntryProcedureID = 56
	and	PatientDetailExtraField.FieldID = 1095
	and	not exists
		(
		select
			1
		from
			SymphonyStaging.PatientDetailExtraField LaterSchool
		where
			LaterSchool.PatientID = PatientDetailExtraField.PatientID
		and	LaterSchool.DataEntryProcedureID = PatientDetailExtraField.DataEntryProcedureID
		and	LaterSchool.FieldID = PatientDetailExtraField.FieldID

		and	LaterSchool.PatientDetailExtraFieldID > PatientDetailExtraField.PatientDetailExtraFieldID
		)
		
	) School
on	School.PatientID = Patient.PatientID

left join
	(
	select
		 PatientID = PatientDetailExtraField.PatientID
		,School = PatientDetailExtraField.FieldValue
	from
		SymphonyStaging.PatientDetailExtraField

	where
		PatientDetailExtraField.DataEntryProcedureID = 56
	and	PatientDetailExtraField.FieldID = 1094
	and	not exists
		(
		select
			1
		from
			SymphonyStaging.PatientDetailExtraField LaterSchool
		where
			LaterSchool.PatientID = PatientDetailExtraField.PatientID
		and	LaterSchool.DataEntryProcedureID = PatientDetailExtraField.DataEntryProcedureID
		and	LaterSchool.FieldID = PatientDetailExtraField.FieldID

		and	LaterSchool.PatientDetailExtraFieldID > PatientDetailExtraField.PatientDetailExtraFieldID
		)
		
	) OtherSchool
on	OtherSchool.PatientID = Patient.PatientID

left join SymphonyStaging.PatientDetail
on	PatientDetail.PatientID = Patient.PatientID

left join SymphonyStaging.Gp
on	Gp.GpID = PatientDetail.GpID

left join SymphonyStaging.ReferenceLookup GpTitle
on	GpTitle.ReferenceLookupID = Gp.TitleID

left join SymphonyStaging.GpAddressLink
on	GpAddressLink.GpID = Gp.GpID
and	GpAddressLink.AddressID = PatientDetail.PracticeAddressID

left join SymphonyStaging.Practice
on	Practice.GpPracticeID = GpAddressLink.GpPracticeID

left join SymphonyStaging.Address PracticeAddress
on	PracticeAddress.AddressID = GpAddressLink.AddressID

left join SymphonyStaging.ReferenceLookup PresentingCondition
on	PresentingCondition.ReferenceLookupID = Attendance.PresentingProblem

left join SymphonyStaging.Triage
on	Triage.AttendanceID = Attendance.AttendanceID
and	not exists
	(
	select
		1
	from
		SymphonyStaging.Triage Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.TriageID < Triage.TriageID
	)

left join SymphonyStaging.StaffMember TriagedBy
on	TriagedBy.StaffID = Triage.CreatedByID

left join SymphonyStaging.ReferenceLookup TriagedByTitle
on	TriagedByTitle.ReferenceLookupID = TriagedBy.TitleID

left join SymphonyStaging.Note TriageComment
on	TriageComment.NoteID = Triage.CommentsID

left join SymphonyStaging.PatientIndicator Allergy
on	Allergy.PatientID = Episode.PatientID
and	Allergy.IndicatorTypeID = 7673
and	not exists
	(
	select
		1
	from
		SymphonyStaging.PatientIndicator Previous
	where
		Previous.IndicatorTypeID = Allergy.IndicatorTypeID
	and	Previous.PatientID = Allergy.PatientID
	and	Previous.PatientIndicatorID > Allergy.PatientIndicatorID
	)

left join SymphonyStaging.ReferenceLookup AllergyOrSensitivity
on	AllergyOrSensitivity.ReferenceLookupID = Allergy.IndicatorID

left join SymphonyStaging.Result InitialObservation
on	InitialObservation.AttendanceID = Attendance.AttendanceID
and	InitialObservation.DataEntryProcedureID = 99
and	not exists
	(
	select
		1
	from
		SymphonyStaging.Result Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.DataEntryProcedureID = InitialObservation.DataEntryProcedureID
	and	Previous.ResultID < InitialObservation.ResultID
	)

left join SymphonyStaging.ResultExtraField InitialObservationGCS
on	InitialObservationGCS.AttendanceID = Attendance.AttendanceID
and	InitialObservationGCS.FieldID = 951
and	not exists
	(
	select
		1
	from
		SymphonyStaging.ResultExtraField Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.FieldID = InitialObservationGCS.FieldID
	and	Previous.ResultExtraFieldID < InitialObservationGCS.ResultExtraFieldID
	)

left join SymphonyStaging.ResultExtraField InitialObservationEWS
on	InitialObservationEWS.AttendanceID = Attendance.AttendanceID
and	InitialObservationEWS.FieldID = 950
and	not exists
	(
	select
		1
	from
		SymphonyStaging.ResultExtraField Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.FieldID = InitialObservationEWS.FieldID
	and	Previous.ResultExtraFieldID < InitialObservationEWS.ResultExtraFieldID
	)

left join SymphonyStaging.ResultExtraField InitialObservationPainScoreRest
on	InitialObservationPainScoreRest.AttendanceID = Attendance.AttendanceID
and	InitialObservationPainScoreRest.FieldID = 1236
and	not exists
	(
	select
		1
	from
		SymphonyStaging.ResultExtraField Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.FieldID = InitialObservationPainScoreRest.FieldID
	and	Previous.ResultExtraFieldID < InitialObservationPainScoreRest.ResultExtraFieldID
	)

left join SymphonyStaging.ReferenceLookup InitialObservationPainScoreRestValue
on	InitialObservationPainScoreRestValue.ReferenceLookupID = InitialObservationPainScoreRest.FieldValue

left join SymphonyStaging.ResultExtraField InitialObservationPainScoreMovement
on	InitialObservationPainScoreMovement.AttendanceID = Attendance.AttendanceID
and	InitialObservationPainScoreMovement.FieldID = 1237
and	not exists
	(
	select
		1
	from
		SymphonyStaging.ResultExtraField Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.FieldID = InitialObservationPainScoreMovement.FieldID
	and	Previous.ResultExtraFieldID < InitialObservationPainScoreMovement.ResultExtraFieldID
	)

left join SymphonyStaging.ReferenceLookup InitialObservationPainScoreMovementValue
on	InitialObservationPainScoreMovementValue.ReferenceLookupID = InitialObservationPainScoreMovement.FieldValue

left join SymphonyStaging.ResultExtraField InitialObservationBloodsugar
on	InitialObservationBloodsugar.AttendanceID = Attendance.AttendanceID
and	InitialObservationBloodsugar.FieldID = 1447
and	not exists
	(
	select
		1
	from
		SymphonyStaging.ResultExtraField Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.FieldID = InitialObservationBloodsugar.FieldID
	and	Previous.ResultExtraFieldID < InitialObservationBloodsugar.ResultExtraFieldID
	)

left join SymphonyStaging.ResultExtraField InitialObservationFractionOfInspiredOxygen
on	InitialObservationFractionOfInspiredOxygen.AttendanceID = Attendance.AttendanceID
and	InitialObservationFractionOfInspiredOxygen.FieldID = 2078
and	not exists
	(
	select
		1
	from
		SymphonyStaging.ResultExtraField Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.FieldID = InitialObservationFractionOfInspiredOxygen.FieldID
	and	Previous.ResultExtraFieldID < InitialObservationFractionOfInspiredOxygen.ResultExtraFieldID
	)


left join SymphonyStaging.ReferenceLookup PerformPeakFlow
on	PerformPeakFlow.ReferenceLookupID = InitialObservation.ResultID1

left join Observation FinalObservationPulse
on
	FinalObservationPulse.CasenoteNumber = --return the string less any trailing "-xxx..." characters e.g. PAS-12345-01 returns PAS-12345.
		reverse(
			right(
				reverse(Attendance.AttendanceNumber)
				,len(Attendance.AttendanceNumber) -
				charindex(
					'-'
					,reverse(Attendance.AttendanceNumber)
				)
			)
		)

and FinalObservationPulse.StartTime between Attendance.ArrivalTime and Departure.LocationTime
and	FinalObservationPulse.ObservationMeasure = 'Heart Rate'

and	FinalObservationPulse.ObservationSetDetailID =
	(
	select top 1
		Previous.ObservationSetDetailID
	from
		Observation Previous
	where
		Previous.CasenoteNumber = FinalObservationPulse.CasenoteNumber
	and	Previous.StartTime between Attendance.ArrivalTime and Departure.LocationTime
	and	Previous.ObservationMeasure = FinalObservationPulse.ObservationMeasure
	order by
		Previous.ObservationSetDetailID desc
	)

left join Observation FinalObservationSystolicBP
on
	FinalObservationSystolicBP.CasenoteNumber = --return the string less any trailing "-xxx..." characters e.g. PAS-12345-01 returns PAS-12345.
		reverse(
			right(
				reverse(Attendance.AttendanceNumber)
				,len(Attendance.AttendanceNumber) -
				charindex(
					'-'
					,reverse(Attendance.AttendanceNumber)
				)
			)
		)

and FinalObservationSystolicBP.StartTime between Attendance.ArrivalTime and Departure.LocationTime
and	FinalObservationSystolicBP.ObservationMeasure = 'Lying Systolic Blood Pressure'

and	FinalObservationSystolicBP.ObservationSetDetailID =
	(
	select top 1
		Previous.ObservationSetDetailID
	from
		Observation Previous
	where
		Previous.CasenoteNumber = FinalObservationSystolicBP.CasenoteNumber
	and	Previous.StartTime between Attendance.ArrivalTime and Departure.LocationTime
	and	Previous.ObservationMeasure = FinalObservationSystolicBP.ObservationMeasure
	order by
		Previous.ObservationSetDetailID desc
	)

left join Observation FinalObservationDiastolicBP
on
	FinalObservationDiastolicBP.CasenoteNumber = --return the string less any trailing "-xxx..." characters e.g. PAS-12345-01 returns PAS-12345.
		reverse(
			right(
				reverse(Attendance.AttendanceNumber)
				,len(Attendance.AttendanceNumber) -
				charindex(
					'-'
					,reverse(Attendance.AttendanceNumber)
				)
			)
		)

and FinalObservationDiastolicBP.StartTime between Attendance.ArrivalTime and Departure.LocationTime
and	FinalObservationDiastolicBP.ObservationMeasure = 'Lying Diastolic Blood Pressure'

and	FinalObservationDiastolicBP.ObservationSetDetailID =
	(
	select top 1
		Previous.ObservationSetDetailID
	from
		Observation Previous
	where
		Previous.CasenoteNumber = FinalObservationDiastolicBP.CasenoteNumber
	and	Previous.StartTime between Attendance.ArrivalTime and Departure.LocationTime
	and	Previous.ObservationMeasure = FinalObservationDiastolicBP.ObservationMeasure
	order by
		Previous.ObservationSetDetailID desc
	)


left join Observation FinalObservationO2Saturation
on
	FinalObservationO2Saturation.CasenoteNumber = --return the string less any trailing "-xxx..." characters e.g. PAS-12345-01 returns PAS-12345.
		reverse(
			right(
				reverse(Attendance.AttendanceNumber)
				,len(Attendance.AttendanceNumber) -
				charindex(
					'-'
					,reverse(Attendance.AttendanceNumber)
				)
			)
		)

and FinalObservationO2Saturation.StartTime between Attendance.ArrivalTime and Departure.LocationTime
and	FinalObservationO2Saturation.ObservationMeasure = 'Oxygen Saturation'

and	FinalObservationO2Saturation.ObservationSetDetailID =
	(
	select top 1
		Previous.ObservationSetDetailID
	from
		Observation Previous
	where
		Previous.CasenoteNumber = FinalObservationO2Saturation.CasenoteNumber
	and	Previous.StartTime between Attendance.ArrivalTime and Departure.LocationTime
	and	Previous.ObservationMeasure = FinalObservationO2Saturation.ObservationMeasure
	order by
		Previous.ObservationSetDetailID desc
	)

left join Observation FinalObservationRespiratoryRate
on
	FinalObservationRespiratoryRate.CasenoteNumber = --return the string less any trailing "-xxx..." characters e.g. PAS-12345-01 returns PAS-12345.
		reverse(
			right(
				reverse(Attendance.AttendanceNumber)
				,len(Attendance.AttendanceNumber) -
				charindex(
					'-'
					,reverse(Attendance.AttendanceNumber)
				)
			)
		)

and FinalObservationRespiratoryRate.StartTime between Attendance.ArrivalTime and Departure.LocationTime
and	FinalObservationRespiratoryRate.ObservationMeasure = 'Respiratory Rate'

and	FinalObservationRespiratoryRate.ObservationSetDetailID =
	(
	select top 1
		Previous.ObservationSetDetailID
	from
		Observation Previous
	where
		Previous.CasenoteNumber = FinalObservationRespiratoryRate.CasenoteNumber
	and	Previous.StartTime between Attendance.ArrivalTime and Departure.LocationTime
	and	Previous.ObservationMeasure = FinalObservationRespiratoryRate.ObservationMeasure
	order by
		Previous.ObservationSetDetailID desc
	)

left join Observation FinalObservationGCS
on
	FinalObservationGCS.CasenoteNumber = --return the string less any trailing "-xxx..." characters e.g. PAS-12345-01 returns PAS-12345.
		reverse(
			right(
				reverse(Attendance.AttendanceNumber)
				,len(Attendance.AttendanceNumber) -
				charindex(
					'-'
					,reverse(Attendance.AttendanceNumber)
				)
			)
		)

and FinalObservationGCS.StartTime between Attendance.ArrivalTime and Departure.LocationTime
and	FinalObservationGCS.ObservationMeasure = 'Conscious Level'

and	FinalObservationGCS.ObservationSetDetailID =
	(
	select top 1
		Previous.ObservationSetDetailID
	from
		Observation Previous
	where
		Previous.CasenoteNumber = FinalObservationGCS.CasenoteNumber
	and	Previous.StartTime between Attendance.ArrivalTime and Departure.LocationTime
	and	Previous.ObservationMeasure = FinalObservationGCS.ObservationMeasure
	order by
		Previous.ObservationSetDetailID desc
	)

left join Observation FinalObservationEWS
on
	FinalObservationEWS.CasenoteNumber = --return the string less any trailing "-xxx..." characters e.g. PAS-12345-01 returns PAS-12345.
		reverse(
			right(
				reverse(Attendance.AttendanceNumber)
				,len(Attendance.AttendanceNumber) -
				charindex(
					'-'
					,reverse(Attendance.AttendanceNumber)
				)
			)
		)

and FinalObservationEWS.StartTime between Attendance.ArrivalTime and Departure.LocationTime

and	FinalObservationEWS.ObservationSetDetailID =
	(
	select top 1
		Previous.ObservationSetDetailID
	from
		Observation Previous
	where
		Previous.CasenoteNumber = FinalObservationEWS.CasenoteNumber
	and	Previous.StartTime between Attendance.ArrivalTime and Departure.LocationTime
	order by
		Previous.ObservationSetDetailID desc
	)

left join SymphonyStaging.DutyClinician
on	DutyClinician.StartTime <= Attendance.ArrivalTime
and	(
		DutyClinician.EndTime >= Attendance.ArrivalTime
	or	DutyClinician.EndTime is null
	)
	
and	datepart(hour, DutyClinician.StartTime) = 8
and datediff(minute, DutyClinician.StartTime, DutyClinician.EndTime) = 1439 -- DG - 03.10.14 - To handle unorthodox data entry in A&E...
	
and	not exists
	(
	select
		1
	from
		SymphonyStaging.DutyClinician Previous
	where
		Previous.StartTime <= Attendance.ArrivalTime
	and	(
			Previous.EndTime >= Attendance.ArrivalTime
		or	Previous.EndTime is null
		)	

	and	datepart(hour, Previous.StartTime) = 8
	and datediff(minute, Previous.StartTime, Previous.EndTime) = 1439 -- DG - 03.10.14 - To handle unorthodox data entry in A&E...
	
	and	Previous.DutyClinicianID < DutyClinician.DutyClinicianID
	)

left join SymphonyStaging.StaffMember DutyConsultant
on	DutyConsultant.StaffID = DutyClinician.StaffID

left join SymphonyStaging.ReferenceLookup DutyConsultantTitle
on	DutyConsultantTitle.ReferenceLookupID = DutyConsultant.TitleID

left join SymphonyStaging.Result FirstClinicalOutcome
on	FirstClinicalOutcome.AttendanceID = Attendance.AttendanceID
and	FirstClinicalOutcome.DataEntryProcedureID in (86 ,195)
and	FirstClinicalOutcome.IsInactive <> 1
and	not exists
	(
	select
		1
	from
		SymphonyStaging.Result Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.DataEntryProcedureID in (86 ,195)
	and	Previous.IsInactive <> 1
	and	(
			Previous.ResultTime < FirstClinicalOutcome.ResultTime
		or	(
				Previous.ResultTime = FirstClinicalOutcome.ResultTime
			and	Previous.ResultID < FirstClinicalOutcome.ResultID
			)
		)
	)

left join SymphonyStaging.StaffMember SeenBy
on	SeenBy.StaffID = FirstClinicalOutcome.StaffID

left join SymphonyStaging.ReferenceLookup SeenByTitle
on	SeenByTitle.ReferenceLookupID = SeenBy.TitleID

left join SymphonyStaging.Result Diagnosis01
on	Diagnosis01.AttendanceID = Attendance.AttendanceID
and	Diagnosis01.DataEntryProcedureID in (35, 202, 219)
and	Diagnosis01.IsInactive <> 1
and	not exists
	(
	select
		1
	from
		SymphonyStaging.Result Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.DataEntryProcedureID in (35, 202, 219)
	and	Previous.IsInactive <> 1
	and	Previous.ResultID < Diagnosis01.ResultID
	)


left join SymphonyStaging.ReferenceLookup Diagnosis01Code
on	Diagnosis01Code.ReferenceLookupID = Diagnosis01.ResultID1

left join SymphonyStaging.Result Diagnosis02
on	Diagnosis02.AttendanceID = Attendance.AttendanceID
and	Diagnosis02.DataEntryProcedureID in (35, 202, 219)
and	Diagnosis02.IsInactive <> 1
and	Diagnosis02.ResultID > Diagnosis01.ResultID
and	not exists
	(
	select
		1
	from
		SymphonyStaging.Result Previous
	where
		Previous.AttendanceID = Attendance.AttendanceID
	and	Previous.DataEntryProcedureID in (35, 202, 219)
	and	Previous.IsInactive <> 1
	and	Previous.ResultID > Diagnosis01.ResultID
	and	Previous.ResultID < Diagnosis02.ResultID
	)

left join SymphonyStaging.ReferenceLookup Diagnosis02Code
on	Diagnosis02Code.ReferenceLookupID = Diagnosis02.ResultID1

left join
	(
	select distinct
		AttendanceID
		,Investigation =
			substring(
				(
				select
					', ' + CombinedInvestigations.DepartmentalInvestigation as [text()]
				from
					Investigation CombinedInvestigations
				where
					CombinedInvestigations.AttendanceID = Investigation.AttendanceID
				order by
					CombinedInvestigations.DepartmentalInvestigation
				for XML PATH ('')
				)

				,3
				,4000
			)
	from
		Investigation
	) DepartmentalInvestigation
on	DepartmentalInvestigation.AttendanceID = Attendance.AttendanceID

left join (
	select
		Investigation.AttendanceID
		,Investigation.InvestigationID
		,Investigation = InvestigationCode.SourceInvestigation
		,SequenceNo = ROW_NUMBER() OVER(PARTITION BY Investigation.AttendanceID ORDER BY Investigation.InvestigationTime)
	from
		Investigation

	inner join AE.SourceInvestigationBase InvestigationCode
	on	InvestigationCode.SourceInvestigationCode = Investigation.InvestigationID

	) Investigation01
on	Investigation01.AttendanceID = Attendance.AttendanceID
and	Investigation01.SequenceNo = 1

left join (
	select
		Investigation.AttendanceID
		,Investigation.InvestigationID
		,Investigation = InvestigationCode.SourceInvestigation
		,SequenceNo = ROW_NUMBER() OVER(PARTITION BY Investigation.AttendanceID ORDER BY Investigation.InvestigationTime)
	from
		Investigation

	inner join AE.SourceInvestigationBase InvestigationCode
	on	InvestigationCode.SourceInvestigationCode = Investigation.InvestigationID

	) Investigation02
on	Investigation02.AttendanceID = Attendance.AttendanceID
and	Investigation02.SequenceNo = 2

left join
	(
	select distinct
		AttendanceID
		,Treatment =
			substring(
				(
				select
					', ' + CombinedTreatments.DepartmentalTreatment as [text()]
				from
					Treatment CombinedTreatments
				where
					CombinedTreatments.AttendanceID = Treatment.AttendanceID
				order by
					CombinedTreatments.DepartmentalTreatment
				for XML PATH ('')
				)

				,3
				,4000
			)
	from
		Treatment
	) DepartmentalTreatment
on	DepartmentalTreatment.AttendanceID = Attendance.AttendanceID

left join
	(
	select distinct
		AttendanceID
		,Medication =
			substring(
				(
				select
					', ' + CombinedMedications.Medication as [text()]
				from
					Medication CombinedMedications
				where
					CombinedMedications.AttendanceID = Medication.AttendanceID
				order by
					CombinedMedications.Medication
				for XML PATH ('')
				)

				,3
				,4000
			)
	from
		Medication
	) DepartmentalMedication
on	DepartmentalMedication.AttendanceID = Attendance.AttendanceID

left join SymphonyStaging.Note DischargeNote
on	DischargeNote.NoteID = Attendance.DischargeNotesID

left join SymphonyStaging.RequestExtraField AuditCScore
on	AuditCScore.AttendanceID = Attendance.AttendanceID
and	AuditCScore.DataEntryProcedureID = 150
and	AuditCScore.FieldID = 1532
and AuditCScore.RequestExtraFieldID =
	(
	select top 1
		LaterAuditCScore.RequestExtraFieldID
	from
		SymphonyStaging.RequestExtraField LaterAuditCScore
	where
		LaterAuditCScore.AttendanceID = AuditCScore.AttendanceID
	and	LaterAuditCScore.DataEntryProcedureID = AuditCScore.DataEntryProcedureID
	and	LaterAuditCScore.FieldID = AuditCScore.FieldID

	order by
		LaterAuditCScore.RequestExtraFieldID desc
	)

left join
	(
	select distinct
		 DisposalID = ReferenceLookupMapping.ReferenceLookupID
		,DisposalCode = Note.Note
	from
		SymphonyStaging.Note

	inner join SymphonyStaging.ReferenceLookupMapping
	on	ReferenceLookupMapping.ValueID = Note.NoteID
	and	ReferenceLookupMapping.ReferenceMappingTypeID = 
		(
		select
			MappingType.MappingTypeID
		from
			SymphonyStaging.MappingType
		where
			MappingType.MappingType = 'CDS'
		)
	) AttendanceDisposal
on	AttendanceDisposal.DisposalId = Attendance.AttendanceDisposalID

left join AE.DisposalBase
on	DisposalBase.DisposalCode = right('0' + AttendanceDisposal.DisposalCode, 2) collate database_default

left join SymphonyStaging.ReferenceLookup DischargeDestination
on	DischargeDestination.ReferenceLookupID = Attendance.DischargeDestinationID

left join SymphonyStaging.PatientSystemId PASSourcePatientNo
on	PASSourcePatientNo.PatientID = Patient.PatientID
and	PASSourcePatientNo.SystemID = 8573

left join [$(PAS)].InquireUpdate.AEATTENDANCE AEAttendance
on	AEAttendance.InternalPatientNumber = cast(PASSourcePatientNo.PatientSystemId as int)
and	left(AEAttendance.AeAttendanceDatetimeneutral, 8) + ' ' +
	substring(AEAttendance.AeAttendanceDatetimeneutral, 9, 2) + ':' +
	right(AEAttendance.AeAttendanceDatetimeneutral, 2)
	= Attendance.ArrivalTime


declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	AE.DischargeSummary target
using
	(
	select
		 DischargedByStaffMember = cast(DischargedByStaffMember as varchar(50)) collate database_default
		,DepartureTime = cast(DepartureTime as datetime)
		,AttendanceNumber = cast(AttendanceNumber as varchar(50)) collate database_default
		,PatientName = cast(PatientName as varchar(255)) collate database_default
		,PatientAddressLine1 = cast(PatientAddressLine1 as varchar(50)) collate database_default
		,PatientAddressLine2 = cast(PatientAddressLine2 as varchar(50)) collate database_default
		,PatientAddressLine3 = cast(PatientAddressLine3 as varchar(50)) collate database_default
		,PatientAddressLine4 = cast(PatientAddressLine4 as varchar(50)) collate database_default
		,PatientPostcode = cast(PatientPostcode as varchar(10)) collate database_default
		,DateOfBirth = cast(DateOfBirth as date)
		,NHSNumber = cast(NHSNumber as varchar(20)) collate database_default
		,HospitalNumber = cast(HospitalNumber as varchar(50)) collate database_default
		,ArrivalDate = cast(ArrivalDate as date)
		,ArrivalTime = cast(ArrivalTime as time)
		,ArrivalMode = cast(ArrivalMode as varchar(200)) collate database_default
		,SourceOfArrival = cast(SourceOfArrival as int)
		,AccompaniedBy = cast(AccompaniedBy as varchar(max)) collate database_default
		,PreviousAttendances = cast(PreviousAttendances as int)
		,ReferredBy = cast(ReferredBy as varchar(200)) collate database_default
		,SchoolAttended = cast(SchoolAttended as varchar(max)) collate database_default
		,RegisteredGpCode = cast(RegisteredGpCode as varchar(10)) collate database_default
		,RegisteredGp = cast(RegisteredGp as varchar(100)) collate database_default
		,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(10)) collate database_default
		,RegisteredGpAddressLine1 = cast(RegisteredGpAddressLine1 as varchar(35)) collate database_default
		,RegisteredGpAddressLine2 = cast(RegisteredGpAddressLine2 as varchar(35)) collate database_default
		,RegisteredGpAddressLine3 = cast(RegisteredGpAddressLine3 as varchar(35)) collate database_default
		,RegisteredGpAddressLine4 = cast(RegisteredGpAddressLine4 as varchar(35)) collate database_default
		,RegisteredGpAddressPostcode = cast(RegisteredGpAddressPostcode as varchar(10)) collate database_default
		,PresentingCondition = cast(PresentingCondition as varchar(max)) collate database_default
		,TriagedBy = cast(TriagedBy as varchar(255)) collate database_default
		,BriefHistory = cast(BriefHistory as varchar(300)) collate database_default
		,TriageComments = cast(TriageComments as varchar(max)) collate database_default
		,AllergyOrSensitivity = cast(AllergyOrSensitivity as varchar(max)) collate database_default
		,InitialObservationsPulse = cast(InitialObservationsPulse as varchar(50)) collate database_default
		,InitialObservationsSystolicBP = cast(InitialObservationsSystolicBP as varchar(50)) collate database_default
		,InitialObservationsDiastolicBP = cast(InitialObservationsDiastolicBP as varchar(50)) collate database_default
		,InitialObservationsO2Saturation = cast(InitialObservationsO2Saturation as varchar(50)) collate database_default
		,InitialObservationsRespiratoryRate = cast(InitialObservationsRespiratoryRate as varchar(50)) collate database_default
		,InitialObservationsGCS = cast(InitialObservationsGCS as varchar(50)) collate database_default
		,InitialObservationsEWS = cast(InitialObservationsEWS as varchar(50)) collate database_default
		,InitialObservationsTemperature = cast(InitialObservationsTemperature as varchar(50)) collate database_default
		,InitialObservationsPainScoreAtRest = cast(InitialObservationsPainScoreAtRest as varchar(50)) collate database_default
		,InitialObservationsPainScoreOnMovement = cast(InitialObservationsPainScoreOnMovement as varchar(50)) collate database_default
		,InitialObservationsBloodSugar = cast(InitialObservationsBloodSugar as varchar(50)) collate database_default
		,InitialObservationsFractionOfInspiredOxygen = cast(InitialObservationsFractionOfInspiredOxygen as varchar(50)) collate database_default
		,InitialObservationsPerformPeakFlow = cast(InitialObservationsPerformPeakFlow as varchar(50)) collate database_default
		,InitialObservationsCapRefill = cast(InitialObservationsCapRefill as varchar(50)) collate database_default
		,FinalObservationsPulse = cast(FinalObservationsPulse as varchar(50)) collate database_default
		,FinalObservationsSystolicBP = cast(FinalObservationsSystolicBP as varchar(50)) collate database_default
		,FinalObservationsDiastolicBP = cast(FinalObservationsDiastolicBP as varchar(50)) collate database_default
		,FinalObservationsO2Saturation = cast(FinalObservationsO2Saturation as varchar(50)) collate database_default
		,FinalObservationsRespiratoryRate = cast(FinalObservationsRespiratoryRate as varchar(50)) collate database_default
		,FinalObservationsGCS = cast(FinalObservationsGCS as varchar(50)) collate database_default
		,FinalObservationsEWS = cast(FinalObservationsEWS as varchar(50)) collate database_default
		,DutyConsultant = cast(DutyConsultant as varchar(255)) collate database_default
		,ClinicianSeen = cast(ClinicianSeen as varchar(255)) collate database_default
		,Diagnosis01 = cast(Diagnosis01 as varchar(255)) collate database_default
		,Diagnosis02 = cast(Diagnosis02 as varchar(255)) collate database_default
		,DepartmentalInvestigation = cast(DepartmentalInvestigation as varchar(max)) collate database_default
		,DepartmentalInvestigationDetails = cast(DepartmentalInvestigationDetails as varchar(max)) collate database_default
		,Investigation01 = cast(Investigation01 as varchar(255)) collate database_default
		,Investigation02 = cast(Investigation02 as varchar(255)) collate database_default
		,Treatment = cast(Treatment as varchar(max)) collate database_default
		,TreatmentDetails = cast(TreatmentDetails as varchar(max)) collate database_default
		,MedicationGiven = cast(MedicationGiven as varchar(max)) collate database_default
		,InformationForGP = cast(InformationForGP as varchar(max)) collate database_default
		,AuditCScore = cast(AuditCScore as varchar(max)) collate database_default
		,FollowUpAdvice = cast(FollowUpAdvice as varchar(20)) collate database_default
		,FollowUpArrangement = cast(FollowUpArrangement as int)
		,FollowUpTime = cast(FollowUpTime as varchar(max))
		,DischargeOutcome = cast(DischargeOutcome as varchar(200)) collate database_default
		,DischargeOutcomeDetails = cast(DischargeOutcomeDetails as varchar(max))
		,DischargeDestination = cast(DischargeDestination as varchar(80)) collate database_default
		,DischargeMedicationGiven = cast(DischargeMedicationGiven as varchar(max))
		,LetterSentBy = cast(LetterSentBy as varchar(19)) collate database_default
		,PASSourcePatientNo = cast(PASSourcePatientNo as int)
		,PASSourceEpisodeNo = cast(PASSourceEpisodeNo as int)
		,DistrictNo = cast(DistrictNo as varchar(50)) collate database_default
		,AESourceUniqueID = cast(AESourceUniqueID as int)
	from
		#AEDischargeSummary 
	) source
	on	source.AttendanceNumber = target.AttendanceNumber

	when not matched by source
	and target.DepartureTime between @from and @to

	then delete

	when not matched
	then
		insert
			(
			DischargedByStaffMember
			,DepartureTime
			,AttendanceNumber
			,PatientName
			,PatientAddressLine1
			,PatientAddressLine2
			,PatientAddressLine3
			,PatientAddressLine4
			,PatientPostcode
			,DateOfBirth
			,NHSNumber
			,HospitalNumber
			,ArrivalDate
			,ArrivalTime
			,ArrivalMode
			,SourceOfArrival
			,AccompaniedBy
			,PreviousAttendances
			,ReferredBy
			,SchoolAttended
			,RegisteredGpCode
			,RegisteredGp
			,RegisteredGpPracticeCode
			,RegisteredGpAddressLine1
			,RegisteredGpAddressLine2
			,RegisteredGpAddressLine3
			,RegisteredGpAddressLine4
			,RegisteredGpAddressPostcode
			,PresentingCondition
			,TriagedBy
			,BriefHistory
			,TriageComments
			,AllergyOrSensitivity
			,InitialObservationsPulse
			,InitialObservationsSystolicBP
			,InitialObservationsDiastolicBP
			,InitialObservationsO2Saturation
			,InitialObservationsRespiratoryRate
			,InitialObservationsGCS
			,InitialObservationsEWS
			,InitialObservationsTemperature
			,InitialObservationsPainScoreAtRest 
			,InitialObservationsPainScoreOnMovement
			,InitialObservationsBloodSugar  
			,InitialObservationsFractionOfInspiredOxygen
			,InitialObservationsPerformPeakFlow
			,InitialObservationsCapRefill 
			,FinalObservationsPulse
			,FinalObservationsSystolicBP
			,FinalObservationsDiastolicBP
			,FinalObservationsO2Saturation
			,FinalObservationsRespiratoryRate
			,FinalObservationsGCS
			,FinalObservationsEWS
			,DutyConsultant
			,ClinicianSeen
			,Diagnosis01
			,Diagnosis02
			,DepartmentalInvestigation
			,DepartmentalInvestigationDetails
			,Investigation01
			,Investigation02
			,Treatment
			,TreatmentDetails
			,MedicationGiven
			,InformationForGP
			,AuditCScore
			,FollowUpAdvice
			,FollowUpArrangement
			,FollowUpTime
			,DischargeOutcome
			,DischargeOutcomeDetails
			,DischargeDestination
			,DischargeMedicationGiven
			,LetterSentBy
			,PASSourcePatientNo
			,PASSourceEpisodeNo
			,DistrictNo
			,AESourceUniqueID
			,Created
			,ByWhom
			)
		values
			(
			 source.DischargedByStaffMember
			,source.DepartureTime
			,source.AttendanceNumber
			,source.PatientName
			,source.PatientAddressLine1
			,source.PatientAddressLine2
			,source.PatientAddressLine3
			,source.PatientAddressLine4
			,source.PatientPostcode
			,source.DateOfBirth
			,source.NHSNumber
			,source.HospitalNumber
			,source.ArrivalDate
			,source.ArrivalTime
			,source.ArrivalMode
			,source.SourceOfArrival
			,source.AccompaniedBy
			,source.PreviousAttendances
			,source.ReferredBy
			,source.SchoolAttended
			,source.RegisteredGpCode
			,source.RegisteredGp
			,source.RegisteredGpPracticeCode
			,source.RegisteredGpAddressLine1
			,source.RegisteredGpAddressLine2
			,source.RegisteredGpAddressLine3
			,source.RegisteredGpAddressLine4
			,source.RegisteredGpAddressPostcode
			,source.PresentingCondition
			,source.TriagedBy
			,source.BriefHistory
			,source.TriageComments
			,source.AllergyOrSensitivity
			,source.InitialObservationsPulse
			,source.InitialObservationsSystolicBP
			,source.InitialObservationsDiastolicBP
			,source.InitialObservationsO2Saturation
			,source.InitialObservationsRespiratoryRate
			,source.InitialObservationsGCS
			,source.InitialObservationsEWS
			,source.InitialObservationsTemperature
			,source.InitialObservationsPainScoreAtRest 
			,source.InitialObservationsPainScoreOnMovement
			,source.InitialObservationsBloodSugar  
			,source.InitialObservationsFractionOfInspiredOxygen
			,source.InitialObservationsPerformPeakFlow
			,source.InitialObservationsCapRefill 
			,source.FinalObservationsPulse
			,source.FinalObservationsSystolicBP
			,source.FinalObservationsDiastolicBP
			,source.FinalObservationsO2Saturation
			,source.FinalObservationsRespiratoryRate
			,source.FinalObservationsGCS
			,source.FinalObservationsEWS
			,source.DutyConsultant
			,source.ClinicianSeen
			,source.Diagnosis01
			,source.Diagnosis02
			,source.DepartmentalInvestigation
			,source.DepartmentalInvestigationDetails
			,source.Investigation01
			,source.Investigation02
			,source.Treatment
			,source.TreatmentDetails
			,source.MedicationGiven
			,source.InformationForGP
			,source.AuditCScore
			,source.FollowUpAdvice
			,source.FollowUpArrangement
			,source.FollowUpTime
			,source.DischargeOutcome
			,source.DischargeOutcomeDetails
			,source.DischargeDestination
			,source.DischargeMedicationGiven
			,source.LetterSentBy
			,source.PASSourcePatientNo
			,source.PASSourceEpisodeNo
			,source.DistrictNo
			,source.AESourceUniqueID
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(		
			isnull(target.DischargedByStaffMember, '') = isnull(source.DischargedByStaffMember, '')
		and isnull(target.DepartureTime, getdate()) = isnull(source.DepartureTime, getdate())
		and isnull(target.AttendanceNumber, '') = isnull(source.AttendanceNumber, '')
		and isnull(target.PatientName, '') = isnull(source.PatientName, '')
		and isnull(target.PatientAddressLine1, '') = isnull(source.PatientAddressLine1, '')
		and isnull(target.PatientAddressLine2, '') = isnull(source.PatientAddressLine2, '')
		and isnull(target.PatientAddressLine3, '') = isnull(source.PatientAddressLine3, '')
		and isnull(target.PatientAddressLine4, '') = isnull(source.PatientAddressLine4, '')
		and isnull(target.PatientPostcode, '') = isnull(source.PatientPostcode, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.HospitalNumber, '') = isnull(source.HospitalNumber, '')
		and isnull(target.ArrivalDate, getdate()) = isnull(source.ArrivalDate, getdate())
		and isnull(target.ArrivalTime, '') = isnull(source.ArrivalTime,'')
		and isnull(target.ArrivalMode, '') = isnull(source.ArrivalMode, '')
		and isnull(target.SourceOfArrival, '') = isnull(source.SourceOfArrival,'')
		and isnull(target.AccompaniedBy, '') = isnull(source.AccompaniedBy, '')
		and isnull(target.PreviousAttendances, 0) = isnull(source.PreviousAttendances, 0)
		and isnull(target.ReferredBy, '') = isnull(source.ReferredBy, '')
		and isnull(target.SchoolAttended, '') = isnull(source.SchoolAttended, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredGp, '') = isnull(source.RegisteredGp, '')
		and isnull(target.RegisteredGpPracticeCode, '') = isnull(source.RegisteredGpPracticeCode, '')
		and isnull(target.RegisteredGpAddressLine1, '') = isnull(source.RegisteredGpAddressLine1, '')
		and isnull(target.RegisteredGpAddressLine2, '') = isnull(source.RegisteredGpAddressLine2, '')
		and isnull(target.RegisteredGpAddressLine3, '') = isnull(source.RegisteredGpAddressLine3, '')
		and isnull(target.RegisteredGpAddressLine4, '') = isnull(source.RegisteredGpAddressLine4, '')
		and isnull(target.RegisteredGpAddressPostcode, '') = isnull(source.RegisteredGpAddressPostcode, '')
		and isnull(target.PresentingCondition, '') = isnull(source.PresentingCondition, '')
		and isnull(target.TriagedBy, '') = isnull(source.TriagedBy, '')
		and isnull(target.BriefHistory, '') = isnull(source.BriefHistory, '')
		and isnull(target.TriageComments, '') = isnull(source.TriageComments, '')
		and isnull(target.AllergyOrSensitivity, '') = isnull(source.AllergyOrSensitivity, '')
		and isnull(target.InitialObservationsPulse, '') = isnull(source.InitialObservationsPulse, '')
		and isnull(target.InitialObservationsSystolicBP, '') = isnull(source.InitialObservationsSystolicBP, '')
		and isnull(target.InitialObservationsDiastolicBP, '') = isnull(source.InitialObservationsDiastolicBP, '')
		and isnull(target.InitialObservationsO2Saturation, '') = isnull(source.InitialObservationsO2Saturation, '')
		and isnull(target.InitialObservationsRespiratoryRate, '') = isnull(source.InitialObservationsRespiratoryRate, '')
		and isnull(target.InitialObservationsGCS, '') = isnull(source.InitialObservationsGCS, '')
		and isnull(target.InitialObservationsEWS, '') = isnull(source.InitialObservationsEWS, '')
		and isnull(target.InitialObservationsTemperature, '') = isnull(source.InitialObservationsTemperature, '')
		and isnull(target.InitialObservationsPainScoreAtRest, '') = isnull(source.InitialObservationsPainScoreAtRest, '')
		and isnull(target.InitialObservationsPainScoreOnMovement, '') = isnull(source.InitialObservationsPainScoreOnMovement, '')
		and isnull(target.InitialObservationsBloodSugar, '') = isnull(source.InitialObservationsBloodSugar, '')
		and isnull(target.InitialObservationsFractionOfInspiredOxygen, '') = isnull(source.InitialObservationsFractionOfInspiredOxygen, '')
		and isnull(target.InitialObservationsPerformPeakFlow, '') = isnull(source.InitialObservationsPerformPeakFlow, '')
		and isnull(target.InitialObservationsCapRefill, '') = isnull(source.InitialObservationsCapRefill,'')
		and isnull(target.FinalObservationsPulse, '') = isnull(source.FinalObservationsPulse, '')
		and isnull(target.FinalObservationsSystolicBP, '') = isnull(source.FinalObservationsSystolicBP, '')
		and isnull(target.FinalObservationsDiastolicBP, '') = isnull(source.FinalObservationsDiastolicBP, '')
		and isnull(target.FinalObservationsO2Saturation, '') = isnull(source.FinalObservationsO2Saturation, '')
		and isnull(target.FinalObservationsRespiratoryRate, '') = isnull(source.FinalObservationsRespiratoryRate, '')
		and isnull(target.FinalObservationsGCS, '') = isnull(source.FinalObservationsGCS, '')
		and isnull(target.FinalObservationsEWS, '') = isnull(source.FinalObservationsEWS, '')
		and isnull(target.DutyConsultant, '') = isnull(source.DutyConsultant, '')
		and isnull(target.ClinicianSeen,'') = isnull(source.ClinicianSeen, '')
		and isnull(target.Diagnosis01, '') = isnull(source.Diagnosis01, '')
		and isnull(target.Diagnosis02, '') = isnull(source.Diagnosis02, '')
		and isnull(target.DepartmentalInvestigation, '') = isnull(source.DepartmentalInvestigation, '')
		and isnull(target.DepartmentalInvestigationDetails, '') = isnull(source.DepartmentalInvestigationDetails, '')
		and isnull(target.Investigation01, '') = isnull(source.Investigation01, '')
		and isnull(target.Investigation02, '') = isnull(source.Investigation02, '')
		and isnull(target.Treatment, '') = isnull(source.Treatment, '')
		and isnull(target.TreatmentDetails, '') = isnull(source.TreatmentDetails, '')
		and isnull(target.MedicationGiven, '') = isnull(source.MedicationGiven, '')
		and isnull(target.InformationForGP, '') = isnull(source.InformationForGP, '')
		and isnull(target.AuditCScore, '') = isnull(source.AuditCScore, '')
		and isnull(target.FollowUpAdvice, '') = isnull(source.FollowUpAdvice, '')
		and isnull(target.FollowUpArrangement, '') = isnull(source.FollowUpArrangement, '')
		and isnull(target.FollowUpTime, '') = isnull(source.FollowUpTime, '')
		and isnull(target.DischargeOutcome, '') = isnull(source.DischargeOutcome, '')
		and isnull(target.DischargeOutcomeDetails, '') = isnull(source.DischargeOutcomeDetails, '')
		and isnull(target.DischargeDestination, '') = isnull(source.DischargeDestination, '')
		and isnull(target.DischargeMedicationGiven, '') = isnull(source.DischargeMedicationGiven, '')
		and isnull(target.LetterSentBy, '') = isnull(source.LetterSentBy, '')
		and isnull(target.PASSourcePatientNo, 0) = isnull(source.PASSourcePatientNo, 0)
		and isnull(target.PASSourceEpisodeNo, 0) = isnull(source.PASSourceEpisodeNo, 0)
		and isnull(target.DistrictNo, 0) = isnull(source.DistrictNo, 0)
		and isnull(target.AESourceUniqueID, 0) = isnull(source.AESourceUniqueID, 0)

		)
	then
		update
		set
			 target.DischargedByStaffMember = source.DischargedByStaffMember
			,target.DepartureTime = source.DepartureTime
			,target.AttendanceNumber = source.AttendanceNumber
			,target.PatientName = source.PatientName
			,target.PatientAddressLine1 = source.PatientAddressLine1
			,target.PatientAddressLine2 = source.PatientAddressLine2
			,target.PatientAddressLine3 = source.PatientAddressLine3
			,target.PatientAddressLine4 = source.PatientAddressLine4
			,target.PatientPostcode = source.PatientPostcode
			,target.DateOfBirth = source.DateOfBirth
			,target.NHSNumber = source.NHSNumber
			,target.HospitalNumber = source.HospitalNumber
			,target.ArrivalDate = source.ArrivalDate
			,target.ArrivalTime = source.ArrivalTime
			,target.ArrivalMode = source.ArrivalMode
			,target.SourceOfArrival = source.SourceOfArrival
			,target.AccompaniedBy = source.AccompaniedBy
			,target.PreviousAttendances = source.PreviousAttendances
			,target.ReferredBy = source.ReferredBy
			,target.SchoolAttended = source.SchoolAttended
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGp = source.RegisteredGp
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.RegisteredGpAddressLine1 = source.RegisteredGpAddressLine1
			,target.RegisteredGpAddressLine2 = source.RegisteredGpAddressLine2
			,target.RegisteredGpAddressLine3 = source.RegisteredGpAddressLine3
			,target.RegisteredGpAddressLine4 = source.RegisteredGpAddressLine4
			,target.RegisteredGpAddressPostcode = source.RegisteredGpAddressPostcode
			,target.PresentingCondition = source.PresentingCondition
			,target.TriagedBy = source.TriagedBy
			,target.BriefHistory = source.BriefHistory
			,target.TriageComments = source.TriageComments
			,target.AllergyOrSensitivity = source.AllergyOrSensitivity
			,target.InitialObservationsPulse = source.InitialObservationsPulse
			,target.InitialObservationsSystolicBP = source.InitialObservationsSystolicBP
			,target.InitialObservationsDiastolicBP = source.InitialObservationsDiastolicBP
			,target.InitialObservationsO2Saturation = source.InitialObservationsO2Saturation
			,target.InitialObservationsRespiratoryRate = source.InitialObservationsRespiratoryRate
			,target.InitialObservationsGCS = source.InitialObservationsGCS
			,target.InitialObservationsEWS = source.InitialObservationsEWS
			,target.InitialObservationsTemperature = source.InitialObservationsTemperature
			,target.InitialObservationsPainScoreAtRest = source.InitialObservationsPainScoreAtRest
			,target.InitialObservationsPainScoreOnMovement = source.InitialObservationsPainScoreOnMovement
			,target.InitialObservationsBloodSugar = source.InitialObservationsBloodSugar
			,target.InitialObservationsFractionOfInspiredOxygen = source.InitialObservationsFractionOfInspiredOxygen
			,target.InitialObservationsPerformPeakFlow = source.InitialObservationsPerformPeakFlow
			,target.InitialObservationsCapRefill = source.InitialObservationsCapRefill
			,target.FinalObservationsPulse = source.FinalObservationsPulse
			,target.FinalObservationsSystolicBP = source.FinalObservationsSystolicBP
			,target.FinalObservationsDiastolicBP = source.FinalObservationsDiastolicBP
			,target.FinalObservationsO2Saturation = source.FinalObservationsO2Saturation
			,target.FinalObservationsRespiratoryRate = source.FinalObservationsRespiratoryRate
			,target.FinalObservationsGCS = source.FinalObservationsGCS
			,target.FinalObservationsEWS = source.FinalObservationsEWS
			,target.DutyConsultant = source.DutyConsultant
			,target.ClinicianSeen = source.ClinicianSeen
			,target.Diagnosis01 = source.Diagnosis01
			,target.Diagnosis02 = source.Diagnosis02
			,target.DepartmentalInvestigation = source.DepartmentalInvestigation
			,target.DepartmentalInvestigationDetails = source.DepartmentalInvestigationDetails
			,target.Investigation01 = source.Investigation01
			,target.Investigation02 = source.Investigation02
			,target.Treatment = source.Treatment
			,target.TreatmentDetails = source.TreatmentDetails
			,target.MedicationGiven = source.MedicationGiven
			,target.InformationForGP = source.InformationForGP
			,target.AuditCScore = source.AuditCScore
			,target.FollowUpAdvice = source.FollowUpAdvice
			,target.FollowUpArrangement = source.FollowUpArrangement
			,target.FollowUpTime = source.FollowUpTime
			,target.DischargeOutcome = source.DischargeOutcome
			,target.DischargeOutcomeDetails = source.DischargeOutcomeDetails
			,target.DischargeDestination = source.DischargeDestination
			,target.DischargeMedicationGiven = source.DischargeMedicationGiven
			,target.LetterSentBy = source.LetterSentBy
			,target.PASSourcePatientNo = source.PASSourcePatientNo
			,target.PASSourceEpisodeNo = source.PASSourceEpisodeNo
			,target.DistrictNo = source.DistrictNo
			,target.AESourceUniqueID = source.AESourceUniqueID
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
output
	$action into @MergeSummary
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
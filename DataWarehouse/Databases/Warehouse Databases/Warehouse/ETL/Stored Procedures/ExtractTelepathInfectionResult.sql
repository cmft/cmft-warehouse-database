﻿


CREATE proc [ETL].[ExtractTelepathInfectionResult]

	 @Fromdate smalldatetime = null
	,@ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

insert ETL.TImportInfectionResult
(
	AlertTypeCode
	,TestCategoryID
	,Positive
	,SpecimenNumber
	,PatientName
	,DistrictNo
	,NHSNumber
	,SexCode
	,DateOfBirth
	,SpecimenCollectionDate
	,SpecimenCollectionTime
	,PreviousPositiveDate
	,OrganismCode
	,SampleTypeCode
	,SampleSubTypeCode
	,SampleQualifierCode
	,AuthorisationDate
	,ExtractFileName
)

select
	AlertTypeCode 
	,TestCategoryID 
	,Positive 
	,SpecimenNumber
	,PatientName 
	,DistrictNo 
	,NHSNumber
	,SexCode 
	,DateOfBirth
	,SpecimenCollectionDate 
	,SpecimenCollectionTime 
	,PreviousPositiveDate 
	,OrganismCode 
	,SampleTypeCode 
	,SampleSubTypeCode
	,SampleQualifierCode 
	,AuthorisationDate
	,ExtractFileName 
from
	(
	select
		AlertTypeCode = AlertType
		,TestCategoryID = TestCategory.TestCategoryID
		,Positive
		,SpecimenNumber
		,PatientName = replace(PatientName, ',', ', ')
		,DistrictNo = nullif(DistrictNumber, '')
		,NHSNumber = nullif(NHSNumber, 'Not Recorded')	
		,SexCode = Sex
		,DateOfBirth
		,SpecimenCollectionDate = cast(SpecimenCollectionDateTime as date)
		,SpecimenCollectionTime = SpecimenCollectionDateTime
		,PreviousPositiveDate = nullif(nullif(DateofPreviousPositive, ''), 'No previous')
		,OrganismCode = replace(Organism, 'NoIsolate', 'NONE') --nullif(Organism, )
		,SampleTypeCode --= SpecimenSiteCode
		,SampleSubTypeCode = nullif(SampleSubTypeCode, '')--= SampleSubType.Code -- replace description with code
		,SampleQualifierCode = nullif(SampleQualifierCode, '') --= nullif(SpecimenQualifierCode, '')
		,AuthorisationDate
		,ExtractFileName = extractDate
	from
		[$(Telepath)].HCAI.TelepathBase

	left join Infection.TestCategory
	on TestCategory.TestCategory = TelepathBase.TestCategory

	--left join Telepath.HCAI.SampleSubType 
	--on	TelepathBase.SampleSubTypeCode = SampleSubType.Description
	--and not exists -- seem to have duplicate descriptions for some codes?
	--			(
	--			select
	--				1
	--			from
	--				Telepath.HCAI.SampleSubType Test
	--			where
	--				Test.Code > SampleSubType.Code
	--			)

	where
		SpecimenCollectionDateTime between @Fromdate and @ToDate

	) Infection




select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


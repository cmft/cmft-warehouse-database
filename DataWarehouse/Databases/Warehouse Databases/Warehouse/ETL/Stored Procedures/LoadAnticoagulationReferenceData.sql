﻿
CREATE proc ETL.LoadAnticoagulationReferenceData

as

insert Anticoagulation.Commissioner
(
CommissionerID
,Commissioner
)

select
	CommissionerID = pkiHealthAuthorityID 
	,Commissioner = cName
	--,cAddress1
	--,cAddress2
	--,cTown
	--,cCounty
	--,cPostCode
	--,cTelephone
	--,cFax
	--,cEmail
	--,lInUse
from
	[$(Dawn)].dbo.HealthAuthority
where
	not exists
			(
			select
				1
			from
				Anticoagulation.Commissioner
			where
				pkiHealthAuthorityID = CommissionerID
			)


insert Anticoagulation.Clinic
(
ClinicID
,Clinic
)

select
	ClinicID = pkiClinicID
	,Clinic = cDescription
	--,nDNAInterval
	--,nAdvanceDays
	--,nMaxCapacity
	--,lExcludeWeekends
	--,fkiOrganisationID
	--,lInUse
	--,fkiApplicationAreaID
	--,cLocalCode

from
	[$(Dawn)].dbo.clinic

where
	not exists
			(
			select
				1
			from
				Anticoagulation.Clinic
			where
				ClinicID = pkiClinicID
			)


insert Anticoagulation.Diagnosis
(
DiagnosisID
,DiagnosisCode
,Diagnosis
)

select
	DiagnosisID = NewDiagnosis.pkiDiagnosisID 
	,DiagnosisCode = NewDiagnosis.cCodeName
	,Diagnosis = NewDiagnosis.cDescription
	--,cCodeName
	--,lInUse
	--,fkiDiagnosisGroupID
	--,GUID
from
	[$(Dawn)].dbo.Diagnosis NewDiagnosis
where
	not exists
		(
		select
			1
		from
			Anticoagulation.Diagnosis
		where
			Diagnosis.DiagnosisID = NewDiagnosis.pkiDiagnosisID
		)
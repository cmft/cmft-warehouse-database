﻿CREATE procedure [ETL].[LoadCWSResult]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null
as


set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @FromDate = coalesce(@FromDate, (select cast(dateadd(month, -3, getdate()) as date)))
--select @ToDate = coalesce(@ToDate, (select cast(getdate() as date)))
select @ToDate = coalesce(@ToDate, '6 jun 2079') -- maximum for smalldatetime - handles future dated start times

truncate table ETL.TImportResultCWSTextResult
exec ETL.ExtractInquireResultCWSTextResult @FromDate, @ToDate
exec ETL.LoadResultCWSTextResult


truncate table ETL.TImportResultCWSNumericResult
exec ETL.ExtractInquireResultCWSNumericResult @FromDate, @ToDate
exec ETL.LoadResultCWSNumericResult

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	convert(varchar(6), @Elapsed) + ' Mins, Period ' + 
	convert(varchar(11), coalesce(@FromDate, '')) + ' to ' +
	convert(varchar(11), coalesce(@ToDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
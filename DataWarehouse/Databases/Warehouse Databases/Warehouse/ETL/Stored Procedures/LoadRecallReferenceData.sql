﻿CREATE Procedure [ETL].[LoadRecallReferenceData]
as


Insert into Recall.UnitBase
	(
	UnitsCode
	,Units
	)
select 
	UnitsKey
	,UnitsDescription
from
	(
	select 
		UnitsKey
		,UnitsDescription
		,Sequence = row_number () over(partition by UnitsKey
										order by UnitsDescription)
	from
		[$(RecallReports)].dbo.EpisodeDrug
	) Unit
where
	Sequence = 1
and not exists
		(
		select
			1
		from 
			Recall.UnitBase Present
		where 
			Present.UnitsCode = Unit.UnitsKey
		)
		
		
Insert into Recall.DrugBase
	(
	DrugCode
	,Drug
	)
select 
	DrugKey
	,DrugDescription
from
	(
	select 
		DrugKey
		,DrugDescription
		,Sequence = row_number () over(partition by DrugKey
										order by DrugDescription)
	from
		[$(RecallReports)].dbo.EpisodeDrug
	) Drug
where
	Sequence = 1
and not exists
		(
		select
			1
		from 
			Recall.DrugBase Present
		where 
			Present.DrugCode = Drug.DrugKey
		)
		
		
		
		

Insert into Recall.LocationBase
	(
	LocationCode
	,Location
	)
select 
	LocationKey
	,LocationDescription
from
	(
	select 
		LocationKey
		,LocationDescription
		,Sequence = row_number () over(partition by LocationKey
										order by LocationDescription)
	from
		[$(RecallReports)].dbo.EpisodeSchedule 
	) Location
where
	Sequence = 1
and not exists
		(
		select
			1
		from 
			Recall.LocationBase Present
		where 
			Present.LocationCode = Location.LocationKey
		)
		
	
Insert into Recall.SpecialtyBase
	(
	SpecialtyCode
	,Specialty
	)
select 
	SpecialtyKey
	,SpecialtyDescription
from
	(
	select 
		SpecialtyKey
		,SpecialtyDescription
		,Sequence = row_number () over(partition by SpecialtyKey
										order by SpecialtyDescription)
	from
		[$(RecallReports)].dbo.EpisodeSpecialty 
	) Specialty
where
	Sequence = 1
and not exists
		(
		select
			1
		from 
			Recall.SpecialtyBase Present
		where 
			Present.SpecialtyCode = Specialty.SpecialtyKey
		)


Insert into Recall.ConsultantBase
	(ConsultantCode
	,Consultant
	)
select 
	ConsultantKey
	,ConsultantName
from
	(select 
		ConsultantKey
		,ConsultantName
		,Sequence = row_number () over(partition by ConsultantKey
										order by ConsultantName)
	from
		[$(RecallReports)].dbo.EpisodeAdmission 
	) Consultant
where
	Sequence = 1
and not exists
		(select
			1
		from 
			Recall.ConsultantBase Present
		where 
			Present.ConsultantCode = Consultant.ConsultantKey
		)
		

	

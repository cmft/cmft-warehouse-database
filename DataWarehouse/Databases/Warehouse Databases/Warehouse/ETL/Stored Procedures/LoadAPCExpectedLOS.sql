﻿

CREATE PROCEDURE [ETL].[LoadAPCExpectedLOS]

AS

BEGIN

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @from varchar(19)
declare @to varchar(19)


select @StartTime = getdate()
select @RowsUpdated = 0

--Insert new records from PAS
INSERT INTO APC.ExpectedLOS

SELECT 
	 SourceUniqueID
	,LoadTime = GETDATE()
	,LoadModifiedTime = NULL
	,SourceCreatedTime = Created
	,EDDCreatedTime = CASE 
							WHEN ExpectedLOS IS NULL THEN NULL 
							ELSE Created
					   END
	
	,SourceSystem = 'PAS'
	,ModifiedFromSystem = NULL
	,AdmissionTime
	,DischargeTime = DischargeDate
	,SourcePatientNo = SourcePatientNo
	,SourceSpellNo = SourceSpellNo
	,SiteCode = StartSiteCode
	,Ward = StartWardTypeCode
	,Specialty = SpecialtyCode
	,Consultant = ConsultantCode
	,ManagementIntentionCode = ManagementIntentionCode
	,CasenoteNumber = CasenoteNumber
	,ExpectedLOS
	,EnteredWithin48hrs = CASE 
							WHEN ExpectedLOS IS NOT NULL 
							AND DATEDIFF(Minute,AdmissionTime, Created) <1440 THEN 1 
							ELSE 0 
						  END
	,CreateEddDaysDuration = CASE 
								WHEN ExpectedLOS IS NOT NULL THEN DATEDIFF(dd,AdmissionTime, Created) 
								ELSE NULL 
							 END
	,DirectorateCode = NULL
	,AdmissionMethodCode = AdmissionMethodCode
	,RTTPathwayCondition = Encounter.RTTPathwayCondition
	 ,ArchiveFlag = 'N'
FROM 
	APC.Encounter
WHERE  
	NOT EXISTS
		(
		SELECT
			1
		FROM
			APC.ExpectedLOS
		WHERE
			ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
		AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo
		)
		AND AdmissionTime >='01/April/2011'
		AND AdmissionTime = EpisodeStartTime

select
	@RowsInserted = @@ROWCOUNT

--Update discharge field
UPDATE APC.ExpectedLOS

SET 
	DischargeTime = Encounter.DischargeTime
	
FROM APC.Encounter

WHERE
	ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
AND ExpectedLOS.AdmissionTime = Encounter.AdmissionTime
AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo
AND ExpectedLOS.DischargeTime IS NULL
AND Encounter.DischargeTime is not null
AND Encounter.AdmissionTime = Encounter.EpisodeStartTime

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

--Update managementintention field
UPDATE APC.ExpectedLOS

SET 
	ExpectedLOS.ManagementIntentionCode = Encounter.ManagementIntentionCode

FROM
	 APC.ExpectedLOS
	,APC.Encounter

WHERE
	ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
AND ExpectedLOS.AdmissionTime = Encounter.AdmissionTime
AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo
AND	(
		ExpectedLOS.ManagementIntentionCode IS NULL
	OR	ExpectedLOS.ManagementIntentionCode <> Encounter.ManagementIntentionCode
	)
AND Encounter.ManagementIntentionCode is not null
AND Encounter.AdmissionTime = Encounter.EpisodeStartTime

select

	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

--Update admission method field
UPDATE APC.ExpectedLOS

SET 
	ExpectedLOS.AdmissionMethodCode = Encounter.AdmissionMethodCode
FROM
	 APC.ExpectedLOS
	,APC.Encounter

WHERE
	ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
AND ExpectedLOS.AdmissionTime = Encounter.AdmissionTime
AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo
AND ExpectedLOS.AdmissionMethodCode IS NULL
AND Encounter.AdmissionMethodCode is not null
AND Encounter.AdmissionTime = Encounter.EpisodeStartTime

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

--Update Site code field
UPDATE APC.ExpectedLOS

SET 
	ExpectedLOS.SiteCode = Encounter.SiteCode
FROM
	 APC.ExpectedLOS
	,APC.Encounter

WHERE
	ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
AND ExpectedLOS.AdmissionTime = Encounter.AdmissionTime
AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo
AND ExpectedLOS.SiteCode IS NULL
AND Encounter.SiteCode is not null
AND Encounter.AdmissionTime = Encounter.EpisodeStartTime

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

--Update RTT Pathway Condition code field
UPDATE APC.ExpectedLOS

SET 
	ExpectedLOS.RTTPathwayCondition = Encounter.RTTPathwayCondition
FROM
	 APC.ExpectedLOS
	,APC.Encounter
WHERE
	ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
AND ExpectedLOS.AdmissionTime = Encounter.AdmissionTime
AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo
AND ExpectedLOS.RTTPathwayCondition IS NULL
AND Encounter.RTTPathwayCondition is not null
AND Encounter.AdmissionTime = Encounter.EpisodeStartTime


END


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Rows updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'LoadAPCExpectedLOS', @Stats, @StartTime



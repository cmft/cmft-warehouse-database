﻿


CREATE procedure [ETL].[AssignRFEncounterDirectorateCode] as


-- main mapping
update RF.Encounter
set
	Encounter.DirectorateCode = DivisionMap.DirectorateCode
from
	RF.Encounter

inner join WH.DivisionMap
on	DivisionMap.SiteCode = Encounter.SiteCode
and	DivisionMap.SpecialtyCode = Encounter.SpecialtyCode
and 
	(
		DivisionMap.ConsultantCode = Encounter.ConsultantCode
	or
		DivisionMap.ConsultantCode is null
	)

and Encounter.ReferralDate between DivisionMap.FromDate and coalesce(ToDate, getdate())


where
	not exists
		(
		select
			1
		from
			WH.DivisionMap Previous
		where
			Previous.SiteCode = Encounter.SiteCode
		and	Previous.SpecialtyCode = Encounter.SpecialtyCode


		and 
			(
				Previous.ConsultantCode = Encounter.ConsultantCode
			or
				Previous.ConsultantCode is null
			)

		and
			Encounter.ReferralDate between Previous.FromDate and coalesce(Previous.ToDate, '31 dec 9999')

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end

				>

				case
				when DivisionMap.ConsultantCode is not null
				then 4
				else 0
				end
	
			--or
			--	(
			--		case
			--		when Previous.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end

			--		=

			--		case
			--		when DivisionRuleBase.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end 

				)
			)



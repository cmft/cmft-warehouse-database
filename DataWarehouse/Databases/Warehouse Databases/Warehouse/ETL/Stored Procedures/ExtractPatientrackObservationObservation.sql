﻿




CREATE proc [ETL].[ExtractPatientrackObservationObservation]

	@FromDate datetime = null
	,@ToDate datetime = null


as



set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @RowsInserted = 0

insert into ETL.TImportObservationObservation
(
	SourceUniqueID
	,ObservationSetSourceUniqueID
	,MeasureID
	,NumericResult
	,ListID
	,FlagCode
	,CreatedByUserID
	,CreatedTime
	,ModifiedByUserID
	,LastModifiedTime
)


select --top 100 
	SourceUniqueID
	,ObservationSetSourceUniqueID 
	,MeasureID
	,NumericResult
	,ListID
	,FlagCode
	,CreatedByUserID
	,CreatedTime
	,ModifiedByUserID
	,LastModifiedTime 

from
	(
	select
		SourceUniqueID = OBSERVATION_SET_DETAIL.OSDET_PK
		,ObservationSetSourceUniqueID = OBSERVATION_SET_DETAIL.OBSET_PK
		,MeasureID = MEASURE_SET_ITEM.OBMSR_PK
		,NumericResult = coalesce(
							OBSERVATION_SET_DETAIL.MEASURE_INT 
							,OBSERVATION_SET_DETAIL.MEASURE_DECIMAL 	
							)
		,ListID = OBSERVATION_SET_DETAIL.MEASURE_LIST 
		,FlagCode = OBSERVATION_SET_DETAIL.MEASURE_FLAG 
		,CreatedByUserID = OBSERVATION_SET_DETAIL.CREATED_BY_USERR_PK
		,CreatedTime = OBSERVATION_SET_DETAIL.CREATED_DTTM
		,ModifiedByUserID = OBSERVATION_SET_DETAIL.LAST_MODIFIED_BY_USERR_PK
		,LastModifiedTime = OBSERVATION_SET_DETAIL.LAST_MODIFIED_DTTM
	from
		[$(PatientrackSS)].dbo.OBSERVATION_SET_DETAIL  

	inner join [$(PatientrackSS)].dbo.MEASURE_SET_ITEM 
	on	OBSERVATION_SET_DETAIL.MSITM_PK = MEASURE_SET_ITEM.MSITM_PK

where
	OBSERVATION_SET_DETAIL.LAST_MODIFIED_DTTM between @FromDate and @ToDate
	and	coalesce(
			OBSERVATION_SET_DETAIL.MEASURE_INT 
			,OBSERVATION_SET_DETAIL.MEASURE_DECIMAL 	
			,OBSERVATION_SET_DETAIL.MEASURE_LIST
			,OBSERVATION_SET_DETAIL.MEASURE_FLAG 
			) is not null

) ObservationSetDetail

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
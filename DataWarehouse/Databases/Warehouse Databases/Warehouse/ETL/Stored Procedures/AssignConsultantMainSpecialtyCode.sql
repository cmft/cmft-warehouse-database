﻿

CREATE proc [ETL].[AssignConsultantMainSpecialtyCode]

(
@consultantcode varchar(10)
,@mainspecialtycode varchar(50)
)

as

--select
--	*
--from
--	[dbo].[EntityXref]
--where
--	EntityTypeCode = 'CONSULTANTPASCODE'
--and
--	XrefEntityTypeCode = 'CONSULTANTMAINSPECIALTYPASCODE'

insert into
	[dbo].[EntityXref]
(
[EntityTypeCode]
,[EntityCode]
,[XrefEntityTypeCode]
,[XrefEntityCode]
)

values
(
'CONSULTANTPASCODE'
,@consultantcode
,'CONSULTANTMAINSPECIALTYPASCODE'
,@mainspecialtycode
)



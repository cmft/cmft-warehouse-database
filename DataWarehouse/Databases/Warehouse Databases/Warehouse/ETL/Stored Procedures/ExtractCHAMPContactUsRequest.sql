﻿



CREATE proc [ETL].[ExtractCHAMPContactUsRequest] 

	 @FromDate	datetime = null
	,@ToDate	datetime = null

as


/*
When		Who			What
========================================================
23/02/2015	Paul Egan	Initial Coding
*/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

set @StartTime = getdate()
set @RowsInserted = 0


insert into ETL.TImportCHAMPContactUsRequest
	(
	id
	,timeOfContactUsRequest
	,email
	,content
	,Created
	)
select
	id
	,timeOfContactUsRequest
	,email
	,content
	,Created = getdate()
from
	[$(CHAMP)].ReportBase.ContactUsRequest
;


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	










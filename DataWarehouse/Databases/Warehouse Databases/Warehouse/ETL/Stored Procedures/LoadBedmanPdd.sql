﻿


CREATE procedure [ETL].[LoadBedmanPdd] as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

TRUNCATE TABLE Bedman.Pdd

INSERT INTO Bedman.Pdd
(
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,CasenoteNumber
	,AdmissionDate
	,AdmissionTime
	,WardTypeCode
	,DischargeDate
	,DischargeTime
	,SpecialtyCode
	,ConsultantCode
	,InterfaceCode
	,PddSequence
	,Pdd
	,PddComment
	,PddEntered
	,PddReason
	,PddPasFlag
	,PddEnteredWithinTarget
	,ExpectedLOS
	,Created
	,Updated
	,ByWhom
)

SELECT 	 
	 EncounterRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,ProviderSpellNo
	,CasenoteNumber
	,AdmissionDate
	,AdmissionTime
	,WardTypeCode
	,DischargeDate
	,DischargeTime
	,SpecialtyCode
	,ConsultantCode
	,InterfaceCode
	,PddSequence
	,Pdd
	,PddComment
	,PddEntered
	,PddReason
	,PddPasFlag
	,PddEnteredWithinTarget
	,ExpectedLOS
	,Created
	,Updated
	,ByWhom	

FROM 
	ETL.TLoadBedmanPdd
	
select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' 


exec WriteAuditLogEvent 'LoadBedmanPdd', @Stats, @StartTime




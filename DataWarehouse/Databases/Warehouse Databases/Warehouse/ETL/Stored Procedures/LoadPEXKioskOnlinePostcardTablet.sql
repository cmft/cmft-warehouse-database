﻿

CREATE proc [ETL].[LoadPEXKioskOnlinePostcardTablet]
(
	@fromDate date = null
	,@toDate date = null
)

as

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime = getdate()
declare @Elapsed int
declare @Stats varchar(255)

select @fromDate = coalesce(@fromDate, (select cast(dateadd(year, -1, getdate()) as date)))
select @toDate = coalesce(@toDate, (select cast(getdate() as date)))

--Extract
exec ETL.ExtractCRTPEXKioskResponse @fromDate, @toDate
exec ETL.ExtractCRTPEXPostcardResponse @fromDate, @toDate
exec ETL.ExtractCRTPEXOnlineResponse @fromDate, @toDate
exec ETL.ExtractCRTPEXTabletResponse @fromDate, @toDate

--Load Reference Data
exec ETL.LoadPEXKioskOnlinePostcardTabletReferenceData

--Load Response
exec ETL.LoadPEXKioskResponse
exec ETL.LoadPEXOnlineResponse
exec ETL.LoadPEXPostcardResponse
exec ETL.LoadPEXTabletResponse

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@fromDate, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@toDate, ''))

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿
CREATE PROCEDURE [ETL].[LoadSymphonyAuditPatientDetail]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony AuditPatientDetail table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	AuditPatientID = cast(AuditPatientID as int)
	,PatientID = cast(PatientID as int)
	,GpID = cast(GpID as int)
	,PracticeID = cast(PracticeID as int)
	,HonoursID = cast(HonoursID as int)
	,PracticeAddressID = cast(PracticeAddressID as int)
	,NoteID = cast(NoteID as int)
	,DateOfDeath = cast(DateOfDeath as datetime)
	,MaritalStatusID = cast(MaritalStatusID as int)
	,ReligionID = cast(ReligionID as int)
	,EthnicCategoryID = cast(EthnicCategoryID as int)
	,Occupation = cast(Occupation as varchar(50))
	,EmploymentStatusID = cast(EmploymentStatusID as int)
	,OverseasVisitorStatusID = cast(OverseasVisitorStatusID as int)
	,NationalityID = cast(NationalityID as int)
	,PUPID = cast(PUPID as int)
	,PEPID = cast(PEPID as int)
	,CreatedByID = cast(CreatedByID as int)
	,UpdatedTime = cast(UpdatedTime as datetime)
	,GpMoveID = cast(GpMoveID as int)
	,SpecialCase = cast(SpecialCase as varchar(255))
	,IsSpecialCase = cast(IsSpecialCase as bit)
into
	#TLoadSymphonyAuditPatientDetailFirstPass
from
	(
	select
		AuditPatientID = Aud_Patient_Details.pdt_identity
		,PatientID = Aud_Patient_Details.pdt_pid
		,GpID = Aud_Patient_Details.pdt_gpid
		,PracticeID = Aud_Patient_Details.pdt_practise
		,HonoursID = Aud_Patient_Details.pdt_honours
		,PracticeAddressID = Aud_Patient_Details.pdt_praddid
		,NoteID = Aud_Patient_Details.pdt_noteid
		,DateOfDeath = Aud_Patient_Details.pdt_dod
		,MaritalStatusID = Aud_Patient_Details.pdt_maritalstatus
		,ReligionID = Aud_Patient_Details.pdt_religion
		,EthnicCategoryID = Aud_Patient_Details.pdt_ethnic
		,Occupation = Aud_Patient_Details.pdt_occupation
		,EmploymentStatusID = Aud_Patient_Details.pdt_employmentstatus
		,OverseasVisitorStatusID = Aud_Patient_Details.pdt_overseas
		,NationalityID = Aud_Patient_Details.pdt_nationality
		,PUPID = Aud_Patient_Details.pdt_PUP
		,PEPID = Aud_Patient_Details.pdt_PEP
		,CreatedByID = Aud_Patient_Details.pdt_createdby
		,UpdatedTime = Aud_Patient_Details.pdt_update
		,GpMoveID = Aud_Patient_Details.pdt_gpmove
		,SpecialCase = Aud_Patient_Details.pdt_SpecialCaseUwm
		,IsSpecialCase = Aud_Patient_Details.pdt_IsSpecialCase
	from
		[$(Symphony)].dbo.Aud_Patient_Details
	where
		Aud_Patient_Details.pdt_deleted = 0

	) Encounter;

CREATE INDEX [#IX_Symphony_1]
    ON #TLoadSymphonyAuditPatientDetailFirstPass(PatientID ASC, UpdatedTime ASC, AuditPatientID ASC);


select
	*
	,EncounterChecksum =
		CHECKSUM(
			AuditPatientID
			,PatientID
			,GpID
			,PracticeID
			,HonoursID
			,PracticeAddressID
			,NoteID
			,DateOfDeath
			,MaritalStatusID
			,ReligionID
			,EthnicCategoryID
			,Occupation
			,EmploymentStatusID
			,OverseasVisitorStatusID
			,NationalityID
			,PUPID
			,PEPID
			,CreatedByID
			,UpdatedTime
			,GpMoveID
			,SpecialCase
			,IsSpecialCase
			)
into
	#TLoadSymphonyAuditPatientDetail
from
	#TLoadSymphonyAuditPatientDetailFirstPass AuditPatientDetail
where
	not exists
	(
	select
		1
	from
		#TLoadSymphonyAuditPatientDetailFirstPass Previous
	where
		Previous.PatientID = AuditPatientDetail.PatientID

	and
		checksum(
			 Previous.PatientID
			,Previous.GpID
			,Previous.PracticeID
			,Previous.HonoursID
			,Previous.PracticeAddressID
			,Previous.NoteID
			,Previous.DateOfDeath
			,Previous.MaritalStatusID
			,Previous.ReligionID
			,Previous.EthnicCategoryID
			,Previous.Occupation
			,Previous.EmploymentStatusID
			,Previous.OverseasVisitorStatusID
			,Previous.NationalityID
			,Previous.PUPID
			,Previous.PEPID
			,Previous.CreatedByID
			,Previous.GpMoveID
			,Previous.SpecialCase
			,Previous.IsSpecialCase
		)
		=
		checksum(
			 AuditPatientDetail.PatientID
			,AuditPatientDetail.GpID
			,AuditPatientDetail.PracticeID
			,AuditPatientDetail.HonoursID
			,AuditPatientDetail.PracticeAddressID
			,AuditPatientDetail.NoteID
			,AuditPatientDetail.DateOfDeath
			,AuditPatientDetail.MaritalStatusID
			,AuditPatientDetail.ReligionID
			,AuditPatientDetail.EthnicCategoryID
			,AuditPatientDetail.Occupation
			,AuditPatientDetail.EmploymentStatusID
			,AuditPatientDetail.OverseasVisitorStatusID
			,AuditPatientDetail.NationalityID
			,AuditPatientDetail.PUPID
			,AuditPatientDetail.PEPID
			,AuditPatientDetail.CreatedByID
			,AuditPatientDetail.GpMoveID
			,AuditPatientDetail.SpecialCase
			,AuditPatientDetail.IsSpecialCase
		)

	and	(
			Previous.UpdatedTime < AuditPatientDetail.UpdatedTime
		or	(
				Previous.UpdatedTime = AuditPatientDetail.UpdatedTime
			and	Previous.AuditPatientID < AuditPatientDetail.AuditPatientID
			)
		)

	)
order by
	AuditPatientID


create unique clustered index #IX_TLoadSymphonyPatient on #TLoadSymphonyAuditPatientDetail
	(
	AuditPatientID ASC
	)

declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.AuditPatientDetail target
using
	(
	select
		AuditPatientID
		,PatientID
		,GpID
		,PracticeID
		,HonoursID
		,PracticeAddressID
		,NoteID
		,DateOfDeath
		,MaritalStatusID
		,ReligionID
		,EthnicCategoryID
		,Occupation
		,EmploymentStatusID
		,OverseasVisitorStatusID
		,NationalityID
		,PUPID
		,PEPID
		,CreatedByID
		,UpdatedTime
		,GpMoveID
		,SpecialCase
		,IsSpecialCase
		,EncounterChecksum
	from
		#TLoadSymphonyAuditPatientDetail
	
	) source
	on	source.AuditPatientID = target.AuditPatientID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			AuditPatientID
			,PatientID
			,GpID
			,PracticeID
			,HonoursID
			,PracticeAddressID
			,NoteID
			,DateOfDeath
			,MaritalStatusID
			,ReligionID
			,EthnicCategoryID
			,Occupation
			,EmploymentStatusID
			,OverseasVisitorStatusID
			,NationalityID
			,PUPID
			,PEPID
			,CreatedByID
			,UpdatedTime
			,GpMoveID
			,SpecialCase
			,IsSpecialCase

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			source.AuditPatientID
			,source.PatientID
			,source.GpID
			,source.PracticeID
			,source.HonoursID
			,source.PracticeAddressID
			,source.NoteID
			,source.DateOfDeath
			,source.MaritalStatusID
			,source.ReligionID
			,source.EthnicCategoryID
			,source.Occupation
			,source.EmploymentStatusID
			,source.OverseasVisitorStatusID
			,source.NationalityID
			,source.PUPID
			,source.PEPID
			,source.CreatedByID
			,source.UpdatedTime
			,source.GpMoveID
			,source.SpecialCase
			,source.IsSpecialCase

			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			target.PatientID = source.PatientID
			,target.GpID = source.GpID
			,target.PracticeID = source.PracticeID
			,target.HonoursID = source.HonoursID
			,target.PracticeAddressID = source.PracticeAddressID
			,target.NoteID = source.NoteID
			,target.DateOfDeath = source.DateOfDeath
			,target.MaritalStatusID = source.MaritalStatusID
			,target.ReligionID = source.ReligionID
			,target.EthnicCategoryID = source.EthnicCategoryID
			,target.Occupation = source.Occupation
			,target.EmploymentStatusID = source.EmploymentStatusID
			,target.OverseasVisitorStatusID = source.OverseasVisitorStatusID
			,target.NationalityID = source.NationalityID
			,target.PUPID = source.PUPID
			,target.PEPID = source.PEPID
			,target.CreatedByID = source.CreatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.GpMoveID = source.GpMoveID
			,target.SpecialCase = source.SpecialCase
			,target.IsSpecialCase = source.IsSpecialCase

			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
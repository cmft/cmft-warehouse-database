﻿
CREATE PROCEDURE [ETL].[LoadIUIEncounter]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, TreatmentDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, TreatmentDate)) 
from
	ETL.TLoadIUIEncounter

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	IUI.Encounter target
using
	(
	select
		 DateOfBirth
		,NHSNumber
		,CasenoteNumber
		,PatientName
		,AttemptNumber
		,CommissionerCode
		,TreatmentDate
		,OutcomeID
		,TreatmentID
		,InterfaceCode
	from
		ETL.TLoadIUIEncounter
	) source
	on	source.TreatmentDate = target.TreatmentDate
	and	source.CasenoteNumber = target.CasenoteNumber
	when not matched by source
	and	target.TreatmentDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			 DateOfBirth
			,NHSNumber
			,CasenoteNumber
			,PatientName
			,AttemptNumber
			,CommissionerCode
			,TreatmentDate
			,OutcomeID
			,TreatmentID
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.DateOfBirth
			,source.NHSNumber
			,source.CasenoteNumber
			,source.PatientName
			,source.AttemptNumber
			,source.CommissionerCode
			,source.TreatmentDate
			,source.OutcomeID
			,source.TreatmentID
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.PatientName, '') = isnull(source.PatientName, '')
		and isnull(target.AttemptNumber, 0) = isnull(source.AttemptNumber, 0)
		and isnull(target.CommissionerCode, '') = isnull(source.CommissionerCode, '')
		and isnull(target.TreatmentDate, getdate()) = isnull(source.TreatmentDate, getdate())
		and isnull(target.OutcomeID, 0) = isnull(source.OutcomeID, 0)
		and isnull(target.TreatmentID, 0) = isnull(source.TreatmentID, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			 target.DateOfBirth = source.DateOfBirth
			,target.NHSNumber = source.NHSNumber
			,target.CasenoteNumber = source.CasenoteNumber
			,target.PatientName = source.PatientName
			,target.AttemptNumber = source.AttemptNumber
			,target.CommissionerCode = source.CommissionerCode
			,target.TreatmentDate = source.TreatmentDate
			,target.OutcomeID = source.OutcomeID
			,target.TreatmentID = source.TreatmentID
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
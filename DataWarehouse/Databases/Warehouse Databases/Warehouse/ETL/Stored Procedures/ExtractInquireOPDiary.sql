﻿CREATE procedure [ETL].[ExtractInquireOPDiary]
	 @debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select
	@from = convert(varchar, dateadd(month, -7, getdate()), 112)

insert into ETL.TImportOPDiary
	(
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionTimeRange
	,ReasonForCancellation
	,SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode
	,DoctorCode
	)
select
	 SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionDate
	,SessionTimeRange
	,ReasonForCancellation
	,SessionPeriod
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	,InterfaceCode = 'INQ'
	,DoctorCode
from
	(
	select
		 DIARYID SourceUniqueID
		,SchedResGrpActvCd ClinicCode
		,ResCode SessionCode
		,Comment SessionDescription
		,ScheduleDateNeutralFormat SessionDate
		,SchResSessTimeRange SessionTimeRange
		,ReasonForCancel ReasonForCancellation
		,SchedulingResourceSessionDescription SessionPeriod
		,Units
		,UsedUnits
		,FreeUnits
		,ValidApptTypes ValidAppointmentTypeCode
		,ResCode DoctorCode
	from
		[$(PAS)].Inquire.DIARY
	where
		ScheduleDateNeutralFormat > @from
	) Diary


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireOPDiary', @Stats, @StartTime

﻿
CREATE PROCEDURE [ETL].[LoadSymphonyResult]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Result table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 ResultID
			,RequestID
			,AttendanceID
			,DepartmentID
			,ResultTime
			,ResultID1
			,Field1ID
			,Field2ID
			,Field3ID
			,Field4ID
			,Field5ID
			,Field6ID
			,Field7ID
			,Field8ID
			,Field9ID
			,Field10ID
			,Field11ID
			,Field12ID
			,Field13ID
			,StaffID
			,HasExtraFields
			,NoteID
			,IncludingPatientLetter
			,CreatedByID
			,CreatedTime
			,UpdatedByID
			,UpdatedTime
			,CautionID
			,RepeatID
			,DnmImageid
			,TPTransID
			,IsCancellation
			)
into
	#TLoadSymphonyResult
from
	(
	select
		 ResultID
		,RequestID = cast(RequestID as int)
		,AttendanceID = cast(AttendanceID as int)
		,DepartmentID = cast(DepartmentID as smallint)
		,ResultTime = cast(ResultTime as datetime)
		,ResultID1 = cast(ResultID1 as int)
		,Field1ID = cast(Field1ID as int)
		,Field2ID = cast(Field2ID as int)
		,Field3ID = cast(Field3ID as int)
		,Field4ID = cast(Field4ID as int)
		,Field5ID = cast(Field5ID as int)
		,Field6ID = cast(Field6ID as varchar(255))
		,Field7ID = cast(Field7ID as varchar(255))
		,Field8ID = cast(Field8ID as varchar(255))
		,Field9ID = cast(Field9ID as varchar(255))
		,Field10ID = cast(Field10ID as varchar(255))
		,Field11ID = cast(Field11ID as varchar(255))
		,Field12ID = cast(Field12ID as varchar(255))
		,Field13ID = cast(Field13ID as varchar(255))
		,StaffID = cast(StaffID as int)
		,HasExtraFields = cast(HasExtraFields as bit)
		,NoteID = cast(NoteID as int)
		,IncludingPatientLetter = cast(IncludingPatientLetter as bit)
		,CreatedByID = cast(CreatedByID as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,UpdatedByID = cast(UpdatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,CautionID = cast(CautionID as int)
		,RepeatID = cast(RepeatID as int)
		,DnmImageid = cast(DnmImageid as int)
		,TPTransID = cast(TPTransID as varchar(30))
		,IsCancellation = cast(IsCancellation as bit)

	from
		(
		select
			 ResultID = Result.res_resid
			,RequestID = Result.res_reqid
			,AttendanceID = Result.res_atdid
			,DepartmentID = Result.res_depid
			,ResultTime = Result.res_date
			,ResultID1 = Result.res_result
			,Field1ID = Result.res_field1
			,Field2ID = Result.res_field2
			,Field3ID = Result.res_field3
			,Field4ID = Result.res_field4
			,Field5ID = Result.res_field5
			,Field6ID = Result.res_field6
			,Field7ID = Result.res_field7
			,Field8ID = Result.res_field8
			,Field9ID = Result.res_field9
			,Field10ID = Result.res_field10
			,Field11ID = Result.res_field11
			,Field12ID = Result.res_field12
			,Field13ID = Result.res_field13
			,StaffID = Result.res_staff1
			,HasExtraFields = Result.res_hasefds
			,NoteID = Result.res_note
			,IncludingPatientLetter = Result.res_includeingpletter
			,CreatedByID = Result.res_createdby
			,CreatedTime = Result.res_created
			,UpdatedByID = Result.res_updatedby
			,UpdatedTime = Result.res_updated
			,CautionID = Result.res_caution
			,RepeatID = Result.res_repeat
			,DnmImageid = Result.res_dnmimageid
			,TPTransID = Result.Res_TPTransID
			,IsCancellation = Result.res_IsCancellation
		from
			[$(Symphony)].dbo.Result_details Result
		where
			Result.res_inactive <> 1

		) Encounter
	) Encounter
order by
	ResultID


create unique clustered index #IX_TLoadSymphonyResult on #TLoadSymphonyResult
	(
	ResultID ASC
	)



declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Result target
using
	(
	select
		 ResultID
		,RequestID
		,AttendanceID
		,DepartmentID
		,ResultTime
		,ResultID1
		,Field1ID
		,Field2ID
		,Field3ID
		,Field4ID
		,Field5ID
		,Field6ID
		,Field7ID
		,Field8ID
		,Field9ID
		,Field10ID
		,Field11ID
		,Field12ID
		,Field13ID
		,StaffID
		,HasExtraFields
		,NoteID
		,IncludingPatientLetter
		,CreatedByID
		,CreatedTime
		,UpdatedByID
		,UpdatedTime
		,CautionID
		,RepeatID
		,DnmImageid
		,TPTransID
		,IsCancellation
		,EncounterChecksum
	from
		#TLoadSymphonyResult
	
	) source
	on	source.ResultID = target.ResultID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 ResultID
			,RequestID
			,AttendanceID
			,DataEntryProcedureID
			,ResultTime
			,ResultID1
			,Field1ID
			,Field2ID
			,Field3ID
			,Field4ID
			,Field5ID
			,Field6ID
			,Field7ID
			,Field8ID
			,Field9ID
			,Field10ID
			,Field11ID
			,Field12ID
			,Field13ID
			,StaffID
			,HasExtraFields
			,NoteID
			,IncludingPatientLetter
			,CreatedByID
			,CreatedTime
			,UpdatedByID
			,UpdatedTime
			,CautionID
			,RepeatID
			,DnmImageid
			,TPTransID
			,IsCancellation

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.ResultID
			,source.RequestID
			,source.AttendanceID
			,source.DepartmentID
			,source.ResultTime
			,source.ResultID1
			,source.Field1ID
			,source.Field2ID
			,source.Field3ID
			,source.Field4ID
			,source.Field5ID
			,source.Field6ID
			,source.Field7ID
			,source.Field8ID
			,source.Field9ID
			,source.Field10ID
			,source.Field11ID
			,source.Field12ID
			,source.Field13ID
			,source.StaffID
			,source.HasExtraFields
			,source.NoteID
			,source.IncludingPatientLetter
			,source.CreatedByID
			,source.CreatedTime
			,source.UpdatedByID
			,source.UpdatedTime
			,source.CautionID
			,source.RepeatID
			,source.DnmImageid
			,source.TPTransID
			,source.IsCancellation

			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.ResultID = source.ResultID
			,target.RequestID = source.RequestID
			,target.AttendanceID = source.AttendanceID
			,target.DataEntryProcedureID = source.DepartmentID
			,target.ResultTime = source.ResultTime
			,target.ResultID1 = source.ResultID1
			,target.Field1ID = source.Field1ID
			,target.Field2ID = source.Field2ID
			,target.Field3ID = source.Field3ID
			,target.Field4ID = source.Field4ID
			,target.Field5ID = source.Field5ID
			,target.Field6ID = source.Field6ID
			,target.Field7ID = source.Field7ID
			,target.Field8ID = source.Field8ID
			,target.Field9ID = source.Field9ID
			,target.Field10ID = source.Field10ID
			,target.Field11ID = source.Field11ID
			,target.Field12ID = source.Field12ID
			,target.Field13ID = source.Field13ID
			,target.StaffID = source.StaffID
			,target.HasExtraFields = source.HasExtraFields
			,target.NoteID = source.NoteID
			,target.IncludingPatientLetter = source.IncludingPatientLetter
			,target.CreatedByID = source.CreatedByID
			,target.CreatedTime = source.CreatedTime
			,target.UpdatedByID = source.UpdatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.CautionID = source.CautionID
			,target.RepeatID = source.RepeatID
			,target.DnmImageid = source.DnmImageid
			,target.TPTransID = source.TPTransID
			,target.IsCancellation = source.IsCancellation
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


--Update IsFirst and IsLast to improve subsequent performance
update
	Symphony.Result
set
	IsFirst =
		case
		when
			Result.ResultID =
			(
			select top 1
				PreviousResult.ResultID
			from
				Symphony.Result PreviousResult
			where
				PreviousResult.DataEntryProcedureID = Result.DataEntryProcedureID
			and	PreviousResult.AttendanceID = Result.AttendanceID
			
			order by
				PreviousResult.ResultTime asc
				,Result.ResultID asc
			)
		then 1
		else 0
		end

	,IsLast =
		case
		when
			Result.ResultID =
			(
			select top 1
				PreviousResult.ResultID
			from
				Symphony.Result PreviousResult
			where
				PreviousResult.DataEntryProcedureID = Result.DataEntryProcedureID
			and	PreviousResult.AttendanceID = Result.AttendanceID
			
			order by
				PreviousResult.ResultTime desc
				,Result.ResultID desc
			)
		then 1
		else 0
		end
from
	Symphony.Result
where
	exists
	(
	select
		1
	from
		Symphony.Result Touched
	where
		Touched.DataEntryProcedureID = Result.DataEntryProcedureID
	and	Touched.AttendanceID = Result.AttendanceID
	and	exists
		(
		select
			1
		from
			@ProcessList ProcessList
		where
			ProcessList.EncounterRecno = Touched.EncounterRecno
		)
	)


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
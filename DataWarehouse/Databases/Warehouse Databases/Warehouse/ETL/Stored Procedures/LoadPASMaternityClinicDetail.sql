﻿CREATE PROCEDURE [ETL].[LoadPASMaternityClinicDetail]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select
	@StartTime = getdate()


truncate table PAS.MaternityClinicDetail

INSERT INTO PAS.MaternityClinicDetail
(
	 MaternityClinicDetailID
	,SourceSpellNo
	,SourcePatientNo
	,PatientKnownToBeImmune
	,RubellaAntibodyImmune
	,StatusOfPersonConductingDeliveryCode
	,MotherHeight
	,NumberOfPreviousNeonatalDeaths
	,NumberOfPreviousNonInducedAbortions
	,NumberOfPreviousCaesarians
	,NumberOfPreviousInducedAbortions
	,NumberOfPreviousRegisteredLiveBirths
	,NumberOfPreviousRegisteredStillBirths
	,NumberOfPreviousTransfusions
	,AntibodyStatusTested
	,AntibodyStatusTestedPositive
) 
select
	 MaternityClinicDetailID
	,SourceSpellNo
	,SourcePatientNo
	,PatientKnownToBeImmune
	,RubellaAntibodyImmune
	,StatusOfPersonConductingDeliveryCode
	,MotherHeight
	,NumberOfPreviousNeonatalDeaths
	,NumberOfPreviousNonInducedAbortions
	,NumberOfPreviousCaesarians
	,NumberOfPreviousInducedAbortions
	,NumberOfPreviousRegisteredLiveBirths
	,NumberOfPreviousRegisteredStillBirths
	,NumberOfPreviousTransfusions
	,AntibodyStatusTested
	,AntibodyStatusTestedPositive
from
	ETL.TLoadPASMaternityClinicDetail

SELECT @RowsInserted = @@Rowcount



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'LoadPASMaternityClinicDetail', @Stats, @StartTime

print @Stats

﻿
create proc ETL.ExtractDictationSpeechDocument

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted int
declare @Sproc varchar(255)
declare @Stats varchar(255);

select @StartTime = getdate();

truncate table ETL.TImportDictationSpeechDocument

insert into ETL.TImportDictationSpeechDocument

select
	[ID]
	,[ExternIdentification]
	,[ConnectionInfoID]
	,[CreationDate]
	,[Title]
	,[Length]
	,[InterfaceID]
	,[DocumentType]
	,[GroupID]
	,[UserIDAuthor]
	,[UserIDTranscriptionist]
	,[UserIDExporter]
	,[UserIDAuthoriser]
	,[ExternTextID]
	,[WorktypeID]
	,[BatchNr]
	,[StatusID]
	,[CheckOutStatus]
	,[Priority]
	,[WorkPlace]
	,[RecRate]
	,[MatterNumber]
	,[Reserved02]
	,[InputDevice]
	,[Supervisor]
	,[Reserved05]
	,[UserDefined01]
	,[UserDefined02]
	,[UserDefined03]
	,[UserDefined04]
	,[UserDefined05]
	,[ContextID]
	,[NextStatusId]
	,[SoundPos]
	,[SrIdentification]
	,[Reserved01]
	,[Reserved03]
	,[Reserved04]
	,[UserIDSupervisor]
	,[CurrentUserID]
	,[CurrentWorkstationId]
	,[CurrentApplicationId]
	,[CorrectionDate]
	,[Reserved06]
	,[Reserved07]
	,[Reserved08]
	,[Reserved09]
	,[Reserved10]
	,[AuthorisationDate]
	,[CorRate]
from 
	[$(G2_G2Speech5)].[dbo].[G2Document]

select @RowsInserted = @@rowcount

select @Elapsed = datediff(minute, @StartTime, getdate());
select @Stats = 'Rows extracted ' + convert(varchar(10), @RowsInserted) + ', Time Elapsed ' + 	convert(varchar(6), @Elapsed) + ' Mins'
select @Sproc = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID);

exec WriteAuditLogEvent @Sproc, @Stats, @StartTime;


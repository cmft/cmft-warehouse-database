﻿CREATE procedure [ETL].[ExtractInquireWAOperation]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(8)
declare @to varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112)

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112)


insert into ETL.TImportWAOperation
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,WASourceUniqueID
)
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,OperationCode
	,OperationDate
	,ConsultantCode
	,WASourceUniqueID
from
	(
	SELECT 
		 WARDATTSECPROC.WARDATTSECPROCID SourceUniqueID
		,WARDATTSECPROC.InternalPatientNumber SourcePatientNo
		,WARDATTSECPROC.EpisodeNumber SourceEncounterNo
		,left(WARDATTSECPROC.SeqNo, 2) SequenceNo
		,WARDATTSECPROC.SecondaryProcCode OperationCode
		,left(WARDATTSECPROC.AttendanceDate, 10) OperationDate
		,null ConsultantCode
		,WARDATTENDER.WARDATTENDERID WASourceUniqueID
	FROM
		[$(PAS)].Inquire.WARDATTENDER

	INNER JOIN [$(PAS)].Inquire.WARDATTSECPROC
	ON	WARDATTENDER.EpisodeNumber = WARDATTSECPROC.EpisodeNumber
	AND	WARDATTENDER.InternalPatientNumber = WARDATTSECPROC.InternalPatientNumber

	WHERE
		WARDATTENDER.AttendanceDateInternal between @from and @to
	and	WARDATTENDER.SeenBy = 'DOCTOR'
	) Activity




select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireWAOperation', @Stats, @StartTime

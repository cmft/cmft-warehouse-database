﻿
CREATE proc [ETL].[ExtractInquireOCMCodedResult]

(
@FromDate date = null
,@ToDate date = null
,@Update bit

)

as

--set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

if @Update = 0

begin
	insert into ETL.TImportOCMCodedResult
	(
	SourceUniqueID
	,SourcePatientNo
	,SourceEpisodeNo
	,SequenceNo
	,TransactionNo
	,SessionNo
	,ResultCode
	,Result
	,ResultDate
	,ResultTime
	,RangeIndicator
	,EnteredBy
	,StatusCode
	,Comment1
	,Comment2
	,Comment3
	,Comment4
	,Comment5
	,Comment6
	)

	select
		 SourceUniqueID
		,SourcePatientNo 
		,SourceEpisodeNo 
		,SequenceNo
		,TransactionNo 
		,SessionNo 
		,Result.ResultCode
		,Result.Result
		,ResultDate 
		,ResultTime
		,Result.RangeIndicator
		,Result.EnteredBy
		,StatusCode 
		,Result.Comment1
		,Result.Comment2
		,Result.Comment3
		,Result.Comment4
		,Result.Comment5
		,Result.Comment6
	from
		(
		select --top 100 
			 SourceUniqueID = Result.OCRESULTID
			,SourcePatientNo = Link.InternalPatientNumber
			,SourceEpisodeNo = Link.EpisodeNumber
			,SequenceNo = Link.TransNo
			,TransactionNo = Result.OcmTransKey
			,SessionNo = Result.SessionNumber
			,ResultCode = ltrim(rtrim(Result.ResultCode))
			,Result.Result
			,ResultDate 
			,ResultTime =
				case
				when Result.ResultTime = '24:00'
				then convert(smalldatetime , dateadd(day , 1 , Result.ResultDate))
				else convert(smalldatetime , cast(Result.ResultDate as varchar(20)) + ' '  + Result.ResultTime)
				end
			,Result.RangeIndicator
			,Result.EnteredBy
			,StatusCode = Result.Status
			,Result.Comment1
			,Result.Comment2
			,Result.Comment3
			,Result.Comment4
			,Result.Comment5
			,Result.Comment6
		from
			[$(PAS)].Inquire.OCRESULT Result

		inner join [$(PAS)].Inquire.OCMPTLINK Link
		on	Link.OcmTransKey = Result.OcmTransKey
		and not exists -- duplicate results across different patient link records?
					(
					select
						1
					from
						[$(PAS)].Inquire.OCMPTLINK LinkLater
					where
						LinkLater.OcmTransKey = Link.OcmTransKey
					and LinkLater.EpisodeNumber > Link.EpisodeNumber
					)

		--Within each OCMTransKey there may be multiple order sessions and result sessions.
		--These are essentially storing a version history.
		--The session numbers decrement by one for each version created (this is for Cache optimisation)
		--E.g. 2 hours after ordering a test the first few results are sent back (creating the first result session); 4 hours later more results are available and the entire result set is sent back (creating a second result session)
		--Therefore the only result session that you need to worry about is the one with the lowest result session number
		
		where
			Result.ResultDate between @FromDate and @ToDate
		and Result.Status = '85' -- Confirmed as required by D.Forster 24.9.14
		and not exists
					(
					select
						1
					from
						[$(PAS)].Inquire.OCRESULT EarlierSession
					where
						EarlierSession.OcmTransKey = Result.OcmTransKey
					and	EarlierSession.SessionNumber < Result.SessionNumber
					)

		) Result
end

else if @Update = 1

begin
	insert into ETL.TImportOCMCodedResult
	(
	SourceUniqueID
	,SourcePatientNo
	,SourceEpisodeNo
	,SequenceNo
	,TransactionNo
	,SessionNo
	,ResultCode
	,Result
	,ResultDate
	,ResultTime
	,RangeIndicator
	,EnteredBy
	,StatusCode
	,Comment1
	,Comment2
	,Comment3
	,Comment4
	,Comment5
	,Comment6
	)

	select
		 SourceUniqueID
		,SourcePatientNo 
		,SourceEpisodeNo 
		,SequenceNo
		,TransactionNo 
		,SessionNo 
		,Result.ResultCode
		,Result.Result
		,ResultDate 
		,ResultTime
		,Result.RangeIndicator
		,Result.EnteredBy
		,StatusCode 
		,Result.Comment1
		,Result.Comment2
		,Result.Comment3
		,Result.Comment4
		,Result.Comment5
		,Result.Comment6
	from
		(
		select --top 100 
			 SourceUniqueID = Result.OCRESULTID
			,SourcePatientNo = Link.InternalPatientNumber
			,SourceEpisodeNo = Link.EpisodeNumber
			,SequenceNo = Link.TransNo
			,TransactionNo = Result.OcmTransKey
			,SessionNo = Result.SessionNumber
			,ResultCode = ltrim(rtrim(Result.ResultCode))
			,Result.Result
			,ResultDate 
			,ResultTime =
				case
				when Result.ResultTime = '24:00'
				then convert(smalldatetime , dateadd(day , 1 , Result.ResultDate))
				else convert(smalldatetime , cast(Result.ResultDate as varchar(20)) + ' '  + Result.ResultTime)
				end
			,Result.RangeIndicator
			,Result.EnteredBy
			,StatusCode = Result.Status
			,Result.Comment1
			,Result.Comment2
			,Result.Comment3
			,Result.Comment4
			,Result.Comment5
			,Result.Comment6
		from
			[$(PAS)].InquireUpdate.OCRESULT Result

		inner join [$(PAS)].InquireUpdate.OCMPTLINK Link
		on	Link.OcmTransKey = Result.OcmTransKey
		and not exists -- duplicate results across different patient link records?
					(
					select
						1
					from
						[$(PAS)].InquireUpdate.OCMPTLINK LinkLater
					where
						LinkLater.OcmTransKey = Link.OcmTransKey
					and LinkLater.EpisodeNumber > Link.EpisodeNumber
					)

		--Within each OCMTransKey there may be multiple order sessions and result sessions.
		--These are essentially storing a version history.
		--The session numbers decrement by one for each version created (this is for Cache optimisation)
		--E.g. 2 hours after ordering a test the first few results are sent back (creating the first result session); 4 hours later more results are available and the entire result set is sent back (creating a second result session)
		--Therefore the only result session that you need to worry about is the one with the lowest result session number
		
		where
			Result.Status = '85' -- Confirmed as required by D.Forster 24.9.14
		and not exists
					(
					select
						1
					from
						[$(PAS)].InquireUpdate.OCRESULT EarlierSession
					where
						EarlierSession.OcmTransKey = Result.OcmTransKey
					and	EarlierSession.SessionNumber < Result.SessionNumber
					)

		) Result
end

select
	@RowsInserted = @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

	
﻿

CREATE proc [ETL].[ExtractPICANetPCCPeriod]

as

/******************************************************************************************************
Stored Procedure	: [ETL].[ExtractPICANetPCCPeriod]
Description			: Paediatric Critical Care - period (parent) level extract

Modification History
====================

Date		Person			Description
=======================================================================================================
23/10/2014	Paul Egan       Initial coding.
30/06/2015	Rachel Royston	Added Pauls script for HCD and PCC to this proc, changed TImport to PCC.Period as this is referenced in the CDS62 procs??
*******************************************************************************************************/


declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @xmlData xml;

select @RowsInserted = 0

set @xmlData = 
	(
	select 
		xmlData 
	from 
		[$(SmallDatasets)].PICANet.BaseExtract 
	where 
		ExtractRecno =	
		(
		select 
			max(ExtractRecno) 
		from 
			[$(SmallDatasets)].PICANet.BaseExtract
		)	-- *** Does this need changing, maybe to a cursor in case more than one extract file needs processing? ***
	)



Insert into PCC.Period
	(
	 PeriodID
	,NHSNumber
	,CasenoteNo
	,DateOfBirth
	,AdmissionTime
	,DischargeTime
	,UnitFunctionCode
	,IsolationReasonCode
	,PrimaryDiagnosisReadCode
	,SecondaryDiagnosisReadCode1
	,SecondaryDiagnosisReadCode2
	,SecondaryDiagnosisReadCode3
	,Created
	,Updated
	,ByWhom
	)
select
	EventID = T.Episode.value('(EventID)[1]', 'int')
	,NHSNumber = T.Episode.value('(nhsNumber)[1]', 'varchar(50)')
	,CaseNo = T.Episode.value('(CaseNo)[1]', 'varchar(50)')
	,DateOfBirth = cast(left(T.Episode.value('(dateOfBirth)[1]', 'varchar(50)'), 10) as date)
	,AdmissionTime = cast(T.Episode.value('(admissionDate)[1]', 'varchar(10)') + ' ' + T.Episode.value('(admissionTime)[1]', 'varchar(50)') as datetime)
	,DischargeTime = cast(T.Episode.value('(dischargeDate)[1]', 'varchar(10)') + ' ' + T.Episode.value('(dischargeTime)[1]', 'varchar(50)') as datetime)
	,UnitFunction = T.Episode.value('(unitFunction)[1]', 'int')
	,IsolationReason = T.Episode.value('(isolationReason)[1]', 'varchar(50)')
	,PrimaryDiagnosis = T.Episode.value('(primaryDiagnosis)[1]', 'varchar(50)')
	,SecondaryDiagnosis = T.Episode.value('(secondaryDiagnosis)[1]', 'varchar(50)')
	,SecondaryDiagnosis2 = T.Episode.value('(secondaryDiagnosis)[2]', 'varchar(50)')
	,SecondaryDiagnosis3 = T.Episode.value('(secondaryDiagnosis)[3]', 'varchar(50)')
	,Created = getdate()
	,Updated = getdate()
	,ByWhom = suser_name()
from
	@xmlData.nodes('picanetPccmdsExport/episode') T(Episode)
where
	not exists
	(
	Select
		1
	from
		PCC.Period
	where
		Period.PeriodID = T.Episode.value('(EventID)[1]', 'int')
	)

select
	@RowsInserted = @@ROWCOUNT



SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted PCCPeriod ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime




﻿CREATE procedure [ETL].[LoadWaitingListEntry] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @from = (select min(convert(smalldatetime, ActivityDate)) from ETL.TLoadWaitingListEntry)
select @to = (select max(convert(smalldatetime, ActivityDate)) from ETL.TLoadWaitingListEntry)

delete from APC.WaitingListEntry
where
	ActivityDate between @from and @to
or	exists
	(
	select
		1
	from
		ETL.TLoadWaitingListEntry
	where
		TLoadWaitingListEntry.SourceUniqueID = WaitingListEntry.SourceUniqueID
	)

select @RowsDeleted = @@rowcount

insert into APC.WaitingListEntry
(
	 SourceUniqueID
	,ActivityDate
	,ActivityTime
	,CasenoteNumber
	,ConsultantCode
	,DiagnosticGroupCode
	,DistrictNo
	,SourceEntityRecno
	,SourcePatientNo
	,IntendedPrimaryOperationCode
	,ManagementIntentionCode
	,SiteCode
	,AdmissionMethodCode
	,Operation
	,SpecialtyCode
	,WaitingListCode
	,ContractSerialNumber
	,PurchaserCode
	,EpisodicGpCode
	,InterfaceCode
	,Created
	,Updated
	,ByWhom
)
select
	 SourceUniqueID
	,ActivityDate
	,ActivityTime
	,CasenoteNumber
	,ConsultantCode
	,DiagnosticGroupCode
	,DistrictNo
	,SourceEntityRecno
	,SourcePatientNo
	,IntendedPrimaryOperationCode
	,ManagementIntentionCode
	,SiteCode
	,AdmissionMethodCode
	,Operation
	,SpecialtyCode
	,WaitingListCode
	,ContractSerialNumber
	,PurchaserCode
	,EpisodicGpCode
	,InterfaceCode
	,Created = getdate()
	,Updated = null
	,ByWhom = system_user
from
	ETL.TLoadWaitingListEntry

select @RowsInserted = @@rowcount

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', ' + 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@from, '')) + ' to ' +
	CONVERT(varchar(11), coalesce(@to, ''))

exec Utility.WriteAuditLogEvent 'LoadWaitingListEntry', @Stats, @StartTime

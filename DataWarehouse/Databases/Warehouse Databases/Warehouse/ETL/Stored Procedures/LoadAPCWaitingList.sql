﻿
CREATE procedure [ETL].[LoadAPCWaitingList]
as

/******************************************************************************************************
Stored procedure : [ETL].[LoadAPCWaitingList]
Description		 : As proc name

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
17/10/2014	Paul Egan       Added TCITime for day case appointment reminders.
*******************************************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

declare @CensusDate smalldatetime

select
	@CensusDate =
	(
	select top 1
		CensusDate
	from
		ETL.TImportAPCWLCurrent
	)

-- delete duplicates
delete
from
	ETL.TImportAPCWLCurrent
where
	exists
	(
	select
		1
	from
		ETL.TImportAPCWLPreadmission
	where
		TImportAPCWLPreadmission.SourcePatientNo = TImportAPCWLCurrent.SourcePatientNo
	and	TImportAPCWLPreadmission.SourceEncounterNo = TImportAPCWLCurrent.SourceEncounterNo
	)


-- Clear down old snapshots
delete
from
	APC.WaitingList
where
	CensusDate = @CensusDate


select @RowsDeleted = @@ROWCOUNT

declare @CensusCutOffDate smalldatetime

select @CensusCutOffDate =
	dateadd(
		 day
		,(
		select
			NumericValue * -1
		from
			Utility.Parameter
		where
			Parameter = 'KEEPSNAPSHOTDAYS'
		)
		,@CensusDate
	)

--last snapshot in month
select
	CensusDate = max(CensusDate) 
into
	#OldSnapshot
from
	APC.WaitingList 
where
	CensusDate < @CensusCutOffDate
group by
	 datepart(year, CensusDate)
	,datepart(month, CensusDate)
;
--sunday snapshot
set datefirst 1
insert into #OldSnapshot

select distinct
	CensusDate 
from
	APC.WaitingList

----removed to avoid circular reference
--inner join WarehouseOLAPMergedV2.WH.Calendar OlapCalendar
--on	WaitingList.CensusDate = OlapCalendar.TheDate
--and	WaitingList.CensusDate = OlapCalendar.LastDayOfWeek

where
--added to avoid circular reference
	datepart(dw, WaitingList.CensusDate) = 7

and
	WaitingList.CensusDate < @CensusCutOffDate
and	not exists
		(
		select
			1
		from
			#OldSnapshot OldSnapshot
		where
			OldSnapshot.CensusDate = WaitingList.CensusDate
		)

--2011-12-12 CCB Keep all snapshots for now
----delete unwanted snapshots
--delete from APC.WaitingList
--where
--	not exists
--	(
--	select
--		1
--	from
--		#OldSnapshot OldSnapshot
--	where
--		OldSnapshot.CensusDate = WaitingList.CensusDate
--	)
--and	CensusDate < @CensusCutOffDate


--select @RowsDeleted = @RowsDeleted + @@ROWCOUNT
select @RowsDeleted = 0


INSERT INTO APC.WaitingList
(
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,AdditionFlag
	,LocalEpisodicGpCode
	,LocalRegisteredGpCode
	,ExpectedAdmissionDate
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,AdmissionReason
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode
	,GuaranteedAdmissionDate
	,WithRTTOpenPathway
	,IsShortNotice
	,TCITime		-- Added 17/10/2014 Paul Egan
	)
select
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,ConsultantCode
	,SpecialtyCode
	,PASSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList
	,TCIDate
	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,InterfaceCode
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,AdditionFlag
	,LocalEpisodicGpCode
	,LocalRegisteredGpCode
	,ExpectedAdmissionDate
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinHomePhone
	,NextOfKinWorkPhone
	,ExpectedLOS
	,AdmissionReason
	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AddedToWaitingListTime
	,SourceUniqueID
	,AdminCategoryCode
	,GuaranteedAdmissionDate
	,WithRTTOpenPathway
	,IsShortNotice
	,TCITime		-- Added 17/10/2014 Paul Egan
from
	ETL.TLoadAPCWaitingList


select @RowsInserted = @@ROWCOUNT

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())
select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Deleted ' + CONVERT(varchar(10), @RowsDeleted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period ' + 
	CONVERT(varchar(11), coalesce(@CensusDate, ''))

EXEC Utility.WriteAuditLogEvent 'LoadAPCWaitingList', @Stats, @StartTime


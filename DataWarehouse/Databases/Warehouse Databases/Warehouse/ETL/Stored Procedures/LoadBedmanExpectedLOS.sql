﻿
CREATE PROCEDURE [ETL].[LoadBedmanExpectedLOS]

AS

BEGIN

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(19)
declare @to varchar(19)

select @StartTime = getdate()

--Insert new records from Bedman
--INSERT INTO APC.ExpectedLOS

--SELECT SourceUniqueID = ID
--	,LoadTime = GETDATE()
--	,LoadModifiedTime = NULL
--	,SourceCreatedTime = LastUpdate
--	,EDDCreatedTime = NULL
--	,SourceSystem = 'Bedman'
--	,ModifiedFromSystem = NULL
--	,AdmissionTime = AdmitDate
--	,DischargeTime = DischDate
--	,SourcePatientNo = IPAdmits.Patient
--	,SourceSpellNo = SUBSTRING([Episode], PATINDEX('%[^0]%', [Episode]+'.'), LEN([Episode]))
--	,SiteCode = AdmitHosp
--	,Ward = AdmitWard
--	,Specialty = AdmitSpec
--	,Consultant = AdmitCons
--	,ManagementIntentionCode = NULL
--	,CasenoteNumber = Casenote
--	,ExpectedLOS = NULL --DATEDIFF(dd, AdmitDate,PASEstimatedDischargeDate)
--	,EnteredWithin48hrs = 0
--	,CreateEddDaysDuration = NULL
--	,DirectorateCode = NULL
--	,AdmissionMethodCode = AdmitMethod
--	,RTTPathwayCondition = NULL
--	,ArchiveFlag = 'N'
--FROM 
--	Bedman.Spell Spell
--WHERE  
--	NOT EXISTS
--		(
--			SELECT	1
--			FROM	APC.ExpectedLOS
--			WHERE	ExpectedLOS.SourcePatientNo = Spell.SourcePatientNo
--				AND ExpectedLOS.SourceSpellNo =  Spell.SourceSpellNo
--		)
--	AND NOT EXISTS
--		(
--		SELECT	1
--			FROM	APC.ExpectedLOS
--			WHERE	ExpectedLOS.SourceUniqueID = Spell.SourceUniqueID
--					AND SourceSystem = 'Bedman'
--		)
--	AND AdmissionDate >='01/April/2011'


--UPDATE table with bedman EDD's
UPDATE APC.ExpectedLOS

SET	 
	 ExpectedLOS = datediff(dd,ExpectedLOS.AdmissionTime, Pdd.Pdd)
	,LoadModifiedTime = GETDATE()
	,ModifiedFromSystem = 'Bedman'
	,EddCreatedTime  = Pdd.PddEntered
	,SourceCreatedTime = Pdd.PddEntered
	,EnteredWithin48hrs =CASE 
							WHEN DATEDIFF(MINUTE,ExpectedLOS.AdmissionTime, Pdd.PddEntered) <1440 THEN 1 
							ELSE 0 
						 END
	,CreateEddDaysDuration = DATEDIFF(dd,ExpectedLOS.AdmissionTime, Pdd.PddEntered)
FROM
	(
		SELECT 
			*
		FROM
			Bedman.Pdd 
		WHERE 
			PddSequence = 1
	)Pdd

WHERE  
		APC.ExpectedLOS.SourcePatientNo = Pdd.SourcePatientNo
	AND APC.ExpectedLOS.SourceSpellNo = Pdd.SourceSpellNo
	AND APC.ExpectedLOS.ExpectedLOS IS NULL

END


﻿
CREATE proc [ETL].[ExtractPatientrackObservationPatientFlag]

	@FromDate datetime = null
	,@ToDate datetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select @RowsInserted = 0


insert ETL.TImportObservationPatientFlag
(
SourceUniqueID
,CasenoteNumber 
,DateOfBirth 
,AdmissionSourceUniqueID
,PatientFlagID
,StartTime
,EndTime
,Comment
,CreatedByUserID
,CreatedTime
,LastModifiedByUserID
,LastModifiedTime
,EWSDisabled
)

select
	SourceUniqueID 
	,CasenoteNumber 
	,DateOfBirth 
	,AdmissionSourceUniqueID 
	,PatientFlagID 
	,StartTime 
	,EndTime 
	,Comment
	,CreatedByUserID 
	,CreatedTime 
	,LastModifiedByUserID 
	,LastModifiedTime 
	,EWSDisabled 
from
	(
	select --top 1000 
		SourceUniqueID = ADFLG_PK
		,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
		,DateOfBirth = Admission.BIRTH_DATE
		,AdmissionSourceUniqueID = AdmissionFlag.ADMSN_PK
		,PatientFlagID = PFLAG_RFVAL 
		,StartTime = START_DTTM 
		,EndTime = END_DTTM 
		,Comment = nullif(cast(AdmissionFlag.NOTE as varchar(max)), '')
		,CreatedByUserID = AdmissionFlag.CREATED_BY_USERR_PK 
		,CreatedTime = AdmissionFlag.CREATED_DTTM
		,LastModifiedByUserID = AdmissionFlag.LAST_MODIFIED_BY_USERR_PK 
		,LastModifiedTime = AdmissionFlag.LAST_MODIFIED_DTTM
		,EWSDisabled = NCEWS_FLAG
	from
		[$(PatientrackSS)].dbo.ADMISSION_FLAG AdmissionFlag

	inner join [$(PatientrackSS)].dbo.ADMISSION Admission
	on	Admission.ADMSN_PK = AdmissionFlag.ADMSN_PK
	and	coalesce(Admission.DELETED_FLAG, 0) = 0

	inner join [$(PatientrackSS)].dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
	on    PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
	and   PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
	and   coalesce(PatientIdentifier.DELETED_FLAG, 0) = 0 

	where
		AdmissionFlag.LAST_MODIFIED_DTTM between @FromDate and @ToDate

	) PatientFlag




select
	@RowsInserted = @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
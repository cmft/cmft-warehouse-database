﻿

CREATE PROCEDURE [ETL].[LoadOCMCodedResult]

(
	@Update bit
)

as


set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, ResultDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, ResultDate)) 
from
	ETL.TLoadOCMCodedResult

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	OCM.CodedResult target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,SourceEpisodeNo
		,SequenceNo
		,TransactionNo
		,SessionNo
		,ResultCode
		,Result
		,ResultDate
		,ResultTime
		,RangeIndicator
		,EnteredBy
		,StatusCode
		,Comment1
		,Comment2
		,Comment3
		,Comment4
		,Comment5
		,Comment6
	from
		ETL.TLoadOCMCodedResult
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ResultDate between @LoadStartDate and @LoadEndDate
	and @Update = 0

	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourcePatientNo
			,SourceEpisodeNo
			,SequenceNo
			,TransactionNo
			,SessionNo
			,ResultCode
			,Result
			,ResultDate
			,ResultTime
			,RangeIndicator
			,EnteredBy
			,StatusCode
			,Comment1
			,Comment2
			,Comment3
			,Comment4
			,Comment5
			,Comment6
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceEpisodeNo
			,source.SequenceNo
			,source.TransactionNo
			,source.SessionNo
			,source.ResultCode
			,source.Result
			,source.ResultDate
			,source.ResultTime
			,source.RangeIndicator
			,source.EnteredBy
			,source.StatusCode
			,source.Comment1
			,source.Comment2
			,source.Comment3
			,source.Comment4
			,source.Comment5
			,source.Comment6
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
		and isnull(target.SourceEpisodeNo, 0) = isnull(source.SourceEpisodeNo, 0)
		and isnull(target.SequenceNo, 0) = isnull(source.SequenceNo, 0)
		and isnull(target.TransactionNo, 0) = isnull(source.TransactionNo, 0)
		and isnull(target.SessionNo, 0) = isnull(source.SessionNo, 0)
		and isnull(target.ResultCode, '') = isnull(source.ResultCode, '')
		and isnull(target.Result, '') = isnull(source.Result, '')
		and isnull(target.ResultDate, getdate()) = isnull(source.ResultDate, getdate())
		and isnull(target.ResultTime, getdate()) = isnull(source.ResultTime, getdate())
		and isnull(target.RangeIndicator, 0) = isnull(source.RangeIndicator, 0)
		and isnull(target.EnteredBy, '') = isnull(source.EnteredBy, '')
		and isnull(target.StatusCode, 0) = isnull(source.StatusCode, 0)
		and isnull(target.Comment1, '') = isnull(source.Comment1, '')
		and isnull(target.Comment2, '') = isnull(source.Comment2, '')
		and isnull(target.Comment3, '') = isnull(source.Comment3, '')
		and isnull(target.Comment4, '') = isnull(source.Comment4, '')
		and isnull(target.Comment5, '') = isnull(source.Comment5, '')
		and isnull(target.Comment6, '') = isnull(source.Comment6, '')
		)
	then
		update
		set
			target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEpisodeNo = source.SourceEpisodeNo
			,target.SequenceNo = source.SequenceNo
			,target.TransactionNo = source.TransactionNo
			,target.SessionNo = source.SessionNo
			,target.ResultCode = source.ResultCode
			,target.Result = source.Result
			,target.ResultDate = source.ResultDate
			,target.ResultTime = source.ResultTime
			,target.RangeIndicator = source.RangeIndicator
			,target.EnteredBy = source.EnteredBy
			,target.StatusCode = source.StatusCode
			,target.Comment1 = source.Comment1
			,target.Comment2 = source.Comment2
			,target.Comment3 = source.Comment3
			,target.Comment4 = source.Comment4
			,target.Comment5 = source.Comment5
			,target.Comment6 = source.Comment6
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

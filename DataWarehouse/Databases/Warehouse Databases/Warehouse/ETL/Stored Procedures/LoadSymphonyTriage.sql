﻿
CREATE PROCEDURE [ETL].[LoadSymphonyTriage]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Triage table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 TriageID
			,AttendanceID
			,RequestID
			,DepartmentID
			,TriageTime
			,Complaint
			,Discriminator
			,CategoryID
			,IsFasttrack
			,FasttrackNoteID
			,Painscore
			,CommentsID
			,Treatment
			,NurseID
			,IncludingPatientLetter
			,CreatedByID
			,CreatedTime
			,UpdatedByID
			,UpdatedTime
			,CautionID
			,RepeatID
			)
into
	#TLoadSymphonyTriage
from
	(
	select
		 TriageID = cast(TriageID as int)
		,AttendanceID = cast(AttendanceID as int)
		,RequestID = cast(RequestID as int)
		,DepartmentID = cast(DepartmentID as smallint)
		,TriageTime = cast(TriageTime as datetime)
		,Complaint = cast(Complaint as varchar(255))
		,Discriminator = cast(Discriminator as varchar(80))
		,CategoryID = cast(CategoryID as tinyint)
		,IsFasttrack = cast(IsFasttrack as bit)
		,FasttrackNoteID = cast(FasttrackNoteID as int)
		,Painscore = cast(Painscore as tinyint)
		,CommentsID = cast(CommentsID as int)
		,Treatment = cast(Treatment as varchar(150))
		,NurseID = cast(NurseID as int)
		,IncludingPatientLetter = cast(IncludingPatientLetter as bit)
		,CreatedByID = cast(CreatedByID as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,UpdatedByID = cast(UpdatedByID as int)
		,UpdatedTime = cast(UpdatedTime as datetime)
		,CautionID = cast(CautionID as int)
		,RepeatID = cast(RepeatID as int)
	from
		(
		select
			 TriageID = Triage.tri_trid
			,AttendanceID = Triage.tri_atdid
			,RequestID = Triage.tri_reqid
			,DepartmentID = Triage.tri_depid
			,TriageTime = Triage.tri_date
			,Complaint = Triage.tri_complaint
			,Discriminator = Triage.tri_discriminator
			,CategoryID = Triage.tri_category
			,IsFasttrack = Triage.tri_fasttrack
			,FasttrackNoteID = Triage.tri_fasttracknote
			,Painscore = Triage.tri_painscore
			,CommentsID = Triage.tri_comments
			,Treatment = Triage.tri_treatment
			,NurseID = Triage.tri_nurse
			,IncludingPatientLetter = Triage.tri_includeingpletter
			,CreatedByID = Triage.tri_createdby
			,CreatedTime = Triage.tri_created
			,UpdatedByID = Triage.tri_updatedby
			,UpdatedTime = Triage.tri_update
			,CautionID = Triage.tri_caution
			,RepeatID = Triage.tri_repeat
		from
			[$(Symphony)].dbo.Triage
		where
			Triage.tri_inactive <> 1

		) Encounter
	) Encounter
order by
	Encounter.TriageID


create unique clustered index #IX_TLoadSymphonyTriage on #TLoadSymphonyTriage
	(
	TriageID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Triage target
using
	(
	select
		 TriageID
		,AttendanceID
		,RequestID
		,DepartmentID
		,TriageTime
		,Complaint
		,Discriminator
		,CategoryID
		,IsFasttrack
		,FasttrackNoteID
		,Painscore
		,CommentsID
		,Treatment
		,NurseID
		,IncludingPatientLetter
		,CreatedByID
		,CreatedTime
		,UpdatedByID
		,UpdatedTime
		,CautionID
		,RepeatID
		,EncounterChecksum
	from
		#TLoadSymphonyTriage
	
	) source
	on	source.TriageID = target.TriageID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 TriageID
			,AttendanceID
			,RequestID
			,DepartmentID
			,TriageTime
			,Complaint
			,Discriminator
			,CategoryID
			,IsFasttrack
			,FasttrackNoteID
			,Painscore
			,CommentsID
			,Treatment
			,NurseID
			,IncludingPatientLetter
			,CreatedByID
			,CreatedTime
			,UpdatedByID
			,UpdatedTime
			,CautionID
			,RepeatID

			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.TriageID
			,source.AttendanceID
			,source.RequestID
			,source.DepartmentID
			,source.TriageTime
			,source.Complaint
			,source.Discriminator
			,source.CategoryID
			,source.IsFasttrack
			,source.FasttrackNoteID
			,source.Painscore
			,source.CommentsID
			,source.Treatment
			,source.NurseID
			,source.IncludingPatientLetter
			,source.CreatedByID
			,source.CreatedTime
			,source.UpdatedByID
			,source.UpdatedTime
			,source.CautionID
			,source.RepeatID
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.TriageID = source.TriageID
			,target.AttendanceID = source.AttendanceID
			,target.RequestID = source.RequestID
			,target.DepartmentID = source.DepartmentID
			,target.TriageTime = source.TriageTime
			,target.Complaint = source.Complaint
			,target.Discriminator = source.Discriminator
			,target.CategoryID = source.CategoryID
			,target.IsFasttrack = source.IsFasttrack
			,target.FasttrackNoteID = source.FasttrackNoteID
			,target.Painscore = source.Painscore
			,target.CommentsID = source.CommentsID
			,target.Treatment = source.Treatment
			,target.NurseID = source.NurseID
			,target.IncludingPatientLetter = source.IncludingPatientLetter
			,target.CreatedByID = source.CreatedByID
			,target.CreatedTime = source.CreatedTime
			,target.UpdatedByID = source.UpdatedByID
			,target.UpdatedTime = source.UpdatedTime
			,target.CautionID = source.CautionID
			,target.RepeatID = source.RepeatID
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
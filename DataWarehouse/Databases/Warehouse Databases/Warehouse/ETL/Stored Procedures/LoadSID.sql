﻿

CREATE procedure ETL.LoadSID

as

exec [ETL].[ExtractSIDAcuteCareBundle];

exec [ETL].[ExtractSIDBeforeDischarge];

exec [ETL].[ExtractSIDCTScanAndAspirin];

exec [ETL].[ExtractSIDEncounter];

exec [ETL].[ExtractSIDInitialAssessment];

exec [ETL].[ExtractSIDLikelyStroke];

exec [ETL].[ExtractSIDMDT];

exec [ETL].[ExtractSIDMedicalReview];

exec [ETL].[ExtractSIDQualityIndicators];

exec [ETL].[ExtractSIDSuspectedTIA];


exec ETL.LoadSIDEncounter

﻿create procedure [ETL].[ExtractSIDBeforeDischarge]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat ymd

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()
select @RowsInserted = 0

truncate table ETL.TImportSIDBeforeDischarge;


WITH XMLNAMESPACES (N'http://www.cmft.nhs.uk/InfoPath.xsd' as DYN)



insert into ETL.TImportSIDBeforeDischarge
(
	 CandidateDataID
	,CandidateID
	,SectionID
	,ActionID
	,DidPatientDieInHospital
	,IsPallativeCareRequired
	,DateTimeIsPallativeCareRequired
	,EndOfLifePathway
	,ClinicalDiagnosis
	,DateOfDeath
	,BarthelScoresBeforeDischarge
	,BarthelScoresMDTFeeding
	,BarthelScoresMDTBathing
	,BarthelScoresMDTGrooming
	,BarthelScoresMDTDressing
	,BarthelScoresMDTBowels
	,BarthelScoresMDTBladder
	,BarthelScoresMDTToiletUse
	,BarthelScoresMDTTransfers
	,BarthelScoresMDTMobility
	,BarthelScoresMDTStairs
	,BarthelScoresMDTTotalScore
	,ModifiedRankinScoreOnDischarge
	,HealthAndSocialCarePlanProvided
	,DateTimeHealthAndSocialCarePlanProvided
	,WillThePatientRequireCarersAfterDischarge
	,WhatSupportDidTheyReceive
	,HowManyVisitsPerWeek
	,RehabilitationGoalsAgreedByDischarge
	,DateTimeRehabilitationGoalsAgreedByDischarge
	,ReferralMadeForEarlySupportedDischarge
	,DateTimeReferralMadeForEarlySupportedDischarge
	,ReferralMadeForHospitalAndCommunityTeam
	,DateTimeReferralMadeForHospitalAndCommunityTeam
	,DateDeemedMedicallySuitableForDischarge
	,DateMDTDeemedPatientNoLongerRequiresInpatientRehab
	,DateDischargedFromStrokeService
	,DischargeDestination
	,DischargeDestinationDetails
	,DischargeDestinationOther
	,IfDischargedHomeIsThePatient
	,ToBeFollowedUpAt6WeeksPostDischargeByDoctor
	,DateTimeToBeFollowedUpAt6WeeksPostDischargeByDoctor
	,TransferOfCareToolCompletedAtDischarge
	,MedicalSummary
	,IsPatientInAtrialFibrilationOnDischarge
	,IfPatientInAtrialFibrilationIsPatientOnAnticoaguationMedication
	,NursingSummary
	,TherapySummary
	,PhysiotherapySummary
	,OccupationalTherapySummary
	,SpeechAndLanguageSummary
	,PsychologySummary
	,DieticianSummary
	,DetailsOfCareAndSupportArranged
	,NameAndContactDetailsOfSocialWorker
	,ContactForCarePackage
	,OutOfHoursEmergencyTelephoneContactNumber
	,Username
	,ModifiedDate
)

select 
	 CandidateDataID = 
		CandidateData.ID
	,CandidateID = 
		CandidateData.CandidateID
	,SectionID = 
		SectionID
	,ActionID = 
		ActionID
	,DidPatientDieInHospital = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/DidPatientDieInHospital)[1]', 'varchar(max)'),'')
	,IsPallativeCareRequired = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/IsPallativeCareRequired)[1]', 'varchar(max)'),'')
	,DateTimeIsPallativeCareRequired = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateTimeIsPallativeCareRequired)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,EndOfLifePathway = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/EndOfLifePathway)[1]', 'varchar(max)'),'')
	,ClinicalDiagnosis = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/ClinicalDiagnosis)[1]', 'varchar(max)'),'')
	,DateOfDeath = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateOfDeath)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,BarthelScoresBeforeDischarge = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresBeforeDischarge)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTFeeding = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Feeding)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTBathing = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Bathing)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTGrooming = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Grooming)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTDressing = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Dressing)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTBowels = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Bowels)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTBladder = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Bladder)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTToiletUse = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/ToiletUse)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTTransfers = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Transfers)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTMobility = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Mobility)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTStairs = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/Stairs)[1]', 'varchar(max)'),'')
	,BarthelScoresMDTTotalScore = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/BarthelScoresMDT/TotalScore)[1]', 'varchar(max)'),'')
	,ModifiedRankinScoreOnDischarge = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/ModifiedRankinScoreOnDischarge)[1]', 'varchar(max)'),'')
	,HealthAndSocialCarePlanProvided = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/HealthAndSocialCarePlanProvided)[1]', 'varchar(max)'),'')
	,DateTimeHealthAndSocialCarePlanProvided = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateTimeHealthAndSocialCarePlanProvided)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,WillThePatientRequireCarersAfterDischarge = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/WillThePatientRequireCarersAfterDischarge)[1]', 'varchar(max)'),'')
	,WhatSupportDidTheyReceive = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/WhatSupportDidTheyReceive)[1]', 'varchar(max)'),'')
	,HowManyVisitsPerWeek = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/HowManyVisitsPerWeek)[1]', 'varchar(max)'),'')
	,RehabilitationGoalsAgreedByDischarge = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/RehabilitationGoalsAgreedByDischarge)[1]', 'varchar(max)'),'')
	,DateTimeRehabilitationGoalsAgreedByDischarge = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateTimeRehabilitationGoalsAgreedByDischarge)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ReferralMadeForEarlySupportedDischarge = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/ReferralMadeForEarlySupportedDischarge)[1]', 'varchar(max)'),'')
	,DateTimeReferralMadeForEarlySupportedDischarge = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateTimeReferralMadeForEarlySupportedDischarge)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,ReferralMadeForHospitalAndCommunityTeam = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/ReferralMadeForHospitalAndCommunityTeam)[1]', 'varchar(max)'),'')
	,DateTimeReferralMadeForHospitalAndCommunityTeam = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateTimeReferralMadeForHospitalAndCommunityTeam)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateDeemedMedicallySuitableForDischarge = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateDeemedMedicallySuitableForDischarge)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateMDTDeemedPatientNoLongerRequiresInpatientRehab = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateMDTDeemedPatientNoLongerRequiresInpatientRehab)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DateDischargedFromStrokeService = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateDischargedFromStrokeService)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,DischargeDestination = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/DischargeDestination)[1]', 'varchar(max)'),'')
	,DischargeDestinationDetails = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/DischargeDestinationDetails)[1]', 'varchar(max)'),'')
	,DischargeDestinationOther = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/DischargeDestinationOther)[1]', 'varchar(max)'),'')
	,IfDischargedHomeIsThePatient = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/IfDischargedHomeIsThePatient)[1]', 'varchar(max)'),'')
	,ToBeFollowedUpAt6WeeksPostDischargeByDoctor = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/ToBeFollowedUpAt6WeeksPostDischargeByDoctor)[1]', 'varchar(max)'),'')
	,DateTimeToBeFollowedUpAt6WeeksPostDischargeByDoctor = 
		nullif(convert(datetime,replace(CandidateData.Details.value('(/BeforeDischargeDTO/DateTimeToBeFollowedUpAt6WeeksPostDischargeByDoctor)[1]', 'varchar(max)'),'T',' ' )),' ' )
	,TransferOfCareToolCompletedAtDischarge = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/TransferOfCareToolCompletedAtDischarge)[1]', 'varchar(max)'),'')
	,MedicalSummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/MedicalSummary)[1]', 'varchar(max)'),'')
	,IsPatientInAtrialFibrilationOnDischarge = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/IsPatientInAtrialFibrilationOnDischarge)[1]', 'varchar(max)'),'')
	,IfPatientInAtrialFibrilationIsPatientOnAnticoaguationMedication = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/IfPatientInAtrialFibrilationIsPatientOnAnticoaguationMedication)[1]', 'varchar(max)'),'')
	,NursingSummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/NursingSummary)[1]', 'varchar(max)'),'')
	,TherapySummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/TherapySummary)[1]', 'varchar(max)'),'')
	,PhysiotherapySummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/PhysiotherapySummary)[1]', 'varchar(max)'),'')
	,OccupationalTherapySummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/OccupationalTherapySummary)[1]', 'varchar(max)'),'')
	,SpeechAndLanguageSummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/SpeechAndLanguageSummary)[1]', 'varchar(max)'),'')
	,PsychologySummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/PsychologySummary)[1]', 'varchar(max)'),'')
	,DieticianSummary = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/DieticianSummary)[1]', 'varchar(max)'),'')
	,DetailsOfCareAndSupportArranged = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/DetailsOfCareAndSupportArranged)[1]', 'varchar(max)'),'')
	,NameAndContactDetailsOfSocialWorker = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/NameAndContactDetailsOfSocialWorker)[1]', 'varchar(max)'),'')
	,ContactForCarePackage = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/ContactForCarePackage)[1]', 'varchar(max)'),'')
	,OutOfHoursEmergencyTelephoneContactNumber = 
		nullif(CandidateData.Details.value('(/BeforeDischargeDTO/OutOfHoursEmergencyTelephoneContactNumber)[1]', 'varchar(max)'),'')
	,Username = 
		CandidateData.Details.value('(/BeforeDischargeDTO/UserModified/Username)[1]', 'varchar(max)')
	,ModifiedDate = 
		convert(datetime,left(replace(CandidateData.Details.value('(/BeforeDischargeDTO/UserModified/ModifiedDate)[1]', 'varchar(max)'),'T',' ' ),19))

from 
	[$(SID)].dbo.TARNCandidateData CandidateData
where
	CandidateData.SectionID = 21
and CandidateData.ActionID = 99


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractSIDBeforeDischarge', @Stats, @StartTime





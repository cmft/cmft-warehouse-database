﻿
create proc ETL.ExtractDictationReferenceData

as

--declare @StartTime datetime
--declare @Elapsed int
--declare @Sproc varchar(255)

--select @StartTime = getdate();


/* SpeechMagicUser */ 

truncate table Dictation.SpeechMagicUser

insert into Dictation.SpeechMagicUser

select	
	UserCode = [userId] 
	,[User] = [userName]
	,AdminRights = [adminRights]
	,RecognitionRights = [recognitionRights]
	,ExcludeFromATR = [excludeFromATR]
	,UserPassword = [userPassword]
	,UserFullName = [userFullName]
	,Gender = [gender]
	,DataVersion = [dataVersion]
	,DeleteVersion = [deleteVersion]
	,HashValue = [hashValue]
	,CheckOutTimeStamp = [checkOutTimeStamp]
	,CheckOutTaskID = [checkOutTaskId]
from
	[$(G2_sm61)].[dbo].[SmUser]

/* Context */

truncate table Dictation.Context

insert into Dictation.Context

select 
	ContextCode = ID
	,Context = Name
from
	[$(G2_G2Speech5)].dbo.G2Context

union 

select 
	ContextCode = ID
	,Context = Name
from
	[$(G2_G2Archive5)].dbo.Archived_G2Context	

	
/* Document Type */	

truncate table Dictation.DocumentType
insert into Dictation.DocumentType

select 
	DocumentTypeCode = ID
	,DocumentType = Name
from
	[$(G2_G2Speech5)].dbo.G2DocumentType

union	
	
select 
	DocumentTypeCode = ID
	,DocumentType = Name
from
	[$(G2_G2Archive5)].dbo.Archived_G2DocumentType


/* Group */

truncate table Dictation.[Group]

insert into Dictation.[Group]

select
	GroupCode = ID
	,Name
	,SpecialtyCode = case
						when datalength(Name) - datalength(replace(Name, '-','')) = 2
						then left(Name, charindex('-',Name) - 1)
						else null
					end
	,Specialty = case
						--when ID = 56
						--then 'Vascular'
						when datalength(Name) - datalength(replace(Name, '-','')) = 2
						then substring(Name, charindex('-',Name) + 1, charindex('-',Name, charindex('-',Name) + 1) - charindex('-',Name) - 1)
						else null
					end
	,Division = case
						--when ID = 56
						--then 'Surgery'
						when datalength(Name) - datalength(replace(Name, '-','')) = 2
						then substring(Name, charindex('-',Name, charindex('-',Name) + 1) + 1, len(Name))
						else null
					end
	
from
	[$(G2_G2Speech5)].dbo.G2Group

union

select
	GroupCode = ID
	,Name
	,SpecialtyCode = case
						when datalength(Name) - datalength(replace(Name, '-','')) = 2
						then left(Name, charindex('-',Name) - 1)
						else null
					end
	,Specialty = case
						--when ID = 56
						--then 'Vascular'
						when datalength(Name) - datalength(replace(Name, '-','')) = 2
						then substring(Name, charindex('-',Name) + 1, charindex('-',Name, charindex('-',Name) + 1) - charindex('-',Name) - 1)
						else null
					end
	,Division = case
						--when ID = 56
						--then 'Surgery'
						when datalength(Name) - datalength(replace(Name, '-','')) = 2
						then substring(Name, charindex('-',Name, charindex('-',Name) + 1) + 1, len(Name))
						else null
					end
					
from
	[$(G2_G2Archive5)].[dbo].[Archived_G2Group]
	

/* Interface */

truncate table Dictation.Interface
insert into Dictation.Interface

select
	InterfaceCode = ID
	,Interface = Name
from
	[$(G2_G2Speech5)].dbo.G2Interface
	
union

select
	InterfaceCode = ID
	,Interface = Name
from
	[$(G2_G2Archive5)].[dbo].[Archived_G2Interface]
	
	
/* Status */

truncate table Dictation.Status
insert into Dictation.Status

select
	StatusCode = ID
	,Status = alias
	,DisplayIndex = DisplayIndex
from
	[$(G2_G2Speech5)].dbo.G2Status
	
	
/* User */	
	
	
truncate table Dictation.[User]
insert into Dictation.[User]
	
select
	UserCode = ID
	,[User] = FullName
	,HasVRLicence = 
				case
					when speechmagicuser.UserName is not null
					then 1
					else 0
				end
	
from
	[$(G2_G2Speech5)].dbo.G2User g2user
	left outer join 
					(
					select
						 UserName = userName
						,RecognitionRights = recognitionRights
					from
						[$(G2_sm61)].dbo.SmUser
					where
						recognitionRights = 1
					) speechmagicuser
					on g2user.SrIdentification = speechmagicuser.UserName
					and g2user.SrIdentification <> ''
	
	
	
union

select
	UserCode = ID
	,[User] = Name
	,VRLicence = 0

from
	[$(G2_G2Archive5)].dbo.Archived_G2User archive
	
where
	not exists
			(
			select
				1
			from
				[$(G2_G2Speech5)].dbo.G2User speech
			where
				speech.ID = archive.ID
			)
			
/* User Group */

truncate table Dictation.UserGroup
insert into Dictation.UserGroup
		
select 
	[UserCode] = UserID
	,[GroupCode] = GroupID
from
	[$(G2_G2Speech5)].dbo.G2UserGroup

union
	
select 
	[UserCode] = UserID
	,[GroupCode] = GroupID
from
	[$(G2_G2Archive5)].dbo.Archived_G2UserGroup

/* Work Type */

truncate table Dictation.Worktype
insert into Dictation.Worktype

select 
	WorkTypeCode = ID
	,WorkType = Name
from 
	[$(G2_G2Speech5)].dbo.G2Worktype
	
union

select 
	WorkTypeCode = ID
	,WorkType = Name
from 
	[$(G2_G2Archive5)].dbo.Archived_G2Worktype

/* Workstation */

truncate table Dictation.Workstation
insert into Dictation.Workstation


select
	WorkStationCode = ID
	,Workstation = Name
from
	[$(G2_G2Speech5)].dbo.G2Workstation
	
union

select
	WorkStationCode = ID
	,Workstation = Name
from
	[$(G2_G2Archive5)].dbo.Archived_G2Workstation
	

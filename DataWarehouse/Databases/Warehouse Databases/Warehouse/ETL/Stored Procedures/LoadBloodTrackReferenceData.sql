﻿-- use Warehouse

CREATE Procedure [ETL].[LoadBloodTrackReferenceData]
as


-- new attributes for TransactionType, Condition, etc.

-- Blood Track

Insert into BloodTrack.SiteBase
	(SiteID
	,SiteName
	)
select 
	SiteID
	,SiteName
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_Site HospitalSite
where
	not exists
		(select
			1
		from 
			BloodTrack.SiteBase Present
		where 
			Present.SiteID = HospitalSite.SiteID
		)
		
		
Insert into BloodTrack.LocationBase
	(LocationID
	,LocationName
	,Location
	)
select 
	LocationID
	,LocationName
	,[Description]
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_Location Location 
where
	not exists
		(select
			1
		from 
			BloodTrack.LocationBase Present
		where 
			Present.LocationID = Location.LocationID
		)

	
Insert into BloodTrack.SystemUserBase
	(UserID
	,UserName
	,UserDesignation
	,StaffNumber
	)
select 
	UserID
	,UserName
	,Designation
	,StaffNumber
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_User SystemUser
where
	not exists
		(select
			1
		from 
			BloodTrack.SystemUserBase Present
		where 
			Present.UserID = SystemUser.UserID
		)		


Insert into BloodTrack.TransactionTypeBase
	(TransactionTypeID
	,TransactionType
	)
select 
	TransactionTypeID
	,Description
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_TransactionType TransactionType
where
	not exists
		(select
			1
		from 
			BloodTrack.TransactionTypeBase Present
		where 
			Present.TransactionTypeID = TransactionType.TransactionTypeID
		)	
		


Insert into BloodTrack.ConditionBase
	(ConditionID
	,Condition
	)
select 
	ConditionID
	,Description
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_BloodUnitCondition ConditionTransaction
where
	not exists
		(select
			1
		from 
			BloodTrack.ConditionBase Present
		where 
			Present.ConditionID = ConditionTransaction.ConditionID
		)	
		
Insert into BloodTrack.BloodGroupBase
	(BloodGroupID
	,BloodGroupCode
	,BloodGroup
	,Alternate
	)
select 
	BloodGroupID
	,BloodGroupCode
	,Description
	,Alternate
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_BloodGroup BloodGroup
where
	not exists
		(select
			1
		from 
			BloodTrack.BloodGroupBase Present
		where 
			Present.BloodGroupID = BloodGroup.BloodGroupID
		)	


Insert into BloodTrack.BloodUnitFateBase
	(FateID
	,Fate
	)
select 
	BloodUnitFateID
	,Description
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_BloodUnitFate Fate
where
	not exists
		(select
			1
		from 
			BloodTrack.BloodUnitFateBase Present
		where 
			Present.FateID = Fate.BloodUnitFateID
		)


	
Insert into BloodTrack.ProductBase
	(
	BloodProductID
	,ProductCode
	,Product
	,Courier
	,SafeTx
	)
select 
	BloodProductID
	,ProductCode
	,[Description]
	,CourierDescription
	,SafeTxDescription
from
	[$(Bloodtrack_Neoteric)].dbo.tbl_BloodProduct Product
where
	not exists
		(select
			1
		from 
			BloodTrack.ProductBase Present
		where 
			Present.BloodProductID = Product.BloodProductID
		)

	
	

﻿
CREATE proc [ETL].[ExtractBadgerNeonatalBedday]

	 @FromDate smalldatetime = null
	,@ToDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

insert into ETL.TImportNeonatalBedday
(
	EpisodeID 
	,CareDate
	,CareLocationID
	,WardCode
	--,LocalLocation -- no data
	,Weight
	,WeightToday
	,HeadCircumference
	,Length
	,OneToOneNursing
	,CarerStatusCode 
	,NursingStatusCode 
	,ObservationsMonitoringCode 
	,SurgeryToday
	,NeonatalLevelOfCare2001Code 
	,NeonatalLevelOfCare2011Code  
	,HRGCode
	,GlasgowScore
	,RespiratorySupportCode 
	,OxygenCode
	,VentilationModeCode 
	,CPAPModeCode 
	,NPAirway
	,NitricOxide
	,ECMO
	,Apnoea
	,Pneumothorax
	,ChestDrain
	,TracheostomyCode 
	,TracheostomyCarerCode 
	,ReplogleTube
	,Surfactant
	,SurfactantDoses
	,SurfactantDoseTotal
	,NecrotizingEntercolitisTreatmentCode
	,InotropesToday
	,ProstinToday
	,PulmonaryVasodilator
	,GastroschisisSilo
	,Stoma
	,RectalWashout
	,Peritonealdialysis
	,Haemofiltration
	,TAT
	,Tone
	,ConsciousnessCode 
	,Convulsions
	,NASTreatment
	,NASDrugTreatment
	,NASScore
	,VPShuntSurgery
	,EEG
	,Cooling
	,EpiduralCatheter
	,VentricularDrain
	,VentricularTap
	,ROPScreen
	,ROPGrade
	,ROPSurgery
	,MaxBilirubin
	,Phototherapy
	,Immunoglbulins
	,UrinaryCtaheter
	,TPN
	,GlucoseElectrolyte
	,EnteralFeedCode 
	,VolumeMilk
	,FormulaCode
	,FeedingMethodCode
	,AdditiveCode
	,LinesInSitu
	,ExchangeFull
	,ExchangePartial
	,BloodProductCode 
	,DiagnosesDay
	,DrugsDay
	,IVDrugsToday
	,SkinToSkinToday
	,PDAToday
	,TransportedToday
	,CVSTreatmentToday
	,InfectionDiagnosis
	,JaundiceDiagnosed
	,Bilirubin
	,HIEScore
	,Probiotics
	,MRSASwab
	,ModifiedTime 
)

select
	EpisodeID
	,CareDate 
	,CareLocationID
	,WardCode
	--,LocalLocation -- no data
	,Weight
	,WeightToday
	,HeadCircumference
	,Length
	,OneToOneNursing
	,CarerStatusCode
	,NursingStatusCode
	,ObservationsMonitoringCode
	,SurgeryToday
	,NeonatalLevelOfCare2001Code
	,NeonatalLevelOfCare2011Code  
	,HRGCode
	,GlasgowScore
	,RespiratorySupportCode
	,OxygenCode 
	,VentilationModeCode
	,CPAPModeCode
	,NPAirway
	,NitricOxide
	,ECMO
	,Apnoea
	,Pneumothorax
	,ChestDrain
	,TracheostomyCode
	,TracheostomyCarerCode
	,ReplogleTube
	,Surfactant
	,SurfactantDoses
	,SurfactantDoseTotal
	,NecrotizingEntercolitisTreatmentCode
	,InotropesToday
	,ProstinToday
	,PulmonaryVasodilator
	,GastroschisisSilo
	,Stoma
	,RectalWashout
	,Peritonealdialysis
	,Haemofiltration
	,TAT
	,Tone
	,ConsciousnessCode
	,Convulsions
	,NASTreatment
	,NASDrugTreatment
	,NASScore
	,VPShuntSurgery
	,EEG
	,Cooling
	,EpiduralCatheter
	,VentricularDrain
	,VentricularTap
	,ROPScreen
	,ROPGrade
	,ROPSurgery
	,MaxBilirubin
	,Phototherapy
	,Immunoglbulins
	,UrinaryCtaheter
	,TPN
	,GlucoseElectrolyte
	,EnteralFeedCode
	,VolumeMilk
	,FormulaCode
	,FeedingMethodCode
	,AdditiveCode
	,LinesInSitu
	,ExchangeFull
	,ExchangePartial
	,BloodProductCode
	,DiagnosesDay
	,DrugsDay
	,IVDrugsToday
	,SkinToSkinToday
	,PDAToday
	,TransportedToday
	,CVSTreatmentToday
	,InfectionDiagnosis
	,JaundiceDiagnosed
	,Bilirubin
	,HIEScore
	,Probiotics
	,MRSASwab
	,ModifiedTime 

from
	(
	select
		EpisodeID = EntityID
		,CareDate = cast(CareDate as date)
		,CareLocationID
		,WardCode = WardLocation
		--,LocalLocation -- no data
		,Weight
		,WeightToday
		,HeadCircumference
		,Length
		,OneToOneNursing
		,CarerStatusCode = CarerStatus
		,NursingStatusCode = NursingStatus
		,ObservationsMonitoringCode = ObservationsMonitoring
		,SurgeryToday
		,NeonatalLevelOfCare2001Code = bapm2001
		,NeonatalLevelOfCare2011Code  = bapm2011
		,HRGCode = HRG
		,GlasgowScore
		,RespiratorySupportCode = RespSupport
		,OxygenCode = AddedO2
		,VentilationModeCode = VentilationMode
		,CPAPModeCode = CPAPMode
		,NPAirway
		,NitricOxide
		,ECMO
		,Apnoea
		,Pneumothorax
		,ChestDrain
		,TracheostomyCode = Tracheostomy
		,TracheostomyCarerCode = TracheostomyCarer
		,ReplogleTube
		,Surfactant
		,SurfactantDoses
		,SurfactantDoseTotal
		,NecrotizingEntercolitisTreatmentCode = NECTreatment
		,InotropesToday
		,ProstinToday
		,PulmonaryVasodilator
		,GastroschisisSilo
		,Stoma
		,RectalWashout
		,Peritonealdialysis
		,Haemofiltration
		,TAT
		,Tone
		,ConsciousnessCode = Consciousness
		,Convulsions
		,NASTreatment
		,NASDrugTreatment
		,NASScore
		,VPShuntSurgery
		,EEG
		,Cooling
		,EpiduralCatheter
		,VentricularDrain
		,VentricularTap
		,ROPScreen
		,ROPGrade
		,ROPSurgery
		,MaxBilirubin
		,Phototherapy
		,Immunoglbulins
		,UrinaryCtaheter
		,TPN
		,GlucoseElectrolyte
		,EnteralFeedCode = EnteralFeeds
		,VolumeMilk
		,FormulaCode = FormulaName
		,FeedingMethodCode = FeedingMethod
		,AdditiveCode = Additives
		,LinesInSitu
		,ExchangeFull
		,ExchangePartial
		,BloodProductCode = BloodProducts
		,DiagnosesDay
		,DrugsDay
		,IVDrugsToday
		,SkinToSkinToday
		,PDAToday
		,TransportedToday
		,CVSTreatmentToday
		,InfectionDiagnosis
		,JaundiceDiagnosed
		,Bilirubin
		,HIEScore
		,Probiotics
		,MRSASwab
		,ModifiedTime = RecordTimestamp
	from
		[$(Bnet_DSS_business)].bnf_dbsync.NNUDaySum

	where
		cast(RecordTimestamp as date) between @FromDate and @ToDate

	) NeontatalBedday


select
	@RowsInserted = @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

	
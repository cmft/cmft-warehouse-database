﻿CREATE procedure [ETL].[ExtractInquireOPDiagnosis]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112) + '0000'

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, -1, getdate()))), 0), 112) + '2400'


insert into ETL.TImportOPDiagnosis
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID
)
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,SequenceNo
	,DiagnosisCode
	,OPSourceUniqueID
from
	(
	SELECT 
		 OPDIAGNOSIS.OPDIAGNOSISID SourceUniqueID
		,OPDIAGNOSIS.EpisodeNumber SourceEncounterNo
		,OPDIAGNOSIS.InternalPatientNumber SourcePatientNo
		,OPDIAGNOSIS.OpEpisodeSecondaryDiagnosisSequenceNumber SequenceNo
		,OPDIAGNOSIS.Diagnosis DiagnosisCode
		,OPA.OPAID OPSourceUniqueID
	FROM
		[$(PAS)].Inquire.OPDIAGNOSIS

	INNER JOIN [$(PAS)].Inquire.OPA 
	ON	OPA.EpisodeNumber = OPDIAGNOSIS.EpisodeNumber
	AND	OPA.InternalPatientNumber = OPDIAGNOSIS.InternalPatientNumber

	WHERE
		OPA.PtApptStartDtimeInt between @from and @to
	) Diagnosis



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireOPDiagnosis', @Stats, @StartTime

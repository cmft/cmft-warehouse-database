﻿--Use Warehouse

-- 20140929	RR	Create Date
--				Pulls Cell Salvage data from the Recall system.  

CREATE Procedure [ETL].[ExtractRecallBloodManagementCellSalvage] --'20130101','20141130'
	@fromDate smalldatetime = null
	,@toDate smalldatetime = null
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

select @RowsInserted = 0

select @from = coalesce(@fromDate, cast(cast(dateadd(month, -3, getdate())as date) as datetime))

select @to = (coalesce(@toDate, cast(cast(dateadd(day, -1, getdate())as date)as datetime))+'23:59')


Truncate Table ETL.TImportBloodManagementCellSalvage
insert into ETL.TImportBloodManagementCellSalvage
	(SourceUniqueID
	,EpisodeKey
	,HospitalNumber
	,LastName
	,FirstName
	,DateOfBirth
	,GenderCode
	,SpecialtyCode
	,ConsultantCode
	,IntendedDestination
	,ActualDestination
	,DrugCode
	,Drug
	,Category
	,Dose
	,Units
	,StartTime
	,EndTime
	,Duration
	,SessionType
	,SessionLocationCode
	,InterfaceCode 
	)
select 
	SourceUniqueID = EpisodeFluidIn.PrimaryKey
	,EpisodeFluidIn.EpisodeKey
	,EpisodePatientDetails.HospitalNumber
	,EpisodePatientDetails.LastName
	,EpisodePatientDetails.FirstName
	,EpisodePatientDetails.DateOfBirth
	,GenderCode = 
		case 
			when left (EpisodePatientDetails.SexDescription,1)='F' then 2
			when left (EpisodePatientDetails.SexDescription,1)='M' then 1
			when left (EpisodePatientDetails.SexDescription,1)='U' then 9
			else 0
		end
	,EpisodeSpecialty.SpecialtyKey
	,EpisodeAdmission.ConsultantKey
	,IntendedDestination = EpisodeAdmission.IntendedDestLocationDesc
	,ActualDestination = EpisodeAdmission.ActualDestLocationDesc
	,EpisodeFluidIn.DrugKey
	,EpisodeFluidIn.DrugDescription
	,Category = EpisodeFluidIn.CategoryDescription
	,EpisodeFluidIn.Dose
	,EpisodeFluidIn.UnitsDescription
	,StartTime = EpisodeFluidIn.StartTime
	,EndTime = 
		case
			when cast(EpisodeFluidIn.EndTime as date) = '18991230' then null
			else EpisodeFluidIn.EndTime
		end
	,Duration = 
		case
			when cast(EpisodeFluidIn.EndTime as date) = '18991230' then null
			else datediff(mi,EpisodeFluidIn.StartTime,EpisodeFluidIn.EndTime)
		end
	,Session.SessionTypeDescription
	,Session.LocationKey
	,InterfaceCode = 'RECALL' 
from
	[$(RecallReports)].dbo.EpisodeFluidIn

inner join [$(RecallReports)].dbo.EpisodePatientDetails
on EpisodeFluidIn.EpisodeKey = EpisodePatientDetails.EpisodeKey

left join [$(RecallReports)].dbo.EpisodeAdmission
on EpisodeAdmission.EpisodeKey = EpisodeFluidIn.EpisodeKey

left join [$(RecallReports)].dbo.EpisodeSpecialty 
on EpisodeFluidIn.EpisodeKey = EpisodeSpecialty.EpisodeKey
and SpecialtyKey <> 0 
and not exists
	(select
		1
	from 
		[$(RecallReports)].dbo.EpisodeSpecialty Latest
	where 
		Latest.EpisodeKey = EpisodeSpecialty.EpisodeKey
	and SpecialtyKey <> 0 
	and Latest.PrimaryKey > EpisodeSpecialty.PrimaryKey
	)

left join [$(RecallReports)].dbo.EpisodeSchedule
on EpisodeFluidIn.EpisodeKey = EpisodeSchedule.EpisodeKey
and not exists
	(Select
		1
	from 
		[$(RecallReports)].dbo.EpisodeSchedule Latest
	where
		Latest.EpisodeKey = EpisodeSchedule.EpisodeKey
	and Latest.PrimaryKey > EpisodeSchedule.PrimaryKey
	)

left join [$(RecallReports)].dbo.SessionListLink
on EpisodeSchedule.ListKey = SessionListLink.ListKey

left join [$(RecallReports)].dbo.[Session]
on SessionListLink.SessionKey = [Session].SessionKey


where 
	cast(EpisodeFluidIn.StartTime as date) between @from and @to 
and EpisodeFluidIn.DrugKey = 2100006148 -- Cell Salvage Data
and not exists
	(select
		1
	from
		[$(RecallReports)].dbo.EpisodePatientDetails TestPatients
	where
		TestPatients.PrimaryKey = EpisodePatientDetails.PrimaryKey
	and left(TestPatients.LastName,4) = 'Test'
	and (
		left(FirstName,4) = 'Test'
		or
		FirstName is null
		)	
	)
--

-- 20141027 Would like to bring through Theatre but joining to EpisodeSchedule is creating some duplicates.  E-mailed Ian Gall and William McNicol to see if there is a specific criteria to use to pull through the correct location	
-- 20141029 set this up to bring max primary key for the episode, but still need Service to confirm this is appropriate

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractRecallCellSalvage', @Stats, @StartTime




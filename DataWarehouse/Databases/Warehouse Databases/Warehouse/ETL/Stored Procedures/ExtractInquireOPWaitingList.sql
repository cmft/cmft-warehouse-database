﻿CREATE    procedure [ETL].[ExtractInquireOPWaitingList] 
	@CensusDate smalldatetime
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)


select @StartTime = getdate()

select @RowsInserted = 0


insert into ETL.TImportOPWaitingList
(
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,NHSNumber
	,DistrictNo
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,ResCode
	,HomePhone
	,WorkPhone
	,ConsultantCode
	,SpecialtyCode
	,PriorityCode
	,CommentClinical
	,SiteCode
	,PurchaserCode
	,CancelledBy
	,CasenoteNumber
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode
	,ContractSerialNumber
	,PatientDeathIndicator
	,ScheduledCancelReasonCode
	,WLAppointmentTypeCode
	,WaitingListCode
	,EthnicOriginCode	
)
select
	 SourcePatientNo
	,SourceEncounterNo
	,@CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	,DateOfBirth
	,NHSNumber
	,DistrictNo
	,ReferralDate
	,BookedDate
	,BookedTime
	,AppointmentDate
	,AppointmentTypeCode
	,AppointmentStatusCode
	,AppointmentCategoryCode
	,QM08StartWaitDate
	,QM08EndWaitDate
	,AppointmentTime
	,ClinicCode
	,SourceOfReferralCode
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,ResCode
	,HomePhone
	,WorkPhone
	,ConsultantCode
	,SpecialtyCode
	,PriorityCode
	,CommentClinical
	,SiteCode
	,PurchaserCode
	,CancelledBy
	,CasenoteNumber
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,AppointmentID
	,SourceUniqueID
	,AdminCategoryCode
	,ContractSerialNo
	,PatientDeathIndicator
	,ScheduledCancelReasonCode
	,WLAppointmentTypeCode
	,WaitingListCode
	,EthnicOriginCode
from
	(
	SELECT
		 SourcePatientNo = R.InternalPatientNumber 
		,SourceEncounterNo = R.EpisodeNumber  
		,PatientSurname = P.Surname  
		,PatientForename = P.Forenames  
		,PatientTitle = P.Title  
		,SexCode = P.Sex  
		,DateOfBirth = P.PtDoB  
		,NHSNumber = P.NHSNumber 
		,DistrictNo = R.DistrictNumber  
		,R.ReferralDate
		,BookedDate = OPA.ApptBookedDate  
		,BookedTime = OPA.ApptBookedTime  
		,AppointmentDate = ApptDate  
		,AppointmentTypeCode = OPA.ApptType  
		,AppointmentStatusCode = OPA.ApptStatus  
		,AppointmentCategoryCode = OPA.ApptCategory  
		,R.QM08StartWaitDate
		,QM08EndWaitDate = R.QM08EndWtDate  
		,AppointmentTime = OPA.PtApptStartDtimeInt  
		,OPA.ClinicCode
		,SourceOfReferralCode = R.RefBy  
		,MaritalStatusCode = P.MaritalStatus 
		,ReligionCode = P.Religion 
		,Postcode = P.PtAddrPostCode  
		,PatientsAddress1 = P.PtAddrLine1 
		,PatientsAddress2 = P.PtAddrLine2 
		,PatientsAddress3 = P.PtAddrLine3 
		,ResCode = OPA.ResCode  
		,HomePhone = P.PtHomePhone 
		,WorkPhone = P.PtWorkPhone 
		,ConsultantCode = R.ConsCode  
		,SpecialtyCode = R.Specialty  
		,PriorityCode = R.PriorityType  
		,CommentClinical = OPA.ApptComment 
		,SiteCode = R.HospitalCode  
		,PurchaserCode = OPA.ApptPurchaser  
		,CancelledBy = OPA.CancelBy  
		,CasenoteNumber = R.CaseNoteNumber  
		,EpisodicGpCode = R.EpiGPCode  
		,EpisodicGpPracticeCode = R.EpiGPPracticeCode  

		,RTTPathwayID = left(PW.PathwayNumber , 25) 
		,RTTPathwayCondition = PW.PathwayCondition 
		,RTTStartDate = PWP.RttStartDate 
		,RTTEndDate = PWP.RttEndDate 
		,RTTSpecialtyCode = PW.RttSpeciality 
		,RTTCurrentProviderCode = PW.RttCurProv 
		,RTTCurrentStatusCode = PW.RttCurrentStatus 
		,RTTCurrentStatusDate = PWP.CurrentStatusDt 
		,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
		,RTTOverseasStatusFlag = PW.RttOsvStatus 
		,AppointmentID = OPA.OPAID 
		,SourceUniqueID = coalesce(OPA.OPAID, R.OPREFERRALID) 
		,AdminCategoryCode = R.Category 
		,ContractSerialNo = ALLOCATEDCONTRACT.ContractId 
		,PatientDeathIndicator = P.PtDeathIndInt
		,ScheduledCancelReasonCode = Attend.SchReasonForCancellationCodeAppointment
		,WLAppointmentTypeCode = OPWLENTRY.AppointmentType 
		,WaitingListCode = OPWLENTRY.TreatmentCode
		,EthnicOriginCode = P.EthnicType  
	FROM
		[$(PAS)].Inquire.OPREFERRAL R
		
	LEFT JOIN [$(PAS)].Inquire.OPA 
	ON	R.EpisodeNumber = OPA.EpisodeNumber
	AND	R.InternalPatientNumber = OPA.InternalPatientNumber
    AND R.ReferralDate = OPA.ReferralDate
	and	OPA.ClinicCode != 'CASREH'

	left JOIN [$(PAS)].Inquire.OPAPPOINTATTEND Attend 
	ON	OPA.InternalPatientNumber = Attend.InternalPatientNumber
	AND	OPA.EpisodeNumber = Attend.EpisodeNumber
	AND	OPA.PtApptStartDtimeInt = Attend.PtApptStartDtimeInt
	AND	OPA.ResCode = Attend.ResCode

	INNER JOIN [$(PAS)].Inquire.PATDATA P
	ON	R.InternalPatientNumber = P.InternalPatientNumber

	left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
	on	EPW.InternalPatientNumber = R.InternalPatientNumber
	and	EPW.EpisodeNumber = R.EpisodeNumber

	left join [$(PAS)].Inquire.RTTPTPATHWAY PW
	on	PW.InternalPatientNumber = EPW.InternalPatientNumber
	and	PW.PathwayNumber = EPW.PathwayNumber

	left join [$(PAS)].Inquire.RTTPERIOD PWP
	on	PWP.InternalPatientNumber = PW.InternalPatientNumber
	and	PWP.PathwayNumber = PW.PathwayNumber
	and	PWP.PPeriodId = '0'

	LEFT JOIN [$(PAS)].Inquire.ALLOCATEDCONTRACT 
	ON	ALLOCATEDCONTRACT.InternalPatientNumber = R.InternalPatientNumber
	AND	ALLOCATEDCONTRACT.EpisodeNumber = R.EpisodeNumber
	AND	ALLOCATEDCONTRACT.CmReverseDttime = '999999999999'
	
	left join [$(PAS)].Inquire.OPWLENTRY
	on	OPWLENTRY.InternalPatientNumber = R.InternalPatientNumber
	AND	OPWLENTRY.EpisodeNumber = R.EpisodeNumber
	and	not exists
		(
		select
			1
		from
			[$(PAS)].Inquire.OPWLENTRY Previous
		where
			Previous.InternalPatientNumber = R.InternalPatientNumber
		AND	Previous.EpisodeNumber = R.EpisodeNumber
		and	(
				Previous.DateOnList < OPWLENTRY.DateOnList
			or	(
					Previous.DateOnList = OPWLENTRY.DateOnList
				and	Previous.OPWLENTRYID < OPWLENTRY.OPWLENTRYID
				)
			)
		)
	WHERE
		R.QM08EndWtDate Is Null
	AND	R.DischargeDt Is Null
	--AND	R.OpRegDtimeInt 
	--	Between 
	--		datename(year, dateadd(year, -1, getdate())) || right(''0'' || datepart(month, dateadd(year, -1, getdate())), 2) || right(''0'' || datepart(day, dateadd(year, -1, getdate())), 2) || ''0000'' 
	--	and	datename(year, getdate()) || right(''0'' || datepart(month, getdate()), 2)   || right(''0'' || datepart(day, getdate()), 2) || ''0000''

	) WLOutpatient


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Census Date ' + CONVERT(varchar(11), @CensusDate) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireOPWaitingList', @Stats, @StartTime

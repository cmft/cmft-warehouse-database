﻿
-- USE Warehouse

-- select * from [ETL].[TLoadAPCDementia]

CREATE Procedure [ETL].[LoadAPCDementia] As

--Table APC.Dementia
-- 
-- Create view in WOMv2 for Traff data, view below
-- view in WOMv2 using APC.Dementia

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, RecordedDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, RecordedDate)) 
from
	ETL.TLoadAPCDementia


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.Dementia target
using
	(
	select
		ProviderSpellNo
		,SpellID 
		,SourceUniqueID 
		,SourcePatientNo 
		,SourceSpellNo 
		,AdmissionWardCode 
		,RecordedDate 
		,RecordedTime
		,KnownToHaveDementia	
		,ForgetfulnessQuestionAnswered	
		,AssessmentScore	
		,Investigation	
		,AssessmentNotPerformedReason	
		,ReferralSentDate	
		,InterfaceCode
	from
		ETL.TLoadAPCDementia
	) source

	on source.SourceUniqueID = target.SourceUniqueID
	and source.RecordedTime = target.RecordedTime
	
	when not matched by source
	and	target.RecordedDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			ProviderSpellNo
			,SpellID 
			,SourceUniqueID 
			,SourcePatientNo 
			,SourceSpellNo 
			,AdmissionWardCode 
			,RecordedDate 
			,RecordedTime
			,KnownToHaveDementia	
			,ForgetfulnessQuestionAnswered	
			,AssessmentScore	
			,Investigation	
			,AssessmentNotPerformedReason	
			,ReferralSentDate	
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.ProviderSpellNo
			,source.SpellID 
			,source.SourceUniqueID 
			,source.SourcePatientNo 
			,source.SourceSpellNo 
			,source.AdmissionWardCode 
			,source.RecordedDate 
			,source.RecordedTime
			,source.KnownToHaveDementia	
			,source.ForgetfulnessQuestionAnswered	
			,source.AssessmentScore	
			,source.Investigation	
			,source.AssessmentNotPerformedReason	
			,source.ReferralSentDate	
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
		    isnull(target.ProviderSpellNo, '') = isnull(source.ProviderSpellNo, '')
		and isnull(target.SpellID, 0) = isnull(source.SpellID, 0)
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.AdmissionWardCode, '') = isnull(source.AdmissionWardCode, '')
		and isnull(target.RecordedDate, getdate()) = isnull(source.RecordedDate, getdate())
		and isnull(target.RecordedTime, getdate()) = isnull(source.RecordedTime, getdate())
		and isnull(target.KnownToHaveDementia, 0) = isnull(source.KnownToHaveDementia, 0)
		and isnull(target.ForgetfulnessQuestionAnswered, 0) = isnull(source.ForgetfulnessQuestionAnswered, 0)
		and isnull(target.AssessmentScore, '') = isnull(source.AssessmentScore, '')
		and isnull(target.Investigation, 0) = isnull(source.Investigation,0)
		and isnull(target.AssessmentNotPerformedReason, '') = isnull(source.AssessmentNotPerformedReason, '')
		and isnull(target.ReferralSentDate, getdate()) = isnull(source.ReferralSentDate, getdate())

		)
	then
		update
		set
			 target.ProviderSpellNo = source.ProviderSpellNo
			,target.SpellID = source.SpellID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.AdmissionWardCode = source.AdmissionWardCode
			,target.RecordedDate = source.RecordedDate
			,target.RecordedTime = source.RecordedTime
			,target.KnownToHaveDementia = source.KnownToHaveDementia
			,target.ForgetfulnessQuestionAnswered = source.ForgetfulnessQuestionAnswered
			,target.AssessmentScore = source.AssessmentScore
			,target.Investigation = source.Investigation
			,target.AssessmentNotPerformedReason = source.AssessmentNotPerformedReason
			,target.ReferralSentDate = source.ReferralSentDate
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


﻿










CREATE proc [ETL].[ExtractCHAMPEncounter] 

	-- @FromDate	datetime = null
	--,@ToDate	datetime = null

as


/*
When		Who			What
========================================================
24/12/2014	Paul Egan	Initial Coding
20/02/2015	Paul Egan	Added YearGroup
24/02/2015	Paul Egan	Changed to PostalExtractsAll (instead of just 2014/15)
03/03/2015	Paul Egan	Added IsAccountRegistered
*/

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

set @StartTime = getdate()
set @RowsInserted = 0


insert into ETL.TImportCHAMPEncounter
	(
	 CRMChildID
	,CRMAssessmentID
	,SchoolCode
	,YearGroup
	,SchoolYear
	,AssessmentTime
	,MckessonID
	,NHSNumber
	,Forename
	,Surname
	,DateOfBirth
	,SexCode
	,OptedOut
	,IsVisited
	,IsMeasured
	,IsMailed
	,IsAccountRegistered
	,Centile91
	,Centile98
	,Centile99
	,Height
	,[Weight]
	,BMI
	,Classification
	,ClassificationOrder
	,idx
	,AgeMonths
	,AgeInMonthsAtMeasurementDate
	,ActivityCode
	,ReasonOtherDescription
	,AlternativeCode
	,CodeDescription
	,LastUpdated
	,CreatedByUserID
	,Created
	)
select
	 ChildBase.CRMChildID
	,ChildBase.CRMAssessmentID 
	,SchoolCode = ChildBase.SchoolCode
	,YearGroup = ChildBase.YearGroup
	,SchoolYear = ChildBase.YearGroupSchoolYear
	,AssessmentTime = cast(ChildBase.AssessmentDate as datetime2(0))
	,McKessonID = ChildBase.MckessonID
	,NHSNumber = ChildBase.NHSNumber
	,ChildBase.Forename
	,ChildBase.Surname
	,DateOfBirth = cast(ChildBase.DateOfBirth as datetime2(0))
	,ChildBase.SexCode
	,ChildBase.OptedOut
	,IsVisited = 1	-- Need to confirm that CHAMP.NCMPExport.ChildBase does equate to children visited?
	,IsMeasured = null --ReportBase.IsMeasured
	,IsMailed = 
		case 
		when PostalExtracts.DateTimeOfExtract is not null then 1 else 0
		end
	,IsAccountRegistered =
		case
			when WebAccount.CRMChildID is not null then 1 else 0
		end
	,Centile91 = str(ChildCentiles.Centile91, 11, 8)
	,Centile98 = str(ChildCentiles.Centile98, 11, 8)
	,Centile99 = str(ChildCentiles.Centile99, 11, 8)
	,Height = ChildBase.Height
	,[Weight] = ChildBase.[Weight]
	,ChildCentiles.BMI
	,Classification = 
		case 
			when BMI < Centile91 then '< 91st Centile'
			when BMI between Centile91 and Centile98 Then '91st - 98th Centile'
			when BMI between Centile98 and Centile99 Then '98th - 99.6th Centile'
			when BMI >= Centile99 Then '> 99.6th Centile'
		end
	,ChildCentiles.ClassificationOrder
	,ChildCentiles.idx
	,ChildCentiles.AgeMonths
	,AgeInMonthsAtMeasurementDate = ChildCentiles.ageInMonthsAtMeasurementDate
	,ActivityCode = ChildCentiles.[Activity Code]
	,ChildCentiles.ReasonOtherDescription
	,ChildCentiles.alternativeCode
	,ChildCentiles.codeDescription
	,LastUpdated = cast(ChildCentiles.LastUpdated as datetime2(0))
	,ChildCentiles.CreatedByUserID
	,Created = getdate()
from
	[$(CHAMP)].NCMPExport.ChildBaseAll ChildBase
	
	left join [$(CHAMP)].ReportBase.PostalExtractsAll PostalExtracts
	on PostalExtracts.McKessonID = ChildBase.MckessonID
	and PostalExtracts.AssessmentDate = ChildBase.AssessmentDate
	
	--left join [$(CHAMP)].NCMPExport.ReportBase	-- Creates 4 duplicates
	--on	ReportBase.CRMChildID = ChildBase.CRMChildID
	--and ReportBase.AssessmentDate = ChildBase.AssessmentDate
		
	left join [$(CHAMP)].ReportBase.ChildCentilesAll ChildCentiles
	on	ChildCentiles.CRMChildID = ChildBase.CRMChildID
	and ChildCentiles.AssessmentDate = ChildBase.AssessmentDate
	
	left join [$(CHAMP)].ReportBase.ChildrenIDWithParentWebAccountsAll WebAccount
	on	WebAccount.CRMChildID = ChildBase.CRMChildID
	
	-- Need to add ethnicity
;


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime	










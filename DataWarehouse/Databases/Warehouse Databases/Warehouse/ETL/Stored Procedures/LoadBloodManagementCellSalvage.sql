﻿

CREATE PROCEDURE [ETL].[LoadBloodManagementCellSalvage]

as


set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartTime datetime
	,@LoadEndTime datetime

select
	 @LoadStartTime = MIN(convert(datetime,cast(StartTime as date)))
	,@LoadEndTime = MAX(convert(datetime,cast (StartTime as date))+'23:59')
from
	ETL.TLoadBloodManagementCellSalvage

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	BloodManagement.CellSalvage target
using
	(
	select
		SourceUniqueID
		,EpisodeKey
		,HospitalNumber
		,LastName
		,FirstName
		,DateOfBirth
		,GenderCode 
		,SpecialtyCode
		,ConsultantCode
		,IntendedDestination
		,ActualDestination
		,DrugCode
		,Drug
		,Category
		,Dose
		,Units
		,StartTime
		,EndTime
		,Duration
		,SessionType
		,SessionLocationCode
		,InterfaceCode
	from
		ETL.TLoadBloodManagementCellSalvage
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	when not matched by source
	and	target.StartTime between @LoadStartTime and @LoadEndTime

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,EpisodeKey
			,HospitalNumber
			,LastName
			,FirstName
			,DateOfBirth
			,GenderCode 
			,SpecialtyCode
			,ConsultantCode
			,IntendedDestination
			,ActualDestination
			,DrugCode
			,Drug
			,Category
			,Dose
			,Units
			,StartTime
			,EndTime
			,Duration
			,SessionType
			,SessionLocationCode
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.EpisodeKey
			,source.HospitalNumber
			,source.LastName
			,source.FirstName
			,source.DateOfBirth
			,source.GenderCode 
			,source.SpecialtyCode
			,source.ConsultantCode
			,source.IntendedDestination
			,source.ActualDestination
			,source.DrugCode
			,source.Drug
			,source.Category
			,source.Dose
			,source.Units
			,source.StartTime
			,source.EndTime
			,source.Duration
			,source.SessionType
			,source.SessionLocationCode
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.EpisodeKey, '') = isnull(source.EpisodeKey, '')
		and isnull(target.HospitalNumber, '')  = isnull(source.HospitalNumber, '')  
		and isnull(target.LastName, '')  = isnull(source.LastName, '')  
		and isnull(target.FirstName, '') = isnull(source.FirstName, '')  
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.GenderCode, '') = isnull(source.GenderCode, '')
		and isnull(target.SpecialtyCode, 0)  = isnull(source.SpecialtyCode,0) 
		and isnull(target.ConsultantCode, 0)  = isnull(source.ConsultantCode,0) 
		and isnull(target.IntendedDestination, '')  = isnull(source.IntendedDestination, '')  
		and isnull(target.ActualDestination, '') = isnull(source.ActualDestination, '')  
		and isnull(target.DrugCode, '') = isnull(source.DrugCode, '')
		and isnull(target.Drug, '')  = isnull(source.Drug, '')  
		and isnull(target.Category, '') = isnull(source.Category, '')
		and isnull(target.Dose, '') = isnull(source.Dose, '')
		and isnull(target.Units, '') = isnull(source.Units, '')
		and isnull(target.StartTime, getdate()) = isnull(source.StartTime, getdate())
		and isnull(target.EndTime, getdate()) = isnull(source.EndTime, getdate())
		and isnull(target.Duration, 0) = isnull(source.Duration, 0)
		and isnull(target.SessionType, '') = isnull(source.SessionType, '')
		and isnull(target.SessionLocationCode, 0)  = isnull(source.SessionLocationCode,0) 
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			target.EpisodeKey = source.EpisodeKey
			,target.HospitalNumber = source.HospitalNumber
			,target.LastName = source.LastName
			,target.FirstName = source.FirstName
			,target.DateOfBirth = source.DateOfBirth
			,target.GenderCode = source.GenderCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ConsultantCode = source.ConsultantCode
			,target.IntendedDestination = source.IntendedDestination
			,target.ActualDestination = source.ActualDestination
			,target.DrugCode = source.DrugCode
			,target.Drug = source.Drug
			,target.Category = source.Category
			,target.Dose = source.Dose
			,target.Units = source.Units
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.Duration = source.Duration
			,target.SessionType = source.SessionType
			,target.SessionLocationCode = source.SessionLocationCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


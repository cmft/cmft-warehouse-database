﻿
/* Procedure */

create procedure ETL.AssignTImportOPEncounterDirectorateCode as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsUpdated Int
declare @Stats varchar(255)

declare @from smalldatetime
declare @to smalldatetime

select @StartTime = getdate()
select @RowsUpdated = 0


if object_id('OP.WHDivisionMap') is not null
  drop table OP.WHDivisionMap;

select * into OP.WHDivisionMap from WH.DivisionMap with (nolock);

-- main mapping
update TImportOPEncounter
set
	 TImportOPEncounter.DirectorateCode = DivisionMap.DirectorateCode

	,TImportOPEncounter.ReferringSpecialtyTypeCode =
		CASE 
		WHEN DivisionMap.Diagnostic = 'Y' THEN 'D'
		WHEN DivisionMap.Diagnostic = 'N' THEN 'I'
		ELSE DivisionMap.Diagnostic
		END
from
	ETL.TImportOPEncounter

inner join OP.WHDivisionMap DivisionMap 
on	DivisionMap.SiteCode = TImportOPEncounter.EpisodicSiteCode
and	DivisionMap.SpecialtyCode = TImportOPEncounter.ReferringSpecialtyCode
and 
	(
		DivisionMap.ConsultantCode = TImportOPEncounter.ConsultantCode
	or
		DivisionMap.ConsultantCode is null
	)
		
and TImportOPEncounter.AppointmentDate between FromDate and coalesce(ToDate, '31 dec 9999')


where
	not exists
		(
		select
			1
		from
			OP.WHDivisionMap Previous
		where
			Previous.SiteCode = TImportOPEncounter.EpisodicSiteCode
		and	Previous.SpecialtyCode = TImportOPEncounter.ReferringSpecialtyCode


		and 
			(
				Previous.ConsultantCode = TImportOPEncounter.ConsultantCode
			or
				Previous.ConsultantCode is null
			)

		and
			TImportOPEncounter.AppointmentDate between Previous.FromDate and coalesce(Previous.ToDate, '31 dec 9999')

		and	(
				case
				when Previous.ConsultantCode is not null
				then 4
				else 0
				end

				>

				case
				when DivisionMap.ConsultantCode is not null
				then 4
				else 0
				end
	
			--or
			--	(
			--		case
			--		when Previous.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end

			--		=

			--		case
			--		when DivisionRuleBase.ConsultantCode is not null
			--		then 4
			--		else 0
			--		end 

				)
			)
			

select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT

-- specific mapping rules

update ETL.TImportOPEncounter
set
	DirectorateCode = 70
WHERE 
	ReferringSpecialtyCode in ('PT' , 'DIET' , 'DTCO') --Like '110 P%' Or	Specialty Like '654%'
AND AppointmentDate < '9 Jan 2009'


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


update ETL.TImportOPEncounter
set
	DirectorateCode = 0
where
	ReferringSpecialtyCode in ( 'PAED' , 'PNEP' )
AND AppointmentDate >= '7 Jan 2009'
AND SiteCode = 'MRI'


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


update ETL.TImportOPEncounter
set
	DirectorateCode = 30
where
	ReferringSpecialtyCode = 'PMET'
AND ClinicCode in ('CAAMMETT' , 'CJEWMET' , 'CJHWMET' , 'RJEWMET')


select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT


update ETL.TImportOPEncounter
set
	DirectorateCode = 0
WHERE 
	ReferringSpecialtyCode = 'OPHC'
AND ClinicCode in ( 'CORTHOP' , 'CPREAD' );

if object_id('OP.WHDivisionMap') is not null 
  drop table OP.WHDivisionMap;



select
	@RowsUpdated = @RowsUpdated + @@ROWCOUNT



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Time Elapsed ' + 
		CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'AssignTImportOPEncounterDirectorateCode', @Stats, @StartTime



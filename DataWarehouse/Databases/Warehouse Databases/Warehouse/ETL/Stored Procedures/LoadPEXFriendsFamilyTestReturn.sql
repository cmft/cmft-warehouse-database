﻿
CREATE procedure [ETL].[LoadPEXFriendsFamilyTestReturn] 

	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null

as

set dateformat dmy

declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

declare @StartTime datetime = getdate()
declare @Elapsed int

declare
	 @deleted int
	,@inserted int
	,@updated int

declare @Stats varchar(255)

truncate table PEX.FriendsFamilyTestReturn

set @deleted = @@rowcount


insert into PEX.FriendsFamilyTestReturn
(
	[Return]
	,CensusDate
	,[HospitalSiteCode]
	,[HospitalSite]
	,[WardCode]
	,[Ward]
	,[1ExtremelyLikely]
	,[2Likely]
	,[3NeitherLikelyNorUnlikely]
	,[4Unlikely]
	,[5ExtremelyUnlikely]
	,[6DontKnow]
	,[Responses]
	,[EligibleResponders]
	,[InterfaceCode]
	,[Created]
	,[Updated]
	,[ByWhom]
)

select
	[Return]
	,CensusDate
	,[HospitalSiteCode]
	,[HospitalSite]
	,[WardCode]
	,[Ward]
	,[1ExtremelyLikely]
	,[2Likely]
	,[3NeitherLikelyNorUnlikely]
	,[4Unlikely]
	,[5ExtremelyUnlikely]
	,[6DontKnow]
	,[Responses]
	,[EligibleResponders]
	,[InterfaceCode]
	,Created = getdate()
	,Updated = getdate()
	,ByWhom = suser_name()

FROM 
	[ETL].[TLoadPEXFriendsFamilyTestReturn]


set @inserted = @@rowcount

set @updated = 0

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
		', Period from ' + CONVERT(varchar(11), @fromDate) + ' to ' + CONVERT(varchar(11), @toDate)


exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats






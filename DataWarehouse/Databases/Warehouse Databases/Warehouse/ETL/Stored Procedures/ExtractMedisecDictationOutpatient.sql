﻿



CREATE proc [ETL].[ExtractMedisecDictationOutpatient]

 @fromDate smalldatetime = null
, @toDate smalldatetime = null

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @census varchar(8)

select @StartTime = getdate()

select @RowsInserted = 0

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -12, getdate()))), 0), 112) 

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, dateadd(day, 0, getdate()))), 0), 112)



insert into ETL.TImportMedisecDictationOutpatient

(
	SourcePatientNo
	,AppointmentTime
	,DoctorCode
)

select 
	SourcePatientNo = cast(outpn as int)
	,AppointmentTime =
						case
						when left(Outpatient.outapptime ,2) = 24
						then dateadd(day, 1, cast(Outpatient.outappdate as datetime))
						else cast(Outpatient.outappdate as datetime)
						end
						+ ' ' + 
						case
						when left(Outpatient.outapptime ,2) = 24
						then '00:00:00'
						when coalesce(Outpatient.outapptime, '') = ''
						then '00:00:00'
						else left(Outpatient.outapptime,2) + ':' + right(Outpatient.outapptime,2) + ':00'
						end 
	,DoctorCode = ltrim(rtrim(outdoctor))
from
	[$(CMFT_MEDISEC)].dbo.OutPatient Outpatient

where
	case
	when left(Outpatient.outapptime ,2) = 24
	then dateadd(day, 1, cast(Outpatient.outappdate as datetime))
	else cast(Outpatient.outappdate as datetime)
	end between @from and @to
	
and	not exists						
	(
	select
		1
	from
		[$(CMFT_MEDISEC)].dbo.OutPatient LaterOutpatient
	where
		LaterOutpatient.outpn = Outpatient.outpn
	and	LaterOutpatient.outappdate = Outpatient.outappdate
	and	LaterOutpatient.outapptime = Outpatient.outapptime
	and coalesce(LaterOutpatient.outletstatus, '') not in ('00','09')
	and	cast(LaterOutpatient.outepno as int) > cast(Outpatient.outepno as int)
	)
and coalesce(Outpatient.outletstatus, '') not in ('00','09') -- 00 = Letter Not Created; 09 = DNR - these are not letters, so can be excluded (DG)


select @RowsInserted = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute,@StartTime,getdate())


select @Stats = 
      'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
      'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractMedisecDictationOutpatient', @Stats, @StartTime



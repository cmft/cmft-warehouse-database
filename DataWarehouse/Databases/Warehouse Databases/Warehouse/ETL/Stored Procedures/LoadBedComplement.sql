﻿CREATE proc [ETL].[LoadBedComplement]
as

exec ETL.ExtractSharepointWard
exec ETL.LoadBedComplementWard

exec ETL.ExtractSharepointWardBed
exec ETL.LoadBedComplementWardBed

exec ETL.ExtractSharepointWardConfiguration
exec ETL.LoadBedComplementWardConfiguration

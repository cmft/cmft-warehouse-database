﻿CREATE procedure [ETL].[AssignRTTAttributes] (
	@Source varchar(10)
)
as

if @Source in ( 'APC' , 'APCWL' , 'OP' , 'OPWL' , 'RF' )
begin

declare @sqlUpdateAPC varchar(1000)
declare @sqlUpdateAPCWL varchar(1000)
declare @sqlUpdateOP varchar(1000)
declare @sqlUpdateOPWL varchar(1000)
declare @sqlUpdateRF varchar(1000)

declare @fromTable varchar(100)


	select @fromTable =
		case @Source
		when 'APC' then 'TImportAPCEncounter'
		when 'APCWL' then 'TImportAPCWaitingList'
		when 'OP' then 'TImportOPEncounter'
		when 'OPWL' then 'TImportOPWaitingList'
		when 'RF' then 'TImportRFEncounter'
		else null
		end

	select
		@sqlUpdateAPC = '
	
	update APC.Encounter
	set
		 RTTPathwayCondition = TEncounter.RTTPathwayCondition
		,RTTStartDate = TEncounter.RTTStartDate
		,RTTEndDate = TEncounter.RTTEndDate
		,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
		,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
		,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
		,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	from
		dbo.' + @fromTable + ' TEncounter
	where
		TEncounter.RTTPathwayID = Encounter.RTTPathwayID
	'

	select
		@sqlUpdateAPCWL = '
	
	update APC.WaitingList
	set
		 RTTPathwayCondition = TEncounter.RTTPathwayCondition
		,RTTStartDate = TEncounter.RTTStartDate
		,RTTEndDate = TEncounter.RTTEndDate
		,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
		,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
		,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
		,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	from
		dbo.' + @fromTable + ' TEncounter
	where
		TEncounter.RTTPathwayID = WaitingList.RTTPathwayID
	'


	select
		@sqlUpdateOP = '
	
	update OP.Encounter
	set
		 RTTPathwayCondition = TEncounter.RTTPathwayCondition
		,RTTStartDate = TEncounter.RTTStartDate
		,RTTEndDate = TEncounter.RTTEndDate
		,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
		,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
		,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
		,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	from
		dbo.' + @fromTable + ' TEncounter
	where
		TEncounter.RTTPathwayID = Encounter.RTTPathwayID
	'

	select
		@sqlUpdateOPWL = '
	
	update OP.WaitingList
	set
		 RTTPathwayCondition = TEncounter.RTTPathwayCondition
		,RTTStartDate = TEncounter.RTTStartDate
		,RTTEndDate = TEncounter.RTTEndDate
		,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
		,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
		,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
		,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	from
		dbo.' + @fromTable + ' TEncounter
	where
		TEncounter.RTTPathwayID = WaitingList.RTTPathwayID
	'

	select
		@sqlUpdateRF = '
	
	update RF.Encounter
	set
		 RTTPathwayCondition = TEncounter.RTTPathwayCondition
		,RTTStartDate = TEncounter.RTTStartDate
		,RTTEndDate = TEncounter.RTTEndDate
		,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
		,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
		,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
		,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	from
		dbo.' + @fromTable + ' TEncounter
	where
		TEncounter.RTTPathwayID = WaitingList.RTTPathwayID
	'

	exec (@sqlUpdateAPC)
	exec (@sqlUpdateAPCWL)
	exec (@sqlUpdateOP)
	exec (@sqlUpdateOPWL)
	exec (@sqlUpdateRF)
end

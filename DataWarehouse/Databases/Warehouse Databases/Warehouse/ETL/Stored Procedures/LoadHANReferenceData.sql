﻿
CREATE procedure [ETL].[LoadHANReferenceData] as


truncate table HAN.Ward

insert HAN.Ward
(
	 WardID
	,Ward
	,PASWardCode
	,HospitalCode
)

select
	 WardID = ID
	,Ward = WardName
	,PASWardCode = PASCode
	,HospitalCode = Hospital
from
	[$(HospitalAtNight)].dbo.Ward




truncate table HAN.Status

insert HAN.Status
(
	 StatusID
	,Status
)

select
	 StatusID = ID
	,Status = StateName
from
	[$(HospitalAtNight)].dbo.Status




truncate table HAN.CallType

insert HAN.CallType
(
	 CallTypeID
	,CallType
	,Active
)

select
	 CallTypeID = ID
	,CallType = Name
	,Active
from
	[$(HospitalAtNight)].dbo.CallType




truncate table HAN.Situation

insert HAN.Situation
(
	 SituationID
	,Situation
	,SituationParentID
	,Active
	,DisplayOrder
)

select
	 SituationID = [ID]
	,Situation = SituationDescription
	,SituationParentID = ParentID
	,Active
	,DisplayOrder = [Order]
from
	[$(HospitalAtNight)].dbo.Situation




truncate table HAN.ClinicalRole

insert HAN.ClinicalRole
(
	 ClinicalRoleID
	,ClinicalRole
	,Active
	,ClinicalCategoryID
	,DisplayOrder
)

select
	 ClinicalRoleID = ID
	,ClinicalRole = Role
	,Active
	,ClinicalCategoryID
	,DisplayOrder = [Order]
from
	[$(HospitalAtNight)].dbo.ClinicalRole




truncate table HAN.ClinicalCategory

insert HAN.ClinicalCategory
(
	 ClinicalCategoryID
	,ClinicalCategory
	,Active
	,DisplayOrder
)

select
	 ClinicalCategoryID = ID
	,ClinicalCategory = CinicalCategoryName
	,Active
	,DisplayOrder = [Order]
from
	[$(HospitalAtNight)].dbo.ClinicalCategories




truncate table HAN.Outcome

insert HAN.Outcome
(
	 OutcomeID
	,Outcome
	,Active
	,DisplayOrder
)

select
	 OutcomeID = ID
	,Outcome = Name
	,Active
	,DisplayOrder = [Order]
from
	[$(HospitalAtNight)].dbo.Outcome


truncate table HAN.Sex

insert HAN.Sex
(
	 SexCode
	,Sex
)

select distinct
	SexCode =
		left(
			ServiceRequest.PatientDetails.value('(PatientInformation/Gender)[1]', 'VARCHAR(50)')
			,1
		)

	,Sex =
		ServiceRequest.PatientDetails.value('(PatientInformation/Gender)[1]', 'VARCHAR(50)')
from
	[$(HospitalAtNight)].dbo.ServiceRequest

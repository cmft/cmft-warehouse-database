﻿

CREATE procedure [ETL].[LoadOPEncounterV2]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, AppointmentDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, AppointmentDate)) 
from
	ETL.TLoadOPEncounter

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	OP.Encounter target
using
	(
	select
		 SourceUniqueID
		,SourcePatientNo
		,SourceEncounterNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,NHSNumber
		,DistrictNo
		,Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,EthnicOriginCode
		,MaritalStatusCode
		,ReligionCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode
		,ReferringConsultantCode
		,ReferringSpecialtyCode
		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryOperationDate
		,SecondaryOperationCode1
		,SecondaryOperationDate1
		,SecondaryOperationCode2
		,SecondaryOperationDate2
		,SecondaryOperationCode3
		,SecondaryOperationDate3
		,SecondaryOperationCode4
		,SecondaryOperationDate4
		,SecondaryOperationCode5
		,SecondaryOperationDate5
		,SecondaryOperationCode6
		,SecondaryOperationDate6
		,SecondaryOperationCode7
		,SecondaryOperationDate7
		,SecondaryOperationCode8
		,SecondaryOperationDate8
		,SecondaryOperationCode9
		,SecondaryOperationDate9
		,SecondaryOperationCode10
		,SecondaryOperationDate10
		,SecondaryOperationCode11
		,SecondaryOperationDate11
		,PurchaserCode
		,ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,LocalAdminCategoryCode
		,PCTCode
		,LocalityCode
		,IsWardAttender
		,HomePhone
		,WorkPhone
		,ReferredByCode
		,ReferrerCode
		,InterpreterIndicator
		,EpisodicSiteCode
		,DirectorateCode
	from
		ETL.TLoadOPEncounter
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.AppointmentDate between @LoadStartDate and @LoadEndDate
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,PatientTitle
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,NHSNumber
			,DistrictNo
			,Postcode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,DHACode
			,EthnicOriginCode
			,MaritalStatusCode
			,ReligionCode
			,RegisteredGpCode
			,RegisteredGpPracticeCode
			,SiteCode
			,AppointmentDate
			,AppointmentTime
			,ClinicCode
			,AdminCategoryCode
			,SourceOfReferralCode
			,ReasonForReferralCode
			,PriorityCode
			,FirstAttendanceFlag
			,DNACode
			,AppointmentStatusCode
			,CancelledByCode
			,TransportRequiredFlag
			,AttendanceOutcomeCode
			,AppointmentTypeCode
			,DisposalCode
			,ConsultantCode
			,SpecialtyCode
			,ReferringConsultantCode
			,ReferringSpecialtyCode
			,BookingTypeCode
			,CasenoteNo
			,AppointmentCreateDate
			,EpisodicGpCode
			,EpisodicGpPracticeCode
			,DoctorCode
			,PrimaryDiagnosisCode
			,SubsidiaryDiagnosisCode
			,SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			,PrimaryOperationCode
			,PrimaryOperationDate
			,SecondaryOperationCode1
			,SecondaryOperationDate1
			,SecondaryOperationCode2
			,SecondaryOperationDate2
			,SecondaryOperationCode3
			,SecondaryOperationDate3
			,SecondaryOperationCode4
			,SecondaryOperationDate4
			,SecondaryOperationCode5
			,SecondaryOperationDate5
			,SecondaryOperationCode6
			,SecondaryOperationDate6
			,SecondaryOperationCode7
			,SecondaryOperationDate7
			,SecondaryOperationCode8
			,SecondaryOperationDate8
			,SecondaryOperationCode9
			,SecondaryOperationDate9
			,SecondaryOperationCode10
			,SecondaryOperationDate10
			,SecondaryOperationCode11
			,SecondaryOperationDate11
			,PurchaserCode
			,ProviderCode
			,ContractSerialNo
			,ReferralDate
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,RTTPeriodStatusCode
			,AppointmentCategoryCode
			,AppointmentCreatedBy
			,AppointmentCancelDate
			,LastRevisedDate
			,LastRevisedBy
			,OverseasStatusFlag
			,PatientChoiceCode
			,ScheduledCancelReasonCode
			,PatientCancelReason
			,DischargeDate
			,QM08StartWaitDate
			,QM08EndWaitDate
			,DestinationSiteCode
			,EBookingReferenceNo
			,InterfaceCode
			,LocalAdminCategoryCode
			,PCTCode
			,LocalityCode
			,IsWardAttender
			,HomePhone
			,WorkPhone
			,ReferredByCode
			,ReferrerCode
			,InterpreterIndicator
			,EpisodicSiteCode
			,DirectorateCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceEncounterNo
			,source.PatientTitle
			,source.PatientForename
			,source.PatientSurname
			,source.DateOfBirth
			,source.DateOfDeath
			,source.SexCode
			,source.NHSNumber
			,source.DistrictNo
			,source.Postcode
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.DHACode
			,source.EthnicOriginCode
			,source.MaritalStatusCode
			,source.ReligionCode
			,source.RegisteredGpCode
			,source.RegisteredGpPracticeCode
			,source.SiteCode
			,source.AppointmentDate
			,source.AppointmentTime
			,source.ClinicCode
			,source.AdminCategoryCode
			,source.SourceOfReferralCode
			,source.ReasonForReferralCode
			,source.PriorityCode
			,source.FirstAttendanceFlag
			,source.DNACode
			,source.AppointmentStatusCode
			,source.CancelledByCode
			,source.TransportRequiredFlag
			,source.AttendanceOutcomeCode
			,source.AppointmentTypeCode
			,source.DisposalCode
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.ReferringConsultantCode
			,source.ReferringSpecialtyCode
			,source.BookingTypeCode
			,source.CasenoteNo
			,source.AppointmentCreateDate
			,source.EpisodicGpCode
			,source.EpisodicGpPracticeCode
			,source.DoctorCode
			,source.PrimaryDiagnosisCode
			,source.SubsidiaryDiagnosisCode
			,source.SecondaryDiagnosisCode1
			,source.SecondaryDiagnosisCode2
			,source.SecondaryDiagnosisCode3
			,source.SecondaryDiagnosisCode4
			,source.SecondaryDiagnosisCode5
			,source.SecondaryDiagnosisCode6
			,source.SecondaryDiagnosisCode7
			,source.SecondaryDiagnosisCode8
			,source.SecondaryDiagnosisCode9
			,source.SecondaryDiagnosisCode10
			,source.SecondaryDiagnosisCode11
			,source.SecondaryDiagnosisCode12
			,source.PrimaryOperationCode
			,source.PrimaryOperationDate
			,source.SecondaryOperationCode1
			,source.SecondaryOperationDate1
			,source.SecondaryOperationCode2
			,source.SecondaryOperationDate2
			,source.SecondaryOperationCode3
			,source.SecondaryOperationDate3
			,source.SecondaryOperationCode4
			,source.SecondaryOperationDate4
			,source.SecondaryOperationCode5
			,source.SecondaryOperationDate5
			,source.SecondaryOperationCode6
			,source.SecondaryOperationDate6
			,source.SecondaryOperationCode7
			,source.SecondaryOperationDate7
			,source.SecondaryOperationCode8
			,source.SecondaryOperationDate8
			,source.SecondaryOperationCode9
			,source.SecondaryOperationDate9
			,source.SecondaryOperationCode10
			,source.SecondaryOperationDate10
			,source.SecondaryOperationCode11
			,source.SecondaryOperationDate11
			,source.PurchaserCode
			,source.ProviderCode
			,source.ContractSerialNo
			,source.ReferralDate
			,source.RTTPathwayID
			,source.RTTPathwayCondition
			,source.RTTStartDate
			,source.RTTEndDate
			,source.RTTSpecialtyCode
			,source.RTTCurrentProviderCode
			,source.RTTCurrentStatusCode
			,source.RTTCurrentStatusDate
			,source.RTTCurrentPrivatePatientFlag
			,source.RTTOverseasStatusFlag
			,source.RTTPeriodStatusCode
			,source.AppointmentCategoryCode
			,source.AppointmentCreatedBy
			,source.AppointmentCancelDate
			,source.LastRevisedDate
			,source.LastRevisedBy
			,source.OverseasStatusFlag
			,source.PatientChoiceCode
			,source.ScheduledCancelReasonCode
			,source.PatientCancelReason
			,source.DischargeDate
			,source.QM08StartWaitDate
			,source.QM08EndWaitDate
			,source.DestinationSiteCode
			,source.EBookingReferenceNo
			,source.InterfaceCode
			,source.LocalAdminCategoryCode
			,source.PCTCode
			,source.LocalityCode
			,source.IsWardAttender
			,source.HomePhone
			,source.WorkPhone
			,source.ReferredByCode
			,source.ReferrerCode
			,source.InterpreterIndicator
			,source.EpisodicSiteCode
			,source.DirectorateCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			target.SourcePatientNo = source.SourcePatientNo
		and	target.SourceEncounterNo = source.SourceEncounterNo
		and	target.PatientTitle = source.PatientTitle
		and	target.PatientForename = source.PatientForename
		and	target.PatientSurname = source.PatientSurname
		and	target.DateOfBirth = source.DateOfBirth
		and	target.DateOfDeath = source.DateOfDeath
		and	target.SexCode = source.SexCode
		and	target.NHSNumber = source.NHSNumber
		and	target.DistrictNo = source.DistrictNo
		and	target.Postcode = source.Postcode
		and	target.PatientAddress1 = source.PatientAddress1
		and	target.PatientAddress2 = source.PatientAddress2
		and	target.PatientAddress3 = source.PatientAddress3
		and	target.PatientAddress4 = source.PatientAddress4
		and	target.DHACode = source.DHACode
		and	target.EthnicOriginCode = source.EthnicOriginCode
		and	target.MaritalStatusCode = source.MaritalStatusCode
		and	target.ReligionCode = source.ReligionCode
		and	target.RegisteredGpCode = source.RegisteredGpCode
		and	target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
		and	target.SiteCode = source.SiteCode
		and	target.AppointmentDate = source.AppointmentDate
		and	target.AppointmentTime = source.AppointmentTime
		and	target.ClinicCode = source.ClinicCode
		and	target.AdminCategoryCode = source.AdminCategoryCode
		and	target.SourceOfReferralCode = source.SourceOfReferralCode
		and	target.ReasonForReferralCode = source.ReasonForReferralCode
		and	target.PriorityCode = source.PriorityCode
		and	target.FirstAttendanceFlag = source.FirstAttendanceFlag
		and	target.DNACode = source.DNACode
		and	target.AppointmentStatusCode = source.AppointmentStatusCode
		and	target.CancelledByCode = source.CancelledByCode
		and	target.TransportRequiredFlag = source.TransportRequiredFlag
		and	target.AttendanceOutcomeCode = source.AttendanceOutcomeCode
		and	target.AppointmentTypeCode = source.AppointmentTypeCode
		and	target.DisposalCode = source.DisposalCode
		and	target.ConsultantCode = source.ConsultantCode
		and	target.SpecialtyCode = source.SpecialtyCode
		and	target.ReferringConsultantCode = source.ReferringConsultantCode
		and	target.ReferringSpecialtyCode = source.ReferringSpecialtyCode
		and	target.BookingTypeCode = source.BookingTypeCode
		and	target.CasenoteNo = source.CasenoteNo
		and	target.AppointmentCreateDate = source.AppointmentCreateDate
		and	target.EpisodicGpCode = source.EpisodicGpCode
		and	target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
		and	target.DoctorCode = source.DoctorCode
		and	target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
		and	target.SubsidiaryDiagnosisCode = source.SubsidiaryDiagnosisCode
		and	target.SecondaryDiagnosisCode1 = source.SecondaryDiagnosisCode1
		and	target.SecondaryDiagnosisCode2 = source.SecondaryDiagnosisCode2
		and	target.SecondaryDiagnosisCode3 = source.SecondaryDiagnosisCode3
		and	target.SecondaryDiagnosisCode4 = source.SecondaryDiagnosisCode4
		and	target.SecondaryDiagnosisCode5 = source.SecondaryDiagnosisCode5
		and	target.SecondaryDiagnosisCode6 = source.SecondaryDiagnosisCode6
		and	target.SecondaryDiagnosisCode7 = source.SecondaryDiagnosisCode7
		and	target.SecondaryDiagnosisCode8 = source.SecondaryDiagnosisCode8
		and	target.SecondaryDiagnosisCode9 = source.SecondaryDiagnosisCode9
		and	target.SecondaryDiagnosisCode10 = source.SecondaryDiagnosisCode10
		and	target.SecondaryDiagnosisCode11 = source.SecondaryDiagnosisCode11
		and	target.SecondaryDiagnosisCode12 = source.SecondaryDiagnosisCode12
		and	target.PrimaryOperationCode = source.PrimaryOperationCode
		and	target.PrimaryOperationDate = source.PrimaryOperationDate
		and	target.SecondaryOperationCode1 = source.SecondaryOperationCode1
		and	target.SecondaryOperationDate1 = source.SecondaryOperationDate1
		and	target.SecondaryOperationCode2 = source.SecondaryOperationCode2
		and	target.SecondaryOperationDate2 = source.SecondaryOperationDate2
		and	target.SecondaryOperationCode3 = source.SecondaryOperationCode3
		and	target.SecondaryOperationDate3 = source.SecondaryOperationDate3
		and	target.SecondaryOperationCode4 = source.SecondaryOperationCode4
		and	target.SecondaryOperationDate4 = source.SecondaryOperationDate4
		and	target.SecondaryOperationCode5 = source.SecondaryOperationCode5
		and	target.SecondaryOperationDate5 = source.SecondaryOperationDate5
		and	target.SecondaryOperationCode6 = source.SecondaryOperationCode6
		and	target.SecondaryOperationDate6 = source.SecondaryOperationDate6
		and	target.SecondaryOperationCode7 = source.SecondaryOperationCode7
		and	target.SecondaryOperationDate7 = source.SecondaryOperationDate7
		and	target.SecondaryOperationCode8 = source.SecondaryOperationCode8
		and	target.SecondaryOperationDate8 = source.SecondaryOperationDate8
		and	target.SecondaryOperationCode9 = source.SecondaryOperationCode9
		and	target.SecondaryOperationDate9 = source.SecondaryOperationDate9
		and	target.SecondaryOperationCode10 = source.SecondaryOperationCode10
		and	target.SecondaryOperationDate10 = source.SecondaryOperationDate10
		and	target.SecondaryOperationCode11 = source.SecondaryOperationCode11
		and	target.SecondaryOperationDate11 = source.SecondaryOperationDate11
		and	target.PurchaserCode = source.PurchaserCode
		and	target.ProviderCode = source.ProviderCode
		and	target.ContractSerialNo = source.ContractSerialNo
		and	target.ReferralDate = source.ReferralDate
		and	target.RTTPathwayID = source.RTTPathwayID
		and	target.RTTPathwayCondition = source.RTTPathwayCondition
		and	target.RTTStartDate = source.RTTStartDate
		and	target.RTTEndDate = source.RTTEndDate
		and	target.RTTSpecialtyCode = source.RTTSpecialtyCode
		and	target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
		and	target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
		and	target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
		and	target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
		and	target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
		and	target.RTTPeriodStatusCode = source.RTTPeriodStatusCode
		and	target.AppointmentCategoryCode = source.AppointmentCategoryCode
		and	target.AppointmentCreatedBy = source.AppointmentCreatedBy
		and	target.AppointmentCancelDate = source.AppointmentCancelDate
		and	target.LastRevisedDate = source.LastRevisedDate
		and	target.LastRevisedBy = source.LastRevisedBy
		and	target.OverseasStatusFlag = source.OverseasStatusFlag
		and	target.PatientChoiceCode = source.PatientChoiceCode
		and	target.ScheduledCancelReasonCode = source.ScheduledCancelReasonCode
		and	target.PatientCancelReason = source.PatientCancelReason
		and	target.DischargeDate = source.DischargeDate
		and	target.QM08StartWaitDate = source.QM08StartWaitDate
		and	target.QM08EndWaitDate = source.QM08EndWaitDate
		and	target.DestinationSiteCode = source.DestinationSiteCode
		and	target.EBookingReferenceNo = source.EBookingReferenceNo
		and	target.InterfaceCode = source.InterfaceCode
		and	target.LocalAdminCategoryCode = source.LocalAdminCategoryCode
		and	target.PCTCode = source.PCTCode
		and	target.LocalityCode = source.LocalityCode
		and	target.IsWardAttender = source.IsWardAttender
		and	target.HomePhone = source.HomePhone
		and	target.WorkPhone = source.WorkPhone
		and	target.ReferredByCode = source.ReferredByCode
		and	target.ReferrerCode = source.ReferrerCode
		and	target.InterpreterIndicator = source.InterpreterIndicator
		and	target.EpisodicSiteCode = source.EpisodicSiteCode
		and	target.DirectorateCode = source.DirectorateCode
		)
	then
		update
		set
			 target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.NHSNumber = source.NHSNumber
			,target.DistrictNo = source.DistrictNo
			,target.Postcode = source.Postcode
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.DHACode = source.DHACode
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.ReligionCode = source.ReligionCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.SiteCode = source.SiteCode
			,target.AppointmentDate = source.AppointmentDate
			,target.AppointmentTime = source.AppointmentTime
			,target.ClinicCode = source.ClinicCode
			,target.AdminCategoryCode = source.AdminCategoryCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ReasonForReferralCode = source.ReasonForReferralCode
			,target.PriorityCode = source.PriorityCode
			,target.FirstAttendanceFlag = source.FirstAttendanceFlag
			,target.DNACode = source.DNACode
			,target.AppointmentStatusCode = source.AppointmentStatusCode
			,target.CancelledByCode = source.CancelledByCode
			,target.TransportRequiredFlag = source.TransportRequiredFlag
			,target.AttendanceOutcomeCode = source.AttendanceOutcomeCode
			,target.AppointmentTypeCode = source.AppointmentTypeCode
			,target.DisposalCode = source.DisposalCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ReferringConsultantCode = source.ReferringConsultantCode
			,target.ReferringSpecialtyCode = source.ReferringSpecialtyCode
			,target.BookingTypeCode = source.BookingTypeCode
			,target.CasenoteNo = source.CasenoteNo
			,target.AppointmentCreateDate = source.AppointmentCreateDate
			,target.EpisodicGpCode = source.EpisodicGpCode
			,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
			,target.DoctorCode = source.DoctorCode
			,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
			,target.SubsidiaryDiagnosisCode = source.SubsidiaryDiagnosisCode
			,target.SecondaryDiagnosisCode1 = source.SecondaryDiagnosisCode1
			,target.SecondaryDiagnosisCode2 = source.SecondaryDiagnosisCode2
			,target.SecondaryDiagnosisCode3 = source.SecondaryDiagnosisCode3
			,target.SecondaryDiagnosisCode4 = source.SecondaryDiagnosisCode4
			,target.SecondaryDiagnosisCode5 = source.SecondaryDiagnosisCode5
			,target.SecondaryDiagnosisCode6 = source.SecondaryDiagnosisCode6
			,target.SecondaryDiagnosisCode7 = source.SecondaryDiagnosisCode7
			,target.SecondaryDiagnosisCode8 = source.SecondaryDiagnosisCode8
			,target.SecondaryDiagnosisCode9 = source.SecondaryDiagnosisCode9
			,target.SecondaryDiagnosisCode10 = source.SecondaryDiagnosisCode10
			,target.SecondaryDiagnosisCode11 = source.SecondaryDiagnosisCode11
			,target.SecondaryDiagnosisCode12 = source.SecondaryDiagnosisCode12
			,target.PrimaryOperationCode = source.PrimaryOperationCode
			,target.PrimaryOperationDate = source.PrimaryOperationDate
			,target.SecondaryOperationCode1 = source.SecondaryOperationCode1
			,target.SecondaryOperationDate1 = source.SecondaryOperationDate1
			,target.SecondaryOperationCode2 = source.SecondaryOperationCode2
			,target.SecondaryOperationDate2 = source.SecondaryOperationDate2
			,target.SecondaryOperationCode3 = source.SecondaryOperationCode3
			,target.SecondaryOperationDate3 = source.SecondaryOperationDate3
			,target.SecondaryOperationCode4 = source.SecondaryOperationCode4
			,target.SecondaryOperationDate4 = source.SecondaryOperationDate4
			,target.SecondaryOperationCode5 = source.SecondaryOperationCode5
			,target.SecondaryOperationDate5 = source.SecondaryOperationDate5
			,target.SecondaryOperationCode6 = source.SecondaryOperationCode6
			,target.SecondaryOperationDate6 = source.SecondaryOperationDate6
			,target.SecondaryOperationCode7 = source.SecondaryOperationCode7
			,target.SecondaryOperationDate7 = source.SecondaryOperationDate7
			,target.SecondaryOperationCode8 = source.SecondaryOperationCode8
			,target.SecondaryOperationDate8 = source.SecondaryOperationDate8
			,target.SecondaryOperationCode9 = source.SecondaryOperationCode9
			,target.SecondaryOperationDate9 = source.SecondaryOperationDate9
			,target.SecondaryOperationCode10 = source.SecondaryOperationCode10
			,target.SecondaryOperationDate10 = source.SecondaryOperationDate10
			,target.SecondaryOperationCode11 = source.SecondaryOperationCode11
			,target.SecondaryOperationDate11 = source.SecondaryOperationDate11
			,target.PurchaserCode = source.PurchaserCode
			,target.ProviderCode = source.ProviderCode
			,target.ContractSerialNo = source.ContractSerialNo
			,target.ReferralDate = source.ReferralDate
			,target.RTTPathwayID = source.RTTPathwayID
			,target.RTTPathwayCondition = source.RTTPathwayCondition
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTSpecialtyCode = source.RTTSpecialtyCode
			,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
			,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
			,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
			,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
			,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
			,target.RTTPeriodStatusCode = source.RTTPeriodStatusCode
			,target.AppointmentCategoryCode = source.AppointmentCategoryCode
			,target.AppointmentCreatedBy = source.AppointmentCreatedBy
			,target.AppointmentCancelDate = source.AppointmentCancelDate
			,target.LastRevisedDate = source.LastRevisedDate
			,target.LastRevisedBy = source.LastRevisedBy
			,target.OverseasStatusFlag = source.OverseasStatusFlag
			,target.PatientChoiceCode = source.PatientChoiceCode
			,target.ScheduledCancelReasonCode = source.ScheduledCancelReasonCode
			,target.PatientCancelReason = source.PatientCancelReason
			,target.DischargeDate = source.DischargeDate
			,target.QM08StartWaitDate = source.QM08StartWaitDate
			,target.QM08EndWaitDate = source.QM08EndWaitDate
			,target.DestinationSiteCode = source.DestinationSiteCode
			,target.EBookingReferenceNo = source.EBookingReferenceNo
			,target.InterfaceCode = source.InterfaceCode
			,target.LocalAdminCategoryCode = source.LocalAdminCategoryCode
			,target.PCTCode = source.PCTCode
			,target.LocalityCode = source.LocalityCode
			,target.IsWardAttender = source.IsWardAttender
			,target.HomePhone = source.HomePhone
			,target.WorkPhone = source.WorkPhone
			,target.ReferredByCode = source.ReferredByCode
			,target.ReferrerCode = source.ReferrerCode
			,target.InterpreterIndicator = source.InterpreterIndicator
			,target.EpisodicSiteCode = source.EpisodicSiteCode
			,target.DirectorateCode = source.DirectorateCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


--now copy RTT Current Status back to historical appointments
update OP.Encounter
set
	 RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
from
	ETL.TImportOPEncounter TEncounter
where
	TEncounter.RTTPathwayID = Encounter.RTTPathwayID


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
		', Period from ' + CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




﻿CREATE procedure [ETL].[ExtractBedManEvent]
	 @fromDate datetime = '19000101'
	,@toDate datetime = '21000101'
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0

truncate table ETL.TImportBedManEvent

-- Insert records
insert into ETL.TImportBedManEvent
(
	SourceUniqueID
	,SourceSpellNo
	,EventTypeCode
	,SourcePatientNo
	,AdmissionTime
	,DischargeTime
	,EventDate
	,EventDetails
)

select 
	SourceUniqueID = E.ID
	,SourceSpellNo = cast(E.HospSpellID as varchar(100))
	,EventTypeCode = cast(E.EventTypeID as varchar(20))
	,SourcePatientNo = HS.Patient
	,AdmissionTime = HS.AdmitDate
	,DischargeTime = HS.DischTime
	,EventDate = -- Derive according to EventType
		case E.EventTypeID
			when 1 then Details.value('(/FallIncident/Fall_TS)[1]', 'datetime')
			when 5 then Details.value('(/VTECondition/Diagnosis_TS)[1]', 'datetime')
			when 8 then Details.value('(/PressureUlcerCondition/Identified_TS)[1]', 'datetime')
			when 2 then Details.value('(/CatheterIntervention/Inserted_TS)[1]', 'datetime')
			when 4 then Details.value('(/VTEAssessment/AssessmentCompleted_TS)[1]', 'datetime')
			else null
		end
	,Details

from [$(BedmanTest)].dbo.Event E

left join [$(BedmanTest)].dbo.HospSpell HS
on E.HospSpellID = HS.SpellID

where HS.AdmitDate BETWEEN @fromDate AND @toDate
/*
MOVE THIS TO BuildWrkDataset

and -- Qualify each EventType to take only records involving actual harm or disease
(
	(
		-- fall incurring actual harm
		E.EventTypeID = 1 
		and details.value('(/FallIncident/InitialSeverity)[1]', 'int') > 1
	) 
	or
	(
		-- VTE requiring medication
		E.EventTypeID = 5 
		and details.value('(/VTECondition/MedicationRequired)[1]', 'bit') = 1
	)	
	or
	(
		-- Pressure ulcer
		E.EventTypeID = 8 
		and details.value('(/PressureUlcerCondition/Category)[1]', 'int') > 1
		and datediff(day, HS.AdmitDate, details.value('(/PressureUlcerCondition/Identified_TS)[1]', 'datetime')) > 3
	)
	or
	(
		-- catheter inducing UTI
		E.EventTypeID = 2  -- catheter
		
		and exists
		(
			-- UTI symptoms observed within catheter period + 3 days
			select 1 
			from [$(BedmanTest)].dbo.Event E1 
			where E1.HospSpellID = E.HospSpellID
			and E1.EventTypeID = 3 -- UTI symptom
			and E1.details.value('(/UTISymptom/UTISymptonsObserved_TS)[1]', 'datetime')
				between E.details.value('(/CatheterIntervention/Inserted_TS)[1]', 'datetime')
				and dateadd(day,3,E.details.value('(/CatheterIntervention/Removed_TS)[1]', 'datetime'))
		)
		
		and exists
		(
			-- positive UTI test result within catheter period + 3 days
			select 1 
			from [$(BedmanTest)].dbo.Event E1 
			where E1.HospSpellID = E.HospSpellID
			and E1.EventTypeID = 9 -- UTI test
			and E1.details.value('(/UTITest/TestResultsPositive)[1]', 'bit') = 1 -- positive result 
			and E1.details.value('(/UTITest/UrinaryTestResultsSent_TS)[1]', 'datetime')
				between E.details.value('(/CatheterIntervention/Inserted_TS)[1]', 'datetime')
				and dateadd(day,3,E.details.value('(/CatheterIntervention/Removed_TS)[1]', 'datetime'))
		)
		
		and exists
		(
			-- UTI treatment starting within catheter period + 3 days
			select 1 
			from [$(BedmanTest)].dbo.Event E1 
			where E1.HospSpellID = E.HospSpellID
			and E1.EventTypeID = 10 -- UTI medication
			and E1.details.value('(/UTIMedication/TreatmentStarted_TS)[1]', 'datetime')
				between E.details.value('(/CatheterIntervention/Inserted_TS)[1]', 'datetime')
				and dateadd(day,3,E.details.value('(/CatheterIntervention/Removed_TS)[1]', 'datetime'))
		)		
	) 
)
*/

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

-- Update extract status
if @RowsInserted != 0
	update Utility.Parameter set NumericValue = 1 where Parameter = 'EXTRACTSTATUSBEDMANEVENT'

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime








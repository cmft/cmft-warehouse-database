﻿CREATE    procedure [ETL].[LoadRADStatus] 
(
	@ImportDateTimeStamp datetime
)

AS

	/******************************************************************************
	**  Name: LoadRADStatus
	**  Purpose: 
	**
	**  Import procedure for Radiology ETL.Status table rows 
	**
	**	Called by LoadRAD
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:     Author:      Description:
	** --------  ------------ ---------------------------------------------------- 
	** 16.05.11    MH         Was deleting rows from ETL.Status based on TLoadRADStatus rows that  
	**                        were not part of the current process time stamp
	** 08.06.11    MH         Place a check on the start and end dates range, to prevent
	**                        a corrupt date from deleting rows from the main table
	** 26.06.12    MH         Altered for CMFT environment
	******************************************************************************/

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @CensusDate smalldatetime
declare @InterfaceCode char(5)
declare @Process varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime
declare @LoadStartDateString varchar(20)
declare @LoadEndDateString varchar(20)


select @Process = 'LoadRADStatus'

select @StartTime = getdate()


select
	 @LoadStartDate = MIN(StatusDate) 
	,@LoadEndDate = MAX(StatusDate) 
from
	ETL.TLoadRADStatus
where
	ImportDateTimeStamp = @ImportDateTimeStamp


DELETE FROM RAD.Status 
WHERE	
--	Status.StatusDate between @LoadStartDate and @LoadEndDate
--OR
	EXISTS (
		SELECT
			1
		FROM
			ETL.TLoadRADStatus
		WHERE
			Status.SourceUniqueID = TLoadRADStatus.SourceUniqueID
		AND TLoadRADStatus.ImportDateTimeStamp = @ImportDateTimeStamp
	)

SELECT @RowsDeleted = @@ROWCOUNT

INSERT INTO RAD.Status
(
	 SourceUniqueID
	,ExamSourceUniqueID
	,EventSourceUniqueID
	,SourcePatientNo
	,SiteCode
	,ExamCode
	,CurrentStatusFlag
	,StatusCode
	,StatusCategoryCode
	,StatusTypeCode
	,StatusDate
	,StatusTime
	,StatusCodeAtEndDate
	,StatusCategoryCodeAtEndDate
	,StatysTypeCodeAtEndDate
	,StartDate
	,StartTime
	,EndDate
	,EndTime
	,LogicalDeleteFlag
	,Description
	,OtherCode
	,UserID
	,Created
	,ByWhom
)


SELECT
	 SourceUniqueID
	,ExamSourceUniqueID
	,EventSourceUniqueID
	,SourcePatientNo
	,SiteCode
	,ExamCode
	,CurrentStatusFlag
	,StatusCode
	,StatusCategoryCode
	,StatusTypeCode
	,StatusDate
	,StatusTime
	,StatusCodeAtEndDate
	,StatusCategoryCodeAtEndDate
	,StatysTypeCodeAtEndDate
	,StartDate
	,StartTime
	,EndDate
	,EndTime
	,LogicalDeleteFlag
	,Description
	,OtherCode
	,UserID

	,Created = getdate()
	,ByWhom = system_user
FROM
	ETL.TLoadRADStatus
where
	ImportDateTimeStamp = @ImportDateTimeStamp


select @RowsInserted = @@ROWCOUNT


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats =
	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Net change '  + CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + 
	', Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

exec Utility.WriteAuditLogEvent @Process , @Stats,  @StartTime

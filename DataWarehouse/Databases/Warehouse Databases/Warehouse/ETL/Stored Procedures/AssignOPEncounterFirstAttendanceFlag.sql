﻿CREATE procedure [ETL].[AssignOPEncounterFirstAttendanceFlag] as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


--override source categorisation

update OP.Encounter
set	
	FirstAttendanceFlag =
		case
		when Encounter.IsWardAttender = 1 and Encounter.FirstAttendanceFlag = 0 then 2
		when exists --there has been a previous attendance
			(
			select
				1
			from
				OP.Encounter PreviousAttendance

			inner join EntityLookup Attend
			on	Attend.EntityTypeCode = 'ATTENDANCEOUTCOMEATTENDED'
			and	Attend.EntityCode = PreviousAttendance.AttendanceOutcomeCode

			where
				PreviousAttendance.SourcePatientNo = Encounter.SourcePatientNo
			and	PreviousAttendance.SourceEncounterNo = Encounter.SourceEncounterNo
			and	PreviousAttendance.AppointmentTime < Encounter.AppointmentTime
			--and	PreviousAttendance.AttendanceOutcomeCode not in ('CND', 'DNA' , 'H' , 'P' , 'NR' , 'NATT')
			)
		then '2'
		else '1'
		end
from
	OP.Encounter

inner join ETL.TImportOPEncounter
on	TImportOPEncounter.SourceUniqueID = Encounter.SourceUniqueID


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

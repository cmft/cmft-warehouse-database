﻿
CREATE proc [ETL].[ExtractDawnAnticoagulationEncounter]
(
@fromDate date 
,@toDate date
)

as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

insert into ETL.TImportAnticoagulationEncounter
(
	SourceUniqueID
	,NHSNumber
	,PatientSurname
	,PatientForename
	,DateOfBirth
	,SexCode
	,Postcode
	,ClinicID
	,TreatmentStartDate
	,TestDate
	,Result
	,Dose
	,DiagnosisID
	,DiagnosisDate
	,CommissionerID
	,GpPracticeCode
)

select
	SourceUniqueID 
	,NHSNumber
	,PatientSurname
	,PatientForename 
	,DateOfBirth 
	,SexCode
	,Postcode 
	,ClinicID 
	,TreatmentStartDate
	,TestDate 
	,Result
	,Dose 
	,DiagnosisID
	,DiagnosisDate 
	,CommissionerID
	,GpPracticeCode
from
	(
	select --top 100
		SourceUniqueID = Treatment.pkiTreatmentID
		,NHSNumber = Patient.cNationalNo
		,PatientSurname = Patient.cLastName 
		,PatientForename = Patient.cFirstName 
		,DateOfBirth = cast(Patient.dDOB as date)
		,SexCode = Patient.cSex
		,Postcode = nullif(Patient.cPostCode,'') 
		,ClinicID = Clinic.pkiClinicID
		,TreatmentStartDate = TreatmentPlan.dStartDate
		,TestDate = cast(Treatment.dINRDate as date)
		,Result = Treatment.nINR
		,Dose = Treatment.nDose
		,DiagnosisID = fkiDiagnosisID
		,DiagnosisDate = cast(dDiagnosisDate as date)
		,CommissionerID = HealthAuthority.pkiHealthAuthorityID
		,GpPracticeCode = HCProfessional.cLocalNumber
	from
		[$(Dawn)].dbo.Patient

	inner join [$(Dawn)].dbo.TreatmentPlan
	on	TreatmentPlan.fkiPatientID = Patient.pkiPatientID

	inner join [$(Dawn)].dbo.Regime
	on	Regime.pkiRegimeID = TreatmentPlan.fkiRegimeID

	inner join [$(Dawn)].dbo.TargetRange
	on	TargetRange.pkiTargetRangeID = TreatmentPlan.fkiTargetRangeID

	inner join [$(Dawn)].dbo.PatientDiagnosis 
	on	PatientDiagnosis.pkiPatientDiagnosisID = TreatmentPlan.fkiPrimaryDiagnosisID

	inner join [$(Dawn)].dbo.Diagnosis 
	on	Diagnosis.pkiDiagnosisID = PatientDiagnosis.fkiDiagnosisID 

	inner join [$(Dawn)].dbo.Treatment
	on	Treatment.fkiTreatmentPlanID = TreatmentPlan.pkiTreatmentPlanID

	left join [$(Dawn)].dbo.HCProfessional
	on	HCProfessional.pkiHCProfessionalID = TreatmentPlan.fkiReferringGPID 

	left join  [$(Dawn)].dbo.Organisation 
	on	Organisation.pkiOrganisationID = HCProfessional.fkiOrganisationID

	left join [$(Dawn)].dbo.HealthAuthority
	on	HealthAuthority.pkiHealthAuthorityID = Organisation.fkiHealthAuthorityID 

	left join [$(Dawn)].dbo.clinic Clinic
	on	Clinic.pkiClinicID = Treatment.fkiClinicID

	where
		Treatment.cStatus = 'DoseAuthorised'
	and	Treatment.dINRDate between @fromDate and @toDate
	
	) Encounter



--	dbo.Clinic 
--RIGHT OUTER JOIN dbo.HealthAuthority RIGHT OUTER JOIN
--                      dbo.Organisation RIGHT OUTER JOIN
--                      dbo.HCProfessional RIGHT OUTER JOIN
--                      dbo.Treatment RIGHT OUTER JOIN
--                      dbo.Diagnosis RIGHT OUTER JOIN
--                      dbo.PatientDiagnosis RIGHT OUTER JOIN
--                      dbo.TargetRange RIGHT OUTER JOIN
--                      dbo.Regime RIGHT OUTER JOIN
--                      dbo.TreatmentPlan RIGHT OUTER JOIN
--                      dbo.Patient ON dbo.TreatmentPlan.fkiPatientID = dbo.Patient.pkiPatientID ON dbo.Regime.pkiRegimeID = dbo.TreatmentPlan.fkiRegimeID ON 
--                      dbo.TargetRange.pkiTargetRangeID = dbo.TreatmentPlan.fkiTargetRangeID ON 
--                      dbo.PatientDiagnosis.pkiPatientDiagnosisID = dbo.TreatmentPlan.fkiPrimaryDiagnosisID ON 
--                      dbo.Diagnosis.pkiDiagnosisID = dbo.PatientDiagnosis.fkiDiagnosisID ON 
--                      dbo.Treatment.fkiTreatmentPlanID = dbo.TreatmentPlan.pkiTreatmentPlanID ON 
--                      dbo.HCProfessional.pkiHCProfessionalID = dbo.TreatmentPlan.fkiReferringGPID ON 
--                      dbo.Organisation.pkiOrganisationID = dbo.HCProfessional.fkiOrganisationID ON 
--                      dbo.HealthAuthority.pkiHealthAuthorityID = dbo.Organisation.fkiHealthAuthorityID ON dbo.Clinic.pkiClinicID = dbo.Treatment.fkiClinicID

--WHERE     (Treatment.pkiTreatmentID IN
--                          (SELECT     pkiTreatmentID
--                            FROM         [$(Dawn)].dbo.Treatment
--                            WHERE      fkiTreatmentPlanID = pkiTreatmentPlanID AND Treatment.cStatus = 'DoseAuthorised'))


select @RowsInserted = @@rowcount

-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime


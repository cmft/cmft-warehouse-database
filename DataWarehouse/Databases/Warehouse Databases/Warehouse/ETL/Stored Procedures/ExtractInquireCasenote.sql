﻿CREATE procedure [ETL].[ExtractInquireCasenote]

as

insert into ETL.TImportPASCasenote 
(
	 Allocated
	,Comment
	,InternalPatientNumber
	,Location
	,PtCasenoteIdPhyskey
	,PtCsNtLocDat
	,Status
	,Withdrawn
)

SELECT
	 Allocated = left(Allocated , 12) 
	,Comment
	,InternalPatientNumber
	,Location
	,PtCasenoteIdPhyskey
	,PtCsNtLocDat = left(PtCsNtLocDat , 12) 
	,Status
	,Withdrawn = left(Withdrawn , 20) 
FROM
	[$(PAS)].Inquire.CASENOTEDETS Casenote

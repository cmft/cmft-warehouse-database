﻿
CREATE proc [ETL].[LoadNeonatalEncounter]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime


select @FromDate = (select min(ModifiedTime) from ETL.TLoadNeonatalEncounter)
select @ToDate = (select max(ModifiedTime) from ETL.TLoadNeonatalEncounter)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Neonatal.Encounter target
using
	(
	select
		EpisodeID
		,NHSNumber
		,CasenoteNumber
		,SourceUniqueID
		,CareLocationID
		,EpisodeTypeCode
		,Surname
		,Forename
		,SexCode
		,BirthTime
		,GestationWeeks
		,GestationDays
		,BirthWeight
		,BirthLength
		,BirthHeadCircumference
		,BirthOrder
		,FetusNumber
		,BirthSummary
		,EpisodeNumber
		,AdmissionDate
		,AdmissionTime
		,AdmissionFromSiteCode
		,AdmissionSourceCode
		,Readmission
		,AdmissionTypeCode
		,SourceOfReferralCode
		,AdmissionReasonCode
		,ProviderCode
		,NetworkID
		,ConsultationWithParents
		,AdmissionTemperatureMeasured
		,AdmissionTemperature
		,AdmissionTemperatureTime
		,AdmissionBloodPressure
		,AdmissionHeartRate
		,AdmissionRespiratoryRate
		,AdmissionSaO2
		,AdmissionBloodGlucose
		,AdmissionWeight
		,AdmissionHeadCircumference
		,TemperatureNotRecordable
		,DischargeDate
		,DischargeTime
		,DischargeDestinationCode
		,DischargeHospitalCode
		,DischargeDestinationWardCode
		,DischargeWeight
		,DischargeHeadCircumference
		,DischargeMilkCode
		,DischargeFeedingCode
		,HomeTubeFeeding
		,DischargeOxygen
		,TransferReasonCode
		,TransferNetworkCode
		,CauseOfDeathCode
		,PostMortemConsent
		,PostMortemCode
		,PostMortemConfirmedNECCode
		,EpisodeSummary
		,VentilationDays
		,CPAPDays
		,OxygenDays
		,OxygenDaysNoCPAPVentilation
		,OxygenLastGestation
		,OxygenLastTime
		,IntensiveCareDays2001
		,HighDependancyCareDays2001
		,SpecialCareDays2001
		,IntensiveCareDays2011
		,HighDependancyCareDays2011
		,SpecialCareDays2011
		,NormalCareDays2011
		,IntensiveCareHRGDays
		,HighDependancyHRGDays
		,SpecialCareNoCarerResidentHRGDays
		,SpecialCareWithCarerResidentHRGDays
		,NormalCareHRGDays
		,NeonatalUnitDays
		,TransitionalCareDays
		,PostnatalWardDays
		,OtherObstetricAreaDays
		,LocnNNUPortion
		,LocnTCPoriton
		,LocnPNWPortion
		,DrugsDuringStay
		,DiagnosisDuringStay
		,MotherNHSNumber
		,MotherCasenoteNumber
		,MotherSurname
		,MotherForename
		,MotherReligionCode
		,MotherMaritalStatusCode
		,MotherOccupation
		,MotherBloodGroup
		,MotherDateOfBirth
		,MotherPostCode
		,MotherEthnicCategoryCode
		,MotherHaemoglobinoapthyCode
		,MotherMedicalProblemCode
		,MotherDiabetesCode
		,MotherHepatitisBCode
		,MotherHepatitisBHighRiskCode
		,MotherHIVCode
		,MotherRubellaScreen
		,MotherSyphilisCode
		,MotherHepatitisCCode
		,MotherMotherHepatitisCScreen
		,MotherSyphilisVDRLScreenCode
		,MotherSyphilisTPHAScreenCode
		,MaternalPyrexiaInLabour
		,MotherIntrapartumAntibiotics
		,MeconiumStainedLiquor
		,MembraneRuptureTime
		,MotherAntenatalScanCode
		,ParentsConsanguinous
		,MotherDrugAbuseCode
		,MotherSmoker
		,MotherCigarettesPerDay
		,MotherAlcoholUseCode
		,MotherPregnancyProblemCode
		,LastMenstrualPeriodDate
		,ExpectedDateOfDelivery
		,PreviousPregnancies
		,FatherOccupation
		,FatherAge
		,FatherEthnicCategoryCode
		,CalculatedGestationWeeks
		,CalculatedGestationDays
		,BookingLocationCode
		,AntenatalSteroidCode
		,AntenatalSteroid
		,AntenatalSteroidCourseCode
		,LabourOnsetCode
		,LabourPresentationCode
		,LabourDeliveryCode
		,PlaceOfBirthCode
		,LabourDrugCode
		,StaffPresentAtResuscitation
		,AgparScoreOneMinute
		,AgparScoreFiveMinutes
		,AgparScoreTenMinutes
		,BloodTypeCode
		,CRIBIIScore
		,Outcome
		,VitaminKCode
		,VitaminKRouteCode
		,CordArterialpH
		,CordVenouspH
		,CordPcO2Arterial
		,CordPcO2Venous
		,CordArterialBE
		,CordVenousBE
		,CordClampingCode
		,CordClampingTimeMinute
		,CordClampingTimeSecond
		,CordStripping
		,ResuscitationCode
		,ResuscitationSurfactantCode
		,FirstGaspCode
		,SpontaneousRespirationCode
		,OffensiveLiquor
		,GPCode
		,GPPracticeCode
		,SeizureCode
		,HIEGradeCode
		,AnticonvulsantDrug
		,Pneumothorax
		,NecrotisingEnterocolitis
		,NeonatalAbstinence
		,ROPScreenDate
		,ROPSurgeryDate
		,Dexamethasone
		,PDAIndomethacin
		,PDAIbuprofen
		,PDASurgery
		,PDADischarge
		,UACDate
		,UVCDate
		,LongLineTime
		,PeripheralArterialLineDate
		,SurgicalLineDate
		,ParenteralNutritionDays
		,FirstHeadScanDate
		,FirstHeadScanResult
		,LastHeadScanDate
		,LastHeadScanResult
		,CongenitalAnomaly
		,VPShuntDate
		,FirstBloodCultureDate
		,FirstBloodCultureResult
		,FirstCSFCultureDate
		,FirstCSFCultureResult
		,FirstUrineCultureDate
		,FirstUrineCultureResult
		,ExchangeTransfusion
		,Tracheostomy
		,PulmonaryVasodilatorDate
		,PulmonaryVasodilatorDrug
		,Inotrope
		,FirstInotropeDate
		,PeritonealDialysis
		,DischargeApnoeaCardioSat
		,Gastroschisis
		,Cooled
		,Consultant
		,LastUpdatedTime
		,ModifiedTime
		,InterfaceCode
	from
		ETL.TLoadNeonatalEncounter

	) source
	on	source.EpisodeID = target.EpisodeID
	
	when not matched by source
	and target.ModifiedTime between @FromDate and @ToDate

	then delete

	when not matched
	then
		insert
			(
			 EpisodeID
			,NHSNumber
			,CasenoteNumber
			,SourceUniqueID
			,CareLocationID
			,EpisodeTypeCode
			,Surname
			,Forename
			,SexCode
			,BirthTime
			,GestationWeeks
			,GestationDays
			,BirthWeight
			,BirthLength
			,BirthHeadCircumference
			,BirthOrder
			,FetusNumber
			,BirthSummary
			,EpisodeNumber
			,AdmissionDate
			,AdmissionTime
			,AdmissionFromSiteCode
			,AdmissionSourceCode
			,Readmission
			,AdmissionTypeCode
			,SourceOfReferralCode
			,AdmissionReasonCode
			,ProviderCode
			,NetworkID
			,ConsultationWithParents
			,AdmissionTemperatureMeasured
			,AdmissionTemperature
			,AdmissionTemperatureTime
			,AdmissionBloodPressure
			,AdmissionHeartRate
			,AdmissionRespiratoryRate
			,AdmissionSaO2
			,AdmissionBloodGlucose
			,AdmissionWeight
			,AdmissionHeadCircumference
			,TemperatureNotRecordable
			,DischargeDate
			,DischargeTime
			,DischargeDestinationCode
			,DischargeHospitalCode
			,DischargeDestinationWardCode
			,DischargeWeight
			,DischargeHeadCircumference
			,DischargeMilkCode
			,DischargeFeedingCode
			,HomeTubeFeeding
			,DischargeOxygen
			,TransferReasonCode
			,TransferNetworkCode
			,CauseOfDeathCode
			,PostMortemConsent
			,PostMortemCode
			,PostMortemConfirmedNECCode
			,EpisodeSummary
			,VentilationDays
			,CPAPDays
			,OxygenDays
			,OxygenDaysNoCPAPVentilation
			,OxygenLastGestation
			,OxygenLastTime
			,IntensiveCareDays2001
			,HighDependancyCareDays2001
			,SpecialCareDays2001
			,IntensiveCareDays2011
			,HighDependancyCareDays2011
			,SpecialCareDays2011
			,NormalCareDays2011
			,IntensiveCareHRGDays
			,HighDependancyHRGDays
			,SpecialCareNoCarerResidentHRGDays
			,SpecialCareWithCarerResidentHRGDays
			,NormalCareHRGDays
			,NeonatalUnitDays
			,TransitionalCareDays
			,PostnatalWardDays
			,OtherObstetricAreaDays
			,LocnNNUPortion
			,LocnTCPoriton
			,LocnPNWPortion
			,DrugsDuringStay
			,DiagnosisDuringStay
			,MotherNHSNumber
			,MotherCasenoteNumber
			,MotherSurname
			,MotherForename
			,MotherReligionCode
			,MotherMaritalStatusCode
			,MotherOccupation
			,MotherBloodGroup
			,MotherDateOfBirth
			,MotherPostCode
			,MotherEthnicCategoryCode
			,MotherHaemoglobinoapthyCode
			,MotherMedicalProblemCode
			,MotherDiabetesCode
			,MotherHepatitisBCode
			,MotherHepatitisBHighRiskCode
			,MotherHIVCode
			,MotherRubellaScreen
			,MotherSyphilisCode
			,MotherHepatitisCCode
			,MotherMotherHepatitisCScreen
			,MotherSyphilisVDRLScreenCode
			,MotherSyphilisTPHAScreenCode
			,MaternalPyrexiaInLabour
			,MotherIntrapartumAntibiotics
			,MeconiumStainedLiquor
			,MembraneRuptureTime
			,MotherAntenatalScanCode
			,ParentsConsanguinous
			,MotherDrugAbuseCode
			,MotherSmoker
			,MotherCigarettesPerDay
			,MotherAlcoholUseCode
			,MotherPregnancyProblemCode
			,LastMenstrualPeriodDate
			,ExpectedDateOfDelivery
			,PreviousPregnancies
			,FatherOccupation
			,FatherAge
			,FatherEthnicCategoryCode
			,CalculatedGestationWeeks
			,CalculatedGestationDays
			,BookingLocationCode
			,AntenatalSteroidCode
			,AntenatalSteroid
			,AntenatalSteroidCourseCode
			,LabourOnsetCode
			,LabourPresentationCode
			,LabourDeliveryCode
			,PlaceOfBirthCode
			,LabourDrugCode
			,StaffPresentAtResuscitation
			,AgparScoreOneMinute
			,AgparScoreFiveMinutes
			,AgparScoreTenMinutes
			,BloodTypeCode
			,CRIBIIScore
			,Outcome
			,VitaminKCode
			,VitaminKRouteCode
			,CordArterialpH
			,CordVenouspH
			,CordPcO2Arterial
			,CordPcO2Venous
			,CordArterialBE
			,CordVenousBE
			,CordClampingCode
			,CordClampingTimeMinute
			,CordClampingTimeSecond
			,CordStripping
			,ResuscitationCode
			,ResuscitationSurfactantCode
			,FirstGaspCode
			,SpontaneousRespirationCode
			,OffensiveLiquor
			,GPCode
			,GPPracticeCode
			,SeizureCode
			,HIEGradeCode
			,AnticonvulsantDrug
			,Pneumothorax
			,NecrotisingEnterocolitis
			,NeonatalAbstinence
			,ROPScreenDate
			,ROPSurgeryDate
			,Dexamethasone
			,PDAIndomethacin
			,PDAIbuprofen
			,PDASurgery
			,PDADischarge
			,UACDate
			,UVCDate
			,LongLineTime
			,PeripheralArterialLineDate
			,SurgicalLineDate
			,ParenteralNutritionDays
			,FirstHeadScanDate
			,FirstHeadScanResult
			,LastHeadScanDate
			,LastHeadScanResult
			,CongenitalAnomaly
			,VPShuntDate
			,FirstBloodCultureDate
			,FirstBloodCultureResult
			,FirstCSFCultureDate
			,FirstCSFCultureResult
			,FirstUrineCultureDate
			,FirstUrineCultureResult
			,ExchangeTransfusion
			,Tracheostomy
			,PulmonaryVasodilatorDate
			,PulmonaryVasodilatorDrug
			,Inotrope
			,FirstInotropeDate
			,PeritonealDialysis
			,DischargeApnoeaCardioSat
			,Gastroschisis
			,Cooled
			,Consultant
			,LastUpdatedTime
			,ModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.EpisodeID
			,source.NHSNumber
			,source.CasenoteNumber
			,source.SourceUniqueID
			,source.CareLocationID
			,source.EpisodeTypeCode
			,source.Surname
			,source.Forename
			,source.SexCode
			,source.BirthTime
			,source.GestationWeeks
			,source.GestationDays
			,source.BirthWeight
			,source.BirthLength
			,source.BirthHeadCircumference
			,source.BirthOrder
			,source.FetusNumber
			,source.BirthSummary
			,source.EpisodeNumber
			,source.AdmissionDate
			,source.AdmissionTime
			,source.AdmissionFromSiteCode
			,source.AdmissionSourceCode
			,source.Readmission
			,source.AdmissionTypeCode
			,source.SourceOfReferralCode
			,source.AdmissionReasonCode
			,source.ProviderCode
			,source.NetworkID
			,source.ConsultationWithParents
			,source.AdmissionTemperatureMeasured
			,source.AdmissionTemperature
			,source.AdmissionTemperatureTime
			,source.AdmissionBloodPressure
			,source.AdmissionHeartRate
			,source.AdmissionRespiratoryRate
			,source.AdmissionSaO2
			,source.AdmissionBloodGlucose
			,source.AdmissionWeight
			,source.AdmissionHeadCircumference
			,source.TemperatureNotRecordable
			,source.DischargeDate
			,source.DischargeTime
			,source.DischargeDestinationCode
			,source.DischargeHospitalCode
			,source.DischargeDestinationWardCode
			,source.DischargeWeight
			,source.DischargeHeadCircumference
			,source.DischargeMilkCode
			,source.DischargeFeedingCode
			,source.HomeTubeFeeding
			,source.DischargeOxygen
			,source.TransferReasonCode
			,source.TransferNetworkCode
			,source.CauseOfDeathCode
			,source.PostMortemConsent
			,source.PostMortemCode
			,source.PostMortemConfirmedNECCode
			,source.EpisodeSummary
			,source.VentilationDays
			,source.CPAPDays
			,source.OxygenDays
			,source.OxygenDaysNoCPAPVentilation
			,source.OxygenLastGestation
			,source.OxygenLastTime
			,source.IntensiveCareDays2001
			,source.HighDependancyCareDays2001
			,source.SpecialCareDays2001
			,source.IntensiveCareDays2011
			,source.HighDependancyCareDays2011
			,source.SpecialCareDays2011
			,source.NormalCareDays2011
			,source.IntensiveCareHRGDays
			,source.HighDependancyHRGDays
			,source.SpecialCareNoCarerResidentHRGDays
			,source.SpecialCareWithCarerResidentHRGDays
			,source.NormalCareHRGDays
			,source.NeonatalUnitDays
			,source.TransitionalCareDays
			,source.PostnatalWardDays
			,source.OtherObstetricAreaDays
			,source.LocnNNUPortion
			,source.LocnTCPoriton
			,source.LocnPNWPortion
			,source.DrugsDuringStay
			,source.DiagnosisDuringStay
			,source.MotherNHSNumber
			,source.MotherCasenoteNumber
			,source.MotherSurname
			,source.MotherForename
			,source.MotherReligionCode
			,source.MotherMaritalStatusCode
			,source.MotherOccupation
			,source.MotherBloodGroup
			,source.MotherDateOfBirth
			,source.MotherPostCode
			,source.MotherEthnicCategoryCode
			,source.MotherHaemoglobinoapthyCode
			,source.MotherMedicalProblemCode
			,source.MotherDiabetesCode
			,source.MotherHepatitisBCode
			,source.MotherHepatitisBHighRiskCode
			,source.MotherHIVCode
			,source.MotherRubellaScreen
			,source.MotherSyphilisCode
			,source.MotherHepatitisCCode
			,source.MotherMotherHepatitisCScreen
			,source.MotherSyphilisVDRLScreenCode
			,source.MotherSyphilisTPHAScreenCode
			,source.MaternalPyrexiaInLabour
			,source.MotherIntrapartumAntibiotics
			,source.MeconiumStainedLiquor
			,source.MembraneRuptureTime
			,source.MotherAntenatalScanCode
			,source.ParentsConsanguinous
			,source.MotherDrugAbuseCode
			,source.MotherSmoker
			,source.MotherCigarettesPerDay
			,source.MotherAlcoholUseCode
			,source.MotherPregnancyProblemCode
			,source.LastMenstrualPeriodDate
			,source.ExpectedDateOfDelivery
			,source.PreviousPregnancies
			,source.FatherOccupation
			,source.FatherAge
			,source.FatherEthnicCategoryCode
			,source.CalculatedGestationWeeks
			,source.CalculatedGestationDays
			,source.BookingLocationCode
			,source.AntenatalSteroidCode
			,source.AntenatalSteroid
			,source.AntenatalSteroidCourseCode
			,source.LabourOnsetCode
			,source.LabourPresentationCode
			,source.LabourDeliveryCode
			,source.PlaceOfBirthCode
			,source.LabourDrugCode
			,source.StaffPresentAtResuscitation
			,source.AgparScoreOneMinute
			,source.AgparScoreFiveMinutes
			,source.AgparScoreTenMinutes
			,source.BloodTypeCode
			,source.CRIBIIScore
			,source.Outcome
			,source.VitaminKCode
			,source.VitaminKRouteCode
			,source.CordArterialpH
			,source.CordVenouspH
			,source.CordPcO2Arterial
			,source.CordPcO2Venous
			,source.CordArterialBE
			,source.CordVenousBE
			,source.CordClampingCode
			,source.CordClampingTimeMinute
			,source.CordClampingTimeSecond
			,source.CordStripping
			,source.ResuscitationCode
			,source.ResuscitationSurfactantCode
			,source.FirstGaspCode
			,source.SpontaneousRespirationCode
			,source.OffensiveLiquor
			,source.GPCode
			,source.GPPracticeCode
			,source.SeizureCode
			,source.HIEGradeCode
			,source.AnticonvulsantDrug
			,source.Pneumothorax
			,source.NecrotisingEnterocolitis
			,source.NeonatalAbstinence
			,source.ROPScreenDate
			,source.ROPSurgeryDate
			,source.Dexamethasone
			,source.PDAIndomethacin
			,source.PDAIbuprofen
			,source.PDASurgery
			,source.PDADischarge
			,source.UACDate
			,source.UVCDate
			,source.LongLineTime
			,source.PeripheralArterialLineDate
			,source.SurgicalLineDate
			,source.ParenteralNutritionDays
			,source.FirstHeadScanDate
			,source.FirstHeadScanResult
			,source.LastHeadScanDate
			,source.LastHeadScanResult
			,source.CongenitalAnomaly
			,source.VPShuntDate
			,source.FirstBloodCultureDate
			,source.FirstBloodCultureResult
			,source.FirstCSFCultureDate
			,source.FirstCSFCultureResult
			,source.FirstUrineCultureDate
			,source.FirstUrineCultureResult
			,source.ExchangeTransfusion
			,source.Tracheostomy
			,source.PulmonaryVasodilatorDate
			,source.PulmonaryVasodilatorDrug
			,source.Inotrope
			,source.FirstInotropeDate
			,source.PeritonealDialysis
			,source.DischargeApnoeaCardioSat
			,source.Gastroschisis
			,source.Cooled
			,source.Consultant
			,source.LastUpdatedTime
			,source.ModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(cast(target.EpisodeID as varchar(100)), '') = isnull(cast(source.EpisodeID as varchar(100)), '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(cast(target.CareLocationID as varchar(100)),'') = isnull(cast(source.CareLocationID as varchar(100)),'')
		and isnull(target.EpisodeTypeCode, '') = isnull(source.EpisodeTypeCode, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.Forename, '') = isnull(source.Forename, '')
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.BirthTime, getdate()) = isnull(source.BirthTime, getdate())
		and isnull(target.GestationWeeks, 0) = isnull(source.GestationWeeks, 0)
		and isnull(target.GestationDays, 0) = isnull(source.GestationDays, 0)
		and isnull(target.BirthWeight, 0) = isnull(source.BirthWeight, 0)
		and isnull(target.BirthLength, 0) = isnull(source.BirthLength, 0)
		and isnull(target.BirthHeadCircumference, 0) = isnull(source.BirthHeadCircumference, 0)
		and isnull(target.BirthOrder, 0) = isnull(source.BirthOrder, 0)
		and isnull(target.FetusNumber, 0) = isnull(source.FetusNumber, 0)
		and isnull(target.BirthSummary, '') = isnull(source.BirthSummary, '')
		and isnull(target.EpisodeNumber, 0) = isnull(source.EpisodeNumber, 0)
		and isnull(target.AdmissionDate, getdate()) = isnull(source.AdmissionDate, getdate())
		and isnull(target.AdmissionTime, getdate()) = isnull(source.AdmissionTime, getdate())
		and isnull(target.AdmissionFromSiteCode, '') = isnull(source.AdmissionFromSiteCode, '')
		and isnull(target.AdmissionSourceCode, 0) = isnull(source.AdmissionSourceCode, 0)
		and isnull(target.Readmission, 0) = isnull(source.Readmission, 0)
		and isnull(target.AdmissionTypeCode, '') = isnull(source.AdmissionTypeCode, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.AdmissionReasonCode, '') = isnull(source.AdmissionReasonCode, '')
		and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
		and isnull(target.NetworkID, 0) = isnull(source.NetworkID, 0)
		and isnull(target.ConsultationWithParents, 0) = isnull(source.ConsultationWithParents, 0)
		and isnull(target.AdmissionTemperatureMeasured, 0) = isnull(source.AdmissionTemperatureMeasured, 0)
		and isnull(target.AdmissionTemperature, 0) = isnull(source.AdmissionTemperature, 0)
		and isnull(target.AdmissionTemperatureTime, getdate()) = isnull(source.AdmissionTemperatureTime, getdate())
		and isnull(target.AdmissionBloodPressure, 0) = isnull(source.AdmissionBloodPressure, 0)
		and isnull(target.AdmissionHeartRate, 0) = isnull(source.AdmissionHeartRate, 0)
		and isnull(target.AdmissionRespiratoryRate, 0) = isnull(source.AdmissionRespiratoryRate, 0)
		and isnull(target.AdmissionSaO2, 0) = isnull(source.AdmissionSaO2, 0)
		and isnull(target.AdmissionBloodGlucose, 0) = isnull(source.AdmissionBloodGlucose, 0)
		and isnull(target.AdmissionWeight, 0) = isnull(source.AdmissionWeight, 0)
		and isnull(target.AdmissionHeadCircumference, 0) = isnull(source.AdmissionHeadCircumference, 0)
		and isnull(target.TemperatureNotRecordable, 0) = isnull(source.TemperatureNotRecordable, 0)
		and isnull(target.DischargeDate, getdate()) = isnull(source.DischargeDate, getdate())
		and isnull(target.DischargeTime, getdate()) = isnull(source.DischargeTime, getdate())
		and isnull(target.DischargeDestinationCode, '') = isnull(source.DischargeDestinationCode, '')
		and isnull(target.DischargeHospitalCode, '') = isnull(source.DischargeHospitalCode, '')
		and isnull(target.DischargeDestinationWardCode, '') = isnull(source.DischargeDestinationWardCode, '')
		and isnull(target.DischargeWeight, 0) = isnull(source.DischargeWeight, 0)
		and isnull(target.DischargeHeadCircumference, 0) = isnull(source.DischargeHeadCircumference, 0)
		and isnull(target.DischargeMilkCode, '') = isnull(source.DischargeMilkCode, '')
		and isnull(target.DischargeFeedingCode, '') = isnull(source.DischargeFeedingCode, '')
		and isnull(target.HomeTubeFeeding, 0) = isnull(source.HomeTubeFeeding, 0)
		and isnull(target.DischargeOxygen, 0) = isnull(source.DischargeOxygen, 0)
		and isnull(target.TransferReasonCode, 0) = isnull(source.TransferReasonCode, 0)
		and isnull(target.TransferNetworkCode, 0) = isnull(source.TransferNetworkCode, 0)
		and isnull(target.CauseOfDeathCode, '') = isnull(source.CauseOfDeathCode, '')
		and isnull(target.PostMortemConsent, 0) = isnull(source.PostMortemConsent, 0)
		and isnull(target.PostMortemCode, 0) = isnull(source.PostMortemCode, 0)
		and isnull(target.PostMortemConfirmedNECCode, 0) = isnull(source.PostMortemConfirmedNECCode, 0)
		and isnull(target.EpisodeSummary, '') = isnull(source.EpisodeSummary, '')
		and isnull(target.VentilationDays, 0) = isnull(source.VentilationDays, 0)
		and isnull(target.CPAPDays, 0) = isnull(source.CPAPDays, 0)
		and isnull(target.OxygenDays, 0) = isnull(source.OxygenDays, 0)
		and isnull(target.OxygenDaysNoCPAPVentilation, 0) = isnull(source.OxygenDaysNoCPAPVentilation, 0)
		and isnull(target.OxygenLastGestation, 0) = isnull(source.OxygenLastGestation, 0)
		and isnull(target.OxygenLastTime, getdate()) = isnull(source.OxygenLastTime, getdate())
		and isnull(target.IntensiveCareDays2001, 0) = isnull(source.IntensiveCareDays2001, 0)
		and isnull(target.HighDependancyCareDays2001, 0) = isnull(source.HighDependancyCareDays2001, 0)
		and isnull(target.SpecialCareDays2001, 0) = isnull(source.SpecialCareDays2001, 0)
		and isnull(target.IntensiveCareDays2011, 0) = isnull(source.IntensiveCareDays2011, 0)
		and isnull(target.HighDependancyCareDays2011, 0) = isnull(source.HighDependancyCareDays2011, 0)
		and isnull(target.SpecialCareDays2011, 0) = isnull(source.SpecialCareDays2011, 0)
		and isnull(target.NormalCareDays2011, 0) = isnull(source.NormalCareDays2011, 0)
		and isnull(target.IntensiveCareHRGDays, 0) = isnull(source.IntensiveCareHRGDays, 0)
		and isnull(target.HighDependancyHRGDays, 0) = isnull(source.HighDependancyHRGDays, 0)
		and isnull(target.SpecialCareNoCarerResidentHRGDays, 0) = isnull(source.SpecialCareNoCarerResidentHRGDays, 0)
		and isnull(target.SpecialCareWithCarerResidentHRGDays, 0) = isnull(source.SpecialCareWithCarerResidentHRGDays, 0)
		and isnull(target.NormalCareHRGDays, 0) = isnull(source.NormalCareHRGDays, 0)
		and isnull(target.NeonatalUnitDays, 0) = isnull(source.NeonatalUnitDays, 0)
		and isnull(target.TransitionalCareDays, 0) = isnull(source.TransitionalCareDays, 0)
		and isnull(target.PostnatalWardDays, 0) = isnull(source.PostnatalWardDays, 0)
		and isnull(target.OtherObstetricAreaDays, 0) = isnull(source.OtherObstetricAreaDays, 0)
		and isnull(target.LocnNNUPortion, 0) = isnull(source.LocnNNUPortion, 0)
		and isnull(target.LocnTCPoriton, 0) = isnull(source.LocnTCPoriton, 0)
		and isnull(target.LocnPNWPortion, 0) = isnull(source.LocnPNWPortion, 0)
		and isnull(target.DrugsDuringStay, '') = isnull(source.DrugsDuringStay, '')
		and isnull(target.DiagnosisDuringStay, '') = isnull(source.DiagnosisDuringStay, '')
		and isnull(target.MotherNHSNumber, '') = isnull(source.MotherNHSNumber, '')
		and isnull(target.MotherCasenoteNumber, '') = isnull(source.MotherCasenoteNumber, '')
		and isnull(target.MotherSurname, '') = isnull(source.MotherSurname, '')
		and isnull(target.MotherForename, '') = isnull(source.MotherForename, '')
		and isnull(target.MotherReligionCode, '') = isnull(source.MotherReligionCode, '')
		and isnull(target.MotherMaritalStatusCode, '') = isnull(source.MotherMaritalStatusCode, '')
		and isnull(target.MotherOccupation, '') = isnull(source.MotherOccupation, '')
		and isnull(target.MotherBloodGroup, '') = isnull(source.MotherBloodGroup, '')
		and isnull(target.MotherDateOfBirth, getdate()) = isnull(source.MotherDateOfBirth, getdate())
		and isnull(target.MotherPostCode, '') = isnull(source.MotherPostCode, '')
		and isnull(target.MotherEthnicCategoryCode, '') = isnull(source.MotherEthnicCategoryCode, '')
		and isnull(target.MotherHaemoglobinoapthyCode, '') = isnull(source.MotherHaemoglobinoapthyCode, '')
		and isnull(target.MotherMedicalProblemCode, '') = isnull(source.MotherMedicalProblemCode, '')
		and isnull(target.MotherDiabetesCode, 0) = isnull(source.MotherDiabetesCode, 0)
		and isnull(target.MotherHepatitisBCode, '') = isnull(source.MotherHepatitisBCode, '')
		and isnull(target.MotherHepatitisBHighRiskCode, '') = isnull(source.MotherHepatitisBHighRiskCode, '')
		and isnull(target.MotherHIVCode, '') = isnull(source.MotherHIVCode, '')
		and isnull(target.MotherRubellaScreen, '') = isnull(source.MotherRubellaScreen, '')
		and isnull(target.MotherSyphilisCode, '') = isnull(source.MotherSyphilisCode, '')
		and isnull(target.MotherHepatitisCCode, '') = isnull(source.MotherHepatitisCCode, '')
		and isnull(target.MotherMotherHepatitisCScreen, '') = isnull(source.MotherMotherHepatitisCScreen, '')
		and isnull(target.MotherSyphilisVDRLScreenCode, '') = isnull(source.MotherSyphilisVDRLScreenCode, '')
		and isnull(target.MotherSyphilisTPHAScreenCode, '') = isnull(source.MotherSyphilisTPHAScreenCode, '')
		and isnull(target.MaternalPyrexiaInLabour, 0) = isnull(source.MaternalPyrexiaInLabour, 0)
		and isnull(target.MotherIntrapartumAntibiotics, 0) = isnull(source.MotherIntrapartumAntibiotics, 0)
		and isnull(target.MeconiumStainedLiquor, 0) = isnull(source.MeconiumStainedLiquor, 0)
		and isnull(target.MembraneRuptureTime, getdate()) = isnull(source.MembraneRuptureTime, getdate())
		and isnull(target.MotherAntenatalScanCode, '') = isnull(source.MotherAntenatalScanCode, '')
		and isnull(target.ParentsConsanguinous, '') = isnull(source.ParentsConsanguinous, '')
		and isnull(target.MotherDrugAbuseCode, '') = isnull(source.MotherDrugAbuseCode, '')
		and isnull(target.MotherSmoker, 0) = isnull(source.MotherSmoker, 0)
		and isnull(target.MotherCigarettesPerDay, 0) = isnull(source.MotherCigarettesPerDay, 0)
		and isnull(target.MotherAlcoholUseCode, '') = isnull(source.MotherAlcoholUseCode, '')
		and isnull(target.MotherPregnancyProblemCode, '') = isnull(source.MotherPregnancyProblemCode, '')
		and isnull(target.LastMenstrualPeriodDate, getdate()) = isnull(source.LastMenstrualPeriodDate, getdate())
		and isnull(target.ExpectedDateOfDelivery, getdate()) = isnull(source.ExpectedDateOfDelivery, getdate())
		and isnull(target.PreviousPregnancies, 0) = isnull(source.PreviousPregnancies, 0)
		and isnull(target.FatherOccupation, '') = isnull(source.FatherOccupation, '')
		and isnull(target.FatherAge, 0) = isnull(source.FatherAge, 0)
		and isnull(target.FatherEthnicCategoryCode, '') = isnull(source.FatherEthnicCategoryCode, '')
		and isnull(target.CalculatedGestationWeeks, 0) = isnull(source.CalculatedGestationWeeks, 0)
		and isnull(target.CalculatedGestationDays, 0) = isnull(source.CalculatedGestationDays, 0)
		and isnull(target.BookingLocationCode, '') = isnull(source.BookingLocationCode, '')
		and isnull(target.AntenatalSteroidCode, 0) = isnull(source.AntenatalSteroidCode, 0)
		and isnull(target.AntenatalSteroid, '') = isnull(source.AntenatalSteroid, '')
		and isnull(target.AntenatalSteroidCourseCode, 0) = isnull(source.AntenatalSteroidCourseCode, 0)
		and isnull(target.LabourOnsetCode, '') = isnull(source.LabourOnsetCode, '')
		and isnull(target.LabourPresentationCode, '') = isnull(source.LabourPresentationCode, '')
		and isnull(target.LabourDeliveryCode, 0) = isnull(source.LabourDeliveryCode, 0)
		and isnull(target.PlaceOfBirthCode, '') = isnull(source.PlaceOfBirthCode, '')
		and isnull(target.LabourDrugCode, '') = isnull(source.LabourDrugCode, '')
		and isnull(target.StaffPresentAtResuscitation, '') = isnull(source.StaffPresentAtResuscitation, '')
		and isnull(target.AgparScoreOneMinute, 0) = isnull(source.AgparScoreOneMinute, 0)
		and isnull(target.AgparScoreFiveMinutes, 0) = isnull(source.AgparScoreFiveMinutes, 0)
		and isnull(target.AgparScoreTenMinutes, 0) = isnull(source.AgparScoreTenMinutes, 0)
		and isnull(target.BloodTypeCode, '') = isnull(source.BloodTypeCode, '')
		and isnull(target.CRIBIIScore, 0) = isnull(source.CRIBIIScore, 0)
		and isnull(target.Outcome, '') = isnull(source.Outcome, '')
		and isnull(target.VitaminKCode, '') = isnull(source.VitaminKCode, '')
		and isnull(target.VitaminKRouteCode, '') = isnull(source.VitaminKRouteCode, '')
		and isnull(target.CordArterialpH, 0) = isnull(source.CordArterialpH, 0)
		and isnull(target.CordVenouspH, 0) = isnull(source.CordVenouspH, 0)
		and isnull(target.CordPcO2Arterial, 0) = isnull(source.CordPcO2Arterial, 0)
		and isnull(target.CordPcO2Venous, 0) = isnull(source.CordPcO2Venous, 0)
		and isnull(target.CordArterialBE, 0) = isnull(source.CordArterialBE, 0)
		and isnull(target.CordVenousBE, 0) = isnull(source.CordVenousBE, 0)
		and isnull(target.CordClampingCode, 0) = isnull(source.CordClampingCode, 0)
		and isnull(target.CordClampingTimeMinute, 0) = isnull(source.CordClampingTimeMinute, 0)
		and isnull(target.CordClampingTimeSecond, 0) = isnull(source.CordClampingTimeSecond, 0)
		and isnull(target.CordStripping, 0) = isnull(source.CordStripping, 0)
		and isnull(target.ResuscitationCode, '') = isnull(source.ResuscitationCode, '')
		and isnull(target.ResuscitationSurfactantCode, 0) = isnull(source.ResuscitationSurfactantCode, 0)
		and isnull(target.FirstGaspCode, 0) = isnull(source.FirstGaspCode, 0)
		and isnull(target.SpontaneousRespirationCode, 0) = isnull(source.SpontaneousRespirationCode, 0)
		and isnull(target.OffensiveLiquor, 0) = isnull(source.OffensiveLiquor, 0)
		and isnull(target.GPCode, '') = isnull(source.GPCode, '')
		and isnull(target.GPPracticeCode, '') = isnull(source.GPPracticeCode, '')
		and isnull(target.SeizureCode, 0) = isnull(source.SeizureCode, 0)
		and isnull(target.HIEGradeCode, 0) = isnull(source.HIEGradeCode, 0)
		and isnull(target.AnticonvulsantDrug, '') = isnull(source.AnticonvulsantDrug, '')
		and isnull(target.Pneumothorax, 0) = isnull(source.Pneumothorax, 0)
		and isnull(target.NecrotisingEnterocolitis, 0) = isnull(source.NecrotisingEnterocolitis, 0)
		and isnull(target.NeonatalAbstinence, 0) = isnull(source.NeonatalAbstinence, 0)
		and isnull(target.ROPScreenDate, getdate()) = isnull(source.ROPScreenDate, getdate())
		and isnull(target.ROPSurgeryDate, getdate()) = isnull(source.ROPSurgeryDate, getdate())
		and isnull(target.Dexamethasone, 0) = isnull(source.Dexamethasone, 0)
		and isnull(target.PDAIndomethacin, 0) = isnull(source.PDAIndomethacin, 0)
		and isnull(target.PDAIbuprofen, 0) = isnull(source.PDAIbuprofen, 0)
		and isnull(target.PDASurgery, 0) = isnull(source.PDASurgery, 0)
		and isnull(target.PDADischarge, 0) = isnull(source.PDADischarge, 0)
		and isnull(target.UACDate, getdate()) = isnull(source.UACDate, getdate())
		and isnull(target.UVCDate, getdate()) = isnull(source.UVCDate, getdate())
		and isnull(target.LongLineTime, getdate()) = isnull(source.LongLineTime, getdate())
		and isnull(target.PeripheralArterialLineDate, getdate()) = isnull(source.PeripheralArterialLineDate, getdate())
		and isnull(target.SurgicalLineDate, getdate()) = isnull(source.SurgicalLineDate, getdate())
		and isnull(target.ParenteralNutritionDays, 0) = isnull(source.ParenteralNutritionDays, 0)
		and isnull(target.FirstHeadScanDate, getdate()) = isnull(source.FirstHeadScanDate, getdate())
		and isnull(target.FirstHeadScanResult, '') = isnull(source.FirstHeadScanResult, '')
		and isnull(target.LastHeadScanDate, getdate()) = isnull(source.LastHeadScanDate, getdate())
		and isnull(target.LastHeadScanResult, '') = isnull(source.LastHeadScanResult, '')
		and isnull(target.CongenitalAnomaly, '') = isnull(source.CongenitalAnomaly, '')
		and isnull(target.VPShuntDate, getdate()) = isnull(source.VPShuntDate, getdate())
		and isnull(target.FirstBloodCultureDate, getdate()) = isnull(source.FirstBloodCultureDate, getdate())
		and isnull(target.FirstBloodCultureResult, '') = isnull(source.FirstBloodCultureResult, '')
		and isnull(target.FirstCSFCultureDate, getdate()) = isnull(source.FirstCSFCultureDate, getdate())
		and isnull(target.FirstCSFCultureResult, '') = isnull(source.FirstCSFCultureResult, '')
		and isnull(target.FirstUrineCultureDate, getdate()) = isnull(source.FirstUrineCultureDate, getdate())
		and isnull(target.FirstUrineCultureResult, '') = isnull(source.FirstUrineCultureResult, '')
		and isnull(target.ExchangeTransfusion, 0) = isnull(source.ExchangeTransfusion, 0)
		and isnull(target.Tracheostomy, 0) = isnull(source.Tracheostomy, 0)
		and isnull(target.PulmonaryVasodilatorDate, getdate()) = isnull(source.PulmonaryVasodilatorDate, getdate())
		and isnull(target.PulmonaryVasodilatorDrug, '') = isnull(source.PulmonaryVasodilatorDrug, '')
		and isnull(target.Inotrope, '') = isnull(source.Inotrope, '')
		and isnull(target.FirstInotropeDate, getdate()) = isnull(source.FirstInotropeDate, getdate())
		and isnull(target.PeritonealDialysis, 0) = isnull(source.PeritonealDialysis, 0)
		and isnull(target.DischargeApnoeaCardioSat, 0) = isnull(source.DischargeApnoeaCardioSat, 0)
		and isnull(target.Gastroschisis, 0) = isnull(source.Gastroschisis, 0)
		and isnull(target.Cooled, 0) = isnull(source.Cooled, 0)
		and isnull(target.Consultant, '') = isnull(source.Consultant, '')
		and isnull(target.LastUpdatedTime, getdate()) = isnull(source.LastUpdatedTime, getdate())
		and isnull(target.ModifiedTime, getdate()) = isnull(source.ModifiedTime, getdate())
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			 target.EpisodeID = source.EpisodeID
			,target.NHSNumber = source.NHSNumber
			,target.CasenoteNumber = source.CasenoteNumber
			,target.SourceUniqueID = source.SourceUniqueID
			,target.CareLocationID = source.CareLocationID
			,target.EpisodeTypeCode = source.EpisodeTypeCode
			,target.Surname = source.Surname
			,target.Forename = source.Forename
			,target.SexCode = source.SexCode
			,target.BirthTime = source.BirthTime
			,target.GestationWeeks = source.GestationWeeks
			,target.GestationDays = source.GestationDays
			,target.BirthWeight = source.BirthWeight
			,target.BirthLength = source.BirthLength
			,target.BirthHeadCircumference = source.BirthHeadCircumference
			,target.BirthOrder = source.BirthOrder
			,target.FetusNumber = source.FetusNumber
			,target.BirthSummary = source.BirthSummary
			,target.EpisodeNumber = source.EpisodeNumber
			,target.AdmissionDate = source.AdmissionDate
			,target.AdmissionTime = source.AdmissionTime
			,target.AdmissionFromSiteCode = source.AdmissionFromSiteCode
			,target.AdmissionSourceCode = source.AdmissionSourceCode
			,target.Readmission = source.Readmission
			,target.AdmissionTypeCode = source.AdmissionTypeCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.AdmissionReasonCode = source.AdmissionReasonCode
			,target.ProviderCode = source.ProviderCode
			,target.NetworkID = source.NetworkID
			,target.ConsultationWithParents = source.ConsultationWithParents
			,target.AdmissionTemperatureMeasured = source.AdmissionTemperatureMeasured
			,target.AdmissionTemperature = source.AdmissionTemperature
			,target.AdmissionTemperatureTime = source.AdmissionTemperatureTime
			,target.AdmissionBloodPressure = source.AdmissionBloodPressure
			,target.AdmissionHeartRate = source.AdmissionHeartRate
			,target.AdmissionRespiratoryRate = source.AdmissionRespiratoryRate
			,target.AdmissionSaO2 = source.AdmissionSaO2
			,target.AdmissionBloodGlucose = source.AdmissionBloodGlucose
			,target.AdmissionWeight = source.AdmissionWeight
			,target.AdmissionHeadCircumference = source.AdmissionHeadCircumference
			,target.TemperatureNotRecordable = source.TemperatureNotRecordable
			,target.DischargeDate = source.DischargeDate
			,target.DischargeTime = source.DischargeTime
			,target.DischargeDestinationCode = source.DischargeDestinationCode
			,target.DischargeHospitalCode = source.DischargeHospitalCode
			,target.DischargeDestinationWardCode = source.DischargeDestinationWardCode
			,target.DischargeWeight = source.DischargeWeight
			,target.DischargeHeadCircumference = source.DischargeHeadCircumference
			,target.DischargeMilkCode = source.DischargeMilkCode
			,target.DischargeFeedingCode = source.DischargeFeedingCode
			,target.HomeTubeFeeding = source.HomeTubeFeeding
			,target.DischargeOxygen = source.DischargeOxygen
			,target.TransferReasonCode = source.TransferReasonCode
			,target.TransferNetworkCode = source.TransferNetworkCode
			,target.CauseOfDeathCode = source.CauseOfDeathCode
			,target.PostMortemConsent = source.PostMortemConsent
			,target.PostMortemCode = source.PostMortemCode
			,target.PostMortemConfirmedNECCode = source.PostMortemConfirmedNECCode
			,target.EpisodeSummary = source.EpisodeSummary
			,target.VentilationDays = source.VentilationDays
			,target.CPAPDays = source.CPAPDays
			,target.OxygenDays = source.OxygenDays
			,target.OxygenDaysNoCPAPVentilation = source.OxygenDaysNoCPAPVentilation
			,target.OxygenLastGestation = source.OxygenLastGestation
			,target.OxygenLastTime = source.OxygenLastTime
			,target.IntensiveCareDays2001 = source.IntensiveCareDays2001
			,target.HighDependancyCareDays2001 = source.HighDependancyCareDays2001
			,target.SpecialCareDays2001 = source.SpecialCareDays2001
			,target.IntensiveCareDays2011 = source.IntensiveCareDays2011
			,target.HighDependancyCareDays2011 = source.HighDependancyCareDays2011
			,target.SpecialCareDays2011 = source.SpecialCareDays2011
			,target.NormalCareDays2011 = source.NormalCareDays2011
			,target.IntensiveCareHRGDays = source.IntensiveCareHRGDays
			,target.HighDependancyHRGDays = source.HighDependancyHRGDays
			,target.SpecialCareNoCarerResidentHRGDays = source.SpecialCareNoCarerResidentHRGDays
			,target.SpecialCareWithCarerResidentHRGDays = source.SpecialCareWithCarerResidentHRGDays
			,target.NormalCareHRGDays = source.NormalCareHRGDays
			,target.NeonatalUnitDays = source.NeonatalUnitDays
			,target.TransitionalCareDays = source.TransitionalCareDays
			,target.PostnatalWardDays = source.PostnatalWardDays
			,target.OtherObstetricAreaDays = source.OtherObstetricAreaDays
			,target.LocnNNUPortion = source.LocnNNUPortion
			,target.LocnTCPoriton = source.LocnTCPoriton
			,target.LocnPNWPortion = source.LocnPNWPortion
			,target.DrugsDuringStay = source.DrugsDuringStay
			,target.DiagnosisDuringStay = source.DiagnosisDuringStay
			,target.MotherNHSNumber = source.MotherNHSNumber
			,target.MotherCasenoteNumber = source.MotherCasenoteNumber
			,target.MotherSurname = source.MotherSurname
			,target.MotherForename = source.MotherForename
			,target.MotherReligionCode = source.MotherReligionCode
			,target.MotherMaritalStatusCode = source.MotherMaritalStatusCode
			,target.MotherOccupation = source.MotherOccupation
			,target.MotherBloodGroup = source.MotherBloodGroup
			,target.MotherDateOfBirth = source.MotherDateOfBirth
			,target.MotherPostCode = source.MotherPostCode
			,target.MotherEthnicCategoryCode = source.MotherEthnicCategoryCode
			,target.MotherHaemoglobinoapthyCode = source.MotherHaemoglobinoapthyCode
			,target.MotherMedicalProblemCode = source.MotherMedicalProblemCode
			,target.MotherDiabetesCode = source.MotherDiabetesCode
			,target.MotherHepatitisBCode = source.MotherHepatitisBCode
			,target.MotherHepatitisBHighRiskCode = source.MotherHepatitisBHighRiskCode
			,target.MotherHIVCode = source.MotherHIVCode
			,target.MotherRubellaScreen = source.MotherRubellaScreen
			,target.MotherSyphilisCode = source.MotherSyphilisCode
			,target.MotherHepatitisCCode = source.MotherHepatitisCCode
			,target.MotherMotherHepatitisCScreen = source.MotherMotherHepatitisCScreen
			,target.MotherSyphilisVDRLScreenCode = source.MotherSyphilisVDRLScreenCode
			,target.MotherSyphilisTPHAScreenCode = source.MotherSyphilisTPHAScreenCode
			,target.MaternalPyrexiaInLabour = source.MaternalPyrexiaInLabour
			,target.MotherIntrapartumAntibiotics = source.MotherIntrapartumAntibiotics
			,target.MeconiumStainedLiquor = source.MeconiumStainedLiquor
			,target.MembraneRuptureTime = source.MembraneRuptureTime
			,target.MotherAntenatalScanCode = source.MotherAntenatalScanCode
			,target.ParentsConsanguinous = source.ParentsConsanguinous
			,target.MotherDrugAbuseCode = source.MotherDrugAbuseCode
			,target.MotherSmoker = source.MotherSmoker
			,target.MotherCigarettesPerDay = source.MotherCigarettesPerDay
			,target.MotherAlcoholUseCode = source.MotherAlcoholUseCode
			,target.MotherPregnancyProblemCode = source.MotherPregnancyProblemCode
			,target.LastMenstrualPeriodDate = source.LastMenstrualPeriodDate
			,target.ExpectedDateOfDelivery = source.ExpectedDateOfDelivery
			,target.PreviousPregnancies = source.PreviousPregnancies
			,target.FatherOccupation = source.FatherOccupation
			,target.FatherAge = source.FatherAge
			,target.FatherEthnicCategoryCode = source.FatherEthnicCategoryCode
			,target.CalculatedGestationWeeks = source.CalculatedGestationWeeks
			,target.CalculatedGestationDays = source.CalculatedGestationDays
			,target.BookingLocationCode = source.BookingLocationCode
			,target.AntenatalSteroidCode = source.AntenatalSteroidCode
			,target.AntenatalSteroid = source.AntenatalSteroid
			,target.AntenatalSteroidCourseCode = source.AntenatalSteroidCourseCode
			,target.LabourOnsetCode = source.LabourOnsetCode
			,target.LabourPresentationCode = source.LabourPresentationCode
			,target.LabourDeliveryCode = source.LabourDeliveryCode
			,target.PlaceOfBirthCode = source.PlaceOfBirthCode
			,target.LabourDrugCode = source.LabourDrugCode
			,target.StaffPresentAtResuscitation = source.StaffPresentAtResuscitation
			,target.AgparScoreOneMinute = source.AgparScoreOneMinute
			,target.AgparScoreFiveMinutes = source.AgparScoreFiveMinutes
			,target.AgparScoreTenMinutes = source.AgparScoreTenMinutes
			,target.BloodTypeCode = source.BloodTypeCode
			,target.CRIBIIScore = source.CRIBIIScore
			,target.Outcome = source.Outcome
			,target.VitaminKCode = source.VitaminKCode
			,target.VitaminKRouteCode = source.VitaminKRouteCode
			,target.CordArterialpH = source.CordArterialpH
			,target.CordVenouspH = source.CordVenouspH
			,target.CordPcO2Arterial = source.CordPcO2Arterial
			,target.CordPcO2Venous = source.CordPcO2Venous
			,target.CordArterialBE = source.CordArterialBE
			,target.CordVenousBE = source.CordVenousBE
			,target.CordClampingCode = source.CordClampingCode
			,target.CordClampingTimeMinute = source.CordClampingTimeMinute
			,target.CordClampingTimeSecond = source.CordClampingTimeSecond
			,target.CordStripping = source.CordStripping
			,target.ResuscitationCode = source.ResuscitationCode
			,target.ResuscitationSurfactantCode = source.ResuscitationSurfactantCode
			,target.FirstGaspCode = source.FirstGaspCode
			,target.SpontaneousRespirationCode = source.SpontaneousRespirationCode
			,target.OffensiveLiquor = source.OffensiveLiquor
			,target.GPCode = source.GPCode
			,target.GPPracticeCode = source.GPPracticeCode
			,target.SeizureCode = source.SeizureCode
			,target.HIEGradeCode = source.HIEGradeCode
			,target.AnticonvulsantDrug = source.AnticonvulsantDrug
			,target.Pneumothorax = source.Pneumothorax
			,target.NecrotisingEnterocolitis = source.NecrotisingEnterocolitis
			,target.NeonatalAbstinence = source.NeonatalAbstinence
			,target.ROPScreenDate = source.ROPScreenDate
			,target.ROPSurgeryDate = source.ROPSurgeryDate
			,target.Dexamethasone = source.Dexamethasone
			,target.PDAIndomethacin = source.PDAIndomethacin
			,target.PDAIbuprofen = source.PDAIbuprofen
			,target.PDASurgery = source.PDASurgery
			,target.PDADischarge = source.PDADischarge
			,target.UACDate = source.UACDate
			,target.UVCDate = source.UVCDate
			,target.LongLineTime = source.LongLineTime
			,target.PeripheralArterialLineDate = source.PeripheralArterialLineDate
			,target.SurgicalLineDate = source.SurgicalLineDate
			,target.ParenteralNutritionDays = source.ParenteralNutritionDays
			,target.FirstHeadScanDate = source.FirstHeadScanDate
			,target.FirstHeadScanResult = source.FirstHeadScanResult
			,target.LastHeadScanDate = source.LastHeadScanDate
			,target.LastHeadScanResult = source.LastHeadScanResult
			,target.CongenitalAnomaly = source.CongenitalAnomaly
			,target.VPShuntDate = source.VPShuntDate
			,target.FirstBloodCultureDate = source.FirstBloodCultureDate
			,target.FirstBloodCultureResult = source.FirstBloodCultureResult
			,target.FirstCSFCultureDate = source.FirstCSFCultureDate
			,target.FirstCSFCultureResult = source.FirstCSFCultureResult
			,target.FirstUrineCultureDate = source.FirstUrineCultureDate
			,target.FirstUrineCultureResult = source.FirstUrineCultureResult
			,target.ExchangeTransfusion = source.ExchangeTransfusion
			,target.Tracheostomy = source.Tracheostomy
			,target.PulmonaryVasodilatorDate = source.PulmonaryVasodilatorDate
			,target.PulmonaryVasodilatorDrug = source.PulmonaryVasodilatorDrug
			,target.Inotrope = source.Inotrope
			,target.FirstInotropeDate = source.FirstInotropeDate
			,target.PeritonealDialysis = source.PeritonealDialysis
			,target.DischargeApnoeaCardioSat = source.DischargeApnoeaCardioSat
			,target.Gastroschisis = source.Gastroschisis
			,target.Cooled = source.Cooled
			,target.Consultant = source.Consultant
			,target.LastUpdatedTime = source.LastUpdatedTime
			,target.ModifiedTime = source.ModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime










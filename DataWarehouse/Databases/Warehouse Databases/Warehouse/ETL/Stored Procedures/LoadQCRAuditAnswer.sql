﻿CREATE PROCEDURE [ETL].[LoadQCRAuditAnswer]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

select
	 @LoadStartDate = MIN(CONVERT(datetime, AuditDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, AuditDate)) 
from [ETL].[TLoadQCRAuditAnswer]


declare @MergeSummary TABLE(Action nvarchar(10));

merge QCR.AuditAnswer target
using ETL.TLoadQCRAuditAnswer source
on source.SourceUniqueID = target.SourceUniqueID

when matched and not
(
	isnull(target.AuditTime, getdate()) = isnull(source.AuditTime, getdate())
	and isnull(target.AuditDate, getdate()) = isnull(source.AuditDate, getdate())
	and isnull(target.LocationCode, 0) = isnull(source.LocationCode, 0)
	and isnull(target.WardCode, 0) = isnull(source.WardCode, 0)
	and isnull(target.AuditTypeCode, 0) = isnull(source.AuditTypeCode, 0)
	and isnull(target.QuestionCode, 0) = isnull(source.QuestionCode, 0)
	and isnull(target.Answer, 0) = isnull(source.Answer, 0)
)
then update set
	target.AuditTime = source.AuditTime
	,target.AuditDate = source.AuditDate
	,target.LocationCode = source.LocationCode
	,target.WardCode = source.WardCode
	,target.AuditTypeCode = source.AuditTypeCode
	,target.QuestionCode = source.QuestionCode
	,target.Answer = source.Answer

when not matched by source then delete

when not matched by target then insert
(
	 SourceUniqueID
	,AuditTime
	,AuditDate
	,LocationCode
	,WardCode
	,AuditTypeCode
	,QuestionCode
	,Answer
)
values
(
	 source.SourceUniqueID
	,source.AuditTime
	,source.AuditDate
	,source.LocationCode
	,source.WardCode
	,source.AuditTypeCode
	,source.QuestionCode
	,source.Answer
)

OUTPUT $Action INTO @MergeSummary;

select
	 @RowsInserted = coalesce(sum(Inserted),0)
	,@RowsUpdated = coalesce(sum(Updated),0)
	,@RowsDeleted = coalesce(sum(Deleted),0)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select @Elapsed = DATEDIFF(minute,@StartTime,getdate())

select @Stats = 

	'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + 
	', Elapsed ' + CONVERT(varchar(10), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

exec Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

print @Stats
    

﻿

CREATE proc [ETL].[LoadFinanceExpenditure]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, CensusDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, CensusDate)) 
from
	ETL.TLoadFinanceExpenditure

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Finance.Expenditure target
using
	(
	select distinct
		CensusDate
		,Division
		,Budget
		,AnnualBudget
		,Actual
		,InterfaceCode
	from
		ETL.TLoadFinanceExpenditure
	) source
	on	source.CensusDate = target.CensusDate	
	and source.Division = target.Division
	
	when not matched by source
	and	target.CensusDate between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			CensusDate
			,Division
			,Budget
			,AnnualBudget
			,Actual
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.CensusDate
			,source.Division
			,source.Budget
			,source.AnnualBudget
			,source.Actual
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.CensusDate, getdate()) = isnull(source.CensusDate, getdate())
		and isnull(target.Division, '') = isnull(source.Division, '')
		and isnull(target.Budget, 0) = isnull(source.Budget, 0)
		and isnull(target.AnnualBudget, 0) = isnull(source.AnnualBudget, 0)
		and isnull(target.Actual, 0) = isnull(source.Actual, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			target.CensusDate = source.CensusDate
			,target.Division = source.Division
			,target.Budget = source.Budget
			,target.AnnualBudget = source.AnnualBudget
			,target.Actual = source.Actual
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

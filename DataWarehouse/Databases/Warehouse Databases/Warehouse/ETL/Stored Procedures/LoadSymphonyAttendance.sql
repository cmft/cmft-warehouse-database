﻿
CREATE PROCEDURE [ETL].[LoadSymphonyAttendance]

as

/********************************************************************************************************************************************************************
Author: Phil Orrell
Date: 
Aim: Load Symphony Attenance table

Change History
20141210 PDO Created
*********************************************************************************************************************************************************************/
declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

--create temporary table to allow indexing and therefore better MERGE performance
select
	 Encounter.*
	,EncounterChecksum =
		CHECKSUM(
			 AttendanceID
			,EpisodeID
			,AttendanceTypeID
			,StatusID
			,IsReattender
			,AttendanceNumber
			,ArrivalTime
			,RegisteredTime
			,BookedTime
			,CancelledTime
			,Slot
			,SourceOfReferralID
			,ReferralMethodID
			,ReferralSpecialtyID
			,ConsultantID
			,GpCode
			,PracticeCode
			,PracticeAddressID
			,ArrivalModeID
			,IncidentLocationTypeID
			,LocationNotesID
			,AmbulanceCrew
			,AttendanceConclusionTime
			,AttendanceDisposalID
			,DischargeDestinationID
			,DischargeNotesID
			,PresentingProblem
			,PatientGroupID
			,CareGroupID
			,PatientComplaint
			,DischargeCreatedByID
			,DischargeCreatedTime
			,DischargeUpdatedByID
			,DischargeUpdatedByTime
			,AttendanceCreatedByID
			,AttendanceUpdatedByID
			,AttendanceCreatedTime
			,AttendanceUpdatedTime
			,NonAttendanceReasonID
			,TransferPatient
			,MergedByID
			,MergeTime
			)
into
	#TLoadSymphonyAttendance
from
	(
	select
		 AttendanceID = cast(AttendanceID as int)
		,EpisodeID = cast(EpisodeID as int)
		,AttendanceTypeID = cast(AttendanceTypeID as tinyint)
		,StatusID = cast(StatusID as tinyint)
		,IsReattender = cast(IsReattender as bit)
		,AttendanceNumber = cast(AttendanceNumber as varchar(20))
		,ArrivalTime = cast(ArrivalTime as datetime)
		,RegisteredTime = cast(RegisteredTime as datetime)
		,BookedTime = cast(BookedTime as datetime)
		,CancelledTime = cast(CancelledTime as datetime)
		,Slot = cast(Slot as smallint)
		,SourceOfReferralID = cast(SourceOfReferralID as int)
		,ReferralMethodID = cast(ReferralMethodID as int)
		,ReferralSpecialtyID = cast(ReferralSpecialtyID as int)
		,ConsultantID = cast(ConsultantID as int)
		,GpCode = cast(GpCode as varchar(14))
		,PracticeCode = cast(PracticeCode as varchar(14))
		,PracticeAddressID = cast(PracticeAddressID as int)
		,ArrivalModeID = cast(ArrivalModeID as int)
		,IncidentLocationTypeID = cast(IncidentLocationTypeID as int)
		,LocationNotesID = cast(LocationNotesID as int)
		,AmbulanceCrew = cast(AmbulanceCrew as varchar(30))
		,AttendanceConclusionTime = cast(AttendanceConclusionTime as datetime)
		,AttendanceDisposalID = cast(AttendanceDisposalID as int)
		,DischargeDestinationID = cast(DischargeDestinationID as int)
		,DischargeNotesID = cast(DischargeNotesID as int)
		,PresentingProblem = cast(PresentingProblem as varchar(300))
		,PatientGroupID = cast(PatientGroupID as int)
		,CareGroupID = cast(CareGroupID as int)
		,PatientComplaint = cast(PatientComplaint as varchar(300))
		,DischargeCreatedByID = cast(DischargeCreatedByID as int)
		,DischargeCreatedTime = cast(DischargeCreatedTime as datetime)
		,DischargeUpdatedByID = cast(DischargeUpdatedByID as int)
		,DischargeUpdatedByTime = cast(DischargeUpdatedByTime as datetime)
		,AttendanceCreatedByID = cast(AttendanceCreatedByID as int)
		,AttendanceUpdatedByID = cast(AttendanceUpdatedByID as int)
		,AttendanceCreatedTime = cast(AttendanceCreatedTime as datetime)
		,AttendanceUpdatedTime = cast(AttendanceUpdatedTime as datetime)
		,NonAttendanceReasonID = cast(NonAttendanceReasonID as int)
		,TransferPatient = cast(TransferPatient as bit)
		,MergedByID = cast(MergedByID as int)
		,MergeTime = cast(MergeTime as datetime)
	from
		(
		select 
			 AttendanceID = Attendance.atd_id
			,EpisodeID = Attendance.atd_epdid
			,AttendanceTypeID = Attendance.atd_attendancetype
			,StatusID = Attendance.atd_status
			,IsReattender = Attendance.atd_reattender
			,AttendanceNumber = Attendance.atd_num
			,ArrivalTime = Attendance.atd_arrivaldate
			,RegisteredTime = Attendance.atd_regdate
			,BookedTime = Attendance.atd_bookdate
			,CancelledTime = Attendance.atd_canceldate
			,Slot = Attendance.atd_slot
			,SourceOfReferralID = Attendance.atd_refsource
			,ReferralMethodID = Attendance.atd_refmethod
			,ReferralSpecialtyID = Attendance.atd_refspecialty
			,ConsultantID = Attendance.atd_consultant
			,GpCode = Attendance.atd_gpid
			,PracticeCode = Attendance.atd_prcode
			,PracticeAddressID = Attendance.atd_praddid
			,ArrivalModeID = Attendance.atd_arrmode
			,IncidentLocationTypeID = Attendance.atd_activity
			,LocationNotesID = Attendance.atd_locnotes
			,AmbulanceCrew = Attendance.atd_ambcrew
			,AttendanceConclusionTime = Attendance.atd_dischdate
			,AttendanceDisposalID = Attendance.atd_dischoutcome
			,DischargeDestinationID = Attendance.atd_dischdest
			,DischargeNotesID = Attendance.atd_dischnotes
			,PresentingProblem = Attendance.atd_complaintid
			,PatientGroupID = Attendance.atd_patgroup
			,CareGroupID = Attendance.atd_caregroup
			,PatientComplaint = Attendance.atd_patcomplaint
			,DischargeCreatedByID = Attendance.atd_dischcreatedby
			,DischargeCreatedTime = Attendance.atd_dischcreated
			,DischargeUpdatedByID = Attendance.atd_dischupdatedby
			,DischargeUpdatedByTime = Attendance.atd_dischupdated
			,AttendanceCreatedByID = Attendance.atd_createdby
			,AttendanceUpdatedByID = Attendance.atd_updatedby
			,AttendanceCreatedTime = Attendance.atd_created
			,AttendanceUpdatedTime = Attendance.atd_update
			,NonAttendanceReasonID = Attendance.atd_nonattendancereason
			,TransferPatient = Attendance.atd_TransferPatient
			,MergedByID = Attendance.atd_mergedby
			,MergeTime = Attendance.atd_mergecreated
		from
			[$(Symphony)].dbo.Attendance_Details Attendance
		where
			Attendance.atd_deleted = 0

		) Encounter
	) Encounter

order by
	Encounter.AttendanceID


create unique clustered index #IX_TLoadSymphonyAttendance on #TLoadSymphonyAttendance
	(
	AttendanceID ASC
	)


declare @ProcessList table
	(
	 EncounterRecno int
	,Action nvarchar(10)
	)

merge
	Symphony.Attendance target
using
	(
	select
			AttendanceID
		,EpisodeID
		,AttendanceTypeID
		,StatusID
		,IsReattender
		,AttendanceNumber
		,ArrivalTime
		,RegisteredTime
		,BookedTime
		,CancelledTime
		,Slot
		,SourceOfReferralID
		,ReferralMethodID
		,ReferralSpecialtyID
		,ConsultantID
		,GpCode
		,PracticeCode
		,PracticeAddressID
		,ArrivalModeID
		,IncidentLocationTypeID
		,LocationNotesID
		,AmbulanceCrew
		,AttendanceConclusionTime
		,AttendanceDisposalID
		,DischargeDestinationID
		,DischargeNotesID
		,PresentingProblem
		,PatientGroupID
		,CareGroupID
		,PatientComplaint
		,DischargeCreatedByID
		,DischargeCreatedTime
		,DischargeUpdatedByID
		,DischargeUpdatedByTime
		,AttendanceCreatedByID
		,AttendanceUpdatedByID
		,AttendanceCreatedTime
		,AttendanceUpdatedTime
		,NonAttendanceReasonID
		,TransferPatient
		,MergedByID
		,MergeTime
		,EncounterChecksum
	from
		#TLoadSymphonyAttendance
	
	) source
	on	source.AttendanceID = target.AttendanceID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 AttendanceID
			,EpisodeID
			,AttendanceTypeID
			,StatusID
			,IsReattender
			,AttendanceNumber
			,ArrivalTime
			,RegisteredTime
			,BookedTime
			,CancelledTime
			,Slot
			,SourceOfReferralID
			,ReferralMethodID
			,ReferralSpecialtyID
			,ConsultantID
			,GpCode
			,PracticeCode
			,PracticeAddressID
			,ArrivalModeID
			,IncidentLocationTypeID
			,LocationNotesID
			,AmbulanceCrew
			,AttendanceConclusionTime
			,AttendanceDisposalID
			,DischargeDestinationID
			,DischargeNotesID
			,PresentingProblem
			,PatientGroupID
			,CareGroupID
			,PatientComplaint
			,DischargeCreatedByID
			,DischargeCreatedTime
			,DischargeUpdatedByID
			,DischargeUpdatedByTime
			,AttendanceCreatedByID
			,AttendanceUpdatedByID
			,AttendanceCreatedTime
			,AttendanceUpdatedTime
			,NonAttendanceReasonID
			,TransferPatient
			,MergedByID
			,MergeTime
			,EncounterChecksum
			,Created
			,ByWhom
			)
		values
			(
			 source.AttendanceID
			,source.EpisodeID
			,source.AttendanceTypeID
			,source.StatusID
			,source.IsReattender
			,source.AttendanceNumber
			,source.ArrivalTime
			,source.RegisteredTime
			,source.BookedTime
			,source.CancelledTime
			,source.Slot
			,source.SourceOfReferralID
			,source.ReferralMethodID
			,source.ReferralSpecialtyID
			,source.ConsultantID
			,source.GpCode
			,source.PracticeCode
			,source.PracticeAddressID
			,source.ArrivalModeID
			,source.IncidentLocationTypeID
			,source.LocationNotesID
			,source.AmbulanceCrew
			,source.AttendanceConclusionTime
			,source.AttendanceDisposalID
			,source.DischargeDestinationID
			,source.DischargeNotesID
			,source.PresentingProblem
			,source.PatientGroupID
			,source.CareGroupID
			,source.PatientComplaint
			,source.DischargeCreatedByID
			,source.DischargeCreatedTime
			,source.DischargeUpdatedByID
			,source.DischargeUpdatedByTime
			,source.AttendanceCreatedByID
			,source.AttendanceUpdatedByID
			,source.AttendanceCreatedTime
			,source.AttendanceUpdatedTime
			,source.NonAttendanceReasonID
			,source.TransferPatient
			,source.MergedByID
			,source.MergeTime
			,source.EncounterChecksum
			,getdate()
			,suser_name()
			)

	when matched
	and source.EncounterChecksum <> target.EncounterChecksum
	then
		update
		set
			 target.AttendanceID = source.AttendanceID
			,target.EpisodeID = source.EpisodeID
			,target.AttendanceTypeID = source.AttendanceTypeID
			,target.StatusID = source.StatusID
			,target.IsReattender = source.IsReattender
			,target.AttendanceNumber = source.AttendanceNumber
			,target.ArrivalTime = source.ArrivalTime
			,target.RegisteredTime = source.RegisteredTime
			,target.BookedTime = source.BookedTime
			,target.CancelledTime = source.CancelledTime
			,target.Slot = source.Slot
			,target.SourceOfReferralID = source.SourceOfReferralID
			,target.ReferralMethodID = source.ReferralMethodID
			,target.ReferralSpecialtyID = source.ReferralSpecialtyID
			,target.ConsultantID = source.ConsultantID
			,target.GpCode = source.GpCode
			,target.PracticeCode = source.PracticeCode
			,target.PracticeAddressID = source.PracticeAddressID
			,target.ArrivalModeID = source.ArrivalModeID
			,target.IncidentLocationTypeID = source.IncidentLocationTypeID
			,target.LocationNotesID = source.LocationNotesID
			,target.AmbulanceCrew = source.AmbulanceCrew
			,target.AttendanceConclusionTime = source.AttendanceConclusionTime
			,target.AttendanceDisposalID = source.AttendanceDisposalID
			,target.DischargeDestinationID = source.DischargeDestinationID
			,target.DischargeNotesID = source.DischargeNotesID
			,target.PresentingProblem = source.PresentingProblem
			,target.PatientGroupID = source.PatientGroupID
			,target.CareGroupID = source.CareGroupID
			,target.PatientComplaint = source.PatientComplaint
			,target.DischargeCreatedByID = source.DischargeCreatedByID
			,target.DischargeCreatedTime = source.DischargeCreatedTime
			,target.DischargeUpdatedByID = source.DischargeUpdatedByID
			,target.DischargeUpdatedByTime = source.DischargeUpdatedByTime
			,target.AttendanceCreatedByID = source.AttendanceCreatedByID
			,target.AttendanceUpdatedByID = source.AttendanceUpdatedByID
			,target.AttendanceCreatedTime = source.AttendanceCreatedTime
			,target.AttendanceUpdatedTime = source.AttendanceUpdatedTime
			,target.NonAttendanceReasonID = source.NonAttendanceReasonID
			,target.TransferPatient = source.TransferPatient
			,target.MergedByID = source.MergedByID
			,target.MergeTime = source.MergeTime
			,target.EncounterChecksum = source.EncounterChecksum

			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.EncounterRecno, deleted.EncounterRecno)
	,$action
into
	@ProcessList
;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@ProcessList
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

exec Utility.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿CREATE    procedure [ETL].[ExtractInquireAPCWLSuspension]
	@CensusDate smalldatetime
as

set dateformat dmy
declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)


select @StartTime = getdate()
select @RowsInserted = 0


insert into ETL.TImportAPCWLSuspension 
(
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,SourceUniqueID
)
select distinct --as there is potential to return duplicates (apparently!)
	 SourcePatientNo
	,SourceEncounterNo
	,@CensusDate
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,SourceUniqueID
from
	(

	SELECT
		 SourcePatientNo = WLCurrent.InternalPatientNumber 
		,SourceEncounterNo = WLCurrent.EpisodeNumber 
		,SuspensionStartDate = Suspend.SuspensionStartDate 

		,SuspensionEndDate =
			coalesce(
				(
				select
					Reinstate.ActivityDate
				from
					[$(PAS)].Inquire.WLACTIVITY Reinstate
				where
					Reinstate.EpisodeNumber = Suspend.EpisodeNumber
				and	Reinstate.InternalPatientNumber = Suspend.InternalPatientNumber
				and	Reinstate.EpsActvTypeInt = 3
				and	Reinstate.EpsActvDtimeInt > Suspend.EpsActvDtimeInt
				and	not exists
					(
					select
						1
					from
						[$(PAS)].Inquire.WLACTIVITY LaterReinstate
					where
						LaterReinstate.EpisodeNumber = Reinstate.EpisodeNumber
					and	LaterReinstate.InternalPatientNumber = Reinstate.InternalPatientNumber
					and	LaterReinstate.EpsActvTypeInt = Reinstate.EpsActvTypeInt
					and	LaterReinstate.EpsActvDtimeInt > Suspend.EpsActvDtimeInt
					and	LaterReinstate.EpsActvDtimeInt < Reinstate.EpsActvDtimeInt
					)
				)
				,Suspend.SuspendedUntil_1
			)

		,SuspensionReasonCode = Suspend.SuspReason 
		,SuspensionReason = Suspend.Reason 
		,SourceUniqueID = Suspend.WLSUSPENSIONID 
	FROM
		[$(PAS)].Inquire.WLCURRENT WLCurrent

	INNER JOIN [$(PAS)].Inquire.WLSUSPENSION Suspend 
	ON	WLCurrent.InternalPatientNumber = Suspend.InternalPatientNumber
	AND	WLCurrent.EpisodeNumber = Suspend.EpisodeNumber

	WHERE
		WLCurrent.EpsCurrActvStsInt between 1 and 2

	union all

	SELECT
		 SourcePatientNo = WLCurrent.InternalPatientNumber 
		,SourceEncounterNo = WLCurrent.EpisodeNumber 
		,SuspensionStartDate = Suspend.SuspensionStartDate 

		,SuspensionEndDate =
			coalesce(
				(
				select
					Reinstate.ActivityDate
				from
					[$(PAS)].Inquire.WLACTIVITY Reinstate
				where
					Reinstate.EpisodeNumber = Suspend.EpisodeNumber
				and	Reinstate.InternalPatientNumber = Suspend.InternalPatientNumber
				and	Reinstate.EpsActvTypeInt = 3
				and	Reinstate.EpsActvDtimeInt > Suspend.EpsActvDtimeInt
				and	not exists
					(
					select
						1
					from
						[$(PAS)].Inquire.WLACTIVITY LaterReinstate
					where
						LaterReinstate.EpisodeNumber = Reinstate.EpisodeNumber
					and	LaterReinstate.InternalPatientNumber = Reinstate.InternalPatientNumber
					and	LaterReinstate.EpsActvTypeInt = Reinstate.EpsActvTypeInt
					and	LaterReinstate.EpsActvDtimeInt > Suspend.EpsActvDtimeInt
					and	LaterReinstate.EpsActvDtimeInt < Reinstate.EpsActvDtimeInt
					)
				)
				,Suspend.SuspendedUntil_1
			)
		,SuspensionReasonCode = Suspend.SuspReason 
		,SuspensionReason = Suspend.Reason 
		,SourceUniqueID = Suspend.WLSUSPENSIONID 
	FROM
		[$(PAS)].Inquire.CURRENTPREADMISSIONS WLCurrent

	INNER JOIN [$(PAS)].Inquire.WLSUSPENSION Suspend 
	ON	WLCurrent.InternalPatientNumber = Suspend.InternalPatientNumber
	AND	WLCurrent.EpisodeNumber = Suspend.EpisodeNumber

	WHERE
		WLCurrent.IntdMgmt In ('I','D')
	AND	WLCurrent.MethodOfAdm In ('WL','BA','BL','PL','BP','EL','PA')

	) Suspension


SELECT @RowsInserted = @RowsInserted + @@ROWCOUNT

-- if the WLCurrent snapshot contains duplicate records keep this snapshot so that the entire Waiting List dataset can be loaded later

-- this block is redundant now that the WLCurrent load removes duplicates
-- delete this bit when the load process is tested satisfactorily
--
--if
--	(
--	select
--		count(*)
--	from
--		TImportWLCurrentKeyViolation
--	where
--		CensusDate = (select top 1 CensusDate from TImportWLSuspension)
--	) != 0
--
--begin
--	delete from TImportWLSuspensionKeyViolation
--	where
--		exists
--			(
--			select
--				1
--			from
--				TImportWLSuspension
--			where
--				TImportWLSuspensionKeyViolation.CensusDate = TImportWLSuspension.CensusDate
--			)
--
--	set identity_insert TImportWLSuspensionKeyViolation on
--	
--	INSERT INTO WH.dbo.TImportWLSuspensionKeyViolation
--		(
--		 EncounterRecno
--		,SourcePatientNo
--		,SourceEncounterNo
--		,CensusDate
--		,SuspensionStartDate
--		,SuspensionEndDate
--		,SuspensionReasonCode
--		,SuspensionReason
--		,SourceUniqueID
--		)
--	SELECT
--		 EncounterRecno
--		,SourcePatientNo
--		,SourceEncounterNo
--		,CensusDate
--		,SuspensionStartDate
--		,SuspensionEndDate
--		,SuspensionReasonCode
--		,SuspensionReason
--		,SourceUniqueID
--	FROM
--		TImportWLSuspension
--
--	set identity_insert TImportWLSuspensionKeyViolation off
--
--end


SELECT @Elapsed = DATEDIFF(minute, @StartTime, getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Census Date ' + CONVERT(varchar(11), @CensusDate) +
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireAPCWLSuspension', @Stats, @StartTime

﻿
CREATE PROCEDURE [ETL].[LoadObservationAdmission]

as 

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 @LoadStartDate = min(LastModifiedTime)
	,@LoadEndDate = max(LastModifiedTime) 
from
	ETL.TLoadObservationAdmission

merge
	Observation.Admission target
using
	(
	select
		 SourceUniqueID
		,CasenoteNumber = cast(CasenoteNumber as varchar(20))
		,DateOfBirth = cast(DateOfBirth as date)
		,SexID = cast(SexID as int)
		,Surname = cast(Surname as varchar(50))
		,Forename = cast(Forename as varchar(50))
		,AdmissionTime = cast(AdmissionTime as datetime)
		,StartSpecialtyID = cast(StartSpecialtyID as int)
		,StartLocationID = cast(StartLocationID as int)
		,DischargeTime = cast(DischargeTime as datetime)
		,EndSpecialtyID = cast(EndSpecialtyID as int)
		,EndLocationID = cast(EndLocationID as int)
		,TreatmentOutcomeID = cast(TreatmentOutcomeID as int)
		,DischargeDestinationID = cast(DischargeDestinationID as int)
		,Comment = cast(Comment as varchar(max))
		,CreatedByUserID = cast(CreatedByUserID as int)
		,CreatedTime = cast(CreatedTime as datetime)
		,LastModifiedByUserID = cast(LastModifiedByUserID as int)
		,LastModifiedTime = cast(LastModifiedTime as datetime)
		,InterfaceCode = cast(InterfaceCode as varchar(11))
	from
		ETL.TLoadObservationAdmission

	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.LastModifiedTime between @LoadStartDate and @LoadEndDate

	then delete	

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SexID
			,Surname
			,Forename
			,AdmissionTime
			,StartSpecialtyID
			,StartLocationID
			,DischargeTime
			,EndSpecialtyID
			,EndLocationID
			,TreatmentOutcomeID
			,DischargeDestinationID
			,Comment
			,CreatedByUserID
			,CreatedTime
			,LastModifiedByUserID
			,LastModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SexID
			,source.Surname
			,source.Forename
			,source.AdmissionTime
			,source.StartSpecialtyID
			,source.StartLocationID
			,source.DischargeTime
			,source.EndSpecialtyID
			,source.EndLocationID
			,source.TreatmentOutcomeID
			,source.DischargeDestinationID
			,source.Comment
			,source.CreatedByUserID
			,source.CreatedTime
			,source.LastModifiedByUserID
			,source.LastModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not 
			isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SexID = source.SexID
			,target.Surname = source.Surname
			,target.Forename = source.Forename
			,target.AdmissionTime = source.AdmissionTime
			,target.StartSpecialtyID = source.StartSpecialtyID
			,target.StartLocationID = source.StartLocationID
			,target.DischargeTime = source.DischargeTime
			,target.EndSpecialtyID = source.EndSpecialtyID
			,target.EndLocationID = source.EndLocationID
			,target.TreatmentOutcomeID = source.TreatmentOutcomeID
			,target.DischargeDestinationID = source.DischargeDestinationID
			,target.Comment = source.Comment
			,target.CreatedByUserID = source.CreatedByUserID
			,target.CreatedTime = source.CreatedTime
			,target.LastModifiedByUserID = source.LastModifiedByUserID
			,target.LastModifiedTime = source.LastModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime


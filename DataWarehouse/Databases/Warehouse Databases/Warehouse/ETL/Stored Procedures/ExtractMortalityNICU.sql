﻿CREATE procedure [ETL].[ExtractMortalityNICU]
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Mortality.NICU target
using
	(
	select
		 SourceUniqueID
		,FormTypeID
		,ReviewStatus
		,ReviewedDate
		,ReviewedBy
		,CasenoteNo
		,Age
		,DateOfDeath
		,NamedConsultant
		,PatientName
		,TerminalCareDeathExpected
		,TerminalCareDeathExpectedComment
		,DeathPreventable
		,TransferComment
		,Postmortem
		,PostmortemComment
		,MedicationErrors
		,MedicationErrorsComment
		,CommunicationComment
		,AnythingCouldBeDoneDifferentlyComment
		,NotableGoodQualityCare
		,LessonsLearned
		,ReferenceNo
		,Gestation
		,BirthWeight
		,DateOfBirth
		,Sex
		,Ethnicity
		,MRSA
		,CriticalIncident
		,CriticalIncidentDetail
		,WithdrawalOfIC
		,WithdrawalOfICDetail
		,WithdrawalOfICLOTADNRFormsUsed
		,SeverelyAbnormalCUSS
		,ConsultantDiscussionB4WDofIC
		,BereavementAppointmentSent
		,BereavementAppointmentSentDetail
		,ClinicalSummaryBackground
		,ClinicalSummaryProgress
		,ClinicalSummaryManagement
		,ClinicalSummarySurgical
		,DiscussionObstetricIssues
		,DiscussionResus
		,DiscussionFirst48Hours
		,DiscussionClinicalManagement
		,DiscussionCommunication
		,DiscussionDocumentation
		,SourcePatientNo
		,EpisodeStartTime
		,ContextCode
	from
		ETL.TLoadMortalityNICU
	where 
		not exists
			(
			Select
				1
			from 
				ETL.TLoadMortalityNICU Latest
			where
				Latest.SourcePatientNo = TLoadMortalityNICU.SourcePatientNo
			and Latest.SourceUniqueID > TLoadMortalityNICU.SourceUniqueID
			)
	) source
	on	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,FormTypeID
			,ReviewStatus
			,ReviewedDate
			,ReviewedBy
			,CasenoteNo
			,Age
			,DateOfDeath
			,NamedConsultant
			,PatientName
			,TerminalCareDeathExpected
			,TerminalCareDeathExpectedComment
			,DeathPreventable
			,TransferComment
			,Postmortem
			,PostmortemComment
			,MedicationErrors
			,MedicationErrorsComment
			,CommunicationComment
			,AnythingCouldBeDoneDifferentlyComment
			,NotableGoodQualityCare
			,LessonsLearned
			,ReferenceNo
			,Gestation
			,BirthWeight
			,DateOfBirth
			,Sex
			,Ethnicity
			,MRSA
			,CriticalIncident
			,CriticalIncidentDetail
			,WithdrawalOfIC
			,WithdrawalOfICDetail
			,WithdrawalOfICLOTADNRFormsUsed
			,SeverelyAbnormalCUSS
			,ConsultantDiscussionB4WDofIC
			,BereavementAppointmentSent
			,BereavementAppointmentSentDetail
			,ClinicalSummaryBackground
			,ClinicalSummaryProgress
			,ClinicalSummaryManagement
			,ClinicalSummarySurgical
			,DiscussionObstetricIssues
			,DiscussionResus
			,DiscussionFirst48Hours
			,DiscussionClinicalManagement
			,DiscussionCommunication
			,DiscussionDocumentation
			,SourcePatientNo
			,EpisodeStartTime
			,ContextCode
			,Created
			,ByWhom
			)
		values
			(
			 source.SourceUniqueID
			,source.FormTypeID
			,source.ReviewStatus
			,source.ReviewedDate
			,source.ReviewedBy
			,source.CasenoteNo
			,source.Age
			,source.DateOfDeath
			,source.NamedConsultant
			,source.PatientName
			,source.TerminalCareDeathExpected
			,source.TerminalCareDeathExpectedComment
			,source.DeathPreventable
			,source.TransferComment
			,source.Postmortem
			,source.PostmortemComment
			,source.MedicationErrors
			,source.MedicationErrorsComment
			,source.CommunicationComment
			,source.AnythingCouldBeDoneDifferentlyComment
			,source.NotableGoodQualityCare
			,source.LessonsLearned
			,source.ReferenceNo
			,source.Gestation
			,source.BirthWeight
			,source.DateOfBirth
			,source.Sex
			,source.Ethnicity
			,source.MRSA
			,source.CriticalIncident
			,source.CriticalIncidentDetail
			,source.WithdrawalOfIC
			,source.WithdrawalOfICDetail
			,source.WithdrawalOfICLOTADNRFormsUsed
			,source.SeverelyAbnormalCUSS
			,source.ConsultantDiscussionB4WDofIC
			,source.BereavementAppointmentSent
			,source.BereavementAppointmentSentDetail
			,source.ClinicalSummaryBackground
			,source.ClinicalSummaryProgress
			,source.ClinicalSummaryManagement
			,source.ClinicalSummarySurgical
			,source.DiscussionObstetricIssues
			,source.DiscussionResus
			,source.DiscussionFirst48Hours
			,source.DiscussionClinicalManagement
			,source.DiscussionCommunication
			,source.DiscussionDocumentation
			,source.SourcePatientNo
			,source.EpisodeStartTime
			,source.ContextCode
			,getdate()
			,system_user
			)

	when matched
	and not
		(
			isnull(target.FormTypeID, 0) = isnull(source.FormTypeID, 0)
		and	isnull(target.ReviewStatus, 0) = isnull(source.ReviewStatus, 0)
		and	isnull(target.ReviewedDate, getdate()) = isnull(source.ReviewedDate, getdate())
		and isnull(target.ReviewedBy, '') = isnull(source.ReviewedBy, '') 
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.Age, 0) = isnull(source.Age, 0)
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.NamedConsultant, '') = isnull(source.NamedConsultant, '')
		and isnull(target.PatientName, '') = isnull(source.PatientName, '')
		and isnull(target.TerminalCareDeathExpected, 0) = isnull(source.TerminalCareDeathExpected, 0)
		and isnull(target.TerminalCareDeathExpectedComment, '') = isnull(source.TerminalCareDeathExpectedComment, '')
		and isnull(target.DeathPreventable, 0) = isnull(source.DeathPreventable, 0)
		and isnull(target.TransferComment, '') = isnull(source.TransferComment, '')
		and isnull(target.Postmortem, '') = isnull(source.Postmortem, '')
		and isnull(target.PostmortemComment, '') = isnull(source.PostmortemComment, '')
		and isnull(target.MedicationErrors, 0) = isnull(source.MedicationErrors, 0)
		and isnull(target.MedicationErrorsComment, '') = isnull(source.MedicationErrorsComment, '')
		and isnull(target.CommunicationComment, '') = isnull(source.CommunicationComment, '')
		and isnull(target.AnythingCouldBeDoneDifferentlyComment, '') = isnull(source.AnythingCouldBeDoneDifferentlyComment, '')
		and isnull(target.NotableGoodQualityCare, '') = isnull(source.NotableGoodQualityCare, '')
		and isnull(target.LessonsLearned, '') = isnull(source.LessonsLearned, '')
		and isnull(target.ReferenceNo, '') = isnull(source.ReferenceNo, '')
		and isnull(target.Gestation, '') = isnull(source.Gestation, '')
		and isnull(target.BirthWeight, '') = isnull(source.BirthWeight, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.Sex, '') = isnull(source.Sex, '')
		and isnull(target.Ethnicity, '') = isnull(source.Ethnicity, '')
		and isnull(target.MRSA, 0) = isnull(source.MRSA, 0)
		and isnull(target.CriticalIncident, 0) = isnull(source.CriticalIncident, 0)
		and isnull(target.CriticalIncidentDetail, '') = isnull(source.CriticalIncidentDetail, '')
		and isnull(target.WithdrawalOfIC, 0) = isnull(source.WithdrawalOfIC, 0)
		and isnull(target.WithdrawalOfICDetail, '') = isnull(source.WithdrawalOfICDetail, '')
		and isnull(target.WithdrawalOfICLOTADNRFormsUsed, 0) = isnull(source.WithdrawalOfICLOTADNRFormsUsed, 0)
		and isnull(target.SeverelyAbnormalCUSS, 0) = isnull(source.SeverelyAbnormalCUSS, 0)
		and isnull(target.ConsultantDiscussionB4WDofIC, 0) = isnull(source.ConsultantDiscussionB4WDofIC, 0)
		and isnull(target.BereavementAppointmentSent, 0) = isnull(source.BereavementAppointmentSent, 0)
		and isnull(target.BereavementAppointmentSentDetail, '') = isnull(source.BereavementAppointmentSentDetail, '')
		and isnull(target.ClinicalSummaryBackground, '') = isnull(source.ClinicalSummaryBackground, '')
		and isnull(target.ClinicalSummaryProgress, '') = isnull(source.ClinicalSummaryProgress, '')
		and isnull(target.ClinicalSummaryManagement, '') = isnull(source.ClinicalSummaryManagement, '')
		and isnull(target.ClinicalSummarySurgical, '') = isnull(source.ClinicalSummarySurgical, '')
		and isnull(target.DiscussionObstetricIssues, '') = isnull(source.DiscussionObstetricIssues, '')
		and isnull(target.DiscussionResus, '') = isnull(source.DiscussionResus, '')
		and isnull(target.DiscussionFirst48Hours, '') = isnull(source.DiscussionFirst48Hours, '')
		and isnull(target.DiscussionClinicalManagement, '') = isnull(source.DiscussionClinicalManagement, '')
		and isnull(target.DiscussionCommunication, '') = isnull(source.DiscussionCommunication, '')
		and isnull(target.DiscussionDocumentation, '') = isnull(source.DiscussionDocumentation, '')
		and isnull(target.SourcePatientNo, 0) = isnull(source.SourcePatientNo, 0)
		and isnull(target.EpisodeStartTime, getdate()) = isnull(source.EpisodeStartTime, getdate())
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set	
			 target.FormTypeID = source.FormTypeID
			,target.ReviewStatus = source.ReviewStatus
			,target.ReviewedDate = source.ReviewedDate
			,target.ReviewedBy = source.ReviewedBy
			,target.CasenoteNo = source.CasenoteNo
			,target.Age = source.Age
			,target.DateOfDeath = source.DateOfDeath
			,target.NamedConsultant = source.NamedConsultant
			,target.PatientName = source.PatientName
			,target.TerminalCareDeathExpected = source.TerminalCareDeathExpected
			,target.TerminalCareDeathExpectedComment = source.TerminalCareDeathExpectedComment
			,target.DeathPreventable = source.DeathPreventable
			,target.TransferComment = source.TransferComment
			,target.Postmortem = source.Postmortem
			,target.PostmortemComment = source.PostmortemComment
			,target.MedicationErrors = source.MedicationErrors
			,target.MedicationErrorsComment = source.MedicationErrorsComment
			,target.CommunicationComment = source.CommunicationComment
			,target.AnythingCouldBeDoneDifferentlyComment = source.AnythingCouldBeDoneDifferentlyComment
			,target.NotableGoodQualityCare = source.NotableGoodQualityCare
			,target.LessonsLearned = source.LessonsLearned
			,target.ReferenceNo = source.ReferenceNo
			,target.Gestation = source.Gestation
			,target.BirthWeight = source.BirthWeight
			,target.DateOfBirth = source.DateOfBirth
			,target.Sex = source.Sex
			,target.Ethnicity = source.Ethnicity
			,target.MRSA = source.MRSA
			,target.CriticalIncident = source.CriticalIncident
			,target.CriticalIncidentDetail = source.CriticalIncidentDetail
			,target.WithdrawalOfIC = source.WithdrawalOfIC
			,target.WithdrawalOfICDetail = source.WithdrawalOfICDetail
			,target.WithdrawalOfICLOTADNRFormsUsed = source.WithdrawalOfICLOTADNRFormsUsed
			,target.SeverelyAbnormalCUSS = source.SeverelyAbnormalCUSS
			,target.ConsultantDiscussionB4WDofIC = source.ConsultantDiscussionB4WDofIC
			,target.BereavementAppointmentSent = source.BereavementAppointmentSent
			,target.BereavementAppointmentSentDetail = source.BereavementAppointmentSentDetail
			,target.ClinicalSummaryBackground = source.ClinicalSummaryBackground
			,target.ClinicalSummaryProgress = source.ClinicalSummaryProgress
			,target.ClinicalSummaryManagement = source.ClinicalSummaryManagement
			,target.ClinicalSummarySurgical = source.ClinicalSummarySurgical
			,target.DiscussionObstetricIssues = source.DiscussionObstetricIssues
			,target.DiscussionResus = source.DiscussionResus
			,target.DiscussionFirst48Hours = source.DiscussionFirst48Hours
			,target.DiscussionClinicalManagement = source.DiscussionClinicalManagement
			,target.DiscussionCommunication = source.DiscussionCommunication
			,target.DiscussionDocumentation = source.DiscussionDocumentation
			,target.SourcePatientNo = source.SourcePatientNo
			,target.EpisodeStartTime = source.EpisodeStartTime
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = system_user

output
	$action into @MergeSummary
;

if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Net change ' + CONVERT(varchar(10), @inserted - @deleted) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'

print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



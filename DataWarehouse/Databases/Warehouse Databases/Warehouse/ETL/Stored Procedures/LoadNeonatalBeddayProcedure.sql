﻿
CREATE proc [ETL].[LoadNeonatalBeddayProcedure]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime


select @FromDate = (select min(ModifiedTime) from ETL.TLoadNeonatalBeddayProcedure)
select @ToDate = (select max(ModifiedTime) from ETL.TLoadNeonatalBeddayProcedure)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Neonatal.BeddayProcedure target
using
	(
	select
		 EpisodeID
		,CareDate
		,ProcedureCode
		,ModifiedTime
		,InterfaceCode
	from
		ETL.TLoadNeonatalBeddayProcedure

	) source
	on	source.EpisodeID = target.EpisodeID
	and	source.CareDate = target.CareDate
	and	source.ProcedureCode = target.ProcedureCode
	
	when not matched by source
	and target.ModifiedTime between @FromDate and @ToDate

	then delete

	when not matched
	then
		insert
			(
			 EpisodeID
			,CareDate
			,ProcedureCode
			,ModifiedTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.EpisodeID
			,source.CareDate
			,source.ProcedureCode
			,source.ModifiedTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	
			isnull(cast(target.EpisodeID as varchar(100)), '') = isnull(cast(source.EpisodeID as varchar(100)), '')
		and isnull(target.CareDate, getdate()) = isnull(source.CareDate, getdate())
		and isnull(target.ProcedureCode, 0) = isnull(source.ProcedureCode, 0)
		and isnull(target.ModifiedTime, getdate()) = isnull(source.ModifiedTime, getdate())
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		)
	then
		update
		set
			 target.EpisodeID = source.EpisodeID
			,target.CareDate = source.CareDate
			,target.ProcedureCode = source.ProcedureCode
			,target.ModifiedTime = source.ModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime










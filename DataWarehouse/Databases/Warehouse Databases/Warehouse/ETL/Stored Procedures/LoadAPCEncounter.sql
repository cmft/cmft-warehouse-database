﻿CREATE PROCEDURE [ETL].[LoadAPCEncounter]

as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select
	@StartTime = getdate()

/* If no Load Start and End Date parameters supplied, use the earliest Episode End Date
   and the latest Episode Start Date */
select
	 @LoadStartDate = MIN(CONVERT(datetime, EpisodeEndDate)) 
	,@LoadEndDate = MAX(CONVERT(datetime, EpisodeStartDate)) 
from
	ETL.TLoadAPCEncounter

delete from APC.Encounter 
where	
	(
		Encounter.EpisodeEndDate >= @LoadStartDate
	or	Encounter.EpisodeEndDate IS NULL
	)
and	Encounter.EpisodeStartDate <= @LoadEndDate
and	not exists
	(
	select
		1
	from
		ETL.TLoadAPCEncounter
	where
		TLoadAPCEncounter.SourceUniqueID = Encounter.SourceUniqueID
	)


SELECT @RowsDeleted = @@Rowcount

update APC.Encounter
set
	 SourcePatientNo = TEncounter.SourcePatientNo
	,SourceSpellNo = TEncounter.SourceSpellNo
	,SourceEncounterNo = TEncounter.SourceEncounterNo
	,ProviderSpellNo = TEncounter.ProviderSpellNo
	,PatientTitle = TEncounter.PatientTitle
	,PatientForename = TEncounter.PatientForename
	,PatientSurname = TEncounter.PatientSurname
	,DateOfBirth = TEncounter.DateOfBirth
	,DateOfDeath = TEncounter.DateOfDeath
	,SexCode = TEncounter.SexCode
	,NHSNumber = TEncounter.NHSNumber
	,Postcode = TEncounter.Postcode
	,PatientAddress1 = TEncounter.PatientAddress1
	,PatientAddress2 = TEncounter.PatientAddress2
	,PatientAddress3 = TEncounter.PatientAddress3
	,PatientAddress4 = TEncounter.PatientAddress4
	,DHACode = TEncounter.DHACode
	,EthnicOriginCode = TEncounter.EthnicOriginCode
	,MaritalStatusCode = TEncounter.MaritalStatusCode
	,ReligionCode = TEncounter.ReligionCode
	,DateOnWaitingList = TEncounter.DateOnWaitingList
	,AdmissionDate = TEncounter.AdmissionDate
	,DischargeDate = TEncounter.DischargeDate
	,EpisodeStartDate = TEncounter.EpisodeStartDate
	,EpisodeEndDate = TEncounter.EpisodeEndDate
	,StartSiteCode = TEncounter.StartSiteCode
	,StartWardTypeCode = TEncounter.StartWardTypeCode
	,EndSiteCode = TEncounter.EndSiteCode
	,EndWardTypeCode = TEncounter.EndWardTypeCode
	,RegisteredGpCode = TEncounter.RegisteredGpCode
	,RegisteredGpPracticeCode = TEncounter.RegisteredGpPracticeCode
	,SiteCode = TEncounter.SiteCode
	,AdmissionMethodCode = TEncounter.AdmissionMethodCode
	,AdmissionSourceCode = TEncounter.AdmissionSourceCode
	,PatientClassificationCode = TEncounter.PatientClassificationCode
	,ManagementIntentionCode = TEncounter.ManagementIntentionCode
	,DischargeMethodCode = TEncounter.DischargeMethodCode
	,DischargeDestinationCode = TEncounter.DischargeDestinationCode
	,AdminCategoryCode = TEncounter.AdminCategoryCode
	,ConsultantCode = TEncounter.ConsultantCode
	,SpecialtyCode = TEncounter.SpecialtyCode
	,LastEpisodeInSpellIndicator = TEncounter.LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit = TEncounter.FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare = TEncounter.NeonatalLevelOfCare
	,IsWellBabyFlag = TEncounter.IsWellBabyFlag
	,PASHRGCode = TEncounter.PASHRGCode
	,ClinicalCodingStatus = TEncounter.ClinicalCodingStatus
	,ClinicalCodingCompleteDate = TEncounter.ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime = TEncounter.ClinicalCodingCompleteTime
	,PrimaryDiagnosisCode = TEncounter.PrimaryDiagnosisCode
	,PrimaryDiagnosisDate = TEncounter.PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode = TEncounter.SubsidiaryDiagnosisCode
	--,SecondaryDiagnosisCode1 = TEncounter.SecondaryDiagnosisCode1
	--,SecondaryDiagnosisCode2 = TEncounter.SecondaryDiagnosisCode2
	--,SecondaryDiagnosisCode3 = TEncounter.SecondaryDiagnosisCode3
	--,SecondaryDiagnosisCode4 = TEncounter.SecondaryDiagnosisCode4
	--,SecondaryDiagnosisCode5 = TEncounter.SecondaryDiagnosisCode5
	--,SecondaryDiagnosisCode6 = TEncounter.SecondaryDiagnosisCode6
	--,SecondaryDiagnosisCode7 = TEncounter.SecondaryDiagnosisCode7
	--,SecondaryDiagnosisCode8 = TEncounter.SecondaryDiagnosisCode8
	--,SecondaryDiagnosisCode9 = TEncounter.SecondaryDiagnosisCode9
	--,SecondaryDiagnosisCode10 = TEncounter.SecondaryDiagnosisCode10
	--,SecondaryDiagnosisCode11 = TEncounter.SecondaryDiagnosisCode11
	--,SecondaryDiagnosisCode12 = TEncounter.SecondaryDiagnosisCode12
	,PrimaryOperationCode = TEncounter.PrimaryOperationCode
	,PrimaryOperationDate = TEncounter.PrimaryOperationDate
	--,SecondaryOperationCode1 = TEncounter.SecondaryOperationCode1
	--,SecondaryOperationDate1 = TEncounter.SecondaryOperationDate1
	--,SecondaryOperationCode2 = TEncounter.SecondaryOperationCode2
	--,SecondaryOperationDate2 = TEncounter.SecondaryOperationDate2
	--,SecondaryOperationCode3 = TEncounter.SecondaryOperationCode3
	--,SecondaryOperationDate3 = TEncounter.SecondaryOperationDate3
	--,SecondaryOperationCode4 = TEncounter.SecondaryOperationCode4
	--,SecondaryOperationDate4 = TEncounter.SecondaryOperationDate4
	--,SecondaryOperationCode5 = TEncounter.SecondaryOperationCode5
	--,SecondaryOperationDate5 = TEncounter.SecondaryOperationDate5
	--,SecondaryOperationCode6 = TEncounter.SecondaryOperationCode6
	--,SecondaryOperationDate6 = TEncounter.SecondaryOperationDate6
	--,SecondaryOperationCode7 = TEncounter.SecondaryOperationCode7
	--,SecondaryOperationDate7 = TEncounter.SecondaryOperationDate7
	--,SecondaryOperationCode8 = TEncounter.SecondaryOperationCode8
	--,SecondaryOperationDate8 = TEncounter.SecondaryOperationDate8
	--,SecondaryOperationCode9 = TEncounter.SecondaryOperationCode9
	--,SecondaryOperationDate9 = TEncounter.SecondaryOperationDate9
	--,SecondaryOperationCode10 = TEncounter.SecondaryOperationCode10
	--,SecondaryOperationDate10 = TEncounter.SecondaryOperationDate10
	--,SecondaryOperationCode11 = TEncounter.SecondaryOperationCode11
	--,SecondaryOperationDate11 = TEncounter.SecondaryOperationDate11
	,OperationStatusCode = TEncounter.OperationStatusCode
	,ContractSerialNo = TEncounter.ContractSerialNo
	,CodingCompleteDate = TEncounter.CodingCompleteDate
	,PurchaserCode = TEncounter.PurchaserCode
	,ProviderCode = TEncounter.ProviderCode
	,EpisodeStartTime = TEncounter.EpisodeStartTime
	,EpisodeEndTime = TEncounter.EpisodeEndTime
	,RegisteredGdpCode = TEncounter.RegisteredGdpCode
	,EpisodicGpCode = TEncounter.EpisodicGpCode
	,InterfaceCode = TEncounter.InterfaceCode
	,CasenoteNumber = TEncounter.CasenoteNumber
	,NHSNumberStatusCode = TEncounter.NHSNumberStatusCode
	,AdmissionTime = TEncounter.AdmissionTime
	,DischargeTime = TEncounter.DischargeTime
	,TransferFrom = TEncounter.TransferFrom
	,DistrictNo = TEncounter.DistrictNo
	,ExpectedLOS = TEncounter.ExpectedLOS
	,MRSAFlag = TEncounter.MRSAFlag
	,RTTPathwayID = TEncounter.RTTPathwayID
	,RTTPathwayCondition = TEncounter.RTTPathwayCondition
	,RTTStartDate = TEncounter.RTTStartDate
	,RTTEndDate = TEncounter.RTTEndDate
	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
	,RTTPeriodStatusCode = TEncounter.RTTPeriodStatusCode
	,IntendedPrimaryOperationCode = TEncounter.IntendedPrimaryOperationCode
	,Operation = TEncounter.Operation
	,Research1 = TEncounter.Research1
	,Research2 = TEncounter.Research2
	,CancelledElectiveAdmissionFlag = TEncounter.CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode = TEncounter.LocalAdminCategoryCode
	,EpisodicGpPracticeCode = TEncounter.EpisodicGpPracticeCode
	,PCTCode = TEncounter.PCTCode
	,LocalityCode = TEncounter.LocalityCode
	--,FirstEpisodeInSpellIndicator = TEncounter.FirstEpisodeInSpellIndicator
	,DischargeReadyDate = TEncounter.DischargeReadyDate
	,DischargeWaitReasonCode = TEncounter.DischargeWaitReasonCode
	,ReferredByCode = TEncounter.ReferredByCode
	,ReferrerCode = TEncounter.ReferrerCode
	,DecidedToAdmitDate = TEncounter.DecidedToAdmitDate
	,ResidencePCTCode = TEncounter.ResidencePCTCode
	,PatientCategoryCode = TEncounter.PatientCategoryCode
	,CarerSupportIndicator = TEncounter.CarerSupportIndicator
	,PseudoPostCode = TEncounter.PseudoPostCode
	,CCGCode = TEncounter.CCGCode
	,GpCodeAtDischarge = TEncounter.GpCodeAtDischarge
	,GpPracticeCodeAtDischarge = TEncounter.GpPracticeCodeAtDischarge
	,PostcodeAtDischarge = TEncounter.PostcodeAtDischarge
	,Updated = getdate()
	,ByWhom = system_user
from
	ETL.TLoadAPCEncounter TEncounter
where
	TEncounter.SourceUniqueID = APC.Encounter.SourceUniqueID

select @RowsUpdated = @@rowcount


INSERT INTO APC.Encounter
	(
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,IsWellBabyFlag
	,PASHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode
	,EpisodicGpPracticeCode
	,PCTCode
	,LocalityCode
	,FirstEpisodeInSpellIndicator
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
	,DecidedToAdmitDate
	,ResidencePCTCode
	,PatientCategoryCode
	,CarerSupportIndicator
	,PseudoPostCode
	,CCGCode
	,GpCodeAtDischarge
	,GpPracticeCodeAtDischarge 
	,PostcodeAtDischarge 
	,Created
	,Updated
	,ByWhom
	) 
select
	 SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,DateOnWaitingList
	,AdmissionDate
	,DischargeDate
	,EpisodeStartDate
	,EpisodeEndDate
	,StartSiteCode
	,StartWardTypeCode
	,EndSiteCode
	,EndWardTypeCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AdmissionMethodCode
	,AdmissionSourceCode
	,PatientClassificationCode
	,ManagementIntentionCode
	,DischargeMethodCode
	,DischargeDestinationCode
	,AdminCategoryCode
	,ConsultantCode
	,SpecialtyCode
	,LastEpisodeInSpellIndicator
	,FirstRegDayOrNightAdmit
	,NeonatalLevelOfCare
	,IsWellBabyFlag
	,PASHRGCode
	,ClinicalCodingStatus
	,ClinicalCodingCompleteDate
	,ClinicalCodingCompleteTime
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisDate
	,SubsidiaryDiagnosisCode
	,SecondaryDiagnosisCode1
	,SecondaryDiagnosisCode2
	,SecondaryDiagnosisCode3
	,SecondaryDiagnosisCode4
	,SecondaryDiagnosisCode5
	,SecondaryDiagnosisCode6
	,SecondaryDiagnosisCode7
	,SecondaryDiagnosisCode8
	,SecondaryDiagnosisCode9
	,SecondaryDiagnosisCode10
	,SecondaryDiagnosisCode11
	,SecondaryDiagnosisCode12
	,PrimaryOperationCode
	,PrimaryOperationDate
	,SecondaryOperationCode1
	,SecondaryOperationDate1
	,SecondaryOperationCode2
	,SecondaryOperationDate2
	,SecondaryOperationCode3
	,SecondaryOperationDate3
	,SecondaryOperationCode4
	,SecondaryOperationDate4
	,SecondaryOperationCode5
	,SecondaryOperationDate5
	,SecondaryOperationCode6
	,SecondaryOperationDate6
	,SecondaryOperationCode7
	,SecondaryOperationDate7
	,SecondaryOperationCode8
	,SecondaryOperationDate8
	,SecondaryOperationCode9
	,SecondaryOperationDate9
	,SecondaryOperationCode10
	,SecondaryOperationDate10
	,SecondaryOperationCode11
	,SecondaryOperationDate11
	,OperationStatusCode
	,ContractSerialNo
	,CodingCompleteDate
	,PurchaserCode
	,ProviderCode
	,EpisodeStartTime
	,EpisodeEndTime
	,RegisteredGdpCode
	,EpisodicGpCode
	,InterfaceCode
	,CasenoteNumber
	,NHSNumberStatusCode
	,AdmissionTime
	,DischargeTime
	,TransferFrom
	,DistrictNo
	,ExpectedLOS
	,MRSAFlag
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,IntendedPrimaryOperationCode
	,Operation
	,Research1
	,Research2
	,CancelledElectiveAdmissionFlag
	,LocalAdminCategoryCode
	,EpisodicGpPracticeCode
	,PCTCode
	,LocalityCode
	,FirstEpisodeInSpellIndicator
	,DischargeReadyDate
	,DischargeWaitReasonCode
	,ReferredByCode
	,ReferrerCode
	,DecidedToAdmitDate
	,ResidencePCTCode
	,PatientCategoryCode
	,CarerSupportIndicator
	,PseudoPostCode
	,CCGCode
	,GpCodeAtDischarge
	,GpPracticeCodeAtDischarge 
	,PostcodeAtDischarge 
	,Created = getdate()
	,Updated = getdate()
	,ByWhom = system_user
from
	ETL.TLoadAPCEncounter
where
	not exists
	(
	select
		1
	from
		APC.Encounter
	where
		Encounter.SourceUniqueID = TLoadAPCEncounter.SourceUniqueID
	)

SELECT @RowsInserted = @@Rowcount

--finally update discharge details on other fces in this spell
--we would normally extract from PAS based on DischargeDate but as there is no useful index
--on INQUIRE we have to use EpisodeEndDate
update
	APC.Encounter
set
	 DateOnWaitingList = TLoadAPCEncounter.DateOnWaitingList
	,AdmissionDate = TLoadAPCEncounter.AdmissionDate
	,DischargeDate = TLoadAPCEncounter.DischargeDate
	,SiteCode = TLoadAPCEncounter.SiteCode
	,AdmissionMethodCode = TLoadAPCEncounter.AdmissionMethodCode
	,AdmissionSourceCode = TLoadAPCEncounter.AdmissionSourceCode
	,ManagementIntentionCode = TLoadAPCEncounter.ManagementIntentionCode
	,DischargeMethodCode = TLoadAPCEncounter.DischargeMethodCode
	,DischargeDestinationCode = TLoadAPCEncounter.DischargeDestinationCode
	,TransferFrom = TLoadAPCEncounter.TransferFrom
	,AdminCategoryCode = TLoadAPCEncounter.AdminCategoryCode
	,LocalAdminCategoryCode = TLoadAPCEncounter.LocalAdminCategoryCode
	,FirstRegDayOrNightAdmit = TLoadAPCEncounter.FirstRegDayOrNightAdmit
	,AdmissionTime = TLoadAPCEncounter.AdmissionTime
	,DischargeTime = TLoadAPCEncounter.DischargeTime
	,ExpectedLOS = TLoadAPCEncounter.ExpectedLOS
	,CancelledElectiveAdmissionFlag = TLoadAPCEncounter.CancelledElectiveAdmissionFlag
	,Updated = GETDATE()
from
	ETL.TLoadAPCEncounter
where
	TLoadAPCEncounter.SourcePatientNo = Encounter.SourcePatientNo
and	TLoadAPCEncounter.SourceSpellNo = Encounter.SourceSpellNo

----now copy RTT Current Status back to historical fces
--update APC.Encounter
--set
--	 RTTPathwayCondition = TEncounter.RTTPathwayCondition
--	,RTTStartDate = TEncounter.RTTStartDate
--	,RTTEndDate = TEncounter.RTTEndDate
--	,RTTSpecialtyCode = TEncounter.RTTSpecialtyCode
--	,RTTCurrentProviderCode = TEncounter.RTTCurrentProviderCode
--	,RTTCurrentStatusCode = TEncounter.RTTCurrentStatusCode
--	,RTTCurrentStatusDate = TEncounter.RTTCurrentStatusDate
--	,RTTCurrentPrivatePatientFlag = TEncounter.RTTCurrentPrivatePatientFlag
--	,RTTOverseasStatusFlag = TEncounter.RTTOverseasStatusFlag
--	,Updated = GETDATE()
--from
--	ETL.TImportAPCEncounter TEncounter
--where
--	TEncounter.RTTPathwayID = Encounter.RTTPathwayID


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
	', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
	', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
	CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
	CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

EXEC Utility.WriteAuditLogEvent 'LoadAPCEncounter', @Stats, @StartTime

print @Stats


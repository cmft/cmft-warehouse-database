﻿
CREATE proc [ETL].[LoadDictationMedisecDocument]

       --@fromDate smalldatetime = null
       --,@toDate smalldatetime = null
as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

select
	 @LoadStartDate = MIN(CONVERT(datetime, TypedTime)) 
	,@LoadEndDate = MAX(CONVERT(datetime, TypedTime)) 
from
	ETL.TLoadDictationMedisecDocument

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Dictation.MedisecDocument target
using
	(
	select
		SourceUniqueID =  cast(SourceUniqueID as varchar(max))
		,SourcePatientNo = cast(SourcePatientNo as varchar(max))
		,DocumentID = cast(DocumentID as varchar(max))
		,DocumentDescription = cast(DocumentDescription as varchar(max))
		,DocumentTypeCode = cast(DocumentTypeCode as varchar(max))
		,AuthorCode = cast(AuthorCode as varchar(max))
		,TypedTime = cast(TypedTime as datetime)
		,DictatedTime = cast(DictatedTime as datetime)
		,DictatedByCode = cast(DictatedByCode as varchar(max))
		,IssuedTime = cast(IssuedTime as datetime)
		,IssuedByCode = cast(IssuedByCode as varchar(max))
		,SignedTime = cast(SignedTime as datetime)
		,SignedByCode = cast(SignedByCode as varchar(max))
		,SignedStatusCode = cast(SignedStatusCode as varchar(max))
		,DepartmentCode
		,DocumentXMLTemplateCode = cast(DocumentXMLTemplateCode as varchar(max))
		,DocumentXML = cast(DocumentXML as varchar(max))
		,TransmissionTime
		,TransmissionDestinationCode
	from
		ETL.TLoadDictationMedisecDocument Document
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	

	when not matched by source
	and	target.TypedTime between @LoadStartDate and @LoadEndDate

	then delete

	when not matched
	then
		insert
			(
			SourceUniqueID
			,SourcePatientNo
			,DocumentID
			,DocumentDescription
			,DocumentTypeCode
			,AuthorCode
			,TypedTime
			,DictatedTime
			,DictatedByCode
			,IssuedTime
			,IssuedByCode
			,SignedTime
			,SignedByCode
			,SignedStatusCode
			,DepartmentCode
			,DocumentXMLTemplateCode
			,DocumentXML
			,TransmissionTime
			,TransmissionDestinationCode
			,Created
			,Updated
			,ByWhom
		  )
		values
			(
			source.SourceUniqueID
			,source.SourcePatientNo
			,source.DocumentID
			,source.DocumentDescription
			,source.DocumentTypeCode
			,source.AuthorCode
			,source.TypedTime
			,source.DictatedTime
			,source.DictatedByCode
			,source.IssuedTime
			,source.IssuedByCode
			,source.SignedTime
			,source.SignedByCode
			,source.SignedStatusCode
			,source.DepartmentCode
			,source.DocumentXMLTemplateCode
			,source.DocumentXML
			,source.TransmissionTime
			,source.TransmissionDestinationCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.DocumentID, '') = isnull(source.DocumentID, '')
		and isnull(target.DocumentDescription, '') = isnull(source.DocumentDescription, '')
		and isnull(target.DocumentTypeCode, '') = isnull(source.DocumentTypeCode, '')
		and isnull(target.AuthorCode, getdate()) = isnull(source.AuthorCode, getdate())
		and isnull(target.TypedTime, getdate()) = isnull(source.TypedTime, getdate())
		and isnull(target.DictatedTime, getdate()) = isnull(source.DictatedTime, getdate())
		and isnull(target.DictatedByCode, '') = isnull(source.DictatedByCode, '')
		and isnull(target.IssuedTime, getdate()) = isnull(source.IssuedTime, getdate())
		and isnull(target.IssuedByCode, '') = isnull(source.IssuedByCode, '')
		and isnull(target.SignedTime, getdate()) = isnull(source.SignedTime, getdate())
		and isnull(target.SignedByCode, '') = isnull(source.SignedByCode, '')
		and isnull(target.SignedStatusCode, '') = isnull(source.SignedStatusCode, '')
		and isnull(target.DepartmentCode, '') = isnull(source.DepartmentCode, '')
		and isnull(target.DocumentXMLTemplateCode, '') = isnull(source.DocumentXMLTemplateCode, '')
		and cast(isnull(target.DocumentXML, '') as varchar(max)) = isnull(source.DocumentXML, '')
		and isnull(target.TransmissionTime, getdate()) = isnull(source.TransmissionTime, getdate())
		and isnull(target.TransmissionDestinationCode, '') = isnull(source.TransmissionDestinationCode, '')
		)
	then
		update
		set
			target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.DocumentID = source.DocumentID
			,target.DocumentDescription = source.DocumentDescription
			,target.DocumentTypeCode = source.DocumentTypeCode
			,target.AuthorCode = source.AuthorCode
			,target.TypedTime = source.TypedTime
			,target.DictatedTime = source.DictatedTime
			,target.DictatedByCode = source.DictatedByCode
			,target.IssuedTime = source.IssuedTime
			,target.IssuedByCode = source.IssuedByCode
			,target.SignedTime = source.SignedTime
			,target.SignedByCode = source.SignedByCode
			,target.SignedStatusCode = source.SignedStatusCode
			,target.DepartmentCode = source.DepartmentCode
			,target.DocumentXMLTemplateCode = source.DocumentXMLTemplateCode
			,target.DocumentXML = source.DocumentXML
			,target.TransmissionTime = source.TransmissionTime
			,target.TransmissionDestinationCode = source.TransmissionDestinationCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




/*

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)
declare @LoadStartDate datetime
declare @LoadEndDate datetime

select
       @StartTime = getdate()

/* If no Load Start and End Date parameters supplied, use the earliest Typed Date
   and the latest Typed Date */

select
       @LoadStartDate = MIN(coalesce(@fromDate, CONVERT(datetime, TypedTime)))
       ,@LoadEndDate = MAX(coalesce(@toDate, CONVERT(datetime, TypedTime)))
from
       ETL.TLoadMedisecDictationDocument


delete from Dictation.MedisecDocument
where       
	not exists
     (
     select
            1
     from
            ETL.TLoadMedisecDictationDocument
     where
            TLoadMedisecDictationDocument.SourceUniqueID = MedisecDocument.SourceUniqueID
     )
and MedisecDocument.TypedTime >= @LoadStartDate


SELECT @RowsDeleted = @@Rowcount


update Dictation.MedisecDocument
set
	SourceUniqueID = TLoadDocument.SourceUniqueID
	,DocumentID = TLoadDocument.DocumentID
	,DocumentDescription = TLoadDocument.DocumentDescription
	,DocumentTypeCode = TLoadDocument.DocumentTypeCode
	,AuthorCode = TLoadDocument.AuthorCode
	,TypedTime = TLoadDocument.TypedTime
	,DictatedTime = TLoadDocument.DictatedTime
	,DictatedByCode = TLoadDocument.DictatedByCode
	,IssuedTime = TLoadDocument.IssuedTime
	,IssuedByCode = TLoadDocument.IssuedByCode
	,SignedTime = TLoadDocument.SignedTime
	,SignedByCode = TLoadDocument.SignedByCode
	,SignedStatusCode = TLoadDocument.SignedStatusCode
	,DocumentXMLTemplateCode = TLoadDocument.DocumentXMLTemplateCode
	,DocumentXML = TLoadDocument.DocumentXML

from
	ETL.TLoadMedisecDictationDocument TLoadDocument
where
	TLoadDocument.SourceUniqueID = Dictation.MedisecDocument.SourceUniqueID

select @RowsUpdated = @@rowcount



insert into Dictation.MedisecDocument
(
	SourceUniqueID
	,DocumentID
	,DocumentDescription
	,DocumentTypeCode
	--,DocumentStatusCode
	,AuthorCode
	,TypedTime
	,DictatedTime
	,DictatedByCode
	,IssuedTime
	,IssuedByCode
	,SignedTime
	,SignedByCode
	,SignedStatusCode
	,DocumentXMLTemplateCode
	,DocumentXML
	,Created
	,ByWhom
)

select
	SourceUniqueID
	,DocumentID
	,DocumentDescription
	,DocumentTypeCode
	--,DocumentStatusCode
	,AuthorCode
	,TypedTime
	,DictatedTime
	,DictatedByCode
	,IssuedTime
	,IssuedByCode
	,SignedTime
	,SignedByCode
	,SignedStatusCode
	,DocumentXMLTemplateCode
	,DocumentXML
    ,Created = getdate()
    ,ByWhom = system_user

from
	ETL.TLoadMedisecDictationDocument

where
	not exists
       (
       select
            1
       from
			Dictation.MedisecDocument
       where
			TLoadMedisecDictationDocument.SourceUniqueID = MedisecDocument.SourceUniqueID
       )


SELECT @RowsInserted = @@Rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Deleted ' + CONVERT(varchar(10), @RowsDeleted)  + 
       ', Updated '  + CONVERT(varchar(10), @RowsUpdated) +  
       ', Inserted '  + CONVERT(varchar(10), @RowsInserted) + ', Net change '  + 
       CONVERT(varchar(10), @RowsInserted - @RowsDeleted) + ', Time Elapsed ' + 
       CONVERT(char(3), @Elapsed) + ' Mins, Period from ' + 
       CONVERT(varchar(11), @LoadStartDate) + ' to ' + CONVERT(varchar(11), @LoadEndDate)

--EXEC WriteAuditLogEvent 'LoadMedisecDocument', @Stats, @StartTime

print @Stats

*/




﻿
CREATE procedure [ETL].[LoadRADExtractRename]
(
	@SourceFile varchar(1000)
)
AS
	/******************************************************************************
	**  Name: LoadRADExtractRename
	**  Purpose: 
	**
	**  Used in Import Radiology Data package
	**
	**	Returns an extract type and derives a date stamped file name based on an FTP'd file name
	**
	**	NOTE - Please maintain this stored procedure via Visual Studio 
	**         Data Warehouse ETL Solution located in the source control database
	**
	*******************************************************************************
	**  Modified: 
	*******************************************************************************
	** Date:		Author:		Description:
	** --------		---------	---------------------------------------------------- 
	** 20110601		MH			Added milliseconds to the date stamp, which will handle
	**							the processing of multiple file. Using seconds to ensure file uniqueness 
	**							was causing file exists errors
	** 20110606		MH			Bug fix - was not 0 padding the milliseconds correctly if < 10
	** 20150817		Paul Egan	Changed LIKE clause to enable loading CRIS backload stats appended 
								with YYYYMM automatically. NB this needs to match 
								LIKE clause in [ETL].[LoadRADExtractRename]
	******************************************************************************/

	DECLARE @CurrentDateTime DATETIME
	SET @CurrentDateTime = GETDATE()
	
	select
		NewName = LTRIM(RTRIM(Extract)) + 
			CAST(DATEPART( yyyy , @CurrentDateTime) AS CHAR(4)) + 
			RIGHT('00' + CAST(DATEPART( mm ,  @CurrentDateTime  ) AS NVARCHAR(2)), 2) + 
			RIGHT('00' + CAST(DATEPART( dd ,  @CurrentDateTime  ) AS NVARCHAR(2)), 2) + 
			RIGHT('00' + CAST(DATEPART( hh ,  @CurrentDateTime  ) AS NVARCHAR(2)), 2) + 
			RIGHT('00' + CAST(DATEPART( mi ,  @CurrentDateTime  ) AS NVARCHAR(2)), 2) + 
			RIGHT('00' + CAST(DATEPART( ss ,  @CurrentDateTime  ) AS NVARCHAR(2)), 2) + 
			RIGHT('000' + CAST(DATEPART( ms ,  @CurrentDateTime  ) AS NVARCHAR(3)), 3) + 
		    '.csv'
		,Extract
	from
		ETL.RADImportFiles
	where
		@SourceFile like '%CMFTDW%' + LTRIM(RTRIM(Extract)) + '%_output%.csv'
	
/* Paul Egan 20150817 this was the original WHERE clause:
		@SourceFile like '%' + LTRIM(RTRIM(Extract)) + '_output%.csv'
*/
		
	order by Extract DESC


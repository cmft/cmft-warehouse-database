﻿


CREATE procedure [ETL].[ExtractInquireOP]
	 @fromDate smalldatetime = null
	,@toDate smalldatetime = null
	,@debug bit = 0
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from varchar(12)
declare @to varchar(12)
declare @toOP varchar(12)

select @StartTime = getdate()

select @RowsInserted = 0

-- 999 day limit for future OPA
select @toOP = convert(varchar, dateadd(day, datediff(day, 0, dateadd(day, 999, getdate())), 0), 112)

select @from = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@fromDate, dateadd(month, -2, getdate()))), 0), 112)

select @to = convert(varchar, dateadd(day, datediff(day, 0, coalesce(@toDate, @toOP)), 0), 112)


insert into ETL.TImportOPEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,HomePhone
	,WorkPhone
	,IsWardAttender
	,InterfaceCode
	,SourceOfReferralGroupCode
	,ReferredByCode
	,ReferrerCode
	,LastDNAorPatientCancelledDate
	,StaffGroupCode
	,InterpreterIndicator
	,EpisodicSiteCode
	,PseudoPostcode

)

SELECT
	 SourceUniqueID = OPA.OPAID 
	,SourcePatientNo = OPA.InternalPatientNumber 
	,SourceEncounterNo = OPA.EpisodeNumber 
	,PatientTitle = Patient.Title 
	,PatientForename = Patient.Forenames 
	,PatientSurname = Patient.Surname 
	,DateOfBirth = Patient.PtDoB 
	,DateOfDeath = Patient.PtDateOfDeath 
	,SexCode = Patient.Sex 
	,NHSNumber = Patient.NHSNumber 
	,DistrictNo = OPA.DistrictNumber 
	,Postcode = Patient.PtAddrPostCode 
	,PatientAddress1 = Patient.PtAddrLine1 
	,PatientAddress2 = Patient.PtAddrLine2 
	,PatientAddress3 = Patient.PtAddrLine3 
	,PatientAddress4 = Patient.PtAddrLine4 
	,DHACode = OPA.HaCode 
	,EthnicOriginCode = Patient.EthnicType 
	,MaritalStatusCode = Patient.MaritalStatus 
	,ReligionCode = Patient.Religion 
	,RegisteredGpCode = OPA.RegGpCode 
	,RegisteredGpPracticeCode = null 
	,SiteCode = OPREFERRAL.HospitalCode 
	,AppointmentDate = OPA.ApptDate 
	,AppointmentTime = OPA.ApptTime 
	,ClinicCode = OPA.ClinicCode 
	,AdminCategoryCode = OPA.PtCategory 
	,SourceOfReferralCode = OPA.RefBy 
	,ReasonForReferralCode = OPA.ReasonForRef 
	,PriorityCode = OPA.RefPriority 
	,FirstAttendanceFlag = OPA.ApptClass 
	,AppointmentStatusCode = OPA.ApptStatus 
	,CancelledByCode = OPA.CancelBy 
	,TransportRequiredFlag = OPA.Transport 
	,AppointmentTypeCode = OPA.ApptType 
	,DisposalCode = OPA.Disposal 
	,ConsultantCode = OPA.ClinicConsultant 
	,SpecialtyCode = OPA.ClinicSpecialty 
	,ReferringConsultantCode = OPA.RefConsultant 
	,ReferringSpecialtyCode = OPA.RefSpecialty 
	,BookingTypeCode = OPA.BookingType 
	,CasenoteNo = OPA.CaseNoteNumber 
	,AppointmentCreateDate = OPA.ApptBookedDate 
	,EpisodicGpCode = OPA.EpiGp 
	,EpisodicGpPracticeCode = OPA.PracticeCode 
	,DoctorCode = OPA.ResCode 
	,PrimaryDiagnosisCode = OPA.RefPrimaryDiagnosisCode 
	,PrimaryOperationCode = OPA.ApptPrimaryProcedureCode 
	,PurchaserCode = OPA.ApptPurchaser 
	,ProviderCode = OUTCLNC.ProviderCode 
	,ContractSerialNo = OPA.ApptConractId 
	,ReferralDate = OPA.ReferralDate 
	,RTTPathwayID = left(PW.PathwayNumber , 25) 
	,RTTPathwayCondition = PW.PathwayCondition 
	,RTTStartDate = PWP.RttStartDate
	,RTTEndDate = PWP.RttEndDate
	,RTTSpecialtyCode = PW.RttSpeciality 
	,RTTCurrentProviderCode = PW.RttCurProv 
	,RTTCurrentStatusCode = PW.RttCurrentStatus 
	,RTTCurrentStatusDate = PWP.CurrentStatusDt 
	,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
	,RTTOverseasStatusFlag = PW.RttOsvStatus 
	,RTTPeriodStatusCode = OPA.RttPeriodStatus 
	,AppointmentCategoryCode = OPA.ApptCategory 
	,AppointmentCreatedBy = OPA.ApptEnteredUID 
	,AppointmentCancelDate = OPA.ApptCancDate 
	,LastRevisedDate = OPA.LastRevisionDtime 
	,LastRevisedBy = OPA.LastRevisionUID 
	,OverseasStatusFlag = OPA.OsvStatus 
	,PatientChoiceCode = OPA.PatChoice 
	,ScheduledCancelReasonCode = Attend.SchReasonForCancellationCodeAppointment 
	,PatientCancelReason = Attend.PatientAppointmentCancellationReason 
	,DischargeDate = OPREFERRAL.DischargeDt 
	,QM08StartWaitDate = OPREFERRAL.QM08StartWaitDate 
	,QM08EndWaitDate = OPREFERRAL.QM08EndWtDate 
	,DestinationSiteCode = OUTCLNC.DestinationHospCd 
	,EBookingReferenceNo = OPA.EbookingReferenceNumber 
	,HomePhone = Patient.PtHomePhone 
	,WorkPhone = Patient.PtWorkPhone 
	,IsWardAttender = 0
	,InterfaceCode = 'INQ'
	,SourceOfReferralGroupCode = null -- rules to be provided by Dusia
	,ReferredByCode = Registration.ReferredBy
	,ReferrerCode = 
		case
		when Registration.ReferredBy  IN ('GP','GPN') 
			then Registration.EpiGP
		when Registration.ReferredBy  IN ('CON','CNN') 
			then Registration.Consultant
		when Registration.ReferredBy  = 'GDP' 
			then Registration.EpiGDP
		else NULL
		end  --OPREFERRAL.EpiGPCode

	,LastDNAorPatientCancelledDate =
		(
		select
			MAX(LastPatientCancel.ApptDate)
		from
			[$(PAS)].Inquire.OPA LastPatientCancel
		where
			LastPatientCancel.InternalPatientNumber = OPA.InternalPatientNumber
		and	LastPatientCancel.EpisodeNumber = OPA.EpisodeNumber
		and	LastPatientCancel.PtApptStartDtimeInt <= OPA.PtApptStartDtimeInt
		and	(
				LastPatientCancel.CancelBy = 'P'
			or	LastPatientCancel.ApptStatus = 'CND'
			or	LastPatientCancel.ApptStatus = 'DNA'
			)
		)

	,StaffGroupCode = Attend.PatientAttendanceGradeOfStaff
	,InterpreterIndicator = OPA.Interpreter
	,EpisodicSiteCode = OUTCLNC.[DestinationHospCd] -- DG added 9 Oct 2012 requested by Dusia for DivisionMap
	,Patient.PtPseudoPostCode

FROM
	[$(PAS)].Inquire.OPA

INNER JOIN [$(PAS)].Inquire.PATDATA Patient
ON	OPA.InternalPatientNumber = Patient.InternalPatientNumber

INNER JOIN [$(PAS)].Inquire.OPREFERRAL 
ON	OPA.InternalPatientNumber = OPREFERRAL.InternalPatientNumber
AND	OPA.EpisodeNumber = OPREFERRAL.EpisodeNumber
AND	OPA.ReferralDateInt = OPREFERRAL.OpRegDtimeInt

INNER JOIN [$(PAS)].Inquire.OUTCLNC
ON	OPA.ClinicCode = OUTCLNC.OUTCLNCID

left JOIN [$(PAS)].Inquire.OPAPPOINTATTEND Attend 
ON	OPA.InternalPatientNumber = Attend.InternalPatientNumber
AND	OPA.EpisodeNumber = Attend.EpisodeNumber
AND	OPA.PtApptStartDtimeInt = Attend.PtApptStartDtimeInt
AND	OPA.ResCode = Attend.ResCode

left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = OPA.InternalPatientNumber
and	EPW.EpisodeNumber = OPA.EpisodeNumber

left join [$(PAS)].Inquire.RTTPTPATHWAY PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber

left join [$(PAS)].Inquire.RTTPERIOD PWP
on	PWP.InternalPatientNumber = PW.InternalPatientNumber
and	PWP.PathwayNumber = PW.PathwayNumber
and	PWP.PPeriodId = '0'

left join [$(PAS)].Inquire.EPIREGISTRATION Registration
on	Registration.InternalPatientNumber = OPA.InternalPatientNumber
and	Registration.EpisodeNumber = OPA.EpisodeNumber

WHERE
	OPA.PtApptStartDtimeInt between @from + '0000' and @to + '2400'



select
	@RowsInserted = @RowsInserted + @@ROWCOUNT



insert into ETL.TImportOPEncounter
(
	 SourceUniqueID
	,SourcePatientNo
	,SourceEncounterNo
	,PatientTitle
	,PatientForename
	,PatientSurname
	,DateOfBirth
	,DateOfDeath
	,SexCode
	,NHSNumber
	,DistrictNo
	,Postcode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,DHACode
	,EthnicOriginCode
	,MaritalStatusCode
	,ReligionCode
	,RegisteredGpCode
	,RegisteredGpPracticeCode
	,SiteCode
	,AppointmentDate
	,AppointmentTime
	,ClinicCode
	,AdminCategoryCode
	,SourceOfReferralCode
	,ReasonForReferralCode
	,PriorityCode
	,FirstAttendanceFlag
	,AppointmentStatusCode
	,CancelledByCode
	,TransportRequiredFlag
	,AppointmentTypeCode
	,DisposalCode
	,ConsultantCode
	,SpecialtyCode
	,ReferringConsultantCode
	,ReferringSpecialtyCode
	,BookingTypeCode
	,CasenoteNo
	,AppointmentCreateDate
	,EpisodicGpCode
	,EpisodicGpPracticeCode
	,DoctorCode
	,PrimaryDiagnosisCode
	,PrimaryOperationCode
	,PurchaserCode
	,ProviderCode
	,ContractSerialNo
	,ReferralDate
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,RTTPeriodStatusCode
	,AppointmentCategoryCode
	,AppointmentCreatedBy
	,AppointmentCancelDate
	,LastRevisedDate
	,LastRevisedBy
	,OverseasStatusFlag
	,PatientChoiceCode
	,ScheduledCancelReasonCode
	,PatientCancelReason
	,DischargeDate
	,QM08StartWaitDate
	,QM08EndWaitDate
	,DestinationSiteCode
	,EBookingReferenceNo
	,IsWardAttender
	,InterfaceCode
	,WardCode
	,InterpreterIndicator
	,EpisodicSiteCode
	,PseudoPostcode
)

SELECT
	 SourceUniqueID = WARDATTENDER.WARDATTENDERID 
	,SourcePatientNo = WARDATTENDER.InternalPatientNumber 
	,SourceEncounterNo = WARDATTENDER.EpisodeNumber 
	,PatientTitle = Patient.Title 
	,PatientForename = Patient.Forenames 
	,PatientSurname = Patient.Surname 
	,DateOfBirth = Patient.PtDoB 
	,DateOfDeath = Patient.PtDateOfDeath 
	,SexCode = Patient.Sex 
	,NHSNumber = Patient.NHSNumber
	,DistrictNo = WARDATTENDER.DistrictNumber 
	,Postcode = Patient.PtAddrPostCode 
	,PatientAddress1 = Patient.PtAddrLine1 
	,PatientAddress2 = Patient.PtAddrLine2 
	,PatientAddress3 = Patient.PtAddrLine3 
	,PatientAddress4 = Patient.PtAddrLine4 
	,DHACode = Patient.DistrictOfResidenceCode 
	,EthnicOriginCode = Patient.EthnicType 
	,MaritalStatusCode = Patient.MaritalStatus 
	,ReligionCode = Patient.Religion 
	,RegisteredGpCode = Patient.GpCode 
	,RegisteredGpPracticeCode = null 
	,SiteCode = WARD.HospitalCode 
	,AppointmentDate = WARDATTENDER.AttendanceDate 
	,AppointmentTime = WARDATTENDER.AttendanceDate 
	,ClinicCode = WARDATTENDER.Ward 
	,AdminCategoryCode = WARDATTENDER.Category 
	,SourceOfReferralCode = WARDATTENDER.RefBy 
	,ReasonForReferralCode = WARDATTENDER.ReasonForReferralCode 
	,PriorityCode = WARDATTENDER.PriorityType 
	,FirstAttendanceFlag = WARDATTENDER.FirstAttendance 
	,AppointmentStatusCode = WARDATTENDER.WaWardAttendersAttendanceStatusInternal 
	,CancelledByCode = null 
	,TransportRequiredFlag = null 
	,AppointmentTypeCode = 'WA' 
	,DisposalCode = WARDATTENDER.Disposal 
	,ConsultantCode = WARDATTENDER.Consultant 
	,SpecialtyCode = WARDATTENDER.Specialty 
	,ReferringConsultantCode = null 
	,ReferringSpecialtyCode = null 
	,BookingTypeCode = null 
	,CasenoteNo = WARDATTENDER.CaseNoteNumber 
	,AppointmentCreateDate = null 
	,EpisodicGpCode = Patient.GpCode 
	,EpisodicGpPracticeCode = null 
	,DoctorCode = WARDATTENDER.WaWardAttendersDoctorCodePhysical 
	,PrimaryDiagnosisCode = null 
	,PrimaryOperationCode = WARDATTENDER.PrimaryProcedureCode 
	,PurchaserCode = WARDATTENDER.PurchaserCode 
	,ProviderCode = null 
	,ContractSerialNo = WARDATTENDER.ContractId 
	,ReferralDate = WARDATTENDER.ReferralDate 
	,RTTPathwayID = left(PW.PathwayNumber , 25) 
	,RTTPathwayCondition = PW.PathwayCondition 
	,RTTStartDate = PWP.RttStartDate
	,RTTEndDate = PWP.RttEndDate
	,RTTSpecialtyCode = PW.RttSpeciality 
	,RTTCurrentProviderCode = PW.RttCurProv 
	,RTTCurrentStatusCode = PW.RttCurrentStatus 
	,RTTCurrentStatusDate = PWP.CurrentStatusDt 
	,RTTCurrentPrivatePatientFlag = PW.RttPrivatePat 
	,RTTOverseasStatusFlag = PW.RttOsvStatus 
	,RTTPeriodStatusCode = null 

	,AppointmentCategoryCode = null 

	,AppointmentCreatedBy = null 
	,AppointmentCancelDate = null 
	,LastRevisedDate = null 
	,LastRevisedBy = null 
	,OverseasStatusFlag = null 
	,PatientChoiceCode = null 
	,ScheduledCancelReasonCode = null 
	,PatientCancelReason = null 
	,DischargeDate = null 
	,QM08StartWaitDate = null 
	,QM08EndWaitDate = null 
	,DestinationSiteCode = WARD.HospitalCode 
	,EBookingReferenceNo = null 
	,IsWardAttender = 1
	,InterfaceCode = 'INQ'
	,WardCode = WARDATTENDER.Ward
	,InterpreterIndicator = null
	,EpisodicSiteCode = WARD.HospitalCode 
	,Patient.PtPseudoPostCode

FROM
	[$(PAS)].Inquire.WARDATTENDER

INNER JOIN [$(PAS)].Inquire.PATDATA Patient
ON	WARDATTENDER.InternalPatientNumber = Patient.InternalPatientNumber

INNER JOIN [$(PAS)].Inquire.WARD
ON	WARDATTENDER.Ward = WARD.WARDID

left join [$(PAS)].Inquire.RTTPTEPIPATHWAY EPW
on	EPW.InternalPatientNumber = WARDATTENDER.InternalPatientNumber
and	EPW.EpisodeNumber = WARDATTENDER.EpisodeNumber

left join [$(PAS)].Inquire.RTTPTPATHWAY PW
on	PW.InternalPatientNumber = EPW.InternalPatientNumber
and	PW.PathwayNumber = EPW.PathwayNumber

left join [$(PAS)].Inquire.RTTPERIOD PWP
on	PWP.InternalPatientNumber = PW.InternalPatientNumber
and	PWP.PathwayNumber = PW.PathwayNumber
and	PWP.PPeriodId = '0'

WHERE
	WARDATTENDER.AttendanceDateInternal between @from and @to
and	WARDATTENDER.SeenBy = 'DOCTOR'




select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent 'ExtractInquireOP', @Stats, @StartTime




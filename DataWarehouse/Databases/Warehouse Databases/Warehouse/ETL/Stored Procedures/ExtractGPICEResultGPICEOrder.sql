﻿

CREATE proc [ETL].[ExtractGPICEResultGPICEOrder] 

(
@StartDate date
,@EndDate date
) 

as

set dateformat ymd


declare @StartTime datetime
declare @Elapsed int
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0


insert ETL.TImportResultGPICEOrder
(
SourceUniqueID
,PatientID
,PatientIdentifier
,DistrictNo
,CasenoteNumber
,NHSNumber 
,ClinicianID
,ProviderID
,LocationID
,OrderPriority
,OrderStatusCode
,OrderRequestTime
,OrderReceivedTime
,OrderCollectionTime

)

select 
	SourceUniqueID 
	,PatientID 
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ClinicianID 
	,ProviderID 
	,LocationID 
	,OrderPriority
	,OrderStatusCode 
	,OrderRequestTime
	,OrderReceivedTime 
	,OrderCollectionTime
from
	(
	select --top 20 
		SourceUniqueID = OrderSummary.Service_Request_Index
		,PatientID = OrderSummary.Patient_Id_Key
		,PatientIdentifier = OrderSummary.HospitalNumber
		,DistrictNo = 
					case
					when 
						isnumeric(OrderSummary.HospitalNumber) = 1
					and len(OrderSummary.HospitalNumber) = 8
					then OrderSummary.HospitalNumber
					else DistrictNo.HospitalNumber
					end
		,CasenoteNumber = CasenoteNumber.HospitalNumber
		,NHSNumber = nullif(PatientSummary.New_NHS_No,'')	
		,ClinicianID = OrderSummary.Clinician_Index
		,ProviderID = OrderSummary.Provider_ID
		,LocationID = OrderSummary.Location_Index
		,OrderPriority = OrderSummary.Priority
		,OrderStatusCode = OrderSummary.Status
		,OrderRequestTime = OrderSummary.Date_Last_Edited
		,OrderReceivedTime = cast(OrderSummary.DateTime_Received as datetime)
		,OrderCollectionTime = cast(cast(cast(OrderSummary.Collection_Date as date) as varchar) + ' ' +  Collection_Time as datetime)
	from
		[$(ICE_GP)].dbo.ICEView_OrderSummary OrderSummary

	inner join [$(ICE_GP)].dbo.ICEView_PatientSummary PatientSummary
	on	OrderSummary.Patient_Id_Key = PatientSummary.Patient_Id_Key 

	left join
		(
		select
			Patient_Id_Key
			,HospitalNumber = max(HospitalNumber)
		from
			[$(ICE_GP)].dbo.ICEView_PatientHospitalNumbers DistrictNo
		where
			Main_Identifier = 1
		and	isnumeric(HospitalNumber) = 1
		and len(HospitalNumber) = 8
		and	Retired = 0
		group by
			Patient_Id_Key
		) DistrictNo
	on	OrderSummary.Patient_Id_Key = DistrictNo.Patient_Id_Key

	left join
		(
		select
			Patient_Id_Key
			,HospitalNumber = max(HospitalNumber)
		from
			[$(ICE_GP)].dbo.ICEView_PatientHospitalNumbers
		where
			charindex('/', HospitalNumber) > 0
		and	Retired = 0
		group by
			Patient_Id_Key
		) CasenoteNumber
	on	OrderSummary.Patient_Id_Key = CasenoteNumber.Patient_Id_Key

	where
		--OrderSummary.Service_Request_Index = 194047 --44649
		cast(OrderSummary.Date_Last_Edited as date) between @StartDate and @EndDate
		--ReportResults.Result_Index = 1723790

	) ICEOrder


select
	@RowsInserted = @RowsInserted + @@ROWCOUNT


-- Calc stats and log them
SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent @ProcedureName, @Stats, @StartTime



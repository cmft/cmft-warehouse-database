﻿
CREATE PROCEDURE [ETL].[LoadQCR] AS

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)
declare @ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)

select @StartTime = getdate()

exec ETL.LoadQCRReferenceData;
EXEC ETL.ExtractQCRAuditAnswer;
EXEC ETL.LoadQCRAuditAnswer;


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins'

EXEC Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime
﻿CREATE procedure [ETL].[LoadAPCSuspension] as

set dateformat dmy

Declare @StartTime datetime
Declare @Elapsed int
Declare @RowsUpdated Int
Declare @RowsInserted Int
Declare @Stats varchar(255)
Declare @from smalldatetime
Declare @to smalldatetime

select @StartTime = getdate()


update APC.Suspension
set
	 SuspensionStartDate = TLoadAPCSuspension.SuspensionStartDate 
	,SuspensionEndDate = TLoadAPCSuspension.SuspensionEndDate
	,SuspensionReasonCode = TLoadAPCSuspension.SuspensionReasonCode
	,SuspensionReason = TLoadAPCSuspension.SuspensionReason
from
	ETL.TLoadAPCSuspension
where
	Suspension.SourcePatientNo = TLoadAPCSuspension.SourcePatientNo
and	Suspension.SourceEncounterNo = TLoadAPCSuspension.SourceEncounterNo

SELECT @RowsUpdated = @@ROWCOUNT


insert into APC.Suspension
(
	 SourcePatientNo
	,SourceEncounterNo
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
)
select
	 SourcePatientNo
	,SourceEncounterNo
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
from
	ETL.TLoadAPCSuspension
where
	not exists
	(
	select
		1
	from
		APC.Suspension
	where
		Suspension.SourcePatientNo = TLoadAPCSuspension.SourcePatientNo
	and	Suspension.SourceEncounterNo = TLoadAPCSuspension.SourceEncounterNo
	)

SELECT @RowsInserted = @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())
SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Rows Updated ' + CONVERT(varchar(10), @RowsUpdated) + ', Net change '  + 
	CONVERT(varchar(10), @RowsInserted - @RowsUpdated) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins, Period '

EXEC Utility.WriteAuditLogEvent 'LoadAPCSuspension', @Stats, @StartTime

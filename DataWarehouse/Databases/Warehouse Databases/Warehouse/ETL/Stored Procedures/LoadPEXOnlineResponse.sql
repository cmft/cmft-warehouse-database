﻿
CREATE proc [ETL].[LoadPEXOnlineResponse]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@FromDate datetime
	,@ToDate datetime


select @FromDate = (select min(SurveyTime) from ETL.TLoadPEXOnlineResponse)
select @ToDate = (select max(SurveyTime) from ETL.TLoadPEXOnlineResponse)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	PEX.OnlineResponse target
using
	(
	select
		SurveyTakenID
		,SurveyID
		,QuestionID
		,AnswerID
		,Answer
		,SurveyDate
		,SurveyTime
		,LocationID
		,WardID
		,DeviceNumber
		,ChannelID
		,QuestionTypeID
		,LocationTypeID
	from
		ETL.TLoadPEXOnlineResponse

	) source
	on	source.SurveyTakenID = target.SurveyTakenID
	and	source.SurveyID = target.SurveyID
	and	source.QuestionID = target.QuestionID
	and	source.ChannelID = target.ChannelID
	and	source.LocationID = target.LocationID
	and source.Answer = target.Answer

	when not matched by source
	and target.SurveyDate between @FromDate and @ToDate

	then delete

	when not matched
	then
		insert
			(
			SurveyTakenID
			,SurveyID
			,QuestionID
			,AnswerID
			,Answer
			,SurveyDate
			,SurveyTime
			,LocationID
			,WardID
			,DeviceNumber
			,ChannelID
			,QuestionTypeID
			,LocationTypeID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.SurveyTakenID
			,source.SurveyID
			,source.QuestionID
			,source.AnswerID
			,source.Answer
			,source.SurveyDate
			,source.SurveyTime
			,source.LocationID
			,source.WardID
			,source.DeviceNumber
			,source.ChannelID
			,source.QuestionTypeID
			,source.LocationTypeID
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	
			isnull(target.SurveyTakenID, 0) = isnull(source.SurveyTakenID, 0)			
		and isnull(target.SurveyID, 0) = isnull(source.SurveyID, 0)
		and	isnull(target.QuestionID, 0) = isnull(source.QuestionID, 0)
		and isnull(target.AnswerID, 0) = isnull(source.AnswerID, 0)
		and isnull(target.Answer, 0) = isnull(source.Answer, 0)		
		and isnull(target.SurveyDate, getdate()) = isnull(source.SurveyDate, getdate())
		and isnull(target.SurveyTime, getdate()) = isnull(source.SurveyTime, getdate())
		and isnull(target.LocationID, 0) = isnull(source.LocationID, 0)
		and isnull(target.WardID, 0) = isnull(source.WardID, 0)
		and isnull(target.DeviceNumber, 0) = isnull(source.DeviceNumber, 0)
		and isnull(target.ChannelID, 0) = isnull(source.ChannelID, 0)
		and isnull(target.QuestionTypeID, 0) = isnull(source.QuestionTypeID, 0)
		and isnull(target.LocationTypeID, 0) = isnull(source.LocationTypeID, 0)				
		)
	then
		update
		set
			target.SurveyTakenID = source.SurveyTakenID
			,target.SurveyID = source.SurveyID
			,target.QuestionID = source.QuestionID
			,target.AnswerID = source.AnswerID
			,target.Answer = source.Answer
			,target.SurveyDate = source.SurveyDate
			,target.SurveyTime = source.SurveyTime
			,target.LocationID = source.LocationID
			,target.WardID = source.WardID		
			,target.DeviceNumber = source.DeviceNumber
			,target.ChannelID = source.ChannelID
			,target.QuestionTypeID = source.QuestionTypeID
			,target.LocationTypeID = source.LocationTypeID
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime

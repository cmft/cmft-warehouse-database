﻿


CREATE procedure [ETL].[LoadObservationReferenceData] 

as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


set dateformat dmy

truncate table Observation.UserBase

insert into	Observation.UserBase

 (
UserID
,UserCode
,TitleCode
,Surname
,Forename
,Initials
,UserPassword
,SurnameUpper
,ForenameUpper
,DefaultWardCode
,Note
,SystemUserFlag
,LockedFlag
,DeletedFlag
,CreatedByUserID
,CreatedTime
,LastModifiedByUserID
,LastModifiedTime
,UserSalt
,ChangePasswordFlag
,StartTime
,EndTime
,PasswordChangedTime
 )

select 
	USERR_PK
	,USER_ID
	,TITLE_RFVAL
	,SURNAME
	,FORENAME
	,INITIALS
	,USER_PASSWORD
	,SURNAME_UPPER
	,FORENAME_UPPER
	,DEFAULT_WARDD_RFVAL
	,NOTE
	,SYSTEM_USER_FLAG
	,LOCKED_FLAG
	,DELETED_FLAG
	,CREATED_BY_USERR_PK
	,CREATED_DTTM
	,LAST_MODIFIED_BY_USERR_PK
	,LAST_MODIFIED_DTTM
	,USER_SALT
	,CHANGE_PASSWORD_FLAG
	,START_DTTM
	,END_DTTM
	,PASSWORD_CHANGED_DTTM
from 
	[$(PatientrackSS)].dbo.USER_ACCOUNT


truncate table Observation.UserGroupBase

insert into Observation.UserGroupBase

(
	SourceUniqueID
	,UserID
	,UserGroupID
	,DeletedFlag
	,CreatedByUserID
	,CreatedTime
	,LastModifedByUserID
	,LastModifiedTime
)


select 
	SourceUniqueID = USRGP_PK
	,UserID = USERR_PK
	,UserGroupID = RIGRP_RFVAL
	,DeletedFlag = DELETED_FLAG
	,CreatedByUserID = CREATED_BY_USERR_PK
	,CreatedTime = CREATED_DTTM
	,LastModifedByUserID = LAST_MODIFIED_BY_USERR_PK
	,LastModifiedTime = LAST_MODIFIED_DTTM
from
	[$(PatientrackSS)].dbo.USER_GROUP


truncate table Observation.ReferenceBase

insert into	Observation.ReferenceBase
(
ReferenceID
,DomainCode
,ReferenceCode
,SystemFlag
,Description
,HiddenFlag
,DefaultFlag
,SortOrder
,StartTime
,EndTime
,DeletedFlag
,CreatedByUserID
,CreatedTime
,LastModifiedByUserID
,LastModifiedTime
)

select 
	RFVAL_PK
	,RFVDM_CODE
	,MAIN_IDENTIFIER
	,SYSTEM_FLAG
	,DESCRIPTION
	,HIDDEN_FLAG
	,DEFAULT_FLAG
	,SORT_ORDER
	,START_DTTM
	,END_DTTM
	,DELETED_FLAG
	,CREATED_BY_USERR_PK
	,CREATED_DTTM
	,LAST_MODIFIED_BY_USERR_PK
	,LAST_MODIFIED_DTTM
from 
	[$(PatientrackSS)].dbo.REFERENCE_VALUE


truncate table Observation.MeasureBase

insert into Observation.MeasureBase
(
	
	MeasureID
	,ParentMeasureID
	,MeasureCode
	,Measure
	,Complex
	,Unit
	,DataTypeCode
	,Format
	,MinimumValue
	,MaximumValue
	,ListCode
	,StartTime
	,EndTime
	,Deleted
	,CreatedByUserID
	,CreatedTime
	,LastModifiedByUserID
	,LastModifiedTime
	,InterfaceCode 
)

select
	OBMSR_PK
	,PARENT_OBMSR_PK
	,CODE
	,NAME
	--,DESCRIPTION
	--,ADDITIONAL_TEXT
	,COMPLEX_FLAG
	,UNIT
	,REFERENCE_VALUE.MAIN_IDENTIFIER
	,FORMAT
	,MIN_VALUE
	,MAX_VALUE
	,LIST_CODE
	--,BUTTON_TEXT
	--,BUTTON_VALUE
	,OBSERVATION_MEASURE.START_DTTM
	,OBSERVATION_MEASURE.END_DTTM
	,OBSERVATION_MEASURE.DELETED_FLAG
	,OBSERVATION_MEASURE.CREATED_BY_USERR_PK
	,OBSERVATION_MEASURE.CREATED_DTTM
	,OBSERVATION_MEASURE.LAST_MODIFIED_BY_USERR_PK
	,OBSERVATION_MEASURE.LAST_MODIFIED_DTTM
	,'PTRACK'
from
	[$(PatientrackSS)].dbo.OBSERVATION_MEASURE

inner join [$(PatientrackSS)].dbo.REFERENCE_VALUE
on	OBSERVATION_MEASURE.DATYP_RFVAL = REFERENCE_VALUE.RFVAL_PK


--Update mapping tables

insert Observation.SpecialtyMap
(
SpecialtyCode
,Specialty
)


select
	SpecialtyCode
	,Specialty
from
	Observation.Specialty

where
	not exists
		(
		select
			1
		from
			Observation.SpecialtyMap
		where
			SpecialtyMap.SpecialtyCode = Specialty.SpecialtyCode
		)


insert Observation.LocationMap
(
LocationCode
,Location
)


select
	LocationCode
	,Location
from
	Observation.Location

where
	not exists
		(
		select
			1
		from
			Observation.LocationMap
		where
			LocationMap.LocationCode = Location.LocationCode
		)


truncate table Observation.ProfileBase

insert Observation.ProfileBase
(
ProfileID
,Profile
,Description
,Template
,StartTime
,EndTime
,Deleted
,StepDownObservationProfileID
,Manual
,CreatedBy
,Created
,LastModifiedBy
,LastModified
,InterfaceCode
)

select --top 1000 
	ObservationProfileID = OBPRF_PK
	,ObservationProfile = NAME
	,Description = DESCRIPTION 
	,Template = TEMPLATE_FLAG
	,StartTime = START_DTTM 
	,EndTime = END_DTTM
	,Deleted = DELETED_FLAG 
	,StepDownObservationProfileID = STEP_DOWN_PROFILE_PK
	,Manual = MANUAL_FLAG
	,CreatedBy = CREATED_BY_USERR_PK
	,Created = CREATED_DTTM
	,LastModifiedBy = LAST_MODIFIED_BY_USERR_PK
	,LastModified = LAST_MODIFIED_DTTM
	,InterfaceCode = 'PTRACK'
from
	[$(PatientrackSS)].dbo.OBSERVATION_PROFILE


insert Observation.ProfileMap
(
ProfileID
)


select
	ProfileID
from
	Observation.ProfileBase
where
	not exists
		(
		select
			1
		from
			Observation.ProfileMap
		where
			ProfileMap.ProfileID = ProfileBase.ProfileID
		)



truncate table Observation.TemplatePriorityBase

insert Observation.TemplatePriorityBase
(
TemplatePriorityID
,TableCode
,TableID
,DrivingTemplateCode
,DrivingTemplateID
,Priority
,StartTime
,EndTime
,Deleted
,CreatedBy
,CreatedTime
,LastModifiedBy
,LastModifiedTime
,HierarchyID
,HierarchyItemID
,InterfaceCode
)

select
	TemplatePriorityID = DTMPR_PK
	,TableCode = TABLE_CODE
	,TableID = TABLE_PK
	,DrivingTemplateCode = DRIVING_TMPAP_CODE
	,DrivingTemplateID = DRIVING_TMPAP_PK 
	,Priority = PRIORITY
	,StartTime = START_DTTM
	,EndTime = END_DTTM
	,Deleted = DELETED_FLAG
	,CreatedBy = CREATED_BY_USERR_PK
	,CreatedTime = CREATED_DTTM
	,LastModifiedBy = LAST_MODIFIED_BY_USERR_PK
	,LastModifiedTime = LAST_MODIFIED_DTTM
	,HierarchyID = HIRCY_PK
	,HierarchyItemID = HITEM_RFVAL
	,InterfaceCode = 'PTRACK'
from
	[$(PatientrackSS)].dbo.DEFAULT_TEMPLATE_PRIORITY



select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec Utility.WriteAuditLogEvent 'LoadObservationReferenceData', @Stats, @StartTime


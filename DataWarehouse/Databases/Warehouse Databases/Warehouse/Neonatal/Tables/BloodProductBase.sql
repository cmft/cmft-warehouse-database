﻿CREATE TABLE [Neonatal].[BloodProductBase] (
    [BloodProductCode] INT          NOT NULL,
    [BloodProduct]     VARCHAR (50) NULL,
    CONSTRAINT [PK__BloodPro__0D67B1C50CC79DE7] PRIMARY KEY CLUSTERED ([BloodProductCode] ASC)
);


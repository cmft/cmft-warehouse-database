﻿CREATE TABLE [Neonatal].[ObservationMonitoringBase] (
    [ObservationMonitoringCode] INT          NOT NULL,
    [ObservationMonitoring]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Observat__91BDADE836BDD7B3] PRIMARY KEY CLUSTERED ([ObservationMonitoringCode] ASC)
);


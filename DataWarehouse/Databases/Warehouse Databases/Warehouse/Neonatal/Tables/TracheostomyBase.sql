﻿CREATE TABLE [Neonatal].[TracheostomyBase] (
    [TracheostomyCode] INT          NOT NULL,
    [Tracheostomy]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Tracheos__69F62C1F422F8A5F] PRIMARY KEY CLUSTERED ([TracheostomyCode] ASC)
);


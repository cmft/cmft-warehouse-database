﻿CREATE TABLE [Neonatal].[BeddayProcedure] (
    [BeddayProcedureRecno] INT              IDENTITY (1, 1) NOT NULL,
    [EpisodeID]            UNIQUEIDENTIFIER NOT NULL,
    [CareDate]             DATE             NOT NULL,
    [ProcedureCode]        INT              NOT NULL,
    [ModifiedTime]         DATETIME         NOT NULL,
    [InterfaceCode]        VARCHAR (4)      NOT NULL,
    [Created]              DATETIME         NULL,
    [Updated]              DATETIME         NULL,
    [ByWhom]               VARCHAR (50)     NULL,
    CONSTRAINT [PK_BeddayProcedure] PRIMARY KEY CLUSTERED ([EpisodeID] ASC, [CareDate] ASC, [ProcedureCode] ASC)
);


﻿CREATE TABLE [Neonatal].[OxygenBase] (
    [OxygenCode] INT          NOT NULL,
    [Oxygen]     VARCHAR (50) NULL,
    CONSTRAINT [PK__OxygenBa__4202DC2E3A8E6897] PRIMARY KEY CLUSTERED ([OxygenCode] ASC)
);


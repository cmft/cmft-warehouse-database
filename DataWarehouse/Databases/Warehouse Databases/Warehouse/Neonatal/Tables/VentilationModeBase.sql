﻿CREATE TABLE [Neonatal].[VentilationModeBase] (
    [VentilationModeCode] INT          NOT NULL,
    [VentilationMode]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Ventilat__C173303849D0AC27] PRIMARY KEY CLUSTERED ([VentilationModeCode] ASC)
);


﻿CREATE TABLE [Neonatal].[FormulaBase] (
    [FormulaCode] INT          NOT NULL,
    [Formula]     VARCHAR (50) NULL,
    CONSTRAINT [PK__FormulaB__9D7E80ED23AB033F] PRIMARY KEY CLUSTERED ([FormulaCode] ASC)
);


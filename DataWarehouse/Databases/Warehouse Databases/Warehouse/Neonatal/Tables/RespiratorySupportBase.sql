﻿CREATE TABLE [Neonatal].[RespiratorySupportBase] (
    [RespiratorySupportCode] INT           NOT NULL,
    [RespiratorySupport]     VARCHAR (100) NULL,
    CONSTRAINT [PK__Respirat__B635153B3E5EF97B] PRIMARY KEY CLUSTERED ([RespiratorySupportCode] ASC)
);


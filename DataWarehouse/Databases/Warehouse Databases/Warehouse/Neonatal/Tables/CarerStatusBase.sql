﻿CREATE TABLE [Neonatal].[CarerStatusBase] (
    [CarerStatusCode] INT          NOT NULL,
    [CarerStatus]     VARCHAR (50) NULL,
    CONSTRAINT [PK__CarerSta__413F052310982ECB] PRIMARY KEY CLUSTERED ([CarerStatusCode] ASC)
);


﻿CREATE TABLE [Neonatal].[EnteralFeedBase] (
    [EnteralFeedCode] INT          NOT NULL,
    [EnteralFeed]     VARCHAR (50) NULL,
    CONSTRAINT [PK__EnteralF__1FB0449A1C09E177] PRIMARY KEY CLUSTERED ([EnteralFeedCode] ASC)
);


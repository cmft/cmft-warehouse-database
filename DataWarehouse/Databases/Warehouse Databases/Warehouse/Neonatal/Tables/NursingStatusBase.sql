﻿CREATE TABLE [Neonatal].[NursingStatusBase] (
    [NursingStatusCode] INT          NOT NULL,
    [NursingStatus]     VARCHAR (50) NULL,
    CONSTRAINT [PK__NursingS__7BA3F58D32ED46CF] PRIMARY KEY CLUSTERED ([NursingStatusCode] ASC)
);


﻿CREATE TABLE [Neonatal].[BeddayManagement] (
    [BeddayManagementRecno] INT              IDENTITY (1, 1) NOT NULL,
    [EpisodeID]             UNIQUEIDENTIFIER NOT NULL,
    [CareDate]              DATE             NOT NULL,
    [ManagementCode]        INT              NOT NULL,
    [ModifiedTime]          DATETIME         NOT NULL,
    [InterfaceCode]         VARCHAR (4)      NOT NULL,
    [Created]               DATETIME         NULL,
    [Updated]               DATETIME         NULL,
    [ByWhom]                VARCHAR (50)     NULL,
    CONSTRAINT [PK_BeddayManagement] PRIMARY KEY CLUSTERED ([EpisodeID] ASC, [CareDate] ASC, [ManagementCode] ASC)
);


﻿CREATE TABLE [Neonatal].[ConsciousnessBase] (
    [ConsciousnessCode] INT          NOT NULL,
    [Consciousness]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Consciou__6011DFE51468BFAF] PRIMARY KEY CLUSTERED ([ConsciousnessCode] ASC)
);


﻿CREATE TABLE [Neonatal].[ProcedureBase] (
    [ProcedureCode] INT           NOT NULL,
    [Procedure]     VARCHAR (200) NULL,
    CONSTRAINT [PK__Procedur__0F12591D51DBE243] PRIMARY KEY CLUSTERED ([ProcedureCode] ASC)
);


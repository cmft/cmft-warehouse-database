﻿CREATE TABLE [Neonatal].[NecrotizingEntercolitisTreatmentBase] (
    [NecrotizingEntercolitisTreatmentCode] INT          NOT NULL,
    [NecrotizingEntercolitisTreatment]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Necrotiz__F571F91B2F1CB5EB] PRIMARY KEY CLUSTERED ([NecrotizingEntercolitisTreatmentCode] ASC)
);


﻿CREATE TABLE [Neonatal].[TracheostomyCarerBase] (
    [TracheostomyCarerCode] INT          NOT NULL,
    [TracheostomyCare]      VARCHAR (50) NULL,
    CONSTRAINT [PK__Tracheos__3A1EDC5146001B43] PRIMARY KEY CLUSTERED ([TracheostomyCarerCode] ASC)
);


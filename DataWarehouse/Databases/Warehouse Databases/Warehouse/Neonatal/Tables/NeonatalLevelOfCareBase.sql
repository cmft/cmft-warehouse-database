﻿CREATE TABLE [Neonatal].[NeonatalLevelOfCareBase] (
    [NeonatalLevelOfCareCode] INT          NOT NULL,
    [NeonatalLevelOfCare]     VARCHAR (50) NULL,
    [Year]                    INT          NOT NULL,
    CONSTRAINT [PK_NeonatalLevelOfCare] PRIMARY KEY CLUSTERED ([NeonatalLevelOfCareCode] ASC, [Year] ASC)
);


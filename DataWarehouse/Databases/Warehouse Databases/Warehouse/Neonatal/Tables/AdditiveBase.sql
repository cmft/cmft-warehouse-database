﻿CREATE TABLE [Neonatal].[AdditiveBase] (
    [AdditiveCode] INT          NOT NULL,
    [Additive]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Additive__E9B9D94508F70D03] PRIMARY KEY CLUSTERED ([AdditiveCode] ASC)
);


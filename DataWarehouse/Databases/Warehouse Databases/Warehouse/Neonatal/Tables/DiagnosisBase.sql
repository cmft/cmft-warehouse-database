﻿CREATE TABLE [Neonatal].[DiagnosisBase] (
    [DiagnosisCode] INT           NOT NULL,
    [Diagnosis]     VARCHAR (200) NULL,
    CONSTRAINT [PK__Diagnosi__A43A22194E0B515F] PRIMARY KEY CLUSTERED ([DiagnosisCode] ASC)
);


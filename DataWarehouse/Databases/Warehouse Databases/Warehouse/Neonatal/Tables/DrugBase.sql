﻿CREATE TABLE [Neonatal].[DrugBase] (
    [DrugCode] INT           NOT NULL,
    [Drug]     VARCHAR (200) NULL,
    CONSTRAINT [PK__DrugBase__EEC5E9664A3AC07B] PRIMARY KEY CLUSTERED ([DrugCode] ASC)
);


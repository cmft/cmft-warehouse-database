﻿CREATE TABLE [Neonatal].[ManagementBase] (
    [ManagementCode] INT          NOT NULL,
    [Management]     VARCHAR (50) NULL,
    CONSTRAINT [PK__Manageme__505CB8693045E44E] PRIMARY KEY CLUSTERED ([ManagementCode] ASC)
);


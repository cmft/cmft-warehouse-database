﻿CREATE TABLE [Neonatal].[FeedingMethodBase] (
    [FeedingMethodCode] INT          NOT NULL,
    [FeedingMethod]     VARCHAR (50) NULL,
    CONSTRAINT [PK__FeedingM__3CE68C1E1FDA725B] PRIMARY KEY CLUSTERED ([FeedingMethodCode] ASC)
);


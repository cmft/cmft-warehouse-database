﻿CREATE TABLE [Neonatal].[xxxNeonatalLevelOfCare2011] (
    [NeonatalLevelOfCareCode] VARCHAR (10) NOT NULL,
    [NeonatalLevelOfCare]     VARCHAR (50) NULL,
    CONSTRAINT [PK_NeonatalLevelOfCareBAPM2011] PRIMARY KEY CLUSTERED ([NeonatalLevelOfCareCode] ASC)
);


﻿CREATE TABLE [Modernisation].[WaitingList] (
    [EncounterRecno]               INT           NOT NULL,
    [WaitingListTypeCode]          CHAR (2)      NULL,
    [SourcePatientNo]              VARCHAR (9)   NOT NULL,
    [SourceEntityRecno]            VARCHAR (50)  NOT NULL,
    [CensusDate]                   DATETIME      NULL,
    [PatientSurname]               VARCHAR (24)  NULL,
    [PatientForename]              VARCHAR (20)  NULL,
    [PatientTitle]                 VARCHAR (5)   NULL,
    [SexCode]                      VARCHAR (1)   NULL,
    [DateOfBirth]                  DATETIME      NULL,
    [DateOfDeath]                  DATETIME      NULL,
    [NHSNumber]                    VARCHAR (17)  NULL,
    [DistrictNo]                   VARCHAR (14)  NULL,
    [MaritalStatusCode]            VARCHAR (1)   NULL,
    [ReligionCode]                 VARCHAR (4)   NULL,
    [Postcode]                     VARCHAR (10)  NULL,
    [PatientsAddress1]             VARCHAR (20)  NULL,
    [PatientsAddress2]             VARCHAR (20)  NULL,
    [PatientsAddress3]             VARCHAR (20)  NULL,
    [PatientsAddress4]             VARCHAR (20)  NULL,
    [DHACode]                      VARCHAR (3)   NULL,
    [HomePhone]                    VARCHAR (23)  NULL,
    [WorkPhone]                    VARCHAR (23)  NULL,
    [EthnicOriginCode]             VARCHAR (4)   NULL,
    [ConsultantCode]               VARCHAR (6)   NULL,
    [SpecialtyCode]                VARCHAR (4)   NULL,
    [MappedSpecialtyCode]          VARCHAR (4)   NULL,
    [ManagementIntentionCode]      VARCHAR (1)   NULL,
    [AdmissionMethodCode]          VARCHAR (2)   NULL,
    [PriorityCode]                 VARCHAR (1)   NULL,
    [WaitingListCode]              VARCHAR (6)   NULL,
    [CommentClinical]              VARCHAR (60)  NULL,
    [CommentNonClinical]           VARCHAR (60)  NULL,
    [IntendedPrimaryOperationCode] VARCHAR (7)   NULL,
    [Operation]                    VARCHAR (60)  NULL,
    [SiteCode]                     VARCHAR (4)   NULL,
    [WardCode]                     VARCHAR (4)   NULL,
    [WLStatus]                     VARCHAR (20)  NULL,
    [ProviderCode]                 VARCHAR (4)   NULL,
    [PurchaserCode]                VARCHAR (5)   NULL,
    [ContractSerialNumber]         VARCHAR (6)   NULL,
    [CancelledBy]                  VARCHAR (3)   NULL,
    [BookingTypeCode]              VARCHAR (4)   NULL,
    [CasenoteNumber]               VARCHAR (14)  NULL,
    [OriginalDateOnWaitingList]    DATETIME      NULL,
    [DateOnWaitingList]            DATETIME      NULL,
    [TCIDate]                      DATETIME      NULL,
    [KornerWait]                   INT           NULL,
    [CountOfDaysSuspended]         VARCHAR (4)   NULL,
    [SuspensionStartDate]          DATETIME      NULL,
    [SuspensionEndDate]            DATETIME      NULL,
    [SuspensionReasonCode]         VARCHAR (4)   NULL,
    [SuspensionReason]             VARCHAR (30)  NULL,
    [InterfaceCode]                CHAR (5)      NULL,
    [EpisodicGpCode]               VARCHAR (8)   NULL,
    [EpisodicGpPracticeCode]       VARCHAR (6)   NULL,
    [RegisteredGpCode]             VARCHAR (8)   NULL,
    [RegisteredPracticeCode]       VARCHAR (6)   NULL,
    [SourceTreatmentFunctionCode]  INT           NULL,
    [TreatmentFunctionCode]        INT           NULL,
    [NationalSpecialityCode]       VARCHAR (3)   NULL,
    [PCTCode]                      VARCHAR (8)   NULL,
    [BreachDate]                   DATETIME      NULL,
    [EligibleStatusCode]           CHAR (1)      NULL,
    [ExpectedAdmissionDate]        DATETIME      NULL,
    [ReferralDate]                 DATETIME      NULL,
    [BookedDate]                   DATETIME      NULL,
    [AppointmentDate]              DATETIME      NULL,
    [AppointmentTypeCode]          VARCHAR (4)   NULL,
    [AppointmentStatusCode]        VARCHAR (4)   NULL,
    [AppointmentCategoryCode]      VARCHAR (4)   NULL,
    [QM08StartWaitDate]            DATETIME      NULL,
    [QM08EndWaitDate]              DATETIME      NULL,
    [AppointmentTime]              VARCHAR (20)  NULL,
    [ClinicCode]                   VARCHAR (15)  NULL,
    [SourceOfReferralCode]         VARCHAR (4)   NULL,
    [FuturePatientCancelDate]      DATETIME      NULL,
    [AdditionFlag]                 BIT           NULL,
    [LastAppointmentFlag]          BIT           NULL,
    [LocalRegisteredGpCode]        VARCHAR (8)   NULL,
    [LocalEpisodicGpCode]          VARCHAR (8)   NULL,
    [NextOfKinName]                VARCHAR (30)  NULL,
    [NextOfKinRelationship]        VARCHAR (9)   NULL,
    [NextOfKinHomePhone]           VARCHAR (23)  NULL,
    [NextOfKinWorkPhone]           VARCHAR (23)  NULL,
    [ExpectedLOS]                  VARCHAR (10)  NULL,
    [TheatreKey]                   INT           NULL,
    [TheatreInterfaceCode]         CHAR (5)      NULL,
    [ProcedureDate]                DATETIME      NULL,
    [FutureCancellationDate]       DATETIME      NULL,
    [ProcedureTime]                DATETIME      NULL,
    [TheatreCode]                  VARCHAR (10)  NULL,
    [AdmissionReason]              VARCHAR (255) NULL,
    [EpisodeNo]                    SMALLINT      NULL,
    [BreachDays]                   INT           NULL,
    [MRSA]                         VARCHAR (10)  NULL,
    [RTTPathwayID]                 VARCHAR (25)  NULL,
    [RTTPathwayCondition]          VARCHAR (20)  NULL,
    [RTTStartDate]                 DATETIME      NULL,
    [RTTEndDate]                   DATETIME      NULL,
    [RTTSpecialtyCode]             VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]       VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]         VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]         DATETIME      NULL,
    [RTTCurrentPrivatePatientFlag] BIT           NULL,
    [RTTOverseasStatusFlag]        BIT           NULL,
    [NationalBreachDate]           DATETIME      NULL,
    [NationalBreachDays]           INT           NULL,
    [DerivedBreachDays]            INT           NULL,
    [DerivedClockStartDate]        DATETIME      NULL,
    [DerivedBreachDate]            DATETIME      NULL,
    [RTTBreachDate]                DATETIME      NULL,
    [RTTDiagnosticBreachDate]      DATETIME      NULL,
    [NationalDiagnosticBreachDate] DATETIME      NULL,
    [SocialSuspensionDays]         INT           NULL,
    [BreachTypeCode]               VARCHAR (10)  NULL,
    [CountOfDNAs]                  INT           NULL,
    [CountOfHospitalCancels]       INT           NULL,
    [CountOfPatientCancels]        INT           NULL,
    [AppointmentID]                VARCHAR (50)  NULL,
    [AddedToWaitingListTime]       DATETIME      NULL
);


﻿CREATE TABLE [BedComplement].[WardBed] (
    [WardID]         INT           NOT NULL,
    [InpatientBeds]  INT           NULL,
    [DaycaseBeds]    INT           NULL,
    [SingleRoomBeds] INT           NULL,
    [Comment]        VARCHAR (255) NULL,
    [StartDate]      DATE          NOT NULL,
    CONSTRAINT [PK_WardComplement] PRIMARY KEY CLUSTERED ([WardID] ASC, [StartDate] ASC)
);


﻿CREATE TABLE [BedComplement].[WardType] (
    [WardTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [WardType]   VARCHAR (255) NULL,
    CONSTRAINT [PK_WardType_1] PRIMARY KEY CLUSTERED ([WardTypeID] ASC)
);


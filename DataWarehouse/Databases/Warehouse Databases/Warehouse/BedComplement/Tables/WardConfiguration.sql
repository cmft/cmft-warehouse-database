﻿CREATE TABLE [BedComplement].[WardConfiguration] (
    [WardID]           INT           NOT NULL,
    [WardTypeID]       INT           NULL,
    [Schedule]         VARCHAR (255) NULL,
    [DivisionCode]     VARCHAR (5)   NULL,
    [SpecialtyGroupID] INT           NULL,
    [Comment]          VARCHAR (255) NULL,
    [StartDate]        DATE          NOT NULL,
    [EndDate]          DATE          NULL,
    CONSTRAINT [PK_WardMapping] PRIMARY KEY CLUSTERED ([WardID] ASC, [StartDate] ASC)
);


﻿CREATE TABLE [BedComplement].[Ward] (
    [WardID]   INT           NOT NULL,
    [WardCode] VARCHAR (255) NULL,
    [Ward]     VARCHAR (255) NULL,
    CONSTRAINT [PK_Ward_1] PRIMARY KEY CLUSTERED ([WardID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Ward]
    ON [BedComplement].[Ward]([WardCode] ASC);


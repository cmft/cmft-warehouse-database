﻿CREATE TABLE [BedComplement].[SpecialtyGroup] (
    [SpecialtyGroupID] INT           IDENTITY (1, 1) NOT NULL,
    [SpecialtyGroup]   VARCHAR (255) NULL,
    CONSTRAINT [PK_SpecialtyGroup_1] PRIMARY KEY CLUSTERED ([SpecialtyGroupID] ASC)
);


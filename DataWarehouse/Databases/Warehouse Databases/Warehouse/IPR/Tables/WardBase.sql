﻿CREATE TABLE [IPR].[WardBase] (
    [WardID]       INT          NULL,
    [WardCode]     VARCHAR (20) NULL,
    [Ward]         VARCHAR (50) NULL,
    [DivisionCode] VARCHAR (10) NULL
);


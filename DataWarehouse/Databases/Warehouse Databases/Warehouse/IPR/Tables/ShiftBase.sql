﻿CREATE TABLE [IPR].[ShiftBase] (
    [ID]        INT          IDENTITY (1, 1) NOT NULL,
    [Shift]     VARCHAR (10) NULL,
    [ShiftType] INT          NULL
);


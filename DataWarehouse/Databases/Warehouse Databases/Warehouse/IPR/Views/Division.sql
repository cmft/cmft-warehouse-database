﻿create view [IPR].[Division]

as

select
	[DivisionID]
	,[DivisionCode]
	,[Division]
from
	[IPR].[DivisionBase]
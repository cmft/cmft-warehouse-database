﻿

create view IPR.Ward

as

select
	[WardCode]
	,[Ward]
	,[DivisionCode]
from
	[IPR].[WardBase]
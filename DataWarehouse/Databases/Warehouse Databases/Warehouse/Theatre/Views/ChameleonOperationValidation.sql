﻿




CREATE view [Theatre].[ChameleonOperationValidation] as

--select -- top 10 
--	OperationNumber = SourceUniqueID -- OP_SEQU
--	,DistrictNo -- OP_MRN
--	,EpisodeNumber = HospitalEpisodeNo -- OP_EPISODE_NUM
--	,OperationStatus = ProceduralStatusCode -- OP_STATUS
--	,LastName = Surname -- OP_LNAME
--	,DateOfBirth = DateOfBirth -- OP_DOB
--	,AgeYears = PatientAgeInYears -- OP_AGE_YRS
--	,Gender = SexCode -- OP_GENDER
--	,StartDateTime = OperationStartDate -- OP_START_DATE_TIME
--	,FinishDateTime = OperationEndDate -- OP_FINISH_DATE_TIME
--	,OperationDate = OperationDate -- OP_OPERATION_DATE
--	,OperationType = OperationTypeCode -- OP_OPERAT_TYPE
--	,InAnaestheticsDateTime = InAnaestheticTime -- OP_IN_ANAES_DATE_TIME
--	,InSuiteDateTime = InSuiteTime -- OP_IN_SUITE_DATE_TIME
--	,InRecDateTime = InRecoveryTime -- OP_IN_REC_DATE_TIME
--	,[OP_ANAES_IND_DATE_TIME] = AnaestheticInductionTime -- OP_ANAES_IND_DATE_TIME

--from
--	Theatre.OperationDetail


select
	DistrictNo = [OP_MRN]
	,OperationNumber = [OP_SEQU]
	,[OP_AGE_YRS]
	,[OP_DAY]
	,StartDateTime = [OP_START_DATE_TIME]
	,[OP_TH_SEQU]
	,OperationDate = [OP_OPERATION_DATE]
	,FinishDateTime = [OP_FINISH_DATE_TIME]
	,DateOfBirth = [OP_DOB]
	,OperationType = [OP_OPERAT_TYPE]
	,[OP_ORDER]
	,[OP_FNAME]
	,InSuiteDateTime = [OP_IN_SUITE_DATE_TIME]
	,LastName = [OP_LNAME]
	,InAnaestheticsDateTime = [OP_IN_ANAES_DATE_TIME]
	,InRecDateTime = [OP_IN_REC_DATE_TIME]
	,[OP_BIOHAZARD]
	,[OP_ASA_SEQU]
	,[OP_UP_SEQU]
	,[OP_REVIEW]
	,[OP_DETAILS_SEQU]
	,[OP_DESCRIPTION]
	,[OP_DEATH]
	,[OP_CAUSE]
	,[OP_DIAGNOSIS_SEQU]
	,[OP_AGE_MONTH]
	,[OP_AGE_DAYS]
	,Gender = [OP_GENDER]
	,[OP_WARD_TO]
	,[OP_PIN_SEQU]
	,[OP_CL_SEQU]
	,[OP_PA_SEQU]
	,[OP_S1_SEQU]
	,[OP_WA_FR_SEQU]
	,[OP_WA_TO_SEQU]
	,[OP_CONS_SEQU]
	,[OP_STREET]
	,[OP_SUBURB]
	,[OP_POSTCODE]
	,[OP_HOME_PHONE]
	,[OP_BUS_PHONE]
	,[OP_SU1_SEQU]
	,[OP_SU2_SEQU]
	,[OP_SU3_SEQU]
	,[OP_AN1_SEQU]
	,[OP_AN2_SEQU]
	,[OP_AN3_SEQU]
	,[OP_SC_NUR_SEQU]
	,[OP_AN_NUR_SEQU]
	,[OP_IN_NUR_SEQU]
	,[OP_TECH1_SEQU]
	,[OP_TECH2_SEQU]
	,[OP_TECH3_SEQU]
	,[OP_SESSION_NO]
	,[OP_CA_SEQU]
	,[OP_SP_SEQU]
	,[OP_DISCH_DATE_TIME]
	,[OP_IC_SEQU]
	,[OP_ESCORT_RET]
	,[OP_CANCELLED]
	,[OP_DE_SEQU]
	,[OP_ANAES_IND_DATE_TIME]
	,[OP_READY_DEPART_DATE_TIME]
	,[OP_OTHER_COMMENTS_SEQU]
	,[OP_PC_SEQU]
	,[OP_POST_OP_COMMENTS_SEQU]
	,[OP_CLP_SEQU]
	,[OP_ADMIT_SU_SEQU]
	,[OP_SN_SEQU]
	,[OP_HOSP_DIS_DATE_TIME]
	,EpisodeNumber = [OP_EPISODE_NUM]
	,[OP_RETURN_TO_THEATRE]
	,[OP_ADMIT_DATE_TIME]
	,[OP_WD_SEQU]
	,[OP_TA_SEQU]
	,[OP_CALF_COMP]
	,[OP_OR_READY_DATE_TIME]
	,[OP_DRESSING_DATE_TIME]
	,[OP_SETUP_COMP_DATE_TIME]
	,[OP_UPD_DATE_TIME]
	,OperationStatus = [OP_STATUS]
	,[OP_ACC_NUMBER]
	,[OP_ACC_INDICATOR]
	,[OP_ACC_SEQU]
	,[OP_UPDATED]
	,[OP_STATE]
	,[OP_OPER_NO]
	,[OP_EXTUBATION_DATE_TIME]
	,[OP_STREET_2]
	,[OP_CITY]
	,[OP_SENT_FOR_DATE_TIME]
	,[OP_PORTER_LEFT_DATE_TIME]
	,[OP_PORTER_SEQU]
	,[OP_TH_CC_SEQU]
	,[OP_CONS_CC_SEQU]
	,[OP_ORDER_CC]
	,[OP_SESSION_NO_CC]
	,[OP_ADDITIONAL]
	,[OP_DICTATED_NOTE]
	,[OP_ANAES_READY_DATE_TIME]
	,[OP_PREP_READY_DATE_TIME]
	,[OP_SS_SEQU]
	,[OP_CC_SS_SEQU]
	,[OP_INCISION_DETAILS_SEQU]
	,[OP_MNAME]
	,[OP_COMPLETE]
	,[OP_IMS_PAT_ASSOC]
	--,[OP_TEAM_TIMEOUT_COMP]
	,[OP_TEAM_TIMEOUT_DATE_TIME]
	,[OP_HOLD]
	,[OP_MOBILE_PHONE]
	,[OP_EMAIL]
	,[OP_ADMIT_STAFF_SU_SEQU]
	,[OP_REQ_TIME]
	,[OP_VALIDATED_PROC_SEQU]
from
	[$(otprd)].[dbo].[FOPERAT]







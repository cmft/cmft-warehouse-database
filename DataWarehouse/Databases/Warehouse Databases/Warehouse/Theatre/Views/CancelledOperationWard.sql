﻿

Create View Theatre.CancelledOperationWard as

Select distinct
	WardCode = ContextCode + '||' + WardCode
	,Ward = ContextCode + '||' + WardCode
from 
	Theatre.CancelledOperation	

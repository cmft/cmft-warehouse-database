﻿CREATE TABLE [Theatre].[Theatre] (
    [TheatreCode]        INT           NOT NULL,
    [Theatre]            VARCHAR (255) NULL,
    [OperatingSuiteCode] INT           NOT NULL,
    [TheatreCode1]       VARCHAR (8)   NULL,
    [InactiveFlag]       BIT           NOT NULL,
    [Colour]             VARCHAR (20)  NULL,
    CONSTRAINT [PK_Theatre] PRIMARY KEY CLUSTERED ([TheatreCode] ASC)
);


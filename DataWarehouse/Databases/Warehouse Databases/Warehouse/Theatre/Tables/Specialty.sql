﻿CREATE TABLE [Theatre].[Specialty] (
    [SpecialtyCode]  INT           NOT NULL,
    [Specialty]      VARCHAR (255) NULL,
    [SpecialtyCode1] VARCHAR (8)   NULL,
    [InactiveFlag]   BIT           NOT NULL,
    CONSTRAINT [PK_TheatreSpecialty] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Specialty]
    ON [Theatre].[Specialty]([SpecialtyCode1] ASC);


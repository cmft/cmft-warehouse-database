﻿CREATE TABLE [Theatre].[ASAScore] (
    [ASAScoreCode]  INT           NOT NULL,
    [ASAScore]      VARCHAR (255) NULL,
    [ASAScoreCode1] VARCHAR (2)   NULL,
    [InactiveFlag]  BIT           NOT NULL,
    CONSTRAINT [PK_ASAScore] PRIMARY KEY CLUSTERED ([ASAScoreCode] ASC)
);


﻿CREATE TABLE [Theatre].[CancelReasonGroup] (
    [CancelReasonGroupCode]  INT           NOT NULL,
    [CancelReasonGroup]      VARCHAR (255) NULL,
    [CancelReasonGroupCode1] VARCHAR (5)   NULL,
    [InactiveFlag]           BIT           NOT NULL,
    CONSTRAINT [PK_TheatreCancelReasonGroup] PRIMARY KEY CLUSTERED ([CancelReasonGroupCode] ASC)
);


﻿CREATE TABLE [Theatre].[Cancellation] (
    [SourceUniqueID]                NUMERIC (9)  NOT NULL,
    [CancellationDate]              DATETIME     NULL,
    [CancelReasonCode]              NUMERIC (9)  NOT NULL,
    [CancelReasonCode1]             VARCHAR (5)  NULL,
    [CancellationComment]           TEXT         NULL,
    [OperationDetailSourceUniqueID] NUMERIC (9)  NOT NULL,
    [ConsultantCode]                VARCHAR (5)  NULL,
    [SpecialtyCode]                 NUMERIC (9)  NOT NULL,
    [SurgeonCode]                   NUMERIC (9)  NOT NULL,
    [SurgeonSpecialtyCode]          NUMERIC (9)  NOT NULL,
    [ProposedOperationDate]         DATETIME     NULL,
    [DistrictNo]                    VARCHAR (12) NULL,
    [Forename]                      VARCHAR (60) NULL,
    [Surname]                       VARCHAR (60) NULL,
    [WardCode]                      NUMERIC (9)  NOT NULL,
    [WardCode1]                     VARCHAR (8)  NULL,
    [PreMedGivenFlag]               NUMERIC (3)  NOT NULL,
    [FastedFlag]                    NUMERIC (3)  NOT NULL,
    [LastUpdated]                   DATETIME     NULL,
    [PatientClassificationCode]     NUMERIC (9)  NOT NULL,
    [PatientSourceUniqueID]         NUMERIC (9)  NOT NULL,
    [CampusCode]                    NUMERIC (9)  NOT NULL,
    [TheatreCode]                   NUMERIC (9)  NOT NULL,
    [TheatreCode1]                  VARCHAR (11) NULL,
    [CancellationSurgeonCode]       NUMERIC (9)  NOT NULL,
    [InitiatorCode]                 NUMERIC (9)  NOT NULL,
    [PriorityCode]                  NUMERIC (9)  NOT NULL,
    [AdmissionDate]                 DATETIME     NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_Cancellation_OperationDetailSourceUniqueID]
    ON [Theatre].[Cancellation]([OperationDetailSourceUniqueID] ASC)
    INCLUDE([SourceUniqueID], [CancellationDate]);


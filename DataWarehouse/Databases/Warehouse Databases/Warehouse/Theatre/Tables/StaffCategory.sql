﻿CREATE TABLE [Theatre].[StaffCategory] (
    [StaffCategoryCode]  NUMERIC (9)  NOT NULL,
    [StaffCategoryCode1] VARCHAR (5)  NULL,
    [StaffCategory]      VARCHAR (60) NULL,
    [SurgeonFlag]        BIT          NOT NULL,
    [InactiveFlag]       BIT          NOT NULL,
    [ConsultantFlag]     BIT          NOT NULL,
    [SupervisionFlag]    BIT          NOT NULL,
    [AnaesthetistFlag]   BIT          NOT NULL,
    [OtherFlag]          BIT          NOT NULL,
    [NurseFlag]          BIT          NOT NULL,
    [PorterFlag]         BIT          NOT NULL,
    [Technician1Flag]    BIT          NOT NULL,
    [Technician2Flag]    BIT          NOT NULL,
    [ClerkFlag]          BIT          NOT NULL,
    CONSTRAINT [PK_StaffCategory] PRIMARY KEY CLUSTERED ([StaffCategoryCode] ASC)
);


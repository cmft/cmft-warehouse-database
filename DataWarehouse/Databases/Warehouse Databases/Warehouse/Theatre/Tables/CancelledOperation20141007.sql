﻿CREATE TABLE [Theatre].[CancelledOperation20141007] (
    [SourceUniqueID]      INT           NULL,
    [CasenoteNumber]      VARCHAR (100) NULL,
    [DirectorateCode]     VARCHAR (100) NULL,
    [Directorate]         VARCHAR (100) NULL,
    [Specialty]           VARCHAR (100) NULL,
    [Consultant]          VARCHAR (100) NULL,
    [AdmissionDate]       DATETIME      NULL,
    [ProcedureDate]       DATETIME      NULL,
    [Ward]                VARCHAR (100) NULL,
    [Anaesthetist]        VARCHAR (100) NULL,
    [Surgeon]             VARCHAR (100) NULL,
    [Procedure]           VARCHAR (100) NULL,
    [CancellationDate]    DATETIME      NULL,
    [CancellationReason]  VARCHAR (100) NULL,
    [TCIDate]             DATETIME      NULL,
    [Comments]            VARCHAR (MAX) NULL,
    [ProcedureCompleted]  VARCHAR (100) NULL,
    [BreachwatchComments] VARCHAR (MAX) NULL,
    [Removed]             VARCHAR (100) NULL,
    [RemovedComments]     VARCHAR (MAX) NULL,
    [ReportableBreach]    VARCHAR (100) NULL
);


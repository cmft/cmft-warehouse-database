﻿CREATE TABLE [Theatre].[Anaesthetic] (
    [AnaestheticCode]  INT           NOT NULL,
    [Anaesthetic]      VARCHAR (255) NULL,
    [AnaestheticCode1] VARCHAR (6)   NULL,
    [InactiveFlag]     BIT           NOT NULL,
    CONSTRAINT [PK_Anaesthetic] PRIMARY KEY CLUSTERED ([AnaestheticCode] ASC)
);


﻿CREATE TABLE [Theatre].[Staff] (
    [StaffCode]         INT          NOT NULL,
    [Surname]           VARCHAR (60) NULL,
    [Forename]          VARCHAR (60) NULL,
    [Initial]           VARCHAR (3)  NULL,
    [StaffCode1]        VARCHAR (14) NULL,
    [StaffCategoryCode] INT          NULL,
    [SpecialtyCode]     INT          NULL,
    [NationalStaffCode] VARCHAR (20) NULL,
    CONSTRAINT [PK_TheatreStaff] PRIMARY KEY CLUSTERED ([StaffCode] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Staff]
    ON [Theatre].[Staff]([StaffCode1] ASC);


﻿CREATE TABLE [Theatre].[UnplannedReturnReason] (
    [UnplannedReturnReasonCode] VARCHAR (5)  NULL,
    [UnplannedReturnReason]     VARCHAR (60) NULL,
    [InactiveFlag]              NUMERIC (3)  NOT NULL
);


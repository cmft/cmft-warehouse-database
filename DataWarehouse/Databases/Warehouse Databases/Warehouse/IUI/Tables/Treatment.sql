﻿CREATE TABLE [IUI].[Treatment] (
    [TreatmentID] INT           IDENTITY (1, 1) NOT NULL,
    [Treatment]   NVARCHAR (50) NULL,
    [Type]        VARCHAR (50)  NULL,
    CONSTRAINT [PK__IUITreat__1A57B711460B083B] PRIMARY KEY CLUSTERED ([TreatmentID] ASC)
);


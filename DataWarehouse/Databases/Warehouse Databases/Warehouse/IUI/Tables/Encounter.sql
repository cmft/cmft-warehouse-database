﻿CREATE TABLE [IUI].[Encounter] (
    [EncounterRecno]   INT          IDENTITY (1, 1) NOT NULL,
    [DateOfBirth]      DATE         NULL,
    [NHSNumber]        VARCHAR (50) NULL,
    [CasenoteNumber]   VARCHAR (50) NULL,
    [PatientName]      VARCHAR (50) NULL,
    [AttemptNumber]    INT          NULL,
    [CommissionerCode] VARCHAR (50) NULL,
    [TreatmentDate]    DATE         NULL,
    [OutcomeID]        INT          NULL,
    [TreatmentID]      INT          NULL,
    [InterfaceCode]    VARCHAR (20) NULL,
    [Created]          DATETIME     NULL,
    [Updated]          DATETIME     NULL,
    [ByWhom]           VARCHAR (50) NULL
);


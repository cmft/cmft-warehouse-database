﻿CREATE TABLE [MSEC].[Comorbidities] (
    [CasenoteNumber] VARCHAR (20)  NULL,
    [PatientNo]      VARCHAR (8)   NOT NULL,
    [SpellNo]        VARCHAR (5)   NULL,
    [AdmissionDate]  SMALLDATETIME NOT NULL,
    [Type]           VARCHAR (30)  NOT NULL,
    [System]         VARCHAR (40)  NOT NULL,
    [Diagnosis]      VARCHAR (310) NOT NULL,
    [Status]         VARCHAR (80)  NOT NULL,
    [EventDate]      SMALLDATETIME NOT NULL,
    [ComorbidityID]  VARCHAR (80)  NOT NULL
);




GO
GRANT VIEW DEFINITION
    ON OBJECT::[MSEC].[Comorbidities] TO [cmmc\adam.black]
    AS [dbo];


GO
GRANT UPDATE
    ON OBJECT::[MSEC].[Comorbidities] TO [cmmc\adam.black]
    AS [dbo];


GO
GRANT SELECT
    ON OBJECT::[MSEC].[Comorbidities] TO [cmmc\adam.black]
    AS [dbo];


GO
GRANT REFERENCES
    ON OBJECT::[MSEC].[Comorbidities] TO [cmmc\adam.black]
    AS [dbo];


GO
GRANT INSERT
    ON OBJECT::[MSEC].[Comorbidities] TO [cmmc\adam.black]
    AS [dbo];


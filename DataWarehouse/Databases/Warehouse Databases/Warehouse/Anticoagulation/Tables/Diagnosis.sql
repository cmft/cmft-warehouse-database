﻿CREATE TABLE [Anticoagulation].[Diagnosis] (
    [DiagnosisID]   INT           NOT NULL,
    [DiagnosisCode] VARCHAR (25)  NULL,
    [Diagnosis]     VARCHAR (100) NOT NULL,
    CONSTRAINT [PK__Diagnosi__0C54CB93393B1D02] PRIMARY KEY CLUSTERED ([DiagnosisID] ASC)
);


﻿CREATE TABLE [Anticoagulation].[Commissioner] (
    [CommissionerID] INT          NOT NULL,
    [Commissioner]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__Commissi__A63B018E2EBD8E8F] PRIMARY KEY CLUSTERED ([CommissionerID] ASC)
);


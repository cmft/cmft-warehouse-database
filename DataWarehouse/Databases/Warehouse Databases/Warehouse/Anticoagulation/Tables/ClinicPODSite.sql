﻿CREATE TABLE [Anticoagulation].[ClinicPODSite] (
    [ClinicCode] VARCHAR (50) NOT NULL,
    [PODCode]    VARCHAR (10) NULL,
    [SiteCode]   VARCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([ClinicCode] ASC)
);


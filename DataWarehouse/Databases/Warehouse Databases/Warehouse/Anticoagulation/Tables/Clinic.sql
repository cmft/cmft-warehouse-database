﻿CREATE TABLE [Anticoagulation].[Clinic] (
    [ClinicID] INT          NOT NULL,
    [Clinic]   VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__Clinic__3347C2FD347667E5] PRIMARY KEY CLUSTERED ([ClinicID] ASC)
);


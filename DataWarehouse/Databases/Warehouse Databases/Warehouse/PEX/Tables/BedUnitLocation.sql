﻿CREATE TABLE [PEX].[BedUnitLocation] (
    [LocationID]     BIGINT        NOT NULL,
    [Location]       VARCHAR (255) NULL,
    [LocationTypeID] INT           NULL,
    [SiteID]         BIGINT        NOT NULL,
    [Site]           VARCHAR (255) NULL,
    PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


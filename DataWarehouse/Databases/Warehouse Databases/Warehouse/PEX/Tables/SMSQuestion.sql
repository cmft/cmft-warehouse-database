﻿CREATE TABLE [PEX].[SMSQuestion] (
    [QuestionID]     INT           NOT NULL,
    [Question]       VARCHAR (MAX) NULL,
    [QuestionTypeID] INT           NULL,
    PRIMARY KEY CLUSTERED ([QuestionID] ASC)
);


﻿CREATE TABLE [PEX].[SMSSurvey] (
    [SurveyID]  INT           NOT NULL,
    [Survey]    VARCHAR (MAX) NULL,
    [ChannelID] INT           NULL,
    PRIMARY KEY CLUSTERED ([SurveyID] ASC)
);


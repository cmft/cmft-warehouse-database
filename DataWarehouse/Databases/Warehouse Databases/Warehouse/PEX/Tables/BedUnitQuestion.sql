﻿CREATE TABLE [PEX].[BedUnitQuestion] (
    [QuestionID]     INT            NOT NULL,
    [Question]       NVARCHAR (MAX) NULL,
    [QuestionTypeID] INT            NULL,
    PRIMARY KEY CLUSTERED ([QuestionID] ASC)
);


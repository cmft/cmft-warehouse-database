﻿CREATE TABLE [PEX].[FriendsFamilyTestReturn] (
    [FFTRecno]                  INT           IDENTITY (1, 1) NOT NULL,
    [Return]                    VARCHAR (2)   NULL,
    [CensusDate]                DATE          NULL,
    [HospitalSiteCode]          VARCHAR (6)   NULL,
    [HospitalSite]              VARCHAR (MAX) NULL,
    [WardCode]                  VARCHAR (15)  NULL,
    [Ward]                      VARCHAR (MAX) NULL,
    [1ExtremelyLikely]          INT           NULL,
    [2Likely]                   INT           NULL,
    [3NeitherLikelyNorUnlikely] INT           NULL,
    [4Unlikely]                 INT           NULL,
    [5ExtremelyUnlikely]        INT           NULL,
    [6DontKnow]                 INT           NULL,
    [Responses]                 INT           NULL,
    [EligibleResponders]        INT           NULL,
    [InterfaceCode]             VARCHAR (10)  NULL,
    [Created]                   DATETIME      NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (50)  NULL
);


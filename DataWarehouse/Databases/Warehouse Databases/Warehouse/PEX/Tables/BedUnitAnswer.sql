﻿CREATE TABLE [PEX].[BedUnitAnswer] (
    [AnswerID] INT           NOT NULL,
    [Answer]   VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([AnswerID] ASC)
);


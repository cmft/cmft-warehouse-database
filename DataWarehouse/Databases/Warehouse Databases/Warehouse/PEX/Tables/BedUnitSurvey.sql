﻿CREATE TABLE [PEX].[BedUnitSurvey] (
    [SurveyID]  INT           NOT NULL,
    [Survey]    VARCHAR (MAX) NULL,
    [ChannelID] INT           NULL,
    PRIMARY KEY CLUSTERED ([SurveyID] ASC)
);


﻿CREATE TABLE [PEX].[SMSAnswer] (
    [AnswerID] BIGINT        NOT NULL,
    [Answer]   VARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([AnswerID] ASC)
);


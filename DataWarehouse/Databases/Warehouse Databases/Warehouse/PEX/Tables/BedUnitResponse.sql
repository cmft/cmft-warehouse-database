﻿CREATE TABLE [PEX].[BedUnitResponse] (
    [BedUnitResponseRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SurveyTakenID]        BIGINT        NOT NULL,
    [SurveyDate]           DATE          NOT NULL,
    [SurveyTime]           DATETIME      NOT NULL,
    [SurveyID]             INT           NOT NULL,
    [LocationID]           INT           NULL,
    [WardID]               INT           NULL,
    [QuestionID]           INT           NOT NULL,
    [QuestionNumber]       INT           NOT NULL,
    [AnswerID]             BIGINT        NULL,
    [Answer]               VARCHAR (MAX) NULL,
    [OptionNo]             INT           NOT NULL,
    [ResponseDate]         DATE          NOT NULL,
    [ResponseTime]         DATETIME      NOT NULL,
    [ChannelID]            INT           NULL,
    [QuestionTypeID]       INT           NULL,
    [LocationTypeID]       INT           NULL,
    [Created]              DATETIME      NULL,
    [Updated]              DATETIME      NULL,
    [ByWhom]               VARCHAR (50)  NULL
);


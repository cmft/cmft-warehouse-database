﻿CREATE TABLE [PEX].[KioskOnlinePostcardTabletLocation] (
    [LocationID]     INT           NOT NULL,
    [Location]       VARCHAR (255) NULL,
    [LocationTypeID] INT           NULL,
    PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


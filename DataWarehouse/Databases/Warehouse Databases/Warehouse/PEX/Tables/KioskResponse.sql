﻿CREATE TABLE [PEX].[KioskResponse] (
    [KioskResponseRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SurveyTakenID]      INT           NOT NULL,
    [SurveyID]           INT           NOT NULL,
    [QuestionID]         INT           NOT NULL,
    [AnswerID]           BIGINT        NULL,
    [Answer]             VARCHAR (MAX) NULL,
    [SurveyDate]         DATE          NOT NULL,
    [SurveyTime]         DATETIME      NOT NULL,
    [LocationID]         INT           NULL,
    [WardID]             INT           NULL,
    [DeviceNumber]       INT           NULL,
    [ChannelID]          INT           NULL,
    [QuestionTypeID]     INT           NULL,
    [LocationTypeID]     INT           NULL,
    [Created]            DATETIME      NULL,
    [Updated]            DATETIME      NULL,
    [ByWhom]             VARCHAR (50)  NULL
);


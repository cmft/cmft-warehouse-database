﻿CREATE TABLE [PEX].[KioskOnlinePostcardTabletQuestion] (
    [QuestionID]     INT            NOT NULL,
    [Question]       NVARCHAR (MAX) NULL,
    [QuestionTypeID] INT            NULL,
    PRIMARY KEY CLUSTERED ([QuestionID] ASC)
);


﻿CREATE TABLE [PEX].[KioskOnlinePostcardTabletAnswer] (
    [AnswerID] INT            NOT NULL,
    [Answer]   NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([AnswerID] ASC)
);


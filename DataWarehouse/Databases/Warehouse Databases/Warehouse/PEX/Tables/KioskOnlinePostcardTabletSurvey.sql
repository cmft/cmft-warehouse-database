﻿CREATE TABLE [PEX].[KioskOnlinePostcardTabletSurvey] (
    [SurveyID]  INT            NOT NULL,
    [Survey]    NVARCHAR (255) NULL,
    [ChannelID] INT            NULL,
    PRIMARY KEY CLUSTERED ([SurveyID] ASC)
);


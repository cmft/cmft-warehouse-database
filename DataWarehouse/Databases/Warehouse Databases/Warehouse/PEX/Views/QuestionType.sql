﻿

CREATE view [PEX].[QuestionType]

as

select
	QuestionTypeID = EntityCode
	,QuestionType = Description
from
	dbo.EntityLookup
   
where
	EntityTypeCode = 'PEXQuestionType' 
	



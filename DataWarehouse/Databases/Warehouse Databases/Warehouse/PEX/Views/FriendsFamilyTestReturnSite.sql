﻿
CREATE View [PEX].[FriendsFamilyTestReturnSite] as

Select distinct
	SiteCode = HospitalSiteCode
	,Site = HospitalSite
from 
	PEX.FriendsFamilyTestReturn
where
	HospitalSiteCode is not null
	


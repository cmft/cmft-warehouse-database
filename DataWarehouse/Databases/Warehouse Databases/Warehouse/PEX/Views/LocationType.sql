﻿

CREATE view [PEX].[LocationType]

as

select 
	LocationTypeID = EntityCode
	,LocationType = Description
from
	dbo.EntityLookup
   
where
	
	EntityTypeCode = 'PEXLOCATIONTYPE' 	
	



﻿

CREATE View [PEX].[FriendsFamilyTestReturnWard] as


Select 
	WardCode
	,Ward = min(Ward)
from 
	PEX.FriendsFamilyTestReturn
where
	WardCode is not null
group by
	WardCode
	



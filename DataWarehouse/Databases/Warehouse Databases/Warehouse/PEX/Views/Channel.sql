﻿
CREATE view [PEX].[Channel]

as

select
	ChannelID = EntityCode
	,Channel = Description
from
	dbo.EntityLookup
   
where
	EntityTypeCode = 'PEXCHANNEL' 
	


﻿


create view Complaint.Grade
as

select
	GradeCode = [BinaryCode]
	,Grade = [DESCRIPT]
	,SourceGradeCode = [CODE]
from
	[Ulysses].[ComplaintGradeBase]
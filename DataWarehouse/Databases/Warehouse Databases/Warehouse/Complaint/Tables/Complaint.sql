﻿CREATE TABLE [Complaint].[Complaint](
	[ComplaintRecno] [int] IDENTITY(1,1) NOT NULL,
	[SourceComplaintCode] [varchar](4) NULL,
	[ComplaintCode] [varbinary](50) NOT NULL,
	[ClaimantCode] [varchar](4) NULL,
	[ClaimTypeCode] [varchar](1) NULL,
	[BehalfOfCode] [varchar](4) NULL,
	[BehalfOfTypeCode] [varchar](1) NULL,
	[CaseNumber] [varchar](16) NULL,
	[ReceivedFromCode] [varchar](2) NULL,
	[ReceiptDate] [date] NULL,
	[AcknowledgementDate] [date] NULL,
	[AcknowledgementMethodCode] [varchar](1) NULL,
	[ComplaintDetail] [varchar](max) NULL,
	[ResolutionDate] [date] NULL,
	[OutcomeDetail] [varchar](max) NULL,
	[Satisfied] [varchar](1) NULL,
	[ResponseDate] [date] NULL,
	[DelayReasonCode] [varchar](2) NULL,
	[SeverityCode] [varchar](2) NULL,
	[ResponseDueDate] [date] NULL,
	[LetterDate] [date] NULL,
	[GradeCode] [varchar](50) NULL,
	[LikelihoodCode] [varchar](2) NULL,
	[RiskRatingCode] [varchar](2) NULL,
	[RiskScore] [int] NULL,
	[HandlerCode] [varchar](2) NULL,
	[StatusTypeCode] [varchar](50) NULL,
	[CaseTypeCode] [varchar](50) NULL,
	[ConsentRequired] [bit] NULL,
	[ConsentDate] [date] NULL,
	[EventDate] [date] NULL,
	[InitialContactDate] [date] NULL,
	[InitialDueDate] [date] NULL,
	[InitialSeverityCode] [varchar](2) NULL,
	[InitalScore] [int] NULL,
	[SourceTargetDate] [date] NULL,
	[SequenceNo] [int] NULL,
	[CategoryCode] [varchar](50) NULL,
	[CategoryTypeCode] [varchar](50) NULL,
	[PrimaryOrSecondary] [varchar](1) NULL,
	[OrganisationCode] [varchar](2) NULL,
	[SiteCode] [varchar](50) NULL,
	[SiteTypeCode] [varchar](2) NULL,
	[DivisionCode] [varchar](50) NULL,
	[DirectorateCode] [varchar](50) NULL,
	[DepartmentCode] [varchar](50) NULL,
	[WardCode] [varchar](50) NULL,
	[ServiceCode] [varchar](2) NULL,
	[ProfessionCode] [varchar](2) NULL,
	[Reopened] [bit] NULL,
	[ReopenedDetail] [varchar](max) NULL,
	[ConsentTargetDate] [date] NULL,
	[TargetDate7Day] [date] NULL,
	[TargetDate14Day] [date] NULL,
	[TargetDate25Day] [date] NULL,
	[TargetDate40Day] [date] NULL,
	[Duration] [int] NULL,
	[InterfaceCode] [varchar](5) NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NULL
) ON [PRIMARY]

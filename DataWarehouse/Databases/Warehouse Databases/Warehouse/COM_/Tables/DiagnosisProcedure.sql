﻿CREATE TABLE [COM].[DiagnosisProcedure] (
    [DiagnosisProcedureID]       NUMERIC (10)  NOT NULL,
    [DiagnosisProcedureCode]     VARCHAR (20)  NULL,
    [DiagnosisProcedureTypeCode] VARCHAR (25)  NULL,
    [DiagnosisProcedure]         VARCHAR (255) NULL,
    [ArchiveFlag]                CHAR (1)      NULL,
    CONSTRAINT [PK_DiagnosisProcedure_1] PRIMARY KEY CLUSTERED ([DiagnosisProcedureID] ASC)
);


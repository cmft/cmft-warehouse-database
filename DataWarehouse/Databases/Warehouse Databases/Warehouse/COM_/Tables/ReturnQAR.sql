﻿CREATE TABLE [COM].[ReturnQAR] (
    [ReturnPeriod]      VARCHAR (9)  NOT NULL,
    [LoadDate]          DATE         NULL,
    [Return]            VARCHAR (10) NOT NULL,
    [ReturnItem]        VARCHAR (25) NOT NULL,
    [InterfaceCode]     VARCHAR (5)  NOT NULL,
    [SourceUniqueNo]    NUMERIC (10) NULL,
    [ItemDate]          DATE         NULL,
    [ItemSpecialty]     VARCHAR (50) NULL,
    [NHSNumber]         VARCHAR (20) NULL,
    [ProfessionalCarer] VARCHAR (80) NOT NULL,
    [CommissionerCode]  VARCHAR (6)  NULL,
    [Commissioner]      VARCHAR (50) NULL
);


﻿CREATE TABLE [COM].[WaitPTLBase] (
    [EncounterRecno] INT NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [ix_WaitPTLBase_EncounterRecno]
    ON [COM].[WaitPTLBase]([EncounterRecno] ASC);


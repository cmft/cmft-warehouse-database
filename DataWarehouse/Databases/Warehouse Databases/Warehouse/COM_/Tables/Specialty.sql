﻿CREATE TABLE [COM].[Specialty] (
    [SpecialtyID]             NUMERIC (10)  NOT NULL,
    [SpecialtyCode]           VARCHAR (20)  NULL,
    [Specialty]               VARCHAR (255) NULL,
    [SpecialtyParentID]       NUMERIC (10)  NULL,
    [SpecialtyHealthOrgOwner] NUMERIC (10)  NULL,
    CONSTRAINT [PK_Specialty_2] PRIMARY KEY CLUSTERED ([SpecialtyID] ASC)
);


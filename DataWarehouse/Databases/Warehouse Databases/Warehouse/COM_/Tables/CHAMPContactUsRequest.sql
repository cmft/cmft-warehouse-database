﻿CREATE TABLE [COM].[CHAMPContactUsRequest] (
    [CHAMPContactUsRequestID]  INT           NOT NULL,
    [ContactUsRequestTime]     DATETIME2 (0) NOT NULL,
    [EmailAddressTo]           VARCHAR (255) NOT NULL,
    [EmailAddressFrom]         VARCHAR (255) NULL,
    [EmailFromName]            VARCHAR (255) NULL,
    [EmailChildName]           VARCHAR (255) NULL,
    [EmailRelationshipToChild] VARCHAR (50)  NULL,
    [EmailMessage]             VARCHAR (MAX) NULL,
    [EmailFullContent]         VARCHAR (MAX) NULL,
    [Created]                  DATETIME2 (3) CONSTRAINT [DF_CHAMPContactUsRequest_Created] DEFAULT (getdate()) NULL,
    [Updated]                  DATETIME2 (3) CONSTRAINT [DF_CHAMPContactUsRequest_Updated] DEFAULT (getdate()) NULL,
    [ByWhom]                   VARCHAR (50)  CONSTRAINT [DF_CHAMPContactUsRequest_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_CHAMPContactUsRequest] PRIMARY KEY CLUSTERED ([CHAMPContactUsRequestID] ASC)
);


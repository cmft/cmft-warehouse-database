﻿CREATE TABLE [COM].[SystemUser] (
    [SystemUserID]    INT          NOT NULL,
    [SystemUserRefno] NUMERIC (10) NULL,
    [SystemUserCode]  VARCHAR (20) NOT NULL,
    [SystemUser]      VARCHAR (35) NULL,
    [Department]      VARCHAR (80) NULL,
    [Email]           VARCHAR (80) NULL,
    [ArchiveCode]     CHAR (1)     NULL,
    CONSTRAINT [PK_SystemUser_1] PRIMARY KEY CLUSTERED ([SystemUserCode] ASC)
);


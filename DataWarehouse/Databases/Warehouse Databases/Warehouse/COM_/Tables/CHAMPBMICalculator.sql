﻿CREATE TABLE [COM].[CHAMPBMICalculator] (
    [BMICalculatorTime] DATETIME2 (0) NOT NULL,
    [Username]          VARCHAR (255) NULL,
    [Created]           DATETIME2 (3) CONSTRAINT [DF_CHAMPBMICalculator_Created] DEFAULT (getdate()) NULL,
    [Updated]           DATETIME2 (3) NULL,
    [ByWhom]            VARCHAR (50)  NULL
);


﻿CREATE TABLE [COM].[LookupBase] (
    [LookupID]          NUMERIC (10) NOT NULL,
    [LookupTypeID]      VARCHAR (5)  NULL,
    [LookupDescription] VARCHAR (80) NULL,
    [LookupCode]        VARCHAR (25) NULL,
    [LookupNHSCode]     VARCHAR (50) NULL,
    [LookupCDSCode]     VARCHAR (50) NULL,
    [LookupNatCode]     VARCHAR (50) NULL,
    [ArchiveFlag]       CHAR (1)     NULL,
    [HealthOrgOwner]    NUMERIC (10) NULL,
    CONSTRAINT [PK_LookupBase_1] PRIMARY KEY NONCLUSTERED ([LookupID] ASC)
);


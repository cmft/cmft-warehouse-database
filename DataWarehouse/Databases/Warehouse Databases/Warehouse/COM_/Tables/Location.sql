﻿CREATE TABLE [COM].[Location] (
    [LocationID]         INT           IDENTITY (1, 1) NOT NULL,
    [LocationType]       VARCHAR (80)  NULL,
    [Location]           VARCHAR (100) NULL,
    [LocationClinic]     VARCHAR (100) NULL,
    [LocationClinicCode] VARCHAR (100) NULL,
    [LocationCode]       NUMERIC (25)  NULL,
    CONSTRAINT [PK_locationID] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


﻿CREATE TABLE [COM].[StaffTeam] (
    [StaffTeamID]             NUMERIC (10)  NOT NULL,
    [StaffTeamCode]           VARCHAR (5)   NULL,
    [StaffTeam]               VARCHAR (20)  NULL,
    [StartTime]               DATETIME      NULL,
    [EndTime]                 DATETIME      NULL,
    [StaffTeamDescription]    VARCHAR (255) NULL,
    [StaffTeamSpecialtyID]    NUMERIC (10)  NULL,
    [TeamLeaderID]            NUMERIC (10)  NULL,
    [ArchiveFlag]             CHAR (1)      NULL,
    [StaffTeamHealthOrgOwner] NUMERIC (10)  NULL,
    CONSTRAINT [PK_StaffTeam] PRIMARY KEY CLUSTERED ([StaffTeamID] ASC)
);


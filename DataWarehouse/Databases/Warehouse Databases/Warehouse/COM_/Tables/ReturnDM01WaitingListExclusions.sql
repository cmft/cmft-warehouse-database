﻿CREATE TABLE [COM].[ReturnDM01WaitingListExclusions] (
    [SourceUniqueNo]    NUMERIC (10)  NOT NULL,
    [IsExcluded]        BIT           NOT NULL,
    [ExclusionReason]   INT           NULL,
    [ExclusionComments] VARCHAR (MAX) NULL,
    [CreatedAt]         DATETIME2 (7) DEFAULT (sysdatetime()) NOT NULL,
    [CreatedBy]         [sysname]     NULL,
    CONSTRAINT [PK_COM_RMWLEx_SUN] PRIMARY KEY CLUSTERED ([SourceUniqueNo] ASC),
    CONSTRAINT [CHK_COM_ReturnDM01WaitingListExclusions] CHECK ([IsExcluded]=(0) AND [ExclusionReason] IS NULL OR [IsExcluded]=(1) AND [ExclusionReason] IS NOT NULL)
);


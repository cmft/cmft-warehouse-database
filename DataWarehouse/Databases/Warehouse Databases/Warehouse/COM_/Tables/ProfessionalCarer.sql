﻿CREATE TABLE [COM].[ProfessionalCarer] (
    [ProfessionalCarerID]     NUMERIC (10)  NOT NULL,
    [ProfessionalCarerCode]   VARCHAR (25)  NULL,
    [ProfessionalCarerTypeID] NUMERIC (10)  NULL,
    [Title]                   VARCHAR (80)  NULL,
    [Forename]                VARCHAR (30)  NULL,
    [Surname]                 VARCHAR (30)  NULL,
    [FullName]                VARCHAR (100) NULL,
    [StartTime]               DATETIME      NULL,
    [EndTime]                 DATETIME      NULL,
    [PrimaryStaffTeamID]      NUMERIC (10)  NULL,
    [ArchiveFlag]             CHAR (1)      NULL,
    [HealthOrgOwner]          NUMERIC (10)  NULL,
    CONSTRAINT [PK_ProfessionalCarer] PRIMARY KEY CLUSTERED ([ProfessionalCarerID] ASC)
);


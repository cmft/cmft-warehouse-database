﻿CREATE TABLE [COM].[CaseType] (
    [Code]             NUMERIC (10) NOT NULL,
    [TypeGroupChannel] VARCHAR (20) NULL,
    [TypeGroup]        VARCHAR (20) NULL,
    [Type]             VARCHAR (30) NULL,
    CONSTRAINT [PK_CaseType] PRIMARY KEY CLUSTERED ([Code] ASC)
);


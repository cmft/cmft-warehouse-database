﻿CREATE TABLE [COM].[CHAMPAccount] (
    [AccountID]          VARCHAR (255) NOT NULL,
    [ContactID]          VARCHAR (255) NOT NULL,
    [AccountCreatedTime] DATETIME2 (0) NULL,
    [Account]            VARCHAR (100) NULL,
    [ContactFullName]    VARCHAR (160) NULL,
    [ContactFirstName]   VARCHAR (50)  NULL,
    [ContactLastName]    VARCHAR (50)  NULL,
    [Created]            DATETIME2 (3) NULL,
    [Updated]            DATETIME2 (3) NULL,
    [ByWhom]             VARCHAR (50)  NULL,
    CONSTRAINT [PK_COMCHAMPAccount] PRIMARY KEY CLUSTERED ([AccountID] ASC)
);


﻿CREATE TABLE [COM].[Service] (
    [ID]               INT           IDENTITY (1, 1) NOT NULL,
    [Code]             VARCHAR (10)  NULL,
    [Name]             VARCHAR (255) NULL,
    [SPECT_REFNO]      INT           NULL,
    [IPMDescription]   VARCHAR (255) NULL,
    [Destination]      VARCHAR (50)  NULL,
    [TCSDestinationID] TINYINT       NULL,
    [District]         VARCHAR (50)  NULL,
    [Directorate]      VARCHAR (255) NULL,
    [StartDate]        DATETIME      NULL,
    [EndDate]          DATETIME      NULL,
    [ArchiveFlag]      BIT           NULL,
    [ModifiedDate]     DATETIME      NULL,
    [ModifiedUser]     VARCHAR (50)  NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([ID] ASC)
);


﻿


CREATE PROCEDURE [COM].[GetDM01WaitingListAdjustments]

AS

BEGIN

SET NOCOUNT ON 

SELECT
	 WL.ReturnPeriod
	,WL.LoadDate
	--,[Return]
	--,ReturnItem
	--,InterfaceCode
	 ,WL.SourceUniqueNo
	,WL.ItemDate
	--,ItemTime
	,WL.ItemSpecialty
	,WL.NHSNumber
	,WL.ReferredByProfessionalCarer
	,WL.ScheduleType
	,WL.StaffTeam
	--,SeenByProfessionalCarer
	--,PatientAge
	,WL.Clinic
	--,ReferralSource
	,WL.AppointmentOutcome
	,WL.ReferralClosedDate
	,WL.FirstContact
	,WL.AppointmentSpecialty
	,WL.RecentActivity
	--,StatusTypeCode
	,WL.StatusType
	--,GPPracticeCode
	--,GPPractice
	--,CommissionerCode
	--,Commissioner
	,WL.IsExcluded
	,WL.ExclusionReason
	,WL.ExclusionComments
	,PeriodType = 'Current Month'
	,UploadDate = CONVERT(DATE,CURRENT_TIMESTAMP,113)

FROM  
	COM.ReturnDM01WaitingList WL
INNER JOIN (
			SELECT 
				 MAX(LoadDate)AS LoadDate
				,MAX(ReturnPeriod) AS ReturnPeriod
			FROM
				COM.ReturnDM01WaitingList
			)MaxWLDate
	ON WL.LoadDate = MaxWLDate.LoadDate
	AND WL.ReturnPeriod = MaxWLDate.ReturnPeriod
WHERE WL.IsExcluded IS NULL


UNION ALL


SELECT
	 WL.ReturnPeriod
	,WL.LoadDate
	--,[Return]
	--,ReturnItem
	--,InterfaceCode
	 ,WL.SourceUniqueNo
	,WL.ItemDate
	--,ItemTime
	,WL.ItemSpecialty
	,WL.NHSNumber
	,WL.ReferredByProfessionalCarer
	,WL.ScheduleType
	,WL.StaffTeam
	--,SeenByProfessionalCarer
	--,PatientAge
	,WL.Clinic
	--,ReferralSource
	,WL.AppointmentOutcome
	,WL.ReferralClosedDate
	,WL.FirstContact
	,WL.AppointmentSpecialty
	,WL.RecentActivity
	--,StatusTypeCode
	,WL.StatusType
	--,GPPracticeCode
	--,GPPractice
	--,CommissionerCode
	--,Commissioner
	,WL.IsExcluded
	,WL.ExclusionReason
	,WL.ExclusionComments
	,PeriodType = 'Previous Month'
	,UploadDate = CONVERT(DATE,CURRENT_TIMESTAMP,113)

FROM  
	COM.ReturnDM01WaitingList WL
INNER JOIN (
			SELECT 
				 MAX(LoadDate)AS LoadDate
				,MAX(ReturnPeriod) AS ReturnPeriod
			FROM
				COM.ReturnDM01WaitingList WL
			WHERE  NOT EXISTS	
					(SELECT  1
					FROM (SELECT 
								 MAX(LoadDate)AS LoadDate
								,MAX(ReturnPeriod) AS ReturnPeriod
							FROM
								COM.ReturnDM01WaitingList
							) WLPrevious
					WHERE WL.ReturnPeriod = WLPrevious.ReturnPeriod
					AND WL.LoadDate = WLPrevious.LoadDate
					)
			)MaxWLDate
	ON WL.LoadDate = MaxWLDate.LoadDate
	AND WL.ReturnPeriod = MaxWLDate.ReturnPeriod

WHERE WL.IsExcluded IS NULL;


END;

﻿


CREATE VIEW [COM].[DM01WaitingListAudWeeksTCI]
--P.McNulty 01/08/2012

AS


SELECT
	 WL.SourceUniqueNo as 'Reference No'	--retain for reference
	,WL.[Weeks To TCI]
	,WL.ItemDate as 'Referral Date'
	,WL.NHSNumber
	,WL.StaffTeam	
	,WL.Clinic
	,WL.AppointmentOutcome
	,WL.FirstContact
	,WL.Comment
	
	--Note. Audiology would wish to add 'Appointment By' date from
	--waiting list...
	
	
FROM 
	COM.ReturnDM01WaitingListAud WL
	




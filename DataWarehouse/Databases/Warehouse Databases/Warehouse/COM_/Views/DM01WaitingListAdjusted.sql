﻿

CREATE VIEW [COM].[DM01WaitingListAdjusted]

AS


SELECT
	 WL.ReturnPeriod
	,WL.LoadDate
	--,[Return]
	--,ReturnItem
	--,InterfaceCode
	 ,WL.SourceUniqueNo
	,WL.ItemDate
	--,ItemTime
	,WL.ItemSpecialty
	,WL.NHSNumber
	,WL.ReferredByProfessionalCarer
	,WL.ScheduleType
	,WL.StaffTeam
	--,SeenByProfessionalCarer
	--,PatientAge
	,WL.Clinic
	--,ReferralSource
	,WL.AppointmentOutcome
	,WL.ReferralClosedDate
	,WL.FirstContact
	,WL.AppointmentSpecialty
	,WL.RecentActivity
	--,StatusTypeCode
	,WL.StatusType
	--,GPPracticeCode
	--,GPPractice
	--,CommissionerCode
	--,Commissioner
	,WL.IsExcluded
	,WL.ExclusionReason
	,WL.ExclusionComments
FROM 
	COM.ReturnDM01WaitingList WL

INNER JOIN
	(
	SELECT 
		MAX(LoadDate)AS LoadDate
		,ReturnPeriod 
	FROM
		COM.ReturnDM01WaitingList
	GROUP BY
		ReturnPeriod
	) MaxWLDate
ON	WL.LoadDate = MaxWLDate.LoadDate
AND WL.ReturnPeriod = MaxWLDate.ReturnPeriod

LEFT JOIN COM.ReturnDM01WaitingListExclusions WLE
ON	WL.SourceUniqueNo = WLE.SourceUniqueNo

WHERE 
	WLE.IsExcluded IS NULL;

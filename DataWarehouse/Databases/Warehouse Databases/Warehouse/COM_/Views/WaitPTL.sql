﻿



CREATE view [COM].[WaitPTL] as

select
	   WL.EncounterRecno
	  ,SourceUniqueID
      ,SourceEncounterNo
      ,CensusDate
      ,ReferredBySpecialtyID
      ,ReferredToSpecialtyID
      ,SourceofReferralID
      ,ReferralReceivedTime
      ,ReasonforReferralID
      ,CancelledTime
      ,CancelledReasonID
      ,ReferralSentTime
      ,UrgencyID
      ,ReferralPriorityID
      ,AuthoriedTime
      ,ReferralClosedTime
      ,ClosureReasonID
      ,ReferredByProfessionalCarerID
      ,ReferredByStaffTeamID
      ,ReferredByHealthOrgID
      ,ReferredToProfessionalCarerID
      ,ReferredToHealthOrgID
      ,ReferredToStaffTeamID
      ,OutcomeOfReferralID
      ,ReferralStatusID
      ,RejectionID
      ,TypeofReferralID
      ,PatientSourceID
      ,PatientSourceSystemUniqueID
      ,PatientNHSNumber
      ,PatientNHSNumberStatusIndicator
      ,PatientLocalIdentifier
      ,PatientPASIdentifier
      ,PatientIdentifier
      ,PatientFacilityIdentifier
      ,PatientTitleID
      ,PatientForename
      ,PatientSurname
      ,PatientAddress1
      ,PatientAddress2
      ,PatientAddress3
      ,PatientAddress4
      ,PatientPostcode
      ,PatientDateOfBirth
      ,PatientDateOfDeath
      ,PatientSexID
      ,PatientEthnicGroupID
      ,PatientSpokenLanguageID
      ,PatientReligionID
      ,PatientMaritalStatusID
      ,ProfessionalCarerCurrentID
      ,ProfessionalCarerHealthOrgCurrentID
      ,ProfessionalCarerParentHealthOrgCurrentID
      ,ProfessionalCarerScheduleID
      ,ProfessionalCarerHealthOrgScheduleID
      ,ProfessionalCarerParentHealthOrgScheduleID
      ,ProfessionalCarerReferralID
      ,ProfessionalCarerHealthOrgReferralID
      ,ProfessionalCarerParentHealthOrgReferralID
      ,RTTStatusDate
      ,RTTPatientPathwayID
      ,RTTStartTime
      ,RTTStartFlag
      ,RTTStatusID
      ,CreatedTime
      ,ModifiedTime
      ,CreatedByID
      ,ModifiedByID
      ,ArchiveFlag
      ,ParentID
      ,HealthOrgOwner
      ,RequestedServiceID
      ,ScheduleID
      ,ScheduleSpecialtyID
      ,ScheduleStaffTeamID
      ,ScheduleProfessionalCarerID
      ,ScheduleSeenByProfessionalCarerID
      ,ScheduleStartTime
      ,ScheduleEndTime
      ,ScheduleArrivedTime
      ,ScheduleSeenTime
      ,ScheduleDepartedTime
      ,ScheduleAttendedID
      ,ScheduleOutcomeID
      ,ScheduleVisitID
      ,ScheduleContactTypeID
      ,ScheduleTypeID
      ,ScheduleReasonID
      ,ScheduleCanceledReasonID
      ,ScheduleCanceledTime
      ,ScheduleCancelledByID
      ,ScheduleMoveReasonID
      ,ScheduleMovetime
      ,ScheduleServicePointID
      ,ScheduleServicePointSessionID
      ,ScheduleMoveCount
      ,ScheduleLocationTypeID
      ,ScheduleLocationDescription
      ,ScheduleLocationTypeHealthOrgID
      ,ScheduleWaitingListID
      ,KornerWait
      ,BreachDate
      ,NationalBreachDate
      ,BreachDays
      ,NationalBreachDays
      ,RTTBreachDate
      ,ClockStartDate
      ,ScheduleOutcomeCode
      ,PatientDeathIndicator
      ,CountOfDNAs
      ,CountOfHospitalCancels
      ,CountOfPatientCancels
      ,LastAppointmentFlag
      ,AdditionFlag
      ,FuturePatientCancelDate
      ,PCTCode
      ,StartWaitDate
      ,EndWaitDate
      ,BookedTime
      ,DerivedFirstAttendanceFlag
	  ,DerivedWaitFlag
	  ,convert(int, coalesce(WL.KornerWait, 0) / 7) WeeksWaiting

	,WL.Created
	,WL.Updated
	,WL.ByWhom
from
	COM.Wait WL

--new only
inner join COM.WaitPTLBase
on	WL.EncounterRecno = WaitPTLBase.EncounterRecno










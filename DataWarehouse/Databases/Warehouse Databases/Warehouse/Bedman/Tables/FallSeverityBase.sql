﻿CREATE TABLE [Bedman].[FallSeverityBase] (
    [ID]          INT          NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [Active]      BIT          NOT NULL,
    [Order]       INT          NOT NULL,
    CONSTRAINT [PK__FallSeve__3214EC2744EBFB79] PRIMARY KEY CLUSTERED ([ID] ASC)
);


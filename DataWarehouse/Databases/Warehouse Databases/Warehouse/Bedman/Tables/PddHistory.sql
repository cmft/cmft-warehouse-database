﻿CREATE TABLE [Bedman].[PddHistory] (
    [PddHistoryID]     INT      NOT NULL,
    [SourceSpellNo]    INT      NOT NULL,
    [PddSequence]      BIGINT   NULL,
    [prev_PddSequence] BIGINT   NULL,
    [next_PddSequence] BIGINT   NULL,
    [FirstPdd]         DATETIME NULL,
    [LastPdd]          DATETIME NULL,
    [Pdd]              DATETIME NULL,
    [prev_Pdd]         DATETIME NULL,
    [next_Pdd]         DATETIME NULL,
    [PddCreated]       DATETIME NULL,
    [PddPAS]           INT      NULL,
    [PddReasonCode]    INT      NULL
);


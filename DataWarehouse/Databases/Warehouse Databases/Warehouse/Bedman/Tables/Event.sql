﻿CREATE TABLE [Bedman].[Event] (
    [BedManEventRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]   INT           NOT NULL,
    [SourceSpellNo]    VARCHAR (100) NULL,
    [EventDate]        DATETIME      NULL,
    [EventTypeCode]    VARCHAR (20)  NULL,
    [EventDetails]     XML           NULL,
    [SourcePatientNo]  VARCHAR (8)   NULL,
    [AdmissionTime]    DATETIME      NULL,
    [DischargeTime]    DATETIME      NULL,
    [Created]          DATETIME      NULL,
    [Updated]          DATETIME      NULL,
    [ByWhom]           VARCHAR (50)  NULL,
    CONSTRAINT [PK__Event__B4136D3011416EEA] PRIMARY KEY CLUSTERED ([BedManEventRecno] ASC)
);


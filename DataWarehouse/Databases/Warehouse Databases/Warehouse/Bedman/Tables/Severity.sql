﻿CREATE TABLE [Bedman].[Severity] (
    [SeverityCode] VARCHAR (20)  NOT NULL,
    [Severity]     VARCHAR (255) NULL,
    CONSTRAINT [PK__Severity__7E2D57EB2B0140ED] PRIMARY KEY CLUSTERED ([SeverityCode] ASC)
);


﻿CREATE TABLE [Bedman].[PressureUlcerCategoryBase] (
    [ID]          INT          NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [Active]      BIT          NOT NULL,
    [Order]       INT          NOT NULL,
    CONSTRAINT [PK__Pressure__3214EC27411B6A95] PRIMARY KEY CLUSTERED ([ID] ASC)
);


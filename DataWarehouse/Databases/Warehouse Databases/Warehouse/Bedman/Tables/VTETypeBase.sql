﻿CREATE TABLE [Bedman].[VTETypeBase] (
    [ID]          INT          NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [Active]      BIT          NOT NULL,
    [Order]       INT          NOT NULL,
    CONSTRAINT [PK__VTETypeB__3214EC274D81417A] PRIMARY KEY CLUSTERED ([ID] ASC)
);


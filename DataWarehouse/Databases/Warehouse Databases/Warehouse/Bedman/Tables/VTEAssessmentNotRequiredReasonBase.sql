﻿CREATE TABLE [Bedman].[VTEAssessmentNotRequiredReasonBase] (
    [ID]          INT           NOT NULL,
    [Description] VARCHAR (100) NOT NULL,
    [Active]      BIT           NOT NULL,
    [Order]       INT           NOT NULL,
    CONSTRAINT [PK__VTEAsses__3214EC2738862494] PRIMARY KEY CLUSTERED ([ID] ASC)
);


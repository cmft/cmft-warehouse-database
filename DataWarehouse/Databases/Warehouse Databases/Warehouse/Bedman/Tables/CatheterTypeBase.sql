﻿CREATE TABLE [Bedman].[CatheterTypeBase] (
    [ID]          INT          NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [Active]      BIT          NOT NULL,
    [Order]       INT          NOT NULL,
    CONSTRAINT [PK__Catheter__3214EC2748BC8C5D] PRIMARY KEY CLUSTERED ([ID] ASC)
);


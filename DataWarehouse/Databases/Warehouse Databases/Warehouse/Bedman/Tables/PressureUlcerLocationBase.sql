﻿CREATE TABLE [Bedman].[PressureUlcerLocationBase] (
    [ID]          INT          NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [Active]      BIT          NOT NULL,
    [Order]       INT          NOT NULL,
    CONSTRAINT [PK__Pressure__3214EC273D4AD9B1] PRIMARY KEY CLUSTERED ([ID] ASC)
);


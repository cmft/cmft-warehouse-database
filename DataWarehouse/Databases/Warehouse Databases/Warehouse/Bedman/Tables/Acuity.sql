﻿CREATE TABLE [Bedman].[Acuity] (
    [DailyScoreID]              INT            NOT NULL,
    [WardCode]                  VARCHAR (4)    NOT NULL,
    [ScoreTime]                 SMALLDATETIME  NOT NULL,
    [TotalPatientsLevel0]       DECIMAL (8, 2) NULL,
    [TotalPatientsLevel0Score]  DECIMAL (8, 2) NULL,
    [AcuityMutiplierLevel0]     DECIMAL (8, 2) NULL,
    [TotalPatientsLevel1a]      DECIMAL (8, 2) NULL,
    [AcuityMutiplierLevel1a]    DECIMAL (8, 2) NULL,
    [TotalPatientsLevel1aScore] DECIMAL (8, 2) NULL,
    [TotalPatientsLevel1b]      DECIMAL (8, 2) NULL,
    [AcuityMutiplierLevel1b]    DECIMAL (8, 2) NULL,
    [TotalPatientsLevel1bScore] DECIMAL (8, 2) NULL,
    [TotalPatientsLevel2]       DECIMAL (8, 2) NULL,
    [AcuityMutiplierLevel2]     DECIMAL (8, 2) NULL,
    [TotalPatientsLevel2Score]  DECIMAL (8, 2) NULL,
    [TotalPatientsLevel3]       DECIMAL (8, 2) NULL,
    [AcuityMutiplierLevel3]     DECIMAL (8, 2) NULL,
    [TotalPatientsLevel3Score]  DECIMAL (8, 2) NULL,
    [AvailableBeds]             INT            NOT NULL
);


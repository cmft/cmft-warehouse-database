﻿CREATE TABLE [Bedman].[EventType] (
    [EventTypeCode] VARCHAR (20)  NOT NULL,
    [EventType]     VARCHAR (255) NULL,
    CONSTRAINT [PK__EventTyp__69F6F70B2730B009] PRIMARY KEY CLUSTERED ([EventTypeCode] ASC)
);


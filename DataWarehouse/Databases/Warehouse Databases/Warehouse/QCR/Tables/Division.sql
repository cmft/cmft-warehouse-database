﻿CREATE TABLE [QCR].[Division] (
    [DivisionCode] INT           NOT NULL,
    [Division]     NVARCHAR (50) NULL,
    CONSTRAINT [PK_Division_1] PRIMARY KEY CLUSTERED ([DivisionCode] ASC)
);


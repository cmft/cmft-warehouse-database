﻿CREATE TABLE [QCR].[Question] (
    [QuestionCode]   INT            NOT NULL,
    [AuditTypeCode]  INT            NOT NULL,
    [Question]       NVARCHAR (MAX) NULL,
    [Guidance]       NVARCHAR (MAX) NULL,
    [AdditionalText] NVARCHAR (255) NULL,
    [QuestionOrder]  INT            NULL,
    CONSTRAINT [PK_Question] PRIMARY KEY CLUSTERED ([QuestionCode] ASC)
);


﻿CREATE TABLE [QCR].[CategoryType] (
    [CategoryTypeCode] INT           NOT NULL,
    [CategoryType]     NVARCHAR (50) NULL,
    CONSTRAINT [PK_CategoryType] PRIMARY KEY CLUSTERED ([CategoryTypeCode] ASC)
);


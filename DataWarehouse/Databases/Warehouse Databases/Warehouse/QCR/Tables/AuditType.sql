﻿CREATE TABLE [QCR].[AuditType] (
    [AuditTypeCode] INT           NOT NULL,
    [AuditType]     VARCHAR (255) NULL,
    [AuditOrder]    INT           NULL
);


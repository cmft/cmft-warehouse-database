﻿CREATE TABLE [QCR].[AuditAnswer] (
    [AuditAnswerRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]   INT           NOT NULL,
    [AuditTime]        SMALLDATETIME NOT NULL,
    [AuditDate]        SMALLDATETIME NOT NULL,
    [LocationCode]     INT           NULL,
    [WardCode]         INT           NULL,
    [AuditTypeCode]    INT           NULL,
    [QuestionCode]     INT           NULL,
    [Answer]           INT           NULL,
    CONSTRAINT [PK_Encounter_7] PRIMARY KEY CLUSTERED ([AuditAnswerRecno] ASC)
);


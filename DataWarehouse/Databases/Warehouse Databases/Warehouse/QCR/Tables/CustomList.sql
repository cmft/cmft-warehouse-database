﻿CREATE TABLE [QCR].[CustomList] (
    [CustomListCode] INT           NOT NULL,
    [CustomList]     NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_CustomList] PRIMARY KEY CLUSTERED ([CustomListCode] ASC)
);


﻿CREATE TABLE [QCR].[Location] (
    [LocationCode]      INT            NOT NULL,
    [Location]          NVARCHAR (255) NULL,
    [CategoryTypeCode]  INT            NULL,
    [CategoryGroupCode] INT            NULL,
    [DivisionCode]      INT            NULL,
    [Active]            BIT            NOT NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([LocationCode] ASC)
);


﻿CREATE TABLE [QCR].[CategoryGroup] (
    [CategoryGroupCode] INT           NOT NULL,
    [CategoryGroup]     NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_CategoryGroup] PRIMARY KEY CLUSTERED ([CategoryGroupCode] ASC)
);


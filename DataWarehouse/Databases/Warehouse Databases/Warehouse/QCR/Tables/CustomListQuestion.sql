﻿CREATE TABLE [QCR].[CustomListQuestion] (
    [CustomListCode] INT NOT NULL,
    [QuestionCode]   INT NOT NULL,
    CONSTRAINT [PK_CustomListQuestions] PRIMARY KEY CLUSTERED ([CustomListCode] ASC, [QuestionCode] ASC)
);


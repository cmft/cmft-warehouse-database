﻿

CREATE view [QCR].[Ward]


as

select
	WardCode = [LocationCode] 
	,Ward = [Location]
	--,[CategoryTypeCode]
	--,[CategoryGroupCode]
	--,[DivisionCode]
	--,[Active]
from
	[QCR].[Location]
where
	[CategoryTypeCode] in (1, 3, 5)

﻿


CREATE procedure [Envoy].[BuildAgentInputFile]
as

/****************************************************************************************
	Stored procedure : [Envoy].[BuildAgentInputFile]
	Description		 : Proc for Healthcare Communications appointment reminders - human agent(?)
	

	Modification History
	====================
	
	Date		Person				Description
	====================================================================================
	?			C Butterworth       Intial Coding
	26/03/2014	Paul Egan			Added FileType filter on inner join to Envoy.SpecialtyMap. This is
									due to Envoy.SpecialtyMap now having more PAS specialty codes for all
									clinics under those specialty(s).
	25/09/2014	Paul Egan			Biofeedback clinic exclusion requested by Suzanne Ryder.
*****************************************************************************************/

declare @StartTime datetime
declare @Elapsed int
declare @RowsDeleted Int
declare @RowsInserted Int
declare @RowsUpdated Int
declare @Stats varchar(255)

set dateformat dmy

select
	@StartTime = getdate()



INSERT INTO Envoy.AgentInputFile
(
	 patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name
	,last_name
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,Division
	,appt_audit_ref
	,appt_type
	,location
	,practitioner
	,procedure_code
	,patient_email
	,genericinfo1
	,genericinfo2
	,genericinfo3
)

SELECT 
	 patient_id = Encounter.DistrictNo
	,appointment_date = convert(varchar , Encounter.AppointmentDate , 103)
	,appointment_time = Encounter.AppointmentTime

	,mobile_number =
		case
		when left(Encounter.MobilePhone , 2) = '07' then Encounter.MobilePhone
		when left(Encounter.HomePhone , 2) = '07' then Encounter.HomePhone
		else null
		end
		
	,landline_number =
		case
		when left(Encounter.HomePhone , 2) != '07' then Encounter.HomePhone
		when left(Encounter.MobilePhone , 2)  != '07' then Encounter.MobilePhone
		else null
		end

	,country_code = '44'

	,contact_tel_no = ''

	,first_name = Encounter.Forenames
	,last_name = Encounter.Surname
	,dob = Encounter.DateOfBirth

	,cancel =
		case
		when Encounter.AppointmentStatusCode = 'CND' then 'cancel'
		else null
		end

	--,specialty_code = NationalSpecialtyCode

	,specialty_code = SpecialtyMap.EnvoySpecialtyCode

	,clinic_code = -- append 'ZN' or 'ZNU' if those are the appointment types (C&B)
		case Encounter.AppointmentTypeCode
		when 'I' then 'AGENT'
		when 'D' then 'AGENTDC'
		end

	,Division = 'Surgical'

	,appt_audit_ref =
		coalesce(ltrim(rtrim(Consultant.Initials)) , '') + coalesce(left(ltrim(Consultant.Surname) , 1) , '')

	,appt_type = null
	,location = null
	,practitioner = Encounter.ConsultantCode
	,procedure_code = null
	,patient_email = null
	,genericinfo1 = coalesce(left(Encounter.Forenames , 1) , '') + coalesce(left(Encounter.Surname , 1) , '')
	,genericinfo2 = null
	,genericinfo3 = Encounter.Postcode

FROM
	ETL.TImportOPImminentSMS Encounter

inner join Envoy.SpecialtyMap
on	SpecialtyMap.PASSpecialtyCode = Encounter.SpecialtyCode
and SpecialtyMap.FileType = 'Agent'		/* Added Paul Egan 26/03/2014 */

left join PAS.Ward
on	Ward.WardCode = Encounter.WardCode

left join PAS.Consultant
on	Consultant.ConsultantCode = Encounter.ConsultantCode

where
	Encounter.DateOfDeath is null
and	Encounter.EncounterTypeCode = 'APC'
and	(
		Encounter.AppointmentDate =
			(
			select
				max(AppointmentDate)
			from
				ETL.TImportOPImminentSMS
			where
				TImportOPImminentSMS.DateOfDeath is null
			and	TImportOPImminentSMS.EncounterTypeCode = 'APC'
			and	coalesce(TImportOPImminentSMS.AppointmentStatusCode , '') != 'CND'
			)
	or	Encounter.AppointmentStatusCode = 'CND'
	)

--snagging 1 : exclude Paediatric Admissions
and	datediff(year , DateOfBirth , getdate()) >= 16
and	coalesce(Ward.SiteCode , '') != 'RMCH'

--snagging 5 : exclude SUB WardCodes
and	left(Encounter.WardCode , 3) not in
	(
	 'SUBA'
	,'SUBB'
	,'SUBC'
	,'SUBD'
	,'SUBE'
	,'SUBG'
	,'SUBH'
	,'SUBM'
	,'SUBP'
	)
and Encounter.ClinicCode not in 
	(
	'BIO',		-- Biofeedback clinic exclusion requested by Suzanne Ryder 25/09/2014 Paul Egan
	'RI',		-- Biofeedback clinic exclusion requested by Suzanne Ryder 25/09/2014 Paul Egan
	'PTNS',		-- Biofeedback clinic exclusion requested by Suzanne Ryder 25/09/2014 Paul Egan
	'SUE-PFSN'	-- Biofeedback clinic exclusion requested by Suzanne Ryder 25/09/2014 Paul Egan
	)



select @RowsInserted = @@rowcount


SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 'Time Elapsed ' + CONVERT(char(3), @Elapsed) + ' Mins'

EXEC WriteAuditLogEvent 'Envoy.BuildAgentInputFile', @Stats, @StartTime

print @Stats




﻿



CREATE view [Envoy].[InputFile] as

/****************************************************************************************
	View : [Warehouse].[Envoy].[InputFile]
	Description	: View used in SSIS package $/DataWarehouse/SSIS/SSIS/Envoy Export Input File.dtsx
	
	Documentation including a complete data flow diagram can be found in folder:
	\\warehousefs\PublicInformation\Data_Warehouse\Documentation\Warehouse - Load Envoy\

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
	27/03/2014	Paul Egan		Added union for table Warehouse.Envoy.ImminentSMSByPASSpecialty
	12/05/2014	Paul Egan		Replace hyphen with space in patient name fields
*****************************************************************************************/

select
	 patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name = replace(first_name, '-', ' ')		/* Hyphen removal requested by Healthcare Comms. Paul Egan 12/05/2014 */
	,last_name = replace(last_name, '-', ' ')		/* Hyphen removal requested by Healthcare Comms. Paul Egan 12/05/2014 */
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,Division
	,appt_audit_ref
	,appt_type
	,location
	,practitioner
	,procedure_code
	,patient_email
	,genericinfo1
	,genericinfo2
	,genericinfo3
from
	Envoy.AgentInputFile

union all

select
	 patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name = replace(first_name, '-', ' ')		/* Hyphen removal requested by Healthcare Comms. Paul Egan 12/05/2014 */
	,last_name = replace(last_name, '-', ' ')		/* Hyphen removal requested by Healthcare Comms. Paul Egan 12/05/2014 */
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,Division
	,appt_audit_ref = null
	,appt_type = null
	,location = null
	,practitioner = null
	,procedure_code = null
	,patient_email = null
	,genericinfo1 = null
	,genericinfo2 = null
	,genericinfo3 = Postcode
from
	OP.ImminentSMSInputFile



/*	Added Paul Egan 27/03/2014
	This enables appointment reminders by PAS specialty (all PAS clinic codes under this specialty).
	This is so that new clinic codes under these PAS specialties don't have to be maintained 
	by the data warehouse team - appointment reminders will be sent automatically. Only clinic
	codes to be excluded will have to be maintained by the data warehouse team.
*/
union

select
	patient_id
	,appointment_date
	,appointment_time
	,mobile_number
	,landline_number
	,country_code
	,contact_tel_no
	,first_name = replace(first_name, '-', ' ')		/* Hyphen removal requested by Healthcare Comms. Paul Egan 12/05/2014 */
	,last_name = replace(last_name, '-', ' ')		/* Hyphen removal requested by Healthcare Comms. Paul Egan 12/05/2014 */
	,dob
	,cancel
	,specialty_code
	,clinic_code
	,Division
	,appt_audit_ref
	,appt_type
	,location
	,practitioner
	,procedure_code
	,patient_email
	,genericinfo1
	,genericinfo2
	,genericinfo3
from
	Envoy.ImminentSMSByPASSpecialty









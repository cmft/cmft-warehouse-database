﻿CREATE TABLE [Envoy].[SpecialtyMap] (
    [PASSpecialtyCode]   VARCHAR (4)  NOT NULL,
    [EnvoySpecialtyCode] INT          NULL,
    [FileType]           VARCHAR (20) NULL,
    [Created]            DATETIME     NULL,
    [CreatedBy]          VARCHAR (50) NULL,
    [Updated]            DATETIME     NULL,
    [UpdatedBy]          VARCHAR (50) NULL
);


﻿CREATE TABLE [Planning].[Model] (
    [CensusDate]           DATE             NULL,
    [Region]               VARCHAR (255)    NOT NULL,
    [Purchaser]            VARCHAR (255)    NOT NULL,
    [Tariff]               VARCHAR (255)    NOT NULL,
    [Site]                 VARCHAR (255)    NOT NULL,
    [Category]             VARCHAR (255)    NOT NULL,
    [Specialty]            VARCHAR (255)    NOT NULL,
    [Hrg]                  VARCHAR (255)    NOT NULL,
    [Age]                  VARCHAR (255)    NOT NULL,
    [Los]                  VARCHAR (255)    NOT NULL,
    [Scenario]             VARCHAR (255)    NOT NULL,
    [Step]                 VARCHAR (255)    NOT NULL,
    [StepSeqno]            TINYINT          NOT NULL,
    [Period]               VARCHAR (255)    NOT NULL,
    [Cases]                DECIMAL (9, 4)   NOT NULL,
    [BedDays]              DECIMAL (9, 4)   NOT NULL,
    [BedsAt90PctOccupancy] DECIMAL (28, 10) NULL,
    [Price]                DECIMAL (17, 4)  NOT NULL,
    [TheatreMins]          DECIMAL (20, 4)  NULL,
    [OperationDuration]    DECIMAL (20, 4)  NULL,
    [AnaestheticDuration]  DECIMAL (20, 4)  NULL
);


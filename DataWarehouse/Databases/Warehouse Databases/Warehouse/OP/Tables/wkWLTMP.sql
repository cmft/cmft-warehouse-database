﻿CREATE TABLE [OP].[wkWLTMP] (
    [SourceEncounterNo]          VARCHAR (50)  NOT NULL,
    [SourcePatientNo]            VARCHAR (20)  NOT NULL,
    [WithRTTOpenPathway]         INT           NOT NULL,
    [RTTPathwayStartDateCurrent] DATETIME2 (7) NULL,
    [RTTWeekBandReturnCode]      VARCHAR (2)   NULL,
    [RTTDaysWaiting]             INT           NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_wkwltmp]
    ON [OP].[wkWLTMP]([SourceEncounterNo] ASC, [SourcePatientNo] ASC)
    INCLUDE([WithRTTOpenPathway], [RTTPathwayStartDateCurrent], [RTTWeekBandReturnCode], [RTTDaysWaiting]);


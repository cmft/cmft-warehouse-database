﻿CREATE TABLE [OP].[WHDivisionMap] (
    [SiteCode]                  VARCHAR (50) NOT NULL,
    [SpecialtyCode]             VARCHAR (50) NOT NULL,
    [IntermediateSpecialtyCode] VARCHAR (50) NULL,
    [NationalSpecialtyCode]     VARCHAR (50) NULL,
    [ConsultantCode]            VARCHAR (10) NULL,
    [DirectorateCode]           VARCHAR (50) NULL,
    [Directorate]               VARCHAR (50) NULL,
    [DivisionCode]              VARCHAR (50) NULL,
    [Division]                  VARCHAR (50) NULL,
    [Specialty]                 VARCHAR (50) NULL,
    [Diagnostic]                VARCHAR (50) NULL,
    [FromDate]                  DATE         NOT NULL,
    [ToDate]                    DATE         NULL
);


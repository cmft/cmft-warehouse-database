﻿CREATE TABLE [OP].[WkRTTPathway] (
    [InternalNo]                  NVARCHAR (9)  NULL,
    [doh_spec]                    NVARCHAR (50) NULL,
    [WL_EpisodeNo]                NVARCHAR (13) NULL,
    [treattype]                   NVARCHAR (6)  NULL,
    [type]                        NVARCHAR (10) NULL,
    [pathway_start_date_current]  DATETIME2 (7) NULL,
    [path_open_days_DNA_adjs]     INT           NULL,
    [pathway_start_date_original] DATETIME2 (7) NULL,
    [pathway_end_date]            DATETIME2 (7) NULL,
    [path_closed_days_DNA_adjs]   INT           NULL,
    [datebefore]                  DATETIME      NULL
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20140723-110744]
    ON [OP].[WkRTTPathway]([InternalNo] ASC, [WL_EpisodeNo] ASC)
    INCLUDE([doh_spec], [type], [pathway_start_date_current], [path_open_days_DNA_adjs], [pathway_end_date], [path_closed_days_DNA_adjs], [datebefore]);


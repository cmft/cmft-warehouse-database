﻿CREATE TABLE [OP].[ImminentSMSInputFile] (
    [patient_id]       VARCHAR (9)  NOT NULL,
    [appointment_date] VARCHAR (10) NULL,
    [appointment_time] VARCHAR (10) NULL,
    [mobile_number]    VARCHAR (23) NULL,
    [landline_number]  VARCHAR (23) NULL,
    [country_code]     VARCHAR (2)  NOT NULL,
    [contact_tel_no]   VARCHAR (11) NULL,
    [first_name]       VARCHAR (20) NULL,
    [last_name]        VARCHAR (24) NULL,
    [dob]              VARCHAR (10) NULL,
    [cancel]           VARCHAR (6)  NULL,
    [specialty_code]   VARCHAR (50) NOT NULL,
    [clinic_code]      VARCHAR (12) NULL,
    [Division]         VARCHAR (50) NULL,
    [Postcode]         VARCHAR (10) NULL
);


﻿CREATE TABLE [OP].[ProcessListArchive] (
    [EncounterRecno] INT           NOT NULL,
    [Action]         NVARCHAR (10) NULL,
    [ArchiveTime]    DATETIME      NOT NULL,
    CONSTRAINT [PK_ProcessListArchive] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [ArchiveTime] ASC)
);


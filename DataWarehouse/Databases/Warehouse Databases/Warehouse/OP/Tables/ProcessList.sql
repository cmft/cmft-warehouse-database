﻿CREATE TABLE [OP].[ProcessList] (
    [EncounterRecno] INT           NOT NULL,
    [Action]         NVARCHAR (10) NULL,
    CONSTRAINT [PK_ProcessList] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);


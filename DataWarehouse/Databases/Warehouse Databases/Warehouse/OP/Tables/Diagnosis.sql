﻿CREATE TABLE [OP].[Diagnosis] (
    [SourceUniqueID]    VARCHAR (50) NOT NULL,
    [SourcePatientNo]   VARCHAR (20) NULL,
    [SourceEncounterNo] VARCHAR (20) NULL,
    [SequenceNo]        SMALLINT     NULL,
    [DiagnosisCode]     VARCHAR (10) NULL,
    [OPSourceUniqueID]  VARCHAR (50) NULL,
    [Created]           DATETIME     NOT NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_OPDiagnosis_1] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


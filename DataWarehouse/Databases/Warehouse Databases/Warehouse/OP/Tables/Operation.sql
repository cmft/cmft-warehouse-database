﻿CREATE TABLE [OP].[Operation] (
    [SourceUniqueID]    VARCHAR (50)  NOT NULL,
    [SourcePatientNo]   VARCHAR (20)  NULL,
    [SourceEncounterNo] VARCHAR (20)  NULL,
    [SequenceNo]        SMALLINT      NULL,
    [OperationCode]     VARCHAR (10)  NULL,
    [OperationDate]     SMALLDATETIME NULL,
    [DoctorCode]        VARCHAR (10)  NULL,
    [OPSourceUniqueID]  VARCHAR (50)  NULL,
    [SourceSequenceNo]  SMALLINT      NULL,
    [Created]           DATETIME      NOT NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Operation_2] PRIMARY KEY NONCLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_Operation_2]
    ON [OP].[Operation]([SourcePatientNo] ASC, [SourceEncounterNo] ASC, [OperationDate] ASC, [SequenceNo] ASC, [DoctorCode] ASC);


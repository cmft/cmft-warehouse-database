﻿CREATE TABLE [OP].[AppointmentStatusMap] (
    [AppointmentStatusCode] VARCHAR (5)   NULL,
    [AppointmentStatus]     VARCHAR (100) NOT NULL,
    [AttendanceOutcomeCode] VARCHAR (5)   NULL,
    [AttendanceOutcome]     VARCHAR (100) NOT NULL,
    [DNAReasonCode]         CHAR (1)      NULL,
    [DNAReason]             VARCHAR (100) NOT NULL
);


﻿CREATE TABLE [OP].[ClinicSessionPeriod] (
    [ClinicSessionPeriodCode] VARCHAR (50)  NOT NULL,
    [ClinicSessionPeriod]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_ClinicSessionPeriod] PRIMARY KEY CLUSTERED ([ClinicSessionPeriodCode] ASC)
);


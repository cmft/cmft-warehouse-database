﻿CREATE TABLE [OP].[Encounter] (
    [EncounterRecno]                INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]                VARCHAR (50)  NOT NULL,
    [SourcePatientNo]               VARCHAR (20)  NOT NULL,
    [SourceEncounterNo]             VARCHAR (20)  NOT NULL,
    [PatientTitle]                  VARCHAR (10)  NULL,
    [PatientForename]               VARCHAR (20)  NULL,
    [PatientSurname]                VARCHAR (30)  NULL,
    [DateOfBirth]                   DATETIME      NULL,
    [DateOfDeath]                   SMALLDATETIME NULL,
    [SexCode]                       VARCHAR (1)   NULL,
    [NHSNumber]                     VARCHAR (17)  NULL,
    [DistrictNo]                    VARCHAR (12)  NULL,
    [Postcode]                      VARCHAR (8)   NULL,
    [PatientAddress1]               VARCHAR (25)  NULL,
    [PatientAddress2]               VARCHAR (25)  NULL,
    [PatientAddress3]               VARCHAR (25)  NULL,
    [PatientAddress4]               VARCHAR (25)  NULL,
    [DHACode]                       VARCHAR (3)   NULL,
    [EthnicOriginCode]              VARCHAR (4)   NULL,
    [MaritalStatusCode]             VARCHAR (1)   NULL,
    [ReligionCode]                  VARCHAR (4)   NULL,
    [RegisteredGpCode]              VARCHAR (8)   NULL,
    [RegisteredGpPracticeCode]      VARCHAR (8)   NULL,
    [SiteCode]                      VARCHAR (5)   NULL,
    [AppointmentDate]               SMALLDATETIME NULL,
    [AppointmentTime]               SMALLDATETIME NULL,
    [ClinicCode]                    VARCHAR (8)   NULL,
    [AdminCategoryCode]             VARCHAR (3)   NULL,
    [SourceOfReferralCode]          VARCHAR (5)   NULL,
    [ReasonForReferralCode]         VARCHAR (5)   NULL,
    [PriorityCode]                  VARCHAR (5)   NULL,
    [FirstAttendanceFlag]           VARCHAR (1)   NULL,
    [DNACode]                       CHAR (1)      NULL,
    [AppointmentStatusCode]         VARCHAR (10)  NULL,
    [CancelledByCode]               VARCHAR (5)   NULL,
    [TransportRequiredFlag]         VARCHAR (3)   NULL,
    [AttendanceOutcomeCode]         VARCHAR (5)   NULL,
    [AppointmentTypeCode]           VARCHAR (5)   NULL,
    [DisposalCode]                  VARCHAR (8)   NULL,
    [ConsultantCode]                VARCHAR (10)  NULL,
    [SpecialtyCode]                 VARCHAR (5)   NULL,
    [ReferringConsultantCode]       VARCHAR (10)  NULL,
    [ReferringSpecialtyCode]        VARCHAR (8)   NULL,
    [BookingTypeCode]               VARCHAR (3)   NULL,
    [CasenoteNo]                    VARCHAR (16)  NULL,
    [AppointmentCreateDate]         SMALLDATETIME NULL,
    [EpisodicGpCode]                VARCHAR (8)   NULL,
    [EpisodicGpPracticeCode]        VARCHAR (6)   NULL,
    [DoctorCode]                    VARCHAR (20)  NULL,
    [PrimaryDiagnosisCode]          VARCHAR (10)  NULL,
    [SubsidiaryDiagnosisCode]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode1]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode2]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode3]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode4]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode5]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode6]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode7]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode8]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode9]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode10]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode11]      VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode12]      VARCHAR (10)  NULL,
    [PrimaryOperationCode]          VARCHAR (10)  NULL,
    [PrimaryOperationDate]          SMALLDATETIME NULL,
    [SecondaryOperationCode1]       VARCHAR (10)  NULL,
    [SecondaryOperationDate1]       SMALLDATETIME NULL,
    [SecondaryOperationCode2]       VARCHAR (10)  NULL,
    [SecondaryOperationDate2]       SMALLDATETIME NULL,
    [SecondaryOperationCode3]       VARCHAR (10)  NULL,
    [SecondaryOperationDate3]       SMALLDATETIME NULL,
    [SecondaryOperationCode4]       VARCHAR (10)  NULL,
    [SecondaryOperationDate4]       SMALLDATETIME NULL,
    [SecondaryOperationCode5]       VARCHAR (10)  NULL,
    [SecondaryOperationDate5]       SMALLDATETIME NULL,
    [SecondaryOperationCode6]       VARCHAR (10)  NULL,
    [SecondaryOperationDate6]       SMALLDATETIME NULL,
    [SecondaryOperationCode7]       VARCHAR (10)  NULL,
    [SecondaryOperationDate7]       SMALLDATETIME NULL,
    [SecondaryOperationCode8]       VARCHAR (10)  NULL,
    [SecondaryOperationDate8]       SMALLDATETIME NULL,
    [SecondaryOperationCode9]       VARCHAR (10)  NULL,
    [SecondaryOperationDate9]       SMALLDATETIME NULL,
    [SecondaryOperationCode10]      VARCHAR (10)  NULL,
    [SecondaryOperationDate10]      SMALLDATETIME NULL,
    [SecondaryOperationCode11]      VARCHAR (10)  NULL,
    [SecondaryOperationDate11]      SMALLDATETIME NULL,
    [PurchaserCode]                 VARCHAR (10)  NULL,
    [ProviderCode]                  VARCHAR (5)   NULL,
    [ContractSerialNo]              VARCHAR (6)   NULL,
    [ReferralDate]                  SMALLDATETIME NULL,
    [RTTPathwayID]                  VARCHAR (25)  NULL,
    [RTTPathwayCondition]           VARCHAR (20)  NULL,
    [RTTStartDate]                  SMALLDATETIME NULL,
    [RTTEndDate]                    SMALLDATETIME NULL,
    [RTTSpecialtyCode]              VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]        VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]          VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]          SMALLDATETIME NULL,
    [RTTCurrentPrivatePatientFlag]  BIT           NULL,
    [RTTOverseasStatusFlag]         BIT           NULL,
    [RTTPeriodStatusCode]           VARCHAR (10)  NULL,
    [AppointmentCategoryCode]       VARCHAR (10)  NULL,
    [AppointmentCreatedBy]          VARCHAR (3)   NULL,
    [AppointmentCancelDate]         SMALLDATETIME NULL,
    [LastRevisedDate]               SMALLDATETIME NULL,
    [LastRevisedBy]                 VARCHAR (3)   NULL,
    [OverseasStatusFlag]            VARCHAR (1)   NULL,
    [PatientChoiceCode]             VARCHAR (3)   NULL,
    [ScheduledCancelReasonCode]     VARCHAR (4)   NULL,
    [PatientCancelReason]           VARCHAR (50)  NULL,
    [DischargeDate]                 SMALLDATETIME NULL,
    [QM08StartWaitDate]             SMALLDATETIME NULL,
    [QM08EndWaitDate]               SMALLDATETIME NULL,
    [DestinationSiteCode]           VARCHAR (10)  NULL,
    [EBookingReferenceNo]           VARCHAR (50)  NULL,
    [InterfaceCode]                 VARCHAR (5)   NULL,
    [Created]                       DATETIME      NULL,
    [Updated]                       DATETIME      NULL,
    [ByWhom]                        VARCHAR (50)  NULL,
    [LocalAdminCategoryCode]        VARCHAR (10)  NULL,
    [PCTCode]                       VARCHAR (10)  NULL,
    [LocalityCode]                  VARCHAR (10)  NULL,
    [IsWardAttender]                BIT           NULL,
    [ClockStartDate]                SMALLDATETIME NULL,
    [RTTBreachDate]                 SMALLDATETIME NULL,
    [HomePhone]                     VARCHAR (50)  NULL,
    [WorkPhone]                     VARCHAR (50)  NULL,
    [SourceOfReferralGroupCode]     VARCHAR (10)  NULL,
    [WardCode]                      VARCHAR (10)  NULL,
    [DirectorateCode]               VARCHAR (5)   NULL,
    [ReferringSpecialtyTypeCode]    VARCHAR (1)   NULL,
    [ReferredByCode]                VARCHAR (10)  NULL,
    [ReferrerCode]                  VARCHAR (10)  NULL,
    [LastDNAorPatientCancelledDate] SMALLDATETIME NULL,
    [StaffGroupCode]                VARCHAR (10)  NULL,
    [DerivedFirstAttendanceFlag]    VARCHAR (1)   NULL,
    [InterpreterIndicator]          VARCHAR (3)   NULL,
    [EpisodicSiteCode]              VARCHAR (5)   NULL,
    [PseudoPostcode]                VARCHAR (10)  NULL,
    [CCGCode]                       VARCHAR (10)  NULL,
    [PostcodeAtAppointment]         VARCHAR (8)   NULL,
    [GpPracticeCodeAtAppointment]   VARCHAR (6)   NULL,
    [GpCodeAtAppointment]           VARCHAR (8)   NULL,
    [EpisodicPostcode]              VARCHAR (8)   NULL,
    [EncounterChecksum]             INT           NULL,
    CONSTRAINT [PK_Encounter_1] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC) ON [OP],
    CONSTRAINT [IX_Encounter_2] UNIQUE NONCLUSTERED ([EncounterRecno] ASC) ON [PRIMARY]
);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_3]
    ON [OP].[Encounter]([SourcePatientNo] ASC, [SourceEncounterNo] ASC, [DNACode] ASC, [FirstAttendanceFlag] ASC)
    ON [OP];


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_5]
    ON [OP].[Encounter]([SourcePatientNo] ASC, [SourceEncounterNo] ASC, [AttendanceOutcomeCode] ASC, [AppointmentTime] ASC)
    ON [OP];


GO
CREATE NONCLUSTERED INDEX [Index_ClinicCodeIncDistrictNo]
    ON [OP].[Encounter]([ClinicCode] ASC)
    INCLUDE([DistrictNo])
    ON [OP];


GO
CREATE NONCLUSTERED INDEX [Index_DistrictNoClinicCode]
    ON [OP].[Encounter]([DistrictNo] ASC, [ClinicCode] ASC)
    ON [PRIMARY];


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_6]
    ON [OP].[Encounter]([AppointmentDate] ASC)
    INCLUDE([AppointmentCreateDate], [AppointmentTime], [DateOfBirth], [DistrictNo], [PatientForename], [PatientSurname], [SourceOfReferralCode], [CasenoteNo], [ClinicCode], [DerivedFirstAttendanceFlag], [DNACode], [DoctorCode], [SourceEncounterNo], [SourcePatientNo], [WardCode]) WITH (FILLFACTOR = 100)
    ON [OP];


GO
CREATE NONCLUSTERED INDEX [IX_OPEncounter_SourcePatientNo_AppointmentTime]
    ON [OP].[Encounter]([SourcePatientNo] ASC, [AppointmentTime] ASC)
    ON [PRIMARY];


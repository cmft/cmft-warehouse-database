﻿CREATE TABLE [Result].[GPICEOrderResult] (
    [OrderResultRecno]     INT          IDENTITY (1, 1) NOT NULL,
    [OrderSourceUniqueID]  INT          NOT NULL,
    [OrderRequestTime]     DATETIME     NULL,
    [ResultSourceUniqueID] INT          NOT NULL,
    [InterfaceCode]        VARCHAR (10) NOT NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL,
    CONSTRAINT [PK__GPICEOrd__D1D39A5D61A9010F] PRIMARY KEY CLUSTERED ([OrderSourceUniqueID] ASC, [ResultSourceUniqueID] ASC),
    CONSTRAINT [GPICEOrderResult_OrderResultRecno] UNIQUE NONCLUSTERED ([OrderResultRecno] ASC)
);


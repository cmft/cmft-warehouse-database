﻿CREATE TABLE [Result].[CENICEProviderBase] (
    [ProviderID]           INT          NOT NULL,
    [ProviderCode]         VARCHAR (8)  NULL,
    [Provider]             VARCHAR (65) NULL,
    [ProviderNationalCode] VARCHAR (8)  NULL,
    [DisciplineID]         INT          NULL,
    [OrganisationCode]     VARCHAR (6)  NOT NULL,
    CONSTRAINT [PK__CENICEPr__B54C689D3520735B] PRIMARY KEY CLUSTERED ([ProviderID] ASC)
);


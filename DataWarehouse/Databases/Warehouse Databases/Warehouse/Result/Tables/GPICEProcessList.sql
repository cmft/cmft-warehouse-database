﻿CREATE TABLE [Result].[GPICEProcessList] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    CONSTRAINT [PK_GPICEProcessList] PRIMARY KEY CLUSTERED ([ResultRecno] ASC)
);


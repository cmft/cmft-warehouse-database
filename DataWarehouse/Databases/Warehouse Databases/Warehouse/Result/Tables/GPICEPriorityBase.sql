﻿CREATE TABLE [Result].[GPICEPriorityBase] (
    [PriorityID] INT          NOT NULL,
    [Priority]   VARCHAR (20) NOT NULL,
    CONSTRAINT [PK__GPICEPri__D0A3D0DE41F05E94] PRIMARY KEY CLUSTERED ([PriorityID] ASC)
);


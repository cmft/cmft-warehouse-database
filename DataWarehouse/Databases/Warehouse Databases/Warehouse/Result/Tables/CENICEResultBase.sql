﻿CREATE TABLE [Result].[CENICEResultBase] (
    [ResultCode]    VARCHAR (10)  NOT NULL,
    [ResultName]    VARCHAR (100) NOT NULL,
    [SpecialtyCode] VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK__CENICERe__92F1EAF61D33DB31] PRIMARY KEY CLUSTERED ([ResultCode] ASC, [ResultName] ASC, [SpecialtyCode] ASC)
);


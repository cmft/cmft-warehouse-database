﻿CREATE TABLE [Result].[CWSNumericResult] (
    [ResultRecno]              INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]           VARCHAR (50)  NOT NULL,
    [SourcePatientNo]          INT           NULL,
    [SourceEpisodeNo]          INT           NULL,
    [TransactionNo]            INT           NOT NULL,
    [SessionNo]                INT           NULL,
    [OrderSourceUniqueID]      VARCHAR (50)  NULL,
    [ConsultantCode]           VARCHAR (10)  NULL,
    [SpecialtyCode]            VARCHAR (8)   NULL,
    [OrderPriorityCode]        VARCHAR (20)  NULL,
    [OrderStatusCode]          TINYINT       NULL,
    [OrderEnteredByCode]       VARCHAR (4)   NULL,
    [OrderedByCode]            VARCHAR (20)  NULL,
    [OrderTime]                VARCHAR (39)  NULL,
    [OrderComment]             VARCHAR (MAX) NULL,
    [SampleReferenceCode]      VARCHAR (50)  NULL,
    [EffectiveTime]            DATETIME      NULL,
    [LocationCode]             VARCHAR (4)   NULL,
    [ServiceCode]              VARCHAR (20)  NULL,
    [ResultCode]               VARCHAR (4)   NOT NULL,
    [Result]                   VARCHAR (9)   NULL,
    [UnitOfMeasurement]        VARCHAR (8)   NULL,
    [LowerReferenceRangeValue] FLOAT (53)    NULL,
    [UpperReferenceRangeValue] FLOAT (53)    NULL,
    [ResultTime]               DATETIME      NULL,
    [RangeIndicator]           SMALLINT      NULL,
    [ResultEnteredByCode]      VARCHAR (4)   NULL,
    [ResultStatusCode]         SMALLINT      NULL,
    [ResultComment]            VARCHAR (MAX) NULL,
    [InterfaceCode]            VARCHAR (3)   NOT NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  NULL,
    [ResultChecksum]           INT           NULL,
    CONSTRAINT [PK__CWSNumer__613D576F00CCA6AD] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC),
    CONSTRAINT [IX_CWSNumericResult_ResultRecno] UNIQUE NONCLUSTERED ([ResultRecno] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_CWSNumericResult_SourcePatientNo]
    ON [Result].[CWSNumericResult]([SourcePatientNo] ASC)
    INCLUDE([RangeIndicator], [Result], [ResultCode], [ResultComment], [ResultTime]);


GO
CREATE NONCLUSTERED INDEX [IX_CWSNumericResult_SourcePatientNo_SourceEpisodeNo]
    ON [Result].[CWSNumericResult]([SourcePatientNo] ASC, [SourceEpisodeNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CWSNumericResult_ServiceCode]
    ON [Result].[CWSNumericResult]([ServiceCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CWSNumericResult_ResultCode]
    ON [Result].[CWSNumericResult]([ResultCode] ASC);


﻿CREATE TABLE [Result].[GPICEProviderBase] (
    [ProviderID]           INT          NOT NULL,
    [ProviderCode]         VARCHAR (8)  NULL,
    [Provider]             VARCHAR (65) NULL,
    [ProviderNationalCode] VARCHAR (8)  NULL,
    [DisciplineID]         INT          NULL,
    [OrganisationCode]     VARCHAR (6)  NOT NULL,
    CONSTRAINT [PK__GPICEPro__B54C689D2EDD8A20] PRIMARY KEY CLUSTERED ([ProviderID] ASC)
);


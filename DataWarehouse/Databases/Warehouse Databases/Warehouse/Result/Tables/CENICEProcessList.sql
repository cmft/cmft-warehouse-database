﻿CREATE TABLE [Result].[CENICEProcessList] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    CONSTRAINT [PK_CENICEProcessList] PRIMARY KEY CLUSTERED ([ResultRecno] ASC)
);


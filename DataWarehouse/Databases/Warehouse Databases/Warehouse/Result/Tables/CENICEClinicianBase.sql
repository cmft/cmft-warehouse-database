﻿CREATE TABLE [Result].[CENICEClinicianBase] (
    [ClinicianID]           INT          NOT NULL,
    [NationalCode]          VARCHAR (8)  NOT NULL,
    [Surname]               VARCHAR (35) NULL,
    [Forename]              VARCHAR (35) NULL,
    [Title]                 VARCHAR (20) NULL,
    [Active]                BIT          NOT NULL,
    [OrganisationCode]      VARCHAR (6)  NULL,
    [NationalSpecialtyCode] SMALLINT     NULL,
    [GP]                    BIT          NOT NULL,
    [Created]               DATETIME     NULL,
    CONSTRAINT [PK__CENICECl__1EAC2ED025DE2FCB] PRIMARY KEY CLUSTERED ([ClinicianID] ASC)
);


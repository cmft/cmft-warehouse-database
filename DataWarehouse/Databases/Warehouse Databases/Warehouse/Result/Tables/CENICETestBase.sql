﻿CREATE TABLE [Result].[CENICETestBase] (
    [TestID]   INT          NOT NULL,
    [TestCode] VARCHAR (30) NOT NULL,
    [Test]     VARCHAR (40) NULL,
    CONSTRAINT [PK__CENICETe__8CC33100483347CF] PRIMARY KEY CLUSTERED ([TestID] ASC)
);


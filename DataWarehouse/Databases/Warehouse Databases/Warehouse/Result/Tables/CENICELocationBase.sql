﻿CREATE TABLE [Result].[CENICELocationBase] (
    [LocationID] INT          NOT NULL,
    [Location]   VARCHAR (30) NULL,
    [Active]     BIT          NOT NULL,
    CONSTRAINT [PK__CENICELo__E7FEA47741713BA7] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


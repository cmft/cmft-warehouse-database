﻿CREATE TABLE [Result].[CWSServiceCodeCRISExamCode](
	[CWSServiceCode] [float] NULL,
	[CRISExamCode] [nvarchar](255) NULL
) ON [PRIMARY]
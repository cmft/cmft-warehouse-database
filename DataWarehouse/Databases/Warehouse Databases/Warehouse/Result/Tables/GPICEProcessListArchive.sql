﻿CREATE TABLE [Result].[GPICEProcessListArchive] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    [ArchiveTime] DATETIME      NOT NULL,
    CONSTRAINT [PK_GPICEProcessListArchive] PRIMARY KEY CLUSTERED ([ResultRecno] ASC, [ArchiveTime] ASC)
);


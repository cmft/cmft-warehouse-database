﻿CREATE TABLE [Result].[CENICESpecialtyBase] (
    [SpecialtyCode] SMALLINT     NOT NULL,
    [Specialty]     VARCHAR (68) NULL,
    CONSTRAINT [PK__CENICESp__739F527440922607] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


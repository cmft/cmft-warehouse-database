﻿CREATE TABLE [Result].[CWSTextProcessList] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    CONSTRAINT [PK_CWSTextProcessList] PRIMARY KEY CLUSTERED ([ResultRecno] ASC)
);


﻿CREATE TABLE [Result].[CWSTextResult] (
    [ResultRecno]         INT           IDENTITY (-1, -1) NOT NULL,
    [SourcePatientNo]     INT           NULL,
    [SourceEpisodeNo]     INT           NULL,
    [TransactionNo]       INT           NOT NULL,
    [SessionNo]           INT           NOT NULL,
    [OrderSourceUniqueID] VARCHAR (50)  NULL,
    [ConsultantCode]      VARCHAR (10)  NULL,
    [SpecialtyCode]       VARCHAR (8)   NULL,
    [OrderPriorityCode]   VARCHAR (20)  NULL,
    [OrderStatusCode]     TINYINT       NULL,
    [OrderEnteredByCode]  VARCHAR (4)   NULL,
    [OrderedByCode]       VARCHAR (20)  NULL,
    [OrderTime]           DATETIME      NULL,
    [OrderComment]        VARCHAR (MAX) NULL,
    [SampleReferenceCode] VARCHAR (50)  NULL,
    [EffectiveTime]       DATETIME      NULL,
    [LocationCode]        VARCHAR (4)   NULL,
    [ServiceCode]         VARCHAR (20)  NULL,
    [ResultCode]          VARCHAR (4)   NOT NULL,
    [Result]              VARCHAR (MAX) NULL,
    [InterfaceCode]       VARCHAR (3)   NOT NULL,
    [Created]             DATETIME      NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (50)  NULL,
    [ResultChecksum]      INT           NULL,
    CONSTRAINT [PK__CWSTextR__2575D5840803B421] PRIMARY KEY CLUSTERED ([TransactionNo] ASC, [SessionNo] ASC, [ResultCode] ASC),
    CONSTRAINT [CWSTextResult_ResultRecno] UNIQUE NONCLUSTERED ([ResultRecno] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_CWSTextResult_ServiceCode]
    ON [Result].[CWSTextResult]([ServiceCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CWSTextResult_ResultCode]
    ON [Result].[CWSTextResult]([ResultCode] ASC);


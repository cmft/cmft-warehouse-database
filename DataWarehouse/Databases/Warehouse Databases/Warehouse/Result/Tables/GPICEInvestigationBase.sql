﻿CREATE TABLE [Result].[GPICEInvestigationBase] (
    [InvestigationCode] VARCHAR (10) NOT NULL,
    [InvestigationName] VARCHAR (60) NOT NULL,
    [SpecialtyCode]     VARCHAR (10) NOT NULL,
    CONSTRAINT [PK__GPICEInv__E9FF1D92065075D9] PRIMARY KEY CLUSTERED ([InvestigationCode] ASC, [InvestigationName] ASC, [SpecialtyCode] ASC)
);


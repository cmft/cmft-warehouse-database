﻿CREATE TABLE [Result].[CWSNumericProcessListArchive] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    [ArchiveTime] DATETIME      NOT NULL,
    CONSTRAINT [PK_CWSProcessListArchive] PRIMARY KEY CLUSTERED ([ResultRecno] ASC, [ArchiveTime] ASC)
);


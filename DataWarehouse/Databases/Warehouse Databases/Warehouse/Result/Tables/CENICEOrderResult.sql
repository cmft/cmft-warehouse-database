﻿CREATE TABLE [Result].[CENICEOrderResult] (
    [OrderResultRecno]     INT          IDENTITY (1, 1) NOT NULL,
    [OrderSourceUniqueID]  INT          NOT NULL,
    [OrderRequestTime]     DATETIME     NULL,
    [ResultSourceUniqueID] INT          NOT NULL,
    [InterfaceCode]        VARCHAR (10) NOT NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL,
    CONSTRAINT [PK__CENICEOr__D1D39A5D544F05F1] PRIMARY KEY CLUSTERED ([OrderSourceUniqueID] ASC, [ResultSourceUniqueID] ASC),
    CONSTRAINT [CENICEOrderResult_OrderResultRecno] UNIQUE NONCLUSTERED ([OrderResultRecno] ASC)
);


﻿CREATE TABLE [Result].[GPICEResultBase] (
    [ResultCode]    VARCHAR (10)  NOT NULL,
    [ResultName]    VARCHAR (100) NOT NULL,
    [SpecialtyCode] VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK__GPICERes__E9FF1D9224D4FCF9] PRIMARY KEY CLUSTERED ([ResultCode] ASC, [ResultName] ASC, [SpecialtyCode] ASC)
);


﻿CREATE TABLE [Result].[GPICEClinicianBase] (
    [ClinicianID]           INT          NOT NULL,
    [NationalCode]          VARCHAR (8)  NOT NULL,
    [Surname]               VARCHAR (35) NULL,
    [Forename]              VARCHAR (35) NULL,
    [Title]                 VARCHAR (20) NULL,
    [Active]                BIT          NOT NULL,
    [OrganisationCode]      VARCHAR (6)  NULL,
    [NationalSpecialtyCode] SMALLINT     NULL,
    [GP]                    BIT          NOT NULL,
    [Created]               DATETIME     NULL,
    CONSTRAINT [PK__GPICECli__1EAC2ED033A23F3D] PRIMARY KEY CLUSTERED ([ClinicianID] ASC)
);


﻿CREATE TABLE [Result].[CENICEInvestigationBase] (
    [InvestigationCode] VARCHAR (10)  NOT NULL,
    [InvestigationName] VARCHAR (100) NOT NULL,
    [SpecialtyCode]     VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK__CENICEIn__E9FF1D9221046C15] PRIMARY KEY CLUSTERED ([InvestigationCode] ASC, [InvestigationName] ASC, [SpecialtyCode] ASC)
);


﻿CREATE TABLE [Result].[CENICETest] (
    [TestRecno]           INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]      INT          NOT NULL,
    [OrderSourceUniqueID] INT          NOT NULL,
    [OrderRequestTime]    DATETIME     NULL,
    [OrderStatusCode]     VARCHAR (10) NULL,
    [PatientID]           INT          NULL,
    [PatientIdentifier]   VARCHAR (50) NULL,
    [DistrictNo]          VARCHAR (8)  NULL,
    [CasenoteNumber]      VARCHAR (20) NULL,
    [NHSNumber]           VARCHAR (10) NULL,
    [ClinicianID]         INT          NULL,
    [MainSpecialtyCode]   INT          NULL,
    [ProviderID]          INT          NOT NULL,
    [LocationID]          INT          NOT NULL,
    [TestStatus]          VARCHAR (32) NOT NULL,
    [TestID]              INT          NOT NULL,
    [InterfaceCode]       VARCHAR (10) NOT NULL,
    [Created]             DATETIME     NULL,
    [Updated]             DATETIME     NULL,
    [ByWhom]              VARCHAR (50) NULL,
    [TestChecksum]        INT          NULL,
    CONSTRAINT [PK__CENICETe__613D576F52BBC33A] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC),
    CONSTRAINT [CENICETest_TestRecno] UNIQUE NONCLUSTERED ([TestRecno] ASC)
);



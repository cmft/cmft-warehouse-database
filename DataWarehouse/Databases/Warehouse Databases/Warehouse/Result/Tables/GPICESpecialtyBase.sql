﻿CREATE TABLE [Result].[GPICESpecialtyBase] (
    [SpecialtyCode] SMALLINT     NOT NULL,
    [Specialty]     VARCHAR (68) NULL,
    CONSTRAINT [PK__GPICESpe__739F52743866F45A] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


﻿CREATE TABLE [Result].[GPICEOrder] (
    [OrderRecno]          INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]      INT          NOT NULL,
    [PatientID]           INT          NULL,
    [PatientIdentifier]   VARCHAR (50) NULL,
    [DistrictNo]          VARCHAR (8)  NULL,
    [CasenoteNumber]      VARCHAR (20) NULL,
    [NHSNumber]           VARCHAR (10) NULL,
    [ClinicianID]         INT          NULL,
    [MainSpecialtyCode]   INT          NULL,
    [ProviderID]          INT          NOT NULL,
    [LocationID]          INT          NOT NULL,
    [OrderPriority]       VARCHAR (20) NOT NULL,
    [OrderStatusCode]     VARCHAR (3)  NULL,
    [OrderRequestTime]    DATETIME     NULL,
    [OrderReceivedTime]   DATETIME     NULL,
    [OrderCollectionTime] DATETIME     NULL,
    [InterfaceCode]       VARCHAR (10) NOT NULL,
    [Created]             DATETIME     NULL,
    [Updated]             DATETIME     NULL,
    [ByWhom]              VARCHAR (50) NULL,
    [OrderChecksum]       INT          NULL,
    CONSTRAINT [PK__GPICEOrd__613D576F4DF70E1D] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC),
    CONSTRAINT [GPICEOrder_OrderRecno] UNIQUE NONCLUSTERED ([OrderRecno] ASC)
);




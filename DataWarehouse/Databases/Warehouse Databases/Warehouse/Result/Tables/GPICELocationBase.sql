﻿CREATE TABLE [Result].[GPICELocationBase] (
    [LocationID] INT          NOT NULL,
    [Location]   VARCHAR (30) NULL,
    [Active]     BIT          NOT NULL,
    CONSTRAINT [PK__GPICELoc__E7FEA4774541CC8B] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


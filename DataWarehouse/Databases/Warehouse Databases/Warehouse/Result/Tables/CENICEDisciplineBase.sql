﻿CREATE TABLE [Result].[CENICEDisciplineBase] (
    [DisciplineID]          INT          NOT NULL,
    [DisciplineCode]        VARCHAR (5)  NULL,
    [Discipline]            VARCHAR (50) NULL,
    [NationalSpecialtyCode] VARCHAR (5)  NULL,
    CONSTRAINT [PK__CENICEDi__2909375029AEC0AF] PRIMARY KEY CLUSTERED ([DisciplineID] ASC)
);


﻿CREATE TABLE [Result].[CWSNumericProcessList] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    CONSTRAINT [PK_CWSProcessList] PRIMARY KEY CLUSTERED ([ResultRecno] ASC)
);


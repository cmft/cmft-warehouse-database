﻿CREATE TABLE [Result].[CWSTextProcessListArchive] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    [ArchiveTime] DATETIME      NOT NULL,
    CONSTRAINT [PK_CWSTextProcessListArchive] PRIMARY KEY CLUSTERED ([ResultRecno] ASC, [ArchiveTime] ASC)
);


﻿CREATE TABLE [Result].[GPICEDisciplineBase] (
    [DisciplineID]          INT          NOT NULL,
    [DisciplineCode]        VARCHAR (5)  NULL,
    [Discipline]            VARCHAR (50) NULL,
    [NationalSpecialtyCode] VARCHAR (5)  NULL,
    CONSTRAINT [PK__GPICEDis__290937502A18D503] PRIMARY KEY CLUSTERED ([DisciplineID] ASC)
);


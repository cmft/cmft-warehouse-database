﻿CREATE TABLE [Result].[CENICEProcessListArchive] (
    [ResultRecno] INT           NOT NULL,
    [Action]      NVARCHAR (10) NULL,
    [ArchiveTime] DATETIME      NOT NULL,
    CONSTRAINT [PK_CENICEProcessListArchive] PRIMARY KEY CLUSTERED ([ResultRecno] ASC, [ArchiveTime] ASC)
);


﻿


CREATE TABLE [Result].[CENICEOrder](
	[OrderRecno] [int] IDENTITY(1,1) NOT NULL,
	[SourceUniqueID] [int] NOT NULL,
	[PatientID] [int] NULL,
	[PatientIdentifier] [varchar](50) NULL,
	[DistrictNo] [varchar](8) NULL,
	[CasenoteNumber] [varchar](20) NULL,
	[NHSNumber] [varchar](10) NULL,
	[ClinicianID] [int] NULL,
	[MainSpecialtyCode] [int] NULL,
	[ProviderID] [int] NOT NULL,
	[LocationID] [int] NOT NULL,
	[OrderPriority] [varchar](20) NOT NULL,
	[OrderStatusCode] [varchar](3) NULL,
	[OrderRequestTime] [datetime] NULL,
	[OrderReceivedTime] [datetime] NULL,
	[OrderCollectionTime] [datetime] NULL,
	[InterfaceCode] [varchar](10) NOT NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NULL,
	[OrderChecksum] [int] NULL,
 CONSTRAINT [PK__CENICEOr__613D576F49325900] PRIMARY KEY CLUSTERED 
(
	[SourceUniqueID] ASC
),
 CONSTRAINT [CENICEOrder_OrderRecno] UNIQUE NONCLUSTERED 
(
	[OrderRecno] ASC
)
);

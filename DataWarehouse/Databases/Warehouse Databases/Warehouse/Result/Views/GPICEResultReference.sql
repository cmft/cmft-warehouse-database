﻿




CREATE view [Result].[GPICEResultReference] as

select
	ResultCode
	,ResultName
	,SpecialtyCode
	,ReferenceMapResultCode = SpecialtyCode + '||' + ResultCode + '||' + cast(checksum(ResultName) as varchar)
from
	Result.GPICEResultBase Base









﻿






CREATE view [Result].[CENICEInvestigationReference] as

select
	InvestigationCode
	,InvestigationName
	,SpecialtyCode
	,ReferenceMapInvestigationCode = SpecialtyCode + '||' + InvestigationCode  + '||' + cast(checksum(InvestigationName) as varchar)
from
	Result.CENICEInvestigationBase Base








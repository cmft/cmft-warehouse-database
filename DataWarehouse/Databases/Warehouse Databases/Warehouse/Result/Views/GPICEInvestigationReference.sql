﻿





CREATE view [Result].[GPICEInvestigationReference] as

select
	InvestigationCode
	,InvestigationName
	,SpecialtyCode
	,ReferenceMapInvestigationCode = SpecialtyCode + '||' + InvestigationCode + '||' + cast(checksum(InvestigationName) as varchar)
from
	Result.GPICEInvestigationBase Base








﻿





CREATE view [Result].[GPICEClinician] as 
select
	[ClinicianID]
	--,Clinician = ltrim(coalesce(Title,'') + ' ' + coalesce([Forename],'') + ' ' + coalesce(Surname,''))
	,Clinician = 
		[$(CMFT)].String.ProperCase(
			ltrim(
				coalesce(
					Title
				, '') 
				+ ' ' +
				coalesce(
					case
					when charindex(', ', Surname) > 0
					then substring(Surname, charindex(', ', Surname) + 2, len(Surname))
					else Forename
					end
				, '') + ' ' + 
				coalesce(			
					case
					when charindex(', ', Surname) > 0
					then left(Surname, charindex(', ', Surname) - 1)
					else Surname
					end	
				, '')			
			)
		)
	,[NationalCode]
	--,[Surname]
	--,[Forename]
	,[Title]
	,[Active]
	,[OrganisationCode]
	,[NationalSpecialtyCode]
	,[GP]
	,[Created]
from
	[Result].[GPICEClinicianBase]





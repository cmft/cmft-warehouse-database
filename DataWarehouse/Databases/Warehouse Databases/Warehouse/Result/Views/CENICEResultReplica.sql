﻿





CREATE view [Result].[CENICEResultReplica]

as

select --top 1000
	 PatientID = ReportSummary.Patient_Id_Key
	,PatientIdentifier = PatientHospitalNumbers.HospitalNumber
	,SampleCollectionTime = ReportSamples.Collection_DateTime 
	,ResultCode = ReportResults.Result_Code 
	,ResultName = ReportResults.Result_Name 
	,Result = nullif(ReportResults.Result , '')
from 
	[$(ICE_Central)].dbo.ICEView_ReportSummary ReportSummary 

inner join	[$(ICE_Central)].dbo.ICEView_ReportInvestigations ReportInvestigations 
on	ReportInvestigations.Service_Report_Index = ReportSummary.Service_Report_Index

inner join [$(ICE_Central)].dbo.ICEView_ReportSamples ReportSamples 
on	ReportSamples.Sample_Index = ReportInvestigations.Sample_Index 
and ReportSamples.Service_Report_Index = ReportInvestigations.Service_Report_Index

inner join	[$(ICE_Central)].dbo.ICEView_ReportResults ReportResults 
on  ReportResults.Investigation_Index = ReportInvestigations.Investigation_Index 
and ReportResults.Sample_Index = ReportInvestigations.Sample_Index

inner join	[$(ICE_Central)].dbo.ICEView_PatientHospitalNumbers PatientHospitalNumbers 
on  PatientHospitalNumbers.Patient_Id_Key = ReportSummary.Patient_Id_Key
and PatientHospitalNumbers.Main_Identifier = 1

where
	ReportSummary.Report_Status <> 'UN'
and	not exists	
		( 
		select
			1
		from
			[$(ICE_Central)].dbo.ICEView_ReportSummary LaterReport
		where
			LaterReport.Service_Request_Index = ReportSummary.Service_Request_Index
		and LaterReport.Report_ID = ReportSummary.Report_ID
		and	LaterReport.DateTime_Of_Report > ReportSummary.DateTime_Of_Report
		and	LaterReport.Report_Status <> 'UN'
		)




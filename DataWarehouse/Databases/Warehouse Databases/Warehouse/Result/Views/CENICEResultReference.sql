﻿





CREATE view [Result].[CENICEResultReference] as

select
	ResultCode
	,ResultName
	,SpecialtyCode
	,ReferenceMapResultCode = SpecialtyCode + '||' + ResultCode + '||' + cast(checksum(ResultName) as varchar)
from
	Result.CENICEResultBase Base









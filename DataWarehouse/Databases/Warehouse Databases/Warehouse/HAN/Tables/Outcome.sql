﻿CREATE TABLE [HAN].[Outcome] (
    [OutcomeID]    INT          NOT NULL,
    [Outcome]      VARCHAR (60) NOT NULL,
    [Active]       BIT          NOT NULL,
    [DisplayOrder] INT          NULL
);


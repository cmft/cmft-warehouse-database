﻿CREATE TABLE [HAN].[Ward] (
    [WardID]       INT          NOT NULL,
    [Ward]         VARCHAR (50) NOT NULL,
    [PASWardCode]  VARCHAR (4)  NOT NULL,
    [HospitalCode] VARCHAR (4)  NULL
);


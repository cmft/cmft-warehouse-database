﻿CREATE TABLE [HAN].[Situation] (
    [SituationID]       INT          NOT NULL,
    [Situation]         VARCHAR (50) NOT NULL,
    [SituationParentID] INT          NULL,
    [Active]            BIT          NOT NULL,
    [DisplayOrder]      INT          NOT NULL
);


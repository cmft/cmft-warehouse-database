﻿CREATE TABLE [HAN].[ServiceRequest] (
    [EncounterRecno]            INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]            INT           NOT NULL,
    [RaisedBy]                  VARCHAR (50)  NOT NULL,
    [ContactTelephone]          VARCHAR (20)  NULL,
    [ContactBleep]              VARCHAR (20)  NULL,
    [EmailAddress]              VARCHAR (100) NULL,
    [StatusID]                  INT           NOT NULL,
    [CallTypeID]                INT           NOT NULL,
    [SituationID]               INT           NOT NULL,
    [SituationNotes]            VARCHAR (MAX) NULL,
    [SituationHistory]          VARCHAR (MAX) NULL,
    [WorkGroupID]               INT           NULL,
    [PersonID]                  INT           NULL,
    [ActionRequestID]           INT           NULL,
    [UrgencyID]                 INT           NOT NULL,
    [SpecialistTypeID]          INT           NULL,
    [RaisedTime]                DATETIME      NOT NULL,
    [RaisedDate]                DATE          NULL,
    [ClinicalRoleRequestedID]   INT           NULL,
    [ClinicalRoleAllocatedID]   INT           NULL,
    [ClinicalRoleAllocatedTime] DATETIME      NULL,
    [ClinicalRoleAllocatedDate] DATE          NULL,
    [OutcomeID]                 INT           NULL,
    [ClosedByClinicalRoleID]    INT           NULL,
    [ClosedNotes]               VARCHAR (MAX) NULL,
    [CasenoteNumber]            VARCHAR (20)  NULL,
    [NHSNumber]                 VARCHAR (20)  NULL,
    [DistrictNo]                VARCHAR (10)  NULL,
    [InpatientEpisode]          VARCHAR (20)  NULL,
    [PatientName]               VARCHAR (100) NULL,
    [DateOfBirth]               DATE          NULL,
    [SexCode]                   VARCHAR (1)   NULL,
    [PASWardCode]               VARCHAR (50)  NULL,
    [BedManSourceUniqueID]      VARCHAR (50)  NULL,
    CONSTRAINT [PK_ServiceRequest] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


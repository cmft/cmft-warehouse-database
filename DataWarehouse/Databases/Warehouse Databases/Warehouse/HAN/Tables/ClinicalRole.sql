﻿CREATE TABLE [HAN].[ClinicalRole] (
    [ClinicalRoleID]     INT          NOT NULL,
    [ClinicalRole]       VARCHAR (50) NOT NULL,
    [Active]             BIT          NOT NULL,
    [ClinicalCategoryID] INT          NOT NULL,
    [DisplayOrder]       INT          NOT NULL
);


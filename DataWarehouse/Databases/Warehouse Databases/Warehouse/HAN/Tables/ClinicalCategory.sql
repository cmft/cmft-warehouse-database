﻿CREATE TABLE [HAN].[ClinicalCategory] (
    [ClinicalCategoryID] INT          NOT NULL,
    [ClinicalCategory]   VARCHAR (30) NOT NULL,
    [Active]             BIT          NOT NULL,
    [DisplayOrder]       INT          NOT NULL
);


﻿CREATE TABLE [OrderComms].[CareProvider] (
    [SiteID]           SMALLINT       NULL,
    [ReplicationFlags] TINYINT        NULL,
    [ModifiedBy]       VARCHAR (50)   NULL,
    [ModifiedTime]     DATETIME       NULL,
    [CreatedBy]        VARCHAR (50)   NULL,
    [CreatedTime]      DATETIME       NULL,
    [Active]           TINYINT        NULL,
    [SourceUniqueID]   NUMERIC (18)   NOT NULL,
    [ProviderCode]     CHAR (9)       NULL,
    [BillingIDCode]    CHAR (9)       NULL,
    [ProviderTypeCode] VARCHAR (30)   NULL,
    [SignOrderFlag]    TINYINT        NULL,
    [DisplayName]      VARCHAR (50)   NULL,
    [Discipline]       VARCHAR (30)   NULL,
    [TaskRoleType]     VARCHAR (30)   NULL,
    [Build]            INT            NULL,
    [Created]          DATETIME       NOT NULL,
    [ByWhom]           NVARCHAR (128) NULL
);


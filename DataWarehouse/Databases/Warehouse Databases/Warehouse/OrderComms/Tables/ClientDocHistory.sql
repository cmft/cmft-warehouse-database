﻿CREATE TABLE [OrderComms].[ClientDocHistory] (
    [SiteID]               SMALLINT       NULL,
    [ReplicationFlags]     TINYINT        NULL,
    [ModifiedBy]           VARCHAR (50)   NULL,
    [ModifiedTime]         DATETIME       NULL,
    [CreatedBy]            VARCHAR (50)   NULL,
    [CreatedTime]          DATETIME       NULL,
    [Active]               TINYINT        NULL,
    [SourceUniqueID]       NUMERIC (18)   NOT NULL,
    [ClientID]             NUMERIC (18)   NULL,
    [ClientDocumentID]     NUMERIC (18)   NULL,
    [UserID]               NUMERIC (18)   NULL,
    [HistoryTypeCode]      INT            NULL,
    [HistoryTime]          DATETIME       NULL,
    [HistoryReason]        VARCHAR (255)  NULL,
    [CreatedByDisplayName] VARCHAR (80)   NULL,
    [AuthorGroupCounter]   INT            NULL,
    [ArrivalTime]          DATETIME       NULL,
    [Build]                INT            NULL,
    [Created]              DATETIME       NOT NULL,
    [ByWhom]               NVARCHAR (128) NULL
);


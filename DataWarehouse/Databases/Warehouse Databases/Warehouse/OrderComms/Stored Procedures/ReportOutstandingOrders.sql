﻿CREATE procedure [OrderComms].[ReportOutstandingOrders] as

select
	 CensusDate = dateadd(day, datediff(day, 0, getdate()), 0)
	,PatientName = Client.DisplayName
	,DistrictNo = Client.DistrictNo
	,AdmissionTime = ClientVisit.AdmissionTime
	,Location = ClientVisit.CurrentLocation
	,OrderID = Orders.SourceUniqueID
	,OrderEnteredTime = Orders.EnteredTime
	,OrderName = Orders.[Order]
	,OrderStatus = OrderStatus.OrderStatus
	,OrderedBy = Users.DisplayName
from
	OrderComms.[Order] Orders

left join OrderComms.OrderStatusHistory
on	OrderStatusHistory.OrderID = Orders.SourceUniqueID
and	not exists
	(
	select
		1
	from
		OrderComms.OrderStatusHistory Previous
	where
		Previous.OrderID = Orders.SourceUniqueID
	and	(
			Previous.CreatedTime > OrderStatusHistory.CreatedTime
		or	(
				Previous.CreatedTime = OrderStatusHistory.CreatedTime
			and	Previous.SourceUniqueID > OrderStatusHistory.SourceUniqueID
			)
		)
	)

left join OrderComms.OrderStatus
on	OrderStatusHistory.OrderStatusCode = OrderStatus.OrderStatusCode

left join OrderComms.ClientVisit
on	ClientVisit.SourceUniqueID = Orders.ClientVisitID

left join OrderComms.Client
on	Client.SourceUniqueID = Orders.ClientID

left join OrderComms.[User] Users
on	Users.SourceUniqueID = Orders.UserID

where
	OrderStatusHistory.OrderStatusCode != 'RESF'
and	ClientVisit.CurrentLocation != 'Administration (RW6)'
--and	ClientVisit.AdmissionTime between '21 dec 2009' and '21 dec 2009 23:59:59'
and	Orders.EnteredTime between '3 Mar 2010' and '5 Mar 2010 23:59:59.999'
--and	Client.DistrictNo = '03218903'

order by
	 PatientName
	,AdmissionTime
	,OrderEnteredTime
	,OrderName

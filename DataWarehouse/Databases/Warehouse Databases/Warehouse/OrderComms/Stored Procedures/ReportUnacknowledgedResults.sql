﻿create procedure [OrderComms].[ReportUnacknowledgedResults] as

select
	 CensusDate = dateadd(day, datediff(day, 0, getdate()), 0)
	,PatientName = Client.DisplayName
	,DistrictNo = Client.DistrictNo
	,AdmissionTime = ClientVisit.AdmissionTime
	,Location = ClientVisit.CurrentLocation
	,OrderEnteredTime = Orders.EnteredTime
	,OrderName = Orders.[Order]
	,OrderStatus = OrderStatus.OrderStatus
	,OrderedBy = Users.DisplayName
from
	OrderComms.BasicObservation

left join OrderComms.[Order] Orders
on	Orders.SourceUniqueID = BasicObservation.OrderID

left join OrderComms.OrderStatusHistory
on	OrderStatusHistory.OrderID = Orders.SourceUniqueID
and	not exists
	(
	select
		1
	from
		OrderComms.OrderStatusHistory Previous
	where
		Previous.OrderID = Orders.SourceUniqueID
	and	Previous.SourceUniqueID > OrderStatusHistory.SourceUniqueID
	)

left join OrderComms.OrderStatus
on	OrderStatusHistory.OrderStatusCode = OrderStatus.OrderStatusCode

left join OrderComms.ClientVisit
on	ClientVisit.SourceUniqueID = BasicObservation.ClientVisitID

left join OrderComms.Client
on	Client.SourceUniqueID = BasicObservation.ClientID

left join OrderComms.[User] Users
on	Users.SourceUniqueID = Orders.UserID

where
	ClientVisit.CurrentLocation != 'Administration (RW6)'

order by
	 PatientName
	,AdmissionTime

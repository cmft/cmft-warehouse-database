﻿CREATE TABLE [GUM].[AppointmentType] (
    [AppointmentTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [AppointmentType]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Appointm__E258530B128B6435] PRIMARY KEY CLUSTERED ([AppointmentTypeID] ASC)
);


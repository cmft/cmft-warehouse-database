﻿CREATE TABLE [GUM].[Encounter] (
    [EncounterRecno]     INT          IDENTITY (1, 1) NOT NULL,
    [AppointmentDate]    DATE         NULL,
    [AppointmentTime]    DATETIME     NOT NULL,
    [PatientIdentifier]  VARCHAR (50) NOT NULL,
    [Postcode]           VARCHAR (50) NULL,
    [ClinicID]           INT          NULL,
    [AppointmentTypeID]  INT          NULL,
    [AttendanceStatusID] INT          NULL,
    [InterfaceCode]      VARCHAR (10) NULL,
    [Created]            DATETIME     NULL,
    [Updated]            DATETIME     NULL,
    [ByWhom]             VARCHAR (50) NULL,
    CONSTRAINT [PK_OPGUMEncounter] PRIMARY KEY CLUSTERED ([AppointmentTime] ASC, [PatientIdentifier] ASC)
);


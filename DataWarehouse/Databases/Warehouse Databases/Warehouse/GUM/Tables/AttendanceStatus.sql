﻿CREATE TABLE [GUM].[AttendanceStatus] (
    [AttendanceStatusID] INT          IDENTITY (1, 1) NOT NULL,
    [AttendanceStatus]   VARCHAR (50) NULL,
    CONSTRAINT [PK__Attendan__7696A71517501952] PRIMARY KEY CLUSTERED ([AttendanceStatusID] ASC)
);


﻿CREATE TABLE [Maternity].[CTGAssessment] (
    [SessionNumber]                  UNIQUEIDENTIFIER NOT NULL,
    [FetusNumber]                    INT              NULL,
    [CaseNumber]                     UNIQUEIDENTIFIER NULL,
    [NHSNumber]                      NVARCHAR (10)    NULL,
    [HospitalPatientID]              NVARCHAR (50)    NULL,
    [Surname]                        NVARCHAR (256)   NULL,
    [OrderID]                        BIGINT           NULL,
    [CTGStartDate]                   DATE             NULL,
    [CTGStartTime]                   DATETIME         NULL,
    [DateEntered]                    DATE             NULL,
    [TimeEntered]                    DATETIME         NOT NULL,
    [MaternalPulseRecorded]          INT              NOT NULL,
    [DilatationRecorded]             INT              NOT NULL,
    [Dilatation]                     VARCHAR (5)      NOT NULL,
    [CTGDescriptionID]               INT              NULL,
    [CTGDescription]                 NVARCHAR (100)   NULL,
    [CheckWithinAnHour]              VARCHAR (5)      NOT NULL,
    [CheckWithin30mins]              NVARCHAR (20)    NULL,
    [BuddySigWithin10mins]           VARCHAR (1)      NULL,
    [BuddyDescription]               NVARCHAR (MAX)   NULL,
    [BuddyStaffGroup]                VARCHAR (15)     NULL,
    [TimeBetweenAssessments]         INT              NULL,
    [TimeBetweenAssessments2ndStage] INT              NULL,
    [LabourCount]                    INT              NULL,
    [StaffGroup]                     VARCHAR (14)     NULL,
    [StaffInitials]                  VARCHAR (4)      NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CTG_CheckSession]
    ON [Maternity].[CTGAssessment]([CheckWithinAnHour] ASC)
    INCLUDE([SessionNumber], [FetusNumber], [TimeEntered]);


GO
CREATE NONCLUSTERED INDEX [IX_CTG_SessionFetus]
    ON [Maternity].[CTGAssessment]([SessionNumber] ASC, [FetusNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CTGCheck30Session]
    ON [Maternity].[CTGAssessment]([CheckWithin30mins] ASC)
    INCLUDE([SessionNumber], [FetusNumber], [TimeEntered]);


GO
CREATE NONCLUSTERED INDEX [IX_CTG_Order]
    ON [Maternity].[CTGAssessment]([OrderID] ASC, [HospitalPatientID] ASC)
    INCLUDE([SessionNumber], [FetusNumber], [Surname], [TimeEntered], [LabourCount]);


GO
CREATE NONCLUSTERED INDEX [IX_CTGFetusStart]
    ON [Maternity].[CTGAssessment]([FetusNumber] ASC, [CTGStartDate] ASC, [CheckWithinAnHour] ASC, [CheckWithin30mins] ASC)
    INCLUDE([SessionNumber], [TimeBetweenAssessments], [CTGStartTime], [LabourCount], [StaffGroup], [StaffInitials]);


GO
CREATE NONCLUSTERED INDEX [IX_CTG_BuddySig]
    ON [Maternity].[CTGAssessment]([BuddySigWithin10mins] ASC, [FetusNumber] ASC, [BuddyStaffGroup] ASC)
    INCLUDE([SessionNumber]);


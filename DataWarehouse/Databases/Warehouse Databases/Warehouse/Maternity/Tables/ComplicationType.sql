﻿CREATE TABLE [Maternity].[ComplicationType] (
    [ComplicationTypeID] INT          IDENTITY (1, 1) NOT NULL,
    [Complication]       VARCHAR (50) NULL,
    CONSTRAINT [PK_ComplicationType] PRIMARY KEY CLUSTERED ([ComplicationTypeID] ASC)
);


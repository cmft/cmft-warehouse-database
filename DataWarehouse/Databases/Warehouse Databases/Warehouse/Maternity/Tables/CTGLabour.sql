﻿CREATE TABLE [Maternity].[CTGLabour] (
    [SessionNumber]     UNIQUEIDENTIFIER NOT NULL,
    [FetusNumber]       INT              NULL,
    [TimeEntered]       DATETIME         NOT NULL,
    [HospitalPatientID] NVARCHAR (50)    NULL,
    [OrderNo]           BIGINT           NULL,
    [LabourCount]       INT              NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_CTGLabour]
    ON [Maternity].[CTGLabour]([LabourCount] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CTG_HospID]
    ON [Maternity].[CTGLabour]([HospitalPatientID] ASC)
    INCLUDE([TimeEntered], [OrderNo]);


﻿CREATE TABLE [SCR].[Imaging](
	[UniqueRecordId] [int] NOT NULL,
	[ReferralUniqueRecordId] [int] NOT NULL,
	[SystemId] [varchar](255) NULL,
	[OrganisationCode] [varchar](5) NULL,
	[EventDate] [date] NULL,
	[ImagingCode] [varchar](6) NULL,
	[ImagingModalityCode] [varchar](4) NULL,
	[AnatomicalSiteCode] [varchar](5) NULL,
	[AnatomicalSideCode] [varchar](1) NULL,
	[LesionSize] [int] NOT NULL,
	[ImagingReportReference] [text] NULL,
	[CA125ResultCode] [varchar](255) NULL, 
    CONSTRAINT [PK_Imaging] PRIMARY KEY ([UniqueRecordId])
)
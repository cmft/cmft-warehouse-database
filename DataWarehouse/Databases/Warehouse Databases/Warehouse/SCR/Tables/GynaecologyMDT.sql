﻿CREATE TABLE [SCR].[GynaecologyMDT](
	[UniqueRecordId] [varchar](255) NOT NULL,
	[ReferralUniqueRecordId] [int] NULL,
	[MeetingId] [int] NULL,
	[MDTDate] [date] NULL,
	[AppointmentDate] [date] NULL,
	[ConsultantCode] [varchar](8) NULL,
	[GradeCode] [varchar](255) NULL, 
    CONSTRAINT [PK_GynaecologyMDT] PRIMARY KEY ([UniqueRecordId])
)

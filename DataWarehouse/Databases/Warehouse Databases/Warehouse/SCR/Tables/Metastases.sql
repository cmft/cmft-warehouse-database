﻿CREATE TABLE [SCR].[Metastases](
	[ReferralUniqueRecordId] [int] NOT NULL,
	[CancerSite] [varchar](50) NULL,
	[Bone] [varchar](50) NULL,
	[Brain] [varchar](50) NULL,
	[Liver] [varchar](50) NULL,
	[Lung] [varchar](50) NULL,
	[Other] [varchar](50) NULL,
	[OtherDetail] [varchar](255) NULL,
	[Lymph] [varchar](50) NULL,
	[Adrenal] [varchar](50) NULL,
	[Skin] [varchar](50) NULL,
	[Unknown] [varchar](50) NULL,
	[BoneMarrow] [varchar](50) NULL,
	[BoneExMarrow] [varchar](50) NULL, 
    CONSTRAINT [PK_Metastases] PRIMARY KEY ([ReferralUniqueRecordId])
)

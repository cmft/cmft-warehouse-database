﻿CREATE TABLE [SCR].[Specialty] (
    [SpecialtyCode] INT           NOT NULL,
    [Specialty]     VARCHAR (150) NOT NULL,
    [Division]      VARCHAR (100) NOT NULL
);


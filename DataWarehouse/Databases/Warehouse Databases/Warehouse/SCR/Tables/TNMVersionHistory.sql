﻿CREATE TABLE [SCR].[TNMVersionHistory](
	[UniqueRecordId] [int] NOT NULL,
	[ReferralUniqueRecordId] [int] NOT NULL,
	[VersionNumber] [int] NOT NULL,
	[ChangedDateTime] [datetime] NOT NULL,
	[UserID] [varchar](50) NOT NULL,
	[Comments] [varchar](1000) NOT NULL,
	[Active] [bit] NOT NULL, 
    CONSTRAINT [PK_TNMVersionHistory] PRIMARY KEY ([UniqueRecordId])
)
﻿CREATE TABLE [SCR].[DefinitiveTreatment] (
    [UniqueRecordId]               INT           NOT NULL,
    [ReferralUniqueRecordId]       INT           NULL,
    [PatientPathwayId]             VARCHAR (20)  NULL,
    [DecisionToTreatDate]          DATETIME      NULL,
    [DTTOrganisationCode]          VARCHAR (5)   NULL,
    [FirstDefinitiveTreatmentDate] DATETIME      NULL,
    [InitialTreatmentCode]         CHAR (2)      NULL,
    [TreatmentOrganisationCode]    VARCHAR (5)   NULL,
    [TreatmentEventTypeCode]       VARCHAR (2)   NULL,
    [TreatmentSettingCode]         VARCHAR (2)   NULL,
    [PriorityCode]                 VARCHAR (1)   NULL,
    [TreatmentIntentCode]          VARCHAR (2)   NULL,
    [SupportTypeCode]              VARCHAR (2)   NULL,
    [ClinicalTrial]                INT           NULL,
    [WaitingTimeAdjustment]        INT           NULL,
    [AdjustmentReasonCode]         INT           NULL,
    [DelayReasonCode]              INT           NULL,
    [TreatmentNo]                  INT           NULL,
    [TreatmentNoId]                INT           NULL,
    [ValidatedForUpload]           INT           NULL,
    [DelayReasonComments]          VARCHAR (MAX) NULL,
    [TrackingNotes]                VARCHAR (MAX) NULL,
    [AllTrackingNotes]             VARCHAR (MAX) NULL,
    CONSTRAINT [PK_DefinitiveTreatment] PRIMARY KEY CLUSTERED ([UniqueRecordId] ASC)
);


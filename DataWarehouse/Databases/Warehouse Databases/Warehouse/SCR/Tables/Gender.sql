﻿CREATE TABLE [SCR].[Gender] (
    [GenderCode] INT          NOT NULL,
    [Gender]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED ([GenderCode] ASC)
);


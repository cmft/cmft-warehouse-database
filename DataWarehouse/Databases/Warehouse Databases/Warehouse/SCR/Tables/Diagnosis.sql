﻿CREATE TABLE [SCR].[Diagnosis] (
    [DiagnosisCode] VARCHAR (50)  NOT NULL,
    [Diagnosis]     VARCHAR (255) NOT NULL,
    [Highlight]     INT           NULL
);


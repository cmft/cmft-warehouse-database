﻿CREATE TABLE [SCR].[CancerSite] (
    [CancerSiteId]   INT          NOT NULL,
    [CancerSite]     VARCHAR (50) NOT NULL,
    [CancerSiteRole] VARCHAR (50) NULL
);


﻿CREATE TABLE [SCR].[ReferralGynaecology](
	[UniqueRecordId] [int] NOT NULL,
	[ReferralUniqueRecordId] [int] NOT NULL,
	[MetastaticLungCode] [int] NULL,
	[MetastaticBoneCode] [int] NULL,
	[MetastaticLiverCode] [int] NULL,
	[MetastaticBrainCode] [int] NULL,
	[MetastaticOtherCode] [int] NULL,
	[OtherMetastatic] [varchar](255) NULL,
	[Figo0Code] [varchar](3) NULL,
	[Figo2Code] [varchar](3) NULL,
	[Figo3Code] [varchar](3) NULL, 
    CONSTRAINT [PK_ReferralGynaecology] PRIMARY KEY ([UniqueRecordId])
)
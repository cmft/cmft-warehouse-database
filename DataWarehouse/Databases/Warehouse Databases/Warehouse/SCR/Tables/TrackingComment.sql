﻿CREATE TABLE [SCR].[TrackingComment] (
    [UniqueRecordId]         INT          NOT NULL,
    [ReferralUniqueRecordId] INT          NOT NULL,
    [UserID]                 VARCHAR (50) NOT NULL,
    [TrackingCommentTime]    DATETIME     NULL,
    [TrackingComment]        TEXT         NULL,
    CONSTRAINT [PK_TrackingComment] PRIMARY KEY CLUSTERED ([UniqueRecordId] ASC)
);


﻿CREATE TABLE [SCR].[Cancellation] (
    [CancellationCode] INT           NOT NULL,
    [Cancellation]     VARCHAR (150) NOT NULL,
    [Live]             INT           NULL
);


﻿create procedure [SCR].[LoadTrackingComment]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.TrackingComment


insert into SCR.TrackingComment
	(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,UserID
	,TrackingCommentTime
	,TrackingComment
	)
select
	 UniqueRecordId = COM_ID
	,ReferralUniqueRecordId= CARE_ID
	,UserID = [USER_ID]
	,TrackingCommentTime = DATE_TIME
	,TrackingComment = COMMENTS
from
	[$(CancerRegister)].dbo.tblTRACKING_COMMENTS


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


﻿CREATE procedure [SCR].[LoadReferenceData] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table SCR.PriorityType

insert into SCR.PriorityType
select
	 PriorityTypeCode = PRIORITY_CODE
	,PriorityType = PRIORITY_DESC
from
	[$(CancerRegister)].dbo.ltblPRIORITY_TYPE


truncate table SCR.CancerStatus

insert into SCR.CancerStatus
select
	 CancerStatusCode = STATUS_CODE
	,CancerStatus = STATUS_DESC
	,CancerStatusShort = SHORT_DESC
	,OrderBy = ORDER_BY
from
	[$(CancerRegister)].dbo.ltblSTATUS


truncate table SCR.SourceForOutpatient

insert into SCR.SourceForOutpatient
select
	 SourceForOutpatientCode = REF_CODE
	,SourceForOutpatient = REF_DESC
	,OrderBy = ORDER_BY
from
	[$(CancerRegister)].dbo.ltblOUT_PATIENT_REFERRAL


truncate table SCR.Cancellation

insert into SCR.Cancellation
select
	 CancellationCode = CANCELLED_CODE
	,Cancellation = CANCELLED_DESC
	,Live = LIVE
from
	[$(CancerRegister)].dbo.ltblCANCELLATION


truncate table SCR.Treatment

insert into SCR.Treatment
select
	 TreatmentCode = TREAT_CODE
	,Treatment = TREAT_DESC
	,OrderBy = ORDER_BY
from
	[$(CancerRegister)].dbo.ltblDEFINITIVE_TYPE


truncate table SCR.TreatmentSetting

insert into SCR.TreatmentSetting
select
	 TreatmentSettingCode = SET_CODE
	,TreatmentSetting = SET_DESC
from
	[$(CancerRegister)].dbo.ltblTREATMENT_SETTING


truncate table SCR.AdjustmentReason

insert into SCR.AdjustmentReason
select
	 AdjustmentReasonCode = ADJ_REASON_CODE
	,AdjustmentReason = ADJ_REASON_DESC
	,Live = LIVE
from
	[$(CancerRegister)].dbo.ltblADJ_TREATMENT


truncate table SCR.Consultant

insert into SCR.Consultant
select
	 ConsultantId = CON_ID
	,NationalConsultantCode = NATIONAL_CODE
	,ConsultantCode = CON_CODE
	,Consultant = CON_DESC
	,SpecialtyCode = SPEC_CODE
	,Title = TITLE
	,DepartmentCode = DEPT
	,Colorectal = COLORECTAL
	,Grade = GRADE
	,Baus = BAUS
	,IsDeleted = IS_DELETED
from
	[$(CancerRegister)].dbo.ltblCONSULTANTS


truncate table SCR.Diagnosis

insert into SCR.Diagnosis
select
	 DiagnosisCode = DIAG_CODE
	,Diagnosis = DIAG_DESC
	,Highlight = HIGHLIGHT
from
	[$(CancerRegister)].dbo.ltblDIAGNOSIS


truncate table SCR.TumourStatus

insert into SCR.TumourStatus
select
	 TumourStatusCode = STATUS_CODE
	,TumourStatus = STATUS_DESC
	,OrderBy = ORDER_BY
from
	[$(CancerRegister)].dbo.ltblCA_STATUS


truncate table SCR.Specialty

insert into SCR.Specialty
select
	 SpecialtyCode = SPECIALTY_CODE
	,Specialty = SPECIALTY_DESC
	,Division = SPECIALIES
from
	[$(CancerRegister)].dbo.ltblSPECIALTIES



truncate table SCR.CancerSite

insert into SCR.CancerSite
select
	 CancerSiteId = CA_ID
	,CancerSite = CA_SITE
	,CancerSiteRole = ROLE_NAME
from
	[$(CancerRegister)].dbo.ltblCANCER_SITES


truncate table SCR.Gender

insert into SCR.Gender
select
	 GenderCode = GENDER_CODE
	,Gender = GENDER_DESC
from
	[$(CancerRegister)].dbo.ltblGENDER



truncate table SCR.CancerType

insert into SCR.CancerType
select
	 CancerTypeCode = CANCER_TYPE_CODE
	,CancerType = CANCER_TYPE_DESC
	,Urology = UROLOGY
	,Haematology = HAEMATOLOGY
	,Breast = BREAST
from
	[$(CancerRegister)].dbo.ltblCANCER_TYPE



truncate table SCR.OPCS4

insert into SCR.OPCS4
select
	 OPCS4Code = PROC_CODE
	,OPCS4 = PROC_DESC
	,OPCSVersion = PROC_V
from
	[$(CancerRegister)].dbo.ltblPROCEDURES



truncate table SCR.Organisation

insert into SCR.Organisation
select
	 OrganisationCode = ORG_CODE
	,Organisation = ORG_DESC

from
	[$(CancerRegister)].dbo.ltblORG_CODES


truncate table SCR.SourceOfReferral

insert into SCR.SourceOfReferral
select
	 SourceOfReferralCode = SOURCE_CODE
	,SourceOfReferral = SOURCE_DESC
	,SourceOfReferralShort = SHORT_DESC
	,SourceOfReferralMini = MINI_DESC
	,OrderBy = ORDER_BY
from
	[$(CancerRegister)].dbo.ltblSOURCE_REFERRAL


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


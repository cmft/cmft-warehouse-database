﻿create procedure [SCR].[LoadChemotherapy]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.Chemotherapy

insert into SCR.Chemotherapy
(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,SystemId
	,OrganisationCode
	,ConsultantCode
	,SpecialtyCode
	,DecisionToTreatDate
	,DrugTherapyTypeCode
	,TreatmentIntentCode
	,DrugRegimenAcronym
	,InitialStartDate
	,PlannedCyclesCoursesNo
	,Dosage
	,Duration
	,DiseaseResponseCode
	,TreatmentOutcomeCode
	,HRGCode
	,EndocrineTherapyTypeCode
	,TreatmentGivenCode
	,TreatmentEventTypeCode
	,TreatmentSettingCode
	,ChemoRadiotherapyCode
	,EndDate
	,ClinicalTrial
	,ActualCyclesCourses
	,SevereComplications
	,Aromatase
	,AromataseDetail
	,RouteOfAdministration
	,DefinitiveTreatmentFlag
	,CwtProformaEntryFlag
	,Comments
)
select
	 UniqueRecordId = CHEMO_ID
	,ReferralUniqueRecordId = CARE_ID
	,SystemId = TEMP_ID
	,OrganisationCode = N9_1_SITE_CODE
	,ConsultantCode = case when N9_2_CONSULTANT = '' then null else N9_2_CONSULTANT end
	,SpecialtyCode = case when N9_3_SPECIALTY = '' then null else N9_3_SPECIALTY end
	,DecisionToTreatDate = N9_4_DECISION_DATE
	,DrugTherapyTypeCode = N9_7_THERAPY_TYPE
	,TreatmentIntentCode = case when N9_8_TREATMENT_INTENT = '' then null else N9_8_TREATMENT_INTENT end
	,DrugRegimenAcronym = case when N9_9_DRUG_REGIMEN = '' then null else N9_9_DRUG_REGIMEN end
	,InitialStartDate = N9_10_START_DATE
	,PlannedCyclesCoursesNo = N9_16_CYCLE_NO
	,Dosage = N9_20_DOSAGE
	,Duration = N9_21_DURATION
	,DiseaseResponseCode = N9_22_RESPONSE
	,TreatmentOutcomeCode = N9_23_CHANGE_REASON
	,HRGCode = N9_24_CHEMO_HRG
	,EndocrineTherapyTypeCode = case when N_B7_ENDOCRINE_TYPE = '' then null else N_B7_ENDOCRINE_TYPE end
	,TreatmentGivenCode = case when N_L27_CHEMO_GIVEN = '' then null else N_L27_CHEMO_GIVEN end
	,TreatmentEventTypeCode = N_TREATMENT_EVENT
	,TreatmentSettingCode = N_TREATMENT_SETTING
	,ChemoRadiotherapyCode = N_CHEMORADIO
	,EndDate = L_END_DATE
	,ClinicalTrial = L_TRIAL
	,ActualCyclesCourses = L_CYCLE_GIVEN
	,SevereComplications = L_NAMED_COMP
	,Aromatase = L_AROMATASE
	,AromataseDetail = L_AROMATASE_DETAILS
	,RouteOfAdministration = L_ROUTE
	,DefinitiveTreatmentFlag = DEFINITIVE_TREATMENT
	,CwtProformaEntryFlag = CWT_PROFORMA
	,Comments = L_COMMENTS
from
	[$(CancerRegister)].dbo.tblMAIN_CHEMOTHERAPY


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


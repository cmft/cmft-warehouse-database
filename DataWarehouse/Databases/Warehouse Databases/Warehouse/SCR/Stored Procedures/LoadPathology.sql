﻿create procedure [SCR].[LoadPathology]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.Pathology


insert into SCR.Pathology
(
	UniqueRecordId
	,ReferralUniqueRecordId
	,SystemId
	,PrimaryDiagnosisCode
	,MorphologyCode
	,Topography
	,GradeOfDifferentiationCode
	,TumourLateralityCode
	,InvestigationTypeCode
	,ExcisionMargin
	,PathologyReport
	,NodesExaminedNumber
	,TStageCode
	,TLetterCode
	,NStageCode
	,NLetterCode
	,MStageCode
	,MLetterCode
	,PreOperativeTherapyCode
)
select
	 UniqueRecordId = Pathology.PATHOLOGY_ID
	,ReferralUniqueRecordId = Pathology.CARE_ID
	,SystemId = Pathology.TEMP_ID
	,PrimaryDiagnosisCode = Pathology.N8_6_DIAGNOSIS
	,MorphologyCode = Pathology.N8_10_HISTOLOGY
	,Topography = Pathology.L_PATHOLOGY_TOPOGRAPHY
	,GradeOfDifferentiationCode = Pathology.N8_11_GRADE_DIFF
	,TumourLateralityCode = Pathology.N8_7_TUMOUR_LATERALITY
	,InvestigationTypeCode = Pathology.N8_1_PATHOLOGY_TYPE
	,ExcisionMargin = Pathology.N8_13_EXCISION_MARGINS
	,PathologyReport = Pathology.L_PATHOLOGY_TEXT
	,NodesExaminedNumber = Pathology.N8_14_NODES
	,TStageCode = Pathology.N8_16_PATH_T_STAGE
	,TLetterCode = Pathology.L_PATH_T_LETTER
	,NStageCode = Pathology.N8_17_PATH_N_STAGE
	,NLetterCode = Pathology.L_PATH_N_LETTER
	,MStageCode = Pathology.N8_18_PATH_M_STAGE
	,MLetterCode = Pathology.L_PATH_M_LETTER
	,PreOperativeTherapyCode = Pathology.L_NEOADJUVANT_THERAPY
from
	[$(CancerRegister)].dbo.tblMAIN_PATHOLOGY Pathology



select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


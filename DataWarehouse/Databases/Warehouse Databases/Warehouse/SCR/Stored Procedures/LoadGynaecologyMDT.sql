﻿create procedure [SCR].[LoadGynaecologyMDT]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.GynaecologyMDT


insert into SCR.GynaecologyMDT
(
	UniqueRecordId
	,ReferralUniqueRecordId
	,MeetingId
	,MDTDate
	,AppointmentDate
	,ConsultantCode
	,GradeCode
)
select
	 UniqueRecordId = GynaecologyMDT.MDT_ID
	,ReferralUniqueRecordId = GynaecologyMDT.CARE_ID
	,MeetingId = GynaecologyMDT.MEETING_ID
	,MDTDate = cast(GynaecologyMDT.MDT_DATE as date)
	,AppointmentDate = cast(GynaecologyMDT.APP_DATE as date)
	,ConsultantCode = GynaecologyMDT.CONSULTANT
	,GradeCode = GynaecologyMDT.GRADE
from
	[$(CancerRegister)].dbo.tblGYNAECOLOGY_MDT GynaecologyMDT
	

select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


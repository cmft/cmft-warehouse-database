﻿create procedure [SCR].[LoadSurgery]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.Surgery


insert into SCR.Surgery
(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,SystemId
	,OrganisationCode
	,ConsultantCode
	,SpecialtyCode
	,TreatmentIntentCode
	,DecisionToOperateDate
	,AdmissionDate
	,SurgeryDate
	,MainProcedureSiteCode
	,MainProcedureTypeCode
	,MainProcedureId
	,SubProcedureSite2
	,SubProcedureType2
	,SubProcedureId2
	,SubProcedureSite3
	,SubProcedureType3
	,SubProcedureId3
	,SubProcedureSite4
	,SubProcedureType4
	,SubProcedureId4
	,MainProcedureCode1
	,SubProcedureCode2
	,SubProcedureCode3
	,SubProcedureCode4
	,DischargeDate
	,DischargeDestinationCode
	,TreatmentEventTypeCode
	,TreatmentSettingCode
	,ClinicalTrial
	,SurgeonCode
	,Surgeon
)
select
	 UniqueRecordId = SURGERY_ID
	,ReferralUniqueRecordId = CARE_ID
	,SystemId = TEMP_ID
	,OrganisationCode = N7_1_SITE_CODE
	,ConsultantCode = case when N7_2_CONSULTANT = '' then null else N7_2_CONSULTANT end
	,SpecialtyCode = case when N7_3_SPECIALTY = '' then null else N7_3_SPECIALTY end
	,TreatmentIntentCode = N7_4_TREATMENT_INTENT
	,DecisionToOperateDate = N7_5_DECISION_DATE
	,AdmissionDate = N7_8_ADMISSION_DATE
	,SurgeryDate = N7_9_SURGERY_DATE
	,MainProcedureSiteCode = L_PROCEDURE_SITE
	,MainProcedureTypeCode = L_PROCEDURE_TYPE
	,MainProcedureId = L_PROCEDURE
	,SubProcedureSite2 = L_PROCEDURE_SITE2
	,SubProcedureType2 = L_PROCEDURE_TYPE2
	,SubProcedureId2 = L_PROCEDURE2
	,SubProcedureSite3 = L_PROCEDURE_SITE3
	,SubProcedureType3 = L_PROCEDURE_TYPE3
	,SubProcedureId3 = L_PROCEDURE3
	,SubProcedureSite4 = L_PROCEDURE_SITE4
	,SubProcedureType4 = L_PROCEDURE_TYPE4
	,SubProcedureId4 = L_PROCEDURE4
	,MainProcedureCode1 = case when N7_10_PROCEDURE_1 = '' then null else N7_10_PROCEDURE_1 end
	,SubProcedureCode2 = case when N7_11_PROCEDURE_2 = '' then null else N7_11_PROCEDURE_2 end
	,SubProcedureCode3 = case when N7_11_PROCEDURE_3 = '' then null else N7_11_PROCEDURE_3 end
	,SubProcedureCode4 = case when N7_11_PROCEDURE_4 = '' then null else N7_11_PROCEDURE_4 end
	,DischargeDate = N7_12_DISCHARGE_DATE
	,DischargeDestinationCode = N7_13_DISCHARGE_DESTINATION
	,TreatmentEventTypeCode = N_TREATMENT_EVENT
	,TreatmentSettingCode = N_TREATMENT_SETTING
	,ClinicalTrial = L_TRIAL
	,SurgeonCode = case when L_SURGEON = '' then null else L_SURGEON end
	,Surgeon = case when L_SURGEON_NAME = '' then null else L_SURGEON_NAME end
from
	[$(CancerRegister)].dbo.tblMAIN_SURGERY


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


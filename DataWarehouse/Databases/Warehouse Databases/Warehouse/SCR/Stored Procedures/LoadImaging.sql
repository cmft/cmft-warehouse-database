﻿create procedure [SCR].[LoadImaging]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.Imaging


insert into SCR.Imaging
(
	UniqueRecordId
	,ReferralUniqueRecordId
	,SystemId
	,OrganisationCode
	,EventDate
	,ImagingCode
	,ImagingModalityCode
	,AnatomicalSiteCode
	,AnatomicalSideCode
	,LesionSize
	,ImagingReportReference
	,CA125ResultCode
)
select
	 UniqueRecordId = Imaging.IMAGE_ID
	,ReferralUniqueRecordId = Imaging.CARE_ID
	,SystemId = Imaging.TEMP_ID
	,OrganisationCode = Imaging.N3_1_SITE_CODE
	,EventDate = cast(Imaging.N3_2_IMAGING_DATE as date)
	,ImagingCode = Imaging.L_IMAGING_CODE
	,ImagingModalityCode = Imaging.N3_3_MODALITY
	,AnatomicalSiteCode = Imaging.N3_4_ANATOMICAL_SITE
	,AnatomicalSideCode = Imaging.L_ANATOMICAL_SIDE_CODE
	,LesionSize = 0
	,ImagingReportReference = Imaging.L_IMAGING_REPORT_TEXT
	,CA125ResultCode = Imaging.R_CA125_RESULT

from
	[$(CancerRegister)].dbo.tblMAIN_IMAGING Imaging


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


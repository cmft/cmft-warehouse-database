﻿create procedure [SCR].[LoadTNMVersionHistory]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.TNMVersionHistory


insert into SCR.TNMVersionHistory
(
	UniqueRecordId
	,ReferralUniqueRecordId
	,VersionNumber
	,ChangedDateTime
	,UserID
	,Comments
	,Active
)
select
	UniqueRecordId = VERSION_ID
	,ReferralUniqueRecordId = CARE_ID
	,VersionNumber = VERSION_NUMBER
	,ChangedDateTime = DATE_CHANGED
	,UserID = [USER_ID]
	,Comments = COMMENTS
	,Active = ACTIVE
from
	[$(CancerRegister)].dbo.tblTNM_VERSION_HISTORY_LOCAL



select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


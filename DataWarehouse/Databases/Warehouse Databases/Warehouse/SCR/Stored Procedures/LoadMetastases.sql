﻿create procedure [SCR].[LoadMetastases]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.Metastases


insert into SCR.Metastases
(
	ReferralUniqueRecordId
	,CancerSite
	,Bone
	,Brain
	,Liver
	,Lung
	,Other
	,OtherDetail
	,Lymph
	,Adrenal
	,Skin
	,Unknown
	,BoneMarrow
	,BoneExMarrow
)
select
	ReferralUniqueRecordId = CARE_ID
	,CancerSite = [Cancer Site]
	,Bone = [Bone Metastases]
	,Brain = [Brain Metastases]
	,Liver = [Liver Metastases]
	,Lung = [Lung Metastases]
	,Other = [Other Metastases]
	,OtherDetail = [Other Metastases Details]
	,Lymph = [Lymph Metastases]
	,Adrenal = [Adrenal Metastases]
	,Skin = [Skin Metastases]
	,Unknown = [Unknown Metastases]
	,BoneMarrow = [Bone Marrow Metastases]
	,BoneExMarrow = [Bone Ex-Marrow Metastases]
from
	[$(CancerRegister)].dbo.BIvwAllMetastases Metastases


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


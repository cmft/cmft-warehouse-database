﻿create procedure [SCR].[LoadReferralGynaecology]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.ReferralGynaecology


insert into SCR.ReferralGynaecology
(
	UniqueRecordId
	,ReferralUniqueRecordId
	,MetastaticLungCode
	,MetastaticBoneCode
	,MetastaticLiverCode
	,MetastaticBrainCode
	,MetastaticOtherCode
	,OtherMetastatic
	,Figo0Code
	,Figo2Code
	,Figo3Code
)
select
	 UniqueRecordId = ReferralGynaecology.REF_ID
	,ReferralUniqueRecordId = ReferralGynaecology.CARE_ID
	,MetastaticLungCode = ReferralGynaecology.R_MET_LUNG
	,MetastaticBoneCode = ReferralGynaecology.R_MET_BONE
	,MetastaticLiverCode = ReferralGynaecology.R_MET_LIVER
	,MetastaticBrainCode = ReferralGynaecology.R_MET_BRAIN
	,MetastaticOtherCode = ReferralGynaecology.R_MET_OTHER
	,OtherMetastatic = ReferralGynaecology.R_OTHER_METS
	,Figo0Code = ReferralGynaecology.R_FIGO
	,Figo2Code = ReferralGynaecology.R_FIGO2
	,Figo3Code = ReferralGynaecology.R_FIGO3
from
	[$(CancerRegister)].dbo.tblREFERRAL_GYNAECOLOGY ReferralGynaecology


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


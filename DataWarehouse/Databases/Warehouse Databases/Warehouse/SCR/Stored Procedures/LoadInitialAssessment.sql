﻿CREATE procedure [SCR].[LoadInitialAssessment]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table SCR.InitialAssessment

insert into SCR.InitialAssessment
	(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,MenstrualStatus
	,LmpDate
	,PreviousMalignancy
	,YearDiagnosed
	,TreatedElsewhereForCurrentTumour
	,TypeOfTobacco
	,HistoryOfSmoking
	,ChewingTobacco
	,YearStoppedSmoking
	,EstimatedPackYears
	,HistoryOfAlcohol
	,SymptomsFirstNotedDate
	,GeneticallyDeterminedSkinCancerSyndrome
	,COPD
	,DementiaCerebrovascularDisease
	,RenalFailure
	,OtherMalignancy
	,SevereWeightLoss
	,CardiovascularDisease
	,PredisposingCondition
	,Splenomegaly
	,Hepatomegaly
	,LymphodenopathySite
	,SkinInvolvement
	,TesticularInvolvement
	,NeurologicalInvolvement
	,Pruritis
	,HospitalCode
	,AssessmentDate
	,AssessmentBy
	,Height
	,Weight
	,DoYouWishToEnterSocialHistory
	,SmokingAmount
	,AlcoholAmount
	,SmokingCessationInfoGiven
	,MalignancyDetails
	,CardiacDiseaseHistory
	,CardiacRiskIndex
	,PerformanceStatus
	,ColorectalAdenoma
	,UlcerativeColitis
	,Crohns
	,FamillialAdenomatousPolyposis
	,ExaminationFindingsR
	,ExaminationFindingsL
	,NumberOfChildren
	,HRT
	,HRTDuration
	,COPDDetails
	,AbdominalPain
	,ChangeInBowels
	,Diarrhoea
	,Constipation
	,AlternatingDiarrhoeaConstipation
	,NocturnalBowelAction
	,BloodInStools
	,Fresh
	,Dark
	,Separate
	,Mixed
	,Painful
	,Painless
	,Tenesmus
	,IronDeficiency
	,AsymptomaticScreening
	,WeightLoss
	,LostWeight
	,PalpableMass
	,PreviousAbdominalSurgery
	,Appendectomy
	,Cholecystectomy
	,Gastroduodenal
	,GynaePelvic
	,OtherPelvic
	,SexualFunction
	,BladderFunction
	,OtherBladderFunction
	,OtherLungDisease
	,LumpNeck
	,LumpPartoid
	,Otologia
	,LumpMouth
	,Voice
	,LesionCheek
	,SoreThroat
	,Dysphagia
	,LesionOralCavity
	,VocalCordPalsy
	,Lymphodenopathy
	,Dysphonia
	,LesionOralPharynx
	,PresentingComplaintsA
	,PresentingComplaintsB
	,PreviousSkinMalignancySameSite
	,PreviousSkinMalignancyOtherSite
	,TransplantPatient
	,ImmunosuppressiveDisorder
	,PhototherapySameSite
	,RadiotherapySameSite
	,UnknownLmp
	,CycleLength
	,AgeAtMenarch
	,AgeAtMenopause
	,MedicalMenopause
	,Children
	,AgeAtFirstDelivery
	,BreastFedFirstChild
	,LengthOfTimeBreastFed
	,OralContractivePill
	,OcpLengthOfTimePreFirstChild
	,OcpTotalLengthOfTime
	,OcpDetails
	,Coil
	,Sterilised
	,Hysterectomy
	,AgeAtHysterectomy
	,TypeOfHysterectomy
	,OvariesConserved
	,MassLeft
	,MassRight
	,PositionLeft
	,PositionRight
	,DistanceFromNippleLeft
	,DistanceFromNippleRight
	,SizeOfMassLeft
	,SizeOfMassRight
	,CysticLeft
	,CysticRight
	,SolidLeft
	,SolidRight
	,FixedToSkinLeft
	,FixedToSkinRight
	,FixedToMuscleLeft
	,FixedToMuscleRight
	,DiscreteLumpLeft
	,DiscreteLumpRight
	,NodularityLeft
	,NodularityRight
	,NippleChangeLeft
	,NippleChangeRight
	,NippleDischargeLeft
	,NippleDischargeRight
	,NippleDistortionLeft
	,NippleDistortionRight
	,AbscessLeft
	,AbscessRight
	,OrangeLeft
	,OrangeRight
	,UlcerationLeft
	,UlcerationRight
	,LymphNodesLeft
	,LymphNodesRight
	,OtherLeft
	,OtherRight
	,AxillaryNodeStatusLeft
	,AxillaryNodeStatusRight
	,PreviousMammogram
	,Details
	,Pregnant
	,Postpartum
	,CurrentlyBreastFeeding
	,ExpectedDeliveryDate
	,DeliveryDate
	,Trimester
	,Lactating
	,IsThereSignificantCoMorbidity
	,Odynophagia
	,EpigastricPain
	,Bleeding
	,Jaundice
	,Vomiting
	,Reflux
	,Oesophagitis
	,BarrettsLength
	,OtherRisks
	,Oesophagus
	,Stomach
	,SmallBowel
	,Colon
	,Rectum
	,PhysicalExam
	,Anaemic
	,Mass
	,Nodes
	,HoarseVoice
	,DisseminatedDisease
	,LymphodenopathySize
	,Relations
	,Parity
	,CesareanSection
	,PreviousSmear
	,DateOfLastSmear
	,ResultOfLastSmear
	,ResultDetails
	,Infertility
	,InfertilityTreatment
	,PelvicExam
	,Vulva
	,Cervix
	,Uterus
	,Adnexa
	,RectumExam
	,CoMorbidityIndex
	,PreExistingMole
	,MoleDetails
	,Luts
	,PreviousTurp
	,DigitalRectalExam
	,AcutePancreatitis
	,AlcoholInducedPancreatitis
	,Anaemia
	,Angina
	,OnAsprin
	,InflammatoryBowelDisease
	,CardiacDisease
	,CerebalVascularDisease
	,ChronicPancreatitis
	,CirrhosisLiverDisease
	,BloodClottingDisorder
	,COAD
	,HistoryOfCVA
	,TypeIDiabetes
	,TypeIIDiabetes
	,HistoryOfCardiacFailure
	,IschaemicHeartDisease
	,Hypertension
	,LiverDisease
	,MentalIllness
	,MyocardialInfarction
	,NeurologicalDisease
	,CMOther
	,PeripheralVascularDisease
	,RenalDisease
	,RespiratoryDisease
	,Creatinine
	,OnSystemicSteroids
	,OtherVascularDisease
	,OnWarfarin
	,OnDipyridamole
	,OnClopidogrel
	,Pacemaker
	,OtherCoMorbidities
	,HistoryOfComplaints
	,SocialHistory
	,PastMedicalHistory
	,Medication
	,Allergies
)
select
	 UniqueRecordId = ASSESS_ID
	,ReferralUniqueRecordId = CARE_ID
	,MenstrualStatus = N_B4_MENSTRUAL_STATUS
	,LmpDate = N_B5_LMP_DATE
	,PreviousMalignancy = N_HN1_CANCER_HISTORY
	,YearDiagnosed = N_HN2_CANCER_YEAR
	,TreatedElsewhereForCurrentTumour = N_HN3_PREVIOUS_TREATMENT
	,TypeOfTobacco = N_HN4_TOBACCO
	,HistoryOfSmoking = N_HN5_SMOKER
	,ChewingTobacco = N_HN6_CHEWING_TOBACCO
	,YearStoppedSmoking = N_HN7_YEAR_STOPPED
	,EstimatedPackYears = N_HN8_PACK_YEARS
	,HistoryOfAlcohol = N_HN9_ALCOHOL
	,SymptomsFirstNotedDate = N_HN11_SYMPTOM_DATE
	,GeneticallyDeterminedSkinCancerSyndrome = N_SK13_DETERMINED
	,COPD = N_L4_COPD
	,DementiaCerebrovascularDisease = N_L10_DEMENTIA
	,RenalFailure = N_L11_RENAL
	,OtherMalignancy = N_L12_OTHER
	,SevereWeightLoss = N_L13_SEVERE
	,CardiovascularDisease = N_L9_CARDIOVASCULAR
	,PredisposingCondition = N_H7_PREDISPOSING
	,Splenomegaly = N_H12_SPLEEN
	,Hepatomegaly = N_H13_HEPATOMEGALY
	,LymphodenopathySite = N_H14_LYMPHODENOPATHY
	,SkinInvolvement = N_H15_SKIN
	,TesticularInvolvement = N_H16_TESTICULAR
	,NeurologicalInvolvement = N_H17_NEUROLOGICAL
	,Pruritis = N_H19_PRURITIS
	,HospitalCode = L_HOSPITAL
	,AssessmentDate = L_ASSESS_DATE
	,AssessmentBy = L_AS_BY
	,Height = L_HEIGHT
	,Weight = L_WEIGHT
	,DoYouWishToEnterSocialHistory = L_SOCIAL
	,SmokingAmount = L_SMOKING_AMOUNT
	,AlcoholAmount = L_ALCOHOL_AMOUNT
	,SmokingCessationInfoGiven = L_SMOKING_CESSATION
	,MalignancyDetails = R_SPECIFY_OTHER_CA
	,CardiacDiseaseHistory = R_DISEASE_HISTORY
	,CardiacRiskIndex = R_CARDIAC_RISK
	,PerformanceStatus = R_PERFORMANCE
	,ColorectalAdenoma = R_ADENOMA
	,UlcerativeColitis = R_COLITIS
	,Crohns = R_CROHNS
	,FamillialAdenomatousPolyposis = R_POLYPOSIS
	,ExaminationFindingsR = R_FINDINGS
	,ExaminationFindingsL = L_FINDINGS
	,NumberOfChildren = L_CHILDREN
	,HRT = L_ON_HRT
	,HRTDuration = L_HRT
	,COPDDetails = L_COPD_DETAILS
	,AbdominalPain = L_ABDO_PAIN
	,ChangeInBowels = L_CHANGE_BOWELS
	,Diarrhoea = L_DIARRHOEA
	,Constipation = L_CONSTIPATION
	,AlternatingDiarrhoeaConstipation = L_ALTERNATING
	,NocturnalBowelAction = L_NOCTURNAL
	,BloodInStools = L_BLOOD_STOOLS
	,Fresh = L_FRESH
	,Dark = L_DARK
	,Separate = L_SEPARATE
	,Mixed = L_MIXED
	,Painful = L_PAINFUL
	,Painless = L_PAINLESS
	,Tenesmus = L_TENESMUS
	,IronDeficiency = L_IRON_DEFICIENCY
	,AsymptomaticScreening = L_ASYMPTOMATIC
	,WeightLoss = L_WEIGHT_LOSS
	,LostWeight = L_LOST_WEIGHT
	,PalpableMass = L_PALPABLE_MASS
	,PreviousAbdominalSurgery = L_ABDO_SURGERY
	,Appendectomy = L_APPENDICECTOMY
	,Cholecystectomy = L_CHOLECYSTECTOMY
	,Gastroduodenal = L_GASTRODUODENAL
	,GynaePelvic = L_GYNAE_PELVIC
	,OtherPelvic = L_OTHER_PELVIC
	,SexualFunction = L_ERECTILE_FUNCTION
	,BladderFunction = L_BLADDER_FUNCTION
	,OtherBladderFunction = L_OTHER_BLADDER
	,OtherLungDisease = L_OTHER_LUNG
	,LumpNeck = L_LUMP_NECK
	,LumpPartoid = L_LUMP_PARTOID
	,Otologia = L_OTOLOGIA
	,LumpMouth = L_SWELLING
	,Voice = L_VOICE
	,LesionCheek = L_LESION
	,SoreThroat = L_THROAT
	,Dysphagia = L_DYSPHAGIA
	,LesionOralCavity = L_MOUTH
	,VocalCordPalsy = L_PALSY
	,Lymphodenopathy = L_LYMPHODENOPATHY
	,Dysphonia = L_DYSPHONIA
	,LesionOralPharynx = L_PHARYNX
	,PresentingComplaintsA = L_COMPLAINT_A
	,PresentingComplaintsB = L_COMPLAINT_B
	,PreviousSkinMalignancySameSite = L_SAME_SKIN
	,PreviousSkinMalignancyOtherSite = L_OTHER_SKIN
	,TransplantPatient = L_TRANSPLANT
	,ImmunosuppressiveDisorder = L_IMMUNO_DISORDER
	,PhototherapySameSite = L_PHOTOTHERAPY
	,RadiotherapySameSite = L_RADIOTHERAPY
	,UnknownLmp = L_UN_LMP
	,CycleLength = L_CYCLE
	,AgeAtMenarch = L_AGE_MEN
	,AgeAtMenopause = L_AGE_MENO
	,MedicalMenopause = L_MEDICAL_MEN
	,Children = L_CHILDREN_YN
	,AgeAtFirstDelivery = L_AGE_FIRST
	,BreastFedFirstChild = L_BREAST_FED
	,LengthOfTimeBreastFed = L_FED_MONTHS
	,OralContractivePill = L_OCP
	,OcpLengthOfTimePreFirstChild = L_OCP_PRE
	,OcpTotalLengthOfTime = L_OCP_TOTAL
	,OcpDetails = L_OCP_COMMENTS
	,Coil = L_COIL
	,Sterilised = L_STERILISED
	,Hysterectomy = L_HYSTERECTOMY
	,AgeAtHysterectomy = L_HYST_AGE
	,TypeOfHysterectomy = L_HYST_TYPE
	,OvariesConserved = L_OVARIES
	,MassLeft = L_MASS_L
	,MassRight = L_MASS_R
	,PositionLeft = L_CLOCK_L
	,PositionRight = L_CLOCK_R
	,DistanceFromNippleLeft = L_DIS_L
	,DistanceFromNippleRight = L_DIS_R
	,SizeOfMassLeft = L_SIZE_L
	,SizeOfMassRight = L_SIZE_R
	,CysticLeft = L_CYSTIC_L
	,CysticRight = L_CYSTIC_R
	,SolidLeft = L_SOLID_L
	,SolidRight = L_SOLID_R
	,FixedToSkinLeft = L_FIXED_SKIN_L
	,FixedToSkinRight = L_FIXED_SKIN_R
	,FixedToMuscleLeft = L_FIXED_MUS_L
	,FixedToMuscleRight = L_FIXED_MUS_R
	,DiscreteLumpLeft = L_LUMP_L
	,DiscreteLumpRight = L_LUMP_R
	,NodularityLeft = L_NODULARITY_L
	,NodularityRight = L_NODULARITY_R
	,NippleChangeLeft = L_NIPPLE_CHANGE_L
	,NippleChangeRight = L_NIPPLE_CHANGE_R
	,NippleDischargeLeft = L_NIPPLE_DISCHARGE_L
	,NippleDischargeRight = L_NIPPLE_DISCHARGE_R
	,NippleDistortionLeft = L_NIPPLE_DISTORTION_L
	,NippleDistortionRight = L_NIPPLE_DISTORTION_R
	,AbscessLeft = L_ABSCESS_L
	,AbscessRight = L_ABSCESS_R
	,OrangeLeft = L_ORANGE_L
	,OrangeRight = L_ORANGE_R
	,UlcerationLeft = L_ULCERATION_L
	,UlcerationRight = L_ULCERATION_R
	,LymphNodesLeft = L_LYMPH_L
	,LymphNodesRight = L_LYMPH_R
	,OtherLeft = L_OTHER_L
	,OtherRight = L_OTHER_R
	,AxillaryNodeStatusLeft = L_AUX_L
	,AxillaryNodeStatusRight = L_AUX_R
	,PreviousMammogram = L_MAMMOGRAM
	,Details = L_MAM_INFO
	,Pregnant = L_PREGNANT
	,Postpartum = L_POSTPARTUM
	,CurrentlyBreastFeeding = L_FEEDING
	,ExpectedDeliveryDate = L_DUE_DATE
	,DeliveryDate = L_DELIVERY_DATE
	,Trimester = L_TRIMESTER
	,Lactating = L_LACTATING
	,IsThereSignificantCoMorbidity = L_CO_M
	,Odynophagia = L_ODYNOPHAGIA
	,EpigastricPain = L_EPIGASTRIC
	,Bleeding = L_BLEEDING
	,Jaundice = L_JAUNDICE
	,Vomiting = L_VOMITING
	,Reflux = L_REFLUX
	,Oesophagitis = L_OESOPHAGITIS
	,BarrettsLength = L_BARRETTS
	,OtherRisks = L_OTHER_RISKS
	,Oesophagus = L_OESOPHAGUS
	,Stomach = L_STOMACH
	,SmallBowel = L_SMALL_BOWEL
	,Colon = L_COLON
	,Rectum = L_RECTUM
	,PhysicalExam = L_PHYSICAL
	,Anaemic = L_ANAEMIC
	,Mass = L_MASS
	,Nodes = L_NODES
	,HoarseVoice = L_HOARSE_VOICE
	,DisseminatedDisease = L_DISSEMINATED
	,LymphodenopathySize = L_LYMPH_SIZE
	,Relations = L_FAMILY
	,Parity = L_PARITY
	,CesareanSection = L_CESAREAN
	,PreviousSmear = L_SMEAR
	,DateOfLastSmear = L_SMEAR_DATE
	,ResultOfLastSmear = L_SMEAR_RESULT
	,ResultDetails = L_SMEAR_DETAILS
	,Infertility = L_INFERTILITY
	,InfertilityTreatment = L_INFERTILITY_TX
	,PelvicExam = L_PELVIC_EXAM
	,Vulva = L_EXAM_VULVA
	,Cervix = L_EXAM_CERVIX
	,Uterus = L_EXAM_UTERUS
	,Adnexa = L_EXAM_ADNEXA
	,RectumExam = L_EXAM_RECTUM
	,CoMorbidityIndex = L_COMORBIDITY_INDEX
	,PreExistingMole = L_MOLE
	,MoleDetails = L_MOLE_DETAILS
	,Luts = L_LUTS
	,PreviousTurp = L_TURP
	,DigitalRectalExam = L_DRE
	,AcutePancreatitis = L_CM_ACUTE
	,AlcoholInducedPancreatitis = L_CM_ALCOHOL
	,Anaemia = L_CM_ANAEMIA
	,Angina = L_CM_ANGINA
	,OnAsprin = L_CM_ASPRIN
	,InflammatoryBowelDisease = L_CM_BOWEL
	,CardiacDisease = L_CM_CARDIAC
	,CerebalVascularDisease = L_CM_CEREBAL
	,ChronicPancreatitis = L_CM_CHRONIC
	,CirrhosisLiverDisease = L_CM_CIRRHOSIS
	,BloodClottingDisorder = L_CM_CLOTTING
	,COAD = L_CM_COAD
	,HistoryOfCVA = L_CM_CVA
	,TypeIDiabetes = L_CM_DIABETES1
	,TypeIIDiabetes = L_CM_DIABETES2
	,HistoryOfCardiacFailure = L_CM_FAILURE
	,IschaemicHeartDisease = L_CM_HEART
	,Hypertension = L_CM_HYPERTENSION
	,LiverDisease = L_CM_LIVER
	,MentalIllness = L_CM_MENTAL
	,MyocardialInfarction = L_CM_MI
	,NeurologicalDisease = L_CM_NEURO
	,CMOther = L_CM_OTHER
	,PeripheralVascularDisease = L_CM_PERIPHERAL
	,RenalDisease = L_CM_RENAL
	,RespiratoryDisease = L_CM_RESPIRATORY
	,Creatinine = L_CM_SERUM
	,OnSystemicSteroids = L_CM_STEROIDS
	,OtherVascularDisease = L_CM_VASCULAR
	,OnWarfarin = L_CM_WARFARIN
	,OnDipyridamole = L_CM_DIP
	,OnClopidogrel = L_CM_CLOP
	,Pacemaker = L_CM_PACE
	,OtherCoMorbidities = L_OTHER_MORBIDITIES
	,HistoryOfComplaints = L_COMMENTS
	,SocialHistory = L_OTHER_INFO
	,PastMedicalHistory = L_OTHER_PMH
	,Medication = L_MEDICATION
	,Allergies = L_ALLERGIES
from
	[$(CancerRegister)].dbo.tblINITIAL_ASSESSMENT


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



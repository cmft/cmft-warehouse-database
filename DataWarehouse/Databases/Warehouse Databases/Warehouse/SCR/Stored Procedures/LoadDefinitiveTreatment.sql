﻿CREATE procedure [SCR].[LoadDefinitiveTreatment]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @inserted int

truncate table SCR.DefinitiveTreatment


insert into SCR.DefinitiveTreatment
	(
	 UniqueRecordId
	,ReferralUniqueRecordId
	,PatientPathwayId
	,DecisionToTreatDate
	,DTTOrganisationCode
	,FirstDefinitiveTreatmentDate
	,InitialTreatmentCode
	,TreatmentOrganisationCode
	,TreatmentEventTypeCode
	,TreatmentSettingCode
	,PriorityCode
	,TreatmentIntentCode
	,SupportTypeCode
	,ClinicalTrial
	,WaitingTimeAdjustment
	,AdjustmentReasonCode
	,DelayReasonCode
	,TreatmentNo
	,TreatmentNoId
	,ValidatedForUpload
	,DelayReasonComments
	,TrackingNotes
	,AllTrackingNotes
	)
select
	 UniqueRecordId = TREATMENT_ID
	,ReferralUniqueRecordId = CARE_ID
	,PatientPathwayId = PATHWAY_ID
	,DecisionToTreatDate = DECISION_DATE
	,DTTOrganisationCode = case when ORG_CODE_DTT = '' then null else ORG_CODE_DTT end
	,FirstDefinitiveTreatmentDate = START_DATE
	,InitialTreatmentCode = TREATMENT
	,TreatmentOrganisationCode = ORG_CODE
	,TreatmentEventTypeCode = TREATMENT_EVENT
	,TreatmentSettingCode = TREATMENT_SETTING
	,PriorityCode = RT_PRIORITY
	,TreatmentIntentCode= RT_INTENT
	,SupportTypeCode = SPECIALIST
	,ClinicalTrial = TRIAL
	,WaitingTimeAdjustment = ADJ_DAYS
	,AdjustmentReasonCode = ADJ_CODE
	,DelayReasonCode = DELAY_CODE
	,TreatmentNo = TREAT_NO
	,TreatmentNoId = TREAT_ID
	,ValidatedForUpload = VALIDATED
	,DelayReasonComments = DELAY_COMMENTS
	,TrackingNotes = COMMENTS
	,AllTrackingNotes = ALL_COMMENTS
from
	[$(CancerRegister)].dbo.tblDEFINITIVE_TREATMENT


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

﻿CREATE TABLE [PCC].[HighCostDrug] (
    [HighCostDrugRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [PeriodID]             INT          NOT NULL,
    [AdmissionDate]        DATETIME     NOT NULL,
    [HighCostDrugDate]     DATE         NOT NULL,
    [HighCostDrugCode]     VARCHAR (20) NOT NULL,
    [HighCostDrugSequence] INT          NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL,
    CONSTRAINT [PK_PCCHighCostDrug] PRIMARY KEY CLUSTERED ([PeriodID] ASC, [HighCostDrugDate] ASC, [HighCostDrugCode] ASC)
);


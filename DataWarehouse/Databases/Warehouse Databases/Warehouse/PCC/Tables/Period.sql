﻿CREATE TABLE [PCC].[Period] (
    [PeriodRecno]                 INT           IDENTITY (1, 1) NOT NULL,
    [PeriodID]                    INT           NOT NULL,
    [NHSNumber]                   VARCHAR (20)  NULL,
    [CasenoteNo]                  VARCHAR (20)  NULL,
    [DateOfBirth]                 DATE          NULL,
    [AdmissionTime]               DATETIME2 (0) NULL,
    [DischargeTime]               DATETIME2 (0) NULL,
    [UnitFunctionCode]            VARCHAR (2)   NULL,
    [IsolationReasonCode]         VARCHAR (20)  NULL,
    [PrimaryDiagnosisReadCode]    VARCHAR (10)  NULL,
    [SecondaryDiagnosisReadCode1] VARCHAR (10)  NULL,
    [SecondaryDiagnosisReadCode2] VARCHAR (10)  NULL,
    [SecondaryDiagnosisReadCode3] VARCHAR (10)  NULL,
    [Created]                     DATETIME2 (3) NULL,
    [Updated]                     DATETIME2 (3) NULL,
    [ByWhom]                      VARCHAR (50)  NULL,
    CONSTRAINT [PK_PCCPeriod] PRIMARY KEY CLUSTERED ([PeriodID] ASC)
);


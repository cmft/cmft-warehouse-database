﻿CREATE TABLE [PCC].[Activity] (
    [ActivityRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [PeriodID]         INT          NOT NULL,
    [AdmissionDate]    DATE         NOT NULL,
    [ActivityDate]     DATE         NOT NULL,
    [ActivityCode]     VARCHAR (20) NOT NULL,
    [ActivitySequence] INT          NULL,
    [Created]          DATETIME     NULL,
    [Updated]          DATETIME     NULL,
    [ByWhom]           VARCHAR (50) NULL,
    CONSTRAINT [PK_PCCActivity] PRIMARY KEY CLUSTERED ([PeriodID] ASC, [ActivityDate] ASC, [ActivityCode] ASC)
);


﻿
CREATE proc [WH].[BuildDayLightSaving]

as

declare @Date datetime = '1 jan 1980'
declare @WorkingYear varchar(4)
declare @StartDay varchar(2)
declare @EndDay varchar(2)

declare @BSTPeriodStartTime datetime
declare @BSTPeriodEndTime datetime
declare @GMTPeriod1StartTime datetime
declare @GMTPeriod1EndTime datetime
declare @GMTPeriod2StartTime datetime
declare @GMTPeriod2EndTime datetime
declare @LocalDate datetime
declare @Limit datetime = dateadd(year, 25, getdate())

truncate table WH.DayLightSaving

while @Date < @Limit

begin

set @StartDay = convert(varchar(2),(31 - (5 * Year(@Date)/4 + 4) % 7))
set @EndDay = convert(varchar(2),(31 - (5 * Year(@Date)/4 + 1) % 7))
set @WorkingYear = convert(varchar(4),Year(@Date))

set @BSTPeriodStartTime = convert(datetime,@WorkingYear + '03' + @StartDay + ' 01:00:00',20)
set @BSTPeriodEndTime = convert(datetime,@WorkingYear + '10' + @EndDay + ' 00:01:00',20)	

set @GMTPeriod1StartTime = convert(datetime,@WorkingYear + '01' + '01' ,20)
set @GMTPeriod1EndTime = dateadd(second, -1, @BSTPeriodStartTime)

set @GMTPeriod2StartTime = dateadd(second, 1, @BSTPeriodEndTime)
set @GMTPeriod2EndTime = convert(datetime,@WorkingYear + '12' + '31' + ' 23:59:59',20)

insert into WH.DayLightSaving
 (
	[Year]
	,[StartTime]
	,[EndTime]
	,[Offset]
)
	select
		@WorkingYear
		,@GMTPeriod1StartTime
		,@GMTPeriod1EndTime
		,Offset = 0

	union

	select
		@WorkingYear
		,@BSTPeriodStartTime
		,@BSTPeriodEndTime
		,Offset = 1

	union

	select
		@WorkingYear
		,@GMTPeriod2StartTime
		,@GMTPeriod2EndTime
		,Offset = 0

	set @Date = dateadd(year, 1, @Date)
end


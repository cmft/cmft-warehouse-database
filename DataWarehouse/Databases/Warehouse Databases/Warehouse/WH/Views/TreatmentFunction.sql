﻿
Create View   WH.TreatmentFunction

as

Select
	SpecialtyCode
	,Specialty
from
	WH.TreatmentFunctionBase
where
	Specialty not in 
		(
		'not a Treatment Function'
		,'Retired'
		)

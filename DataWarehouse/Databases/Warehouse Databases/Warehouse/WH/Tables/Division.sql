﻿CREATE TABLE [WH].[Division] (
    [DivisionCode] VARCHAR (5)  NOT NULL,
    [Division]     VARCHAR (50) NULL,
    CONSTRAINT [PK_WH_Division] PRIMARY KEY CLUSTERED ([DivisionCode] ASC)
);


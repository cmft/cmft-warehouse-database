﻿CREATE TABLE [WH].[SourceOfReferral] (
    [SourceOfReferralCode] VARCHAR (10)  NOT NULL,
    [SourceOfReferral]     VARCHAR (255) NULL,
    CONSTRAINT [PK_SourceOfReferral] PRIMARY KEY CLUSTERED ([SourceOfReferralCode] ASC)
);


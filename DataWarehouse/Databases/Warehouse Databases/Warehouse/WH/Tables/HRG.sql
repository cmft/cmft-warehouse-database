﻿CREATE TABLE [WH].[HRG] (
    [HRGCode]    VARCHAR (10)  NOT NULL,
    [HRG]        VARCHAR (255) NULL,
    [WithCCCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_HRG] PRIMARY KEY CLUSTERED ([HRGCode] ASC)
);


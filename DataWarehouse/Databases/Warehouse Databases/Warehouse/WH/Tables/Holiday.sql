﻿CREATE TABLE [WH].[Holiday] (
    [HolidayDateID] INT  IDENTITY (1, 1) NOT NULL,
    [HolidayDate]   DATE NULL,
    CONSTRAINT [PK_Holiday] PRIMARY KEY CLUSTERED ([HolidayDateID] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Holiday_HolidayDate]
    ON [WH].[Holiday]([HolidayDate] ASC);


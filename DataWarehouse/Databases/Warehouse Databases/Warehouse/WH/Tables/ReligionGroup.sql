﻿CREATE TABLE [WH].[ReligionGroup] (
    [ReligionGroupCode] CHAR (1)     NOT NULL,
    [ReligionGroup]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([ReligionGroupCode] ASC)
);


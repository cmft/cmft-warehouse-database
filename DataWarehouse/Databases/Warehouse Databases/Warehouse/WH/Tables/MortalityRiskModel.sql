﻿CREATE TABLE [WH].[MortalityRiskModel] (
    [IndicatorCode]      VARCHAR (10) NULL,
    [DiagnosisGroupCode] INT          NOT NULL,
    [Predictor]          VARCHAR (50) NOT NULL,
    [ParameterEstimate]  FLOAT (53)   NULL,
    [StandardError]      FLOAT (53)   NULL,
    [WaldChiSquare]      FLOAT (53)   NULL,
    [WaldDf]             FLOAT (53)   NULL,
    [WaldPValue]         FLOAT (53)   NULL,
    [PublicationDate]    DATE         NULL,
    [FromDate]           DATE         NOT NULL,
    [ToDate]             DATE         NULL,
    CONSTRAINT [PK_MortalityRiskModel] PRIMARY KEY CLUSTERED ([DiagnosisGroupCode] ASC, [Predictor] ASC, [FromDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MRM-P]
    ON [WH].[MortalityRiskModel]([Predictor] ASC)
    INCLUDE([DiagnosisGroupCode], [ParameterEstimate], [FromDate], [ToDate]);


﻿CREATE TABLE [WH].[Specialty1] (
    [SpecialtyCode]       VARCHAR (10) NOT NULL,
    [Specialty]           VARCHAR (50) NULL,
    [SpecialtyParentCode] VARCHAR (50) NULL,
    [DirectorateCode]     VARCHAR (5)  NULL,
    CONSTRAINT [PK_WH_Specialty1] PRIMARY KEY NONCLUSTERED ([SpecialtyCode] ASC),
    CONSTRAINT [FK_Specialty_Directorate] FOREIGN KEY ([DirectorateCode]) REFERENCES [WH].[Directorate] ([DirectorateCode])
);


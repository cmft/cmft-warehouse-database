﻿CREATE TABLE [WH].[AdministrativeCategory] (
    [AdministrativeCategoryCode] VARCHAR (2)   NOT NULL,
    [AdministrativeCategory]     VARCHAR (255) NULL,
    CONSTRAINT [PK_AdministrativeCategory_1] PRIMARY KEY CLUSTERED ([AdministrativeCategoryCode] ASC)
);


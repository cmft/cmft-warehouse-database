﻿CREATE TABLE [WH].[Religion] (
    [ReligionCode]      VARCHAR (10) NOT NULL,
    [Religion]          VARCHAR (50) NULL,
    [ReligionGroupCode] CHAR (1)     NULL,
    PRIMARY KEY CLUSTERED ([ReligionCode] ASC)
);


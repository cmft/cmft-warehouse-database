﻿CREATE TABLE [WH].[SessionPeriod] (
    [SessionPeriodCode] VARCHAR (50)  NOT NULL,
    [SessionPeriod]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_SessionPeriod] PRIMARY KEY CLUSTERED ([SessionPeriodCode] ASC)
);


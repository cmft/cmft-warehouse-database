﻿CREATE TABLE [WH].[PatientFlag] (
    [DistrictNo] VARCHAR (14)  NOT NULL,
    [FlagCode]   VARCHAR (4)   NOT NULL,
    [Created]    DATETIME      NOT NULL,
    [Updated]    DATETIME      NOT NULL,
    [ByWhom]     VARCHAR (128) NOT NULL,
    CONSTRAINT [PK_PatientFlag] PRIMARY KEY CLUSTERED ([DistrictNo] ASC, [FlagCode] ASC)
);


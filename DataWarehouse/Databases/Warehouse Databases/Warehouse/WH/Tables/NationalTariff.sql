﻿CREATE TABLE [WH].[NationalTariff] (
    [HRGCode]                                                         NCHAR (5)     NOT NULL,
    [OutpatientProcedureTariff]                                       FLOAT (53)    NULL,
    [CombinedDaycaseOrElectiveTariff]                                 FLOAT (53)    NULL,
    [DaycaseTariff]                                                   FLOAT (53)    NULL,
    [ElectiveSpellTariff]                                             FLOAT (53)    NULL,
    [ElectiveLongStayTrimpoint]                                       FLOAT (53)    NULL,
    [NonElectiveSpellTariff]                                          FLOAT (53)    NULL,
    [NonElectiveLongStayTrimpoint]                                    FLOAT (53)    NULL,
    [PerDayLongStayPayment]                                           FLOAT (53)    NULL,
    [IsReducedShortStayEmergencyTariffApplicable]                     NVARCHAR (5)  NULL,
    [PercentageAppliedInCalculationOfReducedShortStayEmergencyTariff] NVARCHAR (10) NULL,
    [ReducedShortStayEmergencyTariff]                                 FLOAT (53)    NULL,
    [IsEligibleForSpecialistTopUp]                                    NVARCHAR (5)  NULL,
    [EffectiveFrom]                                                   DATE          NOT NULL,
    [EffectiveTo]                                                     DATE          NULL
);


﻿CREATE TABLE [WH].[CharlsonIndexBase] (
    [DiagnosisCode]         VARCHAR (10) NOT NULL,
    [CharlsonConditionCode] INT          NOT NULL,
    [CharlsonCondition]     VARCHAR (50) NULL,
    [NewWeight]             INT          NOT NULL,
    [OldWeight]             INT          NOT NULL,
    CONSTRAINT [PK__Charlson__A43A221910C31754] PRIMARY KEY CLUSTERED ([DiagnosisCode] ASC)
);


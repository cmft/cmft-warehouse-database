﻿CREATE TABLE [WH].[Site] (
    [SiteCode]     VARCHAR (10) NOT NULL,
    [Site]         VARCHAR (50) NULL,
    [Colour]       VARCHAR (50) NULL,
    [Sequence]     INT          NULL,
    [Abbreviation] CHAR (1)     NULL,
    CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED ([SiteCode] ASC)
);


﻿CREATE TABLE [WH].[DivisionRuleBase] (
    [DivisionRuleBaseRecno] INT           IDENTITY (1, 1) NOT NULL,
    [SiteCode]              VARCHAR (10)  NULL,
    [PatientCategoryCode]   VARCHAR (2)   NULL,
    [ConsultantCode]        VARCHAR (10)  NULL,
    [WardCode]              VARCHAR (10)  NULL,
    [SpecialtyCode]         VARCHAR (10)  NULL,
    [ClinicCode]            VARCHAR (10)  NULL,
    [FromDate]              SMALLDATETIME NULL,
    [ToDate]                SMALLDATETIME NULL,
    [DirectorateCode]       VARCHAR (5)   NULL,
    [DivisionCode]          VARCHAR (5)   NULL
);


﻿CREATE TABLE [WH].[TreatmentFunctionBase] (
    [SpecialtyCode]       VARCHAR (10)  NOT NULL,
    [Specialty]           VARCHAR (100) NULL,
    [SpecialtyParentCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_WH_TreatmentFunction] PRIMARY KEY NONCLUSTERED ([SpecialtyCode] ASC)
);


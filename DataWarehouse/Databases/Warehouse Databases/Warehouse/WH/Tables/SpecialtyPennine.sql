﻿CREATE TABLE [WH].[SpecialtyPennine] (
    [SpecialtyCode]         VARCHAR (5)  NOT NULL,
    [Specialty]             VARCHAR (50) NULL,
    [NationalSpecialtyCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_WH_SpecialtyPennine] PRIMARY KEY NONCLUSTERED ([SpecialtyCode] ASC),
    CONSTRAINT [FK_SpecialtyPennine_Specialty] FOREIGN KEY ([NationalSpecialtyCode]) REFERENCES [WH].[Specialty1] ([SpecialtyCode])
);


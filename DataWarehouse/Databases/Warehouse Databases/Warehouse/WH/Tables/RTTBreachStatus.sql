﻿CREATE TABLE [WH].[RTTBreachStatus] (
    [RTTBreachStatusCode] VARCHAR (10) NOT NULL,
    [RTTBreachStatus]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_RTTBreachStatus] PRIMARY KEY CLUSTERED ([RTTBreachStatusCode] ASC)
);


﻿CREATE TABLE [WH].[Specialty] (
    [SpecialtyCode]       VARCHAR (10)  NOT NULL,
    [Specialty]           VARCHAR (100) NULL,
    [SpecialtyParentCode] VARCHAR (10)  NULL,
    [DirectorateCode]     VARCHAR (5)   NULL,
    CONSTRAINT [PK_WH_Specialty] PRIMARY KEY NONCLUSTERED ([SpecialtyCode] ASC)
);


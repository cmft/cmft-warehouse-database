﻿CREATE TABLE [WH].[DayLightSaving] (
    [Year]      VARCHAR (4) NOT NULL,
    [StartTime] DATETIME    NOT NULL,
    [EndTime]   DATETIME    NOT NULL,
    [Offset]    INT         NOT NULL,
    CONSTRAINT [PK_DayLightSaving] PRIMARY KEY CLUSTERED ([StartTime] ASC, [EndTime] ASC)
);


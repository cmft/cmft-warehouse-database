﻿CREATE TABLE [WH].[DischargeDestination] (
    [DischargeDestinationCode] VARCHAR (2)   NOT NULL,
    [DischargeDestination]     VARCHAR (255) NULL,
    CONSTRAINT [PK_DischargeDestination_1] PRIMARY KEY CLUSTERED ([DischargeDestinationCode] ASC)
);


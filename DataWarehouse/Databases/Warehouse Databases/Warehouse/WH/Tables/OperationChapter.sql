﻿CREATE TABLE [WH].[OperationChapter] (
    [OperationChapterCode] VARCHAR (10)  NOT NULL,
    [OperationChapter]     VARCHAR (100) NULL,
    CONSTRAINT [PK_OperationChapter] PRIMARY KEY CLUSTERED ([OperationChapterCode] ASC)
);


﻿CREATE TABLE [WH].[Priority] (
    [PriorityCode] VARCHAR (5)  NOT NULL,
    [Priority]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Priority] PRIMARY KEY NONCLUSTERED ([PriorityCode] ASC)
);


﻿CREATE TABLE [WH].[TimeBand] (
    [TimeBandCode]      INT          NOT NULL,
    [MinuteBand]        VARCHAR (5)  NOT NULL,
    [FiveMinuteBand]    VARCHAR (13) NOT NULL,
    [FifteenMinuteBand] VARCHAR (13) NOT NULL,
    [ThirtyMinuteBand]  VARCHAR (13) NOT NULL,
    [HourBand]          VARCHAR (13) NOT NULL,
    CONSTRAINT [PK_TimeBand] PRIMARY KEY CLUSTERED ([TimeBandCode] ASC)
);


﻿CREATE TABLE [WH].[AdmissionMethod] (
    [AdmissionMethodCode] VARCHAR (2)   NOT NULL,
    [AdmissionMethod]     VARCHAR (255) NULL,
    CONSTRAINT [PK_AdmissionMethod_1] PRIMARY KEY CLUSTERED ([AdmissionMethodCode] ASC)
);


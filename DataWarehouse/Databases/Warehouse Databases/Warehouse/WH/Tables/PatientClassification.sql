﻿CREATE TABLE [WH].[PatientClassification] (
    [PatientClassificationCode] VARCHAR (2)   NOT NULL,
    [PatientClassification]     VARCHAR (255) NULL,
    CONSTRAINT [PK_PatientClassification_1] PRIMARY KEY CLUSTERED ([PatientClassificationCode] ASC)
);


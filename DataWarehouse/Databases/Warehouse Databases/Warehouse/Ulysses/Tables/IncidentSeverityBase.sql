﻿CREATE TABLE [Ulysses].[IncidentSeverityBase] (
    [CODE]       VARCHAR (2)    NULL,
    [BinaryCode] VARBINARY (50) NOT NULL,
    [DESCRIPT]   VARCHAR (50)   NULL,
    [SCORE]      NUMERIC (3)    NULL,
    [ABBREVIATE] VARCHAR (10)   NULL,
    [INACTIVE]   BIT            NULL,
    [MATRIX]     VARCHAR (2)    NULL,
    [ABBREVIAT2] VARCHAR (10)   NULL,
    [ABBREVIAT3] VARCHAR (10)   NULL,
    CONSTRAINT [PK__Severity__341AD4384EBF7E76] PRIMARY KEY CLUSTERED ([BinaryCode] ASC)
);


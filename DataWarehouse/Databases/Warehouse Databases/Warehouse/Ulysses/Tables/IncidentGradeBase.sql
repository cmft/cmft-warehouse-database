﻿CREATE TABLE [Ulysses].[IncidentGradeBase] (
    [CODE]       VARCHAR (2)    NULL,
    [BinaryCode] VARBINARY (50) NOT NULL,
    [DESCRIPT]   VARCHAR (50)   NULL,
    [ABBREVIATE] VARCHAR (10)   NULL,
    [INACTIVE]   BIT            NULL,
    [COLOUR]     NUMERIC (10)   NULL,
    [NPSA2]      VARCHAR (10)   NULL,
    [NPSA3]      VARCHAR (10)   NULL,
    [NPSA4]      VARCHAR (10)   NULL,
    [ABBREVIAT2] VARCHAR (10)   NULL,
    [ABBREVIAT3] VARCHAR (10)   NULL,
    [INFORM_REL] BIT            NULL,
    CONSTRAINT [PK__GradeBas__341AD43876635B7C] PRIMARY KEY CLUSTERED ([BinaryCode] ASC)
);


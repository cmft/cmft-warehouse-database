﻿CREATE TABLE [Ulysses].[IncidentEntityTypeBase] (
    [EntityTypeCode] INT          NOT NULL,
    [EntityType]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK__EntityTy__3EA56DEC35BEC682] PRIMARY KEY CLUSTERED ([EntityTypeCode] ASC)
);


﻿CREATE TABLE [Ulysses].[ComplaintCategoryTypeBase] (
    [CODE]       VARCHAR (2)    NULL,
    [BinaryCode] VARBINARY (50) NOT NULL,
    [DESCRIPT]   VARCHAR (50)   NULL,
    [KO41]       VARCHAR (2)    NULL,
    [INACTIVE]   BIT            NULL,
    [ABBREVIATE] VARCHAR (10)   NULL,
    [KO41B]      VARCHAR (2)    NULL,
    [ABBREVIAT2] VARCHAR (10)   NULL,
    [ABBREVIAT3] VARCHAR (10)   NULL,
    CONSTRAINT [PK__Complain__341AD43859E7185F] PRIMARY KEY CLUSTERED ([BinaryCode] ASC)
);


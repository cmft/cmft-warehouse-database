﻿CREATE TABLE [Ulysses].[IncidentTypeBase] (
    [CODE]       VARCHAR (2)    NULL,
    [BinaryCode] VARBINARY (50) NOT NULL,
    [DESCRIPT]   VARCHAR (50)   NULL,
    [INACTIVE]   BIT            NULL,
    [ABBREVIATE] VARCHAR (10)   NULL,
    [ABBREVIAT2] VARCHAR (10)   NULL,
    [ABBREVIAT3] VARCHAR (10)   NULL,
    CONSTRAINT [PK__Incident__341AD438434DCBCA] PRIMARY KEY CLUSTERED ([BinaryCode] ASC)
);


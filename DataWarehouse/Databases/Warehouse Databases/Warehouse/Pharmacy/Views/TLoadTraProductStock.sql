﻿

CREATE view [Pharmacy].[TLoadTraProductStock] as

select
	 InterfaceCode = 'PHAT'
	,ProductStockCode = case when RTRIM(ProductStock.ProductStockID) = '' then null else ProductStock.ProductStockID end
	,DrugID = case when RTRIM(ProductStock.DrugID) = '' then null else ProductStock.DrugID end
	,InternalSiteCode = case when RTRIM(ProductStock.LocationID_Site) = '' then null else ProductStock.LocationID_Site end
	,SiteCode = case when RTRIM(Site.SiteNumber) = '' then null else Site.SiteNumber end
	,AnnualUse = case when RTRIM(ProductStock.anuse) = '' then null else ProductStock.anuse end
	,ReorderLevelUnits = case when RTRIM(ProductStock.Reorderlvl) = '' then null else ProductStock.Reorderlvl end
	,ReorderQuantity = case when RTRIM(ProductStock.ReorderQty) = '' then null else ProductStock.ReorderQty end
	,StockLevel = case when RTRIM(ProductStock.stocklvl) = '' then null else ProductStock.stocklvl end
	,ArchiveFlag = case when RTRIM(ProductStock.inuse) = 'N' then 1 else 0 end
	,Formulary = case when RTRIM(ProductStock.formulary) = '' then null else ProductStock.formulary end
	,OrderCycle = case when RTRIM(ProductStock.ordercycle) = '' then null else ProductStock.ordercycle end
	,SupplierCode = case when RTRIM(ProductStock.supcode) = '' then null else ProductStock.supcode end
	,LastOrderedDateText = case when RTRIM(ProductStock.lastordered) = '' then null else ProductStock.lastordered end
	,StockTakeStatus = case when RTRIM(ProductStock.stocktakestatus) = '' then null else ProductStock.stocktakestatus end
	,IssuePrice = case when RTRIM(ProductStock.cost) = '' then null else ProductStock.cost end
	,LossesGains = case when RTRIM(ProductStock.lossesgains) = '' then null else ProductStock.lossesgains / 100.0 end
	,LocalCode = case when RTRIM(ProductStock.local) = '' then null else ProductStock.local end
	,BatchTracking = case when RTRIM(ProductStock.batchtracking) = '' then null else ProductStock.batchtracking end
	,LastIssuedDate = case when RTRIM(ProductStock.lastissued) = '' then null else ProductStock.lastissued end
	,LastStocktakeDate = case when RTRIM(ProductStock.laststocktakedate) = '' then null else ProductStock.laststocktakedate end
	,DrugSubGroupCode = case when RTRIM(ProductStock.ledcode) = '' then null else ProductStock.ledcode end
	,DrugGroupCode = case when RTRIM(ProductStock.Therapcode) = '' then null else ProductStock.Therapcode end
	,UseThisPeriodUnits = case when RTRIM(ProductStock.usethisperiod) = '' then null else ProductStock.usethisperiod end
	,LastPeriodEndDate = case when RTRIM(ProductStock.datelastperiodend) = '' then null else ProductStock.datelastperiodend end
	,PrimaryLocationCode = case when RTRIM(ProductStock.loccode) = '' then null else ProductStock.loccode end
	,SecondaryLocationCode = case when RTRIM(ProductStock.loccode2) = '' then null else ProductStock.loccode2 end
	,NSVCode = SiteProductData.siscode
	,ExtraMessage = case when RTRIM(ProductStock.message) = '' then null else ProductStock.message end
	,Stocked = case when RTRIM(ProductStock.sisstock) = '' then null else ProductStock.sisstock end
	,DefinedDailyDoseValue = case when RTRIM(ProductStock.DDDValue) = '' then null else ProductStock.DDDValue end
	,DefinedDailyDoseUnits = case when RTRIM(ProductStock.DDDUnits) = '' then null else ProductStock.DDDUnits end
	,UserField1 = case when RTRIM(ProductStock.UserField1) = '' then null else ProductStock.UserField1 end
	,UserField2 = case when RTRIM(ProductStock.UserField2) = '' then null else ProductStock.UserField2 end
	,UserField3 = case when RTRIM(ProductStock.UserField3) = '' then null else ProductStock.UserField3 end
	,PIPCode = case when RTRIM(ProductStock.PIPCode) = '' then null else ProductStock.PIPCode end
	,MasterPIPCode = case when RTRIM(ProductStock.MasterPIP) = '' then null else ProductStock.MasterPIP end
	,HighCostProduct = case when RTRIM(ProductStock.HIProduct) = '' then null else ProductStock.HIProduct end
	,EDILinkCode = case when RTRIM(ProductStock.EDILinkCode) = '' then null else ProductStock.EDILinkCode end
	,OuterPackSize = case when RTRIM(SupplierProfile.ReorderPckSize) = '' then null else SupplierProfile.ReorderPckSize end
	,SiteProductCode  = case when RTRIM(SiteProductData.SiteProductDataID) = '' then null else SiteProductData.SiteProductDataID end
from
	[$(Pharmacy_Trafford)].icwsys.ProductStock

inner join [$(Pharmacy_Trafford)].icwsys.Site
on	Site.LocationID = ProductStock.LocationID_Site

inner join [$(Pharmacy_Trafford)].icwsys.SiteProductData
on	SiteProductData.DrugID = ProductStock.DrugID
and	SiteProductData.DSSMasterSiteID <> 0
and	not exists
	(
	select
		1
	from
		[$(Pharmacy_Trafford)].icwsys.SiteProductData Previous
	where
		Previous.DrugID = ProductStock.DrugID
	and	Previous.DSSMasterSiteID <> 0
	and	Previous.SiteProductDataID > SiteProductData.SiteProductDataID	
	)

inner join [$(Pharmacy_Trafford)].icwsys.WSupplierProfile SupplierProfile
on	SupplierProfile.NSVCode = SiteProductData.siscode
and	SupplierProfile.SupCode = ProductStock.supcode
and	SupplierProfile.LocationID_Site = ProductStock.LocationID_Site



﻿
CREATE view [Pharmacy].[TLoadOrderCensus] as

select
	 InterfaceCode
	,CensusDate
	,OrderID
	,SiteCode
	,NSVCode
	,OutstandingOrder
	,OutstandingOrderValueNet
	,OutstandingOrderValueGross
	,OrderTime
	,LocationCode
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderNumber
	,PackCostNet
	,PackCostGross
	,PickNo
	,ReceivedPacks
	,ReceivedDate
	,OrderedPacks
	,UrgencyCode
	,ToFollow
	,InternalSiteNo
	,InternalMethodCode
	,PFlag
	,CreatedUser
	,CustomerOrderNo
	,InDispute
	,InDisputeUser
	,ShelfPrinted
	,TaxRate
from
	Pharmacy.TImportOrderCensus











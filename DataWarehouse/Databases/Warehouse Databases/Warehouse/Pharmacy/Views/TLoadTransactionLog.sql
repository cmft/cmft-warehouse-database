﻿






CREATE view [Pharmacy].[TLoadTransactionLog] as

select
	 TransactionLogID = WTranslogID
	,RevisionLevel
	,SourcePatientNo = case when ltrim(PatId) = '' then null else PatId end
	,CasenoteNo = case when ltrim(CaseNo) = '' then null else CaseNo end
	,NSVCode = SisCode
	,PackSize = cast(ConvFact as int)
	,IssueForm = IssueUnits
	,TerminalCode
	,Terminal
	--,Date
	--,Time
	,Quantity = Qty
	,Cost
	,CostExcludingTax = CostExTax
	,TaxCost
	,TaxCode
	,TaxRate
	,WardCode = WardCode
	,ConsultantCode
	,SpecialtyCode = SpecialtyCode
	--,Prescriber
	,IssueDirections = case when ltrim(DirCode) = '' then null else DirCode end
	,IssueKindCode = Kind
	,SiteCode = Site
	,IssueLabelTypeCode = LabelType
	--,Containers
	,EpisodeID = Episode
	--,EventNumber
	,PrescriptionNo = PrescriptionNum
	,BatchNo = case when ltrim(BatchNum) = '' then null else BatchNum end
	,BatchExpiry = case when ltrim(ExpiryDate) = '' then null else ExpiryDate end
	--,PPFlag
	,StockLevel = cast(StockLvl as float)
	,CustomerOrderNo = case when ltrim(CustOrdNo) = '' then null else CustOrdNo end
	--,CivasType
	,CIVASAmount = cast(case when ltrim(CivasAmount) = '' then null else CivasAmount end as float)
	,SiteID
	,CreatedByCode = EntityID
	,ProductID
	,TransactionLogTime = LogDateTime
	,PrescriptionRequestID = RequestID_Prescription
	,PrescriberEntityID = EntityID_Prescriber
	,DispensingRequestID = RequestID_Dispensing
	,StockValue
	,InterfaceCode
	,GPCode
from
	Pharmacy.TImportTransactionLog









﻿create view [Pharmacy].[TLoadReconciliationCensus] as

select
	 InterfaceCode
	,CensusDate
	,ReconciliationCode
	,SiteID
	,SupplierCode
	,OrderNumber
	,NSVCode
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,PackCost
	,ReceivedPacks
	,NetValue
	,GrossValue
	,LocationCode
	,InDispute
	,InDisputeUser
from
	Pharmacy.TImportReconciliationCensus












﻿CREATE TABLE [Pharmacy].[SystemUserGroup] (
    [SystemUserGroupCode] VARCHAR (10)  NOT NULL,
    [SystemUserGroup]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_SystemUserGroup] PRIMARY KEY CLUSTERED ([SystemUserGroupCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[Address] (
    [InterfaceCode]  VARCHAR (10) NOT NULL,
    [AddressCode]    INT          NOT NULL,
    [BoxNumber]      VARCHAR (30) NULL,
    [DoorNumber]     VARCHAR (10) NULL,
    [Street]         VARCHAR (50) NULL,
    [Town]           VARCHAR (50) NULL,
    [LocalAuthority] VARCHAR (50) NULL,
    [District]       VARCHAR (50) NULL,
    [Postcode]       VARCHAR (15) NULL,
    [Province]       VARCHAR (50) NULL,
    [Country]        VARCHAR (50) NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [AddressCode] ASC)
);


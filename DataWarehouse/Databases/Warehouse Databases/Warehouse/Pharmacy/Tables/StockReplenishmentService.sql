﻿CREATE TABLE [Pharmacy].[StockReplenishmentService] (
    [StockReplenishmentServiceCode] VARCHAR (10) NOT NULL,
    [StockReplenishmentService]     VARCHAR (50) NULL,
    CONSTRAINT [PK_StockReplenishmentService] PRIMARY KEY CLUSTERED ([StockReplenishmentServiceCode] ASC)
);


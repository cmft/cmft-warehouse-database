﻿CREATE TABLE [Pharmacy].[Patient] (
    [InterfaceCode]                 VARCHAR (10)  NOT NULL,
    [SourcePatientNo]               INT           NOT NULL,
    [Title]                         VARCHAR (128) NULL,
    [Initials]                      VARCHAR (10)  NULL,
    [Forename]                      VARCHAR (128) NULL,
    [Surname]                       VARCHAR (128) NULL,
    [DateOfBirth]                   DATETIME      NULL,
    [NHSNumber]                     VARCHAR (255) NULL,
    [NHSNumberValidationStatusCode] BIT           NULL,
    [SexCode]                       INT           NULL,
    [PatientRecno]                  INT           IDENTITY (1, 1) NOT NULL,
    [CasenoteNo]                    VARCHAR (255) NULL,
    [DistrictNumber]                VARCHAR (255) NULL,
    CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [SourcePatientNo] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Patient]
    ON [Pharmacy].[Patient]([PatientRecno] ASC);


﻿CREATE TABLE [Pharmacy].[DrugSubGroup] (
    [DrugSubGroupCode] VARCHAR (20) NOT NULL,
    [DrugSubGroup]     VARCHAR (50) NOT NULL,
    [CostTypeCode]     CHAR (1)     NOT NULL,
    CONSTRAINT [PK_DrugSubGroup] PRIMARY KEY CLUSTERED ([DrugSubGroupCode] ASC),
    CONSTRAINT [FK_DrugSubGroup_CostType] FOREIGN KEY ([CostTypeCode]) REFERENCES [Pharmacy].[CostType] ([CostTypeCode])
);


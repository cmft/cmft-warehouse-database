﻿CREATE TABLE [Pharmacy].[TaxRate] (
    [InterfaceCode] VARCHAR (10) NOT NULL,
    [TaxCode]       VARCHAR (1)  NOT NULL,
    [TaxRate]       FLOAT (53)   NULL,
    CONSTRAINT [PK_TaxRate] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [TaxCode] ASC)
);


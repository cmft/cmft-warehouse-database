﻿CREATE TABLE [Pharmacy].[Antibiotic] (
    [InterfaceCode]    VARCHAR (10)   NOT NULL,
    [NSVCode]          VARCHAR (7)    NOT NULL,
    [GenericName]      NVARCHAR (255) NULL,
    [DefinedDailyDose] FLOAT (53)     NULL,
    [Form]             NVARCHAR (255) NULL,
    CONSTRAINT [PK_Antibiotic] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [NSVCode] ASC)
);


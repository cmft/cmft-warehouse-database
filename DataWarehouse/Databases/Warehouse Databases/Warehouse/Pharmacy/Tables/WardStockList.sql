﻿CREATE TABLE [Pharmacy].[WardStockList] (
    [WardStockListRecno] INT          IDENTITY (1, 1) NOT NULL,
    [InterfaceCode]      VARCHAR (5)  NOT NULL,
    [WardStockListCode]  INT          NOT NULL,
    [SupplierCode]       INT          NULL,
    [NSVCode]            VARCHAR (7)  NULL,
    [TopUpLevel]         SMALLINT     NULL,
    [PrintLabel]         VARCHAR (1)  NULL,
    [SiteCode]           INT          NULL,
    [Barcode]            VARCHAR (15) NULL,
    CONSTRAINT [PK_WardStockList] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [WardStockListCode] ASC)
);


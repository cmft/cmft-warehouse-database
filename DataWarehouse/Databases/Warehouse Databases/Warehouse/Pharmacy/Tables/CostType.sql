﻿CREATE TABLE [Pharmacy].[CostType] (
    [CostTypeCode] CHAR (1)     NOT NULL,
    [CostType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_CostType] PRIMARY KEY CLUSTERED ([CostTypeCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[Directorate] (
    [DirectorateCode]       INT          NOT NULL,
    [Directorate]           VARCHAR (50) NULL,
    [SourceDirectorateCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_Directorate] PRIMARY KEY CLUSTERED ([DirectorateCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[Supplier] (
    [InterfaceCode]           VARCHAR (10)  NOT NULL,
    [SupplierCode]            INT           NOT NULL,
    [LocalSupplierCode]       VARCHAR (5)   NOT NULL,
    [ContractAddress]         VARCHAR (100) NULL,
    [SupplierAddress]         VARCHAR (100) NULL,
    [InvoiceAddress]          VARCHAR (100) NULL,
    [ContactTelephoneNo]      VARCHAR (14)  NULL,
    [SupplierTelephoneNo]     VARCHAR (14)  NULL,
    [InvoiceTelephoneNo]      VARCHAR (14)  NULL,
    [DiscountDetail]          VARCHAR (70)  NULL,
    [DiscountValue]           VARCHAR (9)   NULL,
    [OrderMethod]             VARCHAR (1)   NULL,
    [OrderMessage]            VARCHAR (50)  NULL,
    [AverageLeadTime]         VARCHAR (4)   NULL,
    [ContactFaxNo]            VARCHAR (14)  NULL,
    [SupplierFaxNo]           VARCHAR (14)  NULL,
    [InvoiceFaxNo]            VARCHAR (14)  NULL,
    [Supplier]                VARCHAR (15)  NULL,
    [PrintTradeName]          VARCHAR (1)   NULL,
    [PrintNSVCode]            VARCHAR (1)   NULL,
    [SupplierFullName]        VARCHAR (35)  NULL,
    [DiscountBelow]           VARCHAR (4)   NULL,
    [DiscountAbove]           VARCHAR (4)   NULL,
    [InternalAscribeSiteCode] VARCHAR (8)   NULL,
    [CostCentreCode]          VARCHAR (15)  NULL,
    [PrintDeliveryNote]       VARCHAR (1)   NULL,
    [PrintPickingTicket]      VARCHAR (1)   NULL,
    [SupplierTypeCode]        VARCHAR (1)   NULL,
    [OrderOutput]             VARCHAR (1)   NULL,
    [ReceiveGoods]            VARCHAR (1)   NULL,
    [TopupInterval]           VARCHAR (2)   NULL,
    [ATCSupplied]             VARCHAR (1)   NULL,
    [TopupDate]               VARCHAR (8)   NULL,
    [InUse]                   BIT           NULL,
    [WardCode]                VARCHAR (5)   NULL,
    [OnCostPercent]           VARCHAR (3)   NULL,
    [InPatientDirections]     VARCHAR (1)   NULL,
    [AdHocDeliveryNote]       VARCHAR (1)   NULL,
    [SiteID]                  INT           NULL,
    [MinimumOrderValue]       FLOAT (53)    NULL,
    [SupplierRecno]           INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED ([SupplierRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Supplier]
    ON [Pharmacy].[Supplier]([InterfaceCode] ASC, [SiteID] ASC, [LocalSupplierCode] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Supplier_1]
    ON [Pharmacy].[Supplier]([InterfaceCode] ASC, [SupplierCode] ASC);


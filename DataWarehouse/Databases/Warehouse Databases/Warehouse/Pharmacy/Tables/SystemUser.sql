﻿CREATE TABLE [Pharmacy].[SystemUser] (
    [InterfaceCode]       VARCHAR (10)  NOT NULL,
    [SystemUserCode]      INT           NOT NULL,
    [Surname]             VARCHAR (128) NOT NULL,
    [Forename]            VARCHAR (128) NULL,
    [Initials]            VARCHAR (10)  NULL,
    [Username]            VARCHAR (50)  NOT NULL,
    [SystemUserRecno]     INT           IDENTITY (1, 1) NOT NULL,
    [SystemUserGroupCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_SystemUser] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [SystemUserCode] ASC),
    CONSTRAINT [FK_SystemUser_SystemUserGroup] FOREIGN KEY ([SystemUserGroupCode]) REFERENCES [Pharmacy].[SystemUserGroup] ([SystemUserGroupCode])
);


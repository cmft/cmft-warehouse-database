﻿CREATE TABLE [Pharmacy].[Terminal] (
    [InterfaceCode]    VARCHAR (10) NOT NULL,
    [TerminalCode]     INT          NOT NULL,
    [Terminal]         VARCHAR (15) NULL,
    [TerminalTypeCode] INT          NOT NULL,
    [DomainName]       VARCHAR (15) NULL,
    [TerminalRecno]    INT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_Terminal] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [TerminalCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[Division] (
    [DivisionCode] INT          NOT NULL,
    [Division]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Division] PRIMARY KEY CLUSTERED ([DivisionCode] ASC)
);


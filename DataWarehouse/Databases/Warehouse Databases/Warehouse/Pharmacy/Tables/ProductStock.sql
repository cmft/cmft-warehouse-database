﻿CREATE TABLE [Pharmacy].[ProductStock] (
    [InterfaceCode]         VARCHAR (10)  NOT NULL,
    [ProductStockCode]      INT           NOT NULL,
    [DrugID]                INT           NULL,
    [InternalSiteCode]      INT           NULL,
    [SiteCode]              INT           NULL,
    [AnnualUse]             FLOAT (53)    NULL,
    [ReorderLevelUnits]     VARCHAR (8)   NULL,
    [ReorderQuantity]       VARCHAR (6)   NULL,
    [StockLevel]            FLOAT (53)    NULL,
    [ArchiveFlag]           BIT           NULL,
    [Formulary]             VARCHAR (1)   NULL,
    [OrderCycle]            VARCHAR (2)   NULL,
    [SupplierCode]          VARCHAR (5)   NULL,
    [LastOrderedDateText]   VARCHAR (8)   NULL,
    [StockTakeStatus]       VARCHAR (1)   NULL,
    [IssuePrice]            FLOAT (53)    NULL,
    [LossesGains]           FLOAT (53)    NULL,
    [LocalCode]             VARCHAR (20)  NULL,
    [BatchTracking]         VARCHAR (1)   NULL,
    [LastIssuedDate]        VARCHAR (8)   NULL,
    [LastStocktakeDate]     VARCHAR (8)   NULL,
    [DrugSubGroupCode]      VARCHAR (20)  NULL,
    [DrugGroupCode]         VARCHAR (2)   NULL,
    [UseThisPeriodUnits]    FLOAT (53)    NULL,
    [LastPeriodEndDate]     VARCHAR (8)   NULL,
    [PrimaryLocationCode]   VARCHAR (3)   NULL,
    [SecondaryLocationCode] VARCHAR (3)   NULL,
    [NSVCode]               VARCHAR (7)   NULL,
    [ExtraMessage]          VARCHAR (255) NULL,
    [Stocked]               VARCHAR (1)   NULL,
    [DefinedDailyDoseValue] VARCHAR (10)  NULL,
    [DefinedDailyDoseUnits] VARCHAR (10)  NULL,
    [UserField1]            VARCHAR (10)  NULL,
    [UserField2]            VARCHAR (10)  NULL,
    [UserField3]            VARCHAR (10)  NULL,
    [PIPCode]               VARCHAR (7)   NULL,
    [MasterPIPCode]         VARCHAR (7)   NULL,
    [HighCostProduct]       CHAR (1)      NULL,
    [EDILinkCode]           VARCHAR (13)  NULL,
    [OuterPackSize]         VARCHAR (5)   NULL,
    [SiteProductCode]       INT           NULL,
    [ProductStockRecno]     INT           IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_ProductStock] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [ProductStockCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ProductStock_2]
    ON [Pharmacy].[ProductStock]([DrugGroupCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ProductStock_1]
    ON [Pharmacy].[ProductStock]([DrugSubGroupCode] ASC);


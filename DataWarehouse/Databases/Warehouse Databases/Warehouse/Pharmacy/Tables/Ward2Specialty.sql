﻿CREATE TABLE [Pharmacy].[Ward2Specialty] (
    [WardName]  NVARCHAR (255) NULL,
    [Specialty] NVARCHAR (255) NULL,
    [Division]  NVARCHAR (255) NULL,
    [SpecCode]  NVARCHAR (255) NULL,
    [WardCode]  NVARCHAR (255) NULL
);


﻿CREATE TABLE [Pharmacy].[WardType] (
    [WardTypeCode] VARCHAR (10) NOT NULL,
    [WardType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_WardType] PRIMARY KEY CLUSTERED ([WardTypeCode] ASC)
);


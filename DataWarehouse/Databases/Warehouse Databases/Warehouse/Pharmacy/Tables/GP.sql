﻿CREATE TABLE [Pharmacy].[GP] (
    [InterfaceCode]          VARCHAR (10)   NULL,
    [GPCode]                 INT            NOT NULL,
    [GP]                     VARCHAR (128)  NULL,
    [GPClassificationID]     INT            NOT NULL,
    [GPClassification]       VARCHAR (50)   NULL,
    [GPClassificationDetail] VARCHAR (1024) NULL,
    [GPRegistrationNo]       VARCHAR (20)   NOT NULL,
    [Contract]               CHAR (1)       NOT NULL,
    [Obstetric]              BIT            NOT NULL,
    [JobShare]               BIT            NOT NULL,
    [Trainer]                BIT            NOT NULL,
    [GPRecno]                INT            IDENTITY (1, 1) NOT NULL
);


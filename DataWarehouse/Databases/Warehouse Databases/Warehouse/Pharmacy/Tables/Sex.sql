﻿CREATE TABLE [Pharmacy].[Sex] (
    [InterfaceCode] VARCHAR (10) NOT NULL,
    [SexCode]       INT          NOT NULL,
    [Sex]           VARCHAR (20) NOT NULL,
    CONSTRAINT [PK_Sex_1] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [SexCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[TImportReconciliationCensus] (
    [InterfaceCode]      VARCHAR (10) NOT NULL,
    [CensusDate]         DATETIME     NOT NULL,
    [ReconciliationCode] INT          NOT NULL,
    [SiteID]             INT          NOT NULL,
    [SupplierCode]       VARCHAR (5)  NULL,
    [OrderNumber]        INT          NULL,
    [NSVCode]            VARCHAR (7)  NULL,
    [OrderTime]          DATETIME     NULL,
    [ReceivedDate]       DATETIME     NULL,
    [OrderedPacks]       FLOAT (53)   NULL,
    [PackCost]           FLOAT (53)   NULL,
    [ReceivedPacks]      FLOAT (53)   NULL,
    [NetValue]           MONEY        NULL,
    [GrossValue]         MONEY        NULL,
    [LocationCode]       VARCHAR (3)  NULL,
    [InDispute]          VARCHAR (1)  NULL,
    [InDisputeUser]      VARCHAR (3)  NULL,
    CONSTRAINT [PK_TImportReconciliation] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [CensusDate] ASC, [ReconciliationCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[CostCentre] (
    [CostCentreCode] VARCHAR (20) NOT NULL,
    [CostCentre]     VARCHAR (50) NULL,
    CONSTRAINT [PK_CostCentre] PRIMARY KEY CLUSTERED ([CostCentreCode] ASC)
);


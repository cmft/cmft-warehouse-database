﻿CREATE TABLE [Pharmacy].[Budget] (
    [InterfaceCode] VARCHAR (10) NOT NULL,
    [BudgetDate]    DATE         NOT NULL,
    [BudgetAmount]  INT          NULL,
    CONSTRAINT [PK_Budget] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [BudgetDate] ASC)
);


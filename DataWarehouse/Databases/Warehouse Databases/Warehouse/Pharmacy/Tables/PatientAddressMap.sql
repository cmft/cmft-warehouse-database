﻿CREATE TABLE [Pharmacy].[PatientAddressMap] (
    [InterfaceCode]   VARCHAR (10) NOT NULL,
    [SourcePatientNo] INT          NOT NULL,
    [AddressCode]     INT          NOT NULL,
    [AddressTypeCode] INT          NOT NULL,
    CONSTRAINT [PK_PatientAddressMap] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [SourcePatientNo] ASC, [AddressCode] ASC, [AddressTypeCode] ASC)
);


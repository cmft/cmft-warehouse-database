﻿CREATE TABLE [Pharmacy].[Ward2SpecialtyTrafford] (
    [Interfacecode] VARCHAR (10)   NULL,
    [WardName]      VARCHAR (35)   NULL,
    [suppliercode]  INT            NULL,
    [Specialty]     VARCHAR (128)  NULL,
    [Specialtycode] INT            NULL,
    [Division]      VARCHAR (50)   NULL,
    [WardCode]      NVARCHAR (255) NULL,
    [SpecCode]      NVARCHAR (255) NULL
);


﻿CREATE TABLE [Pharmacy].[IssueTransactionType] (
    [IssueTransactionTypeCode] VARCHAR (1)  NOT NULL,
    [IssueTransactionType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_IssueKind] PRIMARY KEY CLUSTERED ([IssueTransactionTypeCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[Site] (
    [InterfaceCode] VARCHAR (10)  NOT NULL,
    [SiteCode]      INT           NOT NULL,
    [Site]          VARCHAR (128) NOT NULL,
    [SiteRecno]     INT           IDENTITY (1, 1) NOT NULL,
    [SiteID]        INT           NULL,
    CONSTRAINT [PK_Site_1] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [SiteCode] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Site]
    ON [Pharmacy].[Site]([InterfaceCode] ASC, [SiteID] ASC);


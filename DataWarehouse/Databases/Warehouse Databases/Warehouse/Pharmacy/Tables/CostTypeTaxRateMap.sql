﻿CREATE TABLE [Pharmacy].[CostTypeTaxRateMap] (
    [CostTypeCode]  CHAR (1)     NOT NULL,
    [InterfaceCode] VARCHAR (10) NOT NULL,
    [TaxCode]       VARCHAR (1)  NOT NULL,
    CONSTRAINT [PK_CostTypeTaxRateMap] PRIMARY KEY CLUSTERED ([CostTypeCode] ASC, [InterfaceCode] ASC),
    CONSTRAINT [FK_CostTypeTaxRateMap_CostType] FOREIGN KEY ([CostTypeCode]) REFERENCES [Pharmacy].[CostType] ([CostTypeCode]),
    CONSTRAINT [FK_CostTypeTaxRateMap_TaxRate] FOREIGN KEY ([InterfaceCode], [TaxCode]) REFERENCES [Pharmacy].[TaxRate] ([InterfaceCode], [TaxCode])
);


﻿CREATE TABLE [Pharmacy].[Specialty] (
    [InterfaceCode]         VARCHAR (10)   NOT NULL,
    [SpecialtyCode]         INT            NOT NULL,
    [Specialty]             VARCHAR (128)  NOT NULL,
    [Detail]                VARCHAR (1024) NOT NULL,
    [LocalSpecialtyCode]    VARCHAR (255)  NULL,
    [NationalSpecialtyCode] VARCHAR (255)  NULL,
    [ArchiveFlag]           BIT            NOT NULL,
    [DirectorateCode]       INT            NULL,
    [DivisionCode]          INT            NULL,
    [SpecialtyRecno]        INT            IDENTITY (1, 1) NOT NULL,
    [TrustCode]             INT            NULL,
    [CostCentreCode]        VARCHAR (20)   NULL,
    CONSTRAINT [PK_Specialty_1] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [SpecialtyCode] ASC),
    CONSTRAINT [FK_Specialty_CostCentre] FOREIGN KEY ([CostCentreCode]) REFERENCES [Pharmacy].[CostCentre] ([CostCentreCode]),
    CONSTRAINT [FK_Specialty_Directorate] FOREIGN KEY ([DirectorateCode]) REFERENCES [Pharmacy].[Directorate] ([DirectorateCode]),
    CONSTRAINT [FK_Specialty_Division] FOREIGN KEY ([DivisionCode]) REFERENCES [Pharmacy].[Division] ([DivisionCode])
);


﻿CREATE TABLE [Pharmacy].[Trust] (
    [TrustCode] INT          NOT NULL,
    [Trust]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Trust] PRIMARY KEY CLUSTERED ([TrustCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[Consultant] (
    [InterfaceCode]   VARCHAR (10)  NOT NULL,
    [ConsultantCode]  INT           NOT NULL,
    [Surname]         VARCHAR (128) NULL,
    [Forename]        VARCHAR (128) NULL,
    [Initials]        VARCHAR (10)  NULL,
    [GMCCode]         VARCHAR (20)  NULL,
    [TelephoneNo]     VARCHAR (20)  NULL,
    [ConsultantRecno] INT           IDENTITY (1, 1) NOT NULL,
    [CostCentreCode]  VARCHAR (15)  NULL,
    CONSTRAINT [PK_Consultant] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [ConsultantCode] ASC)
);


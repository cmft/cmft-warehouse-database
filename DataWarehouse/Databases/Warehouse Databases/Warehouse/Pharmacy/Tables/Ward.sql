﻿CREATE TABLE [Pharmacy].[Ward] (
    [WardRecno]                     INT          NOT NULL,
    [SpecialtyRecno]                INT          NULL,
    [StockReplenishmentServiceCode] VARCHAR (10) NULL,
    [WardTypeCode]                  VARCHAR (10) NULL,
    [InUse]                         BIT          NULL,
    [Divided]                       BIT          NULL,
    [OneStopService]                BIT          NULL,
    [StockAvailableOutOfHours]      BIT          NULL,
    [OutOfHoursLocationPriority]    INT          NULL,
    CONSTRAINT [PK_Ward] PRIMARY KEY CLUSTERED ([WardRecno] ASC),
    CONSTRAINT [FK_Ward_StockReplenishmentService] FOREIGN KEY ([StockReplenishmentServiceCode]) REFERENCES [Pharmacy].[StockReplenishmentService] ([StockReplenishmentServiceCode]),
    CONSTRAINT [FK_Ward_Supplier] FOREIGN KEY ([WardRecno]) REFERENCES [Pharmacy].[Supplier] ([SupplierRecno]),
    CONSTRAINT [FK_Ward_WardType] FOREIGN KEY ([WardTypeCode]) REFERENCES [Pharmacy].[WardType] ([WardTypeCode])
);


﻿CREATE TABLE [Pharmacy].[DrugGroup] (
    [DrugGroupCode] VARCHAR (2)  NOT NULL,
    [DrugGroup]     VARCHAR (50) NULL,
    CONSTRAINT [PK_DrugGroup] PRIMARY KEY CLUSTERED ([DrugGroupCode] ASC)
);


﻿CREATE TABLE [Pharmacy].[AddressType] (
    [InterfaceCode]   VARCHAR (10) NOT NULL,
    [AddressTypeCode] INT          NOT NULL,
    [AddressType]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_AddressType] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [AddressTypeCode] ASC)
);


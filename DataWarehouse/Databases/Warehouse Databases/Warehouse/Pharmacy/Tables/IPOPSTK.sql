﻿CREATE TABLE [Pharmacy].[IPOPSTK] (
    [IssueLabelTypeCode] VARCHAR (1)  NOT NULL,
    [IssueKindCode]      VARCHAR (1)  NOT NULL,
    [IPOPSTK]            VARCHAR (50) NULL,
    CONSTRAINT [PK_IPOPSTK] PRIMARY KEY CLUSTERED ([IssueLabelTypeCode] ASC, [IssueKindCode] ASC),
    CONSTRAINT [FK_IPOPSTK_IssueTransactionType] FOREIGN KEY ([IssueLabelTypeCode]) REFERENCES [Pharmacy].[IssueTransactionType] ([IssueTransactionTypeCode]),
    CONSTRAINT [FK_IPOPSTK_IssueTransactionType1] FOREIGN KEY ([IssueKindCode]) REFERENCES [Pharmacy].[IssueTransactionType] ([IssueTransactionTypeCode])
);


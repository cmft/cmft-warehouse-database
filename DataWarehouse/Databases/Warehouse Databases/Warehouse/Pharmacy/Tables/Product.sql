﻿CREATE TABLE [Pharmacy].[Product] (
    [InterfaceCode]         VARCHAR (10)  NOT NULL,
    [SiteProductCode]       INT           NOT NULL,
    [ProductCode]           INT           NULL,
    [NSVCode]               VARCHAR (7)   NULL,
    [Product]               VARCHAR (56)  NULL,
    [TradeName]             VARCHAR (30)  NULL,
    [StoresDescription]     VARCHAR (56)  NULL,
    [ReorderPackSize]       INT           NULL,
    [MlsPerPack]            REAL          NULL,
    [Cytotoxic]             VARCHAR (1)   NULL,
    [PrimaryWarningCode]    VARCHAR (6)   NULL,
    [SecondaryWarningCode]  VARCHAR (6)   NULL,
    [InstructionCode]       VARCHAR (6)   NULL,
    [DosesPerIssueUnit]     FLOAT (53)    NULL,
    [DosingUnits]           VARCHAR (20)  NULL,
    [DPSForm]               VARCHAR (4)   NULL,
    [BNFCode]               VARCHAR (13)  NULL,
    [ProductBarCode]        VARCHAR (13)  NULL,
    [SupplementaryBarCode1] VARCHAR (13)  NULL,
    [SupplementaryBarCode2] VARCHAR (13)  NULL,
    [SupplementaryBarCode3] VARCHAR (13)  NULL,
    [SupplementaryBarCode4] VARCHAR (13)  NULL,
    [SupplementaryBarCode5] VARCHAR (13)  NULL,
    [CatalogueDescription]  VARCHAR (512) NULL,
    [ReportGroup]           VARCHAR (255) NULL,
    [ProductRecno]          INT           IDENTITY (1, 1) NOT NULL,
    [IssueForm]             VARCHAR (5)   NULL,
    [TaxRate]               FLOAT (53)    NULL,
    [IssuePrice]            FLOAT (53)    NULL,
    [ContractPrice]         FLOAT (53)    NULL,
    [ArchiveFlag]           BIT           NULL,
    CONSTRAINT [PK_SiteProduct] PRIMARY KEY CLUSTERED ([InterfaceCode] ASC, [SiteProductCode] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_Product]
    ON [Pharmacy].[Product]([InterfaceCode] ASC, [NSVCode] ASC);


﻿CREATE procedure [Pharmacy].[ExtractAscribePrescribingTransactionLog]
	 @interfaceCode varchar(10)
	,@debug bit = 0
as

/* 
==============================================================================================
Description:

When		Who							What
20150603	Paul Egan (for Suman Sidda)	Changed NOT EXISTS criteria for PHAC interface code
20150826	Suman Sidda					Changed the way consultantCode gets populated completely for PHAC InterfaceCode
===============================================================================================
*/




declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @TransactionID int

select @StartTime = getdate()

select @RowsInserted = 0

select
	@TransactionID =
		coalesce(
			MAX(TransactionLogID)
			,0
		)
from
	Pharmacy.TransactionLog
where
	InterfaceCode = @interfaceCode

if @interfaceCode = 'PHAT'
begin

	insert into Pharmacy.TImportTransactionLog
	(
		 [WTranslogID]
		,[RevisionLevel]
		,[PatId]
		,[CaseNo]
		,[SisCode]
		,[ConvFact]
		,[IssueUnits]
		,[Terminal]
		,[Date]
		,[Time]
		,[Qty]
		,[Cost]
		,[CostExTax]
		,[TaxCost]
		,[TaxCode]
		,[TaxRate]
		,[Ward]
		,[Consultant]
		,[Specialty]
		,[Prescriber]
		,[DirCode]
		,[Kind]
		,[Site]
		,[LabelType]
		,[Containers]
		,[Episode]
		,[EventNumber]
		,[PrescriptionNum]
		,[BatchNum]
		,[ExpiryDate]
		,[PPFlag]
		,[StockLvl]
		,[CustOrdNo]
		,[CivasType]
		,[CivasAmount]
		,[SiteID]
		,[EntityID]
		,[ProductID]
		,[LogDateTime]
		,[RequestID_Prescription]
		,[EntityID_Prescriber]
		,[RequestID_Dispensing]
		,[StockValue]
		,InterfaceCode
		,ConsultantCode
		,SpecialtyCode
		,WardCode
		,TerminalCode
	)
	select
		 TransactionLog.[WTranslogID]
		,TransactionLog.[RevisionLevel]
		,TransactionLog.[PatId]
		,TransactionLog.[CaseNo]
		,TransactionLog.[SisCode]
		,TransactionLog.[ConvFact]
		,TransactionLog.[IssueUnits]
		,TransactionLog.[Terminal]
		,TransactionLog.[Date]
		,TransactionLog.[Time]
		,TransactionLog.[Qty]
		,TransactionLog.[Cost]
		,TransactionLog.[CostExTax]
		,TransactionLog.[TaxCost]
		,TransactionLog.[TaxCode]
		,TransactionLog.[TaxRate]
		,TransactionLog.[Ward]
		,TransactionLog.[Consultant]
		,TransactionLog.[Specialty]
		,TransactionLog.[Prescriber]
		,TransactionLog.[DirCode]
		,TransactionLog.[Kind]
		,TransactionLog.[Site]
		,TransactionLog.[LabelType]
		,TransactionLog.[Containers]
		,TransactionLog.[Episode]
		,TransactionLog.[EventNumber]
		,TransactionLog.[PrescriptionNum]
		,TransactionLog.[BatchNum]
		,TransactionLog.[ExpiryDate]
		,TransactionLog.[PPFlag]
		,TransactionLog.[StockLvl]
		,TransactionLog.[CustOrdNo]
		,TransactionLog.[CivasType]
		,TransactionLog.[CivasAmount]
		,TransactionLog.[SiteID]
		,TransactionLog.[EntityID]
		,TransactionLog.[ProductID]
		,TransactionLog.[LogDateTime]
		,TransactionLog.[RequestID_Prescription]
		,TransactionLog.[EntityID_Prescriber]
		,TransactionLog.[RequestID_Dispensing]
		,TransactionLog.[StockValue]
		,InterfaceCode = @interfaceCode

		,ConsultantCode =
			coalesce(
				 InitialsBasedConsultant.ConsultantCode
				,EpisodeBasedConsultant.ConsultantCode
			)

	--this could return duplicates if the 1st 5 characters of the specialty are not unique
	--if this happens the load will fail due to primary key violation which is the intended behaviour
		,SpecialtyCode =
			(
			select
				SpecialtyAlias.SpecialtyID
			from
				[$(Pharmacy_Trafford)].icwsys.SpecialtyAlias

			inner join [$(Pharmacy_Trafford)].icwsys.AliasGroup
			on	AliasGroup.AliasGroupID = SpecialtyAlias.AliasGroupID
			and	AliasGroup.Description = 'WSpecialtyCodes'

			where
				left(SpecialtyAlias.Alias, 5) = TransactionLog.Specialty
			)

		,WardCode =
			coalesce(
				 LocalSupplierWard.WSupplierID
				,Ward.WSupplierID
			)

		,TerminalCode = Terminal.LocationID
	from
		[$(Pharmacy_Trafford)].icwsys.WTransLog TransactionLog

	left join 
		(
		select
			 Consultant = Person.Initials
			,ConsultantCode = Person.EntityID
		from
			[$(Pharmacy_Trafford)].icwsys.Consultant Consultant

		inner join [$(Pharmacy_Trafford)].icwsys.Person
		on	Person.EntityID = Consultant.EntityID

		where
			not exists
			(
			select
				1
			from
				[$(Pharmacy_Trafford)].icwsys.Consultant PreviousConsultant

			inner join [$(Pharmacy_Trafford)].icwsys.Person PreviousPerson
			on	PreviousPerson.EntityID = PreviousConsultant.EntityID

			where
				PreviousPerson.Initials = Person.Initials
			and	PreviousPerson.EntityID > Person.EntityID
			)

		) InitialsBasedConsultant
	on	InitialsBasedConsultant.Consultant = TransactionLog.Consultant

	left join 
		(
		select
			 Episode.EpisodeID
			,ConsultantCode = Consultant.EntityID
		from
			[$(Pharmacy_Trafford)].icwsys.Episode

		inner join [$(Pharmacy_Trafford)].icwsys.ResponsibleEpisodeEntity
		on	ResponsibleEpisodeEntity.EpisodeID = Episode.EpisodeID
		and	ResponsibleEpisodeEntity.Active = 1

		inner join [$(Pharmacy_Trafford)].icwsys.Entity Consultant
		on	Consultant.EntityID = ResponsibleEpisodeEntity.EntityID

		inner join [$(Pharmacy_Trafford)].icwsys.EntityRole ResponsibleEpisodeEntityRole
		on	ResponsibleEpisodeEntityRole.EntityRoleID = ResponsibleEpisodeEntity.EntityRoleID
		and ResponsibleEpisodeEntityRole.Description = 'Consultant'
		) EpisodeBasedConsultant
	on	EpisodeBasedConsultant.EpisodeID = TransactionLog.Episode

	left join [$(Pharmacy_Trafford)].icwsys.WSupplier LocalSupplierWard
	on	LocalSupplierWard.Code = TransactionLog.Ward

	left join [$(Pharmacy_Trafford)].icwsys.WSupplier Ward
	on	Ward.WardCode = TransactionLog.Ward
	and	not exists
		(
		select
			1
		from
			[$(Pharmacy_Trafford)].icwsys.WSupplier Previous
		where
			Previous.WardCode = TransactionLog.Ward
		and	Previous.Code > Ward.Code
		)

	left join [$(Pharmacy_Trafford)].icwsys.Terminal
	on	Terminal.ComputerName = upper(TransactionLog.Terminal)

	where
		TransactionLog.WTranslogID > @TransactionID

end


if @interfaceCode = 'PHAC'
begin

	insert into Pharmacy.TImportTransactionLog
	(
		 [WTranslogID]
		,[RevisionLevel]
		,[PatId]
		,[CaseNo]
		,[SisCode]
		,[ConvFact]
		,[IssueUnits]
		,[Terminal]
		,[Date]
		,[Time]
		,[Qty]
		,[Cost]
		,[CostExTax]
		,[TaxCost]
		,[TaxCode]
		,[TaxRate]
		,[Ward]
		,[Consultant]
		,[Specialty]
		,[Prescriber]
		,[DirCode]
		,[Kind]
		,[Site]
		,[LabelType]
		,[Containers]
		,[Episode]
		,[EventNumber]
		,[PrescriptionNum]
		,[BatchNum]
		,[ExpiryDate]
		,[PPFlag]
		,[StockLvl]
		,[CustOrdNo]
		,[CivasType]
		,[CivasAmount]
		,[SiteID]
		,[EntityID]
		,[ProductID]
		,[LogDateTime]
		,[RequestID_Prescription]
		,[EntityID_Prescriber]
		,[RequestID_Dispensing]
		,[StockValue]
		,InterfaceCode
		,ConsultantCode
		,SpecialtyCode
		,WardCode
		,TerminalCode
	)
	select
		 TransactionLog.[WTranslogID]
		,TransactionLog.[RevisionLevel]
		,TransactionLog.[PatId]
		,TransactionLog.[CaseNo]
		,TransactionLog.[SisCode]
		,TransactionLog.[ConvFact]
		,TransactionLog.[IssueUnits]
		,TransactionLog.[Terminal]
		,TransactionLog.[Date]
		,TransactionLog.[Time]
		,TransactionLog.[Qty]
		,TransactionLog.[Cost]
		,TransactionLog.[CostExTax]
		,TransactionLog.[TaxCost]
		,TransactionLog.[TaxCode]
		,TransactionLog.[TaxRate]
		,TransactionLog.[Ward]
		,TransactionLog.[Consultant]
		,TransactionLog.[Specialty]
		,TransactionLog.[Prescriber]
		,TransactionLog.[DirCode]
		,TransactionLog.[Kind]
		,TransactionLog.[Site]
		,TransactionLog.[LabelType]
		,TransactionLog.[Containers]
		,TransactionLog.[Episode]
		,TransactionLog.[EventNumber]
		,TransactionLog.[PrescriptionNum]
		,TransactionLog.[BatchNum]
		,TransactionLog.[ExpiryDate]
		,TransactionLog.[PPFlag]
		,TransactionLog.[StockLvl]
		,TransactionLog.[CustOrdNo]
		,TransactionLog.[CivasType]
		,TransactionLog.[CivasAmount]
		,TransactionLog.[SiteID]
		,TransactionLog.[EntityID]
		,TransactionLog.[ProductID]
		,TransactionLog.[LogDateTime]
		,TransactionLog.[RequestID_Prescription]
		,TransactionLog.[EntityID_Prescriber]
		,TransactionLog.[RequestID_Dispensing]
		,TransactionLog.[StockValue]
		,InterfaceCode = @interfaceCode

		,ConsultantCode = Consultant.ConsultantCode
			--coalesce(
			--	 InitialsBasedConsultant.ConsultantCode
			--	,EpisodeBasedConsultant.ConsultantCode
			--)

	--this could return duplicates if the 1st 5 characters of the specialty are not unique
	--if this happens the load will fail due to primary key violation which is the intended behaviour
		,SpecialtyCode =
			(
			select
				SpecialtyAlias.SpecialtyID
			from
				[$(Pharmacy_Central)].icwsys.SpecialtyAlias

			inner join [$(Pharmacy_Central)].icwsys.AliasGroup
			on	AliasGroup.AliasGroupID = SpecialtyAlias.AliasGroupID
			and	AliasGroup.Description = 'WSpecialtyCodes'

			where
				left(SpecialtyAlias.Alias, 5) = TransactionLog.Specialty
			)

		,WardCode =
			coalesce(
				 LocalSupplierWard.WSupplierID
				,Ward.WSupplierID
			)

		,TerminalCode = Terminal.LocationID
	from
		[$(Pharmacy_Central)].icwsys.WTransLog TransactionLog

	LEFT JOIN	( SELECT	Consultant = Person.Initials,
							ConsultantCode = Person.EntityID
					
					FROM		[$(Pharmacy_Central)].icwsys.Consultant Consultant
					JOIN		[$(Pharmacy_Central)].icwsys.Person	ON	Person.EntityID = Consultant.EntityID
					LEFT JOIN	[$(Pharmacy_Central)].icwsys.[EntityLinkSpecialty] ON Consultant.EntityID  = [EntityLinkSpecialty].EntityID
					LEFT JOIN	[$(Pharmacy_Central)].icwsys.Specialty ON [EntityLinkSpecialty].[SpecialtyID] = Specialty.[SpecialtyID]

					WHERE		Person.Initials <> ''
					AND NOT		(GMCCode IS NULL AND Specialty.[SpecialtyID] IS NULL) 

				) AS Consultant	ON Consultant.Consultant = TransactionLog.Consultant
	
	left join [$(Pharmacy_Central)].icwsys.WSupplier LocalSupplierWard
	on	LocalSupplierWard.Code = TransactionLog.Ward
	and LocalSupplierWard.SiteID = TransactionLog.SiteID

	left join [$(Pharmacy_Central)].icwsys.WSupplier Ward
	on	Ward.WardCode = TransactionLog.Ward
	and Ward.SiteID = TransactionLog.SiteID
	and	not exists
		(
		select
			1
		from
			[$(Pharmacy_Central)].icwsys.WSupplier Previous
		where
			Previous.WardCode = TransactionLog.Ward
		and	Previous.Code > Ward.Code
		)

	left join [$(Pharmacy_Central)].icwsys.Terminal
	on	Terminal.ComputerName = upper(TransactionLog.Terminal)

	where
		TransactionLog.WTranslogID > @TransactionID

end

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins' +
	', InterfaceCode ' + @interfaceCode

EXEC Utility.WriteAuditLogEvent 'Pharmacy.ExtractAscribePrescribingTransactionLog', @Stats, @StartTime


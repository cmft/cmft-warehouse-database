﻿create procedure [Pharmacy].[ExtractAscribePrescribingRequisition]
	@interfaceCode varchar(10)
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

select @StartTime = getdate()

select @RowsInserted = 0


if @interfaceCode = 'PHAT'
begin

	insert into Pharmacy.TImportRequisition
	(
		 InterfaceCode
		,RequisitionID
		,SiteCode
		,Items
		,PickNo
		,InDisputeUser
		,ShelfPrinted
		,RequisitionNo
		,TaxRatePct
		,TaxInclusive
		,InDispute
		,CustomerOrderNo
		,TaxAmount
		,TaxRateCode
		,PFlag
		,CreatedUser
		,ConversionFactor
		,IssueForm
		,Stocked
		,InternalSiteNo
		,InternalMethodCode
		,SupplierTypeCode
		,OrderedPacks
		,Urgency
		,ToFollow
		,InvoiceNumber
		,PayDate
		,Cost
		,ReceivedPacks
		,ReceivedDate
		,SupplierCode
		,StatusCode
		,NumberPrefix
		,OrderTime
		,LocationCode
		,NSVCode
		,OutstandingPacks
	)
	select
		 InterfaceCode = @interfaceCode
		,RequisitionID = WRequisID
		,SiteCode = Site.SiteNumber
		,Items = Num
		,PickNo = PickNo
		,InDisputeUser = case when InDisputeUser = '' then null else InDisputeUser end
		,ShelfPrinted = case when ShelfPrinted = '' then null else ShelfPrinted end
		,RequisitionNo = case when RequisitionNum = '' then null else RequisitionNum end
		,TaxRatePct = case when VatRatePct = '' then null else VatRatePct end
		,TaxInclusive = CAST(ISNULL(VatInclusive, 0) as float) / 100
		,InDispute = case when InDispute = '' then null else InDispute end
		,CustomerOrderNo = case when CustOrdNo = '' then null else CustOrdNo end
		,TaxAmount = CAST(ISNULL(VATamount, 0) as float) / 100  
		,TaxRateCode = case when VatRateCode = '' then null else VatRateCode end
		,PFlag = case when PFlag = '' then null else PFlag end
		,CreatedUser = case when CreatedUser = '' then null else CreatedUser end
		,ConversionFactor = cast(case when ConvFact = '' then null else ConvFact end as float)
		,IssueForm = case when IssueUnits = '' then null else IssueUnits end
		,Stocked = case when Stocked = '' then null else Stocked end
		,InternalSiteNo = case when InternalSiteNo = '' then null else InternalSiteNo end
		,InternalMethodCode = case when InternalMethod = '' then null else InternalMethod end
		,SupplierTypeCode = case when SupplierType = '' then null else SupplierType end
		,OrderedPacks = cast(case when QtyOrdered = '' then null else QtyOrdered end as float)
		,Urgency = case when Urgency = '' then null else Urgency end
		,ToFollow = case when ToFollow = '' then null else ToFollow end
		,InvoiceNumber = case when InvNum = '' then null else InvNum end

		,PayDate =
			case
			when PayDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 PayDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					)
					,103
				)
			end

		,Cost = CAST(ISNULL(Cost, 0) as float) / 100
		,ReceivedPacks = cast(case when Received = '' then null else Received end as float)

		,ReceivedDate =
			case
			when RecDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 RecDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					)
					,103
				)
			end

		,SupplierCode = case when SupCode = '' then null else SupCode end
		,StatusCode = case when Status = '' then null else Status end
		,NumberPrefix = case when NumPrefix = '' then null else NumPrefix end

		,OrderTime =
			case
			when OrdDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 OrdDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					) + 
					case
					when OrdTime = ''
					then ' 00:00:00'
					else ' ' +
							stuff(
								stuff(
									 OrdTime
									,5
									,0
									,':'
								)
								,3
								,0
								,':'
							)
					end
					,103
				)
			end

		,LocationCode = case when LocCode = '' then null else LocCode end
		,NSVCode = case when Code = '' then null else Code end
		,OutstandingPacks = cast(case when Outstanding = '' then null else Outstanding end as float)

	from
		[$(Pharmacy_Trafford)].icwsys.WRequis Requisition

	left join [$(Pharmacy_Trafford)].icwsys.Site
	on	Site.LocationID = Requisition.SiteID


end


if @interfaceCode = 'PHAC'
begin

	insert into Pharmacy.TImportRequisition
	(
		 InterfaceCode
		,RequisitionID
		,SiteCode
		,Items
		,PickNo
		,InDisputeUser
		,ShelfPrinted
		,RequisitionNo
		,TaxRatePct
		,TaxInclusive
		,InDispute
		,CustomerOrderNo
		,TaxAmount
		,TaxRateCode
		,PFlag
		,CreatedUser
		,ConversionFactor
		,IssueForm
		,Stocked
		,InternalSiteNo
		,InternalMethodCode
		,SupplierTypeCode
		,OrderedPacks
		,Urgency
		,ToFollow
		,InvoiceNumber
		,PayDate
		,Cost
		,ReceivedPacks
		,ReceivedDate
		,SupplierCode
		,StatusCode
		,NumberPrefix
		,OrderTime
		,LocationCode
		,NSVCode
		,OutstandingPacks
	)
	select
		 InterfaceCode = @interfaceCode
		,RequisitionID = WRequisID
		,SiteCode = Site.SiteNumber
		,Items = Num
		,PickNo = PickNo
		,InDisputeUser = case when InDisputeUser = '' then null else InDisputeUser end
		,ShelfPrinted = case when ShelfPrinted = '' then null else ShelfPrinted end
		,RequisitionNo = case when RequisitionNum = '' then null else RequisitionNum end
		,TaxRatePct = case when VatRatePct = '' then null else VatRatePct end
		,TaxInclusive = CAST(ISNULL(VatInclusive, 0) as float) / 100
		,InDispute = case when InDispute = '' then null else InDispute end
		,CustomerOrderNo = case when CustOrdNo = '' then null else CustOrdNo end
		,TaxAmount = CAST(ISNULL(VATamount, 0) as float) / 100  
		,TaxRateCode = case when VatRateCode = '' then null else VatRateCode end
		,PFlag = case when PFlag = '' then null else PFlag end
		,CreatedUser = case when CreatedUser = '' then null else CreatedUser end
		,ConversionFactor = cast(case when ConvFact = '' then null else ConvFact end as float)
		,IssueForm = case when IssueUnits = '' then null else IssueUnits end
		,Stocked = case when Stocked = '' then null else Stocked end
		,InternalSiteNo = case when InternalSiteNo = '' then null else InternalSiteNo end
		,InternalMethodCode = case when InternalMethod = '' then null else InternalMethod end
		,SupplierTypeCode = case when SupplierType = '' then null else SupplierType end
		,OrderedPacks = cast(case when QtyOrdered = '' then null else QtyOrdered end as float)
		,Urgency = case when Urgency = '' then null else Urgency end
		,ToFollow = case when ToFollow = '' then null else ToFollow end
		,InvoiceNumber = case when InvNum = '' then null else InvNum end

		,PayDate =
			case
			when PayDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 PayDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					)
					,103
				)
			end

		,Cost = CAST(ISNULL(Cost, 0) as float) / 100
		,ReceivedPacks = cast(case when Received = '' then null else Received end as float)

		,ReceivedDate =
			case
			when RecDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 RecDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					)
					,103
				)
			end

		,SupplierCode = case when SupCode = '' then null else SupCode end
		,StatusCode = case when Status = '' then null else Status end
		,NumberPrefix = case when NumPrefix = '' then null else NumPrefix end

		,OrderTime =
			case
			when OrdDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 OrdDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					) + 
					case
					when OrdTime = ''
					then ' 00:00:00'
					else ' ' +
							stuff(
								stuff(
									 OrdTime
									,5
									,0
									,':'
								)
								,3
								,0
								,':'
							)
					end
					,103
				)
			end

		,LocationCode = case when LocCode = '' then null else LocCode end
		,NSVCode = case when Code = '' then null else Code end
		,OutstandingPacks = cast(case when Outstanding = '' then null else Outstanding end as float)

	from
		[$(Pharmacy_Central)].icwsys.WRequis Requisition

	left join [$(Pharmacy_Central)].icwsys.Site
	on	Site.LocationID = Requisition.SiteID

end

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins' +
	', InterfaceCode ' + @interfaceCode

EXEC Utility.WriteAuditLogEvent 'Pharmacy.ExtractAscribePrescribingRequisition', @Stats, @StartTime



﻿
CREATE procedure [Pharmacy].[LoadOrderCensus]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

delete
from
	Pharmacy.OrderCensus
where
	exists
		(
		select
			1
		from
			Pharmacy.TLoadOrderCensus
		where
			TLoadOrderCensus.InterfaceCode = OrderCensus.InterfaceCode
		and	TLoadOrderCensus.CensusDate = OrderCensus.CensusDate
		)


insert into Pharmacy.OrderCensus
(
	 InterfaceCode
	,CensusDate
	,OrderID
	,SiteCode
	,NSVCode
	,OutstandingOrder
	,OutstandingOrderValueNet
	,OutstandingOrderValueGross
	,OrderTime
	,LocationCode
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderNumber
	,PackCostNet
	,PackCostGross
	,PickNo
	,ReceivedPacks
	,ReceivedDate
	,OrderedPacks
	,UrgencyCode
	,ToFollow
	,InternalSiteNo
	,InternalMethodCode
	,PFlag
	,CreatedUser
	,CustomerOrderNo
	,InDispute
	,InDisputeUser
	,ShelfPrinted
	,TaxRate
	,Created
	,ByWhom
)
select
	 InterfaceCode
	,CensusDate
	,OrderID
	,SiteCode
	,NSVCode
	,OutstandingOrder
	,OutstandingOrderValueNet
	,OutstandingOrderValueGross
	,OrderTime
	,LocationCode
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderNumber
	,PackCostNet
	,PackCostGross
	,PickNo
	,ReceivedPacks
	,ReceivedDate
	,OrderedPacks
	,UrgencyCode
	,ToFollow
	,InternalSiteNo
	,InternalMethodCode
	,PFlag
	,CreatedUser
	,CustomerOrderNo
	,InDispute
	,InDisputeUser
	,ShelfPrinted
	,TaxRate
	,Created = getdate()
	,ByWhom = system_user
from
	Pharmacy.TLoadOrderCensus


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.LoadOrderCensus', @Stats, @StartTime



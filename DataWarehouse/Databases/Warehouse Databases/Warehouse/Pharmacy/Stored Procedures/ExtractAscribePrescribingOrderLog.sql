﻿CREATE procedure [Pharmacy].[ExtractAscribePrescribingOrderLog]
	 @interfaceCode varchar(10)
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @OrderID int

select @StartTime = getdate()

select @RowsInserted = 0

select
	@OrderID =
		coalesce(
			MAX(OrderLogID)
			,0
		)
from
	Pharmacy.OrderLog
where
	InterfaceCode = @interfaceCode

if @interfaceCode = 'PHAT'
begin

	insert into Pharmacy.TImportOrderLog
	(
		 InterfaceCode
		,OrderLogID
		,OrderNumber
		,ConversionFactor
		,IssueForm
		,DispId
		,Terminal
		,OrderTime
		,ReceivedDate
		,OrderedPacks
		,ReceivedPacks
		,SupplierCode
		,BatchNumber
		,ExpiryDate
		,InvoiceNumber
		,LinkedNumber
		,ReasonCode
		,StockLevel
		,CreatedByCode
		,DateOrdered
		,DateReceived
		,SiteID
		,NSVCode
		,OrderLogTime
		,OrderKindCode
		,Cost
		,CostExTax
		,TaxCost
		,TaxCode
		,TaxRate
		,StockValue
		,DateInvoiced
	)
	select
		 InterfaceCode = @interfaceCode
		,OrderLogID = WOrderLogID
		,OrderNumber = case when OrderNum = '' then null else OrderNum end
		,ConversionFactor = CAST(ConvFact AS FLOAT) 
		,IssueForm = case when IssueUnits = '' then null else IssueUnits end
		,DispId = DispId
		,Terminal

		,OrderTime =
			case
			when DateOrdered = '30 Dec 1899'
			then null
			else DateOrdered
			end

		,ReceivedDate =
			case
			when DateReceived = '30 Dec 1899'
			then null
			else DateReceived
			end

		,OrderedPacks = cast(QtyOrd as float)
		,ReceivedPacks = cast(QtyRec as float)
		,SupplierCode = SupCode
		,BatchNumber = case when BatchNum = '' then null else BatchNum end
		,ExpiryDate = case when ExpiryDate = '' then null else ExpiryDate end
		,InvoiceNumber = case when InvNum = '' then null else InvNum end
		,LinkedNumber = case when LinkedNum = '' then null else LinkedNum end
		,ReasonCode = case when ReasonCode = '' then null else ReasonCode end
		,StockLevel = cast(StockLvl as float)
		,CreatedByCode = EntityID
		,DateOrdered
		,DateReceived
		,SiteID
		,NSVCode = case when SisCode = '' then null else SisCode end
		,OrderLogTime = LogDateTime
		,OrderKindCode = Kind

		,Cost =   
		   CASE WHEN Kind = 'A' AND UPPER([SupCode]) = 'EDIT'
						THEN CAST(CONVERT(FLOAT, Cost) / CONVERT(FLOAT, ConvFact) AS MONEY)
						ELSE CAST(CONVERT(FLOAT, Cost) AS MONEY)
						END

		,CostExTax =
		   CASE WHEN Kind = 'A' AND UPPER([SupCode]) = 'EDIT'
						THEN CAST(CONVERT(FLOAT, CostExVat) / CONVERT(FLOAT, ConvFact) AS MONEY)
						ELSE CAST(CONVERT(FLOAT, CostExVat) AS MONEY)
						END

		,TaxCost =                                            
		   CASE WHEN Kind = 'A' AND UPPER([SupCode]) = 'EDIT'
						THEN CAST(CONVERT(FLOAT, VatCost) / CONVERT(FLOAT, ConvFact) AS MONEY)
						ELSE CAST(CAST(VatCost AS FLOAT) AS MONEY)
						END

		,TaxCode = VatCode
		,TaxRate = VatRate
		,StockValue

		,DateInvoiced =
			case
			when DateInvoiced = '30 Dec 1899'
			then null
			else DateInvoiced
			end

	from
		[$(Pharmacy_Trafford)].icwsys.WOrderLog OrderLog
	where
		OrderLog.WOrderLogID > @OrderID

end


if @interfaceCode = 'PHAC'
begin

	insert into Pharmacy.TImportOrderLog
	(
		 InterfaceCode
		,OrderLogID
		,OrderNumber
		,ConversionFactor
		,IssueForm
		,DispId
		,Terminal
		,OrderTime
		,ReceivedDate
		,OrderedPacks
		,ReceivedPacks
		,SupplierCode
		,BatchNumber
		,ExpiryDate
		,InvoiceNumber
		,LinkedNumber
		,ReasonCode
		,StockLevel
		,CreatedByCode
		,DateOrdered
		,DateReceived
		,SiteID
		,NSVCode
		,OrderLogTime
		,OrderKindCode
		,Cost
		,CostExTax
		,TaxCost
		,TaxCode
		,TaxRate
		,StockValue
		,DateInvoiced
	)
	select
		 InterfaceCode = @interfaceCode
		,OrderLogID = WOrderLogID
		,OrderNumber = case when OrderNum = '' then null else OrderNum end
		,ConversionFactor = CAST(ConvFact AS FLOAT) 
		,IssueForm = case when IssueUnits = '' then null else IssueUnits end
		,DispId = DispId
		,Terminal

		,OrderTime = DateOrdered

		,ReceivedDate =
			case
			when DateReceived = '30 Dec 1899'
			then null
			else DateReceived
			end

		,OrderedPacks = cast(QtyOrd as float)
		,ReceivedPacks = cast(QtyRec as float)
		,SupplierCode = SupCode
		,BatchNumber = case when BatchNum = '' then null else BatchNum end
		,ExpiryDate = case when ExpiryDate = '' then null else ExpiryDate end
		,InvoiceNumber = case when InvNum = '' then null else InvNum end
		,LinkedNumber = case when LinkedNum = '' then null else LinkedNum end
		,ReasonCode = case when ReasonCode = '' then null else ReasonCode end
		,StockLevel = cast(StockLvl as float)
		,CreatedByCode = EntityID
		,DateOrdered
		,DateReceived
		,SiteID
		,NSVCode = case when SisCode = '' then null else SisCode end
		,OrderLogTime = LogDateTime
		,OrderKindCode = Kind

		,Cost =   
		   CASE WHEN Kind = 'A' AND UPPER([SupCode]) = 'EDIT'
						THEN CAST(CONVERT(FLOAT, Cost) / CONVERT(FLOAT, ConvFact) AS MONEY)
						ELSE CAST(CONVERT(FLOAT, Cost) AS MONEY)
						END

		,CostExTax =
		   CASE WHEN Kind = 'A' AND UPPER([SupCode]) = 'EDIT'
						THEN CAST(CONVERT(FLOAT, CostExVat) / CONVERT(FLOAT, ConvFact) AS MONEY)
						ELSE CAST(CONVERT(FLOAT, CostExVat) AS MONEY)
						END

		,TaxCost =                                            
		   CASE WHEN Kind = 'A' AND UPPER([SupCode]) = 'EDIT'
						THEN CAST(CONVERT(FLOAT, VatCost) / CONVERT(FLOAT, ConvFact) AS MONEY)
						ELSE CAST(CAST(VatCost AS FLOAT) AS MONEY)
						END

		,TaxCode = VatCode
		,TaxRate = VatRate
		,StockValue

		,DateInvoiced =
			case
			when DateInvoiced = '30 Dec 1899'
			then null
			else DateInvoiced
			end

	from
		[$(Pharmacy_Central)].icwsys.WOrderLog OrderLog
	where
		OrderLog.WOrderLogID > @OrderID

end

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins' +
	', InterfaceCode ' + @interfaceCode

EXEC Utility.WriteAuditLogEvent 'Pharmacy.ExtractAscribePrescribingOrderLog', @Stats, @StartTime



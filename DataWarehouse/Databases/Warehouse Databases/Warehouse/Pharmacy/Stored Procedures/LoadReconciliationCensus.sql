﻿create procedure [Pharmacy].[LoadReconciliationCensus]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

delete
from
	Pharmacy.ReconciliationCensus
where
	exists
		(
		select
			1
		from
			Pharmacy.TLoadReconciliationCensus
		where
			TLoadReconciliationCensus.InterfaceCode = ReconciliationCensus.InterfaceCode
		and	TLoadReconciliationCensus.CensusDate = ReconciliationCensus.CensusDate
		)


insert into Pharmacy.ReconciliationCensus
(
	 InterfaceCode
	,CensusDate
	,ReconciliationCode
	,SiteID
	,SupplierCode
	,OrderNumber
	,NSVCode
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,PackCost
	,ReceivedPacks
	,NetValue
	,GrossValue
	,LocationCode
	,InDispute
	,InDisputeUser
	,Created
	,ByWhom
)
select
	 InterfaceCode
	,CensusDate
	,ReconciliationCode
	,SiteID
	,SupplierCode
	,OrderNumber
	,NSVCode
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,PackCost
	,ReceivedPacks
	,NetValue
	,GrossValue
	,LocationCode
	,InDispute
	,InDisputeUser
	,Created = getdate()
	,ByWhom = system_user
from
	Pharmacy.TLoadReconciliationCensus


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.LoadReconciliationCensus', @Stats, @StartTime



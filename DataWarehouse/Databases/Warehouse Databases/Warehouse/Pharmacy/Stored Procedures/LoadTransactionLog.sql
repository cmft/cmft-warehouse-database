﻿
CREATE procedure [Pharmacy].[LoadTransactionLog] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

insert into Pharmacy.TransactionLog
(
	 InterfaceCode
	,TransactionLogID
	,RevisionLevel
	,SourcePatientNo
	,CasenoteNo
	,NSVCode
	,PackSize
	,IssueForm
	,TerminalCode
	,Terminal
	,Quantity
	,Cost
	,CostExcludingTax
	,TaxCost
	,TaxCode
	,TaxRate
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,IssueDirections
	,IssueKindCode
	,SiteCode
	,IssueLabelTypeCode
	,EpisodeID
	,PrescriptionNo
	,BatchNo
	,BatchExpiry
	,StockLevel
	,CustomerOrderNo
	,CIVASAmount
	,SiteID
	,CreatedByCode
	,ProductID
	,TransactionLogTime
	,PrescriptionRequestID
	,PrescriberEntityID
	,DispensingRequestID
	,StockValue
	,GPCode
	,Created
	,ByWhom
)
select
	 InterfaceCode
	,TransactionLogID
	,RevisionLevel
	,SourcePatientNo
	,CasenoteNo
	,NSVCode
	,PackSize
	,IssueForm
	,TerminalCode
	,Terminal
	,Quantity
	,Cost
	,CostExcludingTax
	,TaxCost
	,TaxCode
	,TaxRate
	,WardCode
	,ConsultantCode
	,SpecialtyCode
	,IssueDirections
	,IssueKindCode
	,SiteCode
	,IssueLabelTypeCode
	,EpisodeID
	,PrescriptionNo
	,BatchNo
	,BatchExpiry
	,StockLevel
	,CustomerOrderNo
	,CIVASAmount
	,SiteID
	,CreatedByCode
	,ProductID
	,TransactionLogTime
	,PrescriptionRequestID
	,PrescriberEntityID
	,DispensingRequestID
	,StockValue
	,GPCode
	,Created = getdate()
	,ByWhom = system_user
from
	Pharmacy.TLoadTransactionLog


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.LoadTransactionLog', @Stats, @StartTime



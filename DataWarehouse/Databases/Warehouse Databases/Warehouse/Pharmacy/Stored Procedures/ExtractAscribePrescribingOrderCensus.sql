﻿CREATE procedure [Pharmacy].[ExtractAscribePrescribingOrderCensus]
	@interfaceCode varchar(10)
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @CensusDate date

select @StartTime = getdate()

select @RowsInserted = 0

select
	@CensusDate =
		CAST(
			getdate()
			as date
		)

if @interfaceCode = 'PHAT'
begin

	insert into Pharmacy.TImportOrderCensus
	(
	 InterfaceCode
	,CensusDate
	,OrderID
	,SiteCode
	,NSVCode
	,OutstandingOrder
	,OutstandingOrderValueNet
	,OutstandingOrderValueGross
	,OrderTime
	,LocationCode
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderNumber
	,PackCostNet
	,PackCostGross
	,PickNo
	,ReceivedPacks
	,ReceivedDate
	,OrderedPacks
	,UrgencyCode
	,ToFollow
	,InternalSiteNo
	,InternalMethodCode
	,PFlag
	,CreatedUser
	,CustomerOrderNo
	,InDispute
	,InDisputeUser
	,ShelfPrinted
	,TaxRate
	)
	select
		 InterfaceCode = @interfaceCode
		,CensusDate = @CensusDate
		,OrderID = Orders.WOrderID
		,SiteCode = Site.SiteNumber
		,NSVCode = Orders.Code

		,OutstandingOrder =
			coalesce(cast(Orders.Outstanding as float), 0)

		,OutstandingOrderValueNet =
			(
				CONVERT(
					 FLOAT
					,ISNULL(Orders.Cost, 0)
				) *
				CONVERT(
					 FLOAT
					,ISNULL(Orders.Outstanding, 0)
				) / 100
			)

		,OutstandingOrderValueGross =
			(
				CONVERT(
					 FLOAT
					,ISNULL(Orders.Cost, 0)
				) *
				CONVERT(
					 FLOAT
					,ISNULL(Orders.Outstanding, 0)
				) / 100
			)
			 * TaxRate.TaxRate

		,OrderTime =
			case
			when Orders.OrdDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 Orders.OrdDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					) + 
					case
					when Orders.OrdTime = ''
					then ' 00:00:00'
					else ' ' +
							stuff(
								stuff(
									 Orders.OrdTime
									,5
									,0
									,':'
								)
								,3
								,0
								,':'
							)
					end
					,103
				)
			end
			
		,LocationCode = case when Orders.LocCode = '' then null else Orders.LocCode end
		,SupplierCode = case when Orders.SupCode = '' then null else Orders.SupCode end
		,StatusCode = case when Orders.Status = '' then null else Orders.Status end
		,NumberPrefix = case when Orders.NumPrefix = '' then null else Orders.NumPrefix end
		,OrderNumber = Orders.Num

		,PackCostNet =
			(
				CONVERT(
					 FLOAT
					,ISNULL(Orders.Cost, 0)
				)
			)

		,PackCostGross =
			(
				CONVERT(
					 FLOAT
					,ISNULL(Orders.Cost, 0)
				)
			)
			 * TaxRate.TaxRate

		,PickNo = Orders.PickNo

		,ReceivedPacks = cast(cast(Orders.Received as float) as int)

		,ReceivedDate =
			case
			when Orders.RecDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 Orders.RecDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					)
					,103
				)
			end

		,OrderedPacks = cast(Orders.QtyOrdered as float)
		,UrgencyCode = case when Orders.Urgency = '' then null else Orders.Urgency end
		,ToFollow = case when Orders.ToFollow = '' then null else Orders.ToFollow end
		,InternalSiteNo = case when Orders.InternalSiteNo = '' then null else Orders.InternalSiteNo end
		,InternalMethodCode = case when Orders.InternalMethod = '' then null else Orders.InternalMethod end
		,PFlag = case when Orders.PFlag = '' then null else Orders.PFlag end
		,CreatedUser = case when Orders.CreatedUser = '' then null else Orders.CreatedUser end
		,CustomerOrderNo = case when Orders.CustOrdNo = '' then null else Orders.CustOrdNo end
		,InDispute = case when Orders.InDispute = '' then null else Orders.InDispute end
		,InDisputeUser = case when Orders.InDisputeUser = '' then null else Orders.InDisputeUser end
		,ShelfPrinted = case when Orders.ShelfPrinted = '' then null else Orders.ShelfPrinted end
		,TaxRate.TaxRate
	from
		[$(Pharmacy_Trafford)].icwsys.WOrder Orders

	left join [$(Pharmacy_Trafford)].icwsys.wProduct Product
	on	Product.siscode = Orders.Code
	and	Product.LocationID_Site = Orders.SiteID

	left join [$(Pharmacy_Trafford)].icwsys.Site
	on	Site.LocationID = Orders.SiteID

	left join
		(
		select
			TaxCode =
				SUBSTRING(
					 TaxRate.[Key]
					,CHARINDEX('(', TaxRate.[Key]) + 1
					,1
				)

			,TaxRate =
				CONVERT(
					FLOAT
					,replace(
						TaxRate.[Value]
						,'"'
						,''
					)
				)
		from
			[$(Pharmacy_Trafford)].icwsys.wConfiguration TaxRate
		where
			TaxRate.Category = 'D|Workingdefaults'
		and	left(TaxRate.[Key], 3) = 'VAT'
		and	not exists
			(
			select
				1
			from
				[$(Pharmacy_Trafford)].icwsys.wConfiguration Previous
			where
				Previous.Category = 'D|Workingdefaults'
			and	left(Previous.[Key], 3) = 'VAT'
			and	Previous.SiteID < TaxRate.SiteID
			)
		) TaxRate
	on	TaxRate.TaxCode = coalesce(Product.vatrate, 1)


end


if @interfaceCode = 'PHAC'
begin

	insert into Pharmacy.TImportOrderCensus
	(
	 InterfaceCode
	,CensusDate
	,OrderID
	,SiteCode
	,NSVCode
	,OutstandingOrder
	,OutstandingOrderValueNet
	,OutstandingOrderValueGross
	,OrderTime
	,LocationCode
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderNumber
	,PackCostNet
	,PackCostGross
	,PickNo
	,ReceivedPacks
	,ReceivedDate
	,OrderedPacks
	,UrgencyCode
	,ToFollow
	,InternalSiteNo
	,InternalMethodCode
	,PFlag
	,CreatedUser
	,CustomerOrderNo
	,InDispute
	,InDisputeUser
	,ShelfPrinted
	,TaxRate
	)
	 select
              InterfaceCode = 'PHAC'
              ,CensusDate = @CensusDate
              ,OrderID = Orders.WOrderID
              ,SiteCode = Site.SiteNumber
              ,NSVCode = Orders.Code

              ,OutstandingOrder = cast(Orders.Outstanding as float)
                      --orders.outstanding
                     --cast(
                     --      ltrim(rtrim(coalesce(Orders.Outstanding, 0)))
                     --      as float
                     --)

              ,OutstandingOrderValueNet =
                     (
                           CONVERT(
                                  FLOAT
                                  ,ISNULL(Orders.Cost, 0)
                           ) *
                           CONVERT(
                                  FLOAT
                                  ,ISNULL(Orders.Outstanding, 0)
                           ) / 100
                     )

              ,OutstandingOrderValueGross =
                     (
                           CONVERT(
                                  FLOAT
                                  ,ISNULL(Orders.Cost, 0)
                           ) *
                           CONVERT(
                                  FLOAT
                                  ,ISNULL(Orders.Outstanding, 0)
                           ) / 100
                     )
                     * TaxRate.TaxRate

              ,OrderDate =
                     case
                     when Orders.OrdDate = ''
                     then null
                     else
                           convert(
                                  datetime
                                  ,stuff(
                                         stuff(
                                                Orders.OrdDate
                                                ,5
                                                ,0
                                                ,'/'
                                         )
                                         ,3
                                         ,0
                                         ,'/'
                                  ) + 
                                  case
                                  when Orders.OrdTime = ''
                                  then ' 00:00:00'
                                  else ' ' +
                                                stuff(
                                                       stuff(
                                                              Orders.OrdTime
                                                              ,5
                                                              ,0
                                                              ,':'
                                                       )
                                                       ,3
                                                       ,0
                                                       ,':'
                                                )
                                  end
                                  ,103
                           )
                     end
                     
              ,LocationCode = case when Orders.LocCode = '' then null else Orders.LocCode end
              ,SupplierCode = case when Orders.SupCode = '' then null else Orders.SupCode end
              ,StatusCode = case when Orders.Status = '' then null else Orders.Status end
              ,NumberPrefix = case when Orders.NumPrefix = '' then null else Orders.NumPrefix end
              ,OrderNumber = Orders.Num

              ,PackCostNet =
                     (
                           CONVERT(
                                  FLOAT
                                  ,ISNULL(Orders.Cost, 0)
                           )
                     )

              ,PackCostGross =
                     (
                           CONVERT(
                                  FLOAT
                                  ,ISNULL(Orders.Cost, 0)
                           )
                     )
                     * TaxRate.TaxRate

              ,PickNo = Orders.PickNo

              ,ReceivedPacks = cast(Orders.Received as Float)

              ,ReceivedDate =
                     case
                     when Orders.RecDate = ''
                     then null
                     else
                           convert(
                                  datetime
                                  ,stuff(
                                         stuff(
                                                Orders.RecDate
                                                ,5
                                                ,0
                                                ,'/'
                                         )
                                         ,3
                                         ,0
                                         ,'/'
                                  )
                                  ,103
                           )
                     end

              ,OrderedPacks = cast(Orders.QtyOrdered as float)
              ,UrgencyCode = case when Orders.Urgency = '' then null else Orders.Urgency end
              ,ToFollow = case when Orders.ToFollow = '' then null else Orders.ToFollow end
              ,InternalSiteNo = case when Orders.InternalSiteNo = '' then null else Orders.InternalSiteNo end
              ,InternalMethodCode = case when Orders.InternalMethod = '' then null else Orders.InternalMethod end
              ,PFlag = case when Orders.PFlag = '' then null else Orders.PFlag end
              ,CreatedUser = case when Orders.CreatedUser = '' then null else Orders.CreatedUser end
              ,CustomerOrderNo = case when Orders.CustOrdNo = '' then null else Orders.CustOrdNo end
              ,InDispute = case when Orders.InDispute = '' then null else Orders.InDispute end
              ,InDisputeUser = case when Orders.InDisputeUser = '' then null else Orders.InDisputeUser end
              ,ShelfPrinted = case when Orders.ShelfPrinted = '' then null else Orders.ShelfPrinted end
              ,TaxRate.TaxRate
       from
              [$(Pharmacy_Central)].icwsys.WOrder Orders

       left join [$(Pharmacy_Central)].icwsys.wProduct Product
       on     Product.siscode = Orders.Code
       and    Product.LocationID_Site = Orders.SiteID

       left join [$(Pharmacy_Central)].icwsys.Site
       on     Site.LocationID = Orders.SiteID

       left join
              (
              select
                     TaxCode =
                           SUBSTRING(
                                  TaxRate.[Key]
                                  ,CHARINDEX('(', TaxRate.[Key]) + 1
                                  ,1
                           )

                     ,TaxRate =
                           CONVERT(
                                  FLOAT
                                  ,replace(
                                         TaxRate.[Value]
                                         ,'"'
                                         ,''
                                  )
                           )
              from
                     [$(Pharmacy_Central)].icwsys.wConfiguration TaxRate
              where
                     TaxRate.Category = 'D|Workingdefaults'
              and    left(TaxRate.[Key], 3) = 'VAT'
              and    not exists
                     (
                     select
                           1
                     from
                           [$(Pharmacy_Central)].icwsys.wConfiguration Previous
                     where
                           Previous.Category = 'D|Workingdefaults'
                     and    left(Previous.[Key], 3) = 'VAT'
                     and    Previous.SiteID < TaxRate.SiteID
                     )
              ) TaxRate
       on     TaxRate.TaxCode = coalesce(Product.vatrate, 1)


end

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins' +
	', InterfaceCode ' + @interfaceCode

EXEC Utility.WriteAuditLogEvent 'Pharmacy.ExtractAscribePrescribingOrderCensus', @Stats, @StartTime



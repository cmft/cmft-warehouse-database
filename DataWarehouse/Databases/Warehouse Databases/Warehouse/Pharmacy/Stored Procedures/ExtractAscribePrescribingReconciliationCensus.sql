﻿create procedure [Pharmacy].[ExtractAscribePrescribingReconciliationCensus]
	@interfaceCode varchar(10)
as

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)

declare @CensusDate date

select @StartTime = getdate()

select @RowsInserted = 0

select
	@CensusDate =
		CAST(
			getdate()
			as date
		)

if @interfaceCode = 'PHAT'
begin

	insert into Pharmacy.TImportReconciliationCensus
	(
		 InterfaceCode
		,CensusDate
		,ReconciliationCode
		,SiteID
		,SupplierCode
		,OrderNumber
		,NSVCode
		,OrderTime
		,ReceivedDate
		,OrderedPacks
		,PackCost
		,ReceivedPacks
		,NetValue
		,GrossValue
		,LocationCode
		,InDispute
		,InDisputeUser
	)
	select
		 InterfaceCode = @interfaceCode
		,CensusDate = @CensusDate
		,ReconciliationCode = Reconciliation.WReconcilID
		,SiteID = Reconciliation.SiteID
		,SupplierCode = case when Reconciliation.SupCode = '' then null else Reconciliation.SupCode end
		,OrderNumber = Reconciliation.Num
		,NSVCode = Reconciliation.Code

		,OrderTime =
			case
			when Reconciliation.OrdDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 Reconciliation.OrdDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					) + 
					case
					when Reconciliation.OrdTime = ''
					then ' 00:00:00'
					else ' ' +
							stuff(
								stuff(
									 Reconciliation.OrdTime
									,5
									,0
									,':'
								)
								,3
								,0
								,':'
							)
					end
					,103
				)
			end


		,ReceivedDate =
			case
			when Reconciliation.RecDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 Reconciliation.RecDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					)
					,103
				)
			end


		,OrderedPacks = cast(Reconciliation.QtyOrdered as float)

		,PackCost = cast(Reconciliation.Cost as float)

		,ReceivedPacks = cast(Reconciliation.Received as float)

		,NetValue =
			cast(
					(
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Cost, 0)
						) *
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Received, 0)
						) / 100
					)
				as money
			)

		,GrossValue =
			cast(
					(
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Cost, 0)
						) *
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Received, 0)
						) / 100
					)
					 * TaxRate.TaxRate
				as money
			)

		,LocationCode = case when Reconciliation.LocCode = '' then null else Reconciliation.LocCode end
		,InDispute = case when Reconciliation.InDispute = '' then null else Reconciliation.InDispute end
		,InDisputeUser = case when Reconciliation.InDisputeUser = '' then null else Reconciliation.InDisputeUser end

	from
		[$(Pharmacy_Trafford)].icwsys.WReconcil Reconciliation

	left join [$(Pharmacy_Trafford)].icwsys.wProduct Product
	on	Product.siscode = Reconciliation.Code
	and	Product.LocationID_Site = Reconciliation.SiteID

	left join
		(
		select
			TaxCode =
				SUBSTRING(
					 TaxRate.[Key]
					,CHARINDEX('(', TaxRate.[Key]) + 1
					,1
				)

			,TaxRate =
				CONVERT(
					FLOAT
					,replace(
						TaxRate.[Value]
						,'"'
						,''
					)
				)
		from
			[$(Pharmacy_Trafford)].icwsys.wConfiguration TaxRate
		where
			TaxRate.Category = 'D|Workingdefaults'
		and	left(TaxRate.[Key], 3) = 'VAT'
		and	not exists
			(
			select
				1
			from
				[$(Pharmacy_Trafford)].icwsys.wConfiguration Previous
			where
				Previous.Category = 'D|Workingdefaults'
			and	left(Previous.[Key], 3) = 'VAT'
			and	Previous.SiteID < TaxRate.SiteID
			)
		) TaxRate
	on	TaxRate.TaxCode = coalesce(Product.vatrate, 1)

	where
		Reconciliation.Status = 4


end


if @interfaceCode = 'PHAC'
begin

	insert into Pharmacy.TImportReconciliationCensus
	(
		 InterfaceCode
		,CensusDate
		,ReconciliationCode
		,SiteID
		,SupplierCode
		,OrderNumber
		,NSVCode
		,OrderTime
		,ReceivedDate
		,OrderedPacks
		,PackCost
		,ReceivedPacks
		,NetValue
		,GrossValue
		,LocationCode
		,InDispute
		,InDisputeUser
	)
	select
		 InterfaceCode = @interfaceCode
		,CensusDate = @CensusDate
		,ReconciliationCode = Reconciliation.WReconcilID
		,SiteID = Reconciliation.SiteID
		,SupplierCode = case when Reconciliation.SupCode = '' then null else Reconciliation.SupCode end
		,OrderNumber = Reconciliation.Num
		,NSVCode = Reconciliation.Code

		,OrderTime =
			case
			when Reconciliation.OrdDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 Reconciliation.OrdDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					) + 
					case
					when Reconciliation.OrdTime = ''
					then ' 00:00:00'
					else ' ' +
							stuff(
								stuff(
									 Reconciliation.OrdTime
									,5
									,0
									,':'
								)
								,3
								,0
								,':'
							)
					end
					,103
				)
			end


		,ReceivedDate =
			case
			when Reconciliation.RecDate = ''
			then null
			else
				convert(
					 datetime
					,stuff(
						stuff(
							 Reconciliation.RecDate
							,5
							,0
							,'/'
						)
						,3
						,0
						,'/'
					)
					,103
				)
			end


		,OrderedPacks = cast(Reconciliation.QtyOrdered as float)

		,PackCost = cast(Reconciliation.Cost as float)

		,ReceivedPacks = cast(Reconciliation.Received as float)

		,NetValue =
			cast(
					(
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Cost, 0)
						) *
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Received, 0)
						) / 100
					)
				as money
			)

		,GrossValue =
			cast(
					(
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Cost, 0)
						) *
						CONVERT(
							 FLOAT
							,ISNULL(Reconciliation.Received, 0)
						) / 100
					)
					 * TaxRate.TaxRate
				as money
			)

		,LocationCode = case when Reconciliation.LocCode = '' then null else Reconciliation.LocCode end
		,InDispute = case when Reconciliation.InDispute = '' then null else Reconciliation.InDispute end
		,InDisputeUser = case when Reconciliation.InDisputeUser = '' then null else Reconciliation.InDisputeUser end

	from
		[$(Pharmacy_Central)].icwsys.WReconcil Reconciliation

	left join [$(Pharmacy_Central)].icwsys.wProduct Product
	on	Product.siscode = Reconciliation.Code
	and	Product.LocationID_Site = Reconciliation.SiteID

	left join
		(
		select
			TaxCode =
				SUBSTRING(
					 TaxRate.[Key]
					,CHARINDEX('(', TaxRate.[Key]) + 1
					,1
				)

			,TaxRate =
				CONVERT(
					FLOAT
					,replace(
						TaxRate.[Value]
						,'"'
						,''
					)
				)
		from
			[$(Pharmacy_Central)].icwsys.wConfiguration TaxRate
		where
			TaxRate.Category = 'D|Workingdefaults'
		and	left(TaxRate.[Key], 3) = 'VAT'
		and	not exists
			(
			select
				1
			from
				[$(Pharmacy_Central)].icwsys.wConfiguration Previous
			where
				Previous.Category = 'D|Workingdefaults'
			and	left(Previous.[Key], 3) = 'VAT'
			and	Previous.SiteID < TaxRate.SiteID
			)
		) TaxRate
	on	TaxRate.TaxCode = coalesce(Product.vatrate, 1)

	where
		Reconciliation.Status = 4

end

select
	@RowsInserted = @RowsInserted + @@ROWCOUNT

SELECT @Elapsed = DATEDIFF(minute,@StartTime,getdate())

SELECT @Stats = 
	'Rows Inserted ' + CONVERT(varchar(10), @RowsInserted) + ', '  + 
	'Time Elapsed ' + CONVERT(varchar(6), @Elapsed) + ' Mins' +
	', InterfaceCode ' + @interfaceCode

EXEC Utility.WriteAuditLogEvent 'Pharmacy.ExtractAscribePrescribingReconciliationCensus', @Stats, @StartTime



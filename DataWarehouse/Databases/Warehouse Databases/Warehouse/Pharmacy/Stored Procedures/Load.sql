﻿CREATE procedure [Pharmacy].[Load]
	 @interfaceCode varchar(10)
as

declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()

truncate table Pharmacy.TImportTransactionLog
exec Pharmacy.ExtractAscribePrescribingTransactionLog @interfaceCode
exec Pharmacy.LoadTransactionLog


truncate table Pharmacy.TImportOrderCensus
exec Pharmacy.ExtractAscribePrescribingOrderCensus @interfaceCode
exec Pharmacy.LoadOrderCensus

truncate table Pharmacy.TImportReconciliationCensus
exec Pharmacy.ExtractAscribePrescribingReconciliationCensus @interfaceCode
exec Pharmacy.LoadReconciliationCensus

truncate table Pharmacy.TImportOrderLog
exec Pharmacy.ExtractAscribePrescribingOrderLog @interfaceCode
exec Pharmacy.LoadOrderLog

truncate table Pharmacy.TImportRequisition
exec Pharmacy.ExtractAscribePrescribingRequisition @interfaceCode
exec Pharmacy.LoadRequisition


exec Pharmacy.ExtractAscribePharmacyReferenceData @interfaceCode


update dbo.Parameter
set
	DateValue = getdate()
where
	Parameter = 'LOADPHARMACYDATE' + @interfaceCode

if @@rowcount = 0

insert into dbo.Parameter
	(
	 Parameter
	,DateValue
	)
select
	 Parameter = 'LOADPHARMACYDATE' + @interfaceCode
	,DateValue = getdate()


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' +
	', InterfaceCode ' + @interfaceCode

exec Utility.WriteAuditLogEvent 'Pharmacy.Load', @Stats, @StartTime





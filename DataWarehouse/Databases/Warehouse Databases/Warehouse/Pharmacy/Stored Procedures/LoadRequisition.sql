﻿create procedure [Pharmacy].[LoadRequisition]
as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

delete
from
	Pharmacy.Requisition
where
	exists
		(
		select
			1
		from
			Pharmacy.TLoadRequisition
		where
			TLoadRequisition.InterfaceCode = Requisition.InterfaceCode
		)


insert into Pharmacy.Requisition
(
	 InterfaceCode
	,RequisitionID
	,SiteCode
	,Items
	,PickNo
	,InDisputeUser
	,ShelfPrinted
	,RequisitionNo
	,TaxRatePct
	,TaxInclusive
	,InDispute
	,CustomerOrderNo
	,TaxAmount
	,TaxRateCode
	,PFlag
	,CreatedUser
	,ConversionFactor
	,IssueForm
	,Stocked
	,InternalSiteNo
	,InternalMethodCode
	,SupplierTypeCode
	,OrderedPacks
	,Urgency
	,ToFollow
	,InvoiceNumber
	,PayDate
	,Cost
	,ReceivedPacks
	,ReceivedDate
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderTime
	,LocationCode
	,NSVCode
	,OutstandingPacks
	,Created
	,ByWhom
)
select
	 InterfaceCode
	,RequisitionID
	,SiteCode
	,Items
	,PickNo
	,InDisputeUser
	,ShelfPrinted
	,RequisitionNo
	,TaxRatePct
	,TaxInclusive
	,InDispute
	,CustomerOrderNo
	,TaxAmount
	,TaxRateCode
	,PFlag
	,CreatedUser
	,ConversionFactor
	,IssueForm
	,Stocked
	,InternalSiteNo
	,InternalMethodCode
	,SupplierTypeCode
	,OrderedPacks
	,Urgency
	,ToFollow
	,InvoiceNumber
	,PayDate
	,Cost
	,ReceivedPacks
	,ReceivedDate
	,SupplierCode
	,StatusCode
	,NumberPrefix
	,OrderTime
	,LocationCode
	,NSVCode
	,OutstandingPacks
	,Created = getdate()
	,ByWhom = system_user
from
	Pharmacy.TLoadRequisition


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.LoadRequisition', @Stats, @StartTime



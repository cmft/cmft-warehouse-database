﻿create procedure [Pharmacy].[LoadOrderLog] as

set dateformat dmy

declare @StartTime datetime
declare @Elapsed int
declare @RowsInserted Int
declare @Stats varchar(255)
declare @from datetime
declare @to datetime

select @StartTime = getdate()

insert into Pharmacy.OrderLog
(
	 InterfaceCode
	,OrderLogID
	,OrderNumber
	,ConversionFactor
	,IssueForm
	,DispId
	,Terminal
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,ReceivedPacks
	,SupplierCode
	,BatchNumber
	,ExpiryDate
	,InvoiceNumber
	,LinkedNumber
	,ReasonCode
	,StockLevel
	,CreatedByCode
	,DateOrdered
	,DateReceived
	,SiteID
	,NSVCode
	,OrderLogTime
	,OrderKindCode
	,Cost
	,CostExTax
	,TaxCost
	,TaxCode
	,TaxRate
	,StockValue
	,DateInvoiced
	,Created
	,ByWhom
)
select
	 InterfaceCode
	,OrderLogID
	,OrderNumber
	,ConversionFactor
	,IssueForm
	,DispId
	,Terminal
	,OrderTime
	,ReceivedDate
	,OrderedPacks
	,ReceivedPacks
	,SupplierCode
	,BatchNumber
	,ExpiryDate
	,InvoiceNumber
	,LinkedNumber
	,ReasonCode
	,StockLevel
	,CreatedByCode
	,DateOrdered
	,DateReceived
	,SiteID
	,NSVCode
	,OrderLogTime
	,OrderKindCode
	,Cost
	,CostExTax
	,TaxCost
	,TaxCode
	,TaxRate
	,StockValue
	,DateInvoiced
	,Created = getdate()
	,ByWhom = system_user
from
	Pharmacy.TLoadOrderLog


select @RowsInserted = @@rowcount


select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Rows inserted ' + CONVERT(varchar(10), @RowsInserted) + ', Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec WriteAuditLogEvent 'Pharmacy.LoadOrderLog', @Stats, @StartTime



﻿CREATE procedure [Pharmacy].[ExtractAscribePharmacyReferenceData]
	 @interfaceCode varchar(10)
as


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


set dateformat dmy


if @interfaceCode = 'PHAT'
begin

	--Tax Rate
	merge 
		Pharmacy.TaxRate target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,TaxCode =
				SUBSTRING(
					 TaxRate.[Key]
					,CHARINDEX('(', TaxRate.[Key]) + 1
					,1
				)

			,TaxRate =
				CONVERT(
					FLOAT
					,replace(
						TaxRate.[Value]
						,'"'
						,''
					)
				)
		from
			[$(Pharmacy_Trafford)].icwsys.wConfiguration TaxRate
		where
			TaxRate.Category = 'D|Workingdefaults'
		and	left(TaxRate.[Key], 3) = 'VAT'
		and	not exists
			(
			select
				1
			from
				[$(Pharmacy_Trafford)].icwsys.wConfiguration Previous
			where
				Previous.Category = 'D|Workingdefaults'
			and	left(Previous.[Key], 3) = 'VAT'
			and	Previous.SiteID < TaxRate.SiteID
			)
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.TaxCode = target.TaxCode

	when matched then

		update
		set
			 TaxRate = source.TaxRate

	when not matched then

		insert
		(
			 InterfaceCode
			,TaxCode
			,TaxRate
		)
		values
		(
			 source.InterfaceCode
			,source.TaxCode
			,source.TaxRate
		)
	;

	--Site
	merge 
		Pharmacy.Site target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SiteCode = Site.SiteNumber
			,Site = Location.Description
			,SiteID = Site.LocationID
		from
			[$(Pharmacy_Trafford)].icwsys.Site

		inner join [$(Pharmacy_Trafford)].icwsys.Location
		on	Location.LocationID = Site.LocationID

		where
			Site.SiteNumber <> 0
		and	Location._Deleted = 'false'
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SiteCode = target.SiteCode

	when matched then

		update
		set
			 Site = source.Site
			,SiteID = source.SiteID

	when not matched then

		insert
		(
			 InterfaceCode
			,SiteCode
			,Site
			,SiteID
		)
		values
		(
			 source.InterfaceCode
			,source.SiteCode
			,source.Site
			,source.SiteID
		)
	;

	--SystemUser
	merge 
		Pharmacy.SystemUser target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SystemUserCode = SystemUser.EntityID
			,Surname = Person.Surname
			,Forename = Person.Forename
			,Initials = Person.Initials
			,Username = SystemUser.Username
		from
			[$(Pharmacy_Trafford)].icwsys.[User] SystemUser

		inner join [$(Pharmacy_Trafford)].icwsys.Person
		on	Person.EntityID = SystemUser.EntityID
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SystemUserCode = target.SystemUserCode

	when matched then

		update
		set
			 Surname = source.Surname
			,Forename = source.Forename
			,Initials = source.Initials
			,Username = source.Username

	when not matched then

		insert
		(
			 InterfaceCode
			,SystemUserCode
			,Surname
			,Forename
			,Initials
			,Username
			,SystemUserGroupCode

		)
		values
		(
			 source.InterfaceCode
			,source.SystemUserCode
			,source.Surname
			,source.Forename
			,source.Initials
			,source.Username
			,'UNASSIGNED'
		)
	;


	--Consultant
	merge 
		Pharmacy.Consultant target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,ConsultantCode = Consultant.EntityID
			,Surname = case when rtrim(Person.Surname) = '' then null else Person.Surname end
			,Forename = case when rtrim(Person.Forename) = '' then null else Person.Forename end
			,Initials = case when rtrim(Person.Initials) = '' then null else Person.Initials end
			,GMCCode = case when rtrim(Consultant.GMCCode) = '' then null else Consultant.GMCCode end
			,TelephoneNo = case when rtrim(Person.Mobile) = '' then null else Person.Mobile end
		from
			[$(Pharmacy_Trafford)].icwsys.Consultant Consultant

		inner join [$(Pharmacy_Trafford)].icwsys.Person
		on	Person.EntityID = Consultant.EntityID
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.ConsultantCode = target.ConsultantCode

	when matched then

		update
		set
			 Surname = source.Surname
			,Forename = source.Forename
			,Initials = source.Initials
			,GMCCode = source.GMCCode
			,TelephoneNo = source.TelephoneNo

	when not matched then

		insert
		(
			 InterfaceCode
			,ConsultantCode
			,Surname
			,Forename
			,Initials
			,GMCCode
			,TelephoneNo
		)
		values
		(
			 source.InterfaceCode
			,source.ConsultantCode
			,source.Surname
			,source.Forename
			,source.Initials
			,source.GMCCode
			,source.TelephoneNo
		)
	;


	--Specialty
	merge 
		Pharmacy.Specialty target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SpecialtyCode = Specialty.SpecialtyID
			,Specialty = Specialty.Description
			,Detail = Specialty.Detail
			,LocalSpecialtyCode =
				(
				select
					SpecialtyAlias.Alias
				from
					[$(Pharmacy_Trafford)].icwsys.SpecialtyAlias

				inner join [$(Pharmacy_Trafford)].icwsys.AliasGroup
				on	AliasGroup.AliasGroupID = SpecialtyAlias.AliasGroupID
				and	AliasGroup.Description = 'WSpecialtyCodes'

				where
					SpecialtyAlias.SpecialtyID = Specialty.SpecialtyID
				)

			,NationalSpecialtyCode =
				(
				select
					SpecialtyAlias.Alias
				from
					[$(Pharmacy_Trafford)].icwsys.SpecialtyAlias

				inner join [$(Pharmacy_Trafford)].icwsys.AliasGroup
				on	AliasGroup.AliasGroupID = SpecialtyAlias.AliasGroupID
				and	AliasGroup.Description = 'PasSpecialtyCode'

				where
					SpecialtyAlias.SpecialtyID = Specialty.SpecialtyID
				)
			,ArchiveFlag = Specialty.out_of_use

			,DirectorateCode = CAST(0 as int)
			,DivisionCode = CAST(14 as int)
			,TrustCode = CAST(1 as int)
		from
			[$(Pharmacy_Trafford)].icwsys.Specialty Specialty
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SpecialtyCode = target.SpecialtyCode

	when matched then

		update
		set
			 Specialty = source.Specialty
			,Detail = source.Detail
			,LocalSpecialtyCode = source.LocalSpecialtyCode
			,NationalSpecialtyCode = source.NationalSpecialtyCode
			,ArchiveFlag = source.ArchiveFlag
			,DirectorateCode = source.DirectorateCode
			,DivisionCode = source.DivisionCode
			,TrustCode = source.TrustCode

	when not matched then

		insert
		(
			 InterfaceCode
			,SpecialtyCode
			,Specialty
			,Detail
			,LocalSpecialtyCode
			,NationalSpecialtyCode
			,ArchiveFlag
			,DirectorateCode
			,DivisionCode
			,TrustCode
		)
		values
		(
			 source.InterfaceCode
			,source.SpecialtyCode
			,source.Specialty
			,source.Detail
			,source.LocalSpecialtyCode
			,source.NationalSpecialtyCode
			,source.ArchiveFlag
			,source.DirectorateCode
			,source.DivisionCode
			,source.TrustCode

		)
	;

	--Supplier
	merge 
		Pharmacy.Supplier target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SupplierCode = [WSupplierID]
			,LocalSupplierCode = [Code]
			,ContractAddress = case when RTRIM([ContractAddress]) = '' then null else [ContractAddress] end
			,SupplierAddress = case when RTRIM([SupAddress]) = '' then null else [SupAddress] end
			,InvoiceAddress = case when RTRIM([InvAddress]) = '' then null else [InvAddress] end
			,ContactTelephoneNo = case when RTRIM([ContTelNo]) = '' then null else [ContTelNo] end
			,SupplierTelephoneNo = case when RTRIM([SupTelNo]) = '' then null else [SupTelNo] end
			,InvoiceTelephoneNo = case when RTRIM([InvTelNo]) = '' then null else [InvTelNo] end
			,DiscountDetail = case when RTRIM([DiscountDesc]) = '' then null else [DiscountDesc] end
			,DiscountValue = case when RTRIM([DiscountVal]) = '' then null else [DiscountVal] end
			,OrderMethod = case when RTRIM([Method]) = '' then null else [Method] end
			,OrderMessage = case when RTRIM([OrdMessage]) = '' then null else [OrdMessage] end
			,AverageLeadTime = case when RTRIM([AvLeadTime]) = '' then null else [AvLeadTime] end
			,ContactFaxNo = case when RTRIM([ContFaxNo]) = '' then null else [ContFaxNo] end
			,SupplierFaxNo = case when RTRIM([SupFaxNo]) = '' then null else [SupFaxNo] end
			,InvoiceFaxNo = case when RTRIM([InvFaxNo]) = '' then null else [InvFaxNo] end
			,Supplier = case when RTRIM([Name]) = '' then null else [Name] end
			,PrintTradeName = case when RTRIM([Ptn]) = '' then null else [Ptn] end
			,PrintNSVCode = case when RTRIM([PSis]) = '' then null else [PSis] end
			,SupplierFullName = case when RTRIM([FullName]) = '' then null else [FullName] end
			,DiscountBelow = case when RTRIM([DiscountBelow]) = '' then null else [DiscountBelow] end
			,DiscountAbove = case when RTRIM([DiscountAbove]) = '' then null else [DiscountAbove] end
			,InternalAscribeSiteCode = case when RTRIM([ICode]) = '' then null else [ICode] end
			,CostCentreCode = case when RTRIM([CostCentre]) = '' then null else [CostCentre] end
			,PrintDeliveryNote = case when RTRIM([PrintDeliveryNote]) = '' then null else [PrintDeliveryNote] end
			,PrintPickingTicket = case when RTRIM([PrintPickTicket]) = '' then null else [PrintPickTicket] end
			,SupplierTypeCode = case when RTRIM([SupplierType]) = '' then null else [SupplierType] end
			,OrderOutput = case when RTRIM([OrderOutput]) = '' then null else [OrderOutput] end
			,ReceiveGoods = case when RTRIM([ReceiveGoods]) = '' then null else [ReceiveGoods] end
			,TopupInterval = case when RTRIM([TopupInterval]) = '' then null else [TopupInterval] end
			,ATCSupplied = case when RTRIM([AtcSupplied]) = '' then null else [AtcSupplied] end
			,TopupDate = case when RTRIM([TopupDate]) = '' then null else [TopupDate] end
			,InUse = case when RTRIM([InUse]) = '' then null else [InUse] end
			,WardCode = case when RTRIM([WardCode]) = '' then null else [WardCode] end
			,OnCostPercent = case when RTRIM([OnCost]) = '' then null else [OnCost] end
			,InPatientDirections = case when RTRIM([InPatientDirections]) = '' then null else [InPatientDirections] end
			,AdHocDeliveryNote = case when RTRIM([AdHocDelNote]) = '' then null else [AdHocDelNote] end
			,SiteID = case when RTRIM([SiteID]) = '' then null else [SiteID] end
			,MinimumOrderValue = case when RTRIM([MinimumOrderValue]) = '' then null else [MinimumOrderValue] end
		from
			[$(Pharmacy_Trafford)].icwsys.WSupplier Supplier
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SupplierCode = target.SupplierCode

	when matched then

		update
		set
			 LocalSupplierCode = source.LocalSupplierCode
			,ContractAddress = source.ContractAddress
			,SupplierAddress = source.SupplierAddress
			,InvoiceAddress = source.InvoiceAddress
			,ContactTelephoneNo = source.ContactTelephoneNo
			,SupplierTelephoneNo = source.SupplierTelephoneNo
			,InvoiceTelephoneNo = source.InvoiceTelephoneNo
			,DiscountDetail = source.DiscountDetail
			,DiscountValue = source.DiscountValue
			,OrderMethod = source.OrderMethod
			,OrderMessage = source.OrderMessage
			,AverageLeadTime = source.AverageLeadTime
			,ContactFaxNo = source.ContactFaxNo
			,SupplierFaxNo = source.SupplierFaxNo
			,InvoiceFaxNo = source.InvoiceFaxNo
			,Supplier = source.Supplier
			,PrintTradeName = source.PrintTradeName
			,PrintNSVCode = source.PrintNSVCode
			,SupplierFullName = source.SupplierFullName
			,DiscountBelow = source.DiscountBelow
			,DiscountAbove = source.DiscountAbove
			,InternalAscribeSiteCode = source.InternalAscribeSiteCode
			,CostCentreCode = source.CostCentreCode
			,PrintDeliveryNote = source.PrintDeliveryNote
			,PrintPickingTicket = source.PrintPickingTicket
			,SupplierTypeCode = source.SupplierTypeCode
			,OrderOutput = source.OrderOutput
			,ReceiveGoods = source.ReceiveGoods
			,TopupInterval = source.TopupInterval
			,ATCSupplied = source.ATCSupplied
			,TopupDate = source.TopupDate
			,InUse = source.InUse
			,WardCode = source.WardCode
			,OnCostPercent = source.OnCostPercent
			,InPatientDirections = source.InPatientDirections
			,AdHocDeliveryNote = source.AdHocDeliveryNote
			,SiteID = source.SiteID
			,MinimumOrderValue = source.MinimumOrderValue

	when not matched then

		insert
		(
			 InterfaceCode
			,SupplierCode
			,LocalSupplierCode
			,ContractAddress
			,SupplierAddress
			,InvoiceAddress
			,ContactTelephoneNo
			,SupplierTelephoneNo
			,InvoiceTelephoneNo
			,DiscountDetail
			,DiscountValue
			,OrderMethod
			,OrderMessage
			,AverageLeadTime
			,ContactFaxNo
			,SupplierFaxNo
			,InvoiceFaxNo
			,Supplier
			,PrintTradeName
			,PrintNSVCode
			,SupplierFullName
			,DiscountBelow
			,DiscountAbove
			,InternalAscribeSiteCode
			,CostCentreCode
			,PrintDeliveryNote
			,PrintPickingTicket
			,SupplierTypeCode
			,OrderOutput
			,ReceiveGoods
			,TopupInterval
			,ATCSupplied
			,TopupDate
			,InUse
			,WardCode
			,OnCostPercent
			,InPatientDirections
			,AdHocDeliveryNote
			,SiteID
			,MinimumOrderValue
		)
		values
		(
			 source.InterfaceCode
			,source.SupplierCode
			,source.LocalSupplierCode
			,source.ContractAddress
			,source.SupplierAddress
			,source.InvoiceAddress
			,source.ContactTelephoneNo
			,source.SupplierTelephoneNo
			,source.InvoiceTelephoneNo
			,source.DiscountDetail
			,source.DiscountValue
			,source.OrderMethod
			,source.OrderMessage
			,source.AverageLeadTime
			,source.ContactFaxNo
			,source.SupplierFaxNo
			,source.InvoiceFaxNo
			,source.Supplier
			,source.PrintTradeName
			,source.PrintNSVCode
			,source.SupplierFullName
			,source.DiscountBelow
			,source.DiscountAbove
			,source.InternalAscribeSiteCode
			,source.CostCentreCode
			,source.PrintDeliveryNote
			,source.PrintPickingTicket
			,source.SupplierTypeCode
			,source.OrderOutput
			,source.ReceiveGoods
			,source.TopupInterval
			,source.ATCSupplied
			,source.TopupDate
			,source.InUse
			,source.WardCode
			,source.OnCostPercent
			,source.InPatientDirections
			,source.AdHocDeliveryNote
			,source.SiteID
			,source.MinimumOrderValue
		)
	;


--load new unassigned wards into Ward table
	insert into Pharmacy.Ward
	(
		 WardRecno
		,SpecialtyRecno
		,StockReplenishmentServiceCode
		,WardTypeCode
		,InUse
		,Divided
		,OneStopService
		,StockAvailableOutOfHours
		,OutOfHoursLocationPriority
	)
	select
		 WardRecno = Supplier.SupplierRecno
		,SpecialtyRecno = 1 --unassigned
		,StockReplenishmentServiceCode = 'N/A'
		,WardTypeCode = '08'
		,InUse = Supplier.InUse

		,Divided = 0

		,OneStopService = 0

		,StockAvailableOutOfHours = 0

		,OutOfHoursLocationPriority = 0
	from
		Pharmacy.Supplier

	where
		Supplier.SupplierTypeCode in
		(
		 'W'
		,'L'
		)

	and	Supplier.InterfaceCode = 'PHAT'
	and	Supplier.SiteID = 15

	and	not exists
		(
		select
			1
		from
			Pharmacy.Ward
		where
			Ward.WardRecno = Supplier.SupplierRecno
		)

	--Patient
	merge 
		Pharmacy.Patient target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SourcePatientNo = Patient.EntityID
			,Title = case when RTRIM(Person.Title) = '' then null else Person.Title end
			,Initials = case when RTRIM(Person.Initials) = '' then null else Person.Initials end
			,Forename = case when RTRIM(Person.Forename) = '' then null else Person.Forename end
			,Surname = case when RTRIM(Person.Surname) = '' then null else Person.Surname end
			,DateOfBirth = case when RTRIM(Patient.DOB) = '' then null else Patient.DOB end
			,NHSNumber = case when RTRIM(NHSNumber.Alias) = '' then null else NHSNumber.Alias end
			,NHSNumberValidationStatusCode = case when RTRIM(NHSNumber.IsValid) = '' then null else NHSNumber.IsValid end
			,SexCode = case when RTRIM(Patient.GenderID) = '' then null else Patient.GenderID end
			,CasenoteNo = CasenoteNo.Alias
		from
			[$(Pharmacy_Trafford)].icwsys.Patient

		inner join [$(Pharmacy_Trafford)].icwsys.Person
		on	Person.EntityID = Patient.EntityID

		left join
			(
			select
				 NHSNumber.EntityID
				,NHSNumber.Alias
				,NHSNumber.IsValid
			from
				[$(Pharmacy_Trafford)].icwsys.EntityAlias NHSNumber

			inner join [$(Pharmacy_Trafford)].icwsys.AliasGroup
			on	AliasGroup.AliasGroupID = NHSNumber.AliasGroupID
			and	AliasGroup.Description = 'NHSNumber'

			inner join [$(Pharmacy_Trafford)].icwsys.Setting
			on	Setting.Value = AliasGroup.Description
			and	Setting.System = 'General'
			and	Setting.Section = 'PatientEditor'
			and	Setting.[Key] = 'PrimaryPatientIdentifier'
			and	Setting.RoleID = 0

			where
				NHSNumber.[Default] = 1

			) NHSNumber
		on	NHSNumber.EntityID = Patient.EntityID


		left join
			(
			select
				 CasenoteNo.EntityID
				,CasenoteNo.Alias
			from
				[$(Pharmacy_Trafford)].icwsys.EntityAlias CasenoteNo

			inner join [$(Pharmacy_Trafford)].icwsys.AliasGroup
			on	AliasGroup.AliasGroupID = CasenoteNo.AliasGroupID
			and	AliasGroup.Description = 'CaseNumber'

			where
				CasenoteNo.[Default] = 1
			) CasenoteNo
		on	CasenoteNo.EntityID = Patient.EntityID

		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SourcePatientNo = target.SourcePatientNo

	when matched then

		update
		set
			 Title = source.Title
			,Initials = source.Initials
			,Forename = source.Forename
			,Surname = source.Surname
			,DateOfBirth = source.DateOfBirth
			,NHSNumber = source.NHSNumber
			,NHSNumberValidationStatusCode = source.NHSNumberValidationStatusCode
			,SexCode = source.SexCode
			,CasenoteNo = source.CasenoteNo

	when not matched then

		insert
		(
			 InterfaceCode
			,SourcePatientNo
			,Title
			,Initials
			,Forename
			,Surname
			,DateOfBirth
			,NHSNumber
			,NHSNumberValidationStatusCode
			,SexCode
			,CasenoteNo
		)
		values
		(
			 source.InterfaceCode
			,source.SourcePatientNo
			,source.Title
			,source.Initials
			,source.Forename
			,source.Surname
			,source.DateOfBirth
			,source.NHSNumber
			,source.NHSNumberValidationStatusCode
			,source.SexCode
			,source.CasenoteNo
		)
	;

	--Address
	merge 
		Pharmacy.Address target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,AddressCode = Address.AddressID
			,BoxNumber = case when RTRIM(BoxNumber) = '' then null else BoxNumber end
			,DoorNumber = case when RTRIM(DoorNumber) = '' then null else DoorNumber end
			,Street = case when RTRIM(Street) = '' then null else Street end
			,Town = case when RTRIM(Town) = '' then null else Town end
			,LocalAuthority = case when RTRIM(LocalAuthority) = '' then null else LocalAuthority end
			,District = case when RTRIM(District) = '' then null else District end
			,Postcode = case when RTRIM(PostCode) = '' then null else PostCode end
			,Province = case when RTRIM(Province) = '' then null else Province end
			,Country = case when RTRIM(Country) = '' then null else Country end
		from
			[$(Pharmacy_Trafford)].icwsys.Address
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.AddressCode = target.AddressCode

	when matched then

		update
		set
			 BoxNumber = source.BoxNumber
			,DoorNumber = source.DoorNumber
			,Street = source.Street
			,Town = source.Town
			,LocalAuthority = source.LocalAuthority
			,District = source.District
			,Postcode = source.Postcode
			,Province = source.Province
			,Country = source.Country

	when not matched then

		insert
		(
			 InterfaceCode
			,AddressCode
			,BoxNumber
			,DoorNumber
			,Street
			,Town
			,LocalAuthority
			,District
			,Postcode
			,Province
			,Country
		)
		values
		(
			 source.InterfaceCode
			,source.AddressCode
			,source.BoxNumber
			,source.DoorNumber
			,source.Street
			,source.Town
			,source.LocalAuthority
			,source.District
			,source.Postcode
			,source.Province
			,source.Country
		)
	;

	--PatientAddressMap
	merge 
		Pharmacy.PatientAddressMap target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SourcePatientNo = EntityLinkAddress.EntityID
			,AddressCode = EntityLinkAddress.AddressID
			,AddressTypeCode = EntityLinkAddress.AddressTypeID
		from
			[$(Pharmacy_Trafford)].icwsys.EntityLinkAddress
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SourcePatientNo = target.SourcePatientNo
	and	source.AddressCode = target.AddressCode
	and	source.AddressTypeCode = target.AddressTypeCode

	when not matched then

		insert
		(
			 InterfaceCode
			,SourcePatientNo
			,AddressCode
			,AddressTypeCode
		)
		values
		(
			 source.InterfaceCode
			,source.SourcePatientNo
			,source.AddressCode
			,source.AddressTypeCode
		)
	;

	--AddressType
	merge 
		Pharmacy.AddressType target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,AddressTypeCode = AddressType.AddressTypeID
			,AddressType = AddressType.Description
		from
			[$(Pharmacy_Trafford)].icwsys.AddressType
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.AddressTypeCode = target.AddressTypeCode

	when matched then

		update
		set
			AddressType = source.AddressType

	when not matched then

		insert
		(
			 InterfaceCode
			,AddressTypeCode
			,AddressType
		)
		values
		(
			 source.InterfaceCode
			,source.AddressTypeCode
			,source.AddressType
		)
	;


	--Terminal
	merge 
		Pharmacy.Terminal target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,TerminalCode = Terminal.LocationID
			,Terminal = Terminal.ComputerName
			,TerminalTypeCode = Terminal.TerminalTypeID
			,DomainName = Terminal.DomainName
		from
			[$(Pharmacy_Trafford)].icwsys.Terminal
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.TerminalCode = target.TerminalCode

	when matched then

		update
		set
			 Terminal = source.Terminal
			,TerminalTypeCode = source.TerminalTypeCode
			,DomainName = source.DomainName

	when not matched then

		insert
		(
			 InterfaceCode
			,TerminalCode
			,Terminal
			,TerminalTypeCode
			,DomainName
		)
		values
		(
			 source.InterfaceCode
			,source.TerminalCode
			,source.Terminal
			,source.TerminalTypeCode
			,source.DomainName
		)
	;

	--Product
	merge 
		Pharmacy.Product target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SiteProductCode  = case when RTRIM(SiteProduct.SiteProductDataID) = '' then null else SiteProduct.SiteProductDataID end
			,ProductCode  = case when RTRIM(SiteProduct.ProductID) = '' then null else SiteProduct.ProductID end
			,NSVCode  = case when RTRIM(SiteProduct.siscode) = '' then null else SiteProduct.siscode end
			,Product  = case when RTRIM(SiteProduct.labeldescription) = '' then null else SiteProduct.labeldescription end
			,TradeName  = case when RTRIM(SiteProduct.tradename) = '' then null else SiteProduct.tradename end
			,StoresDescription  = case when RTRIM(SiteProduct.storesdescription) = '' then null else SiteProduct.storesdescription end
			,ReorderPackSize  = case when RTRIM(SiteProduct.convfact) = '' then null else SiteProduct.convfact end
			,MlsPerPack  = case when RTRIM(SiteProduct.mlsperpack) = '' then null else SiteProduct.mlsperpack end
			,Cytotoxic  = case when RTRIM(SiteProduct.cyto) = '' then null else SiteProduct.cyto end
			,PrimaryWarningCode  = case when RTRIM(SiteProduct.warcode) = '' then null else SiteProduct.warcode end
			,SecondaryWarningCode  = case when RTRIM(SiteProduct.warcode2) = '' then null else SiteProduct.warcode2 end
			,InstructionCode  = case when RTRIM(SiteProduct.inscode) = '' then null else SiteProduct.inscode end
			,DosesPerIssueUnit  = case when RTRIM(SiteProduct.DosesperIssueUnit) = '' then null else SiteProduct.DosesperIssueUnit end
			,DosingUnits  = case when RTRIM(SiteProduct.DosingUnits) = '' then null else SiteProduct.DosingUnits end
			,DPSForm  = case when RTRIM(SiteProduct.DPSForm) = '' then null else SiteProduct.DPSForm end
			,BNFCode  = case when RTRIM(SiteProduct.BNF) = '' then null else SiteProduct.BNF end
			,ProductBarCode  = case when RTRIM(SiteProduct.barcode) = '' then null else SiteProduct.barcode end

			,SupplementaryBarCode1 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Trafford)].icwsys.SiteProductData

					left join [$(Pharmacy_Trafford)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Trafford)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 1
				)

			,SupplementaryBarCode2 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Trafford)].icwsys.SiteProductData

					left join [$(Pharmacy_Trafford)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Trafford)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 2
				)

			,SupplementaryBarCode3 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Trafford)].icwsys.SiteProductData

					left join [$(Pharmacy_Trafford)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Trafford)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 3
				)

			,SupplementaryBarCode4 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Trafford)].icwsys.SiteProductData

					left join [$(Pharmacy_Trafford)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Trafford)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 4
				)

			,SupplementaryBarCode5 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Trafford)].icwsys.SiteProductData

					left join [$(Pharmacy_Trafford)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Trafford)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 5
				)

			,CatalogueDescription = case when RTRIM(OrderCatalogue.CatalogueDescription) = '' then null else OrderCatalogue.CatalogueDescription end
			,ReportGroup = case when RTRIM(ReportGroup.Description) = '' then null else ReportGroup.Description end
			,IssueForm  = case when RTRIM(SiteProduct.printformv) = '' then null else SiteProduct.printformv end

			,TaxRate.TaxRate

		from
			[$(Pharmacy_Trafford)].icwsys.SiteProductData SiteProduct

		left join 
			(
			select
				 BNFCode = OrderCatalogue.Description
				,CatalogueDescription = OrderCatalogue.Detail
			from
				[$(Pharmacy_Trafford)].icwsys.OrderCatalogue

			inner join [$(Pharmacy_Trafford)].icwsys.OrderCatalogueRoot
			on	OrderCatalogueRoot.OrderCatalogueRootID = OrderCatalogue.OrderCatalogueRootID
			and	OrderCatalogueRoot.Description = 'BNF'
			) OrderCatalogue
		on	OrderCatalogue.BNFCode = SiteProduct.BNF

		left join [$(Pharmacy_Trafford)].icwsys.ProductCustomisation
		on	ProductCustomisation.ProductID = SiteProduct.ProductID
		and	not exists
			(
			select
				1
			from
				[$(Pharmacy_Trafford)].icwsys.ProductCustomisation Previous
			where
				Previous.ProductID = SiteProduct.ProductID
			and	Previous.ProductCustomisationID < ProductCustomisation.ProductCustomisationID
			)

		left join [$(Pharmacy_Trafford)].icwsys.ReportGroup
		on	ReportGroup.ReportGroupID = ProductCustomisation.ReportGroupID

		inner join [$(Pharmacy_Trafford)].icwsys.wProduct Product
		on	Product.siscode = SiteProduct.siscode

		inner join [$(Pharmacy_Trafford)].icwsys.Site
		on	Site.LocationID = Product.LocationID_Site

		inner join
			(
			select
				TaxCode =
					SUBSTRING(
						 TaxRate.[Key]
						,CHARINDEX('(', TaxRate.[Key]) + 1
						,1
					)

				,TaxRate =
					CONVERT(
						FLOAT
						,replace(
							TaxRate.[Value]
							,'"'
							,''
						)
					)
			from
				[$(Pharmacy_Trafford)].icwsys.wConfiguration TaxRate
			where
				TaxRate.Category = 'D|Workingdefaults'
			and	left(TaxRate.[Key], 3) = 'VAT'
			and	not exists
				(
				select
					1
				from
					[$(Pharmacy_Trafford)].icwsys.wConfiguration Previous
				where
					Previous.Category = 'D|Workingdefaults'
				and	left(Previous.[Key], 3) = 'VAT'
				and	Previous.SiteID < TaxRate.SiteID
				)
			) TaxRate
		on	TaxRate.TaxCode = coalesce(Product.vatrate, 1)



		where
			SiteProduct.DSSMasterSiteID <> 0
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SiteProductCode = target.SiteProductCode

	when matched then

		update
		set
			 ProductCode = source.ProductCode
			,NSVCode = source.NSVCode
			,Product = source.Product
			,TradeName = source.TradeName
			,StoresDescription = source.StoresDescription
			,ReorderPackSize = source.ReorderPackSize
			,MlsPerPack = source.MlsPerPack
			,Cytotoxic = source.Cytotoxic
			,PrimaryWarningCode = source.PrimaryWarningCode
			,SecondaryWarningCode = source.SecondaryWarningCode
			,InstructionCode = source.InstructionCode
			,DosesPerIssueUnit = source.DosesPerIssueUnit
			,DosingUnits = source.DosingUnits
			,DPSForm = source.DPSForm
			,BNFCode = source.BNFCode
			,ProductBarCode = source.ProductBarCode
			,SupplementaryBarCode1 = source.SupplementaryBarCode1
			,SupplementaryBarCode2 = source.SupplementaryBarCode2
			,SupplementaryBarCode3 = source.SupplementaryBarCode3
			,SupplementaryBarCode4 = source.SupplementaryBarCode4
			,SupplementaryBarCode5 = source.SupplementaryBarCode5
			,CatalogueDescription = source.CatalogueDescription
			,ReportGroup = source.ReportGroup
			,IssueForm = source.IssueForm
			,TaxRate = source.TaxRate

	when not matched then

		insert
		(
			 InterfaceCode
			,SiteProductCode
			,ProductCode
			,NSVCode
			,Product
			,TradeName
			,StoresDescription
			,ReorderPackSize
			,MlsPerPack
			,Cytotoxic
			,PrimaryWarningCode
			,SecondaryWarningCode
			,InstructionCode
			,DosesPerIssueUnit
			,DosingUnits
			,DPSForm
			,BNFCode
			,ProductBarCode
			,SupplementaryBarCode1
			,SupplementaryBarCode2
			,SupplementaryBarCode3
			,SupplementaryBarCode4
			,SupplementaryBarCode5
			,CatalogueDescription
			,ReportGroup
			,IssueForm
			,TaxRate

		)
		values
		(
			 source.InterfaceCode
			,source.SiteProductCode
			,source.ProductCode
			,source.NSVCode
			,source.Product
			,source.TradeName
			,source.StoresDescription
			,source.ReorderPackSize
			,source.MlsPerPack
			,source.Cytotoxic
			,source.PrimaryWarningCode
			,source.SecondaryWarningCode
			,source.InstructionCode
			,source.DosesPerIssueUnit
			,source.DosingUnits
			,source.DPSForm
			,source.BNFCode
			,source.ProductBarCode
			,source.SupplementaryBarCode1
			,source.SupplementaryBarCode2
			,source.SupplementaryBarCode3
			,source.SupplementaryBarCode4
			,source.SupplementaryBarCode5
			,source.CatalogueDescription
			,source.ReportGroup
			,source.IssueForm
			,source.TaxRate
		)
	;

	--ProductStock
	merge 
		Pharmacy.ProductStock target
	using
		(
		select
			 InterfaceCode
			,ProductStockCode = cast(ProductStockCode as int)
			,DrugID = cast(DrugID as int)
			,InternalSiteCode = cast(InternalSiteCode as int)
			,SiteCode = cast(SiteCode as int)
			,AnnualUse = cast(AnnualUse as float)
			,ReorderLevelUnits = cast(ReorderLevelUnits as varchar(8))
			,ReorderQuantity = cast(ReorderQuantity as varchar(6))
			,StockLevel = cast(StockLevel as float)
			,ArchiveFlag = cast(ArchiveFlag as bit)
			,Formulary = cast(Formulary as varchar(1))
			,OrderCycle = cast(OrderCycle as varchar(2))
			,SupplierCode = cast(SupplierCode as varchar(5))
			,LastOrderedDateText = cast(LastOrderedDateText as varchar(8))
			,StockTakeStatus = cast(StockTakeStatus as varchar(1))
			,IssuePrice = cast(IssuePrice as float)
			,LossesGains = cast(LossesGains as float)
			,LocalCode = cast(LocalCode as varchar(20))
			,BatchTracking = cast(BatchTracking as varchar(1))
			,LastIssuedDate = cast(LastIssuedDate as varchar(8))
			,LastStocktakeDate = cast(LastStocktakeDate as varchar(8))
			,DrugSubGroupCode = cast(DrugSubGroupCode as varchar(20))
			,DrugGroupCode = cast(DrugGroupCode as varchar(2))
			,UseThisPeriodUnits = cast(UseThisPeriodUnits as float)
			,LastPeriodEndDate = cast(LastPeriodEndDate as varchar(8))
			,PrimaryLocationCode = cast(PrimaryLocationCode as varchar(3))
			,SecondaryLocationCode = cast(SecondaryLocationCode as varchar(3))
			,NSVCode = cast(NSVCode as varchar(7))
			,ExtraMessage = cast(ExtraMessage as varchar(255))
			,Stocked = cast(Stocked as varchar(1))
			,DefinedDailyDoseValue = cast(DefinedDailyDoseValue as varchar(10))
			,DefinedDailyDoseUnits = cast(DefinedDailyDoseUnits as varchar(10))
			,UserField1 = cast(UserField1 as varchar(10))
			,UserField2 = cast(UserField2 as varchar(10))
			,UserField3 = cast(UserField3 as varchar(10))
			,PIPCode = cast(PIPCode as varchar(7))
			,MasterPIPCode = cast(MasterPIPCode as varchar(7))
			,HighCostProduct = cast(HighCostProduct as char(1))
			,EDILinkCode = cast(EDILinkCode as varchar(13))
			,OuterPackSize = cast(OuterPackSize as varchar(5))
			,SiteProductCode = cast(SiteProductCode as int)
		from
			Pharmacy.TLoadTraProductStock
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.ProductStockCode = target.ProductStockCode

	when matched 
	and not
		(
			isnull(target.DrugID, 0) = isnull(source.DrugID, 0)
		and isnull(target.InternalSiteCode, 0) = isnull(source.InternalSiteCode, 0)
		and isnull(target.SiteCode, 0) = isnull(source.SiteCode, 0)
		and isnull(target.AnnualUse, 0) = isnull(source.AnnualUse, 0)
		and isnull(target.ReorderLevelUnits, '') = isnull(source.ReorderLevelUnits, '')
		and isnull(target.ReorderQuantity, '') = isnull(source.ReorderQuantity, '')
		and isnull(target.StockLevel, 0) = isnull(source.StockLevel, 0)
		and isnull(target.ArchiveFlag, '') = isnull(source.ArchiveFlag, '')
		and isnull(target.Formulary, '') = isnull(source.Formulary, '')
		and isnull(target.OrderCycle, '') = isnull(source.OrderCycle, '')
		and isnull(target.SupplierCode, '') = isnull(source.SupplierCode, '')
		and isnull(target.LastOrderedDateText, '') = isnull(source.LastOrderedDateText, '')
		and isnull(target.StockTakeStatus, '') = isnull(source.StockTakeStatus, '')
		and isnull(target.IssuePrice, 0) = isnull(source.IssuePrice, 0)
		and isnull(target.LossesGains, 0) = isnull(source.LossesGains, 0)
		and isnull(target.LocalCode, '') = isnull(source.LocalCode, '')
		and isnull(target.BatchTracking, '') = isnull(source.BatchTracking, '')
		and isnull(target.LastIssuedDate, '') = isnull(source.LastIssuedDate, '')
		and isnull(target.LastStocktakeDate, '') = isnull(source.LastStocktakeDate, '')
		and isnull(target.DrugSubGroupCode, '') = isnull(source.DrugSubGroupCode, '')
		and isnull(target.DrugGroupCode, '') = isnull(source.DrugGroupCode, '')
		and isnull(target.UseThisPeriodUnits, 0) = isnull(source.UseThisPeriodUnits, 0)
		and isnull(target.LastPeriodEndDate, '') = isnull(source.LastPeriodEndDate, '')
		and isnull(target.PrimaryLocationCode, '') = isnull(source.PrimaryLocationCode, '')
		and isnull(target.SecondaryLocationCode, '') = isnull(source.SecondaryLocationCode, '')
		and isnull(target.NSVCode, '') = isnull(source.NSVCode, '')
		and isnull(target.ExtraMessage, '') = isnull(source.ExtraMessage, '')
		and isnull(target.Stocked, '') = isnull(source.Stocked, '')
		and isnull(target.DefinedDailyDoseValue, '') = isnull(source.DefinedDailyDoseValue, '')
		and isnull(target.DefinedDailyDoseUnits, '') = isnull(source.DefinedDailyDoseUnits, '')
		and isnull(target.UserField1, '') = isnull(source.UserField1, '')
		and isnull(target.UserField2, '') = isnull(source.UserField2, '')
		and isnull(target.UserField3, '') = isnull(source.UserField3, '')
		and isnull(target.PIPCode, '') = isnull(source.PIPCode, '')
		and isnull(target.MasterPIPCode, '') = isnull(source.MasterPIPCode, '')
		and isnull(target.HighCostProduct, '') = isnull(source.HighCostProduct, '')
		and isnull(target.EDILinkCode, '') = isnull(source.EDILinkCode, '')
		and isnull(target.OuterPackSize, '') = isnull(source.OuterPackSize, '')
		and isnull(target.SiteProductCode, 0) = isnull(source.SiteProductCode, 0)
		)
	then

		update
		set
			 DrugID = source.DrugID
			,InternalSiteCode = source.InternalSiteCode
			,SiteCode = source.SiteCode
			,AnnualUse = source.AnnualUse
			,ReorderLevelUnits = source.ReorderLevelUnits
			,ReorderQuantity = source.ReorderQuantity
			,StockLevel = source.StockLevel
			,ArchiveFlag = source.ArchiveFlag
			,Formulary = source.Formulary
			,OrderCycle = source.OrderCycle
			,SupplierCode = source.SupplierCode
			,LastOrderedDateText = source.LastOrderedDateText
			,StockTakeStatus = source.StockTakeStatus
			,IssuePrice = source.IssuePrice
			,LossesGains = source.LossesGains
			,LocalCode = source.LocalCode
			,BatchTracking = source.BatchTracking
			,LastIssuedDate = source.LastIssuedDate
			,LastStocktakeDate = source.LastStocktakeDate
			,DrugSubGroupCode = source.DrugSubGroupCode
			,DrugGroupCode = source.DrugGroupCode
			,UseThisPeriodUnits = source.UseThisPeriodUnits
			,LastPeriodEndDate = source.LastPeriodEndDate
			,PrimaryLocationCode = source.PrimaryLocationCode
			,SecondaryLocationCode = source.SecondaryLocationCode
			,NSVCode = source.NSVCode
			,ExtraMessage = source.ExtraMessage
			,Stocked = source.Stocked
			,DefinedDailyDoseValue = source.DefinedDailyDoseValue
			,DefinedDailyDoseUnits = source.DefinedDailyDoseUnits
			,UserField1 = source.UserField1
			,UserField2 = source.UserField2
			,UserField3 = source.UserField3
			,PIPCode = source.PIPCode
			,MasterPIPCode = source.MasterPIPCode
			,HighCostProduct = source.HighCostProduct
			,EDILinkCode = source.EDILinkCode
			,OuterPackSize = source.OuterPackSize
			,SiteProductCode = source.SiteProductCode


	when not matched by source
	and	target.InterfaceCode = 'PHAT'
	then
	delete
	

	when not matched then

		insert
		(
			 InterfaceCode
			,ProductStockCode
			,DrugID
			,InternalSiteCode
			,SiteCode
			,AnnualUse
			,ReorderLevelUnits
			,ReorderQuantity
			,StockLevel
			,ArchiveFlag
			,Formulary
			,OrderCycle
			,SupplierCode
			,LastOrderedDateText
			,StockTakeStatus
			,IssuePrice
			,LossesGains
			,LocalCode
			,BatchTracking
			,LastIssuedDate
			,LastStocktakeDate
			,DrugSubGroupCode
			,DrugGroupCode
			,UseThisPeriodUnits
			,LastPeriodEndDate
			,PrimaryLocationCode
			,SecondaryLocationCode
			,NSVCode
			,ExtraMessage
			,Stocked
			,DefinedDailyDoseValue
			,DefinedDailyDoseUnits
			,UserField1
			,UserField2
			,UserField3
			,PIPCode
			,MasterPIPCode
			,HighCostProduct
			,EDILinkCode
			,OuterPackSize
			,SiteProductCode
		)
		values
		(
			 source.InterfaceCode
			,source.ProductStockCode
			,source.DrugID
			,source.InternalSiteCode
			,source.SiteCode
			,source.AnnualUse
			,source.ReorderLevelUnits
			,source.ReorderQuantity
			,source.StockLevel
			,source.ArchiveFlag
			,source.Formulary
			,source.OrderCycle
			,source.SupplierCode
			,source.LastOrderedDateText
			,source.StockTakeStatus
			,source.IssuePrice
			,source.LossesGains
			,source.LocalCode
			,source.BatchTracking
			,source.LastIssuedDate
			,source.LastStocktakeDate
			,source.DrugSubGroupCode
			,source.DrugGroupCode
			,source.UseThisPeriodUnits
			,source.LastPeriodEndDate
			,source.PrimaryLocationCode
			,source.SecondaryLocationCode
			,source.NSVCode
			,source.ExtraMessage
			,source.Stocked
			,source.DefinedDailyDoseValue
			,source.DefinedDailyDoseUnits
			,source.UserField1
			,source.UserField2
			,source.UserField3
			,source.PIPCode
			,source.MasterPIPCode
			,source.HighCostProduct
			,source.EDILinkCode
			,source.OuterPackSize
			,source.SiteProductCode
		)
	;

	--WardStockList
	merge 
		Pharmacy.WardStockList target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,WardStockListCode = WWardStockList.WWardStockListID
			,SupplierCode = WWardStockList.WSupplierID
			,NSVCode = WWardStockList.NSVcode
			,TopUpLevel = WWardStockList.TopupLvl
			,PrintLabel = WWardStockList.PrintLabel
			,SiteCode = WWardStockList.SiteID
			,Barcode = WWardStockList.Barcode
		from
			[$(Pharmacy_Trafford)].icwsys.WWardStockList
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.WardStockListCode = target.WardStockListCode

	when matched then

		update
		set
			 SupplierCode = source.SupplierCode
			,NSVCode = source.NSVCode
			,TopUpLevel = source.TopUpLevel
			,PrintLabel = source.PrintLabel
			,SiteCode = source.SiteCode
			,Barcode = source.Barcode

	when not matched then

		insert
		(
			 InterfaceCode
			,WardStockListCode
			,SupplierCode
			,NSVCode
			,TopUpLevel
			,PrintLabel
			,SiteCode
			,Barcode
		)
		values
		(
			 source.InterfaceCode
			,source.WardStockListCode
			,source.SupplierCode
			,source.NSVCode
			,source.TopUpLevel
			,source.PrintLabel
			,source.SiteCode
			,source.Barcode
		)
	;

	--GP
	merge 
		Pharmacy.GP target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,GPCode = GP.EntityID
			,GP = case when GPEntity.Description = '' then null else GPEntity.Description end
			,GPClassificationID = GP.GPClassificationID
			,GPClassification = GPClassification.Description
			,GPClassificationDetail = GPClassification.Detail
			,GPRegistrationNo = GP.RegNumber
			,Contract = GP.Contract
			,Obstetric = GP.Obstetric
			,JobShare = GP.JobShare
			,Trainer = GP.Trainer
		from
			[$(Pharmacy_Trafford)].icwsys.GP GP

		left join [$(Pharmacy_Trafford)].icwsys.GPClassification GPClassification
		on	GPClassification.GPClassificationID = GP.GPClassificationID

		left join [$(Pharmacy_Trafford)].icwsys.Entity GPEntity
		on	GPEntity.EntityID = GP.EntityID
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.GPCode = target.GPCode

	when matched then

		update
		set
			 GP = source.GP
			,GPClassificationID = source.GPClassificationID
			,GPClassification = source.GPClassification
			,GPClassificationDetail = source.GPClassificationDetail
			,GPRegistrationNo = source.GPRegistrationNo
			,Contract = source.Contract
			,Obstetric = source.Obstetric
			,JobShare = source.JobShare
			,Trainer = source.Trainer

	when not matched then

		insert
		(
			 InterfaceCode
			,GPCode
			,GP
			,GPClassificationID
			,GPClassification
			,GPClassificationDetail
			,GPRegistrationNo
			,Contract
			,Obstetric
			,JobShare
			,Trainer
		)
		values
		(
			 source.InterfaceCode
			,source.GPCode
			,source.GP
			,source.GPClassificationID
			,source.GPClassification
			,source.GPClassificationDetail
			,source.GPRegistrationNo
			,source.Contract
			,source.Obstetric
			,source.JobShare
			,source.Trainer
		)
	;

--Sex
		insert
		into
			 Pharmacy.Sex
		(
			 InterfaceCode
			,SexCode
			,Sex
		)
		select
			 InterfaceCode = @interfaceCode
			,SexCode = GenderID
			,Sex = Description
		from
			[$(Pharmacy_Trafford)].icwsys.Gender
		where
			not exists
			(
			select
				1
			from
				Pharmacy.Sex
			where
				Sex.InterfaceCode = @interfaceCode
			and	Sex.SexCode = Gender.GenderID
			)			

end

if @interfaceCode = 'PHAC'
begin

	--Tax Rate
	merge 
		Pharmacy.TaxRate target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,TaxCode =
				SUBSTRING(
					 TaxRate.[Key]
					,CHARINDEX('(', TaxRate.[Key]) + 1
					,1
				)

			,TaxRate =
				CONVERT(
					FLOAT
					,replace(
						TaxRate.[Value]
						,'"'
						,''
					)
				)
		from
			[$(Pharmacy_Central)].icwsys.wConfiguration TaxRate
		where
			TaxRate.Category = 'D|Workingdefaults'
		and	left(TaxRate.[Key], 3) = 'VAT'
		and	not exists
			(
			select
				1
			from
				[$(Pharmacy_Central)].icwsys.wConfiguration Previous
			where
				Previous.Category = 'D|Workingdefaults'
			and	left(Previous.[Key], 3) = 'VAT'
			and	Previous.SiteID < TaxRate.SiteID
			)
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.TaxCode = target.TaxCode

	when matched then

		update
		set
			 TaxRate = source.TaxRate

	when not matched then

		insert
		(
			 InterfaceCode
			,TaxCode
			,TaxRate
		)
		values
		(
			 source.InterfaceCode
			,source.TaxCode
			,source.TaxRate
		)
	;

	--Site
	merge 
		Pharmacy.Site target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SiteCode = Site.SiteNumber
			,Site = Location.Description
			,SiteID = Site.LocationID
		from
			[$(Pharmacy_Central)].icwsys.Site

		inner join [$(Pharmacy_Central)].icwsys.Location
		on	Location.LocationID = Site.LocationID

		where
			Site.SiteNumber <> 0
		and	Location._Deleted = 'false'
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SiteCode = target.SiteCode

	when matched then

		update
		set
			 Site = source.Site
			,SiteID = source.SiteID

	when not matched then

		insert
		(
			 InterfaceCode
			,SiteCode
			,Site
			,SiteID
		)
		values
		(
			 source.InterfaceCode
			,source.SiteCode
			,source.Site
			,source.SiteID
		)
	;

	--SystemUser
	merge 
		Pharmacy.SystemUser target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SystemUserCode = SystemUser.EntityID
			,Surname = Person.Surname
			,Forename = Person.Forename
			,Initials = Person.Initials
			,Username = SystemUser.Username
		from
			[$(Pharmacy_Central)].icwsys.[User] SystemUser

		inner join [$(Pharmacy_Central)].icwsys.Person
		on	Person.EntityID = SystemUser.EntityID
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SystemUserCode = target.SystemUserCode

	when matched then

		update
		set
			 Surname = source.Surname
			,Forename = source.Forename
			,Initials = source.Initials
			,Username = source.Username

	when not matched then

		insert
		(
			 InterfaceCode
			,SystemUserCode
			,Surname
			,Forename
			,Initials
			,Username
			,SystemUserGroupCode

		)
		values
		(
			 source.InterfaceCode
			,source.SystemUserCode
			,source.Surname
			,source.Forename
			,source.Initials
			,source.Username
			,'UNASSIGNED'
		)
	;


	--Consultant
	merge 
		Pharmacy.Consultant target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,ConsultantCode = Consultant.EntityID
			,Surname = case when rtrim(Person.Surname) = '' then null else Person.Surname end
			,Forename = case when rtrim(Person.Forename) = '' then null else Person.Forename end
			,Initials = case when rtrim(Person.Initials) = '' then null else Person.Initials end
			,GMCCode = case when rtrim(Consultant.GMCCode) = '' then null else Consultant.GMCCode end
			,TelephoneNo = case when rtrim(Person.Mobile) = '' then null else Person.Mobile end
		from
			[$(Pharmacy_Central)].icwsys.Consultant Consultant

		inner join [$(Pharmacy_Central)].icwsys.Person
		on	Person.EntityID = Consultant.EntityID
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.ConsultantCode = target.ConsultantCode

	when matched then

		update
		set
			 Surname = source.Surname
			,Forename = source.Forename
			,Initials = source.Initials
			,GMCCode = source.GMCCode
			,TelephoneNo = source.TelephoneNo

	when not matched then

		insert
		(
			 InterfaceCode
			,ConsultantCode
			,Surname
			,Forename
			,Initials
			,GMCCode
			,TelephoneNo
		)
		values
		(
			 source.InterfaceCode
			,source.ConsultantCode
			,source.Surname
			,source.Forename
			,source.Initials
			,source.GMCCode
			,source.TelephoneNo
		)
	;


	--Specialty
	merge 
		Pharmacy.Specialty target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SpecialtyCode = Specialty.SpecialtyID
			,Specialty = Specialty.Description
			,Detail = Specialty.Detail
			,LocalSpecialtyCode =
				(
				select
					SpecialtyAlias.Alias
				from
					[$(Pharmacy_Central)].icwsys.SpecialtyAlias

				inner join [$(Pharmacy_Central)].icwsys.AliasGroup
				on	AliasGroup.AliasGroupID = SpecialtyAlias.AliasGroupID
				and	AliasGroup.Description = 'WSpecialtyCodes'

				where
					SpecialtyAlias.SpecialtyID = Specialty.SpecialtyID
				)

			,NationalSpecialtyCode =
				(
				select
					SpecialtyAlias.Alias
				from
					[$(Pharmacy_Central)].icwsys.SpecialtyAlias

				inner join [$(Pharmacy_Central)].icwsys.AliasGroup
				on	AliasGroup.AliasGroupID = SpecialtyAlias.AliasGroupID
				and	AliasGroup.Description = 'PasSpecialtyCode'

				where
					SpecialtyAlias.SpecialtyID = Specialty.SpecialtyID
				)
			,ArchiveFlag = Specialty.out_of_use

			,DirectorateCode = CAST(0 as int)
			,DivisionCode = CAST(0 as int)
			,TrustCode = CAST(1 as int)
		from
			[$(Pharmacy_Central)].icwsys.Specialty Specialty
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SpecialtyCode = target.SpecialtyCode

	when matched then

		update
		set
			 Specialty = source.Specialty
			,Detail = source.Detail
			,LocalSpecialtyCode = source.LocalSpecialtyCode
			,NationalSpecialtyCode = source.NationalSpecialtyCode
			,ArchiveFlag = source.ArchiveFlag
			--,DirectorateCode = source.DirectorateCode
			--,DivisionCode = source.DivisionCode
			,TrustCode = source.TrustCode

	when not matched then

		insert
		(
			 InterfaceCode
			,SpecialtyCode
			,Specialty
			,Detail
			,LocalSpecialtyCode
			,NationalSpecialtyCode
			,ArchiveFlag
			,DirectorateCode
			,DivisionCode
			,TrustCode
		)
		values
		(
			 source.InterfaceCode
			,source.SpecialtyCode
			,source.Specialty
			,source.Detail
			,source.LocalSpecialtyCode
			,source.NationalSpecialtyCode
			,source.ArchiveFlag
			,source.DirectorateCode
			,source.DivisionCode
			,source.TrustCode

		)
	;

	--Supplier
	merge 
		Pharmacy.Supplier target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SupplierCode = [WSupplierID]
			,LocalSupplierCode = [Code]
			,ContractAddress = case when RTRIM([ContractAddress]) = '' then null else [ContractAddress] end
			,SupplierAddress = case when RTRIM([SupAddress]) = '' then null else [SupAddress] end
			,InvoiceAddress = case when RTRIM([InvAddress]) = '' then null else [InvAddress] end
			,ContactTelephoneNo = case when RTRIM([ContTelNo]) = '' then null else [ContTelNo] end
			,SupplierTelephoneNo = case when RTRIM([SupTelNo]) = '' then null else [SupTelNo] end
			,InvoiceTelephoneNo = case when RTRIM([InvTelNo]) = '' then null else [InvTelNo] end
			,DiscountDetail = case when RTRIM([DiscountDesc]) = '' then null else [DiscountDesc] end
			,DiscountValue = case when RTRIM([DiscountVal]) = '' then null else [DiscountVal] end
			,OrderMethod = case when RTRIM([Method]) = '' then null else [Method] end
			,OrderMessage = case when RTRIM([OrdMessage]) = '' then null else [OrdMessage] end
			,AverageLeadTime = case when RTRIM([AvLeadTime]) = '' then null else [AvLeadTime] end
			,ContactFaxNo = case when RTRIM([ContFaxNo]) = '' then null else [ContFaxNo] end
			,SupplierFaxNo = case when RTRIM([SupFaxNo]) = '' then null else [SupFaxNo] end
			,InvoiceFaxNo = case when RTRIM([InvFaxNo]) = '' then null else [InvFaxNo] end
			,Supplier = case when RTRIM([Name]) = '' then null else [Name] end
			,PrintTradeName = case when RTRIM([Ptn]) = '' then null else [Ptn] end
			,PrintNSVCode = case when RTRIM([PSis]) = '' then null else [PSis] end
			,SupplierFullName = case when RTRIM([FullName]) = '' then null else [FullName] end
			,DiscountBelow = case when RTRIM([DiscountBelow]) = '' then null else [DiscountBelow] end
			,DiscountAbove = case when RTRIM([DiscountAbove]) = '' then null else [DiscountAbove] end
			,InternalAscribeSiteCode = case when RTRIM([ICode]) = '' then null else [ICode] end
			,CostCentreCode = case when RTRIM([CostCentre]) = '' then null else [CostCentre] end
			,PrintDeliveryNote = case when RTRIM([PrintDeliveryNote]) = '' then null else [PrintDeliveryNote] end
			,PrintPickingTicket = case when RTRIM([PrintPickTicket]) = '' then null else [PrintPickTicket] end
			,SupplierTypeCode = case when RTRIM([SupplierType]) = '' then null else [SupplierType] end
			,OrderOutput = case when RTRIM([OrderOutput]) = '' then null else [OrderOutput] end
			,ReceiveGoods = case when RTRIM([ReceiveGoods]) = '' then null else [ReceiveGoods] end
			,TopupInterval = case when RTRIM([TopupInterval]) = '' then null else [TopupInterval] end
			,ATCSupplied = case when RTRIM([AtcSupplied]) = '' then null else [AtcSupplied] end
			,TopupDate = case when RTRIM([TopupDate]) = '' then null else [TopupDate] end
			,InUse = case when RTRIM([InUse]) = '' then null else [InUse] end
			,WardCode = case when RTRIM([WardCode]) = '' then null else [WardCode] end
			,OnCostPercent = case when RTRIM([OnCost]) = '' then null else [OnCost] end
			,InPatientDirections = case when RTRIM([InPatientDirections]) = '' then null else [InPatientDirections] end
			,AdHocDeliveryNote = case when RTRIM([AdHocDelNote]) = '' then null else [AdHocDelNote] end
			,SiteID = case when RTRIM([SiteID]) = '' then null else [SiteID] end
			,MinimumOrderValue = case when RTRIM([MinimumOrderValue]) = '' then null else [MinimumOrderValue] end
		from
			[$(Pharmacy_Central)].icwsys.WSupplier Supplier
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SupplierCode = target.SupplierCode

	when matched then

		update
		set
			 LocalSupplierCode = source.LocalSupplierCode
			,ContractAddress = source.ContractAddress
			,SupplierAddress = source.SupplierAddress
			,InvoiceAddress = source.InvoiceAddress
			,ContactTelephoneNo = source.ContactTelephoneNo
			,SupplierTelephoneNo = source.SupplierTelephoneNo
			,InvoiceTelephoneNo = source.InvoiceTelephoneNo
			,DiscountDetail = source.DiscountDetail
			,DiscountValue = source.DiscountValue
			,OrderMethod = source.OrderMethod
			,OrderMessage = source.OrderMessage
			,AverageLeadTime = source.AverageLeadTime
			,ContactFaxNo = source.ContactFaxNo
			,SupplierFaxNo = source.SupplierFaxNo
			,InvoiceFaxNo = source.InvoiceFaxNo
			,Supplier = source.Supplier
			,PrintTradeName = source.PrintTradeName
			,PrintNSVCode = source.PrintNSVCode
			,SupplierFullName = source.SupplierFullName
			,DiscountBelow = source.DiscountBelow
			,DiscountAbove = source.DiscountAbove
			,InternalAscribeSiteCode = source.InternalAscribeSiteCode
			,CostCentreCode = source.CostCentreCode
			,PrintDeliveryNote = source.PrintDeliveryNote
			,PrintPickingTicket = source.PrintPickingTicket
			,SupplierTypeCode = source.SupplierTypeCode
			,OrderOutput = source.OrderOutput
			,ReceiveGoods = source.ReceiveGoods
			,TopupInterval = source.TopupInterval
			,ATCSupplied = source.ATCSupplied
			,TopupDate = source.TopupDate
			,InUse = source.InUse
			,WardCode = source.WardCode
			,OnCostPercent = source.OnCostPercent
			,InPatientDirections = source.InPatientDirections
			,AdHocDeliveryNote = source.AdHocDeliveryNote
			,SiteID = source.SiteID
			,MinimumOrderValue = source.MinimumOrderValue

	when not matched then

		insert
		(
			 InterfaceCode
			,SupplierCode
			,LocalSupplierCode
			,ContractAddress
			,SupplierAddress
			,InvoiceAddress
			,ContactTelephoneNo
			,SupplierTelephoneNo
			,InvoiceTelephoneNo
			,DiscountDetail
			,DiscountValue
			,OrderMethod
			,OrderMessage
			,AverageLeadTime
			,ContactFaxNo
			,SupplierFaxNo
			,InvoiceFaxNo
			,Supplier
			,PrintTradeName
			,PrintNSVCode
			,SupplierFullName
			,DiscountBelow
			,DiscountAbove
			,InternalAscribeSiteCode
			,CostCentreCode
			,PrintDeliveryNote
			,PrintPickingTicket
			,SupplierTypeCode
			,OrderOutput
			,ReceiveGoods
			,TopupInterval
			,ATCSupplied
			,TopupDate
			,InUse
			,WardCode
			,OnCostPercent
			,InPatientDirections
			,AdHocDeliveryNote
			,SiteID
			,MinimumOrderValue
		)
		values
		(
			 source.InterfaceCode
			,source.SupplierCode
			,source.LocalSupplierCode
			,source.ContractAddress
			,source.SupplierAddress
			,source.InvoiceAddress
			,source.ContactTelephoneNo
			,source.SupplierTelephoneNo
			,source.InvoiceTelephoneNo
			,source.DiscountDetail
			,source.DiscountValue
			,source.OrderMethod
			,source.OrderMessage
			,source.AverageLeadTime
			,source.ContactFaxNo
			,source.SupplierFaxNo
			,source.InvoiceFaxNo
			,source.Supplier
			,source.PrintTradeName
			,source.PrintNSVCode
			,source.SupplierFullName
			,source.DiscountBelow
			,source.DiscountAbove
			,source.InternalAscribeSiteCode
			,source.CostCentreCode
			,source.PrintDeliveryNote
			,source.PrintPickingTicket
			,source.SupplierTypeCode
			,source.OrderOutput
			,source.ReceiveGoods
			,source.TopupInterval
			,source.ATCSupplied
			,source.TopupDate
			,source.InUse
			,source.WardCode
			,source.OnCostPercent
			,source.InPatientDirections
			,source.AdHocDeliveryNote
			,source.SiteID
			,source.MinimumOrderValue
		)
	;

	--Patient
	merge 
		Pharmacy.Patient target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SourcePatientNo = Patient.EntityID
			,Title = case when RTRIM(Person.Title) = '' then null else Person.Title end
			,Initials = case when RTRIM(Person.Initials) = '' then null else Person.Initials end
			,Forename = case when RTRIM(Person.Forename) = '' then null else Person.Forename end
			,Surname = case when RTRIM(Person.Surname) = '' then null else Person.Surname end
			,DateOfBirth = case when RTRIM(Patient.DOB) = '' then null else Patient.DOB end
			,NHSNumber = case when RTRIM(NHSNumber.Alias) = '' then null else NHSNumber.Alias end
			,NHSNumberValidationStatusCode = case when RTRIM(NHSNumber.IsValid) = '' then null else NHSNumber.IsValid end
			,SexCode = case when RTRIM(Patient.GenderID) = '' then null else Patient.GenderID end
			,CasenoteNo =  --CasenoteNo.Alias
			
							(
								select top 1
									 
									CasenoteNo.Alias
									
								from
									[$(Pharmacy_Central)].icwsys.EntityAlias CasenoteNo

								inner join [$(Pharmacy_Central)].icwsys.AliasGroup
								on	AliasGroup.AliasGroupID = CasenoteNo.AliasGroupID
								and	AliasGroup.Description = 'CaseNumber'

								where
									CasenoteNo.[Default] = 1
									
									and
									
									CasenoteNo.EntityID = Patient.EntityID
						
				)
		from
			[$(Pharmacy_Central)].icwsys.Patient

		inner join [$(Pharmacy_Central)].icwsys.Person
		on	Person.EntityID = Patient.EntityID

		left join
			(
			select
				 NHSNumber.EntityID
				,NHSNumber.Alias
				,NHSNumber.IsValid
			from
				[$(Pharmacy_Central)].icwsys.EntityAlias NHSNumber

			inner join [$(Pharmacy_Central)].icwsys.AliasGroup
			on	AliasGroup.AliasGroupID = NHSNumber.AliasGroupID
			and	AliasGroup.Description = 'NHSNumber'

			inner join [$(Pharmacy_Central)].icwsys.Setting
			on	Setting.Value = AliasGroup.Description
			and	Setting.System = 'General'
			and	Setting.Section = 'PatientEditor'
			and	Setting.[Key] = 'PrimaryPatientIdentifier'
			and	Setting.RoleID = 0

			where
				NHSNumber.[Default] = 1

			) NHSNumber
		on	NHSNumber.EntityID = Patient.EntityID

		--Gareth Cunnah Removed 17/02/2015
		--left join
		--	(
		--	select
		--		 CasenoteNo.EntityID
		--		,CasenoteNo.Alias
		--	from
		--		[$(Pharmacy_Central)].icwsys.EntityAlias CasenoteNo

		--	inner join [$(Pharmacy_Central)].icwsys.AliasGroup
		--	on	AliasGroup.AliasGroupID = CasenoteNo.AliasGroupID
		--	and	AliasGroup.Description = 'CaseNumber'

		--	where
		--		CasenoteNo.[Default] = 1
		--	) CasenoteNo
		--on	CasenoteNo.EntityID = Patient.EntityID

		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SourcePatientNo = target.SourcePatientNo

	when matched then

		update
		set
			 Title = source.Title
			,Initials = source.Initials
			,Forename = source.Forename
			,Surname = source.Surname
			,DateOfBirth = source.DateOfBirth
			,NHSNumber = source.NHSNumber
			,NHSNumberValidationStatusCode = source.NHSNumberValidationStatusCode
			,SexCode = source.SexCode
			,CasenoteNo = source.CasenoteNo

	when not matched then

		insert
		(
			 InterfaceCode
			,SourcePatientNo
			,Title
			,Initials
			,Forename
			,Surname
			,DateOfBirth
			,NHSNumber
			,NHSNumberValidationStatusCode
			,SexCode
			,CasenoteNo
		)
		values
		(
			 source.InterfaceCode
			,source.SourcePatientNo
			,source.Title
			,source.Initials
			,source.Forename
			,source.Surname
			,source.DateOfBirth
			,source.NHSNumber
			,source.NHSNumberValidationStatusCode
			,source.SexCode
			,source.CasenoteNo
		)
	;

	--Address
	merge 
		Pharmacy.Address target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,AddressCode = Address.AddressID
			,BoxNumber = case when RTRIM(BoxNumber) = '' then null else BoxNumber end
			,DoorNumber = case when RTRIM(DoorNumber) = '' then null else DoorNumber end
			,Street = case when RTRIM(Street) = '' then null else Street end
			,Town = case when RTRIM(Town) = '' then null else Town end
			,LocalAuthority = case when RTRIM(LocalAuthority) = '' then null else LocalAuthority end
			,District = case when RTRIM(District) = '' then null else District end
			,Postcode = case when RTRIM(PostCode) = '' then null else PostCode end
			,Province = case when RTRIM(Province) = '' then null else Province end
			,Country = case when RTRIM(Country) = '' then null else Country end
		from
			[$(Pharmacy_Central)].icwsys.Address
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.AddressCode = target.AddressCode

	when matched then

		update
		set
			 BoxNumber = source.BoxNumber
			,DoorNumber = source.DoorNumber
			,Street = source.Street
			,Town = source.Town
			,LocalAuthority = source.LocalAuthority
			,District = source.District
			,Postcode = source.Postcode
			,Province = source.Province
			,Country = source.Country

	when not matched then

		insert
		(
			 InterfaceCode
			,AddressCode
			,BoxNumber
			,DoorNumber
			,Street
			,Town
			,LocalAuthority
			,District
			,Postcode
			,Province
			,Country
		)
		values
		(
			 source.InterfaceCode
			,source.AddressCode
			,source.BoxNumber
			,source.DoorNumber
			,source.Street
			,source.Town
			,source.LocalAuthority
			,source.District
			,source.Postcode
			,source.Province
			,source.Country
		)
	;

	--PatientAddressMap
	merge 
		Pharmacy.PatientAddressMap target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SourcePatientNo = EntityLinkAddress.EntityID
			,AddressCode = EntityLinkAddress.AddressID
			,AddressTypeCode = EntityLinkAddress.AddressTypeID
		from
			[$(Pharmacy_Central)].icwsys.EntityLinkAddress
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SourcePatientNo = target.SourcePatientNo
	and	source.AddressCode = target.AddressCode
	and	source.AddressTypeCode = target.AddressTypeCode

	when not matched then

		insert
		(
			 InterfaceCode
			,SourcePatientNo
			,AddressCode
			,AddressTypeCode
		)
		values
		(
			 source.InterfaceCode
			,source.SourcePatientNo
			,source.AddressCode
			,source.AddressTypeCode
		)
	;

	--AddressType
	merge 
		Pharmacy.AddressType target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,AddressTypeCode = AddressType.AddressTypeID
			,AddressType = AddressType.Description
		from
			[$(Pharmacy_Central)].icwsys.AddressType
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.AddressTypeCode = target.AddressTypeCode

	when matched then

		update
		set
			AddressType = source.AddressType

	when not matched then

		insert
		(
			 InterfaceCode
			,AddressTypeCode
			,AddressType
		)
		values
		(
			 source.InterfaceCode
			,source.AddressTypeCode
			,source.AddressType
		)
	;


	--Terminal
	merge 
		Pharmacy.Terminal target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,TerminalCode = Terminal.LocationID
			,Terminal = Terminal.ComputerName
			,TerminalTypeCode = Terminal.TerminalTypeID
			,DomainName = Terminal.DomainName
		from
			[$(Pharmacy_Central)].icwsys.Terminal
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.TerminalCode = target.TerminalCode

	when matched then

		update
		set
			 Terminal = source.Terminal
			,TerminalTypeCode = source.TerminalTypeCode
			,DomainName = source.DomainName

	when not matched then

		insert
		(
			 InterfaceCode
			,TerminalCode
			,Terminal
			,TerminalTypeCode
			,DomainName
		)
		values
		(
			 source.InterfaceCode
			,source.TerminalCode
			,source.Terminal
			,source.TerminalTypeCode
			,source.DomainName
		)
	;

	--Product
	merge 
		Pharmacy.Product target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,SiteProductCode  = case when RTRIM(SiteProduct.SiteProductDataID) = '' then null else SiteProduct.SiteProductDataID end
			,ProductCode  = case when RTRIM(SiteProduct.ProductID) = '' then null else SiteProduct.ProductID end
			,NSVCode  = case when RTRIM(SiteProduct.siscode) = '' then null else SiteProduct.siscode end
			,Product  = case when RTRIM(SiteProduct.labeldescription) = '' then null else SiteProduct.labeldescription end
			,TradeName  = case when RTRIM(SiteProduct.tradename) = '' then null else SiteProduct.tradename end
			,StoresDescription  = case when RTRIM(SiteProduct.storesdescription) = '' then null else SiteProduct.storesdescription end
			,ReorderPackSize  = case when RTRIM(SiteProduct.convfact) = '' then null else SiteProduct.convfact end
			,MlsPerPack  = case when RTRIM(SiteProduct.mlsperpack) = '' then null else SiteProduct.mlsperpack end
			,Cytotoxic  = case when RTRIM(SiteProduct.cyto) = '' then null else SiteProduct.cyto end
			,PrimaryWarningCode  = case when RTRIM(SiteProduct.warcode) = '' then null else SiteProduct.warcode end
			,SecondaryWarningCode  = case when RTRIM(SiteProduct.warcode2) = '' then null else SiteProduct.warcode2 end
			,InstructionCode  = case when RTRIM(SiteProduct.inscode) = '' then null else SiteProduct.inscode end
			,DosesPerIssueUnit  = case when RTRIM(SiteProduct.DosesperIssueUnit) = '' then null else SiteProduct.DosesperIssueUnit end
			,DosingUnits  = case when RTRIM(SiteProduct.DosingUnits) = '' then null else SiteProduct.DosingUnits end
			,DPSForm  = case when RTRIM(SiteProduct.DPSForm) = '' then null else SiteProduct.DPSForm end
			,BNFCode  = case when RTRIM(SiteProduct.BNF) = '' then null else SiteProduct.BNF end
			,ProductBarCode  = case when RTRIM(SiteProduct.barcode) = '' then null else SiteProduct.barcode end

			,SupplementaryBarCode1 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Central)].icwsys.SiteProductData

					left join [$(Pharmacy_Central)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Central)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 1
				)

			,SupplementaryBarCode2 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Central)].icwsys.SiteProductData

					left join [$(Pharmacy_Central)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Central)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 2
				)

			,SupplementaryBarCode3 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Central)].icwsys.SiteProductData

					left join [$(Pharmacy_Central)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Central)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 3
				)

			,SupplementaryBarCode4 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Central)].icwsys.SiteProductData

					left join [$(Pharmacy_Central)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Central)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 4
				)

			,SupplementaryBarCode5 =
				(
				select
					Barcode
				from
					(
					select
						 Barcode = SiteProductDataAlias.Alias
						,Sequence =
							ROW_NUMBER()
							over(
							partition by SiteProductData.siscode
							order by SiteProductDataAlias.Alias
							)
					from
						[$(Pharmacy_Central)].icwsys.SiteProductData

					left join [$(Pharmacy_Central)].icwsys.SiteProductDataAlias
					on	SiteProductDataAlias.SiteProductDataID = SiteProductData.SiteProductDataID

					left join [$(Pharmacy_Central)].icwsys.AliasGroup
					on	AliasGroup.AliasGroupID = SiteProductDataAlias.AliasGroupID

					where
						SiteProductData.DSSMasterSiteID <> 0
					and	AliasGroup.Description = 'AlternativeBarcode'
					and	SiteProductData.siscode = SiteProduct.siscode
					) Activity
				where
					Sequence = 5
				)

			,CatalogueDescription = case when RTRIM(OrderCatalogue.CatalogueDescription) = '' then null else OrderCatalogue.CatalogueDescription end
			,ReportGroup = case when RTRIM(ReportGroup.Description) = '' then null else ReportGroup.Description end
			,IssueForm  = case when RTRIM(SiteProduct.printformv) = '' then null else SiteProduct.printformv end

			,TaxRate.TaxRate

		from
			[$(Pharmacy_Central)].icwsys.SiteProductData SiteProduct

		left join 
			(
			select
				 BNFCode = OrderCatalogue.Description
				,CatalogueDescription = OrderCatalogue.Detail
			from
				[$(Pharmacy_Central)].icwsys.OrderCatalogue

			inner join [$(Pharmacy_Central)].icwsys.OrderCatalogueRoot
			on	OrderCatalogueRoot.OrderCatalogueRootID = OrderCatalogue.OrderCatalogueRootID
			and	OrderCatalogueRoot.Description = 'BNF'
			) OrderCatalogue
		on	OrderCatalogue.BNFCode = SiteProduct.BNF

		left join [$(Pharmacy_Central)].icwsys.ProductCustomisation
		on	ProductCustomisation.ProductID = SiteProduct.ProductID
		and	not exists
			(
			select
				1
			from
				[$(Pharmacy_Central)].icwsys.ProductCustomisation Previous
			where
				Previous.ProductID = SiteProduct.ProductID
			and	Previous.ProductCustomisationID < ProductCustomisation.ProductCustomisationID
			)

		left join [$(Pharmacy_Central)].icwsys.ReportGroup
		on	ReportGroup.ReportGroupID = ProductCustomisation.ReportGroupID

		--get distinct list of products using site 15 as the master
		inner join (
			select
				 Product.siscode
				,Product.vatrate
			from
				[$(Pharmacy_Central)].icwsys.wProduct Product
			where
				Product.LocationID_Site = 15

			union

			select
				 Product.siscode
				,Product.vatrate
			from
				[$(Pharmacy_Central)].icwsys.wProduct Product
			where
				Product.LocationID_Site <> 15
			and	not exists
				(
				select
					1
				from
					[$(Pharmacy_Central)].icwsys.wProduct Product15
				where
					Product15.LocationID_Site = 15
				and	Product15.siscode = Product.siscode
				)
			and	not exists
				(
				select
					1
				from
					[$(Pharmacy_Central)].icwsys.wProduct Previous
				where
					Previous.LocationID_Site <> 15
				and	Previous.siscode = Product.siscode
				and	(
						Previous.vatrate < Product.vatrate
					or	(
							Previous.vatrate = Product.vatrate
						and	Previous.LocationID_Site > Product.LocationID_Site
						)
					)
				)
			) Product
		on	Product.siscode = SiteProduct.siscode

		inner join
			(
			select
				TaxCode =
					SUBSTRING(
						 TaxRate.[Key]
						,CHARINDEX('(', TaxRate.[Key]) + 1
						,1
					)

				,TaxRate =
					CONVERT(
						FLOAT
						,replace(
							TaxRate.[Value]
							,'"'
							,''
						)
					)
			from
				[$(Pharmacy_Central)].icwsys.wConfiguration TaxRate
			where
				TaxRate.Category = 'D|Workingdefaults'
			and	left(TaxRate.[Key], 3) = 'VAT'
			and	not exists
				(
				select
					1
				from
					[$(Pharmacy_Central)].icwsys.wConfiguration Previous
				where
					Previous.Category = 'D|Workingdefaults'
				and	left(Previous.[Key], 3) = 'VAT'
				and	Previous.SiteID < TaxRate.SiteID
				)
			) TaxRate
		on	TaxRate.TaxCode = coalesce(Product.vatrate, 1)

		where
			SiteProduct.DSSMasterSiteID <> 0
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.SiteProductCode = target.SiteProductCode

	when matched then

		update
		set
			 ProductCode = source.ProductCode
			,NSVCode = source.NSVCode
			,Product = source.Product
			,TradeName = source.TradeName
			,StoresDescription = source.StoresDescription
			,ReorderPackSize = source.ReorderPackSize
			,MlsPerPack = source.MlsPerPack
			,Cytotoxic = source.Cytotoxic
			,PrimaryWarningCode = source.PrimaryWarningCode
			,SecondaryWarningCode = source.SecondaryWarningCode
			,InstructionCode = source.InstructionCode
			,DosesPerIssueUnit = source.DosesPerIssueUnit
			,DosingUnits = source.DosingUnits
			,DPSForm = source.DPSForm
			,BNFCode = source.BNFCode
			,ProductBarCode = source.ProductBarCode
			,SupplementaryBarCode1 = source.SupplementaryBarCode1
			,SupplementaryBarCode2 = source.SupplementaryBarCode2
			,SupplementaryBarCode3 = source.SupplementaryBarCode3
			,SupplementaryBarCode4 = source.SupplementaryBarCode4
			,SupplementaryBarCode5 = source.SupplementaryBarCode5
			,CatalogueDescription = source.CatalogueDescription
			,ReportGroup = source.ReportGroup
			,IssueForm = source.IssueForm
			,TaxRate = source.TaxRate

	when not matched then

		insert
		(
			 InterfaceCode
			,SiteProductCode
			,ProductCode
			,NSVCode
			,Product
			,TradeName
			,StoresDescription
			,ReorderPackSize
			,MlsPerPack
			,Cytotoxic
			,PrimaryWarningCode
			,SecondaryWarningCode
			,InstructionCode
			,DosesPerIssueUnit
			,DosingUnits
			,DPSForm
			,BNFCode
			,ProductBarCode
			,SupplementaryBarCode1
			,SupplementaryBarCode2
			,SupplementaryBarCode3
			,SupplementaryBarCode4
			,SupplementaryBarCode5
			,CatalogueDescription
			,ReportGroup
			,IssueForm
			,TaxRate
		)
		values
		(
			 source.InterfaceCode
			,source.SiteProductCode
			,source.ProductCode
			,source.NSVCode
			,source.Product
			,source.TradeName
			,source.StoresDescription
			,source.ReorderPackSize
			,source.MlsPerPack
			,source.Cytotoxic
			,source.PrimaryWarningCode
			,source.SecondaryWarningCode
			,source.InstructionCode
			,source.DosesPerIssueUnit
			,source.DosingUnits
			,source.DPSForm
			,source.BNFCode
			,source.ProductBarCode
			,source.SupplementaryBarCode1
			,source.SupplementaryBarCode2
			,source.SupplementaryBarCode3
			,source.SupplementaryBarCode4
			,source.SupplementaryBarCode5
			,source.CatalogueDescription
			,source.ReportGroup
			,source.IssueForm
			,source.TaxRate
		)
	;

	--ProductStock
	merge 
		Pharmacy.ProductStock target
	using
		(
		select
			 InterfaceCode
			,ProductStockCode = cast(ProductStockCode as int)
			,DrugID = cast(DrugID as int)
			,InternalSiteCode = cast(InternalSiteCode as int)
			,SiteCode = cast(SiteCode as int)
			,AnnualUse = cast(AnnualUse as float)
			,ReorderLevelUnits = cast(ReorderLevelUnits as varchar(8))
			,ReorderQuantity = cast(ReorderQuantity as varchar(6))
			,StockLevel = cast(StockLevel as float)
			,ArchiveFlag = cast(case ArchiveFlag when 'Y' then 1 else 0 end as bit)
			,Formulary = cast(Formulary as varchar(1))
			,OrderCycle = cast(OrderCycle as varchar(2))
			,SupplierCode = cast(SupplierCode as varchar(5))
			,LastOrderedDateText = cast(LastOrderedDateText as varchar(8))
			,StockTakeStatus = cast(StockTakeStatus as varchar(1))
			,IssuePrice = cast(IssuePrice as float)
			,LossesGains = cast(LossesGains as float)
			,LocalCode = cast(LocalCode as varchar(20))
			,BatchTracking = cast(BatchTracking as varchar(1))
			,LastIssuedDate = cast(LastIssuedDate as varchar(8))
			,LastStocktakeDate = cast(LastStocktakeDate as varchar(8))
			,DrugSubGroupCode = cast(DrugSubGroupCode as varchar(20))
			,DrugGroupCode = cast(DrugGroupCode as varchar(2))
			,UseThisPeriodUnits = cast(UseThisPeriodUnits as float)
			,LastPeriodEndDate = cast(LastPeriodEndDate as varchar(8))
			,PrimaryLocationCode = cast(PrimaryLocationCode as varchar(3))
			,SecondaryLocationCode = cast(SecondaryLocationCode as varchar(3))
			,NSVCode = cast(NSVCode as varchar(7))
			,ExtraMessage = cast(ExtraMessage as varchar(255))
			,Stocked = cast(Stocked as varchar(1))
			,DefinedDailyDoseValue = cast(DefinedDailyDoseValue as varchar(10))
			,DefinedDailyDoseUnits = cast(DefinedDailyDoseUnits as varchar(10))
			,UserField1 = cast(UserField1 as varchar(10))
			,UserField2 = cast(UserField2 as varchar(10))
			,UserField3 = cast(UserField3 as varchar(10))
			,PIPCode = cast(PIPCode as varchar(7))
			,MasterPIPCode = cast(MasterPIPCode as varchar(7))
			,HighCostProduct = cast(HighCostProduct as char(1))
			,EDILinkCode = cast(EDILinkCode as varchar(13))
			,OuterPackSize = cast(OuterPackSize as varchar(5))
			,SiteProductCode = cast(SiteProductCode as int)
		from
			Pharmacy.TLoadCenProductStock
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.ProductStockCode = target.ProductStockCode

	when matched 
	and not
		(
			isnull(target.DrugID, 0) = isnull(source.DrugID, 0)
		and isnull(target.InternalSiteCode, 0) = isnull(source.InternalSiteCode, 0)
		and isnull(target.SiteCode, 0) = isnull(source.SiteCode, 0)
		and isnull(target.AnnualUse, 0) = isnull(source.AnnualUse, 0)
		and isnull(target.ReorderLevelUnits, '') = isnull(source.ReorderLevelUnits, '')
		and isnull(target.ReorderQuantity, '') = isnull(source.ReorderQuantity, '')
		and isnull(target.StockLevel, 0) = isnull(source.StockLevel, 0)
		and isnull(target.ArchiveFlag, '') = isnull(source.ArchiveFlag, '')
		and isnull(target.Formulary, '') = isnull(source.Formulary, '')
		and isnull(target.OrderCycle, '') = isnull(source.OrderCycle, '')
		and isnull(target.SupplierCode, '') = isnull(source.SupplierCode, '')
		and isnull(target.LastOrderedDateText, '') = isnull(source.LastOrderedDateText, '')
		and isnull(target.StockTakeStatus, '') = isnull(source.StockTakeStatus, '')
		and isnull(target.IssuePrice, 0) = isnull(source.IssuePrice, 0)
		and isnull(target.LossesGains, 0) = isnull(source.LossesGains, 0)
		and isnull(target.LocalCode, '') = isnull(source.LocalCode, '')
		and isnull(target.BatchTracking, '') = isnull(source.BatchTracking, '')
		and isnull(target.LastIssuedDate, '') = isnull(source.LastIssuedDate, '')
		and isnull(target.LastStocktakeDate, '') = isnull(source.LastStocktakeDate, '')
		and isnull(target.DrugSubGroupCode, '') = isnull(source.DrugSubGroupCode, '')
		and isnull(target.DrugGroupCode, '') = isnull(source.DrugGroupCode, '')
		and isnull(target.UseThisPeriodUnits, 0) = isnull(source.UseThisPeriodUnits, 0)
		and isnull(target.LastPeriodEndDate, '') = isnull(source.LastPeriodEndDate, '')
		and isnull(target.PrimaryLocationCode, '') = isnull(source.PrimaryLocationCode, '')
		and isnull(target.SecondaryLocationCode, '') = isnull(source.SecondaryLocationCode, '')
		and isnull(target.NSVCode, '') = isnull(source.NSVCode, '')
		and isnull(target.ExtraMessage, '') = isnull(source.ExtraMessage, '')
		and isnull(target.Stocked, '') = isnull(source.Stocked, '')
		and isnull(target.DefinedDailyDoseValue, '') = isnull(source.DefinedDailyDoseValue, '')
		and isnull(target.DefinedDailyDoseUnits, '') = isnull(source.DefinedDailyDoseUnits, '')
		and isnull(target.UserField1, '') = isnull(source.UserField1, '')
		and isnull(target.UserField2, '') = isnull(source.UserField2, '')
		and isnull(target.UserField3, '') = isnull(source.UserField3, '')
		and isnull(target.PIPCode, '') = isnull(source.PIPCode, '')
		and isnull(target.MasterPIPCode, '') = isnull(source.MasterPIPCode, '')
		and isnull(target.HighCostProduct, '') = isnull(source.HighCostProduct, '')
		and isnull(target.EDILinkCode, '') = isnull(source.EDILinkCode, '')
		and isnull(target.OuterPackSize, '') = isnull(source.OuterPackSize, '')
		and isnull(target.SiteProductCode, 0) = isnull(source.SiteProductCode, 0)
		)
	then

		update
		set
			 DrugID = source.DrugID
			,InternalSiteCode = source.InternalSiteCode
			,SiteCode = source.SiteCode
			,AnnualUse = source.AnnualUse
			,ReorderLevelUnits = source.ReorderLevelUnits
			,ReorderQuantity = source.ReorderQuantity
			,StockLevel = source.StockLevel
			,ArchiveFlag = source.ArchiveFlag
			,Formulary = source.Formulary
			,OrderCycle = source.OrderCycle
			,SupplierCode = source.SupplierCode
			,LastOrderedDateText = source.LastOrderedDateText
			,StockTakeStatus = source.StockTakeStatus
			,IssuePrice = source.IssuePrice
			,LossesGains = source.LossesGains
			,LocalCode = source.LocalCode
			,BatchTracking = source.BatchTracking
			,LastIssuedDate = source.LastIssuedDate
			,LastStocktakeDate = source.LastStocktakeDate
			,DrugSubGroupCode = source.DrugSubGroupCode
			,DrugGroupCode = source.DrugGroupCode
			,UseThisPeriodUnits = source.UseThisPeriodUnits
			,LastPeriodEndDate = source.LastPeriodEndDate
			,PrimaryLocationCode = source.PrimaryLocationCode
			,SecondaryLocationCode = source.SecondaryLocationCode
			,NSVCode = source.NSVCode
			,ExtraMessage = source.ExtraMessage
			,Stocked = source.Stocked
			,DefinedDailyDoseValue = source.DefinedDailyDoseValue
			,DefinedDailyDoseUnits = source.DefinedDailyDoseUnits
			,UserField1 = source.UserField1
			,UserField2 = source.UserField2
			,UserField3 = source.UserField3
			,PIPCode = source.PIPCode
			,MasterPIPCode = source.MasterPIPCode
			,HighCostProduct = source.HighCostProduct
			,EDILinkCode = source.EDILinkCode
			,OuterPackSize = source.OuterPackSize
			,SiteProductCode = source.SiteProductCode


	when not matched by source
	and	target.InterfaceCode = 'PHAC'
	then
	delete
	

	when not matched then

		insert
		(
			 InterfaceCode
			,ProductStockCode
			,DrugID
			,InternalSiteCode
			,SiteCode
			,AnnualUse
			,ReorderLevelUnits
			,ReorderQuantity
			,StockLevel
			,ArchiveFlag
			,Formulary
			,OrderCycle
			,SupplierCode
			,LastOrderedDateText
			,StockTakeStatus
			,IssuePrice
			,LossesGains
			,LocalCode
			,BatchTracking
			,LastIssuedDate
			,LastStocktakeDate
			,DrugSubGroupCode
			,DrugGroupCode
			,UseThisPeriodUnits
			,LastPeriodEndDate
			,PrimaryLocationCode
			,SecondaryLocationCode
			,NSVCode
			,ExtraMessage
			,Stocked
			,DefinedDailyDoseValue
			,DefinedDailyDoseUnits
			,UserField1
			,UserField2
			,UserField3
			,PIPCode
			,MasterPIPCode
			,HighCostProduct
			,EDILinkCode
			,OuterPackSize
			,SiteProductCode
		)
		values
		(
			 source.InterfaceCode
			,source.ProductStockCode
			,source.DrugID
			,source.InternalSiteCode
			,source.SiteCode
			,source.AnnualUse
			,source.ReorderLevelUnits
			,source.ReorderQuantity
			,source.StockLevel
			,source.ArchiveFlag
			,source.Formulary
			,source.OrderCycle
			,source.SupplierCode
			,source.LastOrderedDateText
			,source.StockTakeStatus
			,source.IssuePrice
			,source.LossesGains
			,source.LocalCode
			,source.BatchTracking
			,source.LastIssuedDate
			,source.LastStocktakeDate
			,source.DrugSubGroupCode
			,source.DrugGroupCode
			,source.UseThisPeriodUnits
			,source.LastPeriodEndDate
			,source.PrimaryLocationCode
			,source.SecondaryLocationCode
			,source.NSVCode
			,source.ExtraMessage
			,source.Stocked
			,source.DefinedDailyDoseValue
			,source.DefinedDailyDoseUnits
			,source.UserField1
			,source.UserField2
			,source.UserField3
			,source.PIPCode
			,source.MasterPIPCode
			,source.HighCostProduct
			,source.EDILinkCode
			,source.OuterPackSize
			,source.SiteProductCode
		)
	;

	--WardStockList
	merge 
		Pharmacy.WardStockList target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,WardStockListCode = WWardStockList.WWardStockListID
			,SupplierCode = WWardStockList.WSupplierID
			,NSVCode = WWardStockList.NSVcode
			,TopUpLevel = WWardStockList.TopupLvl
			,PrintLabel = WWardStockList.PrintLabel
			,SiteCode = WWardStockList.SiteID
			,Barcode = WWardStockList.Barcode
		from
			[$(Pharmacy_Central)].icwsys.WWardStockList
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.WardStockListCode = target.WardStockListCode

	when matched then

		update
		set
			 SupplierCode = source.SupplierCode
			,NSVCode = source.NSVCode
			,TopUpLevel = source.TopUpLevel
			,PrintLabel = source.PrintLabel
			,SiteCode = source.SiteCode
			,Barcode = source.Barcode

	when not matched then

		insert
		(
			 InterfaceCode
			,WardStockListCode
			,SupplierCode
			,NSVCode
			,TopUpLevel
			,PrintLabel
			,SiteCode
			,Barcode
		)
		values
		(
			 source.InterfaceCode
			,source.WardStockListCode
			,source.SupplierCode
			,source.NSVCode
			,source.TopUpLevel
			,source.PrintLabel
			,source.SiteCode
			,source.Barcode
		)
	;

	--GP
	merge 
		Pharmacy.GP target
	using
		(
		select
			 InterfaceCode = @interfaceCode
			,GPCode = GP.EntityID
			,GP = case when GPEntity.Description = '' then null else GPEntity.Description end
			,GPClassificationID = GP.GPClassificationID
			,GPClassification = GPClassification.Description
			,GPClassificationDetail = GPClassification.Detail
			,GPRegistrationNo = GP.RegNumber
			,Contract = GP.Contract
			,Obstetric = GP.Obstetric
			,JobShare = GP.JobShare
			,Trainer = GP.Trainer
		from
			[$(Pharmacy_Central)].icwsys.GP GP

		left join [$(Pharmacy_Central)].icwsys.GPClassification GPClassification
		on	GPClassification.GPClassificationID = GP.GPClassificationID

		left join [$(Pharmacy_Central)].icwsys.Entity GPEntity
		on	GPEntity.EntityID = GP.EntityID
		) source
	on	source.InterfaceCode = target.InterfaceCode
	and	source.GPCode = target.GPCode

	when matched then

		update
		set
			 GP = source.GP
			,GPClassificationID = source.GPClassificationID
			,GPClassification = source.GPClassification
			,GPClassificationDetail = source.GPClassificationDetail
			,GPRegistrationNo = source.GPRegistrationNo
			,Contract = source.Contract
			,Obstetric = source.Obstetric
			,JobShare = source.JobShare
			,Trainer = source.Trainer

	when not matched then

		insert
		(
			 InterfaceCode
			,GPCode
			,GP
			,GPClassificationID
			,GPClassification
			,GPClassificationDetail
			,GPRegistrationNo
			,Contract
			,Obstetric
			,JobShare
			,Trainer
		)
		values
		(
			 source.InterfaceCode
			,source.GPCode
			,source.GP
			,source.GPClassificationID
			,source.GPClassification
			,source.GPClassificationDetail
			,source.GPRegistrationNo
			,source.Contract
			,source.Obstetric
			,source.JobShare
			,source.Trainer
		)
	;

--Sex
		insert
		into
			 Pharmacy.Sex
		(
			 InterfaceCode
			,SexCode
			,Sex
		)
		select
			 InterfaceCode = @interfaceCode
			,SexCode = GenderID
			,Sex = Description
		from
			[$(Pharmacy_Central)].icwsys.Gender
		where
			not exists
			(
			select
				1
			from
				Pharmacy.Sex
			where
				Sex.InterfaceCode = @interfaceCode
			and	Sex.SexCode = Gender.GenderID
			)			

end

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins' +
	', InterfaceCode ' + @interfaceCode

exec Utility.WriteAuditLogEvent 'Pharmacy.ExtractAscribePharmacyReferenceData', @Stats, @StartTime

﻿

CREATE view [Observation].[AlertClosureReason] as

select 
	AlertClosureReasonID = ReferenceID
	,AlertClosureReasonCode = ReferenceCode
	,AlertClosureReason = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'ALCLR'




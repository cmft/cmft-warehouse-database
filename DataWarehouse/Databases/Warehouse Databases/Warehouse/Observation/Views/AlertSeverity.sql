﻿


CREATE view [Observation].[AlertSeverity] as

select 
	 AlertSeverityID = ReferenceID
	,AlertSeverityCode = ReferenceCode
	,AlertSeverity = Description
	,AlertSeverityGroup = 
						case
							when left(Description, 4) = 'High' then 'High'
							when left(Description, 3) = 'Med' then 'Medium'
							when left(Description, 3) = 'Low' then 'Low'
							when left(Description, 3) = 'Red' then 'Red'
							when left(Description, 5) in ('Amber', 'Green') then Description
							else 'N/A'
						end
	--,AlertSeverityGroupOrder = case
	--								when left(Description, 4) = 'High' then 1
	--								when left(Description, 3) = 'Med' then 2
	--								when left(Description, 3) = 'Med' then 3
	--								when left(Description, 3) = 'Red' then 1
	--								when left(Description, 5) = 'Amber' then 2
	--								else 4
	--							end
	,ContextCode = 'CEN||PTRACK'
from 
	Observation.ReferenceBase
where
	DomainCode = 'ALSEV'
      




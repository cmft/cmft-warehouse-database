﻿

CREATE view [Observation].[AlertReason] as

select 
	 AlertReasonID = ReferenceID
	,AlertReasonCode = ReferenceCode
	,AlertReason = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'ALRAR'




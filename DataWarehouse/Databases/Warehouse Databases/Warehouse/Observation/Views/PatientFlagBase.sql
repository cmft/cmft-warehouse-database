﻿

create view [Observation].[PatientFlagBase] as

select 
	 PatientFlagID = ReferenceID
	,PatientFlagCode = ReferenceCode
	,PatientFlag = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'PFLAG'




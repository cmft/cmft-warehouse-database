﻿
create view Observation.List as

select 
	 ListID = ReferenceID
	,ListCode = ReferenceCode
	,List = [Description]
	,ContextCode = 'CEN||PTRACK'
from 
	Observation.ReferenceBase
where
	exists
		(
		select
			1
		from
			Observation.MeasureBase
		where
			ReferenceBase.DomainCode = MeasureBase.ListCode
		)
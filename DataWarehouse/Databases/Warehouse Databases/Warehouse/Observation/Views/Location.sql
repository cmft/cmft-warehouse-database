﻿CREATE view Observation.Location as

select 
	 LocationID = ReferenceID
	,LocationCode = ReferenceCode
	,Location = Description   
	,Division
	,InterfaceCode = 'PTRACK' 
from 
	Observation.ReferenceBase

left outer join Observation.LocationMap
on	LocationCode = ReferenceCode

where 
	DomainCode = 'WARDD'

﻿














CREATE view [Observation].[OverallAssessedStatus] as

select 
	 OverallAssessedStatusID = ReferenceID
	,OverallAssessedStatusCode = ReferenceCode
	,OverallAssessedStatus = Description
	,OverallAssessedStatusGroup = 
					case
					when Description = 'Not Collected'
					then 'Not Collected'
					when Description =  'Not Taken'
					then 'Not Taken'
					when ReferenceCode = 101
					then 'Green'
					when ReferenceCode = 102
					then 'Amber'
					when ReferenceCode = 103
					then 'Red'
					when ReferenceCode between 0 and 2 
					then 'None'
					when ReferenceCode = 3
					then 'Low'
					when ReferenceCode between 4 and 5
					then 'Medium'
					when ReferenceCode >= 6
					then 'High'
					else 'N/A'
					end
	--,OverallAssessedStatusScore = 
	--				case
	--				when Description in ('Not Collected', 'Not Taken') 
	--				then 0
	--				when Description = 'Green'
	--				then 0
	--				when Description = 'Amber'
	--				then 4
	--				when Description = 'Red'
	--				then 6
	--				else ReferenceCode
	--				end
	,ContextCode = 'PTRACK'
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'ASDST'
















﻿


CREATE view [Observation].[OverallRiskIndex] as

select distinct 
	 OverallRiskIndexCode = [OverallRiskIndexCode]
	--,OverallRiskIndex = [OverallRiskIndexCode]
	,OverallRiskIndex = 
							case
								when [OverallRiskIndexCode] between 0 and 2
								then 'EWS 0-2'
								when [OverallRiskIndexCode] = 3
								then 'Low'
								when [OverallRiskIndexCode] between 4 and 5
								then 'Medium'
								when [OverallRiskIndexCode] >= 6
								then 'High'
								else 'N/A'
							end
	--,OverallRiskIndexOrder = 
	--						case
	--							when [OverallRiskIndexCode] between 0 and 2
	--							then 4
	--							when [OverallRiskIndexCode] = 3
	--							then 3
	--							when [OverallRiskIndexCode] between 4 and 5
	--							then 2
	--							when [OverallRiskIndexCode] >= 6
	--							then 1
	--							else 5
	--						end
from 
	Observation.ObservationSet
where
	OverallRiskIndexCode is not null




﻿




CREATE view [Observation].[ClinicianSeniority] as

select 
	ClinicianSeniorityID = ReferenceID
	,ClinicianSeniorityCode = ReferenceCode
	,ClinicianSeniority = Description
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'DRSEN'







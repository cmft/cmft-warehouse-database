﻿

CREATE view [Observation].[Specialty] as
	
select 
	SpecialtyID = ReferenceID
	,SpecialtyCode = ReferenceCode
	,Specialty = [Description]  
	,Division
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
left outer join
	Observation.[SpecialtyMap]
	on [SpecialtyCode] = ReferenceCode
where 
	DomainCode = 'UNITT'





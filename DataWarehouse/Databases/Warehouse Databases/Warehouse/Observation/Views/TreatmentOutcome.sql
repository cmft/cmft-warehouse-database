﻿



CREATE view [Observation].[TreatmentOutcome] as

select 
	 TreatmentOutcomeID = ReferenceID
	,TreatmentOutcomeCode = ReferenceCode
	,TreatmentOutcome = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'TROCM'






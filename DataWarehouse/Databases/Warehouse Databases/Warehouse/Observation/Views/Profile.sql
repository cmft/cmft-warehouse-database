﻿






CREATE view [Observation].[Profile] as
	
select 
	ProfileBase.ProfileID
	,ProfileBase.Profile
	,ProfileBase.Description
	,ProfileBase.Template
	,ProfileBase.StartTime
	,ProfileBase.EndTime
	,ProfileBase.Deleted
	,ProfileBase.StepDownObservationProfileID
	,ProfileBase.Manual
	,ProfileBase.CreatedBy
	,ProfileBase.Created
	,ProfileBase.LastModifiedBy
	,ProfileBase.LastModified
	,ProfileBase.InterfaceCode
	,ContextCode = 'CEN||PTRACK' 
	,ProfileGroup = 
				coalesce(
					ProfileMap.ProfileGroup
					,'Unassigned'
					)
	,ProfileGroupOrder = 
				dense_rank() over (
								order by
									case
									when coalesce(
											ProfileMap.ProfileGroup
											,'Unassigned'
											) = 'Unassigned'
									then 0
									when ProfileMap.ProfileGroup like '%Minute%'
									then 1
									when ProfileMap.ProfileGroup like '%Hour%'
									then 2
									when ProfileMap.ProfileGroup like '%Daily%'
									then 3
									else 4
									end
									,ProfileMap.ProfileGroup
								)
from 
	Observation.ProfileBase

left outer join Observation.ProfileMap
on ProfileMap.ProfileID = ProfileBase.ProfileID
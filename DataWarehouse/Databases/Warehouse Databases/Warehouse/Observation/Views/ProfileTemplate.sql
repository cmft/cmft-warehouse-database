﻿CREATE VIEW [Observation].[ProfileTemplate]
	AS

		
select 
	ProfileBase.ProfileID
	,ProfileBase.Profile
	,ProfileBase.Description
	,ProfileBase.Template
	,ProfileBase.StartTime
	,ProfileBase.EndTime
	,ProfileBase.Deleted
	,ProfileBase.StepDownObservationProfileID
	,ProfileBase.Manual
	,ProfileBase.CreatedBy
	,ProfileBase.Created
	,ProfileBase.LastModifiedBy
	,ProfileBase.LastModified
	,ProfileBase.InterfaceCode
	--,InterfaceCode = 'PTRACK' 

from 
	Observation.ProfileBase
where
	Template = 1

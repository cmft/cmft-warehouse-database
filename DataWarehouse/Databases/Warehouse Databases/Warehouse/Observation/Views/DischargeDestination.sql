﻿



CREATE view [Observation].DischargeDestination as

select 
	 DischargeDestinationID = ReferenceID
	,DischargeDestinationCode = ReferenceCode
	,DischargeDestination = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'DISDE'






﻿


create view [Observation].[UserGroup] as

select 
	 UserGroupID = ReferenceID
	,UserGroupCode = ReferenceCode
	,UserGroup = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'RIGRP'





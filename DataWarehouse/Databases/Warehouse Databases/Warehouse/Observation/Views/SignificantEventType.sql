﻿





CREATE view [Observation].[SignificantEventType] as

select 
	 SignificantEventTypeID = ReferenceID
	,SignificantEventTypeCode = ReferenceCode
	,SignificantEventType = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'SIEVT'








﻿

CREATE view [Observation].[AlertRaisedReason] as

select 
	 AlertMessageTypeID = ReferenceID
	,AlertMessageTypeCode = ReferenceCode
	,AlertMessageType = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'ALRAR'




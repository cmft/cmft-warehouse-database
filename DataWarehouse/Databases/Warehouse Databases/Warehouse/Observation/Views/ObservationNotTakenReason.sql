﻿



CREATE view [Observation].[ObservationNotTakenReason] as

select 
	ObservationNotTakenReasonID = ReferenceID
	,ObservationNotTakenReasonCode = ReferenceCode
	,ObservationNotTakenReason = Description
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'ONTRN'






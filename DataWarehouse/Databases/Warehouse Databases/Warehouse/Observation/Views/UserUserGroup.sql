﻿
create view [Observation].[UserUserGroup] as 

select
	UserBase.UserID
    ,UserCode
	,Username =
				Surname + 
						case
						when Forename is null 
						then ''
						else ', ' + Forename
						end
	,UserGroup = coalesce(
					UserGroup
					,'N/A'
				)
from
	Observation.UserBase

left join Observation.UserGroupBase
on	UserGroupBase.UserID = UserBase.UserID
and not exists
		(
		select
			1
		from
			Observation.UserGroupBase LaterUserGroup
		where
			LaterUserGroup.UserID = UserGroupBase.UserID
		and	LaterUserGroup.SourceUniqueID > UserGroupBase.SourceUniqueID
		)

left join Observation.UserGroup
on	UserGroup.UserGroupID = UserGroupBase.UserGroupID


union all

select
	-1
	,'N/A'
	,'N/A'
	,'N/A'









GO


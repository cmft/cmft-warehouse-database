﻿








CREATE view [Observation].[ProfileReason] as

select 
	 ProfileReasonID = ReferenceID
	,ProfileReasonCode = ReferenceCode
	,ProfileReason = [Description]
	,ContextCode = 'CEN||PTRACK' 
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'OARSN'
﻿





CREATE view [Observation].[ObservationStatus] as

select 
	 ObservationStatusID = ReferenceID
	,ObservationStatusCode = ReferenceCode
	,ObservationStatus = [Description]
	,ObservationShortStatus = case
										when [Description] = 'Obs are currently due' then 'Due'
										when [Description] = 'Taken on Time' then 'On time'
										when [Description] = 'Taken Late' then 'Late'
										when [Description] = 'Obs Missed' then 'Missed'
										--when [Description] = 'Obs Due time was skipped' then 'Skipped' 
										when [Description] = 'Obs Due time was skipped' then 'Additional' -- S.Ingleby requested label 14.3.14 and 9.4.15
										else [Description]
									end
	--,ObservationStatusOrder = case
	--										when [Description] = 'Obs are currently due' then 4
	--										when [Description] = 'Taken on Time' then 3
	--										when [Description] = 'Taken Late' then 2
	--										when [Description] = 'Obs Missed' then 1
	--										when [Description] = 'Obs Due time was skipped' then 5
	--										else 6
	--									end
	,ContextCode = 'CEN||PTRACK'
from 
	Observation.[ReferenceBase]
where
	DomainCode = 'ODTST'








﻿CREATE TABLE [Observation].[ReferenceBase] (
    [ReferenceID]          INT           NOT NULL,
    [DomainCode]           CHAR (5)      NULL,
    [ReferenceCode]        VARCHAR (20)  NULL,
    [SystemFlag]           BIT           NULL,
    [Description]          VARCHAR (255) NULL,
    [HiddenFlag]           BIT           NULL,
    [DefaultFlag]          BIT           NULL,
    [SortOrder]            INT           NULL,
    [StartTime]            DATETIME      NULL,
    [EndTime]              DATETIME      NULL,
    [DeletedFlag]          BIT           NULL,
    [CreatedByUserID]      INT           NULL,
    [CreatedTime]          DATETIME      NULL,
    [LastModifiedByUserID] INT           NULL,
    [LastModifiedTime]     DATETIME      NULL,
    [InterfaceCode]        VARCHAR (10)  NULL,
    CONSTRAINT [PK__Referenc__E1A99A792903E9E2] PRIMARY KEY CLUSTERED ([ReferenceID] ASC)
);




﻿CREATE TABLE [Observation].[StatisticalAnalysisExtract] (
    [ProviderSpellNo]               VARCHAR (50)  NULL,
    [AgeOnAdmission]                SMALLINT      NULL,
    [SexCode]                       VARCHAR (1)   NULL,
    [PostcodeDistrict]              VARCHAR (4)   NULL,
    [NationalAdmissionMethodCode]   VARCHAR (2)   NOT NULL,
    [NationalAdmissionSourceCode]   VARCHAR (2)   NOT NULL,
    [AdmissionFinancialYear]        NVARCHAR (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AdmissionFinancialMonth]       VARCHAR (2)   NULL,
    [AdmissionDayOfTheWeek]         NVARCHAR (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    [AdmissionOutOfHours]           INT           NOT NULL,
    [PrimaryDiagnosisCode]          VARCHAR (6)   NULL,
    [PrimaryOperationCode]          VARCHAR (10)  NULL,
    [DischargeOutOfHours]           INT           NOT NULL,
    [NationalDischargeMethodCode]   VARCHAR (1)   NOT NULL,
    [SpellLOS]                      INT           NULL,
    [WardID]                        INT           NULL,
    [SpecialtyID]                   INT           NULL,
    [Division]                      VARCHAR (255) NULL,
    [ObservationCount]              INT           NULL,
    [AverageRiskScore]              INT           NULL,
    [MinimumRiskScore]              INT           NULL,
    [MaximumRiskScore]              INT           NULL,
    [FirstObservationSetOutOfHours] INT           NULL,
    [FirstRiskScore]                INT           NULL,
    [LastObservationSetOutOfHours]  INT           NULL,
    [LastRiskScore]                 INT           NULL,
    [CardiacArrest]                 INT           NULL
);




GO
GRANT SELECT
    ON OBJECT::[Observation].[StatisticalAnalysisExtract] TO [CMMC\Catherine.Fullwood]
    AS [dbo];


GO
GRANT CONTROL
    ON OBJECT::[Observation].[StatisticalAnalysisExtract] TO [CMMC\Catherine.Fullwood]
    AS [dbo];


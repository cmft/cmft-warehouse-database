﻿CREATE TABLE [Observation].[Observation] (
    [ObservationRecno]             INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]               INT          NOT NULL,
    [ObservationSetSourceUniqueID] INT          NOT NULL,
    [MeasureID]                    INT          NOT NULL,
    [NumericResult]                FLOAT (53)   NULL,
    [ListID]                       INT          NULL,
    [FlagCode]                     BIT          NULL,
    [CreatedByUserID]              INT          NOT NULL,
    [CreatedTime]                  DATETIME     NOT NULL,
    [ModifiedByUserID]             INT          NOT NULL,
    [LastModifiedTime]             DATETIME     NOT NULL,
    [InterfaceCode]                VARCHAR (11) NOT NULL,
    [Created]                      DATETIME     NULL,
    [Updated]                      DATETIME     NULL,
    [ByWhom]                       VARCHAR (50) NULL,
    CONSTRAINT [PK_Observation_ObservationSourceUniqueID] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);






GO
CREATE NONCLUSTERED INDEX [IX_Observation_ObservationSetSourceUniqueID]
    ON [Observation].[Observation]([ObservationSetSourceUniqueID] ASC);


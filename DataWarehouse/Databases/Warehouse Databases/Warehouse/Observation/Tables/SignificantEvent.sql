﻿CREATE TABLE [Observation].[SignificantEvent] (
    [SignificantEventRecno]   INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]          INT           NOT NULL,
    [CasenoteNumber]          VARCHAR (20)  NULL,
    [DateOfBirth]             DATE          NULL,
    [SpecialtyID]             INT           NULL,
    [LocationID]              INT           NULL,
    [AdmissionSourceUniqueID] INT           NOT NULL,
    [SignificantEventTypeID]  INT           NOT NULL,
    [StartTime]               DATETIME      NOT NULL,
    [EndTime]                 DATETIME      NULL,
    [Comment]                 VARCHAR (MAX) NULL,
    [CreatedByUserID]         INT           NOT NULL,
    [CreatedTime]             DATETIME      NOT NULL,
    [LastModifiedByUserID]    INT           NOT NULL,
    [LastModifiedTime]        DATETIME      NOT NULL,
    [InterfaceCode]           VARCHAR (11)  NOT NULL,
    [Created]                 DATETIME      NULL,
    [Updated]                 DATETIME      NULL,
    [ByWhom]                  VARCHAR (50)  NULL
);




﻿CREATE TABLE [Observation].[TemplatePriorityBase] (
    [TemplatePriorityID]  INT         NOT NULL,
    [TableCode]           CHAR (5)    NOT NULL,
    [TableID]             INT         NOT NULL,
    [DrivingTemplateCode] CHAR (5)    NOT NULL,
    [DrivingTemplateID]   INT         NULL,
    [Priority]            INT         NOT NULL,
    [StartTime]           DATETIME    NOT NULL,
    [EndTime]             DATETIME    NULL,
    [Deleted]             BIT         NOT NULL,
    [CreatedBy]           INT         NOT NULL,
    [CreatedTime]         DATETIME    NOT NULL,
    [LastModifiedBy]      INT         NOT NULL,
    [LastModifiedTime]    DATETIME    NOT NULL,
    [HierarchyID]         INT         NULL,
    [HierarchyItemID]     INT         NULL,
    [InterfaceCode]       VARCHAR (6) NULL,
    CONSTRAINT [PK__Template__8B63E7E10981E83F] PRIMARY KEY CLUSTERED ([TemplatePriorityID] ASC)
);


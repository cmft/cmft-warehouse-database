﻿CREATE TABLE [Observation].[PatientFlag] (
    [PatientFlagRecno]        INT           IDENTITY (1, 1) NOT NULL,
    [CasenoteNumber]          VARCHAR (20)  NULL,
    [DateOfBirth]             DATE          NULL,
    [SourceUniqueID]          INT           NOT NULL,
    [AdmissionSourceUniqueID] INT           NOT NULL,
    [PatientFlagID]           INT           NOT NULL,
    [StartTime]               DATETIME      NOT NULL,
    [EndTime]                 DATETIME      NULL,
    [Comment]                 VARCHAR (MAX) NULL,
    [CreatedByUserID]         INT           NOT NULL,
    [CreatedTime]             DATETIME      NOT NULL,
    [LastModifiedByUserID]    INT           NOT NULL,
    [LastModifiedTime]        DATETIME      NOT NULL,
    [EWSDisabled]             BIT           NOT NULL,
    [InterfaceCode]           VARCHAR (11)  NOT NULL,
    [Created]                 DATETIME      NULL,
    [Updated]                 DATETIME      NULL,
    [ByWhom]                  VARCHAR (50)  NULL,
    CONSTRAINT [PK__PatientF__613D576F0BB44974] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);




﻿CREATE TABLE [Observation].[ObservationProfile] (
    [ObservationProfileRecno] INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]          INT          NOT NULL,
    [CasenoteNumber]          VARCHAR (20) NOT NULL,
    [DateOfBirth]             DATETIME     NOT NULL,
    [SpecialtyID]             INT          NOT NULL,
    [LocationID]              INT          NOT NULL,
    [AdmissionSourceUniqueID] INT          NOT NULL,
    [ProfileID]               INT          NOT NULL,
    [TemplatePriorityID]      INT          NULL,
    [DrivingTableCode]        VARCHAR (5)  NULL,
    [DrivingTableID]          INT          NULL,
    [ProfileReasonID]         INT          NOT NULL,
    [StepNumber]              INT          NOT NULL,
    [IterationNumber]         INT          NOT NULL,
    [StartTime]               DATETIME     NOT NULL,
    [EndTime]                 DATETIME     NULL,
    [CreatedBy]               INT          NOT NULL,
    [CreatedTime]             DATETIME     NOT NULL,
    [LastModifiedBy]          INT          NOT NULL,
    [LastModifiedTime]        DATETIME     NOT NULL,
    [Active]                  BIT          NOT NULL,
    [InterfaceCode]           VARCHAR (11) NOT NULL,
    [Created]                 DATETIME     NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) NULL,
    CONSTRAINT [PK__Observat__613D576F14F39AEB] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);




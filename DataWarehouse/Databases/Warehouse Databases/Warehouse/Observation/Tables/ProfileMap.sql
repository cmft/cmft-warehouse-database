﻿CREATE TABLE [Observation].[ProfileMap] (
    [ProfileID]    INT          NOT NULL,
    [ProfileGroup] VARCHAR (50) NULL,
    CONSTRAINT [PK_ProfileMap] PRIMARY KEY CLUSTERED ([ProfileID] ASC)
);


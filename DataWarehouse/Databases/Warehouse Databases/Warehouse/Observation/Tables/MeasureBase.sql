﻿CREATE TABLE [Observation].[MeasureBase] (
    [MeasureID]            INT           NOT NULL,
    [ParentMeasureID]      INT           NULL,
    [MeasureCode]          VARCHAR (10)  NOT NULL,
    [Measure]              VARCHAR (255) NOT NULL,
    [Complex]              BIT           NOT NULL,
    [Unit]                 VARCHAR (20)  NULL,
    [DataTypeCode]         VARCHAR (5)   NULL,
    [Format]               VARCHAR (50)  NULL,
    [MinimumValue]         FLOAT (53)    NULL,
    [MaximumValue]         FLOAT (53)    NULL,
    [ListCode]             VARCHAR (10)  NULL,
    [StartTime]            DATETIME      NOT NULL,
    [EndTime]              DATETIME      NULL,
    [Deleted]              BIT           NOT NULL,
    [CreatedByUserID]      INT           NOT NULL,
    [CreatedTime]          DATETIME      NOT NULL,
    [LastModifiedByUserID] INT           NOT NULL,
    [LastModifiedTime]     DATETIME      NOT NULL,
    [InterfaceCode]        VARCHAR (6)   NOT NULL,
    CONSTRAINT [PK__MeasureB__8C56D76062681B1E] PRIMARY KEY CLUSTERED ([MeasureID] ASC)
);


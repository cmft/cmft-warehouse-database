﻿CREATE TABLE [Observation].[SpecialtyMap] (
    [SpecialtyCode] VARCHAR (50)  NOT NULL,
    [Specialty]     VARCHAR (255) NULL,
    [Division]      VARCHAR (255) NULL,
    CONSTRAINT [PK_SpecialtyMap] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


﻿CREATE TABLE [Observation].[UserBase] (
    [UserID]               INT           NOT NULL,
    [UserCode]             VARCHAR (20)  NULL,
    [TitleCode]            INT           NULL,
    [Surname]              VARCHAR (50)  NULL,
    [Forename]             VARCHAR (50)  NULL,
    [Initials]             VARCHAR (10)  NULL,
    [UserPassword]         VARCHAR (64)  NULL,
    [SurnameUpper]         VARCHAR (50)  NULL,
    [ForenameUpper]        VARCHAR (50)  NULL,
    [DefaultWardCode]      INT           NULL,
    [Note]                 VARCHAR (MAX) NULL,
    [SystemUserFlag]       BIT           NULL,
    [LockedFlag]           BIT           NULL,
    [DeletedFlag]          BIT           NULL,
    [CreatedByUserID]      INT           NULL,
    [CreatedTime]          DATETIME      NULL,
    [LastModifiedByUserID] INT           NULL,
    [LastModifiedTime]     DATETIME      NULL,
    [UserSalt]             VARCHAR (32)  NULL,
    [ChangePasswordFlag]   BIT           NULL,
    [StartTime]            DATETIME      NULL,
    [EndTime]              DATETIME      NULL,
    [PasswordChangedTime]  DATETIME      NOT NULL,
    [InterfaceCode]        VARCHAR (10)  NULL,
    CONSTRAINT [PK__UserBase__1788CCAC5DED7EC4] PRIMARY KEY CLUSTERED ([UserID] ASC)
);




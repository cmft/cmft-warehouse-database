﻿CREATE TABLE [Observation].[LocationMap] (
    [LocationCode] VARCHAR (50)  NOT NULL,
    [Location]     VARCHAR (255) NULL,
    [Division]     VARCHAR (255) NULL,
    CONSTRAINT [PK_LocationMap] PRIMARY KEY CLUSTERED ([LocationCode] ASC)
);


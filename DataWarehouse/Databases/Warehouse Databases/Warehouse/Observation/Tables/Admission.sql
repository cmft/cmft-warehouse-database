﻿CREATE TABLE [Observation].[Admission] (
    [AdmissionRecno]         INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]         INT           NOT NULL,
    [CasenoteNumber]         VARCHAR (20)  NOT NULL,
    [DateOfBirth]            DATE          NULL,
    [SexID]                  INT           NOT NULL,
    [Surname]                VARCHAR (50)  NOT NULL,
    [Forename]               VARCHAR (50)  NULL,
    [AdmissionTime]          DATETIME      NOT NULL,
    [StartSpecialtyID]       INT           NOT NULL,
    [StartLocationID]        INT           NOT NULL,
    [DischargeTime]          DATETIME      NULL,
    [EndSpecialtyID]         INT           NULL,
    [EndLocationID]          INT           NULL,
    [TreatmentOutcomeID]     INT           NOT NULL,
    [DischargeDestinationID] INT           NOT NULL,
    [Comment]                VARCHAR (MAX) NULL,
    [CreatedByUserID]        INT           NOT NULL,
    [CreatedTime]            DATETIME      NOT NULL,
    [LastModifiedByUserID]   INT           NOT NULL,
    [LastModifiedTime]       DATETIME      NOT NULL,
    [InterfaceCode]          VARCHAR (11)  NOT NULL,
    [Created]                DATETIME      NULL,
    [Updated]                DATETIME      NULL,
    [ByWhom]                 VARCHAR (50)  NULL
);






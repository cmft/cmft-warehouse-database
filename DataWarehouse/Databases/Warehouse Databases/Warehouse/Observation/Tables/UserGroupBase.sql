﻿CREATE TABLE [Observation].[UserGroupBase] (
    [SourceUniqueID]      INT      NOT NULL,
    [UserID]              INT      NOT NULL,
    [UserGroupID]         INT      NOT NULL,
    [DeletedFlag]         BIT      NOT NULL,
    [CreatedByUserID]     INT      NOT NULL,
    [CreatedTime]         DATETIME NOT NULL,
    [LastModifedByUserID] INT      NOT NULL,
    [LastModifiedTime]    DATETIME NOT NULL,
    CONSTRAINT [PK__UserGrou__613D576F63A6581A] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);




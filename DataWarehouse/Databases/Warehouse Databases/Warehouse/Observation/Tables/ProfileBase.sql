﻿CREATE TABLE [Observation].[ProfileBase] (
    [ProfileID]                    INT           NOT NULL,
    [Profile]                      VARCHAR (50)  NOT NULL,
    [Description]                  VARCHAR (255) NOT NULL,
    [Template]                     BIT           NOT NULL,
    [StartTime]                    DATETIME      NOT NULL,
    [EndTime]                      DATETIME      NULL,
    [Deleted]                      BIT           NOT NULL,
    [StepDownObservationProfileID] INT           NULL,
    [Manual]                       BIT           NOT NULL,
    [CreatedBy]                    INT           NOT NULL,
    [Created]                      DATETIME      NOT NULL,
    [LastModifiedBy]               INT           NOT NULL,
    [LastModified]                 DATETIME      NOT NULL,
    [InterfaceCode]                VARCHAR (6)   NOT NULL,
    CONSTRAINT [PK__ProfileB__8FBB6E5800ECA23E] PRIMARY KEY CLUSTERED ([ProfileID] ASC)
);


﻿CREATE TABLE [Recall].[LocationBase] (
    [LocationCode] INT           NOT NULL,
    [Location]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([LocationCode] ASC)
);


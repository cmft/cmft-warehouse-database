﻿CREATE TABLE [Recall].[UnitBase] (
    [UnitsCode] INT           NOT NULL,
    [Units]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([UnitsCode] ASC)
);


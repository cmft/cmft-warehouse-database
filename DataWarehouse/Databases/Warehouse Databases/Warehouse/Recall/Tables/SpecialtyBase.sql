﻿CREATE TABLE [Recall].[SpecialtyBase] (
    [SpecialtyCode] INT           NOT NULL,
    [Specialty]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


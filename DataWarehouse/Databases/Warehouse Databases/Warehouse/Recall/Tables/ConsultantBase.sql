﻿CREATE TABLE [Recall].[ConsultantBase] (
    [ConsultantCode] INT           NOT NULL,
    [Consultant]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([ConsultantCode] ASC)
);


﻿CREATE TABLE [Recall].[DrugBase] (
    [DrugCode] INT           NOT NULL,
    [Drug]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([DrugCode] ASC)
);


﻿CREATE TABLE [Infection].[Result] (
    [ResultRecno]            INT           IDENTITY (1, 1) NOT NULL,
    [AlertTypeCode]          VARCHAR (17)  NOT NULL,
    [TestCategoryID]         INT           NOT NULL,
    [Positive]               BIT           NULL,
    [SpecimenNumber]         VARCHAR (18)  NOT NULL,
    [PatientName]            VARCHAR (50)  NULL,
    [DistrictNo]             VARCHAR (14)  NULL,
    [NHSNumber]              VARCHAR (15)  NULL,
    [SexCode]                VARCHAR (1)   NULL,
    [DateOfBirth]            DATE          NULL,
    [SpecimenCollectionDate] DATE          NULL,
    [SpecimenCollectionTime] DATETIME      NULL,
    [PreviousPositiveDate]   VARCHAR (50)  NULL,
    [OrganismCode]           VARCHAR (13)  NOT NULL,
    [SampleTypeCode]         VARCHAR (100) NULL,
    [SampleSubTypeCode]      VARCHAR (15)  NULL,
    [SampleQualifierCode]    VARCHAR (100) NULL,
    [AuthorisationDate]      DATE          NOT NULL,
    [ExtractFileName]        VARCHAR (30)  NULL,
    [InterfaceCode]          VARCHAR (10)  NULL,
    [Created]                DATETIME      NULL,
    [Updated]                DATETIME      NULL,
    [ByWhom]                 VARCHAR (50)  NULL,
    CONSTRAINT [PK__Result__EE348ABE668CE666] PRIMARY KEY CLUSTERED ([AlertTypeCode] ASC, [TestCategoryID] ASC, [SpecimenNumber] ASC, [OrganismCode] ASC, [AuthorisationDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Result_ResultRecno]
    ON [Infection].[Result]([ResultRecno] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Result_DistrictNo_SpecimenCollectionTime]
    ON [Infection].[Result]([DistrictNo] ASC, [SpecimenCollectionTime] ASC);


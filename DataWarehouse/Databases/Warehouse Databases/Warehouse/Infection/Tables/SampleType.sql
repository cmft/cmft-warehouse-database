﻿CREATE TABLE [Infection].[SampleType] (
    [SampleTypeCode] VARCHAR (15)  NOT NULL,
    [SampleType]     VARCHAR (250) NULL,
    CONSTRAINT [PK__SampleTy__96EBE4927DA555E8] PRIMARY KEY CLUSTERED ([SampleTypeCode] ASC)
);


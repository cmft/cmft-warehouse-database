﻿CREATE TABLE [Infection].[Organism] (
    [OrganismCode] VARCHAR (10)  NOT NULL,
    [Organism]     VARCHAR (200) NULL,
    CONSTRAINT [PK__Organism__1A1170D4026A0B05] PRIMARY KEY CLUSTERED ([OrganismCode] ASC)
);


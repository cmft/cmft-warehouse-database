﻿CREATE TABLE [Infection].[SampleSubType] (
    [SampleSubTypeCode] VARCHAR (15)  NOT NULL,
    [SampleSubType]     VARCHAR (200) NULL,
    CONSTRAINT [PK__SampleSu__987D25DA79D4C504] PRIMARY KEY CLUSTERED ([SampleSubTypeCode] ASC)
);


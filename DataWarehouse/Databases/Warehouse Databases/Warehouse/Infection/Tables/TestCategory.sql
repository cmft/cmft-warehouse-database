﻿CREATE TABLE [Infection].[TestCategory] (
    [TestCategoryID] INT          IDENTITY (1, 1) NOT NULL,
    [TestCategory]   VARCHAR (10) NULL,
    CONSTRAINT [PK__TestCate__2CE3751D713F7F03] PRIMARY KEY CLUSTERED ([TestCategoryID] ASC)
);


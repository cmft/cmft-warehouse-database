﻿CREATE TABLE [Infection].[SampleQualifier] (
    [SampleQualifierCode] VARCHAR (15) NOT NULL,
    [SampleQualifier]     VARCHAR (50) NULL,
    CONSTRAINT [PK__SampleQu__67FBCBCC76043420] PRIMARY KEY CLUSTERED ([SampleQualifierCode] ASC)
);


﻿
CREATE VIEW [PatientrackStaging].[ObservationMeasure] as

select
	 ObservationMeasureID = OBMSR_PK
	,ParentObservationMeasureID = PARENT_OBMSR_PK
	,ObservationMeasureCode = CODE
	,ObservationMeasure = NAME
	,Complex = COMPLEX_FLAG
	,Unit = UNIT
	,Format = FORMAT
	,MinimumValue = MIN_VALUE
	,MaximumValue = MAX_VALUE
	,StartTime = START_DTTM
	,EndTime = END_DTTM
	,Deleted = DELETED_FLAG
	,CreatedByUserID = CREATED_BY_USERR_PK
	,CreatedTime = CREATED_DTTM
	,LastModifiedByUserID = LAST_MODIFIED_BY_USERR_PK
	,LastModifiedTime = LAST_MODIFIED_DTTM
from
	[$(PatientrackSS)].dbo.OBSERVATION_MEASURE
﻿
CREATE VIEW [PatientrackStaging].[ObservationSet] as

select
	ObservationSetID = ObservationSet.OBSET_PK
	,CasenoteNumber = PatientIdentifier.PATIENT_IDENTIFIER
	,StartTime = ObservationSet.OBS_START_DTTM
	,OverallRiskIndexCode = ObservationSet.OVERALL_RISK_INDEX
from
	[$(PatientrackSS)].dbo.OBSERVATION_SET ObservationSet

inner join [$(PatientrackSS)].dbo.ADMISSION Admission
on	Admission.ADMSN_PK = ObservationSet.ADMSN_PK
and	isnull(Admission.DELETED_FLAG, 0) = 0

inner join [$(PatientrackSS)].dbo.ADMISSION_PATIENT_IDENTIFIER PatientIdentifier
on	PatientIdentifier.ADMSN_PK = Admission.ADMSN_PK
and	PatientIdentifier.PIDTY_RFVAL = 918 -- patient id
and	isnull(PatientIdentifier.DELETED_FLAG, 0) = 0	

where
	ObservationSet.PARENT_OBSET_PK is null --check
﻿CREATE TABLE [Medisec].[Specialty] (
    [SpecialtyCode] VARCHAR (5)  NOT NULL,
    [Specialty]     VARCHAR (30) NULL,
    PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


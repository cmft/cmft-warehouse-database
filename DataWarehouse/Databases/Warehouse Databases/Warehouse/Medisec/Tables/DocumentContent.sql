﻿CREATE TABLE [Medisec].[DocumentContent] (
    [PatientNumber]                     VARCHAR (8)   NOT NULL,
    [AdmissionDate]                     DATETIME      NOT NULL,
    [ForcebackNumber]                   INT           NOT NULL,
    [TemplateCode]                      VARCHAR (20)  NOT NULL,
    [Content]                           XML           NULL,
    [AdmissionReason]                   VARCHAR (MAX) NULL,
    [Diagnosis]                         VARCHAR (MAX) NULL,
    [Investigations]                    VARCHAR (MAX) NULL,
    [RecommendationsOnFutureManagement] VARCHAR (MAX) NULL,
    [InformationToPatientOrRelatives]   VARCHAR (MAX) NULL,
    [ChangesToMedication]               VARCHAR (MAX) NULL
);


GO
CREATE NONCLUSTERED INDEX [Patient_AdmissionDate]
    ON [Medisec].[DocumentContent]([PatientNumber] ASC, [AdmissionDate] ASC);


﻿CREATE TABLE [Medisec].[Status] (
    [StatusCode] CHAR (2)      NOT NULL,
    [Status]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([StatusCode] ASC)
);


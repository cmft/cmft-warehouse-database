﻿CREATE TABLE [Medisec].[DocumentProgress] (
    [DocumentProgressCode] CHAR (1)      NOT NULL,
    [DocumentProgress]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([DocumentProgressCode] ASC)
);


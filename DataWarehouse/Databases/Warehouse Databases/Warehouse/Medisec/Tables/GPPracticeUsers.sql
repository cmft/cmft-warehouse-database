﻿CREATE TABLE [Medisec].[GPPracticeUsers] (
    [PracticeCode] VARCHAR (8)   NULL,
    [PracticeName] VARCHAR (100) NULL
);


GO
CREATE CLUSTERED INDEX [pk_PracticeCode_GPPracticeUsers]
    ON [Medisec].[GPPracticeUsers]([PracticeCode] ASC);


﻿CREATE TABLE [Medisec].[Consultant] (
    [ConsultantCode] VARCHAR (6)  NULL,
    [Title]          VARCHAR (4)  NULL,
    [Initials]       VARCHAR (3)  NULL,
    [Surname]        VARCHAR (24) NULL,
    [GMCCode]        VARCHAR (8)  NULL,
    [Forename]       VARCHAR (20) NULL,
    [Qualifications] VARCHAR (40) NULL,
    [JobTitle]       VARCHAR (50) NULL
);


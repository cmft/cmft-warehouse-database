﻿CREATE TABLE [Medisec].[DocumentType] (
    [DocumentTypeCode] CHAR (1)     NOT NULL,
    [DocumentType]     VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([DocumentTypeCode] ASC)
);


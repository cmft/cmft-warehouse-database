﻿CREATE TABLE [Medisec].[UserMaster] (
    [SourceUserCode] VARCHAR (20) NULL,
    [UserName]       VARCHAR (50) NULL,
    [UserType]       CHAR (1)     NULL,
    [UserDepartment] VARCHAR (50) NULL
);


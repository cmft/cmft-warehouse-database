﻿CREATE TABLE [PAS].[ClinicGroupBase] (
    [ClinicCode]                        VARCHAR (8)  NOT NULL,
    [ClinicGroupCode]                   VARCHAR (8)  NOT NULL,
    [Description]                       VARCHAR (50) NOT NULL,
    [MfRecStsInd]                       VARCHAR (2)  NOT NULL,
    [SchedulingApplicationAreaExternal] VARCHAR (6)  NOT NULL,
    CONSTRAINT [PK_ClinicGroupBase] PRIMARY KEY CLUSTERED ([ClinicCode] ASC, [ClinicGroupCode] ASC)
);


﻿CREATE TABLE [PAS].[OPCancellationReasonBase] (
    [CancellationCode]        VARCHAR (4)   NOT NULL,
    [Description]             VARCHAR (255) NULL,
    [EbsInboundCancReason]    VARCHAR (3)   NULL,
    [EbsInboundCancReasonInt] VARCHAR (3)   NULL,
    [EbsReasonCode]           VARCHAR (2)   NULL,
    [EbsReasonDesc]           VARCHAR (35)  NULL,
    [MfRecStsInd]             VARCHAR (255) NULL
);


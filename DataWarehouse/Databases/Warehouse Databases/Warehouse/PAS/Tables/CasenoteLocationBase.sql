﻿CREATE TABLE [PAS].[CasenoteLocationBase] (
    [Location]     VARCHAR (10) NOT NULL,
    [LocationName] VARCHAR (20) NULL,
    [MfRecStsInd]  VARCHAR (2)  NOT NULL,
    CONSTRAINT [PK_CasenoteLocationBase] PRIMARY KEY CLUSTERED ([Location] ASC)
);


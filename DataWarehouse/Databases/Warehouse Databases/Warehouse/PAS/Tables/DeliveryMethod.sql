﻿CREATE TABLE [PAS].[DeliveryMethod] (
    [DeliveryMethodCode] VARCHAR (2)   NOT NULL,
    [DeliveryMethod]     VARCHAR (255) NULL,
    [NationalCode]       CHAR (1)      NULL,
    CONSTRAINT [PK_DeliveryMethod] PRIMARY KEY CLUSTERED ([DeliveryMethodCode] ASC)
);


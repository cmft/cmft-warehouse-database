﻿CREATE TABLE [PAS].[StaffGroupBase] (
    [GradeOfStaffCode]     VARCHAR (4)   NOT NULL,
    [GradeOfStaff]         VARCHAR (40)  NULL,
    [MedicalStaffTypeCode] VARCHAR (2)   NULL,
    [MedicalStaffType]     VARCHAR (25)  NULL,
    [MfRecStsInd]          VARCHAR (255) NULL
);


﻿CREATE TABLE [PAS].[SpecialtyBase] (
    [SpecialtyCode]             VARCHAR (4)  NOT NULL,
    [CmtGpfhExemptSpecialtyInt] VARCHAR (1)  NULL,
    [Description]               VARCHAR (32) NULL,
    [DohCode]                   VARCHAR (4)  NULL,
    [GpfhExempt]                VARCHAR (3)  NULL,
    [HaaCode]                   VARCHAR (2)  NULL,
    [KarsIdCode]                VARCHAR (2)  NULL,
    [MfRecStsInd]               VARCHAR (2)  NULL,
    [ScottishIntValue]          VARCHAR (5)  NULL,
    [ScottishLocalCode]         VARCHAR (1)  NULL,
    [SpecGroup]                 VARCHAR (4)  NULL,
    [Specialty]                 VARCHAR (4)  NOT NULL,
    [SpecialtyType]             VARCHAR (2)  NULL,
    [SupraregCode]              VARCHAR (2)  NULL,
    [TreatmentFunction]         VARCHAR (10) NULL,
    [MainSpecialty]             VARCHAR (10) NULL,
    CONSTRAINT [PK_PAS_Specialty] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


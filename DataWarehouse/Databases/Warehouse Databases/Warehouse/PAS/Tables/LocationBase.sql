﻿CREATE TABLE [PAS].[LocationBase] (
    [LocationCode]     VARCHAR (10)  NOT NULL,
    [Location]         VARCHAR (50)  NULL,
    [Location1]        VARCHAR (50)  NULL,
    [LocationTypeCode] VARCHAR (10)  NULL,
    [MfRecStsInd]      VARCHAR (255) NULL,
    [SgLocnLink]       VARCHAR (10)  NULL
);


﻿CREATE TABLE [PAS].[PatientDuplicationBase] (
    [PatientDuplicationId]   INT      IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]        INT      NULL,
    [MasterSourcePatientNo]  INT      NULL,
    [RegistrationDate]       DATE     NULL,
    [IsEarliestRegistration] CHAR (1) NULL,
    CONSTRAINT [PK_PatientDuplication_1] PRIMARY KEY CLUSTERED ([PatientDuplicationId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_PatientDuplication_SourcePatientNo]
    ON [PAS].[PatientDuplicationBase]([SourcePatientNo] ASC)
    INCLUDE([MasterSourcePatientNo]);


﻿CREATE TABLE [PAS].[ScreenTypeBase] (
    [ScreenTypeCode] VARCHAR (4)  NOT NULL,
    [ScreenType]     VARCHAR (20) NULL,
    [Deleted]        BIT          NOT NULL,
    CONSTRAINT [PK__ScreenTy__861809D20B4A3520] PRIMARY KEY CLUSTERED ([ScreenTypeCode] ASC)
);


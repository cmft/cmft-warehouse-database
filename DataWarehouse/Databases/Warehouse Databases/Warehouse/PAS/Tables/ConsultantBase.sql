﻿CREATE TABLE [PAS].[ConsultantBase] (
    [CONSMASTID]          VARCHAR (6)  NOT NULL,
    [Consultant]          VARCHAR (6)  NOT NULL,
    [ConsultantCode2]     VARCHAR (4)  NULL,
    [ContractedSpecialty] VARCHAR (4)  NULL,
    [Deleted]             SMALLINT     NULL,
    [ExternalCons]        VARCHAR (3)  NULL,
    [ForenamePfx]         VARCHAR (6)  NULL,
    [GMCCode]             VARCHAR (8)  NULL,
    [Inits]               VARCHAR (6)  NULL,
    [KornerCode]          VARCHAR (3)  NULL,
    [Midwife]             VARCHAR (3)  NULL,
    [Name]                VARCHAR (30) NULL,
    [PrimarySpecialty]    VARCHAR (4)  NULL,
    [ProviderCode]        VARCHAR (4)  NULL,
    [Surname]             VARCHAR (24) NULL,
    [Created]             DATETIME     NULL,
    [Updated]             DATETIME     NULL,
    [ByWhom]              VARCHAR (50) NULL,
    CONSTRAINT [PK_PAS_Consultant] PRIMARY KEY CLUSTERED ([CONSMASTID] ASC)
);


﻿CREATE TABLE [PAS].[Patient] (
    [SourcePatientNo]   INT           NOT NULL,
    [DateOfBirth]       DATE          NULL,
    [DeathIndicator]    VARCHAR (3)   NULL,
    [DateOfDeath]       DATE          NULL,
    [SexCode]           NVARCHAR (1)  NULL,
    [MaritalStatusCode] NVARCHAR (1)  NULL,
    [Surname]           NVARCHAR (24) NULL,
    [Forenames]         NVARCHAR (20) NULL,
    [Title]             NVARCHAR (5)  NULL,
    [NextOfKin]         NVARCHAR (30) NULL,
    [EthnicOriginCode]  NVARCHAR (4)  NULL,
    [ReligionCode]      NVARCHAR (4)  NULL,
    [PreviousSurname]   NVARCHAR (24) NULL,
    [NHSNumber]         NVARCHAR (17) NULL,
    [NHSNumberStatusId] NVARCHAR (2)  NULL,
    [DOR]               NVARCHAR (3)  NULL,
    [DistrictNo]        NVARCHAR (14) NULL,
    [MilitaryNo]        INT           NULL,
    [AddressLine1]      NVARCHAR (20) NULL,
    [AddressLine2]      NVARCHAR (20) NULL,
    [AddressLine3]      NVARCHAR (20) NULL,
    [AddressLine4]      NVARCHAR (20) NULL,
    [Postcode]          NVARCHAR (10) NULL,
    [RegisteredGpCode]  NVARCHAR (8)  NULL,
    [RegisteredGdpCode] NVARCHAR (6)  NULL,
    [MRSAFlag]          NVARCHAR (3)  NULL,
    [MRSADate]          NVARCHAR (10) NULL,
    [Modified]          SMALLDATETIME NULL,
    [HomePhone]         VARCHAR (23)  NULL,
    [MobilePhone]       VARCHAR (23)  NULL,
    [WorkPhone]         VARCHAR (23)  NULL,
    [RegistrationDate]  DATE          NULL,
    [AEAttendanceToday] BIT           NULL,
    [Deleted]           BIT           NULL,
    [Created]           DATETIME      NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC)
);


GO
CREATE NONCLUSTERED INDEX [Idx_PAS_Patient_PC_Sex_NHSNum__FN_IntDoB]
    ON [PAS].[Patient]([Postcode] ASC, [SexCode] ASC, [NHSNumber] ASC)
    INCLUDE([Forenames], [DateOfBirth]);


GO
CREATE NONCLUSTERED INDEX [Idx_Patient_SourcePatientNumber_BasicDemographics]
    ON [PAS].[Patient]([SourcePatientNo] ASC)
    INCLUDE([DistrictNo], [Forenames], [DateOfBirth], [Surname], [DOR], [SexCode]);


GO
CREATE NONCLUSTERED INDEX [Idx_Patient_DistrictNo]
    ON [PAS].[Patient]([DistrictNo] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_SN_FN_DoB]
    ON [PAS].[Patient]([Surname] ASC, [Forenames] ASC, [DateOfBirth] ASC)
    INCLUDE([SourcePatientNo], [DistrictNo]);


GO
CREATE NONCLUSTERED INDEX [Idx_fuzzylookup_patient]
    ON [PAS].[Patient]([SexCode] ASC, [Forenames] ASC, [Surname] ASC, [DateOfBirth] ASC, [Postcode] ASC)
    INCLUDE([NHSNumber]);


GO
CREATE NONCLUSTERED INDEX [index_NHSNumber_incl_patient_details]
    ON [PAS].[Patient]([NHSNumber] ASC)
    INCLUDE([DistrictNo]);


GO
CREATE NONCLUSTERED INDEX [Indx_NHSLookupDemographics]
    ON [PAS].[Patient]([DateOfBirth] ASC)
    INCLUDE([Forenames], [Postcode], [SexCode], [Surname], [NHSNumber]);


GO
CREATE NONCLUSTERED INDEX [Idx_PAS_Patient__GpCode_i_SourcePatientNo_DistrictNo]
    ON [PAS].[Patient]([RegisteredGpCode] ASC)
    INCLUDE([SourcePatientNo], [DistrictNo]);


GO
CREATE NONCLUSTERED INDEX [DateOfBirthMatch]
    ON [PAS].[Patient]([DateOfDeath] ASC, [NHSNumber] ASC);


﻿CREATE TABLE [PAS].[ReligionBase] (
    [Description]             VARCHAR (20)  NULL,
    [HaaCode]                 VARCHAR (1)   NULL,
    [InternalValue]           VARCHAR (2)   NULL,
    [MfRecStsInd]             VARCHAR (255) NULL,
    [RelgionGroupDescription] VARCHAR (30)  NULL,
    [Religion]                VARCHAR (4)   NOT NULL,
    [ReligionGroup]           VARCHAR (1)   NULL
);


﻿CREATE TABLE [PAS].[OrderStatusBase] (
    [OrderStatusCode] INT          NOT NULL,
    [OrderStatus]     VARCHAR (30) NULL,
    [MfRecStsInd]     BIT          NOT NULL,
    CONSTRAINT [PK__OrderSta__BFAAF46966C2BBE7] PRIMARY KEY CLUSTERED ([OrderStatusCode] ASC)
);


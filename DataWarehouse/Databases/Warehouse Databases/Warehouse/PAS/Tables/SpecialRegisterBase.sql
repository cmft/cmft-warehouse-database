﻿CREATE TABLE [PAS].[SpecialRegisterBase] (
    [SpecialRegisterCode] VARCHAR (4)  NOT NULL,
    [SpecialRegister]     VARCHAR (30) NULL,
    [FormOfWarningCode]   TINYINT      NULL,
    [FormOfWarning]       VARCHAR (9)  NULL,
    [MfRecStsInd]         BIT          NULL,
    CONSTRAINT [PK_PAS_PatientSpecialRegisterBase] PRIMARY KEY CLUSTERED ([SpecialRegisterCode] ASC)
);


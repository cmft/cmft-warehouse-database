﻿CREATE TABLE [PAS].[AugmentedCareLocationBase] (
    [AcpLocationCode]                                 VARCHAR (4)   NULL,
    [AcpLocation]                                     VARCHAR (4)   NULL,
    [AcpLocNatCode]                                   VARCHAR (2)   NULL,
    [ApcAugmentedCareLocationNationalCodeDescription] VARCHAR (50)  NULL,
    [CcUnitCode]                                      VARCHAR (2)   NULL,
    [CcUnitFunctionDescription]                       VARCHAR (50)  NULL,
    [DefaultConsultant]                               VARCHAR (3)   NULL,
    [Description]                                     VARCHAR (30)  NULL,
    [SiteCode]                                        VARCHAR (4)   NULL,
    [MfRecStsInd]                                     VARCHAR (255) NULL,
    [WardCode]                                        VARCHAR (4)   NULL
);


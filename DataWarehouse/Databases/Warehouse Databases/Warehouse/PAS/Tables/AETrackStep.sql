﻿CREATE TABLE [PAS].[AETrackStep] (
    [AETRACKSTEPID]           VARCHAR (4)  NOT NULL,
    [MfRecStsInd]             VARCHAR (1)  NULL,
    [SysTrackingStepCode]     VARCHAR (4)  NULL,
    [TrackingStepCode]        VARCHAR (4)  NOT NULL,
    [TrackingStepDescription] VARCHAR (30) NULL,
    CONSTRAINT [PK_AETrackStep_1] PRIMARY KEY CLUSTERED ([TrackingStepCode] ASC)
);


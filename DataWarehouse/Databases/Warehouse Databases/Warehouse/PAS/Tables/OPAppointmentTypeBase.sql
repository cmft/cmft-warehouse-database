﻿CREATE TABLE [PAS].[OPAppointmentTypeBase] (
    [AppointmentTypeCode]                     VARCHAR (3)  NOT NULL,
    [AppointmentTypeDesc]                     VARCHAR (20) NULL,
    [ApptCategory]                            VARCHAR (3)  NULL,
    [ApptClassification]                      VARCHAR (1)  NULL,
    [EBooking]                                VARCHAR (3)  NULL,
    [EBookingInt]                             VARCHAR (1)  NULL,
    [MfRecStsInd]                             VARCHAR (3)  NULL,
    [ReminderLetter]                          VARCHAR (3)  NULL,
    [SchedulingAppointmentClassification]     VARCHAR (1)  NULL,
    [SchedulingApptClassificationDescription] VARCHAR (17) NULL,
    [SchRemLetterInt]                         VARCHAR (1)  NULL,
    [Urgent]                                  VARCHAR (3)  NULL,
    [Urgent_1]                                VARCHAR (1)  NULL
);


﻿CREATE TABLE [PAS].[BorrowerBase] (
    [BorrowerAddressLine1]     VARCHAR (30) NULL,
    [BorrowerCode]             VARCHAR (6)  NULL,
    [BorrowerPostcode]         VARCHAR (10) NULL,
    [BorrowerSurname]          VARCHAR (22) NULL,
    [BorrowerTelephoneNumber]  VARCHAR (15) NULL,
    [BorrowerTitleAndInitials] VARCHAR (22) NULL,
    [Line2]                    VARCHAR (30) NULL,
    [Line3]                    VARCHAR (30) NULL,
    [Line4]                    VARCHAR (30) NULL,
    [MfRecStsInd]              VARCHAR (2)  NULL
);


GO
CREATE NONCLUSTERED INDEX [BorrowerIndex]
    ON [PAS].[BorrowerBase]([BorrowerCode] ASC)
    INCLUDE([BorrowerSurname], [BorrowerTitleAndInitials]);


﻿CREATE TABLE [PAS].[GpHistory] (
    [GpHistoryID]       VARCHAR (255) NULL,
    [DistrictNo]        VARCHAR (14)  NULL,
    [EffectiveFromDate] DATETIME      NULL,
    [GpAddressLine1]    VARCHAR (255) NULL,
    [GpAddressLine2]    VARCHAR (255) NULL,
    [GpAddressLine3]    VARCHAR (255) NULL,
    [GpAddressLine4]    VARCHAR (255) NULL,
    [GpCode]            VARCHAR (8)   NULL,
    [GpfhCode]          VARCHAR (5)   NULL,
    [Initials]          VARCHAR (4)   NULL,
    [SourcePatientNo]   VARCHAR (9)   NULL,
    [NationalCode]      VARCHAR (7)   NULL,
    [Phone]             VARCHAR (23)  NULL,
    [Postcode]          VARCHAR (10)  NULL,
    [Practice]          VARCHAR (6)   NULL,
    [SequenceNo]        VARCHAR (3)   NULL,
    [Surname]           VARCHAR (24)  NULL,
    [Title]             VARCHAR (4)   NULL,
    [EffectiveToDate]   DATETIME      NULL
);


GO
CREATE NONCLUSTERED INDEX [Idx_PAS_GP_HIST]
    ON [PAS].[GpHistory]([GpCode] ASC, [Practice] ASC, [EffectiveToDate] ASC)
    INCLUDE([GpHistoryID], [DistrictNo], [EffectiveFromDate], [SourcePatientNo]);


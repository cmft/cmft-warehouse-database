﻿CREATE TABLE [PAS].[MaternityAnaesthetic] (
    [MaternityAnaestheticID]            VARCHAR (50) NULL,
    [AnalgesicDuringLabour]             VARCHAR (2)  NULL,
    [DuringLabourAnaestheticCode]       VARCHAR (2)  NULL,
    [DuringLabourAnaestheticReasonCode] VARCHAR (2)  NULL,
    [PostLabourAnaestheticCode]         VARCHAR (1)  NULL,
    [PostLabourAnaestheticReasonCode]   VARCHAR (1)  NULL,
    [SourceSpellNo]                     VARCHAR (12) NOT NULL,
    [SourcePatientNo]                   VARCHAR (12) NOT NULL,
    CONSTRAINT [PK_MaternityAnaesthetic] PRIMARY KEY CLUSTERED ([SourceSpellNo] ASC, [SourcePatientNo] ASC)
);


﻿CREATE TABLE [PAS].[AEAttendance] (
    [EncounterRecno]    INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]    VARCHAR (255) NOT NULL,
    [SourcePatientNo]   VARCHAR (9)   NOT NULL,
    [SourceEncounterNo] VARCHAR (9)   NOT NULL,
    [ArrivalTime]       DATETIME2 (0) NULL,
    [DepartmentCode]    VARCHAR (8)   NULL,
    [HospitalCode]      VARCHAR (4)   NULL,
    [Deleted]           BIT           NULL,
    [Created]           DATETIME2 (0) CONSTRAINT [DF_PAS_AEAttendance_Created] DEFAULT (getdate()) NULL,
    [Updated]           DATETIME2 (0) CONSTRAINT [DF_PAS_AEAttendance_Updated] DEFAULT (getdate()) NULL,
    [ByWhom]            VARCHAR (50)  CONSTRAINT [DF_PAS_AEAttendance_ByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [PK_PAS_AEAttendance] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


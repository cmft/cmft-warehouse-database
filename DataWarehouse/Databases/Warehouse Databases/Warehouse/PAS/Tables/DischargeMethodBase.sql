﻿CREATE TABLE [PAS].[DischargeMethodBase] (
    [MODID]             VARCHAR (2)  NOT NULL,
    [Description]       VARCHAR (50) NULL,
    [FullDescription]   VARCHAR (50) NULL,
    [HaaCode]           VARCHAR (2)  NULL,
    [InternalValue]     VARCHAR (50) NULL,
    [MfRecStsInd]       VARCHAR (2)  NULL,
    [ScottishIntValue]  VARCHAR (2)  NULL,
    [MethodOfDischarge] VARCHAR (2)  NULL,
    CONSTRAINT [PK_DischargeMethod] PRIMARY KEY CLUSTERED ([MODID] ASC)
);


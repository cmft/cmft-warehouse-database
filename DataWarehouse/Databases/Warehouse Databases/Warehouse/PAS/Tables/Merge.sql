﻿CREATE TABLE [PAS].[Merge] (
    [ImpCode]         VARCHAR (4)  NULL,
    [Hospital]        VARCHAR (4)  NULL,
    [InternalNo]      VARCHAR (9)  NULL,
    [EpisodeNo]       VARCHAR (13) NULL,
    [MergeInternalNo] VARCHAR (9)  NULL,
    [MergeEpisodeNo]  VARCHAR (13) NULL
);


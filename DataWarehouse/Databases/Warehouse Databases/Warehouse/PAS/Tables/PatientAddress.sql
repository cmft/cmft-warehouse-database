﻿CREATE TABLE [PAS].[PatientAddress] (
    [SourcePatientNo]               VARCHAR (20) NOT NULL,
    [DistrictNo]                    VARCHAR (20) NULL,
    [AddressLine1]                  VARCHAR (20) NULL,
    [AddressLine2]                  VARCHAR (20) NULL,
    [AddressLine3]                  VARCHAR (20) NULL,
    [AddressLine4]                  VARCHAR (20) NULL,
    [Postcode]                      VARCHAR (10) NULL,
    [PreviousAddressSourceUniqueID] VARCHAR (20) NULL,
    [EffectiveFromDate]             DATE         NOT NULL,
    [EffectiveToDate]               DATE         NULL,
    [IsCurrent]                     BIT          NULL,
    CONSTRAINT [pk_PatientAddress] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [EffectiveFromDate] ASC)
);


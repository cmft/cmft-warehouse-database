﻿CREATE TABLE [PAS].[CasenoteLoan] (
    [SourceUniqueID]     VARCHAR (254) NOT NULL,
    [SourcePatientNo]    VARCHAR (20)  NOT NULL,
    [CasenoteNumber]     VARCHAR (20)  NOT NULL,
    [BorrowerCode]       VARCHAR (6)   NULL,
    [SequenceNo]         INT           NOT NULL,
    [TransactionTime]    SMALLDATETIME NULL,
    [LoanTime]           SMALLDATETIME NOT NULL,
    [ExpectedReturnDate] DATE          NULL,
    [ReturnTime]         SMALLDATETIME NULL,
    [Comment]            VARCHAR (30)  NULL,
    [LoanReason]         VARCHAR (50)  NULL,
    [UserId]             VARCHAR (3)   NULL,
    CONSTRAINT [PK_CasenoteLoan] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [CasenoteNumber] ASC, [SequenceNo] ASC, [LoanTime] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CasenoteLoan]
    ON [PAS].[CasenoteLoan]([SourcePatientNo] ASC, [CasenoteNumber] ASC, [SequenceNo] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_cnloan_cno]
    ON [PAS].[CasenoteLoan]([CasenoteNumber] ASC);


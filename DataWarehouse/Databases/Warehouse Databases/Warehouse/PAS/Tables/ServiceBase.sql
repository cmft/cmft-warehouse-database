﻿CREATE TABLE [PAS].[ServiceBase] (
    [HospitalCode]         SMALLINT     NOT NULL,
    [Service]              INT          NOT NULL,
    [Description]          VARCHAR (30) NULL,
    [DescriptionLabels]    VARCHAR (6)  NULL,
    [DescriptionShort]     VARCHAR (13) NULL,
    [CategoryOfService]    VARCHAR (12) NULL,
    [CategoryOfServiceInt] TINYINT      NULL,
    [ScreenType]           VARCHAR (4)  NULL,
    [Mnemonic]             VARCHAR (13) NULL,
    [Deleted]              BIT          NOT NULL,
    [OCMSERVICEID]         VARCHAR (42) NOT NULL,
    PRIMARY KEY CLUSTERED ([OCMSERVICEID] ASC)
);


﻿CREATE TABLE [PAS].[MaritalStatusBase] (
    [MARITALSID]       VARCHAR (1)  NOT NULL,
    [CancerRegMdsCode] VARCHAR (1)  NULL,
    [Description]      VARCHAR (10) NULL,
    [HaaCode]          VARCHAR (1)  NULL,
    [IntValue]         VARCHAR (1)  NULL,
    [MaritalStatus]    VARCHAR (1)  NOT NULL,
    [MfRecStsInd]      VARCHAR (2)  NULL,
    [ScottishIntValue] VARCHAR (1)  NULL,
    [StsIntCd]         VARCHAR (3)  NULL,
    CONSTRAINT [PK_MaritalStatusBase] PRIMARY KEY CLUSTERED ([MARITALSID] ASC)
);


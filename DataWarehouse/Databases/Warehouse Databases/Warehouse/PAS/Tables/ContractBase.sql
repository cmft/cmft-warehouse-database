﻿CREATE TABLE [PAS].[ContractBase] (
    [AgreementCDS]                           VARCHAR (6)   NULL,
    [CidGrp]                                 VARCHAR (1)   NULL,
    [CidType]                                VARCHAR (1)   NULL,
    [Cm2ContractMfSuitableToAutoAllocateInt] VARCHAR (1)   NULL,
    [ContractDesc]                           VARCHAR (30)  NULL,
    [ContractGroup]                          VARCHAR (4)   NULL,
    [ContractGroupCodeDescription]           VARCHAR (30)  NULL,
    [ContractId]                             VARCHAR (6)   NOT NULL,
    [ContractId_1]                           VARCHAR (16)  NULL,
    [ContractManagementEndDateInternal]      VARCHAR (8)   NULL,
    [ContractManagementStartDateInternal]    VARCHAR (8)   NULL,
    [Costsum]                                VARCHAR (10)  NULL,
    [Description]                            VARCHAR (30)  NULL,
    [Description_1]                          VARCHAR (30)  NULL,
    [EndDate]                                VARCHAR (10)  NULL,
    [InterimBilling]                         VARCHAR (3)   NULL,
    [IntrmBilling]                           VARCHAR (3)   NULL,
    [MfRecStsInd]                            VARCHAR (255) NULL,
    [OutpatientPrice1st]                     VARCHAR (7)   NULL,
    [PaymentTerms]                           VARCHAR (2)   NULL,
    [PriceProfile]                           VARCHAR (10)  NULL,
    [PriceProfileDescription]                VARCHAR (30)  NULL,
    [ProcedureProfileCode]                   VARCHAR (4)   NULL,
    [ProviderCode]                           VARCHAR (4)   NULL,
    [PurchaserCode]                          VARCHAR (5)   NULL,
    [Reatt]                                  VARCHAR (7)   NULL,
    [StartDate]                              VARCHAR (10)  NULL,
    [SuitableToAutoallocate]                 VARCHAR (3)   NULL,
    [Thresholdceiling]                       VARCHAR (4)   NULL,
    [Type]                                   VARCHAR (16)  NULL,
    [TypeOfCont]                             VARCHAR (2)   NULL,
    [WardAttPrice]                           VARCHAR (7)   NULL
);


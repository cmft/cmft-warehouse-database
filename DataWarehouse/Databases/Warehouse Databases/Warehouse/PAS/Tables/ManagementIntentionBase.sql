﻿CREATE TABLE [PAS].[ManagementIntentionBase] (
    [Description]      VARCHAR (20)  NULL,
    [HaaCode]          VARCHAR (1)   NULL,
    [IntdMgmt]         VARCHAR (1)   NOT NULL,
    [InternalValue]    VARCHAR (1)   NULL,
    [IntMgmtCode]      VARCHAR (3)   NULL,
    [MfRecStsInd]      VARCHAR (255) NULL,
    [ScottishIntValue] VARCHAR (1)   NULL,
    CONSTRAINT [PK_ManagementIntentionBase] PRIMARY KEY CLUSTERED ([IntdMgmt] ASC)
);


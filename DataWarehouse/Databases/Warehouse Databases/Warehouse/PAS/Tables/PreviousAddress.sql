﻿CREATE TABLE [PAS].[PreviousAddress] (
    [PREVADDRESSID]       VARCHAR (254) NULL,
    [DistrictNo]          VARCHAR (14)  NULL,
    [EffectiveFromDate]   DATETIME      NULL,
    [EffectiveToDate]     DATETIME      NULL,
    [HaCode]              VARCHAR (3)   NULL,
    [SourcePatientNo]     VARCHAR (9)   NULL,
    [Postcode]            VARCHAR (8)   NULL,
    [PseudoPostCode]      VARCHAR (8)   NULL,
    [PatientAddressLine1] VARCHAR (20)  NULL,
    [PatientAddressLine2] VARCHAR (20)  NULL,
    [PatientAddressLine3] VARCHAR (20)  NULL,
    [PatientAddressLine4] VARCHAR (20)  NULL,
    [SequenceNo]          VARCHAR (3)   NULL
);


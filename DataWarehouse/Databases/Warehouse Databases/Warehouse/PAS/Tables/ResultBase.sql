﻿CREATE TABLE [PAS].[ResultBase] (
    [ResultCd]          VARCHAR (4)  NOT NULL,
    [ResultDescription] VARCHAR (30) NULL,
    [ResultName]        VARCHAR (15) NULL,
    [ResultType]        VARCHAR (14) NULL,
    [UnitOfMeasurement] VARCHAR (8)  NULL,
    [Deleted]           BIT          NOT NULL,
    [OCMFRESULTSID]     VARCHAR (4)  NOT NULL,
    CONSTRAINT [PK__ResultBa__976913CF5BBA5238] PRIMARY KEY CLUSTERED ([ResultCd] ASC)
);


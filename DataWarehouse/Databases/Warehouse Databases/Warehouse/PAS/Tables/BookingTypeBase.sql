﻿CREATE TABLE [PAS].[BookingTypeBase] (
    [OPBOOKTYPEMFID]                    VARCHAR (254) NOT NULL,
    [Code]                              VARCHAR (4)   NOT NULL,
    [Description]                       VARCHAR (50)  NULL,
    [InternalValue]                     VARCHAR (2)   NULL,
    [InternalValueDescription]          VARCHAR (255) NULL,
    [MfRecStsInd]                       VARCHAR (255) NULL,
    [SchedulingApplicationAreaExternal] VARCHAR (6)   NOT NULL,
    CONSTRAINT [PK_BookingTypeBase] PRIMARY KEY CLUSTERED ([OPBOOKTYPEMFID] ASC)
);


﻿CREATE TABLE [PAS].[EthnicOriginBase] (
    [EthnicOriginCode]        VARCHAR (4)   NOT NULL,
    [EthnicOrigin]            VARCHAR (30)  NULL,
    [EthnicGroupNationalCode] VARCHAR (2)   NULL,
    [EthnicityNationalCode]   VARCHAR (2)   NULL,
    [EthnicityNational]       VARCHAR (40)  NULL,
    [MfRecStsInd]             VARCHAR (255) NULL,
    CONSTRAINT [PK_EthnicOriginBase] PRIMARY KEY CLUSTERED ([EthnicOriginCode] ASC)
);


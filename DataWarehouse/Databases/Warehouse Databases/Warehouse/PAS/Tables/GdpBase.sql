﻿CREATE TABLE [PAS].[GdpBase] (
    [GDPID]            VARCHAR (6)   NOT NULL,
    [Address]          VARCHAR (20)  NULL,
    [DpbContractCode]  VARCHAR (8)   NULL,
    [GdpAddLine2]      VARCHAR (20)  NULL,
    [GdpAddrLine3]     VARCHAR (20)  NULL,
    [GdpAddrLine4]     VARCHAR (20)  NULL,
    [GdpAddrOnOneLine] VARCHAR (70)  NULL,
    [GdpCode]          VARCHAR (6)   NOT NULL,
    [Initials]         VARCHAR (5)   NULL,
    [MfRecStsInd]      VARCHAR (255) NULL,
    [Name]             VARCHAR (35)  NULL,
    [Phone]            VARCHAR (23)  NULL,
    [Postcode]         VARCHAR (10)  NULL,
    [Surname]          VARCHAR (24)  NULL,
    [Title]            VARCHAR (4)   NULL
);


﻿CREATE TABLE [PAS].[WaitingListBase] (
    [CurrentlySequencedBy] VARCHAR (40)  NULL,
    [ElAdmListNo]          VARCHAR (3)   NULL,
    [HospitalCode]         VARCHAR (4)   NULL,
    [MainConsultant]       VARCHAR (6)   NULL,
    [MfRecStsInd]          VARCHAR (255) NULL,
    [ProviderCode]         VARCHAR (4)   NULL,
    [RepeatList]           VARCHAR (3)   NULL,
    [RepeatList_1]         VARCHAR (1)   NULL,
    [SequencedBy]          VARCHAR (40)  NULL,
    [SpecOfList]           VARCHAR (4)   NULL,
    [WaitingList]          VARCHAR (6)   NOT NULL,
    [WlDescription]        VARCHAR (30)  NULL,
    [WlMaxAdmTimed]        VARCHAR (3)   NULL,
    [WlMaxAdmTimeo]        VARCHAR (3)   NULL,
    [WlMfCurrentSortInt]   VARCHAR (8)   NULL,
    [WlMfRequestedSortInt] VARCHAR (8)   NULL
);


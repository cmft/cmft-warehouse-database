﻿CREATE TABLE [PAS].[PatientSpecialRegister] (
    [SpecialRegisterRecNo] INT           IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]      INT           NOT NULL,
    [SpecialRegisterCode]  VARCHAR (4)   NOT NULL,
    [DistrictNo]           VARCHAR (20)  NULL,
    [NHSNumber]            VARCHAR (17)  NULL,
    [EnteredDate]          DATE          NULL,
    [ActivityTime]         DATETIME      NULL,
    [SourceEpisodeNo]      VARCHAR (20)  NULL,
    [Created]              DATETIME      CONSTRAINT [df_PAS_PatientSpecialRegister_CreatedTime] DEFAULT (getdate()) NULL,
    [CreatedByWhom]        VARCHAR (255) CONSTRAINT [df_PAS_PatientSpecialRegister_CreatedByWhom] DEFAULT (suser_name()) NULL,
    [Updated]              DATETIME      CONSTRAINT [df_PAS_PatientSpecialRegister_UpdatedTime] DEFAULT (getdate()) NULL,
    [UpdatedByWhom]        VARCHAR (255) CONSTRAINT [df_PAS_PatientSpecialRegister_UpdatedByWhom] DEFAULT (suser_name()) NULL,
    CONSTRAINT [pk_PAS_PatientSpecialRegister] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [SpecialRegisterCode] ASC)
);


﻿CREATE TABLE [PAS].[Birth] (
    [BirthID]            VARCHAR (50)  NULL,
    [BabyPatientNo]      VARCHAR (9)   NULL,
    [BirthOrder]         VARCHAR (1)   NOT NULL,
    [BirthWeight]        VARCHAR (4)   NULL,
    [CasenoteNumber]     VARCHAR (14)  NULL,
    [Comments]           VARCHAR (30)  NULL,
    [DeliveryDate]       SMALLDATETIME NULL,
    [DeliveryTime]       SMALLDATETIME NULL,
    [DrugsUsed]          VARCHAR (30)  NULL,
    [SourceSpellNo]      VARCHAR (12)  NOT NULL,
    [SourcePatientNo]    VARCHAR (12)  NOT NULL,
    [LiveStillBirth]     VARCHAR (1)   NULL,
    [DeliveryMethodCode] VARCHAR (2)   NULL,
    CONSTRAINT [PK_Birth] PRIMARY KEY CLUSTERED ([BirthOrder] ASC, [SourceSpellNo] ASC, [SourcePatientNo] ASC)
);


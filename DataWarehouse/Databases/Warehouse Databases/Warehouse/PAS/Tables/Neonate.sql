﻿CREATE TABLE [PAS].[Neonate] (
    [NeonateID]            VARCHAR (50)  NULL,
    [ApgarScoreAt1Minute]  VARCHAR (2)   NULL,
    [ApgarScoreAt5Minutes] VARCHAR (2)   NULL,
    [BirthOrder]           VARCHAR (1)   NOT NULL,
    [DiagnosisCode1]       VARCHAR (8)   NULL,
    [DiagnosisCode2]       VARCHAR (6)   NULL,
    [SourceSpellNo]        VARCHAR (255) NOT NULL,
    [HeadCircumference]    VARCHAR (2)   NULL,
    [SourcePatientNo]      VARCHAR (255) NOT NULL,
    [BCGAdministered]      VARCHAR (1)   NULL,
    [Feeding]              VARCHAR (1)   NULL,
    [FollowUpCare]         VARCHAR (1)   NULL,
    [HipsExamination]      VARCHAR (1)   NULL,
    [Jaundice]             VARCHAR (1)   NULL,
    [MetabolicScreening]   VARCHAR (1)   NULL,
    [LengthAtBirth]        VARCHAR (2)   NULL,
    [GestationLength]      VARCHAR (2)   NULL,
    CONSTRAINT [PK_Neonate] PRIMARY KEY CLUSTERED ([BirthOrder] ASC, [SourceSpellNo] ASC, [SourcePatientNo] ASC)
);


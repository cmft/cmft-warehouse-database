﻿CREATE TABLE [PAS].[UserBase] (
    [GpUserIdAccountDeletionFlag] VARCHAR (255) NULL,
    [GpxgpUsername]               VARCHAR (15)  NULL,
    [UserId]                      VARCHAR (3)   NOT NULL,
    [UserIdName]                  VARCHAR (255) NULL,
    [UsersDepartment]             VARCHAR (8)   NULL,
    [UsersHospital]               VARCHAR (4)   NULL,
    [UserGroupCode]               VARCHAR (10)  NULL
);


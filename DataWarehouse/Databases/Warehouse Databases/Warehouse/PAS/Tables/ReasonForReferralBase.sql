﻿CREATE TABLE [PAS].[ReasonForReferralBase] (
    [OPREFREASONID]                              VARCHAR (4)  NOT NULL,
    [AppointmentWithinTargetDays]                VARCHAR (3)  NULL,
    [AvailableToGpLinks]                         VARCHAR (3)  NULL,
    [CollectDecisionToReferDate]                 VARCHAR (3)  NULL,
    [Description]                                VARCHAR (30) NULL,
    [DohReasonForReferral]                       VARCHAR (1)  NULL,
    [MfRecStsInd]                                VARCHAR (1)  NULL,
    [PriorityDescription]                        VARCHAR (11) NULL,
    [ReasonForRefCode]                           VARCHAR (4)  NOT NULL,
    [ReferrerPriority]                           VARCHAR (1)  NULL,
    [SchGpLinksReasonForReferralCodeAvailInt]    VARCHAR (1)  NULL,
    [SchReasonForReferralCollectDecisionDateInt] VARCHAR (1)  NULL,
    [SchReasonForReferralIncludeForKarsInt]      VARCHAR (1)  NULL,
    [ScottishIntValue]                           VARCHAR (1)  NULL,
    [SuspCancerDesc]                             VARCHAR (50) NULL,
    [SuspectedCancerCode]                        VARCHAR (6)  NULL,
    [TargetMaximumWaitdays]                      VARCHAR (3)  NULL,
    CONSTRAINT [PK_ReferralReasonBase] PRIMARY KEY CLUSTERED ([OPREFREASONID] ASC)
);


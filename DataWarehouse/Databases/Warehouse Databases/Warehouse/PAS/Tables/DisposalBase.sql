﻿CREATE TABLE [PAS].[DisposalBase] (
    [DISPID]           VARCHAR (4)  NOT NULL,
    [Applications]     VARCHAR (7)  NULL,
    [Description]      VARCHAR (30) NULL,
    [DisposalCode]     VARCHAR (4)  NOT NULL,
    [Doh]              VARCHAR (2)  NULL,
    [MfRecStsInd]      VARCHAR (2)  NULL,
    [ScottishIntValue] VARCHAR (1)  NULL,
    CONSTRAINT [PK_DisposalBase] PRIMARY KEY CLUSTERED ([DisposalCode] ASC)
);


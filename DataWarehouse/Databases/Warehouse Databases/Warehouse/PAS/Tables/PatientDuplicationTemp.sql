﻿CREATE TABLE [PAS].[PatientDuplicationTemp] (
    [PatientDuplicationTempId] INT             IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]          INT             NULL,
    [HashKey]                  VARBINARY (900) NULL,
    [IsMaster]                 BIT             NULL,
    [MasterScore]              INT             NULL,
    [RegistrationDate]         DATE            NULL,
    CONSTRAINT [PK_PatientDuplication] PRIMARY KEY CLUSTERED ([PatientDuplicationTempId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_PatientDuplication_Checksum]
    ON [PAS].[PatientDuplicationTemp]([HashKey] ASC);


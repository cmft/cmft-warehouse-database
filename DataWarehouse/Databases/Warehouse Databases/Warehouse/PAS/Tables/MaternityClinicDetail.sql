﻿CREATE TABLE [PAS].[MaternityClinicDetail] (
    [MaternityClinicDetailID]               VARCHAR (50)  NULL,
    [SourceSpellNo]                         VARCHAR (255) NOT NULL,
    [SourcePatientNo]                       VARCHAR (255) NOT NULL,
    [PatientKnownToBeImmune]                VARCHAR (3)   NULL,
    [RubellaAntibodyImmune]                 VARCHAR (1)   NULL,
    [StatusOfPersonConductingDeliveryCode]  VARCHAR (12)  NULL,
    [MotherHeight]                          VARCHAR (3)   NULL,
    [NumberOfPreviousNeonatalDeaths]        VARCHAR (2)   NULL,
    [NumberOfPreviousNonInducedAbortions]   VARCHAR (2)   NULL,
    [NumberOfPreviousCaesarians]            VARCHAR (2)   NULL,
    [NumberOfPreviousInducedAbortions]      VARCHAR (2)   NULL,
    [NumberOfPreviousRegisteredLiveBirths]  VARCHAR (2)   NULL,
    [NumberOfPreviousRegisteredStillBirths] VARCHAR (2)   NULL,
    [NumberOfPreviousTransfusions]          VARCHAR (3)   NULL,
    [AntibodyStatusTested]                  VARCHAR (3)   NULL,
    [AntibodyStatusTestedPositive]          VARCHAR (3)   NULL,
    CONSTRAINT [PK_MaternityClinicDetail] PRIMARY KEY CLUSTERED ([SourceSpellNo] ASC, [SourcePatientNo] ASC)
);


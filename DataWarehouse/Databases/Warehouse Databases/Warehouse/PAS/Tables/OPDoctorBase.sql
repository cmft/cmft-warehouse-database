﻿CREATE TABLE [PAS].[OPDoctorBase] (
    [OPDOCTORID]       VARCHAR (8)  NOT NULL,
    [AllowOverbooking] VARCHAR (1)  NULL,
    [ClinicHospCode]   VARCHAR (4)  NULL,
    [Comment]          VARCHAR (20) NULL,
    [GmcGdcHcp]        VARCHAR (8)  NULL,
    [GradeOfStaff]     VARCHAR (4)  NULL,
    [LocCode]          VARCHAR (4)  NULL,
    [MfRecStsInd]      VARCHAR (2)  NULL,
    [Name]             VARCHAR (20) NULL,
    [SchDays]          VARCHAR (4)  NULL,
    [SchedResMfCd]     VARCHAR (8)  NOT NULL,
    [ServiceGroup]     VARCHAR (6)  NULL,
    [TypeOfDoctor]     VARCHAR (5)  NULL,
    [ValidApptTypes]   VARCHAR (39) NULL,
    CONSTRAINT [PK_OPDoctorBase] PRIMARY KEY CLUSTERED ([OPDOCTORID] ASC)
);


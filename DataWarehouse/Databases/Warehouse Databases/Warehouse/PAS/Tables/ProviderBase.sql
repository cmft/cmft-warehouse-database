﻿CREATE TABLE [PAS].[ProviderBase] (
    [PROVID]                                      VARCHAR (4)   NOT NULL,
    [CmmCommissioningMainCommissionerDescription] VARCHAR (30)  NULL,
    [CmmCommissioningMainCommissionerPcgDesc]     VARCHAR (30)  NULL,
    [CmmTypeOfProviderDescription]                VARCHAR (30)  NULL,
    [Description]                                 VARCHAR (30)  NULL,
    [DohCode]                                     VARCHAR (5)   NULL,
    [MainCommissionerHa]                          VARCHAR (5)   NULL,
    [MainCommissionerPcg]                         VARCHAR (5)   NULL,
    [MfRecStsInd]                                 VARCHAR (255) NULL,
    [OrganisationCode]                            VARCHAR (3)   NULL,
    [ProvType]                                    VARCHAR (2)   NULL,
    [ProviderCode]                                VARCHAR (4)   NOT NULL
);


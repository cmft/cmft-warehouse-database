﻿CREATE TABLE [PAS].[SourceOfReferralBase] (
    [OPREFSOURCEID]                           VARCHAR (3)  NOT NULL,
    [Applications]                            VARCHAR (17) NULL,
    [CmdsValue]                               VARCHAR (2)  NULL,
    [CollectGpRefStatsInternal]               VARCHAR (1)  NULL,
    [CollectGpReferralStatisticsWithThisCode] VARCHAR (3)  NULL,
    [Description]                             VARCHAR (30) NULL,
    [InternalValue]                           VARCHAR (1)  NULL,
    [MfRecStsInd]                             SMALLINT     NULL,
    [Modules]                                 VARCHAR (3)  NULL,
    [ReferredBy]                              VARCHAR (3)  NOT NULL,
    [ScottishIntValue]                        VARCHAR (1)  NULL,
    [ReportableFlag]                          BIT          NULL,
    [NewFlag]                                 BIT          NULL,
    CONSTRAINT [PK_PASSourceOfReferralBase] PRIMARY KEY CLUSTERED ([OPREFSOURCEID] ASC)
);


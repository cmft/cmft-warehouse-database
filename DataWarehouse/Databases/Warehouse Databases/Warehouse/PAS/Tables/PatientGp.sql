CREATE TABLE [PAS].[PatientGp] (
    [PatientGpRecno]          INT          IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]         INT          NOT NULL,
    [DistrictNo]              VARCHAR (20) NULL,
    [GpPracticeCode]          VARCHAR (6)  NULL,
    [GpCode]                  VARCHAR (8)  NULL,
    [GpSourceUniqueID]        VARCHAR (20) NULL,
    [GpHistorySourceUniqueID] VARCHAR (20) NULL,
    [EffectiveFromDate]       DATE         NOT NULL,
    [EffectiveToDate]         DATE         NULL,
    [IsCurrent]               BIT          NULL,
    CONSTRAINT [PK_PatientGpPractice] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [EffectiveFromDate] ASC)
);




GO



GO
CREATE NONCLUSTERED INDEX [IX_PatientGp_EffectiveToDate_Include_SourcePatientNo]
    ON [PAS].[PatientGp]([EffectiveToDate] ASC)
    INCLUDE([SourcePatientNo]);


GO
CREATE NONCLUSTERED INDEX [IX_PatientGp_EffectiveFromDate]
    ON [PAS].[PatientGp]([EffectiveFromDate] ASC)
    INCLUDE([SourcePatientNo]);


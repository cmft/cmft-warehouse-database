﻿CREATE TABLE [PAS].[Casenote] (
    [SourcePatientNo]      VARCHAR (20)  NOT NULL,
    [CasenoteNumber]       VARCHAR (20)  NOT NULL,
    [AllocatedDate]        DATE          NULL,
    [CasenoteLocationCode] VARCHAR (4)   NULL,
    [CasenoteLocationDate] DATE          NULL,
    [CasenoteStatus]       VARCHAR (10)  NULL,
    [WithdrawnDate]        DATE          NULL,
    [Comment]              VARCHAR (50)  NULL,
    [CurrentBorrowerCode]  VARCHAR (50)  NULL,
    [LoanTime]             SMALLDATETIME NULL,
    [ExpectedReturnDate]   DATE          NULL,
    CONSTRAINT [PK_Casenote] PRIMARY KEY CLUSTERED ([SourcePatientNo] ASC, [CasenoteNumber] ASC)
);


GO
CREATE NONCLUSTERED INDEX [INDEX_Casenote_AllocatedDateStatus]
    ON [PAS].[Casenote]([AllocatedDate] ASC, [CasenoteStatus] ASC)
    INCLUDE([SourcePatientNo]);


GO
CREATE NONCLUSTERED INDEX [IX_Casenote]
    ON [PAS].[Casenote]([SourcePatientNo] ASC, [CasenoteNumber] ASC)
    INCLUDE([CasenoteStatus]);


GO
CREATE NONCLUSTERED INDEX [Idx_Casenote_CasenoteNumber]
    ON [PAS].[Casenote]([CasenoteNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CasenoteNumber_SourcePatientNo]
    ON [PAS].[Casenote]([SourcePatientNo] ASC)
    INCLUDE([CasenoteNumber]);


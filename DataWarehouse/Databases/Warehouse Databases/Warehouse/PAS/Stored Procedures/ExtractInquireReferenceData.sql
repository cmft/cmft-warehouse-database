﻿CREATE procedure [PAS].[ExtractInquireReferenceData] as


declare @StartTime datetime
declare @Elapsed int
declare @Stats varchar(255)

select @StartTime = getdate()


set dateformat dmy


--Site
insert into PAS.Site
(
	 SiteCode
	,Site
	,MappedSiteCode
)
select
	 SiteCode
	,Site
	,MappedSiteCode = '#'
from
	openquery(INQUIRE, '
select
	 HOSPDETID SiteCode
	,HospitalName Site
from
	HOSPDET
') NewSite
where
	not exists
	(
	select
		1
	from
		PAS.Site
	where
		Site.SiteCode = NewSite.SiteCode collate latin1_general_bin
	)

--Gp
truncate table PAS.GpBase

insert into PAS.GpBase
(
	GPID, Address, DateJoined, DateJoinedPractice, DateLeft, DateLeftPractice, DohCode, Fax, GeneralPractitionerDateJoinedFhSchemeInt, GeneralPractitionerDateJoinedGpPracticeInter, GeneralPractitionerDateLeftFhSchemeInternal, GeneralPractitionerDateLeftGpPracticeInterna, GpAddrLine2, GpAddrLine3, GpAddrLine4, GpAddrOnOneLine, GpCode, GpName, GpName_1, GpName_2, GpfhCode, HaaCode, Initials, MfRecStsInd, NationalCode, PathologyGpCode, Phone, Postcode, PracticeCode, Surname, Title
)
select
	GPID, Address, DateJoined, DateJoinedPractice, DateLeft, DateLeftPractice, DohCode, Fax, GeneralPractitionerDateJoinedFhSchemeInt, GeneralPractitionerDateJoinedGpPracticeInter, GeneralPractitionerDateLeftFhSchemeInternal, GeneralPractitionerDateLeftGpPracticeInterna, GpAddrLine2, GpAddrLine3, GpAddrLine4, GpAddrOnOneLine, GpCode, GpName, GpName_1, GpName_2, GpfhCode, HaaCode, Initials, ltrim(rtrim(MfRecStsInd)), NationalCode, PathologyGpCode, Phone, Postcode, PracticeCode, Surname, Title
from
	openquery(INQUIRE,'

select
	GPID, left(Address, 25) Address
	,DateJoined
	,DateJoinedPractice
	,DateLeft
	,DateLeftPractice
	,DohCode
	,Fax
	,GeneralPractitionerDateJoinedFhSchemeInt
	,GeneralPractitionerDateJoinedGpPracticeInter
	,GeneralPractitionerDateLeftFhSchemeInternal
	,GeneralPractitionerDateLeftGpPracticeInterna
	,left(GpAddrLine2, 25) GpAddrLine2
	,left(GpAddrLine3, 25) GpAddrLine3
	,left(GpAddrLine4, 25) GpAddrLine4
	,GpAddrOnOneLine, GpCode, GpName, GpName_1, GpName_2, GpfhCode, HaaCode, Initials, left(MfRecStsInd, 2) MfRecStsInd, NationalCode, PathologyGpCode, Phone, Postcode, PracticeCode, Surname, Title
from
	GP
'
)

--Specialty
truncate table PAS.SpecialtyBase

insert into PAS.SpecialtyBase
(
	SpecialtyCode, CmtGpfhExemptSpecialtyInt, Description, DohCode, GpfhExempt, HaaCode, KarsIdCode, MfRecStsInd, ScottishIntValue, ScottishLocalCode, SpecGroup, Specialty, SpecialtyType, SupraregCode, TreatmentFunction
)
select
	SpecialtyCode, CmtGpfhExemptSpecialtyInt, Description, DohCode, GpfhExempt, HaaCode, KarsIdCode, MfRecStsInd, ScottishIntValue, ScottishLocalCode, SpecGroup, Specialty, SpecialtyType, SupraregCode, TreatmentFunction
from
	openquery(INQUIRE, '

select
	SPECID SpecialtyCode, CmtGpfhExemptSpecialtyInt, Description, left(DohCode, 4) DohCode, GpfhExempt, HaaCode, KarsIdCode, left(MfRecStsInd, 2) MfRecStsInd, ScottishIntValue, ScottishLocalCode, SpecGroup, Specialty, SpecialtyType, SupraregCode, TreatmentFunction
from
	SPEC

')


--add any new specialties to the national Specialty reference table
--insert into WH.Specialty
--(
--	 SpecialtyCode
--	,Specialty
--	,DirectorateCode
--)
--select distinct
--	 PASPennine.NationalSpecialtyCode
--	,max(PASPennine.Specialty)
--	,DirectorateCode = '9' --unknown
--from
--	PAS.Specialty PASPennine
--where
--	not exists
--	(
--	select
--		1
--	from
--		WH.Specialty Specialty
--	where
--		Specialty.SpecialtyCode = PASPennine.NationalSpecialtyCode
--	)
--and	PASPennine.NationalSpecialtyCode is not null
--group by
--	PASPennine.NationalSpecialtyCode


----add any new specialties to the local SpecialtyPennine reference table
--insert into WH.SpecialtyPennine
--(
--	 SpecialtyCode
--	,Specialty
--	,NationalSpecialtyCode
--)
--select
--	 SpecialtyCode
--	,Specialty
--	,NationalSpecialtyCode
--from
--	PAS.Specialty
--where
--	not exists
--	(
--	select
--		1
--	from
--		WH.SpecialtyPennine
--	where
--		SpecialtyPennine.SpecialtyCode = Specialty.SpecialtyCode
--	)


--Consultant
truncate table PAS.ConsultantBase

insert into PAS.ConsultantBase
(
	CONSMASTID, Consultant, ConsultantCode2, ContractedSpecialty, Deleted, ExternalCons, ForenamePfx, GMCCode, Inits, KornerCode, Midwife, Name, PrimarySpecialty, ProviderCode, Surname
)
select
	CONSMASTID, Consultant, ConsultantCode2, ContractedSpecialty, ltrim(rtrim(Deleted)), ExternalCons, ForenamePfx, GMCCode, Inits, KornerCode, Midwife, Name, PrimarySpecialty, ProviderCode, Surname
from
	openquery(INQUIRE, '

select
	CONSMASTID, Consultant, ConsultantCode2, ContractedSpecialty, left(Deleted, 2) Deleted, ExternalCons, left(ForenamePfx, 4) ForenamePfx, GMCCode, Inits, KornerCode, Midwife, Name, PrimarySpecialty, ProviderCode, Surname
from
	CONSMAST

')


--add any new consultants to the national Consultant reference table
insert into WH.Consultant
(
	 ConsultantCode
	,Title
	,Forename
	,Surname
)
select
	 PAS.NationalConsultantCode
	,PAS.Title
	,PAS.Initials
	,PAS.Surname
from
	PAS.Consultant PAS
where
	not exists
	(
	select
		1
	from
		WH.Consultant Consultant
	where
		Consultant.ConsultantCode = PAS.NationalConsultantCode
	)
and	PAS.NationalConsultantCode is not null
and	PAS.Deleted = 0
and	PAS.Surname not like '%DO NOT USE%'

and	not exists
	(
	select
		1
	from
		PAS.Consultant Consultant
	where
		Consultant.NationalConsultantCode = PAS.NationalConsultantCode
	and	Consultant.NationalConsultantCode is not null
	and	Consultant.Deleted = 0
	and	Consultant.Surname not like '%DO NOT USE%'
	and	Consultant.ConsultantCode > PAS.ConsultantCode
	)

--SourceOfReferral
truncate table PAS.SourceOfReferralBase

insert into PAS.SourceOfReferralBase
(
	OPREFSOURCEID, Applications, CmdsValue, CollectGpRefStatsInternal, CollectGpReferralStatisticsWithThisCode, Description, InternalValue, MfRecStsInd, Modules, ReferredBy, ScottishIntValue
	,ReportableFlag
	,NewFlag
)
select
	OPREFSOURCEID, Applications, CmdsValue, CollectGpRefStatsInternal, CollectGpReferralStatisticsWithThisCode, SourceOfReferralBase.Description, InternalValue, ltrim(rtrim(MfRecStsInd)), Modules, ReferredBy, ScottishIntValue
	,case when ExcludedSourceOfReferral.[Description] is null then 1 else 0 end ReportableFlag
	,case when NewSourceOfReferral.[Description] is null then 0 else 1 end NewFlag
from
	openquery(INQUIRE,'

select
	OPREFSOURCEID
	,Applications
	,CmdsValue
	,CollectGpRefStatsInternal
	,CollectGpReferralStatisticsWithThisCode
	,Description
	,InternalValue
	,left(MfRecStsInd, 2) MfRecStsInd
	,left(Modules, 3) Modules
	,ReferredBy
	,ScottishIntValue
from
	OPREFSOURCE
'
) SourceOfReferralBase

left join dbo.EntityLookup ExcludedSourceOfReferral
on	ExcludedSourceOfReferral.EntityCode = SourceOfReferralBase.OPREFSOURCEID collate latin1_general_bin
and	ExcludedSourceOfReferral.EntityTypeCode = 'EXCLUDEDSOURCEOFREFERRAL'

left join dbo.EntityLookup NewSourceOfReferral
on	NewSourceOfReferral.EntityCode = SourceOfReferralBase.OPREFSOURCEID collate latin1_general_bin
and	NewSourceOfReferral.EntityTypeCode = 'NEWSOURCEOFREFERRAL'


--Admission Source
insert into
	PAS.AdmissionSourceBase
(
	 SOADID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,SourceOfAdm
)
select
	 SOADID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,SourceOfAdm
from
	openquery(INQUIRE, '

select
	 SOADID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,left(MfRecStsInd, 2) MfRecStsInd
	,ScottishIntValue
	,SourceOfAdm
from
	SOAD
') NewAdmissionSourceBase
where
	not exists
	(
	select
		1
	from
		PAS.AdmissionSourceBase
	where
		AdmissionSourceBase.SOADID = NewAdmissionSourceBase.SOADID collate latin1_general_bin
	)

--Admission Method
insert into
	PAS.AdmissionMethodBase
(
	 MOAID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfAdmission
)
select
	 MOAID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfAdmission
from
	openquery(INQUIRE, '

select
	 MOAID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,left(MfRecStsInd, 2) MfRecStsInd
	,ScottishIntValue
	,MethodOfAdmission
from
	MOA
') NewAdmissionMethodBase
where
	not exists
	(
	select
		1
	from
		PAS.AdmissionMethodBase
	where
		AdmissionMethodBase.MOAID = NewAdmissionMethodBase.MOAID collate latin1_general_bin
	)

--Discharge Destination
insert into
	PAS.DischargeDestinationBase
(
	 DODID
	,Description
	,FullDescription
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,DestinationCode
)
select
	 DODID
	,Description
	,FullDescription
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,DestinationCode
from
	openquery(INQUIRE, '

select
	 DODID
	,Description
	,FullDescription
	,InternalValue
	,left(MfRecStsInd, 2) MfRecStsInd
	,ScottishIntValue
	,DestinationCode
from
	DOD
') NewDischargeDestinationBase
where
	not exists
	(
	select
		1
	from
		PAS.DischargeDestinationBase DischargeDestinationBase
	where
		DischargeDestinationBase.DODID = NewDischargeDestinationBase.DODID collate latin1_general_bin
	)


--Discharge Method
insert into
	PAS.DischargeMethodBase
(
	 MODID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfDischarge
)
select
	 MODID
	,Description
	,FullDescription
	,HaaCode = case when HaaCode = 7 then 4 else coalesce(HaaCode, InternalValue) end
	,InternalValue
	,MfRecStsInd
	,ScottishIntValue
	,MethodOfDischarge
from
	openquery(INQUIRE, '

select
	 MODID
	,Description
	,FullDescription
	,HaaCode
	,InternalValue
	,left(MfRecStsInd, 2) MfRecStsInd
	,ScottishIntValue
	,MethodOfDischarge
from
	MOD
') NewDischargeMethodBase
where
	not exists
	(
	select
		1
	from
		PAS.DischargeMethodBase DischargeMethodBase
	where
		DischargeMethodBase.MODID = NewDischargeMethodBase.MODID collate latin1_general_bin
	)



--PAS.OperationBase
truncate table PAS.OperationBase

insert into PAS.OperationBase
	(
	 OPCS4ID
	,Opcs4Dx3
	,Opcs4Dx4
	,Opcs4GpfhChargeable
	,Opcs4GpfhEffDate
	,Opcs4Groupcode
	,Opcs4Ldf
	,Opcs4Procpricegroup
	,RevisionFourOperationCode
	)
select
	 OPCS4ID
	,Opcs4Dx3
	,Opcs4Dx4
	,Opcs4GpfhChargeable
	,Opcs4GpfhEffDate
	,Opcs4Groupcode
	,Opcs4Ldf
	,Opcs4Procpricegroup
	,RevisionFourOperationCode
from
	openquery(INQUIRE,'

select
	 OPCS4ID
	,Opcs4Dx3
	,Opcs4Dx4
	,Opcs4GpfhChargeable
	,Opcs4GpfhEffDate
	,Opcs4Groupcode
	,left(Opcs4Ldf, 2) Opcs4Ldf
	,Opcs4Procpricegroup
	,RevisionFourOperationCode
from
	OPCS4
')


--PAS.DiagnosisBase
truncate table PAS.DiagnosisBase

insert into PAS.DiagnosisBase
(
	 ICD10ID
	,A5thDigitLoMax
	,A5thDigitUpMin
	,AccidentFlag
	,AccidentPrompt
	,DaggerAsterisk
	,Deleted
	,DeliveryMethod
	,Description
	,EpisodeCompleteCheck
	,FourDigitName
	,Icd10Code
	,MaxAge
	,MaxBirthWeight
	,MaxDurationStay
	,MethodAdmissionFlag
	,MethodOfOnset
	,MinAge
	,MinBirthWeight
	,NumberOfBabies
	,Parity
	,PrimCondFlag
	,PrimCondPrompt
	,RareDeathFlag
	,RareDeathPrompt
	,RareDiagFlag
	,RareDiagPrompt
	,SexValidationFlag
	,SpecialtyFunction
	,StillBirthInd
	,StillBirthIndPrompt
	,ThreeDigitName
	,ValidForCancerReg
	,ValidForCancerRegExt
)
select
	 ICD10ID
	,A5thDigitLoMax
	,A5thDigitUpMin
	,AccidentFlag
	,AccidentPrompt
	,DaggerAsterisk
	,Deleted
	,DeliveryMethod
	,Description
	,EpisodeCompleteCheck
	,FourDigitName
	,Icd10Code
	,MaxAge
	,MaxBirthWeight
	,MaxDurationStay
	,MethodAdmissionFlag
	,MethodOfOnset
	,MinAge
	,MinBirthWeight
	,NumberOfBabies
	,Parity
	,PrimCondFlag
	,PrimCondPrompt
	,RareDeathFlag
	,RareDeathPrompt
	,RareDiagFlag
	,RareDiagPrompt
	,SexValidationFlag
	,SpecialtyFunction
	,StillBirthInd
	,StillBirthIndPrompt
	,ThreeDigitName
	,ValidForCancerReg
	,ValidForCancerRegExt
from
	openquery(INQUIRE,'

select
	 ICD10ID
	,A5thDigitLoMax
	,A5thDigitUpMin
	,AccidentFlag
	,AccidentPrompt
	,DaggerAsterisk
	,Deleted
	,DeliveryMethod
	,Description
	,EpisodeCompleteCheck
	,left(FourDigitName, 255) FourDigitName
	,Icd10Code
	,MaxAge
	,MaxBirthWeight
	,MaxDurationStay
	,MethodAdmissionFlag
	,MethodOfOnset
	,MinAge
	,MinBirthWeight
	,NumberOfBabies
	,Parity
	,PrimCondFlag
	,PrimCondPrompt
	,RareDeathFlag
	,RareDeathPrompt
	,RareDiagFlag
	,RareDiagPrompt
	,SexValidationFlag
	,SpecialtyFunction
	,StillBirthInd
	,StillBirthIndPrompt
	,ThreeDigitName
	,ValidForCancerReg
	,ValidForCancerRegExt
from
	ICD10
')


--BookingTypeBase
truncate table PAS.BookingTypeBase

insert into PAS.BookingTypeBase
(
	 OPBOOKTYPEMFID
	,Code
	,Description
	,InternalValue
	,InternalValueDescription
	,MfRecStsInd
	,SchedulingApplicationAreaExternal
)
select
	 OPBOOKTYPEMFID
	,Code
	,Description
	,InternalValue
	,InternalValueDescription
	,MfRecStsInd
	,SchedulingApplicationAreaExternal
from
	openquery(INQUIRE, '

select
	 OPBOOKTYPEMFID
	,Code
	,Description
	,InternalValue
	,left(InternalValueDescription, 41) InternalValueDescription
	,left(MfRecStsInd, 1) MfRecStsInd
	,SchedulingApplicationAreaExternal
from
	OPBOOKTYPEMF
')


--Clinic

truncate table PAS.ClinicBase

insert into PAS.ClinicBase
	(
	 OUTCLNCID
	,AppTypesAvlblGpBook
	,AppTypesAvlblGpInt
	,Comment
	,ConsCode
	,Description
	,DestForGpAppLetters
	,DestinationHospCd
	,FunctionCode
	,GpAduAppLtrDocument
	,GpAppConfirmPara
	,GpBroMaxNoDaysFuture
	,GpChiAppLtrDocument
	,GpLnkDftBookTypeDesc
	,GpReasonRefrlText
	,GpRefrlCriteriaPara
	,GplClinMfRtnAppTypes
	,GplClinMfUrgAppTypes
	,GplClinicMfUrgent
	,GplClinicMfUrgentInt
	,GplinkDftBookType
	,MaxAgeThatMeansChild
	,MfRecStsInd
	,ProviderCode
	,ResultLocationCode
	,ResultLocationDesc
	,RptToLocCode
	,SchedResGrpMfCd
	,ServiceGroup
	,Specialty
	)
select
	 OUTCLNCID
	,AppTypesAvlblGpBook
	,AppTypesAvlblGpInt
	,Comment
	,ConsCode
	,Description
	,DestForGpAppLetters
	,DestinationHospCd
	,FunctionCode
	,GpAduAppLtrDocument
	,GpAppConfirmPara
	,GpBroMaxNoDaysFuture
	,GpChiAppLtrDocument
	,GpLnkDftBookTypeDesc
	,GpReasonRefrlText
	,GpRefrlCriteriaPara
	,GplClinMfRtnAppTypes
	,GplClinMfUrgAppTypes
	,GplClinicMfUrgent
	,GplClinicMfUrgentInt
	,GplinkDftBookType
	,MaxAgeThatMeansChild
	,MfRecStsInd
	,ProviderCode
	,ResultLocationCode
	,ResultLocationDesc
	,RptToLocCode
	,SchedResGrpMfCd
	,ServiceGroup
	,Specialty
from
	openquery(INQUIRE, '

select 
	 OUTCLNCID
	,AppTypesAvlblGpBook
	,AppTypesAvlblGpInt
	,Comment
	,ConsCode
	,Description
	,DestForGpAppLetters
	,DestinationHospCd
	,FunctionCode
	,GpAduAppLtrDocument
	,GpAppConfirmPara
	,GpBroMaxNoDaysFuture
	,GpChiAppLtrDocument
	,GpLnkDftBookTypeDesc
	,GpReasonRefrlText
	,GpRefrlCriteriaPara
	,GplClinMfRtnAppTypes
	,GplClinMfUrgAppTypes
	,GplClinicMfUrgent
	,GplClinicMfUrgentInt
	,GplinkDftBookType
	,MaxAgeThatMeansChild
	,left(MfRecStsInd, 1) MfRecStsInd
	,ProviderCode
	,ResultLocationCode
	,ResultLocationDesc
	,RptToLocCode
	,SchedResGrpMfCd
	,ServiceGroup
	,Specialty
from
	OUTCLNC
')


--OPDoctor

truncate table PAS.OPDoctorBase

insert into PAS.OPDoctorBase
	(
	 OPDOCTORID
	,AllowOverbooking
	,ClinicHospCode
	,Comment
	,GmcGdcHcp
	,GradeOfStaff
	,LocCode
	,MfRecStsInd
	,Name
	,SchDays
	,SchedResMfCd
	,ServiceGroup
	,TypeOfDoctor
	,ValidApptTypes
	)
select
	 OPDOCTORID
	,AllowOverBooking
	,ClinicHospCode
	,Comment
	,GmcGdcHcp
	,GradeOfStaff
	,LocCode
	,MfRecStsInd
	,Name
	,SchDays
	,SchedResMfCd
	,ServiceGroup
	,TypeOfDoctor
	,ValidApptTypes
from
	openquery(INQUIRE, '

select 
	 OPDOCTORID
	,AllowOverBooking
	,ClinicHospCode
	,Comment
	,GmcGdcHcp
	,GradeOfStaff
	,LocCode
	,left(MfRecStsInd, 2) MfRecStsInd
	,Name
	,SchDays
	,SchedResMfCd
	,ServiceGroup
	,TypeOfDoctor
	,ValidApptTypes
from
	OPDOCTOR
where
	OPDOCTORID not like ''% ''
')



--Ward

update
	PAS.WardBase
set
	  WARDID = Ward.WARDID
	 ,ActivityStatus = Ward.ActivityStatus
	 ,AgesexGroup = Ward.AgesexGroup
	 ,AgesexGroup_1 = Ward.AgesexGroup_1
	 ,ClosedBeds = Ward.ClosedBeds
	 ,CmdsAgesexMix = Ward.CmdsAgesexMix
	 ,CmdsIntClCare = Ward.CmdsIntClCare
	 ,CmdsPtGroup = Ward.CmdsPtGroup
	 ,CmmLocationTypeDescriptionTruncated = Ward.CmmLocationTypeDescriptionTruncated
	 ,CottedWard = Ward.CottedWard
	 ,DayBedUnit = Ward.DayBedUnit
	 ,Days = Ward.Days
	 ,HospitalCode = Ward.HospitalCode
	 ,IntensityOfCare = Ward.IntensityOfCare
	 ,KarsIdCode = Ward.KarsIdCode
	 ,KornerCode = Ward.KornerCode
	 ,LocationType = Ward.LocationType
	 ,MfRecStsInd = Ward.MfRecStsInd
	 ,Nights = Ward.Nights
	 ,OwningWard = Ward.OwningWard
	 ,PatientGroup = Ward.PatientGroup
	 ,PrintSequence = Ward.PrintSequence
	 ,ProviderCode = Ward.ProviderCode
	 ,PtGroupintOfCare = Ward.PtGroupintOfCare
	 ,TimeAvailable = Ward.TimeAvailable
	 ,Ward = Ward.Ward
	 ,WardClassification = Ward.WardClassification
	 ,WardName = Ward.WardName
	 ,WardType = Ward.WardType
	 ,WardType_1 = Ward.WardType_1
from
	PAS.WardBase WardBase

inner join
	(
	select
		 WARDID
		,ActivityStatus
		,AgesexGroup
		,AgesexGroup_1
		,ClosedBeds
		,CmdsAgesexMix
		,CmdsIntClCare
		,CmdsPtGroup
		,CmmLocationTypeDescriptionTruncated
		,CottedWard
		,DayBedUnit
		,Days
		,HospitalCode
		,IntensityOfCare
		,KarsIdCode
		,KornerCode
		,LocationType
		,MfRecStsInd
		,Nights
		,OwningWard
		,PatientGroup
		,PrintSequence
		,ProviderCode
		,PtGroupintOfCare
		,TimeAvailable
		,Ward
		,WardClassification
		,WardName
		,WardType
		,WardType_1
	from
		openquery(INQUIRE, '
	select
		 WARDID
		,ActivityStatus
		,AgesexGroup
		,AgesexGroup_1
		,ClosedBeds
		,CmdsAgesexMix
		,CmdsIntClCare
		,CmdsPtGroup
		,left(CmmLocationTypeDescriptionTruncated, 50) CmmLocationTypeDescriptionTruncated
		,CottedWard
		,DayBedUnit
		,Days
		,HospitalCode
		,IntensityOfCare
		,KarsIdCode
		,KornerCode
		,LocationType
		,left(MfRecStsInd, 2) MfRecStsInd
		,Nights
		,OwningWard
		,PatientGroup
		,PrintSequence
		,ProviderCode
		,PtGroupintOfCare
		,TimeAvailable
		,Ward
		,WardClassification
		,WardName
		,WardType
		,left(WardType_1, 10) WardType_1
	from
		WARD
	') NewWard
	) Ward
on	WardBase.WARDID = Ward.WARDID

where
	not
	(
		WardBase.ActivityStatus = Ward.ActivityStatus
	and WardBase.AgesexGroup = Ward.AgesexGroup
	and WardBase.AgesexGroup_1 = Ward.AgesexGroup_1
	and WardBase.ClosedBeds = Ward.ClosedBeds
	and WardBase.CmdsAgesexMix = Ward.CmdsAgesexMix
	and WardBase.CmdsIntClCare = Ward.CmdsIntClCare
	and WardBase.CmdsPtGroup = Ward.CmdsPtGroup
	and WardBase.CmmLocationTypeDescriptionTruncated = Ward.CmmLocationTypeDescriptionTruncated
	and WardBase.CottedWard = Ward.CottedWard
	and WardBase.DayBedUnit = Ward.DayBedUnit
	and WardBase.Days = Ward.Days
	and WardBase.HospitalCode = Ward.HospitalCode
	and WardBase.IntensityOfCare = Ward.IntensityOfCare
	and WardBase.KarsIdCode = Ward.KarsIdCode
	and WardBase.KornerCode = Ward.KornerCode
	and WardBase.LocationType = Ward.LocationType
	and WardBase.MfRecStsInd = Ward.MfRecStsInd
	and WardBase.Nights = Ward.Nights
	and WardBase.OwningWard = Ward.OwningWard
	and WardBase.PatientGroup = Ward.PatientGroup
	and WardBase.PrintSequence = Ward.PrintSequence
	and WardBase.ProviderCode = Ward.ProviderCode
	and WardBase.PtGroupintOfCare = Ward.PtGroupintOfCare
	and WardBase.TimeAvailable = Ward.TimeAvailable
	and WardBase.Ward = Ward.Ward
	and WardBase.WardClassification = Ward.WardClassification
	and WardBase.WardName = Ward.WardName
	and WardBase.WardType = Ward.WardType
	and WardBase.WardType_1 = Ward.WardType_1
	)


insert into PAS.WardBase
(
	 WARDID
	,ActivityStatus
	,AgesexGroup
	,AgesexGroup_1
	,ClosedBeds
	,CmdsAgesexMix
	,CmdsIntClCare
	,CmdsPtGroup
	,CmmLocationTypeDescriptionTruncated
	,CottedWard
	,DayBedUnit
	,Days
	,HospitalCode
	,IntensityOfCare
	,KarsIdCode
	,KornerCode
	,LocationType
	,MfRecStsInd
	,Nights
	,OwningWard
	,PatientGroup
	,PrintSequence
	,ProviderCode
	,PtGroupintOfCare
	,TimeAvailable
	,Ward
	,WardClassification
	,WardName
	,WardType
	,WardType_1
)
select
	 WARDID
	,ActivityStatus
	,AgesexGroup
	,AgesexGroup_1
	,ClosedBeds
	,CmdsAgesexMix
	,CmdsIntClCare
	,CmdsPtGroup
	,CmmLocationTypeDescriptionTruncated
	,CottedWard
	,DayBedUnit
	,Days
	,HospitalCode
	,IntensityOfCare
	,KarsIdCode
	,KornerCode
	,LocationType
	,MfRecStsInd
	,Nights
	,OwningWard
	,PatientGroup
	,PrintSequence
	,ProviderCode
	,PtGroupintOfCare
	,TimeAvailable
	,Ward
	,WardClassification
	,WardName
	,WardType
	,WardType_1
from
	openquery(INQUIRE, '
select
	 WARDID
	,ActivityStatus
	,AgesexGroup
	,AgesexGroup_1
	,ClosedBeds
	,CmdsAgesexMix
	,CmdsIntClCare
	,CmdsPtGroup
	,left(CmmLocationTypeDescriptionTruncated, 50) CmmLocationTypeDescriptionTruncated
	,CottedWard
	,DayBedUnit
	,Days
	,HospitalCode
	,IntensityOfCare
	,KarsIdCode
	,KornerCode
	,LocationType
	,left(MfRecStsInd, 2) MfRecStsInd
	,Nights
	,OwningWard
	,PatientGroup
	,PrintSequence
	,ProviderCode
	,PtGroupintOfCare
	,TimeAvailable
	,Ward
	,WardClassification
	,WardName
	,WardType
	,left(WardType_1, 10) WardType_1
from
	WARD
') NewWard
where
	not exists
	(
	select
		1
	from
		PAS.WardBase Ward
	where
		Ward.WARDID = NewWard.WARDID
	)


-- Ethnic Origin

truncate table PAS.EthnicOriginBase

insert into PAS.EthnicOriginBase
	(
	 EthnicOriginCode
	,EthnicOrigin
	,EthnicGroupNationalCode
	,EthnicityNationalCode
	,EthnicityNational
	,MfRecStsInd
	)
    
select
	 EthnicOriginCode
	,EthnicOrigin
	,EthnicGroupNationalCode
	,EthnicityNationalCode
	,EthnicityNational
	,MfRecStsInd
from
	openquery(INQUIRE , '
select
	 EthnicGroupNatCode EthnicGroupNationalCode
	,EthnicityNatCode EthnicityNationalCode
	,EthnicityNatCodeDesc EthnicityNational
	,EthnicOriginCode
	,EthnicOriginDesc EthnicOrigin
	,left(MfRecStsInd , 2) MfRecStsInd
from
	ETHN
')

-- Staff Group

truncate table PAS.StaffGroupBase

insert into PAS.StaffGroupBase
	(
	 GradeOfStaffCode
	,GradeOfStaff
	,MedicalStaffTypeCode
	,MedicalStaffType
	,MfRecStsInd
	)
    
select
	 GradeOfStaffCode
	,GradeOfStaff
	,MedicalStaffTypeCode
	,MedicalStaffType
	,MfRecStsInd
from
	openquery(INQUIRE , '
select
	 GradeOfStaffCode
	,GradeOfStaffDescription GradeOfStaff
	,MedicalStaffType MedicalStaffTypeCode
	,MedicalStaffTypeDescription MedicalStaffType
	,left(MfRecStsInd , 2) MfRecStsInd
from
	STAFFGP
')

-- Management Intention

truncate table [PAS].[ManagementIntentionBase]

insert into [PAS].[ManagementIntentionBase]
           ([Description]
           ,[HaaCode]
           ,[IntdMgmt]
           ,[InternalValue]
           ,[IntMgmtCode]
           ,[MfRecStsInd]
           ,[ScottishIntValue])
select
	*
from
	openquery(INQUIRE , '
select
	 Description
	,HaaCode
	,IntdMgmt
	,InternalValue
	,IntMgmtCode
	,left(MfRecStsInd , 2) MfRecStsInd
	,ScottishIntValue
from
	INMAN
')

-- Contract

truncate table PAS.ContractBase

insert into [PAS].[ContractBase]
	(
	 [AgreementCDS]
	,[CidGrp]
	,[CidType]
	,[Cm2ContractMfSuitableToAutoAllocateInt]
	,[ContractDesc]
	,[ContractGroup]
	,[ContractGroupCodeDescription]
	,[ContractId]
	,[ContractId_1]
	,[ContractManagementEndDateInternal]
	,[ContractManagementStartDateInternal]
	,[Costsum]
	,[Description]
	,[Description_1]
	,[EndDate]
	,[InterimBilling]
	,[IntrmBilling]
	,[MfRecStsInd]
	,[OutpatientPrice1st]
	,[PaymentTerms]
	,[PriceProfile]
	,[PriceProfileDescription]
	,[ProcedureProfileCode]
	,[ProviderCode]
	,[PurchaserCode]
	,[Reatt]
	,[StartDate]
	,[SuitableToAutoallocate]
	,[Thresholdceiling]
	,[Type]
	,[TypeOfCont]
	,[WardAttPrice]
)

select
	*
from
	openquery(INQUIRE , '
select
	 AgreementCDS
	,CidGrp
	,CidType
	,Cm2ContractMfSuitableToAutoAllocateInt
	,ContractDesc
	,ContractGroup
	,ContractGroupCodeDescription
	,ContractId
	,ContractId_1
	,ContractManagementEndDateInternal
	,ContractManagementStartDateInternal
	,Costsum
	,Description
	,Description_1
	,EndDate
	,InterimBilling
	,IntrmBilling
	,left(MfRecStsInd , 2) MfRecStsInd
	,OutpatientPrice1st
	,PaymentTerms
	,PriceProfile
	,PriceProfileDescription
	,ProcedureProfileCode
	,ProviderCode
	,PurchaserCode
	,Reatt
	,StartDate
	,SuitableToAutoallocate
	,Thresholdceiling
	,Type
	,TypeOfCont
	,WardAttPrice
from
	CONTR
')

-- Purchaser

truncate table [PAS].[PurchaserBase]

insert into [PAS].[PurchaserBase]
(
	 [PURCHASERID]
	,[Address1]
	,[Address2]
	,[Address3]
	,[Address4]
	,[Aka]
	,[AuthorisationAddress1]
	,[AuthorisationAddress2]
	,[AuthorisationAddress3]
	,[AuthorisationAddress4]
	,[AuthorisationName]
	,[AuthorisationPhone]
	,[AuthorisationPostcode]
	,[CmmCommissioningConsoritaLeadCommDescription]
	,[CmmOrgCodesPurchaserEndDateinternal]
	,[CmmOrgCodesPurchaserStartDateinternal]
	,[ContactAddress1]
	,[ContactAddress2]
	,[ContactAddress3]
	,[ContactAddress4]
	,[ContactName]
	,[ContactPhone]
	,[ContactPostcode]
	,[Fax]
	,[InvoiceAddress1]
	,[InvoiceAddress2]
	,[InvoiceAddress3]
	,[InvoiceAddress4]
	,[InvoiceName]
	,[InvoicePhone]
	,[InvoicePostcode]
	,[Ldf]
	,[LeadCommissioner]
	,[Name]
	,[Phone]
	,[Postcode]
	,[PurchaserCode]
	,[Type]
)
select
	*
from
	openquery(INQUIRE , '
select
	 PURCHASERID
	,Address1
	,Address2
	,Address3
	,Address4
	,Aka
	,AuthorisationAddress1
	,AuthorisationAddress2
	,AuthorisationAddress3
	,AuthorisationAddress4
	,AuthorisationName
	,AuthorisationPhone
	,AuthorisationPostcode
	,CmmCommissioningConsoritaLeadCommDescription
	,CmmOrgCodesPurchaserEndDateinternal
	,CmmOrgCodesPurchaserStartDateinternal
	,ContactAddress1
	,ContactAddress2
	,ContactAddress3
	,ContactAddress4
	,ContactName
	,ContactPhone
	,ContactPostcode
	,Fax
	,InvoiceAddress1
	,InvoiceAddress2
	,InvoiceAddress3
	,InvoiceAddress4
	,InvoiceName
	,InvoicePhone
	,InvoicePostcode
	,left(Ldf , 2) Ldf
	,LeadCommissioner
	,Name
	,Phone
	,Postcode
	,PurchaserCode
	,Type
from
	PURCHASER
')


-- OP Cancellation Reason

truncate table [PAS].[OPCancellationReasonBase]

insert into [PAS].[OPCancellationReasonBase]
(
	 [CancellationCode]
	,[Description]
	,[EbsInboundCancReason]
	,[EbsInboundCancReasonInt]
	,[EbsReasonCode]
	,[EbsReasonDesc]
	,[MfRecStsInd]
)
select
	*
from
	openquery(INQUIRE , '
select
	 CancellationCode
	,left(Description , 34) Description
	,EbsInboundCancReason
	,EbsInboundCancReasonInt
	,EbsReasonCode
	,EbsReasonDesc
	,left(MfRecStsInd , 2) MfRecStsInd
from
	OPCANCRSN
')

--Casenote Location
truncate table [Warehouse].[PAS].[CasenoteLocationBase];

INSERT INTO [WarehouseSQL].[Warehouse].[PAS].[CasenoteLocationBase]
(Location, LocationName, MfRecStsInd)
SELECT Location, LocationName, MfRecStsInd
FROM openquery(INQUIRE,'
SELECT Location, LocationName, LEFT(MfRecStsInd,2) MfRecStsInd
  FROM CASENLOC');

--Borrower

truncate table PAS.BorrowerBase;

INSERT PAS.BorrowerBase
  (
  BorrowerAddressLine1,
	BorrowerCode,
	BorrowerPostcode,
	BorrowerSurname,
	BorrowerTelephoneNumber,
	BorrowerTitleAndInitials,
	Line2,
	Line3,
	Line4,
	MfRecStsInd
  )
  SELECT 
  BorrowerAddressLine1,
	BorrowerCode,
	BorrowerPostcode,
	BorrowerSurname,
	BorrowerTelephoneNumber,
	BorrowerTitleAndInitials,
	Line2,
	Line3,
	Line4,
	MfRecStsInd
FROM openquery(INQUIRE,  'SELECT BorrowerAddressLine1,
	BorrowerCode,
	BorrowerPostcode,
	BorrowerSurname,
	BorrowerTelephoneNumber,
	BorrowerTitleAndInitials,
	Line2,
	Line3,
	Line4,
	left(MfRecStsInd,2) MfRecStsInd
	FROM BORROWER');
	
   

select @Elapsed = DATEDIFF(minute, @StartTime, getdate())

select @Stats = 
	'Time Elapsed ' + 
	CONVERT(varchar(6), @Elapsed) + ' Mins'

exec dbo.WriteAuditLogEvent 'ExtractInquireReferenceData', @Stats, @StartTime


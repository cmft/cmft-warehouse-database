﻿
create view [PAS].[DischargeMethod] 

as

select
	 DischargeMethodCode = MODID
	,DischargeMethod = Description

	,NationalDischargeMethodCode =
		case MODID
		when 'AB' then '9'
		when 'DN' then '4'
		when 'DP' then '4'
		when 'MH' then '3'
		when 'NO' then '1'
		when 'SD' then '2'
		when 'SP' then '5'
		when 'ST' then '5'
		else '9'
		end

	,DeletedFlag = MfRecStsInd
from
	PAS.DischargeMethodBase


--National Codes:

--1 PATIENT discharged on clinical advice or with clinical consent 
--2 PATIENT discharged him/herself or was discharged by a relative or advocate 
--3 PATIENT discharged by mental health review tribunal, Home Secretary or court 
--4 PATIENT died 
--5 Stillbirth 


﻿



CREATE view [PAS].[Consultant] as

select
	 ConsultantCode = CONSMASTID
--	,Consultant
--	,ConsultantCode2
	,ContractedSpecialtyCode = ContractedSpecialty
	,Deleted
--	,ExternalConsultant = ExternalCons
	,Title = ForenamePfx
	,NationalConsultantCode = GMCCode
	,Initials = Inits
--	,KornerCode
--	,Midwife
	,Consultant = Name
	,MainSpecialtyCode =  --Consultant.PrimarySpecialty
		coalesce(
				xrefmainspecialty.XrefEntityCode 
				,Consultant.PrimarySpecialty
				)
						--case
						--	when xrefmainspecialty.XrefEntityCode is not null
						--	then xrefmainspecialty.XrefEntityCode
						--	else Consultant.PrimarySpecialty
						--end
	,ProviderCode
	,Surname
	,DomainLogin = xrefwindowslogin.DomainLogin
from
	PAS.ConsultantBase Consultant
	
left outer join -- prevents issues with duplicates if consultant is mapped to more than one login. Takes latest association
			(
			select
				ConsultantCode = xrefwindowslogin.EntityCode
				,DomainLogin = xrefwindowslogin.XrefEntityCode
			from
				dbo.EntityXref xrefwindowslogin

			where
				xrefwindowslogin.EntityTypeCode = 'CONSULTANTPASCODE' 
			and xrefwindowslogin.XrefEntityTypeCode = 'CONSULTANTWINDOWSLOGIN'
			and	not exists
						(
							select
								1
							from
								dbo.EntityXref laterxrefwindowslogin 
							where
								laterxrefwindowslogin.EntityCode = xrefwindowslogin.EntityCode
							and laterxrefwindowslogin.EntityTypeCode = xrefwindowslogin.EntityTypeCode 
							and laterxrefwindowslogin.XrefEntityTypeCode = xrefwindowslogin.XrefEntityTypeCode
							and laterxrefwindowslogin.EntityXrefRecno > xrefwindowslogin.EntityXrefRecno
						)
			)	xrefwindowslogin	

on Consultant.CONSMASTID = xrefwindowslogin.ConsultantCode

left outer join dbo.EntityXref xrefmainspecialty
on xrefmainspecialty.EntityCode = Consultant.CONSMASTID
and xrefmainspecialty.EntityTypeCode = 'CONSULTANTPASCODE' 
and xrefmainspecialty.XrefEntityTypeCode = 'CONSULTANTMAINSPECIALTYPASCODE'









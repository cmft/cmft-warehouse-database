﻿
create view [PAS].[DischargeDestination] 

as

select
	 DischargeDestinationCode = DODID
	,DischargeDestination = Description

	,NationalDischargeDestinationCode =
		case DODID
		when 'FC' then '66' 
		when 'HO' then '19' 
		when 'HP' then '87' 
		when 'HS' then '29' 
		when 'LA' then '65' 
		when 'NH' then '54' 
		when 'NR' then '85' 
		when 'NS' then '88' 
		when 'OR' then '19' 
		when 'OV' then '87' 
		when 'PR' then '38' 
		when 'RH' then '19' 
		when 'SP' then '53' 
		when 'TG' then '51' 
		when 'TM' then '52' 
		when 'TO' then '51' 
		when 'TP' then '49' 
		when 'TU' then '51' 
		when 'UK' then '99' 
		when 'ZP' then '79' 
		when 'ZZ' then '79'
		else '99'
		end

	,DeletedFlag = MfRecStsInd
from
	PAS.DischargeDestinationBase

--National Codes:

--19 Usual place of residence unless listed below, for example, a private dwelling whether owner occupied or owned by local authority, housing association or other landlord. This includes wardened accommodation but not residential accommodation where health care is provided. It also includes PATIENTS with no fixed abode. 
--29 Temporary place of residence when usually resident elsewhere (includes hotel, residential educational establishment) 
--30 Repatriation from high security psychiatric accommodation in an NHS Hospital Provider (NHS Trust) 
--37 Court 
--38 Penal establishment or police station 
--48 High Security Psychiatric Hospital, Scotland 
--49 NHS other hospital provider - high security psychiatric accommodation 
--50 NHS other hospital provider - medium secure unit 
--51 NHS other hospital provider - ward for general PATIENTS or the younger physically disabled 
--52 NHS other hospital provider - ward for maternity PATIENTS or neonates 
--53 NHS other hospital provider - ward for PATIENTS who are mentally ill or have learning disabilities 
--54 NHS run Care Home 
--65 Local Authority residential accommodation ie where care is provided 
--66 Local Authority foster care 
--79 Not applicable - PATIENT died or still birth 
--84 Non-NHS run hospital - medium secure unit 
--85 Non-NHS (other than Local Authority) run Care Home 
--87 Non-NHS run hospital 
--88 Non-NHS (other than Local Authority) run Hospice 


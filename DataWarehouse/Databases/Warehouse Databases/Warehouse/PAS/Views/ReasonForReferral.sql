﻿



CREATE view [PAS].[ReasonForReferral] 

as

select
	 ReasonForReferralCode = ReasonForReferralBase.OPREFREASONID
	,ReasonForReferral = Description
	,NationalReasonForReferralCode = ReasonForReferralBase.DohReasonForReferral
	,DeletedFlag = MfRecStsInd
from
	PAS.ReasonForReferralBase





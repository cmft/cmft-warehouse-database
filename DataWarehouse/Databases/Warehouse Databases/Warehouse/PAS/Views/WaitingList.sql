﻿
create view [PAS].[WaitingList] as

SELECT
	 WaitingListCode = WaitingList
	,WaitingList = WlDescription
	,SiteCode = HospitalCode
	,ConsultantCode = MainConsultant
	,SpecialtyCode = SpecOfList
	,ProviderCode
	,CurrentlySequencedBy
	,SequencedBy
	,ElectiveAdmissionListNo = ElAdmListNo
	,DeletedFlag = MfRecStsInd
	--,RepeatList
	--,RepeatList_1
	--,WlMaxAdmTimed
	--,WlMaxAdmTimeo
	--,WlMfCurrentSortInt
	--,WlMfRequestedSortInt
FROM
	PAS.WaitingListBase





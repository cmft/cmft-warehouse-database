﻿
create view PAS.Religion

as

select
	 ReligionCode = ReligionBase.Religion
	,Religion = ReligionBase.Description
	,ReligionNationalCode = XrefEntityCode

from
	PAS.ReligionBase

inner join dbo.EntityXref
on	EntityXref.EntityCode = ReligionBase.Religion
and EntityXref.EntityTypeCode = 'LOCALRELIGIONCODE'
and EntityXref.XrefEntityTypeCode = 'NATIONALRELIGIONCODE'
	

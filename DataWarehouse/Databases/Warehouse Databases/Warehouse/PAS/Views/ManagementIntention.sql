﻿

create view [PAS].[ManagementIntention] 

as

select
	 ManagementIntentionCode = IntdMgmt
	,ManagementIntention = Description
	,NationalManagementIntentionCode = 
										case
										InternalValue
										when '6' then '8'
										when '2' then '2'
										when '1' then '1'
										when '5' then '5'
										when '4' then '4'
										when '3' then '3'
										else '9'
										end
	,InternalCode = InternalValue
	,DeletedFlag = MfRecStsInd
from
	PAS.ManagementIntentionBase
	
/* 	

Code	CodeDesc			IntValEng	ImpCode	LglDelInd	NatCode
		
B		BORN IN HOSP/ON WAY		6		MARI	0			8
D		DAY CASE				2		MARI	0			2
I		INPATIENT				1		MARI	0			1
N		REGULAR NIGHT ADM		5		MARI	0			5				
R		REGULAR DAY ADM			4		MARI	0			4			
V		INTERVAL ADMISSION		3		MARI	0			3

*/ 













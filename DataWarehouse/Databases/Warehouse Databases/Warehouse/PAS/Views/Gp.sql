﻿create view [PAS].[Gp] as

select
	 GpCode = GPID
	,Gp = GpName
	,PracticeCode = PracticeCode
	,NationalCode = DohCode
from
	PAS.GpBase

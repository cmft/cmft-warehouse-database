﻿

CREATE view [PAS].[Doctor] as

select
	 DoctorCode = OPDOCTORID
	,Doctor = Name
	,Comment
	,StaffGradeCode = OPDoctorBase.GradeOfStaff
	,DoctorTypeCode = OPDoctorBase.TypeOfDoctor
from
	PAS.OPDoctorBase



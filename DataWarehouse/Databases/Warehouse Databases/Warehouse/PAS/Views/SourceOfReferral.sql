﻿
CREATE  view [PAS].[SourceOfReferral] as

select
	 SourceOfReferralCode = OPREFSOURCEID
	,SourceOfReferral = Description
	,NationalSourceOfReferralCode = right('0' + CmdsValue, 2)
	,ReportableFlag
	,NewFlag
from
	PAS.SourceOfReferralBase
--where
--	MfRecStsInd = 0

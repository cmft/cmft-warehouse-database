﻿CREATE view [PAS].[CasenoteBorrower] as
SELECT
	 BorrowerCode
	,Borrower = coalesce(BorrowerTitleAndInitials + ' ' , '') + coalesce(BorrowerSurname , '')
	,Surname = BorrowerSurname
	,TitleAndInitials = BorrowerTitleAndInitials
	,Address1 = BorrowerAddressLine1
	,Address2 = Line2
	,Address3 = Line3
	,Address4 = Line4
	,Postcode = BorrowerPostcode
	,PhoneNumber = BorrowerTelephoneNumber
	,DeletedFlag = MfRecStsInd
FROM
	PAS.BorrowerBase

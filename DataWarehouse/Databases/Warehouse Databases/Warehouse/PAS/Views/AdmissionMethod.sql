﻿
create view [PAS].[AdmissionMethod] 

as

select
	 AdmissionMethodCode = MOAID
	,AdmissionMethod = Description

	,NationalAdmissionMethodCode =
		case MOAID
		when '18' then '28'
		when 'AE' then '21'
		when 'AN' then '31'
		when 'BB' then '23'
		when 'BH' then '82'
		when 'BL' then '12'
		when 'BM' then '28'
		when 'BT' then '83'
		when 'CM' then '31'
		when 'DO' then '28'
		when 'EM' then '28'
		when 'ET' then '28'
		when 'GP' then '22'
		when 'NS' then '11'
		when 'OP' then '24'
		when 'PL' then '13'
		when 'PN' then '32'
		when 'R' then '13'
		when 'SN' then '11'
		when 'TR' then '81'
		when 'WL' then '11'
		when 'ZD' then '31'
		when 'ZM' then '31'
		when 'ZZ' then '31'
		else '99'
		end

	,InternalCode = InternalValue
	,DeletedFlag = MfRecStsInd

	,AdmissionMethodTypeCode =
	case 
		left(
		case MOAID
		when '18' then '28'
		when 'AE' then '21'
		when 'AN' then '31'
		when 'BB' then '23'
		when 'BH' then '82'
		when 'BL' then '12'
		when 'BM' then '28'
		when 'BT' then '83'
		when 'CM' then '31'
		when 'DO' then '28'
		when 'EM' then '28'
		when 'ET' then '28'
		when 'GP' then '22'
		when 'NS' then '11'
		when 'OP' then '24'
		when 'PL' then '13'
		when 'PN' then '32'
		when 'R' then '13'
		when 'SN' then '11'
		when 'TR' then '81'
		when 'WL' then '11'
		when 'ZD' then '31'
		when 'ZM' then '31'
		when 'ZZ' then '31'
		else '99'
		end
		, 1)
	when '1' then 'EL'
	when '2' then 'EM'
	when '3' then 'MAT'
	else 'NE'
	end

from
	PAS.AdmissionMethodBase

--National Codes:

--Elective Admission, when the DECISION TO ADMIT could be separated in time from the actual admission: 
--11 Waiting list 
--12 Booked 
--13 Planned 
--Note that this does not include a transfer from another Hospital Provider (see 81 below). 

--Emergency Admission, when admission is unpredictable and at short notice because of clinical need: 
--21 Accident and emergency or dental casualty department of the Health Care Provider  
--22 GENERAL PRACTITIONER: after a request for immediate admission has been made direct to a Hospital Provider, i.e. not through a Bed bureau, by a GENERAL PRACTITIONER or deputy 
--23 Bed bureau 
--24 Consultant Clinic, of this or another Health Care Provider  
--25 Admission via Mental Health Crisis Resolution Team *   
--28 Other means, examples are:
--- admitted from the Accident and Emergency Department of another provider where they had not been admitted
--- transfer of an admitted PATIENT from another Hospital Provider in an emergency
--- baby born at home as intended 

--Maternity Admission, of a pregnant or recently pregnant woman to a maternity ward (including delivery facilities) except when the intention is to terminate the pregnancy 
--31 Admitted ante-partum 
--32 Admitted post-partum 

--Other Admission not specified above 
--82 The birth of a baby in this Health Care Provider  
--83 Baby born outside the Health Care Provider except when born at home as intended. 
--81 Transfer of any admitted PATIENT from other Hospital Provider other than in an emergency 


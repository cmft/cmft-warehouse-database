﻿
create view PAS.Service as

select
	--HospitalCode
	ServiceCode = Service 
	,Service = Description
	,Label = DescriptionLabels
	--,DescriptionShort
	,CategoryOfService
	--,CategoryOfServiceInt
	,ScreenTypeCode = ScreenType
	--,Mnemonic
	--,Deleted
	--,OCMSERVICEID
from
	PAS.ServiceBase
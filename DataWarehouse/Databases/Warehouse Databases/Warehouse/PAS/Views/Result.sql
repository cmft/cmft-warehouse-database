﻿
CREATE view PAS.Result

as

select
	ResultCode = [ResultCd]
	,Result = [ResultDescription]
	--,[ResultName]
	,ResultType = [ResultType]
	,[UnitOfMeasurement]
	,DeletedFlag = [Deleted]
	--,[OCMFRESULTSID]
from
	[PAS].[ResultBase]
GO
--GRANT SELECT
--    ON OBJECT::[PAS].[Result] TO [scc]
--    AS [dbo];


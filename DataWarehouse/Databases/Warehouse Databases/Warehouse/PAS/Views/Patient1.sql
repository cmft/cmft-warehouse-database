﻿





CREATE view [PAS].[Patient1] as

SELECT
	 --ImpCode
	 SourcePatientNo = InternalPatientNumber -- INFOCOM InternalNo
	,DateOfBirth = CONVERT(DateTime,InternalDateOfBirth,113) --DOB
	,DeathIndicator =	CASE 
							WHEN PtDeathIndInt = 0 THEN 'NO' --INFOCOM DeathInd
							WHEN PtDeathIndInt = 1 THEN 'YES'
						END
	--,DeathYear
	,DateOfDeath = PtDateOfDeath
	,SexCode = Sex
	,MaritalStatusCode = MaritalStatus
	,Surname
	,Forenames
	,Title
	,NextOfKin = NoKName
	--,BloodGroup
	--,Rhesus
	--,RiskFactor1
	,EthnicOriginCode = EthnicType
	--,RiskFactor2
	,ReligionCode = Religion
	,PreviousSurname = PreviousSurname1
	,NHSNumber = NHSNumber
	,NHSNumberStatusId = NHSNumberStatus
	,DOR = DistrictOfResidenceCode
	,DistrictNo = DistrictNumber
	,MilitaryNo = NULL
	,AddressLine1 = [PtAddrLine1]
    ,AddressLine2 = [PtAddrLine2]
    ,AddressLine3 = [PtAddrLine3]
    ,AddressLine4 = [PtAddrLine4]
	,Postcode = PtAddrPostCode
	--,GPType
	,RegisteredGpCode = GpCode --INFOCOM RegGP
	--,GDPType
	,RegisteredGdpCode = GdpCode
	,MRSAFlag = MRSA
	,MRSADate = MRSASetDt
	--,PatSchool
	--,PatOccn
	--,Summary1
	--,Summary2
	--,Summary3
	--,BirthName
	--,PlaceOfBirth
	--,CarerRelCd
	--,CPARevDate
	--,CPALastUpDate
	--,CPAPriority
	--,CPARiskCat1
	--,CPARiskCat2
	--,CPARiskCat3
	--,CPARiskCat4
	--,CPARiskCat5
	--,CPAGAFScore
	--,CPACategoryX
	--,CPADependencyX
	--,ExtPmiAddLine1
	--,ExtPmiAddLine2
	--,ExtPmiAddLine3
	--,ExtPmiAddLine4
	--,ExtPmiAddLine5
	--,ExtPmiAddPostcd
	
	
FROM
	PAS.PatientBase







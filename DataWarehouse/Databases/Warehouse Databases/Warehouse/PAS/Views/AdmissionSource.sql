﻿
create view [PAS].[AdmissionSource] 

as

select
	 AdmissionSourceCode = SOADID
	,AdmissionSource = Description

	,NationalAdmissionSourceCode =
		case SOADID
		when 'BH' then '79'
		when 'FC' then '66'
		when 'HO' then '19'
		when 'HP' then '87'
		when 'HS' then '29'
		when 'LA' then '65'
		when 'NH' then '54'
		when 'NR' then '85'
		when 'NS' then '88'
		when 'PR' then '39'
		when 'SP' then '49'
		when 'TG' then '51'
		when 'TM' then '52'
		when 'TP' then '53'
		else '99'
		end

	,DeletedFlag = MfRecStsInd
from
	PAS.AdmissionSourceBase


--National Codes:

--19 Usual place of residence unless listed below, for example, a private dwelling whether owner occupied or owned by local authority, housing association or other landlord. This includes wardened accommodation but not residential accommodation where health care is provided. It also includes patients with no fixed abode. 
--29 Temporary place of residence when usually resident elsewhere (e.g. hotels, residential educational establishments) 
--39 Penal establishment, Court, or police station 
--49 NHS other hospital provider - high security psychiatric accommodation in an NHS hospital provider (NHS trust) 
--51 NHS other hospital provider - ward for general patients or the younger physically disabled or A & E department 
--52 NHS other hospital provider - ward for maternity patients or neonates 
--53 NHS other hospital provider - ward for patients who are mentally ill or have learning disabilities 
--54 NHS run care home 
--65 Local Authority residential accommodation ie where care is provided 
--66 Local Authority foster care 
--79 Babies born in or on the way to hospital 
--85 Non-NHS (other than Local Authority) run care home 
--87 Non NHS run hospital 
--88 Non-NHS (other than Local Authority) run Hospice 


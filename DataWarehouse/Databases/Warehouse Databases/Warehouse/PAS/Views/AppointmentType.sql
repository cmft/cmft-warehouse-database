﻿create view PAS.AppointmentType as
SELECT
	 AppointmentTypeCode
	,AppointmentType = AppointmentTypeDesc
	,EbookingFlag = EBookingInt
	,DeletedFlag = MfRecStsInd
	,ReminderLetter
	,SchedulingAppointmentClassificationCode = SchedulingAppointmentClassification
	,SchedulingAppointmentClassification = SchedulingApptClassificationDescription
	,SchedulingRemiderLetterFlag = SchRemLetterInt
	,UrgentFlag = Urgent_1
FROM
	PAS.OPAppointmentTypeBase

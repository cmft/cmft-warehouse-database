﻿
CREATE VIEW [PAS].[Provider] AS

SELECT
	 ProviderCode = ProviderCode
	,[Description] = [Description]
	,NationalCode = DohCode
FROM
	PAS.ProviderBase


﻿

create  view [PAS].[StaffGroup] as

select
	 StaffGroupCode = StaffGroupBase.GradeOfStaffCode
	,StaffGroup = StaffGroupBase.GradeOfStaff
	,StaffGroupBase.MedicalStaffTypeCode
	,StaffGroupBase.MedicalStaffType
from
	PAS.StaffGroupBase



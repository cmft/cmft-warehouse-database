﻿



CREATE view [PAS].[Specialty] as

select
	 SpecialtyBase.SpecialtyCode
--	,CmtGpfhExemptSpecialtyInt
	,Specialty = SpecialtyBase.Description
	,NationalSpecialtyCode = left(SpecialtyBase.DohCode, 3)
	,LocalSubSpecialtyCode = right(SpecialtyBase.DohCode, 1)
--	,GpfhExempt
--	,HaaCode
--	,KarsIdCode
--	,MfRecStsInd
--	,ScottishIntValue
--	,ScottishLocalCode
--	,SpecGroup
--	,Specialty
--	,SpecialtyType
--	,SupraregCode

	,TreatmentFunctionFlag = 
		convert(
			 bit
			,case

			when SpecialtyMap.SpecialtyCode is not null
			then 0

			when Specialty.SpecialtyCode is not null
			then 0

			else 1
			end
		)

	--,TreatmentFunctionFlag = 
	--	convert(bit
	--		,case
	--		when SpecialtyBase.TreatmentFunction = 'YES'
	--		then 1
	--		else 0
	--		end
	--	)

--	,MainSpecialty
from
	PAS.SpecialtyBase

left join PAS.SpecialtyMap
on	SpecialtyMap.SpecialtyCode = SpecialtyBase.SpecialtyCode

left join WH.Specialty Specialty
on	Specialty.SpecialtyCode = left(SpecialtyBase.DohCode, 3)

--where
--	MfRecStsInd = 0
--or	SpecialtyCode = 'UNK'



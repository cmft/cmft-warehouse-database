﻿
CREATE view [PAS].[Clinic] as

select
	Clinic.*

	,VirtualClinicFlag =
	convert(bit
		,case
		when VirtualClinic.EntityCode is null
		then 0
		else 1
		end
	)

	,POAClinicFlag =
	convert(bit
		,case
		when POAClinic.EntityCode is null
		then 0
		else 1
		end
	)
from
	(
	select
		 ClinicCode = OUTCLNCID
	--	,AppTypesAvlblGpBook
	--	,AppTypesAvlblGpInt
		,Comment
		,ConsultantCode = ConsCode
		,Clinic = Description
	--	,DestForGpAppLetters
		,SiteCode = DestinationHospCd
		,FunctionCode
	--	,GpAduAppLtrDocument
	--	,GpAppConfirmPara
	--	,GpBroMaxNoDaysFuture
	--	,GpChiAppLtrDocument
	--	,GpLnkDftBookTypeDesc
	--	,GpReasonRefrlText
	--	,GpRefrlCriteriaPara
	--	,GplClinMfRtnAppTypes
	--	,GplClinMfUrgAppTypes
	--	,GplClinicMfUrgent
	--	,GplClinicMfUrgentInt
	--	,GplinkDftBookType
	--	,MaxAgeThatMeansChild
		,DeletedFlag = MfRecStsInd
		,ProviderCode
	--	,ResultLocationCode
	--	,ResultLocationDesc
		,ReportToLocationCode = RptToLocCode
	--	,SchedResGrpMfCd
		,ServiceGroupCode = ServiceGroup
		,SpecialtyCode = Specialty
	from
		PAS.ClinicBase
	) Clinic

left join dbo.EntityLookup VirtualClinic
on	VirtualClinic.EntityTypeCode = 'VIRTUALCLINIC'
and	VirtualClinic.EntityCode = Clinic.ClinicCode

left join dbo.EntityLookup POAClinic
on	POAClinic.EntityTypeCode = 'POACLINIC'
and	POAClinic.EntityCode = Clinic.ClinicCode


﻿

CREATE view [PAS].[EthnicOrigin] as

select
	 [EthnicOriginCode]
	,[EthnicOrigin]
	--,[EthnicGroupNationalCode]
	,NationalEthnicOriginCode = [EthnicityNationalCode]
	,NationalEthnicOrigin = [EthnicityNational]
	,DeletedFlag = [MfRecStsInd]
from
	[PAS].[EthnicOriginBase]




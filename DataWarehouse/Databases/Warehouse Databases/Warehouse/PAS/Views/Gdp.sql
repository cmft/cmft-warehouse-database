﻿
create view [PAS].[Gdp] as

select
	 GdpCode = GDPID
	,Gdp = GdpBase.Name
	,NationalCode = GdpBase.DpbContractCode
from
	PAS.GdpBase


﻿CREATE view PAS.SpecialtyMap as

select
	 SpecialtyCode = SPEC collate database_default
	,NationalSpecialtyCode = cast(KORNERSPEC as varchar)
	,NationalSpecialty = KORNERDESC collate database_default
from
	[$(ICOM)].dbo.ISPEC



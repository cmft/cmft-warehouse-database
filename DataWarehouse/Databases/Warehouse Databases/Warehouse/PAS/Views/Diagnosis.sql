﻿






CREATE view [PAS].[Diagnosis] as

select
	 DiagnosisCode = DiagnosisBase.ICD10ID
	,Diagnosis = DiagnosisBase.Description
	,DiagnosisChapterCode = left(DiagnosisBase.ICD10ID, 3)
	--,DiagnosisChapter = -- there is a mixture of ThreeDigitNames for each chapter heading - this code picks one.  Replace with National reference file when available.
	--	(
	--	select top 1
	--		ThreeDigitName
	--	from
	--		Warehouse.PAS.DiagnosisBase Chapter
	--	where
	--		left(Chapter.ICD10ID, 3) = left(DiagnosisBase.ICD10ID, 3)
	--	)
	,DiagnosisChapter = Chapter.ThreeDigitName
	,IsCharlsonFlag =
			case
				when Charlson.[EntityCode] is not null
				then 1
			end
	,IsConnectingForHealthMandatoryFlag = 
				case
					when ConnectingForHealth.[EntityCode] is not null
					then 1
				end
	,IsAlwaysPresentFlag = 
			case
				when CMFTAlways.[EntityCode] is not null
				then 1
			end
	,IsLongTermFlag = 
			case
				when CMFTLongTerm.[EntityCode] is not null
				then 1
			end

from
	PAS.DiagnosisBase

left outer join 
	dbo.EntityLookup ConnectingForHealth
on	ConnectingForHealth.[EntityCode]  = DiagnosisBase.ICD10ID
and ConnectingForHealth.[EntityTypeCode] = 'CFHDIAGNOSISCODE'

--left outer join 
--	dbo.EntityLookup Charlson
--on	Charlson.[EntityCode]  = DiagnosisBase.ICD10ID
--and Charlson.[EntityTypeCode] = 'CHARLSONDIAGNOSISCODE'

left outer join 
	[dbo].[EntityXref] Charlson
on	Charlson.[EntityCode]  = DiagnosisBase.ICD10ID
and Charlson.EntityTypeCode = 'DIAGNOSISCODE'
and Charlson.XrefEntityTypeCode = 'CHARLSONINDEXCODE'

left outer join 
	dbo.EntityLookup CMFTAlways
on	CMFTAlways.[EntityCode]  = DiagnosisBase.ICD10ID
and CMFTAlways.[EntityTypeCode] = 'CMFTALWAYSDIAGNOSISCODE'

left outer join 
	dbo.EntityLookup CMFTLongTerm
on	CMFTLongTerm.[EntityCode]  = DiagnosisBase.ICD10ID
and CMFTLongTerm.[EntityTypeCode] = 'CMFTLONGTERMDIAGNOSISCODE'


inner join
		(
		select 
			ChapterCode = left(ICD10ID, 3)
			,ThreeDigitName
			,SequenceNo = row_number() over (partition by left(ICD10ID, 3)  order by left(ICD10ID, 3)) 
		from
			PAS.DiagnosisBase Chapter
		) Chapter

on	ChapterCode = left(DiagnosisBase.ICD10ID, 3)
and Chapter.SequenceNo = 1









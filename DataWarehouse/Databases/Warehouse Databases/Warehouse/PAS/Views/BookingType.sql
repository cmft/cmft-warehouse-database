﻿CREATE view [PAS].[BookingType] as

select
	 BookingTypeCode = Code
	,BookingType = Description
	,BookingCode = InternalValue
	,Booking = InternalValueDescription
	,DeletedFlag = MfRecStsInd
from
	PAS.BookingTypeBase

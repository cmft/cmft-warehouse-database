﻿
create view PAS.AugmentedCareLocation

as

select
	LocationCode = AcpLocationCode
	--,LocationCode1 = AcpLocation
	,WardCode
	,Ward = Description
	,SiteCode
	,NationalLocationCode = AcpLocNatCode
	--,ApcAugmentedCareLocationDefaultConsultantInt
	,NationalLocation = ApcAugmentedCareLocationNationalCodeDescription
	,CriticalCareUnitCode = CcUnitCode
	,CriticalCareUnit = CcUnitFunctionDescription
	,DefaultConsultantIndicator = DefaultConsultant
	--,MfRecStsInd
from
	PAS.AugmentedCareLocationBase


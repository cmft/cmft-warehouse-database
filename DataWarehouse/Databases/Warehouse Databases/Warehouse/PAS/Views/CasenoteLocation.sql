﻿create view PAS.CasenoteLocation as

SELECT
	 CasenoteLocationCode = Location
	,CasenoteLocation = LocationName
	,DeletedFlag = MfRecStsInd
FROM
	PAS.CasenoteLocationBase

﻿create view PAS.MaritalStatus as

select
	 MaritalStatusCode = MARITALSID
	--,CancerRegMdsCode
	,MaritalStatus = Description
	,NationalMaritalStatusCode = HaaCode
	--,IntValue
	--,MaritalStatus
	--,MfRecStsInd
	--,ScottishIntValue
	--,StsIntCd
from
	PAS.MaritalStatusBase

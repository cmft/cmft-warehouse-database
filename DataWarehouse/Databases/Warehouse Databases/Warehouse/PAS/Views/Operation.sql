﻿
CREATE view [PAS].[Operation] as

select
	 OperationCode = OPCS4ID
	,Operation = Opcs4Dx4
	,OperationChapter3 = Opcs4Dx3
	,OperationGroupCode = Opcs4Groupcode
from
	PAS.OperationBase

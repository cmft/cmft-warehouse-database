﻿CREATE TABLE [HR].[Summary](
	[SummaryRecno] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationCode] [int] NOT NULL,
	[WardCode] [int] NULL,
	[CensusDate] [date] NOT NULL,
	[AppraisalRequired] [float] NULL,
	[AppraisalCompleted] [float] NULL,
	[ClinicalMandatoryTrainingRequired] [float] NULL,
	[ClinicalMandatoryTrainingCompleted] [float] NULL,
	[CorporateMandatoryTrainingRequired] [float] NULL,
	[CorporateMandatoryTrainingCompleted] [float] NULL,
	[RecentAppointment] [float] NULL,
	[LongTermAppointment] [float] NULL,
	[SicknessAbsenceFTE] [float] NULL,
	[SicknessEstablishmentFTE] [float] NULL,
	[Headcount3mthRolling] [float] NULL,
	[FTE3mthRolling] [float] NULL,
	[StartersHeadcount3mthRolling] [float] NULL,
	[StartersFTE3mthRolling] [float] NULL,
	[LeaversHeadcount3mthRolling] [float] NULL,
	[LeaversFTE3mthRolling] [float] NULL,
	[BudgetWTE] [float] NULL,
	[ContractedWTE] [float] NULL,
	[ClinicalBudgetWTE] [float] NULL,
	[ClinicalContractedWTE] [float] NULL,
	[InterfaceCode] [varchar](50) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[ByWhom] [varchar](50) NULL,
	[ACAgencySpend] [float] NULL,
	[HeadcountMonthly] [float] NULL,
	[FTEMonthly] [float] NULL,
	[StartersHeadcountMonthly] [float] NULL,
	[StartersFTEMonthly] [float] NULL,
	[LeaversHeadcountMonthly] [float] NULL,
	[LeaversFTEMonthly] [float] NULL,
	[BMERecentAppointment] [float] NULL,
	[BMELongTermAppointment] [float] NULL,
	[ESRSIP] [float] NULL,
	[GeneralLedgerEst] [float] NULL,
	[ESRSIPBand5] [float] NULL,
	[GeneralLedgerEstBand5] [float] NULL,
 CONSTRAINT [PK_HRSummary] PRIMARY KEY CLUSTERED 
(
	[OrganisationCode] ASC,
	[CensusDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


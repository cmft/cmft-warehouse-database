﻿CREATE TABLE [RAD].[ExamBase] (
    [ExamCode]                             VARCHAR (100) NOT NULL,
    [Exam]                                 VARCHAR (100) NULL,
    [AreaCode]                             VARCHAR (100) NULL,
    [Area]                                 VARCHAR (100) NULL,
    [ARSACLimit]                           VARCHAR (100) NULL,
    [DiagnosticWaitingTimesReportableFlag] VARCHAR (100) NULL,
    [ExamEndDate]                          SMALLDATETIME NULL,
    [InterventionalProcedureFlag]          VARCHAR (100) NULL,
    [KornerBand]                           VARCHAR (100) NULL,
    [KornerCode]                           VARCHAR (100) NULL,
    [KornerValue]                          VARCHAR (100) NULL,
    [KornerWeight]                         VARCHAR (100) NULL,
    [QueryLMPFlag]                         VARCHAR (100) NULL,
    [ModalityCode]                         VARCHAR (100) NULL,
    [NumberOfExams]                        VARCHAR (100) NULL,
    [ExamRealFlag]                         VARCHAR (100) NULL,
    [IntendedExamDuration]                 VARCHAR (100) NULL,
    [NuclearWasteCreated]                  VARCHAR (100) NULL,
    CONSTRAINT [PK_ExamBase_2] PRIMARY KEY CLUSTERED ([ExamCode] ASC)
);


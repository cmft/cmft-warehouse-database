﻿CREATE TABLE [RAD].[LocationBase] (
    [LocationCode]        VARCHAR (50) NOT NULL,
    [Location]            VARCHAR (50) NULL,
    [EndDate]             DATETIME     NULL,
    [SiteCode]            VARCHAR (50) NULL,
    [PatientTypeCode]     VARCHAR (50) NULL,
    [RequestCategoryCode] VARCHAR (50) NULL,
    [ReferralSourceCode]  VARCHAR (50) NULL
);


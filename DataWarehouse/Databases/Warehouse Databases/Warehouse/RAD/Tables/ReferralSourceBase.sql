﻿CREATE TABLE [RAD].[ReferralSourceBase] (
    [ReferralSourceCode]     VARCHAR (200) NOT NULL,
    [ReferralSource]         VARCHAR (200) NULL,
    [EndDate]                DATETIME      NULL,
    [Address1]               VARCHAR (200) NULL,
    [Address2]               VARCHAR (200) NULL,
    [Address3]               VARCHAR (200) NULL,
    [Address4]               VARCHAR (200) NULL,
    [Address5]               VARCHAR (200) NULL,
    [ClusterCode]            VARCHAR (200) NULL,
    [Courier]                VARCHAR (200) NULL,
    [NationalCode]           VARCHAR (200) NULL,
    [PCTCode]                VARCHAR (200) NULL,
    [PostCode1]              VARCHAR (200) NULL,
    [PostCode2]              VARCHAR (200) NULL,
    [SHACode]                VARCHAR (200) NULL,
    [Phone]                  VARCHAR (200) NULL,
    [ReferralSourceTypeCode] VARCHAR (200) NULL,
    CONSTRAINT [PK_ReferralSourceBase_2] PRIMARY KEY CLUSTERED ([ReferralSourceCode] ASC)
);


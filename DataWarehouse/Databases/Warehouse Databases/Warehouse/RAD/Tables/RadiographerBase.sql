﻿CREATE TABLE [RAD].[RadiographerBase] (
    [RadiographerCode] VARCHAR (50) NOT NULL,
    [Radiographer]     VARCHAR (50) NULL,
    [EndDate]          DATETIME     NULL,
    [UserID]           VARCHAR (50) NULL,
    CONSTRAINT [PK_RadiographerBase] PRIMARY KEY CLUSTERED ([RadiographerCode] ASC)
);


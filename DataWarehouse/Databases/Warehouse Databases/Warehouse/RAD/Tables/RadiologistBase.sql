﻿CREATE TABLE [RAD].[RadiologistBase] (
    [RadiologistCode]     VARCHAR (50) NOT NULL,
    [Radiologist]         VARCHAR (50) NULL,
    [EndDate]             DATETIME     NULL,
    [UserID]              VARCHAR (50) NULL,
    [RadiologistTypeCode] VARCHAR (2)  NULL,
    CONSTRAINT [PK_RadiologistBase] PRIMARY KEY CLUSTERED ([RadiologistCode] ASC),
    CONSTRAINT [FK_RadiologistBase_RadiologistBase] FOREIGN KEY ([RadiologistTypeCode]) REFERENCES [RAD].[RadiologistType] ([RadiologistTypeCode])
);


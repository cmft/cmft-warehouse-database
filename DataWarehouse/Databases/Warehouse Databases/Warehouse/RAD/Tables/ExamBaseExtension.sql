﻿CREATE TABLE [RAD].[ExamBaseExtension] (
    [ExamCode]                 VARCHAR (20) NOT NULL,
    [DiagnosticReportLineCode] VARCHAR (10) NULL,
    [WLRequiredForReport]      CHAR (1)     NULL,
    [ExaminationLevel]         CHAR (1)     NULL,
    [ReportLineCode]           VARCHAR (5)  NULL,
    CONSTRAINT [PK_ExamBaseExtension] PRIMARY KEY CLUSTERED ([ExamCode] ASC)
);


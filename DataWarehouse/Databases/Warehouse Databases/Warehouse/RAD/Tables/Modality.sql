﻿CREATE TABLE [RAD].[Modality] (
    [ModalityCode] VARCHAR (20) NOT NULL,
    [Modality]     VARCHAR (50) NULL,
    CONSTRAINT [PK_Modality] PRIMARY KEY CLUSTERED ([ModalityCode] ASC)
);


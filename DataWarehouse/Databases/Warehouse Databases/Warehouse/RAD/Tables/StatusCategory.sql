﻿CREATE TABLE [RAD].[StatusCategory] (
    [StatusCategoryCode] VARCHAR (50) NOT NULL,
    [StatusCategory]     VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_StatusCategory] PRIMARY KEY CLUSTERED ([StatusCategoryCode] ASC)
);


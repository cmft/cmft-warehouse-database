﻿CREATE TABLE [RAD].[UserBase] (
    [UserID]               VARCHAR (50)  NOT NULL,
    [UserName]             VARCHAR (50)  NULL,
    [LoginID]              VARCHAR (50)  NULL,
    [CreationTime]         DATETIME      NULL,
    [LogicallyDeletedFlag] VARCHAR (50)  NULL,
    [Level]                VARCHAR (50)  NULL,
    [ModifiedTime]         DATETIME      NULL,
    [UserAccessLevel]      VARCHAR (500) NULL,
    [ValidFrom]            DATETIME      NULL,
    [ValidTo]              DATETIME      NULL,
    [ModifiedDate]         DATETIME      NULL,
    [SiteCode]             VARCHAR (50)  NULL,
    [SpineRolesDate]       DATETIME      NULL,
    CONSTRAINT [PK_UserBase] PRIMARY KEY CLUSTERED ([UserID] ASC)
);


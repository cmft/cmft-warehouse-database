﻿CREATE TABLE [RAD].[WaitCategory] (
    [WaitCategoryCode] VARCHAR (5)  NOT NULL,
    [WaitCategory]     VARCHAR (50) NULL,
    CONSTRAINT [PK_WaitCategory_1] PRIMARY KEY CLUSTERED ([WaitCategoryCode] ASC)
);


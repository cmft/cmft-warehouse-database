﻿CREATE TABLE [RAD].[AuditYearMonth] (
    [YearMonth]     INT            NULL,
    [EventCount]    INT            NULL,
    [ExamCount]     INT            NULL,
    [StatusCount]   INT            NULL,
    [ReportCount]   INT            NULL,
    [SnapshotTime]  DATETIME2 (0)  NULL,
    [CreatedByWhom] NVARCHAR (128) NULL
);


﻿CREATE TABLE [RAD].[ReferrerBase] (
    [ReferrerCode]     VARCHAR (100) NOT NULL,
    [Referrer]         VARCHAR (100) NULL,
    [Initials]         VARCHAR (100) NULL,
    [StartDate]        DATETIME      NULL,
    [EndDate]          DATETIME      NULL,
    [LeadClinician]    VARCHAR (100) NULL,
    [NationalCode]     VARCHAR (100) NULL,
    [Specialties]      VARCHAR (100) NULL,
    [ReferrerTypeCode] VARCHAR (100) NULL,
    [SendEDI]          VARCHAR (50)  NULL,
    CONSTRAINT [PK_ReferrerBase_1] PRIMARY KEY CLUSTERED ([ReferrerCode] ASC)
);


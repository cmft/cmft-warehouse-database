﻿CREATE TABLE [RAD].[Status] (
    [EncounterRecno]              INT            IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]              VARCHAR (50)   NOT NULL,
    [ExamSourceUniqueID]          VARCHAR (50)   NOT NULL,
    [EventSourceUniqueID]         VARCHAR (50)   NOT NULL,
    [SourcePatientNo]             VARCHAR (50)   NOT NULL,
    [SiteCode]                    VARCHAR (50)   NULL,
    [ExamCode]                    VARCHAR (50)   NULL,
    [CurrentStatusFlag]           VARCHAR (50)   NULL,
    [StatusCode]                  VARCHAR (50)   NULL,
    [StatusCategoryCode]          VARCHAR (50)   NULL,
    [StatusTypeCode]              VARCHAR (50)   NULL,
    [StatusDate]                  DATETIME       NULL,
    [StatusTime]                  DATETIME       NULL,
    [StatusCodeAtEndDate]         VARCHAR (50)   NULL,
    [StatusCategoryCodeAtEndDate] VARCHAR (50)   NULL,
    [StatysTypeCodeAtEndDate]     VARCHAR (50)   NULL,
    [StartDate]                   DATETIME       NULL,
    [StartTime]                   DATETIME       NULL,
    [EndDate]                     DATETIME       NULL,
    [EndTime]                     DATETIME       NULL,
    [LogicalDeleteFlag]           VARCHAR (50)   NULL,
    [Description]                 VARCHAR (4000) NULL,
    [OtherCode]                   VARCHAR (50)   NULL,
    [UserID]                      VARCHAR (50)   NULL,
    [Created]                     SMALLDATETIME  NULL,
    [Updated]                     SMALLDATETIME  NULL,
    [ByWhom]                      VARCHAR (50)   NULL,
    CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_RAD_Status_01]
    ON [RAD].[Status]([CurrentStatusFlag] ASC)
    INCLUDE([EncounterRecno], [ExamSourceUniqueID], [StatusCode]);


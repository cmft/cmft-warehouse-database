﻿CREATE TABLE [RAD].[RoomBase] (
    [RoomCode]       VARCHAR (50)  NOT NULL,
    [Room]           VARCHAR (200) NULL,
    [DepartmentCode] VARCHAR (50)  NULL,
    [SiteCode]       VARCHAR (50)  NOT NULL,
    [DosageTypeCode] VARCHAR (50)  NULL,
    [EndDate]        DATETIME      NULL,
    [ModalityCode]   VARCHAR (50)  NULL
);


﻿CREATE TABLE [RAD].[SpecialtyBase] (
    [SpecialtyCode]  VARCHAR (50)  NOT NULL,
    [Specialty]      VARCHAR (200) NULL,
    [EndDate]        DATETIME      NULL,
    [SpecialtyGroup] VARCHAR (50)  NULL,
    CONSTRAINT [PK_SpecialtyBase] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


﻿CREATE TABLE [RAD].[StatusBase] (
    [StatusCode]         VARCHAR (60) NOT NULL,
    [Status]             VARCHAR (60) NULL,
    [StatusLong]         VARCHAR (60) NULL,
    [StatusTypeCode]     VARCHAR (50) NULL,
    [StatusCategoryCode] VARCHAR (50) NULL,
    [DefaultFlag]        VARCHAR (60) NULL,
    [EndDate]            DATETIME     NULL,
    CONSTRAINT [PK_StatusBase_2] PRIMARY KEY CLUSTERED ([StatusCode] ASC)
);


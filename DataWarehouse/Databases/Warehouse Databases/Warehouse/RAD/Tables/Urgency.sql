﻿CREATE TABLE [RAD].[Urgency] (
    [UrgencyCode]         VARCHAR (20)  NOT NULL,
    [Urgency]             VARCHAR (255) NULL,
    [NationalUrgencyCode] CHAR (10)     NULL,
    CONSTRAINT [PK_Urgency] PRIMARY KEY CLUSTERED ([UrgencyCode] ASC)
);


﻿CREATE TABLE [RAD].[RadiologistType] (
    [RadiologistTypeCode] VARCHAR (2)  NOT NULL,
    [RadiologistType]     VARCHAR (50) NULL,
    CONSTRAINT [PK_RadiologistType] PRIMARY KEY CLUSTERED ([RadiologistTypeCode] ASC)
);


﻿CREATE TABLE [OCM].[CodedResult] (
    [ResultRecno]     INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]  VARCHAR (50)  NOT NULL,
    [SourcePatientNo] INT           NOT NULL,
    [SourceEpisodeNo] INT           NOT NULL,
    [SequenceNo]      INT           NOT NULL,
    [TransactionNo]   INT           NOT NULL,
    [SessionNo]       INT           NOT NULL,
    [ResultCode]      VARCHAR (4)   NOT NULL,
    [Result]          VARCHAR (9)   NULL,
    [ResultDate]      DATE          NULL,
    [ResultTime]      SMALLDATETIME NULL,
    [RangeIndicator]  SMALLINT      NULL,
    [EnteredBy]       VARCHAR (4)   NULL,
    [StatusCode]      SMALLINT      NULL,
    [Comment1]        VARCHAR (60)  NULL,
    [Comment2]        VARCHAR (60)  NULL,
    [Comment3]        VARCHAR (60)  NULL,
    [Comment4]        VARCHAR (60)  NULL,
    [Comment5]        VARCHAR (60)  NULL,
    [Comment6]        VARCHAR (60)  NULL,
    [Created]         DATETIME      NULL,
    [Updated]         DATETIME      NULL,
    [ByWhom]          VARCHAR (50)  NULL,
    CONSTRAINT [PK__Result__613D576F1F705E07] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_Result_SourcePatientNo_SourceEpisodeNo]
    ON [OCM].[CodedResult]([SourcePatientNo] ASC, [SourceEpisodeNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CodedResult_2]
    ON [OCM].[CodedResult]([ResultCode] ASC)
    INCLUDE([Result], [ResultTime], [SessionNo], [SourcePatientNo]);


GO
CREATE NONCLUSTERED INDEX [IX_CodedResult_1]
    ON [OCM].[CodedResult]([SourcePatientNo] ASC, [ResultCode] ASC, [ResultDate] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_OCMCR_RC-RES-RD]
    ON [OCM].[CodedResult]([ResultCode] ASC, [Result] ASC, [ResultDate] ASC)
    INCLUDE([ResultRecno], [SourcePatientNo], [SourceEpisodeNo], [ResultTime]);


GO
--GRANT SELECT
--    ON OBJECT::[OCM].[CodedResult] TO [scc]
--    AS [dbo];


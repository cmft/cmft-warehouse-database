﻿CREATE TABLE [APC].[Encounter] (
    [EncounterRecno]                 INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]                 VARCHAR (50)  NOT NULL,
    [SourcePatientNo]                VARCHAR (20)  NULL,
    [SourceSpellNo]                  VARCHAR (20)  NOT NULL,
    [SourceEncounterNo]              VARCHAR (20)  NOT NULL,
    [ProviderSpellNo]                VARCHAR (50)  NULL,
    [PatientTitle]                   VARCHAR (10)  NULL,
    [PatientForename]                VARCHAR (20)  NULL,
    [PatientSurname]                 VARCHAR (30)  NULL,
    [DateOfBirth]                    DATETIME      NULL,
    [DateOfDeath]                    SMALLDATETIME NULL,
    [SexCode]                        VARCHAR (1)   NULL,
    [NHSNumber]                      VARCHAR (17)  NULL,
    [Postcode]                       VARCHAR (8)   NULL,
    [PatientAddress1]                VARCHAR (25)  NULL,
    [PatientAddress2]                VARCHAR (25)  NULL,
    [PatientAddress3]                VARCHAR (25)  NULL,
    [PatientAddress4]                VARCHAR (25)  NULL,
    [DHACode]                        VARCHAR (3)   NULL,
    [EthnicOriginCode]               VARCHAR (4)   NULL,
    [MaritalStatusCode]              VARCHAR (1)   NULL,
    [ReligionCode]                   VARCHAR (4)   NULL,
    [DateOnWaitingList]              SMALLDATETIME NULL,
    [AdmissionDate]                  SMALLDATETIME NULL,
    [DischargeDate]                  SMALLDATETIME NULL,
    [EpisodeStartDate]               SMALLDATETIME NULL,
    [EpisodeEndDate]                 SMALLDATETIME NULL,
    [StartSiteCode]                  VARCHAR (10)  NULL,
    [StartWardTypeCode]              VARCHAR (10)  NULL,
    [EndSiteCode]                    VARCHAR (10)  NULL,
    [EndWardTypeCode]                VARCHAR (10)  NULL,
    [RegisteredGpCode]               VARCHAR (8)   NULL,
    [RegisteredGpPracticeCode]       VARCHAR (8)   NULL,
    [SiteCode]                       VARCHAR (5)   NULL,
    [AdmissionMethodCode]            VARCHAR (2)   NULL,
    [AdmissionSourceCode]            VARCHAR (2)   NULL,
    [PatientClassificationCode]      VARCHAR (1)   NULL,
    [ManagementIntentionCode]        VARCHAR (2)   NULL,
    [DischargeMethodCode]            VARCHAR (2)   NULL,
    [DischargeDestinationCode]       VARCHAR (2)   NULL,
    [AdminCategoryCode]              VARCHAR (3)   NULL,
    [ConsultantCode]                 VARCHAR (10)  NULL,
    [SpecialtyCode]                  VARCHAR (5)   NULL,
    [LastEpisodeInSpellIndicator]    VARCHAR (1)   NULL,
    [FirstRegDayOrNightAdmit]        VARCHAR (1)   NULL,
    [NeonatalLevelOfCare]            VARCHAR (1)   NULL,
    [IsWellBabyFlag]                 BIT           NULL,
    [PASHRGCode]                     VARCHAR (4)   NULL,
    [ClinicalCodingStatus]           VARCHAR (6)   NULL,
    [ClinicalCodingCompleteDate]     SMALLDATETIME NULL,
    [ClinicalCodingCompleteTime]     SMALLDATETIME NULL,
    [PrimaryDiagnosisCode]           VARCHAR (10)  NULL,
    [PrimaryDiagnosisDate]           SMALLDATETIME NULL,
    [SubsidiaryDiagnosisCode]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode1]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode2]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode3]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode4]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode5]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode6]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode7]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode8]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode9]        VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode10]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode11]       VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode12]       VARCHAR (10)  NULL,
    [PrimaryOperationCode]           VARCHAR (10)  NULL,
    [PrimaryOperationDate]           SMALLDATETIME NULL,
    [SecondaryOperationCode1]        VARCHAR (10)  NULL,
    [SecondaryOperationDate1]        SMALLDATETIME NULL,
    [SecondaryOperationCode2]        VARCHAR (10)  NULL,
    [SecondaryOperationDate2]        SMALLDATETIME NULL,
    [SecondaryOperationCode3]        VARCHAR (10)  NULL,
    [SecondaryOperationDate3]        SMALLDATETIME NULL,
    [SecondaryOperationCode4]        VARCHAR (10)  NULL,
    [SecondaryOperationDate4]        SMALLDATETIME NULL,
    [SecondaryOperationCode5]        VARCHAR (10)  NULL,
    [SecondaryOperationDate5]        SMALLDATETIME NULL,
    [SecondaryOperationCode6]        VARCHAR (10)  NULL,
    [SecondaryOperationDate6]        SMALLDATETIME NULL,
    [SecondaryOperationCode7]        VARCHAR (10)  NULL,
    [SecondaryOperationDate7]        SMALLDATETIME NULL,
    [SecondaryOperationCode8]        VARCHAR (10)  NULL,
    [SecondaryOperationDate8]        SMALLDATETIME NULL,
    [SecondaryOperationCode9]        VARCHAR (10)  NULL,
    [SecondaryOperationDate9]        SMALLDATETIME NULL,
    [SecondaryOperationCode10]       VARCHAR (10)  NULL,
    [SecondaryOperationDate10]       SMALLDATETIME NULL,
    [SecondaryOperationCode11]       VARCHAR (10)  NULL,
    [SecondaryOperationDate11]       SMALLDATETIME NULL,
    [OperationStatusCode]            VARCHAR (1)   NULL,
    [ContractSerialNo]               VARCHAR (6)   NULL,
    [CodingCompleteDate]             SMALLDATETIME NULL,
    [PurchaserCode]                  VARCHAR (10)  NULL,
    [ProviderCode]                   VARCHAR (10)  NULL,
    [EpisodeStartTime]               SMALLDATETIME NULL,
    [EpisodeEndTime]                 SMALLDATETIME NULL,
    [RegisteredGdpCode]              VARCHAR (8)   NULL,
    [EpisodicGpCode]                 VARCHAR (8)   NULL,
    [InterfaceCode]                  VARCHAR (4)   NULL,
    [CasenoteNumber]                 VARCHAR (20)  NULL,
    [NHSNumberStatusCode]            VARCHAR (3)   NULL,
    [AdmissionTime]                  SMALLDATETIME NULL,
    [DischargeTime]                  SMALLDATETIME NULL,
    [TransferFrom]                   VARCHAR (14)  NULL,
    [DistrictNo]                     VARCHAR (20)  NULL,
    [ExpectedLOS]                    INT           NULL,
    [MRSAFlag]                       VARCHAR (50)  NULL,
    [RTTPathwayID]                   VARCHAR (25)  NULL,
    [RTTPathwayCondition]            VARCHAR (20)  NULL,
    [RTTStartDate]                   SMALLDATETIME NULL,
    [RTTEndDate]                     SMALLDATETIME NULL,
    [RTTSpecialtyCode]               VARCHAR (10)  NULL,
    [RTTCurrentProviderCode]         VARCHAR (10)  NULL,
    [RTTCurrentStatusCode]           VARCHAR (10)  NULL,
    [RTTCurrentStatusDate]           SMALLDATETIME NULL,
    [RTTCurrentPrivatePatientFlag]   BIT           NULL,
    [RTTOverseasStatusFlag]          BIT           NULL,
    [RTTPeriodStatusCode]            VARCHAR (10)  NULL,
    [IntendedPrimaryOperationCode]   VARCHAR (10)  NULL,
    [Operation]                      VARCHAR (255) NULL,
    [Research1]                      VARCHAR (10)  NULL,
    [CancelledElectiveAdmissionFlag] VARCHAR (10)  NULL,
    [LocalAdminCategoryCode]         VARCHAR (10)  NULL,
    [EpisodicGpPracticeCode]         VARCHAR (10)  NULL,
    [PCTCode]                        VARCHAR (10)  NULL,
    [LocalityCode]                   VARCHAR (10)  NULL,
    [Created]                        DATETIME      NULL,
    [Updated]                        DATETIME      NULL,
    [ByWhom]                         VARCHAR (50)  NULL,
    [ClockStartDate]                 SMALLDATETIME NULL,
    [BreachDate]                     SMALLDATETIME NULL,
    [RTTBreachDate]                  SMALLDATETIME NULL,
    [NationalBreachDate]             SMALLDATETIME NULL,
    [NationalDiagnosticBreachDate]   SMALLDATETIME NULL,
    [SocialSuspensionDays]           INT           NULL,
    [BreachTypeCode]                 VARCHAR (10)  NULL,
    [ReferralEncounterRecno]         INT           NULL,
    [OperationDetailSourceUniqueID]  INT           NULL,
    [Research2]                      VARCHAR (50)  NULL,
    [AdmissionDivisionCode]          VARCHAR (5)   NULL,
    [DischargeDivisionCode]          VARCHAR (5)   NULL,
    [PatientCategoryCode]            VARCHAR (5)   NULL,
    [TheatrePatientBookingKey]       VARCHAR (50)  NULL,
    [StartDirectorateCode]           VARCHAR (5)   NULL,
    [EndDirectorateCode]             VARCHAR (5)   NULL,
    [ResidencePCTCode]               VARCHAR (10)  NULL,
    [DischargeReadyDate]             SMALLDATETIME NULL,
    [DischargeWaitReasonCode]        VARCHAR (8)   NULL,
    [ReferredByCode]                 VARCHAR (3)   NULL,
    [ReferrerCode]                   VARCHAR (8)   NULL,
    [DecidedToAdmitDate]             SMALLDATETIME NULL,
    [WaitingListCode]                VARCHAR (10)  NULL,
    [ISTAdmissionSpecialtyCode]      VARCHAR (5)   NULL,
    [ISTAdmissionDemandTime]         SMALLDATETIME NULL,
    [ISTDischargeTime]               SMALLDATETIME NULL,
    [FirstEpisodeInSpellIndicator]   VARCHAR (1)   NULL,
    [CarerSupportIndicator]          VARCHAR (3)   NULL,
    [PseudoPostCode]                 VARCHAR (10)  NULL,
    [SecondaryDiagnosisCode13]       VARCHAR (10)  NULL,
    [CCGCode]                        VARCHAR (10)  NULL,
    [PostcodeAtDischarge]            VARCHAR (8)   NULL,
    [GpCodeAtDischarge]              VARCHAR (8)   NULL,
    [GpPracticeCodeAtDischarge]      VARCHAR (8)   NULL,
    CONSTRAINT [PK_Encounter] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC),
    CONSTRAINT [IX_Encounter] UNIQUE NONCLUSTERED ([EncounterRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_4]
    ON [APC].[Encounter]([AdmissionTime] ASC, [EncounterRecno] ASC, [SourceUniqueID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APC_Encounter_1]
    ON [APC].[Encounter]([DischargeDate] ASC, [EpisodeEndTime] ASC)
    INCLUDE([AdmissionDate], [AdmissionDivisionCode], [AdmissionMethodCode], [AdmissionTime], [CasenoteNumber], [DateOfBirth], [DischargeTime], [EpisodeStartTime], [ManagementIntentionCode], [PatientCategoryCode], [ProviderSpellNo], [SiteCode], [SourceUniqueID], [SpecialtyCode], [StartWardTypeCode]);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_6]
    ON [APC].[Encounter]([ProviderSpellNo] ASC, [DischargeDate] ASC, [EpisodeEndTime] ASC, [AdmissionDate] ASC, [EpisodeStartTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APC_Encounter_99]
    ON [APC].[Encounter]([NHSNumber] ASC, [CasenoteNumber] ASC, [PatientSurname] ASC, [PatientForename] ASC, [SexCode] ASC, [DateOfBirth] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_APCEncounter_SourcePatientNo_SourceSpellNo_NULLEpisodeEndDate]
    ON [APC].[Encounter]([SourcePatientNo] ASC, [SourceSpellNo] ASC) WHERE ([EpisodeEndDate] IS NULL);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_DistrictNo]
    ON [APC].[Encounter]([DistrictNo] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_APCEnc_DateWL_IncludeCoverForPreadmit]
    ON [APC].[Encounter]([DateOnWaitingList] ASC)
    INCLUDE([EncounterRecno], [SourceUniqueID], [SourcePatientNo], [SourceSpellNo], [SourceEncounterNo], [ProviderSpellNo], [PatientTitle], [PatientForename], [PatientSurname], [DateOfBirth], [DateOfDeath], [SexCode], [NHSNumber], [Postcode], [PatientAddress1], [PatientAddress2], [PatientAddress3], [PatientAddress4], [DHACode], [EthnicOriginCode], [MaritalStatusCode], [ReligionCode], [AdmissionDate], [DischargeDate], [EpisodeStartDate], [EpisodeEndDate], [StartSiteCode], [StartWardTypeCode], [EndSiteCode], [EndWardTypeCode], [RegisteredGpCode], [RegisteredGpPracticeCode], [SiteCode], [AdmissionMethodCode], [AdmissionSourceCode], [PatientClassificationCode], [ManagementIntentionCode], [DischargeMethodCode], [DischargeDestinationCode], [AdminCategoryCode], [ConsultantCode], [SpecialtyCode], [LastEpisodeInSpellIndicator], [FirstRegDayOrNightAdmit], [NeonatalLevelOfCare], [IsWellBabyFlag], [PASHRGCode], [ClinicalCodingStatus], [ClinicalCodingCompleteDate], [ClinicalCodingCompleteTime], [PrimaryDiagnosisCode], [PrimaryDiagnosisDate], [SubsidiaryDiagnosisCode], [SecondaryDiagnosisCode1], [SecondaryDiagnosisCode2], [SecondaryDiagnosisCode3], [SecondaryDiagnosisCode4], [SecondaryDiagnosisCode5], [SecondaryDiagnosisCode6], [SecondaryDiagnosisCode7], [SecondaryDiagnosisCode8], [SecondaryDiagnosisCode9], [SecondaryDiagnosisCode10], [SecondaryDiagnosisCode11], [SecondaryDiagnosisCode12], [PrimaryOperationCode], [PrimaryOperationDate], [SecondaryOperationCode1], [SecondaryOperationDate1], [SecondaryOperationCode2], [SecondaryOperationDate2], [SecondaryOperationCode3], [SecondaryOperationDate3], [SecondaryOperationCode4], [SecondaryOperationDate4], [SecondaryOperationCode5], [SecondaryOperationDate5], [SecondaryOperationCode6], [SecondaryOperationDate6], [SecondaryOperationCode7], [SecondaryOperationDate7], [SecondaryOperationCode8], [SecondaryOperationDate8], [SecondaryOperationCode9], [SecondaryOperationDate9], [SecondaryOperationCode10], [SecondaryOperationDate10], [SecondaryOperationCode11], [SecondaryOperationDate11], [OperationStatusCode], [ContractSerialNo], [CodingCompleteDate], [PurchaserCode], [ProviderCode], [EpisodeStartTime], [EpisodeEndTime], [RegisteredGdpCode], [EpisodicGpCode], [InterfaceCode], [CasenoteNumber], [NHSNumberStatusCode], [AdmissionTime], [DischargeTime], [TransferFrom], [DistrictNo], [ExpectedLOS], [MRSAFlag], [RTTPathwayID], [RTTPathwayCondition], [RTTStartDate], [RTTEndDate], [RTTSpecialtyCode], [RTTCurrentProviderCode], [RTTCurrentStatusCode], [RTTCurrentStatusDate], [RTTCurrentPrivatePatientFlag], [RTTOverseasStatusFlag], [RTTPeriodStatusCode], [IntendedPrimaryOperationCode], [Operation], [Research1], [CancelledElectiveAdmissionFlag], [LocalAdminCategoryCode], [EpisodicGpPracticeCode], [PCTCode], [LocalityCode], [Created], [Updated], [ByWhom], [ClockStartDate], [BreachDate], [RTTBreachDate], [NationalBreachDate], [NationalDiagnosticBreachDate], [SocialSuspensionDays], [BreachTypeCode], [ReferralEncounterRecno], [OperationDetailSourceUniqueID], [Research2], [AdmissionDivisionCode], [DischargeDivisionCode], [PatientCategoryCode], [TheatrePatientBookingKey], [StartDirectorateCode], [EndDirectorateCode], [ResidencePCTCode], [DischargeReadyDate], [DischargeWaitReasonCode], [ReferredByCode], [ReferrerCode], [DecidedToAdmitDate], [WaitingListCode], [ISTAdmissionSpecialtyCode], [ISTAdmissionDemandTime], [ISTDischargeTime], [FirstEpisodeInSpellIndicator], [CarerSupportIndicator]);


GO
CREATE NONCLUSTERED INDEX [Idx_APC_Enc_AD_PCC]
    ON [APC].[Encounter]([AdmissionDate] ASC, [PatientCategoryCode] ASC)
    INCLUDE([SourcePatientNo], [SourceEncounterNo], [PrimaryOperationCode], [EpisodeStartTime], [AdmissionTime]);


GO
CREATE NONCLUSTERED INDEX [INDEX_APC_epiStartDate_SourcePatientno]
    ON [APC].[Encounter]([EpisodeStartDate] ASC)
    INCLUDE([SourcePatientNo]);


GO
CREATE NONCLUSTERED INDEX [INDEX_APC_epiEndDate_SourcePatientno]
    ON [APC].[Encounter]([EpisodeEndDate] ASC)
    INCLUDE([SourcePatientNo]);


GO
CREATE NONCLUSTERED INDEX [IX_APCEncounter_1]
    ON [APC].[Encounter]([LastEpisodeInSpellIndicator] ASC, [DischargeDate] ASC)
    INCLUDE([EncounterRecno], [ProviderSpellNo], [ConsultantCode], [SpecialtyCode]);


GO
CREATE NONCLUSTERED INDEX [IX_APCEncounter_2]
    ON [APC].[Encounter]([FirstEpisodeInSpellIndicator] ASC)
    INCLUDE([ProviderSpellNo], [AdmissionDate], [DischargeDate], [AdmissionMethodCode], [ManagementIntentionCode]);


GO
CREATE NONCLUSTERED INDEX [IX_APCEncounter_3]
    ON [APC].[Encounter]([FirstEpisodeInSpellIndicator] ASC)
    INCLUDE([EncounterRecno], [ProviderSpellNo], [AdmissionDate], [ConsultantCode], [SpecialtyCode], [StartDirectorateCode]);


GO
CREATE NONCLUSTERED INDEX [IX_APCEncounter_4]
    ON [APC].[Encounter]([LastEpisodeInSpellIndicator] ASC, [DischargeDate] ASC)
    INCLUDE([EncounterRecno], [ProviderSpellNo], [ConsultantCode], [SpecialtyCode], [StartDirectorateCode]);


GO
CREATE NONCLUSTERED INDEX [IX_APCE_SPN_DD_SSN]
    ON [APC].[Encounter]([SourcePatientNo] ASC, [DischargeDate] ASC, [SourceSpellNo] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_Encounter_AdmissionMethodCode_FirstEpisodeInSpellIndicator_includes]
    ON [APC].[Encounter]([AdmissionMethodCode] ASC, [FirstEpisodeInSpellIndicator] ASC)
    INCLUDE([EncounterRecno], [AdmissionTime], [DistrictNo]) WITH (FILLFACTOR = 100);


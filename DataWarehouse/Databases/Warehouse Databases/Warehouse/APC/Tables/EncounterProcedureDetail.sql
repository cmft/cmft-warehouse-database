﻿CREATE TABLE [APC].[EncounterProcedureDetail] (
    [EncounterRecno]                INT         NOT NULL,
    [ProcedureDetailSourceUniqueID] NUMERIC (9) NOT NULL,
    CONSTRAINT [PK_EncounterProcedureDetail] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [ProcedureDetailSourceUniqueID] ASC)
);


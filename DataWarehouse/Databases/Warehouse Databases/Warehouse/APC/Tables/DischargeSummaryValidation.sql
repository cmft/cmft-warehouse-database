﻿CREATE TABLE [APC].[DischargeSummaryValidation] (
    [UniqueKey]       VARCHAR (52) NULL,
    [EncounterRecno]  INT          NOT NULL,
    [DirectorateCode] VARCHAR (5)  NULL,
    [SpecialtyCode]   VARCHAR (5)  NULL
);


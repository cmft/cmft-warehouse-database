﻿CREATE TABLE [APC].[WardStay] (
    [SourceUniqueID]    VARCHAR (50)  NOT NULL,
    [SourcePatientNo]   VARCHAR (20)  NOT NULL,
    [SourceSpellNo]     VARCHAR (20)  NOT NULL,
    [ProviderSpellNo]   VARCHAR (50)  NOT NULL,
    [StartDate]         SMALLDATETIME NOT NULL,
    [EndDate]           SMALLDATETIME NULL,
    [StartTime]         SMALLDATETIME NULL,
    [EndTime]           SMALLDATETIME NULL,
    [SiteCode]          VARCHAR (5)   NULL,
    [WardCode]          VARCHAR (10)  NULL,
    [StartActivityCode] VARCHAR (10)  NULL,
    [EndActivityCode]   VARCHAR (10)  NULL,
    [Created]           DATETIME      NOT NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NOT NULL,
    [EncounterRecno]    INT           IDENTITY (1, 1) NOT NULL,
    [InterfaceCode]     VARCHAR (20)  NULL,
    CONSTRAINT [PK_WardStay] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_WardStay]
    ON [APC].[WardStay]([ProviderSpellNo] ASC, [WardCode] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_WardStay_1]
    ON [APC].[WardStay]([EndDate] ASC)
    INCLUDE([SourcePatientNo], [WardCode]);


GO
CREATE NONCLUSTERED INDEX [IX_WardStay_SourcePatientNo]
    ON [APC].[WardStay]([SourcePatientNo] ASC, [SourceSpellNo] ASC)
    INCLUDE([StartDate], [EndDate], [StartTime], [EndTime], [SiteCode], [WardCode]);


GO
CREATE NONCLUSTERED INDEX [IX_APCWardStay_2]
    ON [APC].[WardStay]([EncounterRecno] ASC, [EndDate] ASC)
    INCLUDE([StartTime], [EndTime], [SiteCode], [WardCode]);


GO
CREATE NONCLUSTERED INDEX [Idx_WardStay_ED_WC_i_SPN_ET]
    ON [APC].[WardStay]([EndDate] ASC, [WardCode] ASC)
    INCLUDE([SourcePatientNo], [EndTime]);


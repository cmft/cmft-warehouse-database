﻿CREATE TABLE [APC].[Operation] (
    [SourceUniqueID]    VARCHAR (50)  NOT NULL,
    [SourceSpellNo]     VARCHAR (20)  NULL,
    [SourcePatientNo]   VARCHAR (20)  NULL,
    [ProviderSpellNo]   VARCHAR (50)  NULL,
    [SourceEncounterNo] VARCHAR (20)  NULL,
    [SequenceNo]        SMALLINT      NULL,
    [OperationCode]     VARCHAR (10)  NULL,
    [OperationDate]     SMALLDATETIME NULL,
    [ConsultantCode]    VARCHAR (10)  NULL,
    [APCSourceUniqueID] VARCHAR (50)  NULL,
    [Created]           DATETIME      NOT NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Procedure_1] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Operation_1]
    ON [APC].[Operation]([ProviderSpellNo] ASC, [SourceEncounterNo] ASC, [SequenceNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Operation]
    ON [APC].[Operation]([APCSourceUniqueID] ASC, [SequenceNo] ASC);


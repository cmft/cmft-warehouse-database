﻿CREATE TABLE [APC].[EncounterResus] (
    [ResusEncounterRecno] INT NOT NULL,
    [APCEncounterRecno]   INT NOT NULL,
    CONSTRAINT [PK_EncounterResuscitation] PRIMARY KEY CLUSTERED ([ResusEncounterRecno] ASC, [APCEncounterRecno] ASC)
);


﻿CREATE TABLE [APC].[AugmentedCarePeriod] (
    [EncounterRecno]                     INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]                     VARCHAR (50) NULL,
    [SourcePatientNo]                    VARCHAR (9)  NULL,
    [SourceSpellNo]                      VARCHAR (9)  NULL,
    [ProviderSpellNo]                    VARCHAR (50) NULL,
    [ConsultantCode]                     VARCHAR (6)  NULL,
    [AcpDisposalCode]                    VARCHAR (2)  NULL,
    [LocalIdentifier]                    VARCHAR (8)  NULL,
    [LocationCode]                       VARCHAR (4)  NULL,
    [OutcomeIndicator]                   VARCHAR (2)  NULL,
    [Source]                             VARCHAR (2)  NULL,
    [SpecialtyCode]                      VARCHAR (4)  NULL,
    [Status]                             VARCHAR (11) NULL,
    [StartDate]                          DATE         NULL,
    [StartTime]                          DATETIME     NULL,
    [EndDate]                            DATE         NULL,
    [EndTime]                            DATETIME     NULL,
    [AdvancedRespiratorySystemIndicator] VARCHAR (3)  NULL,
    [BasicRespiratorySystemIndicator]    VARCHAR (3)  NULL,
    [CirculatorySystemIndicator]         VARCHAR (3)  NULL,
    [NeurologicalSystemIndicator]        VARCHAR (3)  NULL,
    [RenalSystemIndicator]               VARCHAR (3)  NULL,
    [NoOfSupportSystemsUsed]             VARCHAR (1)  NULL,
    [HighDependencyCareLevelDays]        VARCHAR (4)  NULL,
    [IntensiveCareLevelDays]             VARCHAR (4)  NULL,
    [PlannedAcpPeriodIndicator]          VARCHAR (3)  NULL,
    [ReviseByUserId]                     VARCHAR (3)  NULL,
    [ReviseTime]                         VARCHAR (16) NULL,
    [InterfaceCode]                      VARCHAR (20) NULL
);




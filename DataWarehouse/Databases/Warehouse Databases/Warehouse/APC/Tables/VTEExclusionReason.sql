﻿CREATE TABLE [APC].[VTEExclusionReason] (
    [VTEExclusionReasonCode] VARCHAR (20)  NOT NULL,
    [VTEExclusionReason]     VARCHAR (200) NOT NULL,
    PRIMARY KEY CLUSTERED ([VTEExclusionReasonCode] ASC)
);


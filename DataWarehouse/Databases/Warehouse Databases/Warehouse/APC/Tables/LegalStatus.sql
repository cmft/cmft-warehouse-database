﻿CREATE TABLE [APC].[LegalStatus] (
    [SourceUniqueID]               VARCHAR (38)  NOT NULL,
    [SourceSpellNo]                VARCHAR (13)  NOT NULL,
    [SourcePatientNo]              VARCHAR (9)   NOT NULL,
    [HospitalCode]                 VARCHAR (4)   NULL,
    [StartTime]                    DATETIME2 (7) NULL,
    [PsychiatricPatientStatusCode] VARCHAR (1)   NULL,
    [MentalCategoryCode]           VARCHAR (1)   NULL,
    [LegalStatusCode]              VARCHAR (2)   NULL,
    CONSTRAINT [PK_LegalStatus] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_LegalStatus]
    ON [APC].[LegalStatus]([SourcePatientNo] ASC, [SourceSpellNo] ASC, [StartTime] ASC);


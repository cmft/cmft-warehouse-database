﻿CREATE TABLE [APC].[InpatientCensus] (
    [SourceUniqueID]          VARCHAR (50)  NOT NULL,
    [SourcePatientNo]         VARCHAR (20)  NULL,
    [NHSNumber]               VARCHAR (20)  NULL,
    [CasenoteNumber]          VARCHAR (20)  NULL,
    [SourceSpellNo]           VARCHAR (50)  NULL,
    [ProviderSpellNo]         VARCHAR (20)  NULL,
    [DirectorateCode]         VARCHAR (5)   NULL,
    [SiteCode]                VARCHAR (10)  NULL,
    [WardCode]                VARCHAR (10)  NULL,
    [ConsultantCode]          VARCHAR (10)  NULL,
    [SpecialtyCode]           VARCHAR (10)  NULL,
    [SourceAdminCategoryCode] VARCHAR (10)  NULL,
    [ActivityInCode]          VARCHAR (50)  NULL,
    [AdmissionTime]           SMALLDATETIME NULL,
    [AdmissionDate]           SMALLDATETIME NULL,
    [CensusDate]              SMALLDATETIME NOT NULL,
    [InterfaceCode]           VARCHAR (5)   NULL,
    [Created]                 DATETIME      NOT NULL,
    [ByWhom]                  VARCHAR (150) NULL,
    CONSTRAINT [PK_InpatientCensus] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC, [CensusDate] ASC)
);


﻿CREATE TABLE [APC].[LearningDisability] (
    [LearningDisabilityRecno]       INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]                INT          NOT NULL,
    [SourcePatientNo]               VARCHAR (8)  NULL,
    [SourceSpellNo]                 VARCHAR (5)  NULL,
    [AssessmentDate]                DATE         NULL,
    [AssessmentTime]                DATETIME     NULL,
    [KnownLearningDisability]       BIT          NULL,
    [ReasonableAdjustmentsCarePlan] BIT          NULL,
    [LDPassport]                    BIT          NULL,
    [WardCode]                      VARCHAR (10) NOT NULL,
    [InterfaceCode]                 VARCHAR (10) NOT NULL,
    [Created]                       DATETIME     NULL,
    [Updated]                       DATETIME     NULL,
    [ByWhom]                        VARCHAR (50) NULL,
    CONSTRAINT [PK__Learning__613D576F11581431] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


﻿CREATE TABLE [APC].[WaitingListActivity] (
    [SourceUniqueID]            VARCHAR (50)  NOT NULL,
    [ActivityTypeCode]          VARCHAR (10)  NULL,
    [ActivityDate]              SMALLDATETIME NULL,
    [ActivityTime]              SMALLDATETIME NOT NULL,
    [AdminCategoryCode]         VARCHAR (10)  NULL,
    [BookingTypeCode]           VARCHAR (10)  NULL,
    [CancelledBy]               VARCHAR (10)  NULL,
    [ConsultantCode]            VARCHAR (10)  NULL,
    [DeferralEndDate]           SMALLDATETIME NULL,
    [DeferralRevisedEndDate]    SMALLDATETIME NULL,
    [DiagnosticGroupCode]       VARCHAR (10)  NULL,
    [SourceEntityRecno]         VARCHAR (50)  NOT NULL,
    [SiteCode]                  VARCHAR (10)  NULL,
    [SourcePatientNo]           VARCHAR (10)  NOT NULL,
    [LastRevisionTime]          SMALLDATETIME NULL,
    [LastRevisionUser]          VARCHAR (10)  NULL,
    [PreviousActivityID]        VARCHAR (50)  NULL,
    [Reason]                    VARCHAR (50)  NULL,
    [Remark]                    VARCHAR (50)  NULL,
    [RemovalComment]            VARCHAR (50)  NULL,
    [RemovalReasonCode]         VARCHAR (10)  NULL,
    [SpecialtyCode]             VARCHAR (10)  NULL,
    [SuspensionReasonCode]      VARCHAR (10)  NULL,
    [TCIAcceptDate]             SMALLDATETIME NULL,
    [TCITime]                   SMALLDATETIME NULL,
    [TCIOfferDate]              SMALLDATETIME NULL,
    [PriorityCode]              VARCHAR (10)  NULL,
    [WaitingListCode]           VARCHAR (10)  NULL,
    [WardCode]                  VARCHAR (10)  NULL,
    [CharterCancelCode]         VARCHAR (10)  NULL,
    [CharterCancelDeferFlag]    VARCHAR (10)  NULL,
    [CharterCancel]             VARCHAR (100) NULL,
    [OpCancelledFlag]           VARCHAR (10)  NULL,
    [OpCancelled]               VARCHAR (50)  NULL,
    [PatientChoiceFlag]         VARCHAR (10)  NULL,
    [PatientChoice]             VARCHAR (50)  NULL,
    [WLSuspensionInitiatorCode] VARCHAR (10)  NULL,
    [WLSuspensionInitiator]     VARCHAR (50)  NULL,
    [Created]                   DATETIME      NULL,
    [Updated]                   DATETIME      NULL,
    [ByWhom]                    VARCHAR (50)  NULL,
    CONSTRAINT [PK_WaitingListActivity] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC),
    CONSTRAINT [FK_WaitingListActivity_WLActivityType] FOREIGN KEY ([ActivityTypeCode]) REFERENCES [WH].[WLActivityType] ([ActivityTypeCode])
);


GO
CREATE NONCLUSTERED INDEX [Idx_APCWLA_SPN_SERN_AT]
    ON [APC].[WaitingListActivity]([SourcePatientNo] ASC, [SourceEntityRecno] ASC, [ActivityTime] ASC) WHERE ([ActivityTypeCode] IN ('4', '5', '6'));


GO
CREATE NONCLUSTERED INDEX [Idx_APCWLA_ATC]
    ON [APC].[WaitingListActivity]([ActivityTypeCode] ASC)
    INCLUDE([ActivityTime], [SourceEntityRecno], [SourcePatientNo]);


GO
CREATE NONCLUSTERED INDEX [Idx_WaitingListActivity_AD]
    ON [APC].[WaitingListActivity]([ActivityDate] ASC)
    INCLUDE([ConsultantCode], [SourceEntityRecno], [SiteCode], [SourcePatientNo], [LastRevisionTime], [Reason], [SpecialtyCode], [WaitingListCode], [WardCode]);


GO
CREATE NONCLUSTERED INDEX [IX_APCWLA_SPN_AT]
    ON [APC].[WaitingListActivity]([SourcePatientNo] ASC, [ActivityTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_APCWLA_ATC_TCIT]
    ON [APC].[WaitingListActivity]([ActivityTypeCode] ASC, [TCITime] ASC)
    INCLUDE([ActivityDate], [ActivityTime], [SourceEntityRecno], [SourcePatientNo], [WardCode]);


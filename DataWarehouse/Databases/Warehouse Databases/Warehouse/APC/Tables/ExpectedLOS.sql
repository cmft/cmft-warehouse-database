﻿CREATE TABLE [APC].[ExpectedLOS] (
    [SourceUniqueID]          VARCHAR (50) NOT NULL,
    [LoadTime]                DATETIME     NULL,
    [LoadModifiedTime]        DATETIME     NULL,
    [SourceCreatedTime]       DATETIME     NULL,
    [EddCreatedTime]          DATETIME     NULL,
    [SourceSystem]            VARCHAR (10) NULL,
    [ModifiedFromSystem]      VARCHAR (10) NULL,
    [AdmissionTime]           DATETIME     NULL,
    [DischargeTime]           DATETIME     NULL,
    [SourcePatientNo]         VARCHAR (20) NULL,
    [SourceSpellNo]           VARCHAR (20) NULL,
    [SiteCode]                VARCHAR (10) NULL,
    [Ward]                    VARCHAR (10) NULL,
    [Specialty]               VARCHAR (5)  NULL,
    [Consultant]              VARCHAR (10) NULL,
    [ManagementIntentionCode] VARCHAR (2)  NULL,
    [CasenoteNumber]          VARCHAR (50) NULL,
    [ExpectedLOS]             INT          NULL,
    [EnteredWithin48hrs]      INT          NULL,
    [CreateEddDaysDuration]   INT          NULL,
    [DirectorateCode]         VARCHAR (5)  NULL,
    [AdmissionMethodCode]     VARCHAR (2)  NULL,
    [RTTPathwayCondition]     VARCHAR (50) NULL,
    [ArchiveFlag]             VARCHAR (1)  NULL,
    CONSTRAINT [PK_ExpectedLOS] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ExpectedLOS]
    ON [APC].[ExpectedLOS]([SourcePatientNo] ASC, [SourceSpellNo] ASC, [AdmissionTime] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_ExpectedLOS_ExpectedLOSAdmissionTime_SourcePatientNo_SourceSpellNo]
    ON [APC].[ExpectedLOS]([ExpectedLOS] ASC, [AdmissionTime] ASC, [SourcePatientNo] ASC, [SourceSpellNo] ASC);


GO
CREATE NONCLUSTERED INDEX [ix_ExpectedLOS_DischargeTime_Ward_ManagementIntentionCode_ArchiveFlag]
    ON [APC].[ExpectedLOS]([DischargeTime] ASC, [Ward] ASC, [ManagementIntentionCode] ASC, [ArchiveFlag] ASC)
    INCLUDE([EddCreatedTime], [AdmissionTime], [Specialty], [Consultant], [ExpectedLOS], [DirectorateCode]);


GO
CREATE NONCLUSTERED INDEX [ix_ExpectedLOS_DirectorateCodeDischargeTime]
    ON [APC].[ExpectedLOS]([DirectorateCode] ASC, [DischargeTime] ASC)
    INCLUDE([EddCreatedTime], [AdmissionTime], [Ward], [Specialty], [Consultant], [ManagementIntentionCode], [ExpectedLOS], [ArchiveFlag]);


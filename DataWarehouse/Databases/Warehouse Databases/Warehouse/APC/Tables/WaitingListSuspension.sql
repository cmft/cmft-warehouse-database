﻿CREATE TABLE [APC].[WaitingListSuspension] (
    [SuspensionRecno]      INT           IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]      VARCHAR (9)   NOT NULL,
    [SourceEncounterNo]    VARCHAR (9)   NOT NULL,
    [SuspensionStartDate]  SMALLDATETIME NULL,
    [SuspensionEndDate]    SMALLDATETIME NULL,
    [SuspensionReasonCode] VARCHAR (4)   NULL,
    [SuspensionReason]     VARCHAR (30)  NULL,
    [CensusDate]           SMALLDATETIME NULL,
    [SourceUniqueID]       VARCHAR (255) NULL,
    CONSTRAINT [PK_Suspension] PRIMARY KEY CLUSTERED ([SuspensionRecno] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [IX_WaitingListSuspension]
    ON [APC].[WaitingListSuspension]([SourcePatientNo] ASC, [SourceEncounterNo] ASC, [CensusDate] ASC);


﻿CREATE TABLE [APC].[DementiaOverrideMeasure] (
    [DistrictNo]        VARCHAR (20)  NOT NULL,
    [MeasureType]       VARCHAR (50)  NOT NULL,
    [SubmissionMonth]   DATETIME      NOT NULL,
    [NumeratorInclude]  BIT           NULL,
    [DenominatorRemove] BIT           NULL,
    [Comments]          VARCHAR (MAX) NULL,
    CONSTRAINT [PK_APC.DementiaOverrideMeasure] PRIMARY KEY CLUSTERED ([DistrictNo] ASC, [MeasureType] ASC, [SubmissionMonth] ASC)
);


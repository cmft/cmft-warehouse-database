﻿CREATE TABLE [APC].[MidnightBedState] (
    [SourceUniqueID]          VARCHAR (50)  NOT NULL,
    [SourcePatientNo]         VARCHAR (20)  NULL,
    [SourceSpellNo]           VARCHAR (50)  NULL,
    [ProviderSpellNo]         VARCHAR (20)  NULL,
    [SiteCode]                VARCHAR (10)  NULL,
    [WardCode]                VARCHAR (10)  NULL,
    [ConsultantCode]          VARCHAR (10)  NULL,
    [SpecialtyCode]           VARCHAR (10)  NULL,
    [SourceAdminCategoryCode] VARCHAR (10)  NULL,
    [ActivityInCode]          VARCHAR (50)  NULL,
    [AdmissionTime]           SMALLDATETIME NULL,
    [AdmissionDate]           SMALLDATETIME NULL,
    [CensusDate]              SMALLDATETIME NOT NULL,
    [InterfaceCode]           VARCHAR (3)   NULL,
    [Created]                 DATETIME      NOT NULL,
    [ByWhom]                  VARCHAR (50)  NULL,
    CONSTRAINT [PK_MidnightBedState] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC, [CensusDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MidnightBedState]
    ON [APC].[MidnightBedState]([SourcePatientNo] ASC, [CensusDate] ASC);


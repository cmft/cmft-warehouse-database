﻿CREATE TABLE [APC].[Dementia] (
    [DementiaRecno]                 INT          IDENTITY (1, 1) NOT NULL,
    [ProviderSpellNo]               VARCHAR (20) NOT NULL,
    [SpellID]                       INT          NOT NULL,
    [SourceUniqueID]                INT          NOT NULL,
    [SourcePatientNo]               VARCHAR (8)  NULL,
    [SourceSpellNo]                 VARCHAR (5)  NULL,
    [AdmissionWardCode]             VARCHAR (10) NULL,
    [RecordedDate]                  DATE         NULL,
    [RecordedTime]                  DATETIME     NOT NULL,
    [KnownToHaveDementia]           INT          NULL,
    [ForgetfulnessQuestionAnswered] INT          NULL,
    [AssessmentScore]               VARCHAR (60) NULL,
    [Investigation]                 INT          NULL,
    [AssessmentNotPerformedReason]  VARCHAR (60) NULL,
    [ReferralSentDate]              VARCHAR (10) NULL,
    [InterfaceCode]                 VARCHAR (6)  NOT NULL,
    [Created]                       DATETIME     NULL,
    [Updated]                       DATETIME     NULL,
    [ByWhom]                        VARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([DementiaRecno] ASC)
);


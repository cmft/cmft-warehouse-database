﻿CREATE TABLE [APC].[VTESpell] (
    [ProviderSpellNo]        VARCHAR (20) NOT NULL,
    [VTECategoryCode]        CHAR (1)     NULL,
    [VTEExclusionReasonCode] VARCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([ProviderSpellNo] ASC)
);


﻿CREATE TABLE [APC].[StaffingLevel] (
    [StaffingLevelRecno]       INT           IDENTITY (1, 1) NOT NULL,
    [DivisionCode]             VARCHAR (20)  NOT NULL,
    [WardCode]                 VARCHAR (20)  NOT NULL,
    [CensusDate]               DATE          NOT NULL,
    [ShiftID]                  INT           NOT NULL,
    [RegisteredNursePlan]      INT           NULL,
    [RegisteredNurseActual]    INT           NULL,
    [NonRegisteredNursePlan]   INT           NULL,
    [NonRegisteredNurseActual] INT           NULL,
    [Comments]                 VARCHAR (MAX) NULL,
    [SeniorComments]           VARCHAR (MAX) NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  NULL,
    CONSTRAINT [PK_APCStaffingLevel] PRIMARY KEY CLUSTERED ([DivisionCode] ASC, [WardCode] ASC, [CensusDate] ASC, [ShiftID] ASC)
);


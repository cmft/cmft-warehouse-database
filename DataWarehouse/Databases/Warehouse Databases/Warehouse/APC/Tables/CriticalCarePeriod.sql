﻿CREATE TABLE [APC].[CriticalCarePeriod] (
    [SourceUniqueID]                    VARCHAR (254) NOT NULL,
    [SourceSpellNo]                     VARCHAR (9)   NULL,
    [SourcePatientNo]                   VARCHAR (9)   NULL,
    [StartDate]                         SMALLDATETIME NULL,
    [StartTime]                         SMALLDATETIME NULL,
    [EndDate]                           SMALLDATETIME NULL,
    [EndTime]                           SMALLDATETIME NULL,
    [AdvancedCardiovascularSupportDays] SMALLINT      NULL,
    [AdvancedRespiratorySupportDays]    SMALLINT      NULL,
    [BasicCardiovascularSupportDays]    SMALLINT      NULL,
    [BasicRespiratorySupportDays]       SMALLINT      NULL,
    [CriticalCareLevel2Days]            SMALLINT      NULL,
    [CriticalCareLevel3Days]            SMALLINT      NULL,
    [DermatologicalSupportDays]         SMALLINT      NULL,
    [LiverSupportDays]                  SMALLINT      NULL,
    [NeurologicalSupportDays]           SMALLINT      NULL,
    [RenalSupportDays]                  SMALLINT      NULL,
    [CreatedByUser]                     VARCHAR (3)   NULL,
    [CreatedByTime]                     VARCHAR (12)  NULL,
    [LocalIdentifier]                   VARCHAR (8)   NULL,
    [LocationCode]                      VARCHAR (4)   NULL,
    [StatusCode]                        VARCHAR (11)  NULL,
    [TreatmentFunctionCode]             VARCHAR (4)   NULL,
    [PlannedAcpPeriod]                  VARCHAR (3)   NULL,
    [Created]                           DATETIME      NULL,
    [ByWhom]                            VARCHAR (50)  NULL,
    [EncounterRecno]                    INT           IDENTITY (1, 1) NOT NULL,
    [InterfaceCode]                     VARCHAR (5)   NULL,
    [CasenoteNumber]                    VARCHAR (18)  NULL,
    [WardCode]                          VARCHAR (50)  NULL,
    [AdmissionDate]                     DATE          NULL,
    [SiteCode]                          VARCHAR (5)   NULL,
    [NHSNumber]                         VARCHAR (17)  NULL,
    [UnitFunctionCode]                  VARCHAR (2)   NULL,
    CONSTRAINT [PK_CriticalCare] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CriticalCare]
    ON [APC].[CriticalCarePeriod]([SourceUniqueID] ASC, [SourcePatientNo] ASC);


GO
CREATE NONCLUSTERED INDEX [iDX_apc__ccp__ic__i_suid_sd]
    ON [APC].[CriticalCarePeriod]([InterfaceCode] ASC)
    INCLUDE([SourceUniqueID], [StartDate]);


﻿CREATE TABLE [APC].[PressureUlcer] (
    [PressureUlcerRecno]   INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]       INT          NOT NULL,
    [SourcePatientNo]      VARCHAR (8)  NULL,
    [SourceSpellNo]        VARCHAR (5)  NULL,
    [LocationID]           INT          NULL,
    [IdentifiedDate]       DATE         NULL,
    [IdentifiedTime]       DATETIME     NULL,
    [HealedDate]           DATE         NULL,
    [HealedTime]           DATETIME     NULL,
    [CategoryID]           INT          NULL,
    [Validated]            BIT          NULL,
    [OccuredOnAnotherWard] BIT          NULL,
    [WardCode]             VARCHAR (20) NOT NULL,
    [InterfaceCode]        VARCHAR (10) NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL,
    CONSTRAINT [PK__Pressure__613D576F74D018C5] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


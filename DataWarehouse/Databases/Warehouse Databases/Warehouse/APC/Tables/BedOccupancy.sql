﻿CREATE TABLE [APC].[BedOccupancy] (
    [ProviderSpellNo]  VARCHAR (20)  NOT NULL,
    [WardStayRecno]    INT           NOT NULL,
    [EncounterRecno]   INT           NOT NULL,
    [BedOccupancyDate] SMALLDATETIME NOT NULL,
    [Nights]           INT           NOT NULL,
    [ZeroLengthStays]  INT           NOT NULL,
    CONSTRAINT [PK_APCBedOccupancy] PRIMARY KEY CLUSTERED ([ProviderSpellNo] ASC, [WardStayRecno] ASC, [EncounterRecno] ASC, [BedOccupancyDate] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_BedOccupancy_1]
    ON [APC].[BedOccupancy]([WardStayRecno] ASC)
    INCLUDE([EncounterRecno], [BedOccupancyDate]);


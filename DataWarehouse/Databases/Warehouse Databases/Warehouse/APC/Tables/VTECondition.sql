﻿CREATE TABLE [APC].[VTECondition] (
    [VTEConditionRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]       INT          NOT NULL,
    [SourcePatientNo]      VARCHAR (8)  NULL,
    [SourceSpellNo]        VARCHAR (5)  NULL,
    [DiagnosisDate]        DATE         NULL,
    [DiagnosisTime]        DATETIME     NULL,
    [VTETypeID]            INT          NULL,
    [OccuredOnAnotherWard] INT          NULL,
    [MedicationRequired]   BIT          NULL,
    [MedicationStartDate]  DATE         NULL,
    [MedicationStartTime]  DATETIME     NULL,
    [WardCode]             VARCHAR (4)  NOT NULL,
    [InterfaceCode]        VARCHAR (6)  NOT NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL,
    CONSTRAINT [PK__VTECondi__613D576F172530C9] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


﻿CREATE TABLE [APC].[Diagnosis] (
    [SourceUniqueID]    VARCHAR (50) NOT NULL,
    [SourceSpellNo]     VARCHAR (20) NULL,
    [SourcePatientNo]   VARCHAR (20) NULL,
    [ProviderSpellNo]   VARCHAR (50) NULL,
    [SourceEncounterNo] VARCHAR (20) NULL,
    [SequenceNo]        SMALLINT     NULL,
    [DiagnosisCode]     VARCHAR (10) NULL,
    [APCSourceUniqueID] VARCHAR (50) NULL,
    [Created]           DATETIME     NOT NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Diagnosis_1] PRIMARY KEY NONCLUSTERED ([SourceUniqueID] ASC)
);


GO
CREATE CLUSTERED INDEX [IX_Diagnosis_1]
    ON [APC].[Diagnosis]([APCSourceUniqueID] ASC, [SequenceNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Diagnosis]
    ON [APC].[Diagnosis]([ProviderSpellNo] ASC, [SourceEncounterNo] ASC, [SequenceNo] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Diagnosis_2]
    ON [APC].[Diagnosis]([DiagnosisCode] ASC)
    INCLUDE([APCSourceUniqueID], [SourceEncounterNo], [SourcePatientNo], [SourceSpellNo]);


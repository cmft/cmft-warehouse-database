﻿CREATE TABLE [APC].[Fall] (
    [FallRecno]            INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]       INT          NOT NULL,
    [SourcePatientNo]      VARCHAR (8)  NULL,
    [SourceSpellNo]        VARCHAR (5)  NULL,
    [FallDate]             DATE         NULL,
    [FallTime]             DATETIME     NULL,
    [InitialSeverityID]    INT          NULL,
    [Validated]            BIT          NULL,
    [OccuredOnAnotherWard] BIT          NULL,
    [WardCode]             VARCHAR (10) NOT NULL,
    [InterfaceCode]        VARCHAR (10) NOT NULL,
    [Created]              DATETIME     NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NULL,
    CONSTRAINT [PK__Fall__613D576F658DD535] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


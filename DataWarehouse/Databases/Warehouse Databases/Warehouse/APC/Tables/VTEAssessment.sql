﻿CREATE TABLE [APC].[VTEAssessment] (
    [VTEAssessmentRecno]                 INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]                     INT          NOT NULL,
    [SourcePatientNo]                    VARCHAR (8)  NULL,
    [SourceSpellNo]                      VARCHAR (5)  NULL,
    [NoAssessmentReasonID]               INT          NULL,
    [AssessmentDate]                     DATE         NULL,
    [AssessmentTime]                     DATETIME     NULL,
    [AtRisk]                             INT          NOT NULL,
    [PreventativeMedicationStartDate]    DATE         NULL,
    [PreventativeMedicationStartTime]    DATETIME     NULL,
    [PreventativeNursingActionStartDate] DATE         NULL,
    [PreventativeNursingActionStartTime] DATETIME     NULL,
    [WardCode]                           VARCHAR (4)  NOT NULL,
    [InterfaceCode]                      VARCHAR (6)  NOT NULL,
    [Created]                            DATETIME     NULL,
    [Updated]                            DATETIME     NULL,
    [ByWhom]                             VARCHAR (50) NULL,
    CONSTRAINT [PK__VTEAsses__613D576F13549FE5] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC)
);


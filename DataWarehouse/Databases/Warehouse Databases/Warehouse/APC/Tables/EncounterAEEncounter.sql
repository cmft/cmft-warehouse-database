﻿CREATE TABLE [APC].[EncounterAEEncounter] (
    [AEEncounterRecno]  INT           NOT NULL,
    [APCEncounterRecno] INT           NOT NULL,
    [Created]           DATETIME2 (0) CONSTRAINT [DF_APC_EncounterAEEncounter_Created] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_EncounterAEEncounter] PRIMARY KEY CLUSTERED ([APCEncounterRecno] ASC, [AEEncounterRecno] ASC)
);


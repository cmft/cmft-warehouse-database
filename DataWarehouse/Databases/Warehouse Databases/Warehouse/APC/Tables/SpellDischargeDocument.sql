﻿CREATE TABLE [APC].[SpellDischargeDocument] (
    [APCEncounterRecno] INT NOT NULL,
    [DocumentRecno]     INT NULL,
    CONSTRAINT [PK__Discharg__323A99C80A2A5D07] PRIMARY KEY CLUSTERED ([APCEncounterRecno] ASC)
);


﻿CREATE TABLE [APC].[EncounterAlert] (
    [EncounterRecno] INT NOT NULL,
    [AlertRecno]     INT NOT NULL,
    CONSTRAINT [PK_EncounterAlert] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [AlertRecno] ASC)
);


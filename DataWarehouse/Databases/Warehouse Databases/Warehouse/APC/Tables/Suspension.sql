﻿CREATE TABLE [APC].[Suspension] (
    [SuspensionRecno]      INT           IDENTITY (1, 1) NOT NULL,
    [SourcePatientNo]      VARCHAR (9)   NOT NULL,
    [SourceEncounterNo]    VARCHAR (9)   NOT NULL,
    [SuspensionStartDate]  SMALLDATETIME NULL,
    [SuspensionEndDate]    SMALLDATETIME NULL,
    [SuspensionReasonCode] VARCHAR (4)   NULL,
    [SuspensionReason]     VARCHAR (30)  NULL,
    CONSTRAINT [PK_APC_Suspension] PRIMARY KEY CLUSTERED ([SuspensionRecno] ASC) WITH (FILLFACTOR = 90)
);


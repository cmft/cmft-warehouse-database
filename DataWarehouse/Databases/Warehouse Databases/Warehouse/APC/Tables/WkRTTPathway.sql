﻿CREATE TABLE [APC].[WkRTTPathway] (
    [InternalNo]                  NVARCHAR (9)  NULL,
    [doh_spec]                    NVARCHAR (50) NULL,
    [WL_EpisodeNo]                NVARCHAR (13) NULL,
    [treattype]                   NVARCHAR (6)  NULL,
    [type]                        NVARCHAR (10) NULL,
    [pathway_start_date_current]  DATETIME2 (7) NULL,
    [path_open_days_DNA_adjs]     INT           NULL,
    [pathway_start_date_original] DATETIME2 (7) NULL,
    [pathway_end_date]            DATETIME2 (7) NULL,
    [path_closed_days_DNA_adjs]   INT           NULL,
    [datebefore]                  DATETIME      NULL,
    [Comment1]                    VARCHAR (MAX) NULL,
    [Comment2]                    VARCHAR (MAX) NULL
);




GO
CREATE NONCLUSTERED INDEX [IX_APCWkRTTPathway]
    ON [APC].[WkRTTPathway]([treattype] ASC)
    INCLUDE([InternalNo], [doh_spec], [WL_EpisodeNo], [type], [pathway_start_date_current], [path_open_days_DNA_adjs], [pathway_start_date_original], [pathway_end_date]);


GO
CREATE NONCLUSTERED INDEX [IX_APC_WkRTTPathway]
    ON [APC].[WkRTTPathway]([treattype] ASC)
    INCLUDE([InternalNo], [doh_spec], [WL_EpisodeNo], [type], [pathway_start_date_current], [path_open_days_DNA_adjs], [pathway_start_date_original], [pathway_end_date], [path_closed_days_DNA_adjs], [datebefore]);


GO
CREATE NONCLUSTERED INDEX [IX_APC_WkRTTPathway_InternalNo_WL_EpisodeNo]
    ON [APC].[WkRTTPathway]([InternalNo] ASC, [WL_EpisodeNo] ASC)
    INCLUDE([Comment1], [Comment2]);


﻿


/* 
==============================================================================================
Description: Links AE Attendance with APC Admission first episode.

Notes:	Match logic in proc [WarehouseOLAPMergedV2].[APC].[BuildBaseEncounterAEBaseEncounter]

When		Who			What
20150420	Paul Egan	Initial Coding
20150508	Paul Egan	Added CTE & window function to remove FirstEpisodeInSpellIndicator duplicates
						(Tried adding another 'not exists' using WOMv2.ETL.TLoadCenAPCBaseEncounterlogic but
						it took much longer to run)
===============================================================================================
*/

CREATE Procedure [APC].[BuildEncounterAEEncounter] 

as

truncate table APC.EncounterAEEncounter;

with rawDataCTE as  -- Used CTE as FirstEpisodeDuplicateCount window function was much quicker to run than adding another 'not exists'
(
select 
	AEEncounterRecno = AEEncounter.EncounterRecno
	,APCEncounterRecno = APCEncounter.EncounterRecno
	,FirstEpisodeDuplicateCount = 
		row_number() over
			(
			partition by APCEncounter.ProviderSpellNo
			order by APCEncounter.EpisodeStartTime, APCEncounter.SourceEncounterNo  -- Tiebreaker
			)
from
	AE.Encounter AEEncounter

	inner join APC.Encounter APCEncounter
	on	APCEncounter.DistrictNo = AEEncounter.DistrictNo
	and AEEncounter.Reportable = 1 
	--and APCEncounter.Reportable = 1	-- Does not exist in classic
	and APCEncounter.AdmissionMethodCode = 'AE'
	and AEEncounter.AttendanceDisposalCode = '01'   -- Admitted
	and abs(datediff(hour, AEEncounter.DepartureTime, APCEncounter.AdmissionTime)) <= 24
	and APCEncounter.FirstEpisodeInSpellIndicator = 'Y' -- Small number of duplicates, handled with FirstEpisodeDuplicateCount window function
where 
	not exists -- pulls closest APC event to A&E
		(
		select 
			1
		from
			APC.Encounter  Encounter
		where 
			Encounter.DistrictNo = APCEncounter.DistrictNo
		and Encounter.AdmissionMethodCode = APCEncounter.AdmissionMethodCode
		and abs(datediff(minute, AEEncounter.DepartureTime, Encounter.AdmissionTime)) < abs(datediff(minute, AEEncounter.DepartureTime, APCEncounter.AdmissionTime))
		)
	and not exists -- pulls closest A&E event to APC, ie max A&E departure prior to admission time
		(
		select 
			1
		from
			AE.Encounter  Encounter
		where 
			Encounter.DistrictNo = AEEncounter.DistrictNo
		and Encounter.AttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
		and abs(datediff(minute, Encounter.DepartureTime, APCEncounter.AdmissionTime)) < abs(datediff(minute, AEEncounter.DepartureTime, APCEncounter.AdmissionTime))
		)
	--and AEEncounter.AttendanceNumber = 'MAE-11-079066-1'
)



insert into APC.EncounterAEEncounter 
	(
	AEEncounterRecno
	,APCEncounterRecno
	)
select
	 AEEncounterRecno
	,APCEncounterRecno
from
	rawDataCTE
where
	FirstEpisodeDuplicateCount = 1
;

--option (maxdop 3)	 

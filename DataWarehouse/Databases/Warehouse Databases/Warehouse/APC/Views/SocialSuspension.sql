﻿CREATE view [APC].[SocialSuspension] as

select
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,sum(datediff(day, SuspensionStartDate, SuspensionEndDate)) DaysSuspended
from
	APC.WaitingListSuspension Suspension
where
	upper(SuspensionReason) like '%S*%'
--and	coalesce(SuspensionEndDate, CensusDate) >= CensusDate
group by
	 SourcePatientNo
	,SourceEncounterNo
	,CensusDate

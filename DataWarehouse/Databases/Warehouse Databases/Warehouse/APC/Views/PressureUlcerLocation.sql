﻿
create view APC.PressureUlcerLocation

as

select
	PressureUlcerLocationID = [ID]
	,PressureUlcerLocation = [Description]
from
	Bedman.PressureUlcerLocationBase
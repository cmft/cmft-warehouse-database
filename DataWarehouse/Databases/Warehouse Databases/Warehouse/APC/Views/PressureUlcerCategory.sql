﻿
create view APC.PressureUlcerCategory

as

select
	PressureUlcerCategoryID = [ID]
	,PressureUlcerCategory = [Description]
from
	Bedman.PressureUlcerCategoryBase
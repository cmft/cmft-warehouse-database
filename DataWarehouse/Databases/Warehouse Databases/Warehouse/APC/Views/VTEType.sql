﻿
create view APC.VTEType

as

select
	VTETypeID = [ID]
	,VTEType = [Description]
from
	Bedman.VTETypeBase
﻿
create view APC.CatheterType

as

select
	CatheterTypeID = [ID]
	,CatheterType = [Description]
from
	[Bedman].[CatheterTypeBase]
﻿
create view APC.FallSeverity

as

select
	FallSeverityID = [ID]
	,FallSeverity = [Description]
from
	Bedman.FallSeverityBase
﻿
create view APC.VTEAssessmentNotRequiredReason

as

select
	VTEAssessmentNotRequiredReasonID = [ID]
	,VTEAssessmentNotRequiredReason = [Description]
from
	Bedman.VTEAssessmentNotRequiredReasonBase
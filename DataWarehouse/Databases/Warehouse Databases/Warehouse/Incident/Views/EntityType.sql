﻿

CREATE view [Incident].[EntityType]

as

select
	[EntityTypeCode]
	,[EntityType]
from
	Ulysses.IncidentEntityTypeBase

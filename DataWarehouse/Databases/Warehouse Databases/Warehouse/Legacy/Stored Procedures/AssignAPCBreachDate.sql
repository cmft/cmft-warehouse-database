﻿CREATE procedure [Legacy].[AssignAPCBreachDate] as

/*
update APC.Encounter
set
	 ClockStartDate = BreachDetail.ClockStartDate
	,BreachDate = BreachDetail.BreachDate
	,RTTBreachDate = BreachDetail.RTTBreachDate
	,NationalBreachDate = BreachDetail.NationalBreachDate
	,NationalDiagnosticBreachDate = BreachDetail.NationalDiagnosticBreachDate
	,SocialSuspensionDays = BreachDetail.SocialSuspensionDays
	,BreachTypeCode = BreachDetail.BreachTypeCode
	,ReferralEncounterRecno = BreachDetail.ReferralEncounterRecno
from
	APC.Encounter

inner join
	(
	select
		 SourceUniqueID
		,ClockStartDate =
			DATEADD(day, datediff(day, 0, ClockStartDate), 0)

		,BreachDate =
			DATEADD(
				 day
				,datediff(
					 day
					,0
					,case
					when BreachTypeCode = 'RTT'
					then RTTBreachDate
					when BreachTypeCode = 'DIAG'
					then NationalDiagnosticBreachDate
					else NationalBreachDate
					end
				)
				,0
			)

		,RTTBreachDate =
			DATEADD(day, datediff(day, 0, RTTBreachDate), 0)

		,NationalBreachDate =
			DATEADD(day, datediff(day, 0, NationalBreachDate), 0)

		,NationalDiagnosticBreachDate =
			DATEADD(day, datediff(day, 0, NationalDiagnosticBreachDate), 0)

		,SocialSuspensionDays
		,BreachTypeCode
		,ReferralEncounterRecno
	from
		(
		select
			 SourceUniqueID = EncounterSourceUniqueID

			,ClockStartDate =
				coalesce(
					 RTTClockStartDate
					,ManualClockStartDate
					,ReferralDate
					,DateOnWaitingList
				)


			,RTTBreachDate =
				dateadd(
					 day
					,(select NumericValue from dbo.Parameter where Parameter = 'RTTBREACHDAYS') +
					coalesce(
						SocialSuspensionDays
						,ManualAdjustmentDays
					)

					,coalesce(
						 RTTClockStartDate
						,ManualClockStartDate
						,ReferralDate
						,DateOnWaitingList
					)
				)

			,NationalBreachDate =
				dateadd(
					 day
					,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALIPDEFAULTBREACHDAYS') +
					coalesce(
						SocialSuspensionDays
						,ManualAdjustmentDays
					)

					,coalesce(
						 RTTClockStartDate
						,ManualClockStartDate
						,ReferralDate
						,DateOnWaitingList
					)
				)

			,NationalDiagnosticBreachDate =
				dateadd(
					 day
					,(select NumericValue from dbo.Parameter where Parameter = 'NATIONALDIAGNOSTICBREACHDAYS') +
					coalesce(
						SocialSuspensionDays
						,ManualAdjustmentDays
					)

					,coalesce(
						 RTTClockStartDate
						,ManualClockStartDate
						,ReferralDate
						,DateOnWaitingList
					)
				)

			,SocialSuspensionDays =
					coalesce(
						SocialSuspensionDays
						,ManualAdjustmentDays
					)

			,BreachTypeCode =
				case
				when
					Diagnostic = 1
				then 'DIAG' --diagnostic breach
				when
					datediff(
						day
						,coalesce(
							 RTTClockStartDate
							,ManualClockStartDate
							,ReferralDate
							,DateOnWaitingList
						)
						,AdmissionDate
					)

					> 
					(select NumericValue from dbo.Parameter where Parameter = 'BREACHTHRESHOLDDAYS') +
					coalesce(
						SocialSuspensionDays
						,ManualAdjustmentDays
					)

				then 'NAT' --national breach threshold
				else 'RTT' --RTT breach (18 weeks)
				end

			,ReferralEncounterRecno

		from
			(
			select
				 EncounterSourceUniqueID = Encounter.SourceUniqueID

		--various potential clock starts

				,RTTClockStartDate =
					Encounter.RTTStartDate

				,ManualClockStartDate =
					APCManualClock.ClockStartDate

				,ReferralDate =
					RFEncounter.ReferralDate

				,Encounter.DateOnWaitingList

				,SocialSuspensionDays = coalesce(SocialSuspension.DaysSuspended, 0)

				,ManualAdjustmentDays =
					coalesce(APCManualAdjustment.AdjustmentDays, 0)

				,Diagnostic =
					case
					when
						DiagnosticProcedure.ProcedureCode is null
			--		and	upper(Encounter.Operation) not like '%$T%'
					then 0
					else 1
					end

				,Encounter.AdmissionDate

				,RFEncounter.ReferralEncounterRecno

			from
				WH.APC.Encounter Encounter

			left join
				(
				select
					 EncounterRecno = MappedInpatient.SourceEncounterRecno
					,ReferralDate = Referral.ReferralDate
					,ReferralEncounterRecno = MappedReferral.SourceEncounterRecno
				from
					RTT.dbo.MatchMaster

				inner join RTT.dbo.EncounterRecnoMap MappedInpatient
				on	MappedInpatient.EncounterRecno = MatchMaster.InpatientRecno
				and	MappedInpatient.EncounterTypeCode = 'IP'

				inner join RTT.dbo.EncounterRecnoMap MappedReferral
				on	MappedReferral.EncounterRecno = MatchMaster.ReferralRecno
				and	MappedReferral.EncounterTypeCode = 'RF'

				inner join RF.Encounter Referral
				on	Referral.EncounterRecno = MappedReferral.SourceEncounterRecno

				where
					MatchMaster.CensusDate =
						(
						select
							max(CensusDate)
						from
							RTT.dbo.MatchMaster
						)
				)	RFEncounter
			on	RFEncounter.EncounterRecno = Encounter.EncounterRecno

			left join 
			(
				select
					 SourcePatientNo
					,SourceEncounterNo
					,sum(datediff(day, SuspensionStartDate, SuspensionEndDate)) DaysSuspended
				from
					APC.Suspension Suspension
				where
					SuspensionReason like '%S*%'
				group by
					 SourcePatientNo
					,SourceEncounterNo
			) SocialSuspension
			on	SocialSuspension.SourcePatientNo = Encounter.SourcePatientNo
			and	SocialSuspension.SourceEncounterNo = Encounter.SourceSpellNo


		--get manual adjustments applied to this encounter's wait
			left join 
				(
				select
					 SourcePatientNo SourcePatientNo
					,SourceEncounterNo = SourceEntityRecno
					,sum(RTTAdjustment.AdjustmentDays) AdjustmentDays
				from
					RTT.dbo.RTTAdjustment RTTAdjustment
				group by
					 RTTAdjustment.SourcePatientNo
					,RTTAdjustment.SourceEntityRecno
				) APCManualAdjustment
			on	APCManualAdjustment.SourcePatientNo = Encounter.SourcePatientNo
			and	APCManualAdjustment.SourceEncounterNo = Encounter.SourceSpellNo

			left join 
				(
				select
					 RTTClockStartRecno
					,SourcePatientNo SourcePatientNo
					,SourceEncounterNo = SourceEntityRecno
					,ClockStartDate
				from
					RTT.dbo.RTTClockStart RTTClockStart
				where
					not exists
					(
					select
						1
					from
						RTT.dbo.RTTClockStart LatestRTTClockStart
					where
						LatestRTTClockStart.SourcePatientNo = RTTClockStart.SourcePatientNo
					and	LatestRTTClockStart.SourceEntityRecno = RTTClockStart.SourceEntityRecno
					and	(
							LatestRTTClockStart.ClockStartDate > RTTClockStart.ClockStartDate
						or
							(
								LatestRTTClockStart.ClockStartDate = RTTClockStart.ClockStartDate
							and	LatestRTTClockStart.RTTClockStartRecno > RTTClockStart.RTTClockStartRecno
							)
						)
					)
				) APCManualClock
			on	APCManualClock.SourcePatientNo = Encounter.SourcePatientNo
			and	APCManualClock.SourceEncounterNo = Encounter.SourceSpellNo

			left join RTT.dbo.DiagnosticProcedure DiagnosticProcedure
			on	DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode

			) Detail
		) Final
	) BreachDetail
on	BreachDetail.SourceUniqueID = Encounter.SourceUniqueID
*/
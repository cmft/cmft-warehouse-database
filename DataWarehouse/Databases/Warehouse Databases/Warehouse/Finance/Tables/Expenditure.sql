﻿CREATE TABLE [Finance].[Expenditure] (
    [ExpenditureRecno] INT           IDENTITY (1, 1) NOT NULL,
    [CensusDate]       DATE          NOT NULL,
    [Division]         VARCHAR (100) NOT NULL,
    [Budget]           FLOAT (53)    NULL,
    [AnnualBudget]     FLOAT (53)    NULL,
    [Actual]           FLOAT (53)    NULL,
    [InterfaceCode]    VARCHAR (50)  NULL,
    [Created]          DATETIME      NULL,
    [Updated]          DATETIME      NULL,
    [ByWhom]           VARCHAR (50)  NULL,
    CONSTRAINT [PK_FinanceExpenditure] PRIMARY KEY CLUSTERED ([CensusDate] ASC, [Division] ASC)
);


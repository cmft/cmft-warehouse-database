﻿CREATE TABLE [Pseudonym].[PatientApplicationIdentity] (
    [PatientId]                       INT             NOT NULL,
    [SourceApplicationIdentifierId]   INT             NOT NULL,
    [PatientApplicationIdentityValue] NVARCHAR (50)   NOT NULL,
    [ExternalKey]                     VARBINARY (MAX) NULL
);


﻿create procedure [ETL].[LoadGenomicsParticipant]
as


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	@inserted int


truncate table Themis.GenomicsParticipant


insert into Themis.GenomicsParticipant
(
	GenomicsParticipantID
	,NHSNumber
)
select
	Participant.ParticipantId
	,Participant.NhsNumber
from
	ThemisLive.dbo.GEL_Participants Participant

inner join ThemisLive.dbo.GEL_Diseases Disease
on	Disease.DiseaseId = Participant.DiseaseId

where
	Disease.IsRare = 0

and	exists
	(
	select
		1
	from
		ThemisLive.dbo.GEL_Samples Sample

	inner join ThemisLive.dbo.GEL_Derivatives Derivative
	on	Derivative.SampleId = Sample.SampleId

	inner join ThemisLive.dbo.GEL_Dispatches Dispatch
	on	Dispatch.DispatchId = Derivative.DispatchId

	where
		Sample.ParticipantId = Participant.ParticipantId
	)


select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


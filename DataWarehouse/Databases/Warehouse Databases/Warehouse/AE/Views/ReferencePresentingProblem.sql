﻿




CREATE view [AE].[ReferencePresentingProblem] as

select distinct 
	 PresentingProblemCode = atd_complaintid

	,PresentingProblem =
		case 
		when isnumeric(replace(ltrim(rtrim(atd_complaintid)) , '.' , '')) = 1
			then
				(
				SELECT
					Lkp_Name 
				FROM 
					[$(Symphony)].dbo.Lookups 
				WHERE
					Lkp_ID = atd_complaintid
				) 
		else (cast(atd_complaintid as varchar))
		end 


from
	[$(Symphony)].dbo.Attendance_Details Encounter
where 
	case 
	when isnumeric(replace(ltrim(rtrim(atd_complaintid)) , '.' , '')) = 1 
		then
			(
			SELECT 
				Lkp_Name 
			FROM 
				[$(Symphony)].dbo.Lookups 
			WHERE  
				Lkp_ID = atd_complaintid
			) 
	else (cast(atd_complaintid as varchar))
	end <> ''


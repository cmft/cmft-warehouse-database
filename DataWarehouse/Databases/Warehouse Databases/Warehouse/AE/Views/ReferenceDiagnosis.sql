﻿



CREATE view [AE].[ReferenceDiagnosis]

as

select
	 SourceDiagnosisCode = LookupBase.Lkp_ID
	,Diagnosis = LookupBase.Lkp_Name


from
	AE.LookupBase LookupBase
  
  inner join AE.LookupBase LookupGroup
  on  LookupBase.[lkp_TableID] = LookupGroup.Lkp_ID 
  and LookupGroup.Lkp_ParentID = 0
  and LookupGroup.Lkp_Name like '%Diagn%'






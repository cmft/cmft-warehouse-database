﻿create view [AE].[Reference] as

select
	 ReferenceCode = Lkp_ID
	,Reference = Lkp_Name

	,ReferenceParentCode = 
		case
		when Lkp_ParentID = 0
		then null
		else Lkp_ParentID
		end

	,Active = lkp_active
	,DisplayOrder = Lkp_DisplayOrder
from
	AE.LookupBase
where
	Lkp_ID <> 0

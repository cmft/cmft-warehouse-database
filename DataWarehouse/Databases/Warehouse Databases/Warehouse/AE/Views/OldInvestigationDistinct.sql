﻿

CREATE view [AE].[OldInvestigationDistinct] as

select
	 AESourceUniqueID
	,InvestigationCode =
		left(
			 Investigation.InvestigationCode
			,6
		)

	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY AESourceUniqueID ORDER BY InvestigationCode)
from
	(
	select distinct
		 AESourceUniqueID
		,InvestigationCode
	from
		AE.Investigation
	) Investigation



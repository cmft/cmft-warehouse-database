﻿

CREATE view [AE].[OldTreatmentDistinct] as

select
	 AESourceUniqueID
	,TreatmentCode =
		left(
			 [Procedure].ProcedureCode
			,6
		)

	,TreatmentDate = [Procedure].ProcedureDate

	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY AESourceUniqueID ORDER BY ProcedureCode)
from
	(
	select
		 AESourceUniqueID
		,ProcedureCode
		,ProcedureDate
	from
		AE.[Procedure] Treatment

	where
		not exists
		(
		select
			1
		from
			AE.[Procedure] Latest
		where
			Latest.AESourceUniqueID = Treatment.AESourceUniqueID
		and	Latest.ProcedureCode = Treatment.ProcedureCode
		and	(
				Latest.ProcedureDate < Treatment.ProcedureDate
			or	(
					Latest.ProcedureDate = Treatment.ProcedureDate
				and	Latest.SourceUniqueID < Treatment.SourceUniqueID
				)
			)
		)

	) [Procedure]



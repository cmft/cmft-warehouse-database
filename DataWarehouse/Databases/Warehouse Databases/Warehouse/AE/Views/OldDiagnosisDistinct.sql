﻿

CREATE view [AE].[OldDiagnosisDistinct] as

--Diagnosis is comprised:
--Diagnosis Condition n2 
--Sub-Analysis n1 
--Accident And Emergency Attendance - ANATOMICAL AREA  n2 
--Accident And Emergency Attendance - ANATOMICAL SIDE  an1 

select
	 AESourceUniqueID
	,Diagnosis.DiagnosisCode

	,SequenceNo = 
		ROW_NUMBER() OVER(PARTITION BY AESourceUniqueID ORDER BY DiagnosisCode)
from
	(
	select distinct
		 AESourceUniqueID
		,DiagnosisCode = rtrim(DiagnosisCode + ' ' + coalesce(Diagnosis.DiagnosisSiteCode, '  ') +  coalesce(Diagnosis.DiagnosisSideCode, ' '))
	from
		AE.Diagnosis
	) Diagnosis



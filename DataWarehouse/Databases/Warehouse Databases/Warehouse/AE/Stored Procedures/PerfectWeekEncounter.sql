﻿
CREATE proc [AE].[PerfectWeekEncounter] 

with execute as owner

as


-- 20151009	RR	The following proc was excluding patients who arrived before midnight but not yet departed from A&E.  
--				Anthony Smith  requested we changed the first select to pull arrivals since yesterday at 00:00 (rather than today) and exclude the union

select
	Encounter.atd_id SourceUniqueID
	,DistrictNo.DistrictNo DistrictNo
	,Postcode =
		coalesce(
			  PermanentAddress.add_postcode
			  ,TemporaryAddress.add_postcode
		)
	,Patient.pat_dob DateOfBirth
	,RegisteredGpPracticeCode = GpPractice.pr_praccode
	,Encounter.atd_num AttendanceNumber
	,ArrivalModeCode = Encounter.atd_arrmode
	,AttendanceCategoryCode =
		case Encounter.atd_attendancetype
		when 1
		then '2' --planned review
		when 2
		then '3' --unplanned review
		else '1' --attend
		end
	,AttendanceDisposalCode =
		right(
			  '0' + case when Disposal.DisposalCode = '' then null else Disposal.DisposalCode end
			  ,2
		)
	,PatientGroupCode =
				CASE
				WHEN PatientGroup.PatientGroupCode  = '' THEN '0'
				ELSE
				COALESCE
					(
					PatientGroup.PatientGroupCode
					,'0'
					)
				END
	,dateadd(day, datediff(day, 0, Encounter.atd_arrivaldate), 0) ArrivalDate
	,Encounter.atd_arrivaldate ArrivalTime
	,InitialAssessmentTime = Triage.tri_date
	,SeenForTreatmentTime = Result.res_date
	,AttendanceConclusionTime = Encounter.atd_dischdate
	,DepartureTime = Departure.DepartureTime
	,TriageCategoryCode = Triage.tri_category
	,PresentingProblemCode = Encounter.atd_complaintid
	,ToSpecialtyTime = ToSpecialty.InvestigationDate
	,SeenBySpecialtyTime = SeenBySpecialty.InvestigationResultDate
	,ReferredToSpecialtyCode = 
				case
				when Department.SiteCode = 'RW3RC' and PEDToSpecialty.SpecialtyCode <> 0 then PEDToSpecialty.SpecialtyCode
				else ToSpecialty.SpecialtyCode
				end
	,DecisionToAdmitTime = Admission.DecisionToAdmitTime
	,DischargeDestinationCode = Encounter.atd_dischdest
	,SourcePatientNo = Patient.pat_pid
	,SourceAttendanceDisposalCode = Encounter.atd_dischoutcome
	,AmbulanceArrivalTime.AmbulanceArrivalTimeInt
	,SourceCareGroupCode = Encounter.atd_caregroup

	,TriageComment = 
		Triage.TriageComment

	,AcceptanceTime = AcceptingClinician.AcceptingClinicianTime 
	,AcceptingClinician = AcceptingClinician.AcceptingClinician		
--into
--	#AEEncounterPerfectWeek
from
	[$(Symphony)].dbo.Attendance_Details Encounter

inner join [$(Symphony)].dbo.Episodes Episode
on	Episode.epd_id = Encounter.atd_epdid

inner join [$(Symphony)].dbo.Patient Patient
on	Patient.pat_pid = Episode.epd_pid

inner join [$(Symphony)].dbo.Patient_details PatientDetail
on	PatientDetail.pdt_pid = Patient.pat_pid

-- 2010-03-05 CCB get previous patient details

left join [$(Symphony)].dbo.Aud_Patient_Details AuditPatientDetail
on	AuditPatientDetail.pdt_pid = Episode.epd_pid
and	AuditPatientDetail.pdt_update < Encounter.atd_dischdate
and	AuditPatientDetail.pdt_deleted = 0
and	not exists
	(
	select
		1
	from
		[$(Symphony)].dbo.Aud_Patient_Details Previous
	where
		Previous.pdt_pid = AuditPatientDetail.pdt_pid
	and	Previous.pdt_update < Encounter.atd_dischdate
	and	Previous.pdt_identity > AuditPatientDetail.pdt_identity
	and	AuditPatientDetail.pdt_deleted = 0
	)

-- 2010-02-10 CCB modifications to reflect Symphony upgrade to v2.28 BEGIN

-- 2010-05-03 CCB coalesce to ensure we get old details if available and current details if not

left join [$(Symphony)].dbo.GPPracticeAddressLink GPPracticeAddressLinkOld
on	GPPracticeAddressLinkOld.gpa_gpid = AuditPatientDetail.pdt_gpid
and GPPracticeAddressLinkOld.gpa_prid = AuditPatientDetail.pdt_practise
and GPPracticeAddressLinkOld.gpa_praddid = AuditPatientDetail.pdt_praddid

left join [$(Symphony)].dbo.GPPracticeAddressLink GPPracticeAddressLinkCurrent
on	GPPracticeAddressLinkCurrent.gpa_gpid = PatientDetail.pdt_gpid
and GPPracticeAddressLinkCurrent.gpa_prid = PatientDetail.pdt_practise
and GPPracticeAddressLinkCurrent.gpa_praddid = PatientDetail.pdt_praddid

left join [$(Symphony)].dbo.Gp_Practise GpPractice 
on	GpPractice.Pr_id = coalesce(GPPracticeAddressLinkOld.gpa_prid, GPPracticeAddressLinkCurrent.gpa_prid)

left join [$(Symphony)].dbo.Gp Gp 
on	Gp.gp_id = coalesce(GPPracticeAddressLinkOld.gpa_gpid, GPPracticeAddressLinkCurrent.gpa_gpid)

-- 2010-02-10 CCB modifications to reflect Symphony upgrade to v2.28 END

inner join 
      (
	select
		Department.dpt_id
		,CommissioningSerialNo = '5NT00A'

		,PCTCode = '5NT'

		,SiteCode =
		case
		when Department.dpt_name = 'MRI ED' then 'RW3MR'
		when Department.dpt_name = 'PED' then 'RW3RC'
		when Department.dpt_name = 'EEC' then 'RW3RE'
		when Department.dpt_name = 'St Mary''s' then 'RW3SM'
		when Department.dpt_name = 'Dental' then 'RW3DH'
		end
	from
		[$(Symphony)].dbo.CFG_Dept Department
	where
		Department.dpt_name in (
			  'MRI ED'
			  ,'PED'
			  ,'EEC'
			  ,'St Mary''s'
			  ,'Dental'
			  )
      ) Department
on    Department.dpt_id = Episode.epd_deptid

left join
      (
      select distinct
            DistrictNo = patient_system_ids.psi_system_id 
            ,psi_pid
      from
            [$(Symphony)].dbo.Patient_system_ids patient_system_ids
      where
            patient_system_ids.psi_system_name = 
                  (
                  select
                        Lkp_ID
                  from
                        [$(Symphony)].dbo.Lookups
                  where
                        Lkp_Name = 'District Number'
                  )
      and   not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Patient_system_ids PreviousDistrictNo
            where
                  PreviousDistrictNo.psi_system_name = patient_system_ids.psi_system_name
            and   PreviousDistrictNo.psi_pid = patient_system_ids.psi_pid
            and   PreviousDistrictNo.psi_system_id > patient_system_ids.psi_system_id
            )
      ) DistrictNo
on    DistrictNo.psi_pid = Patient.pat_pid

left join
      (
      select
            *
      from
            [$(Symphony)].dbo.Address PermanentAddress
      where
            PermanentAddress.add_linktype = 1
      and   PermanentAddress.add_type = 2673
--    and   PermanentAddress.add_type in 
--          (
--          select
--                Lkp_ID
--          from
--                [$(Symphony)].dbo.lookups AddressType
--          where
--                AddressType.Lkp_Name = 'Permanent address'
--          )
      and not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Address PreviousPermanentAddress
            where
                  PreviousPermanentAddress.add_linkid = PermanentAddress.add_linkid
            and   PreviousPermanentAddress.add_linktype = PermanentAddress.add_linktype
            and   PreviousPermanentAddress.add_type = PermanentAddress.add_type
            and   PreviousPermanentAddress.add_update > PermanentAddress.add_update
            )
      ) PermanentAddress
on    PermanentAddress.add_linkid = PatientDetail.pdt_pid

left join
      (
      select
            *
      from
            [$(Symphony)].dbo.Address TemporaryAddress
      where
            TemporaryAddress.add_linktype = 1
      and   TemporaryAddress.add_type in 
            (
            4993
            ,10522
--          select
--                Lkp_ID
--          from
--                [$(Symphony)].dbo.lookups AddressType
--          where
--                AddressType.Lkp_Name = 'Permanent address'
            )
      and not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Address PreviousTemporaryAddress
            where
                  PreviousTemporaryAddress.add_linkid = TemporaryAddress.add_linkid
            and   PreviousTemporaryAddress.add_linktype = TemporaryAddress.add_linktype
            and   PreviousTemporaryAddress.add_type = TemporaryAddress.add_type
            and   PreviousTemporaryAddress.add_update > TemporaryAddress.add_update
            )
      ) TemporaryAddress
on    TemporaryAddress.add_linkid = PatientDetail.pdt_pid

left join
      (
      select distinct
            DisposalId = flm_lkpid
            ,DisposalCode = not_text
      from
            [$(Symphony)].dbo.Notes

      inner join [$(Symphony)].dbo.LookupMappings
      on    flm_value = not_noteid
      and   flm_mtid = 
            (
            select
                  mt_id
            from
                  [$(Symphony)].dbo.MappingTypes  
            where
                  mt_name = 'CDS'
            )
      ) Disposal
on    Disposal.DisposalId = Encounter.atd_dischoutcome

left join
      (
      select distinct
            PatientGroupId = flm_lkpid
            ,PatientGroupCode = not_text
      from
            [$(Symphony)].dbo.Notes

      inner join [$(Symphony)].dbo.LookupMappings
      on    flm_value = not_noteid
      and   flm_mtid = 
            (
            select
                  mt_id
            from
                  [$(Symphony)].dbo.MappingTypes 
            where
                  mt_name = 'CDS'
            )
      ) PatientGroup
on    PatientGroup.PatientGroupId = Encounter.atd_reason

left join
      (
      select distinct
            SourceOfReferralId = flm_lkpid
            ,SourceOfReferralCode = not_text
      from
            [$(Symphony)].dbo.Notes

      inner join [$(Symphony)].dbo.LookupMappings
      on    flm_value = not_noteid
      and   flm_mtid = 
            (
            select
                  mt_id
            from
                  [$(Symphony)].dbo.MappingTypes  
            where
                  mt_name = 'CDS'
            )
      ) SourceOfReferral
on    SourceOfReferral.SourceOfReferralId = Encounter.atd_refsource

left join (
      select
            Triage.*
            ,TriageComment = 
                  TriageNotes.not_text
      from
            [$(Symphony)].dbo.Triage Triage
      left outer join [$(Symphony)].dbo.Notes TriageNotes 
      on Triage.tri_comments = TriageNotes.not_noteid
      where
            Triage.tri_inactive = 0
      and   not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Triage LatestTriage
            where
                  LatestTriage.tri_inactive = 0
            and   LatestTriage.tri_atdid = Triage.tri_atdid
            and   LatestTriage.tri_trid < Triage.tri_trid
            )
      ) Triage
on    Triage.tri_atdid = Encounter.atd_id

--(9s)
left join --get the first clinician outcome based result date
      (
      select distinct
            Result.res_atdid
            ,Result.res_date
            ,Staff.stf_staffid
      from
            [$(Symphony)].dbo.Result_details Result

      inner join [$(Symphony)].dbo.Staff Staff
      on    Staff.stf_staffid = Result.res_staff1

      where
            Result.res_depid IN ( 86 , 195 ) -- CCB 2011-10-24 the code below is returning DEP 174, which is 'Call MRI'
                  --(
                  --select
                  --    rps_value
                  --from
                  --    [$(Symphony)].dbo.CFG_ReportSettings
                  --where
                  --    rps_name IN('SeeA+EClinicianOutcome','See A+E ClinicianOutcome') -- = 'SeeA+EClinicianOutcome'  GS20111019  Additional label added to bring through dental depID 195
                  --)
      and   Result.res_inactive <> 1
      and   not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Result_details LaterResult

            inner join [$(Symphony)].dbo.Staff Staff
            on    Staff.stf_staffid = LaterResult.res_staff1

            where
                  LaterResult.res_depid IN ( 86 , 195 ) 
                        --(
                        --select
                        --    rps_value
                        --from
                        --    [$(Symphony)].dbo.CFG_ReportSettings
                        --where
                        --    rps_name IN('SeeA+EClinicianOutcome','See A+E ClinicianOutcome') -- = 'SeeA+EClinicianOutcome' GS20111019  Additional label added to bring through dental depID 195
                        --)
            and   LaterResult.res_inactive <> 1
            and   LaterResult.res_atdid = Result.res_atdid
            and (
                        LaterResult.res_date < Result.res_date
                  or    (
                              LaterResult.res_date = Result.res_date
                        and   LaterResult.res_resid < Result.res_resid
                        )
                  )
            )
      ) Result
on    Result.res_atdid = Encounter.atd_id

--(9s)
left join
      (
      select
            Location.cul_atdid
            ,DepartureTime = Location.cul_locationdate
      from
            [$(Symphony)].dbo.Current_Locations Location
      where
            Location.cul_locationid in 
            (
            select
                  loc_id
            from
                  [$(Symphony)].dbo.CFG_Locations
            where
                  loc_name = 'Left Department'
            )
      and   not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Current_Locations PreviousLocation
            where
                  PreviousLocation.cul_locationid in 
                  (
                  select
                        loc_id
                  from
                        [$(Symphony)].dbo.CFG_Locations
                  where
                        loc_name = 'Left Department'
                  )
            and   PreviousLocation.cul_atdid = Location.cul_atdid
            and (
                        --PreviousLocation.cul_locationdate > Location.cul_locationdate
                        PreviousLocation.cul_update > Location.cul_update
                  or
                        (
                              --PreviousLocation.cul_locationdate = Location.cul_locationdate
                              PreviousLocation.cul_locationdate = Location.cul_update
                        and   PreviousLocation.cul_culid > Location.cul_culid
                        )
                  )
            )
      ) Departure
on    Departure.cul_atdid = Encounter.atd_id

--left join [$(Symphony)].dbo.lookups ArrivalMode
--on  ArrivalMode.lkp_id = Encounter.atd_arrmode

left join (
      select
            SourceUniqueID = Request.req_atdid
            ,InvestigationDate = Request.req_date
            ,SpecialtyCode = Request.req_request
      from
            [$(Symphony)].dbo.Request_Details Request
      where
            Request.req_depid in 
                  (
                  select
                        rps_value
                  from
                        [$(Symphony)].dbo.CFG_ReportSettings
                  where
                        rps_name = 'Specialty'
                  )
      and   Request.req_inactive <> 1
      and not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Request_Details PreviousRequest
            where
                  PreviousRequest.req_depid = Request.req_depid
            and   PreviousRequest.req_atdid = Request.req_atdid
            and   PreviousRequest.req_inactive <> 1
            and   (
                        PreviousRequest.req_date > Request.req_date
                  or    (
                              PreviousRequest.req_date = Request.req_date
                        and   PreviousRequest.req_reqid > Request.req_reqid
                        )
                  )
            )
      ) ToSpecialty
on    ToSpecialty.SourceUniqueID = Encounter.atd_id

left join (
      select
            SourceUniqueID = Request.req_atdid
            ,InvestigationDate = Request.req_date
            ,SpecialtyCode =  Request.req_field1
      from
            [$(Symphony)].dbo.Request_Details Request
      where
            Request.req_depid in 
                  (
                  select
                        rps_value
                  from
                        [$(Symphony)].dbo.CFG_ReportSettings
                  where
                        rps_name = 'Specialty'
                  )
      and   Request.req_inactive <> 1
      and not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Request_Details PreviousRequest
            where
                  PreviousRequest.req_depid = Request.req_depid
            and   PreviousRequest.req_atdid = Request.req_atdid
            and   PreviousRequest.req_inactive <> 1
            and   (
                        PreviousRequest.req_date > Request.req_date
                  or    (
                              PreviousRequest.req_date = Request.req_date
                        and   PreviousRequest.req_reqid > Request.req_reqid
                        )
                  )
            )
      ) PEDToSpecialty
on    PEDToSpecialty.SourceUniqueID = Encounter.atd_id

left join (
      select
            SourceUniqueID = Result.res_atdid
            ,InvestigationResultDate = Result.res_date
      from
            [$(Symphony)].dbo.Result_details Result
      where
            Result.res_depid in 
                  (
                  select
                        rps_value
                  from
                        [$(Symphony)].dbo.CFG_ReportSettings
                  where
                        rps_name = 'SpecialtyOutcome'
                  )
      and   Result.res_inactive <> 1
      and not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Result_details PreviousResult
            where
                  PreviousResult.res_depid = Result.res_depid
            and   PreviousResult.res_atdid = Result.res_atdid
            and   PreviousResult.res_inactive <> 1
            and   (
                        PreviousResult.res_date > Result.res_date
                  or    (
                              PreviousResult.res_date = Result.res_date
                        and   PreviousResult.res_resid > Result.res_resid
                        )
                  )
            )
      ) SeenBySpecialty
on    SeenBySpecialty.SourceUniqueID = Encounter.atd_id

left join (
      select
            SourceUniqueID = Request.req_atdid
            ,DecisionToAdmitTime = Request.req_date
      from
            [$(Symphony)].dbo.Request_Details Request
      where
            Request.req_depid in 
                  (
                  select
                        rps_value
                  from
                        [$(Symphony)].dbo.CFG_ReportSettings
                  where
                        rps_name = 'A+E BedRequest'
                  )
      and   Request.req_inactive <> 1
      and not exists
            (
            select
                  1
            from
                  [$(Symphony)].dbo.Request_Details PreviousRequest
            where
                  PreviousRequest.req_depid = Request.req_depid
            and   PreviousRequest.req_atdid = Request.req_atdid
            and   PreviousRequest.req_inactive <> 1
            and   (
                        PreviousRequest.req_date > Request.req_date
                  or    (
                              PreviousRequest.req_date = Request.req_date
                        and   PreviousRequest.req_reqid > Request.req_reqid
                        )
                  )
            )
      ) Admission
on    Admission.SourceUniqueID = Encounter.atd_id

--Ambulance Arrival Times
--CCB 2012-02-07 Optimised
LEFT JOIN (
      select
            atd_EF_atdid
            ,AmbulanceArrivalTimeInt = replace(ltrim(rtrim(atd_EF_Value)), ':', '')
            ,SequenceNo =
                  row_number() over (partition by atd_EF_atdid order by atd_EF_extrafieldID desc)
      from
            [$(Symphony)].dbo.Attendance_Details_ExtraFields AttendanceDetailsExtraFields
      where
            AttendanceDetailsExtraFields.atd_EF_FieldID = 1666 -- Code used to identify Ambulance Arrival Times
      ) AmbulanceArrivalTime
ON    AmbulanceArrivalTime.atd_EF_atdid  = Encounter.atd_id
and   Encounter.atd_arrmode = '6477' -- arrived by Ambulance
and   AmbulanceArrivalTime.SequenceNo = 1

left join 

(
SELECT 

    res_EF_atdid
   ,AcceptingClinician = Lkp_Name 
   ,AcceptingClinicianTime = res_EF_created


 


  FROM [$(Symphony)].[dbo].[Result_Details_ExtraFields] AcceptingClinician
  
  inner join [$(Symphony)].dbo.Lookups Clinician
  on  Clinician.Lkp_ID = AcceptingClinician.res_EF_Value
  and AcceptingClinician. res_EF_FieldID = 2171


) AcceptingClinician
on AcceptingClinician.res_EF_atdid = Encounter.atd_id

where
	Encounter.atd_arrivaldate between cast(getdate()-1 as date) and getdate()
and	Encounter.atd_deleted = 0
and	Episode.epd_deleted = 0



--union

--select
--	Encounter.atd_id SourceUniqueID
--	,DistrictNo.DistrictNo DistrictNo
--	,Postcode =
--		coalesce(
--			  PermanentAddress.add_postcode
--			  ,TemporaryAddress.add_postcode
--		)
--	,Patient.pat_dob DateOfBirth
--	,RegisteredGpPracticeCode = GpPractice.pr_praccode
--	,Encounter.atd_num AttendanceNumber
--	,ArrivalModeCode = Encounter.atd_arrmode
--	,AttendanceCategoryCode =
--		case Encounter.atd_attendancetype
--		when 1
--		then '2' --planned review
--		when 2
--		then '3' --unplanned review
--		else '1' --attend
--		end
--	,AttendanceDisposalCode =
--		right(
--			  '0' + case when Disposal.DisposalCode = '' then null else Disposal.DisposalCode end
--			  ,2
--		)
--	,PatientGroupCode =
--				CASE
--				WHEN PatientGroup.PatientGroupCode  = '' THEN '0'
--				ELSE
--				COALESCE
--					(
--					PatientGroup.PatientGroupCode
--					,'0'
--					)
--				END
--	,dateadd(day, datediff(day, 0, Encounter.atd_arrivaldate), 0) ArrivalDate
--	,Encounter.atd_arrivaldate ArrivalTime
--	,InitialAssessmentTime = Triage.tri_date
--	,SeenForTreatmentTime = Result.res_date
--	,AttendanceConclusionTime = Encounter.atd_dischdate
--	,DepartureTime = Departure.DepartureTime
--	,TriageCategoryCode = Triage.tri_category
--	,PresentingProblemCode = Encounter.atd_complaintid
--	,ToSpecialtyTime = ToSpecialty.InvestigationDate
--	,SeenBySpecialtyTime = SeenBySpecialty.InvestigationResultDate
--	,ReferredToSpecialtyCode = 
--				case
--				when Department.SiteCode = 'RW3RC' and PEDToSpecialty.SpecialtyCode <> 0 then PEDToSpecialty.SpecialtyCode
--				else ToSpecialty.SpecialtyCode
--				end
--	,DecisionToAdmitTime = Admission.DecisionToAdmitTime
--	,DischargeDestinationCode = Encounter.atd_dischdest
--	,SourcePatientNo = Patient.pat_pid
--	,SourceAttendanceDisposalCode = Encounter.atd_dischoutcome
--	,AmbulanceArrivalTime.AmbulanceArrivalTimeInt
--	,SourceCareGroupCode = Encounter.atd_caregroup
--	,TriageComment = 
--		Triage.TriageComment
--from
--	[$(Symphony)].dbo.Attendance_Details Encounter

--inner join [$(Symphony)].dbo.Episodes Episode
--on	Episode.epd_id = Encounter.atd_epdid

--inner join [$(Symphony)].dbo.Patient Patient
--on	Patient.pat_pid = Episode.epd_pid

--inner join [$(Symphony)].dbo.Patient_details PatientDetail
--on	PatientDetail.pdt_pid = Patient.pat_pid

---- 2010-03-05 CCB get previous patient details

--left join [$(Symphony)].dbo.aud_patient_details AuditPatientDetail
--on	AuditPatientDetail.pdt_pid = Episode.epd_pid
--and	AuditPatientDetail.pdt_update < Encounter.atd_dischdate
--and	AuditPatientDetail.pdt_deleted = 0
--and	not exists
--	(
--	select
--		1
--	from
--		[$(Symphony)].dbo.aud_patient_details Previous
--	where
--		Previous.pdt_pid = AuditPatientDetail.pdt_pid
--	and	Previous.pdt_update < Encounter.atd_dischdate
--	and	Previous.pdt_identity > AuditPatientDetail.pdt_identity
--	and	AuditPatientDetail.pdt_deleted = 0
--	)

---- 2010-02-10 CCB modifications to reflect Symphony upgrade to v2.28 BEGIN

---- 2010-05-03 CCB coalesce to ensure we get old details if available and current details if not

--left join [$(Symphony)].dbo.GPPracticeAddressLink GPPracticeAddressLinkOld
--on	GPPracticeAddressLinkOld.gpa_gpid = AuditPatientDetail.pdt_gpid
--and GPPracticeAddressLinkOld.gpa_prid = AuditPatientDetail.pdt_practise
--and GPPracticeAddressLinkOld.gpa_praddid = AuditPatientDetail.pdt_praddid

--left join [$(Symphony)].dbo.GPPracticeAddressLink GPPracticeAddressLinkCurrent
--on	GPPracticeAddressLinkCurrent.gpa_gpid = PatientDetail.pdt_gpid
--and GPPracticeAddressLinkCurrent.gpa_prid = PatientDetail.pdt_practise
--and GPPracticeAddressLinkCurrent.gpa_praddid = PatientDetail.pdt_praddid

--left join [$(Symphony)].dbo.GP_practise GpPractice 
--on	GpPractice.pr_id = coalesce(GPPracticeAddressLinkOld.gpa_prid, GPPracticeAddressLinkCurrent.gpa_prid)

--left join [$(Symphony)].dbo.GP Gp 
--on	Gp.gp_id = coalesce(GPPracticeAddressLinkOld.gpa_gpid, GPPracticeAddressLinkCurrent.gpa_gpid)

---- 2010-02-10 CCB modifications to reflect Symphony upgrade to v2.28 END

--inner join 
--      (
--	select
--		Department.dpt_id
--		,CommissioningSerialNo = '5NT00A'

--		,PCTCode = '5NT'

--		,SiteCode =
--		case
--		when Department.dpt_name = 'MRI ED' then 'RW3MR'
--		when Department.dpt_name = 'PED' then 'RW3RC'
--		when Department.dpt_name = 'EEC' then 'RW3RE'
--		when Department.dpt_name = 'St Mary''s' then 'RW3SM'
--		when Department.dpt_name = 'Dental' then 'RW3DH'
--		end
--	from
--		[$(Symphony)].dbo.CFG_Dept Department
--	where
--		Department.dpt_name in (
--			  'MRI ED'
--			  ,'PED'
--			  ,'EEC'
--			  ,'St Mary''s'
--			  ,'Dental'
--			  )
--      ) Department
--on    Department.dpt_id = Episode.epd_deptid

--left join
--      (
--      select distinct
--            DistrictNo = patient_system_ids.psi_system_id 
--            ,psi_pid
--      from
--            [$(Symphony)].dbo.Patient_system_ids patient_system_ids
--      where
--            patient_system_ids.psi_system_name = 
--                  (
--                  select
--                        lkp_id
--                  from
--                        [$(Symphony)].dbo.Lookups
--                  where
--                        lkp_name = 'District Number'
--                  )
--      and   not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Patient_system_ids PreviousDistrictNo
--            where
--                  PreviousDistrictNo.psi_system_name = patient_system_ids.psi_system_name
--            and   PreviousDistrictNo.psi_pid = patient_system_ids.psi_pid
--            and   PreviousDistrictNo.psi_system_id > patient_system_ids.psi_system_id
--            )
--      ) DistrictNo
--on    DistrictNo.psi_pid = Patient.pat_pid

--left join
--      (
--      select
--            *
--      from
--            [$(Symphony)].dbo.Address PermanentAddress
--      where
--            PermanentAddress.add_linktype = 1
--      and   PermanentAddress.add_type = 2673
----    and   PermanentAddress.add_type in 
----          (
----          select
----                Lkp_ID
----          from
----                [$(Symphony)].dbo.lookups AddressType
----          where
----                AddressType.Lkp_Name = 'Permanent address'
----          )
--      and not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Address PreviousPermanentAddress
--            where
--                  PreviousPermanentAddress.add_linkid = PermanentAddress.add_linkid
--            and   PreviousPermanentAddress.add_linktype = PermanentAddress.add_linktype
--            and   PreviousPermanentAddress.add_type = PermanentAddress.add_type
--            and   PreviousPermanentAddress.add_update > PermanentAddress.add_update
--            )
--      ) PermanentAddress
--on    PermanentAddress.add_linkid = PatientDetail.pdt_pid

--left join
--      (
--      select
--            *
--      from
--            [$(Symphony)].dbo.Address TemporaryAddress
--      where
--            TemporaryAddress.add_linktype = 1
--      and   TemporaryAddress.add_type in 
--            (
--            4993
--            ,10522
----          select
----                Lkp_ID
----          from
----                [$(Symphony)].dbo.lookups AddressType
----          where
----                AddressType.Lkp_Name = 'Permanent address'
--            )
--      and not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Address PreviousTemporaryAddress
--            where
--                  PreviousTemporaryAddress.add_linkid = TemporaryAddress.add_linkid
--            and   PreviousTemporaryAddress.add_linktype = TemporaryAddress.add_linktype
--            and   PreviousTemporaryAddress.add_type = TemporaryAddress.add_type
--            and   PreviousTemporaryAddress.add_update > TemporaryAddress.add_update
--            )
--      ) TemporaryAddress
--on    TemporaryAddress.add_linkid = PatientDetail.pdt_pid

--left join
--      (
--      select distinct
--            DisposalId = flm_lkpid
--            ,DisposalCode = not_text
--      from
--            [$(Symphony)].dbo.Notes

--      inner join [$(Symphony)].dbo.LookupMappings
--      on    flm_value = not_noteid
--      and   flm_mtid = 
--            (
--            select
--                  mt_id
--            from
--                  [$(Symphony)].dbo.MappingTypes  
--            where
--                  mt_name = 'CDS'
--            )
--      ) Disposal
--on    Disposal.DisposalId = Encounter.atd_dischoutcome

--left join
--      (
--      select distinct
--            PatientGroupId = flm_lkpid
--            ,PatientGroupCode = not_text
--      from
--            [$(Symphony)].dbo.Notes

--      inner join [$(Symphony)].dbo.LookupMappings
--      on    flm_value = not_noteid
--      and   flm_mtid = 
--            (
--            select
--                  mt_id
--            from
--                  [$(Symphony)].dbo.MappingTypes  
--            where
--                  mt_name = 'CDS'
--            )
--      ) PatientGroup
--on    PatientGroup.PatientGroupId = Encounter.atd_reason

--left join
--      (
--      select distinct
--            SourceOfReferralId = flm_lkpid
--            ,SourceOfReferralCode = not_text
--      from
--            [$(Symphony)].dbo.Notes

--      inner join [$(Symphony)].dbo.LookupMappings
--      on    flm_value = not_noteid
--      and   flm_mtid = 
--            (
--            select
--                  mt_id
--            from
--                  [$(Symphony)].dbo.MappingTypes  
--            where
--                  mt_name = 'CDS'
--            )
--      ) SourceOfReferral
--on    SourceOfReferral.SourceOfReferralId = Encounter.atd_refsource

--left join (
--      select
--            Triage.*
--            ,TriageComment = 
--                  Triagenotes.not_text
--      from
--            [$(Symphony)].dbo.Triage Triage
--      left outer join [$(Symphony)].dbo.Notes TriageNotes 
--      on Triage.tri_comments = TriageNotes.not_noteid
--      where
--            Triage.tri_inactive = 0
--      and   not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Triage LatestTriage
--            where
--                  LatestTriage.tri_inactive = 0
--            and   LatestTriage.tri_atdid = Triage.tri_atdid
--            and   LatestTriage.tri_trid < Triage.tri_trid
--            )
--      ) Triage
--on    Triage.tri_atdid = Encounter.atd_id

----(9s)
--left join --get the first clinician outcome based result date
--      (
--      select distinct
--            Result.res_atdid
--            ,Result.res_date
--            ,Staff.stf_staffid
--      from
--            [$(Symphony)].dbo.Result_details Result

--      inner join [$(Symphony)].dbo.Staff Staff
--      on    Staff.stf_staffid = Result.res_staff1

--      where
--            Result.res_depid IN ( 86 , 195 ) -- CCB 2011-10-24 the code below is returning DEP 174, which is 'Call MRI'
--                  --(
--                  --select
--                  --    rps_value
--                  --from
--                  --    [$(Symphony)].dbo.CFG_ReportSettings
--                  --where
--                  --    rps_name IN('SeeA+EClinicianOutcome','See A+E ClinicianOutcome') -- = 'SeeA+EClinicianOutcome'  GS20111019  Additional label added to bring through dental depID 195
--                  --)
--      and   Result.res_inactive <> 1
--      and   not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Result_details LaterResult

--            inner join [$(Symphony)].dbo.Staff Staff
--            on    Staff.stf_staffid = LaterResult.res_staff1

--            where
--                  LaterResult.res_depid IN ( 86 , 195 ) 
--                        --(
--                        --select
--                        --    rps_value
--                        --from
--                        --    [$(Symphony)].dbo.CFG_ReportSettings
--                        --where
--                        --    rps_name IN('SeeA+EClinicianOutcome','See A+E ClinicianOutcome') -- = 'SeeA+EClinicianOutcome' GS20111019  Additional label added to bring through dental depID 195
--                        --)
--            and   LaterResult.res_inactive <> 1
--            and   LaterResult.res_atdid = Result.res_atdid
--            and (
--                        LaterResult.res_date < Result.res_date
--                  or    (
--                              LaterResult.res_date = Result.res_date
--                        and   LaterResult.res_resid < Result.res_resid
--                        )
--                  )
--            )
--      ) Result
--on    Result.res_atdid = Encounter.atd_id

----(9s)
--left join
--      (
--      select
--            Location.cul_atdid
--            ,DepartureTime = Location.cul_locationdate
--      from
--            [$(Symphony)].dbo.Current_Locations Location
--      where
--            Location.cul_locationid in 
--            (
--            select
--                  loc_id
--            from
--                  [$(Symphony)].dbo.CFG_Locations
--            where
--                  loc_name = 'Left Department'
--            )
--      and   not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Current_Locations PreviousLocation
--            where
--                  PreviousLocation.cul_locationid in 
--                  (
--                  select
--                        loc_id
--                  from
--                        [$(Symphony)].dbo.CFG_Locations
--                  where
--                        loc_name = 'Left Department'
--                  )
--            and   PreviousLocation.cul_atdid = Location.cul_atdid
--            and (
--                        --PreviousLocation.cul_locationdate > Location.cul_locationdate
--                        PreviousLocation.cul_update > Location.cul_update
--                  or
--                        (
--                              --PreviousLocation.cul_locationdate = Location.cul_locationdate
--                              PreviousLocation.cul_locationdate = Location.cul_update
--                        and   PreviousLocation.cul_culid > Location.cul_culid
--                        )
--                  )
--            )
--      ) Departure
--on    Departure.cul_atdid = Encounter.atd_id

----left join [$(Symphony)].dbo.lookups ArrivalMode
----on  ArrivalMode.lkp_id = Encounter.atd_arrmode

--left join (
--      select
--            SourceUniqueID = Request.req_atdid
--            ,InvestigationDate = Request.req_date
--            ,SpecialtyCode = Request.req_request
--      from
--            [$(Symphony)].dbo.Request_Details Request
--      where
--            Request.req_depid in 
--                  (
--                  select
--                        rps_value
--                  from
--                        [$(Symphony)].dbo.CFG_ReportSettings
--                  where
--                        rps_name = 'Specialty'
--                  )
--      and   Request.req_inactive <> 1
--      and not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Request_Details PreviousRequest
--            where
--                  PreviousRequest.req_depid = Request.req_depid
--            and   PreviousRequest.req_atdid = Request.req_atdid
--            and   PreviousRequest.req_inactive <> 1
--            and   (
--                        PreviousRequest.req_date > Request.req_date
--                  or    (
--                              PreviousRequest.req_date = Request.req_date
--                        and   PreviousRequest.req_reqid > Request.req_reqid
--                        )
--                  )
--            )
--      ) ToSpecialty
--on    ToSpecialty.SourceUniqueID = Encounter.atd_id

--left join (
--      select
--            SourceUniqueID = Request.req_atdid
--            ,InvestigationDate = Request.req_date
--            ,SpecialtyCode =  Request.req_field1
--      from
--            [$(Symphony)].dbo.Request_Details Request
--      where
--            Request.req_depid in 
--                  (
--                  select
--                        rps_value
--                  from
--                        [$(Symphony)].dbo.CFG_ReportSettings
--                  where
--                        rps_name = 'Specialty'
--                  )
--      and   Request.req_inactive <> 1
--      and not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Request_Details PreviousRequest
--            where
--                  PreviousRequest.req_depid = Request.req_depid
--            and   PreviousRequest.req_atdid = Request.req_atdid
--            and   PreviousRequest.req_inactive <> 1
--            and   (
--                        PreviousRequest.req_date > Request.req_date
--                  or    (
--                              PreviousRequest.req_date = Request.req_date
--                        and   PreviousRequest.req_reqid > Request.req_reqid
--                        )
--                  )
--            )
--      ) PEDToSpecialty
--on    PEDToSpecialty.SourceUniqueID = Encounter.atd_id

--left join (
--      select
--            SourceUniqueID = Result.res_atdid
--            ,InvestigationResultDate = Result.res_date
--      from
--            [$(Symphony)].dbo.Result_details Result
--      where
--            Result.res_depid in 
--                  (
--                  select
--                        rps_value
--                  from
--                        [$(Symphony)].dbo.CFG_ReportSettings
--                  where
--                        rps_name = 'SpecialtyOutcome'
--                  )
--      and   Result.res_inactive <> 1
--      and not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Result_details PreviousResult
--            where
--                  PreviousResult.res_depid = Result.res_depid
--            and   PreviousResult.res_atdid = Result.res_atdid
--            and   PreviousResult.res_inactive <> 1
--            and   (
--                        PreviousResult.res_date > Result.res_date
--                  or    (
--                              PreviousResult.res_date = Result.res_date
--                        and   PreviousResult.res_resid > Result.res_resid
--                        )
--                  )
--            )
--      ) SeenBySpecialty
--on    SeenBySpecialty.SourceUniqueID = Encounter.atd_id

--left join (
--      select
--            SourceUniqueID = Request.req_atdid
--            ,DecisionToAdmitTime = Request.req_date
--      from
--            [$(Symphony)].dbo.Request_Details Request
--      where
--            Request.req_depid in 
--                  (
--                  select
--                        rps_value
--                  from
--                        [$(Symphony)].dbo.CFG_ReportSettings
--                  where
--                        rps_name = 'A+E BedRequest'
--                  )
--      and   Request.req_inactive <> 1
--      and not exists
--            (
--            select
--                  1
--            from
--                  [$(Symphony)].dbo.Request_Details PreviousRequest
--            where
--                  PreviousRequest.req_depid = Request.req_depid
--            and   PreviousRequest.req_atdid = Request.req_atdid
--            and   PreviousRequest.req_inactive <> 1
--            and   (
--                        PreviousRequest.req_date > Request.req_date
--                  or    (
--                              PreviousRequest.req_date = Request.req_date
--                        and   PreviousRequest.req_reqid > Request.req_reqid
--                        )
--                  )
--            )
--      ) Admission
--on    Admission.SourceUniqueID = Encounter.atd_id

----Ambulance Arrival Times
----CCB 2012-02-07 Optimised
--LEFT JOIN (
--      select
--            atd_EF_atdid
--            ,AmbulanceArrivalTimeInt = replace(ltrim(rtrim(atd_EF_Value)), ':', '')
--            ,SequenceNo =
--                  row_number() over (partition by atd_EF_atdid order by atd_EF_extrafieldID desc)
--      from
--            [$(Symphony)].dbo.Attendance_Details_ExtraFields AttendanceDetailsExtraFields
--      where
--            AttendanceDetailsExtraFields.atd_EF_FieldID = 1666 -- Code used to identify Ambulance Arrival Times
--      ) AmbulanceArrivalTime
--ON    AmbulanceArrivalTime.atd_EF_atdid  = Encounter.atd_id
--and   Encounter.atd_arrmode = '6477' -- arrived by Ambulance
--and   AmbulanceArrivalTime.SequenceNo = 1

--where
--	--coalesce(Departure.DepartureTime,getdate()) between cast(getdate() as date) and getdate()
--	Departure.DepartureTime between cast(getdate() as date) and getdate()
--and	Encounter.atd_deleted = 0
--and	Episode.epd_deleted = 0
GO
GRANT EXECUTE
    ON OBJECT::[AE].[PerfectWeekEncounter] TO [CMMC\Anthony.Smith2]
    AS [dbo];


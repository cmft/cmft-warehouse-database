﻿CREATE TABLE [AE].[EthnicCategory] (
    [EthnicCategoryCode]         INT           NOT NULL,
    [EthnicCategory]             VARCHAR (255) NULL,
    [EthnicCategoryNationalCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_EthnicCategory] PRIMARY KEY CLUSTERED ([EthnicCategoryCode] ASC)
);


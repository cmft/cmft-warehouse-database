﻿CREATE TABLE [AE].[AttendanceDisposalDestination] (
    [AttendanceDisposalDestinationCode]         INT           NOT NULL,
    [AttendanceDisposalDestination]             VARCHAR (255) NULL,
    [AttendanceDisposalDestinationNationalCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_AttendanceDisposalDestination] PRIMARY KEY CLUSTERED ([AttendanceDisposalDestinationCode] ASC)
);


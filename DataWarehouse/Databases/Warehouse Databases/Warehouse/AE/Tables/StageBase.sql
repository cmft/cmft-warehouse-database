﻿CREATE TABLE [AE].[StageBase] (
    [StageID]     INT           IDENTITY (1, 1) NOT NULL,
    [StageCode]   VARCHAR (50)  NOT NULL,
    [Stage]       VARCHAR (255) NOT NULL,
    [BreachValue] INT           NULL,
    CONSTRAINT [PK_StageBase] PRIMARY KEY CLUSTERED ([StageCode] ASC)
);


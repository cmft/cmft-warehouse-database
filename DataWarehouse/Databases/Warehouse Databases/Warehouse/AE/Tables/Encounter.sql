﻿CREATE TABLE [AE].[Encounter] (
    [EncounterRecno]                      INT            IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]                      VARCHAR (50)   NOT NULL,
    [UniqueBookingReferenceNo]            VARCHAR (50)   NULL,
    [PathwayId]                           VARCHAR (50)   NULL,
    [PathwayIdIssuerCode]                 VARCHAR (10)   NULL,
    [RTTStatusCode]                       VARCHAR (10)   NULL,
    [RTTStartDate]                        SMALLDATETIME  NULL,
    [RTTEndDate]                          SMALLDATETIME  NULL,
    [DistrictNo]                          VARCHAR (50)   NULL,
    [TrustNo]                             VARCHAR (50)   NULL,
    [CasenoteNo]                          VARCHAR (50)   NULL,
    [DistrictNoOrganisationCode]          VARCHAR (10)   NULL,
    [NHSNumber]                           VARCHAR (17)   NULL,
    [NHSNumberStatusId]                   VARCHAR (10)   NULL,
    [PatientTitle]                        VARCHAR (50)   NULL,
    [PatientForename]                     VARCHAR (100)  NULL,
    [PatientSurname]                      VARCHAR (100)  NULL,
    [PatientAddress1]                     VARCHAR (50)   NULL,
    [PatientAddress2]                     VARCHAR (50)   NULL,
    [PatientAddress3]                     VARCHAR (50)   NULL,
    [PatientAddress4]                     VARCHAR (50)   NULL,
    [Postcode]                            VARCHAR (10)   NULL,
    [DateOfBirth]                         DATETIME       NULL,
    [DateOfDeath]                         SMALLDATETIME  NULL,
    [SexCode]                             VARCHAR (10)   NULL,
    [CarerSupportIndicator]               BIT            NULL,
    [RegisteredGpCode]                    VARCHAR (10)   NULL,
    [RegisteredGpPracticeCode]            VARCHAR (10)   NULL,
    [AttendanceNumber]                    VARCHAR (MAX)  NULL,
    [ArrivalModeCode]                     INT            NULL,
    [AttendanceCategoryCode]              INT            NULL,
    [AttendanceDisposalCode]              VARCHAR (10)   NULL,
    [IncidentLocationTypeCode]            VARCHAR (10)   NULL,
    [PatientGroupCode]                    VARCHAR (10)   NULL,
    [SourceOfReferralCode]                VARCHAR (10)   NULL,
    [ArrivalDate]                         SMALLDATETIME  NULL,
    [ArrivalTime]                         SMALLDATETIME  NULL,
    [AgeOnArrival]                        TINYINT        NULL,
    [InitialAssessmentTime]               SMALLDATETIME  NULL,
    [SeenForTreatmentTime]                SMALLDATETIME  NULL,
    [AttendanceConclusionTime]            SMALLDATETIME  NULL,
    [DepartureTime]                       SMALLDATETIME  NULL,
    [CommissioningSerialNo]               VARCHAR (50)   NULL,
    [NHSServiceAgreementLineNo]           VARCHAR (50)   NULL,
    [ProviderReferenceNo]                 VARCHAR (50)   NULL,
    [CommissionerReferenceNo]             VARCHAR (50)   NULL,
    [ProviderCode]                        VARCHAR (10)   NULL,
    [CommissionerCode]                    VARCHAR (10)   NULL,
    [StaffMemberCode]                     VARCHAR (20)   NULL,
    [InvestigationCodeFirst]              VARCHAR (10)   NULL,
    [InvestigationCodeSecond]             VARCHAR (10)   NULL,
    [DiagnosisCodeFirst]                  VARCHAR (10)   NULL,
    [DiagnosisCodeSecond]                 VARCHAR (10)   NULL,
    [TreatmentCodeFirst]                  VARCHAR (10)   NULL,
    [TreatmentCodeSecond]                 VARCHAR (10)   NULL,
    [PASHRGCode]                          VARCHAR (10)   NULL,
    [HRGVersionCode]                      VARCHAR (10)   NULL,
    [PASDGVPCode]                         VARCHAR (10)   NULL,
    [SiteCode]                            VARCHAR (10)   NULL,
    [Created]                             SMALLDATETIME  CONSTRAINT [DF_Encounter_Created] DEFAULT (getdate()) NULL,
    [Updated]                             SMALLDATETIME  NULL,
    [ByWhom]                              VARCHAR (50)   CONSTRAINT [DF_Encounter_ByWhom] DEFAULT (suser_name()) NULL,
    [InterfaceCode]                       VARCHAR (5)    NULL,
    [PCTCode]                             VARCHAR (8)    NULL,
    [ResidencePCTCode]                    VARCHAR (10)   NULL,
    [InvestigationCodeList]               VARCHAR (16)   NULL,
    [TriageCategoryCode]                  VARCHAR (10)   NULL,
    [PresentingProblem]                   VARCHAR (4000) NULL,
    [PresentingProblemCode]               VARCHAR (10)   NULL,
    [ToXrayTime]                          SMALLDATETIME  NULL,
    [FromXrayTime]                        SMALLDATETIME  NULL,
    [ToSpecialtyTime]                     SMALLDATETIME  NULL,
    [SeenBySpecialtyTime]                 SMALLDATETIME  NULL,
    [EthnicCategoryCode]                  VARCHAR (10)   NULL,
    [ReferredToSpecialtyCode]             INT            NULL,
    [DecisionToAdmitTime]                 SMALLDATETIME  NULL,
    [DischargeDestinationCode]            VARCHAR (10)   NULL,
    [RegisteredTime]                      SMALLDATETIME  NULL,
    [TransportRequestTime]                SMALLDATETIME  NULL,
    [TransportAvailableTime]              SMALLDATETIME  NULL,
    [AscribeLeftDeptTime]                 SMALLDATETIME  NULL,
    [CDULeftDepartmentTime]               SMALLDATETIME  NULL,
    [PCDULeftDepartmentTime]              SMALLDATETIME  NULL,
    [TreatmentDateFirst]                  SMALLDATETIME  NULL,
    [TreatmentDateSecond]                 SMALLDATETIME  NULL,
    [SourceAttendanceDisposalCode]        INT            NULL,
    [AmbulanceArrivalTime]                SMALLDATETIME  NULL,
    [AmbulanceCrewPRF]                    VARCHAR (30)   NULL,
    [UnplannedReattend7Day]               INT            NULL,
    [ArrivalTimeAdjusted]                 SMALLDATETIME  NULL,
    [ClinicalAssessmentTime]              SMALLDATETIME  NULL,
    [Reportable]                          BIT            NULL,
    [SourceCareGroupCode]                 INT            NULL,
    [AlcoholLocation]                     VARCHAR (80)   NULL,
    [AlcoholInPast12Hours]                VARCHAR (20)   NULL,
    [RapidAssessmentTime]                 SMALLDATETIME  NULL,
    [CarePathwayDepartureTime]            SMALLDATETIME  NULL,
    [CarePathwayAttendanceConclusionTime] SMALLDATETIME  NULL,
    [TriageComment]                       VARCHAR (4000) NULL,
    [CCGCode]                             VARCHAR (10)   NULL,
    [EpisodicPostCode]                    VARCHAR (8)    NULL,
    [GpCodeAtAttendance]                  VARCHAR (8)    NULL,
    [GpPracticeCodeAtAttendance]          VARCHAR (8)    NULL,
    [PostcodeAtAttendance]                VARCHAR (8)    NULL,
    [ReligionCode]                        VARCHAR (10)   NULL,
    [BriefHistory]                        VARCHAR (MAX)  NULL,
    [TriagedByCode]                       INT            NULL,
    [DischargedByCode]                    INT            NULL,
    [SchoolCode]                          INT            NULL,
    [OtherSchool]                         VARCHAR (100)  NULL,
    [DutyClinicianCode]                   INT            NULL,
    [InformationForGP]                    VARCHAR (MAX)  NULL,
    [AuditCScore]                         INT            NULL,
    [SourcePatientNo]                     INT            NULL,
    CONSTRAINT [PK_EncounterBase] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC)
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Encounter_5]
    ON [AE].[Encounter]([SourceUniqueID] ASC, [InterfaceCode] ASC);


GO
CREATE NONCLUSTERED INDEX [Idx_AE_Encounter_InterfaceCode]
    ON [AE].[Encounter]([InterfaceCode] ASC)
    INCLUDE([EncounterRecno], [ArrivalDate]);


GO
CREATE NONCLUSTERED INDEX [IX_Encounter_DistrictNo]
    ON [AE].[Encounter]([DistrictNo] ASC);


GO
CREATE NONCLUSTERED INDEX [index_Arrivaldate]
    ON [AE].[Encounter]([ArrivalDate] ASC)
    INCLUDE([EncounterRecno], [Reportable]);


GO
CREATE NONCLUSTERED INDEX [INDEX_Pat_Demographics]
    ON [AE].[Encounter]([InterfaceCode] ASC)
    INCLUDE([ArrivalTime], [DateOfBirth], [DepartureTime], [PatientForename], [PatientSurname], [Postcode], [SexCode]);


GO
CREATE NONCLUSTERED INDEX [Index_admit_discharge_pat_demographics]
    ON [AE].[Encounter]([SiteCode] ASC, [InterfaceCode] ASC, [DischargeDestinationCode] ASC, [Reportable] ASC, [ArrivalDate] ASC, [DepartureTime] ASC, [SourceAttendanceDisposalCode] ASC)
    INCLUDE([ArrivalTime], [DateOfBirth], [EncounterRecno], [PatientForename], [PatientSurname], [Postcode], [SexCode]);


GO
CREATE NONCLUSTERED INDEX [IX_AEC_AttDispCode-SiteCode]
    ON [AE].[Encounter]([AttendanceDisposalCode] ASC, [SiteCode] ASC)
    INCLUDE([DistrictNo], [ArrivalTime], [DepartureTime]);


GO
CREATE NONCLUSTERED INDEX [ix_aeencounter_adc_rep]
    ON [AE].[Encounter]([AttendanceDisposalCode] ASC, [Reportable] ASC)
    INCLUDE([EncounterRecno], [DistrictNo], [DepartureTime]);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Warehouse assigned unique record number', @level0type = N'SCHEMA', @level0name = N'AE', @level1type = N'TABLE', @level1name = N'Encounter', @level2type = N'COLUMN', @level2name = N'EncounterRecno';


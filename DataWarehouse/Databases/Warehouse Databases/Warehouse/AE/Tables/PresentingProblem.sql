﻿CREATE TABLE [AE].[PresentingProblem] (
    [PresentingProblemCode]         INT          NOT NULL,
    [PresentingProblem]             VARCHAR (80) NOT NULL,
    [PresentingProblemNationalCode] VARCHAR (1)  NULL,
    CONSTRAINT [PK_PresentingProblem] PRIMARY KEY CLUSTERED ([PresentingProblemCode] ASC)
);


﻿CREATE TABLE [AE].[DischargeDestination] (
    [DischargeDestinationCode] INT           NOT NULL,
    [DischargeDestination]     VARCHAR (255) NULL,
    CONSTRAINT [PK_DischargeDestination] PRIMARY KEY CLUSTERED ([DischargeDestinationCode] ASC)
);


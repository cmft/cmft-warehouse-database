﻿CREATE TABLE [AE].[Assault] (
    [AssaultRecno]               INT            IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]             VARCHAR (50)   NULL,
    [AESourceUniqueID]           VARCHAR (50)   NOT NULL,
    [AssaultDate]                DATE           NULL,
    [AssaultTime]                SMALLDATETIME  NULL,
    [AssaultWeapon]              VARCHAR (80)   NULL,
    [AssaultWeaponDetails]       VARCHAR (4000) NULL,
    [AssaultLocation]            VARCHAR (80)   NULL,
    [AssaultLocationDetails]     VARCHAR (4000) NULL,
    [AlcoholConsumed3Hour]       VARCHAR (80)   NULL,
    [AssaultRelationship]        VARCHAR (80)   NULL,
    [AssaultRelationshipDetails] VARCHAR (4000) NULL,
    CONSTRAINT [PK_AssaultBase] PRIMARY KEY CLUSTERED ([AssaultRecno] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Assault_AESourceUniqueID]
    ON [AE].[Assault]([AESourceUniqueID] ASC);


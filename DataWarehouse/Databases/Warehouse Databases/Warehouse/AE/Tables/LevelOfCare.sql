﻿CREATE TABLE [AE].[LevelOfCare] (
    [LevelOfCareCode] INT         NOT NULL,
    [LevelOfCare]     VARCHAR (6) NOT NULL,
    CONSTRAINT [PK_LevelOfCare] PRIMARY KEY CLUSTERED ([LevelOfCareCode] ASC)
);


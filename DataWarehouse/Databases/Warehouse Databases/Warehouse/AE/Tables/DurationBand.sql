﻿CREATE TABLE [AE].[DurationBand] (
    [DurationBandCode]            INT          NOT NULL,
    [MinuteBand]                  VARCHAR (6)  NOT NULL,
    [FifteenMinuteBandCode]       INT          NULL,
    [FifteenMinuteBand]           VARCHAR (11) NOT NULL,
    [ThirtyMinuteBandCode]        INT          NULL,
    [ThirtyMinuteBand]            VARCHAR (11) NOT NULL,
    [HourBandCode]                NCHAR (10)   NULL,
    [HourBand]                    VARCHAR (11) NOT NULL,
    [BreachFifteenMinuteBandCode] INT          NULL,
    [BreachFifteenMinuteBand]     VARCHAR (12) NULL,
    [BreachThirtyMinuteBandCode]  INT          NULL,
    [BreachThirtyMinuteBand]      VARCHAR (12) NULL,
    [BreachHourBandCode]          NCHAR (10)   NULL,
    [BreachHourBand]              VARCHAR (12) NULL,
    [BreachHourBandGroupCode]     NCHAR (10)   NULL,
    [BreachHourBandGroup]         VARCHAR (12) NULL
);


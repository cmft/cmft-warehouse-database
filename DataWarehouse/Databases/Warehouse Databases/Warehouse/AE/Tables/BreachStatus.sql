﻿CREATE TABLE [AE].[BreachStatus] (
    [BreachStatusID]   INT          NOT NULL,
    [BreachStatusCode] VARCHAR (10) NOT NULL,
    [BreachStatus]     VARCHAR (50) NOT NULL,
    [DischargeStatus]  VARCHAR (50) NULL,
    CONSTRAINT [PK_BreachStatus] PRIMARY KEY CLUSTERED ([BreachStatusCode] ASC)
);


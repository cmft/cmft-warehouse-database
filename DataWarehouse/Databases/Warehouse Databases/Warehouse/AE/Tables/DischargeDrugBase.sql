﻿CREATE TABLE [AE].[DischargeDrugBase] (
    [DrugCode] INT           NOT NULL,
    [Drug]     VARCHAR (100) NULL,
    CONSTRAINT [PK__Discharg__EEC5E966699E5D3B] PRIMARY KEY CLUSTERED ([DrugCode] ASC)
);


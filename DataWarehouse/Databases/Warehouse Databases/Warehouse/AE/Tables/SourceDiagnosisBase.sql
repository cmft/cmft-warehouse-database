﻿CREATE TABLE [AE].[SourceDiagnosisBase] (
    [SourceDiagnosisCode] INT          NOT NULL,
    [SourceDiagnosis]     VARCHAR (80) NULL,
    PRIMARY KEY CLUSTERED ([SourceDiagnosisCode] ASC)
);


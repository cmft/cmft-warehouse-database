﻿CREATE TABLE [AE].[ArrivalMode] (
    [ArrivalModeCode]         INT           NOT NULL,
    [ArrivalMode]             VARCHAR (255) NULL,
    [ArrivalModeNationalCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_ArrivalMode] PRIMARY KEY CLUSTERED ([ArrivalModeCode] ASC)
);


﻿CREATE TABLE [AE].[InvestigationList] (
    [InvestigationCode] INT NOT NULL,
    CONSTRAINT [PK_AE.InvestigationList] PRIMARY KEY CLUSTERED ([InvestigationCode] ASC)
);


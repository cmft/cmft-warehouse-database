﻿CREATE TABLE [AE].[Investigation] (
    [InvestigationRecno]       INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]           VARCHAR (50)  NULL,
    [InvestigationDate]        SMALLDATETIME NULL,
    [SequenceNo]               TINYINT       NULL,
    [InvestigationSchemeInUse] VARCHAR (10)  NULL,
    [InvestigationCode]        VARCHAR (50)  NULL,
    [AESourceUniqueID]         VARCHAR (50)  NULL,
    [SourceInvestigationCode]  VARCHAR (50)  NULL,
    [SourceSequenceNo]         TINYINT       NULL,
    [ResultDate]               SMALLDATETIME NULL,
    [Created]                  DATETIME      CONSTRAINT [DF__Investiga__Creat__2341BA42] DEFAULT (getdate()) NOT NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  CONSTRAINT [DF__Investiga__ByWho__2435DE7B] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK_Investigation] PRIMARY KEY CLUSTERED ([InvestigationRecno] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_Investigation]
    ON [AE].[Investigation]([AESourceUniqueID] ASC, [InvestigationCode] ASC);


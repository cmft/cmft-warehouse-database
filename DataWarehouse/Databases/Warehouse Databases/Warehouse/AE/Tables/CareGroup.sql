﻿CREATE TABLE [AE].[CareGroup] (
    [CareGroupCode]     INT           IDENTITY (1, 1) NOT NULL,
    [CareGroup]         VARCHAR (255) NOT NULL,
    [CareGroupTypeCode] VARCHAR (10)  NOT NULL,
    CONSTRAINT [PK_CareGroup] PRIMARY KEY CLUSTERED ([CareGroupCode] ASC),
    CONSTRAINT [FK_CareGroup_CareGroupType] FOREIGN KEY ([CareGroupTypeCode]) REFERENCES [AE].[CareGroupType] ([CareGroupTypeCode])
);


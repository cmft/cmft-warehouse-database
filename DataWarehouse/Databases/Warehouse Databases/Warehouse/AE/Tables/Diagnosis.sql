﻿CREATE TABLE [AE].[Diagnosis] (
    [DiagnosisRecno]          INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]          VARCHAR (50) NOT NULL,
    [SequenceNo]              SMALLINT     NOT NULL,
    [DiagnosticSchemeInUse]   VARCHAR (10) NULL,
    [DiagnosisCode]           VARCHAR (50) NULL,
    [AESourceUniqueID]        VARCHAR (50) NULL,
    [SourceDiagnosisCode]     VARCHAR (50) NULL,
    [SourceDiagnosisSiteCode] VARCHAR (50) NULL,
    [SourceDiagnosisSideCode] VARCHAR (50) NULL,
    [SourceSequenceNo]        SMALLINT     NULL,
    [DiagnosisSiteCode]       VARCHAR (10) NULL,
    [DiagnosisSideCode]       VARCHAR (10) NULL,
    [Created]                 DATETIME     CONSTRAINT [DF__Diagnosis__Creat__215971D0] DEFAULT (getdate()) NOT NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) CONSTRAINT [DF__Diagnosis__ByWho__224D9609] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK_Diagnosis] PRIMARY KEY CLUSTERED ([DiagnosisRecno] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_Diagnosis_2]
    ON [AE].[Diagnosis]([AESourceUniqueID] ASC, [DiagnosisCode] ASC);


﻿CREATE TABLE [AE].[SourceOfReferral] (
    [SourceOfReferralCode] VARCHAR (5)  NOT NULL,
    [SourceOfReferral]     VARCHAR (80) NOT NULL,
    CONSTRAINT [PK_SourceOfReferral] PRIMARY KEY CLUSTERED ([SourceOfReferralCode] ASC)
);




﻿CREATE TABLE [AE].[Site] (
    [SiteCode]       VARCHAR (10) NOT NULL,
    [Site]           VARCHAR (50) NULL,
    [MappedSiteCode] VARCHAR (10) NULL,
    CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED ([SiteCode] ASC),
    CONSTRAINT [FK_Site_Site1] FOREIGN KEY ([MappedSiteCode]) REFERENCES [WH].[Site] ([SiteCode])
);


﻿CREATE TABLE [AE].[Procedure] (
    [ProcedureRecno]       INT           IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]       VARCHAR (50)  NULL,
    [ProcedureDate]        SMALLDATETIME NULL,
    [SequenceNo]           TINYINT       NULL,
    [ProcedureSchemeInUse] VARCHAR (10)  NULL,
    [ProcedureCode]        VARCHAR (50)  NULL,
    [AESourceUniqueID]     VARCHAR (50)  NULL,
    [SourceProcedureCode]  VARCHAR (50)  NULL,
    [SourceSequenceNo]     TINYINT       NULL,
    [Created]              DATETIME      CONSTRAINT [DF__Procedure__Creat__252A02B4] DEFAULT (getdate()) NOT NULL,
    [Updated]              DATETIME      NULL,
    [ByWhom]               VARCHAR (50)  CONSTRAINT [DF__Procedure__ByWho__261E26ED] DEFAULT (suser_name()) NOT NULL,
    CONSTRAINT [PK_Procedure] PRIMARY KEY CLUSTERED ([ProcedureRecno] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_Procedure]
    ON [AE].[Procedure]([AESourceUniqueID] ASC, [ProcedureCode] ASC, [ProcedureDate] ASC, [SourceUniqueID] ASC);


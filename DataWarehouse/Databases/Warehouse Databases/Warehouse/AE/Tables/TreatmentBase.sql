﻿CREATE TABLE [AE].[TreatmentBase] (
    [TreatmentCode] VARCHAR (5)   NOT NULL,
    [Treatment]     VARCHAR (200) NOT NULL,
    CONSTRAINT [PK_TreatmentBase] PRIMARY KEY CLUSTERED ([TreatmentCode] ASC)
);


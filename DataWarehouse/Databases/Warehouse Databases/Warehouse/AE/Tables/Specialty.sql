﻿CREATE TABLE [AE].[Specialty] (
    [SpecialtyCode]         INT          NOT NULL,
    [Specialty]             VARCHAR (80) NOT NULL,
    [IsMentalHealth]        BIT          NULL,
    [NationalSpecialtyCode] VARCHAR (50) NULL,
    CONSTRAINT [PK_Specialty] PRIMARY KEY CLUSTERED ([SpecialtyCode] ASC)
);


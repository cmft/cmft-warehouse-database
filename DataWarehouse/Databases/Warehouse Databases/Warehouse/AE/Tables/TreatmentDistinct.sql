﻿CREATE TABLE [AE].[TreatmentDistinct] (
    [AESourceUniqueID] VARCHAR (50)  NOT NULL,
    [TreatmentCode]    VARCHAR (6)   NULL,
    [TreatmentDate]    SMALLDATETIME NULL,
    [SequenceNo]       BIGINT        NOT NULL,
    CONSTRAINT [PK_TreatmentDistinct] PRIMARY KEY CLUSTERED ([AESourceUniqueID] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [AE].[Religion] (
    [ReligionCode]         INT           NOT NULL,
    [Religion]             VARCHAR (255) NULL,
    [ReligionNationalCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_Religion] PRIMARY KEY CLUSTERED ([ReligionCode] ASC)
);


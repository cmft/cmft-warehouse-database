﻿CREATE TABLE [AE].[DiagnosisDistinct] (
    [AESourceUniqueID] VARCHAR (50) NOT NULL,
    [DiagnosisCode]    VARCHAR (71) NULL,
    [SequenceNo]       BIGINT       NOT NULL,
    CONSTRAINT [PK_DiagnosisDistinct] PRIMARY KEY CLUSTERED ([AESourceUniqueID] ASC, [SequenceNo] ASC)
);


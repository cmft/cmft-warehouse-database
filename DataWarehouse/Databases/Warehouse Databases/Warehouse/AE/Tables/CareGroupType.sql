﻿CREATE TABLE [AE].[CareGroupType] (
    [CareGroupTypeCode] VARCHAR (10)  NOT NULL,
    [CareGroupType]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_CareGroupType] PRIMARY KEY CLUSTERED ([CareGroupTypeCode] ASC)
);


﻿CREATE TABLE [AE].[InvestigationBase] (
    [InvestigationCode] VARCHAR (5)   NOT NULL,
    [Investigation]     VARCHAR (200) NULL,
    CONSTRAINT [PK_InvestigationBase] PRIMARY KEY CLUSTERED ([InvestigationCode] ASC)
);


﻿CREATE TABLE [AE].[TreatmentSubAnalysis] (
    [TreatmentSubAnalysisCode] VARCHAR (5)   NOT NULL,
    [TreatmentSubAnalysis]     VARCHAR (200) NULL,
    [TreatmentCode]            VARCHAR (5)   NULL,
    CONSTRAINT [PK_TreatmentSubAnalysis] PRIMARY KEY CLUSTERED ([TreatmentSubAnalysisCode] ASC),
    CONSTRAINT [FK_TreatmentSubAnalysis_TreatmentSubAnalysis] FOREIGN KEY ([TreatmentCode]) REFERENCES [AE].[TreatmentBase] ([TreatmentCode])
);


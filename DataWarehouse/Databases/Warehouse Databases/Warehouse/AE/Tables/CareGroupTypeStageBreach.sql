﻿CREATE TABLE [AE].[CareGroupTypeStageBreach] (
    [CareGroupTypeCode] VARCHAR (10) NOT NULL,
    [StageCode]         VARCHAR (50) NOT NULL,
    [BreachValue]       INT          NOT NULL,
    CONSTRAINT [PK_CareGroupTypeStageBreach] PRIMARY KEY CLUSTERED ([CareGroupTypeCode] ASC, [StageCode] ASC),
    CONSTRAINT [FK_CareGroupTypeStageBreach_CareGroupType] FOREIGN KEY ([CareGroupTypeCode]) REFERENCES [AE].[CareGroupType] ([CareGroupTypeCode]),
    CONSTRAINT [FK_CareGroupTypeStageBreach_Stage] FOREIGN KEY ([StageCode]) REFERENCES [AE].[StageBase] ([StageCode])
);


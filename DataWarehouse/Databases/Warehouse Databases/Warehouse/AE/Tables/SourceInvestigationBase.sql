﻿CREATE TABLE [AE].[SourceInvestigationBase] (
    [SourceInvestigationCode] INT          NOT NULL,
    [SourceInvestigation]     VARCHAR (80) NOT NULL,
    PRIMARY KEY CLUSTERED ([SourceInvestigationCode] ASC)
);


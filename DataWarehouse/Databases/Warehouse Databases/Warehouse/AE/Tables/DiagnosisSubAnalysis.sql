﻿CREATE TABLE [AE].[DiagnosisSubAnalysis] (
    [DiagnosisSubAnalysisCode] VARCHAR (5)   NOT NULL,
    [DiagnosisSubAnalysis]     VARCHAR (200) NOT NULL,
    [DiagnosisCode]            VARCHAR (5)   NOT NULL,
    CONSTRAINT [PK_DiagnosisSubAnalysis] PRIMARY KEY CLUSTERED ([DiagnosisSubAnalysisCode] ASC),
    CONSTRAINT [FK_DiagnosisSubAnalysis_DiagnosisBase] FOREIGN KEY ([DiagnosisCode]) REFERENCES [AE].[DiagnosisBase] ([DiagnosisCode])
);


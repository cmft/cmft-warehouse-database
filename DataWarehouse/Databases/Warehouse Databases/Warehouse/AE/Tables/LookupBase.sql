﻿CREATE TABLE [AE].[LookupBase] (
    [Lkp_ID]            INT          NOT NULL,
    [Lkp_ParentID]      INT          NOT NULL,
    [Lkp_Name]          VARCHAR (80) NOT NULL,
    [lkp_active]        BIT          NOT NULL,
    [Lkp_DisplayOrder]  SMALLINT     NOT NULL,
    [Lkp_InAscOrder]    BIT          NOT NULL,
    [Lkp_Used]          BIT          NOT NULL,
    [Lkp_Datecreated]   DATETIME     NOT NULL,
    [Lkp_System]        VARCHAR (50) NOT NULL,
    [Lkp_FlatLookupID]  INT          NOT NULL,
    [lkp_deptid]        INT          NOT NULL,
    [Lkp_createdby]     INT          NOT NULL,
    [Lkp_update]        DATETIME     NOT NULL,
    [lkp_refreshed]     DATETIME     NOT NULL,
    [lkp_deleted]       BIT          NOT NULL,
    [lkp_IsSubAnalysed] BIT          NOT NULL,
    [lkp_TableID]       INT          NOT NULL,
    CONSTRAINT [PK_LookupBase] PRIMARY KEY CLUSTERED ([Lkp_ID] ASC)
);


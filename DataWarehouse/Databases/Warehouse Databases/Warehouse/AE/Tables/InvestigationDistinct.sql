﻿CREATE TABLE [AE].[InvestigationDistinct] (
    [AESourceUniqueID]  VARCHAR (50) NOT NULL,
    [InvestigationCode] VARCHAR (6)  NULL,
    [SequenceNo]        BIGINT       NOT NULL,
    CONSTRAINT [PK_InvestigationDistinct] PRIMARY KEY CLUSTERED ([AESourceUniqueID] ASC, [SequenceNo] ASC)
);


﻿CREATE TABLE [AE].[DischargeSummary] (
    [DischargeSummaryRecno]                       INT           IDENTITY (1, 1) NOT NULL,
    [DischargedByStaffMember]                     VARCHAR (50)  NULL,
    [DepartureTime]                               DATETIME      NULL,
    [AttendanceNumber]                            VARCHAR (50)  NOT NULL,
    [PatientName]                                 VARCHAR (255) NULL,
    [PatientAddressLine1]                         VARCHAR (50)  NULL,
    [PatientAddressLine2]                         VARCHAR (50)  NULL,
    [PatientAddressLine3]                         VARCHAR (50)  NULL,
    [PatientAddressLine4]                         VARCHAR (50)  NULL,
    [PatientPostcode]                             VARCHAR (10)  NULL,
    [DateOfBirth]                                 DATE          NULL,
    [NHSNumber]                                   VARCHAR (17)  NULL,
    [HospitalNumber]                              VARCHAR (50)  NULL,
    [ArrivalDate]                                 DATE          NULL,
    [ArrivalTime]                                 TIME (7)      NULL,
    [ArrivalMode]                                 VARCHAR (200) NULL,
    [SourceOfArrival]                             INT           NULL,
    [AccompaniedBy]                               VARCHAR (MAX) NULL,
    [PreviousAttendances]                         INT           NULL,
    [ReferredBy]                                  VARCHAR (200) NULL,
    [SchoolAttended]                              VARCHAR (MAX) NULL,
    [RegisteredGpCode]                            VARCHAR (10)  NULL,
    [RegisteredGp]                                VARCHAR (100) NULL,
    [RegisteredGpPracticeCode]                    VARCHAR (10)  NULL,
    [RegisteredGpAddressLine1]                    VARCHAR (35)  NULL,
    [RegisteredGpAddressLine2]                    VARCHAR (35)  NULL,
    [RegisteredGpAddressLine3]                    VARCHAR (35)  NULL,
    [RegisteredGpAddressLine4]                    VARCHAR (35)  NULL,
    [RegisteredGpAddressPostcode]                 VARCHAR (10)  NULL,
    [PresentingCondition]                         VARCHAR (MAX) NULL,
    [TriagedBy]                                   VARCHAR (255) NULL,
    [BriefHistory]                                VARCHAR (300) NULL,
    [TriageComments]                              VARCHAR (MAX) NULL,
    [AllergyOrSensitivity]                        VARCHAR (MAX) NULL,
    [InitialObservationsPulse]                    VARCHAR (50)  NULL,
    [InitialObservationsSystolicBP]               VARCHAR (50)  NULL,
    [InitialObservationsDiastolicBP]              VARCHAR (50)  NULL,
    [InitialObservationsO2Saturation]             VARCHAR (50)  NULL,
    [InitialObservationsRespiratoryRate]          VARCHAR (50)  NULL,
    [InitialObservationsGCS]                      VARCHAR (50)  NULL,
    [InitialObservationsEWS]                      VARCHAR (50)  NULL,
    [InitialObservationsTemperature]              VARCHAR (50)  NULL,
    [InitialObservationsPainScoreAtRest]          VARCHAR (50)  NULL,
    [InitialObservationsPainScoreOnMovement]      VARCHAR (50)  NULL,
    [InitialObservationsBloodSugar]               VARCHAR (50)  NULL,
    [InitialObservationsFractionOfInspiredOxygen] VARCHAR (50)  NULL,
    [InitialObservationsPerformPeakFlow]          VARCHAR (50)  NULL,
    [InitialObservationsCapRefill]                VARCHAR (50)  NULL,
    [FinalObservationsPulse]                      VARCHAR (50)  NULL,
    [FinalObservationsSystolicBP]                 VARCHAR (50)  NULL,
    [FinalObservationsDiastolicBP]                VARCHAR (50)  NULL,
    [FinalObservationsO2Saturation]               VARCHAR (50)  NULL,
    [FinalObservationsRespiratoryRate]            VARCHAR (50)  NULL,
    [FinalObservationsGCS]                        VARCHAR (50)  NULL,
    [FinalObservationsEWS]                        VARCHAR (50)  NULL,
    [DutyConsultant]                              VARCHAR (255) NULL,
    [ClinicianSeen]                               VARCHAR (255) NULL,
    [Diagnosis01]                                 VARCHAR (255) NULL,
    [Diagnosis02]                                 VARCHAR (255) NULL,
    [DepartmentalInvestigation]                   VARCHAR (MAX) NULL,
    [DepartmentalInvestigationDetails]            VARCHAR (MAX) NULL,
    [Investigation01]                             VARCHAR (255) NULL,
    [Investigation02]                             VARCHAR (255) NULL,
    [Treatment]                                   VARCHAR (MAX) NULL,
    [TreatmentDetails]                            VARCHAR (MAX) NULL,
    [MedicationGiven]                             VARCHAR (MAX) NULL,
    [InformationForGP]                            VARCHAR (MAX) NULL,
    [AuditCScore]                                 VARCHAR (255) NULL,
    [FollowUpAdvice]                              VARCHAR (MAX) NULL,
    [FollowUpArrangement]                         VARCHAR (MAX) NULL,
    [FollowUpTime]                                VARCHAR (MAX) NULL,
    [DischargeOutcome]                            VARCHAR (200) NULL,
    [DischargeOutcomeDetails]                     VARCHAR (MAX) NULL,
    [DischargeDestination]                        VARCHAR (80)  NULL,
    [DischargeMedicationGiven]                    VARCHAR (MAX) NULL,
    [LetterSentBy]                                VARCHAR (19)  NULL,
    [PASSourcePatientNo]                          INT           NULL,
    [PASSourceEpisodeNo]                          INT           NULL,
    [DistrictNo]                                  VARCHAR (50)  NULL,
    [AESourceUniqueID]                            INT           NULL,
    [EDTSendTime]                                 DATETIME      NULL,
    [Created]                                     DATETIME      NULL,
    [Updated]                                     DATETIME      NULL,
    [ByWhom]                                      VARCHAR (50)  NULL,
    [AttendanceDisposalCode]                      VARCHAR (2)   NULL,
    CONSTRAINT [PK_DischargeSummary] PRIMARY KEY CLUSTERED ([DischargeSummaryRecno] ASC)
);




GO
CREATE NONCLUSTERED INDEX [IX_DischargeSummary_DepartureTime]
    ON [AE].[DischargeSummary]([DepartureTime] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DischargeSummary_DistrictNo]
    ON [AE].[DischargeSummary]([DistrictNo] ASC);


GO
--GRANT SELECT
--    ON OBJECT::[AE].[DischargeSummary] TO [scc]
--    AS [dbo];


﻿CREATE TABLE [AE].[AttendanceCategory] (
    [AttendanceCategoryCode] VARCHAR (50)  NOT NULL,
    [AttendanceCategory]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_AttendanceCategory] PRIMARY KEY CLUSTERED ([AttendanceCategoryCode] ASC)
);


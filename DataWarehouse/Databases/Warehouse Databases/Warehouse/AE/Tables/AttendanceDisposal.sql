﻿CREATE TABLE [AE].[AttendanceDisposal] (
    [AttendanceDisposalCode]         INT           NOT NULL,
    [AttendanceDisposal]             VARCHAR (255) NULL,
    [AttendanceDisposalNationalCode] VARCHAR (10)  NULL,
    CONSTRAINT [PK_AttendanceDisposal] PRIMARY KEY CLUSTERED ([AttendanceDisposalCode] ASC)
);


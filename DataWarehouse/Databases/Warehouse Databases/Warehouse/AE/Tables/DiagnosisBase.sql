﻿CREATE TABLE [AE].[DiagnosisBase] (
    [DiagnosisCode] VARCHAR (5)   NOT NULL,
    [Diagnosis]     VARCHAR (200) NULL,
    CONSTRAINT [PK_DiagnosisBase] PRIMARY KEY CLUSTERED ([DiagnosisCode] ASC),
    CONSTRAINT [FK_DiagnosisBase_DiagnosisBase] FOREIGN KEY ([DiagnosisCode]) REFERENCES [AE].[DiagnosisBase] ([DiagnosisCode])
);


﻿CREATE TABLE [AE].[PCTPractice] (
    [PracticeCode] NVARCHAR (255) NOT NULL,
    [PCTCode]      NVARCHAR (255) NULL,
    [JoinDate]     FLOAT (53)     NULL,
    [LeftDate]     NVARCHAR (255) NULL,
    CONSTRAINT [PK_PCTPractice] PRIMARY KEY CLUSTERED ([PracticeCode] ASC)
);


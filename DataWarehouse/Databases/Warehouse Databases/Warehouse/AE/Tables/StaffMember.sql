﻿CREATE TABLE [AE].[StaffMember] (
    [StaffMemberCode] INT          NOT NULL,
    [Surname]         VARCHAR (35) NOT NULL,
    [Forename]        VARCHAR (35) NOT NULL,
    [MiddleNames]     VARCHAR (35) NOT NULL,
    [Title]           VARCHAR (20) NULL,
    [JobTypeCode]     INT          NOT NULL,
    [StartDate]       DATETIME     NOT NULL,
    [EndDate]         DATETIME     NOT NULL,
    [SpecialtyCode]   INT          NOT NULL,
    [JobTitle]        VARCHAR (30) NOT NULL,
    [LoginId]         VARCHAR (30) NULL
);


﻿CREATE TABLE [AE].[DisposalBase] (
    [DisposalCode] VARCHAR (5)   NOT NULL,
    [Disposal]     VARCHAR (200) NULL,
    CONSTRAINT [PK_DisposalBase] PRIMARY KEY CLUSTERED ([DisposalCode] ASC)
);


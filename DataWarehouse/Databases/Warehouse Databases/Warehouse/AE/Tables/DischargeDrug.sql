﻿CREATE TABLE [AE].[DischargeDrug] (
    [DischargeDrugRecno] INT          IDENTITY (1, 1) NOT NULL,
    [SourceUniqueID]     INT          NOT NULL,
    [AESourceUniqueID]   INT          NOT NULL,
    [DischargeDate]      DATE         NULL,
    [DischargeTime]      DATETIME     NULL,
    [DrugCode]           INT          NOT NULL,
    [SequenceNo]         INT          NOT NULL,
    [InterfaceCode]      VARCHAR (10) NULL,
    [Created]            DATETIME     NULL,
    [Updated]            DATETIME     NULL,
    [ByWhom]             VARCHAR (50) NULL,
    CONSTRAINT [PK__Discharg__DA90395363E583E5] PRIMARY KEY CLUSTERED ([SourceUniqueID] ASC, [SequenceNo] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_DischargeDrug_AESourceUniqueID]
    ON [AE].[DischargeDrug]([AESourceUniqueID] ASC);


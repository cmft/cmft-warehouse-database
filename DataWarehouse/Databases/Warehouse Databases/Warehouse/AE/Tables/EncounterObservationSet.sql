﻿CREATE TABLE [AE].[EncounterObservationSet] (
    [EncounterRecno]      INT NOT NULL,
    [ObservationSetRecno] INT NOT NULL,
    CONSTRAINT [PK_EncounterObservationSet] PRIMARY KEY CLUSTERED ([EncounterRecno] ASC, [ObservationSetRecno] ASC)
);


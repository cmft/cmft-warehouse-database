﻿CREATE TABLE [Symphony].[PatientSystemId] (
    [EncounterRecno]          INT          IDENTITY (1, 1) NOT NULL,
    [PatientSystemIdID]       INT          NOT NULL,
    [PatientID]               INT          NOT NULL,
    [SystemID]                INT          NOT NULL,
    [PatientSystemId]         VARCHAR (30) NOT NULL,
    [StatusID]                INT          NOT NULL,
    [CreatedByID]             INT          NOT NULL,
    [UpdatedTime]             DATETIME     NOT NULL,
    [PatientSystemIdReversed] VARCHAR (30) NOT NULL,
    [EncounterChecksum]       INT          NOT NULL,
    [Created]                 DATETIME     NOT NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Symphony_PatientSystemId] PRIMARY KEY CLUSTERED ([PatientSystemIdID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_PatientSystemId_2]
    ON [Symphony].[PatientSystemId]([SystemID] ASC)
    INCLUDE([PatientID], [PatientSystemId], [PatientSystemIdID]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_PatientSystemId_1]
    ON [Symphony].[PatientSystemId]([PatientID] ASC)
    INCLUDE([PatientSystemId], [PatientSystemIdID], [SystemID]);


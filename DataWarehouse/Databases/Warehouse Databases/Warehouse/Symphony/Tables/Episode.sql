﻿CREATE TABLE [Symphony].[Episode] (
    [EncounterRecno]    INT          IDENTITY (1, 1) NOT NULL,
    [EpisodeID]         INT          NOT NULL,
    [PatientID]         INT          NOT NULL,
    [EpisodeNumber]     VARCHAR (20) NOT NULL,
    [RecordedByID]      INT          NOT NULL,
    [CreatedByID]       INT          NOT NULL,
    [UpdatedTime]       DATETIME     NOT NULL,
    [DepartmentID]      INT          NOT NULL,
    [EncounterChecksum] INT          NOT NULL,
    [Created]           DATETIME     NOT NULL,
    [Updated]           DATETIME     NULL,
    [ByWhom]            VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Episode] PRIMARY KEY CLUSTERED ([EpisodeID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Episode_3]
    ON [Symphony].[Episode]([PatientID] ASC)
    INCLUDE([EpisodeID]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Episode_4]
    ON [Symphony].[Episode]([EncounterRecno] ASC)
    INCLUDE([EpisodeID]);


﻿CREATE TABLE [Symphony].[Department] (
    [DepartmentID]        INT          NOT NULL,
    [Department]          VARCHAR (20) NOT NULL,
    [DepartmentShortName] VARCHAR (4)  NOT NULL,
    [HasBeenUsed]         BIT          NOT NULL,
    [CreatedTime]         DATETIME     NOT NULL,
    [CreatedByID]         INT          NOT NULL,
    [UpdatedTime]         DATETIME     NOT NULL,
    [UpdatedByID]         INT          NOT NULL,
    [IsServiceDepartment] BIT          NOT NULL,
    [Created]             DATETIME     NOT NULL,
    [Updated]             DATETIME     NULL,
    [ByWhom]              VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED ([DepartmentID] ASC)
);


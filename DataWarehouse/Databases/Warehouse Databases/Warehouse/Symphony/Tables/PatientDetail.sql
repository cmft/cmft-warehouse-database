﻿CREATE TABLE [Symphony].[PatientDetail] (
    [EncounterRecno]          INT           IDENTITY (1, 1) NOT NULL,
    [PatientID]               INT           NOT NULL,
    [GpID]                    INT           NULL,
    [PracticeID]              INT           NULL,
    [HonoursID]               INT           NULL,
    [PracticeAddressID]       INT           NULL,
    [NoteID]                  INT           NULL,
    [DateOfDeath]             DATETIME      NULL,
    [MaritalStatusID]         INT           NULL,
    [ReligionID]              INT           NULL,
    [EthnicCategoryID]        INT           NULL,
    [Occupation]              VARCHAR (50)  NULL,
    [EmploymentStatusID]      INT           NULL,
    [OverseasVisitorStatusID] INT           NULL,
    [NationalityID]           INT           NULL,
    [PUPID]                   INT           NULL,
    [PEPID]                   INT           NULL,
    [CreatedByID]             INT           NULL,
    [UpdatedTime]             DATETIME      NULL,
    [GpMoveID]                INT           NULL,
    [SpecialCase]             VARCHAR (255) NULL,
    [IsSpecialCase]           BIT           NULL,
    [EncounterChecksum]       INT           NOT NULL,
    [Created]                 DATETIME      NOT NULL,
    [Updated]                 DATETIME      NULL,
    [ByWhom]                  VARCHAR (50)  NULL,
    CONSTRAINT [PK_PatientDetail] PRIMARY KEY CLUSTERED ([PatientID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Patient_3]
    ON [Symphony].[PatientDetail]([EncounterRecno] ASC)
    INCLUDE([PatientID]);


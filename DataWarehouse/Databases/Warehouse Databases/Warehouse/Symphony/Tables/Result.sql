﻿CREATE TABLE [Symphony].[Result] (
    [EncounterRecno]         INT           IDENTITY (1, 1) NOT NULL,
    [ResultID]               INT           NOT NULL,
    [RequestID]              INT           NOT NULL,
    [AttendanceID]           INT           NOT NULL,
    [DataEntryProcedureID]   SMALLINT      NOT NULL,
    [ResultTime]             DATETIME      NOT NULL,
    [ResultID1]              INT           NOT NULL,
    [Field1ID]               INT           NOT NULL,
    [Field2ID]               INT           NOT NULL,
    [Field3ID]               INT           NOT NULL,
    [Field4ID]               INT           NOT NULL,
    [Field5ID]               INT           NOT NULL,
    [Field6ID]               VARCHAR (255) NULL,
    [Field7ID]               VARCHAR (255) NULL,
    [Field8ID]               VARCHAR (255) NULL,
    [Field9ID]               VARCHAR (255) NULL,
    [Field10ID]              VARCHAR (255) NULL,
    [Field11ID]              VARCHAR (255) NULL,
    [Field12ID]              VARCHAR (255) NULL,
    [Field13ID]              VARCHAR (255) NULL,
    [StaffID]                INT           NOT NULL,
    [HasExtraFields]         BIT           NOT NULL,
    [NoteID]                 INT           NOT NULL,
    [IncludingPatientLetter] BIT           NOT NULL,
    [CreatedByID]            INT           NOT NULL,
    [CreatedTime]            DATETIME      NOT NULL,
    [UpdatedByID]            INT           NOT NULL,
    [UpdatedTime]            DATETIME      NOT NULL,
    [CautionID]              INT           NULL,
    [RepeatID]               INT           NULL,
    [DnmImageid]             INT           NULL,
    [TPTransID]              VARCHAR (30)  NULL,
    [IsCancellation]         BIT           NOT NULL,
    [IsFirst]                BIT           NULL,
    [IsLast]                 BIT           NULL,
    [EncounterChecksum]      INT           NOT NULL,
    [Created]                DATETIME      NOT NULL,
    [Updated]                DATETIME      NULL,
    [ByWhom]                 VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Result] PRIMARY KEY CLUSTERED ([ResultID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Result_2]
    ON [Symphony].[Result]([AttendanceID] ASC, [DataEntryProcedureID] ASC, [IsFirst] ASC)
    INCLUDE([ResultID], [ResultTime], [StaffID]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Result_3]
    ON [Symphony].[Result]([EncounterRecno] ASC)
    INCLUDE([AttendanceID]);


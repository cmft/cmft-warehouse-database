﻿CREATE TABLE [Symphony].[Patient] (
    [EncounterRecno]          INT          IDENTITY (1, 1) NOT NULL,
    [PatientID]               INT          NOT NULL,
    [Surname]                 VARCHAR (35) NULL,
    [TitleID]                 INT          NULL,
    [Forename]                VARCHAR (35) NULL,
    [MiddleNames]             VARCHAR (35) NULL,
    [SexID]                   INT          NULL,
    [DateOfBirth]             DATETIME     NULL,
    [IsTemporaryRegistration] TINYINT      NULL,
    [LastAttendanceNumber]    VARCHAR (20) NULL,
    [CreatedByID]             INT          NULL,
    [UpdatedTime]             DATETIME     NULL,
    [IsDead]                  BIT          NULL,
    [EncounterChecksum]       INT          NOT NULL,
    [Created]                 DATETIME     NOT NULL,
    [Updated]                 DATETIME     NULL,
    [ByWhom]                  VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED ([PatientID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Patient_3]
    ON [Symphony].[Patient]([EncounterRecno] ASC)
    INCLUDE([PatientID]);


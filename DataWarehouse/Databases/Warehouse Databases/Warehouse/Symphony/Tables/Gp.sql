﻿CREATE TABLE [Symphony].[Gp] (
    [GpID]             INT           NOT NULL,
    [GpCode]           VARCHAR (200) NOT NULL,
    [StartDate]        DATE          NOT NULL,
    [EndDate]          DATE          NOT NULL,
    [Surname]          VARCHAR (35)  NOT NULL,
    [Initials]         VARCHAR (30)  NOT NULL,
    [TitleID]          INT           NOT NULL,
    [QualificationsID] INT           NOT NULL,
    [LetterPreference] VARCHAR (35)  NOT NULL,
    [CreatedByID]      INT           NOT NULL,
    [UpdatedTime]      DATETIME      NOT NULL,
    [CreatedTime]      DATETIME      NULL,
    [UpdatedByID]      INT           NULL,
    [RecordVersion]    INT           NULL,
    [Created]          DATETIME      NOT NULL,
    [Updated]          DATETIME      NULL,
    [ByWhom]           VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Gp] PRIMARY KEY CLUSTERED ([GpID] ASC)
);


﻿CREATE TABLE [Symphony].[ClinicInstance] (
    [EncounterRecno]                INT            IDENTITY (1, 1) NOT NULL,
    [ClinicInstanceID]              INT            NOT NULL,
    [ClinicID]                      INT            NULL,
    [ClinicTypeID]                  INT            NULL,
    [Clinic]                        VARCHAR (50)   NULL,
    [InstanceDate]                  DATE           NULL,
    [ClinicianID]                   INT            NULL,
    [FirstAppointmentTime]          TIME (7)       NULL,
    [NumberOfAppointments]          SMALLINT       NULL,
    [AppointmentLength]             SMALLINT       NULL,
    [MaximumPatientsPerAppointment] SMALLINT       NULL,
    [MaximumPatientsPerClinic]      SMALLINT       NULL,
    [AsFirstAttendanceID]           TINYINT        NULL,
    [ClinicStatusID]                TINYINT        NULL,
    [IsDisabled]                    BIT            NULL,
    [Comments]                      VARCHAR (2000) NULL,
    [CreatedByID]                   INT            NULL,
    [CreatedTime]                   DATETIME       NULL,
    [DepartmentID]                  INT            NULL,
    [Created]                       DATETIME       NULL,
    [Updated]                       DATETIME       NULL,
    [ByWhom]                        VARCHAR (50)   NULL,
    CONSTRAINT [PK_Symphony_ClinicInstance] PRIMARY KEY CLUSTERED ([ClinicInstanceID] ASC)
);


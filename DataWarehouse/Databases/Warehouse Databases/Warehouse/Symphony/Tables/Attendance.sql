﻿CREATE TABLE [Symphony].[Attendance] (
    [EncounterRecno]           INT           IDENTITY (1, 1) NOT NULL,
    [AttendanceID]             INT           NOT NULL,
    [EpisodeID]                INT           NOT NULL,
    [AttendanceTypeID]         TINYINT       NOT NULL,
    [StatusID]                 TINYINT       NOT NULL,
    [IsReattender]             BIT           NOT NULL,
    [AttendanceNumber]         VARCHAR (20)  NOT NULL,
    [ArrivalTime]              DATETIME      NOT NULL,
    [RegisteredTime]           DATETIME      NOT NULL,
    [BookedTime]               DATETIME      NOT NULL,
    [CancelledTime]            DATETIME      NOT NULL,
    [Slot]                     SMALLINT      NOT NULL,
    [SourceOfReferralID]       INT           NOT NULL,
    [ReferralMethodID]         INT           NOT NULL,
    [ReferralSpecialtyID]      INT           NOT NULL,
    [ConsultantID]             INT           NOT NULL,
    [GpCode]                   VARCHAR (14)  NOT NULL,
    [PracticeCode]             VARCHAR (14)  NOT NULL,
    [PracticeAddressID]        INT           NOT NULL,
    [ArrivalModeID]            INT           NOT NULL,
    [IncidentLocationTypeID]   INT           NOT NULL,
    [LocationNotesID]          INT           NOT NULL,
    [AmbulanceCrew]            VARCHAR (30)  NOT NULL,
    [AttendanceConclusionTime] DATETIME      NOT NULL,
    [AttendanceDisposalID]     INT           NOT NULL,
    [DischargeDestinationID]   INT           NOT NULL,
    [DischargeNotesID]         INT           NOT NULL,
    [PresentingProblem]        VARCHAR (300) NULL,
    [PatientGroupID]           INT           NOT NULL,
    [CareGroupID]              INT           NOT NULL,
    [PatientComplaint]         VARCHAR (300) NOT NULL,
    [DischargeCreatedByID]     INT           NOT NULL,
    [DischargeCreatedTime]     DATETIME      NOT NULL,
    [DischargeUpdatedByID]     INT           NOT NULL,
    [DischargeUpdatedByTime]   DATETIME      NOT NULL,
    [AttendanceCreatedByID]    INT           NOT NULL,
    [AttendanceUpdatedByID]    INT           NOT NULL,
    [AttendanceCreatedTime]    DATETIME      NOT NULL,
    [AttendanceUpdatedTime]    DATETIME      NOT NULL,
    [NonAttendanceReasonID]    INT           NOT NULL,
    [TransferPatient]          BIT           NULL,
    [MergedByID]               INT           NOT NULL,
    [MergeTime]                DATETIME      NOT NULL,
    [EncounterChecksum]        INT           NULL,
    [Created]                  DATETIME      NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  NULL,
    CONSTRAINT [PK_Attendance] PRIMARY KEY CLUSTERED ([AttendanceID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Attendance_3]
    ON [Symphony].[Attendance]([EpisodeID] ASC)
    INCLUDE([AttendanceID], [EncounterRecno]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Attendance_1]
    ON [Symphony].[Attendance]([EncounterRecno] ASC);


﻿CREATE TABLE [Symphony].[Triage] (
    [EncounterRecno]         INT           IDENTITY (1, 1) NOT NULL,
    [TriageID]               INT           NOT NULL,
    [AttendanceID]           INT           NOT NULL,
    [RequestID]              INT           NOT NULL,
    [DepartmentID]           SMALLINT      NOT NULL,
    [TriageTime]             DATETIME      NOT NULL,
    [Complaint]              VARCHAR (255) NOT NULL,
    [Discriminator]          VARCHAR (80)  NOT NULL,
    [CategoryID]             TINYINT       NOT NULL,
    [IsFasttrack]            BIT           NOT NULL,
    [FasttrackNoteID]        INT           NOT NULL,
    [Painscore]              TINYINT       NOT NULL,
    [CommentsID]             INT           NOT NULL,
    [Treatment]              VARCHAR (150) NOT NULL,
    [NurseID]                INT           NOT NULL,
    [IncludingPatientLetter] BIT           NOT NULL,
    [CreatedByID]            INT           NOT NULL,
    [CreatedTime]            DATETIME      NOT NULL,
    [UpdatedByID]            INT           NOT NULL,
    [UpdatedTime]            DATETIME      NOT NULL,
    [CautionID]              INT           NULL,
    [RepeatID]               INT           NULL,
    [EncounterChecksum]      INT           NOT NULL,
    [Created]                DATETIME      NOT NULL,
    [Updated]                DATETIME      NULL,
    [ByWhom]                 VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Triage] PRIMARY KEY CLUSTERED ([TriageID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Triage_3]
    ON [Symphony].[Triage]([CommentsID] ASC)
    INCLUDE([AttendanceID]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Triage_1]
    ON [Symphony].[Triage]([AttendanceID] ASC);


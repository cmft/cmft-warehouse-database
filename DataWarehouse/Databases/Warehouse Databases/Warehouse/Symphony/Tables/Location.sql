﻿CREATE TABLE [Symphony].[Location] (
    [LocationID]          SMALLINT      NOT NULL,
    [LocationParentID]    SMALLINT      NOT NULL,
    [Location]            VARCHAR (100) NOT NULL,
    [LocationIndex]       SMALLINT      NOT NULL,
    [IsActive]            TINYINT       NOT NULL,
    [LocationDescription] VARCHAR (255) NOT NULL,
    [CreatedByID]         INT           NOT NULL,
    [UpdatedTime]         DATETIME      NOT NULL,
    [ReasonForSuspension] VARCHAR (255) NOT NULL,
    [SuspendedTime]       DATETIME      NOT NULL,
    [SuspendedByID]       INT           NOT NULL,
    [DepartmentID]        INT           NOT NULL,
    [IsUsed]              BIT           NOT NULL,
    [Created]             DATETIME      NOT NULL,
    [Updated]             DATETIME      NULL,
    [ByWhom]              VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


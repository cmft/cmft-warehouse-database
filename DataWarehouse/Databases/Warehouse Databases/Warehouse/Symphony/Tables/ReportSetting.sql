﻿CREATE TABLE [Symphony].[ReportSetting] (
    [ReportSettingID] INT          NOT NULL,
    [ReportSetting]   VARCHAR (50) NOT NULL,
    [ValueID]         INT          NULL,
    [DepartmentID]    INT          NOT NULL,
    [Created]         DATETIME     NOT NULL,
    [Updated]         DATETIME     NULL,
    [ByWhom]          VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_ReportSetting] PRIMARY KEY CLUSTERED ([ReportSettingID] ASC)
);


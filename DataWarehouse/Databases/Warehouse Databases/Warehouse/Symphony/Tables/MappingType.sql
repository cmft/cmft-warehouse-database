﻿CREATE TABLE [Symphony].[MappingType] (
    [MappingTypeID]          INT           NOT NULL,
    [MappingType]            VARCHAR (200) NULL,
    [MappingTypeDescription] VARCHAR (200) NULL,
    [IsUsed]                 BIT           NULL,
    [CreatedByID]            INT           NULL,
    [CreatedTime]            DATETIME      NULL,
    [Created]                DATETIME      NOT NULL,
    [Updated]                DATETIME      NULL,
    [ByWhom]                 VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_MappingType] PRIMARY KEY CLUSTERED ([MappingTypeID] ASC)
);


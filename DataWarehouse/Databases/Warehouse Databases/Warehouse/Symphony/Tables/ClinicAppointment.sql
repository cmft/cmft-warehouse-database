﻿CREATE TABLE [Symphony].[ClinicAppointment] (
    [EncounterRecno]           INT            IDENTITY (1, 1) NOT NULL,
    [AttendanceID]             INT            NOT NULL,
    [ClinicInstanceID]         INT            NULL,
    [CurrentLocationID]        SMALLINT       NULL,
    [SeenByID]                 VARCHAR (100)  NULL,
    [AssociatedDepartmentData] VARCHAR (1000) NULL,
    [IsCancelled]              BIT            NULL,
    [Created]                  DATETIME       NULL,
    [Updated]                  DATETIME       NULL,
    [ByWhom]                   VARCHAR (50)   NULL,
    CONSTRAINT [PK_Symphony_ClinicAppointment] PRIMARY KEY CLUSTERED ([AttendanceID] ASC)
);


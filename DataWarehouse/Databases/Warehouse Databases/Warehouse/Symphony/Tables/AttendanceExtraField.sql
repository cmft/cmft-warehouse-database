﻿CREATE TABLE [Symphony].[AttendanceExtraField] (
    [EncounterRecno]         INT            IDENTITY (1, 1) NOT NULL,
    [AttendanceExtraFieldID] INT            NOT NULL,
    [FieldTypeID]            SMALLINT       NULL,
    [FieldID]                INT            NULL,
    [AttendanceID]           INT            NULL,
    [FieldValue]             VARCHAR (4000) NULL,
    [DataCategoryID]         SMALLINT       NULL,
    [RecordID]               INT            NULL,
    [EncounterChecksum]      INT            NOT NULL,
    [Created]                DATETIME       NOT NULL,
    [Updated]                DATETIME       NULL,
    [ByWhom]                 VARCHAR (50)   NOT NULL,
    CONSTRAINT [PK_AttendanceExtraField] PRIMARY KEY CLUSTERED ([AttendanceExtraFieldID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Attendance_2]
    ON [Symphony].[AttendanceExtraField]([AttendanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Attendance_3]
    ON [Symphony].[AttendanceExtraField]([EncounterRecno] ASC)
    INCLUDE([AttendanceID]);


﻿CREATE TABLE [Symphony].[ReferenceLookupMapping] (
    [EncounterRecno]           INT           IDENTITY (1, 1) NOT NULL,
    [ReferenceLookupMappingID] INT           NOT NULL,
    [ReferenceLookupID]        INT           NULL,
    [ReferenceMappingTypeID]   INT           NULL,
    [ValueID]                  INT           NULL,
    [ReferenceLookupMapping]   VARCHAR (200) NULL,
    [CreatedByID]              INT           NULL,
    [CreatedTime]              DATETIME      NULL,
    [MappedType]               TINYINT       NULL,
    [EncounterChecksum]        INT           NOT NULL,
    [Created]                  DATETIME      NOT NULL,
    [Updated]                  DATETIME      NULL,
    [ByWhom]                   VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Symphony_ReferenceLookupMapping] PRIMARY KEY CLUSTERED ([ReferenceLookupMappingID] ASC)
);


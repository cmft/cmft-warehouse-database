﻿CREATE TABLE [Symphony].[Address] (
    [EncounterRecno]    INT           IDENTITY (1, 1) NOT NULL,
    [AddressID]         INT           NOT NULL,
    [Line1]             VARCHAR (255) NOT NULL,
    [Line2]             VARCHAR (255) NOT NULL,
    [Line3]             VARCHAR (255) NOT NULL,
    [Line4]             VARCHAR (255) NOT NULL,
    [Line5]             VARCHAR (255) NOT NULL,
    [Postcode]          VARCHAR (10)  NOT NULL,
    [DHACode]           VARCHAR (3)   NOT NULL,
    [PCGCode]           VARCHAR (5)   NOT NULL,
    [AddressTypeID]     INT           NOT NULL,
    [LinkTypeID]        TINYINT       NOT NULL,
    [LinkID]            INT           NOT NULL,
    [CreatedByID]       INT           NOT NULL,
    [UpdatedTime]       DATETIME      NOT NULL,
    [ExtraDate1]        DATETIME      NULL,
    [ExtraDate2]        DATETIME      NULL,
    [MoveID]            INT           NULL,
    [EncounterChecksum] INT           NOT NULL,
    [Created]           DATETIME      NOT NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Symphony_Address] PRIMARY KEY CLUSTERED ([AddressID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Address_1]
    ON [Symphony].[Address]([AddressTypeID] ASC, [LinkTypeID] ASC)
    INCLUDE([LinkID], [UpdatedTime]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Address_2]
    ON [Symphony].[Address]([AddressTypeID] ASC, [LinkTypeID] ASC, [LinkID] ASC, [AddressID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Address_3]
    ON [Symphony].[Address]([LinkTypeID] ASC)
    INCLUDE([EncounterRecno], [LinkID]);


﻿CREATE TABLE [Symphony].[GpPractice] (
    [GpPracticeID]          INT           NOT NULL,
    [GpPracticeCode]        VARCHAR (200) NOT NULL,
    [TelephoneID]           INT           NULL,
    [GPFHCode]              VARCHAR (3)   NOT NULL,
    [HealthVisitor]         VARCHAR (56)  NOT NULL,
    [LocalCode]             VARCHAR (56)  NOT NULL,
    [StatusID]              INT           NOT NULL,
    [LocalGpID]             INT           NOT NULL,
    [PracticeGroupCareCode] VARCHAR (35)  NOT NULL,
    [CreatedByID]           INT           NOT NULL,
    [UpdatedTime]           DATETIME      NOT NULL,
    [LetterPreference]      VARCHAR (35)  NOT NULL,
    [CreatedTime]           DATETIME      NULL,
    [UpdatedByID]           INT           NULL,
    [RecordVersion]         INT           NULL,
    [Created]               DATETIME      NOT NULL,
    [Updated]               DATETIME      NULL,
    [ByWhom]                VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_GpPractice] PRIMARY KEY CLUSTERED ([GpPracticeID] ASC)
);


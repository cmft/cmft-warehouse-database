﻿CREATE TABLE [Symphony].[AttendanceLocation] (
    [EncounterRecno]       INT          IDENTITY (1, 1) NOT NULL,
    [AttendanceLocationID] INT          NOT NULL,
    [AttendanceID]         INT          NOT NULL,
    [LocationID]           SMALLINT     NOT NULL,
    [LocationTime]         DATETIME     NOT NULL,
    [CreatedByID]          INT          NOT NULL,
    [UpdatedTime]          DATETIME     NOT NULL,
    [EncounterChecksum]    INT          NOT NULL,
    [Created]              DATETIME     NOT NULL,
    [Updated]              DATETIME     NULL,
    [ByWhom]               VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Symphony_AttendanceLocation] PRIMARY KEY CLUSTERED ([AttendanceLocationID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_AttendanceLocation_01]
    ON [Symphony].[AttendanceLocation]([LocationID] ASC, [AttendanceID] ASC)
    INCLUDE([AttendanceLocationID], [LocationTime]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_AttendanceLocation_3]
    ON [Symphony].[AttendanceLocation]([EncounterRecno] ASC)
    INCLUDE([AttendanceID]);


﻿CREATE TABLE [Symphony].[RequestExtraField] (
    [EncounterRecno]       INT            IDENTITY (1, 1) NOT NULL,
    [RequestExtraFieldID]  INT            NOT NULL,
    [FieldTypeID]          SMALLINT       NULL,
    [FieldID]              INT            NULL,
    [AttendanceID]         INT            NULL,
    [FieldValue]           VARCHAR (4000) NULL,
    [DataCategoryID]       SMALLINT       NULL,
    [RecordID]             INT            NULL,
    [FieldValueCode]       VARCHAR (200)  NULL,
    [ComputationStateID]   TINYINT        NULL,
    [DataEntryProcedureID] TINYINT        NULL,
    [CPMessagesID]         VARCHAR (200)  NULL,
    [EncounterChecksum]    INT            NOT NULL,
    [Created]              DATETIME       NOT NULL,
    [Updated]              DATETIME       NULL,
    [ByWhom]               VARCHAR (50)   NOT NULL,
    CONSTRAINT [PK_RequestExtraField] PRIMARY KEY CLUSTERED ([RequestExtraFieldID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Request_2]
    ON [Symphony].[RequestExtraField]([AttendanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Request_3]
    ON [Symphony].[RequestExtraField]([EncounterRecno] ASC)
    INCLUDE([AttendanceID]);


﻿CREATE TABLE [Symphony].[Request] (
    [EncounterRecno]         INT           IDENTITY (1, 1) NOT NULL,
    [RequestID]              INT           NOT NULL,
    [AttendanceID]           INT           NOT NULL,
    [DataEntryProcedureID]   SMALLINT      NOT NULL,
    [RequestTime]            DATETIME      NOT NULL,
    [RequestID1]             INT           NOT NULL,
    [Field1ID]               INT           NOT NULL,
    [Field2ID]               INT           NOT NULL,
    [Field3ID]               INT           NOT NULL,
    [Field4ID]               INT           NOT NULL,
    [Field5ID]               INT           NOT NULL,
    [Field6ID]               VARCHAR (255) NOT NULL,
    [Field7ID]               VARCHAR (255) NOT NULL,
    [HasExtraFields]         BIT           NOT NULL,
    [NotesID]                INT           NOT NULL,
    [IncludingPatientLetter] BIT           NOT NULL,
    [CreatedByID]            INT           NOT NULL,
    [CreatedTime]            DATETIME      NOT NULL,
    [UpdatedByID]            INT           NOT NULL,
    [UpdatedTime]            DATETIME      NOT NULL,
    [CautionID]              INT           NULL,
    [RepeatID]               INT           NULL,
    [TPTransID]              VARCHAR (30)  NULL,
    [IsFirst]                BIT           NULL,
    [IsLast]                 BIT           NULL,
    [EncounterChecksum]      INT           NOT NULL,
    [Created]                DATETIME      NOT NULL,
    [Updated]                DATETIME      NULL,
    [ByWhom]                 VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Request] PRIMARY KEY CLUSTERED ([RequestID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Request_2]
    ON [Symphony].[Request]([AttendanceID] ASC, [DataEntryProcedureID] ASC, [IsLast] ASC)
    INCLUDE([RequestID], [RequestTime]);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_Request_3]
    ON [Symphony].[Request]([EncounterRecno] ASC)
    INCLUDE([AttendanceID]);


﻿CREATE TABLE [Symphony].[AuditAddress] (
    [EncounterRecno]    INT           IDENTITY (1, 1) NOT NULL,
    [AuditAddressID]    INT           NOT NULL,
    [AddressID]         INT           NOT NULL,
    [Line1]             VARCHAR (255) NULL,
    [Line2]             VARCHAR (255) NULL,
    [Line3]             VARCHAR (255) NULL,
    [Line4]             VARCHAR (255) NULL,
    [Line5]             VARCHAR (255) NULL,
    [Postcode]          VARCHAR (10)  NULL,
    [DHACode]           VARCHAR (3)   NULL,
    [PCGCode]           VARCHAR (5)   NULL,
    [AddressTypeID]     INT           NOT NULL,
    [LinkTypeID]        TINYINT       NOT NULL,
    [LinkID]            INT           NOT NULL,
    [CreatedByID]       INT           NULL,
    [UpdatedTime]       DATETIME      NULL,
    [ExtraDate1]        DATETIME      NULL,
    [ExtraDate2]        DATETIME      NULL,
    [MoveID]            INT           NULL,
    [CreatedTime]       DATETIME      NULL,
    [UpdatedByID]       INT           NULL,
    [VersionNumber]     INT           NULL,
    [CountryID]         INT           NULL,
    [EncounterChecksum] INT           NOT NULL,
    [Created]           DATETIME      NOT NULL,
    [Updated]           DATETIME      NULL,
    [ByWhom]            VARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Symphony_AuditAddress] PRIMARY KEY CLUSTERED ([AuditAddressID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Symphony_AuditAddress_01]
    ON [Symphony].[AuditAddress]([AddressTypeID] ASC, [LinkTypeID] ASC, [LinkID] ASC, [CreatedTime] ASC);


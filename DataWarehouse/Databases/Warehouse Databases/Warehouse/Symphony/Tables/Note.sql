﻿CREATE TABLE [Symphony].[Note] (
    [EncounterRecno]    INT            IDENTITY (1, 1) NOT NULL,
    [NoteID]            INT            NOT NULL,
    [Note]              VARCHAR (4000) NOT NULL,
    [RecordedByID]      INT            NOT NULL,
    [CreatedByID]       INT            NOT NULL,
    [UpdatedTime]       DATETIME       NOT NULL,
    [EncounterChecksum] INT            NOT NULL,
    [Created]           DATETIME       NOT NULL,
    [Updated]           DATETIME       NULL,
    [ByWhom]            VARCHAR (50)   NOT NULL,
    CONSTRAINT [PK_Note] PRIMARY KEY CLUSTERED ([NoteID] ASC)
);


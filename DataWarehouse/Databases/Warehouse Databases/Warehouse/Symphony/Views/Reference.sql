﻿create view Symphony.Reference as

--Based on a script originally written by Keith Lawrence

with

ReferenceCTE AS
	(
	-- Get the top level
	select
		ReferenceLevel = 1

		,ReferenceLookup.ReferenceLookupID
		,ReferenceLookup.ReferenceLookup

		,Structure = CAST(ReferenceLookup.ReferenceLookup AS VARCHAR(1000))

		,Active = ReferenceLookup.IsActive

		,NationalCode =
			(
			select
				cast(CDS.Note as varchar(100))
			from
				Symphony.ReferenceLookupMapping CDSMapping

			inner join Symphony.Note CDS
			on	CDS.NoteID = CDSMapping.ValueID

			where
				CDSMapping.ReferenceLookupID = ReferenceLookup.ReferenceLookupID
			and	CDSMapping.ReferenceMappingTypeID =
				(
				select
					MappingType.MappingTypeID
				from
					Symphony.MappingType
				where
					MappingType.MappingType = 'CDS'
				)
			)


		,CompositeNationalCode = 
				(
				select
					cast(CDS.Note as varchar(100))
				from
					Symphony.ReferenceLookupMapping CDSMapping

				inner join Symphony.Note CDS
				on	CDS.NoteID = CDSMapping.ValueID

				where
					CDSMapping.ReferenceLookupID = ReferenceLookup.ReferenceLookupID
				and	CDSMapping.ReferenceMappingTypeID =
					(
					select
						MappingType.MappingTypeID
					from
						Symphony.MappingType
					where
						MappingType.MappingType = 'CDS'
					)
			)

	from
		Symphony.ReferenceLookup -- This is the parent
	where
		ReferenceLookup.ReferenceLookupParentID = 0	-- Top level only
	and	ReferenceLookup.ReferenceLookupID IN
		(	-- Must be a parent level
		select
			ReferenceLookupParentID
		from
			Symphony.ReferenceLookup
		where
			ReferenceLookupParentID <> 0
		)

	union all

	select
		ReferenceLevel = ReferenceCTE.ReferenceLevel + 1

		,ReferenceLookup.ReferenceLookupID
		,ReferenceLookup.ReferenceLookup

		,Structure = CAST(ReferenceCTE.Structure + ' | ' + ReferenceLookup.ReferenceLookup AS VARCHAR(1000))

		,Active = ReferenceLookup.IsActive

		,NationalCode = 
			cast(
				(
				select
					CDS.Note
				from
					Symphony.ReferenceLookupMapping CDSMapping

				inner join Symphony.Note CDS
				on	CDS.NoteID = CDSMapping.ValueID

				where
					CDSMapping.ReferenceLookupID = ReferenceLookup.ReferenceLookupID
				and	CDSMapping.ReferenceMappingTypeID =
					(
					select
						MappingType.MappingTypeID
					from
						Symphony.MappingType
					where
						MappingType.MappingType = 'CDS'
					)

				and	not exists
					(
					select
						1
					from
						Symphony.ReferenceLookupMapping PreviousCDSMapping

					inner join Symphony.Note PreviousCDS
					on	PreviousCDS.NoteID = PreviousCDSMapping.ValueID

					where
						PreviousCDSMapping.ReferenceLookupID = ReferenceLookup.ReferenceLookupID
					and	PreviousCDSMapping.ReferenceMappingTypeID = CDSMapping.ReferenceMappingTypeID

					and	PreviousCDS.EncounterRecno > CDS.EncounterRecno

					)
				)
			 as varchar(100)
			)


		,CompositeNationalCode = 
			cast(isnull(ReferenceCTE.NationalCode, '') + 
					isnull(
					(
					select
						CDS.Note
					from
						Symphony.ReferenceLookupMapping CDSMapping

					inner join Symphony.Note CDS
					on	CDS.NoteID = CDSMapping.ValueID

					where
						CDSMapping.ReferenceLookupID = ReferenceLookup.ReferenceLookupID
					and	CDSMapping.ReferenceMappingTypeID =
						(
						select
							MappingType.MappingTypeID
						from
							Symphony.MappingType
						where
							MappingType.MappingType = 'CDS'
						)

					and	not exists
						(
						select
							1
						from
							Symphony.ReferenceLookupMapping PreviousCDSMapping

						inner join Symphony.Note PreviousCDS
						on	PreviousCDS.NoteID = PreviousCDSMapping.ValueID

						where
							PreviousCDSMapping.ReferenceLookupID = ReferenceLookup.ReferenceLookupID
						and	PreviousCDSMapping.ReferenceMappingTypeID = CDSMapping.ReferenceMappingTypeID

						and	PreviousCDS.EncounterRecno > CDS.EncounterRecno

						)
					)
					, ''
				)
				as varchar(100)
			)


	from
		Symphony.ReferenceLookup

	inner join ReferenceCTE ReferenceCTE
	on	ReferenceCTE.ReferenceLookupID = ReferenceLookup.ReferenceLookupParentID

	)
	
select
	ReferenceLevel = ReferenceLookup.ReferenceLevel
	,ReferenceID = ReferenceLookup.ReferenceLookupID
	,Reference = ReferenceLookup.ReferenceLookup
	,CompositeReference = ReferenceLookup.Structure
	,Active = ReferenceLookup.Active
	,NationalReferenceCode = ReferenceLookup.NationalCode
	,CompositeNationalReferenceCode = ReferenceLookup.CompositeNationalCode
from
	ReferenceCTE ReferenceLookup
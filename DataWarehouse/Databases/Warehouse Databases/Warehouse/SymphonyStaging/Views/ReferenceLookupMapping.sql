﻿
CREATE VIEW [SymphonyStaging].[ReferenceLookupMapping] as


select
	ReferenceLookupMappingID = LookupMappings.flm_id
	,ReferenceLookupID = LookupMappings.flm_lkpid
	,ReferenceMappingTypeID = LookupMappings.flm_mtid
	,ValueID = LookupMappings.flm_value
	,ReferenceLookupMapping = LookupMappings.flm_description
	,CreatedByID = LookupMappings.flm_createdby
	,CreatedTime = LookupMappings.flm_datecreated
	,MappedType = LookupMappings.flm_MappedType
from
	[$(Symphony)].dbo.LookupMappings

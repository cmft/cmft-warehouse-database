﻿
CREATE VIEW [SymphonyStaging].[DutyClinician] as

select
	DutyClinicianID = DutyClinician.dcl_id
	,StaffID = DutyClinician.dcl_staffid
	,StartTime = DutyClinician.dcl_startDate
	,EndTime = DutyClinician.dcl_enddate
	,CreatedTime = DutyClinician.dcl_created
	,UpdatedTime = DutyClinician.dcl_updated
	,CreatedBy = DutyClinician.dcl_createdby
	,UpdatedBy = DutyClinician.dcl_updatedby
from
	[$(Symphony)].dbo.DutyClinician

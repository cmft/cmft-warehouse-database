﻿
CREATE VIEW [SymphonyStaging].[PatientSystemId] as

select
	PatientSystemIdID = Patient_system_ids.psi_id
	,PatientID = Patient_system_ids.psi_pid
	,SystemID = Patient_system_ids.psi_system_name
	,PatientSystemId = Patient_system_ids.psi_system_id
	,StatusID = Patient_system_ids.psi_status
	,CreatedByID = Patient_system_ids.psi_createdby
	,UpdatedTime = Patient_system_ids.psi_update
	,PatientSystemIdReversed = Patient_system_ids.psi_system_idrev
from
	[$(Symphony)].dbo.Patient_system_ids

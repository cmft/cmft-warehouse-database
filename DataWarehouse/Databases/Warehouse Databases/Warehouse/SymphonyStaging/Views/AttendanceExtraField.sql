﻿
CREATE VIEW [SymphonyStaging].[AttendanceExtraField] as

select 
	AttendanceID = AttendanceExtraField.atd_EF_atdid
	,AttendanceExtraFieldID = AttendanceExtraField.atd_EF_extrafieldID
	,FieldTypeID = AttendanceExtraField.atd_EF_FieldType
	,FieldID = AttendanceExtraField.atd_EF_FieldID
	,FieldValue = AttendanceExtraField.atd_EF_Value
	,DataCategoryID = AttendanceExtraField.atd_EF_DataCategory
	,RecordID = AttendanceExtraField.atd_EF_RecordID
from
	[$(Symphony)].dbo.Attendance_Details_ExtraFields AttendanceExtraField
GO
--GRANT SELECT
--    ON OBJECT::[SymphonyStaging].[AttendanceExtraField] TO [scc]
--    AS [CMMC\Phil.Orrell];


﻿
CREATE VIEW [SymphonyStaging].[Attendance] as

select 
	AttendanceID = Attendance.atd_id
	,EpisodeID = Attendance.atd_epdid
	,AttendanceTypeID = Attendance.atd_attendancetype
	,StatusID = Attendance.atd_status
	,IsReattender = Attendance.atd_reattender
	,AttendanceNumber = Attendance.atd_num
	,ArrivalTime = Attendance.atd_arrivaldate
	,RegisteredTime = Attendance.atd_regdate
	,BookedTime = Attendance.atd_bookdate
	,CancelledTime = Attendance.atd_canceldate
	,Slot = Attendance.atd_slot
	,SourceOfReferralID = Attendance.atd_refsource
	,ReferralMethodID = Attendance.atd_refmethod
	,ReferralSpecialtyID = Attendance.atd_refspecialty
	,ConsultantID = Attendance.atd_consultant
	,GpCode = Attendance.atd_gpid
	,PracticeCode = Attendance.atd_prcode
	,PracticeAddressID = Attendance.atd_praddid
	,ArrivalModeID = Attendance.atd_arrmode
	,IncidentLocationTypeID = Attendance.atd_activity
	,LocationNotesID = Attendance.atd_locnotes
	,AmbulanceCrew = Attendance.atd_ambcrew
	,AttendanceConclusionTime = Attendance.atd_dischdate
	,AttendanceDisposalID = Attendance.atd_dischoutcome
	,DischargeDestinationID = Attendance.atd_dischdest
	,DischargeNotesID = Attendance.atd_dischnotes
	,PresentingProblem = Attendance.atd_complaintid
	,PatientGroupID = Attendance.atd_patgroup
	,CareGroupID = Attendance.atd_caregroup
	,PatientComplaint = Attendance.atd_patcomplaint
	,DischargeCreatedByID = Attendance.atd_dischcreatedby
	,DischargeCreatedTime = Attendance.atd_dischcreated
	,DischargeUpdatedByID = Attendance.atd_dischupdatedby
	,DischargeUpdatedByTime = Attendance.atd_dischupdated
	,AttendanceCreatedByID = Attendance.atd_createdby
	,AttendanceUpdatedByID = Attendance.atd_updatedby
	,AttendanceCreatedTime = Attendance.atd_created
	,AttendanceUpdatedTime = Attendance.atd_update
	,NonAttendanceReasonID = Attendance.atd_nonattendancereason
	,TransferPatient = Attendance.atd_TransferPatient
	,MergedByID = Attendance.atd_mergedby
	,MergeTime = Attendance.atd_mergecreated
	,IsDeleted = Attendance.atd_deleted
	,AccompaniedBy = Attendance.atd_accompany
from
	[$(Symphony)].dbo.Attendance_Details Attendance

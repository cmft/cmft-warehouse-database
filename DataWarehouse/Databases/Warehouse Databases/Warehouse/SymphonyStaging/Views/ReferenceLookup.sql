﻿
CREATE VIEW [SymphonyStaging].[ReferenceLookup] as

select
	ReferenceLookupID = Lookups.Lkp_ID
	,ReferenceLookupParentID = Lookups.Lkp_ParentID
	,ReferenceLookup = Lookups.Lkp_Name
	,DisplayOrder = Lookups.Lkp_DisplayOrder
	,InAscOrder = Lookups.Lkp_InAscOrder
	,Used = Lookups.Lkp_Used
	,CreatedTime = Lookups.Lkp_Datecreated
	,ReferenceLookupSystem = Lookups.Lkp_System
	,FlatLookupID = Lookups.Lkp_FlatLookupID
	,CreatedByID = Lookups.Lkp_createdby
	,UpdatedTime = Lookups.Lkp_update
	,DepartmentID = Lookups.lkp_deptid
	,RefreshedTime = Lookups.lkp_refreshed
	,IsDeleted = Lookups.lkp_deleted
	,IsActive = Lookups.lkp_active
	,IsSubAnalysed = Lookups.lkp_IsSubAnalysed
	,TableID = Lookups.lkp_TableID
from
	[$(Symphony)].dbo.Lookups

GO
--GRANT SELECT
--    ON OBJECT::[SymphonyStaging].[ReferenceLookup] TO [scc]
--    AS [CMMC\Phil.Orrell];


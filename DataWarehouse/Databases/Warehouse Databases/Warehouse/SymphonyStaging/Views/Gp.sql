﻿
CREATE VIEW [SymphonyStaging].[Gp] as

select
	GpID = Gp.gp_id
	,GpCode = Gp.gp_code
	,StartDate = Gp.gp_startdate
	,EndDate = Gp.gp_enddate
	,Surname = Gp.gp_surname
	,Initials = Gp.gp_initials
	,TitleID = Gp.gp_title
	,QualificationsID = Gp.gp_qualifications
	,LetterPreference = Gp.gp_letterpref
	,CreatedByID = Gp.gp_createdby
	,UpdatedTime = Gp.gp_update
	,CreatedTime = null --Gp.gp_created
	,UpdatedByID = null --Gp.gp_updatedby
	,RecordVersion = null --Gp.gp_RecVersion
from
	[$(Symphony)].dbo.Gp

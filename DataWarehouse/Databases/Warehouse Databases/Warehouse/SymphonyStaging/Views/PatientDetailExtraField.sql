﻿
CREATE VIEW [SymphonyStaging].[PatientDetailExtraField] as

select
	PatientDetailExtraFieldID = PatientDetailExtraField.pdt_EF_extrafieldID
	,FieldTypeID = PatientDetailExtraField.pdt_EF_FieldType
	,FieldID = PatientDetailExtraField.pdt_EF_FieldID
	,PatientID = PatientDetailExtraField.pdt_EF_pdtpid
	,FieldValue = PatientDetailExtraField.pdt_EF_Value
	,DataCategoryID = PatientDetailExtraField.pdt_EF_DataCategory
	,RecordID = PatientDetailExtraField.pdt_EF_RecordID
	,FieldValueCode = PatientDetailExtraField.pdt_EF_FieldCodeValues
	,ComputationStateID = PatientDetailExtraField.pdt_EF_ComputationState
	,DataEntryProcedureID = PatientDetailExtraField.pdt_EF_depId
	,CPMessagesID = PatientDetailExtraField.pdt_EF_CPMessagesIDs
from
	[$(Symphony)].dbo.Patient_Details_ExtraFields PatientDetailExtraField

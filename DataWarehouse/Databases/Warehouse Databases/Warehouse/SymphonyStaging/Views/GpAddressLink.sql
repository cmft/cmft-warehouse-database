﻿
CREATE VIEW [SymphonyStaging].[GpAddressLink] as

select
	GpID = GpAddressLink.gpa_gpid
	,GpCode = GpAddressLink.gpa_localcode
	,AddressID = GpAddressLink.gpa_praddid
	,GpPracticeID = GpAddressLink.gpa_prid
	,TelephoneID = GpAddressLink.gpa_telid
	,CreatedBy = GpAddressLink.gpa_CreatedBy
	,UpdatedTime = GpAddressLink.gpa_updated
from
	[$(Symphony)].dbo.GPPracticeAddressLink GpAddressLink

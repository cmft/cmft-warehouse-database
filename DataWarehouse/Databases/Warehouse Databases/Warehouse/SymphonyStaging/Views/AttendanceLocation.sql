﻿
CREATE VIEW [SymphonyStaging].[AttendanceLocation] as

select
	AttendanceLocationID = Current_Locations.cul_culid
	,AttendanceID = Current_Locations.cul_atdid
	,LocationID = Current_Locations.cul_locationid
	,LocationTime = Current_Locations.cul_locationdate
	,CreatedByID = Current_Locations.cul_createdby
	,UpdatedTime = Current_Locations.cul_update
from
	[$(Symphony)].dbo.Current_Locations

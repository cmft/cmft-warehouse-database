﻿
CREATE VIEW [SymphonyStaging].[Episode] as

select
	EpisodeID = Episodes.epd_id
	,PatientID = Episodes.epd_pid
	,EpisodeNumber = Episodes.epd_num
	,RecordedByID = Episodes.epd_recordedby
	,CreatedByID = Episodes.epd_createdby
	,UpdatedTime = Episodes.epd_update
	,DepartmentID = Episodes.epd_deptid
	,IsDeleted = Episodes.epd_deleted
from
	[$(Symphony)].dbo.Episodes

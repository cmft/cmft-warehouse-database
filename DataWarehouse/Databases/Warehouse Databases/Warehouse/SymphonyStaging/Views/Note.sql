﻿
CREATE VIEW [SymphonyStaging].[Note] as

select
	NoteID = Notes.not_noteid
	,Note = Notes.not_text
	,RecordedByID = Notes.not_recordedby
	,CreatedByID = Notes.not_createdby
	,UpdatedTime = Notes.not_update
from
	[$(Symphony)].dbo.Notes

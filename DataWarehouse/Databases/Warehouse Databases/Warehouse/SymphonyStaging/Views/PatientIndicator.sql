﻿
CREATE VIEW [SymphonyStaging].[PatientIndicator] as

select
	PatientIndicatorID = PatientIndicator.pin_id
	,PatientID = PatientIndicator.pin_pid
	,IndicatorTypeID = PatientIndicator.pin_type
	,IndicatorID = PatientIndicator.pin_indicator
	,PatientIndicatorFreeText = PatientIndicator.pin_freetext
	,CreatedBy = PatientIndicator.pin_createdby
	,CreatedTime = PatientIndicator.pin_created
	,ExpiryTime = PatientIndicator.Pin_expirydate
	,TPID = PatientIndicator.Pin_TPID
from
	[$(Symphony)].dbo.Patient_indicators PatientIndicator

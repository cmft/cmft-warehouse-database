﻿
CREATE VIEW [SymphonyStaging].[Location] as

select
	LocationID = CFG_Locations.loc_id
	,LocationParentID = CFG_Locations.loc_parentid
	,Location = CFG_Locations.loc_name
	,LocationIndex = CFG_Locations.loc_index
	,IsActive = CFG_Locations.loc_active
	,LocationDescription = CFG_Locations.loc_description
	,CreatedByID = CFG_Locations.loc_createdby
	,UpdatedTime = CFG_Locations.loc_update
	,ReasonForSuspension = CFG_Locations.loc_reasonforsuspension
	,SuspendedTime = CFG_Locations.loc_suspendedon
	,SuspendedByID = CFG_Locations.loc_suspendedby
	,DepartmentID = CFG_Locations.loc_deptid
	,IsUsed = CFG_Locations.loc_used
from
	[$(Symphony)].dbo.CFG_Locations

﻿
CREATE VIEW [SymphonyStaging].[Practice] as

select
	GpPracticeID = Gp_Practise.Pr_id
	,GpPracticeCode = Gp_Practise.pr_praccode
	,TelephoneID = Gp_Practise.pr_telid
	,GPFHCode = Gp_Practise.pr_gpfhcode
	,HealthVisitor = Gp_Practise.pr_healthvisitor
	,LocalCode = Gp_Practise.pr_localcode
	,StatusID = Gp_Practise.pr_status
	,LocalGpID = Gp_Practise.pr_localgp
	,PracticeGroupCareCode = Gp_Practise.pr_pgrpcarecode
	,CreatedByID = Gp_Practise.pr_createdby
	,UpdatedTime = Gp_Practise.pr_update
	,LetterPreference = Gp_Practise.pr_letterpref
	,CreatedTime = null --Gp_Practise.pr_created
	,UpdatedByID = null --Gp_Practise.pr_updatedby
	,RecordVersion = null --Gp_Practise.pr_RecVersion
from
	[$(Symphony)].dbo.Gp_Practise

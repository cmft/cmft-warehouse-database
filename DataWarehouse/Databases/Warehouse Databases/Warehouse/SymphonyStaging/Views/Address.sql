﻿
CREATE VIEW [SymphonyStaging].[Address] as

select
	AddressID = Address.add_addid
	,Line1 = Address.add_line1
	,Line2 = Address.add_line2
	,Line3 = Address.add_line3
	,Line4 = Address.add_line4
	,Line5 = Address.add_line5
	,Postcode = Address.add_postcode
	,DHACode = Address.add_dha
	,PCGCode = Address.add_pcg
	,AddressTypeID = Address.add_type
	,LinkTypeID = Address.add_linktype
	,LinkID = Address.add_linkid
	,CreatedByID = Address.add_createdby
	,UpdatedTime = Address.add_update
	,ExtraDate1 = Address.add_extradate1
	,ExtraDate2 = Address.add_extradate2
	,MoveID = Address.add_move
from
	[$(Symphony)].dbo.Address

﻿
CREATE VIEW [SymphonyStaging].[StaffMember] as

select
	StaffID = Staff.stf_staffid
	,Surname = Staff.stf_surname
	,TitleID = Staff.stf_title
	,Forename = Staff.stf_forename
	,MiddleNames = Staff.stf_midnames
	,HonorsID = Staff.stf_honors
	,JobTypeID = Staff.stf_jobtype
	,StartTime = Staff.stf_startdate
	,EndTime = Staff.stf_enddate
	,SpecialtyID = Staff.stf_specialty
	,LogonId = Staff.stf_logonid
	,JobTitle = Staff.stf_jobtitle
	,PasswordUpdatedTime = Staff.stf_passwordupdated
	,FailedLogons = Staff.stf_failedlogons
	,IsSuspended = Staff.stf_suspended
	,CreatedByID = Staff.stf_createdby
	,IsDeleted = Staff.stf_deleted
	,CreatedTime = Staff.stf_created
	,UpdatedTime = Staff.stf_update
	,HmeDepartment = Staff.stf_hmedept
	,IsDutyClinician = Staff.stf_dutyclinician
from
	[$(Symphony)].dbo.Staff

﻿
CREATE VIEW [SymphonyStaging].[Triage] as

select
	TriageID = Triage.tri_trid
	,AttendanceID = Triage.tri_atdid
	,RequestID = Triage.tri_reqid
	,DepartmentID = Triage.tri_depid
	,TriageTime = Triage.tri_date
	,Complaint = Triage.tri_complaint
	,Discriminator = Triage.tri_discriminator
	,CategoryID = Triage.tri_category
	,IsFasttrack = Triage.tri_fasttrack
	,FasttrackNoteID = Triage.tri_fasttracknote
	,Painscore = Triage.tri_painscore
	,CommentsID = Triage.tri_comments
	,Treatment = Triage.tri_treatment
	,NurseID = Triage.tri_nurse
	,IncludingPatientLetter = Triage.tri_includeingpletter
	,CreatedByID = Triage.tri_createdby
	,CreatedTime = Triage.tri_created
	,UpdatedByID = Triage.tri_updatedby
	,UpdatedTime = Triage.tri_update
	,CautionID = Triage.tri_caution
	,RepeatID = Triage.tri_repeat
	,IsInactive = Triage.tri_inactive
from
	[$(Symphony)].dbo.Triage

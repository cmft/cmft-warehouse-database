﻿
CREATE VIEW [SymphonyStaging].[Request] as

select
	 RequestID = Request.req_reqid
	,AttendanceID = Request.req_atdid
	,DataEntryProcedureID = Request.req_depid
	,RequestTime = Request.req_date
	,RequestID1 = Request.req_request
	,Field1ID = Request.req_field1
	,Field2ID = Request.req_field2
	,Field3ID = Request.req_field3
	,Field4ID = Request.req_field4
	,Field5ID = Request.req_field5
	,Field6ID = Request.req_field6
	,Field7ID = Request.req_field7
	,HasExtraFields = Request.req_hasefds
	,NoteID = Request.req_notes
	,IncludingPatientLetter = Request.req_includeingpletter
	,CautionID = Request.req_caution
	,RepeatID = Request.req_repeat
	,IsInactive = Request.req_inactive
	,CreatedByID = Request.req_createdby
	,CreatedTime = Request.req_created
	,UpdatedByID = Request.req_updatedby
	,UpdatedTime = Request.req_update
	,TPTransID = Request.Req_TPTransID
from
	[$(Symphony)].dbo.Request_Details Request
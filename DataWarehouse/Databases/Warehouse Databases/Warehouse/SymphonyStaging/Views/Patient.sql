﻿
CREATE VIEW [SymphonyStaging].[Patient] as

select
	PatientID = Patient.pat_pid
	,Surname = Patient.pat_surname
	,TitleID = Patient.pat_title
	,Forename = Patient.pat_forename
	,MiddleNames = Patient.pat_midnames
	,SexID = Patient.pat_sex
	,DateOfBirth = Patient.pat_dob
	,IsTemporaryRegistration = Patient.pat_tempreg
	,LastAttendanceNumber = Patient.pat_lastattno
	,CreatedByID = Patient.pat_createdby
	,UpdatedTime = Patient.pat_update
	,IsDead = Patient.pat_IsDead
	,IsDeleted = Patient.pat_deleted
from
	[$(Symphony)].dbo.Patient

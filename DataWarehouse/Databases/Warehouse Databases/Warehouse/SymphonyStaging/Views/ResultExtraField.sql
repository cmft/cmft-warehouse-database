﻿
CREATE VIEW [SymphonyStaging].[ResultExtraField] as

select
	ResultExtraFieldID = ResultExtraField.res_EF_extrafieldID
	,FieldTypeID = ResultExtraField.res_EF_FieldType
	,FieldID = ResultExtraField.res_EF_FieldID
	,AttendanceID = ResultExtraField.res_EF_atdid
	,FieldValue = ResultExtraField.res_EF_Value
	,DataCategoryID = ResultExtraField.res_EF_DataCategory
	,RecordID = ResultExtraField.res_EF_RecordID
	,FieldValueCode = ResultExtraField.res_EF_FieldCodeValues
	,ComputationStateID = ResultExtraField.Res_EF_ComputationState
	,DataEntryProcedureID = ResultExtraField.Res_EF_depId
	,CPMessagesID = ResultExtraField.Res_EF_CPMessagesIDs			
	,CreatedBy = ResultExtraField.res_EF_CreatedBy
	,CreatedTime = ResultExtraField.res_EF_created
	,UpdatedBy = ResultExtraField.res_EF_updatedby
	,UpdatedTime = ResultExtraField.res_EF_update
from
	[$(Symphony)].dbo.Result_Details_ExtraFields ResultExtraField

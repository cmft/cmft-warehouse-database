﻿
CREATE VIEW [SymphonyStaging].[RequestExtraField] as

select
	AttendanceID = RequestExtraField.req_EF_atdid
	,RequestExtraFieldID = RequestExtraField.req_EF_extrafieldID
	,FieldTypeID = RequestExtraField.req_EF_FieldType
	,FieldID = RequestExtraField.req_EF_FieldID
	,FieldValue = RequestExtraField.req_EF_Value
	,DataCategoryID = RequestExtraField.req_EF_DataCategory
	,RecordID = RequestExtraField.req_EF_RecordID
	,FieldValueCode = RequestExtraField.req_EF_FieldCodeValues
	,ComputationalState = RequestExtraField.Req_EF_ComputationState
	,DataEntryProcedureID = RequestExtraField.Req_EF_depId
	,CPMessagesID = RequestExtraField.Req_EF_CPMessagesIDs
from
	[$(Symphony)].dbo.Request_Details_ExtraFields RequestExtraField
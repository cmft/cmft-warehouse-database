﻿
CREATE VIEW [SymphonyStaging].[Result] as

select
		ResultID = Result.res_resid
	,RequestID = Result.res_reqid
	,AttendanceID = Result.res_atdid
	,DataEntryProcedureID = Result.res_depid
	,ResultTime = Result.res_date
	,ResultID1 = Result.res_result
	,Field1ID = Result.res_field1
	,Field2ID = Result.res_field2
	,Field3ID = Result.res_field3
	,Field4ID = Result.res_field4
	,Field5ID = Result.res_field5
	,Field6ID = Result.res_field6
	,Field7ID = Result.res_field7
	,Field8ID = Result.res_field8
	,Field9ID = Result.res_field9
	,Field10ID = Result.res_field10
	,Field11ID = Result.res_field11
	,Field12ID = Result.res_field12
	,Field13ID = Result.res_field13
	,StaffID = Result.res_staff1
	,HasExtraFields = Result.res_hasefds
	,NoteID = Result.res_note
	,IncludingPatientLetter = Result.res_includeingpletter
	,CreatedByID = Result.res_createdby
	,CreatedTime = Result.res_created
	,UpdatedByID = Result.res_updatedby
	,UpdatedTime = Result.res_updated
	,CautionID = Result.res_caution
	,RepeatID = Result.res_repeat
	,DnmImageid = Result.res_dnmimageid
	,TPTransID = Result.Res_TPTransID
	,IsCancellation = Result.res_IsCancellation
	,IsInactive = Result.res_inactive
from
	[$(Symphony)].dbo.Result_details Result

﻿
CREATE VIEW [SymphonyStaging].[PatientDetail] as

select
	PatientID = Patient_details.pdt_pid
	,GpID = Patient_details.pdt_gpid
	,PracticeID = Patient_details.pdt_practise
	,HonoursID = Patient_details.pdt_honours
	,PracticeAddressID = Patient_details.pdt_praddid
	,NoteID = Patient_details.pdt_noteid
	,DateOfDeath = Patient_details.pdt_dod
	,MaritalStatusID = Patient_details.pdt_maritalstatus
	,ReligionID = Patient_details.pdt_religion
	,EthnicCategoryID = Patient_details.pdt_ethnic
	,Occupation = Patient_details.pdt_occupation
	,EmploymentStatusID = Patient_details.pdt_employmentstatus
	,OverseasVisitorStatusID = Patient_details.pdt_overseas
	,NationalityID = Patient_details.pdt_nationality
	,PUPID = Patient_details.pdt_PUP
	,PEPID = Patient_details.pdt_PEP
	,CreatedByID = Patient_details.pdt_createdby
	,UpdatedTime = Patient_details.pdt_update
	,GpMoveID = Patient_details.pdt_gpmove
	,SpecialCase = Patient_details.pdt_SpecialCaseUwm
	,IsSpecialCase = Patient_details.pdt_IsSpecialCase
from
	[$(Symphony)].dbo.Patient_details

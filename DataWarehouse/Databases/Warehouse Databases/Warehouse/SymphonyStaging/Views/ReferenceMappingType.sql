﻿
CREATE VIEW [SymphonyStaging].[ReferenceMappingType] as

select
	MappingTypeID = MappingTypes.mt_id
	,MappingType = MappingTypes.mt_name
	,MappingTypeDescription = MappingTypes.mt_description
	,IsUsed = MappingTypes.mt_used
	,CreatedByID = MappingTypes.mt_createdby
	,CreatedTime = MappingTypes.mt_datecreated
from
	[$(Symphony)].dbo.MappingTypes

﻿CREATE TABLE [Renal].[PatientRenalModality] (
    [SourceUniqueID]            VARCHAR (100)  NULL,
    [PatientObjectID]           INT            NULL,
    [PatientFullName]           NVARCHAR (40)  NULL,
    [PatientMedicalRecordNo]    NVARCHAR (12)  NULL,
    [StartDate]                 DATETIME       NULL,
    [StopDate]                  DATETIME       NULL,
    [ModalityCode]              NVARCHAR (256) NULL,
    [ModalitySettingCode]       NVARCHAR (256) NULL,
    [DialysisProviderCode]      NVARCHAR (100) NULL,
    [EventDetailCode]           NVARCHAR (256) NULL,
    [InformationSourceCode]     NVARCHAR (256) NULL,
    [ReasonForChangeCode]       NVARCHAR (256) NULL,
    [Notes]                     NVARCHAR (MAX) NULL,
    [IsPrimary]                 INT            NOT NULL,
    [AgeAtEvent]                NUMERIC (18)   NULL,
    [PatientRenalModalityRecno] INT            IDENTITY (1, 1) NOT NULL,
    [Created]                   DATETIME       NULL,
    [Updated]                   DATETIME       NULL,
    [ByWhom]                    VARCHAR (100)  NULL,
    CONSTRAINT [PK__PatientR__BB970AA01E665FDE] PRIMARY KEY CLUSTERED ([PatientRenalModalityRecno] ASC)
);


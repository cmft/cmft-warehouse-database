﻿CREATE TABLE [BloodTrack].[TransactionTypeBase] (
    [TransactionTypeID] INT           NOT NULL,
    [TransactionType]   VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([TransactionTypeID] ASC)
);


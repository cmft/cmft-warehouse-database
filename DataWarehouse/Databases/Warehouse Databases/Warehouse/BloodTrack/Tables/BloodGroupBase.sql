﻿CREATE TABLE [BloodTrack].[BloodGroupBase] (
    [BloodGroupID]   INT          NOT NULL,
    [BloodGroupCode] VARCHAR (10) NULL,
    [BloodGroup]     VARCHAR (10) NULL,
    [Alternate]      VARCHAR (10) NULL,
    PRIMARY KEY CLUSTERED ([BloodGroupID] ASC)
);


﻿CREATE TABLE [BloodTrack].[BloodUnitFateBase] (
    [FateID] INT           NOT NULL,
    [Fate]   VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([FateID] ASC)
);


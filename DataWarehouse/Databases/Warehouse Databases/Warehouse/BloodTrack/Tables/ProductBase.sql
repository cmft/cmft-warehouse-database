﻿CREATE TABLE [BloodTrack].[ProductBase] (
    [BloodProductID] INT           NOT NULL,
    [ProductCode]    VARCHAR (10)  NULL,
    [Product]        VARCHAR (100) NULL,
    [Courier]        VARCHAR (50)  NULL,
    [SafeTx]         VARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([BloodProductID] ASC)
);


﻿CREATE TABLE [BloodTrack].[SystemUserBase] (
    [UserID]          INT           NOT NULL,
    [UserName]        VARCHAR (100) NULL,
    [UserDesignation] VARCHAR (100) NULL,
    [StaffNumber]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([UserID] ASC)
);


﻿CREATE TABLE [BloodTrack].[LocationBase] (
    [LocationID]   INT           NOT NULL,
    [LocationName] VARCHAR (100) NULL,
    [Location]     VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([LocationID] ASC)
);


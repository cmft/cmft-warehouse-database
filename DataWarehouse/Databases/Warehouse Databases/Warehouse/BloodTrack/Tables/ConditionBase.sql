﻿CREATE TABLE [BloodTrack].[ConditionBase] (
    [ConditionID] INT           NOT NULL,
    [Condition]   VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([ConditionID] ASC)
);


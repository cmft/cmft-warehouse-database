﻿CREATE TABLE [BloodTrack].[SiteBase] (
    [SiteID]   INT           NOT NULL,
    [SiteName] VARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([SiteID] ASC)
);


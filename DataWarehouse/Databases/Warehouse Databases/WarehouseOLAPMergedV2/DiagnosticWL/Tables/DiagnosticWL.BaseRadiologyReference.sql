﻿CREATE TABLE [DiagnosticWL].[BaseRadiologyReference] (
    [MergeRecno]   INT NOT NULL,
    [ContextID]    INT NOT NULL,
    [ExamID]       INT NOT NULL,
    [CensusDateID] INT NOT NULL,
    [SpecialtyID]  INT NOT NULL,
    [SiteID]       INT NOT NULL,
    [SexID]        INT NOT NULL
);


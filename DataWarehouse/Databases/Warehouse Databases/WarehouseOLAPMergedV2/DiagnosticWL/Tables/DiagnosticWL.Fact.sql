﻿CREATE TABLE [DiagnosticWL].[Fact] (
    [MergeRecno]           INT           NOT NULL,
    [ContextID]            INT           NOT NULL,
    [CensusDateID]         INT           NOT NULL,
    [DirectorateID]        INT           NOT NULL,
    [CCGID]                INT           NOT NULL,
    [NationalExamID]       INT           NOT NULL,
    [TimeBandID]           INT           NOT NULL,
    [ExamID]               INT           NOT NULL,
    [DataSourceID]         INT           NOT NULL,
    [SpecialtyID]          INT           NOT NULL,
    [ProfessionalCarerID]  INT           NOT NULL,
    [SiteID]               INT           NOT NULL,
    [SexID]                INT           NOT NULL,
    [AgeID]                INT           NOT NULL,
    [WeeksWaitingAtTCIID]  INT           NOT NULL,
    [ServiceRequestTypeID] INT           NULL,
    [IsCancelled]          BIT           NULL,
    [IsReportable]         BIT           NULL,
    [Created]              DATETIME2 (0) CONSTRAINT [df_DiagnosticWL_Fact_Created] DEFAULT (getdate()) NULL,
    [Updated]              DATETIME2 (0) CONSTRAINT [df_DiagnosticWL_Fact_Updated] DEFAULT (NULL) NULL,
    [ByWhom]               VARCHAR (50)  CONSTRAINT [df_DiagnosticWL_Fact_ByWhom] DEFAULT (suser_name()) NULL
);


﻿CREATE TABLE [DiagnosticWL].[BaseReference] (
    [MergeRecno]          INT           NOT NULL,
    [ContextID]           INT           NOT NULL,
    [ExamID]              INT           NOT NULL,
    [CensusDateID]        INT           NOT NULL,
    [SpecialtyID]         INT           NOT NULL,
    [ProfessionalCarerID] INT           NOT NULL,
    [SiteID]              INT           NOT NULL,
    [SexID]               INT           NOT NULL,
    [Created]             DATETIME2 (0) CONSTRAINT [df_DiagnosticWL_BaseReference_Created] DEFAULT (getdate()) NULL,
    [Updated]             DATETIME2 (0) CONSTRAINT [df_DiagnosticWL_BaseReference_Updated] DEFAULT (NULL) NULL,
    [ByWhom]              VARCHAR (50)  CONSTRAINT [df_DiagnosticWL_BaseReference_ByWhom] DEFAULT (suser_name()) NULL
);


﻿CREATE procedure [DiagnosticWL].[BuildFact] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table DiagnosticWL.Fact


insert
into
	DiagnosticWL.Fact
(
	 MergeRecno
	,ContextID
	,CensusDateID
	,DirectorateID
	,CCGID
	,NationalExamID
	,TimeBandID
	,ExamID
	,DataSourceID
	,SpecialtyID
	,ProfessionalCarerID
	,SiteID
	,SexID
	,AgeID
	,WeeksWaitingAtTCIID
	,ServiceRequestTypeID
	,IsCancelled
	,IsReportable
)
select
	 Base.MergeRecno
	,Context.ContextID
	,BaseReference.CensusDateID
	,Directorate.DirectorateID

	,CCGID =
		coalesce(
			 CCG.CCGID
			,0
		)

	,NationalExamID =
		coalesce(
			 NationalExam.NationalExamID
			,1
		)

	,TimeBandID =
		case
		when Base.WeeksWaiting > 12
		then 13
		when Base.WeeksWaiting < 0
		then -1
		when Base.WeeksWaiting is null
		then -1
		else Base.WeeksWaiting
		end

	,ExamID = BaseReference.ExamID

	,Base.DataSourceID

	,BaseReference.SpecialtyID
	,BaseReference.ProfessionalCarerID
	,BaseReference.SiteID
	,BaseReference.SexID
	,Age.AgeID

	,WeeksWaitingAtTCIID =
		case
		when Base.WeeksWaitingAtTCI > 12
		then 13
		when Base.WeeksWaitingAtTCI < 0
		then -1
		when Base.WeeksWaitingAtTCI is null
		then -1
		else Base.WeeksWaitingAtTCI
		end

	,ServiceRequestType.ServiceRequestTypeID

	,Base.IsCancelled

	,IsReportable =
		case
		when Base.IsCancelled = 1 then 0
		when NationalExam.DM01DiagnosticTestCode = 'NA' then 0
		when Base.ServiceRequestTypeCode = '02' then 0 --Planned test required
		else 1
		end

from
	DiagnosticWL.Base

inner join DiagnosticWL.BaseReference
on	BaseReference.MergeRecno = Base.MergeRecno

left join WH.CCG
on	CCG.CCGCode = Base.CCGCode

left join Diagnostic.NationalExam
on	NationalExam.NationalExamCode = Base.NationalExamCode

inner join WH.Context
on	Context.ContextCode = Base.ContextCode

inner join WH.Directorate
on	Directorate.DirectorateCode = Base.DirectorateCode

left join WH.Age
on	Age.AgeCode = Base.AgeCode

inner join Diagnostic.ServiceRequestType
on	ServiceRequestType.ServiceRequestTypeCode = Base.ServiceRequestTypeCode

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




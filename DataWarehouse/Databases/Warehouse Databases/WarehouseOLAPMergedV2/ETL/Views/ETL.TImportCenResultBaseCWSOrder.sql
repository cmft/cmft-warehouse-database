﻿



CREATE view [ETL].[TImportCenResultBaseCWSOrder] as 

select 
	OrderRecno = Import.ResultRecno
	,ContextCode = 'CEN||PAS'
	,SourceUniqueID = Import.OrderSourceUniqueID
	,PatientID = Import.SourcePatientNo
	,PatientIdentifier = Patient.DistrictNo
	,DistrictNo = Patient.DistrictNo
	,CasenoteNumber = null
	,Patient.NHSNumber
	,ConsultantCode = Import.ConsultantCode
	,MainSpecialtyCode
	,SpecialtyCode
	,ProviderCode = ScreenTypeCode
	,LocationCode = null
	,OrderPriority = null
	,OrderStatusCode
	,OrderRequestTime = OrderTime
	,OrderReceivedTime = null
	,OrderCollectionTime = null
	,InterfaceCode
from
	[$(Warehouse)].Result.CWSNumericResult Import

inner join 
	(
	select
		OrderSourceUniqueID
		,ResultRecno = max(ResultRecno)
	from
		[$(Warehouse)].Result.CWSNumericResult Later
	group by
		OrderSourceUniqueID
	) Later
on	Later.OrderSourceUniqueID = Import.OrderSourceUniqueID
and Later.ResultRecno = Import.ResultRecno


left join [$(Warehouse)].PAS.Patient
on	Import.SourcePatientNo = Patient.SourcePatientNo

left join [$(Warehouse)].PAS.Consultant
on	Import.ConsultantCode = Consultant.ConsultantCode

left join [$(Warehouse)].PAS.Service
on	Service.ServiceCode = Import.ServiceCode

union 

select 
	OrderRecno = Import.ResultRecno
	,ContextCode = 'CEN||PAS'
	,SourceUniqueID = Import.OrderSourceUniqueID
	,Import.SourcePatientNo
	,Patient.DistrictNo
	,Patient.DistrictNo
	,CasenoteNumber = null
	,Patient.NHSNumber
	,ConsultantCode = Import.ConsultantCode
	,MainSpecialtyCode
	,SpecialtyCode
	,ProviderCode = ScreenTypeCode
	,LocationCode = null
	,OrderPriority = null
	,OrderStatusCode
	,OrderRequestTime = OrderTime
	,OrderReceivedTime = null
	,OrderCollectionTime = null
	,InterfaceCode
from
	[$(Warehouse)].Result.CWSTextResult Import

inner join 
	(
	select
		OrderSourceUniqueID
		,ResultRecno = max(ResultRecno)
	from
		[$(Warehouse)].Result.CWSTextResult Later
	group by
		OrderSourceUniqueID
	) Later
on	Later.OrderSourceUniqueID = Import.OrderSourceUniqueID
and Later.ResultRecno = Import.ResultRecno


left join [$(Warehouse)].PAS.Patient
on	Import.SourcePatientNo = Patient.SourcePatientNo

left join [$(Warehouse)].PAS.Consultant
on	Import.ConsultantCode = Consultant.ConsultantCode

left join [$(Warehouse)].PAS.Service
on	Service.ServiceCode = Import.ServiceCode


























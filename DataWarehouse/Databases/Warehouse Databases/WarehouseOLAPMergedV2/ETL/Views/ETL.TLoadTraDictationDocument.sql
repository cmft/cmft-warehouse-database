﻿










CREATE view [ETL].[TLoadTraDictationDocument]

as

--select --top 100
--	SourceUniqueID = DocumentID
--	,SourcePatientNo = PatientNoID
--	,DocumentID = DocumentID
--	,DocumentDescription = FormType
--	,DocumentTypeCode = 'F' -- no codes in EPR so we just use the code from medisec that denotes Discharge notification form
--	,AuthorCode = null
--	,TypedTime = DocumentDate
--	,DictatedTime = DocumentDate 
--	,DictatedByCode = null
--	,IssuedTime = null
--	,IssuedByCode = null
--	,SignedTime = DocumentDate
--	,SignedByCode = null
--	,SignedStatusCode = 'G' -- no codes in EPR so we just use the code from medisec that denotes letter sent to gp
--	,DocumentXMLTemplateCode = null
--	,DocumentXML = DocumentXML
--	,ContextCode = 'TRA||EPR'
--from
--	TraffordWarehouse.dbo.EPRImmediateDischarge  


select
	SourceUniqueID = Document.Sourceuniqueid
	,SourcePatientNo = Document.EPMI
	,DocumentID = cast(Document.DocumentID as varchar(50))
	,DocumentDescription = null
	,DocumentTypeCode = Document.TypeID
	,AuthorCode = null
	,TypedTime = Document.IDSDocumentDate
	,DictatedTime = Document.IDSDocumentDate 
	,DictatedByCode = null
	,IssuedTime = Document.IDSDocumentDate
	,IssuedByCode = null
	,SignedTime = Document.IDSDocumentDate
	,SignedByCode = null
	,SignedStatusCode = DocumentStatusID							
	,DocumentXMLTemplateCode = null
	,DocumentXML = Document.IDSXML 
    ,TransmissionTime = cast(null as datetime)
	,TransmissionDestinationCode = null

	,ContextCode = 'TRA||EPR'
from
	[$(TraffordWarehouse)].ETL.TimportEPRIDS Document

left join [$(TraffordWarehouse)].EPR.DocumentStatus
on	DocumentStatus.DocumentStatus = Document.IDSStatus

	
where
	not exists
			(
			select
				1
			from 
				[$(TraffordWarehouse)].ETL.TimportEPRIDS DocumentLater
			where
				--(
				--	DocumentLater.SourceUniqueID = Document.SourceUniqueID
				--and DocumentLater.DocumentID > Document.DocumentID
				--)
			--or
				(
					DocumentLater.DocumentID = Document.DocumentID
				and DocumentLater.Sourceuniqueid > Document.Sourceuniqueid
				)
			)











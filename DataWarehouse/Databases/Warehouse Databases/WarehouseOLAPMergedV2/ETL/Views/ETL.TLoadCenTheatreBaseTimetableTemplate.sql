﻿
CREATE view [ETL].[TLoadCenTheatreBaseTimetableTemplate] as

select
	 TimetableTemplate.TimetableTemplateCode
	,TimetableTemplate.SessionNumber
	,TimetableTemplate.DayNumber
	,TimetableTemplate.TheatreCode
	,TimetableTemplate.SessionStartTime
	,TimetableTemplate.SessionEndTime
	,ConsultantCode = Consultant.StaffCode
	,AnaesthetistCode = Anaesthetist.StaffCode
	,SpecialtyCode = Specialty.SpecialtyCode

	,TimetableTemplate.LogLastUpdated
	,TimetableTemplate.RecordLogTemplates
from
	[$(Warehouse)].Theatre.TimetableTemplate

left join [$(Warehouse)].Theatre.Staff Anaesthetist
on	Anaesthetist.StaffCode1 = TimetableTemplate.AnaesthetistCode

left join [$(Warehouse)].Theatre.Staff Consultant
on	Consultant.StaffCode1 = TimetableTemplate.ConsultantCode

left join [$(Warehouse)].Theatre.Specialty
on	Specialty.SpecialtyCode1 = TimetableTemplate.SpecialtyCode


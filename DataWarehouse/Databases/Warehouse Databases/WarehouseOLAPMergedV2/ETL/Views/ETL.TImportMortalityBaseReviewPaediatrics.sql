﻿






CREATE View [ETL].[TImportMortalityBaseReviewPaediatrics]


as


Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode 
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode = CAST(PrimaryReviewerCode as varchar(50))
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo = row_number() over (partition by SourceUniqueID order by SourceUniqueID)
		,Question
		,Response
		,ContextCode = 'CMFT||MORTARCHV'
	from
		(
		select
			SourceUniqueID
			,SourcePatientNo	
			,EpisodeStartTime	
			,APCContextCode = ContextCode
			,FormTypeCode = FormTypeID
			,ReviewStatusCode = ReviewStatus
			,PrimaryReviewerAssignedTime = ReviewedDate
			,PrimaryReviewerCompletedTime = 
				case
					when ReviewStatus = 10 then ReviewedDate
					else null
				end
			,PrimaryReviewerCode = ReviewedBy
			,SecondaryReviewerAssignedTime = null
			,SecondaryReviewerCompletedTime = null
			,SecondaryReviewerCode = SecondaryReviewer
			,ClassificationGrade = null
			,RequiresFurtherInvestigation = null
			
			,DateOfDeath = isnull(cast(DateOfDeath as varchar(max)),'')
			,ClinicalSummary = cast(ClinicalSummary as varchar(max))
			,PlaceOfDeath = cast(PlaceOfDeath as varchar(max))
			,AgeAtDeath = cast(AgeAtDeath as varchar(max))
			,TransferredFromOtherHospital = cast(TransferredFromOtherHospital as varchar(max))
			,TransferredFrom = cast(TransferredFrom as varchar(max))
			,TransferredFromOther = cast(TransferredFromOther as varchar(max))
			,ClinicalDiagnosis = cast(ClinicalDiagnosis as varchar(max))
			,AdmissionForTerminalCare = cast(AdmissionForTerminalCare as varchar(max))
			,EndOfLifeCarePlan = cast(EndOfLifeCarePlan as varchar(max))
			,AdmittedToICU = cast(AdmittedToICU as varchar(max))
			,ICUSource = cast(ICUSource as varchar(max))
			,ICUWardEWS = cast(ICUWardEWS as varchar(max))
			,ICUWardComments = cast(ICUWardComments as varchar(max))
			,TheatrePlanned = cast(TheatrePlanned as varchar(max))
			,TheatrePlannedComment = cast(TheatrePlannedComment as varchar(max))
			,AEComments = cast(AEComments as varchar(max))
			,DrugErrorIdentified = cast(DrugErrorIdentified as varchar(max))
			,DrugErrorIdentifiedComments = cast(DrugErrorIdentifiedComments as varchar(max))
			,MRSA = cast(MRSA as varchar(max))
			,CDifficile = cast(CDifficile as varchar(max))
			,CriticalIncidentBeforeMortalityReview = cast(CriticalIncidentBeforeMortalityReview as varchar(max))
			,CriticalIncidentBeforeMortalityReviewComments = cast(CriticalIncidentBeforeMortalityReviewComments as varchar(max))
			,WithdrawalOfCare = cast(WithdrawalOfCare as varchar(max))
			,Indication = cast(Indication as varchar(max))
			,DocumentationType = cast(DocumentationType as varchar(max))
			,ByWhom = cast(ByWhom as varchar(max))
			,LeadConsultant = cast(LeadConsultant as varchar(max))
			,LeadConsultantEasilyIdentified = cast(LeadConsultantEasilyIdentified as varchar(max))
			,OtherConsultantInvolved = cast(OtherConsultantInvolved as varchar(max))
			,GradeOfMostSeniorClinicianInvolved = cast(GradeOfMostSeniorClinicianInvolved as varchar(max))
			,PreviousAdmission = cast(PreviousAdmission as varchar(max))
			,ChronicIllness = cast(ChronicIllness as varchar(max))
			,ChronicIllnessSpecify = cast(ChronicIllnessSpecify as varchar(max))
			,RelevantPreviousAdmissionsAttendances = cast(RelevantPreviousAdmissionsAttendances as varchar(max))
			,OperationsProceduresPerformed = cast(OperationsProceduresPerformed as varchar(max))
			,CaseReferredToCoroner = cast(CaseReferredToCoroner as varchar(max))
			,ConsultantReferringCase = cast(ConsultantReferringCase as varchar(max))
			,CaseDiscussedWithCoroner = cast(CaseDiscussedWithCoroner as varchar(max))
			,CaseDiscussedWithCoronerConsultant = cast(CaseDiscussedWithCoronerConsultant as varchar(max))
			,CaseDiscussedWithCoronerDocumentedInMedicalRecords = cast(CaseDiscussedWithCoronerDocumentedInMedicalRecords as varchar(max))
			,CaseDiscussedWithCoronerPostMortemPerformed = cast(CaseDiscussedWithCoronerPostMortemPerformed as varchar(max))
			,CaseDiscussedWithCoronerOutcomeAppropriate = cast(CaseDiscussedWithCoronerOutcomeAppropriate as varchar(max))
			,CaseDiscussedWithCoronerOutcomeNotAppropriateComments = cast(CaseDiscussedWithCoronerOutcomeNotAppropriateComments as varchar(max))
			,DeathCertificateIssued = cast(DeathCertificateIssued as varchar(max))
			,RequestForHospitalPostMortem = cast(RequestForHospitalPostMortem as varchar(max))
			,RequestForHospitalPostMortemMadeToParentsByWhom = cast(RequestForHospitalPostMortemMadeToParentsByWhom as varchar(max))
			,RequestForHospitalPostMortemMadeToParentsByOther = cast(RequestForHospitalPostMortemMadeToParentsByOther as varchar(max))
			,PermissionForHospitalPostMortemGiven = cast(PermissionForHospitalPostMortemGiven as varchar(max))
			,FullPermissionForHospitalPostMortemGiven = cast(FullPermissionForHospitalPostMortemGiven as varchar(max))
			,LimitedPermissionForHospitalPostMortemGiven = cast(LimitedPermissionForHospitalPostMortemGiven as varchar(max))
			,CauseOfDeath1a = cast(CauseOfDeath1a as varchar(max))
			,CauseOfDeath1b = cast(CauseOfDeath1b as varchar(max))
			,CauseOfDeath1c = cast(CauseOfDeath1c as varchar(max))
			,CauseOfDeath2 = cast(CauseOfDeath2 as varchar(max))
			,CauseOfDeathAgreed = cast(CauseOfDeathAgreed as varchar(max))
			,CauseOfDeathNotAgreedReason = cast(CauseOfDeathNotAgreedReason as varchar(max))
			,AnyFurtherPointsOfDiscussion = cast(AnyFurtherPointsOfDiscussion as varchar(max))
			,SignificantQuestionsWhichRemainUnanswered = cast(SignificantQuestionsWhichRemainUnanswered as varchar(max))
			,IssuesForDiscussion = cast(IssuesForDiscussion as varchar(max))
			,OrganDonationDiscussed = cast(OrganDonationDiscussed as varchar(max))
			,TissueDonationDiscussed = cast(TissueDonationDiscussed as varchar(max))
			,OrgansDonated = cast(OrgansDonated as varchar(max))
			,TissuesDonated = cast(TissuesDonated as varchar(max))
			,SecondaryReviewReason = cast(SecondaryReviewReason as varchar(max))
			,SecondaryReviewOutcome = cast(SecondaryReviewOutcome as varchar(max))
			,SecondaryReviewActions = cast(SecondaryReviewActions as varchar(max))
			,SecondaryReviewPatientCareConsultant = cast(SecondaryReviewPatientCareConsultant as varchar(max))
			,SecondaryReviewDiscussedConsultants = cast(SecondaryReviewDiscussedConsultants as varchar(max))
			,SecondaryReviewerDesignation = cast(SecondaryReviewerDesignation as varchar(max))
			,SecondaryReviewFuturePresentation = cast(SecondaryReviewFuturePresentation as varchar(max))
			,SecondaryReviewFuturePresentationPersonNominated = cast(SecondaryReviewFuturePresentationPersonNominated as varchar(max))
			,SecondaryReviewRequired = cast(SecondaryReviewRequired as varchar(max))
			,PersonNominatedForSecondReview = cast(PersonNominatedForSecondReview as varchar(max))
			,DiagDiseaseGroup = cast(DiagDiseaseGroup as varchar(max))
			,RecurringThemes = cast(RecurringThemes as varchar(max))
			,CodingCategory = cast(CodingCategory as varchar(max))
		from
			[$(Warehouse)].Mortality.Paediatrics
		) 
		Mortality

		unpivot
				(
				Response for Question in 
						(
						DateOfDeath
						,ClinicalSummary
						,PlaceOfDeath
						,AgeAtDeath
						,TransferredFromOtherHospital
						,TransferredFrom
						,TransferredFromOther
						,ClinicalDiagnosis
						,AdmissionForTerminalCare
						,EndOfLifeCarePlan
						,AdmittedToICU
						,ICUSource
						,ICUWardEWS
						,ICUWardComments
						,TheatrePlanned
						,TheatrePlannedComment
						,AEComments
						,DrugErrorIdentified
						,DrugErrorIdentifiedComments
						,MRSA
						,CDifficile
						,CriticalIncidentBeforeMortalityReview
						,CriticalIncidentBeforeMortalityReviewComments
						,WithdrawalOfCare
						,Indication
						,DocumentationType
						,ByWhom
						,LeadConsultant
						,LeadConsultantEasilyIdentified
						,OtherConsultantInvolved
						,GradeOfMostSeniorClinicianInvolved
						,PreviousAdmission
						,ChronicIllness
						,ChronicIllnessSpecify
						,RelevantPreviousAdmissionsAttendances
						,OperationsProceduresPerformed
						,CaseReferredToCoroner
						,ConsultantReferringCase
						,CaseDiscussedWithCoroner
						,CaseDiscussedWithCoronerConsultant
						,CaseDiscussedWithCoronerDocumentedInMedicalRecords
						,CaseDiscussedWithCoronerPostMortemPerformed
						,CaseDiscussedWithCoronerOutcomeAppropriate
						,CaseDiscussedWithCoronerOutcomeNotAppropriateComments
						,DeathCertificateIssued
						,RequestForHospitalPostMortem
						,RequestForHospitalPostMortemMadeToParentsByWhom
						,RequestForHospitalPostMortemMadeToParentsByOther
						,PermissionForHospitalPostMortemGiven
						,FullPermissionForHospitalPostMortemGiven
						,LimitedPermissionForHospitalPostMortemGiven
						,CauseOfDeath1a
						,CauseOfDeath1b
						,CauseOfDeath1c
						,CauseOfDeath2
						,CauseOfDeathAgreed
						,CauseOfDeathNotAgreedReason
						,AnyFurtherPointsOfDiscussion
						,SignificantQuestionsWhichRemainUnanswered
						,IssuesForDiscussion
						,OrganDonationDiscussed
						,TissueDonationDiscussed
						,OrgansDonated
						,TissuesDonated
						,SecondaryReviewReason
						,SecondaryReviewOutcome
						,SecondaryReviewActions
						,SecondaryReviewPatientCareConsultant
						,SecondaryReviewDiscussedConsultants
						,SecondaryReviewerDesignation
						,SecondaryReviewFuturePresentation
						,SecondaryReviewFuturePresentationPersonNominated
						,SecondaryReviewRequired
						,PersonNominatedForSecondReview
						,DiagDiseaseGroup
						,RecurringThemes
						,CodingCategory

						)
				) 
				Response 
			) 
			Paediatrics







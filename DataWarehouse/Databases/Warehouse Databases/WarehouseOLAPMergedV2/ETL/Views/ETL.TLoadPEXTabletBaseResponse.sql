﻿





CREATE view [ETL].[TLoadPEXTabletBaseResponse]

as

select
	ResponseRecno = [TabletResponseRecno]
	,[SurveyTakenID]
	,[SurveyID]
	,[QuestionID]
	,[AnswerID]
	,Answer
	,[SurveyDate]
	,[SurveyTime]
	,[LocationID]
	,WardID
	,[ChannelID]
	,[QuestionTypeID]
	,[LocationTypeID]
	,ContextCode = 'CMFT||CRT'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[PEX].[TabletResponse]








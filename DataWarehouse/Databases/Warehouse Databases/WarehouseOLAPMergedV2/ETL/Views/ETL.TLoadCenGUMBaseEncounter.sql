﻿



CREATE view [ETL].[TLoadCenGUMBaseEncounter]

as

select
	[EncounterRecno]
	,[AppointmentDate]
	,[AppointmentTime]
	,[PatientIdentifier]
	,[Postcode]
	,[ClinicID]
	,[AppointmentTypeID]
	,[AttendanceStatusID]
	,[InterfaceCode]
	,ContextCode = 'CEN||MCSH'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].GUM.Encounter


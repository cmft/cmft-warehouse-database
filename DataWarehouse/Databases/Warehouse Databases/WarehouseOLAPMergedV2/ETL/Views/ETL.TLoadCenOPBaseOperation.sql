﻿

create view [ETL].[TLoadCenOPBaseOperation]
as

select
	 Encounter.MergeEncounterRecno
	,Operation.SequenceNo
	,OperationCode = Operation.OperationCode
	,Operation.OperationDate
from
	[$(Warehouse)].OP.Operation Operation

inner join OP.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Operation.OPSourceUniqueID
and	Encounter.ContextCode = 'CEN||PAS'

union all
	
select
	 Encounter.MergeEncounterRecno
	,SequenceNo = 0
	,OperationCode = Encounter.PrimaryOperationCode
	,OperationDate = Encounter.PrimaryProcedureDate
from
	OP.BaseEncounter Encounter
where
	Encounter.PrimaryOperationCode is not null
and	Encounter.ContextCode = 'CEN||PAS'





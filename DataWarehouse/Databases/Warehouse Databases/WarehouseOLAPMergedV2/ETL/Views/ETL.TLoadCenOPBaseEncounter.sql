﻿


CREATE view [ETL].[TLoadCenOPBaseEncounter] as

/******************************************************************************************************
View		: [ETL].[TLoadCenOPBaseEncounter]
Description	: As view name

Modification History
====================

Date		Person			Description
=======================================================================================================
21/10/2014	Paul Egan       Qualified audit columns to remove ambiguity.
*******************************************************************************************************/

SELECT --top 1000
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceEncounterNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath

	--,SexCode =
	--	case
	--	when coalesce(Encounter.SexCode, '') = ''
	--	then null
	--	else Encounter.SexCode
	--	end

	--,SexCode =
	--	case Encounter.SexCode
	--	when '' then '0'
	--	when 'M' then '1'
	--	when 'F' then '2'
	--	when 'I' then '9'
	--	else '#'
	--	end -- commented out as mapping process will take care of these values
	,Encounter.SexCode
	,Encounter.NHSNumber
	,Encounter.NHSNumberStatusCode -- added DG 09/03/2012
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.SiteCode
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime
	,Encounter.ClinicCode
	,Encounter.AdminCategoryCode
	,Encounter.SourceOfReferralCode
	,Encounter.ReasonForReferralCode
	,Encounter.PriorityCode
	,Encounter.FirstAttendanceFlag
	,Encounter.DNACode
	,Encounter.AppointmentStatusCode
	,Encounter.CancelledByCode
	,Encounter.TransportRequiredFlag
	,Encounter.AttendanceOutcomeCode
	,Encounter.AppointmentTypeCode
	,Encounter.DisposalCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.ReferralConsultantCode
	,Encounter.ReferralSpecialtyCode
	,Encounter.BookingTypeCode
	,Encounter.CasenoteNo
	,Encounter.AppointmentCreateDate
	,Encounter.EpisodicGpCode
	,Encounter.EpisodicGpPracticeCode
	,Encounter.DoctorCode
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.PrimaryOperationCode
	,PrimaryProcedureDate = Encounter.PrimaryOperationDate
	,SecondaryProcedureCode1 = Encounter.SecondaryOperationCode1
	,SecondaryProcedureDate1 = Encounter.SecondaryOperationDate1
	,SecondaryProcedureCode2 = Encounter.SecondaryOperationCode2
	,SecondaryProcedureDate2 = Encounter.SecondaryOperationDate2
	,SecondaryProcedureCode3 = Encounter.SecondaryOperationCode3
	,SecondaryProcedureDate3 = Encounter.SecondaryOperationDate3
	,SecondaryProcedureCode4 = Encounter.SecondaryOperationCode4
	,SecondaryProcedureDate4 = Encounter.SecondaryOperationDate4
	,SecondaryProcedureCode5 = Encounter.SecondaryOperationCode5
	,SecondaryProcedureDate5 = Encounter.SecondaryOperationDate5
	,SecondaryProcedureCode6 = Encounter.SecondaryOperationCode6
	,SecondaryProcedureDate6 = Encounter.SecondaryOperationDate6
	,SecondaryProcedureCode7 = Encounter.SecondaryOperationCode7
	,SecondaryProcedureDate7 = Encounter.SecondaryOperationDate7
	,SecondaryProcedureCode8 = Encounter.SecondaryOperationCode8
	,SecondaryProcedureDate8 = Encounter.SecondaryOperationDate8
	,SecondaryProcedureCode9 = Encounter.SecondaryOperationCode9
	,SecondaryProcedureDate9 = Encounter.SecondaryOperationDate9
	,SecondaryProcedureCode10 = Encounter.SecondaryOperationCode10
	,SecondaryProcedureDate10 = Encounter.SecondaryOperationDate10
	,SecondaryProcedureCode11 = Encounter.SecondaryOperationCode11
	,SecondaryProcedureDate11 = Encounter.SecondaryOperationDate11
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.ContractSerialNo
	,Encounter.ReferralDate
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode
	,Encounter.AppointmentCategoryCode
	,Encounter.AppointmentCreatedBy
	,Encounter.AppointmentCancelDate
	,Encounter.LastRevisedDate
	,Encounter.LastRevisedBy
	,Encounter.OverseasStatusFlag
	,Encounter.PatientChoiceCode
	,Encounter.ScheduledCancelReasonCode
	,Encounter.PatientCancelReason
	,Encounter.DischargeDate
	,Encounter.QM08StartWaitDate
	,Encounter.QM08EndWaitDate
	,Encounter.DestinationSiteCode
	,Encounter.EBookingReferenceNo
	,Encounter.InterfaceCode
	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.LocalAdminCategoryCode
	,Encounter.PCTCode
	,Encounter.LocalityCode

	,AgeCode =
		case
		when datediff(day, DateOfBirth, Encounter.AppointmentDate) is null then 'Age Unknown'
		when datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 0 then '00 Days'
		when datediff(day, DateOfBirth, Encounter.AppointmentDate) = 1 then '01 Day'
		when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 1 and
		datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.AppointmentDate)), 2) + ' Days'
		
		when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 28 and
		datediff(month, DateOfBirth, Encounter.AppointmentDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end > 1 and
		 datediff(month, DateOfBirth, Encounter.AppointmentDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.AppointmentDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
			) then 1 else 0 end
		)), 2) + ' Years'

		end
		
	--,HRG4Encounter.HRGCode
	,HRGCode = null

	,Cases = 1

	,LengthOfWait =
		datediff(
			 day
			,Encounter.ReferralDate
			,Encounter.AppointmentDate
		)

	,Encounter.IsWardAttender
	,RTTActivity = null
	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when
			(
				--RTTOPClockStop.ClockStopDate = Encounter.AppointmentDate
			--or	Encounter.RTTEndDate = Encounter.AppointmentDate
				Encounter.RTTEndDate = Encounter.AppointmentDate
			)
		and Encounter.RTTBreachDate < Encounter.AppointmentDate
		then 'B'
		else 'N'
		end

	,Encounter.ClockStartDate
	,RTTBreachDate
	,RTTTreated = null
	,Encounter.DirectorateCode
	,Encounter.ReferralSpecialtyTypeCode
	,LastDNAorPatientCancelledDate
	,ReferredByCode
	,ReferrerCode
	,AppointmentOutcomeCode
	,MedicalStaffTypeCode
	,ContextCode
	,TreatmentFunctionCode = ReferralSpecialtyCode
	,Encounter.CommissioningSerialNo
	,Encounter.DerivedFirstAttendanceFlag
	,Encounter.AttendanceIdentifier
	,Encounter.RegisteredGpAtAppointmentCode
	,RegisteredGpPracticeAtAppointmentCode

	,ReferredByConsultantCode =
		case
		when Encounter.ReferredByCode in ('CON','CNN')
		then Encounter.ReferrerCode
		else null
		end

	,ReferredByGpCode =
		case
		when Encounter.ReferredByCode in ('GP','GPN')
		then Encounter.ReferrerCode
		else null
		end

	,ReferredByGdpCode =
		case
		when Encounter.ReferredByCode = 'GDP'
		then Encounter.ReferrerCode
		else null
		end
	,PseudoPostcode
	,Encounter.CCGCode 
	,EpisodicPostcode 
	,GpCodeAtAppointment 
	,GpPracticeCodeAtAppointment
	,PostcodeAtAppointment
	,EpisodicSiteCode

	,AppointmentResidenceCCGCode = PostcodeAtAppointment.CCGCode
from
	(
	select
		 EncounterRecno
		,SourceUniqueID
		,Encounter.SourcePatientNo
		,SourceEncounterNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,Encounter.NHSNumber
		,NHSNumberStatusCode
		,Encounter.DistrictNo
		,Encounter.Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,Encounter.EthnicOriginCode
		,Encounter.MaritalStatusCode
		,Encounter.ReligionCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode
		,ReferralConsultantCode
		,ReferralSpecialtyCode
		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryOperationDate
		,SecondaryOperationCode1
		,SecondaryOperationDate1
		,SecondaryOperationCode2
		,SecondaryOperationDate2
		,SecondaryOperationCode3
		,SecondaryOperationDate3
		,SecondaryOperationCode4
		,SecondaryOperationDate4
		,SecondaryOperationCode5
		,SecondaryOperationDate5
		,SecondaryOperationCode6
		,SecondaryOperationDate6
		,SecondaryOperationCode7
		,SecondaryOperationDate7
		,SecondaryOperationCode8
		,SecondaryOperationDate8
		,SecondaryOperationCode9
		,SecondaryOperationDate9
		,SecondaryOperationCode10
		,SecondaryOperationDate10
		,SecondaryOperationCode11
		,SecondaryOperationDate11
		,PurchaserCode
		,ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,Created
		,Updated
		,ByWhom
		,LocalAdminCategoryCode
		,PCTCode
		,LocalityCode
		,IsWardAttender
		,ClockStartDate
		,RTTBreachDate
		,HomePhone
		,WorkPhone
		,SourceOfReferralGroupCode
		,WardCode
		,ReferredByCode

		,ReferrerCode = 
			CASE
			WHEN ReferredByCode = 'GP' THEN ReferringGP.NationalCode
			WHEN ReferredByCode = 'GDP' THEN ReferringGdp.NationalCode
			ELSE ReferrerCode
			END

		,LastDNAorPatientCancelledDate
		,DirectorateCode
		,Encounter.StaffGroupCode
		,ReferralSpecialtyTypeCode
		,AppointmentOutcomeCode
		,MedicalStaffTypeCode
		,CommissioningSerialNo
		,DerivedFirstAttendanceFlag
		,AttendanceIdentifier 		
		,RegisteredGpAtAppointmentCode
		,RegisteredGpPracticeAtAppointmentCode
		,ContextCode
		,PseudoPostcode
		,CCGCode 
		,EpisodicPostcode 
		,GpCodeAtAppointment 
		,GpPracticeCodeAtAppointment
		,PostcodeAtAppointment
		,EpisodicSiteCode
	FROM
		(
		SELECT 
			 Encounter.EncounterRecno
			,Encounter.SourceUniqueID
			,Encounter.SourcePatientNo
			,SourceEncounterNo

			,PatientTitle = 
				coalesce(
					 Patient.Title
					,Encounter.PatientTitle
				)

			,PatientForename = 
				coalesce(
					 Patient.Forenames
					,Encounter.PatientForename
				)

			,PatientSurname = 
				coalesce(
					 Patient.Surname
					,Encounter.PatientSurname
				)

			,DateOfBirth = 
				coalesce(
					 Patient.DateOfBirth
					,Encounter.DateOfBirth
				)

		,DateOfDeath =  
			coalesce(
				 Patient.DateOfDeath
				,Encounter.DateOfDeath
			)

			,SexCode = 
				coalesce(
					 Patient.SexCode
					,Encounter.SexCode
				)

			,Encounter.NHSNumber

			,NHSNumberStatusCode =
				coalesce(
					 Patient.NHSNumberStatusId
					,case
					when 
						(
							Encounter.NHSNumber = '1234567899'  
						or	Encounter.NHSNumber is null
						or	rtrim(Encounter.NHSNumber) = ''
						or  Encounter.NHSNumber = 'REMOVE'
						or  left(Encounter.NHSNumber , 4) = 'SLF?'
						or  left(Encounter.NHSNumber , 4) = 'MAN?' 
						) then 'RT'
					when Encounter.NHSNumber is not null then 'NP'
					--else coalesce( -- DG Commented out  as NHSNumberStatusId not in OP.Encounter
					--		 Encounter.NHSNumberStatusId
					--		,'RT'
					--		)
					end
					,'RT'
				)

			,Encounter.DistrictNo
			,Postcode = 
					coalesce(
							Postcode.Postcode
							,Encounter.Postcode
							)
			,PatientAddress1 = 
					coalesce(
					PatientAddressAtAppointmentDate.AddressLine1
					,Encounter.PatientAddress1
							)
			,PatientAddress2 = 
					coalesce(
					PatientAddressAtAppointmentDate.AddressLine2
					,Encounter.PatientAddress2
							)
			,PatientAddress3 = 
					coalesce(
					PatientAddressAtAppointmentDate.AddressLine3
					,Encounter.PatientAddress3
							)
			,PatientAddress4 = 
					coalesce(
					PatientAddressAtAppointmentDate.AddressLine4
					,Encounter.PatientAddress4
							)
			,DHACode
			,Encounter.EthnicOriginCode
			,Encounter.MaritalStatusCode
			,Encounter.ReligionCode
			,RegisteredGpCode = RegisteredGP.NationalCode
			,RegisteredGpPracticeCode

			,SiteCode = 
				COALESCE(
					 DestinationSiteCode
					,SiteCode
				)

			,AppointmentDate
			,AppointmentTime
			,ClinicCode
			,AdminCategoryCode
			,SourceOfReferralCode
			,ReasonForReferralCode
			,PriorityCode
			,FirstAttendanceFlag
			,DNACode
			,AppointmentStatusCode
			,CancelledByCode
			,TransportRequiredFlag
			,AttendanceOutcomeCode = 
				DisposalCode
			,AppointmentTypeCode
			,DisposalCode
			,ConsultantCode
			,SpecialtyCode

	-- this change needs to be pushed back upstream once users start using WarehouseOLAPMerged / WarehouseReportingMerged
			--,ReferringConsultantCode = 
			,ReferralConsultantCode = 
				CASE 
				WHEN AppointmentTypeCode = 'WA' THEN ConsultantCode
				ELSE ReferringConsultantCode
				END

			--,ReferringSpecialtyCode = 
			,ReferralSpecialtyCode = 
				CASE 
				WHEN AppointmentTypeCode = 'WA' THEN SpecialtyCode
				ELSE ReferringSpecialtyCode
				END

			,BookingTypeCode
			,CasenoteNo
			,AppointmentCreateDate
			,EpisodicGpCode = EpisodicGP.NationalCode
			,EpisodicGpPracticeCode
			,DoctorCode
			,PrimaryDiagnosisCode
			,SubsidiaryDiagnosisCode
			,SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			,PrimaryOperationCode
			,PrimaryOperationDate
			,SecondaryOperationCode1
			,SecondaryOperationDate1
			,SecondaryOperationCode2
			,SecondaryOperationDate2
			,SecondaryOperationCode3
			,SecondaryOperationDate3
			,SecondaryOperationCode4
			,SecondaryOperationDate4
			,SecondaryOperationCode5
			,SecondaryOperationDate5
			,SecondaryOperationCode6
			,SecondaryOperationDate6
			,SecondaryOperationCode7
			,SecondaryOperationDate7
			,SecondaryOperationCode8
			,SecondaryOperationDate8
			,SecondaryOperationCode9
			,SecondaryOperationDate9
			,SecondaryOperationCode10
			,SecondaryOperationDate10
			,SecondaryOperationCode11
			,SecondaryOperationDate11

			,PurchaserCode = 
				CASE 
				WHEN AdminCategoryCode = '02' THEN 'VPP00'
				ELSE
					rtrim(
						coalesce(	
							 PCT.OrganisationCode
							,Practice.ParentOrganisationCode
							,Postcode.PCTCode
							,'5NT'
						) 
					)
				END

			,Encounter.ProviderCode
			,ContractSerialNo
			,ReferralDate
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode

			,RTTCurrentProviderCode = 
				Provider.NationalCode

			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,RTTPeriodStatusCode
			,AppointmentCategoryCode
			,AppointmentCreatedBy
			,AppointmentCancelDate
			,LastRevisedDate
			,LastRevisedBy
			,OverseasStatusFlag
			,PatientChoiceCode
			,ScheduledCancelReasonCode
			,PatientCancelReason
			,DischargeDate
			,QM08StartWaitDate
			,QM08EndWaitDate
			,DestinationSiteCode
			,EBookingReferenceNo
			,InterfaceCode
			,Encounter.Created		-- Qualified with 'Encounter' to remove ambiguity. Paul Egan 21/10/2014
			,Encounter.Updated		-- Qualified with 'Encounter' to remove ambiguity. Paul Egan 21/10/2014
			,Encounter.ByWhom		-- Qualified with 'Encounter' to remove ambiguity. Paul Egan 21/10/2014
			,LocalAdminCategoryCode

			,PCTCode =	
					Encounter.PCTCode

			,LocalityCode
			,IsWardAttender
			,ClockStartDate
			,RTTBreachDate
			,Encounter.HomePhone
			,Encounter.WorkPhone
			,SourceOfReferralGroupCode
			,WardCode

			,ReferredByCode = 
				CASE
				WHEN ReferredByCode IN('GP', 'GPN')	  THEN 'GP' 
				WHEN ReferredByCode IN('GDP')		  THEN 'GDP'
				WHEN ReferredByCode IN('CNN', 'CON')  THEN 'CONS'
				ELSE 'OTH'
				END

			,ReferrerCode

			,LastDNAorPatientCancelledDate
			,DirectorateCode
			,Encounter.StaffGroupCode

			,ReferralSpecialtyTypeCode = ReferringSpecialtyTypeCode

			,AppointmentOutcomeCode = AttendanceOutcomeCode

			,MedicalStaffTypeCode = 
					StaffGroup.MedicalStaffTypeCode

			,CommissioningSerialNo =
				CASE
					WHEN AdminCategoryCode = '02' 
						THEN 'YPPPAY'
					WHEN ContractSerialNo IS NULL 
					
						THEN rtrim(
								coalesce(	
										 Practice.ParentOrganisationCode
										,Postcode.PCTCode
										,'5NT'
								) 
							 ) + '00A'
					ELSE 
						ContractSerialNo
				END

			,ContextCode = 'CEN||PAS'
			,RegisteredGP.NationalCode

			,DerivedFirstAttendanceFlag =	
				Encounter.DerivedFirstAttendanceFlag
				--case
				--	when exists --there has been a previous attendance
				--		(
				--		select
				--			1
				--		from
				--			[$(Warehouse)].OP.Encounter PreviousAttendance

				--		inner join [$(Warehouse)].dbo.EntityLookup Attend
				--		on	Attend.EntityTypeCode = 'ATTENDANCEOUTCOMEATTENDED'
				--		and	Attend.EntityCode = PreviousAttendance.AppointmentStatusCode

				--		where
				--			PreviousAttendance.SourcePatientNo = Encounter.SourcePatientNo
				--		and	PreviousAttendance.SourceEncounterNo = Encounter.SourceEncounterNo
				--		and	PreviousAttendance.AppointmentTime < Encounter.AppointmentTime
				--		)
				--	then '2'
				--	else '1'
				--	end

			,AttendanceIdentifier =
				CASE
				WHEN IsWardAttender = 1 THEN Encounter.SourceEncounterNo
				ELSE left(
						replace(
							CONVERT(
								 varchar
								,Encounter.AppointmentTime
								,108
							)
							,':'
							,''
						)
						,4
					 ) 
				END
				+
				Encounter.DoctorCode

			-- this needs checking
			,RegisteredGpAtAppointmentCode = 
				RegisteredGpAtAppointment.GpCode

			,RegisteredGpPracticeAtAppointmentCode = 
				RegisteredGpAtAppointment.GpPracticeCode	
			,PseudoPostcode

			,CCGCode 
			,EpisodicPostcode 
			,GpCodeAtAppointment 
			,GpPracticeCodeAtAppointment
			,Encounter.PostcodeAtAppointment
			,EpisodicSiteCode
	
		from
			[$(Warehouse)].OP.Encounter Encounter

		LEFT OUTER JOIN [$(Warehouse)].PAS.Gp RegisteredGP
		ON	RegisteredGP.GpCode = Encounter.RegisteredGpCode
			
		LEFT OUTER JOIN [$(Warehouse)].PAS.Gp EpisodicGP
		ON	EpisodicGP.GpCode = Encounter.EpisodicGpCode
			
		LEFT OUTER JOIN [$(Warehouse)].PAS.PatientGp RegisteredGpAtAppointment
		ON RegisteredGpAtAppointment.SourcePatientNo = Encounter.SourcePatientNo
		AND Encounter.AppointmentDate BETWEEN RegisteredGpAtAppointment.EffectiveFromDate AND RegisteredGpAtAppointment.EffectiveToDate

		left outer join [$(Organisation)].dbo.PCT PCT
		on left(Encounter.PurchaserCode, 3) = PCT.OrganisationCode

		LEFT OUTER JOIN [$(Warehouse)].PAS.Patient
		ON	Patient.SourcePatientNo = Encounter.SourcePatientNo

		LEFT OUTER JOIN [$(Warehouse)].PAS.StaffGroup
		ON StaffGroup.StaffGroupCode = Encounter.StaffGroupCode 
			
		LEFT OUTER JOIN [$(Warehouse)].PAS.Provider Provider
		ON Provider.ProviderCode = Encounter.RTTCurrentProviderCode
		
		left outer join [$(Warehouse)].PAS.PatientAddress PatientAddressAtAppointmentDate
		on	PatientAddressAtAppointmentDate.SourcePatientNo = Encounter.SourcePatientNo
		and Encounter.AppointmentDate between PatientAddressAtAppointmentDate.EffectiveFromDate and coalesce(PatientAddressAtAppointmentDate.EffectiveToDate, getdate())

		LEFT OUTER JOIN [$(Organisation)].dbo.Postcode Postcode -- DG 19/06/2012 - changed to post code at time of appointment
		on	Postcode.Postcode = 
				case
				when len(PatientAddressAtAppointmentDate.Postcode) = 8 then PatientAddressAtAppointmentDate.Postcode
				else left(PatientAddressAtAppointmentDate.Postcode, 3) + ' ' + right(PatientAddressAtAppointmentDate.Postcode, 4) 
				end

		--LEFT OUTER JOIN [$(Organisation)].dbo.Practice Practice
		--ON	Practice.OrganisationCode = Encounter.RegisteredGpPracticeCode
		
		left outer join [$(Organisation)].dbo.Practice Practice -- DG 19/06/2012 - changed to GP at time of appointment
		ON	Practice.OrganisationCode = RegisteredGpAtAppointment.GpPracticeCode

		inner join [$(Warehouse)].OP.ProcessList
		on	ProcessList.EncounterRecno = Encounter.EncounterRecno
		

		) Encounter

	LEFT OUTER JOIN [$(Warehouse)].PAS.Gp ReferringGP
	ON	ReferringGP.GpCode = Encounter.ReferrerCode
	AND Encounter.ReferredByCode = 'GP'

	LEFT OUTER JOIN [$(Warehouse)].PAS.Gdp ReferringGdp
	ON	ReferringGdp.GdpCode = Encounter.ReferrerCode
	AND Encounter.ReferredByCode = 'GDP'
	) Encounter

--left join [$(Warehouse)].OP.HRG4Encounter HRG4Encounter
--on	HRG4Encounter.EncounterRecno = Encounter.EncounterRecno

left join [$(Warehouse)].WH.RTTStatus RTTStatus
on	RTTStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

left join [$(Organisation)].dbo.CCGPostcode PostcodeAtAppointment
on	PostcodeAtAppointment.Postcode =
		case
		when len(Encounter.PostcodeAtAppointment) = 8 then Encounter.PostcodeAtAppointment
		else left(Encounter.PostcodeAtAppointment, 3) + ' ' + right(Encounter.PostcodeAtAppointment, 4) 
		end

where
	Encounter.AppointmentDate >= '1 Apr 2009'


﻿


create view [ETL].[TLoadTraAEBaseTreatment]
as

select
	 Encounter.MergeEncounterRecno
	,Treatment.SequenceNo
	,TreatmentSchemeCode = Treatment.ProcedureSchemeInUse
	,TreatmentCode = Treatment.ProcedureCode
	,TreatmentTime = Treatment.ProcedureDate
	,Encounter.ContextCode
from
	[$(TraffordWarehouse)].dbo.AEProcedure Treatment

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Treatment.AESourceUniqueID
and	Encounter.ContextCode in
	(
	 'TRA||SYM'
	)




﻿




CREATE view [ETL].[TLoadTraAEBaseAssault]
as

select
	 Encounter.MergeEncounterRecno
	,Assault.AssaultDate
    ,Assault.AssaultTime
    ,Assault.AssaultWeapon
    ,Assault.AssaultWeaponDetails
    ,Assault.AssaultLocation
    ,Assault.AssaultLocationDetails
    ,Assault.AlcoholConsumed3Hour
    ,Assault.AssaultRelationship
    ,Assault.AssaultRelationshipDetails


	,Encounter.ContextCode

	,Encounter.SourceUniqueID
from
	[$(TraffordWarehouse)].dbo.AEAssault Assault

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Assault.AESourceUniqueID
and	Encounter.ContextCode in
	(
	'TRA||SYM'
	
	)

where
	not exists
	(
	select
		1
	from
		[$(TraffordWarehouse)].dbo.AEAssault Previous
	where
		Previous.AESourceUniqueID = Assault.AESourceUniqueID
	and	Previous.AssaultRecno > Assault.AssaultRecno
	)


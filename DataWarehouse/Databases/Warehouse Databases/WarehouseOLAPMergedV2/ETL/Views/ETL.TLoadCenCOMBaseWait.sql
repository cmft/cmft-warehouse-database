﻿


CREATE view [ETL].[TLoadCenCOMBaseWait] as

select
	 EncounterRecno
	,SourceUniqueID
	,SourceEncounterNo
	,CensusDate
	,ReferredBySpecialtyID
	,ReferredToSpecialtyID
	,SourceofReferralID

	,ReferralReceivedTime
	
	,ReferralReceivedDate = 
		convert(date,ReferralReceivedTime)

	,ReasonforReferralID
	,CancelledTime
	,CancelledReasonID
	,ReferralSentTime
	,UrgencyID
	,ReferralPriorityID
	,AuthoriedTime
	,ReferralClosedTime
	,ClosureReasonID
	,ReferredByProfessionalCarerID
	,ReferredByStaffTeamID
	,ReferredByHealthOrgID
	,ReferredToProfessionalCarerID
	,ReferredToHealthOrgID
	,ReferredToStaffTeamID
	,OutcomeOfReferralID
	,ReferralStatusID
	,RejectionID
	,TypeofReferralID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientLocalIdentifier
	,PatientPASIdentifier
	,PatientIdentifier
	,PatientFacilityIdentifier
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,PatientSpokenLanguageID
	,PatientReligionID
	,PatientMaritalStatusID
	,ProfessionalCarerCurrentID
	,ProfessionalCarerHealthOrgCurrentID
	,ProfessionalCarerParentHealthOrgCurrentID
	,ProfessionalCarerScheduleID
	,ProfessionalCarerHealthOrgScheduleID
	,ProfessionalCarerParentHealthOrgScheduleID
	,ProfessionalCarerReferralID
	,ProfessionalCarerHealthOrgReferralID
	,ProfessionalCarerParentHealthOrgReferralID
	,RTTStatusDate
	,RTTPatientPathwayID
	,RTTStartTime
	,RTTStartFlag
	,RTTStatusID
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,ParentID
	,HealthOrgOwner
	,RequestedServiceID
	,ScheduleID
	,ScheduleSpecialtyID
	,ScheduleStaffTeamID
	,ScheduleProfessionalCarerID
	,ScheduleSeenByProfessionalCarerID

	,ScheduleStartTime
	,ScheduleStartDate = 
		coalesce(convert(date,ScheduleStartTime), '1 jan 1900')

	,ScheduleEndTime

	,ScheduleEndDate = 
		convert(date,ScheduleEndTime)

	,ScheduleArrivedTime

	,ScheduleArrivedDate = 
		convert(date,ScheduleArrivedTime)

	,ScheduleSeenTime

	,ScheduleSeenDate = 
		convert(date,ScheduleSeenTime)

	,ScheduleDepartedTime

	,ScheduleDepartedDate = 
		convert(date,ScheduleDepartedTime)

	,ScheduleAttendedID
	,ScheduleOutcomeID
	,ScheduleVisitID
	,ScheduleContactTypeID
	,ScheduleTypeID
	,ScheduleReasonID
	,ScheduleCanceledReasonID
	,ScheduleCanceledTime

	,ScheduleCanceledDate = 
		convert(date,ScheduleCanceledTime)

	,ScheduleCancelledByID
	,ScheduleMoveReasonID
	,ScheduleMovetime
	,ScheduleServicePointID
	,ScheduleServicePointSessionID
	,ScheduleMoveCount
	,ScheduleLocationTypeID
	,ScheduleLocationDescription =
		case 
			when ScheduleLocationDescription = ' ' then null
		else ScheduleLocationDescription
		end
	,ScheduleLocationTypeHealthOrgID
	,ScheduleWaitingListID
	,KornerWait
	,BreachDate
	,NationalBreachDate
	,BreachDays
	,NationalBreachDays
	,RTTBreachDate
	,ClockStartDate
	,ScheduleOutcomeCode
	,PatientDeathIndicator
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,LastAppointmentFlag
	,AdditionFlag
	,FuturePatientCancelDate
	,PCTCode
	,StartWaitDate
	,EndWaitDate
	,BookedTime
	,DerivedFirstAttendanceFlag
	,DerivedWaitFlag
	,WeeksWaiting
		
	,AgeCode =
		case
		when datediff(day, PatientDateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
		when datediff(day, PatientDateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
		when datediff(day, PatientDateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
		when datediff(day, PatientDateOfBirth, Encounter.CensusDate) > 1 and
		datediff(day, PatientDateOfBirth, Encounter.CensusDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, Encounter.CensusDate)), 2) + ' Days'
	
		when datediff(day, PatientDateOfBirth, Encounter.CensusDate) > 28 and
		datediff(month, PatientDateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, PatientDateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, PatientDateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
		 datediff(month, PatientDateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,Encounter.CensusDate) -
		case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, PatientDateOfBirth, Encounter.CensusDate) - 
		(
		case 
		when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, PatientDateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, Encounter.CensusDate) - 
		(
		case 
		when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, PatientDateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		)), 2) + ' Years'

		end

	,convert(int, Encounter.KornerWait) LengthOfWait


	,WithAppointment =
		case
		when Encounter.ScheduleStartTime is null then 0
		when convert(Date,Encounter.ScheduleStartTime) < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900') then 0
		when convert(Date,Encounter.ScheduleStartTime) < CensusDate then 0
		else 1
		end 

	,case when Encounter.BreachDate < convert(Date,Encounter.ScheduleStartTime) then 1 else 0 end BookedBeyondBreach

	,case when datediff(month, Encounter.ScheduleStartTime, Encounter.BreachDate) < 2 then 1 else 0 end NearBreach

	,DurationCode =
		coalesce(
			right('00' +
			convert(varchar, 
			case
			when convert(int, Encounter.KornerWait /7 ) > 207 then 208
			else convert(int, Encounter.KornerWait /7 )
			end), 3)
			,'NA'
		)

	,RTTActivity = null

	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when Encounter.ScheduleStartTime is null
		then 'U'
		when Encounter.RTTBreachDate < convert(date,Encounter.ScheduleStartTime)
		then 'B'
		else 'N'
		end


	,DurationAtAppointmentDateCode =
		case
		when Encounter.ScheduleStartTime is null then 'NA'
		when convert(date,Encounter.ScheduleStartTime) < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900') then 'NA'
		when convert(date,Encounter.ScheduleStartTime) < CensusDate then 'NA'
		else right('00' +
				convert(varchar, 
					case
					when convert(int, (Encounter.KornerWait + datediff(day , CensusDate , ScheduleStartTime)) /7 ) > 207 then 208
					else convert(int, (Encounter.KornerWait + datediff(day , CensusDate , ScheduleStartTime)) /7 )
					end
				)
				,3
			)
		end

	,WithRTTOpenPathway = 0

	,Created
	,Updated
	,ByWhom

	,ContextCode = 'CEN||IPM'

	--,ModifiedRow =
	--	case
	--	when
	--		Encounter.Updated >
	--		(
	--		select
	--			DatasetProcess.DatasetProcessTime
	--		from
	--			ETL.DatasetProcess
	--		where
	--			DatasetProcess.DatasetCode = 'COMWL'
	--		and	DatasetProcess.ProcessCode = 'IMPORT'
	--		)
	--	then 1
	--	else 0
	--	end

from
	[$(Warehouse)].COM.WaitPTL Encounter




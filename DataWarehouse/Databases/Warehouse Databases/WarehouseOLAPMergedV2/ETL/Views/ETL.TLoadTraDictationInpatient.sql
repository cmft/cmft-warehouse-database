﻿


CREATE view [ETL].[TLoadTraDictationInpatient]

as

select
	SourcePatientNo
	,AdmissionTime 
	,RequestTypeCode 
	,DocumentID
	,Inpatient.InterfaceCode
	,ContextCode
from
	(
	select
		SourcePatientNo = EPMI
		,AdmissionTime = AdmissionDate
		,RequestTypeCode = '04' -- no codes in EPR so we just use the code from medisec that denotes discharge notification
		,DocumentID = cast(DocumentID as varchar(100))
		,InterfaceCode = 'EPR'
		,ContextCode = 'TRA||EPR'
	from
		[$(TraffordWarehouse)].ETL.TimportEPRIDS
	where
		not exists
			(
			select
				1
			from
				[$(TraffordWarehouse)].ETL.TimportEPRIDS LaterTimportEPRIDS
			where
				LaterTimportEPRIDS.DocumentID = TimportEPRIDS.DocumentID
			and	LaterTimportEPRIDS.DischargeDocumentID > TimportEPRIDS.DischargeDocumentID
			)
	) Inpatient






















﻿--Use WarehouseOLAPMergedV2

CREATE View [ETL].[TLoadBloodManagementBaseTranexamicAcid]

as

select 
	TranexamicAcidRecno
	,TranexamicAcidPrimaryKey
	,EpisodeKey
	,HospitalNumber
	,LastName
	,FirstName
	,DateOfBirth
	,Gender 
	,SpecialtyKey
	,ConsultantKey
	,IntendedDestination
	,ActualDestination
	,DrugKey
	,DrugDescription
	,Category
	,Dose
	,UnitsKey
	,UnitsDescription
	,DrugAdministeredDate
	,DrugAdministeredTime
	,SessionTypeDescription 
	,SessionLocationKey 
	,InterfaceCode 
	,ContextCode = 'CMFT||RECALL' -- There is data coming through with location Trafford Theatre 5 and 6, small numbers.
	,Created
	,Updated
	,ByWhom
from 
	[$(Warehouse)].BloodManagement.TranexamicAcid
	

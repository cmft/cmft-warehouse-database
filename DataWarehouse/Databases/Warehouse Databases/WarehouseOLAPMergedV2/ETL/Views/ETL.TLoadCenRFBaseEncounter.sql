﻿



CREATE view [ETL].[TLoadCenRFBaseEncounter] as

select 
	 RFEncounter.EncounterRecno
	,RFEncounter.SourceUniqueID
	,RFEncounter.SourcePatientNo
	,RFEncounter.SourceEncounterNo
	,RFEncounter.PatientTitle
	,RFEncounter.PatientForename
	,RFEncounter.PatientSurname
	,RFEncounter.DateOfBirth
	,RFEncounter.DateOfDeath
	,RFEncounter.SexCode
	,RFEncounter.NHSNumber
	,RFEncounter.DistrictNo
	,RFEncounter.Postcode
	,RFEncounter.PatientAddress1
	,RFEncounter.PatientAddress2
	,RFEncounter.PatientAddress3
	,RFEncounter.PatientAddress4
	,RFEncounter.DHACode
	,RFEncounter.EthnicOriginCode
	,RFEncounter.MaritalStatusCode
	,RFEncounter.ReligionCode
	,RegisteredGpCode =
		coalesce(RFEncounter.RegisteredGpCode,	'G9999998')
	,RegisteredGpPracticeCode =
		coalesce(RFEncounter.RegisteredGpPracticeCode, 'V81999')
	,EpisodicGpCode =
		coalesce(RFEncounter.EpisodicGpCode,	'G9999998')
	,EpisodicGpPracticeCode =
		coalesce(RFEncounter.EpisodicGpPracticeCode, 'V81999')
	,RFEncounter.EpisodicGdpCode
	,RFEncounter.SiteCode
	,RFEncounter.ConsultantCode
	,RFEncounter.SpecialtyCode
	,RFEncounter.SourceOfReferralCode
	,RFEncounter.PriorityCode
	,RFEncounter.ReferralDate
	,RFEncounter.DischargeDate
	,RFEncounter.DischargeTime
	,RFEncounter.DischargeReasonCode
	,RFEncounter.DischargeReason
	,RFEncounter.AdminCategoryCode
	,RFEncounter.ContractSerialNo
	,RFEncounter.RTTPathwayID
	,RFEncounter.RTTPathwayCondition
	,RFEncounter.RTTStartDate
	,RFEncounter.RTTEndDate
	,RFEncounter.RTTSpecialtyCode
	,RFEncounter.RTTCurrentProviderCode
	,RFEncounter.RTTCurrentStatusCode
	,RFEncounter.RTTCurrentStatusDate
	,RFEncounter.RTTCurrentPrivatePatientFlag
	,RFEncounter.RTTOverseasStatusFlag
	,RFEncounter.NextFutureAppointmentDate
	,RFEncounter.ReferralComment
	,RFEncounter.InterfaceCode
	,RFEncounter.Created
	,RFEncounter.Updated
	,RFEncounter.ByWhom
	,RFEncounter.ReasonForReferralCode
	,RFEncounter.SourceOfReferralGroupCode
	,RFEncounter.DirectorateCode
	,RFEncounter.CasenoteNo
	,RFEncounter.PCTCode
	,AgeCode =
		case
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) is null then 'Age Unknown'
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) <= 0 then '00 Days'
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) = 1 then '01 Day'
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) > 1 and
		datediff(day, DateOfBirth, RFEncounter.ReferralDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, DateOfBirth, RFEncounter.ReferralDate)), 2) + ' Days'
		
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) > 28 and
		datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end > 1 and
		 datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, DateOfBirth,RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, DateOfBirth, RFEncounter.ReferralDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, RFEncounter.ReferralDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, RFEncounter.ReferralDate) 
			And	datepart(d, DateOfBirth) > datepart(d, RFEncounter.ReferralDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, DateOfBirth, RFEncounter.ReferralDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, RFEncounter.ReferralDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, RFEncounter.ReferralDate) 
			And	datepart(d, DateOfBirth) > datepart(d, RFEncounter.ReferralDate)
			) then 1 else 0 end
		)), 2) + ' Years'

		end

	,RFEncounter.ContextCode

	,RFEncounter.ResidenceCCGCode
	,RFEncounter.CCGCode
from
	(
	SELECT
		 Encounter.EncounterRecno
		,Encounter.SourceUniqueID
		,Encounter.SourcePatientNo
		,Encounter.SourceEncounterNo
		,PatientTitle = 
				coalesce(
					 Patient.Title
					,Encounter.PatientTitle
				)
		,PatientForename = 
				coalesce(
					 Patient.Forenames
					,Encounter.PatientForename
				)
		,PatientSurname = 
				coalesce(
					 Patient.Surname
					,Encounter.PatientSurname
				)
		,DateOfBirth = 
				coalesce(
					 Patient.DateOfBirth
					,Encounter.DateOfBirth
				)

		,DateOfDeath =  
			coalesce(
				 Patient.DateOfDeath
				,Encounter.DateOfDeath
			)

		,SexCode = 
				coalesce(
					 Patient.SexCode
					,Encounter.SexCode
				)
		,Encounter.NHSNumber
		,NHSNumberStatusCode =
				coalesce(
					 Patient.NHSNumberStatusId
					,case
					when 
						(
							Encounter.NHSNumber = '1234567899'  
						or	Encounter.NHSNumber is null
						or	rtrim(Encounter.NHSNumber) = ''
						or  Encounter.NHSNumber = 'REMOVE'
						or  left(Encounter.NHSNumber , 4) = 'SLF?'
						or  left(Encounter.NHSNumber , 4) = 'MAN?' 
						) then 'RT'
					when Encounter.NHSNumber is not null then 'NP'
					end
					,'RT'
				)
		,Encounter.DistrictNo
		,Encounter.Postcode
		,Encounter.PatientAddress1
		,Encounter.PatientAddress2
		,Encounter.PatientAddress3
		,Encounter.PatientAddress4
		,Encounter.DHACode
		,Encounter.EthnicOriginCode
		,Encounter.MaritalStatusCode
		,Encounter.ReligionCode
		,RegisteredGpCode = 
			RegisteredGP.NationalCode
		,Encounter.RegisteredGpPracticeCode
		,EpisodicGpCode = 
			EpisodicGP.NationalCode
		,Encounter.EpisodicGpPracticeCode
		,Encounter.EpisodicGdpCode
		,Encounter.SiteCode
		,Encounter.ConsultantCode
		,Encounter.SpecialtyCode
		,Encounter.SourceOfReferralCode
		,Encounter.PriorityCode
		,Encounter.ReferralDate
		,Encounter.DischargeDate
		,Encounter.DischargeTime
		,Encounter.DischargeReasonCode
		,Encounter.DischargeReason
		,Encounter.AdminCategoryCode
		,Encounter.ContractSerialNo
		,Encounter.RTTPathwayID
		,Encounter.RTTPathwayCondition
		,Encounter.RTTStartDate
		,Encounter.RTTEndDate
		,Encounter.RTTSpecialtyCode
		,RTTCurrentProviderCode = 
				Provider.NationalCode
		,Encounter.RTTCurrentStatusCode
		,Encounter.RTTCurrentStatusDate
		,Encounter.RTTCurrentPrivatePatientFlag
		,Encounter.RTTOverseasStatusFlag
		,Encounter.NextFutureAppointmentDate
		,Encounter.ReferralComment
		,Encounter.InterfaceCode
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
		,Encounter.ReasonForReferralCode
		,Encounter.SourceOfReferralGroupCode
		,Encounter.DirectorateCode
		,Encounter.CasenoteNo
		,Encounter.PCTCode
		,ContextCode = 'CEN||PAS'
		,RegisteredGpAtReferralCode = 
			RegisteredGpAtReferral.GpCode
		,RegisteredGpPracticeAtReferralCode = 
			RegisteredGpAtReferral.GpPracticeCode

		,ResidenceCCGCode = EpisodicPostcode.CCGCode

		,CCGCode =
			coalesce(
				 Practice.ParentOrganisationCode
				,EpisodicPostcode.CCGCode
				,'00W'
			)

	FROM
		[$(Warehouse)].RF.Encounter Encounter
		
	LEFT OUTER JOIN [$(Warehouse)].PAS.Gp RegisteredGP
	ON	RegisteredGP.GpCode = Encounter.RegisteredGpCode
		
	LEFT OUTER JOIN [$(Warehouse)].PAS.Gp EpisodicGP
	ON	EpisodicGP.GpCode = Encounter.EpisodicGpCode
		
	LEFT OUTER JOIN [$(Warehouse)].PAS.PatientGp RegisteredGpAtReferral
	ON RegisteredGpAtReferral.SourcePatientNo = Encounter.SourcePatientNo
	AND Encounter.ReferralDate BETWEEN RegisteredGpAtReferral.EffectiveFromDate AND RegisteredGpAtReferral.EffectiveToDate

	LEFT OUTER JOIN [$(Warehouse)].PAS.Patient
	ON	Patient.SourcePatientNo = Encounter.SourcePatientNo

	LEFT OUTER JOIN [$(Warehouse)].PAS.Provider Provider
	ON Provider.ProviderCode = Encounter.RTTCurrentProviderCode

	left join [$(Organisation)].ODS.Practice
	on	Practice.OrganisationCode = RegisteredGpAtReferral.GpPracticeCode

	left join [$(Organisation)].dbo.CCGPostcode EpisodicPostcode on
			EpisodicPostcode.Postcode = 
				case
				when len(Encounter.Postcode) = 8 then Encounter.Postcode
				else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
				end

	) RFEncounter





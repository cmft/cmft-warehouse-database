﻿



CREATE view [ETL].[TLoadTraAPCBaseDiagnosis]
as

select
	 Encounter.MergeEncounterRecno
	,SequenceNo = cast(Diagnosis.SequenceNo as int) - 1
	,DiagnosisCode = rtrim(left(replace(Diagnosis.DiagnosisCode, '..', '.'),6))
from
	[$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis

inner join APC.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Diagnosis.IPSourceUniqueID
and	Encounter.ContextCode = 'TRA||UG'
and Encounter.Reportable = 1

where 
	--remove fces that exist in both Central and Trafford activity
	not exists
	(
	select
		1
	from
		APC.EncounterDuplicate

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	where
		EncounterDuplicate.DuplicateContextID = Context.ContextID
	and	EncounterDuplicate.DuplicateRecno = Encounter.EncounterRecno
	)







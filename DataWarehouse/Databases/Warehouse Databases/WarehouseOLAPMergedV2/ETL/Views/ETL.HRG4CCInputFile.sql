﻿


CREATE view [ETL].[HRG4CCInputFile]

as

/****************************************************************************************
	View : [ETL].[HRG4CCInputFile]
	Description		 : Used in SSIS package "HRG4 CC Grouper Merged.dtsx"

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
*****************************************************************************************/

select
	CCUF = UnitFunctionCode
	,BCSD = BasicCardiovascularSupportDays
	,ACSD = AdvancedCardiovascularSupportDays
	,BRSD = BasicRespiratorySupportDays
	,ARSD = AdvancedRespiratorySupportDays
	,RSD = RenalSupportDays
	,NSD = NeurologicalSupportDays
	,DSD = DermatologicalSupportDays
	,LSD = LiverSupportDays
	,CCL2D = CriticalCareLevel2Days
	,CCL3D = CriticalCareLevel3Days
	,CCSTARTDATE = StartDate
	,CCDISCHARGEDATE = EndDate
	,Encounter.MergeEncounterRecno
	,FinancialYear = 
				case
				when Discharge.FinancialYear = '2015/2016' -- using 14/15 grouper for 15/16 data - confirmed by Phil Huitson 7 Apr 2015
				then '2014/2015'
				else Discharge.FinancialYear 
				end
from
	APC.BaseCriticalCarePeriod Encounter

left outer join
	APC.BaseCriticalCarePeriodReference Reference
on	Encounter.MergeEncounterRecno = Reference.MergeEncounterRecno

left outer join
	WH.Calendar Discharge
on	Discharge.DateID = Reference.EndDateID

where
	Encounter.EndDate is not null --CH 31072015 discussion with AR that some patients without a discharge are coming through but need to not go into the grouper

--where
--	NHS Number like '%6148364158%'

-- only first CCP appears to be populated
--select
--	1
--from
--	CriticalCare.dbo.tbSys_Inpatient CDS Export
--where
--	CRITICAL CARE START DATE 4 is not null

union all

select --top 100
	CCUF = null
	,BCSD = null
	,ACSD = null
	,BRSD = null
	,ARSD = null
	,RSD = null
	,NSD = null
	,DSD = null
	,LSD = null
	,CCL2D = null
	,CCL3D = null
	,CCSTARTDATE = null
	,CCDISCHARGEDATE = null
	,MergeEncounterRecno  = -20132014 -- dummy record
	,FinancialYear = '2013/2014'






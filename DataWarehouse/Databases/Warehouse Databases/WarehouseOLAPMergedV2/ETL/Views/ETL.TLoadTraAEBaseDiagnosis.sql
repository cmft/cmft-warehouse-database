﻿

CREATE view [ETL].[TLoadTraAEBaseDiagnosis]
as

select
	 Encounter.MergeEncounterRecno
	,Diagnosis.SequenceNo
	,DiagnosticSchemeCode = Diagnosis.DiagnosticSchemeInUse
	,Diagnosis.DiagnosisCode
	--,Diagnosis.DiagnosisSiteCode
	--,Diagnosis.DiagnosisSideCode
	,DiagnosisSiteCode = null -- not CDS mapped in Trafford Symphony
	,DiagnosisSideCode = null -- not CDS mapped in Trafford Symphony
	,Encounter.ContextCode
from
	[$(TraffordWarehouse)].dbo.AEDiagnosis Diagnosis

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Diagnosis.AESourceUniqueID
and	Encounter.ContextCode in
	(
	 'TRA||SYM'
	)




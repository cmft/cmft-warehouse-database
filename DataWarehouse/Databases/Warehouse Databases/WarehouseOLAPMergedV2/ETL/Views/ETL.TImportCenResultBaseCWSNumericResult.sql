﻿






















CREATE view [ETL].[TImportCenResultBaseCWSNumericResult] as

select --top 1000
	ProcessList.ResultRecno
	,ProcessList.Action 
	,ContextCode = 'CEN||PAS'
	,SourceUniqueID
	,Import.SourcePatientNo
	,Patient.DistrictNo
	,Patient.NHSNumber
	,Patient.DateOfBirth
	,Patient.SexCode
	,SourceEpisodeNo
	,TransactionNo
	,SessionNo
	,OrderSourceUniqueID
	,Import.ConsultantCode
	,Consultant.MainSpecialtyCode
	,DisciplineCode = Service.ScreenTypeCode
	,SpecialtyCode
	,OrderPriorityCode
	,OrderStatusCode
	,OrderEnteredByCode
	,OrderedByCode
	,OrderTime
	,OrderComment
	,SampleReferenceCode
	,EffectiveTime
	,LocationCode = null
	,Import.ServiceCode
	,ResultCode
	,Result
	,UnitOfMeasurement
	,LowerReferenceRangeValue
	,UpperReferenceRangeValue
	,ResultTime
	,RangeIndicator
	,ResultEnteredByCode
	,ResultStatusCode
	,ResultComment
	,InterfaceCode
from
	[$(Warehouse)].Result.CWSNumericProcessList ProcessList

left join [$(Warehouse)].Result.CWSNumericResult Import
on	Import.ResultRecno = ProcessList.ResultRecno

left join [$(Warehouse)].PAS.Patient
on	Import.SourcePatientNo = Patient.SourcePatientNo

left join [$(Warehouse)].PAS.Consultant
on	Import.ConsultantCode = Consultant.ConsultantCode

left join [$(Warehouse)].PAS.Service
on	Service.ServiceCode = Import.ServiceCode



























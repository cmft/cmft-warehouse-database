﻿
create view [ETL].[TLoadCenAPCWLBaseWaitingListEntry]

as

select
	[SourceUniqueID]
	,[ActivityDate]
	,[ActivityTime]
	,[CasenoteNumber]
	,[ConsultantCode]
	,[DiagnosticGroupCode]
	,[DistrictNo]
	,[SourceEntityRecno]
	,[SourcePatientNo]
	,[IntendedPrimaryOperationCode]
	,[ManagementIntentionCode]
	,[SiteCode]
	,[AdmissionMethodCode]
	,[Operation]
	,[SpecialtyCode]
	,[WaitingListCode]
	,[ContractSerialNumber]
	,[PurchaserCode]
	,[EpisodicGpCode]
	,[InterfaceCode]
	,[Created]
	,[Updated]
	,[ByWhom]
	,ContextCode = 'CEN||PAS'
from
	[$(Warehouse)].[APC].[WaitingListEntry]

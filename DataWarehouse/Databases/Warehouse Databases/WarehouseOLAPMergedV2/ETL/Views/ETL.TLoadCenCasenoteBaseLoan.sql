﻿

create view [ETL].[TLoadCenCasenoteBaseLoan] as

select
	 SourceUniqueID
	,SourcePatientNo
	,CasenoteNumber
	,BorrowerCode
	,SequenceNo
	,TransactionTime
	,LoanTime
	,ExpectedReturnDate
	,ReturnTime
	,Comment
	,LoanReason
	,UserId
	,ContextCode = 'CEN||PAS'
from
	[$(Warehouse)].PAS.CasenoteLoan







﻿







CREATE view [ETL].[TLoadPEXBedUnitBaseResponse]

as

select
	  ResponseRecno = [BedUnitResponseRecno]
	  ,[SurveyTakenID]
      ,[SurveyDate]
      ,[SurveyTime]
      ,[SurveyID]
      ,[LocationID]
      ,[WardID]
      ,[QuestionID]
      ,[AnswerID]
      ,Answer
      ,[ResponseDate]
      ,[ResponseTime]
      ,[ChannelID]
      ,[QuestionTypeID]
      ,[LocationTypeID]
      ,ContextCode = 'CMFT||HOSP'
      ,[Created]
      ,[Updated]
      ,[ByWhom]
	
from
	[$(Warehouse)].[PEX].[BedUnitResponse]










﻿










CREATE view [ETL].[TLoadCenMaternityBaseAntenatalCare]

as

select
	AntenatalCareRecno
	,SourceUniqueID
	,CasenoteNumber
	,DistrictNo
	,NHSNumber
	,DateOfBirth
	,AgeAtBooking
	,GPPracticeCode
	,VisitDate
	,VisitTime
	,VisitTypeCode
	,FetalMedicineCases
	,Thrombophilia
	,SickleCellCode
	,ThalassemiaCode
	,Thromboembolism
	,AntiTNFTreatment
	,RHIsoimmunisationCode
	,RenalDisease
	,Renal
	,EndocrineCode
	,AntenatalEndocrineCode
	,MalignantDisease
	,HIV
	,HeartDiseaseCode
	,CardiacCode
	,NumberOfFetuses
	,DiabeticCode
	,DiabetesCode
	,RespiratoryProblemCode
	,EarlyPreTerm
	,Miscarriage
	,NeonatalDeath
	,SecondThirdTrimesterLoss
	,GrowthRestrictionCode
	,PreviousBirthWeight
	,PsychiatricProblemCode
	,HELLPEclampsia
	,HypertensionMedication
	,PreviousOperations
	,EpilepsyMedication
	,InheritedDiseases
	,HepatitisB
	,HepatitisC
	,BMIAtBooking
	,PhysicalDisability
	,Interpreter
	,PatientTypeCode
	,SpecialistMidwifeReferral
	,AccomodationTypeCode
	,DrugCode
	,SafeguardingChildrenCode
	,SafeguardingWomenCode
	,HRGCode
	,HypertensionCode
    ,HaematologicalDisorderCode
    ,MedicalHistoryComments
    ,AutoimmuneDiseaseCode
    ,EstimatedDeliveryDate
    ,FirstContactDate
  --  ,FirstContactCareProfessionalTypeCode
    ,FirstContactOther
    ,LastMenstrualPeriodDate
    ,FirstLanguageEnglishCode
    ,EmploymentStatusMotherCode
    ,EmploymentStatusPartnerCode
    ,SmokingStatusCode
    ,CigarettesPerDay
    ,AlcoholUnitsPerWeek
    ,WeightMother
    ,HeightMother
	,ModifiedTime
	,RubellaOfferDate 
	,RubellaOfferStatus 
	,RubellaBloodSampleDate 
	,RubellaResult 
	,HepatitisBOfferDate 
	,HepatitisBOfferStatus 
	,HepatitisBBloodSampleDate 
	,HepatitisBResult
	,HaemoglobinopathyOfferDate 
	,HaemoglobinopathyOfferStatus 
	,HaemoglobinopathyBloodSampleDate 
	,HaemoglobinopathyResult 
	,DownsSyndromeOfferDate 
	,DownsSyndromeOfferStatus 
	,DownsSyndromeBloodSampleDate 
	,DownsSyndromeInvestigationRiskRatio 

	,PreviousCaesareanSections
	,PreviousLiveBirths
	,PreviousStillBirths
	,PreviousLossesLessThan24Weeks
			  	   	
	,InterfaceCode
	,ContextCode = 'CEN||SMMIS'
	,Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].Maternity.AntenatalCare










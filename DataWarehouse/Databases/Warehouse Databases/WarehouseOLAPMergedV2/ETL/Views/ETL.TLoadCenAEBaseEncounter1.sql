﻿











CREATE view [ETL].[TLoadCenAEBaseEncounter1] as

select
	 AEEncounter.EncounterRecno
	,AEEncounter.SourceUniqueID
	,AEEncounter.UniqueBookingReferenceNo
	,AEEncounter.PathwayId
	,AEEncounter.PathwayIdIssuerCode
	,AEEncounter.RTTStatusCode
	,AEEncounter.RTTStartDate
	,AEEncounter.RTTEndDate
	,AEEncounter.DistrictNo
	,AEEncounter.TrustNo
	,AEEncounter.CasenoteNo
	,AEEncounter.DistrictNoOrganisationCode
	,AEEncounter.NHSNumber
	,NHSNumberStatusCode = AEEncounter.NHSNumberStatusId
	,AEEncounter.PatientTitle
	,AEEncounter.PatientForename
	,AEEncounter.PatientSurname
	,AEEncounter.PatientAddress1
	,AEEncounter.PatientAddress2
	,AEEncounter.PatientAddress3
	,AEEncounter.PatientAddress4
	,AEEncounter.Postcode
	,AEEncounter.DateOfBirth
	,AEEncounter.DateOfDeath
	,AEEncounter.SexCode
	,AEEncounter.CarerSupportIndicator
	,RegisteredGpCode = 
		EpisodicGpCode

	,RegisteredPracticeCode =
		EpisodicGpPracticeCode

	,AEEncounter.AttendanceNumber
	,AEEncounter.ArrivalModeCode
	,AEEncounter.AttendanceCategoryCode

	,AttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
		--coalesce(
		--	AEEncounter.AttendanceDisposalCode
		--	,'-1'
		--)

	,AEEncounter.IncidentLocationTypeCode
	,AEEncounter.PatientGroupCode
	,AEEncounter.SourceOfReferralCode
	,AEEncounter.ArrivalDate
	,AEEncounter.ArrivalTime
	,AEEncounter.AgeOnArrival
	,AEEncounter.InitialAssessmentTime
	,AEEncounter.SeenForTreatmentTime
	,AEEncounter.AttendanceConclusionTime
	,AEEncounter.DepartureTime
	,AEEncounter.CommissioningSerialNo
	,AEEncounter.NHSServiceAgreementLineNo
	,AEEncounter.ProviderReferenceNo
	,AEEncounter.CommissionerReferenceNo
	,AEEncounter.ProviderCode
	,AEEncounter.CommissionerCode
	,AEEncounter.StaffMemberCode
	,AEEncounter.InvestigationCodeFirst
	,AEEncounter.InvestigationCodeSecond
	,AEEncounter.DiagnosisCodeFirst
	,AEEncounter.DiagnosisCodeSecond
	,AEEncounter.TreatmentCodeFirst
	,AEEncounter.TreatmentCodeSecond
	,AEEncounter.PASHRGCode
	,AEEncounter.HRGVersionCode
	,AEEncounter.PASDGVPCode
	,AEEncounter.SiteCode
	,AEEncounter.Created
	,Updated = coalesce(AEEncounter.Updated, getdate())
	,AEEncounter.ByWhom
	,AEEncounter.InterfaceCode
	,AEEncounter.PCTCode
	,AEEncounter.ResidencePCTCode
	,AEEncounter.InvestigationCodeList
	,AEEncounter.TriageCategoryCode
	,AEEncounter.PresentingProblem
	,AEEncounter.PresentingProblemCode
	,AEEncounter.ToXrayTime
	,AEEncounter.FromXrayTime
	,AEEncounter.ToSpecialtyTime
	,AEEncounter.SeenBySpecialtyTime
	,AEEncounter.EthnicCategoryCode
	,AEEncounter.ReligionCode
	,AEEncounter.ReferredToSpecialtyCode
	,AEEncounter.DecisionToAdmitTime
	,AEEncounter.DischargeDestinationCode
	,AEEncounter.RegisteredTime
	,AEEncounter.TransportRequestTime
	,AEEncounter.TransportAvailableTime
	,AEEncounter.AscribeLeftDeptTime
	,AEEncounter.CDULeftDepartmentTime
	,AEEncounter.PCDULeftDepartmentTime
	,AEEncounter.TreatmentDateFirst
	,AEEncounter.TreatmentDateSecond
	,AEEncounter.SourceAttendanceDisposalCode
	,AEEncounter.AmbulanceArrivalTime
	,AEEncounter.AmbulanceCrewPRF

	,UnplannedReattend7Day =
		UnplannedReattend.UnplannedReattend7Day

	,AEEncounter.ArrivalTimeAdjusted
	,AEEncounter.ClinicalAssessmentTime


	,LevelOfCareCode =
		case
		when
			AEEncounter.AttendanceDisposalCode = '01'
		then 2 --high
		when
			AEInvestigation.InvestigationDate is null
		and	AEEncounter.ToSpecialtyTime is null
		then 0 --low
		else 1 --medium
		end

	,EncounterBreachStatusCode = 
		case
			when AEEncounter.DepartureTime is null then 'X'
			when EncounterBreach.BreachValue is null then 'N'
			else 'B'
		end

	,EncounterStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
				,AEEncounter.ArrivalTime
			)
			,-1
		)

	,EncounterEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
				,AEEncounter.DepartureTime
			)
			,-1
		)

	,EncounterStartDate =
		dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)

	,EncounterEndDate = 
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounter.PCDULeftDepartmentTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.CDULeftDepartmentTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
		)

	,EncounterDurationMinutes =
		datediff(
			 minute
			,AEEncounter.ArrivalTime
			,AEEncounter.DepartureTime
		)

	,AgeCode =
		case
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) is null then 'Age Unknown'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 0 then '00 Days'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) = 1 then '01 Day'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 1 and
		datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate)), 2) + ' Days'
		
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 28 and
		datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end > 1 and
		 datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, AEEncounter.DateOfBirth,AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
		(
		case 
		when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
		or
			(
				datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
			And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
		(
		case 
		when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
		or
			(
				datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
			And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
			) then 1 else 0 end
		)), 2) + ' Years'

	end

	,LeftWithoutBeingSeenCases =
		case
		when
				(
					SourceAttendanceDisposalCode in ('10804' , '6193', '17369', '17368', '17759', '17901') -- GS 2011-06-02 change additional codes 17369 and 17368 added
				or																			   -- GS 2011-10-12 change additional codes '17759','17901'(both Left dept before being treated) added
					LookupBase.Lkp_Name in ( 'Did Not Wait' , 'Self Discharge') 
				)
			and	SeenForTreatmentTime is null
			and ClinicalAssessmentTime is null
		then 1
		else 0
		end
		
	--,HRG4AEEncounter.HRGCode
	,HRGCode = null

	,Reportable = coalesce(AEEncounter.Reportable , 1)
	,CareGroup = CareGroup.Reference
	,AEEncounter.ContextCode
	,EPMINo = null
	,DepartureTimeAdjusted = 
		case
		when AEEncounter.AttendanceDisposalCode in ('02' , '03') 
			and AEEncounter.AttendanceCategoryCode = 1  
			and AEEncounter.AttendanceConclusionTime < AEEncounter.DepartureTime
			then coalesce (
							 CarePathwayAttendanceConclusionTime
							,AEEncounter.AttendanceConclusionTime
							,CarePathwayDepartureTime
							,AEEncounter.DepartureTime
				)
		else coalesce (
				 CarePathwayDepartureTime
				,AEEncounter.DepartureTime
			)
		end
	,CarePathwayAttendanceConclusionTime
	,CarePathwayDepartureTime

	,DepartmentTypeCode =
		case
		when
			(
				AEEncounter.ContextCode = 'CEN||ADAS'
			or	AEEncounter.ContextCode = 'TRA||ADAS'
			or	Site.NationalValueCode = 'RW3T1'
			) then '03'
		when Site.NationalValueCode in ('RW3SM' , 'RW3RE' , 'RW3DH') then '02'
		else '01'
		end
	,GpCodeAtAttendance
	,GpPracticeCodeAtAttendance
	,PostcodeAtAttendance
	,AEEncounter.CCGCode
	,AlcoholLocation
	,AlcoholInPast12Hours

	,AttendanceResidenceCCGCode = PostcodeAtAttendance.CCGCode

from
	(
	select
		 EncounterRecno
		,Encounter.SourceUniqueID
		,Encounter.UniqueBookingReferenceNo
		,Encounter.PathwayId
		,Encounter.PathwayIdIssuerCode
		,Encounter.RTTStatusCode
		,Encounter.RTTStartDate
		,Encounter.RTTEndDate
		,Encounter.DistrictNo
		,Encounter.TrustNo
		,Encounter.CasenoteNo
		,Encounter.DistrictNoOrganisationCode

		,NHSNumber =
			replace(
				coalesce(
					Patient.NHSNumber
					,case
					when
						(
							Encounter.NHSNumber = '1234567899'  
						or	rtrim(Encounter.NHSNumber) = ''
						or Encounter.NHSNumber = 'REMOVE'
						or left(Encounter.NHSNumber,4) = 'SLF?' 
						or left(Encounter.NHSNumber,4) = 'MAN?'
						)
					then null
					else Encounter.NHSNumber
					end
				)
				,' '
				,''
			)

		,NHSNumberStatusId =
			coalesce(
				 Patient.NHSNumberStatusId
				,case
				when 
					(
						Encounter.NHSNumber = '1234567899'  
					or	Encounter.NHSNumber is null
					or	rtrim(Encounter.NHSNumber) = ''
					or Encounter.NHSNumber = 'REMOVE'
					or left(Encounter.NHSNumber,4) = 'SLF?'
					or left(Encounter.NHSNumber,4) = 'MAN?' 
					)			
				then 'RT'

				when 
					Encounter.NHSNumber is not null
				then 'NP'

				else coalesce(
						 Encounter.NHSNumberStatusId
						,'RT'
					)
				end
				,'RT'
			)
		,Encounter.PatientTitle
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.PatientAddress1
		,Encounter.PatientAddress2
		,Encounter.PatientAddress3
		,Encounter.PatientAddress4
		,Encounter.Postcode
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.SexCode
		,Encounter.CarerSupportIndicator
		,Encounter.RegisteredGpCode
		,Encounter.RegisteredGpPracticeCode
		,Encounter.AttendanceNumber
		,Encounter.ArrivalModeCode
		,Encounter.AttendanceCategoryCode
		,Encounter.AttendanceDisposalCode
		,Encounter.IncidentLocationTypeCode
		,Encounter.PatientGroupCode
		,Encounter.SourceOfReferralCode
		,Encounter.ArrivalDate
		,Encounter.ArrivalTime
		,Encounter.AgeOnArrival
		,Encounter.InitialAssessmentTime
		,Encounter.SeenForTreatmentTime
		,Encounter.AttendanceConclusionTime
		,Encounter.DepartureTime
		,Encounter.CommissioningSerialNo
		,Encounter.NHSServiceAgreementLineNo
		,Encounter.ProviderReferenceNo
		,Encounter.CommissionerReferenceNo
		,Encounter.ProviderCode
		,Encounter.CommissionerCode
		,Encounter.StaffMemberCode
		,Encounter.InvestigationCodeFirst
		,Encounter.InvestigationCodeSecond
		,Encounter.DiagnosisCodeFirst
		,Encounter.DiagnosisCodeSecond
		,Encounter.TreatmentCodeFirst
		,Encounter.TreatmentCodeSecond
		,Encounter.PASHRGCode
		,Encounter.HRGVersionCode
		,Encounter.PASDGVPCode
		,Encounter.SiteCode
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
		,Encounter.InterfaceCode
		,Encounter.PCTCode
		,Encounter.ResidencePCTCode
		,Encounter.InvestigationCodeList
		,Encounter.TriageCategoryCode
		,Encounter.PresentingProblem
		,PresentingProblemCode =
			
				case 
					when Encounter.PresentingProblemCode is null then '-1'
					else
					Encounter.PresentingProblemCode
				end

		,Encounter.ToXrayTime
		,Encounter.FromXrayTime
		,Encounter.ToSpecialtyTime
		,Encounter.SeenBySpecialtyTime
		,Encounter.EthnicCategoryCode
		,Encounter.ReligionCode
		,Encounter.ReferredToSpecialtyCode
		,Encounter.DecisionToAdmitTime
		,Encounter.DischargeDestinationCode
		,Encounter.RegisteredTime
		,Encounter.TransportRequestTime
		,Encounter.TransportAvailableTime
		,Encounter.AscribeLeftDeptTime
		,Encounter.CDULeftDepartmentTime
		,Encounter.PCDULeftDepartmentTime
		,Encounter.TreatmentDateFirst
		,Encounter.TreatmentDateSecond
		,Encounter.SourceAttendanceDisposalCode
		,Encounter.AmbulanceArrivalTime
		,Encounter.AmbulanceCrewPRF
		,Encounter.UnplannedReattend7Day
		,Encounter.ArrivalTimeAdjusted
		,Encounter.ClinicalAssessmentTime
		,Encounter.Reportable
		,Encounter.SourceCareGroupCode
		,EpisodicGpCode = 
			coalesce(
				 CASE WHEN EpisodicGpAtArrival.GpCode = 'G9999981' THEN NULL ELSE EpisodicGpAtArrival.GpCode END
				,Encounter.RegisteredGpCode
				,'G9999998'
			)
		,EpisodicGpPracticeCode = 
			coalesce(
				 CASE WHEN EpisodicGpAtArrival.GpPracticeCode = 'V81998' THEN NULL ELSE EpisodicGpAtArrival.GpPracticeCode END
				,Encounter.RegisteredGpPracticeCode
				,'V81999'
			)
		,Encounter.CarePathwayAttendanceConclusionTime
		,Encounter.CarePathwayDepartureTime
		,ContextCode =
			case
			when Encounter.SiteCode = 'RW3TR' and Encounter.InterfaceCode = 'ADAS' then 'TRA||ADAS'
			else
				'CEN||' + 
				case
				when Encounter.InterfaceCode in ('INFO', 'INQ') then 'PAS'
				else Encounter.InterfaceCode
				end
			end

		,GpCodeAtAttendance
		,GpPracticeCodeAtAttendance
		,PostcodeAtAttendance
		,CCGCode
		,AlcoholLocation
		,AlcoholInPast12Hours

	from
		[$(Warehouse)].AE.Encounter

	left outer join [$(Warehouse)].PAS.Patient
		on	Patient.DistrictNo = Encounter.DistrictNo

	left outer join  [$(Warehouse)].PAS.PatientGp EpisodicGpAtArrival
		on EpisodicGpAtArrival.SourcePatientNo = Patient.SourcePatientNo
	and Encounter.ArrivalDate between EpisodicGpAtArrival.EffectiveFromDate and isnull(EpisodicGpAtArrival.EffectiveToDate,getdate())

	) AEEncounter

left join [$(Warehouse)].AE.StageBase EncounterBreach
on	EncounterBreach.StageCode = 'INDEPARTMENT'
and
	datediff(
		 minute
		,AEEncounter.ArrivalTime
		--,AEEncounter.AttendanceConclusionTime
		,coalesce(
				 AEEncounter.PCDULeftDepartmentTime
				,AEEncounter.CDULeftDepartmentTime
				,AEEncounter.DepartureTime
		)
	)
	 > EncounterBreach.BreachValue


left join [$(Warehouse)].AE.Investigation AEInvestigation
on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
and	AEInvestigation.SequenceNo = 1


--left join [$(Warehouse)].AE.HRG4Encounter HRG4AEEncounter
--on	HRG4AEEncounter.EncounterRecno = AEEncounter.EncounterRecno

-- determine which attendances follow a previous unplanned attendance within seven days
left join (
	select
		 SourceUniqueID
		,UnplannedReattend7Day = 1
	from
		[$(Warehouse)].AE.Encounter AEEncounter
	where
		exists
			(
			select
				1
			from
				[$(Warehouse)].AE.Encounter PreviousAEEncounter
			where
				PreviousAEEncounter.DistrictNo = AEEncounter.DistrictNo
			and	PreviousAEEncounter.SiteCode = AEEncounter.SiteCode
			and	coalesce(
					 dateadd(day, datediff(day, 0, PreviousAEEncounter.PCDULeftDepartmentTime), 0)
					,dateadd(day, datediff(day, 0, PreviousAEEncounter.CDULeftDepartmentTime), 0)
					,dateadd(day, datediff(day, 0, PreviousAEEncounter.DepartureTime), 0)
					,dateadd(day, datediff(day, 0, PreviousAEEncounter.ArrivalTime), 0)
				) between dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime) - 7 , 0) and AEEncounter.ArrivalTime
			and	coalesce(
					 PreviousAEEncounter.PCDULeftDepartmentTime
					,PreviousAEEncounter.CDULeftDepartmentTime
					,PreviousAEEncounter.DepartureTime
					,PreviousAEEncounter.ArrivalTime
				) < AEEncounter.ArrivalTime
			and	PreviousAEEncounter.AttendanceCategoryCode != 2
			-- exclude children's division from the measurement of reattendance rate
			and	PreviousAEEncounter.SiteCode != 'RW3RC'
			)
	) UnplannedReattend

on	UnplannedReattend.SourceUniqueID = AEEncounter.SourceUniqueID
and	AEEncounter.AttendanceCategoryCode != 2

left join [$(Warehouse)].AE.LookupBase
on	LookupBase.Lkp_ID = AEEncounter.SourceAttendanceDisposalCode

LEFT JOIN [$(Warehouse)].AE.Reference CareGroup
ON  CareGroup.ReferenceCode = AEEncounter.SourceCareGroupCode
AND CareGroup.ReferenceParentCode = '5713'

--SITE	Site

left join WH.Member Site
on	Site.SourceValueCode = coalesce(cast(AEEncounter.SiteCode as varchar), '-1')
and	Site.AttributeCode = 'SITE'
and	Site.SourceContextCode = AEEncounter.ContextCode

left join [$(Organisation)].dbo.CCGPostcode PostcodeAtAttendance
on	PostcodeAtAttendance.Postcode =
		case
		when len(AEEncounter.PostcodeAtAttendance) = 8 then AEEncounter.PostcodeAtAttendance
		else left(AEEncounter.PostcodeAtAttendance, 3) + ' ' + right(AEEncounter.PostcodeAtAttendance, 4) 
		end












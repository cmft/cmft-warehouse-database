﻿







CREATE view [ETL].[PSSAPCInputFile]

as

select --top 100
	PROCODET = 'RW3' --Encounter.ProviderCode
	,PROVSPNO = Encounter.GlobalProviderSpellNo
	,STARTAGE = [$(CMFT)].Dates.GetAge(
								Encounter.DateOfBirth
								,Encounter.EpisodeStartDate
								)
	,SEX = Sex.NationalSexCode		
	,CLASSPAT = PatientClassification.NationalPatientClassificationCode
	,ADMIMETH = AdmissionMethod.NationalAdmissionMethodCode
	,MAINSPEF = Consultant.MainSpecialtyCode
	,TRETSPEF = TreatmentFunction.NationalSpecialtyCode
	,DIAG_01 =
			left(
				replace(PrimaryDiagnosis.DiagnosisCode, '.', '')
				,5
				) 
	,DIAG_02 =
		left(
			replace(SecondaryDiagnosis1.DiagnosisCode, '.', '')
			,5
			) 
	,DIAG_03 =
		left(
			replace(SecondaryDiagnosis2.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_04 =
		left(
			replace(SecondaryDiagnosis3.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_05 =
		left(
			replace(SecondaryDiagnosis4.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_06 =
		left(
			replace(SecondaryDiagnosis5.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_07 =
		left(
			replace(SecondaryDiagnosis6.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_08 =
		left(
			replace(SecondaryDiagnosis7.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_09 =
		left(
			replace(SecondaryDiagnosis8.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_10 =
		left(
			replace(SecondaryDiagnosis9.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_11 =
		left(
			replace(SecondaryDiagnosis10.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_12 =
		left(
			replace(SecondaryDiagnosis11.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_13 =
		left(
			replace(SecondaryDiagnosis12.DiagnosisCode, '.', '')
			,5
			)
	,DIAG_14 =
		left(
			replace(SecondaryDiagnosis13.DiagnosisCode, '.', '')
			,5
			)
	--,DIAG_15 =
	--	left(
	--		replace(SecondaryDiagnosis14.DiagnosisCode, '.', '')
	--		,5
	--		)
	--,DIAG_16 =
	--	left(
	--		replace(SecondaryDiagnosis14.DiagnosisCode, '.', '')
	--		,5
	--		)
	--,DIAG_17 =
	--	left(
	--		replace(SecondaryDiagnosis15.DiagnosisCode, '.', '')
	--		,5
	--		)
	,OPER_01 = 
		left(
			replace(PrimaryOperation.OperationCode, '.', '')
			,5
			)
	,OPER_02 = 
		left(
			replace(SecondaryOperation1.OperationCode, '.', '')
			,5
			)
	,OPER_03 = 
		left(
			replace(SecondaryOperation2.OperationCode, '.', '')
			,5
			)
	,OPER_04 = 
		left(
			replace(SecondaryOperation3.OperationCode, '.', '')
			,5
			)
	,OPER_05 = 
		left(
			replace(SecondaryOperation4.OperationCode, '.', '')
			,5
			)
	,OPER_06 = 
		left(
			replace(SecondaryOperation5.OperationCode, '.', '')
			,5
			)
	,OPER_07 = 
		left(
			replace(SecondaryOperation6.OperationCode, '.', '')
			,5
			)
	,OPER_08 = 
		left(
			replace(SecondaryOperation7.OperationCode, '.', '')
			,5
			)	
	,OPER_09 = 
		left(
			replace(SecondaryOperation8.OperationCode, '.', '')
			,5
			)
	,OPER_10 = 
		left(
			replace(SecondaryOperation9.OperationCode, '.', '')
			,5
			)
	,OPER_11 = 
		left(
			replace(SecondaryOperation10.OperationCode, '.', '')
			,5
			)
	,OPER_12 = 
		left(
			replace(SecondaryOperation11.OperationCode, '.', '')
			,5
			)
	--,OPER_13 = 
	--	left(
	--		replace(SecondaryOperation12.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_14 = 
	--	left(
	--		replace(SecondaryOperation13.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_15 = 
	--	left(
	--		replace(SecondaryOperation14.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_16 = 
	--	left(
	--		replace(SecondaryOperation15.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_17 = 
	--	left(
	--		replace(SecondaryOperation16.OperationCode, '.', '')
	--		,5
	--		)
	,PURCODE = PurchaserCode -- Need to point to demographics tables when done
	,CSNUM = ContractSerialNo
	,Encounter.MergeEncounterRecno
	,FinancialYear = 
				case
				when Discharge.FinancialYear = '2015/2016' -- using 14/15 grouper for 15/16 data - confirmed by Phil Huitson 7 Apr 2015
				then '2014/2015'
				else Discharge.FinancialYear
				end
from
	APC.BaseEncounter Encounter

inner join APC.BaseEncounterReference EncounterReference
on	Encounter.MergeEncounterRecno = EncounterReference.MergeEncounterRecno 
and Encounter.Reportable = 1

left outer join WH.Specialty TreatmentFunction
on	TreatmentFunction.SourceSpecialtyID = EncounterReference.SpecialtyID

left outer join WH.Consultant Consultant
on	Consultant.SourceConsultantID = EncounterReference.ConsultantID

left outer join APC.AdmissionMethod AdmissionMethod
on	AdmissionMethod.SourceAdmissionMethodID = EncounterReference.AdmissionMethodID

left outer join WH.Calendar EpisodeStart
on	EpisodeStart.DateID = EncounterReference.EpisodeStartDateID

left outer join WH.Calendar Discharge
on	Discharge.DateID = EncounterReference.DischargeDateID

left outer join WH.Sex Sex
on	Sex.SourceSexID = EncounterReference.SexID

left join APC.PatientClassification PatientClassification
on	SourcePatientClassificationID = EncounterReference.PatientClassificationID

left outer join APC.BaseDiagnosis PrimaryDiagnosis
on	PrimaryDiagnosis.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	PrimaryDiagnosis.SequenceNo = 0

left outer join APC.BaseDiagnosis SecondaryDiagnosis1
on	SecondaryDiagnosis1.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis1.SequenceNo = 1

left outer join APC.BaseDiagnosis SecondaryDiagnosis2
on	SecondaryDiagnosis2.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis2.SequenceNo = 2

left outer join APC.BaseDiagnosis SecondaryDiagnosis3
on	SecondaryDiagnosis3.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis3.SequenceNo = 3

left outer join APC.BaseDiagnosis SecondaryDiagnosis4
on	SecondaryDiagnosis4.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis4.SequenceNo = 4

left outer join APC.BaseDiagnosis SecondaryDiagnosis5
on	SecondaryDiagnosis5.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis5.SequenceNo = 5
	
left outer join APC.BaseDiagnosis SecondaryDiagnosis6
on	SecondaryDiagnosis6.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis6.SequenceNo = 6

left outer join APC.BaseDiagnosis SecondaryDiagnosis7
on	SecondaryDiagnosis7.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis7.SequenceNo = 7

left outer join APC.BaseDiagnosis SecondaryDiagnosis8
on	SecondaryDiagnosis8.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis8.SequenceNo = 8

left outer join APC.BaseDiagnosis SecondaryDiagnosis9
on	SecondaryDiagnosis9.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis9.SequenceNo = 9

left outer join APC.BaseDiagnosis SecondaryDiagnosis10
on	SecondaryDiagnosis10.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis10.SequenceNo = 10

left outer join APC.BaseDiagnosis SecondaryDiagnosis11
on	SecondaryDiagnosis11.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis11.SequenceNo = 11

left outer join APC.BaseDiagnosis SecondaryDiagnosis12
on	SecondaryDiagnosis12.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis12.SequenceNo = 12

left outer join APC.BaseDiagnosis SecondaryDiagnosis13
on	SecondaryDiagnosis13.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis13.SequenceNo = 13

left outer join APC.BaseDiagnosis SecondaryDiagnosis14
on	SecondaryDiagnosis14.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis14.SequenceNo = 14

left outer join APC.BaseDiagnosis SecondaryDiagnosis15
on	SecondaryDiagnosis15.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis15.SequenceNo = 15

left outer join APC.BaseDiagnosis SecondaryDiagnosis16
on	SecondaryDiagnosis16.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryDiagnosis16.SequenceNo = 16

left outer join APC.BaseOperation PrimaryOperation
on	PrimaryOperation.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	PrimaryOperation.SequenceNo = 0

left outer join APC.BaseOperation SecondaryOperation1
on	SecondaryOperation1.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation1.SequenceNo = 1

left outer join APC.BaseOperation SecondaryOperation2
on	SecondaryOperation2.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation2.SequenceNo = 2

left outer join APC.BaseOperation SecondaryOperation3
on	SecondaryOperation3.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation3.SequenceNo = 3

left outer join APC.BaseOperation SecondaryOperation4
on	SecondaryOperation4.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation4.SequenceNo = 4

left outer join APC.BaseOperation SecondaryOperation5
on	SecondaryOperation5.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation5.SequenceNo = 5

left outer join APC.BaseOperation SecondaryOperation6
on	SecondaryOperation6.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation6.SequenceNo = 6

left outer join APC.BaseOperation SecondaryOperation7
on	SecondaryOperation7.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation7.SequenceNo = 7

left outer join APC.BaseOperation SecondaryOperation8
on	SecondaryOperation8.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation8.SequenceNo = 8

left outer join APC.BaseOperation SecondaryOperation9
on	SecondaryOperation9.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation9.SequenceNo = 9

left outer join APC.BaseOperation SecondaryOperation10
on	SecondaryOperation10.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation10.SequenceNo = 10

left outer join APC.BaseOperation SecondaryOperation11
on	SecondaryOperation11.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation11.SequenceNo = 11

left outer join APC.BaseOperation SecondaryOperation12
on	SecondaryOperation12.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation12.SequenceNo = 12

left outer join APC.BaseOperation SecondaryOperation13
on	SecondaryOperation13.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation13.SequenceNo = 13

left outer join APC.BaseOperation SecondaryOperation14
on	SecondaryOperation14.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation14.SequenceNo = 14

left outer join APC.BaseOperation SecondaryOperation15
on	SecondaryOperation15.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation15.SequenceNo = 15

left outer join APC.BaseOperation SecondaryOperation16
on	SecondaryOperation16.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation16.SequenceNo = 16

--where
--	exists
--		(
--		select
--			1
--		from
--			APC.BaseEncounter EncounterUpdate
--		where	
--			EncounterUpdate.Updated >= cast(dateadd(day, -14, getdate()) as date)	
--			--EncounterUpdate.Updated >= dateadd(year, -1, getdate()) -- just for testing
--		and	EncounterUpdate.DischargeDate >= '1 apr 2013' --we don't have groupers plugged in for previous financial years
--		and	EncounterUpdate.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
--		and EncounterUpdate.Reportable = 1
--		)

where
	exists
		(
		select
			1
		from
			APC.BaseEncounter BaseEncounterProcessList
		inner join APC.ProcessList
		on	ProcessList.MergeRecno = BaseEncounterProcessList.MergeEncounterRecno
		where		
			BaseEncounterProcessList.DischargeDate >= '1 apr 2012' --we don't have groupers plugged in for previous financial years
		and	BaseEncounterProcessList.GlobalProviderSpellNo = Encounter.GlobalProviderSpellNo
		and BaseEncounterProcessList.Reportable = 1
		)

union

select --top 100
	PROCODET = null
	,PROVSPNO = null
	,STARTAGE = null
	,SEX = null
	,CLASSPAT = null
	,ADMIMETH = null
	,MAINSPEF = null
	,TRETSPEF = null
	,DIAG_01= null 
	,DIAG_02= null
	,DIAG_03 = null
	,DIAG_04 = null
	,DIAG_05 = null
	,DIAG_06 = null
	,DIAG_07 = null
	,DIAG_08 = null
	,DIAG_09 = null
	,DIAG_10 = null
	,DIAG_11 = null
	,DIAG_12 = null
	,DIAG_13 = null
	,DIAG_14 = null
	,OPER_01 = null
	,OPER_02 = null
	,OPER_03 = null
	,OPER_04 = null
	,OPER_05 = null
	,OPER_06 = null
	,OPER_07 = null
	,OPER_08 = null	
	,OPER_09 = null
	,OPER_10 = null
	,OPER_11 = null
	,OPER_12 = null
	,PURCODE = null
	,CSNUM = null
	,MergeEncounterRecno  = -20132014 -- dummy record
	,FinancialYear = '2013/2014'
































﻿





CREATE view [ETL].[TLoadCenCOMBaseReferral] as

select
	 SourceUniqueID
	,SourcePatientID
	,ReferredBySpecialtyID
	,ReferredToSpecialtyID
	,SourceofReferralID
	,ReferralReceivedTime
	,ReferralReceivedDate
	,ReasonforReferralID
	,CancelledTime
	,CancelledDate
	,CancelledReasonID
	,ReferralSentTime
	,ReferralSentDate
	,Urgency
	,Priority
	,AuthorisedTime = AuthoriedTime
	,AuthorisedDate = AuthoriedDate
	,ReferralClosedTime
	,ReferralClosedDate
	,ClosureReasonID
	,ReferredByProfessionalCarerID
	,ReferredByStaffTeamID
	,ReferredByHealthOrgID
	,ReferredToProfessionalCarerID
	,ReferredToHealthOrgID
	,ReferredToStaffTeamID
	,OutcomeOfReferral
	,ReferralStatus
	,RejectionID
	,TypeofReferralID
	,PatientSourceID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,RTTStatusDate
	,RTTPatientPathwayID
	,RTTStartTime
	,RTTStartFlag
	,RTTStatusID
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,ParentID
	,HealthOrgOwner
	,RecordStatus
	,RequestedServiceID

	,AgeCode =
	case
	when datediff(day, PatientDateOfBirth, Encounter.ReferralReceivedDate) is null then 'Age Unknown'
	when datediff(day, PatientDateOfBirth, Encounter.ReferralReceivedDate) <= 0 then '00 Days'
	when datediff(day, PatientDateOfBirth, Encounter.ReferralReceivedDate) = 1 then '01 Day'
	when datediff(day, PatientDateOfBirth, Encounter.ReferralReceivedDate) > 1 and
	datediff(day, PatientDateOfBirth, Encounter.ReferralReceivedDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, Encounter.ReferralReceivedDate)), 2) + ' Days'
	
	when datediff(day, PatientDateOfBirth, Encounter.ReferralReceivedDate) > 28 and
	datediff(month, PatientDateOfBirth, Encounter.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.ReferralReceivedDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, PatientDateOfBirth, Encounter.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.ReferralReceivedDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, PatientDateOfBirth, Encounter.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.ReferralReceivedDate) then 1 else 0 end > 1 and
	 datediff(month, PatientDateOfBirth, Encounter.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.ReferralReceivedDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,Encounter.ReferralReceivedDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.ReferralReceivedDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, PatientDateOfBirth, Encounter.ReferralReceivedDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.ReferralReceivedDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Encounter.ReferralReceivedDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.ReferralReceivedDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, Encounter.ReferralReceivedDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.ReferralReceivedDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Encounter.ReferralReceivedDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.ReferralReceivedDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom

	,ContextCode = 'CEN||IPM'

	--,ModifiedRow =
	--	case
	--	when
	--		Encounter.Updated >
	--		(
	--		select
	--			DatasetProcess.DatasetProcessTime
	--		from
	--			ETL.DatasetProcess
	--		where
	--			DatasetProcess.DatasetCode = 'COMR'
	--		and	DatasetProcess.ProcessCode = 'IMPORT'
	--		)
	--	then 1
	--	else 0
	--	end

from
	[$(Warehouse)].COM.Referral Encounter
where
	Encounter.ArchiveFlag = 'N'
and	Encounter.RecordStatus = 'C'
and
	(
		Encounter.ReferralReceivedDate >= '01 Apr 2008'
	or	Encounter.ReferralClosedDate >= '01 Apr 2008'
	)









﻿CREATE view [ETL].[TLoadCenSCRBaseReferral] as

select
	 UniqueRecordId
	,DemographicUniqueRecordId
	,SystemId
	,CancerSite
	,SourceOfReferralCode
	,ReferringOrganisationCode
	,ReferrerCode
	,PriorityTypeCode
	,DecisionToReferDate
	,ReceiptOfReferralDate
	,ConsultantCode
	,SpecialtyCode
	,FirstAppointmentDate
	,FirstAppointmentOrganisationCode
	,ReferralToFirstAppointmentDelayReasonCode
	,CancerTypeCode
	,CancerStatusCode
	,FirstAppointmentOfferedCode
	,CancelledAppointmentDate
	,FirstAppointmentWaitingTimeAdjusted
	,FirstAppointmentAdjustmentReasonCode
	,MethodOfReferralCode
	,SourceForOutpatientCode
	,ReferredToSpecialistDate
	,ReferredFromOrganisationCode
	,FirstAppointmentWithSpecialistDate
	,FirstAppointmentSpecialistOrganisationCode
	,ConsultantUpgradeDate
	,OrganisationUpgradeCode
	,WhenPatientUpgradedCode
	,PatientUpgradedByCode
	,DiagnosisDate
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisCode1
	,DiagnosisOrganisationCode
	,PatientInformedDate
	,OtherDiagnosisDate
	,LateralityCode
	,BasisOfDiagnosisCode
	,HistologyGroupCode
	,HistologyCode
	,GradeOfDifferentiationCode
	,PreTreatmentTStageCode
	,PreTreatmentTStageCertaintyCode
	,PreTreatmentNStageCode
	,PreTreatmentNStageCertaintyCode
	,PreTreatmentMStageCode
	,PreTreatmentMStageCertaintyCode
	,PreTreatmentOverallCertaintyCode
	,SiteSpecificClassificationCode
	,FinalOverallCertaintyCode
	,FinalTStageCertaintyCode
	,FinalTStageCode
	,FinalNStageCertaintyCode
	,FinalNStageCode
	,FinalMStageCertaintyCode
	,FinalMStageCode
	,GpInformedOfDiagnosis
	,GpInformedOfDiagnosisDate
	,ReasonNotInformed
	,RelativeCarerInformedOfDiagnosis
	,ReferredToSpecialistNurseDate
	,FirstSeenBySpecialistNurseDate
	,DecisionToReferAdjustment
	,DecisionToTreatAdjustment
	,DecisionToReferAdjustmentReasonCode
	,DecisionToTreatAdjustmentReasonCode
	,DecisionToReferAdjustmentDelayReasonCode
	,DecisionToTreatAdjustmentDelayReasonCode
	,ReferringSymptoms1
	,ReferringSymptoms2
	,ReferringSymptoms3
	,SpecialistNursePresent
	,FinalTNMDate
	,PreTreatmentTNMDate
	,FirstAppointmentConsultantCode
	,ReferralToAppropriateSpecialistCode
	,TertiaryCentreReferredToDate
	,TertiaryCentreOrganisationCode
	,ReasonForReferralTertiaryCentreCode
	,MakeNewReferralCode
	,NewTumourSite
	,AutoReferralFlag
	,OtherTumourSiteDiagnosisCode
	,OtherTumourSiteDiagnosis
	,InappropriateReferralCode
	,InappropriateReason
	,TumourStatusCode
	,NonCancerDetails
	,FirstAppointmentTypeCode
	,ReasonNoAppointmentCode
	,AssessedByCode
	,OtherSymptoms
	,AdditionalCommentsReferral
	,ReferralToFirstAppointmentDelayReasonComments
	,DecisionToReferDelayReasonComments
	,DecisionToTreatDelayReasonComments
	,DiagnosisComments
	,SiteCode = 'CEN'
	,IntegratedTNM

	,ContextCode = 'CEN||SCR'
from
	[$(Warehouse)].SCR.Referral


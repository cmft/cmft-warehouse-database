﻿



CREATE view [ETL].[TImportTraOPBaseEncounter] as

SELECT --top 1000 
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	--,Encounter.SourceEncounterNo
	,SourceEncounterNo = null
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath

	----,SexCode =
	----	case
	----	when coalesce(Encounter.SexCode, '') = ''
	----	then null
	----	else Encounter.SexCode
	----	end

	----,SexCode =
	----	case Encounter.SexCode
	----	when '' then '0'
	----	when 'M' then '1'
	----	when 'F' then '2'
	----	when 'I' then '9'
	----	else '#'
	----	end -- commented out as mapping process will take care of these values
	,Encounter.SexCode
	,Encounter.NHSNumber
	,NHSNumberStatusCode = Encounter.NHSNumberStatusId
	,Encounter.DistrictNo
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	--,Encounter.DHACode
	,DHACode = null
	,EthnicOriginCode = Encounter.EthnicCategoryCode
	,Encounter.MaritalStatusCode
	,ReligionCode =
		coalesce(
			Encounter.ReligionCode
			,'-1'
				)

	,Encounter.RegisteredGpCode
	,RegisteredGpPracticeCode =
		Encounter.RegisteredGpPracticeCode
	,Encounter.SiteCode
	,Encounter.AppointmentDate
	,Encounter.AppointmentTime

	,ClinicCode =
		coalesce(
			Encounter.ClinicCode
			,'N/A'
		)

	,AdminCategoryCode  =  Encounter.SourceAdminCategoryCode
	,Encounter.SourceOfReferralCode
	,ReasonForReferralCode = Encounter.SourceReferralReasonCode
	,PriorityCode = Encounter.PriorityTypeCode
	,FirstAttendanceFlag = FirstAttendanceCode
	,Encounter.DNACode
	,Encounter.AppointmentStatusCode
	,Encounter.CancelledByCode
	
	--,Encounter.TransportRequiredFlag
	,TransportRequiredFlag = null
	,Encounter.AttendanceOutcomeCode
	,Encounter.AppointmentTypeCode
	--,Encounter.DisposalCode
	,DisposalCode = null

	,ConsultantCode =
		coalesce(convert(varchar(20),Encounter.ConsultantCode), 'N/A')

	,SpecialtyCode =
		coalesce(convert(varchar(10),Encounter.TreatmentFunctionCode), 'N/A')

	,ReferralConsultantCode =
		coalesce(convert(varchar(20),Encounter.ReferralConsultantCode), 'N/A')

	,ReferralSpecialtyCode =
		coalesce(convert(varchar(10),Encounter.ReferralSpecialtyCode)
				,convert(varchar(10),Encounter.TreatmentFunctionCode)
				,'N/A'
		)

	--,BookingTypeCode =
	--	coalesce(Encounter.BookingTypeCode, 'N/A')
	,BookingTypeCode = null

	,Encounter.CasenoteNo
	,Encounter.AppointmentCreateDate

	--,Encounter.EpisodicGpCode
	,EpisodicGpCode = null

	--,EpisodicGpPracticeCode =
	--	coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredGpPracticeCode, 'X99999')
	,EpisodicGpPracticeCode = null

	--,Encounter.DoctorCode
	,DoctorCode = null
	,Encounter.PrimaryDiagnosisCode
	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12


	,PrimaryOperationCode = Encounter.PrimaryProcedureCode
		--coalesce(Encounter.PrimaryProcedureCode , '##')
	,PrimaryProcedureDate
	,Encounter.SecondaryProcedureCode1
	,Encounter.SecondaryProcedureDate1
	,Encounter.SecondaryProcedureCode2
	,Encounter.SecondaryProcedureDate2
	,Encounter.SecondaryProcedureCode3
	,Encounter.SecondaryProcedureDate3
	,Encounter.SecondaryProcedureCode4
	,Encounter.SecondaryProcedureDate4
	,Encounter.SecondaryProcedureCode5
	,Encounter.SecondaryProcedureDate5
	,Encounter.SecondaryProcedureCode6
	,Encounter.SecondaryProcedureDate6
	,Encounter.SecondaryProcedureCode7
	,Encounter.SecondaryProcedureDate7
	,Encounter.SecondaryProcedureCode8
	,Encounter.SecondaryProcedureDate8
	,Encounter.SecondaryProcedureCode9
	,Encounter.SecondaryProcedureDate9
	,Encounter.SecondaryProcedureCode10
	,Encounter.SecondaryProcedureDate10
	,Encounter.SecondaryProcedureCode11
	,Encounter.SecondaryProcedureDate11
	--,Encounter.PurchaserCode
	,PurchaserCode = 
		Encounter.CommissionerCode
	,Encounter.ProviderCode
	,ContractSerialNo = Encounter.CommissioningSerialNo
	,ReferralDate = Encounter.ReferralRequestReceivedDate
	,RTTPathwayID = Encounter.PathwayId
	--,Encounter.RTTPathwayCondition
	,RTTPathwayCondition = null
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,RTTSpecialtyCode = Encounter.SpecialtyCode
	,RTTCurrentProviderCode = 
		Encounter.ProviderCode
		+ '00'
	,RTTCurrentStatusCode = Encounter.RTTStatusCode
	--,Encounter.RTTCurrentStatusDate
	,RTTCurrentStatusDate = null
	--,Encounter.RTTCurrentPrivatePatientFlag
	,RTTCurrentPrivatePatientFlag = null
	--,Encounter.RTTOverseasStatusFlag
	,RTTOverseasStatusFlag = null
	--,Encounter.RTTPeriodStatusCode
	,RTTPeriodStatusCode = null
	--,Encounter.AppointmentCategoryCode --link to First attendance?
	--,Encounter.AppointmentCreatedBy
	,AppointmentCategoryCode = null
	,AppointmentCreatedBy = null
	--,Encounter.AppointmentCancelDate
	,AppointmentCancelDate = null
	--,Encounter.LastRevisedDate
	,LastRevisedDate = null
	--,Encounter.LastRevisedBy
	,LastRevisedBy = null
	,OverseasStatusFlag = Encounter.OverseasVisitorCode --check
	--,Encounter.PatientChoiceCode
	,PatientChoiceCode = null
	--,Encounter.ScheduledCancelReasonCode
	,ScheduledCancelReasonCode = null
	--,Encounter.PatientCancelReason
	,PatientCancelReason = null
	--,Encounter.DischargeDate
	,DischargeDate = null
	--,Encounter.QM08StartWaitDate
	,QM08StartWaitDate = null
	--,Encounter.QM08EndWaitDate
	,QM08EndWaitDate = null
	--,Encounter.DestinationSiteCode
	,DestinationSiteCode = null
	--,Encounter.EBookingReferenceNo
	,EBookingReferenceNo = null
	,Encounter.InterfaceCode
	--,Encounter.Created
	,Created = null
	,Encounter.Updated
	,Encounter.ByWhom
	,LocalAdminCategoryCode = Encounter.SourceAdminCategoryCode
	,Encounter.PCTCode
	--,Encounter.LocalityCode
	,LocalityCode = null

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 1 and
	datediff(day, DateOfBirth, Encounter.AppointmentDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.AppointmentDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.AppointmentDate) > 28 and
	datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.AppointmentDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.AppointmentDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.AppointmentDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.AppointmentDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.AppointmentDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.AppointmentDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,HRGCode =
		coalesce (
				Encounter.PASHRGCode
				,'ZZZ'
				)
		--coalesce(
		--		 HRG4Encounter.HRGCode
		--		,'ZZZ'
		--		)
	,Cases = 1
	,LengthOfWait =
		datediff(
			 day
			,Encounter.ReferralRequestReceivedDate
			,Encounter.AppointmentDate
		)
	--,Encounter.IsWardAttender
	,IsWardAttender = null
	,RTTActivity = null
		--convert(
		--	bit
		--	,case
		--	when
		--		Encounter.SpecialtyCode not in 
		--		(
		--		select
		--			SpecialtyCode
		--		from
		--			RTT.dbo.ExcludedSpecialty
		--		)

		--	-- 'GENE' ?
		--	and	Encounter.ConsultantCode <> 'FRAC'

		--	--ward attenders
		--	and	Encounter.IsWardAttender = 0

		--	and	Encounter.SiteCode != 'FMSK'

		--	--attended
		--	and	Encounter.DNACode in ('1', '5', '6')

		--	and	not exists
		--		(
		--		select
		--			1
		--		from
		--			RTT.dbo.RTTOPClockStop ClockChangeReason
		--		where
		--			ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
		--		and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
		--		and	(
		--				coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
		--			or	coalesce(ClockChangeReason.ClockStopReasonCode, '') = 'NOP' --Not an 18 week pathway
		--			)
		--		)

		--	then 1
		--	else 0
		--	end
		--)

	--,RTTBreachStatusCode =
	--	case
	--	when Encounter.RTTBreachDate is null
	--	then 'U'
	--	when
	--		(
	--			--RTTOPClockStop.ClockStopDate = Encounter.AppointmentDate
	--		--or	Encounter.RTTEndDate = Encounter.AppointmentDate
	--			Encounter.RTTEndDate = Encounter.AppointmentDate
	--		)
	--	and Encounter.RTTBreachDate < Encounter.AppointmentDate
	--	then 'B'
	--	else 'N'
	--	end
	,RTTBreachStatusCode = null
	--,Encounter.ClockStartDate
	,ClockStartDate = null
	,RTTBreachDate = null
	,RTTTreated = null -- already set null DG 07/03/2012
		--case
		--when RTTStatus.ClockStopFlag = 1 then 1
		--when RTTOPClockStop.ClockStopDate = Encounter.AppointmentDate then 1
		--when Encounter.RTTEndDate = Encounter.AppointmentDate then 1
		--else 0
		--end

	,DirectorateCode = '80'
	--,ReferralSpecialtyTypeCode =  coalesce(Encounter.ReferralSpecialtyTypeCode , 'U')
	,ReferralSpecialtyTypeCode = null
	,LastDNAorPatientCancelledDate = LastDNAOrCancelledDate
	,ReferredByCode
	,ReferrerCode
	,AppointmentOutcomeCode
	,MedicalStaffTypeCode
	,ContextCode
	,TreatmentFunctionCode
	
	,CommissioningSerialNo = 
		Encounter.CommissioningSerialNo
		
	,DerivedFirstAttendanceFlag = 
		Encounter.FirstAttendanceCode
	
	,AttendanceIdentifier = 
		AttendanceID
	
	,RegisteredGpAtAppointmentCode =
			Encounter.RegisteredGpCode
	
	,RegisteredGpPracticeAtAppointmentCode = 
		Encounter.RegisteredGpPracticeCode

	,Encounter.ReferredByConsultantCode
	,Encounter.ReferredByGpCode
	,Encounter.ReferredByGdpCode
	,Encounter.CCGCode
	,EpisodicPostCode
	,GpCodeAtAppointment 
	,GpPracticeCodeAtAppointment
	,PostcodeAtAppointment

	,AppointmentResidenceCCGCode = PostcodeAtAppointment.CCGCode	 
from
(
select
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.UniqueBookingReferenceNo
	,Encounter.PathwayId
	,Encounter.PathwayIdIssuerCode
	,Encounter.RTTStatusCode
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.DistrictNo
	,Encounter.TrustNo
	,Encounter.CasenoteNo
	,Encounter.DistrictNoOrganisationCode
	,Encounter.NHSNumber
	,Encounter.NHSNumberStatusId
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.Postcode
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath
	,Encounter.SexCode
	,Encounter.CarerSupportIndicator
	,Encounter.EthnicCategoryCode
	,Encounter.MaritalStatusCode
	,Encounter.AttendanceID
	,Encounter.AdminCategoryCode
	,Encounter.DNACode
	,CancelledByCode =
	
		CASE
		WHEN EXISTS
						(
						SELECT
							1
						FROM
							[$(TraffordReferenceData)].dbo.UGContactReason
						WHERE
							UGContactReason.conrsn_code IN ('T-HSusp', 'T-HosSus','T-WLSus', 'HospCan' ,'HospCanc', 'T-HCan', 'T-HCOD', 'T-HosC', 'T-HRWL', 'T-HBE', 'T-HCanPr', 'T-HDV', 'T-HError', 'T-HNV', 'T-HosNV')
						AND UGContactReason.Sourceuniqueid = Encounter.SourceUniqueID
						) AND AppointmentStatusCode = 'C'  THEN 'H'
		when EXISTS
						(
						SELECT
							1
						FROM
							[$(TraffordReferenceData)].dbo.UGContactReason
						WHERE
							UGContactReason.conrsn_code IN ('G-Illnes', 'G-Ill','G-NoProc', 'T-NoProc', 'T-ProcNo', 'T-Susp', 'T-PtSus', 'T-PDNA', 'T-PtDNA', 'MDSPtCan', 'PtCan', 'T-COD', 'T-PCan', 'T-PRWL', 'T-PtCan', 'PatCanc' ,'PtCancWL', 'PtCanOd', 'T-PCOD')
						AND UGContactReason.Sourceuniqueid = Encounter.SourceUniqueID
						) AND AppointmentStatusCode = 'C'  THEN 'P'
						
						ELSE NULL END
	
	
	,Encounter.FirstAttendanceCode
	,Encounter.MedicalStaffTypeCode
	,Encounter.OperationStatusCode
	,Encounter.AttendanceOutcomeCode
	,Encounter.AppointmentDate
	,Encounter.AppointmentNote
	,Encounter.AgeOnAppointment
	,Encounter.EarliestReasonableOfferDate

	,CommissioningSerialNo = 
		CASE 
		WHEN Encounter.CommissioningSerialNo = 'X98000' THEN '5NR000'
		ELSE Encounter.CommissioningSerialNo
		END

	,Encounter.NHSServiceAgreementLineNo
	,Encounter.ProviderReferenceNo
	,Encounter.CommissionerReferenceNo
	,Encounter.ProviderCode

	,CommissionerCode = 
		CASE 
		WHEN Encounter.CommissionerCode = 'X98' THEN '5NR'
		ELSE Encounter.CommissionerCode
		END

	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.TreatmentFunctionCode
	,Encounter.ReferringSpecialtyCode
	,Encounter.ICDDiagnosisSchemeCode

	,PrimaryDiagnosisCode =
		Diagnosis1.DiagnosisCode

	,Encounter.ReadDiagnosisSchemeCode
	,Encounter.PrimaryDiagnosisReadCode
	,Encounter.OPCSProcedureSchemeCode

	,PrimaryProcedureCode =
		Procedure1.ProcedureCode

	,PrimaryProcedureDate =
		Procedure1.ProcedureDate

	,Encounter.ReadProcedureSchemeCode
	,Encounter.PrimaryProcedureReadCode
	,Encounter.LocationClassCode
	,Encounter.SiteCode
	,Encounter.LocationTypeCode
	,Encounter.RegisteredGpCode
	,Encounter.RegisteredGpPracticeCode
	,Encounter.PriorityTypeCode
	,Encounter.ServiceTypeRequestedCode
	,Encounter.SourceOfReferralCode
	,Encounter.ReferralRequestReceivedDate
	,Encounter.ReferrerCode
	,Encounter.ReferrerOrganisationCode
	,Encounter.ReferralReason
	,Encounter.LastDNAOrCancelledDate
	,Encounter.PASHRGCode
	,Encounter.HRGVersionCode
	,Encounter.PASDGVPCode
	,Encounter.PCTCode
	,Encounter.ClinicCode
	,Encounter.ClinicFunctionCode
	,Encounter.OverseasVisitorCode
	,Encounter.ResidencePCTCode
	,Encounter.DepartmentCode
	,Encounter.AppointmentStatusCode
	,Encounter.IsDeleted
	,Encounter.ContactChangeDate
	,Encounter.EpisodeChangeDate
	,Encounter.ContactChangeTime
	,Encounter.EpisodeChangeTime
	,Encounter.PASUpdateDate
	,Encounter.InterfaceCode
	,Encounter.AppointmentTypeCode
	,Encounter.OriginalPathwayId
	,Encounter.OriginalPathwayIdIssuerCode
	,Encounter.OriginalRTTStatusCode
	,Encounter.OriginalRTTStartDate
	,Encounter.OriginalRTTEndDate
	,Encounter.RTTSection
	,Encounter.AppointmentTime
	,Encounter.Updated
	,Encounter.ByWhom
	,Encounter.AppTariff
	,Encounter.ProcTariff
	,Encounter.NFFTariff
	,Encounter.EPMINumber
	,Encounter.OutcomeCode
	,Encounter.APPReferralDate
	,SourceReferralReasonCode = ReferralReason.ReferralReasonID
	,Encounter.SourceAdminCategoryCode

	,SourcePatientNo = DistrictNo

	,SubsidiaryDiagnosisCode = null -- check sequence for all these and check why primary isn't coming through?
	,SecondaryDiagnosisCode1 = Diagnosis2.DiagnosisCode
	,SecondaryDiagnosisCode2 = Diagnosis3.DiagnosisCode
	,SecondaryDiagnosisCode3 = Diagnosis4.DiagnosisCode
	,SecondaryDiagnosisCode4 = Diagnosis5.DiagnosisCode
	,SecondaryDiagnosisCode5 = Diagnosis6.DiagnosisCode
	,SecondaryDiagnosisCode6 = Diagnosis7.DiagnosisCode
	,SecondaryDiagnosisCode7 = Diagnosis8.DiagnosisCode
	,SecondaryDiagnosisCode8 = Diagnosis9.DiagnosisCode
	,SecondaryDiagnosisCode9 = Diagnosis10.DiagnosisCode
	,SecondaryDiagnosisCode10 = Diagnosis11.DiagnosisCode
	,SecondaryDiagnosisCode11 = Diagnosis12.DiagnosisCode
	,SecondaryDiagnosisCode12 = Diagnosis13.DiagnosisCode
	
	,SecondaryProcedureCode1 = Procedure2.ProcedureCode
	,SecondaryProcedureDate1 = Procedure2.ProcedureDate
	,SecondaryProcedureCode2 = Procedure3.ProcedureCode
	,SecondaryProcedureDate2 = Procedure3.ProcedureDate
	,SecondaryProcedureCode3 = Procedure4.ProcedureCode
	,SecondaryProcedureDate3 = Procedure4.ProcedureDate
	,SecondaryProcedureCode4 = Procedure5.ProcedureCode
	,SecondaryProcedureDate4 = Procedure5.ProcedureDate
	,SecondaryProcedureCode5 = Procedure6.ProcedureCode
	,SecondaryProcedureDate5 = Procedure6.ProcedureDate
	,SecondaryProcedureCode6 = Procedure7.ProcedureCode
	,SecondaryProcedureDate6 = Procedure7.ProcedureDate
	,SecondaryProcedureCode7 = Procedure8.ProcedureCode
	,SecondaryProcedureDate7 = Procedure8.ProcedureDate
	,SecondaryProcedureCode8 = Procedure9.ProcedureCode
	,SecondaryProcedureDate8 = Procedure9.ProcedureDate
	,SecondaryProcedureCode9 = Procedure10.ProcedureCode
	,SecondaryProcedureDate9 = Procedure10.ProcedureDate
	,SecondaryProcedureCode10 = Procedure11.ProcedureCode
	,SecondaryProcedureDate10 = Procedure11.ProcedureDate
	,SecondaryProcedureCode11 = Procedure12.ProcedureCode
	,SecondaryProcedureDate11 = Procedure12.ProcedureDate
	
	,AppointmentOutcomeCode =
		CASE 
		WHEN AppointmentStatusCode = 'V' THEN 'ATT'
		WHEN AppointmentStatusCode = 'D' THEN 'DNA'
		WHEN 
				AppointmentStatusCode = 'C' 
			AND	(
					DATEADD(D, 0, DATEDIFF(D, 0, AppointmentTime)) = DATEADD(D, 0, DATEDIFF(D, 0, ContactChangeTime))
				or	EXISTS
						(
						SELECT
							1
						FROM
							[$(TraffordReferenceData)].dbo.UGContactReason
						WHERE
							conrsn_code = 'T-PCOD'
						AND UGContactReason.Sourceuniqueid = Encounter.SourceUniqueID
						)
				) THEN 'CND'
		ELSE 'NR'
		END

	,ReferringConsultantCode = ConsultantCode
	
	,ReferredByCode =
		CASE
		WHEN LEFT(ReferrerCode , 1) = 'G'  THEN 'GP' 
		WHEN LEFT(ReferrerCode , 1) = 'D'  THEN 'GDP'
		WHEN LEFT(ReferrerCode , 1) = 'C'  THEN 'CONS'
		ELSE 'OTH'
		END
	
	,ContextCode = 'TRA||' + Encounter.InterfaceCode

	,ReferralSpecialtyCode = TreatmentFunctionCode /* Changed from SpecialtyCode to align with Trafford SLAM process */
	,ReferralConsultantCode = ConsultantCode

	,ReferredByConsultantCode =
		case
		when LEFT(ReferrerCode , 1) = 'C'
		then Encounter.ReferrerCode
		else null
		end

	,ReferredByGpCode =
		case
		when LEFT(ReferrerCode , 1) = 'G'
		then Encounter.ReferrerCode
		else null
		end

	,ReferredByGdpCode =
		case
		when LEFT(ReferrerCode , 1) = 'D'
		then Encounter.ReferrerCode
		else null
		end

		,CCGCode = 
				coalesce(
					 CCGPractice.ParentOrganisationCode
					,CCGPostcode.CCGCode
					,'02A'
				)
		,EpisodicPostCode  = Encounter.Postcode

		,GpCodeAtAppointment = Encounter.RegisteredGpCode
		,GpPracticeCodeAtAppointment = Encounter.RegisteredGpPracticeCode
		,PostcodeAtAppointment = Encounter.Postcode
		,Encounter.ReligionCode
		,Encounter.AppointmentCreateDate


FROM
	[$(TraffordWarehouse)].OP.Encounter Encounter

	left join [$(Organisation)].dbo.CCGPractice CCGPractice
	on CCGPractice.OrganisationCode = Encounter.RegisteredGpPracticeCode


	left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
	on	CCGPostcode.Postcode = 
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end	


left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis1	
on	Diagnosis1.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis1.SequenceNo = 1

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis2	
on	Diagnosis2.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis2.SequenceNo = 2

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis3
on	Diagnosis3.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis3.SequenceNo = 3

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis4
on	Diagnosis4.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis4.SequenceNo = 4

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis5
on	Diagnosis5.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis5.SequenceNo = 5

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis6
on	Diagnosis6.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis6.SequenceNo = 6

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis7
on	Diagnosis7.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis7.SequenceNo = 7

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis8
on	Diagnosis8.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis8.SequenceNo = 8

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis9
on	Diagnosis9.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis9.SequenceNo = 9

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis10
on	Diagnosis10.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis10.SequenceNo = 10

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis11
on	Diagnosis11.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis11.SequenceNo = 11

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis12
on	Diagnosis12.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis12.SequenceNo = 12

left join [$(TraffordWarehouse)].dbo.OPDiagnosis Diagnosis13
on	Diagnosis13.OPSourceUniqueID = Encounter.SourceUniqueID
and	Diagnosis13.SequenceNo = 13



left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure1
on	Procedure1.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure1.SequenceNo = 1

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure2
on	Procedure2.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure2.SequenceNo = 2

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure3
on	Procedure3.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure3.SequenceNo = 3

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure4
on	Procedure4.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure4.SequenceNo = 4

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure5
on	Procedure5.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure5.SequenceNo = 5

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure6
on	Procedure6.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure6.SequenceNo = 6

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure7
on	Procedure7.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure7.SequenceNo = 7

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure8
on	Procedure8.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure8.SequenceNo = 8

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure9
on	Procedure9.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure9.SequenceNo = 9

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure10
on	Procedure10.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure10.SequenceNo = 10

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure11
on	Procedure11.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure11.SequenceNo = 11

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure12
on	Procedure12.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure12.SequenceNo = 12

left join [$(TraffordWarehouse)].dbo.OPProcedure Procedure13
on	Procedure13.OPSourceUniqueID = Encounter.SourceUniqueID
and	Procedure13.SequenceNo = 13

left join [$(TraffordReferenceData)].dbo.ReferralReason
on	ReferralReason.answer_id = Encounter.SourceReferralReasonCode

) Encounter

--left join [$(Warehouse)].OP.HRG4Encounter HRG4Encounter
--on	HRG4Encounter.EncounterRecno = Encounter.EncounterRecno

--left join [$(Warehouse)].WH.RTTStatus RTTStatus
--on	RTTStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode commented out until can source RTTStatusCode

--left join RTT.dbo.RTTOPClockStop RTTOPClockStop
--on	RTTOPClockStop.SourcePatientNo = Encounter.SourcePatientNo
--and	RTTOPClockStop.SourceEntityRecno = Encounter.SourceEncounterNo
--and	RTTOPClockStop.ClockStopDate is not null
--and	not exists
--	(
--	select
--		1
--	from
--		RTT.dbo.RTTOPClockStop Previous
--	where
--		Previous.SourcePatientNo = Encounter.SourcePatientNo
--	and Previous.SourceEntityRecno = Encounter.SourceEncounterNo
--	and	Previous.ClockStopDate is not null
--	and	(
--			coalesce(Previous.Updated, Previous.Created) > coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
--		or	(
--				coalesce(Previous.Updated, Previous.Created) = coalesce(RTTOPClockStop.Updated, RTTOPClockStop.Created)
--			and	Previous.RTTClockStopRecno > RTTOPClockStop.RTTClockStopRecno
--			)
--		)
--	)

left join [$(Organisation)].dbo.CCGPostcode PostcodeAtAppointment
on	PostcodeAtAppointment.Postcode =
		case
		when len(Encounter.PostcodeAtAppointment) = 8 then Encounter.PostcodeAtAppointment
		else left(Encounter.PostcodeAtAppointment, 3) + ' ' + right(Encounter.PostcodeAtAppointment, 4) 
		end

where
	Encounter.AppointmentDate >= '1 Apr 2009'















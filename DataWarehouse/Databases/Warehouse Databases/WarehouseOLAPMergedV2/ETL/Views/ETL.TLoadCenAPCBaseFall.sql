﻿

CREATE view [ETL].[TLoadCenAPCBaseFall]

as

select
	[FallRecno]
	,[SourceUniqueID]
	,[SourcePatientNo]
	,[SourceSpellNo]
	,[FallDate]
	,[FallTime]
	,InitialSeverityID
	,[Validated]
	,[OccuredOnAnotherWard]
	,[WardCode]
	,[InterfaceCode]
	,ContextCode = 'CEN||BEDMAN'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[APC].[Fall]

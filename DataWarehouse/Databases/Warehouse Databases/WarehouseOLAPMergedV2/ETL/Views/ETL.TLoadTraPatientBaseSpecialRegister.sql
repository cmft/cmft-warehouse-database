﻿




CREATE view [ETL].[TLoadTraPatientBaseSpecialRegister]
as

/****************************************************************************************
	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	20/10/2015	RR				Created
*****************************************************************************************/

select
	SpecialRegisterRecno = SpecialRegisterRecNo
	,SourcePatientNo
	,SpecialRegisterCode
	,Alert
	,DistrictNo
	,NHSNumber
	,EnteredTime
	,LastModifiedTime
	,StartDate
	,EndDate
	,Active
	,ContextCode = 'TRA||UG'
from
	[$(TraffordWarehouse)].Patient.SpecialRegister




﻿






CREATE view [ETL].[TLoadCenPatientBaseDuplication]
as

select
	[PatientDuplicationID] = PatientDuplicationId
	,SourcePatientNo = cast([SourcePatientNo] as varchar(50))
	,[MasterSourcePatientNo]
	,[RegistrationDate]
	,IsEarliestRegistration
	,ContextCode = 'CEN||PAS'
from
	[$(Warehouse)].[PAS].[PatientDuplicationBase]







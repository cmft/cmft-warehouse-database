﻿

CREATE view [ETL].[TLoadCenIUIBaseEncounter]

as 

select
	EncounterRecno
	,DateOfBirth
	,NHSNumber
	,CasenoteNumber
	,PatientName
	,AttemptNumber
	,CommissionerCode
	,TreatmentDate
	,OutcomeID
	,TreatmentID
	,InterfaceCode
	,ContextCode = 'CEN||IUI'
	,Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].IUI.Encounter

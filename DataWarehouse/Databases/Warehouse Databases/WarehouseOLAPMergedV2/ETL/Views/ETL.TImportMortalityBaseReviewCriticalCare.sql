﻿





CREATE View [ETL].[TImportMortalityBaseReviewCriticalCare]

as




Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode 
						,ClassificationGrade
						,RequiresFurtherInvestigation
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode = CAST(PrimaryReviewerCode as varchar(50))
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade
		,RequiresFurtherInvestigation 
		,QuestionNo = row_number() over (partition by SourceUniqueID order by SourceUniqueID)
		,Question
		,Response
		,ContextCode = 'CMFT||MORTARCHV'
	from

		(
		select
			SourceUniqueID
			,SourcePatientNo	
			,EpisodeStartTime	
			,APCContextCode = ContextCode
			,FormTypeCode = FormTypeID
			,ReviewStatusCode = ReviewStatus
			,PrimaryReviewerAssignedTime = ReviewedDate
			,PrimaryReviewerCompletedTime = 
				case
					when ReviewStatus = 10 then ReviewedDate
					else null
				end
			,PrimaryReviewerCode = ReviewedBy
			,SecondaryReviewerAssignedTime = null
			,SecondaryReviewerCompletedTime = null
			,SecondaryReviewerCode = null
			,ClassificationGrade 
			,RequiresFurtherInvestigation = 
					case
						when ClassificationGrade in (2,3) then 1
						else 0
					end 
			
			,FirstConsultantReview = isnull(cast(FirstConsultantReview as varchar(max)),'')
			,SpecialtyGroup = cast(SpecialtyGroup as varchar(max))
			,ConfirmedPrimaryDiagnosis = cast(ConfirmedPrimaryDiagnosis as varchar(max))
			,CauseOfDeath1a = cast(CauseOfDeath1a as varchar(max))
			,CauseOfDeath1b = cast(CauseOfDeath1b as varchar(max))
			,CauseOfDeath1c = cast(CauseOfDeath1c as varchar(max))
			,CauseOfDeath2 = cast(CauseOfDeath2 as varchar(max))
			,CauseOfDeathComment = cast(CauseOfDeathComment as varchar(max))
			,CodedDiagnosis = cast(CodedDiagnosis as varchar(max))
			,AgreeWithStatedCauseOfDeath = cast(AgreeWithStatedCauseOfDeath as varchar(max))
			,HospitalPM = cast(HospitalPM as varchar(max))
			,CoronerInformed = cast(CoronerInformed as varchar(max))
			,CoronerPM = cast(CoronerPM as varchar(max))
			,CoexistingFactor = cast(CoexistingFactor as varchar(max))
			,TimeClinicalDecisionToAdmitMade = cast(TimeClinicalDecisionToAdmitMade as varchar(max))
			,TimePatientAdmittedToCriticalCare = cast(TimePatientAdmittedToCriticalCare as varchar(max))
			,AdmissionStickerUsed = cast(AdmissionStickerUsed as varchar(max))
			,TimeToConsultantReviewComment = cast(TimeToConsultantReviewComment as varchar(max))
			,EvidenceOfClearManagementPlan = cast(EvidenceOfClearManagementPlan as varchar(max))
			,EvidenceOfClearManagementPlanComment = cast(EvidenceOfClearManagementPlanComment as varchar(max))
			,EssentialInvestigationsObtainedWithoutDelay = cast(EssentialInvestigationsObtainedWithoutDelay as varchar(max))
			,EssentialInvestigationsObtainedWithoutDelayComment = cast(EssentialInvestigationsObtainedWithoutDelayComment as varchar(max))
			,InitialManagementStepsAppropriateAndAdequate = cast(InitialManagementStepsAppropriateAndAdequate as varchar(max))
			,InitialManagementStepsAppropriateAndAdequateComment = cast(InitialManagementStepsAppropriateAndAdequateComment as varchar(max))
			,OmissionsInInitialManagement = cast(OmissionsInInitialManagement as varchar(max))
			,OmissionsInInitialManagementComment = cast(OmissionsInInitialManagementComment as varchar(max))
			,PeriodAtrialFibrilation = cast(PeriodAtrialFibrilation as varchar(max))
			,PeriodAtrialFibrilationComment = cast(PeriodAtrialFibrilationComment as varchar(max))
			,AtrialFibrilationAppropriatelyManaged = cast(AtrialFibrilationAppropriatelyManaged as varchar(max))
			,AtrialFibrilationAppropriatelyManagedComment = cast(AtrialFibrilationAppropriatelyManagedComment as varchar(max))
			,EvidenceOfIschemiaInGut = cast(EvidenceOfIschemiaInGut as varchar(max))
			,EvidenceOfIschemiaInGutComment = cast(EvidenceOfIschemiaInGutComment as varchar(max))
			,TherapeuticAntiCoagulation = cast(TherapeuticAntiCoagulation as varchar(max))
			,TherapeuticAntiCoagulationComment = cast(TherapeuticAntiCoagulationComment as varchar(max))
			,CriticalCareRoundsDocumentedEveryDay = cast(CriticalCareRoundsDocumentedEveryDay as varchar(max))
			,CriticalCareRoundsDocumentedEveryDayComment = cast(CriticalCareRoundsDocumentedEveryDayComment as varchar(max))
			,AnyRecognisableMedicationErrors = cast(AnyRecognisableMedicationErrors as varchar(max))
			,AnyRecognisableMedicationErrorsComment = cast(AnyRecognisableMedicationErrorsComment as varchar(max))
			,DelayInDiagnosis = cast(DelayInDiagnosis as varchar(max))
			,DelayInDiagnosisComment = cast(DelayInDiagnosisComment as varchar(max))
			,DelayInDeliveringCare = cast(DelayInDeliveringCare as varchar(max))
			,DelayInOfDeliveringCareComment = cast(DelayInOfDeliveringCareComment as varchar(max))
			,PoorCommunication = cast(PoorCommunication as varchar(max))
			,PoorCommunicationComment = cast(PoorCommunicationComment as varchar(max))
			,AdverseEvents = cast(AdverseEvents as varchar(max))
			,AdverseEventsComment = cast(AdverseEventsComment as varchar(max))
			,AnythingCouldBeDoneDifferently = cast(AnythingCouldBeDoneDifferently as varchar(max))
			,AnythingCouldBeDoneDifferentlyComment = cast(AnythingCouldBeDoneDifferentlyComment as varchar(max))
			,NotableGoodQualityCare = cast(NotableGoodQualityCare as varchar(max))
			,DocumentationIssues = cast(DocumentationIssues as varchar(max))
			,DocumentationIssuesComment = cast(DocumentationIssuesComment as varchar(max))
			,EndOfLifeCareLCPInPatientNotes = cast(EndOfLifeCareLCPInPatientNotes as varchar(max))
			,EndOfLifeCareLCPInPatientNotesComment = cast(EndOfLifeCareLCPInPatientNotesComment as varchar(max))
			,EndOfLifeCarePreferredPlaceOfDeathRecorded = cast(EndOfLifeCarePreferredPlaceOfDeathRecorded as varchar(max))
			,EndOfLifeCarePreferredPlaceOfDeathRecordedComment = cast(EndOfLifeCarePreferredPlaceOfDeathRecordedComment as varchar(max))
			,EndOfLifeCareWishToDieOutsideHospitalExpressed = cast(EndOfLifeCareWishToDieOutsideHospitalExpressed as varchar(max))
			,EndOfLifeCareWishToDieOutsideHospitalExpressedComment = cast(EndOfLifeCareWishToDieOutsideHospitalExpressedComment as varchar(max))
			,LessonsLearned = cast(LessonsLearned as varchar(max))
			,ActionPlan = cast(ActionPlan as varchar(max))
			,DateOfActionPlanReview = cast(DateOfActionPlanReview as varchar(max))
			
			
		from
			[$(Warehouse)].Mortality.CriticalCare
		) 
		Mortality

		unpivot
				(
				Response for Question in 
						(
						FirstConsultantReview
						,SpecialtyGroup
						,ConfirmedPrimaryDiagnosis
						,CauseOfDeath1a
						,CauseOfDeath1b
						,CauseOfDeath1c
						,CauseOfDeath2
						,CauseOfDeathComment
						,CodedDiagnosis
						,AgreeWithStatedCauseOfDeath
						,HospitalPM
						,CoronerInformed
						,CoronerPM
						,CoexistingFactor
						,TimeClinicalDecisionToAdmitMade
						,TimePatientAdmittedToCriticalCare
						,AdmissionStickerUsed
						,TimeToConsultantReviewComment
						,EvidenceOfClearManagementPlan
						,EvidenceOfClearManagementPlanComment
						,EssentialInvestigationsObtainedWithoutDelay
						,EssentialInvestigationsObtainedWithoutDelayComment
						,InitialManagementStepsAppropriateAndAdequate
						,InitialManagementStepsAppropriateAndAdequateComment
						,OmissionsInInitialManagement
						,OmissionsInInitialManagementComment
						,PeriodAtrialFibrilation
						,PeriodAtrialFibrilationComment
						,AtrialFibrilationAppropriatelyManaged
						,AtrialFibrilationAppropriatelyManagedComment
						,EvidenceOfIschemiaInGut
						,EvidenceOfIschemiaInGutComment
						,TherapeuticAntiCoagulation
						,TherapeuticAntiCoagulationComment
						,CriticalCareRoundsDocumentedEveryDay
						,CriticalCareRoundsDocumentedEveryDayComment
						,AnyRecognisableMedicationErrors
						,AnyRecognisableMedicationErrorsComment
						,DelayInDiagnosis
						,DelayInDiagnosisComment
						,DelayInDeliveringCare
						,DelayInOfDeliveringCareComment
						,PoorCommunication
						,PoorCommunicationComment
						,AdverseEvents
						,AdverseEventsComment
						,AnythingCouldBeDoneDifferently
						,AnythingCouldBeDoneDifferentlyComment
						,NotableGoodQualityCare
						,DocumentationIssues
						,DocumentationIssuesComment
						,EndOfLifeCareLCPInPatientNotes
						,EndOfLifeCareLCPInPatientNotesComment
						,EndOfLifeCarePreferredPlaceOfDeathRecorded
						,EndOfLifeCarePreferredPlaceOfDeathRecordedComment
						,EndOfLifeCareWishToDieOutsideHospitalExpressed
						,EndOfLifeCareWishToDieOutsideHospitalExpressedComment
						,LessonsLearned
						,ActionPlan
						,DateOfActionPlanReview
						)
				) 
				Response 
			)
			CriticalCare





﻿


CREATE view [ETL].[TLoadPEXBaseFriendsFamilyTestReturn]

as


select
	[FFTRecno]
	,[Return]
	,CensusDate
	,[HospitalSiteCode]
	,[HospitalSite]
	,[WardCode]
	,[Ward]
	,[1ExtremelyLikely]
	,[2Likely]
	,[3NeitherLikelyNorUnlikely]
	,[4Unlikely]
	,[5ExtremelyUnlikely]
	,[6DontKnow]
	,[Responses]
	,[EligibleResponders]
	,[InterfaceCode]
	,[Created]
	,[Updated]
	,[ByWhom]
	,ContextCode = 'CMFT||FFTRTN'

from
	[$(Warehouse)].[PEX].[FriendsFamilyTestReturn]





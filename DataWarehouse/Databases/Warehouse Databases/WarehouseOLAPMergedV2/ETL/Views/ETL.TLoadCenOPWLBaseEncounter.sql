﻿



CREATE view [ETL].[TLoadCenOPWLBaseEncounter] as	           

SELECT
	 AdditionFlag
	,AdminCategoryCode
	,AdmissionMethodCode
	,AppointmentCategoryCode
	,AppointmentDate
	,AppointmentDoctor
	,AppointmentID
	,AppointmentStatusCode
	,AppointmentTime
	,AppointmentTypeCode
	,BookedDate
	,BookingTypeCode
	,BreachDate
	,BreachDays
	,BreachTypeCode
	,CancelledBy
	,CasenoteNumber
	,CensusDate
	,ClinicCode
	,ClockStartDate
	,CommentClinical
	,CommentNonClinical
	,ContextCode
	,ContractSerialNumber
	,CountOfCNDs
	,CountOfDaysSuspended
	,CountOfDNAs
	,CountOfHospitalCancels
	,CountOfPatientCancels
	,DateOfBirth
	,DateOfDeath
	,DateOnWaitingList
	,DerivedBreachDate
	,DerivedBreachDays
	,DerivedClockStartDate
	,DHACode
	,DistrictNo
	,EpisodeNo
	,EpisodicGpCode
	,EncounterRecno
	,EthnicOriginCode
	,FirstAttendanceCode
	,ExpectedAdmissionDate
	,FutureCancellationDate
	,FuturePatientCancelDate
	,HomePhone
	,IntendedPrimaryOperationCode
	,InterfaceCode
	,LastAppointmentFlag
	,LocalEpisodicGpCode
	,LocalRegisteredGpCode
	,ManagementIntentionCode
	,MaritalStatusCode
	,MRSA
	,NationalBreachDate
	,NationalBreachDays
	,NationalDiagnosticBreachDate
	,NationalSpecialtyCode
	,NextOfKinHomePhone
	,NextOfKinName
	,NextOfKinRelationship
	,NextOfKinWorkPhone
	,NHSNumber
	,Operation
	,OriginalDateOnWaitingList
	,PASSpecialtyCode
	,PatientChoice
	,PatientDeathIndicator
	,PatientForename
	,PatientAddress1 = PatientsAddress1
	,PatientAddress2 = PatientsAddress2
	,PatientAddress3 = PatientsAddress3
	,PatientAddress4 = PatientsAddress4
	,PatientSurname
	,PatientTitle
	,PCTCode
	,Postcode
	,PriorityCode
	,ProviderCode
	,PurchaserCode
	,QM08EndWaitDate
	,QM08StartWaitDate
	,ReferralDate
	,RegisteredGpCode
	,ReligionCode
	,ReportCategoryCode
	,RTTBreachDate
	,RTTCurrentPrivatePatientFlag
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTDiagnosticBreachDate
	,RTTEndDate
	,RTTOverseasStatusFlag
	,RTTPathwayCondition
	,RTTPathwayID
	,RTTSpecialtyCode
	,RTTStartDate
	,SexCode
	,SiteCode
	,SourceEncounterNo
	,SourceOfReferralCode
	,SourcePatientNo
	,SourceTreatmentFunctionCode
	,SourceUniqueID
	,SpecialtyTypeCode
	,SuspensionEndDate
	,SuspensionReason
	,SuspensionReasonCode
	,SuspensionStartDate
	,TreatmentFunctionCode
	,WaitingListCode
	,WardCode
	,WeeksWaiting
	,WLStatus
	,WorkPhone
	,Cases = 1
	,ConsultantCode = coalesce(Encounter.ConsultantCode, 'N/A')
	,SpecialtyCode = coalesce(Encounter.SpecialtyCode, 'N/A')

	,AgeCode =
		case
		when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
		when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
		when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
		when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
		datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
			right('0' + Convert(VARCHAR,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
		when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
		datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
				< 1 then 'Between 28 Days and 1 Month'
		when datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
			datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(VARCHAR,datediff(month, DateOfBirth,Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(VARCHAR, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
		(
			
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		)), 2) + ' Years'

		end
		
	,TCIDate		
	,LengthOfWait
	
	,DurationCode =
		coalesce(
			right('00' +
			convert(VARCHAR, 
			case
				when convert(INT, Encounter.KornerWait /7 ) > 207 then 208
				else convert(INT, Encounter.KornerWait /7 )
			end), 3)
			,'NA'
		)

	,RegisteredGpPracticeCode = coalesce(Encounter.RegisteredPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999')
	,EpisodicGpPracticeCode = coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredPracticeCode, 'X99999')
			  
		      
	,WaitTypeCode =
		case
		when AppointmentTypeCode IN('5','05') then '60'
		when WLAppointmentTypeCode IN('5','05') then '60'
		--when Encounter.WLStatus = 'WL Suspend' then '30'
		when Encounter.AdminCategoryCode = 'Pay' then '70'
		else '50'
		end

	,StatusCode =
		case
		when Encounter.WLStatus = 'WL Suspend' then '20'
		else '10'
		end

	,CategoryCode =
		case
		when rtrim(Encounter.ManagementIntentionCode) = 'I' then '10'
		else '20'
		end
		
	,OPCSCoded =
		case
		when Encounter.IntendedPrimaryOperationCode is null	then 0
		else 1
		end

	,WithRTTStartDate =
		case
		when Encounter.RTTStartDate is null	then 0
		else 1
		end

	,WithRTTStatusCode =
		case
		when Encounter.RTTCurrentStatusCode in ('10', '20') then 1
		else 0
		end

	,WithExpectedAdmissionDate =
		case
		when Encounter.ExpectedAdmissionDate is null then 0
		else 1
		end
				
	,RTTActivity =
		convert(
			bit
			,0
			--,case
			--when
			--	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
			--and	Encounter.SpecialtyCode not in 
			--	(
			--	SELECT
			--		SpecialtyCode
			--	FROM
			--		RTT.dbo.ExcludedSpecialty
			--	)

			---- 'GENE' ?
			--and	Encounter.ConsultantCode <> 'FRAC'

			----remove ICAT
			--and	Encounter.SiteCode != 'FMSK'

			--and	not exists
			--	(
			--	SELECT
			--		1
			--	FROM
			--		RTT.dbo.RTTClockStart ClockChangeReason
			--	WHERE
			--		ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
			--	and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
			--	and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
			--	)
			--then 1
			--else 0
			--end
		)



	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null then 'U'
		when Encounter.RTTBreachDate < Encounter.AppointmentDate then 'B'
		else 'N'
		end

	,DiagnosticProcedure =
		convert(
			bit
			,case
			when DiagnosticProcedure.ProcedureCode is null then 0
			else 1
			end
		)
				
	,DirectorateCode = coalesce(Encounter.DirectorateCode , 'N/A')

	,WithAppointmentDate =
		case
		when AppointmentDate is null then 0
		else 1
		end
			
	,WithRTTOpenPathway
	,RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode 
	,RTTDaysWaiting 


	,DurationAtAppointmentDateCode =
		case
		when Encounter.AppointmentDate is null then 'NA'
		else right('00' +
				convert(varchar, 
					case
					when convert(int, (Encounter.LengthOfWait + datediff(day , CensusDate , AppointmentDate)) /7 ) > 207 then 208
					else convert(int, (Encounter.LengthOfWait + datediff(day , CensusDate , AppointmentDate)) /7 )
					end
				)
				,3
			)
		end

	,Encounter.CCGCode
	,Encounter.ResidenceCCGCode
	
	,Encounter.DataSourceID

FROM 
	(
	select
		 EncounterRecno
		,SourcePatientNo
		,SourceEncounterNo
		,CensusDate
		,PatientSurname
		,PatientForename
		,PatientTitle
		,SexCode
		,DateOfBirth
		,DateOfDeath
		,NHSNumber
		,DistrictNo
		,MaritalStatusCode
		,ReligionCode
		,Encounter.Postcode
		,PatientsAddress1
		,PatientsAddress2
		,PatientsAddress3
		,PatientsAddress4
		,DHACode
		,HomePhone
		,WorkPhone
		,EthnicOriginCode
		,ConsultantCode
		,SpecialtyCode
		,PASSpecialtyCode
		,ManagementIntentionCode
		,AdmissionMethodCode
		,PriorityCode
		,WaitingListCode
		,CommentClinical
		,CommentNonClinical
		,IntendedPrimaryOperationCode
		,Operation
		,SiteCode
		,WardCode
		,WLStatus
		,ProviderCode
		,PurchaserCode
		,ContractSerialNumber
		,CancelledBy
		,BookingTypeCode
		,CasenoteNumber

		,OriginalDateOnWaitingList = 
			ReferralDate

		,DateOnWaitingList = 
			QM08StartWaitDate

		,TCIDate
		,KornerWait

		,LengthOfWait =
			convert(INT, Encounter.KornerWait)

		,CountOfDaysSuspended
		,SuspensionStartDate
		,SuspensionEndDate
		,SuspensionReasonCode
		,SuspensionReason
		,InterfaceCode
		,EpisodicGpCode = 
			EpisodicGP.NationalCode

		,EpisodicGpPracticeCode
		,RegisteredGpCode = 
			RegisteredGP.NationalCode

		,RegisteredPracticeCode
		,SourceTreatmentFunctionCode
		,TreatmentFunctionCode
		,NationalSpecialtyCode
		,PCTCode
		,BreachDate
		,ExpectedAdmissionDate
		,ReferralDate
		,BookedDate

		,AppointmentDate =
			case
			when AppointmentDate < (select min(TheDate) from WH.CalendarBase where TheDate > '2 Jan 1900')
			then null
			when AppointmentDate < CensusDate
			then null
			else AppointmentDate
			end

		,AppointmentTypeCode
		,AppointmentStatusCode
		,AppointmentCategoryCode
		,QM08StartWaitDate
		,QM08EndWaitDate
		,AppointmentTime
		,ClinicCode
		,SourceOfReferralCode
		,FuturePatientCancelDate
		,AdditionFlag
		,LastAppointmentFlag
		,LocalRegisteredGpCode
		,LocalEpisodicGpCode
		,NextOfKinName
		,NextOfKinRelationship
		,NextOfKinHomePhone
		,NextOfKinWorkPhone
		,FutureCancellationDate
		,EpisodeNo
		,BreachDays
		,MRSA
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,NationalBreachDate
		,NationalBreachDays
		,DerivedBreachDays
		,DerivedClockStartDate
		,DerivedBreachDate
		,RTTBreachDate
		,RTTDiagnosticBreachDate
		,NationalDiagnosticBreachDate
		,BreachTypeCode
		,CountOfDNAs
		,CountOfHospitalCancels
		,CountOfPatientCancels
		,AppointmentID
		,SourceUniqueID
		,AdminCategoryCode
		,ClockStartDate
		,PatientDeathIndicator
		,AppointmentDoctor
		,PatientChoice
		,CountOfCNDs
		,ReportCategoryCode
		,DirectorateCode
		,SpecialtyTypeCode
		,WeeksWaiting
		,FirstAttendanceCode = 1
		,WithRTTOpenPathway = 
			coalesce (
					WithRTTOpenPathway
					,0
			)	
		,RTTPathwayStartDateCurrent
		,RTTWeekBandReturnCode 
		,RTTDaysWaiting 
		,WLAppointmentTypeCode
		,ContextCode = 'CEN||PAS'

		,CCGCode =
				coalesce(
					 CCGPractice.ParentOrganisationCode
					,CCGPostcode.CCGCode
					,'00W'
				)

		,ResidenceCCGCode = CCGPostcode.CCGCode
		
		,DataSourceID = 1 --[$(Warehouse)].OP.WaitingList
	from
		[$(Warehouse)].OP.PTL Encounter

	left join [$(Warehouse)].PAS.Gp RegisteredGP
	on	RegisteredGP.GpCode = Encounter.RegisteredGpCode

	left join [$(Warehouse)].PAS.Gp EpisodicGP
	on	EpisodicGP.GpCode = Encounter.EpisodicGpCode

	left join [$(Organisation)].dbo.CCGPractice
	on	CCGPractice.OrganisationCode =
			coalesce(
				 Encounter.EpisodicGpPracticeCode
				,Encounter.RegisteredPracticeCode
			)

	left join [$(Organisation)].dbo.CCGPostcode
	on	CCGPostcode.Postcode = 
			case
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
			else Encounter.Postcode
			end

	union all

	select
		 EncounterRecno
		,SourcePatientNo
		,SourceEncounterNo
		,Encounter.CensusDate
		,PatientSurname
		,PatientForename
		,PatientTitle
		,SexCode
		,DateOfBirth
		,DateOfDeath
		,NHSNumber
		,DistrictNo
		,MaritalStatusCode
		,ReligionCode
		,Encounter.Postcode
		,PatientsAddress1
		,PatientsAddress2
		,PatientsAddress3
		,PatientsAddress4
		,DHACode
		,HomePhone
		,WorkPhone
		,EthnicOriginCode
		,ConsultantCode
		,SpecialtyCode
		,PASSpecialtyCode
		,ManagementIntentionCode
		,AdmissionMethodCode
		,PriorityCode
		,WaitingListCode
		,CommentClinical
		,CommentNonClinical
		,IntendedPrimaryOperationCode
		,Operation
		,SiteCode
		,WardCode
		,WLStatus
		,ProviderCode
		,PurchaserCode
		,ContractSerialNumber
		,CancelledBy
		,BookingTypeCode
		,CasenoteNumber

		,OriginalDateOnWaitingList = 
			ReferralDate

		,DateOnWaitingList = 
			QM08StartWaitDate

		,TCIDate
		,KornerWait

		,LengthOfWait =
			convert(INT, Encounter.KornerWait)

		,CountOfDaysSuspended
		,SuspensionStartDate
		,SuspensionEndDate
		,SuspensionReasonCode
		,SuspensionReason
		,InterfaceCode
		,EpisodicGpCode = 
			EpisodicGP.NationalCode

		,EpisodicGpPracticeCode
		,RegisteredGpCode = 
			RegisteredGP.NationalCode

		,RegisteredPracticeCode
		,SourceTreatmentFunctionCode
		,TreatmentFunctionCode
		,NationalSpecialtyCode
		,PCTCode
		,BreachDate
		,ExpectedAdmissionDate
		,ReferralDate
		,BookedDate

		,AppointmentDate =
			case
			when AppointmentDate < (select min(TheDate) from WH.CalendarBase where TheDate > '2 Jan 1900')
			then null
			when AppointmentDate < Encounter.CensusDate
			then null
			else AppointmentDate
			end

		,AppointmentTypeCode
		,AppointmentStatusCode
		,AppointmentCategoryCode
		,QM08StartWaitDate
		,QM08EndWaitDate
		,AppointmentTime
		,ClinicCode
		,SourceOfReferralCode
		,FuturePatientCancelDate
		,AdditionFlag
		,LastAppointmentFlag
		,LocalRegisteredGpCode
		,LocalEpisodicGpCode
		,NextOfKinName
		,NextOfKinRelationship
		,NextOfKinHomePhone
		,NextOfKinWorkPhone
		,FutureCancellationDate
		,EpisodeNo
		,BreachDays
		,MRSA
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,NationalBreachDate
		,NationalBreachDays
		,DerivedBreachDays
		,DerivedClockStartDate
		,DerivedBreachDate
		,RTTBreachDate
		,RTTDiagnosticBreachDate
		,NationalDiagnosticBreachDate
		,BreachTypeCode
		,CountOfDNAs
		,CountOfHospitalCancels
		,CountOfPatientCancels
		,AppointmentID
		,SourceUniqueID
		,AdminCategoryCode
		,ClockStartDate
		,PatientDeathIndicator
		,AppointmentDoctor
		,PatientChoice
		,CountOfCNDs
		,ReportCategoryCode
		,DirectorateCode
		,SpecialtyTypeCode
		,WeeksWaiting
		,FirstAttendanceCode = 2
		,WithRTTOpenPathway = 
			coalesce (
					WithRTTOpenPathway
					,0
			)	
		,RTTPathwayStartDateCurrent
		,RTTWeekBandReturnCode 
		,RTTDaysWaiting 
		,WLAppointmentTypeCode
		,ContextCode = 'CEN||PAS'

		,CCGCode =
				coalesce(
					 CCGPractice.ParentOrganisationCode
					,CCGPostcode.CCGCode
					,'00W'
				)

		,ResidenceCCGCode = CCGPostcode.CCGCode
		
		,DataSourceID = 1 --[$(Warehouse)].OP.WaitingList
	from
		[$(Warehouse)].OP.WL Encounter

	left join [$(Warehouse)].PAS.Gp RegisteredGP
	on	RegisteredGP.GpCode = Encounter.RegisteredGpCode

	left join [$(Warehouse)].PAS.Gp EpisodicGP
	on	EpisodicGP.GpCode = Encounter.EpisodicGpCode

	left join [$(Organisation)].dbo.CCGPractice
	on	CCGPractice.OrganisationCode =
			coalesce(
				 Encounter.EpisodicGpPracticeCode
				,Encounter.RegisteredPracticeCode
			)

	left join [$(Organisation)].dbo.CCGPostcode
	on	CCGPostcode.Postcode = 
			case
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
			else Encounter.Postcode
			end

	where
	
--where the patient is not new (i.e. not in [$(Warehouse)].OP.PTL)
		not exists
		(
		select
			1
		from
			[$(Warehouse)].OP.PTL
		where
			PTL.SourcePatientNo = Encounter.SourcePatientNo
		and	PTL.SourceEncounterNo = Encounter.SourceEncounterNo
		and	PTL.QM08StartWaitDate = Encounter.QM08StartWaitDate
		and	PTL.CensusDate = Encounter.CensusDate
		)
		
--follow-ups are the latest (based on DateOnWaitingList) record in [$(Warehouse)].OP.WL
	and	not exists
		(
		select
			1
		from
			[$(Warehouse)].OP.WL Previous
		where
			Previous.SourcePatientNo = Encounter.SourcePatientNo
		and	Previous.SourceEncounterNo = Encounter.SourceEncounterNo
		and	Previous.CensusDate = Encounter.CensusDate

		and	(
				Previous.QM08StartWaitDate > Encounter.QM08StartWaitDate
			or	(
					Previous.QM08StartWaitDate = Encounter.QM08StartWaitDate
				and	Previous.EncounterRecno > Encounter.EncounterRecno
				)
			)

		)
	
	union all

	select
		 EncounterRecno
		,SourcePatientNo
		,SourceEncounterNo
		,Encounter.CensusDate
		,PatientSurname
		,PatientForename
		,PatientTitle
		,SexCode
		,DateOfBirth
		,DateOfDeath
		,NHSNumber
		,DistrictNo
		,MaritalStatusCode
		,ReligionCode
		,Encounter.Postcode
		,PatientsAddress1
		,PatientsAddress2
		,PatientsAddress3
		,PatientsAddress4
		,DHACode
		,HomePhone
		,WorkPhone
		,EthnicOriginCode
		,ConsultantCode
		,SpecialtyCode
		,PASSpecialtyCode
		,ManagementIntentionCode
		,AdmissionMethodCode
		,PriorityCode
		,WaitingListCode
		,CommentClinical
		,CommentNonClinical
		,IntendedPrimaryOperationCode
		,Operation
		,SiteCode
		,WardCode
		,WLStatus
		,ProviderCode
		,PurchaserCode
		,ContractSerialNumber
		,CancelledBy
		,BookingTypeCode
		,CasenoteNumber

		,OriginalDateOnWaitingList = 
			ReferralDate

		,DateOnWaitingList = 
			QM08StartWaitDate

		,TCIDate
		,KornerWait

		,LengthOfWait =
			convert(INT, Encounter.KornerWait)

		,CountOfDaysSuspended
		,SuspensionStartDate
		,SuspensionEndDate
		,SuspensionReasonCode
		,SuspensionReason
		,InterfaceCode
		,EpisodicGpCode = 
			EpisodicGP.NationalCode

		,EpisodicGpPracticeCode
		,RegisteredGpCode = 
			RegisteredGP.NationalCode

		,RegisteredPracticeCode
		,SourceTreatmentFunctionCode
		,TreatmentFunctionCode
		,NationalSpecialtyCode
		,PCTCode
		,BreachDate
		,ExpectedAdmissionDate
		,ReferralDate
		,BookedDate

		,AppointmentDate =
			case
			when AppointmentDate < (select min(TheDate) from WH.CalendarBase where TheDate > '2 Jan 1900')
			then null
			when AppointmentDate < Encounter.CensusDate
			then null
			else AppointmentDate
			end

		,AppointmentTypeCode
		,AppointmentStatusCode
		,AppointmentCategoryCode
		,QM08StartWaitDate
		,QM08EndWaitDate
		,AppointmentTime
		,ClinicCode
		,SourceOfReferralCode
		,FuturePatientCancelDate
		,AdditionFlag
		,LastAppointmentFlag
		,LocalRegisteredGpCode
		,LocalEpisodicGpCode
		,NextOfKinName
		,NextOfKinRelationship
		,NextOfKinHomePhone
		,NextOfKinWorkPhone
		,FutureCancellationDate
		,EpisodeNo
		,BreachDays
		,MRSA
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,NationalBreachDate
		,NationalBreachDays
		,DerivedBreachDays
		,DerivedClockStartDate
		,DerivedBreachDate
		,RTTBreachDate
		,RTTDiagnosticBreachDate
		,NationalDiagnosticBreachDate
		,BreachTypeCode
		,CountOfDNAs
		,CountOfHospitalCancels
		,CountOfPatientCancels
		,AppointmentID
		,SourceUniqueID
		,AdminCategoryCode
		,ClockStartDate
		,PatientDeathIndicator
		,AppointmentDoctor
		,PatientChoice
		,CountOfCNDs
		,ReportCategoryCode
		,DirectorateCode
		,SpecialtyTypeCode
		,WeeksWaiting =	convert(int, coalesce(Encounter.KornerWait, 0) / 7)
		,FirstAttendanceCode = 2
		,WithRTTOpenPathway = 
			coalesce (
					WithRTTOpenPathway
					,0
			)	
		,RTTPathwayStartDateCurrent
		,RTTWeekBandReturnCode 
		,RTTDaysWaiting 
		,WLAppointmentTypeCode
		,ContextCode = 'CEN||PAS'

		,CCGCode =
				coalesce(
					 CCGPractice.ParentOrganisationCode
					,CCGPostcode.CCGCode
					,'00W'
				)

		,ResidenceCCGCode = CCGPostcode.CCGCode
		
		,DataSourceID = 2 --[$(Warehouse)].OP.WaitingListReview
	from
		[$(Warehouse)].OP.WaitingListReview Encounter

	left join [$(Warehouse)].PAS.Gp RegisteredGP
	on	RegisteredGP.GpCode = Encounter.RegisteredGpCode

	left join [$(Warehouse)].PAS.Gp EpisodicGP
	on	EpisodicGP.GpCode = Encounter.EpisodicGpCode

	left join [$(Organisation)].dbo.CCGPractice
	on	CCGPractice.OrganisationCode =
			coalesce(
				 Encounter.EpisodicGpPracticeCode
				,Encounter.RegisteredPracticeCode
			)

	left join [$(Organisation)].dbo.CCGPostcode
	on	CCGPostcode.Postcode = 
			case
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
			else Encounter.Postcode
			end

	where
	
--follow-ups are the latest (based on DateOnWaitingList) record in [$(Warehouse)].OP.WL
		not exists
		(
		select
			1
		from
			[$(Warehouse)].OP.WaitingListReview Previous
		where
			Previous.SourcePatientNo = Encounter.SourcePatientNo
		and	Previous.SourceEncounterNo = Encounter.SourceEncounterNo
		and	Previous.CensusDate = Encounter.CensusDate

		and	(
				Previous.QM08StartWaitDate > Encounter.QM08StartWaitDate
			or	(
					Previous.QM08StartWaitDate = Encounter.QM08StartWaitDate
				and	Previous.EncounterRecno > Encounter.EncounterRecno
				)
			)

		)
		
	
	) Encounter

left join 
	(
	SELECT
		 SpecialtyCode = EntityCode
		,WaitingListAddMinutes = convert(INT, Description)
	FROM
		[$(Warehouse)].dbo.EntityLookup
	WHERE
		EntityTypeCode = 'WLADDMINUTES'
	) WaitingListAddMinutes
ON	WaitingListAddMinutes.SpecialtyCode = Encounter.SpecialtyCode

left join [$(Warehouse)].dbo.DiagnosticProcedure
ON	DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode



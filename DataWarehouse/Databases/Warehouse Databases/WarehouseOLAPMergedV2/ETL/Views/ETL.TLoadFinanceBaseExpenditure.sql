﻿





CREATE view [ETL].[TLoadFinanceBaseExpenditure]

as

select
	ExpenditureRecno
	,[CensusDate]
	,Division
	,Budget
	,AnnualBudget
	,Actual
	,[InterfaceCode]
	,ContextCode = 'CMFT||ORCL'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].Finance.Expenditure


/*
Insert into WH.ContextBase (ContextCode,Context,Location)
Select 'CMFT||ORCL','Oracle (CMFT)','CMFT'

Also added to Reference Map - Edit, Contexts, add
*/


﻿








/****************************************************************************************

20140731	RR	Added field Reportable = 1 below, so that it can be inserted into the APC.BaseEncounter table with a default of 1
20150722	RR	Due to Mortality process changing, discussed with DG and removed from APC

*****************************************************************************************/


CREATE view [ETL].[TLoadTraAPCBaseEncounter] as

select
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID

	,SourcePatientNo = Encounter.EPMINumber
	,SourceSpellNo = null
	,SourceEncounterNo = EpisodeNo

	,Encounter.ProviderSpellNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath

	,SexCode = 
		Encounter.SexCode
	--=
		--case Encounter.SexCode
		--when '' then '0'
		--when 'M' then '1'
		--when 'F' then '2'
		--when 'I' then '9'
		--else '#'
		--end

	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4

	,DHACode = null

	,EthnicOriginCode = Encounter.EthnicCategoryCode

	,Encounter.MaritalStatusCode

	,ReligionCode =
		coalesce(
			Encounter.ReligionCode
			,'-1'
				)

	,DateOnWaitingList = cast(null as date)

	,AdmissionDate =
		cast(Encounter.AdmissionDate as date)

	,DischargeDate =
		cast(Encounter.DischargeDate as date)

	,EpisodeStartDate =
		cast(Encounter.EpisodeStartDate as date)

	,EpisodeEndDate =
		cast(Encounter.EpisodeEndDate as date)

	,Encounter.StartSiteCode
	,Encounter.StartWardTypeCode
	,EndSiteCode = Encounter.EndSiteCode
	,Encounter.EndWardTypeCode
	,Encounter.RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode, 'V81999')

	,SiteCode =
		Encounter.StartSiteCode

	,AdmissionMethodCode = 
		Encounter.SourceAdmissionMethodCode

	,AdmissionSourceCode = 
		Encounter.SourceAdmissionSourceCode

	,Encounter.PatientClassificationCode

	,ManagementIntentionCode = 
		Encounter.SourceManagementIntentionCode

	,DischargeMethodCode = 
		Encounter.SourceDischargeMethodCode

	,DischargeDestinationCode = 
		Encounter.SourceDischargeDestinationCode

	,AdminCategoryCode = 
		Encounter.SourceAdminCategoryCode

	,Encounter.ConsultantCode

	,SpecialtyCode = Encounter.TreatmentFunctionCode

	,Encounter.LastEpisodeInSpellIndicator

	,FirstRegDayOrNightAdmit =
		Encounter.FirstRegularDayNightAdmissionFlag

	,NeonatalLevelOfCare =
		Encounter.NeonatalLevelOfCareCode

	,Encounter.PASHRGCode
	,PrimaryDiagnosisCode = replace(Encounter.PrimaryDiagnosisCode, '..', '.')

	--Gareth Cunnah removed as cutting off codes 20/08/2012	
	--,PrimaryDiagnosisCode =
	--	coalesce(rtrim(left(Encounter.PrimaryDiagnosisCode, 5)), '##')

	,SubsidiaryDiagnosisCode = null

	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.SecondaryDiagnosisCode13

	,Encounter.PrimaryProcedureCode

	--Gareth Cunnah removed as cutting off codes 20/08/2012	
	--,PrimaryProcedureCode =
	--	coalesce(rtrim(left(Encounter.PrimaryProcedureCode, 5)), '##')

	,Encounter.PrimaryProcedureDate

	,Encounter.SecondaryProcedureCode1
	,Encounter.SecondaryProcedureDate1
	,Encounter.SecondaryProcedureCode2
	,Encounter.SecondaryProcedureDate2
	,Encounter.SecondaryProcedureCode3
	,Encounter.SecondaryProcedureDate3
	,Encounter.SecondaryProcedureCode4
	,Encounter.SecondaryProcedureDate4
	,Encounter.SecondaryProcedureCode5
	,Encounter.SecondaryProcedureDate5
	,Encounter.SecondaryProcedureCode6
	,Encounter.SecondaryProcedureDate6
	,Encounter.SecondaryProcedureCode7
	,Encounter.SecondaryProcedureDate7
	,Encounter.SecondaryProcedureCode8
	,Encounter.SecondaryProcedureDate8
	,Encounter.SecondaryProcedureCode9
	,Encounter.SecondaryProcedureDate9
	,Encounter.SecondaryProcedureCode10
	,Encounter.SecondaryProcedureDate10
	,Encounter.SecondaryProcedureCode11
	,Encounter.SecondaryProcedureDate11
	,Encounter.OperationStatusCode

	,ContractSerialNo = Encounter.CommissioningSerialNo
	,CodingCompleteDate = Encounter.ClinicalCodingCompleteDate

	,PurchaserCode = Encounter.CommissionerCode

	,Encounter.ProviderCode

	,EpisodeStartTime =
		Encounter.EpisodeStartDate

	,EpisodeEndTime =
		Encounter.EpisodeEndDate

	,RegisteredGdpCode = null
	,Encounter.EpisodicGpCode

	,Encounter.InterfaceCode

	,Encounter.CasenoteNumber

	,NHSNumberStatusCode =
		Encounter.NHSNumberStatusId

	,AdmissionTime =
		Encounter.AdmissionDate

	,DischargeTime =
		Encounter.DischargeDate

	,TransferFrom = null

	,Encounter.DistrictNo

	,ExpectedLOS =
		datediff(day , AdmissionDate , PredictedDischargeDate)

	,MRSAFlag = null

	,RTTPathwayID = 
		Encounter.PathwayId

	,RTTPathwayCondition = null

	,Encounter.RTTStartDate
	,Encounter.RTTEndDate

	,RTTSpecialtyCode = null
	,RTTCurrentProviderCode = null
	,RTTCurrentStatusCode = null
	,RTTCurrentStatusDate = CAST(null as date)
	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null

	,RTTPeriodStatusCode =
		Encounter.RTTStatusCode

	,IntendedPrimaryProcedureCode = null
	,Operation = null
	,Research1 = null
	,CancelledElectiveAdmissionFlag = null
	,LocalAdminCategoryCode = null

	,EpisodicGpPracticeCode =
		coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredGpPracticeCode, 'V81999')

	,Encounter.PCTCode
	,LocalityCode = null

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 1 and
	datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.EpisodeStartDate)), 2) + ' Days'

	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 28 and
	datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,HRGCode = HRG4Encounter.HRGCode

	,SpellHRGCode = HRG4Spell.HRGCode

	,CategoryCode =
		case
		when Encounter.ManagementIntentionCode = 'I' then '10'
		when Encounter.ManagementIntentionCode = 'D' then '20'
		when Encounter.ManagementIntentionCode = 'R' then '24'
		else '28'
		end

	,LOE =
		datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)

	,LOS =
	datediff(
		 day
		,Encounter.AdmissionDate
		,coalesce(
			 Encounter.DischargeDate
			,(
			select
				max(DischargeDate)
			from
				[$(Warehouse)].APC.Encounter
			)
		)
	)

	,Cases = 1

	,FirstEpisodeInSpellIndicator =
		case
		when not exists
		(
		select
			1
		from
			[$(TraffordWarehouse)].dbo.APCEncounter Previous
		where
			Previous.ProviderSpellNo = Encounter.ProviderSpellNo
		and	(
				Previous.EpisodeStartDate < Encounter.EpisodeStartDate
			or	(
					Previous.EpisodeStartDate = Encounter.EpisodeStartDate
				and	Previous.EpisodeNo < Encounter.EpisodeNo
				)
			)
		)
		then 1
		else 0
		end

	,RTTActivity = null
	--	convert(
	--		bit
	--		,case
	--		when
	--			Encounter.AdmissionTime = Encounter.EpisodeStartTime
	--		and	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
	--		and	Encounter.SpecialtyCode not in 
	--			(
	--			select
	--				SpecialtyCode
	--			from
	--				RTT.dbo.ExcludedSpecialty
	--			)

	--		-- 'GENE' ?
	--		and	Encounter.ConsultantCode <> 'FRAC'

	--		--remove CEA admissions
	--		and	Encounter.CancelledElectiveAdmissionFlag <> 1

	--		--remove ICAT
	--		and	Encounter.SiteCode != 'FMSK'

	--		and	not exists
	--			(
	--			select
	--				1
	--			from
	--				RTT.dbo.RTTClockStart ClockChangeReason
	--			where
	--				ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
	--			and	ClockChangeReason.SourceEntityRecno = Encounter.SourceSpellNo
	--			and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
	--			)

	----check for NOPs on previous referral
	--		and	not exists
	--			(
	--			select
	--				1
	--			from
	--				WH.RF.Encounter Referral

	--			inner join RTT.dbo.RTTOPClockStop ClockChangeReason
	--			on	ClockChangeReason.SourcePatientNo = Referral.SourcePatientNo
	--			and	ClockChangeReason.SourceEntityRecno = Referral.SourceEncounterNo
	--			and	(
	--					ClockChangeReason.ClockStopReasonCode = 'NOP' --Not an 18 week pathway
	--				or	ClockChangeReason.ClockStartReasonCode = 'NOP' --Not an 18 week pathway
	--				)

	--			where
	--				Referral.EncounterRecno = Encounter.ReferralEncounterRecno	
	--			)
	--		then 1
	--		else 0
	--		end
	--	)


	,RTTBreachStatusCode = null
		--case
	--	when Encounter.RTTBreachDate is null
	--	then 'U'
	--	when Encounter.RTTBreachDate < Encounter.AdmissionDate
	--	then 'B'
	--	else 'N'
	--	end

	,DiagnosticProcedure = null
	--	convert(
	--		bit
	--		,case
	--		when RTTStatus.ClockStopFlag = 1 then 0
	--		when Encounter.RTTBreachDate is null then 0 -- unknown clock starts are counted
	--		when DiagnosticProcedure.ProcedureCode is null
	--		then 0
	--		else 1
	--		end
	--	)

	,RTTTreated = null
	--	case
	--	when RTTStatus.ClockStopFlag = 1 then 1
	--	when Encounter.RTTBreachDate is null then 1 -- unknown clock starts are counted
	--	else 0
	--	end

	,EpisodeStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, EpisodeStartDate), 0)
				,EpisodeStartDate
			)
			,-1
		)

	,EpisodeEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, EpisodeEndDate), 0)
				,EpisodeEndDate
			)
			,-1
		)

	,AdmissionTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AdmissionDate), 0)
				,AdmissionDate
			)
			,-1
		)

	,DischargeTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, DischargeDate), 0)
				,DischargeDate
			)
			,-1
		)


	,StartDirectorateCode = '80'
	,EndDirectorateCode = '80'

	,ResidencePCTCode = ResidencePCTCode

	,ISTAdmissionSpecialtyCode = null
	,ISTAdmissionDemandTime = CAST(null as date)
	,ISTDischargeTime = CAST(null as date)

	,ISTAdmissionDemandTimeOfDay = null
		--coalesce(
		--	datediff(
		--		 minute
		--		,dateadd(day, datediff(day, 0, ISTAdmissionDemandTime), 0)
		--		,ISTAdmissionDemandTime
		--	)
		--	,-1
		--)

	,ISTDischargeTimeOfDay = null
		--coalesce(
		--	datediff(
		--		 minute
		--		,dateadd(day, datediff(day, 0, ISTDischargeTime), 0)
		--		,ISTDischargeTime
		--	)
		--	,-1
		--)
	,Encounter.PatientCategoryCode
	,Research2 = null
	,Encounter.ClinicalCodingStatus
	,Encounter.ClinicalCodingCompleteDate
	,Encounter.ClinicalCodingCompleteTime
	,Encounter.ReferredByCode
	,Encounter.ReferrerCode
	,Encounter.DecidedToAdmitDate
	,Encounter.PsychiatricPatientStatusCode
	,Encounter.LegalStatusClassificationCode
	,Encounter.ReferringConsultantCode
	,Encounter.DurationOfElectiveWait
	,Encounter.CarerSupportIndicator
	,Encounter.DischargeReadyDate
	,Encounter.ExpectedDateOfDischarge
	,Encounter.EddCreatedTime
	,Encounter.EddCreatedByConsultantFlag
	,Encounter.EddInterfaceCode
	,Encounter.ContextCode

	,VTECategoryCode =
		case

		when datediff(yy, Encounter.DateOfBirth, Encounter.EpisodeStartDate) - 
			(
			case 
			when
				(datepart(m, Encounter.DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
			or
				(
					datepart(m, Encounter.DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
				and	datepart(d, Encounter.DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
				)
			then 1
			else 0
			end
			) < 18

		then null

		when Encounter.PatientCategoryCode = 'RD'
		then null

		when VTE.OriginalCreationDate is not null
		then 'C'
		when VTELegacy.VTECompleted is not null
		then 'C'
		else 'I'
		end

	,Encounter.Ambulatory
	,CCGCode

	,GpCodeAtDischarge = cast(GpCodeAtDischarge as varchar(8))
	,GpPracticeCodeAtDischarge 
	,PostcodeAtDischarge 
	,Encounter.ResidenceCCGCode
	,Reportable
	,CharlsonIndex = coalesce(CharlsonIndex.CharlsonIndex, 0)
from
	(
	select -- top 1000
		 Encounter.EncounterRecno
		,Encounter.SourceUniqueID
		,Encounter.UniqueBookingReferenceNo
		,Encounter.PathwayId
		,Encounter.PathwayIdIssuerCode
		,Encounter.RTTStatusCode
		,Encounter.RTTStartDate
		,Encounter.RTTEndDate
		,Encounter.DistrictNo
		,Encounter.TrustNo
		,CasenoteNumber = 
			isnull(Encounter.EPMINumber,'') + '/' + isnull(Encounter.ProviderSpellNo,'')
		,Encounter.DistrictNoOrganisationCode
		,Encounter.NHSNumber
		,Encounter.NHSNumberStatusId
		,Encounter.PatientTitle
		,Encounter.PatientForename
		,Encounter.PatientSurname
		,Encounter.PatientAddress1
		,Encounter.PatientAddress2
		,Encounter.PatientAddress3
		,Encounter.PatientAddress4
		,Encounter.Postcode
		,Encounter.DateOfBirth
		,Encounter.DateOfDeath
		,Encounter.SexCode
		,Encounter.OverseasVisitorCode
		,Encounter.CarerSupportIndicator
		,Encounter.EthnicCategoryCode
		,Encounter.MaritalStatusCode
		,Encounter.ReligionCode
		,Encounter.LegalStatusClassificationCode
		,Encounter.ProviderSpellNo
		,Encounter.AdminCategoryCode
		,Encounter.PatientClassificationCode
		,Encounter.AdmissionMethodCode
		,Encounter.AdmissionSourceCode
		,Encounter.AdmissionDate
		,Encounter.AgeOnAdmission
		,Encounter.DischargeDestinationCode
		,Encounter.DischargeMethodCode
		,Encounter.DischargeReadyDate
		,Encounter.DischargeDate
		,Encounter.PredictedDischargeDate
		,Encounter.EpisodeNo
		,Encounter.LastEpisodeInSpellIndicator
		,Encounter.EpisodicAdminCategoryCode
		,Encounter.OperationStatusCode
		,Encounter.NeonatalLevelOfCareCode
		,Encounter.FirstRegularDayNightAdmissionFlag
		,Encounter.PsychiatricPatientStatusCode
		,Encounter.EpisodicLegalStatusClassificationCode
		,Encounter.EpisodeStartDate
		,Encounter.EpisodeEndDate
		,Encounter.EpisodeAge
		,Encounter.CommissioningSerialNo
		,Encounter.NHSServiceAgreementLineNo
		,Encounter.ProviderReferenceNo
		,Encounter.CommissionerReferenceNo
		,Encounter.ProviderCode
		,Encounter.CommissionerCode
		,Encounter.ConsultantCode
		,Encounter.SpecialtyCode
		,Encounter.TreatmentFunctionCode
		,Encounter.ICDDiagnosisSchemeCode
		,Encounter.PrimaryDiagnosisCode
		,Encounter.ReadDiagnosisSchemeCode
		,Encounter.PrimaryDiagnosisReadCode
		,Encounter.OPCSProcedureSchemeCode
		,Encounter.PrimaryProcedureCode
		,Encounter.PrimaryProcedureDate
		,Encounter.ReadProcedureSchemeCode
		,Encounter.PrimaryProcedureReadCode
		,Encounter.StartLocationClassCode
		,Encounter.StartSiteCode
		,Encounter.StartLocationTypeCode
		,Encounter.StartWardTypeCode
		,Encounter.EndLocationClassCode
		,Encounter.EndSiteCode
		,Encounter.EndLocationTypeCode
		,Encounter.EndWardTypeCode
		,Encounter.RegisteredGpCode
		,Encounter.RegisteredGpPracticeCode
		,ReferrerCode =
			Encounter.SourceReferrerCode
		,Encounter.ReferrerOrganisationCode
		,DurationOfElectiveWait = 
			case 
				when DurationOfElectiveWait < 0 then 0
				else DurationOfElectiveWait
			end
		,Encounter.ManagementIntentionCode
		,Encounter.DecidedToAdmitDate

		,Encounter.EarliestReasonableOfferDate
		,Encounter.PASHRGCode
		,Encounter.HRGVersionCode
		,Encounter.PASDGVPCode
		,Encounter.PCTCode
		,Encounter.ResidencePCTCode
		,Encounter.PASUpdateDate
		,Encounter.InterfaceCode
		,Encounter.IsDeleted
		,Encounter.ConsultantEpisodeStatusCode
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
		,Encounter.OriginalPathwayId
		,Encounter.OriginalPathwayIdIssuerCode
		,Encounter.OriginalRTTStatusCode
		,Encounter.OriginalRTTStartDate
		,Encounter.OriginalRTTEndDate
		,Encounter.EpisodeTypeCode
		,Encounter.AdmitanceType
		,Encounter.EncounterHRGCode
		,Encounter.EncounterTariffRaw
		,Encounter.EncounterTariffRatio
		,Encounter.SpellHRGCode
		,Encounter.SpellHRGTariff
		,Encounter.SpellLOS
		,Encounter.SpellExcessBedDaysTariff
		,Encounter.EncounterTariffFinal
		,Encounter.EPMINumber
		,Encounter.BookingType
		,Encounter.OutcomeCode
		,Encounter.TheatrePatientBookingKey
		,Encounter.ProcedureTime
		,Encounter.TheatreCode
		,Encounter.ChangeDate
		,Encounter.Admissionchangedate
		,SourceSexCode = null -- DG Commented out as seems to have disapperared?
		,Encounter.SourceAdminCategoryCode
		,Encounter.SourceDischargeDestinationCode
		,Encounter.SourceDischargeMethodCode
		,Encounter.SourceManagementIntentionCode
		,Encounter.SourceAdmissionSourceCode
		,Encounter.SourceAdmissionMethodCode
		,Encounter.AttendanceOutcomeCode
		,SourcePatientNo = DistrictNo
		,SourceEncounterNo = EpisodeNo
		,EpisodicGpCode =
			APCSpellHistory.RegisteredGpCode

		,EpisodicGpPracticeCode =
			APCSpellHistory.RegisteredGpPracticeCode

		,SecondaryDiagnosisCode1 = Diagnosis2.DiagnosisCode
		,SecondaryDiagnosisCode2 = Diagnosis3.DiagnosisCode
		,SecondaryDiagnosisCode3 = Diagnosis4.DiagnosisCode
		,SecondaryDiagnosisCode4 = Diagnosis5.DiagnosisCode
		,SecondaryDiagnosisCode5 = Diagnosis6.DiagnosisCode
		,SecondaryDiagnosisCode6 = Diagnosis7.DiagnosisCode
		,SecondaryDiagnosisCode7 = Diagnosis8.DiagnosisCode
		,SecondaryDiagnosisCode8 = Diagnosis9.DiagnosisCode
		,SecondaryDiagnosisCode9 = Diagnosis10.DiagnosisCode
		,SecondaryDiagnosisCode10 = Diagnosis11.DiagnosisCode
		,SecondaryDiagnosisCode11 = Diagnosis12.DiagnosisCode
		,SecondaryDiagnosisCode12 = Diagnosis13.DiagnosisCode
		,SecondaryDiagnosisCode13 = Diagnosis14.DiagnosisCode


		,SecondaryProcedureCode1 = Procedure1.ProcedureCode
		,SecondaryProcedureCode2 = Procedure2.ProcedureCode
		,SecondaryProcedureCode3 = Procedure3.ProcedureCode
		,SecondaryProcedureCode4 = Procedure4.ProcedureCode
		,SecondaryProcedureCode5 = Procedure5.ProcedureCode
		,SecondaryProcedureCode6 = Procedure6.ProcedureCode
		,SecondaryProcedureCode7 = Procedure7.ProcedureCode
		,SecondaryProcedureCode8 = Procedure8.ProcedureCode
		,SecondaryProcedureCode9 = Procedure9.ProcedureCode
		,SecondaryProcedureCode10 = Procedure10.ProcedureCode
		,SecondaryProcedureCode11 = Procedure11.ProcedureCode

		,SecondaryProcedureDate1 = Procedure1.ProcedureDate
		,SecondaryProcedureDate2 = Procedure2.ProcedureDate
		,SecondaryProcedureDate3 = Procedure3.ProcedureDate
		,SecondaryProcedureDate4 = Procedure4.ProcedureDate
		,SecondaryProcedureDate5 = Procedure5.ProcedureDate
		,SecondaryProcedureDate6 = Procedure6.ProcedureDate
		,SecondaryProcedureDate7 = Procedure7.ProcedureDate
		,SecondaryProcedureDate8 = Procedure8.ProcedureDate
		,SecondaryProcedureDate9 = Procedure9.ProcedureDate
		,SecondaryProcedureDate10 = Procedure10.ProcedureDate
		,SecondaryProcedureDate11 = Procedure11.ProcedureDate
		,PatientCategoryCode = case

				--Elective Inpatient
					when Encounter.AdmissionMethodCode in 
						(
						 '11' --Waiting List
						,'12' --Booked
						,'13' --Planned
						)
					and Encounter.ManagementIntentionCode in 
						(
						 '1' --INPATIENT
						,'3' --INTERVAL ADMISSION
						,'6' --BORN IN HOSP/ON WAY
						)
					then 'EL' --Elective


				--Elective Inpatients where intended management was daycase but patient stayed overnight
					when Encounter.AdmissionMethodCode in 
						(
						 '11'--Waiting List
						,'12'--Booked
						,'13'--Planned
						)
					and Encounter.ManagementIntentionCode = '2' -- DAY CASE
					and	cast(Encounter.AdmissionDate as date) < cast(Encounter.DischargeDate as date)
					then 'EL'--Elective


				--Elective Daycase
					when Encounter.AdmissionMethodCode in 
						(
						 '11'--Waiting List
						,'12'--Booked
						,'13'--Planned
						)
					and Encounter.ManagementIntentionCode = '2'-- DAY CASE
					and	cast(Encounter.AdmissionDate as date) = cast(Encounter.DischargeDate as date)
					then 'DC'--Daycase


				--Elective Inaptient where intended management was daycase but not discharged
					when Encounter.AdmissionMethodCode in 
						(
						 '11'--Waiting List
						,'12'--Booked
						,'13'--Planned
						)
					and Encounter.ManagementIntentionCode = '2'-- DAY CASE
					and	Encounter.DischargeDate IS NULL
					then 'EL'--Elective


				--Regular Day Case where intended management was Regular but patient stayed overnight
					when Encounter.AdmissionMethodCode in 
						(
						 '11'--Waiting List
						,'12'--Booked
						,'13' --Regular
						)
					and Encounter.ManagementIntentionCode IN ('4','5')-- Regular day and night
					and	cast(Encounter.AdmissionDate as date) < cast(Encounter.DischargeDate as date)
					then 'EL'--Elective


				--Regular Day Case
					when Encounter.AdmissionMethodCode in 
						(
						 '11'--Waiting List
						,'12'--Booked
						,'13' --Regular
						)
					and Encounter.ManagementIntentionCode IN ('4','5')-- Regular day and night
					and	cast(Encounter.AdmissionDate as date) = cast(Encounter.DischargeDate as date)
					then 'RD'--Regular


				--Regular Day Case where intended management was Regular but not discharged
					when Encounter.AdmissionMethodCode in 
						(
						 '11'--Waiting List
						,'12'--Booked
						,'13' --Regular
						)
					and Encounter.ManagementIntentionCode IN ('4','5')-- Regular day and night
					and	Encounter.DischargeDate IS NULL
					then 'EL'--Elective	


				--Non Elective
					else 'NE' --Non Elective

				end
		,ReferredByCode = 
			CASE
				WHEN left(ReferrerCode,1) = 'C' THEN 'CON'
				WHEN left(ReferrerCode,1) = 'G' THEN 'GP'
				WHEN left(ReferrerCode,1) = 'D' THEN 'GDP'
			END
		,ReferringConsultantCode = 
			CASE
				WHEN left(ReferrerCode,1) = 'C' THEN Encounter.SourceReferrerCode
				else null
			END
		,ExpectedDateOfDischarge = 
			Case
				when PredictedDischargeDate between '1 jan 1900'and  '6 June 2079' then PredictedDischargeDate
				else  null
			end
		,EddCreatedTime = 
			Case
				when PredictedDischargeDateCreated between '1 jan 1900'and  '6 June 2079' then PredictedDischargeDateCreated
				else  null
			end
		,EddCreatedByConsultantFlag = 
			Encounter.PredictedDischargeDateSetByCon
		,EddInterfaceCode = 
			case when 
				Case
				when PredictedDischargeDateCreated between '1 jan 1900'and  '6 June 2079' then PredictedDischargeDateCreated
				else  null
				end is not null 
			then 'UG' 	
			end
		,ContextCode = 'TRA||' + Encounter.InterfaceCode
		,ClinicalCodingStatus
		,ClinicalCodingCompleteDate
		,ClinicalCodingCompleteTime
	--select top 10000 convert(varchar(20),PredictedDischargeDate,113)

		,Ambulatory = coalesce(Encounter.Ambulatory, 0)

		,CCGCode = 
				coalesce(
					 CCGPractice.[Parent Organisation Code]
					,CCGPostcode.CCGCode
					,'02A'
				)

		,GpCodeAtDischarge = Encounter.RegisteredGpCode
		,GpPracticeCodeAtDischarge = Encounter.RegisteredGpPracticeCode 
		,PostcodeAtDischarge  = Encounter.Postcode

		,ResidenceCCGCode = EpisodicPostcode.CCGCode
		,Reportable = cast(1 as bit)
		
	from
		[$(TraffordWarehouse)].dbo.APCEncounter Encounter

	left join [$(TraffordWarehouse)].dbo.APCSpellHistory
	on	APCSpellHistory.ProviderSpellNo = Encounter.ProviderSpellNo
	and	APCSpellHistory.Status = 'DI'

	left join [$(Organisation)].[ODS].[General Medical Practice]  CCGPractice
	on CCGPractice.[Organisation Code] = APCSpellHistory.RegisteredGpCode


	left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
	on	CCGPostcode.Postcode = 
		case
		when datalength(rtrim(ltrim(APCSpellHistory.Postcode))) = 6 then left(APCSpellHistory.Postcode, 2) + '   ' + right(APCSpellHistory.Postcode, 3)
		when datalength(rtrim(ltrim(APCSpellHistory.Postcode))) = 7 then left(APCSpellHistory.Postcode, 3) + '  ' + right(APCSpellHistory.Postcode, 3)
		else APCSpellHistory.Postcode
		end

	left outer join [$(Organisation)].dbo.CCGPostcode EpisodicPostcode on
			EpisodicPostcode.Postcode = 
				case
				when len(Encounter.Postcode) = 8 then Encounter.Postcode
				else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
				end


	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis1
	on	Diagnosis1.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis1.SequenceNo = 1

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis2
	on	Diagnosis2.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis2.SequenceNo = 2

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis3
	on	Diagnosis3.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis3.SequenceNo = 3

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis4
	on	Diagnosis4.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis4.SequenceNo = 4

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis5
	on	Diagnosis5.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis5.SequenceNo = 5

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis6
	on	Diagnosis6.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis6.SequenceNo = 6

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis7
	on	Diagnosis7.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis7.SequenceNo = 7

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis8
	on	Diagnosis8.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis8.SequenceNo = 8

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis9
	on	Diagnosis9.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis9.SequenceNo = 9

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis10
	on	Diagnosis10.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis10.SequenceNo = 10

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis11
	on	Diagnosis11.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis11.SequenceNo = 11

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis12
	on	Diagnosis12.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis12.SequenceNo = 12

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis13
	on	Diagnosis13.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis13.SequenceNo = 13

	left join [$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis14
	on	Diagnosis14.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Diagnosis14.SequenceNo = 14


	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure1
	on	Procedure1.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure1.SequenceNo = 2

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure2
	on	Procedure2.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure2.SequenceNo = 3

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure3
	on	Procedure3.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure3.SequenceNo = 4

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure4
	on	Procedure4.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure4.SequenceNo = 5

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure5
	on	Procedure5.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure5.SequenceNo = 6

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure6
	on	Procedure6.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure6.SequenceNo = 7

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure7
	on	Procedure7.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure7.SequenceNo = 8

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure8
	on	Procedure8.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure8.SequenceNo = 9

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure9
	on	Procedure9.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure9.SequenceNo = 10

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure10
	on	Procedure10.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure10.SequenceNo = 11

	left join [$(TraffordWarehouse)].dbo.APCProcedure Procedure11
	on	Procedure11.IPSourceUniqueID = Encounter.SourceUniqueID
	and	Procedure11.SequenceNo = 12

	where
	Encounter.IsDeleted = 0

	--remove fces that exist in both Central and Trafford activity
	and	not exists
		(
		select
			1
		from
			--WarehouseOLAPMerged.APC.EncounterDuplicate -- DG 2.6.14 - Not sure why pointing to WarehouseOLAPMerged? Repointed to APC.EncounterDuplicate in WOMV2...
			APC.EncounterDuplicate
		where
			EncounterDuplicate.DuplicateContextID = (select ContextID from WH.Context where ContextCode	= 'TRA||UG') -- DG 2.6.14 - Repointed to ReferenceMapv2 from ReferenceMap 
		and	EncounterDuplicate.DuplicateRecno = Encounter.EncounterRecno
		)

	) Encounter

left join [$(TraffordWarehouse)].dbo.HRG4APCEncounter HRG4Encounter
on	HRG4Encounter.EncounterRecno = Encounter.EncounterRecno

left join [$(TraffordWarehouse)].dbo.HRG4APCSpell HRG4Spell
on	HRG4Spell.EncounterRecno = Encounter.EncounterRecno

--left join [$(Warehouse)].dbo.DiagnosticProcedure
--on	DiagnosticProcedure.ProcedureCode = Encounter.PrimaryOperationCode

--left join [$(Warehouse)].PAS.RTTStatus RTTStatus
--on    RTTStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

left join [$(Whiteboard)].dbo.InfoPathForm_VTEList VTE
on	VTE.AdmissionNumber = Encounter.ProviderSpellNo

left join [$(TraffordWarehouse)].dbo.APCVTELegacy VTELegacy
on	VTELegacy.AdmissionNumber = Encounter.ProviderSpellNo

left join
	(
	select
		Diagnosis.IPSourceUniqueID
		,CharlsonIndex = sum(cast(Charlson.NewWeight as int))
	from
		[$(TraffordWarehouse)].dbo.APCDiagnosis Diagnosis

	inner join [$(Warehouse)].WH.CharlsonIndex Charlson
	on	left(Diagnosis.DiagnosisCode, 6) = Charlson.DiagnosisCode

	where
		Diagnosis.SequenceNo > 1
	and not exists
			(
			select
				1
			from
				[$(TraffordWarehouse)].dbo.APCDiagnosis DiagnosisLater
			inner join [$(Warehouse)].WH.CharlsonIndex CharlsonLater
			on	left(DiagnosisLater.DiagnosisCode, 6) = CharlsonLater.DiagnosisCode		

			where
				DiagnosisLater.SequenceNo > Diagnosis.SequenceNo	
			and DiagnosisLater.IPSourceUniqueID = Diagnosis.IPSourceUniqueID
			and CharlsonLater.CharlsonConditionCode = Charlson.CharlsonConditionCode			
			)
	group by
		IPSourceUniqueID	
	) CharlsonIndex
on	CharlsonIndex.IPSourceUniqueID = Encounter.SourceUniqueID


where
	(
		Encounter.EpisodeEndDate >= '1 Apr 2009'
	or	Encounter.EpisodeEndDate is null
	)

--do not load any PAS data as this is pre-2009
and	Encounter.InterfaceCode <> 'PAS'



































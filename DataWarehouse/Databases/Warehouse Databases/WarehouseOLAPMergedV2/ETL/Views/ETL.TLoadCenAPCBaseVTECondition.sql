﻿
create view ETL.TLoadCenAPCBaseVTECondition

as

select
	[VTEConditionRecno]
	,[SourceUniqueID]
	,[SourcePatientNo]
	,[SourceSpellNo]
	,[DiagnosisDate]
	,[DiagnosisTime]
	,[VTETypeID]
	,[OccuredOnAnotherWard]
	,[MedicationRequired]
	,[MedicationStartDate]
	,[MedicationStartTime]
	,[WardCode]
	,[InterfaceCode]
	,ContextCode = 'CEN||BEDMAN'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[APC].[VTECondition]
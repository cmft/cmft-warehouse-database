﻿
CREATE VIEW [ETL].[TLoadCenAPCBaseCriticalCarePeriod] AS

select
	 SourceUniqueID
	,SourceSpellNo
	,SourcePatientNo
	,StartDate
	,StartTime
	,EndDate
	,EndTime
	,AdvancedCardiovascularSupportDays
	,AdvancedRespiratorySupportDays
	,BasicCardiovascularSupportDays
	,BasicRespiratorySupportDays
	,CriticalCareLevel2Days
	,CriticalCareLevel3Days
	,DermatologicalSupportDays
	,LiverSupportDays
	,NeurologicalSupportDays
	,RenalSupportDays
	,CreatedByUser
	,CreatedByTime
	,LocalIdentifier
	,LocationCode
	,StatusCode
	,TreatmentFunctionCode
	,PlannedAcpPeriod
	,InterfaceCode
	,CasenoteNumber
	,WardCode
	,AdmissionDate
	,SiteCode
	,NHSNumber
	,UnitFunctionCode
	,ContextCode = 'CMFT||MID'
	,Created
	,ByWhom
FROM
	[$(Warehouse)].APC.CriticalCarePeriod CriticalCarePeriod
where 
	InterfaceCode = 'MID'




﻿






CREATE view [ETL].[TLoadCenCOMBaseDiagnosisProcedureEvent] as

select
	 SourceUniqueID
	,DiagnosisProcedureClassificationID
	,SourcePatientNo
	,MedicalProblemLevelID
	,DiagnosisProcedureSubTypeID
	,DiagnosisProcedureID
	,NHSIdentifier
	,Comment
	,DiagnosisProcedure
	,EncounterSourceUniqueID
	,SourceEntityCode
	,DiagnosisProcedureTime
	,DiagnosisProcedureTypeCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,DiagnosisProcedureCode
	,DiagnosisProcedureInternalCode
	,SupplementaryCode
	,SupplementaryInternalCode
	,ArchiveFlag
	,StartTime
	,EndTime
	,UpdateTime
	,StaffTeamID
	,ProfessionalCarerID
	,Duration
	,ConfidentialFlag
	,Dosage
	,DosageUnitID
	,LocationOnBodyID
	,DosageFrequency
	,FrequencyDosageUnitID
	,CancelledTime
	,RulesID
	,Value
	,ProblemFlag
	,LateralityID
	,SourceEpisodeID
	,PeriodAdministeredID
	,ReasonAdministeredID
	,AnaestheticCategoryID
	,CauseOfDeathFlag
	,EventSequenceNo
	,CodingAuthorisationFlag
	,LatchedCodingAuthorisationFlag
	,ActionToTake
	,AlertSeverityID
	,ContractedProcedureFlag
	,SpecialtyID
	,HostitalServiceFlag
	,DiagnosisProcedureEndTime
	,ProcedureLocationID
	,TheatreBodyRegionID
	,IndividualCodingStatusID
	,AnaestheticTypeID
	,CodingOnsetTypeID
	,VisitPurposeID
	,HealthOrganisationOwnerID
	,Created
	,Updated
	,ByWhom

	,ContextCode = 'CEN||IPM'

	--,ModifiedRow =
	--	case
	--	when
	--		DiagnosisProcedure.Updated >
	--		(
	--		select
	--			DatasetProcess.DatasetProcessTime
	--		from
	--			ETL.DatasetProcess
	--		where
	--			DatasetProcess.DatasetCode = 'COMDPE'
	--		and	DatasetProcess.ProcessCode = 'IMPORT'
	--		)
	--	then 1
	--	else 0
	--	end
from
	[$(Warehouse)].COM.DiagnosisProcedureEvent DiagnosisProcedure
where
	DiagnosisProcedure.ArchiveFlag = 'N'
and	DiagnosisProcedure.DiagnosisProcedureTime >= '01 Apr 2008'










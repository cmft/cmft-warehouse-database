﻿

CREATE view [ETL].[TLoadTraAEBaseEncounter] as


/******************************************************************************************************
View		: [ETL].[TLoadTraAEBaseEncounter]
Description	: 

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
04/12/2014	Paul Egan       Assign ServiceID for Trafford UCC to 2 (Urgent Care) instead of 3 (Walk In 
							Centre) so we can differentiate Trafford WiC on AE reports. 
*******************************************************************************************************/

select
	 EncounterRecno = AEEncounter.EncounterRecno
	,AEEncounter.SourceUniqueID
	,AEEncounter.UniqueBookingReferenceNo
	,AEEncounter.PathwayId
	,AEEncounter.PathwayIdIssuerCode
	,AEEncounter.RTTStatusCode
	,AEEncounter.RTTStartDate
	,AEEncounter.RTTEndDate
	,AEEncounter.DistrictNo
	,AEEncounter.TrustNo
	,AEEncounter.CasenoteNo
	,AEEncounter.DistrictNoOrganisationCode
	,AEEncounter.NHSNumber
	,NHSNumberStatusCode = AEEncounter.NHSNumberStatusId
	,AEEncounter.PatientTitle
	,AEEncounter.PatientForename
	,AEEncounter.PatientSurname
	,AEEncounter.PatientAddress1
	,AEEncounter.PatientAddress2
	,AEEncounter.PatientAddress3
	,AEEncounter.PatientAddress4
	,AEEncounter.Postcode
	,AEEncounter.DateOfBirth
	,AEEncounter.DateOfDeath
	,AEEncounter.SexCode
	,AEEncounter.CarerSupportIndicator
	,RegisteredGpCode =
		coalesce(AEEncounter.RegisteredGpCode,	'G9999998')

	,RegisteredPracticeCode =
		coalesce(AEEncounter.RegisteredGpPracticeCode, 'V81999')

	,AEEncounter.AttendanceNumber
	,AEEncounter.ArrivalModeCode
	,AEEncounter.AttendanceCategoryCode

	,AttendanceDisposalCode =
		coalesce(
			AEEncounter.AttendanceDisposalCode
			,'-1'
		)

	,AEEncounter.IncidentLocationTypeCode
	,AEEncounter.PatientGroupCode
	,AEEncounter.SourceOfReferralCode
	,AEEncounter.ArrivalDate
	,AEEncounter.ArrivalTime
	,AEEncounter.AgeOnArrival
	,AEEncounter.InitialAssessmentTime
	,AEEncounter.SeenForTreatmentTime
	,AEEncounter.AttendanceConclusionTime
	,AEEncounter.DepartureTime
	,AEEncounter.CommissioningSerialNo
	,AEEncounter.NHSServiceAgreementLineNo
	,AEEncounter.ProviderReferenceNo
	,AEEncounter.CommissionerReferenceNo
	,AEEncounter.ProviderCode
	,AEEncounter.CommissionerCode
	,AEEncounter.StaffMemberCode
	,AEEncounter.InvestigationCodeFirst
	,AEEncounter.InvestigationCodeSecond
	,AEEncounter.DiagnosisCodeFirst
	,AEEncounter.DiagnosisCodeSecond
	,AEEncounter.TreatmentCodeFirst
	,AEEncounter.TreatmentCodeSecond
	,AEEncounter.PASHRGCode
	,AEEncounter.HRGVersionCode
	,AEEncounter.PASDGVPCode
	,AEEncounter.SiteCode
	,AEEncounter.Created
	,AEEncounter.Updated
	,AEEncounter.ByWhom
	,AEEncounter.InterfaceCode
	,AEEncounter.PCTCode
	,AEEncounter.ResidencePCTCode
	,AEEncounter.InvestigationCodeList
	,AEEncounter.TriageCategoryCode
	,AEEncounter.PresentingProblem
	,PresentingProblemCode = '-1'

	,AEEncounter.ToXrayTime
	,AEEncounter.FromXrayTime
	,AEEncounter.ToSpecialtyTime
	,AEEncounter.SeenBySpecialtyTime
	,AEEncounter.EthnicCategoryCode
	,AEEncounter.ReligionCode
	,ReferredToSpecialtyCode =
		coalesce(
			 AEEncounter.ReferredToSpecialtyCode
			,0
		)
	,AEEncounter.DecisionToAdmitTime
	,DischargeDestinationCode = null
	,RegisteredTime = null
	,TransportRequestTime = null
	,TransportAvailableTime = null
	,AscribeLeftDeptTime = null
	,CDULeftDepartmentTime = null
	,PCDULeftDepartmentTime = null
	,TreatmentDateFirst = null
	,TreatmentDateSecond = null
	,SourceAttendanceDisposalCode = AEEncounter.AttendanceDisposalCode
	,AmbulanceArrivalTime = null
	,AmbulanceCrewPRF = null

	,UnplannedReattend7Day =
		UnplannedReattend.UnplannedReattend7Day

	,ArrivalTimeAdjusted =
		AEEncounter.ArrivalTime

	,ClinicalAssessmentTime = null


	,LevelOfCareCode =
		case
		when
			AEEncounter.AttendanceDisposalCode = '01'
		then 2 --high
		when
			AEInvestigation.InvestigationDate is null
		and	AEEncounter.ToSpecialtyTime is null
		then 0 --low
		else 1 --medium
		end

	,EncounterBreachStatusCode = 
		case
		when AEEncounter.DepartureTime is null then 'X'
		when EncounterBreach.BreachValue is null
		then 'N'
		else 'B'
		end

	,EncounterStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
				,AEEncounter.ArrivalTime
			)
			,-1
		)

	--,EncounterEndTimeOfDay =
	--	coalesce(
	--		datediff(
	--			 minute
	--			,dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
	--			,AEEncounter.DepartureTime
	--		)
	--		,-1
	--	)

	,EncounterEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AEEncounter.DepartureTimeAdjusted), 0)
				,AEEncounter.DepartureTimeAdjusted
			)
			,-1
		)

	,EncounterStartDate =
		dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)

	--,EncounterEndDate = 
	--	coalesce(
	--		 dateadd(day, datediff(day, 0, AEEncounter.DepartureTime), 0)
	--		,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
	--	)

	,EncounterEndDate = 
		coalesce(
			 dateadd(day, datediff(day, 0, AEEncounter.DepartureTimeAdjusted), 0)
			,dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime), 0)
		)

	--,EncounterDurationMinutes =
	--	datediff(
	--		 minute
	--		,AEEncounter.ArrivalTime
	--		,AEEncounter.DepartureTime
	--	)

	,EncounterDurationMinutes =
		datediff(
			 minute
			,AEEncounter.ArrivalTime
			,AEEncounter.DepartureTimeAdjusted
		)

	,AgeCode =
		case
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) is null then 'Age Unknown'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 0 then '00 Days'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) = 1 then '01 Day'
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 1 and
		datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate)), 2) + ' Days'
		
		when datediff(day, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) > 28 and
		datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end > 1 and
		 datediff(month, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, AEEncounter.DateOfBirth,AEEncounter.ArrivalDate) -
		case  when datepart(day, AEEncounter.DateOfBirth) > datepart(day, AEEncounter.ArrivalDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
		(
		case 
		when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
		or
			(
				datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
			And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, AEEncounter.DateOfBirth, AEEncounter.ArrivalDate) - 
		(
		case 
		when	(datepart(m, AEEncounter.DateOfBirth) > datepart(m, AEEncounter.ArrivalDate)) 
		or
			(
				datepart(m, AEEncounter.DateOfBirth) = datepart(m, AEEncounter.ArrivalDate) 
			And	datepart(d, AEEncounter.DateOfBirth) > datepart(d, AEEncounter.ArrivalDate)
			) then 1 else 0 end
		)), 2) + ' Years'

	end

	,LeftWithoutBeingSeenCases =
		case
		when
			AttendanceDisposalCode in ('138' , '4917', '5082') 
		and	SeenForTreatmentTime is null
		then 1
		else 0
		end

	,HRG4AEEncounter.HRGCode

	,Reportable = 1 --coalesce(AEEncounter.Reportable , 1)
	--,CareGroup = CareGroup.Reference
	,CareGroup = null

	,AEEncounter.ContextCode
	,AEEncounter.EPMINo


	,DepartmentTypeCode =
		case
		when
			(
				AEEncounter.ContextCode = 'CEN||ADAS'
			or	AEEncounter.ContextCode = 'TRA||ADAS'
			or	Site.NationalValueCode = 'RW3T1'
			or	
				(
					Site.NationalValueCode = 'RW3TR' -- conversion of TGH to type 3 following Trafford Service redesign
				and
					ArrivalTime >= '28 nov 2013 08:00:00'
				)			
			
			) then '03'
		when Site.NationalValueCode in ('RW3SM' , 'RW3RE' , 'RW3DH') then '02'
		else '01'
		end

	,ServiceID =
		case
		when
			(
				AEEncounter.ContextCode = 'CEN||ADAS'
			or	AEEncounter.ContextCode = 'TRA||ADAS'
			or	Site.NationalValueCode = 'RW3T1'
			) then 3
		when	-- Assign Trafford UCC to 2 instead of 3. 04/12/2014 Paul Egan
			(
				Site.NationalValueCode = 'RW3TR' -- conversion of TGH to type 3 following Trafford Service redesign
			and
				ArrivalTime >= '28 nov 2013 08:00:00'
			) then 2
		when Site.NationalValueCode in ('RW3SM' , 'RW3RE' , 'RW3DH') then 2
		else 1
		end

	,AEEncounter.DepartureTimeAdjusted

	,CarePathwayAttendanceConclusionTime = null
	,CarePathwayDepartureTime = null

	,GpCodeAtAttendance = AEEncounter.RegisteredGpCode
	,GpPracticeCodeAtAttendance = AEEncounter.RegisteredGpPracticeCode
	,PostcodeAtAttendance = AEEncounter.Postcode

	,CCGCode = 
		coalesce(
			 CCGPractice.ParentOrganisationCode
			,CCGPostcode.CCGCode
			,'02A'
			)

	,AttendanceResidenceCCGCode = CCGPostcode.CCGCode
	,[Site].NationalValueCode

from
	(
	select
		 * --goddammit!!!
		,DepartureTimeAdjusted = 
			coalesce(
				 Encounter.AttendanceConclusionTime
				,Encounter.DepartureTime
			)
		,ContextCode = 'TRA||' + Encounter.InterfaceCode
	from
		[$(TraffordWarehouse)].dbo.AEEncounter Encounter
	) AEEncounter

left join AE.Stage EncounterBreach
on	EncounterBreach.StageCode = 'INDEPARTMENTADJUSTEDNATIONAL'
--and
--	datediff(
--		 minute
--		,AEEncounter.ArrivalTime
--		,AEEncounter.DepartureTime
--	)
--	 > EncounterBreach.BreachValue
and
	datediff(
		 minute
		,AEEncounter.ArrivalTime
		,AEEncounter.DepartureTimeAdjusted
	)
	 > EncounterBreach.BreachValue


left join [$(TraffordWarehouse)].dbo.AEInvestigation AEInvestigation
on	AEInvestigation.AESourceUniqueID = AEEncounter.SourceUniqueID
and	AEInvestigation.SequenceNo = 1

left join [$(TraffordWarehouse)].dbo.HRG4AEEncounter
on	HRG4AEEncounter.EncounterRecno = AEEncounter.EncounterRecno

-- determine which unplanned attendances follow a previous unplanned attendance within seven days
left join (
	select
		 SourceUniqueID
		,UnplannedReattend7Day = 1
	from
		[$(TraffordWarehouse)].dbo.AEEncounter
	where
		isnull(AEEncounter.AttendanceDetails,'New Episode') != 'Planned return'
	and	AEEncounter.AttendanceCategoryCode != 2
	and	exists
			(
			select
				1
			from
				[$(TraffordWarehouse)].dbo.AEEncounter PreviousAEEncounter
			where
				PreviousAEEncounter.DistrictNo = AEEncounter.DistrictNo
			and	PreviousAEEncounter.SiteCode = AEEncounter.SiteCode
			and	coalesce(
					 dateadd(day, datediff(day, 0, PreviousAEEncounter.DepartureTime), 0)
					,dateadd(day, datediff(day, 0, PreviousAEEncounter.ArrivalTime), 0)
				) between dateadd(day, datediff(day, 0, AEEncounter.ArrivalTime) - 7 , 0) and AEEncounter.ArrivalTime
			and	coalesce(
					 PreviousAEEncounter.DepartureTime
					,PreviousAEEncounter.ArrivalTime
				) < AEEncounter.ArrivalTime
			and	isnull(AEEncounter.AttendanceDetails,'New Episode') != 'Planned return'
			and	PreviousAEEncounter.AttendanceCategoryCode != 2
			)
	) UnplannedReattend

on	UnplannedReattend.SourceUniqueID = AEEncounter.SourceUniqueID


left join [$(Warehouse)].AE.LookupBase
on	LookupBase.Lkp_ID = AEEncounter.AttendanceDisposalCode


--SITE	Site

left join WH.Member Site
on	Site.SourceValueCode = coalesce(cast(AEEncounter.SiteCode as varchar), '-1')
and	Site.AttributeCode = 'SITE'
and	Site.SourceContextCode = AEEncounter.ContextCode

left join [$(Organisation)].dbo.CCGPractice CCGPractice
on CCGPractice.OrganisationCode = AEEncounter.RegisteredGpPracticeCode


left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
on	CCGPostcode.Postcode = 
	case
	when datalength(rtrim(ltrim(AEEncounter.Postcode))) = 6 then left(AEEncounter.Postcode, 2) + '   ' + right(AEEncounter.Postcode, 3)
	when datalength(rtrim(ltrim(AEEncounter.Postcode))) = 7 then left(AEEncounter.Postcode, 3) + '  ' + right(AEEncounter.Postcode, 3)
	else AEEncounter.Postcode
	end	

;




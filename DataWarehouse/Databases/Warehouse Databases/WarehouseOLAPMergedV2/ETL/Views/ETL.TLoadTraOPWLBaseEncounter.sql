﻿






CREATE view [ETL].[TLoadTraOPWLBaseEncounter] as	    

select 
	 AdminCategoryCode
	,AgeOnAppointment

	,AppointmentDate

	,AppointmentNote
	,AppointmentID = null
	,AppointmentStatusCode

	,AppointmentTypeCode 
	,APPReferralDate
	,AttendanceID
	,AttendanceOutcomeCode
	,CarerSupportIndicator
	,CasenoteNo
	,CensusDate
	,ClinicCode
	,ClinicFunctionCode
	,CommissionerCode
	,CommissionerReferenceNo
	,CommissioningSerialNo
	,ContactChangeDate
	,ContactChangeTime
	,ContactCreationDate
	,ContextCode
	,DateOfBirth
	,DateOfDeath
	,DepartmentCode
	,DistrictNo
	,DistrictNoOrganisationCode
	,DNACode
	,EarliestReasonableOfferDate
	,EncounterRecno
	,EpisodeChangeDate
	,EpisodeChangeTime
	,EthnicCategoryCode
	,FirstAttendanceCode
	,HRGVersionCode
	,ICDDiagnosisSchemeCode
	,IsDeleted
	,LastDNAOrCancelledDate
	,LocationClassCode
	,LocationTypeCode
	,MaritalStatusCode
	,MedicalStaffTypeCode
	,NHSNumber
	,NHSNumberStatusId
	,NHSServiceAgreementLineNo
	,OPCSProcedureSchemeCode
	,OperationStatusCode
	,OriginalPathwayId
	,OriginalPathwayIdIssuerCode
	,OriginalRTTEndDate
	,OriginalRTTStartDate
	,OriginalRTTStatusCode
	,OverseasVisitorCode
	,PASDGVPCode
	,PASHRGCode
	,PASUpdateDate
	,PathwayId
	,PathwayIdIssuerCode
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientForename
	,PatientSurname
	,PatientTitle
	,PCTCode
	,Postcode
	,PrimaryDiagnosisCode
	,PrimaryDiagnosisReadCode
	,PrimaryProcedureCode
	,PrimaryProcedureDate
	,PrimaryProcedureReadCode

	,PriorityCode = PriorityTypeCode

	,PriorityTypeCode
	,ProviderCode
	,ProviderReferenceNo
	,ReadDiagnosisSchemeCode
	,ReadProcedureSchemeCode
	,ReferralRequestReceivedDate
	,ReferrerCode
	,ReferrerOrganisationCode
	,RegisteredGpCode
	,ResidencePCTCode
	,RTTEndDate
	,RTTStartDate
	,RTTStatusCode
	,ServiceTypeRequestedCode
	,SexCode
	,SiteCode
	,SourceOfReferralCode
	,SourcePatientNo
	,SourceUniqueID
	,TreatmentFunctionCode
	,TrustNo
	,UniqueBookingReferenceNo
	,WaitingListCode
  
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,RegisteredGpPracticeCode = coalesce(Encounter.RegisteredGpPracticeCode, 'X99999')
 
   	,AgeCode =
		case
		when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
		when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
		when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
		when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
		datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
	
		when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
		datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
		 datediff(month, DateOfBirth, Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.CensusDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
			And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
			) then 1 else 0 end
		)), 2) + ' Years'

		end

	,Cases = 1



	,WaitTypeCode =
		case
		when AdminCategoryCode = '02' then '70' -- excludes private patients
		else '50'
		end

--	case
--	when rtrim(Encounter.ElectiveAdmissionTypeCode ) = 'PA' then '20'
--	when rtrim(Encounter.ElectiveAdmissionTypeCode ) = 'PL' then '20'
--	--when Encounter.WLStatus = 'WL Suspend' then '30' DG - needs bringing back in when WLStatus available
--	when Encounter.AdminCategoryCode = '' then '40'
--	else '10'
--	end -- check - mappings against THT are National - use Source and remap?
	 
	--,StatusCode =
	--case
	--when Encounter.WLStatus = 'WL Suspend' then '20' 
	--else '10'
	--end

	,StatusCode = '10' -- DG - To get INTO fact table reliant on WLStatus

	--,CategoryCode =
	--case
	----when rtrim(Encounter.ManagementIntentionCode) = 'I' then '10'
	--when rtrim(Encounter.SourceManagementIntentionCode) = '1' then '10' -- DG - amended to be appropriate for Trafford
	--else '20'
	--end

	--,AddedToWaitingListTime = Created -- check

	
	
      ----EpisodicGpPracticeCode 
      
      
      
	--,AddedLastWeek =
	--case
	--when
	--	datediff(
	--		 day
	--		,dateadd(day, datediff(day, 0, Encounter.Created), 0)
	--		,Encounter.CensusDate
	--	) < 8 then 1
	--else 0
	--end
	
	
	,OPCSCoded =
		case
		when Encounter.PrimaryProcedureCode is null	then 0
		else 1
		end

	,WithRTTStartDate =
		case
		when Encounter.RTTStartDate is null	then 0
		else 1
		end

	,WithRTTStatusCode =
		case
		when Encounter.RTTStatusCode in ('10', '20') then 1
		else 0
		end
	
	,WithExpectedAdmissionDate = null 
	
	,RTTActivity =
		convert(
			bit
			,0
			--,case
			--when
			--	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')+
			--and	Encounter.SpecialtyCode not in 
			--	(
			--	SELECT
			--		SpecialtyCode
			--	FROM
			--		RTT.dbo.ExcludedSpecialty
			--	)

			---- 'GENE' ?
			--and	Encounter.ConsultantCode <> 'FRAC'

			----remove ICAT
			--and	Encounter.SiteCode != 'FMSK'

			--and	not exists
			--	(
			--	SELECT
			--		1
			--	FROM
			--		RTT.dbo.RTTClockStart ClockChangeReason
			--	WHERE
			--		ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
			--	and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
			--	and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
			--	)
			--then 1
			--else 0
			--end
		)
	
	
	,DiagnosticProcedure = 0
	--,DirectorateCode = coalesce(Encounter.DirectorateCode , 'N/A')
	,DirectorateCode = 80
	--,GuaranteedAdmissionDate
	
	,DurationAtTCIDateCode = '000'
		
	--,WithTCIDate =
	--case
	--when OfferedForAdmissionDate is null
	--then 0
	--when OfferedForAdmissionDate < (SELECT min(TheDate) FROM WH.CalendarBase WHERE TheDate <> '1 Jan 1900')
	--then 0
	--when OfferedForAdmissionDate < CensusDate
	--then 0
	--else 1
	--end

	--,WithGuaranteedAdmissionDate =
	--	case
	--	when Encounter.GuaranteedAdmissionDate is null
	--	then 0
	--	else 1
	--	end

	,LengthOfWait		    
	    
	,DurationCode =
		coalesce(
			right('00' +
				convert(
					varchar
					,case
					when convert(INT, Encounter.KornerWait / 7 ) > 207 then 208
					else convert(INT, Encounter.KornerWait / 7 )
					end
				)
				,3
			)
			,'NA'
		)

	,PASSpecialtyCode = Encounter.SpecialtyCode
	,WithRTTOpenPathway
	,DateOnWaitingList = ContactCreationDate
	,OriginalDateOnWaitingList = ContactCreationDate
	,Referraldate = ReferralRequestReceivedDate

	,WithAppointmentDate =
		case
		when AppointmentDate is null then 0
		else 1
		end			

	,DurationAtAppointmentDateCode =
		case
		when Encounter.AppointmentDate is null then 'NA'
		when Encounter.AppointmentDate < (select min(TheDate) from WH.Calendar where TheDate > '2 Jan 1900') then 'NA'
		when Encounter.AppointmentDate < CensusDate then 'NA'
		else right('00' +
				convert(varchar, 
					case
					when convert(int, (Encounter.LengthOfWait + datediff(day , CensusDate , AppointmentDate)) /7 ) > 207 then 208
					else convert(int, (Encounter.LengthOfWait + datediff(day , CensusDate , AppointmentDate)) /7 )
					end
				)
				,3
			)
		end

	,Encounter.CCGCode
	,Encounter.ResidenceCCGCode
	,EthnicOriginCode = EthnicCategoryCode
	,DataSourceID = 1
from 
	(
	SELECT
		 EncounterRecno

		,CensusDate = 
			dateadd(d , -1 , Encounter.CensusDate)

		,SourceUniqueID
		,UniqueBookingReferenceNo
		,PathwayId
		,PathwayIdIssuerCode
		,RTTStatusCode
		,RTTStartDate
		,RTTEndDate
		,SourcePatientNo = DistrictNo
		,DistrictNo
		,TrustNo
		,CasenoteNo
		,DistrictNoOrganisationCode
		,NHSNumber
		,NHSNumberStatusId
		,PatientTitle
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,Encounter.Postcode
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,CarerSupportIndicator
		,EthnicCategoryCode
		,MaritalStatusCode
		,AttendanceID
		,AdminCategoryCode
		,DNACode

		,FirstAttendanceCode = 
			case 
			when FollowupAppointments.AppointmentTypeID is null	then FirstAttendanceCode
			else 2
			end

		,MedicalStaffTypeCode
		,OperationStatusCode
		,AttendanceOutcomeCode

		,AppointmentDate =
			case
			when AppointmentDate < (select min(TheDate) from WH.CalendarBase where TheDate > '2 Jan 1900')
			then null
			when AppointmentDate < CensusDate
			then null
			else AppointmentDate
			end

		,AppointmentNote
		,AgeOnAppointment
		,EarliestReasonableOfferDate
		,CommissioningSerialNo
		,NHSServiceAgreementLineNo
		,ProviderReferenceNo
		,CommissionerReferenceNo
		,ProviderCode
		,CommissionerCode
		,ConsultantCode
		,SpecialtyCode
		,TreatmentFunctionCode
		,ICDDiagnosisSchemeCode
		,PrimaryDiagnosisCode
		,ReadDiagnosisSchemeCode
		,PrimaryDiagnosisReadCode
		,OPCSProcedureSchemeCode
		,PrimaryProcedureCode
		,PrimaryProcedureDate
		,ReadProcedureSchemeCode
		,PrimaryProcedureReadCode
		,LocationClassCode
		,SiteCode
		,LocationTypeCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,PriorityTypeCode
		,ServiceTypeRequestedCode
		,SourceOfReferralCode
		,ReferralRequestReceivedDate
		,ReferrerCode
		,ReferrerOrganisationCode
		,LastDNAOrCancelledDate
		,PASHRGCode
		,HRGVersionCode
		,PASDGVPCode
		,PCTCode
		,ClinicCode
		,ClinicFunctionCode
		,OverseasVisitorCode
		,ResidencePCTCode
		,DepartmentCode
		,AppointmentStatusCode
		,IsDeleted
		,ContactChangeDate
		,EpisodeChangeDate
		,ContactChangeTime
		,EpisodeChangeTime
		,PASUpdateDate
		,ContactCreationDate
		,AppointmentTypeCode
		,OriginalPathwayId
		,OriginalPathwayIdIssuerCode
		,OriginalRTTStatusCode
		,OriginalRTTStartDate
		,OriginalRTTEndDate
		,APPReferralDate

		,KornerWait = 
			coalesce( 
				case 
				when KornerWait < 0 then 0
				else KornerWait
				end
				,0
			)

		,LengthOfWait = 
			convert(INT, 
				coalesce( 
					case 
					when KornerWait < 0 then 0
					else KornerWait
					end
					,0
				)
			) 

		,WaitingListCode = 'TRA'

		,WithRTTOpenPathway = 
			coalesce (
				 WithRTTOpenPathway
				,0
			)

		,ContextCode = 'TRA||' + 'UG' --InterfaceCode --NULL Needs fixing

		,CCGCode = 
				coalesce(
					 CCGPractice.ParentOrganisationCode
					,CCGPostcode.CCGCode
					,'02A'
				)

		,ResidenceCCGCode = CCGPostcode.CCGCode

	FROM 
		[$(TraffordWarehouse)].dbo.OPWaitingList Encounter

	left join [$(Organisation)].dbo.CCGPractice CCGPractice
	on CCGPractice.OrganisationCode = Encounter.RegisteredGpPracticeCode


	left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
	on	CCGPostcode.Postcode = 
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end	

	left join
		(
		select 
			AppointmentTypeID =
				apptype_identity
		from
			[$(TraffordReferenceData)].dbo.AppointmentType
		where 
		  apptype_code in ('NewWD', 'FNTrust')
		) FollowupAppointments
	 on FollowupAppointments.AppointmentTypeID = Encounter.AppointmentTypeCode

	where 
		dateadd(d,-1,Encounter.CensusDate) IN
		(
		select 
			Calendar.TheDate
		from
			WH.Calendar
		where
			Calendar.TheDate in
				(
				select
					max(TheDate)
				from
					WH.Calendar
				where
					TheDate between DATEADD(year, -2, getdate()) and getdate()
				group by
					 year(TheDate)
					,month(TheDate)

				union
--snapshots in the last fourteen days
				select
					TheDate
				from
					WH.Calendar
				where
					TheDate between dateadd(day , -14 , getdate()) and getdate()

				union
--Sunday snapshots
				select
					TheDate
				from
					WH.Calendar
				where
					datename(dw, TheDate) = 'Sunday'
				and	TheDate between DATEADD(year, -2, getdate()) and getdate()
				)

--remove this to allow updating of snapshots
		and	not exists
			(
			select
				1
			from
				OPWL.BaseEncounter
			where
				BaseEncounter.CensusDate = Calendar.TheDate
			and BaseEncounter.ContextCode =  'TRA||UG'
			)
		)
	) Encounter








﻿







CREATE view [ETL].[TLoadPEXOnlineBaseResponse]

as

select
	 ResponseRecno = [OnlineResponseRecno]
	,[SurveyTakenID]
	,[SurveyID]
	,[QuestionID]
	,[AnswerID]
	,Answer
	,[SurveyDate]
	,[SurveyTime]
	,[LocationID]
	,WardID
	,[ChannelID]
	,[QuestionTypeID]
	,[LocationTypeID]
	,ContextCode = 'CMFT||CRT'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[PEX].[OnlineResponse]






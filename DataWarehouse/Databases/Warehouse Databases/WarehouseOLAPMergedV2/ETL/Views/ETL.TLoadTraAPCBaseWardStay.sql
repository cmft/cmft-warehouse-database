﻿create VIEW ETL.TLoadTraAPCBaseWardStay AS


SELECT 
	 EncounterRecno = WardStay.WardStayRecno	 
	,SourceUniqueID = WardStay.SourceUniqueID	
	,SourcePatientNo = Encounter.DistrictNo	
	,SourceSpellNo = Encounter.ProviderSpellNo	
	,ProviderSpellNo = Encounter.ProviderSpellNo	
	,StartDate = CONVERT(DATE, WardStay.StartDate)	
	,EndDate = CONVERT(DATE, WardStay.EndDate)	
	,StartTime = WardStay.StartDate	
	,EndTime = WardStay.EndDate	
	,SiteCode = 'RW3TR'	
	,WardCode = WardStay.WardCode	
	,StartActivityCode = WardStay.SequenceNo
	,EndActivityCode = null
	,Created
	,Updated
	,ByWhom	
	,ContextCode = 'TRA||UG'
from
	[$(TraffordWarehouse)].dbo.WardStay WardStay

INNER JOIN [$(TraffordWarehouse)].dbo.APCEncounter Encounter
ON	WardStay.ProviderSpellNo = Encounter.ProviderSpellNo
AND	Encounter.EpisodeNo = 1
AND	Encounter.IsDeleted = 0


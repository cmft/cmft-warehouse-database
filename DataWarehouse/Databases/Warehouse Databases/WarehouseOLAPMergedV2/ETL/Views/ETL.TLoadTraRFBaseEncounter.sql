﻿





CREATE view [ETL].[TLoadTraRFBaseEncounter] as

select 
	 RFEncounter.EncounterRecno
	,RFEncounter.SourceUniqueID
	,RFEncounter.SourcePatientNo
	,RFEncounter.SourceEncounterNo
	,RFEncounter.PatientTitle
	,RFEncounter.PatientForename
	,RFEncounter.PatientSurname
	,RFEncounter.DateOfBirth
	,RFEncounter.DateOfDeath
	,RFEncounter.SexCode
	,RFEncounter.NHSNumber
	,RFEncounter.DistrictNo
	,RFEncounter.Postcode
	,RFEncounter.PatientAddress1
	,RFEncounter.PatientAddress2
	,RFEncounter.PatientAddress3
	,RFEncounter.PatientAddress4
	,RFEncounter.DHACode
	,RFEncounter.EthnicOriginCode
	,RFEncounter.MaritalStatusCode
	,RFEncounter.ReligionCode
	,RegisteredGpCode =
		coalesce(RFEncounter.RegisteredGpCode,	'G9999998')
	,RegisteredGpPracticeCode =
		coalesce(RFEncounter.RegisteredGpPracticeCode, 'V81999')
	,EpisodicGpCode =
		coalesce(RFEncounter.EpisodicGpCode,	'G9999998')
	,EpisodicGpPracticeCode =
		coalesce(RFEncounter.EpisodicGpPracticeCode, 'V81999')
	,RFEncounter.EpisodicGdpCode
	,RFEncounter.SiteCode
	,RFEncounter.ConsultantCode
	,RFEncounter.SpecialtyCode
	,RFEncounter.SourceOfReferralCode
	,RFEncounter.PriorityCode
	,RFEncounter.ReferralDate
	,RFEncounter.DischargeDate
	,RFEncounter.DischargeTime
	,RFEncounter.DischargeReasonCode
	,RFEncounter.DischargeReason
	,RFEncounter.AdminCategoryCode
	,RFEncounter.ContractSerialNo
	,RFEncounter.RTTPathwayID
	,RFEncounter.RTTPathwayCondition
	,RFEncounter.RTTStartDate
	,RFEncounter.RTTEndDate
	,RFEncounter.RTTSpecialtyCode
	,RFEncounter.RTTCurrentProviderCode
	,RFEncounter.RTTCurrentStatusCode
	,RFEncounter.RTTCurrentStatusDate
	,RFEncounter.RTTCurrentPrivatePatientFlag
	,RFEncounter.RTTOverseasStatusFlag
	,RFEncounter.NextFutureAppointmentDate
	,RFEncounter.ReferralComment
	,RFEncounter.InterfaceCode
	,RFEncounter.Created
	,RFEncounter.Updated
	,RFEncounter.ByWhom
	,RFEncounter.ReasonForReferralCode
	,RFEncounter.SourceOfReferralGroupCode
	,RFEncounter.DirectorateCode
	,RFEncounter.CasenoteNo
	,RFEncounter.PCTCode
	,AgeCode =
		case
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) is null then 'Age Unknown'
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) <= 0 then '00 Days'
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) = 1 then '01 Day'
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) > 1 and
		datediff(day, DateOfBirth, RFEncounter.ReferralDate) <= 28 then
			right('0' + Convert(Varchar,datediff(day, DateOfBirth, RFEncounter.ReferralDate)), 2) + ' Days'
		
		when datediff(day, DateOfBirth, RFEncounter.ReferralDate) > 28 and
		datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end
			 < 1 then 'Between 28 Days and 1 Month'
		when datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end = 1
			then '01 Month'
		when datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end > 1 and
		 datediff(month, DateOfBirth, RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end <= 23
		then
		right('0' + Convert(varchar,datediff(month, DateOfBirth,RFEncounter.ReferralDate) -
		case  when datepart(day, DateOfBirth) > datepart(day, RFEncounter.ReferralDate) then 1 else 0 end), 2) + ' Months'
		when datediff(yy, DateOfBirth, RFEncounter.ReferralDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, RFEncounter.ReferralDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, RFEncounter.ReferralDate) 
			And	datepart(d, DateOfBirth) > datepart(d, RFEncounter.ReferralDate)
			) then 1 else 0 end
		) > 99 then '99+'
		else right('0' + convert(varchar, datediff(yy, DateOfBirth, RFEncounter.ReferralDate) - 
		(
		case 
		when	(datepart(m, DateOfBirth) > datepart(m, RFEncounter.ReferralDate)) 
		or
			(
				datepart(m, DateOfBirth) = datepart(m, RFEncounter.ReferralDate) 
			And	datepart(d, DateOfBirth) > datepart(d, RFEncounter.ReferralDate)
			) then 1 else 0 end
		)), 2) + ' Years'

		end

	,RFEncounter.ContextCode

	,RFEncounter.ResidenceCCGCode
	,RFEncounter.CCGCode
from
	(
	SELECT
		 EncounterRecno = ENCOUNTERRECNO
		,SourceUniqueID
		,SourcePatientNo = 
			DistrictNo
		,SourceEncounterNo = ROW_NUMBER() OVER(PARTITION BY DistrictNo ORDER BY OriginalRTTStartDate)
		,PatientTitle = 
			case when Title = '' then null
			else convert(varchar(10),null) 
			end
		,PatientForename = 
			convert(varchar(20),null)
		,PatientSurname = 
			convert(varchar(30),null)
		,DateOfBirth = 
			convert(datetime,null)
		,DateOfDeath = 
			convert(smalldatetime,null)
		,SexCode =  
			convert(varchar(1),null)
		,NHSNumber = 
			CONVERT(varchar(17),NHSNumber)
		,DistrictNo =
			CONVERT(varchar(12),DistrictNo)
		,Postcode =
			CONVERT(varchar(12),Referrals.PostCode)
		,PatientAddress1 =  
			CONVERT(varchar(25),NULL)
		,PatientAddress2 =  
			CONVERT(varchar(25),NULL)
		,PatientAddress3 =  
			CONVERT(varchar(25),NULL)
		,PatientAddress4 =  
			CONVERT(varchar(25),NULL)
		,DHACode =  
			CONVERT(varchar(3),NULL)
		,EthnicOriginCode =  
			CONVERT(varchar(4),NULL)
		,MaritalStatusCode =  
			CONVERT(varchar(1),NULL)
		,ReligionCode =  
			CONVERT(varchar(4),NULL)
		,RegisteredGpCode = 'G9999998'
		,RegisteredGpPracticeCode
		,EpisodicGpCode = 'G9999998'
		,EpisodicGpPracticeCode = 
			RegisteredGpPracticeCode
		,EpisodicGdpCode = 'D9999998'
		,SiteCode =  
			CONVERT(varchar(5),SiteCode)
		,ConsultantCode =  
			CONVERT(varchar(12),ConsultantCode)
		,SpecialtyCode =  
			CONVERT(varchar(5),TreatmentFunctionCode)
		,SourceOfReferralCode =  
			CONVERT(varchar(5), SourceOfReferralCode)
		,PriorityCode =  
			CONVERT(varchar(5), PriorityCode)
		,ReferralDate = 
			OriginalRTTStartDate
		,DischargeDate =  
			CONVERT(date,NULL)
		,DischargeTime =  
			CONVERT(datetime,NULL)
		,DischargeReasonCode =  
			CONVERT(varchar(30), NULL)
		,DischargeReason =  
			CONVERT(varchar(30), NULL)
		,AdminCategoryCode =  
			CONVERT(varchar(3), NULL)
		,ContractSerialNo =  
			CONVERT(varchar(6), NULL)
		,RTTPathwayID = 
			PathwayId
		,RTTPathwayCondition =  
			CONVERT(varchar(20), NULL)
		,RTTStartDate = OriginalRTTStartDate
		,RTTEndDate =  
			CONVERT(smalldatetime, NULL)
		,RTTSpecialtyCode =  
			CONVERT(varchar(10), NULL)
		,RTTCurrentProviderCode = 
			'RW3'
		,RTTCurrentStatusCode =  
			CONVERT(varchar(10), NULL)
		,RTTCurrentStatusDate = '1 APr 2012'
		,RTTCurrentPrivatePatientFlag =  
			CONVERT(bit, NULL)
		,RTTOverseasStatusFlag =  
			CONVERT(bit, NULL)
		,NextFutureAppointmentDate = '1 APr 2012'
		,ReferralComment =  
			CONVERT(varchar(25), NULL)
		,InterfaceCode = 'UG'
		,Created = 
			LoadDate
		,Updated = 
			LoadDate
		,ByWhom = NULL
		,ReasonForReferralCode = 
			CONVERT(varchar(10), ReferralReason)
			
		,SourceOfReferralGroupCode =  
			CONVERT(varchar(10), NULL)
		,DirectorateCode = '80'
		,CasenoteNo = Referrals.EPMI + '/' + DistrictNo
		,PCTCode =  
			CONVERT(nvarchar(6),PCTCode)
		,ContextCode = 'TRA||' + 'UG' --Referrals.InterfaceCode

		,ResidenceCCGCode = EpisodicPostcode.CCGCode

		,CCGCode =
			coalesce(
				 Practice.ParentOrganisationCode
				,EpisodicPostcode.CCGCode
				,'00W'
			)

	FROM
		[$(TraffordWarehouse)].dbo.Referrals Referrals

	left join [$(Organisation)].ODS.Practice
	on	Practice.OrganisationCode = Referrals.RegisteredGpPracticeCode

	left join [$(Organisation)].dbo.CCGPostcode EpisodicPostcode on
			EpisodicPostcode.Postcode = 
				case
				when len(Referrals.PostCode) = 8 then Referrals.PostCode
				else left(Referrals.PostCode, 3) + ' ' + right(Referrals.PostCode, 4) 
				end

--left join

--(
--select
-- EPMI = [SecondaryHISId_Number]
-- ,PatientTitle = [Title]
--,PatientSurname = [FamilyName]
--,PatientForename = [GivenName]
--,DateOfBirth = [BirthDate]
--,DateOfDeath = [DeathDate]
--from
--Warehousesql.Replica_UltraGendaFoundation.dbo.Patient Patient




--left join Warehousesql.Replica_UltraGendaFoundation.dbo.Person Person
--on	Person.Id = Patient.PersonId

--and Patient.IsValid = 1
--) Patient
--on
--Patient.Epmi = Referrals.epmi


	) RFEncounter







﻿
create view ETL.TLoadCenObservationBaseAdmission as

select
	AdmissionRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SexID
	,Surname
	,Forename
	,AdmissionTime
	,StartSpecialtyID
	,StartLocationID
	,DischargeTime
	,EndSpecialtyID
	,EndLocationID
	,TreatmentOutcomeID
	,DischargeDestinationID
	,Comment
	,CreatedByUserID
	,CreatedTime
	,LastModifiedByUserID
	,LastModifiedTime
	,ContextCode = 'CEN||PTRACK'
	,Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].Observation.Admission









﻿





CREATE View [ETL].[TImportMortalityBaseReviewGynaecology]


as


Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode
						,RequiresFurtherInvestigation
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode = CAST(PrimaryReviewerCode as varchar(50))
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade = null
		,RequiresFurtherInvestigation
		,QuestionNo = row_number() over (partition by SourceUniqueID order by SourceUniqueID)
		,Question
		,Response
		,ContextCode = 'CMFT||MORTARCHV'
	from

		(
		select
			SourceUniqueID
			,SourcePatientNo	
			,EpisodeStartTime	
			,APCContextCode = ContextCode
			,FormTypeCode = FormTypeID
			,ReviewStatusCode = ReviewStatus
			,PrimaryReviewerAssignedTime = ReviewedDate
			,PrimaryReviewerCompletedTime = 
				case
					when ReviewStatus = 10 then ReviewedDate
					else null
				end
			,PrimaryReviewerCode = ReviewedBy
			,SecondaryReviewerAssignedTime = null
			,SecondaryReviewerCompletedTime = null
			,SecondaryReviewerCode = null
			,RequiresFurtherInvestigation = 
					case
						when DeathPreventable = 1 then 1
						else 0
					end			
			,ClinicalObs = isnull(cast(ClinicalObs as varchar(max)),'')
			,ClinicalObsDetail = cast(ClinicalObsDetail as varchar(max))
			,CaseDetail = cast(CaseDetail as varchar(max))
			,DNR = cast(DNR as varchar(max))
			,DNRDetail = cast(DNRDetail as varchar(max))
			,DeathPreventable = cast(DeathPreventable as varchar(max))
			,DeathPreventableComment = cast(DeathPreventableComment as varchar(max))
			,NursingConcernsClinicalObs = cast(NursingConcernsClinicalObs as varchar(max))
			,NursingConcernsClinicalObsDetail = cast(NursingConcernsClinicalObsDetail as varchar(max))
			,InitialManagementStepsAppropriateAndAdequate = cast(InitialManagementStepsAppropriateAndAdequate as varchar(max))
			,InitialManagementStepsAppropriateAndAdequateComment = cast(InitialManagementStepsAppropriateAndAdequateComment as varchar(max))
			,Palliative = cast(Palliative as varchar(max))
			,PalliativeDetail = cast(PalliativeDetail as varchar(max))
			,NursingConcernsRoleInDeath = cast(NursingConcernsRoleInDeath as varchar(max))
			,NursingConcernsRoleInDeathDetail = cast(NursingConcernsRoleInDeathDetail as varchar(max))
			,TransferICU = cast(TransferICU as varchar(max))
			,TransferICUDetail = cast(TransferICUDetail as varchar(max))
			,TransferHDU = cast(TransferHDU as varchar(max))
			,TransferHDUDetail = cast(TransferHDUDetail as varchar(max))
			,Miscomm = cast(Miscomm as varchar(max))
			,MiscommDetail = cast(MiscommDetail as varchar(max))
			,PoorCommPrimaryandSecondary = cast(PoorCommPrimaryandSecondary as varchar(max))
			,PoorCommPrimaryandSecondaryDDetail = cast(PoorCommPrimaryandSecondaryDDetail as varchar(max))
			,NotableGoodQualityCare = cast(NotableGoodQualityCare as varchar(max))
			,EndOfLifeCareLCPInPatientNotes = cast(EndOfLifeCareLCPInPatientNotes as varchar(max))
			,EndOfLifeCareLCPApproTime = cast(EndOfLifeCareLCPApproTime as varchar(max))
			,EndOfLifeCareLCPApproTimeComment = cast(EndOfLifeCareLCPApproTimeComment as varchar(max))
			,EndOfLifeCarePreferredPlaceOfDeathRecorded = cast(EndOfLifeCarePreferredPlaceOfDeathRecorded as varchar(max))
			,EndOfLifeCarePreferredPlaceOfDeathRecordedComment = cast(EndOfLifeCarePreferredPlaceOfDeathRecordedComment as varchar(max))
			,LessonsLearned = cast(LessonsLearned as varchar(max))
			,ActionPlan = cast(ActionPlan as varchar(max))
			,DateOfBirth = cast(DateOfBirth as varchar(max))
			,ConsultantDiscussionB4WDofIC = cast(ConsultantDiscussionB4WDofIC as varchar(max))
			,ConsultantDiscussionB4WDofICComment = cast(ConsultantDiscussionB4WDofICComment as varchar(max))
			,EWSgteq3 = cast(EWSgteq3 as varchar(max))
			,EWSgteq3Detail = cast(EWSgteq3Detail as varchar(max))
			,ManageOutOfHospital = cast(ManageOutOfHospital as varchar(max))
			,WhereOutOfHospital = cast(WhereOutOfHospital as varchar(max))
			,PalliativeCareTeamInvolvement = cast(PalliativeCareTeamInvolvement as varchar(max))
			,PalliativeCareTeamInvolvementNoDetail = cast(PalliativeCareTeamInvolvementNoDetail as varchar(max))
			,AdmittedOutOfHours = cast(AdmittedOutOfHours as varchar(max))
			,TransferredOtherHospitalSiteInUnstableCondition = cast(TransferredOtherHospitalSiteInUnstableCondition as varchar(max))
			,TransferredOtherHospitalSiteInUnstableConditionDetail = cast(TransferredOtherHospitalSiteInUnstableConditionDetail as varchar(max))
			,ClinicianNotListeningToPatient = cast(ClinicianNotListeningToPatient as varchar(max))
			,ClinicianNotListeningtoPatientDetail = cast(ClinicianNotListeningtoPatientDetail as varchar(max))
			,PreviousFindingsEBMorHLI = cast(PreviousFindingsEBMorHLI as varchar(max))

		from
			[$(Warehouse)].Mortality.Gynaecology
		) 
		Mortality

		unpivot
				(
				Response for Question in 
						(
						ClinicalObs
						,ClinicalObsDetail
						,CaseDetail
						,DNR
						,DNRDetail
						,DeathPreventable
						,DeathPreventableComment
						,NursingConcernsClinicalObs
						,NursingConcernsClinicalObsDetail
						,InitialManagementStepsAppropriateAndAdequate
						,InitialManagementStepsAppropriateAndAdequateComment
						,Palliative
						,PalliativeDetail
						,NursingConcernsRoleInDeath
						,NursingConcernsRoleInDeathDetail
						,TransferICU
						,TransferICUDetail
						,TransferHDU
						,TransferHDUDetail
						,Miscomm
						,MiscommDetail
						,PoorCommPrimaryandSecondary
						,PoorCommPrimaryandSecondaryDDetail
						,NotableGoodQualityCare
						,EndOfLifeCareLCPInPatientNotes
						,EndOfLifeCareLCPApproTime
						,EndOfLifeCareLCPApproTimeComment
						,EndOfLifeCarePreferredPlaceOfDeathRecorded
						,EndOfLifeCarePreferredPlaceOfDeathRecordedComment
						,LessonsLearned
						,ActionPlan
						,DateOfBirth
						,ConsultantDiscussionB4WDofIC
						,ConsultantDiscussionB4WDofICComment
						,EWSgteq3
						,EWSgteq3Detail
						,ManageOutOfHospital
						,WhereOutOfHospital
						,PalliativeCareTeamInvolvement
						,PalliativeCareTeamInvolvementNoDetail
						,AdmittedOutOfHours
						,TransferredOtherHospitalSiteInUnstableCondition
						,TransferredOtherHospitalSiteInUnstableConditionDetail
						,ClinicianNotListeningToPatient
						,ClinicianNotListeningtoPatientDetail
						,PreviousFindingsEBMorHLI

						)
				) 
				Response 
			)
			Gynaecology





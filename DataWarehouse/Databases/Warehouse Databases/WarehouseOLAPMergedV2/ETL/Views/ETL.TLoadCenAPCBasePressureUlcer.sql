﻿
create view ETL.TLoadCenAPCBasePressureUlcer

as

select
	[PressureUlcerRecno]
	,[SourceUniqueID]
	,[SourcePatientNo]
	,[SourceSpellNo]
	,[LocationID]
	,[IdentifiedDate]
	,[IdentifiedTime]
	,[HealedDate]
	,[HealedTime]
	,[CategoryID]
	,[Validated]
	,[OccuredOnAnotherWard]
	,[WardCode]
	,[InterfaceCode]
	,ContextCode = 'CEN||BEDMAN'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[APC].[PressureUlcer]
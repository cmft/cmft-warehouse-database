﻿


CREATE view [ETL].[TLoadCenPatientBaseSpecialRegister]
as

/****************************************************************************************
	View		: [ETL].[TLoadCenPatientBaseSpecialRegister]
	Description	: Standard ETL view

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	22/08/2014	Paul Egan       Initial Coding
	24/07/2015	RR				Updated
*****************************************************************************************/

select
	SpecialRegisterRecNo
	,SourcePatientNo
	,SpecialRegisterCode
	,DistrictNo
	,NHSNumber
	,EnteredDate
	,ActivityTime
	,SourceEpisodeNo
	,ContextCode = 'CEN||PAS'
from
	[$(Warehouse)].PAS.PatientSpecialRegister


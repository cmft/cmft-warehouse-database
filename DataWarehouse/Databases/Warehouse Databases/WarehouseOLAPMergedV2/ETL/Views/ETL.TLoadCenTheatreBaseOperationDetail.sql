﻿


CREATE view [ETL].[TLoadCenTheatreBaseOperationDetail] as

select
	 OperationDetail.DistrictNo
	,OperationDetail.SourceUniqueID
	,OperationDetail.PatientAgeInYears
	,OperationDetail.OperationDay
	,OperationDetail.OperationStartDate
	,OperationDetail.TheatreCode
	,OperationDetail.OperationDate
	,OperationDetail.OperationEndDate
	,OperationDetail.DateOfBirth

	,OperationTypeCode =
		case
		when OperationDetail.OperationTypeCode = ''
		then null
		else OperationDetail.OperationTypeCode
		end

	,OperationDetail.OperationOrder
	,OperationDetail.Forename
	,OperationDetail.InSuiteTime
	,OperationDetail.Surname
	,OperationDetail.InAnaestheticTime
	,OperationDetail.InRecoveryTime
	,OperationDetail.BiohazardFlag
	,OperationDetail.ASAScoreCode
	,OperationDetail.UnplannedReturnReasonCode
	,OperationDetail.ReviewFlag
	,OperationDetail.OperationDetailSourceUniqueID
	,Operation = CAST(OperationDetail.Operation as varchar(max))
	,OperationDetail.PatientDiedFlag
	,OperationDetail.CauseOfDeath
	,OperationDetail.DiagnosisCode
	,OperationDetail.PatientAgeInYearsMonths
	,OperationDetail.PatientAgeInYearsDays
	,OperationDetail.SexCode
	,OperationDetail.TransferredToWardCode
	,OperationDetail.PatientSourceUniqueNo
	,OperationDetail.PatientClassificationCode
	,OperationDetail.PatientBookingSourceUniqueID
	,OperationDetail.SpecialtyCode
	,OperationDetail.FromWardCode
	,OperationDetail.ToWardCode
	,OperationDetail.ConsultantCode
	,OperationDetail.Address1
	,OperationDetail.Address3
	,OperationDetail.Postcode
	,OperationDetail.HomeTelephone
	,OperationDetail.BusinessTelephone
	,OperationDetail.Surgeon1Code
	,OperationDetail.Surgeon2Code
	,OperationDetail.Surgeon3Code
	,OperationDetail.Anaesthetist1Code
	,OperationDetail.Anaesthetist2Code
	,OperationDetail.Anaesthetist3Code
	,OperationDetail.ScoutNurseCode
	,OperationDetail.AnaesthetistNurseCode
	,OperationDetail.InstrumentNurseCode
	,OperationDetail.Technician1Code
	,OperationDetail.Technician2Code
	,OperationDetail.Technician3Code
	,OperationDetail.SessionNumber
	,OperationDetail.CampusCode
	,OperationDetail.SpecimenCode
	,OperationDetail.DischargeTime
	,OperationDetail.StaffInChargeCode
	,OperationDetail.EscortReturnedTime
	,OperationDetail.OperationCancelledFlag
	,OperationDetail.DelayCode
	,OperationDetail.AnaestheticInductionTime
	,OperationDetail.ReadyToDepartTime
	,OperationDetail.OtherCommentCode
	,OperationDetail.PACUClassificationCode
	,OperationDetail.PostOperationInstructionCode
	,OperationDetail.ClinicalPriorityCode
	,OperationDetail.EmergencyOperationSurgeonCode
	,OperationDetail.SecondaryStaffCode
	,OperationDetail.HospitalDischargeDate
	,OperationDetail.HospitalEpisodeNo
	,OperationDetail.UnplannedReturnToTheatreFlag
	,OperationDetail.AdmissionTime
	,OperationDetail.WaitingListSourceUniqueNo
	,OperationDetail.AdmissionTypeCode
	,OperationDetail.CalfCompressorUsedFlag
	,OperationDetail.OperatingRoomReadyTime
	,OperationDetail.DressingTime
	,OperationDetail.NursingSetupCompleteTime
	,OperationDetail.LastUpdated
	,OperationDetail.ProceduralStatusCode
	,OperationDetail.ACCNumber
	,OperationDetail.ACCIndicatorCode
	,OperationDetail.ACCCode
	,OperationDetail.UpdatedFlag
	,OperationDetail.PatientAddressStatus
	,OperationDetail.OperationKey
	,OperationDetail.ExtubationTime
	,OperationDetail.Address2
	,OperationDetail.Address4
	,OperationDetail.SentForTime
	,OperationDetail.PorterLeftTime
	,OperationDetail.PorterCode
	,OperationDetail.SecondaryTheatreCode
	,OperationDetail.SecondaryConsultantCode
	,OperationDetail.SecondaryOrderNumberCode
	,OperationDetail.SecondarySessionNumber
	,OperationDetail.AdditionalFlag
	,OperationDetail.DictatedSurgeonNoteCode
	,OperationDetail.AnaestheticReadyTime
	,OperationDetail.PreparationReadyTime
	,OperationDetail.SessionSourceUniqueID
	,OperationDetail.CombinedCaseSessionSourceUniqueID
	,OperationDetail.IncisionDetailCode

	,NextOperationAnaestheticInductionTime = 
		case
		when OperationDetail.SessionSourceUniqueID = 0
		then null

		else
			(
			select
				NextOperation.AnaestheticInductionTime
			from
				[$(Warehouse)].Theatre.OperationDetail NextOperation
			where
				NextOperation.SessionSourceUniqueID = OperationDetail.SessionSourceUniqueID
			and	NextOperation.AnaestheticInductionTime is not null
			and	NextOperation.SessionSourceUniqueID <> 0
			and	NextOperation.AnaestheticInductionTime > OperationDetail.AnaestheticInductionTime
			and not exists
				(
				select
					1
				from
					[$(Warehouse)].Theatre.OperationDetail LaterOperation
				where
					LaterOperation.SessionSourceUniqueID = OperationDetail.SessionSourceUniqueID
				and	LaterOperation.AnaestheticInductionTime is not null
				and	LaterOperation.AnaestheticInductionTime > OperationDetail.AnaestheticInductionTime
				and	LaterOperation.SessionSourceUniqueID <> 0
				and	(
						LaterOperation.AnaestheticInductionTime < NextOperation.AnaestheticInductionTime
					or	(
							LaterOperation.AnaestheticInductionTime = NextOperation.AnaestheticInductionTime
						and	LaterOperation.SourceUniqueID < NextOperation.SourceUniqueID
						)
					)
				)
			)
		end

from
	[$(Warehouse)].Theatre.OperationDetail




﻿



CREATE View [ETL].[TImportMortalityBaseReviewNICU]


as


Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode 
						,RequiresFurtherInvestigation
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode = CAST(PrimaryReviewerCode as varchar(50))
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade = null
		,RequiresFurtherInvestigation
		,QuestionNo = row_number() over (partition by SourceUniqueID order by SourceUniqueID)
		,Question
		,Response
		,ContextCode = 'CMFT||MORTARCHV'
	from

		(
		select
			SourceUniqueID
			,SourcePatientNo	
			,EpisodeStartTime	
			,APCContextCode = ContextCode
			,FormTypeCode = FormTypeID
			,ReviewStatusCode = ReviewStatus
			,PrimaryReviewerAssignedTime = null
			,PrimaryReviewerCompletedTime = ReviewedDate
			,PrimaryReviewerCode = ReviewedBy
			,SecondaryReviewerAssignedTime = null
			,SecondaryReviewerCompletedTime = null
			,SecondaryReviewerCode = null
			,RequiresFurtherInvestigation = 
					case
						when DeathPreventable = 1 then 1
						else 0
					end	
			
			,TerminalCareDeathExpected = isnull(cast(TerminalCareDeathExpected as varchar(max)),'')
			,TerminalCareDeathExpectedComment = cast(TerminalCareDeathExpectedComment as varchar(max))
			,DeathPreventable = cast(DeathPreventable as varchar(max))
			,TransferComment = cast(TransferComment as varchar(max))
			,Postmortem = cast(Postmortem as varchar(max))
			,PostmortemComment = cast(PostmortemComment as varchar(max))
			,MedicationErrors = cast(MedicationErrors as varchar(max))
			,MedicationErrorsComment = cast(MedicationErrorsComment as varchar(max))
			,CommunicationComment = cast(CommunicationComment as varchar(max))
			,AnythingCouldBeDoneDifferentlyComment = cast(AnythingCouldBeDoneDifferentlyComment as varchar(max))
			,NotableGoodQualityCare = cast(NotableGoodQualityCare as varchar(max))
			,LessonsLearned = cast(LessonsLearned as varchar(max))
			,ReferenceNo = cast(ReferenceNo as varchar(max))
			,Gestation = cast(Gestation as varchar(max))
			,BirthWeight = cast(BirthWeight as varchar(max))
			,DateOfBirth = cast(DateOfBirth as varchar(max))
			,Sex = cast(Sex as varchar(max))
			,Ethnicity = cast(Ethnicity as varchar(max))
			,MRSA = cast(MRSA as varchar(max))
			,CriticalIncident = cast(CriticalIncident as varchar(max))
			,CriticalIncidentDetail = cast(CriticalIncidentDetail as varchar(max))
			,WithdrawalOfIC = cast(WithdrawalOfIC as varchar(max))
			,WithdrawalOfICDetail = cast(WithdrawalOfICDetail as varchar(max))
			,WithdrawalOfICLOTADNRFormsUsed = cast(WithdrawalOfICLOTADNRFormsUsed as varchar(max))
			,SeverelyAbnormalCUSS = cast(SeverelyAbnormalCUSS as varchar(max))
			,ConsultantDiscussionB4WDofIC = cast(ConsultantDiscussionB4WDofIC as varchar(max))
			,BereavementAppointmentSent = cast(BereavementAppointmentSent as varchar(max))
			,BereavementAppointmentSentDetail = cast(BereavementAppointmentSentDetail as varchar(max))
			,ClinicalSummaryBackground = cast(ClinicalSummaryBackground as varchar(max))
			,ClinicalSummaryProgress = cast(ClinicalSummaryProgress as varchar(max))
			,ClinicalSummaryManagement = cast(ClinicalSummaryManagement as varchar(max))
			,ClinicalSummarySurgical = cast(ClinicalSummarySurgical as varchar(max))
			,DiscussionObstetricIssues = cast(DiscussionObstetricIssues as varchar(max))
			,DiscussionResus = cast(DiscussionResus as varchar(max))
			,DiscussionFirst48Hours = cast(DiscussionFirst48Hours as varchar(max))
			,DiscussionClinicalManagement = cast(DiscussionClinicalManagement as varchar(max))
			,DiscussionCommunication = cast(DiscussionCommunication as varchar(max))
			,DiscussionDocumentation = cast(DiscussionDocumentation as varchar(max))

		from
			[$(Warehouse)].Mortality.NICU
		) 
		Mortality

		unpivot
				(
				Response for Question in 
						(
						TerminalCareDeathExpected
						,TerminalCareDeathExpectedComment
						,DeathPreventable
						,TransferComment
						,Postmortem
						,PostmortemComment
						,MedicationErrors
						,MedicationErrorsComment
						,CommunicationComment
						,AnythingCouldBeDoneDifferentlyComment
						,NotableGoodQualityCare
						,LessonsLearned
						,ReferenceNo
						,Gestation
						,BirthWeight
						,DateOfBirth
						,Sex
						,Ethnicity
						,MRSA
						,CriticalIncident
						,CriticalIncidentDetail
						,WithdrawalOfIC
						,WithdrawalOfICDetail
						,WithdrawalOfICLOTADNRFormsUsed
						,SeverelyAbnormalCUSS
						,ConsultantDiscussionB4WDofIC
						,BereavementAppointmentSent
						,BereavementAppointmentSentDetail
						,ClinicalSummaryBackground
						,ClinicalSummaryProgress
						,ClinicalSummaryManagement
						,ClinicalSummarySurgical
						,DiscussionObstetricIssues
						,DiscussionResus
						,DiscussionFirst48Hours
						,DiscussionClinicalManagement
						,DiscussionCommunication
						,DiscussionDocumentation
						)
				) 
				Response
			)
			NICU 



﻿






CREATE view [ETL].[TLoadPEXPostcardBaseResponse]

as
	
select
	ResponseRecno = [PostcardResponseRecno]
	,[SurveyTakenID]
	,[SurveyID]
	,[QuestionID]
	,[AnswerID]
	,Answer
	,[SurveyDate]
	,[SurveyTime]
	,[LocationID]
	,WardID
	,[ChannelID]
	,[QuestionTypeID]
	,[LocationTypeID]
	,ContextCode = 'CMFT||CRT'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[PEX].[PostcardResponse]		







﻿





CREATE View [ETL].[TImportMortalityBaseReviewSurgery]


as


Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode 
						,ClassificationGrade
						,RequiresFurtherInvestigation
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode = CAST(PrimaryReviewerCode as varchar(50))
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade
		,RequiresFurtherInvestigation
		,QuestionNo = row_number() over (partition by SourceUniqueID order by SourceUniqueID)
		,Question
		,Response
		,ContextCode = 'CMFT||MORTARCHV'
	from
		(
		select
			SourceUniqueID
			,SourcePatientNo	
			,EpisodeStartTime	
			,APCContextCode = ContextCode
			,FormTypeCode = FormTypeID
			,ReviewStatusCode = ReviewStatus
			,PrimaryReviewerAssignedTime = ReviewedDate
			,PrimaryReviewerCompletedTime = 
				case
					when ReviewStatus = 10 then ReviewedDate
					else null
				end
			,PrimaryReviewerCode = ReviewedBy
			,SecondaryReviewerAssignedTime = null
			,SecondaryReviewerCompletedTime = null
			,SecondaryReviewerCode = null
			,ClassificationGrade
			,RequiresFurtherInvestigation = 
					case
						when DeathPreventable = 1 then 1
						when ClassificationGrade in (2,3) then 1
						else 0
					end
			
			,DateOfDeath = isnull(cast(DateOfDeath as varchar(max)),'')
			,RecordedAppropriatelyToYou = cast(RecordedAppropriatelyToYou as varchar(max))
			,AlternateNamedConsultant = cast(AlternateNamedConsultant as varchar(max))
			,TerminalCareDeathExpected = cast(TerminalCareDeathExpected as varchar(max))
			,AgreeWithCauseOfDeath = cast(AgreeWithCauseOfDeath as varchar(max))
			,DidSurgeryDuringLastAdmission = cast(DidSurgeryDuringLastAdmission as varchar(max))
			,HowManyDaysBeforeDeathApprox = cast(HowManyDaysBeforeDeathApprox as varchar(max))
			,CoronerInformed = cast(CoronerInformed as varchar(max))
			,HighLevelInvestigation = cast(HighLevelInvestigation as varchar(max))
			,CaseReferredToBleepReview = cast(CaseReferredToBleepReview as varchar(max))
			,SurgicalComplicationContributedToDeath = cast(SurgicalComplicationContributedToDeath as varchar(max))
			,SurgeryHigherThanNormalAnticipatedMortality = cast(SurgeryHigherThanNormalAnticipatedMortality as varchar(max))
			,StandardRiskPercentage = cast(StandardRiskPercentage as varchar(max))
			,MortalityQuotedPercentage = cast(MortalityQuotedPercentage as varchar(max))
			,DeathPreventable = cast(DeathPreventable as varchar(max))
			,DeathPreventableComment = cast(DeathPreventableComment as varchar(max))
			,TerminalCareManagedWell = cast(TerminalCareManagedWell as varchar(max))
			,TerminalCareManagedWellComment = cast(TerminalCareManagedWellComment as varchar(max))
			,CauseOfDeath1 = cast(CauseOfDeath1 as varchar(max))
			,CauseOfDeath2 = cast(CauseOfDeath2 as varchar(max))
			,CauseOfDeath3 = cast(CauseOfDeath3 as varchar(max))
			,CauseOfDeath4 = cast(CauseOfDeath4 as varchar(max))
			,CoMorbidity1 = cast(CoMorbidity1 as varchar(max))
			,CoMorbidity2 = cast(CoMorbidity2 as varchar(max))
			,CoMorbidity3 = cast(CoMorbidity3 as varchar(max))
			,CoMorbidity4 = cast(CoMorbidity4 as varchar(max))
			,CoMorbidity5 = cast(CoMorbidity5 as varchar(max))
			,DeathDiscussedWithAnaesthetist = cast(DeathDiscussedWithAnaesthetist as varchar(max))
			,Anaesthetist = cast(Anaesthetist as varchar(max))
			
		from
			[$(Warehouse)].Mortality.Surgery
		) 
		Mortality

		unpivot
				(
				Response for Question in 
						(
						DateOfDeath
						,RecordedAppropriatelyToYou
						,AlternateNamedConsultant
						,TerminalCareDeathExpected
						,AgreeWithCauseOfDeath
						,DidSurgeryDuringLastAdmission
						,HowManyDaysBeforeDeathApprox
						,CoronerInformed
						,HighLevelInvestigation
						,CaseReferredToBleepReview
						,SurgicalComplicationContributedToDeath
						,SurgeryHigherThanNormalAnticipatedMortality
						,StandardRiskPercentage
						,MortalityQuotedPercentage
						,DeathPreventable
						,DeathPreventableComment
						,TerminalCareManagedWell
						,TerminalCareManagedWellComment
						,CauseOfDeath1
						,CauseOfDeath2
						,CauseOfDeath3
						,CauseOfDeath4
						,CoMorbidity1
						,CoMorbidity2
						,CoMorbidity3
						,CoMorbidity4
						,CoMorbidity5
						,DeathDiscussedWithAnaesthetist
						,Anaesthetist
						)
				) 
				Response 
			) 
			Surgery





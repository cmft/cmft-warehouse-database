﻿-- USE [WarehouseOLAPMergedV2]
-- drop view ETL.[TLoadTraAPCBaseDementia]

CREATE View [ETL].[TLoadTraAPCBaseDementia]

as

--/***************************************************************************
--**	20140701	RR	Create Dementia Trafford using GC script 
--**
--****************************************************************************/


--select SpellID,count(*) 
--from ETL.TLoadTraAPCBaseDementia
--group by spellid
--A12345	8
--A125478	2


WITH 
	XMLNAMESPACES (N'http://schemas.microsoft.com/office/infopath/2003/myXSD/2009-11-10T09:47:10' as my)


,DementiaTraCTE 
	
	(
	ProviderSpellNo
	,SpellID
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,RecordedDate
	,RecordedTime
	,KnownToHaveDementia
	,PatientDementiaFlag
	,RememberMePlan
	,RememberMePlanCommenced
	,RememberMePlanDiscontinued
	,ForgetfulnessQuestionAnswered
	,AMTTestPerformed
	,ClinicalIndicationsPresent
	,AMTScore
	,AMTTestNotPerformedReason
	,CognitiveInvestigationStarted
	,ReferralNeeded
	,DementiaReferralSentDate
	,SequenceNo
	,AdmissionWardCode
	,CurrentWardCode
		
	)
	as
	
		(
		select
			ProviderSpellNo	= AdmissionNumber
			,SpellID		= AdmissionNumber
			,SourceUniqueID = FormId
			,SourcePatientNo = FormXML.value('/my:myFields[1]/my:PatientDemographics[1]/my:DistrictNo[1]', 'varchar(200)') 
			,SourceSpellNo = AdmissionNumber
			,RecordedDate = cast(OriginalSubmissionDate as date)
			,RecordedTime = OriginalSubmissionDate
			,KnownToHaveDementia = 
					case 
						when FormXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') ='Yes' 
						then 1
						when FormXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') ='No' 
						then 0
						else null
					end	
			,PatientDementiaFlag = 
					case
						when FormXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') ='Yes' 
						then 1
						when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 7
						then 1
						else null
					end
			,RememberMePlan	= 
					case
						when FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed1[1]', 'varchar(200)') = 'True'
							or FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed2[1]', 'varchar(200)') = 'True'
							or FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed3[1]', 'varchar(200)') = 'True'
						then 1
						when FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed1[1]', 'varchar(200)') = 'False'
							And FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed2[1]', 'varchar(200)') = 'False'
							And FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed3[1]', 'varchar(200)') = 'False'
						then 0
						else null
					end
			,RememberMePlanCommenced = 
					coalesce(
						nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate1[1]','VARCHAR(60)'), '') 
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate2[1]','VARCHAR(60)'), '') 
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate3[1]','VARCHAR(60)'), '')
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate4[1]','VARCHAR(60)'), '')
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate5[1]','VARCHAR(60)'), '')
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate6[1]','VARCHAR(60)'), '')
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate7[1]','VARCHAR(60)'), '')
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate8[1]','VARCHAR(60)'), '')
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate9[1]','VARCHAR(60)'), '')
						,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate10[1]','VARCHAR(60)'), ''))
			,RememberMePlanDiscontinued	= null
			,ForgetfulnessQuestionAnswered = 
					case 
						when FormXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='Yes' 
						then 1
						when FormXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='No' 
						then 0
						else null
					end
			,AMTTestPerformed = 
					case
						when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
							and FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 10
						then 1
						when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'false'
						then 0
						else null
					end
			,ClinicalIndicationsPresent	= null
			,AMTScore = 
					case 
						when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
						and FormXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') <> 'No'
						then FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)')
						else Null
					end 
			,AMTTestNotPerformedReason = 
					case 
						when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
							and FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 10
						then null
						else nullif(FormXML.value('/my:myFields[1]/my:UnableSection[1]/my:UnableReason[1]', 'varchar(200)'),'')
					end
			,CognitiveInvestigationStarted = 
					case 
						when FormXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)') = 'true' 
						then 1
						when FormXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)') = 'false' 
						then 0
						else null 
					end
			,ReferralNeeded	= Null
			,DementiaReferralSentDate = 
					case 
						when FormXML.value('/my:myFields[1]/my:GPLetterConfirmSection[1]/my:GPLetterConfirmed[1]', 'varchar(200)') = 'true' 
						then DateAdded
						else null 
					end
			,SequenceNo = row_number ( ) over (partition by AdmissionNumber order by OriginalSubmissionDate asc)
			,AdmissionWardCode = AdmWard.WardCode
			,CurrentWardCode = CurrWard.WardCode
		
		--select *
		from 
			[$(TrustCache)].Dementia.Trafford_InfoPath_Forms DementiaData
		
		left join [APC].[BaseWardStay] AdmWard
		on AdmWard.ProviderSpellNo = DementiaData.AdmissionNumber
		and ContextCode = 'TRA||UG'
		and StartActivityCode = 1
		
		left join 
			(select *
			from
				[APC].[BaseWardStay] Ward
			where 
			ContextCode = 'TRA||UG'
			and not exists
				(select *
				from
					[APC].[BaseWardStay] WardLatest
				where 
				Ward.ProviderSpellNo = WardLatest.ProviderSpellNo
				and WardLatest.StartActivityCode > Ward.StartActivityCode
				)
			)
			CurrWard
		on CurrWard.ProviderSpellNo = DementiaData.AdmissionNumber
			)


 -- These records have an AMTScore A128224,A132048,A132332
select 
	DementiaTraCTE.ProviderSpellNo
	,DementiaTraCTE.SpellID
	,DementiaTraCTE.SourceUniqueID
	,DementiaTraCTE.SourcePatientNo
	,DementiaTraCTE.SourceSpellNo
	,OriginalSubmissionDate = DementiaTraCTE.RecordedDate
	,OriginalSubmissionTime = DementiaTraCTE.RecordedTime
	,DementiaTraCTE.AdmissionWardCode
	,DementiaTraCTE.CurrentWardCode
	,KnownDementiaRecordedDate = DementiaKnown.RecordedTime
	,KnownDementiaResponse = DementiaKnown.KnownToHaveDementia
	,ScreeningQuestionFirstResponseTime = DementiaQuestion.RecordedTime
	,ScreeningQuestionFirstResponse = DementiaQuestion.ForgetfulnessQuestionAnswered
	,ScreeningQuestionLatestPositiveResponseTime = DementiaAnswer.RecordedTime
	,AssessmentDate = cast(DementiaAssessment.RecordedTime as date)
	,AssessmentTime = DementiaAssessment.RecordedTime
	,AssessmentScore = DementiaAssessment.AMTScore
	,InvestigationDate = DementiaInvestigations.RecordedTime
	,AssessmentNotPerformedReason = 
			case
				when DementiaNoAssessment.AMTTestNotPerformedReason = '' then Null
				else DementiaNoAssessment.AMTTestNotPerformedReason 
			end
	,ReferralSentDate = DementiaReferral.DementiaReferralSentDate
	,InterfaceCode = 'InfoPathForms' 	
	,ContextCode = 'TRA||IPATH' 
from 
	DementiaTraCTE

left join
-- Q1 Numerator - Identify earliest recording within spell of KnownToHaveDementia
(
select 
	*
from 
	DementiaTraCTE Dementia
where 
	KnownToHaveDementia = 1
and not exists
	(select 
		1
	from
		DementiaTraCTE DementiaLatest
	where 
		DementiaLatest.SpellID = Dementia.SpellID
	and DementiaLatest.RecordedTime > Dementia.RecordedTime
	)
)
DementiaKnown
on DementiaTraCTE.SpellID = DementiaKnown.SpellID

left join
-- Q1 Numerator - Identify earliest recording within spell of Find Question being asked
(
select 
	*
from 
	DementiaTraCTE Forgetfullness
where 
	ForgetfulnessQuestionAnswered in (0,1)
and not exists
	(select 
		1
	from
		DementiaTraCTE ForgetfullnessEarlier
	where 
		ForgetfullnessEarlier.SpellID = Forgetfullness.SpellID
	and ForgetfullnessEarlier.ForgetfulnessQuestionAnswered in (0,1)
	and ForgetfullnessEarlier.RecordedTime < Forgetfullness.RecordedTime
	)
)
DementiaQuestion
on DementiaTraCTE.SpellID = DementiaQuestion.SpellID

left join
-- Q2 Denominator - Latest answer of Yes to the Find Question, excludes those if they're known to have Dementia
--2014103 changed to latest answer as found a couple of errors, recorded yes, then an hour later 'no'
(
select 
	*
from 
	DementiaTraCTE Forgetfullness
where 
	ForgetfulnessQuestionAnswered = 1
and not exists
	(select 
		1
	from
		DementiaTraCTE ForgetfullnessLatest
	where 
		ForgetfullnessLatest.SpellID = Forgetfullness.SpellID
	and ForgetfullnessLatest.ForgetfulnessQuestionAnswered in (0,1)-- 1
	and ForgetfullnessLatest.RecordedTime > Forgetfullness.RecordedTime
	)
and not exists
	(select 
		1
	from
		DementiaTraCTE KnownDementiaEarlier
	where 
		KnownDementiaEarlier.SpellID = Forgetfullness.SpellID
	and KnownDementiaEarlier.KnownToHaveDementia = 1
	and KnownDementiaEarlier.RecordedTime <= Forgetfullness.RecordedTime
	)
)
DementiaAnswer

on DementiaTraCTE.SpellID = DementiaAnswer.SpellID

left join
-- Q2 Numerator - Identify patients with an AMT score, pull the latest, if multiple
(
select 
	*
from 
	DementiaTraCTE Assessment
where 
	AMTScore is not null
and not exists
	(select 
		1
	from
		DementiaTraCTE AssessmentLatest
	where 
		AssessmentLatest.SpellID = Assessment.SpellID
	and AssessmentLatest.AMTScore is not null
	and AssessmentLatest.RecordedTime > Assessment.RecordedTime
	)
)
DementiaAssessment

on DementiaTraCTE.SpellID = DementiaAssessment.SpellID
and DementiaAnswer.SpellID = DementiaAssessment.SpellID


left join
-- Q2 Numerator - Pull latest recording in spell of investigations started
(
select 
	*
from 
	DementiaTraCTE Investigations
where 
	CognitiveInvestigationStarted = 1
and not exists
	(select 
		1
	from
		DementiaTraCTE InvestigationsLatest
	where 
		InvestigationsLatest.SpellID = Investigations.SpellID
	and InvestigationsLatest.CognitiveInvestigationStarted = 1
	and InvestigationsLatest.RecordedTime > Investigations.RecordedTime
	)
)
DementiaInvestigations

on DementiaTraCTE.SpellID = DementiaInvestigations.SpellID
and DementiaAnswer.SpellID = DementiaInvestigations.SpellID

-- Reason for no assessment
left join
(select 
	*
from 
	DementiaTraCTE NoAssessment
where
	AMTTestNotPerformedReason is not null
and not exists
	(select
		1
	from
		DementiaTraCTE NoAssessmentLatest
	where 
		NoAssessmentLatest.SpellID = NoAssessment.SpellID
	and NoAssessmentLatest.AMTTestNotPerformedReason is not null
	and NoAssessmentLatest.RecordedTime > NoAssessment.RecordedTime
	)
	
)
DementiaNoAssessment
on DementiaTraCTE.SpellID = DementiaNoAssessment.SpellID
and DementiaAnswer.SpellID = DementiaNoAssessment.SpellID

left join
-- Q3 Denominator = subsection of Q2 numerator, ie how many had a positive score
-- Need to check if they need to have had investigations also
-- Q3 Numerator, or those with a positive score how many where referred
(
select 
	*
from 
	DementiaTraCTE Referral
where 
	DementiaReferralSentDate is not null
and not exists
	(select 
		1
	from
		DementiaTraCTE ReferralLatest
	where 
		ReferralLatest.SpellID = Referral.SpellID
	and ReferralLatest.DementiaReferralSentDate is not null
	and ReferralLatest.RecordedTime > Referral.RecordedTime
	)
)
DementiaReferral

on DementiaTraCTE.SpellID = DementiaReferral.SpellID

where 
	DementiaTraCTE.SequenceNo = 1
		
	/****** Script for SelectTopNRows command from SSMS  ******/

  
  --5808

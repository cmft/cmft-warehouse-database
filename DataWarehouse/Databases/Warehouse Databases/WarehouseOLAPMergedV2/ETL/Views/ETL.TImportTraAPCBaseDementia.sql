﻿

CREATE View [ETL].[TImportTraAPCBaseDementia]

as

--/***************************************************************************
--**	20140701	RR	Create Dementia Trafford using GC script 
--**
--****************************************************************************/


--select SpellID,count(*) 
--from ETL.TLoadTraAPCBaseDementia
--group by spellid
--A12345	8
--A125478	2


WITH 
	XMLNAMESPACES (N'http://schemas.microsoft.com/office/infopath/2003/myXSD/2009-11-10T09:47:10' as my)
	
	,DementiaTraCTE 
	
		(
		ProviderSpellNo
		,GlobalProviderSpellNo
		,SpellID
		,SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,RecordedDate
		,RecordedTime
		,KnownToHaveDementia
		,PatientDementiaFlag
		,RememberMePlan
		,RememberMePlanCommenced
		,RememberMePlanDiscontinued
		,ForgetfulnessQuestionAnswered
		,AMTTestPerformed
		,ClinicalIndicationsPresent
		,AMTScore
		,AMTTestNotPerformedReason
		,CognitiveInvestigationStarted
		,ReferralNeeded
		,DementiaReferralSentDate
		,SequenceNo
		,AdmissionWardCode
			
		)
		as
		
			(
			select
				 ProviderSpellNo = AdmissionNumber
				,GlobalProviderSpellNo = coalesce(Encounter.GlobalProviderSpellNo,AdmissionNumber)
				,SpellID = AdmissionNumber
				,SourceUniqueID = FormId
				,SourcePatientNo = FormXML.value('/my:myFields[1]/my:PatientDemographics[1]/my:DistrictNo[1]', 'varchar(200)') 
				,SourceSpellNo = AdmissionNumber
				,RecordedDate = cast(OriginalSubmissionDate as date)
				,RecordedTime = OriginalSubmissionDate

				,KnownToHaveDementia = 
						case 
							when FormXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') ='Yes' 
							then 1
							when FormXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') ='No' 
							then 0
							else null
						end	

				,PatientDementiaFlag = 
						case
							when FormXML.value('/my:myFields[1]/my:KnownDementia[1]', 'varchar(200)') ='Yes' 
							then 1
							when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 7
							then 1
							else null
						end

				,RememberMePlan	= 
						case
							when FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed1[1]', 'varchar(200)') = 'True'
								or FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed2[1]', 'varchar(200)') = 'True'
								or FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed3[1]', 'varchar(200)') = 'True'
							then 1
							when FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed1[1]', 'varchar(200)') = 'False'
								And FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed2[1]', 'varchar(200)') = 'False'
								And FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:Discussed3[1]', 'varchar(200)') = 'False'
							then 0
							else null
						end

				,RememberMePlanCommenced = 
						coalesce(
							nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate1[1]','VARCHAR(60)'), '') 
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate2[1]','VARCHAR(60)'), '') 
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate3[1]','VARCHAR(60)'), '')
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate4[1]','VARCHAR(60)'), '')
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate5[1]','VARCHAR(60)'), '')
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate6[1]','VARCHAR(60)'), '')
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate7[1]','VARCHAR(60)'), '')
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate8[1]','VARCHAR(60)'), '')
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate9[1]','VARCHAR(60)'), '')
							,nullif(FormXML.value('/my:myFields[1]/my:CompleteSection[1]/my:ForgetMeNotIconSection[1]/my:EvaluationDate10[1]','VARCHAR(60)'), ''))

				,RememberMePlanDiscontinued	= null

				,ForgetfulnessQuestionAnswered = 
						case 
							when FormXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='Yes' 
							then 1
							when FormXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') ='No' 
							then 0
							else null
						end

				,AMTTestPerformed = 
						case
							when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
								and FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 10
							then 1
							when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'false'
							then 0
							else null
						end

				,ClinicalIndicationsPresent	= null

				,AMTScore = 
						case 
							when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
							and FormXML.value('/my:myFields[1]/my:ScreeningSection[1]/my:ScreeningQuestion[1]', 'varchar(200)') <> 'No'
							then FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)')
							else Null
						end 

				,AMTTestNotPerformedReason = 
						case 
							when FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:ScoreComplete[1]', 'varchar(200)') = 'true'
								and FormXML.value('/my:myFields[1]/my:MMSESection[1]/my:MMSETotal[1]', 'varchar(200)') between 0 and 10
							then null
							else nullif(FormXML.value('/my:myFields[1]/my:UnableSection[1]/my:UnableReason[1]', 'varchar(200)'),'')
						end

				,CognitiveInvestigationStarted = 
						case 
							when FormXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)') = 'true' 
							then 1
							when FormXML.value('/my:myFields[1]/my:Investigation[1]/my:InvQn[1]', 'varchar(200)') = 'false' 
							then 0
							else null 
						end

				,ReferralNeeded	= Null

				,DementiaReferralSentDate = 
						case 
							when FormXML.value('/my:myFields[1]/my:GPLetterConfirmSection[1]/my:GPLetterConfirmed[1]', 'varchar(200)') = 'true' 
							then DateAdded
							when FormXML.value('/my:myFields[1]/my:GPLetterConfirmSection2[1]/my:GPLetterConfirmed2[1]', 'varchar(200)') = 'true'
							then DateAdded
							else null 
						end

				,SequenceNo = row_number ( ) over (partition by AdmissionNumber order by OriginalSubmissionDate asc)

				,AdmissionWardCode = AdmWard.WardCode
			
			--select Encounter.GlobalProviderSpellNo,AdmissionNumber
			from 
				[$(TrustCache)].Dementia.Trafford_InfoPath_Forms DementiaData
			
			left join [APC].[BaseEncounter] Encounter
			on Encounter.ProviderSpellNo = DementiaData.AdmissionNumber
			and FirstEpisodeInSpellIndicator = 1
			
			left join [APC].[BaseWardStay] AdmWard
			on AdmWard.ProviderSpellNo = DementiaData.AdmissionNumber
			and AdmWard.ContextCode = 'TRA||UG'
			and StartActivityCode = '1'
				)

 -- These records have an AMTScore A128224,A132048,A132332
select 
	ProviderSpellNo
	,GlobalProviderSpellNo
	,SpellID
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,AdmissionWardCode
	,RecordedDate
	,RecordedTime
	,KnownToHaveDementia
	,ForgetfulnessQuestionAnswered
	,AssessmentScore = AMTScore
	,Investigation = CognitiveInvestigationStarted
	,AssessmentNotPerformedReason = 
			case
				when AMTTestNotPerformedReason = '' then Null
				else AMTTestNotPerformedReason 
			end
	,ReferralSentDate = DementiaReferralSentDate
	,InterfaceCode = 'InfoPathForms' 	
	,ContextCode = 'TRA||IPATH' 
from 
	DementiaTraCTE



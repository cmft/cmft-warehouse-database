﻿






CREATE view [ETL].[TLoadCenDictationDocument]

as

select
	DocumentRecno
	,SourceUniqueID
	,SourcePatientNo
	,DocumentID
	,DocumentDescription
	,DocumentTypeCode
	,AuthorCode
	,TypedTime
	,DictatedTime
	,DictatedByCode
	,IssuedTime
	,IssuedByCode
	,SignedTime
	,SignedByCode
	,SignedStatusCode
	,DepartmentCode
	,DocumentXMLTemplateCode
	,DocumentXML
	,TransmissionTime
	,TransmissionDestinationCode
	--,Created
	--,Updated
	--,ByWhom
	,ContextCode = 'CEN||MEDI'
	
from
	[$(Warehouse)].Dictation.MedisecDocument




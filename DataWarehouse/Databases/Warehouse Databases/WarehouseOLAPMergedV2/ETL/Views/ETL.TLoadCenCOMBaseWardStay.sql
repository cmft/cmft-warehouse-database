﻿
CREATE view [ETL].[TLoadCenCOMBaseWardStay] as

select
	 EncounterRecno
	,SourceUniqueID
	,SourceSpellNo
	,SourceEncounterNo
	,ProviderSpellNo
	,AdmissionTime
	,DischargeTime
	,AdmissionDate
	,DischargeDate
	,StartTime
	,EndTime
	,StartDate
	,EndDate
	,PatientSourceUniqueID
	,PatientSourceSystemUniqueID
	,PatientNHSNumber
	,PatientNHSNumberStatusIndicator
	,PatientTitleID
	,PatientForename
	,PatientSurname
	,PatientAddress1
	,PatientAddress2
	,PatientAddress3
	,PatientAddress4
	,PatientPostcode
	,PatientDateOfBirth
	,PatientDateOfDeath
	,PatientSexID
	,PatientEthnicGroupID
	,SourceServicePointID
	,SourceAdmissionMethodID
	,SourceAdmissionSourceID
	,SourcePatientClassificationID
	,SourceAdminCategoryID
	,SourceIntendedManagementID
	,SourceReasonForAdmissionID
	,ExpectedDischargeDate
	,SourceDischargeDestinationID
	,SourceDischargeMethodID
	,SpellProfessionalCarer
	,SpellSpecialty
	,WardStayPracticeCode
	,WardStayGPCode
	,WardStayPCTCode
	,CreatedTime
	,ModifiedTime
	,CreatedByID
	,ModifiedByID
	,ArchiveFlag
	,HealthOrgOwner
	,InterfaceCode
	,RegisteredPracticeID

	,LOS =
		datediff(
			 day
			,WardStay.AdmissionDate
			,coalesce(
				 WardStay.DischargeDate
				,(
				SELECT 
					MAX(MaxDate)
				FROM
					(
						select
							MaxDate = max(EndDate) 
						from
							[$(Warehouse)].COM.WardStay
						UNION ALL
						select
							max(StartDate)
						from
							[$(Warehouse)].COM.WardStay
						
					) tblMaxDate
				)
			)
		)

	,AgeCode =
	case
	when datediff(day, PatientDateOfBirth, WardStay.StartTime) is null then 'Age Unknown'
	when datediff(day, PatientDateOfBirth, WardStay.StartTime) <= 0 then '00 Days'
	when datediff(day, PatientDateOfBirth, WardStay.StartTime) = 1 then '01 Day'
	when datediff(day, PatientDateOfBirth, WardStay.StartTime) > 1 and
	datediff(day, PatientDateOfBirth, WardStay.StartTime) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, WardStay.StartTime)), 2) + ' Days'
	
	when datediff(day, PatientDateOfBirth, WardStay.StartTime) > 28 and
	datediff(month, PatientDateOfBirth, WardStay.StartTime) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, WardStay.StartTime) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, PatientDateOfBirth, WardStay.StartTime) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, WardStay.StartTime) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, PatientDateOfBirth, WardStay.StartTime) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, WardStay.StartTime) then 1 else 0 end > 1 and
	 datediff(month, PatientDateOfBirth, WardStay.StartTime) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, WardStay.StartTime) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,WardStay.StartTime) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, WardStay.StartTime) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, PatientDateOfBirth, WardStay.StartTime) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, WardStay.StartTime)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, WardStay.StartTime) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, WardStay.StartTime)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, WardStay.StartTime) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, WardStay.StartTime)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, WardStay.StartTime) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, WardStay.StartTime)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,ContextCode = 'CEN||IPM'

	,Created
	,Updated
	,ByWhom

	--,ModifiedRow =
	--	case
	--	when
	--		WardStay.Updated >
	--		(
	--		select
	--			DatasetProcess.DatasetProcessTime
	--		from
	--			ETL.DatasetProcess
	--		where
	--			DatasetProcess.DatasetCode = 'COMWS'
	--		and	DatasetProcess.ProcessCode = 'IMPORT'
	--		)
	--	then 1
	--	else 0
	--	end

from
	[$(Warehouse)].COM.WardStay
where
	WardStay.ArchiveFlag = 'N'
and
	(
		AdmissionDate >= '01 Apr 2008'
	or
		(
			DischargeDate >= '01 Apr 2008'
		or	DischargeDate is null
		or	EndDate is null
		)

	)









﻿







CREATE view [ETL].[TImportCenResultBaseGPICEOrder] as

select
	OrderRecno
	,ContextCode = 'CEN||GPICE'
	,SourceUniqueID
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode = ClinicianID
	,MainSpecialtyCode
	,SpecialtyCode = MainSpecialtyCode
	,ProviderCode = ProviderID
	,LocationCode = LocationID
	,OrderPriority
	,OrderStatusCode
	,OrderRequestTime
	,OrderReceivedTime
	,OrderCollectionTime
	,InterfaceCode
from
	[$(Warehouse)].Result.GPICEOrder Import

























﻿







CREATE view [ETL].[TImportCenResultBaseCWSTest] as 

select 
	TestRecno = Import.ResultRecno
	,ContextCode = 'CEN||PAS'
	,SourceUniqueID = Import.OrderSourceUniqueID
	,OrderSourceUniqueID = Import.OrderSourceUniqueID
	,OrderRequestTime = OrderTime
	,OrderStatusCode
	,PatientID = Import.SourcePatientNo
	,PatientIdentifier = Patient.DistrictNo
	,DistrictNo = Patient.DistrictNo
	,CasenoteNumber = null
	,Patient.NHSNumber
	,ConsultantCode = Import.ConsultantCode
	,MainSpecialtyCode
	,SpecialtyCode
	,ProviderCode = ScreenTypeCode
	,LocationCode = null
	,TestCode = Import.ServiceCode
	,TestStatus = null
	,InterfaceCode
from
	[$(Warehouse)].Result.CWSNumericResult Import

inner join 
	(
	select
		OrderSourceUniqueID
		,ServiceCode
		,ResultRecno = max(ResultRecno)
	from
		[$(Warehouse)].Result.CWSNumericResult Later
	group by
		OrderSourceUniqueID
		,ServiceCode
	) Later
on	Later.OrderSourceUniqueID = Import.OrderSourceUniqueID
and Later.ServiceCode = Import.ServiceCode
and Later.ResultRecno = Import.ResultRecno

left join [$(Warehouse)].PAS.Patient
on	Import.SourcePatientNo = Patient.SourcePatientNo

left join [$(Warehouse)].PAS.Consultant
on	Import.ConsultantCode = Consultant.ConsultantCode

left join [$(Warehouse)].PAS.Service
on	Service.ServiceCode = Import.ServiceCode

union 

select 
	TestRecno = Import.ResultRecno
	,ContextCode = 'CEN||PAS'
	,SourceUniqueID = Import.OrderSourceUniqueID
	,OrderSourceUniqueID = Import.OrderSourceUniqueID
	,OrderRequestTime = OrderTime
	,OrderStatusCode
	,PatientID = Import.SourcePatientNo
	,PatientIdentifier = Patient.DistrictNo
	,DistrictNo = Patient.DistrictNo
	,CasenoteNumber = null
	,Patient.NHSNumber
	,ConsultantCode = Import.ConsultantCode
	,MainSpecialtyCode
	,SpecialtyCode
	,ProviderCode = ScreenTypeCode
	,LocationCode = null
	,TestCode = Import.ServiceCode
	,TestStatus = null
	,InterfaceCode
from
	[$(Warehouse)].Result.CWSTextResult Import

inner join 
	(
	select
		OrderSourceUniqueID
		,ServiceCode
		,ResultRecno = max(ResultRecno)
	from
		[$(Warehouse)].Result.CWSTextResult Later
	group by
		OrderSourceUniqueID
		,ServiceCode
	) Later
on	Later.OrderSourceUniqueID = Import.OrderSourceUniqueID
and Later.ServiceCode = Import.ServiceCode
and Later.ResultRecno = Import.ResultRecno


left join [$(Warehouse)].PAS.Patient
on	Import.SourcePatientNo = Patient.SourcePatientNo

left join [$(Warehouse)].PAS.Consultant
on	Import.ConsultantCode = Consultant.ConsultantCode

left join [$(Warehouse)].PAS.Service
on	Service.ServiceCode = Import.ServiceCode






























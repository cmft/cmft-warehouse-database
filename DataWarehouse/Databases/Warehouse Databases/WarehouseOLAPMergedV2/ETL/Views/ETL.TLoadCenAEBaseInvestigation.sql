﻿

create view [ETL].[TLoadCenAEBaseInvestigation]
as

select
	 Encounter.MergeEncounterRecno
	,Investigation.SequenceNo
	,InvestigationSchemeCode = Investigation.InvestigationSchemeInUse
	,Investigation.InvestigationCode
	,InvestigationTime = Investigation.InvestigationDate
	,ResultTime = Investigation.ResultDate
	,Encounter.ContextCode
from
	[$(Warehouse)].AE.Investigation Investigation

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Investigation.AESourceUniqueID
and	Encounter.ContextCode in
	(
	 'CEN||ADAS'
	,'CEN||PAS'
	,'CEN||SYM'
	,'TRA||ADAS'
	)




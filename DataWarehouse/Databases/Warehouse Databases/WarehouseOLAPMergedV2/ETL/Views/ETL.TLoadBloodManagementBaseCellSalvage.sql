﻿

--Use WarehouseOLAPMergedV2

CREATE View [ETL].[TLoadBloodManagementBaseCellSalvage]

as

select 
	CellSalvageRecno
	,SourceUniqueID
	,EpisodeKey
	,HospitalNumber
	,LastName
	,FirstName
	,DateOfBirth
	,GenderCode 
	,SpecialtyCode 
	,ConsultantCode 
	,IntendedDestination
	,ActualDestination
	,DrugCode 
	,Drug
	,Category
	,Dose
	,Units
	,StartTime
	,EndTime
	,Duration
	,SessionType
	,SessionLocationCode 
	,InterfaceCode 
	,ContextCode = 'CMFT||RECALL' -- There is data coming through with location Trafford Theatre 5 and 6, small numbers.
	,Created
	,Updated
	,ByWhom
from 
	[$(Warehouse)].BloodManagement.CellSalvage
	


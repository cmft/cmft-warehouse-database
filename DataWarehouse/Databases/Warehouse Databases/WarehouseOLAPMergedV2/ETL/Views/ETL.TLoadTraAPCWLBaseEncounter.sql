﻿










CREATE view [ETL].[TLoadTraAPCWLBaseEncounter] as

SELECT --top 100
	 Encounter.EncounterRecno
	,InterfaceCode
	,SourcePatientNo
	,SourceEncounterNo = ''
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode
	--,SexCode = -- DG - removed as replaced with mapping process
	--	case Encounter.SexCode
	--	when '' then '0'
	--	when 'M' then '1'
	--	when 'F' then '2'
	--	when 'I' then '9'
	--	else '#'
	--	end
	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode = null
	,Postcode
	,PatientsAddress1 = PatientAddress1
	,PatientsAddress2 = PatientAddress2
	,PatientsAddress3 = PatientAddress3
	,PatientsAddress4 = PatientAddress4
	,DHACode = null
	,HomePhone = null
	,WorkPhone = null
	,EthnicOriginCode = EthnicCategoryCode
	,coalesce(Encounter.ConsultantCode, 'N/A') ConsultantCode
	,coalesce(Encounter.SpecialtyCode, 'N/A') SpecialtyCode
	--,Encounter.PASSpecialtyCode
	,PASSpecialtyCode = Encounter.SpecialtyCode -- national only
	--,coalesce(Encounter.EpisodeSpecialtyCode, 'N/A') EpisodeSpecialtyCode
	,EpisodeSpecialtyCode = Encounter.SpecialtyCode
	,ManagementIntentionCode = SourceManagementIntentionCode
	,AdmissionMethodCode = ElectiveAdmissionTypeCode -- need source code
	,PriorityCode = PriorityTypeCode
	,WaitingListCode = WaitingListCode
	,CommentClinical = null -- notes in THT?
	,CommentNonClinical = null -- notes in THT?
	,IntendedPrimaryOperationCode = IntendedPrimaryOperationCode
	,Operation = left(AppointmentNote,60)
	,SiteCode = IntendedSiteCode
	,WardCode = null
	,WLStatus = null
	,ProviderCode = ProviderCode
	,PurchaserCode = null
	,ContractSerialNumber = CommissioningSerialNo
	,CancelledBy = null
	,BookingTypeCode = null
	,CasenoteNumber = CasenoteNo
	,OriginalDateOnWaitingList = DecidedToAdmitDate
	,DateOnWaitingList  = DecidedToAdmitDate
	,TCIDate = 
		case
		when OfferedForAdmissionDate is null
		then '1 Jan 1900'
		when OfferedForAdmissionDate  < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900')
		then '1 Jan 1900'
		when OfferedForAdmissionDate < CensusDate
		then '1 Jan 1900'
		else cast(OfferedForAdmissionDate as date)
		end
	,KornerWait = KornerWait
	,CountOfDaysSuspended = CountOfDaysSuspended
	,SuspensionStartDate = null
	,SuspensionEndDate = null
	,SuspensionReasonCode = null
	,SuspensionReason = null
	,EpisodicGpCode = RegisteredGpCode
	--,coalesce(Encounter.RegisteredPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999') RegisteredGpPracticeCode
	,RegisteredGpPracticeCode
	,RegisteredPracticeCode = 
		coalesce(Encounter.RegisteredGpPracticeCode, 'X99999') 
	--,coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredPracticeCode, 'X99999') EpisodicGpPracticeCode
	,EpisodicGpPracticeCode = 
		coalesce(Encounter.RegisteredGpPracticeCode, 'X99999') 
	,RegisteredGpCode 
	,SourceTreatmentFunctionCode = Encounter.TreatmentFunctionCode --national only
	,Encounter.TreatmentFunctionCode
	,NationalSpecialtyCode = Encounter.SpecialtyCode
	,PCTCode
	,BreachDate = null
	,ExpectedAdmissionDate = OfferedForAdmissionDate
	,ReferralDate = ReferralRequestReceivedDate
	,ExpectedLOS = null
	,ProcedureDate = [PrimaryProcedureDate]
	,FutureCancellationDate = null
	,TheatreKey = TheatrePatientBookingKey
	,TheatreInterfaceCode = null
	,EpisodeNo = [PathwayId]
	,BreachDays = null
	,MRSA = null
	,RTTPathwayID = [PathwayId]
	,RTTPathwayCondition = null
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode = Encounter.SpecialtyCode
	,RTTCurrentProviderCode = ProviderCode
	,RTTCurrentStatusCode = RTTStatusCode
	,RTTCurrentStatusDate = null
	,RTTCurrentPrivatePatientFlag = null
	,RTTOverseasStatusFlag = null
	,NationalBreachDate = null
	,NationalBreachDays = null
	,DerivedBreachDays = null
	,DerivedClockStartDate = null
	,DerivedBreachDate = null
	,RTTBreachDate = null
	,RTTDiagnosticBreachDate = null
	,NationalDiagnosticBreachDate = null
	,SocialSuspensionDays = null

	,case
	when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
	datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
	datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end AgeCode

	,1 Cases

	--,convert(int, Encounter.KornerWait) LengthOfWait
	,LengthOfWait = 
		convert(int, Encounter.KornerWait)

	,ConsultantSpecialtyRecno = null
	
	,DurationCode =
		coalesce(
			right('00' +
			convert(varchar, 
			case
			when convert(int, Encounter.KornerWait /7 ) > 207 then 208
			else convert(int, Encounter.KornerWait /7 )
			end), 3)
			,'NA'
		)
	--,DurationCode = '000' -- just to get data into fact table 	
	

	----,WaitTypeCode =
	----case
	----when rtrim(Encounter.AdmissionMethodCode) = 'BA' then '20'
	----when rtrim(Encounter.AdmissionMethodCode) = 'BL' then '20'
	----when rtrim(Encounter.AdmissionMethodCode) = 'BP' then '20'
	----when rtrim(Encounter.AdmissionMethodCode) = 'PA' then '30'
	----when rtrim(Encounter.AdmissionMethodCode) = 'PL' then '30'
	----else '10'
	----end

	,WaitTypeCode =
	case
	when rtrim(Encounter.ElectiveAdmissionTypeCode ) = '13' then '20' -- Planned
	--when Encounter.WLStatus = 'WL Suspend' then '30' DG - needs bringing back in when WLStatus available
	when Encounter.AdminCategoryCode = '02' then '40' --Private Patients
	else '10'
	end -- check - ma

	--,WaitTypeCode =
	--case
	--when rtrim(Encounter.ElectiveAdmissionTypeCode ) = 'PA' then '20'
	--when rtrim(Encounter.ElectiveAdmissionTypeCode ) = 'PL' then '20'
	----when Encounter.WLStatus = 'WL Suspend' then '30' DG - needs bringing back in when WLStatus available
	--when Encounter.AdminCategoryCode = '' then '40'
	--else '10'
	--end -- check - mappings against THT are National - use Source and remap?
	 
	--,StatusCode =
	--case
	--when Encounter.WLStatus = 'WL Suspend' then '20' 
	--else '10'
	--end
	,StatusCode = '10' -- DG - To get into fact table reliant on WLStatus

	,CategoryCode =
	case
	--when rtrim(Encounter.ManagementIntentionCode) = 'I' then '10'
	when rtrim(Encounter.SourceManagementIntentionCode) = '1' then '10' -- DG - amended to be appropriate for Trafford
	else '20'
	end

	,AddedToWaitingListTime = Created -- check

	,AddedLastWeek =
	case
	when
		datediff(
			 day
			,dateadd(day, datediff(day, 0, Encounter.Created), 0)
			,Encounter.CensusDate
		) < 8 then 1
	else 0
	end
	--,AddedLastWeek = null

	--,WaitingListAddMinutes =
	--case
	--when
	--	datediff(
	--		 day
	--		,dateadd(day, datediff(day, 0, Encounter.AddedToWaitingListTime), 0)
	--		,Encounter.CensusDate
	--	) < 8
	--then
	--	coalesce(
	--		 WaitingListAddMinutes.WaitingListAddMinutes
	--		,(
	--		select
	--			NumericValue
	--		from
	--			[$(Warehouse)].dbo.Parameter
	--		where
	--			Parameter = 'DEFAULTWLADDMINUTES'
	--		)
	--		,10
	--	)
	--else null
	--end
	,WaitingListAddMinutes = null

	,OPCSCoded =
	case
	when Encounter.PrimaryProcedureCode is null
	then 0
	else 1
	end

	,WithRTTStartDate =
	case
	when Encounter.RTTStartDate is null
	then 0
	else 1
	end

	,WithRTTStatusCode =
	case
	when Encounter.RTTStatusCode in ('10', '20') then 1
	else 0
	end

	--,WithExpectedAdmissionDate =
	--case
	--when Encounter.ExpectedAdmissionDate is null
	--then 0
	--else 1
	--end
	,WithExpectedAdmissionDate = '0'



	,SourceUniqueID
	,AdminCategoryCode

	,RTTActivity =
		convert(
			bit
			,0
			--,case
			--when
			--	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
			--and	Encounter.SpecialtyCode not in 
			--	(
			--	select
			--		SpecialtyCode
			--	from
			--		RTT.dbo.ExcludedSpecialty
			--	)

			---- 'GENE' ?
			--and	Encounter.ConsultantCode <> 'FRAC'

			----remove ICAT
			--and	Encounter.SiteCode != 'FMSK'

			--and	not exists
			--	(
			--	select
			--		1
			--	from
			--		RTT.dbo.RTTClockStart ClockChangeReason
			--	where
			--		ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
			--	and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
			--	and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
			--	)
			--then 1
			--else 0
			--end
		)


	--,RTTBreachStatusCode =
	--	case
	--	when Encounter.RTTBreachDate is null
	--	then 'U'
	--	when Encounter.RTTBreachDate < Encounter.TCIDate
	--	then 'B'
	--	else 'N'
	--	end
	,RTTBreachStatusCode = null


	--,DiagnosticProcedure =
	--	convert(
	--		bit
	--		,case
	--		when DiagnosticProcedure.ProcedureCode is null
	--		then 0
	--		else 1
	--		end
	--	)
	,DiagnosticProcedure = '0'
	--,DirectorateCode = coalesce(Encounter.DirectorateCode , 'N/A')
	,DirectorateCode = '80'
	,GuaranteedAdmissionDate

	,DurationAtTCIDateCode =
		case
		when Encounter.OfferedForAdmissionDate is null then 'NA'
		when Encounter.OfferedForAdmissionDate < (select min(TheDate) from WH.Calendar where TheDate <> '1 Jan 1900') then 'NA'
		when Encounter.OfferedForAdmissionDate < CensusDate then 'NA'
		else right('00' +
				convert(varchar, 
					case
					when convert(int, (Encounter.KornerWait + datediff(day , CensusDate , OfferedForAdmissionDate)) /7 ) > 207 then 208
					else convert(int, (Encounter.KornerWait + datediff(day , CensusDate , OfferedForAdmissionDate)) /7 )
					end
				)
				,3
			)
		end
	--,DurationAtTCIDateCode = '000'

	,WithTCIDate =
		case
		when OfferedForAdmissionDate is null
		then 0
		when OfferedForAdmissionDate < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900')
		then 0
		when OfferedForAdmissionDate < CensusDate
		then 0
		else 1
		end

	,WithGuaranteedAdmissionDate =
		case
		when Encounter.GuaranteedAdmissionDate is null
		then 0
		else 1
		end
	,WithRTTOpenPathway
	,AdmissionReason
	,ContextCode
	,AppointmentTypeCode

	,RTTPathwayStartDateCurrent = Encounter.RTTStartDate
	,RTTWeekBandReturnCode = null
	,RTTDaysWaiting = null

	,Encounter.CCGCode
	,Encounter.ResidenceCCGCode

FROM
	(
	select 
		 EncounterRecno
		,CensusDate = 
			dateadd(d,-1,Encounter.CensusDate)
		,SourceUniqueID
		,UniqueBookingReferenceNo
		,PathwayId
		,PathwayIdIssuerCode
		,RTTStatusCode
		,RTTStartDate
		,RTTEndDate
		,SourcePatientNo = EPMINumber
		,DistrictNo
		,TrustNo
		,CasenoteNo
		,DistrictNoOrganisationCode
		,NHSNumber
		,NHSNumberStatusId
		,PatientTitle
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,Encounter.Postcode
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,OverseasVisitorCode
		,CarerSupportIndicator
		,EthnicCategoryCode
		,MaritalStatusCode
		,LegalStatusClassificationCode
		,CommissioningSerialNo
		,NHSServiceAgreementLineNo
		,ProviderReferenceNo
		,CommissionerReferenceNo
		,ProviderCode
		,CommissionerCode
		,NHSServiceAgreementChangeDate
		,ElectiveAdmissionListEntryNo
		,AdminCategoryCode
		,CountOfDaysSuspended
		,ElectiveAdmissionListStatusCode
		,ElectiveAdmissionTypeCode
		,ManagementIntentionCode
		,IntendedProcedureStatusCode
		,PriorityTypeCode
		,DecidedToAdmitDate
		,AgeAtCensus
		,GuaranteedAdmissionDate
		,LastDNAOrPatientCancelledDate
		,LastReviewedDate
		,ConsultantCode
		,SpecialtyCode
		,TreatmentFunctionCode
		,OPCSProcedureSchemeCode
		,PrimaryProcedureCode
		,PrimaryProcedureDate
		,ReadProcedureSchemeCode
		,PrimaryProcedureReadCode
		,LocationClassCode
		,IntendedSiteCode
		,LocationTypeCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,ReferrerCode
		,ReferrerOrganisationCode
		,AdmissionOfferOutcomeCode
		,OfferedForAdmissionDate
		,EarliestReasonableOfferDate
		,OriginalDecidedToAdmitDate
		,ElectiveAdmissionListRemovalReasonCode
		,ElectiveAdmissionListRemovalReasonDate
		,PASHRGCode
		,HRGVersionCode
		,PASDGVPCode
		,DurationOfElectiveWait
		,PCTCode
		,ResidencePCTCode
		,PASUpdateDate
		,InterfaceCode
		,IsDeleted
		,ConsultantEpisodeStatusCode
		,Created
		,Updated
		,ByWhom
		,OriginalPathwayId
		,OriginalPathwayIdIssuerCode
		,OriginalRTTStatusCode
		,OriginalRTTStartDate
		,OriginalRTTEndDate
		,AppointmentTypeCode
		,TheatrePatientBookingKey
		,ProcedureTime
		,TheatreCode
		,AppointmentNote

		,SourceManagementIntentionCode = SourceManagementIntentioncode

		,KornerWait = 
			case 
			when KornerWait < 0 then 0
			else KornerWait
			end
		,WaitingListCode = coalesce(WaitingListCode, 'TRA')
		,WithRTTOpenPathway = 
			coalesce (
					WithRTTOpenPathway
					,0
			)
		,AdmissionReason = null
		,ContextCode = 'TRA||' + InterfaceCode
		,ReferralRequestReceivedDate

		,CCGCode = 
				coalesce(
					 CCGPractice.[Parent Organisation Code]
					,CCGPostcode.CCGCode
					,'02A'
				)

		,ResidenceCCGCode = EpisodicPostcode.CCGCode
		,IntendedPrimaryOperationCode
	from 
		[$(TraffordWarehouse)].dbo.APCWaitingList Encounter

	left join [$(Organisation)].[ODS].[General Medical Practice]  CCGPractice
	on CCGPractice.[Organisation Code] = Encounter.RegisteredGpCode


	left join [$(Organisation)].dbo.CCGPostcode CCGPostcode
	on	CCGPostcode.Postcode = 
		case
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
		when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
		else Encounter.Postcode
		end

	left outer join [$(Organisation)].dbo.CCGPostcode EpisodicPostcode on
			EpisodicPostcode.Postcode = 
				case
				when len(Encounter.Postcode) = 8 then Encounter.Postcode
				else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
				end

	where 
		dateadd(d,-1,Encounter.CensusDate) IN
		(
		select 
			Calendar.TheDate
		from
			WH.Calendar
		where
			Calendar.TheDate in
				(
				select
					max(TheDate)
				from
					WH.Calendar
				where
					TheDate between DATEADD(year, -2, getdate()) and getdate()
				group by
					 year(TheDate)
					,month(TheDate)

				union
--snapshots in the last fourteen days
				select
					TheDate
				from
					WH.Calendar
				where
					TheDate between dateadd(day , -14 , getdate()) and getdate()

				union
--Sunday snapshots
				select
					TheDate
				from
					WH.Calendar
				where
					datename(dw, TheDate) = 'Sunday'
				and	TheDate between DATEADD(year, -2, getdate()) and getdate()
				)

--remove this to allow updating of snapshots
		and	not exists
			(
			select
				1
			from
				APCWL.BaseEncounter
			where
				BaseEncounter.CensusDate = Calendar.TheDate
			and BaseEncounter.ContextCode =  'TRA||UG'
			)
		)
	) Encounter
	
left join [$(Warehouse)].dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = Encounter.PrimaryProcedureCode

left join 
	(
	select
		 SpecialtyCode = EntityCode
		,WaitingListAddMinutes = convert(int, Description)
	from
		[$(Warehouse)].dbo.EntityLookup
	where
		EntityTypeCode = 'WLADDMINUTES'
	) WaitingListAddMinutes
on	WaitingListAddMinutes.SpecialtyCode = Encounter.SpecialtyCode
















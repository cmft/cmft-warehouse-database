﻿

CREATE view [ETL].[TLoadCenCasenoteBase] as

select
	 SourcePatientNo
	,CasenoteNumber
	,AllocatedDate
	,CasenoteLocationCode
	,CasenoteLocationDate
	,CasenoteStatus
	,WithdrawnDate
	,ContextCode = 'CEN||PAS'
from
	[$(Warehouse)].PAS.Casenote







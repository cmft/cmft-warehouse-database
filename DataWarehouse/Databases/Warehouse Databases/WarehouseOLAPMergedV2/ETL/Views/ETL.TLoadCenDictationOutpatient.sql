﻿









CREATE view [ETL].[TLoadCenDictationOutpatient]

as

select
	OutpatientRecno = MedisecOutpatientRecno
	,MedisecOutpatient.SourcePatientNo
	,MedisecOutpatient.AppointmentTime
	,MedisecOutpatient.DoctorCode
	--,SequenceNo = row_number() over (
	--						partition by
	--							MedisecOutpatient.SourcePatientNo
	--							,MedisecOutpatient.AppointmentTime
	--						order by
	--							TransmissionTime
	--							,SignedTime
	--							,TypedTime
	--						)								 
	,MedisecOutpatient.InterfaceCode
	,ContextCode = 'CEN||MEDI'
from
	[$(Warehouse)].Dictation.MedisecOutpatient
	







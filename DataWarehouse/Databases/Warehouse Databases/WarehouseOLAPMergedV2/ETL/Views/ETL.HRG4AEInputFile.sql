﻿


CREATE view [ETL].[HRG4AEInputFile]

as

/****************************************************************************************
	View : [ETL].[HRG4AEInputFile]
	Description		 : Used in SSIS package "HRG4 AE Grouper Merged.dtsx"

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
	06/05/2014	Paul Egan		Added FY 2013/2014 dummy record.
	15/12/2014	Phil Orrell		Added net change
	04/08/2015	Colin Hunter	CTE added to group national codes
*****************************************************************************************/


/*     Coalesce used to handle nulls as contracting are passing our A&E CDS 
       from the Warehouse into the Grouper - this mirrors that logic
*/

-- 24 none
-- 99 other

with Investigation	

( 
MergeEncounterRecno
,InvestigationCode
,SequenceNo
)

as
	(
	select
		MergeEncounterRecno
		,InvestigationCode
		,SequenceNo = row_number() over (partition by MergeEncounterRecno order by InvestigationCode)
	from
		(
		select
			BaseInvestigation.MergeEncounterRecno
			,InvestigationCode = InvestigationTest.NationalInvestigationCode
		from
			AE.ProcessList

		inner join AE.BaseInvestigation 
		on	ProcessList.MergeRecno = BaseInvestigation.MergeEncounterRecno	
		and	ProcessList.Dataset = 'Encounter'

		inner join AE.Investigation InvestigationTest
		on	BaseInvestigation.InvestigationCode = InvestigationTest.SourceInvestigationCode
		and BaseInvestigation.ContextCode = InvestigationTest.SourceContextCode

		group by
			BaseInvestigation.MergeEncounterRecno
			,InvestigationTest.NationalInvestigationCode

		)	Investigation

	)

select 
	AGE = [$(CMFT)].Dates.GetAge(
							Encounter.DateOfBirth
							,Encounter.ArrivalDate
							)
   ,INV_01 = coalesce(Investigation1.InvestigationCode, '24')
   ,INV_02 = Investigation2.InvestigationCode
   ,INV_03 = Investigation3.InvestigationCode 
   ,INV_04 = Investigation4.InvestigationCode 
   ,INV_05 = Investigation5.InvestigationCode 
   ,INV_06 = Investigation6.InvestigationCode
   ,INV_07 = Investigation7.InvestigationCode
   ,INV_08 = Investigation8.InvestigationCode 
   ,INV_09 = Investigation9.InvestigationCode
   ,INV_10 = Investigation10.InvestigationCode
   ,INV_11 = Investigation11.InvestigationCode 
   ,INV_12 = Investigation12.InvestigationCode
   ,TREAT_01 = coalesce(Treatment1.TreatmentCode, '99')
   ,TREAT_02 = Treatment2.TreatmentCode
   ,TREAT_03 = Treatment3.TreatmentCode
   ,TREAT_04 = Treatment4.TreatmentCode
   ,TREAT_05 = Treatment5.TreatmentCode
   ,TREAT_06 = Treatment6.TreatmentCode
   ,TREAT_07 = Treatment7.TreatmentCode
   ,TREAT_08 = Treatment8.TreatmentCode 
   ,TREAT_09 = Treatment9.TreatmentCode
   ,TREAT_10 = Treatment10.TreatmentCode 
   ,TREAT_11 = Treatment11.TreatmentCode
   ,TREAT_12 = Treatment12.TreatmentCode
   ,Encounter.MergeEncounterRecno
   ,FinancialYear = 
				case
				when Attendance.FinancialYear = '2015/2016' -- using 14/15 grouper for 15/16 data - confirmed by Phil Huitson 7 Apr 2015
				then '2014/2015'
				else Attendance.FinancialYear
				end
from
	AE.BaseEncounter Encounter

inner join AE.BaseEncounterReference EncounterReference
on	Encounter.MergeEncounterRecno = EncounterReference.MergeEncounterRecno

inner join AE.ProcessList
on	ProcessList.MergeRecno = Encounter.MergeEncounterRecno
and	ProcessList.Dataset = 'Encounter'

inner join WH.Calendar Attendance
on	Attendance.DateID = EncounterReference.EncounterStartDateID

left join Investigation Investigation1   
on	Investigation1.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation1.SequenceNo = 1

left join Investigation Investigation2   
on	Investigation2.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation2.SequenceNo = 2

left join Investigation Investigation3   
on	Investigation3.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation3.SequenceNo = 3

left join Investigation Investigation4   
on	Investigation4.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation4.SequenceNo = 4

left join Investigation Investigation5   
on	Investigation5.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation5.SequenceNo = 5

left join Investigation Investigation6   
on	Investigation6.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation6.SequenceNo = 6

left join Investigation Investigation7   
on	Investigation7.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation7.SequenceNo = 7

left join Investigation Investigation8   
on	Investigation8.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation8.SequenceNo = 8

left join Investigation Investigation9   
on	Investigation9.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation9.SequenceNo = 9

left join Investigation Investigation10
on	Investigation10.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation10.SequenceNo = 10

left join Investigation Investigation11
on	Investigation11.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation11.SequenceNo = 11

left join Investigation Investigation12
on	Investigation12.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Investigation12.SequenceNo = 12

left join AE.BaseTreatment Treatment1    
on	Treatment1.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment1.SequenceNo = 1

left join AE.BaseTreatment Treatment2    
on	Treatment2.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment2.SequenceNo = 2

left join AE.BaseTreatment Treatment3    
on	Treatment3.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment3.SequenceNo = 3

left join AE.BaseTreatment Treatment4    
on	Treatment4.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment4.SequenceNo = 4

left join AE.BaseTreatment Treatment5    
on	Treatment5.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment5.SequenceNo = 5

left join AE.BaseTreatment Treatment6    
on	Treatment6.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment6.SequenceNo = 6

left join AE.BaseTreatment Treatment7    
on	Treatment7.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment7.SequenceNo = 7

left join AE.BaseTreatment Treatment8    
on	Treatment8.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment8.SequenceNo = 8

left join AE.BaseTreatment Treatment9    
on	Treatment9.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment9.SequenceNo = 9

left join AE.BaseTreatment Treatment10   
on	Treatment10.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment10.SequenceNo = 10

left join AE.BaseTreatment Treatment11   
on	Treatment11.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment11.SequenceNo = 11

left join AE.BaseTreatment Treatment12   
on	Treatment12.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	Treatment12.SequenceNo = 12

--left join AE.BaseDiagnosis Diagnosis1    
--on     Diagnosis1.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis1.SequenceNo = 1

--left join AE.BaseDiagnosis Diagnosis2    
--on     Diagnosis2.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis2.SequenceNo = 2

--left join AE.BaseDiagnosis Diagnosis3    
--on     Diagnosis3.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis3.SequenceNo = 3

--left join AE.BaseDiagnosis Diagnosis4    
--on     Diagnosis4.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis4.SequenceNo = 4

--left join AE.BaseDiagnosis Diagnosis5    
--on     Diagnosis5.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis5.SequenceNo = 5

--left join AE.BaseDiagnosis Diagnosis6    
--on     Diagnosis6.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis6.SequenceNo = 6

--left join AE.BaseDiagnosis Diagnosis7    
--on     Diagnosis7.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis7.SequenceNo = 7

--left join AE.BaseDiagnosis Diagnosis8    
--on     Diagnosis8.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis8.SequenceNo = 8

--left join AE.BaseDiagnosis Diagnosis9    
--on     Diagnosis9.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis9.SequenceNo = 9

--left join AE.BaseDiagnosis Diagnosis10   
--on     Diagnosis10.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis10.SequenceNo = 10

--left join AE.BaseDiagnosis Diagnosis11   
--on     Diagnosis11.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis11.SequenceNo = 11

--left join AE.BaseDiagnosis Diagnosis12   
--on     Diagnosis12.MergeEncounterRecno = Encounter.MergeEncounterRecno
--and    Diagnosis12.SequenceNo = 12



--left join WarehouseOLAPMerged.WH.Member AttendanceDisposal
--on   AttendanceDisposal.SourceValueID = Encounter.AttendanceDisposalID

--where
--	Encounter.ArrivalDate >= cast(dateadd(month, -6, getdate()) as date)
--or
--	(
--		Encounter.Updated >= cast(dateadd(month, -3, getdate()) as date)
--	and
--		Encounter.ArrivalDate >= '1 apr 2012' -- we don't have groupers plugged in for previous financial years
--	)

 --Encounter.ArrivalDate >= '1 apr 2012'

/* Pass dummy records to grouper so it doesn't error if no records passed to it */

union all

select
	AGE = null
	,INV_01 = null
	,INV_02 = null
	,INV_03 = null
	,INV_04 = null
	,INV_05 = null
	,INV_06 = null
	,INV_07 = null
	,INV_08 = null
	,INV_09 = null
	,INV_10 = null
	,INV_11 = null
	,INV_12 = null
	,TREAT_01 = null
	,TREAT_02 = null
	,TREAT_03 = null
	,TREAT_04 = null
	,TREAT_05 = null
	,TREAT_06 = null
	,TREAT_07 = null
	,TREAT_08 = null
	,TREAT_09 = null
	,TREAT_10 = null
	,TREAT_11 = null
	,TREAT_12 = null
	,MergeEncounterRecno = -20122013 -- dummy record
	,FinancialYear = '2012/2013'
	
/* Added Paul Egan 06/05/2014 */
union all

select
	AGE = null
	,INV_01 = null
	,INV_02 = null
	,INV_03 = null
	,INV_04 = null
	,INV_05 = null
	,INV_06 = null
	,INV_07 = null
	,INV_08 = null
	,INV_09 = null
	,INV_10 = null
	,INV_11 = null
	,INV_12 = null
	,TREAT_01 = null
	,TREAT_02 = null
	,TREAT_03 = null
	,TREAT_04 = null
	,TREAT_05 = null
	,TREAT_06 = null
	,TREAT_07 = null
	,TREAT_08 = null
	,TREAT_09 = null
	,TREAT_10 = null
	,TREAT_11 = null
	,TREAT_12 = null
	,MergeEncounterRecno = -20132014 -- dummy record
	,FinancialYear = '2013/2014'


























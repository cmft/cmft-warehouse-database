﻿
CREATE view [ETL].[TLoadCenTheatreBaseTimetableDetail] as

select
	 TimetableDetail.TimetableDetailCode
	,TimetableDetail.SessionNumber
	,TimetableDetail.DayNumber
	,TimetableDetail.StartDate
	,TimetableDetail.EndDate
	,TimetableDetail.TheatreCode
	,TimetableDetail.SessionStartTime
	,TimetableDetail.SessionEndTime
	,ConsultantCode = Consultant.StaffCode
	,AnaesthetistCode = Anaesthetist.StaffCode
	,SpecialtyCode = Specialty.SpecialtyCode
	,TimetableDetail.TimetableTemplateCode
	,TimetableDetail.LogLastUpdated
	,TimetableDetail.RecordLogDetails
	,TimetableDetail.SessionMinutes
from
	[$(Warehouse)].Theatre.TimetableDetail

left join [$(Warehouse)].Theatre.Staff Anaesthetist
on	Anaesthetist.StaffCode1 = TimetableDetail.AnaesthetistCode

left join [$(Warehouse)].Theatre.Staff Consultant
on	Consultant.StaffCode1 = TimetableDetail.ConsultantCode

left join [$(Warehouse)].Theatre.Specialty
on	Specialty.SpecialtyCode1 = TimetableDetail.SpecialtyCode


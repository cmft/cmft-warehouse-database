﻿



CREATE view [ETL].[TLoadCenCOMBaseEncounter] as

select
	 Encounter.EncounterRecno
	,Encounter.SourceEncounterID
	,Encounter.SourceUniqueID
	,Encounter.SpecialtyID
	,Encounter.StaffTeamID
	,Encounter.ProfessionalCarerID
	,Encounter.SeenByProfessionalCarerID
	,Encounter.EncounterProfessionalCarerID
	,Encounter.StartDate
	,Encounter.StartTime
	,Encounter.EndTime
	,Encounter.ArrivedTime
	,Encounter.SeenTime
	,Encounter.DepartedTime
	,Encounter.AttendedID
	,Encounter.OutcomeID
	,Encounter.EncounterOutcomeCode
	,Encounter.EncounterDuration
	,Encounter.ReferralID
	,Encounter.ProfessionalCarerEpisodeID
	,Encounter.PatientSourceID
	,Encounter.PatientSourceSystemUniqueID
	,Encounter.PatientNHSNumber
	,Encounter.PatientNHSNumberStatusIndicator
	,Encounter.PatientIDLocalIdentifier
	,Encounter.PatientIDPASNumber
	,Encounter.PatientIDPatientIdentifier
	,Encounter.PatientIDPatientFacilityID
	,Encounter.PatientTitleID
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.PatientPostcode
	,Encounter.PatientDateOfBirth
	,Encounter.PatientDateOfDeath
	,Encounter.PatientSexID
	,Encounter.PatientEthnicGroupID
	,Encounter.PatientSpokenLanguageID
	,Encounter.PatientReligionID
	,Encounter.PatientMaritalStatusID
	,Encounter.PatientCurrentRegisteredPracticeCode
	,Encounter.PatientEncounterRegisteredPracticeCode
	,Encounter.PatientCurrentRegisteredGPCode
	,Encounter.PatientEncounterRegisteredGPCode
	,Encounter.VisitID
	,Encounter.ContactTypeID
	,Encounter.EncounterTypeID
	,Encounter.ScheduleTypeID
	,Encounter.ScheduleReasonID
	,Encounter.CanceledReasonID
	,Encounter.CanceledTime
	,Encounter.CancelledByID
	,Encounter.MoveReasonID
	,Encounter.MoveTime
	,Encounter.ServicePointID
	,Encounter.ServicePointSessionID
	,Encounter.MoveCount
	,Encounter.LocationTypeID
	,Encounter.LocationDescription
	,Encounter.LocationTypeHealthOrgID
	,Encounter.EncounterLocationID
	,Encounter.ServicePointStaysID
	,Encounter.WaitingListID
	,Encounter.PlannedAttendees
	,Encounter.ActualAttendees
	,Encounter.ProviderSpellID
	,Encounter.RTTStatusID
	,Encounter.EarliestReasinableOfferTime
	,Encounter.EncounterInterventionsCount
	,Encounter.CreatedTime
	,Encounter.ModifiedTime
	,Encounter.CreatedBy
	,Encounter.ModifiedBy
	,Encounter.Archived
	,Encounter.ParentID
	,Encounter.HealthOrgOwnerID
	,Encounter.RecordStatus
	,Encounter.JointActivityFlag
	,Encounter.Comment
	,Encounter.DerivedFirstAttendanceFlag
	,Encounter.ReferralReceivedTime
	,Encounter.ReferralReceivedDate
	,Encounter.ReferralClosedTime
	,Encounter.ReferralClosedDate
	,Encounter.ReferralReasonID
	,Encounter.ReferredToSpecialtyID
	,Encounter.ReferredToStaffTeamID
	,Encounter.ReferredToProfCarerID
	,Encounter.ReferralSourceID
	,Encounter.ReferralTypeID
	,Encounter.CommissionerCode
	,Encounter.PctOfResidence
	,Encounter.LocalAuthorityCode
	,Encounter.DistrictOfResidence
	,Encounter.PatientEncounterRegisteredPracticeID
	,Encounter.DNAReasonID


	,AgeCode =
	case
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) is null then 'Age Unknown'
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) <= 0 then '00 Days'
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) = 1 then '01 Day'
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) > 1 and
	datediff(day, PatientDateOfBirth, Encounter.StartDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, PatientDateOfBirth, Encounter.StartDate)), 2) + ' Days'
	
	when datediff(day, PatientDateOfBirth, Encounter.StartDate) > 28 and
	datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end > 1 and
	 datediff(month, PatientDateOfBirth, Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, PatientDateOfBirth,Encounter.StartDate) -
	case  when datepart(day, PatientDateOfBirth) > datepart(day, Encounter.StartDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, PatientDateOfBirth, Encounter.StartDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.StartDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Encounter.StartDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.StartDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, PatientDateOfBirth, Encounter.StartDate) - 
	(
	case 
	when	(datepart(m, PatientDateOfBirth) > datepart(m, Encounter.StartDate)) 
	or
		(
			datepart(m, PatientDateOfBirth) = datepart(m, Encounter.StartDate) 
		And	datepart(d, PatientDateOfBirth) > datepart(d, Encounter.StartDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,Encounter.WaitDays

	,Encounter.Created
	,Encounter.Updated
	,Encounter.ByWhom

	,ContextCode = 'CEN||IPM'

	--,ModifiedRow =
	--	case
	--	when
	--		Encounter.Updated >
	--		(
	--		select
	--			DatasetProcess.DatasetProcessTime
	--		from
	--			ETL.DatasetProcess
	--		where
	--			DatasetProcess.DatasetCode = 'COMA'
	--		and	DatasetProcess.ProcessCode = 'IMPORT'
	--		)
	--	then 1
	--	else 0
	--	end


	--,WaitDays =
	--	case
	--	when Encounter.DerivedFirstAttendanceFlag = 1
	--	then DATEDIFF(
	--			 day
	--			,Encounter.ReferralReceivedDate
	--			,Encounter.StartDate
	--		)

	--	else DATEDIFF(
	--			 day
	--			,(
	--			select
	--				PreviousEvent.StartDate
	--			from
	--				[$(Warehouse)].COM.Encounter PreviousEvent
	--			where
	--				PreviousEvent.ReferralID = Encounter.ReferralID
	--			and	PreviousEvent.StartTime < Encounter.StartTime
	--			and	PreviousEvent.Archived = 'N'
	--			and	PreviousEvent.RecordStatus = 'C'
	--			and	PreviousEvent.StartDate >= '01 Apr 2008'
	--			and	not exists
	--				(
	--				select
	--					1
	--				from
	--					[$(Warehouse)].COM.Encounter Previous
	--				where
	--					Previous.ReferralID = Encounter.ReferralID
	--				and	Previous.StartTime < Encounter.StartTime
	--				and	Previous.Archived = 'N'
	--				and	Previous.RecordStatus = 'C'
	--				and	Previous.StartDate >= '01 Apr 2008'
	--				and	(
	--						Previous.StartTime > PreviousEvent.StartTime
	--					or	(
	--							Previous.StartTime = PreviousEvent.StartTime
	--						and	Previous.EncounterRecno > PreviousEvent.EncounterRecno
	--						)
	--					)
	--				)
	--			)

	--			,Encounter.StartDate
	--		)

	--	end
	,CalledTime
from
	[$(Warehouse)].COM.Encounter Encounter
where
	Encounter.Archived = 'N'
and	Encounter.RecordStatus = 'C'
and	Encounter.StartDate >= '01 Apr 2008'
and	Encounter.ReferralID is not null










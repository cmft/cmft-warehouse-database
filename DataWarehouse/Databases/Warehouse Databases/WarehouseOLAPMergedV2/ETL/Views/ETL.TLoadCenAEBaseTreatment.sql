﻿

CREATE view [ETL].[TLoadCenAEBaseTreatment]
as

select
	 Encounter.MergeEncounterRecno
	,Treatment.SequenceNo
	,TreatmentSchemeCode = Treatment.ProcedureSchemeInUse
	,TreatmentCode = Treatment.ProcedureCode
	,TreatmentTime = Treatment.ProcedureDate
	,Encounter.ContextCode
from
	[$(Warehouse)].AE.[Procedure] Treatment

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Treatment.AESourceUniqueID
and	Encounter.ContextCode in
	(
	 'CEN||ADAS'
	,'CEN||PAS'
	,'CEN||SYM'
	,'TRA||ADAS'
	)




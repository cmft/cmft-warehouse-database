﻿



CREATE view [ETL].[TLoadCenObservationBaseAlert] as

select
	AlertRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,LocationID
	,AdmissionSourceUniqueID
	,TypeID
	,ReasonID
	,ObservationSetSourceUniqueID
	,CreatedDate
	,CreatedTime
	,BedsideDueDate
	,BedsideDueTime
	,EscalationDate
	,EscalationTime
	,AcceptedDate
	,AcceptedTime
	,AttendedByUserID
	,AttendanceTypeID
	,Comment
	,SeverityID
	,ClinicianSeniorityID
	,AcceptanceRemindersRemaining
	,ChainSequenceNumber
	,NextReminderTime
	,ClosedDate
	,ClosedTime
	,ClosureReasonID
	,ClosedByUserID
	,CancelledByObservationSetSourceUniqueID
	,LastModifiedTime
	,ContextCode = 'CEN||PTRACK'
from
	[$(Warehouse)].Observation.Alert








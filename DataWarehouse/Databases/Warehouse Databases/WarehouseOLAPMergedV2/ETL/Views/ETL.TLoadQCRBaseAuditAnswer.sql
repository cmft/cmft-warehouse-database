﻿


CREATE view [ETL].[TLoadQCRBaseAuditAnswer] as

select
	[AuditAnswerRecno]
	,[SourceUniqueID]
	,ContextCode = 'CEN||QCR'
	,[AuditTime]
	,[AuditDate]
	,[LocationCode]
	,WardCode
	,[AuditTypeCode]
	,[QuestionCode]
	,[Answer]
from [$(Warehouse)].[QCR].[AuditAnswer]







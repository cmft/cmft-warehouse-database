﻿









CREATE view [ETL].[TLoadCenMortalityBaseReview]

as

/*	
	This renames the columns with a prefix of the form so I can get all of the forms into one row for the review dimension. This handles
	the scenario where we have identical column names in each form.	
	
*/

select
	Mortality.SourceUniqueID
	,FormTypeID
	,ReviewStatus
	,ClassificationGrade
	,RequiresFurtherInvestigation = 
									case
										when
											[$(CMFT)].Dates.GetAge(DateOfBirth, DateOfDeath) < 55 
										or
											ClassificationGrade in (2, 3)
										or
											(
											SurgeryDeathPreventable = 1
										or
											NICUDeathPreventable = 1
										or
											GynaecologyDeathPreventable = 1
											)
										then 1
									end
	,ReviewedDate
	,ReviewedBy
	,AgeCodeAtDeath =
	case
	when datediff(day, DateOfBirth, DateOfDeath) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, DateOfDeath) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, DateOfDeath) = 1 then '01 Day'
	when datediff(day, DateOfBirth, DateOfDeath) > 1 and
	datediff(day, DateOfBirth, DateOfDeath) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, DateOfDeath)), 2) + ' Days'

	when datediff(day, DateOfBirth, DateOfDeath) > 28 and
	datediff(month, DateOfBirth, DateOfDeath) -
	case  when datepart(day, DateOfBirth) > datepart(day, DateOfDeath) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, DateOfDeath) -
	case  when datepart(day, DateOfBirth) > datepart(day, DateOfDeath) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, DateOfDeath) -
	case  when datepart(day, DateOfBirth) > datepart(day, DateOfDeath) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, DateOfDeath) -
	case  when datepart(day, DateOfBirth) > datepart(day, DateOfDeath) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,DateOfDeath) -
	case  when datepart(day, DateOfBirth) > datepart(day, DateOfDeath) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, DateOfDeath) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, DateOfDeath)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, DateOfDeath) 
		And	datepart(d, DateOfBirth) > datepart(d, DateOfDeath)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, DateOfDeath) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, DateOfDeath)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, DateOfDeath) 
		And	datepart(d, DateOfBirth) > datepart(d, DateOfDeath)
		) then 1 else 0 end
	)), 2) + ' Years'
	end
	--,APCMergeEncounterRecno = BaseEncounter.MergeEncounterRecno
	,Mortality.SourcePatientNo
	,Mortality.EpisodeStartTime
	,Mortality.ContextCode
	,CriticalCareCasenoteNo
	,CriticalCareNHSNumber
	,CriticalCareAge
	,CriticalCareAdmissionTime
	,CriticalCareDateOfDeath
	,CriticalCarePlaceOfDeath
	,CriticalCareFirstConsultantReview
	,CriticalCareSpecialtyGroup
	,CriticalCarePatientName
	,CriticalCarePrimaryDiagnosis
	,CriticalCareConfirmedPrimaryDiagnosis
	,CriticalCareCauseOfDeath1a
	,CriticalCareCauseOfDeath1b
	,CriticalCareCauseOfDeath1c
	,CriticalCareCauseOfDeath2
	,CriticalCareCauseOfDeathComment
	,CriticalCareCodedDiagnosis
	,CriticalCareAgreeWithStatedCauseOfDeath
	,CriticalCareHospitalPM
	,CriticalCareCoronerInformed
	,CriticalCareCoronerPM
	,CriticalCareCoexistingFactor
	,CriticalCareTimeClinicalDecisionToAdmitMade
	,CriticalCareTimePatientAdmittedToCriticalCare
	,CriticalCareAdmissionStickerUsed
	,CriticalCareTimeToConsultantReviewComment
	,CriticalCareEvidenceOfClearManagementPlan
	,CriticalCareEvidenceOfClearManagementPlanComment
	,CriticalCareEssentialInvestigationsObtainedWithoutDelay
	,CriticalCareEssentialInvestigationsObtainedWithoutDelayComment
	,CriticalCareInitialManagementStepsAppropriateAndAdequate
	,CriticalCareInitialManagementStepsAppropriateAndAdequateComment
	,CriticalCareOmissionsInInitialManagement
	,CriticalCareOmissionsInInitialManagementComment
	,CriticalCarePeriodAtrialFibrilation
	,CriticalCarePeriodAtrialFibrilationComment
	,CriticalCareAtrialFibrilationAppropriatelyManaged
	,CriticalCareAtrialFibrilationAppropriatelyManagedComment
	,CriticalCareEvidenceOfIschemiaInGut
	,CriticalCareEvidenceOfIschemiaInGutComment
	,CriticalCareTherapeuticAntiCoagulation
	,CriticalCareTherapeuticAntiCoagulationComment
	,CriticalCareRoundsDocumentedEveryDay
	,CriticalCareRoundsDocumentedEveryDayComment
	,CriticalCareAnyRecognisableMedicationErrors
	,CriticalCareAnyRecognisableMedicationErrorsComment
	,CriticalCareDelayInDiagnosis
	,CriticalCareDelayInDiagnosisComment
	,CriticalCareDelayInDeliveringCare
	,CriticalCareDelayInOfDeliveringCareComment
	,CriticalCarePoorCommunication
	,CriticalCarePoorCommunicationComment
	,CriticalCareAdverseEvents
	,CriticalCareAdverseEventsComment
	,CriticalCareAnythingCouldBeDoneDifferently
	,CriticalCareAnythingCouldBeDoneDifferentlyComment
	,CriticalCareNotableGoodQualityCare
	,CriticalCareDocumentationIssues
	,CriticalCareDocumentationIssuesComment
	,CriticalCareEndOfLifeCareLCPInPatientNotes
	,CriticalCareEndOfLifeCareLCPInPatientNotesComment
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecorded
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecordedComment
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressed
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressedComment
	,CriticalCareLessonsLearned
	,CriticalCareActionPlan
	,CriticalCareDateOfActionPlanReview
	,CriticalCareClassificationGrade
	,GynaecologyCasenoteNo
	,GynaecologyDateOfDeath
	,GynaecologyUnitNo
	,GynaecologyClinicalObs
	,GynaecologyClinicalObsDetail
	,GynaecologyAdmissionTime
	,GynaecologyCaseDetail
	,GynaecologyPatientName
	,GynaecologyDNR
	,GynaecologyDNRDetail
	,GynaecologyDeathPreventable
	,GynaecologyDeathPreventableComment
	,GynaecologyNursingConcernsClinicalObs
	,GynaecologyNursingConcernsClinicalObsDetail
	,GynaecologyInitialManagementStepsAppropriateAndAdequate
	,GynaecologyInitialManagementStepsAppropriateAndAdequateComment
	,GynaecologyPalliative
	,GynaecologyPalliativeDetail
	,GynaecologyNursingConcernsRoleInDeath
	,GynaecologyNursingConcernsRoleInDeathDetail
	,GynaecologyTransferICU
	,GynaecologyTransferICUDetail
	,GynaecologyTransferHDU
	,GynaecologyTransferHDUDetail
	,GynaecologyMiscomm
	,GynaecologyMiscommDetail
	,GynaecologyPoorCommPrimaryandSecondary
	,GynaecologyPoorCommPrimaryandSecondaryDDetail
	,GynaecologyNotableGoodQualityCare
	,GynaecologyEndOfLifeCareLCPInPatientNotes
	,GynaecologyEndOfLifeCareLCPApproTime
	,GynaecologyEndOfLifeCareLCPApproTimeComment
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecorded
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecordedComment
	,GynaecologyLessonsLearned
	,GynaecologyActionPlan
	,GynaecologyDateOfBirth
	,GynaecologyConsultantDiscussionB4WDofIC
	,GynaecologyConsultantDiscussionB4WDofICComment
	,GynaecologyEWSgteq3
	,GynaecologyEWSgteq3Detail
	,GynaecologyManageOutOfHospital
	,GynaecologyWhereOutOfHospital
	,GynaecologyPalliativeCareTeamInvolvement
	,GynaecologyPalliativeCareTeamInvolvementNoDetail
	,GynaecologyAdmittedOutOfHours
	,GynaecologyTransferredOtherHospitalSiteInUnstableCondition
	,GynaecologyTransferredOtherHospitalSiteInUnstableConditionDetail
	,GynaecologyClinicianNotListeningToPatient
	,GynaecologyClinicianNotListeningtoPatientDetail
	,GynaecologyPreviousFindingsEBMorHLI
	,NICUCasenoteNo
	,NICUAge
	,NICUDateOfDeath
	,NICUNamedConsultant
	,NICUPatientName
	,NICUTerminalCareDeathExpected
	,NICUTerminalCareDeathExpectedComment
	,NICUDeathPreventable
	,NICUTransferComment
	,NICUPostmortem
	,NICUPostmortemComment
	,NICUMedicationErrors
	,NICUMedicationErrorsComment
	,NICUCommunicationComment
	,NICUAnythingCouldBeDoneDifferentlyComment
	,NICUNotableGoodQualityCare
	,NICULessonsLearned
	,NICUReferenceNo
	,NICUGestation
	,NICUBirthWeight
	,NICUDateOfBirth
	,NICUSex
	,NICUEthnicity
	,NICUMRSA
	,NICUCriticalIncident
	,NICUCriticalIncidentDetail
	,NICUWithdrawalOfIC
	,NICUWithdrawalOfICDetail
	,NICUWithdrawalOfICLOTADNRFormsUsed
	,NICUSeverelyAbnormalCUSS
	,NICUConsultantDiscussionB4WDofIC
	,NICUBereavementAppointmentSent
	,NICUBereavementAppointmentSentDetail
	,NICUClinicalSummaryBackground
	,NICUClinicalSummaryProgress
	,NICUClinicalSummaryManagement
	,NICUClinicalSummarySurgical
	,NICUDiscussionObstetricIssues
	,NICUDiscussionResus
	,NICUDiscussionFirst48Hours
	,NICUDiscussionClinicalManagement
	,NICUDiscussionCommunication
	,NICUDiscussionDocumentation
	,SpecialistMedicalForename
	,SpecialistMedicalSurname
	,SpecialistMedicalCasenoteNo
	,SpecialistMedicalAge
	,SpecialistMedicalAdmissionTime
	,SpecialistMedicalDateOfDeath
	,SpecialistMedicalPlaceOfDeath
	,SpecialistMedicalLeadConsultant
	,SpecialistMedicalOtherConsultants
	,SpecialistMedicalPrimaryDiagnosisOnAdmission
	,SpecialistMedicalPrimaryDiagnosisOnAdmissionComment
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmed
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmedComment
	,SpecialistMedicalCauseOfDeath1a
	,SpecialistMedicalCauseOfDeath1b
	,SpecialistMedicalCauseOfDeath1c
	,SpecialistMedicalCauseOfDeath2
	,SpecialistMedicalCauseOfDeathComment
	,SpecialistMedicalCodedDiagnosis
	,SpecialistMedicalCodedDiagnosisComment
	,SpecialistMedicalAgreeWithCauseOfDeath
	,SpecialistMedicalAgreeWithCauseOfDeathComment
	,SpecialistMedicalHospitalPostmortem
	,SpecialistMedicalHospitalPostmortemComment
	,SpecialistMedicalCoronerInformedOrConsulted
	,SpecialistMedicalCoronerInformedOrConsultedComment
	,SpecialistMedicalCoronerPostmortemPerformed
	,SpecialistMedicalCoronerPostmortemPerformedComment
	,SpecialistMedicalMalignancyPresent
	,SpecialistMedicalMalignancyPresentComment
	,SpecialistMedicalCoExistingFactors
	,SpecialistMedicalCoExistingFactorsComment
	,SpecialistMedicalConsultantReview
	,SpecialistMedicalConsultantReviewComment
	,SpecialistMedicalEvidenceOfClearManagementPlan
	,SpecialistMedicalEvidenceOfClearManagementPlanComment
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelay
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelayComment
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequate
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequateComment
	,SpecialistMedicalOmissionsInInitialManagement
	,SpecialistMedicalOmissionsInInitialManagementComment
	,SpecialistMedicalAdmittedToAppropriateWard
	,SpecialistMedicalAdmittedToAppropriateWardComment
	,SpecialistMedicalHowManyWardsPatientOn
	,SpecialistMedicalHowManyWardsPatientOnComment
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekday
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekdayComment
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeek
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeekComment
	,SpecialistMedicalICUOpinionGiven
	,SpecialistMedicalICUOpinionGivenComment
	,SpecialistMedicalTransferFromGeneralToICUorHDU
	,SpecialistMedicalTransferFromGeneralToICUorHDUComment
	,SpecialistMedicalReadmissionToICUorHDU
	,SpecialistMedicalReadmissionToICUorHDUComment
	,SpecialistMedicalAnyRecognisableMedicationErrors
	,SpecialistMedicalAnyRecognisableMedicationErrorsComment
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30Days
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30DaysComment
	,SpecialistMedicalTimingOfDiagnosis
	,SpecialistMedicalTimingOfDiagnosisComment
	,SpecialistMedicalTimingOfDeliveringCare
	,SpecialistMedicalTimingOfDeliveringCareComment
	,SpecialistMedicalCommunicationIssues
	,SpecialistMedicalCommunicationIssuesComment
	,SpecialistMedicalAdverseEvents
	,SpecialistMedicalAdverseEventsComment
	,SpecialistMedicalIncidentReport
	,SpecialistMedicalIncidentReportComment
	,SpecialistMedicalAnythingCouldBeDoneDifferently
	,SpecialistMedicalAnythingCouldBeDoneDifferentlyComment
	,SpecialistMedicalDocumentationIssues
	,SpecialistMedicalDocumentationIssuesComment
	,SpecialistMedicalNotableGoodQualityCare
	,SpecialistMedicalLCPInPatientsNotes
	,SpecialistMedicalLCPInPatientsNotesComment
	,SpecialistMedicalPreferredPlaceOfDeathRecorded
	,SpecialistMedicalPreferredPlaceOfDeathRecordedComment
	,SpecialistMedicalExpressedWithToDieOutsideOfHospital
	,SpecialistMedicalExpressedWithToDieOutsideOfHospitalComment
	,SpecialistMedicalLessonsLearned
	,SpecialistMedicalActionPlan
	,SpecialistMedicalActionPlanReviewDate
	,SurgeryForename
	,SurgerySurname
	,SurgeryCasenoteNo
	,SurgeryDateOfDeath
	,SurgeryRecordedAppropriatelyToYou
	,SurgeryAlternateNamedConsultant
	,SurgeryTerminalCareDeathExpected
	,SurgeryAgreeWithCauseOfDeath
	,SurgeryDidSurgeryDuringLastAdmission
	,SurgeryHowManyDaysBeforeDeathApprox
	,SurgeryCoronerInformed
	,SurgeryHighLevelInvestigation
	,SurgeryCaseReferredToBleepReview
	,SurgerySurgicalComplicationContributedToDeath
	,SurgerySurgeryHigherThanNormalAnticipatedMortality
	,SurgeryStandardRiskPercentage
	,SurgeryMortalityQuotedPercentage
	,SurgeryDeathPreventable
	,SurgeryDeathPreventableComment
	,SurgeryTerminalCareManagedWell
	,SurgeryTerminalCareManagedWellComment
	,SurgeryCauseOfDeath1
	,SurgeryCauseOfDeath2
	,SurgeryCauseOfDeath3
	,SurgeryCauseOfDeath4
	,SurgeryCoMorbidity1
	,SurgeryCoMorbidity2
	,SurgeryCoMorbidity3
	,SurgeryCoMorbidity4
	,SurgeryCoMorbidity5
	,SurgeryDeathDiscussedWithAnaesthetist
	,SurgeryAnaesthetist
	,SurgeryClassificationGrade
from
	(

select
	SourceUniqueID
	,ReviewedDate
	,ReviewedBy
	,FormTypeID
	,ReviewStatus
	,ClassificationGrade = ClassificationGrade
	,SourcePatientNo
	,EpisodeStartTime
	,ContextCode
	,CriticalCareCasenoteNo  = CasenoteNo 
	,CriticalCareNHSNumber  = CriticalCare.NHSNumber 
	,CriticalCareAge  = Age 
	,CriticalCareAdmissionTime  = CriticalCare.AdmissionTime 
	,CriticalCareDateOfDeath  = CriticalCare.DateOfDeath 
	,CriticalCarePlaceOfDeath = PlaceOfDeath
	,CriticalCareFirstConsultantReview = FirstConsultantReview
	,CriticalCareSpecialtyGroup = SpecialtyGroup
	,CriticalCarePatientName  = PatientName 
	,CriticalCarePrimaryDiagnosis  = PrimaryDiagnosis 
	,CriticalCareConfirmedPrimaryDiagnosis = ConfirmedPrimaryDiagnosis
	,CriticalCareCauseOfDeath1a  = CauseOfDeath1a 
	,CriticalCareCauseOfDeath1b  = CauseOfDeath1b 
	,CriticalCareCauseOfDeath1c  = CauseOfDeath1c 
	,CriticalCareCauseOfDeath2  = CauseOfDeath2 
	,CriticalCareCauseOfDeathComment  = CauseOfDeathComment 
	,CriticalCareCodedDiagnosis  = CodedDiagnosis 
	,CriticalCareAgreeWithStatedCauseOfDeath  = AgreeWithStatedCauseOfDeath 
	,CriticalCareHospitalPM  = HospitalPM 
	,CriticalCareCoronerInformed  = CoronerInformed 
	,CriticalCareCoronerPM  = CoronerPM 
	,CriticalCareCoexistingFactor  = CoexistingFactor 
	,CriticalCareTimeClinicalDecisionToAdmitMade  = TimeClinicalDecisionToAdmitMade 
	,CriticalCareTimePatientAdmittedToCriticalCare  = TimePatientAdmittedToCriticalCare 
	,CriticalCareAdmissionStickerUsed  = AdmissionStickerUsed 
	,CriticalCareTimeToConsultantReviewComment  = TimeToConsultantReviewComment 
	,CriticalCareEvidenceOfClearManagementPlan  = EvidenceOfClearManagementPlan 
	,CriticalCareEvidenceOfClearManagementPlanComment  = EvidenceOfClearManagementPlanComment 
	,CriticalCareEssentialInvestigationsObtainedWithoutDelay  = EssentialInvestigationsObtainedWithoutDelay 
	,CriticalCareEssentialInvestigationsObtainedWithoutDelayComment  = EssentialInvestigationsObtainedWithoutDelayComment 
	,CriticalCareInitialManagementStepsAppropriateAndAdequate  = InitialManagementStepsAppropriateAndAdequate 
	,CriticalCareInitialManagementStepsAppropriateAndAdequateComment  = InitialManagementStepsAppropriateAndAdequateComment 
	,CriticalCareOmissionsInInitialManagement  = OmissionsInInitialManagement 
	,CriticalCareOmissionsInInitialManagementComment  = OmissionsInInitialManagementComment 
	,CriticalCarePeriodAtrialFibrilation  = PeriodAtrialFibrilation 
	,CriticalCarePeriodAtrialFibrilationComment  = PeriodAtrialFibrilationComment 
	,CriticalCareAtrialFibrilationAppropriatelyManaged  = AtrialFibrilationAppropriatelyManaged 
	,CriticalCareAtrialFibrilationAppropriatelyManagedComment  = AtrialFibrilationAppropriatelyManagedComment 
	,CriticalCareEvidenceOfIschemiaInGut  = EvidenceOfIschemiaInGut 
	,CriticalCareEvidenceOfIschemiaInGutComment  = EvidenceOfIschemiaInGutComment 
	,CriticalCareTherapeuticAntiCoagulation  = TherapeuticAntiCoagulation 
	,CriticalCareTherapeuticAntiCoagulationComment  = TherapeuticAntiCoagulationComment 
	,CriticalCareRoundsDocumentedEveryDay  = CriticalCareRoundsDocumentedEveryDay 
	,CriticalCareRoundsDocumentedEveryDayComment  = CriticalCareRoundsDocumentedEveryDayComment 
	,CriticalCareAnyRecognisableMedicationErrors  = AnyRecognisableMedicationErrors 
	,CriticalCareAnyRecognisableMedicationErrorsComment  = AnyRecognisableMedicationErrorsComment 
	,CriticalCareDelayInDiagnosis  = DelayInDiagnosis 
	,CriticalCareDelayInDiagnosisComment  = DelayInDiagnosisComment 
	,CriticalCareDelayInDeliveringCare  = DelayInDeliveringCare 
	,CriticalCareDelayInOfDeliveringCareComment  = DelayInOfDeliveringCareComment 
	,CriticalCarePoorCommunication  = PoorCommunication 
	,CriticalCarePoorCommunicationComment  = PoorCommunicationComment 
	,CriticalCareAdverseEvents  = AdverseEvents 
	,CriticalCareAdverseEventsComment  = AdverseEventsComment 
	,CriticalCareAnythingCouldBeDoneDifferently  = AnythingCouldBeDoneDifferently 
	,CriticalCareAnythingCouldBeDoneDifferentlyComment  = AnythingCouldBeDoneDifferentlyComment 
	,CriticalCareNotableGoodQualityCare  = NotableGoodQualityCare 
	,CriticalCareDocumentationIssues  = DocumentationIssues 
	,CriticalCareDocumentationIssuesComment  = DocumentationIssuesComment 
	,CriticalCareEndOfLifeCareLCPInPatientNotes  = EndOfLifeCareLCPInPatientNotes 
	,CriticalCareEndOfLifeCareLCPInPatientNotesComment  = EndOfLifeCareLCPInPatientNotesComment 
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecorded  = EndOfLifeCarePreferredPlaceOfDeathRecorded 
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecordedComment  = EndOfLifeCarePreferredPlaceOfDeathRecordedComment 
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressed  = EndOfLifeCareWishToDieOutsideHospitalExpressed 
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressedComment  = EndOfLifeCareWishToDieOutsideHospitalExpressedComment 
	,CriticalCareLessonsLearned  = LessonsLearned 
	,CriticalCareActionPlan  = ActionPlan 
	,CriticalCareDateOfActionPlanReview  = DateOfActionPlanReview 
	,CriticalCareClassificationGrade  = ClassificationGrade 

	,GynaecologyCasenoteNo = null
	,GynaecologyDateOfDeath = null
	,GynaecologyUnitNo = null
	,GynaecologyClinicalObs = null
	,GynaecologyClinicalObsDetail = null
	,GynaecologyAdmissionTime = null
	,GynaecologyCaseDetail = null
	,GynaecologyPatientName = null
	,GynaecologyDNR = null
	,GynaecologyDNRDetail = null
	,GynaecologyDeathPreventable = null
	,GynaecologyDeathPreventableComment = null
	,GynaecologyNursingConcernsClinicalObs = null
	,GynaecologyNursingConcernsClinicalObsDetail = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequate = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequateComment = null
	,GynaecologyPalliative = null
	,GynaecologyPalliativeDetail = null
	,GynaecologyNursingConcernsRoleInDeath = null
	,GynaecologyNursingConcernsRoleInDeathDetail = null
	,GynaecologyTransferICU = null
	,GynaecologyTransferICUDetail = null
	,GynaecologyTransferHDU = null
	,GynaecologyTransferHDUDetail = null
	,GynaecologyMiscomm = null
	,GynaecologyMiscommDetail = null
	,GynaecologyPoorCommPrimaryandSecondary = null
	,GynaecologyPoorCommPrimaryandSecondaryDDetail = null
	,GynaecologyNotableGoodQualityCare = null
	,GynaecologyEndOfLifeCareLCPInPatientNotes = null
	,GynaecologyEndOfLifeCareLCPApproTime = null
	,GynaecologyEndOfLifeCareLCPApproTimeComment = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecorded = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecordedComment = null
	,GynaecologyLessonsLearned = null
	,GynaecologyActionPlan = null
	,GynaecologyDateOfBirth = null
	,GynaecologyConsultantDiscussionB4WDofIC = null
	,GynaecologyConsultantDiscussionB4WDofICComment = null
	,GynaecologyEWSgteq3 = null
	,GynaecologyEWSgteq3Detail = null
	,GynaecologyManageOutOfHospital = null
	,GynaecologyWhereOutOfHospital = null
	,GynaecologyPalliativeCareTeamInvolvement = null
	,GynaecologyPalliativeCareTeamInvolvementNoDetail = null
	,GynaecologyAdmittedOutOfHours = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableCondition = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableConditionDetail = null
	,GynaecologyClinicianNotListeningToPatient = null
	,GynaecologyClinicianNotListeningtoPatientDetail = null
	,GynaecologyPreviousFindingsEBMorHLI = null

	,NICUCasenoteNo = null
	,NICUAge = null
	,NICUDateOfDeath = null
	,NICUNamedConsultant = null
	,NICUPatientName = null
	,NICUTerminalCareDeathExpected = null
	,NICUTerminalCareDeathExpectedComment = null
	,NICUDeathPreventable = null
	,NICUTransferComment = null
	,NICUPostmortem = null
	,NICUPostmortemComment = null
	,NICUMedicationErrors = null
	,NICUMedicationErrorsComment = null
	,NICUCommunicationComment = null
	,NICUAnythingCouldBeDoneDifferentlyComment = null
	,NICUNotableGoodQualityCare = null
	,NICULessonsLearned = null
	,NICUReferenceNo = null
	,NICUGestation = null
	,NICUBirthWeight = null
	,NICUDateOfBirth = null
	,NICUSex = null
	,NICUEthnicity = null
	,NICUMRSA = null
	,NICUCriticalIncident = null
	,NICUCriticalIncidentDetail = null
	,NICUWithdrawalOfIC = null
	,NICUWithdrawalOfICDetail = null
	,NICUWithdrawalOfICLOTADNRFormsUsed = null
	,NICUSeverelyAbnormalCUSS = null
	,NICUConsultantDiscussionB4WDofIC = null
	,NICUBereavementAppointmentSent = null
	,NICUBereavementAppointmentSentDetail = null
	,NICUClinicalSummaryBackground = null
	,NICUClinicalSummaryProgress = null
	,NICUClinicalSummaryManagement = null
	,NICUClinicalSummarySurgical = null
	,NICUDiscussionObstetricIssues = null
	,NICUDiscussionResus = null
	,NICUDiscussionFirst48Hours = null
	,NICUDiscussionClinicalManagement = null
	,NICUDiscussionCommunication = null
	,NICUDiscussionDocumentation = null

	,SpecialistMedicalForename= null
	,SpecialistMedicalSurname= null
	,SpecialistMedicalCasenoteNo= null
	,SpecialistMedicalAge= null
	,SpecialistMedicalAdmissionTime= null
	,SpecialistMedicalDateOfDeath= null
	,SpecialistMedicalPlaceOfDeath= null
	,SpecialistMedicalLeadConsultant= null
	,SpecialistMedicalOtherConsultants= null
	,SpecialistMedicalPrimaryDiagnosisOnAdmission= null
	,SpecialistMedicalPrimaryDiagnosisOnAdmissionComment= null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmed= null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmedComment= null
	,SpecialistMedicalCauseOfDeath1a= null
	,SpecialistMedicalCauseOfDeath1b= null
	,SpecialistMedicalCauseOfDeath1c= null
	,SpecialistMedicalCauseOfDeath2= null
	,SpecialistMedicalCauseOfDeathComment= null
	,SpecialistMedicalCodedDiagnosis= null
	,SpecialistMedicalCodedDiagnosisComment= null
	,SpecialistMedicalAgreeWithCauseOfDeath= null
	,SpecialistMedicalAgreeWithCauseOfDeathComment= null
	,SpecialistMedicalHospitalPostmortem= null
	,SpecialistMedicalHospitalPostmortemComment= null
	,SpecialistMedicalCoronerInformedOrConsulted= null
	,SpecialistMedicalCoronerInformedOrConsultedComment= null
	,SpecialistMedicalCoronerPostmortemPerformed= null
	,SpecialistMedicalCoronerPostmortemPerformedComment= null
	,SpecialistMedicalMalignancyPresent= null
	,SpecialistMedicalMalignancyPresentComment= null
	,SpecialistMedicalCoExistingFactors= null
	,SpecialistMedicalCoExistingFactorsComment= null
	,SpecialistMedicalConsultantReview= null
	,SpecialistMedicalConsultantReviewComment= null
	,SpecialistMedicalEvidenceOfClearManagementPlan= null
	,SpecialistMedicalEvidenceOfClearManagementPlanComment= null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelay= null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelayComment= null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequate= null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequateComment= null
	,SpecialistMedicalOmissionsInInitialManagement= null
	,SpecialistMedicalOmissionsInInitialManagementComment= null
	,SpecialistMedicalAdmittedToAppropriateWard= null
	,SpecialistMedicalAdmittedToAppropriateWardComment= null
	,SpecialistMedicalHowManyWardsPatientOn= null
	,SpecialistMedicalHowManyWardsPatientOnComment= null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekday= null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekdayComment= null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeek= null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeekComment= null
	,SpecialistMedicalICUOpinionGiven= null
	,SpecialistMedicalICUOpinionGivenComment= null
	,SpecialistMedicalTransferFromGeneralToICUorHDU= null
	,SpecialistMedicalTransferFromGeneralToICUorHDUComment= null
	,SpecialistMedicalReadmissionToICUorHDU= null
	,SpecialistMedicalReadmissionToICUorHDUComment= null
	,SpecialistMedicalAnyRecognisableMedicationErrors= null
	,SpecialistMedicalAnyRecognisableMedicationErrorsComment= null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30Days= null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30DaysComment= null
	,SpecialistMedicalTimingOfDiagnosis= null
	,SpecialistMedicalTimingOfDiagnosisComment= null
	,SpecialistMedicalTimingOfDeliveringCare= null
	,SpecialistMedicalTimingOfDeliveringCareComment= null
	,SpecialistMedicalCommunicationIssues= null
	,SpecialistMedicalCommunicationIssuesComment= null
	,SpecialistMedicalAdverseEvents= null
	,SpecialistMedicalAdverseEventsComment= null
	,SpecialistMedicalIncidentReport= null
	,SpecialistMedicalIncidentReportComment= null
	,SpecialistMedicalAnythingCouldBeDoneDifferently= null
	,SpecialistMedicalAnythingCouldBeDoneDifferentlyComment= null
	,SpecialistMedicalDocumentationIssues= null
	,SpecialistMedicalDocumentationIssuesComment= null
	,SpecialistMedicalNotableGoodQualityCare= null
	,SpecialistMedicalLCPInPatientsNotes= null
	,SpecialistMedicalLCPInPatientsNotesComment= null
	,SpecialistMedicalPreferredPlaceOfDeathRecorded= null
	,SpecialistMedicalPreferredPlaceOfDeathRecordedComment= null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospital= null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospitalComment= null
	,SpecialistMedicalLessonsLearned= null
	,SpecialistMedicalActionPlan= null
	,SpecialistMedicalActionPlanReviewDate= null

	,SurgeryForename = null
	,SurgerySurname = null
	,SurgeryCasenoteNo = null
	,SurgeryDateOfDeath = null
	,SurgeryRecordedAppropriatelyToYou = null
	,SurgeryAlternateNamedConsultant = null
	,SurgeryTerminalCareDeathExpected = null
	,SurgeryAgreeWithCauseOfDeath = null
	,SurgeryDidSurgeryDuringLastAdmission = null
	,SurgeryHowManyDaysBeforeDeathApprox = null
	,SurgeryCoronerInformed = null
	,SurgeryHighLevelInvestigation = null
	,SurgeryCaseReferredToBleepReview = null
	,SurgerySurgicalComplicationContributedToDeath = null
	,SurgerySurgeryHigherThanNormalAnticipatedMortality = null
	,SurgeryStandardRiskPercentage = null
	,SurgeryMortalityQuotedPercentage = null
	,SurgeryDeathPreventable = null
	,SurgeryDeathPreventableComment = null
	,SurgeryTerminalCareManagedWell = null
	,SurgeryTerminalCareManagedWellComment = null
	,SurgeryCauseOfDeath1 = null
	,SurgeryCauseOfDeath2 = null
	,SurgeryCauseOfDeath3 = null
	,SurgeryCauseOfDeath4 = null
	,SurgeryCoMorbidity1 = null
	,SurgeryCoMorbidity2 = null
	,SurgeryCoMorbidity3 = null
	,SurgeryCoMorbidity4 = null
	,SurgeryCoMorbidity5 = null
	,SurgeryDeathDiscussedWithAnaesthetist = null
	,SurgeryAnaesthetist = null
	,SurgeryClassificationGrade = null

from
	[$(Warehouse)].Mortality.CriticalCare

union 

select
	SourceUniqueID
	,ReviewedDate
	,ReviewedBy
	,FormTypeID
	,ReviewStatus
	,ClassificationGrade = null
	,SourcePatientNo
	,EpisodeStartTime
	,ContextCode
	,CriticalCareCasenoteNo  = null
	,CriticalCareNHSNumber  = null
	,CriticalCareAge  = null
	,CriticalCareAdmissionTime  = null
	,CriticalCareDateOfDeath  = null
	,CriticalCarePlaceOfDeath = null
	,CriticalCareFirstConsultantReview = null
	,CriticalCareSpecialtyGroup = null
	,CriticalCarePatientName  = null
	,CriticalCarePrimaryDiagnosis  = null
	,CriticalCareConfirmedPrimaryDiagnosis = null
	,CriticalCareCauseOfDeath1a  = null
	,CriticalCareCauseOfDeath1b  = null
	,CriticalCareCauseOfDeath1c  = null
	,CriticalCareCauseOfDeath2  = null
	,CriticalCareCauseOfDeathComment  = null
	,CriticalCareCodedDiagnosis  = null
	,CriticalCareAgreeWithStatedCauseOfDeath  = null
	,CriticalCareHospitalPM  = null
	,CriticalCareCoronerInformed  = null
	,CriticalCareCoronerPM  = null
	,CriticalCareCoexistingFactor  = null
	,CriticalCareTimeClinicalDecisionToAdmitMade  = null
	,CriticalCareTimePatientAdmittedToCriticalCare  = null
	,CriticalCareAdmissionStickerUsed  = null
	,CriticalCareTimeToConsultantReviewComment  = null
	,CriticalCareEvidenceOfClearManagementPlan  = null
	,CriticalCareEvidenceOfClearManagementPlanComment  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelay  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelayComment  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequate  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequateComment  = null
	,CriticalCareOmissionsInInitialManagement  = null
	,CriticalCareOmissionsInInitialManagementComment  = null
	,CriticalCarePeriodAtrialFibrilation  = null
	,CriticalCarePeriodAtrialFibrilationComment  = null
	,CriticalCareAtrialFibrilationAppropriatelyManaged  = null
	,CriticalCareAtrialFibrilationAppropriatelyManagedComment  = null
	,CriticalCareEvidenceOfIschemiaInGut  = null
	,CriticalCareEvidenceOfIschemiaInGutComment  = null
	,CriticalCareTherapeuticAntiCoagulation  = null
	,CriticalCareTherapeuticAntiCoagulationComment  = null
	,CriticalCareRoundsDocumentedEveryDay  = null
	,CriticalCareRoundsDocumentedEveryDayComment  = null
	,CriticalCareAnyRecognisableMedicationErrors  = null
	,CriticalCareAnyRecognisableMedicationErrorsComment  = null
	,CriticalCareDelayInDiagnosis  = null
	,CriticalCareDelayInDiagnosisComment  = null
	,CriticalCareDelayInDeliveringCare  = null
	,CriticalCareDelayInOfDeliveringCareComment  = null
	,CriticalCarePoorCommunication  = null
	,CriticalCarePoorCommunicationComment  = null
	,CriticalCareAdverseEvents  = null
	,CriticalCareAdverseEventsComment  = null
	,CriticalCareAnythingCouldBeDoneDifferently  = null
	,CriticalCareAnythingCouldBeDoneDifferentlyComment  = null
	,CriticalCareNotableGoodQualityCare  = null
	,CriticalCareDocumentationIssues  = null
	,CriticalCareDocumentationIssuesComment  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotes  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotesComment  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecorded  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecordedComment  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressed  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressedComment  = null
	,CriticalCareLessonsLearned  = null
	,CriticalCareActionPlan  = null
	,CriticalCareDateOfActionPlanReview  = null
	,CriticalCareClassificationGrade  = null

	,GynaecologyCasenoteNo = CasenoteNo
	,GynaecologyDateOfDeath = Gynaecology.DateOfDeath
	,GynaecologyUnitNo = UnitNo
	,GynaecologyClinicalObs = ClinicalObs
	,GynaecologyClinicalObsDetail = ClinicalObsDetail
	,GynaecologyAdmissionTime = Gynaecology.AdmissionTime
	,GynaecologyCaseDetail = CaseDetail
	,GynaecologyPatientName = PatientName
	,GynaecologyDNR = DNR
	,GynaecologyDNRDetail = DNRDetail
	,GynaecologyDeathPreventable = DeathPreventable
	,GynaecologyDeathPreventableComment = DeathPreventableComment
	,GynaecologyNursingConcernsClinicalObs = NursingConcernsClinicalObs
	,GynaecologyNursingConcernsClinicalObsDetail = NursingConcernsClinicalObsDetail
	,GynaecologyInitialManagementStepsAppropriateAndAdequate = InitialManagementStepsAppropriateAndAdequate
	,GynaecologyInitialManagementStepsAppropriateAndAdequateComment = InitialManagementStepsAppropriateAndAdequateComment
	,GynaecologyPalliative = Palliative
	,GynaecologyPalliativeDetail = PalliativeDetail
	,GynaecologyNursingConcernsRoleInDeath = NursingConcernsRoleInDeath
	,GynaecologyNursingConcernsRoleInDeathDetail = NursingConcernsRoleInDeathDetail
	,GynaecologyTransferICU = TransferICU
	,GynaecologyTransferICUDetail = TransferICUDetail
	,GynaecologyTransferHDU = TransferHDU
	,GynaecologyTransferHDUDetail = TransferHDUDetail
	,GynaecologyMiscomm = Miscomm
	,GynaecologyMiscommDetail = MiscommDetail
	,GynaecologyPoorCommPrimaryandSecondary = PoorCommPrimaryandSecondary
	,GynaecologyPoorCommPrimaryandSecondaryDDetail = PoorCommPrimaryandSecondaryDDetail
	,GynaecologyNotableGoodQualityCare = NotableGoodQualityCare
	,GynaecologyEndOfLifeCareLCPInPatientNotes = EndOfLifeCareLCPInPatientNotes
	,GynaecologyEndOfLifeCareLCPApproTime = EndOfLifeCareLCPApproTime
	,GynaecologyEndOfLifeCareLCPApproTimeComment = EndOfLifeCareLCPApproTimeComment
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecorded = EndOfLifeCarePreferredPlaceOfDeathRecorded
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecordedComment = EndOfLifeCarePreferredPlaceOfDeathRecordedComment
	,GynaecologyLessonsLearned = LessonsLearned
	,GynaecologyActionPlan = ActionPlan
	,GynaecologyDateOfBirth = Gynaecology.DateOfBirth
	,GynaecologyConsultantDiscussionB4WDofIC = ConsultantDiscussionB4WDofIC
	,GynaecologyConsultantDiscussionB4WDofICComment = ConsultantDiscussionB4WDofICComment
	,GynaecologyEWSgteq3 = EWSgteq3
	,GynaecologyEWSgteq3Detail = EWSgteq3Detail
	,GynaecologyManageOutOfHospital = ManageOutOfHospital
	,GynaecologyWhereOutOfHospital = WhereOutOfHospital
	,GynaecologyPalliativeCareTeamInvolvement = PalliativeCareTeamInvolvement
	,GynaecologyPalliativeCareTeamInvolvementNoDetail = PalliativeCareTeamInvolvementNoDetail
	,GynaecologyAdmittedOutOfHours = AdmittedOutOfHours
	,GynaecologyTransferredOtherHospitalSiteInUnstableCondition = TransferredOtherHospitalSiteInUnstableCondition
	,GynaecologyTransferredOtherHospitalSiteInUnstableConditionDetail = TransferredOtherHospitalSiteInUnstableConditionDetail
	,GynaecologyClinicianNotListeningToPatient = ClinicianNotListeningToPatient
	,GynaecologyClinicianNotListeningtoPatientDetail = ClinicianNotListeningtoPatientDetail
	,GynaecologyPreviousFindingsEBMorHLI = PreviousFindingsEBMorHLI

	,NICUCasenoteNo = null
	,NICUAge = null
	,NICUDateOfDeath = null
	,NICUNamedConsultant = null
	,NICUPatientName = null
	,NICUTerminalCareDeathExpected = null
	,NICUTerminalCareDeathExpectedComment = null
	,NICUDeathPreventable = null
	,NICUTransferComment = null
	,NICUPostmortem = null
	,NICUPostmortemComment = null
	,NICUMedicationErrors = null
	,NICUMedicationErrorsComment = null
	,NICUCommunicationComment = null
	,NICUAnythingCouldBeDoneDifferentlyComment = null
	,NICUNotableGoodQualityCare = null
	,NICULessonsLearned = null
	,NICUReferenceNo = null
	,NICUGestation = null
	,NICUBirthWeight = null
	,NICUDateOfBirth = null
	,NICUSex = null
	,NICUEthnicity = null
	,NICUMRSA = null
	,NICUCriticalIncident = null
	,NICUCriticalIncidentDetail = null
	,NICUWithdrawalOfIC = null
	,NICUWithdrawalOfICDetail = null
	,NICUWithdrawalOfICLOTADNRFormsUsed = null
	,NICUSeverelyAbnormalCUSS = null
	,NICUConsultantDiscussionB4WDofIC = null
	,NICUBereavementAppointmentSent = null
	,NICUBereavementAppointmentSentDetail = null
	,NICUClinicalSummaryBackground = null
	,NICUClinicalSummaryProgress = null
	,NICUClinicalSummaryManagement = null
	,NICUClinicalSummarySurgical = null
	,NICUDiscussionObstetricIssues = null
	,NICUDiscussionResus = null
	,NICUDiscussionFirst48Hours = null
	,NICUDiscussionClinicalManagement = null
	,NICUDiscussionCommunication = null
	,NICUDiscussionDocumentation = null

	,SpecialistMedicalForename= null
	,SpecialistMedicalSurname= null
	,SpecialistMedicalCasenoteNo= null
	,SpecialistMedicalAge= null
	,SpecialistMedicalAdmissionTime= null
	,SpecialistMedicalDateOfDeath= null
	,SpecialistMedicalPlaceOfDeath= null
	,SpecialistMedicalLeadConsultant= null
	,SpecialistMedicalOtherConsultants= null
	,SpecialistMedicalPrimaryDiagnosisOnAdmission= null
	,SpecialistMedicalPrimaryDiagnosisOnAdmissionComment= null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmed= null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmedComment= null
	,SpecialistMedicalCauseOfDeath1a= null
	,SpecialistMedicalCauseOfDeath1b= null
	,SpecialistMedicalCauseOfDeath1c= null
	,SpecialistMedicalCauseOfDeath2= null
	,SpecialistMedicalCauseOfDeathComment= null
	,SpecialistMedicalCodedDiagnosis= null
	,SpecialistMedicalCodedDiagnosisComment= null
	,SpecialistMedicalAgreeWithCauseOfDeath= null
	,SpecialistMedicalAgreeWithCauseOfDeathComment= null
	,SpecialistMedicalHospitalPostmortem= null
	,SpecialistMedicalHospitalPostmortemComment= null
	,SpecialistMedicalCoronerInformedOrConsulted= null
	,SpecialistMedicalCoronerInformedOrConsultedComment= null
	,SpecialistMedicalCoronerPostmortemPerformed= null
	,SpecialistMedicalCoronerPostmortemPerformedComment= null
	,SpecialistMedicalMalignancyPresent= null
	,SpecialistMedicalMalignancyPresentComment= null
	,SpecialistMedicalCoExistingFactors= null
	,SpecialistMedicalCoExistingFactorsComment= null
	,SpecialistMedicalConsultantReview= null
	,SpecialistMedicalConsultantReviewComment= null
	,SpecialistMedicalEvidenceOfClearManagementPlan= null
	,SpecialistMedicalEvidenceOfClearManagementPlanComment= null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelay= null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelayComment= null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequate= null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequateComment= null
	,SpecialistMedicalOmissionsInInitialManagement= null
	,SpecialistMedicalOmissionsInInitialManagementComment= null
	,SpecialistMedicalAdmittedToAppropriateWard= null
	,SpecialistMedicalAdmittedToAppropriateWardComment= null
	,SpecialistMedicalHowManyWardsPatientOn= null
	,SpecialistMedicalHowManyWardsPatientOnComment= null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekday= null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekdayComment= null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeek= null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeekComment= null
	,SpecialistMedicalICUOpinionGiven= null
	,SpecialistMedicalICUOpinionGivenComment= null
	,SpecialistMedicalTransferFromGeneralToICUorHDU= null
	,SpecialistMedicalTransferFromGeneralToICUorHDUComment= null
	,SpecialistMedicalReadmissionToICUorHDU= null
	,SpecialistMedicalReadmissionToICUorHDUComment= null
	,SpecialistMedicalAnyRecognisableMedicationErrors= null
	,SpecialistMedicalAnyRecognisableMedicationErrorsComment= null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30Days= null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30DaysComment= null
	,SpecialistMedicalTimingOfDiagnosis= null
	,SpecialistMedicalTimingOfDiagnosisComment= null
	,SpecialistMedicalTimingOfDeliveringCare= null
	,SpecialistMedicalTimingOfDeliveringCareComment= null
	,SpecialistMedicalCommunicationIssues= null
	,SpecialistMedicalCommunicationIssuesComment= null
	,SpecialistMedicalAdverseEvents= null
	,SpecialistMedicalAdverseEventsComment= null
	,SpecialistMedicalIncidentReport= null
	,SpecialistMedicalIncidentReportComment= null
	,SpecialistMedicalAnythingCouldBeDoneDifferently= null
	,SpecialistMedicalAnythingCouldBeDoneDifferentlyComment= null
	,SpecialistMedicalDocumentationIssues= null
	,SpecialistMedicalDocumentationIssuesComment= null
	,SpecialistMedicalNotableGoodQualityCare= null
	,SpecialistMedicalLCPInPatientsNotes= null
	,SpecialistMedicalLCPInPatientsNotesComment= null
	,SpecialistMedicalPreferredPlaceOfDeathRecorded= null
	,SpecialistMedicalPreferredPlaceOfDeathRecordedComment= null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospital= null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospitalComment= null
	,SpecialistMedicalLessonsLearned= null
	,SpecialistMedicalActionPlan= null
	,SpecialistMedicalActionPlanReviewDate= null

	,SurgeryForename = null
	,SurgerySurname = null
	,SurgeryCasenoteNo = null
	,SurgeryDateOfDeath = null
	,SurgeryRecordedAppropriatelyToYou = null
	,SurgeryAlternateNamedConsultant = null
	,SurgeryTerminalCareDeathExpected = null
	,SurgeryAgreeWithCauseOfDeath = null
	,SurgeryDidSurgeryDuringLastAdmission = null
	,SurgeryHowManyDaysBeforeDeathApprox = null
	,SurgeryCoronerInformed = null
	,SurgeryHighLevelInvestigation = null
	,SurgeryCaseReferredToBleepReview = null
	,SurgerySurgicalComplicationContributedToDeath = null
	,SurgerySurgeryHigherThanNormalAnticipatedMortality = null
	,SurgeryStandardRiskPercentage = null
	,SurgeryMortalityQuotedPercentage = null
	,SurgeryDeathPreventable = null
	,SurgeryDeathPreventableComment = null
	,SurgeryTerminalCareManagedWell = null
	,SurgeryTerminalCareManagedWellComment = null
	,SurgeryCauseOfDeath1 = null
	,SurgeryCauseOfDeath2 = null
	,SurgeryCauseOfDeath3 = null
	,SurgeryCauseOfDeath4 = null
	,SurgeryCoMorbidity1 = null
	,SurgeryCoMorbidity2 = null
	,SurgeryCoMorbidity3 = null
	,SurgeryCoMorbidity4 = null
	,SurgeryCoMorbidity5 = null
	,SurgeryDeathDiscussedWithAnaesthetist = null
	,SurgeryAnaesthetist = null
	,SurgeryClassificationGrade = null

from
	[$(Warehouse)].Mortality.Gynaecology

union

select
	SourceUniqueID
	,ReviewedDate
	,ReviewedBy
	,FormTypeID
	,ReviewStatus
	,ClassificationGrade = null
	,SourcePatientNo
	,EpisodeStartTime
	,ContextCode
	,CriticalCareCasenoteNo  = null
	,CriticalCareNHSNumber  = null
	,CriticalCareAge  = null
	,CriticalCareAdmissionTime  = null
	,CriticalCareDateOfDeath  = null
	,CriticalCarePlaceOfDeath = null
	,CriticalCareFirstConsultantReview = null
	,CriticalCareSpecialtyGroup = null
	,CriticalCarePatientName  = null
	,CriticalCarePrimaryDiagnosis  = null
	,CriticalCareConfirmedPrimaryDiagnosis = null
	,CriticalCareCauseOfDeath1a  = null
	,CriticalCareCauseOfDeath1b  = null
	,CriticalCareCauseOfDeath1c  = null
	,CriticalCareCauseOfDeath2  = null
	,CriticalCareCauseOfDeathComment  = null
	,CriticalCareCodedDiagnosis  = null
	,CriticalCareAgreeWithStatedCauseOfDeath  = null
	,CriticalCareHospitalPM  = null
	,CriticalCareCoronerInformed  = null
	,CriticalCareCoronerPM  = null
	,CriticalCareCoexistingFactor  = null
	,CriticalCareTimeClinicalDecisionToAdmitMade  = null
	,CriticalCareTimePatientAdmittedToCriticalCare  = null
	,CriticalCareAdmissionStickerUsed  = null
	,CriticalCareTimeToConsultantReviewComment  = null
	,CriticalCareEvidenceOfClearManagementPlan  = null
	,CriticalCareEvidenceOfClearManagementPlanComment  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelay  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelayComment  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequate  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequateComment  = null
	,CriticalCareOmissionsInInitialManagement  = null
	,CriticalCareOmissionsInInitialManagementComment  = null
	,CriticalCarePeriodAtrialFibrilation  = null
	,CriticalCarePeriodAtrialFibrilationComment  = null
	,CriticalCareAtrialFibrilationAppropriatelyManaged  = null
	,CriticalCareAtrialFibrilationAppropriatelyManagedComment  = null
	,CriticalCareEvidenceOfIschemiaInGut  = null
	,CriticalCareEvidenceOfIschemiaInGutComment  = null
	,CriticalCareTherapeuticAntiCoagulation  = null
	,CriticalCareTherapeuticAntiCoagulationComment  = null
	,CriticalCareRoundsDocumentedEveryDay  = null
	,CriticalCareRoundsDocumentedEveryDayComment  = null
	,CriticalCareAnyRecognisableMedicationErrors  = null
	,CriticalCareAnyRecognisableMedicationErrorsComment  = null
	,CriticalCareDelayInDiagnosis  = null
	,CriticalCareDelayInDiagnosisComment  = null
	,CriticalCareDelayInDeliveringCare  = null
	,CriticalCareDelayInOfDeliveringCareComment  = null
	,CriticalCarePoorCommunication  = null
	,CriticalCarePoorCommunicationComment  = null
	,CriticalCareAdverseEvents  = null
	,CriticalCareAdverseEventsComment  = null
	,CriticalCareAnythingCouldBeDoneDifferently  = null
	,CriticalCareAnythingCouldBeDoneDifferentlyComment  = null
	,CriticalCareNotableGoodQualityCare  = null
	,CriticalCareDocumentationIssues  = null
	,CriticalCareDocumentationIssuesComment  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotes  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotesComment  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecorded  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecordedComment  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressed  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressedComment  = null
	,CriticalCareLessonsLearned  = null
	,CriticalCareActionPlan  = null
	,CriticalCareDateOfActionPlanReview  = null
	,CriticalCareClassificationGrade  = null

	,GynaecologyCasenoteNo = null
	,GynaecologyDateOfDeath = null
	,GynaecologyUnitNo = null
	,GynaecologyClinicalObs = null
	,GynaecologyClinicalObsDetail = null
	,GynaecologyAdmissionTime = null
	,GynaecologyCaseDetail = null
	,GynaecologyPatientName = null
	,GynaecologyDNR = null
	,GynaecologyDNRDetail = null
	,GynaecologyDeathPreventable = null
	,GynaecologyDeathPreventableComment = null
	,GynaecologyNursingConcernsClinicalObs = null
	,GynaecologyNursingConcernsClinicalObsDetail = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequate = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequateComment = null
	,GynaecologyPalliative = null
	,GynaecologyPalliativeDetail = null
	,GynaecologyNursingConcernsRoleInDeath = null
	,GynaecologyNursingConcernsRoleInDeathDetail = null
	,GynaecologyTransferICU = null
	,GynaecologyTransferICUDetail = null
	,GynaecologyTransferHDU = null
	,GynaecologyTransferHDUDetail = null
	,GynaecologyMiscomm = null
	,GynaecologyMiscommDetail = null
	,GynaecologyPoorCommPrimaryandSecondary = null
	,GynaecologyPoorCommPrimaryandSecondaryDDetail = null
	,GynaecologyNotableGoodQualityCare = null
	,GynaecologyEndOfLifeCareLCPInPatientNotes = null
	,GynaecologyEndOfLifeCareLCPApproTime = null
	,GynaecologyEndOfLifeCareLCPApproTimeComment = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecorded = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecordedComment = null
	,GynaecologyLessonsLearned = null
	,GynaecologyActionPlan = null
	,GynaecologyDateOfBirth = null
	,GynaecologyConsultantDiscussionB4WDofIC = null
	,GynaecologyConsultantDiscussionB4WDofICComment = null
	,GynaecologyEWSgteq3 = null
	,GynaecologyEWSgteq3Detail = null
	,GynaecologyManageOutOfHospital = null
	,GynaecologyWhereOutOfHospital = null
	,GynaecologyPalliativeCareTeamInvolvement = null
	,GynaecologyPalliativeCareTeamInvolvementNoDetail = null
	,GynaecologyAdmittedOutOfHours = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableCondition = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableConditionDetail = null
	,GynaecologyClinicianNotListeningToPatient = null
	,GynaecologyClinicianNotListeningtoPatientDetail = null
	,GynaecologyPreviousFindingsEBMorHLI = null

	,NICUCasenoteNo = CasenoteNo
	,NICUAge = Age
	,NICUDateOfDeath = NICU.DateOfDeath
	,NICUNamedConsultant = NamedConsultant
	,NICUPatientName = PatientName
	,NICUTerminalCareDeathExpected = TerminalCareDeathExpected
	,NICUTerminalCareDeathExpectedComment = TerminalCareDeathExpectedComment
	,NICUDeathPreventable = DeathPreventable
	,NICUTransferComment = TransferComment
	,NICUPostmortem = Postmortem
	,NICUPostmortemComment = PostmortemComment
	,NICUMedicationErrors = MedicationErrors
	,NICUMedicationErrorsComment = MedicationErrorsComment
	,NICUCommunicationComment = CommunicationComment
	,NICUAnythingCouldBeDoneDifferentlyComment = AnythingCouldBeDoneDifferentlyComment
	,NICUNotableGoodQualityCare = NotableGoodQualityCare
	,NICULessonsLearned = LessonsLearned
	,NICUReferenceNo = ReferenceNo
	,NICUGestation = Gestation
	,NICUBirthWeight = BirthWeight
	,NICUDateOfBirth = NICU.DateOfBirth
	,NICUSex = Sex
	,NICUEthnicity = Ethnicity
	,NICUMRSA = MRSA
	,NICUCriticalIncident = CriticalIncident
	,NICUCriticalIncidentDetail = CriticalIncidentDetail
	,NICUWithdrawalOfIC = WithdrawalOfIC
	,NICUWithdrawalOfICDetail = WithdrawalOfICDetail
	,NICUWithdrawalOfICLOTADNRFormsUsed = WithdrawalOfICLOTADNRFormsUsed
	,NICUSeverelyAbnormalCUSS = SeverelyAbnormalCUSS
	,NICUConsultantDiscussionB4WDofIC = ConsultantDiscussionB4WDofIC
	,NICUBereavementAppointmentSent = BereavementAppointmentSent
	,NICUBereavementAppointmentSentDetail = BereavementAppointmentSentDetail
	,NICUClinicalSummaryBackground = ClinicalSummaryBackground
	,NICUClinicalSummaryProgress = ClinicalSummaryProgress
	,NICUClinicalSummaryManagement = ClinicalSummaryManagement
	,NICUClinicalSummarySurgical = ClinicalSummarySurgical
	,NICUDiscussionObstetricIssues = DiscussionObstetricIssues
	,NICUDiscussionResus = DiscussionResus
	,NICUDiscussionFirst48Hours = DiscussionFirst48Hours
	,NICUDiscussionClinicalManagement = DiscussionClinicalManagement
	,NICUDiscussionCommunication = DiscussionCommunication
	,NICUDiscussionDocumentation = DiscussionDocumentation


	,SpecialistMedicalForename = null
	,SpecialistMedicalSurname = null
	,SpecialistMedicalCasenoteNo = null
	,SpecialistMedicalAge = null
	,SpecialistMedicalAdmissionTime = null
	,SpecialistMedicalDateOfDeath = null
	,SpecialistMedicalPlaceOfDeath = null
	,SpecialistMedicalLeadConsultant = null
	,SpecialistMedicalOtherConsultants = null
	,SpecialistMedicalPrimaryDiagnosisOnAdmission = null
	,SpecialistMedicalPrimaryDiagnosisOnAdmissionComment = null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmed = null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmedComment = null
	,SpecialistMedicalCauseOfDeath1a = null
	,SpecialistMedicalCauseOfDeath1b = null
	,SpecialistMedicalCauseOfDeath1c = null
	,SpecialistMedicalCauseOfDeath2 = null
	,SpecialistMedicalCauseOfDeathComment = null
	,SpecialistMedicalCodedDiagnosis = null
	,SpecialistMedicalCodedDiagnosisComment = null
	,SpecialistMedicalAgreeWithCauseOfDeath = null
	,SpecialistMedicalAgreeWithCauseOfDeathComment = null
	,SpecialistMedicalHospitalPostmortem = null
	,SpecialistMedicalHospitalPostmortemComment = null
	,SpecialistMedicalCoronerInformedOrConsulted = null
	,SpecialistMedicalCoronerInformedOrConsultedComment = null
	,SpecialistMedicalCoronerPostmortemPerformed = null
	,SpecialistMedicalCoronerPostmortemPerformedComment = null
	,SpecialistMedicalMalignancyPresent = null
	,SpecialistMedicalMalignancyPresentComment = null
	,SpecialistMedicalCoExistingFactors = null
	,SpecialistMedicalCoExistingFactorsComment = null
	,SpecialistMedicalConsultantReview = null
	,SpecialistMedicalConsultantReviewComment = null
	,SpecialistMedicalEvidenceOfClearManagementPlan = null
	,SpecialistMedicalEvidenceOfClearManagementPlanComment = null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelay = null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelayComment = null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequate = null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequateComment = null
	,SpecialistMedicalOmissionsInInitialManagement = null
	,SpecialistMedicalOmissionsInInitialManagementComment = null
	,SpecialistMedicalAdmittedToAppropriateWard = null
	,SpecialistMedicalAdmittedToAppropriateWardComment = null
	,SpecialistMedicalHowManyWardsPatientOn = null
	,SpecialistMedicalHowManyWardsPatientOnComment = null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekday = null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekdayComment = null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeek = null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeekComment = null
	,SpecialistMedicalICUOpinionGiven = null
	,SpecialistMedicalICUOpinionGivenComment = null
	,SpecialistMedicalTransferFromGeneralToICUorHDU = null
	,SpecialistMedicalTransferFromGeneralToICUorHDUComment = null
	,SpecialistMedicalReadmissionToICUorHDU = null
	,SpecialistMedicalReadmissionToICUorHDUComment = null
	,SpecialistMedicalAnyRecognisableMedicationErrors = null
	,SpecialistMedicalAnyRecognisableMedicationErrorsComment = null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30Days = null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30DaysComment = null
	,SpecialistMedicalTimingOfDiagnosis = null
	,SpecialistMedicalTimingOfDiagnosisComment = null
	,SpecialistMedicalTimingOfDeliveringCare = null
	,SpecialistMedicalTimingOfDeliveringCareComment = null
	,SpecialistMedicalCommunicationIssues = null
	,SpecialistMedicalCommunicationIssuesComment = null
	,SpecialistMedicalAdverseEvents = null
	,SpecialistMedicalAdverseEventsComment = null
	,SpecialistMedicalIncidentReport = null
	,SpecialistMedicalIncidentReportComment = null
	,SpecialistMedicalAnythingCouldBeDoneDifferently = null
	,SpecialistMedicalAnythingCouldBeDoneDifferentlyComment = null
	,SpecialistMedicalDocumentationIssues = null
	,SpecialistMedicalDocumentationIssuesComment = null
	,SpecialistMedicalNotableGoodQualityCare = null
	,SpecialistMedicalLCPInPatientsNotes = null
	,SpecialistMedicalLCPInPatientsNotesComment = null
	,SpecialistMedicalPreferredPlaceOfDeathRecorded = null
	,SpecialistMedicalPreferredPlaceOfDeathRecordedComment = null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospital = null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospitalComment = null
	,SpecialistMedicalLessonsLearned = null
	,SpecialistMedicalActionPlan = null
	,SpecialistMedicalActionPlanReviewDate = null

	,SurgeryForename = null
	,SurgerySurname = null
	,SurgeryCasenoteNo = null
	,SurgeryDateOfDeath = null
	,SurgeryRecordedAppropriatelyToYou = null
	,SurgeryAlternateNamedConsultant = null
	,SurgeryTerminalCareDeathExpected = null
	,SurgeryAgreeWithCauseOfDeath = null
	,SurgeryDidSurgeryDuringLastAdmission = null
	,SurgeryHowManyDaysBeforeDeathApprox = null
	,SurgeryCoronerInformed = null
	,SurgeryHighLevelInvestigation = null
	,SurgeryCaseReferredToBleepReview = null
	,SurgerySurgicalComplicationContributedToDeath = null
	,SurgerySurgeryHigherThanNormalAnticipatedMortality = null
	,SurgeryStandardRiskPercentage = null
	,SurgeryMortalityQuotedPercentage = null
	,SurgeryDeathPreventable = null
	,SurgeryDeathPreventableComment = null
	,SurgeryTerminalCareManagedWell = null
	,SurgeryTerminalCareManagedWellComment = null
	,SurgeryCauseOfDeath1 = null
	,SurgeryCauseOfDeath2 = null
	,SurgeryCauseOfDeath3 = null
	,SurgeryCauseOfDeath4 = null
	,SurgeryCoMorbidity1 = null
	,SurgeryCoMorbidity2 = null
	,SurgeryCoMorbidity3 = null
	,SurgeryCoMorbidity4 = null
	,SurgeryCoMorbidity5 = null
	,SurgeryDeathDiscussedWithAnaesthetist = null
	,SurgeryAnaesthetist = null
	,SurgeryClassificationGrade = null

from
	[$(Warehouse)].Mortality.NICU

union

select
	SourceUniqueID
	,ReviewedDate
	,ReviewedBy
	,FormTypeID
	,ReviewStatus
	,ClassificationGrade = null
	,SourcePatientNo
	,EpisodeStartTime
	,ContextCode
	,CriticalCareCasenoteNo  = null
	,CriticalCareNHSNumber  = null
	,CriticalCareAge  = null
	,CriticalCareAdmissionTime  = null
	,CriticalCareDateOfDeath  = null
	,CriticalCarePlaceOfDeath = null
	,CriticalCareFirstConsultantReview = null
	,CriticalCareSpecialtyGroup = null
	,CriticalCarePatientName  = null
	,CriticalCarePrimaryDiagnosis  = null
	,CriticalCareConfirmedPrimaryDiagnosis = null
	,CriticalCareCauseOfDeath1a  = null
	,CriticalCareCauseOfDeath1b  = null
	,CriticalCareCauseOfDeath1c  = null
	,CriticalCareCauseOfDeath2  = null
	,CriticalCareCauseOfDeathComment  = null
	,CriticalCareCodedDiagnosis  = null
	,CriticalCareAgreeWithStatedCauseOfDeath  = null
	,CriticalCareHospitalPM  = null
	,CriticalCareCoronerInformed  = null
	,CriticalCareCoronerPM  = null
	,CriticalCareCoexistingFactor  = null
	,CriticalCareTimeClinicalDecisionToAdmitMade  = null
	,CriticalCareTimePatientAdmittedToCriticalCare  = null
	,CriticalCareAdmissionStickerUsed  = null
	,CriticalCareTimeToConsultantReviewComment  = null
	,CriticalCareEvidenceOfClearManagementPlan  = null
	,CriticalCareEvidenceOfClearManagementPlanComment  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelay  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelayComment  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequate  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequateComment  = null
	,CriticalCareOmissionsInInitialManagement  = null
	,CriticalCareOmissionsInInitialManagementComment  = null
	,CriticalCarePeriodAtrialFibrilation  = null
	,CriticalCarePeriodAtrialFibrilationComment  = null
	,CriticalCareAtrialFibrilationAppropriatelyManaged  = null
	,CriticalCareAtrialFibrilationAppropriatelyManagedComment  = null
	,CriticalCareEvidenceOfIschemiaInGut  = null
	,CriticalCareEvidenceOfIschemiaInGutComment  = null
	,CriticalCareTherapeuticAntiCoagulation  = null
	,CriticalCareTherapeuticAntiCoagulationComment  = null
	,CriticalCareRoundsDocumentedEveryDay  = null
	,CriticalCareRoundsDocumentedEveryDayComment  = null
	,CriticalCareAnyRecognisableMedicationErrors  = null
	,CriticalCareAnyRecognisableMedicationErrorsComment  = null
	,CriticalCareDelayInDiagnosis  = null
	,CriticalCareDelayInDiagnosisComment  = null
	,CriticalCareDelayInDeliveringCare  = null
	,CriticalCareDelayInOfDeliveringCareComment  = null
	,CriticalCarePoorCommunication  = null
	,CriticalCarePoorCommunicationComment  = null
	,CriticalCareAdverseEvents  = null
	,CriticalCareAdverseEventsComment  = null
	,CriticalCareAnythingCouldBeDoneDifferently  = null
	,CriticalCareAnythingCouldBeDoneDifferentlyComment  = null
	,CriticalCareNotableGoodQualityCare  = null
	,CriticalCareDocumentationIssues  = null
	,CriticalCareDocumentationIssuesComment  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotes  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotesComment  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecorded  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecordedComment  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressed  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressedComment  = null
	,CriticalCareLessonsLearned  = null
	,CriticalCareActionPlan  = null
	,CriticalCareDateOfActionPlanReview  = null
	,CriticalCareClassificationGrade  = null

	,GynaecologyCasenoteNo = null
	,GynaecologyDateOfDeath = null
	,GynaecologyUnitNo = null
	,GynaecologyClinicalObs = null
	,GynaecologyClinicalObsDetail = null
	,GynaecologyAdmissionTime = null
	,GynaecologyCaseDetail = null
	,GynaecologyPatientName = null
	,GynaecologyDNR = null
	,GynaecologyDNRDetail = null
	,GynaecologyDeathPreventable = null
	,GynaecologyDeathPreventableComment = null
	,GynaecologyNursingConcernsClinicalObs = null
	,GynaecologyNursingConcernsClinicalObsDetail = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequate = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequateComment = null
	,GynaecologyPalliative = null
	,GynaecologyPalliativeDetail = null
	,GynaecologyNursingConcernsRoleInDeath = null
	,GynaecologyNursingConcernsRoleInDeathDetail = null
	,GynaecologyTransferICU = null
	,GynaecologyTransferICUDetail = null
	,GynaecologyTransferHDU = null
	,GynaecologyTransferHDUDetail = null
	,GynaecologyMiscomm = null
	,GynaecologyMiscommDetail = null
	,GynaecologyPoorCommPrimaryandSecondary = null
	,GynaecologyPoorCommPrimaryandSecondaryDDetail = null
	,GynaecologyNotableGoodQualityCare = null
	,GynaecologyEndOfLifeCareLCPInPatientNotes = null
	,GynaecologyEndOfLifeCareLCPApproTime = null
	,GynaecologyEndOfLifeCareLCPApproTimeComment = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecorded = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecordedComment = null
	,GynaecologyLessonsLearned = null
	,GynaecologyActionPlan = null
	,GynaecologyDateOfBirth = null
	,GynaecologyConsultantDiscussionB4WDofIC = null
	,GynaecologyConsultantDiscussionB4WDofICComment = null
	,GynaecologyEWSgteq3 = null
	,GynaecologyEWSgteq3Detail = null
	,GynaecologyManageOutOfHospital = null
	,GynaecologyWhereOutOfHospital = null
	,GynaecologyPalliativeCareTeamInvolvement = null
	,GynaecologyPalliativeCareTeamInvolvementNoDetail = null
	,GynaecologyAdmittedOutOfHours = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableCondition = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableConditionDetail = null
	,GynaecologyClinicianNotListeningToPatient = null
	,GynaecologyClinicianNotListeningtoPatientDetail = null
	,GynaecologyPreviousFindingsEBMorHLI = null

	,NICUCasenoteNo = null
	,NICUAge = null
	,NICUDateOfDeath = null
	,NICUNamedConsultant = null
	,NICUPatientName = null
	,NICUTerminalCareDeathExpected = null
	,NICUTerminalCareDeathExpectedComment = null
	,NICUDeathPreventable = null
	,NICUTransferComment = null
	,NICUPostmortem = null
	,NICUPostmortemComment = null
	,NICUMedicationErrors = null
	,NICUMedicationErrorsComment = null
	,NICUCommunicationComment = null
	,NICUAnythingCouldBeDoneDifferentlyComment = null
	,NICUNotableGoodQualityCare = null
	,NICULessonsLearned = null
	,NICUReferenceNo = null
	,NICUGestation = null
	,NICUBirthWeight = null
	,NICUDateOfBirth = null
	,NICUSex = null
	,NICUEthnicity = null
	,NICUMRSA = null
	,NICUCriticalIncident = null
	,NICUCriticalIncidentDetail = null
	,NICUWithdrawalOfIC = null
	,NICUWithdrawalOfICDetail = null
	,NICUWithdrawalOfICLOTADNRFormsUsed = null
	,NICUSeverelyAbnormalCUSS = null
	,NICUConsultantDiscussionB4WDofIC = null
	,NICUBereavementAppointmentSent = null
	,NICUBereavementAppointmentSentDetail = null
	,NICUClinicalSummaryBackground = null
	,NICUClinicalSummaryProgress = null
	,NICUClinicalSummaryManagement = null
	,NICUClinicalSummarySurgical = null
	,NICUDiscussionObstetricIssues = null
	,NICUDiscussionResus = null
	,NICUDiscussionFirst48Hours = null
	,NICUDiscussionClinicalManagement = null
	,NICUDiscussionCommunication = null
	,NICUDiscussionDocumentation = null

	,SpecialistMedicineForename = Forename
	,SpecialistMedicineSurname = Surname
	,SpecialistMedicineCasenoteNo = CasenoteNo
	,SpecialistMedicineAge = Age
	,SpecialistMedicineAdmissionTime = SpecialistMedicalServices.AdmissionTime
	,SpecialistMedicineDateOfDeath = SpecialistMedicalServices.DateOfDeath
	,SpecialistMedicinePlaceOfDeath = PlaceOfDeath
	,SpecialistMedicineLeadConsultant = LeadConsultant
	,SpecialistMedicineOtherConsultants = OtherConsultants
	,SpecialistMedicinePrimaryDiagnosisOnAdmission = PrimaryDiagnosisOnAdmission
	,SpecialistMedicinePrimaryDiagnosisOnAdmissionComment = PrimaryDiagnosisOnAdmissionComment
	,SpecialistMedicinePrimaryDiagnosisAfterTestsConfirmed = PrimaryDiagnosisAfterTestsConfirmed
	,SpecialistMedicinePrimaryDiagnosisAfterTestsConfirmedComment = PrimaryDiagnosisAfterTestsConfirmedComment
	,SpecialistMedicineCauseOfDeath1a = CauseOfDeath1a
	,SpecialistMedicineCauseOfDeath1b = CauseOfDeath1b
	,SpecialistMedicineCauseOfDeath1c = CauseOfDeath1c
	,SpecialistMedicineCauseOfDeath2 = CauseOfDeath2
	,SpecialistMedicineCauseOfDeathComment = CauseOfDeathComment
	,SpecialistMedicineCodedDiagnosis = CodedDiagnosis
	,SpecialistMedicineCodedDiagnosisComment = CodedDiagnosisComment
	,SpecialistMedicineAgreeWithCauseOfDeath = AgreeWithCauseOfDeath
	,SpecialistMedicineAgreeWithCauseOfDeathComment = AgreeWithCauseOfDeathComment
	,SpecialistMedicineHospitalPostmortem = HospitalPostmortem
	,SpecialistMedicineHospitalPostmortemComment = HospitalPostmortemComment
	,SpecialistMedicineCoronerInformedOrConsulted = CoronerInformedOrConsulted
	,SpecialistMedicineCoronerInformedOrConsultedComment = CoronerInformedOrConsultedComment
	,SpecialistMedicineCoronerPostmortemPerformed = CoronerPostmortemPerformed
	,SpecialistMedicineCoronerPostmortemPerformedComment = CoronerPostmortemPerformedComment
	,SpecialistMedicineMalignancyPresent = MalignancyPresent
	,SpecialistMedicineMalignancyPresentComment = MalignancyPresentComment
	,SpecialistMedicineCoExistingFactors = CoExistingFactors
	,SpecialistMedicineCoExistingFactorsComment = CoExistingFactorsComment
	,SpecialistMedicineConsultantReview = ConsultantReview
	,SpecialistMedicineConsultantReviewComment = ConsultantReviewComment
	,SpecialistMedicineEvidenceOfClearManagementPlan = EvidenceOfClearManagementPlan
	,SpecialistMedicineEvidenceOfClearManagementPlanComment = EvidenceOfClearManagementPlanComment
	,SpecialistMedicineEssentialInvestigationsObtainedWithoutDelay = EssentialInvestigationsObtainedWithoutDelay
	,SpecialistMedicineEssentialInvestigationsObtainedWithoutDelayComment = EssentialInvestigationsObtainedWithoutDelayComment
	,SpecialistMedicineInitialManagementStepsAppropriateAndAdequate = InitialManagementStepsAppropriateAndAdequate
	,SpecialistMedicineInitialManagementStepsAppropriateAndAdequateComment = InitialManagementStepsAppropriateAndAdequateComment
	,SpecialistMedicineOmissionsInInitialManagement = OmissionsInInitialManagement
	,SpecialistMedicineOmissionsInInitialManagementComment = OmissionsInInitialManagementComment
	,SpecialistMedicineAdmittedToAppropriateWard = AdmittedToAppropriateWard
	,SpecialistMedicineAdmittedToAppropriateWardComment = AdmittedToAppropriateWardComment
	,SpecialistMedicineHowManyWardsPatientOn = HowManyWardsPatientOn
	,SpecialistMedicineHowManyWardsPatientOnComment = HowManyWardsPatientOnComment
	,SpecialistMedicineMedicalStaffWriteInNotesEveryWeekday = MedicalStaffWriteInNotesEveryWeekday
	,SpecialistMedicineMedicalStaffWriteInNotesEveryWeekdayComment = MedicalStaffWriteInNotesEveryWeekdayComment
	,SpecialistMedicineConsultantReviewAtLeastTwiceAWeek = ConsultantReviewAtLeastTwiceAWeek
	,SpecialistMedicineConsultantReviewAtLeastTwiceAWeekComment = ConsultantReviewAtLeastTwiceAWeekComment
	,SpecialistMedicineICUOpinionGiven = ICUOpinionGiven
	,SpecialistMedicineICUOpinionGivenComment = ICUOpinionGivenComment
	,SpecialistMedicineTransferFromGeneralToICUorHDU = TransferFromGeneralToICUorHDU
	,SpecialistMedicineTransferFromGeneralToICUorHDUComment = TransferFromGeneralToICUorHDUComment
	,SpecialistMedicineReadmissionToICUorHDU = ReadmissionToICUorHDU
	,SpecialistMedicineReadmissionToICUorHDUComment = ReadmissionToICUorHDUComment
	,SpecialistMedicineAnyRecognisableMedicationErrors = AnyRecognisableMedicationErrors
	,SpecialistMedicineAnyRecognisableMedicationErrorsComment = AnyRecognisableMedicationErrorsComment
	,SpecialistMedicineChemotherapyOrImmunosuppressantsWithin30Days = ChemotherapyOrImmunosuppressantsWithin30Days
	,SpecialistMedicineChemotherapyOrImmunosuppressantsWithin30DaysComment = ChemotherapyOrImmunosuppressantsWithin30DaysComment
	,SpecialistMedicineTimingOfDiagnosis = TimingOfDiagnosis
	,SpecialistMedicineTimingOfDiagnosisComment = TimingOfDiagnosisComment
	,SpecialistMedicineTimingOfDeliveringCare = TimingOfDeliveringCare
	,SpecialistMedicineTimingOfDeliveringCareComment = TimingOfDeliveringCareComment
	,SpecialistMedicineCommunicationIssues = CommunicationIssues
	,SpecialistMedicineCommunicationIssuesComment = CommunicationIssuesComment
	,SpecialistMedicineAdverseEvents = AdverseEvents
	,SpecialistMedicineAdverseEventsComment = AdverseEventsComment
	,SpecialistMedicineIncidentReport = IncidentReport
	,SpecialistMedicineIncidentReportComment = IncidentReportComment
	,SpecialistMedicineAnythingCouldBeDoneDifferently = AnythingCouldBeDoneDifferently
	,SpecialistMedicineAnythingCouldBeDoneDifferentlyComment = AnythingCouldBeDoneDifferentlyComment
	,SpecialistMedicineDocumentationIssues = DocumentationIssues
	,SpecialistMedicineDocumentationIssuesComment = DocumentationIssuesComment
	,SpecialistMedicineNotableGoodQualityCare = NotableGoodQualityCare
	,SpecialistMedicineLCPInPatientsNotes = LCPInPatientsNotes
	,SpecialistMedicineLCPInPatientsNotesComment = LCPInPatientsNotesComment
	,SpecialistMedicinePreferredPlaceOfDeathRecorded = PreferredPlaceOfDeathRecorded
	,SpecialistMedicinePreferredPlaceOfDeathRecordedComment = PreferredPlaceOfDeathRecordedComment
	,SpecialistMedicineExpressedWithToDieOutsideOfHospital = ExpressedWithToDieOutsideOfHospital
	,SpecialistMedicineExpressedWithToDieOutsideOfHospitalComment = ExpressedWithToDieOutsideOfHospitalComment
	,SpecialistMedicineLessonsLearned = LessonsLearned
	,SpecialistMedicineActionPlan = ActionPlan
	,SpecialistMedicineActionPlanReviewDate = ActionPlanReviewDate

	,SurgeryForename = null
	,SurgerySurname = null
	,SurgeryCasenoteNo = null
	,SurgeryDateOfDeath = null
	,SurgeryRecordedAppropriatelyToYou = null
	,SurgeryAlternateNamedConsultant = null
	,SurgeryTerminalCareDeathExpected = null
	,SurgeryAgreeWithCauseOfDeath = null
	,SurgeryDidSurgeryDuringLastAdmission = null
	,SurgeryHowManyDaysBeforeDeathApprox = null
	,SurgeryCoronerInformed = null
	,SurgeryHighLevelInvestigation = null
	,SurgeryCaseReferredToBleepReview = null
	,SurgerySurgicalComplicationContributedToDeath = null
	,SurgerySurgeryHigherThanNormalAnticipatedMortality = null
	,SurgeryStandardRiskPercentage = null
	,SurgeryMortalityQuotedPercentage = null
	,SurgeryDeathPreventable = null
	,SurgeryDeathPreventableComment = null
	,SurgeryTerminalCareManagedWell = null
	,SurgeryTerminalCareManagedWellComment = null
	,SurgeryCauseOfDeath1 = null
	,SurgeryCauseOfDeath2 = null
	,SurgeryCauseOfDeath3 = null
	,SurgeryCauseOfDeath4 = null
	,SurgeryCoMorbidity1 = null
	,SurgeryCoMorbidity2 = null
	,SurgeryCoMorbidity3 = null
	,SurgeryCoMorbidity4 = null
	,SurgeryCoMorbidity5 = null
	,SurgeryDeathDiscussedWithAnaesthetist = null
	,SurgeryAnaesthetist = null
	,SurgeryClassificationGrade = null

from
	[$(Warehouse)].Mortality.SpecialistMedicalServices

--inner join
--	[$(Warehouse)].APC.Encounter
--on	SpecialistMedicalServices.EpisodeStartTime = Encounter.EpisodeStartTime
--and	SpecialistMedicalServices.SourcePatientNo = Encounter.SourcePatientNo

union

select
	SourceUniqueID
	,ReviewedDate
	,ReviewedBy
	,FormTypeID
	,ReviewStatus
	,ClassificationGrade = ClassificationGrade
	,SourcePatientNo
	,EpisodeStartTime
	,ContextCode
	,CriticalCareCasenoteNo  = null
	,CriticalCareNHSNumber  = null
	,CriticalCareAge  = null
	,CriticalCareAdmissionTime  = null
	,CriticalCareDateOfDeath  = null
	,CriticalCarePlaceOfDeath = null
	,CriticalCareFirstConsultantReview = null
	,CriticalCareSpecialtyGroup = null
	,CriticalCarePatientName  = null
	,CriticalCarePrimaryDiagnosis  = null
	,CriticalCareConfirmedPrimaryDiagnosis = null
	,CriticalCareCauseOfDeath1a  = null
	,CriticalCareCauseOfDeath1b  = null
	,CriticalCareCauseOfDeath1c  = null
	,CriticalCareCauseOfDeath2  = null
	,CriticalCareCauseOfDeathComment  = null
	,CriticalCareCodedDiagnosis  = null
	,CriticalCareAgreeWithStatedCauseOfDeath  = null
	,CriticalCareHospitalPM  = null
	,CriticalCareCoronerInformed  = null
	,CriticalCareCoronerPM  = null
	,CriticalCareCoexistingFactor  = null
	,CriticalCareTimeClinicalDecisionToAdmitMade  = null
	,CriticalCareTimePatientAdmittedToCriticalCare  = null
	,CriticalCareAdmissionStickerUsed  = null
	,CriticalCareTimeToConsultantReviewComment  = null
	,CriticalCareEvidenceOfClearManagementPlan  = null
	,CriticalCareEvidenceOfClearManagementPlanComment  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelay  = null
	,CriticalCareEssentialInvestigationsObtainedWithoutDelayComment  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequate  = null
	,CriticalCareInitialManagementStepsAppropriateAndAdequateComment  = null
	,CriticalCareOmissionsInInitialManagement  = null
	,CriticalCareOmissionsInInitialManagementComment  = null
	,CriticalCarePeriodAtrialFibrilation  = null
	,CriticalCarePeriodAtrialFibrilationComment  = null
	,CriticalCareAtrialFibrilationAppropriatelyManaged  = null
	,CriticalCareAtrialFibrilationAppropriatelyManagedComment  = null
	,CriticalCareEvidenceOfIschemiaInGut  = null
	,CriticalCareEvidenceOfIschemiaInGutComment  = null
	,CriticalCareTherapeuticAntiCoagulation  = null
	,CriticalCareTherapeuticAntiCoagulationComment  = null
	,CriticalCareRoundsDocumentedEveryDay  = null
	,CriticalCareRoundsDocumentedEveryDayComment  = null
	,CriticalCareAnyRecognisableMedicationErrors  = null
	,CriticalCareAnyRecognisableMedicationErrorsComment  = null
	,CriticalCareDelayInDiagnosis  = null
	,CriticalCareDelayInDiagnosisComment  = null
	,CriticalCareDelayInDeliveringCare  = null
	,CriticalCareDelayInOfDeliveringCareComment  = null
	,CriticalCarePoorCommunication  = null
	,CriticalCarePoorCommunicationComment  = null
	,CriticalCareAdverseEvents  = null
	,CriticalCareAdverseEventsComment  = null
	,CriticalCareAnythingCouldBeDoneDifferently  = null
	,CriticalCareAnythingCouldBeDoneDifferentlyComment  = null
	,CriticalCareNotableGoodQualityCare  = null
	,CriticalCareDocumentationIssues  = null
	,CriticalCareDocumentationIssuesComment  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotes  = null
	,CriticalCareEndOfLifeCareLCPInPatientNotesComment  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecorded  = null
	,CriticalCareEndOfLifeCarePreferredPlaceOfDeathRecordedComment  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressed  = null
	,CriticalCareEndOfLifeCareWishToDieOutsideHospitalExpressedComment  = null
	,CriticalCareLessonsLearned  = null
	,CriticalCareActionPlan  = null
	,CriticalCareDateOfActionPlanReview  = null
	,CriticalCareClassificationGrade  = null

	,GynaecologyCasenoteNo = null
	,GynaecologyDateOfDeath = null
	,GynaecologyUnitNo = null
	,GynaecologyClinicalObs = null
	,GynaecologyClinicalObsDetail = null
	,GynaecologyAdmissionTime = null
	,GynaecologyCaseDetail = null
	,GynaecologyPatientName = null
	,GynaecologyDNR = null
	,GynaecologyDNRDetail = null
	,GynaecologyDeathPreventable = null
	,GynaecologyDeathPreventableComment = null
	,GynaecologyNursingConcernsClinicalObs = null
	,GynaecologyNursingConcernsClinicalObsDetail = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequate = null
	,GynaecologyInitialManagementStepsAppropriateAndAdequateComment = null
	,GynaecologyPalliative = null
	,GynaecologyPalliativeDetail = null
	,GynaecologyNursingConcernsRoleInDeath = null
	,GynaecologyNursingConcernsRoleInDeathDetail = null
	,GynaecologyTransferICU = null
	,GynaecologyTransferICUDetail = null
	,GynaecologyTransferHDU = null
	,GynaecologyTransferHDUDetail = null
	,GynaecologyMiscomm = null
	,GynaecologyMiscommDetail = null
	,GynaecologyPoorCommPrimaryandSecondary = null
	,GynaecologyPoorCommPrimaryandSecondaryDDetail = null
	,GynaecologyNotableGoodQualityCare = null
	,GynaecologyEndOfLifeCareLCPInPatientNotes = null
	,GynaecologyEndOfLifeCareLCPApproTime = null
	,GynaecologyEndOfLifeCareLCPApproTimeComment = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecorded = null
	,GynaecologyEndOfLifeCarePreferredPlaceOfDeathRecordedComment = null
	,GynaecologyLessonsLearned = null
	,GynaecologyActionPlan = null
	,GynaecologyDateOfBirth = null
	,GynaecologyConsultantDiscussionB4WDofIC = null
	,GynaecologyConsultantDiscussionB4WDofICComment = null
	,GynaecologyEWSgteq3 = null
	,GynaecologyEWSgteq3Detail = null
	,GynaecologyManageOutOfHospital = null
	,GynaecologyWhereOutOfHospital = null
	,GynaecologyPalliativeCareTeamInvolvement = null
	,GynaecologyPalliativeCareTeamInvolvementNoDetail = null
	,GynaecologyAdmittedOutOfHours = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableCondition = null
	,GynaecologyTransferredOtherHospitalSiteInUnstableConditionDetail = null
	,GynaecologyClinicianNotListeningToPatient = null
	,GynaecologyClinicianNotListeningtoPatientDetail = null
	,GynaecologyPreviousFindingsEBMorHLI = null

	,NICUCasenoteNo = null
	,NICUAge = null
	,NICUDateOfDeath = null
	,NICUNamedConsultant = null
	,NICUPatientName = null
	,NICUTerminalCareDeathExpected = null
	,NICUTerminalCareDeathExpectedComment = null
	,NICUDeathPreventable = null
	,NICUTransferComment = null
	,NICUPostmortem = null
	,NICUPostmortemComment = null
	,NICUMedicationErrors = null
	,NICUMedicationErrorsComment = null
	,NICUCommunicationComment = null
	,NICUAnythingCouldBeDoneDifferentlyComment = null
	,NICUNotableGoodQualityCare = null
	,NICULessonsLearned = null
	,NICUReferenceNo = null
	,NICUGestation = null
	,NICUBirthWeight = null
	,NICUDateOfBirth = null
	,NICUSex = null
	,NICUEthnicity = null
	,NICUMRSA = null
	,NICUCriticalIncident = null
	,NICUCriticalIncidentDetail = null
	,NICUWithdrawalOfIC = null
	,NICUWithdrawalOfICDetail = null
	,NICUWithdrawalOfICLOTADNRFormsUsed = null
	,NICUSeverelyAbnormalCUSS = null
	,NICUConsultantDiscussionB4WDofIC = null
	,NICUBereavementAppointmentSent = null
	,NICUBereavementAppointmentSentDetail = null
	,NICUClinicalSummaryBackground = null
	,NICUClinicalSummaryProgress = null
	,NICUClinicalSummaryManagement = null
	,NICUClinicalSummarySurgical = null
	,NICUDiscussionObstetricIssues = null
	,NICUDiscussionResus = null
	,NICUDiscussionFirst48Hours = null
	,NICUDiscussionClinicalManagement = null
	,NICUDiscussionCommunication = null
	,NICUDiscussionDocumentation = null

	,SpecialistMedicalForename = null
	,SpecialistMedicalSurname = null
	,SpecialistMedicalCasenoteNo = null
	,SpecialistMedicalAge = null
	,SpecialistMedicalAdmissionTime = null
	,SpecialistMedicalDateOfDeath = null
	,SpecialistMedicalPlaceOfDeath = null
	,SpecialistMedicalLeadConsultant = null
	,SpecialistMedicalOtherConsultants = null
	,SpecialistMedicalPrimaryDiagnosisOnAdmission = null
	,SpecialistMedicalPrimaryDiagnosisOnAdmissionComment = null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmed = null
	,SpecialistMedicalPrimaryDiagnosisAfterTestsConfirmedComment = null
	,SpecialistMedicalCauseOfDeath1a = null
	,SpecialistMedicalCauseOfDeath1b = null
	,SpecialistMedicalCauseOfDeath1c = null
	,SpecialistMedicalCauseOfDeath2 = null
	,SpecialistMedicalCauseOfDeathComment = null
	,SpecialistMedicalCodedDiagnosis = null
	,SpecialistMedicalCodedDiagnosisComment = null
	,SpecialistMedicalAgreeWithCauseOfDeath = null
	,SpecialistMedicalAgreeWithCauseOfDeathComment = null
	,SpecialistMedicalHospitalPostmortem = null
	,SpecialistMedicalHospitalPostmortemComment = null
	,SpecialistMedicalCoronerInformedOrConsulted = null
	,SpecialistMedicalCoronerInformedOrConsultedComment = null
	,SpecialistMedicalCoronerPostmortemPerformed = null
	,SpecialistMedicalCoronerPostmortemPerformedComment = null
	,SpecialistMedicalMalignancyPresent = null
	,SpecialistMedicalMalignancyPresentComment = null
	,SpecialistMedicalCoExistingFactors = null
	,SpecialistMedicalCoExistingFactorsComment = null
	,SpecialistMedicalConsultantReview = null
	,SpecialistMedicalConsultantReviewComment = null
	,SpecialistMedicalEvidenceOfClearManagementPlan = null
	,SpecialistMedicalEvidenceOfClearManagementPlanComment = null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelay = null
	,SpecialistMedicalEssentialInvestigationsObtainedWithoutDelayComment = null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequate = null
	,SpecialistMedicalInitialManagementStepsAppropriateAndAdequateComment = null
	,SpecialistMedicalOmissionsInInitialManagement = null
	,SpecialistMedicalOmissionsInInitialManagementComment = null
	,SpecialistMedicalAdmittedToAppropriateWard = null
	,SpecialistMedicalAdmittedToAppropriateWardComment = null
	,SpecialistMedicalHowManyWardsPatientOn = null
	,SpecialistMedicalHowManyWardsPatientOnComment = null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekday = null
	,SpecialistMedicalMedicalStaffWriteInNotesEveryWeekdayComment = null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeek = null
	,SpecialistMedicalConsultantReviewAtLeastTwiceAWeekComment = null
	,SpecialistMedicalICUOpinionGiven = null
	,SpecialistMedicalICUOpinionGivenComment = null
	,SpecialistMedicalTransferFromGeneralToICUorHDU = null
	,SpecialistMedicalTransferFromGeneralToICUorHDUComment = null
	,SpecialistMedicalReadmissionToICUorHDU = null
	,SpecialistMedicalReadmissionToICUorHDUComment = null
	,SpecialistMedicalAnyRecognisableMedicationErrors = null
	,SpecialistMedicalAnyRecognisableMedicationErrorsComment = null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30Days = null
	,SpecialistMedicalChemotherapyOrImmunosuppressantsWithin30DaysComment = null
	,SpecialistMedicalTimingOfDiagnosis = null
	,SpecialistMedicalTimingOfDiagnosisComment = null
	,SpecialistMedicalTimingOfDeliveringCare = null
	,SpecialistMedicalTimingOfDeliveringCareComment = null
	,SpecialistMedicalCommunicationIssues = null
	,SpecialistMedicalCommunicationIssuesComment = null
	,SpecialistMedicalAdverseEvents = null
	,SpecialistMedicalAdverseEventsComment = null
	,SpecialistMedicalIncidentReport = null
	,SpecialistMedicalIncidentReportComment = null
	,SpecialistMedicalAnythingCouldBeDoneDifferently = null
	,SpecialistMedicalAnythingCouldBeDoneDifferentlyComment = null
	,SpecialistMedicalDocumentationIssues = null
	,SpecialistMedicalDocumentationIssuesComment = null
	,SpecialistMedicalNotableGoodQualityCare = null
	,SpecialistMedicalLCPInPatientsNotes = null
	,SpecialistMedicalLCPInPatientsNotesComment = null
	,SpecialistMedicalPreferredPlaceOfDeathRecorded = null
	,SpecialistMedicalPreferredPlaceOfDeathRecordedComment = null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospital = null
	,SpecialistMedicalExpressedWithToDieOutsideOfHospitalComment = null
	,SpecialistMedicalLessonsLearned = null
	,SpecialistMedicalActionPlan = null
	,SpecialistMedicalActionPlanReviewDate = null

	,SurgeryForename = Forename
	,SurgerySurname = Surname
	,SurgeryCasenoteNo = CasenoteNo
	,SurgeryDateOfDeath = Surgery.DateOfDeath
	,SurgeryRecordedAppropriatelyToYou = RecordedAppropriatelyToYou
	,SurgeryAlternateNamedConsultant = AlternateNamedConsultant
	,SurgeryTerminalCareDeathExpected = TerminalCareDeathExpected
	,SurgeryAgreeWithCauseOfDeath = AgreeWithCauseOfDeath
	,SurgeryDidSurgeryDuringLastAdmission = DidSurgeryDuringLastAdmission
	,SurgeryHowManyDaysBeforeDeathApprox = HowManyDaysBeforeDeathApprox
	,SurgeryCoronerInformed = CoronerInformed
	,SurgeryHighLevelInvestigation = HighLevelInvestigation
	,SurgeryCaseReferredToBleepReview = CaseReferredToBleepReview
	,SurgerySurgicalComplicationContributedToDeath = SurgicalComplicationContributedToDeath
	,SurgerySurgeryHigherThanNormalAnticipatedMortality = SurgeryHigherThanNormalAnticipatedMortality
	,SurgeryStandardRiskPercentage = StandardRiskPercentage
	,SurgeryMortalityQuotedPercentage = MortalityQuotedPercentage
	,SurgeryDeathPreventable = DeathPreventable
	,SurgeryDeathPreventableComment = DeathPreventableComment
	,SurgeryTerminalCareManagedWell = TerminalCareManagedWell
	,SurgeryTerminalCareManagedWellComment = TerminalCareManagedWellComment
	,SurgeryCauseOfDeath1 = CauseOfDeath1
	,SurgeryCauseOfDeath2 = CauseOfDeath2
	,SurgeryCauseOfDeath3 = CauseOfDeath3
	,SurgeryCauseOfDeath4 = CauseOfDeath4
	,SurgeryCoMorbidity1 = CoMorbidity1
	,SurgeryCoMorbidity2 = CoMorbidity2
	,SurgeryCoMorbidity3 = CoMorbidity3
	,SurgeryCoMorbidity4 = CoMorbidity4
	,SurgeryCoMorbidity5 = CoMorbidity5
	,SurgeryDeathDiscussedWithAnaesthetist = DeathDiscussedWithAnaesthetist
	,SurgeryAnaesthetist = Anaesthetist
	,SurgeryClassificationGrade = ClassificationGrade

from
	[$(Warehouse)].Mortality.Surgery


	) Mortality
	
	
inner join
	[$(Warehouse)].APC.Encounter
on	Mortality.EpisodeStartTime = Encounter.EpisodeStartTime
and	Mortality.SourcePatientNo = Encounter.SourcePatientNo
and Mortality.ContextCode = 'CEN||PAS'
	

--where
--	APCSourceUniqueID is not null



































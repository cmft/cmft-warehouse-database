﻿CREATE view [ETL].[TLoadTraTheatreBaseTimetableDetail] as

select
	 TimetableDetail.TimetableDetailCode
	,TimetableDetail.SessionNumber
	,TimetableDetail.DayNumber
	,TimetableDetail.StartDate
	,TimetableDetail.EndDate
	,TimetableDetail.TheatreCode
	,TimetableDetail.SessionStartTime
	,TimetableDetail.SessionEndTime
	,ConsultantCode = Consultant.StaffCode
	,AnaesthetistCode = Anaesthetist.StaffCode
	,SpecialtyCode = Specialty.SpecialtyCode
	,TimetableDetail.TimetableTemplateCode
	,TimetableDetail.LogLastUpdated
	,TimetableDetail.RecordLogDetails
	,TimetableDetail.SessionMinutes
from
	[$(TraffordWarehouse)].Theatre.TimetableDetail

left join [$(TraffordWarehouse)].Theatre.Staff Anaesthetist
on	Anaesthetist.StaffCode1 = TimetableDetail.AnaesthetistCode

left join [$(TraffordWarehouse)].Theatre.Staff Consultant
on	Consultant.StaffCode1 = TimetableDetail.ConsultantCode

left join [$(TraffordWarehouse)].Theatre.Specialty
on	Specialty.SpecialtyCode1 = TimetableDetail.SpecialtyCode

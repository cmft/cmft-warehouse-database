﻿
CREATE view ETL.TLoadOPBaseFertility 
as
 
 
select 
	FertilityRecno
	,SourceID 
	,HospitalNumber 
	,PatientForename
	,PatientSurname
	,EncounterDate 
	,TreatmentType
	,DateOfBirth 
	,Age
	,ReferralSource 
	,InterfaceCode 
	,ContextCode = 'CEN||ACU'
from 
	[$(Warehouse)].OP.Fertility


	

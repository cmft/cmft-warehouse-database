﻿







CREATE view [ETL].[TLoadIncidentBaseIncident]

as

select
	 [IncidentRecno]
	,[SourceIncidentCode]
	,[IncidentCode]
	,[IncidentNumber]
	,[Description]
	,[IncidentDate]
	,[IncidentTime]
	,[ReportedDate]
	,[ReportedTime]
	,[ReceiptDate]
	,[CauseCode1]
	,[CauseCode2]
	,[IncidentTypeCode]
	,[ContributingFactorCode]
	,[SignedByCode]
	,[IncidentGradeCode]
	,[CompletedByCode]
	,[SeverityCode]
	,[LikelihoodCode]
	,[RiskRatingCode]
	,[RiskScore]
	,[SpecialtyCode]
	,[PatientSafety]
	,[CauseGroupCode]
	,[SeriousUntowardIncident]
	,[ResearchOrTrialProject]
	,[InitialSeverityCode]
	,[InitialLikelihoodCode]
	,[InitialRiskRatingCode]
	,[InitialRiskScore]
	,[SecurityIncidentReportingSystemReportable]
	,[StatusTypeCode]
	,[NeverEvent]
	,[OrganisationCode]
	,[SiteTypeCode]
	,[SiteCode]
	,SourceDivisionCode = DivisionCode -- DG - 23.6.14 - Renamed as in allocation process we are allocating directorate. Colin wants to retain source codes for validation work
	,SourceDirectorateCode = DirectorateCode -- DG - 23.6.14 - Renamed as in allocation process we are allocating directorate. Colin wants to retain source codes for validation work
	,[LocationCode]
	,[DepartmentCode]
	,[WardCode]
	,[SequenceNo]
	,[PersonCode]
	,[PersonTypeCode]
	,[EntityTypeCode]
	,[DateOfBirth]
	,[SexCode]
	,CasenoteNumber
	,NHSNumber
	,[InterfaceCode]
	,[Created]
	,[Updated]
	,[ByWhom]
	,ContextCode = 'CMFT||ULYSS'
from
	[$(Warehouse)].[Incident].[Incident]







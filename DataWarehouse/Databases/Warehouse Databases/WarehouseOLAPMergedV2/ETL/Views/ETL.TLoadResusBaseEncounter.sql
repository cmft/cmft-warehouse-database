﻿








CREATE view [ETL].[TLoadResusBaseEncounter] as 

select 
	 EventID = Event.EventID 
	,SourcePatientNo = Event.PatientID
	,DistrictNo = cast(Patient.DistrictNumber as varchar(20))
	,NHSNumber = cast(Patient.NHSNumber as varchar(20))
	,DateOfBirth = cast(Patient.DOB as date)
	,AdmissionReasonID = Event.ReasonForAdmission 
	,AdmissionDate = cast(Event.DateOfAdmission as date)
	,PresentingECGID = Event.PresentingECG
	,Comments = cast(Event.Comments as varchar(max))
	,EmergencyBleepMeeting = Event.RequireEBM
	,CallID = Event.CallID
	,CallOutReasonID = Event.ReasonForCallout
	,CallOutTime = Call.CallInfo
	,LocationID = Call.DepartmentID
	,RespondingTeamID = Call.RespondingTeam
	,Event.OutcomeID
	,OutcomeTime = Event.OutcomeDateTime
	,Event.FinalOutcomeID
	,FinalOutcomeTime = Event.FinalOutcomeDateTime
	,AuditTypeID = 
			case
			when CallOutReason.ItemName = 'Cardiac Arrest' and Team.ItemName in ('Adult (Team 23) (Central)', 'Obstetric (Team 16) (Central)')
			then 1
			when CallOutReason.ItemName = 'Cardiac Arrest' and Team.ItemName = 'Paed (Team 15) (Central)'
			then 2
			when CallOutReason.ItemName = 'Cardiac Arrest' and Team.ItemName in ('Adult (Team XX) (TGH)', 'Pead (Team xx) (TGH)')
			then 3
			end
	,InterfaceCode = 'CARD'
	,ContextCode = 'CMFT||CARD'
from
	[$(CardiacArrest)].multisite.tblevent Event

inner join [$(CardiacArrest)].multisite.tblpatient Patient
on	Event.PatientID = Patient.PatientID

inner join [$(CardiacArrest)].multisite.tblcall Call
on	Event.CallID = Call.CallID

left join [$(CardiacArrest)].multisite.tblitem CallOutReason
on	Event.ReasonForCallout = CallOutReason.ItemID
and	CallOutReason.ItemTypeID = 6

left join [$(CardiacArrest)].multisite.tblitem Team
on	Call.RespondingTeam = Team.ItemID
and	Team.ItemTypeID = 2


--where
--	CasenoteNumber = 'M03/007606'










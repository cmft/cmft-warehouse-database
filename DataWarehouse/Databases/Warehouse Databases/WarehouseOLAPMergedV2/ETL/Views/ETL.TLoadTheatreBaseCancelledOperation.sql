﻿

create view [ETL].[TLoadTheatreBaseCancelledOperation]

as

select
	CancelledOperationRecno
	,SourceUniqueID
	,CasenoteNumber
	,DirectorateCode
	,NationalSpecialtyCode
	,ConsultantCode
	,AdmissionDate
	,ProcedureDate
	,WardCode = ContextCode + '||' + WardCode
	,Anaesthetist
	,Surgeon
	,ProcedureCode
	,CancellationDate
	,CancellationReason
	,TCIDate
	,Comments
	,ProcedureCompleted
	,BreachwatchComments
	,Removed
	,RemovedComments
	,ReportableBreach
	,ContextCode = 'CMFT||CANOP'
	,Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].Theatre.CancelledOperation

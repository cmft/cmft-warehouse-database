﻿


CREATE view [ETL].[TLoadTraAPCBaseOperation]
as

select
	 Encounter.MergeEncounterRecno
	,SequenceNo  = cast(Operation.SequenceNo as int) - 1
	,OperationCode =  rtrim(left(Operation.ProcedureCode , 6)) 
	,OperationDate = Operation.ProcedureDate
from
	[$(TraffordWarehouse)].dbo.APCProcedure Operation

inner join APC.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Operation.IPSourceUniqueID
and	Encounter.ContextCode = 'TRA||UG'
and Encounter.Reportable = 1

where
	--remove fces that exist in both Central and Trafford activity
	not exists
	(
	select
		1
	from
		APC.EncounterDuplicate

	inner join WH.Context
	on	Context.ContextCode = Encounter.ContextCode

	where
		EncounterDuplicate.DuplicateContextID = Context.ContextID
	and	EncounterDuplicate.DuplicateRecno = Encounter.EncounterRecno
	)






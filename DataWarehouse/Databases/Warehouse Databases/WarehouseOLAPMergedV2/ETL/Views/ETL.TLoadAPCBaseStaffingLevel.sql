﻿




CREATE view [ETL].[TLoadAPCBaseStaffingLevel]

as

select
	StaffingLevelRecno
	,DivisionCode
	,WardCode
	,CensusDate
	,ShiftID
	,RegisteredNursePlan
	,RegisteredNurseActual 
	,NonRegisteredNursePlan 
	,NonRegisteredNurseActual 
	,Comments
	,SeniorComments
	,InterfaceCode = 'STAFF'
	,ContextCode = 'CMFT||IPATH'
from
	[$(Warehouse)].APC.StaffingLevel




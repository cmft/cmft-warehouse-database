﻿
CREATE view [ETL].[TLoadCenAPCBaseOperation]
as

select
	 Encounter.MergeEncounterRecno
	,Operation.SequenceNo
	,OperationCode = Operation.OperationCode
	,Operation.OperationDate
from
	[$(Warehouse)].APC.Operation Operation

inner join APC.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Operation.APCSourceUniqueID
and	Encounter.ContextCode = 'CEN||PAS'
and Encounter.Reportable = 1

union all
	
select
	 Encounter.MergeEncounterRecno
	,SequenceNo = 0
	,OperationCode = Encounter.PrimaryProcedureCode
	,OperationDate = Encounter.PrimaryProcedureDate
from
	APC.BaseEncounter Encounter
where
	Encounter.PrimaryProcedureCode is not null
and	Encounter.ContextCode = 'CEN||PAS'
and Encounter.Reportable = 1



﻿


CREATE View [ETL].[TImportMortalityBaseReviewAdult]


as


Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,MortalityReviewAdded
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,MortalityReviewAdded
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode 
						,ClassificationGrade
						,RequiresFurtherInvestigation
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,MortalityReviewAdded
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode = CAST(PrimaryReviewerCode as varchar(50))
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade
		,RequiresFurtherInvestigation 
		,QuestionNo = 
			case 
				when ReviewStatusCode = 0 then 0
				else row_number() over (partition by SourceUniqueID order by SourceUniqueID)
			end
		,Question
		,Response
		,ContextCode = 'CMFT||MORT'
	from
	(
	select
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode = ContextCode
		,FormTypeCode 
		,MortalityReviewAdded
		,ReviewStatusCode
		,PrimaryReviewerAssignedTime
		,PrimaryReviewerCompletedTime
		,PrimaryReviewerCode
		,SecondaryReviewerAssignedTime
		,SecondaryReviewerCompletedTime
		,SecondaryReviewerCode
		,ClassificationGrade = cast(ClassificationScore as varchar(max))
		,RequiresFurtherInvestigation = 
				Case
					when DeathExpected = 0 then 1 
					when ClassificationScore in (2,3) then 1
					else 0
				end
		
		,DelayInDiagnosis = IsNull(cast(DelayInDiagnosis as varchar(max)),'')
		,DelayInDiagnosisDetail = cast(DelayInDiagnosisDetail as varchar(max))
		,DelayInDeliveringCare = cast(DelayInDeliveringCare as varchar(max))
		,DelayInDeliveringCareDetail = cast(DelayInDeliveringCareDetail as varchar(max))
		,PoorCommunication = cast(PoorCommunication as varchar(max))
		,PoorCommunicationDetail = cast(PoorCommunicationDetail as varchar(max))
		,OrganisationalFailure = cast(OrganisationalFailure as varchar(max))
		,OrganisationalFailureDetail = cast(OrganisationalFailureDetail as varchar(max))
		,AreasOfConcernNoDiff = cast(AreasOfConcernNoDiff as varchar(max))
		,AreasOfConcernNoDiffDetail = cast(AreasOfConcernNoDiffDetail as varchar(max))
		,AreasOfConcernContributed = cast(AreasOfConcernContributed as varchar(max))
		,AreasOfConcernContributedDetail = cast(AreasOfConcernContributedDetail as varchar(max))
		,DoneDifferently = cast(DoneDifferently as varchar(max))
		,DoneDifferentlyDetail = cast(DoneDifferentlyDetail as varchar(max))
		,DocStandard = cast(DocStandard as varchar(max))
		,FurtherComment = cast(FurtherComment as varchar(max))
		,PrimaryDiagnosis = cast(PrimaryDiagnosis as varchar(max))
		,ConfirmedPrimaryDiagnosis = cast(ConfirmedPrimaryDiagnosis as varchar(max))
		,DeathExpected = cast(DeathExpected as varchar(max))
		,RaisedEWS = cast(RaisedEWS as varchar(max))
		,EWSNotApplicable = cast(EWSNotApplicable as varchar(max))
		,PolicyMet = cast(PolicyMet as varchar(max))
		,EOLPlan = cast(EOLPlan as varchar(max))
		,CauseOfDeath1 = cast(CauseOfDeath1 as varchar(max))
		,CauseOfDeath2 = cast(CauseOfDeath2 as varchar(max))
		,CauseOfDeath3 = cast(CauseOfDeath3 as varchar(max))
		,CauseOfDeath4 = cast(CauseOfDeath4 as varchar(max))
		,PostMortem = cast(PostMortem as varchar(max))
		,CoronerInformed = cast(CoronerInformed as varchar(max))
		,MalignancyPresent = cast(MalignancyPresent as varchar(max))
		,AcuteMyocardialInfarction = cast(AcuteMyocardialInfarction as varchar(max))
		,CerebralVA = cast(CerebralVA as varchar(max))
		,CongestiveHeartFailure = cast(CongestiveHeartFailure as varchar(max))
		,ConnectiveTissue = cast(ConnectiveTissue as varchar(max))
		,Dementia = cast(Dementia as varchar(max))
		,Diabetes = cast(Diabetes as varchar(max))
		,LiverDisease = cast(LiverDisease as varchar(max))
		,PepticUlcer = cast(PepticUlcer as varchar(max))
		,PeripheralVascularDisease = cast(PeripheralVascularDisease as varchar(max))
		,PulmonaryDisease = cast(PulmonaryDisease as varchar(max))
		,Cancer = cast(Cancer as varchar(max))
		,DiabetesComplications = cast(DiabetesComplications as varchar(max))
		,Paraplegia = cast(Paraplegia as varchar(max))
		,RenalDisease = cast(RenalDisease as varchar(max))
		,MetastaticCancer = cast(MetastaticCancer as varchar(max))
		,SevereLiverDisease = cast(SevereLiverDisease as varchar(max))
		,HIV = cast(HIV as varchar(max))
		,ClinicalReviewTime = cast(ClinicalReviewTime as varchar(max))
		,ConsultantReviewTime = cast(ConsultantReviewTime as varchar(max))
		,SeenWithinTwelveHours = cast(SeenWithinTwelveHours as varchar(max))
		,AppropriateWard = cast(AppropriateWard as varchar(max))
		,AppropriateWardDetail = cast(AppropriateWardDetail as varchar(max))
		,HowManyWards = cast(HowManyWards as varchar(max))
		,HowManyWardsDetail = cast(HowManyWardsDetail as varchar(max))
		,WriteInNotes = cast(WriteInNotes as varchar(max))
		,WriteInNotesDetail = cast(WriteInNotesDetail as varchar(max))
		,NotReviewed48Hour = cast(NotReviewed48Hour as varchar(max))
		,NotReviewed48HourDetail = cast(NotReviewed48HourDetail as varchar(max))
		,FluidBalance = cast(FluidBalance as varchar(max))
		,FluidBalanceDetail = cast(FluidBalanceDetail as varchar(max))
		,Surgery = cast(Surgery as varchar(max))
		,SurgeryDetail = cast(SurgeryDetail as varchar(max))
		,Sepsis = cast(Sepsis as varchar(max))
		,SepsisDetail = cast(SepsisDetail as varchar(max))
		,SepsisRecognised = cast(SepsisRecognised as varchar(max))
		,SepsisRecognisedDetail = cast(SepsisRecognisedDetail as varchar(max))
		,SepsisTreated = cast(SepsisTreated as varchar(max))
		,SepsisTreatedDetail = cast(SepsisTreatedDetail as varchar(max))
		,DNACPR = cast(DNACPR as varchar(max))
		,DNACPRDetail = cast(DNACPRDetail as varchar(max))
		,DNACPRDiscussion = cast(DNACPRDiscussion as varchar(max))
		,DNACPRDiscussionDetail = cast(DNACPRDiscussionDetail as varchar(max))
		,ManagementPlan = cast(ManagementPlan as varchar(max))
		,ManagementPlanDetail = cast(ManagementPlanDetail as varchar(max))
		,Investigations = cast(Investigations as varchar(max))
		,InvestigationsDetail = cast(InvestigationsDetail as varchar(max))
		,ManagementSteps = cast(ManagementSteps as varchar(max))
		,ManagementStepsDetail = cast(ManagementStepsDetail as varchar(max))
		,Omissions = cast(Omissions as varchar(max))
		,OmissionsDetail = cast(OmissionsDetail as varchar(max))
		,CriticalCareReview = cast(CriticalCareReview as varchar(max))
		,CriticalCareReviewDetail = cast(CriticalCareReviewDetail as varchar(max))
		,CriticalAppropriateAdmitted = cast(CriticalAppropriateAdmitted as varchar(max))
		,CriticalAppropriateAdmittedDetail = cast(CriticalAppropriateAdmittedDetail as varchar(max))
		,CriticalLessThan4Hours = cast(CriticalLessThan4Hours as varchar(max))
		,CriticalLessThan4HoursDetail = cast(CriticalLessThan4HoursDetail as varchar(max))
		,CriticalAppropriateNotAdmitted = cast(CriticalAppropriateNotAdmitted as varchar(max))
		,CriticalAppropriateNotAdmittedDetail = cast(CriticalAppropriateNotAdmittedDetail as varchar(max))
		,MedicationAnyErrors = cast(MedicationAnyErrors as varchar(max))
		,MedicationAnyErrorsDetail = cast(MedicationAnyErrorsDetail as varchar(max))
	from 
		[$(Warehouse)].Mortality.Adult
	) 
	Mortality

	unpivot
			(
			Response for Question in 
					(
					DelayInDiagnosis
					,DelayInDiagnosisDetail
					,DelayInDeliveringCare
					,DelayInDeliveringCareDetail
					,PoorCommunication
					,PoorCommunicationDetail
					,OrganisationalFailure
					,OrganisationalFailureDetail
					,AreasOfConcernNoDiff
					,AreasOfConcernNoDiffDetail
					,AreasOfConcernContributed
					,AreasOfConcernContributedDetail
					,DoneDifferently
					,DoneDifferentlyDetail
					,DocStandard
					,FurtherComment
					,PrimaryDiagnosis
					,ConfirmedPrimaryDiagnosis
					,DeathExpected
					,RaisedEWS
					,EWSNotApplicable
					,PolicyMet
					,EOLPlan
					,CauseOfDeath1
					,CauseOfDeath2
					,CauseOfDeath3
					,CauseOfDeath4
					,PostMortem
					,CoronerInformed
					,MalignancyPresent
					,AcuteMyocardialInfarction
					,CerebralVA
					,CongestiveHeartFailure
					,ConnectiveTissue
					,Dementia
					,Diabetes
					,LiverDisease
					,PepticUlcer
					,PeripheralVascularDisease
					,PulmonaryDisease
					,Cancer
					,DiabetesComplications
					,Paraplegia
					,RenalDisease
					,MetastaticCancer
					,SevereLiverDisease
					,HIV
					,ClinicalReviewTime
					,ConsultantReviewTime
					,SeenWithinTwelveHours
					,AppropriateWard
					,AppropriateWardDetail
					,HowManyWards
					,HowManyWardsDetail
					,WriteInNotes
					,WriteInNotesDetail
					,NotReviewed48Hour
					,NotReviewed48HourDetail
					,FluidBalance
					,FluidBalanceDetail
					,Surgery
					,SurgeryDetail
					,Sepsis
					,SepsisDetail
					,SepsisRecognised
					,SepsisRecognisedDetail
					,SepsisTreated
					,SepsisTreatedDetail
					,DNACPR
					,DNACPRDetail
					,DNACPRDiscussion
					,DNACPRDiscussionDetail
					,ManagementPlan
					,ManagementPlanDetail
					,Investigations
					,InvestigationsDetail
					,ManagementSteps
					,ManagementStepsDetail
					,Omissions
					,OmissionsDetail
					,CriticalCareReview
					,CriticalCareReviewDetail
					,CriticalAppropriateAdmitted
					,CriticalAppropriateAdmittedDetail
					,CriticalLessThan4Hours
					,CriticalLessThan4HoursDetail
					,CriticalAppropriateNotAdmitted
					,CriticalAppropriateNotAdmittedDetail
					,MedicationAnyErrors
					,MedicationAnyErrorsDetail
					)
			) 
			Response 
		)
		Adult




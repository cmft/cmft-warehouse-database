﻿


--Use WarehouseOLAPMergedV2

CREATE View [ETL].[TLoadBloodManagementBaseTransaction]

as

select 
	TransactionRecno
	,TransactionID
	,TransactionTypeID
	,TransactionTime 
	,ConditionID
	,BloodUniqueID 
	,DonorNumber 
	,BloodProductID
	,BloodProductVolume 
	,BloodGroupID
	,ReservationTime
	,ExpiryTime
	,FateID 
	,SiteID 
	,LocationID
	,PatientID
	,HospitalNumber
	,NHSNumber 
	,LastName
	,FirstName
	,DateOfBirth 
	,GenderCode 
	,SystemUserID
	,InterfaceCode 
	,ContextCode = 'CMFT||BLDTRK' -- There is data coming through with Site = Trafford Theatre
	,Created
	,Updated
	,ByWhom
from 
	[$(Warehouse)].BloodManagement.[Transaction]

	



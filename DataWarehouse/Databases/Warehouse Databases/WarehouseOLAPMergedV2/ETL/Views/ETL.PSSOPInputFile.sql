﻿



CREATE view [ETL].[PSSOPInputFile]

as

select --top 100
	PROCODET = 'RW3' --Encounter.ProviderCode
	,STARTAGE = [$(CMFT)].Dates.GetAge(
								Encounter.DateOfBirth
								,Encounter.AppointmentDate
								)

	,SEX = Sex.NationalSexCode		
	,ATTENDED = AttendanceStatus.NationalAttendanceStatusCode
	,REFSOURC = SourceOfReferral.NationalSourceOfReferralCode
	,MAINSPEF = Consultant.MainSpecialtyCode
	,TRETSPEF = TreatmentFunction.NationalSpecialtyCode
	,OPER_01 = 
		left(
			replace(PrimaryOperation.OperationCode, '.', '')
			,5
			)
	,OPER_02 = 
		left(
			replace(SecondaryOperation1.OperationCode, '.', '')
			,5
			)
	,OPER_03 = 
		left(
			replace(SecondaryOperation2.OperationCode, '.', '')
			,5
			)
	,OPER_04 = 
		left(
			replace(SecondaryOperation3.OperationCode, '.', '')
			,5
			)
	,OPER_05 = 
		left(
			replace(SecondaryOperation4.OperationCode, '.', '')
			,5
			)
	,OPER_06 = 
		left(
			replace(SecondaryOperation5.OperationCode, '.', '')
			,5
			)
	,OPER_07 = 
		left(
			replace(SecondaryOperation6.OperationCode, '.', '')
			,5
			)
	,OPER_08 = 
		left(
			replace(SecondaryOperation7.OperationCode, '.', '')
			,5
			)	
	,OPER_09 = 
		left(
			replace(SecondaryOperation8.OperationCode, '.', '')
			,5
			)
	,OPER_10 = 
		left(
			replace(SecondaryOperation9.OperationCode, '.', '')
			,5
			)
	,OPER_11 = 
		left(
			replace(SecondaryOperation10.OperationCode, '.', '')
			,5
			)
	,OPER_12 = 
		left(
			replace(SecondaryOperation11.OperationCode, '.', '')
			,5
			)
	--,OPER_13 = 
	--	left(
	--		replace(SecondaryOperation12.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_14 = 
	--	left(
	--		replace(SecondaryOperation13.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_15 = 
	--	left(
	--		replace(SecondaryOperation14.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_16 = 
	--	left(
	--		replace(SecondaryOperation15.OperationCode, '.', '')
	--		,5
	--		)
	--,OPER_17 = 
	--	left(
	--		replace(SecondaryOperation16.OperationCode, '.', '')
	--		,5
	--		)
	,PURCODE = PurchaserCode -- Need to point to demographics tables when done
	,Encounter.MergeEncounterRecno
	,FinancialYear = 
				case
				when Appointment.FinancialYear = '2015/2016' -- using 14/15 grouper for 15/16 data - confirmed by Phil Huitson 7 Apr 2015
				then '2014/2015'
				else Appointment.FinancialYear
				end
from
	OP.BaseEncounter Encounter

inner join
	OP.BaseEncounterReference EncounterReference
on	Encounter.MergeEncounterRecno = EncounterReference.MergeEncounterRecno 

inner join OP.ProcessList
on	ProcessList.MergeRecno = Encounter.MergeEncounterRecno

left outer join
	WH.Specialty TreatmentFunction
on	TreatmentFunction.SourceSpecialtyID = EncounterReference.ReferralSpecialtyID
--Requested by Phil H 03/07/2013 actioned by GC
--EncounterReference.SpecialtyID

left outer join
	WH.Consultant Consultant
on	Consultant.SourceConsultantID = EncounterReference.ConsultantID

left outer join
	WH.Calendar Appointment
on	Appointment.DateID = EncounterReference.AppointmentDateID

left outer join
	WH.Sex
on	Sex.SourceSexID = EncounterReference.SexID

left outer join -- should be inner join performance reasons
	OP.AttendanceStatus AttendanceStatus
on	AttendanceStatus.SourceAttendanceStatusID = EncounterReference.AttendanceStatusID

left outer join
	OP.SourceOfReferral SourceOfReferral
on	SourceOfReferral.SourceSourceOfReferralID = EncounterReference.SourceOfReferralID 

left outer join
	OP.BaseOperation PrimaryOperation
on	PrimaryOperation.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	PrimaryOperation.SequenceNo = 0

left outer join
	OP.BaseOperation SecondaryOperation1
on	SecondaryOperation1.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation1.SequenceNo = 1

left outer join
	OP.BaseOperation SecondaryOperation2
on	SecondaryOperation2.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation2.SequenceNo = 2

left outer join
	OP.BaseOperation SecondaryOperation3
on	SecondaryOperation3.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation3.SequenceNo = 3

left outer join
	OP.BaseOperation SecondaryOperation4
on	SecondaryOperation4.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation4.SequenceNo = 4

left outer join
	OP.BaseOperation SecondaryOperation5
on	SecondaryOperation5.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation5.SequenceNo = 5

left outer join
	OP.BaseOperation SecondaryOperation6
on	SecondaryOperation6.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation6.SequenceNo = 6

left outer join
	OP.BaseOperation SecondaryOperation7
on	SecondaryOperation7.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation7.SequenceNo = 7

left outer join
	OP.BaseOperation SecondaryOperation8
on	SecondaryOperation8.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation8.SequenceNo = 8

left outer join
	OP.BaseOperation SecondaryOperation9
on	SecondaryOperation9.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation9.SequenceNo = 9

left outer join
	OP.BaseOperation SecondaryOperation10
on	SecondaryOperation10.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation10.SequenceNo = 10

left outer join
	OP.BaseOperation SecondaryOperation11
on	SecondaryOperation11.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation11.SequenceNo = 11

left outer join
	OP.BaseOperation SecondaryOperation12
on	SecondaryOperation12.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation12.SequenceNo = 12

left outer join
	OP.BaseOperation SecondaryOperation13
on	SecondaryOperation13.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation13.SequenceNo = 13

left outer join
	OP.BaseOperation SecondaryOperation14
on	SecondaryOperation14.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation14.SequenceNo = 14

left outer join
	OP.BaseOperation SecondaryOperation15
on	SecondaryOperation15.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation15.SequenceNo = 15

left outer join
	OP.BaseOperation SecondaryOperation16
on	SecondaryOperation16.MergeEncounterRecno = Encounter.MergeEncounterRecno
and	SecondaryOperation16.SequenceNo = 16

--where 
--	(
--		Encounter.AppointmentDate >= cast(dateadd(month, -6, getdate()) as date)	
--	and	Encounter.AppointmentDate <= getdate()
--	)
--or
--	(
--		Encounter.Updated >= cast(dateadd(month, -3, getdate()) as date)
--	and
--		Encounter.AppointmentDate >= '1 apr 2013' -- we don't have groupers plugged in for previous financial years
--	and
--		Encounter.AppointmentDate <= getdate()
--	)

union 

select --top 100
	PROCODET = null
	,STARTAGE = null
	,SEX = null
	,ATTENDED = null
	,REFSOURC = null
	,MAINSPEF = null
	,TRETSPEF = null
	,OPER_01 = null
	,OPER_02 = null
	,OPER_03 = null
	,OPER_04 = null
	,OPER_05 = null
	,OPER_06 = null
	,OPER_07 = null
	,OPER_08 = null	
	,OPER_09 = null
	,OPER_10 = null
	,OPER_11 = null
	,OPER_12 = null
	,PURCODE = null -- Need to point to demographics tables when done
	,MergeEncounterRecno  = -20132014 -- dummy record
	,FinancialYear = '2013/2014'





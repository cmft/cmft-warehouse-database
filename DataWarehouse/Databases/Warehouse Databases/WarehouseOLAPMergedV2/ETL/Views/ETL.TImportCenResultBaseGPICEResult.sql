﻿















CREATE view [ETL].[TImportCenResultBaseGPICEResult] as

select
	ProcessList.ResultRecno
	,ProcessList.Action
	,ContextCode = 'CEN||GPICE'
	,SourceUniqueID
	,PatientID
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,DateOfBirth
	,SexCode
	,ClinicianID
	,MainSpecialtyCode
	,DisciplineCode = SpecialtyCode
	,SpecialtyCode = MainSpecialtyCode -- ICE does not record the requesting specialty so take the main specialty of the clinician linked to the report
	,LocationID
	,ReportSourceUniqueID
	,ReportStatusCode
	,ReportTime
	,ReportComment
	,SampleReferenceCode
	,SampleTypeCode
	,SampleType
	,SampleCollectionTime
	,InvestigationCode
	,InvestigationName
	,InvestigationComment
	,ResultCode
	,ResultName
	,Result
	,UnitOfMeasurement
	,Abnormal
	,LowerReferenceRangeValue
	,UpperReferenceRangeValue
	,ResultComment
	,InterfaceCode

from
	[$(Warehouse)].Result.GPICEProcessList ProcessList

left join [$(Warehouse)].Result.GPICEResult Import
on	Import.ResultRecno = ProcessList.ResultRecno















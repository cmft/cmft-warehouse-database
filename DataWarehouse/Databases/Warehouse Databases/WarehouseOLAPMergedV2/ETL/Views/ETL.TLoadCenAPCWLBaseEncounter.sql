﻿










CREATE view [ETL].[TLoadCenAPCWLBaseEncounter] as

/******************************************************************************************************
View		: [ETL].[TLoadCenAPCWLBaseEncounter]
Description	: As view name

Modification History
====================

Date		Person			Description
=======================================================================================================
?			?				Initial coding.
17/10/2014	Paul Egan       Added TCITime for day case appointment reminders.
*******************************************************************************************************/

SELECT
	 EncounterRecno
	,InterfaceCode
	,SourcePatientNo
	,SourceEncounterNo
	,CensusDate
	,PatientSurname
	,PatientForename
	,PatientTitle
	,SexCode

	--,SexCode =
	--	case Encounter.SexCode
	--	when '' then '0'
	--	when 'M' then '1'
	--	when 'F' then '2'
	--	when 'I' then '9'
	--	else '#'
	--	end

	,DateOfBirth
	,DateOfDeath
	,NHSNumber
	,DistrictNo
	,MaritalStatusCode
	,ReligionCode
	,Postcode
	,PatientsAddress1
	,PatientsAddress2
	,PatientsAddress3
	,PatientsAddress4
	,DHACode
	,HomePhone
	,WorkPhone
	,EthnicOriginCode
	,coalesce(Encounter.ConsultantCode, '-1') ConsultantCode
	,coalesce(Encounter.SpecialtyCode, '-1') SpecialtyCode
	,Encounter.PASSpecialtyCode
	,coalesce(Encounter.EpisodeSpecialtyCode, '-1') EpisodeSpecialtyCode
	,ManagementIntentionCode
	,AdmissionMethodCode
	,PriorityCode
	,WaitingListCode
	,CommentClinical
	,CommentNonClinical
	,IntendedPrimaryOperationCode
	,Operation
	,SiteCode
	,WardCode
	,WLStatus
	,ProviderCode
	,PurchaserCode
	,ContractSerialNumber
	,CancelledBy
	,BookingTypeCode
	,CasenoteNumber
	,OriginalDateOnWaitingList
	,DateOnWaitingList

	,TCIDate = 
		case
		when TCIDate is null
		then '1 Jan 1900'
		when TCIDate < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900')
		then '1 Jan 1900'
		when TCIDate < CensusDate
		then '1 Jan 1900'
		else cast(TCIDate as date)
		end

	,KornerWait
	,CountOfDaysSuspended
	,SuspensionStartDate
	,SuspensionEndDate
	,SuspensionReasonCode
	,SuspensionReason
	,EpisodicGpCode
	,coalesce(Encounter.RegisteredPracticeCode, Encounter.EpisodicGpPracticeCode, 'X99999') RegisteredGpPracticeCode
	,coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredPracticeCode, 'X99999') EpisodicGpPracticeCode
	,RegisteredGpCode
	,RegisteredPracticeCode
	,SourceTreatmentFunctionCode
	,Encounter.TreatmentFunctionCode
	,NationalSpecialtyCode
	,PCTCode
	,BreachDate
	,ExpectedAdmissionDate
	,ReferralDate
	,ExpectedLOS
	,ProcedureDate
	,FutureCancellationDate
	,TheatreKey
	,TheatreInterfaceCode
	,EpisodeNo

	,BreachDays
	,MRSA
	,RTTPathwayID
	,RTTPathwayCondition
	,RTTStartDate
	,RTTEndDate
	,RTTSpecialtyCode
	,RTTCurrentProviderCode
	,RTTCurrentStatusCode
	,RTTCurrentStatusDate
	,RTTCurrentPrivatePatientFlag
	,RTTOverseasStatusFlag
	,NationalBreachDate
	,NationalBreachDays
	,DerivedBreachDays
	,DerivedClockStartDate
	,DerivedBreachDate
	,RTTBreachDate
	,RTTDiagnosticBreachDate
	,NationalDiagnosticBreachDate
	,SocialSuspensionDays

	,case
	when datediff(day, DateOfBirth, Encounter.CensusDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.CensusDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.CensusDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 1 and
	datediff(day, DateOfBirth, Encounter.CensusDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.CensusDate)), 2) + ' Days'
	
	when datediff(day, DateOfBirth, Encounter.CensusDate) > 28 and
	datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.CensusDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.CensusDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.CensusDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.CensusDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.CensusDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.CensusDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end AgeCode

	,1 Cases

	,convert(int, Encounter.KornerWait) LengthOfWait

	,DurationCode =
		coalesce(
			right('00' +
			convert(varchar, 
			case
			when convert(int, Encounter.KornerWait /7 ) > 207 then 208
			else convert(int, Encounter.KornerWait /7 )
			end), 3)
			,'NA'
		)

	--,WaitTypeCode =
	--case
	--when rtrim(Encounter.AdmissionMethodCode) = 'BA' then '20'
	--when rtrim(Encounter.AdmissionMethodCode) = 'BL' then '20'
	--when rtrim(Encounter.AdmissionMethodCode) = 'BP' then '20'
	--when rtrim(Encounter.AdmissionMethodCode) = 'PA' then '30'
	--when rtrim(Encounter.AdmissionMethodCode) = 'PL' then '30'
	--else '10'
	--end

	,WaitTypeCode =
	case
	when rtrim(Encounter.AdmissionMethodCode) = 'PA' then '20'
	when rtrim(Encounter.AdmissionMethodCode) = 'PL' then '20'
	when Encounter.WLStatus = 'WL Suspend' then '30'
	when Encounter.AdminCategoryCode = 'PAY' then '40'
	else '10'
	end

	,StatusCode =
	case
	when Encounter.WLStatus = 'WL Suspend' then '20'
	else '10'
	end

	,CategoryCode =
	case
	when rtrim(Encounter.ManagementIntentionCode) = 'I' then '10'
	else '20'
	end

	,AddedToWaitingListTime

	,AddedLastWeek =
	case
	when
		datediff(
			 day
			,dateadd(day, datediff(day, 0, Encounter.AddedToWaitingListTime), 0)
			,Encounter.CensusDate
		) < 8 then 1
	else 0
	end

	,WaitingListAddMinutes =
	case
	when
		datediff(
			 day
			,dateadd(day, datediff(day, 0, Encounter.AddedToWaitingListTime), 0)
			,Encounter.CensusDate
		) < 8
	then
		coalesce(
			 WaitingListAddMinutes.WaitingListAddMinutes
			,(
			select
				NumericValue
			from
				[$(Warehouse)].dbo.Parameter
			where
				Parameter = 'DEFAULTWLADDMINUTES'
			)
			,10
		)
	else null
	end

	,OPCSCoded =
	case
	when Encounter.IntendedPrimaryOperationCode is null
	then 0
	else 1
	end

	,WithRTTStartDate =
	case
	when Encounter.RTTStartDate is null
	then 0
	else 1
	end

	,WithRTTStatusCode =
	case
	when Encounter.RTTCurrentStatusCode in ('10', '20') then 1
	else 0
	end

	,WithExpectedAdmissionDate =
	case
	when Encounter.ExpectedAdmissionDate is null
	then 0
	else 1
	end



	,SourceUniqueID
	,AdminCategoryCode

	,RTTActivity =
		convert(
			bit
			,0
			--,case
			--when
			--	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
			--and	Encounter.SpecialtyCode not in 
			--	(
			--	select
			--		SpecialtyCode
			--	from
			--		RTT.dbo.ExcludedSpecialty
			--	)

			---- 'GENE' ?
			--and	Encounter.ConsultantCode <> 'FRAC'

			----remove ICAT
			--and	Encounter.SiteCode != 'FMSK'

			--and	not exists
			--	(
			--	select
			--		1
			--	from
			--		RTT.dbo.RTTClockStart ClockChangeReason
			--	where
			--		ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
			--	and	ClockChangeReason.SourceEntityRecno = Encounter.SourceEncounterNo
			--	and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
			--	)
			--then 1
			--else 0
			--end
		)


	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when Encounter.RTTBreachDate < Encounter.TCIDate
		then 'B'
		else 'N'
		end


	,DiagnosticProcedure =
		convert(
			bit
			,case
			when DiagnosticProcedure.ProcedureCode is null
			then 0
			else 1
			end
		)
	,DirectorateCode = coalesce(Encounter.DirectorateCode , 'N/A')
	,GuaranteedAdmissionDate

	,DurationAtTCIDateCode =
		case
		when Encounter.TCIDate is null then 'NA'
		when Encounter.TCIDate < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900') then 'NA'
		when Encounter.TCIDate < CensusDate then 'NA'
		else right('00' +
				convert(varchar, 
					case
					when convert(int, (Encounter.KornerWait + datediff(day , CensusDate , TCIDate)) /7 ) > 207 then 208
					else convert(int, (Encounter.KornerWait + datediff(day , CensusDate , TCIDate)) /7 )
					end
				)
				,3
			)
		end

	,WithTCIDate =
		case
		when TCIDate is null
		then 0
		when TCIDate < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900')
		then 0
		when TCIDate < CensusDate
		then 0
		else 1
		end

	,WithGuaranteedAdmissionDate =
		case
		when Encounter.GuaranteedAdmissionDate is null
		then 0
		else 1
		end
	,WithRTTOpenPathway
	,RTTPathwayStartDateCurrent
	,RTTWeekBandReturnCode 
	,RTTDaysWaiting 
	,AdmissionReason
	,ContextCode
	,IsShortNotice
	,RTTComment1
	,RTTComment2
	,CCGCode
	,ResidenceCCGCode
	
	,TCITime =					-- Added 17/10/2014 Paul Egan
		case
		when TCIDate is null
		then '1 Jan 1900'
		when TCIDate < (select min(TheDate) from WH.CalendarBase where TheDate <> '1 Jan 1900')
		then '1 Jan 1900'
		when TCIDate < CensusDate
		then '1 Jan 1900'
		else TCITime
		end
	,FutureCancellationTime		-- Added 16/03/2015 Paul Egan

FROM
	(
	select
		 EncounterRecno
		,SourcePatientNo
		,SourceEncounterNo
		,CensusDate
		,PatientSurname
		,PatientForename
		,PatientTitle
		,SexCode
		,DateOfBirth
		,DateOfDeath
		,NHSNumber
		,DistrictNo
		,MaritalStatusCode
		,ReligionCode
		,Encounter.Postcode
		,PatientsAddress1
		,PatientsAddress2
		,PatientsAddress3
		,PatientsAddress4
		,DHACode
		,HomePhone
		,WorkPhone
		,EthnicOriginCode
		,ConsultantCode
		,SpecialtyCode
		,PASSpecialtyCode
		,EpisodeSpecialtyCode
		,ManagementIntentionCode
		,AdmissionMethodCode
		,PriorityCode
		,WaitingListCode
		,CommentClinical
		,CommentNonClinical
		,IntendedPrimaryOperationCode
		,Operation
		,SiteCode
		,WardCode
		,WLStatus
		,ProviderCode
		,PurchaserCode
		,ContractSerialNumber
		,CancelledBy
		,BookingTypeCode
		,CasenoteNumber
		,OriginalDateOnWaitingList
		,DateOnWaitingList
		,TCIDate
		,KornerWait
		,CountOfDaysSuspended
		,SuspensionStartDate
		,SuspensionEndDate
		,SuspensionReasonCode
		,SuspensionReason
		,InterfaceCode
		,EpisodicGpCode = 
			EpisodicGP.NationalCode
		,EpisodicGpPracticeCode 
		,RegisteredGpCode = 
			RegisteredGP.NationalCode
		,RegisteredPracticeCode
		,SourceTreatmentFunctionCode
		,TreatmentFunctionCode
		,NationalSpecialtyCode
		,PCTCode
		,BreachDate
		,ExpectedAdmissionDate
		,ReferralDate
		,FuturePatientCancelDate
		,AdditionFlag
		,LocalRegisteredGpCode
		,LocalEpisodicGpCode
		,NextOfKinName
		,NextOfKinRelationship
		,NextOfKinHomePhone
		,NextOfKinWorkPhone
		,ExpectedLOS
		,TheatreKey
		,TheatreInterfaceCode
		,ProcedureDate
		,FutureCancellationDate
		,ProcedureTime
		,TheatreCode
		,AdmissionReason
		,EpisodeNo
		,BreachDays
		,MRSA
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,NationalBreachDate
		,NationalBreachDays
		,DerivedBreachDays
		,DerivedClockStartDate
		,DerivedBreachDate
		,RTTBreachDate
		,RTTDiagnosticBreachDate
		,NationalDiagnosticBreachDate
		,SocialSuspensionDays
		,BreachTypeCode
		,AddedToWaitingListTime
		,SourceUniqueID
		,AdminCategoryCode
		,ReferralEncounterRecno
		,PatientDeathIndicator
		,CurrentAdmissionDays
		,LastReviewDate
		,KornerCharterWaitMonths
		,KornerWaitMonths
		,DiagnosticGroupCode
		,IntendedProcedureDate
		,IsShortNotice
		,RTTComment1
		,RTTComment2
		,CEAEpisode
		,DirectorateCode
		,GuaranteedAdmissionDate
		,WithRTTOpenPathway = 
			coalesce (
					WithRTTOpenPathway
					,0
			)
		,RTTPathwayStartDateCurrent
		,RTTWeekBandReturnCode 
		,RTTDaysWaiting 
		,ContextCode = 'CEN||PAS'

		,CCGCode =
				coalesce(
					 CCGPractice.ParentOrganisationCode
					,CCGPostcode.CCGCode
					,'00W'
				)

		,ResidenceCCGCode = CCGPostcode.CCGCode
		,TCITime					-- Added 17/10/2014 Paul Egan
		,FutureCancellationTime		-- Added 16/03/2015 Paul Egan
	from
		[$(Warehouse)].APC.WaitingList Encounter

	LEFT OUTER JOIN [$(Warehouse)].PAS.Gp RegisteredGP
		ON	RegisteredGP.GpCode = Encounter.RegisteredGpCode
		
	LEFT OUTER JOIN [$(Warehouse)].PAS.Gp EpisodicGP
		ON	EpisodicGP.GpCode = Encounter.EpisodicGpCode

	left join [$(Organisation)].dbo.CCGPractice
	on	CCGPractice.OrganisationCode =
			coalesce(
				 Encounter.EpisodicGpPracticeCode
				,Encounter.RegisteredPracticeCode
			)

	left join [$(Organisation)].dbo.CCGPostcode
	on	CCGPostcode.Postcode = 
			case
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 6 then left(Encounter.Postcode, 2) + '   ' + right(Encounter.Postcode, 3)
			when datalength(rtrim(ltrim(Encounter.Postcode))) = 7 then left(Encounter.Postcode, 3) + '  ' + right(Encounter.Postcode, 3)
			else Encounter.Postcode
			end

	where 
		Encounter.CensusDate IN
		(
		select 
			Calendar.TheDate
		from
			WH.Calendar
		where
			Calendar.TheDate in
				(
				select
					max(TheDate)
				from
					WH.Calendar
				where
					TheDate between DATEADD(year, -2, getdate()) and getdate()
				group by
					 year(TheDate)
					,month(TheDate)

				union
--snapshots in the last fourteen days
				select
					TheDate
				from
					WH.Calendar
				where
					TheDate between dateadd(day , -14 , getdate()) and getdate()

				union
--Sunday snapshots
				select
					TheDate
				from
					WH.Calendar
				where
					datename(dw, TheDate) = 'Sunday'
				and	TheDate between DATEADD(year, -2, getdate()) and getdate()
				)

--remove this to allow updating of snapshots
		and	not exists
			(
			select
				1
			from
				APCWL.BaseEncounter
			where
				BaseEncounter.CensusDate = Calendar.TheDate
			and BaseEncounter.ContextCode =  'CEN||PAS'
			)
		)
	) Encounter
	
left join [$(Warehouse)].dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = Encounter.IntendedPrimaryOperationCode

left join 
	(
	select
		 SpecialtyCode = EntityCode
		,WaitingListAddMinutes = convert(int, Description)
	from
		[$(Warehouse)].dbo.EntityLookup
	where
		EntityTypeCode = 'WLADDMINUTES'
	) WaitingListAddMinutes
on	WaitingListAddMinutes.SpecialtyCode = Encounter.SpecialtyCode














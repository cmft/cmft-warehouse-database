﻿create view ETL.TLoadCenSCRBaseCarePlan as

SELECT [UniqueRecordId]
      ,[ReferralUniqueRecordId]
      ,[SystemId]
      ,[WasPatientDiscussedAtMDT]
      ,[MDTDate]
      ,[CarePlanAgreedDate]
      ,[DecisionOrganisationCode]
      ,[Recurrence]
      ,[CancerCarePlanIntentCode]
      ,[FirstPlannedTreatmentTypeCode]
      ,[SecondPlannedTreatmentTypeCode]
      ,[ThirdPlannedTreatmentTypeCode]
      ,[FourthPlannedTreatmentTypeCode]
      ,[NoTreatmentReasonCode]
      ,[CoMorbidityIndexCode]
      ,[PerformanceStatusCode]
      ,[PrimaryCareCommunicationDate]
      ,[CarePlanAgreed]
      ,[PlanTypeCode]
      ,[NotFirstChoiceOfTreatmentReasonCode]
      ,[LocationCode]
      ,[PrePostTreatment]
      ,[OncologistPresent]
      ,[MDTActionedBy]
      ,[ReviewedByPathologistPriorToMDT]
      ,[ReviewedBy]
      ,[ReferredTo]
      ,[WhoReferredToCode]
      ,[CopyLetterTo]
      ,[TrustReferredToCode]
      ,[ReasonForReferalCode]
      ,[WasPatientDiscussedAtNetworkMDT]
      ,[NetworkMeetingDate]
      ,[NetworkActionedBy]
      ,[PlannedTransplantTypeCode]
      ,[RecurrenceTypeCode]
      ,[RecurrenceDiagnosedBy]
      ,[ReasonNotCurative]
      ,[Seizures]
      ,[Surgery]
      ,[CarePlanAgreedAtMDT]
      ,[MDTComments]
      ,[NetworkDecisionCode]
      ,[NetworkComments]
      ,[ReasonNotDiscussedCode]
  FROM [$(Warehouse)].[SCR].[CarePlan]

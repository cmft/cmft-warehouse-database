﻿
CREATE view [ETL].[TLoadCenDictationInpatient]

as
 
select
	InpatientRecno = MedisecInpatientRecno
	,SourcePatientNo = MedisecInpatient.SourcePatientNo
	,AdmissionTime = MedisecInpatient.AdmissionTime
	,SpellSequenceNo = MedisecInpatient.SpellSequenceNo
	,RequestTypeCode
						 
	,InterfaceCode = MedisecInpatient.InterfaceCode
	,ContextCode  = MedisecInpatient.ContextCode 
from
	[$(Warehouse)].Dictation.MedisecInpatient

/*
Duplicate (checked and this occurs in Live as well
SourcePatientNo	AdmissionTime
3347967	2013-01-16 15:57:00
*/











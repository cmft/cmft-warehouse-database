﻿

-- use WarehouseOLAPMergedV2
-- drop View ETL.TLoadCENAPCBaseDementia
-- select * from ETL.TLoadCENAPCBaseDementia

CREATE View [ETL].[TImportCenAPCBaseDementia]

as

select 
	 Dementia.DementiaRecno
	,GlobalProviderSpellNo = 
			coalesce(BaseEncounter.GlobalProviderSpellNo
					,Dementia.ProviderSpellNo
					)
	,Dementia.ProviderSpellNo
	,Dementia.SpellID 
	,Dementia.SourceUniqueID 
	,Dementia.SourcePatientNo 
	,Dementia.SourceSpellNo 
	,Dementia.AdmissionWardCode 
	,Dementia.RecordedDate 
	,Dementia.RecordedTime
	,Dementia.KnownToHaveDementia
	,Dementia.ForgetfulnessQuestionAnswered	
	,Dementia.AssessmentScore	
	,Dementia.Investigation
	,Dementia.AssessmentNotPerformedReason 
	,Dementia.ReferralSentDate
	,Dementia.InterfaceCode 		
	,ContextCode = 'CEN||BEDMAN' 	
	,Dementia.Created
	,Dementia.Updated
	,Dementia.ByWhom	
from 
	[$(Warehouse)].APC.Dementia

left join APC.BaseEncounter
on Dementia.ProviderSpellNo = BaseEncounter.ProviderSpellNo
and FirstEpisodeInSpellIndicator = 1
	







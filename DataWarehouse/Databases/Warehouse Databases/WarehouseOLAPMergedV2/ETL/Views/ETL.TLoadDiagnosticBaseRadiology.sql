﻿


CREATE view [ETL].[TLoadDiagnosticBaseRadiology] as

SELECT
--exam
	 ExamEncounterRecno = RADExam.EncounterRecno
	,RADExam.SourceUniqueID
	,RADExam.AccessionNo
	,RADExam.ExamCode
	,RADExam.InterventionalFlag
	,RADExam.AllocatedTimeMins
	,RADExam.ExamDate
	,RADExam.ExamStartTime
	,RADExam.ExamEndTime
	,RADExam.RoomCode
	,RADExam.Radiographer1Code
	,RADExam.Radiographer1CompetenceLevel
	,RADExam.Radiographer1Difficulty
	,RADExam.Radiographer2Code
	,RADExam.Radiographer2CompetenceLevel
	,RADExam.Radiographer2Difficulty
	,RADExam.Radiographer3Code
	,RADExam.Radiographer3CompetenceLevel
	,RADExam.Radiographer3Difficulty
	,RADExam.RadiologistCode
	,RADExam.ContrastBatchNo
	,RADExam.BoneDensitometryValue
	,RADExam.Concentration
	,RADExam.ContractCode
	,RADExam.ContrastCode
	,RADExam.ImageStoreDisk
	,RADExam.ExamQuality
	,RADExam.FlexibleForm
	,RADExam.IgnoreAppt
	,RADExam.InjectedBy
	,RADExam.MuseumCode
	,RADExam.Reaction
	,RADExam.HasRedDot
	,RADExam.RestrainDose
	,RADExam.RestrainName
	,RADExam.RestrainType
	,RADExam.ScanSlices
	,RADExam.ExamScreeningTime
	,RADExam.ExamStatus
	,RADExam.ExamBookedDate
	,RADExam.ExamBookedTime
	,RADExam.ApptBookMode
	,RADExam.ApptCommitFlag
	,RADExam.ApptPreviousDate
	,RADExam.ApptPreviousTime
	,RADExam.ApptLetterPrintedDate
	,RADExam.SiteCodeAtTimeOfBooking
	,RADExam.OrderCommsUniqueID
	,RADExam.DaysWaiting
	,RADExam.ExamLogicalDeleteFlag
	,RADExam.ExamCreatedDate
	,RADExam.ExamCreatedTime
	,RADExam.ExamModifiedDate
	,RADExam.ExamModifiedTime
	,RADExam.ReportCheckedBy
	,RADExam.DateReported
	,RADExam.DateTyped
	,RADExam.DefaultLength
	,RADExam.IDChecked
	,RADExam.IDCheckedBy
	,RADExam.IsTyped
	,RADExam.IsVerified
	,RADExam.MPPSID
	,RADExam.MPPSStatus
	,RADExam.OrderID
	,RADExam.PregnancyChecked
	,RADExam.PregnancyCheckedBy
	,RADExam.QuantityUsed
	,RADExam.StudyID
	,RADExam.TimeFirstVerified
	,RADExam.TimeLastVerified
	,RADExam.WaitBreachDate
	,RADExam.WaitNPland
	,RADExam.WasPlanned
	,RADExam.WasScheduled
	,RADExam.WasWaiting
	,RADExam.WeeksWaiting
	,RADExamCreated = RADExam.Created
	,RADExamUpdated = RADExam.Updated
--event
	,EventEncounterRecno = RADEvent.EncounterRecno
	,EventSourceUniqueID = RADEvent.SourceUniqueID
	,RADEvent.RTTPathwayID
	,RADEvent.SourcePatientNo
	,RADEvent.OriginalSourcePatientNo
	,RADEvent.Abnormal
	,RADEvent.AbnormalActioned
	,RADEvent.AENumber
	,RADEvent.AntiD
	,RADEvent.AttendanceNo
	,RADEvent.AttendedFlag
	,RADEvent.AwaitingAcknowledgement
	,RADEvent.BillReferrerFlag
	,RADEvent.BookingModeCode
	,RADEvent.BookedByCode
	,RADEvent.RequestDate
	,RADEvent.BookedDate
	,RADEvent.EventDate
	,RADEvent.EventTime
	,RADEvent.DateOnWL
	,RADEvent.DayOfWeek
	,RADEvent.EventLogicalDeleteFlag
	,RADEvent.HasPathway
	,RADEvent.IntendedRadiologistCode
	,RADEvent.IsDictated
	,RADEvent.IsProcessed
	,RADEvent.JustifiedBy
	,RADEvent.NumberOfProcs
	,RADEvent.NumberOfScannedDocs
	,RADEvent.PatientTypeCode
	,RADEvent.KeyDate
	,RADEvent.NumberOfCancellations
	,RADEvent.PacsVolumeKey
	,RADEvent.ImageNo
	,RADEvent.LeadClinician
	,RADEvent.LmpDate
	,RADEvent.MobilityCode
	,RADEvent.DuringOnCallHoursFlag
	,RADEvent.PatientHeight
	,RADEvent.PatientWeight
	,RADEvent.Practitioner
	,RADEvent.PregnancyEventKey
	,RADEvent.PregnancyFlag
	,RADEvent.Questions
	,RADEvent.Reason
	,SourceOfReferralCode = RADEvent.ReferralSourceCode
	,RADEvent.ReferrerCode
	,RADEvent.ReferralLocationCode
	,RADEvent.ReferringPCTCode
	,RADEvent.RequestCategoryCode
	,RADEvent.RequiredClinician
	,RADEvent.VettingStatus
	,RADEvent.XDSFolderID
	,RADEvent.XDSFolderUID
	,RADEvent.AgeAtEvent
	,RADEvent.ClinicalHistory
	,RADEvent.Comment
	,RADEvent.ConsentComment
	,RADEvent.PatientCreationDate
	,RADEvent.PatientCreationTime
	,RADEvent.Deleted
	,RADEvent.EmailConsent
	,RADEvent.EpisodicPCTCode
	,RADEvent.EpisodicPCT
	,RADEvent.SiteCode
	,RADEvent.AttendancesAtSite
	,RADEvent.SpecialtyCode
	,RADEvent.UnreportedFlag
	,PriorityCode = RADEvent.UrgencyCode
	,RADEvent.DictatedDate
	,RADEvent.DictatedTime
	,RADEvent.DictatedByCode
	,RADEvent.DictatedBy
	,RADEvent.DictatedBy2Code
	,RADEvent.DictatedBy2
	,RADEvent.ConsentFlag
	,RADEvent.ContrastReactionFlag
	,RADEvent.EpisodicReferrerCode
	,RADEvent.EpisodicLocationCode
	,RADEvent.Surname
	,RADEvent.Forenames
	,RADEvent.Title
	,RADEvent.DateOfBirth
	,RADEvent.DateOfDeath
	,RADEvent.DistrictNo
	,RADEvent.Language
	,RADEvent.PatientModifiedDate
	,RADEvent.PatientModifiedTime
	,RADEvent.NHSNumber
	,RADEvent.NHSNumberStatus
	,RADEvent.NHSTraceStatus
	,RADEvent.RegisteredGpCode
	,RADEvent.RegisteredPracticeCode
	,RADEvent.TelMobileConsent
	,RADEvent.EthnicOriginCode
	,RADEvent.MRSAIndicator
	,RADEvent.SexCode
	,RADEvent.Address1
	,RADEvent.Address2
	,RADEvent.Address3
	,RADEvent.Address4
	,RADEvent.Postcode
	,RADEvent.PostcodePart1
	,RADEvent.PostcodePart2
	,RADEvent.ResidentialStatusCode
	,RADEvent.PhoneMobile
	,RADEvent.Phone1
	,RADEvent.Phone2
	,RADEvent.OriginatingPasInternalPatientNo1
	,RADEvent.OriginatingPasInternalPatientNo2
	,RADEvent.RestrictedDetailsFlag
	,RADEvent.DiabetesDrugFlag
	,RADEvent.ExamsInRange
	,RADEvent.UsesInhalerFlag
	,RADEvent.PreviousEventDate
	,RADEvent.NumberOfAttendances
	,RADEvent.NumberOfEvents
	,RADEvent.PdsScn
	,RADEvent.PatientDetailsStatus
	,RADEvent.HeartTransplantFlag
	,RADEvent.CurrentLocationArrivalDate
	,RADEvent.EventCreatedDate
	,RADEvent.EventCreatedTime
	,RADEvent.EventModifiedDate
	,RADEvent.EventModifiedTime
	,RADEventCreated = RADEvent.Created
	,RADEventUpdated = RADEvent.Updated

-- report

	,Report.ReportedDate
	,Report.ReportedTime
	,Report.ReportedBy
	,Report.TypedDate
	,Report.TypedTime
	,Report.TypedBy
	,Report.VerifiedDate
	,Report.VerifiedTime
	,Report.VerifiedBy
	,Report.LastVerifiedDate
	,Report.LastVerifiedTime
	,Report.LastVerifiedBy

--derivations
	,AgeCode = Utility.fn_CalcAgeCode(RADEvent.DateOfBirth,RADExam.ExamDate)

	,OutcomeCode =
		case 
		when left(RADStatus.StatusCode , 2) = 'AT' then 1
		when RADStatus.StatusCode = 'CPDNA' then 0
		else 2
		end

	,DirectorateCode = 
		CASE
		WHEN LEFT(RADEvent.SiteCode, 3) = 'RM4' THEN '80' --Trafford
		ELSE '92' --Clinical & Scientific
		END

	,ExamDurationMins = datediff(minute, RADExam.ExamStartTime, RADExam.ExamEndTime)
	,ExamToReportDays = datediff(dd, RADExam.ExamDate, Report.ReportedDate)

	--,ExamToReportWorkingDays = 
	--	case
	--	when ReportedDate is null then null
	--	else 
	--		(
	--			select 
	--				sum(convert(int ,IsWorkingDay))
	--			from
	--				WH.Calendar
	--			where
	--				WH.Calendar.TheDate between RADExam.ExamDate and Report.ReportedDate
	--		) - 1
	--	end

	,ReportToTypeDays = datediff(dd, Report.ReportedDate, Report.TypedDate)

	--,ReportToTypeWorkingDays = 
	--	case
	--	when TypedDate is null then null
	--	else 
	--		(
	--			select 
	--				sum(convert(int ,IsWorkingDay))
	--			from
	--				WH.Calendar
	--			where
	--				WH.Calendar.TheDate between Report.ReportedDate and Report.TypedDate
	--		) - 1
	--	end

	,TypeToVerifyDays = datediff(dd, Report.TypedDate, Report.VerifiedDate)

	--,TypeToVerifyWorkingDays = 
	--	case
	--	when VerifiedDate is null then null
	--	else 
	--		(
	--			select 
	--				sum(convert(int ,IsWorkingDay))
	--			from
	--				WH.Calendar
	--			where
	--				WH.Calendar.TheDate between Report.TypedDate and Report.VerifiedDate
	--		) - 1
	--	end

	,ReportToVerifyDays = datediff(dd, Report.ReportedDate, Report.VerifiedDate)

	--,ReportToVerifyWorkingDays = 
	--	case
	--	when VerifiedDate is null then null
	--	else 
	--		(
	--			select 
	--				sum(convert(int ,IsWorkingDay))
	--			from
	--				WH.Calendar
	--			where
	--				WH.Calendar.TheDate between Report.ReportedDate and Report.VerifiedDate
	--		) - 1
	--	end

	,ExamToVerifyDays = datediff(dd, RADExam.ExamDate, Report.VerifiedDate)

	--,ExamToVerifyWorkingDays =
	--	case
	--	when VerifiedDate is null then null
	--	else 
	--		(
	--			select 
	--				sum(convert(int ,IsWorkingDay))
	--			from
	--				WH.Calendar
	--			where
	--				WH.Calendar.TheDate between RADExam.ExamDate and Report.VerifiedDate
	--		) - 1
	--	end

	,CCGCode = CCGPractice.ParentOrganisationCode
	,ResidenceCCGCode = CCGPostcode.CCGCode

	,ExamNationalExamMap.NationalExamCode

	,ContextCode = 'CMFT||CRIS'

FROM
	[$(Warehouse)].RAD.Exam RADExam

INNER JOIN [$(Warehouse)].RAD.Event RADEvent
on	RADExam.EventSourceUniqueID = RADEvent.SourceUniqueID

left join [$(Organisation)].dbo.CCGPractice
on	CCGPractice.OrganisationCode = RADEvent.RegisteredPracticeCode

left join [$(Organisation)].dbo.CCGPostcode
on	CCGPostcode.Postcode = 
		case
		when len(RADEvent.Postcode) = 8 then RADEvent.Postcode
		else left(RADEvent.Postcode, 3) + ' ' + right(RADEvent.Postcode, 4) 
		end

INNER JOIN [$(Warehouse)].RAD.Status RADStatus
on	RADExam.SourceUniqueID = RADStatus.ExamSourceUniqueID
and	RADStatus.CurrentStatusFlag = 'Y'
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].RAD.Status Previous
	where
		Previous.ExamSourceUniqueID = RADStatus.ExamSourceUniqueID
	and	Previous.CurrentStatusFlag = 'Y'
	and	Previous.EncounterRecno > RADStatus.EncounterRecno
	)

LEFT JOIN (


-- 26/03/2008 CCB:
-- Each Examination has 0, 1 or more Summary Reports and 0 or 1 Normal Reports.
-- Multiple Summary Reports typically occur in the case of multiple pregnancies.
-- In these cases the assumption is that Summary Report details are the same across multiple
-- reports.
-- This retrieves distinct Report details by
-- coalescing distinct Normal and Summary Report details

	SELECT distinct
		 Report.ExamSourceUniqueID
		,ReportedDate = coalesce(Normal.ReportedDate, Summary.ReportedDate) 
		,ReportedTime = coalesce(Normal.ReportedTime, Summary.ReportedTime) 
		,ReportedBy = coalesce(Normal.ReportedBy, Summary.ReportedBy) 
		,TypedDate = coalesce(Normal.TypedDate, Summary.TypedDate) 
		,TypedTime = coalesce(Normal.TypedTime, Summary.TypedTime) 
		,TypedBy = coalesce(Normal.TypedBy, Summary.TypedBy) 
		,VerifiedDate = coalesce(Normal.VerifiedDate, Summary.VerifiedDate) 
		,VerifiedTime = coalesce(Normal.VerifiedTime, Summary.VerifiedTime) 
		,VerifiedBy = coalesce(Normal.VerifiedBy, Summary.VerifiedBy) 
		,LastVerifiedDate = coalesce(Normal.LastVerifiedDate, Summary.LastVerifiedDate) 
		,LastVerifiedTime = coalesce(Normal.LastVerifiedTime, Summary.LastVerifiedTime) 
		,LastVerifiedBy = coalesce(Normal.LastVerifiedBy, Summary.LastVerifiedBy) 
	FROM
		[$(Warehouse)].RAD.Report

	left join
		(
		select distinct
			 SourceUniqueID
			,ExamSourceUniqueID
			,ReportedDate
			,ReportedTime
			,ReportedBy
			,TypedDate
			,TypedTime
			,TypedBy
			,VerifiedDate
			,VerifiedTime
			,VerifiedBy
			,LastVerifiedDate
			,LastVerifiedTime
			,LastVerifiedBy
		from
			[$(Warehouse)].RAD.Report
		where
			ReportTypeCode = 'S'
		) Summary

	on	Report.ExamSourceUniqueID = Summary.ExamSourceUniqueID

	left join
		(
		select distinct
			 SourceUniqueID
			,ExamSourceUniqueID
			,ReportedDate
			,ReportedTime
			,ReportedBy
			,TypedDate
			,TypedTime
			,TypedBy
			,VerifiedDate
			,VerifiedTime
			,VerifiedBy
			,LastVerifiedDate
			,LastVerifiedTime
			,LastVerifiedBy
		from
			[$(Warehouse)].RAD.Report
		where
			ReportTypeCode = 'N'
		) Normal

	on	Report.ExamSourceUniqueID = Normal.ExamSourceUniqueID
) Report

on	RADExam.SourceUniqueID = Report.ExamSourceUniqueID


left join [$(Warehouse)].DM01.ExamNationalExamMap
on	ExamNationalExamMap.ExamCode = RADExam.ExamCode
and	ExamNationalExamMap.InterfaceCode = 'CRIS'



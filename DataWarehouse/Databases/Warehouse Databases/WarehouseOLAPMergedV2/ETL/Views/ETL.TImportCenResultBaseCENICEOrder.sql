﻿






CREATE view [ETL].[TImportCenResultBaseCENICEOrder] as

select
	OrderRecno
	,ContextCode = 'CEN||ICE'
	,SourceUniqueID
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode = ClinicianID
	,MainSpecialtyCode
	,SpecialtyCode = MainSpecialtyCode
	,ProviderCode = ProviderID
	,LocationCode = LocationID
	,OrderPriority
	,OrderStatusCode
	,OrderRequestTime
	,OrderReceivedTime
	,OrderCollectionTime
	,InterfaceCode
from
	[$(Warehouse)].Result.CENICEOrder Import
























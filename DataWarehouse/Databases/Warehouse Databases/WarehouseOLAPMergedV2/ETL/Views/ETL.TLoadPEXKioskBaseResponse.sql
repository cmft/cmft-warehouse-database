﻿









CREATE view [ETL].[TLoadPEXKioskBaseResponse]

as

select
	ResponseRecno = [KioskResponseRecno]
	,[SurveyTakenID]
	,[SurveyID]
	,[QuestionID]
	,[AnswerID]
	,[Answer]	
	,[SurveyDate]
	,[SurveyTime]
	,[LocationID]
	,WardID
	,[ChannelID]
	,[QuestionTypeID]
	,[LocationTypeID]
	,ContextCode = 'CMFT||CRT'
	,[Created]
	,[Updated]
	,[ByWhom]

from
	[$(Warehouse)].[PEX].[KioskResponse]










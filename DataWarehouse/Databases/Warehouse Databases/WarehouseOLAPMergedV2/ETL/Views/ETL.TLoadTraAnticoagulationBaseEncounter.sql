﻿

CREATE view [ETL].[TLoadTraAnticoagulationBaseEncounter] as

select
	EncounterRecno
	,SourceUniqueID
	,NHSNumber
	,PatientSurname
	,PatientForename
	,DateOfBirth
	,SexCode
	,Postcode
	,ClinicID
	,TreatmentStartDate
	,TestDate
	,Result
	,Dose
	,DiagnosisID
	,DiagnosisDate
	,CommissionerID
	,GpPracticeCode
	,InterfaceCode
	,ContextCode = 'TRA||DAWN'
	,Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].Anticoagulation.Encounter

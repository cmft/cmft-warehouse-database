﻿


CREATE view [ETL].[TLoadCenObservationBaseObservationProfile] as

select
	ObservationProfileRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,LocationID
	,AdmissionSourceUniqueID 
	,ObservationProfile.ProfileID
	,ProfileTemplateID = ProfileTemplate.ProfileID
	,TemplatePriorityID
	,DrivingTableCode
	,DrivingTableID
	,ProfileReasonID
	,StepNumber
	,IterationNumber
	,ObservationProfile.StartTime
	,ObservationProfile.EndTime
	,ObservationProfile.CreatedBy
	,ObservationProfile.CreatedTime
	,ObservationProfile.LastModifiedBy
	,ObservationProfile.LastModifiedTime
	,Active
	,ContextCode = 'CEN||PTRACK'
	,ObservationProfile.Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].Observation.ObservationProfile

left join [$(Warehouse)].Observation.ProfileTemplate
on	ProfileTemplate.ProfileID = ObservationProfile.ProfileID





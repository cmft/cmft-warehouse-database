﻿
create view ETL.TLoadCenAPCBaseUrinaryTractInfection

as

select
	[UrinaryTractInfectionRecno]
	,[SourceUniqueID]
	,[SourcePatientNo]
	,[SourceSpellNo]
	,[SymptomStartDate]
	,[SymptomStartTime]
	,[SymptomEndDate]
	,[SymptomEndTime]
	,[UrinaryTestRequestedDate]
	,[UrinaryTestRequestedTime]
	,[UrinaryTestReceivedDate]
	,[UrinaryTestReceivedTime]
	,[UrinaryTestPositive]
	,[TreatmentStartDate]
	,[TreatmentStartTime]
	,[TreatmentEndDate]
	,[TreatmentEndTime]
	,[CatheterInsertedDate]
	,[CatheterInsertedTime]
	,[CatheterRemovedDate]
	,[CatheterRemovedTime]
	,[CatheterTypeID]
	,[WardCode]
	,[InterfaceCode]
	,ContextCode = 'CEN||BEDMAN'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[APC].[UrinaryTractInfection]
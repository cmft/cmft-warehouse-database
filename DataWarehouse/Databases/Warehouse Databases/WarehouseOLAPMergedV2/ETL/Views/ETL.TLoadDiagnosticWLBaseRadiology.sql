﻿



CREATE view [ETL].[TLoadDiagnosticWLBaseRadiology] as

/* 
==============================================================================================
Description:	

When		Who			What
20150922	Paul Egan	Changed method from DM01.RadiologyWait to RAD.WaitingList.
						Removed WHERE clause "Waiter.EventDate > Waiter.CensusDate" as some Event
						Dates are NULL if patient does not yet have an appointment date.
20150925	Paul Egan	Added clock reset columns.
						Added Exam Key to SourceUniqueID to guarantee uniqueness.
===============================================================================================
*/


/* Radiology */

/*CCB 2014-04-02
This is the correct approach, but assumes correct maintenance of the RAD schema in Warehouse */

select
	 SourceUniqueID =
			 Waiter.SourceUniqueID
			 + '||' + Waiter.ExamCode
			 + '||' + Waiter.ExamSourceUniqueID		-- Added 20150925 Paul Egan. Need Exam Key to guarantee uniqueness

	,CensusDate = cast(Waiter.CensusDate as date)
	,SourcePatientNo = Waiter.SourcePatientNo
	,PCTCode= Waiter.RegisteredPCTCode
	,SiteCode= Waiter.SiteCode

	,WaitTypeCode =
		CASE
		WHEN  Waiter.Planned = 'Y' THEN '20'
		ELSE '10'
		END

	,WLStatus = Waiter.StatusCode
	,LengthOfWaitDays = datediff(day, Waiter.RequestDate, Waiter.CensusDate)
	,ExamCode = Waiter.ExamCode
	,PatientTypeCode = Waiter.PatientTypeCode

	,DirectorateCode = 
		CASE
		WHEN LEFT(SiteCode, 3) = 'RM4' THEN '80' --Trafford
		ELSE '92' --Clinical & Scientific
		END

	,DiagnosticNationalExamCode = ExamNationalExamMap.NationalExamCode

	,RequestDate = cast(Waiter.RequestDate as date)
	,EventDate = cast(Waiter.EventDate as date)
	,DateOfBirth = cast(Waiter.DateOfBirth as date)
	,DistrictNo = case when Waiter.DistrictNo = '' then null else Waiter.DistrictNo end
	,PatientSurname = Waiter.PatientSurname
	,PatientForename = Waiter.PatientForenames
	,SpecialtyCode = Waiter.SpecialtyCode

	,Waiter.EncounterRecno
	,Waiter.PatientTitle
	,Waiter.SexCode
	,Waiter.NHSNumber
	,Waiter.PatientAddress1
	,Waiter.PatientAddress2
	,Waiter.PatientAddress3
	,Waiter.PatientAddress4
	,Waiter.Postcode
	,Waiter.DaysWaitingAdjusted
	,Waiter.ModalityCode
	,Waiter.RegisteredGpCode
	,Waiter.RegisteredPracticeCode
	,PriorityCode = Waiter.UrgencyCode
	,Waiter.Comment
	,Waiter.ExamGroupCode1
	,Waiter.ExamGroupCode2
	,SourceOfReferralCode = Waiter.ReferralSourceCode
	,Waiter.ReferrerCode
	,Waiter.ReferralLocationCode
	,Waiter.WaitingButNotPlanned
	,Waiter.Planned
	,Waiter.Scheduled
	,Waiter.Waiting

	,AgeCode = Utility.fn_CalcAgeCode(Waiter.DateOfBirth, Waiter.CensusDate)

	,WithAppointment =
		case
		when Waiter.EventDate is null then 0
		else 1
		end 

	,WLStatusTypeCode = Waiter.StatusType

	,CCGCode = CCGPractice.ParentOrganisationCode
	,ResidenceCCGCode = CCGPostcode.CCGCode

	,ContextCode = 'CMFT||CRIS'
	,Waiter.Created
	,Waiter.Updated
	,Waiter.ByWhom
	
	/* Added Paul Egan 20150925 */
	,Waiter.ClockResetDate
	,Waiter.BreachDate
	,EventSourceUniqueID = Waiter.SourceUniqueID
	,Waiter.ExamSourceUniqueID
from
	[$(Warehouse)].RAD.WaitingList Waiter

left join [$(Warehouse)].DM01.ExamNationalExamMap
on	ExamNationalExamMap.ExamCode = Waiter.ExamCode
and	ExamNationalExamMap.InterfaceCode = 'CRIS'

left join [$(Organisation)].dbo.CCGPractice
on	CCGPractice.OrganisationCode = Waiter.RegisteredPracticeCode

left join [$(Organisation)].dbo.CCGPostcode
on	CCGPostcode.Postcode = 
		case
		when len(Waiter.Postcode) = 8 then Waiter.Postcode
		else left(Waiter.Postcode, 3) + ' ' + right(Waiter.Postcode, 4) 
		end

where 
	----------Waiter.EventDate > Waiter.CensusDate		-- Commented out Paul Egan 20150921

/*Only load the latest census */
	Waiter.CensusDate >= '20150930'
--	(
--select
--	max(CensusDate)
--from
--	[$(Warehouse)].RAD.WaitingList Latest
	----------where
	----------	Latest.EventDate > Latest.CensusDate	-- Commented out Paul Egan 20150921
	--)




----CCB 2014-04-02
----this approach uses the legacy DM01 schema objects and should be replaced with the above code when the RAD schema is maintained correctly
--select
--	 SourceUniqueID =
--		Waiter.EventKey
--		+ '||' + Waiter.ExamCode

--	,CensusDate = cast(Waiter.CensusDate as date)
--	,SourcePatientNo = Waiter.SourcePatientNo
--	,PCTCode = Waiter.PCTCode
--	,SiteCode = Waiter.SiteCode

--	,WaitTypeCode = 
--		CASE
--		WHEN  Waiter.Planned = 'Y' THEN '20'
--		ELSE '10'
--		END

--	,WLStatus = Waiter.StatusCode
--	,LengthOfWaitDays = datediff(day , Waiter.RequestDate , Waiter.CensusDate)
--	,ExamCode = Waiter.ExamCode

--	,PatientTypeCode = 
--		case Waiter.PatientTypeDescription
--		when 'GP Direct Access Patient' then ''
--		when 'In Patient' then ''
--		when 'Other Patient' then ''
--		when 'Research' then ''
--		when 'Dental Patient' then ''
--		when 'Day Case Patient' then ''
--		when 'Out Patient' then ''
--		when 'A & E Attender' then ''
--		when 'In Patient Research' then ''
--		end

--	,DirectorateCode = 
--		CASE
--		WHEN LEFT(SiteCode, 3) = 'RM4' THEN '80' --Trafford
--		ELSE '92' --Clinical & Scientific
--		END

--	,DiagnosticNationalExamCode = ExamNationalExamMap.NationalExamCode

--	,RequestDate = cast(Waiter.RequestDate as date)
--	,EventDate = cast(Waiter.EventDate as date)
--	,DateOfBirth = cast(Waiter.DOB as date)
--	,DistrictNo = case when Waiter.DistrictNo = '' then null else Waiter.DistrictNo end
--	,PatientSurname = Waiter.Surname
--	,PatientForename = Waiter.Forenames
--	,SpecialtyCode = null

--	,/*Waiter.EncounterRecno*/ EncounterRecno = null
--	,PatientTitle = Patient.Title
--	,SexCode = Patient.SexCode
--	,NHSNumber = Waiter.NHSNumber
--	,PatientAddress1 = Patient.AddressLine1
--	,PatientAddress2 = Patient.AddressLine2
--	,PatientAddress3 = Patient.AddressLine3
--	,PatientAddress4 = Patient.AddressLine4
--	,Postcode = Patient.Postcode
--	,/*Waiter.DaysWaitingAdjusted*/ DaysWaitingAdjusted = Waiter.Days
--	,Waiter.ModalityCode

--	,/*Waiter.RegisteredGpCode*/ Patient.RegisteredGpCode
--	,/*Waiter.RegisteredPracticeCode*/ RegisteredPracticeCode = Gp.PracticeCode
--	,PriorityCode = null --Waiter.UrgencyCode
--	,Comment = null --Waiter.Comment
--	,ExamGroupCode1 = Waiter.Group1
--	,ExamGroupCode2 = Waiter.Group2
--	,SourceOfReferralCode = null --Waiter.ReferralSourceCode
--	,ReferrerCode = null --Waiter.ReferrerCode
--	,ReferralLocationCode = null --Waiter.ReferralLocationCode

--	,/*Waiter.WaitingButNotPlanned*/ WaitingButNotPlanned =
--		case
--		when Waiter.Planned = 'N' then 'Y'
--		else 'N'
--		end

--	,Waiter.Planned
--	,Waiter.Scheduled
--	,/*Waiter.Waiting*/ Waiting = 'Y'

--	,AgeCode = Utility.fn_CalcAgeCode(Waiter.DOB, Waiter.CensusDate)

--	,WithAppointment =
--		case
--		when Waiter.EventDate is null then 0
--		else 1
--		end 

--	,WLStatusTypeCode = null --Waiter.StatusType

--	,CCGCode = CCGPractice.ParentOrganisationCode
--	,ResidenceCCGCode = CCGPostcode.CCGCode

--	,ContextCode = 'CMFT||CRIS'
--	,Created = null
--	,Updated = null
--	,ByWhom = null
--from
--	[$(Warehouse)].DM01.RadiologyWait Waiter

--left join [$(Warehouse)].DM01.ExamNationalExamMap
--on	ExamNationalExamMap.ExamCode = Waiter.ExamCode
--and	ExamNationalExamMap.InterfaceCode = 'CRIS'

--left join [$(Warehouse)].PAS.Patient
--on	Waiter.DistrictNo = Patient.DistrictNo

--left join [$(Warehouse)].PAS.Gp
--on	Gp.GpCode = Patient.RegisteredGpCode

--left join [$(Organisation)].dbo.CCGPractice
--on	CCGPractice.OrganisationCode = Gp.PracticeCode

--left join [$(Organisation)].dbo.CCGPostcode
--on	CCGPostcode.Postcode = Patient.Postcode

--where 
--	Waiter.EventDate > Waiter.CensusDate

----only load the latest census
--and	Waiter.CensusDate =
--	(
--	select
--		max(CensusDate)
--	from
--		[$(Warehouse)].DM01.RadiologyWait Latest
--	where
--		Latest.EventDate > Latest.CensusDate
--	)









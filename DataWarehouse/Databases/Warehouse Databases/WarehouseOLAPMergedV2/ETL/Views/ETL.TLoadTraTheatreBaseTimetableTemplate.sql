﻿CREATE view [ETL].[TLoadTraTheatreBaseTimetableTemplate] as

select
	 TimetableTemplate.TimetableTemplateCode
	,TimetableTemplate.SessionNumber
	,TimetableTemplate.DayNumber
	,TimetableTemplate.TheatreCode
	,TimetableTemplate.SessionStartTime
	,TimetableTemplate.SessionEndTime
	,ConsultantCode = Consultant.StaffCode
	,AnaesthetistCode = Anaesthetist.StaffCode
	,SpecialtyCode = Specialty.SpecialtyCode

	,TimetableTemplate.LogLastUpdated
	,TimetableTemplate.RecordLogTemplates
from
	[$(TraffordWarehouse)].Theatre.TimetableTemplate

left join [$(TraffordWarehouse)].Theatre.Staff Anaesthetist
on	Anaesthetist.StaffCode1 = TimetableTemplate.AnaesthetistCode

left join [$(TraffordWarehouse)].Theatre.Staff Consultant
on	Consultant.StaffCode1 = TimetableTemplate.ConsultantCode

left join [$(TraffordWarehouse)].Theatre.Specialty
on	Specialty.SpecialtyCode1 = TimetableTemplate.SpecialtyCode

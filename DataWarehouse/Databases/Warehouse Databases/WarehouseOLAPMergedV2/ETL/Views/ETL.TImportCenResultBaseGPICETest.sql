﻿












CREATE view [ETL].[TImportCenResultBaseGPICETest] as

select
	TestRecno
	,ContextCode = 'CEN||GPICE'
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode = ClinicianID
	,MainSpecialtyCode
	,SpecialtyCode = MainSpecialtyCode
	,ProviderCode = ProviderID
	,LocationCode = LocationID
	,TestCode = TestID
	,TestStatus
	,InterfaceCode
	,TestChecksum
	,Created
	,Updated
from
	[$(Warehouse)].Result.GPICETest Import


















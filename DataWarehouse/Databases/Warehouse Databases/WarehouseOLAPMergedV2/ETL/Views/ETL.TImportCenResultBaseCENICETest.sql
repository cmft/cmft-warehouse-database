﻿











CREATE view [ETL].[TImportCenResultBaseCENICETest] as

select
	TestRecno
	,ContextCode = 'CEN||ICE'
	,SourceUniqueID
	,OrderSourceUniqueID
	,OrderRequestTime
	,OrderStatusCode
	,PatientID
	,PatientIdentifier
	,DistrictNo
	,CasenoteNumber
	,NHSNumber
	,ConsultantCode = ClinicianID
	,MainSpecialtyCode
	,SpecialtyCode = MainSpecialtyCode
	,ProviderCode = ProviderID
	,LocationCode = LocationID
	,TestCode = TestID
	,TestStatus
	,InterfaceCode
	,TestChecksum
	,Created
	,Updated
from
	[$(Warehouse)].Result.CENICETest Import

















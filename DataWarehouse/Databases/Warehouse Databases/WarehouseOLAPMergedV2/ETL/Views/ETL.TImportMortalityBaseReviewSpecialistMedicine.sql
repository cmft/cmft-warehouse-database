﻿





CREATE View [ETL].[TImportMortalityBaseReviewSpecialistMedicine]


as

Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode 
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade = null
		,RequiresFurtherInvestigation = 0
		,QuestionNo = row_number() over (partition by SourceUniqueID order by SourceUniqueID)
		,Question
		,Response
		,ContextCode = 'CMFT||MORTARCHV'
		
	from

		(
		select
			SourceUniqueID
			,SourcePatientNo	
			,EpisodeStartTime	
			,APCContextCode = ContextCode
			,FormTypeCode = FormTypeID
			,ReviewStatusCode = ReviewStatus
			,PrimaryReviewerAssignedTime = ReviewedDate
			,PrimaryReviewerCompletedTime = 
				case
					when ReviewStatus = 10 then ReviewedDate
					else null
				end
			,PrimaryReviewerCode = ReviewedBy
			,SecondaryReviewerAssignedTime = null
			,SecondaryReviewerCompletedTime = null
			,SecondaryReviewerCode = null
			
			,PrimaryDiagnosisOnAdmission = isnull(cast(PrimaryDiagnosisOnAdmission as varchar(max)),'')
			,PrimaryDiagnosisOnAdmissionComment = cast(PrimaryDiagnosisOnAdmissionComment as varchar(max))
			,PrimaryDiagnosisAfterTestsConfirmed = cast(PrimaryDiagnosisAfterTestsConfirmed as varchar(max))
			,PrimaryDiagnosisAfterTestsConfirmedComment = cast(PrimaryDiagnosisAfterTestsConfirmedComment as varchar(max))
			,CauseOfDeath1a = cast(CauseOfDeath1a as varchar(max))
			,CauseOfDeath1b = cast(CauseOfDeath1b as varchar(max))
			,CauseOfDeath1c = cast(CauseOfDeath1c as varchar(max))
			,CauseOfDeath2 = cast(CauseOfDeath2 as varchar(max))
			,CauseOfDeathComment = cast(CauseOfDeathComment as varchar(max))
			,CodedDiagnosis = cast(CodedDiagnosis as varchar(max))
			,CodedDiagnosisComment = cast(CodedDiagnosisComment as varchar(max))
			,AgreeWithCauseOfDeath = cast(AgreeWithCauseOfDeath as varchar(max))
			,AgreeWithCauseOfDeathComment = cast(AgreeWithCauseOfDeathComment as varchar(max))
			,HospitalPostmortem = cast(HospitalPostmortem as varchar(max))
			,HospitalPostmortemComment = cast(HospitalPostmortemComment as varchar(max))
			,CoronerInformedOrConsulted = cast(CoronerInformedOrConsulted as varchar(max))
			,CoronerInformedOrConsultedComment = cast(CoronerInformedOrConsultedComment as varchar(max))
			,CoronerPostmortemPerformed = cast(CoronerPostmortemPerformed as varchar(max))
			,CoronerPostmortemPerformedComment = cast(CoronerPostmortemPerformedComment as varchar(max))
			,MalignancyPresent = cast(MalignancyPresent as varchar(max))
			,MalignancyPresentComment = cast(MalignancyPresentComment as varchar(max))
			,CoExistingFactors = cast(CoExistingFactors as varchar(max))
			,CoExistingFactorsComment = cast(CoExistingFactorsComment as varchar(max))
			,ConsultantReview = cast(ConsultantReview as varchar(max))
			,ConsultantReviewComment = cast(ConsultantReviewComment as varchar(max))
			,EvidenceOfClearManagementPlan = cast(EvidenceOfClearManagementPlan as varchar(max))
			,EvidenceOfClearManagementPlanComment = cast(EvidenceOfClearManagementPlanComment as varchar(max))
			,EssentialInvestigationsObtainedWithoutDelay = cast(EssentialInvestigationsObtainedWithoutDelay as varchar(max))
			,EssentialInvestigationsObtainedWithoutDelayComment = cast(EssentialInvestigationsObtainedWithoutDelayComment as varchar(max))
			,InitialManagementStepsAppropriateAndAdequate = cast(InitialManagementStepsAppropriateAndAdequate as varchar(max))
			,InitialManagementStepsAppropriateAndAdequateComment = cast(InitialManagementStepsAppropriateAndAdequateComment as varchar(max))
			,OmissionsInInitialManagement = cast(OmissionsInInitialManagement as varchar(max))
			,OmissionsInInitialManagementComment = cast(OmissionsInInitialManagementComment as varchar(max))
			,AdmittedToAppropriateWard = cast(AdmittedToAppropriateWard as varchar(max))
			,AdmittedToAppropriateWardComment = cast(AdmittedToAppropriateWardComment as varchar(max))
			,HowManyWardsPatientOn = cast(HowManyWardsPatientOn as varchar(max))
			,HowManyWardsPatientOnComment = cast(HowManyWardsPatientOnComment as varchar(max))
			,MedicalStaffWriteInNotesEveryWeekday = cast(MedicalStaffWriteInNotesEveryWeekday as varchar(max))
			,MedicalStaffWriteInNotesEveryWeekdayComment = cast(MedicalStaffWriteInNotesEveryWeekdayComment as varchar(max))
			,ConsultantReviewAtLeastTwiceAWeek = cast(ConsultantReviewAtLeastTwiceAWeek as varchar(max))
			,ConsultantReviewAtLeastTwiceAWeekComment = cast(ConsultantReviewAtLeastTwiceAWeekComment as varchar(max))
			,ICUOpinionGiven = cast(ICUOpinionGiven as varchar(max))
			,ICUOpinionGivenComment = cast(ICUOpinionGivenComment as varchar(max))
			,TransferFromGeneralToICUorHDU = cast(TransferFromGeneralToICUorHDU as varchar(max))
			,TransferFromGeneralToICUorHDUComment = cast(TransferFromGeneralToICUorHDUComment as varchar(max))
			,ReadmissionToICUorHDU = cast(ReadmissionToICUorHDU as varchar(max))
			,ReadmissionToICUorHDUComment = cast(ReadmissionToICUorHDUComment as varchar(max))
			,AnyRecognisableMedicationErrors = cast(AnyRecognisableMedicationErrors as varchar(max))
			,AnyRecognisableMedicationErrorsComment = cast(AnyRecognisableMedicationErrorsComment as varchar(max))
			,ChemotherapyOrImmunosuppressantsWithin30Days = cast(ChemotherapyOrImmunosuppressantsWithin30Days as varchar(max))
			,ChemotherapyOrImmunosuppressantsWithin30DaysComment = cast(ChemotherapyOrImmunosuppressantsWithin30DaysComment as varchar(max))
			,TimingOfDiagnosis = cast(TimingOfDiagnosis as varchar(max))
			,TimingOfDiagnosisComment = cast(TimingOfDiagnosisComment as varchar(max))
			,TimingOfDeliveringCare = cast(TimingOfDeliveringCare as varchar(max))
			,TimingOfDeliveringCareComment = cast(TimingOfDeliveringCareComment as varchar(max))
			,CommunicationIssues = cast(CommunicationIssues as varchar(max))
			,CommunicationIssuesComment = cast(CommunicationIssuesComment as varchar(max))
			,AdverseEvents = cast(AdverseEvents as varchar(max))
			,AdverseEventsComment = cast(AdverseEventsComment as varchar(max))
			,IncidentReport = cast(IncidentReport as varchar(max))
			,IncidentReportComment = cast(IncidentReportComment as varchar(max))
			,AnythingCouldBeDoneDifferently = cast(AnythingCouldBeDoneDifferently as varchar(max))
			,AnythingCouldBeDoneDifferentlyComment = cast(AnythingCouldBeDoneDifferentlyComment as varchar(max))
			,DocumentationIssues = cast(DocumentationIssues as varchar(max))
			,DocumentationIssuesComment = cast(DocumentationIssuesComment as varchar(max))
			,NotableGoodQualityCare = cast(NotableGoodQualityCare as varchar(max))
			,LCPInPatientsNotes = cast(LCPInPatientsNotes as varchar(max))
			,LCPInPatientsNotesComment = cast(LCPInPatientsNotesComment as varchar(max))
			,PreferredPlaceOfDeathRecorded = cast(PreferredPlaceOfDeathRecorded as varchar(max))
			,PreferredPlaceOfDeathRecordedComment = cast(PreferredPlaceOfDeathRecordedComment as varchar(max))
			,ExpressedWithToDieOutsideOfHospital = cast(ExpressedWithToDieOutsideOfHospital as varchar(max))
			,ExpressedWithToDieOutsideOfHospitalComment = cast(ExpressedWithToDieOutsideOfHospitalComment as varchar(max))
			,LessonsLearned = cast(LessonsLearned as varchar(max))
			,ActionPlan = cast(ActionPlan as varchar(max))
			,ActionPlanReviewDate = cast(ActionPlanReviewDate as varchar(max))
		from
			[$(Warehouse)].Mortality.SpecialistMedicalServices
		) 
		Mortality

		unpivot
				(
				Response for Question in 
						(
						PrimaryDiagnosisOnAdmission
						,PrimaryDiagnosisOnAdmissionComment
						,PrimaryDiagnosisAfterTestsConfirmed
						,PrimaryDiagnosisAfterTestsConfirmedComment
						,CauseOfDeath1a
						,CauseOfDeath1b
						,CauseOfDeath1c
						,CauseOfDeath2
						,CauseOfDeathComment
						,CodedDiagnosis
						,CodedDiagnosisComment
						,AgreeWithCauseOfDeath
						,AgreeWithCauseOfDeathComment
						,HospitalPostmortem
						,HospitalPostmortemComment
						,CoronerInformedOrConsulted
						,CoronerInformedOrConsultedComment
						,CoronerPostmortemPerformed
						,CoronerPostmortemPerformedComment
						,MalignancyPresent
						,MalignancyPresentComment
						,CoExistingFactors
						,CoExistingFactorsComment
						,ConsultantReview
						,ConsultantReviewComment
						,EvidenceOfClearManagementPlan
						,EvidenceOfClearManagementPlanComment
						,EssentialInvestigationsObtainedWithoutDelay
						,EssentialInvestigationsObtainedWithoutDelayComment
						,InitialManagementStepsAppropriateAndAdequate
						,InitialManagementStepsAppropriateAndAdequateComment
						,OmissionsInInitialManagement
						,OmissionsInInitialManagementComment
						,AdmittedToAppropriateWard
						,AdmittedToAppropriateWardComment
						,HowManyWardsPatientOn
						,HowManyWardsPatientOnComment
						,MedicalStaffWriteInNotesEveryWeekday
						,MedicalStaffWriteInNotesEveryWeekdayComment
						,ConsultantReviewAtLeastTwiceAWeek
						,ConsultantReviewAtLeastTwiceAWeekComment
						,ICUOpinionGiven
						,ICUOpinionGivenComment
						,TransferFromGeneralToICUorHDU
						,TransferFromGeneralToICUorHDUComment
						,ReadmissionToICUorHDU
						,ReadmissionToICUorHDUComment
						,AnyRecognisableMedicationErrors
						,AnyRecognisableMedicationErrorsComment
						,ChemotherapyOrImmunosuppressantsWithin30Days
						,ChemotherapyOrImmunosuppressantsWithin30DaysComment
						,TimingOfDiagnosis
						,TimingOfDiagnosisComment
						,TimingOfDeliveringCare
						,TimingOfDeliveringCareComment
						,CommunicationIssues
						,CommunicationIssuesComment
						,AdverseEvents
						,AdverseEventsComment
						,IncidentReport
						,IncidentReportComment
						,AnythingCouldBeDoneDifferently
						,AnythingCouldBeDoneDifferentlyComment
						,DocumentationIssues
						,DocumentationIssuesComment
						,NotableGoodQualityCare
						,LCPInPatientsNotes
						,LCPInPatientsNotesComment
						,PreferredPlaceOfDeathRecorded
						,PreferredPlaceOfDeathRecordedComment
						,ExpressedWithToDieOutsideOfHospital
						,ExpressedWithToDieOutsideOfHospitalComment
						,LessonsLearned
						,ActionPlan
						,ActionPlanReviewDate

						)
				) 
				Response 
		) 
		SpecialistMedicine





﻿
CREATE view [ETL].[TLoadCenAEBaseDiagnosis]
as

select
	 Encounter.MergeEncounterRecno
	,Diagnosis.SequenceNo
	,DiagnosticSchemeCode = Diagnosis.DiagnosticSchemeInUse
	,Diagnosis.DiagnosisCode
	,Diagnosis.DiagnosisSiteCode
	,Diagnosis.DiagnosisSideCode
	,Encounter.ContextCode
from
	[$(Warehouse)].AE.Diagnosis Diagnosis

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Diagnosis.AESourceUniqueID
and	Encounter.ContextCode in
	(
	 'CEN||ADAS'
	,'CEN||PAS'
	,'CEN||SYM'
	,'TRA||ADAS'
	)



﻿

create view [ETL].[TLoadCenAEBaseAssault]
as

select
	 Encounter.MergeEncounterRecno
	,Assault.AssaultDate
    ,Assault.AssaultTime
    ,Assault.AssaultWeapon
    ,Assault.AssaultWeaponDetails
    ,Assault.AssaultLocation
    ,Assault.AssaultLocationDetails
    ,Assault.AlcoholConsumed3Hour
    ,Assault.AssaultRelationship
    ,Assault.AssaultRelationshipDetails


	,Encounter.ContextCode
from
	[$(Warehouse)].AE.Assault Assault

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Assault.AESourceUniqueID
and	Encounter.ContextCode in
	(
	 'CEN||ADAS'
	,'CEN||PAS'
	,'CEN||SYM'
	,'TRA||ADAS'
	)




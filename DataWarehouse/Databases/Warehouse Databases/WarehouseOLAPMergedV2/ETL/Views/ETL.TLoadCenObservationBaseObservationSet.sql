﻿









CREATE view [ETL].[TLoadCenObservationBaseObservationSet]
as

select
	 ObservationSetRecno
	,SourceUniqueID
	,CasenoteNumber
	,DateOfBirth
	,SpecialtyID
	,LocationID
	,LocationStartTime
	,ReplacedSourceUniqueID
	,CurrentObservationSetFlag
	,AdditionalObservationSetFlag
	,AdmissionSourceUniqueID
	,EarlyWarningScoreRegimeApplicationID
	,ObservationProfileApplicationID
	,ObservationNotTakenReasonID
	,ClinicianPresentSeniorityID
	,StartDate
	,StartTime
	,TakenDate
	,TakenTime
	,OverallRiskIndexCode
	,OverallAssessedStatusID
	,AlertSeverityID
	,DueTime
	,DueTimeStatusID
	,DueTimeCreatedBySourceUniqueID
	,AlertChainID
	,FirstInAlertChain
	,LastInAlertChain
	,AlertChainMinutes
	,LastModifiedTime
	,ContextCode = 'CEN||PTRACK'
from
	[$(Warehouse)].Observation.ObservationSet










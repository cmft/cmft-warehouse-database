﻿
create view [ETL].[TLoadCenAPCBaseVTEAssessment]

as

select
	[VTEAssessmentRecno]
	,[SourceUniqueID]
	,[SourcePatientNo]
	,[SourceSpellNo]
	,[NoAssessmentReasonID]
	,[AssessmentDate]
	,[AssessmentTime]
	,[AtRisk]
	,[PreventativeMedicationStartDate]
	,[PreventativeMedicationStartTime]
	,[PreventativeNursingActionStartDate]
	,[PreventativeNursingActionStartTime]
	,[WardCode]
	,[InterfaceCode]
	,ContextCode = 'CEN||BEDMAN'
	,[Created]
	,[Updated]
	,[ByWhom]
from
	[$(Warehouse)].[APC].[VTEAssessment]
﻿



CREATE View [ETL].[TImportMortalityBaseReviewNeonatology]


as


Select
	SourceUniqueID
	,SourcePatientNo	
	,EpisodeStartTime	
	,APCContextCode
	,FormTypeCode
	,MortalityReviewAdded
	,ReviewStatusCode
	,PrimaryReviewAssignedTime
	,PrimaryReviewCompletedTime
	,PrimaryReviewerCode
	,SecondaryReviewAssignedTime 
	,SecondaryReviewCompletedTime
	,SecondaryReviewerCode 
	,ClassificationGrade
	,RequiresFurtherInvestigation
	,QuestionNo
	,Question
	,Response
	,ContextCode
	,ReviewCheckSum = CHECKSUM 
						(
						SourceUniqueID
						,SourcePatientNo	
						,EpisodeStartTime	
						,APCContextCode
						,FormTypeCode
						,MortalityReviewAdded
						,ReviewStatusCode
						,PrimaryReviewAssignedTime
						,PrimaryReviewCompletedTime
						,PrimaryReviewerCode
						,SecondaryReviewAssignedTime 
						,SecondaryReviewCompletedTime
						,SecondaryReviewerCode 
						,ClassificationGrade
						,RequiresFurtherInvestigation
						,QuestionNo
						,Question
						,Response
						,ContextCode
						)
from
	(
	
	select 
		SourceUniqueID
		,SourcePatientNo	
		,EpisodeStartTime	
		,APCContextCode
		,FormTypeCode
		,MortalityReviewAdded
		,ReviewStatusCode
		,PrimaryReviewAssignedTime = CAST(PrimaryReviewerAssignedTime as datetime)
		,PrimaryReviewCompletedTime = CAST(PrimaryReviewerCompletedTime as datetime)
		,PrimaryReviewerCode = CAST(PrimaryReviewerCode as varchar(50))
		,SecondaryReviewAssignedTime = CAST(SecondaryReviewerAssignedTime as datetime) 
		,SecondaryReviewCompletedTime = CAST(SecondaryReviewerCompletedTime as datetime)
		,SecondaryReviewerCode = CAST(SecondaryReviewerCode as varchar(50))
		,ClassificationGrade
		,RequiresFurtherInvestigation 
		,QuestionNo = 
			case 
				when ReviewStatusCode = 0 then 0
				else row_number() over (partition by SourceUniqueID order by SourceUniqueID)
			end
		,Question
		,Response
		,ContextCode = 'CMFT||MORT'
	from
	(
	select
		SourceUniqueID
		,SourcePatientNo
		,EpisodeStartTime
		,APCContextCode = ContextCode
		,FormTypeCode
		,MortalityReviewAdded
		,ReviewStatusCode
		
		,PrimaryReviewerAssignedTime = PRAssignedTime
		,PrimaryReviewerCompletedTime = PRCompletedTime
		,PrimaryReviewerCode = PrimaryReviewerID
		,SecondaryReviewerAssignedTime = SRAssignedTime
		,SecondaryReviewerCompletedTime = SRCompletedTime
		,SecondaryReviewerCode = SecondaryReviewerID
		,ClassificationGrade = cast(ClassificationScore as varchar(max))
		,RequiresFurtherInvestigation = 
					Case
						when DeathExpected = 0 then 1 
						when ClassificationScore in (2,3) then 1
						else 0
					end
		
		,MRSA = IsNull(cast(MRSA as varchar(max)),'')
		,MedicationErrorsReported = IsNull(cast(MedicationErrorsReported as varchar(max)),'')
		,CriticalIncident = IsNull(cast(CriticalIncident as varchar(max)),'')
		,CauseOfDeath1 = IsNull(cast(CauseOfDeath1 as varchar(max)),'')
		,CauseOfDeath2 = IsNull(cast(CauseOfDeath2 as varchar(max)),'')
		,IntensiveCareWithdrawn = cast(IntensiveCareWithdrawn as varchar(max))
		,LOTADNR = cast(LOTADNR as varchar(max))
		,ConsultantDiscussion = cast(ConsultantDiscussion as varchar(max))
		,DeathExpected = cast(DeathExpected as varchar(max))
		,DeathPreventable = cast(DeathPreventable as varchar(max))
		,PalliativeCare = cast(PalliativeCare as varchar(max))
		,PostMortem = cast(PostMortem as varchar(max))
		,BereavementAppointmentDate = cast(BereavementAppointmentDate as varchar(max))
		,AppointmentAttended = cast(AppointmentAttended as varchar(max))
		,FollowUp = cast(FollowUp as varchar(max))
		,AdditionalInformation = cast(AdditionalInformation as varchar(max))
		,SignificantIssues = cast(SignificantIssues as varchar(max))
		,AreasOfNotablePractice = cast(AreasOfNotablePractice as varchar(max))
		,AreasForFurtherDevelopment = cast(AreasForFurtherDevelopment as varchar(max))
		,AdditionalInstructions = cast(AdditionalInstructions as varchar(max))
		,SourceOfAdmission = cast(SourceOfAdmission as varchar(max))
		,Gestation = cast(Gestation as varchar(max))
		,BirthWeight = cast(BirthWeight as varchar(max))
		,DelayInDiagnosis = IsNull(cast(DelayInDiagnosis as varchar(max)),'')
		,DelayInDeliveringCare = cast(DelayInDelivering as varchar(max))
		,PoorCommunication = cast(PoorComms as varchar(max))
		,OrganisationalFailure = cast(OrganisationalFailure as varchar(max))
		,AreasOfConcernNoDiff = cast(AreasofConcernND as varchar(max))
		,AreasOfConcernContributed = cast(AreasofConcernContributed as varchar(max))
		,DoneDifferently = cast(DoneDifferently as varchar(max))
		,DocStandard = cast(DocStandards as varchar(max))
		,GoodQualityDetail = cast(GoodQualityDetails as varchar(max))
		,FurtherComment = cast(FurtherComments as varchar(max)) 
		,CEFactors = cast(CEFactors as varchar(max)) 
		,SecondaryReviewConsultantList = cast(SecondaryReviewConsultantList as varchar(max)) 
		,SecondaryReviewConsultantDiscussion = cast(SecondaryReviewConsultantDiscussion as varchar(max)) 
		,SubmittedDiagnostics = cast(SubmittedDiagnostics as varchar(max)) 
		,SubmittedRecurringThemes = cast(SubmittedRecurringThemes as varchar(max)) 
	from
		[$(Warehouse)].Mortality.Neonatology
	) 
	Mortality

	unpivot
			(
			Response for Question in 
					(
					MRSA
					,MedicationErrorsReported 
					,CriticalIncident 
					,CauseOfDeath1 
					,CauseOfDeath2 
					,IntensiveCareWithdrawn 
					,LOTADNR 
					,ConsultantDiscussion 
					,DeathExpected 
					,DeathPreventable
					,PalliativeCare 
					,PostMortem 
					,BereavementAppointmentDate 
					,AppointmentAttended 
					,FollowUp 
					,AdditionalInformation 
					,SignificantIssues 
					,AreasOfNotablePractice 
					,AreasForFurtherDevelopment 
					,AdditionalInstructions 
					,SourceOfAdmission 
					,Gestation 
					,BirthWeight
					,DelayInDiagnosis 
					,DelayInDeliveringCare 
					,PoorCommunication 
					,OrganisationalFailure 
					,AreasOfConcernNoDiff 
					,AreasOfConcernContributed 
					,DoneDifferently 
					,DocStandard 
					,GoodQualityDetail 
					,FurtherComment 
					,CEFactors 
					,SecondaryReviewConsultantList 
					,SecondaryReviewConsultantDiscussion
					,SubmittedDiagnostics 
					,SubmittedRecurringThemes
					)
			) 
			Response 
		)
		Neonatal





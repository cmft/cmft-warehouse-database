﻿create view [ETL].[TLoadTraAEBaseInvestigation]
as

select
	 Encounter.MergeEncounterRecno
	,Investigation.SequenceNo
	,InvestigationSchemeCode = Investigation.InvestigationSchemeInUse
	,Investigation.InvestigationCode
	,InvestigationTime = Investigation.InvestigationDate
	,ResultTime = Investigation.ResultDate
	,Encounter.ContextCode
from
	[$(TraffordWarehouse)].dbo.AEInvestigation Investigation

inner join AE.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Investigation.AESourceUniqueID
and	Encounter.ContextCode in
	(
	 'TRA||SYM'
	)




﻿





CREATE view [ETL].[TLoadPEXSMSBaseResponse]

as
	
SELECT 
	ResponseRecno = [SMSResponseRecno]
	,[ResponseID]
	,[SurveyTakenID]
	,[SurveyDate]
	,[SurveyTime]
	,SurveyID
	,[LocationID]
	,WardID
	,[QuestionID]
	,[AnswerID]
	,Answer
	,[ResponseDate]
	,[ResponseTime]
	,[ChannelID]
	,[QuestionTypeID]
	,[LocationTypeID]
	,ContextCode = 'CMFT||ENVOY'	      
	,[Created]
	,[Updated]
	,[ByWhom]
 FROM 
	[$(Warehouse)].[PEX].[SMSResponse]








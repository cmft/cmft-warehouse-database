﻿







CREATE view [ETL].[TLoadHRBaseSummary]

as

select
	[SummaryRecno]
	,[OrganisationCode]
	,[WardCode]
	,[CensusDate]
	,[AppraisalRequired]
	,[AppraisalCompleted]
	,[ClinicalMandatoryTrainingRequired]
	,[ClinicalMandatoryTrainingCompleted]
	,[CorporateMandatoryTrainingRequired]
	,[CorporateMandatoryTrainingCompleted]
	,[RecentAppointment]
	,[LongTermAppointment]
	,[SicknessAbsenceFTE]
	,[SicknessEstablishmentFTE]
	,[Headcount3mthRolling]
	,[FTE3mthRolling]
	,[StartersHeadcount3mthRolling]
	,[StartersFTE3mthRolling]
	,[LeaversHeadcount3mthRolling]
	,[LeaversFTE3mthRolling]
	,BudgetWTE
	,ContractedWTE
	,[ClinicalBudgetWTE]
	,[ClinicalContractedWTE]
	,ACAgencySpend
	,[InterfaceCode]
	,ContextCode = 'CMFT||ESREXT'
	,[Created]
	,[Updated]
	,[ByWhom]
	,[HeadcountMonthly]
	,[FTEMonthly]
	,[StartersHeadcountMonthly]
	,[StartersFTEMonthly]
	,[LeaversHeadcountMonthly]
	,[LeaversFTEMonthly]
	,BMERecentAppointment
	,BMELongTermAppointment
	,ESRSIP 
	,GeneralLedgerEst	
	,ESRSIPBand5 
	,GeneralLedgerEstBand5 	
from
	[$(Warehouse)].[HR].[Summary]







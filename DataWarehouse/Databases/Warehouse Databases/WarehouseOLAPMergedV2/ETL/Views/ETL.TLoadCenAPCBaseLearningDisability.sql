﻿


CREATE view [ETL].[TLoadCenAPCBaseLearningDisability]

as

select
	LearningDisabilityRecno
	,SourceUniqueID
	,SourcePatientNo
	,SourceSpellNo
	,AssessmentDate	
	,AssessmentTime	
	,KnownLearningDisability	
	,ReasonableAdjustmentsCarePlan	
	,LDPassport
	,WardCode
	,InterfaceCode
	,ContextCode = 'CEN||BEDMAN'
	,Created
	,Updated
	,ByWhom
from
	[$(Warehouse)].[APC].LearningDisability


﻿
















CREATE view [ETL].[HRG4OPInputFile]

as

/****************************************************************************************
	View : [ETL].[HRG4OPInputFile]
	Description		 : Used in SSIS package "HRG4 OP Grouper Merged.dtsx"

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
	20140502	Paul Egan		Added FY 2013/2014 dummy record.
	20141208	Phil Orrell		Added net change
	20150505	Paul Egan		2016/2017 future booked were not being grouped. Routed to 2014/2015 
								Grouper (for now - logic will need to be changed when moving to new grouper)
*****************************************************************************************/

select --top 1000
	 --STARTAGE =
		--	floor(
		--	datediff(
		--			day
		--		,Encounter.DateOfBirth
		--		,Encounter.AppointmentDate
		--	)/365.25
		--	)

	STARTAGE = [$(CMFT)].Dates.GetAge(
								Encounter.DateOfBirth
								,Encounter.AppointmentDate
								)
	,SEX = NationalSexCode
	,MAINSPEF = Consultant.MainSpecialtyCode
	,TRETSPEF = Specialty.NationalSpecialtyCode
	,FIRSTATT = NationalFirstAttendanceCode
	,OPER_01 = 
	left(
		case
			when Encounter.PrimaryOperationCode = '##'
			then null
			else replace(Encounter.PrimaryOperationCode, '.', '')
		end
		,5
		)
	,OPER_02 = 
	left(
		replace(Encounter.SecondaryProcedureCode1, '.', '')
		,5
		)
	,OPER_03 = 
	left(
		replace(Encounter.SecondaryProcedureCode2, '.', '')
		,5
		)
	,OPER_04 = 
	left(
		replace(Encounter.SecondaryProcedureCode3, '.', '')
		,5
		)
	,OPER_05 = 
	left(
		replace(Encounter.SecondaryProcedureCode4, '.', '')
		,5
		)
	,OPER_06 = 
	left(
		replace(Encounter.SecondaryProcedureCode5, '.', '')
		,5
		)
	,OPER_07 = 
	left(
		replace(Encounter.SecondaryProcedureCode6, '.', '')
		,5
		)
	,OPER_08 = 
	left(
		replace(Encounter.SecondaryProcedureCode7, '.', '')
		,5
		)
	,OPER_09 = 
	left(
		replace(Encounter.SecondaryProcedureCode8, '.', '')
		,5
		)
	,OPER_10 = 
	left(
		replace(Encounter.SecondaryProcedureCode9, '.', '')
		,5
		)
	,OPER_11 = 
	left(
		replace(Encounter.SecondaryProcedureCode10, '.', '')
		,5
		)
	,OPER_12 = 
	left(
		replace(Encounter.SecondaryProcedureCode11, '.', '')
		,5
		)
	,Encounter.MergeEncounterRecno
	,FinancialYear = 
				case
				when Encounter.AppointmentDate >= '20150401' -- using 14/15 grouper for 15/16 (and beyond) data - confirmed by Phil Huitson 7 Apr 2015
				then '2014/2015'
				else AppointmentDate.FinancialYear
				end

from
	OP.BaseEncounter Encounter 

inner join
	OP.BaseEncounterReference Reference
on	Reference.MergeEncounterRecno = Encounter.MergeEncounterRecno

inner join OP.ProcessList
on	ProcessList.MergeRecno = Encounter.MergeEncounterRecno

left outer join
	WH.Calendar AppointmentDate
on	AppointmentDate.DateID = Reference.AppointmentDateID

left outer join
	WH.Specialty
on	Specialty.SourceSpecialtyID = Reference.ReferralSpecialtyID

left outer join
	WH.Consultant
on	Consultant.SourceConsultantID = Reference.ConsultantID

left outer join
	WH.Sex
on	Sex.SourceSexID = Reference.SexID

left outer join
	OP.FirstAttendance
on	FirstAttendance.SourceFirstAttendanceID = Reference.DerivedFirstAttendanceID

where
	AppointmentDate >= '1 apr 2012' -- no groupers installed prior to this

/* Pass dummy records to grouper so it doesn't error if no records passed to it */

union all

select
	STARTAGE = null
	,SEX = null
	,MAINSPEF = null
	,TRETSPEF = null
	,FIRSTATT = null
	,OPER_01 = null
	,OPER_02 = null
	,OPER_03 = null
	,OPER_04 = null
	,OPER_05 = null
	,OPER_06 = null
	,OPER_07 = null
	,OPER_08 = null
	,OPER_09 = null
	,OPER_10 = null
	,OPER_11 = null
	,OPER_12 = null
	,MergeEncounterRecno  = -20122013 -- dummy record
	,FinancialYear = '2012/2013'

/* Added Paul Egan 02/05/2014 */
union all

select
	STARTAGE = null
	,SEX = null
	,MAINSPEF = null
	,TRETSPEF = null
	,FIRSTATT = null
	,OPER_01 = null
	,OPER_02 = null
	,OPER_03 = null
	,OPER_04 = null
	,OPER_05 = null
	,OPER_06 = null
	,OPER_07 = null
	,OPER_08 = null
	,OPER_09 = null
	,OPER_10 = null
	,OPER_11 = null
	,OPER_12 = null
	,MergeEncounterRecno  = -20132014 -- dummy record
	,FinancialYear = '2013/2014'













﻿
CREATE view [ETL].[TLoadCenAPCBaseDiagnosis]
as

select
	 Encounter.MergeEncounterRecno
	,Diagnosis.SequenceNo
	,DiagnosisCode = rtrim(left(Diagnosis.DiagnosisCode,6))
from
	[$(Warehouse)].APC.Diagnosis Diagnosis

inner join APC.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Diagnosis.APCSourceUniqueID
and	Encounter.ContextCode = 'CEN||PAS'
and Encounter.Reportable = 1

union all
	
select
	 Encounter.MergeEncounterRecno
	,SequenceNo = 0
	,DiagnosisCode = rtrim(left(Encounter.PrimaryDiagnosisCode,6))
from
	APC.BaseEncounter Encounter
where
	Encounter.PrimaryDiagnosisCode is not null
and	Encounter.ContextCode = 'CEN||PAS'
and Encounter.Reportable = 1



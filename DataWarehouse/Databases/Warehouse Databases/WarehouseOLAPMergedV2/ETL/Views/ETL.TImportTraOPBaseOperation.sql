﻿



CREATE view [ETL].[TImportTraOPBaseOperation]
as

select
	 Encounter.MergeEncounterRecno
	,Encounter.EncounterRecno
	,Encounter.ContextCode 
	,SequenceNo = cast(Operation.SequenceNo as int) - 1
	,OperationCode =  rtrim(left(Operation.ProcedureCode , 6)) 
	,OperationDate = Operation.ProcedureDate
	
from
	[$(TraffordWarehouse)].dbo.OPProcedure Operation

inner join OP.BaseEncounter Encounter
on	Encounter.SourceUniqueID = Operation.OPSourceUniqueID
and	Encounter.ContextCode = 'TRA||UG'





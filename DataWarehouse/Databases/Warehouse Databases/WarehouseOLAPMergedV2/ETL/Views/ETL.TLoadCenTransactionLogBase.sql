﻿

CREATE view [ETL].[TLoadCenTransactionLogBase] as

select
	 TransactionRecno = TRNID
	,TransactionID
	,ColumnID
	,ActivityID = TransactionTypeID
	,ActivityTypeID = TransactionLogTypeID
	,UserID
	,ActionType
	,TransactionTime
	,DistrictNo
	,SourceSpellNo = EpisodeNumber
	,ActivityTime
	,TransactionSourceEncounterNo = ConsultantEpisodeAtTransaction
	,CurrentSourceEncounterNo = ConsultantEpisodeCurrent
	,NewValue
	,OldValue
	,Created = DateAdded
	,ContextCode = 'CEN||PAS'
from
	[$(PASTransactionLog)].dbo.TransactionBase TransactionBase



﻿





/****************************************************************************************
	View : [ETL].[TLoadCenAPCBaseEncounter]

	Modification History
	====================
	
	Date		Person			Description
	====================================================================================
	?			?				Intial Coding
	29/04/2014	DG/PE			Corrected CASE logic for VTECategoryCode (it was not 
								checking Bedman if PAS Research2 was not coded as 'V...')
	20140731	RR				Added field Reportable = 1 below, so that it can be inserted into the APC.BaseEncounter table with a default of 1
	20150722	RR				Due to Mortality process changing, discussed with DG and agreed to remove from the APC table

*****************************************************************************************/


CREATE view [ETL].[TLoadCenAPCBaseEncounter] as

select
	 Encounter.EncounterRecno
	,Encounter.SourceUniqueID
	,Encounter.SourcePatientNo
	,Encounter.SourceSpellNo
	,Encounter.SourceEncounterNo
	,Encounter.ProviderSpellNo
	,Encounter.PatientTitle
	,Encounter.PatientForename
	,Encounter.PatientSurname
	,Encounter.DateOfBirth
	,Encounter.DateOfDeath

	,Encounter.SexCode
	--,SexCode =
	--	case Encounter.SexCode
	--	when '' then '0'
	--	when 'M' then '1'
	--	when 'F' then '2'
	--	when 'I' then '9'
	--	else '#'
	--	end

	,Encounter.NHSNumber
	,Encounter.Postcode
	,Encounter.PatientAddress1
	,Encounter.PatientAddress2
	,Encounter.PatientAddress3
	,Encounter.PatientAddress4
	,Encounter.DHACode
	,Encounter.EthnicOriginCode
	,Encounter.MaritalStatusCode
	,Encounter.ReligionCode
	,Encounter.DateOnWaitingList
	,Encounter.AdmissionDate
	,Encounter.DischargeDate
	,Encounter.EpisodeStartDate
	,Encounter.EpisodeEndDate
	,Encounter.StartSiteCode
	,Encounter.StartWardTypeCode
	,EndSiteCode = Encounter.EndSiteCode
	,Encounter.EndWardTypeCode
	,Encounter.RegisteredGpCode

	,RegisteredGpPracticeCode =
		coalesce(Encounter.RegisteredGpPracticeCode,Encounter.EpisodicGpPracticeCode, 'V81999')

	,Encounter.SiteCode
	,Encounter.AdmissionMethodCode
	,Encounter.AdmissionSourceCode
	,Encounter.PatientClassificationCode
	,Encounter.ManagementIntentionCode
	,Encounter.DischargeMethodCode
	,Encounter.DischargeDestinationCode
	,Encounter.AdminCategoryCode
	,Encounter.ConsultantCode
	,Encounter.SpecialtyCode
	,Encounter.LastEpisodeInSpellIndicator
	,Encounter.FirstRegDayOrNightAdmit
	,Encounter.NeonatalLevelOfCare
	,Encounter.PASHRGCode

	,Encounter.PrimaryDiagnosisCode

	--Gareth Cunnah removed as cutting off codes 20/08/2012
--	,PrimaryDiagnosisCode =
	--	coalesce(rtrim(left(Encounter.PrimaryDiagnosisCode, 5)), '##') 

	,Encounter.SubsidiaryDiagnosisCode
	,Encounter.SecondaryDiagnosisCode1
	,Encounter.SecondaryDiagnosisCode2
	,Encounter.SecondaryDiagnosisCode3
	,Encounter.SecondaryDiagnosisCode4
	,Encounter.SecondaryDiagnosisCode5
	,Encounter.SecondaryDiagnosisCode6
	,Encounter.SecondaryDiagnosisCode7
	,Encounter.SecondaryDiagnosisCode8
	,Encounter.SecondaryDiagnosisCode9
	,Encounter.SecondaryDiagnosisCode10
	,Encounter.SecondaryDiagnosisCode11
	,Encounter.SecondaryDiagnosisCode12
	,Encounter.SecondaryDiagnosisCode13

	,PrimaryProcedureCode = Encounter.PrimaryOperationCode

	--Gareth Cunnah removed as cutting off codes 20/08/2012	
	--,PrimaryProcedureCode =
	--	coalesce(rtrim(left(Encounter.PrimaryOperationCode, 5)), '##')

	,PrimaryProcedureDate = Encounter.PrimaryOperationDate
	,SecondaryProcedureCode1 = Encounter.SecondaryOperationCode1
	,SecondaryProcedureDate1 = Encounter.SecondaryOperationDate1
	,SecondaryProcedureCode2 = Encounter.SecondaryOperationCode2
	,SecondaryProcedureDate2 = Encounter.SecondaryOperationDate2
	,SecondaryProcedureCode3 = Encounter.SecondaryOperationCode3
	,SecondaryProcedureDate3 = Encounter.SecondaryOperationDate3
	,SecondaryProcedureCode4 = Encounter.SecondaryOperationCode4
	,SecondaryProcedureDate4 = Encounter.SecondaryOperationDate4
	,SecondaryProcedureCode5 = Encounter.SecondaryOperationCode5
	,SecondaryProcedureDate5 = Encounter.SecondaryOperationDate5
	,SecondaryProcedureCode6 = Encounter.SecondaryOperationCode6
	,SecondaryProcedureDate6 = Encounter.SecondaryOperationDate6
	,SecondaryProcedureCode7 = Encounter.SecondaryOperationCode7
	,SecondaryProcedureDate7 = Encounter.SecondaryOperationDate7
	,SecondaryProcedureCode8 = Encounter.SecondaryOperationCode8
	,SecondaryProcedureDate8 = Encounter.SecondaryOperationDate8
	,SecondaryProcedureCode9 = Encounter.SecondaryOperationCode9
	,SecondaryProcedureDate9 = Encounter.SecondaryOperationDate9
	,SecondaryProcedureCode10 = Encounter.SecondaryOperationCode10
	,SecondaryProcedureDate10 = Encounter.SecondaryOperationDate10
	,SecondaryProcedureCode11 = Encounter.SecondaryOperationCode11
	,SecondaryProcedureDate11 = Encounter.SecondaryOperationDate11

	,Encounter.OperationStatusCode
	,Encounter.ContractSerialNo
	,Encounter.CodingCompleteDate
	,Encounter.PurchaserCode
	,Encounter.ProviderCode
	,Encounter.EpisodeStartTime
	,Encounter.EpisodeEndTime
	,Encounter.RegisteredGdpCode
	,Encounter.EpisodicGpCode
	,Encounter.InterfaceCode
	,Encounter.CasenoteNumber
	,Encounter.NHSNumberStatusCode
	,Encounter.AdmissionTime
	,Encounter.DischargeTime
	,Encounter.TransferFrom
	,Encounter.DistrictNo
	,Encounter.ExpectedLOS
	,Encounter.MRSAFlag
	,Encounter.RTTPathwayID
	,Encounter.RTTPathwayCondition
	,Encounter.RTTStartDate
	,Encounter.RTTEndDate
	,Encounter.RTTSpecialtyCode
	,Encounter.RTTCurrentProviderCode
	,Encounter.RTTCurrentStatusCode
	,Encounter.RTTCurrentStatusDate
	,Encounter.RTTCurrentPrivatePatientFlag
	,Encounter.RTTOverseasStatusFlag
	,Encounter.RTTPeriodStatusCode

	,IntendedPrimaryProcedureCode =
		Encounter.IntendedPrimaryOperationCode

	,Encounter.Operation
	,Encounter.Research1
	,Encounter.CancelledElectiveAdmissionFlag
	,Encounter.LocalAdminCategoryCode

	,EpisodicGpPracticeCode =
		coalesce(Encounter.EpisodicGpPracticeCode, Encounter.RegisteredGpPracticeCode, 'V81999')
	,Encounter.PCTCode
	,Encounter.LocalityCode

	,AgeCode =
	case
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) is null then 'Age Unknown'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 0 then '00 Days'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) = 1 then '01 Day'
	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 1 and
	datediff(day, DateOfBirth, Encounter.EpisodeStartDate) <= 28 then
	    right('0' + Convert(Varchar,datediff(day, DateOfBirth, Encounter.EpisodeStartDate)), 2) + ' Days'

	when datediff(day, DateOfBirth, Encounter.EpisodeStartDate) > 28 and
	datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end
	     < 1 then 'Between 28 Days and 1 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end = 1
	    then '01 Month'
	when datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end > 1 and
	 datediff(month, DateOfBirth, Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end <= 23
	then
	right('0' + Convert(varchar,datediff(month, DateOfBirth,Encounter.EpisodeStartDate) -
	case  when datepart(day, DateOfBirth) > datepart(day, Encounter.EpisodeStartDate) then 1 else 0 end), 2) + ' Months'
	when datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	) > 99 then '99+'
	else right('0' + convert(varchar, datediff(yy, DateOfBirth, Encounter.EpisodeStartDate) - 
	(
	case 
	when	(datepart(m, DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
	or
		(
			datepart(m, DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
		And	datepart(d, DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
		) then 1 else 0 end
	)), 2) + ' Years'

	end

	,HRGCode = null --HRG4Encounter.HRGCode

	,SpellHRGCode = null --HRG4Spell.HRGCode

	,CategoryCode =
		case
		when Encounter.ManagementIntentionCode = 'I' then '10'
		when Encounter.ManagementIntentionCode = 'D' then '20'
		when Encounter.ManagementIntentionCode = 'R' then '24'
		else '28'
		end

	,LOE =
		datediff(day, Encounter.EpisodeStartDate, Encounter.EpisodeEndDate)

	,LOS =
	datediff(
		 day
		,Encounter.AdmissionDate
		,coalesce(
			 Encounter.DischargeDate
			,(
			select
				max(DischargeDate)
			from
				[$(Warehouse)].APC.Encounter
			)
		)
	)

	,Cases = 1

	,FirstEpisodeInSpellIndicator =
		case
		when not exists
		(
		select
			1
		from
			[$(Warehouse)].APC.Encounter Previous
		where
			Previous.ProviderSpellNo = Encounter.ProviderSpellNo
		and	(
				Previous.EpisodeStartTime < Encounter.EpisodeStartTime
			or	(
					Previous.EpisodeStartTime = Encounter.EpisodeStartTime
				and	Previous.SourceEncounterNo < Encounter.SourceEncounterNo
				)
			)
		)
		then 1
		else 0
		end

	,RTTActivity = null
	--	convert(
	--		bit
	--		,case
	--		when
	--			Encounter.AdmissionTime = Encounter.EpisodeStartTime
	--		and	Encounter.AdmissionMethodCode in ('BA', 'BL', 'WL')
	--		and	Encounter.SpecialtyCode not in 
	--			(
	--			select
	--				SpecialtyCode
	--			from
	--				RTT.dbo.ExcludedSpecialty
	--			)

	--		-- 'GENE' ?
	--		and	Encounter.ConsultantCode <> 'FRAC'

	--		--remove CEA admissions
	--		and	Encounter.CancelledElectiveAdmissionFlag <> 1

	--		--remove ICAT
	--		and	Encounter.SiteCode != 'FMSK'

	--		and	not exists
	--			(
	--			select
	--				1
	--			from
	--				RTT.dbo.RTTClockStart ClockChangeReason
	--			where
	--				ClockChangeReason.SourcePatientNo = Encounter.SourcePatientNo
	--			and	ClockChangeReason.SourceEntityRecno = Encounter.SourceSpellNo
	--			and	coalesce(ClockChangeReason.ClockStartReasonCode, '') = 'NOP' --Not an 18 week pathway
	--			)

	----check for NOPs on previous referral
	--		and	not exists
	--			(
	--			select
	--				1
	--			from
	--				WH.RF.Encounter Referral

	--			inner join RTT.dbo.RTTOPClockStop ClockChangeReason
	--			on	ClockChangeReason.SourcePatientNo = Referral.SourcePatientNo
	--			and	ClockChangeReason.SourceEntityRecno = Referral.SourceEncounterNo
	--			and	(
	--					ClockChangeReason.ClockStopReasonCode = 'NOP' --Not an 18 week pathway
	--				or	ClockChangeReason.ClockStartReasonCode = 'NOP' --Not an 18 week pathway
	--				)

	--			where
	--				Referral.EncounterRecno = Encounter.ReferralEncounterRecno	
	--			)
	--		then 1
	--		else 0
	--		end
	--	)


	,RTTBreachStatusCode =
		case
		when Encounter.RTTBreachDate is null
		then 'U'
		when Encounter.RTTBreachDate < Encounter.AdmissionDate
		then 'B'
		else 'N'
		end

	,DiagnosticProcedure =
		convert(
			bit
			,case
			when RTTStatus.ClockStopFlag = 1 then 0
			when Encounter.RTTBreachDate is null then 0 -- unknown clock starts are counted
			when DiagnosticProcedure.ProcedureCode is null
			then 0
			else 1
			end
		)

	,RTTTreated =
		case
		when RTTStatus.ClockStopFlag = 1 then 1
		when Encounter.RTTBreachDate is null then 1 -- unknown clock starts are counted
		else 0
		end

	,EpisodeStartTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, Encounter.EpisodeStartTime), 0)
				,Encounter.EpisodeStartTime
			)
			,-1
		)

	,EpisodeEndTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, EpisodeEndTime), 0)
				,EpisodeEndTime
			)
			,-1
		)

	,AdmissionTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, AdmissionTime), 0)
				,AdmissionTime
			)
			,-1
		)

	,DischargeTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, DischargeTime), 0)
				,DischargeTime
			)
			,-1
		)


	,Encounter.StartDirectorateCode
	,Encounter.EndDirectorateCode
	,ResidencePCTCode = ResidencePCTCode

	,ISTAdmissionSpecialtyCode
	,ISTAdmissionDemandTime
	,ISTDischargeTime

	,ISTAdmissionDemandTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, ISTAdmissionDemandTime), 0)
				,ISTAdmissionDemandTime
			)
			,-1
		)

	,ISTDischargeTimeOfDay =
		coalesce(
			datediff(
				 minute
				,dateadd(day, datediff(day, 0, ISTDischargeTime), 0)
				,ISTDischargeTime
			)
			,-1
		)
	,Encounter.PatientCategoryCode
	,Encounter.Research2
	,Encounter.ClinicalCodingStatus
	,Encounter.ClinicalCodingCompleteDate
	,Encounter.ClinicalCodingCompleteTime

	,Encounter.ReferredByCode
	,Encounter.ReferrerCode
	,Encounter.DecidedToAdmitDate

	,Encounter.PsychiatricPatientStatusCode
	,Encounter.LegalStatusClassificationCode
	,Encounter.ReferringConsultantCode
	,Encounter.DurationOfElectiveWait
	,Encounter.CarerSupportIndicator
	,Encounter.DischargeReadyDate
	,Encounter.ContextCode
	,Encounter.EddCreatedTime
	,Encounter.ExpectedDateOfDischarge
	,Encounter.EddCreatedByConsultantFlag
	,Encounter.EddInterfaceCode

	,VTECategoryCode =
		case

		--when left(Encounter.Research2, 1) <> 'V'		/* Logic corrected 29/04/2014 DG & PE */
		--then null

		when datediff(yy, Encounter.DateOfBirth, Encounter.EpisodeStartDate) - 
			(
			case 
			when
				(datepart(m, Encounter.DateOfBirth) > datepart(m, Encounter.EpisodeStartDate)) 
			or
				(
					datepart(m, Encounter.DateOfBirth) = datepart(m, Encounter.EpisodeStartDate) 
				and	datepart(d, Encounter.DateOfBirth) > datepart(d, Encounter.EpisodeStartDate)
				)
			then 1
			else 0
			end
			) < 18

		then null

		when Encounter.PatientCategoryCode = 'RD'
		then null

		when left(Encounter.Research2, 1) = 'V'
		and	charindex('C', Encounter.Research2) > 1			/* Logic corrected 29/04/2014 DG & PE */
		then 'C'
		
		
		/* Scan for completed VTE assessment only in BEDMAN - DG 16.1.14 - Requested by GS */
		
		when
			exists
				(
				select
					1
				from
					[$(Warehouse)].Bedman.Event
				where
					Event.EventTypeCode = 4 -- VTE Assessment
				and	Event.EventDate != '1 jan 1900' -- Completed VTE Assessment only (does not take into account not requireds)
				and	Encounter.SourcePatientNo = Event.SourcePatientNo
				and	Encounter.AdmissionTime = Event.AdmissionTime	
				)	
					
		then 'C'

		when left(Encounter.Research2, 1) = 'V'			/* Logic corrected 29/04/2014 DG & PE */
		and charindex('I', Encounter.Research2) > 1
		then 'I'

		when left(Encounter.Research2, 1) = 'V'			/* Logic corrected 29/04/2014 DG & PE */
		and charindex('M', Encounter.Research2) > 1
		then 'M'
		
		else null

		end

	,Ambulatory = null

	,PseudoPostcode = Encounter.PseudoPostCode

	,CCGCode 
	,GpCodeAtDischarge
	,GpPracticeCodeAtDischarge
	,PostcodeAtDischarge
	,Encounter.ResidenceCCGCode
	,Reportable
	,CharlsonIndex = coalesce(CharlsonIndex.CharlsonIndex, 0)
from
	(
	select
		 Encounter.EncounterRecno
		,Encounter.SourceUniqueID
		,Encounter.SourcePatientNo
		,Encounter.SourceSpellNo
		,Encounter.SourceEncounterNo
		,Encounter.ProviderSpellNo
		,PatientTitle = 
				coalesce(
					 Patient.Title
					,Encounter.PatientTitle
			)
		,PatientForename = 
				coalesce(
					 Patient.Forenames
					,Encounter.PatientForename
			)
		,PatientSurname = 
				coalesce(
					 Patient.Surname
					,Encounter.PatientSurname
			)
		,DateOfBirth = 
				coalesce(
					 Patient.DateOfBirth
					,Encounter.DateOfBirth
			)
		,DateOfDeath =  
				coalesce(
					 Patient.DateOfDeath
					,Encounter.DateOfDeath
			)

		,SexCode = 
			coalesce(
					 Patient.SexCode
					,Encounter.SexCode
			)
		,NHSNumber =
			replace(
				coalesce(
					Patient.NHSNumber

					,case
					when
						(
							Encounter.NHSNumber = '1234567899'  
						or	rtrim(Encounter.NHSNumber) = ''
						or Encounter.NHSNumber = 'REMOVE'
						or left(Encounter.NHSNumber,4) = 'SLF?' 
						or left(Encounter.NHSNumber,4) = 'MAN?'
						)
					then null
					else Encounter.NHSNumber
					end
				)
				,' '
				,''
			)
		,Postcode  = 
			coalesce(
					 Postcode.Postcode
					,Encounter.Postcode
					) 
		,PatientAddress1 =
			coalesce(
					 patientaddressatdischarge.AddressLine1
					,Encounter.PatientAddress1
					) 
		,PatientAddress2 =
			coalesce(
					 patientaddressatdischarge.AddressLine2
					,Encounter.PatientAddress2
					) 			
		,PatientAddress3 =
			coalesce(
					 patientaddressatdischarge.AddressLine3
					,Encounter.PatientAddress3
					) 	
		,PatientAddress4 =
			coalesce(
					 patientaddressatdischarge.AddressLine4
					,Encounter.PatientAddress4
					) 	
		,Encounter.DHACode
		,EthnicOriginCode =	
			coalesce(
					 Patient.EthnicOriginCode
					,Encounter.EthnicOriginCode
			)
		,MaritalStatusCode =	
			coalesce(
					 Patient.MaritalStatusCode
					,Encounter.MaritalStatusCode
			)
		,ReligionCode = 
			coalesce(
					 Patient.ReligionCode
					,Encounter.ReligionCode
			)
		,Encounter.DateOnWaitingList
		,Encounter.AdmissionDate
		,Encounter.DischargeDate
		,Encounter.EpisodeStartDate
		,Encounter.EpisodeEndDate
		,Encounter.StartSiteCode
		,Encounter.StartWardTypeCode
		,Encounter.EndSiteCode
		,Encounter.EndWardTypeCode
		,RegisteredGpCode = Gp.NationalCode
		,Encounter.RegisteredGpPracticeCode
		,RegisteredGpPracticeAtAdmission = RegisteredGpPracticeAtAdmission.GpPracticeCode
		,RegisteredGpPracticeAtDischarge = RegisteredGpPracticeAtDischarge.GpPracticeCode
		,Encounter.SiteCode
		,Encounter.AdmissionMethodCode
		,Encounter.AdmissionSourceCode
		,Encounter.PatientClassificationCode
		,Encounter.ManagementIntentionCode
		,Encounter.DischargeMethodCode
		,Encounter.DischargeDestinationCode
		,Encounter.AdminCategoryCode
		,Encounter.ConsultantCode
		,Encounter.SpecialtyCode
		,Encounter.LastEpisodeInSpellIndicator
		,Encounter.FirstRegDayOrNightAdmit
		,Encounter.NeonatalLevelOfCare
		,Encounter.IsWellBabyFlag
		,Encounter.PASHRGCode
		,Encounter.ClinicalCodingStatus
		,Encounter.ClinicalCodingCompleteDate
		,Encounter.ClinicalCodingCompleteTime
		,Encounter.PrimaryDiagnosisCode
		,Encounter.PrimaryDiagnosisDate
		,Encounter.SubsidiaryDiagnosisCode
		,Encounter.SecondaryDiagnosisCode1
		,Encounter.SecondaryDiagnosisCode2
		,Encounter.SecondaryDiagnosisCode3
		,Encounter.SecondaryDiagnosisCode4
		,Encounter.SecondaryDiagnosisCode5
		,Encounter.SecondaryDiagnosisCode6
		,Encounter.SecondaryDiagnosisCode7
		,Encounter.SecondaryDiagnosisCode8
		,Encounter.SecondaryDiagnosisCode9
		,Encounter.SecondaryDiagnosisCode10
		,Encounter.SecondaryDiagnosisCode11
		,Encounter.SecondaryDiagnosisCode12
		,Encounter.SecondaryDiagnosisCode13
		,Encounter.PrimaryOperationCode
		,Encounter.PrimaryOperationDate
		,Encounter.SecondaryOperationCode1
		,Encounter.SecondaryOperationDate1
		,Encounter.SecondaryOperationCode2
		,Encounter.SecondaryOperationDate2
		,Encounter.SecondaryOperationCode3
		,Encounter.SecondaryOperationDate3
		,Encounter.SecondaryOperationCode4
		,Encounter.SecondaryOperationDate4
		,Encounter.SecondaryOperationCode5
		,Encounter.SecondaryOperationDate5
		,Encounter.SecondaryOperationCode6
		,Encounter.SecondaryOperationDate6
		,Encounter.SecondaryOperationCode7
		,Encounter.SecondaryOperationDate7
		,Encounter.SecondaryOperationCode8
		,Encounter.SecondaryOperationDate8
		,Encounter.SecondaryOperationCode9
		,Encounter.SecondaryOperationDate9
		,Encounter.SecondaryOperationCode10
		,Encounter.SecondaryOperationDate10
		,Encounter.SecondaryOperationCode11
		,Encounter.SecondaryOperationDate11
		,Encounter.OperationStatusCode
		,ContractSerialNo =
				CASE
					WHEN AdminCategoryCode = '02' THEN 'YPPPAY'
					WHEN ContractSerialNo IS NULL 
					THEN
					 rtrim(
							coalesce(	
									 Practice.ParentOrganisationCode
									,Postcode.PCTCode
									,'5NT'
							) 
					) + '00A'
				ELSE ContractSerialNo
				END
		,Encounter.CodingCompleteDate
		,PurchaserCode = 
				CASE
					WHEN AdminCategoryCode = '02' THEN 'VPP00'
					ELSE
						coalesce(
							 PCT.OrganisationCode -- joined from PurchaserCode
							,Practice.ParentOrganisationCode
							,Postcode.PCTCode
							,'5NT'
						)	
				END
		,Encounter.ProviderCode
		,Encounter.EpisodeStartTime
		,Encounter.EpisodeEndTime
		,Encounter.RegisteredGdpCode
		,Encounter.EpisodicGpCode
		,Encounter.InterfaceCode
		,Encounter.CasenoteNumber
		,NHSNumberStatusCode =
			coalesce(
				 Patient.NHSNumberStatusId
				,case
				when 
					(
						Encounter.NHSNumber = '1234567899'  
					or	Encounter.NHSNumber is null
					or	rtrim(Encounter.NHSNumber) = ''
					or Encounter.NHSNumber = 'REMOVE'
					or left(Encounter.NHSNumber,4) = 'SLF?'
					or left(Encounter.NHSNumber,4) = 'MAN?' 
					)			
				then 'RT'

				when 
					Encounter.NHSNumber is not null
				then 'NP'

				else coalesce(
						 Encounter.NHSNumberStatusCode
						,'RT'
					)
				end

				,'RT'
			)
		,Encounter.AdmissionTime
		,Encounter.DischargeTime
		,Encounter.TransferFrom
		,DistrictNo  = 
			coalesce(
					 Patient.DistrictNo
					,Encounter.DistrictNo
			)
		,Encounter.ExpectedLOS
		,Encounter.MRSAFlag
		,Encounter.RTTPathwayID
		,Encounter.RTTPathwayCondition
		,Encounter.RTTStartDate
		,Encounter.RTTEndDate
		,Encounter.RTTSpecialtyCode
		,RTTCurrentProviderCode = 
			Provider.NationalCode
		,Encounter.RTTCurrentStatusCode
		,Encounter.RTTCurrentStatusDate
		,Encounter.RTTCurrentPrivatePatientFlag
		,Encounter.RTTOverseasStatusFlag
		,Encounter.RTTPeriodStatusCode
		,Encounter.IntendedPrimaryOperationCode
		,Encounter.Operation
		,Encounter.Research1
		,Encounter.CancelledElectiveAdmissionFlag
		,Encounter.LocalAdminCategoryCode
		,Encounter.EpisodicGpPracticeCode
		,Encounter.PCTCode
		,Encounter.LocalityCode
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
		,Encounter.ClockStartDate
		,Encounter.BreachDate
		,Encounter.RTTBreachDate
		,Encounter.NationalBreachDate
		,Encounter.NationalDiagnosticBreachDate
		,Encounter.SocialSuspensionDays
		,Encounter.BreachTypeCode
		,Encounter.ReferralEncounterRecno
		,Encounter.OperationDetailSourceUniqueID
		,Encounter.Research2
		,Encounter.AdmissionDivisionCode
		,Encounter.DischargeDivisionCode
		,Encounter.PatientCategoryCode
		,Encounter.TheatrePatientBookingKey
		,Encounter.StartDirectorateCode
		,Encounter.EndDirectorateCode
		,Encounter.DischargeReadyDate
		,Encounter.DischargeWaitReasonCode
		,Encounter.ReferredByCode
		,ReferrerCode =
			case
				when ReferredByCode = 'GP'
				then GPReferrerCode.NationalCode
				when ReferredByCode = 'GDP'
				then GDPReferrerCode.NationalCode
				when ReferredByCode = 'CON'
				then ReferrerCode
			end

		,Encounter.DecidedToAdmitDate
		,Encounter.ResidencePCTCode
		,Encounter.WaitingListCode
		,Encounter.ISTAdmissionSpecialtyCode
		,Encounter.ISTAdmissionDemandTime
		,Encounter.ISTDischargeTime
		,Encounter.FirstEpisodeInSpellIndicator
		,PsychiatricPatientStatusCode = 
				(
				select
					PsychiatricPatientStatusCode
				from
					[$(Warehouse)].APC.LegalStatus
				where
					LegalStatus.SourceSpellNo = Encounter.SourceSpellNo
				and	LegalStatus.SourcePatientNo = Encounter.SourcePatientNo
				and	LegalStatus.StartTime <= Encounter.EpisodeEndTime
				and	LegalStatus.PsychiatricPatientStatusCode is not null
				and	not exists
					(
					select
						1
					from
						[$(Warehouse)].APC.LegalStatus Previous
					where
						Previous.SourceSpellNo = Encounter.SourceSpellNo
					and	Previous.SourcePatientNo = Encounter.SourcePatientNo
					and	Previous.StartTime <= Encounter.EpisodeEndTime
					and	Previous.PsychiatricPatientStatusCode is not null
					and	(
							Previous.StartTime > LegalStatus.StartTime
						or	(
								Previous.StartTime = LegalStatus.StartTime
							and	Previous.SourceUniqueID > LegalStatus.SourceUniqueID
							)
						)
					)
				)
		,LegalStatusClassificationCode = 
			(
			select
				LegalStatusCode
			from
				[$(Warehouse)].APC.LegalStatus
			where
				LegalStatus.SourceSpellNo = Encounter.SourceSpellNo
			and	LegalStatus.SourcePatientNo = Encounter.SourcePatientNo
			and	LegalStatus.StartTime <= Encounter.EpisodeEndTime
			and	not exists
				(
				select
					1
				from
					[$(Warehouse)].APC.LegalStatus Previous
				where
					Previous.SourceSpellNo = Encounter.SourceSpellNo
				and	Previous.SourcePatientNo = Encounter.SourcePatientNo
				and	Previous.StartTime <= Encounter.EpisodeEndTime
				and	(
						Previous.StartTime > LegalStatus.StartTime
					or	(
							Previous.StartTime = LegalStatus.StartTime
						and	Previous.SourceUniqueID > LegalStatus.SourceUniqueID
						)
					)
				)
			)
		,ReferringConsultantCode =
			case
				when ReferredByCode = 'CON'
				then ReferrerCode
				else null
			end
		,DurationOfElectiveWait = 
				datediff(day, Encounter.DecidedToAdmitDate, Encounter.AdmissionDate) -
				coalesce(
					(
					select
						sum(datediff(day, Suspension.SuspensionStartDate, Suspension.SuspensionEndDate))
					from
						[$(Warehouse)].APC.Suspension Suspension
					where
						Suspension.SourcePatientNo = Encounter.SourcePatientNo
					AND Suspension.SourceEncounterNo =  Encounter.SourceEncounterNo
					)
					,0
				)
		,CarerSupportIndicator = 
			Encounter.CarerSupportIndicator
		,EddCreatedTime = 
			ExpectedLOS.EddCreatedTime
		,ExpectedDateOfDischarge = 
			dateadd(d , ExpectedLOS.ExpectedLOS , Encounter.AdmissionDate)
		,EddCreatedByConsultantFlag = 
			NULL
		,EddInterfaceCode = 
			case when EddCreatedTime is not null then
				case 
				when ExpectedLOS.EddInterfaceCode = 'Bedman' then 'Bed'
				when ExpectedLOS.EddInterfaceCode = 'Inquire' then 'PAS'
				else ExpectedLOS.EddInterfaceCode
				end
			end
		,ContextCode = 'CEN||PAS'
		,PseudoPostCode

		,Encounter.CCGCode 
		,Encounter.GpCodeAtDischarge
		,Encounter.GpPracticeCodeAtDischarge
		,Encounter.PostcodeAtDischarge		

		,ResidenceCCGCode = EpisodicPostcode.CCGCode
		,Reportable = cast(1 as bit)
		
	from
		[$(Warehouse)].APC.Encounter

	left outer join [$(Warehouse)].PAS.Patient Patient
		ON Patient.SourcePatientNo = Encounter.SourcePatientNo

	left outer join [$(Warehouse)].PAS.Gp
		ON Gp.GpCode = Encounter.RegisteredGpCode

	left outer join [$(Warehouse)].PAS.Gp GPReferrerCode
		ON GPReferrerCode.GpCode = Encounter.ReferrerCode
		AND Encounter.ReferredByCode = 'GP'

	left outer join [$(Warehouse)].PAS.Gdp GDPReferrerCode
		ON GDPReferrerCode.GdpCode = Encounter.ReferrerCode
		AND Encounter.ReferredByCode = 'GDP'

	left outer join [$(Warehouse)].PAS.PatientGp RegisteredGpPracticeAtAdmission
		ON RegisteredGpPracticeAtAdmission.SourcePatientNo = Encounter.SourcePatientNo
		AND Encounter.AdmissionDate BETWEEN RegisteredGpPracticeAtAdmission.EffectiveFromDate AND coalesce(RegisteredGpPracticeAtAdmission.EffectiveToDate, getdate())

	left outer join [$(Warehouse)].PAS.PatientGp RegisteredGpPracticeAtDischarge
		ON RegisteredGpPracticeAtDischarge.SourcePatientNo = Encounter.SourcePatientNo
		AND Encounter.DischargeDate BETWEEN RegisteredGpPracticeAtDischarge.EffectiveFromDate AND coalesce(RegisteredGpPracticeAtDischarge.EffectiveToDate, getdate())

	left outer join [$(Organisation)].dbo.PCT PCT
		on left(Encounter.PurchaserCode, 3) = PCT.OrganisationCode

	left outer join [$(Warehouse)].PAS.Provider Provider
		ON Provider.ProviderCode = Encounter.RTTCurrentProviderCode

	left outer join [$(Warehouse)].PAS.PatientAddress patientaddressatdischarge
		on patientaddressatdischarge.SourcePatientNo = Encounter.SourcePatientNo
		and Encounter.DischargeDate between patientaddressatdischarge.EffectiveFromDate and coalesce(patientaddressatdischarge.EffectiveToDate, getdate())

	left outer join [$(Organisation)].dbo.Postcode Postcode on -- now linked to address at time of discharge (DG 19/06/2012 agreed with Tim)
			Postcode.Postcode = 
				case
				when len(patientaddressatdischarge.Postcode) = 8 then patientaddressatdischarge.Postcode
				else left(patientaddressatdischarge.Postcode, 3) + ' ' + right(patientaddressatdischarge.Postcode, 4) 
				end

	left outer join [$(Organisation)].dbo.Practice Practice
		ON	Practice.OrganisationCode = RegisteredGpPracticeAtDischarge.GpPracticeCode

	left outer join 
		(
		select distinct
			 SourcePatientNo
			,SourceSpellNo
			,EddCreatedTime
			,ExpectedLOS
			,EddInterfaceCode = 
				coalesce(
					 ModifiedFromSystem
					,SourceSystem
				)
		from
			[$(Warehouse)].APC.ExpectedLOS 
		where 
			ExpectedLOS.ArchiveFlag = 'N'
		)ExpectedLOS
		on ExpectedLOS.SourcePatientNo = Encounter.SourcePatientNo
	AND ExpectedLOS.SourceSpellNo = Encounter.SourceSpellNo

	left outer join [$(Organisation)].dbo.CCGPostcode EpisodicPostcode on
			EpisodicPostcode.Postcode = 
				case
				when len(Encounter.Postcode) = 8 then Encounter.Postcode
				else left(Encounter.Postcode, 3) + ' ' + right(Encounter.Postcode, 4) 
				end

	) Encounter

--left join [$(Warehouse)].APC.HRG4Encounter HRG4Encounter
--on	HRG4Encounter.EncounterRecno = Encounter.EncounterRecno

--left join [$(Warehouse)].APC.HRG4Spell HRG4Spell
--on	HRG4Spell.EncounterRecno = Encounter.EncounterRecno

left join [$(Warehouse)].dbo.DiagnosticProcedure
on	DiagnosticProcedure.ProcedureCode = Encounter.PrimaryOperationCode

left join [$(Warehouse)].PAS.RTTStatus RTTStatus
on    RTTStatus.RTTStatusCode = Encounter.RTTPeriodStatusCode

left join
	(
	select
		Diagnosis.APCSourceUniqueID
		,CharlsonIndex = sum(cast(Charlson.NewWeight as int))
	from
		[$(Warehouse)].APC.Diagnosis

	inner join [$(Warehouse)].WH.CharlsonIndex Charlson
	on	left(Diagnosis.DiagnosisCode, 6) = Charlson.DiagnosisCode

	where
		not exists
				(
				select
					1
				from
					[$(Warehouse)].APC.Diagnosis DiagnosisLater
				inner join [$(Warehouse)].WH.CharlsonIndex CharlsonLater
				on	left(DiagnosisLater.DiagnosisCode, 6) = CharlsonLater.DiagnosisCode		
	
				where
					DiagnosisLater.SequenceNo > Diagnosis.SequenceNo	
				and DiagnosisLater.APCSourceUniqueID = Diagnosis.APCSourceUniqueID
				and CharlsonLater.CharlsonConditionCode = Charlson.CharlsonConditionCode			
				)
	group by
		APCSourceUniqueID
		
	) CharlsonIndex
	on	CharlsonIndex.APCSourceUniqueID = Encounter.SourceUniqueID


where
	Encounter.EpisodeEndDate >= '1 Apr 2009'
or	Encounter.EpisodeEndDate is null








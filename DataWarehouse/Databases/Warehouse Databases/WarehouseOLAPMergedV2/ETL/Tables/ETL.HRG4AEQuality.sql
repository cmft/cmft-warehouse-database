﻿CREATE TABLE [ETL].[HRG4AEQuality] (
    [FinancialYear]   VARCHAR (10)  NULL,
    [RowNo]           INT           NULL,
    [SequenceNo]      INT           NULL,
    [QualityTypeCode] VARCHAR (50)  NULL,
    [QualityCode]     VARCHAR (50)  NULL,
    [QualityMessage]  VARCHAR (255) NULL
);


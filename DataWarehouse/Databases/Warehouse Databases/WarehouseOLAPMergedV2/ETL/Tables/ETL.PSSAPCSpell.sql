﻿CREATE TABLE [ETL].[PSSAPCSpell] (
    [FinancialYear]                      VARCHAR (10) NULL,
    [RowNo]                              INT          NULL,
    [NationalProgrammeOfCareServiceCode] VARCHAR (50) NULL,
    [ServiceLineCode]                    VARCHAR (50) NULL
);


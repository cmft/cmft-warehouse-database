﻿CREATE TABLE [ETL].[HRG4APCSpell] (
    [FinancialYear]          VARCHAR (10) NULL,
    [RowNo]                  INT          NULL,
    [HRGCode]                VARCHAR (50) NULL,
    [GroupingMethodFlag]     VARCHAR (50) NULL,
    [DominantOperationCode]  VARCHAR (50) NULL,
    [PrimaryDiagnosisCode]   VARCHAR (50) NULL,
    [SecondaryDiagnosisCode] VARCHAR (50) NULL,
    [EpisodeCount]           VARCHAR (50) NULL,
    [LOS]                    VARCHAR (50) NULL,
    [PBCCode]                VARCHAR (50) NULL
);


﻿CREATE TABLE [ETL].[TLoadResultBaseTest] (
    [TestRecno]           INT          NOT NULL,
    [ContextCode]         VARCHAR (10) NOT NULL,
    [SourceUniqueID]      VARCHAR (50) NULL,
    [OrderSourceUniqueID] VARCHAR (50) NULL,
    [OrderRequestTime]    DATETIME     NULL,
    [OrderStatusCode]     VARCHAR (10) NULL,
    [PatientID]           INT          NULL,
    [PatientIdentifier]   VARCHAR (50) NULL,
    [DistrictNo]          VARCHAR (8)  NULL,
    [CasenoteNumber]      VARCHAR (20) NULL,
    [NHSNumber]           VARCHAR (20) NULL,
    [ConsultantCode]      VARCHAR (50) NULL,
    [MainSpecialtyCode]   VARCHAR (5)  NULL,
    [SpecialtyCode]       VARCHAR (5)  NULL,
    [ProviderCode]        VARCHAR (10) NULL,
    [LocationCode]        VARCHAR (10) NULL,
    [TestCode]            VARCHAR (10) NULL,
    [TestStatus]          VARCHAR (50) NULL,
    [InterfaceCode]       VARCHAR (10) NULL,
    CONSTRAINT [PK_TLoadResultBaseTest] PRIMARY KEY CLUSTERED ([TestRecno] ASC, [ContextCode] ASC)
);


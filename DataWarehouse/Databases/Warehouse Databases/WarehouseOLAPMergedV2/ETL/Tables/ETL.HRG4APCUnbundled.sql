﻿CREATE TABLE [ETL].[HRG4APCUnbundled] (
    [FinancialYear] VARCHAR (10) NULL,
    [RowNo]         INT          NULL,
    [SequenceNo]    INT          NULL,
    [HRGCode]       VARCHAR (50) NULL
);


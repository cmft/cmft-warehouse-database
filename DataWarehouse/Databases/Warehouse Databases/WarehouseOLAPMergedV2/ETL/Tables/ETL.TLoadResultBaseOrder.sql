﻿CREATE TABLE [ETL].[TLoadResultBaseOrder] (
    [OrderRecno]          INT          NOT NULL,
    [ContextCode]         VARCHAR (10) NOT NULL,
    [SourceUniqueID]      VARCHAR (50) NULL,
    [PatientID]           INT          NULL,
    [PatientIdentifier]   VARCHAR (50) NULL,
    [DistrictNo]          VARCHAR (8)  NULL,
    [CasenoteNumber]      VARCHAR (20) NULL,
    [NHSNumber]           VARCHAR (20) NULL,
    [ConsultantCode]      VARCHAR (10) NULL,
    [MainSpecialtyCode]   VARCHAR (5)  NULL,
    [SpecialtyCode]       VARCHAR (5)  NULL,
    [ProviderCode]        VARCHAR (10) NULL,
    [LocationCode]        VARCHAR (10) NULL,
    [OrderPriority]       VARCHAR (20) NULL,
    [OrderStatusCode]     VARCHAR (3)  NULL,
    [OrderRequestTime]    DATETIME     NULL,
    [OrderReceivedTime]   DATETIME     NULL,
    [OrderCollectionTime] DATETIME     NULL,
    [InterfaceCode]       VARCHAR (10) NULL,
    CONSTRAINT [PK_TLoadResultBaseOrder] PRIMARY KEY CLUSTERED ([OrderRecno] ASC, [ContextCode] ASC)
);


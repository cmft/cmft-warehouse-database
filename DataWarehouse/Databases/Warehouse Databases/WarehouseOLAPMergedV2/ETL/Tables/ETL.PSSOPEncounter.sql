﻿CREATE TABLE [ETL].[PSSOPEncounter] (
    [MergeEncounterRecno]                INT          NULL,
    [FinancialYear]                      VARCHAR (10) NULL,
    [RowNo]                              INT          NULL,
    [NationalProgrammeOfCareServiceCode] VARCHAR (50) NULL,
    [ServiceLineCode]                    VARCHAR (50) NULL
);


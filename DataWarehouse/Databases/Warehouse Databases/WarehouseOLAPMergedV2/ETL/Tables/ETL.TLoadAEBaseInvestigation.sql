﻿CREATE TABLE [ETL].[TLoadAEBaseInvestigation] (
    [InvestigationRecno]      INT           NOT NULL,
    [MergeEncounterRecno]     INT           NOT NULL,
    [InvestigationSchemeCode] VARCHAR (10)  NULL,
    [InvestigationCode]       VARCHAR (50)  NULL,
    [InvestigationTime]       DATETIME      NULL,
    [ResultTime]              DATETIME      NULL,
    [ContextCode]             VARCHAR (10)  NOT NULL,
    [Action]                  NVARCHAR (10) NULL,
    CONSTRAINT [PK_TLoadAEBaseInvestigation] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [InvestigationRecno] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_ETL_TLoadAEBaseInvestigation_01]
    ON [ETL].[TLoadAEBaseInvestigation]([MergeEncounterRecno] ASC)
    INCLUDE([ContextCode], [InvestigationCode]);


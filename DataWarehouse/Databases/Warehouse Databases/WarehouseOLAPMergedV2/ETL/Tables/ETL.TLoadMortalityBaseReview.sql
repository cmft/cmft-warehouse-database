﻿CREATE TABLE [ETL].[TLoadMortalityBaseReview] (
    [SourceUniqueID]               BIGINT        NOT NULL,
    [SourcePatientNo]              VARCHAR (20)  NULL,
    [EpisodeStartTime]             DATETIME      NULL,
    [APCContextCode]               VARCHAR (20)  NULL,
    [FormTypeCode]                 INT           NULL,
    [ReviewStatusCode]             INT           NULL,
    [PrimaryReviewAssignedTime]    DATETIME      NULL,
    [PrimaryReviewCompletedTime]   DATETIME      NULL,
    [PrimaryReviewerCode]          VARCHAR (50)  NULL,
    [SecondaryReviewAssignedTime]  DATETIME      NULL,
    [SecondaryReviewCompletedTime] DATETIME      NULL,
    [SecondaryReviewerCode]        VARCHAR (50)  NULL,
    [ClassificationGrade]          INT           NULL,
    [RequiresFurtherInvestigation] BIT           NULL,
    [QuestionNo]                   BIGINT        NULL,
    [Question]                     VARCHAR (MAX) NULL,
    [Response]                     VARCHAR (MAX) NULL,
    [ContextCode]                  VARCHAR (20)  NOT NULL,
    [ReviewCheckSum]               INT           NULL,
    [MortalityReviewAdded]         DATE          NULL
);


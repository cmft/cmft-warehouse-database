﻿CREATE TABLE [ETL].[HRG4OPFlag] (
    [FinancialYear] VARCHAR (10) NULL,
    [RowNo]         INT          NULL,
    [SequenceNo]    INT          NULL,
    [FlagCode]      VARCHAR (50) NULL
);


﻿CREATE TABLE [ETL].[PSSAPCQuality] (
    [FinancialYear]   VARCHAR (10) NULL,
    [RowNo]           INT          NULL,
    [SequenceNo]      INT          NULL,
    [QualityTypeCode] VARCHAR (50) NULL,
    [QualityCode]     VARCHAR (50) NULL,
    [QualityMessage]  VARCHAR (50) NULL
);


﻿CREATE TABLE [ETL].[TLoadAPCBaseDementia] (
    [DementiaRecNo]                 INT          NOT NULL,
    [GlobalProviderSpellNo]         VARCHAR (50) NULL,
    [ProviderSpellNo]               VARCHAR (20) NOT NULL,
    [SpellID]                       VARCHAR (20) NOT NULL,
    [SourceUniqueID]                INT          NOT NULL,
    [SourcePatientNo]               VARCHAR (10) NULL,
    [SourceSpellNo]                 VARCHAR (10) NULL,
    [AdmissionWardCode]             VARCHAR (10) NULL,
    [RecordedDate]                  DATE         NULL,
    [RecordedTime]                  DATETIME     NOT NULL,
    [KnownToHaveDementia]           INT          NULL,
    [ForgetfulnessQuestionAnswered] INT          NULL,
    [AssessmentScore]               VARCHAR (60) NULL,
    [Investigation]                 INT          NULL,
    [AssessmentNotPerformedReason]  VARCHAR (60) NULL,
    [ReferralSentDate]              VARCHAR (10) NULL,
    [InterfaceCode]                 VARCHAR (20) NOT NULL,
    [ContextCode]                   VARCHAR (20) NOT NULL,
    [Created]                       DATETIME     NULL,
    [Updated]                       DATETIME     NULL,
    [ByWhom]                        VARCHAR (50) NULL,
    [SequenceNo]                    INT          NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_GlobalProviderSpellNo]
    ON [ETL].[TLoadAPCBaseDementia]([GlobalProviderSpellNo] ASC);


﻿CREATE TABLE [ETL].[HRG4APCEncounter] (
    [MergeEncounterRecno]   INT          NULL,
    [FinancialYear]         VARCHAR (10) NULL,
    [RowNo]                 INT          NULL,
    [HRGCode]               VARCHAR (50) NULL,
    [GroupingMethodFlag]    VARCHAR (50) NULL,
    [DominantOperationCode] VARCHAR (50) NULL,
    [PBCCode]               VARCHAR (50) NULL,
    [LOE]                   INT          NULL
);


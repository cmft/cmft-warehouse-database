﻿CREATE TABLE [ETL].[TLoadOPBaseOperation] (
    [MergeEncounterRecno] INT           NOT NULL,
    [EncounterRecno]      INT           NOT NULL,
    [ContextCode]         VARCHAR (10)  NULL,
    [SequenceNo]          SMALLINT      NOT NULL,
    [OperationCode]       VARCHAR (10)  NOT NULL,
    [OperationDate]       SMALLDATETIME NULL,
    CONSTRAINT [PK_TLoadOPBaseOperation] PRIMARY KEY CLUSTERED ([MergeEncounterRecno] ASC, [SequenceNo] ASC)
);


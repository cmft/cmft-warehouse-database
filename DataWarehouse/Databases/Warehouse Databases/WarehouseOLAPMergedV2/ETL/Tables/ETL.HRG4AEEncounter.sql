﻿CREATE TABLE [ETL].[HRG4AEEncounter] (
    [MergeEncounterRecno] INT          NULL,
    [FinancialYear]       VARCHAR (10) NULL,
    [RowNo]               VARCHAR (50) NULL,
    [HRGCode]             VARCHAR (50) NULL
);


﻿CREATE TABLE [ETL].[TImportWardBedConfiguration] (
    [WardCode]          NVARCHAR (255) NULL,
    [ContextCode]       NVARCHAR (255) NULL,
    [StartDate]         NVARCHAR (255) NULL,
    [EndDate]           NVARCHAR (255) NULL,
    [InpatientBeds]     FLOAT (53)     NULL,
    [DaycaseBeds]       FLOAT (53)     NULL,
    [SingleRoomBeds]    FLOAT (53)     NULL,
    [SpecialtySectorID] FLOAT (53)     NULL
);


﻿CREATE TABLE [ETL].[TLoadAEBaseTreatment] (
    [MergeEncounterRecno] INT           NULL,
    [SequenceNo]          SMALLINT      NULL,
    [TreatmentSchemeCode] VARCHAR (10)  NULL,
    [TreatmentCode]       VARCHAR (50)  NULL,
    [TreatmentTime]       SMALLDATETIME NULL,
    [ContextCode]         VARCHAR (10)  NOT NULL,
    [Action]              NVARCHAR (10) NULL,
    [TreatmentRecno]      INT           NOT NULL,
    CONSTRAINT [PK_TLoadAEBaseTreatment] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [TreatmentRecno] ASC)
);


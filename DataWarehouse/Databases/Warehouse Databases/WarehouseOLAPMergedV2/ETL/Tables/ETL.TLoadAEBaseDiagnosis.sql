﻿CREATE TABLE [ETL].[TLoadAEBaseDiagnosis] (
    [MergeEncounterRecno]  INT           NULL,
    [SequenceNo]           SMALLINT      NULL,
    [DiagnosticSchemeCode] VARCHAR (10)  NULL,
    [DiagnosisCode]        VARCHAR (50)  NULL,
    [DiagnosisSiteCode]    VARCHAR (10)  NULL,
    [DiagnosisSideCode]    VARCHAR (10)  NULL,
    [ContextCode]          VARCHAR (10)  NOT NULL,
    [Action]               NVARCHAR (10) NULL,
    [DiagnosisRecno]       INT           NOT NULL,
    CONSTRAINT [PK_TLoadAEBaseDiagnosis] PRIMARY KEY CLUSTERED ([ContextCode] ASC, [DiagnosisRecno] ASC)
);


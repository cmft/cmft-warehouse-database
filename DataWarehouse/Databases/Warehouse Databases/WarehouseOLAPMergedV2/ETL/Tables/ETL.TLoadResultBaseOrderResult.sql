﻿CREATE TABLE [ETL].[TLoadResultBaseOrderResult] (
    [OrderResultRecno]     INT          NOT NULL,
    [ContextCode]          VARCHAR (8)  NOT NULL,
    [OrderSourceUniqueID]  INT          NOT NULL,
    [OrderRequestTime]     DATETIME     NULL,
    [ResultSourceUniqueID] INT          NOT NULL,
    [InterfaceCode]        VARCHAR (10) NOT NULL,
    CONSTRAINT [PK_TLoadResultBaseOrderResult] PRIMARY KEY CLUSTERED ([OrderResultRecno] ASC, [ContextCode] ASC)
);


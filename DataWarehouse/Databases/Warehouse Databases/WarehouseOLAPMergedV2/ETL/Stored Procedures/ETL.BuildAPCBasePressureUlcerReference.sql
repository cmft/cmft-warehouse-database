﻿

create procedure ETL.BuildAPCBasePressureUlcerReference
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BasePressureUlcerReference
			where
				BasePressureUlcerReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BasePressureUlcerReference
where
	not exists
	(
	select
		1
	from
		APC.BasePressureUlcer
	where
		BasePressureUlcer.MergePressureUlcerRecno = BasePressureUlcerReference.MergePressureUlcerRecno
	)
and	BasePressureUlcerReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BasePressureUlcerReference target
using
	(
	select
		BasePressureUlcer.MergePressureUlcerRecno
		,ContextID = Context.ContextID
		,IdentifiedDateID =
			coalesce(
				 IdentifiedDate.DateID

				,case
				when BasePressureUlcer.IdentifiedDate is null
				then NullDate.DateID

				when BasePressureUlcer.IdentifiedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,HealedDateID =
			coalesce(
				 HealedDate.DateID

				,case
				when BasePressureUlcer.HealedDate is null
				then NullDate.DateID

				when BasePressureUlcer.HealedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,LocationID = PressureUlcerLocation.SourcePressureUlcerLocationID	
		,CategoryID = PressureUlcerCategory.SourcePressureUlcerCategoryID	
		,WardID = Ward.SourceWardID	
		,BasePressureUlcer.Created
		,BasePressureUlcer.Updated
		,BasePressureUlcer.ByWhom
	from
		APC.BasePressureUlcer

	inner join WH.Context
	on	Context.ContextCode = BasePressureUlcer.ContextCode

	inner join APC.PressureUlcerLocation
	on	PressureUlcerLocation.SourcePressureUlcerLocationCode = coalesce(BasePressureUlcer.LocationID, '-1')
	and	PressureUlcerLocation.SourceContextCode = BasePressureUlcer.ContextCode

	inner join APC.PressureUlcerCategory
	on	PressureUlcerCategory.SourcePressureUlcerCategoryCode = coalesce(BasePressureUlcer.CategoryID, '-1')
	and	PressureUlcerCategory.SourceContextCode = BasePressureUlcer.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(BasePressureUlcer.WardCode, '-1')
	and	Ward.SourceContextCode = BasePressureUlcer.ContextCode

	left join WH.Calendar IdentifiedDate
	on	IdentifiedDate.TheDate = BasePressureUlcer.IdentifiedDate

	left join WH.Calendar HealedDate
	on	HealedDate.TheDate = BasePressureUlcer.HealedDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BasePressureUlcer.Updated > @LastUpdated
	and	BasePressureUlcer.ContextCode = @ContextCode

	) source
	on	source.MergePressureUlcerRecno = target.MergePressureUlcerRecno

	when not matched
	then
		insert
			(
			MergePressureUlcerRecno
			,ContextID
			,IdentifiedDateID
			,HealedDateID
			,LocationID
			,CategoryID
			,WardID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergePressureUlcerRecno
			,source.ContextID
			,source.IdentifiedDateID
			,source.HealedDateID
			,source.LocationID
			,source.CategoryID
			,source.WardID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergePressureUlcerRecno = source.MergePressureUlcerRecno
			,target.ContextID = source.ContextID
			,target.IdentifiedDateID = source.IdentifiedDateID
			,target.HealedDateID = source.HealedDateID
			,target.LocationID = source.LocationID
			,target.WardID = source.WardID
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






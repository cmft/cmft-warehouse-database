﻿



CREATE proc [ETL].[BuildCenMaternityBaseAntenatalCare]

as

set dateformat dmy


declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Maternity.BaseAntenatalCare target
using
	(
	select
		 AntenatalCareRecno
		,SourceUniqueID
		,CasenoteNumber
		,DistrictNo
		,NHSNumber
		,DateOfBirth
		,AgeAtBooking
		,GPPracticeCode
		,VisitDate
		,VisitTime
		,VisitTypeCode
		,FetalMedicineCases
		,Thrombophilia
		,SickleCellCode
		,ThalassemiaCode
		,Thromboembolism
		,AntiTNFTreatment
		,RHIsoimmunisationCode
		,RenalDisease
		,Renal
		,EndocrineCode
		,AntenatalEndocrineCode
		,MalignantDisease
		,HIV
		,HeartDiseaseCode
		,CardiacCode
		,NumberOfFetuses
		,DiabeticCode
		,DiabetesCode
		,RespiratoryProblemCode
		,EarlyPreTerm
		,Miscarriage
		,NeonatalDeath
		,SecondThirdTrimesterLoss
		,GrowthRestrictionCode
		,PreviousBirthWeight
		,PsychiatricProblemCode
		,HELLPEclampsia
		,HypertensionMedication
		,PreviousOperations
		,EpilepsyMedication
		,InheritedDiseases
		,HepatitisB
		,HepatitisC
		,BMIAtBooking
		,PhysicalDisability
		,Interpreter
		,PatientTypeCode
		,SpecialistMidwifeReferral
		,AccomodationTypeCode
		,DrugCode
		,SafeguardingChildrenCode
		,SafeguardingWomenCode
		,HRGCode
		,HypertensionCode
		,HaematologicalDisorderCode
		,MedicalHistoryComments
		,AutoimmuneDiseaseCode
		,EstimatedDeliveryDate
		,FirstContactDate
	--	,FirstContactCareProfessionalTypeCode
		,FirstContactOther
		,LastMenstrualPeriodDate
		,FirstLanguageEnglishCode
		,EmploymentStatusMotherCode
		,EmploymentStatusPartnerCode
		,SmokingStatusCode
		,CigarettesPerDay
		,AlcoholUnitsPerWeek
		,WeightMother
		,HeightMother
		,RubellaOfferDate 
		,RubellaOfferStatus 
		,RubellaBloodSampleDate 
		,RubellaResult 
		,HepatitisBOfferDate 
		,HepatitisBOfferStatus 
		,HepatitisBBloodSampleDate 
		,HepatitisBResult 
		,HaemoglobinopathyOfferDate 
		,HaemoglobinopathyOfferStatus 
		,HaemoglobinopathyBloodSampleDate 
		,HaemoglobinopathyResult 
		,DownsSyndromeOfferDate 
		,DownsSyndromeOfferStatus 
		,DownsSyndromeBloodSampleDate 
		,DownsSyndromeInvestigationRiskRatio 
		,PreviousCaesareanSections
		,PreviousLiveBirths
		,PreviousStillBirths
		,PreviousLossesLessThan24Weeks						 				
		,ModifiedTime
		,InterfaceCode
		,ContextCode
	from
		ETL.TLoadCenMaternityBaseAntenatalCare
	) source
	on	source.AntenatalCareRecno = target.AntenatalCareRecno
	and	source.ContextCode = target.ContextCode
	

	when not matched by source
	and	target.ContextCode = 'CEN||CMIS'

	then delete

	when not matched
	then
		insert
			(
			 AntenatalCareRecno
			,SourceUniqueID
			,CasenoteNumber
			,DistrictNo
			,NHSNumber
			,DateOfBirth
			,AgeAtBooking
			,GPPracticeCode
			,VisitDate
			,VisitTime
			,VisitTypeCode
			,FetalMedicineCases
			,Thrombophilia
			,SickleCellCode
			,ThalassemiaCode
			,Thromboembolism
			,AntiTNFTreatment
			,RHIsoimmunisationCode
			,RenalDisease
			,Renal
			,EndocrineCode
			,AntenatalEndocrineCode
			,MalignantDisease
			,HIV
			,HeartDiseaseCode
			,CardiacCode
			,NumberOfFetuses
			,DiabeticCode
			,DiabetesCode
			,RespiratoryProblemCode
			,EarlyPreTerm
			,Miscarriage
			,NeonatalDeath
			,SecondThirdTrimesterLoss
			,GrowthRestrictionCode
			,PreviousBirthWeight
			,PsychiatricProblemCode
			,HELLPEclampsia
			,HypertensionMedication
			,PreviousOperations
			,EpilepsyMedication
			,InheritedDiseases
			,HepatitisB
			,HepatitisC
			,BMIAtBooking
			,PhysicalDisability
			,Interpreter
			,PatientTypeCode
			,SpecialistMidwifeReferral
			,AccomodationTypeCode
			,DrugCode
			,SafeguardingChildrenCode
			,SafeguardingWomenCode
			,HRGCode
			,HypertensionCode 
			,HaematologicalDisorderCode
			,MedicalHistoryComments
			,AutoimmuneDiseaseCode
			,EstimatedDeliveryDate
			,FirstContactDate
		--	,FirstContactCareProfessionalTypeCode
			,FirstContactOther
			,LastMenstrualPeriodDate
			,FirstLanguageEnglishCode
			,EmploymentStatusMotherCode
			,EmploymentStatusPartnerCode
			,SmokingStatusCode
			,CigarettesPerDay
			,AlcoholUnitsPerWeek
			,WeightMother
			,HeightMother
			,RubellaOfferDate 
			,RubellaOfferStatus 
			,RubellaBloodSampleDate 
			,RubellaResult 	
			,HepatitisBOfferDate 
			,HepatitisBOfferStatus 
			,HepatitisBBloodSampleDate 
			,HepatitisBResult 
			,HaemoglobinopathyOfferDate 
			,HaemoglobinopathyOfferStatus 
			,HaemoglobinopathyBloodSampleDate 
			,HaemoglobinopathyResult 
			,DownsSyndromeOfferDate 
			,DownsSyndromeOfferStatus 
			,DownsSyndromeBloodSampleDate 
			,DownsSyndromeInvestigationRiskRatio 
			,PreviousCaesareanSections
			,PreviousLiveBirths
			,PreviousStillBirths
			,PreviousLossesLessThan24Weeks						 		
			,ModifiedTime
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
		  )
		values
			(
			 source.AntenatalCareRecno
			,source.SourceUniqueID
			,source.CasenoteNumber
			,source.DistrictNo
			,source.NHSNumber
			,source.DateOfBirth
			,source.AgeAtBooking
			,source.GPPracticeCode
			,source.VisitDate
			,source.VisitTime
			,source.VisitTypeCode
			,source.FetalMedicineCases
			,source.Thrombophilia
			,source.SickleCellCode
			,source.ThalassemiaCode
			,source.Thromboembolism
			,source.AntiTNFTreatment
			,source.RHIsoimmunisationCode
			,source.RenalDisease
			,source.Renal
			,source.EndocrineCode
			,source.AntenatalEndocrineCode
			,source.MalignantDisease
			,source.HIV
			,source.HeartDiseaseCode
			,source.CardiacCode
			,source.NumberOfFetuses
			,source.DiabeticCode
			,source.DiabetesCode
			,source.RespiratoryProblemCode
			,source.EarlyPreTerm
			,source.Miscarriage
			,source.NeonatalDeath
			,source.SecondThirdTrimesterLoss
			,source.GrowthRestrictionCode
			,source.PreviousBirthWeight
			,source.PsychiatricProblemCode
			,source.HELLPEclampsia
			,source.HypertensionMedication
			,source.PreviousOperations
			,source.EpilepsyMedication
			,source.InheritedDiseases
			,source.HepatitisB
			,source.HepatitisC
			,source.BMIAtBooking
			,source.PhysicalDisability
			,source.Interpreter
			,source.PatientTypeCode
			,source.SpecialistMidwifeReferral
			,source.AccomodationTypeCode
			,source.DrugCode
			,source.SafeguardingChildrenCode
			,source.SafeguardingWomenCode
			,source.HRGCode
			,source.HypertensionCode 
			,source.HaematologicalDisorderCode
			,source.MedicalHistoryComments
			,source.AutoimmuneDiseaseCode
			,source.EstimatedDeliveryDate
			,source.FirstContactDate
		--	,source.FirstContactCareProfessionalTypeCode
			,source.FirstContactOther
			,source.LastMenstrualPeriodDate
			,source.FirstLanguageEnglishCode
			,source.EmploymentStatusMotherCode
			,source.EmploymentStatusPartnerCode
			,source.SmokingStatusCode
			,source.CigarettesPerDay
			,source.AlcoholUnitsPerWeek
			,source.WeightMother
			,source.HeightMother
			,source.RubellaOfferDate 
			,source.RubellaOfferStatus 
			,source.RubellaBloodSampleDate 
			,source.RubellaResult 
			,source.HepatitisBOfferDate 
			,source.HepatitisBOfferStatus 
			,source.HepatitisBBloodSampleDate 
			,source.HepatitisBResult  	
			,source.HaemoglobinopathyOfferDate 
			,source.HaemoglobinopathyOfferStatus 
			,source.HaemoglobinopathyBloodSampleDate 
			,source.HaemoglobinopathyResult 
			,source.DownsSyndromeOfferDate 
			,source.DownsSyndromeOfferStatus 
			,source.DownsSyndromeBloodSampleDate 
			,source.DownsSyndromeInvestigationRiskRatio 
			,source.PreviousCaesareanSections
			,source.PreviousLiveBirths
			,source.PreviousStillBirths
			,source.PreviousLossesLessThan24Weeks														
			,source.ModifiedTime
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.AntenatalCareRecno, 0) = isnull(source.AntenatalCareRecno, 0)
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.CasenoteNumber, '') = isnull(source.CasenoteNumber, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.AgeAtBooking, 0) = isnull(source.AgeAtBooking, 0)
		and isnull(target.GPPracticeCode, '') = isnull(source.GPPracticeCode, '')
		and isnull(target.VisitDate, getdate()) = isnull(source.VisitDate, getdate())
		and isnull(target.VisitTime, getdate()) = isnull(source.VisitTime, getdate())
		and isnull(target.VisitTypeCode, '') = isnull(source.VisitTypeCode, '')
		and isnull(target.FetalMedicineCases, 0) = isnull(source.FetalMedicineCases, 0)
		and isnull(target.Thrombophilia, '') = isnull(source.Thrombophilia, '')
		and isnull(target.SickleCellCode, '') = isnull(source.SickleCellCode, '')
		and isnull(target.ThalassemiaCode, '') = isnull(source.ThalassemiaCode, '')
		and isnull(target.Thromboembolism, '') = isnull(source.Thromboembolism, '')
		and isnull(target.AntiTNFTreatment, '') = isnull(source.AntiTNFTreatment, '')
		and isnull(target.RHIsoimmunisationCode, '') = isnull(source.RHIsoimmunisationCode, '')
		and isnull(target.RenalDisease, '') = isnull(source.RenalDisease, '')
		and isnull(target.Renal, '') = isnull(source.Renal, '')
		and isnull(target.EndocrineCode, '') = isnull(source.EndocrineCode, '')
		and isnull(target.AntenatalEndocrineCode, '') = isnull(source.AntenatalEndocrineCode, '')
		and isnull(target.MalignantDisease, '') = isnull(source.MalignantDisease, '')
		and isnull(target.HIV, '') = isnull(source.HIV, '')
		and isnull(target.HeartDiseaseCode, '') = isnull(source.HeartDiseaseCode, '')
		and isnull(target.CardiacCode, '') = isnull(source.CardiacCode, '')
		and isnull(target.NumberOfFetuses, 0) = isnull(source.NumberOfFetuses, 0)
		and isnull(target.DiabeticCode, '') = isnull(source.DiabeticCode, '')
		and isnull(target.DiabetesCode, '') = isnull(source.DiabetesCode, '')
		and isnull(target.RespiratoryProblemCode, '') = isnull(source.RespiratoryProblemCode, '')
		and isnull(target.EarlyPreTerm, 0) = isnull(source.EarlyPreTerm, 0)
		and isnull(target.Miscarriage, 0) = isnull(source.Miscarriage, 0)
		and isnull(target.NeonatalDeath, 0) = isnull(source.NeonatalDeath, 0)
		and isnull(target.SecondThirdTrimesterLoss, 0) = isnull(source.SecondThirdTrimesterLoss, 0)
		and isnull(target.GrowthRestrictionCode, 0) = isnull(source.GrowthRestrictionCode, 0)
		and isnull(target.PreviousBirthWeight, 0) = isnull(source.PreviousBirthWeight, 0)
		and isnull(target.PsychiatricProblemCode, '') = isnull(source.PsychiatricProblemCode, '')
		and isnull(target.HELLPEclampsia, 0) = isnull(source.HELLPEclampsia, 0)
		and isnull(target.HypertensionMedication, '') = isnull(source.HypertensionMedication, '')
		and isnull(target.PreviousOperations, 0) = isnull(source.PreviousOperations, 0)
		and isnull(target.EpilepsyMedication, '') = isnull(source.EpilepsyMedication, '')
		and isnull(target.InheritedDiseases, '') = isnull(source.InheritedDiseases, '')
		and isnull(target.HepatitisB, '') = isnull(source.HepatitisB, '')
		and isnull(target.HepatitisC, '') = isnull(source.HepatitisC, '')
		and isnull(target.BMIAtBooking, '') = isnull(source.BMIAtBooking, '')
		and isnull(target.PhysicalDisability, '') = isnull(source.PhysicalDisability, '')
		and isnull(target.Interpreter, '') = isnull(source.Interpreter, '')
		and isnull(target.PatientTypeCode, '') = isnull(source.PatientTypeCode, '')
		and isnull(target.SpecialistMidwifeReferral, '') = isnull(source.SpecialistMidwifeReferral, '')
		and isnull(target.AccomodationTypeCode, '') = isnull(source.AccomodationTypeCode, '')
		and isnull(target.DrugCode, '') = isnull(source.DrugCode, '')
		and isnull(target.SafeguardingChildrenCode, '') = isnull(source.SafeguardingChildrenCode, '')
		and isnull(target.SafeguardingWomenCode, '') = isnull(source.SafeguardingWomenCode, '')
		and isnull(target.HRGCode, '') = isnull(source.HRGCode, '')
		and isnull(target.HypertensionCode, '') = isnull(source.HypertensionCode, '')

		and isnull(target.HaematologicalDisorderCode, '') = isnull(source.HaematologicalDisorderCode, '')
		and isnull(target.MedicalHistoryComments, '') = isnull(source.MedicalHistoryComments, '')
		and isnull(target.AutoimmuneDiseaseCode, '') = isnull(source.AutoimmuneDiseaseCode, '')
		and isnull(target.EstimatedDeliveryDate, '') = isnull(source.EstimatedDeliveryDate, '')
		and isnull(target.FirstContactDate, '') = isnull(source.FirstContactDate, '')
		--and isnull(target.FirstContactCareProfessionalTypeCode, '') = isnull(source.FirstContactCareProfessionalTypeCode, '')
		and isnull(target.FirstContactOther, '') = isnull(source.FirstContactOther, '')
		and isnull(target.LastMenstrualPeriodDate, '') = isnull(source.LastMenstrualPeriodDate, '')
		and isnull(target.FirstLanguageEnglishCode, '') = isnull(source.FirstLanguageEnglishCode, '')
		and isnull(target.EmploymentStatusMotherCode, '') = isnull(source.EmploymentStatusMotherCode, '')
		and isnull(target.EmploymentStatusPartnerCode, '') = isnull(source.EmploymentStatusPartnerCode, '')
		and isnull(target.SmokingStatusCode, '') = isnull(source.SmokingStatusCode, '')
		and isnull(target.CigarettesPerDay, '') = isnull(source.CigarettesPerDay, '')
		and isnull(target.AlcoholUnitsPerWeek, '') = isnull(source.AlcoholUnitsPerWeek, '')
		and isnull(target.WeightMother, '') = isnull(source.WeightMother, '')
		and isnull(target.HeightMother, '') = isnull(source.HeightMother, '')
		and isnull(target.RubellaOfferDate, '') = isnull(source.RubellaOfferDate, '')
		and isnull(target.RubellaOfferStatus, '') = isnull(source.RubellaOfferStatus, '')
		and isnull(target.RubellaBloodSampleDate, '') = isnull(source.RubellaBloodSampleDate, '')
		and isnull(target.RubellaResult, '') = isnull(source.RubellaResult, '')	
		and isnull(target.HepatitisBOfferDate, '') = isnull(source.HepatitisBOfferDate, '')
		and isnull(target.HepatitisBOfferStatus, '') = isnull(source.HepatitisBOfferStatus, '')
		and isnull(target.HepatitisBBloodSampleDate, '') = isnull(source.HepatitisBBloodSampleDate, '')
		and isnull(target.HepatitisBResult, '') = isnull(source.HepatitisBResult, '')	
		and isnull(target.HaemoglobinopathyOfferDate, '') = isnull(source.HaemoglobinopathyOfferDate, '')
		and isnull(target.HaemoglobinopathyOfferStatus, '') = isnull(source.HaemoglobinopathyOfferStatus, '')
		and isnull(target.HaemoglobinopathyBloodSampleDate, '') = isnull(source.HaemoglobinopathyBloodSampleDate, '')
		and isnull(target.HaemoglobinopathyResult, '') = isnull(source.HaemoglobinopathyResult, '')	
		and isnull(target.DownsSyndromeOfferDate, '') = isnull(source.DownsSyndromeOfferDate, '')
		and isnull(target.DownsSyndromeOfferStatus, '') = isnull(source.DownsSyndromeOfferStatus, '')
		and isnull(target.DownsSyndromeBloodSampleDate, '') = isnull(source.DownsSyndromeBloodSampleDate, '')
		and isnull(target.DownsSyndromeInvestigationRiskRatio, '') = isnull(source.DownsSyndromeInvestigationRiskRatio, '')
		and isnull(target.PreviousCaesareanSections, '') = isnull(source.PreviousCaesareanSections, '')
		and isnull(target.PreviousLiveBirths, '') = isnull(source.PreviousLiveBirths, '')
		and isnull(target.PreviousStillBirths, '') = isnull(source.PreviousStillBirths, '')
		and isnull(target.PreviousLossesLessThan24Weeks, '') = isnull(source.PreviousLossesLessThan24Weeks, '')
		and isnull(target.ModifiedTime, getdate()) = isnull(source.ModifiedTime, getdate())
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.AntenatalCareRecno = source.AntenatalCareRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DistrictNo = source.DistrictNo
			,target.NHSNumber = source.NHSNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.AgeAtBooking = source.AgeAtBooking
			,target.GPPracticeCode = source.GPPracticeCode
			,target.VisitDate = source.VisitDate
			,target.VisitTime = source.VisitTime
			,target.VisitTypeCode = source.VisitTypeCode
			,target.FetalMedicineCases = source.FetalMedicineCases
			,target.Thrombophilia = source.Thrombophilia
			,target.SickleCellCode = source.SickleCellCode
			,target.ThalassemiaCode = source.ThalassemiaCode
			,target.Thromboembolism = source.Thromboembolism
			,target.AntiTNFTreatment = source.AntiTNFTreatment
			,target.RHIsoimmunisationCode = source.RHIsoimmunisationCode
			,target.RenalDisease = source.RenalDisease
			,target.Renal = source.Renal
			,target.EndocrineCode = source.EndocrineCode
			,target.AntenatalEndocrineCode = source.AntenatalEndocrineCode
			,target.MalignantDisease = source.MalignantDisease
			,target.HIV = source.HIV
			,target.HeartDiseaseCode = source.HeartDiseaseCode
			,target.CardiacCode = source.CardiacCode
			,target.NumberOfFetuses = source.NumberOfFetuses
			,target.DiabeticCode = source.DiabeticCode
			,target.DiabetesCode = source.DiabetesCode
			,target.RespiratoryProblemCode = source.RespiratoryProblemCode
			,target.EarlyPreTerm = source.EarlyPreTerm
			,target.Miscarriage = source.Miscarriage
			,target.NeonatalDeath = source.NeonatalDeath
			,target.SecondThirdTrimesterLoss = source.SecondThirdTrimesterLoss
			,target.GrowthRestrictionCode = source.GrowthRestrictionCode
			,target.PreviousBirthWeight = source.PreviousBirthWeight
			,target.PsychiatricProblemCode = source.PsychiatricProblemCode
			,target.HELLPEclampsia = source.HELLPEclampsia
			,target.HypertensionMedication = source.HypertensionMedication
			,target.PreviousOperations = source.PreviousOperations
			,target.EpilepsyMedication = source.EpilepsyMedication
			,target.InheritedDiseases = source.InheritedDiseases
			,target.HepatitisB = source.HepatitisB
			,target.HepatitisC = source.HepatitisC
			,target.BMIAtBooking = source.BMIAtBooking
			,target.PhysicalDisability = source.PhysicalDisability
			,target.Interpreter = source.Interpreter
			,target.PatientTypeCode = source.PatientTypeCode
			,target.SpecialistMidwifeReferral = source.SpecialistMidwifeReferral
			,target.AccomodationTypeCode = source.AccomodationTypeCode
			,target.DrugCode = source.DrugCode
			,target.SafeguardingChildrenCode = source.SafeguardingChildrenCode
			,target.SafeguardingWomenCode = source.SafeguardingWomenCode
			,target.HRGCode = source.HRGCode
			,target.HypertensionCode = source.HypertensionCode
			,target.HaematologicalDisorderCode = source.HaematologicalDisorderCode
			,target.MedicalHistoryComments = source.MedicalHistoryComments
			,target.AutoimmuneDiseaseCode = source.AutoimmuneDiseaseCode
			,target.EstimatedDeliveryDate = source.EstimatedDeliveryDate
			,target.FirstContactDate = source.FirstContactDate
			--,target.FirstContactCareProfessionalTypeCode = source.FirstContactCareProfessionalTypeCode
			,target.FirstContactOther = source.FirstContactOther
			,target.LastMenstrualPeriodDate = source.LastMenstrualPeriodDate
			,target.FirstLanguageEnglishCode = source.FirstLanguageEnglishCode
			,target.EmploymentStatusMotherCode = source.EmploymentStatusMotherCode
			,target.EmploymentStatusPartnerCode = source.EmploymentStatusPartnerCode
			,target.SmokingStatusCode = source.SmokingStatusCode
			,target.CigarettesPerDay = source.CigarettesPerDay
			,target.AlcoholUnitsPerWeek = source.AlcoholUnitsPerWeek
			,target.WeightMother = source.WeightMother
			,target.HeightMother = source.HeightMother
			,target.RubellaOfferDate = source.RubellaOfferDate
			,target.RubellaOfferStatus = source.RubellaOfferStatus
			,target.RubellaBloodSampleDate = source.RubellaBloodSampleDate
			,target.RubellaResult = source.RubellaResult
			,target.HepatitisBOfferDate = source.HepatitisBOfferDate
			,target.HepatitisBOfferStatus = source.HepatitisBOfferStatus
			,target.HepatitisBBloodSampleDate = source.HepatitisBBloodSampleDate
			,target.HepatitisBResult = source.HepatitisBResult	
			,target.HaemoglobinopathyOfferDate = source.HaemoglobinopathyOfferDate
			,target.HaemoglobinopathyOfferStatus = source.HaemoglobinopathyOfferStatus
			,target.HaemoglobinopathyBloodSampleDate = source.HaemoglobinopathyBloodSampleDate
			,target.HaemoglobinopathyResult = source.HaemoglobinopathyResult	
			,target.DownsSyndromeOfferDate = source.DownsSyndromeOfferDate
			,target.DownsSyndromeOfferStatus = source.DownsSyndromeOfferStatus
			,target.DownsSyndromeBloodSampleDate = source.DownsSyndromeBloodSampleDate
			,target.DownsSyndromeInvestigationRiskRatio = source.DownsSyndromeInvestigationRiskRatio	
			,target.PreviousCaesareanSections = source.PreviousCaesareanSections
			,target.PreviousLiveBirths = source.PreviousLiveBirths
			,target.PreviousStillBirths = source.PreviousStillBirths
			,target.PreviousLossesLessThan24Weeks = source.PreviousLossesLessThan24Weeks																			
			,target.ModifiedTime = source.ModifiedTime
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary
;
select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




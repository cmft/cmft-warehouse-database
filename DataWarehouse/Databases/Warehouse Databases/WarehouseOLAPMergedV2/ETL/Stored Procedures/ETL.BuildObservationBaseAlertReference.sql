﻿CREATE procedure [ETL].[BuildObservationBaseAlertReference]

	@ContextCode varchar(50)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Observation.BaseAlertReference
			where
				BaseAlertReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Observation.BaseAlertReference
where
	not exists
	(
	select
		1
	from
		Observation.BaseAlert
	where
		BaseAlert.MergeAlertRecno = BaseAlertReference.MergeAlertRecno
	)
and	BaseAlertReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Observation.BaseAlertReference target
using
	(
	select
		 Alert.MergeAlertRecno
		,ContextID = Context.ContextID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,LocationID = Location.SourceLocationID
		,TypeID = AlertType.SourceAlertTypeID
		,ReasonID = AlertReason.SourceAlertReasonID
		,CreatedDateID = 
				coalesce(
						 CreatedDate.DateID

						,case
						when Alert.CreatedDate is null
						then NullDate.DateID

						when Alert.CreatedDate < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,BedsideDueDateID = null
		,EscalationDateID = null
		,AcceptedDateID = null
		,AttendedByUserID = AttendedByUser.SystemUserID
		,AttendanceTypeID = null
		,SeverityID = AlertSeverity.SourceAlertSeverityID
		,ClinicianSeniorityID = null
		,ClosedDateID =
				coalesce(
						 ClosedDate.DateID

						,case
						when Alert.ClosedDate is null
						then NullDate.DateID

						when Alert.ClosedDate < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,ClosureReasonID = AlertClosureReason.SourceAlertClosureReasonID
		,ClosedByUserID = ClosedByUser.SystemUserID
		,Alert.Created
		,Alert.Updated
		,Alert.ByWhom
	from
		Observation.BaseAlert Alert

	inner join WH.Context
	on	Context.ContextCode = Alert.ContextCode

	inner join Observation.AlertClosureReason
	on	AlertClosureReason.SourceAlertClosureReasonCode = coalesce(Alert.ClosureReasonID, '-1')
	and	AlertClosureReason.SourceContextCode = Alert.ContextCode

	inner join Observation.AlertReason
	on	AlertReason.SourceAlertReasonCode = coalesce(Alert.ReasonID, '-1')
	and	AlertReason.SourceContextCode = Alert.ContextCode

	inner join Observation.AlertSeverity
	on	AlertSeverity.SourceAlertSeverityCode = coalesce(Alert.SeverityID, '-1')
	and	AlertSeverity.SourceContextCode = Alert.ContextCode

	inner join Observation.AlertType
	on	AlertType.SourceAlertTypeCode = coalesce(Alert.TypeID, '-1')
	and	AlertType.SourceContextCode = Alert.ContextCode

	inner join WH.Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(Alert.SpecialtyID, '-1')
	and	Specialty.SourceContextCode = Alert.ContextCode

	inner join WH.Location
	on	Location.SourceLocationCode = coalesce(Alert.LocationID, '-1')
	and	Location.SourceContextCode = Alert.ContextCode

	inner join WH.SystemUser AttendedByUser
	on	AttendedByUser.UserID = coalesce(Alert.AttendedByUserID, '-1')
	and	AttendedByUser.ContextCode = Alert.ContextCode

	inner join WH.SystemUser ClosedByUser
	on	ClosedByUser.UserID = coalesce(Alert.ClosedByUserID, '-1')
	and	ClosedByUser.ContextCode = Alert.ContextCode

	left join WH.Calendar CreatedDate
	on	CreatedDate.TheDate = Alert.CreatedDate

	left join WH.Calendar ClosedDate
	on	ClosedDate.TheDate = Alert.ClosedDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Alert.Updated > @LastUpdated
	and	Alert.ContextCode = @ContextCode

	) source
	on	source.MergeAlertRecno = target.MergeAlertRecno

	when not matched
	then
		insert
			(
			MergeAlertRecno
			,ContextID
			,SpecialtyID
			,LocationID
			,TypeID
			,ReasonID
			,CreatedDateID
			,BedsideDueDateID
			,EscalationDateID
			,AcceptedDateID
			,AttendedByUserID
			,AttendanceTypeID
			,SeverityID
			,ClinicianSeniorityID
			,ClosedDateID
			,ClosureReasonID
			,ClosedByUserID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeAlertRecno
			,source.ContextID
			,source.SpecialtyID
			,source.LocationID
			,source.TypeID
			,source.ReasonID
			,source.CreatedDateID
			,source.BedsideDueDateID
			,source.EscalationDateID
			,source.AcceptedDateID
			,source.AttendedByUserID
			,source.AttendanceTypeID
			,source.SeverityID
			,source.ClinicianSeniorityID
			,source.ClosedDateID
			,source.ClosureReasonID
			,source.ClosedByUserID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and	target.Updated != source.Updated
	then
		update
		set
			target.ContextID = source.ContextID
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.TypeID = source.TypeID
			,target.ReasonID = source.ReasonID
			,target.CreatedDateID = source.CreatedDateID
			,target.BedsideDueDateID = source.BedsideDueDateID
			,target.EscalationDateID = source.EscalationDateID
			,target.AcceptedDateID = source.AcceptedDateID		
			,target.AttendedByUserID = source.AttendedByUserID
			,target.AttendanceTypeID = source.AttendanceTypeID
			,target.SeverityID = source.SeverityID
			,target.ClinicianSeniorityID = source.ClinicianSeniorityID
			,target.ClosedDateID = source.ClosedDateID
			,target.ClosureReasonID = source.ClosureReasonID
			,target.ClosedByUserID = source.ClosedByUserID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime



﻿
CREATE procedure [ETL].[ImportCenNeonatalBaseEncounter] as

--import the data
exec ETL.BuildCenNeonatalBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Neonatal].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildNeonatalBaseEncounterReference 'CEN||BDGR'


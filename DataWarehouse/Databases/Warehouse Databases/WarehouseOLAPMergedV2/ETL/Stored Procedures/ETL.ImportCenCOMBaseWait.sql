﻿CREATE procedure [ETL].[ImportCenCOMBaseWait] as


--import the data
exec ETL.BuildCenCOMBaseWait


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[COM].[BaseWait]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildCOMBaseWaitReference 'CEN||IPM'

exec WH.BuildCensus

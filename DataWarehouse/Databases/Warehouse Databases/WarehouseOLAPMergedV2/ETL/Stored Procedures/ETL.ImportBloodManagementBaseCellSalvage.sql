﻿
CREATE procedure [ETL].[ImportBloodManagementBaseCellSalvage] as

--import the data
exec ETL.BuildBloodManagementBaseCellSalvage


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[BloodManagement].[BaseCellSalvage]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
 exec ETL.BuildBloodManagementBaseCellSalvageReference 'CMFT||RECALL'



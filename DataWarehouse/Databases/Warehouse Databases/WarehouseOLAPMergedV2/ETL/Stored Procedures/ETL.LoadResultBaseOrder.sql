﻿CREATE procedure [ETL].[LoadResultBaseOrder]
as

-------------------------------------------------
--When		Who	What
--20150709	DG	Created procedure
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @LoadStartDate datetime
	,@LoadEndDate datetime

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE
	(
	 MergeRecno int
	,Action nvarchar(10)
	);


merge
	Result.BaseOrder target
using
	(
	select
		 OrderRecno
		,ContextCode 
		,SourceUniqueID 
		,PatientID 
		,PatientIdentifier 
		,DistrictNo 
		,CasenoteNumber
		,NHSNumber 
		,ConsultantCode 
		,MainSpecialtyCode
		,SpecialtyCode
		,ProviderCode 
		,LocationCode
		,OrderPriority 
		,OrderStatusCode 
		,OrderRequestTime
		,OrderReceivedTime
		,OrderCollectionTime 
		,InterfaceCode
		,OrderChecksum = 
			checksum(
				 OrderRecno
				,ContextCode
				,SourceUniqueID
				,PatientID
				,PatientIdentifier
				,DistrictNo
				,CasenoteNumber
				,NHSNumber
				,ConsultantCode
				,MainSpecialtyCode
				,SpecialtyCode
				,ProviderCode
				,LocationCode
				,OrderPriority
				,OrderStatusCode
				,OrderRequestTime
				,OrderReceivedTime
				,OrderCollectionTime
				,InterfaceCode
			)				
	from
		ETL.TLoadResultBaseOrder
	) source
	on	source.ContextCode = target.ContextCode
	and	source.OrderRecno = target.OrderRecno

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 OrderRecno
			,ContextCode
			,SourceUniqueID
			,PatientID
			,PatientIdentifier
			,DistrictNo
			,CasenoteNumber
			,NHSNumber
			,ConsultantCode
			,MainSpecialtyCode
			,SpecialtyCode
			,ProviderCode
			,LocationCode
			,OrderPriority
			,OrderStatusCode
			,OrderRequestTime
			,OrderReceivedTime
			,OrderCollectionTime
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,OrderChecksum
			)
		values
			(
			 source.OrderRecno
			,source.ContextCode
			,source.SourceUniqueID
			,source.PatientID
			,source.PatientIdentifier
			,source.DistrictNo
			,source.CasenoteNumber
			,source.NHSNumber
			,source.ConsultantCode
			,source.MainSpecialtyCode
			,source.SpecialtyCode
			,source.ProviderCode
			,source.LocationCode
			,source.OrderPriority
			,source.OrderStatusCode
			,source.OrderRequestTime
			,source.OrderReceivedTime
			,source.OrderCollectionTime
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.OrderChecksum
			)

	when matched
	and	target.OrderChecksum <> source.OrderChecksum
	then
		update
		set
			 target.OrderRecno = source.OrderRecno
			,target.ContextCode = source.ContextCode
			,target.SourceUniqueID = source.SourceUniqueID
			,target.PatientID = source.PatientID
			,target.PatientIdentifier = source.PatientIdentifier
			,target.DistrictNo = source.DistrictNo
			,target.CasenoteNumber = source.CasenoteNumber
			,target.NHSNumber = source.NHSNumber
			,target.ConsultantCode = source.ConsultantCode
			,target.MainSpecialtyCode = source.MainSpecialtyCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ProviderCode = source.ProviderCode
			,target.LocationCode = source.LocationCode
			,target.OrderPriority = source.OrderPriority
			,target.OrderStatusCode = source.OrderStatusCode
			,target.OrderRequestTime = source.OrderRequestTime
			,target.OrderReceivedTime = source.OrderReceivedTime
			,target.OrderCollectionTime = source.OrderCollectionTime
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.OrderChecksum = source.OrderChecksum

output
	 coalesce(inserted.MergeOrderRecno, deleted.MergeOrderRecno)
	,$action
	into
		@MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


--merge ProcessList in case it already has rows from a previous (failed) run
--merge
--	Result.ProcessList target
--using
--	@MergeSummary source
--on	source.MergeRecno = target.MergeRecno

--when not matched
--then
--	insert
--		(
--		 MergeRecno
--		,Action
--		)
--	values
--		(
--		 MergeRecno
--		,Action
--		)

--when matched
--then
--	update
--	set
--		target.Action = source.Action
;



select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




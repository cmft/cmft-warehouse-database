﻿CREATE procedure [ETL].[ImportCenOPBaseEncounter] as

--import the data
exec ETL.BuildCenOPBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[OP].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildOPBaseEncounterReference 'CEN||PAS'

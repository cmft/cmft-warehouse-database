﻿create procedure [ETL].[BuildCenAEBaseDiagnosis]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


delete
from
	AE.BaseDiagnosis
where
	not exists
	(
	select
		1
	from
		ETL.TLoadCenAEBaseDiagnosis
	where
		TLoadCenAEBaseDiagnosis.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	)

and	exists
	(
	select
		1
	from
		AE.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseDiagnosis.MergeEncounterRecno
	and	BaseEncounter.ContextCode in
		(
		 'CEN||ADAS'
		,'CEN||PAS'
		,'CEN||SYM'
		,'TRA||ADAS'
		)
	)

select
	@deleted = @@ROWCOUNT


merge
	AE.BaseDiagnosis target
using
	(
	select
		 MergeEncounterRecno
		,SequenceNo = cast(SequenceNo as smallint)
		,DiagnosticSchemeCode = cast(DiagnosticSchemeCode as varchar(10))
		,DiagnosisCode = cast(DiagnosisCode as varchar(50))
		,DiagnosisSiteCode = cast(DiagnosisSiteCode as varchar(10))
		,DiagnosisSideCode = cast(DiagnosisSideCode as varchar(10))
		,ContextCode
		,Created = getdate()
		,Updated = getdate()
		,ByWhom = suser_name()
	from
		ETL.TLoadCenAEBaseDiagnosis Encounter
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno
	and	source.SequenceNo = target.SequenceNo

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,SequenceNo
			,DiagnosticSchemeCode
			,DiagnosisCode
			,DiagnosisSiteCode
			,DiagnosisSideCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.SequenceNo
			,source.DiagnosticSchemeCode
			,source.DiagnosisCode
			,source.DiagnosisSiteCode
			,source.DiagnosisSideCode
			,source.ContextCode
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			isnull(target.DiagnosticSchemeCode, '') = isnull(source.DiagnosticSchemeCode, '')
		and isnull(target.DiagnosisCode, '') = isnull(source.DiagnosisCode, '')
		and isnull(target.DiagnosisSiteCode, '') = isnull(source.DiagnosisSiteCode, '')
		and isnull(target.DiagnosisSideCode, '') = isnull(source.DiagnosisSideCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.DiagnosticSchemeCode = source.DiagnosticSchemeCode
			,target.DiagnosisCode = source.DiagnosisCode
			,target.DiagnosisSiteCode = source.DiagnosisSiteCode
			,target.DiagnosisSideCode = source.DiagnosisSideCode
			,target.ContextCode = source.ContextCode

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



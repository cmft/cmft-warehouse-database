﻿

create procedure [ETL].[BuildMaternityBaseAntenatalCareReference] 
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Maternity.BaseAntenatalCareReference
			where
				BaseAntenatalCareReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Maternity.BaseAntenatalCareReference
where
	not exists
	(
	select
		1
	from
		Maternity.BaseAntenatalCare
	where
		BaseAntenatalCare.MergeAntenatalCareRecno = BaseAntenatalCareReference.MergeAntenatalCareRecno
	)
and	BaseAntenatalCareReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Maternity.BaseAntenatalCareReference target
using
	(
	select
		BaseAntenatalCare.MergeAntenatalCareRecno
		,ContextID = Context.ContextID
		,VisitDateID =
			coalesce(
				 VisitDate.DateID

				,case
				when BaseAntenatalCare.VisitDate is null
				then NullDate.DateID

				when BaseAntenatalCare.VisitDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,BaseAntenatalCare.Created
		,BaseAntenatalCare.Updated
		,BaseAntenatalCare.ByWhom
	from
		Maternity.BaseAntenatalCare

	inner join WH.Context
	on	Context.ContextCode = BaseAntenatalCare.ContextCode

	left join WH.Calendar VisitDate
	on	VisitDate.TheDate = BaseAntenatalCare.VisitDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseAntenatalCare.Updated > @LastUpdated
	and	BaseAntenatalCare.ContextCode = @ContextCode

	) source
	on	source.MergeAntenatalCareRecno = target.MergeAntenatalCareRecno

	when not matched
	then
		insert
			(
			MergeAntenatalCareRecno
			,ContextID
			,VisitDateID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeAntenatalCareRecno
			,source.ContextID
			,source.VisitDateID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeAntenatalCareRecno = source.MergeAntenatalCareRecno
			,target.ContextID = source.ContextID
			,target.VisitDateID = source.VisitDateID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






﻿
CREATE procedure [ETL].[BuildRenalBasePatientRenalModality]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||CV'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Renal.BasePatientRenalModality target
using [$(Warehouse)].Renal.PatientRenalModality source
on	source.PatientRenalModalityRecno = target.PatientRenalModalityRecno

when matched and not
(
	isnull(target.[SourceUniqueID], '') = isnull(source.[SourceUniqueID], '')
	and isnull(target.[PatientObjectID], 0) = isnull(source.[PatientObjectID], 0)
	and isnull(target.[PatientFullName], '') = isnull(source.[PatientFullName], '')
	and isnull(target.[PatientMedicalRecordNo], '') = isnull(source.[PatientMedicalRecordNo], '')
	and isnull(target.[StartDate], getdate()) = isnull(source.[StartDate], getdate())
	and isnull(target.[StopDate], getdate()) = isnull(source.[StopDate], getdate())
	and isnull(target.[ModalityCode], '') = isnull(source.[ModalityCode], '')
	and isnull(target.[ModalitySettingCode], '') = isnull(source.[ModalitySettingCode], '')
	and isnull(target.[DialysisProviderCode], '') = isnull(source.[DialysisProviderCode], '')
	and isnull(target.[EventDetailCode], '') = isnull(source.[EventDetailCode], '')
	and isnull(target.[InformationSourceCode], '') = isnull(source.[InformationSourceCode], '')
	and isnull(target.[ReasonForChangeCode], '') = isnull(source.[ReasonForChangeCode], '')
	and isnull(target.[Notes], '') = isnull(source.[Notes], '')
	and isnull(target.[IsPrimary], 0) = isnull(source.[IsPrimary], 0)
	and isnull(target.[AgeAtEvent], 0) = isnull(source.[AgeAtEvent], 0)
)
then update set
	target.[SourceUniqueID] = source.[SourceUniqueID]
	,target.[PatientObjectID] = source.[PatientObjectID]
	,target.[PatientFullName] = source.[PatientFullName]
	,target.[PatientMedicalRecordNo] = source.[PatientMedicalRecordNo]
	,target.[StartDate] = source.[StartDate]
	,target.[StopDate] = source.[StopDate]
	,target.[ModalityCode] = source.[ModalityCode]
	,target.[ModalitySettingCode] = source.[ModalitySettingCode]
	,target.[DialysisProviderCode] = source.[DialysisProviderCode]
	,target.[EventDetailCode] = source.[EventDetailCode]
	,target.[InformationSourceCode] = source.[InformationSourceCode]
	,target.[ReasonForChangeCode] = source.[ReasonForChangeCode]
	,target.[Notes] = source.[Notes]
	,target.[IsPrimary] = source.[IsPrimary]
	,target.[AgeAtEvent] = source.[AgeAtEvent]
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	 [SourceUniqueID]
	,[PatientObjectID]
	,[PatientFullName]
	,[PatientMedicalRecordNo]
	,[StartDate]
	,[StopDate]
	,[ModalityCode]
	,[ModalitySettingCode]
	,[DialysisProviderCode]
	,[EventDetailCode]
	,[InformationSourceCode]
	,[ReasonForChangeCode]
	,[Notes]
	,[IsPrimary]
	,[AgeAtEvent]
	,[PatientRenalModalityRecno]
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[SourceUniqueID]
	,source.[PatientObjectID]
	,source.[PatientFullName]
	,source.[PatientMedicalRecordNo]
	,source.[StartDate]
	,source.[StopDate]
	,source.[ModalityCode]
	,source.[ModalitySettingCode]
	,source.[DialysisProviderCode]
	,source.[EventDetailCode]
	,source.[InformationSourceCode]
	,source.[ReasonForChangeCode]
	,source.[Notes]
	,source.[IsPrimary]
	,source.[AgeAtEvent]
	,source.[PatientRenalModalityRecno]
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

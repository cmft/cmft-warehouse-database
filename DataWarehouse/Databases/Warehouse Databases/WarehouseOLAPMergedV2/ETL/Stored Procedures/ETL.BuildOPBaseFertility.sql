﻿

CREATE Procedure ETL.BuildOPBaseFertility 
as
 

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	OP.BaseFertility target
using
	(
	select
		FertilityRecno
		,SourceID 
		,HospitalNumber
		,PatientForename
		,PatientSurname
		,EncounterDate
		,TreatmentType
		,DateOfBirth 
		,Age
		,ReferralSource
		,InterfaceCode 
		,ContextCode
	from
		ETL.TLoadOPBaseFertility
	) source
	on	source.SourceID = target.SourceID
	and source.EncounterDate = target.EncounterDate
	
	when not matched by source
	
	then delete

	when not matched
	then
		insert
			(
			FertilityRecno
			,SourceID 
			,HospitalNumber
			,PatientForename
			,PatientSurname
			,EncounterDate
			,TreatmentType
			,DateOfBirth 
			,Age
			,ReferralSource
			,InterfaceCode 
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.FertilityRecno
			,source.SourceID 
			,source.HospitalNumber
			,source.PatientForename
			,source.PatientSurname
			,source.EncounterDate
			,source.TreatmentType
			,source.DateOfBirth 
			,source.Age
			,source.ReferralSource
			,source.InterfaceCode 
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.FertilityRecno, 0) = isnull(source.FertilityRecno, 0)
		and isnull(target.SourceID, '') = isnull(source.SourceID, '')
		and isnull(target.HospitalNumber, '') = isnull(source.HospitalNumber, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.EncounterDate, getdate()) = isnull(source.EncounterDate, getdate())
		and isnull(target.TreatmentType, '') = isnull(source.TreatmentType, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.Age, 0) = isnull(source.Age, 0)
		and isnull(target.ReferralSource, '') = isnull(source.ReferralSource, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	
	then
		update
		set
			target.FertilityRecno = source.FertilityRecno
			,target.SourceID = source.SourceID
			,target.HospitalNumber = source.HospitalNumber
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.EncounterDate = source.EncounterDate
			,target.TreatmentType = source.TreatmentType
			,target.DateOfBirth = source.DateOfBirth
			,target.Age = source.Age
			,target.ReferralSource = source.ReferralSource
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
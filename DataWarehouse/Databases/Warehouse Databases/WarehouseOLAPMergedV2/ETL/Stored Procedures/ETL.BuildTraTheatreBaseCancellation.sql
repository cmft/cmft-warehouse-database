﻿create procedure [ETL].[BuildTraTheatreBaseCancellation]

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	Theatre.BaseCancellation target
using
	(
	select
		 SourceUniqueID
		,CancellationDate
		,CancelReasonCode
		,CancelReasonCode1
		,CancellationComment = cast(CancellationComment as varchar(max))
		,OperationDetailSourceUniqueID
		,ConsultantCode
		,SpecialtyCode
		,SurgeonCode
		,SurgeonSpecialtyCode
		,ProposedOperationDate
		,DistrictNo
		,Forename
		,Surname
		,WardCode
		,WardCode1
		,PreMedGivenFlag
		,FastedFlag
		,LastUpdated
		,PatientClassificationCode
		,PatientSourceUniqueID
		,CampusCode
		,TheatreCode
		,TheatreCode1
		,CancellationSurgeonCode
		,InitiatorCode
		,PriorityCode
		,AdmissionDate
		,ContextCode = 'TRA||ORMIS'
	from
		[$(TraffordWarehouse)].Theatre.Cancellation
	) source
	on	source.SourceUniqueID = target.SourceUniqueID
	and	source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'TRA||ORMIS'
	then delete

	when not matched
	then
		insert
			(
			 SourceUniqueID
			,CancellationDate
			,CancelReasonCode
			,CancelReasonCode1
			,CancellationComment
			,OperationDetailSourceUniqueID
			,ConsultantCode
			,SpecialtyCode
			,SurgeonCode
			,SurgeonSpecialtyCode
			,ProposedOperationDate
			,DistrictNo
			,Forename
			,Surname
			,WardCode
			,WardCode1
			,PreMedGivenFlag
			,FastedFlag
			,LastUpdated
			,PatientClassificationCode
			,PatientSourceUniqueID
			,CampusCode
			,TheatreCode
			,TheatreCode1
			,CancellationSurgeonCode
			,InitiatorCode
			,PriorityCode
			,AdmissionDate
			,ContextCode
			)
		values
			(
			 source.SourceUniqueID
			,source.CancellationDate
			,source.CancelReasonCode
			,source.CancelReasonCode1
			,source.CancellationComment
			,source.OperationDetailSourceUniqueID
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.SurgeonCode
			,source.SurgeonSpecialtyCode
			,source.ProposedOperationDate
			,source.DistrictNo
			,source.Forename
			,source.Surname
			,source.WardCode
			,source.WardCode1
			,source.PreMedGivenFlag
			,source.FastedFlag
			,source.LastUpdated
			,source.PatientClassificationCode
			,source.PatientSourceUniqueID
			,source.CampusCode
			,source.TheatreCode
			,source.TheatreCode1
			,source.CancellationSurgeonCode
			,source.InitiatorCode
			,source.PriorityCode
			,source.AdmissionDate
			,source.ContextCode
			)

	when matched
	and not
		(
			isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and isnull(target.CancellationDate, getdate()) = isnull(source.CancellationDate, getdate())
		and isnull(target.CancelReasonCode, 0) = isnull(source.CancelReasonCode, 0)
		and isnull(target.CancelReasonCode1, '') = isnull(source.CancelReasonCode1, '')
		and isnull(target.CancellationComment, '') = isnull(source.CancellationComment, '')
		and isnull(target.OperationDetailSourceUniqueID, 0) = isnull(source.OperationDetailSourceUniqueID, 0)
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.SpecialtyCode, 0) = isnull(source.SpecialtyCode, 0)
		and isnull(target.SurgeonCode, 0) = isnull(source.SurgeonCode, 0)
		and isnull(target.SurgeonSpecialtyCode, 0) = isnull(source.SurgeonSpecialtyCode, 0)
		and isnull(target.ProposedOperationDate, getdate()) = isnull(source.ProposedOperationDate, getdate())
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.Forename, '') = isnull(source.Forename, '')
		and isnull(target.Surname, '') = isnull(source.Surname, '')
		and isnull(target.WardCode, 0) = isnull(source.WardCode, 0)
		and isnull(target.WardCode1, '') = isnull(source.WardCode1, '')
		and isnull(target.PreMedGivenFlag, 0) = isnull(source.PreMedGivenFlag, 0)
		and isnull(target.FastedFlag, 0) = isnull(source.FastedFlag, 0)
		and isnull(target.LastUpdated, getdate()) = isnull(source.LastUpdated, getdate())
		and isnull(target.PatientClassificationCode, 0) = isnull(source.PatientClassificationCode, 0)
		and isnull(target.PatientSourceUniqueID, 0) = isnull(source.PatientSourceUniqueID, 0)
		and isnull(target.CampusCode, 0) = isnull(source.CampusCode, 0)
		and isnull(target.TheatreCode, 0) = isnull(source.TheatreCode, 0)
		and isnull(target.TheatreCode1, '') = isnull(source.TheatreCode1, '')
		and isnull(target.CancellationSurgeonCode, 0) = isnull(source.CancellationSurgeonCode, 0)
		and isnull(target.InitiatorCode, 0) = isnull(source.InitiatorCode, 0)
		and isnull(target.PriorityCode, 0) = isnull(source.PriorityCode, 0)
		and isnull(target.AdmissionDate, getdate()) = isnull(source.AdmissionDate, getdate())
		)
	then
		update
		set
			 target.SourceUniqueID = source.SourceUniqueID
			,target.CancellationDate = source.CancellationDate
			,target.CancelReasonCode = source.CancelReasonCode
			,target.CancelReasonCode1 = source.CancelReasonCode1
			,target.CancellationComment = source.CancellationComment
			,target.OperationDetailSourceUniqueID = source.OperationDetailSourceUniqueID
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.SurgeonCode = source.SurgeonCode
			,target.SurgeonSpecialtyCode = source.SurgeonSpecialtyCode
			,target.ProposedOperationDate = source.ProposedOperationDate
			,target.DistrictNo = source.DistrictNo
			,target.Forename = source.Forename
			,target.Surname = source.Surname
			,target.WardCode = source.WardCode
			,target.WardCode1 = source.WardCode1
			,target.PreMedGivenFlag = source.PreMedGivenFlag
			,target.FastedFlag = source.FastedFlag
			,target.LastUpdated = source.LastUpdated
			,target.PatientClassificationCode = source.PatientClassificationCode
			,target.PatientSourceUniqueID = source.PatientSourceUniqueID
			,target.CampusCode = source.CampusCode
			,target.TheatreCode = source.TheatreCode
			,target.TheatreCode1 = source.TheatreCode1
			,target.CancellationSurgeonCode = source.CancellationSurgeonCode
			,target.InitiatorCode = source.InitiatorCode
			,target.PriorityCode = source.PriorityCode
			,target.AdmissionDate = source.AdmissionDate

output
	$action into @MergeSummary
;


if @@ROWCOUNT = 0

	select
		 @inserted = 0
		,@updated = 0
		,@deleted = 0

else

	select
		 @inserted = sum(Inserted)
		,@updated = sum(Updated)
		,@deleted = sum(Deleted)
	from
		(
		select
			 Inserted = case when Action = 'INSERT' then 1 else 0 end
			,Updated = case when Action = 'UPDATE' then 1 else 0 end
			,Deleted = case when Action = 'DELETE' then 1 else 0 end
		from
			@MergeSummary
		) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

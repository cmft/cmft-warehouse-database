﻿CREATE procedure [ETL].[ImportAPCBaseStaffingLevel] as

--import the data
exec ETL.BuildAPCBaseStaffingLevel


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseStaffingLevel]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCBaseStaffingLevelReference 'CMFT||IPATH'


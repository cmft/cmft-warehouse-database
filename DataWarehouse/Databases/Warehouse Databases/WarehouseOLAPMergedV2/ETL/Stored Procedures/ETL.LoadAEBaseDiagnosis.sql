﻿CREATE procedure [ETL].[LoadAEBaseDiagnosis]
as

-------------------------------------------------
--When		Who	What
--20141215	PDO	Created procedure
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE
	(
	 MergeRecno int
	,Action nvarchar(10)
	);


merge
	AE.BaseDiagnosis target
using
	(
	select
		 MergeEncounterRecno
		,SequenceNo
		,DiagnosticSchemeCode
		,DiagnosisCode
		,DiagnosisSiteCode
		,DiagnosisSideCode
		,ContextCode
		,DiagnosisRecno
		,Action
	from
		ETL.TLoadAEBaseDiagnosis
	) source
	on	source.DiagnosisRecno = target.DiagnosisRecno
	and	source.ContextCode = target.ContextCode

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,SequenceNo
			,DiagnosticSchemeCode
			,DiagnosisCode
			,DiagnosisSiteCode
			,DiagnosisSideCode
			,ContextCode
			,DiagnosisRecno
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.SequenceNo
			,source.DiagnosticSchemeCode
			,source.DiagnosisCode
			,source.DiagnosisSiteCode
			,source.DiagnosisSideCode
			,source.ContextCode
			,source.DiagnosisRecno
			,getdate()
			,suser_name()
			)

	when matched
	and	source.Action <> 'DELETE'
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.SequenceNo = source.SequenceNo
			,target.DiagnosticSchemeCode = source.DiagnosticSchemeCode
			,target.DiagnosisCode = source.DiagnosisCode
			,target.DiagnosisSiteCode = source.DiagnosisSiteCode
			,target.DiagnosisSideCode = source.DiagnosisSideCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.MergeRecno, deleted.MergeRecno)
	,$action
	into
		@MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



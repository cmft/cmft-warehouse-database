﻿CREATE procedure [ETL].[ImportCenCOMBaseWardStay] as


--import the data
exec ETL.BuildCenCOMBaseWardStay


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[COM].[BaseWardStay]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildCOMBaseWardStayReference 'CEN||IPM'

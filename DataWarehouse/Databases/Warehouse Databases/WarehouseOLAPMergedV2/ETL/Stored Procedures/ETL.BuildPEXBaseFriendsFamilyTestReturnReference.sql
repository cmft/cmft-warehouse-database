﻿


Create procedure [ETL].[BuildPEXBaseFriendsFamilyTestReturnReference] 
	@ContextCode varchar(20)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)
		

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				PEX.BaseFriendsFamilyTestReturnReference
			where
				BaseFriendsFamilyTestReturnReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)

delete
from
	PEX.BaseFriendsFamilyTestReturnReference
where
	not exists
	(
	select
		1
	from
		PEX.BaseFriendsFamilyTestReturn
	where
		BaseFriendsFamilyTestReturn.MergeFFTRecno = BaseFriendsFamilyTestReturnReference.MergeFFTRecno
	)
and	BaseFriendsFamilyTestReturnReference.ContextID = @ContextID


select
	@deleted = @@ROWCOUNT


merge
	PEX.BaseFriendsFamilyTestReturnReference target
using
	(
	select
		 Encounter.MergeFFTRecno
		,ContextID = Context.ContextID
		,SiteID = Site.SourceSiteID
		,WardID = Ward.SourceWardID
		,CensusDateID = coalesce(Calendar.DateID
							,case
								when Encounter.CensusDate is null then NullDate.DateID
								when Encounter.CensusDate < CalendarStartDate.DateValue then PreviousDate.DateID
								else FutureDate.DateID
							end
							)
		--,ConsultantID = Consultant.SourceConsultantID
		--,SpecialtyID = Specialty.SourceSpecialtyID
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		PEX.BaseFriendsFamilyTestReturn Encounter

	inner join WH.Context 
	on	Context.ContextCode = Encounter.ContextCode

	inner join WH.Site 
	on	Site.SourceSiteCode = coalesce(Encounter.HospitalSiteCode, '-1') 
	and	Site.SourceContextCode = Encounter.ContextCode
	
	inner join WH.Ward 
	on	Ward.SourceWardCode = coalesce(Encounter.WardCode, '-1') 
	and	Ward.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar 
	on	Calendar.TheDate = Encounter.CensusDate

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'
	
	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'
	
	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'
	
	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	--inner join WH.Specialty -- don't have specialty in the data
	--on	Specialty.SourceContextCode = Encounter.ContextCode
	
	--inner join WH.Consultant Consultant -- don't have specialty in the data
	--on	Consultant.SourceContextCode = Encounter.ContextCode

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode
	) source
on	source.MergeFFTRecno = target.MergeFFTRecno


when not matched
then
	insert
		(
		 MergeFFTRecno
		,ContextID 
		,SiteID 
		,WardID 
		,CensusDateID
		--,ConsultantID
		--,SpecialtyID 
		,Created
		,Updated
		,ByWhom
		)
	values
		(
		 source.MergeFFTRecno
		,source.ContextID 
		,source.SiteID 
		,source.WardID 
		,source.CensusDateID
		--,source.ConsultantID
		--,source.SpecialtyID 
		,source.Created
		,source.Updated
		,source.ByWhom
		)

when matched and not 
	(
	target.Updated = source.Updated
	)
then
	update
	set
		target.MergeFFTRecno = source.MergeFFTRecno
		,target.ContextID = source.ContextID
		,target.SiteID  = source.SiteID
		,target.WardID  = source.WardID
		,target.CensusDateID = source.CensusDateID 
		--,target.SpecialtyID = source.SpecialtyID
		--,target.ConsultantID = source.ConsultantID
		,target.Created = source.Created
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats







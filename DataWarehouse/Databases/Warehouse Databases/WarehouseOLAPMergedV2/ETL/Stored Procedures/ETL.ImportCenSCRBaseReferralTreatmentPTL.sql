﻿CREATE procedure [ETL].[ImportCenSCRBaseReferralTreatmentPTL] as

--import the data
exec ETL.BuildCenSCRBaseReferralTreatmentPTL


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[SCR].[BaseReferralTreatmentPTL]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildSCRBaseReferralTreatmentPTLReference 'CEN||SCR'


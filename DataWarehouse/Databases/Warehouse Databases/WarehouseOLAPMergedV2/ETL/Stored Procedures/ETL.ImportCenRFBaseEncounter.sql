﻿CREATE procedure [ETL].[ImportCenRFBaseEncounter] as

--import the data
exec ETL.BuildCenRFBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[RF].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember
	
if @NewRows > 0
	exec  ETL.BuildWHConsultantBase	


--Build the BaseReference table
exec ETL.BuildRFBaseEncounterReference 'CEN||PAS'

﻿
CREATE procedure [ETL].[ImportPEXBaseFriendsFamilyTestReturn] as

--import the data
exec ETL.BuildPEXBaseFriendsFamilyTestReturn


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[PEX].[BaseFriendsFamilyTestReturn]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildPEXBaseFriendsFamilyTestReturnReference 'CMFT||FFTRTN'




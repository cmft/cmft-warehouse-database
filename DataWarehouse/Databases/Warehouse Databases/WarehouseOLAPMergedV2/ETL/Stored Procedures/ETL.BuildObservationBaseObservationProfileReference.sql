﻿CREATE procedure [ETL].[BuildObservationBaseObservationProfileReference]

	@ContextCode varchar(50)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Observation.BaseObservationProfileReference
			where
				BaseObservationProfileReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Observation.BaseObservationProfileReference
where
	not exists
	(
	select
		1
	from
		Observation.BaseObservationProfile
	where
		BaseObservationProfile.MergeObservationProfileRecno = BaseObservationProfileReference.MergeObservationProfileRecno
	)
and	BaseObservationProfileReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Observation.BaseObservationProfileReference target
using
	(
	select
		 ObservationProfile.MergeObservationProfileRecno
		,ContextID = Context.ContextID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,LocationID = Location.SourceLocationID
		,ProfileID = null 
		,ProfileTemplateID = Profile.SourceProfileID
		,TemplatePriorityID = null
		,DrivingTableID = null
		,ProfileReasonID = ProfileReason.SourceProfileReasonID
		,StartDateID = 
				coalesce(
						 ObservationProfileStartDate.DateID

						,case
						when cast(ObservationProfile.StartTime as date) is null
						then NullDate.DateID

						when cast(ObservationProfile.StartTime as date) < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,EndDateID = 
				coalesce(
						 ObservationProfileEndDate.DateID

						,case
						when cast(ObservationProfile.EndTime as date) is null
						then NullDate.DateID

						when cast(ObservationProfile.EndTime as date) < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,ObservationProfile.Created
		,ObservationProfile.Updated
		,ObservationProfile.ByWhom
	from
		Observation.BaseObservationProfile ObservationProfile

	inner join WH.Context
	on	Context.ContextCode = ObservationProfile.ContextCode

	inner join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(ObservationProfile.SpecialtyID, '-1')
	and	Specialty.SourceContextCode = ObservationProfile.ContextCode

	inner join WH.Location Location
	on	Location.SourceLocationCode = coalesce(ObservationProfile.LocationID, '-1')
	and	Location.SourceContextCode = ObservationProfile.ContextCode

	inner join Observation.Profile
	on	Profile.SourceProfileCode = coalesce(ObservationProfile.ProfileTemplateID, '-1')
	and	Profile.SourceContextCode = ObservationProfile.ContextCode

	inner join Observation.ProfileReason
	on	ProfileReason.SourceProfileReasonCode = coalesce(ObservationProfile.ProfileReasonID, '-1')
	and	ProfileReason.SourceContextCode = ObservationProfile.ContextCode

	left join WH.Calendar ObservationProfileStartDate
	on	ObservationProfileStartDate.TheDate = cast(ObservationProfile.StartTime as date)

	left join WH.Calendar ObservationProfileEndDate
	on	ObservationProfileEndDate.TheDate = cast(ObservationProfile.StartTime as date)

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		ObservationProfile.Updated > @LastUpdated
	and	ObservationProfile.ContextCode = @ContextCode

	) source
	on	source.MergeObservationProfileRecno = target.MergeObservationProfileRecno

	when not matched
	then
		insert
			(
			 MergeObservationProfileRecno
			,ContextID
			,SpecialtyID
			,LocationID
			,ProfileID
			,ProfileTemplateID
			,TemplatePriorityID
			,DrivingTableID
			,ProfileReasonID
			,StartDateID
			,EndDateID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeObservationProfileRecno
			,source.ContextID
			,source.SpecialtyID
			,source.LocationID
			,source.ProfileID
			,source.ProfileTemplateID
			,source.TemplatePriorityID
			,source.DrivingTableID
			,source.ProfileReasonID
			,source.StartDateID
			,source.EndDateID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and	target.Updated != source.Updated
	then
		update
		set
			target.ContextID = source.ContextID
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.ProfileID = source.ProfileID
			,target.ProfileTemplateID = source.ProfileTemplateID
			,target.TemplatePriorityID = source.TemplatePriorityID
			,target.DrivingTableID = source.DrivingTableID
			,target.ProfileReasonID = source.ProfileReasonID
			,target.StartDateID = source.StartDateID
			,target.EndDateID = source.EndDateID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats	
	,@StartTime


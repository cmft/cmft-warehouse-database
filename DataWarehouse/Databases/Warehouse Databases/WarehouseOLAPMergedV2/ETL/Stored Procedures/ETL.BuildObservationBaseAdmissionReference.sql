﻿CREATE procedure [ETL].[BuildObservationBaseAdmissionReference]

	@ContextCode varchar(50)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Observation.BaseAdmissionReference
			where
				BaseAdmissionReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Observation.BaseAdmissionReference
where
	not exists
	(
	select
		1
	from
		Observation.BaseAdmission
	where
		BaseAdmission.MergeAdmissionRecno = BaseAdmissionReference.MergeAdmissionRecno
	)
and	BaseAdmissionReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Observation.BaseAdmissionReference target
using
	(
	select
		 Admission.MergeAdmissionRecno
		,ContextID = Context.ContextID
		,SexID = Sex.SourceSexID
		,AdmissionDateID = 
				coalesce(
						 AdmissionDate.DateID

						,case
						when cast(Admission.AdmissionTime as date) is null
						then NullDate.DateID

						when cast(Admission.AdmissionTime as date) < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,StartSpecialtyID = StartSpecialty.SourceSpecialtyID
		,StartLocationID = StartLocation.SourceLocationID
		,DischargeDateID = 
				coalesce(
						 DischargeDate.DateID

						,case
						when cast(Admission.DischargeTime as date) is null
						then NullDate.DateID

						when cast(Admission.DischargeTime as date) < CalendarStartDate.DateValue
						then PreviousDate.DateID

						else FutureDate.DateID
						end
					)
		,EndSpecialtyID = EndSpecialty.SourceSpecialtyID
		,EndLocationID = EndLocation.SourceLocationID
		,TreatmentOutcomeID = Outcome.SourceOutcomeID
		,DischargeDestinationID = DischargeDestination.DischargeDestinationID
		,Admission.Created
		,Admission.Updated
		,Admission.ByWhom
	from
		Observation.BaseAdmission Admission

	inner join WH.Context
	on	Context.ContextCode = Admission.ContextCode

	inner join WH.Sex
	on	Sex.SourceSexCode = coalesce(Admission.SexID, '-1')
	and	Sex.SourceContextCode = Admission.ContextCode

	inner join WH.Specialty StartSpecialty
	on	StartSpecialty.SourceSpecialtyCode = coalesce(Admission.StartSpecialtyID, '-1')
	and	StartSpecialty.SourceContextCode = Admission.ContextCode

	left join WH.Specialty EndSpecialty
	on	EndSpecialty.SourceSpecialtyCode = coalesce(Admission.EndSpecialtyID, '-1')
	and	EndSpecialty.SourceContextCode = Admission.ContextCode

	inner join WH.Location StartLocation
	on	StartLocation.SourceLocationCode = coalesce(Admission.StartLocationID, '-1')
	and	StartLocation.SourceContextCode = Admission.ContextCode

	left join WH.Location EndLocation
	on	EndLocation.SourceLocationCode = coalesce(Admission.EndLocationID, '-1')
	and	EndLocation.SourceContextCode = Admission.ContextCode

	left join WH.Outcome Outcome
	on	Outcome.SourceOutcomeCode = coalesce(Admission.TreatmentOutcomeID, '-1')
	and	Outcome.SourceContextCode = Admission.ContextCode

	left join WH.DischargeDestination DischargeDestination
	on	DischargeDestination.DischargeDestinationID = coalesce(Admission.DischargeDestinationID, '-1')
	and	DischargeDestination.ContextCode = Admission.ContextCode

	left join WH.Calendar AdmissionDate
	on	AdmissionDate.TheDate = cast(Admission.AdmissionTime as date)

	left join WH.Calendar DischargeDate
	on	DischargeDate.TheDate = cast(Admission.DischargeTime as date)

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		Admission.Updated > @LastUpdated
	and	Admission.ContextCode = @ContextCode

	) source
	on	source.MergeAdmissionRecno = target.MergeAdmissionRecno

	when not matched
	then
		insert
			(
			 MergeAdmissionRecno
			,ContextID
			,SexID
			,AdmissionDateID
			,StartSpecialtyID
			,StartLocationID
			,DischargeDateID
			,EndSpecialtyID
			,EndLocationID
			,TreatmentOutcomeID
			,DischargeDestinationID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeAdmissionRecno
			,source.ContextID
			,source.SexID
			,source.AdmissionDateID
			,source.StartSpecialtyID
			,source.StartLocationID
			,source.DischargeDateID
			,source.EndSpecialtyID
			,source.EndLocationID
			,source.TreatmentOutcomeID
			,source.DischargeDestinationID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and	target.Updated != source.Updated
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.SexID = source.SexID
			,target.AdmissionDateID = source.AdmissionDateID
			,target.StartSpecialtyID = source.StartSpecialtyID
			,target.StartLocationID = source.StartLocationID
			,target.DischargeDateID = source.DischargeDateID
			,target.EndSpecialtyID = source.EndSpecialtyID
			,target.EndLocationID = source.EndLocationID
			,target.TreatmentOutcomeID = source.TreatmentOutcomeID
			,target.DischargeDestinationID = source.DischargeDestinationID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime



﻿CREATE procedure [ETL].[LoadOPBaseDiary]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 EncounterChecksum =
		CHECKSUM(
			 SourceUniqueID
			,ClinicCode
			,SessionCode
			,SessionDescription
			,SessionStartTime
			,SessionEndTime
			,ReasonForCancellation
			,SessionPeriod
			,DoctorCode
			,Units
			,UsedUnits
			,FreeUnits
			,ValidAppointmentTypeCode
		)
	,*
	,CensusDate = GETDATE()
into
	#Encounter
from
	(
	select
		 ContextCode = CAST('CEN||PAS' as varchar(10))
		,SourceUniqueID = cast(SourceUniqueID as varchar(254))
		,ClinicCode = cast(ClinicCode as varchar(8))
		,SessionCode = cast(SessionCode as varchar(8))
		,SessionDescription = cast(SessionDescription as varchar(255))
		,SessionStartTime = cast(SessionStartTime as smalldatetime)
		,SessionEndTime = cast(SessionEndTime as smalldatetime)
		,ReasonForCancellation = cast(ReasonForCancellation as varchar(30))
		,SessionPeriod = cast(SessionPeriod as varchar(3))
		,DoctorCode = cast(DoctorCode as varchar(10))
		,Units = cast(Units as int)
		,UsedUnits = cast(UsedUnits as int)
		,FreeUnits = cast(FreeUnits as int)
		,ValidAppointmentTypeCode = cast(ValidAppointmentTypeCode as varchar(23))
	from
		[$(Warehouse)].OP.Diary
		
--the Trafford data does not have a unique key - need to speak to Gareth C about extracting this dataset...
	--union all
	--select
	--	 ContextCode
	--	,SourceUniqueID

	--	,ClinicCode
	--	,SessionCode
	--	,SessionDescription
	--	,SessionStartTime
	--	,SessionEndTime
	--	,ReasonForCancellation
	--	,SessionPeriod
	--	,DoctorCode
	--	,Units = sum(Units)
	--	,UsedUnits = sum(UsedUnits)
	--	,FreeUnits = sum(Units) - sum(UsedUnits)
	--	,ValidAppointmentTypeCode
	--from
	--	(
	--	select
	--		 ContextCode = CAST('TRA||UG' as varchar(10))
	--		,SourceUniqueID = cast(SourceUniqueID as varchar(254))
	--		,ClinicCode = cast(ClinicCode as varchar(8))
	--		,SessionCode = cast(SessionCode as varchar(8))
	--		,SessionDescription = cast(SessionDescription as varchar(255))
	--		,SessionStartTime = cast(SessionStartTime as smalldatetime)
	--		,SessionEndTime = cast(SessionEndTime as smalldatetime)
	--		,ReasonForCancellation = cast(ReasonForCancellation as varchar(30))
	--		,SessionPeriod = cast(SessionPeriod as varchar(3))
	--		,DoctorCode = cast(DoctorCode as varchar(10))
	--		,Units = cast(Units as int)
	--		,UsedUnits = cast(UsedUnits as int)
	--		,FreeUnits = cast(Units as int) - cast(UsedUnits as int)
	--		,ValidAppointmentTypeCode = cast(ValidAppointmentTypeCode as varchar(23))
	--	from
	--		(
	--		select
	--			 SourceUniqueID =
	--				BaseDiary.LocationAbbreviation + '||' + 
	--				BaseDiary.SessionAbbreviation + '||' + 
	--				cast(BaseDiary.ScheduleId as varchar) + '||' + 
	--				convert(varchar, BaseDiary.SessionStartTime, 112) + replace(convert(varchar, BaseDiary.SessionStartTime, 108), ':', '') + '||' + 
	--				convert(varchar, BaseDiary.SessionEndTime, 112) + replace(convert(varchar, BaseDiary.SessionEndTime, 108), ':', '')
					
	--			,ClinicCode = BaseDiary.LocationAbbreviation
	--			,SessionCode = case when BaseDiary.SessionAbbreviation = '' then null else BaseDiary.SessionAbbreviation end
	--			,SessionDescription = case when BaseDiary.SessionName = '' then null else BaseDiary.SessionName end
	--			,SessionStartTime = BaseDiary.SessionStartTime
	--			,SessionEndTime = BaseDiary.SessionEndTime
	--			,ReasonForCancellation = null
	--			,SessionPeriod = BaseDiary.SessionNumber
	--			,DoctorCode = null
	--			,Units = 1
	--			,UsedUnits = case when BaseDiary.FreeSlotID is null then 0 else 1 end
	--			,ValidAppointmentTypeCode = null
	--		from
	--			TGHWarehouse.dbo.SlotCountUG BaseDiary
	--		) Diary
	--	) Diary

	--group by
	--	ContextCode
	--	,SourceUniqueID
	--	,ClinicCode
	--	,SessionCode
	--	,SessionDescription
	--	,SessionStartTime
	--	,SessionEndTime
	--	,ReasonForCancellation
	--	,SessionPeriod
	--	,DoctorCode
	--	,ValidAppointmentTypeCode

	

	) Encounter

CREATE UNIQUE CLUSTERED INDEX IX_#Encounter ON #Encounter
	(
	 ContextCode
	,SourceUniqueID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


insert
into
	OP.BaseDiary
	(
	 EncounterChecksum
	,CensusDate
	,ContextCode
	,SourceUniqueID
	,ClinicCode
	,SessionCode
	,SessionDescription
	,SessionStartTime
	,SessionEndTime
	,ReasonForCancellation
	,SessionPeriod
	,DoctorCode
	,Units
	,UsedUnits
	,FreeUnits
	,ValidAppointmentTypeCode
	)
select
	 source.EncounterChecksum
	,source.CensusDate
	,source.ContextCode
	,source.SourceUniqueID
	,source.ClinicCode
	,source.SessionCode
	,source.SessionDescription
	,source.SessionStartTime
	,source.SessionEndTime
	,source.ReasonForCancellation
	,source.SessionPeriod
	,source.DoctorCode
	,source.Units
	,source.UsedUnits
	,source.FreeUnits
	,source.ValidAppointmentTypeCode
from
	#Encounter source
where
	(
	--updated rows
		exists
		(
		select
			1
		from
			OP.BaseDiary target
		where
			target.ContextCode = source.ContextCode
		and	target.SourceUniqueID = source.SourceUniqueID
		and target.EncounterChecksum <> source.EncounterChecksum
		
		and	not exists
			(
			select
				1
			from
				OP.BaseDiary Previous
			where
				Previous.ContextCode = target.ContextCode
			and	Previous.SourceUniqueID = target.SourceUniqueID
			and	Previous.CensusDate > target.CensusDate
			)
		)

	--new rows
	or
		not exists
		(
		select
			1
		from
			OP.BaseDiary target
		where
			target.ContextCode = source.ContextCode
		and	target.SourceUniqueID = source.SourceUniqueID
		)
	)


select
	 @inserted = @@ROWCOUNT

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




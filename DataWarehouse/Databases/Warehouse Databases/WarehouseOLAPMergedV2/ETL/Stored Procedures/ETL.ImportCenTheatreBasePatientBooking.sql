﻿create procedure ETL.ImportCenTheatreBasePatientBooking as

--import the data
exec ETL.BuildCenTheatreBasePatientBooking


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Theatre].[BasePatientBooking]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildTheatreBasePatientBookingReference 'CEN||ORMIS'



﻿CREATE procedure [ETL].[ImportCenAPCBaseEncounter] as

--import the data
exec ETL.BuildCenAPCBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCBaseEncounterReference 'CEN||PAS'


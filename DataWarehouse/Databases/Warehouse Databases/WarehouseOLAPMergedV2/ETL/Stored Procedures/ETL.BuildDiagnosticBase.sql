﻿CREATE procedure [ETL].[BuildDiagnosticBase] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Diagnostic.Base


insert
into
	Diagnostic.Base
(
	 SourceMergeRecno
	,ContextCode
	,EventDate
	,DirectorateCode
	,CCGCode
	,NationalExamCode
	,ExamCode
	,DataSourceID
	,SpecialtyCode
	,ProfessionalCarerCode
	,AgeCode
	,WeeksWaitingAtTCI
	,ServiceRequestTypeCode
	,IsCancelled
)
select
	 Diagnostic.SourceMergeRecno
	,Diagnostic.ContextCode
	,Diagnostic.EventDate
	,Diagnostic.DirectorateCode
	,Diagnostic.CCGCode
	,Diagnostic.NationalExamCode
	,Diagnostic.ExamCode
	,DataSource.DataSourceID
	,Diagnostic.SpecialtyCode
	,Diagnostic.ProfessionalCarerCode
	,Diagnostic.AgeCode
	,Diagnostic.WeeksWaitingAtTCI
	,Diagnostic.ServiceRequestTypeCode
	,Diagnostic.IsCancelled
from
	(
	select
		 SourceMergeRecno = Base.MergeRecno
		,Base.ContextCode
		,Base.EventDate
		,Base.DirectorateCode
		,Base.NationalExamCode

		,CCGCode =
			coalesce(
				 Base.CCGCode
				,Base.ResidenceCCGCode
			)

		,Base.ExamCode

		,DataSourceCode = 'RAD'

		,Base.SpecialtyCode
		,ProfessionalCarerCode = Base.RadiologistCode
		,Base.AgeCode

		,WeeksWaitingAtTCI =
			DATEDIFF(
				 day
				,Base.RequestDate
				,case when Base.EventDate < Base.RequestDate then null else Base.EventDate end
			) / 7

		,ServiceRequestTypeCode =
			case
			when Base.WasPlanned = 'Y' --Planned
			then '02' --Planned test required

			when Base.PatientTypeCode = 'A' --Inpatient
			then '04' --Test/procedure following Elective Admission

			else '01' --Waiting list for test/procedure
			end

		,IsCancelled = 0
	from
		Diagnostic.BaseRadiology Base

	union all

	select
		 Base.MergeEncounterRecno
		,Base.ContextCode
		,EventDate = Base.EpisodeStartDate
		,Base.StartDirectorateCode
		,DiagnosticNationalExamCode = Allocation.SourceAllocationID

		,CommissionerCode =
			coalesce(
				 Base.CCGCode
				,Base.ResidenceCCGCode
			)

		,ExamCode = null

		,DataSourceCode = 'APC'

		,Base.SpecialtyCode
		,ProfessionalCarerCode = Base.ConsultantCode
		,Base.AgeCode

		,WeeksWaitingAtTCI =
			DATEDIFF(
				 day
				,Base.DateOnWaitingList
				,case when Base.AdmissionDate < Base.DateOnWaitingList then null else Base.AdmissionDate end
			) / 7

		,ServiceRequestTypeCode = '03' --Emergency or unscheduled diagnostic test or procedure 

		,IsCancelled = 0

	from
		APC.BaseEncounter Base

	inner join Allocation.DatasetAllocation
	on	DatasetAllocation.DatasetCode = 'APC'
	and	DatasetAllocation.AllocationTypeID = 1
	and	DatasetAllocation.DatasetRecno = Base.MergeEncounterRecno
	and Base.Reportable = 1
	
	inner join Allocation.Allocation
	on	Allocation.AllocationID = DatasetAllocation.AllocationID
	and	Allocation.SourceAllocationID <> '#'

	union all

	select
		 Base.MergeEncounterRecno
		,Base.ContextCode
		,EventDate = Base.AppointmentDate
		,Base.DirectorateCode
		,DiagnosticNationalExamCode = Allocation.SourceAllocationID
		,CommissionerCode = Base.CCGCode
		,ExamCode = null
		,DataSourceCode = 'OP'
		,Base.SpecialtyCode
		,ProfessionalCarerCode = Base.ConsultantCode
		,Base.AgeCode

		,WeeksWaitingAtTCI = Base.LengthOfWait / 7

		,ServiceRequestTypeCode = '03' --Emergency or unscheduled diagnostic test or procedure 

		,IsCancelled =
			case
			when Base.CancelledByCode is null
			then 0
			else 1
			end

	from
		OP.BaseEncounter Base

	inner join Allocation.DatasetAllocation
	on	DatasetAllocation.DatasetCode = 'OP'
	and	DatasetAllocation.AllocationTypeID = 1
	and	DatasetAllocation.DatasetRecno = Base.MergeEncounterRecno

	inner join Allocation.Allocation
	on	Allocation.AllocationID = DatasetAllocation.AllocationID
	and	Allocation.SourceAllocationID <> '#'


	) Diagnostic

inner join Diagnostic.DataSource
on	DataSource.DataSourceCode = Diagnostic.DataSourceCode


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats




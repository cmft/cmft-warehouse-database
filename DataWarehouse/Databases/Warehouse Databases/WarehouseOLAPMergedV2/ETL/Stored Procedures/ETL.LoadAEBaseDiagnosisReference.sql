﻿CREATE procedure [ETL].[LoadAEBaseDiagnosisReference]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge
	AE.BaseDiagnosisReference target
using
	(
	select
		 ProcessList.MergeRecno
		,ProcessList.Action
		,Context.ContextID
		,DiagnosisID = Diagnosis.SourceDiagnosisID
	from
		AE.ProcessList

	left join AE.BaseDiagnosis
	on	BaseDiagnosis.MergeRecno = ProcessList.MergeRecno

	left join AE.Diagnosis
	on	Diagnosis.SourceDiagnosisCode = cast(coalesce(BaseDiagnosis.DiagnosisCode, '-1') as varchar)
	and	Diagnosis.SourceContextCode = BaseDiagnosis.ContextCode

	left join WH.Context
	on	Context.ContextCode = BaseDiagnosis.ContextCode
	
	where
		ProcessList.Dataset = 'Diagnosis'

	) source
	on	source.MergeRecno = target.MergeRecno

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	then

		insert
			(
			 MergeRecno
			,ContextID
			,DiagnosisID
			)
		values
			(
			 source.MergeRecno
			,source.ContextID
			,source.DiagnosisID
			)

	when matched
	and	source.Action <> 'DELETE'
	then
		update
		set
			 target.ContextID = source.ContextID
			,target.DiagnosisID = source.DiagnosisID

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


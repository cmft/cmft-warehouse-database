﻿create procedure [ETL].BuildSCRBaseDefinitiveTreatmentReference
	@ContextCode varchar(10)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				SCR.BaseDefinitiveTreatmentReference
			where
				BaseDefinitiveTreatmentReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	SCR.BaseDefinitiveTreatmentReference
where
	not exists
	(
	select
		1
	from
		SCR.BaseDefinitiveTreatment
	where
		BaseDefinitiveTreatment.UniqueRecordID = BaseDefinitiveTreatmentReference.UniqueRecordID
	)
and	BaseDefinitiveTreatmentReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	SCR.BaseDefinitiveTreatmentReference target
using
	(
	select
		 Treatment.UniqueRecordID
		,ContextID = Context.ContextID

		,FirstDefinitiveTreatmentDateID = FirstDefinitiveTreatmentDate.DateID

		,Treatment.Created
		,Treatment.Updated
		,Treatment.ByWhom
	from
		SCR.BaseDefinitiveTreatment Treatment

	inner join WH.Context
	on	Context.ContextCode = Treatment.ContextCode

	left join WH.Calendar FirstDefinitiveTreatmentDate
	on	FirstDefinitiveTreatmentDate.TheDate = Treatment.FirstDefinitiveTreatmentDate


	where
		Treatment.Updated > @LastUpdated
	and	Treatment.ContextCode = @ContextCode

	) source
	on	source.UniqueRecordID = target.UniqueRecordID

	when not matched
	then
		insert
			(
			 [UniqueRecordID]
			,[ContextID]
			,[FirstDefinitiveTreatmentDateID]
			,[Created]
			,[Updated]
			,[ByWhom]			
			)
		values
			(
			 source.[UniqueRecordID]
			,source.[ContextID]
			,source.[FirstDefinitiveTreatmentDateID]
			,source.[Created]
			,source.[Updated]
			,source.[ByWhom]			
			)

	when matched
	then
		update
		set
			 target.[UniqueRecordID] = source.[UniqueRecordID]
			,target.[ContextID] = source.[ContextID]
			,target.[FirstDefinitiveTreatmentDateID] = source.[FirstDefinitiveTreatmentDateID]
			,target.[Created] = source.[Created]
			,target.[Updated] = source.[Updated]
			,target.[ByWhom] = source.[ByWhom]

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



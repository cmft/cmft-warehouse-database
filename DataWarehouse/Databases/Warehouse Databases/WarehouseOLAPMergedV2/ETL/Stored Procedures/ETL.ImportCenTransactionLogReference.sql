﻿CREATE procedure ETL.ImportCenTransactionLogReference as

insert
into
	WH.SystemUserBase
(
	 UserID
	,ContextCode
	,Username
	,AlternateUserId
	,HospitalCode
)
select
	 UserId
	,ContextCode = 'CEN||PAS'
	,Username = UserIdName
	,AlternateUserId = GpxgpUsername
	,HospitalCode = UsersHospital
from
	[$(Warehouse)].PAS.UserBase
where
	not exists
	(
	select
		1
	from
		WH.SystemUserBase
	where
		SystemUserBase.UserID = UserBase.UserId
	and	SystemUserBase.ContextCode = 'CEN||PAS'
	)

--there is a pesky duplicate in UserBase (SMS)
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].PAS.UserBase Previous
	where
		Previous.UserId = UserBase.UserId
	and	Previous.GpxgpUsername > UserBase.GpxgpUsername
	)

set identity_insert WH.SystemUserBase on

insert
into
	WH.SystemUserBase
(
	 SystemUserID
	,UserID
	,ContextCode
	,Username
	,AlternateUserId
	,HospitalCode
)

select
	 -1
	,UserID = 'N/A'
	,ContextCode = 'N/A'
	,Username = 'N/A'
	,AlternateUserId = null
	,HospitalCode = null
where
	not exists
	(
	select
		1
	from
		WH.SystemUserBase
	where
		SystemUserBase.SystemUserID = -1
	)

set identity_insert WH.SystemUserBase off

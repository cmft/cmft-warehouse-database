﻿

create procedure [ETL].[BuildTraAEBaseAssault]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


delete
from
	AE.BaseAssault
where
	not exists
	(
	select
		1
	from
		ETL.TLoadTraAEBaseAssault
	where
		TLoadTraAEBaseAssault.MergeEncounterRecno = BaseAssault.MergeEncounterRecno
	)

and	exists
	(
	select
		1
	from
		AE.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseAssault.MergeEncounterRecno
	and	BaseEncounter.ContextCode in
		(
		 'Tra||ADAS'
		,'Tra||PAS'
		,'Tra||SYM'
		,'TRA||ADAS'
		)
	)

select
	@deleted = @@ROWCOUNT


merge
	AE.BaseAssault target
using
	(
	select
		 MergeEncounterRecno
		,AssaultDate = cast(AssaultDate as date)
		,AssaultTime = cast(AssaultTime as smalldatetime)
		,AssaultWeapon = cast(AssaultWeapon as varchar (80))
		,AssaultWeaponDetails = cast(AssaultWeaponDetails as varchar (4000))
		,AssaultLocation = cast(AssaultLocation as varchar (80))
		,AssaultLocationDetails= cast(AssaultLocationDetails as varchar (4000))
		,AlcoholConsumed3Hour = cast(AlcoholConsumed3Hour as varchar (80))
		,AssaultRelationship = cast(AssaultRelationship  as varchar (80))
		,AssaultRelationshipDetails = cast(AssaultRelationshipDetails as varchar (4000))
		
		,ContextCode
		,Created = getdate()
		,Updated = getdate()
		,ByWhom = suser_name()
	from
		ETL.TLoadTraAEBaseAssault Encounter
	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno
	

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,AssaultDate
			,AssaultTime
			,AssaultWeapon
			,AssaultWeaponDetails
			,AssaultLocation
			,AssaultLocationDetails
			,AlcoholConsumed3Hour
			,AssaultRelationship
			,AssaultRelationshipDetails
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.AssaultDate
			,source.AssaultTime
			,source.AssaultWeapon
			,source.AssaultWeaponDetails
			,source.AssaultLocation
			,source.AssaultLocationDetails
			,source.AlcoholConsumed3Hour
			,source.AssaultRelationship
			,source.AssaultRelationshipDetails
			,source.ContextCode
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			 isnull(target.AssaultDate, '') = isnull(source.AssaultDate, '')
			and isnull(target.AssaultTime, '') = isnull(source.AssaultTime, '')
			and isnull(target.AssaultWeapon, '') = isnull(source.AssaultWeapon, '')
			and isnull(target.AssaultWeaponDetails, '') = isnull(source.AssaultWeaponDetails, '')
			and isnull(target.AssaultLocation, '') = isnull(source.AssaultLocation, '')
			and isnull(target.AssaultLocationDetails, '') = isnull(source.AssaultLocationDetails, '')
			and isnull(target.AlcoholConsumed3Hour, '') = isnull(source.AlcoholConsumed3Hour, '')
			and isnull(target.AssaultRelationship, '') = isnull(source.AssaultRelationship, '')
			and isnull(target.AssaultRelationshipDetails, '') = isnull(source.AssaultRelationshipDetails, '')
			and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')



		
		)
	then
		update
		set

			target.AssaultDate = source.AssaultDate
			,target.AssaultTime = source.AssaultTime
			,target.AssaultWeapon = source.AssaultWeapon
			,target.AssaultWeaponDetails = source.AssaultWeaponDetails
			,target.AssaultLocation = source.AssaultLocation
			,target.AssaultLocationDetails = source.AssaultLocationDetails
			,target.AlcoholConsumed3Hour = source.AlcoholConsumed3Hour
			,target.AssaultRelationship = source.AssaultRelationship
			,target.AssaultRelationshipDetails = source.AssaultRelationshipDetails
			,target.ContextCode = source.ContextCode




output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





﻿CREATE procedure [ETL].[LoadAEBaseInvestigation]
as

-------------------------------------------------
--When		Who	What
--20141215	PDO	Created procedure
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE
	(
	 MergeRecno int
	,Action nvarchar(10)
	);


merge
	AE.BaseInvestigation target
using
	(
	select
		 MergeEncounterRecno
		,SequenceNo = 
			case
				when [Action] = 'DELETE' then null
				else row_number() over (partition by MergeEncounterRecno order by InvestigationTime, InvestigationCode, ContextCode, InvestigationRecno)
			end
		,InvestigationSchemeCode
		,InvestigationCode
		,InvestigationTime
		,ResultTime
		,ContextCode
		,InvestigationRecno
		,Action		
	from
		ETL.TLoadAEBaseInvestigation
	) source
	on	source.InvestigationRecno = target.InvestigationRecno
	and	source.ContextCode = target.ContextCode

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	then
		insert
			(
			 MergeEncounterRecno
			,SequenceNo
			,InvestigationSchemeCode
			,InvestigationCode
			,InvestigationTime
			,ResultTime
			,ContextCode
			,InvestigationRecno
			,Created
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.SequenceNo
			,source.InvestigationSchemeCode
			,source.InvestigationCode
			,source.InvestigationTime
			,source.ResultTime
			,source.ContextCode
			,source.InvestigationRecno
			,getdate()
			,suser_name()
			)

	when matched
	and	source.Action <> 'DELETE'
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.SequenceNo = source.SequenceNo
			,target.InvestigationSchemeCode = source.InvestigationSchemeCode
			,target.InvestigationCode = source.InvestigationCode
			,target.InvestigationTime = source.InvestigationTime
			,target.ResultTime = source.ResultTime
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	 coalesce(inserted.MergeRecno, deleted.MergeRecno)
	,$action
	into
		@MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	
	


﻿CREATE procedure [ETL].[BuildOPProcessListArchive] as

insert
into
	OP.ProcessListArchive
(
	 MergeRecno
	,Action
	,ArchiveTime
)
select
	 MergeRecno
	,Action
	,ArchiveTime = getdate()
from
	OP.ProcessList;


truncate table OP.ProcessList;

--once merged has proccessed correctly the Classic list can be cleared
exec [$(Warehouse)].ETL.BuildOPProcessListArchive;
truncate table [$(Warehouse)].OP.ProcessList;

﻿CREATE proc [ETL].[ImportCenObservation]

as

exec ETL.ImportCenObservationBaseAdmission
exec ETL.ImportCenObservationBaseAlert
exec ETL.ImportCenObservationBaseObservationSet
exec ETL.ImportCenObservationBaseObservationProfile
﻿CREATE procedure [ETL].[ImportCenObservationBaseAlert] as


--import the data
exec ETL.BuildCenObservationBaseAlert


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Observation].[BaseAlert]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildObservationBaseAlertReference 'CEN||PTRACK'


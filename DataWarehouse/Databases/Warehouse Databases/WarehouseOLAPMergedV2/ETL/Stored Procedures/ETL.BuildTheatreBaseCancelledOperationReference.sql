﻿

CREATE procedure [ETL].[BuildTheatreBaseCancelledOperationReference] 
	@ContextCode varchar(20)

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)
		

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Theatre.BaseCancelledOperationReference
			where
				BaseCancelledOperationReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)



delete
from
	Theatre.BaseCancelledOperationReference
where
	not exists
	(
	select
		1
	from
		Theatre.BaseCancelledOperation
	where
		BaseCancelledOperation.MergeCancelledOperationRecno = BaseCancelledOperationReference.MergeCancelledOperationRecno
	)
and	BaseCancelledOperationReference.ContextID = @ContextID


select
	@deleted = @@ROWCOUNT


merge
	Theatre.BaseCancelledOperationReference target
using
	(
	select
		 Encounter.MergeCancelledOperationRecno
		,ContextID = Context.ContextID
		,WardID = Ward.SourceWardID
		,CancelledOperationDateID = coalesce(
									 CancellationDate.DateID

									,case
									when Encounter.CancellationDate is null
									then NullDate.DateID

									when Encounter.CancellationDate < CalendarStartDate.DateValue
									then PreviousDate.DateID

									else FutureDate.DateID
									end
								)

		,ConsultantID = Consultant.SourceConsultantID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,Encounter.Created
		,Encounter.Updated
		,Encounter.ByWhom
	from
		Theatre.BaseCancelledOperation Encounter

	inner join WH.Context 
	on	Context.ContextCode = Encounter.ContextCode

	inner join WH.Specialty 
	on	Specialty.SourceSpecialtyCode = coalesce(Encounter.NationalSpecialtyCode, '-1')
	and	Specialty.SourceContextCode = Encounter.ContextCode

	inner join WH.Ward 
	on	Ward.SourceWardCode = coalesce(Encounter.WardCode, '-1') 
	and	Ward.SourceContextCode = Encounter.ContextCode

	left join WH.Calendar CancellationDate
	on	CancellationDate.TheDate = Encounter.CancellationDate

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'
	
	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'
	
	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'
	
	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	inner join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = cast(coalesce(Encounter.ConsultantCode, '-1') as varchar)
	and	Consultant.SourceContextCode = Encounter.ContextCode

	where
		Encounter.Updated > @LastUpdated
	and	Encounter.ContextCode = @ContextCode
	) source
on	source.MergeCancelledOperationRecno = target.MergeCancelledOperationRecno


when not matched
then
	insert
		(
		 MergeCancelledOperationRecno
		,ContextID 
		,WardID 
		,CancelledOperationDateID 
		,ConsultantID 
		,SpecialtyID
		,Created
		,Updated
		,ByWhom
		)
	values
		(
		 source.MergeCancelledOperationRecno
		,source.ContextID 
		,source.WardID 
		,source.CancelledOperationDateID 
		,source.ConsultantID 
		,source.SpecialtyID
		,source.Created
		,source.Updated
		,source.ByWhom
		)

when matched and not 
	(
	target.Updated = source.Updated
	)
then
	update
	set
		target.MergeCancelledOperationRecno = source.MergeCancelledOperationRecno
		,target.ContextID = source.ContextID
		,target.WardID  = source.WardID
		,target.CancelledOperationDateID = source.CancelledOperationDateID 
		,target.ConsultantID = source.ConsultantID
		,target.SpecialtyID = source.SpecialtyID
		,target.Created = source.Created
		,target.Updated = getdate()
		,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






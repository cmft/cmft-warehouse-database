﻿

create procedure [ETL].[BuildAnticoagulationBaseEncounterReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Anticoagulation.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Anticoagulation.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		Anticoagulation.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	Anticoagulation.BaseEncounterReference target
using
	(
	select
		BaseEncounter.MergeEncounterRecno
		,ContextID = Context.ContextID

		,TreatmentStartDateID =
			coalesce(
				 TreatmentStartDate.DateID

				,case
				when BaseEncounter.TreatmentStartDate is null
				then NullDate.DateID

				when BaseEncounter.TreatmentStartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,TestDateID =
			coalesce(
				 TestDate.DateID

				,case
				when BaseEncounter.TestDate is null
				then NullDate.DateID

				when BaseEncounter.TestDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,DiagnosisDateID =
			coalesce(
				 DiagnosisDate.DateID

				,case
				when BaseEncounter.DiagnosisDate is null
				then NullDate.DateID

				when BaseEncounter.DiagnosisDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		
		,ClinicID = Clinic.SourceClinicID
		,DiagnosisID = Diagnosis.SourceValueID
		,CommissionerID	= Commissioner.SourceValueID
		,BaseEncounter.Created
		,BaseEncounter.Updated
		,BaseEncounter.ByWhom
	from
		Anticoagulation.BaseEncounter

	inner join WH.Context
	on	Context.ContextCode = BaseEncounter.ContextCode

	inner join OP.Clinic
	on	Clinic.SourceClinicCode = coalesce(BaseEncounter.ClinicID, '-1')
	and	Clinic.SourceContextCode = BaseEncounter.ContextCode

	inner join Anticoagulation.Diagnosis
	on	Diagnosis.DiagnosisCode = coalesce(BaseEncounter.DiagnosisID, '-1')
	and	Diagnosis.ContextCode = BaseEncounter.ContextCode

	inner join Anticoagulation.Commissioner
	on	Commissioner.CommissionerCode = coalesce(BaseEncounter.CommissionerID, '-1')
	and	Commissioner.ContextCode = BaseEncounter.ContextCode

	left join WH.Calendar TreatmentStartDate
	on	TreatmentStartDate.TheDate = BaseEncounter.TreatmentStartDate

	left join WH.Calendar TestDate
	on	TestDate.TheDate = BaseEncounter.TestDate

	left join WH.Calendar DiagnosisDate
	on	DiagnosisDate.TheDate = BaseEncounter.DiagnosisDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseEncounter.Updated > @LastUpdated
	and	BaseEncounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			MergeEncounterRecno
			,ContextID
			,TreatmentStartDateID
			,TestDateID
			,DiagnosisDateID
			,ClinicID
			,DiagnosisID
			,CommissionerID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.TreatmentStartDateID
			,source.TestDateID
			,source.DiagnosisDateID
			,source.ClinicID
			,source.DiagnosisID
			,source.CommissionerID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.TreatmentStartDateID = source.TreatmentStartDateID
			,target.TestDateID = source.TestDateID
			,target.DiagnosisDateID = source.DiagnosisDateID
			,target.ClinicID = source.ClinicID
			,target.DiagnosisID = source.DiagnosisID
			,target.CommissionerID = source.CommissionerID
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






﻿CREATE procedure [ETL].[ImportCenGUMBaseEncounter] as

--import the data
exec ETL.BuildCenGUMBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[GUM].[BaseEncounter]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildGUMBaseEncounterReference 'CEN||MCSH'


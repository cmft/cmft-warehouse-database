﻿CREATE procedure [ETL].[ImportCenCOMBaseDiagnosisProcedureEvent] as


--import the data
exec ETL.BuildCenCOMBaseDiagnosisProcedureEvent


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[COM].[BaseDiagnosisProcedureEvent]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildCOMBaseDiagnosisProcedureEventReference 'CEN||IPM'

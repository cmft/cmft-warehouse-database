﻿

CREATE procedure [ETL].[BuildTraAnticoagulationBaseEncounter]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Anticoagulation.BaseEncounter target
using
	(
	select
		 EncounterRecno
		,SourceUniqueID = cast(SourceUniqueID as int)
		,NHSNumber = cast(NHSNumber as varchar(20))
		,PatientSurname = cast(PatientSurname as varchar(50))
		,PatientForename = cast(PatientForename as varchar(50))
		,DateOfBirth = cast(DateOfBirth as date)
		,SexCode = cast(SexCode as char(1))
		,Postcode = cast(Postcode as varchar(15))
		,ClinicID = cast(ClinicID as int)
		,TreatmentStartDate = cast(TreatmentStartDate as date)
		,TestDate = cast(TestDate as date)
		,Result = cast(Result as float)
		,Dose = cast(Dose as float)
		,DiagnosisID = cast(DiagnosisID as int)
		,DiagnosisDate = cast(DiagnosisDate as date)
		,CommissionerID = cast(CommissionerID as int)
		,GpPracticeCode = cast(GpPracticeCode as varchar(20))
		,InterfaceCode = cast(InterfaceCode as varchar(4))
		,ContextCode = cast(ContextCode as varchar(10))
	from
		ETL.TLoadTraAnticoagulationBaseEncounter
	) source
	on	source.EncounterRecno = target.EncounterRecno
	and	source.ContextCode = target.ContextCode
	
	when not matched by source
	and	target.ContextCode = 'TRA||DAWN'

	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,NHSNumber
			,PatientSurname
			,PatientForename
			,DateOfBirth
			,SexCode
			,Postcode
			,ClinicID
			,TreatmentStartDate
			,TestDate
			,Result
			,Dose
			,DiagnosisID
			,DiagnosisDate
			,CommissionerID
			,GpPracticeCode
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.EncounterRecno
			,source.SourceUniqueID
			,source.NHSNumber
			,source.PatientSurname
			,source.PatientForename
			,source.DateOfBirth
			,source.SexCode
			,source.Postcode
			,source.ClinicID
			,source.TreatmentStartDate
			,source.TestDate
			,source.Result
			,source.Dose
			,source.DiagnosisID
			,source.DiagnosisDate
			,source.CommissionerID
			,source.GpPracticeCode
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(	
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.ClinicID, 0) = isnull(source.ClinicID, 0)
		and isnull(target.TreatmentStartDate, getdate()) = isnull(source.TreatmentStartDate, getdate())
		and isnull(target.TestDate, getdate()) = isnull(source.TestDate, getdate())
		and isnull(target.Result, 0) = isnull(source.Result, 0)
		and isnull(target.Dose, 0) = isnull(source.Dose, 0)
		and isnull(target.DiagnosisID, 0) = isnull(source.DiagnosisID, 0)
		and isnull(target.DiagnosisDate, getdate()) = isnull(source.DiagnosisDate, getdate())
		and isnull(target.CommissionerID, 0) = isnull(source.CommissionerID, 0)
		and isnull(target.GpPracticeCode, '') = isnull(source.GpPracticeCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.NHSNumber = source.NHSNumber
			,target.PatientSurname = source.PatientSurname
			,target.PatientForename = source.PatientForename
			,target.DateOfBirth = source.DateOfBirth
			,target.SexCode = source.SexCode
			,target.Postcode = source.Postcode
			,target.ClinicID = source.ClinicID
			,target.TreatmentStartDate = source.TreatmentStartDate
			,target.TestDate = source.TestDate
			,target.Result = source.Result
			,target.Dose = source.Dose
			,target.DiagnosisID = source.DiagnosisID
			,target.DiagnosisDate = source.DiagnosisDate
			,target.CommissionerID = source.CommissionerID
			,target.GpPracticeCode = source.GpPracticeCode
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime




﻿CREATE procedure [ETL].[BuildCenSCRBaseReferralTreatmentPTL]

	@CensusDate date = null

as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


select
	@CensusDate =
		coalesce(@CensusDate, getdate())

delete from SCR.BaseReferralTreatmentPTL
where
	CensusDate = @CensusDate
and	ContextCode =  'CEN||SCR'


select
	@deleted = @@ROWCOUNT

--clear out old snapshots
delete
from
	SCR.BaseReferralTreatmentPTL
where 
	not exists
	(
	select
		1
	from
		(
	--Month end snapshots
		select
			Calendar.TheDate
		from
			WH.Calendar
		where
			Calendar.TheDate in
			(
			select
				max(TheDate)
			from
				WH.Calendar
			group by
				 year(TheDate)
				,month(TheDate)

			union
	--snapshots in the last fourteen days
			select
				TheDate
			from
				WH.Calendar
			where
				TheDate between dateadd(day , -14 , getdate()) and getdate()

			union
	--Sunday snapshots
			select
				TheDate
			from
				WH.Calendar
			where
				datename(dw, TheDate) = 'Sunday'
			)
		) Calendar
		where
			BaseReferralTreatmentPTL.CensusDate = TheDate
	)
and	SCR.BaseReferralTreatmentPTL.ContextCode =  'CEN||SCR'

select
	@deleted = @deleted + @@ROWCOUNT


insert into SCR.BaseReferralTreatmentPTL
(
	 UniqueRecordId
	,DefinitiveTreatmentUniqueRecordId
	,CensusDate
	,Surname
	,Forename
	,HospitalNumber
	,DateOfBirth
	,GenderCode
	,PracticeCode
	,PCTCode
	,AgeAtDiagnosis
	,DTTAdjustmentReasonCode
	,PriorityTypeCode
	,CancerTypeCode
	,CancerStatusCode
	,SourceForOutpatientCode
	,FirstAppointmentAdjustmentReasonCode
	,InitialTreatmentCode
	,TreatmentSettingCode
	,ConsultantCode
	,SpecialtyCode
	,PrimaryDiagnosisCode
	,IntendedProcedureCode
	,TumourStatusCode
	,ReceiptOfReferralDate
	,ConsultantUpgradeDate
	,FirstAppointmentConsultantCode
	,CancerSite
	,PathwayTypeCode
	,FirstAppointmentAttendDate
	,FirstAppointmentDate
	,DecisionToTreatDate
	,TCIDate
	,PreOpDate
	,MDTDate
	,DTTAdjustmentDays
	,TrackingNotes
	,SevenDayWait
	,BreachDate62Days
	,CurrentWait62Days
	,BreachDate31Days
	,CurrentWait31Days
	,RTT
	,Screening
	,Upgrade
	,DTT
	,Subsequent
	,NHSNumber
	,SourceOfReferralCode
	,ReferringOrganisationCode
	,ReferrerCode
	,TreatmentOrganisationCode
	,DiagnosisOrganisationCode
	,FirstAppointmentOrganisationCode
	,FirstAppointmentSpecialistOrganisationCode
	,OrganisationUpgradeCode
	,ReferredFromOrganisationCode
	,TertiaryCentreOrganisationCode
	,SubTumourSiteID
	,FirstDefinitiveTreatmentDate
	,ContextCode
	,Created
	,ByWhom
)
select
	 Referral.UniqueRecordId

	,DefinitiveTreatmentUniqueRecordId = DefinitiveTreatment.UniqueRecordId

	,CensusDate = @CensusDate

	,Demographic.Surname
	,Demographic.Forename
	,Demographic.HospitalNumber
	,Demographic.DateOfBirth
	,Demographic.GenderCode
	,Demographic.PracticeCode
	,Demographic.PCTCode

	,AgeAtDiagnosis =
		DATEDIFF(
			year
			,Demographic.DateOfBirth
			,coalesce(Referral.DiagnosisDate, Referral.FirstAppointmentDate, @CensusDate)
		) -
		case
		when DATEPART(month, Demographic.DateOfBirth) < DATEPART(month, coalesce(Referral.DiagnosisDate, Referral.FirstAppointmentDate, @CensusDate))
		then 0
		else 1
		end

	,DTTAdjustmentReasonCode =
		DefinitiveTreatment.AdjustmentReasonCode

	,Referral.PriorityTypeCode
	,Referral.CancerTypeCode
	,Referral.CancerStatusCode
	,Referral.SourceForOutpatientCode
	,Referral.FirstAppointmentAdjustmentReasonCode
	,DefinitiveTreatment.InitialTreatmentCode
	,DefinitiveTreatment.TreatmentSettingCode
	,Referral.ConsultantCode
	,Referral.SpecialtyCode
	
	--KO changed 09/08/2012 because ICD codes not existing in reference table caused cube load to fail
	--,Referral.PrimaryDiagnosisCode
	,PrimaryDiagnosisCode =
		case Referral.PrimaryDiagnosisCode
		when  'C80' then 'C80X'
		else Referral.PrimaryDiagnosisCode
		end

	,IntendedProcedureCode =
		coalesce(
			 Surgery.MainProcedureCode1
			,Chemotherapy.DrugTherapyTypeCode
		)

	,Referral.TumourStatusCode

	,Referral.ReceiptOfReferralDate
	,Referral.ConsultantUpgradeDate
	,Referral.FirstAppointmentConsultantCode
	,Referral.CancerSite

--if there is no DefinitiveTreatment.DecisionToTreatDate then non-admitted else admitted
	,PathwayTypeCode =
		case
		when DefinitiveTreatment.DecisionToTreatDate is null
		then 'N' --non-admitted
		else 'A' --admitted
		end

	,FirstAppointmentAttendDate =
		case
		when
			Referral.CancelledAppointmentDate is null
		and	Referral.FirstAppointmentDate <= @CensusDate
		then Referral.FirstAppointmentDate
		else null
		end

	,Referral.FirstAppointmentDate

	,DecisionToTreatDate =
		coalesce(
			 Surgery.DecisionToOperateDate
			,Chemotherapy.DecisionToTreatDate
			,DefinitiveTreatment.DecisionToTreatDate
		)

	,TCIDate =
		coalesce(
			 Surgery.AdmissionDate
			,Chemotherapy.InitialStartDate
		)

	,PreOpDate =
		InitialAssessment.AssessmentDate

	,MDTDate =
		(
		select
			MAX(CarePlan.MDTDate)
		from
			[$(Warehouse)].SCR.CarePlan
		where
			CarePlan.ReferralUniqueRecordId = Referral.UniqueRecordId
		and	CarePlan.MDTDate <= @CensusDate
		)

	,DTTAdjustmentDays =
		DefinitiveTreatment.WaitingTimeAdjustment

	,TrackingNotes =
		cast(TrackingComment.TrackingComment as varchar(max))
		--case
		--when DefinitiveTreatment.TreatmentNo = 1
		--then DefinitiveTreatment.TrackingNotes
		--end

	,SevenDayWait =
		case
		when
			Referral.PriorityTypeCode = '03' --2 Week Wait
		then
--have they been seen?
			case
			when
				Referral.CancelledAppointmentDate is null
			and	Referral.FirstAppointmentDate <= @CensusDate
--yes
			then null
--if they have not been seen then how many days have they waited
			else DATEDIFF(day, Referral.ReceiptOfReferralDate, @CensusDate)
			end
		end

	,BreachDate62Days =

--add the adjustment days to the calculated breach date defined below
		DATEADD(
			day
			,coalesce(
				 DefinitiveTreatment.WaitingTimeAdjustment
				,0
			)

			,case
			when
--not subsequent treatment
				coalesce(DefinitiveTreatment.TreatmentNo, 0) <> 2
			and	Referral.TumourStatusCode not in
				(
				 '4' --Recurrence
				,'5' --Metastasis
				)
			and	Referral.CancerStatusCode <> '15' --Suspected recurrent cancer
			and	(
					Referral.PriorityTypeCode = '03' --Two Week Wait
				or	(
						Referral.PriorityTypeCode = '02' --Urgent
					and	Referral.SourceForOutpatientCode = '17' --National Screening Programme
					)
				or	Referral.ConsultantUpgradeDate is not null
				)
			then
				case
				when Referral.ConsultantUpgradeDate is not null
				then Referral.ConsultantUpgradeDate + 62

				when
					Referral.CancelledAppointmentDate is not null
				and	Referral.FirstAppointmentAdjustmentReasonCode = 3 --DNA - a patient Does Not Attend an out-patient appointment
				and	Referral.PrimaryDiagnosisCode in
					(
					 'C62'
					,'C620'
					,'C621'
					,'C629'
					,'C910'
					,'C920'
					,'C924'
					,'C925'
					,'C930'
					,'C942'
					,'C950'
					)
				then Referral.CancelledAppointmentDate + 31

				when
					Referral.CancelledAppointmentDate is not null
				and	Referral.FirstAppointmentAdjustmentReasonCode = 3 --DNA - a patient Does Not Attend an out-patient appointment
				and	Referral.CancerTypeCode in
					(
					 '02' --Suspected childrens cancer
					,'05' --Suspected acute leukaemia
					,'12' --Suspected testicular cancers
					)
				then Referral.CancelledAppointmentDate + 31

				when
					Referral.ReceiptOfReferralDate is not null
				and	Referral.PrimaryDiagnosisCode in
					(
					 'C62'
					,'C620'
					,'C621'
					,'C629'
					,'C910'
					,'C920'
					,'C924'
					,'C925'
					,'C930'
					,'C942'
					,'C950'
					)
				then Referral.ReceiptOfReferralDate + 31

				when
					Referral.ReceiptOfReferralDate is not null
				and	Referral.CancerTypeCode in
					(
					 '02' --Suspected childrens cancer
					,'05' --Suspected acute leukaemia
					,'12' --Suspected testicular cancers
					)
				then Referral.ReceiptOfReferralDate + 31

--children do not have clock reset after a DNA
				when
					Referral.CancelledAppointmentDate is not null
				and	Referral.FirstAppointmentAdjustmentReasonCode = 3 --DNA - a patient Does Not Attend an out-patient appointment
				and
					DATEDIFF(
						year
						,Demographic.DateOfBirth
						,coalesce(Referral.DiagnosisDate, Referral.FirstAppointmentDate)
					) -
					case
					when DATEPART(month, Demographic.DateOfBirth) < DATEPART(month, coalesce(Referral.DiagnosisDate, Referral.FirstAppointmentDate))
					then 0
					else 1
					end between 0 and 15
				then Referral.CancelledAppointmentDate + 31

--children do not have clock reset after a DNA
				when
					Referral.ReceiptOfReferralDate is not null
				and
					DATEDIFF(
						year
						,Demographic.DateOfBirth
						,coalesce(Referral.DiagnosisDate, Referral.FirstAppointmentDate)
					) -
					case
					when DATEPART(month, Demographic.DateOfBirth) < DATEPART(month, coalesce(Referral.DiagnosisDate, Referral.FirstAppointmentDate))
					then 0
					else 1
					end between 0 and 15
				then Referral.ReceiptOfReferralDate + 31

				when
					Referral.CancelledAppointmentDate is not null
				and	Referral.FirstAppointmentAdjustmentReasonCode = 3 --DNA - a patient Does Not Attend an out-patient appointment
				then Referral.CancelledAppointmentDate + 62

				else Referral.ReceiptOfReferralDate + 62

				end

			end
		)


--calculate number of days waiting on 62 day PTL
	,CurrentWait62Days =

--2011-11-16 PDO - If no FirstDefinitiveTreatmentDate then use the CensusDate

		case
		when
			coalesce(DefinitiveTreatment.TreatmentNo, 0) <> 2
		and	Referral.TumourStatusCode not in
			(
			 '4' --Recurrence
			,'5' --Metastasis
			)
		and	Referral.CancerStatusCode <> '15' --Suspected recurrent cancer
		and	(
				Referral.PriorityTypeCode = '03' --Two Week Wait
			or	(
					Referral.PriorityTypeCode = '02' --Urgent
				and	Referral.SourceForOutpatientCode = '17' --National Screening Programme
				)
			or	Referral.ConsultantUpgradeDate is not null
			)
		--and	DefinitiveTreatment.FirstDefinitiveTreatmentDate is not null
		then
			datediff(
				day
				,case
				when Referral.ConsultantUpgradeDate is not null
				then Referral.ConsultantUpgradeDate

				when
					Referral.CancelledAppointmentDate is not null
				and	Referral.FirstAppointmentAdjustmentReasonCode = 3 --DNA - a patient Does Not Attend an out-patient appointment
				then Referral.CancelledAppointmentDate

				else Referral.ReceiptOfReferralDate
				end

				,coalesce(
					 DefinitiveTreatment.FirstDefinitiveTreatmentDate
					,@CensusDate
				)
			)
		end

		-

		coalesce(
			 DefinitiveTreatment.WaitingTimeAdjustment
			,0
		)

	,BreachDate31Days =
		DATEADD(
			day
			,31 +
			coalesce(
				 DefinitiveTreatment.WaitingTimeAdjustment
				,0
			)

			,DefinitiveTreatment.DecisionToTreatDate
		)


	,CurrentWait31Days =
		datediff(
			day
			,DefinitiveTreatment.DecisionToTreatDate
			,coalesce(
				 DefinitiveTreatment.FirstDefinitiveTreatmentDate
				,@CensusDate
			)
		)

		-

		coalesce(
			 DefinitiveTreatment.WaitingTimeAdjustment
			,0
		)


	,RTT = --62 day
		case
		when
			Referral.CancerStatusCode = '08' --First treatment commenced
		and	DefinitiveTreatment.FirstDefinitiveTreatmentDate <= @CensusDate
		then 0

		when
			Referral.ReceiptOfReferralDate <= @CensusDate
		and	Referral.PriorityTypeCode = '03' --Two Week Wait
		and	coalesce(Referral.InappropriateReferralCode, '0') <> '1'
		and	Referral.CancerStatusCode not in
			(
			 '03' --No new cancer diagnosis identified
			,'07' --Diagnosis of new cancer confirmed - no English NHS treatment planned
			,'08' --First treatment commenced
			,'15' --Suspected recurrent cancer
			,'18' --Diagnosis of recurrent cancer confirmed - no English NHS treatment planned
			,'21' --Subsequent treatment commenced
			)
		and	Referral.TumourStatusCode not in
			(
			 '4' --Recurrence
			,'5' --Metastasis
			)
		and	coalesce(DefinitiveTreatment.TreatmentNo, 0) <> 2
		then 1
		else 0
		end

	,Screening =
		case
		when
			Referral.ReceiptOfReferralDate <= @CensusDate
		and	Referral.PriorityTypeCode = '02' --Urgent
		and	coalesce(Referral.InappropriateReferralCode, '0') <> '1'
		and	Referral.CancerStatusCode not in
			(
			 '03' --No new cancer diagnosis identified
			,'07' --Diagnosis of new cancer confirmed - no English NHS treatment planned
			,'08' --First treatment commenced
			,'15' --Suspected recurrent cancer
			,'18' --Diagnosis of recurrent cancer confirmed - no English NHS treatment planned
			,'21' --Subsequent treatment commenced
			)
		and	Referral.TumourStatusCode not in
			(
			 '4' --Recurrence
			,'5' --Metastasis
			)
		and	coalesce(DefinitiveTreatment.TreatmentNo, 0) <> 2
		and	Referral.SourceForOutpatientCode = '17' --National Screening Programme
		then 1
		else 0
		end

	,Upgrade =
		case
		when
			Referral.ConsultantUpgradeDate <= @CensusDate
		and	isdate(Referral.ConsultantUpgradeDate) = 1
		and	coalesce(Referral.InappropriateReferralCode, '0') <> '1'
		and	Referral.CancerStatusCode not in
			(
			 '03' --No new cancer diagnosis identified
			,'07' --Diagnosis of new cancer confirmed - no English NHS treatment planned
			,'08' --First treatment commenced
			,'15' --Suspected recurrent cancer
			,'18' --Diagnosis of recurrent cancer confirmed - no English NHS treatment planned
			,'21' --Subsequent treatment commenced
			)
		and	Referral.TumourStatusCode not in
			(
			 '4' --Recurrence
			,'5' --Metastasis
			)
		and	coalesce(DefinitiveTreatment.TreatmentNo, 0) <> 2
		then 1
		else 0
		end

	,DTT = --31 day
		case
		--when
		--	Referral.CancerStatusCode = '08' --First treatment commenced
		--and	DefinitiveTreatment.FirstDefinitiveTreatmentDate <= @CensusDate
		--then 0

		when
			coalesce(
				 Referral.DiagnosisDate
				,DefinitiveTreatment.DecisionToTreatDate
				--,Referral.DecisionToReferDate

----use the DefinitiveTreatment.DecisionToTreatDate only when they have not yet been admitted for surgery
--				,case
--				when DefinitiveTreatment.DecisionToTreatDate is not null
--				and	(
--						Surgery.AdmissionDate is null
--					or	Surgery.AdmissionDate > @CensusDate
--					)
--				then DefinitiveTreatment.DecisionToTreatDate
--				else null
--				end
			)
			 <= @CensusDate
		and	coalesce(Referral.InappropriateReferralCode, '0') <> '1'
		and	Referral.CancerStatusCode not in
			(
			 '03' --No new cancer diagnosis identified
			,'07' --Diagnosis of new cancer confirmed - no English NHS treatment planned
			,'18' --Diagnosis of recurrent cancer confirmed - no English NHS treatment planned
			)
		and	coalesce(DefinitiveTreatment.TreatmentNo, 0) <> 2
		then 1
		else 0
		end

	,Subsequent =
		case
		when
			DefinitiveTreatment.DecisionToTreatDate is null
		and	DefinitiveTreatment.FirstDefinitiveTreatmentDate < @CensusDate
		and	Referral.CancerStatusCode in
			(
			 '21' --Subsequent treatment commenced
			)
		then 0

		when
			Referral.DecisionToReferDate is not null
		and	coalesce(Referral.InappropriateReferralCode, '0') <> '1'
		and	Referral.CancerStatusCode not in
			(
			 '03' --No new cancer diagnosis identified
			,'07' --Diagnosis of new cancer confirmed - no English NHS treatment planned
			,'18' --Diagnosis of recurrent cancer confirmed - no English NHS treatment planned
			,'21' --Subsequent treatment commenced
			)
		and	DefinitiveTreatment.TreatmentNo = 2
		then 1

		--include patients without a DTT/ECAD recorded (subsequent treatment not yet planned)
		when
			Referral.CancerStatusCode in
			(
			 '12' --Diagnosis of new cancer confirmed - subsequent treatment not yet planned
			--,'13' --Diagnosis of new cancer confirmed - subsequent English NHS treatment planned
			,'19' --Diagnosis of recurrent cancer confirmed - subsequent treatment not yet planned
			)
		then 1
		else 0
		end

	,Demographic.NHSNumber
	,Referral.SourceOfReferralCode
	,Referral.ReferringOrganisationCode
	,Referral.ReferrerCode

	,TreatmentOrganisationCode =
		coalesce(
			 Referral.TertiaryCentreOrganisationCode
			,DefinitiveTreatment.TreatmentOrganisationCode
		)

	,Referral.DiagnosisOrganisationCode
	,Referral.FirstAppointmentOrganisationCode
	,Referral.FirstAppointmentSpecialistOrganisationCode
	,Referral.OrganisationUpgradeCode
	,Referral.ReferredFromOrganisationCode
	,Referral.TertiaryCentreOrganisationCode

	,SubTumourSiteID =
		case
		when Referral.AdditionalCommentsReferral like 'UGI%'
		then 1

		when Referral.AdditionalCommentsReferral like 'HPB%'
		then 2

		end

	,DefinitiveTreatment.FirstDefinitiveTreatmentDate

	,ContextCode = 'CEN||SCR'
	,Created = GETDATE()
	,ByWhom = SUSER_NAME()
from
	[$(Warehouse)].SCR.Referral

left join [$(Warehouse)].SCR.DefinitiveTreatment
on	DefinitiveTreatment.ReferralUniqueRecordId = Referral.UniqueRecordId
--and	DefinitiveTreatment.FirstDefinitiveTreatmentDate <= @CensusDate

--pull LATEST definitive treatment based on the FirstDefinitiveTreatmentDate
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.DefinitiveTreatment Previous
	where
		Previous.ReferralUniqueRecordId = Referral.UniqueRecordId
	--and	Previous.FirstDefinitiveTreatmentDate <= @CensusDate
	and	(
			Previous.FirstDefinitiveTreatmentDate > DefinitiveTreatment.FirstDefinitiveTreatmentDate
		or	(
				Previous.FirstDefinitiveTreatmentDate = DefinitiveTreatment.FirstDefinitiveTreatmentDate
			and	Previous.UniqueRecordId > DefinitiveTreatment.UniqueRecordId
			)
		)
	)

inner join [$(Warehouse)].SCR.Demographic
on	Demographic.UniqueRecordId = Referral.DemographicUniqueRecordId

left join [$(Warehouse)].SCR.Surgery
on	Surgery.ReferralUniqueRecordId = Referral.UniqueRecordId

--pull LATEST surgery based on the AdmissionDate
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.Surgery Previous
	where
		Previous.ReferralUniqueRecordId = Referral.UniqueRecordId
	and	(
			coalesce(Previous.AdmissionDate, getdate()) > coalesce(Surgery.AdmissionDate, getdate())
		or	(
				coalesce(Previous.AdmissionDate, getdate()) = coalesce(Surgery.AdmissionDate, getdate())
			and	Previous.UniqueRecordId > Surgery.UniqueRecordId
			)
		)
	)

--PDO 16 Feb 2012 - replaced with above

--left join [$(Warehouse)].SCR.Surgery
--on	Surgery.ReferralUniqueRecordId = Referral.UniqueRecordId
--and	Surgery.SurgeryDate <= @CensusDate
--and	not exists
--	(
--	select
--		1
--	from
--		[$(Warehouse)].SCR.Surgery Previous
--	where
--		Previous.ReferralUniqueRecordId = Referral.UniqueRecordId
--	and	Previous.SurgeryDate <= @CensusDate
--	and	(
--			Previous.SurgeryDate > Surgery.SurgeryDate
--		or	(
--				Previous.SurgeryDate = Surgery.SurgeryDate
--			and	Previous.UniqueRecordId > Surgery.UniqueRecordId
--			)
--		)
--	)

left join [$(Warehouse)].SCR.InitialAssessment
on	InitialAssessment.ReferralUniqueRecordId = Referral.UniqueRecordId

--pull LATEST InitialAssessment based on the AssessmentDate
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.InitialAssessment Previous
	where
		Previous.ReferralUniqueRecordId = Referral.UniqueRecordId
	--and	Previous.InitialStartDate <= @CensusDate
	and	(
			coalesce(Previous.AssessmentDate, getdate()) > coalesce(InitialAssessment.AssessmentDate, getdate())
		or	(
				coalesce(Previous.AssessmentDate, getdate()) = coalesce(InitialAssessment.AssessmentDate, getdate())
			and	Previous.UniqueRecordId > InitialAssessment.UniqueRecordId
			)
		)
	)

left join [$(Warehouse)].SCR.Chemotherapy
on	Chemotherapy.ReferralUniqueRecordId = Referral.UniqueRecordId
--and	Chemotherapy.InitialStartDate <= @CensusDate

--pull LATEST Chemotherapy based on the InitialStartDate
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.Chemotherapy Previous
	where
		Previous.ReferralUniqueRecordId = Referral.UniqueRecordId
	--and	Previous.InitialStartDate <= @CensusDate
	and	(
			coalesce(Previous.InitialStartDate, getdate()) > coalesce(Chemotherapy.InitialStartDate, getdate())
		or	(
				coalesce(Previous.InitialStartDate, getdate()) = coalesce(Chemotherapy.InitialStartDate, getdate())
			and	Previous.UniqueRecordId > Chemotherapy.UniqueRecordId
			)
		)
	)

left join [$(Warehouse)].SCR.TrackingComment TrackingComment
on	TrackingComment.ReferralUniqueRecordId = Referral.UniqueRecordId

--pull LATEST TrackingComment based on the TrackingCommentTime
and	not exists
	(
	select
		1
	from
		[$(Warehouse)].SCR.TrackingComment Previous
	where
		Previous.ReferralUniqueRecordId = TrackingComment.ReferralUniqueRecordId
	and	(
			coalesce(Previous.TrackingCommentTime, getdate()) > coalesce(TrackingComment.TrackingCommentTime, getdate())
		or	(
				coalesce(Previous.TrackingCommentTime, getdate()) = coalesce(TrackingComment.TrackingCommentTime, getdate())
			and	Previous.UniqueRecordId > TrackingComment.UniqueRecordId
			)
		)
	)

where

--exclude patients treated more than 7 days from report date
	(
		DefinitiveTreatment.FirstDefinitiveTreatmentDate is null
	or	datediff(day, DefinitiveTreatment.FirstDefinitiveTreatmentDate, @CensusDate) < 15
	or	Referral.CancerStatusCode in
		(
		 '12' --Diagnosis Of New Cancer Confirmed - Subsequent Treatment Not Yet Planned
		,'19' --Diagnosis Of Recurrent Cancer Confirmed - Subsequent Treatment Not Yet Planned
		)

--PDO 16 Feb 2012 - replaced with above
	--or	datediff(day, DefinitiveTreatment.FirstDefinitiveTreatmentDate, @CensusDate) > 6
	)

--exclude BCCs
and	not
	(
		left(coalesce(Referral.PrimaryDiagnosisCode, 'xxx'), 3) = 'C44'
	and	coalesce(Referral.HistologyCode, 'x') in
			(
			 'M80903'
			,'M80913'
			,'M80923'
			,'M80933'
			,'M80943'
			,'M80953'
			,'M81103'
			)
	)

--exclude D codes
and	(
		left(coalesce(Referral.PrimaryDiagnosisCode, 'xxx'), 1) <> 'D'
	or	left(coalesce(Referral.PrimaryDiagnosisCode, 'xxx'), 3) = 'D05'
	)

----remove urgent/routine with no activity other than referral
--and	(

----not urgent/routine
--		Referral.PriorityTypeCode = '03' --2 Week Wait
--	or	Referral.SourceForOutpatientCode = 17 --National Screening Programme

----has there been any activity for this referral
--	or	(
--			(
--				Surgery.UniqueRecordId is not null
--			or	Chemotherapy.UniqueRecordId is not null
--			--or	InitialAssessment.UniqueRecordId is not null --2011-11-03 PDO removed condition
--			or	DefinitiveTreatment.UniqueRecordId is not null
--			or	Referral.PrimaryDiagnosisCode is not null
--			or	Referral.ConsultantUpgradeDate is not null
--			)

----and there is a future surgery admission date or a DecisionToOperateDate
--		and (
--				Surgery.UniqueRecordId is null
--			or	(
--					Surgery.AdmissionDate > @CensusDate
--				or	(
--						Surgery.AdmissionDate is null
--					and	Surgery.DecisionToOperateDate is not null
--					)
--				)
--			)
--		)
--	)

----patient has not died
--and	Demographic.DateOfDeath is null

--has not been validated for upload or as been validated for upload but still has planned treatment
and	(
		coalesce(DefinitiveTreatment.ValidatedForUpload, 0) <> 1
	or	(
			DefinitiveTreatment.TreatmentNo = 2 --subsequent treatment
		and	DefinitiveTreatment.DecisionToTreatDate is not null
		and	(
				Surgery.AdmissionDate > @CensusDate
			or	Surgery.AdmissionDate is null
			)
		)
	)



select @inserted = @@rowcount


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


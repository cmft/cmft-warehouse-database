﻿CREATE procedure [ETL].[BuildReferenceDataMap]
	 @Table varchar(255)
	,@NewRows int output
	,@Debug bit = 0
	,@ActivityTableOverride varchar(max) = null
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int


declare @TableID smallint =
	(
	select
		TableBase.TableID
	from
		ReferenceMapV2.Map.TableBase
	where
		TableBase.[Table] = @Table
	)

if @Debug = 1
	print @TableID

exec ReferenceMapV2.Map.BuildMissingValues @TableID, @NewRows output, @Debug, @ActivityTableOverride

--build location Values, due to be replaced with generic approach CH 16092015
exec ReferenceMapV2.Map.BuildLocationValues

--missing cross-references
exec ReferenceMapV2.Map.BuildMissingXrefs

--default values form source reference data
exec ReferenceMapV2.Map.BuildDefaultXrefs

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())

select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes' +
		', Table: ' + @Table


exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

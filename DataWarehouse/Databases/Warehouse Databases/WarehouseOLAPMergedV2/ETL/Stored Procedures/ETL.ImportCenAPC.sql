﻿


CREATE procedure [ETL].[ImportCenAPC] as

exec ETL.ImportCenAPCBaseEncounter
exec ETL.ImportCenAPCBaseCriticalCarePeriod
exec ETL.ImportCenAPCBaseWardStay;
--exec ETL.ImportCenBaseConsultant;
exec ETL.ImportCenAPCBaseDiagnosis;
exec ETL.ImportCenAPCBaseOperation;
exec ETL.ImportCenAPCBaseAugmentedCarePeriod

--exec ETL.BuildAPCBaseEncounterProcedureDetail 'CEN||PAS', 'CEN||ORMIS'

exec ETL.BuildWHConsultantBase
exec ETL.BuildAPCWardBase


exec Allocation.AllocateAPCExclusion -- 20141007 Needed to put this before BuildSpell, so it doesn't include Non Reportable as part of updating the Global Provider Spell
exec APC.BuildSpell

--bedman sources

exec ETL.ImportCenAPCBaseFall
exec APC.BuildBaseEncounterBaseFall 

exec ETL.ImportCenAPCBasePressureUlcer
exec APC.BuildBaseEncounterBasePressureUlcer 

exec ETL.ImportCenAPCBaseUrinaryTractInfection
exec APC.BuildBaseEncounterBaseUrinaryTractInfection

exec ETL.ImportCenAPCBaseVTEAssessment
exec APC.BuildBaseEncounterBaseVTEAssessment

exec ETL.ImportCenAPCBaseVTECondition
exec APC.BuildBaseEncounterBaseVTECondition

exec ETL.ImportCenAPCBaseLearningDisability
exec APC.BuildBaseEncounterBaseLearningDisability

Exec APC.BuildBaseEncounterBaseSpecialRegister -- SpecialRegister is updated early hours of the morning before APC starts as part of Warehouse - Load PAS job.

exec APC.BuildBaseEncounterAEBaseEncounter



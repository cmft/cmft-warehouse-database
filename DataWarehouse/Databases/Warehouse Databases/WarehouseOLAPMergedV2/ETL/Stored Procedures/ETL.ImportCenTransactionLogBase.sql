﻿CREATE procedure [ETL].[ImportCenTransactionLogBase] as

--import the data
exec ETL.BuildCenTransactionLogBase


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[TransactionLog].[Base]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


----Build the BaseReference table
exec ETL.BuildTransactionLogBaseReference 'CEN||PAS'

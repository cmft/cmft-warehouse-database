﻿
create PROCEDURE [ETL].[BuildCenAPCBaseVTECondition]

as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)



declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.BaseVTECondition target
using
	(
	select
		VTEConditionRecno
		,SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,DiagnosisDate
		,DiagnosisTime
		,VTETypeID
		,OccuredOnAnotherWard
		,MedicationRequired
		,MedicationStartDate
		,MedicationStartTime
		,WardCode
		,InterfaceCode
		,ContextCode
	from
		ETL.TLoadCenAPCBaseVTECondition
	) source
	on	source.VTEConditionRecno = target.VTEConditionRecno
	and	source.ContextCode = target.ContextCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			VTEConditionRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,DiagnosisDate
			,DiagnosisTime
			,VTETypeID
			,OccuredOnAnotherWard
			,MedicationRequired
			,MedicationStartDate
			,MedicationStartTime
			,WardCode
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.VTEConditionRecno
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.DiagnosisDate
			,source.DiagnosisTime
			,source.VTETypeID
			,source.OccuredOnAnotherWard
			,source.MedicationRequired
			,source.MedicationStartDate
			,source.MedicationStartTime
			,source.WardCode
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.VTEConditionRecno, 0) = isnull(source.VTEConditionRecno, 0)
		and isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and	isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.DiagnosisDate, getdate()) = isnull(source.DiagnosisDate, getdate())
		and isnull(target.DiagnosisTime, getdate()) = isnull(source.DiagnosisTime, getdate())
		and isnull(target.VTETypeID, 0) = isnull(source.VTETypeID, 0)
		and isnull(target.OccuredOnAnotherWard, 0) = isnull(source.OccuredOnAnotherWard, 0)
		and isnull(target.MedicationRequired, 0) = isnull(source.MedicationRequired, 0)
		and isnull(target.MedicationStartDate, getdate()) = isnull(source.MedicationStartDate, getdate())
		and isnull(target.MedicationStartTime, getdate()) = isnull(source.MedicationStartTime, getdate())
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')

		)
	then
		update
		set
			target.VTEConditionRecno = source.VTEConditionRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.DiagnosisDate = source.DiagnosisDate
			,target.DiagnosisTime = source.DiagnosisTime
			,target.VTETypeID = source.VTETypeID
			,target.OccuredOnAnotherWard = source.OccuredOnAnotherWard
			,target.MedicationRequired = source.MedicationRequired
			,target.MedicationStartDate = source.MedicationStartDate
			,target.MedicationStartTime = source.MedicationStartTime
			,target.WardCode = source.WardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
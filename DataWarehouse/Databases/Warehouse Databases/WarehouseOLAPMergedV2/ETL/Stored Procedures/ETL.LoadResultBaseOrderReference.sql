﻿CREATE procedure [ETL].[LoadResultBaseOrderReference] 
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				Result.BaseOrderReference
			where
				BaseOrderReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	Result.BaseOrderReference
where
	not exists
	(
	select
		1
	from
		Result.BaseOrder
	where
		BaseOrder.MergeOrderRecno = BaseOrderReference.MergeOrderRecno
	)
and	BaseOrderReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT

merge
	Result.BaseOrderReference target
using
	(
	select
		BaseOrder.MergeOrderRecno
		,Context.ContextID
		,ConsultantID = Consultant.SourceConsultantID
		,MainSpecialtyID = MainSpecialty.SourceSpecialtyID
		,SpecialtyID = Specialty.SourceSpecialtyID
		,ProviderID = ServiceProvider.SourceServiceProviderID
		,LocationID = Location.SourceLocationID
		,OrderPriorityID = OrderPriority.SourceOrderPriorityID
		,OrderStatusID = OrderStatus.SourceOrderStatusID
  		,OrderRequestDateID =
				coalesce(
					 OrderRequestDate.DateID

					,case
					when BaseOrder.OrderRequestTime is null
					then NullDate.DateID

					when cast(BaseOrder.OrderRequestTime  as date) < CalendarStartDate.DateValue
					then PreviousDate.DateID

					else FutureDate.DateID
					end
				)
  		,OrderReceivedDateID =
				coalesce(
					 OrderReceivedDate.DateID

					,case
					when BaseOrder.OrderReceivedTime is null
					then NullDate.DateID

					when cast(BaseOrder.OrderReceivedTime  as date) < CalendarStartDate.DateValue
					then PreviousDate.DateID

					else FutureDate.DateID
					end
				)
  		,OrderCollectionDateID =
				coalesce(
					 OrderCollectionDate.DateID

					,case
					when BaseOrder.OrderCollectionTime is null
					then NullDate.DateID

					when cast(BaseOrder.OrderCollectionTime  as date) < CalendarStartDate.DateValue
					then PreviousDate.DateID

					else FutureDate.DateID
					end
				)
		,BaseOrder.Created
		,BaseOrder.Updated
		,BaseOrder.ByWhom
	from
		Result.BaseOrder

	left join WH.Calendar OrderRequestDate
	on	OrderRequestDate.TheDate = cast(BaseOrder.OrderRequestTime as date)

	left join WH.Calendar OrderReceivedDate
	on	OrderReceivedDate.TheDate = cast(BaseOrder.OrderReceivedTime as date)
	
	left join WH.Calendar OrderCollectionDate
	on	OrderCollectionDate.TheDate = cast(BaseOrder.OrderCollectionTime as date)
	
	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	left join WH.Consultant Consultant
	on	Consultant.SourceConsultantCode = coalesce(cast(BaseOrder.ConsultantCode as varchar(100)), '-1')
	and	Consultant.SourceContextCode = BaseOrder.ContextCode

	left join WH.Specialty MainSpecialty
	on	MainSpecialty.SourceSpecialtyCode = coalesce(cast(BaseOrder.MainSpecialtyCode as varchar(100)), '-1')
	and	MainSpecialty.SourceContextCode = BaseOrder.ContextCode

	left join WH.Specialty Specialty
	on	Specialty.SourceSpecialtyCode = coalesce(cast(BaseOrder.SpecialtyCode as varchar(100)), '-1')
	and	Specialty.SourceContextCode = BaseOrder.ContextCode

	left join Result.ServiceProvider
	on	ServiceProvider.SourceServiceProviderCode = coalesce(cast(BaseOrder.ProviderCode as varchar(100)), '-1')
	and	ServiceProvider.SourceContextCode = BaseOrder.ContextCode

	left join WH.Location
	on	Location.SourceLocationCode = coalesce(cast(BaseOrder.LocationCode as varchar(100)), '-1')
	and	Location.SourceContextCode = BaseOrder.ContextCode

	left join Result.OrderPriority
	on	OrderPriority.SourceOrderPriorityCode = coalesce(cast(BaseOrder.OrderPriority as varchar(100)), '-1')
	and	OrderPriority.SourceContextCode = BaseOrder.ContextCode

	left join Result.OrderStatus
	on	OrderStatus.SourceOrderStatusCode = coalesce(cast(BaseOrder.OrderStatusCode as varchar(100)), '-1')
	and	OrderStatus.SourceContextCode = BaseOrder.ContextCode
	
	left join WH.Context
	on	Context.ContextCode = BaseOrder.ContextCode

	) source
	on	source.MergeOrderRecno = target.MergeOrderRecno

	when not matched
	then
		insert
			(
			MergeOrderRecno
			,ContextID
			,ConsultantID
			,MainSpecialtyID
			,SpecialtyID
			,ProviderID
			,LocationID
			,OrderPriorityID
			,OrderStatusID
			,OrderRequestDateID
			,OrderReceivedDateID
			,OrderCollectionDateID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeOrderRecno
			,source.ContextID
			,source.ConsultantID
			,source.MainSpecialtyID
			,source.SpecialtyID
			,source.ProviderID
			,source.LocationID
			,source.OrderPriorityID
			,source.OrderStatusID
			,source.OrderRequestDateID
			,source.OrderReceivedDateID
			,source.OrderCollectionDateID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeOrderRecno = source.MergeOrderRecno
			,target.ContextID = source.ContextID
			,target.ConsultantID = source.ConsultantID
			,target.MainSpecialtyID = source.MainSpecialtyID
			,target.SpecialtyID = source.SpecialtyID
			,target.ProviderID = source.ProviderID
			,target.LocationID = source.LocationID
			,target.OrderPriorityID = source.OrderPriorityID
			,target.OrderStatusID = source.OrderStatusID
			,target.OrderRequestDateID = source.OrderRequestDateID
			,target.OrderReceivedDateID = source.OrderReceivedDateID
			,target.OrderCollectionDateID = source.OrderCollectionDateID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats

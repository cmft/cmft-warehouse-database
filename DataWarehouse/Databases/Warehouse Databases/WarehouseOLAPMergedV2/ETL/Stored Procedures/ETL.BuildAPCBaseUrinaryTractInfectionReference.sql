﻿

create procedure [ETL].[BuildAPCBaseUrinaryTractInfectionReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				APC.BaseUrinaryTractInfectionReference
			where
				BaseUrinaryTractInfectionReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	APC.BaseUrinaryTractInfectionReference
where
	not exists
	(
	select
		1
	from
		APC.BaseUrinaryTractInfection
	where
		BaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno = BaseUrinaryTractInfectionReference.MergeUrinaryTractInfectionRecno
	)
and	BaseUrinaryTractInfectionReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	APC.BaseUrinaryTractInfectionReference target
using
	(
	select
		BaseUrinaryTractInfection.MergeUrinaryTractInfectionRecno
		,ContextID = Context.ContextID
		,SymptomStartDateID =
			coalesce(
				 SymptomStartDate.DateID

				,case
				when BaseUrinaryTractInfection.SymptomStartDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.SymptomStartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,SymptomEndDateID =
			coalesce(
				 SymptomEndDate.DateID

				,case
				when BaseUrinaryTractInfection.SymptomEndDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.SymptomEndDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,UrinaryTestRequestedDateID =
			coalesce(
				 UrinaryTestRequestedDate.DateID

				,case
				when BaseUrinaryTractInfection.UrinaryTestRequestedDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.UrinaryTestRequestedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		
		,UrinaryTestReceivedDateID =
			coalesce(
				 UrinaryTestReceivedDate.DateID

				,case
				when BaseUrinaryTractInfection.UrinaryTestReceivedDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.UrinaryTestReceivedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,TreatmentStartDateID =
			coalesce(
				 TreatmentStartDate.DateID

				,case
				when BaseUrinaryTractInfection.TreatmentStartDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.TreatmentStartDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,TreatmentEndDateID =
			coalesce(
				 TreatmentEndDate.DateID

				,case
				when BaseUrinaryTractInfection.TreatmentEndDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.TreatmentEndDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)

		,CatheterInsertedDateID =
			coalesce(
				 CatheterInsertedDate.DateID

				,case
				when BaseUrinaryTractInfection.CatheterInsertedDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.CatheterInsertedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		,CatheterRemovedDateID =
			coalesce(
				 CatheterRemovedDate.DateID

				,case
				when BaseUrinaryTractInfection.CatheterRemovedDate is null
				then NullDate.DateID

				when BaseUrinaryTractInfection.CatheterRemovedDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		
		,CatheterTypeID = CatheterType.SourceCatheterTypeID	
		,WardID = Ward.SourceWardID	
		,BaseUrinaryTractInfection.Created
		,BaseUrinaryTractInfection.Updated
		,BaseUrinaryTractInfection.ByWhom
	from
		APC.BaseUrinaryTractInfection

	inner join WH.Context
	on	Context.ContextCode = BaseUrinaryTractInfection.ContextCode

	inner join APC.CatheterType
	on	CatheterType.SourceCatheterTypeCode = coalesce(BaseUrinaryTractInfection.CatheterTypeID, '-1')
	and	CatheterType.SourceContextCode = BaseUrinaryTractInfection.ContextCode

	inner join WH.Ward
	on	Ward.SourceWardCode = coalesce(BaseUrinaryTractInfection.WardCode, '-1')
	and	Ward.SourceContextCode = BaseUrinaryTractInfection.ContextCode

	left join WH.Calendar SymptomStartDate
	on	SymptomStartDate.TheDate = BaseUrinaryTractInfection.SymptomStartDate

	left join WH.Calendar SymptomEndDate
	on	SymptomEndDate.TheDate = BaseUrinaryTractInfection.SymptomEndDate

	left join WH.Calendar UrinaryTestRequestedDate
	on	UrinaryTestRequestedDate.TheDate = BaseUrinaryTractInfection.UrinaryTestRequestedDate

	left join WH.Calendar UrinaryTestReceivedDate
	on	UrinaryTestReceivedDate.TheDate = BaseUrinaryTractInfection.UrinaryTestReceivedDate

	left join WH.Calendar TreatmentStartDate
	on	TreatmentStartDate.TheDate = BaseUrinaryTractInfection.TreatmentStartDate

	left join WH.Calendar TreatmentEndDate
	on	TreatmentEndDate.TheDate = BaseUrinaryTractInfection.TreatmentEndDate

	left join WH.Calendar CatheterInsertedDate
	on	CatheterInsertedDate.TheDate = BaseUrinaryTractInfection.CatheterInsertedDate

	left join WH.Calendar CatheterRemovedDate
	on	CatheterRemovedDate.TheDate = BaseUrinaryTractInfection.CatheterRemovedDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseUrinaryTractInfection.Updated > @LastUpdated
	and	BaseUrinaryTractInfection.ContextCode = @ContextCode

	) source
	on	source.MergeUrinaryTractInfectionRecno = target.MergeUrinaryTractInfectionRecno

	when not matched
	then
		insert
			(
			MergeUrinaryTractInfectionRecno
			,ContextID
			,SymptomStartDateID
			,SymptomEndDateID
			,UrinaryTestRequestedDateID
			,UrinaryTestReceivedDateID
			,TreatmentStartDateID
			,TreatmentEndDateID
			,CatheterInsertedDateID
			,CatheterRemovedDateID
			,CatheterTypeID
			,WardID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.MergeUrinaryTractInfectionRecno
			,source.ContextID
			,source.SymptomStartDateID
			,source.SymptomEndDateID
			,source.UrinaryTestRequestedDateID
			,source.UrinaryTestReceivedDateID
			,source.TreatmentStartDateID
			,source.TreatmentEndDateID
			,source.CatheterInsertedDateID
			,source.CatheterRemovedDateID
			,source.CatheterTypeID
			,source.WardID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeUrinaryTractInfectionRecno = source.MergeUrinaryTractInfectionRecno
			,target.ContextID = source.ContextID
			,target.SymptomStartDateID = source.SymptomStartDateID
			,target.SymptomEndDateID = source.SymptomEndDateID
			,target.UrinaryTestRequestedDateID = source.UrinaryTestRequestedDateID
			,target.UrinaryTestReceivedDateID = source.UrinaryTestReceivedDateID
			,target.TreatmentStartDateID = source.TreatmentStartDateID
			,target.TreatmentEndDateID = source.TreatmentEndDateID
			,target.CatheterInsertedDateID = source.CatheterInsertedDateID
			,target.CatheterRemovedDateID = source.CatheterRemovedDateID
			,target.CatheterTypeID = source.CatheterTypeID
			,target.WardID = source.WardID
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






﻿
CREATE procedure [ETL].[BuildQCRBaseAuditAnswer]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge QCR.BaseAuditAnswer target
using ETL.TLoadQCRBaseAuditAnswer source
on	source.AuditAnswerRecno = target.AuditAnswerRecno

when not matched by source
then delete

when not matched 
then insert
	(
	AuditAnswerRecno
	,SourceUniqueID
	,ContextCode
	,AuditTime
	,AuditDate
	,LocationCode
	,WardCode
	,AuditTypeCode
	,QuestionCode
	,Answer
	,Created
	,Updated
	,ByWhom
	)	
	values
	(
	 source.AuditAnswerRecno
	,source.SourceUniqueID
	,source.ContextCode
	,source.AuditTime
	,source.AuditDate
	,source.LocationCode
	,source.WardCode
	,source.AuditTypeCode
	,source.QuestionCode
	,source.Answer
	,getdate()
	,getdate()
	,suser_name()
	)

when matched and not
(
	isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
	and isnull(target.ContextCode, 0) = isnull(source.ContextCode, 0)
	and isnull(target.AuditTime, getdate()) = isnull(source.AuditTime, getdate())
	and isnull(target.AuditDate, getdate()) = isnull(source.AuditDate, getdate())
	and isnull(target.LocationCode, 0) = isnull(source.LocationCode, 0)
	and isnull(target.WardCode, 0) = isnull(source.WardCode, 0)
	and isnull(target.AuditTypeCode, 0) = isnull(source.AuditTypeCode, 0)
	and isnull(target.QuestionCode, 0) = isnull(source.QuestionCode, 0)
	and isnull(target.Answer, 0) = isnull(source.Answer, 0)
)
then update set
	target.SourceUniqueID = source.SourceUniqueID
	,target.ContextCode = source.ContextCode
	,target.AuditTime = source.AuditTime
	,target.AuditDate = source.AuditDate
	,target.LocationCode = source.LocationCode
	,target.WardCode = source.WardCode
	,target.AuditTypeCode = source.AuditTypeCode
	,target.QuestionCode = source.QuestionCode
	,target.Answer = source.Answer
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
﻿
CREATE procedure [ETL].[ImportBloodManagementBaseTranexamicAcid] as

--import the data
exec ETL.BuildBloodManagementBaseTranexamicAcid


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[BloodManagement].[BaseTranexamicAcid]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
 exec ETL.BuildBloodManagementBaseTranexamicAcidReference 'CMFT||RECALL'



﻿CREATE procedure [ETL].[ImportOPDiary] as

-------------------------------------------------
--When		Who	What
--20141029	PDO	Created procedure
-------------------------------------------------

--load encounters and generate process list
exec ETL.LoadOPBaseDiary

--generate missing reference data
declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[OP].[BaseDiary]'
	,@ProcessListTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[OP].[ProcessList]'
	,@NewRows int

exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output,@Debug = 0 ,@ActivityTableOverride = null

if @NewRows > 0
	exec WH.BuildMember

--load reference values
exec ETL.LoadOPBaseDiaryReference


﻿CREATE procedure [ETL].[ImportCenAPCBaseVTECondition] as

--import the data
exec ETL.BuildCenAPCBaseVTECondition


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseVTECondition]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCBaseVTEConditionReference 'CEN||BEDMAN'


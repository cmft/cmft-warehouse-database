﻿create procedure [ETL].[ImportCenObservationBaseObservationProfile] as


--import the data
exec ETL.BuildCenObservationBaseObservationProfile


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Observation].[BaseObservationProfile]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildObservationBaseObservationProfileReference 'CEN||PTRACK'


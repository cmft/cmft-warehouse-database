﻿
CREATE procedure [ETL].[BuildCenObservationBaseObservationSet]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Observation.BaseObservationSet target
using
	(
	select
		ObservationSetRecno
		,SourceUniqueID
		,CasenoteNumber
		,DateOfBirth
		,SpecialtyID
		,LocationID
		,LocationStartTime
		,ReplacedSourceUniqueID
		,CurrentObservationSetFlag
		,AdditionalObservationSetFlag
		,AdmissionSourceUniqueID
		,EarlyWarningScoreRegimeApplicationID
		,ObservationProfileApplicationID
		,ObservationNotTakenReasonID
		,ClinicianPresentSeniorityID
		,StartDate
		,StartTime
		,TakenDate
		,TakenTime
		,OverallRiskIndexCode
		,OverallAssessedStatusID
		,AlertSeverityID
		,DueTime
		,DueTimeStatusID
		,DueTimeCreatedBySourceUniqueID
		,AlertChainID
		,FirstInAlertChain
		,LastInAlertChain
		,AlertChainMinutes
		,LastModifiedTime
		,ContextCode
	from
		ETL.TLoadCenObservationBaseObservationSet

	) source
	on	source.ObservationSetRecno = target.ObservationSetRecno
	and	source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CEN||PTRACK'
	then delete

	when not matched 
	then
		insert
			(
			ObservationSetRecno
			,SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SpecialtyID
			,LocationID
			,LocationStartTime
			,ReplacedSourceUniqueID
			,CurrentObservationSetFlag
			,AdditionalObservationSetFlag
			,AdmissionSourceUniqueID
			,EarlyWarningScoreRegimeApplicationID
			,ObservationProfileApplicationID
			,ObservationNotTakenReasonID
			,ClinicianPresentSeniorityID
			,StartDate
			,StartTime
			,TakenDate
			,TakenTime
			,OverallRiskIndexCode
			,OverallAssessedStatusID
			,AlertSeverityID
			,DueTime
			,DueTimeStatusID
			,DueTimeCreatedBySourceUniqueID
			,AlertChainID
			,FirstInAlertChain
			,LastInAlertChain
			,AlertChainMinutes
			,LastModifiedTime
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.ObservationSetRecno
			,source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SpecialtyID
			,source.LocationID
			,source.LocationStartTime
			,source.ReplacedSourceUniqueID
			,source.CurrentObservationSetFlag
			,source.AdditionalObservationSetFlag
			,source.AdmissionSourceUniqueID
			,source.EarlyWarningScoreRegimeApplicationID
			,source.ObservationProfileApplicationID
			,source.ObservationNotTakenReasonID
			,source.ClinicianPresentSeniorityID
			,source.StartDate
			,source.StartTime
			,source.TakenDate
			,source.TakenTime
			,source.OverallRiskIndexCode
			,source.OverallAssessedStatusID
			,source.AlertSeverityID
			,source.DueTime
			,source.DueTimeStatusID
			,source.DueTimeCreatedBySourceUniqueID
			,source.AlertChainID
			,source.FirstInAlertChain
			,source.LastInAlertChain
			,source.AlertChainMinutes
			,source.LastModifiedTime
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched 
	and not
			isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	then
		update 
		set
			target.ObservationSetRecno = source.ObservationSetRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.LocationStartTime = source.LocationStartTime
			,target.ReplacedSourceUniqueID = source.ReplacedSourceUniqueID
			,target.CurrentObservationSetFlag = source.CurrentObservationSetFlag
			,target.AdditionalObservationSetFlag = source.AdditionalObservationSetFlag
			,target.AdmissionSourceUniqueID = source.AdmissionSourceUniqueID
			,target.EarlyWarningScoreRegimeApplicationID = source.EarlyWarningScoreRegimeApplicationID
			,target.ObservationProfileApplicationID = source.ObservationProfileApplicationID
			,target.ObservationNotTakenReasonID = source.ObservationNotTakenReasonID
			,target.ClinicianPresentSeniorityID = source.ClinicianPresentSeniorityID
			,target.StartDate = source.StartDate
			,target.StartTime = source.StartTime
			,target.TakenDate = source.TakenDate
			,target.TakenTime = source.TakenTime
			,target.OverallRiskIndexCode = source.OverallRiskIndexCode
			,target.OverallAssessedStatusID = source.OverallAssessedStatusID
			,target.AlertSeverityID = source.AlertSeverityID
			,target.DueTime = source.DueTime
			,target.DueTimeStatusID = source.DueTimeStatusID
			,target.DueTimeCreatedBySourceUniqueID = source.DueTimeCreatedBySourceUniqueID
			,target.AlertChainID = source.AlertChainID
			,target.FirstInAlertChain = source.FirstInAlertChain
			,target.LastInAlertChain = source.LastInAlertChain
			,target.AlertChainMinutes = source.AlertChainMinutes
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

﻿CREATE procedure [ETL].[BuildCenSCRBaseDefinitiveTreatment]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	,@ContextCode varchar(100) = 'CEN||SCR'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge SCR.BaseDefinitiveTreatment target
using [$(Warehouse)].SCR.DefinitiveTreatment source
on	source.UniqueRecordId = target.UniqueRecordID

when matched and not
(
	isnull(target.[ReferralUniqueRecordID], 0) = isnull(source.[ReferralUniqueRecordId], 0)
	
	and isnull(target.[PatientPathwayID], '') = isnull(source.[PatientPathwayId], '')
	and isnull(target.[DecisionToTreatDate], getdate()) = isnull(source.[DecisionToTreatDate], getdate())
	and isnull(target.[DTTOrganisationCode], '') = isnull(source.[DTTOrganisationCode], '')
	and isnull(target.[FirstDefinitiveTreatmentDate], getdate()) = isnull(source.[FirstDefinitiveTreatmentDate], getdate())
	and isnull(target.[InitialTreatmentCode], '') = isnull(source.[InitialTreatmentCode], '')
	and isnull(target.[TreatmentOrganisationCode], '') = isnull(source.[TreatmentOrganisationCode], '')
	and isnull(target.[TreatmentEventTypeCode], '') = isnull(source.[TreatmentEventTypeCode], '')
	and isnull(target.[TreatmentSettingCode], '') = isnull(source.[TreatmentSettingCode], '')
	and isnull(target.[PriorityCode], '') = isnull(source.[PriorityCode], '')
	and isnull(target.[TreatmentIntentCode], '') = isnull(source.[TreatmentIntentCode], '')
	and isnull(target.[SupportTypeCode], '') = isnull(source.[SupportTypeCode], '')
	and isnull(target.[ClinicalTrial], 0) = isnull(source.[ClinicalTrial], 0)
	and isnull(target.[WaitingTimeAdjustment], 0) = isnull(source.[WaitingTimeAdjustment], 0)
	and isnull(target.[AdjustmentReasonCode], 0) = isnull(source.[AdjustmentReasonCode], 0)
	and isnull(target.[DelayReasonCode], 0) = isnull(source.[DelayReasonCode], 0)
	and isnull(target.[TreatmentNo], 0) = isnull(source.[TreatmentNo], 0)
	and isnull(target.[TreatmentNoID], 0) = isnull(source.[TreatmentNoId], 0)
	
	and isnull(target.[ValidatedForUpload], 0) = isnull(source.[ValidatedForUpload], 0)
	and isnull(target.[DelayReasonComments], '') = isnull(source.[DelayReasonComments], '')
	and isnull(target.[TrackingNotes], '') = isnull(source.[TrackingNotes], '')
	and isnull(target.[AllTrackingNotes], '') = isnull(source.[AllTrackingNotes], '')
)
	
then update set
	target.[ReferralUniqueRecordID] = source.[ReferralUniqueRecordId]
	,target.[PatientPathwayID] = source.[PatientPathwayId]
	,target.[DecisionToTreatDate] = source.[DecisionToTreatDate]
	,target.[DTTOrganisationCode] = source.[DTTOrganisationCode]
	,target.[FirstDefinitiveTreatmentDate] = source.[FirstDefinitiveTreatmentDate]
	,target.[InitialTreatmentCode] = source.[InitialTreatmentCode]
	,target.[TreatmentOrganisationCode] = source.[TreatmentOrganisationCode]
	,target.[TreatmentEventTypeCode] = source.[TreatmentEventTypeCode]
	,target.[TreatmentSettingCode] = source.[TreatmentSettingCode]
	,target.[PriorityCode] = source.[PriorityCode]
	,target.[TreatmentIntentCode] = source.[TreatmentIntentCode]
	,target.[SupportTypeCode] = source.[SupportTypeCode]
	,target.[ClinicalTrial] = source.[ClinicalTrial]
	,target.[WaitingTimeAdjustment] = source.[WaitingTimeAdjustment]
	,target.[AdjustmentReasonCode] = source.[AdjustmentReasonCode]
	,target.[DelayReasonCode] = source.[DelayReasonCode]
	,target.[TreatmentNo] = source.[TreatmentNo]
	,target.[TreatmentNoID] = source.[TreatmentNoId]
	,target.[ValidatedForUpload] = source.[ValidatedForUpload]
	,target.[DelayReasonComments] = source.[DelayReasonComments]
	,target.[TrackingNotes] = source.[TrackingNotes]
	,target.[AllTrackingNotes] = source.[AllTrackingNotes]	
	,target.Updated = getdate()
	,target.ByWhom = suser_name()
			
when not matched by source
then delete

when not matched 
then insert
(
	 [UniqueRecordID]
	,[ReferralUniqueRecordID]
	,[PatientPathwayID]
	,[DecisionToTreatDate]
	,[DTTOrganisationCode]
	,[FirstDefinitiveTreatmentDate]
	,[InitialTreatmentCode]
	,[TreatmentOrganisationCode]
	,[TreatmentEventTypeCode]
	,[TreatmentSettingCode]
	,[PriorityCode]
	,[TreatmentIntentCode]
	,[SupportTypeCode]
	,[ClinicalTrial]
	,[WaitingTimeAdjustment]
	,[AdjustmentReasonCode]
	,[DelayReasonCode]
	,[TreatmentNo]
	,[TreatmentNoID]
	,[ValidatedForUpload]
	,[DelayReasonComments]
	,[TrackingNotes]
	,[AllTrackingNotes]	
	,ContextCode
	,Created
	,Updated
	,ByWhom
)	
values
(
	 source.[UniqueRecordId]
	,source.[ReferralUniqueRecordId]
	,source.[PatientPathwayId]
	,source.[DecisionToTreatDate]
	,source.[DTTOrganisationCode]
	,source.[FirstDefinitiveTreatmentDate]
	,source.[InitialTreatmentCode]
	,source.[TreatmentOrganisationCode]
	,source.[TreatmentEventTypeCode]
	,source.[TreatmentSettingCode]
	,source.[PriorityCode]
	,source.[TreatmentIntentCode]
	,source.[SupportTypeCode]
	,source.[ClinicalTrial]
	,source.[WaitingTimeAdjustment]
	,source.[AdjustmentReasonCode]
	,source.[DelayReasonCode]
	,source.[TreatmentNo]
	,source.[TreatmentNoId]
	,source.[ValidatedForUpload]
	,source.[DelayReasonComments]
	,source.[TrackingNotes]
	,source.[AllTrackingNotes]	
	,@ContextCode
	,getdate()
	,getdate()
	,suser_name()
)

output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

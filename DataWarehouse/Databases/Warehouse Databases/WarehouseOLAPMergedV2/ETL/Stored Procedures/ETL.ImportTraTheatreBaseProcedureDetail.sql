﻿CREATE procedure [ETL].[ImportTraTheatreBaseProcedureDetail] as

--import the data
exec ETL.BuildTraTheatreBaseProcedureDetail


--Build the BaseReference table
exec ETL.BuildTheatreBaseProcedureDetailReference 'TRA||ORMIS'

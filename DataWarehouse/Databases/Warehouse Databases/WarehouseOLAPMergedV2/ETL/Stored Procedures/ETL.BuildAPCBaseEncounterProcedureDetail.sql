﻿create procedure ETL.[BuildAPCBaseEncounterProcedureDetail]
	 @EncounterContextCode varchar(10)
	,@ProcedureDetailContextCode varchar(10)
as


merge
	APC.BaseEncounterProcedureDetail target
using
	(
	select
		 EncounterRecno
		,ProcedureDetailSourceUniqueID
	from
		[$(Warehouse)].APC.EncounterProcedureDetail
	) source
	on	source.EncounterRecno = target.EncounterRecno
	and	source.ProcedureDetailSourceUniqueID = target.ProcedureDetailSourceUniqueID
	and	target.EncounterContextCode = @EncounterContextCode
	and	target.ProcedureDetailContextCode = @ProcedureDetailContextCode

	when not matched by source
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,ProcedureDetailSourceUniqueID
			,EncounterContextCode
			,ProcedureDetailContextCode
			)
		values
			(
			 source.EncounterRecno
			,source.ProcedureDetailSourceUniqueID
			,@EncounterContextCode
			,@ProcedureDetailContextCode
			)

;


﻿
CREATE procedure [ETL].[BuildCenObservationBaseObservationProfile]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	Observation.BaseObservationProfile target
using
	(
	select
		ObservationProfileRecno
		,SourceUniqueID
		,CasenoteNumber
		,DateOfBirth
		,SpecialtyID
		,LocationID
		,AdmissionSourceUniqueID
		,ProfileID
		,ProfileTemplateID
		,TemplatePriorityID
		,DrivingTableCode
		,DrivingTableID
		,ProfileReasonID
		,StepNumber
		,IterationNumber
		,StartTime
		,EndTime
		,CreatedBy
		,CreatedTime
		,LastModifiedBy
		,LastModifiedTime
		,Active
		,ContextCode
	from
		ETL.TLoadCenObservationBaseObservationProfile

	) source
	on	source.ObservationProfileRecno = target.ObservationProfileRecno
	and	source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CEN||PTRACK'
	then delete

	when not matched 
	then
		insert
			(
			 ObservationProfileRecno
			,SourceUniqueID
			,CasenoteNumber
			,DateOfBirth
			,SpecialtyID
			,LocationID
			,AdmissionSourceUniqueID
			,ProfileID
			,ProfileTemplateID
			,TemplatePriorityID
			,DrivingTableCode
			,DrivingTableID
			,ProfileReasonID
			,StepNumber
			,IterationNumber
			,StartTime
			,EndTime
			,CreatedBy
			,CreatedTime
			,LastModifiedBy
			,LastModifiedTime
			,Active
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.ObservationProfileRecno
			,source.SourceUniqueID
			,source.CasenoteNumber
			,source.DateOfBirth
			,source.SpecialtyID
			,source.LocationID
			,source.AdmissionSourceUniqueID
			,source.ProfileID
			,source.ProfileTemplateID
			,source.TemplatePriorityID
			,source.DrivingTableCode
			,source.DrivingTableID
			,source.ProfileReasonID
			,source.StepNumber
			,source.IterationNumber
			,source.StartTime
			,source.EndTime
			,source.CreatedBy
			,source.CreatedTime
			,source.LastModifiedBy
			,source.LastModifiedTime
			,source.Active
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched 
	and not
			isnull(target.LastModifiedTime, getdate()) = isnull(source.LastModifiedTime, getdate())
	then
		update 
		set
			target.ObservationProfileRecno = source.ObservationProfileRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.CasenoteNumber = source.CasenoteNumber
			,target.DateOfBirth = source.DateOfBirth
			,target.SpecialtyID = source.SpecialtyID
			,target.LocationID = source.LocationID
			,target.AdmissionSourceUniqueID = source.AdmissionSourceUniqueID
			,target.ProfileID = source.ProfileID
			,target.ProfileTemplateID = source.ProfileTemplateID
			,target.TemplatePriorityID = source.TemplatePriorityID
			,target.DrivingTableCode = source.DrivingTableCode
			,target.DrivingTableID = source.DrivingTableID
			,target.ProfileReasonID = source.ProfileReasonID
			,target.StepNumber = source.StepNumber
			,target.IterationNumber = source.IterationNumber
			,target.StartTime = source.StartTime
			,target.EndTime = source.EndTime
			,target.CreatedBy = source.CreatedBy
			,target.CreatedTime = source.CreatedTime
			,target.LastModifiedBy = source.LastModifiedBy
			,target.LastModifiedTime = source.LastModifiedTime
			,target.Active = source.Active
			,target.ContextCode = source.ContextCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			
output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime

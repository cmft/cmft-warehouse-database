﻿CREATE procedure [ETL].[BuildTraOPBaseEncounter]
as

-------------------------------------------------
--When		Who	What
--20140916	PDO	DirectorateCode and PurchaserCode are assigned as part of the allocation process therefore removed from UPDATE check
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

select
	 EncounterRecno = cast(EncounterRecno as int)
	,SourceUniqueID = cast(SourceUniqueID as varchar(50))
	,SourcePatientNo = cast(SourcePatientNo as varchar(20))
	,SourceEncounterNo = cast(SourceEncounterNo as varchar(20))
	,PatientTitle = cast(PatientTitle as varchar(10))
	,PatientForename = cast(PatientForename as varchar(100))
	,PatientSurname = cast(PatientSurname as varchar(100))
	,DateOfBirth = cast(DateOfBirth as datetime)
	,DateOfDeath = cast(DateOfDeath as smalldatetime)
	,SexCode = cast(SexCode as varchar(10))
	,NHSNumber = cast(NHSNumber as varchar(17))
	,NHSNumberStatusCode = cast(NHSNumberStatusCode as varchar(10))
	,DistrictNo = cast(DistrictNo as varchar(12))
	,Postcode = cast(Postcode as varchar(8))
	,PatientAddress1 = cast(PatientAddress1 as varchar(100))
	,PatientAddress2 = cast(PatientAddress2 as varchar(100))
	,PatientAddress3 = cast(PatientAddress3 as varchar(100))
	,PatientAddress4 = cast(PatientAddress4 as varchar(100))
	,DHACode = cast(DHACode as varchar(3))
	,EthnicOriginCode = cast(EthnicOriginCode as varchar(10))
	,MaritalStatusCode = cast(MaritalStatusCode as varchar(10))
	,ReligionCode = cast(ReligionCode as varchar(10))
	,RegisteredGpCode = cast(RegisteredGpCode as varchar(8))
	,RegisteredGpPracticeCode = cast(RegisteredGpPracticeCode as varchar(8))
	,SiteCode = cast(SiteCode as varchar(10))
	,AppointmentDate = cast(AppointmentDate as smalldatetime)
	,AppointmentTime = cast(AppointmentTime as smalldatetime)
	,ClinicCode = cast(ClinicCode as varchar(10))
	,AdminCategoryCode = cast(AdminCategoryCode as varchar(10))
	,SourceOfReferralCode = cast(SourceOfReferralCode as varchar(10))
	,ReasonForReferralCode = cast(ReasonForReferralCode as varchar(10))
	,PriorityCode = cast(PriorityCode as varchar(10))
	,FirstAttendanceFlag = cast(FirstAttendanceFlag as varchar(10))
	,DNACode = cast(DNACode as varchar(10))
	,AppointmentStatusCode = cast(AppointmentStatusCode as varchar(10))
	,CancelledByCode = cast(CancelledByCode as varchar(10))
	,TransportRequiredFlag = cast(TransportRequiredFlag as varchar(10))
	,AttendanceOutcomeCode = cast(AttendanceOutcomeCode as varchar(10))
	,AppointmentTypeCode = cast(AppointmentTypeCode as varchar(10))
	,DisposalCode = cast(DisposalCode as varchar(10))
	,ConsultantCode = cast(ConsultantCode as varchar(20)) -- changed as truncating Trafford consultants. Have also amended other procs
	,SpecialtyCode = cast(SpecialtyCode as varchar(10))
	,ReferralConsultantCode = cast(ReferralConsultantCode as varchar(20)) -- changed as truncating Trafford consultants. Have also amended other procs
	,ReferralSpecialtyCode = cast(ReferralSpecialtyCode as varchar(10))
	,BookingTypeCode = cast(BookingTypeCode as varchar(10))
	,CasenoteNo = cast(CasenoteNo as varchar(16))
	,AppointmentCreateDate = cast(AppointmentCreateDate as smalldatetime)
	,EpisodicGpCode = cast(EpisodicGpCode as varchar(8))
	,EpisodicGpPracticeCode = cast(EpisodicGpPracticeCode as varchar(6))
	,DoctorCode = cast(DoctorCode as varchar(20))
	,PrimaryDiagnosisCode = cast(PrimaryDiagnosisCode as varchar(10))
	,SubsidiaryDiagnosisCode = cast(SubsidiaryDiagnosisCode as varchar(10))
	,SecondaryDiagnosisCode1 = cast(SecondaryDiagnosisCode1 as varchar(10))
	,SecondaryDiagnosisCode2 = cast(SecondaryDiagnosisCode2 as varchar(10))
	,SecondaryDiagnosisCode3 = cast(SecondaryDiagnosisCode3 as varchar(10))
	,SecondaryDiagnosisCode4 = cast(SecondaryDiagnosisCode4 as varchar(10))
	,SecondaryDiagnosisCode5 = cast(SecondaryDiagnosisCode5 as varchar(10))
	,SecondaryDiagnosisCode6 = cast(SecondaryDiagnosisCode6 as varchar(10))
	,SecondaryDiagnosisCode7 = cast(SecondaryDiagnosisCode7 as varchar(10))
	,SecondaryDiagnosisCode8 = cast(SecondaryDiagnosisCode8 as varchar(10))
	,SecondaryDiagnosisCode9 = cast(SecondaryDiagnosisCode9 as varchar(10))
	,SecondaryDiagnosisCode10 = cast(SecondaryDiagnosisCode10 as varchar(10))
	,SecondaryDiagnosisCode11 = cast(SecondaryDiagnosisCode11 as varchar(10))
	,SecondaryDiagnosisCode12 = cast(SecondaryDiagnosisCode12 as varchar(10))
	,PrimaryOperationCode = cast(PrimaryOperationCode as varchar(10))
	,PrimaryProcedureDate = cast(PrimaryProcedureDate as smalldatetime)
	,SecondaryProcedureCode1 = cast(SecondaryProcedureCode1 as varchar(10))
	,SecondaryProcedureDate1 = cast(SecondaryProcedureDate1 as smalldatetime)
	,SecondaryProcedureCode2 = cast(SecondaryProcedureCode2 as varchar(10))
	,SecondaryProcedureDate2 = cast(SecondaryProcedureDate2 as smalldatetime)
	,SecondaryProcedureCode3 = cast(SecondaryProcedureCode3 as varchar(10))
	,SecondaryProcedureDate3 = cast(SecondaryProcedureDate3 as smalldatetime)
	,SecondaryProcedureCode4 = cast(SecondaryProcedureCode4 as varchar(10))
	,SecondaryProcedureDate4 = cast(SecondaryProcedureDate4 as smalldatetime)
	,SecondaryProcedureCode5 = cast(SecondaryProcedureCode5 as varchar(10))
	,SecondaryProcedureDate5 = cast(SecondaryProcedureDate5 as smalldatetime)
	,SecondaryProcedureCode6 = cast(SecondaryProcedureCode6 as varchar(10))
	,SecondaryProcedureDate6 = cast(SecondaryProcedureDate6 as smalldatetime)
	,SecondaryProcedureCode7 = cast(SecondaryProcedureCode7 as varchar(10))
	,SecondaryProcedureDate7 = cast(SecondaryProcedureDate7 as smalldatetime)
	,SecondaryProcedureCode8 = cast(SecondaryProcedureCode8 as varchar(10))
	,SecondaryProcedureDate8 = cast(SecondaryProcedureDate8 as smalldatetime)
	,SecondaryProcedureCode9 = cast(SecondaryProcedureCode9 as varchar(10))
	,SecondaryProcedureDate9 = cast(SecondaryProcedureDate9 as smalldatetime)
	,SecondaryProcedureCode10 = cast(SecondaryProcedureCode10 as varchar(10))
	,SecondaryProcedureDate10 = cast(SecondaryProcedureDate10 as smalldatetime)
	,SecondaryProcedureCode11 = cast(SecondaryProcedureCode11 as varchar(10))
	,SecondaryProcedureDate11 = cast(SecondaryProcedureDate11 as smalldatetime)
	,PurchaserCode = cast(PurchaserCode as varchar(10))
	,ProviderCode = cast(ProviderCode as varchar(10))
	,ContractSerialNo = cast(ContractSerialNo as varchar(6))
	,ReferralDate = cast(ReferralDate as smalldatetime)
	,RTTPathwayID = cast(RTTPathwayID as varchar(25))
	,RTTPathwayCondition = cast(RTTPathwayCondition as varchar(20))
	,RTTStartDate = cast(RTTStartDate as smalldatetime)
	,RTTEndDate = cast(RTTEndDate as smalldatetime)
	,RTTSpecialtyCode = cast(RTTSpecialtyCode as varchar(10))
	,RTTCurrentProviderCode = cast(RTTCurrentProviderCode as varchar(10))
	,RTTCurrentStatusCode = cast(RTTCurrentStatusCode as varchar(10))
	,RTTCurrentStatusDate = cast(RTTCurrentStatusDate as smalldatetime)
	,RTTCurrentPrivatePatientFlag = cast(RTTCurrentPrivatePatientFlag as bit)
	,RTTOverseasStatusFlag = cast(RTTOverseasStatusFlag as bit)
	,RTTPeriodStatusCode = cast(RTTPeriodStatusCode as varchar(10))
	,AppointmentCategoryCode = cast(AppointmentCategoryCode as varchar(10))
	,AppointmentCreatedBy = cast(AppointmentCreatedBy as varchar(10))
	,AppointmentCancelDate = cast(AppointmentCancelDate as smalldatetime)
	,LastRevisedDate = cast(LastRevisedDate as smalldatetime)
	,LastRevisedBy = cast(LastRevisedBy as varchar(10))
	,OverseasStatusFlag = cast(OverseasStatusFlag as varchar(10))
	,PatientChoiceCode = cast(PatientChoiceCode as varchar(10))
	,ScheduledCancelReasonCode = cast(ScheduledCancelReasonCode as varchar(10))
	,PatientCancelReason = cast(PatientCancelReason as varchar(50))
	,DischargeDate = cast(DischargeDate as smalldatetime)
	,QM08StartWaitDate = cast(QM08StartWaitDate as smalldatetime)
	,QM08EndWaitDate = cast(QM08EndWaitDate as smalldatetime)
	,DestinationSiteCode = cast(DestinationSiteCode as varchar(10))
	,EBookingReferenceNo = cast(EBookingReferenceNo as varchar(50))
	,InterfaceCode = cast(InterfaceCode as varchar(5))
	,Created = cast(Created as datetime)
	,Updated = cast(Updated as datetime)
	,ByWhom = cast(ByWhom as varchar(50))
	,LocalAdminCategoryCode = cast(LocalAdminCategoryCode as varchar(10))
	,PCTCode = cast(PCTCode as varchar(10))
	,LocalityCode = cast(LocalityCode as varchar(10))
	,AgeCode = cast(AgeCode as varchar(27))
	,HRGCode = cast(HRGCode as varchar(10))
	,Cases = cast(Cases as int)
	,LengthOfWait = cast(LengthOfWait as int)
	,IsWardAttender = cast(IsWardAttender as bit)
	,RTTActivity = cast(RTTActivity as int)
	,RTTBreachStatusCode = cast(RTTBreachStatusCode as varchar(10))
	,ClockStartDate = cast(ClockStartDate as smalldatetime)
	,RTTBreachDate = cast(RTTBreachDate as smalldatetime)
	,RTTTreated = cast(RTTTreated as int)
	,DirectorateCode = cast(DirectorateCode as varchar(5))
	,ReferralSpecialtyTypeCode = cast(ReferralSpecialtyTypeCode as varchar(10))
	,LastDNAorPatientCancelledDate = cast(LastDNAorPatientCancelledDate as smalldatetime)
	,ReferredByCode = cast(ReferredByCode as varchar(10))
	,ReferrerCode = cast(ReferrerCode as varchar(10))
	,AppointmentOutcomeCode = cast(AppointmentOutcomeCode as varchar(10))
	,MedicalStaffTypeCode = cast(MedicalStaffTypeCode as varchar(10))
	,ContextCode = cast(ContextCode as varchar(8))
	,TreatmentFunctionCode = cast(TreatmentFunctionCode as varchar(10))
	,CommissioningSerialNo = cast(CommissioningSerialNo as varchar(10))
	,DerivedFirstAttendanceFlag = cast(DerivedFirstAttendanceFlag as varchar(1))
	,AttendanceIdentifier = cast(AttendanceIdentifier as varchar(40))
	,RegisteredGpAtAppointmentCode = cast(RegisteredGpAtAppointmentCode as varchar(8))
	,RegisteredGpPracticeAtAppointmentCode = cast(RegisteredGpPracticeAtAppointmentCode as varchar(6))
	,ReferredByConsultantCode = cast(ReferredByConsultantCode as varchar(10))
	,ReferredByGpCode = cast(ReferredByGpCode as varchar(10))
	,ReferredByGdpCode = cast(ReferredByGdpCode as varchar(10))
	,CCGCode = cast(CCGCode as varchar)
	,EpisodicPostcode  = cast(EpisodicPostCode as varchar (8))
	,GpCodeAtAppointment = cast(GpCodeAtAppointment as varchar (8))
	,GpPracticeCodeAtAppointment = cast(GpPracticeCodeAtAppointment as varchar (8))
	,PostcodeAtAppointment = cast(PostcodeAtAppointment as varchar (8))
	,AppointmentResidenceCCGCode = cast(AppointmentResidenceCCGCode as varchar)
into
	#Encounter
from
	ETL.TLoadTraOPBaseEncounter Encounter

CREATE UNIQUE CLUSTERED INDEX IX_TLoadTraOPBaseEncounter ON #Encounter
	(
	 EncounterRecno
	,ContextCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


merge
	OP.BaseEncounter target
using
	(
	select
		 EncounterRecno
		,SourceUniqueID
		,SourcePatientNo
		,SourceEncounterNo
		,PatientTitle
		,PatientForename
		,PatientSurname
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,NHSNumber
		,NHSNumberStatusCode
		,DistrictNo
		,Postcode
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,DHACode
		,EthnicOriginCode
		,MaritalStatusCode
		,ReligionCode
		,RegisteredGpCode
		,RegisteredGpPracticeCode
		,SiteCode
		,AppointmentDate
		,AppointmentTime
		,ClinicCode
		,AdminCategoryCode
		,SourceOfReferralCode
		,ReasonForReferralCode
		,PriorityCode
		,FirstAttendanceFlag
		,DNACode
		,AppointmentStatusCode
		,CancelledByCode
		,TransportRequiredFlag
		,AttendanceOutcomeCode
		,AppointmentTypeCode
		,DisposalCode
		,ConsultantCode
		,SpecialtyCode
		,ReferralConsultantCode
		,ReferralSpecialtyCode
		,BookingTypeCode
		,CasenoteNo
		,AppointmentCreateDate
		,EpisodicGpCode
		,EpisodicGpPracticeCode
		,DoctorCode
		,PrimaryDiagnosisCode
		,SubsidiaryDiagnosisCode
		,SecondaryDiagnosisCode1
		,SecondaryDiagnosisCode2
		,SecondaryDiagnosisCode3
		,SecondaryDiagnosisCode4
		,SecondaryDiagnosisCode5
		,SecondaryDiagnosisCode6
		,SecondaryDiagnosisCode7
		,SecondaryDiagnosisCode8
		,SecondaryDiagnosisCode9
		,SecondaryDiagnosisCode10
		,SecondaryDiagnosisCode11
		,SecondaryDiagnosisCode12
		,PrimaryOperationCode
		,PrimaryProcedureDate
		,SecondaryProcedureCode1
		,SecondaryProcedureDate1
		,SecondaryProcedureCode2
		,SecondaryProcedureDate2
		,SecondaryProcedureCode3
		,SecondaryProcedureDate3
		,SecondaryProcedureCode4
		,SecondaryProcedureDate4
		,SecondaryProcedureCode5
		,SecondaryProcedureDate5
		,SecondaryProcedureCode6
		,SecondaryProcedureDate6
		,SecondaryProcedureCode7
		,SecondaryProcedureDate7
		,SecondaryProcedureCode8
		,SecondaryProcedureDate8
		,SecondaryProcedureCode9
		,SecondaryProcedureDate9
		,SecondaryProcedureCode10
		,SecondaryProcedureDate10
		,SecondaryProcedureCode11
		,SecondaryProcedureDate11
		,PurchaserCode
		,ProviderCode
		,ContractSerialNo
		,ReferralDate
		,RTTPathwayID
		,RTTPathwayCondition
		,RTTStartDate
		,RTTEndDate
		,RTTSpecialtyCode
		,RTTCurrentProviderCode
		,RTTCurrentStatusCode
		,RTTCurrentStatusDate
		,RTTCurrentPrivatePatientFlag
		,RTTOverseasStatusFlag
		,RTTPeriodStatusCode
		,AppointmentCategoryCode
		,AppointmentCreatedBy
		,AppointmentCancelDate
		,LastRevisedDate
		,LastRevisedBy
		,OverseasStatusFlag
		,PatientChoiceCode
		,ScheduledCancelReasonCode
		,PatientCancelReason
		,DischargeDate
		,QM08StartWaitDate
		,QM08EndWaitDate
		,DestinationSiteCode
		,EBookingReferenceNo
		,InterfaceCode
		,Created
		,Updated
		,ByWhom
		,LocalAdminCategoryCode
		,PCTCode
		,LocalityCode
		,AgeCode
		,HRGCode
		,Cases
		,LengthOfWait
		,IsWardAttender
		,RTTActivity
		,RTTBreachStatusCode
		,ClockStartDate
		,RTTBreachDate
		,RTTTreated
		,DirectorateCode
		,ReferralSpecialtyTypeCode
		,LastDNAorPatientCancelledDate
		,ReferredByCode
		,ReferrerCode
		,AppointmentOutcomeCode
		,MedicalStaffTypeCode
		,ContextCode
		,TreatmentFunctionCode
		,CommissioningSerialNo
		,DerivedFirstAttendanceFlag
		,AttendanceIdentifier
		,RegisteredGpAtAppointmentCode
		,RegisteredGpPracticeAtAppointmentCode
		,ReferredByConsultantCode
		,ReferredByGpCode
		,ReferredByGdpCode
		,CCGCode 
		,EpisodicPostCode
		,GpCodeAtAppointment 
		,GpPracticeCodeAtAppointment
		,PostcodeAtAppointment
		,AppointmentResidenceCCGCode
	from
		#Encounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.SourceUniqueID = target.SourceUniqueID

	when not matched by source
	and	target.ContextCode = 'TRA||UG'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceEncounterNo
			,PatientTitle
			,PatientForename
			,PatientSurname
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,NHSNumber
			,NHSNumberStatusCode
			,DistrictNo
			,Postcode
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,DHACode
			,EthnicOriginCode
			,MaritalStatusCode
			,ReligionCode
			,RegisteredGpCode
			,RegisteredGpPracticeCode
			,SiteCode
			,AppointmentDate
			,AppointmentTime
			,ClinicCode
			,AdminCategoryCode
			,SourceOfReferralCode
			,ReasonForReferralCode
			,PriorityCode
			,FirstAttendanceFlag
			,DNACode
			,AppointmentStatusCode
			,CancelledByCode
			,TransportRequiredFlag
			,AttendanceOutcomeCode
			,AppointmentTypeCode
			,DisposalCode
			,ConsultantCode
			,SpecialtyCode
			,ReferralConsultantCode
			,ReferralSpecialtyCode
			,BookingTypeCode
			,CasenoteNo
			,AppointmentCreateDate
			,EpisodicGpCode
			,EpisodicGpPracticeCode
			,DoctorCode
			,PrimaryDiagnosisCode
			,SubsidiaryDiagnosisCode
			,SecondaryDiagnosisCode1
			,SecondaryDiagnosisCode2
			,SecondaryDiagnosisCode3
			,SecondaryDiagnosisCode4
			,SecondaryDiagnosisCode5
			,SecondaryDiagnosisCode6
			,SecondaryDiagnosisCode7
			,SecondaryDiagnosisCode8
			,SecondaryDiagnosisCode9
			,SecondaryDiagnosisCode10
			,SecondaryDiagnosisCode11
			,SecondaryDiagnosisCode12
			,PrimaryOperationCode
			,PrimaryProcedureDate
			,SecondaryProcedureCode1
			,SecondaryProcedureDate1
			,SecondaryProcedureCode2
			,SecondaryProcedureDate2
			,SecondaryProcedureCode3
			,SecondaryProcedureDate3
			,SecondaryProcedureCode4
			,SecondaryProcedureDate4
			,SecondaryProcedureCode5
			,SecondaryProcedureDate5
			,SecondaryProcedureCode6
			,SecondaryProcedureDate6
			,SecondaryProcedureCode7
			,SecondaryProcedureDate7
			,SecondaryProcedureCode8
			,SecondaryProcedureDate8
			,SecondaryProcedureCode9
			,SecondaryProcedureDate9
			,SecondaryProcedureCode10
			,SecondaryProcedureDate10
			,SecondaryProcedureCode11
			,SecondaryProcedureDate11
			,PurchaserCode
			,ProviderCode
			,ContractSerialNo
			,ReferralDate
			,RTTPathwayID
			,RTTPathwayCondition
			,RTTStartDate
			,RTTEndDate
			,RTTSpecialtyCode
			,RTTCurrentProviderCode
			,RTTCurrentStatusCode
			,RTTCurrentStatusDate
			,RTTCurrentPrivatePatientFlag
			,RTTOverseasStatusFlag
			,RTTPeriodStatusCode
			,AppointmentCategoryCode
			,AppointmentCreatedBy
			,AppointmentCancelDate
			,LastRevisedDate
			,LastRevisedBy
			,OverseasStatusFlag
			,PatientChoiceCode
			,ScheduledCancelReasonCode
			,PatientCancelReason
			,DischargeDate
			,QM08StartWaitDate
			,QM08EndWaitDate
			,DestinationSiteCode
			,EBookingReferenceNo
			,InterfaceCode
			,Created
			,Updated
			,ByWhom
			,LocalAdminCategoryCode
			,PCTCode
			,LocalityCode
			,AgeCode
			,HRGCode
			,Cases
			,LengthOfWait
			,IsWardAttender
			,RTTActivity
			,RTTBreachStatusCode
			,ClockStartDate
			,RTTBreachDate
			,RTTTreated
			,DirectorateCode
			,ReferralSpecialtyTypeCode
			,LastDNAorPatientCancelledDate
			,ReferredByCode
			,ReferrerCode
			,AppointmentOutcomeCode
			,MedicalStaffTypeCode
			,ContextCode
			,TreatmentFunctionCode
			,CommissioningSerialNo
			,DerivedFirstAttendanceFlag
			,AttendanceIdentifier
			,RegisteredGpAtAppointmentCode
			,RegisteredGpPracticeAtAppointmentCode
			,ReferredByConsultantCode
			,ReferredByGpCode
			,ReferredByGdpCode
			,CCGCode 
			,EpisodicPostcode
			,GpCodeAtAppointment 
			,GpPracticeCodeAtAppointment
			,PostcodeAtAppointment
			,AppointmentResidenceCCGCode
			)
		values
			(
			 source.EncounterRecno
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceEncounterNo
			,source.PatientTitle
			,source.PatientForename
			,source.PatientSurname
			,source.DateOfBirth
			,source.DateOfDeath
			,source.SexCode
			,source.NHSNumber
			,source.NHSNumberStatusCode
			,source.DistrictNo
			,source.Postcode
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.DHACode
			,source.EthnicOriginCode
			,source.MaritalStatusCode
			,source.ReligionCode
			,source.RegisteredGpCode
			,source.RegisteredGpPracticeCode
			,source.SiteCode
			,source.AppointmentDate
			,source.AppointmentTime
			,source.ClinicCode
			,source.AdminCategoryCode
			,source.SourceOfReferralCode
			,source.ReasonForReferralCode
			,source.PriorityCode
			,source.FirstAttendanceFlag
			,source.DNACode
			,source.AppointmentStatusCode
			,source.CancelledByCode
			,source.TransportRequiredFlag
			,source.AttendanceOutcomeCode
			,source.AppointmentTypeCode
			,source.DisposalCode
			,source.ConsultantCode
			,source.SpecialtyCode
			,source.ReferralConsultantCode
			,source.ReferralSpecialtyCode
			,source.BookingTypeCode
			,source.CasenoteNo
			,source.AppointmentCreateDate
			,source.EpisodicGpCode
			,source.EpisodicGpPracticeCode
			,source.DoctorCode
			,source.PrimaryDiagnosisCode
			,source.SubsidiaryDiagnosisCode
			,source.SecondaryDiagnosisCode1
			,source.SecondaryDiagnosisCode2
			,source.SecondaryDiagnosisCode3
			,source.SecondaryDiagnosisCode4
			,source.SecondaryDiagnosisCode5
			,source.SecondaryDiagnosisCode6
			,source.SecondaryDiagnosisCode7
			,source.SecondaryDiagnosisCode8
			,source.SecondaryDiagnosisCode9
			,source.SecondaryDiagnosisCode10
			,source.SecondaryDiagnosisCode11
			,source.SecondaryDiagnosisCode12
			,source.PrimaryOperationCode
			,source.PrimaryProcedureDate
			,source.SecondaryProcedureCode1
			,source.SecondaryProcedureDate1
			,source.SecondaryProcedureCode2
			,source.SecondaryProcedureDate2
			,source.SecondaryProcedureCode3
			,source.SecondaryProcedureDate3
			,source.SecondaryProcedureCode4
			,source.SecondaryProcedureDate4
			,source.SecondaryProcedureCode5
			,source.SecondaryProcedureDate5
			,source.SecondaryProcedureCode6
			,source.SecondaryProcedureDate6
			,source.SecondaryProcedureCode7
			,source.SecondaryProcedureDate7
			,source.SecondaryProcedureCode8
			,source.SecondaryProcedureDate8
			,source.SecondaryProcedureCode9
			,source.SecondaryProcedureDate9
			,source.SecondaryProcedureCode10
			,source.SecondaryProcedureDate10
			,source.SecondaryProcedureCode11
			,source.SecondaryProcedureDate11
			,source.PurchaserCode
			,source.ProviderCode
			,source.ContractSerialNo
			,source.ReferralDate
			,source.RTTPathwayID
			,source.RTTPathwayCondition
			,source.RTTStartDate
			,source.RTTEndDate
			,source.RTTSpecialtyCode
			,source.RTTCurrentProviderCode
			,source.RTTCurrentStatusCode
			,source.RTTCurrentStatusDate
			,source.RTTCurrentPrivatePatientFlag
			,source.RTTOverseasStatusFlag
			,source.RTTPeriodStatusCode
			,source.AppointmentCategoryCode
			,source.AppointmentCreatedBy
			,source.AppointmentCancelDate
			,source.LastRevisedDate
			,source.LastRevisedBy
			,source.OverseasStatusFlag
			,source.PatientChoiceCode
			,source.ScheduledCancelReasonCode
			,source.PatientCancelReason
			,source.DischargeDate
			,source.QM08StartWaitDate
			,source.QM08EndWaitDate
			,source.DestinationSiteCode
			,source.EBookingReferenceNo
			,source.InterfaceCode
			,getdate()
			,getdate()
			,suser_name()
			,source.LocalAdminCategoryCode
			,source.PCTCode
			,source.LocalityCode
			,source.AgeCode
			,source.HRGCode
			,source.Cases
			,source.LengthOfWait
			,source.IsWardAttender
			,source.RTTActivity
			,source.RTTBreachStatusCode
			,source.ClockStartDate
			,source.RTTBreachDate
			,source.RTTTreated
			,source.DirectorateCode
			,source.ReferralSpecialtyTypeCode
			,source.LastDNAorPatientCancelledDate
			,source.ReferredByCode
			,source.ReferrerCode
			,source.AppointmentOutcomeCode
			,source.MedicalStaffTypeCode
			,source.ContextCode
			,source.TreatmentFunctionCode
			,source.CommissioningSerialNo
			,source.DerivedFirstAttendanceFlag
			,source.AttendanceIdentifier
			,source.RegisteredGpAtAppointmentCode
			,source.RegisteredGpPracticeAtAppointmentCode
			,source.ReferredByConsultantCode
			,source.ReferredByGpCode
			,source.ReferredByGdpCode
			,source.CCGCode 
			,source.EpisodicPostCode
			,source.GpCodeAtAppointment 
			,source.GpPracticeCodeAtAppointment
			,source.PostcodeAtAppointment
			,source.AppointmentResidenceCCGCode
			)

	when matched
	and not
		(
			isnull(target.EncounterRecno, 0) = isnull(source.EncounterRecno, 0)
		and isnull(target.SourceUniqueID, '') = isnull(source.SourceUniqueID, '')
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceEncounterNo, '') = isnull(source.SourceEncounterNo, '')
		and isnull(target.PatientTitle, '') = isnull(source.PatientTitle, '')
		and isnull(target.PatientForename, '') = isnull(source.PatientForename, '')
		and isnull(target.PatientSurname, '') = isnull(source.PatientSurname, '')
		and isnull(target.DateOfBirth, getdate()) = isnull(source.DateOfBirth, getdate())
		and isnull(target.DateOfDeath, getdate()) = isnull(source.DateOfDeath, getdate())
		and isnull(target.SexCode, '') = isnull(source.SexCode, '')
		and isnull(target.NHSNumber, '') = isnull(source.NHSNumber, '')
		and isnull(target.NHSNumberStatusCode, '') = isnull(source.NHSNumberStatusCode, '')
		and isnull(target.DistrictNo, '') = isnull(source.DistrictNo, '')
		and isnull(target.Postcode, '') = isnull(source.Postcode, '')
		and isnull(target.PatientAddress1, '') = isnull(source.PatientAddress1, '')
		and isnull(target.PatientAddress2, '') = isnull(source.PatientAddress2, '')
		and isnull(target.PatientAddress3, '') = isnull(source.PatientAddress3, '')
		and isnull(target.PatientAddress4, '') = isnull(source.PatientAddress4, '')
		and isnull(target.DHACode, '') = isnull(source.DHACode, '')
		and isnull(target.EthnicOriginCode, '') = isnull(source.EthnicOriginCode, '')
		and isnull(target.MaritalStatusCode, '') = isnull(source.MaritalStatusCode, '')
		and isnull(target.ReligionCode, '') = isnull(source.ReligionCode, '')
		and isnull(target.RegisteredGpCode, '') = isnull(source.RegisteredGpCode, '')
		and isnull(target.RegisteredGpPracticeCode, '') = isnull(source.RegisteredGpPracticeCode, '')
		and isnull(target.SiteCode, '') = isnull(source.SiteCode, '')
		and isnull(target.AppointmentDate, getdate()) = isnull(source.AppointmentDate, getdate())
		and isnull(target.AppointmentTime, getdate()) = isnull(source.AppointmentTime, getdate())
		and isnull(target.ClinicCode, '') = isnull(source.ClinicCode, '')
		and isnull(target.AdminCategoryCode, '') = isnull(source.AdminCategoryCode, '')
		and isnull(target.SourceOfReferralCode, '') = isnull(source.SourceOfReferralCode, '')
		and isnull(target.ReasonForReferralCode, '') = isnull(source.ReasonForReferralCode, '')
		and isnull(target.PriorityCode, '') = isnull(source.PriorityCode, '')
		and isnull(target.FirstAttendanceFlag, '') = isnull(source.FirstAttendanceFlag, '')
		and isnull(target.DNACode, '') = isnull(source.DNACode, '')
		and isnull(target.AppointmentStatusCode, '') = isnull(source.AppointmentStatusCode, '')
		and isnull(target.CancelledByCode, '') = isnull(source.CancelledByCode, '')
		and isnull(target.TransportRequiredFlag, '') = isnull(source.TransportRequiredFlag, '')
		and isnull(target.AttendanceOutcomeCode, '') = isnull(source.AttendanceOutcomeCode, '')
		and isnull(target.AppointmentTypeCode, '') = isnull(source.AppointmentTypeCode, '')
		and isnull(target.DisposalCode, '') = isnull(source.DisposalCode, '')
		and isnull(target.ConsultantCode, '') = isnull(source.ConsultantCode, '')
		and isnull(target.SpecialtyCode, '') = isnull(source.SpecialtyCode, '')
		and isnull(target.ReferralConsultantCode, '') = isnull(source.ReferralConsultantCode, '')
		and isnull(target.ReferralSpecialtyCode, '') = isnull(source.ReferralSpecialtyCode, '')
		and isnull(target.BookingTypeCode, '') = isnull(source.BookingTypeCode, '')
		and isnull(target.CasenoteNo, '') = isnull(source.CasenoteNo, '')
		and isnull(target.AppointmentCreateDate, getdate()) = isnull(source.AppointmentCreateDate, getdate())
		and isnull(target.EpisodicGpCode, '') = isnull(source.EpisodicGpCode, '')
		and isnull(target.EpisodicGpPracticeCode, '') = isnull(source.EpisodicGpPracticeCode, '')
		and isnull(target.DoctorCode, '') = isnull(source.DoctorCode, '')
		and isnull(target.PrimaryDiagnosisCode, '') = isnull(source.PrimaryDiagnosisCode, '')
		and isnull(target.SubsidiaryDiagnosisCode, '') = isnull(source.SubsidiaryDiagnosisCode, '')
		and isnull(target.SecondaryDiagnosisCode1, '') = isnull(source.SecondaryDiagnosisCode1, '')
		and isnull(target.SecondaryDiagnosisCode2, '') = isnull(source.SecondaryDiagnosisCode2, '')
		and isnull(target.SecondaryDiagnosisCode3, '') = isnull(source.SecondaryDiagnosisCode3, '')
		and isnull(target.SecondaryDiagnosisCode4, '') = isnull(source.SecondaryDiagnosisCode4, '')
		and isnull(target.SecondaryDiagnosisCode5, '') = isnull(source.SecondaryDiagnosisCode5, '')
		and isnull(target.SecondaryDiagnosisCode6, '') = isnull(source.SecondaryDiagnosisCode6, '')
		and isnull(target.SecondaryDiagnosisCode7, '') = isnull(source.SecondaryDiagnosisCode7, '')
		and isnull(target.SecondaryDiagnosisCode8, '') = isnull(source.SecondaryDiagnosisCode8, '')
		and isnull(target.SecondaryDiagnosisCode9, '') = isnull(source.SecondaryDiagnosisCode9, '')
		and isnull(target.SecondaryDiagnosisCode10, '') = isnull(source.SecondaryDiagnosisCode10, '')
		and isnull(target.SecondaryDiagnosisCode11, '') = isnull(source.SecondaryDiagnosisCode11, '')
		and isnull(target.SecondaryDiagnosisCode12, '') = isnull(source.SecondaryDiagnosisCode12, '')
		and isnull(target.PrimaryOperationCode, '') = isnull(source.PrimaryOperationCode, '')
		and isnull(target.PrimaryProcedureDate, getdate()) = isnull(source.PrimaryProcedureDate, getdate())
		and isnull(target.SecondaryProcedureCode1, '') = isnull(source.SecondaryProcedureCode1, '')
		and isnull(target.SecondaryProcedureDate1, getdate()) = isnull(source.SecondaryProcedureDate1, getdate())
		and isnull(target.SecondaryProcedureCode2, '') = isnull(source.SecondaryProcedureCode2, '')
		and isnull(target.SecondaryProcedureDate2, getdate()) = isnull(source.SecondaryProcedureDate2, getdate())
		and isnull(target.SecondaryProcedureCode3, '') = isnull(source.SecondaryProcedureCode3, '')
		and isnull(target.SecondaryProcedureDate3, getdate()) = isnull(source.SecondaryProcedureDate3, getdate())
		and isnull(target.SecondaryProcedureCode4, '') = isnull(source.SecondaryProcedureCode4, '')
		and isnull(target.SecondaryProcedureDate4, getdate()) = isnull(source.SecondaryProcedureDate4, getdate())
		and isnull(target.SecondaryProcedureCode5, '') = isnull(source.SecondaryProcedureCode5, '')
		and isnull(target.SecondaryProcedureDate5, getdate()) = isnull(source.SecondaryProcedureDate5, getdate())
		and isnull(target.SecondaryProcedureCode6, '') = isnull(source.SecondaryProcedureCode6, '')
		and isnull(target.SecondaryProcedureDate6, getdate()) = isnull(source.SecondaryProcedureDate6, getdate())
		and isnull(target.SecondaryProcedureCode7, '') = isnull(source.SecondaryProcedureCode7, '')
		and isnull(target.SecondaryProcedureDate7, getdate()) = isnull(source.SecondaryProcedureDate7, getdate())
		and isnull(target.SecondaryProcedureCode8, '') = isnull(source.SecondaryProcedureCode8, '')
		and isnull(target.SecondaryProcedureDate8, getdate()) = isnull(source.SecondaryProcedureDate8, getdate())
		and isnull(target.SecondaryProcedureCode9, '') = isnull(source.SecondaryProcedureCode9, '')
		and isnull(target.SecondaryProcedureDate9, getdate()) = isnull(source.SecondaryProcedureDate9, getdate())
		and isnull(target.SecondaryProcedureCode10, '') = isnull(source.SecondaryProcedureCode10, '')
		and isnull(target.SecondaryProcedureDate10, getdate()) = isnull(source.SecondaryProcedureDate10, getdate())
		and isnull(target.SecondaryProcedureCode11, '') = isnull(source.SecondaryProcedureCode11, '')
		and isnull(target.SecondaryProcedureDate11, getdate()) = isnull(source.SecondaryProcedureDate11, getdate())
		--and isnull(target.PurchaserCode, '') = isnull(source.PurchaserCode, '')
		and isnull(target.ProviderCode, '') = isnull(source.ProviderCode, '')
		and isnull(target.ContractSerialNo, '') = isnull(source.ContractSerialNo, '')
		and isnull(target.ReferralDate, getdate()) = isnull(source.ReferralDate, getdate())
		and isnull(target.RTTPathwayID, '') = isnull(source.RTTPathwayID, '')
		and isnull(target.RTTPathwayCondition, '') = isnull(source.RTTPathwayCondition, '')
		and isnull(target.RTTStartDate, getdate()) = isnull(source.RTTStartDate, getdate())
		and isnull(target.RTTEndDate, getdate()) = isnull(source.RTTEndDate, getdate())
		and isnull(target.RTTSpecialtyCode, '') = isnull(source.RTTSpecialtyCode, '')
		and isnull(target.RTTCurrentProviderCode, '') = isnull(source.RTTCurrentProviderCode, '')
		and isnull(target.RTTCurrentStatusCode, '') = isnull(source.RTTCurrentStatusCode, '')
		and isnull(target.RTTCurrentStatusDate, getdate()) = isnull(source.RTTCurrentStatusDate, getdate())
		and isnull(target.RTTCurrentPrivatePatientFlag, 0) = isnull(source.RTTCurrentPrivatePatientFlag, 0)
		and isnull(target.RTTOverseasStatusFlag, 0) = isnull(source.RTTOverseasStatusFlag, 0)
		and isnull(target.RTTPeriodStatusCode, '') = isnull(source.RTTPeriodStatusCode, '')
		and isnull(target.AppointmentCategoryCode, '') = isnull(source.AppointmentCategoryCode, '')
		and isnull(target.AppointmentCreatedBy, '') = isnull(source.AppointmentCreatedBy, '')
		and isnull(target.AppointmentCancelDate, getdate()) = isnull(source.AppointmentCancelDate, getdate())
		and isnull(target.LastRevisedDate, getdate()) = isnull(source.LastRevisedDate, getdate())
		and isnull(target.LastRevisedBy, '') = isnull(source.LastRevisedBy, '')
		and isnull(target.OverseasStatusFlag, '') = isnull(source.OverseasStatusFlag, '')
		and isnull(target.PatientChoiceCode, '') = isnull(source.PatientChoiceCode, '')
		and isnull(target.ScheduledCancelReasonCode, '') = isnull(source.ScheduledCancelReasonCode, '')
		and isnull(target.PatientCancelReason, '') = isnull(source.PatientCancelReason, '')
		and isnull(target.DischargeDate, getdate()) = isnull(source.DischargeDate, getdate())
		and isnull(target.QM08StartWaitDate, getdate()) = isnull(source.QM08StartWaitDate, getdate())
		and isnull(target.QM08EndWaitDate, getdate()) = isnull(source.QM08EndWaitDate, getdate())
		and isnull(target.DestinationSiteCode, '') = isnull(source.DestinationSiteCode, '')
		and isnull(target.EBookingReferenceNo, '') = isnull(source.EBookingReferenceNo, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.LocalAdminCategoryCode, '') = isnull(source.LocalAdminCategoryCode, '')
		and isnull(target.PCTCode, '') = isnull(source.PCTCode, '')
		and isnull(target.LocalityCode, '') = isnull(source.LocalityCode, '')
		and isnull(target.AgeCode, '') = isnull(source.AgeCode, '')
		and isnull(target.HRGCode, '') = isnull(source.HRGCode, '')
		and isnull(target.Cases, 0) = isnull(source.Cases, 0)
		and isnull(target.LengthOfWait, 0) = isnull(source.LengthOfWait, 0)
		and isnull(target.IsWardAttender, 0) = isnull(source.IsWardAttender, 0)
		and isnull(target.RTTActivity, 0) = isnull(source.RTTActivity, 0)
		and isnull(target.RTTBreachStatusCode, '') = isnull(source.RTTBreachStatusCode, '')
		and isnull(target.ClockStartDate, getdate()) = isnull(source.ClockStartDate, getdate())
		and isnull(target.RTTBreachDate, getdate()) = isnull(source.RTTBreachDate, getdate())
		and isnull(target.RTTTreated, 0) = isnull(source.RTTTreated, 0)
		--and isnull(target.DirectorateCode, '') = isnull(source.DirectorateCode, '')
		and isnull(target.ReferralSpecialtyTypeCode, '') = isnull(source.ReferralSpecialtyTypeCode, '')
		and isnull(target.LastDNAorPatientCancelledDate, getdate()) = isnull(source.LastDNAorPatientCancelledDate, getdate())
		and isnull(target.ReferredByCode, '') = isnull(source.ReferredByCode, '')
		and isnull(target.ReferrerCode, '') = isnull(source.ReferrerCode, '')
		and isnull(target.AppointmentOutcomeCode, '') = isnull(source.AppointmentOutcomeCode, '')
		and isnull(target.MedicalStaffTypeCode, '') = isnull(source.MedicalStaffTypeCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		and isnull(target.TreatmentFunctionCode, '') = isnull(source.TreatmentFunctionCode, '')
		and isnull(target.CommissioningSerialNo, '') = isnull(source.CommissioningSerialNo, '')
		and isnull(target.DerivedFirstAttendanceFlag, '') = isnull(source.DerivedFirstAttendanceFlag, '')
		and isnull(target.AttendanceIdentifier, '') = isnull(source.AttendanceIdentifier, '')
		and isnull(target.RegisteredGpAtAppointmentCode, '') = isnull(source.RegisteredGpAtAppointmentCode, '')
		and isnull(target.RegisteredGpPracticeAtAppointmentCode, '') = isnull(source.RegisteredGpPracticeAtAppointmentCode, '')
		and isnull(target.ReferredByConsultantCode, '') = isnull(source.ReferredByConsultantCode, '')
		and isnull(target.ReferredByGpCode, '') = isnull(source.ReferredByGpCode, '')
		and isnull(target.ReferredByGdpCode, '') = isnull(source.ReferredByGdpCode, '')
		and isnull(target.CCGCode, '') =  isnull(source.CCGCode, '')
		and isnull(target.EpisodicPostcode, '') = isnull(source.EpisodicPostCode, '')
		and isnull(target.GpCodeAtAppointment, '') = isnull(source.GpCodeAtAppointment, '')
		and isnull(target.GpPracticeCodeAtAppointment, '') = isnull(source.GpPracticeCodeAtAppointment, '')
		and isnull(target.PostcodeAtAppointment, '') = isnull(source.PostcodeAtAppointment, '')
		and isnull(target.AppointmentResidenceCCGCode, '') = isnull(source.AppointmentResidenceCCGCode, '')
		)
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceEncounterNo = source.SourceEncounterNo
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.NHSNumber = source.NHSNumber
			,target.NHSNumberStatusCode = source.NHSNumberStatusCode
			,target.DistrictNo = source.DistrictNo
			,target.Postcode = source.Postcode
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.DHACode = source.DHACode
			,target.EthnicOriginCode = source.EthnicOriginCode
			,target.MaritalStatusCode = source.MaritalStatusCode
			,target.ReligionCode = source.ReligionCode
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredGpPracticeCode = source.RegisteredGpPracticeCode
			,target.SiteCode = source.SiteCode
			,target.AppointmentDate = source.AppointmentDate
			,target.AppointmentTime = source.AppointmentTime
			,target.ClinicCode = source.ClinicCode
			,target.AdminCategoryCode = source.AdminCategoryCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ReasonForReferralCode = source.ReasonForReferralCode
			,target.PriorityCode = source.PriorityCode
			,target.FirstAttendanceFlag = source.FirstAttendanceFlag
			,target.DNACode = source.DNACode
			,target.AppointmentStatusCode = source.AppointmentStatusCode
			,target.CancelledByCode = source.CancelledByCode
			,target.TransportRequiredFlag = source.TransportRequiredFlag
			,target.AttendanceOutcomeCode = source.AttendanceOutcomeCode
			,target.AppointmentTypeCode = source.AppointmentTypeCode
			,target.DisposalCode = source.DisposalCode
			,target.ConsultantCode = source.ConsultantCode
			,target.SpecialtyCode = source.SpecialtyCode
			,target.ReferralConsultantCode = source.ReferralConsultantCode
			,target.ReferralSpecialtyCode = source.ReferralSpecialtyCode
			,target.BookingTypeCode = source.BookingTypeCode
			,target.CasenoteNo = source.CasenoteNo
			,target.AppointmentCreateDate = source.AppointmentCreateDate
			,target.EpisodicGpCode = source.EpisodicGpCode
			,target.EpisodicGpPracticeCode = source.EpisodicGpPracticeCode
			,target.DoctorCode = source.DoctorCode
			,target.PrimaryDiagnosisCode = source.PrimaryDiagnosisCode
			,target.SubsidiaryDiagnosisCode = source.SubsidiaryDiagnosisCode
			,target.SecondaryDiagnosisCode1 = source.SecondaryDiagnosisCode1
			,target.SecondaryDiagnosisCode2 = source.SecondaryDiagnosisCode2
			,target.SecondaryDiagnosisCode3 = source.SecondaryDiagnosisCode3
			,target.SecondaryDiagnosisCode4 = source.SecondaryDiagnosisCode4
			,target.SecondaryDiagnosisCode5 = source.SecondaryDiagnosisCode5
			,target.SecondaryDiagnosisCode6 = source.SecondaryDiagnosisCode6
			,target.SecondaryDiagnosisCode7 = source.SecondaryDiagnosisCode7
			,target.SecondaryDiagnosisCode8 = source.SecondaryDiagnosisCode8
			,target.SecondaryDiagnosisCode9 = source.SecondaryDiagnosisCode9
			,target.SecondaryDiagnosisCode10 = source.SecondaryDiagnosisCode10
			,target.SecondaryDiagnosisCode11 = source.SecondaryDiagnosisCode11
			,target.SecondaryDiagnosisCode12 = source.SecondaryDiagnosisCode12
			,target.PrimaryOperationCode = source.PrimaryOperationCode
			,target.PrimaryProcedureDate = source.PrimaryProcedureDate
			,target.SecondaryProcedureCode1 = source.SecondaryProcedureCode1
			,target.SecondaryProcedureDate1 = source.SecondaryProcedureDate1
			,target.SecondaryProcedureCode2 = source.SecondaryProcedureCode2
			,target.SecondaryProcedureDate2 = source.SecondaryProcedureDate2
			,target.SecondaryProcedureCode3 = source.SecondaryProcedureCode3
			,target.SecondaryProcedureDate3 = source.SecondaryProcedureDate3
			,target.SecondaryProcedureCode4 = source.SecondaryProcedureCode4
			,target.SecondaryProcedureDate4 = source.SecondaryProcedureDate4
			,target.SecondaryProcedureCode5 = source.SecondaryProcedureCode5
			,target.SecondaryProcedureDate5 = source.SecondaryProcedureDate5
			,target.SecondaryProcedureCode6 = source.SecondaryProcedureCode6
			,target.SecondaryProcedureDate6 = source.SecondaryProcedureDate6
			,target.SecondaryProcedureCode7 = source.SecondaryProcedureCode7
			,target.SecondaryProcedureDate7 = source.SecondaryProcedureDate7
			,target.SecondaryProcedureCode8 = source.SecondaryProcedureCode8
			,target.SecondaryProcedureDate8 = source.SecondaryProcedureDate8
			,target.SecondaryProcedureCode9 = source.SecondaryProcedureCode9
			,target.SecondaryProcedureDate9 = source.SecondaryProcedureDate9
			,target.SecondaryProcedureCode10 = source.SecondaryProcedureCode10
			,target.SecondaryProcedureDate10 = source.SecondaryProcedureDate10
			,target.SecondaryProcedureCode11 = source.SecondaryProcedureCode11
			,target.SecondaryProcedureDate11 = source.SecondaryProcedureDate11
			--,target.PurchaserCode = source.PurchaserCode
			,target.ProviderCode = source.ProviderCode
			,target.ContractSerialNo = source.ContractSerialNo
			,target.ReferralDate = source.ReferralDate
			,target.RTTPathwayID = source.RTTPathwayID
			,target.RTTPathwayCondition = source.RTTPathwayCondition
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.RTTSpecialtyCode = source.RTTSpecialtyCode
			,target.RTTCurrentProviderCode = source.RTTCurrentProviderCode
			,target.RTTCurrentStatusCode = source.RTTCurrentStatusCode
			,target.RTTCurrentStatusDate = source.RTTCurrentStatusDate
			,target.RTTCurrentPrivatePatientFlag = source.RTTCurrentPrivatePatientFlag
			,target.RTTOverseasStatusFlag = source.RTTOverseasStatusFlag
			,target.RTTPeriodStatusCode = source.RTTPeriodStatusCode
			,target.AppointmentCategoryCode = source.AppointmentCategoryCode
			,target.AppointmentCreatedBy = source.AppointmentCreatedBy
			,target.AppointmentCancelDate = source.AppointmentCancelDate
			,target.LastRevisedDate = source.LastRevisedDate
			,target.LastRevisedBy = source.LastRevisedBy
			,target.OverseasStatusFlag = source.OverseasStatusFlag
			,target.PatientChoiceCode = source.PatientChoiceCode
			,target.ScheduledCancelReasonCode = source.ScheduledCancelReasonCode
			,target.PatientCancelReason = source.PatientCancelReason
			,target.DischargeDate = source.DischargeDate
			,target.QM08StartWaitDate = source.QM08StartWaitDate
			,target.QM08EndWaitDate = source.QM08EndWaitDate
			,target.DestinationSiteCode = source.DestinationSiteCode
			,target.EBookingReferenceNo = source.EBookingReferenceNo
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()
			,target.LocalAdminCategoryCode = source.LocalAdminCategoryCode
			,target.PCTCode = source.PCTCode
			,target.LocalityCode = source.LocalityCode
			,target.AgeCode = source.AgeCode
			,target.HRGCode = source.HRGCode
			,target.Cases = source.Cases
			,target.LengthOfWait = source.LengthOfWait
			,target.IsWardAttender = source.IsWardAttender
			,target.RTTActivity = source.RTTActivity
			,target.RTTBreachStatusCode = source.RTTBreachStatusCode
			,target.ClockStartDate = source.ClockStartDate
			,target.RTTBreachDate = source.RTTBreachDate
			,target.RTTTreated = source.RTTTreated
			--,target.DirectorateCode = source.DirectorateCode
			,target.ReferralSpecialtyTypeCode = source.ReferralSpecialtyTypeCode
			,target.LastDNAorPatientCancelledDate = source.LastDNAorPatientCancelledDate
			,target.ReferredByCode = source.ReferredByCode
			,target.ReferrerCode = source.ReferrerCode
			,target.AppointmentOutcomeCode = source.AppointmentOutcomeCode
			,target.MedicalStaffTypeCode = source.MedicalStaffTypeCode
			,target.ContextCode = source.ContextCode
			,target.TreatmentFunctionCode = source.TreatmentFunctionCode
			,target.CommissioningSerialNo = source.CommissioningSerialNo
			,target.DerivedFirstAttendanceFlag = source.DerivedFirstAttendanceFlag
			,target.AttendanceIdentifier = source.AttendanceIdentifier
			,target.RegisteredGpAtAppointmentCode = source.RegisteredGpAtAppointmentCode
			,target.RegisteredGpPracticeAtAppointmentCode = source.RegisteredGpPracticeAtAppointmentCode
			,target.ReferredByConsultantCode = source.ReferredByConsultantCode
			,target.ReferredByGpCode = source.ReferredByGpCode
			,target.ReferredByGdpCode = source.ReferredByGdpCode
			,target.CCGCode =  source.CCGCode
			,target.EpisodicPostcode = source.EpisodicPostCode
			,target.GpCodeAtAppointment = source.GpCodeAtAppointment
			,target.GpPracticeCodeAtAppointment = source.GpPracticeCodeAtAppointment
			,target.PostcodeAtAppointment = source.PostcodeAtAppointment	
			,target.AppointmentResidenceCCGCode = source.AppointmentResidenceCCGCode	

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats



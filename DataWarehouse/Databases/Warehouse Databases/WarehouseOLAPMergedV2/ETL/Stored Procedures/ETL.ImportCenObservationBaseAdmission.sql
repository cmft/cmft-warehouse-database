﻿create procedure [ETL].[ImportCenObservationBaseAdmission] as


--import the data
exec ETL.BuildCenObservationBaseAdmission


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[Observation].[BaseAdmission]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildObservationBaseAdmissionReference 'CEN||PTRACK'


﻿

CREATE procedure [ETL].[BuildFinanceBaseExpenditure]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));


merge Finance.BaseExpenditure target
using 
	(
	select
		ExpenditureRecno
		,CensusDate
		,Division
		,Budget
		,AnnualBudget
		,Actual
		,InterfaceCode
		,ContextCode
		,Created
		,Updated
		,ByWhom	
	from
		ETL.TLoadFinanceBaseExpenditure
	) source
	on	source.ExpenditureRecno = target.ExpenditureRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CMFT||ORCL'
	then delete

	when not matched 
	then
		insert
			(
			ExpenditureRecno
			,CensusDate
			,Division
			,Budget
			,AnnualBudget
			,Actual
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom	
			)

		values
			(
			source.ExpenditureRecno
			,source.CensusDate
			,source.Division
			,source.Budget
			,source.AnnualBudget
			,source.Actual
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.CensusDate, getdate()) = isnull(source.CensusDate, getdate())
		and isnull(target.Division, '') = isnull(source.Division, '')
		and isnull(target.Budget, 0) = isnull(source.Budget, 0)
		and isnull(target.AnnualBudget, 0) = isnull(source.AnnualBudget, 0)
		and isnull(target.Actual, 0) = isnull(source.Actual, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	
	then 
		update 
		set
			target.CensusDate = source.CensusDate
			,target.Division = source.Division
			,target.Budget = source.Budget
			,target.AnnualBudget = source.AnnualBudget
			,target.Actual = source.Actual
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime



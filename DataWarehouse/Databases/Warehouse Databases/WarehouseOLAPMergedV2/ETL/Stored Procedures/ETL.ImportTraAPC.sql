﻿

CREATE procedure [ETL].[ImportTraAPC] as

exec ETL.ImportTraAPCBaseEncounter
exec ETL.ImportTraAPCBaseWardStay;
--exec ETL.ImportTraBaseConsultant;
exec ETL.ImportTraAPCBaseDiagnosis;
exec ETL.ImportTraAPCBaseOperation;

--exec ETL.BuildAPCBaseEncounterProcedureDetail 'CEN||PAS', 'CEN||ORMIS'

exec ETL.BuildWHConsultantBase
exec ETL.BuildAPCWardBase


exec Allocation.AllocateAPCExclusion
exec APC.BuildSpell

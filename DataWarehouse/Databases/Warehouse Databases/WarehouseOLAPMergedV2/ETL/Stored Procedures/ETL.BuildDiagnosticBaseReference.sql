﻿
CREATE procedure [ETL].[BuildDiagnosticBaseReference] as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)


truncate table Diagnostic.BaseReference


insert
into
	Diagnostic.BaseReference
(
	 MergeRecno
	,ContextID
	,ExamID
	,EventDateID
	,SpecialtyID
	,ProfessionalCarerID
	,SiteID
	,SexID
)
select
	 Base.MergeRecno
	,Diagnostic.ContextID

	,ExamID =
		coalesce(
			 Diagnostic.ExamID
			,NullExam.SourceExamID
		)

	,Diagnostic.EventDateID

	,Diagnostic.SpecialtyID

	,ProfessionalCarerID =
		coalesce(
			 ProfessionalCarer.SourceProfessionalCarerID
			,-1
		)

	,Diagnostic.SiteID
	,Diagnostic.SexID

from
	(
	select
		 SourceMergeRecno = BaseReference.MergeRecno
		,BaseReference.ContextID
		,BaseReference.ExamID
		,BaseReference.EventDateID
		,DataSourceID =
			(
			select
				DataSource.DataSourceID
			from
				Diagnostic.DataSource
			where
				DataSource.DataSourceCode = 'RAD'
			)

		,BaseReference.SpecialtyID
		,BaseReference.SiteID
		,BaseReference.SexID

	from
		Diagnostic.BaseRadiologyReference BaseReference

	union all

	select
		 SourceMergeRecno = BaseReference.MergeEncounterRecno
		,BaseReference.ContextID
		,ExamID = null
		,BaseReference.EpisodeStartDateID
		,DataSourceID =
			(
			select
				DataSource.DataSourceID
			from
				Diagnostic.DataSource
			where
				DataSource.DataSourceCode = 'APC'
			)

		,BaseReference.SpecialtyID
		,BaseReference.StartSiteID
		,BaseReference.SexID
	from
		APC.BaseEncounterReference BaseReference

	union all

	select
		 SourceMergeRecno = BaseReference.MergeEncounterRecno
		,BaseReference.ContextID
		,ExamID = null
		,BaseReference.AppointmentDateID
		,DataSourceID =
			(
			select
				DataSource.DataSourceID
			from
				Diagnostic.DataSource
			where
				DataSource.DataSourceCode = 'OP'
			)

		,BaseReference.SpecialtyID
		,BaseReference.SiteID
		,BaseReference.SexID
	from
		OP.BaseEncounterReference BaseReference

	) Diagnostic

inner join WH.Context
on	Context.ContextID = Diagnostic.ContextID

inner join Diagnostic.Base
on	Base.SourceMergeRecno = Diagnostic.SourceMergeRecno
and	Base.DataSourceID = Diagnostic.DataSourceID
and	Base.ContextCode = Context.ContextCode

left join Diagnostic.Exam NullExam
on	NullExam.SourceContextCode = 'N/A'
and	NullExam.SourceExamCode = '-1'

left join WH.ProfessionalCarer ProfessionalCarer
on	ProfessionalCarer.SourceProfessionalCarerCode = coalesce(Base.ProfessionalCarerCode, '-1')
and	ProfessionalCarer.SourceContextCode = Base.ContextCode

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats





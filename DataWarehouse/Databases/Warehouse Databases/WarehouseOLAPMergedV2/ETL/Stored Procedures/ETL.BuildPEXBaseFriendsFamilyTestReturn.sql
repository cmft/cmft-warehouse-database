﻿

CREATE procedure [ETL].[BuildPEXBaseFriendsFamilyTestReturn]
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)
	--,@ContextCode varchar(100) = 'CMFT||ULYSS'

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge PEX.BaseFriendsFamilyTestReturn target
using 
	(
	select
		[FFTRecno]
		,[Return]
		,CensusDate
		,HospitalSiteCode
		,WardCode
		,[1ExtremelyLikely]
		,[2Likely]
		,[3NeitherLikelyNorUnlikely]
		,[4Unlikely]
		,[5ExtremelyUnlikely]
		,[6DontKnow]
		,[Responses]
		,[EligibleResponders]
		,[InterfaceCode]
		,[Created]
		,[Updated]
		,[ByWhom]
		,[ContextCode]

	from
		 [ETL].[TLoadPEXBaseFriendsFamilyTestReturn]

		) source
	on	source.FFTRecno = target.FFTRecno
	and source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CMFT||FFTRTN'
	then delete

	when not matched 
	then
		insert
			(
			[FFTRecno]
			,[Return]
			,CensusDate
			,HospitalSiteCode
			,WardCode
			,[1ExtremelyLikely]
			,[2Likely]
			,[3NeitherLikelyNorUnlikely]
			,[4Unlikely]
			,[5ExtremelyUnlikely]
			,[6DontKnow]
			,[Responses]
			,[EligibleResponders]
			,[InterfaceCode]
			,[ContextCode]
			,[Created]
			,[Updated]
			,[ByWhom]
			)

		values
			(
			 source.[FFTRecno]
			,source.[Return]
			,source.CensusDate
			,source.HospitalSiteCode
			,source.WardCode
			,source.[1ExtremelyLikely]
			,source.[2Likely]
			,source.[3NeitherLikelyNorUnlikely]
			,source.[4Unlikely]
			,source.[5ExtremelyUnlikely]
			,source.[6DontKnow]
			,source.[Responses]
			,source.[EligibleResponders]
			,source.[InterfaceCode]
			,source.[ContextCode]
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.FFTRecno, 0) = isnull(source.FFTRecno, 0)
		and isnull(target.[Return], '') = isnull(source.[Return], '')
		and isnull(target.CensusDate, getdate()) = isnull(source.CensusDate, getdate())
		and isnull(target.HospitalSiteCode, '') = isnull(source.HospitalSiteCode, '')
		and isnull(target.WardCode, '') = isnull(source.WardCode, '')
		and isnull(target.[1ExtremelyLikely], '') = isnull(source.[1ExtremelyLikely], '')
		and isnull(target.[2Likely], '') = isnull(source.[2Likely], '')
		and isnull(target.[3NeitherLikelyNorUnlikely], '') = isnull(source.[3NeitherLikelyNorUnlikely], '')
		and isnull(target.[4Unlikely], '') = isnull(source.[4Unlikely], '')
		and isnull(target.[5ExtremelyUnlikely], '') = isnull(source.[5ExtremelyUnlikely], '')
		and isnull(target.[6DontKnow], '') = isnull(source.[6DontKnow], '')
		and isnull(target.Responses, '') = isnull(source.Responses, '')
		and isnull(target.EligibleResponders, '') = isnull(source.EligibleResponders, '')
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	
	then 
		update 
		set
			target.FFTRecno = source.FFTRecno
			,target.[Return] = source.[Return]
			,target.CensusDate = source.CensusDate	
			,target.HospitalSiteCode = source.HospitalSiteCode
			,target.WardCode = source.WardCode
			,target.[1ExtremelyLikely] = source.[1ExtremelyLikely]
			,target.[2Likely] = source.[2Likely]
			,target.[3NeitherLikelyNorUnlikely] = source.[3NeitherLikelyNorUnlikely]
			,target.[4Unlikely] = source.[4Unlikely]
			,target.[5ExtremelyUnlikely] = source.[5ExtremelyUnlikely]
			,target.[6DontKnow] = source.[6DontKnow]
			,target.Responses = source.Responses
			,target.EligibleResponders = source.EligibleResponders
			,target.InterfaceCode = source.InterfaceCode
			,target.ContextCode = source.ContextCode
			,target.Created = source.Created
			,target.Updated = getdate()
			,target.ByWhom = suser_name()


output
	$action into @MergeSummary;

select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].Utility.WriteAuditLogEvent @ProcedureName, @Stats, @StartTime




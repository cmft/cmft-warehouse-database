﻿CREATE procedure [ETL].[ImportCenSCRBaseReferral] as

--import the data
exec ETL.BuildCenSCRBaseReferral


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[SCR].[BaseReferral]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildSCRBaseReferralReference 'CEN||SCR'


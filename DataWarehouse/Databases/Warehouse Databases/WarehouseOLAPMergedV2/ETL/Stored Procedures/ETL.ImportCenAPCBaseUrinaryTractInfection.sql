﻿CREATE procedure [ETL].[ImportCenAPCBaseUrinaryTractInfection] as

--import the data
exec ETL.BuildCenAPCBaseUrinaryTractInfection


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[APC].[BaseUrinaryTractInfection]'
	,@NewRows int


--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output


if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildAPCBaseUrinaryTractInfectionReference 'CEN||BEDMAN'


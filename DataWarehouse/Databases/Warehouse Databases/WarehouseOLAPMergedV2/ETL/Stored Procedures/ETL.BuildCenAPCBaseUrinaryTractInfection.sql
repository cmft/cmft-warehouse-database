﻿
CREATE PROCEDURE [ETL].[BuildCenAPCBaseUrinaryTractInfection]


as

set dateformat dmy

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)



declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

merge
	APC.BaseUrinaryTractInfection target
using
	(
	select
		UrinaryTractInfectionRecno
		,SourceUniqueID
		,SourcePatientNo
		,SourceSpellNo
		,SymptomStartDate
		,SymptomStartTime
		,SymptomEndDate
		,SymptomEndTime
		,UrinaryTestRequestedDate
		,UrinaryTestRequestedTime
		,UrinaryTestReceivedDate
		,UrinaryTestReceivedTime
		,UrinaryTestPositive
		,TreatmentStartDate
		,TreatmentStartTime
		,TreatmentEndDate
		,TreatmentEndTime
		,CatheterInsertedDate
		,CatheterInsertedTime
		,CatheterRemovedDate
		,CatheterRemovedTime
		,CatheterTypeID
		,WardCode
		,InterfaceCode
		,ContextCode
	from
		ETL.TLoadCenAPCBaseUrinaryTractInfection
	) source
	on	source.UrinaryTractInfectionRecno = target.UrinaryTractInfectionRecno
	and	source.ContextCode = target.ContextCode

	when not matched by source
	and	target.ContextCode = 'CEN||BEDMAN'

	then delete

	when not matched
	then
		insert
			(
			UrinaryTractInfectionRecno
			,SourceUniqueID
			,SourcePatientNo
			,SourceSpellNo
			,SymptomStartDate
			,SymptomStartTime
			,SymptomEndDate
			,SymptomEndTime
			,UrinaryTestRequestedDate
			,UrinaryTestRequestedTime
			,UrinaryTestReceivedDate
			,UrinaryTestReceivedTime
			,UrinaryTestPositive
			,TreatmentStartDate
			,TreatmentStartTime
			,TreatmentEndDate
			,TreatmentEndTime
			,CatheterInsertedDate
			,CatheterInsertedTime
			,CatheterRemovedDate
			,CatheterRemovedTime
			,CatheterTypeID
			,WardCode
			,InterfaceCode
			,ContextCode
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			source.UrinaryTractInfectionRecno
			,source.SourceUniqueID
			,source.SourcePatientNo
			,source.SourceSpellNo
			,source.SymptomStartDate
			,source.SymptomStartTime
			,source.SymptomEndDate
			,source.SymptomEndTime
			,source.UrinaryTestRequestedDate
			,source.UrinaryTestRequestedTime
			,source.UrinaryTestReceivedDate
			,source.UrinaryTestReceivedTime
			,source.UrinaryTestPositive
			,source.TreatmentStartDate
			,source.TreatmentStartTime
			,source.TreatmentEndDate
			,source.TreatmentEndTime
			,source.CatheterInsertedDate
			,source.CatheterInsertedTime
			,source.CatheterRemovedDate
			,source.CatheterRemovedTime
			,source.CatheterTypeID
			,source.WardCode
			,source.InterfaceCode
			,source.ContextCode
			,getdate()
			,getdate()
			,suser_name()
			)

	when matched
	and not
		(
			isnull(target.UrinaryTractInfectionRecno, 0) = isnull(source.UrinaryTractInfectionRecno, 0)
		and	isnull(target.SourceUniqueID, 0) = isnull(source.SourceUniqueID, 0)
		and isnull(target.SourcePatientNo, '') = isnull(source.SourcePatientNo, '')
		and isnull(target.SourceSpellNo, '') = isnull(source.SourceSpellNo, '')
		and isnull(target.SymptomStartDate, getdate()) = isnull(source.SymptomStartDate, getdate())
		and isnull(target.SymptomStartTime, getdate()) = isnull(source.SymptomStartTime, getdate())
		and isnull(target.SymptomEndDate, getdate()) = isnull(source.SymptomEndDate, getdate())
		and isnull(target.SymptomEndTime, getdate()) = isnull(source.SymptomEndTime, getdate())
		and isnull(target.UrinaryTestRequestedDate, getdate()) = isnull(source.UrinaryTestRequestedDate, getdate())
		and isnull(target.UrinaryTestRequestedTime, getdate()) = isnull(source.UrinaryTestRequestedTime, getdate())
		and isnull(target.UrinaryTestReceivedDate, getdate()) = isnull(source.UrinaryTestReceivedDate, getdate())
		and isnull(target.UrinaryTestReceivedTime, getdate()) = isnull(source.UrinaryTestReceivedTime, getdate())
		and isnull(target.UrinaryTestPositive, 0) = isnull(source.UrinaryTestPositive, 0)
		and isnull(target.TreatmentStartDate, getdate()) = isnull(source.TreatmentStartDate, getdate())
		and isnull(target.TreatmentStartTime, getdate()) = isnull(source.TreatmentStartTime, getdate())
		and isnull(target.TreatmentEndDate, getdate()) = isnull(source.TreatmentEndDate, getdate())
		and isnull(target.TreatmentEndTime, getdate()) = isnull(source.TreatmentEndTime, getdate())
		and isnull(target.CatheterInsertedDate, getdate()) = isnull(source.CatheterInsertedDate, getdate())
		and isnull(target.CatheterInsertedTime, getdate()) = isnull(source.CatheterInsertedTime, getdate())
		and isnull(target.CatheterInsertedTime, getdate()) = isnull(source.CatheterInsertedTime, getdate())
		and isnull(target.CatheterRemovedTime, getdate()) = isnull(source.CatheterRemovedTime, getdate())
		and isnull(target.CatheterTypeID, 0) = isnull(source.CatheterTypeID, 0)
		and isnull(target.WardCode, 0) = isnull(source.WardCode, 0)
		and isnull(target.InterfaceCode, '') = isnull(source.InterfaceCode, '')
		and isnull(target.ContextCode, '') = isnull(source.ContextCode, '')
		)
	then
		update
		set
			target.UrinaryTractInfectionRecno = source.UrinaryTractInfectionRecno
			,target.SourceUniqueID = source.SourceUniqueID
			,target.SourcePatientNo = source.SourcePatientNo
			,target.SourceSpellNo = source.SourceSpellNo
			,target.SymptomStartDate = source.SymptomStartDate
			,target.SymptomStartTime = source.SymptomStartTime
			,target.SymptomEndDate = source.SymptomEndDate
			,target.SymptomEndTime = source.SymptomEndTime	
			,target.UrinaryTestRequestedDate = source.UrinaryTestRequestedDate
			,target.UrinaryTestRequestedTime = source.UrinaryTestRequestedTime
			,target.UrinaryTestReceivedDate = source.UrinaryTestReceivedDate
			,target.UrinaryTestReceivedTime = source.UrinaryTestReceivedTime
			,target.UrinaryTestPositive = source.UrinaryTestPositive
			,target.TreatmentStartDate = source.TreatmentStartDate
			,target.TreatmentStartTime = source.TreatmentStartTime
			,target.TreatmentEndDate = source.TreatmentEndDate	
			,target.TreatmentEndTime = source.TreatmentEndTime
			,target.CatheterInsertedDate = source.CatheterInsertedDate
			,target.CatheterInsertedTime = source.CatheterInsertedTime
			,target.CatheterRemovedDate = source.CatheterRemovedDate
			,target.CatheterRemovedTime = source.CatheterRemovedTime
			,target.CatheterTypeID = source.CatheterTypeID
			,target.WardCode = source.WardCode
			,target.InterfaceCode = source.InterfaceCode
			,target.Updated = getdate()
			,target.ByWhom = suser_name()

output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats
	,@StartTime
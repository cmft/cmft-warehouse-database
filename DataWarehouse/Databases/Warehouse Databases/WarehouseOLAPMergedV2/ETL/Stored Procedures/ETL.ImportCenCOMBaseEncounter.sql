﻿CREATE procedure [ETL].[ImportCenCOMBaseEncounter] as

--import the data
exec ETL.BuildCenCOMBaseEncounter


declare
	 @ActivityTable varchar(255) = '[' + @@SERVERNAME + '].[' + DB_NAME() + '].[COM].[BaseEncounter]'
	,@NewRows int

--generate reference data
exec ETL.BuildReferenceDataMap @ActivityTable, @NewRows output

if @NewRows > 0
	exec WH.BuildMember


--Build the BaseReference table
exec ETL.BuildCOMBaseEncounterReference 'CEN||IPM'

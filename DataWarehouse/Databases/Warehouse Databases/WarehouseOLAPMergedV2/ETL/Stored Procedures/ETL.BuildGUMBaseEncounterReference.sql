﻿

CREATE procedure [ETL].[BuildGUMBaseEncounterReference]
	@ContextCode varchar(20)
as

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE(Action nvarchar(10));

declare
	@ContextID int =
		(
		select
			Context.ContextID
		from
			WH.Context
		where
			Context.ContextCode = @ContextCode
		)

declare
	@LastUpdated datetime =
		coalesce(
			(
			select
				MAX(Updated)
			from
				GUM.BaseEncounterReference
			where
				BaseEncounterReference.ContextID = @ContextID
			)
			,'1 jan 1900'
		)


--more efficient to delete here than in the merge
delete
from
	GUM.BaseEncounterReference
where
	not exists
	(
	select
		1
	from
		GUM.BaseEncounter
	where
		BaseEncounter.MergeEncounterRecno = BaseEncounterReference.MergeEncounterRecno
	)
and	BaseEncounterReference.ContextID = @ContextID

select
	@deleted = @@ROWCOUNT


merge
	GUM.BaseEncounterReference target
using
	(
	select
		BaseEncounter.MergeEncounterRecno
		,ContextID = Context.ContextID
		,AppointmentDateID =
			coalesce(
				 AppointmentDate.DateID

				,case
				when BaseEncounter.AppointmentDate is null
				then NullDate.DateID

				when BaseEncounter.AppointmentDate < CalendarStartDate.DateValue
				then PreviousDate.DateID

				else FutureDate.DateID
				end
			)
		
		,ClinicID = Clinic.SourceClinicID	
		,AppointmentTypeID = AppointmentType.SourceAppointmentTypeID		
		,AttendanceStatusID = AttendanceStatus.SourceAttendanceStatusID
		,BaseEncounter.Created
		,BaseEncounter.Updated
		,BaseEncounter.ByWhom
	from
		GUM.BaseEncounter

	inner join WH.Context
	on	Context.ContextCode = BaseEncounter.ContextCode

	inner join OP.Clinic
	on	Clinic.SourceClinicCode = coalesce(BaseEncounter.ClinicID, '-1')
	and	Clinic.SourceContextCode = BaseEncounter.ContextCode

	inner join OP.AppointmentType
	on	AppointmentType.SourceAppointmentTypeCode = coalesce(BaseEncounter.AppointmentTypeID, '-1')
	and	AppointmentType.SourceContextCode = BaseEncounter.ContextCode

	inner join OP.AttendanceStatus
	on	AttendanceStatus.SourceAttendanceStatusCode = coalesce(BaseEncounter.AttendanceStatusID, '-1')
	and	AttendanceStatus.SourceContextCode = BaseEncounter.ContextCode

	left join WH.Calendar AppointmentDate
	on	AppointmentDate.TheDate = BaseEncounter.AppointmentDate

	left join WH.Calendar FutureDate
	on	FutureDate.TheDate = '31 dec 9999'

	left join WH.Calendar PreviousDate
	on	PreviousDate.TheDate = '2 Jan 1900'

	left join Utility.Parameter CalendarStartDate
	on	CalendarStartDate.Parameter = 'CALENDARSTARTDATE'

	left join WH.Calendar NullDate
	on	NullDate.TheDate = '1 jan 1900'

	where
		BaseEncounter.Updated > @LastUpdated
	and	BaseEncounter.ContextCode = @ContextCode

	) source
	on	source.MergeEncounterRecno = target.MergeEncounterRecno

	when not matched
	then
		insert
			(
			MergeEncounterRecno
			,ContextID
			,AppointmentDateID
			,ClinicID
			,AppointmentTypeID
			,AttendanceStatusID
			,Created
			,Updated
			,ByWhom
			)
		values
			(
			 source.MergeEncounterRecno
			,source.ContextID
			,source.AppointmentDateID
			,source.ClinicID
			,source.AppointmentTypeID
			,source.AttendanceStatusID
			,source.Created
			,source.Updated
			,source.ByWhom
			)

	when matched
	and not
		(
			target.Updated = source.Updated
		)
	then
		update
		set
			 target.MergeEncounterRecno = source.MergeEncounterRecno
			,target.ContextID = source.ContextID
			,target.AppointmentDateID = source.AppointmentDateID
			,target.ClinicID = source.ClinicID
			,target.AppointmentTypeID = source.AppointmentTypeID
			,target.AttendanceStatusID = source.AttendanceStatusID
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
output
	$action into @MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary

select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats






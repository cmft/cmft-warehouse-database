﻿CREATE procedure [ETL].[LoadAEBaseEncounter]
as

-------------------------------------------------
--When		Who	What
--20141215	PDO	Created procedure
-------------------------------------------------

declare
	 @StartTime datetime = getdate()
	,@ProcedureName varchar(255) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)
	,@Elapsed int
	,@Stats varchar(max)

declare
	 @deleted int
	,@inserted int
	,@updated int

declare
	@MergeSummary TABLE
	(
	 MergeRecno int
	,Action nvarchar(10)
	);


merge
	AE.BaseEncounter target
using
	(
	select
		 EncounterRecno
		,SourceUniqueID
		,UniqueBookingReferenceNo
		,PathwayId
		,PathwayIdIssuerCode
		,RTTStatusCode
		,RTTStartDate
		,RTTEndDate
		,DistrictNo
		,TrustNo
		,CasenoteNo
		,DistrictNoOrganisationCode
		,NHSNumber
		,NHSNumberStatusCode
		,PatientTitle
		,PatientForename
		,PatientSurname
		,PatientAddress1
		,PatientAddress2
		,PatientAddress3
		,PatientAddress4
		,Postcode
		,DateOfBirth
		,DateOfDeath
		,SexCode
		,CarerSupportIndicator
		,RegisteredGpCode
		,RegisteredPracticeCode
		,AttendanceNumber
		,ArrivalModeCode
		,AttendanceCategoryCode
		,AttendanceDisposalCode
		,IncidentLocationTypeCode
		,PatientGroupCode
		,SourceOfReferralCode
		,ArrivalDate
		,ArrivalTime
		,AgeOnArrival
		,InitialAssessmentTime
		,SeenForTreatmentTime
		,AttendanceConclusionTime
		,DepartureTime
		,CommissioningSerialNo
		,NHSServiceAgreementLineNo
		,ProviderReferenceNo
		,CommissionerReferenceNo
		,ProviderCode
		,CommissionerCode
		,StaffMemberCode
		,InvestigationCodeFirst
		,InvestigationCodeSecond
		,DiagnosisCodeFirst
		,DiagnosisCodeSecond
		,TreatmentCodeFirst
		,TreatmentCodeSecond
		,PASHRGCode
		,HRGVersionCode
		,PASDGVPCode
		,SiteCode
		,Created
		,Updated
		,ByWhom = isnull(ByWhom, suser_name())
		,InterfaceCode
		,PCTCode
		,ResidencePCTCode
		,InvestigationCodeList
		,TriageCategoryCode
		,PresentingProblem
		,PresentingProblemCode
		,ToXrayTime
		,FromXrayTime
		,ToSpecialtyTime
		,SeenBySpecialtyTime
		,EthnicCategoryCode
		,ReferredToSpecialtyCode
		,DecisionToAdmitTime
		,DischargeDestinationCode
		,RegisteredTime
		,TransportRequestTime
		,TransportAvailableTime
		,AscribeLeftDeptTime
		,CDULeftDepartmentTime
		,PCDULeftDepartmentTime
		,TreatmentDateFirst
		,TreatmentDateSecond
		,SourceAttendanceDisposalCode
		,AmbulanceArrivalTime
		,AmbulanceCrewPRF
		,UnplannedReattend7Day
		,ArrivalTimeAdjusted
		,ClinicalAssessmentTime
		,LevelOfCareCode
		,EncounterBreachStatusCode
		,EncounterStartTimeOfDay
		,EncounterEndTimeOfDay
		,EncounterStartDate
		,EncounterEndDate
		,EncounterDurationMinutes
		,AgeCode
		,LeftWithoutBeingSeenCases
		,HRGCode
		,Reportable
		,CareGroup
		,EPMINo
		,DepartmentTypeCode
		,DepartureTimeAdjusted
		,CarePathwayAttendanceConclusionTime
		,CarePathwayDepartureTime
		,ContextCode
		,GpCodeAtAttendance
		,GpPracticeCodeAtAttendance
		,PostcodeAtAttendance
		,CCGCode
		,AlcoholLocation
		,AlcoholInPast12Hours
		,ReligionCode
		,AttendanceResidenceCCGCode
		,ServiceID
		,Action
	from
		ETL.TLoadAEBaseEncounter Encounter
	) source
	on	source.ContextCode = target.ContextCode
	and	source.EncounterRecno = target.EncounterRecno

	when matched
	and	source.Action = 'DELETE'
	then delete

	when not matched
	then
		insert
			(
			 EncounterRecno
			,SourceUniqueID
			,UniqueBookingReferenceNo
			,PathwayId
			,PathwayIdIssuerCode
			,RTTStatusCode
			,RTTStartDate
			,RTTEndDate
			,DistrictNo
			,TrustNo
			,CasenoteNo
			,DistrictNoOrganisationCode
			,NHSNumber
			,NHSNumberStatusCode
			,PatientTitle
			,PatientForename
			,PatientSurname
			,PatientAddress1
			,PatientAddress2
			,PatientAddress3
			,PatientAddress4
			,Postcode
			,DateOfBirth
			,DateOfDeath
			,SexCode
			,CarerSupportIndicator
			,RegisteredGpCode
			,RegisteredPracticeCode
			,AttendanceNumber
			,ArrivalModeCode
			,AttendanceCategoryCode
			,AttendanceDisposalCode
			,IncidentLocationTypeCode
			,PatientGroupCode
			,SourceOfReferralCode
			,ArrivalDate
			,ArrivalTime
			,AgeOnArrival
			,InitialAssessmentTime
			,SeenForTreatmentTime
			,AttendanceConclusionTime
			,DepartureTime
			,CommissioningSerialNo
			,NHSServiceAgreementLineNo
			,ProviderReferenceNo
			,CommissionerReferenceNo
			,ProviderCode
			,CommissionerCode
			,StaffMemberCode
			,InvestigationCodeFirst
			,InvestigationCodeSecond
			,DiagnosisCodeFirst
			,DiagnosisCodeSecond
			,TreatmentCodeFirst
			,TreatmentCodeSecond
			,PASHRGCode
			,HRGVersionCode
			,PASDGVPCode
			,SiteCode
			,Created
			,Updated
			,ByWhom
			,InterfaceCode
			,PCTCode
			,ResidencePCTCode
			,InvestigationCodeList
			,TriageCategoryCode
			,PresentingProblem
			,PresentingProblemCode
			,ToXrayTime
			,FromXrayTime
			,ToSpecialtyTime
			,SeenBySpecialtyTime
			,EthnicCategoryCode
			,ReferredToSpecialtyCode
			,DecisionToAdmitTime
			,DischargeDestinationCode
			,RegisteredTime
			,TransportRequestTime
			,TransportAvailableTime
			,AscribeLeftDeptTime
			,CDULeftDepartmentTime
			,PCDULeftDepartmentTime
			,TreatmentDateFirst
			,TreatmentDateSecond
			,SourceAttendanceDisposalCode
			,AmbulanceArrivalTime
			,AmbulanceCrewPRF
			,UnplannedReattend7Day
			,ArrivalTimeAdjusted
			,ClinicalAssessmentTime
			,LevelOfCareCode
			,EncounterBreachStatusCode
			,EncounterStartTimeOfDay
			,EncounterEndTimeOfDay
			,EncounterStartDate
			,EncounterEndDate
			,EncounterDurationMinutes
			,AgeCode
			,LeftWithoutBeingSeenCases
			,HRGCode
			,Reportable
			,CareGroup
			,EPMINo
			,DepartmentTypeCode
			,DepartureTimeAdjusted
			,CarePathwayAttendanceConclusionTime
			,CarePathwayDepartureTime
			,ContextCode
			,GpCodeAtAttendance
			,GpPracticeCodeAtAttendance
			,PostcodeAtAttendance
			,CCGCode
			,AlcoholLocation
			,AlcoholInPast12Hours
			,ReligionCode
			,AttendanceResidenceCCGCode
			,ServiceID
			)
		values
			(
			 source.EncounterRecno
			,source.SourceUniqueID
			,source.UniqueBookingReferenceNo
			,source.PathwayId
			,source.PathwayIdIssuerCode
			,source.RTTStatusCode
			,source.RTTStartDate
			,source.RTTEndDate
			,source.DistrictNo
			,source.TrustNo
			,source.CasenoteNo
			,source.DistrictNoOrganisationCode
			,source.NHSNumber
			,source.NHSNumberStatusCode
			,source.PatientTitle
			,source.PatientForename
			,source.PatientSurname
			,source.PatientAddress1
			,source.PatientAddress2
			,source.PatientAddress3
			,source.PatientAddress4
			,source.Postcode
			,source.DateOfBirth
			,source.DateOfDeath
			,source.SexCode
			,source.CarerSupportIndicator
			,source.RegisteredGpCode
			,source.RegisteredPracticeCode
			,source.AttendanceNumber
			,source.ArrivalModeCode
			,source.AttendanceCategoryCode
			,source.AttendanceDisposalCode
			,source.IncidentLocationTypeCode
			,source.PatientGroupCode
			,source.SourceOfReferralCode
			,source.ArrivalDate
			,source.ArrivalTime
			,source.AgeOnArrival
			,source.InitialAssessmentTime
			,source.SeenForTreatmentTime
			,source.AttendanceConclusionTime
			,source.DepartureTime
			,source.CommissioningSerialNo
			,source.NHSServiceAgreementLineNo
			,source.ProviderReferenceNo
			,source.CommissionerReferenceNo
			,source.ProviderCode
			,source.CommissionerCode
			,source.StaffMemberCode
			,source.InvestigationCodeFirst
			,source.InvestigationCodeSecond
			,source.DiagnosisCodeFirst
			,source.DiagnosisCodeSecond
			,source.TreatmentCodeFirst
			,source.TreatmentCodeSecond
			,source.PASHRGCode
			,source.HRGVersionCode
			,source.PASDGVPCode
			,source.SiteCode
			,source.Created
			,source.Updated
			,source.ByWhom
			,source.InterfaceCode
			,source.PCTCode
			,source.ResidencePCTCode
			,source.InvestigationCodeList
			,source.TriageCategoryCode
			,source.PresentingProblem
			,source.PresentingProblemCode
			,source.ToXrayTime
			,source.FromXrayTime
			,source.ToSpecialtyTime
			,source.SeenBySpecialtyTime
			,source.EthnicCategoryCode
			,source.ReferredToSpecialtyCode
			,source.DecisionToAdmitTime
			,source.DischargeDestinationCode
			,source.RegisteredTime
			,source.TransportRequestTime
			,source.TransportAvailableTime
			,source.AscribeLeftDeptTime
			,source.CDULeftDepartmentTime
			,source.PCDULeftDepartmentTime
			,source.TreatmentDateFirst
			,source.TreatmentDateSecond
			,source.SourceAttendanceDisposalCode
			,source.AmbulanceArrivalTime
			,source.AmbulanceCrewPRF
			,source.UnplannedReattend7Day
			,source.ArrivalTimeAdjusted
			,source.ClinicalAssessmentTime
			,source.LevelOfCareCode
			,source.EncounterBreachStatusCode
			,source.EncounterStartTimeOfDay
			,source.EncounterEndTimeOfDay
			,source.EncounterStartDate
			,source.EncounterEndDate
			,source.EncounterDurationMinutes
			,source.AgeCode
			,source.LeftWithoutBeingSeenCases
			,source.HRGCode
			,source.Reportable
			,source.CareGroup
			,source.EPMINo
			,source.DepartmentTypeCode
			,source.DepartureTimeAdjusted
			,source.CarePathwayAttendanceConclusionTime
			,source.CarePathwayDepartureTime
			,source.ContextCode
			,source.GpCodeAtAttendance
			,source.GpPracticeCodeAtAttendance
			,source.PostcodeAtAttendance
			,source.CCGCode
			,source.AlcoholLocation
			,source.AlcoholInPast12Hours
			,source.ReligionCode
			,source.AttendanceResidenceCCGCode
			,source.ServiceID
			)

	when matched
	and	source.Action <> 'DELETE'
	then
		update
		set
			 target.EncounterRecno = source.EncounterRecno
			,target.UniqueBookingReferenceNo = source.UniqueBookingReferenceNo
			,target.PathwayId = source.PathwayId
			,target.PathwayIdIssuerCode = source.PathwayIdIssuerCode
			,target.RTTStatusCode = source.RTTStatusCode
			,target.RTTStartDate = source.RTTStartDate
			,target.RTTEndDate = source.RTTEndDate
			,target.DistrictNo = source.DistrictNo
			,target.TrustNo = source.TrustNo
			,target.CasenoteNo = source.CasenoteNo
			,target.DistrictNoOrganisationCode = source.DistrictNoOrganisationCode
			,target.NHSNumber = source.NHSNumber
			,target.NHSNumberStatusCode = source.NHSNumberStatusCode
			,target.PatientTitle = source.PatientTitle
			,target.PatientForename = source.PatientForename
			,target.PatientSurname = source.PatientSurname
			,target.PatientAddress1 = source.PatientAddress1
			,target.PatientAddress2 = source.PatientAddress2
			,target.PatientAddress3 = source.PatientAddress3
			,target.PatientAddress4 = source.PatientAddress4
			,target.Postcode = source.Postcode
			,target.DateOfBirth = source.DateOfBirth
			,target.DateOfDeath = source.DateOfDeath
			,target.SexCode = source.SexCode
			,target.CarerSupportIndicator = source.CarerSupportIndicator
			,target.RegisteredGpCode = source.RegisteredGpCode
			,target.RegisteredPracticeCode = source.RegisteredPracticeCode
			,target.AttendanceNumber = source.AttendanceNumber
			,target.ArrivalModeCode = source.ArrivalModeCode
			,target.AttendanceCategoryCode = source.AttendanceCategoryCode
			,target.AttendanceDisposalCode = source.AttendanceDisposalCode
			,target.IncidentLocationTypeCode = source.IncidentLocationTypeCode
			,target.PatientGroupCode = source.PatientGroupCode
			,target.SourceOfReferralCode = source.SourceOfReferralCode
			,target.ArrivalDate = source.ArrivalDate
			,target.ArrivalTime = source.ArrivalTime
			,target.AgeOnArrival = source.AgeOnArrival
			,target.InitialAssessmentTime = source.InitialAssessmentTime
			,target.SeenForTreatmentTime = source.SeenForTreatmentTime
			,target.AttendanceConclusionTime = source.AttendanceConclusionTime
			,target.DepartureTime = source.DepartureTime
			,target.CommissioningSerialNo = source.CommissioningSerialNo
			,target.NHSServiceAgreementLineNo = source.NHSServiceAgreementLineNo
			,target.ProviderReferenceNo = source.ProviderReferenceNo
			,target.CommissionerReferenceNo = source.CommissionerReferenceNo
			,target.ProviderCode = source.ProviderCode
			,target.CommissionerCode = source.CommissionerCode
			,target.StaffMemberCode = source.StaffMemberCode
			,target.InvestigationCodeFirst = source.InvestigationCodeFirst
			,target.InvestigationCodeSecond = source.InvestigationCodeSecond
			,target.DiagnosisCodeFirst = source.DiagnosisCodeFirst
			,target.DiagnosisCodeSecond = source.DiagnosisCodeSecond
			,target.TreatmentCodeFirst = source.TreatmentCodeFirst
			,target.TreatmentCodeSecond = source.TreatmentCodeSecond
			,target.PASHRGCode = source.PASHRGCode
			,target.HRGVersionCode = source.HRGVersionCode
			,target.PASDGVPCode = source.PASDGVPCode
			,target.SiteCode = source.SiteCode
			,target.Created = source.Created
			,target.Updated = source.Updated
			,target.ByWhom = source.ByWhom
			,target.InterfaceCode = source.InterfaceCode
			,target.PCTCode = source.PCTCode
			,target.ResidencePCTCode = source.ResidencePCTCode
			,target.InvestigationCodeList = source.InvestigationCodeList
			,target.TriageCategoryCode = source.TriageCategoryCode
			,target.PresentingProblem = source.PresentingProblem
			,target.PresentingProblemCode = source.PresentingProblemCode
			,target.ToXrayTime = source.ToXrayTime
			,target.FromXrayTime = source.FromXrayTime
			,target.ToSpecialtyTime = source.ToSpecialtyTime
			,target.SeenBySpecialtyTime = source.SeenBySpecialtyTime
			,target.EthnicCategoryCode = source.EthnicCategoryCode
			,target.ReferredToSpecialtyCode = source.ReferredToSpecialtyCode
			,target.DecisionToAdmitTime = source.DecisionToAdmitTime
			,target.DischargeDestinationCode = source.DischargeDestinationCode
			,target.RegisteredTime = source.RegisteredTime
			,target.TransportRequestTime = source.TransportRequestTime
			,target.TransportAvailableTime = source.TransportAvailableTime
			,target.AscribeLeftDeptTime = source.AscribeLeftDeptTime
			,target.CDULeftDepartmentTime = source.CDULeftDepartmentTime
			,target.PCDULeftDepartmentTime = source.PCDULeftDepartmentTime
			,target.TreatmentDateFirst = source.TreatmentDateFirst
			,target.TreatmentDateSecond = source.TreatmentDateSecond
			,target.SourceAttendanceDisposalCode = source.SourceAttendanceDisposalCode
			,target.AmbulanceArrivalTime = source.AmbulanceArrivalTime
			,target.AmbulanceCrewPRF = source.AmbulanceCrewPRF
			,target.UnplannedReattend7Day = source.UnplannedReattend7Day
			,target.ArrivalTimeAdjusted = source.ArrivalTimeAdjusted
			,target.ClinicalAssessmentTime = source.ClinicalAssessmentTime
			,target.LevelOfCareCode = source.LevelOfCareCode
			,target.EncounterBreachStatusCode = source.EncounterBreachStatusCode
			,target.EncounterStartTimeOfDay = source.EncounterStartTimeOfDay
			,target.EncounterEndTimeOfDay = source.EncounterEndTimeOfDay
			,target.EncounterStartDate = source.EncounterStartDate
			,target.EncounterEndDate = source.EncounterEndDate
			,target.EncounterDurationMinutes = source.EncounterDurationMinutes
			,target.AgeCode = source.AgeCode
			,target.LeftWithoutBeingSeenCases = source.LeftWithoutBeingSeenCases
			,target.HRGCode = source.HRGCode
			,target.Reportable = source.Reportable
			,target.CareGroup = source.CareGroup
			,target.EPMINo = source.EPMINo
			,target.DepartmentTypeCode = source.DepartmentTypeCode
			,target.DepartureTimeAdjusted = source.DepartureTimeAdjusted
			,target.CarePathwayAttendanceConclusionTime = source.CarePathwayAttendanceConclusionTime
			,target.CarePathwayDepartureTime = source.CarePathwayDepartureTime
			,target.GpCodeAtAttendance = source.GpCodeAtAttendance
			,target.GpPracticeCodeAtAttendance = source.GpPracticeCodeAtAttendance
			,target.PostcodeAtAttendance = source.PostcodeAtAttendance
			,target.CCGCode = source.CCGCode
			,target.AlcoholLocation = source.AlcoholLocation
			,target.AlcoholInPast12Hours = source.AlcoholInPast12Hours
			,target.ReligionCode = source.ReligionCode
			,target.AttendanceResidenceCCGCode = source.AttendanceResidenceCCGCode
			,target.ServiceID = source.ServiceID

output
	 coalesce(inserted.MergeEncounterRecno, deleted.MergeEncounterRecno)
	,$action
	into
		@MergeSummary
;


select
	 @inserted = sum(Inserted)
	,@updated = sum(Updated)
	,@deleted = sum(Deleted)
from
	(
	select
		 Inserted = case when Action = 'INSERT' then 1 else 0 end
		,Updated = case when Action = 'UPDATE' then 1 else 0 end
		,Deleted = case when Action = 'DELETE' then 1 else 0 end
	from
		@MergeSummary
	) MergeSummary


select
	@Elapsed = DATEDIFF(minute, @StartTime, getdate())


select
	@Stats =
		'Inserted: ' + cast(coalesce(@inserted, 0) as varchar) +
		', Updated: ' + cast(coalesce(@updated, 0) as varchar) +
		', Deleted: ' + cast(coalesce(@deleted, 0) as varchar) +
		', Elapsed time: ' + CAST(@Elapsed as varchar) + ' minutes'


print @Stats

exec [$(Warehouse)].dbo.WriteAuditLogEvent
	 @ProcedureName
	,@Stats


